@echo off

set name=build

if #%1# == #c# goto done

if #%1# == #r# set name=rebuild

set path=%path%;"C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\MSBuild\Current\Bin"

set tool=msbuild.exe /nologo /v:m /m /p:Configuration=%2 

%tool% /t:%name% /p:Platform=Win32 %3\Source\Version\Version.sln || goto done

%tool% /t:%name% /p:Platform=Win32 %3\Source\Tools\Tools.sln || goto done

%tool% /t:%name% /p:Platform=Win32 %3\Source\Config\Config\Config.sln || goto done

%tool% /t:%name% /p:Platform=Win32 %3\Source\Config\Config\Utils\Utils.sln || goto done

%tool% /t:%name% /p:Platform=Win32 %3\Source\Config\Config\OEM\RedLion\RedLionOEM.sln || goto done

%tool% /t:%name% /p:Platform=GcLinuxA8 %3\Source\Linux\Linux\Linux.sln || goto done

%tool% /t:%name% /p:Platform=GcSitara  %3\Source\Runtime\Runtime\Runtime.sln || goto done
%tool% /t:%name% /p:Platform=GcMX51    %3\Source\Runtime\Runtime\Runtime.sln || goto done
%tool% /t:%name% /p:Platform=GcLinuxA8 %3\Source\Runtime\Runtime\Runtime.sln || goto done
%tool% /t:%name% /p:Platform=Win32     %3\Source\Runtime\Runtime\Runtime.sln || goto done

%tool% /t:%name% /p:Platform=GcSitara  %3\Source\DLDs\DLDs\DLDs.sln || goto done
%tool% /t:%name% /p:Platform=GcMX51    %3\Source\DLDs\DLDs\DLDs.sln || goto done
%tool% /t:%name% /p:Platform=GcLinuxA8 %3\Source\DLDs\DLDs\DLDs.sln || goto done
%tool% /t:%name% /p:Platform=Win32     %3\Source\DLDs\DLDs\DLDs.sln || goto done

rem goto nosetup

%tool% /t:%name% /p:Platform=Setup %3\Source\Config\Setup\ConfigSetup.sln   || goto done
%tool% /t:%name% /p:Platform=Setup %3\Source\Runtime\Setup\RuntimeSetup.sln || goto done
%tool% /t:%name% /p:Platform=Setup %3\Source\DLDs\Setup\DldSetup.sln        || goto done

%tool% /t:%name% /p:Platform=Setup %3\Source\Setup\OEM\RedLion\RedLionSetup.sln || goto done

:nosetup

exit 0

:done
