@echo off

for /f "delims=" %%i in ('uuidgen') do set _PGUID=%%i

rep32 "__%2__" "%_PGUID%" %1

set _PGUID=""
