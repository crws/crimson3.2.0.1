
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

// DO NOT EDIT -- AUTOMATICALLY GENERATED

//////////////////////////////////////////////////////////////////////////
//
// Version Information
//

#define C3_VERSION	"3.2.__BUILD__.0"

#define	C3_SUB_VER	" "

#define	C3_REG_VER	"3.2"

#define C3_MAJOR	3

#define C3_MINOR	2

#define C3_BUILD	(1__BUILD__ - 10000)

#define C3_HOTFIX	__HOTFIX__

#define	C3_BRANCH	"__BRANCH__"

#define	C3_PRODUCTNAME	"Crimson 3.2"

#define	C3_COMPANYNAME	"Red Lion Controls Inc"

#define	C3_COPYRIGHT	"Copyright � 1993-2020 Red Lion Controls Inc"

#define	C3_BETA		FALSE

//////////////////////////////////////////////////////////////////////////
//
// Version Comment
//

#ifdef  _DEBUG

#define C3_COMMENT	"__BRANCH__ (Checked)"

#else

#define C3_COMMENT	"__BRANCH__ (Free)"

#endif

// End of File
