
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Database Format
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// NOTE -- Increment DBASE_FORMAT if you change the format of the
// downloaded database image. This will prevent the runtime from
// trying to execute the old and now-invalid image, and force the
// config to download it again in the new format. If you fail to
// do this, the terminal may lock-up upon firmware upgrade!

#ifndef	DBASE_FORMAT

#define DBASE_FORMAT	3229

#endif

// NOTE -- Increment DBASE_MAJOR if you change something so major
// that you expect earlier versions of the configuration to have
// problems loading the database file. Do not increment this unless
// you do something serious, as we want to allow older versions to
// open the file in the majority of cases.

#ifndef	DBASE_MAJOR

#define	DBASE_MAJOR	2

#endif

// End of File
