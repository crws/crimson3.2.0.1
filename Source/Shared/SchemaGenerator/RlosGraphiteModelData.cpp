
#include "Intern.hpp"

#include "RlosGraphiteModelData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

#if defined(AEON_ENVIRONMENT)

extern ICrimsonIdentity    * Create_NandIdentity(UINT uStart, UINT uEnd);
extern IDatabase           * Create_NandDatabase(UINT uStart, UINT uEnd, UINT uFram, UINT uBase, UINT uPool);
extern IEventStorage       * Create_NandEventStorage(UINT uStart, UINT uEnd);
extern IPersist            * Create_NandPersist(UINT uStart, UINT uEnd);
extern IFirmwareProps      * Create_NandFirmwareProps(UINT uStart, UINT uEnd);
extern IFirmwareProgram    * Create_NandFirmwareProgram(UINT uStart, UINT uEnd);
extern IConfigStorage      * Create_NandConfigStorage(UINT uStart, UINT uEnd);
extern IDatabase	   * Create_FileDatabase(CString const &Root, UINT uBase, UINT uPool);
extern IConfigStorage      * Create_FileConfigStorage(CString const &Root);
extern ISchemaGenerator    * Create_RlosSchemaGenerator(void);
extern INetApplicator      * Create_RlosNetApplicator(void);
extern IHardwareApplicator * Create_RlosHardwareApplicator(void);

#endif

//////////////////////////////////////////////////////////////////////////
//
// RLOS Graphite Model Data
//

// Instantiators

DLLAPI IPxeModel * Create_RlosGraphiteModelData(void)
{
	return new CRlosGraphiteModelData();
}

DLLAPI IPxeModel * Create_RlosGraphiteModelData(CString const &Model)
{
	return new CRlosGraphiteModelData(Model);
}

// Constructors

CRlosGraphiteModelData::CRlosGraphiteModelData(void) : CRlosBaseModelData()
{
	BindModel();
}

CRlosGraphiteModelData::CRlosGraphiteModelData(CString const &Model) : CRlosBaseModelData(Model)
{
	BindModel();
}

// IPxeModel

//	START	END	BLOCKS	SIZE	USAGE
//	=======	=======	=======	=======	===============
//	0	8	8	1MB	Boot Loader
//	8	128	120	15MB	Firmware
//	128	248	120	15MB	Upgrade
//	248	256	8	1MB	Identity
//	256	384	128	16MB    Event Log
//	384	512	128	16MB	Persistence
//	512	576	64	8MB	Device Config
//	576	2048	1504	184MB	Database
//	=======	=======	=======	=======	===============

void CRlosGraphiteModelData::MakeAppObjects(void)
{
	#if defined(AEON_ENVIRONMENT)

	piob->RegisterSingleton("c3.firmprops", 0, Create_NandFirmwareProps(8, 128));

	piob->RegisterSingleton("c3.firmpend", 0, Create_NandFirmwareProgram(128, 248));

	piob->RegisterSingleton("c3.identity", 0, Create_NandIdentity(248, 256));

	#if defined(AEON_PLAT_WIN32)

	piob->RegisterSingleton("c3.database", 0, Create_FileDatabase("\\!\\AppDbase\\", 16, 20480));

	#else

	piob->RegisterSingleton("c3.database", 0, Create_NandDatabase(576, 2048, 4, 16, 20480));

	#endif

	piob->RegisterSingleton("c3.eventstorage", 0, Create_NandEventStorage(256, 384));

	piob->RegisterSingleton("c3.persist", 0, Create_NandPersist(384, 512));

	#endif
}

void CRlosGraphiteModelData::MakePxeObjects(void)
{
	#if defined(AEON_ENVIRONMENT)

	piob->RegisterSingleton("c3.schema-generator", 0, Create_RlosSchemaGenerator());

	#if defined(AEON_PLAT_WIN32)

	piob->RegisterSingleton("c3.config-storage", 0, Create_FileConfigStorage("\\!\\SysDbase\\"));

	#else

	piob->RegisterSingleton("c3.config-storage", 0, Create_NandConfigStorage(512, 576));

	#endif

	piob->RegisterSingleton("c3.hardware-applicator", 0, Create_RlosHardwareApplicator());

	piob->RegisterSingleton("c3.net-applicator", 0, Create_RlosNetApplicator());

	#endif
}

BOOL CRlosGraphiteModelData::GetDispList(CArray<DWORD> &List)
{
	return (this->*m_pfnGetDispList)(List);
}

UINT CRlosGraphiteModelData::GetObjCount(char cTag)
{
	return (this->*m_pfnGetObjCount)(cTag);
}

PCDWORD CRlosGraphiteModelData::GetUsbPaths(char cTag)
{
	return (this->*m_pfnGetUsbPaths)(cTag);
}

// Implementation

void CRlosGraphiteModelData::BindModel(void)
{
	if( m_Model == T("g07") ) {

		m_pfnGetDispList = &CRlosGraphiteModelData::G07_GetDispList;
		m_pfnGetObjCount = &CRlosGraphiteModelData::G07_GetObjCount;
		m_pfnGetUsbPaths = &CRlosGraphiteModelData::G07_GetUsbPaths;

		return;
	}

	if( m_Model == T("g09") ) {

		m_pfnGetDispList = &CRlosGraphiteModelData::G09_GetDispList;
		m_pfnGetObjCount = &CRlosGraphiteModelData::G09_GetObjCount;
		m_pfnGetUsbPaths = &CRlosGraphiteModelData::G09_GetUsbPaths;

		return;
	}

	if( m_Model == T("g10") ) {

		m_pfnGetDispList = &CRlosGraphiteModelData::G10_GetDispList;
		m_pfnGetObjCount = &CRlosGraphiteModelData::G10_GetObjCount;
		m_pfnGetUsbPaths = &CRlosGraphiteModelData::G10_GetUsbPaths;

		return;
	}

	if( m_Model == T("g10r") ) {

		m_pfnGetDispList = &CRlosGraphiteModelData::G11_GetDispList;
		m_pfnGetObjCount = &CRlosGraphiteModelData::G10_GetObjCount;
		m_pfnGetUsbPaths = &CRlosGraphiteModelData::G10_GetUsbPaths;

		return;
	}

	if( m_Model == T("g12") ) {

		m_pfnGetDispList = &CRlosGraphiteModelData::G12_GetDispList;
		m_pfnGetObjCount = &CRlosGraphiteModelData::G12_GetObjCount;
		m_pfnGetUsbPaths = &CRlosGraphiteModelData::G12_GetUsbPaths;

		return;
	}

	if( m_Model == T("g15") ) {

		m_pfnGetDispList = &CRlosGraphiteModelData::G15_GetDispList;
		m_pfnGetObjCount = &CRlosGraphiteModelData::G15_GetObjCount;
		m_pfnGetUsbPaths = &CRlosGraphiteModelData::G15_GetUsbPaths;

		return;
	}

	if( m_Model.StartsWith(T("gc")) ) {

		m_pfnGetDispList = &CRlosGraphiteModelData::GCE_GetDispList;
		m_pfnGetObjCount = &CRlosGraphiteModelData::GCE_GetObjCount;
		m_pfnGetUsbPaths = &CRlosGraphiteModelData::GCE_GetUsbPaths;

		return;
	}

	if( m_Model.StartsWith(T("gs")) ) {

		m_pfnGetDispList = &CRlosGraphiteModelData::GSR_GetDispList;
		m_pfnGetObjCount = &CRlosGraphiteModelData::GSR_GetObjCount;
		m_pfnGetUsbPaths = &CRlosGraphiteModelData::GSR_GetUsbPaths;

		return;
	}

	if( m_Model == T("Generic") ) {

		m_pfnGetDispList = &CRlosGraphiteModelData::Gen_GetDispList;
		m_pfnGetObjCount = &CRlosGraphiteModelData::Gen_GetObjCount;
		m_pfnGetUsbPaths = &CRlosGraphiteModelData::Gen_GetUsbPaths;

		return;
	}

	AfxAssert(FALSE);
}

// Model Data

BOOL CRlosGraphiteModelData::G07_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(800, 480));

	return TRUE;
}

UINT CRlosGraphiteModelData::G07_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 1;
		case 'p': return 3;
		case 'b': return 5;
		case 'f': return 0;
		case 't': return 4;
		case 'g': return 48;
	}

	return 0;
}

PCDWORD CRlosGraphiteModelData::G07_GetUsbPaths(char cTag)
{
	#if defined(AEON_ENVIRONMENT)

	if( cTag == 'b' ) {

		static const UsbTreePath p[] = {

			{ { 0, 0, 2, 0, 1, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 7, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 6, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 5, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 4, 0, 0, 0, 0 } },

			{ 0 },

		};

		return PCDWORD(p);
	}

	if( cTag == 't' ) {

		static const UsbTreePath p[] = {

			{ { 0, 0, 3, 0, 2, 4, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 3, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 2, 0, 0, 0 } },

			{ 0 },

			{ { 0, 0, 4, 0, 2, 1, 4, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 3, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 2, 0, 0 } },

			{ 0 },

			{ { 0, 0, 5, 0, 2, 1, 1, 4, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 3, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 2, 0 } },

			{ 0 },

			{ { 0, 0, 6, 0, 2, 1, 1, 1, 4 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 3 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 2 } },

			{ 0 },
			{ 0 },

		};

		return PCDWORD(p);
	}

	#endif

	static DWORD const z[] = { 0, 0 };

	return z;
}

BOOL CRlosGraphiteModelData::G09_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(800, 480));

	return TRUE;
}

UINT CRlosGraphiteModelData::G09_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 2;
		case 'p': return 3;
		case 'b': return 6;
		case 'f': return 0;
		case 't': return 4;
		case 'g': return 48;
	}

	return 0;
}

PCDWORD CRlosGraphiteModelData::G09_GetUsbPaths(char cTag)
{
	#if defined(AEON_ENVIRONMENT)

	if( cTag == 'b' ) {

		static const UsbTreePath p[] = {

			{ { 1, 0, 2, 0, 2, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 1, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 7, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 6, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 5, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 4, 0, 0, 0, 0 } },

			{ 0 },

		};

		return PCDWORD(p);
	}

	if( cTag == 't' ) {

		static const UsbTreePath p[] = {

			{ { 0, 0, 3, 0, 2, 4, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 3, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 2, 0, 0, 0 } },

			{ 0 },

			{ { 0, 0, 4, 0, 2, 1, 4, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 3, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 2, 0, 0 } },

			{ 0 },

			{ { 0, 0, 5, 0, 2, 1, 1, 4, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 3, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 2, 0 } },

			{ 0 },

			{ { 0, 0, 6, 0, 2, 1, 1, 1, 4 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 3 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 2 } },

			{ 0 },
			{ 0 },

		};

		return PCDWORD(p);
	}

	#endif

	static DWORD const z[] = { 0, 0 };

	return z;
}

BOOL CRlosGraphiteModelData::G10_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(640, 480));

	return TRUE;
}

BOOL CRlosGraphiteModelData::G11_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(800, 600));

	return TRUE;
}

UINT CRlosGraphiteModelData::G10_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 2;
		case 'p': return 3;
		case 'b': return 7;
		case 'f': return 0;
		case 't': return 4;
		case 'g': return 48;
	}

	return 0;
}

PCDWORD CRlosGraphiteModelData::G10_GetUsbPaths(char cTag)
{
	#if defined(AEON_ENVIRONMENT)

	if( cTag == 'b' ) {

		static const UsbTreePath p[] = {

			{ { 1, 0, 2, 0, 3, 0, 0, 0, 0 } },
			{ { 1, 0, 2, 0, 2, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 1, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 7, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 6, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 5, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 4, 0, 0, 0, 0 } },

			{ 0 },

		};

		return PCDWORD(p);
	}

	if( cTag == 't' ) {

		static const UsbTreePath p[] = {

			{ { 0, 0, 3, 0, 2, 4, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 3, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 2, 0, 0, 0 } },

			{ 0 },

			{ { 0, 0, 4, 0, 2, 1, 4, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 3, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 2, 0, 0 } },

			{ 0 },

			{ { 0, 0, 5, 0, 2, 1, 1, 4, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 3, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 2, 0 } },

			{ 0 },

			{ { 0, 0, 6, 0, 2, 1, 1, 1, 4 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 3 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 2 } },

			{ 0 },
			{ 0 },

		};

		return PCDWORD(p);
	}

	#endif

	static DWORD const z[] = { 0, 0 };

	return z;
}

BOOL CRlosGraphiteModelData::G12_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(1280, 800));

	return TRUE;
}

UINT CRlosGraphiteModelData::G12_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 2;
		case 'p': return 4;
		case 'b': return 8;
		case 'f': return 0;
		case 't': return 4;
		case 'g': return 48;
	}

	return 0;
}

PCDWORD CRlosGraphiteModelData::G12_GetUsbPaths(char cTag)
{
	#if defined(AEON_ENVIRONMENT)

	if( cTag == 'b' ) {

		static const UsbTreePath p[] = {

			{ { 1, 0, 2, 0, 4, 0, 0, 0, 0 } },
			{ { 1, 0, 2, 0, 3, 0, 0, 0, 0 } },
			{ { 1, 0, 2, 0, 2, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 1, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 7, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 6, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 5, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 4, 0, 0, 0, 0 } },

			{ 0 },

		};

		return PCDWORD(p);
	}

	if( cTag == 't' ) {

		static const UsbTreePath p[] = {

			{ { 0, 0, 3, 0, 2, 4, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 3, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 2, 0, 0, 0 } },

			{ 0 },

			{ { 0, 0, 4, 0, 2, 1, 4, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 3, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 2, 0, 0 } },

			{ 0 },

			{ { 0, 0, 5, 0, 2, 1, 1, 4, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 3, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 2, 0 } },

			{ 0 },

			{ { 0, 0, 6, 0, 2, 1, 1, 1, 4 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 3 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 2 } },

			{ 0 },
			{ 0 },

		};

		return PCDWORD(p);
	}

	#endif

	static DWORD const z[] = { 0, 0 };

	return z;
}

BOOL CRlosGraphiteModelData::G15_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(1024, 768));

	return TRUE;
}

UINT CRlosGraphiteModelData::G15_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 2;
		case 'p': return 4;
		case 'b': return 8;
		case 'f': return 0;
		case 't': return 4;
		case 'g': return 48;
	}

	return 0;
}

PCDWORD CRlosGraphiteModelData::G15_GetUsbPaths(char cTag)
{
	#if defined(AEON_ENVIRONMENT)

	if( cTag == 'b' ) {

		static const UsbTreePath p[] = {

			{ { 1, 0, 2, 0, 4, 0, 0, 0, 0 } },
			{ { 1, 0, 2, 0, 3, 0, 0, 0, 0 } },
			{ { 1, 0, 2, 0, 2, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 1, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 7, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 6, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 5, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 4, 0, 0, 0, 0 } },

			{ 0 },

		};

		return PCDWORD(p);
	}

	if( cTag == 't' ) {

		static const UsbTreePath p[] = {

			{ { 0, 0, 3, 0, 2, 4, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 3, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 2, 0, 0, 0 } },

			{ 0 },

			{ { 0, 0, 4, 0, 2, 1, 4, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 3, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 2, 0, 0 } },

			{ 0 },

			{ { 0, 0, 5, 0, 2, 1, 1, 4, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 3, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 2, 0 } },

			{ 0 },

			{ { 0, 0, 6, 0, 2, 1, 1, 1, 4 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 3 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 2 } },

			{ 0 },
			{ 0 },

		};

		return PCDWORD(p);
	}

	#endif

	static DWORD const z[] = { 0, 0 };

	return z;
}

BOOL CRlosGraphiteModelData::GCE_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(320, 240));
	List.Append(MAKELONG(480, 272));
	List.Append(MAKELONG(640, 480));
	List.Append(MAKELONG(800, 480));
	List.Append(MAKELONG(800, 600));
	List.Append(MAKELONG(1024, 768));
	List.Append(MAKELONG(1280, 720));
	List.Append(MAKELONG(1280, 800));

	return TRUE;
}

UINT CRlosGraphiteModelData::GCE_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 2;
		case 'p': return 3;
		case 'b': return 1;
		case 'f': return 4;
		case 't': return 4;
		case 'g': return 32;
	}

	return 0;
}

PCDWORD CRlosGraphiteModelData::GCE_GetUsbPaths(char cTag)
{
	#if defined(AEON_ENVIRONMENT)

	if( cTag == 'b' ) {

		static const UsbTreePath p[] = {

			{ { 1, 0, 2, 0, 5, 0, 0, 0, 0 } },

			{ 0 },

		};

		return PCDWORD(p);
	}

	if( cTag == 't' ) {

		static const UsbTreePath p[] = {

			{ { 0, 0, 3, 0, 2, 4, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 3, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 2, 0, 0, 0 } },

			{ 0 },

			{ { 0, 0, 4, 0, 2, 1, 4, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 3, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 2, 0, 0 } },

			{ 0 },

			{ { 0, 0, 5, 0, 2, 1, 1, 4, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 3, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 2, 0 } },

			{ 0 },

			{ { 0, 0, 6, 0, 2, 1, 1, 1, 4 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 3 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 2 } },

			{ 0 },
			{ 0 },

		};

		return PCDWORD(p);
	}

	#endif

	static DWORD const z[] = { 0, 0 };

	return z;
}

BOOL CRlosGraphiteModelData::GSR_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(320, 240));
	List.Append(MAKELONG(480, 272));
	List.Append(MAKELONG(640, 480));
	List.Append(MAKELONG(800, 480));
	List.Append(MAKELONG(800, 600));
	List.Append(MAKELONG(1024, 768));
	List.Append(MAKELONG(1280, 720));
	List.Append(MAKELONG(1280, 800));

	return TRUE;
}

UINT CRlosGraphiteModelData::GSR_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 1;
		case 'p': return 3;
		case 'b': return 5;
		case 'f': return 4;
		case 't': return 4;
		case 'g': return 32;
	}

	return 0;
}

PCDWORD CRlosGraphiteModelData::GSR_GetUsbPaths(char cTag)
{
	#if defined(AEON_ENVIRONMENT)

	if( cTag == 'b' ) {

		static const UsbTreePath p[] = {

			{ { 0, 0, 2, 0, 1, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 7, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 6, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 5, 0, 0, 0, 0 } },
			{ { 0, 0, 2, 0, 4, 0, 0, 0, 0 } },

			{ 0 },

		};

		return PCDWORD(p);
	}

	if( cTag == 't' ) {

		static const UsbTreePath p[] = {

			{ { 0, 0, 3, 0, 2, 4, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 3, 0, 0, 0 } },
			{ { 0, 0, 3, 0, 2, 2, 0, 0, 0 } },

			{ 0 },

			{ { 0, 0, 4, 0, 2, 1, 4, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 3, 0, 0 } },
			{ { 0, 0, 4, 0, 2, 1, 2, 0, 0 } },

			{ 0 },

			{ { 0, 0, 5, 0, 2, 1, 1, 4, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 3, 0 } },
			{ { 0, 0, 5, 0, 2, 1, 1, 2, 0 } },

			{ 0 },

			{ { 0, 0, 6, 0, 2, 1, 1, 1, 4 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 3 } },
			{ { 0, 0, 6, 0, 2, 1, 1, 1, 2 } },

			{ 0 },
			{ 0 },

		};

		return PCDWORD(p);
	}

	#endif

	static DWORD const z[] = { 0, 0 };

	return z;
}

BOOL CRlosGraphiteModelData::Gen_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(320, 240));
	List.Append(MAKELONG(480, 272));
	List.Append(MAKELONG(640, 480));
	List.Append(MAKELONG(800, 480));
	List.Append(MAKELONG(800, 600));
	List.Append(MAKELONG(1024, 768));
	List.Append(MAKELONG(1280, 720));
	List.Append(MAKELONG(1280, 800));

	return TRUE;
}

UINT CRlosGraphiteModelData::Gen_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 2;
		case 'p': return 4;
		case 'b': return 8;
		case 'f': return 4;
		case 't': return 4;
		case 'g': return 63;
	}

	return 0;
}

PCDWORD CRlosGraphiteModelData::Gen_GetUsbPaths(char cTag)
{
	static DWORD const z[] = { 0, 0 };

	return z;
}

// End of File
