
#include "Intern.hpp"

#include "LinuxModelData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#if defined(AEON_ENVIRONMENT)

#include "ScriptFiles.hpp"

#endif

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

#if defined(AEON_ENVIRONMENT)

extern ICrimsonIdentity    * Create_NandIdentity(UINT uStart, UINT uEnd);
extern IDatabase           * Create_NandDatabase(UINT uStart, UINT uEnd, UINT uFram, UINT uBase, UINT uPool);
extern IEventStorage       * Create_NandEventStorage(UINT uStart, UINT uEnd);
extern IPersist            * Create_NandPersist(UINT uStart, UINT uEnd);
extern IFirmwareProps      * Create_NandFirmwareProps(UINT uStart, UINT uEnd);
extern IConfigStorage      * Create_NandConfigStorage(UINT uStart, UINT uEnd);
extern IDatabase	   * Create_FileDatabase(CString const &Root, UINT uBase, UINT uPool);
extern IConfigStorage      * Create_FileConfigStorage(CString const &Root);
extern IFirmwareProps      * Create_FileFirmwareProps(CString const &Root);
extern IFirmwareProgram    * Create_FileFirmwareProgram(CString const &Root);
extern ISchemaGenerator    * Create_LinuxDaxSchemaGenerator(void);
extern INetApplicator      * Create_LinuxNetApplicator(void);
extern INetApplicator      * Create_RlosNetApplicator(void);
extern IHardwareApplicator * Create_LinuxHardwareApplicator(void);

#endif

//////////////////////////////////////////////////////////////////////////
//
// Linux Model Data
//

// Instantiator

DLLAPI IPxeModel * Create_LinuxModelData(void)
{
	return new CLinuxModelData;
}

DLLAPI IPxeModel * Create_LinuxModelData(CString const &Model)
{
	return new CLinuxModelData(Model);
}

// Constructor

CLinuxModelData::CLinuxModelData(void)
{
	#if defined(AEON_ENVIRONMENT)

	m_pPlatform = NULL;

	AfxGetObject("platform", 0, IPlatform, m_pPlatform);

	m_Model   = m_pPlatform->GetModel();

	m_Variant = m_pPlatform->GetVariant();

	m_dwSize  = 0;

	m_Model.MakeLower();

	m_Variant.MakeLower();

	DeployScripts();

	#endif

	StdSetRef();
}

CLinuxModelData::CLinuxModelData(CString Model)
{
	m_pPlatform = NULL;

	m_Model     = Model.StripToken(L'|');

	m_Variant   = Model.StripToken(L'|');

	ParseSize(Model.StripToken(L'|'));

	m_Options   = Model;

	m_Options.Replace('|', ',');

	StdSetRef();
}

// Destructor

CLinuxModelData::~CLinuxModelData(void)
{
	#if defined(AEON_ENVIRONMENT)

	AfxRelease(m_pPlatform);

	#endif
}

// IUnknown

HRESULT CLinuxModelData::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPxeModel);

	StdQueryInterface(IPxeModel);

	return E_NOINTERFACE;
}

ULONG CLinuxModelData::AddRef(void)
{
	StdAddRef();
}

ULONG CLinuxModelData::Release(void)
{
	StdRelease();
}

// IPxeModel

void CLinuxModelData::MakeAppObjects(void)
{
	#if defined(AEON_ENVIRONMENT)

	piob->RegisterSingleton("c3.firmprops", 0, Create_FileFirmwareProps("\\.\\opt\\crimson\\bin\\"));

	piob->RegisterSingleton("c3.firmpend", 0, Create_FileFirmwareProgram("\\.\\tmp\\crimson\\fwupdate\\"));

	piob->RegisterSingleton("c3.bootpend", 0, Create_FileFirmwareProgram("\\.\\tmp\\crimson\\osupdate\\"));

	piob->RegisterSingleton("c3.database", 0, Create_FileDatabase("\\!\\appbase\\", 16, 20480));

	piob->RegisterSingleton("c3.identity", 0, Create_NandIdentity(0, 16));

	piob->RegisterSingleton("c3.eventstorage", 0, Create_NandEventStorage(16, 136));

	piob->RegisterSingleton("c3.persist", 0, Create_NandPersist(136, 256));

	#endif
}

void CLinuxModelData::MakePxeObjects(void)
{
	#if defined(AEON_ENVIRONMENT)

	piob->RegisterSingleton("c3.schema-generator", 0, Create_LinuxDaxSchemaGenerator());

	piob->RegisterSingleton("c3.config-storage", 0, Create_FileConfigStorage("\\!\\sysbase\\"));

	piob->RegisterSingleton("c3.hardware-applicator", 0, Create_LinuxHardwareApplicator());

	#if defined(AEON_PLAT_WIN32)

	piob->RegisterSingleton("c3.net-applicator", 0, Create_RlosNetApplicator());

	#else

	piob->RegisterSingleton("c3.net-applicator", 0, Create_LinuxNetApplicator());

	#endif

	#endif
}

BOOL CLinuxModelData::AdjustHardware(CJsonData *pData)
{
	CMap<CString, CString> Map;

	if( ParseOptions(Map) ) {

		if( TRUE ) {

			UINT       uSleds = GetObjCount('x');

			CJsonData *pSleds = NULL;

			pData->AddChild(T("sleds.slist"), TRUE, pSleds);

			pSleds->Empty();

			for( UINT s = 0; s < uSleds; s++ ) {

				CPrintf    Sled(T("s%u"), s);

				INDEX      Find = Map.FindName(Sled);

				CJsonData *pRow = NULL;

				pSleds->AddChild(FALSE, pRow);

				if( !Map.Failed(Find) ) {

					pRow->AddValue(T("type"), Map.GetData(Find), jsonString);
				}
				else {
					pRow->AddValue(T("type"), T("0"), jsonString);
				}

				pRow->AddValue(T("order"), CPrintf(T("%u"), s), jsonString);
			}
		}

		if( TRUE ) {

			INDEX Find = Map.FindName("g");

			if( !Map.Failed(Find) ) {

				pData->AddValue("general.group", Map.GetData(Find), jsonString);
			}
		}
	}

	if( TRUE ) {

		UINT uPorts = GetObjCount('s');

		for( UINT p = 0; p < uPorts; p++ ) {

			CPrintf Port(T("base.port%u"), 1+p);

			CPrintf Type(T("%u"), GetPortType(p));

			pData->AddValue(Port, Type);
		}
	}

	if( TRUE ) {

		DWORD dwSize = m_dwSize ? m_dwSize : MAKELONG(800, 480);

		pData->AddValue(T("general.format"), CPrintf(T("%u"), dwSize), jsonString);
	}

	return TRUE;
}

BOOL CLinuxModelData::ApplyModelSpec(CString Model)
{
	m_Model     = Model.StripToken(L'|');

	m_Variant   = Model.StripToken(L'|');

	ParseSize(Model.StripToken(L'|'));

	m_Options   = Model;

	m_Options.Replace('|', ',');

	return TRUE;
}

BOOL CLinuxModelData::AppendModelSpec(CString &Model, CJsonData *pData)
{
	CJsonData *pList = pData->GetChild(T("sleds.slist"));

	if( pList ) {

		UINT uSleds = GetObjCount('x');

		BOOL fInit  = TRUE;

		for( UINT s = 0; s < uSleds; s++ ) {

			CJsonData *pSled = pList->GetChild(s);

			if( pSled ) {

				UINT uType = tatoi(pSled->GetValue(T("type"), T("0")));

				if( uType ) {

					Model += fInit ? T('|') : T(',');

					Model += CPrintf(T("s%u=%u"), s, uType);

					fInit = FALSE;
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxModelData::GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(320, 240));
	List.Append(MAKELONG(480, 272));
	List.Append(MAKELONG(640, 480));
	List.Append(MAKELONG(800, 480));
	List.Append(MAKELONG(800, 600));
	List.Append(MAKELONG(1024, 768));
	List.Append(MAKELONG(1280, 720));
	List.Append(MAKELONG(1280, 800));

	return TRUE;
}

UINT CLinuxModelData::GetObjCount(char cTag)
{
	return GetDaxObjCount(cTag);
}

PCDWORD CLinuxModelData::GetUsbPaths(char cTag)
{
	return GetDaxUsbPaths(cTag);
}

UINT CLinuxModelData::GetPortType(UINT uPort)
{
	if( m_Model == T("da50") ) {

		switch( uPort ) {

			case 0: return 0;
			case 1: return 1;
		}
	}

	if( m_Model == T("da70") ) {

		if( m_Variant == T("0f") ) {

			switch( uPort ) {

				case 0: return 0;
				case 1: return 0;
				case 2: return 1;
			}
		}

		if( m_Variant == T("0g") ) {

			switch( uPort ) {

				case 0: return 0;
				case 1: return 1;
				case 2: return 1;
			}
		}
	}

	return 0;
}

// Implementation

BOOL CLinuxModelData::ParseSize(CString s)
{
	UINT p = s.Find('x');

	if( p < NOTHING ) {

		int x = tatoi(PCTXT(s));

		int y = tatoi(PCTXT(s) + p + 1);

		m_dwSize = MAKELONG(x, y);

		return TRUE;
	}

	return FALSE;
}

UINT CLinuxModelData::GetDaxObjCount(char cTag)
{
	if( m_Model == T("da50") ) {

		switch( cTag ) {

			case 'e': return 2;
			case 's': return 2;
			case 'm': return 0;
			case 'x': return 1;
			case 'g': return 51;
			case 'i': return 1;
		}
	}

	if( m_Model == T("da70") ) {

		switch( cTag ) {

			case 'e': return 2;
			case 's': return 3;
			case 'm': return 10;
			case 'x': return 3;
			case 'g': return 51;
			case 'i': return 0;
		}
	}

	return 0;
}

PCDWORD CLinuxModelData::GetDaxUsbPaths(char cTag)
{
	return NULL;
}

BOOL CLinuxModelData::DeployScripts(void)
{
	#if defined(AEON_ENVIRONMENT)

	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		bool fRestart = false;

		CString Path("/./tmp/crimson/scripts");

		mkdir(Path, 0755);

		CScriptFiles    Files;

		CScriptFileData const *pData;

		for( UINT f = 0; Files.FindFile(pData, f); f++ ) {

			CString   Name(pData->m_pName);

			CString   Full(Path + '/' + Name);

			CAutoFile File(Full, "w");

			if( File ) {

				CString Text(PCSTR(pData->m_pData), pData->m_uSize);

				Text.Replace("\r\n", "\n");

				File.Write(PCSTR(Text), Text.GetLength());

				File.Close();

				chmod(Full, 0755);
			}
		}

		if( true ) {

			static PCSTR List[] = { "sysinit", "crimson", "syslog", "udev2" };

			for( UINT n = 0; n < elements(List); n++ ) {

				CPrintf From("%s/c3-rc-%s", PCSTR(Path.Mid(2)), List[n]);

				CPrintf Dest(n ? "/etc/init.d/%s" : "/etc/rc.d/rc.%s", List[n]);

				if( FileCrc(From) != FileCrc(Dest) ) {

					CPrintf Args("-p %s %s", PCSTR(From), PCSTR(Dest));

					pLinux->CallProcess("/bin/cp", Args, NULL, NULL);

					fRestart = true;
				}
			}
		}

		if( true ) {

			mkdir("/./opt/crimson/scripts", 0755);

			CString Args("/opt/crimson/scripts " + Path.Mid(2));

			pLinux->CallProcess("/opt/crimson/bin/Flipper", Args, ".", ".");
		}

		if( fRestart ) {

			AfxTrace("restarting after rc file update\n");

			m_pPlatform->ResetSystem();
		}

		return TRUE;
	}

	#endif

	return FALSE;
}

BOOL CLinuxModelData::ParseOptions(CMap<CString, CString> &Map)
{
	if( !m_Options.IsEmpty() ) {

		CStringArray List;

		m_Options.Tokenize(List, ',');

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CString Data = List[n];

			CString Name = Data.StripToken('=');

			Map.Insert(Name, Data);
		}

		return TRUE;
	}

	return FALSE;
}

DWORD CLinuxModelData::FileCrc(CString const &Name)
{
	#if defined(AEON_ENVIRONMENT)

	CAutoFile File("/./" + Name, "r");

	if( File ) {

		UINT  uSize = File.GetSize();

		int   fd    = fileno(File);

		UINT  uBuff = 32 * 1024;

		DWORD calc  = 0;

		CAutoArray<BYTE> Data(uBuff);

		lseek(fd, 0, 0);

		while( uSize ) {

			UINT uLump = min(uSize, uBuff);

			UINT uRead = read(fd, PBYTE(Data), uLump);

			if( uRead == uLump ) {

				calc  = crc(calc, Data, uRead);

				uSize = uSize - uRead;

				continue;
			}

			return 0;
		}

		return calc;
	}

	#endif

	return 0;
}

// End of File
