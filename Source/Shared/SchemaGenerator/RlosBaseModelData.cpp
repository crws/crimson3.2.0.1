
#include "Intern.hpp"

#include "RlosBaseModelData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

DLLAPI IPxeModel * Create_RlosDaxModelData(void);

DLLAPI IPxeModel * Create_RlosGraphiteModelData(void);

DLLAPI IPxeModel * Create_RlosCrxModelData(void);

DLLAPI IPxeModel * Create_RlosDaxModelData(CString const &Model);

DLLAPI IPxeModel * Create_RlosGraphiteModelData(CString const &Model);

DLLAPI IPxeModel * Create_RlosCrxModelData(CString const &Model);

//////////////////////////////////////////////////////////////////////////
//
// RLOS Base Model Data
//

// Instantiators

DLLAPI IPxeModel * Create_RlosModelData(void)
{
	#if defined(AEON_ENVIRONMENT)

	AfxGetAutoObject(pPlatform, "platform", 0, IPlatform);

	if( pPlatform ) {

		CString Model = pPlatform->GetModel();

		if( Model.StartsWith(T("da")) ) {

			return Create_RlosDaxModelData();
		}

		if( Model.StartsWith(T("g")) ) {

			return Create_RlosGraphiteModelData();
		}

		if( Model.StartsWith(T("co")) || Model.StartsWith(T("ca")) ) {

			return Create_RlosCrxModelData();
		}
	}

	#endif

	return NULL;
}

DLLAPI IPxeModel * Create_RlosModelData(CString const &Model)
{
	if( Model.StartsWith(T("da")) ) {

		return Create_RlosDaxModelData(Model);
	}

	if( Model.StartsWith(T("g")) ) {

		return Create_RlosGraphiteModelData(Model);
	}

	if( Model.StartsWith(T("co")) || Model.StartsWith(T("ca")) ) {

		return Create_RlosCrxModelData(Model);
	}

	return NULL;
}

// Constructors

CRlosBaseModelData::CRlosBaseModelData(void)
{
	#if defined(AEON_ENVIRONMENT)

	m_pPlatform = NULL;

	AfxGetObject("platform", 0, IPlatform, m_pPlatform);

	m_Model   = m_pPlatform->GetModel();

	m_Variant = m_pPlatform->GetVariant();

	m_dwSize  = 0;

	m_Model.MakeLower();

	m_Variant.MakeLower();

	#endif

	StdSetRef();
}

CRlosBaseModelData::CRlosBaseModelData(CString Model)
{
	m_pPlatform = NULL;

	m_Model     = Model.StripToken(L'|');

	m_Variant   = Model.StripToken(L'|');

	ParseSize(Model.StripToken(L'|'));

	m_Options   = Model;

	m_Options.Replace('|', ',');

	StdSetRef();
}

// Destructor

CRlosBaseModelData::~CRlosBaseModelData(void)
{
	#if defined(AEON_ENVIRONMENT)

	AfxRelease(m_pPlatform);

	#endif
}

// IUnknown

HRESULT CRlosBaseModelData::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPxeModel);

	StdQueryInterface(IPxeModel);

	return E_NOINTERFACE;
}

ULONG CRlosBaseModelData::AddRef(void)
{
	StdAddRef();
}

ULONG CRlosBaseModelData::Release(void)
{
	StdRelease();
}

// IPxeModel

BOOL CRlosBaseModelData::AdjustHardware(CJsonData *pData)
{
	CMap<CString, CString> Map;

	if( ParseOptions(Map) ) {

		if( TRUE ) {

			INDEX Find = Map.FindName("g");

			if( !Map.Failed(Find) ) {

				pData->AddValue("general.group", Map.GetData(Find), jsonString);
			}
		}
	}

	if( TRUE ) {

		DWORD dwSize = m_dwSize ? m_dwSize : MAKELONG(800, 480);

		pData->AddValue(T("general.format"), CPrintf(T("%u"), dwSize), jsonString);
	}

	return TRUE;
}

BOOL CRlosBaseModelData::ApplyModelSpec(CString Model)
{
	return TRUE;
}

BOOL CRlosBaseModelData::AppendModelSpec(CString &Model, CJsonData *pData)
{
	return TRUE;
}

UINT CRlosBaseModelData::GetPortType(UINT uPort)
{
	return 0;
}

// Implementation

BOOL CRlosBaseModelData::ParseSize(CString s)
{
	UINT p = s.Find('x');

	if( p < NOTHING ) {

		int x = tatoi(PCTXT(s));

		int y = tatoi(PCTXT(s) + p + 1);

		m_dwSize = MAKELONG(x, y);

		return TRUE;
	}

	m_dwSize = 0;

	return FALSE;
}

BOOL CRlosBaseModelData::ParseOptions(CMap<CString, CString> &Map)
{
	if( !m_Options.IsEmpty() ) {

		CStringArray List;

		m_Options.Tokenize(List, ',');

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CString Data = List[n];

			CString Name = Data.StripToken('=');

			Map.Insert(Name, Data);
		}

		return TRUE;
	}

	return FALSE;
}

// End of File
