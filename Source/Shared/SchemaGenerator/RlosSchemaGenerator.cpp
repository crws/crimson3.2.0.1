
#include "Intern.hpp"

#include "RlosSchemaGenerator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SchemaFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphite Schema Generator
//

// Instantiator

DLLAPI ISchemaGenerator * Create_RlosSchemaGenerator(void)
{
	return new CRlosSchemaGenerator;
}

DLLAPI ISchemaGenerator * Create_RlosSchemaGenerator(IConfigStorage *pConfig, IPxeModel *pModel)
{
	return new CRlosSchemaGenerator(pConfig, pModel);
}

// Constructor

CRlosSchemaGenerator::CRlosSchemaGenerator(void)
{
	m_Default = "sc-rlos";

	m_pModel  = NULL;

	#if defined(AEON_ENVIRONMENT)

	AfxGetObject("c3.model", 0, IPxeModel, m_pModel);

	#endif
}

CRlosSchemaGenerator::CRlosSchemaGenerator(IConfigStorage *pConfig, IPxeModel *pModel)
{
	m_Default = "sc-rlos";

	m_pModel  = pModel;

	m_pConfig = pConfig;

	m_pModel->AddRef();

	m_pConfig->AddRef();
}

// Destructor

CRlosSchemaGenerator::~CRlosSchemaGenerator(void)
{
	AfxRelease(m_pModel);
}

// Overridables

bool CRlosSchemaGenerator::MakeHardwareSchema(void)
{
	CAutoPointer<CJsonData> pJson(GetFileJson(T("hs-rlos")));

	if( pJson ) {

		AddPermittedGroups(pJson, m_pModel->GetObjCount('g'));

		CArray<DWORD> List;

		m_pModel->GetDispList(List);

		AddDisplaySize(pJson, List);

		UINT nb = m_pModel->GetObjCount('b');
		
		UINT nf = m_pModel->GetObjCount('f');
		
		UINT nt = m_pModel->GetObjCount('t');

		if( !nb ) {

			pJson->Delete("children.0001");
		}
		else {
			CJsonData *pBase = pJson->GetChild("children.0001.children.0000.tabs.0000.sections.0000.table");

			pBase->AddValue("rows", CPrintf("%u", nb));

			if( nf ) {

				CJsonData *pConf = NULL;

				pJson->GetChild("children.0001.tabs.0000.sections")->AddChild(FALSE, pConf);

				pConf->Parse(GetFileText(T("hs-rlos-conf-fixed")));

				pConf->AddValue("fields.0000.format", GetEnumCount(nf));

				CJsonData *pHead = NULL;

				pJson->GetChild("children.0001.children")->AddChild(FALSE, pHead);

				pHead->Parse(GetFileText(T("hs-rlos-head-fixed")));

				for( UINT r = 0; r < nf; r++ ) {

					CJsonData *pRack = NULL;

					pHead->GetChild("children")->AddChild(FALSE, pRack);

					pRack->Parse(GetFileText(T("hs-rlos-rack-fixed")));

					Substitute(pRack, "{n}", CPrintf("%u", 1+r));
				}
			}

			if( nt ) {

				CJsonData *pConf = NULL;

				pJson->GetChild("children.0001.tabs.0000.sections")->AddChild(FALSE, pConf);

				pConf->Parse(GetFileText(T("hs-rlos-conf-tethered")));

				pConf->AddValue("fields.0000.format", GetEnumCount(nt));

				CJsonData *pHead = NULL;

				pJson->GetChild("children.0001.children")->AddChild(FALSE, pHead);

				pHead->Parse(GetFileText(T("hs-rlos-head-tethered")));

				for( UINT r = 0; r < nt; r++ ) {

					CJsonData *pRack = NULL;

					pHead->GetChild("children")->AddChild(FALSE, pRack);

					pRack->Parse(GetFileText(T("hs-rlos-rack-tethered")));

					Substitute(pRack, "{n}", CPrintf("%u", 1+r));
				}
			}
		}

		AddFragment(pJson, T("children"), T("hs-ext-modem"));

		AddFragment(pJson, T("children"), T("hs-ext-gps"));

		m_HSchema = pJson->GetAsText(true);

		CheckConfig('h');

		return true;
	}

	return false;
}

bool CRlosSchemaGenerator::MakePersonalitySchema(void)
{
	m_PSchema = GetFileText(T("ps-master"));

	return true;
}

bool CRlosSchemaGenerator::MakeSystemSchema(void)
{
	CAutoPointer<CJsonData> pJson(GetFileJson(T("ss-rlos")));

	if( pJson ) {

		UINT ne = m_pModel->GetObjCount('e');

		UINT nm = 0;

		CJsonData *pList = pJson->GetChild("children.0000.children.0001.children");

		for( UINT i = 0; i < ne; i++ ) {

			CJsonData *pFace = AddInterface(pList, i, T("ss-rlos-ethernet"));

			pFace->GetChild("tabs.0000")->AddValue("note", "Base Unit");
		}

		CAutoPointer<CJsonData> pHard(New CJsonData);

		pHard->Parse(GetHardwareConfig());

		ScanForModules(pList, pHard, 106, ne, T("ss-rlos-ethernet"));

		if( pHard->GetValue("modem.enable", "0") == "1" ) {

			CJsonData *pFace = AddInterface(pList, nm, T("ss-rlos-ext-modem"));

			pFace->GetChild("tabs.0000")->AddValue("note", "External Modem");

			nm++;
		}

		ScanForModules(pList, pHard, 100, nm, T("ss-rlos-hspa-modem"));

		AddServices(pJson);

		CString EnumFaces = "Automatic";

		CString EnumGps;

		CString EnumCell;

		for( INDEX i = pList->GetHead(); !pList->Failed(i); pList->GetNext(i) ) {

			CString Name = pList->GetChild(i)->GetValue("label", "");

			if( !Name.IsEmpty() ) {

				EnumFaces += "|";

				EnumFaces += Name;
			}
		}

		if( pHard->GetValue("gps.enable", "0") == "1" ) {

			EnumGps += "|External GPS";
		}

		CJsonData *pAliases = pJson->GetChild("aliases");

		pAliases->AddValue("gpstime", "Automatic" + EnumGps);

		pAliases->AddValue("celltime", "Automatic" + EnumCell);

		pAliases->AddValue("locs", "None|Fixed" + EnumGps);

		pAliases->AddValue("faces", EnumFaces);

		m_SSchema = pJson->GetAsText(true);

		CheckConfig('s');

		return true;
	}

	return false;
}

bool CRlosSchemaGenerator::MakeUserSchema(void)
{
	m_USchema = GetFileText(T("us-master"));

	return true;
}

bool CRlosSchemaGenerator::AdjustDefault(char cTag, CJsonData *pData)
{
	if( cTag == 'h' ) {

		UINT uMask = m_pModel->GetObjCount('g');

		if( uMask == 32 ) {

			pData->AddValue(T("general.group"), CPrintf(T("%u"), 5), jsonString);
		}
		else {
			for( UINT g = 5; g > 0; g-- ) {

				if( uMask & (1 << (g-1)) ) {

					pData->AddValue(T("general.group"), CPrintf(T("%u"), g-1), jsonString);

					break;
				}
			}
		}

		m_pModel->AdjustHardware(pData);
	}

	return true;
}

// Implementation

void CRlosSchemaGenerator::ScanForModules(CJsonData *pList, CJsonData *pHard, UINT uType, UINT uFrom, PCTXT pFragment)
{
	UINT nb = m_pModel->GetObjCount('b');

	UINT nf = tatoi(pHard->GetValue("exp.fcount", "0"));

	UINT nt = tatoi(pHard->GetValue("exp.tcount", "0"));

	for( UINT n = 0; n < nb + 3 * nf + 3 * nt; n++ ) {

		CJsonData *pSlot = NULL;

		CString    Where;

		if( !pSlot && n < nb ) {

			pSlot = pHard->GetChild(CPrintf("exp.base.modules.%4.4X", n));

			Where.Printf("Expansion Slot %u", 1+ n);
		}

		if( !pSlot && n < nb + 3 * nf ) {

			int b = nb;

			int r = 1 + (n - b) / 3;

			int s = (n - b) % 3;

			pSlot = pHard->GetChild(CPrintf("exp.fixed.rack%u.modules.%4.4X", r, s));

			Where.Printf("Fixed Rack %u Slot %u", r, 1 + s);
		}

		if( !pSlot && n < nb + 3 * nf + 3 * nt ) {

			int b = nb + 3 * nf;

			int r = 1 + (n - b) / 3;

			int s = (n - b) % 3;

			pSlot = pHard->GetChild(CPrintf("exp.tethered.rack%u.modules.%4.4X", r, s));

			Where.Printf("Tethered Rack %u Slot %u", r, 1 + s);
		}

		if( pSlot ) {

			if( UINT(tatoi(pSlot->GetValue("type", "0"))) == uType ) {

				CJsonData *pFace = AddInterface(pList, uFrom, pFragment);

				pFace->GetChild("tabs.0000")->AddValue("note", Where);

				uFrom++;
			}
		}
	}
}

CJsonData * CRlosSchemaGenerator::AddInterface(CJsonData *pList, UINT uIndex, PCTXT pFragment)
{
	CJsonData *pFace = NULL;

	pList->AddChild(FALSE, pFace);

	pFace->Parse(GetFileText(pFragment));

	Substitute(pFace, "{i}", CPrintf("%u", uIndex));

	Substitute(pFace, "{n}", CPrintf("%u", uIndex+1));

	return pFace;
}

void CRlosSchemaGenerator::AddServices(CJsonData *pSchema)
{
	static PCTXT pName[] = {

		T("s-time"),
		T("s-ftps"),
		T("s-loc"),
		T("s-sms"),
		T("s-smtp")
	};

	CJsonData *pList = pSchema->GetChild("children.0001.children");

	for( UINT s = 0; s < elements(pName); s++ ) {

		CString Frag;

		if( pName[s][0] == 's' ) {

			Frag = "ss-serv-" + CString(pName[s]+2);
		}

		AddFragment(pList, Frag);
	}
}

// End of File
