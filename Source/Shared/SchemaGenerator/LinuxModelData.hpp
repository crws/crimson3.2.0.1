
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinuxModelData_HPP

#define INCLUDE_LinuxModelData_HPP

//////////////////////////////////////////////////////////////////////////
//
// Linux Model Data
//

class CLinuxModelData : public IPxeModel
{
public:
	// Constructors
	CLinuxModelData(void);
	CLinuxModelData(CString Model);

	// Destructor
	~CLinuxModelData(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IPxeModel
	void    METHOD MakeAppObjects(void);
	void    METHOD MakePxeObjects(void);
	BOOL    METHOD AdjustHardware(CJsonData *pData);
	BOOL    METHOD ApplyModelSpec(CString Model);
	BOOL	METHOD AppendModelSpec(CString &Model, CJsonData *pData);
	BOOL    METHOD GetDispList(CArray<DWORD> &List);
	UINT    METHOD GetObjCount(char cTag);
	PCDWORD METHOD GetUsbPaths(char cTag);
	UINT    METHOD GetPortType(UINT uPort);

protected:
	// Data Members
	ULONG	    m_uRefs;
	IPlatform * m_pPlatform;
	CString     m_Model;
	CString     m_Variant;
	CString	    m_Options;
	DWORD	    m_dwSize;

	// Implementation
	BOOL    ParseSize(CString s);
	UINT    GetDaxObjCount(char cTag);
	PCDWORD GetDaxUsbPaths(char cTag);
	BOOL    DeployScripts(void);
	BOOL	ParseOptions(CMap<CString, CString> &Map);
	DWORD   FileCrc(CString const &Name);
};

// End of File

#endif
