
#include "Intern.hpp"

#include "SchemaFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Helper
//

#define Entry(f, p) { T(f), data_##p, size_##p }

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

#include "hc-default.json.dat"
#include "hs-ext-modem.json.dat"
#include "hs-ext-gps.json.dat"
#include "hs-dax.json.dat"
#include "hs-dax-serial-50.json.dat"
#include "hs-dax-serial-70.json.dat"
#include "hs-dax-tunnels.json.dat"
#include "hs-dax-tools.json.dat"
#include "hs-dax-vlans.json.dat"
#include "hs-rlos.json.dat"
#include "hs-rlos-conf-fixed.json.dat"
#include "hs-rlos-conf-tethered.json.dat"
#include "hs-rlos-head-fixed.json.dat"
#include "hs-rlos-head-tethered.json.dat"
#include "hs-rlos-rack-fixed.json.dat"
#include "hs-rlos-rack-tethered.json.dat"
#include "pc-default.json.dat"
#include "ps-master.json.dat"
#include "ss-serv-ftps.json.dat"
#include "ss-serv-loc.json.dat"
#include "ss-serv-sms.json.dat"
#include "ss-serv-smtp.json.dat"
#include "ss-serv-svm.json.dat"
#include "ss-serv-time.json.dat"
#include "sc-linux.json.dat"
#include "ss-linux.json.dat"
#include "ss-linux-iface-addips.json.dat"
#include "ss-linux-iface-checker.json.dat"
#include "ss-linux-iface-config.json.dat"
#include "ss-linux-iface-dhcp-s.json.dat"
#include "ss-linux-iface-iptfwd.json.dat"
#include "ss-linux-iface-macs.json.dat"
#include "ss-linux-iface-dyndns.json.dat"
#include "ss-linux-iface-routing.json.dat"
#include "ss-linux-iface-simple.json.dat"
#include "ss-linux-iface-status.json.dat"
#include "ss-linux-eth-base.json.dat"
#include "ss-linux-eth-sled.json.dat"
#include "ss-linux-vlan.json.dat"
#include "ss-linux-cell-sled.json.dat"
#include "ss-linux-wifi-sled.json.dat"
#include "ss-linux-tun-gre.json.dat"
#include "ss-linux-tun-ipsec.json.dat"
#include "ss-linux-tun-l2tp.json.dat"
#include "ss-linux-tun-open.json.dat"
#include "ss-linux-tun-otap.json.dat"
#include "ss-linux-serv-dhcp-r.json.dat"
#include "ss-linux-serv-dhcp-s.json.dat"
#include "ss-linux-serv-geo.json.dat"
#include "ss-linux-serv-io.json.dat"
#include "ss-linux-serv-snmp.json.dat"
#include "ss-linux-serv-syslog.json.dat"
#include "ss-linux-serv-tls-c.json.dat"
#include "ss-linux-serv-tls-s.json.dat"
#include "ss-linux-fw-custom.json.dat"
#include "ss-linux-fw-filters.json.dat"
#include "ss-linux-fw-forward.json.dat"
#include "ss-linux-fw-inbound.json.dat"
#include "ss-linux-fw-lists.json.dat"
#include "ss-linux-fw-nat.json.dat"
#include "sc-rlos.json.dat"
#include "ss-rlos.json.dat"
#include "ss-rlos-ethernet.json.dat"
#include "ss-rlos-ext-modem.json.dat"
#include "ss-rlos-hspa-modem.json.dat"
#include "uc-default.json.dat"
#include "us-master.json.dat"

//////////////////////////////////////////////////////////////////////////
//
// File Table
//

CSchemaFileData const CSchemaFiles::m_Files[] = {

	Entry("hc-default",			hc_default_json),
	Entry("hs-ext-modem",			hs_ext_modem_json),
	Entry("hs-ext-gps",			hs_ext_gps_json),
	Entry("hs-dax",				hs_dax_json),
	Entry("hs-dax-serial-50",		hs_dax_serial_50_json),
	Entry("hs-dax-serial-70",		hs_dax_serial_70_json),
	Entry("hs-dax-tunnels",			hs_dax_tunnels_json),
	Entry("hs-dax-tools",			hs_dax_tools_json),
	Entry("hs-dax-vlans",			hs_dax_vlans_json),
	Entry("hs-rlos",			hs_rlos_json),
	Entry("hs-rlos-conf-fixed",		hs_rlos_conf_fixed_json),
	Entry("hs-rlos-conf-tethered",		hs_rlos_conf_tethered_json),
	Entry("hs-rlos-head-fixed",		hs_rlos_head_fixed_json),
	Entry("hs-rlos-head-tethered",		hs_rlos_head_tethered_json),
	Entry("hs-rlos-rack-fixed",		hs_rlos_rack_fixed_json),
	Entry("hs-rlos-rack-tethered",		hs_rlos_rack_tethered_json),
	Entry("pc-default",			pc_default_json),
	Entry("ps-master",			ps_master_json),
	Entry("uc-default",			uc_default_json),
	Entry("us-master",			us_master_json),
	Entry("ss-serv-ftps",			ss_serv_ftps_json),
	Entry("ss-serv-loc",			ss_serv_loc_json),
	Entry("ss-serv-sms",			ss_serv_sms_json),
	Entry("ss-serv-smtp",			ss_serv_smtp_json),
	Entry("ss-serv-svm",			ss_serv_svm_json),
	Entry("ss-serv-time",			ss_serv_time_json),
	Entry("sc-linux",			sc_linux_json),
	Entry("ss-linux",			ss_linux_json),
	Entry("ss-linux-iface-addips",		ss_linux_iface_addips_json),
	Entry("ss-linux-iface-checker",		ss_linux_iface_checker_json),
	Entry("ss-linux-iface-config",		ss_linux_iface_config_json),
	Entry("ss-linux-iface-dhcp-s",		ss_linux_iface_dhcp_s_json),
	Entry("ss-linux-iface-dyndns",		ss_linux_iface_dyndns_json),
	Entry("ss-linux-iface-iptfwd",		ss_linux_iface_iptfwd_json),
	Entry("ss-linux-iface-macs",		ss_linux_iface_macs_json),
	Entry("ss-linux-iface-routing",		ss_linux_iface_routing_json),
	Entry("ss-linux-iface-simple",		ss_linux_iface_simple_json),
	Entry("ss-linux-iface-status",		ss_linux_iface_status_json),
	Entry("ss-linux-eth-base",		ss_linux_eth_base_json),
	Entry("ss-linux-eth-sled",		ss_linux_eth_sled_json),
	Entry("ss-linux-vlan",			ss_linux_vlan_json),
	Entry("ss-linux-cell-sled",		ss_linux_cell_sled_json),
	Entry("ss-linux-wifi-sled",		ss_linux_wifi_sled_json),
	Entry("ss-linux-tun-gre",		ss_linux_tun_gre_json),
	Entry("ss-linux-tun-ipsec",		ss_linux_tun_ipsec_json),
	Entry("ss-linux-tun-l2tp",		ss_linux_tun_l2tp_json),
	Entry("ss-linux-tun-open",		ss_linux_tun_open_json),
	Entry("ss-linux-tun-otap",		ss_linux_tun_otap_json),
	Entry("ss-linux-serv-dhcp-r",		ss_linux_serv_dhcp_r_json),
	Entry("ss-linux-serv-dhcp-s",		ss_linux_serv_dhcp_s_json),
	Entry("ss-linux-serv-geo",		ss_linux_serv_geo_json),
	Entry("ss-linux-serv-io",		ss_linux_serv_io_json),
	Entry("ss-linux-serv-snmp",		ss_linux_serv_snmp_json),
	Entry("ss-linux-serv-syslog",		ss_linux_serv_syslog_json),
	Entry("ss-linux-serv-tls-c",		ss_linux_serv_tls_c_json),
	Entry("ss-linux-serv-tls-s",		ss_linux_serv_tls_s_json),
	Entry("ss-linux-fw-custom",		ss_linux_fw_custom_json),
	Entry("ss-linux-fw-filters",		ss_linux_fw_filters_json),
	Entry("ss-linux-fw-forward",		ss_linux_fw_forward_json),
	Entry("ss-linux-fw-inbound",		ss_linux_fw_inbound_json),
	Entry("ss-linux-fw-lists",		ss_linux_fw_lists_json),
	Entry("ss-linux-fw-nat",		ss_linux_fw_nat_json),
	Entry("sc-rlos",			sc_rlos_json),
	Entry("ss-rlos",			ss_rlos_json),
	Entry("ss-rlos-ethernet",		ss_rlos_ethernet_json),
	Entry("ss-rlos-ext-modem",		ss_rlos_ext_modem_json),
	Entry("ss-rlos-hspa-modem",		ss_rlos_hspa_modem_json),
};

//////////////////////////////////////////////////////////////////////////
//
// Schema Files
//

// Constructors

CSchemaFiles::CSchemaFiles(void)
{
	MakeIndex();
}

// File Lookup

BOOL CSchemaFiles::FindFile(CSchemaFileData const * &pFile, PCTXT pName)
{
	INDEX n = m_Index.FindName(pName);

	if( !m_Index.Failed(n) ) {

		pFile = m_Files + m_Index.GetData(n);

		return TRUE;
	}

	return FALSE;
}

// Implementation

BOOL CSchemaFiles::MakeIndex(void)
{
	for( UINT n = 0; n < elements(m_Files); n++ ) {

		m_Index.Insert(m_Files[n].m_pName, n);
	}

	return TRUE;
}

// End of File
