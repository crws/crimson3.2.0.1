
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosSchemaGenerator_HPP

#define INCLUDE_RlosSchemaGenerator_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SchemaGenerator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphite Schema Generator
//

class CRlosSchemaGenerator : public CSchemaGenerator
{
public:
	// Constructors
	CRlosSchemaGenerator(void);
	CRlosSchemaGenerator(IConfigStorage *pConfig, IPxeModel *pModel);

	// Destructor
	~CRlosSchemaGenerator(void);

protected:
	// Data Members
	IPxeModel * m_pModel;

	// Overridables
	bool MakeHardwareSchema(void);
	bool MakePersonalitySchema(void);
	bool MakeSystemSchema(void);
	bool MakeUserSchema(void);
	bool AdjustDefault(char cTag, CJsonData *pData);

	// Implementation
	void        ScanForModules(CJsonData *pList, CJsonData *pHard, UINT uType, UINT uFrom, PCTXT pFragment);
	CJsonData * AddInterface(CJsonData *pList, UINT uIndex, PCTXT pFragment);
	void	    AddServices(CJsonData *pSchema);
};

// End of File

#endif
