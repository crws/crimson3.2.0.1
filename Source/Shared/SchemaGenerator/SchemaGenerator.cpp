
#include "Intern.hpp"

#include "SchemaGenerator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SchemaFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Schema Generator
//

// Constructor

CSchemaGenerator::CSchemaGenerator(void)
{
	m_pFiles  = New CSchemaFiles;

	m_pConfig = NULL;

	StdSetRef();
}

// Destructor

CSchemaGenerator::~CSchemaGenerator(void)
{
	delete m_pFiles;

	AfxRelease(m_pConfig);
}

// IUnknown

HRESULT CSchemaGenerator::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IConfigUpdate);

	StdQueryInterface(IConfigUpdate);

	StdQueryInterface(ISchemaGenerator);

	return E_NOINTERFACE;
}

ULONG CSchemaGenerator::AddRef(void)
{
	StdAddRef();
}

ULONG CSchemaGenerator::Release(void)
{
	StdRelease();
}

// IConfigUpdate

void CSchemaGenerator::OnConfigUpdate(char cTag)
{
	if( cTag == 'h' ) {

		MakeSystemSchema();
	}

	if( cTag == 'u' ) {

		CheckUserConfig();
	}
}

// ISchemaGenerator

bool CSchemaGenerator::Open(void)
{
	#if defined(AEON_ENVIRONMENT)

	if( !m_pConfig ) {

		AfxGetObject("c3.config-storage", 0, IConfigStorage, m_pConfig);

		CheckUserConfig();

		m_pConfig->AddUpdateSink(this);

		return MakeSchemas();
	}

	return true;

	#else

	CheckUserConfig();

	m_pConfig->AddUpdateSink(this);

	return MakeSchemas();

	#endif
}

bool CSchemaGenerator::GetDefault(char cTag, CString &Text)
{
	switch( cTag ) {

		case 'h':
			Text = FormatDefault(cTag, GetFileText(T("hc-default")));
			return true;

		case 'p':
			Text = FormatDefault(cTag, GetFileText(T("pc-default")));
			return true;

		case 's':
			Text = FormatDefault(cTag, GetFileText(m_Default));
			return true;

		case 'u':
			Text = FormatDefault(cTag, GetFileText(T("uc-default")));
			return true;
	}

	return false;
}

bool CSchemaGenerator::GetSchema(char cTag, CString &Text)
{
	switch( cTag ) {

		case 'h':
			Text = m_HSchema;
			return true;

		case 'p':
			Text = m_PSchema;
			return true;

		case 's':
			Text = m_SSchema;
			return true;

		case 'u':
			Text = m_USchema;
			return true;
	}

	return false;
}

bool CSchemaGenerator::GetNaming(char cTag, CString &Text)
{
	switch( cTag ) {

		case 'h':
			Text = "hardware";
			return true;

		case 'p':
			Text = "personality";
			return true;

		case 's':
			Text = "system";
			return true;

		case 'u':
			Text = "users";
			return true;
	}

	return false;
}

// Implementation

bool CSchemaGenerator::MakeSchemas(void)
{
	MakeHardwareSchema();

	MakePersonalitySchema();

	MakeSystemSchema();

	MakeUserSchema();

	return true;
}

void CSchemaGenerator::CheckUserConfig(void)
{
	CString Config;

	m_pConfig->GetConfig('u', Config);

	CAutoPointer<CJsonData> pJson(new CJsonData);

	if( pJson->Parse(Config) ) {

		CJsonData *pList = pJson->GetChild("set.users");

		BOOL       fFind = FALSE;

		for( UINT n = 0; n < pList->GetCount(); n++ ) {

			CJsonData *pUser = pList->GetChild(n);

			if( pUser->GetValue("access", "") == "0" ) {

				fFind = TRUE;

				break;
			}
		}

		if( !fFind ) {

			for( UINT n = 0; n < pList->GetCount(); n++ ) {

				CJsonData *pUser = pList->GetChild(n);

				if( pUser->GetValue("user", "") == "Admin" ) {

					CPrintf Name("User%u", 1+n);

					pUser->AddValue("user", Name);
				}
			}

			CJsonData *pAdmin;

			pList->AddChild(FALSE, pAdmin);

			CString pass = "password";

			#if defined(AEON_ENVIRONMENT)

			AfxGetAutoObject(pSec, "dev.security", 0, IFeatures);

			if( pSec ) {

				CSecureDeviceInfo Info;

				if( pSec->GetDeviceInfo(Info) ) {

					pass = PCSTR(Info.m_bDefPass);
				}
			}

			#endif

			pAdmin->AddValue("user", "Admin");

			pAdmin->AddValue("name", "Administrator");

			pAdmin->AddValue("pass", pass);

			pAdmin->AddValue("access", "0", jsonString);

			pAdmin->AddValue("ftp", "0", jsonString);

			Config = pJson->GetAsText(FALSE);

			m_pConfig->SetConfig('u', Config, true);
		}
	}
}

bool CSchemaGenerator::CheckConfig(char cTag)
{
	CString Config;

	CString Schema;

	m_pConfig->GetConfig(cTag, Config);

	GetSchema(cTag, Schema);

	CAutoPointer<CJsonData> pConfig(New CJsonData);

	CAutoPointer<CJsonData> pSchema(New CJsonData);

	if( pConfig->Parse(Config) && pSchema->Parse(Schema) ) {

		if( CheckConfig(pConfig, pSchema, true) ) {

			pConfig->AddValue("atype", CPrintf("%c", cTag));

			CString Output = pConfig->GetAsText(FALSE);

			if( Output.CompareC(Config) ) {

				#if defined(AEON_ENVIRONMENT)

				bool fEdit = m_pConfig->IsEdited(cTag);

				#else

				bool fEdit = true;

				#endif

				return m_pConfig->SetConfig(cTag, Output, fEdit);
			}

			return true;
		}
	}

	return false;
}

bool CSchemaGenerator::CheckConfig(CJsonData *pConfig, CJsonData *pSchema, bool fTop)
{
	CTree<CString> Seen;

	if( fTop ) {

		Seen.Insert("atype");
	}

	if( pSchema->HasName("children") ) {

		CJsonData *pChildren = pSchema->GetChild("children");

		for( INDEX i = pChildren->GetHead(); !pChildren->Failed(i); pChildren->GetNext(i) ) {

			CJsonData *pChild = pChildren->GetChild(i);

			CString    Name   = pChild->GetValue("name", "");

			if( pConfig->HasName(Name) ) {

				if( pConfig->GetType(Name) == jsonObject ) {

					CheckConfig(pConfig->GetChild(Name), pChild, false);

					Seen.Insert(Name);
				}
			}
			else {
				CJsonData *pSub;

				pConfig->AddChild(Name, FALSE, pSub);

				Seen.Insert(Name);
			}
		}
	}

	if( pSchema->HasName("tabs") ) {

		CJsonData *pTabs = pSchema->GetChild("tabs");

		for( INDEX t = pTabs->GetHead(); !pTabs->Failed(t); pTabs->GetNext(t) ) {

			CJsonData *pTab = pTabs->GetChild(t);

			if( !pTab->HasName("name") ) {

				CheckTab(Seen, pConfig, pTab);
			}
			else {
				CString Name = pTab->GetValue("name", "");

				if( pConfig->HasName(Name) ) {

					if( pConfig->GetType(Name) == jsonObject ) {

						Seen.Insert(Name);

						CJsonData *pChild = pConfig->GetChild(Name);

						if( TRUE ) {

							CTree<CString> Seen;

							CheckTab(Seen, pChild, pTab);

							CleanConfig(Seen, pChild);
						}
					}
				}
				else {
					CJsonData *pSub;

					pConfig->AddChild(Name, FALSE, pSub);

					Seen.Insert(Name);
				}
			}
		}
	}

	CleanConfig(Seen, pConfig);

	return true;
}

bool CSchemaGenerator::CheckTab(CTree<CString> &Seen, CJsonData *pConfig, CJsonData *pTab)
{
	if( pTab->HasName("sections") ) {

		CJsonData *pSections = pTab->GetChild("sections");

		for( INDEX s = pSections->GetHead(); !pSections->Failed(s); pSections->GetNext(s) ) {

			CJsonData *pSection = pSections->GetChild(s);

			if( pSection->HasName("fields") ) {

				CJsonData *pFields = pSection->GetChild("fields");

				for( INDEX f = pFields->GetHead(); !pFields->Failed(f); pFields->GetNext(f) ) {

					CJsonData *pField = pFields->GetChild(f);

					Seen.Insert(pField->GetValue("name", ""));
				}
			}

			if( pSection->HasName("table") ) {

				CJsonData *pTable = pSection->GetChild("table");

				CString    Name   = pTable->GetValue("name", "");

				if( pConfig->HasName(Name) ) {

					if( pConfig->GetType(Name) == jsonObject ) {

						// TODO -- Remove any bad fields???

						CJsonData *pSub = pConfig->GetChild(Name);

						UINT uLimit = tatoi(pTable->GetValue("rows", "9999"));

						UINT uCount = pSub->GetCount();

						for( UINT r = uLimit; r < uCount; r++ ) {

							pSub->Delete(CPrintf("%4.4X", r));
						}

						Seen.Insert(Name);
					}
				}
			}
		}

		return true;
	}

	return false;
}

bool CSchemaGenerator::CleanConfig(CTree<CString> const &Seen, CJsonData *pConfig)
{
	CStringArray Kill;

	for( INDEX i = pConfig->GetHead(); !pConfig->Failed(i); pConfig->GetNext(i) ) {

		CString Name = pConfig->GetName(i);

		if( Seen.Failed(Seen.Find(Name)) ) {

			Kill.Append(Name);
		}
	}

	for( UINT k = 0; k < Kill.GetCount(); k++ ) {

		pConfig->Delete(Kill[k]);
	}

	return true;
}

bool CSchemaGenerator::Substitute(CJsonData *pJson, CString Find, CString Subst)
{
	for( INDEX i = pJson->GetHead(); !pJson->Failed(i); pJson->GetNext(i) ) {

		if( pJson->GetType(i) == jsonObject ) {

			Substitute(pJson->GetChild(i), Find, Subst);
		}

		if( pJson->GetType(i) == jsonString ) {

			CString Data = pJson->GetValue(i);

			Data.Replace(Find, Subst);

			pJson->SetValue(i, Data);
		}
	}

	return true;
}

void CSchemaGenerator::AddDisplaySize(CJsonData *pJson, CArray<DWORD> const &List)
{
	CJsonData *pBase = pJson->GetChild("children.0000.tabs.0000.sections.0001.fields.0000");

	CString Enum;

	for( UINT n = 0; n < List.GetCount(); n++ ) {

		DWORD r = List[n];

		if( !Enum.IsEmpty() ) {

			Enum += '|';
		}

		if( !r ) {

			Enum += CPrintf(T("0=Not Available"));

			pBase->AddValue(T("enable"), T("0"));
		}
		else {
			int x = LOWORD(r);

			int y = HIWORD(r);

			Enum += CPrintf(T("%u=%u by %u"), r, x, y);
		}

	}

	pBase->AddValue(T("format"), Enum);
}

void CSchemaGenerator::AddPermittedGroups(CJsonData *pJson, UINT uMask)
{
	CJsonData *pBase = pJson->GetChild("children.0000.tabs.0000.sections.0000.fields.0000");

	CString Enum;

	pBase->AddValue(T("enable"), T("0"), jsonString);

	for( UINT n = 0; n < 6; n++ ) {

		if( uMask & (1 << n) ) {

			if( !Enum.IsEmpty() ) {

				pBase->AddValue(T("enable"), T("1"), jsonString);

				Enum += '|';
			}

			switch( n ) {

				case 0: Enum += T("0=Group 1"); break;
				case 1: Enum += T("1=Group 2"); break;
				case 2: Enum += T("2=Group 3"); break;
				case 3: Enum += T("3=Group 3"); break;
				case 4: Enum += T("4=Group 3"); break;
				case 5: Enum += T("5=Group 4"); break;
			}
		}
	}

	pBase->AddValue(T("format"), Enum);
}

CString CSchemaGenerator::GetEnumCount(UINT n)
{
	CString e = T("None");

	for( UINT i = 0; i < n; i++ ) {

		switch( i ) {

			case 0: e += T("|One");   break;
			case 1: e += T("|Two");   break;
			case 2: e += T("|Three"); break;
			case 3: e += T("|Four");  break;
			case 4: e += T("|Five");  break;
			case 5: e += T("|Six");   break;
			case 6: e += T("|Seven"); break;
			case 7: e += T("|Eight"); break;
			case 8: e += T("|Nine");  break;
		}
	}

	return e;
}

CString CSchemaGenerator::GetHardwareConfig(void)
{
	CString Text;

	m_pConfig->GetConfig('h', Text);

	return Text;
}

CString CSchemaGenerator::GetFileText(PCTXT pName)
{
	CSchemaFileData const *pData;

	if( m_pFiles->FindFile(pData, pName) ) {

		PCSTR pText = PCSTR(pData->m_pData);

		UINT  uSize = pData->m_uSize;

		return CString(pText, uSize);
	}

	return T("");
}

CJsonData * CSchemaGenerator::GetFileJson(PCTXT pName)
{
	CAutoPointer<CJsonData> pJson(new CJsonData);

	if( pJson->Parse(GetFileText(pName)) ) {

		return pJson.TakeOver();
	}

	return NULL;
}

CString CSchemaGenerator::FormatDefault(char cTag, CString const &Text)
{
	CJsonData Data;

	if( Data.Parse(Text) ) {

		AdjustDefault(cTag, &Data);

		return Data.GetAsText(FALSE);
	}

	return Text;
}

void CSchemaGenerator::AddFragment(CJsonData *pSchema, PCTXT pWhere, PCTXT pName)
{
	AddFragment(pSchema->GetChild(pWhere), pName);
}

void CSchemaGenerator::AddFragment(CJsonData *pList, PCTXT pName)
{
	CJsonData *pFrag = NULL;

	pList->AddChild(FALSE, pFrag);

	pFrag->Parse(GetFileText(pName));
}

// End of File
