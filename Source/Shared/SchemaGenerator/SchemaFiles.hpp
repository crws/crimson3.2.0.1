
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_SchemaFiles_HPP

#define INCLUDE_SchemaFiles_HPP

//////////////////////////////////////////////////////////////////////////
//
// Comparison
//

#if !defined(AEON_ENVIRONMENT)

inline int AfxCompare(PCTXT const &a, PCTXT const &b)
{
	return wcsicmp(a, b);
}

#endif

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

struct CSchemaFileData
{
	PCTXT  m_pName;
	PCBYTE m_pData;
	UINT   m_uSize;
};

//////////////////////////////////////////////////////////////////////////
//
// Schema Files
//

class CSchemaFiles
{
public:
	// Constructors
	CSchemaFiles(void);

	// File Lookup
	BOOL FindFile(CSchemaFileData const * &pFile, PCTXT pName);

protected:
	// File Table
	static CSchemaFileData const m_Files[];

	// File Index
	CMap<PCTXT, UINT> m_Index;

	// Implementation
	BOOL MakeIndex(void);
};

// End of File

#endif
