
#include "intern.hpp"

#include "lecom2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Lenze LECOM Type II Master Serial Driver
//

// Instantiator

INSTANTIATE(CLecom2MasterDriver);

// Constructor

CLecom2MasterDriver::CLecom2MasterDriver(void)
{
	m_Ident = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	pHex = Hex;
	}

// Destructor

CLecom2MasterDriver::~CLecom2MasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CLecom2MasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	UINT uAddr = m_pCtx->m_Ping * 100;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = LOWORD(uAddr);
	Addr.a.m_Extra	= HIWORD(uAddr);	
	Addr.a.m_Type   = addrLongAsLong;

	return Read(Addr, Data, 1); 

	}

// Configuration

void MCALL CLecom2MasterDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CLecom2MasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CLecom2MasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CLecom2MasterDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CLecom2MasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop      = GetByte(pData);

			m_pCtx->m_Ping	     = GetWord(pData);	
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;

	}

CCODE MCALL CLecom2MasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CLecom2MasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	if ( !(m_pCtx->m_bDrop % 10) ) {

		return uCount;
		}

	LONG uAddr = MAKELONG(Addr.a.m_Offset, Addr.a.m_Extra);

	UINT uCode = uAddr / 100;

	UINT uSubCode = uAddr % 100;

	UINT uTag = Addr.a.m_Table;

	PutRead( uCode, uSubCode );

	if( GetFrame() ) { 

		switch( uTag ) {
	
			case TYPE_FIXED_4:
			
				*pData = GetData(7, 'C' - '@');
				break;

			case TYPE_INT:
			
				*pData = GetData(7, 'I' - '@');
				break;

			case TYPE_HEX:
			
				*pData = GetData(7, 'H' - '@');
				break;
			
			default:
				*pData = 0;
				break;
			}

		return uCount;
		}

	return CCODE_ERROR;

	}

CCODE MCALL CLecom2MasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	PutWrite( Addr, pData );

	if ( !(m_pCtx->m_bDrop % 10) ) {

		return uCount;
		}
	
	if( GetFrame() ) {
	
		return uCount;	
		}

	return CCODE_ERROR;

	}

// Implementation

void CLecom2MasterDriver::PutRead(UINT uCode, UINT uSubCode)
{
	StartFrame();
		
	AddByte( EOT );

	AddByte( pHex[m_pCtx->m_bDrop / 10] );
	AddByte( pHex[m_pCtx->m_bDrop % 10] );

	AddByte( '!' );

	PutGeneric( uCode, 16, 4096 );

	PutGeneric( uSubCode, 16, 16 );
	
	AddByte( ENQ );

	Send();		
	}

void CLecom2MasterDriver::PutWrite(AREF Addr, PDWORD pData)
{
	StartFrame();
		
	AddByte( EOT );

	AddByte( pHex[m_pCtx->m_bDrop/10] );
	AddByte( pHex[m_pCtx->m_bDrop%10] );

	AddByte( STX );

	m_bCheck = 0;
	
	AddByte( '!' );

	LONG uAddr = MAKELONG(Addr.a.m_Offset, Addr.a.m_Extra);

	UINT uCode = uAddr / 100;

	UINT uSubCode = uAddr % 100;

	PutGeneric( uCode, 16, 4096 );
	
	PutGeneric( uSubCode, 16, 16 );

	UINT uTag = Addr.a.m_Table;

	PutData( *pData, uTag );

	AddByte( ETX );

	AddByte( m_bCheck );

	Send();		
	}

void CLecom2MasterDriver::StartFrame(void)
{
	m_uPtr   = 0;

	m_bCheck = 0;
	}
	
void CLecom2MasterDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {
	
		m_bTx[ m_uPtr++ ] = bData;
	
		m_bCheck ^= bData;
		}
	}
	
void CLecom2MasterDriver::Send(void)
{
	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

BOOL CLecom2MasterDriver::GetFrame(void)
{
	UINT uState = 0;
	
	m_uPtr = 0;
	
	UINT uTimer = 0;

	UINT uData = 0;
       	
	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uData = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uData == ACK ) {
					
					return TRUE;
					}
				if( uData == NAK ) {
					
					return FALSE;
					}
				if( uData == STX ) {
				
					m_bCheck = 0;
					m_uPtr   = 0;
					uState   = 1;
					}
				break;
				
			case 1:					
				if( uData == '?' ) {
					
					return FALSE;
					}
				if( uData == EOT ) {
					
					return FALSE;
					}
				m_bCheck ^= uData;
					
				m_bRx[ m_uPtr ++ ] = uData;
					
				if( m_uPtr == sizeof(m_bRx) ) {
					
					return FALSE;
					}
				
				if( uData == ETX ) 
					uState = 2;
				break;

			case 2:
				if( uData == m_bCheck ) {
					
					return TRUE;
					}
				
				return FALSE;

				break;
			}
		}

	return FALSE;
	}

DWORD CLecom2MasterDriver::GetData(UINT uOffset, WORD wSpace)
{
	DWORD dData = 0;
	
	UINT uDPlaces = 0;
	
	if(m_bRx[uOffset] == 'H') {
	
		UINT uDataLen = GetDataLength(uOffset + 1);
	
		dData = GetGeneric(16, uDataLen, uOffset + 1, uDPlaces);
		}
	else {

		UINT uDataLen = GetDataLength(uOffset);
	
		dData = GetGeneric(10, uDataLen, uOffset, uDPlaces);

		switch( wSpace + '@' ) {
		
			case 'C':
				
				dData *= PowerOf( 10, (4 - uDPlaces) );

				break;
	
			case 'I':
			case 'H':
				break;

			}
		}
	
	return dData;
	}

WORD CLecom2MasterDriver::GetDataLength(WORD wOffset)
{
	BYTE *pData = m_bRx + wOffset;

	WORD wCount = 0;
	
	while(*pData++ != ETX) {

		wCount++;
		}

	return wCount;
	}

DWORD CLecom2MasterDriver::GetGeneric(UINT uRadix, UINT uLength, UINT uOffset, UINT &uDPlaces)
{
	BYTE *pData = m_bRx + uOffset;
	
	DWORD dData = 0;
	
	BOOL fNeg = FALSE;
	
	BOOL fDP = FALSE;
	
	uDPlaces = 0;
	
	while( uLength-- ) {
	
		char cData = (char) *(pData++);
		
		if( cData == '-' )
			fNeg = TRUE;
		
		if( fDP )
			uDPlaces++;
		
		if( cData == '.' )
			fDP = TRUE;
		
		if( cData >= '0' && cData <= '9' )
			dData = dData * uRadix + cData - '0';

		if( cData >= 'a' && cData <= 'f' )
			dData = dData * uRadix + cData - 'a' + 10;

		if( cData >= 'A' && cData <= 'F' )
			dData = dData * uRadix + cData - 'A' + 10;
		
		}
	
	return fNeg ? (-1 * dData) : dData;

	}

void CLecom2MasterDriver::PutData(DWORD dData, UINT uTag)
{
	DWORD dValue = 0;

	if ( uTag != TYPE_HEX ) {

		if ( Neg(dData) ) {

			AddByte( '-' );
			}

		dValue = Abs(dData);
		}
	
	else {
		dValue = dData;
		}

	switch( uTag ) {
		
		case TYPE_FIXED_4:

			PutValue( dValue / 10000 );

			AddByte( '.' );

			PutGeneric( dValue % 10000, 10, 1000 );

			break;

		case TYPE_INT:
			
			PutValue( dValue );
			
			break;

		case TYPE_HEX:
			
			AddByte( 'H' );

			PutHexadecimal( dValue, 16, 4096 );
			
			break;
		}
	}

DWORD CLecom2MasterDriver::PowerOf(UINT uBase, UINT uPower)
{
	DWORD dValue = 1;
	
	while(uPower--) {

		dValue *= uBase;
		}
	
	return dValue;
	}

void CLecom2MasterDriver::PutGeneric(DWORD dData, UINT uRadix, UINT uFactor)
{
	while( uFactor ) {
	
		BYTE bData = pHex[(dData / uFactor) % uRadix];

		AddByte(bData);
	
		uFactor /= uRadix;
								
		}
	}

	
void CLecom2MasterDriver::PutHexadecimal(DWORD dData, UINT uRadix, UINT uFactor)
{
	UINT Value = HIWORD(dData);

	UINT Factor = uFactor;
	
	while( Factor ) {
	
		BYTE bData = pHex[(Value / Factor) % uRadix];

		AddByte(bData);
		
		Factor /= uRadix;
				
		}

	Value = LOWORD(dData);

	Factor = uFactor;

	while( Factor ) {
	
		BYTE bData = pHex[(Value / Factor) % uRadix];

		AddByte(bData);
		
		Factor /= uRadix;
				
		}
	}

void CLecom2MasterDriver::PutValue(DWORD dValue)
{
	LONG d = 1000000000;

	while ( d ) {

		BYTE b = dValue / d;

		AddByte(b + '0');
				
		dValue -= ( d * b );
		
		d /= 10;
		}
	}

DWORD CLecom2MasterDriver::Abs(DWORD dData)
{
	return ( dData > 0x7FFFFFFF ) ? (-1 * dData) : dData;
	}

BOOL CLecom2MasterDriver::Neg(DWORD dData)
{
	return ( dData > 0x7FFFFFFF ) ? TRUE : FALSE;
	}

// End of File
