
#include "intern.hpp"

#include "../ModDevS/MbServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Exception Codes
//

#define	ILLEGAL_FUNCTION	0x01

#define ILLEGAL_ADDRESS		0x02

#define	ILLEGAL_DATA		0x03

/////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CModbusGatewayHandler;

//////////////////////////////////////////////////////////////////////////
//
// Modbus Device Gateway Serial Slave Driver - Multi Device 
//
//

class CModbusDeviceServerSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CModbusDeviceServerSerialDriver(void);

		// Destructor
		~CModbusDeviceServerSerialDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE)Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)Write(AREF Addr, PDWORD pData, UINT uCount);

		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);
		DEFMETH(UINT) DevCtrl(void * pContext, UINT uFunc, PCTXT Value);

	protected:
		// Context
		struct CContext
		{
			BYTE            m_bDrop;
			BOOL	        m_fFlipLong;
			BOOL	        m_fFlipReal;
			BOOL	        m_fEnable;
			CModbusServer * m_pServer;
			CContext      * m_pNext;
			CContext      * m_pPrev;
			};

		// Data Members
		CContext *	m_pCtx;
		BYTE		m_bDrop;
		BOOL		m_fFlipLong;
		BOOL		m_fFlipReal; 
		CContext      * m_pHead;
		CContext      * m_pTail;
		CContext      * m_pThis;

		// Handler
		CModbusGatewayHandler * m_pHandler;

		// Friends
		friend class CModbusGatewayHandler;
	};

/////////////////////////////////////////////////////////////////////////
//
// Modbus Device Gateway Port Handler - Multi Device
//

class CModbusGatewayHandler : public IPortHandler
{
	public:
		// Constructor
		CModbusGatewayHandler(IHelper *pHelper, CModbusDeviceServerSerialDriver *pDriver);

		// Destructor
		~CModbusGatewayHandler(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Binding
		void MCALL Bind(IPortObject *pPort);

		// Event Handlers
		void MCALL OnOpen(CSerialConfig const &Config);
		void MCALL OnClose(void);
		void MCALL OnTimer(void);
		BOOL MCALL OnTxData(BYTE &bData);
		void MCALL OnTxDone(void);
		void MCALL OnRxData(BYTE bData);
		void MCALL OnRxDone(void);		

	protected:
		// Driver
		CModbusDeviceServerSerialDriver * m_pDriver;

		// Help
		IHelper          * m_pHelper;
		IExtraHelper	 * m_pExtra;

		// Port
		IPortObject      * m_pPort;
		BOOL               m_fOpen;	       		
		
		// Members
		ULONG           m_uRefs;
		LPCTXT		m_pHex;
		UINT		m_uTxSize;
		UINT		m_uRxSize;
		PBYTE     	m_pTx;
		PBYTE     	m_pRx;
		UINT		m_RxPtr;
		UINT		m_Gap;
		UINT		m_TxPtr;
		UINT		m_TxCount;
		PDWORD		m_pWork;
		CRC16		m_CRC;

		// Device Context
		CModbusDeviceServerSerialDriver::CContext * m_pThis;

		// Slave Handling
		UINT SlaveRead (AREF Addr, PDWORD pData, UINT uCount);
		UINT SlaveWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Implementation
		BOOL DoProcess(void);
		BOOL DoListen(void);
		void Tx(void);
		BOOL HandleFrame(void);

		// Frame Handlers		
		BOOL HandleRead(UINT uTable);
		BOOL HandleBitRead(UINT uTable);
		BOOL HandleMultiWrite(UINT uTable);
		BOOL HandleMultiBitWrite(UINT uTable);
		BOOL HandleSingleWrite(UINT uTable);
		BOOL HandleSingleBitWrite(UINT uTable);
		BOOL HandleLoopBack(void);
		BOOL HandleReadWrite(void);
		BOOL TakeException(BYTE bCode);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);

		// Response Helper
		void UnpackBits(PDWORD pWork, UINT uCount);

		// Helpers
		BOOL TxDelay(void);
		void Make16BitSigned(DWORD &dwData);
		BOOL IsLongReg(UINT uType);
		BOOL IsReal(UINT uType);
		void AllocBuffers(void);
		void FreeBuffers(void);
	};

// End of File

