
#include "intern.hpp"

#include "moddevss.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Source
//

#include "../ModDevS/MbServer.cpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Device Gateway Serial Slave Driver - Multi Device 
//

// Instantiator

INSTANTIATE(CModbusDeviceServerSerialDriver);

// Constructor
 
CModbusDeviceServerSerialDriver::CModbusDeviceServerSerialDriver(void)
{
	m_Ident    = DRIVER_ID;

	m_pHead    = NULL;

	m_pTail    = NULL;
       	}

// Destructor

CModbusDeviceServerSerialDriver::~CModbusDeviceServerSerialDriver(void)
{
	}

// Config

void MCALL CModbusDeviceServerSerialDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bDrop     = GetByte(pData);

		m_fFlipLong = GetByte(pData);

		m_fFlipReal = GetByte(pData);

		return;
		}
	}

void MCALL CModbusDeviceServerSerialDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}   
	
	Make485(Config, FALSE);
	}

void MCALL CModbusDeviceServerSerialDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CModbusGatewayHandler(m_pHelper, this);

	pPort->Bind(m_pHandler);
	}

void MCALL CModbusDeviceServerSerialDriver::Detach(void)
{
	m_pHandler->Release();
	}

void MCALL CModbusDeviceServerSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CModbusDeviceServerSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop     = GetByte(pData);

			if( GetByte(pData) ) {

				m_pCtx->m_fFlipLong = GetByte(pData);

				m_pCtx->m_fFlipReal = GetByte(pData);
				}
			else {
				m_pCtx->m_fFlipLong = m_fFlipLong;

				m_pCtx->m_fFlipReal = m_fFlipReal;
				}

			m_pCtx->m_fEnable = TRUE;

			CModbusServer *pServer = New CModbusServer();

			#if defined(_DEBUG)

			pServer->BindDriver(this);

			#endif

			m_pCtx->m_pServer = pServer;

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}
      
	return CCODE_SUCCESS;
		
	}

CCODE MCALL CModbusDeviceServerSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		delete m_pCtx->m_pServer;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CModbusDeviceServerSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pCtx->m_pServer->MasterRead(Addr, pData, uCount);
	}

CCODE MCALL CModbusDeviceServerSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pCtx->m_pServer->MasterWrite(Addr, pData, uCount);
	}

// User Access

UINT MCALL CModbusDeviceServerSerialDriver::DrvCtrl(UINT uFunc, PCTXT Value)
{
	return 0;
	}

UINT MCALL CModbusDeviceServerSerialDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CContext * pThis = (CContext *) pContext;

	if( uFunc == 1 ) {

		for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

			if( pCtx->m_bDrop == pThis->m_bDrop) {
				
				return (pCtx->m_fEnable = !stricmp(Value, "on"));
				}
			}

		return 0;
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Device Gateway Port Handler - Multi Device
//

// Constructor

CModbusGatewayHandler::CModbusGatewayHandler(IHelper *pHelper, CModbusDeviceServerSerialDriver * pDriver)
{
	StdSetRef();

	m_pDriver     = pDriver;
	
	m_pHelper     = pHelper;

	m_pPort       = NULL;

	m_fOpen       = FALSE;

	m_RxPtr	      = 0;

	m_TxPtr	      = 0;

	m_TxCount     = 0;

	m_pExtra      = NULL;

	m_uTxSize     = 512;
	
	m_uRxSize     = 512;

	m_pWork       = NULL;

	m_pTx         = NULL;

	m_pRx         = NULL;

	CTEXT Hex[]   = "0123456789ABCDEF";

	m_pHex	      = Hex;
	
	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

// Destructor

CModbusGatewayHandler::~CModbusGatewayHandler(void)
{
	FreeBuffers();

	m_pExtra->Release();
	}

// IUnknown

HRESULT CModbusGatewayHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG CModbusGatewayHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG CModbusGatewayHandler::Release(void)
{
	StdRelease();
	}

// Binding

void MCALL CModbusGatewayHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;

	AllocBuffers();
	}

// Event Handlers

void MCALL CModbusGatewayHandler::OnOpen(CSerialConfig const &Config)
{
	m_fOpen      = TRUE;
	}

void MCALL CModbusGatewayHandler::OnClose(void)
{
	m_fOpen = FALSE;
	}

void MCALL CModbusGatewayHandler::OnTimer(void)
{
	m_Gap++;
	
	if( m_Gap >= 5 ) {

		if( m_RxPtr >= 4 ) {

			m_CRC.Preset();
			
			PBYTE p = m_pRx;
				
			UINT  n = m_RxPtr - 2;
			
			for( UINT i = 0; i < n; i++ ) {

				m_CRC.Add(*(p++));
				}

			WORD c1 = IntelToHost(PU2(p)[0]);
				
			WORD c2 = m_CRC.GetValue();
					
			if( c1 == c2 ) {

				if( DoProcess() ) {

					HandleFrame();
					}

				m_RxPtr = 0;

				m_Gap   = 0;
				}
			}

		m_RxPtr = 0;

		m_Gap   = 0;
		}
	}

BOOL MCALL CModbusGatewayHandler::OnTxData(BYTE &bData)
{
	if( m_TxPtr < m_TxCount ) {
		
		bData = m_pTx[m_TxPtr++];

		return TRUE;
		}
	
	return FALSE;
	}

void MCALL CModbusGatewayHandler::OnTxDone(void)
{
	}

void MCALL CModbusGatewayHandler::OnRxData(BYTE bData)
{	
	m_pRx[m_RxPtr++] = bData;

	if( m_RxPtr <= m_uRxSize ) {

		m_Gap = 0;
		}
	}

void MCALL CModbusGatewayHandler::OnRxDone(void)
{
	}

// Slave Handling

UINT CModbusGatewayHandler::SlaveRead(AREF Addr, PDWORD pData, UINT uCount)
{
	m_pPort->EnableInterrupts(FALSE);

	UINT c =  m_pThis->m_pServer->SlaveRead(Addr, pData, uCount);

	m_pPort->EnableInterrupts(TRUE);

	return c;
	}

UINT CModbusGatewayHandler::SlaveWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	m_pPort->EnableInterrupts(FALSE);

	UINT c =  m_pThis->m_pServer->SlaveWrite(Addr, pData, uCount);

	m_pPort->EnableInterrupts(TRUE);

	return c;
	}

// Implementation

BOOL CModbusGatewayHandler::DoProcess(void)
{	
	return TRUE;
	}

BOOL CModbusGatewayHandler::DoListen(void)
{
	return FALSE;
	}

void CModbusGatewayHandler::Tx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_TxPtr; i++ ) {
	
		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));
	
	m_TxCount = m_TxPtr;

	m_TxPtr = 1;

	//TxDelay();
	
	m_pPort->Send(m_pTx[0]);
	}

BOOL CModbusGatewayHandler::HandleFrame(void)
{
	CModbusDeviceServerSerialDriver::CContext *pCtx = m_pDriver->m_pHead;

	for( ; pCtx; pCtx = pCtx->m_pNext ) {
			
		if( pCtx->m_bDrop == m_pRx[0] ) {

			m_pThis = pCtx;
				
			if( m_pThis->m_fEnable ) {

				switch( m_pRx[1] ) {

					case 0x01:	return HandleBitRead(3);

					case 0x02:	return HandleBitRead(4);

					case 0x03:	return HandleRead(2);
					
					case 0x04:	return HandleRead(1);
					
					case 0x05:	return HandleSingleBitWrite(3);
					
					case 0x06:	return HandleSingleWrite(2);

					case 0x08:	return HandleLoopBack();
					
					case 0x0F:	return HandleMultiBitWrite(3);
					
					case 0x10:	return HandleMultiWrite(2);

					case 0x17:	return HandleReadWrite();

					default:	return TakeException(ILLEGAL_FUNCTION);
					}
				}

			break;
			}
		}

	return FALSE;
       	}

// Frame Handlers

BOOL CModbusGatewayHandler::HandleRead(UINT uTable)
{
	UINT uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	UINT uCount = MotorToHost(PWORD(m_pRx + 4)[0]);

	if( uCount > 125 || uAddr + uCount > 0xFFFF ) {

		TakeException(ILLEGAL_ADDRESS);

		return FALSE;
		}
	else {
		memset(m_pWork, 0, uCount * sizeof(DWORD));

		UINT   uType[] = { 
			
			addrWordAsWord, 
			addrWordAsLong,
			addrWordAsReal, 
			addrLongAsLong, 
			addrLongAsReal 
			
			};

		UINT   uFact[] = { 1, 2, 2, 1, 1 };

		UINT   uTotal  = 0;

		for( UINT t = 0; t < elements(uType); t++ ) {

			if( !(uCount % uFact[t]) ) {

				CAddress Addr;

				Addr.a.m_Table  = uTable;
				Addr.a.m_Extra  = 0;
				Addr.a.m_Offset = 1 + uAddr; 
				Addr.a.m_Type   = uType[t];

				if( IsLongReg(uType[t]) ) {

					Addr.a.m_Table += 4;
					}

				uTotal = uCount / uFact[t];

				if( !SlaveRead(Addr, m_pWork, uTotal) ) {

					if( t == 0 ) {

						Addr.a.m_Extra = 1;

						if( !SlaveRead(Addr, m_pWork, uTotal) ) {

							continue;
							}
						}
					else
						continue;
					}
				
				break;
				}
			}

		if( t < elements(uType) ) {

			StartFrame(m_pRx[1]);

			UINT uBytes = ((IsLongReg(uType[t])) ? 4 : 2);

			AddByte(uBytes * uCount);
		
			BOOL fReal = IsReal(uType[t]);

			BOOL fFlip = fReal ? m_pThis->m_fFlipReal : m_pThis->m_fFlipLong;

			for( UINT n = 0; n < uTotal; n++ ) {

				if( t == 0 ) {

					AddWord(m_pWork[n]);
					}
				else {
					if( fFlip ) {
					       
						AddWord(LOWORD(m_pWork[n]));
						AddWord(HIWORD(m_pWork[n]));
						}
					else {
						AddWord(HIWORD(m_pWork[n]));
						AddWord(LOWORD(m_pWork[n]));
						}
					}
				}

			Tx();

			return TRUE;
			}

		return FALSE;
		} 
	}

BOOL CModbusGatewayHandler::HandleBitRead(UINT uTable)
{
	UINT uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	UINT uCount = MotorToHost(PWORD(m_pRx + 4)[0]);

	if( uCount > 125 || uAddr + uCount > 0xFFFF ) {

		TakeException(ILLEGAL_ADDRESS);

		return FALSE;
		}
	else {
		memset(m_pWork, 0, uCount * sizeof(DWORD));

		CAddress Addr;

		Addr.a.m_Table  = uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Offset = 1 + uAddr;
		Addr.a.m_Type   = addrBitAsBit;

		if( SlaveRead(Addr, m_pWork, uCount) ) {

			StartFrame(m_pRx[1]);

			AddByte( (uCount+7)/8 );

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( m_pWork[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b);
				}
		
			Tx();

			return TRUE;
			}

		return FALSE;
		}
	}

BOOL CModbusGatewayHandler::HandleMultiWrite(UINT uTable)
{
	UINT  uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	UINT  uCount = MotorToHost(PWORD(m_pRx + 4)[0]);

	PU2   pWrite = PU2(m_pRx + 7);

	if( uCount > 125 || uAddr + uCount >  0xFFFF ) {

		TakeException(ILLEGAL_ADDRESS);

		return FALSE;
		}
	else {
		UINT   uType[] = { addrWordAsWord, addrWordAsLong, addrWordAsReal, addrLongAsLong, addrLongAsReal };

		UINT   uFact[] = { 1, 2, 2, 1, 1 };

		UINT   uTotal  = 0;

		BOOL   fSign   = FALSE;

		for( UINT t = 0; t < elements(uType); t++ ) {

			if( !(uCount % uFact[t]) ) {

				CAddress Addr;

				Addr.a.m_Table  = uTable;
				Addr.a.m_Extra  = 0;
				Addr.a.m_Offset = 1 + uAddr; 
				Addr.a.m_Type   = uType[t];

				if( IsLongReg(uType[t]) ) {

					Addr.a.m_Table += 4;
					}

				uTotal = uCount / uFact[t];

				if( !SlaveWrite(Addr, NULL, uTotal) ) {

					if( t == 0 ) {

						Addr.a.m_Extra = 1;

						if( !SlaveWrite(Addr, NULL, uTotal) ) {

							continue;
							}

						fSign = TRUE;
						}
					else
						continue;
					}

				BOOL fReal = IsReal(uType[t]);

				BOOL fFlip = fReal ? m_pThis->m_fFlipReal : m_pThis->m_fFlipLong;

				for( UINT n = 0; n < uTotal; n++ ) {

					if( t == 0 ) {

						DWORD dwData = MotorToHost(pWrite[n]);

						if( fSign ) {

							Make16BitSigned(dwData);
							}

						m_pWork[n] = dwData;
						}
					else {
						WORD hi  = MotorToHost(pWrite[2*n+0]);
						
						WORD lo  = MotorToHost(pWrite[2*n+1]);

						if( fFlip )
							m_pWork[n] = MAKELONG(hi, lo);
						else
							m_pWork[n] = MAKELONG(lo, hi);
						}
					}

				SlaveWrite(Addr, m_pWork, uTotal);
					
				break;
				}
			}

		if( t < elements(uType) ) {

			StartFrame(m_pRx[1]);

			AddWord(uAddr);

			AddWord(uCount);
					
			Tx();

			return TRUE;
			}

		return FALSE;
		}
	}

BOOL CModbusGatewayHandler::HandleMultiBitWrite(UINT uTable)
{
	UINT uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	UINT uCount = MotorToHost(PWORD(m_pRx + 4)[0]);

	if( uCount > 125 || uAddr + uCount >  0xFFFF ) {

		TakeException(ILLEGAL_ADDRESS);

		return FALSE;
		}
	else {
		CAddress Addr;

		Addr.a.m_Table  = uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Offset = 1 + uAddr;
		Addr.a.m_Type   = addrBitAsBit;

		UnpackBits( m_pWork, uCount );
		
		if( SlaveWrite(Addr, m_pWork, uCount) ) {

			StartFrame(m_pRx[1]);

			AddWord(uAddr);

			AddWord(uCount);

			Tx();

			return TRUE;
			}

		return FALSE;
		}
	}

BOOL CModbusGatewayHandler::HandleSingleWrite(UINT uTable)
{	
	UINT  uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	DWORD dwData = MotorToHost(PWORD(m_pRx + 4)[0]);

	CAddress Addr;

	Addr.a.m_Table  = uTable;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Offset = 1 + uAddr;
	Addr.a.m_Type   = addrWordAsWord;

	if( !SlaveWrite(Addr, &dwData, 1) ) {

		Addr.a.m_Extra = 1;

		Make16BitSigned(dwData);

		if( !SlaveWrite(Addr, &dwData, 1) ) {

			return FALSE;
			}
		}

	StartFrame(m_pRx[1]);

	AddWord(uAddr);

	AddWord(dwData);

	Tx();

	return TRUE;
	}

BOOL CModbusGatewayHandler::HandleSingleBitWrite(UINT uTable)
{	
	UINT  uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	DWORD dwData = MotorToHost(PWORD(m_pRx + 4)[0]);

	CAddress Addr;

	Addr.a.m_Table  = uTable;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Offset = 1 + uAddr;
	Addr.a.m_Type   = addrBitAsBit;

	if( LOWORD(dwData) == 0xFF00 ) {

		dwData = 1;
		}
	
	else if( LOWORD(dwData) != 0 ) {

		TakeException(ILLEGAL_DATA);

		return FALSE;
		}

	if( SlaveWrite(Addr, &dwData, 1) ) {

		StartFrame(m_pRx[1]);

		AddWord(uAddr);

		AddWord(dwData ? 0xFF00 : 0x0);

		Tx();

		return TRUE;
		}

	return FALSE;
	}

BOOL CModbusGatewayHandler::HandleLoopBack(void)
{
	if( m_pRx[2] || m_pRx[3] ) {

		return TakeException(ILLEGAL_FUNCTION);
		}

	memcpy(m_pTx, m_pRx, 6);

	m_TxPtr = 6;

	Tx();

	return TRUE;
	}

BOOL CModbusGatewayHandler::HandleReadWrite(void)
{
	if( TRUE ) {

		UINT  uAddr  = MotorToHost(PWORD(m_pRx + 6)[0]);

		UINT  uCount = MotorToHost(PWORD(m_pRx + 8)[0]);

		PU2   pWrite = PU2(m_pRx + 11);

		if( uCount > 125 || uAddr + uCount > 0xFFFF ) {

			TakeException(ILLEGAL_ADDRESS);

			return FALSE;
			}
		else {
			UINT   uType[] = { addrWordAsWord, addrWordAsLong, addrWordAsReal };

			UINT   uFact[] = { 1, 2, 2 };

			UINT   uTotal  = 0;

			BOOL   fSign   = FALSE;

			for( UINT t = 0; t < elements(uType); t++ ) {

				if( !(uCount % uFact[t]) ) {

					CAddress Addr;

					Addr.a.m_Table  = 2;
					Addr.a.m_Extra  = 0;
					Addr.a.m_Offset = 1 + uAddr; 
					Addr.a.m_Type   = uType[t];

					uTotal = uCount / uFact[t];

					if( !SlaveWrite(Addr, NULL, uCount) ) {

						if( uFact[t] == 1 ) {

							Addr.a.m_Extra = 1;

							if( !SlaveWrite(Addr, NULL, uCount) ) {

								continue;
								}

							fSign = TRUE;
							}
						else
							continue;
						}

					BOOL fReal = (uType[t] == addrWordAsReal);

					BOOL fFlip = fReal ? m_pThis->m_fFlipReal : m_pThis->m_fFlipLong;

					for( UINT n = 0; n < uTotal; n++ ) {

						if( uFact[t] == 1 ) {

							DWORD dwData = MotorToHost(pWrite[n]);

							if( fSign ) {

								Make16BitSigned(dwData);
								}

							m_pWork[n] = dwData;
							}
						else {
							WORD hi  = MotorToHost(pWrite[2*n+0]);
							
							WORD lo  = MotorToHost(pWrite[2*n+1]);

							if( fFlip )
								m_pWork[n] = MAKELONG(hi, lo);
							else
								m_pWork[n] = MAKELONG(lo, hi);
							}
						}

					SlaveWrite(Addr, m_pWork, uCount);
						
					break;
					}
				}
			}
		}

	if( TRUE ) {
			
		UINT uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

		UINT uCount = MotorToHost(PWORD(m_pRx + 4)[0]);

		if( uCount > 125 || uAddr + uCount > 0xFFFF ) {

			TakeException(ILLEGAL_ADDRESS);

			return FALSE;
			}
		else {
			memset(m_pWork, 0, uCount * sizeof(DWORD));

			UINT   uType[] = { addrWordAsWord, addrWordAsLong, addrWordAsReal };

			UINT   uFact[] = { 1, 2, 2 };

			UINT   uTotal  = 0;

			for( UINT t = 0; t < elements(uType); t++ ) {

				if( !(uCount % uFact[t]) ) {

					CAddress Addr;

					Addr.a.m_Table  = 2;
					Addr.a.m_Extra  = 0;
					Addr.a.m_Offset = 1 + uAddr; 
					Addr.a.m_Type   = uType[t];

					uTotal = uCount / uFact[t];

					if( SlaveRead(Addr, m_pWork, uTotal) ) {

						break;
						}
					}
				}

			if( t < elements(uType) ) {

				StartFrame(m_pRx[1]);

				AddByte(2 * uCount);

				BOOL fReal = (uType[t] == addrWordAsReal);

				BOOL fFlip = fReal ? m_pThis->m_fFlipReal : m_pThis->m_fFlipLong;

				for( UINT n = 0; n < uTotal; n++ ) {

					if( uFact[t] == 1 ) {

						AddWord(m_pWork[n]);
						}
					else {
						if( fFlip ) {
						       
							AddWord(LOWORD(m_pWork[n]));
							AddWord(HIWORD(m_pWork[n]));
							}
						else {
							AddWord(HIWORD(m_pWork[n]));
							AddWord(LOWORD(m_pWork[n]));
							}
						}
					}
				
				Tx();

				return TRUE;
				}

			return FALSE;
			}
		}
	}

BOOL CModbusGatewayHandler::TakeException(BYTE bCode)
{
	StartFrame(m_pRx[1] | 0x80);

	AddByte(bCode);

	Tx();

	return FALSE;
	}

// Frame Building

void CModbusGatewayHandler::StartFrame(BYTE bOpcode)
{
	m_TxPtr = 0;
	
	AddByte(m_pRx[0]);
	
	AddByte(bOpcode);
	}

void CModbusGatewayHandler::AddByte(BYTE bData)
{
	if( m_TxPtr < m_uTxSize ) {
	
		m_pTx[m_TxPtr] = bData;
		
		m_TxPtr++;
		}
	}

void CModbusGatewayHandler::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CModbusGatewayHandler::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Response Helper

void CModbusGatewayHandler::UnpackBits(PDWORD pWork, UINT uCount)
{
	UINT b = 7;

	BYTE m = 1;

	for( UINT n = 0; n < uCount; n++ ) {

		pWork[n] = ( m_pRx[b] & m ) ? TRUE : FALSE;

		if( !(m <<= 1) ) {

			b++;

			m = 1;
			}
		}
	}
		
// Helpers

BOOL CModbusGatewayHandler::TxDelay(void)
{
	UINT uCount = 1000;

	while( uCount -= 1 ) GetTickCount();

	return TRUE;
	}

void CModbusGatewayHandler::Make16BitSigned(DWORD &dwData)
{
	if( dwData & 0x8000 ) {

		dwData |= 0xFFFF0000;
		}
	}

BOOL CModbusGatewayHandler::IsLongReg(UINT uType)
{
	switch( uType ) {

		case addrLongAsLong:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CModbusGatewayHandler::IsReal(UINT uType)
{
	switch( uType ) {

		case addrWordAsReal:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

void CModbusGatewayHandler::AllocBuffers(void)
{
	UINT uAlloc = 125;

	m_pWork = new DWORD [ 125 ];

	memset(m_pWork, 0, uAlloc * sizeof(DWORD));

	m_pTx   = new BYTE [ m_uTxSize ];

	memset(m_pTx, 0, m_uTxSize);

	m_pRx   = new BYTE [ m_uRxSize ];

	memset(m_pRx, 0, m_uRxSize);
	}

void CModbusGatewayHandler::FreeBuffers(void)
{
	if( m_pWork ) {

		delete m_pWork;
		
		m_pWork = NULL;
		}

	if( m_pTx ) {

		delete m_pTx;
		
		m_pTx = NULL;
		}

	if( m_pRx ) {

		delete m_pRx;
		
		m_pRx = NULL;
		}
	}

// End of File

