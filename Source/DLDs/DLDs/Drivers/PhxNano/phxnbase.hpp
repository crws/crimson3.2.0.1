
#ifndef PHOENIXNANOBASE
#define	PHOENIXNANOBASE
//////////////////////////////////////////////////////////////////////////
//
// Phoenix NanoLC Data Spaces
//
#define	SPACE_R		 1
#define	SPACE_A		 2
#define	SPACE_Q		 3
#define	SPACE_I		 4
#define	SPACE_F		 5
#define	SPACE_O		 6
#define	SPACE_TCA	 7
#define	SPACE_TCP	 8
#define	SPACE_OTA	 9
#define	SPACE_OTP	10
#define	SPACE_HSA	11
#define	SPACE_HSP	12
#define	SPACE_MOD	20

// Address Offsets
#define	OFFFLAG		0x1000
#define	OFFAOUT		0x1000
#define	OFFTCP		0x2000
#define	OFFOTP		0x3000
#define	OFFHSP		0x4000
#define	OFFTCA		0x5000
#define	OFFOTA		0x6000
#define	OFFHSA		0x7000

// Op Codes
#define	READOUT		 1
#define	READINP		 2
#define	READREG		 3
#define	READANI		 4
#define	WRITE1O		 5
#define	WRITE1R		 6
#define	WRITEnO		15
#define	WRITEnR		16

#define	BUFFSIZE	300

//////////////////////////////////////////////////////////////////////////
//
// Phoenix NanoLC Driver Base Functions
//

class CPhoenixNanoBase : public CMasterDriver
{
	public:
		// Constructor
		CPhoenixNanoBase(void);

		// Destructor
		~CPhoenixNanoBase(void);
		
		// Entry Points
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Data Members
		CRC16	m_CRC;

		PBYTE	m_pTx;
		PBYTE	m_pRx;

		UINT	m_uPtr;
		UINT	m_uWriteError;

		BYTE	m_bDrop;
		BOOL	m_fTCP;
		
		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddCRC(void);
		void	EndFrame(void);
		
		// Read Handlers
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoBitRead (AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoBitWrite (AREF Addr, PDWORD pData, UINT uCount);

		// Transact
		BOOL	Send(BOOL fIgnore);

		// Helpers
		DWORD	SwapLong(DWORD dData);

		// Frame Header
		virtual	void AddFrameHeader(BYTE bOpcode);

		// Transport Layer
		virtual	BOOL Transact(UINT uSize, BOOL fIgnore);

		// Get Device Info
		virtual	void GetDeviceInfo(PBYTE pDrop, BOOL *pfTCP, PBYTE *pTx, PBYTE *pRx);
	};

// End of File

#endif
