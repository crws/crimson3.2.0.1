
#include "intern.hpp"

#include "phxnano.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Phoenix NanoLC Driver
//

// Instantiator

INSTANTIATE(CPhoenixNanoLCDriver);

// Constructor

CPhoenixNanoLCDriver::CPhoenixNanoLCDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_pTx       = m_bTx;

	m_pRx       = m_bRx;

	m_uTimeout  = 600;
	}

// Destructor

CPhoenixNanoLCDriver::~CPhoenixNanoLCDriver(void)
{
	}

// Configuration

void MCALL CPhoenixNanoLCDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CPhoenixNanoLCDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CPhoenixNanoLCDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CPhoenixNanoLCDriver::Open(void)
{
	}

// Device

CCODE MCALL CPhoenixNanoLCDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop	= GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CPhoenixNanoLCDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CPhoenixNanoLCDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPACE_R;
		
	Addr.a.m_Offset = 0;
		
	Addr.a.m_Type   = addrLongAsLong;

	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

// Port Access

UINT CPhoenixNanoLCDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Transport Layer

BOOL CPhoenixNanoLCDriver::Transact(UINT uSize, BOOL fIgnore)
{
	if( PutFrame(uSize) ) {
	
		if( !m_pCtx->m_bDrop ) { 

			while( m_pData->Read(0) < NOTHING );

			return TRUE;
			}
			
		if( GetFrame() ) {

			if( m_pRx[0] == m_pTx[0] ) {

				if( fIgnore ) {

					return TRUE;
					}

				if( m_pRx[1] & 0x80 ) {
		
					return FALSE;
					}

				return TRUE;
				}
			}
		}
		
	return FALSE;
	}

BOOL CPhoenixNanoLCDriver::PutFrame(UINT uSize)
{
	m_pData->ClearRx();
	
	return BinaryTx(uSize);
	}

BOOL CPhoenixNanoLCDriver::GetFrame(void)
{
	return BinaryRx();
	}

BOOL CPhoenixNanoLCDriver::BinaryTx(UINT uSize)
{
//**/	AfxTrace0("\r\n"); for(UINT i = 0; i < uSize; i++) AfxTrace1("[%2.2x]", m_pTx[i]);

	m_pData->Write(m_pTx, uSize, FOREVER);

	return TRUE;
	}

BOOL CPhoenixNanoLCDriver::BinaryRx(void)
{
	UINT uByte = 0;

	UINT uSize = NOTHING;

	UINT uPtr  = 0;

	UINT uGap  = 0;

	UINT uEnd  = FindEndTime();

	SetTimer(m_uTimeout);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (uByte = RxByte(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

//**/			AfxTrace1("<%2.2x>", uByte);

			if( uPtr == sizeof(m_bRx) ) return FALSE;

			uGap = 0;
			}
		else
			uGap = uGap + 1;

		if( uPtr >= uSize || uGap >= uEnd ) {

			if( uPtr >= 4 ) {

				m_CRC.Preset();
				
				PBYTE p = m_pRx;
				
				UINT  n = uPtr - 2;
			
				for( UINT i = 0; i < n; i++ ) {

					m_CRC.Add(*(p++));
					}

				WORD c1 = IntelToHost(PU2(p)[0]);
				
				WORD c2 = m_CRC.GetValue();
					
				if( c1 == c2 ) {

					return TRUE;
					}
				}

			uSize = NOTHING;
				
			uPtr  = 0;
			
			uGap  = 0;
			}
		}

	return FALSE;
	}

// Frame Header
void CPhoenixNanoLCDriver::AddFrameHeader(BYTE bOpcode)
{
	AddByte(m_bDrop);

	AddByte(bOpcode);
	}

// Transport Helpers

UINT CPhoenixNanoLCDriver::FindEndTime(void)
{
	return ToTicks(25);
	}

// Device Information

void CPhoenixNanoLCDriver::GetDeviceInfo(PBYTE pbDrop, BOOL * pfTCP, PBYTE *pTx, PBYTE *pRx)
{
	*pbDrop = m_pCtx->m_bDrop;

	*pfTCP  = FALSE;

	*pTx     = m_bTx;

	*pRx     = m_bRx;
	}

// End of File
