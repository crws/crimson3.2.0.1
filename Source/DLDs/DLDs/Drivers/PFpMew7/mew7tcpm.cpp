
#include "mew7tcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 PLC MEWTOCOL7 TCP/IP Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CPanFpMewtocol7TcpMasterDriver);

// Constructor

CPanFpMewtocol7TcpMasterDriver::CPanFpMewtocol7TcpMasterDriver(void)
{
	m_Ident = DRIVER_ID;
	
	m_pCtx = NULL;

	m_uKeep = 0;	
	}

// Configuration

void MCALL CPanFpMewtocol7TcpMasterDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CPanFpMewtocol7TcpMasterDriver::Attach(IPortObject *pPort)
{

	}

void MCALL CPanFpMewtocol7TcpMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CPanFpMewtocol7TcpMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			MakeMap(pData);

			m_pBase->m_dwStation = 0x0EEE;
			
			InitErrorCode();

			InitErrorRequest();

			m_pCtx->m_IP	     = GetAddr(pData);
			m_pCtx->m_IP2	     = GetAddr(pData);
			m_pCtx->m_uPort	     = GetWord(pData);
			m_pCtx->m_fKeep	     = GetByte(pData);
			m_pCtx->m_fPing	     = GetByte(pData);
			m_pCtx->m_uTime1     = GetWord(pData);
			m_pCtx->m_uTime2     = GetWord(pData);
			m_pCtx->m_uTime3     = GetWord(pData);
			m_pCtx->m_wTrans     = 0;
			m_pCtx->m_pSock	     = NULL;
			m_pCtx->m_uLast	     = GetTickCount();
			m_pCtx->m_fDirty     = FALSE;
			m_pCtx->m_fAux       = FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;

	}

CCODE MCALL CPanFpMewtocol7TcpMasterDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		DeleteMap();

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CPanFpMewtocol7TcpMasterDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 || uFunc == 5 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		if( uFunc == 1  ) {

			pCtx->m_IP  = MotorToHost(dwValue);
			}

		if( uFunc == 5 ) {

			pCtx->m_IP2 = MotorToHost(dwValue);
			}

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort  = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 4 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP);
		}
	
	if( uFunc == 6 ) {
		
		// Get Current IP Address
		
		return MotorToHost(pCtx->m_IP2);
		}
		
	if( uFunc == 7 ) {
		
		//Get Fallback Status

		return pCtx->m_fAux;
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL CPanFpMewtocol7TcpMasterDriver::Ping(void)
{
	if( m_pCtx->m_fPing ) {
		
		DWORD IP;

		if( !m_pCtx->m_fAux ) {

			IP = m_pCtx->m_IP;
			}
		else
			IP = m_pCtx->m_IP2;
		
		if( CheckIP(IP, m_pCtx->m_uTime2) == NOTHING ) {
			
			if( m_pCtx->m_IP2 ) {

				m_pCtx->m_fAux = !m_pCtx->m_fAux;
				}

			return CCODE_ERROR; 
			}
		}

	if( OpenSocket() ) {

		return CPanFpMewtocol7BaseMasterDriver::Ping();
		}

	return CCODE_ERROR;
	}

// Socket Management

BOOL CPanFpMewtocol7TcpMasterDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CPanFpMewtocol7TcpMasterDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		m_pCtx->m_fAux   = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}
	
	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   Port;

		if( !m_pCtx->m_fAux ) {

			IP   = (IPADDR const &) m_pCtx->m_IP;

			Port = WORD(m_pCtx->m_uPort);
			}
		else {
			IP   = (IPADDR const &) m_pCtx->m_IP2;

			Port = WORD(m_pCtx->m_uPort);
			}
		
		if( m_pCtx->m_pSock->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			if( m_pCtx->m_IP2 ) {

				m_pCtx->m_fAux = !m_pCtx->m_fAux;
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		if( m_pCtx->m_IP2 ) {

			m_pCtx->m_fAux = !m_pCtx->m_fAux;
			}

		return FALSE;
		}

	return FALSE;
	}

void CPanFpMewtocol7TcpMasterDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Implementation

BOOL CPanFpMewtocol7TcpMasterDriver::SendFrame(void)
{
	UINT uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_pTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE; 
	}

BOOL CPanFpMewtocol7TcpMasterDriver::RecvFrame(void)
{
	UINT uPtr   = 0;

	UINT uSize  = 0;

	BOOL fBegin = FALSE;

	UINT uFrom  = 0;

	UINT uEnd   = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = m_uRxSize - uPtr;

		m_pCtx->m_pSock->Recv(m_pRx + uPtr, uSize);
		
		if( uSize ) {

			for( UINT u = uPtr; u < uPtr + uSize; u++ ) {

				if( !fBegin ) {

					if( m_pRx[u] == '>' ) {

						fBegin = TRUE;

						uFrom  = u;

						continue;
						}
					}

				if( m_pRx[u] == CR ) {

					uEnd = u;
					}
				}

			uPtr += uSize;

			if( uPtr >= m_uRxSize ) {

				return FALSE;
				}

			if( uEnd ) {

				memcpy(m_pRx, m_pRx + uFrom, uEnd - uFrom);

				return CheckFrame(uEnd - 4);
				}

			continue;
			}

		if( !CheckSocket() ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

// Overidables

BOOL CPanFpMewtocol7TcpMasterDriver::Transact(void)
{  
	if( OpenSocket() ) {

		if( SendFrame() ) {
		
			return RecvFrame();
			}

		CloseSocket(TRUE);
		}

	return FALSE;
	}

// Helpers

BOOL CPanFpMewtocol7TcpMasterDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CPanFpMewtocol7TcpMasterDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CPanFpMewtocol7TcpMasterDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
