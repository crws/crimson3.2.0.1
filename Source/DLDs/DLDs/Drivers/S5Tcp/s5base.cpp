
#include "intern.hpp"

#include "s5base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Master Base
//

// Constructor

CS5AS511Base::CS5AS511Base(void)
{
	}

// Destructor

CS5AS511Base::~CS5AS511Base(void)
{
	}

// Entry Points

CCODE MCALL CS5AS511Base::Ping(void)
{
	if( ReadSystem() ) {

		FindWidth();

		FreeCache(btDB);

		FreeCache(btDX);

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR; 
	}

CCODE MCALL CS5AS511Base::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !CheckLink() ) {

		return CCODE_ERROR;
		}

	UINT  uSpace  = Addr.a.m_Table & 0x0F;

	DWORD dwAddr  = FindAddress(Addr);

	UINT  uWidth  = GetWidth(uSpace);

	if( dwAddr < NOTHING ) {

		switch( Addr.a.m_Type ) {

			case addrBitAsByte:
			case addrByteAsByte:

				return DoByteRead(dwAddr, pData, uCount, uWidth);

			case addrBitAsWord:
			case addrByteAsWord:
			case addrWordAsWord:

				return DoWordRead(dwAddr, pData, uCount, uWidth);

			case addrBitAsLong:
			case addrWordAsLong:

				return DoLongRead(dwAddr, pData, uCount, uWidth);
			}
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE MCALL CS5AS511Base::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !CheckLink() ) {

		return CCODE_ERROR;
		}

	DWORD dwAddr = FindAddress(Addr);

	if( dwAddr < NOTHING ) {

		switch( Addr.a.m_Type ) {

			case addrBitAsByte:
			case addrByteAsByte:

				return DoByteWrite(dwAddr, pData, uCount);

			case addrBitAsWord:
			case addrByteAsWord:
			case addrWordAsWord:

				return DoWordWrite(dwAddr, pData, uCount);

			case addrBitAsLong:
			case addrWordAsLong:

				return DoLongWrite(dwAddr, pData, uCount);
			}
		}
	
	return CCODE_ERROR | CCODE_NO_RETRY;
	}

// Read/Write Handlers

CCODE CS5AS511Base::DoByteRead(DWORD dwAddr, PDWORD pData, UINT uCount, UINT uWidth)
{
	uCount = min(sizeof(m_bRxBuf), uCount);
	
	if( Read(dwAddr, uCount / uWidth) ) {

		uCount = (m_uRxPtr - m_pBase->m_wPad);

		CMotorDataPacker(pData, uCount, FALSE).Unpack(m_bRxBuf + m_pBase->m_wPad);
		
		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CS5AS511Base::DoWordRead(DWORD dwAddr, PDWORD pData, UINT uCount, UINT uWidth)
{
	uCount = min(sizeof(m_bRxBuf), uCount * sizeof(WORD));

	if( Read(dwAddr, uCount / uWidth) ) {

		uCount = (m_uRxPtr - m_pBase->m_wPad) / sizeof(WORD);

		CMotorDataPacker(pData, uCount, FALSE).Unpack(PWORD(m_bRxBuf + m_pBase->m_wPad));
		
		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CS5AS511Base::DoLongRead(DWORD dwAddr, PDWORD pData, UINT uCount, UINT uWidth)
{
	uCount = min(sizeof(m_bRxBuf), uCount * sizeof(DWORD));
	
	if( Read(dwAddr, uCount / uWidth) ) {

		uCount = (m_uRxPtr - m_pBase->m_wPad) / sizeof(DWORD);
		
		CMotorDataPacker(pData, uCount, FALSE).Unpack(PDWORD(m_bRxBuf + m_pBase->m_wPad));
		
		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CS5AS511Base::DoByteWrite(DWORD dwAddr, PDWORD pData, UINT uCount)
{
	uCount = min(sizeof(m_bPack) - 2, uCount);

	CMotorDataPacker(pData, uCount, FALSE).Pack(m_bPack);

	return Write(dwAddr, m_bPack, uCount) ? uCount : CCODE_ERROR;
	}

CCODE CS5AS511Base::DoWordWrite(DWORD dwAddr, PDWORD pData, UINT uCount)
{
	uCount = min(sizeof(m_bPack) / sizeof(WORD) - 2, uCount);
	
	CMotorDataPacker(pData, uCount, FALSE).Pack(PWORD(m_bPack));
	
	return Write(dwAddr, m_bPack, uCount * sizeof(WORD)) ? uCount : CCODE_ERROR;
	}

CCODE CS5AS511Base::DoLongWrite(DWORD dwAddr, PDWORD pData, UINT uCount)
{
	uCount = min(sizeof(m_bPack) / sizeof(DWORD) - 2, uCount);
	
	CMotorDataPacker(pData, uCount, FALSE).Pack(PDWORD(m_bPack));
	
	return Write(dwAddr, m_bPack, uCount * sizeof(DWORD)) ? uCount : CCODE_ERROR;
	}

// Address Cache

BOOL CS5AS511Base::IsCacheOk(UINT uType)
{
	return uType == btDB ? m_pBase->m_pAddrDB != NULL : m_pBase->m_pAddrDX != NULL;
	}

void CS5AS511Base::InitCache(UINT uType, UINT uCount)
{
	if( uType == btDB ) {

		FreeCache(btDB);

		m_pBase->m_pAddrDB = new CBlock [ uCount ];

		m_pBase->m_uSizeDB = uCount;

		memset(m_pBase->m_pAddrDB, 0, uCount * sizeof(CBlock));
		}

	if( uType == btDX ) {

		FreeCache(btDB);

		m_pBase->m_pAddrDX = new CBlock [ uCount ];

		m_pBase->m_uSizeDX = uCount;

		memset(m_pBase->m_pAddrDX, 0, uCount * sizeof(CBlock));
		}
	}

BOOL CS5AS511Base::FindCache(UINT uType, UINT uIndex, DWORD &dwAddr)
{
	CBlock *pCache = uType == btDB ? m_pBase->m_pAddrDB : m_pBase->m_pAddrDX;

	UINT    uCount = uType == btDB ? m_pBase->m_uSizeDB : m_pBase->m_uSizeDX;

	if( pCache ) {
	
		UINT i = uIndex % uCount;

		for( UINT uScan = 0; uScan < uCount; uScan ++ ) {

			CBlock &b = pCache[i];

			if( b.m_fUsed ) {
				
				if( b.m_wIdx == uIndex ) {

					dwAddr = b.m_dwAddr;

					if( m_pBase->m_fAddr32 ) {

						dwAddr <<= 4;
						}
						
					return TRUE;
					}

				i = (i + 1) % uCount;

				continue;
				}

			break;
			}
		}
	
	return FALSE;
	}

void CS5AS511Base::LoadCache(UINT uType)
{
	if( ReadDir(uType) ) {

		PWORD pAddr  = PWORD(m_bRxBuf);

		UINT  uCount = 0;

		for( UINT uPass = 0; uPass < 2; uPass ++ ) {

			if( uPass ) {

				InitCache(uType, uCount);
				}

			for( UINT uIdx = 0; uIdx < 256; uIdx ++ ) {

				WORD wAddr = MotorToHost(pAddr[uIdx]);

				if( wAddr ) {

					if( uPass ) {

						LoadCache(uType, uIdx, wAddr);
						}
					else {
						uCount ++;
						}
					}
				}
			}
		}
	}

void CS5AS511Base::LoadCache(UINT uType, UINT uIndex, DWORD dwAddr)
{
	CBlock *pCache = uType == btDB ? m_pBase->m_pAddrDB : m_pBase->m_pAddrDX;

	UINT    uCount = uType == btDB ? m_pBase->m_uSizeDB : m_pBase->m_uSizeDX;
	
	if( pCache ) {
	
		UINT i = uIndex % uCount;

		for( UINT uScan = 0; uScan < uCount; uScan ++ ) {

			CBlock &b = pCache[i];

			if( !b.m_fUsed ) {
				
				b.m_fUsed  = TRUE;
				
				b.m_wIdx   = uIndex;
				
				b.m_dwAddr = dwAddr;

				return;
				}

			i = (i + 1) % uCount;
			}
		}
	}

void CS5AS511Base::FreeCache(UINT uType)
{
	if( uType == btDB && m_pBase->m_pAddrDB ) {

		delete [] m_pBase->m_pAddrDB;

		m_pBase->m_pAddrDB = NULL;
		}

	if( uType == btDX && m_pBase->m_pAddrDX ) {

		delete [] m_pBase->m_pAddrDX;

		m_pBase->m_pAddrDX = NULL;
		}
	}

// Implementation

DWORD CS5AS511Base::FindAddress(AREF Addr)
{
	UINT uSpace  = Addr.a.m_Table & 0x0F;

	UINT uOffset = Addr.a.m_Offset;

	if( uSpace == 0x06 || uSpace == 0x07 ) {

		UINT uBlock = Addr.a.m_Extra | (Addr.a.m_Table & 0xF0);

		UINT uType  = uSpace == 0x06 ? btDB : btDX;
			
		if( m_pBase->m_fCache ) {

			if( !IsCacheOk(uType) ) {
 
				LoadCache(uType);
				}

			if( IsCacheOk(uType) ) {
			
				DWORD dwAddr;
			
				if( FindCache(uType, uBlock, dwAddr) ) {
					
					return dwAddr + (uOffset * sizeof(WORD) / GetWidth(uSpace));
					}
				}
			}

		DWORD dwAddr;
		
		WORD  wLen;

		if( BlockInfo(uType, uBlock, dwAddr, wLen) ) {
			
			return dwAddr + (uOffset * sizeof(WORD) / GetWidth(uSpace));
			}
		
		return NOTHING;
		}

	switch( uSpace ) {

		case 0x1: return m_pBase->m_dwAddrPIQ + uOffset / GetWidth(uSpace);

		case 0x2: return m_pBase->m_dwAddrPII + uOffset / GetWidth(uSpace);

		case 0x3: return m_pBase->m_dwAddrF + uOffset / (8 * GetWidth(uSpace));
		
		case 0x4: return m_pBase->m_dwAddrT + (uOffset * sizeof(WORD) / GetWidth(uSpace));

		case 0x5: return m_pBase->m_dwAddrC + (uOffset * sizeof(WORD) / GetWidth(uSpace));

		case 0x8: return m_pBase->m_dwAddrSys + (uOffset * sizeof(WORD) / GetWidth(uSpace));
		}

	return NOTHING;
	}

UINT CS5AS511Base::GetWidth(UINT uSpace)
{
	switch( uSpace ) {

		case 0x1: 
		case 0x2: 
		case 0x3: 
			return 1;

		default:
			return m_pBase->m_wWidth;
		}
	}

BOOL CS5AS511Base::FindWidth(void)
{
	StartFrame();

	if( m_pBase->m_fAddr32 ) {
	
		AddDataLong(m_pBase->m_dwAddrSys);

		AddDataLong(m_pBase->m_dwAddrSys);
		}
	else {
		AddDataWord(WORD(m_pBase->m_dwAddrSys));

		AddDataWord(WORD(m_pBase->m_dwAddrSys));
		}
	
	if( Transact(0x04, FALSE) ) {

		m_pBase->m_wWidth = WORD(m_uRxPtr - m_pBase->m_wPad);

		if( !m_pBase->m_wWidth ) {

			m_pBase->m_wWidth = 2;

			m_pBase->m_fExtra = TRUE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CS5AS511Base::Write(DWORD dwAddr, PBYTE pData, UINT uLen)
{
	StartFrame();

	if( m_pBase->m_fAddr32 ) {

		AddDataLong(dwAddr);

		AddDataLong(dwAddr + uLen - 1);
		}
	else {
		AddDataWord(WORD(dwAddr));
		}

	AddDataBuff(pData, uLen);
	
	return Transact(0x03, TRUE);
	}

BOOL CS5AS511Base::Read(DWORD dwAddr, UINT uLen)
{
	StartFrame();

	if( m_pBase->m_fAddr32 ) {

		AddDataLong(dwAddr);

		AddDataLong(dwAddr + uLen + (m_pBase->m_fExtra ? 0 : -1));
		}
	else {
		AddDataWord(WORD(dwAddr));

		AddDataWord(WORD(dwAddr + uLen - 1));
		}

	return Transact(0x04, FALSE);
	}

BOOL CS5AS511Base::ReadSystem(void)
{
	StartFrame();

	if( Transact(0x18, FALSE) && m_uRxPtr == 44 ) {
	
		PWORD p = PWORD(m_bRxBuf);

		m_pBase->m_dwAddrPII = MotorToHost(p[ 2]);
		m_pBase->m_dwAddrPIQ = MotorToHost(p[ 3]);
		m_pBase->m_dwAddrF   = MotorToHost(p[ 4]);
		m_pBase->m_dwAddrT   = MotorToHost(p[ 5]);
		m_pBase->m_dwAddrC   = MotorToHost(p[ 6]);
		m_pBase->m_dwAddrSys = MotorToHost(p[10]);
		m_pBase->m_wCPU1     = MotorToHost(p[19]);
		m_pBase->m_wCPU2     = MotorToHost(p[21]);

		if( m_pBase->m_fAddr32 ) {
			
			m_pBase->m_wPad        = 8;
			m_pBase->m_dwAddrPII <<= 4;
			m_pBase->m_dwAddrPIQ <<= 4;
			m_pBase->m_dwAddrF   <<= 4;
			m_pBase->m_dwAddrT   <<= 4;
			m_pBase->m_dwAddrC   <<= 4;
			m_pBase->m_dwAddrSys <<= 4;
			}
		else {
			m_pBase->m_wPad = 4;
			}

		/*
		AfxTrace1("m_dwAddrPII 0x%8.8X\n", m_pBase->m_dwAddrPII);
		AfxTrace1("m_dwAddrPIQ 0x%8.8X\n", m_pBase->m_dwAddrPIQ);
		AfxTrace1("m_dwAddrF   0x%8.8X\n", m_pBase->m_dwAddrF);;
		AfxTrace1("m_dwAddrT   0x%8.8X\n", m_pBase->m_dwAddrT);
		AfxTrace1("m_dwAddrC   0x%8.8X\n", m_pBase->m_dwAddrC);
		AfxTrace1("m_dwAddrSys 0x%8.8X\n", m_pBase->m_dwAddrSys);
		*/	

		return TRUE;
		}

	return FALSE;
	}

BOOL CS5AS511Base::ReadDir(UINT uType)
{
	StartFrame();

	AddDataByte(BYTE(uType));
	
	return Transact(0x1B, FALSE) && m_uRxPtr == 512;
	}

BOOL CS5AS511Base::BlockInfo(UINT uType, UINT uBlock, DWORD &dwAddr, WORD &wLen)
{
	StartFrame();

	AddDataByte(BYTE(uType));

	AddDataByte(BYTE(uBlock));
	
	if( Transact(0x1A, FALSE) && m_uRxPtr == 12 ) {

		PWORD p = PWORD(m_bRxBuf);

		dwAddr = MotorToHost(p[0]);

		wLen   = MotorToHost(p[5]);

		if( m_pBase->m_fAddr32 ) {

			dwAddr <<= 4;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CS5AS511Base::Transact(BYTE bFunc, BOOL fWrite)
{
	if( Start(bFunc) ) {

		AddCtrl(EOT);
		
		Send();

		if( fWrite ) {

			if( RecvCtrl(ACK) ) {

				Terminate();

				return TRUE;
				}
			}
		else {
			if( Continue() && Recv(TRUE) ) {

				SendCtrl(ACK);

				Terminate();

				return TRUE;
				}
			}
		}

	AbortLink();

	return FALSE;
	}

BOOL CS5AS511Base::Start(BYTE bFunc)
{
	WaitBusy();

	ClearRx();
	
	SendCtrl(STX);

	if( RecvCtrl(ACK) ) {

		SendData(bFunc);

		if( RecvCtrl(STX) ) {

			SendCtrl(ACK);

			if( RecvCtrl(SYN) && RecvCtrl(ETX) ) {
			
				SendCtrl(ACK);

				return TRUE;
				}
			
			Stop();
			}
		}

	return FALSE;
	}		

BOOL CS5AS511Base::Stop(void)
{
	if( RecvCtrl(STX) ) {

		SendCtrl(ACK);

		return Terminate();
		}
	
	return FALSE;
	}

BOOL CS5AS511Base::Continue(void)
{
	if( RecvCtrl(ACK) ) {
		
		if( RecvCtrl(STX) ) {
	
			SendCtrl(ACK);

			if( RecvCtrl(NUL) ) {

				return TRUE;
				}

			Stop();
			}
		}

	return FALSE;
	}

BOOL CS5AS511Base::Terminate(void)
{
	if( RecvCtrl(STX) ) {

		SendCtrl(ACK);

		if( RecvCtrl(DC2) && RecvCtrl(ETX) ) {

			SendCtrl(ACK);

			return TRUE;
			}
		}

	return FALSE;
	}

void CS5AS511Base::SetBusy(void)
{
	m_pBase->m_fBusy = TRUE;
	}

void CS5AS511Base::WaitBusy(void)
{
	if( m_pBase->m_fBusy ) {

		Sleep(QUIET);
		
		m_pBase->m_fBusy = FALSE;
		}
	}

// Transport

void CS5AS511Base::SendData(BYTE bData)
{
	if( bData == DLE ) {
		
		BYTE bBuf[] = { DLE, DLE };

		Send(bBuf, sizeof(bBuf));
		}
	else {
		Send(&bData, 1);
		}
	}

void CS5AS511Base::SendCtrl(BYTE bData)
{
	if( bData == STX ) {

		Send(&bData, 1);
		}
	else {
		BYTE bBuf[2]; 
		
		bBuf[0] = DLE;

		bBuf[1] = bData;

		Send(bBuf, sizeof(bBuf));
		}
	}

BOOL CS5AS511Base::RecvCtrl(BYTE bData)
{
	UINT uSym = RecvSym();
	
	switch( bData ) {

		case STX: return uSym == symStx;
		case ETX: return uSym == symEtx;
		case EOT: return uSym == symEot;
		case ACK: return uSym == symAck;
		case NAK: return uSym == symNak;

		default:
			return uSym == bData;
		}
	}

BOOL CS5AS511Base::Send(BYTE bData)
{
	return Send(&bData, 1);
	}

BOOL CS5AS511Base::Send(void)
{
	return Send(m_bTxBuf, m_uTxPtr);
	}

BOOL CS5AS511Base::Recv(BYTE &bData)
{
	UINT uLen = 1;
	
	return Recv(&bData, uLen, FALSE) && uLen == 1;
	}

BOOL CS5AS511Base::Recv(BOOL fTerm)
{
	m_uRxPtr = sizeof(m_bRxBuf);

	return Recv(m_bRxBuf, m_uRxPtr, TRUE);
	}

UINT CS5AS511Base::RecvSym(void)
{
	WORD wState = 0;

	for(;;) {

		BYTE bData;

		if( !Recv(bData) ) {

			SetBusy();
			
			return symNul;
			}
		
		switch( wState ) {
		
			case 0:
				switch( bData ) {
				
					case DLE:
						wState = 1;
						break;
						
					case STX:
						return symStx;
					
					default:
						return bData;
					}
				break;
				
			case 1:
				switch( bData ) {
				
					case DLE:
						return bData;
						
					case ETX:
						return symEtx;
						
					case EOT:
						return symEot;
					
					case ACK:
						return symAck;
					
					case NAK:
						SetBusy();

						return symNak;
					
					default:
						return symErr;
					}
				break;
			}
		}

	SetBusy();
	
	return symNul;
	}

// Frame Building

void CS5AS511Base::StartFrame(void)
{
	m_uTxPtr = 0;
	}

void CS5AS511Base::AddByte(BYTE bData)
{
	if( m_uTxPtr < sizeof(m_bTxBuf) ) {
	
		m_bTxBuf[m_uTxPtr] = bData;
		
		m_uTxPtr ++;
		}
	}

void CS5AS511Base::AddCtrl(BYTE bData)
{
	if( bData == STX ) {

		AddByte(STX);
		}
	else {
		AddByte(DLE);
		
		AddByte(bData);
		}
	}

void CS5AS511Base::AddDataByte(BYTE bData)
{
	if( bData == DLE ) {
		
		AddByte(DLE);

		AddByte(DLE);
		}
	else {
		AddByte(bData);
		}
	}

void CS5AS511Base::AddDataWord(WORD wData)
{
	AddDataByte(HIBYTE(wData));

	AddDataByte(LOBYTE(wData));
	}

void CS5AS511Base::AddDataLong(DWORD dwData)
{
	AddDataWord(HIWORD(dwData));

	AddDataWord(LOWORD(dwData));
	}

void CS5AS511Base::AddDataBuff(PBYTE pData, UINT uLen)
{
	while( uLen-- ) {
		
		AddDataByte(*pData++);
		}
	}

// Link Management
		
BOOL CS5AS511Base::CheckLink(void)
{
	return TRUE;
	}

void CS5AS511Base::AbortLink(void)
{
	}

// Port Access

void CS5AS511Base::ClearRx(void)
{
	}

BOOL CS5AS511Base::Recv(PBYTE pData, UINT &uLen, BOOL fTerm)
{
	return FALSE;
	}

BOOL CS5AS511Base::Send(PCBYTE pData, UINT uLen)
{
	return FALSE;
	}

// Base Context

void CS5AS511Base::FreeBase(void)
{
	FreeCache(btDB);

	FreeCache(btDX);

	m_pBase = NULL;
	}

void CS5AS511Base::InitBase(CBase *pCtx)
{
	pCtx->m_fCache  = FALSE;
	
	pCtx->m_fBusy   = FALSE;

	pCtx->m_wWidth  = 1;
	
	pCtx->m_fExtra  = FALSE;

	pCtx->m_pAddrDB = NULL;
	
	pCtx->m_pAddrDX = NULL;
	
	pCtx->m_uSizeDB = 0;
	
	pCtx->m_uSizeDX = 0;

	SetBase(pCtx);
	}

void CS5AS511Base::SetBase(CBase *pCtx)
{
	m_pBase = pCtx;
	}

// Debug 

void CS5AS511Base::Dump(PCBYTE pData, UINT uLen, BOOL fTx)
{
/*	if( fTx ) {

		AfxTrace0("---> ");
		}
	else {
		AfxTrace0("<--- ");
		}

	for( UINT i = 0; i < uLen; i ++ ) {

		if( i && !(i % 16) ) {

			AfxTrace0("\n<--- ");
			}

		AfxTrace1("%2.2X ", pData[i]);
		}

	AfxTrace0("\n");
*/	}

// End of File
