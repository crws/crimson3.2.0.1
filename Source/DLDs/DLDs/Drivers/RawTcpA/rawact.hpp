
//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Active Driver
//

class CRawTCPActiveDriver : public CRawPortDriver
{
	public:
		// Constructor
		CRawTCPActiveDriver(void);

		// Destructor
		~CRawTCPActiveDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(void) Disconnect(void);
		DEFMETH(UINT) Read (UINT uTime);
		DEFMETH(UINT) Write(BYTE bData, UINT uTime);
		DEFMETH(UINT) Write(PCBYTE pData, UINT uCount, UINT uTime);

	protected:
		// Data Members
		DWORD     m_IP;
		UINT	  m_uPort;
		UINT	  m_uTime;
		BOOL	  m_fKeep;
		ISocket * m_pSock;
		BOOL	  m_fDirty;

		// Implementation
		BOOL CheckSocket(BOOL fWait);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);

	};

// End of File
