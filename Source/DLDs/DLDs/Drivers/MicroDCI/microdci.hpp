
//////////////////////////////////////////////////////////////////////////
//
// Micromod Micro-DCI
//

// Space Definitions
#define	SP_A	('A')	// 10 Char Text
#define	SP_B	('B')	// Int read
#define	SP_C	('C')	// Lo Res Float
#define	SP_F	('F')	// 5 Char Text
#define	SP_H	('H')	// Hi Res Float
#define	SP_L	('L')	// Bit
#define	SP_Z	('Z')	// Direct Access to Bytes

// Type shorthand
#define	BB	addrBitAsBit
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal
#define	YL	addrByteAsLong
#define YW	addrByteAsWord
#define	YY	addrByteAsByte

// Datalink Transform Definitions
#define	TRANSFORM 0x8002	// Base Memory Selection

#define	TRANB	0x200
#define	TRANL	0x500
#define	TRANC	0x600
#define	TRANH	0xF00
#define	TRANAF	0x1400

#define	FORMB(x)	(TRANB  +  x)
#define	FORML(x)	(TRANL  + (x /  8))
#define	FORMC(x)	(TRANC  + (x *  3))
#define	FORMH(x)	(TRANH  + (x *  5))
#define	FORMAF(x)	(TRANAF + (x /  8 * 5))

// Operands
#define	INTER	0xE0
#define	CHNGE	0xA0
#define	CNGBT	0xC0
#define	ACKNO	0x80
#define	RESPO	0x20

#define	MSOH	0x7E
#define	STUFFIT	258	// address that controls stuffed byte handling on receive
#define	IS_A	0x7D
#define	IS_F	0x7E
#define	MASKA	0xFFFF0000
#define	MASKF	0xFF000000;

#define	BUFFSZA	40	// buffer size without 0 following MSOH
#define	BUFFSZB	72	// actual transmit buffer with 0's included

#define	DATCHKR	0	// No response data check for read
#define	DATCHKW	1	// Response data check byte-by-byte
#define	DATCHKB	2	// Response data check for bits

#define	ATXTEND	3
#define	ATXTMID	2
#define	FTXTEND	1
#define	AFSTART	0

class CMicrodciDriver : public CMasterDriver
{
	public:
		// Constructor
		CMicrodciDriver(void);

		// Destructor
		~CMicrodciDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BOOL	m_fHasStuffedByte;
			BOOL	m_fUseDirectAccess;
			UINT	m_uWriteErr;
			};

		struct WRITETEXT
		{
			PDWORD	pSrce;
			PBYTE	pList;
			BOOL	fIsA;
			UINT	uCount;
			UINT	uDPos;
			UINT	uRAllocPerStr;
			UINT	uCharPerStr;
			UINT	uRUsedPerStr;
			DWORD	dm_Ref;
			UINT	uAddr;
			PDWORD	pWrData;
			};

		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[BUFFSZA];
		BYTE	m_bRx[BUFFSZA];
		BYTE	m_bTR[BUFFSZB];
		BYTE	m_ReadStrings[30];
		BYTE	m_WriteStrings[30];
		UINT	m_uPtr;
		DWORD	m_dForceWrite;
		
		// Frame Building
		void	StartFrame(UINT uOffset, UINT uCount, BYTE bOp);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dData);

		// Opcode handling
		CCODE	DoBitRead  (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoByteRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoRealRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoTextRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoRead(UINT uOffset, PDWORD pData, UINT uCount, UINT uSize, BOOL fReal);
		BOOL	ReadBytes  (UINT uOffset, UINT uCount);
		CCODE	DoBitWrite (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoByteWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoRealWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoTextWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWrite(UINT uOffset, PDWORD pData, UINT uCount, UINT uSize, BOOL fReal);
		
		// Transport Layer
		BOOL	Transact(BOOL fChkWrite);
		void	Send(void);
		BOOL	GetReply(BOOL fChkWrite);
		BOOL	CheckReply(UINT uSize, BOOL fChkWrite);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		BOOL	IsTextSpace(UINT uTable);
		UINT	Transform(UINT uTable, UINT uOffset);
		void	SendAck(void);
		void	DoLRC(void);
		void	DCIRealToIEEE(UINT uItem, PDWORD pData, UINT uSize);
		void	IEEEToDCIReal(DWORD dData, UINT uSize);
		DWORD	MakeReadExponent(BYTE b);
		BYTE	MakeWriteExponent(BYTE b);
		UINT	GetRealSize(UINT uTable);
		UINT	GetExistingString(AREF Addr, UINT uStartReg, UINT uCount, PDWORD pData);
		void	UnpackDWORDs(WRITETEXT *pWText, BOOL fDCIRead);
		void	UnpackDWORD (PBYTE p, DWORD dData);
		BYTE	CheckChar(BYTE b);
		void	PutOneDWORD(PBYTE p, DWORD d, UINT uSel);
		void	DoReadAfterWrite(WRITETEXT *pWText);
		DWORD	Get4Bytes(PBYTE p);
	};

// End of File
