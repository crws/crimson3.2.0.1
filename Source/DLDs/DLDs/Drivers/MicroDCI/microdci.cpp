
#include "intern.hpp"

#include "microdci.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Micromod Micro-DCI Driver
//

// Instantiator

INSTANTIATE(CMicrodciDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CMicrodciDriver::CMicrodciDriver(void)
{
	m_Ident     = DRIVER_ID;
	}

// Destructor

CMicrodciDriver::~CMicrodciDriver(void)
{
	}

// Configuration

void MCALL CMicrodciDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CMicrodciDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {

		Config.m_uFlags |= flagFastRx;
		}

	Make485(Config, TRUE);
	}
	
// Management

void MCALL CMicrodciDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMicrodciDriver::Open(void)
{
	}

// Device

CCODE MCALL CMicrodciDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop      = GetByte(pData);

			m_pCtx->m_uWriteErr  = 0;

			m_pCtx->m_fHasStuffedByte  = TRUE;

			m_pCtx->m_fUseDirectAccess = FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMicrodciDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMicrodciDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table	= SP_L;
	Addr.a.m_Offset	= STUFFIT;
	Addr.a.m_Type	= BB;
	Addr.a.m_Extra  = 0;

	CCODE c = Read(Addr, Data, 1);

	if( c == 1 ) {

		m_pCtx->m_fHasStuffedByte = !(BOOL)Data[0];
		}

	return c;
	}

CCODE MCALL CMicrodciDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nREAD T=%d O=%d C=%d", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( Addr.a.m_Table == SP_Z ) {

		*pData = (DWORD)m_pCtx->m_fUseDirectAccess;

		return 1;
		}

//**/	Sleep(100);	// slow down during debug

	CAddress A;

	A.m_Ref = Addr.m_Ref;

	A.a.m_Offset = Transform(A.a.m_Table, A.a.m_Offset);

	switch( Addr.a.m_Type ) {

		case BB:
			return DoBitRead (Addr, pData, uCount);

		case YY:
			return DoByteRead(A, pData, uCount);

		case YW:
			return DoWordRead(A, pData, uCount);

		case LL:
		case YL:
			return IsTextSpace(A.a.m_Table) ? DoTextRead(A, pData, uCount) : DoLongRead(A, pData, uCount);

		case RR:
			return DoRealRead(A, pData, uCount);
		}

	return 1;
	}

CCODE MCALL CMicrodciDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n********** WRITE T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( Addr.a.m_Table == SP_Z ) {

		m_pCtx->m_fUseDirectAccess = (BOOL)*pData;

		return 1;
		}

	CAddress A;

	A.m_Ref = Addr.m_Ref;

	A.a.m_Offset = Transform(A.a.m_Table, A.a.m_Offset);

	CCODE c = CCODE_ERROR;

	switch( Addr.a.m_Type ) {

		case BB:
			c = DoBitWrite (Addr, pData, uCount);
			break;

		case YY:
			c = DoByteWrite(A, pData, uCount);
			break;

		case YW:
			c = DoWordWrite(A, pData, uCount);
			break;

		case LL:
		case YL:
			c = IsTextSpace(A.a.m_Table) ? DoTextWrite(Addr, pData, uCount) : DoLongWrite(A, pData, uCount);
			break;

		case RR:
			c = DoRealWrite(A, pData, uCount);
			break;

		default: return uCount;
		}

//**/	AfxTrace1("\r\n%8.8lx\r\n", c);

	if( c & CCODE_ERROR ) {

		if( ++m_pCtx->m_uWriteErr > 1 ) {

			m_pCtx->m_uWriteErr = 0;

			return IsTextSpace(A.a.m_Table) ? uCount : 1;
			}
		}

	else {
		m_pCtx->m_uWriteErr = 0;
		}

	return c;
	}

// PRIVATE METHODS

// Frame Building

void CMicrodciDriver::StartFrame(UINT uOffset, UINT uCount, BYTE bOp)
{
	m_bTx[0] = MSOH;

	m_uPtr   = 1;

	AddByte( bOp | m_pCtx->m_bDrop );

	AddByte( LOBYTE(uCount) );

	AddWord( (WORD)uOffset );
	}

void CMicrodciDriver::EndFrame(void)
{
	DoLRC();

	m_bTR[0] = MSOH;

	for( UINT i = 1, j = 1; i < m_uPtr; i++ ) {

		BYTE b   = m_bTx[i];

		m_bTR[j++] = b;

		if( b == MSOH ) {

			m_bTR[j++] = 0;

			m_uPtr++;
			}
		}
	}

void CMicrodciDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CMicrodciDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));
	AddByte(HIBYTE(wData));
	}

void CMicrodciDriver::AddLong(DWORD dData)
{
	AddWord(LOWORD(dData));
	AddWord(HIWORD(dData));
	}

// Opcode handling

CCODE CMicrodciDriver::DoBitRead( AREF Addr, PDWORD pData, UINT uCount )
{
	MakeMin(uCount, 256);

	UINT uOffset	= Addr.a.m_Offset;

	UINT uByteNum	= m_pCtx->m_fUseDirectAccess ? uOffset / 8 : FORML(uOffset);

	UINT uBitNumber	= uOffset & 0x7;

	UINT uByteCount	= 1 + ((uBitNumber + uCount) >> 3);

	if( ReadBytes(uByteNum, uByteCount) ) { // read necessary number of bytes to get uCount bits

		BYTE bBit = 1 << uBitNumber;

		UINT n    = 0;

		for( UINT c = 0; c < uByteCount; c++ ) {

			BYTE r = m_bRx[c + 5];

			while( bBit ) {

				if( n == uCount ) break;

				pData[n++] = bBit & r ? 1L : 0;

				bBit <<= 1;
				}

			bBit = 1;
			}

		return n;
		}

	return CCODE_ERROR;
	}

CCODE CMicrodciDriver::DoByteRead(AREF Addr, PDWORD pData, UINT uCount)
{
	return DoRead(Addr.a.m_Offset, pData, uCount, 1, FALSE);
	}

CCODE CMicrodciDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	return DoRead(Addr.a.m_Offset, pData, uCount, 2, FALSE);
	}

CCODE CMicrodciDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	return DoRead(Addr.a.m_Offset, pData, uCount, 4, FALSE);
	}

CCODE CMicrodciDriver::DoRealRead(AREF Addr, PDWORD pData, UINT uCount)
{
	return DoRead(Addr.a.m_Offset, pData, uCount, GetRealSize(Addr.a.m_Table), TRUE);
	}

CCODE CMicrodciDriver::DoTextRead(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 12);

	BOOL fIsA	= Addr.a.m_Table == SP_A;

	UINT uByteCount	= fIsA ? ((uCount + 3) / 4) * 10 : ((uCount + 1) / 2) * 5; // A=4 regs, F=2 regs

	if( ReadBytes(Addr.a.m_Offset, uByteCount) ) {

		PBYTE p   = m_bRx + 5;

		UINT uInc = fIsA ?  4 : 2;
		UINT uSze = fIsA ? 10 : 5;

		for( UINT k = 0; k < uByteCount; k++ ) {

			p[k] = CheckChar(p[k]); // turn nulls to space, other invalids to ?
			}

		m_dForceWrite++;

		for( UINT i = 0, j = 0; i < uCount; i += uInc, j++ ) {

			PBYTE pd = p + (j * uSze);

			pData[i]   = Get4Bytes(pd);

			if( uCount > i + 1 ) {

				pData[i+1] = Get4Bytes(pd + 4);

				if( fIsA ) {

					if( uCount > i + 2 ) {

						pData[i+2]  = Get4Bytes(pd + 8) & MASKA;

						pData[i+2] += LOBYTE(m_dForceWrite); // trigger write of this item

						if( uCount > i + 3 ) {

							pData[i+3] = 0;
							}
						}
					}

				else {
					pData[i+1] &= MASKF;

					pData[i+1] |= m_dForceWrite & ~MASKF; // force 2nd DWORD to be written, also.
					}
				}
			}

//**/		AfxTrace1("\r\nRead return %d ", uCount);

		if( uCount ) return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CMicrodciDriver::DoRead(UINT uOffset, PDWORD pData, UINT uCount, UINT uSize, BOOL fReal)
{
	MakeMin(uCount, 32/uSize);

	UINT uByteCount = uCount * uSize;

	PBYTE p = m_bRx + 5;

	if( ReadBytes(uOffset, uByteCount) ) {

		for( UINT i = 0; i < uCount; i++ ) {

			switch( uSize ) {

				case 2:
					pData[i] = (DWORD)IntelToHost(PU2(p)[0]);

					p += 2;

					break;

				case 3:
				case 4:
				case 5:
					if( fReal ) { // 3 or 5

						DCIRealToIEEE(i, pData, uSize);
						}

					else {
						pData[i] = (DWORD)IntelToHost(PU4(p)[0]);

						p += 4;
						}
					break;

				default:
					pData[i] = (DWORD)*(p + i);
					break;
				}
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

BOOL CMicrodciDriver::ReadBytes( UINT uOffset, UINT uCount)
{
	StartFrame(uOffset, uCount, INTER);

	if( Transact(FALSE) ) {

		m_pCtx->m_uWriteErr = 0;

		return TRUE;
		}

	return FALSE;
	}

CCODE CMicrodciDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset	= Addr.a.m_Offset;

	UINT uByteNum	= m_pCtx->m_fUseDirectAccess ? uOffset / 8 : FORML(uOffset);

	UINT uBitNumber	= uOffset & 0x7;

	StartFrame(uByteNum, 2, CNGBT);

	BYTE bBit	= 1 << uBitNumber;

	BYTE bMask	= 0xFF ^ bBit;	// Mask - bit = 0 means allow write

	AddByte(bMask);

	AddByte((*pData & 1) ? bBit : 0);

	if( Transact(TRUE) ) {

		m_pCtx->m_uWriteErr = 0;

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE CMicrodciDriver::DoByteWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	return DoWrite(Addr.a.m_Offset, pData, uCount, 1, FALSE);
	}

CCODE CMicrodciDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	return DoWrite(Addr.a.m_Offset, pData, uCount, 2, FALSE);
	}

CCODE CMicrodciDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	return DoWrite(Addr.a.m_Offset, pData, uCount, 4, FALSE);
	}

CCODE CMicrodciDriver::DoRealWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	return DoWrite(Addr.a.m_Offset, pData, uCount, GetRealSize(Addr.a.m_Table), TRUE);
	}

CCODE CMicrodciDriver::DoTextWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	BOOL fIsA	= Addr.a.m_Table == SP_A;

	UINT uRAllocPerStr	= fIsA ? 16 : 8;

	UINT uCharPerStr	= fIsA ? 10 : 5;

	UINT uRUsedPerStr	= fIsA ?  3 : 2;

	DWORD Data[16];

	UINT uChangeReg		= Addr.a.m_Offset % uRAllocPerStr;

	UINT uBase		= Addr.a.m_Offset - uChangeReg;

	WRITETEXT wText;

	WRITETEXT *pWText	= &wText;

	pWText->fIsA		= fIsA;
	pWText->uRAllocPerStr	= uRAllocPerStr;
	pWText->uCharPerStr	= uCharPerStr;
	pWText->uRUsedPerStr	= uRUsedPerStr;
	pWText->dm_Ref		= Addr.m_Ref;
	pWText->pWrData		= pData;
	pWText->uDPos		= uChangeReg / 4;

	UINT uAllocSz		= fIsA ? 4 : 2;

	UINT uGetStringCt	= 1;

	if( uCount > uAllocSz && !uChangeReg ) {

		uGetStringCt = min( 12, (uCount + uAllocSz - 1) ) / uAllocSz;
		}

	UINT uCnt = GetExistingString(Addr, uBase, uGetStringCt, Data);	// read up to 30 char's from controller

	if( !uCnt ) {

		return CCODE_ERROR;
		}

	UINT uQty	= uCnt / uAllocSz;

	UINT uByteCount	= 0;

	memset(m_ReadStrings, 0, sizeof(m_ReadStrings));

	pWText->pList	= m_ReadStrings;
	pWText->pSrce	= Data;
	pWText->uCount	= pWText->uRUsedPerStr;

	UINT uNext	= uRAllocPerStr / 4;

	for( UINT i = 0, j = 0; i < uQty; i++, j += uNext ) {

		pWText->pSrce	= &Data[j];

		UnpackDWORDs(pWText, TRUE);

		pWText->pList	+= pWText->uCharPerStr;
		}

	UINT uActualAddress	= FORMAF(uBase);

	UINT uReturnCount	= uRUsedPerStr;

	if( uChangeReg || uCount < pWText->uRUsedPerStr ) {	// re-write partial

		if( fIsA && uChangeReg > 8 ) return 1;	// pData[3] is allocated in config, but unused

		BYTE bWriteIn[10];

		UINT uFirst	= pWText->uDPos;	// first register to be written

		uQty		= min(uCount, uRUsedPerStr - uFirst);

		pWText->pList	= bWriteIn;
		pWText->pSrce	= pData;
		pWText->uCount	= min(uCount, uQty);

		UnpackDWORDs(pWText, FALSE);	// transfer write data DWORDs to byte list

		for( UINT r = 0, n = 0; r < uRUsedPerStr; r++, n += 4 ) {

			if( r < uFirst ) {

				memcpy(&m_WriteStrings[n], &m_ReadStrings[n], 4);	// use data that was read
				}

			else {	// use data that is changed
				if( uCount == 1 && r != uFirst ) {

					memcpy(&m_WriteStrings[n], &m_ReadStrings[n], 4);
					}

				else {
					memcpy(&m_WriteStrings[n], &bWriteIn[n], 4);
					}
				}
			}

		StartFrame(uActualAddress, pWText->uCharPerStr, CHNGE);

		for( n = 0; n < pWText->uCharPerStr; n++ ) {

			AddByte(m_WriteStrings[n]);
			}

		if( Transact(TRUE) ) {

			pWText->uAddr = uBase;

			DoReadAfterWrite(pWText);

			return (CCODE)uQty;
			}

		return CCODE_ERROR;
		}

	else {	// starting at the base address

		pWText->pList	= m_WriteStrings;
		pWText->pSrce	= pData;
		pWText->uCount	= pWText->uRUsedPerStr;

		uQty		= min( uQty, uCount );

		for( UINT i = 0, j = 0; i < uQty; i++, j += uRUsedPerStr ) {

			pWText->pSrce	= &pData[j];

			UnpackDWORDs(pWText, FALSE);

			pWText->pList	+= pWText->uCharPerStr;

			uByteCount	+= pWText->uCharPerStr;
			}

		uReturnCount = min( uCount, uQty * pWText->uRUsedPerStr );
		}

	StartFrame(uActualAddress, uQty * pWText->uCharPerStr, CHNGE);

	for( UINT n = 0; n < uByteCount; n++ ) {

		AddByte(m_WriteStrings[n]);
		}

	if( Transact(TRUE) ) {

		pWText->uAddr = uBase;

		DoReadAfterWrite(pWText);

		return min( uCount, uQty * pWText->uRUsedPerStr );
		}

	return CCODE_ERROR;
	}

CCODE CMicrodciDriver::DoWrite(UINT uOffset, PDWORD pData, UINT uCount, UINT uSize, BOOL fReal)
{
	MakeMin(uCount, 32/uSize);

	StartFrame(uOffset, uCount * uSize, CHNGE);

	for( UINT i = 0; i < uCount; i++ ) {

		switch( uSize ) {

			case 2:
				AddWord(LOWORD(pData[i]));
				break;

			case 3:
			case 5:
				IEEEToDCIReal(pData[i], uSize);
				break;

			case 4:
				AddLong(pData[i]);
				break;

			default:
				AddByte(LOBYTE(pData[i]));
				break;
			}
		}

	return Transact(TRUE) ? uCount : CCODE_ERROR;
	}

// Transport Layer

BOOL CMicrodciDriver::Transact(BOOL fChkWrite)
{
	Send();

	if( GetReply(fChkWrite) ) {

		if( fChkWrite ) {

			SendAck();
			}

		return TRUE;
		}

	return FALSE;
	}

void CMicrodciDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CMicrodciDriver::GetReply(BOOL fChkWrite)
{
	UINT uState = 0;

	UINT uPtr   = 0;

	UINT uTimer = 0;

	UINT uEnd   = BUFFSZA - 1;

	UINT uData;

	BOOL fIsSOH = FALSE;

	BOOL fArmed = FALSE;

	SetTimer( TIMEOUT );

//**/	AfxTrace0("\r\n");

	while( (uTimer = GetTimer()) ) {

		if( ((uData = Get(uTimer)) == NOTHING) ) {

			continue;
			}

		BYTE bData	= LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);

		if( fIsSOH ) { // Skip this stuffed byte. If LRC == MSOH, however, return

			if( uPtr >= uEnd ) return CheckReply(uEnd, fChkWrite);

			fIsSOH = FALSE;

			continue;
			}

		m_bRx[uPtr++]	= bData;

		fIsSOH = fArmed && (bData == MSOH);

		switch( uState ) {

			case 0:
				if( bData == MSOH ) {

					uState = 1;
					fArmed = m_pCtx->m_fHasStuffedByte;
					}

				else {
					uPtr = 0;
					uEnd = BUFFSZA - 1;
					}

				break;

			case 1:
				uState = (bData & 0xE0) == RESPO ? 2 : 0;
				break;

			case 2:
				if( bData <= 32 ) {

					uEnd	= bData + 6;
					uState	= 3;
					}

				else uState = 0;
				break;

			case 3:
				uState	= bData == m_bTx[3] ? 4 : 0;	// LOBYTE Parameter address
				break;

			case 4:
				uState	= bData == m_bTx[4] ? 5 : 0;	// HIBYTE Parameter address
				break;

			case 5:
				if( uPtr >= uEnd && !fIsSOH ) {

					return CheckReply(uEnd, fChkWrite);
					}
				break;
			}
		}	
 
	return FALSE;
	}

BOOL CMicrodciDriver::CheckReply(UINT uSize, BOOL fChkWrite)
{
	BYTE bLRC = 0;

	UINT uEnd = uSize - 1;

	for( UINT i = 1; i < uEnd; i++ ) {

		BYTE b = m_bRx[i];

		bLRC  += b;
		}

	if( fChkWrite ) {

		for( i = 2; i < uEnd; i++ ) {

			if( m_bRx[i] != m_bTx[i] ) return FALSE; // data mismatch
			}
		}

	return bLRC == m_bRx[uEnd];
	}

// Port Access

void CMicrodciDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_pData->Write( PCBYTE(m_bTR), m_uPtr, FOREVER );
	}

UINT CMicrodciDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

BOOL CMicrodciDriver::IsTextSpace(UINT uTable)
{
	return uTable == SP_A || uTable == SP_F;
	}

UINT CMicrodciDriver::Transform(UINT uTable, UINT uOffset)
{
	if( !m_pCtx->m_fUseDirectAccess ) {

		switch( uTable ) {

			case SP_A:
			case SP_F: return FORMAF(uOffset);

			case SP_B: return FORMB(uOffset);
			case SP_C: return FORMC(uOffset);
			case SP_H: return FORMH(uOffset);
			case SP_L: return FORML(uOffset);
			}
		}

	return uOffset;
	}

void CMicrodciDriver::SendAck(void)
{
	m_pData->ClearRx();

	m_uPtr = 2;

	m_bTR[0] = MSOH;
	m_bTR[1] = ACKNO + m_pCtx->m_bDrop;

	Sleep(20);

	Put();
	}

void CMicrodciDriver::DoLRC(void)
{
	BYTE b = 0;

	for( UINT i = 1; i < m_uPtr; i++  ){

		b += m_bTx[i];
		}

	AddByte(b);
	}

void CMicrodciDriver::DCIRealToIEEE(UINT uItem, PDWORD pData, UINT uSize)
{
	PBYTE p	  = &m_bRx[5 + (uItem * uSize)];

	BOOL fLo  = uSize == 3;

	BOOL fNeg = p[0] & 0x80;

	if( fNeg ) {

		for( UINT i = 0; i < uSize - 1; i++ ) {

			p[i] = ~p[i];
			}
		}

	DWORD x   = Get4Bytes(p);

	UINT u0   = fLo ? 0x3FFFFF00 : 0x7FFFFFFF;

	if( x & u0 ) {

		x &= fLo ? 0x3FFF0000 : 0x3FFFFFFF;

		x >>= 7;

		if( fNeg ) x |= 0x80000000;

		DWORD e   = MakeReadExponent(p[uSize - 1]);

		pData[uItem] = x | e;
		}

	else pData[uItem] = 0;
	}

void CMicrodciDriver::IEEEToDCIReal(DWORD dData, UINT uSize)
{
	BOOL fNeg = dData & 0x80000000;

	if( fNeg ) dData ^= 0x1FFFF;

	BYTE b = LOBYTE((dData >> 17) & 0x3F) | 0x40; // low 6 bits of 1st byte of mantissa + the '1'

	if( fNeg ) b = ~b | 0x80; // negate and add sign back in

	AddByte(b);

	AddByte(dData >> 9);

	if( uSize == 5 ) {

		AddByte(dData >> 1);
		AddByte(fNeg ? 0xFF : 0);
		}

	AddByte(MakeWriteExponent(dData >> 23));
	}

DWORD CMicrodciDriver::MakeReadExponent(BYTE b)
{
	b += 0x7E;

	return (DWORD)b << 23;
	}

BYTE CMicrodciDriver::MakeWriteExponent(BYTE b)
{
	return b - 0x7E;
	}

UINT CMicrodciDriver::GetRealSize(UINT uTable)
{
	return uTable == SP_C ? 3 : 5;
	}

UINT CMicrodciDriver::GetExistingString(AREF Addr, UINT uStartReg, UINT uCount, PDWORD pData)
{
	BOOL fIsA	= Addr.a.m_Table == SP_A;

	CAddress A;

	A.m_Ref		= Addr.m_Ref;

	A.a.m_Offset	= uStartReg;

	UINT uCnt	= fIsA ? 4 : 2;

	for( UINT i = 0; i < uCount; i++ ) {

		UINT uWE = m_pCtx->m_uWriteErr;

		if( Read(A, pData, uCnt) != (CCODE)uCnt ) {

			return 0;
			}

		m_pCtx->m_uWriteErr = uWE;

		pData += uCnt;

		A.a.m_Offset += fIsA ? 16 : 8;

		if( A.a.m_Offset > 5120 ) {

			return i * uCnt;
			}
		}

	return uCount * uCnt;
	}

void CMicrodciDriver::UnpackDWORDs(WRITETEXT *pWText, BOOL fDCIRead)
{
	PBYTE  pB	= pWText->pList;
	PDWORD pS	= pWText->pSrce;

	BOOL   fIsA	= pWText->fIsA;

	UnpackDWORD(pB, *pS);	// unpack bytes 0-3

	if( fDCIRead ) {

		DWORD d = pS[fIsA ? 2 : 1];

		if( fIsA ) {

			UnpackDWORD(&pB[4], pS[1]);

			pB[8]	= CheckChar( HIBYTE(HIWORD(d)) );
			pB[9]	= CheckChar( LOBYTE(HIWORD(d)) );
			}

		else {
			pB[4] = CheckChar(HIBYTE(HIWORD(d)));
			}
		}

	else {
		UINT uMax = pWText->uCount;

		switch( pWText->uDPos ) {

			case 0:
				UnpackDWORD(pB, pS[0]);

				if( uMax > 1 ) {

					if( fIsA ) {

						UnpackDWORD(&pB[4], pS[1]);

						if( uMax > 2 ) {

							PutOneDWORD(pB, pS[2], ATXTEND);
							}
						}

					else {
						PutOneDWORD(pB, pS[1], FTXTEND);
						}
					}
				break;

			case 1:
				if( fIsA ) {

					UnpackDWORD(&pB[4], pS[0]);

					if( uMax > 1 ) {

						PutOneDWORD(pB, pS[1], ATXTEND);
						}
					}

				else {
					PutOneDWORD(pB, pS[0], FTXTEND);
					}
				break;

			case 2:
				PutOneDWORD(pB, pS[0], ATXTEND);
				break;
			}
		}

	if( !fIsA ) memset(&pB[5], 0, 5);
	}

void CMicrodciDriver::UnpackDWORD(PBYTE p, DWORD dData)
{
	p[0] = CheckChar(HIBYTE(HIWORD(dData)));
	p[1] = CheckChar(LOBYTE(HIWORD(dData)));
	p[2] = CheckChar(HIBYTE(LOWORD(dData)));
	p[3] = CheckChar(LOBYTE(LOWORD(dData)));
	}

BYTE CMicrodciDriver::CheckChar(BYTE b)
{
	if( !b ) return ' ';

	return (b >= ' ' && b < 128) ? b : '?';
	}

void CMicrodciDriver::PutOneDWORD(PBYTE p, DWORD d, UINT uSel)
{
	switch( uSel ) {

		case ATXTEND:
			p[8] = CheckChar(HIBYTE(LOWORD(d)));
			p[9] = CheckChar(LOBYTE(LOWORD(d)));
			break;

		case ATXTMID:
			UnpackDWORD(&p[4], d);
			break;

		case FTXTEND:
			p[4] = CheckChar(LOBYTE(LOWORD(d)));
			break;

		default: // A/F first
			UnpackDWORD(p, d);
			break;
		}			
	}

void CMicrodciDriver::DoReadAfterWrite(WRITETEXT *pWText)
{
	DWORD Data[4];

	CAddress Addr;

	Addr.m_Ref	= pWText->dm_Ref;

	pWText->pList	= m_ReadStrings;
	pWText->pSrce	= Data;

	UINT uMiss	= 0;
	UINT uCheck	= 0;
	UINT uSize	= pWText->fIsA ? 10 : 5;

	while( uCheck++ < 5 ) {
	
		Sleep(50);

		uMiss = 0;

		if( GetExistingString(Addr, pWText->uAddr, 1, Data) ) {

			UnpackDWORDs(pWText, TRUE);

			UINT n = 0;

			while( n < uSize ) {

				if( m_ReadStrings[n] != m_WriteStrings[n] ) {

					uMiss++;
					}
				n++;
				}
			}

		if( !uMiss ) {

			return;	// string matches
			}
		}
	}

DWORD CMicrodciDriver::Get4Bytes(PBYTE p)
{
	DWORD x = 0;

	for( UINT i = 0; i < 4; i++ ) {

		x <<= 8;

		x  += p[i];
		}

	return x;
	}

/* Format of Micromod DCI Floating Point Numbers
SP_C - Low Precision
Represents floating point values that have a resolution of one part in 32,768 (15 bits) and a dynamic
range of � 10^38. The first two bytes represent a 2�s complement notation in fractional form (2^-n ) whose
absolute value is between 0.5 and 0.9999. The third byte is the power of 2 in 2�s complement notation.
Floating point example:
64H 00H 07H = 100D (Decimal)
64H = 01100100, fractional binary weights left to right are
0 = 2�s complement positive,
1 = 2^-1 = 1/2 = 0.5,
1 = 2^-2 = 1/4 = 0.25,
0=0,
0=0,
1 = 2^-5 = 1/32 = 0.03125,
0=0,
0=0.

64H = 0.5 + 0.25 + 0.03125 = 0.78125.
07H = 128D
128D X 0.78125D = 100.

Examples from Micromod:
This is a request for C00 from a MC5000 controller with network address 11. That is,  it reads three bytes from address 0x600.
7e eb 03 00 06 f4 
This is the response. The value is approx. 12.34
7e 2b 03 00 06 62 b8 04 52

This is a request for C01 from a MC5000 controller. That is,  it reads three bytes from address 0x603.
7e eb 03 03 06 f7 
This is the response. The value is approx 0.089
7e 2b 03 03 06 5b 22 fd b1
************
************
SP_H - High Precision
Represents high precision floating point values that have a resolution of one part in 2 billion (31 bits)
and a dynamic range of � 10^38. The first four bytes represent a 2�s complement notation in fractional form (2^-n)
whose absolute value is between 0.5 and 0.9999. The fifth byte is the power of 2 in 2�s complement notation.
Floating point example:
9CH 00H 00H 00H 07H = -100.
The 2�s complement notation bit in the 9 = 1 (1001) indicating a negative number; therefore, 9C must be 
re-complemented.
9C = 10011100, change 1�s to 0�s and 0�s to 1�s = 0110 0011 and add 1
=
01100100 (64H).
Fractional binary weights left to right for 0110 0100 are
0 = 2�s complement positive,
1 = 2^-1 = 1/2 = 0.5,
1 = 2^-2 = 1/4 = 0.25,
0=0,
0=0,
1= 2^-5= 1/32 = 0.03125,
0=0,
0=0.
64H = 0.5 + 0.25 + 0.03125 = 0.78125.
07H=128D, 128D X 0.78125D = 100.
A negative sign is assigned (-100) because the original 2�s complement binary bit in the 9 (1001) of 9C was set
indicating a negative number.
*/

// End of File
