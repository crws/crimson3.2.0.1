#ifndef	INCLUDE_Dnp3IpM_HPP
	
#define	INCLUDE_Dnp3IpM_HPP

#include "dnp3m.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 TCP Master Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

class CDnp3IpMaster : public CDnp3Master
{
	public:
		// Constructor
		CDnp3IpMaster(void);

		// Destructor
		~CDnp3IpMaster(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:

		struct CIpCtx : CDnp3Master::CContext {

			DWORD	 m_IP1;
			DWORD	 m_IP2;
			WORD	 m_TcpPort;
			WORD     m_UdpPort;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime3;
			BOOL	 m_fKeep;
			BOOL	 m_fDirty;
			BOOL	 m_fAux;
						
			IDnpChannel * m_pChannel;

			IDnpChannelConfig * m_pConfig;
			};
		
		//Data Members
		CIpCtx * m_pIpCtx;
		UINT	 m_uKeep;
		
		// Channels
	virtual	void OpenChannel (void);
	virtual BOOL CloseChannel(void);

		// Helpers
		BOOL IsShared(void);

	};

#endif

// End of File
