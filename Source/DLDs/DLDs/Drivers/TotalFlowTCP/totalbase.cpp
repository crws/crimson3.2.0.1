
#include "intern.hpp"

#include "totalbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Driver
//

// Constructor

CTotalFlowMasterDriver::CTotalFlowMasterDriver(void)
{
	m_pCtx = NULL;
	}

// Entry Points

CCODE MCALL CTotalFlowMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		CSuper   Super;

		CHeader  Header;

		CBuffer *pBuff = AllocBuffer();

		AddInitialSequence(pBuff);

		AddHeader(pBuff, 0, 0, 1);

		if( !(Addr.a.m_Extra & 1) ) {

			MakeMin(uCount, 32);

			AddReadRecord( pBuff,
				       m_pCtx->m_bApp,
				       Addr.a.m_Table - 1,
				       Addr.a.m_Offset,
				       uCount
				       );

			if( Transact(pBuff, Super, Header) ) {

				if( Header.m_bCount == 1 ) {

					UINT  uReply = 0;

					PBYTE pReply = RecvSizedFrame(uReply);

					if( pReply ) {

						BOOL fMatch = CheckReadRecord( pReply,
									       m_pCtx->m_bApp,
									       Addr.a.m_Table - 1,
									       Addr.a.m_Offset,
									       uCount
									       );

						if( fMatch ) {

							if( Addr.a.m_Type == addrByteAsByte ) {

								PCBYTE pFrom = PCBYTE(pReply + 10);

								for( UINT n = 0; n < uCount; n++ ) {

									pData[n] = pFrom[n];
									}

								delete [] pReply;

								return uCount;
								}

							if( Addr.a.m_Type == addrWordAsWord ) {

								PCWORD pFrom = PCWORD(pReply + 10);

								for( UINT n = 0; n < uCount; n++ ) {

									pData[n] = IntelToHost(pFrom[n]);
									}

								delete [] pReply;

								return uCount;
								}

							if( Addr.a.m_Type == addrLongAsLong || Addr.a.m_Type == addrRealAsReal ) {

								PCDWORD pFrom = PCDWORD(pReply + 10);

								for( UINT n = 0; n < uCount; n++ ) {

									pData[n] = IntelToHost(pFrom[n]);
									}

								delete [] pReply;

								return uCount;
								}

							memset(pData, 0, 4 * uCount);

							delete [] pReply;

							return uCount;
							}
						}
					}
				}
			}
		else {
			MakeMin(uCount, 16);

			UINT uIndex = Addr.a.m_Offset;

			AddReadRecord( pBuff,
				       m_pCtx->m_bApp,
				       Addr.a.m_Table - 1,
				       uIndex / 16,
				       uCount
				       );

			CSuper  Super;

			CHeader Header;

			if( Transact(pBuff, Super, Header) ) {

				if( Header.m_bCount == 1 ) {

					UINT  uReply = 0;

					PBYTE pReply = RecvSizedFrame(uReply);

					if( pReply ) {

						BOOL fMatch = CheckReadRecord( pReply,
									       m_pCtx->m_bApp,
									       Addr.a.m_Table - 1,
									       uIndex / 16,
									       uCount
									       );

						if( fMatch ) {

							PCDWORD pFrom = PCDWORD(pReply + 10) + uIndex % 16;

							for( UINT n = 0; n < uCount; n++ ) {

								pData[n] = IntelToHost(pFrom[n]);
								}

							delete [] pReply;

							return uCount;
							}
						}
					}
				}
			}

		AbortLink();

		return CCODE_ERROR;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CTotalFlowMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		CSuper   Super;

		CHeader  Header;

		CBuffer *pBuff = AllocBuffer();

		AddInitialSequence(pBuff);

		AddHeader(pBuff, 0, 1, 1);

		if( !(Addr.a.m_Extra & 1) ) {

			if( Transact(pBuff, Super, Header) ) {

				MakeMin(uCount, 16);

				PBYTE pCopy = New BYTE [ uCount * 4];

				UINT  uCopy = 0;

				for( UINT n = 0; n < uCount; n++ ) {

					if( Addr.a.m_Type == addrByteAsByte ) {

						pCopy[uCopy++] = BYTE(pData[n]);
						}

					if( Addr.a.m_Type == addrWordAsWord ) {

						pCopy[uCopy++] = LOBYTE(pData[n]);

						pCopy[uCopy++] = HIBYTE(pData[n]);
						}

					if( Addr.a.m_Type == addrLongAsLong || Addr.a.m_Type == addrRealAsReal ) {

						pCopy[uCopy++] = LOBYTE(LOWORD(pData[n]));

						pCopy[uCopy++] = HIBYTE(LOWORD(pData[n]));

						pCopy[uCopy++] = LOBYTE(HIWORD(pData[n]));

						pCopy[uCopy++] = HIBYTE(HIWORD(pData[n]));
						}
					}

				if( uCopy ) {

					pBuff = AllocBuffer();

					AddSupervisoryFrame(pBuff);

					AddWriteRecord( pBuff,
							m_pCtx->m_bApp,
							Addr.a.m_Table - 1,
							Addr.a.m_Offset,
							pCopy,
							uCopy
							);

					if( Transact(pBuff, Super, Header) ) {

						delete [] pCopy;

						return uCount;
						}
					}

				delete [] pCopy;
				}
			}
		else
			pBuff->Release();

		AbortLink();

		return CCODE_ERROR;
		}

	return CCODE_ERROR;
	}

// Buffer Allocation

CBuffer * CTotalFlowMasterDriver::AllocBuffer(void)
{
	CBuffer *pBuff;

	while( !(pBuff = CreateBuffer(1280, TRUE)) ) {

		Sleep(5);
		}

	return pBuff;
	}

// Frame Creation

void CTotalFlowMasterDriver::AddInitialSequence(CBuffer *pBuff)
{
	for( UINT n = 0; n < m_pCtx->m_uEstab; n++ ) {

		AddSupervisoryFrame(pBuff);
		}

	AddSyncPattern(pBuff);
	}

void CTotalFlowMasterDriver::AddSupervisoryFrame(CBuffer *pBuff)
{
	BuffAppend(pBuff, BYTE, 0xFF);
	
	CSuper *pSuper = BuffAddTail(pBuff, CSuper);

	pSuper->m_bSOH = 0x01;

	UINT uName = strlen(m_pCtx->m_sName);

	memset(pSuper->m_sName, 32, 10);

	memcpy(pSuper->m_sName, m_pCtx->m_sName, min(uName, 10));

	pSuper->m_bType = 'X';

	pSuper->m_bSlot = 0;

	CRC16 crc;

	crc.Clear();

	Add(crc, PCBYTE(m_pCtx->m_sCode), 4);

	Add(crc, pSuper, &pSuper->m_wCRC);

	pSuper->m_wCRC = HostToMotor(crc.GetValue());
	}

void CTotalFlowMasterDriver::AddPasswordFrame(CBuffer *pBuff, PCTXT pCode)
{
	BuffAppend(pBuff, BYTE, 0x00);

	PCBYTE pFrom = BuffGetNext(pBuff);

	BuffAppend(pBuff, BYTE, pCode[0] | 0x80);
	BuffAppend(pBuff, BYTE, pCode[1] | 0x80);
	BuffAppend(pBuff, BYTE, pCode[2] | 0x80);
	BuffAppend(pBuff, BYTE, pCode[3] | 0x80);

	BuffAppend(pBuff, BYTE, 0x00);

	CRC16 crc;

	crc.Clear();

	Add(crc, pFrom, BuffGetNext(pBuff));

	BuffAppend(pBuff, WORD, HostToMotor(crc.GetValue()));
	}

void CTotalFlowMasterDriver::AddSyncPattern(CBuffer *pBuff)
{
	BuffAppend(pBuff, BYTE, 0xD3);

	BuffAppend(pBuff, BYTE, 0xD9);

	BuffAppend(pBuff, BYTE, 0xCE);

	BuffAppend(pBuff, BYTE, 0xC3);
	}

void CTotalFlowMasterDriver::AddHeader(CBuffer *pBuff, BYTE bType, BYTE bRequest, BYTE bCount)
{
	CHeader Header;

	Header.m_bType    = bType;

	Header.m_bRequest = bRequest;

	Header.m_bCount   = bCount;

	AddSizedFrame(pBuff, &Header, sizeof(Header));
	}

void CTotalFlowMasterDriver::AddReadRecord(CBuffer *pBuff, BYTE bApp, BYTE bArray, WORD wIndex, WORD wCount)
{
	CRecord Record;

	Record.m_bType    = 0;

	Record.m_bRequest = 2;

	Record.m_bApp     = bApp;

	Record.m_bArray   = bArray;

	Record.m_wIndex   = HostToIntel(wIndex);

	Record.m_wCount   = HostToIntel(wCount);

	Record.m_wSpare   = 0;

	AddSizedFrame(pBuff, &Record, sizeof(Record));
	}

void CTotalFlowMasterDriver::AddWriteRecord(CBuffer *pBuff, BYTE bApp, BYTE bArray, WORD wIndex, PCBYTE pData, UINT uData)
{
	UINT     uWork  = sizeof(CRecord) + uData;

	PBYTE    pWork  = New BYTE [ uWork ];

	CRecord &Record = (CRecord &) pWork[0];

	Record.m_bType    = 0;

	Record.m_bRequest = 2;

	Record.m_bApp     = bApp;

	Record.m_bArray   = bArray;

	Record.m_wIndex   = HostToIntel(wIndex);

	Record.m_wCount   = HostToIntel(WORD(1));

	Record.m_wSpare   = 0;

	memcpy(pWork + sizeof(CRecord), pData, uData);

	AddSizedFrame(pBuff, pWork, uWork);

	delete [] pWork;
	}

void CTotalFlowMasterDriver::AddSizedFrame(CBuffer *pBuff, PCVOID pData, UINT uData)
{
	PCBYTE pFrom = BuffGetNext(pBuff);

	BuffAppend(pBuff, WORD,  HostToIntel(WORD(uData)));

	BuffAppend(pBuff, WORD, ~HostToIntel(WORD(uData)));

	memcpy(pBuff->AddTail(uData), pData, uData);

	CRC16 crc;

	crc.Clear();

	Add(crc, pFrom, BuffGetNext(pBuff));

	BuffAppend(pBuff, WORD, HostToIntel(crc.GetValue()));
	}

// Frame Checking

BOOL CTotalFlowMasterDriver::CheckReadRecord(PCBYTE pData, BYTE bApp, BYTE bArray, WORD wIndex, WORD wCount)
{
	CRecord const *pRecord = (CRecord const *) pData;

	if( pRecord->m_bApp == bApp ) {

		if( pRecord->m_bArray == bArray ) {

			if( pRecord->m_wIndex == HostToIntel(wIndex) ) {

				if( pRecord->m_wCount == HostToIntel(wCount) ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Transactions

BOOL CTotalFlowMasterDriver::Transact(CBuffer *pBuff, CSuper &Super, CHeader &Header)
{
	if( Send(pBuff) ) {

		if( RecvSupervisoryFrame(Super) ) {

			if( RecvHeader(Header) ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	pBuff->Release();

	return FALSE;
	}

BOOL CTotalFlowMasterDriver::RecvSupervisoryFrame(CSuper &Super)
{
	UINT uState = 0;

	UINT uCount = 0;

	UINT uTime  = m_pCtx->m_uTime2;

	for(;;) {

		UINT uRead = Recv(uTime);

		if( uRead == NOTHING ) {

			break;
			}

		switch( uState ) {

			case 0:

				if( uRead == 0x01 ) {

					Super.m_bSOH = 0x01;

					uCount = 1;

					uState = 1;
					}
				break;
					
			case 1:

				(PBYTE(&Super))[uCount] = BYTE(uRead);

				if( ++uCount == sizeof(CSuper) ) {

					CRC16 crc;

					crc.Clear();

					Add(crc, &Super, &Super.m_wCRC);

					if( crc.GetValue() == MotorToHost(Super.m_wCRC) ) {

						return TRUE;
						}

					return FALSE;
					}

				break;
			}

		uTime = max(10, m_pCtx->m_uTime2 / 10);
		}

	return FALSE;
	}

BOOL CTotalFlowMasterDriver::RecvHeader(CHeader &Header)
{
	return RecvSizedFrame(PBYTE(&Header), sizeof(Header));
	}

BOOL CTotalFlowMasterDriver::RecvSizedFrame(PBYTE pData, UINT uData)
{
	UINT  uState = 0;

	UINT  uCount = 0;

	UINT  uTime  = m_pCtx->m_uTime2;

	CRC16 crc;

	for(;;) {

		UINT uRead = Recv(uTime);

		if( uRead == NOTHING ) {

			break;
			}

		if( uState <= 4 ) {

			if( uState == 0 ) {

				crc.Clear();
				}

			crc.Add(uRead);
			}

		switch( uState ) {

			case 0:
				if( uRead == LOBYTE(uData) ) {

					uState = 1;

					break;
					}

				uState = 0;

				break;

			case 1:
				if( uRead == HIBYTE(uData) ) {

					uState = 2;

					break;
					}

				uState = 0;

				break;

			case 2:
				if( uRead == LOBYTE(~uData) ) {

					uState = 3;

					break;
					}

				uState = 0;

				break;

			case 3:
				if( uRead == HIBYTE(~uData) ) {

					uCount = 0;

					uState = 4;
					
					break;
					}

				uState = 0;

				break;

			case 4:
				pData[uCount] = BYTE(uRead);

				if( ++uCount == uData ) {

					uState = 5;
					}
				break;

			case 5:
				if( uRead == LOBYTE(crc.GetValue()) ) {

					uState = 6;

					break;
					}

				return FALSE;

			case 6:
				if( uRead == HIBYTE(crc.GetValue()) ) {

					return TRUE;
					}

				return FALSE;
			}

		uTime = max(10, m_pCtx->m_uTime2 / 10);
		}

	return FALSE;
	}

PBYTE CTotalFlowMasterDriver::RecvSizedFrame(UINT &uData)
{
	UINT  uState = 0;

	UINT  uCount = 0;

	PBYTE pData  = NULL;

	UINT  uTime  = m_pCtx->m_uTime2;

	CRC16 crc;

	for(;;) {

		UINT uRead = Recv(uTime);

		if( uRead == NOTHING ) {

			if( pData ) {
				
				delete [] pData;
				}

			break;
			}

		if( uState <= 4 ) {

			if( uState == 0 ) {

				crc.Clear();
				}

			crc.Add(uRead);
			}

		switch( uState ) {

			case 0:
				uData  = uRead;

				uState = 1;

				break;

			case 1:
				uData  = uData | (uRead << 8);

				uState = 2;

				break;

			case 2:
				if( uRead == LOBYTE(~uData) ) {

					uState = 3;

					break;
					}

				uState = 0;

				break;

			case 3:
				if( uRead == HIBYTE(~uData) ) {

					pData  = New BYTE [ uData ];

					uCount = 0;

					uState = 4;
					
					break;
					}

				uState = 0;

				break;

			case 4:
				pData[uCount] = BYTE(uRead);

				if( ++uCount == uData ) {

					uState = 5;
					}
				break;

			case 5:
				if( uRead == LOBYTE(crc.GetValue()) ) {

					uState = 6;

					break;
					}

				delete [] pData;

				uData = 0;

				return NULL;

			case 6:
				if( uRead == HIBYTE(crc.GetValue()) ) {

					return pData;
					}

				delete [] pData;
	
				uData = 0;

				return NULL;
			}

		uTime = max(10, m_pCtx->m_uTime2 / 10);
		}

	uData = 0;

	return NULL;
	}

// CRC Helpers

void CTotalFlowMasterDriver::Add(CRC16 &crc, PCBYTE pData, UINT uData)
{
	for( ; uData--; crc.Add(*pData++) );
	}

void CTotalFlowMasterDriver::Add(CRC16 &crc, PCVOID pFrom, PCVOID pEnd)
{
	for( PCBYTE pData = PCBYTE(pFrom); pData < pEnd; crc.Add(*pData++) );
	}

// End of File
