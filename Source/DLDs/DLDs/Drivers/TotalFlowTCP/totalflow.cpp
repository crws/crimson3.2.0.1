
#include "intern.hpp"

#include "totalflow.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX Series PLC Master Driver
//

// Instantiator

INSTANTIATE(CTotalFlowTcpMasterDriver);

// Constructor

CTotalFlowTcpMasterDriver::CTotalFlowTcpMasterDriver(void)
{
	m_Ident = DRIVER_ID;

	m_pCtx  = NULL;

	m_uKeep = 0;
	}

// Destructor

CTotalFlowTcpMasterDriver::~CTotalFlowTcpMasterDriver(void)
{
	}

// Configuration

void MCALL CTotalFlowTcpMasterDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CTotalFlowTcpMasterDriver::CheckConfig(CSerialConfig &Config)
{
	}
	
// Management

void MCALL CTotalFlowTcpMasterDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CTotalFlowTcpMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CTotalFlowTcpMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CCtx;

			strcpy(m_pCtx->m_sName, GetString(pData));

			strcpy(m_pCtx->m_sCode, GetString(pData));

			m_pCtx->m_bApp	 = GetByte(pData);

			m_pCtx->m_uEstab = 1;

			m_pCtx->m_IP     = GetAddr(pData);

			m_pCtx->m_uPort  = GetWord(pData);

			m_pCtx->m_fKeep  = GetByte(pData);

			m_pCtx->m_fPing  = GetByte(pData);

			m_pCtx->m_uTime1 = GetWord(pData);

			m_pCtx->m_uTime2 = GetWord(pData);

			m_pCtx->m_uTime3 = GetWord(pData);

			m_pCtx->m_pSock = NULL;

			m_pCtx->m_uLast = GetTickCount();

			CTotalFlowMasterDriver::m_pCtx = m_pCtx;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	CTotalFlowMasterDriver::m_pCtx = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CTotalFlowTcpMasterDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CTotalFlowTcpMasterDriver::Ping(void)
{
	if( m_pCtx->m_fPing ) {
		
		DWORD IP = m_pCtx->m_IP;
		
		if( CheckIP(IP, m_pCtx->m_uTime2) == NOTHING ) {
			
			return CCODE_ERROR; 
			}
		}

	if( OpenSocket() ) {
						
		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

// Socket Management

BOOL CTotalFlowTcpMasterDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CTotalFlowTcpMasterDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP   = (IPADDR const &) m_pCtx->m_IP;

		WORD   Port = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		return FALSE;
		}

	return FALSE;
	}

void CTotalFlowTcpMasterDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort ) {

			m_pCtx->m_pSock->Abort();
			}
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport

BOOL CTotalFlowTcpMasterDriver::CheckLink(void)
{
	return OpenSocket();
	}

void CTotalFlowTcpMasterDriver::AbortLink(void)
{
	CloseSocket(TRUE);
	}

BOOL CTotalFlowTcpMasterDriver::Send(CBuffer *pBuff)
{
	return m_pCtx->m_pSock->Send(pBuff) == S_OK;
	}

UINT CTotalFlowTcpMasterDriver::Recv(UINT uTime)
{
	SetTimer(uTime);

	while( GetTimer() ) {

		if( CheckSocket() ) {

			BYTE bData = 0;

			UINT uSize = 1;

			if( m_pCtx->m_pSock->Recv(&bData, uSize) == S_OK ) {

				if( uSize ) {

					return bData;
					}
				}

			Sleep(5);

			continue;
			}

		break;
		}

	return NOTHING;
	}

// End of File
