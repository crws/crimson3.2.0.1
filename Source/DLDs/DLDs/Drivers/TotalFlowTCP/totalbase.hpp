
//////////////////////////////////////////////////////////////////////////
//
// Support Macros
//

#define	BuffAppend(p, t, v)	BEGIN { *BuffAddTail(p, t) = v; } END

#define	BuffGetNext(p)		((p)->GetData() + (p)->GetSize())

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Driver
//

class CTotalFlowMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CTotalFlowMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Tight Packing
		#pragma pack(1)

		// Supervisory Frame
		struct CSuper
		{
			BYTE	m_bSOH;
			char	m_sName[10];
			BYTE	m_bType;
			BYTE	m_bSlot;
			WORD	m_wCRC;
			};

		// Header Packet
		struct CHeader
		{
			BYTE	m_bType;
			BYTE	m_bRequest;
			BYTE	m_bCount;
			};

		// Record Packet
		struct CRecord
		{
			BYTE	m_bType;
			BYTE	m_bRequest;
			BYTE	m_bApp;
			BYTE	m_bArray;
			WORD	m_wIndex;
			WORD	m_wCount;
			WORD	m_wSpare;
			};

		// Restore Packing
		#pragma pack()

		// Device Config
		struct CCtx
		{
			char    m_sName[16];
			char    m_sCode[8];
			BYTE    m_bApp;
			UINT	m_uEstab;
			UINT	m_uTime2;
			};

		// Current Device
		CCtx * m_pCtx;

		// Buffer Allocation
		CBuffer * AllocBuffer(void);

		// Frame Creation
		void AddInitialSequence(CBuffer *pBuff);
		void AddSupervisoryFrame(CBuffer *pBuff);
		void AddPasswordFrame(CBuffer *pBuff, PCTXT pCode);
		void AddSyncPattern(CBuffer *pBuff);
		void AddHeader(CBuffer *pBuff, BYTE bType, BYTE bRequest, BYTE bCount);
		void AddReadRecord(CBuffer *pBuff, BYTE bApp, BYTE bArray, WORD wIndex, WORD wCount);
		void AddWriteRecord(CBuffer *pBuff, BYTE bApp, BYTE bArray, WORD wIndex, PCBYTE pData, UINT uData);
		void AddSizedFrame(CBuffer *pBuff, PCVOID pData, UINT uData);

		// Frame Checking
		BOOL CheckReadRecord(PCBYTE pData, BYTE bApp, BYTE bArray, WORD wIndex, WORD wCount);

		// Transactions
		BOOL  Transact(CBuffer *pBuff, CSuper &Super, CHeader &Header);
		BOOL  RecvSupervisoryFrame(CSuper &Super);
		BOOL  RecvHeader(CHeader &Header);
		BOOL  RecvSizedFrame(PBYTE pData, UINT uData);
		PBYTE RecvSizedFrame(UINT &uData);

		// Transport
		virtual BOOL CheckLink(void)      = 0;
		virtual void AbortLink(void)      = 0;
		virtual BOOL Send(CBuffer *pBuff) = 0;
		virtual UINT Recv(UINT uTime)     = 0;

		// CRC Helpers
		void Add(CRC16 &crc, PCBYTE pData, UINT uData);
		void Add(CRC16 &crc, PCVOID pFrom, PCVOID pEnd);
	};

// End of File
