
#include "intern.hpp"

#include "gem80.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Base Master Driver
//

// Constructor

CGem80MasterDriver::CGem80MasterDriver(void)
{
	m_uRxSize = sizeof(m_bRxBuff);

	m_uTxSize = sizeof(m_bTxBuff);

	memset(m_bTxBuff, 0, m_uTxSize);

	memset(m_bRxBuff, 0, m_uRxSize);

	}

// Destructor

CGem80MasterDriver::~CGem80MasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CGem80MasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 'E';
	Addr.a.m_Offset = 0x0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);

	}

CCODE MCALL CGem80MasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
       	if( Start() ) {

		AddByte('S');
	
		AddByte(BYTE(Addr.a.m_Table));

		AddWord(Addr.a.m_Offset);

		UINT uType = Addr.a.m_Type;

		switch( uType ) {

			case addrWordAsWord:

				return DoWordRead(pData, uCount, uType);

			case addrWordAsLong:
			case addrRealAsReal:

				return DoLongRead(pData, uCount, uType);
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CGem80MasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Start() ) {

		AddByte('X');
	
		AddByte(BYTE(Addr.a.m_Table));

		AddWord(Addr.a.m_Offset);

		UINT uType = Addr.a.m_Type;

		switch( uType ) {

			case addrWordAsWord:

				return DoWordWrite(pData, uCount, uType);

			case addrWordAsLong:
			case addrRealAsReal:

				return DoLongWrite(pData, uCount, uType);

			}
		}

	return CCODE_ERROR;

	}

// Implementation

BOOL CGem80MasterDriver::Start(void)
{
	m_uPtr = 0;

	return TRUE;
	}

BOOL CGem80MasterDriver::Transact(BOOL fWrite) 
{
	return FALSE;
	}
	 

void CGem80MasterDriver::AddCount(UINT &uCount, UINT uType)
{

	}

void CGem80MasterDriver::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {
	
		m_bTxBuff[m_uPtr++] = bData;
		}
	}

void CGem80MasterDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CGem80MasterDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

// Read Handlers

CCODE CGem80MasterDriver::DoWordRead(PDWORD pData, UINT uCount, UINT uType)
{
	AddCount(uCount, uType);

	if( Transact(FALSE) ) {

		for( UINT u = 0; u < uCount; u++ ) { 

			WORD x = PWORD(m_bRxBuff)[u];
			
			pData[u] = LONG(SHORT(IntelToHost(x)));

			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CGem80MasterDriver::DoLongRead(PDWORD pData, UINT uCount, UINT uType)
{
	AddCount(uCount, uType);
	
	if( Transact(FALSE) ) {
	
		for( UINT u = 0; u < uCount; u++ ) { 
			
			DWORD x = PDWORD(m_bRxBuff)[u];
			
			pData[u] = IntelToHost(x);
			}
       
		return uCount;
		}

	return CCODE_ERROR;
	}


// Write Handlers

CCODE CGem80MasterDriver::DoWordWrite(PDWORD pData, UINT uCount, UINT uType)
{
	AddCount(uCount, uType);

	for( UINT u = 0; u < uCount; u++ ) {

		AddWord(pData[u]);
		}
							
	if( Transact(TRUE) ) {
		
		return uCount;
		}
	
	return CCODE_ERROR;
	}

CCODE CGem80MasterDriver::DoLongWrite(PDWORD pData, UINT uCount, UINT uType)
{
	AddCount(uCount, uType);

	for( UINT u = 0; u < uCount; u++ ) {

		AddLong(pData[u]);
		}
				
	if( Transact(TRUE) ) {
	
		return uCount; 
		}

	return CCODE_ERROR;
	
	}


//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Base Slave Driver
//

// Constructor

CGem80SlaveDriver::CGem80SlaveDriver(void)
{
	m_bDrop = 0;

	m_uRxSize = sizeof(m_bRxBuff);

	m_uTxSize = sizeof(m_bTxBuff);

	}

// Destructor

CGem80SlaveDriver::~CGem80SlaveDriver(void)
{
	}

// Entry Points

void MCALL CGem80SlaveDriver::Service(void)
{
	
	}

// Implementation

void CGem80SlaveDriver::Send(BYTE bData)
{
	
	} 

// End of File
