
#include "intern.hpp"

#include "gem80m.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 PLC Driver
//

// Instantiator

INSTANTIATE(CGem80MasterSerialDriver);

// Constructor

CGem80MasterSerialDriver::CGem80MasterSerialDriver(void)
{
	m_Ident         = DRIVER_ID;

	}

// Destructor

CGem80MasterSerialDriver::~CGem80MasterSerialDriver(void)
{
	}

// Configuration

void MCALL CGem80MasterSerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CGem80MasterSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CGem80MasterSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CGem80MasterSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CGem80MasterSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_bDrop	= GetByte(pData);

			m_pCtx->m_fToggle = FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;

	}

CCODE MCALL CGem80MasterSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Implementation

void CGem80MasterSerialDriver::TxPacket(BYTE bMode)
{
	Send(PAD);

	Send(STX);
	
	m_CRC.Clear();
	
	Send((m_pCtx->m_bDrop << 4) | bMode);
	
	for( UINT uScan = 0; uScan < m_uPtr; uScan++ ) {

		switch( m_bTxBuff[uScan] ) {
		
			case ENQ:
			case STX:
			case ETX:
			case ETB:
			case EOT:
			case DLE:
				Send(DLE);

			default:
				Send( m_bTxBuff[uScan] );
			}
		}

	m_bTerm = m_pCtx->m_fToggle ? ETX : ETB;
	
	Send(m_bTerm);

	m_pCtx->m_fToggle = !m_pCtx->m_fToggle;
		
	UINT uTxCRC = m_CRC.GetValue();
	
	Send(uTxCRC % 256);

	Send(uTxCRC / 256);

	}

UINT CGem80MasterSerialDriver::RxPacket(void)
{
	UINT uTarget = 0;
	
	UINT uRxCRC = 0;

	BOOL fDLE = FALSE;

	UINT uRetry = 3;

	UINT uState = 0; 

	UINT uTimer = 0;

	UINT uByte = 0;
	
	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

			
		m_CRC.Add(uByte);
			
		if( !fDLE && uByte == DLE ) {

			if( uState == 0 || uState == 2 ) {

				fDLE = TRUE;

				continue;
				}
			}
			
		switch( uState ) {
		
			case 0:
				switch( uByte ) {
				
					case STX:
						m_CRC.Clear();

						uState = 1;

						break;
					
					case NAK:

						return RX_NAK;
						
					case ACK:

						uState = 5;

						break;
					}
				break;
				
			case 1:
				if( (uByte & 0xF0) == (UINT(m_pCtx->m_bDrop << 4)) ) {
			
					m_uPtr = 0;
		
					uState = 2;
					}
				else {
					if( uRetry-- ) {

						m_pData->Write(ENQ, FOREVER);
	
						uState = 0;
	
						fDLE = FALSE;
						}
					else {
						return RX_ERROR;
						}
					}
				break;
				
			case 2:
				if( !fDLE ) {

					if( uByte == ETX || uByte == ETB ) {

						if( uByte != m_bTerm ) {

							return RX_ERROR;
							}
						else {
							uTarget = m_CRC.GetValue();

							uState = 3;
							
							break;
							}
						}
					}

				if( m_uPtr < m_uRxSize ) {

					m_bRxBuff[m_uPtr++] = uByte;
					}
				else {
					return RX_ERROR;
					}
				break;
				
			case 3:
				uRxCRC = uByte;

				uState = 4;

				break;
				
			case 4:
				if( uTarget == MAKEWORD(uRxCRC, uByte) ) {

					return RX_FRAME;
					}
					
				if( !uRetry-- )	{

					return RX_ERROR;
					}
				else {
					m_pData->Write(ENQ, FOREVER);
					
					uState = 0;
					
					fDLE = FALSE;
					}
				break;
				
			case 5:
				if( uByte == m_bTerm ) {

					return RX_ACK;
					}
				else {
					return RX_ERROR;
					}
				break;
			}
		
		fDLE = FALSE;
		}
		
	return RX_ERROR;
	}

void CGem80MasterSerialDriver::Send(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	
	m_CRC.Add(bData);

	}

BOOL CGem80MasterSerialDriver::Transact(BOOL fWrite)
{
	TxPacket(0x00); 
	
	UINT uReply = RxPacket();
	
	if( uReply != RX_FRAME && uReply != RX_ACK ) {

		return FALSE;
		}

	UINT uDataSize = sizeof(WORD) * m_uCount;

	if ( m_uPtr != uDataSize && m_bRxBuff[0] != '?' && uReply != RX_ACK ) {

		return FALSE;
		}

	return TRUE;
	}

void CGem80MasterSerialDriver::AddCount(UINT &uCount, UINT uType)
{
	if( uType == addrWordAsWord ) {

		MakeMin(uCount, 32);

		AddByte(BYTE(uCount));

		m_uCount = uCount;

		return;
		}

	MakeMin(uCount, 16);

	AddByte(BYTE(uCount * 2));

	m_uCount = uCount;
	
	}
 // End of File
