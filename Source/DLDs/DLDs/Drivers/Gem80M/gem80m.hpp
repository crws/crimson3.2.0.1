#include "gem80.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Master Serial Driver
//

class CGem80MasterSerialDriver : public CGem80MasterDriver
{
	public:
		// Constructor
		CGem80MasterSerialDriver(void);

		// Destructor
		~CGem80MasterSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
	protected:
		// Device Context
		struct CContext : CGem80MasterDriver::CBaseCtx
		{
			};

		// Data Members
		CContext * m_pCtx;
		CRC16	   m_CRC;
		UINT	   m_uCount;
				
		// Implementation 
		void TxPacket(BYTE bMode);
		UINT RxPacket(void);
		void Send(BYTE bData);
		BOOL Transact(BOOL fWrite);
		void AddCount(UINT &uCount, UINT uType);

	};

// End of File
