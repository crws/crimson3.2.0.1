
////////////////////////////////////////////////////////////////////////
//
// MP Electronics Display Driver
//

class CMPEDriver : public CDisplayDriver 
{
	public:
		// Constructor
		CMPEDriver(void);

		// Destructor
		~CMPEDriver(void);

		// Configuration
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Entry Points
		DEFMETH(UINT) GetSize(int cx, int cy);
		DEFMETH(void) Capture(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait);
		DEFMETH(void) Service(UINT uDrop, PBYTE pPrev, PBYTE pData, UINT uSize);

	protected:
		// Data
		UINT		 m_uType;
		IDisplayHelper * m_pDisplayHelper;
		BYTE		 m_bTxData[4096];
		BYTE		 m_bError;
		UINT		 m_uPtr;
		WORD		 m_wCheck;
		BOOL		 m_fInit;
		BOOL		 m_fFlip;

		// Packet Building
		void NewPacket(BYTE bType);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddData(PCBYTE pData, UINT uSize);
		void AddText(PCTXT pText);
		void AddZero(UINT uSize);
		BOOL SendPacket(void);

		// Ack Reception
		BOOL GetAck(void);

		// Commands
		BOOL StopDisplay(void);
		BOOL SendBitmap(PCTXT pName, PCBYTE pData, int cx, int cy);
		BOOL SendProgram(PCTXT pName, PCBYTE pData, UINT uSize);
		BOOL SendExecute(PCTXT pName);

		// Sequences
		BOOL DoInit(void);
		BOOL DoSend(PCBYTE pData);

		// Implementaiton
		void GetHelp(void);
		void CaptureVGA (PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize);
		void CaptureMono(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize);
	};

// End of File
