
#include "intern.hpp"

#include "mpe.hpp"

////////////////////////////////////////////////////////////////////////
//
// MP Electronics Display Driver
//

// Instantiator

INSTANTIATE(CMPEDriver);

// Constructor

CMPEDriver::CMPEDriver(void)
{
	m_Ident          = DRIVER_ID;

	m_pDisplayHelper = NULL;
	}

// Destructor

CMPEDriver::~CMPEDriver(void)
{
	m_pDisplayHelper = NULL;
	}

// Configuration
	
void MCALL CMPEDriver::CheckConfig(CSerialConfig &Config)
{
	}

// Management

void MCALL CMPEDriver::Attach(IPortObject *pPort)
{
	if( pPort ) {

		m_pData = MakeSingleDataHandler();

		pPort->Bind(m_pData);
		}
	}

void MCALL CMPEDriver::Detach(void)
{
	if( m_pData ) {
	
		m_pData->Release();

		m_pData = NULL;
		}
	}

void MCALL CMPEDriver::Open(void)
{
	GetHelp();

	m_fInit = FALSE;

	m_fFlip = FALSE;
	}

// Entry Points

UINT MCALL CMPEDriver::GetSize(int cx, int cy)
{
	return 2 * ((cx * cy + 7) / 8) + 2 * sizeof(INT);
	}

void MCALL CMPEDriver::Capture(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait)
{
	switch( m_uType ) {

		case 0:

			CaptureMono(pDest, pSrc, px, py, cx, cy, uSize);

			break;

		case 1 :

			CaptureVGA (pDest, pSrc, px, py, cx, cy, uSize);

			break;
		}
	}

void MCALL CMPEDriver::Service(UINT uDrop, PBYTE pPrev, PBYTE pData, UINT uSize)
{
	if( memcmp(pPrev, pData, uSize) ) {

		if( !m_fInit ) {

			if( !DoInit() ) {

				return;
				}
			}

		if( !DoSend(pData) ) {

			m_fInit = FALSE;
			}
		}
	}

// Packet Building

void CMPEDriver::NewPacket(BYTE bType)
{
	m_bError = 0xFF;

	m_uPtr   = 0;

	m_wCheck = 0;

	AddByte(0x16);

	AddByte(0x00);

	AddByte(0x00);

	AddByte(0x01);

	AddByte(bType);
	}

void CMPEDriver::AddByte(BYTE bData)
{
	m_bTxData[m_uPtr++] = bData;

	m_wCheck = m_wCheck + bData;
	}

void CMPEDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CMPEDriver::AddData(PCBYTE pData, UINT uSize)
{
	while( uSize-- ) AddByte(*pData++);
	}

void CMPEDriver::AddText(PCSTR pText)
{
	while( *pText ) AddByte(*pText++);
	}

void CMPEDriver::AddZero(UINT uSize)
{
	while( uSize-- ) AddByte(0);
	}

BOOL CMPEDriver::SendPacket(void)
{
	m_bTxData[1] = LOBYTE(m_uPtr+2);

	m_bTxData[2] = HIBYTE(m_uPtr+2);

	m_wCheck += m_bTxData[1];

	m_wCheck += m_bTxData[2];

	m_bTxData[m_uPtr++] = LOBYTE(m_wCheck);

	m_bTxData[m_uPtr++] = HIBYTE(m_wCheck);

	m_pData->Write(m_bTxData, m_uPtr, 0);

	return TRUE;
	}

// Ack Reception

BOOL CMPEDriver::GetAck(void)
{
	SetTimer(5000);

	UINT uState = 0;

	while( GetTimer() ) {

		UINT uData = m_pData->Read(20);

		if( uData == NOTHING ) {

			continue;
			}

		switch( uState ) {

			case 0:
				if( uData == 0x06 ) {

					uState = 1;
					}
				break;

			case 1:
				m_bError = uData;

				return !m_bError;
			}
		}

	return FALSE;
	}

// Commands

BOOL CMPEDriver::StopDisplay(void)
{
	NewPacket(0x03);

	SendPacket();

	return GetAck();
	}

BOOL CMPEDriver::SendBitmap(PCSTR pName, PCBYTE pData, int cx, int cy)
{
	NewPacket(0x16);

	AddText(pName);

	AddWord(32 + 2 * cx * (cy + 7) / 8);

	AddWord(0);

	AddByte(0x00);

	SendPacket();

	if( GetAck() ) {

		NewPacket(0x0C);

		AddByte(0x00);		// Type
		AddByte(0x00);		// Compression
		AddWord(0x00);		// X Pos
		AddWord(0x00);		// Y Pos
		AddWord(cx);		// Width
		AddWord(cy);		// Height
		AddWord(1);		// Frames
		AddWord(0);		// Convert
		AddZero(3);		// Padding
		AddByte(2);		// Pages
		AddZero(14);		// Padding

		AddData(pData, 2 * cx * (cy + 7) / 8);

		SendPacket();

		return GetAck();
		}

	return FALSE;
	}

BOOL CMPEDriver::SendProgram(PCSTR pName, PCBYTE pData, UINT uSize)
{
	NewPacket(0x06);

	AddText(pName);

	SendPacket();

	if( GetAck() ) {

		NewPacket(0x0C);

		AddData(pData, uSize);

		SendPacket();

		return GetAck();
		}

	return FALSE;
	}

BOOL CMPEDriver::SendExecute(PCSTR pName)
{
	NewPacket(0x1F);

	AddText(pName);

	SendPacket();

	return GetAck();
	}

// Sequences

BOOL CMPEDriver::DoInit(void)
{
	if( StopDisplay() ) {

		static BYTE const p0[] = {	0xCE, '*', 'B', 'I', 'T', 'M', 'A', 'P', '0',
						',', '1', ',', '6', '4',
						0xC8, '$', 'S', 'T', 'O', 'P'
						};

		static BYTE const p1[] = {	0xCE, '*', 'B', 'I', 'T', 'M', 'A', 'P', '1',
						',', '1', ',', '6', '4',
						0xC8, '$', 'S', 'T', 'O', 'P'
						};

		if( SendProgram("PROGRAM0", p0, sizeof(p0)) ) {

			if( SendProgram("PROGRAM1", p1, sizeof(p1)) ) {

				m_fInit = TRUE;

				m_fFlip = FALSE;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CMPEDriver::DoSend(PCBYTE pData)
{
	int cx = PINT(pData)[0];

	int cy = PINT(pData)[1];

	pData += 2 * sizeof(INT);

	for(;;) {

		if( SendBitmap(m_fFlip ? "*BITMAP1" : "*BITMAP0", pData, cx, cy) ) {

			if( SendExecute(m_fFlip ? "PROGRAM1" : "PROGRAM0") ) {

				m_fFlip = !m_fFlip;

				return TRUE;
				}

			return FALSE;
			}

		if( m_bError == 0x02 ) {

			Sleep(20);

			continue;
			}

		return FALSE;
		}
	}

// Implementation

void CMPEDriver::GetHelp(void)
{
	if( !m_pDisplayHelper ) {
		
		if( MoreHelp(IDH_DISPLAY, (void **) &m_pDisplayHelper) ) {

			switch( m_pDisplayHelper->GetType() ) {

				case 0:
					m_uType = 0;
					break;

				case 1:
				case 2:
				case 3:
					m_uType = 1;
					break;
				}
			}
		}
	}

void CMPEDriver::CaptureVGA(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize)
{
	((PINT) pDest)[0] = cx;

	((PINT) pDest)[1] = cy;

	pDest += 2 * sizeof(INT);

	int c = (cy + 7) / 8;

	int p = (cx * c);

	pSrc += px + py * uSize;

	for( int r = 0; r < c; r++ ) {

		for( int x = 0; x < cx; x++ ) {

			BYTE br = 0;
			
			BYTE bg = 0;

			if( pSrc[x+0*uSize] & 1 ) br |= (1<<7);
			if( pSrc[x+1*uSize] & 1 ) br |= (1<<6);
			if( pSrc[x+2*uSize] & 1 ) br |= (1<<5);
			if( pSrc[x+3*uSize] & 1 ) br |= (1<<4);
			if( pSrc[x+4*uSize] & 1 ) br |= (1<<3);
			if( pSrc[x+5*uSize] & 1 ) br |= (1<<2);
			if( pSrc[x+6*uSize] & 1 ) br |= (1<<1);
			if( pSrc[x+7*uSize] & 1 ) br |= (1<<0);

			if( pSrc[x+0*uSize] & 2 ) bg |= (1<<7);
			if( pSrc[x+1*uSize] & 2 ) bg |= (1<<6);
			if( pSrc[x+2*uSize] & 2 ) bg |= (1<<5);
			if( pSrc[x+3*uSize] & 2 ) bg |= (1<<4);
			if( pSrc[x+4*uSize] & 2 ) bg |= (1<<3);
			if( pSrc[x+5*uSize] & 2 ) bg |= (1<<2);
			if( pSrc[x+6*uSize] & 2 ) bg |= (1<<1);
			if( pSrc[x+7*uSize] & 2 ) bg |= (1<<0);

			pDest[0*p] = br;

			pDest[1*p] = bg;

			pDest++;
			}

		pSrc += 8 * uSize;
		}
	}

void CMPEDriver::CaptureMono(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize)
{
	((PINT) pDest)[0] = 0;

	((PINT) pDest)[1] = 0;
	}

// End of File
