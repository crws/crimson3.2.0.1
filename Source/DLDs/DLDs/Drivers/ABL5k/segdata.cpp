
#include "intern.hpp"

#include "segment.hpp"

#include "abl5k.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Data
//

// Constructor

CSegData::CSegData(void)
{
	SetType(segData);
	
	m_Data.p = NULL;

	m_uSize  = 0;
	}

// Destructor

CSegData::~CSegData(void)
{
	Free();
	}

// Operations

void CSegData::SetSimple(PBYTE pData, UINT uSize)
{
	switch( uSize ) {

		case 0:
			return;

		case sizeof(BYTE):			
			m_Data.b = *pData;
			break;

		case sizeof(WORD):
			m_Data.w = *PWORD(pData);
			break;

		case sizeof(DWORD):
			m_Data.l = *PDWORD(pData);
			break;

		default:
			if( uSize < 255 ) {

				Free();

				m_uSize = uSize;

				m_Data.p = new BYTE [m_uSize];

				memcpy(m_Data.p, pData, m_uSize);

				break;
				}
			return;
		}

	SetFormat(dataSimple);
	}

void CSegData::SetSymbol(PCTXT pSymbol)
{
	SetSymbol(PBYTE(pSymbol), strlen(pSymbol));
	}

void CSegData::SetSymbol(PBYTE pData, UINT uSize)
{
	if( uSize < 255 ) {

		Free();

		m_uSize = uSize;

		m_Data.p = new BYTE [m_uSize];

		memcpy(m_Data.p, pData, m_uSize);

		SetFormat(dataSymbol);
		}
	}

// Encoding

UINT CSegData::GetLength(void) const
{
	if( GetFormat() == dataSimple ) {

		UINT uLen = CSegment::GetLength();
		
		return uLen;
		}

	if( GetFormat() == dataSymbol ) {

		UINT uLen = CSegment::GetLength();

		uLen += 1;

		uLen += m_uSize;

		uLen += UsePadding() ? 1 : 0;

		return uLen;
		}

	return 0;
	}

UINT CSegData::Encode(CDataBuf &Buff)
{
	switch( GetFormat() ) {
		
		case dataSimple:

			return EncodeSimple(Buff);

		case dataSymbol:

			return EncodeSymbol(Buff);

		default:
			return 0;
		}
	}

// Encoding Help

UINT CSegData::EncodeSimple(CDataBuf &Buff)
{
	// TODO : for completeness

	CSegment::Encode(Buff);

	return GetLength();
	}

UINT CSegData::EncodeSymbol(CDataBuf &Buff)
{
	CSegment::Encode(Buff);

	Buff.AddByte(BYTE(m_uSize));

	Buff.AddData(m_Data.p, m_uSize);

	if( UsePadding() ) {

		Buff.AddByte(0);
		}

	return GetLength();
	}

// Implementation

void CSegData::Free(void)
{
	if( m_Data.p ) {

		delete m_Data.p;
		
		m_Data.p = NULL;
		}

	m_uSize = 0;
	}

BOOL CSegData::UsePadding(void) const
{
	if( GetPadded() ) {

		if( GetFormat() == dataSymbol ) {

			if( m_uSize & 1 ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// End of File
