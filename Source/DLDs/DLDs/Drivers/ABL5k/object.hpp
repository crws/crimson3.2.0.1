
#ifndef	INCLUDE_OBJECT_HPP

#define	INCLUDE_OBJECT_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "abl5k.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Service Codes
//

#define	CIP_READ	0x4C

#define	CIP_WRITE	0x4D

//////////////////////////////////////////////////////////////////////////
//
// IOI Segment
//

class CIOISegment : public CCIPPath
{
	public:
		// Constructor
		CIOISegment(void);

		// Operations
		void BuildPath (PCTXT pText);
		void AddElement(UINT uIndex);

	protected:
		// Implementation
		void AddSymbol(PBYTE pData, UINT uLen);
		UINT ToInt(PBYTE pData, UINT uLen);
		UINT GetDecChar(BYTE bData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Base CIP Object
//

class CCIPObject
{
	public:
		// Constructor
		CCIPObject(void);

		// Service
		BOOL Service(BYTE bCode, CIOISegment &Name, CDataBuf &Data, CBuffer * &pRecv);
		BYTE GetLastError(void);

	public:
		// Data
		IEIPMaster * m_pDriver;
	};

//////////////////////////////////////////////////////////////////////////
//
// Logix Data Base Object
//

class CLogixData : public CCIPObject
{
	public:
		// Constructor
		CLogixData(void);

		// Object Specific Services
		virtual CCODE ReadData (PDWORD pData, UINT uIndex, UINT uCount);
		virtual CCODE WriteData(PDWORD pData, UINT uIndex, UINT uCount);

	public:
		// Data
		PCTXT m_pName;
		UINT  m_uType;
	};

//////////////////////////////////////////////////////////////////////////
//
// Logix Table Data Object
//

class CLogixTableData : public CLogixData
{
	public:
		// Constructor
		CLogixTableData(void);

		// Destructor
		~CLogixTableData(void);

		// Object Specific Services
		CCODE ReadData (PDWORD pData, UINT uIndex, UINT uCount);
		CCODE WriteData(PDWORD pData, UINT uIndex, UINT uCount);

	public:
		// Data
		UINT  m_uSize;
		PUINT m_pDims;
	};

//////////////////////////////////////////////////////////////////////////
//
// Logix Data Named Object
//

class CLogixNamedData : public CLogixData
{
	public:
		// Constructor
		CLogixNamedData(void);

		// Object Specific Services
		CCODE ReadData (PDWORD pData, UINT uIndex, UINT uCount);
		CCODE WriteData(PDWORD pData, UINT uIndex, UINT uCount);
	};

//////////////////////////////////////////////////////////////////////////
//
// CIPADDR Wrapper Class
//

class CCIPAddr : public CIPADDR
{
	public:
		// Constructor
		CCIPAddr(void);

		// Constructor
		~CCIPAddr(void);
	};

// End of File

#endif
