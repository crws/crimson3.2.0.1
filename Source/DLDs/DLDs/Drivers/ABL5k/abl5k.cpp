
#include "intern.hpp"

#include "segment.hpp"

#include "abl5k.hpp"

#include "object.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Logix5000 Driver - EIP Master
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CABL5kDriver);

// Constructor

CABL5kDriver::CABL5kDriver(void)
{
	m_Ident       = DRIVER_ID;

	m_fOpen       = FALSE;

	m_pEnetHelper = NULL;

	m_pExplicit   = NULL;

	m_pCtx        = NULL;
}

// Management

void MCALL CABL5kDriver::Attach(IPortObject *pPort)
{
	if( !m_pEnetHelper && !m_fOpen ) {

		if( MoreHelp(IDH_ENETIP, (void **) &m_pEnetHelper) ) {

			m_fOpen = m_pEnetHelper->Open();
			}
		}
	}

void MCALL CABL5kDriver::Detach(void)
{
	if( m_pEnetHelper ) {

		m_pEnetHelper->Close();

		m_pEnetHelper->Release();

		m_pEnetHelper = NULL;

		m_pExplicit   = NULL;

		m_fOpen       = FALSE;
	}
}

// Device

CCODE MCALL CABL5kDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx              = new CContext;

			GetByte(pData);
			m_pCtx->m_IP	    = GetAddr(pData);
			m_pCtx->m_wPort	    = GetWord(pData);
			m_pCtx->m_wSlot	    = GetWord(pData);
			m_pCtx->m_wTimeout  = GetWord(pData);
			m_pCtx->m_pPath     = GetString(pData);

			m_pCtx->m_Named.Load(pData, ThisObject);

			m_pCtx->m_Table.Load(pData, ThisObject);

			FindPing();

			pDevice->SetContext(m_pCtx);

			return MakeLink() ? CCODE_SUCCESS : CCODE_ERROR;
			}

		return CCODE_ERROR | CCODE_HARD;
	}

	return MakeLink() ? CCODE_SUCCESS : CCODE_ERROR;
	}

CCODE MCALL CABL5kDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

UINT MCALL CABL5kDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) {

		// Set IP Address

		BYTE b[4] = { 0 };

		char o[4] = { 0 };

		PCTXT   p = Value;

		for( UINT n = 0; n < elements(b); p++ ) {

			UINT i = 0;

			while( !IsDelim(*p) ) {

				if( !IsDigit(*p) ) {

					return 0;
				}

				if( i > 3 ) {

					return 0;
				}

				o[i++] = *p;

				p++;
			}

			o[i] = '\0';

			DWORD Data = ATOI(o);

			if( !IsOctet(Data) ) {

				return 0;
			}

			b[n++] = Data;
		}

		pCtx->m_IP = (DWORD &) b;

		return 1;
	}

	if( uFunc == 2 ) {

		// Get IP Address

		return MotorToHost(pCtx->m_IP);
	}

	return 0;
}

// Entry Points

CCODE MCALL CABL5kDriver::Ping(void)
{
	if( CheckLink() ) {

		DWORD dwData = 0;

		return m_pCtx->m_pPing ? m_pCtx->m_pPing->ReadData(&dwData, 0, 1) : CCODE_SUCCESS;
	}

	return CCODE_ERROR;
}

void MCALL CABL5kDriver::Service(void)
{
}

CCODE MCALL CABL5kDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		CLogixData * pObj = FindDataObject(Addr);

		if( pObj ) {

			if( AddrIsTable(Addr) ) {

				UINT uIndex = AddrDataIndex(Addr);

				UINT  uType = Addr.a.m_Type;

				switch( uType ) {

					case addrBitAsLong:

						uIndex /= 32;

						break;

					case addrLongAsLong:
					case addrLongAsReal:
					case addrRealAsReal:

						MakeMin(uCount, 120);

						break;

					default:

						break;
				}

				MakeMin(uCount, 240);

				return pObj->ReadData(pData, uIndex, uCount);
			}

			if( AddrIsNamed(Addr) ) {

				UINT uIndex = 0;

				return pObj->ReadData(pData, uIndex, uCount);
			}
		}
	}

	return CCODE_ERROR;
}

CCODE MCALL CABL5kDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		CLogixData * pObj = FindDataObject(Addr);

		if( pObj ) {

			if( AddrIsTable(Addr) ) {

				UINT uIndex = AddrDataIndex(Addr);

				UINT  uType = Addr.a.m_Type;

				switch( uType ) {

					case addrBitAsLong:

						uIndex /= 32;

						break;

					default:

						break;
				}

				MakeMin(uCount, 64);

				return pObj->WriteData(pData, uIndex, uCount);
			}

			if( AddrIsNamed(Addr) ) {

				return pObj->WriteData(pData, 0, 1);
			}
		}
	}

	return CCODE_ERROR;
}

// I5EIPMaster

BOOL MCALL CABL5kDriver::Send(BYTE bCode, CCIPPath &Path, CDataBuf &Buffer)
{
	PBYTE pData = Buffer.GetDataPtr();

	UINT uSize  = Buffer.GetSize();

	return Send(bCode, Path, EIP_EMPTY, EIP_EMPTY, pData, uSize);
}

BOOL MCALL CABL5kDriver::Send(BYTE bCode, CCIPPath &Path, WORD wClass, WORD wInst, PBYTE pData, UINT uSize)
{
	CCIPAddr Addr;

	Addr.m_ip	= (IPADDR const &) m_pCtx->m_IP;

	Addr.m_wPort	= m_pCtx->m_wPort;

	Addr.m_wSlot	= m_pCtx->m_wSlot;

	Addr.m_pPath    = m_pCtx->m_pPath;

	Addr.m_bService = bCode;

	Addr.m_wClass	= wClass;

	Addr.m_wInst	= wInst;

	if( Path.GetLength() > 1 ) {

		CDataBuf Data;

		Data.Encode(Path);

		Addr.m_bTagData = Data.GetDataPtr();

		Addr.m_uTagSize = Data.GetSize();

		//Dump(Addr.m_bTagData, Addr.m_uTagSize);
	}

	m_bError = 0;

	m_pExplicit->SetTimeout(m_pCtx->m_wTimeout);

	return m_pExplicit->Send(Addr, pData, uSize);
}

BOOL MCALL CABL5kDriver::Recv(CBuffer * &pBuff)
{
	if( m_pExplicit->Recv(pBuff) ) {

		if( pBuff ) {

			UINT  uSize = pBuff->GetSize();

			PBYTE pData = pBuff->GetData();

			//Dump(pData, uSize);

			return TRUE;
		}

		m_bError = 0;

		return FALSE;
	}

	m_bError = m_pExplicit->GetLastError();

	return FALSE;
}

BYTE MCALL CABL5kDriver::GetLastError(void)
{
	return m_bError;
}

void MCALL CABL5kDriver::Dump(CBuffer *pBuff)
{
	UINT  uSize = pBuff->GetSize();

	PBYTE pData = pBuff->GetData();

	Dump(pData, uSize);
}

void MCALL CABL5kDriver::Dump(PBYTE pData, UINT uSize)
{
	for( UINT n = 0; n < uSize; n++ ) {

		if( n && !(n % 16) )
			AfxTrace("\n");

		AfxTrace("%2.2X ", pData[n]);
	}

	AfxTrace("\n");
}

void MCALL CABL5kDriver::Trace(LPCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	::AfxTrace(pText, pArgs);

	va_end(pArgs);
}

// Text Loading

PTXT CABL5kDriver::GetString(PCBYTE &pData)
{
	WORD wLength = GetWord(pData);

	PTXT String = PTXT(Alloc(wLength + 1));

	memset(String, 0, wLength + 1);

	for( UINT n = 0; n < wLength; n++ ) {

		String[n] = GetByte(pData);
	}

	return String;
}

// Transport Layer

BOOL CABL5kDriver::MakeLink(void)
{	
	if( m_fOpen ) {

		m_pExplicit = m_pEnetHelper->GetExplicit();
		}

	return CheckLink();
	}

BOOL CABL5kDriver::CheckLink(void)
{
	return m_fOpen && m_pExplicit;
}

// Logix Data Object Support

CLogixData * CABL5kDriver::FindDataObject(AREF Addr)
{
	if( AddrIsTable(Addr) ) {

		UINT uIndex = AddrTagIndex(Addr) - 1;

		return m_pCtx->m_Table.FindObject(uIndex);
	}

	if( AddrIsNamed(Addr) ) {

		UINT uIndex = AddrTagIndex(Addr) - 1;

		return m_pCtx->m_Named.FindObject(uIndex);
	}

	return NULL;
}

// Implementation

void CABL5kDriver::FindPing(void)
{
	if( (m_pCtx->m_pPing = m_pCtx->m_Named.FindObject(0)) ) {

		return;
	}

	if( (m_pCtx->m_pPing = m_pCtx->m_Table.FindObject(0)) ) {

		return;
	}

	m_pCtx->m_pPing = NULL;
}

// Parsing Help

BOOL CABL5kDriver::IsDelim(char c)
{
	return c == '.' || c == 0;
}

BOOL CABL5kDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
}

BOOL CABL5kDriver::IsOctet(DWORD d)
{
	return d <= 255;
}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Logix5000 Driver - Tag Name List
//

// Constructor

CNameList::CNameList(void)
{
	m_uCount = 0;

	m_pList  = NULL;
}

// Destructor

CNameList::~CNameList(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		delete m_pList[n];
	}

	delete m_pList;
}

// Configuration

void CNameList::Load(LPCBYTE &pData, CABL5kDriver &Driver)
{
	m_uCount = Driver.GetWord(pData);

	m_pList = new CLogixData *[m_uCount];

	//Driver.Trace("load %d names\n", m_uCount);

	for( UINT n = 0; n < m_uCount; n++ ) {

		PTXT pName = Driver.GetString(pData);

		UINT uSize = Driver.GetByte(pData);

		//Driver.Trace("load <%s>\n", pName);

		if( uSize > 0 ) {

			CLogixTableData *pLogix = new CLogixTableData;

			pLogix->m_pDriver = &Driver;

			pLogix->m_pName = pName;

			pLogix->m_uSize = uSize;

			pLogix->m_pDims = new UINT[uSize];

			for( UINT m = 0; m < uSize; m++ ) {

				pLogix->m_pDims[m] = Driver.GetWord(pData);
			}

			m_pList[n] = pLogix;
		}
		else {
			CLogixNamedData *pLogix = new CLogixNamedData;

			pLogix->m_pDriver = &Driver;

			pLogix->m_pName = pName;

			m_pList[n] = pLogix;
		}
	}
}

// Attributes		

CLogixData * CNameList::FindObject(UINT uIndex)
{
	if( uIndex < m_uCount ) {

		return m_pList[uIndex];
	}

	return NULL;
}

// End of File
