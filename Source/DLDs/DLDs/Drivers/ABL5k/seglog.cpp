
#include "intern.hpp"

#include "segment.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Logical
//

// Constructor

CSegLogical::CSegLogical(void)
{
	SetType(segLogical);

	m_dwValue = 0;
	}

CSegLogical::CSegLogical(UINT uType, DWORD dwValue)
{
	SetType(segLogical);

	SetLogType(uType);
	
	SetLogValue(dwValue);
	}

// Operations

void CSegLogical::SetLogType(UINT uType)
{
	m_bType &= ~0x1C;

	m_bType |= (0x1C & (uType << 2));
	}

void CSegLogical::SetLogValue(DWORD dwValue)
{
	m_dwValue = dwValue;

	SetLogFormat(FindLogFormat());
	}

void CSegLogical::SetLogFormat(UINT uType)
{
	m_bType &= ~0x03;

	m_bType |= (0x03 & (uType << 0));
	}

// Operations

UINT CSegLogical::GetLogType(void) const
{
	return (GetFormat() >> 2) & 0x07;
	}

UINT CSegLogical::GetLogFormat(void) const
{
	return (GetFormat() >> 0) & 0x03;
	}

// Encoding

UINT CSegLogical::GetLength(void) const
{
	switch( GetLogFormat() ) {
		
		case addr8bit:
			return 2;
		
		case addr16bit:
			return UsePadding() ? 4 : 3;
		
		case addr32bit:
			return UsePadding() ? 6 : 5;

		default:
			return 0;
		}
	}

UINT CSegLogical::Encode(CDataBuf &Buff)
{
	CSegment::Encode(Buff);

	if( UsePadding() ) {

		Buff.AddByte(0);
		}

	switch( GetLogFormat() ) {
		
		case addr8bit:
			Buff.AddByte(m_dwValue);
			break;
		
		case addr16bit:
			Buff.AddWord(m_dwValue);
			break;
		
		case addr32bit:
			Buff.AddLong(m_dwValue);
			break;
		}

	return Buff.GetSize();
	}

// Implementation

UINT CSegLogical::FindLogFormat(void) const
{
	if( m_dwValue > 0xFF ) {

		if( m_dwValue > 0xFFFF ) {
			
			return addr32bit;
			}
		
		return addr16bit;
		}

	return addr8bit;
	}

BOOL CSegLogical::UsePadding(void) const
{
	if( GetPadded() ) {

		switch( GetLogFormat() ) {

			case addr16bit:
			case addr32bit:

				return TRUE;
			}		
		}

	return FALSE;
	}

// End of File
