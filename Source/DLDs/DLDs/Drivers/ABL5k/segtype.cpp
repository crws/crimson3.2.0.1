
#include "intern.hpp"

#include "segment.hpp"

#include "abl5k.hpp"

#define AfxTrace	m_pPath->m_pDriver->Trace

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Data Type E
//

// Constructor

CSegDataTypeE::CSegDataTypeE(void)
{
	SetType(segDataTypeE);
	}

// Destructor

CSegDataTypeE::~CSegDataTypeE(void)
{
	}

// Attributes

UINT CSegDataTypeE::GetTypeCode(void) const
{
	return GetFormat();
	}

void CSegDataTypeE::SetTypeCode(UINT uType)
{
	SetFormat(uType);
	}

// Encoding

UINT CSegDataTypeE::GetLength(void) const
{
	UINT uLen = 0;

	uLen += sizeof(BYTE);

	uLen += sizeof(BYTE);

	return uLen;
	}

UINT CSegDataTypeE::Encode(CDataBuf &Buff)
{
	UINT uLen = CSegment::Encode(Buff);

	Buff.AddByte(0);	// pad

	return GetLength();
	}

// Parsing

UINT CSegDataTypeE::Parse(PBYTE pData, UINT uSize)
{
	UINT uLen = CSegment::Parse(pData, uSize);

	CDataBuf Decoder(pData, uSize);

	BYTE bPad = Decoder.GetByte(uLen);

	pData += uLen;

	if( GetType() == segDataTypeE ) {

		m_pData = pData;

		m_uSize = uSize - uLen;

		return uSize;
		}

	return uLen;
	}

// Data Parsing/Encoding

UINT CSegDataTypeE::ParseData(PDWORD pData, UINT uCount)
{
	UINT uLen = 0;

	CDataBuf Buff(m_pData, m_uSize);
	
	while( uLen < m_uSize && uCount ) {

		switch( GetTypeCode() ) {

			case typeAlt:
				break;

			case typeBool:
			case typeSint:
			case typeUsint:
			case typeByte:
				*pData = Buff.GetByte(uLen);
				break;

			case typeInt:
			case typeUint:
			case typeWord:
				*pData = Buff.GetWord(uLen);
				break;

			case typeDint:
			case typeUdint:
			case typeReal:
			case typeDword:
				*pData = Buff.GetLong(uLen);
				break;

			case typeLint:
			case typeUlint:
			case typeLreal:
			case typeStime:
			case typeData:
			case typeTod:
			case typeDat:
			case typeStr1:
			case typeLword:
			case typeStr2:
			case typeFtime:
			case typeLtime:
			case typeItime:
			case typeStrN:
			case typeShstr:
			case typeTime:
			case typeEpath:
			case typeEngUnit:
				break;

			default:
				break;
			}

		pData ++;

		uCount --;
		}

	return uCount;
	}

UINT CSegDataTypeE::EncodeData(CDataBuf &Buff, PDWORD pData, UINT uCount)
{
	Buff.AddWord(uCount);

	while( uCount ) {

		switch( GetTypeCode() ) {

			case typeAlt:
				break;

			case typeBool:

				Buff.AddByte(*pData & 1 ? 0xFF : 0x00 );

				break;

			case typeSint:
			case typeUsint:
			case typeByte:

				Buff.AddByte(*pData);

				break;

			case typeInt:
			case typeUint:
			case typeWord:

				Buff.AddWord(*pData);
				
				break;

			case typeDint:
			case typeUdint:
			case typeReal:
			case typeDword:

				Buff.AddLong(*pData);

				break;

			case typeLint:
			case typeUlint:
			case typeLreal:
			case typeStime:
			case typeData:
			case typeTod:
			case typeDat:
			case typeStr1:
			case typeLword:
			case typeStr2:
			case typeFtime:
			case typeLtime:
			case typeItime:
			case typeStrN:
			case typeShstr:
			case typeTime:
			case typeEpath:
			case typeEngUnit:
				break;

			default:
				break;
			}

		uCount --;

		pData ++;
		}

	return uCount;
	}

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Data Type C
//

// Constructor

CSegDataTypeC::CSegDataTypeC(void)
{
	SetType(segDataTypeC);

	m_wCRC = 0;
	}

// Attributes

WORD CSegDataTypeC::GetTypeCRC(void)
{
	return m_wCRC;
	}

UINT CSegDataTypeC::GetTypeCode(void) const
{
	return GetFormat();
	}

// Parsing

UINT CSegDataTypeC::Parse(PBYTE pData, UINT uSize)
{
	UINT uLen = 0;
		
	uLen  += CSegment::Parse(pData, uSize);

	pData += uLen;

	if( GetType() == segDataTypeC ) {

		switch( GetTypeCode() ) {
			
			case typeAbbrevStruc:

				uLen += ParseAbbrevStruct(pData, uSize);
				
				break;

			case typeAbbrevArray:
				break;

			case typeFormalStruc:
				break;

			case typeFormalArray:
				break;

			default:
				return uLen;
			}
		}

	return uLen;
	}

// Parsing Help

UINT CSegDataTypeC::ParseAbbrevStruct(PBYTE pData, UINT uSize)
{
	CDataBuf Decoder(pData, uSize);

	UINT uLen = 0;
	
	BYTE bSize;

	bSize  = Decoder.GetByte(uLen);

	m_wCRC = Decoder.GetWord(uLen);

	// Save the Data ?

	return uLen;
	}

// End of File
