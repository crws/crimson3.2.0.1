
#include "intern.hpp"

#include "segment.hpp"

#include "abl5k.hpp"

#define AfxTrace	m_pPath->m_pDriver->Trace

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment
//

// Constructor

CSegment::CSegment(void)
{
	m_bType  = 0;
	
	m_pPrev  = NULL;

	m_pNext  = NULL;

	m_pPath  = NULL;
	}

// Destructor

CSegment::~CSegment(void)
{
	}

// Attributes

UINT CSegment::GetType(void) const
{
	return (m_bType & 0xE0) >> 5;
	}

UINT CSegment::GetFormat(void) const
{
	return (m_bType & 0x1F) >> 0;
	}

BOOL CSegment::GetPadded(void) const
{
	return m_pPath ? m_pPath->GetPadded() : TRUE;
	}

// Encoding

UINT CSegment::GetLength(void) const
{
	return 1;
	}

UINT CSegment::Encode(CDataBuf &Buff)
{
	Buff.AddByte(m_bType);

	return GetLength();
	}

// Parsing

UINT CSegment::Parse(PBYTE pData, UINT uSize)
{
	if( uSize >= 1 ) {

		m_bType = *pData ++;

		return 1;
		}

	return 0;
	}

void CSegment::SetPath(CCIPPath *pPath)
{
	m_pPath = pPath;
	}

// Implementation

void CSegment::SetType(UINT uType)
{
	m_bType &= ~0xE0;
	
	m_bType |= (0xE0 & (uType << 5));
	}

void CSegment::SetFormat(UINT uFormat)
{
	m_bType &= ~0x1F;

	m_bType |= (0x1F & (uFormat << 0));
	}

// End of File
