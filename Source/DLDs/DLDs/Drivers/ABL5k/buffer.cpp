
#include "intern.hpp"

#include "segment.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Buffer
//

// Constructor

CDataBuf::CDataBuf(void)
{
	m_uIndex = 0;
	
	m_uAlloc = BUFF_SIZE;

	m_pBuff  = m_bData;
	
	memset(m_bData, 0, m_uAlloc);
	}

CDataBuf::CDataBuf(PBYTE pData, UINT uSize)
{
	m_uIndex = 0;

	m_pBuff  = pData;
	
	m_uAlloc = uSize;
	}

// Attributes

UINT CDataBuf::GetFree(void)
{
	return m_uAlloc - GetSize();
	}

UINT CDataBuf::GetSize(void)
{
	return m_uIndex;
	}

PBYTE CDataBuf::GetDataPtr(void)
{
	return m_bData;
	}

// Management

void CDataBuf::Alloc(UINT uAlloc)
{
	}

void CDataBuf::Empty(void)
{
	}

// Encoding

void CDataBuf::Encode(CCIPPath &Path)
{
	CSegment *pSeg = Path.GetHead();

	while( pSeg ) {

		Encode(pSeg);
		
		pSeg  = pSeg->m_pNext;
		}
	}

void CDataBuf::Encode(CSegment *pSeg)
{
	UINT uSize = pSeg->GetLength();

	if( uSize <= GetFree() ) {

		pSeg->Encode(*this);
		}
	}

void CDataBuf::AddByte(BYTE Data)
{
	if( sizeof(Data) <= GetFree() ) {

		*m_pBuff = Data;

		m_uIndex += sizeof(Data);

		m_pBuff  += sizeof(Data);
		}
	}

void CDataBuf::AddWord(WORD Data)
{
	if( sizeof(Data) <= GetFree() ) {

		*PWORD(m_pBuff) = HostToIntel(Data);

		m_uIndex       += sizeof(Data);

		m_pBuff        += sizeof(Data);
		}
	}

void CDataBuf::AddLong(DWORD Data)
{
	if( sizeof(Data) <= GetFree() ) {

		*PDWORD(m_pBuff) = HostToIntel(Data);

		m_uIndex        += sizeof(Data);

		m_pBuff         += sizeof(Data);
		}
	}

void CDataBuf::AddData(PBYTE pData, UINT uSize)
{
	if( uSize <= GetFree() ) {

		memcpy(m_pBuff, pData, uSize);

		m_uIndex        += uSize;

		m_pBuff         += uSize;
		}
	}

// Parsing

BYTE CDataBuf::GetByte(UINT &uIndex)
{
	BYTE Data = 0;

	if( sizeof(Data) <= GetFree() ) {

		Data      = BYTE(*(m_pBuff ++));

		m_uIndex += sizeof(Data);

		uIndex   += sizeof(Data);
		}

	return Data;
	}

WORD CDataBuf::GetWord(UINT &uIndex)
{
	WORD Data = 0;

	if( sizeof(Data) <= GetFree() ) {

		Data      = IntelToHost(*(PU2(m_pBuff)));

		m_pBuff  += sizeof(Data);

		m_uIndex += sizeof(Data);

		uIndex   += sizeof(Data);
		}

	return Data;
	}

DWORD CDataBuf::GetLong(UINT &uIndex)
{
	DWORD Data = 0;

	if( sizeof(Data) <= GetFree() ) {

		Data      = IntelToHost(*(PU4(m_pBuff)));

		m_pBuff  += sizeof(Data);

		m_uIndex += sizeof(Data);

		uIndex   += sizeof(Data);
		}

	return Data;
	}

// End of File
