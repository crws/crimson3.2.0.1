#include "intern.hpp"

#include "airnet.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Dometic AirNet CAN Bus Driver
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CDometicAirnetDriver);

// Constructor

CDometicAirnetDriver::CDometicAirnetDriver(void)
{
	m_Ident = DRIVER_ID;
}

// Destructor

CDometicAirnetDriver::~CDometicAirnetDriver(void)
{

}

// Config

void MCALL CDometicAirnetDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bSrc = GetByte(pData);
	}
}

void MCALL CDometicAirnetDriver::CheckConfig(CSerialConfig &Config)
{
	Config.m_bDrop = m_bSrc;

	Config.m_uFlags |= flagPrivate;

	Config.m_uFlags |= flagExtID;
}

void MCALL CDometicAirnetDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CAirNetHandler(m_pHelper, this);

	m_pHandler->SetSource(m_bSrc);

	pPort->Bind(m_pHandler);
}

void MCALL CDometicAirnetDriver::Detach(void)
{
	m_pHandler->Release();
}

void MCALL CDometicAirnetDriver::Open(void)
{
}

// Device

CCODE MCALL CDometicAirnetDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			BYTE bID  = GetByte(pData);

			BYTE bDef = GetByte(pData);

			m_pCtx->m_bDef    = bDef;
			m_pCtx->m_bDrop   = bID;
			m_pCtx->m_fOnline = FALSE;
			m_pCtx->m_Serial  = 0;
			m_pCtx->m_bGroup  = 0;
			m_pCtx->m_Device  = 0;
			m_pCtx->m_bBoot   = 0;
			m_pCtx->m_bCode   = 0;
			m_pCtx->m_OpMode  = 0;
			m_pCtx->m_FanMode = 0;
			m_pCtx->m_FanSpd  = 0;
			m_pCtx->m_SetTemp = 0;
			m_pCtx->m_fSilent = FALSE;
			m_pCtx->m_Stamp	  = 0;

			memset(m_pCtx->m_Dirty, 0, sizeof(m_pCtx->m_Dirty));

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;

}

CCODE MCALL CDometicAirnetDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

UINT MCALL CDometicAirnetDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) {  // Set ID

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bDrop = uValue;

			return 1;
		}
	}

	return 0;
}

// Entry Points

CCODE MCALL CDometicAirnetDriver::Ping(void)
{
	if( IsBroadcast() ) {

		m_pCtx->m_fOnline = TRUE;

		return CCODE_SUCCESS;
	}

	m_pCtx->m_fOnline = FALSE;

	ID Req  = FindID(CMD_PING);

	ID Resp	= FindID(CMD_PING2);

	PBYTE pData = PBYTE(alloca(GetLength(Resp.i.m_CMD)));

	if( Transact(Req, Resp, 500, pData, FALSE) ) {

		if( m_pCtx->m_bDrop == pData[0] ) {

			m_pCtx->m_bGroup  = pData[1];

			m_pCtx->m_Device  = MAKEWORD(pData[2], pData[3]);

			m_pCtx->m_bBoot   = pData[4];

			m_pCtx->m_bCode   = pData[5];

			m_pCtx->m_Serial  = pData[6] << 16;

			m_pCtx->m_Serial |= pData[7] << 8;

			m_pCtx->m_Serial |= pData[8];

			m_pCtx->m_fOnline = TRUE;

			m_pCtx->m_Stamp   = GetTickCount();

			if( m_pCtx->m_Stamp >= NOTHING - ToTicks(1000) ) {

				m_pCtx->m_Stamp = 0;
			}

			return CCODE_SUCCESS;
		}
	}

	return CCODE_ERROR;
}

CCODE MCALL CDometicAirnetDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_fOnline ) {

		return CCODE_ERROR;
	}

	UINT uTable = Addr.a.m_Table;

	if( IsWriteOnly(uTable) ) {

		return uCount;
	}

	if( uTable >= tableSNR && uTable <= tableCRR ) {

		if( m_pCtx->m_Stamp <= (GetTickCount() - ToTicks(1000)) ) {

			if( !COMMS_SUCCESS(Ping()) ) {

				return CCODE_ERROR;
			}
		}

		switch( uTable ) {

			case tableSNR:	pData[0] = m_pCtx->m_Serial;	return 1;
			case tableUIDR:	pData[0] = m_pCtx->m_bDrop;	return 1;
			case tableGIDR:	pData[0] = m_pCtx->m_bGroup;	return 1;
			case tableDIDR:	pData[0] = m_pCtx->m_Device;	return 1;
			case tableBRR:	pData[0] = m_pCtx->m_bBoot;	return 1;
			case tableCRR:  pData[0] = m_pCtx->m_bCode;	return 1;
		}

		return CCODE_ERROR;
	}

	PDU * pPDU = NULL;

	if( uTable >= tableHCFR && uTable <= tableHATR ) {

		// HVAC Status

		MakeMin(uCount, 1);

		ID Resp	= FindID(CMD_HVACS);

		PBYTE pResp = PBYTE(alloca(GetLength(Resp.i.m_CMD)));

		if( Transact(NULL, Resp, 10, pResp, FALSE) ) {

			BOOL fShift = FALSE;

			switch( uTable ) {

				case tableHCFR:	pData[0] = pResp[0] & 0xF0;	fShift = TRUE; 	break;
				case tableHMR:	pData[0] = pResp[0] & 0x0F;			break;
				case tableHSR:	pData[0] = pResp[1];				break;
				case tableHFMR: pData[0] = pResp[2] & 0xF0;	fShift = TRUE;	break;
				case tableHFSR:	pData[0] = pResp[2] & 0x0F;			break;
				case tableHSTR: pData[0] = pResp[3];				break;
				case tableHAMR:	pData[0] = pResp[4];				break;
				case tableHOTR:	pData[0] = pResp[5];				break;
				case tableHFR:	pData[0] = pResp[6];				break;
				case tableHATR:	pData[0] = pResp[7];				break;
			}

			if( fShift ) {

				pData[0] = pData[0] >> 4;
			}

			return uCount;
		}

		return m_pCtx->m_fSilent ? CCODE_SILENT : CCODE_ERROR;
	}

	if( uTable >= tableISR && uTable <= tableIACR ) {

		// IceMaker Status

		MakeMin(uCount, 1);

		ID Resp	= FindID(CMD_ICEMS);

		PBYTE pResp = PBYTE(alloca(GetLength(Resp.i.m_CMD)));

		if( Transact(NULL, Resp, 10, pResp, FALSE) ) {

			switch( uTable ) {

				case tableISR:	pData[0] = pResp[0]; 	break;
				case tableIHSR:	pData[0] = pResp[1];	break;
				case tableIFR:	pData[0] = pResp[2];	break;
				case tableILVR: pData[0] = pResp[3];	break;
				case tableICCR:	pData[0] = pResp[4];	break;
				case tableIACR: pData[0] = pResp[5];	break;
			}

			return uCount;
		}

		return m_pCtx->m_fSilent ? CCODE_SILENT : CCODE_ERROR;
	}

	if( uTable == tableRM ) {

		// Read Memory

		UINT uMax  = 1;

		UINT uMult = 4;

		UINT uType = Addr.a.m_Type;

		switch( uType ) {

			case addrByteAsWord:	uMax = 2; uMult = 2;	break;
			case addrByteAsByte:	uMax = 5; uMult = 1;	break;
		}

	//MakeMin(uCount, uMax);

		PDU * pPDU = FindPDU(CMD_READ);

		if( pPDU ) {

			UINT uOffset = Addr.a.m_Offset;

			pPDU->m_pData[0] = HIBYTE(uOffset);

			pPDU->m_pData[1] = LOBYTE(uOffset);

			pPDU->m_pData[2] = uCount * uMult;

			ID Reply = FindID(CMD_REPLY);

			PBYTE pReply = PBYTE(alloca(GetLength(Reply.i.m_CMD)));

			if( Transact(pPDU, Reply, 2000, pReply, TRUE) ) {

				for( UINT u = 0; u < uCount; u++ ) {

					switch( uType ) {

						case addrByteAsByte:

							pData[u] = pReply[u];
							break;

						case addrByteAsWord:

							pData[u] = PU2(pReply + u * 2)[0];
							break;

						case addrByteAsLong:

							pData[u] = PU4(pReply + u * 4)[0];
							break;
					}
				}

				return uCount;
			}
		}

		return CCODE_ERROR;
	}

	return CCODE_ERROR;
}

CCODE MCALL CDometicAirnetDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_fOnline ) {

		return CCODE_ERROR;
	}

	UINT uTable = Addr.a.m_Table;

	if( IsReadOnly(uTable) ) {

		return uCount;
	}

	MakeMin(uCount, 1);

	PDU * pPDU = NULL;

	if( uTable >= tableHOMW && uTable <= tableSHVAC ) {

		// HVAC Command

		switch( uTable ) {

			case tableHOMW: m_pCtx->m_OpMode  = pData[0] & 0xFF; m_pCtx->m_Dirty[0] = 1; return uCount;
			case tableHFMW:	m_pCtx->m_FanMode = pData[0] & 0xFF; m_pCtx->m_Dirty[1] = 1; return uCount;
			case tableHFSW:	m_pCtx->m_FanSpd  = pData[0] & 0xFF; m_pCtx->m_Dirty[2] = 1; return uCount;
			case tableHSTW:	m_pCtx->m_SetTemp = pData[0] & 0xFF; m_pCtx->m_Dirty[3] = 1; return uCount;
		}

		if( pData[0] > 0 ) {

			if( !DoHVACCmd() ) {

				return CCODE_ERROR;
			}
		}

		return uCount;
	}

	if( uTable == tableISW ) {

		// IceMaker Command

		if( DoIceMakerCmd(pData[0]) ) {

			return uCount;
		}

		return CCODE_ERROR;
	}

	if( uTable == tableUACW ) {

		// Address change

		if( DoChangeAddress(CMD_ADDR, pData[0]) ) {

			return uCount;
		}

		return CCODE_ERROR;
	}

	if( uTable == tableGACW ) {

		// Group Address change

		if( DoChangeAddress(CMD_GROUP, pData[0]) ) {

			return uCount;
		}

		return CCODE_ERROR;
	}

	if( uTable == tableSM ) {

		// Silent Mode enable/disable

		if( DoSilentMode(pData[0]) ) {

			return uCount;
		}

		return CCODE_ERROR;
	}

	return CCODE_ERROR;
}

// Handler Helpers

BOOL CDometicAirnetDriver::IsSpecial(FRAME_EXT * pFrame)
{
	if( (pFrame->m_ID & 0x3F00000) == (CMD_PING2 << 16) ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CDometicAirnetDriver::HandleSpecial(FRAME_EXT * pFrame, PDU * pPDU)
{
	if( IsSpecial(pFrame) ) {

		m_pHandler->SetPDU(pPDU);

		pPDU->m_pData[6] = (pFrame->m_ID & 0xF0000) >> 16;

		pPDU->m_pData[7] = (pFrame->m_ID & 0x0FF00) >> 8;

		pPDU->m_pData[8] = (pFrame->m_ID & 0x000FF);

		return TRUE;
	}

	return FALSE;
}

// Write Ops

BOOL CDometicAirnetDriver::DoHVACCmd(void)
{
	PDU * pPDU = FindPDU(CMD_HVAC);

	if( pPDU ) {

		pPDU->m_pData[0] = m_pCtx->m_Dirty[0] ? m_pCtx->m_OpMode : 0xFF;
		pPDU->m_pData[1] = m_pCtx->m_Dirty[1] ? m_pCtx->m_FanMode : 0xFF;
		pPDU->m_pData[2] = m_pCtx->m_Dirty[2] ? m_pCtx->m_FanSpd : 0xFF;
		pPDU->m_pData[3] = m_pCtx->m_Dirty[3] ? m_pCtx->m_SetTemp : 0xFF;

		ID Resp	= FindID(CMD_HVACS);

		PBYTE pResp = PBYTE(alloca(GetLength(Resp.i.m_CMD)));

		if( Transact(pPDU, Resp, 1000, pResp, TRUE) ) {

			memset(m_pCtx->m_Dirty, 0, sizeof(m_pCtx->m_Dirty));

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CDometicAirnetDriver::DoIceMakerCmd(DWORD dwData)
{
	PDU * pPDU = FindPDU(CMD_ICEM);

	if( pPDU ) {

		pPDU->m_pData[0] = dwData & 0xFF;

		ID Resp	= FindID(CMD_ICEMS);

		PBYTE pResp = PBYTE(alloca(GetLength(Resp.i.m_CMD)));

		if( Transact(pPDU, Resp, 1000, pResp, TRUE) ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CDometicAirnetDriver::DoChangeAddress(UINT uCmd, DWORD dwData)
{
	PDU * pPDU = FindPDU(uCmd);

	if( pPDU && (uCmd == CMD_ADDR || uCmd == CMD_GROUP) ) {

		BYTE bDrop = dwData & 0xFF;

		pPDU->m_pData[0] = bDrop;

		DWORD dwSerial = ToBCD(m_pCtx->m_Serial);

		BYTE bOld = m_pCtx->m_bGroup;

		BOOL fGroup = uCmd == CMD_GROUP;

		if( !fGroup ) {

			pPDU->m_pData[1] = HIBYTE(HIWORD(dwSerial));

			pPDU->m_pData[2] = LOBYTE(HIWORD(dwSerial));

			pPDU->m_pData[3] = HIBYTE(LOWORD(dwSerial));

			pPDU->m_pData[4] = LOBYTE(LOWORD(dwSerial));

			bOld = m_pCtx->m_bDrop;

			m_pCtx->m_bDrop = bDrop;
		}
		else {
			m_pCtx->m_bGroup = bDrop;
		}

		ID Resp	= FindID(CMD_ADDRR);

		PBYTE pResp = PBYTE(alloca(GetLength(Resp.i.m_CMD)));

		if( Transact(pPDU, Resp, 1000, pResp, TRUE) ) {

			if( (pResp[0] == !fGroup ? m_pCtx->m_bGroup : bDrop) &&
			    (pResp[1] == !fGroup ? bDrop : m_pCtx->m_bDrop)) {

				if( !memcmp(pResp + 2, PBYTE(&dwSerial), GetLength(CMD_ADDRR) - 2) ) {

					for( UINT u = 0; u < 3; u++ ) {

						if( COMMS_SUCCESS(Ping()) ) {

							return TRUE;
						}
					}
				}
			}
		}

		if( !fGroup ) {

			m_pCtx->m_bDrop = bOld;
		}
		else {
			m_pCtx->m_bGroup = bOld;
		}
	}

	return FALSE;
}

BOOL CDometicAirnetDriver::DoSilentMode(DWORD dwData)
{
	PDU * pPDU = FindPDU(CMD_SHHH);

	if( pPDU ) {

		pPDU->m_pData[0] = dwData & 0x01;

		ID Resp	= FindID(CMD_SHHR);

		PBYTE pResp = PBYTE(alloca(GetLength(Resp.i.m_CMD)));

		if( Transact(pPDU, Resp, 1000, pResp, TRUE) ) {

			m_pCtx->m_fSilent = dwData & 0x01;

			FindPDU(CMD_HVACS)->m_uSet = 0;

			return TRUE;
		}
	}

	return FALSE;
}

// Implementation

ID CDometicAirnetDriver::MakeID(UINT uCmd, UINT uOrd)
{
	ID id;

	id.m_Ref   = 0;

	id.i.m_P   = 5;

	id.i.m_CMD = uCmd;

	switch( uOrd ) {

		case orderNone:		break;

		case orderIn:		id.i.m_SA = m_pCtx->m_bDrop;
			id.i.m_DA = m_bSrc;
			break;

		case orderOut:		id.i.m_SA = m_bSrc;
			id.i.m_DA = m_pCtx->m_bDrop;
			break;
	}

	return id;
}

PDU * CDometicAirnetDriver::FindPDU(ID id)
{
	PDU * pPDU = m_pHandler->FindPDU(id);

	if( !pPDU ) {

		MakePDU(id.i.m_CMD);

		pPDU = m_pHandler->FindPDU(id);
	}

	return pPDU;
}

PDU * CDometicAirnetDriver::FindPDU(UINT uCmd)
{
	ID id = FindID(uCmd);

	return FindPDU(id);
}

void CDometicAirnetDriver::MakePDU(UINT uCmd)
{
	ID id = FindID(uCmd);

	switch( uCmd ) {

		case CMD_HVACS:	m_pHandler->MakePDU(id, GetLength(uCmd), 10000); break;
	//	case CMD_ICEMS:	m_pHandler->MakePDU(id,	GetLength(uCmd), 10000); break;	
		case CMD_PING:	m_pHandler->MakePDU(id, GetLength(uCmd), 0);	 break;
		case CMD_PING2:	m_pHandler->MakePDU(id, GetLength(uCmd), 0);	 break;
		case CMD_HVAC:	m_pHandler->MakePDU(id, GetLength(uCmd), 0);	 break;
	//	case CMD_ICEM:	m_pHandler->MakePDU(id,	GetLength(uCmd),  0);	 break;
		case CMD_ADDR:	m_pHandler->MakePDU(id, GetLength(uCmd), 0);	 break;
		case CMD_GROUP:	m_pHandler->MakePDU(id, GetLength(uCmd), 0);	 break;
		case CMD_ADDRR: m_pHandler->MakePDU(id, GetLength(uCmd), 0);	 break;
		case CMD_SHHH:	m_pHandler->MakePDU(id, GetLength(uCmd), 0);	 break;
		case CMD_SHHR:	m_pHandler->MakePDU(id, GetLength(uCmd), 0);	 break;
	//	case CMD_READ:	m_pHandler->MakePDU(id,	GetLength(uCmd),  0);	 break;
	//	case CMD_REPLY:	m_pHandler->MakePDU(id,	GetLength(uCmd),  0);	 break;
	}
}

ID   CDometicAirnetDriver::FindID(UINT uCmd)
{
	switch( uCmd ) {

		case CMD_HVACS:	return MakeID(CMD_HVACS, orderOut);
	//	case CMD_ICEMS:	return MakeID(CMD_ICEMS,	orderOut ); 	
		case CMD_PING:	return MakeID(CMD_PING, orderOut);
		case CMD_PING2:	return MakeID(CMD_PING2, orderIn);
		case CMD_HVAC:	return MakeID(CMD_HVAC, orderOut);
	//	case CMD_ICEM:	return MakeID(CMD_ICEM,		orderOut );
		case CMD_ADDR:	return MakeID(CMD_ADDR, orderOut);
		case CMD_GROUP:	return MakeID(CMD_GROUP, orderOut);
		case CMD_ADDRR: return MakeID(CMD_ADDRR, orderOut);
		case CMD_SHHH:	return MakeID(CMD_SHHH, orderOut);
		case CMD_SHHR:	return MakeID(CMD_SHHR, orderOut);
	//    	case CMD_READ:	return MakeID(CMD_READ,		orderIn  );
	//	case CMD_REPLY:	return MakeID(CMD_REPLY,	orderOut );
	}

	ID id;

	id.m_Ref = 0;

	return id;
}

UINT CDometicAirnetDriver::GetLength(UINT uCmd)
{
	switch( uCmd ) {

		case CMD_HVACS:	return UINT(8);
	//	case CMD_ICEMS:	return UINT(6);	
		case CMD_PING:	return UINT(8);
		case CMD_PING2:	return UINT(10);
		case CMD_HVAC:	return UINT(4);
	//	case CMD_ICEM:	return UINT(1);
		case CMD_ADDR:	return UINT(5);
		case CMD_GROUP:	return UINT(1);
		case CMD_ADDRR: return UINT(6);
		case CMD_SHHH:	return UINT(1);
		case CMD_SHHR:	return UINT(1);
	//     	case CMD_READ:	return UINT(3);
	//	case CMD_REPLY:	return UINT(8);
	}

	return 0;
}

BOOL CDometicAirnetDriver::SendPDU(PDU * pPDU, BOOL fWrite)
{
	if( m_pHandler->IsTimedOut(pPDU->m_uReq, pPDU->m_uLive) ) {

		if( fWrite ) {

			return m_pHandler->SendPDU(pPDU);
		}

		return m_pHandler->SendPDUReq(pPDU);
	}

	return FALSE;
}

// Transport

BOOL  CDometicAirnetDriver::Transact(ID Req, ID Resp, UINT uTime, PBYTE pData, BOOL fWrite)
{
	PDU * pReq = FindPDU(Req);

	return Transact(pReq, Resp, uTime, pData, fWrite);
}

BOOL CDometicAirnetDriver::Transact(PDU * pReq, ID Resp, UINT uTime, PBYTE pData, BOOL fWrite)
{
	PDU * pResp = FindPDU(Resp);

	if( pReq && pResp ) {

		m_pHandler->Clear(pResp);

		SendPDU(pReq, fWrite);
	}

	if( IsBroadcast() ) {

		return TRUE;
	}

	if( pResp ) {

		SetTimer(uTime);

		while( GetTimer() ) {

			if( m_pHandler->HasData(pResp) ) {

				memcpy(pData, pResp->m_pData, pResp->m_uBytes);

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Helpers

BOOL CDometicAirnetDriver::IsWriteOnly(UINT uTable)
{
	if( (uTable >= tableHOMW && uTable <= tableSHVAC) ||
	    (uTable >= tableUACW && uTable <= tableSM)    ||
	    (uTable == tableISW) ||
		
		IsBroadcast() ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CDometicAirnetDriver::IsReadOnly(UINT uTable)
{
	if( (uTable >= tableHCFR && uTable <= tableHATR) ||
	    (uTable >= tableISR  && uTable <= tableCRR)  ||
	    (uTable == tableRM) ) {

		return TRUE;
	}

	return FALSE;
}

DWORD CDometicAirnetDriver::ToBCD(DWORD dwData)
{
	DWORD dwWork = 0;

	DWORD dwHex  = 0x10000000;

	DWORD dwDec  = 10000000;

	while( dwHex ) {

		dwWork += dwHex * ((dwData / dwDec) % 10);

		dwHex /= 16;
		dwDec /= 10;
	}

	return dwWork;
}

BOOL CDometicAirnetDriver::IsBroadcast(void)
{
	return m_pCtx->m_bDef >  0 || IsSilent();
}

BOOL CDometicAirnetDriver::IsSilent(void)
{
	PDU * pPDU = FindPDU(CMD_HVACS);

	BOOL fSilent  = FALSE;

	if( pPDU ) {

		fSilent = pPDU->m_uSet == 0;
	}

	if( !fSilent ) {

		pPDU = FindPDU(CMD_ICEMS);

		if( pPDU ) {

			fSilent = pPDU->m_uSet == 0;
		}
	}

	return fSilent || m_pCtx->m_fSilent;
}

// End of File

