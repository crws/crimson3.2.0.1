
#ifndef	INCLUDE_AIRNET_HPP

#define	INCLUDE_AIRNET_HPP  

//////////////////////////////////////////////////////////////////////////
//
// Dometic AirNet CAN Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//


//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDometicAirnetDriver;

//////////////////////////////////////////////////////////////////////////
//
// 29-Bit Identifier Field
//

struct ID {

	union {
		struct {

			#ifdef _E_LITTLE

			UINT  m_SA : 8;
			UINT  m_DA : 8;
			UINT  m_CMD : 10;
			UINT  m_P : 3;
			UINT  m_X : 3;

			#else

			UINT  m_X : 3;
			UINT  m_P : 3;
			UINT  m_CMD : 10;
			UINT  m_DA : 8;
			UINT  m_SA : 8;

			#endif
		} i;

		DWORD m_Ref;
	};
};

//////////////////////////////////////////////////////////////////////////
//
// PDU Storage
//

struct PDU {

	ID       m_ID;
	UINT     m_uBytes;
	BYTE *   m_pData;
	UINT     m_uSet;
	UINT	 m_uLive;
	UINT	 m_uReq;
	BOOL	 m_fSend;
};

//////////////////////////////////////////////////////////////////////////
//
// 29-bit FRAME
//

#pragma pack(1)

struct FRAME_EXT
{
	BYTE	m_Ctrl;
	BYTE    m_Zero;
	DWORD	m_ID;
	BYTE	m_bData[8];
};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Driver Context Structures
//

struct SPN
{
	BYTE  m_Size;
	UINT  m_Number;
};

struct PG
{
	UINT	 m_Number;
	BYTE	 m_Diag;
	BYTE	 m_Priority;
	BYTE     m_SendReq;
	UINT	 m_uSPNs;
	SPN *	 m_pSPNs;
	UINT     m_RepRate;
	UINT     m_uRequest;
};

//////////////////////////////////////////////////////////////////////////
//
// AirNet Handler
//

class CAirNetHandler : public IPortHandler
{
	public:
		// Constructor
		CAirNetHandler(IHelper *pHelper, CDometicAirnetDriver *pDriver);

		// Destructor
		~CAirNetHandler(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject * pPort);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnTimer(void);

		// Driver Access
		BOOL SendPDU(PDU * pPDU);
		BOOL SendPDUReq(PDU * pPDU);
		void MakePDU(ID id, UINT uSize, UINT uLive);
		PDU* FindPDU(ID id);
		BOOL SetPDU(PDU * pPDU);
		BOOL HasData(PDU * pPDU);
		BOOL IsTimedOut(UINT uTime, UINT uSpan);
		void SetSource(BYTE bSrc);
		void Clear(PDU * pPDU);

	protected:

		// Help

		IHelper		     * m_pHelper;

		// Driver Access

		CDometicAirnetDriver * m_pDriver;

		// Data Members
		ULONG		m_uRefs;
		PDU *		m_pPDUs;
		UINT		m_uPDUs;
		FRAME_EXT *	m_pRxData;
		PCBYTE		m_pTxData;
		UINT		m_uTx;
		UINT		m_uRx;
		IPortObject *	m_pPort;
		FRAME_EXT	m_Rx;
		FRAME_EXT	m_Tx;
		BYTE 		m_bSrc;

		// Implementation
		void Start(void);
		void AddByte(BYTE bByte, UINT &uPos);
		void AddID(ID id);
		void AddCtrl(BYTE bCtrl);
		void AddData(PDU * pPDU);
		void GrowPDUs(PDU * pPDU);
		BOOL PutFrame(void);
		void SendFrame(void);
		void HandleFrame(void);
		BOOL DoListen(void);
		PDU* FindPDU(void);

		// Helpers
		BOOL IsThisNode(void);
		BOOL IsBroadcast(void);

		// Testing / Debug
		void ShowPDUs(void);
};

//////////////////////////////////////////////////////////////////////////
//
// Dometic Constants
//
//
//

// Commands

#define CMD_HVACS	0x3E0
#define CMD_ICEMS	0x3E1
#define CMD_PING	0x200 
#define CMD_PING2	0x220
#define CMD_HVAC	0x3F0
#define CMD_ICEM	0x3F1
#define CMD_ADDR	0x230
#define CMD_GROUP	0x240
#define CMD_ADDRR	0x250
#define CMD_SHHH	0x260
#define CMD_SHHR	0x270
#define CMD_READ	0x285
#define CMD_REPLY	0x286

// Order Specifiers

enum
{
	orderNone  = 0,
	orderIn    = 1,
	orderOut   = 2,
};

// Tables

enum
{
	tableHOMW  = 1,
	tableHFMW  = 2,
	tableHFSW  = 3,
	tableHSTW  = 4,
	tableSHVAC = 5,
	tableHCFR  = 6,
	tableHMR   = 7,
	tableHSR   = 8,
	tableHFMR  = 9,
	tableHFSR  = 10,
	tableHSTR  = 11,
	tableHAMR  = 12,
	tableHOTR  = 13,
	tableHFR   = 14,
	tableHATR  = 15,

	tableISW   = 16,
	tableISR   = 17,
	tableIHSR  = 18,
	tableIFR   = 19,
	tableILVR  = 20,
	tableICCR  = 21,
	tableIACR  = 22,

	tableSNR   = 23,
	tableUIDR  = 24,
	tableGIDR  = 25,
	tableDIDR  = 26,
	tableBRR   = 27,
	tableCRR   = 28,

	tableUACW  = 29,
	tableGACW  = 30,
	tableSM    = 31,
	tableRM    = 32,
};

//////////////////////////////////////////////////////////////////////////
//
// Dometic AirNet CAN Bus Driver
//
//
//

class CDometicAirnetDriver : public CMasterDriver
{
	public:
		// Constructor
		CDometicAirnetDriver(void);

		// Destructor
		~CDometicAirnetDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE)Ping(void);
		DEFMETH(CCODE)Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)Write(AREF Addr, PDWORD pData, UINT uCount);

		// Handler Helpers
		BOOL IsSpecial(FRAME_EXT * pFrame);
		BOOL HandleSpecial(FRAME_EXT * pFrame, PDU * pPDU);

	protected:

		struct CContext
		{
			BYTE	       m_bDef;
			BYTE           m_bDrop;
			BOOL	       m_fOnline;
			DWORD	       m_Serial;
			BYTE	       m_bGroup;
			WORD	       m_Device;
			BYTE	       m_bBoot;
			BYTE	       m_bCode;

			BYTE	       m_OpMode;
			BYTE	       m_FanMode;
			BYTE	       m_FanSpd;
			BYTE	       m_SetTemp;
			BYTE	       m_Dirty[4];

			BOOL	       m_fSilent;

			UINT	       m_Stamp;
		};

		// Data Members
		
		CContext *	m_pCtx;
		BYTE		m_bSrc;

		// Handler

		CAirNetHandler * m_pHandler;

		// Write Ops

		BOOL	DoHVACCmd(void);
		BOOL	DoIceMakerCmd(DWORD dwData);
		BOOL	DoChangeAddress(UINT uCmd, DWORD dwData);
		BOOL	DoSilentMode(DWORD dwData);

		// Implementation

		ID	MakeID(UINT uCmd, UINT uOrder);
		PDU *	FindPDU(ID id);
		PDU *   FindPDU(UINT uCmd);
		void	MakePDU(UINT uCmd);
		ID      FindID(UINT uCmd);
		UINT	GetLength(UINT uCmd);
		BOOL	SendPDU(PDU * pPDU, BOOL fWrite);


		// Transport

		BOOL    Transact(ID Req, ID Resp, UINT uTime, PBYTE pData, BOOL fWrite);
		BOOL	Transact(PDU * pReq, ID Resp, UINT uTime, PBYTE pData, BOOL fWrite);


		// Helpers

		BOOL	IsWriteOnly(UINT uTable);
		BOOL	IsReadOnly(UINT uTable);
		BOOL	IsBroadcast(void);
		BOOL	IsSilent(void);
		DWORD   ToBCD(DWORD dwData);
};

// End of File

#endif

