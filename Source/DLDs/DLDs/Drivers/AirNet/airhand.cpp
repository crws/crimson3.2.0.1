
#include "intern.hpp"

#include "airnet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Dometic AirNet CAN BUS 29-bit Handler
//
/// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Constructor

CAirNetHandler::CAirNetHandler(IHelper *pHelper, CDometicAirnetDriver *pDriver)
{
	StdSetRef();

	m_uPDUs      = 0;

	m_pPDUs	     = NULL;

	m_pRxData  = NULL;

	m_pTxData  = NULL;

	m_pPort    = NULL;

	m_pHelper  = pHelper;

	m_pDriver  = pDriver;

	m_bSrc	   = 0;
}

// Destructor

CAirNetHandler::~CAirNetHandler(void)
{
	
}

// IUnknown

HRESULT CAirNetHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
}

ULONG CAirNetHandler::AddRef(void)
{
	StdAddRef();
}

ULONG CAirNetHandler::Release(void)
{
	StdRelease();
}

// IPortHandler

void CAirNetHandler::Bind(IPortObject * pPort)
{
	m_pPort = pPort;
}

void CAirNetHandler::OnOpen(CSerialConfig const &Config)
{
	m_pTxData    = NULL;

	m_pRxData    = NULL;

	m_uTx	     = 0;

	m_uRx        = 0;
}

void CAirNetHandler::OnClose(void)
{
}

void CAirNetHandler::OnRxData(BYTE bData)
{
	if( m_uRx < sizeof(FRAME_EXT) ) {

		PBYTE pRx = PBYTE(&m_Rx);

		pRx[m_uRx] = bData;

		m_uRx++;
	}
}

void CAirNetHandler::OnRxDone(void)
{
	if( m_uRx == sizeof(FRAME_EXT) ) {

		HandleFrame();

		m_uRx = 0;
	}
}

BOOL CAirNetHandler::OnTxData(BYTE &bData)
{
	if( m_uTx > 0 ) {

		if( m_pTxData ) {

			m_uTx--;

			bData = *m_pTxData++;

			return TRUE;
		}
	}

	m_pTxData = NULL;

	return FALSE;
}

void CAirNetHandler::OnTxDone(void)
{

}

void CAirNetHandler::OnTimer(void)
{
	for( UINT u = 0; u < m_uPDUs; u++ ) {

		PDU * pPDU = &m_pPDUs[u];

		if( pPDU->m_uSet > 0 && pPDU->m_uLive > 0 ) {

			if( IsTimedOut(pPDU->m_uSet, pPDU->m_uLive) ) {

				Clear(pPDU);
			}
		}
	}
}

// PDU Control

BOOL CAirNetHandler::SendPDU(PDU * pPDU)
{
	if( pPDU ) {

		Start();

		AddID(pPDU->m_ID);

		AddCtrl(pPDU->m_uBytes);

		AddData(pPDU);

		return PutFrame();
	}

	return FALSE;
}

BOOL CAirNetHandler::SendPDUReq(PDU * pPDU)
{
	if( pPDU ) {

		Start();

		ID id;

		id.m_Ref = pPDU->m_ID.m_Ref;

		AddID(id);

		AddCtrl(0);

		AddData(pPDU);

		pPDU->m_uReq = GetTickCount();

		return PutFrame();
	}

	return FALSE;
}

// Data Access

void CAirNetHandler::MakePDU(ID id, UINT uBytes, UINT uLive)
{
	if( id.m_Ref ) {

		PDU * pPDU         = new PDU;

		pPDU->m_ID         = id;

		pPDU->m_uBytes     = uBytes;

		pPDU->m_pData      = NULL;

		pPDU->m_uLive      = uLive;

		GrowPDUs(pPDU);

		delete pPDU;
	}
}

PDU* CAirNetHandler::FindPDU(ID id)
{
	if( id.m_Ref ) {

		UINT uMask = 0x3FFFFFF;

		for( UINT u = 0; u < m_uPDUs; u++ ) {

			if( (id.m_Ref & uMask) == (m_pPDUs[u].m_ID.m_Ref & uMask) ) {

				return &m_pPDUs[u];
			}
		}
	}

	return NULL;
}

// Public Helpers

BOOL CAirNetHandler::HasData(PDU * pPDU)
{
	return pPDU->m_uSet ? TRUE : FALSE;
}

BOOL CAirNetHandler::IsTimedOut(UINT uTime, UINT uSpan)
{
	return (((GetTickCount() - uTime) > ToTicks(uSpan)) ? 1 : 0);
}

void CAirNetHandler::SetSource(BYTE bSrc)
{
	m_bSrc = bSrc;
}

void CAirNetHandler::Clear(PDU * pPDU)
{
	pPDU->m_uSet = 0;
}

// Implementation

void CAirNetHandler::Start(void)
{
	memset(&m_Tx, 0x0, sizeof(FRAME_EXT));
}

void CAirNetHandler::AddByte(BYTE bByte, UINT &uPos)
{
	m_Tx.m_bData[uPos++] = bByte;
}

void CAirNetHandler::AddID(ID id)
{
	m_Tx.m_ID  = id.m_Ref;
}

void CAirNetHandler::AddCtrl(BYTE bCtrl)
{
	m_Tx.m_Ctrl = bCtrl | 0x80;
}

void CAirNetHandler::AddData(PDU * pPDU)
{
	memcpy(m_Tx.m_bData, pPDU->m_pData, UINT(m_Tx.m_Ctrl & 0x0F));
}

void CAirNetHandler::GrowPDUs(PDU * pPDU)
{
	PDU * pPDUList = new PDU[m_uPDUs + 1];

	UINT uSize = 0;

	for( UINT a = 0; a < m_uPDUs; a++ ) {

		pPDUList[a].m_ID = m_pPDUs[a].m_ID;

		uSize = m_pPDUs[a].m_uBytes;

		pPDUList[a].m_uBytes = uSize;

		pPDUList[a].m_pData = new BYTE[uSize];

		memcpy(pPDUList[a].m_pData, m_pPDUs[a].m_pData, uSize);

		pPDUList[a].m_uSet  = m_pPDUs[a].m_uSet;

		pPDUList[a].m_uReq  = m_pPDUs[a].m_uReq;

		pPDUList[a].m_uLive = m_pPDUs[a].m_uLive;

		pPDUList[a].m_fSend = m_pPDUs[a].m_fSend;
	}

	pPDUList[m_uPDUs].m_ID = pPDU->m_ID;

	uSize = pPDU->m_uBytes;

	pPDUList[m_uPDUs].m_uBytes = uSize;

	pPDUList[m_uPDUs].m_pData = new BYTE[uSize];

	memset(pPDUList[m_uPDUs].m_pData, 0, uSize);

	pPDUList[m_uPDUs].m_uSet  = GetTickCount();

	pPDUList[m_uPDUs].m_uReq  = 0;

	pPDUList[m_uPDUs].m_uLive = pPDU->m_uLive * 2;

	pPDUList[m_uPDUs].m_fSend = FALSE;

	for( UINT y = 0; y < m_uPDUs; y++ ) {

		delete[] m_pPDUs[y].m_pData;
	}

	delete[] m_pPDUs;

	m_pPDUs = pPDUList;

	m_uPDUs++;
}

BOOL CAirNetHandler::PutFrame(void)
{
	SendFrame();

	return TRUE;
}

void CAirNetHandler::SendFrame(void)
{
	m_pTxData = PCBYTE(&m_Tx);

	m_uTx     = sizeof(FRAME_EXT) - 1;

	m_pPort->Send(*m_pTxData++);
}

void CAirNetHandler::HandleFrame(void)
{
	m_pRxData = &m_Rx;

	PDU * pPDU = FindPDU();

	if( pPDU ) {

		if( m_pDriver->HandleSpecial(m_pRxData, pPDU) ) {

			return;
		}

		if( DoListen() || IsBroadcast() ) {

			SetPDU(pPDU);
		}
	}

	m_pRxData = NULL;
}

BOOL CAirNetHandler::DoListen(void)
{
	return IsThisNode();
}

PDU* CAirNetHandler::FindPDU(void)
{
	UINT uMask = 0x3FFFFFF;

	ID id;

	id.m_Ref = m_pRxData->m_ID;

	if( IsBroadcast() ) {

		uMask = 0x3FFFF00;
	}

	if( m_pDriver->IsSpecial(m_pRxData) ) {

		id.m_Ref = (m_pRxData->m_ID & 0x3F00000) | (m_bSrc << 8) | m_pRxData->m_bData[0];
	}

	for( UINT u = 0; u < m_uPDUs; u++ ) {

		if( ((id.m_Ref & uMask) == (m_pPDUs[u].m_ID.m_Ref & uMask)) ) {

			return &m_pPDUs[u];
		}
	}

	return NULL;
}

BOOL CAirNetHandler::SetPDU(PDU * pPDU)
{
	UINT uCount  = m_pRxData->m_Ctrl;

	memcpy(pPDU->m_pData, m_pRxData->m_bData, uCount);

	UINT uTick   = GetTickCount();

	pPDU->m_uSet = uTick ? uTick : 1;

	return TRUE;
}

// Helpers

BOOL CAirNetHandler::IsThisNode(void)
{
	ID id;

	id.m_Ref = m_pRxData->m_ID;

	return id.i.m_SA == m_bSrc;
}

BOOL CAirNetHandler::IsBroadcast(void)
{
	ID id;

	id.m_Ref = m_pRxData->m_ID;

	return id.i.m_SA == 0x0;
}

// Testing / Debug

void CAirNetHandler::ShowPDUs(void)
{
	for( UINT u = 0; u < m_uPDUs; u++ ) {

		AfxTrace("PDU %u of %u...\n", u + 1, m_uPDUs);

		AfxTrace("PDU %8.8x has %u bytes\n", m_pPDUs[u].m_ID, m_pPDUs[u].m_uBytes);
	}
}

// End of File

