#ifndef INCLUDE_UNIDRIVE_M_BASE
#define INCLUDE_UNIDRIVE_M_BASE

#include "intern.hpp"

// Modbus Spaces
#define	SPACE_HOLD	0x01
#define	SPACE_ANALOG	0x02
#define	SPACE_OUTPUT	0x03
#define	SPACE_INPUT	0x04
#define	SPACE_HOLD32	0x05
#define	SPACE_ANALOG32	0x06

//////////////////////////////////////////////////////////////////////////
//
// Emerson - Control Techniques Unidrive M Master Base
//

class CUnidriveMBase : public CMasterDriver
{
	public:
		// Entry Points
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		struct UnidriveMBaseContext
		{		
			UINT	   m_uRegMode;
			UINT	   m_uDriveMode;
			WORD	   m_wPing;
			};

		BYTE	m_bRx[300];
		BYTE	m_bTx[300];
		UINT	m_uPtr;

		// Frame Building
		virtual void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);

		// Overridable Transport Layer
		virtual BOOL Transact(BOOL fIgnore);

		// Read/Write Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);

		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
	};

// End of File

#endif
