//////////////////////////////////////////////////////////////////////////
//
// Emerson - Control Techniques Unidrive M Serial Master
//

#include "UnidriveMSerial.hpp"

// Instantiator

INSTANTIATE(CUnidriveMSerial)

// Constructor

CUnidriveMSerial::CUnidriveMSerial(void)
{
	m_Ident = DRIVER_ID;
	
	m_pCtx	= NULL;

	m_pHead = NULL;

	m_pTail = NULL;

	m_uTimeout = 1000;
	}

// Destructor

CUnidriveMSerial::~CUnidriveMSerial(void)
{
	}

// Configuration

void CUnidriveMSerial::Load(LPCBYTE pData)
{
	}

void CUnidriveMSerial::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {

		Config.m_uFlags |= flagFastRx;
		}

	Make485(Config, TRUE);
	}

// Management

void CUnidriveMSerial::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void CUnidriveMSerial::Open(void)
{
	}

// Device

CCODE CUnidriveMSerial::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop		= GetByte(pData);
			m_pCtx->m_wPing		= GetWord(pData);
			m_pCtx->m_uRegMode	= GetLong(pData);
			m_pCtx->m_uDriveMode	= GetLong(pData);

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE CUnidriveMSerial::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CCommsDriver::DeviceClose(fPersist);
	}

CCODE CUnidriveMSerial::Ping(void)
{
	CAddress Addr;

	DWORD Data[1];

	Addr.a.m_Offset	= m_pCtx->m_wPing;
	Addr.a.m_Type	= addrWordAsWord;
	Addr.a.m_Table	= SPACE_HOLD;
	Addr.a.m_Extra  = 0;

	return DoWordRead(Addr, Data, 1);
	}

// Port Access

void  CUnidriveMSerial::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT  CUnidriveMSerial::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CUnidriveMSerial::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	m_bRx[1] = 0;

	AddByte(m_pCtx->m_bDrop);

	AddByte(bOpcode);
	}

// Transport Layer

BOOL CUnidriveMSerial::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {

		if( GetFrame(fIgnore) ) {

			return CheckReply(fIgnore);
			}
		}

	return FALSE;
	}

BOOL CUnidriveMSerial::PutFrame(void)
{
	m_pData->ClearRx();
	
	return BinaryTx();
	}

BOOL CUnidriveMSerial::GetFrame(BOOL fWrite)
{
	return BinaryRx(fWrite);
	}

BOOL CUnidriveMSerial::CheckReply(BOOL fIgnore)
{
	if( m_bRx[0] == m_bTx[0] ) {

		if( fIgnore ) {

			return TRUE;
			}
	
		if( !HasReadException() ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL  CUnidriveMSerial::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_bTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL  CUnidriveMSerial::BinaryRx(BOOL fWrite)
{
	UINT uPtr = 0;

	UINT uGap = 0;

	SetTimer(m_uTimeout);

	while( GetTimer() ) {

		UINT uByte;
		
		if( (uByte = RxByte(5)) < NOTHING ) {

			m_bRx[uPtr++] = uByte;

			if( uPtr <= sizeof(m_bRx) ) {
			
				uGap = 0;
				
				continue;
				}
				
			return FALSE;
			}
		
		if( ++uGap >= 5 ) {
			
			if( uPtr >= 4 ) {
				
				m_CRC.Preset();
				
				PBYTE p = m_bRx;
				
				UINT  n = uPtr - 2;
			
				for( UINT i = 0; i < n; i++ ) {

					m_CRC.Add(*(p++));
					}

				WORD c1 = IntelToHost(PU2(p)[0]);
				
				WORD c2 = m_CRC.GetValue();
					
				if( c1 == c2 ) {
						 
					return TRUE;
					}
				}				
				
			uPtr = 0;
			
			uGap = 0;
			}
		}

	return FALSE;
	}

BOOL CUnidriveMSerial::HasReadException(void)
{
	return m_bRx[1] & 0x80;
	}

// End of File
