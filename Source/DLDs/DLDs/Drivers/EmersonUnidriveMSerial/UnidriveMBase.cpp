//////////////////////////////////////////////////////////////////////////
//
// Emerson - Control Techniques Unidrive M Master Base
//

#include "UnidriveMBase.hpp"

// Entry Points

CCODE CUnidriveMBase::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress ModAddr = Addr;

	if( ModAddr.a.m_Table == addrNamed ) {

		switch( ModAddr.a.m_Type ) {

			case addrBitAsBit:
				ModAddr.a.m_Table = SPACE_INPUT;
				break;

			case addrWordAsWord:
				ModAddr.a.m_Table = SPACE_HOLD;
				break;

			case addrLongAsLong:
				ModAddr.a.m_Table = SPACE_HOLD32;
				break;
			}
		}

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:
			return DoBitRead(ModAddr, pData, uCount);

		case addrWordAsWord:
			return DoWordRead(ModAddr, pData, uCount);

		case addrLongAsLong:
		case addrRealAsReal:
			return DoLongRead(ModAddr, pData, uCount);
		}

	return CCODE_ERROR;
	}

CCODE CUnidriveMBase::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress ModAddr = Addr;

	if( ModAddr.a.m_Table == addrNamed ) {

		switch( ModAddr.a.m_Type ) {

			case addrBitAsBit:
				ModAddr.a.m_Table = SPACE_INPUT;
				break;

			case addrWordAsWord:
				ModAddr.a.m_Table = SPACE_HOLD;
				break;

			case addrLongAsLong:
				ModAddr.a.m_Table = SPACE_HOLD32;
				break;
			}
		}

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:
			return DoBitWrite(ModAddr, pData, uCount);

		case addrWordAsWord:
			return DoWordWrite(ModAddr, pData, uCount);

		case addrLongAsLong:
		case addrRealAsReal:
			return DoLongWrite(ModAddr, pData, uCount);
		}

	return CCODE_ERROR;
	}

// Frame building

void CUnidriveMBase::StartFrame(BYTE bOpcode)
{
	}

void CUnidriveMBase::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr] = bData;

		m_uPtr++;
		}
	}

void CUnidriveMBase::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CUnidriveMBase::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CUnidriveMBase::Transact(BOOL fIgnore)
{
	return FALSE;
	}

// Read Handlers

CCODE CUnidriveMBase::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
	
	AddWord(Addr.a.m_Offset);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x = PU2(m_bRx + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CUnidriveMBase::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLD:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	// The 32-bit holding registers start at offset 0x4000
	UINT uOffset = Addr.a.m_Offset + 0x4000;

	AddWord(uOffset);
	
	AddWord(uCount * 2); // set 16 bit word count
	
	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {

			UINT u = 3 + (4 * n);

			DWORD x  = *(PU4(&m_bRx[u]));

			pData[n] = MotorToHost(x);
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CUnidriveMBase::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
			
		case SPACE_OUTPUT:
			StartFrame(0x01);
			break;
			
		case SPACE_INPUT:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(Addr.a.m_Offset);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b    = 3;

		BYTE m    = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	return CCODE_ERROR;
	}


// Write Handlers

CCODE CUnidriveMBase::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_HOLD ) {

		if( uCount == 1 ) {

			StartFrame(6);

			AddWord(Addr.a.m_Offset);
		
			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);

			AddWord(Addr.a.m_Offset);

			AddWord(uCount);

			AddByte(uCount * 2);

			for( UINT n = 0; n < uCount; n++ ) {

				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE) ) {

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CUnidriveMBase::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_HOLD32 || Addr.a.m_Table == SPACE_HOLD ) {

		StartFrame(16);

		UINT uOffset = Addr.a.m_Offset + 0x4000;
		
		AddWord(uOffset);
		
		AddWord(2 * uCount);
		
		AddByte(uCount * 4);

		for( UINT n = 0; n < uCount; n++ ) {

			AddLong(pData[n]);
			}

		if( Transact(TRUE) ) {

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CUnidriveMBase::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_OUTPUT ) {

		if( uCount == 1 ) {
			
			StartFrame(5);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			StartFrame(15);

			AddWord(Addr.a.m_Offset - 1);

			AddWord(uCount);

			AddByte((uCount + 7) / 8);

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b);
				}
			}

		if( Transact(TRUE) ) {

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// End of File
