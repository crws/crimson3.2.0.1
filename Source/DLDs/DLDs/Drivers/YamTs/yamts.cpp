#include "intern.hpp"

#include "yamts.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller TS Series Master Info Structur
//


CMD CODE_SEG CYamahaTsDriver::m_Cmd[] = {

	{addrNamed,	cmdStart,	fieldData,	respRun  | respEnd,	accessWO,	"START"	},
	{addrNamed,	cmdStop,	fieldNoData,	respNone,		accessWO,	"STOP"	},
	{addrNamed,	cmdOrg,		fieldNoData,	respRun  | respEnd,	accessWO,	"ORG"	},
	{addrNamed,	cmdJogP,	fieldNoData,	respRun  | respOK,	accessWO,	"JOG+"	},
	{addrNamed,	cmdJogM,	fieldNoData,	respRun  | respOK,	accessWO,	"JOG-"	},
	{addrNamed,	cmdInchP,	fieldNoData,	respRun	 | respEnd,	accessWO,	"INCH+"	},
	{addrNamed,	cmdInchM,	fieldNoData,	respRun	 | respEnd,	accessWO,	"INCH-"	},
	{addrNamed,	cmdSrvo,	fieldFlag,	respOK,			accessSR,	"SRVO"	},
	{addrNamed,	cmdBrk,		fieldFlag,	respOK,			accessSR,	"BRK"	},
	{addrNamed,	cmdReset,	fieldNoData,	respOK,			accessWO,	"RESET"	},
	};

CMD CODE_SEG CYamahaTsDriver::m_Tbl[] = {

	{   tabErr,	cmdError,	fieldNone,	respNone,		accessRO,	""	},
	{    tabOp,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"M"	},
	{     tabP,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"P"	},
	{     tabS,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"S"	},
	{    tabAC,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"AC"	},
	{    tabDC,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"DC"	},
	{     tabQ,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"Q"	},
	{    tabZL,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"ZL"	},
	{    tabZH,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"ZH"	},
	{    tabN,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"N"	},
	{    tabJ,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"J"	},
	{    tabF,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"F"	},
	{    tabT,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"T"	},
	{tabTeach,	cmdTable,	fieldData,	respOK,			accessWO,	"TEACH"	},
	{ tabCopy,	cmdTable,	fieldData2,	respOK,			accessWO,	"COPY"	},
	{  tabDel,	cmdTable,	fieldData2,	respOK,			accessWO,	"DEL"	},
	{    tabK,	cmdTable,	fieldData,	respEcho | respOK,	accessRW,	"K"	},
	{    tabD,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"D"	},
	{   tabIn,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"IN"	},
	{  tabInB,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"INB"	},
	{  tabOut,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"OUT"	},
	{ tabOutB,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"OUTB"	},
	{  tabWIn,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"WIN"	},
	{ tabWOut,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"WOUT"	},
	{  tabOpt,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"OPT"	},
	{ tabOptB,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"OPTB"	},
	{  tabAlm,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"ALM"	},
	{ tabWarn,	cmdTable,	fieldData,	respEcho | respOK,	accessRO,	"WARN"	},
	};

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller TS Series Master Serial Driver
//

// Instantiator

INSTANTIATE(CYamahaTsDriver);

// Constructor

CYamahaTsDriver::CYamahaTsDriver(void)
{
	m_Ident	= DRIVER_ID; 

	m_pCmd	= (CMD FAR *)m_Cmd;

	m_pTbl  = (CMD FAR *)m_Tbl;

	m_pItem	= NULL;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex	    = Hex;
	}

// Destructor

CYamahaTsDriver::~CYamahaTsDriver(void)
{
	
	}

// Configuration

void MCALL CYamahaTsDriver::Load(LPCBYTE pData)
{
	
	}
	
void MCALL CYamahaTsDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CYamahaTsDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CYamahaTsDriver::Open(void)
{
	}

// Device

CCODE MCALL CYamahaTsDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_Station = GetByte(pData);

			memset(PBYTE(m_pCtx->m_Latest), 0, elements(m_pCtx->m_Latest) * sizeof(DWORD));

			m_pCtx->m_fSrvo	  = 0;

			m_pCtx->m_fBrk	  = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYamahaTsDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) { 

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYamahaTsDriver::Ping(void)
{
	DWORD    Data[1];
	
	CAddress Addr;

	Addr.a.m_Table  = 2;
		
	Addr.a.m_Offset = 1;
		
	Addr.a.m_Type   = addrLongAsLong;

	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CYamahaTsDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	FindCmd(Addr);

	if( m_pItem ) {

		if( !CanRead() ) {

			return uCount;
			}

		if( GetError(Addr, pData, uCount) ) {

			return uCount;
			}

		if( GetInternal(pData) ) {

			return uCount;
			}

		MakeReadReq(Addr);

		if( Transact() ) {

			if( GetData(pData) ) {

				return 1;
				}
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD; 
	}

CCODE MCALL CYamahaTsDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{	
	FindCmd(Addr);

	if( m_pItem ) {

		if( !CanWrite(pData) ) {

			return uCount;
			}

		MakeWriteReq(Addr, pData);

		if( m_TxPtr > SUPP ) {

			PBYTE pError = PBYTE(m_pCtx->m_Latest);

			memset(pError, 0, ERR * 2);

			for( UINT t = 0; t < m_TxPtr - 2; t++ ) {

				pError[t] = m_Tx[t];
				}

			return 1;
			}

		if( Transact() ) {

			SetInternal(pData);

			return 1;
			}
		
		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

void CYamahaTsDriver::FindCmd(AREF Addr)
{
	BOOL fNamed = Addr.a.m_Table == addrNamed;

	UINT uIndex = fNamed ? Addr.a.m_Offset : Addr.a.m_Table;

	UINT uLimit = fNamed ? elements(m_Cmd) : elements(m_Tbl);

	m_pItem     = NULL;

	if( uIndex - 1 < uLimit ) {

		m_pItem = fNamed ? m_pCmd : m_pTbl;

		m_pItem = &m_pItem[uIndex - 1];

		return;
		}
	}

void CYamahaTsDriver::MakeReadReq(AREF Addr)
{
	Begin();

	AddCmd(TRUE);

	AddDataPoint(Addr, NULL);

	AddStation();

	End();
	}

void CYamahaTsDriver::MakeWriteReq(AREF Addr, PDWORD pData)
{
	Begin();

	AddCmd(FALSE);
	
	AddDataPoint(Addr, pData);

	AddStation();

	AddSetting(pData);

	End();
	}

void CYamahaTsDriver::Begin(void)
{
	m_TxPtr = 0;

	memset(m_Tx, 0, elements(m_Tx));

	AddByte('@');
	}

void CYamahaTsDriver::End(void)
{
	AddByte(CR);

	AddByte(LF);
	}

void CYamahaTsDriver::AddCmd(BOOL fRead)
{
	if(  fRead && m_pItem->m_Table != addrNamed ) {

		AddByte('?');
		}

	AddText(m_pItem->m_Text);
	}

void CYamahaTsDriver::AddDataPoint(AREF Addr, PDWORD pData)
{
	UINT uData  = m_pItem->m_Table == addrNamed ? pData[0] : Addr.a.m_Offset;

	if( m_pItem->m_Field == fieldData ) {

		AddNumber(uData);

		return;
		}

	if( m_pItem->m_Field == fieldFlag ) {

		AddNumber(uData ? 1 : 0);

		return;
		}

	if( m_pItem->m_Field == fieldData2 ) {

		AddNumber(Addr.a.m_Offset);

		AddByte('-');

		AddNumber(pData[0]);

		return;
		}
	}

void CYamahaTsDriver::AddStation(void)
{
	AddByte('.');

	AddNumber(m_pCtx->m_Station);
	}

void CYamahaTsDriver::AddSetting(PDWORD pData)
{
	if( CanSetValue() ) {

		AddByte('=');

		AddNumber(pData[0]);
		}
	}

void CYamahaTsDriver::AddByte(BYTE bByte)
{
	m_Tx[m_TxPtr] = bByte;

	m_TxPtr++;
	}

void CYamahaTsDriver::AddText(PCTXT pTxt)
{
	PBYTE pByte = (PBYTE)pTxt;

	while( *pByte != '\0' ) {

		AddByte(*pByte);

		pByte++;
		}
	}

void CYamahaTsDriver::AddNumber(UINT uNum)
{
	BOOL fSet = FALSE;
	
	for( int u = 1000000000; u > 0; u /= 10  ) {

		UINT uDig = uNum / u;
		
		if( uDig || fSet ) {

			AddByte(m_pHex[uDig % 10]);

			uNum = uNum % u;

			fSet = TRUE;
			}
		}

	if( !fSet ) {

		AddByte('0');
		}
	}

BOOL CYamahaTsDriver::GetData(PDWORD pData)
{
	if( strspn(PCTXT(m_Rx), PCTXT(m_Tx)) ) {

		PCTXT pValue = strchr(PCTXT(m_Rx), '=');

		if( pValue ) {

			PTXT pStart = PTXT(pValue + 1);

			pData[0] = ATOI(pStart);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CYamahaTsDriver::GetInternal(PDWORD pData)
{
	if( m_pItem ) {

		if( m_pItem->m_Access == accessSR ) {

			switch( m_pItem->m_ID ) {

				case cmdSrvo:	pData[0] = m_pCtx->m_fSrvo ? TRUE : FALSE;	return TRUE;
				case cmdBrk:	pData[0] = m_pCtx->m_fBrk  ? TRUE : FALSE;	return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CYamahaTsDriver::SetInternal(PDWORD pData)
{
	if( m_pItem ) {

		if( m_pItem->m_Access == accessSR ) {

			switch( m_pItem->m_ID ) {

				case cmdSrvo:	m_pCtx->m_fSrvo = pData[0] ? TRUE : FALSE;	return TRUE;
				case cmdBrk:	m_pCtx->m_fBrk  = pData[0] ? TRUE : FALSE;	return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CYamahaTsDriver::GetError(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pItem ) {

		if( m_pItem->m_Table == tabErr ) {

			MakeMin(uCount, elements(m_pCtx->m_Latest));

			for( UINT i = Addr.a.m_Offset, u = 0; u < uCount; i++, u++ ) {

				DWORD x = m_pCtx->m_Latest[i];

				pData[u] = MotorToHost(x);
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Transport 

BOOL CYamahaTsDriver::Transact(void)
{
	if( Send() && Recv() ) {
		
		return TRUE;	
		}

	return FALSE; 
	}

BOOL CYamahaTsDriver::Send(void)
{
	m_pData->ClearRx();
	
	m_pData->Write(m_Tx, m_TxPtr, FOREVER);

/*	AfxTrace("Tx ");

	for( UINT u = 0; u < m_TxPtr; u++ ) {

		AfxTrace("%c", m_Tx[u]);
		}  

*/	return TRUE;
	}

BOOL CYamahaTsDriver::Recv(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_RxPtr = 0;

	m_RxCr  = 0;

	memset(m_Rx, 0, elements(m_Rx));

//	AfxTrace("\nRx :");

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

	//	AfxTrace("%c", uData);
  
		if( m_RxCr ) {

			if( !CheckFrame() ) {

				m_RxPtr = 0;

				m_RxCr = 0;

				SetTimer(2000);

				continue;
				}

			return TRUE;
			}

		if( uData == CR ) {

			m_RxCr = m_RxPtr;

			SetTimer(50);

			continue;
			}

		m_Rx[m_RxPtr++] = uData;

		if( m_RxPtr >= elements(m_Rx) ) {

			return FALSE;
			}
		}

	if( ExpectNoReply() ) {

		return TRUE;
		}

	return m_RxCr ? TRUE : FALSE;
	}

BOOL CYamahaTsDriver::CheckFrame(void)
{
	char Tx[BUFF];

	char Rx[BUFF];

	PTXT pTx = Tx;

	PTXT pRx = Rx;

	FindReqCmd(pTx);

	FindResCmd(pRx);

	if( CheckResponse(respRun, pRx) ) {

		return TRUE;
		}

	if( CheckResponse(respEnd, pRx) ) {

		return TRUE;
		}

	if( CheckResponse(respOK, pRx) ) {

		if( m_pItem->m_Resp & respEcho ) {
			
			if( strchr(pTx, '=' ) ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	UINT uSize = strspn(pTx, pRx);

	if( uSize >= m_TxPtr - 4 ) {

		return TRUE;
		}

	if( CheckError(pRx, pTx) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CYamahaTsDriver::CheckResponse(UINT uType, PTXT pRx)
{
	if( m_pItem->m_Resp & uType ) {

		PTXT pResp = "";

		switch( uType ) {

			case respOK:	pResp = PTXT("OK.%u");		break;
			case respEnd:	pResp = PTXT("END.%u");		break;
			case respRun:	pResp = PTXT("RUN.%u");		break;
			}

		UINT uLen = strlen(pRx);

		char Data[BUFF] = { };

		PTXT pData = Data;

		SPrintf(pData, pResp, m_pCtx->m_Station);

		if( uLen == strlen(pData) ) {

			if( !memcmp(pRx, pData, uLen) ) {
			
				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CYamahaTsDriver::CheckError(PTXT pRx, PTXT pTx)
{
	UINT uLen = strlen(pRx);

	char Data[BUFF] = { };

	PTXT pData = Data;

	SPrintf(pData, PCTXT("NG.%u"), m_pCtx->m_Station);

	if( uLen == strlen(pData) ) {

		if( !memcmp(pRx, PCTXT(pData), uLen) ) {

			PBYTE pError = PBYTE(m_pCtx->m_Latest);

			memset(pError, 0, ERR * 2);

			for( UINT t = 0; t < ERR; t++ ) {

				pError[t] = pTx[t];
				}

			for( UINT r = ERR; r < m_RxCr + ERR; r++ ) {

				pError[r] = m_Rx[r - ERR];
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Helpers

BOOL CYamahaTsDriver::CanRead(void)
{
	if( m_pItem ) {

		return m_pItem->m_Access != accessWO;
		}

	return FALSE;
	}

BOOL CYamahaTsDriver::CanWrite(PDWORD pData)
{
	if( m_pItem ) {

		if( m_pItem->m_Table == addrNamed ) {

			switch( m_pItem->m_ID ) {

				case cmdSrvo:
				case cmdBrk:

					return TRUE;
				}

			if( pData[0] == 0 ) {

				return FALSE;
				}
			}		

		return m_pItem->m_Access != accessRO;
		}

	return FALSE;
	}

BOOL CYamahaTsDriver::CanSetValue(void)
{
	if( m_pItem ) {

		switch( m_pItem->m_Table ) {

			case tabTeach:
			case tabCopy:
			case tabDel:
			case addrNamed:

				return FALSE;
			}
		}

	return TRUE;
	}

BOOL CYamahaTsDriver::IsInternal(void)
{
	if( m_pItem ) {

		return m_pItem->m_Access == accessSR;
		}

	return FALSE;
	}

BOOL CYamahaTsDriver::ExpectNoReply(void)
{
	if( m_pItem ) {

		return ((m_pItem->m_Resp == respNone) && !m_RxPtr);
		}

	return FALSE;
	}

void CYamahaTsDriver::FindReqCmd(PTXT pTx)
{
	memset(PBYTE(pTx), 0, BUFF);

	UINT i = 0;

	while( !isalpha(m_Tx[i]) ) {

		i++;
		}

	UINT uLen = m_TxPtr - 2 - i;

	for( UINT u = 0; u < uLen; u++ ) {

		pTx[u] = m_Tx[u + i];
		}
	}

void CYamahaTsDriver::FindResCmd(PTXT pRx)
{
	memset(PBYTE(pRx), 0, BUFF);

	UINT i = 0;

	while( i < m_RxCr && m_Rx[i] != '=' ) {

		i++;
		}

	for( UINT u = 0; u < i; u++ ) {

		pRx[u] = m_Rx[u];
		}
	}

// End of File
