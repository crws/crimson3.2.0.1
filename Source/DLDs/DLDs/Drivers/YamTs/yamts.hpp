//////////////////////////////////////////////////////////////////////////
//
// Yamaha TS Series Enumerations
//

enum Cmd {

	cmdStart	= 1,
	cmdStop		= 2,
	cmdOrg		= 3,
	cmdJogP		= 4,
	cmdJogM		= 5,
	cmdInchP	= 6,
	cmdInchM	= 7,
	cmdSrvo		= 8,
	cmdBrk		= 9,
	cmdReset	= 10,
	cmdTable	= 11,
	cmdError	= 12,
	};

enum Table {

	tabErr		= 1,
	tabOp		= 2,
	tabP		= 3,
	tabS		= 4,
	tabAC		= 5,
	tabDC		= 6,
	tabQ		= 7,
	tabZL		= 8,
	tabZH		= 9,
	tabN		= 10,
	tabJ		= 11,
	tabF		= 12,
	tabT		= 13,
	tabTeach	= 14,
	tabCopy		= 15,
	tabDel		= 16,
	tabK		= 17,
	tabD		= 18,
	tabIn		= 19,
	tabInB		= 20,
	tabOut		= 21,
	tabOutB		= 22,
	tabWIn		= 23,
	tabWOut		= 24,
	tabOpt		= 25,
	tabOptB		= 26,
	tabAlm		= 27,
	tabWarn		= 28,	
	};


enum Field {

	fieldNoData	= 0,
	fieldData	= 1,
	fieldDataOffset = 2,
	fieldFlag	= 4,
	fieldData2	= 8,
	fieldNone	= 16,
	};

enum Resp {

	respNone = 0,
	respRun	 = 1,
	respEnd  = 2,
	respOK	 = 4,
	respEcho = 8,
	};

enum Access {

	accessRW = 0,
	accessRO = 1,
	accessWO = 2,
	accessSR = 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Yamaha TS Series Constants
//

#define BUFF	24
#define SUPP	16
#define	ERR	24


//////////////////////////////////////////////////////////////////////////
//
// Yamaha TS Series Structure
//

struct FAR CMD {
		BYTE	m_Table;
		BYTE	m_ID;
		BYTE	m_Field;
		BYTE	m_Resp;
		BYTE	m_Access;
		char	m_Text[8];
		};

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller TS Series Master Serial Driver
//

class CYamahaTsDriver : public CMasterDriver
{
	public:
		// Constructor
		CYamahaTsDriver(void);

		// Destructor
		~CYamahaTsDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		
	protected:
		// Device Context
		struct CContext 
		{
			BYTE  m_Station;
			DWORD m_Latest[ERR*2/4];
			BOOL  m_fSrvo;
			BOOL  m_fBrk;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_Tx[BUFF];
		BYTE	   m_Rx[BUFF];
		UINT	   m_RxPtr;
		UINT	   m_RxCr;
		UINT	   m_TxPtr;
		CMD FAR *  m_pCmd;
		CMD FAR *  m_pTbl;
		CMD FAR *  m_pItem;
		LPCTXT	   m_pHex;

		static CMD CODE_SEG m_Cmd[];
		static CMD CODE_SEG m_Tbl[];
		
		// Implementation
		void FindCmd(AREF Addr);
		void MakeReadReq(AREF Addr);
		void MakeWriteReq(AREF Addr, PDWORD pData);
		void Begin(void);
		void End(void);
		void AddCmd(BOOL fRead);
		void AddDataPoint(AREF Addr, PDWORD pData);
		void AddStation(void);
		void AddSetting(PDWORD pData);
		void AddByte(BYTE bByte);
		void AddText(PCTXT pTxt);
		void AddNumber(UINT uNum);
		BOOL GetData(PDWORD pData);
		BOOL GetInternal(PDWORD pData);
		BOOL SetInternal(PDWORD pData);
		BOOL GetError(AREF Addr, PDWORD pData, UINT uCount);
		
		// Transport
		BOOL Transact(void);
		BOOL Send(void);
		BOOL Recv(void);
		BOOL CheckFrame(void);
		BOOL CheckResponse(UINT uType, PTXT pRx);
		BOOL CheckError(PTXT pRx, PTXT pTx);

		// Helpers
		BOOL CanRead(void);
		BOOL CanWrite(PDWORD pData);
		BOOL CanSetValue(void);
		BOOL IsInternal(void);
		BOOL ExpectNoReply(void);
		void FindReqCmd(PTXT pTx);
		void FindResCmd(PTXT pRx);
		
	};

// End of File
