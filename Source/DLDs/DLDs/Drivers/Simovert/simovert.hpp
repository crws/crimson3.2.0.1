//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert Commands
//

#define	CMD_NONE	0x0
#define CMD_READ	0x1
#define CMD_WRITE	0x2
#define CMD_WRITE_LONG	0x3
#define	CMD_R_ARRAY	0x6
#define	CMD_W_ARRAY_16	0x7
#define	CMD_W_ARRAY_32	0x8
#define	CMD_W_A2EEP_32	0xB
#define	CMD_W_A2EEP_16	0xC
#define CMD_EEPROM_LONG	0xD
#define CMD_EEPROM	0xE
#define CMD_BROADCAST	0x20		

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert Functions
//

#define	FNC_NONE	0x0000

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert Fields
//

#define	FIELD_STW	0x1
#define FIELD_HSW	0x2
#define FIELD_SEND	0x3
#define FIELD_ZSW	0x4
#define FIELD_HIW	0x5

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert Replys
//

#define REPLY_NONE	0x0
#define REPLY_WORD	0x1
#define REPLY_DWORD	0x2
#define	REPLY_ARRAY4	0x4
#define	REPLY_ARRAY5	0x5

//////////////////////////////////////////////////////////////////////////
//
// PKW
//

#define PKW_FIXED3	0x0
#define PKW_FIXED4	0x1
#define PKW_VAR		0x2
#define PKW_VAR_NO	0x3

//////////////////////////////////////////////////////////////////////////
//
// Model Types
//

enum {
	MODELVERT = 1,
	MODELREG  = 3,
	};

// Simoreg extended address
#define	EXTSTART	2000
#define	EXTREGPNU	0x8000

// Table Def's
enum {
	SP_P	= 1,
	SP_LP	= 2,
	SP_I0	= 3,
	SP_I1	= 4,
	SP_I2	= 5,
	SP_L0	= 6,
	SP_L1	= 7,
	SP_L2	= 8,
	SP_PZ	= 9,
	SP_OUT  = 10,
	SP_IN	= 11,
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert via USS Driver
//

class CSimovertDriver : public CMasterDriver
{
	public:
		// Constructor
		CSimovertDriver(void);

		// Destructor
		~CSimovertDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) WriteCommand(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE  m_bDrop;
			WORD  m_wSTW;
			WORD  m_wHSW;
			WORD  m_wZSW;
			WORD  m_wHIW;
			BYTE  m_PZD;
			BYTE  m_PKW;
			BYTE  m_Model;
			WORD  m_Ping;
			UINT  m_uWriteError;
			WORD  m_wPZD[11];
			DWORD m_OUT[11];
			DWORD m_IN[11];
			BOOL  m_fDIR;
			};

		
		CContext * m_pCtx;

		// Data Members
		BYTE m_bRx[256];
		BYTE m_bTx[256];
		BYTE m_Ptr;
		BYTE m_bBcc;
		BYTE m_bLen;

		BOOL m_fIsArray;
		BOOL m_fResend;
			 
		// Frame Building
		void  StartFrame(BYTE bLen);
		void  AddByte(BYTE bData);
		void  AddWord(WORD wData);
		void  AddDWord(DWORD dwData);
		void  AddDrop(void);
		void  AddPZDCache(void);
		void  AddOUTCache(void);
		BYTE  ReadByte(void);
		WORD  ReadWord(void);
		DWORD ReadDWord(void);
		void  ReadPZD(void);
		void  ReadIN(void);
		
		// Transport Layer
		BOOL TxFrame(void);
		BOOL RxFrame(void);

		// Checksum Handler
		void ClearCheck(void);
		void AddToCheck(BYTE b);
		void SendCheck(void);

		// Helpers
		UINT FindBasePNU(AREF Addr);
		UINT FindExtPNU(AREF Addr);
		BOOL IsLong(UINT uType);
		UINT FindReplyID(UINT uNum, UINT uIND);
		BOOL CheckArray(UINT uTable);
		UINT CheckReply(AREF Addr);
		BOOL CheckReplyCode(UINT uPKE);
		BOOL CheckReplyType(UINT uPKE, BOOL fLong);
		BOOL CheckIndex(UINT uOffset, UINT uIND);
		UINT GetWriteCmd(UINT uExtra, BOOL fLong);
		UINT ParseAddr(AREF Addr);
		UINT GetInxIndex(UINT uOffset);
		BOOL IsDirectional(void);
		void MarkLongs(BOOL fIn, UINT uOffset, UINT uType, UINT uCount);
	};

// End of File
