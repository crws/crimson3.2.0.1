
#include "intern.hpp"

#include "yaskmp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Series Master Serial Driver
//

// Instantiator

INSTANTIATE(CYaskawaMPMasterSerialDriver);

// Constructor

CYaskawaMPMasterSerialDriver::CYaskawaMPMasterSerialDriver(void)
{
	m_Ident     = DRIVER_ID; 

	m_uMaxWords = 64;
	
	m_uMaxBits  = 1024;

	m_fAscii    = FALSE;

	m_fTrack    = FALSE;
	}

// Destructor

CYaskawaMPMasterSerialDriver::~CYaskawaMPMasterSerialDriver(void)
{
	}

// Configuration

void MCALL CYaskawaMPMasterSerialDriver::Load(LPCBYTE pData)
{
	AllocBuffers();
	}
	
void MCALL CYaskawaMPMasterSerialDriver::CheckConfig(CSerialConfig &Config)
{
	if ( !m_fAscii && Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}
		
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CYaskawaMPMasterSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CYaskawaMPMasterSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CYaskawaMPMasterSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop      = GetByte(pData);
			m_pCtx->m_fRLCAuto   = FALSE;
			m_pCtx->m_fDisable15 = FALSE;
			m_pCtx->m_fDisable16 = FALSE;
			m_pCtx->m_uMax01     = 1024;
			m_pCtx->m_uMax02     = m_pCtx->m_uMax01;
			m_pCtx->m_uMax03     = 64;
			m_pCtx->m_uMax04     = m_pCtx->m_uMax03;
			m_pCtx->m_uMax15     = 1024;
			m_pCtx->m_uMax16     = 64;

			m_pCtx->m_uPing	     = 0;
			m_pCtx->m_fDisable5  = FALSE;
			m_pCtx->m_fDisable6  = FALSE;
			m_pCtx->m_fNoCheck   = FALSE;
			m_pCtx->m_fNoReadEx  = FALSE;
			m_pCtx->m_uPoll	     = 0;
			m_pCtx->m_fSwapCRC   = FALSE;

			Limit(m_pCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax03, 1, m_uMaxWords);     
			Limit(m_pCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pCtx->m_uMax15, 1, m_pCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pCtx->m_uMax16, 1, m_pCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskawaMPMasterSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskawaMPMasterSerialDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 0x00;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CYaskawaMPMasterSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uType = Addr.a.m_Type;
	
	switch( Addr.a.m_Table ) {

		case SPACE_MW:
		case SPACE_IW:

			if( uType == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax03));
				}
			
			return DoLongRead(Addr, pData, min(uCount, m_pCtx->m_uMax03/2));

		case SPACE_MB:
		case SPACE_IB:

			return DoBitRead(Addr, pData, min(uCount, m_pCtx->m_uMax01));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CYaskawaMPMasterSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uType = Addr.a.m_Type;

	switch( Addr.a.m_Table ) {

		case SPACE_MW:

			if( uType == addrWordAsWord ) {

				return DoWordWrite(Addr, pData, min(uCount, m_pCtx->m_uMax16));
				}

			return DoLongWrite(Addr, pData, min(uCount, m_pCtx->m_uMax16/2));

		case SPACE_MB:
			
			return DoBitWrite(Addr, pData, min(uCount, m_pCtx->m_uMax15));

		case SPACE_IW:
		case SPACE_IB:

			return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Read Handlers

CCODE CYaskawaMPMasterSerialDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOpcode = 0x03;
	
	switch( Addr.a.m_Table ) {

		case SPACE_IW:

			uOpcode = 0x04;

			break;
		}
	
	StartFrame(uOpcode);

	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x = PU2(m_pRx + 3)[n];

			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CYaskawaMPMasterSerialDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOpcode = 0x03;
	
	switch( Addr.a.m_Table ) {

		case SPACE_IW:

			uOpcode = 0x04;

			break;
		}
	
	StartFrame(uOpcode);

	AddWord(Addr.a.m_Offset);

	AddWord(uCount * 2);
	
	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x = PU4(m_pRx + 3)[n] << 16;

			x |= PU4(m_pRx + 3)[n] >> 16;

			pData[n] = MotorToHost(x);

			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CYaskawaMPMasterSerialDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
			
		case SPACE_MB:

			StartFrame(0x01);

			break;
			
		case SPACE_IB:
		
			StartFrame(0x02);

			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_pRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CYaskawaMPMasterSerialDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( uCount == 1 ) {
		
		StartFrame(0x06); 
			
		AddWord(Addr.a.m_Offset);
		}	
		
       	else {
		StartFrame(0x10);

		AddWord(Addr.a.m_Offset);
			
		AddWord(uCount);

		AddByte(uCount * 2);
		}
			
	for( UINT n = 0; n < uCount; n++ ) {

		AddWord(LOWORD(pData[n]));
		}
	
	if( Transact(TRUE) ) {
			
		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CYaskawaMPMasterSerialDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(0x10);
			
	AddWord(Addr.a.m_Offset);

	AddWord(uCount * 2);
			
	AddByte(uCount * 4);
			
	for( UINT n = 0; n < uCount; n++ ) {
				
		AddWord(LOWORD(pData[n]));

		AddWord(HIWORD(pData[n]));
		}
	
	if( Transact(TRUE) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CYaskawaMPMasterSerialDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
        if( uCount == 1 ) {
		
		StartFrame(0x05);
			
		AddWord(Addr.a.m_Offset);

		AddWord(pData[0] ? 0xFF00 : 0x0000);
		}
	
	else {
		StartFrame(0x0F);
	
		AddWord(Addr.a.m_Offset);

		AddWord(uCount);
		
		AddByte((uCount + 7) / 8);

		UINT b = 0;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			if( pData[n] ) {
					
				b |= m;
				}

			if( !(m <<= 1) ) {

				AddByte(b);

				b = 0;

				m = 1;
				}
			}
		
		if( m > 1 ) {
	
			AddByte(b);
			}
		}

	if( Transact(TRUE) ) {
			
		return uCount;
		}

	return CCODE_ERROR;
	}

// End of File
