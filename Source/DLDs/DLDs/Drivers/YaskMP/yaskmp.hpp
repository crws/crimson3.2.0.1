#include "modbus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Memobus Spaces
//

#define	SPACE_MW	1
#define	SPACE_IW	2
#define	SPACE_MB	3
#define	SPACE_IB	4

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Series Master Serial Driver
//

class CYaskawaMPMasterSerialDriver : public CModbusDriver
{
	public:
		// Constructor
		CYaskawaMPMasterSerialDriver(void);

		// Destructor
		~CYaskawaMPMasterSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
	
		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
	};
