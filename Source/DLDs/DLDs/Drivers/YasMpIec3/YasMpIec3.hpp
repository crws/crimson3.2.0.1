//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP 3000 Series IEC TCP/IP Master Driver
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "yasmpiec.hpp"

// Spaces

enum {
	SP_9	=  9,
	SP_10	= 10,
	SP_MB	= 29,
	SP_M32  = 30,
	SP_M64	= 31,	
	};

class CYaskawaMpIec3Master : public CYaskawaMPIECMaster
{
	public:
		// Constructor
		CYaskawaMpIec3Master(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
						
		// Entry Points
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CCtxMp3 : public CYaskawaMPIECMaster::CContext
		{
			BYTE m_MemWordSwap;
			};

		// Data Members
		CCtxMp3 * m_pCtxMp3;

		// Implementation
		DWORD GetRef(AREF Addr);
		void  SwapMemWords(AREF Addr, PDWORD pData, UINT uCount);
		DWORD SwapWords(DWORD dwData);
		void  Swap64Bit(PDWORD pData);
		BOOL  IsMemReg(UINT uTable);
		BOOL  IsModbus(UINT uTable);

		// Overridables
		UINT  GetModbusSpace(CAddress Addr);
		BOOL  Is64Bit(UINT uTable);
		DWORD Convert64To32(PDWORD pData, UINT uTable);
		void  Convert32To64(DWORD d, PDWORD pData, UINT uTable);
	};

// End of File
