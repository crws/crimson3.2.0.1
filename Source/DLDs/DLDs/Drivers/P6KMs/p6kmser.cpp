#include "intern.hpp"

#include "p6kmser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Parker Compumotor 6K Master Serial Driver
//

// Instantiator

INSTANTIATE(CParker6KMasterDriver);

// Constructor

CParker6KMasterDriver::CParker6KMasterDriver(void)
{
	m_Ident         = DRIVER_ID;

	m_fInit		= FALSE;

	}

// Destructor

CParker6KMasterDriver::~CParker6KMasterDriver(void)
{
	}

// Configuration

void MCALL CParker6KMasterDriver::Load(LPCBYTE pData)
{

	}
	
void MCALL CParker6KMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CParker6KMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CParker6KMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CParker6KMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pBase = (CBaseCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pBase = new CBaseCtx;

			m_pBase->m_bDrop = GetByte(pData);

			m_pBase->m_uInitTime = GetWord(pData);

			m_pBase->m_bDevice = GetByte(pData);

			memset(m_pBase->m_Arg1, 0, sizeof(m_pBase->m_Arg1));

			pDevice->SetContext(m_pBase);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CParker6KMasterDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CParker6KMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SEL_A;
	Addr.a.m_Offset = 1;
	Addr.a.m_Type   = addrRealAsReal;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CParker6KMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uIndex = FindIndex(Addr.a.m_Table, Addr.a.m_Offset);

	FindCmd(uIndex);

	if ( m_pItem->m_Access == ACC_W ) {

		return uCount;
		}

	UINT uOffset = Addr.a.m_Offset;

	UINT uMax = m_pItem->m_Form & FORM_STR ? uCount : 1;

	if( m_pItem->m_Form == (FORM_VAR | FORM_STR) ) {

		uOffset -= 64;

		if( uOffset % 13 != 0 ) {

			return uCount;
			}
		
		uMax = 13;
		}

	MakeMin(uCount, uMax);

	CCODE Code = CCODE_ERROR;
	
	if ( m_pCmd ) {

		if( GetArg(pData, uCount) ) {

			return uCount;
			}
	
		Start();
	
		if ( !PutRead(uOffset, Addr.a.m_Type) ) {

			*pData = 0;

			return uCount;
			}

		Code = GetResponse(FALSE);

		if ( COMMS_SUCCESS(Code) ) {

			if( !m_fInit ) {

				if( GetRead(uOffset, Addr.a.m_Type, pData, uCount ) ) {

					return uCount; 
					}
				}

			m_fInit = FALSE;

			return uCount;
			} 
		}
	
	return Code;

  	}

CCODE MCALL CParker6KMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uIndex = FindIndex(Addr.a.m_Table, Addr.a.m_Offset);
	
	FindCmd(uIndex);

	if ( m_pItem->m_Access == ACC_R ) {

		return uCount;
		}

	if ( m_pItem->m_Form == FORM_ARG && pData[0] == 0 ) {

		return uCount;
		}

	UINT uOffset = Addr.a.m_Offset;

	UINT uMax = m_pItem->m_Form & FORM_STR ? uCount : 1;

	if( m_pItem->m_Form == (FORM_VAR | FORM_STR) ) {

		uOffset -= 64;

		if( uOffset % 13 != 0 ) {

			return uCount;
			}

		uMax = 13;
		}

	MakeMin(uCount, uMax);

	CCODE Code = CCODE_ERROR;

	if ( m_pCmd ) {

		if( SetArg(pData, uCount) ) {

			return uCount;
			}
	
		Start();

		if ( PutWrite(uOffset, Addr.a.m_Type, pData, Addr.a.m_Extra & 0x01) ) {

			Code = GetResponse(TRUE);

			if( COMMS_SUCCESS(Code) ) {

				if( m_pItem->m_Response != RESP_NONE && m_pItem->m_Access == ACC_RW ) { 

					Start();
	
					if( PutRead(uOffset, Addr.a.m_Type) ) {
				
						Code = GetResponse(FALSE);

						if ( COMMS_SUCCESS(Code) ) {

							PDWORD Data = PDWORD(alloca(uCount * 4));

							if( GetRead(uOffset, Addr.a.m_Type, Data, uCount ) ) {

								return uCount;
								}
							}
						}
					}
				else {
					return uCount;
					}
				}
			}
		}
	
	return Code;
	}
	

// Implementation

BOOL CParker6KMasterDriver::PutRead(UINT uOffset, UINT uType)
{
	if ( !PutReadCommand( uOffset, uType ) ) {
		
		return FALSE;
		}
			
	End();

	return TRUE;
	}

BOOL CParker6KMasterDriver::PutWrite(UINT uOffset, UINT uType, PDWORD pData, BOOL fGlobal)
{
	if( !PutWriteCommand(uOffset, uType, pData, fGlobal) ) {

		return FALSE;
		}
	
	End();

	return TRUE;
	}

void CParker6KMasterDriver::End(void)
{
    	PutChar( CR );

	Send();
	}

void CParker6KMasterDriver::Send(void)
{
	m_pData->ClearRx();
	
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);
	}
	
CCODE CParker6KMasterDriver::GetResponse(BOOL fWrite)
{	
	if ( m_pItem ) {

		if ( m_pItem->m_Response == RESP_NONE ) {

			Sleep(50);

			return CCODE_SUCCESS;
			}
		}
	
	UINT uPtr = 0;

	UINT uTxPtr = 0;

	for ( UINT u = 0; u < m_uPtr; u++ ) {

		if ( m_bTxBuff[u] == '!' ) {
			
			uTxPtr = ++u;

			break;
			}
		}

	BOOL fAsterRcvd = FALSE;

	PCTXT pInit = "PARKER";

	UINT uInitPtr = 0;
	
	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	UINT uTimer = 0;

	UINT uByte = 0;

	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if ( uByte == '?' ) { 

			continue;
			}

		if ( fAsterRcvd ) {

			if ( m_bTxBuff[uTxPtr] != CR ) {

				if ( uByte != m_bTxBuff[uTxPtr] ) {

					if ( uByte == (UINT)pInit[uInitPtr] ) {

						if ( uInitPtr == sizeof(pInit) - 1 ) {

							UINT uTimer = 0;

							UINT uByte = 0;

							SetTimer(m_pBase->m_uInitTime);

							while ( (uTimer = GetTimer()) ) {

								uByte = m_pData->Read(uTimer);

								}
							
							m_fInit = TRUE;

							return CCODE_SUCCESS;
							 
							}

						uInitPtr++;

						continue;
						}
					
					return CCODE_ERROR;
					}
				
				uTxPtr++;
				}

			m_bRxBuff[uPtr++] = uByte;

			if ( uByte == CR ) {

				return CCODE_SUCCESS;
				}
			}
		
		else if ( uByte == '*' ) {

			fAsterRcvd = TRUE;

			uPtr = 0;
			
			}

		else if ( ( uByte == CR ) && fWrite ) {

			Sleep(50);

			return CCODE_SUCCESS;
			}

		if ( uPtr >= sizeof(m_bRxBuff) ) {
		
			return CCODE_ERROR;
			}
		}
	
	return CCODE_ERROR;
	}

 
// End of File
