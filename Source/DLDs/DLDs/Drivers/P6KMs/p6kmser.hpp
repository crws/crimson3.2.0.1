#include "p6k.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Parker Compumotor 6K Master Serial Driver
//

class CParker6KMasterDriver : public CParker6KBaseDriver
{
	public:
		// Constructor
		CParker6KMasterDriver(void);

		// Destructor
		~CParker6KMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		BOOL m_fInit;

		// Implementation
		BOOL PutRead(UINT uOffset, UINT uType);
	
		BOOL PutWrite(UINT uOffset, UINT uType, PDWORD pData, BOOL fGlobal);
	
		CCODE GetResponse(BOOL fWrite);
	
		void End(void);
	 
		void Send(void);
	  	
	};

// End of File
