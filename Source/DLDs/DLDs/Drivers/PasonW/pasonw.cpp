#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "pasonw.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Pason WITS Driver
//

// Instantiator

INSTANTIATE(CPasonWITSDriver);

// Constructor

CPasonWITSDriver::CPasonWITSDriver(void)
{
	m_Ident		= DRIVER_ID;

	CTEXT Hex[]	= "0123456789abcdef"; // there's a reason for lower case alpha's

	m_pHex		= Hex;

	CTEXT Prefix[]	= "PASON1984/EDR";

	m_pPrefix	= Prefix;

	CTEXT Neg[]	= "GHIJKLMNOPQRSTUV";

	m_pNeg		= Neg;

	CTEXT CDP[]	= "QRSTUVWXYZ";

	m_pCDP		= CDP;

	m_uWEnd		= 0;
	}

// Destructor

CPasonWITSDriver::~CPasonWITSDriver(void)
{
	}

// Configuration

void  MCALL CPasonWITSDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CPasonWITSDriver::CheckConfig(CSerialConfig &Config)
{
	}

// Management

void  MCALL CPasonWITSDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void  MCALL CPasonWITSDriver::Open(void)
{
	}

// Device

CCODE MCALL CPasonWITSDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		m_pCtx = new CContext;

		memset(m_pCtx->m_bRecv,  0, SZSR * 2);

		memset(m_pCtx->m_bWITS,  0, SZSR);

		m_pCtx->m_fIsWITSIn = FALSE;

		m_pCtx->m_uDelay = 0;

		pDevice->SetContext(m_pCtx);

		return CCODE_SUCCESS;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CPasonWITSDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CPasonWITSDriver::Ping(void)
{
	m_uBusy = 0;

	return 1;
	}

CCODE MCALL CPasonWITSDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	GetReceive();

	switch( Addr.a.m_Table ) {

		case SP_REAL:
//**/	AfxTrace3("\r\n*** READ T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);
			break;

		case SP_INT:
//**/	AfxTrace3("\r\n*** READ T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);
			break;

		case SP_PREF:
			*pData = m_pCtx->m_dUsePrefix == WMAGIC;
			break;

		case SP_DELY:
			*pData = m_pCtx->m_uDelay;
			break;

		case SP_RECV:
			PutReceive(pData, Addr.a.m_Offset, uCount);
			m_uBusy = 0;
			break;
		}

	return uCount;
	}

void CPasonWITSDriver::PutReceive(PDWORD pData, UINT uStart, UINT uCount)
{
	memset(pData, 0, uCount * sizeof(DWORD));

	UINT uCt = min(uCount, SZSRW);

	for( UINT i = 0; i < uCt; i++ ) {

		DWORD x  = ((PU4)m_pCtx->m_bWITS)[i];

		pData[i] = MotorToHost(x);
		}
	}

CCODE MCALL CPasonWITSDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n*** WRITE T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( Addr.a.m_Table != SP_REAL && Addr.a.m_Table != SP_INT ) {

		switch( Addr.a.m_Table ) {

			case SP_PREF:
				m_pCtx->m_dUsePrefix = *pData ? WMAGIC : 0;
				return 1;

			case SP_DELY:
				m_pCtx->m_uDelay = *pData;
				return 1;

			case SP_RECV:
				memset(m_pCtx->m_bRecv, 0, SZSR);
				return 1;
			}

		return uCount;
		}

	if( m_uBusy % 3 ) {

		m_uBusy--;

		return uCount; // don't trample on end device transmit
		}

	AddStart();

	if( UsePrefix() ) {

		AddText(m_pPrefix);
		EndLine();
		}

	for( UINT uScan = 0; uScan < uCount; uScan++ ) {

		AddCode(Addr.a.m_Offset + uScan);

		Addr.a.m_Table == SP_REAL ? AddReal(pData[uScan]) : AddInt(pData[uScan]);

		EndLine();

		Put();

		m_uPtr = 0;
		}

	EndFrame();

	GetReceive();

	return uCount;
	}

// Implementation

void  CPasonWITSDriver::AddStart(void)
{
	m_uPtr = 0;

	AddByte('&');
	AddByte('&');

	EndLine();
	}

void  CPasonWITSDriver::AddCode(UINT uTable)
{
	AddByte(m_pHex[(uTable / 1000) % 10]);
	AddByte(m_pHex[(uTable /  100) % 10]);
	AddByte(m_pHex[(uTable /   10) % 10]);
	AddByte(m_pHex[ uTable         % 10]);
	}

// Frame Building

void  CPasonWITSDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr++]  = bData;
		}
	}

void  CPasonWITSDriver::AddText(PCTXT pData)
{
	if( pData[0] ) {

		UINT uLen = min(strlen(pData), sizeof(m_bTx) - m_uPtr);

		if( !uLen ) {

			EndFrame();

			return;
			}

		for( UINT i = 0; i < uLen; i++ ) {

			BYTE b = pData[i];

			if( b ) AddByte(b);
			}
		}
	}

void  CPasonWITSDriver::AddReal(DWORD dData)
{
	HostAlignStack();

	char c[20];

	SPrintf(c, "%f", PassFloat(dData));

	Round(PCTXT(c));
	}

void  CPasonWITSDriver::AddInt(DWORD dData)
{
	char c[20];

	SPrintf(c, "%d.00", dData);

	AddText(PCTXT(c));
	}

void  CPasonWITSDriver::EndLine(void)
{
	AddByte(13);

	AddByte(10);
	}

void  CPasonWITSDriver::EndFrame(void)
{
	m_uPtr = 0;

	AddByte('!');
	AddByte('!');

	EndLine();

	Put();
	}

// Transport Layer

void  CPasonWITSDriver::GetReceive(void)
{
	UINT uData  = NOTHING;

	UINT uTimer = 100;

	if( (uData = Get(100)) == NOTHING ) return;

	BYTE bData = LOBYTE(uData);

	m_pCtx->m_bRecv[0] = bData;

//**/	AfxTrace1("\r\n<%2.2x>", bData);

	UINT uPos = 1;

	m_uBusy   = 2;

	SetTimer(uTimer);

	while( GetTimer() ) {

		if( (uData = Get(40)) == NOTHING ) {

			memcpy(m_pCtx->m_bWITS, m_pCtx->m_bRecv, uPos);

			break;
			}

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);

		m_pCtx->m_bRecv[uPos++] = bData;

		uPos %= SZRCV;

		SetTimer(100);
		}

	memcpy(m_pCtx->m_bWITS, m_pCtx->m_bRecv, uPos);
	}

void  CPasonWITSDriver::ProcessWITS(void)
{
	PBYTE pS   = m_pCtx->m_bWITS;

	UINT uSPos = 0;

	UINT uDPos = 0;

	memset(m_bProc, 0, SZSR);

	while( uSPos < m_uWEnd ) {

		BYTE b = pS[uSPos];

		if( IsParam(&pS[uSPos]) ) { // Check for valid Param Number

			memcpy(&m_bProc[uDPos], &pS[uSPos], 4);

			uSPos += 4;
			uDPos += 4;
			}

		else return;

		UINT uDP  = 6;

		DWORD d   = 0;
		DWORD d1  = 0;

		UINT i    = 0;

		BOOL fNeg = FALSE;

		BOOL fDP  = FALSE;

		BOOL fGo  = TRUE;

		b = pS[uSPos++];

		while( b != CR && uSPos < m_uWEnd && uDPos < SZSR ) { // create int value from string

			if( !IsNumericChar(b) ) return;

			if( fGo ) {

				switch( b ) {

					case '-':
						fNeg = TRUE;
						i++;
						break;

					case '+':
						break;

					case '.':
						fDP = TRUE;

						break;

					default:
						d1 = (d * 10) + (b - '0');

						if( d1 < 0x1000000 ) {

							d = d1;

							if( fDP ) uDP--;

							i++;
							}

						else fGo = FALSE;
					break;
					}
				}

			b = pS[uSPos++];
			}

		DWORD dDiv  = 0x100000; // make hex 6 char string from int value

		BYTE bL[6]  = {0};

		for( i = 0; i < 6; i++ ) { // get raw hex nibbles

			bL[i] = (d / dDiv) % 16;

			dDiv >>= 4;
			}

		b = bL[0];

		m_bProc[uDPos++] = fNeg ? m_pNeg[b] : m_pHex[b];

		for( i = 1; i < 6; i++ ) {

			BYTE b1 = bL[i];

			b = m_pHex[b1];

			if( uDP < 6 && i == uDP ) {

				m_bProc[uDPos++] = b1 <= 9 ? m_pCDP[b1] : b & 0x5F;
				}

			else m_bProc[uDPos++] = b;
			}

		if( uDPos >= SZSR ) break;
		}

	if( uSPos >= m_uWEnd || uDPos >= SZSR ) {

		memset(m_pCtx->m_bWITS, 0, SZRCV);

		memcpy(m_pCtx->m_bWITS, m_bProc, SZSR);
		}
	}

// Port Access

void  CPasonWITSDriver::Put(void)
{
//**/	AfxTrace0("\r\n");
//**/	for( UINT i = 0; i < m_uPtr; i++ ) {
//**/		AfxTrace1("[%2.2x]", m_bTx[i] );
//**/		if( m_bTx[i] == 0xA ) AfxTrace0("\r\n");
//**/		}

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

UINT  CPasonWITSDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

void CPasonWITSDriver::Round(PCTXT pText)
{
	UINT uLen  = strlen(pText);

	UINT uPos  = 0;

	while( uPos < uLen ) {

		if( pText[uPos++] == '.' ) {

			break;
			}
		}

	UINT uILen = uPos - 1;

	char cI[20];

	memset(cI, 0, 20);

	char cD[20];

	memset(cD, 0, 20);

	memcpy(cI, pText, uPos);

	for( UINT i = 0; i < min(uLen, uPos + 3); i++ ) {

		cD[i] = pText[uPos + i];
		}

	BOOL fIntUp = FALSE;

	if( uLen - uPos > 2 ) { // enough chars to round

		if( cD[2] >= '5' ) {

			cD[1]++;

			if( cD[1] > '9' ) {

				cD[1] = '0';

				cD[0]++;

				if( cD[0] > '9' ) {

					cD[0] = '0';

					fIntUp   = TRUE;
					}
				}
			}
		}

	cD[2] = 0;

	if( fIntUp ) {

		UINT uInt = ATOI(cI) + 1;

		memset(cI, 0, uLen + 1);

		SPrintf(cI, "%d", uInt);

		cI[strlen(cI)] = '.';
		}

	AddText(cI);
	AddText(cD);
	}

BOOL  CPasonWITSDriver::UsePrefix(void)
{
	return m_pCtx->m_dUsePrefix == WMAGIC;
	}

BOOL  CPasonWITSDriver::IsNumericChar(BYTE b)
{
	return IsDigit(b) || b == '.' || b == '-' || b == '+';
	}

BOOL  CPasonWITSDriver::IsDigit(BYTE b)
{
	return b >= '0' && b <= '9';
	}

BOOL  CPasonWITSDriver::IsParam(PBYTE pb)
{
	for( UINT i = 0; i < 4; i++ ) {

		if( !IsDigit(pb[i]) ) {

			return FALSE;
			}
		}

	return TRUE;
	}

UINT  CPasonWITSDriver::ResetWITS(void)
{
	m_pCtx->m_fIsWITSIn = FALSE;

	return 0;
	}

// End of File
