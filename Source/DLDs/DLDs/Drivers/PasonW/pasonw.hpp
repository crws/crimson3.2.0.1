
//////////////////////////////////////////////////////////////////////////
//
// Pason WITS Driver Data Spaces
//

#define	SP_REAL	0x01
#define	SP_INT	0x02
#define	SP_DELY	0x03
#define	SP_PREF	0x04
#define	SP_RECV	0x05

#define	WMAGIC	0x12345678

#define	SZRCV	320
#define	SZSR	160
#define	SZSRW	(SZSR / sizeof(DWORD))

//////////////////////////////////////////////////////////////////////////
//
// Pason WITS Driver
//

class CPasonWITSDriver : public CMasterDriver
{
	public:
		// Constructor
		CPasonWITSDriver(void);

		// Destructor
		~CPasonWITSDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bRecv[SZRCV];
			BYTE	m_bWITS[SZRCV];
			BOOL	m_fIsWITSIn;
			UINT	m_uDelay;
			DWORD	m_dUsePrefix;
			};

		// Data Members
		CContext *m_pCtx;

		BYTE	m_bTx[256];
		BYTE	m_bProc[SZSR];

		UINT	m_uPtr;
		UINT	m_uWEnd;
		UINT	m_uDP;
		UINT	m_uBusy;

		PCTXT	m_pHex;
		PCTXT	m_pPrefix;
		PCTXT	m_pNeg;
		PCTXT	m_pCDP;

		// Implementation
		void	PutReceive(PDWORD pData, UINT uStart, UINT uCount);
		void	ProcessWITS(void);

		void	AddStart(void);
		void	AddCode (UINT uTable);

		// Frame Building
		void	AddByte(BYTE  bData);
		void	AddText(PCTXT  pData);
		void	AddReal(DWORD dData);
		void	AddInt(DWORD dData);
		void	EndLine(void);
		void	EndFrame(void);

		// Transport Layer
		void	GetReceive(void);

		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		void	Round(PCTXT pText);
		BOOL	UsePrefix(void);
		BOOL	IsNumericChar(BYTE b);
		BOOL	IsDigit(BYTE b);
		BOOL	IsParam(PBYTE pb);
		UINT	ResetWITS(void);
		BYTE	MakeWNeg(BYTE b);
	};

// End of File
