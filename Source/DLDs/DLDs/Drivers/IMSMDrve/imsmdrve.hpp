
//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Universal SMC Driver
//

#define IMSMDRIVE_ID 0x4022

struct FAR IMSMDriveCmdDef {
	UINT	uID;
	char 	sName[4];	
	};

class CIMSMDriveDriver : public CMasterDriver
{
	public:
		// Constructor
		CIMSMDriveDriver(void);

		// Destructor
		~CIMSMDriveDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(UINT)	DevCtrl(void * pContext, UINT uFunc, PCTXT Value);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bName;

			// Cache Data
			DWORD	m_EX[4];
			DWORD	m_TI[4];
			DWORD	m_TP[4];
			DWORD	m_MA;
			DWORD	m_MR;
			};

		CContext * m_pCtx;

		// Data Members
		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		UINT	m_uPtr;

		static	IMSMDriveCmdDef CODE_SEG CL[];
		IMSMDriveCmdDef FAR * m_pCL;
		IMSMDriveCmdDef FAR * m_pItem;

		// Hex Lookup
		LPCTXT	m_pHex;

		// Initiate
		void	Initiate(void);
	
		// Opcode Handlers
		void	DoRead(UINT uOffset);
		void	DoWrite(UINT uOffset, PDWORD pData);
		
		// Frame Building
		void	StartFrame(BOOL fIsRead);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData);
		void	AddCommand(UINT uOffset, BOOL fIsIOAll);
		void	AddLabelData(PDWORD pLabel);
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		void	GetResponse(PDWORD pData);
		void	GetSIResponse(PDWORD pData);
		
		// Port Access
		void	Put(void);
		WORD	Get(UINT uTimer);

		// Helpers
		BOOL	SetpItem(AREF Addr);
		BOOL	ReadNoTransmit(PDWORD pData, UINT uCount);
		BOOL	WriteNoTransmit(PDWORD pData, UINT uCount);
		void	CopyStringData(PDWORD pSrc, PDWORD pDest, UINT uCount, BOOL fClear);
		UINT	GetWriteDataType(UINT uOffset);
		BOOL	IsString(void);
	};

#define	AN		addrNamed

#define	NA_STRING	0x4E2F4100 // "N/A"

#define	NO_WDATA	0
#define	BOOL_WDATA	1
#define	INT_WDATA	2
#define	SI_WDATA	3
#define	EX_WDATA	4
#define	TI_WDATA	5
#define	TP_WDATA	6

#define	CMA	0x01 //	Acceleration
#define	CMD	0x02 //	Deceleration
#define	CMDE	0x03 //	Drive Enable
#define	CMEX	0x04 //	Execute Program
#define	CMEXL	0x05 //	Execute Program, Label String
#define	CMHC	0x06 //	Hold Current %
#define	CMHI	0x07 //	Home to Index Mark
#define	CMHM	0x08 //	Home to Home Switch
#define	CMHT	0x09 //	Hold Current Delay Time
#define	CMJE	0x0A //	Jog Enable Flag
#define	CMLM	0x0B //	Limit Stop Mode
#define	CMMA	0x0C //	Move to Absolute Position
#define	CMMD	0x0D //	Motion Mode Setting
#define	CMMR	0x0E //	Move to Relative Position
#define	CMMS	0x0F //	Microstep Resolution
#define	CMMT	0x10 //	Motor Settling Delay Time
#define	CMMV	0x11 //	Moving Flag
#define	CMPS	0x12 //	Pause Program
#define	CMRC	0x13 //	Run Current %
#define	CMRS	0x14 //	Resume Paused Program
#define	CMSL	0x15 //	Slew Axis
#define	CMTIE	0x16 //	Trip On Input, Execute
#define	CMTIL	0x17 //	Trip On Input, Label String
#define	CMTPE	0x18 //	Trip On Position, Execute
#define	CMTPL	0x19 //	Trip On Position, Label String
#define	CMV	0x1A //	Current Velocity
#define	CMVC	0x1B //	Velocity Changing Flag
#define	CMVI	0x1C //	Initial Velocity
#define	CMVM	0x1D //	Maximum Velocity
#define	CMDF	0x20 //	Input Digital Filtering
#define	CMI	0x21 //	Read Input Value
#define	CMO	0x22 //	Set Output
#define	CMSI	0x23 //	Setup IO Point
#define	CMTE	0x24 //	Trip Enable
#define	CMC1	0x30 //	Counter 1 Motor Counts
#define	CMP	0x31 //	Position Motor/Encoder Counts
#define	CMPC	0x32 //	Capture Position at Trip
#define	CMC2	0x40 //	Counter 2
#define	CMDB	0x41 //	Encoder Deadband
#define	CMEE	0x42 //	Enable/Disable Encoder Functions
#define	CMPM	0x43 //	Position Maintenance Enable
#define	CMSF	0x44 //	Stall Factor
#define	CMSM	0x45 //	Stall Mode
#define	CMST	0x46 //	Stall Flag
#define	CMBY	0x50 //	BSY Flag
#define	CMEF	0x51 //	Error Flag
#define	CMER	0x52 //	Error Number
#define	CMFD	0x53 //	Factory Defaults
#define	CMIP	0x54 //	Initialize Parameters
#define	CMS	0x55 //	Save to EEProm
#define	CMUR	0x56 //	User Register
#define	CMUV	0x57 //	User Variable
#define	CMVR	0x58 //	Firmware Version String

// End of File
