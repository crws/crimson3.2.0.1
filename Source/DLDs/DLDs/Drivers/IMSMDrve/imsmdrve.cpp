
#include "intern.hpp"

#include "imsmdrve.hpp"

IMSMDriveCmdDef CODE_SEG CIMSMDriveDriver::CL[] = {

	{CMDF,	"D#"},	// Input Digital Filtering
	{CMI,	"I#"},	// Read Input Value
	{CMO,	"O#"},	// Set Output
	{CMSI,	"S#"},	// Setup IO Point
	{CMTE,	"TE"},	// Trip Enable

	{CMC1,	"C1"},	// Counter 1 Motor Counts
	{CMP,	"P"},	// Position Motor/Encoder Counts
	{CMPC,	"PC"},	// Capture Position at Trip

	{CMC2,	"C2"},	// Counter 2
	{CMDB,	"DB"},	// Encoder Deadband
	{CMEE,	"EE"},	// Enable/Disable Encoder Functions
	{CMPM,	"PM"},	// Position Maintenance Enable
	{CMSF,	"SF"},	// Stall Factor
	{CMSM,	"SM"},	// Stall Mode
	{CMST,	"ST"},	// Stall Flag

	{CMBY,	"BY"},	// BSY Flag
	{CMEF,	"EF"},	// Error Flag
	{CMER,	"ER"},	// Error Number
	{CMFD,	"FD"},	// Factory Defaults
	{CMIP,	"IP"},	// Initialize Parameters
	{CMS,	"S"},	// Save to EEProm
	{CMUR,	"R#"},	// User Register
	{CMUV,	"UV"},	// User Variable
	{CMVR,	"VR"},	// Firmware Version Number

	{CMA,	"A"},	// Acceleration
	{CMD,	"D"},	// Deceleration
	{CMDE,	"DE"},	// Drive Enable
	{CMEX,	"EX"},	// Execute Program, Mode
	{CMEXL,	"_X"},	// Execute Program, Label
	{CMHC,	"HC"},	// Hold Current %
	{CMHI,	"HI"},	// Home to Index Mark
	{CMHM,	"HM"},	// Home to Home Switch
	{CMHT,	"HT"},	// Hold Current Delay Time
	{CMJE,	"JE"},	// Jog Enable Flag
	{CMLM,	"LM"},	// Limit Stop Mode
	{CMMA,	"MA"},	// Move to Absolute Position
	{CMMD,	"MD"},	// Motion Mode Setting
	{CMMR,	"MR"},	// Move to Relative Position
	{CMMS,	"MS"},	// Microstep Resolution
	{CMMT,	"MT"},	// Motor Settling Delay Time
	{CMMV,	"MV"},	// Moving Flag
	{CMPS,	"PS"},	// Pause Program
	{CMRC,	"RC"},	// Run Current %
	{CMRS,	"RS"},	// Resume Paused Program
	{CMSL,	"SL"},	// Slew Axis
	{CMTIE,	"TI"},	// Trip On Input, Execute
	{CMTIL,	"_I"},	// Trip On Input, Label
	{CMTPE,	"TP"},	// Trip On Position, Execute
	{CMTPL,	"_P"},	// Trip On Position, Label
	{CMV,	"V"},	// Current Velocity
	{CMVC,	"VC"},	// Velocity Changing Flag
	{CMVI,	"VI"},	// Initial Velocity
	{CMVM,	"VM"},	// Maximum Velocity
	};

//////////////////////////////////////////////////////////////////////////
//
// IMSMDrive Driver
//

// Instantiator

INSTANTIATE(CIMSMDriveDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CIMSMDriveDriver::CIMSMDriveDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Destructor

CIMSMDriveDriver::~CIMSMDriveDriver(void)
{
	}

// Configuration

void MCALL CIMSMDriveDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CIMSMDriveDriver::CheckConfig(CSerialConfig &Config)
{
	}
	
// Management

void MCALL CIMSMDriveDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CIMSMDriveDriver::Open(void)
{
	m_pCL = (IMSMDriveCmdDef FAR * )CL;
	}

// Device

CCODE MCALL CIMSMDriveDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			char c[4];

			memcpy(&c[0], GetString(pData), 4);

			m_pCtx->m_bName	= c[0];

			m_pCtx->m_EX[0]	= NA_STRING;
			m_pCtx->m_TI[0]	= NA_STRING;
			m_pCtx->m_TP[0]	= NA_STRING;

			m_pCtx->m_MA	= 0;
			m_pCtx->m_MR	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CIMSMDriveDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CIMSMDriveDriver::Ping(void)
{
	Initiate();

	DWORD Data[1];

	CAddress Addr;

	Addr.a.m_Table  = AN;
	Addr.a.m_Offset = CMA;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrLongAsLong;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CIMSMDriveDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !SetpItem(Addr) ) return CCODE_ERROR | CCODE_HARD;

	if( ReadNoTransmit(pData, uCount) ) return IsString() ? uCount : 1;;

	UINT uO = Addr.a.m_Offset;

	DoRead(uO);

	if( Transact() ) {

		if( m_bRx[0] == '?' || m_bRx[0] == NAK ) return CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY;

		if( m_pItem->uID != CMSI || !uO || uO > 4 ) GetResponse(pData);

		else GetSIResponse(pData);

		switch( m_pItem->uID ) {

			case CMMA: m_pCtx->m_MA = *pData; break;

			case CMMR: m_pCtx->m_MR = *pData; break;
			}

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CIMSMDriveDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !SetpItem(Addr) ) return CCODE_ERROR | CCODE_HARD;

/**/	AfxTrace3("\r\n*****WRITE***** %d %d %d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( WriteNoTransmit(pData, uCount) ) return IsString() ? uCount : 1;

	DoWrite(Addr.a.m_Offset, pData);

	return Transact() ? 1 : CCODE_ERROR;
	}

UINT CIMSMDriveDriver::DevCtrl(void * pContext, UINT uFunc, PCTXT Value)
{
	m_uPtr = strlen(Value);

	memcpy( m_bTx, Value, m_uPtr );

	Send();

	return 1;
	}

// PRIVATE METHODS

// Initialize
void	CIMSMDriveDriver::Initiate(void)
{
	m_bTx[0] = 'E';
	m_bTx[1] = 'M';
	m_bTx[2] = '=';
	m_bTx[3] = '2'; // Echo Mode - no prompts
	m_uPtr   =   4;
	Transact();

	m_bTx[0] = 'C';
	m_bTx[1] = 'K';
	m_bTx[2] = '=';
	m_bTx[3] = '0'; // Checksum Off
	m_uPtr   =   4;
	Transact();

	m_uPtr = 0;
	m_bTx[m_uPtr++] = 'P';
	m_bTx[m_uPtr++] = 'Y';
	m_bTx[m_uPtr++] = '=';
	m_bTx[m_uPtr++] = '1';
	Transact();

	m_pData->Write( 10, FOREVER ); // Activate Multiple Devices
	}

// Opcode Handlers

void CIMSMDriveDriver::DoRead(UINT uOffset)
{
	StartFrame(TRUE);

	AddCommand(uOffset, m_pItem->uID == CMI);
	}

void CIMSMDriveDriver::DoWrite(UINT uOffset, PDWORD pData)
{
	StartFrame(FALSE);

	AddCommand(uOffset, m_pItem->uID == CMO);

	switch( GetWriteDataType(uOffset) ) {

		case INT_WDATA:

			AddByte( '=' );
			AddData( *pData );
			return;

		case BOOL_WDATA:

			AddByte( *pData ? '1' : '0' );
			return;

		case SI_WDATA:

			AddByte( m_pHex[(*pData >> 4) & 0xF] );
			AddByte( ',' );
			AddByte( m_pHex[ *pData & 0xF] );
			return;

		case EX_WDATA:

			AddLabelData( m_pCtx->m_EX );
			AddByte( ',' );
			AddByte( '0' );
			return;

		case TI_WDATA:
				       
			AddData( *pData );
			AddByte( ',' );
			AddLabelData( m_pCtx->m_TI );
			return;

		case TP_WDATA:
				       
			AddData( *pData );
			AddByte( ',' );
			AddLabelData( m_pCtx->m_TP );
			return;
		}
	}

// Header Building

void CIMSMDriveDriver::StartFrame(BOOL fIsRead)
{
	m_uPtr = 0;

	AddByte( m_pCtx->m_bName );

	if( fIsRead ) {

		AddByte( 'P' );
		AddByte( 'R' );
		AddByte( ' ' );
		}
	}

void CIMSMDriveDriver::EndFrame(void)
{
	AddByte( LF );
	}

void CIMSMDriveDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CIMSMDriveDriver::AddData(DWORD dData)
{
	BOOL fFirst   = FALSE;
	DWORD dFactor = 1000000000;

	if( dData & 0x80000000 ) {

		AddByte('-');
		dData = -dData;
		}

	while( dFactor ) {

		BYTE b = (dData/dFactor) % 10;

		if( fFirst || b ) {

			fFirst = TRUE;

			AddByte( m_pHex[b] );
			}

		dFactor /= 10;
		}

	if( !fFirst ) AddByte('0');
	}

void CIMSMDriveDriver::AddCommand(UINT uOffset, BOOL fIsIOAll)
{
	if( m_pItem->uID == CMUV ) {

		AddByte( HIBYTE(uOffset) );
		AddByte( LOBYTE(uOffset) );
		return;
		}

	AddByte( m_pItem->sName[0] );

	if( fIsIOAll ) {

		if( !uOffset ) {

			AddByte( m_pItem->uID == CMI ? 'N' : 'T' );

			return;
			}
		}

	if( m_pItem->sName[1] == '#' ) AddByte( m_pHex[uOffset & 0xF] );

	else {
		if( strlen(m_pItem->sName) > 1 ) AddByte( m_pItem->sName[1] );
		}
	}

void	CIMSMDriveDriver::AddLabelData(PDWORD pLabel)
{
	for( UINT i = 0; i < 4; i++ ) {

		UINT uShift = 32;

		DWORD dData = pLabel[i];

		while( uShift ) {

			uShift -= 8;

			BYTE b = dData >> uShift;

			if( !b ) return;

			AddByte(b);
			}
		}
	}

// Transport Layer

BOOL CIMSMDriveDriver::Transact(void)
{
	Send();

	return GetReply();
	}

void CIMSMDriveDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CIMSMDriveDriver::GetReply(void)
{
	UINT uCount = 0;
	UINT uTime  = TIMEOUT;
	WORD wData;

	SetTimer(uTime);

/**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (wData = Get(uTime)) == LOWORD(NOTHING) ) {			

			Sleep(5);

			continue;
			}

		BYTE b = LOBYTE(wData);

/**/		AfxTrace1("<%2.2x>", b);

		m_bRx[uCount++] = b;

		if( uCount > sizeof(m_bRx) ) return FALSE;

		if( b == CR || b == LF || b == ACK || b == NAK ) {

			m_bRx[uCount] = 0;

			return TRUE;
			}
		}

	return FALSE;
	}

void CIMSMDriveDriver::GetResponse(PDWORD pData)
{
	char s[16];
	char * s2;

	*pData = strtol( (char *)(&m_bRx[0]), &s2, 10 );
	}

void CIMSMDriveDriver::GetSIResponse(PDWORD pData)
{
	char s[32];
	char * s2 = s;

	DWORD d1 = strtol( (char *)(&m_bRx[0]), &s2, 10 );
	DWORD d2 = 0;

	UINT l = strlen( (char *)m_bRx );

	UINT i = 0;

	while( i < l && m_bRx[i] != CR ) {

		if( m_bRx[i++] == ',' ) break;
		}

	if( m_bRx[i] != CR ) d2 = strtol( (char *)(&m_bRx[i]), &s2, 10 );

	*pData = ( (d1 & 0xF) << 4 ) + (d2 & 0xF);
	}

// Port Access

void CIMSMDriveDriver::Put(void)
{
/**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

WORD CIMSMDriveDriver::Get(UINT uTimer)
{
	return m_pData->Read(uTimer);
	}

// Helpers

BOOL CIMSMDriveDriver::SetpItem(AREF Addr)
{
	m_pItem  = m_pCL;

	UINT uID = Addr.a.m_Table == AN ? Addr.a.m_Offset : Addr.a.m_Table;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == m_pItem->uID ) return TRUE;

		m_pItem++;
		}

	return FALSE;
	}

BOOL	CIMSMDriveDriver::ReadNoTransmit(PDWORD pData, UINT uCount)
{
	UINT i;

	switch ( m_pItem->uID ) {

		case CMEXL: CopyStringData(m_pCtx->m_EX, pData, uCount, FALSE); return TRUE;
		case CMTIL: CopyStringData(m_pCtx->m_TI, pData, uCount, FALSE); return TRUE;
		case CMTPL: CopyStringData(m_pCtx->m_TP, pData, uCount, FALSE); return TRUE;

		case CMTPE: *pData = 0xFFFFFFFF; return TRUE;

		case CMEX:
		case CMFD:
		case CMHI:
		case CMHM:
		case CMPS:
		case CMRS:
		case CMS:
		case CMTIE: *pData = 0; return TRUE;

		case CMMA:  *pData = m_pCtx->m_MA; return TRUE;
		case CMMR:  *pData = m_pCtx->m_MR; return TRUE;

		case CMO:   for( i = 0; i < uCount; i++ ) pData[i] = 0xFFFFFFFF; return TRUE;
		}

	return m_pCtx->m_bName == '*'; // don't allow the send of a global read
	}

BOOL	CIMSMDriveDriver::WriteNoTransmit(PDWORD pData, UINT uCount)
{
	UINT i;

	switch ( m_pItem->uID ) {

		case CMBY:
		case CMEF:
		case CMI:
		case CMMD:
		case CMMV:
		case CMUV:
		case CMV:
		case CMVC:
		case CMVR:	return TRUE;

		case CMEXL:	CopyStringData(pData, m_pCtx->m_EX, uCount, TRUE); return TRUE;
		case CMTIL:	CopyStringData(pData, m_pCtx->m_TI, uCount, TRUE); return TRUE;
		case CMTPL:	CopyStringData(pData, m_pCtx->m_TP, uCount, TRUE); return TRUE;
		}

	return FALSE;
	}

void	CIMSMDriveDriver::CopyStringData(PDWORD pSrc, PDWORD pDest, UINT uCount, BOOL fClear)
{
	for( UINT i = 0; i < 4; i++ ) {

		if( i < uCount ) pDest[i] = pSrc[i];

		else {
			if( fClear ) pDest[i] = 0;
			}
		}
	}

UINT	CIMSMDriveDriver::GetWriteDataType(UINT uOffset)
{
	switch( m_pItem->uID ) {

		case CMIP:
		case CMFD:
		case CMPS:
		case CMRS:
		case CMS:	return NO_WDATA;

		case CMDE:
		case CMEE:
		case CMJE:
		case CMPM:
		case CMSM:
		case CMST:	return BOOL_WDATA;

		case CMO:	return uOffset == 0 ? INT_WDATA : BOOL_WDATA;

		case CMSI:	return uOffset == 5 ? INT_WDATA : SI_WDATA;

		case CMEX:	return EX_WDATA;

		case CMTIE:	return TI_WDATA;

		case CMTPE:	return TP_WDATA;
		}

	return INT_WDATA;
	}

BOOL	CIMSMDriveDriver::IsString(void)
{
	switch( m_pItem->uID ) {
		
		case CMEXL:
		case CMTIL:
		case CMTPL:
		case CMVR:	return TRUE;
		}

	return FALSE;
	}

// End of File
