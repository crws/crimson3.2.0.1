
#include "totalflow2base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced TCP/IP Driver
//

class CTotalFlow2TcpMasterDriver : public CTotalFlow2Master
{
	public:
		// Constructor
		CTotalFlow2TcpMasterDriver(void);

		// Destructor
		~CTotalFlow2TcpMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:

		struct CCtx : public CBaseCtx
		{
			DWORD     m_IP;
			UINT      m_uPort;
			char      m_fKeep;
			BOOL      m_fPing;
			UINT      m_uTime1;
			UINT      m_uTime3;
			ISocket * m_pSock;
			UINT      m_uLast;
			UINT	  m_uConn;
			};

		CCtx	* m_pCtx;
		UINT	  m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		BOOL CheckConnection(void);

		// Transport Layer
		BOOL CheckLink(void);
		void AbortLink(void);
		BOOL Send(PBYTE pBuff, UINT uLength);
		UINT Recv(UINT uTime);
	};

// End of File
