
#ifndef	INCLUDE_totalflow2base_HPP
	
#define	INCLUDE_totalflow2base_HPP

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Base Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#define	BuffAppend(p, t, v)	BEGIN { *BuffAddTail(p, t) = v; } END

#define	BuffGetNext(p)		((p)->GetData() + (p)->GetSize())

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Slot Definition
//

struct CSlot
{
	UINT	  m_Slot;
	UINT	  m_App;
	UINT	  m_Arr;
	UINT	  m_Reg;
	UINT	  m_Type;
	UINT	  m_Size;
	BOOL	  m_fString;

	DWORD	* m_pData;
	BOOL	  m_fValid;
	UINT	  m_uRef;
	UINT	  m_uLast;
	};

//////////////////////////////////////////////////////////////////////////
//
// More TotalFlow Structures
//

struct CBlock
{
	BYTE m_App;
	BYTE m_Arr;
	WORD m_Reg;
	BYTE m_Type;
	WORD m_Size;
	WORD m_Count;
	BOOL m_fString;
	};

struct CAppKey
{
	BYTE m_bLookup;
	BYTE m_bApp;
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Driver Base
//

class CTotalFlow2Master : public CMasterDriver
{
	public:
		// Constructor
		CTotalFlow2Master(void);

		// Destructor
		~CTotalFlow2Master(void);

		// Entry Points
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(void)  Service(void);

	protected:

		// Protocol Headers
		#pragma pack(1)

		// Supervisory Frame
		struct CSuper
		{
			BYTE	m_bSOH;
			char	m_sName[10];
			BYTE	m_bType;
			BYTE	m_bSlot;
			WORD	m_wCRC;
			};

		// Header Packet
		struct CHeader
		{
			BYTE	m_bType;
			BYTE	m_bRequest;
			BYTE	m_bCount;
			};

		// Record Packet
		struct CRecord
		{
			BYTE	m_bType;
			BYTE	m_bRequest;
			BYTE	m_bApp;
			BYTE	m_bArray;
			WORD	m_wIndex;
			WORD	m_wCount;
			WORD	m_wSpare;
			};

		#pragma pack()

		// Base Context
		struct CBaseCtx
		{
			PTXT	  m_Name;
			PTXT	  m_Code;
			UINT	  m_uSlots;
			CSlot **  m_pSlots;
			UINT	  m_uTime2;
			UINT	  m_uUpdate;
			UINT	  m_uTimeout;
			UINT	  m_uLastUpdate;
			UINT	  m_uEstab;
			BOOL	  m_fForceUpdate;
			BOOL	  m_fUseAppKey;
			UINT	  m_uKeys;
			CAppKey * m_pKey;
			};

		// Data Members
		CBaseCtx	* m_pCtx;

		UINT	m_uPtr;
		UINT	m_uAlloc;
		PBYTE	m_pBuff;

		// Slot Management
		void	PrintSlots(void);

		// Slot Overridables
		virtual void    GetSlots(BYTE const * &pData, CBaseCtx *pCtx);
		virtual void    CleanupSlots(void);
		virtual CSlot * FindSlot(AREF Addr, UINT &uCount);
		virtual CSlot * FindSlot(AREF Addr);

		// Application Management
		void GetAppKey(BYTE const * &pData, CBaseCtx *pCtx);
		UINT LookupApp(BYTE bLook);
		void LookupApps(void);
		
		// Frame Building
		void AddInitialSequence(void);
		void AddSupervisoryFrame(void);
		void AddPasswordFrame(PCTXT pCode);
		void AddSyncPattern(void);
		void AddHeader(BYTE bType, BYTE bRequest, BYTE bCount);
		void AddReadRecords(CBlock * pBlocks, UINT uCount);
		void AddReadRecord(BYTE bApp, BYTE bArray, WORD wIndex, WORD wCount);
		void AddWriteRecord(BYTE bApp, BYTE bArray, WORD wIndex, PCBYTE pData, UINT uData, UINT uCount);
		void AddSizedFrame(PCVOID pData, UINT uData);
		void AddByte(BYTE b);
		void AddWord(WORD w);
		void AddData(PBYTE pData, UINT uSize);

		// Frame  Checking
		BOOL CheckReadRecord(PCBYTE pData, BYTE bApp, BYTE bArray, WORD wIndex, WORD wCount);

		// CRC Helpers
		void Add(CRC16 &Crc, PCBYTE pData, UINT uData);
		void Add(CRC16 &Crc, PCVOID pFrom, PCVOID pEnd);

		// Buffer Management
		void GrowBuffer(void);
		void ResetBuffer(void);

		// Transactions
		BOOL  Transact(PBYTE pBuff, UINT uLength, CSuper &Super, CHeader &Header);
		BOOL  RecvSupervisoryFrame(CSuper &Super);
		BOOL  RecvHeader(CHeader &Header);
		BOOL  RecvSizedFrame(PBYTE pData, UINT uData);
		PBYTE RecvSizedFrame(UINT &uData);

		// Transport
		virtual BOOL CheckLink(void)			= 0;
		virtual void AbortLink(void)			= 0;
		virtual BOOL Send(PBYTE pBuff, UINT uLength)	= 0;
		virtual UINT Recv(UINT uTime)			= 0;

		// Implementation
		void InvalidateSlots(CSlot **pSlots, UINT uCount);
		void UpdateSlotData(BOOL fForce);
		UINT CollectSlots(CSlot ** pSlots, UINT &uPos, UINT uMax);
		BOOL ShouldUpdate(CBaseCtx *pCtx, UINT uTicks);
		BOOL IsStale(CSlot * const pSlot);
		BOOL GetRecordsData(CBlock * pBlocks, CSlot ** pSlot, UINT uCount);
		void GetByteData(CSlot &Slot, PCBYTE pFrom, UINT uSize, UINT uOffset = 0);
		void GetWordData(CSlot &Slot, PCBYTE pFrom, UINT uSize, UINT uOffset = 0);
		void GetLongData(CSlot &Slot, PCBYTE pFrom, UINT uSize, UINT uOffset = 0);
		void GetRealData(CSlot &Slot, PCBYTE pFrom, UINT uSize, UINT uOffset = 0);
		WORD GetRecordCount(PCBYTE pFrom);
		BOOL IsWord(UINT m_Type);
		BOOL IsLong(UINT m_Type);
		BOOL IsBlock(CSlot& Slot1, CSlot& Slot2);
		UINT GetDataSize(CBlock Block);
		void CheckDataSize(CSlot Slot, UINT &uSize, PCBYTE pData);

		// Overridables
		virtual UINT ConstructBlocks(CBlock * pBlocks, CSlot ** pSlots, UINT uCount);
		virtual void GetSlotData(CBlock Block, CSlot ** pSlots, UINT uReply, UINT &uSlot, PCBYTE pData);
		virtual void UpdateReferences(void);
		virtual UINT GetDataOffset(CSlot * pSlot, AREF Addr);
		virtual UINT GetMaxCount(void);
		virtual UINT GetStringOffset(CSlot * pSlot, AREF Addr);
		virtual WORD GetWriteOffset(CSlot * pSlot, AREF Addr);
		virtual WORD GetWriteCount(CSlot * pSlot, UINT uCount);
		virtual UINT GetStringSize(CSlot * pSlot);
		
		// Slot Sorting
		static int SortFunc(PCVOID p1, PCVOID p2);
		
	};

#endif
	
// End of File
