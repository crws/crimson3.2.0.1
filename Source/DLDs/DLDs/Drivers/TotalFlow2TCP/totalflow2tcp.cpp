
#include "intern.hpp"

#include "totalflow2tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced TCP/IP Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//

INSTANTIATE(CTotalFlow2TcpMasterDriver);

// Constructor

CTotalFlow2TcpMasterDriver::CTotalFlow2TcpMasterDriver(void)
{
	m_Ident	= DRIVER_ID;

	m_pCtx	= NULL;

	m_uKeep = 0;
	}

// Destructor

CTotalFlow2TcpMasterDriver::~CTotalFlow2TcpMasterDriver(void)
{
	}

// Configuration

void MCALL CTotalFlow2TcpMasterDriver::Load(LPCBYTE pData)
{
	}

// Management

void MCALL CTotalFlow2TcpMasterDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CTotalFlow2TcpMasterDriver::Open(void)
{
	}
		
// Device

CCODE MCALL CTotalFlow2TcpMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CCtx;

			m_pCtx->m_Name		= GetString(pData);
			m_pCtx->m_Code		= GetString(pData);
			m_pCtx->m_uUpdate	= GetLong(pData);
			m_pCtx->m_uTimeout	= GetLong(pData);
			m_pCtx->m_uLastUpdate	= 0;
			m_pCtx->m_fForceUpdate	= FALSE;
			m_pCtx->m_fUseAppKey	= GetByte(pData) ? TRUE : FALSE;

			GetAppKey(pData, m_pCtx);

			GetSlots(pData, m_pCtx);

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);			
			m_pCtx->m_uLast  = GetTickCount();
			m_pCtx->m_uConn	 = GetTickCount();
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uEstab = 1;
			
			CTotalFlow2Master::m_pCtx = m_pCtx;

			pDevice->SetContext(m_pCtx);

			LookupApps();

			return CCODE_SUCCESS;
			}
		
		return CCODE_ERROR;
		}

	CTotalFlow2Master::m_pCtx = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CTotalFlow2TcpMasterDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		CleanupSlots();

		Free(m_pCtx->m_Name);

		Free(m_pCtx->m_Code);

		delete [] m_pCtx->m_pKey;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CTotalFlow2TcpMasterDriver::Ping(void)
{
	if( m_pCtx->m_fPing ) {
		
		DWORD IP = m_pCtx->m_IP;
		
		if( CheckIP(IP, m_pCtx->m_uTime2) == NOTHING ) {
			
			return CCODE_ERROR; 
			}
		}

	if( OpenSocket() ) {
						
		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

// Socket Management

BOOL CTotalFlow2TcpMasterDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CTotalFlow2TcpMasterDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP   = (IPADDR const &) m_pCtx->m_IP;

		WORD   Port = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					m_pCtx->m_uConn = GetTickCount();

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		return FALSE;
		}

	return FALSE;
	}

void CTotalFlow2TcpMasterDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort ) {

			m_pCtx->m_pSock->Abort();
			}
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CTotalFlow2TcpMasterDriver::CheckLink(void)
{
	return OpenSocket();
	}

void CTotalFlow2TcpMasterDriver::AbortLink(void)
{
	CloseSocket(TRUE);
	}

BOOL CTotalFlow2TcpMasterDriver::Send(PBYTE pBuff, UINT uLength)
{
	if( CheckLink() && CheckConnection() ) {

		UINT uLimit = 1460;

		while( uLength > 0 ) {

			UINT uSend = min(uLength, uLimit);

			if( m_pCtx->m_pSock->Send(pBuff, uSend) != S_OK ) {

				return FALSE;
				}

			uLength -= uSend;

			pBuff   += uSend;

			Sleep(20);
			}

		return TRUE;
		}

	return FALSE;
	}

UINT CTotalFlow2TcpMasterDriver::Recv(UINT uTime)
{
	if( CheckConnection() ) {

		SetTimer(uTime);

		while( GetTimer() ) {

			if( CheckSocket() ) {

				BYTE bData = 0;

				UINT uSize = 1;

				if( m_pCtx->m_pSock->Recv(&bData, uSize) == S_OK ) {

					if( uSize ) {

						m_pCtx->m_uConn = GetTickCount();
					
						return bData;
						}
					}

				Sleep(5);

				continue;
				}

			break;
			}
		}

	return NOTHING;
	}

BOOL CTotalFlow2TcpMasterDriver::CheckConnection(void)
{
	if( (GetTickCount() - m_pCtx->m_uConn) > ToTicks(m_pCtx->m_uTime1) ) {

		CloseSocket(TRUE);

		return FALSE;
		}

	return TRUE;
	}

// End of File
