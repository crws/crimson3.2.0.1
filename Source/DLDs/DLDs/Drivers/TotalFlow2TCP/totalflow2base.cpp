
#include "totalflow2base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Driver Base
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#define REF_COUNT	2

#define ALLOC_DEFAULT	1024

#define	MAX_REQUEST	255

#define BLOCK_SIZE	32

// Constructor

CTotalFlow2Master::CTotalFlow2Master(void)
{
	m_pCtx   = NULL;

	m_pBuff  = NULL;

	m_uAlloc = 0;

	m_uPtr   = 0;
	}

// Destructor

CTotalFlow2Master::~CTotalFlow2Master(void)
{
	if( m_pBuff ) {

		delete [] m_pBuff;
		}
	}

// Entry Points

void MCALL CTotalFlow2Master::Service(void)
{
	UINT uTicks = GetTickCount();

	if( ShouldUpdate(m_pCtx, uTicks) ) {

		InvalidateSlots(m_pCtx->m_pSlots, m_pCtx->m_uSlots);

		UpdateSlotData(m_pCtx->m_fForceUpdate);
		}
	}

CCODE MCALL CTotalFlow2Master::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CSlot * pSlot = FindSlot(Addr, uCount);

	if( pSlot ) {

		if( m_pCtx->m_fForceUpdate ) {

			pSlot->m_uRef = REF_COUNT;

			return CCODE_SUCCESS | CCODE_NO_DATA;
			}

		if( pSlot->m_uRef == 0 ) {

			pSlot->m_uRef = REF_COUNT;

			m_pCtx->m_fForceUpdate = TRUE;

			return CCODE_SUCCESS | CCODE_NO_DATA;
			}

		pSlot->m_uRef = REF_COUNT;

		if( pSlot->m_fValid ) {

			UINT uStart = GetDataOffset(pSlot, Addr);

			for( UINT n = uStart; n < (uCount + uStart); n++ ) {

				pData[n - uStart] = pSlot->m_pData[n];
				}
			
			return uCount;
			}

		return CCODE_ERROR | CCODE_NO_RETRY;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CTotalFlow2Master::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		CSlot *pSlot = FindSlot(Addr);
		
		if( pSlot ) {

			CSuper	Super;

			CHeader	Header;

			AddInitialSequence();

			AddHeader(0, 1, 1);

			if( Transact(m_pBuff, m_uPtr, Super, Header) ) {

				UINT uCopy   = 0;

				PBYTE pCopy  = NULL;

				UINT uWrite  = GetWriteOffset(pSlot, Addr);

				if( pSlot->m_fString ) {

					UINT uSize = GetStringSize(pSlot);

					MakeMin(uCount, uSize);

					uCopy = uSize * 4;

					pCopy = PBYTE(alloca(uCopy));

					PDWORD pString = (PDWORD) pCopy;

					UINT uString   = GetStringOffset(pSlot, Addr);

					UINT uData     = GetDataOffset(pSlot, Addr) - uString;

					for( UINT m = 0; m < uSize; m++ ) {

						pString[m] = HostToIntel(pSlot->m_pData[m + uData]);
						}

					for( UINT n = 0; n < uCount; n++ ) {

						pString[n + uString] = HostToIntel(pData[n]);
						}

					UINT uSpace = uCopy - 1;

					while( uSpace > 0 ) {

						if( pCopy[uSpace] == 0x20 ) {

							pCopy[uSpace] = 0;

							uSpace--;

							continue;
							}
						break;
						}
					}
				else {
					MakeMin(uCount, GetMaxCount());

					pCopy = PBYTE(alloca(uCount * 4));

					for( UINT n = 0; n < uCount; n++ ) {

						UINT uType = Addr.a.m_Type;

						if( uType == addrByteAsByte ) {

							pCopy[uCopy++] = BYTE(pData[n]);
							}

						if( uType == addrWordAsWord ) {

							pCopy[uCopy++] = LOBYTE(pData[n]);

							pCopy[uCopy++] = HIBYTE(pData[n]);
							}

						if( uType == addrLongAsLong || uType == addrRealAsReal ) {

							pCopy[uCopy++] = LOBYTE(LOWORD(pData[n]));

							pCopy[uCopy++] = HIBYTE(LOWORD(pData[n]));

							pCopy[uCopy++] = LOBYTE(HIWORD(pData[n]));

							pCopy[uCopy++] = HIBYTE(HIWORD(pData[n]));
							}
						}
					}
				
				if( uCopy ) {

					UINT uWriteCount = GetWriteCount(pSlot, uCount);

					AddSupervisoryFrame();

					AddWriteRecord(	pSlot->m_App,
							pSlot->m_Arr,
							pSlot->m_Reg + uWrite,
							pCopy,
							uCopy,
							uWriteCount
							);

					if( Transact(m_pBuff, m_uPtr, Super, Header) ) {

						if( pSlot->m_fString ) {

							UINT uString = GetStringOffset(pSlot, Addr);

							UINT uData   = GetDataOffset(pSlot, Addr) - uString;

							GetLongData(*pSlot, pCopy, GetStringSize(pSlot), uData);

							return uCount;
							}

						switch( Addr.a.m_Type ) {

							case addrByteAsByte: GetByteData(*pSlot, pCopy, uWriteCount, uWrite); break;

							case addrWordAsWord: GetWordData(*pSlot, pCopy, uWriteCount, uWrite); break;

							case addrLongAsLong: GetLongData(*pSlot, pCopy, uWriteCount, uWrite); break;
						
							case addrRealAsReal: GetRealData(*pSlot, pCopy, uWriteCount, uWrite); break;
							}

						return uCount;
						}
					}
				}

			return CCODE_ERROR;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_ERROR;
	}

// Slot Management

void CTotalFlow2Master::GetSlots(BYTE const * &pData, CBaseCtx *pCtx)
{
	UINT uCount = GetLong(pData);

	pCtx->m_uSlots = uCount;

	CSlot **pSlots = new CSlot * [ uCount ];

	for( UINT n = 0; n < uCount; n++ ) {

		pSlots[n]	= new CSlot;

		CSlot * pSlot   = pSlots[n];

		pSlot->m_Slot	= GetLong(pData);
		pSlot->m_App	= GetLong(pData);
		pSlot->m_Arr	= GetLong(pData);
		pSlot->m_Reg	= GetLong(pData);
		pSlot->m_Type	= GetWord(pData);
		UINT uSize	= GetWord(pData);
		pSlot->m_fString= GetByte(pData);
		pSlot->m_Size	= uSize;
		pSlot->m_fValid	= FALSE;
		pSlot->m_uRef	= 0;
		pSlot->m_uLast	= 0;

		pSlot->m_pData = new DWORD[ uSize ];

		memset(pSlot->m_pData, 0, uSize * sizeof(DWORD));
		}

	pCtx->m_pSlots = (CSlot **)pSlots;

	qsort(pCtx->m_pSlots, uCount, sizeof(CSlot*), SortFunc);
	}

CSlot * CTotalFlow2Master::FindSlot(AREF Addr, UINT &uCount)
{
	return FindSlot(Addr);
	}

CSlot * CTotalFlow2Master::FindSlot(AREF Addr)
{
	UINT uPos   = (Addr.a.m_Extra & 1) ? Addr.a.m_Table : Addr.a.m_Offset;

	UINT uSlots = m_pCtx->m_uSlots;

	for( UINT u = 0; u < uSlots; u++ ) {

		if( uPos == m_pCtx->m_pSlots[u]->m_Slot ) {

			return m_pCtx->m_pSlots[u];
			}
		}

	return NULL;
	}

void CTotalFlow2Master::CleanupSlots(void)
{
	for( UINT n = 0; n < m_pCtx->m_uSlots; n++ ) {

		delete [] m_pCtx->m_pSlots[n]->m_pData;

		m_pCtx->m_pSlots[n]->m_pData = NULL;
		}
	}

void CTotalFlow2Master::PrintSlots(void)
{
	UINT uCount = m_pCtx->m_uSlots;

	for( UINT n = 0; n < uCount; n++ ) {

		CSlot * pSlot = m_pCtx->m_pSlots[n];

		AfxTrace("\nSlot %u - %u.%u.%u size %u type %u string %u slot %8.8x", n, pSlot->m_App,
											 pSlot->m_Arr,
											 pSlot->m_Reg,
											 pSlot->m_Size,
											 pSlot->m_Type,
											 pSlot->m_fString,
											 pSlot->m_Slot);
		}
	}

// Application Management

void CTotalFlow2Master::GetAppKey(BYTE const * &pData, CBaseCtx *pCtx)
{
	pCtx->m_pKey  = NULL;

	if( GetWord(pData) == 0x1234)  {

		pCtx->m_uKeys = GetWord(pData);
		
		if( pCtx->m_uKeys > 0 ) {
			
			pCtx->m_pKey  = new CAppKey[pCtx->m_uKeys];
			
			for( UINT u = 0; u < pCtx->m_uKeys; u++ ) {
				
				pCtx->m_pKey[u].m_bLookup = GetByte(pData);

				pCtx->m_pKey[u].m_bApp	  = GetByte(pData);
				}
			}
		}
	}

UINT CTotalFlow2Master::LookupApp(BYTE bLook)
{
	for( UINT u = 0; u < m_pCtx->m_uKeys; u++ ) {

		if( m_pCtx->m_pKey[u].m_bLookup == bLook ) {

			return m_pCtx->m_pKey[u].m_bApp;
			}
		}

	return NOTHING;
	}

void CTotalFlow2Master::LookupApps(void)
{
	if( m_pCtx->m_fUseAppKey ) {

		for( UINT s = 0; s <  m_pCtx->m_uSlots; s++ ) {

			UINT uLook = LookupApp(m_pCtx->m_pSlots[s]->m_App);

			if( uLook < NOTHING ) {

				m_pCtx->m_pSlots[s]->m_App = uLook;
				}
			}
		}
	}


// Frame Building

void CTotalFlow2Master::AddInitialSequence(void)
{
	for( UINT n = 0; n < m_pCtx->m_uEstab; n++ ) {

		AddSupervisoryFrame();
		}

	AddSyncPattern();
	}

void CTotalFlow2Master::AddSupervisoryFrame(void)
{
	AddByte(0xFF);

	CSuper Super;

	Super.m_bSlot = 0;

	Super.m_bSOH = 0x01;

	Super.m_bType = 'X';

	UINT uName = strlen(m_pCtx->m_Name);

	memset(Super.m_sName, ' ', 10);

	memcpy(Super.m_sName, m_pCtx->m_Name, min(uName, 10));

	CRC16 Crc;

	Crc.Clear();

	Add(Crc, PCBYTE(m_pCtx->m_Code), 4);

	CSuper * pSuper = &Super;

	Add(Crc, pSuper, &pSuper->m_wCRC);

	Super.m_wCRC = HostToMotor(Crc.GetValue());

	AddData(PBYTE(&Super), sizeof(Super));
	}

void CTotalFlow2Master::AddPasswordFrame(PCTXT pCode)
{
	AddByte(0x00);

	AddByte(pCode[0] | 0x80);
	AddByte(pCode[1] | 0x80);
	AddByte(pCode[2] | 0x80);
	AddByte(pCode[3] | 0x80);

	AddByte(0x00);

	CRC16 Crc;

	Crc.Clear();

	PCBYTE pFrom = m_pBuff + m_uPtr - 6;

	PCBYTE pEnd  = m_pBuff + m_uPtr;

	Add(Crc, pFrom, pEnd);

	AddWord(MotorToHost(Crc.GetValue()));
	}

void CTotalFlow2Master::AddSyncPattern(void)
{
	AddByte(0xD3);

	AddByte(0xD9);

	AddByte(0xCE);

	AddByte(0xC3);
	}

void CTotalFlow2Master::AddHeader(BYTE bType, BYTE bRequest, BYTE bCount)
{
	CHeader Header;

	Header.m_bType    = bType;

	Header.m_bRequest = bRequest;

	Header.m_bCount   = bCount;

	AddSizedFrame(&Header, sizeof(Header));
	}

void CTotalFlow2Master::AddReadRecords(CBlock * pBlocks, UINT uCount)
{
	for( UINT b = 0; b < uCount; b++ ) {

		AddReadRecord(pBlocks[b].m_App, pBlocks[b].m_Arr, pBlocks[b].m_Reg, pBlocks[b].m_Count);
		}
	}

void CTotalFlow2Master::AddReadRecord(BYTE bApp, BYTE bArray, WORD wIndex, WORD wCount)
{
	CRecord Record;

	Record.m_bType    = 0;

	Record.m_bRequest = 2;

	Record.m_bApp     = bApp;

	Record.m_bArray   = bArray;

	Record.m_wIndex   = HostToIntel(wIndex);

	Record.m_wCount   = HostToIntel(wCount);

	Record.m_wSpare   = 0;

	AddSizedFrame(&Record, sizeof(Record));
	}

void CTotalFlow2Master::AddWriteRecord(BYTE bApp, BYTE bArray, WORD wIndex, PCBYTE pData, UINT uData, UINT uCount)
{
	UINT     uWork  = sizeof(CRecord) + uData;

	PBYTE    pWork  = New BYTE [ uWork ];

	CRecord &Record = (CRecord &) pWork[0];

	Record.m_bType    = 0;

	Record.m_bRequest = 2;

	Record.m_bApp     = bApp;

	Record.m_bArray   = bArray;

	Record.m_wIndex   = HostToIntel(wIndex);

	Record.m_wCount   = HostToIntel(WORD(uCount));

	Record.m_wSpare   = 0;

	memcpy(pWork + sizeof(CRecord), pData, uData);

	AddSizedFrame(pWork, uWork);

	delete [] pWork;
	}

void CTotalFlow2Master::AddSizedFrame(PCVOID pData, UINT uData)
{
	AddWord(WORD(uData));

	AddWord(~WORD(uData));

	AddData(PBYTE(pData), uData);

	CRC16 Crc;

	Crc.Clear();

	PCBYTE pFrom = m_pBuff + m_uPtr - 4 - uData;

	PCBYTE pEnd  = m_pBuff + m_uPtr;

	Add(Crc, pFrom, pEnd);

	AddWord(Crc.GetValue());
	}

void CTotalFlow2Master::AddByte(BYTE b)
{
	if( m_uPtr == m_uAlloc ) {

		GrowBuffer();
		}

	if( m_pBuff ) {

		m_pBuff[m_uPtr++] = b;
		}
	}

void CTotalFlow2Master::AddWord(WORD w)
{
	AddByte(LOBYTE(w));

	AddByte(HIBYTE(w));
	}

void CTotalFlow2Master::AddData(PBYTE pData, UINT uSize)
{
	for( UINT n = 0; n < uSize; n++ ) {

		AddByte(pData[n]);
		}
	}

// Frame  Checking

BOOL CTotalFlow2Master::CheckReadRecord(PCBYTE pData, BYTE bApp, BYTE bArray, WORD wIndex, WORD wCount)
{
	CRecord const *pRecord = (CRecord const *) pData;

	if( pRecord->m_bApp == bApp ) {

		if( pRecord->m_bArray == bArray ) {

			if( pRecord->m_wIndex == HostToIntel(wIndex) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// CRC Helpers

void CTotalFlow2Master::Add(CRC16 &Crc, PCBYTE pData, UINT uData)
{
	for( ; uData--; Crc.Add(*pData++) );
	}

void CTotalFlow2Master::Add(CRC16 &Crc, PCVOID pFrom, PCVOID pEnd)
{
	for( PCBYTE pData = PCBYTE(pFrom); pData < pEnd; Crc.Add(*pData++) );
	}

// Transactions

BOOL CTotalFlow2Master::Transact(PBYTE pBuff, UINT uLength, CSuper &Super, CHeader &Header)
{
	if( Send(pBuff, uLength) ) {

		if( RecvSupervisoryFrame(Super) ) {

			if( RecvHeader(Header) ) {

				ResetBuffer();

				return TRUE;
				}
			}
		}

	AbortLink();

	ResetBuffer();

	return FALSE;
	}

BOOL CTotalFlow2Master::RecvSupervisoryFrame(CSuper &Super)
{
	UINT uState = 0;

	UINT uCount = 0;

	UINT uTime  = m_pCtx->m_uTime2;

	for(;;) {

		UINT uRead = Recv(uTime);

		if( uRead == NOTHING ) {

			break;
			}

		switch( uState ) {

			case 0:

				if( uRead == 0x01 ) {

					Super.m_bSOH = 0x01;

					uCount = 1;

					uState = 1;
					}

				break;
					
			case 1:

				(PBYTE(&Super))[uCount] = BYTE(uRead);

				if( ++uCount == sizeof(CSuper) ) {

					CRC16 crc;

					crc.Clear();

					Add(crc, &Super, &Super.m_wCRC);

					if( crc.GetValue() == MotorToHost(Super.m_wCRC) ) {

						return TRUE;
						}

					return FALSE;
					}

				break;
			}

		uTime = max(10, m_pCtx->m_uTime2 / 10);
		}

	return FALSE;
	}

BOOL CTotalFlow2Master::RecvHeader(CHeader &Header)
{
	return RecvSizedFrame(PBYTE(&Header), sizeof(Header));
	}

BOOL CTotalFlow2Master::RecvSizedFrame(PBYTE pData, UINT uData)
{
	UINT  uState = 0;

	UINT  uCount = 0;

	UINT  uTime  = m_pCtx->m_uTime2;

	CRC16 crc;

	for(;;) {

		UINT uRead = Recv(uTime);

		if( uRead == NOTHING ) {

			break;
			}

		if( uState <= 4 ) {

			if( uState == 0 ) {

				crc.Clear();
				}

			crc.Add(uRead);
			}

		switch( uState ) {

			case 0:
				if( uRead == LOBYTE(uData) ) {

					uState = 1;

					break;
					}

				uState = 0;

				break;

			case 1:
				if( uRead == HIBYTE(uData) ) {

					uState = 2;

					break;
					}

				uState = 0;

				break;

			case 2:
				if( uRead == LOBYTE(~uData) ) {

					uState = 3;

					break;
					}

				uState = 0;

				break;

			case 3:
				if( uRead == HIBYTE(~uData) ) {

					uCount = 0;

					uState = 4;
					
					break;
					}

				uState = 0;

				break;

			case 4:
				pData[uCount] = BYTE(uRead);

				if( ++uCount == uData ) {

					uState = 5;
					}
				break;

			case 5:
				if( uRead == LOBYTE(crc.GetValue()) ) {

					uState = 6;

					break;
					}

				return FALSE;

			case 6:
				if( uRead == HIBYTE(crc.GetValue()) ) {

					return TRUE;
					}

				return FALSE;
			}

		uTime = max(10, m_pCtx->m_uTime2 / 10);
		}

	return FALSE;
	}

PBYTE CTotalFlow2Master::RecvSizedFrame(UINT &uData)
{
	UINT  uState = 0;

	UINT  uCount = 0;

	PBYTE pData  = NULL;

	UINT  uTime  = m_pCtx->m_uTime2;

	CRC16 crc;

	for(;;) {

		UINT uRead = Recv(uTime);

		if( uRead == NOTHING ) {

			if( pData ) {
				
				delete [] pData;
				}

			break;
			}

		if( uState <= 4 ) {

			if( uState == 0 ) {

				crc.Clear();
				}

			crc.Add(uRead);
			}

		switch( uState ) {

			case 0:
				uData  = uRead;

				uState = 1;

				break;

			case 1:
				uData  = uData | (uRead << 8);

				uState = 2;

				break;

			case 2:
				if( uRead == LOBYTE(~uData) ) {

					uState = 3;

					break;
					}

				uState = 0;

				break;

			case 3:
				if( uRead == HIBYTE(~uData) ) {

					pData  = New BYTE [ uData ];

					uCount = 0;

					uState = 4;
					
					break;
					}

				uState = 0;

				break;

			case 4:
				pData[uCount] = BYTE(uRead);

				if( ++uCount == uData ) {

					uState = 5;
					}
				break;

			case 5:
				if( uRead == LOBYTE(crc.GetValue()) ) {

					uState = 6;

					break;
					}

				delete [] pData;

				uData = 0;

				return NULL;

			case 6:
				if( uRead == HIBYTE(crc.GetValue()) ) {

					return pData;
					}

				delete [] pData;
	
				uData = 0;

				return NULL;
			}

		uTime = max(10, m_pCtx->m_uTime2 / 10);
		}

	uData = 0;

	return NULL;
	}

// Buffer Management

void CTotalFlow2Master::GrowBuffer(void)
{
	UINT uOld = m_uAlloc;

	m_uAlloc = m_uAlloc ? m_uAlloc << 1 : ALLOC_DEFAULT;

	PBYTE pNewBuff = new BYTE[m_uAlloc];

	if( m_pBuff ) {

		memcpy(pNewBuff, m_pBuff, uOld);

		delete [] m_pBuff;
		}

	m_pBuff = pNewBuff;
	}

void CTotalFlow2Master::ResetBuffer(void)
{
	if( m_pBuff ) {

		delete [] m_pBuff;

		m_pBuff	 = NULL;
		}
	
	m_uPtr	 = 0;

	m_uAlloc = 0;
	}

// Implementation

void CTotalFlow2Master::InvalidateSlots(CSlot **pSlots, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		if( IsStale(pSlots[n]) ) {

			pSlots[n]->m_fValid = FALSE;

			memset(pSlots[n]->m_pData, 0, pSlots[n]->m_Size);
			}
		}
	}

void CTotalFlow2Master::UpdateSlotData(BOOL fForce)
{
	if( !fForce ) {

		UpdateReferences();
		}

	UINT uSlots = m_pCtx->m_uSlots;	

	UINT uPos = 0;

	while( uPos < uSlots ) {

		CSuper Super;

		CHeader Header;

		AddInitialSequence();

		CSlot ** pSlots = new CSlot *[ MAX_REQUEST ];

		UINT uCount = CollectSlots(pSlots, uPos, MAX_REQUEST);
		
		if( uCount == 0 ) {

			delete [] pSlots;

			ResetBuffer();

			return;
			}

		CBlock * pBlocks = new CBlock[MAX_REQUEST];

		UINT uBlocks = ConstructBlocks(pBlocks, pSlots, uCount);

		AddHeader(0, 0, uBlocks);

		AddReadRecords(pBlocks, uBlocks);

		if( Transact(m_pBuff, m_uPtr, Super, Header) ) {

			if( !GetRecordsData(pBlocks, pSlots, uBlocks) ) {

				delete [] pSlots;

				delete [] pBlocks;

				break;
				}

			for( UINT u = 0; u < uCount; u++ ) {

				pSlots[u]->m_uLast = GetTickCount();
				}

			m_pCtx->m_uLastUpdate = GetTickCount();

			if( fForce ) {

				m_pCtx->m_fForceUpdate = FALSE;
				}
			}

		delete [] pSlots;

		delete [] pBlocks;
		}
	}

UINT CTotalFlow2Master::CollectSlots(CSlot ** pSlots, UINT &uPos, UINT uMax)
{
	for( UINT u = 0; u < uMax; u++ ) {

		pSlots[u] = NULL;
		}

	UINT uCount = 0;

	for(;;) {

		if( uPos == m_pCtx->m_uSlots ) {

			break;
			}

		CSlot * pSlot = m_pCtx->m_pSlots[uPos];

		if( pSlot->m_uRef > 0 && IsStale(pSlot) ) {

			pSlots[uCount++] = pSlot;
			}

		if( uCount == uMax ) {

			break;
			}
			
		uPos++;
		}

	return uCount;
	}

UINT CTotalFlow2Master::ConstructBlocks(CBlock * pBlocks, CSlot ** pSlots, UINT uCount)
{
	UINT uBlocks = 0;

	for( UINT s = 0; s < uCount; s++, uBlocks++ ) {

		CSlot * pSlot1 = pSlots[s];

		CSlot * pSlot2 = pSlots[s + 1];

		pBlocks[uBlocks].m_App = pSlot1->m_App;

		pBlocks[uBlocks].m_Arr = pSlot1->m_Arr;

		pBlocks[uBlocks].m_Reg = pSlot1->m_Reg;

		pBlocks[uBlocks].m_Type = pSlot1->m_Type;

		pBlocks[uBlocks].m_Size = pSlot1->m_Size;

		pBlocks[uBlocks].m_fString = pSlot1->m_fString;
			
		pBlocks[uBlocks].m_Count = 1;

		while( IsBlock(*pSlot1, *pSlot2) && s + 1 < uCount ) {

			pBlocks[uBlocks].m_Count += pSlot2->m_Reg - pSlot1->m_Reg;

			s++;

			pSlot1 = pSlot2;

			pSlot2 = pSlots[s + 1];
			}
		}

	return uBlocks;
	}

void CTotalFlow2Master::GetSlotData(CBlock Block, CSlot ** pSlots, UINT uReply, UINT &uSlot, PCBYTE pData)
{
	UINT uSlots = GetRecordCount(pData);

	pData  += 8;

	uReply -= 6;

	for( UINT u = 0; u < uSlots; u++ ) {

		UINT uSize = GetDataSize(Block);

		if( pSlots[uSlot]->m_Reg == Block.m_Reg + u ) {

			CheckDataSize(*pSlots[uSlot], uSize, pData);

			if( uSize == 0 ) {

				return;
				}

			if( uReply >= uSize ) {

				uReply -= uSize;

				switch( pSlots[uSlot]->m_Type ) {

					case addrByteAsByte: GetByteData(*pSlots[uSlot], pData, uSize /  sizeof(BYTE));	break;

					case addrWordAsWord: GetWordData(*pSlots[uSlot], pData, uSize /  sizeof(WORD)); break;

					case addrLongAsLong: GetLongData(*pSlots[uSlot], pData, uSize / sizeof(DWORD)); break;
						
					case addrRealAsReal: GetRealData(*pSlots[uSlot], pData, uSize / sizeof(DWORD)); break;
					}

				uSlot++;
				}
			}

		pData += uSize;
		}
	}

void CTotalFlow2Master::UpdateReferences(void)
{
	for( UINT n = 0; n < m_pCtx->m_uSlots; n++ ) {

		CSlot * pSlot = m_pCtx->m_pSlots[n];

		if( pSlot->m_uRef ) {

			pSlot->m_uRef--;
			}
		}
	}

BOOL CTotalFlow2Master::ShouldUpdate(CBaseCtx *pCtx, UINT uTicks)
{
	if( pCtx->m_uLastUpdate == 0 ) {

		return TRUE;
		}

	if( pCtx->m_fForceUpdate ) {

		return TRUE;
		}

	return (uTicks - pCtx->m_uLastUpdate) > ToTicks(pCtx->m_uUpdate);
	}

BOOL CTotalFlow2Master::IsStale(CSlot * const pSlot)
{
	return (GetTickCount() - pSlot->m_uLast) > ToTicks(m_pCtx->m_uTimeout);
	}

BOOL CTotalFlow2Master::GetRecordsData(CBlock * pBlocks, CSlot ** pSlots, UINT uCount)
{
	UINT uSlot = 0;

	for( UINT b = 0; b < uCount; b++ ) {

		UINT uReply = 0;

		PBYTE pReply = RecvSizedFrame(uReply);

		if( pReply ) {

			PCBYTE pFrom = pReply;

			if( pReply[1] == 2 ) {

				pFrom  += 2;

				uReply -= 2;

				BOOL fCheck = CheckReadRecord(pReply,
							      pBlocks[b].m_App,
							      pBlocks[b].m_Arr,
							      pBlocks[b].m_Reg,
							      pBlocks[b].m_Count);

				if( fCheck ) {

					GetSlotData(pBlocks[b], pSlots, uReply, uSlot, pFrom);
					}					
				}
			
			delete [] pReply;

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

void CTotalFlow2Master::GetByteData(CSlot &Slot, PCBYTE pFrom, UINT uSize, UINT uOffset)
{
	for( UINT n = 0, u = uOffset; n < uSize; n++, u++ ) {

		Slot.m_pData[u] = pFrom[n];
		}

	Slot.m_fValid = TRUE;
	}

void CTotalFlow2Master::GetWordData(CSlot &Slot, PCBYTE pFrom, UINT uSize, UINT uOffset)
{
	PCWORD pData = PCWORD(pFrom);

	for( UINT n = 0, u = uOffset; n < uSize; n++, u++ ) {

		Slot.m_pData[u] = IntelToHost(WORD(pData[n]));
		}		

	Slot.m_fValid = TRUE;
	}

void CTotalFlow2Master::GetLongData(CSlot &Slot, PCBYTE pFrom, UINT uSize, UINT uOffset)
{
	PCDWORD pData = PCDWORD(pFrom);

	for( UINT n = 0, u = uOffset; n < uSize; n++, u++ ) {

		Slot.m_pData[u] = IntelToHost(DWORD(pData[n]));
		}

	Slot.m_fValid = TRUE;
	}

void CTotalFlow2Master::GetRealData(CSlot &Slot, PCBYTE pFrom, UINT uSize, UINT uOffset)
{
	PCDWORD pData = PCDWORD(pFrom);

	for( UINT n = 0, u = uOffset; n < uSize; n++, u++ ) {

		Slot.m_pData[u] = IntelToHost(DWORD(pData[n]));
		}

	Slot.m_fValid = TRUE;
	}

BOOL CTotalFlow2Master::IsBlock(CSlot& Slot1, CSlot& Slot2)
{
	UINT uMax = Slot1.m_fString ? Slot1.m_Reg + 1 : Slot1.m_Reg + BLOCK_SIZE;

	return (Slot1.m_App	== Slot2.m_App	   &&
		Slot1.m_Arr	== Slot2.m_Arr     &&
		Slot1.m_Type	== Slot2.m_Type    &&
		Slot1.m_fString == Slot2.m_fString &&
		Slot2.m_Reg	<= uMax);
	}

UINT CTotalFlow2Master::GetDataOffset(CSlot * pSlot, AREF Addr)
{
	if( pSlot && pSlot->m_fString ) {

		return Addr.a.m_Offset;
		}

	return 0;
	}

UINT CTotalFlow2Master::GetMaxCount(void)
{
	return 16;
	}

UINT CTotalFlow2Master::GetStringOffset(CSlot * pSlot, AREF Addr)
{
	return Addr.a.m_Offset;
	}

WORD CTotalFlow2Master::GetWriteOffset(CSlot * pSlot, AREF Addr)
{
	return 0;
	}

WORD CTotalFlow2Master::GetWriteCount(CSlot * pSlot, UINT uCount)
{
	return 1;
	}

UINT CTotalFlow2Master::GetStringSize(CSlot * pSlot)
{
	return	pSlot->m_Size;
	}

WORD CTotalFlow2Master::GetRecordCount(PCBYTE pFrom)
{
	return IntelToHost(PWORD(pFrom + 4)[0]);
	}

BOOL CTotalFlow2Master::IsWord(UINT m_Type)
{
	return m_Type == addrWordAsWord;
	}

BOOL CTotalFlow2Master::IsLong(UINT m_Type)
{
	return m_Type == addrLongAsLong || m_Type == addrRealAsReal;
	}

UINT CTotalFlow2Master::GetDataSize(CBlock Block)
{
	if( IsWord(Block.m_Type) ) {
		
		return 2 * Block.m_Size;
		}

	if( IsLong(Block.m_Type) ) {

		UINT uTerm = Block.m_fString ? 1 : 0;

		return 4 * Block.m_Size + uTerm;
		}

	return 1 * Block.m_Size;
	}

void CTotalFlow2Master::CheckDataSize(CSlot Slot, UINT &uSize, PCBYTE pData)
{
	if( Slot.m_fString ) {

		UINT uRead = 0;

		while( isalpha(pData[uRead]) ||
		       isdigit(pData[uRead]) ||
		       ispunct(pData[uRead]) ||
		       isspace(pData[uRead]) ) {

			uRead++;

			if( uRead == uSize ) {

				return;
				}
			}

		while( pData[uRead] == 0 ) {

			uRead++;

			if( uRead == uSize ) {

				return;
				}
			}

		uSize = uRead;
		}
	}

// Slot Sorting

int CTotalFlow2Master::SortFunc(PCVOID p1, PCVOID p2)
{
	CSlot ** a1 = (CSlot**)p1;

	CSlot ** a2 = (CSlot**)p2;
	
	CSlot * s1  = a1[0];

	CSlot * s2  = a2[0];

	////////

	if( s1->m_App < s2->m_App ) return -1;
	
	if( s1->m_App > s2->m_App ) return +1;

	////////

	if( s1->m_Arr < s2->m_Arr ) return -1;

	if( s1->m_Arr > s2->m_Arr ) return +1;

	////////

	if( s1->m_Reg < s2->m_Reg ) return -1;

	if( s1->m_Reg > s2->m_Reg ) return +1;

	////////

	return 0;
	}

// End of File
