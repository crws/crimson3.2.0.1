#include "intern.hpp"

#include "emroc.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Master Serial Driver
//

// Instantiator

INSTANTIATE(CEmRocMasterSerialDriver);


// Constructor

CEmRocMasterSerialDriver::CEmRocMasterSerialDriver(void)
{
	m_Ident         = DRIVER_ID; 

	m_bGroup	= 2;

	m_bUnit		= 1;

	m_TxPtr		= 0;

	m_RxPtr		= 0;

	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx));
	}

// Destructor

CEmRocMasterSerialDriver::~CEmRocMasterSerialDriver(void)
{
	}

// Configuration

void MCALL CEmRocMasterSerialDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bGroup   = GetByte(pData);

		m_bUnit    = GetByte(pData);
		}
	}

	
void MCALL CEmRocMasterSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CEmRocMasterSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEmRocMasterSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmRocMasterSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bGroup  = GetByte(pData);
			
			m_pCtx->m_bUnit	  = GetByte(pData);

			m_pCtx->m_Device  = 1 << GetByte(pData);

			m_pCtx->m_bLast   = 0xFF;

			m_pCtx->m_uLast   = 0;
			
			m_pCtx->m_uTime   = 0;

			m_pCtx->m_uPoll   = ToTicks(GetWord(pData));

			m_pCtx->m_Pass    = GetWord(pData);

			m_pCtx->m_Use     = GetByte(pData);

			m_pCtx->m_Acc	  = GetByte(pData);

			m_pCtx->m_OpID    = GetString(pData);

			m_pCtx->m_Logon   = FALSE;
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmRocMasterSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmRocMasterSerialDriver::Ping(void)
{	
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
		
	Addr.a.m_Offset = 1;
		
	Addr.a.m_Type   = addrLongAsLong;

	if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

		return CCODE_SUCCESS;
		} 

	if( !m_pCtx->m_Logon ) {

		return LogOn();
		}

	return CCODE_ERROR; 
	}

CCODE MCALL CEmRocMasterSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsOp0(Addr) ) {

		return DoOpcode0(Addr, pData, uCount);
		}

	if( IsOp6(Addr) ) {

		return DoOpcode6(Addr, pData, uCount);
		}

	if( IsOp103(Addr) ) {

		return DoOpcode103(Addr, pData, uCount);
		}

	if( IsOp105(Addr) ) {

		return DoOpcode105(Addr, pData, uCount);
		}

	if( IsOp107(Addr) ) {

		return DoOpcode107(Addr, pData, uCount);
		}

	if( IsOp165(Addr) ) {

		return DoOpcode165(Addr, pData, uCount);
		}
	
	if( Addr.a.m_Table == addrNamed ) {

		if( IsTime(Addr) ) {

			return DoOpcode7(Addr, pData, uCount);
			}
		}

	return DoOpcode180(Addr, pData, uCount);
	}

CCODE MCALL CEmRocMasterSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsOp0(Addr) ) {

		return uCount;
		}

	if( IsOp6(Addr) ) {

		return uCount;
		}

	if( IsOp103(Addr) ) {

		return uCount;
		}

	if( IsOp105(Addr) ) {

		return uCount;
		}

	if( IsOp107(Addr) ) {

		return uCount;
		}

	if( IsOp165(Addr) ) {

		return uCount;
		}

	if( Addr.a.m_Table == addrNamed ) {

		if( IsTime(Addr) ) {

			return DoOpcode8(Addr, pData, uCount);
			}
		}
	
	return DoOpcode181(Addr, pData, uCount);
	}

// Handlers

CCODE CEmRocMasterSerialDriver::LogOn(void)
{
	Begin();

	AddByte(17);

	AddByte(m_pCtx->m_Use ? 6 : 5);

	AddText(m_pCtx->m_OpID, 3);

	AddWord(m_pCtx->m_Pass);

	if( m_pCtx->m_Use ) {

		AddByte(m_pCtx->m_Acc);
		}

	End();

	if( Transact() ) {

		m_pCtx->m_Logon = TRUE;

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode0(AREF Addr, PDWORD pData, UINT uCount)
{
	// Currently disabled in config - heavily dependant on correct user configuration - Should we support???
	
	if( m_pCtx->m_bLast == 0 ) {

		if( !IsTimedOut() ) {

			if( GetOpcode0Data(Addr, pData, uCount) ) {

				return uCount;
				}

			return CCODE_ERROR | CCODE_HARD;
			}
		}
	
	Begin();

	AddByte(0);				// Opcode

	AddByte(2);				// Data Length

	AddByte(0);				// Block

	AddByte(0x7F);				// Data Flag

	End();

	if( Transact() ) {

		memcpy(m_pCtx->m_OpData, m_bRx, m_RxPtr);

		if( GetOpcode0Data(Addr, pData, uCount) ) {

			m_pCtx->m_bLast = 0;

			m_pCtx->m_uTime = GetTickCount();

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

BOOL  CEmRocMasterSerialDriver::GetOpcode0Data(AREF Addr, PDWORD pData, UINT uCount)
{
	// TODO ????

	return FALSE;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode6(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pCtx->m_bLast == 6 ) {

		if( !IsTimedOut() ) {

			if( GetOpcode6Data(Addr, pData, uCount) ) {

				return uCount;
				}

			return CCODE_ERROR | CCODE_HARD;
			}
		}

	Begin();
	
	AddByte(6);				// Opcode

	AddByte(0);				// Data Length

	End();

	if( Transact() ) {

		memcpy(m_pCtx->m_OpData, m_bRx, m_RxPtr);

		if( GetOpcode6Data(Addr, pData, uCount) ) {

			m_pCtx->m_bLast = 6;

			m_pCtx->m_uTime = GetTickCount();

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode103(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pCtx->m_bLast == 103 ) {

		if( !IsTimedOut() ) {

			if( GetOpcode103Data(Addr, pData, uCount) ) {

				return uCount;
				}

			return CCODE_ERROR | CCODE_HARD;
			}
		}
	
	Begin();
	
	AddByte(103);				// Opcode

	AddByte(0);				// Data Length

	End();

	if( Transact() ) {

		memcpy(m_pCtx->m_OpData, m_bRx, m_RxPtr);

		if( GetOpcode103Data(Addr, pData, uCount) ) {

			m_pCtx->m_bLast = 103;

			m_pCtx->m_uTime = GetTickCount();

			return uCount;
			}
		}

	return CCODE_ERROR;
	}


CCODE CEmRocMasterSerialDriver::DoOpcode105(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pCtx->m_bLast == 105 ) {

		if( m_pCtx->m_uLast == Addr.a.m_Offset ) {

			if( !IsTimedOut() ) {

				if( GetOpcode105Data(Addr, pData, uCount) ) {

					return 1;
					}

				return CCODE_ERROR | CCODE_HARD;
				}
			}
		}
	
	Begin();
	
	AddByte(105);				// Opcode

	AddByte(1);				// Data Length

	AddByte(Addr.a.m_Offset);		// History Index

	End();

	if( Transact() ) {

		memcpy(m_pCtx->m_OpData, m_bRx, m_RxPtr);

		if( GetOpcode105Data(Addr, pData, uCount) ) {

			m_pCtx->m_bLast = 105;

			m_pCtx->m_uLast = Addr.a.m_Offset;

			m_pCtx->m_uTime = GetTickCount();

			return 1;
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode107(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, IsString(Addr.a.m_Table) ? 3 : 1);
	
	if( m_pCtx->m_bLast == 107 ) {

		if( m_pCtx->m_uLast == Addr.a.m_Offset ) {
		
			if( !IsTimedOut() ) {

				if( GetOpcode107Data(Addr, pData, uCount) ) {

					return uCount;
					}
				}
			}
		}

	Begin();
	
	AddByte(107);						// Opcode

	AddByte(3);						// Data Length

	AddByte(0);						// Base RAM

	UINT uOp = GetOperand(Addr.a.m_Table);

	AddByte(1);						// Number of Historical Points (Device Under Test sends error otherwise)

	AddByte(Addr.a.m_Offset / GetOperand(Addr.a.m_Table));	// Logical Historical Point

	End();

	if( Transact() ) {

		memcpy(m_pCtx->m_OpData, m_bRx, m_RxPtr);

		if( GetOpcode107Data(Addr, pData, uCount) ) {

			m_pCtx->m_bLast = 107;

			m_pCtx->m_uLast = Addr.a.m_Offset;

			m_pCtx->m_uTime = GetTickCount();

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode165(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pCtx->m_bLast == 165 ) {

		if( !IsTimedOut() ) {

			if( GetOpcode165Data(Addr, pData, uCount) ) {

				return uCount;
				}

			return CCODE_ERROR | CCODE_HARD;
			}
		}
	
	Begin();
	
	AddByte(165);				// Opcode

	AddByte(4);				// Data Length

	AddByte(0);				// 0

	AddByte(0);			        // 0

	AddByte(0);				// Floboss 100/500 Series
			
	AddByte(0);				// 0

	End();

	if( Transact() ) {

		memcpy(m_pCtx->m_OpData, m_bRx, m_RxPtr);

		if( GetOpcode165Data(Addr, pData, uCount) ) {

			m_pCtx->m_bLast = 165;

			m_pCtx->m_uTime = GetTickCount();

			return uCount;
			}

		}

	return CCODE_ERROR;
	}  

BOOL CEmRocMasterSerialDriver::GetOpcode6Data(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == 208 ) {

		GetOpStringLongs(Addr.a.m_Table, pData, uCount, 26);

		return TRUE;
		}

	switch( Addr.a.m_Offset ) {

		case 7:		pData[0] = m_pCtx->m_OpData[6];		return TRUE;
		case 8:		pData[0] = m_pCtx->m_OpData[7];		return TRUE;
		case 9:		pData[0] = m_pCtx->m_OpData[8];		return TRUE;
		case 10:	pData[0] = m_pCtx->m_OpData[9];		return TRUE;
		case 11:	pData[0] = m_pCtx->m_OpData[10];	return TRUE;
		case 12:	pData[0] = m_pCtx->m_OpData[11];	return TRUE;
		case 13:	pData[0] = m_pCtx->m_OpData[12];	return TRUE;
		case 14:	pData[0] = m_pCtx->m_OpData[13];	return TRUE;
		case 15:	pData[0] = m_pCtx->m_OpData[14];	return TRUE;
		case 16:	pData[0] = m_pCtx->m_OpData[15];	return TRUE;
		case 17:	pData[0] = m_pCtx->m_OpData[18];	return TRUE;
		case 18:	pData[0] = m_pCtx->m_OpData[22];	return TRUE;
		case 19:	pData[0] = m_pCtx->m_OpData[23];	return TRUE;
		case 20:	pData[0] = m_pCtx->m_OpData[24];	return TRUE;
		case 21:	pData[0] = m_pCtx->m_OpData[25];	return TRUE;
		case 22:	pData[0] = m_pCtx->m_OpData[46];	return TRUE;
		case 23:	pData[0] = m_pCtx->m_OpData[47];	return TRUE;
		case 24:	pData[0] = m_pCtx->m_OpData[49];	return TRUE;
		case 25:	pData[0] = m_pCtx->m_OpData[50];	return TRUE;
		case 26:	pData[0] = m_pCtx->m_OpData[51];	return TRUE;
		case 27:	pData[0] = m_pCtx->m_OpData[52];	return TRUE;
		case 28:	pData[0] = m_pCtx->m_OpData[53];	return TRUE;
		case 29:	pData[0] = m_pCtx->m_OpData[54];	return TRUE;
		case 30:	pData[0] = m_pCtx->m_OpData[55];	return TRUE;
		case 31:	pData[0] = m_pCtx->m_OpData[56];	return TRUE;
		case 32:	pData[0] = m_pCtx->m_OpData[57];	return TRUE;
		case 33:	pData[0] = m_pCtx->m_OpData[58];	return TRUE;
		case 34:	pData[0] = m_pCtx->m_OpData[59];	return TRUE;
		case 35:	pData[0] = m_pCtx->m_OpData[60];	return TRUE;
		case 36:	pData[0] = m_pCtx->m_OpData[61];	return TRUE;
		case 37:	pData[0] = m_pCtx->m_OpData[62];	return TRUE;
		case 38:	pData[0] = m_pCtx->m_OpData[63];	return TRUE;
		case 39:	pData[0] = m_pCtx->m_OpData[64];	return TRUE;
		case 40:	pData[0] = m_pCtx->m_OpData[65];	return TRUE;
		case 41:	pData[0] = m_pCtx->m_OpData[66];	return TRUE;
		case 42:	pData[0] = m_pCtx->m_OpData[67];	return TRUE;
		case 43:	pData[0] = m_pCtx->m_OpData[68];	return TRUE;
		case 44:	pData[0] = m_pCtx->m_OpData[69];	return TRUE;
		case 45:	pData[0] = m_pCtx->m_OpData[70];	return TRUE;
		case 46:	pData[0] = m_pCtx->m_OpData[71];	return TRUE;
		case 47:	pData[0] = m_pCtx->m_OpData[72];	return TRUE;
		case 48:	pData[0] = m_pCtx->m_OpData[76];	return TRUE;
		case 49:	pData[0] = m_pCtx->m_OpData[77];	return TRUE;
		case 50:	pData[0] = m_pCtx->m_OpData[78];	return TRUE;
		case 51:	pData[0] = m_pCtx->m_OpData[79];	return TRUE;
		case 52:	pData[0] = m_pCtx->m_OpData[80];	return TRUE;
		case 53:	pData[0] = m_pCtx->m_OpData[81];	return TRUE;
		case 54:	pData[0] = m_pCtx->m_OpData[82];	return TRUE;
		case 55:	pData[0] = m_pCtx->m_OpData[83];	return TRUE;
		case 56:	pData[0] = m_pCtx->m_OpData[85];	return TRUE;
		}
		
	return FALSE;
	}

BOOL CEmRocMasterSerialDriver::GetOpcode103Data(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table != addrNamed ) {

		UINT uOffset = 0;

		switch( Addr.a.m_Table ) {

			case 207:	uOffset = 20;	break;
			case 206:	uOffset = 40;	break;
			case 205:	uOffset = 60;	break;
			case 204:	uOffset = 82;	break;
			}

		GetOpStringLongs(Addr.a.m_Table, pData, uCount, uOffset);

		return TRUE;
		}

	switch( Addr.a.m_Offset ) {

		case 57:	pData[0] = m_pCtx->m_OpData[80];	return TRUE;
		case 58:	pData[0] = m_pCtx->m_OpData[81];	return TRUE;

		case 59:	pData[0] += Time(m_pCtx->m_OpData[104], m_pCtx->m_OpData[103], m_pCtx->m_OpData[102]);
				pData[0] += Date(m_pCtx->m_OpData[107], m_pCtx->m_OpData[106], m_pCtx->m_OpData[105]);
				
				return TRUE;
		}
		
	return FALSE;
	}


BOOL CEmRocMasterSerialDriver::GetOpcode105Data(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD x = 0;

	UINT  o = 0;

	switch( Addr.a.m_Table ) {

		case 218:
		case 217:
		case 216:

			x = PU4(m_pCtx->m_OpData + 12 + 4 * (218 - Addr.a.m_Table))[0];

			pData[0] = IntelToHost(x);
 
			return TRUE;

		case 215:
		case 214:

			o = 24 + 5 * (215 - Addr.a.m_Table); 

			pData[0] = 0;

			pData[0] += Time(m_pCtx->m_OpData[o + 2], m_pCtx->m_OpData[o + 1], m_pCtx->m_OpData[o]);

			pData[0] += Date(0, m_bRx[o + 4], m_pCtx->m_OpData[o + 3]);

			return TRUE;



		case 213:
		case 212:

			x = PU4(m_pCtx->m_OpData + 34 + 4 * (213 - Addr.a.m_Table))[0];

			pData[0] = IntelToHost(x);

			return TRUE;

		case 211:
		case 210:

			o = 42 + 5 * (211 - Addr.a.m_Table); 

			pData[0] = 0;

			pData[0] += Time(m_pCtx->m_OpData[o + 2], m_pCtx->m_OpData[o + 1], m_pCtx->m_OpData[o]);

			pData[0] += Date(0, m_bRx[o + 4], m_pCtx->m_OpData[o + 3]);


			return TRUE;

		case 209:

			x = PU4(m_pCtx->m_OpData + 52)[0];

			pData[0] = IntelToHost(x);

			return TRUE;
		}



	return FALSE;
	}

BOOL CEmRocMasterSerialDriver::GetOpcode107Data(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = 0;
	
	if( Addr.a.m_Table == 203 ) {

		UINT uWidth   = GetOperand(Addr.a.m_Table);

		MakeMin(uCount, m_pCtx->m_OpData[7]);

		uOffset  = Addr.a.m_Offset / uWidth;

		if( uOffset < m_pCtx->m_OpData[8] ) {

			return FALSE;
			}

		if( uOffset >  m_pCtx->m_OpData[8] + uCount ) {

			return FALSE;
			}

		GetOpStringLongs(Addr.a.m_Table, pData, uWidth / 4, 9);
	
		return TRUE;
		}

	if( Addr.a.m_Table == 202 ) {

		uOffset = Addr.a.m_Offset;

		if( uOffset < m_pCtx->m_OpData[8] ) {

			return FALSE;
			}

		MakeMin(uCount, m_pCtx->m_OpData[7]);
		
		if( uOffset >  m_pCtx->m_OpData[8] + uCount ) {

			return FALSE;
			}
		 
		for( UINT y = uOffset, u = 0; y < uOffset + uCount; y++ ) {

			WORD w = PU2(m_pCtx->m_OpData + 19 + u * 12)[0];

			pData[u] = LONG(SHORT(IntelToHost(w)));
			}

		return TRUE;
		}
	
	return FALSE;
	}


BOOL CEmRocMasterSerialDriver::GetOpcode165Data(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uPts = m_pCtx->m_OpData[8];

	UINT i = 9;

	UINT t = 222;

	while( Addr.a.m_Table != t ) {

		i++;

		t--;
		}

	for( UINT u = 0, x = 0; u < uPts; u++, i+=4 ) {

		if( u == Addr.a.m_Offset + x ) {

			pData[x] = m_pCtx->m_OpData[i];

			x++;

			if( x == uCount ) {

				return TRUE;
				}
			}

		}

	return FALSE;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode180(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsString(Addr.a.m_Table) ) {

		return DoOpcode180String(Addr, pData, uCount);
		}

	MakeMin(uCount, 24);

	Begin();

	AddByte(180);			// Opcode

	AddByte(3 * uCount + 1);	// Data Length
		
	AddByte(uCount);		// Number of Params

	UINT uTable  = Addr.a.m_Table;

	BOOL fColl   = HasCollection(uTable);

	UINT uOffset = fColl ? ((Addr.a.m_Offset & 0xFFE0) >> 5) : Addr.a.m_Offset;

	BYTE bType   = GetPointType(Addr);

	UINT uAbs    = HasAbsolute(bType) ? uOffset : 0;

	BYTE bPar    = GetParameterNumber(uTable, Addr.a.m_Offset, bType);

	for( UINT x = 0; x < uCount; x++ ) {

		AddByte(bType);	

		if( !fColl ) {

			AddByte(uAbs + x);				

			AddByte(bPar);					

			continue;
			}
	
		AddByte(uAbs);
		
		AddByte(GetParameterNumber(uTable, Addr.a.m_Offset + x, bType));
		}

	End();

	if( Transact() ) {

		MakeMin(uCount, m_bRx[6]);

		switch( Addr.a.m_Type ) {

			case addrByteAsByte:	return GetBytes(pData, uCount);

			case addrWordAsWord:	return GetWords(pData, uCount);

			case addrLongAsLong:
			case addrRealAsReal:	return GetLongs(pData, uCount);
			
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode180String(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT Count = uCount;

	MakeMin(Count, GetOperand(Addr.a.m_Table));
	
	Begin();

	AddByte(180);					// Opcode

	AddByte(3 + 1);					// Data Length
		
	AddByte(1);					// Number of Params

	BYTE bType   = GetPointType(Addr);

	UINT uTable  = Addr.a.m_Table;

	UINT uOffset = Addr.a.m_Offset;

	BYTE bAbs    = HasAbsolute(bType) ? BYTE(uOffset / GetOperand(uTable)) : 0;
	
	AddByte(bType);					

	AddByte(bAbs);
	
	AddByte(GetParameterNumber(uTable, uOffset, bType));
	
	End();

	if( Transact() ) {

		GetStringLongs(uTable, pData, Count, 10);

		return Count;
		}
	
	return CCODE_ERROR;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode7(AREF Addr, PDWORD pData, UINT uCount)
{
	Begin();					// Read Current Time

	AddByte(7);					// OpCode

	AddByte(0);					// Data Length

	End();

	if( Transact() ) {

		if( Addr.a.m_Offset == 1 ) {

			pData[0] = 0;

			pData[0] += Time(m_bRx[8], m_bRx[7], m_bRx[6]);

			pData[0] += Date(m_bRx[11], m_bRx[10], m_bRx[9]);
			}
		
		else {
			pData[0] = m_bRx[Addr.a.m_Offset + 10];
			}

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode181(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsReadOnly(Addr) ) {

		return uCount;
		}
	
	if( IsString(Addr.a.m_Table) ) {

		return DoOpcode181String(Addr, pData, uCount);
		}

	MakeMin(uCount, 24);

	UINT uLen = 1 * uCount;

	if( IsWord(Addr.a.m_Type) ) {

		uLen = 2 * uCount;
		}
	
	else if( IsLong(Addr.a.m_Type) ) {

		uLen = 4 * uCount;
		}	
	
	Begin();

	AddByte(181);			// Opcode

	AddByte(3 * uCount + uLen + 1);	// Data Length
		
	AddByte(uCount);		// Number of Params 

	UINT uTable  = Addr.a.m_Table;

	BOOL fColl   = HasCollection(uTable);

	UINT uOffset = fColl ? ((Addr.a.m_Offset & 0xFFE0) >> 5) : Addr.a.m_Offset;

	UINT uType   = GetPointType(Addr);  

	UINT uAbs    = HasAbsolute(uType) ? uOffset : 0;

	UINT uPar    = GetParameterNumber(uTable, Addr.a.m_Offset, uType);

	for( UINT x = 0; x < uCount; x++ ) {

		AddByte(uType);		// Point type

		if( !fColl ) {

			AddByte(uAbs + x);	// Absolute

			AddByte(uPar);		// Param
			}
		else {
			AddByte(uAbs);		// Absolute

			AddByte(uPar + x);	// Param
			}

		AddData(Addr.a.m_Type, pData, x);
		}

	End();

	if( Transact() ) {

		MakeMin(uCount, m_bRx[6]);

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode181String(AREF Addr, PDWORD pData, UINT uCount)
{
	Begin();

	AddByte(181);							// Opcode

	AddByte(3 + GetStringLen(Addr.a.m_Table) + 3);			// Data Length
		
	AddByte(1);							// Number of Params

	UINT uType   = GetPointType(Addr);

	UINT uTable  = Addr.a.m_Table;

	UINT uOffset = Addr.a.m_Offset;
	
	AddByte(uType);							// Point type

	AddByte(BYTE(uOffset / GetOperand(uTable)));			// Absolute

	AddByte(GetParameterNumber(uTable, uOffset, uType));		// Param

	UINT uLen = GetStringLen(Addr.a.m_Table);
	
	UINT uPad  = uLen % 4;

	UINT uBytes = uLen + uPad;

	PBYTE pBytes = PBYTE(alloca(uBytes));

	memset(pBytes, 0x20, uBytes);
	
	MakeMin(uLen, uCount * sizeof(DWORD));

	memcpy(pBytes, PBYTE(pData), uLen);
	
	UINT Count = GetOperand(Addr.a.m_Table);

	for( UINT u = 0; u < Count; u++ ) {

		DWORD x = PDWORD(pBytes + u * sizeof(DWORD))[0];

		WORD  l = LOWORD(x);

		WORD  h = HIWORD(x);

		AddByte(HIBYTE(h));

		AddByte(LOBYTE(h));

		AddByte(HIBYTE(l));

		AddByte(LOBYTE(l));
		}

	End();

	if( Transact() ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEmRocMasterSerialDriver::DoOpcode8(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Offset > 1 ) {			// Read Only

		return uCount;
		}
	
	PDWORD pRead = PDWORD(alloca(uCount));
	
	if( COMMS_SUCCESS(DoOpcode7(Addr, pRead, uCount)) ) {

		Begin();				// Write Current Time

		AddByte(8);				// OpCode

		AddByte(8);				// Data Len

		UINT uTime = 0;

		uTime += Time(m_bRx[8], m_bRx[7], m_bRx[6]);

		uTime += Date(m_bRx[11], m_bRx[10], m_bRx[9]);

		BYTE bLeap = m_bRx[12];

		BYTE bCurr = m_bRx[13];

		switch( Addr.a.m_Offset ) {

			case 1:	uTime = pData[0];	 	break;
			case 2:	bLeap = pData[0] & 0xFF; 	break;
			case 3: bCurr = pData[0] & 0xFF; 	break;
			}

		AddByte(GetSec  (uTime) % 60 );
	
		AddByte(GetMin  (uTime) % 60 );
	
		AddByte(GetHour (uTime) % 24 );
	
		AddByte(GetDate (uTime)      );
	
		AddByte(GetMonth(uTime) % 12 );
	
		AddByte(GetYear (uTime) % 100);

		AddByte(bLeap);

		AddByte(bCurr);

		End();

		if( Transact() ) {

			return 1;
			}
		}

	return CCODE_ERROR;
	}

// Implementation

BYTE CEmRocMasterSerialDriver::GetPointType(AREF Addr)
{
	if( Addr.a.m_Table < addrNamed ) {

		switch( Addr.a.m_Table ) {

			case 196:
			case 197:
			case 198:
			case 199:
			case 200:
			case 201:
				
				return 15;
			}

		for( UINT x = 0; x < elements(Start); x++ ) {

			if( (Addr.a.m_Table >= Start[x]) && Addr.a.m_Table <= Finish[x] ) {

				return x;
				}
			}
		}  

	for( UINT y = 12; y < elements(Start); y++ ) {

		if( Addr.a.m_Offset >= Start[y] && Addr.a.m_Offset <= Finish[y] ) {

			return y;
			}
		} 
	
	return 0;
	}

BYTE CEmRocMasterSerialDriver::GetParameterNumber(UINT uTable, UINT uOffset, UINT uPoint)
{
	switch( uPoint ) {

		case 1:		
		case 2:
		case 3:
		case 4:
		case 5:
		case 7:
			return uTable - Start[uPoint];

	    	case 8:
			if( IsString(uTable) ) {
			
				return uTable - Start[uPoint] + ((uOffset - 1) / GetOperand(uTable)) * 4;
				}

			return uTable - Start[uPoint] + (uOffset - 1) * 4;

		case 13:
			return uOffset;

		case 15:

			if( IsString(uTable) ) {

				switch( uTable ) {

					case 196:	return 15;
					case 197:	return 14;
					case 198:	return 13;
					case 199:	return 12;
					case 200:	return 11;
					case 201:	return 2;

					}
				}
								
			return uOffset - Start[uPoint];
		
		case 16:
			if( uTable < 114 ) {

				return uTable - Start[uPoint];
				}

			if( uTable == 114 ) {

				return 2  + (uOffset & 0xF) - 1;
				}

			if( uTable == 115 ) {

				return 12 + (uOffset & 0xF) - 1;
				}

			if( uTable == 116 ) {

				return 16 + (uOffset & 0xF) - 1;
				}

			if( uTable == 118 ) {

				return 19 + (uOffset & 0xF) - 1;
				}

			if( uTable > 118 ) {

				return uTable - 119 + 23;
				}

			break;

		case 17:
			if( uTable < 126 ) {

				return uTable - Start[uPoint];
				}

			if( uTable == 126 ) {

				return 2 + (uOffset & 0xF) - 1;
;
				}

			break;
		}   

	return 0;
	}

UINT CEmRocMasterSerialDriver::GetBytes(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = PBYTE(m_bRx + 7 + 3 * (u + 1) + sizeof(BYTE) * u)[0];
		}

	return uCount;
	}

UINT CEmRocMasterSerialDriver::GetWords(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		WORD x = PU2(m_bRx + 7 + 3 * (u + 1) + sizeof(WORD) * u)[0];

		pData[u] = LONG(SHORT(IntelToHost(x)));
		}

	return uCount;
	}


UINT CEmRocMasterSerialDriver::GetLongs(PDWORD pData, UINT uCount)
{
       for( UINT u = 0; u < uCount; u++ ) {

	       	DWORD x = PU4(m_bRx + 7 + 3 * (u + 1) + sizeof(DWORD) * u)[0];
		
		pData[u] = IntelToHost(x);
		}

	return uCount;
	}

UINT CEmRocMasterSerialDriver::GetStringLongs(UINT uTable, PDWORD pData, UINT uCount, UINT uOffset)
{
	UINT uLongs   = GetOperand(uTable);

	MakeMin(uLongs, uCount);

	UINT uLen = GetStringLen(uTable);

	UINT uPad = uLen % 4;

	if( uPad ) {

		memset(PBYTE(m_bRx + uOffset + uLen), 0x20, uPad);
		}

	for( UINT u = 0; u < uLongs; u++ ) {

		DWORD x = PU4(m_bRx + uOffset + sizeof(DWORD) * u)[0];

		pData[u] = MotorToHost(x);
		}

	return uCount;
	}

UINT CEmRocMasterSerialDriver::GetOpStringLongs(UINT uTable, PDWORD pData, UINT uCount, UINT uOffset)
{
	UINT uLongs   = GetOperand(uTable);

	MakeMin(uLongs, uCount);

	UINT uLen = GetStringLen(uTable);

	UINT uPad = uLen % 4;

	if( uPad ) {

		memset(PBYTE(m_pCtx->m_OpData + uOffset + uLen), 0x20, uPad);
		}

	for( UINT u = 0; u < uLongs; u++ ) {

		DWORD x = PU4(m_pCtx->m_OpData + uOffset + sizeof(DWORD) * u)[0];

		pData[u] = MotorToHost(x);
		}

	return uCount;
	}

void CEmRocMasterSerialDriver::Begin(void)
{
	m_TxPtr = 0;

	m_CRC.Clear();

	AddDest();

	AddHost();
	}

void CEmRocMasterSerialDriver::End(void)
{
	AddWord(m_CRC.GetValue());
	}

void CEmRocMasterSerialDriver::AddDest(void)
{
	AddByte(m_pCtx->m_bUnit);

	AddByte(m_pCtx->m_bGroup);
	}

void CEmRocMasterSerialDriver::AddHost(void)
{
	AddByte(m_bUnit);

	AddByte(m_bGroup);
	}

void CEmRocMasterSerialDriver::AddData(UINT uType, PDWORD pData, UINT uPos)
{
	if( IsLong(uType) ) {

		AddLong(pData[uPos]);

		return;
		}

	if( IsWord(uType) ) {

		AddWord(WORD(pData[uPos]));

		return;
		}

	AddByte(BYTE(pData[uPos]));
	}


void CEmRocMasterSerialDriver::AddByte(BYTE bByte)
{
	m_bTx[m_TxPtr] = bByte;

	AddCheck(bByte);

	m_TxPtr++;
       	}

void CEmRocMasterSerialDriver::AddWord(WORD wWord)
{
	AddByte(LOBYTE(wWord));

	AddByte(HIBYTE(wWord));
	}

void CEmRocMasterSerialDriver::AddLong(DWORD dwWord)
{
	AddWord(LOWORD(dwWord));

	AddWord(HIWORD(dwWord));
	}

void CEmRocMasterSerialDriver::AddText(PTXT pText, UINT uCount)
{
	for( UINT u = 0; u < uCount;  u++ ) {

		AddByte(pText[u]);
		}
	}

void CEmRocMasterSerialDriver::AddCheck(BYTE bByte)
{
	m_CRC.Add(bByte);
	}


BOOL CEmRocMasterSerialDriver::Transact(void)
{
	if( Send() ) {

		if( Recv() ) {

			if( CheckFrame() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CEmRocMasterSerialDriver::Send(void)
{
	m_pData->Write(m_bTx, m_TxPtr, FOREVER);

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_TxPtr; u++ ) {

		AfxTrace("%2.2x ", m_bTx[u]);
		}

*/	return TRUE;
	}

BOOL CEmRocMasterSerialDriver::Recv(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	UINT uTotal = 0;

	m_CRC.Clear();

	m_RxPtr = 0;

//	AfxTrace("\nRx : ");

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

//		AfxTrace("%2.2x ", uData);

		m_bRx[m_RxPtr] = uData;

		m_CRC.Add(uData);

		if( m_RxPtr == 5 ) {

			uTotal = uData + 8;
			}

		else if( uTotal ) {

			if( m_RxPtr >= (uTotal - 1) ) {

				return CheckCRC();
				}

			if( m_RxPtr >= sizeof(m_bRx) ) {

				// TODO:  Request next block of data !! 
				}
			}

		m_RxPtr++;
		} 

	return FALSE;
	}

BOOL CEmRocMasterSerialDriver::CheckFrame(void)
{
	if( m_bRx[0] != m_bUnit	) {

		return FALSE;
		}

	if( m_bRx[1] != m_bGroup ) {

		return FALSE;
		}

	if( m_bRx[2] != m_pCtx->m_bUnit ) {

		return FALSE;
		}

	if( m_bRx[3] != m_pCtx->m_bGroup ) {

		return FALSE;
		}

	if( m_bRx[4] == 0xFF ) {

		// ERROR

		switch( m_bRx[6] ) {

			case 20:
			case 21:
			case 63:

				m_pCtx->m_Logon = FALSE;
				break;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CEmRocMasterSerialDriver::CheckCRC(void)
{
	return !m_CRC.GetValue();
	}

// Helpers

BOOL CEmRocMasterSerialDriver::IsWord(UINT uType)
{
	return uType == addrWordAsWord;
	}

BOOL CEmRocMasterSerialDriver::IsLong(UINT uType)
{
	switch( uType ) {

		case addrLongAsLong:
		case addrRealAsReal:
	
			return TRUE;
		}

	return FALSE;
	}

BOOL CEmRocMasterSerialDriver::IsString(UINT uTable)
{
       switch( uTable ) {

		case 1:
		case 12:
		case 24:
		case 31:
		case 39:
		case 40:
		case 75:
		case 76:
		case 85:
		case 86:
		case 108:
		case 109:
		case 112:
		case 116:
		case 117:
		case 124:
		case 127:
		case 140:
		case 172:
		case 173:
		case 174:
		case 175:
		case 196:
		case 197:
		case 198:
		case 199:
		case 200:
		case 201:
		case 203:
		case 204:
		case 205:
		case 206:
		case 207:
		case 208:


			return TRUE;
		}

	
	return FALSE;
	}

UINT CEmRocMasterSerialDriver::GetStringLen(UINT uTable)
{
	switch( uTable ) {

		case 1:
		case 12:
		case 24:
		case 31:
		case 39:
		case 40:
		case 75:
		case 76:
		case 85:
		case 86:
		case 108:
		case 109:
		case 112:
		case 117:
		case 124:
		case 127:
		case 172:
		case 173:
		case 174:
		case 175:
		case 203:

			return 10;

		case 197:

			return 12;

		case 196:
		case 198:
		case 199:
		case 200:
		case 201:
		case 204:
		case 205:
		case 206:
		case 208:

			return 20;

		case 116:
		case 140:

			return 30;

		case 207:
			
			return 40;

		}

	return 0;
	}

UINT CEmRocMasterSerialDriver::GetOperand(UINT uTable)
{
	UINT uLen = GetStringLen(uTable);

	if( uLen == 0 ) {

		return 1;
		}

	while( uLen % 4 != 0 ) {

		uLen++;
		}

	if( uLen ) {

		return uLen / sizeof(DWORD);
		}

	return 1;
	}

BOOL CEmRocMasterSerialDriver::HasAbsolute(UINT uPoint)
{
	switch( uPoint ) {

		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 7:
		case 16:
		case 17:
			return TRUE;
		}

	return FALSE;
	}

BOOL CEmRocMasterSerialDriver::HasCollection(UINT uTable)
{
	switch( uTable ) {

		case 114:
		case 115:
		case 116:
		case 118:
		case 126:

			return TRUE;
		}

	return FALSE;
	}

BOOL CEmRocMasterSerialDriver::IsReadOnly(AREF Addr)
{	
	if( Addr.a.m_Table == addrNamed ) {

		if( Addr.a.m_Offset >= 76 && Addr.a.m_Offset <= 84 ) {

			return TRUE;
			}

		return FALSE;
		}

	switch( Addr.a.m_Table ) {

		case   5:
		case  23:
		case  29:
		case  55:
		case  57:
		case  59:
		case  60:
		case  61:
		case  62:
		case  63:
		case  72:
		case  83:
		case  84:
		case 100:
		case 102:
		case 104:
		case 105:
		case 107:
		case 108:
		case 117:
		case 135:
		case 141:
		case 196:
		case 197:
		case 198:
		case 199:
		case 200:
		
			return TRUE;

	
		case 109:
			if( Addr.a.m_Offset < 9 * GetOperand(Addr.a.m_Table) ) {
	
				return TRUE;
				}

			break;

		case 110:
		case 111:

			if( Addr.a.m_Offset < 9 ) {

				return TRUE;
				}

			break;


		}

	return FALSE;
	}

BOOL CEmRocMasterSerialDriver::IsTime(AREF Addr)
{
	if( Addr.a.m_Table == addrNamed ) {

		return Addr.a.m_Offset < 4;
		}

	return FALSE;
	}

BOOL CEmRocMasterSerialDriver::IsOp0(AREF Addr)
{
	if( Addr.a.m_Table == addrNamed ) {

		return Addr.a.m_Offset >= 4 && Addr.a.m_Offset <= 6;
		}

	return Addr.a.m_Table >= 223 && Addr.a.m_Table <= 239;
	}

BOOL CEmRocMasterSerialDriver::IsOp6(AREF Addr)
{	
	if( Addr.a.m_Table == addrNamed ) {

		return Addr.a.m_Offset >= 7 && Addr.a.m_Offset <= 56;
		}

	return Addr.a.m_Table == 208;
	}

BOOL CEmRocMasterSerialDriver::IsOp103(AREF Addr)
{
	if( Addr.a.m_Table == addrNamed ) {

		return Addr.a.m_Offset >= 57 && Addr.a.m_Offset <=59;
		}

	return Addr.a.m_Table >= 204 && Addr.a.m_Table <= 207;
	}

BOOL CEmRocMasterSerialDriver::IsOp105(AREF Addr)
{
	return Addr.a.m_Table >= 209 && Addr.a.m_Table <= 218;
	}

BOOL CEmRocMasterSerialDriver::IsOp107(AREF Addr)
{
	return Addr.a.m_Table >= 202 && Addr.a.m_Table <= 203;
	}

BOOL CEmRocMasterSerialDriver::IsOp165(AREF Addr)
{
 	return Addr.a.m_Table >= 219 && Addr.a.m_Table <= 222;
	}

BOOL CEmRocMasterSerialDriver::IsTimedOut(void)
{
	return int(GetTickCount() - m_pCtx->m_uTime - m_pCtx->m_uPoll) >= 0;
	}

 // End of file
