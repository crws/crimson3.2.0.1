//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Parameter Info
//
/*
struct FAR Param {

	BYTE m_Dev;
	UINT m_ID;
	UINT m_Mask;
	BYTE m_Size;
	};

struct FAR Point {

	BYTE	m_Point;
	Param	m_Params[64];
	};

struct FAR Op	{

	BYTE	m_OpCode;
	BYTE	m_ReqLen;
	Point	m_Points[64];
	};
 */


//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CEmRocMasterSerialDriver;

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Master Lookups and Enumerations
//

static BYTE const Start[]  = {	  0,  1, 24, 39, 75,  85, 0, 127, 108, 0, 0, 0, 1, 181, 0, 60, 112, 124 };

static BYTE const Finish[] = {	  0, 23, 38, 74, 84, 107, 0, 180, 111, 0, 0, 0, 5, 181, 0, 85, 123, 126 };

enum Device {

	deviceFlo100	= 0x01,
	deviceFlo107	= 0x02,
	deviceFlo407	= 0x04,
	deviceFlo500	= 0x08,
	deviceRegFlo	= 0x10,
	deviceROCPAC	= 0x20,
	deviceFlashPac	= 0x40,
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Master Serial Driver
//

class CEmRocMasterSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CEmRocMasterSerialDriver(void);

		// Destructor
		~CEmRocMasterSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		
	protected:
		// Device Context
		struct CContext 
		{
			BYTE m_bGroup;
			BYTE m_bUnit;
			BYTE m_Device;
			BYTE m_bLast;
			UINT m_uLast;
			UINT m_uTime;
			UINT m_uPoll;
			
			PTXT m_OpID;
			UINT m_Pass;
			BYTE m_Acc;
			BOOL m_Logon;
			BOOL m_Use;

			BYTE m_OpData[1200];
			};

		// Data Members
		CContext * m_pCtx;

		BYTE	m_bGroup;
		BYTE	m_bUnit;
		BYTE	m_bTx[248];
		BYTE	m_bRx[1200];
		UINT	m_TxPtr;
		UINT	m_RxPtr;
		CRC16	m_CRC;

		// Handlers
		CCODE	LogOn(void);
		CCODE	DoOpcode0(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode6(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode103(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode105(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode107(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode165(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   DoOpcode180(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode180String(AREF Addr, PDWORD pData, UINT uCount); 
		CCODE	DoOpcode7(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   DoOpcode181(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   DoOpcode181String(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode8(AREF Addr, PDWORD pData, UINT uCount);
		
		// Implementation
		BOOL    GetOpcode0Data(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	GetOpcode6Data(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	GetOpcode103Data(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	GetOpcode105Data(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	GetOpcode107Data(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	GetOpcode165Data(AREF Addr, PDWORD pData, UINT uCount);
		BYTE	GetPointType(AREF Addr);
		BYTE	GetParameterNumber(UINT uTable, UINT uOffset, UINT uPoint);
		UINT	GetBytes(PDWORD pData, UINT uCount);
		UINT	GetWords(PDWORD pData, UINT uCount);
		UINT	GetLongs(PDWORD pData, UINT uCount);
		UINT	GetStringLongs(UINT uTable, PDWORD pData, UINT uCount, UINT uOffset);
		UINT	GetOpStringLongs(UINT uTable, PDWORD pData, UINT uCount, UINT uOffset);
		void	Begin(void);
		void	End(void);
		void	AddDest(void);
		void	AddHost(void);
		void	AddData(UINT uType, PDWORD pData, UINT uPos);
		void	AddByte(BYTE bByte);
		void	AddWord(WORD wWord);
		void	AddLong(DWORD dwWord);
		void	AddText(PTXT pText, UINT uCount);
		void	AddCheck(BYTE bByte);
		BOOL	Transact(void);
		BOOL	Send(void);
		BOOL	Recv(void);
		BOOL	CheckFrame(void);
		BOOL	CheckCRC(void);

		// Helpers
		BOOL IsWord(UINT uType);
		BOOL IsLong(UINT uType);
		BOOL IsString(UINT uTable);
		UINT GetStringLen(UINT uTable);
		UINT GetOperand(UINT uTable);
		BOOL HasAbsolute(UINT uPoint);
		BOOL HasCollection(UINT uTable);
		BOOL IsReadOnly(AREF Addr);
		BOOL IsTime(AREF Addr);
		BOOL IsOp0(AREF Addr);
		BOOL IsOp6(AREF Addr); 
		BOOL IsOp103(AREF Addr);
		BOOL IsOp105(AREF Addr);
		BOOL IsOp107(AREF Addr);
		BOOL IsOp165(AREF Addr);
		BOOL IsTimedOut(void);

	
	};


// End of file

