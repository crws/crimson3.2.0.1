
//////////////////////////////////////////////////////////////////////////
//
// CoriolisMMI Modbus TCP Driver
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "CoriolisMMITCP.hpp"

// Instantiator

INSTANTIATE(CCoriolisTCPDriver);

// Constructor

CCoriolisTCPDriver::CCoriolisTCPDriver(void)
{
	m_Ident	    = DRIVER_ID;

	m_uKeep     = 0;

	m_pTCPCtx   = NULL;

	m_pCtx	    = NULL;

	m_uMaxWords = 128;
	
	m_uMaxBits  = 2000;
	}

// Destructor

CCoriolisTCPDriver::~CCoriolisTCPDriver(void)
{
	
	}
		
// Device

CCODE CCoriolisTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pTCPCtx = (CTCPContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pTCPCtx = new CTCPContext;
			
			m_pTCPCtx->m_PingReg	= GetWord(pData);
			m_pTCPCtx->m_bUnit	= GetByte(pData);
			m_pTCPCtx->m_fFlipLong	= 0;
			m_pTCPCtx->m_fFlipReal	= 0;

			m_pTCPCtx->m_IP1		= GetAddr(pData);
			m_pTCPCtx->m_IP2		= GetAddr(pData);
			m_pTCPCtx->m_uPort		= GetWord(pData);
			m_pTCPCtx->m_fKeep		= GetByte(pData);
			m_pTCPCtx->m_fPing		= GetByte(pData);
			m_pTCPCtx->m_uTime1		= GetWord(pData);
			m_pTCPCtx->m_uTime2		= GetWord(pData);
			m_pTCPCtx->m_uTime3		= GetWord(pData);

			m_pTCPCtx->m_uNumCoilRegs	= GetWord(pData);

			m_pTCPCtx->m_pCoilRegisters  = new CAddress[m_pTCPCtx->m_uNumCoilRegs];

			for( UINT d = 0; d < m_pTCPCtx->m_uNumCoilRegs; d++ ) {

				CAddress *Addr	= &m_pTCPCtx->m_pCoilRegisters[d];

				Addr->m_Ref	= GetLong(pData);
				}

			m_pTCPCtx->m_uNumHoldingRegs	= GetWord(pData);

			m_pTCPCtx->m_pHoldingRegisters  = new CAddress[m_pTCPCtx->m_uNumHoldingRegs];

			for( UINT e = 0; e < m_pTCPCtx->m_uNumHoldingRegs; e++ ) {

				CAddress *Addr		= &m_pTCPCtx->m_pHoldingRegisters[e];

				Addr->m_Ref		= GetLong(pData);
				}

			m_pTCPCtx->m_fDisable15	= FALSE;
			m_pTCPCtx->m_fDisable16	= FALSE;
			m_pTCPCtx->m_fDisable5	= FALSE;
			m_pTCPCtx->m_fDisable6	= FALSE;			
			m_pTCPCtx->m_fNoReadEx	= FALSE;
			m_pTCPCtx->m_wTrans	= 0;
			m_pTCPCtx->m_pSock	= NULL;
			m_pTCPCtx->m_uLast	= GetTickCount();

			m_pTCPCtx->m_uMax01	= 512;
			m_pTCPCtx->m_uMax02	= 512;
			m_pTCPCtx->m_uMax03	= 32;
			m_pTCPCtx->m_uMax04	= 32;
			m_pTCPCtx->m_uMax15	= 512;
			m_pTCPCtx->m_uMax16	= 32;

			m_pTCPCtx->m_fDirty	= FALSE;
			m_pTCPCtx->m_fAux       = FALSE;

			Limit(m_pTCPCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pTCPCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pTCPCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pTCPCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pTCPCtx->m_uMax15, 1, m_pTCPCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pTCPCtx->m_uMax16, 1, m_pTCPCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pTCPCtx);

			m_pCtx = m_pTCPCtx;

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	m_pCtx = m_pTCPCtx;

	return CCODE_SUCCESS;
	}

CCODE CCoriolisTCPDriver::DeviceClose(BOOL fPersist) {

	if( !fPersist ) {

		CloseSocket(FALSE);

		delete m_pTCPCtx->m_pCoilRegisters; 
			
		m_pTCPCtx->m_pCoilRegisters = NULL;

		delete m_pTCPCtx->m_pHoldingRegisters;

		m_pTCPCtx->m_pHoldingRegisters = NULL;
		}

	return CModbusTCPMaster::DeviceClose(fPersist);
	}

// Entry Points

CCODE CCoriolisTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return CModbusTCPMaster::Read(Addr, pData, uCount);
	}

CCODE CCoriolisTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( GetMaxCount(Addr, uCount) ) {
		
		return CModbusTCPMaster::Write(Addr, pData, uCount);
		}

	return uCount;
	}

// Implemetation

BOOL CCoriolisTCPDriver::GetMaxCount(AREF Address, UINT &uCount) 
{
	BOOL fCoils	= IsCoils(Address.a.m_Table);

	CAddress *pRegs	= GetRegArray(fCoils);

	UINT uRegs	= GetRegTotal(fCoils);

	UINT uOffset	= Address.a.m_Offset;

	UINT uSize	= GetSize(Address);

	UINT Count	= 0;

	for( UINT i = 0; i < uRegs; i++ ) {

		CAddress Reg  = pRegs[i];

		if( uOffset == Reg.a.m_Offset ) {

			if( Reg.a.m_Extra ) {
				
				if( Reg.m_Ref != Address.m_Ref ) {

					return FALSE;
					}
				
				MakeMin(uCount, GetSize(Reg));

				return TRUE;
				}

			UINT uSpan = 0;

			while ( uOffset <= Reg.a.m_Offset && i < uRegs && Count < uCount ) {

				uSpan = Reg.a.m_Offset - uOffset;
				
				if( uSpan ) {
					
					break;
					}
				else {
					Count++;
					}

				uOffset += uSize;

				i++;

				Reg = pRegs[i];
				}

			MakeMin(uCount, Count);

			return TRUE;
			}

		while ( uOffset < Reg.a.m_Offset ) {

			Count++;

			uOffset += uSize;

			if( uOffset >= Reg.a.m_Offset || Count == uCount ) {

				MakeMin(uCount, Count);

				return FALSE;
				}
			}
		}

	MakeMin(uCount, 1);

	return FALSE;
	}

UINT CCoriolisTCPDriver::GetSize(AREF Addr)
{
	if( Addr.a.m_Extra ) {

		return Addr.a.m_Extra + 1;
		}

	switch( Addr.a.m_Type ) {

		case addrWordAsLong:
		case addrWordAsReal:	return 2;

		case addrLongAsLong:
		case addrRealAsReal:	return 4;
		}

	return 1;
	}

UINT CCoriolisTCPDriver::GetRegTotal(BOOL fCoils)
{
	return fCoils ? m_pTCPCtx->m_uNumCoilRegs   : m_pTCPCtx->m_uNumHoldingRegs;
	}


CAddress * CCoriolisTCPDriver::GetRegArray(BOOL fCoils)
{
	return fCoils ? m_pTCPCtx->m_pCoilRegisters : m_pTCPCtx->m_pHoldingRegisters;
	}

// Helpers

BOOL CCoriolisTCPDriver::IsCoils(UINT uTable)
{
	return ((uTable != inputRegisters) && (uTable != holdingRegisters));	
	}


// End of File
