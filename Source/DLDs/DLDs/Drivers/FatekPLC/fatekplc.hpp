
//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC Driver
//
#include "fatebase.hpp"

#ifndef	FATEKSERIALINC
#define	FATEKSERIALINC

class CFatekPLCSerialDriver : public CFatekPLCBaseDriver
{
	public:
		// Constructor
		CFatekPLCSerialDriver(void);

		// Destructor
		~CFatekPLCSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:
		// Device Data
		struct CContext : CFatekPLCBaseDriver::CBaseCtx
		{
			};

		CContext *	m_pCtx;
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);

		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);
	};

// End of File

#endif
