
//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC Base Driver
//

#include "intern.hpp"

#include "fatebase.hpp"

// Constructor

CFatekPLCBaseDriver::CFatekPLCBaseDriver(void)
{
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_pBase	= NULL;
	}

// Destructor

CFatekPLCBaseDriver::~CFatekPLCBaseDriver(void)
{
	}

// Entry Points

CCODE MCALL CFatekPLCBaseDriver::Ping(void)
{
	return TestLoopBack();
	}

CCODE MCALL CFatekPLCBaseDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( ReadNoTransmit(Addr, pData) ) return 1;

//**/	AfxTrace3("\r\nRead T=%d O=%d C=%x ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	UINT uCt = min(8, uCount);

	AddRead(Addr, uCt);

	if( Transact() ) {

		UINT uSize    = 1;

		switch(Addr.a.m_Type) {

			case BB:
				break;

			case WW:
				uSize = 4;
				break;

			case LL:
			case RR:
				uSize = 8;
				break;
			}

		GetResponse(pData, uCt, uSize);

		return uCt;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CFatekPLCBaseDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( WriteNoTransmit(Addr, pData) ) return 1;

//**/	AfxTrace3("\r\n\n**** Write T=%d O=%d C=%x ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	UINT uCt = Addr.a.m_Type == WW ? min(8, uCount) : 1;

	AddWrite(Addr, uCount, pData);

	if( Transact() ) {

		m_uWriteError = 0;

		return uCt;
		}

	if( ++m_uWriteError > 2 ) {

		m_uWriteError = 0;

		return 1;
		}
	
	return CCODE_ERROR;
	}

CCODE CFatekPLCBaseDriver::TestLoopBack(void)
{
	StartFrame();

	AddHex(LOOPBK);
	AddHex(0x12);
	AddHex(0x34);

	memset(m_bRx, 0, 12);

	Transact();

	m_pBase->m_dLatestError = 0;

	if( GetGeneric(5,4) == 0x1234 ) {

		m_pBase->m_dLatestError = 0;

		return 1;
		}

	return CCODE_ERROR;
	}

// Frame Building

void CFatekPLCBaseDriver::AddRead(AREF Addr, UINT uCount)
{
	if( IsLong(Addr.a.m_Type) && uCount > 2 ) uCount -= 1; // Longs = 2 Words

	BuildFrame(Addr, uCount, FALSE, 0);
	}

void CFatekPLCBaseDriver::AddWrite(AREF Addr, UINT uCount, PDWORD pData)
{
	BYTE bData  = 0;

	BOOL fData  = BOOL(*pData);

	switch( GetBitType(Addr.a.m_Table) ) {

		case ISBITSTAT:	bData = fData ? BITSET : BITRST; break;
		case ISBITENBL:	bData = fData ? BITDIS : BITENB; break; // 1 = Disable
		default:
			if( IsCmds(Addr.a.m_Table) ) {

				bData = fData ? '1' : '0';
				}

			break;
		}

	if( !BuildFrame(Addr, uCount, TRUE, bData) ) {

		if( Addr.a.m_Type == WW ) {

			for( UINT i = 0; i < uCount; i++ ) {

				AddData(pData[i], TRUE, 0x1000);
				}
			}

		else AddData(*pData, TRUE, 0x10000000);
		}
	}

BOOL CFatekPLCBaseDriver::BuildFrame(AREF Addr, UINT uCount, BOOL fIsWrite, BYTE bWriteData)
{
	StartFrame();

	UINT uTable  = Addr.a.m_Table;

	if( IsCmds(uTable) ) {

		if( fIsWrite ) {

			AddHex(RUNSTOP);

			AddByte(bWriteData);
			}

		else {
			AddHex(RSSTAT);
			}

		return TRUE;
		}

	UINT uType   = Addr.a.m_Type;

	BOOL fIsBits = Addr.a.m_Type == BB;

	AddReqCode(uTable, fIsBits, fIsWrite);

	if( fIsBits && fIsWrite ) {

		AddByte(bWriteData);
		}

	else {
		AddHex(LOBYTE(uCount));
		}

	AddLetters(uTable, uType);

	AddStart(uTable, Addr.a.m_Offset);

	return fIsBits; // no further info to be added if BB
	}

void CFatekPLCBaseDriver::StartFrame(void)
{
	m_uPtr = 0;
	AddByte(STX);
	AddByte(m_pBase->Drop[0]);
	AddByte(m_pBase->Drop[1]);
	}

void CFatekPLCBaseDriver::AddReqCode(UINT uTable, BOOL fIsBits, BOOL fIsWrite)
{
	if( fIsWrite ) {

		AddHex(fIsBits ? WONEB : WCONTR);

		return;
		}

	if( fIsBits ) {

		AddHex(GetBitType(uTable) == ISBITSTAT ? RDISCB : RENBLB);

		return;
		}

	AddHex(RCONTR);
	}

void CFatekPLCBaseDriver::AddLetters(UINT uTable, UINT uType)
{
	if( uType == BB ) AddBitsLetter(uTable);

	else AddRegsLetter(uTable, uType);
	}

void CFatekPLCBaseDriver::AddBitsLetter(UINT uTable)
{
	switch( uTable ) {

		case SP_XS:
		case SP_XE: AddByte('X'); return;

		case SP_YS:
		case SP_YE: AddByte('Y'); return;

		case SP_MS:
		case SP_ME: AddByte('M'); return;

		case SP_SS:
		case SP_SE: AddByte('S'); return;

		case SP_TS:
		case SP_TE: AddByte('T'); return;

		case SP_CS:
		case SP_CE: AddByte('C'); return;
		}
	}

void CFatekPLCBaseDriver::AddRegsLetter(UINT uTable, UINT uType)
{
	if( uType == LL ) AddByte('D');

	if( IsBits(uTable) ) {

		AddByte('W');

		AddBitsLetter(uTable);

		return;
		}

	if( uType == RR ) AddByte('D');

	switch( uTable ) {

		case SP_TR: AddByte('R'); AddByte('T'); return;
		case SP_CR: AddByte('R'); AddByte('C'); return;
		case SP_H:  AddByte('R'); return;
		case SP_D:  AddByte('D'); return;
		}
	}

void CFatekPLCBaseDriver::AddStart(UINT uTable, UINT uOffset)
{
	DWORD dFactor = IsHD(uTable) ? 10000L : 1000L;

	AddData(uOffset, FALSE, dFactor);
	}

void CFatekPLCBaseDriver::AddHex(BYTE bData)
{
	AddByte(m_pHex[bData / 16]);
	AddByte(m_pHex[bData % 16]);
	}

void CFatekPLCBaseDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr++] = bData;
		}
	}

void CFatekPLCBaseDriver::AddData(DWORD dData, BOOL fHex, DWORD dFactor)
{
	UINT uBase = fHex ? 16 : 10;

	while( dFactor ) {

		AddByte(m_pHex[(dData/dFactor) % uBase]);

		dFactor /= uBase;
		}
	}

void CFatekPLCBaseDriver::EndFrame(void)
{	
	BYTE b = MakeChecksum(m_bTx, m_uPtr);

	AddHex(b);

	AddByte(ETX);
	}

// Frame Building Helpers

BYTE CFatekPLCBaseDriver::GetBitType(UINT uTable)
{
	switch( uTable ) {

		case SP_XS:
		case SP_YS:
		case SP_MS:
		case SP_SS:
		case SP_TS:
		case SP_CS:
			return ISBITSTAT;

		case SP_XE:
		case SP_YE:
		case SP_ME:
		case SP_SE:
		case SP_TE:
		case SP_CE:
			return ISBITENBL;
		}

	return 0;
	}

BOOL CFatekPLCBaseDriver::IsBits(UINT uTable)
{
	return BOOL(GetBitType(uTable));
	}

BOOL CFatekPLCBaseDriver::IsCmds(UINT uTable)
{
	switch( uTable ) {

		case SP_ST:
		case SP_RS:
			return TRUE;
		}

	return FALSE;
	}

BOOL CFatekPLCBaseDriver::IsHD(UINT uTable)
{
	return uTable == SP_H || uTable == SP_D;
	}

BOOL CFatekPLCBaseDriver::IsLong(UINT uType)
{
	return uType == LL || uType == RR;
	}

// Transport Layer

BOOL CFatekPLCBaseDriver::Transact(void) // virtual
{
	return FALSE;
	}

BOOL CFatekPLCBaseDriver::CheckReply(UINT uLen)
{
	if( uLen < 9 ) return FALSE; // STX,AddrH,AddrL,CodeH,CodeL,Error,ChkH,ChkL,ETX

	uLen -= 3;

	BYTE bChk = MakeChecksum(m_bRx, uLen);

	return m_pHex[bChk/16] == m_bRx[uLen] && m_pHex[bChk%16] == m_bRx[uLen+1];
	}

void CFatekPLCBaseDriver::GetResponse(PDWORD pData, UINT uCount, UINT uSize)
{
	UINT uNext = min(uSize, 4); // Longs overlap

	for( UINT i = 0; i < uCount; i++ ) {

		pData[i] = GetGeneric(6 + (i * uNext), uSize);
		}
	}

// Port Access

// Helpers

BOOL CFatekPLCBaseDriver::ReadNoTransmit(AREF Addr, PDWORD pData)
{
	switch( Addr.a.m_Table ) {

		case SP_RS:

			*pData = RTNZERO;

			return TRUE;

		case SP_ERR:

			*pData = m_pBase->m_dLatestError;

			return TRUE;
		}

	return FALSE;
	}

BOOL CFatekPLCBaseDriver::WriteNoTransmit(AREF Addr, PDWORD pData)
{
	switch( Addr.a.m_Table ) {

		case SP_ST:

			return TRUE;

		case SP_ERR:

			m_pBase->m_dLatestError = 0;

			return TRUE;
		}

	return FALSE;
	}

BYTE CFatekPLCBaseDriver::MakeChecksum(PBYTE pBuff, UINT uLen)
{
	BYTE bChk = 0;

	for( UINT i = 0; i < uLen; i++ ) {

		bChk += pBuff[i];
		}

	return bChk;
	}

DWORD CFatekPLCBaseDriver::GetGeneric(UINT uPos, UINT uCount)
{
	DWORD d = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		d <<= 4;

		d += xtoi(m_bRx[uPos + i]);
		}

	return d;
	}

BYTE CFatekPLCBaseDriver::xtoi(BYTE b)
{
	if( b >= '0' && b <= '9' ) return b - '0';

	if( b >= 'A' && b <= 'F' ) return b - '7';

	if( b >= 'a' && b <= 'f' ) return b - 'W';

	return 0;
	}

// End of File
