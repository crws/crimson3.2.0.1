#include "intern.hpp"

#include "ez.hpp"

//////////////////////////////////////////////////////////////////////////
//
// EZ Base Master Driver
//

// Constructor

CEZMasterDriver::CEZMasterDriver(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	m_pBase = NULL;

	}

// Destructor

CEZMasterDriver::~CEZMasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CEZMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 6;
	Addr.a.m_Offset = 0x1;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1); 
	}

CCODE MCALL CEZMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uType = Addr.a.m_Type;

	BOOL fLong = IsLong(uType);

	MakeMin(uCount, UINT(fLong ? 36 : 78));

	UINT uActual = fLong ? uCount * 2 : uCount;
	
	Start();

	// Disable Block Reads since they do not appear to function as documented!

	BOOL fBlock = FALSE;

	fBlock ? DoBlockRead(Addr.a.m_Table, Addr.a.m_Offset, uActual) 
	       : DoMultiRead(Addr.a.m_Table, Addr.a.m_Offset, uActual);

	if( Transact() ) {

		return GetData(uType, uCount, pData, fBlock ? 9 : 6);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEZMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uType = Addr.a.m_Type;

	BOOL fLong = IsLong(uType);

	MakeMin(uCount, UINT(fLong ? 16 : 32));

	UINT uActual = fLong ? uCount * 2 : uCount;

	Start();

	AddByte(7 + uActual * 5 );

	AddID();

	AddByte(25);

	AddByte(uActual);

	AddData(Addr, pData, uCount);

	if( Transact() ) {

		return uCount;
		}

	return CCODE_ERROR;
	}
 
// Implementation

void CEZMasterDriver::DoBlockRead(UINT uTable, UINT uOffset, UINT uCount)
{
	AddByte(10);

	AddID();
	
	AddCommandCode(uTable);
	
	AddType(uTable);

	AddWord(uOffset - 1);

	AddCount(uTable, uCount);
	}

void CEZMasterDriver::DoMultiRead(UINT uTable, UINT uOffset, UINT uCount)
{
	AddByte(7 + uCount * 3);

	AddID();
	
	AddByte(21);

	AddCount(uTable, uCount);

	uOffset -= 1;

	for( UINT u = 0; u < uCount; u++) {

		AddType(uTable);

		AddWord(uOffset++);
		}
	}

void CEZMasterDriver::Start(void)
{
	m_uPtr = 0;

	m_bTxBuff[m_uPtr++] = EZ_START;
	}

void CEZMasterDriver::AddByte(BYTE bByte)
{
	m_bTxBuff[m_uPtr++] = bByte;
	}

void CEZMasterDriver::AddID(void)
{
	AddByte((((m_pBase->m_bGroup & 0x000F) << 4) | ((m_pBase->m_wUnit & 0x0FFF) >> 4)));

	AddByte(m_pBase->m_wUnit & 0x00FF);
	}

void CEZMasterDriver::AddCommandCode(UINT uTable)
{
	AddByte(IsDiscrete(uTable) ? 19 : 20);
	}

void CEZMasterDriver::AddType(UINT uTable)
{
	AddByte(GetType(uTable));
	}

void CEZMasterDriver::AddWord(UINT uOffset)
{
	WORD wOffset = WORD(uOffset);
	
	AddByte(HIBYTE(wOffset));

	AddByte(LOBYTE(wOffset));
	}

void CEZMasterDriver::AddCount(UINT uTable, UINT uCount)
{
	AddByte(uCount);
	}

void CEZMasterDriver::AddData(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsLong(Addr.a.m_Type) ) {

		AddLongData(Addr, pData, uCount);

		return;
		}

	AddWordData(Addr, pData, uCount);
	}

void CEZMasterDriver::AddWordData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uType = GetType(Addr.a.m_Table);

	UINT uOffset = Addr.a.m_Offset - 1;

	for( UINT u = 0; u < uCount; u++ ) {

		AddByte(uType);

		AddWord(uOffset++);

		AddWord(LOWORD(pData[u]));
		}
	}

void CEZMasterDriver::AddLongData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uType = GetType(Addr.a.m_Table);

	UINT uOffset = Addr.a.m_Offset - 1;

	for( UINT u = 0; u < uCount; u++ ) {

		AddByte(uType);

		AddWord(uOffset++);

		AddWord(LOWORD(pData[u]));

		AddByte(uType);

		AddWord(uOffset++);

		AddWord(HIWORD(pData[u]));
		}
	}

UINT CEZMasterDriver::GetData(UINT uType, UINT uCount, PDWORD pData, UINT uOffset)
{
	switch( uType ) {

		case addrBitAsBit:

			return GetDiscreteData(uCount, pData, uOffset);

		case addrWordAsWord:

			return GetWordData(uCount, pData, uOffset);
		}

	return GetLongData(uCount, pData, uOffset);
	}

UINT CEZMasterDriver::GetDiscreteData(UINT uCount,  PDWORD pData, UINT uOffset)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		pData[n] = m_bRxBuff[n + uOffset];
		}

	return uCount;
	}

UINT CEZMasterDriver::GetWordData(UINT uCount, PDWORD pData, UINT uOffset)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		WORD x = PU2(m_bRxBuff + uOffset)[n];
			
		pData[n] = LONG(SHORT(MotorToHost(x)));
		}

	return uCount;
	}

UINT CEZMasterDriver::GetLongData(UINT uCount, PDWORD pData, UINT uOffset)
{
	for( UINT n = 0, u = 0; n < uCount; n++ ) {
		
		WORD l = PU2(m_bRxBuff + uOffset)[u++];

		WORD h = PU2(m_bRxBuff + uOffset)[u++];

		DWORD x = l | ( h << 16 );

		pData[n] = MotorToHost(x);
		}

	return uCount;
	}

UINT CEZMasterDriver::GetType(UINT uTable)
{
	return uTable - 1;
	}

// Helpers

BOOL CEZMasterDriver::IsDiscrete(UINT uTable)
{
	switch( uTable ) {

		case 1:
		case 2:
		case 3:
		case 7:
			return TRUE;
		}

	return FALSE;
	}

BOOL CEZMasterDriver::IsLong(UINT uType) 
{
	switch( uType ) {

		case addrWordAsLong:
		case addrWordAsReal:

			return TRUE;
		}

	return FALSE;
	}

// Transport Layer

BOOL CEZMasterDriver::Transact(void)
{
	return FALSE;
	}

// End of File
