#include "intern.hpp"

#include "basefp2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP MEWTOCOL Base Driver
//

// Constructor

CMatFP2BaseDriver::CMatFP2BaseDriver(void)
{
	CTEXT Hex[]   = "0123456789ABCDEF";

	m_pHex	      = Hex;
	}

// Destructor

CMatFP2BaseDriver::~CMatFP2BaseDriver(void)
{
	}

	
// Entry Points

CCODE MCALL CMatFP2BaseDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CMatFP2BaseDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckSpace(Addr.a.m_Table) ) {

		UINT uType = Addr.a.m_Type;

		if( uType == addrBitAsBit ) {

			return DoBitRead(Addr, pData, uCount);
			}
		
		else if( uType == addrWordAsWord ) {

			return DoWordRead(Addr, pData, uCount);
			}

		return DoLongRead(Addr, pData, uCount);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CMatFP2BaseDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uSpace = Addr.a.m_Table;
	
	if( CheckSpace(uSpace) ) {

		if( !IsWriteable(uSpace) ) {

			return uCount;
			}

		UINT uType = Addr.a.m_Type;

		if( uType == addrBitAsBit ) {

			return DoBitWrite(Addr, pData, uCount);
			}
		
		else if( uType == addrWordAsWord ) {

			return DoWordWrite(Addr, pData, uCount);
			}

		return DoLongWrite(Addr, pData, uCount);
	       	}
	
	return CCODE_ERROR;
	}

// Read Handlers

CCODE CMatFP2BaseDriver::DoBitRead (AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 8);
	
	StartFrame();

	AddByte('R');

	AddByte('C');

	if( uCount > 1 ) {

		AddByte('P');

		AddGeneric(uCount, 10, 1);
		}
	else {
       		AddByte('S');
		}

	UINT uOffset = Addr.a.m_Offset;

	for( UINT u = 0; u < uCount; u++, uOffset++ ) {

		AddAddress(Addr.a.m_Table, uOffset);
		}

	if( Transact() ) {

		GetData(Addr.a.m_Type, uCount, pData);

		return uCount;
		}
	
	return CCODE_ERROR;
	}

CCODE CMatFP2BaseDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uSpace = Addr.a.m_Table;

	MakeMin(uCount, UINT(IsIndex(uSpace) ? 1 : 26));

	StartFrame();

	AddByte('R');
	
	if( IsRelay(uSpace) ) {

		AddByte('C');

		AddByte('C');
		}

	else if( IsData(uSpace) ) {

		AddByte('D');
		}

	AddAddressRange(uSpace, Addr.a.m_Offset, uCount);

	if( Transact() ) {

		GetData(Addr.a.m_Type, uCount, pData);

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CMatFP2BaseDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uResult = DoWordRead(Addr, pData, uCount * 2);

	if( uResult != CCODE_ERROR ) {

		return uResult / 2;
		}

	return CCODE_ERROR;
	}
		
// Write Handlers

CCODE CMatFP2BaseDriver::DoBitWrite (AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 8);

	StartFrame();

	AddByte('W');

	AddByte('C');

	if( uCount > 1 ) {

		AddByte('P');

		AddGeneric(uCount, 10, 1);
		}
	else {
       		AddByte('S');
		}

	UINT uOffset = Addr.a.m_Offset;

	for( UINT u = 0; u < uCount; u++, uOffset++ ) {

		AddAddress(Addr.a.m_Table, uOffset);

		AddByte(pData[0] == 0 ? '0' : '1');
		}
	
	if( Transact() ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CMatFP2BaseDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uSpace = Addr.a.m_Table;
	
	MakeMin(uCount, UINT(IsIndex(uSpace) ? 1 : 24));

	StartFrame();

	AddByte('W');

	if( IsRelay(uSpace) ) {

		AddByte('C');

		AddByte('C');
		}

	else if( IsData(uSpace) ) {

		AddByte('D');
		}

	AddAddressRange(uSpace, Addr.a.m_Offset, uCount);

	PutData(Addr.a.m_Type, uCount, pData);
		
	if( Transact() ) {

		return uCount;
		}

	return CCODE_ERROR;
	}
		
CCODE CMatFP2BaseDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uResult = DoWordWrite(Addr, pData, uCount * 2);

	if( uResult != CCODE_ERROR ) {

		return uResult / 2;
		}

	return CCODE_ERROR;
	}

void CMatFP2BaseDriver::AddByte(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;

	m_bCheck ^= bData;
	}

void CMatFP2BaseDriver::AddCheck(void)
{
	m_bTxBuff[m_uPtr++] = m_pHex[m_bCheck / 16];
	m_bTxBuff[m_uPtr++] = m_pHex[m_bCheck % 16];
	m_bTxBuff[m_uPtr++] = CR;
	}

void CMatFP2BaseDriver::AddAddress(UINT uSpace, UINT uOffset)
{
	switch( uSpace ) {

		case SPACE_X:	AddByte('X');	AddGeneric(uOffset, 16, 0x1000);	break;
		case SPACE_Y:	AddByte('Y');	AddGeneric(uOffset, 16, 0x1000);	break;
		case SPACE_T:	AddByte('T');	AddGeneric(uOffset, 10,   1000);	break;
		case SPACE_C:	AddByte('C');	AddGeneric(uOffset, 10,   1000);	break;
		case SPACE_SR:	
		case SPACE_R:	AddByte('R');	AddGeneric(uOffset, 16, 0x1000);	break;
		case SPACE_LR:	AddByte('L');	AddGeneric(uOffset, 16, 0x1000);	break;
		}
	}

void CMatFP2BaseDriver::AddAddressRange(UINT uSpace, UINT uOffset, UINT uCount)
{
	UINT uEnd = uOffset + uCount - 1;

	switch( uSpace ) {

		case SPACE_WX:	AddByte('X');	AddGeneric(uOffset, 10,   1000); AddGeneric(uEnd, 10,   1000);	break;
		case SPACE_WY:	AddByte('Y');	AddGeneric(uOffset, 10,   1000); AddGeneric(uEnd, 10,   1000);	break;
		case SPACE_WR:	AddByte('R');	AddGeneric(uOffset, 10,   1000); AddGeneric(uEnd, 10,   1000);	break;
		case SPACE_WL:	AddByte('L');	AddGeneric(uOffset, 10,   1000); AddGeneric(uEnd, 10,   1000);	break;
		case SPACE_DT:	AddByte('D');	AddGeneric(uOffset, 10,  10000); AddGeneric(uEnd, 10,  10000);	break;
		case SPACE_SDT:	AddByte('D');	uOffset = uOffset / 1000 * 10000 + uOffset % 1000;
						uEnd    = uEnd    / 1000 * 10000 + uEnd    % 1000;
						AddGeneric(uOffset, 10,  10000); AddGeneric(uEnd, 10,  10000);	break;
		case SPACE_FL:	AddByte('F');	AddGeneric(uOffset, 10,  10000); AddGeneric(uEnd, 10,  10000);	break;
		case SPACE_LD:	AddByte('L');	AddGeneric(uOffset, 10,  10000); AddGeneric(uEnd, 10,  10000);	break;
		case SPACE_SV:	AddByte('S');	AddGeneric(uOffset, 10,   1000); AddGeneric(uEnd, 10,   1000);	break;
		case SPACE_EV:	AddByte('K');	AddGeneric(uOffset, 10,   1000); AddGeneric(uEnd, 10,   1000);	break;
		case SPACE_IX:	AddByte('I');
				AddByte('X');	AddGeneric(0, 10,   10000); AddGeneric(uOffset, 10,  1000);	break;
		case SPACE_IY:	AddByte('I');
				AddByte('Y');	AddGeneric(0, 10,   10000); AddGeneric(uOffset, 10,  1000);	break;

		}
	}

		
void CMatFP2BaseDriver::GetData(UINT uType, UINT uCount, PDWORD pData)
{
	UINT uBytes = GetByteCount(uType);

	if( IsLong(uType) ) {

		uCount /= 2;
		}
		
	for( UINT u = 0; u < uCount; u++ ) {

		PCSTR pText = PCSTR(m_bRxBuff + 5 + uBytes * u);

		switch( uBytes ) {

			case 1:
		
				pData[u] = xtoin(pText, 1);
				break;

			case 4:
				pData[u] = SwapBytes(xtoin(pText, 4));
				break;

			case 8:
				pData[u] = SwapLong(xtoin(pText, 8));
				break;
			}
		}
	 }

void CMatFP2BaseDriver::PutData(UINT uType, UINT uCount, PDWORD pData)
{
	BOOL fLong = IsLong(uType);

	if( fLong ) {

		for( UINT u = 0; u < uCount / 2; u++ ) {

			AddGeneric(SwapLong(pData[u]), 16, 0x10000000);
			}

		return;
		}
		
	for( UINT u = 0; u < uCount; u++ ) {

		AddGeneric(SwapBytes(LOWORD(pData[u])), 16, 0x1000);
		}
	}

void CMatFP2BaseDriver::AddGeneric(DWORD dData, UINT uRadix, UINT uFactor)
{
	while( uFactor ) {
	
		AddByte( m_pHex[(dData / uFactor) % uRadix] );

		uFactor /= uRadix;
		}
	}

// Helpers

DWORD CMatFP2BaseDriver::xtoin(PCTXT pText, UINT uCount)
{
	DWORD dData = 0;
	
	while( uCount-- ) {
	
		char cData = *(pText++);
		
		if( cData >= '0' && cData <= '9' )
			dData = 16 * dData + cData - '0';

		else if( cData >= 'A' && cData <= 'F' )
			dData = 16 * dData + cData - 'A' + 10;

		else if( cData >= 'a' && cData <= 'f' )
			dData = 16 * dData + cData - 'a' + 10;

		else break;
		}

	return dData;
	}

WORD CMatFP2BaseDriver::SwapBytes(WORD wData)
{
	WORD wSwap = (wData << 8) & 0xFF00;

	wSwap |= ( (wData & 0xFF00) >> 8 );
		
	return wSwap;
	}

DWORD CMatFP2BaseDriver::SwapLong(DWORD dData)
{
	return SwapBytes(HIWORD(dData)) | ( SwapBytes(LOWORD(dData) ) << 16 );
	}

BOOL CMatFP2BaseDriver::IsLong(UINT uType)
{
	return ( (uType == addrWordAsLong) || (uType == addrWordAsReal) );
	}

BOOL CMatFP2BaseDriver::IsBit(UINT uType)
{
	return uType == addrBitAsBit;
	}

UINT CMatFP2BaseDriver::GetByteCount(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:	return 1;
		case addrWordAsWord:	return 4;
		case addrWordAsLong:
		case addrWordAsReal:	return 8;
		}

	return 0;
	}

BOOL CMatFP2BaseDriver::IsRelay(UINT uSpace)
{
	switch( uSpace ) {

		case SPACE_WX:
		case SPACE_WY:
		case SPACE_WR:
		case SPACE_WL:
		case SPACE_SR:
		case SPACE_LR:
		
			return TRUE;
		}

	return FALSE;
	}

BOOL CMatFP2BaseDriver::IsData(UINT uSpace)
{
	switch( uSpace ) {

		case SPACE_DT:
		case SPACE_FL:
		case SPACE_LD:
		case SPACE_SDT:
		case SPACE_IX:
		case SPACE_IY:
		
			return TRUE;
		}

	return FALSE;
	}

BOOL CMatFP2BaseDriver::IsIndex(UINT uSpace)
{
	switch( uSpace ) {

		case SPACE_IX:
		case SPACE_IY:
		
			return TRUE;
		}

	return FALSE;
	}

BOOL CMatFP2BaseDriver::CheckSpace(UINT uSpace)
{
	switch(uSpace)
	{
		case SPACE_DT:
		case SPACE_SDT:
		case SPACE_LD:
		case SPACE_FL:
		case SPACE_IX:
		case SPACE_IY:
		case SPACE_SV:
		case SPACE_EV:
		case SPACE_C:
		case SPACE_T:
		case SPACE_R:
		case SPACE_SR:
		case SPACE_LR:
		case SPACE_X:
		case SPACE_Y:
		case SPACE_WR:
		case SPACE_WL:
		case SPACE_WX:
		case SPACE_WY:

			return TRUE;
		}

	return FALSE;
	}

BOOL CMatFP2BaseDriver::IsWriteable(UINT uSpace)
{
	switch(uSpace)
	{
		case SPACE_DT:
		case SPACE_LD:
		case SPACE_FL:
		case SPACE_IX:
		case SPACE_IY:
		case SPACE_SV:
		case SPACE_EV:
		case SPACE_R:
		case SPACE_LR:
		case SPACE_Y:
		case SPACE_WR:
		case SPACE_WL:
		case SPACE_WY:

			return TRUE;
		}
	
	return FALSE;
	}

// Transport 

void CMatFP2BaseDriver::StartFrame(void)
{
	m_uPtr = 0;

	m_bCheck = 0;
		
	AddByte( '%' );
		
	AddByte( m_pHex[m_pCtx->m_bDrop / 16] );
	AddByte( m_pHex[m_pCtx->m_bDrop % 16] );

	AddByte( '#' );
	}
	
BOOL CMatFP2BaseDriver::SendFrame(void)
{
	m_pData->ClearRx();

	AddCheck();

	m_pData->Write( PCBYTE(m_bTxBuff), m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CMatFP2BaseDriver::GetFrame(void)
{
	UINT uState = 0;

	UINT uByte = 0;

	UINT uTimer = 0;
	
	UINT uCount = 0;	

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uByte = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == '%' ) {

					uState = 1;
					}
				break;
				
			case 1:
				if( uByte == CR ) {
				
					if( CheckReply(uCount) ) {
						
						return TRUE;
						}
					
					return FALSE;
					}
				
				m_bRxBuff[uCount++] = uByte;
				
				if( uCount == sizeof(m_bRxBuff) ) {
					
					return FALSE;
					}
					
				break;
			}
		}
		
	return FALSE;
	}
	

BOOL CMatFP2BaseDriver::CheckReply(UINT uCount)
{
	BYTE bCheck = '%';
	
	for( UINT uScan = 0; uScan < uCount - 2; uScan++ )  {

		bCheck ^= m_bRxBuff[uScan];
		}

	if( xtoin(PSTR(m_bRxBuff + uScan), 2) == bCheck ) {

		if( m_bRxBuff[2] == '!' ) {

			return FALSE;
			}

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CMatFP2BaseDriver::Transact(void)
{
	SendFrame();
				
	return GetFrame();
	}
 
// End of File
