#include "basefp2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP v2 via MEWTOCOL Serial Master
//

class CMatFP2SerialDriver : public CMatFP2BaseDriver {

	public:
		// Constructor
		CMatFP2SerialDriver(void);

		// Destructor
		~CMatFP2SerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
					
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:
			
	};

// End of File
