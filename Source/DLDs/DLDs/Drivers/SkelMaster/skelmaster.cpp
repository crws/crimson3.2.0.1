
#include "intern.hpp"

#include "SkelMaster.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX Series PLC Master Driver
//

// Instantiator

INSTANTIATE(CSkeletonMasterDriver);

// Constructor

CSkeletonMasterDriver::CSkeletonMasterDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CSkeletonMasterDriver::~CSkeletonMasterDriver(void)
{
	}

// Configuration

void MCALL CSkeletonMasterDriver::Load(LPCBYTE pData)
{
	AfxTrace("CSkeletonMasterDriver::Load\n");

	if( GetWord(pData) == 0x1234 ) {

		m_Data1 = GetWord(pData);
		m_Data2 = GetWord(pData);
		m_Data3 = GetWord(pData);
		m_Data4 = GetWord(pData);

		AfxTrace("CSkeletonMasterDriver::Load -- Data is %u %u %u %u\n", m_Data1, m_Data2, m_Data3, m_Data4);
		}
	}
	
void MCALL CSkeletonMasterDriver::CheckConfig(CSerialConfig &Config)
{
	AfxTrace("CSkeletonMasterDriver::CheckConfig\n");
	}
	
// Management

void MCALL CSkeletonMasterDriver::Attach(IPortObject *pPort)
{
	AfxTrace("CSkeletonMasterDriver::Bind\n");

	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CSkeletonMasterDriver::Open(void)
{
	AfxTrace("CSkeletonMasterDriver::Open\n");
	}

// Device

CCODE MCALL CSkeletonMasterDriver::DeviceOpen(IDevice *pDevice)
{
	AfxTrace("CSkeletonMasterDriver::DeviceOpen\n");

	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CCtx;

			m_pCtx->m_Data1 = GetWord(pData);
			m_pCtx->m_Data2 = GetWord(pData);
			m_pCtx->m_Data3 = GetWord(pData);
			m_pCtx->m_Data4 = GetWord(pData);

			AfxTrace("CSkeletonMasterDriver::DeviceOpen -- New Device %u\n", m_pCtx->m_Data1);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}
	else
		AfxTrace("CSkeletonMasterDriver::DeviceOpen -- Old Device %u\n", m_pCtx->m_Data1);

	return CCODE_SUCCESS;
	}

CCODE MCALL CSkeletonMasterDriver::DeviceClose(BOOL fPersist)
{
	AfxTrace("CSkeletonMasterDriver::DeviceClose\n");

	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CSkeletonMasterDriver::Ping(void)
{
	AfxTrace("CSkeletonMasterDriver::Ping\n");

	return CCODE_SUCCESS;
	}

void MCALL CSkeletonMasterDriver::Service(void)
{	
	AfxTrace("CSkeletonMasterDriver::Service\n");
	}

CCODE MCALL CSkeletonMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	AfxTrace("CSkeletonMasterDriver::Read type=%u addr=%c%u count=%u\n", Addr.a.m_Type, Addr.a.m_Table, Addr.a.m_Offset, uCount);

	pData[0] = 1234;

	return 1;
	}

CCODE MCALL CSkeletonMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	AfxTrace("CSkeletonMasterDriver::Write type=%u addr=%c%u count=%u\n", Addr.a.m_Type, Addr.a.m_Table, Addr.a.m_Offset, uCount);

	return 1;
	}

// End of File
