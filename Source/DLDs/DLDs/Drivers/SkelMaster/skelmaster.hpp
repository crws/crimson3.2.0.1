
//////////////////////////////////////////////////////////////////////////
//
// Skeleton Master Driver
//

class CSkeletonMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CSkeletonMasterDriver(void);

		// Destructor
		~CSkeletonMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Config
		struct CCtx
		{
			UINT m_Data1;
			UINT m_Data2;
			UINT m_Data3;
			UINT m_Data4;
			};

		// Driver Config
		UINT   m_Data1;
		UINT   m_Data2;
		UINT   m_Data3;
		UINT   m_Data4;

		// Current Device
		CCtx * m_pCtx;
	};

// End of File
