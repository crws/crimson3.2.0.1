//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP IEC Data Spaces
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Spaces
enum {
	SP_0	=  1, // coils
	SP_1	=  2, // input bits
	SP_3	=  3, // read-only registers
	SP_4	=  4, // holding registers
	SP_5	=  5, // 32 bit read only reals
	SP_6	=  6, // 64 bit read only reals
	SP_7	=  7, // 32 bit holding register reals
	SP_8	=  8, // 64 bit holding register reals
	SP_IX	= 20, // coils
	SP_QX	= 21, // input bits
	SP_QB	= 22, // registers 30000-31023
	SP_IB	= 23, // registers 40000-41023
	SP_Q	= 24, // registers 41024-42047
	SP_Q32	= 25, // 32 bit reals 30000-31022
	SP_Q64	= 26, // 64 bit reals 30000-31020
	SP_L32	= 27, // 32 bit reals 40000-42046
	SP_L64	= 28  // 64 bit reals 40000-42044
	};

#define	IEC_BIT_LOBYTE	1152
#define	IEC_BIT_LO	11520
#define	IEC_BIT_HI	11677
#define	IEC_LO_R_LO	1168
#define	IEC_LO_R_HI	3215
#define	IEC_HI_R_LO	3221
#define	IEC_HI_R_HI	5268

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP IEC TCP/IP Master Driver
//

class CYaskawaMPIECMaster : public CMasterDriver
{
	public:
		// Constructor
		CYaskawaMPIECMaster(void);

		// Destructor
		~CYaskawaMPIECMaster(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
				
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BYTE	 m_bUnit;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL     m_fDirty;
			BYTE	 m_ExtendedWrite;
			BYTE	 m_Endian;
			WORD	 m_IXHead;
			WORD	 m_QXHead;
			WORD	 m_IBHead;
			WORD	 m_QBHead;
			WORD	 m_QHead;
			BOOL	 m_fQHigh;
			BYTE	 m_Model;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		UINT	   m_uKeep;
		BOOL	   m_fIs64;
		
		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddDblLong(PDWORD pdwData);
		void	AddMotorOrder(WORD wData);
				
		// Transport Layer
		BOOL	SendFrame(void);
		BOOL	RecvFrame(void);
		BOOL	Transact(BOOL fIgnore);
		BOOL	CheckFrame(void);

		// Read Handlers
		CCODE	DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoRealRead(AREF Addr, PDWORD pData, UINT uCount);
		DWORD	Make64(WORD w1, WORD w2, WORD w3, WORD w4, UINT uTable);
		DWORD	Make32(WORD w1, WORD w2);

		// Write Handlers
		CCODE	DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoRealWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Overridables
		virtual	UINT	GetModbusSpace(CAddress Addr);
		virtual	BOOL	Is64Bit(UINT uTable);
		virtual	DWORD	Convert64To32(PDWORD p, UINT uTable);
		virtual	void	Convert32To64(DWORD d, PDWORD p, UINT uTable);

		// Helpers
		BOOL	AllowWrite(AREF Addr);
	
		// Socket Management
		BOOL	CheckSocket(void);
		BOOL	OpenSocket(void);
		void	CloseSocket(BOOL fAbort);
	};

// End of File
