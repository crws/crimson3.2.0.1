
#include "intern.hpp"

#include "unitrn90.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Unitronics M90 Driver
//

// Instantiator

INSTANTIATE(CUnitronicsM90Driver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CUnitronicsM90Driver::CUnitronicsM90Driver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Destructor

CUnitronicsM90Driver::~CUnitronicsM90Driver(void)
{
	}

// Configuration

void MCALL CUnitronicsM90Driver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CUnitronicsM90Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CUnitronicsM90Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CUnitronicsM90Driver::Open(void)
{
	}

// Device

CCODE MCALL CUnitronicsM90Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CUnitronicsM90Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CUnitronicsM90Driver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPMEMW;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read( Addr, Data, 1 );
	}

CCODE MCALL CUnitronicsM90Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !SetCount(Addr, &uCount) ) return CCODE_ERROR | CCODE_HARD;

	MakeMin( uCount, RMAX );

	AddRead(Addr, uCount);

	if( Transact() ) {

		GetResponse( pData, Addr, uCount );

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CUnitronicsM90Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsReadOnly(Addr.a.m_Table) ) return uCount;

	if( !SetCount(Addr, &uCount) )   return CCODE_ERROR | CCODE_HARD;

	AddWrite(Addr, pData, uCount);

	return Transact() ? uCount : CCODE_ERROR;
	}

// Frame Building

void CUnitronicsM90Driver::AddRead( AREF Addr, UINT uCount )
{
	StartFrame( Addr.a.m_Table, FALSE );

	if( Addr.a.m_Table != SPRTC ) {

		AddAddrCount(Addr.a.m_Offset, uCount);
		}
	}

void CUnitronicsM90Driver::AddWrite( AREF Addr, PDWORD pData, UINT uCount )
{
	UINT uScan;

	StartFrame( Addr.a.m_Table, TRUE );

	if( Addr.a.m_Table == SPRTC ) {

		for( uScan = 0; uScan < uCount; uScan++ ) {

			m_bRTC[Addr.a.m_Offset + uScan] = pData[uScan];
			}

		for( uScan = 0; uScan < RTCMAX; uScan++ ) {

			AddByte( m_pHex[m_bRTC[uScan] / 10] );

			AddByte( m_pHex[m_bRTC[uScan] % 10] );
			}

		return;
		}

	AddAddrCount(Addr.a.m_Offset, uCount);

	for( uScan = 0; uScan < uCount; uScan++ ) {

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	AddByte( pData[uScan] ? '1' : '0' );	break;

			case addrLongAsLong:	AddWord( HIWORD(pData[uScan]) );	// Fall Through

			case addrWordAsWord:	AddWord( LOWORD(pData[uScan]) );	break;

			case addrRealAsReal:

				AddWord( LOWORD(pData[uScan]) );
				AddWord( HIWORD(pData[uScan]) );
				break;
			}
		}
	}

void CUnitronicsM90Driver::StartFrame(UINT uTable, BOOL fIsWrite)
{
	m_uPtr = 0;

	AddByte(USTX);

	AddHexChars( m_pCtx->m_bDrop );

	AddOpType( uTable, fIsWrite );
	}

void CUnitronicsM90Driver::EndFrame(void)
{
	BYTE b = 0;

	for( UINT i = 1; i < m_uPtr; i++ ) {

		b += m_bTx[i];
		}

	AddHexChars(b);

	AddByte(CR);
	}

void CUnitronicsM90Driver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CUnitronicsM90Driver::AddHexChars(BYTE bData)
{
	AddByte(m_pHex[bData/16]);
	AddByte(m_pHex[bData%16]);
	}

void CUnitronicsM90Driver::AddWord(UINT uData)
{
	AddHexChars( HIBYTE(LOWORD(uData)) );
	AddHexChars( LOBYTE(LOWORD(uData)) );
	}

void CUnitronicsM90Driver::AddAddrCount(UINT uAddr, UINT uCount)
{
	AddWord( uAddr );
	AddHexChars( LOBYTE(uCount) );
	}

void CUnitronicsM90Driver::AddOpType(UINT uTable, BOOL fIsWrite )
{
	BYTE b1 = fIsWrite ? 'S' : 'R';
	BYTE b2 = 0;
	BYTE b3 = 0;

	switch( uTable ) {

		case SPOUT:
			b2 = 'A';
			break;

		case SPBIT:
			b2 = 'B';
			break;

		case SPINP:
			b2 = 'E';
			break;

		case SPSYSB:
			if( !fIsWrite ) b1 = 'G';
			b2 = 'S';
			break;

		case SPCTRB:
			b2 = 'M';
			break;

		case SPTMRB:
			b2 = 'T';
			break;

		case SPMEMW:
			b2 = 'W';
			break;

		case SPMEML:
			b2 = 'N';
			b3 = 'L';
			break;

		case SPMEMD:
			b2 = 'N';
			b3 = 'D';
			break;

		case SPMEMR:
			b2 = 'N';
			b3 = 'F';
			break;

		case SPSYSW:
			if( !fIsWrite ) b1 = 'G';
			b2 = 'F';
			break;

		case SPSYSL:
			b2 = 'N';
			b3 = 'H';
			break;

		case SPSYSD:
			b2 = 'N';
			b3 = 'J';
			break;

		case SPTMRP:
			b1 = 'G';
			b2 = 'P';
			break;

		case SPTMRV:
			b1 = 'G';
			b2 = 'T';
			break;

		case SPCTRV:
			b1 = 'G';
			b2 = 'X';
			break;

		case SPCTRP:
			b1 = 'G';
			b2 = 'Y';
			break;

		case SPRTC:
			b2 = 'C';
			break;
		}

	m_bType[0] = b1;
	m_bType[1] = b2;

	AddByte( b1 );
	AddByte( b2 );
	if( b3 ) AddByte( b3 );
	}

// Transport Layer

BOOL CUnitronicsM90Driver::Transact(void)
{
	Send();

	return GetReply();
	}

void CUnitronicsM90Driver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CUnitronicsM90Driver::GetReply(void)
{
	UINT uState = 0;

	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uData;

	BYTE bData;

	SetTimer( TIMEOUT );

//**/	AfxTrace0("\r\n");

	while( (uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2X>", bData);

		m_bRx[uCount++] = bData;

		if( bData == CR ) return uCount > 6 ? CheckReply(uCount) : FALSE;

		if( uCount > sizeof(m_bRx) ) return FALSE;

		switch( uState ) {

			case 0:
				if( bData == USTX ) {

					uState = 1;

					uCount = 1;

					m_bRx[0] = USTX;
					}

				break;

			case 1:
				uState = bData == UACK ? 2 : 10;
				break;

			case 2:
				break;

			case 10:
				uCount = 0;
				break;
			}
		}	
 
	return FALSE;
	}

BOOL CUnitronicsM90Driver::CheckReply(UINT uCount)
{
	if(	GetValue( &m_bRx[2], 2, 16 ) != m_pCtx->m_bDrop  ||	// drop mismatch

		m_bRx[4] != m_bType[0] || m_bRx[5] != m_bType[1] ||	// type mismatch

		( m_bRx[6] == 'N' && m_bRx[7] == 'D' )			// unsupported opcode

		) {

		return FALSE;
		}

	BYTE bCheck = 0;

	for( UINT i = 2; i < uCount - 3; i++ ) {

		bCheck += m_bRx[i];
		}

	return GetValue( &m_bRx[uCount-3], 2, 16 ) == bCheck;	
	}

void CUnitronicsM90Driver::GetResponse( PDWORD pData, AREF Addr, UINT uCount )
{
	PBYTE p = &m_bRx[6];

	UINT  i;

	if( Addr.a.m_Table == SPRTC ) {

		for( i = 0; i < RTCMAX; i++, p += 2 ) {

			m_bRTC[i] = GetValue( p, 2, 10 );
			}

		for( i = 0; i < uCount; i++ ) {

			pData[i] = m_bRTC[i + Addr.a.m_Offset];
			}

		return;
		}

	BOOL fIsReal = Addr.a.m_Type == addrRealAsReal;

	UINT uSize = 4;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			for( i = 0; i < uCount; i++, p++ ) {

				pData[i] = *p == '1' ? 1 : 0;
				}

			return;

		case addrWordAsWord:	uSize = 4;	break;

		case addrLongAsLong:
		case addrRealAsReal:	uSize = 8;	break;
		}

	for( i = 0; i < uCount; i++, p += uSize ) {

		DWORD d = GetValue( p, uSize, 16 );

		if( fIsReal ) d = SwapWords(d);

		else {
			if( uSize == 4 && (d & 0x8000) ) d |= 0xFFFF0000;
			}

		pData[i] = d;
		}
	}

// Port Access

void CUnitronicsM90Driver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2X]", m_bTx[k] );

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );
	}

UINT CUnitronicsM90Driver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

BOOL CUnitronicsM90Driver::SetCount(AREF Addr, UINT * pCount)
{
	if( Addr.a.m_Table != SPRTC ) {

		*pCount = min( *pCount, RMAX );

		return TRUE;
		}

	if( Addr.a.m_Offset < RTCMAX ) {

		*pCount = min( *pCount, UINT(RTCMAX - Addr.a.m_Offset) );

		return TRUE;
		}

	return FALSE;
	}

UINT CUnitronicsM90Driver::GetHexChar(BYTE b)
{
	if( b >= '0' && b <= '9' ) return b - '0';

	if( b >= 'A' && b <= 'F' ) return b - '7';

	if( b >= 'a' && b <= 'f' ) return b - 'W';

	return 0;
	}

UINT CUnitronicsM90Driver::GetValue(PBYTE p, UINT uCount, UINT uRadix)
{
	UINT u = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		u *= uRadix;

		u += GetHexChar( p[i] );
		}

	return u;
	}

BOOL CUnitronicsM90Driver::IsReadOnly(UINT uTable)
{
	switch( uTable ) {

		case SPINP:
		case SPTMRB:
		case SPCTRB:
		case SPTMRP:
		case SPTMRV:
		case SPCTRP:
		case SPCTRV:
			return TRUE;
		}

	return FALSE;
	}

DWORD CUnitronicsM90Driver::SwapWords(DWORD dData)
{
	DWORD  Lo = LOWORD( dData ) << 16;

	return Lo + ( HIWORD(dData) );
	}

// End of File
