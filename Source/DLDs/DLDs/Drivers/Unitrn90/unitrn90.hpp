
//////////////////////////////////////////////////////////////////////////
//
// Unitronics M90 Driver
//

// Data Spaces
#define	SPOUT	 1
#define	SPBIT	 2
#define	SPINP	 3
#define	SPSYSB	 4
#define	SPCTRB	 5
#define	SPTMRB	 6
#define	SPMEMW	 7
#define	SPMEML	 8
#define	SPMEMD	 9
#define	SPMEMR	10
#define	SPSYSW	11
#define	SPSYSL	12
#define	SPSYSD	13
#define	SPTMRP	14
#define	SPTMRV	15
#define	SPCTRP	16
#define	SPCTRV	17
#define	SPRTC	18

#define	RTCMAX	 7
#define	RMAX	16

#define	USTX	'/'
#define	UACK	'A'

class CUnitronicsM90Driver : public CMasterDriver
{
	public:
		// Constructor
		CUnitronicsM90Driver(void);

		// Destructor
		~CUnitronicsM90Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			};
		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[80];
		BYTE	m_bRx[80];
		UINT	m_uPtr;
		BYTE	m_bType[2];
		UINT	m_uRcvCt;
		BYTE	m_bRTC[8];

		// Hex Lookup
		LPCTXT	m_pHex;
		
		// Frame Building
		void	AddRead(AREF Addr, UINT uCount);
		void	AddWrite(AREF Addr, PDWORD pData, UINT uCount);
		void	StartFrame(UINT uTable, BOOL fIsWrite);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddWord(UINT uData);
		void	AddHexChars(BYTE bData);
		void	AddAddrCount(UINT uAddr, UINT uCount);
		void	AddOpType(UINT uTable, BOOL fIsWrite);
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	CheckReply(UINT uCount);
		void	GetResponse(PDWORD pData, AREF Addr, UINT uCount);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		BOOL	SetCount(AREF Addr, UINT * pCount);
		UINT	GetHexChar(BYTE b);
		UINT	GetValue(PBYTE p, UINT uCount, UINT uRadix);
		UINT	GetDecValue(PBYTE p, UINT uCount);
		BOOL	IsReadOnly(UINT uTable);
		DWORD	SwapWords(DWORD dData);

	};

// End of File
