#include "intern.hpp"

#include "pcoma.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Ascii Base Driver
//

// Constructor

CPcomAMasterDriver::CPcomAMasterDriver(void)
{
	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx));

	m_pBase = NULL;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;
	}

// Destructor

CPcomAMasterDriver::~CPcomAMasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CPcomAMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 7;
	Addr.a.m_Offset = 0x1;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CPcomAMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, GetCount(Addr.a.m_Type));
	
	Begin();

	UINT uTable = Addr.a.m_Table;

	AddCommandCode(uTable, FALSE);

	if( uTable != T_RC ) {

		AddAddress(Addr.a.m_Offset);

		AddLength(uCount);
		}

	End();

	if( Transact() ) {

		FrameFromAscii(2 + 2);

		if( uTable == T_RC ) {

			GetTime(pData);

			return 1;
			}
		
		GetData(pData, Addr.a.m_Type, uCount, Addr.a.m_Table);
		
		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CPcomAMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsReadOnly(Addr.a.m_Table) ) {

		return uCount;
		}

	MakeMin(uCount, GetCount(Addr.a.m_Type));
	
	Begin();

	UINT uTable = Addr.a.m_Table;

	AddCommandCode(uTable, TRUE);

	if( uTable != T_RC ) {

		AddAddress(Addr.a.m_Offset);

		AddLength(uCount);

		AddData(pData, Addr.a.m_Type, uCount);
		}
	else {
		AddTime(pData);
		}
		

	End();

	if( Transact() ) {

		return uCount;
		}
	
	return CCODE_ERROR;
	}
 
// Implementation

void CPcomAMasterDriver::AddByte(BYTE bByte)
{
	m_bTx[m_uPtr] = bByte;

	m_uPtr++;

	m_uCheck += bByte;
	}

void CPcomAMasterDriver::AddBytes(BYTE bByte1, BYTE bByte2)
{
	AddByte(bByte1);

	AddByte(bByte2);
	}

void CPcomAMasterDriver::AddAscii(BYTE bByte)
{
	AddBytes(m_pHex[bByte / 0x10], m_pHex[bByte % 0x10]);
	}

void CPcomAMasterDriver::AddCommandCode(UINT uTable, BOOL fWrite)
{
	switch( uTable ) {

		case T_A:	fWrite ? AddByte('S') : AddByte('R');  AddByte ('A');		break;
		case T_B:	fWrite ? AddByte('S') : AddByte('R');  AddByte ('B');		break;
		case T_E:				AddByte('R');  AddByte ('E');		break;
		case T_S:	fWrite ? AddByte('S') : AddByte('G');  AddByte ('S');		break;
		case T_C:				AddByte('R');  AddByte ('M');		break;
		case T_T:				AddByte('R');  AddByte ('T');		break;
		case T_W:	fWrite ? AddByte('S') : AddByte('R');  AddByte ('W');		break;
		case T_NL:	fWrite ? AddByte('S') : AddByte('R');  AddBytes('N','L');	break; 
		case T_ND:	fWrite ? AddByte('S') : AddByte('R');  AddBytes('N','D');	break;
		case T_NF:	fWrite ? AddByte('S') : AddByte('R');  AddBytes('N','F');	break;
		case T_F:	fWrite ? AddByte('S') : AddByte('G');  AddByte ('F');		break;
	       	case T_NH:	fWrite ? AddByte('S') : AddByte('R');  AddBytes('N','H');	break;
		case T_NJ:	fWrite ? AddByte('S') : AddByte('R');  AddBytes('N','J');	break;
		case T_GP:					       AddBytes('G','P');	break;
		case T_GT:					       AddBytes('G','T');	break;
		case T_GY:					       AddBytes('G','Y');	break;
		case T_GX:					       AddBytes('G','X');	break;
		case T_RC:	fWrite ? AddByte('S') : AddByte('R');  AddByte ('C');		break;

		}

	}

void CPcomAMasterDriver::AddAddress(UINT uOffset)
{
	BYTE bHi = HIBYTE(LOWORD(uOffset));

	BYTE bLo = LOBYTE(LOWORD(uOffset));

	AddAscii(bHi);

	AddAscii(bLo);
    	}

void CPcomAMasterDriver::AddLength(UINT uCount)
{
	UINT Count = uCount & 0xFF;

	AddAscii(LOBYTE(LOWORD(Count)));
	}

void CPcomAMasterDriver::AddData(PDWORD pData, UINT uType, UINT uCount)
{
	switch( uType ) {
		
		case addrBitAsBit:	AddBits (pData, uCount);		return;
		case addrWordAsWord:	AddWords(pData, uCount);		return;
		case addrLongAsLong:	AddLongs(pData, uCount);		return;
		case addrRealAsReal:	AddReals(pData, uCount);		return;
		
		}
	}

void CPcomAMasterDriver::AddBits(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddByte(m_pHex[pData[u] & 0x1]);
		}
	}

void CPcomAMasterDriver::AddWords(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddAscii(HIBYTE(LOWORD(pData[u])));

		AddAscii(LOBYTE(LOWORD(pData[u])));
		}
	}

void CPcomAMasterDriver::AddLongs(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddAscii(HIBYTE(HIWORD(pData[u])));

		AddAscii(LOBYTE(HIWORD(pData[u])));

		AddAscii(HIBYTE(LOWORD(pData[u])));

		AddAscii(LOBYTE(LOWORD(pData[u])));
		}
	}

void CPcomAMasterDriver::AddReals(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddAscii(HIBYTE(LOWORD(pData[u])));

		AddAscii(LOBYTE(LOWORD(pData[u])));

		AddAscii(HIBYTE(HIWORD(pData[u])));

		AddAscii(LOBYTE(HIWORD(pData[u])));
		}
	}

void CPcomAMasterDriver::AddTime(PDWORD pData)
{
	DWORD t = pData[0];

	UINT uSec   = GetSec(t) % 60;

	UINT uMin   = GetMin(t) % 60;

	UINT uHours = GetHours(t) % 24;

	UINT uDay   = GetDay(t) % 7;

	UINT uMonth = GetMonth(t);

	UINT uYear  = GetYear(t);

	UINT uDays  = GetDate(t);

	AddAscii(ByteToDec(uSec));

	AddAscii(ByteToDec(uMin));

	AddAscii(ByteToDec(uHours));

	AddAscii(ByteToDec(uDay));

	AddAscii(ByteToDec(uDays));

	AddAscii(ByteToDec(uMonth));

	AddAscii(ByteToDec(uYear - 2000));
	}

UINT CPcomAMasterDriver::GetCount(UINT uType)
{
	switch( uType ) {

		case addrLongAsLong:
		case addrRealAsReal:

			return 24;

		case addrBitAsBit:

			return 64;
		}

	return 48;
	}

void CPcomAMasterDriver::GetData(PDWORD pData, UINT uType, UINT uCount, UINT uTable)
{
	switch( uType ) {
		
		case addrBitAsBit:	GetBits (pData, uCount, uTable);		return;
		case addrWordAsWord:	GetWords(pData, uCount, uTable);		return;
		case addrLongAsLong:	GetLongs(pData, uCount, uTable);		return;
		case addrRealAsReal:	GetReals(pData, uCount, uTable);		return;
		}
	}

void CPcomAMasterDriver::GetBits(PDWORD pData, UINT uCount, UINT uTable)
{
	for( UINT u = 0, i = 0; u < uCount; u++ ) {

		if( u % 2 == 0 ) {

			pData[u] = (m_bRx[i] & 0x10) >> 4;
			}
		else {
			pData[u] = (m_bRx[i] & 0x01) >> 0;

			i++;
			}
		}
	}

void CPcomAMasterDriver::GetWords(PDWORD pData, UINT uCount, UINT uTable)
{	
	for( UINT u = 0; u < uCount; u++ ) {

		WORD x = PU2(m_bRx)[u];
			
		pData[u] = LONG(SHORT(MotorToHost(x)));
		} 
	}

void CPcomAMasterDriver::GetLongs(PDWORD pData, UINT uCount, UINT uTable)
{
	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x  = PU4(m_bRx)[u];

		pData[u] = MotorToHost(x);
		}
	}

void CPcomAMasterDriver::GetReals(PDWORD pData, UINT uCount, UINT uTable)
{
	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x  = PU4(m_bRx)[u];

		WORD  l  = LOWORD(x);

		WORD  h  = HIWORD(x);

		x        = MAKELONG(h, l);

		pData[u] = MotorToHost(x);
		}
	}

void CPcomAMasterDriver::GetTime(PDWORD pData)
{
	UINT u = 0;

	UINT uSecs  = ByteToHex(m_bRx[u++]);	

	UINT uMins  = ByteToHex(m_bRx[u++]);

	UINT uHours = ByteToHex(m_bRx[u++]);

	u++;

	UINT uDayM  = ByteToHex(m_bRx[u++]);

	UINT uMonth = ByteToHex(m_bRx[u++]);

	UINT uYear  = ByteToHex(m_bRx[u++]);
	
	DWORD t = 0;

	t += Time(uHours, uMins, uSecs);

	t += Date(uYear, uMonth, uDayM);

	pData[0] = t;
	}

void CPcomAMasterDriver::Begin(void)
{
	m_uPtr = 0;

	AddByte('/');

	m_uCheck = 0;

	AddAscii(m_pBase->m_bUnit);
	}

void CPcomAMasterDriver::End(void)
{
	BYTE bCheck = m_uCheck % 256;

	AddAscii(bCheck);

	AddByte(CR);
	}

// Helpers

BOOL CPcomAMasterDriver::IsReadOnly(UINT uTable)
{
	switch( uTable ) {

		case T_E:
		case T_T:
		case T_C:
		case T_GP:	
		case T_GT:	
		case T_GY:	
		case T_GX:
		
			return TRUE;
		}

	return FALSE;
	}

void CPcomAMasterDriver::FrameFromAscii(UINT uBegin)
{
	BYTE bTemp[400];

	UINT n = 0;

	for( UINT u = uBegin + 2; u < m_uPtr - 1; u++ ) {

		bTemp[n]  = ByteFromAscii(m_bRx[u]) << 4;

		u++;
		
		bTemp[n] |= ByteFromAscii(m_bRx[u]);

		n++;
		}

	memcpy(m_bRx, bTemp, n);
	}

BYTE CPcomAMasterDriver::ByteFromAscii(BYTE bByte)
{
	if( bByte >= '0' && bByte <= '9' )

		return bByte - '0';

	return	bByte - '@' + 9;
	}

BYTE CPcomAMasterDriver::ByteToHex(BYTE bByte)
{
	BYTE bHex = bByte / 16 * 10;

	bHex += bByte % 16; 

	return bHex;
	}

BYTE CPcomAMasterDriver::ByteToDec(BYTE bByte)
{
	BYTE bDec = bByte / 10 * 16;

	bDec += bByte % 10; 

	return bDec;
	}

// Transport Layer

BOOL CPcomAMasterDriver::Transact(void)
{
	return TRUE;
	}

// End of File
