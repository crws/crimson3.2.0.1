
#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCOM ASCII Constants
//

#define T_A	1
#define T_B	2
#define T_E	3
#define T_S	4
#define T_C	5
#define T_T	6
#define T_W	7
#define T_NL	8
#define T_ND	9
#define T_NF	10
#define T_F	11
#define T_NH	12
#define T_NJ	13
#define T_GP	14
#define T_GT	15
#define T_GY	16
#define T_GX	17
#define T_RC	18

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Ascii Master Base Driver
//

class CPcomAMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CPcomAMasterDriver(void);

		// Destructor
		~CPcomAMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Data
		struct CBaseCtx
		{
			BYTE	m_bUnit;
			};

	protected:

		CBaseCtx * m_pBase;
		BYTE	   m_bTx[400];
		BYTE	   m_bRx[400];
		UINT	   m_uPtr;
		UINT	   m_uCheck;
		LPCTXT	   m_pHex;

					
		// Implementation

		void AddByte(BYTE bByte);
		void AddBytes(BYTE bByte1, BYTE bByte2);
		void AddAscii(BYTE bByte);
		void AddCommandCode(UINT uTable, BOOL fWrite);
		void AddAddress(UINT uOffset);
		void AddLength(UINT uCount);
		void AddData(PDWORD pData, UINT uType, UINT uCount);
		void AddBits(PDWORD pData, UINT uCount);
		void AddWords(PDWORD pData, UINT uCount);
		void AddLongs(PDWORD pData, UINT uCount);
		void AddReals(PDWORD pData, UINT uCount);
		void AddTime(PDWORD pData);
	virtual	UINT GetCount(UINT uType);
		void GetData(PDWORD pData, UINT uType, UINT uCount, UINT uTable);
		void GetBits(PDWORD pData, UINT uCount, UINT uTable);
		void GetWords(PDWORD pData, UINT uCount, UINT uTable);
		void GetLongs(PDWORD pData, UINT uCount, UINT uTable);
		void GetReals(PDWORD pData, UINT uCount, UINT uTable);
		void GetTime(PDWORD pData);
		void Begin(void);
		void End(void);
		
		
		// Helpers

		BOOL IsReadOnly(UINT uTable);
		void FrameFromAscii(UINT uBegin);
		BYTE ByteFromAscii(BYTE bByte);
		BYTE ByteToHex(BYTE bByte);
		BYTE ByteToDec(BYTE bByte);
	
		
		// Transport Layer

		virtual BOOL Transact(void);
	
	};

// End of File
