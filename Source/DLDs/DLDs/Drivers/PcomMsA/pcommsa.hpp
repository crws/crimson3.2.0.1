#include "pcoma.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Ascii Base Driver
//
//

class CPcomMsADriver : public CPcomAMasterDriver
{
	public:
		// Constructor
		CPcomMsADriver(void);

		// Destructor
		~CPcomMsADriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Device Context
		struct CContext : CPcomAMasterDriver::CBaseCtx
		{
		
			};

	protected:

		// Data Members
		CContext * m_pCtx;
		

		// Transport
		virtual BOOL Transact(void);
		     
		BOOL CheckFrame(void);
		BOOL Send(void);
		BOOL RecvFrame(void);

		UINT GetCount(UINT uType);

				
	};

// End of File

