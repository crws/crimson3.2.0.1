#include "intern.hpp"

#include "omg9sp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omron G9SP-Series Serial Master Driver
//

// Instantiator

INSTANTIATE(COmronFinsG9spMasterDriver);

// Constructor

COmronFinsG9spMasterDriver::COmronFinsG9spMasterDriver(void)
{
	m_Ident = DRIVER_ID;

	memset(m_bBuff, 0, sizeof(m_bBuff));

	m_uTime = 0;

	m_uPoll = 0;

	m_uData = 0;

	m_fConn = FALSE;
	}

// Configuration

void MCALL COmronFinsG9spMasterDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uPoll      = ToTicks(GetWord(pData));
		}

	}

// Management

void MCALL COmronFinsG9spMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL COmronFinsG9spMasterDriver::Open(void)
{
	}

	


// Entry Points

CCODE MCALL COmronFinsG9spMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 2;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrBitAsBit;
	Addr.a.m_Extra  = 0;

	Start();

	PutHeader();

	AddData();

	AddCheck();

	End();

	if( Transact() ) {

		m_fConn = TRUE;
			
		return CCODE_SUCCESS;
		}

	m_fConn = FALSE;

	return CCODE_ERROR;
	}

CCODE MCALL COmronFinsG9spMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsWriteOnly(Addr.a.m_Table, Addr.a.m_Offset) ) {
		
		return uCount;
		}

	if( !IsTimedOut() ) {

		return GetData(Addr, pData, uCount);
		} 
	
	Start();
		
	PutHeader();

	AddData();

	AddCheck();

	End();

	if( Transact() ) {

		return GetData(Addr, pData, uCount);
		}

	return CCODE_ERROR;
	}

CCODE MCALL COmronFinsG9spMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsWriteOnly(Addr.a.m_Table, Addr.a.m_Offset) ) {

		for( UINT u = 0; u < uCount; u++ ) {

			UINT uMask = (0x1 << (Addr.a.m_Offset + u));

			if( pData[u] & 0x1 ) {

				m_uData |= uMask;
				}
			else{
				m_uData &= ~uMask;
				}
			}

		if( COMMS_SUCCESS(Ping()) ) {
		
			return uCount;
			}
		}
	
	return uCount;
	}

void MCALL COmronFinsG9spMasterDriver::Service(void)
{
	if( m_fConn ) {
	
		if( IsTimedOut() ) {

			Ping();
			}
		}
	}

// Transport Layer

BOOL COmronFinsG9spMasterDriver::Transact(void)
{
	if( IsTimedOut() ) {
	
		if( Send() ) {

			if( RecvFrame() ) {

				if( CheckFrame() ) {

					Critical(TRUE);

					memcpy(m_bBuff, m_bRx + 7, 188);

					Critical(FALSE);

					m_uTime = GetTickCount();

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	return TRUE;	
	}

BOOL COmronFinsG9spMasterDriver::Send(void)
{
	m_pData->ClearRx();
	
	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL COmronFinsG9spMasterDriver::RecvFrame(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_uPtr = 0;

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		m_bRx[m_uPtr] = uData;

		m_uPtr++;

		if( m_uPtr >= 199 ) {

			return ( m_bRx[197] == 0x2A && m_bRx[198] == 0x0D );
			}
		}

	return FALSE;
	}

BOOL COmronFinsG9spMasterDriver::CheckFrame(void)
{
	m_uCheck = 0;

	for( UINT i = 0; i < 195; i++ ) {

		m_uCheck += m_bRx[i];
		}

	if( HIBYTE(m_uCheck) != m_bRx[195] || LOBYTE(m_uCheck) != m_bRx[196] ) {

		return FALSE;
		}
	
	
	BYTE bBytes [] = {	0x40, 
				0x00, 
				0x00, 
				0xC3, 
				0x00, 
				0x00, 
				0xCB };

	for( UINT u = 0; u < elements(bBytes); u++ ) {

		if( m_bRx[u] != bBytes[u] ) {

			return FALSE;
			}
		}
	
	return TRUE;
	}

// Implementation

void COmronFinsG9spMasterDriver::Start(void)
{
	m_uPtr = 0;

	m_uCheck = 0;
	}

void COmronFinsG9spMasterDriver::AddByte(BYTE bByte)
{
	m_bTx[m_uPtr] = bByte;

	m_uPtr++;

	m_uCheck += bByte;
	}

void COmronFinsG9spMasterDriver::AddData(void)
{
	WORD wHi = HIWORD(m_uData);

	WORD wLo = LOWORD(m_uData);

	AddByte(LOBYTE(wLo));

	AddByte(HIBYTE(wLo));

	AddByte(LOBYTE(wHi));

	AddByte(HIBYTE(wHi));
	
	AddByte(0);			// Echo

	AddByte(0);			// Reserved
	}

void COmronFinsG9spMasterDriver::AddCheck(void)
{
	UINT Check = m_uCheck;

	AddByte(HIBYTE(Check));

	AddByte(LOBYTE(Check));
	}

void COmronFinsG9spMasterDriver::End(void)
{
	AddByte(0x2A);

	AddByte(0x0D);
	}

UINT COmronFinsG9spMasterDriver::GetData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT At = Addr.a.m_Table;

	UINT Count = uCount;

	UINT Index = Addr.a.m_Offset;

	if( Addr.a.m_Table == 24 || Addr.a.m_Table == 48 ) {

		GetNibblesAt(pData, At, Index, Count);

		return Count;
		}
	
	if( Addr.a.m_Table == addrNamed ) {

		At = Addr.a.m_Offset;

		Count = 1;

		Index = 0;
		}

	if( Addr.a.m_Table <= 2 ) {

		At = 0;
		}

	switch( Addr.a.m_Type ) {

		case addrLongAsLong:	GetLongsAt(pData, At, Index, Count);	break;
		case addrWordAsWord:	GetWordsAt(pData, At, Index, Count);	break;
		case addrByteAsByte:	GetBytesAt(pData, At, Index, Count);	break;
		case addrBitAsBit:	GetBitsAt (pData, At, Index, Count);	break;
		}

	return Count;
       	}

void COmronFinsG9spMasterDriver::GetLongsAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount)
{	
	for( UINT u = 0, i = 0; u < uCount + uIndex; u++ ) {

		if( u >= uIndex ) {

			DWORD x  = PU4(m_bBuff + uAt)[u];
			
			pData[i] = IntelToHost(x);

			i++;
			}
		}
	}

void COmronFinsG9spMasterDriver::GetWordsAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount)
{
	for( UINT u = 0, i = 0; u < uCount + uIndex; u++ ) {

		if( u >= uIndex ) {
	
			WORD x  = PU2(m_bBuff + uAt)[u];
			
			pData[i] = LONG(SHORT(IntelToHost(x)));

			i++;
			}
		}
	}

void COmronFinsG9spMasterDriver::GetBytesAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount)
{
	for( UINT u = 0, i = 0; u < uCount + uIndex; u++ ) {

		if( u >= uIndex ) {
				
			pData[i] = PBYTE(m_bBuff + uAt)[u];

			i++;
			}
		}
	}

void COmronFinsG9spMasterDriver::GetNibblesAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount)
{
	BYTE bNibble = 0;
	
	for( UINT u = 0, i = 0, b = 0; u < uCount + uIndex; u++ ) {

		if( u % 2 == 0 ) {

			bNibble = PBYTE(m_bBuff + uAt)[b];

			b++;
			}

		if( u >= uIndex ) {
				
			pData[i] = ((bNibble >> ((u % 2) * 4)) & 0xF);

			i++;
			}
		}
	}

void COmronFinsG9spMasterDriver::GetBitsAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount)
{
	BYTE bBits = 0;
	
	for( UINT u = 0, i = 0, b = 0; u < uCount + uIndex; u++ ) {

		if( u % 8 == 0 ) {

			bBits = PBYTE(m_bBuff + uAt)[b];

			b++;
			}

		if( u >= uIndex ) {
	
			pData[i] = (bBits >> (u % 8)) & 0x1;

			i++;
			}
		}
	}

void COmronFinsG9spMasterDriver::PutHeader(void)
{
	AddByte(0x40);

	AddByte(0x00);

	AddByte(0x00);

	AddByte(0x0F);

	AddByte(0x4B);

	AddByte(0x03);

	AddByte(0x4D);

	AddByte(0x00);

	AddByte(0x01);
	}

BOOL COmronFinsG9spMasterDriver::IsTimedOut(void)
{
	return int(GetTickCount() - m_uTime - m_uPoll) >= 0;
	}

BOOL COmronFinsG9spMasterDriver::IsWriteOnly(UINT uTable, UINT uOffset)
{
	if( uTable == 1 ) {

		return TRUE;
		}

	return FALSE;
	}

// End of File
