#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "bmikels.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Serial Master Driver
//

// Instantiator

#if DRIVER_ID == 0x409B

INSTANTIATE(CBMikeLSMasterDriver);

#endif

// Constructor

CBMikeLSMasterDriver::CBMikeLSMasterDriver(void)
{
	m_Ident   = DRIVER_ID;

	m_fTB	  = FALSE;

	m_fTF	  = FALSE;

	m_fKickTF = FALSE;
	}

// Configuration

void MCALL CBMikeLSMasterDriver::Load(LPCBYTE pData)
{
	}

void MCALL CBMikeLSMasterDriver::CheckConfig(CSerialConfig &Config)
{
	m_bMask = 0xFF;

	/*if( Config.m_uDataBits == 7 ) {

		m_bMask = 0x7F;
		}*/
	
	Make422(Config, TRUE);
	}

// Management

void MCALL CBMikeLSMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CBMikeLSMasterDriver::Open(void)
{
	}


// Device

CCODE MCALL CBMikeLSMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pBase = (CBaseCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pBase = new CBaseCtx;

			m_pBase->m_Scan	    = ToTicks(GetWord(pData));
			m_pBase->m_Timeout  = GetWord(pData);
			m_pBase->m_bAuto    = GetByte(pData);

			memset(m_pBase->m_Time,   0, elements(m_pBase->m_Time)   * sizeof(DWORD));
			memset(m_pBase->m_Record, 0, elements(m_pBase->m_Record) * sizeof(DWORD));
			memset(m_pBase->m_Info,   0, elements(m_pBase->m_Info)   * sizeof(DWORD));
			memset(m_pBase->m_Comms,  0, elements(m_pBase->m_Comms)  * sizeof(DWORD));

			m_pBase->m_fError = TRUE;

			m_pBase->m_bModel = 0;

			memset(m_pBase->m_ARB, 0, elements(m_pBase->m_ARB) * sizeof(DWORD));

			m_pBase->m_fInit  = m_pBase->m_bAuto ? FALSE : TRUE;

			pDevice->SetContext(m_pBase);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CBMikeLSMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pBase;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CBMikeLSMasterDriver::Ping(void)
{
	if( !m_pBase->m_fInit ) {

		m_pBase->m_fInit = TRUE;

		if( LSAutoBaudSeq() ) {

			return CCODE_SUCCESS;
			}
		}

	if( FindModel() ) {

		return CCODE_SUCCESS;
		}

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = 193;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	DWORD Data[1];

	return Read(Addr, Data, 1);
	}

CCODE MCALL CBMikeLSMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uSpace = GetItemNumber(Addr);

	if( IsWriteOnly(uSpace) ) {

		return uCount;
		}

	MakeMin(uCount, 16);

	if( IsArbRes(uSpace) ) {

		UINT uBytes = uCount * sizeof(DWORD);

		memcpy(pData, m_pBase->m_ARB, min(uBytes, 64));

		return uCount;
		}

	if( !IsModelValid(uSpace) ) {
		
		return CCODE_ERROR | CCODE_NO_DATA;
		}

	BOOL fReal = IsRealTime(uSpace);

	if( !fReal && IsTimedOut(uSpace) ) {

		if( Read(uSpace) ) {

			CacheData(Addr, uCount);			
			}
		else {	
			return CCODE_ERROR;
			}
		}

	if( IsValid(uSpace) ) {

		if( fReal && m_pBase->m_fError ) {
			
			return CCODE_ERROR;
			}

		GetData(Addr, pData, uCount);
		
		return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CBMikeLSMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uSpace = GetItemNumber(Addr);

	if( IsRealTime(uSpace) ) {

		return uCount;
		}

	MakeMin(uCount, 16);

	if( IsArbCmd(uSpace) ) {

		if( ArbCmd(uSpace, pData) ) {

			return uCount;
			}

		return CCODE_ERROR;
		}

	if( !IsModelValid(uSpace) ) {
		
		return CCODE_ERROR | CCODE_NO_DATA;
		}

	if( Write(uSpace, pData, uCount, Addr.a.m_Type) ) {

		if( IsWriteOnly(uSpace) ) {

			return uCount;
			}

		m_pBase->m_Time[uSpace] = 0;

		PDWORD pRead = PDWORD(alloca(uCount * 4));

		if( COMMS_SUCCESS(Read(Addr, pRead, uCount)) ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

void MCALL CBMikeLSMasterDriver::Service(void)
{	
	// Get Real Time Data 

	if( m_fTB ) {	// TB Binary Output Mode

		BOOL fSuccess = FALSE;

		Begin(spaceRTL);

		End(spaceRTL);

		if( Send() ) {

			// Recv real time data and cache

			UINT uTimer = 0;

			UINT uData = 0;

			UINT uSeq  = 0;

			m_uPtr = 0;

			SetTimer(500);
			
			while( (uTimer = GetTimer()) ) {

				if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

					continue;
					}

				if( uSeq == 5 ) {

					if( m_uPtr == 0 && uData == 0xFF ) {

						break;
						}

					m_bRx[m_uPtr] = uData;

					m_uPtr++;

					if( m_uPtr == 11 ) {

						if( ChecksumCheck() ) {

							// Cache data

							DWORD x =  PU4(m_bRx + 1)[0];

							m_pBase->m_Record[spaceRTL] = MotorToHost(x);

							x = PU4(m_bRx + 6)[0];

							m_pBase->m_Record[spaceRTV] = MotorToHost(x);

							m_pBase->m_Record[spaceRTQF] = m_bRx[0];

							m_pBase->m_Record[spaceRTS] = m_bRx[5];

							EndRealTime();
	
							fSuccess = TRUE;

							break;
							}
						}	
					}

				else if( uData == m_bMask ) {

					uSeq++;
					}
				
				if( m_uPtr >= sizeof(m_bRx) ) {

					break;
					}
				}
			}

		if( !fSuccess ) {

			EndRealTime();	
			}

		m_pBase->m_fError = !fSuccess;
		}
	}

// Transport Layer

BOOL CBMikeLSMasterDriver::Transact(UINT uSpace)
{
	if( Send() ) {

		if( RecvFrame() ) {

			if( IsExempt(uSpace) || CheckFrame() ) {

				return TRUE;
				}
			}
		}
	
	return FALSE;
	}

BOOL CBMikeLSMasterDriver::Send(void)
{
	m_pData->ClearRx();
	
	m_pData->Write(m_bTx, m_uPtr, FOREVER);

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_uPtr; u++ ) {

		AfxTrace("%c", m_bTx[u]);
		}

*/	return TRUE;
	}

BOOL CBMikeLSMasterDriver::RecvFrame(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_uPtr = 0;

//	AfxTrace("\nRx : ");

	SetTimer(m_pBase->m_Timeout);

	while( (uTimer = GetTimer()) ) {

		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

//		AfxTrace("%c", uData);

		m_bRx[m_uPtr] = uData;

		m_uPtr++;

		if( uData == CR ) {

			return TRUE;
			}	
		}

	return FALSE;
	}

void CBMikeLSMasterDriver::EndRealTime(void)
{
	m_uPtr = 0;

	AddByte('.');

	AddByte(CR);

	Send();

	if( m_pData ) {

		Sleep(20);	

		while( m_pData->Read(0) < NOTHING );
		}
	}

BOOL CBMikeLSMasterDriver::CheckFrame(void)
{
	return !memcmp(PBYTE(m_bTx), PBYTE(m_bRx), m_uCheck);
	}

BOOL CBMikeLSMasterDriver::ChecksumCheck(void)
{
	BYTE bCheck = 0;

	for( UINT u = 0; u < 5; u++ ) {

		bCheck +=  0xFF;
		}
	
	for( UINT i = 0; i < m_uPtr - 1; i++ ) {

		bCheck += m_bRx[i];
		}

	return bCheck == m_bRx[m_uPtr - 1];
	}

// Implementation

BOOL CBMikeLSMasterDriver::Read(UINT uSpace)
{
	if( Begin(uSpace) ) {

		AddExtra(uSpace, FALSE);

		End(uSpace);	

		return Transact(uSpace);
		}

	return FALSE;
	}

BOOL CBMikeLSMasterDriver::Write(UINT uSpace, PDWORD pData, UINT uCount, UINT uType)
{
	if( Begin(uSpace) ) {

		AddExtra(uSpace, TRUE);

		AddArg(uSpace, pData, uCount, uType);

		End(uSpace);

		Send();

		if( m_pData ) {

			while( m_pData->Read(0) < NOTHING );

			Sleep(20);

			if( m_pBase->m_bAuto ) {

				LSAutoBaudSeq();
				}
			}		

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CBMikeLSMasterDriver::Begin(UINT uSpace)
{
	m_uPtr = 0;

	switch( uSpace ) {

		case spaceRTL:
		case spaceRTV:
		case spaceRTS:

			AddByte('T');
			AddByte('B');
			break;

		case spaceFL:
		
			AddByte('T');
			AddByte('F');
			break;
	
		case space422BRS:

			AddByte('@');
			break;

		case space232BRS:

			AddByte('J'); 
			break;

		case spaceQFWThr:

			AddByte('&');
			break;

		case spaceQFWTo:

			AddByte('<');
			break;

		case spaceDIS:

			AddByte('A');
			break;

		case spaceHVIA:

			AddByte('B');
			break;

		case spaceVHT:

			AddByte('$');
			AddByte('T');
			break;

		case spaceCT:

			AddByte('C');
			break;
		
		case spaceAFS:

			AddByte('D');
			break;

		case spaceMPIM:

			AddByte('S');
			break;

		case spaceMPDT:
			
			AddByte('F');
			break;

		case spaceMPVT:

			AddByte('G');
			break;

		case spaceMPT:

			AddByte('M');
			break;

		case spaceVALM:

			AddByte('T');
			AddByte('P');
			break;

		case spaceHMCV:

			AddByte('H');
			break;

		case spaceCTmp:
		case spaceMTmp:
		
			AddByte('I');
			break;

		case spaceAT:

			AddByte('K');
			break;

		case spaceUUR:
			
			AddByte('O');
			break;

		case spaceRTQF:

			AddByte('Q');
			break;

		case spaceRILC:

			AddByte('R');
			break;

		case spaceLRIA:

			AddByte('%');
			break;

		case spaceLRV:

			AddByte('$');
			AddByte('R');
			break;

		case spaceMU:

			AddByte('U');
			break;

		case spaceWT:

			AddByte('W');
			break;

		case spaceMinVL:

			AddByte('$');
			AddByte('N');
			break;

		case spaceMaxVL:

			AddByte('$');
			AddByte('X');
			break;

		case spaceMFL:

			AddByte('X');
			break;

		case spaceADOF:

			AddByte('#');
			break;

		case spaceIPL:

			if( m_pBase->m_bModel == LS4K ) {

				AddByte('E');
				break;
				}

			AddByte('Y');
			break;

		case spaceIPE:

			if( m_pBase->m_bModel == LS4K ) {

				AddByte('L');
				break;
				}

			AddByte('!');
			break;

		case spaceLTA:

			AddByte('$');
			AddByte('A');
			break;

		case spaceLTB:

			AddByte('$');
			AddByte('B');
			break;
			
		case spaceHSPC:

			AddByte('$');
			AddByte('H');
			break;

		case spaceHSPR:

			AddByte('L');
			break;

		case spaceLSPC:

			AddByte('$');
			AddByte('L');
			break;

		case spaceLSPR:

			AddByte('P');
			break;

		case spaceFVS:

			AddByte('Z');
			break;

		case spaceCLC:

			AddByte('$');
			AddByte('C');
			break;

		case spaceSL:

			AddByte('*');
			AddByte('L');
			AddByte('O');
			AddByte('C');
			AddByte('K');
			AddByte('E');
			AddByte('D');
			break;

		case spaceSLE:

			AddByte('*');
			AddByte('L');
			AddByte('O');
			AddByte('C');
			AddByte('K');
			AddByte('E');
			AddByte('N');
			break;

		case spaceGSN:
			
			AddByte('*');
			AddByte('G');
			AddByte('I');
			AddByte('N');
			AddByte('F');
			AddByte('O');
			break;

		case spaceEE:

			AddByte('*');
			AddByte('E');
			AddByte('T');
			AddByte('H');
			AddByte('E');
			AddByte('R');
			AddByte('N');
			AddByte('E');
			AddByte('T');
			break;

		case spaceELS:

			AddByte('*');
			AddByte('L');
			AddByte('I');
			AddByte('N');
			AddByte('K');
			break;

		case spaceEHI:

			AddByte('*');
			AddByte('M');
			AddByte('A');
			AddByte('C');
			AddByte('I');
			AddByte('D');
			break;

		case spaceDHCP:

			AddByte('*');
			AddByte('D');
			AddByte('H');
			AddByte('C');
			AddByte('P');
			break;

		case spaceHN:

			AddByte('*');
			AddByte('N');
			AddByte('A');
			AddByte('M');
			AddByte('E');
			break;

		case spaceIA:

			AddByte('*');
			AddByte('I');
			AddByte('P');
			AddByte('A');
			AddByte('D');
			AddByte('D');
			AddByte('R');
			break;

		case spaceIDG:

			AddByte('*');
			AddByte('I');
			AddByte('P');
			AddByte('D');
			AddByte('E');
			AddByte('F');
			AddByte('G');
			AddByte('W');
			break;

		case spaceINM:

			AddByte('*');
			AddByte('I');
			AddByte('P');
			AddByte('N');
			AddByte('E');
			AddByte('T');
			AddByte('M');
			AddByte('S');
			AddByte('K');
			break;

		case spaceUDPDP:

			AddByte('*');
			AddByte('D');
			AddByte('A');
			AddByte('T');
			AddByte('A');
			AddByte('P');
			AddByte('O');
			AddByte('R');
			AddByte('T');
			break;

		case space232POM:

			AddByte('*');
			AddByte('A');
			AddByte('U');
			AddByte('T');
			AddByte('O');
			AddByte('2');
			AddByte('3');
			AddByte('2');
			break;

		case space422POM:

			AddByte('*');
			AddByte('A');
			AddByte('U');
			AddByte('T');
			AddByte('O');
			AddByte('4');
			AddByte('2');
			AddByte('2');
			break;

		case spaceUDPPOM:

			AddByte('*');
			AddByte('A');
			AddByte('U');
			AddByte('T');
			AddByte('O');
			AddByte('U');
			AddByte('D');
			AddByte('P');
			break;

		case spaceUDPDIA:

			AddByte('*');
			AddByte('A');
			AddByte('U');
			AddByte('T');
			AddByte('O');
			AddByte('A');
			AddByte('D');
			AddByte('D');
			AddByte('R');
			break;

		case spaceUDPDIP:

			AddByte('*');
			AddByte('A');
			AddByte('U');
			AddByte('T');
			AddByte('O');
			AddByte('P');
			AddByte('O');
			AddByte('R');
			AddByte('T');
			break;

		case  spaceMVer:

			AddByte('*');
			AddByte('M');
			AddByte('V');
			AddByte('E');
			AddByte('R');
			break;

		case spaceHSPCLR:

			AddByte('$');
			AddByte('D');
			break;

		case spaceLSPCLR:

			AddByte('$');
			AddByte('E');
			break;

		case spaceDLCM:

			AddByte('T');
			AddByte('O');
			break;

		case spaceSTS:

			AddByte('T');
			AddByte('S');
			break;

		case spaceSF:

			AddByte('T');
			AddByte('U');
			break;

		case spaceRZH:

			AddByte('$');
			AddByte('Z');
			break;

		case spaceAZS:

			AddByte('N');
			break;
		}

	m_uCheck = m_uPtr > 2 ? m_uPtr : 0;
				
	return TRUE;
	}

void CBMikeLSMasterDriver::AddExtra(UINT uSpace, BOOL fWrite)
{
	switch( uSpace ) {

		case spaceSL:
		case spaceSLE:
		case spaceGSN:
		case spaceEE:
		case spaceELS:
		case spaceEHI:
		case spaceDHCP:
		case spaceHN:
		case spaceIA:
		case spaceIDG:
		case spaceINM:
		case spaceUDPDP:
		case space232POM:
		case space422POM:
		case spaceUDPPOM:
		case spaceUDPDIA:
		case spaceUDPDIP:
		case spaceMVer:
			
			fWrite ? AddByte('=') : AddByte('?');
			break;

		}
	}

void CBMikeLSMasterDriver::AddArg(UINT uSpace, PDWORD pData, UINT uCount, UINT uType)
{
	switch( uSpace ) {

		case spaceRILC:		

			return;

		case space232POM:
		case space422POM:
		case spaceUDPPOM:
		case spaceIA:
		case spaceIDG:
		case spaceINM:
		case spaceHN:
		case spaceUDPDIA:

			AddString(pData, uCount);
			return;

		case spaceIPL:

			if( m_pBase->m_bModel == LS4K ) {

				// The LS4K couples the Index Pulse Length with the Speed Range Enables.
				// The Speed Range enables (low byte) should always be set to 31.

				pData[0] = (pData[0] << 8) | 31;

				break;
				}
		}

	AddNumber(pData[0], uType == addrRealAsReal);
	}

void CBMikeLSMasterDriver::End(UINT uSpace)
{
	AddByte(CR);
	}

void CBMikeLSMasterDriver::AddByte(BYTE bByte)
{
	m_bTx[m_uPtr++] = bByte;
	}

void CBMikeLSMasterDriver::AddNumber(DWORD dwValue, BOOL fReal)
{
	char Data[16];

	memset(Data, 0, sizeof(Data));

	if( fReal ) {

		SPrintf(Data, "%f", PassFloat(dwValue)); 
		}
	else {	
		SPrintf(Data, "%d", dwValue);
		}

	AddText(PCTXT(Data));
	}

BOOL CBMikeLSMasterDriver::AddText(PCTXT pTxt)
{
	BOOL fAdd = FALSE;

	for( UINT u = 0; u < 64; u++ ) {

		char c = pTxt[u];

		if( c && c != 0x20 ) {

			AddByte(c);

			fAdd = TRUE;
			
			continue;
			}
		break;
		}

	return fAdd;
	}

void CBMikeLSMasterDriver::AddString(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x = pData[u];

		for( UINT i = 0; i < 4; i++ ) {

			char c = 0x20;

			switch( i ) {

				case 0:	c = HIBYTE(HIWORD(x));	break;
				case 1: c = LOBYTE(HIWORD(x));	break;
				case 2: c = HIBYTE(LOWORD(x));	break;
				case 3: c = LOBYTE(LOWORD(x));	break;
				}

			if( c && c != 0x20 ) {

				AddByte(c);

				continue;
				}
			break;
			}
		}
	}

DWORD CBMikeLSMasterDriver::GetReal(UINT uSpace)
{
	float dwFloat = 0.0;

	BOOL fNeg = FALSE;

	BOOL fDP  = FALSE;

	UINT uDiv = 10;

	for( UINT u = 0; m_bRx[u] != CR && u < sizeof(m_bRx); u++ ) {

		if( m_bRx[u] == '-' ) {

			fNeg = TRUE;

			continue;
			}

		if( m_bRx[u] == '.' ) {

			fDP = TRUE;

			continue;
			}

		if( m_bRx[u] == ',' ) {		// Special Case

			if( uSpace == spaceFL ) {

				break;
				}

			continue;
			}

		BYTE bByte = m_bRx[u] - 0x30;

		if( bByte <= 9 ) {

			if( !fDP ) {

				dwFloat *= 10;

				dwFloat += bByte;
				}
			else {
				float Add = float(bByte) / float(uDiv);
				
				dwFloat += Add;

				uDiv *= 10;
				}
	   		}
		}

	if( fNeg ) {

		dwFloat = -dwFloat;
		}

	return R2I(dwFloat);
	}

DWORD CBMikeLSMasterDriver::GetDec(UINT uSpace)
{
	UINT uIndex = 0;

	BOOL fNeg = FALSE;

	DWORD dwData = 0;

	while( m_bRx[uIndex] != CR && uIndex < sizeof(m_bRx) ) {

		if( uIndex >= m_uPtr ) {

			break;
			}

		if( m_bRx[uIndex] == '+' ) {

			uIndex++;

			continue;
			}

		if( m_bRx[uIndex] == '-' ) {

			fNeg = TRUE;

			uIndex++;

			continue;
			}

		if( m_bRx[uIndex] == '=' ) {

			dwData = 0;
			
			uIndex++;

			continue;
			}

		if( m_bRx[uIndex] == ',' ) {	// Special Case

			if( uSpace == spaceCTmp ) {

				break;
				}

			if( uSpace == spaceMTmp || uSpace == spaceAQF ) {

				dwData = 0;

				uIndex++;

				continue;
				}
			}

		BYTE bByte = FromAscii(m_bRx[uIndex]);

		if( bByte < 10 ) {

			dwData = dwData * 10;

			dwData += FromAscii(m_bRx[uIndex]);
			}

		uIndex++;
		}

	if( uSpace == spaceIPL ) {		// Special Case

		if( m_pBase->m_bModel == LS4K ) {

			// The LS4K couples the Index Pulse Length with the Speed Range Enables.
			// The Speed Range enables (low byte) should always be set to 31.
			
			dwData = dwData >> 8;
			}
		}

	return fNeg ? -dwData : dwData;
	}

void CBMikeLSMasterDriver::GetData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uSpace = Addr.a.m_Offset;

	if( Addr.a.m_Table == 4 ) {		// Info 

		memcpy(pData, m_pBase->m_Info + uSpace, uCount * sizeof(DWORD));

		return;
		}

	if( Addr.a.m_Table == 5 ) {		// Comms 

		memcpy(pData, m_pBase->m_Comms + uSpace, uCount * sizeof(DWORD));
	
		return;
		}
	
	pData[0] = m_pBase->m_Record[uSpace];
	}
		
void CBMikeLSMasterDriver::CacheData(AREF Addr, UINT uCount)
{
	UINT uSpace = Addr.a.m_Offset;

	if( Addr.a.m_Table != addrNamed ) {

		UINT uPos = 0;

		for( UINT u = 0; u < m_uPtr; u++ ) {

			if( m_bRx[u] == '=' ) {

				uPos = u + 1;

				break;
				}
			
			if( m_bRx[u] == CR ) {

				break;
				}
			}

		m_pBase->m_Time[(Addr.a.m_Table - 1) * 64 + uSpace / 16] = GetTickCount();

		if( Addr.a.m_Table == 4 ) {		// Info 

			CacheString(m_pBase->m_Info, uSpace, uPos);

			return;
			}

		if( Addr.a.m_Table == 5 ) {		// Comms 

			CacheString(m_pBase->m_Comms, uSpace, uPos);

			return;
			}
		}

	m_pBase->m_Time[uSpace] = GetTickCount();

	if( Addr.a.m_Type == addrRealAsReal ) {

		m_pBase->m_Record[uSpace] = GetReal(uSpace); 
		}

	else {
		m_pBase->m_Record[uSpace] = GetDec(uSpace);
		}
	}

void CBMikeLSMasterDriver::CacheString(PDWORD pData, UINT uSpace, UINT uPos)
{
	memset(pData + uSpace, '\0', 16 * sizeof(DWORD));

	for( UINT u = 0, i = 0; u < m_uPtr - uPos; u++ ) {

		switch( u % 4 ) {

			case 0:		pData[uSpace + i]  = 0;
					pData[uSpace + i] |= ((m_bRx[uPos + u]) << 24);		break;

			case 1:		pData[uSpace + i] |= ((m_bRx[uPos + u]) << 16);		break;
			case 2:		pData[uSpace + i] |= ((m_bRx[uPos + u]) <<  8);		break;
			case 3:		pData[uSpace + i] |= ((m_bRx[uPos + u]) <<  0);	i++;	break;
			}
		}
	}

BOOL CBMikeLSMasterDriver::FindModel(void)
{
	if( Read(spaceFVS) ) {

		if( !memcmp(m_bRx, PBYTE("LS9KV"), 5) ) {

			m_pBase->m_bModel = LS9K;
			}

		else if( !memcmp(m_bRx, PBYTE("LS8KV"), 5) ) {

			m_pBase->m_bModel = LS8K;
			}

		else if( !memcmp(m_bRx, PBYTE("LS4KV"), 5) ) {

			m_pBase->m_bModel = LS4K;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL  CBMikeLSMasterDriver::ArbCmd(UINT uSpace, PDWORD pData)
{
	Begin(uSpace);

	m_uPtr = 0;
	
	PCTXT pText = PCTXT(pData);

	if( pText && AddText(pText) ) {

		AddByte(CR);
	
		if( Transact(uSpace) ) {

			CacheString(m_pBase->m_ARB, 0, 0);

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL  CBMikeLSMasterDriver::LSAutoBaudSeq(void)
{
	m_uPtr = 0;

	UINT uTime = GetTickCount();

	UINT uWind = ToTicks(20000);

	while( int(GetTickCount() - uTime - uWind) < 0 ) {

		m_uPtr = 0;

		AddByte(CR);

		AddByte(CR);

		AddByte(CR);

		if( Send() ) {

			if( m_pData ) {

				while( m_pData->Read(0) < NOTHING );

				Sleep(20);
				}

			if( FindModel() ) {

				return TRUE;
				}
			}
		}
	
	return FALSE;
	}

// Helpers

BOOL CBMikeLSMasterDriver::IsWriteOnly(UINT uSpace)
{
	switch( uSpace ) {

		case spaceRILC:
		case spaceARB:
		
			return TRUE;
		}

	return FALSE;
	}

BOOL CBMikeLSMasterDriver::IsArbCmd(UINT uSpace)
{
	return uSpace == spaceARB;
	}

BOOL CBMikeLSMasterDriver::IsArbRes(UINT uSpace)
{
	return uSpace == spaceARBRES;
	}

BOOL CBMikeLSMasterDriver::IsRealTime(UINT uSpace)
{	
	switch( uSpace ) {

		case spaceRTL:
		case spaceRTV:
		case spaceRTS:
			m_fTB = TRUE;

			
			return  m_fTB;

		case spaceRTQF:
			return m_fTB;

		case spaceFL:
			m_fTF = FALSE;
			
			return  m_fTF;
			

		case spaceAQF:
			return  m_fTF;
		}

	return FALSE;
	}

BOOL CBMikeLSMasterDriver::IsTimedOut(UINT uSpace)
{
	return int(GetTickCount() - m_pBase->m_Time[uSpace] - m_pBase->m_Scan) >= 0;
	}

BOOL CBMikeLSMasterDriver::IsValid(UINT uSpace)
{
	return TRUE;
	}

BOOL CBMikeLSMasterDriver::IsModelValid(UINT uSpace)
{
	if( m_pBase->m_bModel > 0 ) {

		if( m_pBase->m_bModel == LS9K ) {

			switch( uSpace ) {

				case spaceAZS:

					return FALSE;
				}		
			}

		else if( m_pBase->m_bModel == LS8K ) {

			switch( uSpace ) {

				case spaceMinVL:
				case spaceMaxVL:
				case spaceSF:
				case spaceRZH:

					return FALSE;
				}		
			}

		else if( m_pBase->m_bModel == LS4K ) {

			switch( uSpace ) {

				case spaceFL:
				case space422BRS:
				case spaceMPIM:
				case spaceMPDT:
				case spaceMPVT:
				case spaceMPT:
				case spaceVALM:
				case spaceLRV:
				case spaceMinVL:
				case spaceMaxVL:
				case spaceMFL:
				case spaceHSPR:
				case space422POM:
				case spaceHSPCLR:
				case spaceLSPCLR:
				case spaceDLCM:
				case spaceSF:
				case spaceRZH:


					return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CBMikeLSMasterDriver::IsExempt(UINT uSpace)
{
	switch( uSpace ) {

		case spaceMVer:
		case spaceARB:

			return TRUE;
		}

	return FALSE;
	}

BYTE CBMikeLSMasterDriver::FromAscii(BYTE bByte)
{
	if( bByte >= '0' ) {
	
		if( bByte <= '9' ) {

			return bByte - '0';
			}

		return	bByte - '@' + 9;
		}

	return bByte;
	}

UINT CBMikeLSMasterDriver::GetItemNumber(AREF Addr)
{
	if( Addr.a.m_Table == addrNamed ) {

		return Addr.a.m_Offset;
		}

	return (Addr.a.m_Table - 1) * 64 + Addr.a.m_Offset / 16;
	}

// End of File
