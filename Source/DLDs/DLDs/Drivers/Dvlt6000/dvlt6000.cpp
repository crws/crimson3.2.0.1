
#include "intern.hpp"

#include "dvlt6000.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Danfoss VLT 6000 Driver
//

// Instantiator

INSTANTIATE(CDVLT6000Driver);

// Constructor

CDVLT6000Driver::CDVLT6000Driver(void)
{
	m_Ident = DRIVER_ID;
	
	}

// Destructor

CDVLT6000Driver::~CDVLT6000Driver(void)
{
	}

// Entry Points

CCODE MCALL CDVLT6000Driver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed + 1;
	Addr.a.m_Offset = m_pCtx->m_wPing;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

// Configuration

void MCALL CDVLT6000Driver::Load(LPCBYTE pData)
{
	}
	
void MCALL CDVLT6000Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CDVLT6000Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CDVLT6000Driver::Open(void)
{	
	
	}

// Device

CCODE MCALL CDVLT6000Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_wBus = 0;

			m_pCtx->m_wPing = GetWord(pData);
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CDVLT6000Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CDVLT6000Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	UINT uTable = Addr.a.m_Table;

	UINT uOffset = Addr.a.m_Offset & 0xFFF;

	if( IsCmd(uTable, uOffset) || IsBus(uTable, uOffset) ) {

		return uCount;
		}

	BOOL fStat  = IsStat(uTable, uOffset);

	BOOL fOut   = IsOut(uTable, uOffset);

	BYTE bLen   = 12;

	UINT uIndex = ((Addr.a.m_Offset >> 10) & 0x30) | Addr.a.m_Extra;

	StartFrame(bLen + 2);
	
	CParam PKE;

	(WORD &)PKE = 0;
	
	PKE.Num = LOWORD((fStat || fOut) ? 1 : uOffset);

	PKE.Cmd = CMD_READ;

	AddWord((WORD &)PKE);	// PKE

	AddWord(uIndex);	// IND

	AddDWord(0x0);		// PWE

	AddWord(FNC_NONE);	// PCD1

	AddWord(0x0);		// PCD2

	TxFrame();

	if( RxFrame() && m_bLen == bLen ) {

		*(WORD *)&PKE = ReadWord();

		if( PKE.Cmd == REPLY_WORD || PKE.Cmd == REPLY_DWORD ) {

			m_Ptr += 2;

			if( fStat ) {

				m_Ptr += 4;

				*pData = ReadWord();

				return uCount;
				}
			
			else if ( fOut ) {

				m_Ptr += 6;

				*pData = ReadWord();

				return uCount;
				}

			*pData = ReadDWord();
			
			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CDVLT6000Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	UINT uTable = Addr.a.m_Table;

	UINT uOffset = Addr.a.m_Offset & 0xFFF;

	if( IsStat(uTable, uOffset) || IsOut(uTable, uOffset) ) {

		return uCount;
		}

	if( IsBus(uTable, uOffset) ) {
		
		m_pCtx->m_wBus = LOWORD(*pData);

		return uCount;
		}

	BOOL fCmd   = IsCmd(uTable, uOffset);

	BYTE bLen   = 12;

	UINT uIndex = ((Addr.a.m_Offset >> 10) & 0x30) | Addr.a.m_Extra;

	StartFrame(bLen + 2);
	
	CParam PKE;

	(WORD &)PKE = 0;

	PKE.Cmd = fCmd ? CMD_READ : CMD_WRITE;

	PKE.Num = LOWORD(fCmd ? 1 : uOffset);

	AddWord((WORD &)PKE);					// PKE

	AddWord(uIndex);					// IND
							
	AddDWord(fCmd ? 0x0 : *pData);				// PWE

	AddWord(fCmd ? LOWORD(*pData | FNC_USED) : FNC_NONE);	// PCD1

	AddWord(fCmd ? m_pCtx->m_wBus : 0x0);			// PCD2

	TxFrame();

	if( RxFrame() && m_bLen == bLen ) {

		*(WORD *)&PKE = ReadWord();

		if( fCmd ) {

			if( PKE.Cmd == REPLY_WORD ) {

				return uCount;
				}
			}

		if( PKE.Cmd == REPLY_DWORD || PKE.Cmd == REPLY_WORD ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

// Frame Building

void CDVLT6000Driver::StartFrame(BYTE bLen)
{
	m_Ptr = 0;

	AddByte(STX);

	AddByte(bLen);

	AddDrop();
	}

void CDVLT6000Driver::AddByte(BYTE bData)
{
	if( m_Ptr < sizeof(m_bBuff) ) {

		m_bBuff[m_Ptr ++] = bData;
		}
	}

void CDVLT6000Driver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CDVLT6000Driver::AddDWord(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void CDVLT6000Driver::AddDrop(void)
{
	m_pCtx->m_bDrop |= 0x80;

	AddByte(m_pCtx->m_bDrop);
	}

BYTE CDVLT6000Driver::ReadByte(void)
{
	return m_bBuff[m_Ptr ++];
	}

WORD CDVLT6000Driver::ReadWord(void)
{
	BYTE bHi = ReadByte();

	BYTE bLo = ReadByte();

	return MAKEWORD(bLo, bHi); 
	}

DWORD CDVLT6000Driver::ReadDWord(void)
{
	WORD wHi = ReadWord();

	WORD wLo = ReadWord();

	return MAKELONG(wLo, wHi); 
	}

// Transport Layer

BOOL CDVLT6000Driver::TxFrame(void)
{
	ClearCheck();

	for( UINT n = 0; n < m_Ptr; n ++ ) {
	
		AddToCheck(m_bBuff[n]);
		}

	AddByte(m_bBcc);

	m_pData->Write(m_bBuff, m_Ptr, FOREVER);

	return TRUE;
	}

BOOL CDVLT6000Driver::RxFrame(void)
{
	UINT uState = 0;

	BYTE bCheck;

	BYTE bDrop;

	UINT uTimer = 0;

	UINT uWord = 0;

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uWord = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uWord == STX ) {
				
					ClearCheck();

					AddToCheck(STX);
					
					uState = 1;
					}
				break;
				
			case 1:
				AddToCheck(BYTE(uWord));
				
				m_bLen = BYTE(uWord) - 2;

				uState = 2;
				
				break;
				
			case 2:
				AddToCheck(BYTE(uWord));
				
				bDrop = BYTE(uWord);

				if( bDrop == m_pCtx->m_bDrop ) {

					m_Ptr  = 0;
					
					uState = 3;

					break;
					}
				
				return FALSE;

			case 3:
				AddToCheck(BYTE(uWord));

				m_bBuff[m_Ptr ++] = uWord;
					
				if( m_Ptr == sizeof(m_bBuff) ) {
					
					return FALSE;
					}
				
				if( m_Ptr == m_bLen ) {

					uState = 4;

					m_Ptr  = 0;
					}
				break;
				
			case 4:
				bCheck = uWord;
				
				return bCheck == m_bBcc;
			}
		}
	
	return FALSE;
	}

// Checksum Handler

void CDVLT6000Driver::ClearCheck(void)
{
	m_bBcc = 0;
	}

void CDVLT6000Driver::AddToCheck(BYTE b)
{
	m_bBcc ^= b;
	}

void CDVLT6000Driver::SendCheck(void)
{	 
	m_pData->Write(m_bBcc, FOREVER);
	}

// Helpers

BOOL CDVLT6000Driver::IsStat(UINT uTable, UINT uOffset)
{
	return uTable == addrNamed && uOffset == 3;
	}

BOOL CDVLT6000Driver::IsCmd(UINT uTable, UINT uOffset)
{
	return uTable == addrNamed && uOffset == 2;
	}

BOOL CDVLT6000Driver::IsBus(UINT uTable, UINT uOffset)
{
	return uTable == addrNamed && uOffset == 4;
	}

BOOL CDVLT6000Driver::IsOut(UINT uTable, UINT uOffset)
{
	return uTable == addrNamed && uOffset == 5;
	}

// End of File
