
//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Master Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class  CCANCalibrationMaster;
class  CCCPPortHandler;
struct FRAME_EXT;

//////////////////////////////////////////////////////////////////////////
//
// J1939 NAME Field
//

struct NAME
{
	union {
		struct {

			#ifdef _E_LITTLE

			DWORD	m_ID	: 21;
			DWORD	m_MC	: 11;

			#else

			DWORD	m_MC	: 11;
			DWORD	m_ID	: 21;

			#endif

			} b; 
		
		DWORD Lo;
		};

	union {

		struct {

			#ifdef _E_LITTLE

			DWORD   m_ECUI	: 3;
			DWORD   m_FI	: 5;
			DWORD	m_F	: 8;
			DWORD   m_R	: 1;
			DWORD   m_VS	: 7;
			DWORD	m_VSI	: 4;
			DWORD	m_IG	: 3;
			DWORD	m_Arb	: 1;

			#else

			DWORD	m_Arb	: 1;
			DWORD	m_IG	: 3;	 // Global = 0
			DWORD	m_VSI	: 4;
			DWORD   m_VS	: 7;	 //
			DWORD   m_R	: 1;
			DWORD	m_F	: 8;	 // Cab Display = 60  // Also preferred name if and iecu = 0
			DWORD   m_FI	: 5;
			DWORD   m_ECUI	: 3;

			#endif 

			} a; 
		
		DWORD Hi;
	       	};
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 29-Bit Identifier Field
//

struct ID {

	union {
		struct {

			#ifdef _E_LITTLE

			UINT  m_SA  : 8;
		       	UINT  m_PS  : 8;
			UINT  m_PF  : 8;
			UINT  m_DP  : 1;
			UINT  m_EDP : 1;
			UINT  m_P   : 3;
			UINT  m_X   : 3;

			#else
			
			UINT  m_X   : 3;
			UINT  m_P   : 3;
			UINT  m_EDP : 1;
			UINT  m_DP  : 1;
			UINT  m_PF  : 8;
		       	UINT  m_PS  : 8;
			UINT  m_SA  : 8;

			#endif
			
			} i;

		DWORD m_Ref;
		};
	};

// J1939 Network Management

#define CLAIM_REQ	0xEA00
#define CLAIM_RESP	0xEE00

// J1939 Data Transfer

#define PGN_ACK		0xE800
#define PGN_REQ		0xEA00	
#define PGN_TRANSFER	0xEB00
#define PGN_TRANSPORT	0xEC00

//////////////////////////////////////////////////////////////////////////
//
// J1939 SPN
//

struct CSPN
{
	UINT m_uSize;
	UINT m_uOffset;
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 PDU
//

class CPDU
{
	public:
		// Constructor
		CPDU(UINT uPGN, BYTE bPriority, UINT uUpdate, BOOL fCommand);

		// Destructor
		~CPDU(void);

		// Attributes
		UINT m_uPGN;
		UINT GetPriority(void);
		BOOL ShouldSendRequests(void);
		BOOL IsCommand(void);

		// Data Access
		void   StartData(void);
		void   FinishData(void);
		void   StoreData(FRAME_EXT const &Frame, UINT uOffset);
		void   StoreData(PCBYTE pData, UINT uOffset, UINT uCount);
		BOOL   ReadData(UINT uSPN, DWORD &dwData);
		BOOL   IsDataStale(void);
		void   RequestSent(void);
		BOOL   IsPending(void);
		BOOL   IsTimedOut(void);
		void   Reset(void);
		BOOL   IsValid(void);

		// SPN Management
		void   AllocSPNs(UINT uCount);
		void   AppendSPN(UINT uSize, UINT uOffset);	

	protected:
		// Constants
		static const UINT constSizeMax = 1785;

		// Data Members
		PBYTE  m_pData;
		PBYTE  m_pCache;
		UINT   m_uData;
		CSPN * m_pSPNs;
		UINT   m_uSPN;
		BYTE   m_bPriority;
		UINT   m_uUpdate;
		BOOL   m_fValid;
		UINT   m_uByteCount;
		UINT   m_uLastValid;
		UINT   m_uTicks;
		UINT   m_uTime;
		BOOL   m_fPending;
		BOOL   m_fCommand;

		// Implementation
		UINT  GetMask(UINT uBits);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Frame
//

#pragma pack(1)

struct FRAME_EXT
{
	BYTE	m_Ctrl;
	BYTE    m_Zero;
	DWORD	m_ID;
	BYTE	m_bData[8];
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// CCP DAQ Reference
//

struct CDaqRef
{
	BYTE m_bDaq;
	BYTE m_bId;
	};

//////////////////////////////////////////////////////////////////////////
//
// CCP DAQ Data Wrapper
//

class CDaqData
{
	public:
		// Constructor
		CDaqData(void);

		// Data
		BYTE  GetAsByte(BYTE bOffset);
		WORD  GetAsWord(BYTE bOffset, BOOL fBig);
		DWORD GetAsLong(BYTE bOffset, BOOL fBig);

		// Properties
		BOOL  m_fValid;
		BYTE  m_bDaq;
		BYTE  m_bPacketId;
		BYTE  m_bFirst;
		BYTE  m_bData[8];
		UINT  m_uLast;
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Address
//

struct CCANCalibrationAddress
{
	// CCP Properties
	UINT m_Address;
	BYTE m_Extension;
	BYTE m_Size;
	UINT m_SizeX;
	UINT m_SizeY;
	BOOL m_fDaq;
	UINT m_Type;
	UINT m_Ref;

	// CCP DAQ Properties
	DWORD m_CanId;
	BYTE  m_DaqId;
	BYTE  m_Odt;
	BYTE  m_Elem;
	BYTE  m_DaqOffset;

	// Data Scaling
	int m_Radix;
	int m_Scale;
	int m_Offset;
	};

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef CMap<UINT, CDaqData *> CDaqMap;

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Master Driver
//

class CCANCalibrationMaster : public CMasterDriver
{
	public:
		// Constructor
		CCANCalibrationMaster(void);

		// Configuration
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		DEFMETH(void) Close(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(void ) Service(void);

		// User Access
		DEFMETH(UINT) DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
	protected:
		// CCP State Machine
		enum {
			stateClosed,
			stateConnect,
			stateLocked,
			stateCal,
			stateDaq,
			stateOpen,		
			};

		// CCP Commands
		enum {
			cmdConnect       = 0x01,
			cmdExchangeId    = 0x17,
			cmdGetSeed       = 0x12,
			cmdUnlock        = 0x13,
			cmdSetMta        = 0x02,
			cmdDownload      = 0x03,
			cmdDownload6     = 0x23,
			cmdUpload        = 0x04,
			cmdShortUp       = 0x0F,
			cmdSelectCal     = 0x11,
			cmdGetDaqSize    = 0x14,
			cmdSetDaqPtr     = 0x15,
			cmdWriteDaq      = 0x16,
			cmdStartStop     = 0x06,
			cmdDisconnect    = 0x07,
			cmdSetSStatus    = 0x0C,
			cmdGetSStatus    = 0x0D,
			cmdBuildChksum   = 0x0E,
			cmdClearMemory   = 0x10,
			cmdProgram       = 0x18,
			cmdProgram6      = 0x22,
			cmdMove          = 0x19,
			cmdDiagService   = 0x20,
			cmdActionService = 0x21,
			cmdTest          = 0x05,
			cmdStartStopAll  = 0x08,
			cmdGetActviceCal = 0x09,
			cmdGetCcpVer     = 0x1B
			};

		// CCP Command Return Codes
		enum {
			codeAckOk        = 0x00,
			codeDaqOverload  = 0x01,
			codeProcBusy     = 0x10,
			codeTimeout      = 0x12,
			codeKeyReq       = 0x18,
			codeSessionReq   = 0x19,
			codeColdStart    = 0x20,
			codeCalDataInit  = 0x21,
			codeDaqListInit  = 0x22,
			codeUpdateReq    = 0x23,
			codeUnknownCmd   = 0x30,
			codeCmdSyntax    = 0x31,
			codeParamRange   = 0x32,
			codeAccessDenied = 0x33,
			codeOverload     = 0x34,
			codeAccessLocked = 0x35,
			codeNotAvailable = 0x36
			};

		// CCP Resources
		enum {
			resourceCal = Bit(0),
			resourceDaq = Bit(1),
			resourcePgm = Bit(6)
			};

		// CCP Status Bits
		enum {
			statCal    = Bit(0),
			statDaq    = Bit(1),
			statResume = Bit(2),
			statStore  = Bit(6),
			statRun    = Bit(7)
			};

		// CCP DAQ Modes
		enum {
			modeDaqStop  = 0x00,
			modeDaqStart = 0x01,
			modeDaqSync  = 0x02
			};

		// Device Data
		struct CContext
		{
			WORD  m_wStation;
			BOOL  m_fTest;
			UINT  m_uTime;
			UINT  m_uDaqTime;
			BYTE  m_bCounter;
			UINT  m_uState;
			UINT  m_fBig;
			UINT  m_uMasterId;
			UINT  m_uSlaveId;
			BYTE  m_bSlaveJ1939;			
			BOOL  m_fKeyValid;
			DWORD m_dwKey;

			CDaqMap     m_DaqMap;

			CCANCalibrationAddress * m_pNamed;
			CCANCalibrationAddress * m_pTable;

			UINT m_uTableMax;
			UINT m_uNamedMax;

			CPDU ** m_ppPDUs;
			UINT    m_uPDUs;
			};

		// Data Members
		CCCPPortHandler * m_pData;
		CContext        * m_pCtx;		
		FRAME_EXT         m_TxData;
		FRAME_EXT         m_RxData;
		UINT		  m_uBaud;
		BYTE              m_bMasterJ1939;

		// CCP Commands
		BOOL Connect(void);
		BOOL Disconnect(void);
		BOOL ExchangeId(BYTE &bLocked);
		BOOL GetSeed(BYTE bMask, BOOL &fLocked, DWORD &dwSeed);
		BOOL Unlock(DWORD dwKey);
		BOOL SetMta(BYTE bExt, DWORD dwAddress);
		BOOL Download(UINT uSize, PCBYTE pData);
		BOOL Upload(UINT uSize, PBYTE pData);
		BOOL ShortUpload(BYTE bSize, BYTE bExt, DWORD dwAddr, PBYTE pData);
		BOOL GetDaqSize(BYTE bDaq, DWORD dwCan, BYTE &bSize, BYTE &bId);
		BOOL SetDaqPtr(BYTE bDaq, BYTE bOdt, BYTE bElem);
		BOOL WriteDaq(BYTE bSize, BYTE bExt, DWORD dwAddr);
		BOOL StartStop(BYTE bMode, BYTE bDaq, BYTE bOdt, BYTE bEvt, WORD wPre);
		BOOL StartStopAll(BOOL fStart);
		BOOL SetSStatus(BYTE bStatus);
		BOOL GetSStatus(BYTE &bStatus);
		BOOL Test(void);
		BOOL GetVersion(BYTE &bMajor, BYTE &bMinor);

		// CCP Implementation
		BOOL  StartSession(void);
		BOOL  EndSession(CContext * pCtx);
		BOOL  UnlockResource(BYTE bResource);
		DWORD CalcKeyCal(DWORD dwSeed);
		DWORD CalcKeyDaq(DWORD dwSeed);
		void  SetupDaq(void);
		BOOL  ConfigDaq(CCANCalibrationAddress const &Addr);
		BOOL  UploadByte(BYTE bExt, DWORD dwAddr, PBYTE pData);
		BOOL  UploadWord(BYTE bExt, DWORD dwAddr, PBYTE pData);
		BOOL  UploadLong(BYTE bExt, DWORD dwAddr, PBYTE pData);
		BOOL  UploadData(CCANCalibrationAddress const &Addr, PDWORD pData, UINT uOffset, UINT uCount);
		BOOL  DownloadData(CCANCalibrationAddress const &Addr, PDWORD pData, UINT uCount);
		BOOL  DownloadByte(BYTE bData);
		BOOL  DownloadWord(WORD wData);
		BOOL  DownloadLong(DWORD dwData);
		void  GetDaqData(FRAME_EXT Frame);
		void  UpdateDaqTime(void);
		CCODE DaqRead(CCANCalibrationAddress Address, PDWORD pData, UINT uCount);
		DWORD TransformData(CCANCalibrationAddress const &Address, UINT uValue);
		DWORD PrepareData(CCANCalibrationAddress const &Address, UINT uValue);
		BOOL  CheckResponse(BYTE bCounter);
		void  HandleError(BYTE bError);
		BOOL  LookupAddress(AREF Addr, CCANCalibrationAddress &Address);
		BOOL  NeedDaq(void);

		// J1939 Diagnostic Support
		void   LoadJ1939(PCBYTE &pData);
		void   LoadPGN(UINT uPos, PCBYTE &pData);
		void   LoadSPN(CPDU * pPDU, PCBYTE &pData);
		BOOL   IsJ1939Diag(AREF Addr);
		CCODE  DiagRead(CAddress Addr, PDWORD pData, UINT uCount);
		CPDU * GetPDU(UINT uOffset);
		CPDU * FindPDU(UINT uPGN);
	
		// Implementation
		void  LoadAddresses(PCBYTE &pData);
		void  StartFrame(BYTE bCommand);
		BOOL  PutFrame(void);
		BOOL  GetFrame(void);
		void  SetByte(UINT uOffset, BYTE bData);
		void  SetWord(UINT uOffset, WORD wData);
		void  SetLong(UINT uOffset, DWORD dwData);
		BYTE  GrabByte(UINT uOffset);
		WORD  GrabWord(UINT uOffset);
		DWORD GrabLong(UINT uOffset);
		BOOL  Transact(BOOL fDump);

		// Debugging
		void DumpFrame(void);
		void Trace(PCTXT pMsg);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Frame Data
//

class CFrameData
{
	public:
		// Constructor
		CFrameData(void);

		// Operations
		void AddId(DWORD dwId);
		void RemoveId(DWORD dwId);
		void StoreFrame(FRAME_EXT Frame);
		BOOL GetCmdFrame(FRAME_EXT &Frame, UINT uTime);
		BOOL GetDaqFrame(FRAME_EXT &Frame, UINT uTime);
		BOOL GetEventFrame(FRAME_EXT &Frame, UINT uTime);
		void Reset(void);

		// Attributes
		BOOL HasId(DWORD dwId);

		// CCP Packet IDs
		enum {
			idCmd       = 0xFF,
			idEventMsg  = 0xFE,
			idDaqMax    = 0xFD,
			};
		
	protected:
		// Data Members
		UINT         m_uCmdHead;
		UINT         m_uCmdTail;
		UINT         m_uDaqHead;
		UINT         m_uDaqTail;
		UINT         m_uEvtHead;
		UINT         m_uEvtTail;
		ISemaphore * m_pCmdFlag;
		ISemaphore * m_pDaqFlag;
		ISemaphore * m_pEvtFlag;
		FRAME_EXT    m_RxCmd[32];
		FRAME_EXT    m_RxDaq[64];
		FRAME_EXT    m_RxEvt[32];
		CList<DWORD> m_Id;

		// Implementation
		void StoreCmdFrame(FRAME_EXT Frame);
		void StoreDaqFrame(FRAME_EXT Frame);
		void StoreEventFrame(FRAME_EXT Frame);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Port Handler
//

class CCCPPortHandler : public IPortHandler
{
	public:
		// Constructor
		CCCPPortHandler(CCANCalibrationMaster * pMaster, IHelper *pHelper);

		// Destructor
		~CCCPPortHandler(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);
		
		// Operations
		BOOL GetCmdFrame(void * pCtx, FRAME_EXT &Frame, UINT uTime);
		BOOL GetEventFrame(void * pCtx, FRAME_EXT &Frame, UINT uTime);
		BOOL GetDaqFrame(void * pCtx, FRAME_EXT &Frame, UINT uTime);
		BOOL PutFrame(void * pCtx, FRAME_EXT &Frame, UINT uTime);

		// J1939 Operations
		void SetSA(BYTE bSA);
		BOOL ClaimAddress(void);
		BOOL SourceAddrClaim(void);
		UINT DiagRead(BYTE bSlave, UINT uPGN, UINT uSPN, PDWORD pData, UINT uCount);
		UINT DiagCmd(BYTE bSlave, CPDU * pPDU, PDWORD pData, UINT uCount);

		// Configuration
		void AddContext(void * pCtx);
		void AddId(void * pCtx, DWORD dwId);
		void AddPDU(BYTE bSlave, CPDU * pPdu);

		// Status Constants
		enum {
			codeOk    = 1,
			codeSoft  = NOTHING - 1,
			codeHard  = NOTHING
			};
		
	protected:

		struct CReq
		{
			CPDU * m_pPDU;
			BYTE   m_bDA;

			bool operator == (CReq const &That);
			};

		typedef CList <CPDU *> CPDUList;
		typedef CArray<CReq  > CReqArray;

		struct CSlaveData {

			BYTE     m_bAddress;
			UINT     m_uPGN;
			UINT     m_uPackets;
			CPDUList m_List;
			};

		// Typedefs
		typedef CMap<void *, CFrameData *> CFrameMap;
		typedef CMap<BYTE  , CSlaveData *> CSlaveMap;

		// Data Members
		ULONG          m_uRefs;
		IPortObject  * m_pPort;
		IExtraHelper * m_pHelper;
		UINT           m_uTxCount;
		PCBYTE         m_pTxData;
		UINT           m_uRxByte;
		FRAME_EXT      m_RxData;
		void         * m_pSender;
		CFrameMap      m_FrameData;
		FRAME_EXT      m_Send[64];
		UINT volatile  m_uQueue;
		UINT volatile  m_uSend;
		INT  volatile  m_nTxDisable;

		// J1939 Data Members
		BYTE      m_bSrc;
		BOOL      m_fClaim;
		BOOL      m_fClaimSent;
		NAME    * m_pName;
		UINT      m_uClaim;
		CSlaveMap m_SlaveMap;
		CReqArray m_ReqQueue;
		UINT      m_uReq;

		// Implementation
		void SaveFrame(FRAME_EXT const &Frame);
		BOOL IsThisNode(FRAME_EXT const &Frame);
		BOOL IsJ1939(FRAME_EXT const &Frame);
		BOOL IsDM(UINT uPGN);
		void Increment(UINT volatile &uIndex);
		void SendFrame(void);

		// J1939 Implementation
		void HandleJ1939(FRAME_EXT const &Frame);
		BOOL IsAck(UINT uPGN);
		BOOL IsTransport(UINT uPGN);
		BOOL SendAck(ID id);
		BOOL SendNack(ID id);
		BOOL QueuePGNRequest(CPDU * pPDU, BYTE bDA);
		BOOL RemovePGNRequest(CPDU * pPDU, BYTE bDA);
		BOOL SendPGNRequest(CPDU * pPDU, BYTE bDA);

		// J1939 Network Management
		BOOL CannotClaimAddr(void);
		BOOL HandleClaimAddress(FRAME_EXT const &Frame);

		// J1939 Helpers
		BOOL  IsAddrClaimed(FRAME_EXT const &Frame);
		BOOL  IsAddrClaimedReq(FRAME_EXT const &Frame);
		BOOL  IsDestinationGlobal(ID id);
		BOOL  IsDestinationSpecific(ID id);
		BOOL  IsGroupExtension(ID id);
		BOOL  IsRequestPGN(FRAME_EXT const &Frame);
		BOOL  IsTransferPGN(FRAME_EXT const &Frame);
		BOOL  IsTransportPGN(FRAME_EXT const &Frame);
		BOOL  IsRTS(FRAME_EXT const &Frame);
		BOOL  IsCTS(FRAME_EXT const &Frame);
		BOOL  IsBAM(FRAME_EXT const &Frame);
		BOOL  IsEndOfMsg(FRAME_EXT const &Frame);
		BOOL  IsAbort(FRAME_EXT const &Frame);
		BOOL  IsDataTransfer(FRAME_EXT const &Frame);
		BOOL  IsBroadcast(ID id);
		void  MakeId(ID &id, UINT uID, UINT uPriority);
		UINT  FindPGN(FRAME_EXT const &Frame);
		UINT  FindPGN(ID const &id);
		void  StoreData(CSlaveData *pSlave, FRAME_EXT const &Frame, BOOL fTransfer, UINT uPGN = 0);
		BOOL  HasPending(void);
		void  CheckTimeouts(void);

		// J1939 Transport
		void ClearToSend(BYTE bDest, BYTE bPackets, UINT uPGN);
		void EndOfMsgAck(BYTE bDest);
		void HandleTransport(FRAME_EXT const &Frame);
		void HandleTransfer(FRAME_EXT const &Frame);
		void HandleData(UINT uPGN, FRAME_EXT const &Frame);

		// J1939 Frame Helpers
		void InitFrame(FRAME_EXT &Frame);
		void AddByte(FRAME_EXT &Frame, BYTE bData, UINT &uPos);
		void AddWord(FRAME_EXT &Frame, WORD wData, UINT &uPos);
		void AddLong(FRAME_EXT &Frame, DWORD dwData, UINT &uPos);
		void AddJ1939Id(FRAME_EXT &Frame, DWORD dwId, UINT &uPos);
		void AddPGN(FRAME_EXT &Frame, UINT uPGN, UINT &uPos);
		void AddName(FRAME_EXT &Frame);
	};

// End of file
