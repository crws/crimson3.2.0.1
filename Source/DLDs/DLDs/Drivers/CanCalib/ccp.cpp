
#include "intern.hpp"

#include "ccp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Master Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// CCP DAQ Data Wrapper
//

// Constructors

CDaqData::CDaqData(void)
{
	memset(m_bData, 0x00, sizeof(m_bData));

	m_fValid = FALSE;

	m_uLast  = 0;
	}

// Data

BYTE CDaqData::GetAsByte(BYTE bOffset)
{
	return m_bData[bOffset];
	}

WORD CDaqData::GetAsWord(BYTE bOffset, BOOL fBig)
{
	PBYTE pData = m_bData + bOffset;

	WORD  wData = *PWORD(pData);

	return fBig ? MotorToHost(wData) : IntelToHost(wData);
	}

DWORD CDaqData::GetAsLong(BYTE bOffset, BOOL fBig)
{
	PBYTE pData  = m_bData + bOffset;

	DWORD dwData = *PDWORD(pData);

	return fBig ? MotorToHost(dwData) : IntelToHost(dwData);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Master Driver
//

// Instantiator

INSTANTIATE(CCANCalibrationMaster);

// Constructor

CCANCalibrationMaster::CCANCalibrationMaster(void)
{
	m_Ident = DRIVER_ID;
	}

// Configuration

void MCALL CCANCalibrationMaster::CheckConfig(CSerialConfig &Config)
{
	Config.m_bDrop = 0xF9;

	Config.m_uFlags |= flagPrivate;
	
	Config.m_uFlags |= flagExtID;
	}

void CCANCalibrationMaster::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bMasterJ1939 = GetByte(pData);
		}
	}

// Management

void MCALL CCANCalibrationMaster::Attach(IPortObject *pPort)
{
	m_pData = new CCCPPortHandler(this, m_pHelper);

	m_pData->SetSA(m_bMasterJ1939);

	pPort->Bind(m_pData);
	}

void MCALL CCANCalibrationMaster::Detach(void)
{
	m_pData->Release();
	}

void MCALL CCANCalibrationMaster::Open(void)
{
	CMasterDriver::Open();
	}

void MCALL CCANCalibrationMaster::Close(void)
{
	if( m_pCtx->m_uState != stateClosed ) {

		EndSession(m_pCtx);
		}

	CMasterDriver::Close();
	}

// Device

CCODE MCALL CCANCalibrationMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_fTest        = GetByte(pData);
			m_pCtx->m_wStation     = GetWord(pData);
			m_pCtx->m_uTime        = GetLong(pData);
			m_pCtx->m_uDaqTime     = GetLong(pData);
			m_pCtx->m_uMasterId    = GetLong(pData);
			m_pCtx->m_uSlaveId     = GetLong(pData);
			m_pCtx->m_bSlaveJ1939  = GetByte(pData);
			m_pCtx->m_fBig         = GetByte(pData);
			m_pCtx->m_dwKey        = GetLong(pData);
			m_pCtx->m_uTableMax    = GetLong(pData);
			m_pCtx->m_uNamedMax    = GetLong(pData);
			m_pCtx->m_uState       = stateClosed;
			m_pCtx->m_bCounter     = 0;
			m_pCtx->m_fKeyValid    = TRUE;

			LoadAddresses(pData);

			LoadJ1939(pData);

			pDevice->SetContext(m_pCtx);

			m_pData->AddContext(m_pCtx);

			m_pData->AddId(m_pCtx, m_pCtx->m_uMasterId);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CCANCalibrationMaster::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		if( m_pCtx->m_uState != stateClosed ) {

			EndSession(m_pCtx);
			}

		delete [] m_pCtx->m_pTable;

		delete [] m_pCtx->m_pNamed;

		for( UINT n = 0; n < m_pCtx->m_uPDUs; n++ ) {

			delete m_pCtx->m_ppPDUs[n];
			}

		delete [] m_pCtx->m_ppPDUs;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}
		
// Entry Points

CCODE MCALL CCANCalibrationMaster::Ping(void)
{
	m_pData->ClaimAddress();

	if( m_pCtx->m_fTest ) {
		
		return Test() ? CCODE_SUCCESS : CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CCANCalibrationMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsJ1939Diag(Addr) ) {

		return DiagRead(Addr, pData, uCount);
		}
	
	if( m_pCtx->m_uState != stateOpen ) {
		
		StartSession();
		}

	if( m_pCtx->m_uState == stateOpen ) {
		
		CCANCalibrationAddress Address;

		if( LookupAddress(Addr, Address) ) {
			
			if( Address.m_fDaq ) {
				
				return DaqRead(Address, pData, uCount);
				}

			if( Addr.a.m_Table < addrNamed ) {
				
				if( UploadData(Address, pData, Addr.a.m_Offset, uCount) ) {

					return uCount;
					}

				return CCODE_ERROR;
				}

			BYTE  bData  = 0;

			WORD  wData  = 0;

			DWORD dwData = 0;

			switch( Address.m_Size ) {

				case 1:
					if( UploadByte(Address.m_Extension, Address.m_Address, &bData) ) {

						*pData = TransformData(Address, bData);

						return 1;
						}

					break;

				case 2:
					if( UploadWord(Address.m_Extension, Address.m_Address, PBYTE(&wData)) ) {

						*pData = TransformData(Address, wData);

						return 1;
						}

					break;

				case 4:
					if( UploadLong(Address.m_Extension, Address.m_Address, PBYTE(&dwData)) ) {

						*pData = TransformData(Address, dwData);

						return 1;
						}

					break;
				}
			}
		}
	
	EndSession(m_pCtx);
	
	return CCODE_ERROR;
	}

CCODE MCALL CCANCalibrationMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsJ1939Diag(Addr) ) {
		
		return uCount;
		}

	if( m_pCtx->m_uState != stateOpen ) {

		StartSession();
		}

	if( m_pCtx->m_uState == stateOpen ) {

		UINT uRef = (Addr.a.m_Table << 16) | Addr.a.m_Offset;

		CCANCalibrationAddress Address;

		if( LookupAddress(Addr, Address) ) {
			
			if( Addr.a.m_Table < addrNamed ) {

				if( DownloadData(Address, pData, uCount) ) {

					return uCount;
					}

				return CCODE_ERROR;
				}

			SetMta(Address.m_Extension, Address.m_Address);

			DWORD dwPrep = PrepareData(Address, *pData);

			if( Address.m_Size == 1 ) {

				BYTE bData = BYTE(dwPrep);

				if( DownloadByte(bData) ) {

					return 1;
					}
				}

			if( Address.m_Size == 2 ) {
				
				WORD wData = WORD(dwPrep);

				if( DownloadWord(wData) ) {

					return 1;
					}
				}

			if( Address.m_Size == 4 ) {
								
				DWORD dwData = dwPrep;

				if( DownloadLong(dwData) ) {

					return 1;
					}
				}
			}
		}

	EndSession(m_pCtx);

	return CCODE_ERROR;
	}

void CCANCalibrationMaster::Service(void)
{
	FRAME_EXT Frame;

	while( m_pData->GetEventFrame(m_pCtx, Frame, 0) ) {

		BYTE bError = Frame.m_bData[1];

		if( bError >= 0x20 && bError <= 0x23 ) {

			EndSession(m_pCtx);
			} 
		}

	UpdateDaqTime();
	
	while( m_pData->GetDaqFrame(m_pCtx, Frame, 0) ) {
		
		GetDaqData(Frame);
		}
	}

// User Access

UINT CCANCalibrationMaster::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	if( uFunc == 1 ) {

		CContext * pCtx = (CContext *) pContext;

		EndSession(pCtx);

		CString Num(Value);

		UINT uFind = Num.Find(';');

		if( uFind != NOTHING ) {

			Num = Num.Mid(uFind + 1);

			uFind = Num.Find('&');

			if( uFind != NOTHING ) {

				Num = Num.Left(uFind);
				}
			}

		DWORD dwKey       = strtoul(PCTXT(Num), NULL, 16);

		pCtx->m_fKeyValid = TRUE;

		pCtx->m_dwKey     = dwKey;

		return 1;
		}

	if( uFunc == 2 ) {

		CContext * pCtx = (CContext *) pContext;

		UINT uPGN = strtoul(Value, NULL, 10);

		CPDU * pPDU = FindPDU(uPGN);

		if( pPDU ) {

			if( pPDU->IsCommand() ) {

				m_pData->DiagCmd(pCtx->m_bSlaveJ1939, pPDU, NULL, 0);

				return 1;
				}
			}

		return 0;
		}

	return 0;
	}

// Implementation

void CCANCalibrationMaster::LoadAddresses(PCBYTE &pData)
{
	if( GetWord(pData) == 0x1234 ) {

		WORD wCount = GetWord(pData);

		m_pCtx->m_pTable = new CCANCalibrationAddress[ m_pCtx->m_uTableMax + 1 ];

		m_pCtx->m_pNamed = new CCANCalibrationAddress[ m_pCtx->m_uNamedMax + 1 ];

		memset(m_pCtx->m_pTable, 0, sizeof(CCANCalibrationAddress) * m_pCtx->m_uTableMax);

		memset(m_pCtx->m_pNamed, 0, sizeof(CCANCalibrationAddress) * m_pCtx->m_uNamedMax);
		
		for( UINT n = 0; n < wCount; n++ ) {

			if( GetWord(pData) == 0x1234 ) {

				CCANCalibrationAddress Address;

				Address.m_Address   = GetLong(pData);
				Address.m_Extension = GetByte(pData);
				Address.m_Size      = GetByte(pData);
				Address.m_SizeX     = GetLong(pData);
				Address.m_SizeY     = GetLong(pData);
				Address.m_fDaq      = GetByte(pData);
				Address.m_Type      = GetByte(pData);
				Address.m_Ref       = GetLong(pData);
				Address.m_CanId     = GetLong(pData);
				Address.m_DaqId     = GetByte(pData);
				Address.m_Odt       = GetByte(pData);
				Address.m_Elem      = GetByte(pData);
				Address.m_DaqOffset = GetByte(pData);
				Address.m_Radix     = GetLong(pData);
				Address.m_Scale     = GetLong(pData);
				Address.m_Offset    = GetLong(pData);
				
				UINT uTab = (Address.m_Ref & 0x00FF0000) >> 16;

				if( uTab < addrNamed ) {

					UINT uPos = uTab;

					m_pCtx->m_pTable[uPos] = Address;
					}
				else {
					UINT uPos = Address.m_Ref & 0xFFFF;

					m_pCtx->m_pNamed[uPos] = Address;					
					}
				}
			}
		}
	}

BOOL CCANCalibrationMaster::GetFrame(void)
{
	return m_pData->GetCmdFrame(m_pCtx, m_RxData, m_pCtx->m_uTime);
	}

void CCANCalibrationMaster::SetByte(UINT uOffset, BYTE bData)
{
	m_TxData.m_bData[uOffset] = bData;
	}

void CCANCalibrationMaster::SetWord(UINT uOffset, WORD wData)
{
	PBYTE pData = m_TxData.m_bData + uOffset;

	wData = m_pCtx->m_fBig ? HostToMotor(wData) : HostToIntel(wData);

	memcpy(pData, &wData, sizeof(wData));
	}

void CCANCalibrationMaster::SetLong(UINT uOffset, DWORD dwData)
{
	PBYTE pData = m_TxData.m_bData + uOffset;

	dwData = m_pCtx->m_fBig ? HostToMotor(dwData) : HostToIntel(dwData);

	memcpy(pData, &dwData, sizeof(dwData));
	}

BYTE CCANCalibrationMaster::GrabByte(UINT uOffset)
{
	return m_RxData.m_bData[uOffset];
	}

WORD CCANCalibrationMaster::GrabWord(UINT uOffset)
{
	PBYTE pData = m_RxData.m_bData + uOffset;

	WORD wData  = *PWORD(pData);

	return m_pCtx->m_fBig ? MotorToHost(wData) : IntelToHost(wData);
	}

DWORD CCANCalibrationMaster::GrabLong(UINT uOffset)
{
	PBYTE pData = m_RxData.m_bData + uOffset;

	DWORD dwData  = *PDWORD(pData);

	return m_pCtx->m_fBig ? MotorToHost(dwData) : IntelToHost(dwData);
	}

void CCANCalibrationMaster::StartFrame(BYTE bCommand)
{
	memset(&m_TxData, 0, sizeof(FRAME_EXT));

	memset(&m_RxData, 0, sizeof(FRAME_EXT));

	m_TxData.m_ID       = m_pCtx->m_uSlaveId;

	m_TxData.m_Zero     = 0;
	
	m_TxData.m_Ctrl     = 8 | 0x80;

	SetByte(0, bCommand);

	SetByte(1, m_pCtx->m_bCounter);
	}

BOOL CCANCalibrationMaster::PutFrame(void)
{
	return m_pData->PutFrame(m_pCtx, m_TxData, 1000);
	}

BOOL CCANCalibrationMaster::Transact(BOOL fDump = FALSE)
{
	BYTE bCtr = m_pCtx->m_bCounter;
	
	if( PutFrame() ) {

		if( GetFrame() ) {

			m_pCtx->m_bCounter++;

			if( fDump ) {

				DumpFrame();
				}
			
			if( CheckResponse(bCtr) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Protocol Implementation

BOOL CCANCalibrationMaster::Connect(void)
{
	StartFrame(cmdConnect);

	SetByte(2, LOBYTE(m_pCtx->m_wStation));

	SetByte(3, HIBYTE(m_pCtx->m_wStation));

	return Transact();
	}

BOOL CCANCalibrationMaster::Disconnect(void)
{
	StartFrame(cmdDisconnect);

	SetByte(2, 0x01);

	SetByte(3, 0x00);

	SetByte(4, LOBYTE(m_pCtx->m_wStation));

	SetByte(5, HIBYTE(m_pCtx->m_wStation));

	return Transact();
	}

BOOL CCANCalibrationMaster::ExchangeId(BYTE &bLocked)
{
	StartFrame(cmdExchangeId);

	m_TxData.m_bData[2] = 0x12;

	m_TxData.m_bData[3] = 0x34;

	if( Transact() ) {

		BYTE bSlaveLen = GrabByte(3);

		BYTE bSlaveImp = GrabByte(4);

		BYTE bAvail    = GrabByte(5);

		BYTE bProtect  = GrabByte(6);
		
		bLocked = bAvail & bProtect;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::GetSeed(BYTE bMask, BOOL &fLocked, DWORD &dwSeed)
{
	StartFrame(cmdGetSeed);
	
	SetByte(2, bMask);

	if( Transact() ) {

		fLocked = GrabByte(3);

		dwSeed  = GrabLong(4);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::SetMta(BYTE bExtension, DWORD dwAddress)
{
	StartFrame(cmdSetMta);

	SetByte(2, 0);

	SetByte(3, bExtension);

	SetLong(4, dwAddress);

	return Transact();
	}

BOOL CCANCalibrationMaster::Unlock(DWORD dwKey)
{
	StartFrame(cmdUnlock);
	
	SetLong(2, dwKey);

	if( Transact() ) {

		BYTE bMask = GrabByte(3);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::Download(UINT uSize, PCBYTE pData)
{
	StartFrame(cmdDownload);

	m_TxData.m_bData[2] = uSize;

	PBYTE pDest = m_TxData.m_bData + 3;

	memcpy(pDest, pData, uSize);

	if( Transact() ) {

		DWORD dwMta = GrabLong(4);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::Upload(UINT uSize, PBYTE pData)
{
	StartFrame(cmdUpload);

	SetByte(2, uSize);

	if( Transact() ) {
		
		for( UINT n = 0; n < uSize; n++ ) {

			pData[n] = GrabByte(n + 3);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::ShortUpload(BYTE bSize, BYTE bExt, DWORD dwAddr, PBYTE pData)
{
	StartFrame(cmdShortUp);

	SetByte(2, bSize);

	SetByte(3, bExt);

	SetLong(4, dwAddr);

	if( Transact() ) {

		PCBYTE pSource = m_RxData.m_bData + 3;

		memcpy(pData, pSource, bSize);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::GetDaqSize(BYTE bDaq, DWORD dwCan, BYTE &bSize, BYTE &bId)
{
	StartFrame(cmdGetDaqSize);

	SetByte(2, bDaq);

	if( dwCan > 0x07FF ) {

		dwCan |= 0x80000000;
		}
	
	SetLong(4, dwCan);

	if( Transact() ) {

		bSize = GrabByte(3);

		bId   = GrabByte(4);
		
		return bSize > 0;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::SetDaqPtr(BYTE bDaq, BYTE bOdt, BYTE bElem)
{
	StartFrame(cmdSetDaqPtr);

	SetByte(2, bDaq);

	SetByte(3, bOdt);

	SetByte(4, bElem);

	return Transact();
	}

BOOL CCANCalibrationMaster::WriteDaq(BYTE bSize, BYTE bExt, DWORD dwAddr)
{
	StartFrame(cmdWriteDaq);

	SetByte(2, bSize);

	SetByte(3, bExt);

	SetLong(4, dwAddr);

	return Transact();
	}

BOOL CCANCalibrationMaster::StartStop(BYTE bMode, BYTE bDaq, BYTE bOdt, BYTE bEvt, WORD wPre)
{	
	StartFrame(cmdStartStop);

	SetByte(2, bMode);

	SetByte(3, bDaq);

	SetByte(4, bOdt);

	SetByte(5, bEvt);

	SetWord(6, wPre);

	return Transact();
	}

BOOL CCANCalibrationMaster::StartStopAll(BOOL fStart)
{
	StartFrame(cmdStartStopAll);

	SetByte(2, fStart);
	
	return Transact();
	}

BOOL CCANCalibrationMaster::SetSStatus(BYTE bStatus)
{
	StartFrame(cmdSetSStatus);

	SetByte(2, bStatus);

	return Transact();
	}

BOOL CCANCalibrationMaster::GetSStatus(BYTE &bStatus)
{
	StartFrame(cmdGetSStatus);

	if( Transact() ) {

		bStatus = GrabByte(3);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::Test(void)
{
	StartFrame(cmdTest);

	SetByte(2, LOBYTE(m_pCtx->m_wStation));

	SetByte(3, HIBYTE(m_pCtx->m_wStation));
	
	return Transact();
	}

BOOL CCANCalibrationMaster::GetVersion(BYTE &bMajor, BYTE &bMinor)
{
	StartFrame(cmdGetCcpVer);

	SetByte(2, 2);

	SetByte(3, 1);

	if( Transact() ) {

		bMajor = GrabByte(3);

		bMinor = GrabByte(4);
		
		return TRUE;
		}

	return FALSE;
	}

// CCP Implementation

BOOL CCANCalibrationMaster::StartSession(void)
{
	if( !m_pCtx->m_fKeyValid ) {

		return FALSE;
		}
		
	if( Connect() ) {
		
		BYTE bMajor, bMinor;

		if( !GetVersion(bMajor, bMinor) ) {

			return FALSE;
			}

		BYTE bLocked = 0;

		if( !ExchangeId(bLocked) ) {

			return FALSE;
			}

		if( bLocked & resourceCal ) {

			if( !UnlockResource(resourceCal) ) {

				m_pCtx->m_fKeyValid = FALSE;

				return FALSE;
				}
			}

		m_pCtx->m_uState = stateCal;

		if( bLocked & resourceDaq ) {
			
			if( !UnlockResource(resourceDaq) ) {

				if( NeedDaq() ) {

					m_pCtx->m_fKeyValid = FALSE;

					return FALSE;
					}
				}
			}
		
		SetupDaq();

		m_pCtx->m_uState = stateOpen;
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::EndSession(CContext * pCtx)
{
	if( pCtx->m_uState != stateClosed ) {

		StartStopAll(FALSE);

		CDaqMap &Map = pCtx->m_DaqMap;

		for( INDEX i = Map.GetHead(); !Map.Failed(i); Map.GetNext(i) ) {

			CDaqData * pData = Map.GetData(i);

			delete pData;
			}

		pCtx->m_DaqMap.Empty();

		pCtx->m_uState   = stateClosed;

		pCtx->m_bCounter = 0;

		return Disconnect();
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::UnlockResource(BYTE bResource)
{
	BOOL fLocked  = FALSE;

	UINT uLen     = 0;

	DWORD dwSeed  = 0;

	if( GetSeed(bResource, fLocked, dwSeed) ) {
		
		PCBYTE pData = m_RxData.m_bData + 4;

		DWORD dwKey = bResource == resourceCal ? CalcKeyCal(dwSeed) : CalcKeyDaq(dwSeed);

		if( Unlock(dwKey) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

DWORD CCANCalibrationMaster::CalcKeyCal(DWORD dwSeed)
{
	DWORD dwKey = m_pCtx->m_dwKey;

	DWORD dwOut = 0;

	dwOut = dwSeed ^ dwKey;

	dwOut = (dwOut >> 7) | (dwOut << 25);

	dwOut = dwOut ^ dwSeed;

	return dwOut;
	}

DWORD CCANCalibrationMaster::CalcKeyDaq(DWORD dwSeed)
{
	DWORD dwKey = m_pCtx->m_dwKey;

	DWORD dwOut = 0;

	dwOut = dwSeed ^ dwKey;

	dwOut = (dwOut >> 3) | (dwOut << 29);

	dwOut = dwOut ^ dwSeed;

	dwOut = (dwOut << 5) | (dwOut >> 27);

	dwOut = dwOut ^ dwSeed;

	return dwOut;
	}

void CCANCalibrationMaster::SetupDaq(void)
{
	for( UINT n = 0; n < m_pCtx->m_uNamedMax + 1; n++ ) {
		
		CCANCalibrationAddress Addr = m_pCtx->m_pNamed[n];

		if( Addr.m_Ref && Addr.m_fDaq ) {

			INDEX iDaq = m_pCtx->m_DaqMap.FindName(Addr.m_Odt);

			BYTE bSize, bId;

			if( m_pCtx->m_DaqMap.Failed(iDaq) ) {

				CDaqData *pData    = new CDaqData;

				pData->m_bDaq      = Addr.m_DaqId;

				pData->m_bPacketId = Addr.m_Odt;

				m_pCtx->m_DaqMap.Insert(Addr.m_Odt, pData);

				m_pData->AddId(m_pCtx, Addr.m_CanId);

				GetDaqSize(Addr.m_DaqId, Addr.m_CanId, bSize, bId);

				pData->m_bFirst = bId;
				}
			
			ConfigDaq(Addr);
			}
		}
	
	CDaqMap DaqMap = m_pCtx->m_DaqMap;

	for( INDEX j = DaqMap.GetHead(); !DaqMap.Failed(j); DaqMap.GetNext(j) ) {

		CDaqData *pData = DaqMap.GetData(j);
		
		StartStop(modeDaqStart, pData->m_bDaq, pData->m_bPacketId, 1, 1);
		}

	SetSStatus(statRun | statDaq);
	}

BOOL CCANCalibrationMaster::ConfigDaq(CCANCalibrationAddress const &Addr)
{
	INDEX i = m_pCtx->m_DaqMap.FindName(Addr.m_Odt);

	if( m_pCtx->m_DaqMap.Failed(i) ) {

		return FALSE;
		}

	CDaqData * pDaq = m_pCtx->m_DaqMap.GetData(i);

	if( !SetDaqPtr(Addr.m_DaqId, Addr.m_Odt - pDaq->m_bFirst, Addr.m_Elem) ) {

		return FALSE;
		}
		
	if( !WriteDaq(Addr.m_Size, Addr.m_Extension, Addr.m_Address) ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CCANCalibrationMaster::UploadByte(BYTE bExt, DWORD dwAddr, PBYTE pData)
{
	if( ShortUpload(1, bExt, dwAddr, pData) ) {

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCANCalibrationMaster::UploadWord(BYTE bExt, DWORD dwAddr, PBYTE pData)
{
	if( ShortUpload(2, bExt, dwAddr, pData) ) {

		PWORD pWord = PWORD(pData);
			
		*pWord = m_pCtx->m_fBig ? MotorToHost(*pWord) : IntelToHost(*pWord);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::UploadLong(BYTE bExt, DWORD dwAddr, PBYTE pData)
{
	if( ShortUpload(4, bExt, dwAddr, pData) ) {

		PDWORD pDword = PDWORD(pData);
			
		*pDword = m_pCtx->m_fBig ? MotorToHost(*pDword) : IntelToHost(*pDword);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::UploadData(CCANCalibrationAddress const &Addr, PDWORD pData, UINT uOffset, UINT uCount)
{
	DWORD dwStart = Addr.m_Address + (uOffset * Addr.m_Size);

	if( !SetMta(Addr.m_Extension, dwStart) ) {
				
		return FALSE;
		}

	UINT uMax    = 5 / Addr.m_Size;

	UINT uPos    = 0;
	
	BYTE Buff[5] = {0};

	while( uCount ) {
		
		UINT uTake = min(uCount * Addr.m_Size, uMax);

		if( !Upload(uTake, Buff) ) {
			
			return FALSE;
			}

		BOOL fBig = m_pCtx->m_fBig;

		for( UINT n = 0; n < uTake; ) {
						
			DWORD dwRaw  = 0;

			if( Addr.m_Size == 1 ) {

				dwRaw = Buff[n];

				pData[uPos] = TransformData(Addr, dwRaw);

				n++;
				}

			if( Addr.m_Size == 2 ) {

				PBYTE pBuff = Buff + n;

				WORD wData  = *PWORD(pBuff);

				dwRaw = fBig ? MotorToHost(wData) : IntelToHost(wData);

				pData[uPos] = TransformData(Addr, dwRaw);

				n += 2;
				}

			if( Addr.m_Size == 4 ) {

				PBYTE pBuff  = Buff + n;

				DWORD dwData = *PDWORD(pBuff);

				dwRaw = fBig ? MotorToHost(dwData) : IntelToHost(dwData);
				
				pData[uPos] = TransformData(Addr, dwRaw);

				n += 4;
				}

			uCount--;

			uPos++;
			}
		}
	
	return TRUE;
	}

BOOL CCANCalibrationMaster::DownloadData(CCANCalibrationAddress const &Addr, PDWORD pData, UINT uCount)
{
	if( !SetMta(Addr.m_Extension, Addr.m_Address) ) {

		return FALSE;
		}

	UINT uMax    = 5 / Addr.m_Size;

	UINT uLeft   = uCount;

	BYTE Buff[5] = {0};

	UINT uPos    = 0;

	while( uLeft ) {

		UINT uPut = min(uLeft, uMax);		

		for( UINT n = 0; n < uPut; uPos++ ) {

			DWORD dwRaw  = 0;

			BOOL  fBig   = m_pCtx->m_fBig;

			if( Addr.m_Size == 1 ) {

				Buff[n] = PrepareData(Addr, pData[uPos]);

				n++;
				}

			if( Addr.m_Size == 2 ) {

				WORD wData = PrepareData(Addr, pData[uPos]);

				wData = fBig ? HostToMotor(wData) : HostToIntel(wData);

				memcpy(Buff + n, PBYTE(&wData), sizeof(wData));

				n += 2;
				}

			if( Addr.m_Size == 4 ) {

				DWORD dwData = PrepareData(Addr, pData[uPos]);

				dwData = fBig ? HostToMotor(dwData) : HostToIntel(dwData);

				memcpy(Buff + n, PBYTE(&dwData), sizeof(dwData));

				n += 4;
				}
			}
		
		if( !Download(uPut, Buff) ) {

			return FALSE;
			}	

		uLeft -= uPut;
		}

	return TRUE;
	}

BOOL CCANCalibrationMaster::DownloadByte(BYTE bData)
{
	return Download(1, &bData);
	}

BOOL CCANCalibrationMaster::DownloadWord(WORD wData)
{
	wData = m_pCtx->m_fBig ? HostToMotor(wData) : HostToIntel(wData);

	return Download(2, PBYTE(&wData));
	}

BOOL CCANCalibrationMaster::DownloadLong(DWORD dwData)
{
	dwData = m_pCtx->m_fBig ? HostToMotor(dwData) : HostToIntel(dwData);

	return Download(4, PBYTE(&dwData));
	}

void CCANCalibrationMaster::GetDaqData(FRAME_EXT Frame)
{
	BYTE bPacket = Frame.m_bData[0];

	CDaqMap Map = m_pCtx->m_DaqMap;

	INDEX i = Map.FindName(bPacket);

	if( !Map.Failed(i) ) {
		
		CDaqData * pDaq = Map.GetData(i);

		memcpy(pDaq->m_bData, Frame.m_bData, 8);

		pDaq->m_fValid = TRUE;

		pDaq->m_uLast  = GetTickCount();
		}
	}

void CCANCalibrationMaster::UpdateDaqTime(void)
{
	CDaqMap Map = m_pCtx->m_DaqMap;

	for( INDEX i = Map.GetHead(); !Map.Failed(i); Map.GetNext(i) ) {

		CDaqData * pDaq = Map.GetData(i);

		if( pDaq->m_fValid ) {

			if( ToTime(GetTickCount() - pDaq->m_uLast) > m_pCtx->m_uDaqTime ) {

				pDaq->m_fValid = FALSE;
				}
			}
		}
	}

CCODE CCANCalibrationMaster::DaqRead(CCANCalibrationAddress Address, PDWORD pData, UINT uCount)
{
	BYTE bPacket = Address.m_Odt;

	INDEX i = m_pCtx->m_DaqMap.FindName(bPacket);

	BYTE bOffset = Address.m_DaqOffset;

	if( !m_pCtx->m_DaqMap.Failed(i) ) {

		CDaqData *pDaq = m_pCtx->m_DaqMap.GetData(i);

		if( pDaq->m_fValid ) {

			switch( Address.m_Size ) {

				case 1:
					*pData = TransformData(Address, pDaq->GetAsByte(bOffset));

					break;

				case 2:
					*pData = TransformData(Address, pDaq->GetAsWord(bOffset, m_pCtx->m_fBig));

					break;

				case 4:
					*pData = TransformData(Address, pDaq->GetAsLong(bOffset, m_pCtx->m_fBig));

					break;
				}

			return 1;
			}
		}

	EndSession(m_pCtx);

	return CCODE_ERROR;
	}

DWORD CCANCalibrationMaster::TransformData(CCANCalibrationAddress const &Address, UINT uValue)
{
	BOOL fInvert = !!(Address.m_Type & 0x80);
	
	uValue = uValue * Address.m_Scale;

	UINT uShift = abs(Address.m_Radix);

	if( Address.m_Radix > 0 ) {

		uValue <<= uShift;
		}
	else {
		uValue >>= uShift;
		}

	if( fInvert ) {

		uValue = 1.0f / uValue;
		}

	return uValue + Address.m_Offset;
	}

DWORD CCANCalibrationMaster::PrepareData(CCANCalibrationAddress const &Address, UINT uValue)
{
	BOOL fInvert = !!(Address.m_Type & 0x80);

	uValue -= Address.m_Offset;

	if( fInvert && uValue > 0 ) {

		uValue = 1.0f / uValue;
		}
	
	UINT uShift = abs(Address.m_Radix);

	if( Address.m_Radix > 0 ) {

		uValue >>= uShift;
		}
	else {
		uValue <<= uShift;
		}
	
	uValue = uValue / Address.m_Scale;
		
	return uValue;
	}


BOOL CCANCalibrationMaster::CheckResponse(BYTE bCounter)
{
	BYTE bPacketId = GrabByte(0);

	BYTE bCode     = GrabByte(1);

	BYTE bCtr      = GrabByte(2);

	if( bPacketId == 0xFF && bCtr == bCounter ) {
		
		if( bCode == codeAckOk ) {

			return TRUE;
			}

		HandleError(bCode);
		}
	
	return FALSE;
	}

void CCANCalibrationMaster::HandleError(BYTE bError)
{
	AfxTrace("CCP ERROR -- %2.2X\n", bError);
	
	if( bError >= 0x20 && bError <= 0x23 ) {

		EndSession(m_pCtx);
		}
	}

BOOL CCANCalibrationMaster::LookupAddress(AREF Addr, CCANCalibrationAddress &Address)
{
	if( Addr.a.m_Table < addrNamed ) {

		Address = m_pCtx->m_pTable[Addr.a.m_Table];

		return TRUE;
		}
	else {
		Address = m_pCtx->m_pNamed[Addr.a.m_Offset];

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANCalibrationMaster::NeedDaq(void)
{
	for( UINT n = 0; n < m_pCtx->m_uNamedMax; n++ ) {

		if( m_pCtx->m_pNamed[n].m_fDaq ) {
		
			return TRUE;
			}
		}

	return FALSE;
	}

// J1939 Diagnostic Support

void CCANCalibrationMaster::LoadJ1939(PCBYTE &pData)
{
	if( GetWord(pData) == 0x1234 ) {

		WORD wCount = GetWord(pData);

		m_pCtx->m_uPDUs = wCount;
		
		m_pCtx->m_ppPDUs = new CPDU * [wCount];

		for( UINT n = 0; n < wCount; n++ ) {

			LoadPGN(n, pData);
			}
		}
	}

void CCANCalibrationMaster::LoadPGN(UINT uPos, PCBYTE &pData)
{
	if( GetWord(pData) == 0x1234 ) {
		
		UINT uNumber   = GetLong(pData);
		
		BYTE bPriority = GetByte(pData);

		UINT uUpdate   = GetLong(pData);

		BOOL fCommand  = GetByte(pData);
				
		CPDU * pPDU    = new CPDU(uNumber, bPriority, uUpdate, fCommand);

		m_pCtx->m_ppPDUs[uPos] = pPDU;

		m_pData->AddPDU(m_pCtx->m_bSlaveJ1939, pPDU);

		if( GetWord(pData) == 0x1234 ) {

			WORD wCount = GetWord(pData);

			pPDU->AllocSPNs(wCount);

			for( UINT n = 0; n < wCount; n++ ) {

				LoadSPN(pPDU, pData);
				}
			}
		}
	}

void CCANCalibrationMaster::LoadSPN(CPDU * pPDU, PCBYTE &pData)
{
	if( GetWord(pData) == 0x1234 ) {

		UINT uNumber = GetLong(pData);

		UINT uBits   = GetLong(pData);

		UINT uOffset = GetLong(pData);

		pPDU->AppendSPN(uBits, uOffset);
		}
	}

BOOL CCANCalibrationMaster::IsJ1939Diag(AREF Addr)
{
	return Addr.a.m_Extra == 1;
	}

CCODE CCANCalibrationMaster::DiagRead(CAddress Addr, PDWORD pData, UINT uCount)
{
	CPDU * pPDU = GetPDU(Addr.a.m_Table);

	UINT uRead = m_pData->DiagRead(m_pCtx->m_bSlaveJ1939, pPDU->m_uPGN, Addr.a.m_Offset, pData, uCount);

	if( uRead == CCCPPortHandler::codeSoft ) {

		return CCODE_SILENT;
		}

	if( uRead == CCCPPortHandler::codeOk ) {

		return 1;
		}

	return CCODE_ERROR;
	}

CPDU * CCANCalibrationMaster::GetPDU(UINT uOffset)
{
	if( uOffset < m_pCtx->m_uPDUs ) {

		return m_pCtx->m_ppPDUs[uOffset];
		}

	return NULL;
	}

CPDU * CCANCalibrationMaster::FindPDU(UINT uPGN)
{
	for( UINT n = 0; n < m_pCtx->m_uPDUs; n++ ) {

		if( m_pCtx->m_ppPDUs[n]->m_uPGN == uPGN ) {

			return m_pCtx->m_ppPDUs[n];
			}
		}
	
	return NULL;
	}

// Debugging

void CCANCalibrationMaster::DumpFrame(void)
{
	AfxTrace("(%8.8X) -- ", m_RxData.m_ID);

	for( UINT n = 0; n < (m_RxData.m_Ctrl & ~0x80); n++ ) {

		AfxTrace("%2.2X ", GrabByte(n));
		}

	AfxTrace("\n");
	}

void CCANCalibrationMaster::Trace(PCTXT pMsg)
{
	#if defined CCP_DEBUG

	AfxTrace("CCP - %s\n", pMsg);

	#endif
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol FrameData
//

// Constructor

CFrameData::CFrameData(void)
{
	m_pCmdFlag = Create_Semaphore(0);

	m_pDaqFlag = Create_Semaphore(0);

	m_pEvtFlag = Create_Semaphore(0);

	m_uCmdHead = 0;

	m_uCmdTail = 0;

	m_uDaqHead = 0;

	m_uDaqTail = 0;

	m_uEvtHead = 0;

	m_uEvtTail = 0;
	}

// Operations

void CFrameData::AddId(DWORD dwId)
{	
	m_Id.Append(dwId);
	}

void CFrameData::StoreFrame(FRAME_EXT Frame)
{
	if( HasId(Frame.m_ID) ) {
		
		BYTE bID = Frame.m_bData[0];

		if( bID == idCmd ) {

			StoreCmdFrame(Frame);
			}

		if( bID <= idDaqMax ) {
								
			StoreDaqFrame(Frame);
			}

		if( bID == idEventMsg ) {

			StoreEventFrame(Frame);
			}
		}
	}

BOOL CFrameData::GetCmdFrame(FRAME_EXT &Frame, UINT uTime)
{
	if( m_pCmdFlag->Wait(uTime) ) {

		Frame      = m_RxCmd[m_uCmdHead];

		m_uCmdHead = (m_uCmdHead + 1) % elements(m_RxCmd);

		return TRUE;
		}

	return FALSE;
	}

BOOL CFrameData::GetDaqFrame(FRAME_EXT &Frame, UINT uTime)
{
	if( m_pDaqFlag->Wait(uTime) ) {

		Frame      = m_RxDaq[m_uDaqHead];

		m_uDaqHead = (m_uDaqHead + 1) % elements(m_RxDaq);

		return TRUE;
		}

	return FALSE;
	}

BOOL CFrameData::GetEventFrame(FRAME_EXT &Frame, UINT uTime)
{
	if( m_pEvtFlag->Wait(uTime) ) {

		Frame      = m_RxEvt[m_uEvtHead];

		m_uEvtHead = (m_uEvtHead + 1) % elements(m_RxEvt);

		return TRUE;
		}

	return FALSE;
	}

void CFrameData::Reset(void)
{
	while( m_pCmdFlag->Wait(0) );

	while( m_pDaqFlag->Wait(0) );

	while( m_pEvtFlag->Wait(0) );
	}

// Attributes

BOOL CFrameData::HasId(DWORD dwId)
{
	INDEX i = m_Id.Find(dwId);

	return !m_Id.Failed(i);
	}

// Implementation

void CFrameData::StoreCmdFrame(FRAME_EXT Frame)
{
	UINT uNext = (m_uCmdTail + 1) % elements(m_RxCmd);

	if( uNext != m_uCmdHead ) {

		m_RxCmd[m_uCmdTail] = Frame;

		m_uCmdTail = uNext;

		m_pCmdFlag->Signal(1);
		}
	}

void CFrameData::StoreDaqFrame(FRAME_EXT Frame)
{
	UINT uNext = (m_uDaqTail + 1) % elements(m_RxDaq);

	if( uNext != m_uDaqHead ) {

		m_RxDaq[m_uDaqTail] = Frame;

		m_uDaqTail = uNext;

		m_pDaqFlag->Signal(1);
		}
	}

void CFrameData::StoreEventFrame(FRAME_EXT Frame)
{
	UINT uNext = (m_uEvtTail + 1) % elements(m_RxEvt);

	if( uNext != m_uEvtHead ) {

		m_RxEvt[m_uEvtTail] = Frame;

		m_uEvtTail = uNext;

		m_pEvtFlag->Signal(1);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// J1939 PDU
//
	
// Constructor

CPDU::CPDU(UINT uPGN, BYTE bPriority, UINT uUpdate, BOOL fCommand)
{
	m_uPGN       = uPGN;

	m_bPriority  = bPriority;

	m_uUpdate    = uUpdate;

	m_fCommand   = fCommand;
	
	m_uTicks     = 0;

	m_uTime      = 0;

	m_pSPNs      = NULL;

	m_fValid     = FALSE;

	m_uLastValid = 0;

	m_uByteCount = 0;

	m_fPending   = FALSE;

	m_uData      = constSizeMax;

	m_pData      = new BYTE[m_uData];

	m_pCache     = new BYTE[m_uData];
	
	memset(m_pData,  0, constSizeMax);

	memset(m_pCache, 0, constSizeMax);
	}

// Destructor

CPDU::~CPDU(void)
{
	delete [] m_pData;

	delete [] m_pCache;

	delete [] m_pSPNs;
	}

// Attributes

UINT CPDU::GetPriority(void)
{
	return m_bPriority;
	}

BOOL CPDU::ShouldSendRequests(void)
{
	return m_uUpdate > 0;
	}

BOOL CPDU::IsCommand(void)
{
	return m_fCommand;
	}

// Data Access

void CPDU::StartData(void)
{
	memset(m_pData, 0, constSizeMax);

	m_uByteCount = 0;
	}

void CPDU::FinishData(void)
{
	m_fValid = TRUE;

	m_fPending = FALSE;

	m_uTicks = GetTickCount();

	m_uLastValid = m_uByteCount;

	memcpy(m_pCache, m_pData, constSizeMax);
	}

void CPDU::StoreData(FRAME_EXT const &Frame, UINT uOffset)
{
	UINT uLen = Frame.m_Ctrl;

	memcpy(m_pData + uOffset, Frame.m_bData, uLen);

	m_uByteCount += uLen;
	}

void CPDU::StoreData(PCBYTE pData, UINT uOffset, UINT uCount)
{
	memcpy(m_pData + uOffset, pData, uCount);

	m_uByteCount += uCount;
	}

BOOL CPDU::ReadData(UINT uSPN, DWORD &dwData)
{
	if( m_fValid ) {

		CSPN * pSPN = m_pSPNs + uSPN;

		UINT uBits = pSPN->m_uSize;

		UINT k = 0;

		PBYTE pData = PBYTE(&dwData);

		while( uBits ) {

			UINT uByte = k + (pSPN->m_uOffset / 8);
		
			if( uByte > m_uLastValid ) {

				return FALSE;
				}

			UINT uMask = GetMask(uBits);

			UINT uShift = 0;

			UINT uOffset = pSPN->m_uOffset % 8;

			if( uBits < 8 ) {

				uShift = 8 - uBits - uOffset;

				uMask <<= uShift;
				}

			pData[k] = (uMask & m_pCache[uByte]) >> uShift;

			uBits -= min(uBits, 8);

			k++;
			}
				
		return TRUE;
		}

	return FALSE;
	}

BOOL CPDU::IsDataStale(void)
{
	return GetTickCount() - m_uTicks >= ToTicks(m_uUpdate);
	}

void CPDU::RequestSent(void)
{	
	m_uTime = GetTickCount();

	m_fPending = TRUE;
	}

BOOL CPDU::IsPending(void)
{
	return m_fPending;
	}

BOOL CPDU::IsTimedOut(void)
{
	return IsPending() && (GetTickCount() - m_uTime >= ToTicks(2 * m_uUpdate));
	}

void CPDU::Reset(void)
{
	m_uLastValid = 0;

	m_fPending = FALSE;

	m_fValid = FALSE;
	}

BOOL CPDU::IsValid(void)
{
	return m_fValid;
	}

// SPN Management

void CPDU::AllocSPNs(UINT uCount)
{
	m_pSPNs = new CSPN[uCount];

	m_uSPN  = 0;
	}

void CPDU::AppendSPN(UINT uSize, UINT uOffset)
{
	m_pSPNs[m_uSPN].m_uSize   = uSize;

	m_pSPNs[m_uSPN].m_uOffset = uOffset;

	m_uSPN++;
	}

// Implementation

UINT CPDU::GetMask(UINT uBits)
{
	UINT uMask = 0;

	uBits -= 1;

	MakeMin(uBits, 7);
	
	for( INT i = uBits; i >= 0; i-- ) {

		uMask |= (1 << i);
		}

	return uMask;
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Calibration Protocol Port Handler
//

// Constructor

CCCPPortHandler::CCCPPortHandler(CCANCalibrationMaster * pMaster, IHelper *pHelper)
{
	StdSetRef();

	m_pPort      = NULL;

	m_fClaim     = FALSE;

	m_fClaimSent = FALSE;

	m_pName      = NULL;
	
	m_uReq       = 0;

	m_uQueue     = 0;
	
	m_uSend      = 0;

	m_pHelper    = NULL;

	pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pHelper);
	}

// Destructor

CCCPPortHandler::~CCCPPortHandler(void)
{
	for( INDEX i = m_FrameData.GetHead(); !m_FrameData.Failed(i); m_FrameData.GetNext(i) ) {

		CFrameData * pFrameData = m_FrameData.GetData(i);

		delete pFrameData;
		}

	for( INDEX j = m_SlaveMap.GetHead(); !m_SlaveMap.Failed(j); m_SlaveMap.GetNext(j) ) {

		CSlaveData * pSlave = m_SlaveMap.GetData(j);

		delete pSlave;
		}

	delete m_pName;
	}

// IUnknown

HRESULT METHOD CCCPPortHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG METHOD CCCPPortHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG METHOD CCCPPortHandler::Release(void)
{
	StdRelease();
	}

// IPortHandler

void METHOD CCCPPortHandler::Bind(IPortObject * pPort)
{
	m_pPort = pPort;
	}

void METHOD CCCPPortHandler::OnOpen(CSerialConfig const &Config)
{
	m_uRxByte    = 0;

	m_pTxData    = NULL;
	
	m_uTxCount   = 0;

	m_pSender    = NULL;

	m_nTxDisable = 0;	
	}

void METHOD CCCPPortHandler::OnClose(void)
{
	for( INDEX i = m_FrameData.GetHead(); !m_FrameData.Failed(i); m_FrameData.GetNext(i) ) {

		CFrameData * pFrameData = m_FrameData.GetData(i);

		pFrameData->Reset();
		}
	}

void METHOD CCCPPortHandler::OnRxData(BYTE bData)
{
	if( m_uRxByte < sizeof(FRAME_EXT) ) {
		
		PBYTE pData = PBYTE(&m_RxData);
		
		pData[m_uRxByte++] = bData;
		}
	}

void METHOD CCCPPortHandler::OnRxDone(void)
{
	if( m_uRxByte == sizeof(FRAME_EXT) ) {

		FRAME_EXT Frame = m_RxData;

		SaveFrame(Frame);
		}

	m_uRxByte = 0;
	}

BOOL METHOD CCCPPortHandler::OnTxData(BYTE &bData)
{
	if( m_uTxCount ) {

		if( m_pTxData ) {

			m_uTxCount--;

			bData = *m_pTxData++;

			return TRUE;
			}
		}

	Increment(m_uSend);

	m_pTxData = NULL;

	return FALSE;
	}

void METHOD CCCPPortHandler::OnTxDone(void)
{
	if( !m_pTxData && !m_nTxDisable ) { 

		if( m_uSend != m_uQueue ) {

			SendFrame();
			}
		}
	}

void METHOD CCCPPortHandler::OnTimer(void)
{	
	if( !m_fClaim && m_fClaimSent ) {

		if( GetTickCount() >= m_uClaim + ToTicks(1250) ) {
			
			m_fClaim = TRUE;

			m_fClaimSent = FALSE;
			}
		}

	if( m_fClaim ) {

		if( GetTickCount() >+ m_uReq + ToTicks(100) ) {

			if( m_ReqQueue.GetCount() > 0 ) {

				CheckTimeouts();

				if( !HasPending() ) {

					CReq Req = m_ReqQueue.GetAt(0);

					if( SendPGNRequest(Req.m_pPDU, Req.m_bDA) ) {

						Req.m_pPDU->RequestSent();
						}
					}
				}

			m_uReq = GetTickCount();
			}
		}
	}

// Operations

BOOL CCCPPortHandler::GetCmdFrame(void * pCtx, FRAME_EXT &Frame, UINT uTime)
{
	INDEX i = m_FrameData.FindName(pCtx);

	if( !m_FrameData.Failed(i) ) {

		CFrameData * pFrameData = m_FrameData.GetData(i);

		return pFrameData->GetCmdFrame(Frame, uTime);
		}

	return FALSE;
	}

BOOL CCCPPortHandler::GetDaqFrame(void * pCtx, FRAME_EXT &Frame, UINT uTime)
{
	INDEX i = m_FrameData.FindName(pCtx);

	if( !m_FrameData.Failed(i) ) {

		CFrameData * pFrameData = m_FrameData.GetData(i);

		return pFrameData->GetDaqFrame(Frame, uTime);
		}

	return FALSE;
	}

BOOL CCCPPortHandler::GetEventFrame(void * pCtx, FRAME_EXT &Frame, UINT uTime)
{
	INDEX i = m_FrameData.FindName(pCtx);

	if( !m_FrameData.Failed(i) ) {

		CFrameData * pFrameData = m_FrameData.GetData(i);

		return pFrameData->GetEventFrame(Frame, uTime);
		}

	return FALSE;
	}

BOOL CCCPPortHandler::PutFrame(void * pCtx, FRAME_EXT &Frame, UINT uTime)
{
	AfxRaiseIRQL(IRQL_HARDWARE + ExpGetIPL(0, 4));

	UINT uQueue = m_uQueue;
	
	Increment(m_uQueue);
	
	if( m_uQueue != m_uSend ) {
		
		m_nTxDisable++;

		AfxLowerIRQL();
		
		memcpy(&m_Send[uQueue], PBYTE(&Frame), sizeof(FRAME_EXT));

		if( pCtx ) {
			
			m_pSender = pCtx;
			}
		
		AfxRaiseIRQL(IRQL_HARDWARE + ExpGetIPL(0, 4));

		if( !--m_nTxDisable && !m_pTxData ) { 
			
			SendFrame();
			}
		
		AfxLowerIRQL();
		
		return TRUE;
		}
	
	m_uQueue = uQueue;

	AfxLowerIRQL();
	
	return FALSE;
	}

// J1939 Operations

void CCCPPortHandler::SetSA(BYTE bSA)
{
	m_bSrc = bSA;

	if( !m_pName ) {

		m_pName = new NAME;

		memset(PBYTE(m_pName), 0, sizeof(NAME));

		m_pName->a.m_Arb  = 0;
	       
		m_pName->a.m_IG   = 0;

		m_pName->a.m_VSI  = 0;

		m_pName->a.m_VS   = 0x7F;

		m_pName->a.m_R    = 0;

		m_pName->a.m_F    = 0xFF;

		m_pName->a.m_FI   = 0;

		m_pName->a.m_ECUI = 0;

		m_pName->b.m_MC   = 267; 

		CMacAddr Mac;

		m_pHelper->GetMacId(0, Mac.m_Addr);

		m_pName->b.m_ID = MotorToHost((DWORD &) Mac.m_Addr[2]);  
		}

	switch( m_bSrc ) {

		case 38:
			m_pName->a.m_F = 29;
			break;
		case 40:
			m_pName->a.m_F = 60;
			break;
		case 251:
			m_pName->a.m_F = 130;
			break;

		default:
			m_pName->a.m_F = 0xFF;
		} 
	}

UINT CCCPPortHandler::DiagRead(BYTE bSlave, UINT uPGN, UINT uSPN, PDWORD pData, UINT uCount)
{
	CPDU * pFind = NULL;

	INDEX i = m_SlaveMap.FindName(bSlave);

	if( !m_SlaveMap.Failed(i) ) {

		CSlaveData * pSlave = m_SlaveMap.GetData(i);

		for( INDEX j = pSlave->m_List.GetHead(); !pSlave->m_List.Failed(j); pSlave->m_List.GetNext(j) ) {

			CPDU * pPDU = pSlave->m_List.GetAt(j);

			if( pPDU->m_uPGN == uPGN ) {

				pFind = pPDU;

				break;
				}
			}

		if( pFind ) {

			if( pFind->ShouldSendRequests() && pFind->IsDataStale() ) {

				QueuePGNRequest(pFind, bSlave);
				}
			
			DWORD dwData = 0;

			if( pFind->ReadData(uSPN, dwData) ) {
				
				*pData = dwData;

				return codeOk;
				}

			else if( pFind->IsValid() ) {

				return codeSoft;
				}
			}
		}

	return codeHard;
	}

UINT CCCPPortHandler::DiagCmd(BYTE bSlave, CPDU * pPDU, PDWORD pData, UINT uCount)
{
	if( pPDU ) {

		if( SendPGNRequest(pPDU, bSlave) ) {

			return 1;
			}
		}
	
	return 0;
	}

// Configuration

void CCCPPortHandler::AddContext(void * pCtx)
{
	CFrameData *pFrameData = new CFrameData;

	m_FrameData.Insert(pCtx, pFrameData);
	}

void CCCPPortHandler::AddId(void * pCtx, DWORD dwId)
{
	INDEX i = m_FrameData.FindName(pCtx);
	
	if( !m_FrameData.Failed(i) ) {

		CFrameData * pFrameData = m_FrameData.GetData(i);

		pFrameData->AddId(dwId);
		}
	}

void CCCPPortHandler::AddPDU(BYTE bSlave, CPDU * pPdu)
{
	INDEX i = m_SlaveMap.FindName(bSlave);

	CSlaveData *pSlave = NULL;

	if( m_SlaveMap.Failed(i) ) {

		pSlave = new CSlaveData;

		pSlave->m_bAddress = bSlave;

		m_SlaveMap.Insert(bSlave, pSlave);
		}
	else {
		INDEX j = m_SlaveMap.FindName(bSlave);

		pSlave = m_SlaveMap.GetData(j);
		}

	pSlave->m_List.Append(pPdu);
	}

bool CCCPPortHandler::CReq::operator == (CReq const &That)
{
	return m_pPDU->m_uPGN == That.m_pPDU->m_uPGN && m_bDA == That.m_bDA;
	}

void CCCPPortHandler::SaveFrame(FRAME_EXT const &Frame)
{
	if( IsJ1939(Frame) && IsThisNode(Frame) ) {

		HandleJ1939(Frame);
		}

	for( INDEX i = m_FrameData.GetHead(); !m_FrameData.Failed(i); m_FrameData.GetNext(i) ) {

		CFrameData * pFrameData = m_FrameData.GetData(i);

		if( pFrameData->HasId(Frame.m_ID) ) {

			void * pCtx = m_FrameData.GetName(i);

			BYTE bID    = Frame.m_bData[0];

			BOOL fKeep  = (m_pSender == pCtx) && (bID == CFrameData::idCmd);

			if( bID < CFrameData::idCmd || fKeep ) {
							
				pFrameData->StoreFrame(Frame);

				if( bID == CFrameData::idCmd ) {

					m_pSender = NULL;
					}				
				}
			}
		}
	}

BOOL CCCPPortHandler::IsThisNode(FRAME_EXT const &Frame)
{
	ID id;
	
	id.m_Ref = Frame.m_ID;

	return ((id.i.m_PS == m_bSrc) || (id.i.m_PF >= 0xF0) || (id.i.m_PS == 0xFF));
	}

BOOL CCCPPortHandler::IsJ1939(FRAME_EXT const &Frame)
{
	return !((Frame.m_ID >> 25) & 0x1);
	}

BOOL CCCPPortHandler::IsDM(UINT uPGN)
{
	switch( uPGN ) {

		case 65226:
		case 65227:
		case 65228:
		case 65229:
		case 65235:

			return TRUE;
		}

	return FALSE;
	}

void CCCPPortHandler::Increment(UINT volatile &uIndex)
{
	uIndex = (uIndex + 1) % elements(m_Send);
	}

void CCCPPortHandler::SendFrame(void)
{
	m_pTxData  = PCBYTE(&m_Send[m_uSend]);

	m_uTxCount = sizeof(FRAME_EXT) - 1;

	m_pPort->Send(*m_pTxData++);
	}

// J1939 Implementation

void CCCPPortHandler::HandleJ1939(FRAME_EXT const &Frame)
{
	ID id;
		
	id.m_Ref = Frame.m_ID;

	UINT uPGN = FindPGN(Frame);
	
	if( IsDM(uPGN) ) {
					
		HandleData(uPGN, Frame);

		return;
		}

	if( IsRequestPGN(Frame) ) {

		if( IsAddrClaimedReq(Frame) ) {

			SourceAddrClaim();
			}
		else {
			SendNack(id);
			}

		return;
		}

	if( IsTransportPGN(Frame) ) {
				
		HandleTransport(Frame);

		return;
		}

	if( IsTransferPGN(Frame) ) {

		HandleTransfer(Frame);

		return;
		}

	if( IsAddrClaimed(Frame) ) {

		BOOL fClaim = HandleClaimAddress(Frame);

		if( !fClaim ) {

			ID id;

			id.m_Ref = Frame.m_ID;

			SendNack(id);
			}

		return;
		}
	}

BOOL CCCPPortHandler::SendAck(ID id)
{
	ID ack;

	MakeId(ack, PGN_ACK, 6);

	FRAME_EXT Frame;

	InitFrame(Frame);

	Frame.m_ID   = ack.m_Ref;

	Frame.m_Ctrl = 8;

	UINT uPos = 0;

	AddByte(Frame, 0, uPos);

	AddByte(Frame, 255, uPos);
	AddByte(Frame, 255, uPos);
	AddByte(Frame, 255, uPos);

	AddByte(Frame, id.i.m_SA , uPos);

	DWORD dwID = (id.m_Ref & 0x3FFFFFF) >> 8;

	AddJ1939Id(Frame, dwID, uPos);
	
	return PutFrame(NULL, Frame, 1000);
	}

BOOL CCCPPortHandler::SendNack(ID id)
{
	ID nack;

	MakeId(nack, PGN_ACK, 6);

	FRAME_EXT Frame;

	InitFrame(Frame);

	Frame.m_ID   = nack.m_Ref;

	Frame.m_Ctrl = 8;

	UINT uPos = 0;

	AddByte(Frame, 1, uPos);

	AddByte(Frame, 255, uPos);
	AddByte(Frame, 255, uPos);
	AddByte(Frame, 255, uPos);

	AddByte(Frame, id.i.m_SA , uPos);

	DWORD dwID = (id.m_Ref & 0x3FFFFFF) >> 8;

	AddJ1939Id(Frame, dwID, uPos);
	
	return PutFrame(NULL, Frame, 1000);
	}

// Network Management

BOOL CCCPPortHandler::ClaimAddress(void)
{
	if( !m_fClaim && !m_fClaimSent ) {

		return SourceAddrClaim();
		}

	return FALSE;
	}

BOOL CCCPPortHandler::SourceAddrClaim(void)
{
	FRAME_EXT Frame;

	ID id;
	
	MakeId(id, CLAIM_RESP, 6);

	Frame.m_ID   = id.m_Ref;
	
	Frame.m_Ctrl = 8;

	AddName(Frame);

	m_fClaimSent = PutFrame(NULL, Frame, 1000);

	if( m_fClaimSent ) {

		m_uClaim = GetTickCount();
		}

	return m_fClaimSent;
	}

BOOL CCCPPortHandler::IsAck(UINT uPGN)
{
	return uPGN == PGN_ACK;
	}

BOOL CCCPPortHandler::IsTransport(UINT uPGN)
{
	return uPGN == PGN_TRANSPORT;
	}

BOOL CCCPPortHandler::IsAddrClaimedReq(FRAME_EXT const &Frame)
{
	ID id;

	id.m_Ref = Frame.m_ID;

	UINT uID = id.i.m_DP << 16;

	uID |= id.i.m_PF << 8;

	if( uID == CLAIM_REQ ) {

		if( Frame.m_Ctrl == 3 ) {

			UINT uPGN = Frame.m_bData[0];

			uPGN     |= Frame.m_bData[1] << 8;

			uPGN	 |= Frame.m_bData[2] << 16;

			if( uPGN == CLAIM_RESP ) {

				return TRUE;
				}
			}
		}
	
	return FALSE;
	}

// J1939 Network Management

BOOL CCCPPortHandler::CannotClaimAddr(void)
{
	ID id;
	
	MakeId(id, CLAIM_RESP, 6);
	
	id.i.m_SA = 254;

	int Random = rand() % 153;

	if( CanTaskWait() ) {

		Sleep(Random);
		}

	FRAME_EXT Frame;

	InitFrame(Frame);

	Frame.m_ID   = id.m_Ref;

	Frame.m_Ctrl = 8;

	AddName(Frame);

	m_fClaim     = FALSE;

	m_fClaimSent = FALSE;
	
	return PutFrame(NULL, Frame, 1000);
	}

BOOL CCCPPortHandler::HandleClaimAddress(FRAME_EXT const &Frame)
{
	ID rx;

	rx.m_Ref = Frame.m_ID;
	
	if( IsAddrClaimed(Frame) ) {

		if( rx.i.m_SA == m_bSrc ) {

			PBYTE Name = PBYTE(m_pName);

			for( UINT u = 7; u > 0; u-- ) {

				if( Name[u] < Frame.m_bData[u] ) {

					SourceAddrClaim();

					return TRUE;
					}

				if( Name[u] > Frame.m_bData[u] ) {

					if( m_fClaim || m_fClaimSent ) {

						CannotClaimAddr();
						}

					return TRUE;
					}
				}
			}
	
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCCPPortHandler::IsAddrClaimed(FRAME_EXT const &Frame)
{
	ID id;

	id.m_Ref = Frame.m_ID;

	UINT uID = id.i.m_DP << 16;

	uID |= id.i.m_PF << 8;

	if( uID == CLAIM_RESP ) {

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCCPPortHandler::IsDestinationSpecific(ID id)
{
	return id.i.m_PF < 0xF0;
       	}

BOOL CCCPPortHandler::IsDestinationGlobal(ID id)
{
	return id.i.m_PF == 0xFF;
	}

BOOL CCCPPortHandler::IsGroupExtension(ID id)
{
	return id.i.m_PF >> 4 == 0xF; 
	}

BOOL CCCPPortHandler::IsRequestPGN(FRAME_EXT const &Frame)
{	
	return (((Frame.m_ID & 0x1FFFF00) >> 8) == UINT(PGN_REQ | m_bSrc));
	}

BOOL CCCPPortHandler::IsTransferPGN(FRAME_EXT const &Frame)
{	
	return (((Frame.m_ID & 0x1FF0000) >> 8) == PGN_TRANSFER);
	}

BOOL CCCPPortHandler::IsTransportPGN(FRAME_EXT const &Frame)
{	
	return (((Frame.m_ID & 0x1FF0000) >> 8) == PGN_TRANSPORT);
	}

BOOL CCCPPortHandler::IsRTS(FRAME_EXT const &Frame)
{
	return Frame.m_bData[0] == 16;
	}

BOOL CCCPPortHandler::IsCTS(FRAME_EXT const &Frame)
{
	return Frame.m_bData[0] == 17;
	}

BOOL CCCPPortHandler::IsBAM(FRAME_EXT const &Frame)
{
	return Frame.m_bData[0] == 32;
	}

BOOL CCCPPortHandler::IsEndOfMsg(FRAME_EXT const &Frame)
{
	return Frame.m_bData[0] == 19;
	}

BOOL CCCPPortHandler::IsAbort(FRAME_EXT const &Frame)
{
	return Frame.m_bData[0] == 255;
   	}

BOOL CCCPPortHandler::IsDataTransfer(FRAME_EXT const &Frame)
{
	ID id;
	
	id.m_Ref = Frame.m_ID;

	return id.i.m_PF == HIBYTE(PGN_TRANSFER);
	}

BOOL CCCPPortHandler::IsBroadcast(ID id)
{	
	return id.i.m_PF < 0xF0 && id.i.m_PS == 0xFF;
	}

void CCCPPortHandler::MakeId(ID &id, UINT uID, UINT uPriority)
{
	id.i.m_X   = 0;
	
	id.i.m_P   = uPriority;

	id.i.m_EDP = 0;

	id.i.m_DP  = (uID & 0x10000) >> 16;

	id.i.m_PF  = (uID & 0x0FF00) >> 8;

	id.i.m_PS  = 0xFF;

	id.i.m_SA  = m_bSrc;
	}

UINT CCCPPortHandler::FindPGN(FRAME_EXT const &Frame)
{
	ID id;
	
	id.m_Ref = Frame.m_ID;

	return FindPGN(id);
	}

UINT CCCPPortHandler::FindPGN(ID const &id)
{
	return (id.m_Ref & 0x00FFFF00) >> 8;
	}

void CCCPPortHandler::StoreData(CSlaveData *pSlave, FRAME_EXT const &Frame, BOOL fTransfer, UINT uPGN)
{	
	for( INDEX j = pSlave->m_List.GetHead(); !pSlave->m_List.Failed(j); pSlave->m_List.GetNext(j) ) {

		ID id;

		id.m_Ref = Frame.m_ID;

		BYTE bPktNum = fTransfer ? Frame.m_bData[0] : 1;

		CPDU * pPDU = pSlave->m_List.GetAt(j);

		UINT uTarget = uPGN ? uPGN : pSlave->m_uPGN;
		
		if( pPDU->m_uPGN == uTarget ) {

			if( bPktNum == 1 ) {

				pPDU->StartData();
				}

			if( fTransfer ) {

				pPDU->StoreData(Frame.m_bData + 1, 7 * (bPktNum - 1), 7);

				if( bPktNum == pSlave->m_uPackets ) {

					pPDU->FinishData();

					pSlave->m_uPackets = 0;

					RemovePGNRequest(pPDU, id.i.m_SA);
					}
				}
			else {				
				pPDU->StoreData(Frame, 0);
				
				pPDU->FinishData();
				
				RemovePGNRequest(pPDU, id.i.m_SA);
				}

			break;
			}
		}
	}

BOOL CCCPPortHandler::HasPending(void)
{
	for( UINT n = 0; n < m_ReqQueue.GetCount(); n++ ) {

		if( m_ReqQueue[n].m_pPDU->IsPending() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CCCPPortHandler::CheckTimeouts(void)
{	
	for( UINT n = 0; n < m_ReqQueue.GetCount(); n++ ) {

		if( m_ReqQueue[n].m_pPDU->IsTimedOut() ) {

			m_ReqQueue[n].m_pPDU->Reset();
			}
		}
	}

BOOL CCCPPortHandler::QueuePGNRequest(CPDU * pPDU, BYTE bDA)
{
	CReq Req;

	Req.m_bDA  = bDA;
	Req.m_pPDU = pPDU;

	UINT uPos = m_ReqQueue.Find(Req);

	if( uPos == NOTHING ) {
						
		m_ReqQueue.Append(Req);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCCPPortHandler::RemovePGNRequest(CPDU * pPDU, BYTE bDA)
{
	CReq Req;

	Req.m_bDA  = bDA;
	Req.m_pPDU = pPDU;

	UINT uPos = m_ReqQueue.Find(Req);

	if( uPos != NOTHING ) {

		m_ReqQueue.Remove(uPos);

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCCPPortHandler::SendPGNRequest(CPDU * pPDU, BYTE bDA)
{
	if( m_fClaim ) {
							
		ID id;

		FRAME_EXT Frame;

		InitFrame(Frame);

		MakeId(id, PGN_REQ, pPDU->GetPriority());

		id.i.m_PS    = bDA;      

		Frame.m_ID   = id.m_Ref;

		Frame.m_Ctrl = 3;

		UINT uPos = 0;

		AddPGN(Frame, pPDU->m_uPGN, uPos);
				
		return PutFrame(NULL, Frame, 1000);
		}

	return FALSE;
	}

// J1939 Transport

void CCCPPortHandler::ClearToSend(BYTE bDest, BYTE bPackets, UINT uPGN)
{
	INDEX i = m_SlaveMap.FindName(bDest);

	if( !m_SlaveMap.Failed(i) ) {

		CSlaveData *pSlave = m_SlaveMap.GetData(i);

		pSlave->m_uPGN = uPGN;

		FRAME_EXT Frame;

		InitFrame(Frame);

		Frame.m_Ctrl = 8;

		ID id;

		MakeId(id, PGN_TRANSPORT, 7);

		id.i.m_PS = bDest;

		Frame.m_ID = id.m_Ref;

		BYTE bStart = 1;

		UINT uPos = 0;

		AddByte(Frame, 17, uPos);

		AddByte(Frame, bPackets, uPos);

		AddByte(Frame, bStart, uPos); 

		AddByte(Frame, 0xFF, uPos);

		AddByte(Frame, 0xFF, uPos);

		AddPGN(Frame, uPGN, uPos);
	
		PutFrame(NULL, Frame, 1000);
		}
	}

void CCCPPortHandler::HandleTransport(FRAME_EXT const &Frame)
{
	if( IsRTS(Frame) ) {

		ID id;

		id.m_Ref = Frame.m_ID;

		BYTE bPackets = Frame.m_bData[3];

		UINT uSize = Frame.m_bData[2] <<  8 | Frame.m_bData[1];

		UINT uPGN  = Frame.m_bData[7] << 16 | Frame.m_bData[6] << 8 | Frame.m_bData[5];

		UINT uMax  = Frame.m_bData[4];

		BYTE bSA = id.i.m_SA;

		ClearToSend(bSA, bPackets, uPGN);

		INDEX i = m_SlaveMap.FindName(bSA);

		if( !m_SlaveMap.Failed(i) ) {

			CSlaveData *pSlave = m_SlaveMap.GetData(i);

			pSlave->m_uPackets = bPackets;
			}		
		}

	if( IsBAM(Frame) ) {
		
		ID id;
		
		id.m_Ref  = Frame.m_ID;

		UINT uPackets = Frame.m_bData[3];

		UINT uSize = Frame.m_bData[2] <<  8 | Frame.m_bData[1];

		UINT uPGN  = Frame.m_bData[7] << 16 | Frame.m_bData[6] << 8 | Frame.m_bData[5];

		INDEX i = m_SlaveMap.FindName(id.i.m_SA);

		if( !m_SlaveMap.Failed(i) ) {
						
			CSlaveData *pSlave = m_SlaveMap.GetData(i);

			pSlave->m_uPackets = uPackets;

			pSlave->m_uPGN = uPGN;
			}
		}
	}

void CCCPPortHandler::HandleTransfer(FRAME_EXT const &Frame)
{	
	ID id;

	id.m_Ref = Frame.m_ID;

	INDEX i = m_SlaveMap.FindName(id.i.m_SA);	
		
	if( !m_SlaveMap.Failed(i) ) {

		CSlaveData *pSlave = m_SlaveMap.GetData(i);
		
		StoreData(pSlave, Frame, TRUE);
		}
	}

void CCCPPortHandler::HandleData(UINT uPGN, FRAME_EXT const &Frame)
{
	ID id;

	id.m_Ref = Frame.m_ID;

	INDEX i = m_SlaveMap.FindName(id.i.m_SA);
		
	if( !m_SlaveMap.Failed(i) ) {
		
		CSlaveData *pSlave = m_SlaveMap.GetData(i);
		
		StoreData(pSlave, Frame, FALSE, uPGN);
		}
	}

// J1939 Frame Helpers

void CCCPPortHandler::InitFrame(FRAME_EXT &Frame)
{
	memset(Frame.m_bData, 0, 8);
	}

void CCCPPortHandler::AddByte(FRAME_EXT &Frame, BYTE bData, UINT &uPos)
{
	Frame.m_bData[uPos++] = bData;
	}

void CCCPPortHandler::AddWord(FRAME_EXT &Frame, WORD wData, UINT &uPos)
{
	AddByte(Frame, LOBYTE(wData), uPos);

	AddByte(Frame, HIBYTE(wData), uPos);
	}

void CCCPPortHandler::AddLong(FRAME_EXT &Frame, DWORD dwData, UINT &uPos)
{	
	AddWord(Frame, LOWORD(dwData), uPos);

	AddWord(Frame, HIWORD(dwData), uPos);
	}

void CCCPPortHandler::AddJ1939Id(FRAME_EXT &Frame, DWORD dwId, UINT &uPos)
{
	AddByte(Frame, LOBYTE(LOWORD(dwId)), uPos);

	AddByte(Frame, HIBYTE(LOWORD(dwId)), uPos);

	AddByte(Frame, LOBYTE(HIWORD(dwId)), uPos);
	}

void CCCPPortHandler::AddPGN(FRAME_EXT &Frame, UINT uPGN, UINT &uPos)
{
	AddByte(Frame, LOBYTE(LOWORD(uPGN)), uPos);

	AddByte(Frame, HIBYTE(LOWORD(uPGN)), uPos);

	AddByte(Frame, LOBYTE(HIWORD(uPGN)), uPos);
	}

void CCCPPortHandler::AddName(FRAME_EXT &Frame)
{
	if( m_pName ) {

		DWORD dwHi = m_pName->Hi;

		DWORD dwLo = m_pName->Lo;

		UINT uPos = 0;

		AddWord(Frame, LOWORD(dwLo), uPos);

		AddWord(Frame, HIWORD(dwLo), uPos);

		AddWord(Frame, LOWORD(dwHi), uPos);

		AddWord(Frame, HIWORD(dwHi), uPos);
		}
	}

// End of File
