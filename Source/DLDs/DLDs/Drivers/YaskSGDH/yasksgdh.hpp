
//////////////////////////////////////////////////////////////////////////
//
// Yaskawa SGDH Servo Amplifier
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#define NOTWRITE	FALSE
#define ISWRITE		TRUE

class CYaskSGDHDriver : public CMasterDriver {

	public:
		// Constructor
		CYaskSGDHDriver(void);

		// Destructor
		~CYaskSGDHDriver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
			
		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);
		CCODE MCALL Read(AREF Addr, PDWORD pData, UINT uCount);
		CCODE MCALL Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDropText;
			UINT m_uPowerUpDelay;
			WORD m_Ping;
			};
		CContext *	m_pCtx;

		// Data Members
		LPCTXT	m_pHex;

		BYTE	m_bTx[16];
		BYTE	m_bRx[16];
		UINT	m_uPtr;
		UINT	m_uCount;
		
		// Implementation

		void PutRead(UINT uOffset);
		void PutWrite(UINT uOffset, DWORD dData);

		// Data Link Layer
		void StartFrame( BOOL fIsWrite );
		void SendFrame( void );
		BOOL GetFrame( void );
		BOOL CheckReply(void);
		BOOL Transact( void );

		// Port Access
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);

		// Helpers
		void AddByte( BYTE bData );
		DWORD xtoin(PBYTE pText, UINT uCount);
		void PutGeneric(DWORD dData, UINT uRadix, UINT uFactor);
		BYTE GetCheck( PBYTE p );
	};

// End of File
