
#include "intern.hpp"

#include "yasksgdh.hpp"

#define YDEBUG FALSE

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa SGDH Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CYaskSGDHDriver);

// Constructor

CYaskSGDHDriver::CYaskSGDHDriver(void)
{
	m_Ident	    = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex	    = Hex;
	}

// Destructor

CYaskSGDHDriver::~CYaskSGDHDriver(void)
{
	}

// Configuration

void MCALL CYaskSGDHDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CYaskSGDHDriver::CheckConfig(CSerialConfig &Config)
{
	if ( Config.m_uPhysical == 3 ) Make485(Config, TRUE);
	}
	
// Management

void MCALL CYaskSGDHDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CYaskSGDHDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CYaskSGDHDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			BYTE bDrop = GetByte(pData);

			m_pCtx->m_bDropText = '0';

			if( bDrop < 20 ) {

				m_pCtx->m_bDropText = '0' + (bDrop>>1);
				}

			else {
				if( bDrop < 33 ) {

					m_pCtx->m_bDropText = 'A' + (bDrop>>1) - 10;
					}
				}

			m_pCtx->m_uPowerUpDelay = 1;

			m_pCtx->m_Ping = GetWord(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS; 
	}

CCODE MCALL CYaskSGDHDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskSGDHDriver::Ping(void)
{
	DWORD	Data[1];
	CCODE	Err;

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = m_pCtx->m_Ping;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	if( m_pCtx->m_uPowerUpDelay ) {

		m_pCtx->m_uPowerUpDelay = (m_pCtx->m_uPowerUpDelay + 1) % 8;

		return CCODE_ERROR;
		}

	Err = Read( Addr, Data, 1);

	if( Err != 1 ) {

		m_pCtx->m_uPowerUpDelay = 1;

		return CCODE_ERROR;
		}

	return 1;
	}

CCODE MCALL CYaskSGDHDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	PutRead(Addr.a.m_Offset);

	if( Transact() ) {

		pData[0] = xtoin( &m_bRx[6], 4 );

		return 1;
		}
			
	return CCODE_ERROR;
	}

CCODE MCALL CYaskSGDHDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	PutWrite(Addr.a.m_Offset, pData[0]);

	if( Transact() ) {
			
		return 1;
		}
			
	return CCODE_ERROR;
	}

void CYaskSGDHDriver::PutRead(UINT uOffset)
{
	StartFrame(NOTWRITE);

	PutGeneric( uOffset, 16, 0x1000 );

	PutGeneric( 0, 16, 0x1000 );
	}

void CYaskSGDHDriver::PutWrite(UINT uOffset, DWORD dData)
{
	StartFrame(ISWRITE);

	PutGeneric( uOffset, 16, 0x1000 );

	PutGeneric(LOWORD(dData), 16, 0x1000);
	}

// Data Link Layer

void CYaskSGDHDriver::StartFrame(BOOL fIsWrite)
{
	m_bTx[0] = 'W';

	m_uPtr = 1;

	AddByte(m_pCtx->m_bDropText);

	AddByte( fIsWrite ? '1' : '0' );
	}
	
void CYaskSGDHDriver::SendFrame(void)
{
	BYTE bCheck;

	m_pData->ClearRx();

//	if( YDEBUG ) { for(UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);}

	bCheck = GetCheck( &m_bTx[1] );

	m_bTx[m_uPtr++] = m_pHex[bCheck / 16];
	m_bTx[m_uPtr++] = m_pHex[bCheck % 16];
	m_bTx[m_uPtr++] = CR;

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

BOOL CYaskSGDHDriver::GetFrame(void)
{
	UINT uState	= 0;

	UINT uData	= 0;

	UINT uTimer	= 0;
	
	UINT uCount	= 0;
	
	BYTE bData;

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uData = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

//		if( YDEBUG ) AfxTrace1("<%2.2x>", bData);

		switch( uState ) {
		
			case 0:
				if( bData == 'W' ) {

					uState = 1;

					uCount = 0;
					}
				break;
				
			case 1:
				if( bData == CR ) {
					
					return CheckReply();
					}
				
				m_bRx[uCount++] = bData;
				
				if( uCount >= sizeof(m_bRx) ) {
					
					return FALSE;
					}
					
				break;
			}
		}
		
	return FALSE;
	}
	

BOOL CYaskSGDHDriver::CheckReply(void)
{
	BYTE bCheck = 0;

	for( UINT i = 0; i < 6; i++ ) {

		if( m_bRx[i] != m_bTx[i+1] ) {

			return FALSE;
			}
		}

	bCheck = GetCheck( &m_bRx[0] );

	if( m_bRx[10] == m_pHex[bCheck/16] && m_bRx[11] == m_pHex[bCheck%16] )

		return TRUE;

	return FALSE;
	}
	
BOOL CYaskSGDHDriver::Transact(void)
{
	m_pData->ClearRx();

//	if( YDEBUG ) AfxTrace0("\r\n");

	SendFrame();

//	if( YDEBUG ) AfxTrace0("\r\n");
				
	return GetFrame();
	}

// Port Access

void CYaskSGDHDriver::TxByte( BYTE bData )
{
	m_pData->Write(bData, FOREVER);

//	if( YDEBUG ) AfxTrace1("[%2.2x]", bData );
	}

UINT CYaskSGDHDriver::RxByte( UINT uTime )
{
	return m_pData->Read(uTime);
	}

// Helpers

void CYaskSGDHDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

DWORD CYaskSGDHDriver::xtoin(PBYTE pText, UINT uCount)
{
	DWORD dData = 0;
	
	while( uCount-- ) {
	
		char cData = *(pText++);
		
		if( cData >= '0' && cData <= '9' )
			dData = 16 * dData + cData - '0';

		else if( cData >= 'A' && cData <= 'F' )
			dData = 16 * dData + cData - 'A' + 10;

		else if( cData >= 'a' && cData <= 'f' )
			dData = 16 * dData + cData - 'a' + 10;

		else break;
		}

	return dData;
	}

void CYaskSGDHDriver::PutGeneric(DWORD dData, UINT uRadix, UINT uFactor)
{
	while( uFactor ) {
	
		AddByte(m_pHex[(dData / uFactor) % uRadix]);

		uFactor /= uRadix;
		}
	}

BYTE CYaskSGDHDriver::GetCheck( PBYTE p )
{
	DWORD d = 0;

	for( UINT i = 0; i < 10; i += 2 ) {

		d += xtoin( &p[i], 2 );
		}

	return 0xFF & ( 0x100 - LOBYTE(d) );
	}

// End of File
