
#include "intern.hpp"

#include "ciabase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAN Port Handler
//

// Constructor

CCANPortHandler::CCANPortHandler(void)
{	
	StdSetRef();

	m_pPort   = NULL;

	m_pTxFlag = Create_ManualEvent();

	m_pRxFlag = Create_Semaphore(0);

	m_uMode = 0;
	}

// Destructor

CCANPortHandler::~CCANPortHandler(void)
{
	}

// IUnknown

HRESULT METHOD CCANPortHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG METHOD CCANPortHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG METHOD CCANPortHandler::Release(void)
{
	StdRelease();
	}

// IPortHandler

void METHOD CCANPortHandler::Bind(IPortObject * pPort)
{
	m_pPort = pPort;
	}

void METHOD CCANPortHandler::OnOpen(CSerialConfig const &Config)
{
	m_uRxHead  = 0;

	m_uRxTail  = 0;

	m_uRxByte  = 0;

	m_pTxData  = NULL;
	
	m_uTxCount = 0;

	m_pTxFlag->Set();
	}

void METHOD CCANPortHandler::OnClose(void)
{
	m_pTxFlag->Wait(FOREVER);

	while( m_pRxFlag->Wait(0) );
	}

void METHOD CCANPortHandler::OnRxData(BYTE bData)
{
	if( m_uRxByte < sizeof(FRAME) ) {

		PBYTE pData = PBYTE(m_RxData + m_uRxTail);
		
		pData[m_uRxByte++] = bData;
		}
	}

void METHOD CCANPortHandler::OnRxDone(void)
{
	if( m_uRxByte == sizeof(FRAME) ) {

		UINT uNext = (m_uRxTail + 1) % elements(m_RxData);

		if( uNext != m_uRxHead ) {

			m_uRxTail = uNext;

			m_pRxFlag->Signal(1);
			}
		}

	m_uRxByte = 0;
	}

BOOL METHOD CCANPortHandler::OnTxData(BYTE &bData)
{
	if( m_uTxCount ) {

		m_uTxCount--;

		bData = *m_pTxData++;

		return TRUE;
		}

	m_pTxFlag->Set();

	return FALSE;
	}

void METHOD CCANPortHandler::OnTxDone(void)
{
	}

void METHOD CCANPortHandler::OnTimer(void)
{
	}

// Operations

BOOL CCANPortHandler::GetFrame(FRAME &Frame, UINT uTime)
{
	if( m_pRxFlag->Wait(uTime) ) {

		Frame     = m_RxData[m_uRxHead];

		m_uRxHead = (m_uRxHead + 1) % elements(m_RxData);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANPortHandler::PutFrame(FRAME &Frame, UINT uTime)
{
	if( m_pTxFlag->Wait(uTime) ) {

		m_pTxData  = PCBYTE(&Frame);

		m_uTxCount = sizeof(Frame) - 1;

		GuardTask(TRUE);

		m_pTxFlag->Clear();

		m_pPort->Send(*m_pTxData++);

		GuardTask(FALSE);

		return TRUE;
		}

	return FALSE;
	}

// End of File

