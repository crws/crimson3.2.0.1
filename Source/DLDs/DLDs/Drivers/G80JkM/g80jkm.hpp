/////////////////////////////////////////////////////////////////////////
//
// Constants
//
 
#define	RX_ERROR	0x00
#define	RX_FRAME	0x01
#define	RX_ACK		0x02
#define	RX_NAK		0x03
#define TX_SLAVES	16

//////////////////////////////////////////////////////////////////////////
//
// Backup Structure
//

struct BACKUP {

	BYTE  m_bDrop;
	PBYTE m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 JK Master Serial Driver
//

class CGem80JKMasterSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CGem80JKMasterSerialDriver(void);

		// Destructor
		~CGem80JKMasterSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext 
		{
			BYTE  m_bDrop;
			BYTE  m_fToggle;
			UINT  m_uCount;
			PBYTE m_pData;
			BYTE  m_Ping;
			UINT  m_uTimeout;
			BYTE  m_Monitor;
			};

		// Data Members
		CContext * m_pCtx;
		CRC16	   m_CRC;
		UINT	   m_uPtr;
		BYTE       m_bTerm;
		BYTE       m_bRxBuff[400];
		BYTE       m_bTxBuff[400];
		UINT	   m_uRxSize;
		UINT       m_uTxSize;
		BACKUP *   m_pBackup[TX_SLAVES];
				
		// Implementation 
		void Start(void);
		BOOL Exchange(BOOL fMonitor);
		void TxPacket(BYTE bMode, BOOL fMonitor);
		UINT RxPacket(void);
		void Send(BYTE bData);
		void GetBackupData(void); 
		void SetBackupData(void);

		// Frame Building
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);

		// Read Handlers
		CCODE DoWordRead(PDWORD pData, UINT uCount, UINT uOffset);
		CCODE DoLongRead(PDWORD pData, UINT uCount, UINT uOffset);

		// Write Handlers
		CCODE DoWordWrite(PDWORD pData, UINT uCount, UINT uOffset);
		CCODE DoLongWrite(PDWORD pData, UINT uCount, UINT uOffset);
       		
	};

// End of File
