
#include "intern.hpp"

#include "g80jkm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 PLC Driver
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//


// Instantiator

INSTANTIATE(CGem80JKMasterSerialDriver);

// Constructor

CGem80JKMasterSerialDriver::CGem80JKMasterSerialDriver(void)
{
	m_uRxSize = sizeof(m_bRxBuff);

	m_uTxSize = sizeof(m_bTxBuff);

	memset(m_bTxBuff, 0, m_uTxSize);

	memset(m_bRxBuff, 0, m_uRxSize);

	m_Ident         = DRIVER_ID;

	for( UINT u = 0; u < TX_SLAVES; u++ ) {

		m_pBackup[u] = NULL;
	}
}

// Destructor

CGem80JKMasterSerialDriver::~CGem80JKMasterSerialDriver(void)
{
	for( UINT u = 0; u < TX_SLAVES; u++ ) {

		if( m_pBackup[u] ) {

			delete[] m_pBackup[u]->m_pData;

			delete m_pBackup[u];
		}
	}
}

// Configuration

void MCALL CGem80JKMasterSerialDriver::Load(LPCBYTE pData)
{
}

void MCALL CGem80JKMasterSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
}

// Management

void MCALL CGem80JKMasterSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
}

void MCALL CGem80JKMasterSerialDriver::Open(void)
{

}

// Device

CCODE MCALL CGem80JKMasterSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pCtx->m_bDrop	 = GetByte(pData);

			BYTE bCount = GetByte(pData);

			m_pCtx->m_Ping = GetByte(pData);

			m_pCtx->m_uTimeout = GetWord(pData);

			m_pCtx->m_Monitor = GetByte(pData);

			m_pCtx->m_uCount = bCount;

			m_pCtx->m_fToggle = FALSE;

			m_pCtx->m_pData = new BYTE[bCount * 2];

			pDevice->SetContext(m_pCtx);

			GetBackupData();

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;

}

CCODE MCALL CGem80JKMasterSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		SetBackupData();

		delete[] m_pCtx->m_pData;

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CGem80JKMasterSerialDriver::Ping(void)
{
	if( m_pCtx->m_Ping ) {

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = 'K';
		Addr.a.m_Offset = 0x0;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;

		return Read(Addr, Data, 0);
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CGem80JKMasterSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case 'K':
			break;

		case 'J':
			return uCount;

		default:
			return CCODE_ERROR | CCODE_HARD;
	}

	UINT uOffset = Addr.a.m_Offset;

	Start();

	AddByte('K');

	AddByte('J');

	if( Exchange(TRUE) ) {

		switch( Addr.a.m_Type ) {

			case addrWordAsWord:

				return DoWordRead(pData, uCount, uOffset);

				break;

			case addrWordAsLong:

				return DoLongRead(pData, uCount, uOffset);

				break;
		}
	}

	return CCODE_ERROR;

}

CCODE MCALL CGem80JKMasterSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case 'J':
			break;

		case 'K':
			return uCount;

		default:
			return CCODE_ERROR | CCODE_HARD;
	}

	UINT uOffset = Addr.a.m_Offset;

	Start();

	AddByte('K');

	AddByte('J');

	switch( Addr.a.m_Type ) {

		case addrWordAsWord:

			DoWordWrite(pData, uCount, uOffset);

			break;

		case addrWordAsLong:

			DoLongWrite(pData, uCount, uOffset);

			break;
	}

	if( m_pCtx->m_Ping || m_pCtx->m_Monitor ) {

		if( !Exchange(FALSE) ) {

			return CCODE_ERROR;
		}
	}

	memcpy(m_pCtx->m_pData, m_bTxBuff + 2, m_pCtx->m_uCount * 2);

	return uCount;
}

// Implementation

void CGem80JKMasterSerialDriver::Start(void)
{
	m_uPtr = 0;

	memcpy(m_bTxBuff + 2, m_pCtx->m_pData, m_pCtx->m_uCount * 2);
}

void CGem80JKMasterSerialDriver::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {

		m_bTxBuff[m_uPtr++] = bData;
	}
}

void CGem80JKMasterSerialDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
}

void CGem80JKMasterSerialDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
}

BOOL CGem80JKMasterSerialDriver::Exchange(BOOL fMonitor)
{
	for( UINT uRetry = 0; uRetry < 2; uRetry++ ) {

		TxPacket(0x03, fMonitor);

		UINT uResult = RxPacket();

		if( uResult == RX_FRAME ) {

			return TRUE;
		}

		if( uResult == RX_ERROR ) {

			break;
		}

		Sleep(10);
	}

	return FALSE;
}

void CGem80JKMasterSerialDriver::TxPacket(BYTE bMode, BOOL fMonitor)
{
	Send(PAD);

	Send(STX);

	m_CRC.Clear();

	UINT uCount = ((fMonitor && m_pCtx->m_Monitor) ? 2 : m_pCtx->m_uCount * 2 + 2);

	Send((m_pCtx->m_bDrop << 4) | bMode);

	for( UINT uScan = 0; uScan < uCount; uScan++ ) {

		switch( m_bTxBuff[uScan] ) {

			case ENQ:
			case STX:
			case ETX:
			case ETB:
			case EOT:
			case DLE:
				Send(DLE);

			default:
				Send(m_bTxBuff[uScan]);
		}
	}

	m_bTerm = m_pCtx->m_fToggle ? ETX : ETB;

	Send(m_bTerm);

	m_pCtx->m_fToggle = !m_pCtx->m_fToggle;

	UINT uTxCRC = m_CRC.GetValue();

	Send(uTxCRC % 256);

	Send(uTxCRC / 256);

}

UINT CGem80JKMasterSerialDriver::RxPacket(void)
{
	UINT uTarget = 0;

	UINT uRxCRC = 0;

	BOOL fDLE = FALSE;

	UINT uRetry = 3;

	UINT uState = 0;

	UINT uTimer = 0;

	UINT uByte = 0;

	UINT uPtr = 0;

	SetTimer(m_pCtx->m_uTimeout);

	while( (uTimer = GetTimer()) ) {

		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
		}

		m_CRC.Add(uByte);

		if( !fDLE && uByte == DLE ) {

			if( uState == 0 || uState == 2 ) {

				fDLE = TRUE;

				continue;
			}
		}

		switch( uState ) {

			case 0:
				switch( uByte ) {

					case STX:
						m_CRC.Clear();

						uState = 1;

						break;

					case NAK:

						return RX_NAK;

					case ACK:

						uState = 5;

						break;
				}
				break;

			case 1:
				if( (uByte & 0xF0) == UINT(m_pCtx->m_bDrop << 4) ) {

					uPtr = 0;

					uState = 2;
				}
				else {
					if( uRetry-- ) {

						m_pData->Write(ENQ, FOREVER);

						uState = 0;

						fDLE = FALSE;
					}
					else {
						return RX_ERROR;
					}
				}
				break;

			case 2:
				if( !fDLE ) {

					if( uByte == ETX || uByte == ETB ) {

						if( uByte != m_bTerm ) {

							return RX_ERROR;
						}
						else {
							uTarget = m_CRC.GetValue();

							uState = 3;

							break;
						}
					}
				}

				if( uPtr < m_uRxSize ) {

					m_bRxBuff[uPtr++] = uByte;
				}
				else {
					return RX_ERROR;
				}
				break;

			case 3:
				uRxCRC = uByte;

				uState = 4;

				break;

			case 4:
				if( uTarget == MAKEWORD(uRxCRC, uByte) ) {

					return RX_FRAME;
				}

				if( !uRetry-- ) {

					return RX_ERROR;
				}
				else {
					m_pData->Write(ENQ, FOREVER);

					uState = 0;

					fDLE = FALSE;
				}
				break;

			case 5:
				if( uByte == m_bTerm ) {

					return RX_ACK;
				}
				else {
					return RX_ERROR;
				}
				break;
		}

		fDLE = FALSE;
	}

	return RX_ERROR;
}

void CGem80JKMasterSerialDriver::Send(BYTE bData)
{
	m_pData->Write(bData, FOREVER);

	m_CRC.Add(bData);
}

CCODE CGem80JKMasterSerialDriver::DoWordRead(PDWORD pData, UINT uCount, UINT uOffset)
{
	MakeMin(uCount, m_pCtx->m_uCount);

	for( UINT u = 0; u < uCount; u++ ) {

		WORD x = PWORD(m_bRxBuff)[u + uOffset];

		pData[u] = LONG(SHORT(IntelToHost(x)));

	}

	return uCount;

}

CCODE CGem80JKMasterSerialDriver::DoLongRead(PDWORD pData, UINT uCount, UINT uOffset)
{
	UINT Count = m_pCtx->m_uCount / 2;

	MakeMin(uCount, Count);

	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x = PDWORD(m_bRxBuff)[u + uOffset / 2];

		pData[u] = IntelToHost(x);

	}

	return uCount;
}

CCODE CGem80JKMasterSerialDriver::DoWordWrite(PDWORD pData, UINT uCount, UINT uOffset)
{
	MakeMin(uCount, m_pCtx->m_uCount);

	for( UINT u = 0, i = 0; u < m_pCtx->m_uCount; u++ ) {

		if( u >= uOffset && u < uOffset + uCount ) {

			AddWord(pData[i++]);

			continue;
		}

		m_uPtr += 2;
	}


	return uCount;

}

CCODE CGem80JKMasterSerialDriver::DoLongWrite(PDWORD pData, UINT uCount, UINT uOffset)
{
	UINT Count = m_pCtx->m_uCount / 2;

	MakeMin(uCount, Count);

	for( UINT u = 0, i = 0; u < Count; u++ ) {

		if( u >= uOffset && u < uOffset + uCount ) {

			AddLong(pData[i++]);

			continue;
		}

		m_uPtr += 4;
	}

	return uCount;

}

void CGem80JKMasterSerialDriver::GetBackupData(void)
{
	for( UINT u = 0; u < TX_SLAVES; u++ ) {

		if( m_pBackup[u] ) {

			if( m_pBackup[u]->m_bDrop == m_pCtx->m_bDrop ) {

				memcpy(m_pCtx->m_pData, m_pBackup[u]->m_pData, m_pCtx->m_uCount * 2);

				return;
			}
		}
		else {
			m_pBackup[u] = new BACKUP;

			m_pBackup[u]->m_bDrop = m_pCtx->m_bDrop;

			m_pBackup[u]->m_pData = new BYTE[m_pCtx->m_uCount * 2];

			memset(m_pCtx->m_pData, 0, m_pCtx->m_uCount * 2);

			return;
		}
	}
}

void CGem80JKMasterSerialDriver::SetBackupData(void)
{
	for( UINT u = 0; u < TX_SLAVES; u++ ) {

		if( m_pBackup[u] ) {

			if( m_pBackup[u]->m_bDrop == m_pCtx->m_bDrop ) {

				memcpy(m_pBackup[u]->m_pData, m_pCtx->m_pData, m_pCtx->m_uCount * 2);

				return;
			}
		}
	}
}

// End of File
