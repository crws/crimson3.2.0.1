#include "camp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CTI CAMP Master Serial Driver
//

class CCti2CampM : public CCampMasterDriver
{
	public:
		// Constructor
		CCti2CampM(void);

		// Destructor
		~CCti2CampM(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		
	protected:
		// Device Context
		struct CContext : CCampMasterDriver::CBaseCtx
		{
		
			};

		// Data Members
		CContext * m_pCtx;
		
		// Transport Layer
		BOOL Send(void);
		BOOL RecvFrame(void);
		BOOL Transact(void);
	};

// End of File
