#include "intern.hpp"

#include "cticampm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CTI Camp Master Serial Driver
//

// Instantiator

INSTANTIATE(CCti2CampM);

// Constructor

CCti2CampM::CCti2CampM(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CCti2CampM::~CCti2CampM(void)
{
	}

// Configuration

void MCALL CCti2CampM::Load(LPCBYTE pData)
{
	}

void MCALL CCti2CampM::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}

	
// Management

void MCALL CCti2CampM::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}
	
void MCALL CCti2CampM::Open(void)
{
	}

// Device

CCODE MCALL CCti2CampM::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pBase->m_Error  = 0;
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CCti2CampM::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);

	}

// Entry Points

CCODE MCALL CCti2CampM::Ping(void)
{
	return CCampMasterDriver::Ping();
	
	}

// Transport Layer

BOOL CCti2CampM::Transact(void)
{
	if( Send() && RecvFrame() ) {
		
		return CheckFrame();
		}

	return FALSE; 
	}

BOOL CCti2CampM::Send(void)
{
	m_pData->ClearRx();

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CCti2CampM::RecvFrame(void)
{
	UINT uTimer = 0;

	UINT uData  = 0;
	
	m_uPtr      = 0;

	BOOL fBegin = FALSE;

	SetTimer(1500);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( uData == '[' ) {

			fBegin = TRUE;

			continue;
			}

		if( fBegin ) {

			if( uData == ']' ) {

				return TRUE;
				}

			m_bRxBuff[m_uPtr++] = uData;
			}	
		}

	return FALSE;
	}

// End of File
