
#include "intern.hpp"

#include "finsudpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omron Fins UDP Master Driver
//

// Instantiator

INSTANTIATE(COmronFinsMasterUDPDriver);

// Constructor

COmronFinsMasterUDPDriver::COmronFinsMasterUDPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

COmronFinsMasterUDPDriver::~COmronFinsMasterUDPDriver(void)
{
	}

// Configuration

void MCALL COmronFinsMasterUDPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL COmronFinsMasterUDPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL COmronFinsMasterUDPDriver::Open(void)
{
	}

// Device

CCODE MCALL COmronFinsMasterUDPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP1    = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();
			m_pCtx->m_bDna   = GetByte(pData);
			m_pCtx->m_bDa1	 = GetByte(pData);
			m_pCtx->m_bDa2	 = GetByte(pData);
			m_pCtx->m_bSna	 = GetByte(pData);
			m_pCtx->m_bSa1   = GetByte(pData);
			m_pCtx->m_bSa2	 = GetByte(pData);
			m_pCtx->m_bSid   = 0;
			m_pCtx->m_bMode	 = GetByte(pData);

			m_pCtx->m_fAux	 = FALSE;

			m_pCtx->m_IP2    = GetAddr(pData);
			m_pCtx->m_bDna2  = GetByte(pData);
			m_pCtx->m_bDa12	 = GetByte(pData);
			m_pCtx->m_bDa22	 = GetByte(pData);

			m_pCtx->m_fDirty = FALSE;

			m_pCtx->m_uPoll  = ToTicks(GetWord(pData));
			m_pCtx->m_uData  = 0;
			m_pCtx->m_uTime  = 0;

			m_pCtx->m_fIgnoreNonFatal = GetByte(pData);
			m_pCtx->m_fIgnoreFatal    = GetByte(pData);
			m_pCtx->m_bFaultMask	  = 0xFF;

			if( m_pCtx->m_fIgnoreNonFatal )
				m_pCtx->m_bFaultMask &= 0xBF;

			if( m_pCtx->m_fIgnoreFatal )
				m_pCtx->m_bFaultMask &= 0x7F;
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL COmronFinsMasterUDPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);

	}

// User Access

UINT MCALL COmronFinsMasterUDPDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx  = (CContext *) pContext;
       
	if( uFunc == 1 || uFunc == 5 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		if( uFunc == 1  ) {

			pCtx->m_IP1 = MotorToHost(dwValue);
			}

		if( uFunc == 5 ) {

			pCtx->m_IP2 = MotorToHost(dwValue);
			}

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort  = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 4 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP1);
		}
	
	if( uFunc == 6 ) {
		
		// Get Current Fallback IP Address

		return MotorToHost(pCtx->m_IP2);
		}

	CBaseCtx * pBase = pCtx;

	if( uFunc == 7 || uFunc == 13 ) {

		//  Set Destination Network

		UINT uValue = ATOI(Value);

		if( uValue >= 0 && uValue <= 127 ) {

			pCtx->m_fDirty = TRUE;

			if( uFunc == 7 ) {

				pBase->m_bDna = uValue;
				}

			if( uFunc == 13 ) {

				pCtx->m_bDna2 = uValue;
				}
			}

		return 0;
		}

	if( uFunc == 9 || uFunc == 15 ) {

		//  Set Destination Node

		UINT uValue = ATOI(Value);

		if( uValue >= 0 && uValue <= 255 ) {

			pCtx->m_fDirty = TRUE;

			if( uFunc == 9 ) {

				pBase->m_bDa1 = uValue;
				}

			if( uFunc == 15 ) {

				pCtx->m_bDa12 = uValue;
				}
			}

		return 0;
		}

	if( uFunc == 11 || uFunc == 17 ) {

		//  Set Destination Unit

		UINT uValue = ATOI(Value);

		if( uValue >= 0 && uValue <= 254 ) {

			pCtx->m_fDirty = TRUE;

			if( uFunc == 11 ) {

				pBase->m_bDa2 = uValue;
				}

			if( uFunc == 17 ) {

				pCtx->m_bDa22 = uValue;
				}
			}

		return 0;
		}

	if( uFunc == 8 ) {

		// Get Destination Network

		return pBase->m_bDna;
		}

	if( uFunc == 10 ) {

		// Get Destination Node

		return pBase->m_bDa1;
		}

	if( uFunc == 12 ) {

		// Get Destination Unit

		return pBase->m_bDa2;
		}

	if( uFunc == 14 ) {

		// Get Fallback Destination Network

		return pCtx->m_bDna2;
		}

	if( uFunc == 16 ) {

		// Get Fallback Destination Node

		return pCtx->m_bDa12;
		}

	if( uFunc == 18 ) {

		// Get Fallback Destination Unit

		return pCtx->m_bDa22;
		} 
	
	return 0;
	}

// Entry Points

CCODE MCALL COmronFinsMasterUDPDriver::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		DWORD IP;

		if( !m_pCtx->m_fAux ) {

			IP = m_pCtx->m_IP1;
			}
		else
			IP = m_pCtx->m_IP2;

		if( CheckIP(IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}
		}

	return COmronFinsMasterDriver::Ping();
	}

// Socket Management

BOOL COmronFinsMasterUDPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL COmronFinsMasterUDPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		m_pCtx->m_fAux   = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_UDP)) ) { 

		IPADDR IP;

		WORD   Port;

		if( !m_pCtx->m_fAux ) {

			IP   = (IPADDR const &) m_pCtx->m_IP1;

			Port = WORD(m_pCtx->m_uPort);
			}
		else {
			IP   = (IPADDR const &) m_pCtx->m_IP2;

			Port = WORD(m_pCtx->m_uPort);
			}

		m_pCtx->m_pSock->SetOption(OPT_RECV_QUEUE, sizeof(m_bRxBuff));
		
		if( m_pCtx->m_pSock->Connect(IP, Port, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);
		
			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}
				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			if( m_pCtx->m_IP2 ) {

				m_pCtx->m_fAux = !m_pCtx->m_fAux;
				}
				
			CloseSocket(TRUE); 

			return FALSE;
			}

		if( m_pCtx->m_IP2 ) {

			m_pCtx->m_fAux = !m_pCtx->m_fAux;
			}

		return FALSE;
		}

	return FALSE;
	}

void COmronFinsMasterUDPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL COmronFinsMasterUDPDriver::Send(void)
{
	UINT uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL COmronFinsMasterUDPDriver::RecvFrame(UINT uTotal)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= uTotal && uTotal > 9 ) {

				if ( (m_bRxBuff[6] == (m_pCtx->m_fAux ? m_pCtx->m_bDna2 : m_pCtx->m_bDna)) && 
				     (m_bRxBuff[7] == (m_pCtx->m_fAux ? m_pCtx->m_bDa12 : m_pCtx->m_bDa1)) && 
				     (m_bRxBuff[8] == (m_pCtx->m_fAux ? m_pCtx->m_bDa22 : m_pCtx->m_bDa2)) &&
				     (m_bRxBuff[3] == m_pCtx->m_bSna) && 
				     (m_bRxBuff[4] == m_pCtx->m_bSa1) && 
				     (m_bRxBuff[5] == m_pCtx->m_bSa2) &&
				     (m_bRxBuff[9] == m_pCtx->m_bSid - 1) ) {
					
					memcpy(m_bRxBuff, m_bRxBuff + 10, uTotal);

					return TRUE;
					}

				return FALSE;
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	if( m_pCtx->m_IP2 ) {

		m_pCtx->m_fAux = !m_pCtx->m_fAux;
		}
		
	return FALSE;
	}

BOOL COmronFinsMasterUDPDriver::Transact(BYTE bMRes, BYTE bSRes, UINT uCount, UINT uType)
{
	if( Send() ) {
		
		if( IsBroadcast() ) {

			while( m_pData->Read(0) < NOTHING );
			
			Sleep(10);
			
			return TRUE;
			}

		UINT uMult = UINT(uType == addrWordAsWord ? 2 : 4);
		
		if( RecvFrame(uCount * uMult + 14) ) {

			return CheckFrame(bMRes, bSRes, (bSRes != FINS_SRC_PING));
			}
		}

	CloseSocket(TRUE);

	return FALSE; 
	}

BOOL COmronFinsMasterUDPDriver::CheckFrame(BYTE bMRes, BYTE bSRes, BOOL fCheck)
{
	if( (m_bRxBuff[0] == bMRes) && (m_bRxBuff[1] == bSRes) ) {

		BYTE bMainEndCode = m_bRxBuff[2];

		// Mask out the CPU fault bits
		BYTE bSubEndCode = m_bRxBuff[3] & m_pCtx->m_bFaultMask;

		return fCheck ? (!bMainEndCode && !bSubEndCode) : TRUE;
		}

	return FALSE;
	}

BOOL COmronFinsMasterUDPDriver::Start(void)
{
	if( !OpenSocket() ) {

		return FALSE;
		}

	return COmronFinsMasterDriver::Start();
	}

void COmronFinsMasterUDPDriver::PutFinsHeader(void)
{
	AddByte(FINS_ICF | ( IsBroadcast() ? 1 : 0 ));

	AddByte(FINS_RSV);

	AddByte(FINS_GCT);

	AddByte(m_pCtx->m_fAux ? m_pCtx->m_bDna2 : m_pCtx->m_bDna);

	AddByte(m_pCtx->m_fAux ? m_pCtx->m_bDa12 : m_pCtx->m_bDa1);

	AddByte(m_pCtx->m_fAux ? m_pCtx->m_bDa22 : m_pCtx->m_bDa2);

	AddByte(m_pCtx->m_bSna);

	AddByte(m_pCtx->m_bSa1);

	AddByte(m_pCtx->m_bSa2);

	AddByte(m_pCtx->m_bSid);

	m_pCtx->m_bSid = m_pCtx->m_bSid == 0xFF ? 0 : m_pCtx->m_bSid + 1;
       
	}

// Helpers

BOOL COmronFinsMasterUDPDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL COmronFinsMasterUDPDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL COmronFinsMasterUDPDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
