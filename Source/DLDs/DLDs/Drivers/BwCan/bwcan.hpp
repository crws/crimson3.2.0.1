
#include "..\rawcan\rawcan.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Boulder Wind Power CAN Driver
//

class CBoulderWindPowerRawCANDriver : public CRawCANDriver
{
	public:
		// Constructor
		CBoulderWindPowerRawCANDriver(void);

		DEFMETH(BOOL) MakeRxMailBox(UINT uBox, UINT uMask, UINT uFilter, UINT uDLC);
		DEFMETH(BOOL) MakeTxMailBox(UINT uBox, UINT uId, UINT uDLC);
		DEFMETH(BOOL) RxMail(PTXT Mail);
		DEFMETH(BOOL) TxMailBox(UINT uBox, PDWORD pData);
		
		// Data Access
		DEFMETH(UINT) GetMailDLC(UINT uBox);
		
	protected:
	};

// End of File
