
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMSHawkCamera;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define PNGP	0x89504E47

#define IHDR	0x49484452

#define IDAT	0x49444154

#define IEND	0x49454E44

/////////////////////////////////////////////////////////////////////////
//
// MicrScan Hawk Camera Driver
//

class CMSHawkCamera : public CCameraDriver
{
	public:
		// Constructor
		CMSHawkCamera(void);

		// Srcructor
		~CMSHawkCamera(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT  )	DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE ) Ping     (void);
		DEFMETH(CCODE ) ReadImage(PBYTE &pData);
		DEFMETH(void  ) SwapImage(PBYTE pData);
		DEFMETH(void  ) KillImage(void);
		
		DEFMETH(PCBYTE) GetData  (UINT uDevice);
		DEFMETH(UINT  ) GetInfo  (UINT uDevice, UINT uParam);
		
		DEFMETH(BOOL  ) SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile);
		DEFMETH(BOOL  ) LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile);
		DEFMETH(BOOL  ) UseSetup (PVOID pContext, UINT uIndex);

	protected:
		// Device Data
		struct CContext
		{
			CContext * m_pNext;
			CContext * m_pPrev;
			UINT	   m_uDevice;
			DWORD	   m_IP;
			UINT	   m_uPort;
			BOOL	   m_fKeep;
			BOOL	   m_fPing;
			UINT	   m_uTime1;
			UINT	   m_uTime2;
			UINT	   m_uTime3;
			UINT	   m_uTime4;
			UINT	   m_uLast3;
			UINT	   m_uLast4;
			PBYTE	   m_pData;
			UINT	   m_uInfo[4];
			ISocket  * m_pSock;
			BYTE	   m_Scale;
			UINT	   m_X;
			UINT	   m_Y;
			char	   m_Http[360];
			BOOL	   m_fDirty;
			BYTE	   m_Graphic;
			BYTE	   m_Border;
			BYTE	   m_Failed;
			};

		// Data Members
		CContext * m_pCtx;
		CContext * m_pHead;
		CContext * m_pTail;
		UINT	   m_uKeep;
						
		// Extra Help
		IExtraHelper   * m_pExtra;
								
		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Implementation
		BOOL GetImage(PBYTE &pData);
		BOOL Read(PBYTE pData, UINT uCount);
		BOOL Send(ISocket *pSock, PCTXT pText, PTXT pArgs);
		BOOL Send(ISocket *pSock, PCBYTE pText, UINT uSize);
		void SetOptions(CContext * pCtx);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
		UINT IsEnable(PCTXT pText, BOOL &fEnable);
								
	};

// End of File
