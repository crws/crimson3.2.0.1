
#include "intern.hpp"

#include "mshawk.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Microscan Hawk Camera Driver
//

// Timeouts

static UINT const timeSendTimeout  = 5000;

static UINT const timeSendDelay	   = 5;

static UINT const timeRecvTimeout  = 10000;

static UINT const timeRecvDelay	   = 5;

static UINT const timeBuffDelay    = 100;

// Instantiator

INSTANTIATE(CMSHawkCamera);

// Constructor

CMSHawkCamera::CMSHawkCamera(void)
{
	m_Ident = DRIVER_ID;

	m_pCtx  = NULL;

	m_uKeep = 0;

	m_pHead = NULL;

	m_pTail = NULL;

	m_pHelper = NULL;
	}

// Destructor

CMSHawkCamera::~CMSHawkCamera(void)
{
	}

// Configuration

void MCALL CMSHawkCamera::Load(LPCBYTE pData)
{
	m_pHelper->MoreHelp(IDH_EXTRA,   (void **) &m_pExtra);
	
	if ( GetWord( pData ) == 0x1234 ) {

		}
	}
	
// Management

void MCALL CMSHawkCamera::Attach(IPortObject *pPort)
{

	}

void MCALL CMSHawkCamera::Open(void)
{
	}

// Device

CCODE MCALL CMSHawkCamera::DeviceOpen(IDevice *pDevice)
{
	CCameraDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {
			
			m_pCtx = new CContext;

			m_pCtx->m_uDevice = GetWord(pData);
			m_pCtx->m_IP	  = GetAddr(pData);
			m_pCtx->m_uPort	  = GetWord(pData);
			m_pCtx->m_fKeep   = FALSE;
			m_pCtx->m_fPing   = TRUE;
			m_pCtx->m_uTime1  = 2000;
			m_pCtx->m_uTime2  = 1000;
			m_pCtx->m_uTime3  = 500;
			m_pCtx->m_uTime4  = GetWord(pData);
			m_pCtx->m_uLast3  = GetTickCount();
			m_pCtx->m_uLast4  = GetTickCount();
			m_pCtx->m_pSock	  = NULL;
			m_pCtx->m_pData   = NULL;
			m_pCtx->m_Scale   = GetByte(pData);
			m_pCtx->m_X	  = GetWord(pData);
			m_pCtx->m_Y	  = GetWord(pData);
			m_pCtx->m_Graphic = GetByte(pData);
			m_pCtx->m_Border  = GetByte(pData);
			m_pCtx->m_Failed  = GetByte(pData);

			m_pCtx->m_fDirty  = FALSE;

			memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			SetOptions(m_pCtx);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMSHawkCamera::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {  
		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		CloseSocket(FALSE);

		delete m_pCtx->m_pData;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CCameraDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CMSHawkCamera::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 )  {

		BOOL fEnable = FALSE;

		if( IsEnable(Value, fEnable) ) {

			pCtx->m_Graphic = fEnable ? 1 : 0;

			SetOptions(pCtx);

			return 1;
			}

		return 0;
		}

	if( uFunc == 2 ) {

		BOOL fEnable = FALSE;

		if( IsEnable(Value, fEnable) ) {

			pCtx->m_Border = fEnable ? 1 : 0;

			SetOptions(pCtx);

			return 1;
			}

		return 0;
		}

	if( uFunc == 4 ) {

		BOOL fEnable = FALSE;

		if( IsEnable(Value, fEnable) ) {

			pCtx->m_Failed = fEnable ? 1 : 0;

			SetOptions(pCtx);

			return 1;
			}

		return 0;
		}

	if( uFunc == 6 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		pCtx->m_IP = MotorToHost(dwValue);
		
		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 7 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP);
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL CMSHawkCamera::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMSHawkCamera::ReadImage(PBYTE &pData)
{
	if( OpenSocket() ) {

		if ( GetImage(pData) ) {

			m_pCtx->m_uLast4 = GetTickCount();

			return CCODE_SUCCESS;
			}
		
		if( Ping() == CCODE_SUCCESS ) {

			UINT dt = GetTickCount() - m_pCtx->m_uLast4;

			UINT tt = 1000 * ToTicks(m_pCtx->m_uTime4);

			if( !tt || dt < tt ) {

				pData = NULL;

				return CCODE_SUCCESS;
				}
			}

		CloseSocket(FALSE);
		}	

	return CCODE_ERROR;
	}

void MCALL CMSHawkCamera::SwapImage(PBYTE pData)
{
	delete m_pCtx->m_pData;

	m_pCtx->m_uInfo[0] = GetTimeStamp();

	m_pCtx->m_uInfo[1] = ((BITMAP_RLC *) pData)->Frame;

	m_pCtx->m_pData    = pData;
	}

void MCALL CMSHawkCamera::KillImage(void)
{
	if( m_pCtx->m_pData ) {
		
		memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

		delete m_pCtx->m_pData;

		m_pCtx->m_pData = NULL;
		}
	}

PCBYTE MCALL CMSHawkCamera::GetData(UINT uDevice)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			return pCtx->m_pData;
			}
		}

	return NULL;
	}

UINT MCALL CMSHawkCamera::GetInfo(UINT uDevice, UINT uParam)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			if( uParam < elements(pCtx->m_uInfo) ) {

				return pCtx->m_uInfo[uParam];
				}
			}
		}

	return 0;
	}

BOOL MCALL CMSHawkCamera::SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	return FALSE;
	}

BOOL MCALL CMSHawkCamera::LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	return FALSE;
	}

BOOL MCALL CMSHawkCamera::UseSetup(PVOID pContext, UINT uIndex)
{
	return FALSE;
	}

// Socket Management

BOOL CMSHawkCamera::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CMSHawkCamera::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast3;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {
			
					m_pCtx->m_uLast4 = GetTickCount();
					
					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CMSHawkCamera::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock  = NULL;

		m_pCtx->m_uLast3 = GetTickCount();

		m_uKeep--;
		}
	}

// Implementation

BOOL CMSHawkCamera::GetImage(PBYTE &pImage)
{
	if( Send(m_pCtx->m_pSock, m_pCtx->m_Http, PTXT(&m_pCtx->m_Http + 1)) ) {

		BOOL  fOk = FALSE;

		UINT  uPos  = 0;

		UINT  uBytes = 1460;

		PBYTE pWork = new BYTE[uBytes];

		memset(pWork,  0, uBytes);

		SetTimer(timeRecvTimeout);

		while( GetTimer() ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				UINT n = uBytes - uPos;
				
				if( m_pCtx->m_pSock->Recv(pWork + uPos, n) == S_OK ) {

					if( !fOk ) {
						
						for( UINT o = 0; o < n; o++ ) {

							if( !memcmp(pWork + o, "Content-length:", 15) ) {

								fOk = TRUE;

								uBytes = ATOI(PTXT(pWork + o + 16));

								delete pWork;

								pWork = NULL;
								
								pWork = new BYTE[uBytes];

								break;
								}
							}

						continue;
						}
	
					else {	
						if( uPos == 0 ) {

							DWORD dwPNG = MotorToHost(PU4(pWork)[0]);

							if( dwPNG != PNGP ) {

								continue;
								}
							}

						if( uPos + n >= uBytes ) {

							for( UINT u = uPos; u < uBytes; u++ ) {

								DWORD dwEnd = MotorToHost(PU4(pWork + u)[0]);

								if( dwEnd == IEND ) {
	
									PBYTE pResult = m_pExtra->Png2RLCBmp(pWork, u + 8);

									pImage = pResult;

									delete pWork;

									return TRUE;
									}
								}
							}
								
						uPos += n;

						continue;
						}
					}
				}

			if( Phase == PHASE_CLOSING ) {

				break;
				}

			if( Phase == PHASE_ERROR ) {

				break;
				}

			Sleep(timeRecvDelay);
			}
						
		delete pWork;
		}
	
	return FALSE;
	}

BOOL CMSHawkCamera::Send(ISocket *pSock, PCTXT pText, PTXT pArgs)
{
	UINT uSize = strlen(pText);

	PTXT pWork = new char [ uSize + 256 ];

	SPrintf(pWork, pText, pArgs);

//	AfxTrace("\nTx : %s", pWork);

	if( Send(pSock, PCBYTE(pWork), strlen(pWork)) ) {

		delete pWork;

		return TRUE;
		}

	delete pWork;

	return FALSE;
	}

BOOL CMSHawkCamera::Send(ISocket *pSock, PCBYTE pText, UINT uSize)
{
	UINT uLimit = 1280;

	while( uSize ) {

		CBuffer *pBuff = CreateBuffer(uLimit, TRUE);

		if( pBuff ) {

			PBYTE pData = pBuff->GetData();

			UINT  uCopy = min(uLimit, uSize);

			memcpy(pData, pText, uCopy);

			pBuff->AddTail(uCopy);  

			SetTimer(timeSendTimeout);

			while( pSock->Send(pBuff) == E_FAIL ) {

				UINT Phase;

				pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN && GetTimer() ) {

					Sleep(timeSendDelay);

					continue;
					}

				BuffRelease(pBuff);
				
				return FALSE;
				}

			pText += uCopy;

			uSize -= uCopy;
			}
		else {  
			Sleep(timeBuffDelay);

			return FALSE;
			}
		}

	return TRUE;
	}

void CMSHawkCamera::SetOptions(CContext * pCtx)
{
	memset(pCtx->m_Http, 0, sizeof(pCtx->m_Http));

	UINT uOpts = pCtx->m_Graphic | pCtx->m_Border << 1 | pCtx->m_Failed << 2;

	if( pCtx->m_Scale ) {

		uOpts |= (8 | 16);

		SPrintf(PTXT(pCtx->m_Http), 

			PTXT("GET /getimage.png?opt=%u&width=%u&height=%u HTTP/1.1\r\n"
			     "Accept: */*\r\n"
			     "Accept-Language: en-us\r\n"
			     "Accept-Encoding: gzip, deflate\r\n"
			     "Connection: Keep-Alive\r\n"
			     "\r\n"), 

			     uOpts,

			     pCtx->m_X, 
			     pCtx->m_Y );

		return;
		}
	
	SPrintf(PTXT(pCtx->m_Http), 

		PTXT("GET /getimage.png?opt=%u& HTTP/1.1\r\n"
		     "Accept: */*\r\n"
		     "Accept-Language: en-us\r\n"
		     "Accept-Encoding: gzip, deflate\r\n"
		     "Connection: Keep-Alive\r\n"
		     "\r\n"),

		     uOpts);
	}

// Helpers

BOOL CMSHawkCamera::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CMSHawkCamera::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CMSHawkCamera::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

UINT CMSHawkCamera::IsEnable(PCTXT pText, BOOL &fEnable)
{
	UINT uValue = ATOI(pText);

	switch( uValue ) {

		case 0: fEnable = FALSE;	break;
		case 1: fEnable = TRUE;		break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// End of File
