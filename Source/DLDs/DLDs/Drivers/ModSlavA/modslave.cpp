
#include "intern.hpp"

#include "modslave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus ASCII Slave Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CModbusSlaveASCII);

// Constructor

CModbusSlaveASCII::CModbusSlaveASCII(void)
{
	m_Ident  = 0x3402;

	m_fAscii = TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave Driver
//

// Constructor

CModbusSlave::CModbusSlave(void)
{
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_fBroadcast = FALSE;
	}

// Config

void MCALL CModbusSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bDrop     = GetByte(pData);

		m_fFlipLong = GetByte(pData);

		m_fFlipReal = GetByte(pData);
				
		return;
		}
	}

void MCALL CModbusSlave::CheckConfig(CSerialConfig &Config)
{
	if( !m_fAscii && Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}

	Make485(Config, FALSE);
	}

// Management

void MCALL CModbusSlave::Attach(IPortObject *pPort)
{
	if( m_fAscii ) {

		m_pData = MakeDoubleDataHandler();

		pPort->Bind(m_pData);
		}
	else {
		m_pData = MakeSingleDataHandler();

		pPort->Bind(m_pData);
		}
	}

// User Access

UINT MCALL CModbusSlave::DrvCtrl(UINT uFunc, PCTXT Value)
{	
	UINT uValue  = 0;
	
	switch( uFunc ) {
		
		case 1:
			uValue = ATOI(Value);

			if( uValue ) {

				m_bDrop = BYTE(uValue);

				return 1;
				}

			break;
		}

	return 0;
	}

// Entry Points

void MCALL CModbusSlave::Service(void)
{
	AllocBuffers();

	for(;;) {

		if( !GetFrame() ) {

			continue;
			}

		if( m_pRx[0] == m_bDrop ) {

			switch( m_pRx[1] ) {

				case 0x01:
					HandleBitRead(3);
					break;	

				case 0x02:
					HandleBitRead(4);
					break;

				case 0x03:
					HandleRead(2);
					break;
					
				case 0x04:
					HandleRead(1);
					break;
					
				case 0x05:
					HandleSingleBitWrite(3);
					break;
					
				case 0x06:
					HandleSingleWrite(2);
					break;

				case 0x08:
					HandleLoopBack();
					break;
					
				case 0x0F:
					HandleMultiBitWrite(3);
					break;
					
				case 0x10:
					HandleMultiWrite(2);
					break;

				case 0x17:
					HandleReadWrite();
					break;

				default:
					TakeException(ILLEGAL_FUNCTION);
					break;
				}
			}

		if( m_pRx[0] == 0 ) {	

			m_fBroadcast = TRUE;

			switch( m_pRx[1] ) {

				case 0x05:
					HandleSingleBitWrite(3);
					break;
					
				case 0x06:
					HandleSingleWrite(2);
					break;

				case 0x0F:
					HandleMultiBitWrite(3);
					break;
					
				case 0x10:
					HandleMultiWrite(2);
					break;
				}

			m_fBroadcast = FALSE;
			}
		}
	}

// Frame Handlers

BOOL CModbusSlave::HandleRead(UINT uTable)
{
	UINT uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	UINT uCount = MotorToHost(PWORD(m_pRx + 4)[0]);

	if( uCount == 0 ) {

		TakeException(ILLEGAL_DATA);

		return FALSE;
	}

	if( uCount > 125 || uAddr + uCount > 0xFFFF ) {

		TakeException(ILLEGAL_ADDRESS);

		return FALSE;
		}
	else {
		PDWORD pWork = new DWORD [ uCount ];

		memset(pWork, 0, uCount * sizeof(DWORD));

		UINT   uType[] = { addrWordAsWord, addrWordAsLong, addrWordAsReal, addrLongAsLong, addrLongAsReal };

		UINT   uFact[] = { 1, 2, 2, 1, 1 };

		UINT   uTotal  = 0;

		for( UINT t = 0; t < elements(uType); t++ ) {

			if( !(uCount % uFact[t]) ) {

				CAddress Addr;

				Addr.a.m_Table  = uTable;
				Addr.a.m_Extra  = 0;
				Addr.a.m_Offset = 1 + uAddr; 
				Addr.a.m_Type   = uType[t];

				if( IsLongReg(uType[t]) ) {

					Addr.a.m_Table += 4;
					}

				uTotal = uCount / uFact[t];

				if( !COMMS_SUCCESS(Read(Addr, pWork, uTotal)) ) {

					if( t == 0 ) {

						Addr.a.m_Extra = 1;

						if( !COMMS_SUCCESS(Read(Addr, pWork, uTotal)) ) {

							continue;
							}
						}
					else
						continue;
					}
				
				break;
				}
			}

		if( t < elements(uType) ) {

			StartFrame(0, m_pRx[1]);

			UINT uBytes = ((IsLongReg(uType[t])) ? 4 : 2);

			AddByte(uBytes * uCount);
		
			BOOL fReal = IsReal(uType[t]);

			BOOL fFlip = fReal ? m_fFlipReal : m_fFlipLong;

			for( UINT n = 0; n < uTotal; n++ ) {

				if( t == 0 ) {

					AddWord(pWork[n]);
					}
				else {
					if( fFlip ) {
					       
						AddWord(LOWORD(pWork[n]));
						AddWord(HIWORD(pWork[n]));
						}
					else {
						AddWord(HIWORD(pWork[n]));
						AddWord(LOWORD(pWork[n]));
						}
					}
				}
			
			PutFrame();

			delete pWork;

			return TRUE;
			}

		delete pWork;

		return FALSE;
		}
	}

BOOL CModbusSlave::HandleBitRead(UINT uTable)
{
	UINT uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	UINT uCount = MotorToHost(PWORD(m_pRx + 4)[0]);

	if( uCount == 0 ) {

		TakeException(ILLEGAL_DATA);

		return FALSE;
	}

	if( uCount > 125 * 16 || uAddr + uCount > 0xFFFF ) {

		TakeException(ILLEGAL_ADDRESS);

		return FALSE;
		}
	else {
		PDWORD pWork = new DWORD [ uCount ];

		memset(pWork, 0, uCount * sizeof(DWORD));

		CAddress Addr;

		Addr.a.m_Table  = uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Offset = 1 + uAddr;
		Addr.a.m_Type   = addrBitAsBit;

		if( COMMS_SUCCESS(Read(Addr, pWork, uCount)) ) {

			StartFrame(0, m_pRx[1]);

			AddByte( (uCount+7)/8 );

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pWork[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b);
				}
		
			PutFrame();

			delete pWork;

			return TRUE;
			}

		delete pWork;

		return FALSE;
		}
	}

BOOL CModbusSlave::HandleMultiWrite(UINT uTable)
{
	UINT  uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	UINT  uCount = MotorToHost(PWORD(m_pRx + 4)[0]);

	UINT  uBytes = m_pRx[6];

	PU2   pWrite = PU2(m_pRx + 7);

	if( uCount == 0 ) {

		TakeException(ILLEGAL_DATA);

		return FALSE;
	}

	if( uCount > 125 || uAddr + uCount >  0xFFFF ) {

		TakeException(ILLEGAL_ADDRESS);

		return FALSE;
		}
	else {
		PDWORD pWork = new DWORD [ uCount ];

		UINT   uType[] = { addrWordAsWord, addrWordAsLong, addrWordAsReal, addrLongAsLong, addrLongAsReal };

		UINT   uFact[] = { 1, 2, 2, 1, 1 };

		UINT   uTotal  = 0;

		BOOL   fSign   = FALSE;

		for( UINT t = 0; t < elements(uType); t++ ) {

			if( !(uCount % uFact[t]) ) {

				CAddress Addr;

				Addr.a.m_Table  = uTable;
				Addr.a.m_Extra  = 0;
				Addr.a.m_Offset = 1 + uAddr; 
				Addr.a.m_Type   = uType[t];

				if( IsLongReg(uType[t]) ) {

					Addr.a.m_Table += 4;
					}

				uTotal = uCount / uFact[t];

				if( !COMMS_SUCCESS(Write(Addr, NULL, uTotal)) ) {

					if( t == 0 ) {

						Addr.a.m_Extra = 1;

						if( !COMMS_SUCCESS(Write(Addr, NULL, uTotal)) ) {

							continue;
							}

						fSign = TRUE;
						}
					else
						continue;
					}

				UINT uOctets = IsLongReg(uType[t]) ? uCount * sizeof(LONG) : uCount * sizeof(WORD);

				if( uBytes != uOctets ) {

					TakeException(ILLEGAL_DATA);

					return FALSE;
				}

				BOOL fReal = IsReal(uType[t]);

				BOOL fFlip = fReal ? m_fFlipReal : m_fFlipLong;

				for( UINT n = 0; n < uTotal; n++ ) {

					if( t == 0 ) {

						DWORD dwData = MotorToHost(pWrite[n]);

						if( fSign ) {

							Make16BitSigned(dwData);
							}

						pWork[n] = dwData;
						}
					else {
						WORD hi  = MotorToHost(pWrite[2*n+0]);
						
						WORD lo  = MotorToHost(pWrite[2*n+1]);

						if( fFlip )
							pWork[n] = MAKELONG(hi, lo);
						else
							pWork[n] = MAKELONG(lo, hi);
						}
					}

				Write(Addr, pWork, uTotal);
					
				break;
				}
			}

		if( t < elements(uType) ) {

			if( !m_fBroadcast ) {

				StartFrame(0, m_pRx[1]);

				AddWord(uAddr);

				AddWord(uCount);
						
				PutFrame();
				}

			delete pWork;

			return TRUE;
			}

		delete pWork;

		return FALSE;
		}
	}

BOOL CModbusSlave::HandleMultiBitWrite(UINT uTable)
{
	UINT uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	UINT uCount = MotorToHost(PWORD(m_pRx + 4)[0]);

	UINT uBytes = m_pRx[6];

	if( uCount == 0 || uBytes != (uCount+7)/8 ) {

		TakeException(ILLEGAL_DATA);

		return FALSE;
	}

	if( uCount > 125 * 16 || uAddr + uCount >  0xFFFF ) {

		TakeException(ILLEGAL_ADDRESS);

		return FALSE;
		}
	else {
		PDWORD pWork = new DWORD [ uCount ];

		CAddress Addr;

		Addr.a.m_Table  = uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Offset = 1 + uAddr;
		Addr.a.m_Type   = addrBitAsBit;

		UnpackBits( pWork, uCount );
		
		if( COMMS_SUCCESS(Write(Addr, pWork, uCount)) ) {

			if( !m_fBroadcast ) {

				StartFrame(0, m_pRx[1]);

				AddWord(uAddr);

				AddWord(uCount);

				PutFrame();
				}

			delete pWork;

			return TRUE;
			}

		delete pWork;

		return FALSE;
		}
	}

BOOL CModbusSlave::HandleSingleWrite(UINT uTable)
{	
	UINT  uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	DWORD dwData = MotorToHost(PWORD(m_pRx + 4)[0]);

	CAddress Addr;

	Addr.a.m_Table  = uTable;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Offset = 1 + uAddr;
	Addr.a.m_Type   = addrWordAsWord;

	if( !COMMS_SUCCESS(Write(Addr, &dwData, 1)) ) {

		Addr.a.m_Extra = 1;

		Make16BitSigned(dwData);

		if( !COMMS_SUCCESS(Write(Addr, &dwData, 1)) ) {

			return FALSE;
			}
		}

	if( !m_fBroadcast ) {

		StartFrame(0, m_pRx[1]);

		AddWord(uAddr);

		AddWord(dwData);

		PutFrame();
		}

	return TRUE;
	}

BOOL CModbusSlave::HandleSingleBitWrite(UINT uTable)
{	
	UINT  uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	DWORD dwData = MotorToHost(PWORD(m_pRx + 4)[0]);

	CAddress Addr;

	Addr.a.m_Table  = uTable;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Offset = 1 + uAddr;
	Addr.a.m_Type   = addrBitAsBit;

	if( LOWORD(dwData) == 0xFF00 ) {

		dwData = 1;
		}
	
	else if( LOWORD(dwData) != 0 ) {

		TakeException(ILLEGAL_DATA);

		return FALSE;
		}

	if( COMMS_SUCCESS(Write(Addr, &dwData, 1)) ) {

		if( !m_fBroadcast ) {

			StartFrame(0, m_pRx[1]);

			AddWord(uAddr);

			AddWord(dwData ? 0xFF00 : 0x0);

			PutFrame();
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CModbusSlave::HandleLoopBack(void)
{
	if( m_pRx[2] || m_pRx[3] ) {

		return TakeException(ILLEGAL_FUNCTION);
		}

	memcpy(m_pTx, m_pRx, 6);

	m_uPtr = 6;

	PutFrame();

	return TRUE;
	}

BOOL CModbusSlave::HandleReadWrite(void)
{
	if( TRUE ) {

		UINT  uAddr  = MotorToHost(PWORD(m_pRx + 6)[0]);

		UINT  uCount = MotorToHost(PWORD(m_pRx + 8)[0]);

		UINT  uBytes = m_pRx[10];

		PU2   pWrite = PU2(m_pRx + 11);

		if( uCount == 0 || uCount != uBytes / sizeof(WORD) ) {

			TakeException(ILLEGAL_DATA);

			return FALSE;
		}

		if( uCount > 125 || uAddr + uCount > 0xFFFF ) {

			TakeException(ILLEGAL_ADDRESS);

			return FALSE;
			}
		else {
			PDWORD pWork = new DWORD [ uCount ];

			UINT   uType[] = { addrWordAsWord, addrWordAsLong, addrWordAsReal };

			UINT   uFact[] = { 1, 2, 2 };

			UINT   uTotal  = 0;

			BOOL   fSign   = FALSE;

			for( UINT t = 0; t < elements(uType); t++ ) {

				if( !(uCount % uFact[t]) ) {

					CAddress Addr;

					Addr.a.m_Table  = 2;
					Addr.a.m_Extra  = 0;
					Addr.a.m_Offset = 1 + uAddr; 
					Addr.a.m_Type   = uType[t];

					uTotal = uCount / uFact[t];

					if( !COMMS_SUCCESS(Write(Addr, NULL, uTotal)) ) {

						if( uFact[t] == 1 ) {

							Addr.a.m_Extra = 1;

							if( !COMMS_SUCCESS(Write(Addr, NULL, uTotal)) ) {

								continue;
								}

							fSign = TRUE;
							}
						else
							continue;
						}

					BOOL fReal = (uType[t] == addrWordAsReal);

					BOOL fFlip = fReal ? m_fFlipReal : m_fFlipLong;

					for( UINT n = 0; n < uTotal; n++ ) {

						if( uFact[t] == 1 ) {

							DWORD dwData = MotorToHost(pWrite[n]);

							if( fSign ) {

								Make16BitSigned(dwData);
								}

							pWork[n] = dwData;
							}
						else {
							WORD hi  = MotorToHost(pWrite[2*n+0]);
							
							WORD lo  = MotorToHost(pWrite[2*n+1]);

							if( fFlip )
								pWork[n] = MAKELONG(hi, lo);
							else
								pWork[n] = MAKELONG(lo, hi);
							}
						}

					Write(Addr, pWork, uTotal);
						
					break;
					}
				}

			delete pWork;
			}
		}

	if( TRUE ) {
			
		UINT uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

		UINT uCount = MotorToHost(PWORD(m_pRx + 4)[0]);

		if( uCount == 0 ) {

			TakeException(ILLEGAL_DATA);

			return FALSE;
		}

		if( uCount > 125 || uAddr + uCount > 0xFFFF ) {

			TakeException(ILLEGAL_ADDRESS);

			return FALSE;
			}
		else {
			PDWORD pWork = new DWORD [ uCount ];

			memset(pWork, 0, uCount * sizeof(DWORD));

			UINT   uType[] = { addrWordAsWord, addrWordAsLong, addrWordAsReal };

			UINT   uFact[] = { 1, 2, 2 };

			UINT   uTotal  = 0;

			for( UINT t = 0; t < elements(uType); t++ ) {

				if( !(uCount % uFact[t]) ) {

					CAddress Addr;

					Addr.a.m_Table  = 2;
					Addr.a.m_Extra  = 0;
					Addr.a.m_Offset = 1 + uAddr; 
					Addr.a.m_Type   = uType[t];

					uTotal = uCount / uFact[t];

					if( COMMS_SUCCESS(Read(Addr, pWork, uTotal)) ) {

						break;
						}
					}
				}

			if( t < elements(uType) ) {

				StartFrame(0, m_pRx[1]);

				AddByte(2 * uCount);

				BOOL fReal = (uType[t] == addrWordAsReal);

				BOOL fFlip = fReal ? m_fFlipReal : m_fFlipLong;

				for( UINT n = 0; n < uTotal; n++ ) {

					if( uFact[t] == 1 ) {

						AddWord(pWork[n]);
						}
					else {
						if( fFlip ) {
						       
							AddWord(LOWORD(pWork[n]));
							AddWord(HIWORD(pWork[n]));
							}
						else {
							AddWord(HIWORD(pWork[n]));
							AddWord(LOWORD(pWork[n]));
							}
						}
					}
				
				PutFrame();

				delete pWork;

				return TRUE;
				}

			delete pWork;

			return FALSE;
			}
		}
	}

BOOL CModbusSlave::TakeException(BYTE bCode)
{
	if( !m_fBroadcast ) {

		StartFrame(0, m_pRx[1] | 0x80);

		AddByte(bCode);

		PutFrame();
		}

	return FALSE;
	}

// Implementation

void CModbusSlave::AllocBuffers(void)
{
	m_uTxSize = 512;
	
	m_uRxSize = 512;
	
	m_pTx = New BYTE [ m_uTxSize ];

	m_pRx = New BYTE [ m_uRxSize ];
	}
	
BOOL CModbusSlave::IsHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return TRUE;
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return TRUE;
		}
		
	return FALSE;
	}

WORD CModbusSlave::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return bData - '0';
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return bData - 'A' + 10;
		}
		
	return 0;
	}

// Port Access

void CModbusSlave::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CModbusSlave::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CModbusSlave::StartFrame(BYTE bDrop, BYTE bOpcode)
{
	m_uPtr = 0;
	
	AddByte(m_bDrop);
	
	AddByte(bOpcode);
	}

void CModbusSlave::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {
	
		m_pTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CModbusSlave::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CModbusSlave::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CModbusSlave::PutFrame(void)
{
	m_pData->ClearRx();
	
	return m_fAscii ? AsciiTx() : BinaryTx();
	}

BOOL CModbusSlave::GetFrame(void)
{
	return m_fAscii ? AsciiRx() : BinaryRx();
	}

BOOL CModbusSlave::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

	m_pData->Write(m_pTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CModbusSlave::BinaryRx(void)
{
	UINT uPtr = 0;

	UINT uGap = 0;

	while( TRUE ) {

		UINT uByte;
		
		if( (uByte = RxByte(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

			if( uPtr <= m_uRxSize ) {
			
				uGap = 0;
				
				continue;
				}
				
			return FALSE;
			}
		
		if( ++uGap >= 5 ) {
			
			if( uPtr >= 4 ) {
				
				m_CRC.Preset();
				
				PBYTE p = m_pRx;
				
				UINT  n = uPtr - 2;
			
				for( UINT i = 0; i < n; i++ ) {

					m_CRC.Add(*(p++));
					}

				WORD c1 = IntelToHost(PU2(p)[0]);
				
				WORD c2 = m_CRC.GetValue();
					
				if( c1 == c2 ) {
					
					return TRUE;
					}
				}
				
			uPtr = 0;
			
			uGap = 0;
			}
		}

	return FALSE;
	}

BOOL CModbusSlave::AsciiTx(void)
{
	BYTE bCheck = 0;

	TxByte(':');
		
	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		TxByte(m_pHex[m_pTx[i] / 16]);
		
		TxByte(m_pHex[m_pTx[i] % 16]);

		bCheck += m_pTx[i];
		}
		
	bCheck = BYTE(0x100 - WORD(bCheck));
	
	TxByte(m_pHex[bCheck / 16]);
	TxByte(m_pHex[bCheck % 16]);
	
	TxByte(CR);
	TxByte(LF);
	
	return TRUE;
	}

BOOL CModbusSlave::AsciiRx(void)
{
	UINT uState = 0;

	UINT uPtr   = 0;

	BYTE bCheck = 0;

	while( TRUE ) {

		UINT uByte;
		
		if( (uByte = RxByte(200)) == NOTHING ) {

			uState = 0;
			
			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == ':' ) {

					uPtr   = 0;

					uState = 1;

					bCheck = 0;
					}
				break;
				
			case 1:
				if( IsHex(uByte) ) {
				
					m_pRx[uPtr] = FromHex(uByte) << 4;
				
					uState = 2;
					}
				else {
					if( uByte == CR ) {
					
						continue;
						}

					if( uByte == LF ) {
												
						if( bCheck == 0 ) {

							// This is a hack to allow lower baud rates 
							// to work on the V2 platform's 5445x port.

							Sleep(10);

							return TRUE;
							}
						}
					
					return FALSE;
					}
				break;
				
			case 2:
				if( IsHex(uByte) ) {

					m_pRx[uPtr] |= FromHex(uByte);
					
					bCheck += m_pRx[uPtr];
					
					if( ++uPtr == m_uRxSize ) {
						
						return FALSE;
						}
					
					uState = 1;
					}
				else
					return FALSE;
				break;
			}
		}
		
	return FALSE;
	}

// Response Helper

void CModbusSlave::UnpackBits(PDWORD pWork, UINT uCount)
{
	UINT b = 7;

	BYTE m = 1;

	for( UINT n = 0; n < uCount; n++ ) {

		pWork[n] = ( m_pRx[b] & m ) ? TRUE : FALSE;

		if( !(m <<= 1) ) {

			b++;

			m = 1;
			}
		}
	}

// Helpers

void CModbusSlave::Make16BitSigned(DWORD &dwData)
{
	if( dwData & 0x8000 ) {

		dwData |= 0xFFFF0000;
		}
	}

BOOL CModbusSlave::IsLongReg(UINT uType)
{
	switch( uType ) {

		case addrLongAsLong:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CModbusSlave::IsReal(UINT uType)
{
	switch( uType ) {

		case addrWordAsReal:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

// End of File
