
//////////////////////////////////////////////////////////////////////////
//
// Modbus Exception Codes
//

#define	ILLEGAL_FUNCTION	0x01

#define ILLEGAL_ADDRESS		0x02

#define	ILLEGAL_DATA		0x03

//////////////////////////////////////////////////////////////////////////
//
// Modbus Slave Driver
//

class CModbusSlave : public CSlaveDriver
{
	public:
		// Constructor
		CModbusSlave(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(void) Service(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);

	protected:
		// Data
		LPCTXT	   m_pHex;
		BYTE	   m_bDrop;
		BOOL	   m_fAscii;
		UINT	   m_uTxSize;
		UINT	   m_uRxSize;
		BYTE     * m_pTx;
		BYTE     * m_pRx;
		UINT	   m_uPtr;
		CRC16	   m_CRC;
		BOOL	   m_fFlipLong;
		BOOL	   m_fFlipReal;
		BOOL	   m_fBroadcast;
				
		// Frame Handlers
		BOOL HandleRead(UINT uTable);
		BOOL HandleBitRead(UINT uTable);
		BOOL HandleMultiWrite(UINT uTable);
		BOOL HandleMultiBitWrite(UINT uTable);
		BOOL HandleSingleWrite(UINT uTable);
		BOOL HandleSingleBitWrite(UINT uTable);
		BOOL HandleLoopBack(void);
		BOOL HandleReadWrite(void);
		BOOL TakeException(BYTE bCode);

		// Implementation
		void Limit(UINT &uData, UINT uMin, UINT uMax);
		void AllocBuffers(void);
		BOOL IsHex(BYTE bData);
		WORD FromHex(BYTE bData);

		// Port Access
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);
		
		// Frame Building
		void StartFrame(BYTE bDrop, BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		
		// Transport Layer
		BOOL PutFrame(void);
		BOOL GetFrame(void);
		BOOL BinaryTx(void);
		BOOL BinaryRx(void);
		BOOL AsciiTx(void);
		BOOL AsciiRx(void);

		// Response Helper
		void UnpackBits(PDWORD pWork, UINT uCount);

		// Helpers
		void Make16BitSigned(DWORD &dwData);
		BOOL IsLongReg(UINT uType);
		BOOL IsReal(UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus ASCII Slave Driver
//

class CModbusSlaveASCII : public CModbusSlave
{
	public:
		// Constructor
		CModbusSlaveASCII(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus RTU Slave Driver
//

class CModbusSlaveRTU : public CModbusSlave
{
	public:
		// Constructor
		CModbusSlaveRTU(void);
	};

// End of File
