
//////////////////////////////////////////////////////////////////////////
//
// PCOM Binary Structures
//

struct CColumn
{
	UINT m_Type;
	UINT m_Bytes;
	};

struct CTable
{
	UINT	  m_Rows;
	CColumn   m_Cols[32];
	};

//////////////////////////////////////////////////////////////////////////
//
// PCOM Types
//

enum
{
	typeBit		= 0,
	typeByte	= 1,
	typeWord	= 2,
	typeLong	= 3,
	typeReal	= 4,
	typeUser	= 5
	};

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Master Base Driver
//

class CPcomBMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CPcomBMasterDriver(void);

		// Destructor
		~CPcomBMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Data
		struct CBaseCtx
		{
			BYTE	m_bUnit;
			UINT    m_TableCount;
			CTable *m_pTables;
			};

	protected:

		CBaseCtx * m_pBase;
		BYTE	   m_bTx[300];
		BYTE	   m_bRx[300];
		UINT	   m_uPtr;
		UINT	   m_uCheck;
			
		// Implementation
		void MakeMessage(AREF Addr, UINT& uCount, BOOL fWrite);
		void AddByte(BYTE bByte);
		void AddWord(WORD wWord);
		void AddLong(DWORD dwLong);
		void AddLongData(DWORD dwLong);
		void AddHeader(BOOL fWrite);
		void AddDetails(UINT uType, UINT uCol, UINT uTable, UINT uCount);
		void AddFooter(void);
		void AddAddress(UINT uTable, UINT uCol, UINT uRow);
		void AddLength(BOOL fWrite, UINT uType, UINT uCount);
		void AddData(PDWORD pData, UINT uType, UINT uCount);
		void AddBits(PDWORD pData, UINT uCount);
		void AddBytes(PDWORD pData, UINT uCount);
		void AddWords(PDWORD pData, UINT uCount);
		void AddLongs(PDWORD pData, UINT uCount);
		void AddReals(PDWORD pData, UINT uCount);
		void GetData(PDWORD pData, UINT uType, UINT uCount, UINT uTable);
		void GetBits(PDWORD pData, UINT uCount); 
		void GetBytes(PDWORD pData, UINT uCount);
		void GetWords(PDWORD pData, UINT uCount);
		WORD GetTwo(UINT uPos);
		void GetReals(PDWORD pData, UINT uCount);
		void GetLongs(PDWORD pData, UINT uCount);
		void Begin(void);
		void End(void);
				
		// Helpers
		UINT TypeBytes(UINT uType);
		UINT FindType(UINT uType);
		UINT FindMinCount(UINT uTable, UINT uOffset, UINT uCount);
		UINT GetCount(UINT uType);
		BOOL IsReadOnly(UINT uTable);
		BOOL CheckFrame(void);
										
		// Transport Layer
		virtual BOOL Transact(void);
	};

// End of File
