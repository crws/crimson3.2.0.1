
//////////////////////////////////////////////////////////////////////////
//
// SCI-TURBO L Driver
//

class CSciTurbo : public CMasterDriver
{
	public:
		// Constructor
		CSciTurbo(void);

		// Destructor
		~CSciTurbo(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Ping   (void);
		DEFMETH(CCODE) Read   (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write  (AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Command
		struct CCmndInfo {

			PTXT        m_pCmd;

			CCmndInfo * m_pNext;

			CCmndInfo * m_pPrev;
			};

		// Data Members
		UINT            m_uRawPort;
		BOOL            m_fUseRawPort;
		IExtraHelper  * m_pExtra;
		ICommsRawPort * m_pHost;
		BOOL		m_fInit;
		DWORD		m_uData[128];
		PTXT		m_pUnit[16];
		char		m_sLine[256];
		BOOL		m_fSendD;
		BOOL		m_fSendQ;
		UINT		m_uLastD;
		UINT		m_uLastQ;
		char		m_sHost[128];
		UINT		m_uHost;
		UINT		m_uRate;
		PTXT		m_pCmd;
		CCmndInfo     * m_pHead;
		CCmndInfo     * m_pTail;
		IMutex        * m_pMutex;
		ISyncHelper   * m_pSync;

		// Handling Transmission
		void HandleHost(void);
		BOOL ProcessHost(void);
		BOOL HandleInit(void);
		BOOL HandleUpload(void);
		BOOL HandleTypeD(void);
		BOOL HandleTypeQ(void);
		BOOL HandleStatus(void);
		BOOL HandleCmnd(void);
		BOOL SendCommand(PCTXT pCmd);
		BOOL WaitFor(char cData);
		BOOL AcceptUpload(void);
		BOOL AcceptStatus(void);
		BOOL AcceptLine(void);
		BOOL ParseTypeD(void);
		BOOL ParseTypeQ(void);
		
		// Implementation
		BOOL FindHostPort(void);

		// Command Queue
		void QueueCmnd(PCTXT pCmd);
		void SendCmnd(CCmndInfo *pCmnd);
		void ClaimData(void);
		void FreeData(void);
	};

// End of File
