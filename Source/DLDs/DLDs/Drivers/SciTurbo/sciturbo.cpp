
#include "intern.hpp"

#include "sciturbo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SCI-TURBO L Driver
//

// Instantiator

INSTANTIATE(CSciTurbo);

// Constructor

CSciTurbo::CSciTurbo(void)
{
	m_Ident  = DRIVER_ID;

	m_Flags  = DF_FORCE_SERVICE;

	m_uHost  = 0;

	m_fSendD = FALSE;

	m_fSendQ = FALSE;

	m_uRate  = 5;

	memset(m_uData, 0, sizeof(m_uData));
	
	memset(m_pUnit, 0, sizeof(m_pUnit));

	m_pCmd   = NULL;

	m_pHead  = NULL;

	m_pTail  = NULL;
	}

// Destructor

CSciTurbo::~CSciTurbo(void)
{
	m_pSync ->Release();

	m_pExtra->Release();
	
	m_pMutex->Release();
	}

// Configuration

void MCALL CSciTurbo::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_fUseRawPort = GetByte(pData);

		m_uRawPort    = GetByte(pData);
		}

	m_pExtra = NULL;

	m_pHost  = NULL;

	MoreHelp(IDH_EXTRA, (void **) &m_pExtra);

	MoreHelp(IDH_SYNC,  (void **) &m_pSync);

	m_pMutex = m_pSync->CreateMutex();
	}
	
void MCALL CSciTurbo::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}
	
// Management

void MCALL CSciTurbo::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CSciTurbo::Open(void)
{
	m_fInit = TRUE;
	}

// Device

CCODE MCALL CSciTurbo::DeviceOpen(IDevice *pDevice)
{
	return CCODE_SUCCESS;
	}

CCODE MCALL CSciTurbo::DeviceClose(BOOL fPersist)
{
	return CCODE_SUCCESS;
	}

UINT MCALL CSciTurbo::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	if( !m_fInit ) {

		QueueCmnd(Value);
		}
	
	return 0;
	}

// Entry Points

void MCALL CSciTurbo::Service(void)
{
	if( m_fInit ) {

		if( !HandleInit() ) {

			return;
			}

		m_fInit = FALSE;
		}

	if( FindHostPort() ) {

		HandleHost();
		}

	if( !HandleUpload() ) {

		m_fInit = TRUE;

		return;
		}

	if( FindHostPort() ) {

		HandleHost();
		}

	if( !HandleTypeD() ) {

		m_fInit = TRUE;

		return;
		}

	if( FindHostPort() ) {

		HandleHost();
		}

	if( !HandleTypeQ() ) {

		m_fInit = TRUE;

		return;
		}

	if( FindHostPort() ) {

		HandleHost();
		}

	if( !HandleStatus() ) {

		m_fInit = TRUE;

		return;
		}

	if( FindHostPort() ) {

		HandleHost();
		}

	HandleCmnd();
	}

CCODE MCALL CSciTurbo::Ping(void)
{
	return CCODE_SUCCESS;
	}

CCODE MCALL CSciTurbo::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_fInit ) {

		if( Addr.a.m_Table == 1 ) {

			*pData = m_uData[Addr.a.m_Offset];
			
			return 1;
			}

		if( Addr.a.m_Table == 2 ) {

			PTXT pUnit = m_pUnit[Addr.a.m_Offset];

			memcpy(pData, pUnit, strlen(pUnit));
			
			return uCount;
			}		
		}

	return CCODE_ERROR;
	}

CCODE MCALL CSciTurbo::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_fInit ) {
		
		
		}

	return 1;
	}

// Command Handling

void CSciTurbo::HandleHost(void)
{
	for(;;) {

		UINT uData = m_pHost->Read(0);

		if( uData == NOTHING ) {

			return;
			}

		if( uData == CR ) {

			m_pHost->Write( PCBYTE("\r\n"),
					2,
					FOREVER
					);

			m_sHost[m_uHost] = 0;

			if( ProcessHost() ) {

				m_pHost->Write( PCBYTE(">"),
						1,
						FOREVER
						);
				}

			m_uHost = 0;
			}

		if( isprint(uData) ) {

			if( m_uHost < elements(m_sHost) - 1 ) {

				m_sHost[m_uHost] = uData;

				m_pHost->Write( PCBYTE(m_sHost+m_uHost),
						1,
						FOREVER
						);

				m_uHost++;
				}
			}
		}
	}

BOOL CSciTurbo::ProcessHost(void)
{
	switch( m_sHost[0] ) {

		case 'D':

			m_fSendD = TRUE;
			
			m_uLastD = 0;
			
			return FALSE;

		case 'E':

			m_fSendD = FALSE;
			
			return TRUE;

		case '=':

			m_fSendQ = TRUE;
			
			m_uLastQ = 0;
			
			return FALSE;

		case '-':

			m_fSendQ = FALSE;
			
			return TRUE;

		case 'R':

			m_uRate  = ATOI(m_sHost+1);

			return FALSE;
		}

	if( m_sHost[0] ) {

		SendCommand(m_sHost);

		while( AcceptLine() ) {

			if( m_sLine[0] == '>' ) {

				break;
				}

			m_pHost->Write( PCBYTE(m_sLine),
					strlen(m_sLine),
					FOREVER
					);

			m_pHost->Write( PCBYTE("\r\n"),
					2,
					FOREVER
					);
			}
		}

	return TRUE;
	}

BOOL CSciTurbo::HandleInit(void)
{
	return SendCommand("E") && SendCommand("-") && SendCommand("R10");
	}

BOOL CSciTurbo::HandleUpload(void)
{
	return SendCommand("U") && AcceptUpload();
	}

BOOL CSciTurbo::HandleTypeD(void)
{
	if( SendCommand("D") && AcceptLine() ) {

		if( SendCommand("E") ) {

			if( m_fSendD ) {

				if( m_pHost ) {

					UINT uTime = GetTickCount();

					if( uTime - m_uLastD >= ToTicks(1000) * m_uRate ) {

						m_pHost->Write( PCBYTE(m_sLine),
								strlen(m_sLine),
								FOREVER
								);

						m_pHost->Write( PCBYTE("\r\n"),
								2,
								FOREVER
								);

						m_uLastD = uTime;
						}
					}
				}

			if( ParseTypeD() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CSciTurbo::HandleTypeQ(void)
{
	if( SendCommand("=") && AcceptLine() ) {

		if( SendCommand("-") ) {

			if( m_fSendQ ) {

				if( m_pHost ) {

					UINT uTime = GetTickCount();

					if( uTime - m_uLastQ >= ToTicks(1000) * m_uRate ) {

						m_pHost->Write( PCBYTE(m_sLine),
								strlen(m_sLine),
								FOREVER
								);

						m_pHost->Write( PCBYTE("\r\n"),
								2,
								FOREVER
								);

						m_uLastQ = uTime;
						}
					}
				}

			if( ParseTypeQ() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CSciTurbo::HandleStatus(void)
{
	return SendCommand("N") && AcceptStatus();
	}

BOOL CSciTurbo::HandleCmnd(void)
{
	ClaimData();

	for( CCmndInfo *pCmnd = m_pHead; pCmnd; ) {

		CCmndInfo *pNext = pCmnd->m_pNext;

		SendCmnd(pCmnd);

		pCmnd = pNext;
		}

	FreeData();

	return FALSE;
	}

BOOL CSciTurbo::SendCommand(PCTXT pCmd)
{
	m_pData->ClearRx();

	m_pData->Write(CR, FOREVER);

	if( WaitFor(CR) ) {

		while( *pCmd ) {

			char cCmd = *pCmd++;

			m_pData->Write(cCmd, FOREVER);

			if( !WaitFor(cCmd) ) {

				return FALSE;
				}
			}

		m_pData->Write(CR, FOREVER);

		if( WaitFor(CR) ) {

			return TRUE;
			}
		}

	return TRUE;
	}

BOOL CSciTurbo::WaitFor(char cData)
{
	SetTimer(500);

	for(;;) {

		UINT uData = m_pData->Read(GetTimer());

		if( uData == NOTHING ) {

			return FALSE;
			}

		if( uData == BYTE(cData) ) {

			return TRUE;
			}
		}
	}

BOOL CSciTurbo::AcceptUpload(void)
{
	char sData[32];

	char sUnit[32];

	UINT uSlot  = 0;

	UINT uState = 0;

	UINT uPos   = 0;

	BOOL fDot   = FALSE;

	SetTimer(500);

	for(;;) {

		UINT uData = m_pData->Read(GetTimer());

		if( uData == NOTHING ) {

			return FALSE;
			}

		if( uData == LF ) {

			continue;
			}

		switch( uState ) {

			case 0:
				if( uData == '>' ) {

					return TRUE;
					}

				if( uData != CR ) {

					uState = 1;
					}

				break;

			case 1:
				if( uData == CR ) {

					SetTimer(500);

					uState = 0;
					}

				if( uData == ':' ) {

					uState = 2;
					}

				break;
			
			case 2:
				if( isdigit(uData) ) {

					uPos          = 0;

					fDot          = FALSE;
					
					sData[uPos++] = uData;

					uState        = 3;
					}
				break;

			case 3:
				if( isdigit(uData) || uData == '.' ) {
					
					if( uPos == elements(sData) - 1 ) {

						return FALSE;
						}

					if( uData == '.' ) {

						fDot = TRUE;
						}

					sData[uPos++] = uData;
					}
				else {
					sData  [uPos ] = 0;

					m_uData[uSlot] = fDot ? ATOF(sData) : ATOI(sData);

					if( uSlot < 4 ) {

						uState = 4;
						
						uPos   = 0;
						}
					else {
						uSlot  = uSlot + 1;

						uState = (uData == CR) ? 0 : 5;						
						}
					}
				
				break;

			case 4:
				if( uData == CR ) {
					
					sUnit[uPos] = 0;

					if( m_pUnit[uSlot] ) {
						
						free(m_pUnit[uSlot]);

						m_pUnit[uSlot] = NULL;
						}

					m_pUnit[uSlot] = strdup(sUnit);

					uSlot  = uSlot + 1;

					uState = 0;
					}
				else {
					if( uPos == elements(sUnit) - 1 ) {

						return FALSE;
						}

					sUnit[uPos++] = uData;
					}

				break;

			case 5:
				if( uData == CR ) {

					SetTimer(500);

					uState = 0;
					}
				break;
			}
		}
	}

BOOL CSciTurbo::AcceptStatus(void)
{
	char sData[32] = { 0 };

	UINT uSlot  = 96;

	UINT uState = 0;

	UINT uPos   = 0;

	SetTimer(500);

	for(;;) {

		UINT uData = m_pData->Read(GetTimer());

		if( uData == NOTHING ) {

			return FALSE;
			}

		if( uData == LF ) {

			continue;
			}

		switch( uState ) {

			case 0:
				if( uData == '>' ) {

					return TRUE;
					}

				if( uData != CR ) {

					uState = 1;
					}

				break;

			case 1:
				if( uData == CR ) {

					SetTimer(500);

					uState = 0;
					}

				if( uData == ':' ) {

					uState = 2;
					}

				break;
			
			case 2:
				if( isalnum(uData) ) {

					uPos          = 0;

					sData[uPos++] = uData;
					}

				if( uData == CR ) {

					m_uData[uSlot] = sData[0];
					
					uSlot  = uSlot + 1;

					uState = 0;
					}
				break;
			}
		}
	}

BOOL CSciTurbo::AcceptLine(void)
{
	UINT uPos = 0;

	SetTimer(500);

	for(;;) {

		UINT uData = m_pData->Read(GetTimer());

		if( uData == NOTHING ) {

			return FALSE;
			}

		if( uData == LF ) {

			continue;
			}

		if( uData == CR ) {

			m_sLine[uPos] = 0;

			return TRUE;
			}

		if( uData == '>' ) {

			if( uPos == 0 ) {

				m_sLine[0] = '>';

				m_sLine[1] = 0;

				return TRUE;
				}
			}

		if( uPos == elements(m_sLine) - 1 ) {

			return FALSE;
			}

		m_sLine[uPos++] = uData;
		}
	}

BOOL CSciTurbo::ParseTypeD(void)
{
	UINT uSlot  = 32;

	UINT uScan  = 0;

	UINT uState = 0;

	UINT uPos   = 0;

	BOOL fDot   = FALSE;

	for(;;) {

		UINT uData = m_sLine[uScan];

		if( uData == 0 ) {

			return TRUE;
			}

		switch( uState ) {

			case 0:
				if( isdigit(uData) ) {

					uPos   = uScan;

					fDot   = FALSE;

					uState = 1;

					}

				break;

			case 1:
				if( isdigit(uData) || uData == '.' ) {

					if( uData != '.' ) {
					
						fDot = TRUE;
						}
					}
				else {
					m_sLine[uScan] = 0;

					m_uData[uSlot] = fDot ? ATOF(m_sLine + uPos) : ATOI(m_sLine + uPos);
					
					uSlot  = uSlot + 1;
					
					uState = 0;
					}

				break;
			}

		uScan++;
		}
	}

BOOL CSciTurbo::ParseTypeQ(void)
{
	struct CField
	{
		UINT	m_uPos;
		UINT	m_uLen;
		UINT	m_uType;
		};

	static CField List[] = {

		{	1,	3,	0	},
		{	4,	3,	0	},
		{	8,	3,	0	},
		{	11,	3,	0	},
		{	15,	3,	0	},
		{	18,	3,	0	},
		{	22,	2,	1	},
		{	24,	2,	1	},
		{	26,	2,	1	},
		{	28,	2,	1	},
		{	32,	5,	2	},
		{	38,	5,	2	},
		{	44,	6,	2	},
		{	51,	3,	2	},
		{	55,	5,	2	},
		{	60,	5,	2	},
		{	65,	5,	2	},
		{	70,	5,	2	},
		{	75,	5,	2	},
		{	81,	5,	2	},
		{	86,	5,	2	},
		{	96,	5,	2	},
		{	101,	5,	2	}

		};

	for( UINT n = 0; n < elements(List); n++ ) {

		UINT uPos  = List[n].m_uPos - 1;

		UINT uEnd  = uPos + List[n].m_uLen;

		char cKeep = m_sLine[uEnd];

		UINT uSlot = 64 + n;

		m_sLine[uEnd] = 0;

		switch( List[n].m_uType ) {

			case 0:
				m_uData[uSlot] = stricmp(m_sLine + uPos, "OFF") ? 1 : 0;
				break;

			case 1:
				m_uData[uSlot] = ATOI(m_sLine + uPos);
				break;

			case 2:
				m_uData[uSlot] = ATOF(m_sLine + uPos);
				break;
			}

		m_sLine[uEnd] = cKeep;
		}

	return TRUE;
	}

// Command List

void CSciTurbo::QueueCmnd(PCTXT pCmd)
{
	ClaimData();

	CCmndInfo *pCmnd = New CCmndInfo;

	pCmnd->m_pCmd    = strdup(pCmd);

	AfxListAppend(m_pHead, m_pTail, pCmnd, m_pNext, m_pPrev);

	FreeData();
	}

void CSciTurbo::SendCmnd(CCmndInfo *pCmnd)
{
	if( pCmnd ) {

		SendCommand(pCmnd->m_pCmd);

		AfxListRemove(m_pHead, m_pTail, pCmnd, m_pNext, m_pPrev);

		free(pCmnd->m_pCmd);

		delete pCmnd;
		}							 	
	}

void CSciTurbo::ClaimData(void)
{
	m_pMutex->Wait(FOREVER);
	}

void CSciTurbo::FreeData(void)
{
	m_pMutex->Free();
	}

// Implementation

BOOL CSciTurbo::FindHostPort(void)
{
	if( m_fUseRawPort && !m_pHost ) {

		m_pExtra->GetRawPort(m_uRawPort, &m_pHost);
		}

	return m_pHost ? TRUE : FALSE;
	}

// End of File
