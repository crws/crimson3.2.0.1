//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 MPI Constants
//

#define	SPACE_OUTPUT		0x01
#define	SPACE_INPUT		0x02
#define	SPACE_FLAG		0x03
#define	SPACE_TIMER		0x04
#define	SPACE_COUNTER		0x05
#define	SPACE_PERIPHERAL	0x06
#define	SPACE_DATA		0x0A

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 MPI Macro's
//

#define DATA_BLOCK(a,b,c)	(a + (b & 0xF0) + ((c & 0xE000) >> 5))
#define INDEX(a)		(a & 0x1FFF)

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 PLC via MPI Master Driver
//

class CS7DirectMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CS7DirectMasterDriver(void);
	
		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			BYTE	m_bThatDrop;
			};

		// Data Members
		CContext * m_pCtx;
		
		BYTE		m_bThisDrop;
		BYTE		m_bLastDrop;
		BYTE		m_bBaudRate;
		IMPIHelper    * m_pMPI;

	};

//////////////////////////////////////////////////////////////////////////
//
// MPI Address Wrapper
//

class CMPIAddrWrapper : public CMPIAddr
{
	public:
		// Constructor
		CMPIAddrWrapper(AREF Addr);

		// Attributes
		BOOL IsValid(void);

	protected:
		// Data
		CAddress	m_Addr;
		BOOL		m_fValid;

		// Implementation
		void Parse(void);
		BOOL ParseDataBlock(void);
		BOOL ParseInput(void);
		BOOL ParseOutput(void);
		BOOL ParseMemory(void);
		BOOL ParsePeripheral(void);
		BOOL ParseCounter(void);
		BOOL ParseTimer(void);
		BOOL ParseDataType(void);
	};

// End of File
