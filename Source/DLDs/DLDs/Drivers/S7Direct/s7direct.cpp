
#include "intern.hpp"

#include "s7direct.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 via MPI Adpater Driver
//

// Instantiator

INSTANTIATE(CS7DirectMasterDriver);

// Constructor

CS7DirectMasterDriver::CS7DirectMasterDriver(void)
{
	m_Ident      = DRIVER_ID;

	m_fOpen      = FALSE;

	m_pMPI = NULL;
	}

// Configuration

void MCALL CS7DirectMasterDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bBaudRate = GetByte(pData);

		m_bThisDrop = GetByte(pData);
		
		m_bLastDrop = GetByte(pData);		
		}
	}
	
void MCALL CS7DirectMasterDriver::CheckConfig(CSerialConfig &Config)
{
	}
	
// Management

void MCALL CS7DirectMasterDriver::Attach(IPortObject *pPort)
{
	if( !m_pMPI && !m_fOpen ) {

		if( MoreHelp(IDH_MPI, (void **) &m_pMPI) ) {

			m_pMPI->Attach(pPort);
			}
		}
	}

void MCALL CS7DirectMasterDriver::Detach(void)
{
	if( m_pMPI ) {

		m_pMPI->Close();

		m_pMPI->Release();

		m_pMPI = NULL;
		}
	}

void MCALL CS7DirectMasterDriver::Open(void)
{
	m_pMPI->Open(m_bThisDrop, m_bLastDrop, m_bBaudRate);
	}

// Device

CCODE MCALL CS7DirectMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {
			
			m_pCtx              = new CContext;

			m_pCtx->m_bThatDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;	
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CS7DirectMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CS7DirectMasterDriver::Ping(void)
{
	return m_pMPI->IsOnline(m_pCtx->m_bThatDrop) ? CCODE_SUCCESS : CCODE_ERROR;
	}

CCODE MCALL CS7DirectMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pMPI->OpenSAP(m_pCtx->m_bThatDrop) ) {

		switch( Addr.a.m_Type ) {

			case addrByteAsByte:
				MakeMin(uCount, 94);
				break;

			case addrByteAsWord:
				MakeMin(uCount, 47);
				break;

			case addrByteAsLong:
				MakeMin(uCount, 23);
				break;
			}
	
		if( m_pMPI->Read(CMPIAddrWrapper(Addr), pData, uCount) ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CS7DirectMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pMPI->OpenSAP(m_pCtx->m_bThatDrop) ) {

		if( m_pMPI->Write(CMPIAddrWrapper(Addr), pData, uCount) ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

//////////////////////////////////////////////////////////////////////////
//
// MPI Address Wrapper
//

// Constructor

CMPIAddrWrapper::CMPIAddrWrapper(AREF Addr)
{
	m_Addr = Addr;

	Parse();
	}

// Attributes

BOOL CMPIAddrWrapper::IsValid(void)
{
	return m_fValid;
	}

// Implementation

BOOL CMPIAddrWrapper::ParseDataBlock(void)
{
	UINT Space  = m_Addr.a.m_Table & 0x0F;

	UINT Index  = m_Addr.a.m_Offset;

	if( Space >= SPACE_DATA ) {

		m_uDomain    = domainDataBlock;
		m_uSubDomain = DATA_BLOCK(m_Addr.a.m_Extra, m_Addr.a.m_Table, m_Addr.a.m_Offset);
		m_uBitOffset = INDEX(Index) << 3;
		
		return ParseDataType();
		}

	return FALSE;
	}

BOOL CMPIAddrWrapper::ParseInput(void)
{
	UINT Space  = m_Addr.a.m_Table & 0x0F;

	UINT Index  = m_Addr.a.m_Offset;

	if( Space == SPACE_INPUT ) {

		m_uDomain    = domainInput;
		m_uSubDomain = 0;
		m_uBitOffset = INDEX(Index) << 3;
		
		return ParseDataType();
		}
	
	return FALSE;
	}

BOOL CMPIAddrWrapper::ParseOutput(void)
{
	UINT Space  = m_Addr.a.m_Table & 0x0F;

	UINT Index  = m_Addr.a.m_Offset;

	if( Space == SPACE_OUTPUT ) {

		m_uDomain    = domainOutput;
		m_uSubDomain = 0;
		m_uBitOffset = INDEX(Index) << 3;
		
		return ParseDataType();
		}
	
	return FALSE;
	}

BOOL CMPIAddrWrapper::ParseMemory(void)
{
	UINT Space  = m_Addr.a.m_Table & 0x0F;

	UINT Index  = m_Addr.a.m_Offset;

	if( Space == SPACE_FLAG ) {

		m_uDomain    = domainMemory;
		m_uSubDomain = 0;
		m_uBitOffset = INDEX(Index) << 3;
		
		return ParseDataType();
		}
	
	return FALSE;
	}

BOOL CMPIAddrWrapper::ParsePeripheral(void)
{
	UINT Space  = m_Addr.a.m_Table & 0x0F;

	UINT Index  = m_Addr.a.m_Offset;

	if( Space == SPACE_PERIPHERAL ) {

		m_uDomain    = domainPeripheral;
		m_uSubDomain = 0;
		m_uBitOffset = INDEX(Index) << 3;
		
		return ParseDataType();
		}
	
	return FALSE;
	}

BOOL CMPIAddrWrapper::ParseCounter(void)
{
	UINT Space  = m_Addr.a.m_Table & 0x0F;

	UINT Index  = m_Addr.a.m_Offset;

	if( Space == SPACE_COUNTER ) {

		m_uDomain    = domainCounter;
		m_uSubDomain = 0;
		m_uBitOffset = INDEX(Index);
		
		m_uType      = typeS7Counter;

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CMPIAddrWrapper::ParseTimer(void)
{
	UINT Space  = m_Addr.a.m_Table & 0x0F;

	UINT Index  = m_Addr.a.m_Offset;

	if( Space == SPACE_TIMER ) {

		m_uDomain    = domainTimer;
		m_uSubDomain = 0;
		m_uBitOffset = INDEX(Index);
		
		m_uType      = typeS7Timer;

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CMPIAddrWrapper::ParseDataType(void)
{
	switch( m_Addr.a.m_Type ) {

		case addrBitAsByte:

			m_uType      = typeS7Bool;

			break;

		case addrByteAsByte:

			m_uType      = typeS7Byte;

			break;

		case addrByteAsWord:
		case addrWordAsWord:

			m_uType      = typeS7Word;
			
			break;

		case addrByteAsLong:

			m_uType      = typeS7DWord;
			
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

void CMPIAddrWrapper::Parse(void)
{
	m_fValid = ParseDataBlock()	||
		   ParseInput()		||
		   ParseOutput()	||
		   ParseMemory()	||
		   ParseCounter()	||
		   ParsePeripheral()	||
		   ParseTimer()		;;
	}

// End of File
