//////////////////////////////////////////////////////////////////////////
//
// Emerson - Control Techniques Unidrive M TCP Master
//

#include "intern.hpp"

#include "UnidriveMTCP.hpp"

// Base Class

#include "../EmersonUnidriveMSerial/UnidriveMBase.cpp"

// Instantiator

INSTANTIATE(CUnidriveMTCP)

// Constructor

CUnidriveMTCP::CUnidriveMTCP(void)
{
	m_Ident	= DRIVER_ID;

	m_pCtx	= NULL;

	m_pHead	= NULL;
	
	m_pTail	= NULL;

	m_uKeep = 0;
	}

// Destructor

CUnidriveMTCP::~CUnidriveMTCP(void)
{
	}

// Configuration

void CUnidriveMTCP::Load(LPCBYTE pData)
{
	}

// Management

void CUnidriveMTCP::Attach(IPortObject *pPort)
{
	}

void CUnidriveMTCP::Open(void)
{
	}

// Entry Points

CCODE CUnidriveMTCP::Ping(void)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	DWORD Data[1];

	CAddress Addr;

	Addr.a.m_Table	= SPACE_HOLD;
	Addr.a.m_Offset	= 1;
	Addr.a.m_Type	= addrWordAsWord;
	Addr.a.m_Extra  = 0;
	
	return DoWordRead(Addr, Data, 1);
	}

CCODE CUnidriveMTCP::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	return CUnidriveMBase::Read(Addr, pData, uCount);
	}

CCODE CUnidriveMTCP::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	return CUnidriveMBase::Write(Addr, pData, uCount);
	}

// Device

CCODE CUnidriveMTCP::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_uRegMode	= GetLong(pData);
			m_pCtx->m_uDriveMode	= GetLong(pData);

			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE CUnidriveMTCP::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Frame Building

void CUnidriveMTCP::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);

	AddByte(bOpcode);
	}

// Transport Layer

BOOL CUnidriveMTCP::SendFrame(void)
{
	m_bTx[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUnidriveMTCP::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRx[5];

				if( uPtr >= uTotal ) {

					if( m_bRx[0] == m_bTx[0] ) {

						if( m_bRx[1] == m_bTx[1] ) {

							memcpy(m_bRx, m_bRx + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRx[n] = m_bRx[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CUnidriveMTCP::Transact(BOOL fIgnore)
{
	m_bRx[0] = 0xFF;
	m_bRx[1] = 0xFE;

	if( m_pCtx->m_pSock ) {

		if( SendFrame() && RecvFrame() ) {

			if( fIgnore ) {

				return TRUE;
				}

			return CheckFrame();
			}

		CloseSocket(TRUE);
		}

	return FALSE;
	}

BOOL CUnidriveMTCP::CheckFrame(void)
{
	if( !(m_bRx[1] & 0x80) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CUnidriveMTCP::HasReadException(void)
{
	return m_bRx[1] & 0x80;
	}

// Socket Management

BOOL CUnidriveMTCP::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {
			
			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CUnidriveMTCP::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CUnidriveMTCP::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort ) 
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
