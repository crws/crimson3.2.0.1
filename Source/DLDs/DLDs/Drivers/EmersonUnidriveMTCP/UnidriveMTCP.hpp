#ifndef INCLUDE_UNIDRIVE_M_TCP
#define INCLUDE_UNIDRIVE_M_TCP

#include "intern.hpp"

#include "../EmersonUnidriveMSerial/UnidriveMBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson - Control Techniques Unidrive M TCP Master
//

class CUnidriveMTCP : public CUnidriveMBase
{
	public:
		// Constructor
		CUnidriveMTCP(void);

		// Destructor
		~CUnidriveMTCP(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Entry Points
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:

		// Device data
		struct CContext : CUnidriveMBase::UnidriveMBaseContext
		{
			CContext * m_pNext;
			CContext * m_pPrev;
			DWORD	   m_IP;
			UINT	   m_uPort;
			BYTE	   m_bUnit;
			BOOL	   m_fKeep;
			BOOL	   m_fPing;
			UINT	   m_uTime1;
			UINT	   m_uTime2;
			UINT	   m_uTime3;
			UINT	   m_uLast;
			WORD	   m_wTrans;
			ISocket	 * m_pSock;
			};

		// Data Members
		CContext	* m_pCtx;
		CContext	* m_pHead;
		CContext	* m_pTail;
		UINT		  m_uKeep;		

		// Frame Building
		void StartFrame(BYTE bOpcode);
		
		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL Transact(BOOL fIgnore);
		BOOL CheckFrame(void);
		BOOL HasReadException(void);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
	};

#endif

// End of File
