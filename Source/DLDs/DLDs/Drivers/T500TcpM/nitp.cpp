#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "nitp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TI 500 NITP Base Driver
//

Area CODE_SEG CNitpDriver::m_Area[] = {

{	tableV,		0 << 11,	1024,	   0,	0,	cat1,	0x01,	0x02, 0xF,  0x01},
{	tableK,		1 << 11,	1024,      0,	0,	cat1,	0x01,	0x02, 0xF,  0x02},
{	tableDCP,	2 << 11,	 480,	0x10,	0,	cat1,	0x01,	0x02, 0xF,  0x12},
{	tableDCC,	3 << 11,	1024,	   1,	0,	cat1,	0x01,	0x02, 0xF,  0x1B},
{	tableSTW,	4 << 11,	1024,	   1,	0,	cat1,	0x01,	0x02, 0xF,  0x1A},
{	tableTCC,	8 << 11,	 128,	   1, 129,	cat1,	0x01,	0x02, 0xF,  0x0F},
{	tableTCP,	8 << 11,	 128,	   1,   1,	cat1,	0x01,	0x02, 0xF,  0x0E},
{	tableWX,       12 << 11,	1024,	   1,	0,	cat1,	0x01,	0x02, 0xF,  0x09},
{	tableWY,       14 << 11,	1024,	   1,	0,	cat1,	0x01,	0x02, 0xF,  0x0A},
{	tableC,		0 << 12,	 512,	   0,	0,	cat1,	0x12,	0x14, 0xE,  0x05},
{	tableX,		4 << 12,	1024,	   0,	0,	cat1,	0x12,	0x14, 0xE,  0x03},
{	tableY,		6 << 12,	1024,	   0,	0,	cat1,	0x12,	0x14, 0xE,  0x04}, 
{	tableCP,	0 << 12,	 512,	   0,   0,      cat1,   0x9D,	0x9E, 0xE,  0x08},
{	tableXP,	4 << 12,	1024,	   0,   0,	cat1,   0x9D,   0x9E, 0xE,  0x06},
{	tableYP,	6 << 12,	1024,	   0,   0,	cat1,   0x9D,	0x9E, 0xE,  0x07},
/*
// Variable Data
{	tableLPV,	1 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	}, 			
{	tableLSP,	2 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableLMN,	3 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableLMX,	4 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableLERR,	5 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableLKC,	6 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableLTD,	7 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableLTI,	8 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableLVF,	9 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableLRSF,     10 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableAPV,      11 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableASP,      12 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},
{	tableAVF,      13 <<  0,	 512,	   0,   0,	cat2,	0x7F,	0x5A,   0	},

// Extended Variable Data

{	tableDXW,      52 <<  0,	8192,	   0,   0,	cat2,	0x7F,	0x5A,   0	}, 			
*/

};

// Constructor

CNitpDriver::CNitpDriver(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	m_pArea	= (Area FAR *)m_Area;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;
	
	m_uPtr		= 0;
	}

// Destructor

CNitpDriver::~CNitpDriver(void)
{
	}

// Entry Points

CCODE MCALL CNitpDriver::Ping(void)
{
	DWORD    Data[1];
	
	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 1;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CNitpDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	FindItem(Addr.a.m_Table);

	if( m_pItem ) {

		MakeCount(Addr.a.m_Type, uCount, FALSE); 

		Begin(FALSE);

		Construct(Addr, uCount, NULL);

		End();
	       
		if( Transact() ) {

			switch( Addr.a.m_Type ) {

				case addrBitAsBit:	
					
					MakeMin(uCount, m_uPtr - 2);
					
					if( IsBlockOp() ) {

						GetPackedBitAsBit(pData, uCount);
						}
					else {
						GetBits(pData, uCount);
						}
					break;

				case addrBitAsByte:

					MakeMin(uCount, m_uPtr - 2);
					GetPackedBits(pData, uCount);
					break;

				case addrWordAsWord:	
					
					MakeMin(uCount, (m_uPtr - 2) / 2);
					GetWords(pData, uCount);	
					break;

				case addrWordAsLong:
				case addrWordAsReal:	
					
					MakeMin(uCount, (m_uPtr - 2) / 4 );
					GetLongs(pData, uCount);	
					break;
				}  
			
			return uCount;
			}

		return CCODE_ERROR;	
		}

	return CCODE_ERROR | CCODE_HARD; 
	}

CCODE MCALL CNitpDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{	
	FindItem(Addr.a.m_Table);

	if( m_pItem ) {

		MakeCount(Addr.a.m_Type, uCount, TRUE);

		Begin(TRUE);

		Construct(Addr, uCount, pData);

		End();
	
		if( Transact() ) {

			return uCount;
			}		
		
		return CCODE_ERROR;	
		}

	return CCODE_ERROR | CCODE_HARD; 
	}
 
// Implementation

void CNitpDriver::FindItem(UINT uID)
{
	m_pItem = m_pArea;
	
	for ( UINT u = 0; u < elements(m_Area); u++ ) {

		if ( m_pItem->m_bTable == uID ) {

			return;
			}

		m_pItem++;
		}

	m_pItem = NULL;	
	}

void CNitpDriver::Begin(BOOL fWrite)
{
 	m_uPtr = 0;

	m_bTxBuff[m_uPtr++] = ':';

	m_bTxBuff[m_uPtr++] = 0;

	m_bTxBuff[m_uPtr++] = 0;

	if( IsBlockOp() ) {

		AddTaskCode(fWrite);
		
		return;
		}

	AddByte(fWrite ? m_pItem->m_bWrite : m_pItem->m_bRead);
	}

void CNitpDriver::Add(BYTE bValue)
{
	m_bTxBuff[m_uPtr++] = m_pHex[bValue / 16];
	m_bTxBuff[m_uPtr++] = m_pHex[bValue % 16];
	}

void CNitpDriver::AddByte(BYTE bByte)
{
	Add(bByte);
	}
	
void CNitpDriver::AddWord(WORD wValue)
{
	Add(wValue / 256);

	Add(wValue % 256);
	}

void CNitpDriver::AddLong(DWORD dwValue)
{
	AddWord(HIWORD(dwValue));

	AddWord(LOWORD(dwValue));
	}

void CNitpDriver::GetBits(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		pData[n] = (m_bRxBuff[n + 2] ? 1 : 0);
		}
	}

void CNitpDriver::GetPackedBitAsBit(PDWORD pData, UINT uCount)
{
	PBYTE pByte = PBYTE(m_bRxBuff + 2);
	
	for( UINT n = 0; n < uCount; n++ ) {

		pData[n] = (*pByte >> (n % 8)) & 0x1;

		if( n + 1 % 8 == 0 ) {

			pByte++;
			}
		}
	}

void CNitpDriver::GetPackedBits(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		pData[n] = m_bRxBuff[n + 2];
		}
	}

void CNitpDriver::GetWords(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		WORD x   = PU2(m_bRxBuff + 2)[n];
			
		pData[n] = LONG(SHORT(MotorToHost(x)));
		}
	}

void CNitpDriver::GetLongs(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = PU4(m_bRxBuff + 2)[n];
			
		pData[n] = MotorToHost(x);
		}
	}

void CNitpDriver::Construct(AREF Addr, UINT uCount, PDWORD pData)
{
	if( m_pItem ) {

		UINT uOffset = Addr.a.m_Offset;

		if( IsLong(Addr.a.m_Type) ) {

			uCount *= 2;
			}

		if( IsBlockOp() ) {

			if( Addr.a.m_Type == addrBitAsByte ) {

				uCount = uCount * 8;
				}

			if( pData ) {

				MakeBlockWrite(Addr, uCount, pData);

				return;
				}

			MakeBlockRead(Addr, uCount);

			return;
			}
		
		for( UINT u = uOffset, n = 0; u < uOffset + uCount; u++ ) {
		
			switch( m_pItem->m_bCat ) {

				case cat1:	
					
					MakeCategory1(u);
					
					break;

				case cat2:

					if( !pData || u == uOffset ) {
					
						MakeCategory2(u, Addr.a.m_Type);
						}

					if( !pData ) {

						// TODO:  Specify count ???

						return;
						}

					break;
				}

			if( pData ) {

				WORD wLocal    = 0;

				switch( Addr.a.m_Type ) {

					case addrBitAsBit:	
						
						wLocal = WORD(pData[n] ? 0x3 : 0x2);
								
						Add(wLocal & 0xFF);

						n++;
								
						break;

					case addrWordAsWord:	
						
						AddWord(pData[n] & 0xFFFF);

						n++;
								
						break;

					case addrWordAsLong:
					case addrWordAsReal:

						if( Addr.a.m_Offset % 2 ) {

							wLocal = ((u % 2 == 0) ? LOWORD(pData[n]) : HIWORD(pData[n++]));
							}
						else {
							wLocal = ((u % 2 == 0) ? HIWORD(pData[n]) : LOWORD(pData[n++]));
							}
						
						AddWord(wLocal);
						
						break;
					}
				}
			}
		}
	}

void CNitpDriver::MakeCategory1(UINT uElement)
{
	UINT uPage   = FindPage(uElement); 
	
	if( uPage > 0 ) {			

		AddWord((m_pItem->m_bKey << 11) | (uPage & 0x3FF));
		}

	AddWord(m_pItem->m_uWC | (FindOffset(uElement) & 0x7FF));	
	}

void CNitpDriver::MakeCategory2(UINT uElement, UINT uType)
{
	if( m_pItem ) {

		BYTE bShift = !(uType == addrWordAsWord); 

		if( m_pItem->m_uWC >= 0xF ) {

			AddWord((bShift << 15) | (0x1FF << 6) | m_pItem->m_uWC);
			
			AddWord(uElement);

			return;
			}

		AddWord((bShift << 15) | (0x1F << 10) | ((m_pItem->m_uWC & 0xF) << 6) | uElement);
		}
	}

void CNitpDriver::MakeBlockRead(AREF Addr, UINT uCount)
{
	AddElement(Addr.a.m_Offset, uCount);
	}

void CNitpDriver::MakeBlockWrite(AREF Addr, UINT uCount, PDWORD pData)
{
	AddElement(Addr.a.m_Offset, uCount);

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			if( IsBlockOp() ) {

				AddPackedBitAsBit(uCount, pData);
				}
			else {
				AddBits(uCount, pData);
				}
			
			return;

		case addrBitAsByte:

			AddPackedBits(uCount, pData);
			return;

		case addrWordAsWord:

			AddWords(uCount, pData);
			return;
			

		case addrWordAsLong:
		case addrWordAsReal:

			AddLongs(uCount, pData);
			return;
		}
	}

void CNitpDriver::AddElement(UINT uOffset, UINT uCount)
{
	
	}

void CNitpDriver::AddTaskCode(BOOL fWrite)
{

	}

void CNitpDriver::AddBits(UINT uCount, PDWORD pData)
{
	for( UINT u = 0; u < uCount; u++ ) {

		WORD wLocal = WORD(pData[u] ? 0xFF : 0x00);
								
		Add(wLocal);
		}
	}

void CNitpDriver::AddPackedBitAsBit(UINT uCount, PDWORD pData)
{
	BYTE bByte = 0;
	
	for( UINT u = 0; u < uCount; u++ ) {

		bByte |= (( pData[u] ? 1 : 0 ) << ( u % 8));

		if( (u + 1) % 8 == 0 ) {

			AddByte(bByte);

			bByte = 0;
			}
		}

	if( u % 8 ) {

		AddByte(bByte);
		}
	}

void CNitpDriver::AddPackedBits(UINT uCount, PDWORD pData)
{
	for( UINT u = 0; u < uCount / 8; u++ ) {

		AddByte(pData[u] & 0xFF);
		}
	}
 
void CNitpDriver::AddWords(UINT uCount, PDWORD pData)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddWord(pData[u] & 0xFFFF);
		}
	}

void CNitpDriver::AddLongs(UINT uCount, PDWORD pData)
{
	for( UINT u = 0; u < uCount / 2; u++ ) {

		AddLong(pData[u]);
		}
	}

void CNitpDriver::End(void)
{
	WORD Len = m_uPtr + 5; 

	m_bTxBuff[1] = m_pHex[Len / 16];

	m_bTxBuff[2] = m_pHex[Len % 16];

	AddWord(MakeCheck(m_bTxBuff));

	m_bTxBuff[m_uPtr++] = ';';
	}

UINT CNitpDriver::FindPage(UINT uElement)
{
  	UINT uPage = 0;

	if( m_pItem ) {

		switch( m_pItem->m_bTable ) {

			case tableV:
			case tableK:
			case tableDCC:
			case tableSTW:
			case tableWX:
			case tableWY:
			case tableTCC:
			case tableTCP:
			case tableC:
			case tableX:
			case tableY:
			case tableCP:
			case tableXP:
			case tableYP:
		     		
				return (uElement - 1) / m_pItem->m_wSize;

			case tableDCP:

				return (uElement - 1) / (m_pItem->m_wSize / m_pItem->m_bFirst);
			}     
		}

	return uPage;
	}

UINT CNitpDriver::FindOffset(UINT uElement)
{
	UINT uOffset = 0;

	if( m_pItem ) {

		switch( m_pItem->m_bTable ) {

			case tableV:
			case tableK:
			case tableTCC:
			case tableTCP:
		
				return ((uElement - 1) % m_pItem->m_wSize) + m_pItem->m_bNext;

			case tableDCP:

				return (((uElement - 1) % (m_pItem->m_wSize / m_pItem->m_bFirst)) + 1) * 16;
			
			case tableDCC:
			case tableSTW:
			case tableWX:
			case tableWY:
			case tableC:
			case tableX:
			case tableY:
			case tableCP:
			case tableXP:
			case tableYP:

				return (uElement - 1) % m_pItem->m_wSize + 1;
			}
		}

	return uOffset;
	}

UINT CNitpDriver::FindElement(UINT uPage, UINT uOffset)
{
	UINT uElement = 0;

	if( m_pItem ) {

		switch( m_pItem->m_bTable ) {

			case tableV:
			case tableK:

				return uPage * m_pItem->m_wSize + 1 + uOffset;

			case tableDCP:

				return uPage * (m_pItem->m_wSize / m_pItem->m_bFirst) + uOffset / 16;
			
			case tableDCC:
			case tableSTW:
			case tableWX:
			case tableWY:

				return uPage * m_pItem->m_wSize + uOffset;

			case tableTCC:
			case tableTCP:

				return uPage * m_pItem->m_wSize + ((uOffset - 1) % m_pItem->m_wSize) + 1;

			}
		}

	return uElement;
	}

// Helpers

WORD CNitpDriver::Swap(WORD wData)
{
	return ((wData & 0x00FF) << 8) | ((wData & 0xFF00) >> 8);
	}

BYTE CNitpDriver::FromAscii(BYTE bByte)
{
	if( bByte >= '0' ) {
	
		if( bByte <= '9' ) {

			return bByte - '0';
			}

		return	bByte - '@' + 9;
		}

	return bByte;
	}

void CNitpDriver::BuffFromAscii(void)
{
	for( UINT u = 0, a = 0;  a < m_uPtr; u++, a += 2 ) {

		m_bRxBuff[u] = (FromAscii(m_bRxBuff[a]) << 4) | FromAscii(m_bRxBuff[a + 1]);
		}

	m_uPtr = m_uPtr / 2;
	}

BOOL CNitpDriver::IsLong(UINT uType)
{
	return (uType == addrWordAsLong || uType == addrWordAsReal);
	}

void CNitpDriver::MakeCount(UINT uType, UINT& uCount, BOOL fWrite)
{
	BOOL fBlock = IsBlockOp();

	UINT Count = uCount;

	if( fWrite ) {

		switch( uType ) {

			case addrBitAsBit:

				Count = fBlock ? 26 : 5;
				break;

			case addrBitAsByte:

				Count = 208;
				break;

			case addrWordAsWord:

				Count = fBlock ? 13 : 5;
				break;

			case addrWordAsLong:
			case addrWordAsReal:

				Count = fBlock ?  6 : 2;
				break;
			}
		}

	else {

		switch( uType ) {

			case addrBitAsBit:

				Count = fBlock ? 31 : 7;
				break;

			case addrBitAsByte:

				Count = 248;
				break;
		
			case addrWordAsWord:

				Count = fBlock ? 15 : 7;
				break;

			case addrWordAsLong:
			case addrWordAsReal:

				Count = fBlock ?  7 : 3;
				break;
			}
		}

	MakeMin(uCount, Count);
	} 

WORD CNitpDriver::MakeCheck(PBYTE pFrame)
{
	PBYTE pBuff = PBYTE(alloca(m_uPtr));
       
	for( UINT u = 0, a = 1;  a < m_uPtr; u++, a += 2 ) {

		pBuff[u] = (FromAscii(pFrame[a]) << 4) | FromAscii(pFrame[a + 1]);
		}

	UINT uMod = u % 4;

	UINT uIndex = u;

	while( uMod ) {

		pBuff[uIndex] = 0;

		uIndex++;

		uMod--;
		}

	m_wCheck = 0;

	for( UINT i = 0; i < uIndex / 2; i++ ) {

		WORD wCheck = PWORD(pBuff)[i];

		m_wCheck += (SHORT(MotorToHost(wCheck)));
		}

	return (-m_wCheck); 
	}

// Transport Layer

BOOL CNitpDriver::Transact(void)
{
	return FALSE;
	}

BOOL CNitpDriver::RecvFrame(void)
{
	return FALSE;
	}

BOOL CNitpDriver::CheckFrame(void)
{
	BuffFromAscii();

	m_uPtr -= 2;

	WORD wTarget = *(PU2(&m_bRxBuff[m_uPtr]));

	UINT uMod = m_uPtr % 4;

	while( uMod ) {

		m_bRxBuff[m_uPtr] = 0;

		m_uPtr++;

		uMod--;
		}

	WORD wCheck = 0;

	for( UINT u = 0; u < m_uPtr; u += sizeof(WORD) ) {

		WORD x = PU2(m_bRxBuff + u)[0];
			
		wCheck += (SHORT(MotorToHost(x)));
		}

	wCheck = -wCheck & 0xFFFF; 

	if( wTarget != MotorToHost(wCheck) ) {

		return FALSE;
		}

	if( m_bRxBuff[1] == 0 ) {

		return FALSE;
		} 

	return TRUE;
	}

BOOL CNitpDriver::IsBlockOp(void)
{
	return FALSE;
	}

// End of File
