//////////////////////////////////////////////////////////////////////////
//
// TI 500 NITP Structure

struct FAR Area{

		BYTE m_bTable;
		UINT m_uWC;
		WORD m_wSize;
		BYTE m_bFirst;
		BYTE m_bNext;
		BYTE m_bCat;
		BYTE m_bRead;
		BYTE m_bWrite;
		BYTE m_bKey;
		BYTE m_bPLCtt;
		};

//////////////////////////////////////////////////////////////////////////
//
// TI 500 NITP Enumerations
//

enum
{
	tableV		= 1,
	tableK		= 2,
	tableDCP	= 3,
	tableDCC	= 4,
	tableSTW	= 5,
	tableTCC	= 8,
	tableTCP	= 9,
	tableWX		= 13,
	tableWY		= 15,
	tableC		= 0x21,
	tableCP		= 0x22,
	tableX		= 0x25,
	tableXP		= 0x26,
	tableY		= 0x27,
	tableYP		= 0x28,
/*	tableLPV	= 0x31,
	tableLSP	= 0x32,
	tableLMN	= 0x33,
	tableLMX	= 0x34,
	tableLERR	= 0x35,
	tableLKC	= 0x36,
	tableLTD	= 0x37,
	tableLTI	= 0x38,
	tableLVF        = 0x39,
	tableLRSF	= 0x3A,
	tableAPV	= 0x3B,
	tableASP	= 0x3C,
	tableAVF	= 0x3D,

	tableDXW	= 0x61,
 */    	};

enum
{
	cat1		= 1,
	cat2		= 2,
	cat3		= 3,
	cat4		= 4,
     	};

//////////////////////////////////////////////////////////////////////////
//
// TI 500 Series NITP Base Driver
//

class CNitpDriver : public CMasterDriver
{
	public:
		// Constructor
		CNitpDriver(void);

		// Destructor
		~CNitpDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Data Members
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		LPCTXT	   m_pHex;
		WORD	   m_wCheck;

		static Area CODE_SEG m_Area[];
		Area FAR * m_pArea;
		Area FAR * m_pItem;

			
		// Implementation

		void FindItem(UINT uID);
		void Begin(BOOL fWrite);
		void Add(BYTE bByte);
		void AddByte(BYTE bByte);
		void AddWord(WORD wWord);
		void AddLong(DWORD dwLong);
		void GetBits(PDWORD pData, UINT uCount);
		void GetPackedBitAsBit(PDWORD pData, UINT uCount);
		void GetPackedBits(PDWORD pData, UINT uCount);
		void GetWords(PDWORD pData, UINT uCount);
		void GetLongs(PDWORD pData, UINT uCount);
		void Construct(AREF Addr, UINT uCount, PDWORD pData);
		void MakeCategory1(UINT uElement);
		void MakeCategory2(UINT uElement, UINT uType);
		void MakeBlockRead(AREF Addr, UINT uCount);
		void MakeBlockWrite(AREF Addr, UINT uCount, PDWORD pData);
		void AddBits(UINT uCount, PDWORD pData);
		void AddPackedBitAsBit(UINT uCount, PDWORD pData);
		void AddPackedBits(UINT uCount, PDWORD pData);
		void AddWords(UINT uCount, PDWORD pData);
		void AddLongs(UINT uCount, PDWORD pData);
		void End(void);
		UINT FindPage(UINT uElement);
		UINT FindOffset(UINT uElement);
		UINT FindElement(UINT uPage, UINT uOffset);

		// Helpers
		WORD Swap(WORD wData);
		BYTE FromAscii(BYTE bByte);
		void BuffFromAscii(void);
		BOOL IsLong(UINT uType);
		void MakeCount(UINT uType, UINT& uCount, BOOL fWrite);
		WORD MakeCheck(PBYTE pFrame);

		
		// Transport Layer
		virtual BOOL Transact(void);
		virtual BOOL RecvFrame(void);
		virtual BOOL CheckFrame(void);
		

		// Specific Implementation
		virtual BOOL IsBlockOp(void);
		virtual	void AddElement(UINT uOffset, UINT uCount);
		virtual void AddTaskCode(BOOL fWrite);
	};

// End of file

