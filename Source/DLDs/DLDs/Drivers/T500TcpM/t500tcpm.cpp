#include "intern.hpp"

#include "t500tcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TI 500 Master TCP Driver
//

// Instantiator

INSTANTIATE(CT500TcpM);

// Constructor

CT500TcpM::CT500TcpM(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CT500TcpM::~CT500TcpM(void)
{
	}

// Configuration

void MCALL CT500TcpM::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CT500TcpM::Attach(IPortObject *pPort)
{
	}

void MCALL CT500TcpM::Open(void)
{
	}

// Device

CCODE MCALL CT500TcpM::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_fBlock = GetByte(pData) ? TRUE : FALSE;
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CT500TcpM::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CT500TcpM::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}
		}

	return CNitpDriver::Ping();
	
	}

// Socket Management

BOOL CT500TcpM::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CT500TcpM::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {
	
		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}
				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}
				
			CloseSocket(TRUE); 

			return FALSE;
			}
		}

	return FALSE;
	}

void CT500TcpM::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CT500TcpM::Transact(void)
{
	if( OpenSocket() ) {

		if( Send() && RecvFrame() ) {
		
			return CheckFrame();
			}

		CloseSocket(TRUE); 
		}

	return FALSE; 
	}

BOOL CT500TcpM::Send(void)
{
	UINT uSize = m_uPtr;

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < uSize; u++ ) {

		AfxTrace("%c", m_bTxBuff[u]);
		} */

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CT500TcpM::RecvFrame(void)
{
	m_uPtr      = 0;

	UINT uSize  = 0;

	UINT uTotal = 0;

//	AfxTrace("\nRx : ");

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - m_uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + m_uPtr, uSize);

		if( uSize ) {

			for( UINT u = m_uPtr; u < m_uPtr + uSize; u++ ) {

//				AfxTrace("%c", m_bRxBuff[u]);
 
				if( m_bRxBuff[u] == ';' && m_bRxBuff[0] == ':' ) {

					m_uPtr += uSize;

					m_uPtr -= 4;

					memcpy(m_bRxBuff, m_bRxBuff + 1, m_uPtr);

					return TRUE;
					} 
				} 
			
			m_uPtr += uSize;
			
			continue;  
			}

		if( !CheckSocket() ) {
			
			return FALSE;
			}

		Sleep(10);
		} 

	return FALSE;
	}

BOOL CT500TcpM::IsBlockOp(void)
{
	if( m_pCtx->m_fBlock ) {

		return TRUE;
		}

	if( m_pItem ) {

		if( m_pItem->m_bRead == 0x9D ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CT500TcpM::AddElement(UINT uOffset, UINT uCount)
{
	if( m_pItem ) {

		UINT uType = m_pItem->m_bPLCtt;

		// Force packed bits

		switch( m_pItem->m_bTable ) {

			case tableC:
			case tableCP:

				uType = 8;
				break;

			case tableX:
			case tableXP:

				uType = 6;
				break;

			case tableY:
			case tableYP:

				uType = 7;
				break;
			}

	
		AddByte(uCount & 0xFF);

		AddByte(uType);

		AddByte(0x00);

		AddWord(uOffset - 1);
		}
	}

void CT500TcpM::AddTaskCode(BOOL fWrite)
{
	if( m_pItem ) {

		AddByte(fWrite ? 0x9E : 0x9D);
		}
	}

// End of File
