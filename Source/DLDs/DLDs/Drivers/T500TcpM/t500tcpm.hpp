#include "nitp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TI 500 Master TCP Driver
//

class CT500TcpM : public CNitpDriver
{
	public:
		// Constructor
		CT500TcpM(void);

		// Destructor
		~CT500TcpM(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		
	protected:
		// Device Context
		struct CContext 
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL	 m_fBlock;
			};

		// Data Members
		CContext * m_pCtx;
		UINT	   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Transport Layer
		BOOL Send(void);
		BOOL RecvFrame(void);
		BOOL Transact(void);
		
		// Specific
		BOOL IsBlockOp(void);
		void AddElement(UINT uOffset, UINT uCount);
		void AddTaskCode(BOOL fWrite);

	};

// End of File
