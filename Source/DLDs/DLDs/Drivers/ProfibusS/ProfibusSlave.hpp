
//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP Slave Driver
//

class CProfibusDPSlave : public CSlaveDriver
{
	public:
		// Constructor
		CProfibusDPSlave(void);

		// Destructor
		~CProfibusDPSlave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Entry Points
		DEFMETH(void) Service(void);

	protected:
		// Data Members
		IProfibusPort * m_pPort;
		BYTE            m_bStation;

		// Implementation
		void CheckWrites(void);
		void CheckReads(void);
	};

// End of File
