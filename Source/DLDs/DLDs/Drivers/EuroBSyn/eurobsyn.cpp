
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "eurobsyn.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 590 Ascii Driver
//

// Instantiator

INSTANTIATE(CEurothermBiSynchDriver);

// Constructor

CEurothermBiSynchDriver::CEurothermBiSynchDriver(void)
{
	m_Ident	    = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Destructor

CEurothermBiSynchDriver::~CEurothermBiSynchDriver(void)
{
	}

// Configuration

void MCALL CEurothermBiSynchDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CEurothermBiSynchDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CEurothermBiSynchDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEurothermBiSynchDriver::Open(void)
{
	}

// Device

CCODE MCALL CEurothermBiSynchDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx	= new CContext;

			m_pCtx->m_GID = m_pHex[GetByte(pData)];
			m_pCtx->m_UID = m_pHex[GetByte(pData)];

			m_pCtx->m_fChanSel = GetByte(pData);
			m_pCtx->m_Channel  = m_pHex[LOBYTE(GetWord(pData))];

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEurothermBiSynchDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEurothermBiSynchDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = PARAM_WORD;
	Addr.a.m_Offset = (UINT)('I' << 8) + (UINT)('I');
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	if( Read(Addr, Data, 1 ) == 1 ) {

		return 1;
		}

	m_pData->Write( EOT, FOREVER );

//**/	AfxTrace0("*** EOT ***");

	return CCODE_ERROR;
	}

CCODE MCALL CEurothermBiSynchDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nREAD T=%d O=%4.4x T=%x ", Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Type);

	switch( Addr.a.m_Table ) {

		case PARAM_REAL:
			return DoRealRead(Addr.a.m_Offset, pData);

		case PARAM_WORD:
			return DoWordRead(Addr.a.m_Offset, Addr.a.m_Table, pData);

		case PARAM_BOOL:
			return DoBitRead(Addr.a.m_Offset, pData);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CEurothermBiSynchDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nWRITE T=%d O=%4.4x T=%x ", Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Type); AfxTrace1("D=%8.8lx ", *pData);

	switch( Addr.a.m_Table ) {

		case PARAM_REAL:
			return DoRealWrite(Addr.a.m_Offset, *pData);

		case PARAM_WORD:
			return DoWordWrite(Addr.a.m_Offset, Addr.a.m_Table, *pData);

		case PARAM_BOOL:
			return DoBitWrite(Addr.a.m_Offset, Addr.a.m_Table, *pData);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

// Port Access

UINT CEurothermBiSynchDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CEurothermBiSynchDriver::StartFrame( void )
{
	m_uPtr = 0;
	
	AddByte( EOT );

	AddByte( m_pCtx->m_GID );
	AddByte( m_pCtx->m_GID );

	AddByte( m_pCtx->m_UID );
	AddByte( m_pCtx->m_UID );
	}

void CEurothermBiSynchDriver::EndFrame(BOOL fIsWrite)
{
	if( !fIsWrite ) {

		AddByte( ENQ );
		}

	else {

		AddByte( ETX );

		AddByte( m_bCheck );
		}
	}

void CEurothermBiSynchDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {
	
		m_bTx[m_uPtr] = bData;

		m_bCheck ^= bData;

		m_uPtr++;
		}
	}

void CEurothermBiSynchDriver::AddValue(char * pV)
{
	strcpy( (char *)&m_bTx[m_uPtr], pV );

	m_uPtr += strlen(pV);
	}

void CEurothermBiSynchDriver::PutInteger(DWORD dData, BOOL fNotHex)
{
	char c[6];

	SPrintf( c,
		fNotHex ? "%d" : ">%X",
		dData
		);

	AddValue(c);
	}
		
// Transport Layer

BOOL CEurothermBiSynchDriver::Send(void)
{
	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );

	return TRUE;
	}

UINT CEurothermBiSynchDriver::GetReply(BOOL fIsWrite)
{
	UINT uState;

	UINT uPtr   = 0;

	UINT uTimer = 0;
	
	BYTE bCheck = 0;

	UINT uError = 0;

	uState = fIsWrite ? 10 : 0;

	UINT uDevID = 5;

//**/	AfxTrace0("\r\n.");

	SetTimer(1000);
	
	while( (uTimer = GetTimer()) ) {

		UINT uByte;
		
		if( (uByte = RxByte(uTimer)) == NOTHING ) {
			
			continue;
			}

		bCheck ^= uByte;

//**/		AfxTrace1("<%2.2x>", uByte );

		switch( uState ) {
		
			case 0:
				if( uByte == STX ) {

					uState = 1;

					bCheck = 0;

					if( m_pCtx->m_fChanSel ) {

						uState = 11;
						}
					}

				else {
					if( uByte == EOT ) {

						m_bRx[0] = EOT;

						return 1;
						}
					}

				break;
				
			case 1:
				if( uByte != m_bTx[uDevID++] )

					uError = 1;

				uState = 2;

				break;
				
			case 2:
				if( uByte != m_bTx[uDevID] )

					uError = 1;

				uState = 3;

				uPtr = 0;

				break;

			case 3:
				m_bRx[uPtr++] = uByte;

				if( uPtr > sizeof(m_bRx) )
					return FALSE;

				if( uByte == ETX ) {

					uState = 4;

					m_bRx[uPtr-1] = 0;
					}

				break;

			case 4:

				if( uError )

					return 2;

				return( (bCheck & 0x7F) == 0 );

			case 10:
				if( uByte == ACK )

					return 0;

				else {
					m_bRx[0] = uByte;

					return 1;
					}
				break;

			case 11:
				if( m_bTx[uDevID++] != uByte ) {

					uError = 1;
					}

				uState = 1;

				break;
			}
		}

	return 2;
	}

BOOL CEurothermBiSynchDriver::Transact(BOOL fIsWrite)
{
	EndFrame( fIsWrite );

	if( Send() ) {

		if( m_pCtx->m_GID == 0x7F || m_pCtx->m_UID == 0x7F ) {

			m_pData->Read(0); // ensure transmit of full frame

			return TRUE; // Broadcast
			}

		return GetReply(fIsWrite) > 1 ? FALSE : TRUE;
		}
		
	return FALSE;
	}

// Frame Formatting

void CEurothermBiSynchDriver::PutIdentifier(UINT uOffset, BOOL fIsWrite)
{
	StartFrame();

	if( fIsWrite ) AddByte( STX );

	m_bCheck = 0;

	if( m_pCtx->m_fChanSel ) {

		AddByte(m_pCtx->m_Channel);
		}

	AddByte( HIBYTE(uOffset) );

	AddByte( LOBYTE(uOffset) );
	}

// Read Handlers

CCODE CEurothermBiSynchDriver::DoRealRead(UINT Addr, PDWORD pData)
{
	PutIdentifier( Addr, NOTWRITE );
	
	if( Transact(NOTWRITE) ) {

		if( m_bRx[0] == EOT ) {

			pData[0] = 0;

			return 1;
			}

		pData[0] = ATOF( (const char * )&m_bRx[0] );

		return 1;
		}

	return SetCCError(FALSE);
	}

CCODE CEurothermBiSynchDriver::DoWordRead(UINT Addr, UINT uTable, PDWORD pData)
{
	char s1[32];
	char * s2     = s1;
	UINT uStart   = 0;
	DWORD dResult = 0;
	BOOL fNeg = FALSE;

	PutIdentifier( Addr, NOTWRITE );

	if( Transact(NOTWRITE) ) {

		if( m_bRx[0] == EOT ) {

			pData[0] = 0;

			return 1;
			}

		if( m_bRx[0] != '>' ) { // do Base 10 or string

			if( m_bRx[0] == '-' ) {

				fNeg = TRUE;

				uStart = 1;
				}

			if( IsADigit( m_bRx[uStart], NOTHEX ) ) {

				dResult = strtoul( (char *)(&m_bRx[uStart]), &s2, 10 );

				pData[0] = !fNeg ? dResult : -dResult;
				}

			else { // may be string character

				dResult = 0;

				for( UINT i = 0; i < 4; i++ ) {

					if( m_bRx[i] ) {

						dResult <<= 8;

						dResult += m_bRx[i];
						}

					else break;
					}

				pData[0] = dResult;
				}

			return 1;
			}

		if( IsADigit( m_bRx[1], ISHEX ) ) {

			pData[0] = strtoul( (char *)(&m_bRx[1]), &s2, 16 );

			return 1;
			}

		pData[0] = 0;

		return 1;
		}

	return SetCCError(FALSE);
	}

CCODE CEurothermBiSynchDriver::DoBitRead(UINT Addr, PDWORD pData)
{
	PutIdentifier( Addr, NOTWRITE );
	
	if( Transact(NOTWRITE) ) {

		UINT u = m_bRx[0] == '>' ?  2 :  0;
			
		*pData = m_bRx[u] != '0' ? 1L : 0L;

		return 1;
		}

	return SetCCError(FALSE);
	}

// Write Handlers

CCODE CEurothermBiSynchDriver::DoRealWrite(UINT Addr, DWORD dData)
{
	char c[14] = {0};

	SPrintf( c,
		"%.4f",
		PassFloat( dData )
		);

	for( UINT i = strlen(c) - 1; c[i] == '0'; i-- ) {

		c[i] = 0;
		}

	PutIdentifier( Addr, ISWRITE );

	AddValue(c);

	if( Transact(ISWRITE) ) return 1;

	return SetCCError(TRUE);
	}

CCODE CEurothermBiSynchDriver::DoWordWrite(UINT Addr, UINT uTable, DWORD dData)
{
	PutIdentifier( Addr, ISWRITE );

	PutInteger( dData, TRUE );

	if( Transact(ISWRITE) ) {

		return 1;
		}

	if( m_bRx[0] == NAK ) { // try hex write

		PutIdentifier( Addr, ISWRITE );

		PutInteger( dData, FALSE );

		if( Transact(ISWRITE) ) {

			return 1;
			}
		}

	return SetCCError(TRUE);
	}

CCODE CEurothermBiSynchDriver::DoBitWrite(UINT Addr, UINT uTable, DWORD dData)
{
	PutIdentifier( Addr, ISWRITE );

	AddByte( '>' );

	AddByte( dData ? '1' : '0' );

	if( Transact(ISWRITE) ) return 1;

	if( m_bRx[0] == NAK ) { // try integer write

		PutIdentifier( Addr, ISWRITE );

		AddByte( dData ? '1' : '0' );

		AddByte( '.' );

		if( Transact(ISWRITE) ) return 1;
		}

	return SetCCError(TRUE);
	}

// Helpers

BOOL CEurothermBiSynchDriver::IsADigit( BYTE b, BOOL fIsHex )
{
	if( b >= '0' && b <= '9' ) return TRUE;

	if( b == '-' || b == '+' ) return !fIsHex;

	if( b >= 'A' && b <= 'F' ) return fIsHex;

	if( b >= 'a' && b <= 'f' ) return fIsHex;

	return FALSE;
	}

CCODE CEurothermBiSynchDriver::SetCCError(BOOL fIsWrite)
{
	if( !fIsWrite && m_bRx[0] == EOT ) return 1;

	if( m_bRx[0] == NAK ) {

		return fIsWrite ? 1 : CCODE_ERROR | CCODE_NO_RETRY;
		}

	return CCODE_ERROR;
	}

// End of File
