
#include "intern.hpp"

#include "EIpwrTCP.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver
//

// Instantiator

INSTANTIATE(CEIepowerTCPDriver);

// Constructor

CEIepowerTCPDriver::CEIepowerTCPDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_uKeep	    = 0;

	m_pAddrArr	= NULL;
	m_pPosnArr	= NULL;
	m_pTypeArr	= NULL;
	m_pSortDataArr	= NULL;
	m_pSortPosnArr	= NULL;
	m_pSortTypeArr	= NULL;
	}

// Destructor

CEIepowerTCPDriver::~CEIepowerTCPDriver(void)
{
	ClearArr();
	}

// Configuration

void MCALL CEIepowerTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CEIepowerTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CEIepowerTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CEIepowerTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP1		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->uArrCount	= 0;
			m_pCtx->uWriteErrCt	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEIepowerTCPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CEIepowerTCPDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;
	
	return 0;
	}

// Entry Points

CCODE MCALL CEIepowerTCPDriver::Ping(void)
{
	if( OpenSocket() ) {

//**/	AfxTrace1("\r\nPing Ping Ping Ping Ping %d\r\n", m_pCtx->m_bDrop);

		if( m_pCtx->m_bUnit == 255 ) {
			return CCODE_SUCCESS;
			}
		
		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = SP_4;
		
		Addr.a.m_Offset = 1;
		
		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;

		UINT uCount = 1;

		return Read(Addr, Data, uCount);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEIepowerTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	if( !m_pCtx->m_bUnit) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

//**/	Sleep(100); // Slow down for debug

	return HandleRead(Addr, pData, uCount);
	}

CCODE MCALL CEIepowerTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	return HandleWrite(Addr, pData, uCount);
	}

// Frame Building

void CEIepowerTCPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;
	
	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);
	}

void CEIepowerTCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {

		m_bTxBuff[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CEIepowerTCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CEIepowerTCPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CEIepowerTCPDriver::Transact(BOOL fIgnore)
{
	if( SendFrame() && RecvFrame() ) {
	
		if( fIgnore ) {

			return TRUE;
			}
			
		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CEIepowerTCPDriver::SendFrame(void)
{
	m_bTxBuff[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

//**/		AfxTrace1("\r\nSend %d ", uSize); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("[%2.2x]", m_bTxBuff[k]);

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEIepowerTCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

//**/			AfxTrace1("--- Recv %d\r\n", uSize); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("<%2.2x>", m_bRxBuff[k]);

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRxBuff[5];

				if( uPtr >= uTotal ) {

					if( m_bRxBuff[0] == m_bTxBuff[0] ) {

						if( m_bRxBuff[1] == m_bTxBuff[1] ) {

							memcpy(m_bRxBuff, m_bRxBuff + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRxBuff[n] = m_bRxBuff[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CEIepowerTCPDriver::CheckFrame(void)
{
	if( !(m_bRxBuff[1] & 0x80) ) {
	
		return TRUE;
		}

	return FALSE;
	}

// EPower Handling
CCODE CEIepowerTCPDriver::HandleRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\n\nHandle Read %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	CCODE c = CCODE_ERROR;

	if( Addr.a.m_Table == SP_4 ) {

		c = HandleModbusRead(Addr, pData, uCount);

		if( c & CCODE_ERROR ) {

			Sleep(40);

//**/			AfxTrace0("\r\n^^^^^^^^^^");

			c = HandleModbusRead(Addr, pData, uCount);
			}
		}

	else {
		m_uTickCount = GetTickCount();

		c = HandleEPower(Addr, pData, uCount, FALSE);

		if( c & CCODE_ERROR ) {

			if( GetTickCount() - m_uTickCount < 0x20 ) {

				Sleep(40);  // Serial sometimes needs a rest

//**/				AfxTrace0("\r\n&&&&&&&");

				c = HandleEPower(Addr, pData, uCount, FALSE);
				}
			}
		}

	ClearArr();

	return c;
	}

CCODE CEIepowerTCPDriver::HandleModbusRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\n\n### Modbus Read O=%d C=%d ", Addr.a.m_Offset, uCount );

	if( Addr.a.m_Type == addrWordAsWord ) {

		return DoWordRead(Addr, pData, uCount);
		}

	return DoLongRead(Addr, pData, uCount);
	}

CCODE CEIepowerTCPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nDoWordRead %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	m_bRxBuff[7] = 0;

	StartFrame(0x03);

	AddWord((WORD)Addr.a.m_Offset);
	
	AddWord((WORD)uCount);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x = PU2(m_bRxBuff + 3)[n];
			
			pData[n] = (DWORD)MotorToHost(x);
			}

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CEIepowerTCPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nDoLongRead %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	m_bRxBuff[7] = 0;

	StartFrame(0x03);

	AddWord((WORD)Addr.a.m_Offset);

	UINT uCt = uCount + 1; // since base address is doubled...
	
	AddWord((WORD)uCt);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_bRxBuff + 3)[n];

			pData[n] = (DWORD)MotorToHost(x);
			}

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CEIepowerTCPDriver::HandleWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n++++++ Write +++++ T=%d O=%x D=%8.8lx ", Addr.a.m_Table, Addr.a.m_Offset, pData[0]);

	CCODE c = CCODE_ERROR;

	if( Addr.a.m_Table == SP_4 ) {

		c = Addr.a.m_Type == WW ? DoWordWrite(Addr, pData, uCount) : DoLongWrite(Addr, pData, uCount);
		}

	else {
		c = HandleEPower(Addr, pData, uCount, TRUE);
		}

	ClearArr();

	return c;
	}

CCODE CEIepowerTCPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nWrite Word %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount); AfxTrace1("%u ", pData[0]);
	
	StartFrame(16);
		
	AddWord((WORD)Addr.a.m_Offset);
		
	AddWord((WORD)uCount);
		
	AddByte((BYTE)uCount * 2);

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = pData[n];

		AddWord(x);
		}

	if( Transact(TRUE) ) {

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}

	return ++m_pCtx->uWriteErrCt < 3 ? CCODE_ERROR : 1;
	}

CCODE CEIepowerTCPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("Write Long %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	StartFrame(16);
		
	AddWord((WORD)Addr.a.m_Offset);
		
	AddWord((WORD)uCount * 2);
		
	AddByte((BYTE)uCount * 4);

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = pData[n];

		AddLong(x);
		}

	if( Transact(TRUE) ) {

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}

	return ++m_pCtx->uWriteErrCt < 3 ? CCODE_ERROR : 1;
	}

// EPower Handling
CCODE CEIepowerTCPDriver::HandleEPower(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsWrite)
{
	uCount = fIsWrite ? 1 : min(uCount, MAXREAD);

	MakeArr(Addr.a.m_Table);

	return fIsWrite ? DoEPowerWrite(Addr, pData, uCount) : DoEPowerRead(Addr, pData, uCount);
	}

CCODE CEIepowerTCPDriver::DoEPowerRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n******* DoEPowerRead T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount); AfxTrace1("%d ", Addr.a.m_Type);

	CAddress A;

	A.m_Ref     = Addr.m_Ref;

	UINT *pSAdd = m_pSortDataArr;
	UINT *pSPos = m_pSortPosnArr;
	UINT *pSTyp = m_pSortTypeArr;
	UINT *pAdd  = m_pAddrArr;
	UINT *pPos  = m_pPosnArr;
	UINT *pTyp  = m_pTypeArr;

	DWORD pWork[MAXREAD];

	UINT uDone = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		if( GetTickCount() - m_uTickCount > 0x20 ) {

//**/			AfxTrace2("\r\nTO - Tick = %d Done=%x ", GetTickCount() - m_uTickCount, uDone);

			m_uTickCount = GetTickCount();

			return CheckDoneCt(uDone, uCount);
			}

		UINT uThis = i + Addr.a.m_Offset;

		if( pPos[uThis] < RDONE ) {	// skip read if this address has been read

			UINT uRCt = MakeBlock(uThis, uCount - i); // Get all addresses within range of uThis

//**/			AfxTrace1("\r\n=== RCT = %d === ", uRCt);

			if( !uRCt ) return CheckDoneCt(uDone, i);

//**/			AfxTrace0("\r\n"); for( UINT xx = 0; xx < uRCt; xx++ ) AfxTrace2("{A=%d P=%d}", m_pSortDataArr[xx], m_pSortPosnArr[xx]);

			UINT uSpan   = pSAdd[uRCt - 1] - pSAdd[0] + 1; // number of items to read

			A.a.m_Offset = pSAdd[0];

			CCODE uGot = CCODE_ERROR;

			if( pSTyp[0] < LL ) uGot = DoWordRead(A, pWork, uSpan); // block comprises bits/bytes/words

			else uGot = DoLongRead(A, pWork, uSpan);

			// Code for Debug when register is not in slave

			if( uGot & CCODE_ERROR ) {

// Code used for debugging when slave has not been programmed with this register

//**/				if( m_bRxBuff[7] == 0x83 && m_bRxBuff[8] == 0x2 ) uGot = 1L; else // address not found

				return CheckDoneCt(uDone, uCount);
				}

			UINT uRcv = (UINT)uGot;

			UINT k    = 0;

//**/			AfxTrace1("\r\nRCt = %d ", uRCt);

			while( k < uRCt ) {					// run through sorted list

				UINT uWorkPos = pSAdd[k] - pSAdd[0];		// Data position in pWork

				if( pSTyp[k]  > WW ) {

					uWorkPos /= 2;
					}

				if( uWorkPos < uRcv ) {				// Data was read

					if( pSPos[k] < RDONE ) {

						UINT uRspPos	= pSPos[k] - Addr.a.m_Offset;		// Position in pData

						DWORD d		= pWork[uWorkPos];

						switch( pSTyp[k] ) {

							case BB: d = (DWORD)d ? TRUE : FALSE;	break;
							case YY: d = (DWORD)LOBYTE(d);		break;
							case WW: d = (DWORD)LOWORD(d);		break;
							}

						pData[uRspPos] = d;			// receive routine already fixed byte order

						pPos[pSPos[k]]	= RDONE;		// flag this address as done

						uDone |= (1 << uRspPos);		// bit for checking return count

//**/						AfxTrace2("{%d %x} ", pSAdd[k], uDone);
						}
					}

				else break;					// register not read

				if( ++k > uRcv ) break;
				}
			}

///**/		else {
///**/			AfxTrace3("\r\n---Skip %d %d %d ", i, pSAdd[i], pSPos[i]);
///**/			}
		}

	return CheckDoneCt(uDone, uCount);
	}

CCODE CEIepowerTCPDriver::DoEPowerWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress A;

	A.m_Ref	     = Addr.m_Ref;

	A.a.m_Offset = m_pAddrArr[Addr.a.m_Offset];

	switch( m_pTypeArr[Addr.a.m_Offset] ) {

		case LL:
			A.a.m_Type = LL;
			break;

		case RR:
			A.a.m_Type = RR;
			break;

		default:
			A.a.m_Type = WW;
			break;
		}

	uCount  = 1;

	if( A.a.m_Type < LL ) return DoWordWrite(A, pData, uCount);

	return DoLongWrite(A, pData, uCount);
	}

UINT CEIepowerTCPDriver::Check4Error(UINT ccode)
{
	if( !(ccode & CCODE_ERROR) ) {

		switch( m_bRxBuff[7] ) {

			case 0x3:	return RXGOOD;

			case 0x83:	return RXRERR; // address not found
			}
		}

	return RXBAD;
	}

CCODE CEIepowerTCPDriver::CheckDoneCt(UINT uDone, UINT uCount)
{
	// Get the number of consecutive Data starting with pData[0]

	UINT i = 0;

	while( i < uCount ) {

		if( !( 1 & (uDone >> i) ) ) break;

		i++;
		}

	if( i ) return i;

	return CCODE_ERROR;
	}

// Helpers

void CEIepowerTCPDriver::SetArrCount(UINT uTable)
{
	UINT uCount = 0;

	switch( uTable ) {

		case SP_ACC: uCount =  7;	break;
		case SP_COM: uCount = 40;	break;
		case SP_CON: uCount = 40 *  4;	break;
		case SP_COU: uCount =  9 *  4;	break;
		case SP_CUS: uCount =  8 *  4;	break;
		case SP_CST: uCount =  4 *  4;	break;
		case SP_ENE: uCount = 14 *  5;	break;
		case SP_EVE: uCount =  2 * 40;	break;
		case SP_EVS: uCount =  1;	break;
		case SP_FAU: uCount =  8;	break;
		case SP_FIR: uCount = 11 *  4;	break;
		case SP_INS: uCount = 17;	break;
		case SP_IOI: uCount =  5 *  5;	break;
		case SP_IOO: uCount = 11 *  4;	break;
		case SP_IOD: uCount =  4 *  8;	break;
		case SP_IOR: uCount =  2 *  4;	break;
		case SP_IPM: uCount = 11 *  4;	break;
		case SP_LG2: uCount =  8 *  4;	break;
		case SP_LG8: uCount = 13 *  4;	break;
		case SP_LTC: uCount = 26;	break;
		case SP_MAT: uCount = 14 *  4;	break;
		case SP_MOD: uCount =  8 *  4;	break;
		case SP_AAC: uCount = 15 *  4;	break;
		case SP_ADE: uCount = 15 *  4;	break;
		case SP_ADI: uCount = 15 *  4;	break;
		case SP_ALA: uCount = 15 *  4;	break;
		case SP_ASI: uCount = 15 *  4;	break;
		case SP_AST: uCount = 15 *  4;	break;
		case SP_MEA: uCount = 31 *  4;	break;
		case SP_NSU: uCount = 27 *  4;	break;
		case SP_PLM: uCount = 23;	break;
		case SP_PLC: uCount =  5 *  4;	break;
		case SP_QST: uCount = 14;	break;
		case SP_SET: uCount = 13 *  4;	break;
		case SP_TIM: uCount =  6 *  4;	break;
		case SP_TOT: uCount =  9 *  4;	break;
		case SP_USR: uCount =  6 *  4;	break;
		}

	m_pCtx->uArrCount = uCount;
	}

void CEIepowerTCPDriver::SetArrAddr(UINT uTable)
{
	UINT *pA	= m_pAddrArr;
	UINT *pT	= m_pTypeArr;

	UINT uQty	= m_pCtx->uArrCount;

	UINT uInc1	= 0;
	UINT uInc2	= 0;

	for( UINT n = 0; n < uQty; n++ ) pT[n] = YY;

	n		= 0;

	switch( uTable ) {

		case SP_ACC:
			
			pA[n++]	= 2021;	pT[n-1] = WW;
			pA[n++]	= 2020;	pT[n-1] = WW;
			pA[n++]	= 2018;
			pA[n++]	= 199;
			pA[n++]	= 2025;
			pA[n++]	= 2019;	pT[n-1] = WW;
			pA[n++]	= 2022;	pT[n-1] = WW;

			break;

		case SP_COM:
			pA[n++]	= 1942;
			pA[n++]	= 1943;
			pA[n++]	= 1900;
			pA[n++]	= 1901;
			pA[n++]	= 1920;	pT[n-1] = BB;
			pA[n++]	= 1912;
			pA[n++]	= 1913;
			pA[n++]	= 1914;
			pA[n++]	= 1915;
			pA[n++]	= 1903;
			pA[n++]	= 1945;
			pA[n++]	= 1898;
			pA[n++]	= 1904;
			pA[n++]	= 1905;
			pA[n++]	= 1906;
			pA[n++]	= 1907;
			pA[n++]	= 1929;
			pA[n++]	= 1930;
			pA[n++]	= 1931;
			pA[n++]	= 1932;
			pA[n++]	= 1933;
			pA[n++]	= 1934;
			pA[n++]	= 1941;
			pA[n++]	= 1921;
			pA[n++]	= 1944;
			pA[n++]	= 1946;
			pA[n++]	= 1902;
			pA[n++]	= 3073;
			pA[n++]	= 3072;
			pA[n++]	= 1916;
			pA[n++]	= 1917;
			pA[n++]	= 1918;
			pA[n++]	= 1919;
			pA[n++]	= 1899;
			pA[n++]	= 1928;	pT[n-1] = BB;
			pA[n++]	= 1908;
			pA[n++]	= 1909;
			pA[n++]	= 1910;
			pA[n++]	= 1911;
			pA[n++]	= 1927;

			break;

		case SP_CON:

			while( n < uQty ) {

				switch( n % 40 ) {

					case  0: pA[n++] = 951 + uInc1;	
					case  1: pA[n++] = 953 + uInc1;	
					case  2: pA[n++] = 952 + uInc1;	
					case  3: pA[n++] = 942 + uInc1;	
					case  4: pA[n++] = 944 + uInc1;	
					case  5: pA[n++] = 943 + uInc1;	
					case  6: pA[n++] = 939 + uInc1;	
					case  7: pA[n++] = 941 + uInc1;	
					case  8: pA[n++] = 940 + uInc1;	
					case  9: pA[n++] = 948 + uInc1;	
					case 10: pA[n++] = 950 + uInc1;	
					case 11: pA[n++] = 949 + uInc1;	
					case 12: pA[n++] = 945 + uInc1;	
					case 13: pA[n++] = 947 + uInc1;	
					case 14: pA[n++] = 946 + uInc1;	
					case 15: pA[n++] = 954 + uInc1;	
					case 16: pA[n++] = 956 + uInc1;	
					case 17: pA[n++] = 955 + uInc1;	
					case 18: pA[n++] = Fix32(937, uInc1);	pT[n-1] = RR;
					case 19: pA[n++] = Fix32(938, uInc1);	pT[n-1] = RR;
					case 20: pA[n++] = 936 + uInc1;	
					case 21: pA[n++] = Fix32(929, uInc1);	pT[n-1] = RR;
					case 22: pA[n++] = Fix32(930, uInc1);	pT[n-1] = RR;
					case 23: pA[n++] = Fix32(931, uInc1);	pT[n-1] = RR;
					case 24: pA[n++] = Fix32(932, uInc1);	pT[n-1] = RR;
					case 25: pA[n++] = Fix32(933, uInc1);	pT[n-1] = RR;
					case 26: pA[n++] = Fix32(934, uInc1);	pT[n-1] = RR;
					case 27: pA[n++] = Fix32(935, uInc1);	pT[n-1] = RR;
					case 28: pA[n++] = Fix32(924, uInc1);	pT[n-1] = RR;
					case 29: pA[n++] = Fix32(925, uInc1);	pT[n-1] = RR;
					case 30: pA[n++] = Fix32(928, uInc1);	pT[n-1] = RR;
					case 31: pA[n++] = Fix32(926, uInc1);	pT[n-1] = RR;
					case 32: pA[n++] = Fix32(927, uInc1);	pT[n-1] = RR;
					case 33: pA[n++] = 918 + uInc1;	
					case 34: pA[n++] = Fix32(921, uInc1);	pT[n-1] = RR;
					case 35: pA[n++] = Fix32(922, uInc1);	pT[n-1] = RR;
					case 36: pA[n++] = 920 + uInc1;	
					case 37: pA[n++] = Fix32(917, uInc1);	pT[n-1] = RR;
					case 38: pA[n++] = 916 + uInc1;	
					case 39: pA[n++] = 919 + uInc1;	
					}

				uInc1 += 50;
				}

			break;

		case SP_COU:

			while( n < uQty ) {

				switch( n % 9 ) {

					case 0: pA[n++] = 2578 + uInc1;			pT[n-1] = BB;
					case 1: pA[n++] = 2574 + uInc1;			pT[n-1] = BB;
					case 2: pA[n++] = Fix32(2576, uInc1);	pT[n-1] = LL;
					case 3: pA[n++] = 2571 + uInc1;			pT[n-1] = BB;
					case 4: pA[n++] = 2570 + uInc1;			pT[n-1] = BB;
					case 5: pA[n++] = 2573 + uInc1;			pT[n-1] = BB;
					case 6: pA[n++] = 2577 + uInc1;			pT[n-1] = BB;
					case 7: pA[n++] = 2572 + uInc1;			pT[n-1] = BB;
					case 8: pA[n++] = Fix32(2575, uInc1);	pT[n-1] = LL;
					}

				uInc1  += 19;
				}

			break;

		case SP_CUS:

			while( n < uQty ) {

				switch( n % 8 ) {

					case 0: pA[n++] = Fix32(2040, uInc1);	pT[n-1] = LL;
					case 1: pA[n++] = Fix32(2041, uInc1);	pT[n-1] = LL;
					case 2: pA[n++] = Fix32(2042, uInc1);	pT[n-1] = LL;
					case 3: pA[n++] = Fix32(2043, uInc1);	pT[n-1] = LL;
					case 4: pA[n++] = 2044 + uInc1;	
					case 5: pA[n++] = 2045 + uInc1;	
					case 6: pA[n++] = 2046 + uInc1;	
					case 7: pA[n++] = 2047 + uInc1;	
					}

				uInc1  += 20;
				}

			break;

		case SP_CST:

			while( n < uQty ) {

				switch( n % 4 ) {

					case 0: pA[n++] = Fix32(16384, uInc1);
					case 1: pA[n++] = Fix32(16389, uInc1);
					case 2: pA[n++] = Fix32(16394, uInc1);
					case 3: pA[n++] = Fix32(16399, uInc1);
					}

				uInc1  += 20;
				}

			for( n = 0; n < uQty; n++ ) pT[n] = LL;

			break;

		case SP_ENE:

			while( n < uQty ) {

				switch( n % 14 ) {

					case  0: pA[n++] = 2831 + uInc1;	pT[n-1] = BB;
					case  1: pA[n++] = 2821 + uInc1;	pT[n-1] = BB;
					case  2: pA[n++] = Fix32(2822, uInc1);	pT[n-1] = RR;
					case  3: pA[n++] = Fix32(2832, uInc1);	pT[n-1] = RR;
					case  4: pA[n++] = Fix32(2833, uInc1);	pT[n-1] = RR;
					case  5: pA[n++] = 2825 + uInc1;	pT[n-1] = BB;
					case  6: pA[n++] = 2826 + uInc1;	pT[n-1] = WW;
					case  7: pA[n++] = 2828 + uInc1;	
					case  8: pA[n++] = 2823 + uInc1;	pT[n-1] = BB;
					case  9: pA[n++] = Fix32(2824, uInc1);	pT[n-1] = RR;
					case 10: pA[n++] = 2829 + uInc1;	
					case 11: pA[n++] = 2830 + uInc1;	pT[n-1] = BB;
					case 12: pA[n++] = Fix32(2820, uInc1);	pT[n-1] = RR;
					case 13: pA[n++] = 2827 + uInc1;	
					}

				uInc1  += 20;
				}

			break;

		case SP_EVE:

			while( n < uQty ) {

				switch( n % 2 ) {

					case 0: pA[n++] = 1807 + uInc1;
					case 1: pA[n++] = 1806 + uInc1;
					}

				uInc1  += 2;
				}

			break;

		case SP_EVS:

			pA[0] = 1887;
			break;

		case SP_FAU:

			pA[n++]	= 1704;	pT[n-1] = WW;
			pA[n++]	= 1705;	pT[n-1] = WW;
			pA[n++]	= 1699;
			pA[n++]	= 1698;
			pA[n++]	= 1695;
			pA[n++]	= 1700;
			pA[n++]	= 1702;	pT[n-1] = WW;
			pA[n++]	= 1703;

			break;

		case SP_FIR:

			while( n < uQty ) {

				switch( n % 11 ) {

					case  0: pA[n++] = 1210 + uInc1;	
					case  1: pA[n++] = 1214 + uInc1;	
					case  2: pA[n++] = Fix32(1211, uInc1);	pT[n-1] = RR;
					case  3: pA[n++] = 1204 + uInc1;	
					case  4: pA[n++] = 1205 + uInc1;	
					case  5: pA[n++] = 1206 + uInc1;	
					case  6: pA[n++] = Fix32(1212, uInc1);	pT[n-1] = RR;
					case  7: pA[n++] = Fix32(1207, uInc1);	pT[n-1] = RR;
					case  8: pA[n++] = 1213 + uInc1;	
					case  9: pA[n++] = Fix32(1208, uInc1);	pT[n-1] = RR;
					case 10: pA[n++] = Fix32(1209, uInc1);	pT[n-1] = RR;
					}

				uInc1 += 21;
				}
			break;

		case SP_INS:

			pA[n++]	= 2209;	
			pA[n++]	= 2228;	
			pA[n++]	= 2202;	
			pA[n++]	= 2210;	
			pA[n++]	= 2212;			pT[n-1] = BB;	
			pA[n++]	= 2199;	
			pA[n++]	= 2198;	
			pA[n++]	= 2204;	
			pA[n++]	= 2205;	
			pA[n++]	= 2206;	
			pA[n++]	= 2207;	
			pA[n++]	= Fix32(221, 0);	pT[n-1] = RR;
			pA[n++]	= 2208;	
			pA[n++]	= 2169;	
			pA[n++]	= Fix32(2170, 0);		pT[n-1] = LL;
			pA[n++]	= 122;	
			pA[n++]	= 199;	

			break;

		case SP_IOI:

			while( n < uQty ) {

				switch( n % 5 ) {

					case 0: pA[n++] = Fix32(1491, uInc1);	pT[n-1] = RR;
					case 1: pA[n++] = Fix32(1492, uInc1);	pT[n-1] = RR;
					case 2: pA[n++] = Fix32(1489, uInc1);	pT[n-1] = RR;
					case 3: pA[n++] = Fix32(1490, uInc1);	pT[n-1] = RR;
					case 4: pA[n++] = 1488 + uInc1;	
					}

				uInc1 += 15;
				}
			break;

		case SP_IOO:

			while( n < uQty ) {

				switch( n % 11 ) {

					case  0: pA[n++] = 1572 + uInc1;	
					case  1: pA[n++] = 1569 + uInc1;	
					case  2: pA[n++] = 1568 + uInc1;	
					case  3: pA[n++] = 1571 + uInc1;	
					case  4: pA[n++] = 1570 + uInc1;	
					case  5: pA[n++] = 1573 + uInc1;
					case  6: pA[n++] = Fix32(1567, uInc1);	pT[n-1] = RR;
					case  7: pA[n++] = Fix32(1566, uInc1);	pT[n-1] = RR;
					case  8: pA[n++] = Fix32(1564, uInc1);	pT[n-1] = RR;
					case  9: pA[n++] = Fix32(1565, uInc1);	pT[n-1] = RR;
					case 10: pA[n++] = 1563 + uInc1;	
					}

				uInc1 += 21;
				}
			break;

		case SP_IOD:

			while( n < uQty ) {

				switch( n % 4 ) {

					case 0: pA[n++] = 1369 + uInc1;	pT[n-1] = BB;
					case 1: pA[n++] = 1370 + uInc1;	pT[n-1] = BB;
					case 2: pA[n++] = 1371 + uInc1;	pT[n-1] = BB;
					case 3: pA[n++] = 1368 + uInc1;
					}

				uInc1 += 15;
				}
			break;

		case SP_IOR:

			while( n < uQty ) {

				switch( n % 2 ) {

					case 0: pA[n++] = 1648 + uInc1;
					case 1: pA[n++] = 1647 + uInc1;
					}

				uInc1 += 15;
				}

			for( n = 0; n < uQty; n++ ) pT[n] = BB;
			break;

		case SP_IPM:

			while( n < uQty ) {

				switch( n % 11 ) {

					case  0: pA[n++] = 2655 + uInc1;	
					case  1: pA[n++] = Fix32(2653, uInc1);	pT[n-1] = LL;
					case  2: pA[n++] = 2654 + uInc1;	
					case  3: pA[n++] = Fix32(2647, uInc1);	pT[n-1] = RR;
					case  4: pA[n++] = 2656 + uInc1;	pT[n-1] = BB;
					case  5: pA[n++] = Fix32(2649, uInc1);	pT[n-1] = RR;
					case  6: pA[n++] = Fix32(2650, uInc1);	pT[n-1] = RR;
					case  7: pA[n++] = 2652 + uInc1;	pT[n-1] = BB;
					case  8: pA[n++] = 2648 + uInc1;	pT[n-1] = BB;
					case  9: pA[n++] = Fix32(2646, uInc1);	pT[n-1] = RR;
					case 10: pA[n++] = Fix32(2651, uInc1);	pT[n-1] = LL;
					}

				uInc1 += 22;
				}
			break;

		case SP_LG2:

			while( n < uQty ) {

				switch( n % 8 ) {

					case 0: pA[n++] = 2743 + uInc1;	
					case 1: pA[n++] = Fix32(2747, uInc1);	pT[n-1] = RR;
					case 2: pA[n++] = Fix32(2741, uInc1);	pT[n-1] = RR;
					case 3: pA[n++] = Fix32(2742, uInc1);	pT[n-1] = RR;
					case 4: pA[n++] = 2744 + uInc1;	
					case 5: pA[n++] = 2740 + uInc1;	
					case 6: pA[n++] = 2745 + uInc1;		pT[n-1] = BB;
					case 7: pA[n++] = 2746 + uInc1;		pT[n-1] = BB;
					}

				uInc1 += 10;
				}
			break;

		case SP_LG8:

			while( n < uQty ) {

				switch( n % 13 ) {

					case  0: pA[n++] = 2481 + uInc1;	pT[n-1] = BB;
					case  1: pA[n++] = 2482 + uInc1;	pT[n-1] = BB;
					case  2: pA[n++] = 2483 + uInc1;	pT[n-1] = BB;
					case  3: pA[n++] = 2484 + uInc1;	pT[n-1] = BB;
					case  4: pA[n++] = 2485 + uInc1;	pT[n-1] = BB;
					case  5: pA[n++] = 2486 + uInc1;	pT[n-1] = BB;
					case  6: pA[n++] = 2487 + uInc1;	pT[n-1] = BB;
					case  7: pA[n++] = 2488 + uInc1;	pT[n-1] = BB;
					case  8: pA[n++] = 2479 + uInc1;
					case  9: pA[n++] = 2480 + uInc1;
					case 10: pA[n++] = 2478 + uInc1;
					case 11: pA[n++] = 2489 + uInc1;	pT[n-1] = BB;
					case 12: pA[n++] = 2490 + uInc1;	pT[n-1] = BB;
					}

				uInc1 += 23;
				}
			break;

		case SP_LTC:

			pA[n++] = 2802;	
			pA[n++] = 2803;	
			pA[n++] = 2796;	
			pA[n++] = 2797;	
			pA[n++] = 2794;	
			pA[n++] = 2795;	
			pA[n++] = 2800;	
			pA[n++] = 2801;	
			pA[n++] = 2798;	
			pA[n++] = 2799;	
			pA[n++] = 2804;	
			pA[n++] = 2805;	
			pA[n++] = 2792;	
			pA[n++] = 2793;	
			pA[n++] = Fix32(2782, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(2788, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(2789, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(2790, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(2791, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(2783, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(2784, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(2785, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(2786, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(2787, 0);	pT[n-1] = RR;
			pA[n++] = 2781;	
			pA[n++] = 2780;	

			break;

		case SP_MAT:

			while( n < uQty ) {

				switch( n % 14 ) {

					case  0: pA[n++] = 2242 + uInc1;	
					case  1: pA[n++] = Fix32(2235, uInc1);	pT[n-1] = RR;
					case  2: pA[n++] = Fix32(2236, uInc1);	pT[n-1] = RR;
					case  3: pA[n++] = Fix32(2231, uInc1);	pT[n-1] = RR;
					case  4: pA[n++] = Fix32(2230, uInc1);	pT[n-1] = RR;
					case  5: pA[n++] = Fix32(2233, uInc1);	pT[n-1] = RR;
					case  6: pA[n++] = Fix32(2232, uInc1);	pT[n-1] = RR;
					case  7: pA[n++] = Fix32(2237, uInc1);	pT[n-1] = RR;
					case  8: pA[n++] = 2234 + uInc1;	
					case  9: pA[n++] = Fix32(2239, uInc1);	pT[n-1] = RR;
					case 10: pA[n++] = 2240 + uInc1;	
					case 11: pA[n++] = 2243 + uInc1;	pT[n-1] = BB;
					case 12: pA[n++] = 2238 + uInc1;	pT[n-1] = BB;
					case 13: pA[n++] = 2241 + uInc1;	
					}

				uInc1 += 24;
				}
			break;

		case SP_MOD:

			while( n < uQty ) {

				switch( n % 8 ) {

					case  0: pA[n++] = 2481 + uInc1;	pT[n-1] = WW;
					case  1: pA[n++] = Fix32(2482, uInc1);	pT[n-1] = RR;
					case  2: pA[n++] = 2483 + uInc1;	
					case  3: pA[n++] = 2484 + uInc1;	pT[n-1] = WW;
					case  4: pA[n++] = 2485 + uInc1;	
					case  5: pA[n++] = Fix32(2486, uInc1);	pT[n-1] = RR;
					case  6: pA[n++] = 2487 + uInc1;	pT[n-1] = WW;
					case  7: pA[n++] = 2488 + uInc1;	
					}

				uInc1 += 22;
				}
			break;

		case SP_AAC:

			while( n < uQty ) {

				switch( n % 15 ) {

					case 0:  pA[n++] = 391 + uInc1;
					case 1:  pA[n++] = 388 + uInc1;
					case 2:  pA[n++] = 385 + uInc1;
					case 3:  pA[n++] = 394 + uInc1;
					case 4:  pA[n++] = 382 + uInc1;
					case 5:  pA[n++] = 387 + uInc1;
					case 6:  pA[n++] = 384 + uInc1;
					case 7:  pA[n++] = 396 + uInc1;
					case 8:  pA[n++] = 386 + uInc1;
					case 9:  pA[n++] = 389 + uInc1;
					case 10: pA[n++] = 392 + uInc1;
					case 11: pA[n++] = 393 + uInc1;
					case 12: pA[n++] = 395 + uInc1;
					case 13: pA[n++] = 383 + uInc1;
					case 14: pA[n++] = 390 + uInc1;
					}

				uInc1 += 165;
				}
			break;

		case SP_ADE:

			while( n < uQty ) {

				switch( n % 15 ) {

					case 0:  pA[n++] = 346 + uInc1;
					case 1:  pA[n++] = 343 + uInc1;
					case 2:  pA[n++] = 340 + uInc1;
					case 3:  pA[n++] = 349 + uInc1;
					case 4:  pA[n++] = 337 + uInc1;
					case 5:  pA[n++] = 342 + uInc1;
					case 6:  pA[n++] = 339 + uInc1;
					case 7:  pA[n++] = 351 + uInc1;
					case 8:  pA[n++] = 341 + uInc1;
					case 9:  pA[n++] = 344 + uInc1;
					case 10: pA[n++] = 347 + uInc1;
					case 11: pA[n++] = 348 + uInc1;
					case 12: pA[n++] = 350 + uInc1;
					case 13: pA[n++] = 338 + uInc1;
					case 14: pA[n++] = 345 + uInc1;
					}

				uInc1 += 165;
				}
			break;

		case SP_ADI:

			while( n < uQty ) {

				switch( n % 15 ) {

					case 0:  pA[n++] = 331 + uInc1;
					case 1:  pA[n++] = 328 + uInc1;
					case 2:  pA[n++] = 325 + uInc1;
					case 3:  pA[n++] = 334 + uInc1;
					case 4:  pA[n++] = 322 + uInc1;
					case 5:  pA[n++] = 327 + uInc1;
					case 6:  pA[n++] = 324 + uInc1;
					case 7:  pA[n++] = 336 + uInc1;
					case 8:  pA[n++] = 326 + uInc1;
					case 9:  pA[n++] = 329 + uInc1;
					case 10: pA[n++] = 332 + uInc1;
					case 11: pA[n++] = 333 + uInc1;
					case 12: pA[n++] = 335 + uInc1;
					case 13: pA[n++] = 323 + uInc1;
					case 14: pA[n++] = 330 + uInc1;
					}

				uInc1 += 165;
				}
			break;

		case SP_ALA:

			while( n < uQty ) {

				switch( n % 15 ) {

					case 0:  pA[n++] = 376 + uInc1;
					case 1:  pA[n++] = 373 + uInc1;
					case 2:  pA[n++] = 370 + uInc1;
					case 3:  pA[n++] = 379 + uInc1;
					case 4:  pA[n++] = 367 + uInc1;
					case 5:  pA[n++] = 372 + uInc1;
					case 6:  pA[n++] = 369 + uInc1;
					case 7:  pA[n++] = 381 + uInc1;
					case 8:  pA[n++] = 371 + uInc1;
					case 9:  pA[n++] = 374 + uInc1;
					case 10: pA[n++] = 377 + uInc1;
					case 11: pA[n++] = 378 + uInc1;
					case 12: pA[n++] = 380 + uInc1;
					case 13: pA[n++] = 368 + uInc1;
					case 14: pA[n++] = 375 + uInc1;
					}

				uInc1 += 165;
				}
			break;

		case SP_ASI:

			while( n < uQty ) {

				switch( n % 15 ) {

					case 0:  pA[n++] = 361 + uInc1;
					case 1:  pA[n++] = 358 + uInc1;
					case 2:  pA[n++] = 355 + uInc1;
					case 3:  pA[n++] = 364 + uInc1;
					case 4:  pA[n++] = 352 + uInc1;
					case 5:  pA[n++] = 357 + uInc1;
					case 6:  pA[n++] = 354 + uInc1;
					case 7:  pA[n++] = 366 + uInc1;
					case 8:  pA[n++] = 356 + uInc1;
					case 9:  pA[n++] = 359 + uInc1;
					case 10: pA[n++] = 362 + uInc1;
					case 11: pA[n++] = 363 + uInc1;
					case 12: pA[n++] = 365 + uInc1;
					case 13: pA[n++] = 353 + uInc1;
					case 14: pA[n++] = 360 + uInc1;
					}

				uInc1 += 165;
				}
			break;

		case SP_AST:

			while( n < uQty ) {

				switch( n % 15 ) {

					case 0:  pA[n++] = 406 + uInc1;
					case 1:  pA[n++] = 403 + uInc1;
					case 2:  pA[n++] = 400 + uInc1;
					case 3:  pA[n++] = 409 + uInc1;
					case 4:  pA[n++] = 397 + uInc1;
					case 5:  pA[n++] = 402 + uInc1;
					case 6:  pA[n++] = 399 + uInc1;
					case 7:  pA[n++] = 411 + uInc1;
					case 8:  pA[n++] = 401 + uInc1;
					case 9:  pA[n++] = 404 + uInc1;
					case 10: pA[n++] = 407 + uInc1;
					case 11: pA[n++] = 408 + uInc1;
					case 12: pA[n++] = 410 + uInc1;
					case 13: pA[n++] = 398 + uInc1;
					case 14: pA[n++] = 405 + uInc1;
					}

				uInc1 += 165;
				}
			break;

		case SP_MEA:

			while( n < uQty ) {

				switch( n % 31 ) {

					case 0:  pA[n++] = Fix32(280, uInc1);
					case 1:  pA[n++] = Fix32(282, uInc1);
					case 2:  pA[n++] = Fix32(283, uInc1);
					case 3:  pA[n++] = Fix32(284, uInc1);
					case 4:  pA[n++] = Fix32(259, uInc1);
					case 5:  pA[n++] = Fix32(260, uInc1);
					case 6:  pA[n++] = Fix32(261, uInc1);
					case 7:  pA[n++] = Fix32(262, uInc1);
					case 8:  pA[n++] = Fix32(288, uInc1);
					case 9:  pA[n++] = Fix32(264, uInc1);
					case 10: pA[n++] = Fix32(263, uInc1);
					case 11: pA[n++] = Fix32(265, uInc1);
					case 12: pA[n++] = Fix32(273, uInc1);
					case 13: pA[n++] = Fix32(272, uInc1);
					case 14: pA[n++] = Fix32(275, uInc1);
					case 15: pA[n++] = Fix32(276, uInc1);
					case 16: pA[n++] = Fix32(274, uInc1);
					case 17: pA[n++] = Fix32(266, uInc1);
					case 18: pA[n++] = Fix32(267, uInc1);
					case 19: pA[n++] = Fix32(268, uInc1);
					case 20: pA[n++] = Fix32(269, uInc1);
					case 21: pA[n++] = Fix32(256, uInc1);
					case 22: pA[n++] = Fix32(257, uInc1);
					case 23: pA[n++] = Fix32(258, uInc1);
					case 24: pA[n++] = Fix32(289, uInc1);
					case 25: pA[n++] = Fix32(270, uInc1);
					case 26: pA[n++] = Fix32(281, uInc1);
					case 27: pA[n++] = Fix32(271, uInc1);
					case 28: pA[n++] = Fix32(277, uInc1);
					case 29: pA[n++] = Fix32(278, uInc1);
					case 30: pA[n++] = Fix32(279, uInc1);
					}

				uInc1 += 165;
				}

			for( n = 0; n < uQty; n++ ) pT[n] = RR;

			break;

		case SP_NSU:

			while( n < uQty ) {

				switch( n % 27 ) {

					case 0:  pA[n++] = 294 + uInc1;	
					case 1:  pA[n++] = 292 + uInc1;	
					case 2:  pA[n++] = 293 + uInc1;		pT[n-1] = WW;
					case 3:  pA[n++] = 295 + uInc1;		pT[n-1] = WW;
					case 4:  pA[n++] = Fix32(319, uInc1);	pT[n-1] = RR;
					case 5:  pA[n++] = 303 + uInc1;	
					case 6:  pA[n++] = 298 + uInc1;	
					case 7:  pA[n++] = 290 + uInc1;	
					case 8:  pA[n++] = Fix32(306, uInc1);	pT[n-1] = RR;
					case 9:  pA[n++] = 136 + uInc1;	
					case 10: pA[n++] = Fix32(309, uInc1);	pT[n-1] = RR;
					case 11: pA[n++] = 307 + uInc1;	
					case 12: pA[n++] = 302 + uInc1;		pT[n-1] = WW;
					case 13: pA[n++] = 296 + uInc1;	
					case 14: pA[n++] = 299 + uInc1;	
					case 15: pA[n++] = 305 + uInc1;	
					case 16: pA[n++] = 300 + uInc1;	
					case 17: pA[n++] = 301 + uInc1;	
					case 18: pA[n++] = 297 + uInc1;	
					case 19: pA[n++] = 291 + uInc1;	
					case 20: pA[n++] = Fix32(320, uInc1);	pT[n-1] = RR;
					case 21: pA[n++] = Fix32(304, uInc1);	pT[n-1] = RR;
					case 22: pA[n++] = Fix32(308, uInc1);	pT[n-1] = RR;
					case 23: pA[n++] = 321 + uInc1;	
					case 24: pA[n++] = Fix32(313, uInc1);	pT[n-1] = RR;
					case 25: pA[n++] = Fix32(314, uInc1);	pT[n-1] = RR;
					case 26: pA[n++] = Fix32(315, uInc1);	pT[n-1] = RR;
					}

				uInc1 += 165;
				}

			break;

		case SP_PLM:

			pA[n++] = 1734;	
			pA[n++] = 1731;	
			pA[n++] = 1730;	
			pA[n++] = 1733;	
			pA[n++] = 1732;	
			pA[n++] = 1735;	
			pA[n++] = 1714;			pT[n-1] = WW;
			pA[n++] = 1713;	
			pA[n++] = 1728;	
			pA[n++] = 1729;	
			pA[n++] = Fix32(1724, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(1727, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(1726, 0);	pT[n-1] = RR;
			pA[n++] = Fix32(1725, 0);	pT[n-1] = RR;
			pA[n++] = 1723;	
			pA[n++] = 1722;	
			pA[n++] = 1715;	
			pA[n++] = 1717;	
			pA[n++] = 1718;			pT[n-1] = WW;
			pA[n++] = 1719;			pT[n-1] = WW;
			pA[n++] = 1720;			pT[n-1] = WW;
			pA[n++] = 1721;			pT[n-1] = WW;
			pA[n++] = 1716;	
			break;

		case SP_PLC:

			while( n < uQty ) {

				switch( n % 5 ) {

					case 0:  pA[n++] = 1747 + uInc1;	
					case 1:  pA[n++] = 1749 + uInc1;	pT[n-1] = WW;
					case 2:  pA[n++] = 1750 + uInc1;	pT[n-1] = WW;
					case 3:  pA[n++] = Fix32(1746, uInc1);	pT[n-1] = RR;
					case 4:  pA[n++] = 1748 + uInc1;	
					}

				uInc1 += 15;
				}
			break;

		case SP_QST:

			pA[n++] = 2122;
			pA[n++] = 2123;
			pA[n++] = 2120;
			pA[n++] = 2121;
			pA[n++] = 2135;
			pA[n++] = 2119;
			pA[n++] = 2118;
			pA[n++] = 2126;
			pA[n++] = 2124;
			pA[n++] = 2134;	pT[n-1] = WW;
			pA[n++] = 2129;
			pA[n++] = 2125;
			pA[n++] = 2128;
			pA[n++] = 2127;
			break;

		case SP_SET:

			while( n < uQty ) {

				switch( n % 13 ) {

					case 0:  pA[n++] = 1292 + uInc1;	
					case 1:  pA[n++] = Fix32(1301, uInc1);	pT[n-1] = RR;
					case 2:  pA[n++] = Fix32(1299, uInc1);	pT[n-1] = RR;
					case 3:  pA[n++] = Fix32(1297, uInc1);	pT[n-1] = RR;
					case 4:  pA[n++] = Fix32(1288, uInc1);	pT[n-1] = RR;
					case 5:  pA[n++] = Fix32(1291, uInc1);	pT[n-1] = RR;
					case 6:  pA[n++] = Fix32(1294, uInc1);	pT[n-1] = RR;
					case 7:  pA[n++] = Fix32(1295, uInc1);	pT[n-1] = RR;
					case 8:  pA[n++] = 1296 + uInc1;	
					case 9:  pA[n++] = 1290 + uInc1;	
					case 10: pA[n++] = 1298 + uInc1;	
					case 11: pA[n++] = 1300 + uInc1;	
					case 12: pA[n++] = Fix32(1289, uInc1);	pT[n-1] = RR;
					}

				uInc1 += 20;
				}
			break;

		case SP_TIM:

			while( n < uQty ) {

				switch( n % 6 ) {

					case 0:  pA[n++] = Fix32(2326, uInc1);	pT[n-1] = LL;
					case 1:  pA[n++] = 2331 + uInc1;	pT[n-1] = BB;
					case 2:  pA[n++] = 2327 + uInc1;	pT[n-1] = BB;
					case 3:  pA[n++] = Fix32(2328, uInc1);	pT[n-1] = LL;
					case 4:  pA[n++] = 2329 + uInc1;	pT[n-1] = BB;
					case 5:  pA[n++] = 2330 + uInc1;	
					}

				uInc1 += 17;
				}
			break;

		case SP_TOT:

			while( n < uQty ) {

				switch( n % 9 ) {

					case 0:  pA[n++] = 2396 + uInc1;	pT[n-1] = BB;
					case 1:  pA[n++] = Fix32(2394, uInc1);	pT[n-1] = RR;
					case 2:  pA[n++] = 2401 + uInc1;	pT[n-1] = BB;
					case 3:  pA[n++] = Fix32(2399, uInc1);	pT[n-1] = RR;
					case 4:  pA[n++] = 2402 + uInc1;	pT[n-1] = BB;
					case 5:  pA[n++] = 2398 + uInc1;	
					case 6:  pA[n++] = 2400 + uInc1;	pT[n-1] = BB;
					case 7:  pA[n++] = Fix32(2395, uInc1);	pT[n-1] = RR;
					case 8:  pA[n++] = 2397 + uInc1;	
					}

				uInc1 += 21;
				}
			break;

		case SP_USR:

			while( n < uQty ) {

				switch( n % 6 ) {

					case 0:  pA[n++] = Fix32(1956, uInc1);	pT[n-1] = RR;
					case 1:  pA[n++] = Fix32(1957, uInc1);	pT[n-1] = RR;
					case 2:  pA[n++] = 1955 + uInc1	;
					case 3:  pA[n++] = 1959 + uInc1;	pT[n-1] = BB;
					case 4:  pA[n++] = 1954 + uInc1;	
					case 5:  pA[n++] = Fix32(1958, uInc1);	pT[n-1] = RR;
					}

				uInc1 += 16;
				}
			break;
		}
	}

UINT  CEIepowerTCPDriver::Fix32(UINT uOffset, UINT uInc)
{
	return 0x8000 + (2 * (uOffset + uInc));
	}

void CEIepowerTCPDriver::ClearArr(void)
{
	if( m_pAddrArr ) {

		delete [] m_pAddrArr;

		m_pAddrArr = NULL;
		}

	if( m_pPosnArr ) {

		delete [] m_pPosnArr;

		m_pPosnArr = NULL;
		}

	if( m_pTypeArr ) {

		delete [] m_pTypeArr;

		m_pTypeArr = NULL;
		}

	if( m_pSortDataArr ) {

		delete [] m_pSortDataArr;

		m_pSortDataArr = NULL;
		}

	if( m_pSortPosnArr ) {

		delete [] m_pSortPosnArr;

		m_pSortPosnArr = NULL;
		}

	if( m_pSortTypeArr ) {

		delete [] m_pSortTypeArr;

		m_pSortTypeArr = NULL;
		}
	}

void CEIepowerTCPDriver::MakeArr(UINT uTable)
{
	ClearArr();

	SetArrCount(uTable);

	MakeAddr();

	SetArrAddr(uTable);
	}

void CEIepowerTCPDriver::MakeAddr(void)
{
	UINT uSize = m_pCtx->uArrCount;

	m_pAddrArr	= new UINT [uSize];

	m_pPosnArr	= new UINT [uSize];

	m_pTypeArr	= new UINT [uSize];

	m_pSortDataArr	= new UINT [uSize];

	m_pSortPosnArr	= new UINT [uSize];

	m_pSortTypeArr	= new UINT [uSize];

	for( UINT k = 0; k < uSize; k++ ) m_pPosnArr[k] = k; // initial positions

	uSize *= sizeof(UINT);

	memset(m_pAddrArr, 0, uSize);

	memset(m_pTypeArr, 0, uSize);

	memset(m_pSortDataArr, 0, uSize);

	memset(m_pSortPosnArr, 0xFF, uSize);

	memset(m_pSortTypeArr, 0, uSize);
	}

UINT CEIepowerTCPDriver::MakeBlock(UINT uStart, UINT uCount)
{
	UINT *pA   = m_pAddrArr;

//**/	AfxTrace3("\r\nMakeBlock @ %d %d %d ", uStart, pA[uStart], m_pTypeArr[uStart] );

	UINT uThis = pA[uStart];

	UINT *pP   = m_pPosnArr;
	UINT *pT   = m_pTypeArr;

	if( uCount == 1 ) {

		m_pSortDataArr[0] = uThis;

		m_pSortPosnArr[0] = pP[uStart];

		m_pSortTypeArr[0] = pT[uStart];

//**/		AfxTrace1("--- Only 1 %d ", uThis);

		return 1;
		}

	UINT uCt   = 0;

	UINT uTarg;

	UINT pAWork[MAXREAD];
	UINT pPWork[MAXREAD];
	UINT pTWork[MAXREAD];

	UINT uUSize = MAXREAD * sizeof(UINT);

	memset(pAWork, 0xFF, uUSize);
	memset(pPWork, 0xFF, uUSize);
	memset(pTWork, 0xFF, uUSize);

	UINT uMaxP = uStart + uCount; // A useable Array Position must be < uMaxP

//**/	UINT uMax  = uThis;

	for( UINT i = uStart; i < uMaxP; i++ ) { // check for offsets that are within range of uCount

		uTarg = pA[i];

		if( (uTarg >= uThis) && (uTarg < uThis + MAXREAD) && (pP[i] <= uMaxP) ) { // cache only addresses within range of start address and offset

			pAWork[uCt] = uTarg;
			pPWork[uCt] = pP[i]; // position in original list
			pTWork[uCt] = pT[i]; // type in original list

//**/			if( !uCt ) AfxTrace1("%d - ", pAWork[0]);
//**/			if( pAWork[uCt] > uMax ) uMax = pAWork[uCt];

			uCt++; // quantity of results to return
			}
		}

//**/	AfxTrace1("%d ", uMax); // show range of addresses accessible

	DoAddrSort(uCt, pAWork, pPWork, pTWork);

	return uCt;
	}

void CEIepowerTCPDriver::DoAddrSort(UINT uCount, UINT * pASrc, UINT * pPSrc, UINT * pTSrc)
{
	UINT *pADest = m_pSortDataArr;
	UINT *pPDest = m_pSortPosnArr;
	UINT *pTDest = m_pSortTypeArr;

	for( UINT i = 0; i < uCount; i++ ) {

		pADest[i] = pASrc[i];
		pPDest[i] = pPSrc[i];
		pTDest[i] = pTSrc[i];
		}

	i = 0;

	UINT j = 1;

	while( j < uCount ) {

		if( pADest[i] > pADest[j] ) {

			SwapPositions(pADest, pPDest, pTDest, i, j);

			i = 0;
			j = 1;
			}

		else {
			i++;
			j++;
			}
		}
	}

void CEIepowerTCPDriver::SwapPositions(UINT *pAdd, UINT *pPos, UINT *pTyp, UINT ui, UINT uj)
{
	SwapItem(pAdd, ui, uj);
	SwapItem(pPos, ui, uj);
	SwapItem(pTyp, ui, uj);
	}

void CEIepowerTCPDriver::SwapItem(UINT *p, UINT ui, UINT uj)
{
	UINT u = p[ui];
	p[ui]  = p[uj];
	p[uj]  = u;
	}

// Socket Management

BOOL CEIepowerTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	CloseSocket(FALSE);

	return FALSE;
	}

BOOL CEIepowerTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   Port;

		IP   = (IPADDR const &) m_pCtx->m_IP1;

		Port = WORD(m_pCtx->m_uPort);

		if( m_pCtx->m_pSock->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		return FALSE;
		}

	return FALSE;
	}

void CEIepowerTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
