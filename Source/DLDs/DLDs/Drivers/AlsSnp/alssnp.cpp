
#include "intern.hpp"

#include "alssnp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom Alspa SNP Master Serial Driver
//

// Instantiator

INSTANTIATE(CAlstomSNPMaster);

// Constructor

CAlstomSNPMaster::CAlstomSNPMaster(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CAlstomSNPMaster::~CAlstomSNPMaster(void)
{
	}

// End of File
