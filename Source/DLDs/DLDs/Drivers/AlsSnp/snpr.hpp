#include "check.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	LS_OKAY		0
#define	LS_ATTACH	1
#define	LS_REINIT	2

#define	T0_PERIOD	50
#define	T1_PERIOD	20
#define	T2_PERIOD	1000
#define	T3_PERIOD	30000
#define	T4_PERIOD	200

#define	MB_INT_SING	0xC0
#define	MB_INT_TEXT	0x80
#define	MB_ACK_SING	0xD4
#define	MB_ACK_TEXT	0x94
#define	MB_NAK		0xD1

#define	MA_SRC_TASK	0x3A00
#define	MA_SNP_TASK	0x3E10
#define	MA_REQ_TASK	0x0A00

#define SNP_ID_LENGTH	7

//////////////////////////////////////////////////////////////////////////
//
// GE Fanuc / Alspa via SNP
//

class CSNPDriver : public CMasterDriver {

	public:
		// Constructor
		CSNPDriver(void);

		// Destructor
		~CSNPDriver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
			
		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);

	protected:

		// Device Data
		struct CContext
		{
			PTXT	m_Drop;
			BOOL	m_fBreakFree;
			WORD	m_wLinkState;
			BYTE	m_bSequence;
			WORD	m_wIdLen;
			BOOL	m_fAttach;
			BYTE	m_bSlot;						
			};

		// Data Members
		LPCTXT 		m_pHex;
		CCheckSum	m_Check;
		CContext *	m_pCtx;
		BYTE		m_bTxBuff[256];
		BYTE		m_bRxBuff[256];
		WORD		m_wTxSize;
		WORD		m_wRxSize;
		UINT		m_uPtr;
		BYTE		m_sID[SNP_ID_LENGTH + 1];

		
		// Implementation
		CCODE MCALL Read(AREF Addr, PDWORD pData, UINT uCount);
		CCODE MCALL Write(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
				
		// Application Layer
		BOOL	PerformInitLink(PTXT pDrop);		
		BOOL	PutRead(AREF Addr, UINT uCount);		
		BOOL	PutWriteSmall(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	PutWriteBit(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	PutWriteHeader(AREF Addr, UINT uCount);
		BOOL	PutWriteData(AREF Addr, PDWORD pData, UINT uCount);		
		void	AddRegSpec(AREF Addr, UINT uCount);
		BYTE	GetTypeID(WORD wType, BOOL fBit);
		void	GetWordRead(PDWORD pData, UINT uCount, UINT uOffset);
		void	GetLongRead(PDWORD pData, UINT uCount, UINT uOffset);

		

		// Network Layer
		BOOL	PerformAttach(void);
		BOOL	PutAttach(void);
		BOOL	PerformSetParams(void);
		BOOL	PutSetParams(void);
		BOOL	PerformSetPrivilege(void);
		BOOL	PutSetPrivilege(void);
		void	AddCurrentID(void);
		void	AddMailBoxHeader(BYTE bType, BOOL fSNP);
		void	AddMailBoxSource(void);
		void	AddMailBoxDest(BOOL fSNP);
		BOOL	ValidateMail(BYTE bType, BOOL fCheck);
		BOOL	ValidateData(void);
	
		// Data Link Layer
		void	NewFrame(char cType);
		void	AddByte(BYTE bData);
		void	AddRepeatedByte(int nCount, BYTE bData);
		void	AddFourDigitHex(WORD wData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	EndFrame(char cNextType, WORD wNextSize);
		void	SetAttach(BOOL fAttach);
		BOOL	PutFrame();
		BOOL	GetAck(void);
		BOOL	GetFrame(PBYTE pData, WORD wSize);
		BOOL	ValidateFrame(PCBYTE pData, WORD wSize);

		// Data Link Layer
		void SendLongBreak(void); 
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);

		
	};

// End of File
