#include "snpr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom Alsp SNP Driver
//

class CAlstomSNPMaster : public CSNPDriver
{
	public:
		// Constructor
		CAlstomSNPMaster(void);

		// Destructor
		~CAlstomSNPMaster(void);
	};

// End of File
