
#include "intern.hpp"

#include "imok7.hpp"

//////////////////////////////////////////////////////////////////////////
//
// IMOK7 Driver
//

// Instantiator

INSTANTIATE(CIMOK7Driver);

// Constructor

CIMOK7Driver::CIMOK7Driver(void)
{
	m_Ident		= DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	CTEXT Comm[]	= " PMLKCTDSPMLKCTFF";

	m_pComm		= Comm;

	m_ErrorCode	= 0;
	}

// Destructor

CIMOK7Driver::~CIMOK7Driver(void)
{
	}

// Configuration

void MCALL CIMOK7Driver::Load(LPCBYTE pData)
{
	}
	
void MCALL CIMOK7Driver::CheckConfig(CSerialConfig &Config)
{
	if ( Config.m_uPhysical == 3 ) Make485(Config, TRUE);
	}
	
// Management

void MCALL CIMOK7Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CIMOK7Driver::Open(void)
{	
	
	}

// Device

CCODE MCALL CIMOK7Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS; 
	}

CCODE MCALL CIMOK7Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CIMOK7Driver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CIMOK7Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{	
	if ( Addr.a.m_Table == ERRCODE ) {
		*pData = m_ErrorCode;
		return 1;
		}

	if ( Addr.a.m_Table == 13 || Addr.a.m_Table == 14 )
		return ReadTCBits(Addr, pData, uCount);

	StartFrame('r');

	MakeMin( uCount, 16 );

	AddHex( uCount, 0x10 );

	for ( UINT i = 0; i < uCount; i++ ) ConfigFrame( Addr, Addr.a.m_Offset, i );

	AddByte(EOT);
			
	if( Transact() ) {
 
		UINT uRcvBlkCt;
		UINT uDataSize;
		UINT uRxPos;
		UINT n;
		UINT uData = 0;
		UINT uByteCount = 6; // Total Byte Count for Word Commands
		UINT uStartPos = 4; // Start Position for Word Commands
		UINT uSize = 4; // # of Bytes to examine

		uRcvBlkCt = GetHex( 0, 2 ); // Received Block Count

		uDataSize = GetHex( 2, 2 ); // =1->Bits, =2->Word. Size is same for all data

		if ( uDataSize != 1 && uDataSize != 2 ) return CCODE_ERROR;

		if ( Addr.a.m_Type == addrBitAsBit ) {

			uByteCount = 4;

			uStartPos = 5; // skip first byte of 2

			uSize = 1;
			}

		for( n = 0, uRxPos = uStartPos; n < uCount; n++, uRxPos += uByteCount ) {

			if ( uRcvBlkCt ) {

				uData = GetHex( uRxPos, uSize );

				pData[n] = uSize==1 ? uData & 1 : uData;

				uRcvBlkCt--;
				}

			else pData[n] = 0;
			}

		return uCount;
		}
	m_ErrorCode = (DWORD(Addr.a.m_Offset) << 16) + 1;

	return CCODE_ERROR;
	}

CCODE MCALL CIMOK7Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uData;
	UINT i = 0;
	UINT uMask;

	if ( Addr.a.m_Table == ERRCODE ) {
		m_ErrorCode = 0;
		return 1;
		}

	if ( Addr.a.m_Table == 13 || Addr.a.m_Table == 14 )
		uCount = 1;

	MakeMin( uCount, 16 );

	StartFrame('w');

	AddHex( uCount, 0x10 );

	for ( i = 0; i < uCount; i++ ) {

		ConfigFrame( Addr, Addr.a.m_Offset, i );

		uData = UINT( LOWORD(pData[i]) );

		if ( Addr.a.m_Type == addrBitAsBit ) {

			uMask = 0x10;

			if ( uData ) uData = 1;
			}

		else uMask = 0x1000;

		AddHex( uData, uMask );
		}

	AddByte( EOT );

	if( Transact() ) 
		return uCount;

	m_ErrorCode = (DWORD(Addr.a.m_Offset) << 16) + 1;

	return CCODE_ERROR;
	}

// Implementation

CCODE CIMOK7Driver::ReadTCBits(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame('r');

	UINT uReqCt;
	UINT uOffset = Addr.a.m_Offset - (Addr.a.m_Offset % MAXBITS);
	UINT uMod = Addr.a.m_Offset % MAXBITS;

	if ( uMod + uCount > MAXBITS )
		uReqCt = MAXBITS;
	else
		uReqCt = uMod + uCount;

	AddHex( uReqCt, 0x10 );

	for ( UINT i = 0; i < uReqCt*2; i+= 2 ) ConfigFrame( Addr, uOffset, i );

	AddByte(EOT);
			
	if( Transact() ) {
 
		UINT uRcvBlkCt;
		UINT uDataSize;
		UINT uRxPos;
		UINT n;
		UINT uData = 0;
		UINT uByteCount = 4;
		UINT uStartPos = 5;
		UINT uSize = 1;
		UINT uEnd;
		UINT uRetCt = 0;

		uRcvBlkCt = GetHex( 0, 2 ); // Received Block Count

		uDataSize = GetHex( 2, 2 );

		uEnd = uStartPos + ( 4*(uRcvBlkCt-1) );

		if ( uDataSize != 1 ) return CCODE_ERROR;
 
		for( n = 0, uRxPos = uStartPos; uRxPos <= uEnd; n++, uRxPos += uByteCount ) {

			if ( uRcvBlkCt ) {

				uData = GetHex( uRxPos, uSize );

				if ( n >= uMod && uRetCt <= uCount ) {

					pData[uRetCt++] = uData & 1;
					}

				uRcvBlkCt--;
				}

			else pData[n] = 0;
			}

		return uRetCt > 0 ? uRetCt : CCODE_ERROR;
		}

	m_ErrorCode = (DWORD(Addr.a.m_Offset) << 16) + 1;

	return CCODE_ERROR;
	} 

// Frame Building

void CIMOK7Driver::ConfigFrame( AREF Addr, UINT uOffset, UINT uPosition )
{
	AddByte( '0' ); // Address Byte Count MSB

	AddByte( '6' ); // Address Byte Count LSB

	AddByte( '%' );

	AddByte(m_pComm[Addr.a.m_Table]);

	Addr.a.m_Type == addrBitAsBit ? AddByte('X') : AddByte('W');

	AddAddress(uOffset+uPosition, !IsTC( Addr.a.m_Table) );
	}

void CIMOK7Driver::StartFrame(char cCommand)
{
	m_wPtr = 0;

	m_bCheck = 0;

	AddByte(ENQ);
		
	AddByte(m_pHex[(m_pCtx->m_bDrop) / 16]);

	AddByte(m_pHex[(m_pCtx->m_bDrop) % 16]);

	AddByte(cCommand);

	AddByte( 'S' );

	AddByte( 'S' );
	}

// Transport
	
BOOL CIMOK7Driver::SendFrame(void)
{
	m_bTxBuff[m_wPtr++] = m_pHex[m_bCheck / 16];
	m_bTxBuff[m_wPtr++] = m_pHex[m_bCheck % 16];

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_wPtr; k++ ) AfxTrace1("[%2.2x]", m_bTxBuff[k]);

	m_pData->Write( PCBYTE(m_bTxBuff), m_wPtr, FOREVER );

	return TRUE;
	}
	
BOOL CIMOK7Driver::GetFrame(void)
{
	UINT uState = 0;
	
	UINT uPtr   = 0;
	
	BOOL fOkay  = TRUE;
	
	BYTE bCheck = 0;

	SetTimer(1000);

//**/	AfxTrace0("\r\n");
	
	while( GetTimer() ) {

		WORD wData = RxByte();
		
		if( wData == LOWORD(NOTHING) ) {
		
			Sleep(20);
			
			continue;
			}

		if ( uState != 7 && uState != 8 ) bCheck += wData;

//**/		AfxTrace1("<%2.2x>", wData);

		if ( uPtr >= sizeof(m_bRxBuff) ) {

			return FALSE;
			}

		SetTimer(500);

		switch( uState ) {
		
			case 0:
				uPtr = 0;
				bCheck = wData;

				if( wData == ACK) uState = 1;

				else if ( wData == NAK ) {
					uState = 21;
					fOkay = FALSE;
					}
				break;

			case 1: // first Station number byte
				uState = 2;
				break;

			case 2: // Second Station number byte
				uState = 3;
				break;

			case 3:
				uState = (wData == 'W') ? 4 : 11;
				break;

// Finish Write Response
			case 4:
				uState = 5; // First byte Command Type
				break;

			case 5:
				uState = 6; // Second byte Command Type
				break;

			case 6:
				if ( wData != ETX ) fOkay = FALSE;
				uState = 7;
				break;

			case 7:
				if( !(m_pHex[bCheck / 16] == wData) )
					fOkay = FALSE;

				uState = 8;
				
				break;
				
			case 8:
				if( !(m_pHex[bCheck % 16] == wData) )
					fOkay = FALSE;

				return fOkay;

// Finish Read Response
			case 11: // First byte Command Type
				uState = 12;
				break;

			case 12: // Second byte Command Type
				uState = 13;
				break;

			case 13:
				m_bRxBuff[uPtr++] = wData;
				if ( wData == ETX ) uState = 7;
				break;

// Finish NAK Response
			case 21:
				if ( wData == ETX ) uState = 7;
				break;
			}
		}
		
	return FALSE;
	}
	
BOOL CIMOK7Driver::Transact(void)
{
	m_pData->ClearRx();

	SendFrame();
				
	return GetFrame();
	}

// Port Access

void CIMOK7Driver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CIMOK7Driver::RxByte( void )
{
	UINT uData;

	uData = m_pData->Read(100);

	return uData;
	}

// Helpers

void CIMOK7Driver::AddByte(BYTE bData)
{
	m_bTxBuff[m_wPtr++] = bData;

	m_bCheck += bData;
	}
	
void CIMOK7Driver::AddHex(UINT uData, UINT uMask)
{
	while( uMask ) {
	
		AddByte(m_pHex[(uData / uMask) % 16]);
		
		uMask >>= 4;
		}
	}
	
void CIMOK7Driver::AddDec(UINT uData, UINT uMask)
{
	while( uMask ) {
	
		AddByte(m_pHex[(uData / uMask) % 10]);
		
		uMask /= 10;
		}
	}

BOOL CIMOK7Driver::AddAddress(WORD dAddr, BOOL fisHex)
{
	WORD wAddr = dAddr;
	
	if ( fisHex )
		AddHex(wAddr, 0x100);
	else
		AddDec( wAddr, 100 );
	
	return TRUE;
	}

WORD CIMOK7Driver::GetHex(UINT p, UINT n)
{
	WORD d = 0;
	
	while( n-- ) {
	
		char c = char(m_bRxBuff[p++]);

		if( c >= 'A' && c <= 'F' )
			d = (16 * d) + (c - '7');

		if( c >= 'a' && c <= 'f' )
			d = (16 * d) + (c - 'W');

		if( c >= '0' && c <= '9' )
			d = (16 * d) + (c - '0');
		}
		
	return d;
	}

BOOL CIMOK7Driver::IsTC( UINT uTable )
{
	switch ( uTable ) {
		case 5:  // TC Words address in Decimal
		case 6:
//		case 13: // TC Bits address in Hex
//		case 14:
			return TRUE;
		default:
			break;
		}

	return FALSE;
	}

// End of File
