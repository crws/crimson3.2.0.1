
//////////////////////////////////////////////////////////////////////////
//
// IMOK7
//
#define MAXBITS 16
#define ERRCODE 17

class CIMOK7Driver : public CMasterDriver {

	public:
		// Constructor
		CIMOK7Driver(void);

		// Destructor
		~CIMOK7Driver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
			
		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);
		CCODE MCALL Read(AREF Addr, PDWORD pData, UINT uCount);
		CCODE MCALL Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			};
		CContext *	m_pCtx;

		// Data Members
		LPCTXT	m_pHex;
		LPCTXT	m_pComm;

		BYTE	m_bTxBuff[256];
		BYTE	m_bRxBuff[256];
		WORD	m_wTxSize;
		WORD	m_wRxSize;
		UINT	m_wPtr;
		BYTE	m_bCheck;
		DWORD	m_ErrorCode;
		
		// Implementation

		CCODE ReadTCBits(AREF Addr, PDWORD pData, UINT uCount);

		// Frame Building
		void ConfigFrame( AREF Addr, UINT uOffset, UINT uPosition);
		void StartFrame( char cCommand );

		// Transport
		BOOL SendFrame( void );
		BOOL GetFrame( void );
		BOOL Transact( void );

		// Port Access
		void TxByte(BYTE bData);
		UINT RxByte(void);

		// Helpers
		void AddByte( BYTE bData );
		void AddHex(UINT uData, UINT uMask);
		void AddDec(UINT uData, UINT uMask);
		BOOL AddAddress( WORD dAddr, BOOL fisHex );
		WORD GetHex(UINT p, UINT n);
		BOOL IsTC( UINT uTable );
	};

// End of File
