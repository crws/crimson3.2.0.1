#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "emcofp93.hpp"

//////////////////////////////////////////////////////////////////////////
//
// EMCO FP-93 Driver
//

// Instantiator

INSTANTIATE(CEMCOFP93Driver);

// Constructor

CEMCOFP93Driver::CEMCOFP93Driver(void)
{
	m_Ident = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex   = Hex;

	m_uWErrCt = 0;
	}

// Destructor

CEMCOFP93Driver::~CEMCOFP93Driver(void)
{
	}

// Configuration

void MCALL CEMCOFP93Driver::Load(LPCBYTE pData)
{
	}

void MCALL CEMCOFP93Driver::CheckConfig(CSerialConfig &Config)
{
	}

// Management

void MCALL CEMCOFP93Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEMCOFP93Driver::Open(void)
{
	}

// Device

CCODE MCALL CEMCOFP93Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_uDrop = GetWord(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEMCOFP93Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEMCOFP93Driver::Ping(void)
{
	DWORD Data[4];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = C94;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 4);
	}

CCODE MCALL CEMCOFP93Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uCt = IsStringItem( CheckIfString(Addr.a.m_Table, Addr.a.m_Offset) ) ? uCount : 1;

	if( NoReadTransmit(Addr, pData, uCt) ) return 1;

	return Read(Addr, pData, &uCt) ? uCt : CCODE_ERROR;
	}

CCODE MCALL CEMCOFP93Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nWRITE *** T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( NoWriteTransmit(Addr, pData) ) return uCount;

	return Write(Addr, pData, &uCount) ? uCount : CCODE_ERROR;
	}

// Execute Read/Write

BOOL CEMCOFP93Driver::Read(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uID = CheckIfString(Addr.a.m_Table, Addr.a.m_Offset);

	StartFrame(READOP, uID);

	if( Transact(FALSE) ) {

		GetResponse(pData, Addr.a.m_Type, pCount);

		return TRUE;
		}

	return FALSE;
	}

BOOL CEMCOFP93Driver::Write(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uID = Addr.a.m_Offset;

	StartFrame(READOP, uID);

//	AddWriteData(*pData, GetDataType(uID));

//	if( Transact(TRUE) ) {

	if( Transact(FALSE) ) {

		*pCount = 1;

		m_uWErrCt = 0;

		return TRUE;//m_bRx[WOKPOS] == RESPOK;
		}

	return !((++m_uWErrCt) % 3); // max 3 retries
	}

// Frame Building

void CEMCOFP93Driver::StartFrame(BYTE bOp, UINT uID)
{
	m_uPtr = 0;

	char c[7] = {0};

	SPrintf( c, ":%5.5d", m_pCtx->m_uDrop );

	AddText(c);

	AddByte(bOp);

	SPrintf( c, "%2.2d", uID );

	memcpy(m_bTxOp, c, 2);

	AddText(c);
	}

void CEMCOFP93Driver::EndFrame(BOOL fIsWrite)
{
	if( fIsWrite ) AddCRC();

	AddByte( CR );

	m_pData->ClearRx();
	}

void CEMCOFP93Driver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) m_bTx[m_uPtr++] = bData;
	}

void CEMCOFP93Driver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));
	AddByte(LOBYTE(wData));
	}

void CEMCOFP93Driver::AddLong(DWORD dData)
{
	AddWord(HIWORD(dData));
	AddWord(LOWORD(dData));
	}

void CEMCOFP93Driver::AddText(char *cText)
{
	for( UINT i = 0; i < strlen(cText); i++ ) AddByte(cText[i]);
	}

void CEMCOFP93Driver::AddCRC(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_bTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));
	}

void CEMCOFP93Driver::AddWriteData(DWORD dData, UINT uType)
{
	switch( uType ) {

		case DTYPEF:	MakeRealData(dData);	return;
		case DTYPEL:	MakeLongData(dData);	return;
		case DTYPEH:	MakeHexData(dData);	return;
		}
	}

// Data String Building

void CEMCOFP93Driver::MakeRealData(DWORD dData)
{
	char cR[12] = {0};

	if( dData == 0x80000000 ) dData = 0;

	SPrintf( cR, "%.5f", PassFloat(dData) );

	AddText(cR);
	}

void CEMCOFP93Driver::MakeLongData(DWORD dData)
{
	char cR[12] = {0};

	SPrintf( cR, "%lu", dData );

	AddText(cR);
	}

void CEMCOFP93Driver::MakeHexData(DWORD dData)
{
	char cR[12] = {0};

	SPrintf( cR, "%4.4x", dData );

	AddText(cR);
	}

// Transport

BOOL CEMCOFP93Driver::Transact(BOOL fIsWrite)
{
	EndFrame(fIsWrite);

	Send();

	return GetReply(BYTE(fIsWrite ? WRITEOP : READOP));
	}

void CEMCOFP93Driver::Send(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

BOOL CEMCOFP93Driver::GetReply(BYTE b)
{
	UINT uPtr   = 0;

	UINT uState = 0;

	UINT uTimer  = 1000;

	SetTimer(uTimer);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		WORD wData;

		if( (wData = m_pData->Read(uTimer)) == LOWORD(NOTHING) ) {

			continue;
			}

		BYTE bData  = LOBYTE(wData);

		m_bRx[uPtr++] = bData;

//**/		AfxTrace1("<%2.2x>", bData);

		switch( uState ) {

			case 0:
				if( bData == ':' ) uState = 1;
				break;

			case 1:
				if( bData == CR ) uState = 2;
				break;

			case 2:
				if( bData == LF ) return CheckReply(b);
				break;
			}

		if( uPtr >= sizeof(m_bRx) ) return FALSE;
		}

	return FALSE;
	}

// Response Handling

BOOL CEMCOFP93Driver::CheckReply(BYTE bOp)
{
	UINT uState = 0;
	UINT uP1    = 1;
	UINT uP2    = 0;
	BYTE r;
	BYTE c[32];

	memset(c, 0, sizeof(c));

	while( m_bRx[uP1] != CR ) {

		BYTE b = m_bRx[uP1++];

		switch( uState ) {

			case 0: // Drop Number Check
				if( GetHex(b, &r) ) c[uP2++] = b;

				else {
					if( ATOI(PCTXT(c)) != m_pCtx->m_uDrop ) return FALSE;

					uState = 1;

					uP1--;
					}

				break;

			case 1: // Opcode Check
				if( bOp == WRITEOP ) return b == WRITEOP;

				if( b != bOp ) return FALSE;

				uState = 2;
				break;

			case 2: // Command Number Check
				if( b != m_bTxOp[0] ) return FALSE;

				b = m_bRx[uP1++];

				if( b != m_bTxOp[1] ) return FALSE;

				uState = 3;
				break;				

			case 3: // Save Data Type
				m_bRType = b;

				uP2 = 0;

				memset(c, 0, sizeof(c));

				uState = 4;

				break;

			case 4: // Save Data
				if( b == RESPOK || b == RESPFLT ) {

					m_bRData[uP2] = 0;
					return TRUE;
					}

				m_bRData[uP2++] = b;

				if( uP2 >= sizeof(m_bRData) ) return TRUE;
				break;
			}

		if( uP1 >= sizeof(m_bRx) ) return FALSE;
		}

	return FALSE;
	}

void CEMCOFP93Driver::GetResponse(PDWORD pData, UINT uType, UINT *pCount)
{
	UINT i;

	if( m_bRType == DTYPES ) {

		UINT d = sizeof(DWORD);
		UINT u = *pCount;

		UINT uCt = min( u, sizeof(m_bRData)/sizeof(DWORD) );

		for( i = 0; i < u; i++ ) {

			if( i < uCt ) memcpy(&pData[i], &m_bRData[i*d], d);

			else pData[i] = 0;
			}

		return;
		}

	*pCount = 1;

	if( m_bRType == DTYPEH && uType == addrLongAsLong) {

		DWORD d = 0;
		BYTE  r;

		for( i = 0; i < 4; i++ ) {

			GetHex(m_bRData[i], &r);

			d <<= 4;

			d += r;
			}

		*pData = d;

		return;
		}

	switch( uType ) {

		case addrRealAsReal:

			*pData = ATOF(PCTXT(m_bRData));
			return;

		case addrLongAsLong:

			*pData = ATOI(PCTXT(m_bRData));
			return;
		}
	}

// Helpers

BOOL CEMCOFP93Driver::NoReadTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uType = Addr.a.m_Type;

	switch( GetRegisterType( CheckIfString(Addr.a.m_Table, Addr.a.m_Offset) ) ) {

		case DTYPEW:
			*pData = 0;
			return TRUE;

		case DTYPEI:
			*pData = uType == addrRealAsReal ? RNAN : 0;
			return TRUE;

		case DTYPES:
			if( uType == addrRealAsReal || Addr.a.m_Table == C1F) {

				*pData  = RNAN;
				return TRUE;
				}

			break;
		}

	return FALSE;
	}

BOOL CEMCOFP93Driver::NoWriteTransmit(AREF Addr, PDWORD pData)
{
	return !(IsClearCommand( CheckIfString(Addr.a.m_Table, Addr.a.m_Offset) ) && *pData);
	}

BOOL CEMCOFP93Driver::GetHex(BYTE b, PBYTE r)
{
	*r = 0xFF;

	if( (b >= '0') && (b <= '9') ) *r = b - '0';

	else if( (b >= 'A') && (b <= 'F') ) *r = b - '7';

	else if( (b >= 'a') && (b <= 'f') ) *r = b - 'W';

	return *r != 0xFF;
	}

BOOL CEMCOFP93Driver::IsStringItem(UINT uID)
{
	switch( uID ) {

		case	C19:
		case	C24:
		case	C26:
		case	C38:
		case	C44:
		case	C49:
		case	C94:
			return TRUE;
		}

	return FALSE;
	}

BOOL CEMCOFP93Driver::IsRealItem(UINT uID)
{
	switch( uID ) {

		case C01:
		case C02:
		case C03:
		case C04:
		case C05:
		case C06:
		case C07:
		case C10:
		case C11:
		case C12:
		case C13:
		case C14:
		case C15:
		case C16:
		case C17:
		case C18:
		case C20:
		case C21:
		case C22:
		case C23:
		case C25:
		case C27:
		case C30:
		case C31:
		case C32:
		case C33:
		case C34:
		case C35:
		case C36:
		case C37:
		case C40:
		case C41:
		case C42:
		case C43:
		case C45:
		case C46:
		case C47:
		case C48:
		case C50:
		case C51:
		case C57:
		case C59:
		case C60:
		case C61:
		case C62:
		case C63:
		case C64:
		case C65:
		case C66:
		case C67:
		case C68:
		case C69:
			return TRUE;
		}

	return FALSE;
	}

BOOL CEMCOFP93Driver::IsLongItem(UINT uID)
{
	switch( uID ) {

		case C08:
		case C52:
		case C53:
		case C54:
		case C55:
		case C56:
		case C58:
		case C70:
		case C71:
		case C72:
		case C73:
		case C74:
		case C75:
		case C76:
		case C77:
		case C78:
		case C79:
		case C80:
		case C81:
		case C82:
		case C83:
		case C84:
		case C85:
		case C90:
		case C91:
		case C92:
		case C93:
			return TRUE;
		}

	return FALSE;
	}

BOOL CEMCOFP93Driver::IsClearCommand(UINT uID)
{
	return uID >= C91 && uID <= C93;
	}

UINT CEMCOFP93Driver::GetDataType(UINT uID)
{
	if( IsStringItem(uID) )	return DTYPES;

	if( IsRealItem(uID) )	return DTYPEF;

	return uID == C90 ? DTYPEH : DTYPEL;
	}

UINT CEMCOFP93Driver::GetRegisterType(UINT uID)
{
	if( IsClearCommand(uID) ) return DTYPEW;

	if( IsStringItem(uID) ) return DTYPES;

	if( IsRealItem(uID) ) return DTYPEF;

	if( IsLongItem(uID) ) return DTYPEL;

	return DTYPEI;
	}

UINT CEMCOFP93Driver::CheckIfString(UINT uTable, UINT uOffset)
{
	switch( uTable ) {

		case T19:	return C19;
		case T24:	return C24;
		case T26:	return C26;
		case T38:	return C38;
		case T44:	return C44;
		case T49:	return C49;
		case T94:	return C94;
		}

	return uOffset;
	}

// End of File
