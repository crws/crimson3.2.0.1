
#include "intern.hpp"

#include "iaixsel.hpp"

/*
struct FAR IaixselCmdDef {
	UINT	uTable;
	UINT	uOP;
	Description
	UINT	uCPos; // Read-m_bRx[n], Write-Cache Position
	BYTE	bSize; // number of bytes
	BYTE	bType;
	};
*/

IaixselCmdDef CODE_SEG CIaixselDriver::CL[] = {

	{0x200,	0x200, /*Test Call*/				 6,	10,	RR},
	{C01A,	0x201, /*Version Code Model*/			 9,	2,	RR},
	{C01B,	0x201, /*Version Code Unit*/			11,	2,	RR},
	{C01C,	0x201, /*Version Code Version*/			13,	4,	RR},
	{C01D,	0x201, /*Version Code Y/M/D*/			17,	8,	RR},
	{C01E,	0x201, /*Version Code H/M/S*/			25,	6,	RR},
	{0x208,	0x208, /*No. of Active Points*/			 6,	3,	RR},
	{C09A,	0x209, /*Active Point Acceleration*/		14,	4,	RR},
	{C09B,	0x209, /*Active Point Deceleration*/		18,	4,	RR},
	{C09C,	0x209, /*Active Point Velocity*/		22,	4,	RR},
	{C09D,	0x209, /*Active Point Position*/		26,	4,	RR},
	{C09E,	0x209, /*Active Point Axis Pattern*/		12,	2,	RR},
	{C0B,	0x20B, /*Input Port - Input Bytes*/		14,	2,	RR},
	{C0BB,	0x20B, /*Input Port - Input Bits*/		14,	2,	RR},
	{C0C,	0x20C, /*Output Port - Output Bytes*/		14,	2,	RW},
	{CCB,	0x20C, /*Output Port - Output Bits*/		14,	2,	RW},
	{C0D,	0x20D, /*Flag - Flag Bytes*/			16,	2,	RW},
	{C0DB,	0x20D, /*Flag - Flag Bits*/			16,	2,	RW},
	{C0E,	0x20E, /*R/W Integer Variable*/			13,	8,	RW},
	{C0F,	0x20F, /*R/W Real Variable 1 - 1299*/		13,	16,	RW},
	{C0F1,	0x20F, /*R/W Real Variable 1000 - 1999*/	13,	16,	RW},
	{C12A,	0x212, /*Axes Status*/				8,	2,	RR},
	{C12B,	0x212, /*Axis Sensor Input Status*/		10,	1,	RR},
	{C12C,	0x212, /*Axes related error code*/		11,	3,	RR},
	{C12D,	0x212, /*Encoder Status*/			14,	2,	RR},
	{C12E,	0x212, /*Current Position*/			16,	8,	RR},
	{C13A,	0x213, /*Program Status - Status*/		8,	1,	RR},
	{C13B,	0x213, /*Program Status - Step*/		9,	4,	RR},
	{C13C,	0x213, /*Program Status - Error*/		13,	3,	RR},
	{C13D,	0x213, /*Program Status - Error Step*/		16,	4,	RR},
	{0x152,	0x215, /*System Status - Mode*/			6,	1,	RR},
	{0x153,	0x215, /*System Status - Error High*/		7,	3,	RR},
	{0x154,	0x215, /*System Status - Error New*/		10,	3,	RR},
	{0x155,	0x215, /*System Status - Byte 1*/		13,	2,	RR},
	{0x156,	0x215, /*System Status - Byte 2*/		15,	2,	RR},
	{0x157,	0x215, /*System Status - Byte 3*/		17,	2,	RR},
	{0x158,	0x215, /*System Status - Byte 4*/		19,	2,	RR},
// Added Apr 07
	{CRQAT,	0x2A1, /*Scalar Type for Axis Request*/		0,	1,	WR},
	{0xA1C,	0x2A1, /*Axis Work Coordinate System*/		6,	2,	RR},
	{0xA1D,	0x2A1, /*Axis Tool Coordinate System*/		8,	2,	RR},
	{0xA1E,	0x2A1, /*Axis Common Status*/			10,	2,	RR},
	{0xA1F,	0x2A1, /*Axis Pattern Response*/		12,	2,	RR},
	{CRSPST,0x2A1, /*Status for Selected Axis*/		14,	2,	RR},
	{CRSPIN,0x2A1, /*Sensor Input for Selected Axis*/	16,	1,	RR},
	{CRSPEC,0x2A1, /*Error Code for Selected Axis*/		17,	3,	RR},
	{CRSPEN,0x2A1, /*Encoder Status for Selected Axis*/	20,	2,	RR},
	{CRSPPL,0x2A1, /*Present Location for Selected Axis*/	22,	8,	RR},

	{CA1,	0x2A1, /*Axis Status Scalar Location*/		0,	1,	WR},
	{0xA13,	0x2A1, /*Axis Work Coordinate System*/		6,	2,	RR},
	{0xA14,	0x2A1, /*Axis Tool Coordinate System*/		8,	2,	RR},
	{0xA15,	0x2A1, /*Axis Common Status*/			10,	2,	RR},
	{0xA16,	0x2A1, /*Axis Pattern Response*/		12,	2,	RR},
	{0xA17,	0x2A1, /*Axis Status*/				14,	2,	RR},
	{0xA18,	0x2A1, /*Axis Sensor Status*/			16,	1,	RR},
	{0xA19,	0x2A1, /*Axis Relation Error*/			17,	3,	RR},
	{0xA1A,	0x2A1, /*Axis Encoder Status*/			20,	2,	RR},
	{0xA1B,	0x2A1, /*Axis Present Position*/		22,	8,	RR},
	{0x322,	0x232, /*Servo OFF*/				0,	0,	W1},
	{0x323,	0x232, /*Servo ON*/				0,	0,	W1},
	{0x333,	0x233, /*Homing End Search Velocity*/		0,	0,	WC},
	{0x334,	0x233, /*Homing Creep Velocity*/		1,	0,	WC},
	{0x332,	0x233, /*EXECUTE Homing for Axes...*/		0,	0,	WW},
	{0x343,	0x234, /*Absolute Move Acceleration*/		0,	1,	WC},
	{0x344,	0x234, /*Absolute Move Deceleration*/		1,	1,	WC},
	{0x345,	0x234, /*Absolute Move Velocity*/		2,	1,	WC},
	{C34,	0x234, /*Absolute Move Position*/		3,	1,	WC},
	{0x342,	0x234, /*EXECUTE Absolute Move*/		0,	0,	WW},
	{0x353,	0x235, /*Relative Move Acceleration*/		0,	2,	WC},
	{0x354,	0x235, /*Relative Move Deceleration*/		1,	2,	WC},
	{0x355,	0x235, /*Relative Move Velocity*/		2,	2,	WC},
	{C35,	0x235, /*Relative Move Position*/		3,	2,	WC},
	{0x352,	0x235, /*EXECUTE Relative Move*/		0,	0,	WW},
	{0x363,	0x236, /*Jog Acceleration*/			0,	3,	WC},
	{0x364,	0x236, /*Jog Deceleration*/			1,	3,	WC},
	{0x365,	0x236, /*Jog Velocity*/				2,	3,	WC},
	{0x366,	0x236, /*Jog Position*/				3,	3,	WC},
	{0x367,	0x236, /*Jog Direction (0=Positive)*/		5,	3,	WC},
	{0x362,	0x236, /*EXECUTE Jog*/				0,	0,	WW},
	{0x373,	0x237, /*Move to Point Acceleration*/		0,	4,	WC},
	{0x374,	0x237, /*Move to Point Deceleration*/		1,	4,	WC},
	{0x375,	0x237, /*Move to Point Velocity*/		2,	4,	WC},
	{0x376,	0x237, /*Move to Point Point Number*/		3,	4,	WC},
	{0x372,	0x237, /*EXECUTE Move to Point*/		0,	0,	WW},
	{0x442,	0x244, /*1 Point Write - Acceleration*/		0,	5,	WC},
	{0x443,	0x244, /*1 Point Write - Deceleration*/		1,	5,	WC},
	{0x444,	0x244, /*1 Point Write - Velocity*/		2,	5,	WC},
	{C44D,	0x244, /*1 Point Write - Position*/		3,	5,	WC},
	{C44E,	0x244, /*1 Pt. Write V1.0 <Point Number>*/	4,	4,	WW}, // Version 1.0
	{0x445,	0x244, /*Pt.Wr.V1.0 <(Axis Pattern<<16),Point>*/0,	0,	WW}, // Version 1.0 Execute
	{0x446,	0x244, /*1 Point Write (V1.1) <Point Number>*/	0,	0,	WC}, // Version 1.1
	{0x447,	0x244, /*Send 244A,B,C,D,G(V1.1)<Axis Pattern>*/0,	0,	WW}, // Version 1.1 Execute
	{C38,	0x238, /*Stop and Cancel*/			0,	0,	W1},
	{C46,	0x246, /*Clear Point Data*/			0,	0,	W1},
	{0x252,	0x252, /*Alarm Reset*/				0,	0,	W0},
	{0x253,	0x253, /*Execute Program*/			0,	0,	W1},
	{0x254,	0x254, /*Stop Program*/				0,	0,	W1},
	{0x255,	0x255, /*Hold Program*/				0,	0,	W1},
	{0x256,	0x256, /*Execute Program 1 Step*/		0,	0,	W1},
	{0x257,	0x257, /*Resume Program Execution*/		0,	0,	W1},
	{0x25B,	0x25B, /*Software Reset*/			0,	0,	W0},
	{0x25C,	0x25C, /*Drive Power Recovery*/			0,	0,	W0},
	{0x25E,	0x25E, /*Hold Release*/				0,	0,	W0},
	{C62,	0x262, /*Speed Change*/				0,	0,	W1},
// March 07
	{0xD4A,	0x2D4, /*Absolute Move - Acceleration*/		1,	4,	WC},
	{0xD4B,	0x2D4, /*Absolute Move - Deceleration*/		2,	4,	WC},
	{0xD4C,	0x2D4, /*Absolute Move - Speed*/		3,	4,	WC},
	{0xD4D,	0x2D4, /*Absolute Move - Positioning Type*/	4,	2,	WC},
	{CD4E,	0x2D4, /*Absolute Move - Axis Coordinate Data*/	5,	4,	WC},
	{0xD4F,	0x2D4, /*EXECUTE Absolute Move <Axis Pattern>*/	0,	0,	WW},

	{0xD5A,	0x2D5, /*Relative Move - Acceleration*/		1,	4,	WC},
	{0xD5B,	0x2D5, /*Relative Move - Deceleration*/		2,	4,	WC},
	{0xD5C,	0x2D5, /*Relative Move - Speed*/		3,	4,	WC},
	{0xD5D,	0x2D5, /*Relative Move - Positioning Type*/	4,	2,	WC},
	{CD5E,	0x2D5, /*Relative Move - Axis Coordinate Data*/	5,	4,	WC},
	{0xD5F,	0x2D5, /*EXECUTE Relative Move <Axis Pattern>*/	0,	0,	WW},

	{0xD6A,	0x2D6, /*Move To Point - Acceleration*/		1,	4,	WC},
	{0xD6B,	0x2D6, /*Move To Point - Deceleration*/		2,	4,	WC},
	{0xD6C,	0x2D6, /*Move To Point - Speed*/		3,	4,	WC},
	{0xD6D,	0x2D6, /*Move To Point - Positioning Type*/	4,	4,	WC},
	{0xD6E,	0x2D6, /*Move To Point - Point Number*/		5,	3,	WC},
	{0xD6F,	0x2D6, /*EXECUTE Move To Point <Axis Pattern>*/	0,	0,	WW},
// End March 07
	{0xE02,	0xE02, /*Latest Error*/				0,	0,	RW},
	};

//////////////////////////////////////////////////////////////////////////
//
// Iaixsel Driver
//

// Instantiator

INSTANTIATE(CIaixselDriver);

// Constructor

CIaixselDriver::CIaixselDriver(void)
{
	m_Ident = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	InitCaches();
	}

// Destructor

CIaixselDriver::~CIaixselDriver(void)
{
	}

// Configuration

void MCALL CIaixselDriver::Load(LPCBYTE pData)
{
	}

void MCALL CIaixselDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}

// Management

void MCALL CIaixselDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CIaixselDriver::Open(void)
{
	m_pCL = (IaixselCmdDef FAR * )CL;
	}

// Device

CCODE MCALL CIaixselDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_dError = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CIaixselDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CIaixselDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = AN;
	Addr.a.m_Offset = TESTCALL;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CIaixselDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( GetItemPointer(Addr) == NULL ) return CCODE_ERROR | CCODE_NO_RETRY;

//**/	ShowItemInfo(Addr, *pData, uCount, FALSE);

	UINT uOffset = Addr.a.m_Offset;

	if( m_pXItem->uTable == AXISSCALAR ) {

		CScalar17 = Addr.a.m_Extra;
		CAxis17   = uOffset;
		*pData    = (CScalar17 << 16) + CAxis17;
		return 1;
		}

	if( ReadNoTransmit( pData, Addr, &uCount ) ) return IsAxisStatus() == 2 ? uCount : 1;

	UINT uCt = min( 8, uCount );

	DWORD dReadParam = 1;

	switch( m_pXItem->uTable ) {

		case FLAGVAR:
		case INTEGERVAR:
			break;

		case INPUTPORTB:
		case FLAGVARB:
			uCt = min( uCt, 8 - ( uOffset % 8 ) );
			dReadParam = MAKELONG(uOffset % 8, uCt);
			break;

		case OUTPUTPORTB:
			uCt = min( uCt, 8 - ( (uOffset-300) % 8 ) );
			dReadParam = MAKELONG((uOffset-300) % 8, uCt);
			break;

		case REALVAR:
		case REALVAR1:
			uCt = min( 4, uCount );
			break;

		case INPUTPORT:
		case OUTPUTPORT:
		default:
			uCt = 1;
			break;
		}

	ReadOpcode( Addr, uCt );

	if( Transact() && GetResponse( pData, dReadParam, Addr, &uCt ) ) return uCt;

	return CCODE_ERROR;
	}

CCODE MCALL CIaixselDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( GetItemPointer(Addr) == NULL ) return CCODE_ERROR | CCODE_NO_RETRY;

//**/	ShowItemInfo(Addr, *pData, uCount, TRUE);

	if( WriteNoTransmit( pData[0], Addr.a.m_Offset ) ) return 1;

	UINT uCt;

	switch( m_pXItem->uTable ) {

		case OUTPUTPORT: // Write Byte
		case FLAGVAR:

			for( uCt = 0; uCt < 8; uCt++ ) {

				WriteOpcode(Addr, *pData, uCt );

				if( !Transact() || !GetResponse( pData, 0, Addr, &uCount ) ) return uCount;
				}

			return 1;

		default:
			WriteOpcode(Addr, *pData, 1);

			return Transact() && GetResponse(pData, 0, Addr, &uCount) ? 1 : uCount;
		}

	return CCODE_ERROR;
	}

// Command List Pointer

IaixselCmdDef * CIaixselDriver::GetItemPointer( AREF Addr )
{
	m_pXItem = (Addr.a.m_Table == AN) ? GetpItem( Addr.a.m_Offset ) : GetpItem( Addr.a.m_Table );

	IsAxisStatus(); // reset Axis Status request if not 2A1

	return m_pXItem;
	}

IaixselCmdDef * CIaixselDriver::GetpItem(UINT uID)
{
	IaixselCmdDef * p;

	p = m_pCL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == p->uTable ) return p;

		p++;
		}

	return NULL;
	}

// Opcode Handlers

void CIaixselDriver::ReadOpcode(AREF Addr, UINT uCt)
{
	StartFrame();

	AddCommand(FALSE);

	AddReadParameters(Addr, uCt);
	}

void CIaixselDriver::WriteOpcode(AREF Addr, DWORD dData, UINT uCount)
{
	StartFrame();

	AddCommand(TRUE);

	AddWriteInformation(Addr, dData, uCount);
	}

// Header Building

void CIaixselDriver::StartFrame(void)
{
	m_uPtr = 0;

	AddByte( '!' );

	AddByte( '9' );

	AddByte( '9' );
	}

void CIaixselDriver::EndFrame(void)
{
	AddByte('@'); // use generic checksum

	AddByte('@');

	AddByte( CR );

	AddByte( LF );
	}

void CIaixselDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) m_bTx[m_uPtr++] = bData;
	}

void CIaixselDriver::AddData(DWORD dData, UINT uSize)
{
	while ( uSize ) {

		AddByte( m_pHex[ (dData/uSize) % 16 ] );

		uSize /= 16;
		}
	}

void CIaixselDriver::AddDataB(DWORD dData, UINT uByteCount)
{
	switch( uByteCount ) {

		case 2:	AddData(dData, 0x10);		return;
		case 3:	AddData(dData, 0x100);		return;
		case 4:	AddData(dData, 0x1000);		return;
		case 8:	AddData(dData, 0x10000000);	return;
		}
	}

void CIaixselDriver::AddText(PCTXT cText)
{
	UINT i = 0;

	while( cText[i] ) AddByte( cText[i++] );
	}

void CIaixselDriver::AddCommand( BOOL fIsWrite )
{
	UINT uOp;

	if( !fIsWrite ) uOp = m_pXItem->uOP;

	else {
		switch( m_pXItem->uTable ) {

			case OUTPUTPORT:
			case OUTPUTPORTB:
				uOp = 0x24A;
				break;

			case FLAGVAR:
			case FLAGVARB:
				uOp = 0x24B;
				break;

			case INTEGERVAR:
				uOp = 0x24C;
				break;

			case REALVAR:
			case REALVAR1:
				uOp = 0x24D;
				break;

			default:
				uOp = m_pXItem->uOP;
				break;
			}
		}

	AddData( uOp, 0x100 );

	return;
	}

void CIaixselDriver::AddReadParameters( AREF Addr, UINT uCt )
{
	UINT uT = m_pXItem->uTable;
	UINT uO = Addr.a.m_Offset;

	switch ( uT ) {

		case TESTCALL:

			char c[11];

			SPrintf( c, "1234567890" );
			c[10] = 0;
			AddText(c);
			break;

		case VERSIONCODEM:
		case VERSIONCODEU:
		case VERSIONCODEV:
		case VERSIONCODEY:
		case VERSIONCODEH:
			AddData( uO, 0x10 );
			AddByte( m_pHex[Addr.a.m_Extra] );
			break;

		case ACTIVEPOINTSA:
		case ACTIVEPOINTSD:
		case ACTIVEPOINTSV:
		case ACTIVEPOINTSP:
		case ACTIVEPOINTSX:
			AddData( uO, 0x100 );
			AddData( 1, 0x100 );
			break;

		case INPUTPORT: // read only on multiple of 8
		case INPUTPORTB:
			AddData( uO & 0xFFF8, 0x1000 );
			AddData( 8, 0x1000 );
			break;

		case OUTPUTPORT: // read only on multiple of 8 from 300
		case OUTPUTPORTB:
			AddData( 300 + ( (uO-300) & 0xFFF8 ), 0x1000 );
			AddData( 8, 0x1000 );
			break;

		case FLAGVAR: // read only on multiple of 8
		case FLAGVARB:
			AddData( uO & 0x8000 ? Addr.a.m_Extra+16 : Addr.a.m_Extra, 0x10 );
			AddData( uO & 0x7FF8, 0x1000 );
			AddData( 8, 0x1000 );
			break;

		case INTEGERVAR:
		case REALVAR:
		case REALVAR1:
			AddData( uO & 0x8000 ? Addr.a.m_Extra+16 : Addr.a.m_Extra, 0x10 );

			uO &= 0x7FFF;

			if( uT == REALVAR1 ) uO += 1000;

			AddData( uO, 0x100 );
			AddData( uCt, 0x10 );
			break;

		case AXISSTATUS:
		case AXISSENSOR:
		case AXISERROR:
		case AXISENCODER:
			AddData( uO, 0x10 );
			break;

		case AXISPOSITION:
			AddData( 1 << (Addr.a.m_Extra - 1), 0x10 );
			break;

		case PROGSTATUS:
		case PROGSTEP:
		case PROGERROR:
		case PROGERRORSTEP:
			AddData( uO, 0x10 );
			break;
// Single Axis Status read
		case AXISWORK:
		case AXISTOOL:
		case AXISCOMMON:
		case AXISPATTERN:
		case AXISSTATUS1:
		case AXISSENSORA:
		case AXISRELATION:
		case AXISENCODERA:
		case AXISPRESENT:
			AddData( 1<<(CAxis17 - 1), 0x10 );
			AddByte( m_pHex[CScalar17] );
			break;
// Added Apr 07 Multi-Axis Status read
		case AXISWORK2:
		case AXISTOOL2:
		case AXISCOMMON2:
		case AXISPATTERN2:
		case AXISSTAT:
		case AXISINPUT:
		case AXISERRORC:
		case AXISENCDR:
		case AXISLOCATION:
			AddData(MAXAX, 0x10); // request maximum axes in pattern
			AddData(m_pC2A1->Type, 1);
			break;

		default:
			break;
		}
	}

void CIaixselDriver::AddWriteInformation( AREF Addr, DWORD dData, UINT uCount )
{
	UINT	i;
	UINT	uMask;
	DWORD	d64[2];

	UINT uT = m_pXItem->uTable;
	UINT uO = Addr.a.m_Offset;

	switch ( uT ) {

		case OUTPUTPORT: // Write Byte
			AddData( (uO) + uCount, 0x1000 );
			AddByte( dData & (1 << uCount) ? '1' : '0' );
			return;

		case OUTPUTPORTB:
			AddData( uO, 0x1000 );
			AddByte( dData & 1 ? '1' : '0' );
			return;

		case FLAGVAR: // Write Byte
			AddData( uO & 0x8000 ? Addr.a.m_Extra+16 : Addr.a.m_Extra, 0x10 );
			AddData( (uO & 0x7FFF) + uCount, 0x1000 );
			AddByte( dData & (1 << uCount) ? '1' : '0' );
			return;

		case FLAGVARB:
			AddData( uO & 0x8000 ? Addr.a.m_Extra+16 : Addr.a.m_Extra, 0x10 );
			AddData( uO & 0x7FFF, 0x1000 );
			AddByte( dData & 1 ? '1' : '0' );
			return;

		case INTEGERVAR:
			AddData( uO & 0x8000 ? Addr.a.m_Extra+16 : Addr.a.m_Extra, 0x10 );
			AddData( uO & 0x7FFF, 0x100 );
			AddData( 1, 0x10 );
			AddData( dData, 0x10000000 );
			return;

		case REALVAR:
		case REALVAR1:
			AddData( uO & 0x8000 ? Addr.a.m_Extra+16 : Addr.a.m_Extra, 0x10 );

			uO &= 0x7FFF;

			if( uT == REALVAR1 ) uO += 1000;

			AddData( uO, 0x100 );
			AddData( 1, 0x10 );

			f32To64( dData, d64 );

			AddArrayData(d64, 88);
			return;

		case SERVOOFF:
		case SERVOON:
			AddData( dData, 0x10 ); // Axis Pattern
			AddByte( uT == SERVOOFF ? '0' : '1');
			return;

		case EXHOMING:
			AddData( dData, 0x10 ); // Axis Pattern
			AddArrayData(CHoming, 33);
			return;

		case EXABSMOVE:
			AddData( dData, 0x10 ); // Axis Pattern
			AddArrayData(CMoveAbs, 444);
			AddPositionData(&CMoveAbsPos[0], dData);
			return;

		case EXRELMOVE:
			AddData( dData, 0x10 ); // Axis Pattern
			AddArrayData(CMoveRel, 444);
			AddPositionData(&CMoveRelPos[0], dData);
			return;

		case EXJOG:
			AddData( dData, 0x10 ); // Axis Pattern
			AddArrayData(CJog, 8444);
			AddByte( CJog[4] ? '1' : '0' );
			return;

		case EXMOVETOPOINT:
			AddData( dData, 0x10 ); // Axis Pattern
			AddArrayData(CMovePoint, 3444);
			return;

		case WRITEPOINT:
		case WRITEAXISPOINT: // Version 1.0

			UINT uAxisPattern;

			uAxisPattern = (m_pXItem->uTable == WRITEPOINT ? uO : dData >> 16) & 0xFF;

			AddData( dData & 0xFFF, 0x100 ); //  point number

			AddData( 1, 0x100 ); // point count

			AddData( uAxisPattern, 0x10 );

			AddArrayData( CWritePoint, 444 );
			AddPositionData( CWritePointPos, uAxisPattern );
			return;

		case WRAXISPOINT11: // Version 1.1
			AddData( CWritePoint[3], 0x100 ); // point number
			AddData( 1, 0x100 ); // point count
			AddData( dData, 0x10 ); // Axis Pattern
			AddArrayData( CWritePoint, 444 ); // Accel,Decel,Velocity
			AddPositionData( CWritePointPos, dData );
			return;

		case STOPANDCANCEL:
			AddData( dData, 0x10 );
			AddData( uO, 0x10);
			return;

		case CLEARPOINTDATA:
			AddData( uO, 0x100 );
			AddData( dData, 0x100 );
			return;

		case SPEEDCHANGE:
			AddData( uO, 0x10 );
			AddData( dData, 0x1000 );
			return;

		case MASEXECUTE:
			AddData( dData, 0x10); // Axis Pattern
			AddArrayData(CMoveAbsSCARA, 2444);
			AddPositionData(CMoveAbsSCARAPos, dData);
			return;

		case MRSEXECUTE:
			AddData( dData, 0x10); // Axis Pattern
			AddArrayData(CMoveRelSCARA, 2444);
			AddPositionData(CMoveRelSCARAPos, dData);
			return;

		case MPSEXECUTE:
			AddData( dData, 0x10); // Axis Pattern
			AddArrayData(CMovePointSCARA, 32444);
			return;

		case EXPROGRAM:
		case STOPPROGRAM:
		case HOLDPROGRAM:
		case EXONESTEP:
		case RESUMEPROGRAM:
			AddData( dData, 0x10 );
			return;

		case ALARMRESET:
		case SOFTWARERESET:
		case DRIVERECOVERY:
		case HOLDRELEASE:
		default:
			return;
		}
	}

void CIaixselDriver::AddArrayData(PDWORD pCache, UINT uForm)
{
	UINT i = 0;

	while( uForm ) {

		AddDataB( pCache[i], uForm % 10 );

		uForm /= 10;

		i++;
		}
	}

void CIaixselDriver::AddPositionData(PDWORD pCache, UINT uAxisPattern)
{
	UINT uMask = 1;

	UINT i = 1;

	while( uMask & 0xFF ) {

		if( uMask & uAxisPattern ) AddData( pCache[i], 0x10000000 );
		uMask <<= 1;
		i++;
		}
	}

// Transport Layer

BOOL CIaixselDriver::Transact(void)
{
	Send();

	return GetReply();
	}

void CIaixselDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CIaixselDriver::GetReply(void)
{
	UINT uCount = 0;
	UINT uState = 0;
	UINT uData;
	UINT uTimer;
	BYTE bData;

	SetTimer(1000);

//**/	AfxTrace0("\r\n");

	while( ( uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);

		m_bRx[uCount++] = bData;

		if( uCount >= sizeof(m_bRx) ) return FALSE;

		switch ( uState ) {

			case 0:
				if( bData == '#' || bData == '&' ) {

					uCount = 1;

					uState = 1;
					}
				break;

			case 1:
				if( bData == LF ) {

					m_uPtr = uCount;

					return TRUE;
					}

				break;

			}
		}

	return FALSE;
	}

// Response Handling
BOOL CIaixselDriver::GetResponse(PDWORD pData, DWORD dReadParam, AREF Addr, UINT * pCt)
{
	BYTE bCk[2];

	UINT uAxisPattern;

	UINT uRcvCt;

	UINT i     = 0;

	UINT uCt   = *pCt;

	UINT uAxis = Addr.a.m_Extra;

	UINT uOff  = Addr.a.m_Offset;

	DWORD f64[2];

	DWORD dResult;

	if( m_bRx[0] != '#' ) {

		if( m_bRx[0] == '&' ) m_pCtx->m_dError = GetGeneric( &m_bRx[3], 3 );

		else m_pCtx->m_dError = 0xFFFF;

		return FALSE;
		}

	if( dReadParam ) {

		switch( m_pXItem->uTable ) {

			case TESTCALL:
				*pData =  CheckTestCall();
				break;

			case VERSIONCODEY: // gets YMD in one 32 bit result
				dResult = 10000 * GetGeneric( &m_bRx[17], 4 );
				dResult += 100 * GetGeneric( &m_bRx[21], 2 );
				dResult += GetGeneric( &m_bRx[23], 2 );
				*pData = dResult;
				break;

			case VERSIONCODEH: // gets HMS in one 32 bit result
				dResult = 10000 * GetGeneric( &m_bRx[25], 2 );
				dResult += 100 * GetGeneric( &m_bRx[27], 2 );
				dResult += GetGeneric( &m_bRx[29], 2 );
				*pData = dResult;
				break;

			case ACTIVEPOINTSP:
				uAxisPattern = GetGeneric( &m_bRx[12], 2 );

				if( GetPositionInResponse( uAxisPattern, uAxis, &uRcvCt ) ) {

					*pData = GetGeneric( &m_bRx[26 + uRcvCt], 8 );
					}

				else *pData = 0;

				*pCt = 1;

				break;

			case INPUTPORT:
			case OUTPUTPORT:

				if( Addr.a.m_Type != addrBitAsBit ) {

					pData[0] = GetGeneric(&m_bRx[14], 2);

					*pCt = 1;
					}

				else {
					if( m_pXItem->uTable == INPUTPORT ) {

						dReadParam = MAKELONG(uOff % 8, 1);
						}

					else {
						dReadParam = MAKELONG((uOff-300) % 8, 1);
						}

					GetBitData( pData, dReadParam, 14 );

					*pCt = 1;
					}
				break;


			case INPUTPORTB:
			case OUTPUTPORTB:

				GetBitData( pData, dReadParam, 14 );

				*pCt = HIWORD(dReadParam); // Requested bit count

				break;

			case FLAGVAR:

				if( Addr.a.m_Type != addrBitAsBit ) {

					uRcvCt = min(uCt, (GetGeneric(&m_bRx[12], 4)) / 8);

					for( i = 0; i < uRcvCt; i++ ) {

						pData[i] = GetGeneric(&m_bRx[16+(i*2)], 2);
						}

					if( uRcvCt ) *pCt = uRcvCt;
					}

				else {
					dReadParam = MAKELONG( uOff % 8, 1 );

					GetBitData( pData, dReadParam, 16 );

					*pCt = 1;
					}

				break;

			case FLAGVARB:

				GetBitData( pData, dReadParam, 16 );

				*pCt = HIWORD(dReadParam); // Requested bit count

				break;

			case INTEGERVAR:

				uRcvCt = min(uCt, GetGeneric(&m_bRx[11], 2));

				for( i = 0; i < uRcvCt; i++ ) {

					pData[i] = GetGeneric(&m_bRx[13+(i*8)], 8);
					}

				if( uRcvCt ) *pCt = uRcvCt;
				break;

			case REALVAR:
			case REALVAR1:

				uRcvCt = min(uCt, GetGeneric(&m_bRx[11], 2));

				for( i = 0; i < uRcvCt; i++ ) {

					f64[0] = GetGeneric( &m_bRx[21+(i*16)], 8 );
					f64[1] = GetGeneric( &m_bRx[13+(i*16)], 8 );

					pData[i] = f64To32( &f64[0] );
					}

				if( uRcvCt ) *pCt = uRcvCt;
				break;

			case AXISPRESENT:
				uAxisPattern = GetGeneric( &m_bRx[12], 2 );

				if( GetPositionInResponse( uAxisPattern, CAxis17, &uRcvCt ) ) {

					*pData = GetGeneric( &m_bRx[22], 8 );
					}

				else *pData = 0;

				break;

			case AXISSTAT:
			case AXISINPUT:
			case AXISERRORC:
			case AXISENCDR:
			case AXISLOCATION:
			case AXISWORK2:
			case AXISTOOL2:
			case AXISCOMMON2:
			case AXISPATTERN2:

				StoreAxisStatus();

				uAxis  = uOff - 1;

				if( IsAxisStatus() == 2 ) {

					uRcvCt = *pCt;
					}

				else {
					uRcvCt = 1;
					*pCt   = 1;
					}

				for( i = 0; i < uRcvCt; i++ ) {

					pData[i] = GetStoredAxisStatus(uAxis + i );
					}

				m_uLast2A1T = Addr.a.m_Table;
				m_uLast2A1O = uOff;
				return TRUE;

			case AXISPOSITION:
				*pData = GetGeneric( &m_bRx[GetReadPosition()], GetItemSize() );
				*pCt   = 1;
				return TRUE;

			default:
				*pData = GetGeneric( &m_bRx[GetReadPosition()], GetItemSize() );
				break;
			}
		}
		
	return TRUE;
	}

DWORD CIaixselDriver::CheckTestCall(void)
{
	for( UINT i = 6; i < 16; i++ ) {

		if( m_bRx[i] != m_bTx[i] ) return 0L;
		}

	return 1L;
	}

BOOL CIaixselDriver::GetPositionInResponse( UINT uAxisPattern, UINT uAxisNumber, UINT * pCt )
{
	BYTE bMask = 1;

	UINT uCt = 0;

	bMask <<= ( uAxisNumber - 1 );

	if( !(bMask & uAxisPattern) ) return FALSE;

	while( !(bMask & 1) ) {

		if( uAxisPattern & 1 ) uCt += 8;

		bMask >>= 1;

		uAxisPattern >>= 1;
		}

	*pCt = uCt;

	return TRUE;
	}

// Port Access

void CIaixselDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k] );

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

UINT CIaixselDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

BOOL CIaixselDriver::ReadNoTransmit(PDWORD pData, AREF Addr, UINT * pCount)
{
	if( m_pXItem->uOP == LATESTERROR ) {

		*pData = m_pCtx->m_dError;

		return TRUE;
		}

	UINT uItem = Addr.a.m_Offset;

	switch( m_pXItem->bType ) {

		case WR: // Data sent with Read
			if( m_pXItem->uTable == AXISREQTYPE ) *pData = LOBYTE(m_pC2A1->Type);
			return TRUE;

		case W0: // write only operations
		case W1:
		case WW:
			if( m_pXItem->uTable != WRITEPOINT && m_pXItem->uTable != WRITEAXISPOINT ) { // keep write point axis data

				if( m_pXItem->uTable == WRAXISPOINT11 ) *pData = 0;

				else *pData = 0xFFFFFFFF;
				}

			return TRUE;

		case WC: // Get Cached Data
			switch( m_pXItem->uTable ) {

				case HOMINGSEARCH: // Homing
					*pData = CHoming[0];
					return TRUE;

				case HOMINGCREEP: // Homing
					*pData = CHoming[1];
					return TRUE;

				case ABSMOVEACC:
					*pData = CMoveAbs[0];
					return TRUE;

				case ABSMOVEDEC:
					*pData = CMoveAbs[1];
					return TRUE;

				case ABSMOVEVEL:
					*pData = CMoveAbs[2];
					return TRUE;

				case ABSMOVEPOS:
					*pData = CMoveAbsPos[uItem];
					return TRUE;

				case RELMOVEACC:
					*pData = CMoveRel[0];
					return TRUE;

				case RELMOVEDEC:
					*pData = CMoveRel[1];
					return TRUE;

				case RELMOVEVEL:
					*pData = CMoveRel[2];
					return TRUE;

				case RELMOVEPOS:
					*pData = CMoveRelPos[uItem];
					return TRUE;

				case JOGACC:
					*pData = CJog[0];
					return TRUE;

				case JOGDEC:
					*pData = CJog[1];
					return TRUE;

				case JOGVEL:
					*pData = CJog[2];
					return TRUE;

				case JOGPOS:
					*pData = CJog[3];
					return TRUE;

				case JOGDIR:
					*pData = CJog[4];
					return TRUE;

				case MOVETOPOINTACC:
					*pData = CMovePoint[0];
					return TRUE;

				case MOVETOPOINTDEC:
					*pData = CMovePoint[1];
					return TRUE;

				case MOVETOPOINTVEL:
					*pData = CMovePoint[2];
					return TRUE;

				case MOVETOPOINTNUM:
					*pData = CMovePoint[3];
					return TRUE;

				case WRITEPOINTACC:
					*pData = CWritePoint[0];
					return TRUE;

				case WRITEPOINTDEC:
					*pData = CWritePoint[1];
					return TRUE;

				case WRITEPOINTVEL:
					*pData = CWritePoint[2];
					return TRUE;

				case WRITEPOINTPOS:
					*pData = CWritePointPos[uItem];
					return TRUE;

				case WRPOINTNUM11:
					*pData = CWritePoint[3];
					return TRUE;

				case MASACCEL: // Move Absolute SCARA
					*pData = CMoveAbsSCARA[0];
					return TRUE;

				case MASDECEL:
					*pData = CMoveAbsSCARA[1];
					return TRUE;

				case MASSPEED:
					*pData = CMoveAbsSCARA[2];
					return TRUE;

				case MASPOSTY:
					*pData = CMoveAbsSCARA[3];
					return TRUE;

				case MASCDATA:
					*pData = CMoveAbsSCARAPos[uItem]; // 1 based
					return TRUE;

				case MRSACCEL: // Move Relative SCARA
					*pData = CMoveRelSCARA[0];
					return TRUE;

				case MRSDECEL:
					*pData = CMoveRelSCARA[1];
					return TRUE;

				case MRSSPEED:
					*pData = CMoveRelSCARA[2];
					return TRUE;

				case MRSPOSTY:
					*pData = CMoveRelSCARA[3];
					return TRUE;

				case MRSCDATA:
					*pData = CMoveRelSCARAPos[uItem]; // 1 based
					return TRUE;

				case MPSACCEL: // Move To Point SCARA
					*pData = CMovePointSCARA[0];
					return TRUE;

				case MPSDECEL:
					*pData = CMovePointSCARA[1];
					return TRUE;

				case MPSSPEED:
					*pData = CMovePointSCARA[2];
					return TRUE;

				case MPSPOSTY:
					*pData = CMovePointSCARA[3];
					return TRUE;

				case MPSPOINT:
					*pData = CMovePointSCARA[4];
					return TRUE;

				case AXISREQTYPE:
					*pData = m_pC2A1->Type;
					return TRUE;

				break;
				}

		case RR: // Multiple Axis Status Request Control

			if( DoNewAxisRead(Addr) ) return FALSE;

			UINT i;

			uItem -= 1;

			UINT uControl;

			uControl = IsAxisStatus(); // 1 = Common Items, 2 = Individual Axis Items

			if( uControl ) {

				if( uControl == 2 ) uControl = *pCount;

				for( i = 0; i < uControl; i++ ) pData[i] = GetStoredAxisStatus(uItem+i);

				return TRUE;
				}

			break;
		}

	return FALSE;
	}

BOOL CIaixselDriver::WriteNoTransmit(DWORD dData, UINT uItem)
{
	if( m_pXItem->uTable == LATESTERROR ) {

		m_pCtx->m_dError = 0;

		return TRUE;
		}

	switch( m_pXItem->bType ) {

		case RR:
		case WR:
			if( m_pXItem->uTable == AXISREQTYPE ) m_pC2A1->Type = LOBYTE(dData);
			return TRUE;

		case WC:
			switch( m_pXItem->uTable ) {

				case HOMINGSEARCH: // Homing
					CHoming[0] = dData;
					return TRUE;

				case HOMINGCREEP: // Homing
					CHoming[1] = dData;
					return TRUE;

				case ABSMOVEACC:
					CMoveAbs[0] = dData;
					return TRUE;

				case ABSMOVEDEC:
					CMoveAbs[1] = dData;
					return TRUE;

				case ABSMOVEVEL:
					CMoveAbs[2] = dData;
					return TRUE;

				case ABSMOVEPOS:
					CMoveAbsPos[uItem] = dData;
					return TRUE;

				case RELMOVEACC:
					CMoveRel[0] = dData;
					return TRUE;

				case RELMOVEDEC:
					CMoveRel[1] = dData;
					return TRUE;

				case RELMOVEVEL:
					CMoveRel[2] = dData;
					return TRUE;

				case RELMOVEPOS:
					CMoveRelPos[uItem] = dData;
					return TRUE;

				case JOGACC:
					CJog[0] = dData;
					return TRUE;

				case JOGDEC:
					CJog[1] = dData;
					return TRUE;

				case JOGVEL:
					CJog[2] = dData;
					return TRUE;

				case JOGPOS:
					CJog[3] = dData;
					return TRUE;

				case JOGDIR:
					CJog[4] = dData;
					return TRUE;

				case MOVETOPOINTACC:
					CMovePoint[0] = dData;
					return TRUE;

				case MOVETOPOINTDEC:
					CMovePoint[1] = dData;
					return TRUE;

				case MOVETOPOINTVEL:
					CMovePoint[2] = dData;
					return TRUE;

				case MOVETOPOINTNUM:
					CMovePoint[3] = dData;
					return TRUE;

				case WRITEPOINTACC:
					CWritePoint[0] = dData;
					return TRUE;

				case WRITEPOINTDEC:
					CWritePoint[1] = dData;
					return TRUE;

				case WRITEPOINTVEL:
					CWritePoint[2] = dData;
					return TRUE;

				case WRITEPOINTPOS:
					CWritePointPos[uItem] = dData;
					return TRUE;

				case WRPOINTNUM11:
					CWritePoint[3] = dData;
					return TRUE;

				case MASACCEL: // Move Absolute SCARA
					CMoveAbsSCARA[0] = dData;
					return TRUE;

				case MASDECEL:
					CMoveAbsSCARA[1] = dData;
					return TRUE;

				case MASSPEED:
					CMoveAbsSCARA[2] = dData;
					return TRUE;

				case MASPOSTY:
					CMoveAbsSCARA[3] = dData;
					return TRUE;

				case MASCDATA:
					CMoveAbsSCARAPos[uItem] = dData; // 1 based
					return TRUE;

				case MRSACCEL: // Move Relative SCARA
					CMoveRelSCARA[0] = dData;
					return TRUE;

				case MRSDECEL:
					CMoveRelSCARA[1] = dData;
					return TRUE;

				case MRSSPEED:
					CMoveRelSCARA[2] = dData;
					return TRUE;

				case MRSPOSTY:
					CMoveRelSCARA[3] = dData;
					return TRUE;

				case MRSCDATA:
					CMoveRelSCARAPos[uItem] = dData; // 1 based
					return TRUE;

				case MPSACCEL: // Move To Point SCARA
					CMovePointSCARA[0] = dData;
					return TRUE;

				case MPSDECEL:
					CMovePointSCARA[1] = dData;
					return TRUE;

				case MPSSPEED:
					CMovePointSCARA[2] = dData;
					return TRUE;

				case MPSPOSTY:
					CMovePointSCARA[3] = dData;
					return TRUE;

				case MPSPOINT:
					CMovePointSCARA[4] = dData;
					return TRUE;
				}

		case W1:
		case WW:
			switch( m_pXItem->uTable ) {

				case SERVOOFF: // needs axis pattern
				case SERVOON: // needs axis pattern
				case EXHOMING: // needs axis pattern
				case EXABSMOVE: // needs axis pattern
				case EXRELMOVE: // needs axis pattern
				case EXJOG: // needs axis pattern
				case EXMOVETOPOINT: // needs axis pattern
				case WRITEPOINT: // needs axis pattern
				case WRAXISPOINT11: // need axis pattern
				case EXPROGRAM: // no program 0
				case STOPANDCANCEL: // needs axis pattern
				case CLEARPOINTDATA: // needs quantity of points
				case MASEXECUTE: // needs axis pattern
				case MRSEXECUTE: // needs axis pattern
				case MPSEXECUTE: // needs axis pattern

					return ( dData == 0 ); // can't execute if data = 0;

				case WRITEAXISPOINT:

					return !(dData & 0xFF0000); // axis pattern == 0

				default:
					break;
				}
		}

	return FALSE;
	}

UINT CIaixselDriver::GetReadPosition(void)
{
	return m_pXItem->uCPos;
	}

UINT CIaixselDriver::GetItemSize(void)
{
	return m_pXItem->bSize;
	}

DWORD CIaixselDriver::f64To32( PDWORD p )
{
	DWORD dOut;

	DWORD dExp;

	if( p[0] == 0 && p[1] == 0 ) return 0;

	dExp = (p[1] >> 20) & 0x7FF;

	dOut = p[1] & 0x80000000; // Sign Bit

	dOut += (dExp - 896) << 23;

	return dOut | ( (p[1] & 0xFFFFF) << 3 ) | ( p[0] >> 29 );
	}

void CIaixselDriver::f32To64( DWORD d, PDWORD p )
{ // Note High DWord is first

	if( d == 0 ) {

		p[0] = 0;

		p[1] = 0;

		return;
		}

	DWORD dExp;

	DWORD dOut;

	dOut = d & 0x80000000; // Sign Bit

	dExp = (d>>23) & 0xFF; // 8 bit exponent in 32 bit IEEE

	dExp += 896; // 1023 - 127

	dOut |= dExp << 20; // 11 bit exponent in 64 bit IEEE

	p[0] = dOut | ( (d & 0x7FFFFF) >> 3 );

	p[1] = ( ( d & 7 ) << 29 );
	}

DWORD CIaixselDriver::GetGeneric( PBYTE p, UINT uCt )
{
	DWORD d = 0L;
	BYTE b;

	for( UINT i = 0; i < uCt; i++ ) {

		d <<= 4;

		b = p[i];

		if( b >= '0' && b <= '9' ) d += (b - '0');

		else if( b >= 'a' && b <= 'f' ) d += (b - 'W');

		else if( b >= 'A' && b <= 'F' ) d += (b - '7');
		}

	return d;
	}

void CIaixselDriver::GetBitData( PDWORD pData, DWORD dReadParam, UINT uRcvPos )
{
	UINT uReqCt = HIWORD(dReadParam);

	BYTE bMask = 1 << LOWORD(dReadParam);

	UINT i = 0;

	while( uReqCt ) {

		BYTE bData = GetGeneric( &m_bRx[uRcvPos], 2 );

		uRcvPos += 2;

		while ( uReqCt && (bMask & 0xFF) ) {

			pData[i++] = (bData & bMask) ? 1L : 0L;

			bMask <<= 1;

			uReqCt--;
			}

		bMask = 1;
		}

	return;
	}

void CIaixselDriver::InitCaches(void)
{
	CAxis17   = 0;
	CScalar17 = 0;
	memset(CHoming, 0, sizeof(CHoming));
	memset(CMoveAbs, 0, sizeof(CMoveAbs));
	memset(CMoveAbsPos, 0, sizeof(CMoveAbsPos));
	memset(CMoveRel, 0, sizeof(CMoveRel));
	memset(CMoveRelPos, 0, sizeof(CMoveRelPos));
	memset(CJog, 0, sizeof(CJog));
	memset(CMovePoint, 0, sizeof(CMovePoint));
	memset(CWritePoint, 0, sizeof(CWritePoint));
	memset(CWritePointPos, 0, sizeof(CWritePointPos));
	memset(CMoveAbsSCARA, 0, sizeof(CMoveAbsSCARA));
	memset(CMoveAbsSCARAPos, 0, sizeof(CMoveAbsSCARAPos));
	memset(CMoveRelSCARA, 0, sizeof(CMoveRelSCARA));
	memset(CMoveRelSCARAPos, 0, sizeof(CMoveRelSCARAPos));
	memset(CMovePointSCARA, 0, sizeof(CMovePointSCARA));

	m_pC2A1 = &C2A1;
	memset(m_pC2A1, 0, sizeof( C2A1 ) );
	m_uLast2A1T = 0;
	m_uLast2A1O = 0;
	}

void CIaixselDriver::StoreAxisStatus(void)
{
	m_pC2A1->Work    = GetGeneric( &m_bRx[POSWORK], 2 );
	m_pC2A1->Tool    = GetGeneric( &m_bRx[POSTOOL], 2 );
	m_pC2A1->Common  = GetGeneric( &m_bRx[POSCOMMON], 2 );
	m_pC2A1->Pattern = GetGeneric( &m_bRx[POSPATTERN], 2 );

	PBYTE p = &m_bRx[POSSTATUS];

	BYTE bMask = 1;

	UINT Axis = 0;

	while( bMask & MAXAX ) {

		if( bMask & m_pC2A1->Pattern ) {

			m_pC2A1->Status[Axis]   = GetGeneric( p, 2 );
			m_pC2A1->Sensor[Axis]   = GetGeneric( p + 2, 1 );
			m_pC2A1->Error[Axis]    = GetGeneric( p + 3, 3 );
			m_pC2A1->Encoder[Axis]  = GetGeneric( p + 6, 2 );
			m_pC2A1->Location[Axis] = GetGeneric( p + 8, 8 );
			p += SIZEDATA;
			}

		bMask <<= 1;
		Axis++;
		}
	}

DWORD CIaixselDriver::GetStoredAxisStatus(UINT uAxis)
{
	switch( m_pXItem->uTable ) {

		case AXISWORK2:		return m_pC2A1->Work;
		case AXISTOOL2:		return m_pC2A1->Tool;
		case AXISCOMMON2:	return m_pC2A1->Common;
		case AXISPATTERN2:	return m_pC2A1->Pattern;
		case AXISSTAT:		return m_pC2A1->Status[uAxis];
		case AXISINPUT:		return m_pC2A1->Sensor[uAxis];
		case AXISERRORC:	return m_pC2A1->Error[uAxis];
		case AXISENCDR:		return m_pC2A1->Encoder[uAxis];
		case AXISLOCATION:	return m_pC2A1->Location[uAxis];
		}

	return 0;
	}

BOOL CIaixselDriver::DoNewAxisRead(AREF Addr)
{
	if( !m_uLast2A1T ) return TRUE;	// not 2A1 or new 2A1 request

	if( (Addr.a.m_Table == m_uLast2A1T) && (Addr.a.m_Offset == m_uLast2A1O) ) {

		m_uLast2A1T = 0;
		m_uLast2A1O = 0;
		return TRUE; // repeat of saved 2A1 request, send new 2A1 request
		}

	return FALSE;
	}

UINT CIaixselDriver::IsAxisStatus(void)
{
	switch( m_pXItem->uTable ) {

		case AXISWORK2:
		case AXISTOOL2:
		case AXISCOMMON2:
		case AXISPATTERN2:	return 1;

		case AXISSTAT:
		case AXISINPUT:
		case AXISERRORC:
		case AXISENCDR:
		case AXISLOCATION:	return 2;
		}

	m_uLast2A1T = 0;
	m_uLast2A1O = 0;

	return 0;
	}

//**/void CIaixselDriver::ShowItemInfo(AREF Addr, DWORD dData, UINT uCount, BOOL fIsWrite)
//**/{
//**/	char c = 'L';
//**/
//**/	switch( Addr.a.m_Type ) {
//**/
//**/		case addrBitAsBit:	c = 'B'; break;
//**/		case addrByteAsByte:	c = 'Y'; break;
//**/		case addrWordAsWord:	c = 'W'; break;
//**/		case addrRealAsReal:	c = 'R'; break;
//**/		}
//**/
//**/	if( fIsWrite ) AfxTrace0("\r\n\n*** WWW ");
//**/	else AfxTrace0("\r\n*** R ");
//**/ 
//**/	AfxTrace3("Tab=%X, Off=%X, Op=%x ", Addr.a.m_Table, Addr.a.m_Offset, m_pXItem->uOP);
//**/	AfxTrace3("Type=%c, Ex=%x, Ct=%d ", c, Addr.a.m_Extra, uCount);
//**/	if( fIsWrite ) AfxTrace1("Data=%8.8lx\r\n\n", dData);
//**/	}

// End of File
