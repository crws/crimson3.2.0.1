
//////////////////////////////////////////////////////////////////////////
//
// IAI XSEL Driver
//

#define IAIXSEL_ID 0x339D

#define AN	addrNamed

#define	RR	0 // Read Only
#define WR	1 // Value sent with Read
#define RW	2 // Read or Write
#define WW	3 // Execute Cached Data Write
#define W0	4 // No data sent with Command
#define W1	5 // Send Data with Command
#define WC	6 // Cached Data

#define	MAXAX	0x3F // assume maximum of 6 axis

#define	POSWORK		6
#define	POSTOOL		8
#define	POSCOMMON	10
#define	POSPATTERN	12
#define	POSSTATUS	14
#define	POSSENSOR	16
#define	POSERROR	17
#define	POSENCODER	20
#define	POSLOCATION	22
#define SIZEDATA	POSLOCATION + 8 - POSSTATUS

struct FAR IaixselCmdDef {
	UINT	uTable;
	UINT	uOP;
	/* Description */
	UINT	uCPos; // Read-m_bRx[n], Write-Cache Position
	BYTE	bSize; // number of bytes
	BYTE	bType;	
	};

#pragma pack(1)
struct C2A1Status {

	BYTE	Type;		// must be specified for read
	BYTE	Work;		// Common work coordinate system
	BYTE	Tool;		// Common tool coordinate system
	BYTE	Common;		// Common Status
	BYTE	Pattern;	// Common Axis Pattern
	BYTE	Status[8];	// Specific Status
	BYTE	Sensor[8];	// Sensor Input Status
	WORD	Error[8];	// Relation Error Code
	BYTE	Encoder[8];	// Encoder Status
	DWORD	Location[8];	// Present Location
	};
#pragma pack()

class CIaixselDriver : public CMasterDriver
{
	public:
		// Constructor
		CIaixselDriver(void);

		// Destructor
		~CIaixselDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			DWORD	m_dError;
			};

		CContext	* m_pCtx;

		// Data Members
		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		WORD	m_uPtr;

		UINT	CAxis17;
		UINT	CScalar17;
		DWORD	CHoming[2];
		DWORD	CMoveAbs[3];
		DWORD	CMoveAbsPos[9];
		DWORD	CMoveRel[3];
		DWORD	CMoveRelPos[9];
		DWORD	CJog[5];
		DWORD	CMovePoint[4];
		DWORD	CWritePoint[4];
		DWORD	CWritePointPos[9];
		DWORD	CMoveAbsSCARA[4];
		DWORD	CMoveAbsSCARAPos[9];
		DWORD	CMoveRelSCARA[4];
		DWORD	CMoveRelSCARAPos[9];
		DWORD	CMovePointSCARA[6];
		struct C2A1Status C2A1;
		struct C2A1Status *m_pC2A1;

		UINT	m_uLast2A1T;
		UINT	m_uLast2A1O;

		LPCTXT	m_pHex;

		static	IaixselCmdDef CODE_SEG CL[];
		IaixselCmdDef FAR * m_pCL;
		IaixselCmdDef FAR * m_pXItem;

		PDWORD	m_pCache;

		// Command List pointer
		IaixselCmdDef * GetItemPointer( AREF Addr );
		IaixselCmdDef *	GetpItem( UINT uID );
	
		// Opcode Handlers
		void	ReadOpcode(AREF Addr, UINT uCt);
		void	WriteOpcode(AREF Addr, DWORD dData, UINT uCount);
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, UINT uSize);
		void	AddDataB(DWORD dData, UINT uByteCount);
		void	AddText(PCTXT cText);
		void	AddCommand( BOOL fIsWrite );
		void	AddReadParameters( AREF Addr, UINT uCt );
		void	AddWriteInformation( AREF Addr, DWORD dData, UINT uCount );
		void	AddArrayData(PDWORD pCache, UINT uForm);
		void	AddPositionData(PDWORD pCache, UINT uAxisPattern);
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);

		// Response Handling
		BOOL	GetResponse(PDWORD pData, DWORD dReadParam, AREF Addr, UINT * pCt);
		DWORD	CheckTestCall(void);
		BOOL	GetPositionInResponse( UINT uAxisPattern, UINT uAxisNumber, UINT * pCt );
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		BOOL	ReadNoTransmit( PDWORD pData, AREF Addr, UINT * pCount );
		BOOL	WriteNoTransmit( DWORD dData, UINT uItem );
		UINT	GetReadPosition(void);
		UINT	GetItemSize(void);
		DWORD	f64To32( PDWORD p );
		void	f32To64( DWORD d, PDWORD p );
		DWORD	GetGeneric( PBYTE p, UINT uCt );
		void	GetBitData( PDWORD pData, DWORD dReadParam, UINT uRcvPos );
		void	InitCaches(void);
		void	StoreAxisStatus(void);
		DWORD	GetStoredAxisStatus(UINT uAxis);
		BOOL	DoNewAxisRead(AREF Addr);
		UINT	IsAxisStatus(void);

		// Debug
//**/		void	ShowItemInfo(AREF Addr, DWORD dData, UINT uCount, BOOL fIsWrite);
	};

// Move Control
#define	MV3		3
#define	MV4		4
#define	MVDATAONLY	10

// Table Addresses
#define	C09A	1
#define	C09B	2
#define	C09C	3
#define	C09D	4
#define	CD4E	5
#define	C09E	6
#define	C0B	7
#define	C0C	8
#define	C0D	9
#define	C0E	10
#define	C12A	11
#define	C12B	12
#define	C12C	13
#define	C12D	14
#define	C12E	15
#define	CCB	16
#define	CA1	17
#define	C0DB	18
#define	C0BB	19
#define	C13A	20
#define	C13B	21
#define	C13C	22
#define	C13D	23
#define	C38	24
#define	C46	25
#define	C62	26
#define	C34	27
#define	CD5E	28
#define	C35	29
#define	CXXP	30
#define	C0F	31
#define	C01A	32
#define	C01B	33
#define	C01C	34
#define	C01D	35
#define	C01E	36
#define	C44D	37
#define	C44E	38
#define	CSND	39
#define	CRCV	40
// Added Apr 07
#define	CRQAT	41
#define	CRSPST	42
#define	CRSPIN	43
#define	CRSPEC	44
#define	CRSPEN	45
#define	CRSPPL	46
// Added Feb 09
#define	C0F1	47

#define	TESTCALL	0x200
#define	VERSIONCODEM	C01A
#define	VERSIONCODEU	C01B
#define	VERSIONCODEV	C01C
#define	VERSIONCODEY	C01D
#define	VERSIONCODEH	C01E
#define	ACTIVEPOINTS	0x208
#define	ACTIVEPOINTSA	C09A
#define	ACTIVEPOINTSD	C09B
#define	ACTIVEPOINTSV	C09C
#define	ACTIVEPOINTSP	C09D
#define	ACTIVEPOINTSX	C09E
#define	INPUTPORT	C0B
#define	INPUTPORTB	C0BB
#define	OUTPUTPORT	C0C
#define	OUTPUTPORTB	CCB
#define	FLAGVAR		C0D
#define	FLAGVARB	C0DB
#define	INTEGERVAR	C0E
#define	REALVAR		C0F
#define	REALVAR1	C0F1
#define	AXISSTATUS	C12A
#define	AXISSENSOR	C12B
#define	AXISERROR	C12C
#define	AXISENCODER	C12D
#define	AXISPOSITION	C12E
#define PROGSTATUS	C13A
#define PROGSTEP	C13B
#define PROGERROR	C13C
#define PROGERRORSTEP	C13D
#define	SYSMODE		0x152
#define	SYSERRORHI	0x153
#define	SYSERRORNEW	0x154
#define	SYSBYTE1	0x155
#define	SYSBYTE2	0x156
#define	SYSBYTE3	0x157
#define	SYSBYTE4	0x158
// Added Apr 07
#define	AXISREQTYPE	CRQAT // Sent with Read
#define	AXISWORK2	0xA1C // Following are the response
#define	AXISTOOL2	0xA1D
#define	AXISCOMMON2	0xA1E
#define	AXISPATTERN2	0xA1F
#define	AXISSTAT	CRSPST
#define	AXISINPUT	CRSPIN
#define	AXISERRORC	CRSPEC
#define	AXISENCDR	CRSPEN
#define	AXISLOCATION	CRSPPL

#define	AXISSCALAR	CA1
#define	AXISWORK	0xA13
#define	AXISTOOL	0xA14
#define	AXISCOMMON	0xA15
#define	AXISPATTERN	0xA16
#define	AXISSTATUS1	0xA17
#define	AXISSENSORA	0xA18
#define	AXISRELATION	0xA19
#define	AXISENCODERA	0xA1A
#define	AXISPRESENT	0xA1B
#define	SERVOOFF	0x322
#define	SERVOON		0x323
#define	HOMINGSEARCH	0x333
#define	HOMINGCREEP	0x334
#define	EXHOMING	0x332
#define	ABSMOVEACC	0x343
#define	ABSMOVEDEC	0x344
#define	ABSMOVEVEL	0x345
#define	ABSMOVEPOS	C34
#define	EXABSMOVE	0x342
#define	RELMOVEACC	0x353
#define	RELMOVEDEC	0x354
#define	RELMOVEVEL	0x355
#define	RELMOVEPOS	C35
#define	EXRELMOVE	0x352
#define	JOGACC		0x363
#define	JOGDEC		0x364
#define	JOGVEL		0x365
#define	JOGPOS		0x366
#define	JOGDIR		0x367
#define	EXJOG		0x362
#define	MOVETOPOINTACC	0x373
#define	MOVETOPOINTDEC	0x374
#define	MOVETOPOINTVEL	0x375
#define	MOVETOPOINTNUM	0x376
#define	EXMOVETOPOINT	0x372
#define	STOPANDCANCEL	C38
#define	CLEARPOINTDATA	C46
#define	WRITEPOINTACC	0x442
#define	WRITEPOINTDEC	0x443
#define	WRITEPOINTVEL	0x444
#define	WRITEPOINTPOS	C44D
#define	WRITEPOINT	C44E
#define	WRITEAXISPOINT	0x445
#define	WRPOINTNUM11	0x446
#define	WRAXISPOINT11	0x447
#define	ALARMRESET	0x252
#define	EXPROGRAM	0x253
#define	STOPPROGRAM	0x254
#define	HOLDPROGRAM	0x255
#define	EXONESTEP	0x256
#define	RESUMEPROGRAM	0x257
#define	SOFTWARERESET	0x25B
#define	DRIVERECOVERY	0x25C
#define	HOLDRELEASE	0x25E
#define	SPEEDCHANGE	C62
#define	USERSEND	CSND
#define	USERRECEIVE	CRCV
#define	MASACCEL	0xD4A
#define	MASDECEL	0xD4B
#define	MASSPEED	0xD4C
#define	MASPOSTY	0xD4D
#define	MASCDATA	CD4E
#define	MASEXECUTE	0xD4F
#define	MRSACCEL	0xD5A
#define	MRSDECEL	0xD5B
#define	MRSSPEED	0xD5C
#define	MRSPOSTY	0xD5D
#define	MRSCDATA	CD5E
#define	MRSEXECUTE	0xD5F
#define	MPSACCEL	0xD6A
#define	MPSDECEL	0xD6B
#define	MPSSPEED	0xD6C
#define	MPSPOSTY	0xD6D
#define	MPSPOINT	0xD6E
#define	MPSEXECUTE	0xD6F
#define	LATESTERROR	0xE02

// End of File
