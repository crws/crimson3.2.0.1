
//////////////////////////////////////////////////////////////////////////
//
// IMO G Series Loader Port

// Offset is ddx
#define	OPMX	1
#define	OPIX	2
#define	OPQX	3
#define	OPMB	4
#define	OPIB	5
#define	OPQB	6
#define	OPMW	7
#define	OPIW	8
#define	OPQW	9
#define	OPMD	10
#define	OPID	11
#define	OPQD	12
#define ERRCODE 17

class CIMOGLoaderPortDriver : public CMasterDriver {

	public:
		// Constructor
		CIMOGLoaderPortDriver(void);

		// Destructor
		~CIMOGLoaderPortDriver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
			
		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);
		CCODE MCALL Read(AREF Addr, PDWORD pData, UINT uCount);
		CCODE MCALL Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Data Members
		LPCTXT	m_pHex;

		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		UINT	m_uPtr;
		UINT	m_uSendCount;
		BYTE	m_bCheck;
		UINT	m_uLastByte;
		DWORD	m_ErrorCode;
		
		// Implementation

		// Frame Building
		void ConfigureRead(AREF Addr, UINT uCount);
		void ConfigureWrite(AREF Addr, UINT uCount, PDWORD pData);
		void StartFrame(BYTE bOpcode, BYTE bAddrCode);
		void EndFrame(void);
		void PutAddress(AREF Addr);
		void PutCount(AREF Addr, UINT uCount);
		void AddWriteData(AREF Addr, PDWORD pData, UINT uCount);
		BYTE GetAddrCode( UINT uTable );

		// Data Transfer
		BOOL Transact(void);
		BOOL GetReply(void);

		// Response Handlers
		CCODE GetResponse(PDWORD pData, AREF Addr, UINT uReturnCount);
		BOOL GetBitResponse(PDWORD pData, UINT uOffset, UINT * pReturnCount);
		BOOL GetByteResponse(PDWORD pData, UINT * pReturnCount);
		BOOL GetWordResponse(PDWORD pData, UINT * pReturnCount);
		BOOL GetLongResponse(PDWORD pData, UINT * pReturnCount);

		// Port Access
		void Put(void);
		UINT Get(void);

		// Helpers
		void AddByte(BYTE bData);
		void AddData(UINT uData, UINT uMask);
		UINT GetHex(UINT p, UINT n);
		UINT GetMaxCount( AREF Addr, UINT uCount );
		UINT SwapLoWordBytes( UINT uData );
	};

// End of File
