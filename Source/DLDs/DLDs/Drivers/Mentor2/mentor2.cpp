
#include "intern.hpp"

#include "mentor2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Control Techniques Mentor II Master Serial Driver
//

// Instantiator

INSTANTIATE(CMentor2MasterDriver);

// Constructor

CMentor2MasterDriver::CMentor2MasterDriver(void)
{
	m_Ident = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;

	m_fWide = FALSE;
	
	}

// Destructor

CMentor2MasterDriver::~CMentor2MasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CMentor2MasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 0;
	Addr.a.m_Extra	= 0;	
	Addr.a.m_Type   = addrLongAsLong;

	return Read(Addr, Data, 1);

	}

// Configuration

void MCALL CMentor2MasterDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CMentor2MasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CMentor2MasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMentor2MasterDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CMentor2MasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop      = GetByte(pData);
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;

	}

CCODE MCALL CMentor2MasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CMentor2MasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	UINT uAddr = Addr.a.m_Offset;

	PutRead(uAddr);

	if( GetFrame() ) {
	
		GetDataFormat(4);

		DWORD dData = GetGeneric(10, GetDataLength(4), 4);

		*pData = dData;
		
		return uCount;
		}

	return CCODE_ERROR;

	}

CCODE MCALL CMentor2MasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	UINT uAddr = Addr.a.m_Offset;

	PutWrite(uAddr, pData);

	if( GetFrame() )
		return uCount;	

	return CCODE_ERROR;
	
	}

// Implementation

void CMentor2MasterDriver::PutRead(UINT uAddr)
{
	BYTE bMenu	= uAddr / 100;
		
	BYTE bParameter	= uAddr % 100;

	StartFrame();
		
	AddByte( EOT );

	AddByte( m_pHex[m_pCtx->m_bDrop/10] );
	AddByte( m_pHex[m_pCtx->m_bDrop/10] );
	AddByte( m_pHex[m_pCtx->m_bDrop%10] );
	AddByte( m_pHex[m_pCtx->m_bDrop%10] );
	
	AddByte( m_pHex[bMenu/10] );
	AddByte( m_pHex[bMenu%10] );

	AddByte( m_pHex[bParameter/10] );
	AddByte( m_pHex[bParameter%10] );

	AddByte(ENQ);
	
	Send();		
	}

void CMentor2MasterDriver::PutWrite(UINT uAddr, PDWORD pData)
{
	BYTE bMenu	= uAddr / 100;
		
	BYTE bParameter	= uAddr % 100;

	StartFrame();
		
	AddByte( EOT );

	AddByte( m_pHex[m_pCtx->m_bDrop/10] );
	AddByte( m_pHex[m_pCtx->m_bDrop/10] );
	AddByte( m_pHex[m_pCtx->m_bDrop%10] );
	AddByte( m_pHex[m_pCtx->m_bDrop%10] );
	
	AddByte( STX );

	m_bCheck = 0;

	AddByte( m_pHex[bMenu/10] );
	AddByte( m_pHex[bMenu%10] );

	AddByte( m_pHex[bParameter/10] );
	AddByte( m_pHex[bParameter%10] );

	PutData( *pData );

	AddByte(ETX);
	
	if( m_bCheck < 32 ) {

		m_bCheck += 32;
		}
	
	AddByte( m_bCheck );

	Send();		
	}

void CMentor2MasterDriver::StartFrame(void)
{
	m_uPtr = 0;

	m_bCheck = 0;
	}
	
void CMentor2MasterDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	
	m_bCheck ^= bData;
	}
	
void CMentor2MasterDriver::Send(void)
{
	AddByte( NUL );

	m_pData->ClearRx();
	
	m_pData->Write(m_bTx, m_uPtr - 1, FOREVER);

	}

BOOL CMentor2MasterDriver::GetFrame(void)
{
	UINT uState = 0;
	
	UINT uPtr = 0;

	UINT uTimer = 0;

	UINT uData = 0;
	
	SetTimer(FRAME_TIMEOUT);
	
	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				
				switch(uData) {
					
					case ACK:
						return TRUE;
					
					case NAK:
					case EOT:
						return FALSE;
					
					case STX:
						m_bCheck = 0;
						uPtr   = 0;
						uState = 1;
						break;
					}				

				break;
				
			case 1:					
				if( uData == ETX ) {	
				
					if( m_bCheck < 32 ) {

						m_bCheck += 32;
						}
						
					uState = 2;
					}

				m_bCheck ^= uData;
					
				m_bRx[uPtr++] = uData;
					
				if( uPtr == sizeof(m_bRx) ){

					return FALSE;
					}
				
				break;

			case 2:
				if(uData == m_bCheck) {
					
					return TRUE;
					}

				return FALSE;

				break;
			}
		}
		
	return FALSE;
	}

void CMentor2MasterDriver::GetDataFormat(UINT uOffset)
{
	BYTE *pData = m_bRx + uOffset;

	if( *pData == '+' || *pData == '-') {

		m_fWide = FALSE;

		return;
		}
	
	m_fWide = TRUE;
	}

UINT CMentor2MasterDriver::GetDataLength(UINT uOffset)
{
	BYTE *pData = m_bRx + uOffset;

	UINT uCount = 0;
	
	while(uCount <= 6) {
		
		if( *pData++ == ETX)
			break;

		uCount++;
		}

	return uCount;
	}

DWORD CMentor2MasterDriver::GetGeneric(UINT uRadix, UINT uLength, UINT uOffset)
{
	BYTE *pData = m_bRx + uOffset;
	
	DWORD dData = 0;
	
	BOOL fNeg = FALSE;
	
	while( uLength-- ) {
	
		char cData = (char) *(pData++);
		
		if( cData == '-' )
			fNeg = TRUE;
		
		if( cData >= '0' && cData <= '9' )
			dData = dData * uRadix + cData - '0';

		if( cData >= 'a' && cData <= 'f' )
			dData = dData * uRadix + cData - 'a' + 10;

		if( cData >= 'A' && cData <= 'F' )
			dData = dData * uRadix + cData - 'A' + 10;
		}
		
	return fNeg ? (-1 * (C2INT) dData) : dData;
	}

void CMentor2MasterDriver::PutData(DWORD dData)
{
	if( m_fWide ) {

		PutGeneric(Abs(dData), 10, 10000);
		}
	else {
		Neg(dData) ? AddByte( '-' ) : AddByte( '+' );

		PutGeneric(Abs(dData), 10, 1000);
		}
	}

void CMentor2MasterDriver::PutGeneric(DWORD dData, UINT uRadix, UINT uFactor)
{
	while( uFactor ) {
	
		BYTE bData = m_pHex[(dData / uFactor) % uRadix];
		
		AddByte(bData);
		
		uFactor /= uRadix;
		}
	}

DWORD CMentor2MasterDriver::Abs(DWORD dData)
{
	return (((C2INT) dData) < 0) ? (-1 * (C2INT) dData) : dData;
	}

BOOL CMentor2MasterDriver::Neg(DWORD dData)
{
	return (((C2INT) dData) < 0) ? TRUE : FALSE;
	}

// End of File
