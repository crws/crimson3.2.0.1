
#ifndef INCLUDE_J1939_HPP

#define INCLUDE_J1939_HPP

//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Network Management

#define CLAIM_REQ	0xEA00
#define CLAIM_RESP	0xEE00

// Data Transfer

#define PGN_ACK		0xE800
#define PGN_REQ		0xEA00	
#define PGN_TRANSFER	0xEB00
#define PGN_TRANSPORT	0xEC00

// Time

#define TIME_TR		200
#define TIME_TH		500
#define TIME_T1		750
#define TIME_T2		1250
#define TIME_T3		1250
#define TIME_T4		1050

// State

#define STATE_HOLD	0
#define STATE_RTS	1
#define STATE_CTS	2
#define STATE_DATA	3
#define STATE_ACK	4
#define STATE_RESET	5
#define STATE_RETRY	6
#define STATE_WAIT	7
#define STATE_BEGIN	0xFF

// Abort

#define ABORT_SESSION	1
#define ABORT_RESOURCE	2
#define	ABORT_TIMEOUT	3

// Transport

#define TRANS_RECV	0
#define TRANS_SEND	1
#define TRANS_MODE	2

// Misc

#define DEVICES		255

#define UNSPECIFIED	9999 

// Diagnostics

#define DIAG_IDS	1
#define DIAG_PGNS	2

// Enhanced

#define ENH_RAPIDFIRE	1

// DM Write Support

BYTE const SEND_MASK[4] = { 0xF8, 0x07, 0xFE, 0x01 };

BYTE const INIT_MASK[4] = { 0xE0, 0x1F, 0x80, 0x7F };

BYTE const SEND_SHFT[4] = { 3, 5, 1, 7 };

BYTE const INIT_SHFT[4] = { 5, 3, 7, 1 };

//////////////////////////////////////////////////////////////////////////
//
// J1939 NAME Field
//

struct NAME
{
	union {
		struct {

			#ifdef _E_LITTLE

			DWORD	m_ID	: 21;
			DWORD	m_MC	: 11;

			#else

			DWORD	m_MC	: 11;
			DWORD	m_ID	: 21;

			#endif

			} b; 
		
		DWORD Lo;
		};

	union {

		struct {

			#ifdef _E_LITTLE

			DWORD   m_ECUI	: 3;
			DWORD   m_FI	: 5;
			DWORD	m_F	: 8;
			DWORD   m_R	: 1;
			DWORD   m_VS	: 7;
			DWORD	m_VSI	: 4;
			DWORD	m_IG	: 3;
			DWORD	m_Arb	: 1;

			#else

			DWORD	m_Arb	: 1;
			DWORD	m_IG	: 3;	 // Global = 0
			DWORD	m_VSI	: 4;
			DWORD   m_VS	: 7;	 //
			DWORD   m_R	: 1;
			DWORD	m_F	: 8;	 // Cab Display = 60  // Also preferred name if and iecu = 0
			DWORD   m_FI	: 5;
			DWORD   m_ECUI	: 3;

			#endif 

			} a; 
		
		DWORD Hi;
	       	};
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 29-Bit Identifier Field
//

struct ID {

	union {
		struct {

			#ifdef _E_LITTLE

			UINT  m_SA  : 8;
		       	UINT  m_PS  : 8;
			UINT  m_PF  : 8;
			UINT  m_DP  : 1;
			UINT  m_EDP : 1;
			UINT  m_P   : 3;
			UINT  m_X   : 3;

			#else
			
			UINT  m_X   : 3;
			UINT  m_P   : 3;
			UINT  m_EDP : 1;
			UINT  m_DP  : 1;
			UINT  m_PF  : 8;
		       	UINT  m_PS  : 8;
			UINT  m_SA  : 8;

			#endif
			
			} i;

		DWORD m_Ref;
		};
	};

//////////////////////////////////////////////////////////////////////////
//
// PDU Storage
//

struct PDU {

	ID       m_ID;
	UINT     m_uBytes;
	UINT	 m_uTotal;
	BYTE *   m_pData;
	BYTE *   m_pTransfer;
	BYTE	 m_bSequence[TRANS_MODE];
	BYTE	 m_bState   [TRANS_MODE];
	UINT	 m_uTime    [TRANS_MODE];
	BYTE	 m_bLimit   [TRANS_MODE];
	BOOL	 m_fCTS	    [TRANS_MODE];
	UINT	 m_uSet;
	UINT     m_uReq;
	UINT	 m_uLive;
	BOOL     m_fSend;
	BYTE	 m_bDiag;
	BYTE     m_bEnh;
	BYTE *   m_pEnhanced;
	BYTE	 m_bExt;
	BYTE     m_bDir;
	BOOL     m_fSrc;
	DWORD    m_Ref;
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 FRAME
//

#pragma pack(1)

struct FRAME_EXT
{
	BYTE	m_Ctrl;
	BYTE    m_Zero;
	DWORD	m_ID;
	BYTE	m_bData[8];
	};

#pragma pack()    

//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver Context Structures
//

struct SPN
{
	BYTE  m_Size;
	UINT  m_Number;
	};

struct PGN
{
	UINT	 m_Number;
	BYTE	 m_Diag;
	BYTE	 m_Priority;
	BYTE     m_SendReq;
	UINT	 m_uSPNs;
	SPN *	 m_pSPNs;
	UINT     m_RepRate;
	UINT     m_uRequest;
	BYTE     m_Enh;
	BYTE     m_Dir;
	};

/////////////////////////////////////////////////////////////////////////
//
// J1939 Types
//

enum J1939Types {

	type1bit	= 1,
	type2bits	= 2,
	type3bits	= 3,
	type4bits	= 4,
	type5bits	= 5,
	type6bits	= 6,
	type7bits	= 7,
	type8bits	= 8,
	type9bits	= 9,
	type10bits	= 10,
	type11bits	= 11,
	type12bits	= 12,
	type1byte	= 13,
	type2bytes	= 14,
	type3bytes	= 15,
	type4bytes	= 16,
	};

/////////////////////////////////////////////////////////////////////////
//
// Other Enumerations
//

enum Transport {

	transHandler = 0,
	transDriver  = 1,
	transTimer   = 2,
	};

enum Direction {

	dirBoth	     = 0,
	dirTx	     = 1,
	dirRx	     = 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 Handler
//

class CCANJ1939Handler : public IPortHandler
{	
	public:
		// Constructor
		CCANJ1939Handler(IHelper *pHelper);

		// Destructor
		~CCANJ1939Handler(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);
		
		// Network Management
		CCODE ClaimAddress(BOOL bSrc, UINT uTrans = transHandler);

		// FRAME_EXT Access
		BOOL SendPGNRequest(PGN * pPG, BYTE bDA, UINT uBackOff, BOOL fSrc, UINT uTrans = transHandler);


		// Data Access
		void MakePDU(ID * id, UINT uSize, UINT uLive, UINT uDiag, UINT uEnh, UINT uDir, BOOL fSrc = FALSE, DWORD dwRef = 0, UINT uExt = 0);
		PDU* FindPDU(ID * id, UINT uDir);
		PDU* FindPDU(ID * id, UINT uDir, BOOL  fSrc);
		PDU* GetPDU(UINT uIndex);
		BYTE GetSA(BOOL fSrc);
		BOOL GetSrc(ID  * id);
		BOOL HasData(PDU * pPDU);
		void SetClearDTC(BYTE bDA, BYTE bClrDTC);
		UINT GetFlopStart(PDU * pPDU);
		UINT GetFlopEnd(PDU * pPDU);
		void SetSecondaryUsed(BOOL fSrc);

		// Overridables
		virtual	BOOL SendPDU(PDU * pPDU, BOOL fRetry = FALSE, UINT uTrans = transHandler);
		virtual	void SetSA(BYTE bSA, BYTE bSA2 = 0);
		virtual	UINT GetMask(void);
			
		// Public helpers
		BOOL IsTimedOut(UINT uTime, UINT uSpan);
		void Suspend(BOOL fSuspend);	
		BOOL HasUpdate(void);

		// Testing / Debug
		void ShowPDUs(void);

	protected:
		// Data
		ULONG           m_uRefs;
		PDU           *	m_pPDUs;
		UINT		m_uPDUs;
		FRAME_EXT 	m_RxData;
		PCBYTE		m_pTxData;
		UINT		m_uTx;
		UINT		m_uRx;
		IPortObject   *	m_pPort;
		UINT volatile	m_uQueue;
		UINT volatile	m_uSend;
		FRAME_EXT	m_Rx;
		FRAME_EXT	m_Tx[3];
		FRAME_EXT	m_Send[64];
		BOOL		m_fClaim[2];
		BOOL		m_fClaimSent[2];
		UINT		m_uClaim[2];
		BYTE 		m_bSrc[2];
		NAME *		m_Name[2];
		PDU           *	m_pTransfer[DEVICES];
		PDU           *	m_pBroadcast[DEVICES];
		BOOL		m_fSet;
		BOOL		m_uTimer;
		BYTE		m_Diag;
		BYTE		m_Diag1[DEVICES];
		DWORD		m_Diag2[DEVICES];
		BOOL		m_fSuspend;
		BOOL		m_fUpdate;
		BYTE		m_bClrDTC[DEVICES];
		INT volatile    m_nTxDisable;
		BOOL		m_fTimer;
		IHelper       * m_pHelper;
		IExtraHelper  * m_pExtra;
		BOOL            m_fSecondary;
		
		// Implementation
		void Start(UINT uTrans = transHandler);
		void AddByte(BYTE bByte, UINT &uPos, UINT uTrans = transHandler);
		void AddWord(WORD wWord, UINT &uPos, UINT uTrans = transHandler);
		void AddID(DWORD dwID, UINT &uPos, UINT uTrans = transHandler);
		void AddID(PDU * pPDU, UINT uTrans = transHandler);
		void AddID(ID id, UINT uTrans = transHandler);
		void AddCtrl(UINT uBytes, UINT uTrans = transHandler);
		void AddPGN(UINT uPGN, UINT uTrans = transHandler);
		void AddData(PDU * pPDU, UINT uTrans = transHandler);
		void AddName(BOOL fSrc, UINT uTrans = transHandler);
	        void GrowPDUs(PDU * pPDU);
		BOOL PutFrame(UINT uTrans = transHandler);
		void SendFrame(void);
		void RecvFrame(void);
		BOOL SendAck(PDU * pPDU);
		BOOL SendNak(ID id);
		BOOL HandleGlobalRequest(PDU * pPDU, BOOL fRTR);
		BOOL HandleRTS(ID id);
		BOOL HandleCTS(ID id);
		BOOL HandleEOM(ID id);
		BOOL HandleBAM(ID id);
		BOOL HandleDataTransfer(void);
		BOOL HandleAbort(ID id);
		BOOL HandleNak(ID id, BOOL fForce);
		BOOL SetPDU(PDU * pPDU);
		BOOL TransferData(PDU * pPDU, UINT uLimit = 0, UINT uSequence = 1, UINT uTrans = transHandler);
	
		// Overridables
		virtual void AddCtrl(PDU * pPDU, UINT uTrans = transHandler);
		virtual PDU* FindPDU(UINT uNext = 0);
		virtual	void HandleFrame(void);
		virtual	BOOL DoListen(void);
		virtual	BOOL HandleRequest(void);
		virtual	BOOL HandleSpecificRequest(PDU * pPDU, BOOL fRTR);
		virtual	BOOL HandleRequest(PDU * pPDU);
		virtual	BOOL HandleClaimAddressReq(PDU * pPDU);
		virtual	BOOL HandleClaimAddress(void);
		virtual	BOOL HandleTransport(void);
		virtual	BOOL HandleRapidFire(PDU * pPDU);
		virtual	void DoNak(void);
		virtual void FlopBytes(PDU * pPDU, UINT uCount);
		virtual	BOOL IsThisNode(void);
		virtual	BOOL IsDestinationSpecific(ID id);
		virtual	BOOL IsBroadcast(ID id);
		virtual BOOL IsDM(PDU * pPDU);
		
		// Network Management
		BOOL SourceAddrClaim(BOOL fSrc, UINT uTrans = transHandler);
		BOOL CannotClaimAddr(BOOL fSrc);
				
		// Transport Protocol - Flow Control
		BOOL SendMultipacketPDU(PDU * pPDU, BOOL fRetry, UINT uTrans = transHandler);
		BOOL RequestToSend(PDU * pPDU, BOOL fRetry, UINT uTrans = transHandler);
		BOOL ClearToSend(PDU * pPDU);
		BOOL EndOfMsgACK(PDU * pPDU);
		BOOL ConnAbort(PDU * pPDU, UINT uReason, UINT uTrans = transHandler);
		BOOL BroadcastAnnounceMsg(PDU * pPDU, UINT uTrans = transHandler);

		// Helpers
		void Increment(UINT volatile &uIndex);
		BOOL IsISO15765(void);
		BOOL IsJ1939(void);
		BOOL IsMultipacket(PDU * pPDU);
		BOOL IsAddrClaimedReq(PDU * pPDU);
		BOOL IsAddrClaimed(ID id);
		BOOL IsDestinationGlobal(ID id);
		BOOL IsGroupExtension(ID id);
		BOOL IsRequestPGN(void);
		BOOL IsTransferPGN(void);
		BOOL IsTransportPGN(void);
		BOOL IsRTS(void);
		BOOL IsCTS(void);
		BOOL IsBAM(void);
		BOOL IsEndOfMsg(void);
		BOOL IsAbort(void);
		BOOL IsDataTransfer(void);
		BOOL IsSendPDU(PDU * pPDU);
		BOOL IsSetPDU(PDU * pPDU);
		void FindID(ID &id, UINT uID, UINT uPriority, BOOL fSrc);
		UINT FindTotalPackets(PDU * pPDU);
		BOOL SetState(PDU * pPDU, UINT uMode, UINT uState, UINT uTrans = transHandler);
		void ClearData(PDU * pPDU);
		void SetLED(UINT uLed, BOOL fState);
		BOOL IsDM1(PDU * pPDU);
		BOOL IsCommandOnly(PDU * pPDU);
		BOOL IsRapidFire(PDU * pPDU);
		UINT FindTimeout(BYTE bState);
		BOOL Retry(BYTE bState);
	
		// Diagnostics
		void DoDiagnostics(BYTE bDiag);
	};

//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver
//

class CCANJ1939Driver : public CMasterDriver
{
	public:
		// Constructor
		CCANJ1939Driver(void);

		// Destructor
		~CCANJ1939Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);
					
	protected:

		// Device Data
		struct CContext
		{
			BYTE  m_bDA;
			UINT  m_uBackOff;
			BYTE  m_bClrDTC;
			UINT  m_uLast;
			UINT  m_uPGNs;
			PGN * m_pPGNs;
			BOOL  m_fSrc;
			};

		CContext * 	m_pCtx;
		BYTE		m_bSrc;
		UINT            m_bSrc2;
					
		// Handler
		CCANJ1939Handler *	m_pHandler;

		// Implementation
		BOOL  FindID(ID &id, UINT uID, UINT uPriority, BOOL fSrc);
		BOOL  FindID(ID &id, AREF Addr, BOOL fSrc);
		UINT  FindPGNByteSize(PGN * pPGN, UINT uCount);
		UINT  FindPGNBitSize(PGN * pPGN, UINT uCount);
		UINT  FindSPNBitSize(PGN * pPGN, UINT uCount);
		void  SetDirection(PDU * pPDU, BOOL fSend);
		UINT  GetSpecialSize(UINT uIndex, UINT uSize, PGN * pPGN);
		BOOL  IsDead(PGN *pPGN, UINT uOffset);
		void  FlopBytes(PDU * pPDU, BOOL fInit);
		BOOL  IsCommand(PGN * pPGN);

		// Overridables
		virtual	PGN * FindPG(PDU * pPDU);
		virtual BOOL  IsDM(PGN * pPGN);

		// User Access Methods
		BOOL UserSendPGNRqst(CContext * pCtx, UINT uPGN);
		BOOL UserSendPGNData(CContext * pCtx, UINT uPGN);

	};

// End of file

#endif
