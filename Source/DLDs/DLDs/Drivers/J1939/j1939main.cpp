
#include "intern.hpp"

#include "j1939.hpp" 

//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CCANJ1939Driver);

// End of File

