
#include "intern.hpp"

#include "iairobo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// IAI Robocylinder Driver
//		

//struct FAR IairoboCmdDef {
//	char	Com[3];
//	UINT	MinimumDeviceType;
//	UINT	Table;
//	};
	
IairoboCmdDef CODE_SEG CIairoboDriver::CL[] = {
	{"R4",	/* Read/Write Memory (R4,RM,WM)"*/		RCPTYPE,	RWMEM},
	{"n",	/*"Status Inquiry (n)",*/			RCPTYPE,	STINQ},
	{"0",	/*"Status",*/					RCPTYPE,	STATUS},
	{"0",	/*"Alarms",*/					RCPTYPE,	ALARMS},
	{"0",	/*"Input Status",*/				RCPTYPE,	INPUTS},
	{"0",	/*"Output Status",*/				RCPTYPE,	OUTPUT},
	{"a",	/*"Absolute Move (a)",*/			RCPTYPE,	MOVABS},
	{"d",	/*"Stop Motion (d)",*/				RCPTYPE,	STOP},
	{"m",	/*"Increment Move (m)",*/			RCPTYPE,	INCR},
	{"o",	/*"Home (o)",*/					RCPTYPE,	HOME},
	{"0",	/*"Home position (0 -> Motor/Forward)",*/	RCPTYPE,	HOMEPOS},
	{"q",	/*"Servo On/Off (q)",*/				RCPTYPE,	SERVO},
	{"Q3",	/*"Position Move (Q3)",*/			RCPTYPE,	MOVPOS},
	{"0",	/*"Velocity Data",*/				RCPTYPE,	VELC},
	{"0",	/*"Acceleration Data",*/			RCPTYPE,	ACCC},
	{"v",	/*"Send VVE/VAC (v)",*/				RCPTYPE,	VELACC},
	{"Q1",	/*"Move Point Data->Edit Area (Q1)",*/		RCPTYPE,	PTPOSA},// Change 6 Mar 2008
	{"T4",	/*"Send Address Allocation (T4)",*/		RCPTYPE,	ADDRS},
	{"0",	/*"Address Response",*/				RCPTYPE,	ADDRR},
	{"W4",	/*"Send Data (W4)",*/				RCPTYPE,	DATAS},
	{"0",	/*"Data Response",*/				RCPTYPE,	DATAR},
	{"V5",	/*"Move Edit Area->Point (V5)",*/		RCPTYPE,	PTPOSB},// Change 6 Mar 2008
	{"0",	/*"Edit Area->Point Response",*/		RCPTYPE,	BAPOSR},// Change 6 Mar 2008
	{"0",	/*"Source Point Number",*/			RCPTYPE,	ABPOSC},// Change 6 Mar 2008
	{"0",	/*"Address Allocation",*/			RCPTYPE,	ADDRC},
	{"0",	/*"Data Write",*/				RCPTYPE,	DATAC},
	{"0",	/*"Destination Point Number",*/			RCPTYPE,	BAPOSC},// Change 6 Mar 2008
	{"0",	/*"Write 1 Point Item (XQ1-XT4-XW4-XV5)",*/	RCPTYPE,	PTWRT}, // Change 6 Mar 2008
	{"0",	/*"Status of Point Data Write"*/		RCPTYPE,	PTOK},  // Change 6 Mar 2008
	{"i",	/*"Position Band Change (i)"*/			RCPTYPE,	PBAND},
	{"r",	/*"Alarm Reset (r)"*/				RCPTYPE,	ALMR},
	{"c",	/*"Absolute Move mm (c)"*/			RCSTYPE,	MOVMA},
	{"e",	/*"Increment Move mm (e)"*/			RCSTYPE,	MOVMI},
	{"0",	/*"Velocity Data mm"*/				RCSTYPE,	FSPDC},
	{"0",	/*"Acceleration Data mm"*/			RCSTYPE,	FACCC},
	{"f",	/*"Send FVE/FAC mm (f)"*/			RCSTYPE,	FSEND},
	{"k",	/*"Decelerate and Stop (k)"*/			RCSTYPE,	RAMPS},
// Added 6 Mar 2008
	{"0",	/*"Position for XA (Add=0x400)",*/		RCPTYPE,	XWP},
	{"0",	/*"Velocity for XA (Add=0x404)",*/		RCPTYPE,	XWV},
	{"0",	/*"Acceleration for XA (Add=0x405)",*/		RCPTYPE,	XWA},
	{"0",	/*"Moving Current Limit for XA (Add=0x407)",*/	RCPTYPE,	XWM},
	{"0",	/*"Write P/V/A/L to Pt (XQ1-(Add)-XWx-XV5)",*/	RCPTYPE,	XA},
	};									
// Instantiator

INSTANTIATE(CIairoboDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CIairoboDriver::CIairoboDriver(void)
{
	m_Ident      = DRIVER_ID;
	
	CTEXT Hex[]  = "0123456789ABCDEF";

	m_pHex       = Hex;

	m_uPingCount = 0;
	}

// Destructor

CIairoboDriver::~CIairoboDriver(void)
{
	}

// Configuration

void MCALL CIairoboDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CIairoboDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);

	m_fIs9600 = (Config.m_uBaudRate == 9600);
	}
	
// Management

void MCALL CIairoboDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CIairoboDriver::Open(void)
{
	m_pCL = (IairoboCmdDef FAR * )CL;
	}

// Device

CCODE MCALL CIairoboDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = m_pHex[ GetByte(pData) / 2 ];

			m_pCtx->m_dOK = 0;

			m_pCtx->m_dThisDeviceType = 0;

			m_pCtx->m_bHomePosition   = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CIairoboDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CIairoboDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	if( m_uPingCount >= 5 ) {

		ResetComms();
		m_uPingCount = 0;
		}

	Addr.a.m_Table  = RWMEM;
	Addr.a.m_Offset = MODELADDR;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	m_pCtx->m_dThisDeviceType = 0L;

	Data[0] = 0L;

	if( Read(Addr, Data, 1 ) == 1 ) {

		m_pCtx->m_dThisDeviceType = Data[0] >> 24;

		m_uPingCount = 0;

		return 1;
		}

	m_uPingCount++;

	return CCODE_ERROR;
	}

CCODE MCALL CIairoboDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nREAD T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	SetpItem( Addr.a.m_Table == addrNamed ? LOBYTE(Addr.a.m_Offset) : Addr.a.m_Table );

	if( m_pItem == NULL ) return CCODE_ERROR;

	if( Addr.a.m_Table == addrNamed ) {

		if( m_pCtx->m_dThisDeviceType != 0 ) {

			if( !ValidType() ) {

				*pData = 0;

				return 1;
				}
			}

		if( NoReadTransmit( pData ) ) return 1;
		}

	AddCommand(Addr.a.m_Offset, FALSE);

	return Transact() && GetResponse( pData, 0 ) ? 1 : CCODE_ERROR;
	}

CCODE MCALL CIairoboDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	SetpItem( Addr.a.m_Table == addrNamed ? LOBYTE(Addr.a.m_Offset) : Addr.a.m_Table );

//**/	AfxTrace3("\r\n** %x %ld %lx ", Addr.a.m_Offset, *pData, uCount );

	if( m_pItem == NULL ) return CCODE_ERROR;

	if( Addr.a.m_Table == addrNamed ) {

		if( !ValidType() ) return 1;
		}

	else {
		if( m_pCtx->m_dThisDeviceType == RCPTYPE ) return 1; // can't use WM in these devices
		}

	if( NoWriteTransmit( *pData ) ) return 1;

	switch( m_pItem->Table ) {

		case PTWRT:
			m_pCtx->m_dOK = 1;

			while( m_pCtx->m_dOK <= 4 ) {

				PutPointWrite();

				if( Transact() && GetResponse( pData, 0x100 + m_pCtx->m_dOK ) ) m_pCtx->m_dOK++;

				else return CCODE_ERROR;
				}

			return 1;

		case XA:
			m_pCtx->m_dOK = 1;

			while( m_pCtx->m_dOK <= 10 ) {

				PutAllPointData();

				if( Transact() && GetResponse( pData, 0x100 + m_pCtx->m_dOK ) ) m_pCtx->m_dOK++;

				else return 1;
				}

			m_pCtx->m_dOK = 0;

			return 1;
		}

	if( m_pItem->Table == RWMEM ) {

		if( !SetAddressForWMemory(Addr.a.m_Offset) ) return 1;
		}

	AddCommand(*pData, TRUE);

	if( Transact() && GetResponse( pData, 1 ) ) return 1;

	return (++m_uWriteErr) % 3 ? CCODE_ERROR : 1;
	}

// PRIVATE METHODS


// Opcode Handler

void CIairoboDriver::PutPointWrite(void)
{
	StartFrame();

	switch( m_pCtx->m_dOK ) {

		case 1: AddQ( m_pHex[m_pCtx->m_dABPosition & 0xF] ); break;
		case 2: AddT( m_pCtx->m_dAddrAlloc ); break;
		case 3: AddW( m_pCtx->m_dData ); break;
		case 4: AddV( m_pHex[m_pCtx->m_dBAPosition & 0xF] ); break;
		}
	}

void CIairoboDriver::PutAllPointData(void)
{
	StartFrame();

	PDWORD pPD = m_pCtx->m_dXA;

	switch( m_pCtx->m_dOK ) {

		case  1: AddQ( m_pHex[m_pCtx->m_dABPosition & 0xF] ); break;
		case  2: AddT( POSADD );	break;
		case  3: AddW( pPD[0] );	break;
		case  4: AddT( VELADD );	break;
		case  5: AddW( pPD[1] );	break;
		case  6: AddT( ACCADD );	break;
		case  7: AddW( pPD[2] );	break;
		case  8: AddT( LIMADD );	break;
		case  9: AddW( pPD[3] );	break;
		case 10: AddV( m_pHex[m_pCtx->m_dBAPosition & 0xF] ); break;
		}
	}

void CIairoboDriver::AddQ(BYTE bPoint)
{
	AddByte( 'Q' );
	AddByte( '1' );
	AddData( 0x10, 0x100 );
	AddByte( bPoint );
	Pad(5);
	}

void CIairoboDriver::AddT(DWORD dData)
{
	AddByte( 'T' );
	AddByte( '4' );
	AddData( dData, 0x10000000 );
	AddByte( '0' );
	}

void CIairoboDriver::AddW(DWORD dData)
{
	AddByte( 'W' );
	AddByte( '4' );
	AddData( dData, 0x10000000 );
	AddByte( '0' );
	}

void CIairoboDriver::AddV(BYTE bPoint)
{
	AddByte( 'V' );
	AddByte( '5' );
	AddData( 0x10, 0x100 );
	AddByte( bPoint );
	Pad(5);
	}

// Frame Building

void CIairoboDriver::StartFrame(void)
{
	m_uPtr = 0;
	AddByte( STX );
	AddByte( m_pCtx->m_bDrop );
	}

void CIairoboDriver::EndFrame(void)
{
	MakeCheckSum( m_bTx, &m_bTx[13] );
	
	m_bTx[15] = ETX;
	}

void CIairoboDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CIairoboDriver::AddData(DWORD dData, DWORD dFactor)
{
	while ( dFactor ) {

		AddByte( m_pHex[(dData/dFactor) % 16] );

		dFactor /= 16;
		}
	}

void CIairoboDriver::AddCommand( DWORD dData, BOOL fIsWrite )
{
	StartFrame();

	PutText( m_pItem->Com );

	switch( m_pItem->Table ) {

		case RWMEM:

			if( m_pCtx->m_dThisDeviceType != RCPTYPE ) {

				if( fIsWrite ) {

					m_bTx[2] = 'W';
					}

				m_bTx[3] = 'M';
				}

			AddData( dData, 0x10000000 );
			AddByte('0');
			break;

		case STINQ:
		case STOP:
		case RAMPS:
			Pad(10);
			break;

		case ALMR:
			AddData( 3, 0x10 );
			Pad(8);
			break;

		case MOVABS:
		case INCR:
		case PBAND:
		case MOVMA:
		case MOVMI:
			AddData( dData, 0x10000000 );
			Pad(2);
			break;

		case HOME:

			if( dData > 1 ) AddData( dData, 0x10 );

			else {

				switch( m_pCtx->m_dThisDeviceType ) {

					case RCSTYPE:
						AddData( m_pCtx->m_bHomePosition ? 0xA : 9, 0x10 );
						break;

					case RCPTYPE:
					case RCP2TYPE:
						AddData( m_pCtx->m_bHomePosition ? 8 : 7, 0x10 );
						break;
					}
				}
			Pad(8);
			break;

		case SERVO:
			AddByte( dData & 1 ? '1' : '0' );
			Pad(9);
			break;

		case PTPOSA:
		case PTPOSB:
		case MOVPOS:
			AddData( 0x10, 0x100 );
			AddByte( m_pHex[ dData & 0xF ] );
			Pad(5);
			break;

		case VELACC:
			AddByte( '2' );
			AddData( m_pCtx->m_dVel, 0x1000 );
			AddData( m_pCtx->m_dAcc, 0x1000 );
			AddByte( '0' );
			break;

		case ADDRS:
			AddData( dData, 0x10000000 );
			AddByte( '0' );
			break;

		case DATAS:
			AddData( dData, 0x10000000 );
			AddByte( '0' );
			break;

		case FSEND:
			AddByte( '2' );
			AddData( m_pCtx->m_dVelmm, 0x1000 );
			AddData( m_pCtx->m_dAccmm, 0x1000 );
			AddByte( '0' );
			break;
		}
	}
	
void CIairoboDriver::PutText(LPCTXT pCmd)
{
	for( UINT i=0; pCmd[i]; i++ )

		AddByte( BYTE(pCmd[i]) );
	}

// Transport Layer

BOOL CIairoboDriver::Transact(void)
{
	Send();

	return GetReply();
	}

void CIairoboDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CIairoboDriver::GetReply(void)
{
	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uData;

	SetTimer( TIMEOUT );

//**/	AfxTrace0("\r\n");

	while( (uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

//**/		AfxTrace1("<%2.2x>", LOBYTE(uData) );

		m_bRx[uCount++] = LOBYTE(uData);

		if( uData == ETX ) return (uCount == 16) && CheckReply();

		if( uCount > 16 ) return FALSE;
		}	

	return FALSE;
	}

BOOL CIairoboDriver::CheckReply(void)
{
	BYTE	bCk[2];

	MakeCheckSum( m_bRx, bCk );

	if(	m_bRx[0] == STX		&&
		m_bRx[1] == 'U'		&&
		m_bRx[2] == m_bTx[1]	&&
		m_bRx[3] == m_bTx[2]	&&
		bCk[0]   == m_bRx[13]	&&
		bCk[1]   == m_bRx[14]
		)
		return TRUE;

	return FALSE;
	}

BOOL CIairoboDriver::GetResponse( PDWORD pData, UINT uStep )
{
	DWORD dData = GetGeneric( &m_bRx[5], 8 );

	switch( uStep ) {

		case 0: // Read
			switch( m_pItem->Table ) {

				case RWMEM:
					*pData = dData;
					break;

				case STINQ:
					GetStatus();
					*pData = GetGeneric( &m_bRx[4], 8 );
					return TRUE;
				}
			break;

		case 1: // Single Write/Command
			switch( m_pItem->Table ) {

				case ADDRS:
					m_pCtx->m_dAAResp = dData;
					break;

				case DATAS:
					m_pCtx->m_dDataResp = dData;
					break;

				case PTPOSB:
					m_pCtx->m_dBAPosResp = dData;
					break;

				default:
					GetStatus();
					break;
				}
			break;

		case 0x101:
			m_pCtx->m_dBAPosResp = dData;
			break;

		case 0x102:
			m_pCtx->m_dDataResp = dData;
			break;

		case 0x103:
			m_pCtx->m_dAAResp = dData;
			break;

		case 0x104:
			GetStatus();
			break;
		}

	return TRUE;
	}

// Port Access

void CIairoboDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < 16; k++ ) AfxTrace1("[%2.2x]", m_bTx[k] );

	m_pData->Write( PCBYTE(m_bTx), 16, FOREVER );
	}

UINT CIairoboDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

BOOL CIairoboDriver::ValidType(void)
{
	return m_pItem->MinimumDeviceType <= m_pCtx->m_dThisDeviceType;
	}

BOOL CIairoboDriver::NoReadTransmit( PDWORD pData )
{
	switch( m_pItem->Table ) {

		case STATUS:
			*pData = m_dStatus;
			return TRUE;

		case ALARMS:
			*pData = m_dAlarm;
			return TRUE;

		case INPUTS:
			*pData = m_dInput;
			return TRUE;

		case OUTPUT:
			*pData = m_dOutput;
			return TRUE;

		case VELC:
			*pData = m_pCtx->m_dVel;
			return TRUE;

		case ACCC:
			*pData = m_pCtx->m_dAcc;
			return TRUE;

		case ABPOSC:
			*pData = m_pCtx->m_dABPosition;
			return TRUE;

		case ADDRC:
			*pData = m_pCtx->m_dAddrAlloc;
			return TRUE;

		case ADDRR:
			*pData = m_pCtx->m_dAAResp;
			return TRUE;

		case DATAC:
			*pData = m_pCtx->m_dData;
			return TRUE;

		case DATAR:
			*pData = m_pCtx->m_dDataResp;
			return TRUE;

		case BAPOSC:
			*pData = m_pCtx->m_dBAPosition;
			return TRUE;

		case BAPOSR:
			*pData = m_pCtx->m_dBAPosResp;
			return TRUE;

		case PTOK:
			*pData = m_pCtx->m_dOK;
			return TRUE;

		case FSPDC:
			*pData = m_pCtx->m_dVelmm;
			return TRUE;

		case FACCC:
			*pData = m_pCtx->m_dAccmm;
			return TRUE;

		case HOMEPOS:
			*pData = m_pCtx->m_bHomePosition;
			return TRUE;

		case XWP:
		case XWV:
		case XWA:
		case XWM:
			*pData = m_pCtx->m_dXA[m_pItem->Table - XWP];
			return TRUE;

		case MOVABS:
		case STOP:
		case INCR:
		case HOME:
		case MOVPOS:
		case VELACC:
		case PTPOSA:
		case ADDRS:
		case DATAS:
		case PTPOSB:
		case PTWRT:
		case PBAND:
		case ALMR:
		case MOVMA:
		case MOVMI:
		case FSEND:
		case RAMPS:
			*pData = 0xFFFFFFFF;
			return TRUE;

		case SERVO:
		case XA:
			return TRUE;
		}

	return FALSE;
	}

BOOL CIairoboDriver::NoWriteTransmit( DWORD dData )
{
	switch( m_pItem->Table ) {

		case STINQ:
		case STATUS:
		case ALARMS:
		case INPUTS:
		case OUTPUT:
			return TRUE;

		case VELC:
			m_pCtx->m_dVel = dData;
			return TRUE;

		case ACCC:
			m_pCtx->m_dAcc = dData;
			return TRUE;

		case ABPOSC:
			m_pCtx->m_dABPosition = dData;
			return TRUE;

		case ADDRC:
			m_pCtx->m_dAddrAlloc = dData;
			return TRUE;

		case DATAC:
			m_pCtx->m_dData = dData;
			return TRUE;

		case BAPOSC:
			m_pCtx->m_dBAPosition = dData;
			return TRUE;

		case FSPDC:
			m_pCtx->m_dVelmm = dData;
			return TRUE;

		case FACCC:
			m_pCtx->m_dAccmm = dData;
			return TRUE;

		case ADDRR:
			m_pCtx->m_dAAResp = 0;
			return TRUE;

		case DATAR:
			m_pCtx->m_dDataResp = 0;
			return TRUE;

		case BAPOSR:
			m_pCtx->m_dBAPosResp = 0;
			return TRUE;

		case PTOK:
			m_pCtx->m_dOK = 0;
			return TRUE;

		case HOMEPOS:
			m_pCtx->m_bHomePosition = dData;
			return TRUE;

		case XWP:
		case XWV:
		case XWA:
		case XWM:
			m_pCtx->m_dXA[m_pItem->Table - XWP] = dData;
			return TRUE;

		case XA:
			return !dData;
		}

	return FALSE;
	}

void CIairoboDriver::SetpItem(UINT uID)
{
	m_pItem = m_pCL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == m_pItem->Table ) return;

		m_pItem++;
		}

	m_pItem = NULL;
	}

void CIairoboDriver::GetStatus(void)
{
	m_dStatus = GetGeneric( &m_bRx[4],  2 );
	m_dAlarm  = GetGeneric( &m_bRx[6],  2 );
	m_dInput  = GetGeneric( &m_bRx[8],  2 );
	m_dOutput = GetGeneric( &m_bRx[10], 2 );
	}

BOOL CIairoboDriver::SetAddressForWMemory(UINT uAddress)
{
	StartFrame();

	AddT(uAddress);

	return Transact();
	}

DWORD CIairoboDriver::GetGeneric( PBYTE p, UINT uCt )
{
	DWORD d = 0L;
	BYTE b;

	for( UINT i = 0; i < uCt; i++ ) {

		d <<= 4;

		b = p[i];
		
		if( b >= '0' && b <= '9' )
			d += (b - '0');

		if( b >= 'a' && b <= 'f' )
			d += (b - 'W');

		if( b >= 'A' && b <= 'F' )
			d += (b - '7');
		}

	return d;
	}

void CIairoboDriver::MakeCheckSum( PBYTE pSrc, PBYTE pDest )
{
	BYTE bCheck = 0;

	for( UINT i = 1; i < 13; i++ ) {

		bCheck += pSrc[i];
		}

	bCheck = 0x100 - (bCheck & 0xFF );

	pDest[0] = m_pHex[ bCheck/16 ];
	pDest[1] = m_pHex[ bCheck%16 ];
	}

void CIairoboDriver::Pad( UINT uCt )
{
	while( uCt-- ) AddByte( '0' );
	}

void CIairoboDriver::ResetComms(void)
{
	m_pData->SetBreak(TRUE);

	Sleep(1000);

	m_pData->SetBreak(FALSE);
	}

// End of File
