#include "mlinka.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SEW Movilink B Serial Master Driver
//

class CSEWMovilinkB : public CSEWMovilinkA 
{
	public:
		// Constructor
		CSEWMovilinkB(void);

		// Destructor
		~CSEWMovilinkB(void);
	};

// End of File
