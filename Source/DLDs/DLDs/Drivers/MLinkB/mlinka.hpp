//////////////////////////////////////////////////////////////////////////
//
// SEW Movilink A Constants
//

#define	FRAME_TIMEOUT	1000   

#define SD1		0x02
#define SD2		0x1D
#define TYP		0x86
#define RES		0x00  
#define LEN		0x03 
#define WAIT		50
#define EORX		0x0C
#define HEAD		0x05
#define MANAGE		0x03
#define IDLE		7

#define MOVITRAC	1
#define PDPA		3
#define SPPA		4
//#define PIPA		0xFFE0 

//#define CFG_PIPA	0x0001
#define CMD_BROADCAST	255
#define CMD_EXCEPTION	0xFFFF

#define SW_OFFSET	10
#define ID_STATUS	0x2076

//////////////////////////////////////////////////////////////////////////
//
// SEW Movilink A Serial Master Driver
//

class CSEWMovilinkA : public CMasterDriver
{
	public:
		// Constructor
		CSEWMovilinkA(void);

		// Destructor
		~CSEWMovilinkA(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		

	protected:

		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			DWORD	m_dwLatestError;
			UINT 	m_uDevice;
			UINT    m_uPO[3];
			UINT    m_uPI[3];
			BOOL	m_fHandshake;
						
			};

		
		CContext * m_pCtx;

		// Data Members
		BYTE	m_bRx[40];
		BYTE	m_bTx[40];
		UINT	m_uPtr;
		BYTE	m_bCheck;
		UINT    m_uCount;
				
		// Implementation
		BOOL PutRead(UINT uOffset, UINT uOp);
		BOOL PutData(PDWORD pData);
		BOOL PutWrite(UINT uOffset, UINT uOp, PDWORD pData);
		
		CCODE DoProcessData(UINT uOffset, UINT uOp);
		CCODE SaveProcessData(UINT uAddr, PDWORD pData);
		
		BOOL PutProcessData(UINT uOffset, UINT uOp);
		BOOL PutProcessDataPDU(UINT uAddr, UINT uOptions);
		UINT GetOffset(UINT uAddr);
	      	BOOL PutPDU(UINT uOffset, UINT uOp, BOOL fWrite);
		BOOL PutManagement(UINT uOffset, UINT uOp, BOOL fWrite);
		BYTE GetServiceID(UINT uOp, BOOL fWrite);
		BYTE GetDataLength(void);
		BYTE GetHandshake(void);
		BYTE GetStatus(void); 
		BOOL PutData(void); 
		
		void GetGeneric(PDWORD pData);
		void GetBit(PDWORD pData, BYTE bBit);
		void DecodeStatusWord1(PDWORD pData, BYTE bBit);
		BOOL GetReply(BYTE bLength);
		BOOL CheckReply(BYTE bLength); 
		BOOL IsError(BYTE Management);
	                                                
		BOOL Start(void); 
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void Send(void);
		void End(void);

		
	};

// End of File
