
//////////////////////////////////////////////////////////////////////////
//
// Toshiba Tosvert Constants
//
//

#define	START_CMND_CODE		'('
#define END_CMND_CODE		')'
#define	ADDR_INC_CODE		'+'
#define INVTR_TRIPPED_CODE	'#'
#define PRE_CHECKSUM_CODE	'&'
#define TERMINATOR_CODE		13
#define ADDRESS_CMND_CODE	'A'
#define READ_CMND_CODE		'R'
#define WRITE_CMND_CODE		'W'
#define MASK_CMND_CODE		'M'
#define BANK_CMND_CODE		'B'
#define TEST_CMND_CODE		'T'
#define ERROR_CMND_CODE		'N'
#define	RAM_BANK		0
#define EEPROM_BANK		1
#define INT_ROM_BANK		2
#define EXT_ROM_BANK		3
#define OPTION_BUS_BANK		4
#define RAM_DATA_SPACE		'A'
#define EEPROM_DATA_SPACE	'P'
#define INT_ROM_DATA_SPACE	'I'
#define EXT_ROM_DATA_SPACE	'E'
#define OPTION_BUS_DATA_SPACE	'O'
#define	MISC_DATA_SPACE		'M'
#define	CURR_TRIPPED_STATE_MISC	0
#define LAST_ERROR_CODE_MISC	1
#define	INITIAL_STATE		0
#define	RX_DATA_STATE		1
#define RX_TERM_STATE		2			
#define FRAME_TIMEOUT		1000		
#define MIN_REPLY_LENGTH	10

//////////////////////////////////////////////////////////////////////////
//
// Toshiba Tosvert Master Serial Driver
//
//

class CTosvertSerialMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CTosvertSerialMasterDriver(void);

		// Destructor
		~CTosvertSerialMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE	m_bID;
			};

		CContext * m_pCtx;
		LPCTXT	   m_pHex;
		BYTE	   m_bTx[300];
		BYTE	   m_bRx[300];
		UINT	   m_uPtr;
		WORD       m_wCheck;
		UINT	   m_LastError;
		UINT	   m_State;
		UINT	   m_CurrentBank;
		UINT       m_RxLen;
							
		// Frame Building
		void AddByte(BYTE bByte);
		BOOL PutCmnd(BYTE bCmnd, UINT uData, BOOL fIncrement);
										
		// Transport Layer
		BOOL Send(void);
		BOOL Recv(BYTE bCmnd, UINT &uData);
		BOOL CheckFrame(BYTE bCmnd, UINT &uData);

		// Implementation
		void FormChecksum(UINT uCheck, UINT &uASCIICheck);
	      	BOOL ValidCheck(void);
		BOOL SetBank(UINT uType);
		BOOL SetAddress(UINT uOffset);
		void atox(PSTR pText, UINT &uData);
		BOOL GetMiscData(PDWORD pData, UINT uOffset);

	};

// End of File

