
#include "intern.hpp"

#include "melbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Melsec Base Master Driver
//

// Constructor

CMelBaseMasterDriver::CMelBaseMasterDriver(void)
{
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex; 
	}

// Destructor

CMelBaseMasterDriver::~CMelBaseMasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CMelBaseMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 'D';
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CMelBaseMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Start() ) {

		UINT uType = Addr.a.m_Type;

		GetCount(uCount,uType, FALSE);
	
		AddID();

		AddCommand(uType, GetCommand(uType, FALSE), uCount);

		AddAddress(Addr);

		BOOL fDouble = ((uType == addrWordAsLong) || (uType == addrWordAsReal));

		AddCount(fDouble  ? uCount * 2 : uCount);

		UINT uTotal = uType == addrBitAsBit ? uCount / 2 : uCount * 2;

		if( fDouble ) {

			uTotal *= 2;
			}

		if( Transact(uTotal + 2) ) {

			switch( uType ) {

				case addrBitAsBit:

					return GetBits( pData, uCount );

				case addrBitAsWord:
				case addrWordAsWord:

					return GetWords( pData, uCount );

				case addrWordAsLong:
				case addrWordAsReal:

					return GetLongs( pData, uCount );
				}
			}
		}
      
	return CCODE_ERROR;
	}

CCODE MCALL CMelBaseMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Start() ) {

		UINT uType = Addr.a.m_Type;
		
		GetCount(uCount, uType, TRUE);

		AddID();

		AddCommand(uType, GetCommand(uType, TRUE), uCount);

		AddAddress(Addr);

		BOOL fDouble = ((uType == addrWordAsLong) || (uType == addrWordAsReal));

		AddCount(fDouble ? uCount * 2 : uCount);

		switch( uType ) {

			case addrBitAsBit:

				SetBits( pData, uCount );
				break;

			case addrBitAsWord:
			case addrWordAsWord:

				SetWords( pData, uCount );
				break;

			case addrWordAsLong:
			case addrWordAsReal:

				SetLongs( pData, uCount );
				break;
			}

		if ( Transact(2) ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

// Overridables

BOOL CMelBaseMasterDriver::Start(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));
	
	m_uPtr = 0;

	m_bCheck = 0;

	m_DLE = 0;

	return TRUE;
	}

void CMelBaseMasterDriver::AddID(void)
{
       }

void CMelBaseMasterDriver::AddCommand(UINT uType, UINT uCommand, UINT uCount)
{
       	}

void CMelBaseMasterDriver::AddAddress(AREF Addr)
{
       	}

void CMelBaseMasterDriver::AddOffset(UINT uOffset)
{
	} 

void CMelBaseMasterDriver::AddCount(UINT uCount)
{
	AddWord(uCount);    
	}

void CMelBaseMasterDriver::GetCount(UINT &uCount, UINT uType, BOOL fWrite)
{
	switch ( uType ) {

		case addrBitAsBit:

			MakeMin(uCount, UINT(!fWrite ? 160 : 64));

			return;

		case addrWordAsLong:
		case addrWordAsReal:

			MakeMin(uCount, UINT(!fWrite ? 16 : 5));

			return;
	       			
		}

	MakeMin(uCount, UINT(!fWrite ? 32 : 10));
	}

WORD CMelBaseMasterDriver::GetCommand(UINT uType, BOOL fWrite)
{
	return 0;
	}

BOOL CMelBaseMasterDriver::Transact(UINT uTotal) 
{
	return FALSE;
	}

// Implementation

CCODE CMelBaseMasterDriver::GetBits(PDWORD pData, UINT Count)
{
	if( m_pBase->m_fCode ) {

		return GetASCIIBits(pData, Count);
		}

	UINT b = 2;

	BYTE m = 0x10;

	for( UINT n = 0; n < Count; n++ ) {

		pData[n] = (m_bRxBuff[b] & m) ? TRUE : FALSE;

		if( !(m >>= 4) ) {

			b = b + 1;

			m = 0x10;
			}
		}
       	
	return Count;
	}

CCODE CMelBaseMasterDriver::GetASCIIBits(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = GetBitData(PCSTR(m_bRxBuff + 4 + u));
		}

	return uCount;
	}

BOOL CMelBaseMasterDriver::GetBitData(PCTXT pText)
{
	char cData = *(pText);

	if( cData == '1' ) {

		return TRUE;
		}

	return FALSE;
      	}

CCODE CMelBaseMasterDriver::GetWords(PDWORD pData, UINT uCount)
{
	if( m_pBase->m_fCode ) {

		return GetASCIIWords(pData, uCount);
		}
		
	for( UINT u = 0; u < uCount; u++ ) { 
	
		WORD x = PU2(m_bRxBuff + 2)[u];

		pData[u] = LONG(SHORT(IntelToHost(x)));
		}
	
	return uCount;
	}

CCODE CMelBaseMasterDriver::GetASCIIWords(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = GetWordData(PCSTR(m_bRxBuff + 4 + 4 * u), 4);
		}

	return uCount;
	}

WORD CMelBaseMasterDriver::GetWordData(PCTXT pText, UINT uCount)
{
	WORD wData = 0;

	while( uCount-- )  {

		char cData = *(pText++);

		if( cData >= '0' && cData <= '9' ) {

			wData = 16 * wData + cData - '0';
			}

		else if ( cData >= 'A' && cData <= 'F' ) {

			wData = 16 * wData + cData - 'A' + 10;
			}

		else if ( cData >= 'a' && cData <= 'f' ) {

			wData = 16 * wData + cData - 'a' + 10;
			}

		else break;
		}

	return wData;
	}

CCODE CMelBaseMasterDriver::GetLongs(PDWORD pData, UINT uCount)
{
	if( m_pBase->m_fCode ) {

		return GetASCIILongs(pData, uCount);
		}
		
	for( UINT u = 0; u < uCount; u++ ) { 
		
		DWORD x = PU4(m_bRxBuff + 2)[u];

		pData[u] = IntelToHost(x);
		}
	
	return uCount;
	}

CCODE CMelBaseMasterDriver::GetASCIILongs(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		WORD Hi =  GetWordData(PCSTR(m_bRxBuff + 4 + (8 * u) + 4), 4);

		WORD Lo =  GetWordData(PCSTR(m_bRxBuff + 4 + (8 * u)), 4);

		pData[u] = MAKELONG(Lo, Hi);
		}

	return uCount;
	}

void CMelBaseMasterDriver::SetWords(PDWORD pData, UINT uCount)
{
	if( m_pBase->m_fCode ) {

		SetASCIIWords(pData, uCount);

		return;
		}

	for ( UINT u = 0; u < uCount; u++ ) {

		AddWord(WORD(pData[u]));
		}
	}

void CMelBaseMasterDriver::SetASCIIWords(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddHex(pData[u], 0x1000);
		}
	}

void CMelBaseMasterDriver::SetLongs(PDWORD pData, UINT uCount)
{
	if( m_pBase->m_fCode ) {

		SetASCIILongs(pData, uCount);

		return;
		}

	for ( UINT u = 0; u < uCount; u++ ) {

		AddLong(pData[u]);
		}
	}

void CMelBaseMasterDriver::SetASCIILongs(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x = pData[u] << 16;

		x = x | (pData[u] >> 16);

		AddHex(x, 0x10000000); 	
		}
	}

void CMelBaseMasterDriver::SetBits(PDWORD pData, UINT Count)
{
	if( m_pBase->m_fCode ) {

		SetASCIIBits(pData, Count);

		return;
		}

	UINT b = 0;

	BYTE m = 0x10;

	for( UINT n = 0; n < Count; n++ ) {

		if( pData[n] ) {
					
			b |= m;
			}

		if( !(m >>= 4) ) {

			AddByte(b);
		
			b = 0;

			m = 0x10;
			}
		}

	if( Count % 2 ) {

		AddByte(b);
		}
	}

void CMelBaseMasterDriver::SetASCIIBits(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddByte( pData[u] ? 0x31 : 0x30);
		}
	}

UINT CMelBaseMasterDriver::GetSubCommand(UINT Type)
{
	switch( Type ) {

		case addrBitAsBit:

			return MITQ_BIT;
		}

	return MITQ_WORD;
	}

// Frame Building
	 
void CMelBaseMasterDriver::AddByte(BYTE bValue, BOOL fDLE)
{
	if( fDLE && bValue == DLE ) {

		m_bTxBuff[m_uPtr++] = DLE; 

		m_DLE++;
		}
		
	m_bTxBuff[m_uPtr++] = bValue; 
	
	m_bCheck += bValue; 
	}

void CMelBaseMasterDriver::AddPrefix(UINT Table)
{ 
	if( IsSpecial(Table) ) {

		AddASCIISpecialPrefix2(Table);

		AddASCIISpecialPrefix1(Table);

		return;
		}

	switch ( Table ) {
	
		case 'T':  	
		case 'C': 
			
			AddByte(0x4E);
			break; 

		default:
			AddByte(0x20);
			break;
		}

	AddByte(Table);
	}

void CMelBaseMasterDriver::AddBinaryPrefix(UINT Table)
{ 
	switch ( Table ) {
	
		case 'X':	AddByte(0x9C);	break;
		case 'Y':	AddByte(0x9D);	break;
		case '1':	AddByte(0xA2);	break;
		case '2':	AddByte(0xA3);  break;
		case 'F': 	AddByte(0x93);	break;
		case 'M':	AddByte(0x90);	break;
		case 'L': 	AddByte(0x92);	break;
		case 'B': 	AddByte(0xA0);	break;
		case 'S': 	AddByte(0x98);	break;
		case 'V':	AddByte(0x94);	break;
		case '4':	AddByte(0xA1);  break;
		case '5':	AddByte(0xB5);	break;
		case 'D':	AddByte(0xA8);	break; 
		case 'R':	AddByte(0xAF);	break;
		case 'Z':	AddByte(0xCC);	break;
		case 'W':	AddByte(0xB4);	break;
		case 'T':	AddByte(0xC2);	break;
		case 'C':	AddByte(0xC5);	break;
		case '3':	AddByte(0xC8);	break;
		case '6':	AddByte(0x91);  break;
		case '7':	AddByte(0xA9);	break;
		case '8':	AddByte(0xC1);	break;
		case '9':	AddByte(0xC0);	break;
		case 'A':	AddByte(0xC4);	break;
		case 'E':	AddByte(0xC3);	break;
		case '0':	AddByte(0xB0);	break;
		}
	}

void CMelBaseMasterDriver::AddASCIIPrefix(UINT Table)
{
	if( IsSpecial(Table) ) {

		AddASCIISpecialPrefix1(Table);

		AddASCIISpecialPrefix2(Table);

		return;
		}

	AddByte(Table);
	
	switch( Table ) {
	
		case 'T':  	
		case 'C': 
			
			AddByte(0x4E);
			break; 
		}
	}

void CMelBaseMasterDriver::AddASCIISpecialPrefix1(UINT uTable)
{
	switch( uTable ) {

		case '1':
		case '2':	AddByte('D');	break;

		case '3':
		case '4':
		case '5':
		case '6':
		case '7':	AddByte('S');	break;

		case '8':
		case '9':	AddByte('T');	break;

		case 'A':
		case 'E':	AddByte('C');	break;
		}
	}

void CMelBaseMasterDriver::AddASCIISpecialPrefix2(UINT uTable)
{
	switch( uTable ) {

		case '1':	AddByte('X');	break;
		case '2':	AddByte('Y');	break;
		case '3':	AddByte('N');	break;
		case '4':	AddByte('B');	break;
		case '5':	AddByte('W');	break;
		case '6':	AddByte('M');	break;
		case '7':	AddByte('D');	break;
		case '8':
		case 'A':	AddByte('S');	break;
		case '9':	
		case 'E':	AddByte('C');	break;
		}
	}

void CMelBaseMasterDriver::AddHex(UINT uValue, UINT uMask)
{
	if( m_pBase->m_fCode ) { 

		for( ; uMask; uMask /= 16 ) {

			AddByte(m_pHex[(uValue / uMask) % 16]);
			}

		return;
		}

	for( UINT u = 1; u <= uMask; u *= 16 ) {

		AddByte((uValue / u) % 16);
		}
	}

void CMelBaseMasterDriver::AddDec(UINT uValue, UINT uMask)
{
	for( ; uMask; uMask /= 10 ) {

		AddByte(m_pHex[(uValue / uMask) % 10]);
		}
	}

void CMelBaseMasterDriver::AddWord(WORD Word)
{
	if( m_pBase->m_fCode ) {

		AddHex(Word, 0x1000);

		return;
	       	}

	AddByte(LOBYTE(Word));

	AddByte(HIBYTE(Word));
	}

void CMelBaseMasterDriver::AddLong(DWORD dwWord)
{
	if( m_pBase->m_fCode ) {

		AddHex(dwWord, 0x10000000);

		return;
	       	}

	AddWord(LOWORD(dwWord));

	AddWord(HIWORD(dwWord));
	}

BOOL CMelBaseMasterDriver::IsSpecial(UINT uTable)
{
	switch(uTable) {

		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 'A':
		case 'E':

			return TRUE;
		}

	return FALSE;
	}

// End of File
