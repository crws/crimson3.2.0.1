
#include "intern.hpp"

#include "cangen.hpp" 

//////////////////////////////////////////////////////////////////////////
//
// CAN Generic 11-bit / 29-bit ID Entry Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Imports

#include "raw29id\r29id.cpp"

// Instantiator

INSTANTIATE(CCANGenericDriver);

// Constructor

CCANGenericDriver::CCANGenericDriver(void)
{
	m_Ident = DRIVER_ID;
       	}

// Configuration

void MCALL CCANGenericDriver::CheckConfig(CSerialConfig &Config)
{
	CCAN29bitIdEntryRawDriver::CheckConfig(Config);
	
	Config.m_uFlags |= flagExtPass;
	}
	
// Management

void MCALL CCANGenericDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CCANGenericHandler(m_pHelper);

	pPort->Bind(m_pHandler);
	}

// Device

CCODE MCALL CCANGenericDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);
	
	if( !(m_pIdent29 = (CIdent29 *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pIdent29		= new CIdent29;

			m_pCtx			= m_pIdent29;

			m_pIdent29->m_uBackOff	= GetLong(pData);

			if( GetWord(pData) == 0x1234 ) {

				m_pIdent29->m_uPGNs = GetWord(pData);
			
				m_pIdent29->m_pIDs  = new ID29[m_pIdent29->m_uPGNs];

				for( UINT x = 0; x < m_pIdent29->m_uPGNs; x++ ) {

					m_pIdent29->m_pIDs[x].m_Number   = GetLong(pData);

					m_pIdent29->m_pIDs[x].m_Ref      = GetLong(pData);

					m_pIdent29->m_pIDs[x].m_SendReq  = GetByte(pData);

					m_pIdent29->m_pIDs[x].m_uRequest = GetTickCount();

					m_pIdent29->m_pIDs[x].m_Diag	 = 0;

					m_pIdent29->m_pIDs[x].m_Enh	 = 0;

					m_pIdent29->m_pIDs[x].m_Dir	 = 0;

					if( GetWord(pData) == 0x1234 ) {

						m_pIdent29->m_pIDs[x].m_uSPNs   = GetWord(pData);
						 
						m_pIdent29->m_pIDs[x].m_pSPNs   = new SPN[m_pIdent29->m_pIDs[x].m_uSPNs];
	
						for( UINT y = 0; y < m_pIdent29->m_pIDs[x].m_uSPNs; y++ ) {

							m_pIdent29->m_pIDs[x].m_pSPNs[y].m_Size = GetByte(pData) & 0x7F;
							}

						m_pIdent29->m_pIDs[x].m_RepRate = GetLong(pData);

						BYTE bActive                    = GetByte(pData);

						BYTE bEnhanced                  = GetByte(pData);

						BYTE bExt                       = GetByte(pData) ? 0x00 : 0x80;

						if( bActive ) {

							ID id;

							FindID(id, m_pIdent29->m_pIDs[x].m_Number, 0);

							UINT uBytes = FindPGNByteSize(&m_pIdent29->m_pIDs[x], m_pIdent29->m_pIDs[x].m_uSPNs);
										
							m_pHandler->MakePDU(&id,
									    uBytes,
									    m_pIdent29->m_pIDs[x].m_RepRate,
									    0,
									    bEnhanced,
									    0,
									    m_pIdent29->m_pIDs[x].m_Ref,
									    bExt);
							}
						}
					}
				}
									 
			m_pIdent29->m_uLast = GetTickCount();
			
			pDevice->SetContext(m_pIdent29);
			
			m_pHandler->ShowPDUs();

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx = m_pIdent29;

	return CCODE_SUCCESS;
 	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Generic 11-bit / 29-bit ID Entry Handler
//

// Constructor

CCANGenericHandler::CCANGenericHandler(IHelper *pHelper) : CCAN29bitIdEntryRawHandler(pHelper)
{
	}

// Implementation

void CCANGenericHandler::AddCtrl(PDU * pPDU, UINT uTrans)
{
	UINT uBytes = pPDU->m_uBytes;

	MakeMin(uBytes, 8);

	m_Tx[uTrans].m_Ctrl = uBytes | pPDU->m_bExt;
	}

PDU* CCANGenericHandler::FindPDU(void)
{
	UINT uMask = GetMask();

	for( UINT u = 0; u < m_uPDUs; u++ ) {

		if( ((m_RxData.m_ID & uMask) == (m_pPDUs[u].m_ID.m_Ref & uMask)) ) {

			if( m_RxData.m_Zero == m_pPDUs[u].m_bExt >> 7 ) {

				return &m_pPDUs[u];
				}
			}
		}
      
	return NULL;
	}

// End of File

