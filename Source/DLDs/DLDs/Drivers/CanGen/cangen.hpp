
//////////////////////////////////////////////////////////////////////////
//
// CAN Generic 11-bit / 29-bit ID Entry Driver
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#include "..\raw29id\r29id.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw 29-bit Identifier Entry Handler
//

class CCANGenericHandler : public CCAN29bitIdEntryRawHandler
{	
	public:
		// Constructor
		CCANGenericHandler(IHelper *pHelper);

	protected:
		
		// Implementation
		void  AddCtrl(PDU * pPDU, UINT uTrans = transHandler);
		PDU*  FindPDU(void);
		
	};

/////////////////////////////////////////////////////////////////////////
//
// CAN Generic 11-bit / 29-bit ID Entry Driver
//

class CCANGenericDriver : public CCAN29bitIdEntryRawDriver
{
	public:
		// Constructor
		CCANGenericDriver(void);

		// Configuration
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
				
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
	};

// End of file
