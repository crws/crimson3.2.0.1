
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "yunivsmc.hpp"
	
YUNIVSMCCmdDef CODE_SEG CYaskawaUnivSMCDriver::CL[] = {
{"AB", /* "Abort Motion and Program",*/			CM_COM0,	1},
{"AB", /* "Abort Motion Only",*/			CM_COM1,	2},
{"AC", /* "Acceleration",*/				CM_BOTH3,	3},
{"AD", /* "After Distance",*/				CM_WRITE1,	4},
{"AE", /* "Absolute Encoder",*/				CM_WRITE1,	126},
{"AF", /* "Enable Digital Feedback",*/			CM_BOTH0,	5},
{"AF", /* "Enable Analog Feedback",*/			CM_BOTH1,	6},
{"AI", /* "After Input",*/				CM_WRITE1,	7},
{"AL", /* "Arm Latch",*/				CM_COM3,	8},
{"AM", /* "After Motion",*/				CM_COM3,	9},
{"AO", /* "Analog Output",*/				CM_WRITE1,	10},
{"AP", /* "After Absolute Postion",*/			CM_WRITE1,	11},
{"AR", /* "After Relative Distance",*/			CM_WRITE1,	12},
{"AS", /* "At Speed",*/					CM_COM3,	13},
{"AT", /* "At Time",*/					CM_WRITE1,	14},
{"BG", /* "Begin Motion",*/				CM_COM3,	15},
{"BL", /* "Reverse Software Limit",*/			CM_BOTH3,	16},
{"BN", /* "Burn",*/					CM_COM4,	17},
{"BP", /* "Burn Program",*/				CM_COM4,	18},
{"BV", /* "Burn Variables",*/				CM_COM4,	19},
{"CB", /* "Clear Bit",*/				CM_WRITE1,	20},
{"CD", /* "Contour Data",*/				CM_WRITE1,	21},
{"CE", /* "Configure Encoder",*/			CM_BOTH3,	22},
{"CM", /* "Contour Mode",*/				CM_COM3,	23},
{"DC", /* "Deceleration",*/				CM_BOTH3,	24},
{"DE", /* "Dual (Auxiliary) Encoder Position",*/	CM_BOTH3,	25},
{"DP", /* "Define Position",*/				CM_BOTH3,	26},
{"DT", /* "Delta Time",*/				CM_BOTH2,	27},
{"DV", /* "Dual Velocity Enable (Dual Loop)",*/		CM_BOTH3,	28},
{"EA", /* "ECAM Master Axis",*/				CM_WRITE2,	29},
{"EB", /* "Enable ECAM Mode Enable",*/			CM_BOTH2,	30},
{"EC", /* "ECAM Counter",*/				CM_BOTH2,	31},
{"EG", /* "ECAM Engage",*/				CM_WRITE1,	32},
{"EM", /* "ECAM Cycle",*/				CM_WRITE1,	33},
{"EQ", /* "ECAM Quit (Disengage)",*/			CM_BOTH2,	34},
{"ER", /* "Error Limit",*/				CM_BOTH3,	35},
{"FA", /* "Acceleration Feed Forward",*/		CM_BOTH3,	36},
{"FE", /* "Find Edge",*/				CM_COM3,	37},
{"FI", /* "Find Index",*/				CM_COM3,	38},
{"FL", /* "Forward Software Limit",*/			CM_BOTH3,	39},
{"FV", /* "Velocity Feed Forward",*/			CM_BOTH3,	40},
{"GA", /* "Master Axis for Gearing",*/			CM_COM3,	41},
{"GR", /* "Gear Ratio",*/				CM_BOTH2,	42},
{"HM", /* "Home",*/					CM_COM3,	43},
{"HX", /* "Halt Task Execution",*/			CM_WRITE1,	44},
{"IL", /* "Integrator Limit",*/				CM_BOTH3,	45},
{"IP", /* "Increment Position",*/			CM_BOTH3,	46},
{"IT", /* "Independent Time Constant",*/		CM_BOTH3,	47},
{"JG", /* "Jog",*/					CM_BOTH3,	48},
{"KD", /* "Derivative Constant",*/			CM_BOTH3,	49},
{"KI", /* "Integrator",*/				CM_BOTH3,	50},
{"KP", /* "Proportional Constant",*/			CM_BOTH3,	51},
{"MC", /* "Motion Complete (In Position)",*/		CM_COM3,	52},
{"MF", /* "Forward Motion to Position",*/		CM_WRITE1,	53},
{"MM", /* "Master Modulus",*/				CM_WRITE1,	54},
{"MO", /* "Motor Off",*/				CM_COM4,	55},
{"MR", /* "Reverse Motion to Position",*/		CM_WRITE1,	56},
{"MT", /* "Motor Type",*/				CM_BOTH2,	57},
{"NA", /* "Number of Axes",*/				CM_BOTH2,	58},
{"OB", /* "Output Bit",*/				CM_WRITE1,	59},
{"OE", /* "Off on Error - Enable/Disable",*/		CM_BOTH3,	60},
{"OF", /* "Offset",*/					CM_BOTH3,	61},
{"OP", /* "Output Port",*/				CM_BOTH2,	62},
{"PA", /* "Position Absolute",*/			CM_BOTH3,	63},
{"PR", /* "Position Relative",*/			CM_BOTH3,	64},
{"RL", /* "Report Latched Position",*/			CM_READ2,	65},
{"RP", /* "Reference Position",*/			CM_READ2,	127},
{"RS", /* "Reset",*/					CM_COM4,	66},
{"SB", /* "Set Bit",*/					CM_WRITE1,	67},
{"SC", /* "Stop Code",*/				CM_READ2,	125},
{"SH", /* "Servo Here",*/				CM_COM3,	68},
{"SP", /* "Speed",*/					CM_BOTH3,	69},
{"ST", /* "Stop",*/					CM_COM3,	70},
{"TB", /* "Tell Status Byte",*/				CM_READ1,	71},
{"TC", /* "Tell Error Code",*/				CM_READ1,	72},
{"TD", /* "Tell Dual Encoder",*/			CM_READ2,	73},
{"TE", /* "Tell Error",*/				CM_READ2,	74},
{"TI", /* "Tell Inputs ( Axis 1 only )",*/		CM_READ1,	75},
{"TL", /* "Torque Limit",*/				CM_BOTH3,	76},
{"TM", /* "Time Command",*/				CM_BOTH2,	77},
{"TP", /* "Tell Position",*/				CM_READ2,	78},
{"TS", /* "Tell Switches",*/				CM_READ2,	79},
{"TT", /* "Tell Torque",*/				CM_READ2,	80},
{"TV", /* "Tell Velocity",*/				CM_READ1,	81},
{"TW", /* "Timeout for In Position (MC)",*/		CM_BOTH3,	82},
{"VA", /* "Vector Acceleration",*/			CM_BOTH2,	83},
{"VD", /* "Vector Deceleration",*/			CM_BOTH2,	84},
{"VE", /* "Vector Sequence End",*/			CM_READ1,	85},
{"VR", /* "Vector Speed Ratio",*/			CM_BOTH2,	86},
{"VS", /* "Vector Speed",*/				CM_BOTH2,	87},
{"VT", /* "Vector Time Constant",*/			CM_BOTH2,	88},
{"WC", /* "Wait for Contour Data",*/			CM_COM4,	89},
{"WT", /* "Wait",*/					CM_WRITE1,	90},
{"PF", /* "Position Format - See App. Note"*/		CM_BOTH2,	POSITIONFORMAT},
{"VF", /* "Variable Format - See App. Note"*/		CM_BOTH2,	VARIABLEFORMAT},
{"YP", /* "User Variable YP",*/				CM_BOTH2,	FIRSTUSERVAR},
{"YQ", /* "User Variable YQ",*/				CM_BOTH2,	FIRSTUSERVAR+1},
{"YR", /* "User Variable YR",*/				CM_BOTH2,	FIRSTUSERVAR+2},
{"YS", /* "User Variable YS",*/				CM_BOTH2,	FIRSTUSERVAR+3},
{"YT", /* "User Variable YT",*/				CM_BOTH2,	FIRSTUSERVAR+4},
{"YU", /* "User Variable YU",*/				CM_BOTH2,	FIRSTUSERVAR+5},
{"YV", /* "User Variable YV",*/				CM_BOTH2,	FIRSTUSERVAR+6},
{"YW", /* "User Variable YW",*/				CM_BOTH2,	FIRSTUSERVAR+7},
{"YX", /* "User Variable YX",*/				CM_BOTH2,	FIRSTUSERVAR+8},
{"YY", /* "User Variable YY",*/				CM_BOTH2,	FIRSTUSERVAR+9},
{"IA", /* "Ethernet IP Address",*/			CM_SPEC,	CIA},
{"IH", /* "Open Internet Handle (WO),*/			CM_SPEC,	CIH},
{"IH", /* "Internet Handle - Handle",*/			CM_SPEC,	CIHA},
{"IH", /* "Internet Handle - IP",*/			CM_SPEC,	CIHB},
{"IH", /* "Internet Handle - Port",*/			CM_SPEC,	CIHC},
{"IH", /* "Internet Handle - Protocol",*/		CM_SPEC,	CIHD},
{"IH", /* "Internet Handle - Terminate (WO)*/		CM_SPEC,	CIHE},
{"CH", /* "Connect Handle (WO)",*/			CM_SPEC,	CCH},
{"CH", /* "Connect Handle - Axis",*/			CM_SPEC,	CCHA},
{"CH", /* "Connect Handle - Send",*/			CM_SPEC,	CCHB},
{"CH", /* "Connect Handle - Receive",*/			CM_SPEC,	CCHC},
{"XQ", /* "Execute Program (WO),*/			CM_WRITE1,	CXQ},
{"V0", /* "Array Words 0000 - 0999",*/			CM_BOTH4,	CV0},
{"V1", /* "Array Words 1000 - 1999",*/			CM_BOTH4,	CV1},
{"V2", /* "Array Words 2000 - 2999",*/			CM_BOTH4,	CV2},
{"V3", /* "Array Words 3000 - 3999",*/			CM_BOTH4,	CV3},
{"V4", /* "Array Words 4000 - 4999",*/			CM_BOTH4,	CV4},
{"V5", /* "Array Words 5000 - 5999",*/			CM_BOTH4,	CV5},
{"V6", /* "Array Words 6000 - 6999",*/			CM_BOTH4,	CV6},
{"V7", /* "Array Words 7000 - 7999",*/			CM_BOTH4,	CV7},
{"V8", /* "Array Words 8000 - 8999",*/			CM_BOTH4,	CV8},
{"V9", /* "Array Words 9000 - 9999",*/			CM_BOTH4,	CV9},
{"EO", /* "Echo OFF ( Internal Only )",*/		CM_COM2,	CEO},
{"UV", /* "User Reals  000 -  999",*/			CM_BOTH4,	CUV},
{"D0", /* "Array DWords 0000 - 0999",*/			CM_BOTH4,	CD0},
{"D1", /* "Array DWords 1000 - 1999",*/			CM_BOTH4,	CD1},
{"D2", /* "Array DWords 2000 - 2999",*/			CM_BOTH4,	CD2},
{"D3", /* "Array DWords 3000 - 3999",*/			CM_BOTH4,	CD3},
{"D4", /* "Array DWords 4000 - 4999",*/			CM_BOTH4,	CD4},
{"D5", /* "Array DWords 5000 - 5999",*/			CM_BOTH4,	CD5},
{"D6", /* "Array DWords 6000 - 6999",*/			CM_BOTH4,	CD6},
{"D7", /* "Array DWords 7000 - 7999",*/			CM_BOTH4,	CD7},
{"R0", /* "Array Reals 0000 - 0999",*/			CM_BOTH4,	CR0},
{"R1", /* "Array Reals 1000 - 1999",*/			CM_BOTH4,	CR1},
{"R2", /* "Array Reals 2000 - 2999",*/			CM_BOTH4,	CR2},
{"R3", /* "Array Reals 3000 - 3999",*/			CM_BOTH4,	CR3},
{"R4", /* "Array Reals 4000 - 4999",*/			CM_BOTH4,	CR4},
{"R5", /* "Array Reals 5000 - 5999",*/			CM_BOTH4,	CR5},
{"R6", /* "Array Reals 6000 - 6999",*/			CM_BOTH4,	CR6},
{"R7", /* "Array Reals 7000 - 7999",*/			CM_BOTH4,	CR7},
{"AW", /* "V,D,R Array Access Control", */		CM_SPEC,	CAW},
{"TI", /* "Tell Inputs (Any Axis)*/			CM_READ2,	CTIA},
{"WO", /* "Write Specific Output Bit*/			CM_SPEC,	CWO},
{"HS", /* "TCP Only - Handle Swap"*/			CM_SPEC,	0},
{"HS", /* "TCP Only - Handle Swap"*/			CM_SPEC,	0},
{"HS", /* "TCP Only - Handle Swap"*/			CM_SPEC,	0},
{"HS", /* "TCP Only - Handle Swap"*/			CM_SPEC,	0},

{"TX", /* "Text Strings to Read data,*/			CM_SPEC,	CRTXT},
{"TX", /* "TXTn Data as Integer,*/			CM_SPEC,	CRTXTI},
{"TX", /* "TXTn Data as Real,*/				CM_SPEC,	CRTXTR},
{"TX", /* "TXTn Data as String,*/			CM_SPEC,	CTXTS0},
{"TX", /* "TXTn Data as String,*/			CM_SPEC,	CTXTS1},
{"TX", /* "TXTn Data as String,*/			CM_SPEC,	CTXTS2},
{"TX", /* "TXTn Data as String,*/			CM_SPEC,	CTXTS3},
{"TX", /* "TXTn Data as String,*/			CM_SPEC,	CTXTS4},
{"TX", /* "TXTn Data as String,*/			CM_SPEC,	CTXTS5},
{"TX", /* "TXTn Data as String,*/			CM_SPEC,	CTXTS6},
{"TX", /* "Text Strings to Write data,*/		CM_SPEC,	CWTXT},
{"TX", /* "Send WTXTn Data as Integer,*/		CM_SPEC,	CWTXTI},
{"TX", /* "Send WTXTn Data as Real,*/			CM_SPEC,	CWTXTR},
{"TX", /* "Send WTXTn Data as String,*/			CM_SPEC,	CWTXTS},

{"US", /* "Device Control Command Response",*/		CM_STRING,	CUSRRSP},
	};

//////////////////////////////////////////////////////////////////////////
//
// YaskawaUnivSMC Driver
//

// Instantiator

#if DRIVER_ID == 0x400F

INSTANTIATE(CYaskawaUnivSMCDriver);

#endif

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CYaskawaUnivSMCDriver::CYaskawaUnivSMCDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_fUseSingleArrayAccess = FALSE;
	}

// Destructor

CYaskawaUnivSMCDriver::~CYaskawaUnivSMCDriver(void)
{
	}

// Configuration

void MCALL CYaskawaUnivSMCDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CYaskawaUnivSMCDriver::CheckConfig(CSerialConfig &Config)
{
	Config.m_uFlags |= flagHonorCTS;
	}
	
// Management

void MCALL CYaskawaUnivSMCDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CYaskawaUnivSMCDriver::Open(void)
{
	m_pCL = (YUNIVSMCCmdDef FAR * )CL;
	}

// Device

CCODE MCALL CYaskawaUnivSMCDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);
			m_pCtx->m_fEchoOffSent = FALSE;

			memset( m_pCtx->m_DCName,   0, sizeof(m_pCtx->m_DCName) );

			memset( m_pCtx->m_bRead,    0, sizeof(m_pCtx->m_bRead) );

			memset( m_pCtx->m_bWrite,   0, sizeof(m_pCtx->m_bWrite) );

			memset(m_pCtx->m_dUserResponse, 0, SZUSERRESPB);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskawaUnivSMCDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskawaUnivSMCDriver::Ping(void)
{
	return SendEcho() ? 1 : CCODE_ERROR;
	}

CCODE MCALL CYaskawaUnivSMCDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table >= CCURHAN && Addr.a.m_Table <= CHANSWP ) return uCount;

	UINT uOffset = Addr.a.m_Table == addrNamed ? Addr.a.m_Offset : Addr.a.m_Table;

	SetpItem( uOffset );

	if( m_pItem == NULL ) {

		return CCODE_ERROR | CCODE_HARD;
		}

	if( ReadNoTransmit(Addr.a.m_Offset, pData, uCount) ) {

		return uCount;
		}

	if( IsTextFunction(Addr.a.m_Table) ) {

		return RWTextFunction( Addr, pData, &uCount ) ? uCount : CCODE_ERROR;
		}

	if( !SendEcho() ) return CCODE_ERROR;

	if( Read( Addr, &uCount ) ) {

		if( Transact(TRUE) ) {

			if( GetResponse( pData, Addr.a.m_Type, &uCount ) ) {

				return IsMultipleArrayVar() ? uCount : 1;
				}
			}
		}

	m_Error = (uOffset << 16) + LOWORD(m_Error)+1;

	return CCODE_ERROR;
	}

CCODE MCALL CYaskawaUnivSMCDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table >= CCURHAN && Addr.a.m_Table <= CHANSWP ) return uCount;

	UINT uOffset = Addr.a.m_Table == addrNamed ? Addr.a.m_Offset : Addr.a.m_Table;

	SetpItem( uOffset );

//**/	AfxTrace3("\r\n*****WRITE***** %d %d %d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( m_pItem == NULL ) {

		return CCODE_ERROR | CCODE_HARD;
		}

	if( WriteNoTransmit( Addr.a.m_Offset, pData, uCount ) ) {

		return uCount;
		}

	if( !SendEcho() ) return CCODE_ERROR;

	if( IsTextFunction(Addr.a.m_Table) ) {

		WriteTextFunction(Addr, pData, &uCount);
		return uCount;
		}

	if( WriteSMC(Addr, pData, &uCount) ) {

		if( Transact(FALSE) ) {

			return IsMultipleArrayVar() ? uCount : 1;
			}
		}

	return CCODE_ERROR;
	}

UINT CYaskawaUnivSMCDriver::DevCtrl(void * pContext, UINT uFunc, PCTXT Value)
{
	if( uFunc >= NAMESTART && strlen(Value) <= NAMESIZE ) {

		UINT uPos = SetDCPointer(uFunc);

		memset( &(m_pCtx->m_DCName[uPos]), 0, NAMESIZE );

		if( strlen(Value) ) {

			for( UINT i = 0; i < strlen(Value); i++ ) {

				((CContext *)pContext)->m_DCName[uPos + i] = Value[i];
				}
			}

		return 1;
		}

	if( uFunc == 1 || uFunc == 2 ) {

		TransactUser(uFunc, Value);

		return 1;
		}

	return 0;
	}

// PRIVATE METHODS

BOOL CYaskawaUnivSMCDriver::SendEcho(void)
{
	if( m_pCtx->m_fEchoOffSent ) return TRUE;

	SetpItem( CEO );

	StartFrame();

	AddByte( 'E' );
	AddByte( 'O' );

	if( !Transact(FALSE) ) {

		return FALSE;
		}

	m_pCtx->m_fEchoOffSent = TRUE;

	return TRUE;

//	return SendFormat();
	}

BOOL CYaskawaUnivSMCDriver::SendFormat(void)
{
	return SendPFormat() && SendVFormat();
	}

BOOL CYaskawaUnivSMCDriver::SendPFormat(void)
{
	SetpItem( POSITIONFORMAT );

	StartFrame();

	AddByte( 'P' );
	AddByte( 'F' );
	AddByte( '1' );
	AddByte( '0' );
	AddByte( '.' );
	AddByte( '4' );

	return Transact(FALSE);
	}

BOOL CYaskawaUnivSMCDriver::SendVFormat(void)
{
	SetpItem( VARIABLEFORMAT );

	StartFrame();

	AddByte( 'V' );
	AddByte( 'F' );
	AddByte( '1' );
	AddByte( '0' );
	AddByte( '.' );
	AddByte( '4' );

	return Transact(FALSE);
	}

// Opcode Handlers

BOOL CYaskawaUnivSMCDriver::Read(AREF Addr, UINT * pCount)
{
	UINT uOffset = Addr.a.m_Offset;

	char cc[6] = {0};

	StartFrame();

	if( IsMultipleArrayVar() ) {

		UINT uCount = *pCount;

		MakeMin( uCount, 16 );

		if( uOffset + uCount > 999 ) {

			uCount = 1000 - uOffset;
			}

		char cc[32] = {0};

		SPrintf(cc, "QU %s[],%3.3d,%3.3d,1",
			m_pItem->sName,
			uOffset,
			uOffset + uCount - 1
			);

		AddParam(cc);

		*pCount = uCount;

		return TRUE;
		}

	else {
		AddCommand(Addr.a.m_Offset);

		if( NameChangeRequired(uOffset) ) { // UV Name Change

			AddByte('=');
			AddByte('?');

			return TRUE;
			}
		}

	switch( m_pItem->Type ) {

		case CM_READ1:
			break;

		case CM_READ2:

			if( m_pItem->uID == CTIA ) {

				SPrintf( cc, " %1.1d00", Addr.a.m_Offset );
				AddParam(cc);
				}

			else {
				SetOneAxisLetter( uOffset );
				}
			break;

		case CM_BOTH0:
		case CM_BOTH1:
		case CM_BOTH2:
			AddByte('?');
			break;

		case CM_BOTH3:
			SetOneAxisLetter( uOffset );
			AddByte('=');
			AddByte('?');
			break;

		case CM_BOTH4:
			IsUserVarArray() ? SPrintf(cc, "[%3.3d]=?", uOffset) : SPrintf(cc, "%3.3d=?", uOffset);
			AddParam(cc);
			break;

		case CM_SPEC:

			if( m_pItem->uID != CIA ) {

				return FALSE;
				}

			AddByte(' ');
			AddByte('?');
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CYaskawaUnivSMCDriver::WriteSMC(AREF Addr, PDWORD pData, UINT * pCount)
{
	char cIn[64]     = {0};
	char cReturn[32] = {0};

	UINT uOffset = Addr.a.m_Offset;
 
	StartFrame();

	DWORD d = *pData;

	if( IsUserVarArray() ) {

		if( !m_fUseSingleArrayAccess ) {

			*pCount = PutArrayData(Addr, *pCount, pData);

			return TRUE;
			}

		AddCommand(Addr.a.m_Offset); // Add Vn, Dn, or Rn

		SPrintf(cReturn, "[%3.3d]=", uOffset); // Make Array Offset

		AddParam(cReturn); // Add Array Offset

		if( m_pItem->uID >= CV0 && m_pItem->uID <= CV9 ) { // Array Words

			SPrintf(cReturn, "%4.4d", LOWORD(d));
			}

		else {
			if( m_pItem->uID >= CD0 && m_pItem->uID <= CD7 ) { // Array Doubles

				SPrintf(cReturn, "%8.8d", d);
				}

			else {
				if( d == 0x80000000) {

					d = 0;
					}

				SPrintf( cIn, "%f", PassFloat(d) ); // Array Reals
				AdjustFormat( cIn, cReturn );
				}
			}

		AddParam(cReturn);

		return TRUE;
		}

	else {
		AddCommand(Addr.a.m_Offset);
		}

	switch( m_pItem->uID ) {

		case	CAB:	// CM 0
				AddByte('0');
				break;

		case	CAB1:	// CM 1
				AddByte('1');
				break;

		case	CAC:	// CM x,y,z,w
		case	CAD:
		case	CAE:
		case	CAP:
		case	CAR:
		case	CCD:
		case	CCE:
		case	CDC:
		case	CEG:
		case	CEM:
		case	CMF:
		case	CMR:
		case	CTW:
				SetAxisData(uOffset, d, 0x10000000 );
				break;

		case	CDV:
		case	COE:
				SetAxisData(uOffset, d ? 1 : 0, 0x1 );
				break;

		case	CAF:	// CM x,y,z,w ( data = 0 )
				SetAxisData(uOffset, 0, 1 );
				break;

		case	CAF1:	// CM x,y,z,w ( data = 1 )
				SetAxisData(uOffset, 1, 1 );
				break;

		case	CAI:	// CM n
		case	CAT:
		case	CCB:
		case	CDT:
		case	CEC:
		case	CEQ:
		case	CHX:
		case	CMM:
		case	COP:
		case	CSB:
		case	CTM:
		case	CWT:
				SPrintf( cReturn, "%ld", d );
				AddParam( cReturn );
				break;
				
		case	CAL:	// CM XYZW from Offset
				SetOneAxisLetter( uOffset );
				break;

		case	CAM:	// CM XYZW from Data
		case	CAS:
		case	CBG:
		case	CCM:
		case	CEA:
		case	CFE:
		case	CFI:
		case	CHM:
		case	CMC:
		case	CMO:
		case	CSH:
		case	CST:
				SetAxisLetters( d );
				break;

		case	CBL:	// CM x,y,z,w data = float
		case	CDE:
		case	CDP:
		case	CER:
		case	CFA:
		case	CFL:
		case	CFV:
		case	CIL:
		case	CIP:
		case	CIT:
		case	CJG:
		case	CKD:
		case	CKI:
		case	CKP:
		case	COF:
		case	CPA:
		case	CPR:
		case	CSP:
		case	CTL:
				if( d == 0x80000000) {

					d = 0;
					}

				SetAxisPosition(uOffset);
				SPrintf( cIn, "%f", PassFloat(d) );
				AdjustFormat( cIn, cReturn );
				AddParam( cReturn );
				break;
		
		case	CAO:	// CM float
		case	CGR:
		case	CPF:
		case	CVF:
				if( d == 0x80000000) {

					d = 0;
					}

				SPrintf( cIn, " %f", PassFloat(d) );
				AdjustFormat( cIn, cReturn );
				AddParam( cReturn );
				break;

		case	CUV:	// UVnnn
				if( !NameChangeRequired(uOffset) ) {
					SPrintf( cIn, "%3.3d", uOffset );
					AddParam( cIn );
					}

				AddByte('=');

				if( d == 0x80000000 ) {

					d = 0;
					}

				SPrintf( cIn, "%f", PassFloat(d) );
				AdjustFormat( cIn, cReturn );

				AddParam( cReturn );
				break;

		case	COB:	// CM bit number,data
				SPrintf( cReturn, " %1.1d,", uOffset );
				AddParam( cReturn );
				AddByte( (d) ? '1' : '0');
				break;

		case	CEB:	// CM0/1
				AddByte( d ? '1' : '0' );
				break;

		case	CBN:	// No Parameter
		case	CBP:
		case	CBV:
		case	CRS:
		case	CXQ:
				break;

		case	CIA:	// IA jjj.kkk.lll.mmm
				IPToString( d );
				SPrintf( cReturn, m_cFormat );
				AddParam( cReturn );
				break;

		case	CIH:
				m_wPtr = 0;
				IPToString(m_pCtx->m_IHIP);
				SPrintf( cReturn, "IH%c=%s <%d> %d",
					SetHandleLetter(m_pCtx->m_IHHandle, 6),
					m_cFormat,
					m_pCtx->m_IHPort,
					(m_pCtx->m_IHProtocol > 2) ? 0 : m_pCtx->m_IHProtocol
					);
				AddParam( cReturn );
				break;

		case	CIHE:
				m_wPtr = 0;
				SPrintf( cReturn, "IH%c= -%c",
					SetHandleLetter(m_pCtx->m_IHHandle, 6),
					(*pData & 1) ? '1' : '2'
					);
				AddParam(cReturn);
				break;

		case	CCH:
				m_wPtr = 0;
				SPrintf( cReturn, "CH%c= %c,%c",
					SetHandleLetter(m_pCtx->m_CHAxis, 8),
					SetHandleLetter(m_pCtx->m_CHSend, 6),
					SetHandleLetter(m_pCtx->m_CHReceive, 6)
					);
				AddParam(cReturn);
				break;

		case	CWO:
				m_wPtr = 0;
				SPrintf( cReturn, "%cB%d",
					*pData ? 'S' : 'C',
					Addr.a.m_Offset
					);
				AddParam(cReturn);
				break;				
		}

	return TRUE;
	}

// Text Function Execution
BOOL CYaskawaUnivSMCDriver::RWTextFunction(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT u;

	PBYTE p = SetCachePosition(Addr.a.m_Offset, TRUE, &u);

	BOOL fInvalidStringData = !p || *p < ' ' || *p > 'z';

	if( !fInvalidStringData ) {

		if( PutTxString( p, u ) ) AddByte('?');	// '=' detected

		if( !Transact(TRUE) ) return FALSE;
		}

	switch( Addr.a.m_Table ) {

		case CRTXTI:
			*pData  = fInvalidStringData ? 0 : GetRoundedData();
			*pCount = 1;
			return TRUE;

		case CRTXTR:
			*pData  = fInvalidStringData ? 0 : GetReal(0);
			*pCount = 1;
			return TRUE;

		case CTXTS0:
		case CTXTS1:
		case CTXTS2:
		case CTXTS3:
		case CTXTS4:
		case CTXTS5:
		case CTXTS6:
			UINT i;
			UINT uShift;
			BOOL fDone;
			DWORD dResult;

			i       = 0;
			u       = 0;
			fDone   = FALSE;

			while( !fDone ) {

				if( !fInvalidStringData ) {

					uShift  = 32;

					dResult = 0;

					while( uShift ) {

						uShift -= 8;

						BYTE b = m_bRx[i];

						if( b < ' ' || b > 'z' ) fDone = TRUE;

						dResult += fDone ? 0 : (DWORD(b) << uShift);

						if( !fDone ) i++;
						}
					}

				else {
					dResult = 0x3f3f3f00; // "???"
					fDone   = TRUE;
					}

				pData[u++] = dResult;

				if( u >= *pCount ) return TRUE;
				}

			if( fDone ) {

				while( u < *pCount ) pData[u++] = 0;
				}

			break;
		}

	return TRUE;
	}

BOOL CYaskawaUnivSMCDriver::WriteTextFunction(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT u;
	PBYTE p = NULL;

	switch( m_pItem->uID ) {

		case CWTXTI:
		case CWTXTR:
		case CWTXTS:
			p = SetCachePosition(Addr.a.m_Offset, FALSE, &u);
			break;

		case CRTXTI:
		case CRTXTR:
		case CTXTS0:
		case CTXTS1:
		case CTXTS2:
		case CTXTS3:
		case CTXTS4:
		case CTXTS5:
		case CTXTS6:
			p = SetCachePosition(Addr.a.m_Offset, TRUE, &u);
			break;
		}

	if( !p || *p < ' ' || *p > 'z' ) return TRUE;

	PutTxString( p, u );

	char cValue[16] = {0};

	DWORD d = pData[0];

	switch(m_pItem->uID) {

		case CRTXTI:
		case CWTXTI:
			SPrintf( cValue, "%d", d );
			AddParam( cValue );
			*pCount = 1;
			break;

		case CRTXTR:
		case CWTXTR:
			if( d == 0x80000000) d = 0;
			SPrintf( cValue, "%f", PassFloat(d) );
			AddParam( cValue );
			*pCount = 1;
			break;

		case CTXTS0:
		case CTXTS1:
		case CTXTS2:
		case CTXTS3:
		case CTXTS4:
		case CTXTS5:
		case CTXTS6:
		case CWTXTS:
			UINT uDone;

			uDone = FALSE;

			for( u = 0; u < *pCount; u++ ) {

				uDone = UnpackDWORD( cValue, pData[u], CACHESEMIC );

				AddParam( cValue );

				if( uDone == CACHEDONE ) break;
				}

			break;
		}

	return Transact(FALSE);
	}

BOOL CYaskawaUnivSMCDriver::PutTxString(PBYTE pCache, UINT uMaxSize)
{
	m_wPtr = 0;

	while( m_wPtr < uMaxSize ) {

		BYTE b = pCache[m_wPtr];

		UINT u = EndofCacheData(b);

		if( u > CACHEEQUAL ) return FALSE;

		m_bTx[m_wPtr++] = b;

		if( u == CACHEEQUAL ) return TRUE;
		}

	m_wPtr = uMaxSize; // string uses all remaining bytes

	return FALSE; // no '=' sign, don't add '?' to Read command
	}

BOOL CYaskawaUnivSMCDriver::IsTextFunction(UINT uTable)
{
	switch( uTable ) {

		case CRTXTI:
		case CRTXTR:
		case CTXTS0:
		case CTXTS1:
		case CTXTS2:
		case CTXTS3:
		case CTXTS4:
		case CTXTS5:
		case CTXTS6:
		case CWTXTI:
		case CWTXTR:
		case CWTXTS:
			return TRUE;
		}

	return FALSE;
	}

// Header Building

void CYaskawaUnivSMCDriver::StartFrame(void)
{
	m_wPtr = 0;
	}

void CYaskawaUnivSMCDriver::EndFrame(void)
{
	if( !(IsMultipleArrayVar() && m_bRx[m_wPtr-1] == '\\') ) {

		AddByte( CR );
		}
	}

void CYaskawaUnivSMCDriver::AddByte(BYTE bData)
{
	m_bTx[m_wPtr++] = bData;
	}

void CYaskawaUnivSMCDriver::AddData(PDWORD pData, WORD wCount)
{
	for( UINT i = 0; i < wCount; i++ ) {

		m_bTx[m_wPtr++] = LOBYTE(LOWORD(*(pData+i)));

		m_bTx[m_wPtr++] = HIBYTE(LOWORD(*(pData+i)));
		}
	}

void CYaskawaUnivSMCDriver::AddCommand(UINT uOffset)
{
	if( !NameChangeRequired(uOffset) ) {
	
		PutText( m_pItem->sName );
		}

	else {
		PutUserName(uOffset);
		}

	return;
	}

void CYaskawaUnivSMCDriver::AddParam( char * cParam )
{
	for( UINT i = 0; i < strlen(cParam); i++ ) {

		AddByte(cParam[i]);
		}
	}
	
void CYaskawaUnivSMCDriver::PutText(LPCTXT pCmd)
{
	for( UINT i=0; pCmd[i]; i++ ) {

		AddByte( BYTE(pCmd[i]) );
		}
	}

// Transport Layer

BOOL CYaskawaUnivSMCDriver::Transact(BOOL fWantReply)
{
	Send();

	return GetReply(fWantReply);
	}

void CYaskawaUnivSMCDriver::TransactUser(UINT uFunc, PCTXT Value)
{
	m_wPtr = strlen(Value);

	if( !m_wPtr || m_wPtr > sizeof(m_bTx) - 2 ) return;

	memset( m_bTx, 0, sizeof(m_bTx) );
	memset( m_bRx, 0, sizeof(m_bRx) );

	memcpy( m_bTx, Value, m_wPtr );

	if( m_bTx[m_wPtr-1] == CR ) m_wPtr--; // Let Send() add CR

	Send();

	BYTE fResp = uFunc == 1;

	SetTimer( fResp ? 1000 : 100 );

	WORD wData;
	UINT uCount = 0;

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (wData = Get()) == LOWORD(NOTHING) ) {

			Sleep(20);
			continue;
			}

		m_bRx[uCount++] = wData;

//**/		AfxTrace1("<%2.2x>", wData);

		if( wData == ':' ) break;
		}

	if( uCount && fResp ) {

		PDWORD p = PDWORD(&m_bRx[0]);

		for( UINT n = 0; n < SZUSERRESPD; n++, p++ ) m_pCtx->m_dUserResponse[n] = *p;
		}
	}

void CYaskawaUnivSMCDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CYaskawaUnivSMCDriver::GetReply(BOOL fIsRead)
{
	BOOL GotEOL = FALSE;

	UINT uCount = 0;

	SetTimer(fIsRead ? 1000 : 100);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		WORD wData = Get();

		if( wData == LOWORD(NOTHING) ) {			

			Sleep(20);

			continue;
			}

//**/		AfxTrace1("<%2.2x>", wData);

		if( GotEOL && wData != ':' ) { // echo is on

			m_pCtx->m_fEchoOffSent = FALSE;

			uCount = 0;
			}

		m_bRx[uCount++] = wData;

		switch( wData ) {

			case '?':
			case '=':
				uCount = 0;
				break;

			case ':':
				if( GotEOL || !fIsRead ) {

					m_bRx[uCount] = 0;

					return TRUE;
					}
				break;

			case 0xA:

				GotEOL = TRUE;

				break;

			case 0xD:
			case 26:
				if( IsMultipleArrayVar() ) {

					GotEOL = TRUE;
					}
				break;
			}

		if( uCount >= sizeof(m_bRx) ) return FALSE;
		}

	if( GotEOL ) {

		m_bRx[uCount] = 0;

		return TRUE;
		}

	m_pCtx->m_fEchoOffSent = FALSE;	

	return FALSE;
	}

BOOL CYaskawaUnivSMCDriver::GetResponse( PDWORD pData, UINT uType, UINT * pCount )
{
	UINT i = 0;

	switch( m_bRx[0] ) {

		case '?':
		case CR :
			return FALSE;

		case ':':
			if( m_bRx[1] == ':' || m_bRx[1] == '?' || m_bRx[1] == CR ) return FALSE;
			i = 1;
			// fall through
		case ' ':
			while( m_bRx[i] == ' ' ) {

				if( ++i > 5 ) {

					*pData = 0;

					return TRUE;
					}
				}
			break;
		}

	if( uType == addrRealAsReal ) {

		if( !IsMultipleArrayVar() ) {
			
			*pCount = 1;
			}

		for( UINT uCt = 0; uCt < *pCount; uCt++ ) {

			pData[uCt] =  (m_bRx[i] == '$') ? GetNumber(i) : GetReal(i);

			if( uCt+1 < *pCount ) {

				BYTE b;

				while( (b = m_bRx[i++]) != ',' ) {

					if( !b || b == 0xD || b == ':' ) {

						*pCount = uCt;

						return TRUE;
						}
					}
				}
			}

		*pCount = uCt;
		}

	else {
		if( m_pItem->Type == CM_SPEC ) {

			return EthernetReadResponse( pData );
			}

		else {
			if( !IsMultipleArrayVar() ) {

				*pData = GetNumber(i);
				}

			else {
				for( UINT uCt = 0; uCt < *pCount; uCt++ ) {

					pData[uCt] = GetNumber(i);

					if( uCt+1 < *pCount ) {

						BYTE b;

						while( (b = m_bRx[i++]) != ',' ) {

							if( !b || b == 0xD || b == ':' ) {

								*pCount = uCt;

								return TRUE;
								}
							}
						}
					}

				*pCount = uCt;
				}
			}
		}

	return TRUE;
	}

// Port Access

void CYaskawaUnivSMCDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_wPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write( PCBYTE(m_bTx), m_wPtr, FOREVER);
	}

WORD CYaskawaUnivSMCDriver::Get(void)
{
	WORD wData;

	wData = m_pData->Read(TIMEOUT);

	return wData;
	}

// Helpers

void CYaskawaUnivSMCDriver::SetpItem(UINT uID)
{
	m_pItem = m_pCL;

	YUNIVSMCCmdDef * pFail = NULL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == m_pItem->uID ) return;

		if( m_pItem->uID == CTC ) {

			pFail = m_pItem;
			}

		m_pItem++;
		}

	m_pItem = pFail;
	}

void CYaskawaUnivSMCDriver::SetOneAxisLetter( UINT uAxis )
{
	if( uAxis && uAxis <= 8 ) {

		if( uAxis == 1 ) AddByte('X');
		if( uAxis == 2 ) AddByte('Y');
		if( uAxis == 3 ) AddByte('Z');
		if( uAxis == 4 ) AddByte('W');
		if( uAxis == 5 ) AddByte('E');
		if( uAxis == 6 ) AddByte('F');
		if( uAxis == 7 ) AddByte('G');
		if( uAxis == 8 ) AddByte('H');
		}
	}

void CYaskawaUnivSMCDriver::SetAxisLetters( UINT uAxes )
{
	if( uAxes && uAxes <= 8 ) {

		if( uAxes &   1 ) AddByte('X');
		if( uAxes &   2 ) AddByte('Y');
		if( uAxes &   4 ) AddByte('Z');
		if( uAxes &   8 ) AddByte('W');
		if( uAxes &  16 ) AddByte('E');
		if( uAxes &  32 ) AddByte('F');
		if( uAxes &  64 ) AddByte('G');
		if( uAxes & 128 ) AddByte('H');
		}
	}

void CYaskawaUnivSMCDriver::SetAxisPosition(UINT uAxes)
{
	if( uAxes && uAxes <= 8) {

		AddByte(' ');

		switch (uAxes) {

			case 8: AddByte(',');
			case 7: AddByte(',');
			case 6: AddByte(',');
			case 5: AddByte(',');
			case 4: AddByte(',');
			case 3: AddByte(',');
			case 2: AddByte(',');
			}
		}
	}

void CYaskawaUnivSMCDriver::SetAxisData( UINT uAxes, DWORD dData, DWORD uMaxFactor )
{
	SetAxisPosition(uAxes);

	PutNumber( dData);
	}

void CYaskawaUnivSMCDriver::PutNumber(DWORD wNum)
{
	char c[16] = {0};

	if( wNum & 0x80000000 ) {

		AddByte('-');

		wNum = (wNum^0xFFFFFFFF)+1;
		}

	SPrintf( c, "%ld", wNum );

	for( UINT i = 0; i < strlen(c); i++ ) {
		AddByte( c[i] );
		}
	}

DWORD CYaskawaUnivSMCDriver::GetNumber( UINT uPos )
{
	UINT uRad = 10;

	char s1[32];
	char *s2 = s1;

	if( m_bRx[uPos] == ' ' ) uPos++;

	if( m_bRx[uPos] == '$' ) {

		uPos++;

		uRad = 16;
		}

	return ( strtol( (char *)(&m_bRx[uPos]), &s2, uRad ) );
	}

DWORD	CYaskawaUnivSMCDriver::GetRoundedData(void)
{
	DWORD dData = GetNumber(0);

	BOOL fFoundDigit = FALSE;

	for( UINT i = 0; i < sizeof(m_bRx); i++ ) {

		if( m_bRx[i] >= '0' && m_bRx[i] <= '9' ) {

			fFoundDigit = TRUE;
			}

		if( m_bRx[i] == '.' ) {

			if( m_bRx[i+1] > '4' ) {

				return dData & 0x80000000 ? dData - 1 : dData + 1;
				}

			return dData;
			}

		if( fFoundDigit && (m_bRx[i] < '0' || m_bRx[i] > '9') ) {

			return dData;
			}
		}

	return fFoundDigit ? dData : 0;
	}

DWORD	CYaskawaUnivSMCDriver::GetReal(UINT uPos)
{
	return ATOF( (const char * )&m_bRx[uPos] );
	}

UINT CYaskawaUnivSMCDriver::PutArrayData(AREF Addr, UINT uCount, PDWORD pData)
{	
	char c1[64]  = {0};

	char c2[32]  = {0};

	char * p = new char[256];

	UINT uOffset = Addr.a.m_Offset;

	MakeMin( uCount, 16 );

	if( uOffset + uCount > 999 ) {

		uCount = 1000 - uOffset;
		}

	SPrintf( p, "QD %s[],%3.3d,%3.3d\r\n",
		m_pItem->sName,
		uOffset,
		uOffset + uCount - 1
		);

	for( UINT i = 0; i < uCount; i++ ) {

		DWORD d = pData[i];

		UINT uAdjust;

		switch( Addr.a.m_Type ) {

			case addrRealAsReal:

				if( d == 0x80000000) {

					d = 0;
					}

				SPrintf( c1, "%f", PassFloat(d) );

				uAdjust = AdjustFormat( c1, c2 );

				break;

			case addrWordAsWord:

				d &= 0xFFFF;
				// fall through
			default:

				SPrintf( c2, "%d", d );

				uAdjust = strlen( c2 );

				break;
			}

		if( strlen(p) + uAdjust < 250 ) {

			if( i ) {

				strcat( p, ",");
				}

			strcat( p, c2 );
			}

		else break;
		}

	AddParam( p );

	AddByte( '\\' );

	delete p;

	return i;
	}

UINT CYaskawaUnivSMCDriver::AdjustFormat(char * pIn, char * pOut)
{
	char cIn[4]   = {0};
	char cOut[32] = {0};

	UINT uDP     = 0;
	UINT uInPos  = 0;
	UINT uOutPos = 0;

	if( !*pIn ) {

		cIn[0] = '0';
		cIn[1] = '.';
		cIn[2] = '0';

		pIn    = cIn;
		}

	UINT uLen = strlen(pIn);

	if( pIn[0] == '-' ) {

		cOut[uOutPos++] = '-';

		uInPos++;
		}

	for( UINT i = 0; i < uLen; i++ ) {

		if( pIn[uInPos + i] == '.' ) {

			uDP = uInPos + i;

			break;
			}
		}

	if( uDP > 10 ) {

		SPrintf( &cOut[uOutPos], "%d", 0x7FFFFFFF );
		}

	else {
		UINT uIntLen  = uDP - uInPos;
		UINT uFracLen = min(  4, uLen-uDP-1 );

		memcpy(&cOut[uOutPos], &pIn[uInPos], uIntLen);

		uOutPos += uIntLen;

		if( uFracLen ) {

			cOut[uOutPos++] = '.';

			memcpy( &cOut[uOutPos], &pIn[uDP+1], uFracLen );

			uOutPos += uFracLen;
			}
		}

	strcpy( pOut, &cOut[0] );

	return uOutPos;
	}

DWORD CYaskawaUnivSMCDriver::DWordToFloat( DWORD dDWord, DWORD dDP )
{
	return FloatToDWord( (float( dDWord )) / dDP );
	}

DWORD CYaskawaUnivSMCDriver::FloatToDWord( float fFloat )
{
	return DWORD( *((DWORD*) &fFloat));
	}

BOOL	CYaskawaUnivSMCDriver::EthernetReadResponse( PDWORD pData )
{
	if( m_pItem->uID == CIA ) *pData = IPToDWORD();

	return TRUE;
	}

DWORD	CYaskawaUnivSMCDriver::IPToDWORD( void )
{
	DWORD d = 0L;
	UINT uOctad = 0;
	BOOL fIsDec = FALSE;
	UINT uPos = 0;
	UINT u = 0;

	while ( uPos < 6 ) {
		if( m_bRx[uPos++] == ',' ) fIsDec = TRUE;
		}

	uPos = 0;

	while ( m_bRx[uPos] != CR ) {

		u = m_bRx[uPos++];

		if( u == ' ' ) u = m_bRx[uPos++];

		if( fIsDec ) {
			if( u == ',' ) {
				d += uOctad;
				d <<= 8;
				uOctad = 0;
				}

			else if( u >= '0' && u <= '9' )

				uOctad = (uOctad*10) + u - '0';

			else return 0;
			}

		else {
			if( GetHexDigit(&u) )
				d = (d*16) + u;

			else return 0;
			}
		}

	if( fIsDec ) d += uOctad;

	return d;
	}

void	CYaskawaUnivSMCDriver::IPToString( DWORD dIP )
{
	memset( m_cFormat, 0, 32 );

	SPrintf( m_cFormat, "%3d,%3d,%3d,%3d",
		UINT((dIP>>24)&0xFF),
		UINT((dIP>>16)&0xFF),
		UINT((dIP>>8)&0xFF),
		UINT(dIP&0xFF)
		);
	}

BOOL	CYaskawaUnivSMCDriver::GetHexDigit( UINT * p )
{
	if( *p >= '0' && *p <= '9' ) {
		*p -= '0';
		return TRUE;
		}

	if( *p >= 'a' && *p <= 'f' ) {
		*p -= 'W';
		return TRUE;
		}

	if( *p >= 'A' && *p <= 'F' ) {
		*p -= '7';
		return TRUE;
		}

	else return FALSE;
	}

UINT	CYaskawaUnivSMCDriver::SetHandleLetter( UINT uH, UINT uMax )
{
	if( uH > 0 && uH <= uMax ) return '@' + uH;

	if( uH >= '1' && uH <= '8' )
		return (uH < '7' || uMax==8) ? uH + 0x10 : 'A'; // '1'-'8'->'A'-'H'

	if( uH >= 'A' && uH <= 'H' )
		return (uH < 'G' || uMax==8) ? uH : 'A';

	if( uH >= 'a' && uH <= 'h' )
		return (uH < 'g' || uMax==8) ? uH & 0x5F : 'A';

	return 'A';
	}

BOOL	CYaskawaUnivSMCDriver::IsUserVarArray(void)
{
	if( m_pItem->uID >= CV0 && m_pItem->uID <= CV9 ) return TRUE;

	return (m_pItem->uID >= CD0 && m_pItem->uID <= CR7 );
	}

BOOL	CYaskawaUnivSMCDriver::IsMultipleArrayVar(void)
{
	return IsUserVarArray() && !m_fUseSingleArrayAccess;
	}

BOOL	CYaskawaUnivSMCDriver::ReadNoTransmit(UINT uOffset, PDWORD pData, UINT uCount)
{
	if( m_pItem->Type == CM_SPEC ) {

		switch ( m_pItem->uID ) {

			case CAW:
				*pData = m_fUseSingleArrayAccess;
				return TRUE;

			case CIH:
			case CIHE:
			case CCH:
				*pData = 0;
				return TRUE;

			case CHX:
				*pData = 0xFFFFFFFF;
				return TRUE;

			case CIHA:
				*pData = m_pCtx->m_IHHandle;
				return TRUE;

			case CIHB:
				*pData = m_pCtx->m_IHIP;
				return TRUE;

			case CIHC:
				*pData = m_pCtx->m_IHPort;
				return TRUE;

			case CIHD:
				*pData = m_pCtx->m_IHProtocol;
				return TRUE;

			case CCHA:
				*pData = m_pCtx->m_CHAxis;
				return TRUE;

			case CCHB:
				*pData = m_pCtx->m_CHSend;
				return TRUE;

			case CCHC:
				*pData = m_pCtx->m_CHReceive;
				return TRUE;

			case CWO:
				*pData = 0xFF;
				return TRUE;

			case CRTXT:
			case CWTXT:
				FromCache( uOffset, pData, uCount*sizeof(DWORD), m_pItem->uID == CRTXT );
				return TRUE;

			case CWTXTI:
			case CWTXTR:
			case CWTXTS:
				return TRUE;

			default:
				break;
			}
		}

	else if( m_pItem->Type == CM_STRING ) {

		PDWORD p = PDWORD(&m_pCtx->m_dUserResponse[sizeof(DWORD) * (uOffset % SZUSERRESPD)]);

		for( UINT i = 0; i < min(uCount, SZUSERRESPD); i++, p++ ) pData[i] = *p;

		return TRUE;
		}

	else {
		if ( m_pItem->Type > CM_BOTH4 ) { // Write Only items

			if( m_pItem->uID == CXQ ) *pData = 0xFFFFFFFF;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL	CYaskawaUnivSMCDriver::WriteNoTransmit(UINT uOffset, PDWORD pData, UINT uCount)
{
	DWORD dData = *pData;

	if( m_pItem->Type < CM_BOTH0 ) { // Read Only items

		return TRUE;
		}

	else if( m_pItem->Type == CM_SPEC ) {

		switch ( m_pItem->uID ) {

			case CAW:
				m_fUseSingleArrayAccess = (dData != 0);
				return TRUE;

			case CIHA:
				m_pCtx->m_IHHandle = LOWORD(dData);
				return TRUE;

			case CIHB:
				m_pCtx->m_IHIP = dData;
				return TRUE;

			case CIHC:
				m_pCtx->m_IHPort = LOWORD(dData);
				return TRUE;

			case CIHD:
				m_pCtx->m_IHProtocol = LOWORD(dData);
				return TRUE;

			case CCHA:
				m_pCtx->m_CHAxis = LOWORD(dData);
				return TRUE;

			case CCHB:
				m_pCtx->m_CHSend = LOWORD(dData);
				return TRUE;

			case CCHC:
				m_pCtx->m_CHReceive = LOWORD(dData);
				return TRUE;

			case CRTXT:
			case CWTXT:
				ToCache(uOffset, pData, uCount*sizeof(DWORD), m_pItem->uID == CRTXT);
				return TRUE;

			default:
				break;
			}
		}

	else {
		if( m_pItem->Type == CM_STRING ) {

			memset(m_pCtx->m_dUserResponse, 0, SZUSERRESPB);

			return TRUE;
			}
		}

	return FALSE;
	}

void	CYaskawaUnivSMCDriver::PutUserName(UINT uOffset)
{
	UINT i = 0;

	UINT uPtr = SetDCPointer(uOffset);

	while( m_pCtx->m_DCName[uPtr + i] && i < NAMESIZE ) {

		AddByte(m_pCtx->m_DCName[uPtr + i]);

		i++;
		}
	}

BOOL	CYaskawaUnivSMCDriver::NameChangeRequired(UINT uOffset)
{
	return m_pItem->uID == CUV && uOffset >= NAMESTART && (m_pCtx->m_DCName[SetDCPointer(uOffset)] != 0);
	}

UINT	CYaskawaUnivSMCDriver::SetDCPointer(UINT uOffset)
{
	return NAMESIZE * ( uOffset - NAMESTART );
	}

PBYTE	CYaskawaUnivSMCDriver::SetCachePosition(UINT uItemNumber, BOOL fReadCache, UINT * pRemainingSpace)
{
	UINT i = 0;
	UINT c = 0;

	PBYTE p = fReadCache ? m_pCtx->m_bRead : m_pCtx->m_bWrite;

	while( i < CACHESIZE - 1 ) {

		if( c < uItemNumber ) {

			switch( EndofCacheData(p[i]) ) {

				case CACHECONT:
					break;

				case CACHEEQUAL:
				case CACHESEMIC:
					c++;
					break;

				case CACHEDONE:
				default:
					*pRemainingSpace = CACHESIZE - i - 1;
					return p + i;
				}

			i++;
			}

		else {
			*pRemainingSpace = CACHESIZE - i - 1;
			return p + i;
			}
		}

	return NULL;
	}

BOOL	CYaskawaUnivSMCDriver::FromCache(UINT uItemNumber, PDWORD pData, UINT uCount, BOOL fReadCache)
{
	UINT  uRemainingSpace;

	PBYTE p = SetCachePosition(uItemNumber, fReadCache, &uRemainingSpace);

	if( !p ) {

		if( uCount ) memset( pData, 0, uCount );

		return FALSE;
		}

	UINT i      = 0;
	UINT uTotal = 0;
	UINT uCt    = 0;
	BOOL fDone  = FALSE;

	while( uTotal < uCount ) {

		DWORD dResult = 0;

		if( !fDone && i < uRemainingSpace ) {

			UINT uShift = 32;

			while( uShift ) {

				uShift -= 8;

				BYTE b = p[i];

				if( EndofCacheData(b) != CACHEDONE ) {

					dResult += (DWORD(b) << uShift);
					i++;
					}

				else fDone = TRUE;
				}
			}

		pData[uCt++] = dResult;

		uTotal += 4;
		}

	return TRUE;
	}

BOOL	CYaskawaUnivSMCDriver::ToCache(UINT uItemNumber, PDWORD pData, UINT uCount, BOOL fReadCache)
{
	UINT uRemainingSpace = 0;

	PBYTE p = SetCachePosition(uItemNumber, fReadCache, &uRemainingSpace);

	if( !p || uRemainingSpace < uCount ) return FALSE;

	DWORD dData  = 0;
	UINT  uPtr   = 0;
	UINT  i      = 0;
	UINT  uDone  = 0;
	char  c[5];

	while( i < uCount ) {

		uDone = UnpackDWORD( c, pData[uPtr], CACHEDONE );

		memcpy( &p[i], c, 4 );

		if( uDone == CACHEDONE ) {

			return TRUE;
			}

		i += 4;

		uPtr++;
		}

	return TRUE;
	}

UINT	CYaskawaUnivSMCDriver::EndofCacheData(BYTE bData)
{
	if( bData == '=' ) return CACHEEQUAL;

	if( bData == ';' ) return CACHESEMIC;

	return bData  < ' ' || bData  > 'z' ? CACHEDONE : CACHECONT;
	}

BOOL	CYaskawaUnivSMCDriver::UnpackDWORD( char * c, DWORD dData, UINT uDontInclude )
{
	UINT uShift = 24;

	memset( c, 0, 5 );

	for( UINT i = 0; i < 4; i++ ) {

		BYTE b = dData >> uShift;

		if( EndofCacheData(b) < uDontInclude ) c[i] = b;

		else return TRUE;

		uShift -= 8;
		}

	return FALSE;
	}

// End of File
