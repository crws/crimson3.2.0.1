/////////////////////////////////////////////////////////////////////////
//
// Constants
//
 
#define	FRAME_TIMEOUT	500

#define	RX_ERROR	0x00
#define	RX_FRAME	0x01
#define	RX_ACK		0x02
#define	RX_NAK		0x03

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Base Drivers
//

class CGem80MasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CGem80MasterDriver(void);

		// Destructor
		~CGem80MasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Data
		struct CBaseCtx
		{
			BYTE m_bDrop;
			BYTE m_fToggle;
			};

	protected:
		// Data Members
		BYTE m_bTerm;
		BYTE m_bRxBuff[400];
		BYTE m_bTxBuff[400];
		UINT m_uPtr;
		UINT m_uRxSize;
		UINT m_uTxSize;
		CBaseCtx * m_pBase;
			
		// Frame Building
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);

		// Overridables
		virtual BOOL Start(void);
		virtual BOOL Transact(BOOL fWrite);
		virtual void AddCount(UINT &uCount, UINT uType);

		// Read Handlers
		CCODE DoWordRead(PDWORD pData, UINT uCount, UINT uType);
		CCODE DoLongRead(PDWORD pData, UINT uCount, UINT uType);

		// Write Handlers
		CCODE DoWordWrite(PDWORD pData, UINT uCount, UINT uType);
		CCODE DoLongWrite(PDWORD pData, UINT uCount, UINT uType);

	};

class CGem80SlaveDriver : public CSlaveDriver
{
	public:
		// Constructor
		CGem80SlaveDriver(void);

		// Destructor
		~CGem80SlaveDriver(void);

		// Entry Points
		DEFMETH(void) Service (void);
		

	protected:

		BYTE m_bTerm;
		BYTE m_bDrop;
		BYTE m_bRxBuff[300];
		BYTE m_bTxBuff[300];
		UINT m_uPtr;
		UINT m_uRxSize;
		UINT m_uTxSize;
		
		void Send(BYTE bData);
	};

// End of File
