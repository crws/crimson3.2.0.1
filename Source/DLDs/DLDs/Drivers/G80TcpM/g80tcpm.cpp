
#include "intern.hpp"

#include "g80tcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 TCP/IP Master Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CGem80MasterTCPDriver);

// Constructor

CGem80MasterTCPDriver::CGem80MasterTCPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CGem80MasterTCPDriver::~CGem80MasterTCPDriver(void)
{
	}

// Configuration

void MCALL CGem80MasterTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CGem80MasterTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CGem80MasterTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CGem80MasterTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CGem80MasterTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Socket Management

BOOL CGem80MasterTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CGem80MasterTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CGem80MasterTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Implementation

BOOL CGem80MasterTCPDriver::Start(void)
{
	if( !OpenSocket() ) {

		return FALSE;
		}

	if( CGem80MasterDriver::Start() ) {

		AddWord(0x0000);

		AddByte(0x00);

		return TRUE;
		}

	return FALSE;
	}

// Transport Layer

BOOL CGem80MasterTCPDriver::Send(void)
{
	UINT uSize = m_uPtr;

	m_bTxBuff[0] = uSize - 2;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CGem80MasterTCPDriver::RecvFrame(BOOL fWrite)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {
			
			uPtr += uSize;

			if( uPtr >= 2 ) {

				UINT uTotal = IntelToHost(PWORD(m_bRxBuff)[0]) + 2;

				if( uPtr >= uTotal ) {

					if( uPtr == uTotal ) {

						if( fWrite ) {

							return TRUE;
							}
					
						if( uTotal == UINT(2 + m_bTxBuff[7] * 2) ) {

							memcpy(m_bRxBuff, m_bRxBuff + 2, uTotal - 2);
						
							return TRUE;
							}
						}

					return FALSE;
					}
				}

			continue;
			}

		if( !CheckSocket() ) {
			
			return FALSE;
			} 

		Sleep(10);
		}
	
	return FALSE;
	}

BOOL CGem80MasterTCPDriver::Transact(BOOL fWrite)
{
	if( Send() && RecvFrame(fWrite) ) {

		return TRUE;
		}

	CloseSocket(TRUE);

	return FALSE;
	}

void CGem80MasterTCPDriver::AddCount(UINT &uCount, UINT uType)
{
	if( uType == addrWordAsWord ) {

		MakeMin(uCount, 110);

		AddByte(BYTE(uCount));

		return;
		}

	MakeMin(uCount, 55);

	AddByte(BYTE(uCount * 2));
	
	}

// End of File
