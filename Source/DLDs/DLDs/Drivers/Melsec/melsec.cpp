#include "intern.hpp"

#include "melsec.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Melsec PLC Driver	- Supports Melsec 1 and 4
//

// Instantiator

INSTANTIATE(CMelsecMasterDriver);

// Constructor

CMelsecMasterDriver::CMelsecMasterDriver(void)
{
	m_Ident         = DRIVER_ID;

	m_fCheck	= TRUE;

	}

// Destructor

CMelsecMasterDriver::~CMelsecMasterDriver(void)
{
	}

// Configuration

void MCALL CMelsecMasterDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_fCheck = GetByte(pData);

		}
	}
	
void MCALL CMelsecMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CMelsecMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMelsecMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CMelsecMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_bDrop  = GetByte(pData);

			BYTE bFormat	 = GetByte(pData);

			m_pCtx->m_Format = bFormat;
			   
			m_pBase->m_fCode = bFormat < 2 ? 1 : 0;

			m_pCtx->m_bPc    = GetByte(pData);

			m_pCtx->m_bCpu	 = GetByte(pData);

			m_pCtx->m_bNet   = GetByte(pData);
						
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
		
	}

CCODE MCALL CMelsecMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Implementation

UINT CMelsecMasterDriver::GetMask(UINT Table)
{
	switch ( Table ) {

		case 'L':
		case 'M':
		case 'F':
		case 'S':
		case 'V':
		case 'D':
		case 'R':
		case '6':
		case '7':
		case 'Z':
				return 1000; 

		case 'T':
		case 'C':
		case '8':
		case '9':
		case 'A':
		case 'E':
		case '3':
		

				return 100;
				
		case 'X':
		case 'Y':
		case 'W':
		case '1':
		case '2':
		case '4':
		case '5':
		case 'B':
				return 0x1000;
		}
		
        return 0;

	}
	
	
BOOL CMelsecMasterDriver::IsHex(UINT Table)
{
	switch ( Table ) {
	
		case 'X':
		case 'Y':
		case 'W':
		case '1':
		case '2':
		case '4':
		case '5':
		case 'B':
		 
			return TRUE;
		}

        return FALSE;

	}
 
// Data Link Layer Code

BOOL CMelsecMasterDriver::SendReq(void)
{
	if ( !IsAscii() ) {

		m_bTxBuff[2] = m_uPtr - 4 - m_DLE;

		m_bCheck += m_bTxBuff[2];

		m_bTxBuff[m_uPtr++] = DLE;

		m_bTxBuff[m_uPtr++] = ETX;
		}
	
	if ( m_fCheck ) {
	
		m_bTxBuff[m_uPtr++] = m_pHex[m_bCheck / 16];
		
		m_bTxBuff[m_uPtr++] = m_pHex[m_bCheck % 16]; 
		}
	
	if ( m_pCtx->m_Format == 1 ) {
	
		AddByte(CR);
	
		AddByte(LF);
		} 
	
	Send();
		
	return TRUE;
	} 
	
void CMelsecMasterDriver::Send( )
{
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_uPtr; u++ ) {

		AfxTrace("%2.2x ", m_bTxBuff[u]);
		}
*/	}

void CMelsecMasterDriver::SendAck(void)
{
	Start();
	
	AddByte(ACK);

	AddByte(m_pHex[m_pCtx->m_bDrop / 16]);
	AddByte(m_pHex[m_pCtx->m_bDrop % 16]);

	AddByte('F');
	AddByte('F');   
	
	if ( m_pCtx->m_Format == 1 ) {
	
		AddByte(CR);
	
		AddByte(LF); 
		}
	
	Send();
	
	}

void CMelsecMasterDriver::SendNak(void)
{
	Start();
	
	AddByte(NAK);

	AddByte(m_pHex[m_pCtx->m_bDrop / 16]);
	AddByte(m_pHex[m_pCtx->m_bDrop % 16]);

	AddByte('F');
	AddByte('F');   
	
	if ( m_pCtx->m_Format == 1 ) {
	
		AddByte(CR);
	
		AddByte(LF);  
		}
	
	Send();	
		
	}

BOOL CMelsecMasterDriver::RecvFrame(void)
{
	BYTE bCompare = 0, bCheck = 0;

	UINT uPtr = 0;

	UINT uRetry = 0;

	UINT uState = 0; 

	UINT uTimer = 0;

	UINT uByte = 0;

	BOOL fDLE = FALSE;

//	AfxTrace("\nRx : ");	

	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

//		AfxTrace("%2.2x ", uByte);

		switch( uState ) {
		
			case 0:	
				if( uByte == NAK ) {
					uState = 1;
					uPtr   = 0;
					}
				if( uByte == ACK ) {
					uState = 2;
					uPtr   = 0;
					}
				if( uByte == STX ) {
					uState = 3;
					uPtr   = 0;
					bCheck = 0;
					}
				break;
				
			case 1:	
				m_bRxBuff[uPtr++] = uByte;
				
				if( uPtr == 6 ) {

					return FALSE;
					}
				break;
			
			case 2:	
				m_bRxBuff[uPtr++] = uByte;
				
				if( uPtr == 4 ) {

					if( Check() ) {

						return TRUE;
						}
					else {
						return FALSE; 
						}
					}
				break;
				
			case 3: 
				if( IsAscii() ) {
				
					if( uByte == ETX ) {
					
						bCheck += uByte;
					
						uState = 4;
					
						if ( !m_fCheck ) {
					
							SendAck();
						
							return TRUE;
							}
						}
					else {
						bCheck += uByte;
					
						m_bRxBuff[uPtr++] = uByte;
					
						if( uPtr == sizeof(m_bRxBuff) )
							uState = 0;
						}
					}
				else {
					if( fDLE && (uByte == ETX) ) {

						bCheck -= DLE;
					
						uState = 4;

						if ( !m_fCheck ) {
					
							SendAck();
						
							return TRUE;
							}
						}
					else {
						if( fDLE ) {

							fDLE = FALSE;

							break;
							}

						bCheck += uByte;
				     						
						fDLE = uByte == DLE ? TRUE : FALSE;
					     
						m_bRxBuff[uPtr++] = uByte;
						
						if( uPtr == sizeof(m_bRxBuff) )
							uState = 0;
						} 
					} 
				break;
				
			case 4: 
				bCompare = 16 * xval(uByte);
				uState = 5;
				break;
				
			case 5: 
				if ( m_fCheck ) {
				
					bCompare += xval(uByte);
					
					if( Check() && bCompare == bCheck ) {

						SendAck();

						return TRUE;
						}
					else {
						if( uRetry++ < 4 ) {
						
							SendNak();
						
							uState = 0;
							}
						else {
							return FALSE;
							}
						}
					}
					
				else {
					SendAck();
					
					return TRUE;
					} 
				break;
			}
		}

	return FALSE;
	}

BOOL CMelsecMasterDriver::Check(void)
{
	if( !IsAscii() ) {

		return CheckBinary();
		}
	
	if ( m_fCheck ) {
	
		if( m_bRxBuff[0] != m_pHex[m_pCtx->m_bDrop / 16] )
			return FALSE;
	
		if( m_bRxBuff[1] != m_pHex[m_pCtx->m_bDrop % 16] )
			return FALSE;
	
		if( m_bRxBuff[2] != 'F' )
			return FALSE;
	
		if( m_bRxBuff[3] != 'F' )
			return FALSE;
		}
	
	return TRUE;
	}

BOOL CMelsecMasterDriver::CheckBinary(void)
{
	if( m_fCheck ) {

		if( m_bRxBuff[3] != m_pCtx->m_bDrop )
			return FALSE;

		if( m_bRxBuff[10] != 0xFF )
			return FALSE;

		if( m_bRxBuff[11] != 0xFF ) 
			return FALSE;

		if( m_bRxBuff[12] || m_bRxBuff[13] ) {	// Completion Code

			return FALSE;
			}
		}

	UINT uSize = m_bRxBuff[0];

	if( uSize ) {

		memcpy( m_bRxBuff, m_bRxBuff + 12, uSize);

		return TRUE;
		}

	return FALSE;
	}

WORD CMelsecMasterDriver::xval(char cText)
{
	if( cText >= '0' && cText <= '9' )
		return cText - '0';
		
	if( cText >= 'a' && cText <= 'f' )
		return cText - 'a' + 10;

	if( cText >= 'A' && cText <= 'F' )
		return cText - 'A' + 10;
	
	return (WORD)NOTHING;
	}

// Overridables

void CMelsecMasterDriver::AddID(void)
{
       if( IsAscii() ) {
	
		m_bTxBuff[m_uPtr++] = ENQ;

		AddByte(m_pHex[m_pCtx->m_bDrop / 16]);
	
		AddByte(m_pHex[m_pCtx->m_bDrop % 16]);

		return;
		}
	
	m_bTxBuff[m_uPtr++] = DLE;

	m_bTxBuff[m_uPtr++] = STX;

	AddWord(0x0000);

	AddByte(0xF8);

	AddByte(m_pCtx->m_bDrop);

	AddByte(m_pCtx->m_bNet);

	AddByte(m_pCtx->m_bPc);

	AddCpu(); 

	AddWord(0x000);
	}

void CMelsecMasterDriver::AddCommand(UINT uType, UINT uCommand, UINT uCount)
{
	if( IsAscii() ) {
	
		AddByte('F');	// PC No
		
		AddByte('F');	// PC No

		AddByte(LOBYTE(uCommand));
	
		AddByte(HIBYTE(uCommand));

		return;
		}

	AddWord(uCommand);

	AddWord(GetSubCommand(uType));
       	}

void CMelsecMasterDriver::AddAddress(AREF Addr)
{
	UINT uTable = Addr.a.m_Table;

	UINT uOffset = Addr.a.m_Offset;
		
	if( IsAscii() ) {

		AddByte('0');

		AddASCIIPrefix(uTable);
	
		if ( IsHex(uTable) ) {
	        
			AddHex( uOffset, GetMask(uTable) );
		
			return;
			} 
		
		AddDec( uOffset, GetMask(uTable) );

		return;
		}

	AddOffset(uOffset);

	AddBinaryPrefix(uTable);
       	}

void CMelsecMasterDriver::AddOffset(UINT uOffset)
{
	WORD wOffset = uOffset;
	
	AddByte(LOBYTE(wOffset), TRUE);

	AddByte(HIBYTE(wOffset), TRUE);

	AddByte(0x00);
	} 

void CMelsecMasterDriver::AddCount(UINT uCount)
{
	if( IsAscii() ) {

		AddHex(uCount, 0x10 );

		return;
		}

	WORD wCount = uCount;
	
	AddByte(LOBYTE(wCount), TRUE);
	
	AddByte(HIBYTE(wCount), TRUE);
	}

WORD CMelsecMasterDriver::GetCommand(UINT uType, BOOL fWrite)
{
	if( !IsAscii() ) {

		if( fWrite ) {

			return CMD_WRITE;
			}
		
		return CMD_READ;
		}
	
	switch( uType ) {

		case addrBitAsBit:

			return !fWrite ? 0x5242 : 0x5742;
		}

	return !fWrite ? 0x5257 : 0x5757;

	}

BOOL CMelsecMasterDriver::Transact(UINT uTotal) 
{
	if( SendReq() ) {

		return RecvFrame();
		}
	
	return FALSE;
	}

BOOL CMelsecMasterDriver::IsAscii(void)
{
	return (m_pCtx->m_Format < 2);
	}

// Helpers

void CMelsecMasterDriver::AddCpu(void)
{
	switch( m_pCtx->m_bCpu ) {

		case 1: AddWord(cpu1);		return;
		case 2:	AddWord(cpu2);		return;
		case 3: AddWord(cpu3);		return;
		case 4:	AddWord(cpu4);		return;
		case 5:	AddWord(cpuCtrl);	return;
		case 6:	AddWord(cpuStdBy);	return;
		case 7:	AddWord(cpuSysA);	return;
		case 8: AddWord(cpuSysB);	return;
		}

	AddWord(cpuLocal); 
	}

// End of File
