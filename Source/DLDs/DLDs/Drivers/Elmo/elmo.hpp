
//////////////////////////////////////////////////////////////////////////
//
// Elmo Driver
//

#define ELMO_ID 0x403B

// Command Def
struct FAR ElmoCmdDef {
	UINT	uID;
	char 	sName[6];
	BOOL	fHasParam;
	UINT	uType;	
	};

class CElmoDriver : public CMasterDriver
{
	public:
		// Constructor
		CElmoDriver(void);

		// Destructor
		~CElmoDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(UINT)	DevCtrl(void * pContext, UINT uFunc, PCTXT Value);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			};

		CContext * m_pCtx;

		// Data Members
		BYTE	m_bTx[256];
		BYTE	m_bRx[256];

		UINT	m_uPtr;
		UINT	m_uError;

		DWORD	m_dLabel[10];

		static	ElmoCmdDef CODE_SEG CL[];

		ElmoCmdDef FAR * m_pCL;
		ElmoCmdDef FAR * m_pItem;

		// Hex Lookup
		LPCTXT	m_pHex;
	
		// Opcode Handlers
		BOOL	DoRead(AREF Addr);
		BOOL	DoWrite(AREF Addr, PDWORD pData, UINT uCount);
		
		// Frame Building
		void	StartFrame(UINT uOffset);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddCommand(void);
		void	PutText(LPCTXT pCmd);
		void	AddLabel(PDWORD pData);
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	CheckReply(char * cEcho);
		BOOL	GetResponse(PDWORD pData, UINT uType, UINT * pCount);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		void	SetpItem(UINT uID);
		void	PutNumber(DWORD wNum, BOOL fIsReal);
		DWORD	GetInteger(void);
		DWORD	GetReal(void);
		BOOL	ReadNoTransmit(UINT uTable, PDWORD pData, UINT * pCount);
		BOOL	WriteNoTransmit(UINT uTable, PDWORD pData);
		CCODE	InitComms(void);
		BOOL	SendInit(char * c);
		BOOL	HasNoValue(void);
		BOOL	HasString(void);
	};

// Data Types
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Echo Buffer
#define	MAXTX	12

// Numeric Constants
#define	RTNZ	409600000 // = 0x186A0000 -> 5 Decimal 0's, 4 Hex 0's
#define	NEGZERO	0x80000000

// Command Definitions
// Motion Commands
#define	CAC	1
#define	CBG	2
#define	CBT	3
#define	CDC	4
#define	CIL	5
#define	CJV	6
#define	CMO	7
#define	CPA	8
#define	CPR	9
#define	CSD	10
#define	CSF	11
#define	CSP	12
#define	CST	13
#define	CTC	14

// I/O Commands
#define	CAN	21
#define	CIB	22
#define	CIF	23
#define	CIP	24
#define	COB	25
#define	COC	26
#define	COL	27
#define	COP	28

// Status Commands
#define	CBV	31
#define	CDD	32
#define	CDV	33
#define	CEC	34
#define	CLC	35
#define	CMF	36
#define	CMS	37
#define	CPK	38
#define	CSN	39
#define	CSR	40
#define	CTI	41
#define	CVR	42

// Feedback Commands
#define	CAB	51
#define	CID	52
#define	CIQ	53
#define	CPE	54
#define	CPX	55
#define	CPY	56
#define	CVE	57
#define	CVX	58
#define	CVY	59
#define	CYA	60

// Configuration Commands
#define	CAG	71
#define	CAS	72
#define	CBP	73
#define	CCA	74
#define	CCL	75
#define	CEF	76
#define	CEM	77
#define	CET	78
#define	CFF	79
#define	CFR	80
#define	CHM	81
#define	CHY	82
#define	CMC	83
#define	CMP	84
#define	CPL	85
#define	CPM	86
#define	CPT	87
#define	CPV	88
#define	CPW	89
#define	CQP	90
#define	CQT	91
#define	CQV	92
#define	CRM	93
#define	CUM	94
#define	CTR	95
#define	CVH	96
#define	CVL	97
#define	CXM	98
#define	CYM	99

// Control Filter Commands
#define	CGS	111
#define	CKG	112
#define	CKR	113
#define	CKI	114
#define	CKP	115
#define	CKV	116
#define	CXA	117
#define	CXP	118

// Protection Commands
#define	CER	121
#define	CHL	122
#define	CLL	123

// Data Recording Commands
#define	CBH	131
#define	CRC	132
#define	CRG	133
#define	CRL	134
#define	CRP	135
#define	CRR	136
#define	CRV	137

// User Program Commands
#define	CHP	141
#define	CKL	142
#define	CMI	143
#define	CPS	144
#define	CXC	145
#define	CXQ	146

// General Commands
#define	CLD	151
#define	CRS	152
#define	CSV	153
#define	CTM	154
#define	CTS	155
#define	CUF	156
#define	CUI	157
#define	CWI	158
#define	CWS	159
#define	CZX	160

// End of File
