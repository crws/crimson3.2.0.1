#include "toshbase.hpp"
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CToshT2MasterDriver;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define FRAME_TIMEOUT	500 

//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 Master Serial Driver - Supports Toshiba T-Series and EX-Series PLCs.
//
//

class CToshT2MasterDriver : public CToshibaBaseMasterDriver
{
	public:
		// Constructor
		CToshT2MasterDriver(void);

		// Destructor
		~CToshT2MasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		
	protected:

		BOOL Start(UINT &uCount, UINT uType);

		// Transport Layer

		BOOL Transact(void);
		BOOL Send(void);
		BOOL Recv(void);
		BOOL CheckFrame(UINT uPtr);
	}; 

// End of File
