//////////////////////////////////////////////////////////////////////////
//
// Toshiba T/Ex Series Base Driver
//

#define	SERIEST		0
#define	SERIESEX	1
#define	SERIESS		2

class CToshibaBaseMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CToshibaBaseMasterDriver(void);

		// Destructor
		~CToshibaBaseMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			BYTE m_bSeries;
			BOOL m_fCheck;
						
			};

		
		CContext * m_pCtx;

		// Data Members

		LPCTXT 	m_pHex;
		UINT m_uCheck;
		UINT m_uRxLen;
		UINT m_uPtr;
		BYTE m_bRxBuff[300];
		BYTE m_bTxBuff[300];

		// Implementation

		BOOL PutRead(AREF Addr, UINT uCount);
		BOOL PutWrite(AREF Addr, PDWORD pData, UINT uCount);
		void PutData(PDWORD pData, UINT uCount, UINT uType, BOOL fBit, UINT uTable, UINT uExtra);
		void PutWordWrite(PDWORD pData, UINT uCount, BOOL fBit, UINT uTable, UINT uExtra);
		void PutLongWrite(PDWORD pData, UINT uCount, BOOL fBit, UINT uType);
		void PutRealWrite(PDWORD pData, UINT uCount, BOOL fBit, UINT uType);

		void GetData(PDWORD pData, UINT uCount, UINT uType, UINT uTable, UINT uExtra);
		void GetWordRead(PDWORD pData, UINT uCount, UINT uTable, UINT uExtra);
		void GetLongRead(PDWORD pData, UINT uCount, UINT uType);
		void GetRealRead(PDWORD pData, UINT uCount, UINT uType);
		UINT GetWordWrite(UINT uWord, BOOL fBit);

		BOOL IsBit(UINT uTable);
		WORD xtoin(PCTXT pText, UINT uCount);
		
		// Frame Building

		void AddDrop(void); 
		void AddByte(BYTE bByte);
		BOOL AddCommand(UINT uTable, BOOL fWrite);
		void AddOffset(UINT uTable, UINT uOffset);
		void AddCount(UINT uCount, UINT uType);
		void AddDec(UINT n, UINT f);
		void AddHex(UINT n, UINT f);
		void AddCheck(void);
		void AddDeviceData(UINT uExtra);

		// Overridables
				
		virtual BOOL Start(UINT &uCount, UINT uType);
									
		// Transport Layer

		virtual	BOOL Transact(void);

		// Helpers
		UINT GetDigits(UINT uValue);
		BOOL HasDeviceData(UINT uTable, UINT uExtra);
		BOOL IsStatus(UINT uTable, UINT uExtra);
		

		
	};

// End of File
