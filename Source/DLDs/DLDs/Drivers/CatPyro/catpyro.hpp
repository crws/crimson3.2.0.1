
//////////////////////////////////////////////////////////////////////////
//
// CatPyro Master Driver
//

class CCatPyroMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CCatPyroMasterDriver(void);

		// Destructor
		~CCatPyroMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Data Members
		UINT m_Drop;
		BYTE m_bSend[11];
		BYTE m_bRecv[7];
		bool Refresh;

		// Implementation
		void SendReadRequest(UINT uAddr);
		BOOL AcceptReadReply(PDWORD pData, UINT uAddr);
		DWORD tLookup(PDWORD pData);
		bool SendRefresh(void);
		void Debug(PCTXT pText, ...);
	};

// End of File
