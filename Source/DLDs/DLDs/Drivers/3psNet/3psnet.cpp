#include "3psnet.hpp"

/////////////////////////////////////////////////////////////////////////
//
// 3 Point Solutions 3psNet Driver
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(C3psNetDriver);

// Constructor

C3psNetDriver::C3psNetDriver(void)
{
	m_Ident = DRIVER_ID;
}

// Destructor

C3psNetDriver::~C3psNetDriver(void)
{

}

// Config

void MCALL C3psNetDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bSrc = GetByte(pData);
	}
}

void MCALL C3psNetDriver::CheckConfig(CSerialConfig &Config)
{
	Config.m_bDrop = m_bSrc;

	Config.m_uFlags |= flagPrivate;
}

void MCALL C3psNetDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new C3psNetHandler(m_pHelper, this);

	m_pHandler->SetSource(m_bSrc);

	pPort->Bind(m_pHandler);
}

void MCALL C3psNetDriver::Detach(void)
{
	m_pHandler->Release();
}

void MCALL C3psNetDriver::Open(void)
{
}

// Device

CCODE MCALL C3psNetDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_ID		= GetWord(pData);

			m_pCtx->m_bType		= GetByte(pData);

			m_pCtx->m_Custom	= GetByte(pData);

			m_pCtx->m_Wireless	= GetByte(pData);

			m_pCtx->m_RelayValue	= 0;

			m_pCtx->m_uInterval	= GetLong(pData);

			m_pCtx->m_Transact	= GetWord(pData);

			pDevice->SetContext(m_pCtx);

			m_pHandler->MakeDEV(m_pCtx->m_ID, GetType(), m_pCtx->m_Wireless, m_pCtx->m_uInterval, m_pCtx->m_Transact);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL C3psNetDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL C3psNetDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = messGetConfigRevision;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
}

CCODE MCALL C3psNetDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	DEV * pDev = FindDEV();

	if( pDev ) {

		if( IsBroadcast(Addr.a.m_Table, Addr.a.m_Offset) ) {

			if( m_pHandler->HasBroadcast(pDev) ) {

				if( GetBroadcast(pDev, Addr.a.m_Offset & 0xFF, pData) ) {

					return 1;
				}

				return CCODE_ERROR | CCODE_HARD;
			}

			return CCODE_ERROR;
		}

		if( GetInternal(pDev, Addr.a.m_Table, Addr.a.m_Offset, pData) ) {

			return 1;
		}

		if( !IsValid(pDev, Addr.a.m_Table, Addr.a.m_Offset) ) {

			return CCODE_ERROR | CCODE_HARD;
		}

		WORD wMsg = GetReadMsg(Addr.a.m_Table, Addr.a.m_Offset);

		if( IsWriteOnly(wMsg) ) {

			return 1;
		}

		if( Transact(pDev, wMsg, pData, FALSE) ) {

			return 1;
		}

		return CCODE_ERROR;
	}

	return CCODE_ERROR | CCODE_HARD;
}

CCODE MCALL C3psNetDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	DEV * pDev = FindDEV();

	if( pDev ) {

		if( IsBroadcast(Addr.a.m_Table, Addr.a.m_Offset) ) {

			return 1;
		}

		if( SetInternal(pDev, Addr.a.m_Table, Addr.a.m_Offset, pData[0]) ) {

			return 1;
		}

		if( !IsValid(pDev, Addr.a.m_Table, Addr.a.m_Offset) ) {

			return CCODE_ERROR | CCODE_HARD;
		}

		WORD wMsg = GetWriteMsg(Addr.a.m_Table, Addr.a.m_Offset);

		if( IsReadOnly(wMsg) ) {

			return 1;
		}

		if( !SetData(pDev, Addr.a.m_Offset, pData) ) {

			return 1;
		}

		if( Transact(pDev, wMsg, pData, TRUE) ) {

			return 1;
		}

		return CCODE_ERROR;
	}

	return CCODE_ERROR | CCODE_HARD;
}

// Implementation

DEV * C3psNetDriver::FindDEV(void)
{
	BYTE bType = GetType();

	DEV * pDEV = m_pHandler->FindDevice(m_pCtx->m_ID, bType);

	if( !pDEV ) {

		m_pHandler->MakeDEV(m_pCtx->m_ID, bType, m_pCtx->m_Wireless, m_pCtx->m_uInterval, m_pCtx->m_Transact);

		pDEV = m_pHandler->FindDevice(m_pCtx->m_ID, bType);
	}

	return pDEV;
}

BOOL C3psNetDriver::GetBroadcast(DEV * pDev, BYTE bIndex, PDWORD pData)
{
	switch( bIndex ) {

		case broStatus:		pData[0] = pDev->m_bStatus;		return TRUE;
		case broBattery:	pData[0] = pDev->m_bBattery;		return TRUE;
		case broTemp:		pData[0] = pDev->m_bTemp;		return TRUE;
		case broValue0:		pData[0] = pDev->m_pData[msgBroadcast];	return TRUE;
	}

	return FALSE;
}

BOOL C3psNetDriver::GetInternal(DEV * pDev, BYTE bTable, UINT uOffset, PDWORD pData)
{
	UINT uMsg = (GetType() << 8) | bTable;

	if( uMsg == messIntRelayValue ) {

		UINT uShift = (uOffset - 1) * 8;

		pData[0] = ((m_pCtx->m_RelayValue >> uShift) & 0xFF) ? 1 : 0;

		return TRUE;
	}

	return FALSE;
}

BOOL C3psNetDriver::SetInternal(DEV * pDev, BYTE bTable, UINT uOffset, DWORD dwData)
{
	UINT uMsg = (GetType() << 8) | bTable;

	if( uMsg == messIntRelayValue ) {

		UINT uShift = (uOffset - 1) * 8;

		if( dwData ) {

			m_pCtx->m_RelayValue |=  (0xFF << uShift);
		}
		else {
			m_pCtx->m_RelayValue &= ~(0xFF << uShift);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL C3psNetDriver::SetData(DEV * pDev, UINT uOffset, PDWORD pData)
{
	UINT uMsg = (GetType() << 8) | uOffset;

	if( uMsg == messSetRelayValue ) {

		if( pData[0] ) {

			pData[0] = m_pCtx->m_RelayValue;

			return TRUE;
		}

		return FALSE;
	}

	if( uMsg == messZeroAngle ) {

		if( pData[0] ) {

			pData[0] = 0;

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

DWORD C3psNetDriver::FixUp(WORD wMsg, DWORD dwData)
{
	if( wMsg == messGetConfigRevision ) {

		return dwData >> 8;
	}

	switch( wMsg ) {

		case messGetTempPoint0:
		case messGetTempPoint1:
		case messSetTempPoint0:
		case messSetTempPoint1:

			return dwData & 0x00FFFFFF;

		case messSetShuntCal:
		case messSetPowerDown:

			if( dwData ) {

				return dwData == 1 ? 0x01010101 : 1;
			}
	}

	return dwData;
}

WORD C3psNetDriver::GetReadMsg(BYTE bTable, UINT uMsg)
{
	WORD wMsg = bTable == addrNamed ? uMsg : bTable;

	switch( wMsg ) {

		case messGetSpan0:		if( uMsg ) wMsg = messGetSpan1;		break;
		case messGetZero0:		if( uMsg ) wMsg = messGetZero1;		break;
		case messGetTare0:		if( uMsg ) wMsg = messGetTare1;		break;
		case messGetCold0:		if( uMsg ) wMsg = messGetCold1;		break;
		case messGetRoom0:		if( uMsg ) wMsg = messGetRoom1;		break;
		case messGetHot0:		if( uMsg ) wMsg = messGetHot1;		break;
		case messGetTempPoint0:		if( uMsg ) wMsg = messGetTempPoint1;	break;
		case messIntRelayValue & 0xFF:	if( uMsg ) wMsg = messIntRelayValue;	break;
	}

	return wMsg;
}

WORD C3psNetDriver::GetWriteMsg(BYTE bTable, UINT uMsg)
{
	WORD wMsg = GetReadMsg(bTable, uMsg);

	switch( wMsg ) {

		case messGetCapacity:		wMsg = messSetCapacity;		break;
		case messGetSpan0:		wMsg = messSetSpan0;		break;
		case messGetZero0:		wMsg = messSetZero0;		break;
		case messGetTare0:		wMsg = messSetTare0;		break;
		case messGetCold0:		wMsg = messSetCold0;		break;
		case messGetRoom0:		wMsg = messSetRoom0;		break;
		case messGetHot0:		wMsg = messSetHot0;		break;
		case messGetTempPoint0:		wMsg = messSetTempPoint0;	break;
		case messGetFilterThreshold:	wMsg = messSetFilterThreshold;	break;
		case messGetFilterConstant:	wMsg = messSetFilterConstant;	break;
		case messGetTXID:		wMsg = messSetTXID;		break;
		case messGetSpan1:		wMsg = messSetSpan1;		break;
		case messGetZero1:		wMsg = messSetZero1;		break;
		case messGetTare1:		wMsg = messSetTare1;		break;
		case messGetCold1:		wMsg = messSetCold1;		break;
		case messGetRoom1:		wMsg = messSetRoom1;		break;
		case messGetHot1:		wMsg = messSetHot1;		break;
		case messGetTempPoint1:		wMsg = messSetTempPoint1;	break;
	}

	return wMsg;
}

// Transport

BOOL C3psNetDriver::Transact(DEV * pDev, WORD wMsg, PDWORD pData, BOOL fWrite)
{
	if( pDev ) {

		if( fWrite ) {

			DWORD Data = FixUp(wMsg, pData[0]);

			return m_pHandler->SendMessage(GetType(), pDev->m_ID, wMsg & 0xFF, &Data);
		}

		if( m_pHandler->SendMessageReq(GetType(), pDev->m_ID, wMsg & 0xFF) ) {

			SetTimer(pDev->m_uTransact);

			while( GetTimer() ) {

				if( m_pHandler->HasExplicit(pDev) ) {

					pData[0] = FixUp(wMsg, pDev->m_pData[msgExplicit]);

					return TRUE;
				}

				Sleep(20);
			}
		}
	}

	return FALSE;
}

// Helpers

BOOL C3psNetDriver::IsWriteOnly(WORD wMsg)
{
	switch( wMsg ) {

		case messSetShuntCal:
		case messSetRawData:
		case messIntRelayValue:
		case messSetRelayValue:
		case messSetPowerDown:
		case messZeroAngle:
		case messResetLength:

			return TRUE;
	}


	return FALSE;
}

BOOL C3psNetDriver::IsReadOnly(WORD wMsg)
{
	switch( wMsg ) {

		case messGetConfigRevision:

			return TRUE;
	}

	return FALSE;
}

BOOL C3psNetDriver::IsBroadcast(BYTE bTable, UINT uOffset)
{
	return ((bTable == addrNamed) && ((uOffset >> 8) == 0xFF));
}

BOOL C3psNetDriver::IsValid(DEV * pDev, BYTE bTable, UINT uOffset)
{
	if( IsCustom() ) {

		return TRUE;
	}

	BYTE bType = GetType();

	switch( bType ) {

		case devLoadSensHard:
		case devLoadSensWireless:

			return uOffset <= 0xFF;

		case devRelayOutputHard4:

			if( bTable == (messIntRelayValue & 0xFF) ) {

				return TRUE;
			}

			break;

		case devAngleSensorHardBoom:

			if( uOffset == (messZeroAngle & 0xFF) ) {

				return TRUE;
			}

			break;
	}

	return ((uOffset == 0) || ((uOffset >> 8) == bType));
}

BYTE C3psNetDriver::GetType(void)
{
	if( m_pCtx->m_bType == devCustom ) {

		return m_pCtx->m_Custom;
	}

	return m_pCtx->m_bType;
}

BOOL C3psNetDriver::IsCustom(void)
{
	return m_pCtx->m_bType == devCustom;
}

// End of File

