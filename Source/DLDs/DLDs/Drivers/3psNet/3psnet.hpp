
#ifndef	INCLUDE_3PSNET_HPP

#define	INCLUDE_3PSNET_HPP

#include "intern.hpp"

/////////////////////////////////////////////////////////////////////////
//
// 3 Point Solutions 3psNet Driver
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class C3psNetDriver;

//////////////////////////////////////////////////////////////////////////
//
// 3PSNet Enumerations
//

enum Msg {

	msgBroadcast		= 0,
	msgExplicit		= 1
};

enum DevType {

	devLoadSensHard		= 2,
	devLoadSensWireless	= 3,
	devA2BSensorHard	= 4,
	devA2BSensorWireless	= 5,
	devAngleSensorHardBoom  = 6,
	devRelayOutputHard4	= 7,
	devWindSpeedWireless	= 8,
	devPayoutHard		= 9,
	devRemoteAntenna	= 10,
	devSpeedHard		= 11,
	devAnalogOuputHard1	= 13,
	devAnalogInput		= 12,
	devDualAngleWireless	= 14,
	devSlewSensorHard	= 15,
	devDualAngleSensorHard	= 16,
	devPayoutWireless	= 17,
	devPayoutSpeedWireless	= 18,
	devTorqueWireless	= 19,
	devTorqueHookWireless	= 20,
	devPressureSensor	= 21,
	devSwitchInputsHard8    = 35,
	devBoomLengthHard	= 26,
	devCustom		= 255,
};

enum DevMessType {

	messGetConfigRevision	= 0x0000,
	messSetShuntCal		= 0x0001,
	messSetRawData		= 0x0002,
	messSetCapacity		= 0x0003,
	messGetCapacity		= 0x0004,
	messSetSpan0		= 0x0007,
	messGetSpan0		= 0x0008,
	messSetSpan1		= 0x0009,
	messGetSpan1		= 0x000A,
	messSetZero0		= 0x000B,
	messGetZero0		= 0x000C,
	messSetZero1		= 0x000D,
	messGetZero1		= 0x000E,
	messSetTare0		= 0x000F,
	messGetTare0		= 0x0010,
	messSetTare1		= 0x0011,
	messGetTare1		= 0x0012,
	messSetCold0		= 0x0013,
	messGetCold0		= 0x0014,
	messSetRoom0		= 0x0015,
	messGetRoom0		= 0x0016,
	messSetHot0		= 0x0017,
	messGetHot0		= 0x0018,
	messSetCold1		= 0x0019,
	messGetCold1		= 0x001A,
	messSetRoom1		= 0x001B,
	messGetRoom1		= 0x001C,
	messSetHot1		= 0x001D,
	messGetHot1		= 0x001E,
	messSetTempPoint0	= 0x001F,
	messGetTempPoint0	= 0x0020,
	messSetTempPoint1	= 0x0021,
	messGetTempPoint1	= 0x0022,
	messSetFilterThreshold  = 0x0026,
	messGetFilterThreshold  = 0x0027,
	messSetFilterConstant   = 0x0028,
	messGetFilterConstant   = 0x0029,
	messIntRelayValue	= 0x07EF,
	messSetRelayValue	= 0x0702,
	messSetTXID		= 0x0A02,
	messGetTXID		= 0x0A03,
	messSetPowerDown	= 0x0A04,

	messBroadcastStatus	= 0xFF01,
	messBroadcastBattery	= 0xFF02,
	messBroadcastTemp	= 0xFF03,
	messBroadcastValue	= 0xFF04,

	messZeroAngle		= 0x0602,
	messResetLength		= 0x1A05,
};

enum Explicit {

	expDevType = 0,
	expNetIdLo = 1,
	expNetIdHi = 2,
	expMesType = 3,
	expValue0  = 4,
	expValue1  = 5,
	expValue2  = 6,
	expValue3  = 7,
};

enum BroadCast {

	broMesType = 0,
	broStatus  = 1,
	broBattery = 2,
	broTemp    = 3,
	broValue0  = 4,
	broValue1  = 5,
	broValue2  = 6,
	broValue3  = 7,
};

//////////////////////////////////////////////////////////////////////////
//
// CAN Frame
//

#pragma pack(1)

struct FRAME {

	BYTE	bCount;
	BYTE	bZero;
	WORD	wID;
	BYTE	bData[8];
};

#pragma pack()


//////////////////////////////////////////////////////////////////////////
//
// Device Structure
//

struct DEV {

	DWORD m_pData[2];
	UINT  m_uTime;
	UINT  m_uInterval;
	UINT  m_uTransact;

	WORD  m_ID;
	BYTE  m_bType;
	BYTE  m_bWireless;
	BYTE  m_bStatus;
	BYTE  m_bBattery;
	BYTE  m_bTemp;
	BYTE  m_bMessage;
	BYTE  m_bSet;
};

//////////////////////////////////////////////////////////////////////////
//
// 3psNet Handler
//

class C3psNetHandler : public IPortHandler
{
	public:
		// Constructor
		C3psNetHandler(IHelper *pHelper, C3psNetDriver *pDriver);

		// Destructor
		~C3psNetHandler(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Events
		void CreateEvents(void);

		// IPortHandler
		void METHOD Bind(IPortObject * pPort);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnTimer(void);

		// Driver Access
		BOOL SendMessage(BYTE bDev, WORD wID, BYTE bMess, PDWORD pData);
		BOOL SendMessageReq(BYTE bDev, WORD wID, BYTE bMsg);
		void MakeDEV(WORD wId, BYTE bType, BYTE bWireless, UINT uLive, UINT uTran);
		DEV* FindDevice(WORD wID, BYTE bType);
		BOOL HasBroadcast(DEV * pDEV);
		BOOL HasExplicit(DEV * pDEV);
		void SetSource(BYTE bSrc);
		void Clear(DEV * pDEV);

	protected:

		// Help
		IHelper	*	m_pHelper;

		// Driver Access
		C3psNetDriver * m_pDriver;

		// Data Members
		ULONG		m_uRefs;
		DEV *		m_pDEVs;
		UINT		m_uDEVs;
		FRAME		m_RxData;
		PCBYTE		m_pTxData;
		UINT		m_uRx;
		UINT		m_uTx;
		IPortObject *	m_pPort;
		FRAME 		m_Rx;
		FRAME		m_Tx;
		BYTE 		m_bSrc;
		UINT		m_uSend;
		FRAME		m_Send[64];
		UINT		m_uQueue;
		ISemaphore *	m_pRxFlag;
		IEvent     *	m_pTxFlag;
		UINT		m_uRxTail;
		UINT		m_uRxHead;

		// Implementation
		void Start(UINT &uIndex);
		void AddByte(BYTE bByte, UINT &uIndex);
		void AddData(PDWORD pData, UINT &uIndex);
		void GrowDEVs(DEV * pDEV);
		BOOL PutFrame(void);
		void SendFrame(void);
		void HandleFrame(void);
		BOOL SetBroadcast(DEV * pDEV);
		BOOL SetExplicit(DEV * pDEV);

		// Helpers
		BOOL IsBroadcast(void);
		BOOL IsExplicit(void);
		BOOL IsTimedOut(UINT uTime, UINT uSpan);
		void Increment(UINT &uIndex);

		// Testing / Debug
		void ShowDEVs(void);
};

//////////////////////////////////////////////////////////////////////////
//
// 3 Point Solutions 3PSNET CAN Bus Driver
//
//
//

class C3psNetDriver : public CMasterDriver
{
	public:
		// Constructor
		C3psNetDriver(void);

		// Destructor
		~C3psNetDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
	
		// Entry Points
		DEFMETH(CCODE)Ping(void);
		DEFMETH(CCODE)Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		struct CContext
		{
			WORD	       m_ID;
			BYTE           m_bType;
			DWORD	       m_Serial;
			UINT	       m_uInterval;
			DWORD	       m_RelayValue;
			BYTE	       m_Custom;
			BYTE	       m_Wireless;
			WORD	       m_Transact;
		};

		// Data Members

		CContext	* m_pCtx;
		BYTE		  m_bSrc;

		// Handler

		C3psNetHandler * m_pHandler;

		// Implementation
		DEV * FindDEV(void);
		BOOL  GetBroadcast(DEV * pDev, BYTE bMsg, PDWORD pData);
		BOOL  GetInternal(DEV * pDev, BYTE bTable, UINT uOffset, PDWORD pData);
		BOOL  SetInternal(DEV * pDev, BYTE bTable, UINT uOffset, DWORD dwData);
		BOOL  SetData(DEV * pDev, UINT uOffset, PDWORD pData);
		DWORD FixUp(WORD wMsg, DWORD dwData);
		WORD  GetReadMsg(BYTE bTable, UINT uMsg);
		WORD  GetWriteMsg(BYTE bTable, UINT uMsg);

		// Transport
		BOOL Transact(DEV * pDev, WORD wMsg, PDWORD pData, BOOL fWrite);

		// Helpers
		BOOL IsWriteOnly(WORD wMsg);
		BOOL IsReadOnly(WORD wMsg);
		BOOL IsBroadcast(BYTE bTable, UINT uOffset);
		BOOL IsValid(DEV * pDev, BYTE bTable, UINT uOffset);
		BYTE GetType(void);
		BOOL IsCustom(void);

};

// End of File

#endif

