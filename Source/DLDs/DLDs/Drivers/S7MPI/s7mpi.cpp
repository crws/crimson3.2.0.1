
#include "intern.hpp"

#include "s7mpi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 via MPI Adpater Driver
//

// Instantiator

INSTANTIATE(CS7MPIMasterDriver);

// Constructor

CS7MPIMasterDriver::CS7MPIMasterDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_uBurst    = 0;
	
	m_bLastDrop = 0x1F;
	}

// Destructor

CS7MPIMasterDriver::~CS7MPIMasterDriver(void)
{
	}

// Configuration

void MCALL CS7MPIMasterDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bLastDrop = GetByte(pData);
		}
	}
	
void MCALL CS7MPIMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);		
	}
	
// Management

void MCALL CS7MPIMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CS7MPIMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CS7MPIMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bThisDrop	= GetByte(pData);

			m_pCtx->m_bDrop		= GetByte(pData);

			m_pCtx->m_uTimeout	= GetLong(pData);

			m_pCtx->m_uMode		= MODE_IDLE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CS7MPIMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CS7MPIMasterDriver::Ping(void)
{
	if( !InitDrop() ) {

		return CCODE_ERROR;
		}   

	return CCODE_SUCCESS;
	}

CCODE MCALL CS7MPIMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !InitDrop() ) {

		return CCODE_ERROR;
		}

	UINT Space  = Addr.a.m_Table & 0x0F;
	
	UINT Index  = Addr.a.m_Offset;

	GetCount(Addr.a.m_Type, uCount);

	if( Space < SPACE_DATA ) {
	
		BYTE Opcode;
		
		switch( Space ) {
		
			case SPACE_INPUT:
				Opcode = 0x12;
				break;
		
			case SPACE_OUTPUT:
				Opcode = 0x10;
				break;
				
			case SPACE_FLAG:
				Opcode = 0x14;
				break;

			case SPACE_TIMER:
				Opcode = 0x16;
				break;
		
			case SPACE_COUNTER:
				Opcode = 0x18;
				break;
				
			default:
				return CCODE_ERROR;
			}
			
		HmiStartFrame(Opcode, 1, 4);
		}
	else {
		BYTE Opcode = 0x1A;

		HmiStartFrame(Opcode, 1, 6);

		AddWord(DATA_BLOCK(Addr.a.m_Extra, Addr.a.m_Table, Addr.a.m_Offset));
		}

	AddWord(INDEX(Index));

	switch( Addr.a.m_Type ) {

		case addrByteAsByte:

			return DoByteRead(pData, uCount);

		case addrByteAsWord:
		case addrWordAsWord:

			return DoWordRead(pData, uCount);

		case addrByteAsLong:

			return DoLongRead(pData, uCount);

		}

	return CCODE_ERROR; 
	}

CCODE MCALL CS7MPIMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !InitDrop() ) {

		return CCODE_ERROR;
		}
	
	UINT Space  = Addr.a.m_Table & 0x0F;
	
	UINT Index  = Addr.a.m_Offset;

	GetCount(Addr.a.m_Type, uCount);

	if( Space < SPACE_DATA ) {
	
		BYTE Opcode;
		
		switch( Space ) {

			case SPACE_INPUT:
				Opcode = 0x13;
				break;
		
			case SPACE_OUTPUT:
				Opcode = 0x11;
				break;
				
			case SPACE_FLAG:
				Opcode = 0x15;
				break;
				
			case SPACE_TIMER:
				Opcode = 0x17;
				break;
				
			case SPACE_COUNTER:
				Opcode = 0x19;
				break;
				
			default:
				return uCount;
			}

		if( Space == SPACE_TIMER || Space == SPACE_COUNTER ) {
		
			HmiStartFrame(Opcode, 1, 4 + uCount * 2);
			}
		else
			HmiStartFrame(Opcode, 1, 4 + uCount);
		}
	else {
		BYTE Opcode = 0x1B;

		HmiStartFrame(Opcode, 1, 6 + uCount);
		
		AddWord(DATA_BLOCK(Addr.a.m_Extra, Addr.a.m_Table, Addr.a.m_Offset));
		}
		
	AddWord(INDEX(Index));

	switch( Addr.a.m_Type ) {

		case addrByteAsByte:

			return DoByteWrite(pData, uCount);

		case addrByteAsWord:
		case addrWordAsWord:

			return DoWordWrite(pData, uCount);

		case addrByteAsLong:

			return DoLongWrite(pData, uCount);
		} 

	return CCODE_ERROR;

	}

// Implementation 

BOOL CS7MPIMasterDriver::InitDrop(void)
{
	return HmiInitLink(m_pCtx->m_bDrop);
	}

void CS7MPIMasterDriver::ResetLink(void)
{
	m_pData->SetBreak(TRUE);
	
	Sleep(500);

	m_pData->SetBreak(FALSE);
	
	Sleep(500);
	}

void CS7MPIMasterDriver::PutByte(BYTE b)
{
	m_pData->Write(b, FOREVER);
	}

// 3964R Transport

void CS7MPIMasterDriver::StartFrame(void)
{
	m_uPtr   = 0;
	
	m_bCheck = 0;	
	}

void CS7MPIMasterDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CS7MPIMasterDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void CS7MPIMasterDriver::AddWordIntel(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}
	
void CS7MPIMasterDriver::AddByte(BYTE bData)
{
	if( bData == DLE ) {

		AddCtrl(bData);
		}
	
	AddCtrl(bData);
	}

void CS7MPIMasterDriver::AddCtrl(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;
	
	m_bCheck           ^= bData;
	}

BOOL CS7MPIMasterDriver::PutFrame(void)
{
	for( UINT n = 0; n < 2; n++ ) {
	
		PutByte(STX);
		
		if( GetInitAck() ) {

			Sleep(10);

			PutPDU(n == 0);
			
			return GetTermAck();
			}
		
		Sleep(400);
		}
	
	return FALSE;
	}

void CS7MPIMasterDriver::PutPDU(BOOL fAdd)
{
	if( fAdd ) {

		AddCtrl(DLE);

		AddCtrl(ETX);

		AddByte(m_bCheck);
		}

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);
	}
		
BOOL CS7MPIMasterDriver::GetInitAck(void)
{
	UINT uTimer = 0;

	UINT uByte  = 0;

	SetTimer(600);
	
	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uByte ) {
		
			case DLE:

				return TRUE;

			case STX:
			
				PutByte(DLE);
			
			case NAK:

				return FALSE;
			}
		}
		
	return FALSE;
	}

BOOL CS7MPIMasterDriver::GetTermAck(void)
{
	UINT uTimer = 0;

	UINT uByte  = 0;

	SetTimer(1000);
	
	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uByte ) {
		
			case DLE:
				
				return TRUE;
				
			case STX:

				PutByte(DLE);

			case NAK:

				return FALSE;
			}
		}

	return FALSE;
	}

BOOL CS7MPIMasterDriver::GetFrame(UINT uTimeout)
{
	UINT uByte  = 0;
	
	UINT uRetry = 2;

	UINT uState = 0;
	
	UINT uCount = 0;
	
	BYTE bCheck = 0;

	UINT uTimer = 0;

	SetTimer(uTimeout);

	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
			
				if( uByte == STX ) {
				
					PutByte(DLE);

					SetTimer(uTimeout);
					
					uCount = 0;
					
					bCheck = 0;

					uState = 1;
					}
				break;
				
			case 1:
			
				if( uByte == DLE )
					uState = 2;
				else {
					m_bRxBuff[uCount++] = uByte;
					
					bCheck ^= uByte;
					
					if( uCount == sizeof(m_bRxBuff) ) {
					
						PutByte(DLE);

						return FALSE;
						}
					}
				break;
				
			case 2:
			
				if( uByte == DLE ) {
				
					m_bRxBuff[uCount++] = uByte;
					
					if( uCount == sizeof(m_bRxBuff) ) {
					
						PutByte(DLE);

						return FALSE;
						}
						
					uState = 1;
					}
				else {
					if( uByte == ETX ) {
					
						bCheck ^= DLE;

						bCheck ^= ETX;
					
						uState  = 3;
						}
					else {
						PutByte(DLE);
						
						return FALSE;
						}
					}
				break;

			case 3:
			
				if( bCheck == uByte ) {
				
					PutByte(DLE);

					return TRUE;
					}
					
				if( !uRetry-- ) {

					PutByte(DLE);

					return FALSE;
					}
				else {
					PutByte(NAK);
					
					uState = 0;

					}
				break;
			}
		}

	PutByte(DLE);

	return FALSE;
	}

// Adapter Support

BOOL CS7MPIMasterDriver::HmiInitLink(BYTE bDrop)
{
	for( UINT n = 0; n < 2; ) {

		if( m_pCtx->m_uMode == MODE_IDLE ) {
			
			ResetLink();
			
			if( HmiStartNetwork() ) {
				
				m_pCtx->m_uMode = MODE_RUNNING;
				
				continue;
				}
			}
			
		if( m_pCtx->m_uMode == MODE_CONNECTED ) {
			
			if( m_bDrop != bDrop ) {
			
				if( HmiDisconnect() ) {
				
					// NOTE: Frig for Siemens problem!
				
					Sleep(80);
				
					m_pCtx->m_uMode = MODE_RUNNING;
					
					continue;
					}
				}
			else
				return TRUE;
			}
			
		if( m_pCtx->m_uMode == MODE_RUNNING ) {
			
			if( HmiConnect(bDrop) ) {
			
				m_bDrop = bDrop;
				
				m_pCtx->m_uMode = MODE_CONNECTED;
				
				return TRUE;
				}
			}

		n++;
		
		Sleep(800);
		} 

	return FALSE;
	}

BOOL CS7MPIMasterDriver::HmiStartNetwork(void)
{
	WORD a = m_bLastDrop;
	WORD p = m_pCtx->m_bThisDrop;
	
	StartFrame();
	
	AddByte(0x01);
	AddByte(0x03);
	AddByte(0x02);
	
	AddWordIntel(a + 8);
	
	AddWordIntel(0x019F);
	AddWordIntel(0x003C);
	AddWordIntel(0x0190);
	AddWordIntel(0x0014);
	
	AddByte(0x00);
	AddByte(0x00);
	AddByte(0x02);
	
	AddByte(p);
	AddByte(a);

	AddByte(0x02);
	AddByte(0x01);
	AddByte(0x01);
	AddByte(0x03);
	AddByte(0x81);

	return PutFrame() && GetFrame(m_pCtx->m_uTimeout);
	}
	
BOOL CS7MPIMasterDriver::HmiConnect(BYTE bDrop)
{
	HmiStartFrame(0x80, 0, 16);
	
	for( UINT n = 0; n < 4; n++ ) {
	
		if( !n ) {
			AddByte(bDrop);
			AddByte(0x01);
			AddByte(0x00);
			AddByte(0x00);
			}
		else {
			AddByte(0x00);
			AddByte(0x00);
			AddByte(0x00);
			AddByte(0x00);
			}
		} 

	return HmiTransact();
	}
	
BOOL CS7MPIMasterDriver::HmiDisconnect(void)
{
	HmiStartFrame(0x81, 0, 0);

	return HmiTransact();
	}

void CS7MPIMasterDriver::HmiStartFrame(BYTE op, BYTE c, WORD n)
{
	StartFrame();
		
	AddByte(0x21);

	AddByte(op);

	AddWord(0x0000);

	AddWord(n);

	AddByte(c);

	AddByte(0x01);

	AddByte(0x00);

	AddByte(0x00);
	}
	
BOOL CS7MPIMasterDriver::HmiTransact(void)
{
	if( PutFrame() && GetFrame(800) ) {

		if( m_bRxBuff[2] || m_bRxBuff[3] ) {
		
			m_uError = MAKEWORD(m_bRxBuff[2], m_bRxBuff[3]);

			HmiCheckError();
			
			return FALSE;
			}

		m_uError = ERROR_NONE;
		
		HmiCheckError();
		
		return TRUE;
		}

	m_uError = ERROR_COMMS;

	HmiCheckError();
		
	return FALSE;
	}

void CS7MPIMasterDriver::HmiCheckError(void)
{
	switch( m_uError ) {
	
		case ERROR_NONE:
			
			m_uBurst = 0;
			
			break;

		case 0x0204:
		case 0x031A:
		
			m_pCtx->m_uMode = MODE_IDLE;
			
			break;

		default:
		
			if( ++m_uBurst == 4 ) {

				m_pCtx->m_uMode    = MODE_IDLE;
			
				m_uBurst = 0;
				}

			break;
		}
	}

CCODE CS7MPIMasterDriver::DoByteRead(PDWORD pData, UINT uCount)
{
	AddWord(uCount);

	if( HmiTransact() ) {

		for( UINT n = 0; n < uCount; n++ ) {
		
			BYTE bData = PBYTE(m_bRxBuff)[10 + n];

			pData[n] = bData;
			}
		
		return uCount;
		} 
       
	return CCODE_ERROR;
	}

CCODE CS7MPIMasterDriver::DoWordRead(PDWORD pData, UINT uCount)
{
	AddWord(uCount * 2);

	if( HmiTransact() ) {

		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x = PU2(m_bRxBuff)[5 + n];

			pData[n] = LONG(SHORT(MotorToHost(x)));
			}
						
		return uCount;
		} 
	
	return CCODE_ERROR;
	}

CCODE CS7MPIMasterDriver::DoLongRead(PDWORD pData, UINT uCount)
{
	AddWord(uCount * 4);

	if( HmiTransact() ) {

		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x = PU4(m_bRxBuff + 10)[n];
			
			pData[n] = MotorToHost(x);
			}
			
		return uCount;
		} 
	
	return CCODE_ERROR;
	}

CCODE CS7MPIMasterDriver::DoByteWrite(PDWORD pData, UINT uCount)
{
	AddWord(uCount);

	for( UINT n = 0; n < uCount; n++ ) {

		BYTE bData = pData[n];
	
		AddByte(bData);
		}
			    
	return HmiTransact() ? uCount : CCODE_ERROR;
	}

CCODE CS7MPIMasterDriver::DoWordWrite(PDWORD pData, UINT uCount)
{
	AddWord(uCount * 2);

	for( UINT n = 0; n < uCount; n++ ) {

		AddWord(pData[n]);
		}
	
	return HmiTransact() ? uCount : CCODE_ERROR;
	}

CCODE CS7MPIMasterDriver::DoLongWrite(PDWORD pData, UINT uCount)
{
	AddWord(uCount * 4);

	for( UINT n = 0; n < uCount; n++ ) {

		AddLong(pData[n]);
		}
	
	return HmiTransact() ? uCount : CCODE_ERROR;
	}

void CS7MPIMasterDriver::GetCount(UINT uType, UINT& uCount)
{
	switch( uType ) {

		case addrByteAsByte:

			MakeMin(uCount, 64);
			break;

		case addrByteAsWord:
	
			MakeMin(uCount, 64);
			break;

		case addrWordAsWord:
		case addrByteAsLong:

			MakeMin(uCount, 32);
			break;
		}
	}

// End of File
