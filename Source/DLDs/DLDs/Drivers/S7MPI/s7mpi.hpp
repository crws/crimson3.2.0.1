//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 MPI Constants
//

#define	MODE_IDLE	0x00
#define	MODE_RUNNING	0x01
#define	MODE_CONNECTED	0x02

#define	ERROR_NONE	0x0000
#define	ERROR_COMMS	0x0001

#define	SPACE_OUTPUT	0x01
#define	SPACE_INPUT	0x02
#define	SPACE_FLAG	0x03
#define	SPACE_TIMER	0x04
#define	SPACE_COUNTER	0x05
#define	SPACE_DATA	0x0A

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 MPI Macro's
//

#define DATA_BLOCK(a,b,c)	(a + (b & 0xF0) + ((c & 0xE000) >> 5))
#define INDEX(a)			(a & 0x1FFF)

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 PLC via MPI Master Driver
//

class CS7MPIMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CS7MPIMasterDriver(void);

		// Destructor
		~CS7MPIMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			BYTE	m_bThisDrop;
			BYTE	m_bDrop;
			UINT	m_uMode;
			UINT	m_uTimeout;

			};

		// Data Members
		CContext * m_pCtx;
		
		BYTE	m_bLastDrop;
		BYTE	m_bRxBuff[256];
		BYTE	m_bTxBuff[256];
		UINT	m_uPtr;
		BYTE	m_bCheck;
		UINT	m_uError;
		UINT	m_uBurst;
		BYTE	m_bDrop;

		// Request Helpers
		BOOL	InitDrop(void);
		void	ResetLink(void);
		
		
		// 3964R Transport
		void	StartFrame(void);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddWordIntel(WORD wData);
		void	AddByte(BYTE bData);
		void	AddCtrl(BYTE bData);
		void    PutByte(BYTE b);
		BOOL	PutFrame(void);
		void	PutPDU(BOOL fAdd);
		BOOL	GetInitAck(void);
		BOOL	GetTermAck(void);
		BOOL	GetFrame(UINT uTimeout);
		
		// HMI Adapter Support
		BOOL	HmiInitLink(BYTE bDrop);
		BOOL	HmiStartNetwork(void);
		BOOL	HmiConnect(BYTE bDrop);
		BOOL	HmiDisconnect(void);
		void	HmiStartFrame(BYTE op, BYTE c, WORD n);
		BOOL	HmiTransact(void);
		void	HmiCheckError(void);

		// Read Handlers

		CCODE DoByteRead(PDWORD pData, UINT uCount);
		CCODE DoWordRead(PDWORD pData, UINT uCount);
		CCODE DoLongRead(PDWORD pData, UINT uCount);

		// Write Handlers

		CCODE DoByteWrite(PDWORD pData, UINT uCount);
		CCODE DoWordWrite(PDWORD pData, UINT uCount);
		CCODE DoLongWrite(PDWORD pData, UINT uCount); 

		// Helpers
		void GetCount(UINT uType, UINT& uCount);

	
	};

// End of File
