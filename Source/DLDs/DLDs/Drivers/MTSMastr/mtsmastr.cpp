
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "mtsmastr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA App Master/Network Slave Driver
//

// Instantiator

INSTANTIATE(CMTSMasterDriver);

// Constructor

CMTSMasterDriver::CMTSMasterDriver(void)
{
	m_Ident		= DRIVER_ID;

	CTEXT Dec[]	= "0123456789";

	m_pDec		= Dec;

	m_pReq		= &m_ReqFrame;

	m_dError	= 0;
	m_dNak		= 0;

	m_uBusy		= 0;
	m_uTimeout	= DEFAULTWAIT;

	m_fIDCheck	= FALSE;
	m_fWriteCommand	= FALSE;
	m_fInPing	= FALSE;

	m_uState	= WAITDROP;
	m_uDumpCount	= 0;
	m_uNoResponse	= 0;
	m_uPingRelease  = 0;
	}

// Destructor

CMTSMasterDriver::~CMTSMasterDriver(void)
{
	}

// Configuration

void MCALL CMTSMasterDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CMTSMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CMTSMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMTSMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CMTSMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_Drop		= GetByte(pData);

			m_pCtx->m_uRTDCt	= 0;

			m_pCtx->m_uWriteErrCt	= 0;

			m_pCtx->m_dGRCV		= 0;

			memset(m_pCtx->m_dPROD, 0, sizeof(m_pCtx->m_dPROD));
			memset(m_pCtx->m_dINTF, 0, sizeof(m_pCtx->m_dINTF));
			memset(m_pCtx->m_dTAVG, 0, sizeof(m_pCtx->m_dTAVG));

			memset(m_pCtx->m_dTID0, 0, sizeof(m_pCtx->m_dTID0));
			memset(m_pCtx->m_dTID1, 0, sizeof(m_pCtx->m_dTID1));
			memset(m_pCtx->m_dTID2, 0, sizeof(m_pCtx->m_dTID2));

			memset(m_pCtx->m_dFTMP, 0, sizeof(m_pCtx->m_dFTMP));

			memset(m_pCtx->m_dQFR,  0, sizeof(m_pCtx->m_dQFR ));

			memset(m_pCtx->m_dFZP,  0, sizeof(m_pCtx->m_dFZP ));

			memset(m_pCtx->m_dRTDP, 0, sizeof(m_pCtx->m_dRTDP));

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMTSMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		delete m_pReq;

		m_pReq = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMTSMasterDriver::Ping(void)
{
//**/	AfxTrace2("\r\n\nPING %2.2x Busy=%d\r\n\n", m_pCtx->m_Drop, m_uBusy);

	if( m_uTimeout && m_uBusy ) {

		if( ++m_uPingRelease > 1000 ) {

			m_uPingRelease = 0;

			m_uBusy = 0;

			return CCODE_ERROR;
			}

		return CCODE_ERROR | CCODE_BUSY;
		}

	m_uPingRelease  = 0;

	m_fWriteCommand = FALSE;

	m_fInPing       = TRUE;

	if( DoIDCheck() && HandleRTDCount() ) {

		return 1;
		}

	// reset gauge input buffer
	Sleep(1000);

	m_pData->ClearRx();

	m_bRx[0]   = 0;

	m_pData->Write(m_pCtx->m_Drop, FOREVER);
	m_pData->Write(PINGADD, FOREVER);

	m_uTimeout = 1000;

	ReceiveFrame(FALSE);

	m_fInPing  = FALSE;

	m_uBusy    = 0;

	return CCODE_ERROR;
	}

BOOL CMTSMasterDriver::HandleRTDCount(void)
{
	for( UINT uCount = 0; uCount < 3; uCount++ ) {

		CAddress Addr;

		DWORD Data[1];

		Addr.a.m_Table  = SPQFR;
		Addr.a.m_Offset	= 1;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;

		if( Read(Addr, Data, 2) == 2 ) { // RTD count needed for setting timeouts

			m_fInPing = FALSE;

			return TRUE;
			}
		}

	return FALSE;
	}

CCODE MCALL CMTSMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//***/	ShowItemInfo(Addr, uCount, TRUE);

	if( NoReadTransmit(Addr, pData, uCount) ) return 1;

	if( m_uNoResponse++ > 100 ) {

		m_uBusy       = 0;

		m_uNoResponse = 0;

		return CCODE_ERROR;
		}

	if( OnlyOneDatum(Addr.a.m_Table) ) uCount = 1;

	if( m_uBusy ) {

		if( !ThisDevBusy() ) return CMNOEX;

		m_uNoResponse = 0;

		if( m_uState == STXWAIT ) return HandleBusy(Addr, pData, uCount);

		else {
			m_uBusy  = 0;
			m_uState = WAITDROP;
			}
		}

	m_pReq->Addr.m_Ref = Addr.m_Ref;

	if( InitComm(AdjustCommand(TRUE), FALSE) ) {

//***/		AfxTrace0("\r\n"); ShowText(Addr); AfxTrace1("--- %d", uCount);

		m_uNoResponse = 0;

		m_uState = STXWAIT;

		return DoRead(Addr, pData, uCount);
		}

	m_uNoResponse = 0;

	m_uBusy = BOOL(m_uTimeout) ? m_pCtx->m_Drop : 0;

	return m_uBusy ? CCODE_BUSY : CCODE_ERROR;
	}

CCODE MCALL CMTSMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//***/	ShowItemInfo(Addr, uCount, FALSE);

	if( NoWriteTransmit(Addr, *pData, uCount) ) return 1;

	if( m_uBusy && !ThisDevBusy() ) return CMNOEX;

	m_pReq->Addr.m_Ref = Addr.m_Ref;

	if( InitComm(AdjustCommand(FALSE), TRUE) ) {

		m_uPtr          = 0;

		m_fWriteCommand = TRUE;

		switch( Addr.a.m_Type ) {

			case addrWordAsWord:	return DoWordWrite(*pData);

			case addrRealAsReal:	return DoRealWrite(*pData);

			default:		return CCODE_ERROR | CCODE_HARD;
			}
		}

	return ChkWriteError(FALSE);
	}

CCODE CMTSMasterDriver::HandleBusy(AREF dRef, PDWORD pData, UINT uCount)
{
	if( dRef.m_Ref == m_pReq->BusyAddr.m_Ref ) {

		m_uDumpCount = 0;

		return DoRead(dRef, pData, uCount);
		}

	UINT uTable = dRef.a.m_Table;

	GetPrevious(SetPrevious(uTable, dRef.a.m_Offset), pData, uCount);

	if( m_uDumpCount++ < 20 ) {

		return uCount;
		}

	if( m_uDumpCount < 40 ) {

		return CCODE_ERROR || CCODE_NO_RETRY;
		}

	m_uBusy      = 0;
	m_uDumpCount = 0;

	return CCODE_ERROR;
	}

// Read Functions

CCODE CMTSMasterDriver::DoRead(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !ReceiveFrame(FALSE) ) {

		if( m_uTimeout ) {

			GetPrevious(m_pCtx->m_pPrev, pData, uCount);

			m_uBusy = m_pCtx->m_Drop;
			}

		return CheckBusy();
		}

	if( Addr.a.m_Type == addrWordAsWord ) return DoWordRead(Addr, pData, uCount);

	return DoRealRead(pData, uCount);
	}

CCODE CMTSMasterDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPQFR ) return SaveCounts(pData, uCount) ? uCount : CCODE_ERROR;

	return GetFastTemp(Addr.a.m_Offset, pData, uCount) ? uCount : CCODE_ERROR;
	}

CCODE CMTSMasterDriver::DoRealRead(PDWORD pData, UINT uCount)
{
	GetRealData(pData, &uCount);

	PDWORD pPrev = m_pCtx->m_pPrev;

	for( UINT i = 0; i < uCount; i++, pData++, pPrev++ ) {

///***/		ShowReal(*pData, pPrev);

		*pPrev = *pData;
		}

	return uCount;
	}

void  CMTSMasterDriver::GetPrevious(PDWORD pPrev, PDWORD pData, UINT uCount)
{
	if( pPrev == NULL ) return;

	for( UINT i = 0; i < uCount; i++, pData++, pPrev++ ) {

///***/		ShowReal(*pPrev, pPrev);

		*pData = *pPrev;
		}
	} 

// Write Functions

CCODE CMTSMasterDriver::DoWordWrite(DWORD dData)
{
	AddCountsToWrite(dData);

	return ExecuteWrite(3);
	}

CCODE CMTSMasterDriver::DoRealWrite(DWORD dData)
{
	AddByte(SOH);

	if( m_pReq->Addr.a.m_Table != SPGRCV ) {

		AddByte(m_pDec[m_pReq->Addr.a.m_Offset]);

		AddByte(':');
		}

	UINT u = m_uPtr;
	
	return ExecuteWrite(u + AddRealData(dData));
	}

CCODE CMTSMasterDriver::ExecuteWrite(UINT uCount)
{
	AddByte(EOT);

	return ChkWriteError(SendData(uCount) && SendENQ());
	}

CCODE CMTSMasterDriver::ChkWriteError(BOOL fResponseStatus)
{
	m_uBusy = 0;

	if( fResponseStatus || ++m_pCtx->m_uWriteErrCt >= 2 ) {

		m_pCtx->m_uWriteErrCt = 0;
		}

	return 1;//CCODE_ERROR;
	}

void CMTSMasterDriver::AddCountsToWrite(DWORD dData)
{
	AddByte(SOH);

	AddByte(m_pDec[LOBYTE(dData >> 8)]);

	AddByte(':');

	AddByte(m_pDec[LOBYTE(dData) & 0xF]);
	}

UINT CMTSMasterDriver::AddRealData(DWORD dData)
{
	char c[12]  = {0}; // need room for -9999.99d + rounding digit + Null

	SPrintf(c, "%f", PassFloat(dData));

	return AdjustDP(c);
	}

UINT CMTSMasterDriver::AdjustDP(char * pc)
{
	UINT uLen	= strlen(pc);
	UINT uDPPos	= FindDecimalPoint(pc, uLen);
	UINT uFracLen	= 0;

	DWORD dOverflow	= 99;

	switch( m_pReq->Addr.a.m_Table ) { // point to rounding digit position

		case SPGRCV:
			uFracLen  = RESGRAD;
			dOverflow = 999999;
			break;

		case SPFZP:
		case SPWFZP:
			uFracLen  = RESFZP;
			dOverflow = 9999;
			break;

		case SPRTDP:
			uFracLen  = RESRTDP;
			break;
		}

	UINT uEndPos = uDPPos + uFracLen + 1;

	pc[uEndPos] = 0;	// terminate after rounding digit

	pc[uDPPos]       = '.';

	uLen = strlen(pc);

	for( UINT i = uLen; i <= uEndPos - 1; i++ ) {

		pc[i] = '0';	// Pad to resolution if necessary
		}

	DWORD dI  = ATOI((const char *)pc);

	DWORD dF  = ATOI((char *)&pc[uDPPos + 1]);

	dF       += 5; // round up

	if( dF > dOverflow ) {

		dF = 0;
		dI = dI < 0 ? dI - 1 : dI + 1;
		}

	else dF /= 10; // remove rounding digit

	char cI[8];

	SPrintf(cI, "%d.", dI);

	char cF[8];

	char FFormat[7] = {0};

	FFormat[0] = '%';
	SPrintf(&FFormat[1], "%1.1d.%1.1dd", uFracLen - 1, uFracLen - 1); // remove rounding digit

	SPrintf(cF, FFormat, dF);

	AddText((PCBYTE)cI);
	AddText((PCBYTE)cF);

	return strlen(cI) + strlen(cF);
	}

// Process Sequence

UINT CMTSMasterDriver::AdjustCommand(BOOL fIsRead)
{
	UINT uTable  = m_pReq->Addr.a.m_Table;
	UINT uOffset = m_pReq->Addr.a.m_Offset;

	switch( uTable ) {

		case SPPROD:
		case SPINTF:
			uTable += uOffset - 1;
			break;

		case SPTAVG:
			uTable += uOffset;
			break;

		case SPQFR:
		case SPGRCV:
		case SPFZP:
			if( !fIsRead ) uTable += 10;
			break;

		case SPRTDP:
			if( !fIsRead ) uTable += 11;
			break;
		}

	if( fIsRead ) m_pCtx->m_pPrev = SetPrevious(uTable, uOffset);

	return uTable;
	}

PDWORD CMTSMasterDriver::SetPrevious(UINT uTable, UINT uOffset)
{
	UINT u = uOffset - 1;

	switch( uTable ) {

		case SPPROD:
		case SPPRD2:
		case SPPRD3:	return &m_pCtx->m_dPROD[u];

		case SPINTF:
		case SPINF2:
		case SPINF3:	return &m_pCtx->m_dINTF[u];

		case SPTAVG:
		case SPTAV1:
		case SPTAV2:	return &m_pCtx->m_dTAVG[uOffset];

		case SPTID0:	return &m_pCtx->m_dTID0[u];
		case SPTID1:	return &m_pCtx->m_dTID1[u];
		case SPTID2:	return &m_pCtx->m_dTID2[u];

		case SPFTMP:	return &m_pCtx->m_dFTMP[uOffset];

		case SPQFR:	return &m_pCtx->m_dQFR[u];

		case SPGRCV:	return &m_pCtx->m_dGRCV;

		case SPFZP:	return &m_pCtx->m_dFZP[u];

		case SPRTDP:	return &m_pCtx->m_dFZP[u];
		}

	return NULL;
	}

BOOL CMTSMasterDriver::InitComm(UINT uCommand, BOOL fIsWrite)
{
	Sleep(TCOMMDELAY);

	m_uPtr          = 0;

	m_fWriteCommand = FALSE;

	m_uTimeout      = SetTimeoutValue(uCommand, FALSE);

	AddByte(m_pCtx->m_Drop);

	AddByte(uCommand);

	SendFrame();

	while( m_uTimeout ) {

		m_uState = WAITDROP;

		if( ReceiveFrame(FALSE) && m_bRx[0] == uCommand ) {

			m_uTimeout = SetTimeoutValue(uCommand, fIsWrite);

			m_uBusy    = 0;

			Sleep(50);

			return TRUE;
			}
		}

	m_uBusy = 0;

	return FALSE;
	}

BOOL CMTSMasterDriver::SendData(UINT uCount)
{
	m_uState = STXWAIT;

	if( Transact(TRUE) ) { // Send Write Data

		for( UINT i = 0; i < uCount - 1; i++ ) { // Verification

			if( m_bRx[i] != m_bTx[i+1] ) {

				return FALSE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CMTSMasterDriver::SendENQ(void)
{
	m_bRx[0]   = 0;

	m_uTimeout = DEFAULTWAIT;

	m_uState   = WAITACKNAK;

	m_pData->ClearRx();

	m_pData->Write(ENQ, FOREVER);

	return ReceiveFrame(TRUE) && m_bRx[0] == ACK;
	}

// Get Response Data

DWORD CMTSMasterDriver::GetFieldData(UINT uField, BOOL fIsReal)
{
	UINT uPos = 0;

	for( UINT i = 1; i < uField; i++ ) {

		FindColon(&uPos);
		}

	return uPos < 99 ? GetValue(uPos, fIsReal) : 0;
	}

void CMTSMasterDriver::FindColon(UINT *pCurrentPos)
{
	UINT i = *pCurrentPos;

	if( i == 99 ) return;

	for( i = *pCurrentPos; i < sizeof(m_bRx); i++ ) {

		switch( m_bRx[i] ) {

			case ':':
				*pCurrentPos = i + 1;
				return;

			case ETX:
			case ACK:
				*pCurrentPos = 99;
				return;
			}
		}

	*pCurrentPos = 99;
	}

BOOL CMTSMasterDriver::SaveCounts(PDWORD pData, UINT uCount)
{
	if( // 1 or 2 floats, 1 to 5 RTD's
		m_bRx[0]  < '1' ||
		m_bRx[0]  > '2' ||
		m_bRx[1] != ':' ||
		m_bRx[2]  < '1' ||
		m_bRx[2]  > '5'
		) {

		return FALSE;
		}

	UINT    uFloatCt = GetFieldData(1, FALSE);
	m_pCtx->m_uRTDCt = GetFieldData(2, FALSE);
	UINT uOffset     = m_pReq->Addr.a.m_Offset;

	UINT a[3];

	a[0] = uFloatCt;
	a[1] = m_pCtx->m_uRTDCt;
	a[2] = CombineCts(uFloatCt);

	for( UINT i = 0; i < uCount; i++ ) {

		pData[i] = a[uOffset + i - 1];
		}

	for( i = 0; i < 3; i++ ) m_pCtx->m_dQFR[i] = a[i];

	return TRUE;
	}

BOOL  CMTSMasterDriver::GetFastTemp(UINT uOffset, PDWORD pData, UINT uCount)
{
	for( UINT i = 0; i < uCount; i++ ) {

		pData[i] = GetFieldData(uOffset + i + 1, FALSE);

		m_pCtx->m_pPrev[uOffset + i] = pData[i];
		}

	return TRUE;
	}

void  CMTSMasterDriver::GetRealData(PDWORD pData, UINT *pCount)
{
	UINT uOffset = m_pReq->Addr.a.m_Offset;
	UINT uTable  = m_pReq->Addr.a.m_Table;

	BOOL fIndT   = IsIndTemp(uTable);

	UINT uItem   = OnlyOneDatum(uTable) ? 1 : 1 + NormalizeOffset(uTable, uOffset);

	for( UINT i = 0; i < *pCount; i++ ) {

		DWORD d = GetFieldData(i + uItem, TRUE);

		if( fIndT ) {

			UINT uErr = m_pCtx->m_uRTDErr[uOffset + i];

			if( uErr ) {

				d = uErr = RTDOORL || uErr == RTDSHORT ? RTDELO : RTDEHI;
				}
			}

		pData[i] = d;
		}
	}

DWORD CMTSMasterDriver::GetValue(UINT uRxPos, BOOL fIsReal)
{
	const char * p = (const char *)&m_bRx[uRxPos];

	return fIsReal ? ATOF(p) : ATOI(p);
	}

void CMTSMasterDriver::HandleError(UINT uPos, BOOL fNak)
{
	if( !uPos ) return;

	m_dError  = GetValue(uPos, FALSE);

	m_dError |= (AdjustCommand(!m_fWriteCommand)) << 16;

	if( fNak ) m_dNak = m_dError;
	}

void CMTSMasterDriver::CheckRTDError(UINT *pErrPos, UINT uItem)
{
	UINT uErrPos = *pErrPos;

	m_pCtx->m_uRTDErr[uItem] = uErrPos ? GetValue(uErrPos, FALSE) : 0;

	*pErrPos = 0;
	}

// ID check
BOOL CMTSMasterDriver::DoIDCheck(void)
{
	m_uPtr     = 0;

	m_fIDCheck = TRUE;

	m_uTimeout = DEFAULTWAIT;

	AddByte(m_pCtx->m_Drop);

	AddByte(PINGADD);

	m_uState = WAITDROP;

	if( Transact(FALSE) ) {

		m_uState = STXWAIT;

		if( ReceiveFrame(FALSE) ) {

			return m_bRx[0] == 'D' && m_bRx[1] == 'D' && m_bRx[2] == 'A';
			}
		}

	return FALSE;
	}

// Frame Building

void CMTSMasterDriver::AddByte(BYTE b)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr++] = b;
		}
	}

void CMTSMasterDriver::AddText(PCBYTE pbData)
{
	BYTE c;

	while( (c = *pbData) ) {

		AddByte(c);

		pbData++;
		}
	}

void CMTSMasterDriver::AddChecksum(void)
{
	BOOL fSOH  = FALSE;
	BOOL fDone = FALSE;

	UINT uSum  = 0;
	UINT i     = 0;

	while( !fDone ) {

		BYTE b = m_bTx[i];

		if( b == SOH )	fSOH = TRUE;

		if( fSOH )	uSum += b;

		if( b == EOT )	fDone = TRUE;

		i++;
		}

	char c[6] = {0};

	SPrintf(c, "%5.5u", 0x10000 - uSum);

	AddText((PCBYTE)c);
	}

// Frame Access

BOOL CMTSMasterDriver::Transact(BOOL fIsWrite)
{
	SendFrame();

	return ReceiveFrame(fIsWrite);
	}

void CMTSMasterDriver::SendFrame(void)
{
	memset(m_bRx, 0, 12);

	m_pData->ClearRx();

	Put();
	}

BOOL CMTSMasterDriver::ReceiveFrame(BOOL fIsWrite)
{
	BOOL	fNak	= FALSE;
	BOOL	fInd	= m_fIDCheck ? FALSE : IsIndTemp(m_pReq->Addr.a.m_Table);

	m_fIDCheck      = FALSE;

	BYTE	bData;

	UINT	uData;
	UINT	uPtr	= 0;
	UINT	uChkCt	= 0;
	UINT	uError	= 0;
	UINT	uIndT	= 1;
	UINT	uState  = m_uState;

	m_uBusy		= 0;

	SetTimer(m_uTimeout);

//**/	AfxTrace2("\r\nState=%d TO=%d\r\n", uState, m_uTimeout);

	while( GetTimer() ) {

		if( (uData = Get(m_fInPing || fIsWrite ? m_uTimeout : SHORTWAIT) ) == NOTHING ) {

			if( !m_fInPing ) {

				m_uTimeout = GetTimer();

				m_uBusy    = !fIsWrite && (uState == STXWAIT) ? m_pCtx->m_Drop : 0;
				}

			else {
				m_uTimeout = 0;
				m_uBusy    = 0;
				}

			return FALSE;
			}

		bData   = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);

		if( !bData ) {

			uPtr = 0;

			SetTimer(500);

			continue;
			}

		switch( uState ) {

			case WAITDROP:

				if( bData == m_pCtx->m_Drop ) uState = GETCOMMAND;

				else {
					uError = 1;

					if( bData == ETX ) uState = GETCHECKSUM;
					}

				break;

			case GETCOMMAND:

				m_uBusy  = 0;

				m_bRx[0] = bData;

				return TRUE;

			case STXWAIT:

				SetTimer(DEFAULTWAIT);

				if( bData == STX ) {

					uPtr    = 0;

					uState  = COLLECTDATA;

					m_uBusy = 0;
					}

				else return FALSE;

				break;

			case WAITACKNAK:

				m_bRx[uPtr++] = bData;

				if( bData == ACK ) return TRUE;

				uState = COLLECTDATA;

				fNak = TRUE;

				break;

			case COLLECTDATA:

				if( uPtr < sizeof(m_bRx) ) m_bRx[uPtr++] = bData;

				switch( bData ) {

					case 'E':
						uError = uPtr; // point to 1st digit of error code
						break;

					case ':':
						if( fInd ) {

							CheckRTDError(&uError, uIndT);

							uIndT++;
							}
						break;

					case ETX:
						if( fInd ) CheckRTDError(&uError, uIndT);

						else HandleError(uError, fNak);

						uState      = GETCHECKSUM;

						m_bRx[uPtr] = 0;

						break;
					}

				break;

			case GETCHECKSUM:

				uChkCt++;

				if( uChkCt > 4 ) {

					m_uTimeout = 0;

					m_uBusy    = 0;

//**/					AfxTrace1("*** %x ***\r\n", uError);

					return !uError;
					}

				break;
			}
		}

	Sleep(1000);

	m_uTimeout = 0;

	return FALSE;
	}

UINT CMTSMasterDriver::SetTimeoutValue(UINT uTable, BOOL fIsWrite)
{
	UINT c = DEFAULTWAIT;
	UINT r = max(1, m_pCtx->m_uRTDCt);
	UINT k = 0;

	switch( uTable ) {

		case SPPROD:
		case SPINTF:
			return TMOUTPI1;

		case SPPRD2:
		case SPINF2:
			return TMOUTPI2;
			break;

		case SPPRD3:
		case SPINF3:
			return TMOUTPI3;
			break;

		case SPTAVG:
			c = TMAC25;
			k = TMAK25;
			break;

		case SPTAV1:
			c = TMAC26;
			k = TMAK26;
			break;

		case SPTAV2:
			c = TMAC27;
			k = TMAK27;
			break;

		case SPTID0:
			c = TMIC28;
			k = TMIK28;
			break;

		case SPTID1:
			c = TMIC29;
			k = TMIK29;
			break;

		case SPTID2:
			c = TMIC30;
			k = TMIK30;
			break;

		case SPFTMP:
			c = TMFC37;
			k = TMFK37;
			break;

		case SPFZP:
			return 2000;

		default:
			if( fIsWrite ) c = 1200;
			break;
		}

	return TMCALC(c, r, k);
	}

// Port Access

void CMTSMasterDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

UINT CMTSMasterDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Access Cached Items

BOOL CMTSMasterDriver::NoReadTransmit(CAddress Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPERR:
			*pData = m_dError;
			return TRUE;

		case SPNAK:
			*pData = m_dNak;
			return TRUE;

		case SPRTDE:
			UINT n;
			for( n = 0; n < uCount; n++ ) {

				pData[n] = m_pCtx->m_uRTDErr[Addr.a.m_Offset + n];
				}

			return TRUE;

		case SPWFZP:
			*pData = RTDELO; // -9999
			return TRUE;

		case SPBUSY:
			m_dBusy = m_dBusy != 1 ? 1 : 0;
			*pData  = m_dBusy;
			return TRUE;
		}

	return FALSE;
	}

BOOL CMTSMasterDriver::NoWriteTransmit(CAddress Addr, DWORD dData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPERR:
			m_dError = 0;
			return TRUE;

		case SPNAK:
			m_dNak   = 0;
			return TRUE;

		case SPPROD:
		case SPINTF:
		case SPTAVG:
		case SPTID0:
		case SPTID1:
		case SPTID2:
		case SPFTMP:
			return TRUE;

		case SPRTDE:
			UINT n;
			for( n = 0; n < uCount; n++ ) {

				m_pCtx->m_uRTDErr[Addr.a.m_Offset + n] = 0;
				}

			return TRUE;

		case SPFZP:
		case SPWFZP:
			char s[10];

			SPrintf(s, "%f", PassFloat(dData & 0x7FFFFFFF)); // absolute value

			UINT	uLim;

			UINT	uVal;

			uLim	= dData & 0x80000000 ? 1000 : 10000; // data -999.999 to 9999.999

			uVal	= ATOI(s);

			return	uVal >= uLim; // don't write

		case SPQFR:
			if( Addr.a.m_Offset == 3 ) {

				UINT uF = HIBYTE(LOWORD(dData));
				UINT uR = LOBYTE(LOWORD(dData));

				return (uF < 1) || (uF > 2) || (uR < 1) || (uR > 5);
				}

			return TRUE;
		}

	return FALSE;
	}

// Helpers

BOOL CMTSMasterDriver::OnlyOneDatum(UINT uTable)
{
	return IsProdOrIntf(uTable) || IsAvgTemp(uTable) || uTable == SPGRCV;
	}

BOOL CMTSMasterDriver::IsProdOrIntf(UINT uTable)
{
	return uTable >= SPPROD && uTable <= SPINF3;
	}

BOOL CMTSMasterDriver::IsAvgTemp(UINT uTable)
{
	return uTable >= SPTAVG && uTable <= SPTAV2;
	}

BOOL CMTSMasterDriver::IsIndTemp(UINT uTable)
{
	return uTable >= SPTID0 && uTable <= SPTID2;
	}

CCODE CMTSMasterDriver::CheckBusy(void)
{
	if( m_uBusy ) {

		m_pReq->BusyAddr.m_Ref = m_pReq->Addr.m_Ref;

		return CCODE_ERROR | CCODE_BUSY;
		}

	return CCODE_ERROR;
 	}

BOOL  CMTSMasterDriver::ThisDevBusy(void)
{
	return m_uBusy == m_pCtx->m_Drop;
	}

DWORD CMTSMasterDriver::CombineCts(UINT uFloatCt)
{
	return DWORD((uFloatCt << 8) + m_pCtx->m_uRTDCt);
	}

UINT CMTSMasterDriver::FindDecimalPoint(char *pc, UINT uLen)
{
	for( UINT i = 0; i < uLen; i++ ) {

		if( pc[i] == '.' ) {

			return i;
			}
		}

	return uLen;
	}

UINT CMTSMasterDriver::NormalizeOffset(UINT uTable, UINT uOffset)
{
	switch( uTable ) {

		case SPTID0:
		case SPTID1:
		case SPTID2:
		case SPFZP:
		case SPRTDP:	return uOffset-1;
		}

	return uOffset;
	}

// Debug
//***/void CMTSMasterDriver::ShowText(AREF Addr)
//***/{
//***/	BYTE a = m_pCtx->m_Drop == 192 ? 'C' : 'c';
//***/	BYTE b = m_pCtx->m_Drop == 192 ? '0' : '1';
//***/	BYTE c = '?'; // command
//***/	BYTE d = 0;
//***/	BYTE e = m_pDec[Addr.a.m_Offset%10];
//***/
//***/	switch( Addr.a.m_Table ) {
//***/
//***/		case SPPROD:
//***/		case SPPRD2:
//***/		case SPPRD3:	c = 'P'; break;
//***/		case SPINTF:
//***/		case SPINF2:
//***/		case SPINF3:	c = 'I'; break;
//***/		case SPTAVG:
//***/		case SPTAV1:
//***/		case SPTAV2:	c = 'A'; break;
//***/		case SPTID0:	c = 'T'; d = '0'; break;
//***/		case SPTID1:	c = 'T'; d = '1'; break;
//***/		case SPTID2:	c = 'T'; d = '2'; break;
//***/		case SPFTMP:	c = 'F'; break;
//***/		case SPQFR:	c = 'R'; break;
//***/		case SPGRCV:	c = 'G'; break;
//***/		case SPFZP:	c = 'Z'; break;
//***/		case SPWFZP:	c = 'W'; break;
//***/		case SPRTDP:	c = 'D'; break;
//***/		case SPBUSY:	c = 'B'; break;
//***/		}
//***/
//***/	AfxTrace3("*** %c%c_%c", a, b, c);
//***/	if( d ) AfxTrace1("%c ", d);
//***/	AfxTrace1("%c", e);
//***/	}

//***/void CMTSMasterDriver::ShowItemInfo(AREF Addr, UINT uCount, BOOL fIsRead)
//***/{
//***/	if( !fIsRead ) AfxTrace0("\n");
//***/
//***/	AfxTrace2("\r\n%c** Drop=%d ", (fIsRead ? 'R' : 'W'), m_pCtx->m_Drop);
//***/
//***/	char c = 'L';
//***/
//***/	switch( Addr.a.m_Type ) {
//***/
//***/		case addrBitAsBit:	c = 'B'; break;
//***/		case addrByteAsByte:	c = 'Y'; break;
//***/		case addrWordAsWord:	c = 'W'; break;
//***/		case addrRealAsReal:	c = 'R'; break;
//***/		}
//***/
//***/	AfxTrace3("Tab=%d Off=%d Type=%c ", Addr.a.m_Table, Addr.a.m_Offset, c);
//***/	AfxTrace1("Ct=%d ", uCount);
//***/	ShowText(Addr);
//***/	if( !fIsRead ) AfxTrace0("**WRITE**\r\n");
//***/	}

//***/void CMTSMasterDriver::ShowReal(DWORD d, PDWORD p)
//***/{
//***/	char a[12] = {0};
//***/	SPrintf( a, " %f", PassFloat(d) );
//***/	for( UINT k = 0; a[k]; k++ ) AfxTrace1("%c", a[k]);
//***/	AfxTrace2(" %8.8lx %8.8lx", p, *p);
//***/	}

// End of File
