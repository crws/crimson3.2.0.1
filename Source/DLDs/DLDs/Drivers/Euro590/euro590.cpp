
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "euro590.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 590 Ascii Driver
//

// Instantiator

INSTANTIATE(CEurotherm590Driver);

// Constructor

CEurotherm590Driver::CEurotherm590Driver(void)
{
	m_Ident	    = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Destructor

CEurotherm590Driver::~CEurotherm590Driver(void)
{
	}

// Configuration

void MCALL CEurotherm590Driver::Load(LPCBYTE pData)
{
	}
	
void MCALL CEurotherm590Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CEurotherm590Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEurotherm590Driver::Open(void)
{
	}

// Device

CCODE MCALL CEurotherm590Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop[0]           = m_pHex[GetByte(pData)];
			m_pCtx->m_bDrop[1]           = m_pHex[GetByte(pData)];

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEurotherm590Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEurotherm590Driver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPACE_II;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CEurotherm590Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPACE_REAL:
			return DoRealRead(Addr.a.m_Offset, pData);

		case SPACE_WORD:
			return DoWordRead(Addr.a.m_Offset, Addr.a.m_Table, pData);

		case SPACE_BOOL:
			return DoByteRead(Addr.a.m_Offset, pData);

		case SPACE_ERROR:
		case SPACE_II:
		case SPACE_V0:
		case SPACE_V1:
		case SPACE_CW:
		case SPACE_SR:
		case SPACE_CS:
		case SPACE_SS:
			return DoSpecialRead( Addr.a.m_Table, pData );
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CEurotherm590Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPACE_REAL:
			return DoRealWrite(Addr.a.m_Offset, *pData);

		case SPACE_WORD:
			return DoWordWrite(Addr.a.m_Offset, Addr.a.m_Table, *pData);

		case SPACE_BOOL:
			return DoByteWrite(Addr.a.m_Offset, Addr.a.m_Table, *pData);

		case SPACE_ERROR:
		case SPACE_II:
		case SPACE_V0:
		case SPACE_V1:
		case SPACE_CW:
		case SPACE_SR:
		case SPACE_CS:
		case SPACE_SS:
			return DoSpecialWrite( Addr.a.m_Table, *pData );
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

// Port Access

UINT CEurotherm590Driver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CEurotherm590Driver::StartFrame( void )
{
	m_uPtr = 0;
	
	AddByte( EOT );

	AddByte( m_pCtx->m_bDrop[0] );
	AddByte( m_pCtx->m_bDrop[0] );

	AddByte( m_pCtx->m_bDrop[1] );
	AddByte( m_pCtx->m_bDrop[1] );
	}

void CEurotherm590Driver::EndFrame(BOOL fIsWrite)
{
	if( !fIsWrite ) {

		AddByte( ENQ );
		}

	else {

		AddByte( ETX );

		AddByte( m_bCheck );
		}
	}

void CEurotherm590Driver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {
	
		m_bTx[m_uPtr] = bData;

		m_bCheck ^= bData;
		
		m_uPtr++;
		}
	}

void CEurotherm590Driver::PutHexInteger(DWORD dData, DWORD dFactor)
{
	BYTE b;
	BOOL fFirst = FALSE;

	AddByte('>');

	while( dFactor ) {

		b = m_pHex[(dData / dFactor) % 16];

		if( b != '0' || fFirst ) {

			AddByte( m_pHex[(dData / dFactor) % 16] );

			fFirst = TRUE;
			}
		
		dFactor /= 16;
		}

	if( !fFirst ) AddByte('0');
	}

void CEurotherm590Driver::PutDecInteger(DWORD dData, DWORD dFactor)
{
	BYTE b;
	BOOL fFirst = FALSE;

	if( dData & 0x8000000 ) {

		AddByte('-');

		dData ^= 0xFFFFFFFF;

		dData += 1;
		}

	while( dFactor ) {

		b = m_pHex[(dData / dFactor) % 10];

		if( b != '0' || fFirst ) {

			AddByte( m_pHex[(dData / dFactor) % 10] );

			fFirst = TRUE;
			}
		
		dFactor /= 10;
		}

	if( !fFirst ) AddByte('0');
	}
		
// Transport Layer

BOOL CEurotherm590Driver::AsciiTx(void)
{
	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );

	return TRUE;
	}

UINT CEurotherm590Driver::AsciiRx(BOOL fIsWrite)
{
	UINT uState;

	UINT uPtr   = 0;

	UINT uTimer = 0;
	
	BYTE bCheck = 0;

	UINT uError = 0;

	uState = fIsWrite ? 10 : 0;

//**/	AfxTrace0("\r\n");

	SetTimer(1000);
	
	while( (uTimer = GetTimer()) ) {

		UINT uByte;
		
		if( (uByte = RxByte(uTimer)) == NOTHING ) {
			
			continue;
			}

		bCheck ^= uByte;

//**/	AfxTrace1("<%2.2x>", uByte );

		switch( uState ) {
		
			case 0:
				if( uByte == STX ) {

					uState = 1;

					bCheck = 0;
					}

				else {
					if( uByte == EOT ) {

						m_bRx[0] = EOT;

						return 1;
						}
					}

				break;
				
			case 1:
				if( uByte != m_bTx[5] )

					uError = 1;

				uState = 2;

				break;
				
			case 2:
				if( uByte != m_bTx[6] )

					uError = 1;

				uState = 3;

				uPtr = 0;

				break;

			case 3:
				m_bRx[uPtr++] = uByte;

				if( uPtr > sizeof(m_bRx) )
					return FALSE;

				if( uByte == ETX ) {

					uState = 4;

					m_bRx[uPtr-1] = 0;
					}

				break;

			case 4:

				if( uError )

					return 2;

				return( (bCheck & 0x7F) == 0 );

			case 10:
				if( uByte == ACK )

					return 0;

				else {
					m_bRx[0] = uByte;

					return 1;
					}
				break;
			}
		}
		
	return 2;
	}

BOOL CEurotherm590Driver::Transact(BOOL fIsWrite)
{
	EndFrame( fIsWrite );

	if( AsciiTx() ) {

		if( m_pCtx->m_bDrop[0] == 0x7F || m_pCtx->m_bDrop[1] == 0x7F ) {

			m_pData->Read(0); // ensure transmit of full frame

			return TRUE; // Broadcast
			}
			
		return AsciiRx(fIsWrite) > 1 ? FALSE : TRUE;
		}
		
	return FALSE;
	}

// Frame Formatting

void CEurotherm590Driver::PutIdentifier(UINT uOffset, UINT uTable, BOOL fIsWrite)
{
	BYTE bHi;

	BYTE bLo;

	StartFrame();

	if( fIsWrite )

		AddByte( STX );

	m_bCheck = 0;

	GetIDs( uOffset, uTable, &bHi, &bLo );

	AddByte( bHi );

	AddByte( bLo );
	}

// Read Handlers

CCODE CEurotherm590Driver::DoRealRead(UINT Addr, PDWORD pData)
{
	PutIdentifier( Addr, SPACE_REAL, NOTWRITE );
	
	if( Transact(NOTWRITE) ) {

		if( m_bRx[0] == EOT ) {

			pData[0] = 0;

			return 1;
			}

		pData[0] = ATOF( (const char * )&m_bRx[0] );

		return 1;
		}
		
	return CCODE_ERROR;
	}

CCODE CEurotherm590Driver::DoWordRead(UINT Addr, UINT uTable, PDWORD pData)
{
	char s1[32];
	char * s2 = s1;
	UINT uStart = 0;
	DWORD dResult = 0;
	BOOL fNeg = FALSE;

	PutIdentifier( Addr, uTable, NOTWRITE );
	
	if( Transact(NOTWRITE) ) {

		if( m_bRx[0] == EOT ) {

			pData[0] = 0;

			return 1;
			}

		if( m_bRx[0] != '>' ) { // do Base 10 or string

			if( m_bRx[0] == '-' ) {

				fNeg = TRUE;

				uStart = 1;
				}

			if( IsADigit( m_bRx[uStart], NOTHEX ) ) {

				dResult = strtoul( (char *)(&m_bRx[uStart]), &s2, 10 );

				pData[0] = !fNeg ? dResult : -dResult;
				}

			else { // may be string character

				dResult = 0;

				for( UINT i = 0; i < 4; i++ ) {

					if( m_bRx[i] ) {

						dResult <<= 8;

						dResult += m_bRx[i];
						}

					else break;
					}

				pData[0] = dResult;
				}

			return 1;
			}

		if( IsADigit( m_bRx[1], ISHEX ) ) {

			pData[0] = strtoul( (char *)(&m_bRx[1]), &s2, 16 );

			return 1;
			}

		pData[0] = 0;

		return 1;
		}
		
	return CCODE_ERROR;
	}

CCODE CEurotherm590Driver::DoByteRead(UINT Addr, PDWORD pData)
{
	PutIdentifier( Addr, SPACE_BOOL, NOTWRITE );
	
	if( Transact(NOTWRITE) ) {

		if( m_bRx[0] == EOT ) {

			pData[0] = 0;

			return 1;
			}

		UINT u = (m_bRx[0] == '>') ? 2 : 0;
			
		pData[0] = m_bRx[u] != '0' ? 1L : 0L;

		return 1;
		}
		
	return CCODE_ERROR;
	}

CCODE CEurotherm590Driver::DoSpecialRead(UINT uTable, PDWORD pData)
{
	switch( uTable ) {

		case SPACE_CW:
		case SPACE_CS:
			pData[0] = 0;
			return 1;
		}

	return DoWordRead( 0, uTable, pData );
	}

// Write Handlers

CCODE CEurotherm590Driver::DoRealWrite(UINT Addr, DWORD dData)
{
	DWORD dExponent = 0;
	DWORD dResult = 0;
	DWORD dInt = 0;
	DWORD dFrac = 0;
	UINT  uDP = 0;
	UINT  uPtr = 0;
	char  cStart[32] = {0};
	char  cFrac[32]  = {0};
	BOOL  fNeg = FALSE;

	PutIdentifier( Addr, SPACE_REAL, ISWRITE );

	dResult = dData;

	if( dResult & 0x80000000 ) {

		fNeg = TRUE;

		dResult &= 0x7FFFFFFF;
		}

	if ( dResult ) {

		 // protect against invalid Values from Hex writes
		dExponent = dResult & 0x7F800000;

		dExponent >>= 23;

		if( dExponent < 113 ) { // if dResult < .0001

			dResult = 0;

			fNeg = FALSE;
			}

		if( dExponent > 157 ) {

			dResult = 0x4EFFFFFF; // 2147483647
			}

		if( fNeg )
			AddByte('-');

		// Create floating point string

		SPrintf(cStart, "%f", PassFloat(dResult) );

		// determine decimal point position for possible rounding
		for( uPtr = 0; uPtr < strlen(cStart); uPtr++ ) {

			if( cStart[uPtr] == '.' ) {

				uDP = uPtr;

				break;
				}
			}

		dInt = ATOI( cStart );  // integer portion

		cFrac[0] = '.'; // default, all reals get a dp, even if no frac.

		if( uDP ) {

			dFrac = ATOI( &cStart[uDP+1] );

			if( dFrac ) {

				uPtr = strlen(cStart) - uDP - 1; // number of dp's in text

				if( uPtr > 4 ) { // round value to 4 places

					while ( uPtr > 4 ) {

						dFrac += 5;

						dFrac /= 10;

						uPtr--;
						}

					if( dFrac >= 10000 )

						dInt += 1;

					dFrac %= 10000;
					}
				}

			if( dFrac ) {

				SPrintf( cFrac, ".%4.4ld", dFrac );
				}
			}

		uPtr = strlen(cFrac) - 1;

		while ( cFrac[uPtr] == '0' ) {

			cFrac[uPtr] = 0;

			uPtr--;
			}

		if( dInt )

			SPrintf( cStart, "%ld%s", dInt, cFrac );

		else
			SPrintf( cStart, "0%s", cFrac );
		}

	else {
		SPrintf( cStart, "0." );
		}

	for( uPtr = 0; uPtr < strlen(cStart); uPtr++ )
		
		AddByte( cStart[uPtr] );

	if( Transact(ISWRITE) ) {
			
		return 1;
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE CEurotherm590Driver::DoWordWrite(UINT Addr, UINT uTable, DWORD dData)
{
	PutIdentifier( Addr, uTable, ISWRITE );

	PutDecInteger( dData, 1000000000 );

	if( Transact(ISWRITE) ) {
			
		return 1;
		}

	else {
		if( m_bRx[0] == 0x15 ) { // try hex write

			PutIdentifier( Addr, uTable, ISWRITE );

			PutHexInteger( dData, 0x10000000 );

			if( Transact(ISWRITE) ) {

				return 1;
				}
			}
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE CEurotherm590Driver::DoByteWrite(UINT Addr, UINT uTable, DWORD dData)
{
	PutIdentifier( Addr, uTable, ISWRITE );

	AddByte( '>' );

	AddByte( '0' );

	AddByte( dData ? '1' : '0' );

	if( Transact(ISWRITE) ) {
			
		return 1;
		}

	else {
		if( m_bRx[0] == 0x15 ) { // try integer write

			PutIdentifier( Addr, uTable, ISWRITE );

			AddByte( dData ? '1' : '0' );

			AddByte( '.' );

			if( Transact(ISWRITE) ) {

				return 1;
				}
			}
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE CEurotherm590Driver::DoSpecialWrite(UINT uTable, DWORD dData)
{
	switch( uTable ) {

		case SPACE_CW:
		case SPACE_CS:

			PutIdentifier( 0, uTable, ISWRITE );

			PutHexInteger( dData, 0x1000 );

			if( Transact( ISWRITE ) ) {

				return 1;
				}
			break;

		case SPACE_ERROR:

			return DoByteWrite( 0, SPACE_ERROR, 1 );
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

// Helpers

void CEurotherm590Driver::GetIDs( UINT uOffset, UINT uTable, PBYTE pHi, PBYTE pLo )
{
	switch( uTable ) {

		case SPACE_BOOL:
		case SPACE_WORD:
		case SPACE_REAL:
// New formula provided September 2008

			BOOL fLow;
			UINT uHi;
			UINT uLo;

			fLow = uOffset < 936;

			uHi  = fLow ? (uOffset + 360) / 36 : (uOffset - 936) / 26;
			uLo  = fLow ? (uOffset + 360) % 36 : (uOffset - 936) % 26;

			if( fLow ) {

				*pHi = BYTE(uHi > 9 ? 'a' + uHi - 10 : '0' + uHi);
				*pLo = BYTE(uLo > 9 ? 'a' + uLo - 10 : '0' + uLo);
				}

			else {
				*pHi = BYTE('a' + uHi);
				*pLo = BYTE('A' + uLo);
				}
/*
			if( uOffset < 1296 ) {

				UINT uHi = uOffset / 36;

				UINT uLo = uOffset % 36;

				*pHi = LOBYTE( uHi + 'a' );

				*pLo = LOBYTE( uLo < 10 ? uLo + '0' : uLo + 'a' - 10 );
				}

			else {
				*pHi = LOBYTE( 'a' + ((uOffset-1296) / 26) );

				*pLo = LOBYTE( 'A' + ((uOffset-1296) % 26) );
				}
*/
 			break;

		case SPACE_ERROR:
			*pHi = 'E';
			*pLo = 'E';
			break;

		case SPACE_II:
			*pHi = 'I';
			*pLo = 'I';
			break;

		case SPACE_V0:
			*pHi = 'V';
			*pLo = '0';
			break;

		case SPACE_V1:
			*pHi = 'V';
			*pLo = '1';
			break;

		case SPACE_CW:
			*pHi = '!';
			*pLo = '1';
			break;

		case SPACE_SR:
			*pHi = '!';
			*pLo = '2';
			break;

		case SPACE_CS:
			*pHi = '!';
			*pLo = '3';
			break;

		case SPACE_SS:
			*pHi = '!';
			*pLo = '4';
			break;
		}
	}

BOOL CEurotherm590Driver::IsADigit( BYTE b, BOOL fIsHex )
{
	if( b >= '0' && b <= '9' )
		return TRUE;

	if( b == '-' )
		return !fIsHex;

	if( b >= 'A' && b <= 'F' )
		return fIsHex;

	if( b >= 'a' && b <= 'f' )
		return fIsHex;

	return FALSE;
	}

// End of File
