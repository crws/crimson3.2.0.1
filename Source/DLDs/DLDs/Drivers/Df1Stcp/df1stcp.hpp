
#include "../Df1Shared/df1m.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ENET Header
//

#pragma pack(1)

struct NETHEADER
{
	BYTE	bMode;
	BYTE	bSub;
	U2	wCount;
	U4	dwID;
	U4	dwStat;
	BYTE	bData[16];
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// DF1 Functions
//

#define	A1_READ		0xA1
#define	A9_WRITE	0xA9
#define AB_MASK_WRITE	0xAB

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 TCP/IP Slave Driver
//

class CDf1TCPSlave : public CSlaveDriver
{
	public:
		// Constructor
		CDf1TCPSlave(void);

		// Destructor
		~CDf1TCPSlave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Entry Points
		DEFMETH(void) Service(void);
		
	protected:
		// Socket Context
		struct SOCKET
		{
			ISocket	* m_pSocket;
			UINT	  m_uPtr;
			PBYTE	  m_pData;
			BOOL	  m_fBusy;
			UINT	  m_uTime;
			};

		// Data Members
		UINT	m_uDevice;
		UINT    m_uPort;
		UINT    m_uCount;
		UINT	m_uRestrict;
		DWORD	m_SecData;
		DWORD	m_SecMask;
		SOCKET *m_pSock;
		UINT	m_ID;
		PBYTE   m_pOct;

		// Implementation
		void OpenSocket(UINT n, SOCKET &Sock);
		void CloseSocket(SOCKET &Sock);
		void ReadData(SOCKET &Sock);
		BOOL CheckAccess(SOCKET &Sock, BOOL fWrite);
		
		BOOL DoFrame(SOCKET &Sock, PBYTE pData, UINT uSize);
		BOOL DoConnect(SOCKET &Sock, PBYTE pData);
		BOOL DoCSP(SOCKET &Sock, PBYTE pData);
		BOOL DoCmd6(SOCKET &Sock, PBYTE pData);
		BOOL DoCmdF(SOCKET &Sock, PBYTE pData);
		BOOL DoWhoIs(SOCKET &Sock, PBYTE pData);
		BOOL DoWhoName(SOCKET &Sock, PBYTE pData);
		BOOL DoBaseRead(SOCKET &Sock, PBYTE pData);
		BOOL DoBaseWrite(SOCKET &Sock, PBYTE pData);
		BOOL DoRead(SOCKET &Sock, PBYTE pData);
		BOOL DoWrite(SOCKET &Sock, PBYTE pData);
		BOOL DoMaskWrite(SOCKET &Sock, PBYTE pData);
		
		void SetHeader(NETHEADER &Reply, BYTE bSub, UINT uCount, PBYTE pData);
		void SetHeader(DF1PCCCHEAD &Rep, DF1PCCCHEAD &Req, BOOL fError);
		void SetData(CBuffer * pBuff, UINT uType, UINT uCount, PDWORD pWork);
		void GetData(DF1PCCCHEAD &Req, UINT uType, UINT uCount, PDWORD pWork);
		void GetDataWithMask(PBYTE pData, UINT &uIndex, UINT uType, UINT uCount, PDWORD pWork, WORD wMask);
		void SetStringData(CBuffer * pBuff, UINT uCount, PDWORD pWork);
		void GetStringData(DF1PCCCHEAD &Req, UINT uCount, PDWORD pWork);

		UINT FindSpace(UINT uCode);
		UINT FindType(UINT uSpace);
		UINT FindCount(UINT uBytes, UINT uType);
		UINT FindOffset(PBYTE pData, UINT uSpace, UINT &uTable, UINT &uIndex);
		UINT FindTable(UINT uTable, UINT uSpace);
		UINT FindField(PBYTE pByte, UINT &uIndex);
		WORD FindBitMask(PBYTE pByte, UINT&uIndex);
		UINT FindBytes(UINT uType, UINT uCount);
		
		// Helpers

		BOOL IsRequest(PBYTE pData);
		BOOL IsTriple(UINT uSpace);
		BOOL IsFileIO(UINT uSpace, UINT uTable);
		BOOL IsBaseIO(UINT uSpace);
		BOOL IsString(UINT uType);
		BOOL IsLong(UINT uType);
		BOOL HasExtraField(UINT uFunc);
	};

// End of File
