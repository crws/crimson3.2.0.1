
#include "intern.hpp"

#include "df1stcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Base Class
//

#include "../Df1Shared/df1m.cpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 TCP/IP Slave Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CDf1TCPSlave);

// Constructor

CDf1TCPSlave::CDf1TCPSlave(void)
{
	m_Ident  = DRIVER_ID;

	m_uPort  = 2222;

	m_uCount = 1;

	m_pOct	 = NULL;
	}

// Destructor

CDf1TCPSlave::~CDf1TCPSlave(void)
{
	delete [] m_pSock;
	}

// Configuration

void MCALL CDf1TCPSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uDevice   = GetByte(pData);

		m_uPort     = GetWord(pData);

		m_uCount    = GetWord(pData);

		m_uRestrict = GetByte(pData);

		m_SecMask   = GetAddr(pData);

		m_SecData   = GetAddr(pData);

		m_ID	    = 0x00010100;
		}

	m_pSock = new SOCKET [ m_uCount ];
	}

// Management

void MCALL CDf1TCPSlave::Attach(IPortObject *pPort)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		SOCKET &Sock = m_pSock[n];

		OpenSocket(n, Sock);
		}
	}

void MCALL CDf1TCPSlave::Detach(void)
{
	BOOL fHit = FALSE;

	for( UINT p = 0; p < 2; p++ ) {

		for( UINT n = 0; n < m_uCount; n++ ) {

			SOCKET &Sock = m_pSock[n];

			if( Sock.m_pSocket ) {

				if( !p ) {

					Sock.m_pSocket->Close();

					fHit = TRUE;
					}
				else {
					Sock.m_pSocket->Abort();

					Sock.m_pSocket->Release();
					
					Sock.m_pSocket = NULL;
					}
				}

			if( !p && !fHit ) {

				break;
				}

			Sleep(100);
			}
		}

	delete m_pOct;

	m_pOct = NULL;
	}

// Entry Point

void MCALL CDf1TCPSlave::Service(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		SOCKET &Sock = m_pSock[n];

		if( Sock.m_pSocket ) {

			UINT Phase;

			Sock.m_pSocket->GetPhase(Phase);

			switch( Phase ) {

				case PHASE_OPEN:

					if( !Sock.m_fBusy ) {

						if( !CheckAccess(Sock, FALSE) ) {

							Sock.m_pSocket->Close();

							break;
							}

						Sock.m_uTime = GetTickCount();

						Sock.m_fBusy = TRUE;
						}

					ReadData(Sock);

					break;

				case PHASE_CLOSING:

					Sock.m_pSocket->Close();

					break;

				case PHASE_ERROR:

					CloseSocket(Sock);

					OpenSocket(n, Sock);

					break;
				}
			}
		else
			OpenSocket(n, Sock);
		}

	ForceSleep(20);
	}

// Implementation

void CDf1TCPSlave::OpenSocket(UINT n, SOCKET &Sock)
{
	Sock.m_pSocket = CreateSocket(IP_TCP);

	if( Sock.m_pSocket ) {

		Sock.m_pSocket->Listen(m_uPort);

		Sock.m_pData = new BYTE [ 300 ];

		Sock.m_uPtr  = 0;

		Sock.m_fBusy = FALSE;
		}
	}

void CDf1TCPSlave::CloseSocket(SOCKET &Sock)
{
	Sock.m_pSocket->Release();

	if( Sock.m_pData ) {

		delete Sock.m_pData;

		Sock.m_pData = NULL;
		}

	Sock.m_pSocket = NULL;
	}

void CDf1TCPSlave::ReadData(SOCKET &Sock)
{
	if( !m_pOct ) {

		IPADDR IP;
			
		Sock.m_pSocket->GetLocal(IP);
								    
		m_pOct = new BYTE[sizeof(m_pOct)];
			
		memcpy(m_pOct, &IP, sizeof(m_pOct));
		}
		
	UINT uSize = 300 - Sock.m_uPtr;

	Sock.m_pSocket->Recv(Sock.m_pData + Sock.m_uPtr, uSize);

	if( uSize ) {

		Sock.m_uPtr += uSize;

		while( Sock.m_uPtr >= 3 ) {

			UINT uTotal = 28 + ( Sock.m_pData[2] << 8 ) + Sock.m_pData[3];

			if( Sock.m_uPtr >= uTotal ) {

				if( !DoFrame(Sock, Sock.m_pData, uTotal) ) {

					Sock.m_pSocket->Close();

					return;
					}

				if( Sock.m_uPtr -= uTotal ) {

					for( UINT n = 0; n < Sock.m_uPtr; n++ ) {

						Sock.m_pData[n] = Sock.m_pData[uTotal++];
						}

					continue;
					} 
				}
			
			break;
			}

		Sock.m_uTime = GetTickCount();

		return;
		}

	if( UINT(GetTickCount() - Sock.m_uTime) >= ToTicks(10000) ) {

		Sock.m_pSocket->Close();

		return;
		}
	}

BOOL CDf1TCPSlave::CheckAccess(SOCKET &Sock, BOOL fWrite)
{
	DWORD IP;

	if( Sock.m_pSocket->GetRemote((IPADDR &) IP) == S_OK ) {

		if( m_uRestrict == 0 ) {

			return TRUE;
			}

		if( m_uRestrict == 2 || fWrite == TRUE ) {

			if( (IP & m_SecMask) == (m_SecData & m_SecMask) ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CDf1TCPSlave::DoFrame(SOCKET &Sock, PBYTE pData, UINT uSize)
{
	if( IsRequest(pData) ) {

		BYTE bSub = pData[1];

		switch( bSub ) {

			case 1:  return DoConnect(Sock, pData);	
		       
			case 7:  return DoCSP(Sock, pData);		
			}
		} 

	return TRUE;
	}

BOOL CDf1TCPSlave::DoConnect(SOCKET &Sock, PBYTE pData)
{
	UINT uSize = sizeof(NETHEADER);
	
	CBuffer *pBuff = CreateBuffer(uSize, TRUE);

	if( pBuff ) {

		NETHEADER &Reply = (NETHEADER &) pBuff->AddTail(uSize)[0];

		SetHeader(Reply, 1, 0, pData);

		if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

			return TRUE;
			}

		BuffRelease(pBuff);
		}

	return FALSE;
	}

BOOL CDf1TCPSlave::DoCSP(SOCKET &Sock, PBYTE pData)
{
	UINT uHeader = sizeof(NETHEADER);

	UINT uCmd    = pData[uHeader + 4];

	switch( uCmd ) {

		case 0x6:	return DoCmd6(Sock, pData);

		case 0xF:	return DoCmdF(Sock, pData);

		}

	return FALSE;
	}

BOOL CDf1TCPSlave::DoCmd6(SOCKET &Sock, PBYTE pData)
{
	UINT uFunc = pData[sizeof(NETHEADER) + 8];

	switch( uFunc ) {

		case 3:		return DoWhoIs(Sock, pData);
		}
	
	return FALSE;
	}

BOOL CDf1TCPSlave::DoCmdF(SOCKET &Sock, PBYTE pData)
{
	UINT uFunc = pData[sizeof(NETHEADER) + 8];

	switch( uFunc ) {

		case BASE_READ:		return DoBaseRead(Sock, pData);
		case BASE_WRITE:	return DoBaseWrite(Sock, pData);
			
		case A1_READ:		return DoRead(Sock, pData);
		case A9_WRITE:		return DoWrite(Sock, pData);

		case AB_MASK_WRITE:	return DoMaskWrite(Sock, pData);

		}
	
	return FALSE;
	}

BOOL CDf1TCPSlave::DoWhoIs(SOCKET &Sock, PBYTE pData)
{
	UINT uSize = sizeof(NETHEADER) + 4 + sizeof(DF1PCCCHEAD) + 24;

	CBuffer *pBuff = CreateBuffer(uSize, TRUE);

	if( pBuff ) {

		NETHEADER &Head = (NETHEADER &) pBuff->AddTail(sizeof(NETHEADER))[0];

		SetHeader(Head, 7, uSize - sizeof(NETHEADER), pData);

		PBYTE pPad  = PBYTE(pBuff->AddTail(4));

		BYTE Pad [] = { 0, 5, 0, 0 };

		memcpy(pPad, Pad, sizeof(Pad));

		DF1PCCCHEAD &Rep = (DF1PCCCHEAD &) pBuff->AddTail(sizeof(DF1PCCCHEAD))[0];

		DF1PCCCHEAD &Req = (DF1PCCCHEAD &) pData[sizeof(NETHEADER) + 4];
			
		SetHeader(Rep, Req, FALSE);
		
		PBYTE pData = (PBYTE) pBuff->AddTail(24);

		*pData++ = 0x00;
		*pData++ = 0xEE;
		*pData++ = 0x4A;
		*pData++ = 0x77;
		*pData++ = 0x45;
		*pData++ = 0x35;
		*pData++ = 0x2F;
		*pData++ = 0x30;
		*pData++ = 0x35;
		*pData++ = 0x20;
		*pData++ = 0x20;
		*pData++ = 0x20;
		*pData++ = 0x20;
		*pData++ = 0x20;
		*pData++ = 0x20;
		*pData++ = 0x20;
		*pData++ = 0x00;
		*pData++ = 0x00;
		*pData++ = 0x91;
		*pData++ = 0x00;
		*pData++ = 0x64;
		*pData++ = 0xF2;
		*pData++ = 0x10;
		*pData++ = 0xFE;

		if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

			return TRUE;
			}

		BuffRelease(pBuff);
		}
	
	return FALSE;
	}

BOOL CDf1TCPSlave::DoWhoName(SOCKET &Sock, PBYTE pData)
{
	UINT uSize = sizeof(NETHEADER) + 4 + sizeof(DF1PCCCHEAD) + 16;

	CBuffer *pBuff = CreateBuffer(uSize, TRUE);

	if( pBuff ) {

		NETHEADER &Head = (NETHEADER &) pBuff->AddTail(sizeof(NETHEADER))[0];

		SetHeader(Head, 7, uSize - sizeof(NETHEADER), pData);

		PBYTE pPad  = PBYTE(pBuff->AddTail(4));

		BYTE Pad [] = { 0, 5, 0, 0 };

		memcpy(pPad, Pad, sizeof(Pad));

		DF1PCCCHEAD &Rep = (DF1PCCCHEAD &) pBuff->AddTail(sizeof(DF1PCCCHEAD))[0];

		DF1PCCCHEAD &Req = (DF1PCCCHEAD &) pData[sizeof(NETHEADER) + 4];
			
		SetHeader(Rep, Req, FALSE);
		
		PBYTE pData = (PBYTE) pBuff->AddTail(16);

		memset(pData, 0, 16);

		*pData++ = 'P';
		*pData++ = 'A';
		*pData++ = 'N';
		*pData++ = 'E';
		*pData++ = 'L';
		*pData++ =  m_pOct[3] / 100 + 0x30;
		*pData++ =  m_pOct[3] % 100 / 10  + 0x30;
		*pData++ =  m_pOct[3] % 10  + 0x30;
					
		if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

			return TRUE;
			}

		BuffRelease(pBuff);
		}
	
	return FALSE;
	}

BOOL CDf1TCPSlave::DoBaseRead(SOCKET &Sock, PBYTE pData)
{	
	DF1PCCCHEAD &Req = (DF1PCCCHEAD &) pData[sizeof(NETHEADER) + 4];
       
	CAddress Addr;

	UINT uIndex     = 2;

	UINT uTable     = FindField(Req.bData, uIndex);

	Addr.a.m_Extra  = FindSpace(Req.bData[3]);

	if( Addr.a.m_Extra ) {

		uIndex++;
		
		Addr.a.m_Offset = FindOffset(Req.bData, Addr.a.m_Extra, uTable, uIndex);

		Addr.a.m_Type   = FindType(Addr.a.m_Extra);

		if( Addr.a.m_Type ) {

			UINT uCount = FindCount(Req.bData[1], Addr.a.m_Type);

			if( IsString(Addr.a.m_Type) ) {

				Addr.a.m_Offset *= 80;

				MakeMin(uCount, 80);
				}

			PDWORD pWork	= new DWORD[uCount];

			Addr.a.m_Table = FindTable(uTable, Addr.a.m_Extra);

			BOOL fSuccess = COMMS_SUCCESS(Read(Addr, pWork, uCount));

			UINT uSize = sizeof(NETHEADER) + 4 + sizeof(DF1PCCCHEAD) + Req.bData[1];

			CBuffer *pBuff = CreateBuffer(uSize, TRUE);

			if( pBuff ) {

				NETHEADER &Head = (NETHEADER &) pBuff->AddTail(sizeof(NETHEADER))[0];

				SetHeader(Head, 7, uSize - sizeof(NETHEADER), pData);

				PBYTE pPad  = PBYTE(pBuff->AddTail(4));

				BYTE Pad [] = { 5, 0, 0, 0 };

				memcpy(pPad, Pad, sizeof(Pad));

				DF1PCCCHEAD &Rep = (DF1PCCCHEAD &) pBuff->AddTail(sizeof(DF1PCCCHEAD))[0];
			
				SetHeader(Rep, Req, !fSuccess);

				if( fSuccess ) {

					SetData(pBuff, Addr.a.m_Type, uCount, pWork);
					}

				if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

					delete pWork;
					
					return TRUE;
					}

				BuffRelease(pBuff);
				}

			delete pWork;
			}
		} 

	return FALSE;
	}

BOOL CDf1TCPSlave::DoBaseWrite(SOCKET &Sock, PBYTE pData)
{
	DF1PCCCHEAD &Req = (DF1PCCCHEAD &) pData[sizeof(NETHEADER) + 4];
       
	CAddress Addr;

	UINT uIndex     = 2;

	UINT uTable     = FindField(Req.bData, uIndex);
	
	Addr.a.m_Extra  = FindSpace(Req.bData[3]);

	if( Addr.a.m_Extra ) {

		uIndex++;
	
		Addr.a.m_Offset = FindOffset(Req.bData, Addr.a.m_Extra, uTable, uIndex);
	
		Addr.a.m_Type   = FindType(Addr.a.m_Extra);

		if( Addr.a.m_Type ) {

			UINT uCount = FindCount(Req.bData[1], Addr.a.m_Type);

			if( IsString(Addr.a.m_Type) ) {

				Addr.a.m_Offset *= 80;

				MakeMin(uCount, 80);
				}

			PDWORD pWork	= new DWORD[uCount];

			memset(pWork, 0, uCount * 4);

			if( IsString(Addr.a.m_Type) ) {

				uCount = uCount / 4;
				}
			
			GetData(Req, Addr.a.m_Type, uCount, pWork);

			Addr.a.m_Table = FindTable(uTable, Addr.a.m_Extra);

			BOOL fSuccess = COMMS_SUCCESS(Write(Addr, pWork, uCount));

			UINT uSize = sizeof(NETHEADER) + 4 + sizeof(DF1PCCCHEAD);

			CBuffer *pBuff = CreateBuffer(uSize, TRUE);

			if( pBuff ) {

				NETHEADER &Head = (NETHEADER &) pBuff->AddTail(sizeof(NETHEADER))[0];

				SetHeader(Head, 7, uSize - sizeof(NETHEADER), pData);

				PBYTE pPad  = PBYTE(pBuff->AddTail(4));

				BYTE Pad [] = { 5, 0, 0, 0 };

				memcpy(pPad, Pad, sizeof(Pad));

				DF1PCCCHEAD &Rep = (DF1PCCCHEAD &) pBuff->AddTail(sizeof(DF1PCCCHEAD))[0];
			
				SetHeader(Rep, Req, !fSuccess);
			
				if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

					delete pWork;

					return TRUE;
					}

				BuffRelease(pBuff);
				}   
			
			delete pWork;
			}
		} 

	return FALSE;
	} 

BOOL CDf1TCPSlave::DoRead(SOCKET &Sock, PBYTE pData)
{
	DF1PCCCHEAD &Req = (DF1PCCCHEAD &) pData[sizeof(NETHEADER) + 4];
       
	CAddress Addr;

	UINT uTable     = Req.bData[2];
	
	if( uTable == 0 ) {

		return DoWhoName(Sock, pData);
		}

	return DoBaseRead(Sock, pData);
	}

BOOL CDf1TCPSlave::DoWrite(SOCKET &Sock, PBYTE pData)
{
 	return DoBaseWrite(Sock, pData);
	}

BOOL CDf1TCPSlave::DoMaskWrite(SOCKET &Sock, PBYTE pData)
{
	DF1PCCCHEAD &Req = (DF1PCCCHEAD &) pData[sizeof(NETHEADER) + 4];
       
	CAddress Addr;

	UINT uIndex     = 2;

	UINT uTable     = FindField(Req.bData, uIndex);
	
	Addr.a.m_Extra  = FindSpace(Req.bData[3]);

	if( Addr.a.m_Extra ) {

		uIndex++;
	
		Addr.a.m_Offset = FindOffset(Req.bData, Addr.a.m_Extra, uTable, uIndex);
	
		Addr.a.m_Type   = FindType(Addr.a.m_Extra);

		if( Addr.a.m_Type ) {

			UINT uCount = FindCount(Req.bData[1], Addr.a.m_Type);

			UINT uSub   = FindField(Req.bData, uIndex);

			Addr.a.m_Offset += uSub;

			WORD wMask  = FindBitMask(Req.bData, uIndex);

			if( IsString(Addr.a.m_Type) ) {

				Addr.a.m_Offset *= 80;

				MakeMin(uCount, 80);
				}

			PDWORD pWork	= new DWORD[uCount];

			memset(pWork, 0, uCount * 4);					   

			if( IsString(Addr.a.m_Type) ) {

				uCount = uCount / 4;
				}

			Addr.a.m_Table = FindTable(uTable, Addr.a.m_Extra);

			UINT Count = uCount;

			if( IsTriple(Addr.a.m_Extra) ) {

				Count = uCount + 2 - uSub;
				}

			BOOL fSuccess = COMMS_SUCCESS(Read(Addr, pWork, Count));
		
			GetDataWithMask(Req.bData, uIndex, Addr.a.m_Type, uCount, pWork, wMask);

			if( fSuccess )  {

				fSuccess = COMMS_SUCCESS(Write(Addr, pWork, Count));
				}

			UINT uSize = sizeof(NETHEADER) + 4 + sizeof(DF1PCCCHEAD);

			CBuffer *pBuff = CreateBuffer(uSize, TRUE);

			if( pBuff ) {

				NETHEADER &Head = (NETHEADER &) pBuff->AddTail(sizeof(NETHEADER))[0];

				SetHeader(Head, 7, uSize - sizeof(NETHEADER), pData);

				PBYTE pPad  = PBYTE(pBuff->AddTail(4));

				BYTE Pad [] = { 5, 0, 0, 0 };

				memcpy(pPad, Pad, sizeof(Pad));

				DF1PCCCHEAD &Rep = (DF1PCCCHEAD &) pBuff->AddTail(sizeof(DF1PCCCHEAD))[0];
			
				SetHeader(Rep, Req, !fSuccess);
			
				if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

					delete pWork;

					return TRUE;
					}

				BuffRelease(pBuff);
				}   
				
			delete pWork;
			}
		} 

	return FALSE;
	}

void CDf1TCPSlave::SetHeader(NETHEADER &Reply, BYTE bSub, UINT uCount, PBYTE pData)
{
	Reply.bMode  = 2;

	Reply.bSub   = bSub;

	Reply.wCount = SHORT(MotorToHost(WORD(uCount)));

	Reply.dwID   = MotorToHost(DWORD(m_ID));

	Reply.dwStat = 0;

	memcpy(Reply.bData - 4, pData + sizeof(NETHEADER) - 16 - 4, sizeof(Reply.bData) + 4);
	}

void CDf1TCPSlave::SetHeader(DF1PCCCHEAD &Reply, DF1PCCCHEAD &Request, BOOL fError)
{
	Reply.bComm   = Request.bComm | 0x40;
	
	Reply.bStatus = fError ? 0x50 : 0;
	
	Reply.wTrans  = Request.wTrans;
	}

void CDf1TCPSlave::SetData(CBuffer * pBuff, UINT uType, UINT uCount, PDWORD pWork)
{
	if( IsString(uType) ) {

		SetStringData(pBuff, uCount, pWork);

		return;
		}

	PBYTE pData = PBYTE(pBuff->AddTail(uCount * (IsLong(uType) ? 4 : 2)));

	for( UINT u = 0, i = 0; u < uCount; u++, i++ ) {

		pData[i] = LOBYTE(LOWORD(pWork[u]));

		if( uType == addrByteAsByte ) {

			continue;
			}

		i++;

		pData[i] = HIBYTE(LOWORD(pWork[u]));

		if( uType == addrWordAsWord ) {

			continue;
			} 
		
		i++;

		pData[i] = LOBYTE(HIWORD(pWork[u]));

		i++;

		pData[i] = HIBYTE(HIWORD(pWork[u]));
		}
       	}

void CDf1TCPSlave::GetData(DF1PCCCHEAD &Req, UINT uType, UINT uCount, PDWORD pWork)
{
	if( IsString(uType) ) {

		GetStringData(Req, uCount, pWork);

		return;
		}

	UINT uBytes = Req.bData[1];

	PBYTE pData = PBYTE(alloca(uBytes));

	UINT uPlus  = (Req.bData[4] == 0xFF ? 7 : 5);

	if( HasExtraField(Req.bData[0]) ) {

		uPlus++;
		}

	memcpy(pData, Req.bData + uPlus, uBytes);
	
	for( UINT u = 0, i = 0; u < uCount; u++, i++ ) {

		pWork[u] = pData[i];

		if( uType == addrByteAsByte ) {

			if( pWork[u] > 0x7F ) {

				pWork[u] |= 0xFFFFFF00;
				}
	    
			continue;
			}

		i++;

		pWork[u] |= ( pData[i] << 8 );

		if( uType == addrWordAsWord ) {

			if( pWork[u] > 0x7FFF ) {

				pWork[u] |= 0xFFFF0000;
				}
    
			continue;
			}
	      
		i++;

		pWork[u] |= ( pData[i] << 16 );

		i++;

		pWork[u] |= ( pData[i] << 24 );
		} 
	}

void CDf1TCPSlave::GetDataWithMask(PBYTE pData, UINT &uIndex, UINT uType, UINT uCount, PDWORD pWork, WORD wMask)
{
	UINT uBytes  = FindBytes(uType, uCount);

	PBYTE pBytes = PBYTE(alloca(uBytes));

	memcpy(pBytes, pData + uIndex, uBytes);

	for( UINT u = 0, i = 0; u < uCount; u++, i++ ) {

		WORD x  = IntelToHost(PU2(pBytes)[i]) & wMask;

		pWork[u]  &= ~wMask;

		pWork[u]  |= x;

		if( uType == addrWordAsWord ) {

			continue;
			}

		i++;

		x = IntelToHost(PU2(pBytes)[i]) & wMask;

		pWork[u] &= ~(wMask << 16);

		pWork[u] |=  (x << 16);
		} 
	}

void CDf1TCPSlave::SetStringData(CBuffer * pBuff, UINT uCount, PDWORD pWork)
{
	uCount = uCount / 4;
	
	UINT uChars = 0;

	BOOL fExit  = FALSE;

	for( UINT x = 0; x < uCount; x++ ) {

		DWORD dwChars = pWork[x];

		for( UINT z = 0; z < 4; z++ ) {

			if( (dwChars & 0xFF) != 0 ) {

				uChars++;

				continue;
				}

			fExit = TRUE;
			
			break;
			}

		if( fExit ) {

			break;
			}
		}
	       	
	PBYTE pData = PBYTE(pBuff->AddTail(2));

	pData[0] = uChars;

	pData[1] = 0;

	pData = PBYTE(pBuff->AddTail(uCount * 4));

	for( UINT u = 0, i = 0; u < uCount; u++ ) {

		pData[i++] = LOBYTE(HIWORD(pWork[u]));

		pData[i++] = HIBYTE(HIWORD(pWork[u]));

		pData[i++] = LOBYTE(LOWORD(pWork[u]));

		pData[i++] = HIBYTE(LOWORD(pWork[u]));
		}
	}

void CDf1TCPSlave::GetStringData(DF1PCCCHEAD &Req, UINT uCount, PDWORD pWork)
{
	UINT uBytes = Req.bData[1];
	
	PBYTE pData    = PBYTE(alloca(uBytes));

	memcpy(pData, Req.bData + 10, uBytes);

	UINT uChars = pData[0];

	MakeMin(uCount, pData[0]);

	for( UINT u = 0, i = 2; u < uCount; u++ ) {

		pWork[u]  = pData[i++] << 16;

		pWork[u] |= pData[i++] << 24;

		pWork[u] |= pData[i++];

		pWork[u] |= pData[i++] << 8;
		}  
	}

UINT CDf1TCPSlave::FindSpace(UINT uCode)
{
	switch( uCode ) {

		case 0x8B:	return 1;
		case 0x8C:	return 2;
		case 0x84:	return 3;
		case 0x85:	return 4;
		case 0x89:	return 5;
		case 0x86:	return 6;
		case 0x87:	return 7;
		case 0x8A:	return 8;
		case 0x91:	return 9;
		case 0x8D:	return 10;
		}

	return 0;
	}

UINT CDf1TCPSlave::FindType(UINT uSpace)
{
	switch( uSpace ) {

		case 1:		
		case 2:
		case 3:
		case 4:
		case 5:		return addrWordAsWord;

		case 6:
		case 7:		return addrWordAsWord; 

		case 8:		return addrLongAsReal;
		case 9:		return addrLongAsLong;

		case 10:	return addrByteAsLong;
	     	}

	return 0;
	}

UINT CDf1TCPSlave::FindCount(UINT uBytes, UINT uType)
{
	switch( uType ) {

		case addrWordAsWord:	return uBytes / 2;
		case addrLongAsLong:
		case addrLongAsReal:	return uBytes / 4;

		}

	return uBytes;
	}

UINT CDf1TCPSlave::FindOffset(PBYTE pData, UINT uSpace, UINT &uTable, UINT &uIndex)
{
	UINT uLo = ( pData[4] > 0xFE ) ? uIndex + 1 : uIndex;

	UINT uHi = ( pData[4] > 0xFE ) ? uIndex + 2 : 0;

	if( HasExtraField(pData[0]) && uHi == 0 ) {

		uHi = uIndex + 1;
		}

	if( IsTriple(uSpace) ) {

		if( uHi > 0 ) {

			uIndex += 3;
			
			return pData[uLo] * 3 + pData[uHi];
			}

		uIndex++;

		return pData[uLo] * 3;
		}

	if( IsFileIO(uSpace, uTable) ) {

		uIndex++;

		if( uHi == 0 ) {

			return pData[uLo];
			}
		
		uTable = pData[uLo];

		return pData[uHi];
		}

	if( uHi > 0 ) {

		uIndex += 3;

		return MAKEWORD(pData[uLo], pData[uHi]);
		}

	uIndex++;

	return pData[uLo];
	}

UINT CDf1TCPSlave::FindTable(UINT uTable, UINT uSpace)
{
	if( uSpace == 2 && uTable == 0 ) {

		return 0xE0;
		}

	if( uSpace == 1 && uTable == 0 ) {

		return 0xE0;
		}

	if( uSpace == 3 ) {

		return 0xE0;
		}
	
	return uTable;
	}

UINT CDf1TCPSlave::FindField(PBYTE pData, UINT &uIndex)
{
	if( pData[uIndex] == 0xFF ) {

		uIndex += 3;

		return PU2(pData + uIndex - 2)[0];
		}

	uIndex++;

	return pData[uIndex - 1];
	}

WORD CDf1TCPSlave::FindBitMask(PBYTE pData, UINT &uIndex)
{  
	WORD x = PU2(pData + uIndex)[0];
	
	uIndex += 2;
	
	return SHORT(IntelToHost(x));
	}

UINT CDf1TCPSlave::FindBytes(UINT uType, UINT uCount)
{
	UINT uBytes = 0;

	switch( uType ) {

		case addrByteAsByte:	uBytes = uCount;	break;
		case addrWordAsWord:	uBytes = uCount * 2;	break;
		case addrByteAsLong:
		case addrWordAsLong:
		case addrLongAsLong:
		case addrLongAsReal:	uBytes = uCount * 4;	break;
		}

	return uBytes;
	}

// Helpers

BOOL CDf1TCPSlave::IsRequest(PBYTE pData)
{
	return pData[0] == 1;
	}

BOOL CDf1TCPSlave::IsTriple(UINT uSpace)
{
	if( uSpace == 6 || uSpace == 7 ) {

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CDf1TCPSlave::IsFileIO(UINT uSpace, UINT uTable)
{
	if( IsBaseIO(uSpace) ) {
 
		if( uTable != 0xE0 ) {

			return TRUE;
			}
		}
	
	return FALSE;
        }

BOOL CDf1TCPSlave::IsBaseIO(UINT uSpace)
{
	return ( uSpace > 0 && uSpace <= 2 );
	}

BOOL CDf1TCPSlave::IsString(UINT uType)
{
	return uType == addrByteAsLong;
	}

BOOL CDf1TCPSlave::IsLong(UINT uType)
{
	switch( uType ) {

		case addrByteAsLong:
		case addrWordAsLong:
		case addrLongAsLong:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CDf1TCPSlave::HasExtraField(UINT uFunc)
{
	switch( uFunc ) {

		case BASE_READ:
		case BASE_WRITE:

			return TRUE;
		}

	return FALSE;
	}

// End of File
