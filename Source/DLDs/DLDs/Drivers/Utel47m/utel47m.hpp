
//////////////////////////////////////////////////////////////////////////
//
// Unitelway Master Driver

// *** This driver is the version that was active for Config Ver 187,
// which worked with the TSX47, while subsequent versions did not.

// DATA LINK TRANSACTIONS

#define	DLL_NULL	0x00
#define	DLL_ACK		0x01
#define	DLL_NAK		0x02
#define	DLL_EOT		0x03
#define	DLL_ENQ		0x04
#define	DLL_FRAME	0x05
#define	DLL_ERROR	0x06

// DATA LINK TYPES

#define	TYPE_POLL	0x00
#define	TYPE_REPLY	0x01
#define	TYPE_SCAN	0x02

// PROTOCOL CONSTANTS

#define	DLL_POLL_TIMEOUT	500
#define	DLL_REPLY_TIMEOUT	1000
#define	DLL_GAP_TIMEOUT		(500 / DLL_PAUSE)
#define	DLL_PAUSE		10
#define	NET_POLL_GAP		50
#define	RETRIES			5

#define OPRDI	0
#define OPRDS	1
#define	OPWRI	0x10
#define	OPWRS	0x11
#define	OPREAD	0x36
#define	OPWRITE	0x37

// Space Identifiers
#define	MWORD	1  // internal words
#define	SWORD	2  // system words
#define	CWORD	3  // constant words
#define	MLONG	4  // internal longs
#define	SLONG	5  // system longs
#define	CLONG	6  // constant longs
#define	MREAL	7  // internal bits
#define	MBYTE	8  // internal bytes
#define	SBYTE	9  // system bytes
#define	MBIT	10 // internal bits
#define	SBIT	11 // system bits
#define CREAL	12 // constant reals

// Data Segments
#define	BITSEG	0x64
#define	MEMSEG	0x68
#define	CONSEG	0x69
#define	SYSSEG	0x6A
#define	SYSBSEG	0 // system byte segment

// Types NOTE:types other than 7, 8 don't work with test unit
#define	MEMBIT	5
#define	MEMBYTE	6
#define	MEMWORD	7
#define	MEMLONG	8
#define	MEMREAL	10
#define	CONWORD	7
#define	CONLONG	8
#define	SYSBIT	6
#define	SYSBYTE 1
#define	SYSWORD	7
#define	SYSLONG	8

class CUnitel47mDriver : public CMasterDriver
{
	public:
		// Constructor
		CUnitel47mDriver(void);

		// Destructor
		~CUnitel47mDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			UINT m_Drop;
			UINT m_Category;
			};
		CContext *	m_pCtx;

		// Data Members
		BYTE		m_bTx[256];
		BYTE		m_bRx[256];
		UINT		m_uPtr;

		// Hex Lookup
		LPCTXT m_pHex;
		char cFormat[32];
	
		// Opcode Handlers
		BOOL BitRead  (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL ByteRead (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL WordRead (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL LongRead (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL BitWrite (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL ByteWrite(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL WordWrite(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL LongWrite(AREF Addr, PDWORD pData, UINT * pCount);
		
		// Frame Building
		void PutShortHeader(UINT uOffset, BYTE bOp);
		void PutWordsHeader(UINT uOffset, UINT uCount, BYTE bOp, BYTE bSeg, BYTE bType);
		void AddByte(BYTE bData);
		void AddData(UINT uData);

		// Transport Layer
		BOOL Transact(BOOL fWait);
		BOOL Send(BOOL fWait);
		void SendFrame(void);
		void SendPoll(BYTE bStation);
		
		// Port Access
		void Put(BYTE b);
		UINT Get(void);

		// Helpers
		UINT Receive(UINT uType);
		void Clear(BYTE bStation);
		DWORD GetData( UINT uPos, UINT uByteCount );
	};

// End of File
