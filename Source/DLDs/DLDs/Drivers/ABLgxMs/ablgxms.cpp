
#include "intern.hpp"

#include "ablgxms.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Base Class
//

#include "../Df1Shared/df1ms.cpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Logix5000 Driver - Serial Master
//

// Instantiator

INSTANTIATE(CABLogix5SerialMaster);

// Constructor

CABLogix5SerialMaster::CABLogix5SerialMaster(void)
{
	m_Ident = DRIVER_ID;
	}

// Device

CCODE MCALL CABLogix5SerialMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_uHeader = headBase;

			m_pCtx->m_bDevice = GetByte(pData);

			m_pCtx->m_bDest   = GetByte(pData);

			m_pCtx->m_uTime1  = GetWord(pData);

			m_pCtx->m_uTime2  = GetWord(pData);

			m_pCtx->m_wTrans  = WORD(RAND(m_pCtx->m_bDevice + 1));

			m_pCtx->m_uCache  = 0;

			LoadTagNames(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}
		
		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CABLogix5SerialMaster::DeviceClose(BOOL fPersist)
{	
	if( !fPersist ) {

		delete m_pBase->m_pTags;

		m_pBase->m_pTags = NULL;

		delete m_pCtx;

		m_pCtx  = NULL;
		
		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CDF1SerialMaster::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CABLogix5SerialMaster::Ping(void)
{
	if( CheckLink() ) {

		NewFrame(0x06, 0x00);

		PCTXT pData = "PING!!";

		UINT  uSize = strlen(pData);

		AddText(pData);

		if( Transact() ) {

			PBYTE pReply = FindDataField(m_bRxBuff, TRUE);

			if( !memcmp(pReply, pData, uSize) ) {

				return CCODE_SUCCESS;
				}
			}
		}

	return CCODE_ERROR; 
	}

CCODE MCALL CABLogix5SerialMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		MakeMin(uCount, 48);

		NewFrame(0x0F, PLC5_READ);

		AddWord(0x00);

		AddWord(uCount);		

		AddAddress(Addr);

		AddWord(uCount);

		if( Transact() ) {

			PBYTE pReply = FindDataField(m_bRxBuff, FALSE);

			if( pReply ) {

				switch( Addr.a.m_Type ) {
					
					case addrByteAsByte:
						CopyByte(pReply, pData, uCount);
						break;
					
					case addrWordAsWord:
						CopyWord(pReply, pData, uCount);
						break;
					
					case addrLongAsLong:
					case addrRealAsReal:
						CopyLong(pReply, pData, uCount);
						break;

					default:
						return CCODE_ERROR | CCODE_NO_DATA;
					}

				return uCount;
				}
			}

		if( m_bError == 0xF0 ) {

			return CCODE_ERROR | CCODE_NO_DATA;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CABLogix5SerialMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		MakeMin(uCount, 48);

		NewFrame(0x0F, PLC5_WRITE);

		AddWord(0x00);

		AddWord(uCount);

		AddAddress(Addr);

		AddType(Addr, uCount);
			
		switch( Addr.a.m_Type ) {
			
			case addrByteAsByte:
				AddByte(pData, uCount);
				break;
			
			case addrWordAsWord:
				AddWord(pData, uCount);
				break;
			
			case addrLongAsLong:
			case addrRealAsReal:
				AddLong(pData, uCount);
				break;

			default:
				return CCODE_ERROR | CCODE_HARD;
			}

		if( Transact() ) {

			return uCount;
			}
		}
	
	return CCODE_ERROR;
	}

// Frame Building

void CABLogix5SerialMaster::AddAddress(AREF Addr)
{
	char s[64];

	if( Addr.a.m_Table < addrNamed ) {
		
		SPrintf(s, "%s[%d]", 
			   GetTagName(Addr.a.m_Table), 
			   Addr.a.m_Offset
			   );
		}
	else
		SPrintf(s, "%s", 
			   GetTagName(Addr.a.m_Offset)
			   );

	AddByte(0);

	AddText(s);
	
	AddByte(0);
	}

void CABLogix5SerialMaster::AddType(AREF Addr, UINT uCount)
{
	if( Addr.a.m_Table < addrNamed ) {

		AddByte(0x99); 

		AddByte(0x09);
		
		switch( Addr.a.m_Type ) {
			
			case addrByteAsByte:
				AddByte(1 + 1 * uCount);
				break;
			
			case addrWordAsWord:
				AddByte(1 + 2 * uCount);
				break;
			
			case addrLongAsLong:
				AddByte(1 + 4 * uCount);
				break;
			
			case addrRealAsReal:
				AddByte(2 + 4 * uCount);
				break;
			}
		}

	switch( Addr.a.m_Type ) {
		
		case addrByteAsByte:
			AddByte(0x41);		
			break;
		
		case addrWordAsWord:
			AddByte(0x42);
			break;
		
		case addrLongAsLong:
			AddByte(0x44);
			break;
		
		case addrRealAsReal:
			AddByte(0x94);
			AddByte(0x08);
			break;
		}
	}

// Tag Name Support

void CABLogix5SerialMaster::LoadTagNames(PCBYTE pData)
{
	UINT uCount       = GetWord(pData);

	m_pBase->m_uCount = uCount;

	m_pBase->m_pTags  = new PTXT [ uCount ];

	for( UINT n = 0; n < uCount; n ++ ) {
		
		m_pBase->m_pTags[n] = GetString(pData);
		}	
	}

// Text Loading

PTXT CABLogix5SerialMaster::GetString(PCBYTE &pData)
{
	WORD wLength = GetWord(pData);

	PTXT String = PTXT(Alloc(wLength + 1));

	memset(String, 0, wLength + 1);

	for( UINT n = 0; n < wLength; n++ ) {

		String[n] = GetByte(pData);
		}

	return String;
	}

// End of File
