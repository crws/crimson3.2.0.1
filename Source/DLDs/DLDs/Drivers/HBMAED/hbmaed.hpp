
//////////////////////////////////////////////////////////////////////////
//
// HBM AED Driver
//

#define HBMAED_ID	0x4012

// Response Types
#define	RESPONSEFAIL	0
#define	NORESPONSE	1
#define	READRESPONSE	2
#define	WRITERESPONSE	3
#define	LONGRESPONSE	4
#define	ASCIIRESPONSE	5
#define	MULTIRESPONSE	6
#define	HEXRESPONSE	7
#define	DGRRESPONSE	8
#define	BADUSRCMD	9
#define	REALRESPONSE	10

#define	LONGTIMEOUT	20 // some commands take up to 5 seconds to respond
#define	LONGWRITEOFF	0
#define	LONGWRITEBUSY	1
#define	LONGWRITEOK	2
#define	LONGWRITEERR	3

// Password Controls
#define	PASSWORDNOTSET	0
#define	PASSWORDONESET	1
#define	PASSWORDTWOSET	2
#define	PASSWORDALLSET	3

// Parameter Size Types
#define	TNONE		0
#define	TBOOL		1
#define	TINT		2
#define	TSTRING		3
#define	TREAL		4

// Port Selection
#define	IS2WIRE		0
#define IS232		1
#define	IS4WIRE		2
#define	ISETHPORT	3
#define	IS20MPORT	4
#define NOPORT		5

// m_COF Definitions
#define	BIN4WIRE	0
#define	ASCNOT2WIRE	3
#define	BIN2WIRE	64
#define	ASC2WIRE	67

// Valid Device Control
#define	DOSING		0x1
#define	PWFITONLY	0x2
#define	AEDONLY		0x4
#define	ALLUNITS	0x7
#define	INVALIDUNIT	0xFF

// Write Outputs Control
enum {
	ONLY12	= 0,
	ALLOUTS	= 0x4,
	ONLY36	= 0x8,
	TOGOUTS	= 0xC,
	OUTFUNC	= 0xC,	// function select bits mask
	OUTMASK	= 0xF3	// mask out inputs
	};	

#define	MAVNODATA	0x80000000

struct FAR HBMAEDCmdDef {
	char	Com[4];
	UINT	Table;
	UINT	ParSize;
	UINT	Valid; // no longer functional Mar 2006
	};

class CHBMAEDDriver : public CMasterDriver
{
	public:
		// Constructor
		CHBMAEDDriver(void);

		// Destructor
		~CHBMAEDDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_Password[7];
			UINT	m_uPhysical;
			BYTE	m_uID[34];
			DWORD	m_DZT[2];
			DWORD	m_LIV1[4];
			DWORD	m_LIV2[4];
			DWORD	m_LIV3[4];
			DWORD	m_LIV4[4];
			DWORD	m_NTF[2];
			DWORD	m_PVS[2];
			BYTE	m_SetPW[7];
			DWORD	m_TRC[5];
			DWORD	m_dMAVCache;
			UINT	m_uLongWaitCtr;
			UINT	m_uLongWaitFn;
			UINT	m_uLongWaitOp;
			BOOL	m_fLongWaitTimeout;
			BYTE	m_bUSRC[40];
			BYTE	m_bUSRR[40];
			BOOL	m_fLDW;
			BOOL	m_fLWT;
			};

		CContext *	m_pCtx;

		// Data Members
		BOOL	m_fCOF13;

		BYTE	m_bTx[64];
		BYTE	m_bRx[64];
		BYTE	m_PrevSelect;

		UINT	m_uPtr;
		UINT	m_uPhysical;
		UINT	m_WriteError;

		//
		// List access
		static HBMAEDCmdDef CODE_SEG CL[];
		HBMAEDCmdDef	FAR * m_pCL;
		HBMAEDCmdDef	FAR * m_pItem;

		// Hex Lookup
		LPCTXT	m_pHex;

		BOOL	CheckCommStatus(void);

		UINT	PutReadInfo(void);
		UINT	PutWriteInfo(DWORD dData);
		void	PutArrayValues( PDWORD pArr, UINT uCount );
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, DWORD dFactor);
		void	AddCommand(void);
		void	PutText(LPCTXT pCmd);
		void	PutMUX(void);
		
		// Transport Layer
		BOOL	Transact(UINT uResponseType);
		void	Send(void);
		BOOL	GetReply(BOOL fIsLongWait);
		BOOL	GetResponse(PDWORD pData, UINT uResponseType, UINT uCount);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		void	SelectDevice(void);
		BOOL	CheckReject(void);

		// COF Handling
		BOOL	SetCOF(void);
		BOOL	TestCOF(BOOL fIs13);
		BOOL	GetCOF(void);
		BOOL	InvalidCOF(DWORD dData);

		// No Transmit Handling
		BOOL	NoReadTransmit (PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(PDWORD pData, UINT uCount);

		// Item Handling
		UINT	GetParameterSize(void);
		void	SetpItem(UINT uTable);
		void	SetLDW_LWTRead(void);
		DWORD	CheckSign(DWORD dData);
		BOOL	NeedPlus(void);
		BOOL	IsMSV(void);
		BOOL	IsReal(void);
		UINT	GetMSVLF(void);

		// I/O Handling
		BOOL	HandleIO(PDWORD pData, BOOL fIsWrite);
		void	HandleOutWrite(DWORD dNew, DWORD dNow, BOOL fBoth);
		void	ToggleFlaggedOutputs(DWORD dNew, DWORD dNow, BOOL fBoth);
		WORD	ReadIO(PDWORD pData);
		void	SendOutWrite(DWORD dData, UINT uTable);

		// Data Handling
		void	PutGeneric(DWORD dData);
		BOOL	GetGeneric(UINT * pPos, PDWORD pData);
		DWORD	GetAscii(UINT * pPos);
		BOOL	DataToASCII(DWORD dData, UINT uCt);

		// Password Handling
		void	StorePassword(DWORD dData, PBYTE pb, UINT uCt);
		void	GetPassword( PDWORD pData, PBYTE pb, UINT uCt);
		UINT	IsValidPWChar(BYTE b);
		BOOL	IsValidPW(UINT * pPos, PBYTE pPW);
		void	ClearPW(PBYTE pPW);

		// Response Timing
		BOOL	CheckLongReply(void);
		void	LongWriteTimeout(void);
		BOOL	ProcessLongResponse(void);
		UINT	CheckLongFunction(void);
		BOOL	CheckLongBusy(void);
		void	ClearLongWait(void);

		// String Handling
		BOOL	AddENU(DWORD dData);
		BOOL	IsUserString(UINT uTable);
		void	TransferString(PBYTE pDest, PBYTE pSrc, UINT uCount);
	};

// March 2006 allowing all commands to be sent to any unit.	
#define ACL	1  // Not in FIT
#define ADR	2
#define	AOV	116 // Fit Specific, Added Feb 06
#define ASF	3
#define ASS	4  // Not in FIT
#define	BRK	89 // FIT Specific
#define CAL	5  // Not in FIT
#define	CBK	90 // FIT Specific
#define	CBT	117 // Fit Specific, Added Feb 06
#define	CDL	118 // Fit Specific, Added Feb 06
#define	CFD	91 // FIT Specific
#define	CFT	119 // Fit Specific, Added Feb 06
#define COF	6
#define	CPV	120 // Fit Specific, Added Feb 06
#define CRC	7
#define	CSM	187 // Added Mar 11
#define	CSN	92 // FIT Specific
#define	CTR	121 // Fit Specific, Added Feb 06
#define CWN	8
#define	CWL	87
#define	DGA	176 // Added Mar 06
#define	DGL	177 // Added Mar 06
#define	DGN	178 // Added Mar 06
#define	DGRV	180 // Added Mar 06, Table Item
#define	DGRS	181 // Added Mar 06, Table Item
#define	DGS	179 // Added Mar 06
#define	DMD	122 // Fit Specific, Added Feb 06
#define DP1	9
#define DP2	10
#define	DPT	123 // Enabled Nov 08

#define	DPW	11
#define	DST	124 // Fit Specific, Added Feb 06
#define	DZR1	158 // Fit Specific, Added Feb 06
#define	DZR2	159 // Fit Specific, Added Feb 06
#define	DZT	125 // Fit Specific, Added Feb 06
#define	DZT1	126 // Fit Specific, Added Feb 06
#define	DZT2	127 // Fit Specific, Added Feb 06
#define	EMD	128 // Fit Specific, Added Feb 06
#define ENU	12
#define	EPT	93 // FIT Specific
#define ESR	13
#define	EWT	94 // FIT Specific
#define	FBK	95 // FIT Specific
#define	FBT	129 // Fit Specific, Added Feb 06
#define	FFD	96 // FIT Specific
#define	FFL	130 // Fit Specific, Added Feb 06
#define	FFM	97 // FIT Specific
#define	FFT	131 // Fit Specific, Added Feb 06
#define FMD	14
#define	FNB	132 // Fit Specific, Added Feb 06
#define	FRS	98 // FIT Specific
#define	FWT	99 // FIT Specific
#define GRU	15 // Not in FIT
#define	HSM	133 // Fit Specific, Added Feb 06
#define ICR	16
#define ID0	17
#define ID1	18
#define ID2	19
#define ID3	20

#define ID4	21
#define ID5	22
#define ID6	23
#define ID7	24
#define ID8	25
#define IMD	26
#define LDC	27
#define	LDV	28
#define	LDST	183 // Write Status Added Nov 08
#define LFT	29
#define	LC0	30

#define	LC1	31
#define	LC2	32
#define	LC3	33

#define	LR1M	34
#define	LR1I	35
#define	LR1O	36
#define	LR1F	37
#define	LR2M	38
#define	LR2I	39
#define	LR2O	40
#define	LR2F	41
#define	LR3M	160 // Added Mar 06
#define	LR3I	161 // Added Mar 06
#define	LR3O	162 // Added Mar 06
#define	LR3F	163 // Added Mar 06
#define	LR4M	164 // Added Mar 06
#define	LR4I	165 // Added Mar 06
#define	LR4O	166 // Added Mar 06
#define	LR4F	167 // Added Mar 06

#define	LW1M	42
#define	LW1I	43
#define	LW1O	44
#define	LW1F	45
#define	LW2M	46
#define	LW2I	47
#define	LW2O	48
#define	LW2F	49
#define	LW3M	168 // Added Mar 06
#define	LW3I	169 // Added Mar 06
#define	LW3O	170 // Added Mar 06
#define	LW3F	171 // Added Mar 06
#define	LW4M	172 // Added Mar 06
#define	LW4I	173 // Added Mar 06
#define	LW4O	174 // Added Mar 06
#define	LW4F	175 // Added Mar 06
#define LIV	50

#define	LTC	100 // FIT Specific
#define	LTF	101 // FIT Specific
#define	LTL	102 // FIT Specific
#define LWC	51
#define LWV	52
#define	LWST	184 // Write Status Added Nov 08
#define MAV	53
#define	MAVR	186 // Real Number selection Nov 08
#define	MDT	134 // Fit Specific, Added Feb 06
#define	MRA	135 // Fit Specific, Added Feb 06
#define MSV	54  // Integer Selection
#define	MSVR	185 // Real Number selection Nov 08
#define MTD	55
#define	MUX	200 // Not Configured, used to get O3-O6 Added May 10

#define	NDS	103 // FIT Specific
#define NOV	56
#define	NTR1	136 // Fit Specific, Added Feb 06
#define	NTR2	137 // Fit Specific, Added Feb 06
#define	NTF	138 // Fit Specific, Added Feb 06
#define	NTW1	139 // Fit Specific, Added Feb 06
#define	NTW2	140 // Fit Specific, Added Feb 06
#define	OMD	104 // FIT Specific
#define	OSN	105 // FIT Specific

#define	PVAL	141 // Fit Specific, Added Feb 06
#define	PVAH	142 // Fit Specific, Added Feb 06
#define	PVS	143 // Fit Specific, Added Feb 06
#define	PVSL	144 // Fit Specific, Added Feb 06
#define	PVSH	145 // Fit Specific, Added Feb 06
#define PW1	57
#define PW2	88
#define POR	58
#define	RDP	146 // Fit Specific, Added Feb 06
#define	RDS	147 // Fit Specific, Added Feb 06
#define RES	59
#define	RFT	106 // FIT Specific
#define	RIO	182 // March 06
#define	RSN	107 // FIT Specific
#define	RUN	108 // FIT Specific
#define	SDF	148 // Fit Specific, Added Feb 06
#define	SDM	149 // Fit Specific, Added Feb 06
#define	SDO	109 // FIT Specific
#define	SDS	150 // Fit Specific, Added Feb 06
#define SEL	60

#define SFC	61  // Not in FIT
#define SFV	62  // Not in FIT
#define	SOV	151 // Fit Specific, Added Feb 06
#define SP1	63
#define SP2	64
#define SPW	65
#define STR	66
#define	STT	110 // FIT Specific
#define	SUM	111 // FIT Specific
#define	SYD	112 // FIT Specific
#define SZC	67  // Not in FIT
#define SZV	68  // Not in FIT
#define	TAD	113 // FIT Specific
#define TAR	69
#define TAS	70

#define TAV	71
#define TCR	72
#define TDD	73
#define	TMD	114 // FIT Specific
#define TR1	74
#define TR2	75
#define TR3	76
#define TR4	77
#define TR5	78
#define	TRF	152 // Fit Specific, Added Feb 06
#define	TRM	153 // Fit Specific, Added Feb 06
#define	TRN	154 // Fit Specific, Added Feb 06
#define	TRS	155 // Fit Specific, Added Feb 06
#define TW1	79
#define TW2	80

#define TW3	81
#define TW4	82
#define TW5	83
#define TRC	84
#define	USR	197 // Added Nov 08
#define	USRC	198 // Added Nov 08
#define	USRR	199 // Added Nov 08
#define	UTL	115 // FIT Specific
#define	VCT	156 // Fit Specific, Added Feb 06
#define	WDP	157 // Fit Specific, Added Feb 06
#define ZSE	85
#define ZTR	86

// End of File
