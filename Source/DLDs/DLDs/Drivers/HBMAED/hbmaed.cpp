
#include "intern.hpp"

#include "hbmaed.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HBM AED Driver
//		

//struct FAR HBMAEDCmdDef {
//	char	Com[4];
//	UINT	Table;
//	UINT	ParSize;
//	UINT	Valid; // no longer functional Mar 2006
//	};

HBMAEDCmdDef CODE_SEG CHBMAEDDriver::CL[] = {	

	{"ACL",	ACL,	TBOOL,		AEDONLY},
	{"ADR",	ADR,	TINT,		ALLUNITS},
	{"AOV",	AOV,	TINT,		DOSING}, // FIT, FEB06
	{"ASF",	ASF,	TINT,		ALLUNITS},
	{"ASS",	ASS,	TINT,		AEDONLY},

	{"BRK",	BRK,	TNONE,		DOSING}, // FIT

	{"CAL",	CAL,	TNONE,		AEDONLY},
	{"CBK",	CBK,	TINT,		DOSING}, // FIT
	{"CBT",	CBT,	TINT,		DOSING}, // FIT, FEB06
	{"CDL",	CDL,	TNONE,		DOSING}, // FIT, FEB06
	{"CFD",	CFD,	TINT,		DOSING}, // FIT
	{"CFT",	CFT,	TINT,		DOSING}, // FIT, FEB06
	{"COF",	COF,	TINT,		ALLUNITS},
	{"CPV",	CPV,	TNONE,		DOSING}, // FIT, FEB06
	{"CRC",	CRC,	TINT,		ALLUNITS},
	{"CSM",	CSM,	TINT,		ALLUNITS}, // MAR11
	{"CSN",	CSN,	TNONE,		DOSING}, // FIT
	{"CTR",	CTR,	TNONE,		DOSING}, // FIT, FEB06
	{"CWT",	CWN,	TINT,		ALLUNITS},
	{"CWT",	CWL,	TINT,		ALLUNITS},

	{"DGA",	DGA,	TINT,		ALLUNITS}, // MAR 06
	{"DGL",	DGL,	TINT,		ALLUNITS}, // MAR 06
	{"DGN",	DGN,	TINT,		ALLUNITS}, // MAR 06
	{"DGR",	DGRV,	TINT,		ALLUNITS}, // MAR 06
	{"DGR",	DGRS,	TINT,		ALLUNITS}, // MAR 06
	{"DGS",	DGS,	TINT,		ALLUNITS}, // MAR 06

	{"DMD",	DMD,	TBOOL,		DOSING}, // FIT, FEB06
	{"DPT",	DPT,	TINT,		DOSING}, // FIT, FEB06
	{"DPW",	DP1,	TINT,		ALLUNITS},
	{"DPW",	DP2,	TINT,		ALLUNITS},
	{"DPW",	DPW,	TSTRING,	ALLUNITS},
	{"DST",	DST,	TINT,		DOSING}, // FIT, FEB06
	{"DZT",	DZR1,	TINT,		DOSING}, // FIT, FEB06
	{"DZT",	DZR2,	TINT,		DOSING}, // FIT, FEB06
	{"DZT",	DZT,	TINT,		DOSING}, // FIT, FEB06
	{"DZT",	DZT1,	TINT,		DOSING}, // FIT, FEB06
	{"DZT",	DZT2,	TINT,		DOSING}, // FIT, FEB06

	{"EMD",	EMD,	TBOOL,		DOSING}, // FIT, FEB06
	{"ENU",	ENU,	TSTRING,	ALLUNITS},
	{"EPT",	EPT,	TINT,		DOSING}, // FIT
	{"ESR",	ESR,	TINT,		ALLUNITS},
	{"EWT",	EWT,	TINT,		DOSING}, // FIT

	{"FBK",	FBK,	TINT,		DOSING}, // FIT
	{"FBT",	FBT,	TINT,		DOSING}, // FIT, FEB06
	{"FFD",	FFD,	TINT,		DOSING}, // FIT
	{"FFL",	FFL,	TINT,		DOSING}, // FIT, FEB06
	{"FFM",	FFM,	TINT,		DOSING}, // FIT
	{"FFT",	FFT,	TINT,		DOSING}, // FIT, FEB06
	{"FMD",	FMD,	TINT,		ALLUNITS},
	{"FNB",	FNB,	TINT,		DOSING}, // FIT, FEB06
	{"FRS",	FRS,	TINT,		DOSING}, // FIT
	{"FWT",	FWT,	TINT,		DOSING}, // FIT

	{"GRU",	GRU,	TINT,		AEDONLY},

	{"HSM",	HSM,	TBOOL,		DOSING}, // FIT, FEB06

	{"ICR",	ICR,	TINT,		ALLUNITS},
	{"IDN",	ID0,	TSTRING,	ALLUNITS},
	{"IDN",	ID1,	TSTRING,	ALLUNITS},
	{"IDN",	ID2,	TSTRING,	ALLUNITS},
	{"IDN",	ID3,	TSTRING,	ALLUNITS},
	{"IDN",	ID4,	TSTRING,	ALLUNITS},
	{"IDN",	ID5,	TSTRING,	ALLUNITS},
	{"IDN",	ID6,	TSTRING,	ALLUNITS},
	{"IDN",	ID7,	TSTRING,	ALLUNITS},
	{"IDN",	ID8,	TSTRING,	ALLUNITS},
	{"IMD",	IMD,	TINT,		ALLUNITS},

	{"LDW",	LDC,	TNONE,		ALLUNITS},
	{"LDW",	LDV,	TINT,		ALLUNITS},
	{"ldw", LDST,	TBOOL,		ALLUNITS},
	{"LFT",	LFT,	TBOOL,		ALLUNITS},
	{"LIC",	LC0,	TINT,		ALLUNITS},
	{"LIC",	LC1,	TINT,		ALLUNITS},
	{"LIC",	LC2,	TINT,		ALLUNITS},
	{"LIC",	LC3,	TINT,		ALLUNITS},

	{"LIV",	LR1M,	TINT,		ALLUNITS},
	{"LIV",	LR1I,	TINT,		ALLUNITS},
	{"LIV",	LR1O,	TINT,		ALLUNITS},
	{"LIV",	LR1F,	TINT,		ALLUNITS},
	{"LIV",	LR2M,	TINT,		ALLUNITS},
	{"LIV",	LR2I,	TINT,		ALLUNITS},
	{"LIV",	LR2O,	TINT,		ALLUNITS},
	{"LIV",	LR2F,	TINT,		ALLUNITS},
	{"LIV",	LR3M,	TINT,		ALLUNITS},
	{"LIV",	LR3I,	TINT,		ALLUNITS},
	{"LIV",	LR3O,	TINT,		ALLUNITS},
	{"LIV",	LR3F,	TINT,		ALLUNITS},
	{"LIV",	LR4M,	TINT,		ALLUNITS},
	{"LIV",	LR4I,	TINT,		ALLUNITS},
	{"LIV",	LR4O,	TINT,		ALLUNITS},
	{"LIV",	LR4F,	TINT,		ALLUNITS},

	{"LIV",	LW1M,	TINT,		ALLUNITS},
	{"LIV",	LW1I,	TINT,		ALLUNITS},
	{"LIV",	LW1O,	TINT,		ALLUNITS},
	{"LIV",	LW1F,	TINT,		ALLUNITS},
	{"LIV",	LW2M,	TINT,		ALLUNITS},
	{"LIV",	LW2I,	TINT,		ALLUNITS},
	{"LIV",	LW2O,	TINT,		ALLUNITS},
	{"LIV",	LW2F,	TINT,		ALLUNITS},
	{"LIV",	LW3M,	TINT,		ALLUNITS},
	{"LIV",	LW3I,	TINT,		ALLUNITS},
	{"LIV",	LW3O,	TINT,		ALLUNITS},
	{"LIV",	LW3F,	TINT,		ALLUNITS},
	{"LIV",	LW4M,	TINT,		ALLUNITS},
	{"LIV",	LW4I,	TINT,		ALLUNITS},
	{"LIV",	LW4O,	TINT,		ALLUNITS},
	{"LIV",	LW4F,	TINT,		ALLUNITS},
	{"LIV",	LIV,	TINT,		ALLUNITS},

	{"LTC",	LTC,	TINT,		DOSING}, // FIT
	{"LTF",	LTF,	TINT,		DOSING}, // FIT
	{"LTL",	LTL,	TINT,		DOSING}, // FIT
	{"LWT",	LWC,	TNONE,		ALLUNITS},
	{"LWT",	LWV,	TINT,		ALLUNITS},
	{"lwt",	LWST,	TBOOL,		ALLUNITS},

	{"MAV",	MAV,	TINT,		ALLUNITS},
	{"MAV",	MAVR,	TREAL,		ALLUNITS},
	{"MDT",	MDT,	TINT,		DOSING}, // FIT, FEB06
	{"MRA",	MRA,	TINT,		DOSING}, // FIT, FEB06
	{"MSV",	MSV,	TINT,		ALLUNITS},
	{"MSV",	MSVR,	TREAL,		ALLUNITS},
	{"MTD",	MTD,	TINT,		ALLUNITS},
	{"MUX", MUX,	TINT,		ALLUNITS}, // MAY10

	{"NDS",	NDS,	TINT,		DOSING}, // FIT
	{"NOV",	NOV,	TINT,		ALLUNITS},
	{"NTF",	NTR1,	TINT,		DOSING}, // FIT, FEB06
	{"NTF",	NTR2,	TINT,		DOSING}, // FIT, FEB06
	{"NTF",	NTF,	TINT,		DOSING}, // FIT, FEB06
	{"NTF",	NTW1,	TINT,		DOSING}, // FIT, FEB06
	{"NTF",	NTW2,	TINT,		DOSING}, // FIT, FEB06

	{"OMD",	OMD,	TINT,		DOSING}, // FIT
	{"OSN",	OSN,	TINT,		DOSING}, // FIT

	{"POR",	POR,	TINT,		ALLUNITS},
	{"POR",	PW1,	TBOOL,		ALLUNITS},
	{"POR",	PW2,	TBOOL,		ALLUNITS},
	{"PVA",	PVAL,	TINT,		DOSING}, // FIT, FEB06
	{"PVA",	PVAH,	TINT,		DOSING}, // FIT, FEB06
	{"PVS",	PVS,	TINT,		DOSING}, // FIT, FEB06
	{"PVS",	PVSL,	TINT,		DOSING}, // FIT, FEB06
	{"PVS",	PVSH,	TINT,		DOSING}, // FIT, FEB06

	{"RDP",	RDP,	TINT,		DOSING}, // FIT, FEB06
	{"RDS",	RDS,	TINT,		DOSING}, // FIT, FEB06
	{"RES",	RES,	TNONE,		ALLUNITS},
	{"RFT",	RFT,	TINT,		DOSING}, // FIT
	{"RIO",	RIO,	TINT,		ALLUNITS}, // March 06
	{"RSN",	RSN,	TINT,		PWFITONLY}, // FIT
	{"RUN",	RUN,	TNONE,		DOSING}, // FIT

	{"SDF",	SDF,	TINT,		DOSING}, // FIT, FEB06
	{"SDM",	SDM,	TINT,		DOSING}, // FIT, FEB06
	{"SDO",	SDO,	TINT,		DOSING}, // FIT
	{"SDS",	SDS,	TINT,		DOSING}, // FIT, FEB06
	{";S",	SEL,	TINT,		ALLUNITS},
	{"SFA",	SFC,	TNONE,		AEDONLY},
	{"SFA",	SFV,	TINT,		AEDONLY},
	{"SOV",	SOV,	TINT,		DOSING}, // FIT, FEB06
	{"SPW",	SP1,	TINT,		ALLUNITS},
	{"SPW",	SP2,	TINT,		ALLUNITS},
	{"SPW",	SPW,	TSTRING,	ALLUNITS},
	{"STR",	STR,	TBOOL,		ALLUNITS},
	{"STT",	STT,	TINT,		DOSING}, // FIT
	{"SUM",	SUM,	TINT,		DOSING}, // FIT
	{"SYD",	SYD,	TINT,		DOSING}, // FIT
	{"SZA",	SZC,	TNONE,		AEDONLY},
	{"SZA",	SZV,	TINT,		AEDONLY},

	{"TAD",	TAD,	TINT,		DOSING}, // FIT
	{"TAR",	TAR,	TNONE,		ALLUNITS},
	{"TAS",	TAS,	TBOOL,		ALLUNITS},
	{"TAV",	TAV,	TINT,		ALLUNITS},
	{"TCR",	TCR,	TINT,		ALLUNITS},
	{"TDD",	TDD,	TINT,		ALLUNITS},
	{"TMD",	TMD,	TBOOL,		DOSING}, // FIT

	{"TRC",	TR1,	TINT,		ALLUNITS},
	{"TRC",	TR2,	TINT,		ALLUNITS},
	{"TRC",	TR3,	TINT,		ALLUNITS},
	{"TRC",	TR4,	TINT,		ALLUNITS},
	{"TRC",	TR5,	TINT,		ALLUNITS},
	{"TRC",	TW1,	TBOOL,		ALLUNITS},
	{"TRC",	TW2,	TBOOL,		ALLUNITS},
	{"TRC",	TW3,	TINT,		ALLUNITS},
	{"TRC",	TW4,	TINT,		ALLUNITS},
	{"TRC",	TW5,	TINT,		ALLUNITS},
	{"TRC",	TRC,	TINT,		ALLUNITS},

	{"TRF",	TRF,	TINT,		DOSING}, // FIT, FEB06
	{"TRM",	TRM,	TINT,		DOSING}, // FIT, FEB06
	{"TRN",	TRN,	TINT,		DOSING}, // FIT, FEB06
	{"TRS",	TRS,	TINT,		DOSING}, // FIT, FEB06

	{"usr",	USR,	TSTRING,	ALLUNITS}, // Nov 08
	{"usr",	USRC,	TSTRING,	ALLUNITS}, // Nov 08
	{"usr",	USRR,	TSTRING,	ALLUNITS}, // Nov 08
	{"UTL",	UTL,	TINT,		DOSING}, // FIT

	{"VCT",	VCT,	TINT,		DOSING}, // FIT, FEB06

	{"WDP",	WDP,	TINT,		DOSING}, // FIT, FEB06

	{"ZSE",	ZSE,	TINT,		ALLUNITS},
	{"ZTR",	ZTR,	TBOOL,		ALLUNITS},
	};

// Instantiator

INSTANTIATE(CHBMAEDDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CHBMAEDDriver::CHBMAEDDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_PrevSelect	= INVALIDUNIT;

	m_WriteError	= 0;

	m_fCOF13	= FALSE;
	}

// Destructor

CHBMAEDDriver::~CHBMAEDDriver(void)
{
	}

// Configuration

void MCALL CHBMAEDDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CHBMAEDDriver::CheckConfig(CSerialConfig &Config)
{
//	Config.m_uFlags |= flagFastRx;

	switch( Config.m_uPhysical ) {

		case 1:
			m_uPhysical = IS232;
			break;

		case 2:
			Make422(Config, TRUE);
			m_uPhysical = IS4WIRE;
			break;

		case 4:
			Make422(Config, TRUE);
			m_uPhysical = Config.m_uPhysMode ? IS4WIRE : IS2WIRE;
			break;

		case 5:
			m_uPhysical = IS20MPORT;
			break;

		case 6:
			m_uPhysical = ISETHPORT;
			break;

		case 3:
		default:
			m_uPhysical = NOPORT;
			break;
		}
	}
	
// Management

void MCALL CHBMAEDDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CHBMAEDDriver::Open(void)
{
	m_pCL = (HBMAEDCmdDef FAR * )CL;
	}

// Device

CCODE MCALL CHBMAEDDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop     = GetByte(pData);

			m_pCtx->m_dMAVCache = 0;

			pDevice->SetContext(m_pCtx);

			m_pCtx->m_uPhysical = m_uPhysical;

			ClearLongWait();

			m_pCtx->m_fLongWaitTimeout = FALSE;

			memset(m_pCtx->m_bUSRC, 0, 40);
			memset(m_pCtx->m_bUSRR, 0, 40);

			m_pCtx->m_fLDW = FALSE;
			m_pCtx->m_fLWT = FALSE;

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CHBMAEDDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CHBMAEDDriver::Ping(void)
{
//**/	AfxTrace0("\r\nPING ");

	if( CheckCommStatus() ) {

		return 1; // testing command output
		}

	SetpItem(COF);

	if( GetCOF() ) {

		ClearLongWait();

		return 1;
		}

	m_PrevSelect = INVALIDUNIT;

	return CCODE_ERROR;
	}

CCODE MCALL CHBMAEDDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	if( !NoReadTransmit(pData, uCount) ) AfxTrace3("\r\nRead-- T=%d O=%d C=%d", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( CheckLongBusy() ) return CCODE_ERROR | CCODE_BUSY;

	if( Addr.a.m_Table == addrNamed && Addr.a.m_Offset == POR ) {

		return HandleIO(pData, FALSE) ? 1: CCODE_ERROR;
		}		

	UINT uTable = Addr.a.m_Table;

	SetpItem( uTable == addrNamed ? Addr.a.m_Offset : uTable );

	if( m_pItem == NULL ) return CCODE_ERROR | CCODE_HARD;

	if( NoReadTransmit(pData, uCount) ) return IsUserString(Addr.a.m_Table) ? uCount : 1;

	SelectDevice();

	if( IsMSV() ) {

		if( !SetCOF() ) {

			return CCODE_ERROR;
			}
		}

	UINT uResponseType = PutReadInfo();

	if( uResponseType == DGRRESPONSE ) PutGeneric( Addr.a.m_Offset );

	if( Transact( uResponseType ) ) {

		if( ProcessLongResponse() ) {

			m_pCtx->m_fLongWaitTimeout = FALSE;

			return CCODE_ERROR | CCODE_NO_DATA; //Long Write Response
			}

		if( GetResponse( pData, uResponseType, uCount ) ) {

			if( m_pItem->Table == MAV ) {

				if( pData[0] & MAVNODATA ) pData[0] = m_pCtx->m_dMAVCache;

				else m_pCtx->m_dMAVCache = pData[0];
				}

			return 1;
			}
		}

	if( CheckReject() ) return CCODE_NO_RETRY;	

	m_PrevSelect = INVALIDUNIT;

	SelectDevice();

	return CCODE_ERROR;
	}

CCODE MCALL CHBMAEDDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n***** WRITE ** %d %d %d ", Addr.a.m_Table, Addr.a.m_Offset, uCount ); AfxTrace1("%8.8lx ", *pData);

	BOOL fCheckLong = FALSE;

	UINT uLongOp = Addr.a.m_Table;

	if( uLongOp == addrNamed ) uLongOp = Addr.a.m_Offset;

	switch( CheckLongFunction() ) {

		case 1:
			fCheckLong = TRUE;
			break;

		case 2:
			return CCODE_ERROR | CCODE_BUSY;

		default:
			break;
		}

	if( fCheckLong ) {

		if( m_pCtx->m_uLongWaitOp != uLongOp ) {

			return CCODE_ERROR | CCODE_BUSY | CCODE_NO_RETRY;
			}

		switch( m_pCtx->m_uLongWaitFn ) {

			case LONGWRITEOK:

				ClearLongWait();
				m_pCtx->m_fLongWaitTimeout = FALSE;
				return 1;

			case LONGWRITEERR:

				ClearLongWait();
				m_pCtx->m_fLongWaitTimeout = FALSE;
				return CCODE_ERROR;
			}
		}

	if( m_pCtx->m_fLongWaitTimeout ) {

		m_pCtx->m_fLongWaitTimeout = FALSE;

		ClearLongWait();

		if( ++m_WriteError > 2 ) {

			m_WriteError = FALSE;

			return 1;
			}
		}

	if( Addr.a.m_Table == addrNamed && Addr.a.m_Offset == POR ) {

		HandleIO(pData, TRUE);

		return uCount;
		}

	SetpItem( Addr.a.m_Table == addrNamed ? Addr.a.m_Offset : Addr.a.m_Table );

	if( m_pItem == NULL ) return CCODE_ERROR;

//**/	AfxTrace2("*** %s %d ", m_pItem->Com, m_pCtx->m_uLongWaitCtr );

	if( NoWriteTransmit( pData, uCount ) ) return IsUserString(Addr.a.m_Table) ? uCount : 1;

	SelectDevice();

	UINT uResponseType = PutWriteInfo(*pData);

	if( uResponseType != LONGRESPONSE ) ClearLongWait();

	switch( uResponseType ) {

		case RESPONSEFAIL: // bad Ascii write

			m_WriteError = 0;

			return 1;

		case LONGRESPONSE:

			m_pCtx->m_uLongWaitFn  = LONGWRITEBUSY;

			m_pCtx->m_uLongWaitOp  = uLongOp;

			m_pCtx->m_uLongWaitCtr = 0;

			if( Transact(uResponseType) ) {

				m_pCtx->m_uLongWaitFn = LONGWRITEOFF;

				m_WriteError          = 0;

				SetLDW_LWTRead();

				return uCount;
				}

			SetLDW_LWTRead();

			if( CheckReject() ) return CCODE_NO_RETRY;	

			return CCODE_ERROR | CCODE_BUSY;

		case BADUSRCMD:
			return uCount;

		default:
			if( Transact( uResponseType ) ) {

				if( uResponseType == NORESPONSE || GetResponse(pData, uResponseType, uCount) ) {

					m_WriteError = 0;

					SetLDW_LWTRead();

					return 1;
					}
				}

			SetLDW_LWTRead();

			if( ++m_WriteError > 2 ) {

				m_WriteError = 0;

				return 1;
				}

			if( CheckReject() ) return CCODE_NO_RETRY;	

			return CCODE_ERROR;
		}

	m_PrevSelect = INVALIDUNIT;

	SelectDevice();
	
	return CCODE_ERROR;
	}

// PRIVATE METHODS

BOOL CHBMAEDDriver::CheckCommStatus(void)
{
	UINT uData;

	// Check serial output
//***/	DWORD Data[1];

//***/	CAddress Addr;
//***/	Addr.a.m_Table  = addrNamed;
//***/	Addr.a.m_Offset = TDD;
//***/	Addr.a.m_Type   = addrByteAsByte;
//***/	Addr.a.m_Extra  = 0;

//***/	Data[0]         = 1;

//***/	Write(Addr, Data, 1);

//***/	return TRUE;

	if( (uData = m_pData->Read(60)) != NOTHING ) { // drive is transmitting values

		// Address all units
		m_uPtr = 0;
		AddByte(';');
		AddByte('S');
		AddByte('0');
		AddByte('0');
		AddByte(';');
		Put();
		Sleep(10);

		// Send STOP command
		m_uPtr = 0;
		AddByte('S');
		AddByte('T');
		AddByte('P');
		AddByte(';');
		Put();
		Sleep(10);
		}

	return FALSE;
	}

UINT CHBMAEDDriver::PutReadInfo(void)
{
	UINT uResponseType = READRESPONSE;

	UINT uT            = m_pItem->Table;

	BYTE bAddLast      = '?';

	AddCommand();

	switch( uT ) {

		case ENU:
		case ID0:
		case ID1:
		case ID2:
		case ID3:
		case ID4:
		case ID5:
		case ID6:
		case ID7:
		case ID8:	uResponseType = ASCIIRESPONSE;	break;

		case CWL:
		case DZR2:
		case LC0:
		case LC1:
		case LC2:
		case LC3:
		case NTR2:
		case POR:
		case PVAH:
		case PVSH:
		case PW1:
		case PW2:
		case TR1:
		case TR2:
		case TR3:
		case TR4:
		case TR5:	uResponseType = MULTIRESPONSE;	break;

		case DGRV:
		case DGRS:	uResponseType = DGRRESPONSE;	break;

		case LR1M:
		case LR1I:
		case LR1O:											      
		case LR1F:
			AddByte('?');
			bAddLast = '1';
			uResponseType = MULTIRESPONSE;
			break;

		case LR2M:
		case LR2I:
		case LR2O:
		case LR2F:
			AddByte('?');
			bAddLast = '2';
			uResponseType = MULTIRESPONSE;
			break;

		case LR3M:
		case LR3I:
		case LR3O:
		case LR3F:
			AddByte('?');
			bAddLast = '3';
			uResponseType = MULTIRESPONSE;
			break;

		case LR4M:
		case LR4I:
		case LR4O:
		case LR4F:
			AddByte('?');
			bAddLast = '4';
			uResponseType = MULTIRESPONSE;
			break;

		case MSV:
			if( m_pCtx->m_uPhysical == IS4WIRE ) {

				AddByte('?');

				bAddLast = '2';
				}
			break;

		case MAVR:
		case MSVR:
			uResponseType = REALRESPONSE;
			break;

		default:
			break;
		}

	AddByte(bAddLast);

	return uResponseType;
	}

UINT CHBMAEDDriver::PutWriteInfo(DWORD dData)
{
	UINT uPWPos = 0;

	AddCommand();

	char c[4];

	switch( m_pItem->Table ) {

		case ENU:
			return AddENU(dData) ? WRITERESPONSE : RESPONSEFAIL;

		case DPW:
			if( !IsValidPW( &uPWPos, &(m_pCtx->m_Password[0]) ) ) {

				return RESPONSEFAIL;
				}

			AddByte('"');

			while( uPWPos < 7 && m_pCtx->m_Password[uPWPos] ) {

				AddByte( m_pCtx->m_Password[uPWPos] );

				uPWPos++;
				}

			AddByte('"');

			return WRITERESPONSE;

		case SPW:
			if( !IsValidPW( &uPWPos, &(m_pCtx->m_SetPW[0]) ) ) {

				return RESPONSEFAIL;
				}

			AddByte('"');

			while( uPWPos < 7 && m_pCtx->m_SetPW[uPWPos] ) {

				AddByte( m_pCtx->m_SetPW[uPWPos] );

				uPWPos++;
				}

			AddByte('"');

			return WRITERESPONSE;

		case SFC:
		case SFV:
		case SZC:
		case LDC:
		case LWC:
		case CAL:
		case RES:	return LONGRESPONSE;

		case LC0:
		case LC1:
		case LC2:
		case LC3:
			AddByte( '0' + m_pItem->Table - LC0 );

			AddByte( ',' );

			PutGeneric( CheckSign(dData) );

			return WRITERESPONSE;

		case TDD:
			PutGeneric( CheckSign(dData) );

			return LONGRESPONSE;

		case TRC:
			PutArrayValues( m_pCtx->m_TRC, 5 );

			return WRITERESPONSE;

		case LIV:
			AddByte( m_pHex[dData & 7] );
			AddByte( ',' );

			switch( dData ) {

				case 1:	PutArrayValues( m_pCtx->m_LIV1, 4 );	break;
				case 2:	PutArrayValues( m_pCtx->m_LIV2, 4 );	break;
				case 3:	PutArrayValues( m_pCtx->m_LIV3, 4 );	break;
				case 4:	PutArrayValues( m_pCtx->m_LIV4, 4 );	break;
				}

			return WRITERESPONSE;

		case PW2:
			AddByte( ',' ); // then fall through			
		case PW1:
			AddByte( '1' );

			return WRITERESPONSE;

		case POR:
			AddByte(dData & 1 ? '1' : '0');
			AddByte(',');
			AddByte(dData & 2 ? '1' : '0');

			return WRITERESPONSE;

		case DZT:
			PutArrayValues( m_pCtx->m_DZT, 2 );

			return WRITERESPONSE;

		case NTF:
			PutArrayValues( m_pCtx->m_NTF, 2 );

			return WRITERESPONSE;

		case PVS:
			PutArrayValues( m_pCtx->m_PVS, 2 );

			return WRITERESPONSE;

		case USR:
			if( m_pCtx->m_bUSRC[0] ) {

				PutText((PCTXT)m_pCtx->m_bUSRC);

				BYTE b;

				b = m_bTx[m_uPtr - 1];

				if( b != ';' ) {

					m_bTx[m_uPtr++] = ';';
					}

				memset(m_pCtx->m_bUSRR, 0, 40);

				return ASCIIRESPONSE;
				}

			m_pCtx->m_bUSRC[0] = '?';

			return BADUSRCMD;

		case MUX:
			SPrintf( c, " %2.2d\0", LOBYTE(LOWORD(dData)) & 0xF);

			PutText((LPCTXT)c);

			return WRITERESPONSE;

		default:
			switch( GetParameterSize() ) {

				case TBOOL:	AddByte( dData ? '1' : '0' );	break;
					
				case TNONE:					break;

				case TINT:
				default:	PutGeneric( CheckSign(dData) );	break;
				}

			return WRITERESPONSE;
		}

	return NORESPONSE;
	}

void CHBMAEDDriver::PutArrayValues(PDWORD pArr, UINT uCount)
{
	for( UINT i = 0; i < uCount - 1; i++ ) {

		PutGeneric( CheckSign( pArr[i] ) );

		AddByte(',');
		}

	PutGeneric( CheckSign( pArr[uCount-1] ) );
	}

// Frame Building

void CHBMAEDDriver::StartFrame(void)
{
	m_uPtr = 0;
	}

void CHBMAEDDriver::EndFrame(void)
{
	if( m_uPtr < sizeof(m_bTx) && m_bTx[m_uPtr - 1] != ';' ) {

		AddByte( ';' );
		}
	}

void CHBMAEDDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CHBMAEDDriver::AddData(DWORD dData, DWORD dFactor)
{
	while ( dFactor ) {

		AddByte( m_pHex[(dData/dFactor) % 10] );

		dFactor /= 10;
		}
	}

void CHBMAEDDriver::AddCommand(void)
{
	StartFrame();

	if( m_pItem->Com[0] <= 'Z' ) { // don't add Com for lower case

		PutText( m_pItem->Com );
		}
	}
	
void CHBMAEDDriver::PutText(LPCTXT pCmd)
{
	for( UINT i=0; pCmd[i]; i++ ) {

		AddByte( BYTE(pCmd[i]) );
		}
	}

// Transport Layer

BOOL CHBMAEDDriver::Transact(UINT uResponseType)
{
	Send();

	if(	 uResponseType == NORESPONSE ||
		(uResponseType == WRITERESPONSE && m_pCtx->m_uPhysical == IS2WIRE) // bus mode
		) {

		m_pData->Read(0);

		Sleep(20);

		return TRUE;
		}

	return GetReply(uResponseType == LONGRESPONSE);
	}

void CHBMAEDDriver::Send(void)
{
	memset(m_bRx, 0, 3);

	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CHBMAEDDriver::GetReply(BOOL fIsLongWait)
{
	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uData;

	UINT uLFCount = 1;

	BYTE bData;

	if( IsMSV() ) {

		uLFCount = GetMSVLF();
		}

	SetTimer( TIMEOUT );

//**/	AfxTrace0("\r\nRCV-");

	while( (uTimer = GetTimer()) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);

		m_bRx[uCount++] = bData;

		if( bData == LF ) {

//**/			if( m_bRx[0] == '?' ) AfxTrace0("\r\n");

			if( m_bRx[1] == CR) {

				return TRUE;
				}

			switch( uLFCount ) {

				case 0:	
					if( uCount == 6 ) return TRUE;
					break;

				case 1:
					if( m_bRx[uCount-2] == CR ) return TRUE;
					break;

				case 2:
					uLFCount = 1;
					uCount  = 0;
					break;
				}
			}
		}

	if( !fIsLongWait ) m_pData->Write( ';', FOREVER );

	return FALSE;
	}

BOOL CHBMAEDDriver::GetResponse( PDWORD pData, UINT uResponseType, UINT uCount )
{
	DWORD dData = 0;
	DWORD d2    = 0;

	UINT uPos   = 0;
	UINT uItem  = 0;
	UINT uT     = m_pItem->Table;
	UINT  i;

	if( m_bRx[0] == '?' && uT != USR ) {

		return FALSE;
		}

	switch( uResponseType ) {

		case NORESPONSE:	return TRUE;

		case WRITERESPONSE:

			return m_bRx[0] == '0';

		case ASCIIRESPONSE:

			switch( uT ) {

				case ID0:
				case ID1:
				case ID2:
				case ID3:
				case ID4:
				case ID5:
				case ID6:
				case ID7:
				case ID8:	uPos = (uT - ID0) * 4;	break;

				case USR:
					TransferString(m_pCtx->m_bUSRR, m_bRx, 10);
					pData = 0;
					return TRUE;

				case ENU:	break;
				}

			*pData = GetAscii( &uPos );

			return TRUE;

		case MULTIRESPONSE: // keeps getting data until the desired item is reached

			switch( uT ) {

				case CWL: // 2nd Item of Multiple data
				case PVAH:
				case PVSH:
				case DZR2:
				case NTR2:	uItem = 1;		break;

				case LR1M:
				case LR1I:
				case LR1O:
				case LR1F:	uItem = uT - LR1M + 1;	break;

				case LR2M:
				case LR2I:
				case LR2O:
				case LR2F:	uItem = uT - LR2M + 1;	break;

				case LR3M:
				case LR3I:
				case LR3O:
				case LR3F:	uItem = uT - LR3M + 1;	break;

				case LR4M:
				case LR4I:
				case LR4O:
				case LR4F:	uItem = uT - LR4M + 1;	break;

				case LC0:
				case LC1:
				case LC2:
				case LC3:	uItem = uT - LC0;	break;

				case TR1:
				case TR2:
				case TR3:
				case TR4:
				case TR5:	uItem = uT - TR1;	break;

				case POR:
				case PW1:
				case PW2:
					for( i = 0; i < 4; i++ ) {

						if( !GetGeneric(&uPos, &d2) ) return FALSE;

						dData <<= 1;

						dData |= d2 ? 1 : 0;
						}

					switch( uT ) {

						case POR:	*pData = dData;			break;
						case PW1:	*pData = dData & 8 ? 1 : 0;	break;
						case PW2:	*pData = dData & 4 ? 1 : 0;	break;
						}

					return TRUE;

				default:	return FALSE;
				}

			for( i = 0; i <= uItem; i++ ) {

				if( !GetGeneric(&uPos, &dData) ) return FALSE;
				}
			break;

		case HEXRESPONSE:

			for( i = 0; i < 3; i++ ) {

				dData <<= 8;

				dData += m_bRx[i];
				}

			if( dData & 0x800000 ) dData |= 0xFF000000;

			break;

		case DGRRESPONSE:

			dData = uT == DGRV ? (*PDWORD(&m_bRx[0])) >> 8 : m_bRx[3];

			break;

		case REALRESPONSE:

			if( IsMSV() && m_fCOF13 ) {

				if( m_bRx[0] != ' ' && m_bRx[0] != '-' && m_bRx[0] != '+' ) {
				
					uPos = 4;
					}
				}

			*pData = ATOF((const char *)&m_bRx[uPos]);

			return TRUE;

		default:
			if( !GetGeneric(&uPos, &dData) ) return FALSE;

			if( m_pItem->Table == COF ) {

				if( InvalidCOF(dData) ) return SetCOF();
				}

			break;
		}

	*pData = dData;

	return TRUE;
	}

// Port Access

void CHBMAEDDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k] );

	m_bRx[0] = 0;

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );
	}

UINT CHBMAEDDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

void CHBMAEDDriver::SelectDevice(void)
{
	m_uPtr = 0;

	if( m_PrevSelect == m_pCtx->m_bDrop ) return;

	m_PrevSelect = m_pCtx->m_bDrop;

	AddByte(';');
	AddByte('S');
	AddByte(m_pHex[m_pCtx->m_bDrop/10]);
	AddByte(m_pHex[m_pCtx->m_bDrop%10]);
	AddByte(';');

	Put();

	Sleep(10);

	m_uPtr = 0;
	}

BOOL CHBMAEDDriver::CheckReject(void)
{
	return m_bRx[0] == '?';
	}

// COF Handling

BOOL CHBMAEDDriver::SetCOF(void)
{
	if( IsUserString(m_pItem->Table) ) {

		return TRUE;
		}

	if( (m_fCOF13 = TestCOF(TRUE)) ) {

		return TRUE;
		}

	return TestCOF(FALSE);
	}

BOOL CHBMAEDDriver::TestCOF(BOOL fIs13)
{
	m_uPtr = 0;

	AddByte(';');
	AddByte('C');
	AddByte('O');
	AddByte('F');

	if( fIs13 ) {

		AddByte('1');
		}

	AddByte('3');

 	Transact(WRITERESPONSE);

	Sleep(10);

	return m_bRx[0] == '0';
	}

BOOL CHBMAEDDriver::GetCOF(void)
{
	DWORD Data;
 
	CAddress A;

	A.a.m_Offset = COF;
	A.a.m_Table  = addrNamed;
	A.a.m_Extra  = 0;
	A.a.m_Type   = addrLongAsLong;

	return Read(A, &Data, 1) == 1 ? !InvalidCOF(Data) || SetCOF() : FALSE;
	}

BOOL CHBMAEDDriver::InvalidCOF(DWORD dData)
{
	return dData != ASCNOT2WIRE;
	}

// No Transmit Handling

BOOL CHBMAEDDriver::NoReadTransmit( PDWORD pData, UINT uCount )
{
	switch( m_pItem->Table ) {

		case BRK:
		case CAL:
		case CDL:
		case CPV:
		case CSN:
		case CTR:
		case DPW:
		case DZT:
		case LIV:
		case NTF:
		case PVS:
		case RES:
		case RUN:
		case TAR:
		case SPW:
		case TRC:
		case WDP:
		case USR:
			*pData = 0;
			return TRUE;

		case DP1:
			GetPassword( pData, &(m_pCtx->m_Password[0]), 4 );
			return TRUE;

		case DP2:
			GetPassword( pData, &(m_pCtx->m_Password[4]), 3 );
			return TRUE;

		case SP1:
			GetPassword( pData, &(m_pCtx->m_SetPW[0]), 4 );
			return TRUE;

		case SP2:
			GetPassword( pData, &(m_pCtx->m_SetPW[4]), 3 );
			return TRUE;

		case LDST:
			*pData = m_pCtx->m_fLDW;
			return TRUE;

		case LWST:
			*pData = m_pCtx->m_fLWT;
			return TRUE;

		case LW1M:
		case LW1I:
		case LW1O:
		case LW1F:
			*pData = m_pCtx->m_LIV1[m_pItem->Table - LW1M];
			return TRUE;

		case LW2M:
		case LW2I:
		case LW2O:
		case LW2F:
			*pData = m_pCtx->m_LIV2[m_pItem->Table - LW2M];
			return TRUE;

		case LW3M:
		case LW3I:
		case LW3O:
		case LW3F:
			*pData = m_pCtx->m_LIV3[m_pItem->Table - LW3M];
			return TRUE;

		case LW4M:
		case LW4I:
		case LW4O:
		case LW4F:
			*pData = m_pCtx->m_LIV4[m_pItem->Table - LW4M];
			return TRUE;

		case SEL:
		case TDD:
			*pData = 0xFFFFFFFF;
			return TRUE;

		case TW1:
		case TW2:
		case TW3:
		case TW4:
		case TW5:
			*pData = m_pCtx->m_TRC[m_pItem->Table - TW1];
			return TRUE;

		case DZT1:
		case DZT2:
			*pData = m_pCtx->m_DZT[m_pItem->Table - DZT1];
			return TRUE;

		case NTW1:
		case NTW2:
			*pData = m_pCtx->m_NTF[m_pItem->Table - NTW1];
			return TRUE;

		case PVSL:
		case PVSH:
			*pData = m_pCtx->m_PVS[m_pItem->Table - PVSL];
			return TRUE;

		case USRC:
			TransferString(PBYTE(pData), m_pCtx->m_bUSRC, uCount);
			return TRUE;

		case USRR:
			TransferString(PBYTE(pData), m_pCtx->m_bUSRR, uCount);
			return TRUE;
		}

	return FALSE;
	}

BOOL CHBMAEDDriver::NoWriteTransmit( PDWORD pData, UINT uCount )
{
	DWORD dData = * pData;

	switch( m_pItem->Table ) {

		case BRK:
		case CAL:
		case CDL:
		case CPV:
		case CSN:
		case CTR:
		case DPW:
		case DZT:
		case LIV:
		case NTF:
		case PVS:
		case PW1:
		case PW2:
		case RES:
		case RUN:
		case TAR:
		case TRC:
		case USR:
			return !dData;

		case AOV:
		case CFT:
		case COF:
		case CWL:
		case DST:
		case ESR:
		case FFT:
		case FNB:
		case FRS:
		case MAV:
		case MAVR:
		case MSV:
		case MSVR:
		case NDS:
		case PVAL:
		case PVAH:
		case SDM:
		case SDO:
		case SDS:
		case SOV:
		case SUM:
		case TCR:
		case TR1:
		case TR2:
		case TR3:
		case TR4:
		case TR5:
		case TRM:
		case TRN:
		case TRS:
		case ID0:
		case ID1:
		case ID2:
		case ID3:
		case ID4:
		case ID5:
		case ID6:
		case ID7:
		case DGRV:
		case DGRS:
		case RIO:	return TRUE;

		case DP1:
			StorePassword( dData, &(m_pCtx->m_Password[0]), 4 );
			if( m_pCtx->m_Password[3] == 0 ) memset(&(m_pCtx->m_Password[4]), 0, 3);
			return TRUE;

		case DP2:	StorePassword( dData, &(m_pCtx->m_Password[4]), 3 );	return TRUE;

		case LDST:	m_pCtx->m_fLDW = FALSE;				return TRUE;

		case LWST:	m_pCtx->m_fLWT = FALSE;				return TRUE;
 
		case LW1M:
		case LW1I:
		case LW1O:
		case LW1F:	m_pCtx->m_LIV1[m_pItem->Table - LW1M] = dData;		return TRUE;
 
		case LW2M:
		case LW2I:
		case LW2O:
		case LW2F:	m_pCtx->m_LIV2[m_pItem->Table - LW2M] = dData;		return TRUE;
 
		case LW3M:
		case LW3I:
		case LW3O:
		case LW3F:	m_pCtx->m_LIV3[m_pItem->Table - LW3M] = dData;		return TRUE;
 
		case LW4M:
		case LW4I:
		case LW4O:
		case LW4F:	m_pCtx->m_LIV4[m_pItem->Table - LW4M] = dData;		return TRUE;

		case SP1:
			StorePassword( dData, &(m_pCtx->m_SetPW[0]), 4 );
			if( m_pCtx->m_SetPW[3] == 0 ) memset(&(m_pCtx->m_SetPW[4]), 0, 3);
			return TRUE;

		case SP2:	StorePassword( dData, &(m_pCtx->m_SetPW[4]), 3 );	return TRUE;

		case TW1:
		case TW2:
		case TW3:
		case TW4:
		case TW5:	m_pCtx->m_TRC[m_pItem->Table - TW1] = dData;		return TRUE;

		case DZT1:
		case DZT2:	m_pCtx->m_DZT[m_pItem->Table - DZT1] = dData;		return TRUE;

		case NTW1:
		case NTW2:	m_pCtx->m_NTF[m_pItem->Table - NTW1] = dData;		return TRUE;

		case PVSL:
		case PVSH:	m_pCtx->m_PVS[m_pItem->Table - PVSL] = dData;		return TRUE;

		case USRC:	TransferString(m_pCtx->m_bUSRC, PBYTE(pData), uCount);	return TRUE;

		case USRR:	memset(m_pCtx->m_bUSRR, 0, 40);				return TRUE;
		}

	return FALSE;
	}

// Item Handling

UINT CHBMAEDDriver::GetParameterSize(void)
{
	return m_pItem->ParSize;
	}

void CHBMAEDDriver::SetpItem(UINT uID)
{
	m_pItem = m_pCL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == m_pItem->Table ) return;

		m_pItem++;
		}

	m_pItem = NULL;
	}

void CHBMAEDDriver::SetLDW_LWTRead(void)
{
	BOOL fAck = m_bRx[1] == CR;

	switch( m_pItem->Table ) {

		case LDC:
		case LDV:
			m_pCtx->m_fLDW = fAck && m_bRx[0] != '0';
			return;

		case LWC:
		case LWV:
			m_pCtx->m_fLWT = fAck && m_bRx[0] != '0';
			break;
		}
	}

DWORD CHBMAEDDriver::CheckSign(DWORD dData)
{
	if( dData & 0x80000000 ) {

		AddByte('-');

		dData = (dData ^ 0xFFFFFFFF) + 1;
		}

	else {
		if( NeedPlus() ) AddByte('+');
		}

	return dData;
	}

BOOL CHBMAEDDriver::NeedPlus(void)
{
	switch( m_pItem->Table ) {

		case LC0:
		case LC1:
		case LC2:
		case LC3:
		case SDM:
		case SDS:
		case TAV:	return TRUE;
		}

	return FALSE;
	}

BOOL CHBMAEDDriver::IsMSV(void)
{
	return m_pItem->Table == MSV  || m_pItem->Table == MSVR;
	}

BOOL CHBMAEDDriver::IsReal(void)
{
	return m_pItem->Table == MAVR || m_pItem->Table == MSVR;
	}

UINT CHBMAEDDriver::GetMSVLF(void)
{
	return m_fCOF13 ? 1 : 2;
	}

// I/O Handling

BOOL CHBMAEDDriver::HandleIO(PDWORD pData, BOOL fIsWrite)
{
	SelectDevice();

	DWORD dNow;

	WORD wResult = ReadIO(&dNow); // try to read current values

	if( !wResult ) {

		return FALSE; // read failed totally
		}

	if( fIsWrite ) {

		DWORD dReverse = dNow;

		BYTE b1  = LOBYTE(LOWORD(dReverse)) & 0xF;

		BYTE b2	 =  b1 >> 3;
		b2	|= (b1 >> 1) & 0x2;
		b2	|= (b1 << 1) & 0x4;
		b2	|= (b1 << 3) & 0x8;

		dReverse &= 0xFFFFFFF0;
		dReverse |= b2;

		HandleOutWrite(*pData, dReverse, wResult == 2);
		}

	else {
		*pData = dNow;
		}
	
	return TRUE;
	}

void CHBMAEDDriver::HandleOutWrite(DWORD dNew, DWORD dNow, BOOL fBoth)
{
	switch( LOBYTE(dNew) & OUTFUNC) {

		case ONLY12:

			SendOutWrite(dNew & 3, POR);

			return;

		case ALLOUTS:

			SendOutWrite(dNew & 3, POR);
			// Fall through to send 3-6

		case ONLY36:

			if( fBoth ) SendOutWrite((dNew >> 4) & 0xF, MUX);

			return;

		case TOGOUTS:

			if( !fBoth ) {

				dNew &= 3; // 3-6 don't exist, clear toggle bits
				}

			ToggleFlaggedOutputs(dNew & OUTMASK, dNow, fBoth);

			return;
		}
	}

void CHBMAEDDriver::ToggleFlaggedOutputs(DWORD dNew, DWORD dNow, BOOL fBoth)
{
	DWORD dData = 0;

	switch(LOBYTE(LOWORD(dNew)) & 3) {

		case 1: // toggle output 1 only

			dData = dNow & 2;

			if( !(dNow & 1) ) dData |= 1;

			SendOutWrite(dData, POR);

			break;

		case 2: // do output 2 only

			dData = dNow & 1;

			if( !(dNow & 2) ) dData |= 2;

			SendOutWrite(dData, POR);

			break;

		case 3: // change both outputs

			SendOutWrite((dNew ^ dNow) & 3, POR);

			break;
		}

		dNew &= 0xF0; // 3-6 bits to toggle

	if( fBoth && dNew ) {

		dNow &= 0xF0; // current 3-6 bit status

		dData = (dNew ^ dNow) >> 4; // toggle desired bits

		if( dNew ) { // bit(s) to be toggled

			SendOutWrite(dData, MUX); // toggle selected bits
			}
		}
	}

WORD CHBMAEDDriver::ReadIO(PDWORD pData)
{
	SetpItem(POR);

	PutReadInfo();

	if( Transact(MULTIRESPONSE) ) {

		GetResponse(pData, MULTIRESPONSE, 1);
		}

	else {
		return 0;
		}

	SetpItem(MUX);

	PutReadInfo();

	memset(m_bRx, 0, 12);

	BOOL f = Transact(MULTIRESPONSE); // try to read outputs 3-6

	if( f && m_bRx[0] != '?' ) { // MUX exists in this device

		for( UINT i = 0; i < 12; i++ ) {

			BYTE c = m_bRx[i];

			if( c == '0' || c == '1' ) {

				BYTE b  = (c - '0') * 10;

				b      += m_bRx[i+1] - '0';

				*pData += b << 4;

				return 2; // output 3 and higher available
				}

			if( !c || c == CR ) {

				return 1; // only outputs 1 and 2 available
				}
			}
		}

	return 1; // only O1 and O2 are available
	}

void CHBMAEDDriver::SendOutWrite(DWORD dData, UINT uTable)
{
	SetpItem(uTable);

	PutWriteInfo(dData);

	memset(m_bRx, 0, 12);

	Transact(WRITERESPONSE);
	}

// Data Handling

void CHBMAEDDriver::PutGeneric(DWORD dData)
{
	DWORD dDivisor = 1000000000;

	UINT u;

	BOOL fFirst = FALSE;

	while( dDivisor ) {

		u = (dData/dDivisor) % 10;

		if( u || fFirst ) {

			AddByte( m_pHex[u] );

			fFirst = TRUE;
			}

		dDivisor /= 10;
		}

	if( !fFirst ) AddByte('0');
	}

BOOL CHBMAEDDriver::GetGeneric(UINT * pPos, PDWORD pData)
{
	DWORD d = 0L;

	UINT i = *pPos;

	BOOL fNeg = FALSE;

	BOOL fDone = FALSE;

	if( m_bRx[i] < '0' || m_bRx[i] > '9' ) {

		while( !fDone ) {

			if( m_bRx[i] >= '0' && m_bRx[i] <= '9' ) fDone = TRUE;

			else {
				switch( m_bRx[i++] ) {

					case '-':
						fNeg = TRUE;
						// fall through
					case '+':
					case ' ':
					case ',':
						break;
					}

				if( i >= sizeof(m_bRx) ) return FALSE;
				}
			}
		}

	while( m_bRx[i] >= '0' && m_bRx[i] <= '9' ) {

		d *= 10;

		d += m_bRx[i++] - '0';

		if( i > sizeof(m_bRx) ) return FALSE;
		}

	*pPos = i+1;

	*pData = fNeg ? -d : d;

	return TRUE;
	}

DWORD CHBMAEDDriver::GetAscii(UINT * pPos)
{
	UINT uPos = *pPos;

	DWORD d = 0;

	for( UINT i = uPos; i < uPos+4; i++ ) {

		d <<= 8;

		d += m_bRx[i];
		}

	return d;
	}

BOOL CHBMAEDDriver::DataToASCII(DWORD dData, UINT uCt)
{
	BYTE b;

	UINT uShiftStart = 8 * (uCt - 1);

	while( uCt-- ) {

		b = (dData >> uShiftStart) & 0xFF;

		if( !b ) b = ' ';

		if( b < ' ' || b > 0x7E ) return FALSE;

		else AddByte(b);

		uShiftStart -= 8;
		}

	return TRUE;
	}

// Password Handling

void CHBMAEDDriver::StorePassword(DWORD dData, PBYTE pb, UINT uCt)
{
	BYTE b;

	UINT uShift = uCt == 4 ? 24 : 16;

	UINT i = 0;

	UINT uPos = uCt;

	while( uPos ) {

		b = (dData >> uShift) & 0xFF;

		if( IsValidPWChar(b) == 2 ) pb[i++] = b;

		uShift -= 8;

		uPos--;
		}

	while( i < uCt ) pb[i++] = 0;
	}

void CHBMAEDDriver::GetPassword(PDWORD pData, PBYTE pb, UINT uCt)
{
	DWORD dData = 0;

	for( UINT i = 0; i < uCt; i++ ) dData = (dData << 8) + pb[i];

	if( uCt == 3 ) dData <<= 8;

	*pData = dData;
	}

UINT CHBMAEDDriver::IsValidPWChar(BYTE b)
{
	return	(b >= '0' && b <= '9') ||
		(b >= 'A' && b <= 'Z') ||
		(b >= 'a' && b <= 'z')

		? 2 : !b ? 1 : 0;
	}

BOOL CHBMAEDDriver::IsValidPW(UINT * pPos, PBYTE pPW)
{
	BOOL fFirst = FALSE;
	BOOL fNull  = FALSE;
	BOOL fOK    = TRUE;

	for( UINT i = 0; fOK && (i < 7); i++ ) {

		switch( IsValidPWChar(pPW[i]) ) {

			case 1:
				fNull = fFirst; // null following some valid character

				break;

			case 2:
				fOK = !fNull; // Check if null found in middle of string

				if( !fFirst ) {

					*pPos = i;

					fFirst = TRUE;
					}
				break;

			case 0:
			default:
				fOK = FALSE;
				break;
			}
		}

	if( fOK && fFirst ) return TRUE;

	ClearPW(pPW);

	return FALSE;
	}

void CHBMAEDDriver::ClearPW(PBYTE pPW)
{
	for( UINT i = 0; i < 7; i++ ) pPW[i] = 0;
	}

// Response Timing

BOOL CHBMAEDDriver::CheckLongReply(void)
{
	return GetReply(TRUE);
	}

void CHBMAEDDriver::LongWriteTimeout(void)
{
	if( (++m_pCtx->m_uLongWaitCtr) > LONGTIMEOUT ) m_pCtx->m_fLongWaitTimeout  = TRUE;
	}

BOOL CHBMAEDDriver::ProcessLongResponse(void)
{
	switch( m_pItem->Table ) { // Check for long response opcodes

		case SFC:
		case SFV:
		case SZC:
		case LDC:
		case LWC:
		case CAL:
		case RES:
		case TDD:
			break;

		default:
			return FALSE; // response might not include command characters
		}

	if( m_bRx[1] == CR && m_bRx[2] == LF ) {

		if( m_bRx[0] == '0' ) {

			m_pCtx->m_uLongWaitFn = LONGWRITEOK;
			return TRUE;
			}

		else {
			if( m_bRx[0] == '?' ) {

				m_pCtx->m_uLongWaitFn = LONGWRITEERR;
				return TRUE;
				}
			}
		}

	return FALSE;
	}

UINT CHBMAEDDriver::CheckLongFunction(void)
{
	switch( m_pCtx->m_uLongWaitFn ) {

		case LONGWRITEBUSY: // no response received, yet

			LongWriteTimeout(); // tick timeout

			if( !CheckLongReply() || !ProcessLongResponse() ) return 2;

			break;

		// Response received during a Read
		case LONGWRITEOK: // ack
		case LONGWRITEERR: // nak

			break;

		default:
			return 0;
		}

	return 1;
	}

BOOL CHBMAEDDriver::CheckLongBusy(void)
{
	if( m_pCtx->m_uLongWaitFn ) {

		LongWriteTimeout(); // tick timeout

		if( m_pCtx->m_uLongWaitFn == LONGWRITEBUSY ) {

			if( CheckLongReply() && ProcessLongResponse() ) {

				m_pCtx->m_fLongWaitTimeout = FALSE;

				m_pCtx->m_uLongWaitCtr     = 0;
				}
			}

		if( m_pCtx->m_uLongWaitFn == LONGWRITEBUSY ) { // recheck

			if( !m_pCtx->m_fLongWaitTimeout ) return TRUE;

			else m_pCtx->m_uLongWaitFn = LONGWRITEOFF;
			}
		}

	return FALSE;
	}

void CHBMAEDDriver::ClearLongWait(void)
{
	m_pCtx->m_uLongWaitFn  = LONGWRITEOFF;
	m_pCtx->m_uLongWaitCtr = 0;
	}

// String Handling

BOOL CHBMAEDDriver::AddENU(DWORD dData)
{
	AddByte('"');

	if( !DataToASCII(dData, 4) ) return FALSE;

	AddByte('"');

	return TRUE;
	}

BOOL CHBMAEDDriver::IsUserString(UINT uTable)
{
	return uTable == USR || uTable == USRC || uTable == USRR;
	}

void CHBMAEDDriver::TransferString(PBYTE pDest, PBYTE pSrc, UINT uCount)
{
	UINT uCt = uCount * sizeof(DWORD);

	for( UINT i = 0; i < uCt - 1; i++ ) {

		BYTE b   = pSrc[i];

		pDest[i] = b;

		switch( b ) {

			case 0:
				return;

			case CR:
				pDest[i]   = 0;
				return;

			case ';':
				pDest[i+1] = 0;
				return;
			}
		}

	pDest[uCt] = 0;
	}

// End of File
