//////////////////////////////////////////////////////////////////////////
//
// CTC 2xxx Spaces
//

#define SPACE_R		9
#define SPACE_R_16	77
#define SPACE_F		17
#define SPACE_I		15
#define SPACE_O		21
#define SPACE_AI	29
#define SPACE_AO	31
#define SPACE_SP	23
#define SPACE_SE	47
#define SPACE_SI	27
#define SPACE_STRT	addrNamed + 1
#define SPACE_STP	addrNamed + 2
#define SPACE_RST	addrNamed + 3

//////////////////////////////////////////////////////////////////////////
//
// CTC 2xxx Master Serial Driver
//

class CCTC2xxxDriver : public CMasterDriver
{
	public:
		// Constructor
		CCTC2xxxDriver(void);

		// Destructor
		~CCTC2xxxDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected: 

		// Device Data
		struct CContext
		{
			BOOL m_fAscii;
			BYTE m_bDrop;
			};
		CContext *	m_pCtx;

		// Data Members
		BYTE m_bTxBuff[72];
		BYTE m_bRxBuff[72];
		UINT m_uPtr;
		BYTE m_bCheck;
		LPCTXT m_pHex;

		// Read Handlers

		CCODE ReadAscii(AREF Addr, PDWORD pData, UINT uCount);
		CCODE ReadBinary(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers

		CCODE WriteAscii(AREF Addr, PDWORD pData, UINT uCount);
		CCODE WriteBinary(AREF Addr, PDWORD pData, UINT uCount);

		// Frame Building

		void Begin(void);
		void AddByte(BYTE bByte);
		BOOL AddAsciiPrefix(UINT uTable);
		void AddAscii(DWORD dData, UINT uRadix, UINT uFactor);
		BOOL AddBinaryPrefix(UINT uTable, BOOL fWrite = FALSE);
		void AddBinary(DWORD dData, UINT uTable);
		void AddTerm(void);
		BYTE GetChecksum(void);

		// Transport

		BOOL Transact(BOOL fWrite = FALSE);
		BOOL TxData(void);
		BOOL RxData(BOOL fWrite);
		BOOL RxAsciiData(BOOL fWrite);
		BOOL RxBinaryData(BOOL fWrite);
		BOOL CheckData(void);
		BOOL CheckBinaryResponse(void);
		BOOL GetAsciiData(UINT uType, PDWORD pData);
		BOOL GetBinaryData(UINT uTable, UINT uType, PDWORD pData, UINT uOffset, UINT uCount);

		
		// Helpers

		UINT GetFactor(UINT uData, UINT uRadix);
		BOOL IsPrefixValid(UINT uTable);
		BYTE FromAscii(BYTE bByte);
		BOOL IsDigit(BYTE bByte);
		UINT GetByteCount(UINT uTable);
		UINT GetDataByteCount(UINT uTable);
		BOOL IsBank(UINT uTable);
		UINT GetBinaryCount(UINT &uTable, UINT uCount, UINT &uOffset);
		BOOL IsMulti(UINT uTable); 
		BOOL IsReg(UINT uTable);
		BOOL IsFlag(UINT uTable);

	};

// End of File
