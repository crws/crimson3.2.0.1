#include "intern.hpp"

#include "ctc2xxx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CTC 2xxx Master Driver
//

// Instantiator

INSTANTIATE(CCTC2xxxDriver);

// Constructor

CCTC2xxxDriver::CCTC2xxxDriver(void)
{
	m_Ident       = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Destructor

CCTC2xxxDriver::~CCTC2xxxDriver(void)
{
	}

// Configuration

void MCALL CCTC2xxxDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CCTC2xxxDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CCTC2xxxDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CCTC2xxxDriver::Open(void)
{
	}

// Device

CCODE MCALL CCTC2xxxDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_fAscii = GetByte(pData) ? TRUE : FALSE;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CCTC2xxxDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CCTC2xxxDriver::Ping(void)
{
	if( m_pCtx->m_fAscii ) {

		Begin();

		AddByte('P');

		AddByte('C');

		AddByte(CR);

		if( Transact() ) {

			return CCODE_SUCCESS;
			}
		}

	else {

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = 9;
		Addr.a.m_Offset = 1;
		Addr.a.m_Type   = addrLongAsLong;
		Addr.a.m_Extra	= 0;

		return Read(Addr, Data, 1); 
		}

	return CCODE_ERROR;
	}

CCODE MCALL CCTC2xxxDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pCtx->m_fAscii ? ReadAscii(Addr, pData, uCount) : ReadBinary(Addr, pData, uCount);
	}

CCODE MCALL CCTC2xxxDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pCtx->m_fAscii ? WriteAscii(Addr, pData, uCount) : WriteBinary(Addr, pData, uCount);
	}

// Read Handlers

CCODE CCTC2xxxDriver::ReadAscii(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	if( Addr.a.m_Table >= addrNamed ) {

		pData[0] = 0;

		return uCount;
		}
	
	Begin();
	
	AddByte('N');

	AddByte(m_pCtx->m_bDrop);
	
	if( AddAsciiPrefix(Addr.a.m_Table) ) {

		AddAscii(Addr.a.m_Offset, 10, GetFactor(Addr.a.m_Offset, 10));

		AddTerm();

		if( Transact() ) {

			if( GetAsciiData(Addr.a.m_Type, pData) ) {

				return uCount;
				}
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CCTC2xxxDriver::ReadBinary(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable = Addr.a.m_Table;

	UINT uOffset = Addr.a.m_Offset;

	uCount = GetBinaryCount(uTable, uCount, uOffset);

	Begin();

	AddByte(0x01);

	AddByte(0x00);

	m_bCheck = 0;

	if( AddBinaryPrefix(uTable) ) {

		UINT uBytes = GetByteCount(uTable);

		uOffset = IsReg(uTable)  ? uOffset : uOffset - 1;

		uOffset = IsBank(uTable) ? (uOffset - 1) / 8 : uOffset;

		for( UINT u = uBytes; u > 0; u-- ) {

			AddByte((uOffset >> ( ( u - 1 ) * 8 )) & 0xFF);
			}
			
		AddByte(GetChecksum());

		AddByte(0xFF);

		m_bTxBuff[1] = m_uPtr - 2;

		if( Transact() ) { 

			if( GetBinaryData(uTable, Addr.a.m_Type, pData, uOffset, uCount) ) {

				return uCount;
				}
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Write Handlers

CCODE CCTC2xxxDriver::WriteAscii(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);
	
	Begin();
	
	AddByte('N');

	AddByte(m_pCtx->m_bDrop);

	if( AddAsciiPrefix(Addr.a.m_Table) ) {

		if( Addr.a.m_Table < addrNamed ) {

			AddAscii(Addr.a.m_Offset, 10, GetFactor(Addr.a.m_Offset, 10));

			AddByte('=');

			AddAscii(*pData, 10, GetFactor(*pData, 10));
			}

		AddTerm();

		if( Transact(TRUE) ) {

			return uCount;
			}

		return CCODE_ERROR;
		} 

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CCTC2xxxDriver::WriteBinary(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	Begin();

	AddByte(0x01);

	UINT uTable = Addr.a.m_Table;

	UINT uBytes = GetByteCount(uTable);

	AddByte(1 + 2 + uBytes + GetDataByteCount(uTable));

	m_bCheck = 0; 

        if( AddBinaryPrefix(Addr.a.m_Table + 2, TRUE) ) {

	       	UINT uOffset = IsReg(Addr.a.m_Table) ? Addr.a.m_Offset : Addr.a.m_Offset - 1;

		for( UINT u = uBytes; u > 0; u-- ) {

			AddByte((uOffset >> ( ( u - 1 ) * 8 )) & 0xFF);
			}

		AddBinary(pData[0], Addr.a.m_Table);
			
		AddByte(GetChecksum());

		AddByte(0xFF);

		if( Transact(TRUE) ) { 
	
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;

	}

// Frame Building

void CCTC2xxxDriver::Begin(void)
{
	m_uPtr = 0;

	m_bCheck = 0;
	}

void CCTC2xxxDriver::AddByte(BYTE bByte)
{
	m_bTxBuff[m_uPtr++] = bByte;

	m_bCheck += bByte;
	}

BOOL CCTC2xxxDriver::AddAsciiPrefix(UINT uTable)
{
	if( IsPrefixValid(uTable) ) {
	
		switch( uTable ) {

			case SPACE_R:

				AddByte('R');
				break;

			case SPACE_F:

				AddByte('F');
				break;

			case SPACE_AI:

				AddByte('A');

			case SPACE_I:

				AddByte('I');
				break;

			case SPACE_AO:
				
				AddByte('A');

			case SPACE_O:

				AddByte('O');
				break;

			case SPACE_STRT:

				AddByte('+');
				break;

			case SPACE_STP:

				AddByte('-');
				break;

			case SPACE_RST:

				AddByte('*');
				break;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCTC2xxxDriver::AddBinaryPrefix(UINT uTable, BOOL fWrite)
{
	UINT uSpace = fWrite ? uTable - 2 : uTable;
	
	if( IsPrefixValid(uSpace) ) {
	
		AddByte(uTable);

		return TRUE;
		}

	return FALSE;
	}

void CCTC2xxxDriver::AddAscii(DWORD dData, UINT uRadix, UINT uFactor)
{
	while( uFactor ) {
		
		BYTE bData = m_pHex[(dData / uFactor) % uRadix];
		
		AddByte(bData);
		
		uFactor /= uRadix;
		}
	}

void CCTC2xxxDriver::AddBinary(DWORD dData, UINT uTable)
{	
	UINT uBytes = GetDataByteCount(uTable);

	for( UINT u = 0; u <= uBytes - 1; u++ ) {

		if( IsFlag(uTable) ) {

			BYTE bData = (dData >> ( u * 8 )) & 0xFF;

			bData = bData ? 0xFF : 0x00;

			AddByte(bData);
			}
		else {
			AddByte((dData >> ( u * 8 )) & 0xFF);
			}

		if( u == uBytes - 1 ) {

			break;
			} 
		}
	}

void CCTC2xxxDriver::AddTerm(void)
{
	AddByte(CR);
	}

BYTE CCTC2xxxDriver::GetChecksum(void)
{
	return (0xFF - (m_bCheck % 256));
	}

// Transport

BOOL CCTC2xxxDriver::Transact(BOOL fWrite)
{
	if( TxData() && RxData(fWrite) ) {

		if( CheckData() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCTC2xxxDriver::TxData(void)
{
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CCTC2xxxDriver::RxData(BOOL fWrite)
{
	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));
	
	if( m_pCtx->m_fAscii ) {

		return RxAsciiData(fWrite);
		}
	
	return RxBinaryData(fWrite);
	} 

BOOL CCTC2xxxDriver::RxAsciiData(BOOL fWrite)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_uPtr = 0;

	BOOL fData = FALSE;

	BOOL fCR = FALSE;

	BOOL fLF = FALSE;

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {

		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( !fData ) {

			if ( uData == LF || uData == CR ) {

				if( fWrite ) {

					return TRUE;
					}

				fLF = uData == LF;

				continue;
				}

			fData = TRUE;
			}

		if( uData == CR ) {

			if( !fLF ) {

				return TRUE;
				}

			fCR = TRUE;

			continue;
			}

		if( uData == LF ) {

			return fLF && fCR;
			}

		if(m_uPtr <= 1 && uData == 0x07) {

			return FALSE;
			}

		BYTE bByte = FromAscii(LOBYTE(uData));

		if( IsDigit(bByte) ) {

			m_bRxBuff[m_uPtr++] = bByte;
			}
		}

	return FALSE;
	}

BOOL CCTC2xxxDriver::RxBinaryData(BOOL fWrite)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_uPtr = 0;

	m_bCheck = 0;

	BOOL fData = FALSE;

	UINT uLen = 0;

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if ( !fData ) {

			uLen = uData;

			fData = TRUE;

			m_bRxBuff[m_uPtr++] = uData;
			
			continue;
			}

		if( m_uPtr <= uLen ) {

			if( m_uPtr < uLen - 1 ) {

				m_bCheck += uData;
				}
	
			m_bRxBuff[m_uPtr++] = uData;
			}

		if( m_uPtr > uLen ) {

			return ( m_bRxBuff[uLen] == 0xFF && m_bRxBuff[uLen - 1] == GetChecksum() );
			}
		}
	
	return FALSE;
	}

BOOL CCTC2xxxDriver::CheckData(void)
{
	if( m_pCtx->m_fAscii ) {

		return TRUE;
		}

	return CheckBinaryResponse();
	}

BOOL CCTC2xxxDriver::CheckBinaryResponse(void)
{
	BYTE bResult = PBYTE(m_bRxBuff + 1)[0];
	
	switch( m_bTxBuff[2] ) {

		case SPACE_R:
			return bResult == 0x0A;

		case SPACE_R_16:
			return bResult == 0x4E;
			
		case SPACE_F:
			return bResult == 0x12;

		case SPACE_I:
			return bResult == 0x10;
	       
		case SPACE_O:
			return bResult == 0x16;

		case SPACE_AI:
			return bResult == 0x1E;

		case SPACE_AO:
			return bResult == 0x20;

		case SPACE_SP:
			return bResult == 0x18;

		case SPACE_SE:
			return bResult == 0x30;

		case SPACE_SI:
			return bResult == 0x1C;

		case SPACE_R + 2:
		case SPACE_F + 2:
		//case SPACE_O + 2:
		case SPACE_AO + 2:
		
			return m_bRxBuff[0] == 3 && bResult == 0x64;
		}

	return FALSE;
	}

BOOL CCTC2xxxDriver::GetAsciiData(UINT uType, PDWORD pData)
{
	pData[0] = 0;

	UINT uMult = 1;

	UINT u = 0;

	for( u = 0; u < m_uPtr - 1; u++, uMult *= 10 );

	for( u = 0; u < m_uPtr; u++, uMult /= 10 ) {

		pData[0] += m_bRxBuff[u] * uMult;
		}

	return TRUE;
	}

BOOL CCTC2xxxDriver::GetBinaryData(UINT uTable, UINT uType, PDWORD pData, UINT uOffset, UINT uCount)
{
	if( uType == addrLongAsLong ) {

		UINT uHeader = IsMulti(uTable) ? 4 : 2;

		for( UINT u = 0; u < uCount; u++ ) {

			DWORD x = PDWORD(m_bRxBuff + uHeader)[u];
		
			pData[u] = IntelToHost(x);
			}

		return TRUE;
		}

	if( uType == addrWordAsWord ) {

		WORD x = PWORD(m_bRxBuff + 2)[0];

		pData[0] = LONG(SHORT(IntelToHost(x)));

		return TRUE;
		}

	if( uType == addrBitAsBit ) {

		BYTE x = PBYTE(m_bRxBuff + 2)[0];

		if( IsBank(uTable) ) {

			pData[0] = (x & (uOffset % 8) ) ? 1 : 0;
			
			return TRUE;
			}
	       
		pData[0] = x == 0xFF ? 1 : 0;

		return TRUE;
		}
		
	return FALSE;
	}

// Helpers

UINT CCTC2xxxDriver::GetFactor(UINT uData, UINT uRadix)
{
	UINT uFactor = 1;

	while( uData / (uFactor * uRadix) >= 1 ) {

		uFactor *= uRadix;
		}

	return uFactor;
	}

BOOL CCTC2xxxDriver::IsPrefixValid(UINT uTable)
{
	switch( uTable ) {

		case SPACE_R:
		case SPACE_F:
		case SPACE_I:
		case SPACE_O:
		case SPACE_AI:
		case SPACE_AO:
		case SPACE_STRT:
		case SPACE_STP:
		case SPACE_RST:

			return TRUE;

		case SPACE_R_16:
		case SPACE_SP:
		case SPACE_SE:
		case SPACE_SI:

			return m_pCtx->m_fAscii ? FALSE : TRUE;

		}

	return FALSE;
	}

BYTE CCTC2xxxDriver::FromAscii(BYTE bByte)
{
	if( bByte >= '0' && bByte <= '9' )

		return bByte - '0';

	return	bByte - '@' + 9;
	}

BOOL CCTC2xxxDriver::IsDigit(BYTE bByte)
{
	return( bByte <= 9 );
	}

UINT CCTC2xxxDriver::GetByteCount(UINT uTable)
{
	switch( uTable ) {

		
		case SPACE_F:
		case SPACE_I:
		case SPACE_O:
			return 1;

		case SPACE_R:
		case SPACE_R_16:
		case SPACE_AI:
		case SPACE_AO:
		case SPACE_STRT:
		case SPACE_STP:
		case SPACE_RST:
		case SPACE_SI:
			return 2;

		
		case SPACE_SP:
		case SPACE_SE:
			return 4;
       		}    

	return 0;
	}

UINT CCTC2xxxDriver::GetDataByteCount(UINT uTable)
{
	switch( uTable ) {

		case SPACE_F:
		case SPACE_I:
		case SPACE_O:
			return 1;
		
		case SPACE_R:
		case SPACE_R_16:
			return 4;

		case SPACE_AO:
			return 2;
		} 

	return 0;
	}

BOOL  CCTC2xxxDriver::IsBank(UINT uTable)
{
	if( !m_pCtx->m_fAscii )	{

		switch( uTable ) {

			case SPACE_I:
			case SPACE_O:

				return TRUE;
			}
		}

	return FALSE;
	}

UINT  CCTC2xxxDriver::GetBinaryCount(UINT &uTable, UINT uCount, UINT &uOffset)
{
	if( uTable == SPACE_R && uCount >= 16 ) {

		uTable = SPACE_R_16;

		uOffset = uOffset - 1;
		}

	return ( uTable == SPACE_R_16 ) ? 16 : 1;
	}

BOOL CCTC2xxxDriver::IsMulti(UINT uTable) 
{
	switch( uTable ) {

		case SPACE_R_16:
			
			return TRUE;
		}

	return FALSE;
	}

BOOL CCTC2xxxDriver::IsReg(UINT uTable)
{      
	switch( uTable ) {

		case SPACE_R:
		case SPACE_R_16:
			
			return TRUE;
		}

	return FALSE;
	}

BOOL CCTC2xxxDriver::IsFlag(UINT uTable)
{
	switch( uTable ) {

		case SPACE_F:
		
			return TRUE;
		}

	return FALSE;
	}

// End of File

