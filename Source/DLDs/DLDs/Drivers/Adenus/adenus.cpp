
#include "intern.hpp"

#include "adenus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Adenus Telnet Driver
//

// Instantiator

INSTANTIATE(CAdenusDriver);

// Constructor

CAdenusDriver::CAdenusDriver(void)
{
	m_Ident  = DRIVER_ID;

	m_pSock  = NULL;

	m_pExtra = NULL;
	}

// Destructor

CAdenusDriver::~CAdenusDriver(void)
{
	m_pExtra->Release();
	}

// Configuration

void MCALL CAdenusDriver::Load(LPCBYTE pData)
{
	MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

// Management

void MCALL CAdenusDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CAdenusDriver::Detach(void)
{
	}

// Entry Point

void MCALL CAdenusDriver::Service(void)
{
	if( !m_pSock ) {

		m_pSock = CreateSocket(IP_TCP);

		if( m_pSock ) {

			m_pSock->Listen(23);

			m_uState = 0;
			}
		}
	else {
		switch( m_uState ) {

			case 0:
			case 2:
			case 4:
				SendPrompt();
				break;

			case 1:
			case 3:
			case 5:
				GetLine();
				break;
			}
		}
	}

// Implementation

BOOL CAdenusDriver::TestSocket(void)
{
	if( m_pSock ) {

		UINT Phase;

		m_pSock->GetPhase(Phase);

		if( Phase == PHASE_IDLE ) {

			return FALSE;
			}

		if( Phase == PHASE_OPEN ) {

			return TRUE;
			}

		if( Phase == PHASE_CLOSING ) {

			Error(4);

			m_pSock->Close();

			return FALSE;
			}

		if( Phase == PHASE_ERROR ) {

			m_pSock->Release();

			m_pSock = NULL;

			return FALSE;
			}
		}

	return FALSE;
	}

void CAdenusDriver::SendPrompt(void)
{
	if( TestSocket() ) {

		PCTXT pPrompt = "RL Prompt > ";

		switch( m_uState ) {

			case 0: pPrompt = "RL Log In > ";   break;
			case 2: pPrompt = "RL Password > "; break;
			case 4: pPrompt = "RL Ready > ";    break;

			}

		Send(pPrompt);

		m_uPtr  = 0;

		m_uTime = GetTickCount();

		m_uState++;
		}
	}

void CAdenusDriver::Send(PCTXT pText)
{
	UINT uSize = strlen(pText);

	m_pSock->Send(PBYTE(pText), uSize);
	}

void CAdenusDriver::GetLine(void)
{
	while( TestSocket() ) {

		char cData;

		UINT uSize = sizeof(cData);

		m_pSock->Recv(PBYTE(&cData), uSize);

		if( uSize ) {

			if( cData == '\r' ) {

				m_sLine[m_uPtr++] = 0;

				CheckLine();
				}
			else {
				if( cData == 0x08 ) {

					if( m_uPtr ) {

						Send(" \010");

						m_uPtr--;
						}
					else
						Send(" ");
					}

				if( isprint(cData) ) {

					if( m_uPtr < sizeof(m_sLine) - 2 ) {

						m_sLine[m_uPtr++] = cData;
						}
					}
				}

			continue;
			}

		if( GetTickCount() - m_uTime >= ToTicks(60000) ) {

			Send("\r\n");

			Error(3);

			m_pSock->Close();
			}

		break;
		}
	}

void CAdenusDriver::CheckLine(void)
{
	if( !stricmp(m_sLine, "QUIT") ) {

		Send("BYE\r\n");

		m_pSock->Close();

		return;
		}

	if( m_uState == 1 ) {

		PTXT pComma = strchr(m_sLine, ',');

		if( pComma ) {

			*pComma = 0;

			strcpy(m_sUser, m_sLine);
			
			strcpy(m_sPass, pComma+1);

			Logon();
			}
		else {
			strcpy(m_sUser, m_sLine);

			m_uState = 2;
			}

		return;
		}

	if( m_uState == 3 ) {

		strcpy(m_sPass, m_sLine);

		Logon();

		return;
		}

	if( m_uState == 5 ) {

		PTXT pSpace = strchr(m_sLine, ' ');

		if( pSpace ) *pSpace = 0;

		if( !stricmp(m_sLine, "LOGOFF") ) {

			m_uState = 0;

			return;
			}

		if( !stricmp(m_sLine, "GET") ) {

			PTXT pName  = pSpace + 1;

			UINT uIndex = m_pExtra->FindTagIndex(pName);

			Send("RL Response > ");

			Send(pName);

			Send(",");

			if( uIndex == NOTHING ) {

				Send("NULL");
				}
			else {
				PTXT pData = m_pExtra->FormatTagData(uIndex);

				if( pData ) {

					Send(pData);

					free(pData);
					}
				else
					Send("NULL");
				}

			Send("\r\n");
				
			m_uState = 4;

			return;
			}

		Error(6);

		m_uState = 4;

		return;
		}
	}

void CAdenusDriver::Error(UINT uCode)
{
	char sText[32];

	SPrintf(sText, "ERROR %2.2u\r\n", uCode);

	Send(sText);
	}

void CAdenusDriver::Logon(void)
{
	if( m_pExtra->ValidateLogon(m_sUser, m_sPass, 1<<16) ) {

		m_uState = 4;
		}
	else {
		m_uState = 0;

		Error(1);
		}
	}

// End of File
