#include "pcomb.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Base Driver
//
//

class CPcomMsBDriver : public CPcomBMasterDriver
{
	public:
		// Constructor
		CPcomMsBDriver(void);

		// Destructor
		~CPcomMsBDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
	protected:

		// Device Context
		struct CContext : CPcomBMasterDriver::CBaseCtx
		{
		
			};

		// Data Members
		CContext * m_pCtx;
		

		// Transport
		virtual BOOL Transact(void);
		     
		BOOL Send(void);
		BOOL RecvFrame(void);

				
	};

// End of File

