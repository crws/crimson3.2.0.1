#include "intern.hpp"

#include "pcomb.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Base Driver
//

// Constructor

CPcomBMasterDriver::CPcomBMasterDriver(void)
{
	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx));

	m_pBase = NULL;
     	}

// Destructor

CPcomBMasterDriver::~CPcomBMasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CPcomBMasterDriver::Ping(void)
{
	if( m_pBase->m_pTables ) {
	
		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Extra  = 0;

		for( UINT x = 0; x < m_pBase->m_TableCount; x++ ) {

			Addr.a.m_Table  = x + 1;

			Addr.a.m_Offset = 0x0;

			for( UINT y = 0; y < elements(m_pBase->m_pTables->m_Cols); y++ ) {

				UINT uType = FindType(m_pBase->m_pTables[x].m_Cols[y].m_Type);

				if( uType < NOTHING ) {

					Addr.a.m_Type   = uType;

					return Read(Addr, Data, 1);
					}
				}
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CPcomBMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMessage(Addr, uCount, FALSE);

	AddFooter();

	if( Transact() ) {

		GetData(pData, Addr.a.m_Type, uCount, Addr.a.m_Table);

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CPcomBMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsReadOnly(Addr.a.m_Table) ) {

		return uCount;
		}

	MakeMessage(Addr, uCount, TRUE);

	AddData(pData, Addr.a.m_Type, uCount);

	AddFooter();

	if( Transact() ) {

		return uCount;
		}

	return CCODE_ERROR;
	}
 
// Implementation

void CPcomBMasterDriver::MakeMessage(AREF Addr, UINT& uCount, BOOL fWrite)
{
	Begin();

	UINT uRow = Addr.a.m_Offset & 0x7FFF;

	UINT uCol = Addr.a.m_Extra | ((Addr.a.m_Offset & 0x8000) >> 0x11);

	CTable * pTable = &m_pBase->m_pTables[Addr.a.m_Table - 1];

	if( pTable->m_Cols[uCol].m_Type != typeUser ) {

		uCount = FindMinCount(Addr.a.m_Table, Addr.a.m_Offset, uCount);

		MakeMin(uCount, GetCount(Addr.a.m_Type));
		}
		
	AddHeader(fWrite);
	
	AddAddress(Addr.a.m_Table, uCol, uRow);

	AddLength(fWrite, Addr.a.m_Type, uCount);
	
	AddDetails(Addr.a.m_Type, uCol, Addr.a.m_Table, uCount);
	}

void CPcomBMasterDriver::AddByte(BYTE bByte)
{
	m_bTx[m_uPtr] = bByte;

	m_uPtr++;

	m_uCheck += bByte;
	}

void CPcomBMasterDriver::AddWord(WORD wWord)
{
	AddByte(LOBYTE(wWord));

	AddByte(HIBYTE(wWord));
	}

void CPcomBMasterDriver::AddLong(DWORD dwLong)
{
	AddWord(LOWORD(dwLong));

	AddWord(HIWORD(dwLong));
	} 

void CPcomBMasterDriver::AddLongData(DWORD dwLong)
{
	AddWord(HIWORD(dwLong));

	AddWord(LOWORD(dwLong)); 
	}

void CPcomBMasterDriver::AddAddress(UINT uTable, UINT uCol, UINT uRow)
{
	UINT uBytes = 0;

	for( UINT x = 0; x < m_pBase->m_TableCount; x++ ) {

		CTable * pTable = &m_pBase->m_pTables[x];

		if( x + 1 == uTable ) {

			for( UINT z = 0; z <= uRow; z++ ) {

				UINT uCols = z != uRow ? elements(pTable->m_Cols) : uCol;

				for( UINT i = 0; i < uCols; i++ ) {

					if( pTable->m_Cols[i].m_Bytes < NOTHING ) {

						uBytes += pTable->m_Cols[i].m_Bytes;

						continue;
						}
					break;
					}
				}

			break;
			}

		for( UINT y = 0; y < elements(pTable->m_Cols); y++ ) {

			if( pTable->m_Cols[y].m_Bytes < NOTHING ) {

				uBytes += pTable->m_Cols[y].m_Bytes * pTable->m_Rows;

				continue;
				}
				
			break;
			}
		}

	AddByte(LOBYTE(LOWORD(uBytes)));    

	AddByte(HIBYTE(LOWORD(uBytes)));

	AddByte(LOBYTE(HIWORD(uBytes)));

	AddByte(HIBYTE(HIWORD(uBytes))); 
	
	AddByte(0);

	AddByte(0);    
	}

void CPcomBMasterDriver::AddLength(BOOL fWrite, UINT uType, UINT uCount)
{
	BYTE bLen = 32;

	if( fWrite ) {

		switch( uType ) {

			case addrBitAsBit:
			case addrByteAsByte:

				bLen = bLen + uCount * 1;
				break;

			case addrWordAsWord:

				bLen = bLen + uCount * 2;
				break;

			case addrLongAsLong:
			case addrRealAsReal:

				bLen = bLen + uCount * 4;
				break;
			}
		}

	AddByte(bLen);

	AddByte(0);

	End();
	}

void CPcomBMasterDriver::AddData(PDWORD pData, UINT uType, UINT uCount)
{
	switch( uType ) {
		
		case addrBitAsBit:	AddBits (pData, uCount);		return;
		case addrByteAsByte:	AddBytes(pData, uCount);		return;
		case addrWordAsWord:	AddWords(pData, uCount);		return;
		case addrLongAsLong:	AddLongs(pData, uCount);		return;
		case addrRealAsReal:	AddReals(pData, uCount);		return;
		
		}
	}

void CPcomBMasterDriver::AddBits(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddByte(pData[u] ? 1 : 0);
		}
	}

void CPcomBMasterDriver::AddBytes(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddByte(pData[u]);
		}
	}

void CPcomBMasterDriver::AddWords(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddWord(pData[u]);
		}
	}

void CPcomBMasterDriver::AddLongs(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddLong(pData[u]);
		}
	}

void CPcomBMasterDriver::AddReals(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddLongData(pData[u]);
		}
	}

void CPcomBMasterDriver::GetData(PDWORD pData, UINT uType, UINT uCount, UINT uTable)
{
	memcpy(m_bRx, m_bRx + 24, m_uPtr - 24);

	switch( uType ) {
		
		case addrBitAsBit:	GetBits (pData, uCount);		return;
		case addrByteAsByte:	GetBytes(pData, uCount);		return;
		case addrWordAsWord:	GetWords(pData, uCount);		return;
		case addrLongAsLong:	GetLongs(pData, uCount);		return;
		case addrRealAsReal:	GetReals(pData, uCount);		return;
		
		}
	}

void CPcomBMasterDriver::GetBits(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = m_bRx[u] ? TRUE : FALSE;
		}
	}

void CPcomBMasterDriver::GetBytes(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = m_bRx[u];
		}
	}

void CPcomBMasterDriver::GetWords(PDWORD pData, UINT uCount)
{	
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = GetTwo(u);
		}
	}

WORD CPcomBMasterDriver::GetTwo(UINT uPos)
{
	WORD x = PU2(m_bRx)[uPos];

	return LONG(SHORT(IntelToHost(x)));
	}

void CPcomBMasterDriver::GetLongs(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {
	
		DWORD x = PU4(m_bRx)[u];

		pData[u] = IntelToHost(x);
		}
	}

void CPcomBMasterDriver::GetReals(PDWORD pData, UINT uCount)
{
	for( UINT u = 0, i = 0; u < uCount; u++, i++ ) {

		pData[u]  = GetTwo(i++) << 16;

		pData[u] |= GetTwo(i);
		}
	}

void CPcomBMasterDriver::Begin(void)
{
	m_uPtr = 0;

	m_uCheck = 0;
	}

void CPcomBMasterDriver::AddHeader(BOOL fWrite)
{
	AddByte('/');

	AddByte('_');

	AddByte('O');

	AddByte('P');

	AddByte('L');

	AddByte('C');
      
	AddByte(m_pBase->m_bUnit);

	AddByte(0xFE);

	AddByte(1);

	AddByte(0);

	AddByte(0);

	AddByte(0);

	AddByte(fWrite ? 68 : 4);

	AddByte(0);
	}

void CPcomBMasterDriver::AddDetails(UINT uType, UINT uCol, UINT uTable, UINT uCount)
{
	CTable * pTable = &m_pBase->m_pTables[uTable - 1];

	if( pTable ) {

		if( pTable->m_Cols[uCol].m_Type == typeUser ) {

			AddWord(pTable->m_Cols[uCol].m_Bytes);

			AddWord(1);
			}
		else {
	
			AddWord(WORD(TypeBytes(uType)));	// # bytes to read from each table row
	
			AddWord(WORD(uCount));			// # rows to read from data table
			}
    
		UINT uBytes = 0;

		UINT uCols = elements(pTable->m_Cols);

		for( UINT i = 0; i < uCols; i++ ) {

			if( pTable->m_Cols[i].m_Bytes < NOTHING ) {

				uBytes += pTable->m_Cols[i].m_Bytes;

				continue;
				}
		
			break;
			}

		AddLong(uBytes);			// # bytes in entire row of data table

		AddLong(0);				// reserved

		AddLong(0);

		AddLong(0);

		AddLong(0);

		AddLong(0);

		AddLong(0);
		}
	}

void CPcomBMasterDriver::AddFooter(void)
{
	End();

	AddByte('\\');
	}

void CPcomBMasterDriver::End(void)
{
	m_uCheck = ( ~(m_uCheck % 0x10000) ) + 1;

	BYTE bByte = m_uCheck & 0xFF;

	m_bTx[m_uPtr] = bByte;

	m_uPtr++;

	bByte = (m_uCheck & 0xFF00) >> 8;

	m_bTx[m_uPtr] = bByte;

	m_uPtr++;

	m_uCheck = 0;
	}

// Helpers

UINT CPcomBMasterDriver::TypeBytes(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrByteAsByte:

			return 1;

		case addrWordAsWord:

			return 2;

		case addrLongAsLong:
		case addrRealAsReal:

			return 4;
		}

	return 0;
	}

UINT CPcomBMasterDriver::FindType(UINT uType)
{
	switch( uType ) {

		case typeBit:	return addrBitAsBit;
		case typeByte:	return addrByteAsByte;
		case typeWord:	return addrWordAsWord;
		case typeLong:	return addrLongAsLong;
		case typeReal:	return addrRealAsReal;
		case typeUser:	return addrByteAsByte;
		}

	return NOTHING;
	}

UINT CPcomBMasterDriver::FindMinCount(UINT uTable, UINT uOffset, UINT uCount)
{
	UINT Count = 0;

	CTable * pTable = &m_pBase->m_pTables[uTable - 1];

	if( pTable ) {

		Count = uCount;

		UINT uRows = pTable->m_Rows;

		if( uOffset + uCount > uRows ) {

			Count = uRows - uOffset;
			}
		}
	
	return Count;
	}

UINT CPcomBMasterDriver::GetCount(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrByteAsByte:

			return 256;

		case addrLongAsLong:
		case addrRealAsReal:

			return 64;
		}

	return 128;
	}

BOOL CPcomBMasterDriver::IsReadOnly(UINT uTable)
{
	switch( uTable ) {

		}

	return FALSE;
	}

BOOL CPcomBMasterDriver::CheckFrame(void)
{	
	m_bTx[6] = 0xFE;

	m_bTx[7] = m_pBase->m_bUnit;

	if( memcmp(m_bTx, m_bRx, 9) ) { 

		return FALSE;
		} 

	return m_bRx[m_uPtr - 1] == 0x5C;
	}
							
// Transport Layer

BOOL CPcomBMasterDriver::Transact(void)
{
	return TRUE;
	}

// End of File

