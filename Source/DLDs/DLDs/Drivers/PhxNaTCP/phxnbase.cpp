
#include "intern.hpp"

#include "phxnbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Phoenix NanoLC Driver Base
//

// Constructor

CPhoenixNanoBase::CPhoenixNanoBase(void)
{
	}

// Destructor

CPhoenixNanoBase::~CPhoenixNanoBase(void)
{
	}

// Entry Points

CCODE MCALL CPhoenixNanoBase::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nREAD %d %d %d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	GetDeviceInfo(&m_bDrop, &m_fTCP, &m_pTx, &m_pRx);

	if( !m_bDrop ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			return DoBitRead (Addr, pData, min(uCount, 16));

		case addrWordAsWord:

			return DoWordRead(Addr, pData, min(uCount, 16));

		case addrLongAsLong:

			return DoLongRead(Addr, pData, min(uCount, 8));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CPhoenixNanoBase::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n***** WRITE %d %d %8.8lx\r\n", Addr.a.m_Table, Addr.a.m_Offset, *pData);

	GetDeviceInfo(&m_bDrop, &m_fTCP, &m_pTx, &m_pRx);

	CCODE cc = CCODE_ERROR;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			cc = DoBitWrite (Addr, pData, min(uCount, 16));

			break;

		case addrWordAsWord:

			cc = DoWordWrite(Addr, pData, min(uCount, 16));

			break;

		case addrLongAsLong:

			cc = DoLongWrite(Addr, pData, min(uCount, 8));

			break;
		}

	if( !(cc & CCODE_ERROR) ) {

		m_uWriteError = 0;

//**/		AfxTrace0("\r\n");

		return cc;
		}

	return ++m_uWriteError > 2 ? uCount : CCODE_ERROR;
	}

// Frame Building

void CPhoenixNanoBase::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddFrameHeader(bOpcode);
	}

void CPhoenixNanoBase::AddByte(BYTE bData)
{
	if( m_uPtr < BUFFSIZE ) {

		m_pTx[m_uPtr] = bData;

		m_uPtr++;
		}
	}

void CPhoenixNanoBase::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CPhoenixNanoBase::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void CPhoenixNanoBase::AddCRC(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {

		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));
	}

void CPhoenixNanoBase::EndFrame(void)
{
	if( !m_fTCP ) AddCRC();
	}

// Read Handlers

CCODE CPhoenixNanoBase::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uT = Addr.a.m_Table;

	UINT uO = Addr.a.m_Offset;

	BYTE b  = uT == SPACE_A ? READANI : READREG;

	if( uT == SPACE_O ) uO += OFFAOUT;

	StartFrame(b);

	AddWord(uO);

	AddWord(uCount);

	if( Send(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x   = PU2(m_pRx + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CPhoenixNanoBase::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uO = Addr.a.m_Offset * 2;

	StartFrame(READREG);

	switch( Addr.a.m_Table ) {

		case SPACE_TCA:	uO += OFFTCA;	break;

		case SPACE_TCP:	uO += OFFTCP;	break;

		case SPACE_OTA:	uO += OFFOTA;	break;

		case SPACE_OTP:	uO += OFFOTP;	break;

		case SPACE_HSA:	uO += OFFHSA;	break;

		case SPACE_HSP:	uO += OFFHSP;	break;

		case SPACE_R:			break;

		default:	return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(uO);

	AddWord(uCount * 2);

	if( Send(FALSE) ) {

		if( uCount * 4 > m_pRx[2] ) {

			uCount = (m_pRx[2] + 2) / 4;
			}

		for( UINT n = 0; n < uCount; n++ ) {

			DWORD x  = SwapLong(PU4(m_pRx + 3)[n]);

			pData[n] = MotorToHost(x);
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CPhoenixNanoBase::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uO = Addr.a.m_Offset;

	switch( Addr.a.m_Table ) {

		case SPACE_F:	uO += OFFFLAG;

				// Fall through

		case SPACE_Q:	StartFrame(READOUT);	break;

		case SPACE_I:	StartFrame(READINP);	break;

		default:	return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(uO);

	AddWord(uCount);

	if( Send(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_pRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CPhoenixNanoBase::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(uCount == 1 ? WRITE1R : WRITEnR);

	AddWord(Addr.a.m_Offset + (Addr.a.m_Table == SPACE_O ? OFFAOUT : 0));

	if( uCount > 1 ) {

		AddWord(WORD(uCount));

		AddByte(BYTE(uCount*2));
		}

	for( UINT n = 0; n < uCount; n++ ) {

		AddWord(LOWORD(pData[n]));
		}

	return Send(TRUE) ? uCount : CCODE_ERROR;
	}

CCODE CPhoenixNanoBase::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uO = Addr.a.m_Offset * 2;

	switch( Addr.a.m_Table ) {

		case SPACE_TCA:	uO += OFFTCA;	break;

		case SPACE_TCP:	uO += OFFTCP;	break;

		case SPACE_OTA:	uO += OFFOTA;	break;

		case SPACE_OTP:	uO += OFFOTP;	break;

		case SPACE_HSA:	uO += OFFHSA;	break;

		case SPACE_HSP:	uO += OFFHSP;	break;

		case SPACE_R:			break;

		default:			return uCount;
		}

	StartFrame(WRITEnR);

	AddWord(uO);

	AddWord(uCount * 2);

	AddByte(uCount * 4);

	for( UINT n = 0; n < uCount; n++ ) {

		AddLong(SwapLong(pData[n]));
		}

	return Send(TRUE) ? uCount : CCODE_ERROR;
	}

CCODE CPhoenixNanoBase::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	BOOL fC = uCount == 1;

	StartFrame(fC ? WRITE1O : WRITEnO);

	AddWord(Addr.a.m_Offset + (Addr.a.m_Table == SPACE_F ? OFFFLAG : 0));

	if( fC ) AddWord(pData[0] ? 0xFF00 : 0);

	else {
		AddWord(uCount);

		AddByte((uCount + 7) / 8);

		UINT b = 0;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			if( pData[n] ) {

				b |= m;
				}

			if( !(m <<= 1) ) {

				AddByte(b);

				b = 0;

				m = 1;
				}
			}

		if( m > 1 ) {

			AddByte(b);
			}
		}

	return Send(TRUE) ? uCount : CCODE_ERROR;
	}

// Transact

BOOL CPhoenixNanoBase::Send(BOOL fIgnore)
{
	EndFrame();

	return Transact(m_uPtr, fIgnore);
	}

// Helper

DWORD CPhoenixNanoBase::SwapLong(DWORD dData)
{
	return MAKELONG(HIWORD(dData), LOWORD(dData));
	}

// Virtuals

// Frame Header
void CPhoenixNanoBase::AddFrameHeader(BYTE bOpcode)
{
	}

// Transport Layer

BOOL CPhoenixNanoBase::Transact(UINT uSize, BOOL fIgnore)
{
	return FALSE;
	}

// Get Device Info

void CPhoenixNanoBase::GetDeviceInfo(PBYTE pbDrop, BOOL *pfTCP, PBYTE *pTx, PBYTE *pRx)
{
	*pbDrop = 0;
	*pfTCP  = FALSE;
	*pTx    = NULL;
	*pRx    = NULL;
	}

// End of File
