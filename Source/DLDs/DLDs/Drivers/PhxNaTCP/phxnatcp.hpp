#include "phxnbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Phoenix NanoLC TCP/IP Master Driver
//

class CPhoenixNanoLCTCPDriver : public CPhoenixNanoBase
{
	public:
		// Constructor
		CPhoenixNanoLCTCPDriver(void);

		// Destructor
		~CPhoenixNanoLCTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BYTE	 m_bUnit;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			};

		// Data Members
		CContext	*m_pCtx;

		CRC16		m_CRC;

		PBYTE		m_pTx;
		PBYTE		m_pRx;

		BYTE		m_bTx[BUFFSIZE];
		BYTE		m_bRx[BUFFSIZE];

		UINT		m_uKeep;
				
		// Transport Layer
		BOOL Transact(UINT uSize, BOOL fIgnore);
		BOOL SendFrame(UINT uSize);
		BOOL RecvFrame(void);
		BOOL CheckFrame(void);

		// Frame Header
		void AddFrameHeader(BYTE bOpcode);

		// Device Info
		void GetDeviceInfo(PBYTE pbDrop, BOOL *pfTCP, PBYTE *pTx, PBYTE *pRx);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
	};

// End of File
