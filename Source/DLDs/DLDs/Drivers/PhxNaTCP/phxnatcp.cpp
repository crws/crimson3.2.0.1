
#include "intern.hpp"

#include "phxnatcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Phoenix NanoLC TCP Driver
//

// Instantiator

INSTANTIATE(CPhoenixNanoLCTCPDriver);

// Constructor

CPhoenixNanoLCTCPDriver::CPhoenixNanoLCTCPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;

	m_pTx   = m_bTx;

	m_pRx   = m_bRx;
	}

// Destructor

CPhoenixNanoLCTCPDriver::~CPhoenixNanoLCTCPDriver(void)
{
	}

// Configuration

void MCALL CPhoenixNanoLCTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CPhoenixNanoLCTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CPhoenixNanoLCTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CPhoenixNanoLCTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CPhoenixNanoLCTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CPhoenixNanoLCTCPDriver::Ping(void)
{
//**/	AfxTrace0("\r\nPING ");

	if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}

		if( m_pCtx->m_bUnit == 255 ) {

			return CCODE_SUCCESS;
			}	

		DWORD    Data[1];
	
		CAddress Addr;

		Addr.a.m_Table  = SPACE_R;
		
		Addr.a.m_Offset = 0;
		
		Addr.a.m_Type   = addrLongAsLong;

		Addr.a.m_Extra  = 0;
	
		return Read(Addr, Data, 1);
		}

	return CCODE_ERROR; 
	}

// Transport Layer

BOOL CPhoenixNanoLCTCPDriver::Transact(UINT uSize, BOOL fIgnore)
{
	if( SendFrame(uSize) && RecvFrame() ) {

		if( fIgnore ) {

			return TRUE;
			}

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CPhoenixNanoLCTCPDriver::SendFrame(UINT uSize)
{
	m_pTx[5] = BYTE(uSize - 6);

	UINT uSz = uSize;

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < uSize; k++) AfxTrace1("[%2.2x]", m_pTx[k]);

	if( CheckSocket() && (m_pCtx->m_pSock->Send(m_pTx, uSize) == S_OK) ) {

		if( uSz == uSize ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPhoenixNanoLCTCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

//**/	AfxTrace0("\r\nR:");

	while( GetTimer() ) {

		uSize = BUFFSIZE - uPtr;

		m_pCtx->m_pSock->Recv(m_pRx + uPtr, uSize);

		if( uSize ) {

//**/			for( UINT k = 0; k < uSize; k++ ) AfxTrace1("<%2.2x>", m_pRx[k]);

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_pRx[5];

				if( uPtr >= uTotal ) {

					if( m_pRx[0] == m_pTx[0] ) {

						if( m_pRx[1] == m_pTx[1] ) {

							memcpy(m_pRx, m_pRx + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_pRx[n] = m_pRx[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CPhoenixNanoLCTCPDriver::CheckFrame(void)
{
	if( !(m_pRx[1] & 0x80) ) {
	
		return TRUE;
		}

	return FALSE;
	}

// Frame Header

void CPhoenixNanoLCTCPDriver::AddFrameHeader(BYTE bOpcode)
{
	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);
	}

// Device Info

void CPhoenixNanoLCTCPDriver::GetDeviceInfo(PBYTE pbDrop, BOOL *pfTCP, PBYTE *pTx, PBYTE *pRx)
{
	*pbDrop = m_pCtx->m_bUnit;

	*pfTCP  = TRUE;

	*pTx    = m_pTx;

	*pRx    = m_pRx;
	}

// Socket Management

BOOL CPhoenixNanoLCTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CPhoenixNanoLCTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CPhoenixNanoLCTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
