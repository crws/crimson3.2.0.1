#include "intern.hpp"

#include "metan2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 System Master Driver
//

// Instantiator

INSTANTIATE(CMetaSysN2SystemMaster);

// Constructor

CMetaSysN2SystemMaster::CMetaSysN2SystemMaster(void)
{
	m_Ident		= DRIVER_ID;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_fOnline	= FALSE;

	m_pExtra	= NULL;
       	}

// Destructor

CMetaSysN2SystemMaster::~CMetaSysN2SystemMaster(void)
{
	m_pExtra->Release();
       	}

// Config

void MCALL CMetaSysN2SystemMaster::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, FALSE);
	}

void MCALL CMetaSysN2SystemMaster::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);

	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

void MCALL CMetaSysN2SystemMaster::Open(void)
{
	}

// Device

CCODE MCALL CMetaSysN2SystemMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop   = GetByte(pData);

			m_pCtx->m_fOnline = FALSE;

			m_pCtx->m_fAck    = TRUE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}
      
	return CCODE_SUCCESS;
		
	}

CCODE MCALL CMetaSysN2SystemMaster::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMetaSysN2SystemMaster::Ping(void)
{
/*	DoOpto22ResetCmd();

	if( Transact() ) {
	
		DoIdentifyDeviceMsg();

		m_pCtx->m_fOnline = Transact();

		if( m_pCtx->m_fOnline ) {

			return CCODE_SUCCESS;
			}
		}
	
	return CCODE_ERROR;*/return 1;   
	}

void MCALL CMetaSysN2SystemMaster::Service(void)
{
/*	AfxTrace("Service %u\n", m_pCtx->m_bDrop);

	if( IsOnline() ) {

		DoPollMsg(m_pCtx->m_fAck);

		m_pCtx->m_fAck = Transact();

		if( m_pCtx->m_fAck ) {

			if( IsCOS() ) {

				// Record

				AfxTrace("Record COS\n");
				}
			}
		} */
	}

CCODE MCALL CMetaSysN2SystemMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsOnline() ) {

		UINT uTable = Addr.a.m_Table;

		if( IsWriteOnly(uTable) ) {

			return uCount;
			}

		UINT uRegion = Region[uTable - 1];

		UINT uObject = ((Addr.a.m_Offset & 0xFF00) >> 8);
		
		UINT uAttrib = ((Addr.a.m_Offset & 0x00F0) >> 4);

		if( uCount > 1 ) {

			MakeMin(uCount, Attrib[uTable - 1] - uAttrib);
			}

		uAttrib++;
	
		DoReadCmd(uRegion, uObject, uAttrib);

		if( Transact() ) {

			GetData(uTable, uAttrib, uCount, pData);

			return uCount;
			}
		}
	
	return CCODE_ERROR;
	}

CCODE MCALL CMetaSysN2SystemMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsOnline() ) {

		UINT uTable = Addr.a.m_Table;
		
		if( IsWriteOnly(uTable) ) {

			switch( Addr.a.m_Offset ) {
				
				case cmdTimeSynch:

					if( pData[0] == 0 ) {

						return uCount;
						}

					DoSynchTimeCmd();

					if( Transact() ) {

						return uCount;
						}
				}

			return CCODE_ERROR;
			}
	
		UINT uObject = ((Addr.a.m_Offset & 0xFF00) >> 8);
		
		UINT uAttrib = ((Addr.a.m_Offset & 0x00F0) >> 4);

		if( uCount > 1 ) {

			MakeMin(uCount, Attrib[uTable - 1] - uAttrib);
			}

		uAttrib++;
	
		if( uCount == 1 && IsReadOnly(uTable, uAttrib) ) {

			return uCount;
			}

		UINT uRegion = Region[uTable - 1];

	       	DoWriteCmd(uRegion, uTable, uObject, uAttrib, uCount, pData);
			
		if( Transact() ) {

			return uCount;
			}
		}
	
	return CCODE_ERROR;
	}

// Implementation

void CMetaSysN2SystemMaster::Begin(void)
{
	m_Ptr = 0;

	AddByte('>');

	m_Check = 0;

	PutChar2(m_pCtx->m_bDrop);
	}

void CMetaSysN2SystemMaster::AddByte(BYTE bByte)
{
	m_Tx[m_Ptr] = bByte;
	
	m_Ptr++;

	m_Check += bByte;
	}

void CMetaSysN2SystemMaster::AddCheck(void)
{
	PutChar2(m_Check % 256);

	AddByte(CR);
	}

void CMetaSysN2SystemMaster::PutChar1(DWORD Data)
{
	AddByte(m_pHex[Data & 0xF]);	
	}

void CMetaSysN2SystemMaster::PutChar2(DWORD Data)
{
	AddByte(m_pHex[(Data & 0xFF) / 0x10]);

	AddByte(m_pHex[(Data & 0xFF) % 0x10]);
	}

void CMetaSysN2SystemMaster::PutChar4(DWORD Data)
{		
	PutChar2((Data >> 8) & 0xFF);
	
	PutChar2(Data & 0xFF);
	}

void CMetaSysN2SystemMaster::PutChar8(DWORD Data)
{
	PutChar4(HIWORD(Data));

	PutChar4(LOWORD(Data));
	}

void CMetaSysN2SystemMaster::DoSynchTimeCmd(void)
{
	Begin();
	
	PutChar1(0);

	PutChar1(0);

	DWORD t = m_pExtra->GetNow();

	UINT uSec   = GetSec(t) % 60;

	UINT uMin   = GetMin(t) % 60;

	UINT uHours = GetHours(t) % 24;

	UINT uDay   = GetDay(t) % 7;

	UINT uMonth = GetMonth(t);

	UINT uYear  = GetYear(t);

	UINT uDays  = GetDate(t); 

	PutChar4(uYear);

	PutChar2(uMonth);

	PutChar2(uDays);

	PutChar2(uDay);

	PutChar2(uHours);

	PutChar2(uMin);

	PutChar2(uSec);

	AddCheck();
	}

void CMetaSysN2SystemMaster::DoOpto22ResetCmd(void)
{
	Begin();

	AddByte('A');

	AddCheck();
	}

void CMetaSysN2SystemMaster::DoPollMsg(BOOL fAck)
{
	Begin();

	PutChar1(0);

	PutChar1(fAck ? 5 : 4);

	AddCheck();
	}

void CMetaSysN2SystemMaster::DoIdentifyDeviceMsg(void)
{
	Begin();
	
	AddByte('F');

	AddCheck();
	}

void CMetaSysN2SystemMaster::DoReadCmd(UINT uRegion, UINT uObj, UINT uAttr)
{
	Begin();
	
	PutChar1(1);

	PutChar1(uRegion);

	PutChar2(uObj);

	PutChar2(uAttr);
	
	AddCheck();
	}

void CMetaSysN2SystemMaster::DoWriteCmd(UINT uRegion, UINT uTable, UINT uObj, UINT uAttr, UINT uCount, PDWORD pData)
{
	Begin();
	
	PutChar1(2);

	PutChar1(uRegion);

	PutChar2(uObj);

	if( !IsInternal(uTable) ) {

		PutChar2(uAttr);
		}

	PutData(uTable, uObj, uAttr, uCount, pData);

	AddCheck();
	}

void CMetaSysN2SystemMaster::PutData(UINT uTable, UINT uObj, UINT uAttr, UINT uCount, PDWORD pData)
{	
	for( UINT u = 0; u < uCount; u++ ) {

		if( !IsReadOnly(uTable, uAttr) ) {

			UINT uType = FindDataType(uTable, uAttr);

			switch( uType ) {

				case addrBitAsByte:
				case addrByteAsByte:

					PutChar2(pData[u]);	break;

				case addrWordAsWord:

					PutChar4(pData[u]);	break;

				case addrLongAsLong:
				case addrLongAsReal:

					PutChar8(pData[u]);	break;
				}
			}
		}
	}

void CMetaSysN2SystemMaster::GetData(UINT uTable, UINT uAttr, UINT uCount, PDWORD pData)
{
	for( UINT uPos = 3, u = 0; u < uCount; u++ ) {

		UINT uType = FindDataType(uTable, uAttr);

		switch( uType ) {

			case addrBitAsByte:
			case addrByteAsByte:

				pData[u] = GetChar2(uPos);	break;

			case addrWordAsWord:

				pData[u] = GetChar4(uPos);	break;

			case addrLongAsLong:
			case addrLongAsReal:
				
				pData[u] = GetChar8(uPos);	break;
			}
		}
	}

// Frame Access

UINT CMetaSysN2SystemMaster::GetChar1(UINT& uPos)
{
	UINT uChar = m_Rx[uPos];

	uPos++;

	if( uChar >= '0' && uChar <= '9' ) {

		return uChar - '0';
		}

	return uChar - '@' + 9;
      	}

UINT CMetaSysN2SystemMaster::GetChar2(UINT& uPos)
{
	UINT uChar = GetChar1(uPos);

	uChar <<= 4;

	uChar += GetChar1(uPos);

	return uChar;
	}

UINT CMetaSysN2SystemMaster::GetChar4(UINT& uPos)
{
	UINT uHi = GetChar2(uPos);

	UINT uLo = GetChar2(uPos);
	
	return MAKEWORD(uLo, uHi);
	}

UINT CMetaSysN2SystemMaster::GetChar8(UINT& uPos)
{
	UINT uHi = GetChar4(uPos);

	UINT uLo = GetChar4(uPos);
	
	return MAKELONG(uLo, uHi);
	}

// Transport

BOOL CMetaSysN2SystemMaster::Transact(void)
{
	if( Send() && Recv() && CheckFrame() ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CMetaSysN2SystemMaster::Send(void)
{	
	m_pData->Write(m_Tx, m_Ptr, FOREVER);

/*	AfxTrace("\n Tx : ");

	for( UINT u = 0; u < m_Ptr; u++ ) {

		AfxTrace("%c ", m_Tx[u]);
		}
 */
	return TRUE;
	}

BOOL CMetaSysN2SystemMaster::Recv(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_Ptr = 0;

	m_Check = 0;

//	AfxTrace("\nRx : ");

	BOOL fError = FALSE;

	SetTimer(3000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

//		AfxTrace("%2.2x ", uData);

		if( IsValid(uData) ) {

			if( m_Ptr == 0 ) {

				if( uData != 'A' && uData != 'N' ) {

					fError = TRUE;
					}
				}

			if( !fError ) {

				if( uData == CR ) {

					return TRUE;
					}
				
				m_Rx[m_Ptr] = uData;

				m_Ptr++;
				}
			}
		}

	return FALSE;
	}

BOOL CMetaSysN2SystemMaster::CheckFrame(void)
{
	if( IsAck() ) {

		return TRUE;
		}
	
	m_Check = 0;

	UINT u = 0;

	for( u = 1; u < m_Ptr - 2; u++ ) {

		m_Check += m_Rx[u];
		}

	m_Check = m_Check % 256;

	if( m_Check != GetChar2(u) ) {

		return FALSE;
		}

	return TRUE;
	}

// Helpers

BOOL CMetaSysN2SystemMaster::IsReadOnly(UINT uObj, UINT uAttr)
{
	switch( uObj ) {

		case spaceAI:
		case spaceAO:

			if( uAttr == 1 || uAttr >= 4 ) {

				return FALSE;
				}
			
			return TRUE;

		case spaceBI:
		case spaceBO:

			if( uAttr == 2 ) {

				return TRUE;
				}

			return FALSE;
		}
       
	return FALSE;
	}

BOOL CMetaSysN2SystemMaster::IsWriteOnly(UINT uObj)
{
	return uObj >= addrNamed;
	}

BOOL CMetaSysN2SystemMaster::IsValid(UINT uByte)
{
	switch( uByte ) {

		case '>':
		case 'N':
		case CR :

			return TRUE;
		}

	if( uByte >= '0' && uByte <= '9' ) {
		
		return TRUE;
		}

	if( uByte >= 'A' && uByte <= 'F' ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CMetaSysN2SystemMaster::IsOnline(void)
{
	return m_pCtx->m_fOnline;
	}

BOOL CMetaSysN2SystemMaster::IsInternal(UINT uObject)
{
	return ((uObject >= spaceIF) && (uObject <= spaceIB));
	}

BOOL CMetaSysN2SystemMaster::IsCOS(void)
{
	return ((m_Rx[0] == 'A') && (m_Ptr > 1));
	}

BOOL CMetaSysN2SystemMaster::IsAck(void)
{
	return ((m_Rx[0] == 'A') && (m_Ptr == 1));
	}

UINT CMetaSysN2SystemMaster::FindDataType(UINT uObject, UINT uAttr)
{
	if( IsInternal(uObject) ) {

		if( uAttr == 2 ) {

			if( uObject == spaceIF ) {

				return addrLongAsReal;
				}

			if( uObject == spaceII ) {

				return addrWordAsWord;
				}

			if( uObject == spaceIB ) {

				return addrByteAsByte;
				}
			}

		return addrByteAsByte;
		}

	if( uAttr >= 3 ) {

		switch( uObject ) {

			case spaceAI:
			case spaceAO:
		
				if( uAttr == 13 ) {

					return addrWordAsWord;
					}

				return addrLongAsReal;
		
			case spaceBI:

				if( uAttr == 3 ) {

					return addrWordAsWord;
					}

				return addrLongAsLong;
		 
			case spaceBO:
						
				return addrWordAsWord;
			}
		}

	return addrByteAsByte;
	}

// End of File

