//////////////////////////////////////////////////////////////////////////
//
// Other headers
//
//

#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Info
//

static BYTE const Object[] = { 1, 2, 3, 4, 5, 6, 7 };

static BYTE const Region[] = { 1, 3, 2, 4, 5, 6, 7 };

static BYTE const Attrib[] = {14, 5, 4, 7, 2, 2, 2 };

//////////////////////////////////////////////////////////////////////////
//
// Spaces
//

enum Spaces
{
	spaceAI = 1,
	spaceAO = 2,
	spaceBI = 3,
	spaceBO = 4,
	spaceIF = 5,
	spaceII = 6,
	spaceIB = 7,
	};

//////////////////////////////////////////////////////////////////////////
//
// Commands
//

enum Commands
{
	cmdTimeSynch = 1,
	};

//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 System Master Driver
//
//

class CMetaSysN2SystemMaster : public CMasterDriver
{
	public:
		// Constructor
		CMetaSysN2SystemMaster(void);

		// Destructor
		~CMetaSysN2SystemMaster(void);

		// Config
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE)Ping   (void);
		DEFMETH(void )Service(void);
		DEFMETH(CCODE)Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		struct CContext
		{
			BYTE           m_bDrop;
			BOOL	       m_fOnline;
			BOOL	       m_fAck;
			};

		// Data Members

		CContext *	m_pCtx;
		UINT		m_Drop;
		BYTE		m_Rx[256];
		BYTE		m_Tx[256];
		UINT		m_Ptr;
		BYTE		m_Check;
		LPCTXT		m_pHex;
		BOOL		m_fOnline;
		
		// Extra Help
		IExtraHelper   * m_pExtra;

		// Implementation

		void Begin(void);
		void AddByte(BYTE bByte);
		void AddCheck(void);
		void PutChar1(DWORD Data);
		void PutChar2(DWORD Data);
		void PutChar4(DWORD Data);
		void PutChar8(DWORD Data);
		
		
		void DoSynchTimeCmd(void);
		void DoOpto22ResetCmd(void);
		void DoPollMsg(BOOL fAck);
		void DoIdentifyDeviceMsg(void);
		void DoReadCmd(UINT uRegion, UINT uObj, UINT uAttr);
		void DoWriteCmd(UINT uReg, UINT uTable, UINT uObj, UINT uAttr, UINT uCount, PDWORD pData);
		void PutData(UINT uTable, UINT uObj, UINT uAttr, UINT uCount, PDWORD pData);
		void GetData(UINT uTable, UINT uAttr, UINT uCount, PDWORD pData);

		// Frame Access
		UINT GetChar1(UINT& uPos);
		UINT GetChar2(UINT& uPos);
		UINT GetChar4(UINT& uPos);
		UINT GetChar8(UINT& uPos);
		
		// Transport
		BOOL Transact(void);
		BOOL Send(void);
		BOOL Recv(void);
		BOOL CheckFrame(void);
		
		// Helpers
		BOOL IsReadOnly(UINT uObj, UINT uAttr);
		BOOL IsWriteOnly(UINT uObj);
		BOOL IsValid(UINT uByte);
		BOOL IsOnline(void);
		BOOL IsInternal(UINT uObject);
		BOOL IsCOS(void);
		BOOL IsAck(void);
		UINT FindDataType(UINT uObject, UINT uAttr);

	
	};

// End of File
