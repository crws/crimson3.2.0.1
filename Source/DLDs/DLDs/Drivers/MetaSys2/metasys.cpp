#include "intern.hpp"

#include "metasys.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Johnson Controls MetaSys N2 System Slave Driver
//

// Instantiator

//INSTANTIATE(CMetaSysN2SystemDriver);

// Constructor

CMetaSysN2SystemDriver::CMetaSysN2SystemDriver(void)
{
	m_Ident = 0x405D;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_fOnline	= FALSE;
       	}

// Destructor

CMetaSysN2SystemDriver::~CMetaSysN2SystemDriver(void)
{
	m_pExtra->Release();
	}

// Config

void MCALL CMetaSysN2SystemDriver::Load(LPCBYTE pData)
{
	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	
	if ( GetWord( pData ) == 0x1234 ) {

		m_Drop = GetByte(pData);

		return;
		}
	}

void MCALL CMetaSysN2SystemDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, FALSE);
	}
	

void MCALL CMetaSysN2SystemDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

// Entry Points

void MCALL CMetaSysN2SystemDriver::Service(void)
{
	for(;;) {

		if( Rx() ) {

			HandleFrame();
			}
		}
	}

// Implementation

BOOL CMetaSysN2SystemDriver::Rx(void)
{
       	BOOL fSOM = FALSE;

	UINT uData = 0;

	m_Ptr = 0;
	
	for(;;) {
	
		if( (uData = m_pData->Read(FOREVER)) == NOTHING ) {

			continue;
			}

		if( !fSOM && uData == '>' ) {

			fSOM = TRUE;

			continue;
			}

		if( fSOM ) {

			if( uData == CR  ) {

				m_Check = 0;

				for( UINT u = 0; u < m_Ptr - 2; u++ ) {

					m_Check	+= m_Rx[u];
					}

				m_Check = m_Check % 0x100;

				BOOL fCheck = m_pHex[m_Check / 0x10] == m_Rx[m_Ptr - 2] &&
					      m_pHex[m_Check % 0x10] == m_Rx[m_Ptr - 1];

				if( DoListen() ) {

					Sleep(10);

					if( !fCheck ) {

						HandleError(0x02);
						}

					return fCheck;
					}

				return FALSE;
				}

			m_Rx[m_Ptr] = uData;

			m_Ptr++;
			}
	       }
	
	return FALSE;
	}

BOOL CMetaSysN2SystemDriver::Tx(void)
{
	m_pData->Write(m_Tx, m_Ptr, FOREVER);

	return TRUE;
	}

BOOL CMetaSysN2SystemDriver::DoListen(void)
{
	if( m_Ptr > 0 ) {

		return m_pHex[m_Drop / 0x10] == m_Rx[0] && 
		       m_pHex[m_Drop % 0x10] == m_Rx[1];
		}

	return FALSE;
	}

void CMetaSysN2SystemDriver::HandleFrame(void)
{
	UINT uPtr = 2;

	UINT uOne = GetChar1(uPtr);

	if( IsIdentifyDeviceTypeCmd(uOne) ) {

		HandleIdentifyDeviceCmd();

		return;
		}
	
	if( !IsOnline() ) {

		HandleError(0x00);

		return;
		}
	
	UINT uTwo = GetChar1(uPtr);

	if( IsSynchTimeCmd(uOne, uTwo) ) {

		HandleSynchTimeCmd();

		return;
		}

	if( IsPollNoAck(uOne, uTwo) ) {

		HandlePollNoAck();

		return;
		}

	if( IsPollWithAck(uOne, uTwo) ) {

		HandlePollWithAck();

		return;
		}

	if( IsWriteSingle(uOne) ) {

		HandleWriteSingle(uTwo);

		return;
		}

	if( IsReadSingle(uOne) ) {

		HandleReadSingle(uTwo);

		return;
		}

	if( IsWriteMulti(uOne, uTwo) ) {

		HandleWriteMulti();

		return;
		}

	if( IsReadMulti(uOne, uTwo) ) {

		HandleReadMulti();

		return;
		}

	if( IsOverride(uOne, uTwo) ) {

		HandleOverride();

		return;
		}

	if( IsOverrideReleaseReq(uOne, uTwo) ) {

		HandleOverrideRelease();

		return;
		}

	HandleError(0x01);
       	}

void CMetaSysN2SystemDriver::HandleSynchTimeCmd(void)
{
	UINT uPtr = 4;

	UINT uYear  = GetChar4(uPtr, FALSE);

	UINT uMonth = GetChar2(uPtr, FALSE);

	UINT uDayM  = GetChar2(uPtr, FALSE);

	UINT uDayW  = GetChar2(uPtr, FALSE);

	UINT uHours = GetChar2(uPtr, FALSE);

	UINT uMins  = GetChar2(uPtr, FALSE);

	UINT uSecs  = GetChar2(uPtr, FALSE);

	DWORD t = 0;

	t += Time(uHours, uMins, uSecs);

	t += Date(uYear, uMonth, uDayM);
	
	m_pExtra->SetNow(t);

	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysN2SystemDriver::HandlePollNoAck(void)
{
	// No COS
	
	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysN2SystemDriver::HandlePollWithAck(void)
{
	// No COS
	
	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysN2SystemDriver::HandleIdentifyDeviceCmd(void)
{
	Start('A');

	PutChar2(0x10);

	AddCheck();

	Tx();
	}

void CMetaSysN2SystemDriver::HandleWriteSingle(UINT uRegion)
{
	UINT uLook = FindLookup(uRegion);

	UINT uPtr = 4;

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = GetChar2(uPtr);

	CAddress Addr;
	
	Addr.a.m_Table  = Table[uLook];

	Addr.a.m_Type   = Type[uLook];

	Addr.a.m_Extra  = uRegion - Region[uLook];

	Addr.a.m_Offset = uObj * Attrib[uLook] + (uAtr - 1);

	PDWORD pData = new DWORD[1];

	if( !SetData(Addr, uLook, uAtr, pData, 1, uPtr) ) {

		// Always send a response !!
		}
	
	delete pData;

	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysN2SystemDriver::HandleReadSingle(UINT uRegion)
{
	UINT uLook = FindLookup(uRegion);

	UINT uPtr = 4;

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = GetChar2(uPtr);

	FindAttribute(uLook, uAtr);

	CAddress Addr;
	
	Addr.a.m_Table  = Table[uLook];

	Addr.a.m_Type   = Type[uLook];

	Addr.a.m_Extra  = uRegion - Region[uLook];

	Addr.a.m_Offset = uObj * Attrib[uLook] + (uAtr - 1);

	UINT uCount = FindCount(uLook, uAtr);
		
	PDWORD pData = new DWORD[uCount];

	memset(PBYTE(pData), 0, uCount * sizeof(DWORD));

	if( COMMS_SUCCESS(Read(Addr, pData, uCount)) ) {

		// Always send a response !!
		}

	SendData(Addr.a.m_Type, uLook, uAtr, pData, uCount);
	
	delete pData;
	}

void CMetaSysN2SystemDriver::HandleWriteMulti(void)
{
	UINT uPtr = 4;

	UINT uRegion = GetChar2(uPtr);
       
	UINT uLook = FindLookup(uRegion);

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = 1;
	
	CAddress Addr;
	
	Addr.a.m_Table  = Table[uLook];

	Addr.a.m_Type   = Type[uLook];

	Addr.a.m_Extra  = uRegion - Region[uLook];

	Addr.a.m_Offset = uObj * Attrib[uLook] + (uAtr - 1);

	UINT uCount = Attrib[uLook];

	PDWORD pData = new DWORD[uCount];

	if( !SetData(Addr, uLook, uAtr, pData, uCount, uPtr) ) {

		// Always send a response !!
		}

	delete pData;

	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysN2SystemDriver::HandleReadMulti(void)
{
	UINT uPtr = 4;
	
	UINT uRegion = GetChar2(uPtr);

	UINT uLook = FindLookup(uRegion);

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = 1;
	
	CAddress Addr;

	Addr.a.m_Table	= Table[uLook];

	Addr.a.m_Type   = Type[uLook];
	
	Addr.a.m_Extra	= uRegion - Region[uLook];

	Addr.a.m_Offset = uObj * Attrib[uLook] + (uAtr - 1);

	UINT uCount = Attrib[uLook];
	
	PDWORD pData = new DWORD[uCount];

	memset(PBYTE(pData), 0, uCount * sizeof(DWORD));

	UINT uSuccess = 0;
	
	if( (uSuccess = Read(Addr, pData, uCount)) ) {

		// Always send a response !!

		}
	
	SendData(Addr.a.m_Type, uLook, uAtr, pData, uCount);
	
	delete pData;
	}

void CMetaSysN2SystemDriver::HandleOverride(void)
{
	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysN2SystemDriver::HandleOverrideRelease(void)
{
	Start('A');

	AddByte(CR);

	Tx();
	}
 
void CMetaSysN2SystemDriver::HandleError(UINT uCode)
{
	Start('N');

	PutChar2(uCode);

	AddByte(CR);

	Tx();
	}

BOOL CMetaSysN2SystemDriver::SetData(CAddress Addr, UINT uLook, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos)
{
	switch( Table[uLook]) {

		case 1:  
		case 2:  return SetAnalogs	(Addr, uAtr, pData, uCount, uPos, uLook);	break;
		case 3:  
		case 4:	 return SetBinarys	(Addr, uAtr, pData, uCount, uPos, uLook);	break;
		case 5:  
		case 6:
		case 7:	 return SetInternals	(Addr, uAtr, pData, uCount, uPos);		break;
	  	}

	return FALSE;
	}

BOOL CMetaSysN2SystemDriver::Set(CAddress Addr, PDWORD pData, UINT uCount)
{
	if( COMMS_SUCCESS(Write(Addr, pData, 1)) ) {
	
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CMetaSysN2SystemDriver::SetAnalogs(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos, UINT uLook)
{	
	UINT u = uAtr;
	
	for( UINT i = 0; u < uAtr + uCount; u++ ) {

		if( u == 1 ) {

			pData[0] = GetChar2(uPos);

			Set(Addr, pData, 1);

			Addr.a.m_Offset += 1;

			continue;
			}
	      
		if( u == 2 || u == 3 ) {

			Addr.a.m_Offset += 1;

			continue;
			}

		pData[i] = GetChar8(uPos);

		i++;
		}

	MakeMin(uCount, UINT(Attrib[uLook] - 3)); 
	
	return Set(Addr, pData, uCount);
	}

BOOL CMetaSysN2SystemDriver::SetBinarys(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos, UINT uLook)
{
	UINT u = uAtr;
	
	for( UINT i = 0; u < uAtr + uCount; u++ ) {

		if( u == 1 ) {

			pData[0] = GetChar2(uPos);

			Set(Addr, pData, 1);

			Addr.a.m_Offset += 1;

			continue;
			}
	      
		if( u == 2 ) {

			Addr.a.m_Offset += 1;

			continue;
			}

		if( Addr.a.m_Table == 3 && u == 4 ) {

			uCount = 1;

			break;
			}

		pData[i] = GetChar4(uPos);

		i++;
		}

	MakeMin(uCount, UINT(Attrib[uLook] - 2)); 
	
	return Set(Addr, pData, uCount);
	}

BOOL CMetaSysN2SystemDriver::SetInternals(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos)
{
	UINT uBytes = m_Ptr - 2 - 7;

	switch( uBytes ) {

		case 2:	pData[0] = GetChar2(uPos);	break;
		case 4: pData[0] = GetChar4(uPos);	break;
		case 8: pData[0] = GetChar8(uPos);	break;
		}

	return Set(Addr, pData, 1);
	}

void CMetaSysN2SystemDriver::SendData(UINT uType, UINT uLook, UINT uAtr, PDWORD pData, UINT uCount)
{
	switch( Table[uLook]) {

		case 1:  
		case 2:  SendAnalogs	  (uAtr, pData, uCount);	break;
		case 3:  SendBinaryInputs (uAtr, pData, uCount);	break;
		case 4:	 SendBinaryOutputs(uAtr, pData, uCount);	break;
		case 5:  
		case 6:
		case 7:	 SendInternals	  (uType, uAtr, pData, uCount);	break;
		}
	}

void CMetaSysN2SystemDriver::SendAnalogs(UINT uAtr, PDWORD pData, UINT uCount)
{
	Start('A');

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr <= 2 ) {

			PutChar2(pData[u]);

			continue;
			}

		PutChar8(pData[u]);
		}
		
	AddCheck();

	Tx();
	}

void CMetaSysN2SystemDriver::SendBinaryInputs(UINT uAtr, PDWORD pData, UINT uCount)
{
	Start('A');

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr <= 2 ) {

			PutChar2(pData[u]);
			}
		
		else if( uAtr == 3 ) {

			PutChar4(pData[u]);
			}
		else {		   
			PutChar8(pData[u]);
			}
		}
		
	AddCheck();

	Tx();
	}

void CMetaSysN2SystemDriver::SendBinaryOutputs(UINT uAtr, PDWORD pData, UINT uCount)
{
	Start('A');

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr <= 2 ) {

			PutChar2(pData[u]);

			continue;
			}
		
		PutChar4(pData[u]);
		}
		
	AddCheck();

	Tx();
	}

void CMetaSysN2SystemDriver::SendInternals(UINT uType, UINT uAtr, PDWORD pData, UINT uCount)
{
	Start('A');

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr == 1 ) {

			PutChar2(pData[u]);

			continue;
			}
		   
		switch( uType ) {

			case addrByteAsByte: PutChar2(pData[u]);	break;
			case addrWordAsWord: PutChar4(pData[u]);	break;
			case addrLongAsLong: PutChar8(pData[u]);	break;
			case addrRealAsReal: PutChar8(pData[u]);	break;
			}
		}
		
	AddCheck();

	Tx();
	}

// Frame Building
 		
void CMetaSysN2SystemDriver::Start(UINT uByte)
{
	m_Ptr = 0;

	AddByte(uByte);

	m_Check = 0;
	}

void CMetaSysN2SystemDriver::AddByte(BYTE bByte)
{
	m_Tx[m_Ptr] = bByte;
	
	m_Ptr++;

	m_Check += bByte;
	}

void CMetaSysN2SystemDriver::AddCheck(void)
{
	PutChar2(m_Check % 256);

	AddByte(CR);
	}

void CMetaSysN2SystemDriver::PutChar1(DWORD Data)
{
	AddByte(m_pHex[Data & 0xF]);	
	}

void CMetaSysN2SystemDriver::PutChar2(DWORD Data)
{
	AddByte(m_pHex[(Data & 0xFF) / 0x10]);

	AddByte(m_pHex[(Data & 0xFF) % 0x10]);
	}

void CMetaSysN2SystemDriver::PutChar4(DWORD Data)
{		
	PutChar2((Data >> 8) & 0xFF);
	
	PutChar2(Data & 0xFF);
	}

void CMetaSysN2SystemDriver::PutChar8(DWORD Data)
{
	PutChar4(HIWORD(Data));

	PutChar4(LOWORD(Data));
	} 

// Frame Access

UINT CMetaSysN2SystemDriver::GetChar1(UINT& uPos)
{
	UINT uChar = m_Rx[uPos];

	uPos++;

	if( uChar >= '0' && uChar <= '9' ) {

		return uChar - '0';
		}

	return uChar - '@' + 9;
      	}

UINT CMetaSysN2SystemDriver::GetChar2(UINT& uPos, BOOL fHex)
{
	UINT uChar = GetChar1(uPos);

	fHex ? uChar <<= 4 : uChar *= 10;

	uChar += GetChar1(uPos);

	return uChar;
	}

UINT CMetaSysN2SystemDriver::GetChar4(UINT& uPos, BOOL fHex)
{
	UINT uHi = GetChar2(uPos, fHex);

	UINT uLo = GetChar2(uPos, fHex);
	
	return MAKEWORD(uLo, uHi);
	}

UINT CMetaSysN2SystemDriver::GetChar8(UINT& uPos)
{
	UINT uHi = GetChar4(uPos);

	UINT uLo = GetChar4(uPos);
	
	return MAKELONG(uLo, uHi);
	}

// Helpers

BOOL CMetaSysN2SystemDriver::IsOnline(void)
{
	return m_fOnline;
	}

BOOL CMetaSysN2SystemDriver::IsSynchTimeCmd(UINT uCmd, UINT uSub)
{
	return uCmd == 0 && uSub == 0;
 	}

BOOL CMetaSysN2SystemDriver::IsPollNoAck(UINT uCmd, UINT uSub)
{
	return uCmd == 0 && uSub == 4;
	}

BOOL CMetaSysN2SystemDriver::IsPollWithAck(UINT uCmd, UINT uSub)
{
	return uCmd == 0 && uSub == 5;
	}

BOOL CMetaSysN2SystemDriver::IsIdentifyDeviceTypeCmd(UINT uCmd)
{
	if( !IsOnline() ) {

		m_fOnline = ( uCmd == 0xF && m_Ptr == 5 );
		}

	if( m_Ptr == 3 || ( uCmd == 0xF && m_Ptr == 5 ) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CMetaSysN2SystemDriver::IsWriteSingle(UINT uCmd)
{
	return uCmd == 2;
	}

BOOL CMetaSysN2SystemDriver::IsReadSingle(UINT uCmd)
{
	return uCmd == 1;
	}

BOOL CMetaSysN2SystemDriver::IsWriteMulti(UINT uCmd, UINT uSub)
{
	return uCmd == 7 && uSub == 7;
	}

BOOL CMetaSysN2SystemDriver::IsReadMulti(UINT uCmd, UINT uSub)
{
	return uCmd == 7 && uSub == 8;
	}

BOOL CMetaSysN2SystemDriver::IsOverride(UINT uCmd, UINT uSub)
{
	return uCmd == 7 && uSub == 2;
	}

BOOL CMetaSysN2SystemDriver::IsOverrideReleaseReq(UINT uCmd, UINT uSub)
{
	return uCmd == 7 && uSub == 3;
	}

UINT CMetaSysN2SystemDriver::FindLookup(UINT uRegion)
{
	for( UINT u = 0; u < elements(Table); u++ ) {

		if( uRegion >= UINT(Region[u]) && uRegion <= UINT(Region[u] + Span[u]) ) {

			return u;
			}
		}

	return NOTHING;
	}

void CMetaSysN2SystemDriver::FindAttribute(UINT uLook, UINT &uAtr)
{
	if( ( Table[uLook] == 1 || Table[uLook] == 2 )  && uAtr == 3 ) {

		uAtr = 2;
		}

	if( Table[uLook] == 5 ) {

		uAtr = 1;
		}
	}

UINT CMetaSysN2SystemDriver::FindCount(UINT uLook, UINT uAtr)
{
	if( ( Table[uLook] == 1 || Table[uLook] == 2 ) && uAtr == 2 ) {

		return 2;
		}

	if( Table[uLook] == 5 ) {

		return 2;
		}

	return 1;
	}

// End of File

