
//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "metasys.hpp"
#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Johnson Controls MetaSys N2 System Slave Driver
//
//

class CMetaSysN2SystemDriver2 : public CMetaSysN2SystemDriver
{
	public:
		// Constructor
		CMetaSysN2SystemDriver2(void);

		void HandleWriteSingle(UINT uRegion);
		void HandleReadSingle(UINT uRegion);
		void HandleWriteMulti(void);
		void HandleReadMulti(void);
		
		BOOL SetAnalogs(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos, UINT uLook);
		BOOL SetBinarys(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos, UINT uLook);
		BOOL SetInternals(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos);
		
		void SendAnalogs(UINT uAtr, PDWORD pData, UINT uCount);
		void SendBinaryInputs(UINT uAtr, PDWORD pData, UINT uCount);
		void SendBinaryOutputs(UINT uAtr, PDWORD pData, UINT uCount);
		void SendInternals(UINT uType, UINT uAtr, PDWORD pData, UINT uCount);
	};

// End of File
