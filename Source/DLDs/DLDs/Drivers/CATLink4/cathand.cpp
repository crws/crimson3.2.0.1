
#include "intern.hpp"

#include "catlink.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Custom Port Handler
//

// Constructor

CCatLinkHandler::CCatLinkHandler(IHelper *pHelper, CCatLinkDriver *pDriver, BYTE bSelf)
{
	StdSetRef();

	m_pHelper  = pHelper;

	m_pDriver  = pDriver;

	m_bSelf    = bSelf;

	m_fRun     = FALSE;

	m_uCatTick = 0;

	m_fCatConn = TRUE;

	m_uTimeout = pDriver->Handler_GetConnectionTime();
	}

// Destructor

CCatLinkHandler::~CCatLinkHandler(void)
{
	}

// Operations

void CCatLinkHandler::Start(void)
{
	m_fSync   = FALSE;

	m_fSelf   = FALSE;

	m_uRxPtr  = 0;

	m_uTxPtr  = 0;

	m_uTxSize = 0;

	m_fRun    = TRUE;

	StartEdgeTimer(TRUE);
	}

void CCatLinkHandler::Stop(void)
{
	m_fRun = FALSE;
	}

// IUnknown

HRESULT CCatLinkHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG CCatLinkHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG CCatLinkHandler::Release(void)
{
	StdRelease();
	}

// IPortHandler

void CCatLinkHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
	}

void CCatLinkHandler::OnRxData(BYTE bData)
{
	if( m_fRun && m_fSync ) {

		if( m_uRxPtr < elements(m_bRxData) ) {

			m_bRxData[m_uRxPtr++] = bData;
			}
		}
	}

void CCatLinkHandler::OnRxDone(void)
{
	if( m_fRun ) {

		StartEdgeTimer(FALSE);

		if( m_fSync ) {

			if( m_uRxPtr ) {

				if( m_fSelf ) {

					if( CheckContention() ) {

						m_pDriver->Handler_GetNextSend();
						}
					}

				if( m_uRxPtr < elements(m_bRxData) ) {

					BYTE bSum = 0;

					for( UINT n = 0; n < m_uRxPtr; n++ ) {

						bSum += m_bRxData[n];
						}

					if( !bSum ) {

						BYTE  bSrc   = m_bRxData[0];

						BYTE  bDest  = m_bRxData[1];

						PBYTE pFrame = m_bRxData + 2;

						UINT  uSize  = m_uRxPtr  - 3;

						if( bSrc != m_bSelf ) {

							m_uCatTick = GetTickCount();

							m_pDriver->Handler_SeeDrop(bSrc);

							m_pDriver->Handler_SeeDrop(bDest);

							ParseData(bSrc, bDest, pFrame, uSize);
							}
						}
					}
				}
			}

		m_fSync  = TRUE;

		m_fSelf  = FALSE;

		m_uRxPtr = 0;

		if( m_uTimeout > 0 ) {

			m_fCatConn = !(int(GetTickCount() - m_uCatTick - ToTicks(m_uTimeout)) >= 0);
			}
		}
	}

BOOL CCatLinkHandler::OnTxData(BYTE &bData)
{
	if( m_uTxPtr < m_uTxSize ) {

		bData = m_bTxData[m_uTxPtr++];

		return TRUE;
		}

	return FALSE;
	}

void CCatLinkHandler::OnTxDone(void)
{
	}

void CCatLinkHandler::OnOpen(CSerialConfig const &Config)
{
	}

void CCatLinkHandler::OnClose(void)
{
	}

void CCatLinkHandler::OnTimer(void)
{
	if( m_fRun ) {

		m_pPort->EnableInterrupts(FALSE);

		if( m_fCatConn && (m_uTxSize = m_pDriver->Handler_GetSendData(m_bTxData)) ) {

			m_pPort->EnableInterrupts(TRUE);

			ClearContention();

			m_uTxPtr = 1;

			m_fSelf  = TRUE;

			m_pPort->Send(m_bTxData[0]);

			m_pDriver->Handler_SetPoll();

			return;
			}
		
		m_pDriver->Handler_GetNextSend();

		m_pPort->EnableInterrupts(TRUE);

		StartEdgeTimer(TRUE);
		}
	}

// Frame Parsing

BOOL CCatLinkHandler::ParseData(BYTE bSrc, BYTE bDest, PCBYTE pData, UINT uCount)
{
	if( uCount ) {

		if( pData[0] == 0xFA ) {

			// Fault frames start with 0xFA. We accept them if they
			// are sent to us or to the broadcast address of 0x08.

			if( bDest == m_bSelf || bDest == 0x08 ) {

				m_pDriver->Handler_FaultData(bSrc, pData, uCount);
				}

			return TRUE;
			}

		if( pData[0] >= 0x82 && pData[0] <= 0x84 ) {

			// These are the old-style fault code frames used on
			// certain marine engines. We treat them as we would
			// other fault frames and pass them on for handling.

			if( bDest == m_bSelf || bDest == 0x08 ) {

				m_pDriver->Handler_FaultData(bSrc, pData, uCount);
				}

			if( bSrc == 0x0E && bDest == 0x0A ) {

				m_pDriver->Handler_FaultData(bSrc, pData, uCount);
				}

			return TRUE;
			}

		if( pData[0] == HIBYTE(pidSerial) ) {
			
			if( pData[1] == LOBYTE(pidSerial) ) {

				if( bDest == m_bSelf ) {

					// We accept the engine serial number PID if
					// it is sent to us, but we give it some
					// special handling to accept the string data.

					if( pData[2] > 8 ) {

						m_pDriver->Handler_SerialNum("INVALID", bSrc);
						}
					else {
						char s[12];

						memset(s, 0, sizeof(s));

						memcpy(s, pData + 3, pData[2]);

						m_pDriver->Handler_SerialNum(s, bSrc);
						}

					m_pDriver->Handler_DataFrame(bSrc, pidSerial, 0);
					}

				return TRUE;
				}
			}

		if( IsUnknownFrame(pData) ) {

			return FALSE;
			}

		if( IsPollSequence(pData) ) {

			return FALSE;
			}

		UINT uPtr     = 0;

		UINT uNameLen = GetNameLen(pData[uPtr]);

		UINT uDataLen = GetDataLen(pData[uPtr]);

		if( uNameLen && uDataLen ) {

			BOOL fSelf = (bDest == m_bSelf);

			if( fSelf || m_pDriver->Handler_ReportPID() ) {

				// If the frame is addresses to us (or PID 
				// reporting is being used) and contains a
				// valid PID, we accept that data. Note that we
				// do not scan for multiple PIDs in the same frame
				// as we will never ask for these. The name and
				// data lengths are variable, so we use the base
				// class functions to extract them.

				UINT uName = GetNameBytes(pData + uPtr, uNameLen);

				uPtr += uNameLen;

				UINT uData = GetDataBytes(pData + uPtr, uDataLen);

				uPtr += uDataLen;

				if( fSelf ) {

					m_pDriver->Handler_DataFrame(bSrc, uName, uData);
					}

				m_pDriver->Handler_SeePID(bSrc, uName, uData);
				}
			else {
				uPtr += uNameLen;

				uPtr += uDataLen;
				}

			if( uPtr < uCount ) {

				if( pData[uPtr] == 0xBE ) {

					// This sequence after a PID seems to be associated
					// with a write. We thought it was a write ack, but
					// we have seen it sent to MIDs other than us after
					// we have sent a write. We therefore accept it no
					// matter where it is directed.

					uPtr += 2;

					UINT uNameLen = GetNameLen  (pData[uPtr]);

					UINT uName    = GetNameBytes(pData + uPtr, uNameLen);
					
					m_pDriver->Handler_WriteDone(bSrc, uName);
					}
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCatLinkHandler::IsPollSequence(PCBYTE pData)
{
	if( pData[0] == 0x20 ) {

		return TRUE;
		}

	if( pData[0] == 0x60 ) {

		return TRUE;
		}

	if( pData[0] == 0xCF && pData[1] == 0x00 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkHandler::IsUnknownFrame(PCBYTE pData)
{
	if( pData[0] == 0x82 ) {

		return TRUE;
		}

	if( pData[0] == 0xA2 ) {

		return TRUE;
		}

	if( pData[0] == 0xBC ) {

		return TRUE;
		}

	if( pData[0] == 0xCF ) {

		return TRUE;
		}

	if( pData[0] == 0xE0 ) {

		return TRUE;
		}

	if( pData[0] == 0xCF && pData[1] == 0x80 ) {

		return TRUE;
		}

	if( pData[0] >= 0xF8 && pData[0] <= 0xFB ) {

		return TRUE;
		}

	return FALSE;
	}

// Port Management

void CCatLinkHandler::ClearContention(void)
{
	m_pPort->SetOutput(0x80, 0);
	}

BOOL CCatLinkHandler::CheckContention(void)
{
	return !m_pPort->GetInput(0x80);
	}

void CCatLinkHandler::StartEdgeTimer(BOOL fSlow)
{
	m_pPort->SetOutput(0x81, fSlow);
	}

// End of File
