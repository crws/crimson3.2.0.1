
#include "Intern.hpp"

#include "CipCommon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 2012 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// CIP Common Items
//

// Buffer Allocation

CBuffer * CCipCommon::CipAllocBuffer(void)
{
	for(;;) {

		CBuffer *pBuff = BuffAllocate(1280);

		if( pBuff ) {

			UINT uHead = 128;

			// Make a little more room in the front of the buffer.
			
			pBuff->AddTail  (uHead);

			pBuff->StripHead(uHead);

			return pBuff;
			}
		
		Sleep(5);
		}
	}

bool CCipCommon::CipReleaseBuffer(CBuffer * &pBuff)
{
	if( pBuff ) {

		pBuff->Release();

		pBuff = NULL;

		return true;
		}

	return false;
	}

// Trace Output

void CCipCommon::AfxTrace(PCTXT pText, ...)
{
	#if !defined(__ISense__)

	va_list pArgs;

	va_start(pArgs, pText);

	::AfxTrace(pText, pArgs);

	va_end(pArgs);

	#endif
	}

// End of File
