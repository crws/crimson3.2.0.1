
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CipConnect_HPP

#define	INCLUDE_CipConnect_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CipCommon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CEthernetIp;

class CCipPath;

//////////////////////////////////////////////////////////////////////////
//
// CIP Explicit Connection
//

class CCipConnect : public CCipCommon
{
	public:
		// Constructor
		CCipConnect(CEthernetIp *pEip, PCTXT pRoute);

		// Destructor
		~CCipConnect(void);

		// Attributes
		bool IsLayerUp(void) const;

		// Operations
		bool Open(void);
		bool Exchange(CBuffer * &pBuff, BYTE Service, CCipPath const &Path, UINT &Status);
		bool Close(void);

	protected:
		// Data Structures
		struct CRequest;
		struct CReply;
		struct CForwardOpenRequest;
		struct CForwardOpenReply;

		// Data Members
		CEthernetIp * m_pEip;
		bool	      m_fLayerUp;
		DWORD	      m_O2TConnection;
		DWORD	      m_T2OConnection;
		WORD	      m_Sequence;
		PCTXT	      m_pRoute;

		// Layer Transition
		bool LayerUp(void);
		bool LayerDown(void);
		void ClearData(void);

		// Connection Control
		bool ForwardOpen(void);
		bool ForwardClose(void);
		void BuildRoute(CCipPath &Path);
		void SetT2OConnection(void);

		// Message Exchange
		bool ExchangeUnconnect(CBuffer * &pBuff, BYTE Service, CCipPath const &Path, UINT &Status);
		bool ExchangeConnected(CBuffer * &pBuff, BYTE Service, CCipPath const &Path, UINT &Status);
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic Request
//

#pragma pack(1)

struct CCipConnect::CRequest
{
	BYTE Service;

	void ReOrder(void)
	{
		CipRo(Service);
		}
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Generic Reply
//

#pragma pack(1)

struct CCipConnect::CReply
{
	BYTE Service;
	BYTE Spare;
	BYTE Status;
	BYTE Extra;

	void ReOrder(void)
	{
		CipRo(Service);
		CipRo(Spare);
		CipRo(Status);
		CipRo(Extra);
		}
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Forward Open Request
//

#pragma pack(1)

struct CCipConnect::CForwardOpenRequest
{
	BYTE	TimingData;
	BYTE	TimeoutTicks;
	DWORD	O2TConnection;
	DWORD	T2OConnection;
	WORD	ConnectionSerial;
	WORD	OriginatorVendor;
	DWORD	OriginatorSerial;
	BYTE	TimeoutMultiplier;
	BYTE	Unused1;
	BYTE	Unused2;
	BYTE	Unused3;
	DWORD	O2TRate;
	WORD	O2TParams;
	DWORD	T2ORate;
	WORD	T2OParams;
	BYTE	Type;

	void ReOrder(void)
	{
		CipRo(TimingData);
		CipRo(TimeoutTicks);
		CipRo(O2TConnection);
		CipRo(T2OConnection);
		CipRo(ConnectionSerial);
		CipRo(OriginatorVendor);
		CipRo(OriginatorSerial);
		CipRo(TimeoutMultiplier);
		CipRo(Unused1);
		CipRo(Unused2);
		CipRo(Unused3);
		CipRo(O2TRate);
		CipRo(O2TParams);
		CipRo(T2ORate);
		CipRo(T2OParams);
		CipRo(Type);
		}
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Forward Open Reply
//

#pragma pack(1)

struct CCipConnect::CForwardOpenReply
{
	DWORD	O2TConnection;
	DWORD	T2OConnection;
	WORD	ConnectionSerial;
	WORD	OriginatorVendor;
	DWORD	OriginatorSerial;
	DWORD	O2TRate;
	DWORD	T2ORate;
	BYTE	AppReply;
	BYTE	Unused1;

	void ReOrder(void)
	{
		CipRo(O2TConnection);
		CipRo(T2OConnection);
		CipRo(ConnectionSerial);
		CipRo(OriginatorVendor);
		CipRo(OriginatorSerial);
		CipRo(O2TRate);
		CipRo(T2ORate);
		CipRo(AppReply);
		CipRo(Unused1);
		}
	};

#pragma pack()

// End of File

#endif
