
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_TcpIpSock_HPP

#define	INCLUDE_TcpIpSock_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CipCommon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Socket Interface
//

class CTcpIpSock : public CCipCommon
{
	public:
		// Constructor
		CTcpIpSock(DWORD Addr, WORD Port);

		// Destructor
		~CTcpIpSock(void);

		// Attributes
		bool IsLayerUp(void) const;

		// Operations
		bool Open(void);
		bool Send(CBuffer *  pBuff);
		bool Recv(CBuffer * &pBuff, UINT uSize, UINT uTime);
		bool Close(void);

	protected:
		// Data Members
		bool	  m_fOpen;
		ISocket * m_pSock;
		DWORD	  m_Addr;
		WORD	  m_Port;

		// Implementation
		bool MakeSocket(void);
		void FreeSocket(void);
	};

// End of File

#endif
