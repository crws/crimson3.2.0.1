
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CipPath_HPP

#define	INCLUDE_CipPath_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CipCommon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Path
//

class CCipPath : public CCipCommon
{
	public:
		// Constructor
		CCipPath(void);
		CCipPath(bool fPad);

		// Destructor
		~CCipPath(void);

		// Attributes
		bool IsEmpty(void) const;

		// Operations
		void SetPad(bool fPad);
		void AddPort(UINT Port, UINT Addr);
		void AddPort(UINT Port, PCSTR pAddr);
		void AddObject(UINT Class, UINT Instance);
		bool AddSymbol(PCSTR pName);
		void AddClass(UINT Data);
		void AddInstance(UINT Data);
		void AddInstance16(UINT Data);
		void AddMember(UINT Data);
		void AddConnectionPoint(UINT Data);
		void AddAttribute(UINT Data);
		void AddService(UINT Data);
		void AddLogical(BYTE Type, UINT Data);
		void AddLogical(BYTE Type, BYTE Format, UINT Data);
		void AddString(PCSTR pData);
		void AddByte(BYTE Data);

		// Transcription
		void Prefix(CBuffer *pBuff, bool fAddSize) const;
		void Append(CBuffer *pBuff, bool fAddSize) const;

		void Clear(void);
		PBYTE GetData(void);
		UINT GetSize(void);

	protected:
		// Data Members
		bool m_fPad;
		BYTE m_bData[192];
		UINT m_uData;

		// Helpers
		void CopyString(PCSTR pData);
	};

// End of File

#endif
