
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "CipConnection.hpp"

#include "EthernetIp.hpp"

#include "CipPath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Explicit Connection
//

// Constructor

CCipConnect::CCipConnect(CEthernetIp *pEip, PCTXT pRoute)
{
	m_pEip     = pEip;

	m_fLayerUp = false;

	m_pRoute   = pRoute;

	ClearData();
	}

// Destructor

CCipConnect::~CCipConnect(void)
{
	Close();

	delete m_pEip;
	}

// Attributes

bool CCipConnect::IsLayerUp(void) const
{
	return m_fLayerUp;
	}

// Operations

bool CCipConnect::Open(void)
{
	if( m_pEip->Open() ) {

		if( LayerUp() ) {

			return true;
			}

		m_pEip->Close();
		}

	return false;
	}

bool CCipConnect::Exchange(CBuffer * &pBuff, BYTE Service, CCipPath const &Path, UINT &Status)
{
	if( IsLayerUp() ) {

		if( ExchangeConnected(pBuff, Service, Path, Status) ) {

			return true;
			}

		if( Status == NOTHING ) {

			Close();
			}
		}

	return false;
	}

bool CCipConnect::Close(void)
{
	if( IsLayerUp() ) {

		LayerDown();

		return true;
		}

	return false;
	}

// Layer Transition

bool CCipConnect::LayerUp(void)
{
	if( ForwardOpen() ) {

		m_fLayerUp = true;

		return true;
		}

	return false;
	}

bool CCipConnect::LayerDown(void)
{
	if( m_pEip->IsLayerUp() ) {

		ForwardClose();

		m_pEip->Close();
		}

	m_fLayerUp = false;

	ClearData();

	return true;
	}

void CCipConnect::ClearData(void)
{
	m_O2TConnection = 0;

	m_T2OConnection = 0;

	m_Sequence      = 1;
	}

// Connection Control

bool CCipConnect::ForwardOpen(void)
{
	SetT2OConnection();

	CCipPath TargetPath(true);

	TargetPath.AddObject(cipConnectionManager, 1);

	CCipPath ConnectPath(true);

	BuildRoute(ConnectPath);

	ConnectPath.AddObject(cipMessageRouter, 1);

	CBuffer             *pBuff = CipAllocBuffer();

	CForwardOpenRequest *pSend = BuffAddTail(pBuff, CForwardOpenRequest);

	pSend->TimingData	 = 6;
	pSend->TimeoutTicks	 = 128;
	pSend->O2TConnection	 = 0;
	pSend->T2OConnection	 = m_T2OConnection;
	pSend->ConnectionSerial	 = LOWORD(m_T2OConnection);
	pSend->OriginatorVendor	 = 176;
	pSend->OriginatorSerial	 = 0x1234;
	pSend->TimeoutMultiplier = 2;
	pSend->Unused1		 = 0;
	pSend->Unused2		 = 0;
	pSend->Unused3		 = 0;
	pSend->O2TRate		 = 0x001E8480;
	pSend->O2TParams	 = 0x43F8;
	pSend->T2ORate		 = 0x001E8480;
	pSend->T2OParams	 = 0x43F8;
	pSend->Type		 = 0xA3;

	pSend->ReOrder();
	
	ConnectPath.Append(pBuff, true);

	UINT Status = 0;

	if( ExchangeUnconnect(pBuff, cipForwardOpen, TargetPath, Status) ) {

		CForwardOpenReply *pReply = BuffStripHead(pBuff, CForwardOpenReply);

		pReply->ReOrder();

		m_O2TConnection = pReply->O2TConnection;

		CipReleaseBuffer(pBuff);

		return true;
		}

	if( Status == statFailure ) {

		ClearData();
		}

	return false;
	}

bool CCipConnect::ForwardClose(void)
{
	// TODO -- Implement

	return true;
	}

void CCipConnect::BuildRoute(CCipPath &Path)
{
	if( !strcmp(m_pRoute, "NULL") ) {

		return;
		}

	if( m_pRoute[0] == '\0' ) {

		Path.AddPort(1, UINT(0));

		return;
		}

	CHAR Route[33] = {0};

	strncpy(Route, m_pRoute, sizeof(Route));

	PTXT pTok = strtok(Route, ",");

	UINT uPort = 0;

	for( UINT n = 0; pTok; n++ ) {

		if( (n % 2) == 0 ) {

			uPort = strtol(pTok, NULL, 10);
			}
		else {
			BOOL fNum = TRUE;

			for( UINT u = 0; pTok[u]; u++ ) {

				if( !isdigit(pTok[u]) ) {

					fNum = FALSE;

					break;
					}
				}

			if( fNum ) {

				UINT uAddr = strtol(pTok, NULL, 10);

				Path.AddPort(uPort, uAddr);
				}
			else {
				Path.AddPort(uPort, pTok);
				}
			}

		pTok = strtok(NULL, ",");
		}
	}

void CCipConnect::SetT2OConnection(void)
{
	m_T2OConnection = rand();
	}

// Message Exchange

bool CCipConnect::ExchangeUnconnect(CBuffer * &pBuff, BYTE Service, CCipPath const &Path, UINT &Status)
{
	Path.Prefix(pBuff, true);

	CRequest *pRequest = BuffAddHead(pBuff, CRequest);

	pRequest->Service  = Service;

	pRequest->ReOrder();

	if( m_pEip->ExchangeUnconnect(pBuff) ) {

		CReply *pReply = BuffStripHead(pBuff, CReply);

		pReply->ReOrder();

		switch( (Status = pReply->Status) ) {

			case statSuccess:
			case statPartial:

				return true;
			}

		CipReleaseBuffer(pBuff);

		return false;
		}

	Status = NOTHING;

	return false;
	}

bool CCipConnect::ExchangeConnected(CBuffer * &pBuff, BYTE Service, CCipPath const &Path, UINT &Status)
{
	Path.Prefix(pBuff, true);

	CRequest *pRequest = BuffAddHead(pBuff, CRequest);

	pRequest->Service  = Service;

	pRequest->ReOrder();

	if( m_pEip->ExchangeConnected(pBuff, m_O2TConnection, m_T2OConnection, m_Sequence++) ) {

		CReply *pReply = BuffStripHead(pBuff, CReply);

		pReply->ReOrder();

		switch( (Status = pReply->Status) ) {

			case statSuccess:
			case statPartial:

				return true;
			}

		CipReleaseBuffer(pBuff);

		return false;
		}

	Status = NOTHING;

	return false;
	}

// End of File
