
#include "Intern.hpp"

#include "CipClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 2012 Red Lion Controls
//
// All Rights Reserved
//

#include "CipConnection.hpp"

#include "CipPath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Client
//

// Constructor

CCipClient::CCipClient(CCipConnect *pConnect)
{
	m_pConnect	= pConnect;

	m_fLayerUp	= false;

	m_pTags		= NULL;

	m_uTags		= 0;

	m_uAlloc	= 0;

	m_pStructTags	= NULL;

	m_uStructTags	= 0;

	m_uStructAlloc	= 0;

	m_ppNames	= NULL;

	m_uKeepNames	= 0;

	m_uKeepAlloc	= 0;

	m_pCached	= NULL;

	m_uCache	= 0;

	m_uCacheAlloc	= 0;

	m_fSort		= false;

	ClearData();
	}

// Destructor

CCipClient::~CCipClient(void)
{
	Close();

	delete m_pConnect;
	}

// Attributes

bool CCipClient::IsLayerUp(void) const
{
	return m_fLayerUp;
	}

// Operations

bool CCipClient::Open(void)
{
	if( m_pConnect->Open() ) {

		if( LayerUp() ) {

			if( FindAvailableTags() ) {

				return true;
				}
			}

		m_pConnect->Close();
		}

	return false;
	}

UINT CCipClient::ReadTag(PCTXT pName, PDWORD pData, UINT uCount, CCipPath const &TargetPath)
{
	if( IsLayerUp() ) {

		UINT uRead = 0;

		CBuffer *pBuff = CipAllocBuffer();

		UINT    Status = NOTHING;

		CipAddTail(pBuff, WORD, uCount);

		if( m_pConnect->Exchange(pBuff, cipReadItem, TargetPath, Status) ) {

			WORD Type = CipStripWord(pBuff);

			while( pBuff->GetSize() ) {

				switch( Type ) {

					case 0x00C1:
					case 0x00C2:
						pData[uRead++] = CipStripHead(pBuff, BYTE);
						break;

					case 0x00C3:
						pData[uRead++] = CipStripWord(pBuff);
						break;

					case 0x00C4:
					case 0x00CA:
					case 0x00D3:
						pData[uRead++] = CipStripLong(pBuff);
						break;

					default:
						CipReleaseBuffer(pBuff);

						return 0;
					}
				}

			CipReleaseBuffer(pBuff);

			if( Status == statSuccess ) {

				return uCount;
				}

			if( Status == statPartial ) {

				return uRead;
				}

			return 0;
			}

		CipReleaseBuffer(pBuff);

		if( Status == NOTHING ) {

			Close();
			}
		}

	return 0;
	}

UINT CCipClient::WriteTag(PCTXT pName, PDWORD pData, UINT uCount, CCipPath const &TargetPath)
{
	if( IsLayerUp() ) {

		CBuffer *pBuff = CipAllocBuffer();

		UINT    Status = NOTHING;

		CipAddTail(pBuff, WORD, 1);

		if( m_pConnect->Exchange(pBuff, cipReadItem, TargetPath, Status) ) {

			WORD Type = CipStripWord(pBuff);

			CipReleaseBuffer(pBuff);

			UINT uWrite = 96;

			MakeMin(uWrite, uCount);

			pBuff = CipAllocBuffer();

			CipAddTail(pBuff, WORD,  Type);

			CipAddTail(pBuff, WORD,  uWrite);

			for( UINT n = 0; n < uWrite; n++ ) {
				
				switch( Type ) {

					case 0x00C1:
					case 0x00C2:
						CipAddTail(pBuff, BYTE, pData[n]);
						break;

					case 0x00C3:
						CipAddTail(pBuff, WORD, pData[n]);
						break;

					case 0x00C4:
					case 0x00CA:
					case 0x00D3:
						CipAddTail(pBuff, DWORD, pData[n]);
						break;

					default:
						CipReleaseBuffer(pBuff);

						return 0;
					}
				}

			if( m_pConnect->Exchange(pBuff, cipWriteItem, TargetPath, Status) ) {

				CipReleaseBuffer(pBuff);

				return uWrite;
				}
			}

		if( Status == NOTHING ) {

			Close();
			}

		CipReleaseBuffer(pBuff);
		}

	return 0;
	}

bool CCipClient::Close(void)
{
	FreeTags(m_pTags, m_uAlloc, m_uTags);

	FreeTags(m_pStructTags, m_uStructAlloc, m_uStructTags);

	FreeKeepTags();

	if( IsLayerUp() ) {

		LayerDown();

		return true;
		}

	return false;
	}

UINT CCipClient::CreateWatchList(void)
{
	CCipPath TargetPath(true);

	TargetPath.AddObject(cipWatchList, 0);

	CBuffer *pBuff = CipAllocBuffer();

	UINT    Status = 0;

	WORD	WatchListInst;

	// Watch list creation arguments,
	// determined by observation.
	CipAddTail(pBuff, WORD, 0x0001);
	CipAddTail(pBuff, WORD, 0x0003);
	CipAddTail(pBuff, WORD, 0x0003);

	if( m_pConnect->Exchange(pBuff, cipCreate, TargetPath, Status) ) {

		WatchListInst = *PU2(pBuff->GetData());

		CipRo(WatchListInst);

		CipReleaseBuffer(pBuff);

		return WatchListInst;
		}

	CipReleaseBuffer(pBuff);

	return 0;
	}

bool CCipClient::ConfigureWatchList(CCipPath const &Path, UINT uInst, UINT uSize)
{
	CBuffer *pBuff = CipAllocBuffer();
	
	UINT Status = NOTHING;

	CCipPath TargetPath(true);

	TargetPath.AddObject(cipWatchList, uInst);

	// Configuration arguments found by observation.
	CipAddTail(pBuff, WORD, 0x0002);
	CipAddTail(pBuff, WORD, 0x0101);
	CipAddTail(pBuff, BYTE, 0x1);

	CipAddTail(pBuff, BYTE, uSize);
	CipAddTail(pBuff, BYTE, 0x0);

	Path.Append(pBuff, true);
	
	if( m_pConnect->Exchange(pBuff, cipLoadWatchList, TargetPath, Status) ) {

		CipReleaseBuffer(pBuff);

		return true;
		}

	CipReleaseBuffer(pBuff);

	return false;
	}

bool CCipClient::DeleteWatchList(UINT uInst)
{
	CCipPath TargetPath(true);

	TargetPath.AddObject(cipWatchList, uInst);

	CBuffer *pBuff = CipAllocBuffer();

	UINT    Status = 0;

	if( m_pConnect->Exchange(pBuff, cipDelete, TargetPath, Status) ) {

		CipReleaseBuffer(pBuff);

		return true;
		}

	CipReleaseBuffer(pBuff);

	return false;
	}

bool CCipClient::GetTagInfo(PCTXT pName, CCipPath &Path, UINT &uSize, UINT &uType, UINT &uExtra)
{
	CTag *pFind = NULL;

	// Dummy tag to pass to the search function
	CTag Search;

	char *s = strdup(pName);

	char *c = NULL;
	char *d = NULL;

	// Remove array subscript for name lookup
	while( (c = strchr(s, '[')) ) {

		if( (d = strchr(s, ']')) ) {

			UINT uLen = strlen(d + 1) + 1;

			memmove(c, d + 1, uLen);
			}
		}

	Search.m_pName = PTXT(s);

	pFind = (CTag *) bsearch(&Search, m_pTags, m_uTags, sizeof(CTag), CompFunc);
	
	free(s);

	if( pFind ) {

		Path = pFind->m_Path;

		uSize = pFind->m_uSize;

		uType = pFind->m_uType;

		uExtra = pFind->m_wExtra;

		return true;
		}

	return false;
	}

bool CCipClient::GetWatchListData(PBYTE pData, UINT uInst, UINT uSize)
{
	CBuffer *pBuff = CipAllocBuffer();

	CCipPath TargetPath(true);

	TargetPath.AddObject(cipWatchList, uInst);
	
	UINT Status = 0;

	if( m_pConnect->Exchange(pBuff, cipReadItem, TargetPath, Status) ) {

		memcpy(pData, pBuff->GetData(), uSize);

		CipReleaseBuffer(pBuff);

		return true;
		}

	CipReleaseBuffer(pBuff);

	Close();

	return false;
	}

void CCipClient::AllocKeepTags(UINT uKeep)
{
	m_uKeepAlloc = uKeep;

	m_uKeepNames = 0;

	m_ppNames = new PTXT [ m_uKeepAlloc ];

	memset(m_ppNames, 0, m_uKeepAlloc * sizeof(PTXT));
	}

void CCipClient::AddTagToKeep(PCTXT pName)
{
	if( pName ) {

		if( m_uKeepNames < m_uKeepAlloc ) {

			m_ppNames[m_uKeepNames] = strdup(pName);

			// The tag names from the controller do not
			// have array indices in them, so we strip
			// out the indices from the names here.

			char *s = m_ppNames[m_uKeepNames];

			char *c = NULL;
			char *d = NULL;

			while( (c = strchr(s, '[')) ) {

				if( (d = strchr(s, ']')) ) {

					UINT uLen = strlen(d + 1) + 1;

					memmove(c, d + 1, uLen);
					}
				}

			m_uKeepNames++;
			}
		}
	}

void CCipClient::FreeKeepTags(void)
{
	if( m_ppNames ) {

		for( UINT u = 0; u < m_uKeepNames; u++ ) {

			free(m_ppNames[u]);

			m_ppNames[u] = NULL;
			}

		delete [] m_ppNames;

		m_ppNames = NULL;
		}
	}

// Layer Transition

bool CCipClient::LayerUp(void)
{
	m_fLayerUp = true;

	return true;
	}

bool CCipClient::LayerDown(void)
{
	if( m_pConnect->IsLayerUp() ) {

		m_pConnect->Close();
		}

	m_fLayerUp = false;

	ClearData();

	return true;
	}

void CCipClient::ClearData(void)
{
	}

// Tag List

void CCipClient::AddTag(CTag const &Tag)
{
	if( Tag.m_uType & 0x8000 ) {

		AddStruct(Tag);
		}
	else {

		if( m_uTags == m_uAlloc ) {

			CTag *pTags;

			m_uAlloc = m_uAlloc ? (m_uAlloc << 1) : 64;

			pTags    = new CTag [ m_uAlloc ];

			if( m_pTags ) {

				memcpy(pTags, m_pTags, sizeof(CTag) * m_uTags);

				delete [] m_pTags;
				}

			m_pTags = pTags;
			}

		m_pTags[m_uTags++] = Tag;

		m_fSort = true;
		}
	}

void CCipClient::AddStruct(CTag const &Tag)
{
	if( m_uStructTags == m_uStructAlloc ) {

		CTag *pTags;

		m_uStructAlloc = m_uStructAlloc ? (m_uStructAlloc << 1) : 64;

		pTags    = new CTag [ m_uStructAlloc ];

		if( m_pStructTags ) {

			memcpy(pTags, m_pStructTags, sizeof(CTag) * m_uStructTags);

			delete [] m_pStructTags;
			}

		m_pStructTags = pTags;
		}

	m_pStructTags[m_uStructTags++] = Tag;
	}

bool CCipClient::ShouldKeepTag(CTag const &Tag)
{
	// Keep structured types
	if( Tag.m_uType & 0x8000 ) {

		return ShouldKeepStruct(Tag);
		}

	// Keep Programs
	if( Tag.m_uType == 0x1068 ) {

		return true;
		}

	// Look for the tag name in the names we kept from the controller list.
	char **pFind = (char **) bsearch(&Tag.m_pName, m_ppNames, m_uKeepNames, sizeof(char *), CompNames);

	if( pFind ) {

		return true;
		}

	return false;
	}

bool CCipClient::ShouldKeepStruct(CTag const &Tag)
{
	char **pFind = (char **) bsearch(&Tag.m_pName, m_ppNames, m_uKeepNames, sizeof(char *), CompStruct);

	return pFind != NULL;
	}

void CCipClient::FreeTags(CTag *&pTags, UINT &uAlloc, UINT &uCount)
{
	if( pTags ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CTag const &Tag = pTags[n];
			
			delete [] Tag.m_pName;
			}

		delete [] pTags;

		pTags = NULL;

		uAlloc = 0;

		uCount = 0;
		}
	}

void CCipClient::SortTags(void)
{
	if( m_pTags ) {

		if( m_fSort ) {

			qsort(m_pTags, m_uTags, sizeof(CTag), CompFunc);

			m_fSort = false;
			}
		}
	}

// Sort/Search Comparison Functions

int CCipClient::CompFunc(void const *p1, void const *p2)
{
	CTag const *t1 = (CTag const *) p1;

	CTag const *t2 = (CTag const *) p2;

	return stricmp(t1->m_pName, t2->m_pName);
	}

int CCipClient::CompNames(void const *p1, void const *p2)
{
	char **pStr1 = (char **) p1;

	char **pStr2 = (char **) p2;

	return stricmp(*pStr1, *pStr2);
	}

int CCipClient::CompStruct(void const *p1, void const *p2)
{
	char **pTest = (char **) p1;

	char **pKeep = (char **) p2;

	UINT uLen = strlen(*pKeep);

	char *pName = new char[uLen + 1];

	memset(pName, 0, uLen + 1);

	bool fProgram = (strncmp("Program:", *pKeep, 8) == 0);

	char *pFind = NULL;

	if( fProgram ) {

		pFind = strchr(*pKeep, '.');

		if( pFind ) {

			pFind = strchr(pFind + 1, '.');
			}
		}
	else {
		pFind = strchr(*pKeep, '.');
		}
		
	if( pFind ) {

		strncpy(pName, *pKeep, UINT(pFind - *pKeep));
		}

	int nCompare = stricmp(*pTest, pName);

	delete [] pName;

	return nCompare;
	}

bool CCipClient::FindAvailableTags(void)
{
	qsort(m_ppNames, m_uKeepNames, sizeof(char *), CompNames);

	if( FindTags(NULL) ) {

		for( UINT n = 0; n < m_uTags; n++ ) {

			CTag const &Tag = m_pTags[n];

			if( Tag.m_uType == 0x1068 ) {

				FindTags(Tag.m_pName);
				}
			}

		for( UINT u = 0; u < m_uStructTags; u++ ) {

			CTag &Tag = m_pStructTags[u];

			ReadStructMetaData(Tag);
			}

		SortTags();

		FreeTags(m_pStructTags, m_uStructAlloc, m_uStructTags);

		FreeKeepTags();

		ClearStructCache();

		return true;
		}

	return false;
	}

bool CCipClient::FindTags(PCTXT pScope)
{
	// If pScope is NULL, then we will find the 
	// controller scoped tags.
	bool fProgram = pScope != NULL;

	DWORD id = 0;

	WORD Handle = 0;

	if( fProgram ) {

		Handle = FindProgramHandle(pScope);
		}

	for(;;) {

		CCipPath TargetPath(true);

		if( fProgram ) {

			TargetPath.AddString(pScope);
			}

		TargetPath.AddObject(cipTagDirectory, id);

		CBuffer *pBuff = CipAllocBuffer();

		UINT    Status = 0;

		CipAddTail(pBuff, WORD, 0x0004);
		CipAddTail(pBuff, WORD, 0x0002);
		CipAddTail(pBuff, WORD, 0x0007);
		CipAddTail(pBuff, WORD, 0x0008);
		CipAddTail(pBuff, WORD, 0x0001);

		if( m_pConnect->Exchange(pBuff, cipDumpFolder, TargetPath, Status) ) {

			while( pBuff->GetSize() ) {

				CTag Tag = { };
				
				Tag.m_uIndex = CipStripWord(pBuff);

				pBuff->StripHead(2);

				Tag.m_uType = CipStripWord(pBuff);

				Tag.m_uSize = CipStripWord(pBuff);

				BOOL bRawType = Tag.m_uType & 0xFF;

				if( bRawType == 0xC1 ) {

					SetBitOffset(Tag);
					}

				pBuff->StripHead(12);

				UINT uName = CipStripWord(pBuff);

				char *name = NULL;

				if( fProgram ) {

					UINT uSize = strlen(pScope) + uName + 1;

					name = new char[uSize + 1];

					strncpy(name, pScope, uSize);

					strncat(name, ".", uSize);

					memcpy(name + strlen(pScope) + 1, pBuff->StripHead(uName), uName);

					name[uSize] = '\0';
					}
				else {
					name = new char [ uName + 1 ];

					memcpy(name, pBuff->StripHead(uName), uName);

					name[uName] = '\0';
					}

				Tag.m_pName = name;

				Tag.m_Path.SetPad(true);

				if( fProgram ) {

					Tag.m_ProgHandle = Handle;

					Tag.m_Path.AddObject(cipProgram, Tag.m_ProgHandle);
					}				

				Tag.m_Path.AddObject(cipTagDirectory, Tag.m_uIndex);

				Tag.m_fInit = true;

				if( ShouldKeepTag(Tag) ) {

					AddTag(Tag);
					}
				else {
					delete [] Tag.m_pName;
					}

				id = Tag.m_uIndex + 1;
				}

			CipReleaseBuffer(pBuff);

			if( Status == statSuccess ) {

				return true;
				}

			if( Status == statPartial ) {

				continue;
				}
			}

		CipReleaseBuffer(pBuff);

		Close();

		return false;
		}

	return false;
	}

WORD CCipClient::FindProgramHandle(PCTXT pName)
{
	CCipPath TargetPath(true);

	CBuffer *pBuff = CipAllocBuffer();

	UINT Status = 0;

	WORD Handle = 0;

	TargetPath.AddString(pName);

	CipAddTail(pBuff, WORD, 0x0001);

	CipAddTail(pBuff, WORD, 0x0016);

	if( m_pConnect->Exchange(pBuff, cipGetAttributeList, TargetPath, Status) ) {

		if( Status == statSuccess ) {

			Handle = PU2(pBuff->GetData())[3];

			CipRo(Handle);

			CipReleaseBuffer(pBuff);

			return Handle;
			}
		}

	CipReleaseBuffer(pBuff);

	Close();

	return Handle;
	}

bool CCipClient::ReadStructMetaData(CTag &Tag)
{
	CStructType Struct;

	if( !FindStructInfo(Tag, Struct) ) {

		Close();

		return false;
		}

	WORD wMembers	 = Struct.m_wMembers;

	WORD wSize	 = Struct.m_wSize;

	PBYTE pStructure = Struct.m_pStruct;

	char ** ppNames = NULL;

	FindMemberStrings(Tag, Struct, ppNames);

	for( int t = 0; t < wMembers; t++ ) {

		CTag MemberTag = Tag;

		if( !MemberTag.m_fInit ) {

			if( Tag.m_ProgHandle ) {

				MemberTag.m_Path.AddObject(cipProgram, Tag.m_ProgHandle);
				}

			MemberTag.m_Path.AddObject(cipTagDirectory, Tag.m_uIndex);

			MemberTag.m_fInit = true;
			}

		MemberTag.m_Path.AddLogical(2, t);

		// Offset into the data for this tag.
		PBYTE pInfo = pStructure + t * 8;

		MemberTag.m_wExtra = IntelToHost(*PU2(pInfo));

		UINT uType = IntelToHost(PU2(pInfo)[1]);

		MemberTag.m_uType = uType;

		MemberTag.m_uSize = GetAtomicSize(uType);

		UINT size = strlen(Tag.m_pName) + strlen(ppNames[t]) + 2;

		char *name = new char[size];

		strncpy(name, Tag.m_pName, size);

		strncat(name, ".", size);

		strncat(name, ppNames[t], size);

		MemberTag.m_pName = name;

		if( uType & 0x8000 ) {

			CStructType Dummy;

			if( !GetCachedStruct(uType, Dummy) ) {

				AddTag(MemberTag);
				}

			ReadStructMetaData(MemberTag);
			}

		else if( ShouldKeepTag(MemberTag) ) {

			AddTag(MemberTag);
			}
		else {
			delete [] MemberTag.m_pName;
			}
		}

	for( UINT u = 0; u < wMembers; u++ ) {

		delete [] ppNames[u];
		}

	delete [] ppNames;

	return true;
	}

bool CCipClient::FindStructInfo(CTag const &Tag, CStructType &Struct)
{
	if( GetCachedStruct(Tag.m_uType, Struct) ) {

		return true;
		}
	else {
		if( !GetStructSizes(Tag, Struct.m_wSize, Struct.m_wMembers) ) {

			return false;
			}

		UINT Status = 0;

		CCipPath Path(true);

		UINT uInst = Tag.m_uType & 0x0FFF;

		Path.AddObject(cipStructuredType, uInst);

		// This calculation was determined by observation.
		// It's probably meaningful to someone at Allen-Bradley, 
		// but I'm not sure why it's like this.
		UINT uStructSize = (Struct.m_wSize * 4) - 0x14;

		WORD wRequest = uStructSize;

		WORD wStart = 0;

		PBYTE pStructure = new BYTE[ wRequest ];

		while( wRequest > 0 ) {

			CBuffer *pBuff = CipAllocBuffer();

			CipAddTail(pBuff, WORD, wStart);
			CipAddTail(pBuff, WORD, 0x0000);
			CipAddTail(pBuff, WORD, wRequest);

			if( m_pConnect->Exchange(pBuff, cipReadItem, Path, Status) ) {

				memcpy(pStructure + wStart, pBuff->GetData(), pBuff->GetSize());

				wStart += pBuff->GetSize();

				wRequest -= pBuff->GetSize();

				CipReleaseBuffer(pBuff);
				}
			else {
				CipReleaseBuffer(pBuff);

				delete [] pStructure;

				pStructure = NULL;

				return false;
				}
			}

		Struct.m_pStruct = pStructure;

		Struct.m_uType = Tag.m_uType;

		AddCachedStruct(Struct);

		return true;
		}

	return false;
	}

void CCipClient::FindMemberStrings(CTag const &Tag, CStructType const &Struct, char ** &ppNames)
{
	WORD wMembers = Struct.m_wMembers;

	PBYTE pStructure = Struct.m_pStruct;

	// Tag names have a maximum length of 40 characters.
	const int uChars = 40 + 1;

	ppNames = new char * [ wMembers ];

	for( UINT i = 0; i < wMembers; i++ ) {

		ppNames[i] = new char[ uChars ];
		}
	
	char UDTname[uChars] = {0};

	char c = 0;

	// Each member has an 8 byte type/size entry at the beginning of the
	// structure definition. We want the names, so this calculation
	// skips past the types/sizes to the names.
	char *pName = (char *) pStructure + wMembers * 8;

	for( int j = 0; j < uChars; j++ ) {

		c = *pName++;

		// Pre-defined types end with a NUL character
		if( c == '\0' ) {

			break;
			}

		// User-defined and module-defined type
		// names end with a semicolon
		if( c == ';' ) {

			// These types contain extraneous data
			// after the semicolon, so we will advance the
			// pointer until we hit another NUL character
			while( *pName++ )
				;

			break;
			}

		UDTname[j] = c;
		}

	for( int n = 0; n < wMembers; n++ ) {

		for( int m = 0; m < uChars; m++ ) {

			c = *pName++;

			ppNames[n][m] = c;

			if( c == '\0' ) {

				break;
				}
			}
		}
	}

bool CCipClient::GetStructSizes(CTag const &Tag, WORD &wSize, WORD &wMembers)
{
	CBuffer *pBuff = CipAllocBuffer();

	CCipPath Path(true);

	UINT uInst = Tag.m_uType & 0x0FFF;

	Path.AddObject(cipStructuredType, uInst);

	UINT Status = 0;

	if( m_pConnect->Exchange(pBuff, cipGetAttrAll, Path, Status) ) {

		wMembers = PU2(pBuff->GetData())[1];

		wSize = PU2(pBuff->GetData())[3];

		CipRo(wMembers);

		CipRo(wSize);

		CipReleaseBuffer(pBuff);

		return true;
		}

	CipReleaseBuffer(pBuff);

	return false;
	}

void CCipClient::AddCachedStruct(CStructType const &Struct)
{
	if( !m_pCached ) {

		// m_uStructTags is a good starting point for the number
		// of structures to cache.
		m_uCacheAlloc = m_uStructTags;

		m_pCached = new CStructType[ m_uCacheAlloc ];

		memset(m_pCached, 0, m_uCacheAlloc * sizeof(CStructType));
		}

	if( m_uCache == m_uCacheAlloc ) {

		UINT uOldSize = m_uCacheAlloc;

		m_uCacheAlloc <<= 1;

		CStructType *pCache = new CStructType[ m_uCacheAlloc ];

		memset(pCache, 0, m_uCacheAlloc * sizeof(CStructType));

		for( UINT n = 0; n < uOldSize; n++ ) {

			pCache[n] = m_pCached[n];
			}

		delete [] m_pCached;

		m_pCached = pCache;
		}

	UINT u;

	for( u = 0; u < m_uCache; u++ ) {

		if( Struct.m_uType == m_pCached[u].m_uType ) {

			// We found a cached version already.
			return;
			}

		if( !m_pCached[u].m_pStruct ) {

			// We reached an empty slot.
			break;
			}
		}

	m_pCached[m_uCache++] = Struct;
	}

bool CCipClient::GetCachedStruct(UINT uType, CStructType &Struct)
{
	if( m_pCached ) {

		UINT u;

		for( u = 0; u < m_uCache; u++ ) {

			if( uType == m_pCached[u].m_uType ) {

				Struct = m_pCached[u];

				return true;
				}
			}
		}

	return false;
	}


void CCipClient::ClearStructCache(void)
{
	if( m_pCached ) {

		for( UINT u = 0; u < m_uCache; u++ ) {

			if( m_pCached[u].m_pStruct ) {
		
				delete [] m_pCached[u].m_pStruct;

				m_pCached[u].m_pStruct = NULL;
				}
			}

		delete [] m_pCached;

		m_pCached = NULL;

		m_uCache  = 0;
		}
	}

UINT CCipClient::GetAtomicSize(UINT uType)
{
	UINT uAtom = uType & 0x0FFF;

	switch( uAtom ) {

		case 0xC1:
		case 0xC2:
			return 1;

		case 0xC3:
			return 2;

		case 0xC4:
		case 0xCA:
		case 0xD3:
			return 4;
		}

	return 0;
	}

void CCipClient::SetBitOffset(CTag &Tag)
{
	if( !(Tag.m_uType & 0x8000) ) {

		if( Tag.m_wExtra == 0 ) {

			WORD wExtra = (Tag.m_uType & 0xFF00) >> 8;

			Tag.m_wExtra = wExtra;

			Tag.m_uType &= 0xFF;
			}
		}
	}

// End of File
