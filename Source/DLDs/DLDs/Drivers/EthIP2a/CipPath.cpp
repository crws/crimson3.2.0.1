
#include "Intern.hpp"

#include "CipPath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 2012 Red Lion Controls
//
// All Rights Reserved
//

#include "EthernetIp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Path
//

// Constructor

CCipPath::CCipPath(void)
{
	m_fPad  = true;

	m_uData = 0;
	}

CCipPath::CCipPath(bool fPad)
{
	m_fPad  = fPad;

	m_uData = 0;
	}

// Destructor

CCipPath::~CCipPath(void)
{
	}

// Attributes

bool CCipPath::IsEmpty(void) const
{
	return m_uData == 0;
	}

// Operations

void CCipPath::SetPad(bool fPad)
{
	m_fPad = fPad;
	}

void CCipPath::AddPort(UINT Port, UINT Addr)
{
	AddByte(Port);
	
	AddByte(Addr);
	}

void CCipPath::AddPort(UINT Port, PCSTR pAddr)
{
	BYTE b = (1 << 4) | Port;

	AddByte(b);

	CopyString(pAddr);
	}

void CCipPath::AddObject(UINT Class, UINT Instance)
{
	AddLogical(0, Class);

	AddLogical(1, Instance);
	}

bool CCipPath::AddSymbol(PCSTR pName)
{
	UINT uLen = strlen(pName);

	if( uLen < 32 ) {

		AddByte((3 << 5) | uLen);

		for( UINT n = 0; n < uLen; n++ ) {

			AddByte(pName[n]);
			}

		return true;
		}

	return false;
	}

void CCipPath::AddClass(UINT Data)
{
	AddLogical(0, Data);
	}

void CCipPath::AddInstance(UINT Data)
{
	AddLogical(1, Data);
	}

void CCipPath::AddInstance16(UINT Data)
{
	AddLogical(1, 1, Data);
	}

void CCipPath::AddMember(UINT Data)
{
	AddLogical(2, Data);
	}

void CCipPath::AddConnectionPoint(UINT Data)
{
	AddLogical(3, Data);
	}

void CCipPath::AddAttribute(UINT Data)
{
	AddLogical(4, Data);
	}

void CCipPath::AddService(UINT Data)
{
	AddLogical(6, 0, Data);
	}

void CCipPath::AddLogical(BYTE Type, UINT Data)
{
	if( !HIWORD(Data) ) {

		if( !HIBYTE(Data) ) {

			AddLogical(Type, 0, Data);
			}
		else
			AddLogical(Type, 1, Data);
		}
	else
		AddLogical(Type, 2, Data);
	}

void CCipPath::AddLogical(BYTE Type, BYTE Format, UINT Data)
{
	AddByte( (1      << 5) |
		 (Type   << 2) |
		 (Format << 0)
		 );

	if( Format == 0 ) {

		AddByte(BYTE(Data));
		}
	else {
		if( m_fPad ) {

			AddByte(0);
			}

		if( Format == 1 ) {

			AddByte(LOBYTE(LOWORD(Data)));
			AddByte(HIBYTE(LOWORD(Data)));
			}
		else {
			AddByte(LOBYTE(LOWORD(Data)));
			AddByte(HIBYTE(LOWORD(Data)));
			AddByte(LOBYTE(HIWORD(Data)));
			AddByte(HIBYTE(HIWORD(Data)));
			}
		}
	}

void CCipPath::AddString(PCSTR pData)
{
	AddByte(0x91);

	CopyString(pData);
	}

void CCipPath::AddByte(BYTE Data)
{
	m_bData[m_uData++] = Data;
	}

// Transcription

void CCipPath::Prefix(CBuffer *pBuff, bool fAddSize) const
{
	PBYTE pData = pBuff->AddHead(m_uData);

	memcpy(pData, m_bData, m_uData);

	if( fAddSize ) {

		PBYTE pSize = pBuff->AddHead(1);

		*pSize      = m_uData / 2;
		}
	}

void CCipPath::Append(CBuffer *pBuff, bool fAddSize) const
{
	if( fAddSize ) {

		PBYTE pSize = pBuff->AddTail(1);

		*pSize      = m_uData / 2;
		}

	PBYTE pData = pBuff->AddTail(m_uData);

	memcpy(pData, m_bData, m_uData);
	}

void CCipPath::Clear(void)
{
	m_uData = 0;
	}

PBYTE CCipPath::GetData(void)
{
	return m_bData;
	}

UINT CCipPath::GetSize(void)
{
	return m_uData;
	}

// Helpers

void CCipPath::CopyString(PCSTR pData)
{
	UINT uSize = strlen(pData);

	AddByte(uSize);

	uSize = uSize + (uSize & 1);

	memcpy(m_bData + m_uData, pData, uSize);

	m_uData += uSize;
	}

// End of File
