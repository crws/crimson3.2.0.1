#include "Intern.hpp"

#ifndef INCLUDE_WatchList_HPP
#define INCLUDE_WatchList_HPP

#include "CipClient.hpp"

#include "TagInfo.hpp"


class CTagInfo;

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Native Tag Watch List
//

class CWatchList : public CCipCommon
{
	public:
		// Constructors
		CWatchList(void);
		CWatchList(CCipClient *pClient);

		// Destructor
		~CWatchList(void);

		BOOL CreateInstance(void);

		// Tag management
		void AddTag(CTagInfo *pTag);
		void RemoveTag(CTagInfo *pTag);
		void Touch(void);
		void ClearTouch(void);
		BOOL ConfigureAllTags(void);

		// Data Management
		BOOL IsTooLarge(UINT TagSize);
		BOOL HasData(void);
		PBYTE GetData(void);
		BOOL IsSparse(void);
		BOOL IsEmpty(void);
		void SetRebuild(void);
		BOOL IsDirty(void);
		void Rebuild(void);
		BOOL UpdateData(void);
		void Delete(void);
		
		CTagInfo   * m_pHead;
		CTagInfo   * m_pTail;
		
		CWatchList * m_pNext;
		CWatchList * m_pPrev;

	protected:
		UINT	     m_uInst;
		BOOL	     m_fDirty;
		BOOL	     m_fRebuild;
		CCipClient * m_pClient;
		UINT	     m_uCount;
		UINT	     m_uTouch;
		UINT	     m_uSize;
		BOOL	     m_fData;
		PBYTE	     m_pData;
		UINT	     m_uThresh;
};

// End of File

#endif
