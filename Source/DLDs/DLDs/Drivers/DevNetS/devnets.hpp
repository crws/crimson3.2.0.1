
//////////////////////////////////////////////////////////////////////////
//
// DevicetNet Slave
//

enum {
	assyDataSend	= 100,
	assyDataRecv	= 101,
	assyBitSend	= 102,
	assyBitRecv	= 103,
	assyPollSend	= 104,
	assyPollRecv	= 105,
	assyMultiSend	= 106,
	assyMultiRecv	= 107,
	};

//////////////////////////////////////////////////////////////////////////
//
// DevicetNet Slave
//

class CDeviceNetSlave : public CSlaveDriver
{
	public:
		// Constructor
		CDeviceNetSlave(void);

		// Destructor
		~CDeviceNetSlave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersistConnection);

		// Entry Points
		DEFMETH(void) Service(void);

	protected:
		// Data
		IDeviceNetHelper * m_pDevNet;
		BYTE		   m_bMac;
		BOOL		   m_fBit;
		WORD               m_wBitRecv;
		WORD		   m_wBitSend;
		BOOL		   m_fPoll;
		WORD		   m_wPollRecv;
		WORD		   m_wPollSend;
		BOOL		   m_fData;
		WORD		   m_wDataRecv;
		WORD		   m_wDataSend;
		BOOL		   m_fIntel;
		PBYTE              m_pNetBuf;
		PDWORD	           m_pHostBuf;

		// Implementation
		void CheckReads(void);
		void CheckBitReads(void);
		void CheckPollReads(void);
		void CheckDataReads(void);
		void CheckWrites(void);
		void CheckBitWrites(void);
		void CheckPollWrites(void);
		void CheckDataWrites(void);

		UINT CheckType(UINT uTable);
		BOOL DoRead(UINT uTable, UINT uCount);
		BOOL DoWordRead(UINT uTable, UINT uCount);
		BOOL DoLongRead(UINT uTable, UINT uCount);
		BOOL DoWrite(UINT uTable, UINT uCount);
		BOOL DoWordWrite(UINT uTable, UINT uCount);
		BOOL DoLongWrite(UINT uTable, UINT uCount);

		// Buffer
		void AllocBuffers(void);
		void FreeBuffers(void);
	};

// End of File
