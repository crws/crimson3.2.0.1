
#include "intern.hpp"

#include "devnets.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Slave Driver
//

// Instantiator

INSTANTIATE(CDeviceNetSlave);

// Constructor

CDeviceNetSlave::CDeviceNetSlave(void)
{
	m_Ident    = DRIVER_ID;

	m_pDevNet  = NULL;

	m_pHostBuf = NULL;

	m_pNetBuf  = NULL;
	}

// Destructor

CDeviceNetSlave::~CDeviceNetSlave(void)
{
	}

// Configuration

void MCALL CDeviceNetSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bMac      = GetByte(pData);

		m_fBit      = GetByte(pData);

		m_wBitRecv  = GetByte(pData);

		m_wBitSend  = GetByte(pData);

		m_fPoll     = GetByte(pData);

		m_wPollRecv = GetWord(pData);

		m_wPollSend = GetWord(pData);

		m_fData     = GetByte(pData);

		m_wDataRecv = GetWord(pData);

		m_wDataSend = GetWord(pData);

		m_fIntel    = GetByte(pData);
		}
	}

// Management

void MCALL CDeviceNetSlave::Attach(IPortObject *pPort)
{
	MoreHelp(IDH_DEVNET, (void **) &m_pDevNet);

	m_pDevNet->Attach(pPort);
	}

void MCALL CDeviceNetSlave::Detach(void)
{
	m_pDevNet->Close();
	
	m_pDevNet->Detach();

	m_pDevNet->Release();
	
	FreeBuffers();
	}

// Device

CCODE MCALL CDeviceNetSlave::DeviceOpen(IDevice *pDevice)
{
	CCommsDriver::DeviceOpen(pDevice);

	if( m_pDevNet ) {

		m_pDevNet->SetMac(m_bMac);

		if( m_fBit && !m_pDevNet->EnableBitStrobe(m_wBitRecv, m_wBitSend) ) {

			return CCODE_ERROR;
			}

		if( m_fPoll && !m_pDevNet->EnablePolled(m_wPollRecv, m_wPollSend) ) {

			return CCODE_ERROR;
			}

		if( m_fData && !m_pDevNet->EnableData(m_wDataRecv, m_wDataSend) ) {

			return CCODE_ERROR;
			}

		if( m_pDevNet->Open() ) {

			AllocBuffers();

			return CCODE_SUCCESS;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CDeviceNetSlave::DeviceClose(BOOL fPersist)
{
	FreeBuffers();
	
	return CSlaveDriver::DeviceClose(fPersist);
	}

void MCALL CDeviceNetSlave::Service(void)
{
	for(;;) {

		if( m_pDevNet && m_pDevNet->IsOnLine() ) {

			CheckWrites();

			CheckReads();
			}

		ForceSleep(50);
		}
	}

// Implementation

void CDeviceNetSlave::CheckReads(void)
{
	CheckBitReads();

	CheckPollReads();

	CheckDataReads();
	}

void CDeviceNetSlave::CheckWrites(void)
{
	CheckBitWrites();

	CheckPollWrites();

	CheckDataWrites();
	}

void CDeviceNetSlave::CheckBitWrites(void)
{
	if( m_fBit && m_wBitRecv ) {
	
		UINT uCount = m_pDevNet->Read(assyBitRecv, m_pNetBuf, m_wBitRecv);

		if( COMMS_SUCCESS(uCount) && uCount ) {

			CAddress Addr;

			Addr.a.m_Table  = assyBitRecv;

			Addr.a.m_Type   = addrBitAsBit;
				
			Addr.a.m_Offset = 0;

			Addr.a.m_Extra  = 0;

			m_pHostBuf[0] = m_pNetBuf[0];

			CCODE cc = Write(Addr, m_pHostBuf, 1);

			if( COMMS_ERROR(cc) ) {

				m_wBitRecv = 0;
				}
			}
		}
	}

void CDeviceNetSlave::CheckBitReads(void)
{
	if( m_fBit && m_wBitSend ) {

		if( !DoRead(assyBitSend, m_wBitSend) ) {

			m_wBitSend = 0;
			}
		}
	}

void CDeviceNetSlave::CheckPollWrites(void)
{
	if( m_fPoll && m_wPollRecv ) {

		if( !DoWrite(assyPollRecv, m_wPollRecv) ) {

			m_wPollRecv = 0;
			}
		}
	}

void CDeviceNetSlave::CheckPollReads(void)
{
	if( m_fPoll && m_wPollSend ) {

		if( !DoRead(assyPollSend, m_wPollSend) ) {

			m_wPollSend = 0;
			}
		}
	}

void CDeviceNetSlave::CheckDataWrites(void)
{
	if( m_fData && m_wDataRecv ) {

		if( !DoWrite(assyDataRecv, m_wDataRecv) ) {

			m_wDataRecv = 0;
			}
		}
	}

void CDeviceNetSlave::CheckDataReads(void)
{
	if( m_fData && m_wDataSend ) {

		if( !DoRead(assyDataSend, m_wDataSend) ) {

			m_wDataSend = 0;
			}
		}
	}

UINT CDeviceNetSlave::CheckType(UINT uTable)
{
	for( UINT uPass = 0; uPass < 2; uPass ++ ) {

		CAddress Addr;

		Addr.a.m_Table  = uTable;

		Addr.a.m_Type   = uPass ? addrWordAsLong : addrWordAsWord;
			
		Addr.a.m_Offset = 0;

		Addr.a.m_Extra  = 0;

		if( COMMS_SUCCESS(Read(Addr, NULL, 1)) ) {

			return Addr.a.m_Type;
			}
		}

	return addrReserved;
	}

BOOL CDeviceNetSlave::DoRead(UINT uTable, UINT uCount)
{
	UINT uType = CheckType(uTable);

	switch( uType ) {
	
		case addrWordAsWord:
			
			DoWordRead(uTable, uCount);

			return TRUE;

		case addrWordAsLong:

			DoLongRead(uTable, uCount);

			return TRUE;
		}
	
	return FALSE;
	}

BOOL CDeviceNetSlave::DoWordRead(UINT uTable, UINT uCount)
{
	CAddress Addr;

	Addr.a.m_Table  = uTable;

	Addr.a.m_Type   = addrWordAsWord;
		
	Addr.a.m_Offset = 0;

	Addr.a.m_Extra  = 0;

	UINT  cb = uCount / sizeof(WORD);
		
	CCODE cc = Read(Addr, m_pHostBuf, cb);

	if( COMMS_SUCCESS(cc) ) {

		if( m_fIntel ) {
		
			CIntelDataPacker Data(m_pHostBuf, cb, TRUE);
					
			Data.Pack(PWORD(m_pNetBuf));
			}
		else {
			CMotorDataPacker Data(m_pHostBuf, cb, TRUE);
					
			Data.Pack(PWORD(m_pNetBuf));
			}

		m_pDevNet->Write(uTable, m_pNetBuf, uCount);
		
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CDeviceNetSlave::DoLongRead(UINT uTable, UINT uCount)
{
	CAddress Addr;

	Addr.a.m_Table  = uTable;

	Addr.a.m_Type   = addrWordAsLong;
		
	Addr.a.m_Offset = 0;

	Addr.a.m_Extra  = 0;

	UINT  cb = uCount / sizeof(DWORD);
		
	CCODE cc = Read(Addr, m_pHostBuf, cb);

	if( COMMS_SUCCESS(cc) ) {

		if( m_fIntel ) {
		
			CIntelDataPacker Data(m_pHostBuf, cb, TRUE);
					
			Data.Pack(PDWORD(m_pNetBuf));
			}
		else {
			CMotorDataPacker Data(m_pHostBuf, cb, TRUE);
					
			Data.Pack(PDWORD(m_pNetBuf));
			}

		m_pDevNet->Write(uTable, m_pNetBuf, uCount);
		
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CDeviceNetSlave::DoWrite(UINT uTable, UINT uCount)
{
	UINT uType = CheckType(uTable);

	switch( uType ) {
	
		case addrWordAsWord:
			
			DoWordWrite(uTable, uCount);

			return TRUE;

		case addrWordAsLong:

			DoLongWrite(uTable, uCount);

			return TRUE;
		}
	
	return FALSE;
	}

BOOL CDeviceNetSlave::DoWordWrite(UINT uTable, UINT uCount)
{
	uCount = m_pDevNet->Read(uTable, m_pNetBuf, uCount);

	if( COMMS_SUCCESS(uCount) && uCount ) {

		CAddress Addr;

		Addr.a.m_Table  = uTable;

		Addr.a.m_Type   = addrWordAsWord;
			
		Addr.a.m_Offset = 0;

		Addr.a.m_Extra  = 0;

		uCount /= sizeof(WORD);

		if( m_fIntel ) {

			CIntelDataPacker Data(m_pHostBuf, uCount, TRUE);

			Data.Unpack(PWORD(m_pNetBuf));
			}
		else {
			CMotorDataPacker Data(m_pHostBuf, uCount, TRUE);

			Data.Unpack(PWORD(m_pNetBuf));
			}

		CCODE cc = Write(Addr, m_pHostBuf, uCount * sizeof(WORD));

		return COMMS_SUCCESS(cc);
		}
	
	return TRUE;
	}

BOOL CDeviceNetSlave::DoLongWrite(UINT uTable, UINT uCount)
{
	uCount = m_pDevNet->Read(uTable, m_pNetBuf, uCount);

	if( COMMS_SUCCESS(uCount) && uCount ) {

		CAddress Addr;

		Addr.a.m_Table  = uTable;

		Addr.a.m_Type   = addrWordAsLong;
			
		Addr.a.m_Offset = 0;

		Addr.a.m_Extra  = 0;

		uCount /= sizeof(DWORD);

		if( m_fIntel ) {

			CIntelDataPacker Data(m_pHostBuf, uCount, TRUE);

			Data.Unpack(PDWORD(m_pNetBuf));
			}
		else {
			CMotorDataPacker Data(m_pHostBuf, uCount, TRUE);

			Data.Unpack(PDWORD(m_pNetBuf));
			}

		CCODE cc = Write(Addr, m_pHostBuf, uCount * sizeof(DWORD));

		return COMMS_SUCCESS(cc);
		}
	
	return TRUE;
	}

// Buffer

void CDeviceNetSlave::AllocBuffers(void)
{
	UINT uSize = 0;

	if( m_fBit ) {

		MakeMax(uSize, m_wBitSend);

		MakeMax(uSize, m_wBitRecv);
		}

	if( m_fPoll ) {

		MakeMax(uSize, m_wPollSend);

		MakeMax(uSize, m_wPollRecv);
		}
		
	if( m_fData ) {

		MakeMax(uSize, m_wDataSend);

		MakeMax(uSize, m_wDataRecv);
		}
		
	if( uSize ) {

		m_pNetBuf  = new BYTE [ uSize ];

		m_pHostBuf = new DWORD[ uSize ]; 
		}
	}

void CDeviceNetSlave::FreeBuffers(void)
{
	if( m_pHostBuf ) {

		delete [] m_pHostBuf;
		
		m_pHostBuf = NULL;
		}

	if( m_pNetBuf ) {

		delete [] m_pNetBuf;
		
		m_pNetBuf = NULL;
		}
	}

// End of File
