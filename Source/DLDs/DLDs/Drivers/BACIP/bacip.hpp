//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Base Class
//

#include "bacbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Driver
//

class CBACNetIP : public CBACNetBase
{
	public:
		// Constructor
		CBACNetIP(void);

		// Destructor
		~CBACNetIP(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping(void);

	protected:
		// MAC Address
		struct CMacAddr
		{
			IPADDR m_IP;
			WORD   m_Port;
		};

		// Device Context
		struct CContext : CBACNetBase::CBaseCtx
		{
			UINT     m_uTime1;
			CMacAddr m_Mac;
			BOOL     m_fSubNet;
			UINT     m_uRoute;
		};

		// Data Members
		IExtraHelper     * m_pExtra;
		CContext	 * m_pCtx;
		ISocket		 * m_pSock;
		CMacAddr	   m_RxMac;

		// Implemetation
		BOOL GetBroadcast(DWORD &Broadcast);

		// Transport Hooks
		BOOL SendFrame(BOOL fThis, BOOL fReply);
		BOOL RecvFrame(void);

		// Transport Header
		void AddTransportHeader(BOOL fThis);
		BOOL StripTransportHeader(void);
};

// End of File
