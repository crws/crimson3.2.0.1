
#include "intern.hpp"

#include "rawser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw Serial Port Driver
//

// Instantiator

INSTANTIATE(CRawSerialDriver);

// Constructor

CRawSerialDriver::CRawSerialDriver(void)
{
	m_Ident = DRIVER_ID;

	m_pPort = NULL;
	}

// Destructor

CRawSerialDriver::~CRawSerialDriver(void)
{
	}

// Management

void MCALL CRawSerialDriver::Attach(IPortObject *pPort)
{
	if( pPort ) {

		m_pPort = pPort;

		m_pData = MakeDoubleDataHandler();

		m_pData->SetRxSize(m_wBuffer);

		m_pData->SetTxSize(m_wBuffer);

		pPort->Bind(m_pData);
		}
	}

// Operations

void MCALL CRawSerialDriver::SetRTS(BOOL fState)
{
	if( m_pPort ) m_pPort->SetOutput(0, fState);
	}

BOOL MCALL CRawSerialDriver::GetCTS(void)
{
	return m_pPort ? m_pPort->GetInput(0) : FALSE;
	}

// Configuration

void MCALL CRawSerialDriver::Load(LPCBYTE pData)
{
	SkipUpdate(pData);

	m_bMode = GetByte(pData);

	m_wBuffer = GetWord(pData);
	}

void MCALL CRawSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Config.m_uFlags |= flagNoBatchSend;
	
	Make485(Config, m_bMode == 0);
	}

// End of File
