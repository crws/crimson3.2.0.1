
//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Passive Driver
//

class CRawUDPDriver : public CRawPortDriver
{
	public:
		// Constructor
		CRawUDPDriver(void);

		// Destructor
		~CRawUDPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Entry Points
		DEFMETH(void) Disconnect(void);
		DEFMETH(UINT) Read (UINT uTime);
		DEFMETH(UINT) Write(BYTE bData, UINT uTime);
		DEFMETH(UINT) Write(PCBYTE pData, UINT uCount, UINT uTime);

	protected:
		// Data Members
		UINT	  m_uPort;
		ISocket * m_pSock;
		UINT      m_uTxState;
		BYTE      m_bTxDest[6];
		UINT      m_uTxCount;
		CBuffer * m_pTxBuff;
		UINT	  m_uRxState;
		CBuffer * m_pRxBuff;

		// Implementation
		BOOL CheckSocket(void);
	};

// End of File
