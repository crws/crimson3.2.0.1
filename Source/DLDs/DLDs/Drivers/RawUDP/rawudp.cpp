
#include "intern.hpp"

#include "rawudp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Passive Port Driver
//

// Instantiator

INSTANTIATE(CRawUDPDriver);

// Constructor

CRawUDPDriver::CRawUDPDriver(void)
{
	m_Ident    = DRIVER_ID;
	
	m_pSock    = NULL;

	m_uPort    = 1234;

	m_uTxState = 0;

	m_uRxState = 0;

	m_pTxBuff  = NULL;

	m_pRxBuff  = NULL;
	}

// Destructor

CRawUDPDriver::~CRawUDPDriver(void)
{
	}

// Configuration

void MCALL CRawUDPDriver::Load(LPCBYTE pData)
{
	SkipUpdate(pData);

	m_uPort = GetWord(pData);
	}

// Management

void MCALL CRawUDPDriver::Attach(IPortObject *pPort)
{
	if( (m_pSock = CreateSocket(IP_UDP)) ) {

		m_pSock->SetOption(OPT_RECV_QUEUE, 12);
		
		m_pSock->SetOption(OPT_ADDRESS,     1);

		m_pSock->Listen   (m_uPort);
		}
	}

void MCALL CRawUDPDriver::Detach(void)
{
	if( m_pSock ) {

		m_pSock->Release();

		m_pSock = NULL;
		}

	if( m_pTxBuff ) {

		m_pTxBuff->Release();

		m_pTxBuff = NULL;
		}

	if( m_pRxBuff ) {

		m_pRxBuff->Release();

		m_pRxBuff = NULL;
		}

	m_uTxState = 0;

	m_uRxState = 0;
	}

// Entry Points

void MCALL CRawUDPDriver::Disconnect(void)
{
	}

UINT MCALL CRawUDPDriver::Read(UINT uTime)
{
	UINT uData = NOTHING;

	switch( m_uRxState ) {

		case 0:
			if( m_pSock ) {

				if( m_pSock->Recv(m_pRxBuff) == S_OK ) {

					uData      = 'R';

					m_uRxState = 1;
					}
				}
			break;

		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:

			uData = *(m_pRxBuff->StripHead(1));

			m_uRxState++;

			break;

		case 7:
			uData      = HIBYTE(m_pRxBuff->GetSize());

			m_uRxState = 8;

			break;

		case 8:
			uData = LOBYTE(m_pRxBuff->GetSize());

			if( !m_pRxBuff->GetSize() ) {

				m_pRxBuff->Release();

				m_pRxBuff  = NULL;

				m_uRxState = 0;
				}
			else
				m_uRxState = 9;

			break;

		case 9:
			uData = *(m_pRxBuff->StripHead(1));

			if( !m_pRxBuff->GetSize() ) {

				m_pRxBuff->Release();

				m_pRxBuff  = NULL;

				m_uRxState = 0;
				}
			break;
		}

	return uData;
	}

UINT MCALL CRawUDPDriver::Write(BYTE bData, UINT uTime)
{
	switch( m_uTxState ) {

		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			m_bTxDest[m_uTxState] = bData;

			m_uTxState++;

			break;

		case 6:
			m_uTxCount = bData;

			m_uTxState++;

			break;

		case 7:
			m_uTxCount *= 256;

			m_uTxCount += bData;

			if( m_uTxCount ) {

				m_pTxBuff  = CreateBuffer(m_uTxCount, TRUE);

				m_uTxState = 8;
				}
			else
				m_uTxState = 0;

			break;

		default:
			if( m_pTxBuff ) {

				*(m_pTxBuff->AddTail(1)) = bData;
				}

			if( m_uTxState == 7 + m_uTxCount ) {

				if( m_pTxBuff ) {

					if( m_pSock ) {

						UINT  uSize = 6;

						PBYTE pData = m_pTxBuff->AddHead(uSize);

						memcpy(pData, m_bTxDest, uSize);

						if( m_pSock->Send(m_pTxBuff) == S_OK ) {

							m_pTxBuff = NULL;
							}
						}

					if( m_pTxBuff ) {

						m_pTxBuff->Release();
						}
					}

				m_uTxState = 0;
				}
			else
				m_uTxState++;

			break;
		}

	return 1;
	}

UINT MCALL CRawUDPDriver::Write(PCBYTE pData, UINT uCount, UINT uTime)
{
	for( UINT n = 0; n < uCount; n++ ) {

		Write(*pData++, uTime);
		}

	return n;
	}

// End of File
