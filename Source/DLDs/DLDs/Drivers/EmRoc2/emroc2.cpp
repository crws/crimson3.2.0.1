#include "intern.hpp"

#include "emroc2.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Emerson ROC 2 Master Serial Driver
//

// Instantiator

INSTANTIATE(CEmRoc2MasterSerialDriver);

// Constructor

CEmRoc2MasterSerialDriver::CEmRoc2MasterSerialDriver(void)
{
	m_Ident         = DRIVER_ID; 

	m_bGroup	= 2;

	m_bUnit		= 1;

	m_TxPtr		= 0;

	m_RxPtr		= 0;

	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx));
	}

// Destructor

CEmRoc2MasterSerialDriver::~CEmRoc2MasterSerialDriver(void)
{
	}

// Configuration

void MCALL CEmRoc2MasterSerialDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bGroup   = GetByte(pData);

		m_bUnit    = GetByte(pData);
		}
	}

	
void MCALL CEmRoc2MasterSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CEmRoc2MasterSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEmRoc2MasterSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmRoc2MasterSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bGroup  = GetByte(pData);
			
			m_pCtx->m_bUnit	  = GetByte(pData);

			m_pCtx->m_Device  = 1 << GetByte(pData);

			m_pCtx->m_bLast   = 0xFF;

			m_pCtx->m_uLast   = 0;
			
			m_pCtx->m_uTime   = 0;

			m_pCtx->m_uPoll   = ToTicks(GetWord(pData));

			m_pCtx->m_Pass    = GetWord(pData);

			m_pCtx->m_Use     = GetByte(pData);

			m_pCtx->m_Acc	  = GetByte(pData);

			m_pCtx->m_OpID    = GetString(pData);

			m_pCtx->m_Logon   = FALSE;
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmRoc2MasterSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmRoc2MasterSerialDriver::Ping(void)
{	
	DWORD    Data[1];

	CAddress Addr;

	if( m_pCtx->m_Device == devFlo103104 ) {

		// TLP 15,0,0  - SYS 1, ROCADDR - ROC Drop

		Addr.a.m_Table  = 183;
		
		Addr.a.m_Offset = WORD(0x3 << 14);

		Addr.a.m_Extra  = 3;
		
		Addr.a.m_Type   = addrByteAsByte;
		}

	if( m_pCtx->m_Device == devRoc800 ) {

		// TLP 91,0,0  - SYS 1, ROCADDR - ROC Drop

		Addr.a.m_Table  = 183;
		
		Addr.a.m_Offset = 0;

		Addr.a.m_Extra  = 3;
		
		Addr.a.m_Type   = addrByteAsByte;
		}

	if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

		return CCODE_SUCCESS;
		} 

	if( !m_pCtx->m_Logon ) {

		return LogOn();
		}

	return CCODE_ERROR; 
	}

CCODE MCALL CEmRoc2MasterSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return DoOpcode180(Addr, pData, uCount);
	}

CCODE MCALL CEmRoc2MasterSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return DoOpcode181(Addr, pData, uCount);
	}

// Handlers

CCODE CEmRoc2MasterSerialDriver::LogOn(void)
{
	Begin();

	AddByte(17);

	AddByte(m_pCtx->m_Use ? 6 : 5);

	AddText(m_pCtx->m_OpID, 3);

	AddWord(m_pCtx->m_Pass);

	if( m_pCtx->m_Use ) {

		AddByte(m_pCtx->m_Acc);
		}

	End();

	if( Transact() ) {

		m_pCtx->m_Logon = TRUE;

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc2MasterSerialDriver::DoOpcode180(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsString(Addr.a.m_Table) ) {

		return DoOpcode180String(Addr, pData, uCount);
		}

	if( IsTime(Addr) ) {

		return DoOpcode7(Addr, pData, uCount);
		}

	MakeMin(uCount, 1);

	Begin();

	AddByte(180);			// Opcode

	AddByte(1 + 3);			// Data Length
		
	AddByte(uCount);		// Number of Params

	AddByte(GetPointType(Addr));	// Point Type

	AddByte(GetLogical(Addr));	// Logical

	AddByte(GetParameter(Addr));	// Parameter

	End();

	if( Transact() ) {

		MakeMin(uCount, m_bRx[6]);

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:
			case addrByteAsByte:	
				
				return GetBytes(pData, uCount);

			case addrWordAsWord:	
				
				return GetWords(pData, uCount);

			case addrLongAsLong:	
				
				if( IsTLP(Addr) ) {

					return GetTLPs(pData, uCount);
					}

				// Fall thru

			case addrRealAsReal:	

				if( IsDouble(Addr) ) {

					return GetDoubles(pData, uCount);
					}
				
				return GetLongs(pData, uCount);
			
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc2MasterSerialDriver::DoOpcode180String(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 8);

	Begin();

	AddByte(180);				// Opcode

	AddByte(1 + 3);				// Data Length
		
	AddByte(1);				// Number of Params

	AddByte(GetStringPointType(Addr));	// Point Type

	AddByte(GetStringLogical(Addr));	// Logical

	AddByte(GetStringParameter(Addr));	// Parameter

	End();

	if( Transact() ) {

		GetStrings(pData, uCount);

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc2MasterSerialDriver::DoOpcode7(AREF Addr, PDWORD pData, UINT uCount)
{
	Begin();					// Read Current Time

	AddByte(7);					// OpCode

	AddByte(0);					// Data Length

	End();

	if( Transact() ) {

		pData[0] = 0;

		pData[0] += Time(m_bRx[8], m_bRx[7], m_bRx[6]);

		if( m_pCtx->m_Device == devRoc800 ) {

			WORD Year = MAKEWORD(m_bRx[11], m_bRx[12]);

			pData[0] += Date(BYTE(Year - 2000), m_bRx[10], m_bRx[9]);
			}
		else {
			pData[0] += Date(m_bRx[11], m_bRx[10], m_bRx[9]);
			}
				
		return 1;
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc2MasterSerialDriver::DoOpcode181(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsString(Addr.a.m_Table) ) {

		return DoOpcode181String(Addr, pData, uCount);
		}

	if( IsTime(Addr) ) {

		return DoOpcode8(Addr, pData, uCount);
		}

	MakeMin(uCount, 1);

	UINT uBytes = 1 * uCount;

	if( IsWord(Addr.a.m_Type) ) {

		uBytes = sizeof(WORD) * uCount;
		}
	
	else if( IsLong(Addr.a.m_Type) ) {

		if( IsTLP(Addr) ) {

			uBytes = 3 * uCount;
			}
		else {
			uBytes = sizeof(DWORD) * uCount;
			}
		}	
	
	Begin();

	AddByte(181);				// Opcode

	AddByte(1 + 3 + uBytes);		// Data Length
		
	AddByte(uCount);			// Number of Params 

	UINT uPointType = GetPointType(Addr);

	UINT uLogical	= GetLogical(Addr);

	UINT uParameter = GetParameter(Addr);

	for( UINT x = 0; x < uCount; x++ ) {

		AddByte(uPointType);		

		AddByte(uLogical);

		AddByte(uParameter + x);

		AddData(Addr, pData, x);
		}

	End();

	if( Transact() ) {

		MakeMin(uCount, m_bRx[6]);

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc2MasterSerialDriver::DoOpcode181String(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uMark = 8;

	if( Addr.a.m_Offset % uMark ) {

		return uCount;
		}

	MakeMin(uCount, uMark);

	Begin();

	BYTE bPointType = GetStringPointType(Addr);

	BYTE bParameter = GetStringParameter(Addr);

	BYTE bLen	= GetStrLen(bPointType, bParameter);

	AddByte(181);							// Opcode

	AddByte( 1 + 3 + bLen);						// Data Length
		
	AddByte(1);							// Number of Params

	AddByte(bPointType);

	AddByte(GetStringLogical(Addr));

	AddByte(bParameter);

	AddString(pData, bLen, uCount * 4);

	End();

	if( Transact() ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc2MasterSerialDriver::DoOpcode8(AREF Addr, PDWORD pData, UINT uCount)
{
	PDWORD pRead = PDWORD(alloca(uCount));
	
	if( COMMS_SUCCESS(DoOpcode7(Addr, pRead, uCount)) ) {

		Begin();							// Write Current Time

		AddByte(8);							// OpCode

		m_pCtx->m_Device == devRoc800 ? AddByte(7) : AddByte(8);	// Data Len

		UINT uTime = pData[0];

		BYTE bLeap = m_bRx[12];

		BYTE bCurr = m_bRx[13];

		AddByte(GetSec  (uTime) % 60 );
	
		AddByte(GetMin  (uTime) % 60 );
	
		AddByte(GetHour (uTime) % 24 );
	
		AddByte(GetDate (uTime)      );
	
		AddByte(GetMonth(uTime) % 12 );

		if( m_pCtx->m_Device == devRoc800 ) {

			AddWord(GetYear(uTime));
			}
		else {
	
			AddByte(GetYear (uTime) % 100);

			AddByte(bLeap);

			AddByte(bCurr);
			}
		End();

		if( Transact() ) {

			return 1;
			}
		}

	return CCODE_ERROR;
	}

// Implementation

void CEmRoc2MasterSerialDriver::Begin(void)
{
	m_TxPtr = 0;

	m_CRC.Clear();

	AddDest();

	AddHost();
	}

void CEmRoc2MasterSerialDriver::End(void)
{
	AddWord(m_CRC.GetValue());
	}

void CEmRoc2MasterSerialDriver::AddDest(void)
{
	AddByte(m_pCtx->m_bUnit);

	AddByte(m_pCtx->m_bGroup);
	}

void CEmRoc2MasterSerialDriver::AddHost(void)
{
	AddByte(m_bUnit);

	AddByte(m_bGroup);
	}

void CEmRoc2MasterSerialDriver::AddData(AREF Addr, PDWORD pData, UINT uPos)
{
	if( IsLong(Addr.a.m_Type) ) {

		if( IsTLP(Addr) ) {

			AddTLP(pData[uPos]);

			return;
			}

		if( IsDouble(Addr) ) {

			AddDouble(pData[uPos]);

			return;
			}

		AddLong(pData[uPos]);

		return;
		}

	if( IsWord(Addr.a.m_Type) ) {

		AddWord(WORD(pData[uPos]));

		return;
		}

	AddByte(BYTE(pData[uPos]));
	}


void CEmRoc2MasterSerialDriver::AddByte(BYTE bByte)
{
	m_bTx[m_TxPtr] = bByte;

	AddCheck(bByte);

	m_TxPtr++;
       	}

void CEmRoc2MasterSerialDriver::AddWord(WORD wWord)
{
	AddByte(LOBYTE(wWord));

	AddByte(HIBYTE(wWord));
	}

void CEmRoc2MasterSerialDriver::AddLong(DWORD dwWord)
{
	AddWord(LOWORD(dwWord));

	AddWord(HIWORD(dwWord));
	}

void CEmRoc2MasterSerialDriver::AddDouble(DWORD dwWord)
{
	DWORD d[2];

	Convert32To64(dwWord, d);

	AddLong(d[1]);

	AddLong(d[0]);

	d[0] = 0;

	d[1] = 0x408f4000;
	}

void CEmRoc2MasterSerialDriver::AddTLP(DWORD dwWord)
{
	AddByte(LOBYTE(HIWORD(dwWord)));

	AddByte(HIBYTE(LOWORD(dwWord)));

	AddByte(LOBYTE(LOWORD(dwWord)));
	}

void CEmRoc2MasterSerialDriver::AddText(PTXT pText, UINT uCount)
{
	for( UINT u = 0; u < uCount;  u++ ) {

		AddByte(pText[u]);
		}
	}

void CEmRoc2MasterSerialDriver::AddString(PDWORD pData, UINT uLen, UINT uMax)
{	
	BYTE bShift = 0;

	BYTE bByte  = 0;

	for( UINT u = 0, i = 0; u < uLen; u++) {

		switch( u % 4 ) {

			case 0:	bShift = 24;	break;	
			case 1: bShift = 16;	break;
			case 2: bShift = 8;	break;
			case 3: bShift = 0;	break;
			}

		bByte = ((pData[i] >> bShift) & 0xFF);

		if( bByte >= 0x20 && u < uMax ) {

			AddByte(bByte);
			}
		else {
			AddByte(0);
			}

		if( !bShift ) {

			i++;
			}
		}
	}

void CEmRoc2MasterSerialDriver::AddCheck(BYTE bByte)
{
	m_CRC.Add(bByte);
	}


BOOL CEmRoc2MasterSerialDriver::Transact(void)
{
	if( Send() ) {

		if( Recv() ) {

			if( CheckFrame() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CEmRoc2MasterSerialDriver::Send(void)
{
	m_pData->Write(m_bTx, m_TxPtr, FOREVER);

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_TxPtr; u++ ) {

		AfxTrace("%2.2x ", m_bTx[u]);
		}

*/	return TRUE;
	}

BOOL CEmRoc2MasterSerialDriver::Recv(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	UINT uTotal = 0;

	m_CRC.Clear();

	m_RxPtr = 0;

//	AfxTrace("\nRx : ");

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

//		AfxTrace("%2.2x ", uData);

		m_bRx[m_RxPtr] = uData;

		m_CRC.Add(uData);

		if( m_RxPtr == 5 ) {

			uTotal = uData + 8;
			}

		else if( uTotal ) {

			if( m_RxPtr >= (uTotal - 1) ) {

				return CheckCRC();
				}

			if( m_RxPtr >= sizeof(m_bRx) ) {

				// TODO:  Request next block of data !! 
				}
			}

		m_RxPtr++;
		} 

	return FALSE;
	}

BOOL CEmRoc2MasterSerialDriver::CheckFrame(void)
{
	if( m_bRx[0] != m_bUnit	) {

		return FALSE;
		}

	if( m_bRx[1] != m_bGroup ) {

		return FALSE;
		}

	if( m_bRx[2] != m_pCtx->m_bUnit ) {

		return FALSE;
		}

	if( m_bRx[3] != m_pCtx->m_bGroup ) {

		return FALSE;
		}

	if( m_bRx[4] == 0xFF ) {

		// ERROR

		switch( m_bRx[6] ) {

			case 20:
			case 21:
			case 63:

				m_pCtx->m_Logon = FALSE;
				break;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CEmRoc2MasterSerialDriver::CheckCRC(void)
{
	return !m_CRC.GetValue();
	}

UINT CEmRoc2MasterSerialDriver::GetBytes(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = PBYTE(m_bRx + 7 + 3 * (u + 1) + sizeof(BYTE) * u)[0];
		}

	return uCount;
	}

UINT CEmRoc2MasterSerialDriver::GetWords(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		WORD x = PU2(m_bRx + 7 + 3 * (u + 1) + sizeof(WORD) * u)[0];

		pData[u] = LONG(SHORT(IntelToHost(x)));
		}

	return uCount;
	}


UINT CEmRoc2MasterSerialDriver::GetLongs(PDWORD pData, UINT uCount)
{
       for( UINT u = 0; u < uCount; u++ ) {

	       	DWORD x = PU4(m_bRx + 7 + 3 * (u + 1) + sizeof(DWORD) * u)[0];
		
		pData[u] = IntelToHost(x);
		}

	return uCount;
	}

UINT CEmRoc2MasterSerialDriver::GetDoubles(PDWORD pData, UINT uCount)
{
	DWORD d[2];

	for( UINT u = 0; u < uCount; u++ ) {

	       	d[0] = PU4(m_bRx + 7 + 3 * (u + 1) + 8 * u)[0];

		d[1] = PU4(m_bRx + 7 + 3 * (u + 1) + 8 * u + 4)[0];

		d[0] = IntelToHost(d[0]);

		d[1] = IntelToHost(d[1]);
		
		pData[u] = Convert64To32(d);
		}

	return uCount;
	}

UINT CEmRoc2MasterSerialDriver::GetTLPs(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = 0;

		pData[u] |= ((m_bRx[7 + 3 * (u + 1) + 3 * u + 0]) << 16);

		pData[u] |= ((m_bRx[7 + 3 * (u + 1) + 3 * u + 1]) <<  8);

		pData[u] |= ((m_bRx[7 + 3 * (u + 1) + 3 * u + 2]) <<  0);
		}

	return uCount;
	}

UINT CEmRoc2MasterSerialDriver::GetStrings(PDWORD pData, UINT uCount)
{
       UINT uBytes = m_bRx[5];

       if( uBytes > 4 ) {

		uBytes -= 4;

		MakeMin(uBytes, uCount * sizeof(DWORD));

		for( UINT u = 0, i = 0; u < uBytes; u++ ) {

			switch( u % 4 ) {

				case 0: pData[i]  = 0;	pData[i] |= ((m_bRx[7 + 3 + u]) << 24);		break;
				case 1:			pData[i] |= ((m_bRx[7 + 3 + u]) << 16);		break;
				case 2:			pData[i] |= ((m_bRx[7 + 3 + u]) <<  8);		break;
				case 3:			pData[i] |= ((m_bRx[7 + 3 + u]) <<  0);	i++;	break;
				}
			}
		
		return uCount;
		}

	return CCODE_ERROR;
	}

BYTE CEmRoc2MasterSerialDriver::GetPointType(AREF Addr)
{
	BYTE bExt = IsExtended(Addr.a.m_Table) & 0x1;

	BYTE bOffset = 0;

	UINT uPointType = BYTE(((Addr.a.m_Extra << 2) & 0x3C) | ((Addr.a.m_Offset >> 14) & 0x3) | (bExt << 6));

	if( m_pCtx->m_Device == devRoc800 ) {

		bOffset = OFF_ROC800;

		if( uPointType == 1 || uPointType == 2 ) {

			bOffset -= 8;
			}
		}

	return uPointType + bOffset;
	}

BYTE CEmRoc2MasterSerialDriver::GetLogical(AREF Addr)
{
	if( m_pCtx->m_Device == devRoc800 ) {

		BYTE bExt = 0;

		if( Addr.a.m_Table > 200 ) {

			bExt = ((Addr.a.m_Table - 201) / 5 + 1) * 64;
			}
			
		return BYTE((Addr.a.m_Offset >> 8) & 0x3F) + bExt;
		}

	return BYTE((Addr.a.m_Offset >> 7) & 0x7F);
	}

BYTE CEmRoc2MasterSerialDriver::GetParameter(AREF Addr)
{
	if( m_pCtx->m_Device == devRoc800 ) {

		return BYTE(Addr.a.m_Offset & 0xFF);
		}

	return BYTE(Addr.a.m_Offset & 0x7F);
	}

BYTE CEmRoc2MasterSerialDriver::GetStringPointType(AREF Addr)
{
	return Addr.a.m_Table - 6;
	}

BYTE CEmRoc2MasterSerialDriver::GetStringLogical(AREF Addr)
{
	BYTE bExt = BYTE(((Addr.a.m_Offset >> 12) & 0x1) * 128);

	return BYTE((Addr.a.m_Offset >> 13) | (Addr.a.m_Extra << 3)) + bExt;
	}

BYTE CEmRoc2MasterSerialDriver::GetStringParameter(AREF Addr)
{
	return BYTE((Addr.a.m_Offset & 0xFFF) / 15);
	}

// Helpers

BOOL CEmRoc2MasterSerialDriver::IsWord(UINT uType)
{
	return (uType == addrWordAsWord);
	}

BOOL CEmRoc2MasterSerialDriver::IsLong(UINT uType)
{
	return ((uType == addrLongAsLong) || (uType == addrRealAsReal));
	}

BOOL CEmRoc2MasterSerialDriver::IsString(UINT uTable)
{
	return ((uTable >= 6) && (uTable <= 6 + 140));
	}

BOOL CEmRoc2MasterSerialDriver::IsTLP(AREF Addr)
{
	UINT uPointType = GetPointType(Addr);

	switch( uPointType ) {

		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 10:
		case 12:
		case 15:
		case 16:
		case 17:
		case 19:
		case 21:
		case 42:
		case 44:
		case 45:
		case 47:
		case 56:
		case 58:
		case 87:
		case 91:
		case 92:
		case 93:
		case 94:
		case 95:
		case 96:
		case 97:
		case 98:
		case 101:
		case 102:
		case 103:
		case 104:
		case 105:
		case 108:
		case 109:
		case 117:
		case 119:
		case 120:
		case 121:
		case 122:
		case 123:
		case 124:
		case 136:
		case 137:
		case 138:
		case 139:
		case 140:
			return FALSE;
		}

	UINT uParameter = GetParameter(Addr);

	if( uPointType == 0 ) {

		return ((uParameter >= 1) && (uParameter <= 44));
		}
	
	if( uPointType == 7 ) {

		return ((uParameter >= 45) && (uParameter <= 48));
		}

	if( uPointType == 8 ) {
		
		if( uParameter % 4 == 0 ) {

			return TRUE;
			}

		return ((uParameter - 1) % 4 == 0);
		}

	if( uPointType == 41 ) {

		switch( uParameter ) {

			case 3:
			case 17:
			case 21:
			case 25:
			case 29:
			case 33:
			case 37:
			case 41:
			case 45:
			case 49:
			case 53:

				return TRUE;
			
			}

		return FALSE;
		}

	if( uPointType == 43 ) {

		return ((uParameter >= 16) && (uParameter <= 31));
		}

	if( uPointType == 46 ) {

		return (((uParameter >= 47) && (uParameter <= 50)) || uParameter == 59);
		}

	if( uPointType == 71 ) {

		switch( uParameter ) {

			case 2:
			case 6:
			case 10:
			case 14:
			case 22:

				return TRUE;
			}

		return FALSE;
		}

	if( uPointType == 72 ) {

		return (uParameter == 2);
		}

	if( uPointType == 86 ) {

		switch( uParameter ) {

			case 2:
			case 3:
			case 6:
			case 7:
			case 10:
			case 11:
			case 14:
			case 15:
			case 18:
			case 19:
			case 22:
			case 23:
			case 26:
			case 27:
			case 30:
			case 31:
			case 34:
			case 35:
			case 38:
			case 39:
			case 42:
			case 43:
			case 46:
			case 47:
			case 50:
			case 51:
			case 54:
			case 55:
			case 58:
			case 59:
							
				return TRUE;
			}

		return FALSE;
		}

	if( uPointType == 99 ) {

		return (uParameter >= 1);
		}

	if( uPointType == 100 ) {

		return (uParameter == 15);
		}

	if( uPointType == 118 ) {

		switch( uParameter ) {

			case 3:
			case 9:
			case 15:
			case 21:
			case 27:
			case 33:
			case 39:
			case 45:
			case 51:
			case 57:
			case 63:
			case 69:
			case 75:
			case 81:
			case 87:

				return TRUE;
			}

		return FALSE;
		}

	if( uPointType == 125 ) {

		return (uParameter == 2);
		}

	return FALSE;
	}

BOOL CEmRoc2MasterSerialDriver::IsTime(AREF Addr)
{
	UINT uPointType = GetPointType(Addr);

	UINT uParameter = GetParameter(Addr);

	if( m_pCtx->m_Device == devFlo103104 ) {

		if( uPointType == 12 ) {
			
			return (uParameter == 8);
			}
		}

	if( m_pCtx->m_Device == devRoc800 ) {

		if( uPointType == 136 ) {

			return (uParameter == 7);
			}
		}

	return FALSE;
	}

BOOL CEmRoc2MasterSerialDriver::IsExtended(UINT uTable)
{
	return uTable <= 5;
	}

UINT CEmRoc2MasterSerialDriver::GetStrLen(UINT uPointType, UINT uParameter)
{
	if( uPointType == 7 ) {

		if( uParameter == 13 ) {

			return 30;
			}
		}

	else if( uPointType == 15 ) {

		switch( uParameter ) {

			case 2:
			case 11:
			case 12:
			case 13:
			case 15:

				return 20;

			case 14:

				return 12;
			}
		}

	else if( uPointType == 16 ) {

		switch( uParameter ) {

			case 16:
			case 17:

				return 30;

			}
		}

	else if( uPointType == 21 ) {

		return 20;
		}
	
	else if( uPointType == 46 ) {

		if( uParameter == 1 ) {

			return 30;
			}
		}

	else if( uPointType == 58 ) {

		if( uParameter == 0 ) {

			return 20;
			}
		}

	else if( uPointType == 91 ) {

		return 20;
		}

	else if( uPointType == 93 ) {

		switch( uParameter ) {

			case 2:
			case 3:
				return 20;
			}
		}

	else if( uPointType == 94 ) {

		switch( uParameter ) {
			
			case 0:
				return 20;
			case 1:
			case 3:
				return 12;
			}
		}

	else if( uPointType == 95 ) {

		switch( uParameter ) {

			case 12:
			case 13:
			case 16:

				return 40;
			}
		}

	else if( uPointType == 96 ) {

		switch( uParameter ) {

			case 16:
			case 17:
				return 30;
			case 29:
				return 40;
			}
		}

	else if( uPointType == 98 ) {

		return 40;
		}

	else if( uPointType == 120 ) {

		if( uParameter > 0 ) {

			return 30;
			}
		}

	else if( uPointType == 122 ) {

		return 20;
		}

	else if( uPointType == 123 ) {

		return 20;
		}

	else if( uPointType == 137 ) {

		if ( uParameter == 0 ) {

			return 12;
			}
		
		return 20;
		}

	else if( uPointType == 138 ) {

		return 12;
		}

	else if( uPointType == 139 ) {

		switch( uParameter ) {

			case 4:
			case 5:
			case 7:	
			case 8:
			case 9:
			case 11:
				return 20;

			case 10:
				return 30;
			}

		}
		
	return 10;
	}

BOOL CEmRoc2MasterSerialDriver::IsDouble(AREF Addr)
{
	if( m_pCtx->m_Device == devRoc800 ) {

		UINT uPointType = GetPointType(Addr);

		UINT uParameter = GetParameter(Addr);

		if( uPointType == 91 ) {

			return (uParameter == 53);
			}

		if( uPointType == 98 ) {

			return (uParameter >= 43 && uParameter <= 53);
			}
		}
	
	return FALSE;
	}

DWORD CEmRoc2MasterSerialDriver::Convert64To32(PDWORD d)
{
	DWORD dwLong = 0;

	if(d[1] || (d[0] & 0x7FFFFFFF) ) {

		DWORD dExp = (d[1] >> 20) & 0x7FF;

		dwLong  = d[1] & 0x80000000; 

		dwLong |= (dExp - (1023 - 127)) << 23;

		dwLong |= (d[1] & 0xFFFFF) << 3;

		dwLong |= d[0] >> 29;
		}

	return dwLong;
	}

void CEmRoc2MasterSerialDriver::Convert32To64(DWORD dwData, PDWORD d)
{
	if( !(dwData & 0x7FFFFFF) ) {

		d[0] = 0;

		d[1] = 0;

		return;
		}

	DWORD dOut = dwData & 0x80000000; 

	DWORD dExp = (dwData >> 23) & 0xFF; 

	dExp += 1023 - 127;

	dOut |= dExp << 20; 

	d[0]  = dOut | ( (dwData & 0x7FFFFF) >> 3 );

	d[1]  = ( ( dwData & 7 ) << 29 );
	}


// End of file
