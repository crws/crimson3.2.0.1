
#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Constants
//

#define OFF_ROC800	78;

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Master Serial Driver Number 2
//

class CEmRoc2MasterSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CEmRoc2MasterSerialDriver(void);

		// Destructor
		~CEmRoc2MasterSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		
	protected:
		// Device Context
		struct CContext 
		{
			BYTE m_bGroup;
			BYTE m_bUnit;
			BYTE m_Device;
			BYTE m_bLast;
			UINT m_uLast;
			UINT m_uTime;
			UINT m_uPoll;
			
			PTXT m_OpID;
			UINT m_Pass;
			BYTE m_Acc;
			BOOL m_Logon;
			BOOL m_Use;
			};

		// Devices

		enum {
			devFlo103104 = 1 << 0,
			devRoc800    = 1 << 1,
			};

		// Data Members
		CContext * m_pCtx;

		BYTE	m_bGroup;
		BYTE	m_bUnit;
		BYTE	m_bTx[248];
		BYTE	m_bRx[1200];
		UINT	m_TxPtr;
		UINT	m_RxPtr;
		CRC16	m_CRC;
		
		// Handlers
		CCODE	LogOn(void);
		CCODE   DoOpcode180(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode180String(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode7(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   DoOpcode181(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode181String(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode8(AREF Addr, PDWORD pData, UINT uCount);
		
		// Implementation
		void	Begin(void);
		void	End(void);
		void	AddDest(void);
		void	AddHost(void);
		void	AddData(AREF Addr, PDWORD pData, UINT uPos);
		void	AddByte(BYTE bByte);
		void	AddWord(WORD wWord);
		void	AddLong(DWORD dwWord);
		void	AddDouble(DWORD dwWord);
		void	AddTLP(DWORD dwWord);
		void	AddTime(DWORD dwWord);
		void	AddText(PTXT pText, UINT uCount);
		void	AddString(PDWORD pData, UINT uLen, UINT uMax);
		void	AddCheck(BYTE bByte);
		BOOL	Transact(void);
		BOOL	Send(void);
		BOOL	Recv(void);
		BOOL	CheckFrame(void);
		BOOL	CheckCRC(void);
		UINT	GetBytes(PDWORD pData, UINT uCount);
		UINT	GetWords(PDWORD pData, UINT uCount);
		UINT	GetLongs(PDWORD pData, UINT uCount);
		UINT	GetDoubles(PDWORD pData, UINT uCount);
		UINT	GetTLPs(PDWORD pData, UINT uCount);
		UINT	GetStrings(PDWORD pData, UINT uCount);
		BYTE	GetPointType(AREF Addr);
		BYTE	GetLogical(AREF Addr);
		BYTE	GetParameter(AREF Addr);
		BYTE	GetStringPointType(AREF Addr);
		BYTE	GetStringLogical(AREF Addr);
		BYTE	GetStringParameter(AREF Addr);
		
		// Helpers
		BOOL IsWord(UINT uType);
		BOOL IsLong(UINT uType);
		BOOL IsString(UINT uTable);
		BOOL IsTLP(AREF Addr);
		BOOL IsTime(AREF Addr);
		BOOL IsExtended(UINT uTable);
		UINT GetStrLen(UINT uPointType, UINT uParameter);
		BOOL IsDouble(AREF Addr);
		DWORD Convert64To32(PDWORD d);
		void Convert32To64(DWORD dwData, PDWORD d);
	};


// End of file

