
//////////////////////////////////////////////////////////////////////////
//
// Base Class
//

#include "bacbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet IEEE 802.3 Driver
//

class CBACNet8023 : public CBACNetBase
{
	public:
		// Constructor
		CBACNet8023(void);

		// Destructor
		~CBACNet8023(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping(void);

	protected:
		// MAC Address
		struct CMacAddr
		{
			BYTE m_MAC[6];
			};

		// Device Context
		struct CContext : CBACNetBase::CBaseCtx
		{
			UINT     m_uTime1;
			CMacAddr m_Mac;
			};

		// Data Members
		CContext * m_pCtx;
		ISocket  * m_pSock;
		CMacAddr   m_RxMac;

		// Transport Hooks
		BOOL SendFrame(BOOL fThis, BOOL fReply);
		BOOL RecvFrame(void);

		// Transport Header
		void AddTransportHeader(BOOL fThis);
		BOOL StripTransportHeader(void);

		// Device Helpers
		BOOL IsThisDevice(void);
	};

// End of File
