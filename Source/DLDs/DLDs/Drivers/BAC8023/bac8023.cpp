
#include "intern.hpp"

#include "bac8023.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet IEEE 802.3 Driver
//

// Instantiator

INSTANTIATE(CBACNet8023);

// Constructor

CBACNet8023::CBACNet8023(void)
{
	m_Ident = DRIVER_ID;

	m_pSock = NULL;
	}

// Destructor

CBACNet8023::~CBACNet8023(void)
{
	}

// Configuration

void MCALL CBACNet8023::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CBACNet8023::Attach(IPortObject *pPort)
{
	m_pSock = CreateSocket(IP_RAW);
	}

void MCALL CBACNet8023::Detach(void)
{
	if( m_pSock ) {

		m_pSock->Release();

		m_pSock = NULL;
		}
	}

void MCALL CBACNet8023::Open(void)
{
	if( m_pSock ) {

		m_pSock->Listen(0x82);
		}	
	}

// Device

CCODE MCALL CBACNet8023::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx           = new CContext;

			m_pBase          = m_pCtx;

			m_pCtx->m_fBroke = FALSE;

			m_pCtx->m_uTime1 = GetWord(pData);

			LoadConfig(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CBACNet8023::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete [] m_pBase->m_pObj;

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CBACNet8023::Ping(void)
{
	if( m_pSock ) {

		if( m_pCtx->m_fFound ) {

			return CCODE_SUCCESS;
			}

		if( CBACNetBase::Ping() == CCODE_SUCCESS ) {

			m_pCtx->m_Mac    = m_RxMac;

			m_pCtx->m_fFound = TRUE;

			Show("ping", "found object id");
			
			return CCODE_SUCCESS;
			}
		}

	return CCODE_ERROR;
	}

// Transport Hooks

BOOL CBACNet8023::SendFrame(BOOL fThis, BOOL fReply)
{
	if( m_pSock ) {

		if( !fThis || m_pCtx->m_fFound ) {

			AddNetworkHeader  (fThis, fReply);

			AddTransportHeader(fThis);

			Dump("tx", m_pTxBuff);

			for( UINT n = 0; n < 5; n++ ) {

				if( m_pSock->Send(m_pTxBuff) == S_OK ) {

					m_pTxBuff = NULL;

					return TRUE;
					}

				Sleep(10);
				}

			Show("send-frame", "can't send");
			}
		else
			Show("send-frame", "unknown mac");
		}

	m_pTxBuff->Release();

	m_pTxBuff = NULL;

	return FALSE;
	}

BOOL CBACNet8023::RecvFrame(void)
{
	UINT uCount = 0;

	SetTimer(m_pCtx->m_uTime1);

	while( GetTimer() ) {

		if( m_pSock ) {
			
			m_pSock->Recv(m_pRxBuff);

			if( m_pRxBuff ) {

				if( StripTransportHeader() ) {

					if( StripNetworkHeader() ) {

						if( IsThisDevice() ) {

							Dump("rx", m_pRxBuff);

							return TRUE;
							}
						}
					}
				
				uCount++;

				RecvDone();
				}

			Sleep(10);

			continue;
			}

		Sleep(GetTimer());
		}

	Show("recv-frame", uCount ? "no good frame" : "receive timeout");

	return FALSE;
	}

// Transport Header

void CBACNet8023::AddTransportHeader(BOOL fThis)
{
	UINT  uData = m_pTxBuff->GetSize();

	UINT  uSize = 17;

	PBYTE pData = m_pTxBuff->AddHead(uSize);

	if( fThis ) {
	
		memcpy(pData + 0, m_pCtx->m_Mac.m_MAC, 6);

		memset(pData + 6, 0, 6);
		}
	else {
		memset(pData + 0, 0xFF, 6);

		memset(pData + 6, 0, 6);
		}

	pData[12] = HIBYTE(uData + 3);

	pData[13] = LOBYTE(uData + 3);

	pData[14] = 0x82;

	pData[15] = 0x82;

	pData[16] = 0x03;
	}

BOOL CBACNet8023::StripTransportHeader(void)
{
	PBYTE pData = m_pRxBuff->GetData();

	if( pData[14] == 0x82 ) {
		
		if( pData[15] == 0x82 ) {

			if( pData[16] == 0x03 ) {

				memcpy(m_RxMac.m_MAC, pData + 6, 6);

				m_pRxBuff->StripHead(17);

				return TRUE;
				}
			}
		}

	Show("strip", "invalid transport header");

	return FALSE;
	}

// Device Helpers

BOOL CBACNet8023::IsThisDevice(void)
{
	if( m_pCtx->m_fFound ) {

		if( memcmp(m_RxMac.m_MAC, m_pCtx->m_Mac.m_MAC, 6) ) {

			return FALSE;
			}
		}

	return TRUE;
	}

// End of File
