
#include "intern.hpp"

#include "bacbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet Base Driver
//

// Constructor

CBACNetBase::CBACNetBase(void)
{
	m_pTxBuff = NULL;
	}

// Destructor

CBACNetBase::~CBACNetBase(void)
{
	}

// User Functions

UINT MCALL CBACNetBase::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	if( pContext ) {

		CBaseCtx *pBase = (CBaseCtx *) pContext;

		if( uFunc == 1 ) {

			pBase->m_Device = ATOI(Value);

			pBase->m_fFound = FALSE;
	
			return 1;
			}
		}

	return 0;
	}

// Entry Points

CCODE MCALL CBACNetBase::Ping(void)
{
	for( UINT n = 0; n < (m_pBase->m_fBroke ? 3u : 2u); n++ ) {

		if( InitFrame() ) {

			DWORD DevObj = (m_pBase->m_Device | 0x02000000);

			if( n == 0 ) {

				Show("ping", "discovery via who-has");

				AddRawByte(0x10);
				
				AddRawByte(0x07);

				AddCtxLong(2, DevObj);

				AddDone();
				}
			else {
				Show("ping", "discovery via who-is");

				AddRawByte(0x10);
				
				AddRawByte(0x08);

				if( n == 1 ) {

					AddCtxLong(0, m_pBase->m_Device);

					AddCtxLong(1, m_pBase->m_Device);
					}

				AddDone();
				}

			if( SendFrame(FALSE, FALSE) ) {

				for( UINT r = 0; r < 3; r++ ) {

					if( RecvFrame() ) {

						UINT  uSize = m_pRxBuff->GetSize();
	
						PBYTE pRecv = m_pRxBuff->GetData();

						if( pRecv[0] == 0x10 && (pRecv[1] == (n == 0 ? 0x01 : 0x00)) )  {

							PCBYTE pData  = pRecv + 2;
	
							DWORD  Object = 0;

							if( ExtractValue(Object, addrLongAsLong, pData) ) {

								if( Object == DevObj ) {

									m_pBase->m_Net = m_RxNet;

									RecvDone();

									return CCODE_SUCCESS;
									}
								}
				
							Show("ping", "not correct device");
							}

						RecvDone();
						}
					}
				}
			else {
				Show("ping", "can't send frame");

				break;
				}
			}
		else {
			Show("ping", "can't make frame");

			break;
			}
		}

	m_pBase->m_fFound = FALSE;

	return CCODE_ERROR;
	}

CCODE MCALL CBACNetBase::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pBase->m_fFound ) {

		Show("read", "unknown device");

		return CCODE_ERROR | CCODE_NO_RETRY;
		}

	UINT uObject = MAKEWORD( HIBYTE(Addr.a.m_Offset),
				 LOBYTE(Addr.a.m_Extra)
				 ) - 1;

	if( uObject < m_pBase->m_nObj ) {

		DWORD ObjectID = m_pBase->m_pObj[uObject];

		BYTE  PropID   = LOBYTE(Addr.a.m_Offset);

		BYTE  TypeID   = (Addr.a.m_Table & 0x0F);

		if( IsRelinquish(TypeID) ) {

			return 1;
			}

		if( BuildPropRead(ObjectID, PropID) ) {

			if( SendFrame(TRUE, TRUE) ) {

				for( UINT u = 0; u < 3; u++ ) {
				
					if( RecvFrame() ) {

						UINT  uSize = m_pRxBuff->GetSize();

						PBYTE pRecv = m_pRxBuff->GetData();

						if( pRecv[0] == 0x30 ) {

							if( pRecv[1] != m_pBase->m_bTxSeq ) {

								RecvDone();

								continue;
								}

							if( pRecv[2] == 12 ) {

								BYTE   bOurType = Addr.a.m_Type;

								PCBYTE pSource  = pRecv + 11;

								if( ExtractValue(pData[0], bOurType, pSource) ) {

									RecvDone();
			
									return 1;
									}
								else
									Show("read", "can't extract");
								}
							else
								Show("read", "operation failed");

							RecvDone();

							return CCODE_ERROR;
							}

						if( pRecv[0] == 0x50 ) {

							if( pRecv[1] != m_pBase->m_bTxSeq ) {

								RecvDone();

								continue;
								}

							// TODO -- Should we eat errors that indicate
							// no such property or no such object? That
							// would make things more stable...

							Show("read", "operation failed");

							RecvDone();

							return CCODE_ERROR;
							}

						RecvDone();

						continue;
						}

					m_pBase->m_fFound = FALSE;

					Show("read", "receive timeout");

					break;
					}
				}
			else
				Show("read", "can't send frame");

			return CCODE_ERROR;
			}
		else
			Show("read", "can't make frame");

		return CCODE_BUSY;
		}

	Show("read", "invalid object");

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CBACNetBase::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pBase->m_fFound ) {

		Show("write", "unknown device");

		return CCODE_ERROR | CCODE_NO_RETRY;
		}

	UINT uObject = MAKEWORD( HIBYTE(Addr.a.m_Offset),
				 LOBYTE(Addr.a.m_Extra)
				 ) - 1;

	if( uObject < m_pBase->m_nObj ) {

		DWORD ObjectID = m_pBase->m_pObj[uObject];

		BYTE  PropID   = LOBYTE(Addr.a.m_Offset);

		BYTE  TypeID   = (Addr.a.m_Table & 0x0F);

		if( TypeID == 8 ) {

			// NOTE -- Horrible hack to find length of bit
			// string as we can't get it from our type info.

			DWORD Data = 0;

			if( !Read(Addr, &Data, 1) ) {

				Show("write", "can't find length");

				return CCODE_ERROR;
				}
			}

		if( IsRelinquish(TypeID) ) {

			if( !(*pData) ) {

				return 1;
				}

			TypeID = 0;
			}

		if( BuildPropWrite(ObjectID, PropID, TypeID, *pData) ) {

			if( SendFrame(TRUE, TRUE) ) {
			
				for( UINT u = 0; u < 3; u++ ) {
				
					if( RecvFrame() ) {

						UINT  uSize = m_pRxBuff->GetSize();

						PBYTE pRecv = m_pRxBuff->GetData();

						if( pRecv[0] == 0x20 ) {

							if( pRecv[1] != m_pBase->m_bTxSeq ) {

								RecvDone();

								continue;
								}

							if( pRecv[2] == 15 ) {

								RecvDone();

								return 1;
								}

							Show("write", "operation failed");
						
							RecvDone();

							return CCODE_ERROR;
							}

						if( pRecv[0] == 0x50 ) {

							if( pRecv[1] != m_pBase->m_bTxSeq ) {

								RecvDone();

								continue;
								}

							if( pRecv[4] == 2 ) {
							
								if( pRecv[6] == 37 || pRecv[6] == 40 ) {

									Show("write", "operation failed but eaten");

									// NOTE -- Eat failed writes.

									RecvDone();

									return 1;
									}
								}

							Show("write", "operation failed");
						
							RecvDone();

							return CCODE_ERROR;
							}

						RecvDone();

						continue;
						}

					m_pBase->m_fFound = FALSE;

					Show("write", "receive timeout");

					break;
					}
				}
			else
				Show("write", "can't send frame");

			return CCODE_ERROR;
			}
		else
			Show("write", "can't make frame");

		return CCODE_BUSY;
		}
	
	Show("write", "invalid object");

	return CCODE_ERROR | CCODE_HARD;
	}

// Configuration

BOOL CBACNetBase::LoadConfig(PCBYTE &pData)
{
	m_pBase->m_Device = GetLong(pData);

	m_pBase->m_nObj   = GetWord(pData) / 4;

	m_pBase->m_pObj   = new DWORD [ m_pBase->m_nObj ];

	for( UINT n = 0; n < m_pBase->m_nObj; n++ ) {

		m_pBase->m_pObj[n] = GetLong(pData);
		}

	m_pBase->m_bTxSeq = 0;

	m_pBase->m_fFound = FALSE;

	return TRUE;
	}

// Frame Building

BOOL CBACNetBase::InitFrame(void)
{
	if( (m_pTxBuff = CreateBuffer(600, TRUE)) ) {

		m_pTxBuff->AddTail(600);

		m_pTxData = m_pTxBuff->GetData();

		m_uTxPtr  = 0;

		return TRUE;
		}
	
	Sleep(10);

	return FALSE;
	}

void CBACNetBase::AddDone(void)
{
	m_pTxBuff->StripTail(m_pTxBuff->GetSize() - m_uTxPtr);
	}

void CBACNetBase::AddRawByte(BYTE bData)
{
	m_pTxData[m_uTxPtr++] = bData;
	}

void CBACNetBase::AddRawWord(WORD wData)
{
	AddRawByte(HIBYTE(wData));

	AddRawByte(LOBYTE(wData));
	}

void CBACNetBase::AddRawTrip(DWORD dwData)
{
	AddRawByte(LOBYTE(HIWORD(dwData)));

	AddRawWord(LOWORD(dwData));
	}

void CBACNetBase::AddRawLong(DWORD dwData)
{
	AddRawWord(HIWORD(dwData));

	AddRawWord(LOWORD(dwData));
	}

void CBACNetBase::AddAppTag(BYTE bTag, BYTE bSize)
{
	AddRawByte((bTag << 4) | 0x00 | bSize);
	}

void CBACNetBase::AddCtxTag(BYTE bTag, BYTE bSize)
{
	AddRawByte((bTag << 4) | 0x08 | bSize);
	}

void CBACNetBase::AddCtxByte(BYTE bTag, BYTE bData)
{
	AddCtxTag(bTag, sizeof(BYTE));

	AddRawByte(bData);
	}

void CBACNetBase::AddCtxWord(BYTE bTag, WORD wData)
{
	AddCtxTag(bTag, sizeof(WORD));

	AddRawWord(wData);
	}

void CBACNetBase::AddCtxTrip(BYTE bTag, DWORD dwData)
{
	AddCtxTag(bTag, sizeof(WORD) + sizeof(BYTE));

	AddRawTrip(dwData);
	}

void CBACNetBase::AddCtxLong(BYTE bTag, DWORD dwData)
{
	AddCtxTag(bTag, sizeof(DWORD));

	AddRawLong(dwData);
	}

void CBACNetBase::AddCtxOpen(BYTE bTag)
{
	AddCtxTag(bTag, 6);
	}

void CBACNetBase::AddCtxClose(BYTE bTag)
{
	AddCtxTag(bTag, 7);
	}

void CBACNetBase::AddNetworkHeader(BOOL fThis, BOOL fReply)
{
	if( fThis ) {

		if( m_pBase->m_Net.m_NET ) {

			UINT  uSize = 6 + m_pBase->m_Net.m_LEN;

			PBYTE pData = m_pTxBuff->AddHead(uSize);

			pData[0] = 0x01;

			pData[1] = fReply ? 0x24 : 0x20;

			pData[2] = HIBYTE(m_pBase->m_Net.m_NET);

			pData[3] = LOBYTE(m_pBase->m_Net.m_NET);

			pData[4] = m_pBase->m_Net.m_LEN;

			memcpy(pData + 5, m_pBase->m_Net.m_MAC, m_pBase->m_Net.m_LEN);

			pData[5 + m_pBase->m_Net.m_LEN] = 200;
			}
		else {
			UINT  uSize = 2;

			PBYTE pData = m_pTxBuff->AddHead(uSize);

			pData[0] = 0x01;
		
			pData[1] = fReply ? 0x04 : 0x00;
			}
		}
	else {
		UINT  uSize = 6;

		PBYTE pData = m_pTxBuff->AddHead(uSize);

		pData[0] = 0x01;
		
		pData[1] = fReply ? 0x24 : 0x20;

		pData[2] = 0xFF;

		pData[3] = 0xFF;

		pData[4] = 0;

		pData[5] = 200;
		}
	}

// Frame Parsing

void CBACNetBase::RecvDone(void)
{
	m_pRxBuff->Release();

	m_pRxBuff = NULL;
	}

BOOL CBACNetBase::StripNetworkHeader(void)
{
	PBYTE pData = m_pRxBuff->GetData();

	UINT  uPtr  = 0;

	if( pData[uPtr++] == 0x01 ) {

		BYTE bFlags = pData[uPtr++];

		if( !(bFlags & 0x80) ) {

			if( bFlags & 0x20 ) {

				BYTE DLEN = pData[uPtr+2];

				uPtr      = uPtr + 3 + DLEN;
				}

			if( bFlags & 0x08 ) {

				m_RxNet.m_NET = MAKEWORD(pData[uPtr+1], pData[uPtr+0]);

				m_RxNet.m_LEN = pData[uPtr+2];

				memcpy(m_RxNet.m_MAC, pData + uPtr + 3, m_RxNet.m_LEN);

				uPtr = uPtr + 3 + m_RxNet.m_LEN;
				}
			else {
				BYTE bNull = 0;

				UINT uSize = sizeof(m_RxNet);

				memset(&m_RxNet, bNull, uSize);
				}

			if( bFlags & 0x20 ) {

				uPtr = uPtr + 1;
				}

			m_pRxBuff->StripHead(uPtr);

			return TRUE;
			}
		}

	Show("strip", "invalid network header");

	return FALSE;
	}

// Read Requests

BOOL CBACNetBase::BuildPropRead(DWORD ObjectID, BYTE PropID)
{
	if( InitFrame() ) {

		BYTE Priority = 0;

		DecodePriority(PropID, Priority);

		AddRawByte(0x00);
		
		AddRawByte(0x00);
		
		AddRawByte(++m_pBase->m_bTxSeq);

		AddRawByte(12);

		AddCtxLong(0, ObjectID);

		AddCtxByte(1, PropID);

		AddDone();

		return TRUE;
		}

	return FALSE;
	}

BOOL CBACNetBase::ExtractValue(DWORD &Dest, BYTE bOurType, PCBYTE &pData)
{
	BYTE bNetType = (*pData >> 4);

	BYTE bNetSize = (*pData & 15);

	pData++;

	// TODO -- Handle extended network sizing?

	if( bOurType == addrLongAsLong ) {

		if( bNetType == 1 ) {

			// Boolean

			Dest = bNetSize ? 1 : 0;

			return TRUE;
			}

		if( bNetType == 2 || bNetType == 9 ) {

			// Unsigned Integer or Enumeration
				
			Dest = 0;

			if( bNetSize ) {

				while( bNetSize-- ) {

					Dest <<= 8;

					Dest  += *pData++;
					}
				}

			return TRUE;
			}

		if( bNetType == 3 ) {

			// Signed Integer

			Dest = 0;

			if( bNetSize ) {

				DWORD Test = (0x00000080 << 8 * (bNetSize - 1));

				DWORD Fill = (0xFFFFFF00 << 8 * (bNetSize - 1));

				while( bNetSize-- ) {

					Dest <<= 8;

					Dest  += *pData++;
					}

				if( Fill && (Dest & Test) ) {

					Dest |= Fill;
					}
				}

			return TRUE;
			}

		if( bNetType == 8 ) {

			// Bit String

			if( bNetSize ) {

				UINT  uNull = *pData++;

				UINT  uBits = 8 * (bNetSize - 1) - uNull;

				if( (m_uBits = uBits) <= 32 ) {

					BYTE  bSrcData = *pData++;

					BYTE  bSrcMask = 0x80;

					DWORD DestMask = 1;

					Dest           = 0;

					while( uBits-- ) {

						if( bSrcData & bSrcMask ) {

							Dest |= DestMask;
							}

						if( uBits && !(bSrcMask >>= 1) ) {

							bSrcMask = 0x80;

							bSrcData = *pData++;
							}

						DestMask <<= 1;
						}

					return TRUE;
					}
				}

			Show("extract", "bad bit-string");

			return FALSE;
			}

		if( bNetType == 10 ) {

			// Date

			if( bNetSize == 4 ) {

				UINT y = *pData++ + 1900;

				UINT m = *pData++;

				UINT d = *pData++;

				Dest   = Date(y, m, d);

				pData += bNetSize;

				return TRUE;
				}

			Show("extract", "bad date");

			return FALSE;
			}

		if( bNetType == 11 ) {

			// Time

			if( bNetSize == 4 ) {

				UINT h = *pData++;

				UINT m = *pData++;
				
				UINT s = *pData++;

				Dest   = Time(h, m, s);

				pData += bNetSize;

				return TRUE;
				}

			Show("extract", "bad time");

			return FALSE;
			}

		if( bNetType == 12 ) {

			// ObjectID

			if( bNetSize == 4 ) {

				Dest   = MotorToHost(*PU4(PVOID(pData)));

				pData += bNetSize;

				return TRUE;
				}

			Show("extract", "bad object-id");

			return FALSE;
			}

		Show("extract", "bad type for long-as-long");

		return FALSE;
		}

	if( bOurType == addrRealAsReal ) {

		if( bNetType == 4 ) {

			// Real Number

			if( bNetSize == 4 ) {

				Dest   = MotorToHost(*PU4(PVOID(pData)));

				pData += bNetSize;

				return TRUE;
				}

			Show("extract", "bad real");

			return FALSE;
			}

		Show("extract", "bad type for real-as-real");

		return FALSE;
		}

	Show("extract", "bad our-type");

	return FALSE;
	}

// Write Requests

BOOL CBACNetBase::BuildPropWrite(DWORD ObjectID, BYTE PropID, BYTE TypeID, DWORD dwData)
{
	if( InitFrame() ) {

		BYTE Priority = 0;

		DecodePriority(PropID, Priority);

		AddRawByte(0x00);
		
		AddRawByte(0x00);
		
		AddRawByte(++m_pBase->m_bTxSeq);

		AddRawByte(15);

		AddCtxLong(0, ObjectID);

		AddCtxByte(1, PropID);

		AddCtxOpen(3);

		if( EncodeValue(TypeID, dwData) ) {

			AddCtxClose(3);

			if( Priority ) {

				AddCtxByte(4, Priority);
				}

			AddDone();

			return TRUE;
			}
		}

	m_pTxBuff->Release();

	m_pTxBuff = NULL;

	return FALSE;
	}

BOOL CBACNetBase::EncodeValue(BYTE bNetType, DWORD dwData)
{
	if( bNetType == 0 ) {

		// Relinquish

		AddAppTag(bNetType, 0);

		return TRUE;
		}

	if( bNetType == 1 ) {

		// Boolean

		AddAppTag(bNetType, dwData ? 1 : 0);

		return TRUE;
		}

	if( bNetType == 2 || bNetType == 9 ) {

		// Unsigned Integer or Enumeration

		if( (dwData & 0xFFFFFF00) == 0x00000000 ) {

			AddAppTag (bNetType, 1);

			AddRawByte(BYTE(dwData));

			return TRUE;
			}

		if( (dwData & 0xFFFF0000) == 0x00000000 ) {

			AddAppTag (bNetType, 2);

			AddRawWord(WORD(dwData));

			return TRUE;
			}

		if( (dwData & 0xFF000000) == 0x00000000 ) {

			AddAppTag (bNetType, 3);

			AddRawTrip(dwData);

			return TRUE;
			}

		AddAppTag (bNetType, 4);

		AddRawLong(dwData);

		return TRUE;
		}

	if( bNetType == 3 ) {

		// Signed Integer

		if( (dwData & 0xFFFFFF80) == 0x00000000 || (dwData & 0xFFFFFF80) == 0xFFFFFF80 ) {

			AddAppTag (bNetType, 1);

			AddRawByte(BYTE(dwData));

			return TRUE;
			}

		if( (dwData & 0xFFFF8000) == 0x00000000 || (dwData & 0xFFFF8000) == 0xFFFF8000 ) {

			AddAppTag (bNetType, 2);

			AddRawWord(WORD(dwData));

			return TRUE;
			}

		if( (dwData & 0xFF800000) == 0x00000000 || (dwData & 0xFF800000) == 0xFF800000 ) {

			AddAppTag (bNetType, 3);

			AddRawTrip(dwData);

			return TRUE;
			}

		AddAppTag (bNetType, 4);

		AddRawLong(dwData);

		return TRUE;
		}

	if( bNetType == 4 ) {

		// Real Number

		AddAppTag (bNetType, 4);

		AddRawLong(dwData);

		return TRUE;
		}

	if( bNetType == 8 ) {

		// Bit String

		// TODO -- What about extended sizing?

		AddAppTag (bNetType, 1 + (m_uBits + 7) / 8);

		AddRawByte((8 - m_uBits % 8) % 8);

		DWORD dwSrcMask = 1;

		BYTE  bDestData = 0;

		BYTE  bDestMask = 0x80;

		while( m_uBits-- ) {

			if( dwData & dwSrcMask ) {

				bDestData |= bDestMask;
				}

			if( !(bDestMask >>= 1) || !m_uBits ) {

				AddRawByte(bDestData);

				bDestData = 0;

				bDestMask = 0x80;
				}

			dwSrcMask <<= 1;
			}

		return TRUE;
		}

	if( bNetType == 10 ) {

		// Date

		BYTE y = GetYear (dwData) - 1900;

		BYTE m = GetMonth(dwData);

		BYTE d = GetDate (dwData);

		AddAppTag (bNetType, 4);

		AddRawByte(y);
		
		AddRawByte(m);
		
		AddRawByte(d);
		
		AddRawByte(0xFF);

		return TRUE;
		}

	if( bNetType == 11 ) {

		// Time

		BYTE h = GetHour(dwData);

		BYTE m = GetMin (dwData);

		BYTE s = GetSec (dwData);

		AddAppTag (bNetType, 4);

		AddRawByte(h);
		
		AddRawByte(m);
		
		AddRawByte(s);
		
		AddRawByte(0);

		return TRUE;
		}

	if( bNetType == 12 ) {

		// ObjectID

		AddAppTag (bNetType, 4);

		AddRawLong(dwData);

		return TRUE;
		}

	Show("encode", "bad type");

	return FALSE;
	}

// Priority Encoding

BOOL CBACNetBase::DecodePriority(BYTE &PropID, BYTE &Priority)
{
	if( PropID >= 230 && PropID <= 246 ) {

		Priority = BYTE(PropID - 230);

		PropID   = 85;

		return TRUE;
		}

	if( PropID == 85 ) {

		Priority = 0;

		return TRUE;
		}

	return FALSE;
	}

// Relinquish Support

BOOL CBACNetBase::IsRelinquish(BYTE TypeID)
{
	return (TypeID == 13);
	}

// Debugging

void CBACNetBase::Dump(PCTXT pName, CBuffer *pBuff)
{
/*	UINT  uSize = pBuff->GetSize();

	PBYTE pRecv = pBuff->GetData();

	AfxTrace("%4.4X : %6u : %-10.10s : ", m_Ident, m_pBase->m_Device, pName);

	for( UINT n = 0; n < uSize; n++ ) {

		AfxTrace("%2.2X ", pRecv[n]);
		}

	AfxTrace("\n");
*/	}

void CBACNetBase::Show(PCTXT pName, PCTXT pError)
{
/*	AfxTrace("%4.4X : %6u : %-10.10s : %s", m_Ident, m_pBase->m_Device, pName, pError);

	AfxTrace("\n");
*/	}

// End of File
