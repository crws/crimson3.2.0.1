
#ifndef	INCLUDE_totalflow3tcp_HPP
	
#define	INCLUDE_totalflow3tcp_HPP

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced TCP/IP Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#include "totalflow3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced TCP/IP Driver
//

class CTotalFlow3TcpMasterDriver : public CTotalFlow3Master
{
	public:
		// Constructor
		CTotalFlow3TcpMasterDriver(void);

		// Destructor
		~CTotalFlow3TcpMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:

		struct CCtx : public CTotalFlow3Master::CBase3Ctx
		{
			DWORD     m_IP;
			UINT      m_uPort;
			char      m_fKeep;
			BOOL      m_fPing;
			UINT      m_uTime1;
			UINT      m_uTime3;
			ISocket * m_pSock;
			UINT      m_uLast;
			UINT	  m_uConn;
			};

		CCtx	* m_pCtx;
		UINT	  m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		BOOL CheckConnection(void);

		// Transport Layer
		BOOL CheckLink(void);
		void AbortLink(void);
		BOOL Send(PBYTE pBuff, UINT uLength);
		UINT Recv(UINT uTime);

		// Overridables
		UINT GetMaxCount(void);
	};

#endif

// End of File
