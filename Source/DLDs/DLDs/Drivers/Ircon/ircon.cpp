
#include "intern.hpp"

#include "ircon.hpp"

//#define IRCONDEBUG TRUE

//////////////////////////////////////////////////////////////////////////
//
// Ircon Driver
//		

//struct FAR IRCONCmdDef {
//	char 	sName[4];
//	UINT	Type;
//	UINT	uID;
//	BYTE	bMod;
//	};
	
IRCONCmdDef CODE_SEG CIrconDriver::CL[] = {
// Modline 5 and 3 combined
	{"TT",	READONLY,	1,	M3ANDM5},
	{"EM",	READWRITE,	CEM,	M3ANDM5},
	{"ES",	READWRITE,	CES,	M3ANDM5},
	{"RT",	READWRITE,	4,	M3ANDM5},
	{"DR",	READWRITE,	5,	M3ANDM5},
	{"AZ",	READWRITE,	6,	M3ANDM5},
	{"AF",	READWRITE,	7,	M3ANDM5},
	{"PS",	READWRITE,	8,	M3ANDM5},
	{"PK",	READWRITE,	9,	M3ANDM5},
	{"PD",	READWRITE,	10,	M3ANDM5},
	{"TS",	READONLY,	OPTS,	M3ANDM5},  // Modline 5 TS Status
	{"ST",	READONLY,	12,	M3ANDM5},
	{"VR",	READONLY,	MVR,	M3ANDM5},
	{"MD",	READONLY,	MM1,	M3ANDM5},
	{"TP",	READONLY,	15,	M3ANDM5},
	{"SP",	READWRITE,	16,	M3ONLY},
	{"KP",	READWRITE,	17,	M3ONLY},
	{"KI",	READWRITE,	18,	M3ONLY},
	{"KD",	READWRITE,	19,	M3ONLY},
	{"KL",	READWRITE,	20,	M3ANDM5},
	{"AT",	READWRITE,	21,	M5ONLY},
	{"AM",	READWRITE,	22,	M3ONLY},
	{"CP",	READWRITE,	23,	M3ONLY},
	{"BP",	READWRITE,	24,	M3ONLY},
	{"OA",	READWRITE,	25,	M3ONLY},
	{"OB",	READWRITE,	26,	M3ONLY},
	{"AC",	READWRITE,	27,	M5ONLY},
	{"AH",	READWRITE,	28,	M5ONLY},
	{"AL",	READWRITE,	29,	M5ONLY},
	{"AO",	READWRITE,	30,	M5ONLY},
	{"BT",	READONLY,	31,	M5ONLY},
	{"CL",	READWRITE,	32,	M5ONLY},
	{"DT",	READWRITE,	33,	M5ONLY},
	{"FT",	READONLY,	34,	M5ONLY},
	{"LS",	READWRITE,	35,	M5ONLY},
	{"MT",	READWRITE,	CMT,	M5ONLY},
	{"PR",	READWRITE,	37,	M5ONLY},
	{"RC",	READONLY,	38,	M5ONLY},
	{"RP",	READWRITE,	39,	M5ONLY},
	{"RR",	READWRITE,	40,	M5ONLY},
	{"SG",	READWRITE,	41,	M5ONLY},
	{"SN",	READONLY,	SN,	M5ONLY},
	{"SN",	READONLY,	SNB,	M5ONLY},
	{"SN",	READONLY,	SNC,	M5ONLY},
	{"SN",	READONLY,	SND,	M5ONLY},
	{"SN",	READONLY,	SNE,	M5ONLY},
	{"SN",	READONLY,	SNF,	M5ONLY},
	{"SW",	READONLY,	43,	M5ONLY},
	{"UF",	READONLY,	44,	M5ONLY},
	{"UN",	READWRITE,	45,	M5ONLY},
	{"UZ",	READONLY,	46,	M5ONLY},
	{"MD",	READONLY,	MM2,	M3ANDM5},
	{"MD",	READONLY,	MM3,	M3ANDM5},
	{"MD",	READONLY,	MM4,	M3ANDM5},
	{"MD",	READONLY,	MM5,	M3ANDM5},
	{"MD",	READONLY,	MM6,	M3ANDM5},
	{"AA",	READWRITE,	MM6+1,	M5ONLY},
	{"CE",	READONLY,	CCE,	M5ONLY}, // Response to Latest EM Write
	{"CM",	READONLY,	CCM,	M5ONLY}, // Response to Latest MT Write
	{"CS",	READONLY,	CCS,	M5ONLY}, // Response to Latest ES Write
	{"TS",	READONLY,	CCTS,	M5ONLY}, // Modline 5 TS Temperature
	{"TS",	READWRITE,	C3TS,	M3ONLY}, // Modline 3 Track/Hold Command
	{"TX",	READWRITE,	ITIME,	M3ANDM5}, // User Adjustable Timeout

/* Modline 3 command set
	{"TT",	READONLY,	1},
	{"EM",	READWRITE,	2},
	{"ES",	READWRITE,	3},
	{"RT",	READWRITE,	4},
	{"DR",	READWRITE,	5},
	{"AZ",	READWRITE,	6},
	{"AF",	READWRITE,	7},
	{"PS",	READWRITE,	8},
	{"PK",	READWRITE,	9},
	{"PD",	READWRITE,	10},
	{"TS",	READWRITE,	11},
	{"ST",	READONLY,	12},
//	{"VR",	READONLY,	13},
//	{"MD",	READONLY,	14},
	{"TP",	READONLY,	15},
	{"SP",	READWRITE,	16},
	{"KP",	READWRITE,	17},
	{"KI",	READWRITE,	18},
	{"KD",	READWRITE,	19},
	{"KL",	READWRITE,	20},
	{"AT",	READWRITE,	21},
	{"AM",	READWRITE,	22},
	{"CP",	READWRITE,	23},
	{"BP",	READWRITE,	24},
	{"OA",	READWRITE,	25},
	{"OB",	READWRITE,	26},
*/
	};

/*
AA - analog alarm output
AC - auto cal time period (in hours)
AF - analog full scale
AH - analog output high temperature
AL - analog output low temperature
AO - analog output mode
AZ - analog zero scale
BT - Report Isoblock temperature (read only)
CL - Set to 1 or 2 color mode (R-series only)
DR - peak picker decay rate
DT - dirty window threshold (DWD only)
EM - emissivity (only 1 color or 1 color mode)
ES - E slope (only 2 color mode in R-series only)
FT - Features Matrix (read only)
KL - keyboard lock
LS - Laser control
MD - model # (read only)
MT - match function
PD - peak delay
PK - peak picker reset below
PR - peak picker reset
PS - peak picker auto reset
RC - instrument temperature (read only)
RP - relay polarity
RR - relay response
RT - response time
SG - input signal mode
SN - unit serial number (read only)
ST - system alarm status (read only)
SW - switch input status (read only)
TP - controller type (read only)
TS - temperature and status (read only)
TT - temperature (read only)
UF - unit full scale (read only)
UN - units select
UZ - unit zero scale (read only)
VR - firmware version (read only)


*/

// Instantiator

INSTANTIATE(CIrconDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CIrconDriver::CIrconDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_ErrorCount = 0;
	}

// Destructor

CIrconDriver::~CIrconDriver(void)
{
	}

// Configuration

void MCALL CIrconDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CIrconDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CIrconDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CIrconDriver::Open(void)
{
	m_pCL = (IRCONCmdDef FAR * )CL;
	}

// Device

CCODE MCALL CIrconDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_bModel = GetByte(pData);

			if( m_pCtx->m_bDrop < '0' || m_pCtx->m_bDrop > 'Z' ) return CCODE_ERROR | CCODE_HARD;

			if( m_pCtx->m_bDrop > '9' && m_pCtx->m_bDrop < 'A' ) return CCODE_ERROR | CCODE_HARD;

			m_pCtx->m_EMCached = 0;
			m_pCtx->m_MTCached = 0;
			m_pCtx->m_ESCached = 0;
			m_pCtx->m_uTimeout = 800;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CIrconDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CIrconDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = 0x1;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CIrconDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	SetpItem( LOBYTE(Addr.a.m_Offset) );

	switch ( m_pItem->uID ) {

		case CCE: // response to EM Write
			*pData = m_pCtx->m_EMCached;
			return 1;

		case CMT: // MT is write Only
		case CCM: // response to MT Write
			*pData = m_pCtx->m_MTCached;
			return 1;

		case CCS: // response to ES Write
			*pData = m_pCtx->m_ESCached;
			return 1;

		case ITIME: // User Adjustable Timeout
			*pData = m_pCtx->m_uTimeout;
			return 1;
		}

	if( m_pItem->bMod != M3ANDM5 ) {

		if( m_pItem->bMod != m_pCtx->m_bModel ) {

			*pData = 0;

			return 1;
			}
		}

//	if( IRCONDEBUG ) AfxTrace0("\r\n");

	PutCommand(NULL);

	if( Transact() ) {

		if( GetResponse( pData, Addr.a.m_Offset ) ) return 1;
		}

	m_ErrorCount = (m_ErrorCount+1) & 0xFF;

	m_ErrorCount += (Addr.a.m_Offset << 16);

	return CCODE_ERROR;
	}

CCODE MCALL CIrconDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	SetpItem( LOBYTE(Addr.a.m_Offset) );

	if( m_pItem->Type != READWRITE ) {

		return 1;
		}

	if( m_pItem->uID == ITIME ) {

		if( *pData >= 800 && *pData <= 3000 ) {

			m_pCtx->m_uTimeout = *pData;

			return 1;
			}
		}

	if( m_pItem->bMod != M3ANDM5 ) {

		if( m_pItem->bMod != m_pCtx->m_bModel ) {

			return 1;
			}
		}

//	if( IRCONDEBUG ) AfxTrace0("\r\n");

	PutCommand(pData);

	if( Transact() && CheckResponseHeader() ) {

		ProcessCached();

		return 1;
		}

	m_ErrorCount = (m_ErrorCount+1) & 0xFF;

	m_ErrorCount += (Addr.a.m_Offset << 16);
	
	return CCODE_ERROR;
	}

// PRIVATE METHODS


// Opcode Handlers

void CIrconDriver::PutCommand(PDWORD pData)
{
	StartFrame();

	AddCommand();

	if( pData ) AddData( *pData );
	}

// Frame Building

void CIrconDriver::StartFrame(void)
{
	m_wPtr = 0;
	AddByte( '#' );
	AddByte( m_pCtx->m_bDrop );
	AddByte( '0' ); // channel
	}

void CIrconDriver::EndFrame(void)
{
	AddByte( CR );
	}

void CIrconDriver::AddByte(BYTE bData)
{
	m_bTxBuff[m_wPtr++] = bData;
	}

void CIrconDriver::AddData(DWORD dData)
{
	DWORD uFactor = 10000;

	BYTE bNum;

	BOOL fFirst = FALSE;

	while ( uFactor ) {

		bNum = (dData/uFactor) % 10;

		if( bNum || fFirst ) {

			AddByte( m_pHex[bNum] );

			fFirst = TRUE;
			}

		uFactor /= 10;
		}

	if( !fFirst ) AddByte( '0' );
	}

void CIrconDriver::AddCommand( void )
{
	PutText( m_pItem->sName );
	}
	
void CIrconDriver::PutText(LPCTXT pCmd)
{
	for( UINT i=0; pCmd[i]; i++ )

		AddByte( BYTE(pCmd[i]) );
	}

// Transport Layer

BOOL CIrconDriver::Transact(void)
{
	Send();

	return GetReply();
	}

void CIrconDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put( m_bTxBuff[0] );
	}

BOOL CIrconDriver::GetReply(void)
{
	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uData;

	if( m_pItem->uID == CMT ) { // change per field recommendation

		SetTimer( m_pCtx->m_uTimeout + 1000 );
		}

	else {

		SetTimer( m_pCtx->m_uTimeout );
		}

	while( (uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

//		if( IRCONDEBUG ) AfxTrace1("<%2.2x>", LOBYTE(uData) );

		m_bRxBuff[uCount++] = LOBYTE(uData);

		if( uData == CR ) return TRUE;

		if( uCount > sizeof(m_bRxBuff) ) return FALSE;
		}	
 
	return FALSE;
	}

BOOL CIrconDriver::GetResponse( PDWORD pData, UINT uID )
{
	UINT uPos = 5;

	UINT j;

	if( !CheckResponseHeader() ) return FALSE;

	DWORD dResult = 0L;

	BOOL fFlag = FALSE;

	switch ( uID ) {

		case MVR: // VV.RR
			dResult =  1000 * (m_bRxBuff[5] -'0');
			dResult += 100  * (m_bRxBuff[6] -'0');
			dResult += 10   * (m_bRxBuff[8] -'0');
			dResult += m_bRxBuff[9] -'0';
			break;

		case MM6:
		case SNF:
			uPos += 4;
		case MM5:
		case SNE:
			uPos += 4;
		case MM4:
		case SND:
			uPos += 4;
		case MM3:
		case SNC:
			uPos += 4;
		case MM2:
		case SNB:
			uPos += 4;
		case MM1:
		case SN:

			for( j = 5; j < uPos; j++ ) {

				if( m_bRxBuff[j] == CR ) { // no chars
					*pData = 0x20202020;
					return TRUE;
					}
				}

			for( j = 0; j < 4; j++ ) {

				if( m_bRxBuff[uPos+j] == CR ) fFlag = TRUE;

				if( !fFlag ) dResult = ( dResult << 8 ) + m_bRxBuff[uPos+j];
				else dResult = (dResult << 8 ) + ' ';
				}
			break;

		default:
// have to handle both numeric results to skip C or F at end (Modline 5), or,
// return string characters, depending on the instruction.  Some devices return
// model string when Serial Number command is issued.

			dResult = MakeData();

			break;
		}

	*pData = dResult;

	return TRUE;
	}

BOOL CIrconDriver::CheckResponseHeader(void)
{
	for( UINT i = 0; i < 5; i++ ) {

		if( m_bRxBuff[i] != m_bTxBuff[i] ) return FALSE;
		}

	if( m_bRxBuff[5] == '?' ) return FALSE;

	return TRUE;
	}

// Port Access

void CIrconDriver::Put(BYTE b)
{
//	if( IRCONDEBUG ) for( UINT i=0;i<m_wPtr;i++ ) AfxTrace1("[%2.2x]", m_bTxBuff[i] );

	m_pData->Write( PCBYTE(m_bTxBuff), m_wPtr, FOREVER );
	}

UINT CIrconDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

void CIrconDriver::SetpItem(UINT uID)
{
	m_pItem = m_pCL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == m_pItem->uID ) return;

		m_pItem++;
		}
	}

BOOL CIrconDriver::IsDigit( BYTE b )
{
	return b >= '0' && b <= '9';
	}

void CIrconDriver::ProcessCached(void)
{
	PDWORD p = NULL;

	switch( m_pItem->uID ){

		case CEM:
			p = &(m_pCtx->m_EMCached); // save response to write
			break;

		case CMT:
			p = &(m_pCtx->m_MTCached); // save response to write
			break;

		case CES:
			p = &(m_pCtx->m_ESCached); // save response to write
			break;

		default:
			return;
		}

	*p = MakeData();
	}

DWORD CIrconDriver::MakeData(void)
{
	BOOL fFlag = FALSE;
	BOOL fNeg = FALSE;
	UINT uPos = 5;
	DWORD dResult = 0L;

	switch ( m_bRxBuff[uPos] ) {

		case '-':
			fNeg = 1;
			// fall through
		case ' ':
		case '+':
			uPos = 6;
			break;

		default : break;
		}

	fFlag = !IsDigit(m_bRxBuff[uPos]);

	while ( m_bRxBuff[uPos] != CR ) {

		if( !fFlag ) {

			if( IsDigit( m_bRxBuff[uPos] ) ) { // ignore non-numerics in normal responses

				dResult = (dResult*10) + m_bRxBuff[uPos] - '0';
				}

			else {

				if( m_bRxBuff[uPos] == ',' ) {

					if( m_pItem->uID == CCTS ) { // have temperature, so done

						return !fNeg ? dResult : -dResult;
						}

					if( m_pItem->uID == OPTS ) { // get status value

						dResult = 0L;

						fNeg = FALSE;

						if( m_bRxBuff[uPos+1] == '-' ) {

							fNeg = TRUE;

							uPos++;
							}
						}
					}
				}
			}
		else {
			dResult <<= 8;
			dResult += m_bRxBuff[uPos]; // last 4 non-numerics in buffer
			}

		uPos++;
		}

	if( m_pItem->uID == 12 || m_pItem->uID == OPTS ) { // treat as bits, only

		if( fNeg ) {

			dResult = (0x10000 - dResult) & 0xFFFF;

			return dResult;
			}
		}

	return fFlag ? dResult : (!fNeg ? dResult : -dResult);
	}

// End of File
