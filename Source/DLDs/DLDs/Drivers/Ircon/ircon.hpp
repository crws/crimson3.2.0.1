
//////////////////////////////////////////////////////////////////////////
//
// Ircon Driver
//

#define IRCON_ID	0x3352
#define addrNamed	0xF0
#define READONLY	0
#define READWRITE	1
#define M3ONLY		0
#define M3ANDM5		2
#define M5ONLY		1

// Opcodes with special functions
#define OPTS		11
#define MVR		13
#define MM1		14
#define MM2		47
#define MM3		48
#define MM4		49
#define MM5		50
#define MM6		51
#define	SN		42
#define	SNB		58
#define	SNC		59
#define	SND		60
#define	SNE		61
#define	SNF		62
#define	CCE		53
#define	CCM		54
#define	CCS		55
#define CCTS		56
#define C3TS		57
#define	CEM		2
#define CES		3
#define CMT		36
#define	ITIME		99		

struct FAR IRCONCmdDef {
	char 	sName[4];
	UINT	Type;
	UINT	uID;
	BYTE	bMod;
	};

class CIrconDriver : public CMasterDriver
{
	public:
		// Constructor
		CIrconDriver(void);

		// Destructor
		~CIrconDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			BYTE m_bModel;
			DWORD m_EMCached;
			DWORD m_MTCached;
			DWORD m_ESCached;
			UINT  m_uTimeout;
			};
		CContext *	m_pCtx;

		// Data Members
		BYTE  m_bTxBuff[32];
		BYTE  m_bRxBuff[32];
		WORD  m_wPtr;
		DWORD m_ErrorCount;
		//
		// List access
		static IRCONCmdDef CODE_SEG CL[];
		IRCONCmdDef FAR * m_pCL;
		IRCONCmdDef FAR * m_pItem;

		// Hex Lookup
		LPCTXT m_pHex;
	
		// Opcode Handlers
		void PutCommand(PDWORD pData);
		
		// Frame Building
		void StartFrame(void);
		void EndFrame(void);
		void AddByte(BYTE bData);
		void AddData(DWORD dData);
		void AddCommand( void );
		void PutText(LPCTXT pCmd);
		
		// Transport Layer
		BOOL Transact(void);
		void Send(void);
		BOOL GetReply(void);
		BOOL GetResponse(PDWORD pData, UINT uID);
		BOOL CheckResponseHeader(void);
		
		// Port Access
		void Put(BYTE b);
		UINT Get(UINT uTime);

		// Helpers
		void SetpItem(UINT uID);
		BOOL IsDigit( BYTE b );
		void ProcessCached(void);
		DWORD MakeData(void);

	};

// End of File
