
#include "intern.hpp"

#include "fam3tcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC TCP Master Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CFam3TCPMasterDriver);

// Constructor

CFam3TCPMasterDriver::CFam3TCPMasterDriver(void)
{
	
	}

// Destructor

CFam3TCPMasterDriver::~CFam3TCPMasterDriver(void)
{
	m_Ident = DRIVER_ID;
	
	m_pCtx = NULL;

	m_uKeep = 0;

	}

// Configuration

void MCALL CFam3TCPMasterDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CFam3TCPMasterDriver::Attach(IPortObject *pPort)
{

	}

void MCALL CFam3TCPMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CFam3TCPMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_uCpu		= GetByte(pData);
			m_pCtx->m_uMode		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;

	}

CCODE MCALL CFam3TCPMasterDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CFam3TCPMasterDriver::Ping(void)
{
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}

		return CFam3BaseMasterDriver::Ping();
		}

	return CCODE_ERROR;
	}

// Socket Management

BOOL CFam3TCPMasterDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CFam3TCPMasterDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {
		
			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {
					
					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}    
		}

	return FALSE;
	}

void CFam3TCPMasterDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

BOOL CFam3TCPMasterDriver::Transact(UINT uCmd)
{      
	if( SendFrame() && RecvFrame() ) {
		
		return CheckFrame(uCmd);
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CFam3TCPMasterDriver::SendFrame(void)
{
	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE; 
	}

// Overidables

BOOL CFam3TCPMasterDriver::CheckFrame(UINT uCmd)
{
	if( m_pCtx->m_uMode ) {

		if( m_bRxBuff[0] == '1' && UINT(m_bRxBuff[1] - 0x30) == m_pCtx->m_uCpu ) {
			
			return ( m_bRxBuff[2] == 'O' && m_bRxBuff[3] =='K' );
			}

		return FALSE;
		}

	return ( (m_bRxBuff[0] == (uCmd + 0x80) ) && m_bRxBuff[1] == 0 );
	}

BOOL CFam3TCPMasterDriver::RecvFrame(void)
{
	if( m_pCtx->m_uMode ) {

		return RecvAsciiFrame();
		}
	
	UINT uPtr  = 0;

	UINT uSize = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 4 ) {

				WORD wCount = MAKEWORD(m_bRxBuff[3], m_bRxBuff[2]);

				if( uPtr >= UINT(wCount + 4) ) {

					return TRUE;
					}
				}
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CFam3TCPMasterDriver::RecvAsciiFrame(void)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( m_bRxBuff[uPtr - 2] == CR && m_bRxBuff[uPtr - 1] == LF ) {

				return TRUE;
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CFam3TCPMasterDriver::Start(void)
{
	if( !OpenSocket() ) {

		return FALSE;
		}

	return CFam3BaseMasterDriver::Start();
	}

void CFam3TCPMasterDriver::AddPreamble(UINT uType, BOOL fWrite)
{
       	if( m_pCtx->m_uMode ) {

		return;
		}
	
	switch( uType ) {

		case addrBitAsBit:
		case addrBitAsByte:
		case addrBitAsWord:

			AddByte(fWrite ? CMD_BW : CMD_BR);

			break;

		case addrWordAsWord:
		case addrWordAsLong:
		case addrWordAsReal:

			AddByte(fWrite ? CMD_WW : CMD_WR);

			break;
		}
	}

void CFam3TCPMasterDriver::AddTerm(void)
{
	AddByte(CR);

	AddByte(LF);
	}

// End of File
