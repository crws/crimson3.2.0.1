#include "fam3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Fam3subishi Base TCP Driver
//

class CFam3TCPMasterDriver : public CFam3BaseMasterDriver
{
	public:
		// Constructor
		CFam3TCPMasterDriver(void);

		// Destructor
		~CFam3TCPMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:
		// Device Data
		struct CContext	: CFam3BaseMasterDriver::CBaseCtx
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			};

		// Data Members
		CContext * m_pCtx; 
		UINT	   m_uKeep;
		
		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Implementation
		BOOL SendFrame(void);
		BOOL Transact(UINT uCmd);
		BOOL RecvFrame(void);
		BOOL RecvAsciiFrame(void);
		BOOL CheckFrame(UINT uCmd);

		// Overridables
		BOOL Start(void);
       		void AddPreamble(UINT uType, BOOL fWrite);
		void AddTerm(void);
	};
		

// End of File

