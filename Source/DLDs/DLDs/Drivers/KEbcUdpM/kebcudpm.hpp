//////////////////////////////////////////////////////////////////////////
//
// Koyo UDP Master Driver
//

class CKEbcUdpM : public CMasterDriver
{
	public:
		// Constructor
		CKEbcUdpM(void);

		// Destructor
		~CKEbcUdpM(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		UINT	   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL Transact(void);
		
		// Frame Building
		void StartHeader(void);
		void AddCommand(UINT uCmd);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
	
		// Implementation
		void FindSlot(UINT uSlot, UINT uTable);
		UINT ParseData(AREF Addr, PDWORD pData, UINT uCount);
		void SetData(AREF Addr, PDWORD pData, UINT uCount, PBYTE pBytes);
		void SetDiscreteData(UINT uOffset, PDWORD pData, UINT uCount, PBYTE pBytes);
		void SetAnalogData(UINT uOffset, PDWORD pData, UINT uCount, PBYTE pBytes);
		void SetAnalogLongData(UINT uOffset, PDWORD pData, UINT uCount, PBYTE pBytes);
		UINT GetDiscreteData(UINT uOffset, UINT uPtr, PDWORD pData, UINT uCount);
		UINT GetAnalogData(UINT uOffset, UINT uPtr, PDWORD pData, UINT uCount);
		UINT GetAnalogLongData(UINT uOffset, UINT uPtr, PDWORD pData, UINT uCount);
		void SetByteCount(void);

		// Helpers
		BOOL IsWritable(UINT uTable);

	};

// End of File
