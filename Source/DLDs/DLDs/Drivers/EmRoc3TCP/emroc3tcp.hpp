
#include "../EmRoc3/emroc3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC 3 Communications Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Master TCP Driver Number 3 - Enhanced
//

class CEmRoc3MasterTCPDriver : public CEmRoc3MasterBaseDriver
{
	public:
		// Constructor
		CEmRoc3MasterTCPDriver(void);

		// Destructor
		~CEmRoc3MasterTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);


		
	protected:

		// Device Context
		struct CContext : public CBaseCtx
		{
			DWORD	 m_IP1;
			DWORD    m_IP2;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			ISocket *m_pSock;
			BOOL     m_fDirty;
			BOOL     m_fAux;
			};

		// Data Members
		CContext * m_pCtx;
		UINT       m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Transport Layer
		BOOL	Send(void);
		BOOL	Recv(void);

		// Implementation
		BOOL CheckFrame(void);
		BOOL CheckReply(void);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
	};

// End of file
