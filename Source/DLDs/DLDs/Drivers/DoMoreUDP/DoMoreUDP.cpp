
//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC UDP Driver
// 

#include "intern.hpp"

#include "DoMoreUDP.hpp"

// Shared code

#include "../DoMoreSerial/DoMoreBase.cpp"

#include "../DoMoreSerial/MxAppProtocol.cpp"

// Instantiator

INSTANTIATE(CDoMoreUDPDriver);

// Constructor

CDoMoreUDPDriver::CDoMoreUDPDriver(void)
{
	m_Ident	= DRIVER_ID;

	m_pCtx	= NULL;

	m_uPtr	= 0;
	}

// Destructor

CDoMoreUDPDriver::~CDoMoreUDPDriver(void)
{
	}

// Configuration

void CDoMoreUDPDriver::Load(LPCBYTE pData)
{
	}

void CDoMoreUDPDriver::CheckConfig(CSerialConfig &Config)
{
	}

// Management

void CDoMoreUDPDriver::Attach(IPortObject *pPort)
{
	}

void CDoMoreUDPDriver::Open(void)
{
	}
		
// Device

CCODE CDoMoreUDPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;
		
			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->m_uPingReg	= GetLong(pData);			
			m_pCtx->m_fUsePing	= GetByte(pData);
			m_pCtx->m_pPassword	= GetString(pData);

			m_pCtx->m_wSession	= 0;			
			m_pCtx->m_fSessionOpen	= FALSE;
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;		

			m_pBaseCtx = m_pCtx;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBaseCtx = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE CDoMoreUDPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		Free(m_pCtx->m_pPassword);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE CDoMoreUDPDriver::Ping (void)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	return CDoMoreBaseDriver::Ping();
	}

// Transport Layer

BOOL CDoMoreUDPDriver::SendFrame(void)
{
	if( !CheckSocket() ) {

		return FALSE;
		}

	UINT uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {
			
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CDoMoreUDPDriver::RecvFrame(void)
{
	if( !CheckSocket() ) {

		return FALSE;
		}

	SetTimer(m_pCtx->m_uTime2);

	CBuffer *pBuff = NULL;

	while( GetTimer() ) {

		if( m_pCtx->m_pSock->Recv(pBuff) == S_OK ) {

			PBYTE pData = pBuff->GetData();

			WORD wTrans = IntelToHost(*PU2(pData + 3));
			
			if( CheckTransID(wTrans) ) {
					
				if( pData[9] == hapReply ) {

					WORD wSize = IntelToHost(*PU2(pData + 11));

					memcpy(m_bRxBuff, pData + 13, wSize);

					pBuff->Release();

					return TRUE;
					}
				}
			else {
				pBuff->Release();

				continue;
				}
			}

		if( !CheckSocket() ) {

			break;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CDoMoreUDPDriver::RecvCheckAck(void)
{
	if( !CheckSocket() ) {

		return FALSE;
		}

	CBuffer *pBuff = NULL;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		if( m_pCtx->m_pSock->Recv(pBuff) == S_OK ) {

			PBYTE pData = pBuff->GetData();

			WORD wTransID = IntelToHost(*PU2(pData + 3));

			if( CheckTransID(wTransID) ) {

				BYTE bAckNack = pData[9];

				pBuff->Release();

				return bAckNack == hapACK;
				}

			pBuff->Release();

			return FALSE;
			}

		if( !CheckSocket() ) {

			break;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CDoMoreUDPDriver::CheckTransID(WORD wTransID)
{
	return (m_pCtx->m_wTrans - 1) == wTransID;
	}

BOOL CDoMoreUDPDriver::Transact(void)
{
	if( SendFrame() && RecvCheckAck() && RecvFrame() ) {

		return TRUE;
		}

	CloseSocket(TRUE);

	return FALSE;
	}

// Frame Building

void CDoMoreUDPDriver::StartHeader(void)
{
	m_uPtr = 0;

	AddByte(0x48);

	AddByte(0x41);

	AddByte(0x50);
	
	AddWord(m_pCtx->m_wTrans++);

	// NOTE -- The checksum isn't needed over UDP,
	// so we set it to 0 and the PLC should ignore it.

	AddWord(0x0000);
	}


BOOL CDoMoreUDPDriver::StartRequest(CMxAppRequest &Req)
{
	StartHeader();

	PBYTE pBuff = new BYTE[ BUFF_SIZE ];

	memset(pBuff, 0, BUFF_SIZE);

	WORD wLen = Req.Encode(pBuff, BUFF_SIZE);

	if( wLen > 0 ) {

		AddWord(wLen + 2);

		AddByte(0x1D);

		AddByte(0x00);

		AddWord(wLen);

		AddData(pBuff, wLen);

		if( Transact() ) {

			delete [] pBuff;

			return TRUE;
			}
		}

	delete [] pBuff;

	return FALSE;
	}

// Socket Management

BOOL CDoMoreUDPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CDoMoreUDPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_UDP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		m_pCtx->m_pSock->SetOption(OPT_RECV_QUEUE, sizeof(m_bRxBuff));
		
		if( m_pCtx->m_pSock->Connect(IP, 0x7070, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10); 
				} 

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CDoMoreUDPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
