
//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Microscan Concentrator Driver
//

class CMicroscan : public CMasterDriver
{
	public:
		// Constructor
		CMicroscan(void);

		// Destructor
		~CMicroscan(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Ping   (void);
		DEFMETH(CCODE) Read   (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write  (AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	   m_bDrop;
			BOOL	   m_fOnline;
			UINT	   m_uCmdCount;
			UINT	   m_uCmdCode;
			UINT	   m_uCmdSeq;
			UINT	   m_uCmdLast;
			PCTXT	   m_pCmdList;
			UINT	   m_uCmdIndex;
			UINT	   m_uCount;
			UINT	   m_uTime;
			UINT	   m_uFract;
			char	   m_sCode[600];
			char	   m_sRecv[600];
			char	   m_sSend[4096];
			BYTE	   m_bPoll;
			BYTE	   m_bSelect;
			CContext * m_pNext;
			CContext * m_pPrev;
			};

		// Device Context		
		CContext * m_pCtx;
		CContext * m_pHead;
		CContext * m_pTail;

		// Config Data
		BOOL		m_fDebug;
		BOOL		m_fEnable;
		UINT		m_uRawPort;
		UINT		m_uProtocol;
		BOOL		m_fLRC;
		BYTE		m_bDateFmt;
		BYTE		m_bTimeFmt;
		char		m_sSep[2];
		BYTE		m_fAppend;
		PTXT		m_pTxOpen;
		PTXT		m_pTxClose;
		BOOL		m_fTxDrop;
		BYTE		m_bRxTerm;
		UINT		m_uRxTime;
		UINT		m_uTxTime;
		BOOL		m_fAckNak;
		BOOL		m_fReqRes;
		BOOL		m_fDuplex;
		UINT		m_uRetries;
		BYTE		m_STX;
		BYTE		m_ETX;
		BYTE		m_ACK;
		BYTE		m_NAK;
		BYTE		m_REQ;
		BYTE		m_RES;

		// Data Members
		IExtraHelper  * m_pExtra;
		ICommsRawPort * m_pHost;
		PBYTE	        m_pHostData;
		UINT		m_uHostSize;
		UINT		m_uHostHead;
		UINT		m_uHostTail;
		char		m_sData[512];
		char		m_sRecv[512];
		char		m_sSend[1024];
		BYTE		m_bTemp[1024];
		UINT		m_uRxState;
		UINT		m_uRxPtr;
		UINT		m_uRxLast;
		UINT		m_uTxState;
		UINT		m_uTxPtr;
		UINT		m_uTxLast;
		UINT		m_uTxLeft;

		// Reader Communications
		BOOL ServiceReader(void);
		BOOL ReaderPollSequence(void);
		BOOL ReaderPoll(void);
		BOOL ReaderSend(BOOL fHost);
		BOOL ReaderGetSelectAck(void);
		void ReaderNotOnline(void);

		// Reader Commands
		BOOL ReaderHasHostCmd(void);
		BOOL ReaderHasAutoCmd(void);
		BOOL SendReaderHostCmd(void);
		BOOL SendReaderAutoCmd(void);
		BOOL SendReaderCmd(PCSTR pSend, UINT uCount);
		BOOL QueueReaderHostCmd(UINT uDrop, PCTXT pSend);
		void StripReaderHostCmd(void);
		void EmptyReaderHostCmd(void);

		// Reader Frame Parsing
		void ExtractReaderData(void);
		void ExtractReaderCode(void);

		// Reader Comms Primitives
		void ReaderSendPoll(void);
		void ReaderSendSelect(void);
		void ReaderSendAck(void);
		void ReaderSendNak(void);
		void ReaderSendRes(void);

		// Reader Port Access
		void ReaderWrite(PCBYTE pData, UINT uCount);

		// Host Communications
		void ServiceHost(void);
		void HostSend(void);
		UINT HostSend(PCBYTE pSend, UINT uSend);
		UINT HostSendPointToPoint(PCBYTE pSend, UINT uSend);
		UINT HostSendAckNak(PCBYTE pSend, UINT uSend);
		void HostRead(void);
		BOOL HostRead(UINT uData);
		BOOL HostReadPointToPoint(UINT uData);
		BOOL HostReadAckNak(UINT uData);
		void ParseHostLine(void);
		BOOL FindHostPort(void);

		// Ack-Nak Support
		void AddAckNakByte(BYTE bData);
		void AddAckNakData(PCBYTE pData, UINT uSize);
		void AckNakSendAck(void);
		void AckNakSendNak(void);
		void AckNakSendRes(void);
		void AckNakResetTx(void);

		// Relaying to Host
		void RelayDataToHost(UINT uSize);
		void RelayCodeToHost(void);

		// Local Commands
		BOOL LocalCommand(PCTXT pCmd);

		// Host Frame Building
		void StartHostFrame(BYTE bDrop);
		void HostAppend(PCTXT pText);
		void HostPrintf(PCTXT pText, ...);
		BOOL QueueHostFrame(void);
		BOOL FinishHostFrame(UINT &uSize);

		// Time Stamping
		BOOL  AppendMark(PTXT pText, BOOL fNow);
		BOOL  AppendTime(PTXT pText, BYTE bFormat, DWORD dwTime, UINT uFract);
		BOOL  AppendDate(PTXT pText, BYTE bFormat, DWORD dwTime);
		PCTXT GetDayName(DWORD dwTime);
		DWORD GetNow(void);

		// Firmware Checksums
		WORD FindChecksums(void);
		WORD FindChecksum1(void);
		WORD FindChecksum2(void);

		// Debug Support
		BOOL Debug(PCTXT pText, ...);
	};

// End of File
