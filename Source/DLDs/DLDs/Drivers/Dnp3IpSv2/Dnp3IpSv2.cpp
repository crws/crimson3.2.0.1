
#include "Dnp3IpSv2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Tcp Slave Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CDnp3IpSlaveV2);

// Constructor

CDnp3IpSlaveV2::CDnp3IpSlaveV2(void)
{
	m_Ident   = DRIVER_ID;
	}

// End of File

