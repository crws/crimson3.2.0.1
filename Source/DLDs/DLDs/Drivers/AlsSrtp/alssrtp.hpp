#include "srtpmtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom Alsp SNP Driver
//

class CAlstomSrtpTCPMaster : public CGeSrtpTCPMaster
{
	public:
		// Constructor
		CAlstomSrtpTCPMaster(void);

		// Destructor
		~CAlstomSrtpTCPMaster(void);
	};

// End of File
