//////////////////////////////////////////////////////////////////////////
//
// Acroloop Motion Controller Spaces
//

#define	SPACE_PU	1
#define	SPACE_P		2
#define	SPACE_BF	3

//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop Motion Controller Driver
//

class CAcroloopDriver : public CMasterDriver
{
	public:
		// Constructor
		CAcroloopDriver(void);

		// Destructor
		~CAcroloopDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		
		// Device Data
		struct CBaseCtx
		{
			BYTE  m_bDevice;
			WORD  m_wGVA_Hi;
			WORD  m_wGVA_Lo;
			WORD  m_wDimGV;
			};

	protected:

		CBaseCtx * m_pBase;

		UINT  m_uTxSize;
		UINT  m_uRxSize;
		BYTE  m_bTx[64];
		BYTE  m_bRx[64];
		UINT  m_uPtr;
		BOOL  m_fAwake;
		WORD  m_wGVA_Hi;
		WORD  m_wGVA_Lo;
		WORD  m_wDimGV;
		
		// Transport Layer
		BOOL PutFrame(void);
	virtual	BOOL BinaryTx(void);
		BOOL BinaryRx(void);
		BOOL GetFrame(void);
	virtual	BOOL Transact(WORD CheckBytes);
		BOOL CheckReply(WORD CheckBytes);
		void Encode(PBYTE pTx, UINT &n);
		void Decode(UINT n);

		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		
		// Implementation
	virtual	BOOL IsAwake(AREF Addr);
		BOOL PeekGVA(void);
		BOOL PeekDimGV(void);
	virtual UINT GetSystemPointer(void);
	virtual UINT GetPeek(UINT uIndex);
	virtual void FixupDataIn(DWORD &dwData);
	virtual void FixupDataOut(DWORD &dwData);
		void Convert64To32(DWORD &dwData);
		void Convert32To64(DWORD &dwData);
		
		// Comms Handlers
		CCODE ReadUserParam(AREF Addr, PDWORD pData, UINT uCount);
		CCODE WriteUserParam(AREF Addr, PDWORD pData, UINT uCount);
		CCODE ReadSysParam(AREF Addr, PDWORD pData, UINT uCount);
		CCODE WriteSysParam(AREF Addr, PDWORD pData, UINT uCount);
		CCODE WriteBit(AREF Addr, PDWORD pData, UINT uCount);

		
	};

// End of File
