
#include "intern.hpp"

#include "kebdin2.hpp"

//*!*/ Reverse //*!*/ and /*!*/ comment format if writing of commands 2,3,4,5,6,7,10,11 and 12 is wanted.
//*!*/ The code has been checked, but returns "Service Not Available" when sent to the KEB.
//*!*/ The referenced commands have been similarly commented in the Config

//////////////////////////////////////////////////////////////////////////
//
// KEB DIN66019II Driver
//

// Instantiator

INSTANTIATE(CKEBDIN2Driver);

// Constructor

CKEBDIN2Driver::CKEBDIN2Driver(void)
{
	m_Ident = DRIVER_ID; // 339B
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;

	m_Error	= 0L;

	m_uIID  = 15;

	m_fS12ALoaded = FALSE;
	}

// Destructor

CKEBDIN2Driver::~CKEBDIN2Driver(void)
{
	}

// Configuration

void MCALL CKEBDIN2Driver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CKEBDIN2Driver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uPhysical == 3 ) Make485(Config, TRUE);
	}
	
// Management

void MCALL CKEBDIN2Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CKEBDIN2Driver::Open(void)
{
	}

// Device

CCODE MCALL CKEBDIN2Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			memset( m_R48, 0, sizeof(m_R48) );
			memset( m_W48, 0, sizeof(m_W48) );
			memset( m_R49, 0, sizeof(m_R49) );
			memset( m_W49, 0, sizeof(m_W49) );

			if( !m_fS12ALoaded ) m_pCtx->m_S12[0] = 0xFFFFFFFF;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKEBDIN2Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CKEBDIN2Driver::Ping(void)
{
//*!*/	m_pCtx->m_S12D[SZS12D - 1] = 0;

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 0xF0;
	Addr.a.m_Offset = 0x8200;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrLongAsLong;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CKEBDIN2Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( ReadNoTransmit( Addr.a.m_Table, Addr.a.m_Offset, pData, uCount ) ) {

		case RNTDATA:	Sleep(10);	return 1;

		case RNTDONE:	Sleep(10);	return CCODE_NO_RETRY;

		case RNTARR:	Sleep(10);	return uCount;

		default:			break;
		}

	if( m_pCtx->m_bDrop >= 0xF0 ) { // can't multicast/broadcast read

		return 1;
		}

	m_uTable = Addr.a.m_Table;

	UINT uCt = 1;

	switch( m_uTable ) {

		case CM6:
		case CM11:
		case R12D:
		case R16A:
		case R17A:
			uCt = uCount;
			break;
		}

	if( Read( Addr ) ) {

		switch( Transact() ) {

			case CFAIL:
				return CCODE_ERROR;

			case COK:
				return GetResponse( pData, Addr, uCt ) ? uCt : CCODE_ERROR;

			case CERROR:
				m_Error = ((DWORD)(m_bRx[1]) << 16) + m_bTx[3];

				return CCODE_NO_RETRY;					
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CKEBDIN2Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
// NOTE: Removed Writing capablility in Config from all commands that returned "Service Not Available"
//**/	AfxTrace3("\r\n**** WRITE **** %d %d %d ", Addr.a.m_Table, Addr.a.m_Offset, uCount );

	if( WriteNoTransmit( Addr.a.m_Table, Addr.a.m_Offset, pData, uCount ) ) {

		Sleep(10);

		return uCount;
		}

//**/	AfxTrace0(" **");

	m_uTable = Addr.a.m_Table;

	UINT uCt = 1;

//*!*/	switch( m_uTable ) {

//*!*/		case CM6:
//*!*/		case CM11:
//*!*/		case R12D:
//*!*/			uCt = uCount;
//*!*/			break;
//*!*/		}

	switch( WriteKEB(Addr, pData, uCt) ) {

		case CFAIL:
			return CCODE_ERROR;

		case COK:
			return uCt;

		case CERROR:
			m_Error = ((DWORD)(m_bRx[1]) << 16) + m_bTx[4];

			return CCODE_NO_RETRY;
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

// Opcode Handlers

BOOL CKEBDIN2Driver::Read(AREF Addr)
{
	StartFrame(FALSE);

	AddParameter(Addr, 0, FALSE);

	EndFrame(FALSE);

	return TRUE;
	}

UINT CKEBDIN2Driver::WriteKEB(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(TRUE);

	DWORD d = PU4(pData)[0];

	AddParameter(Addr, d, TRUE);

	AddWriteData(Addr, pData, uCount);

	EndFrame(TRUE);

	UINT RtnVal = Transact();

	if( RtnVal == COK && (Addr.a.m_Table == S48 || Addr.a.m_Table == S49) ) {

		if( !GetResponse( pData, Addr, uCount ) ) RtnVal = CFAIL;
		}

	return RtnVal;
	}

// Header Building

void CKEBDIN2Driver::StartFrame(BOOL fIsWrite)
{
	BYTE bService = GetServiceByte();

	m_uPtr = 0;

	AddByte( EOT );

	AddData( m_pCtx->m_bDrop, 0x10 );

	if( fIsWrite ) AddByte( STX );

	m_uCheck = 0;

	AddByte( bService );

	if( m_uIID >= 15 ) m_uIID = 0;

	AddByte( m_pHex[ LOBYTE(++m_uIID) ] );
	}

void CKEBDIN2Driver::EndFrame(BOOL fIsWrite)
{
	AddByte( fIsWrite ? ETX : ENQ );

	AddByte( m_uCheck >= 0x20 ? m_uCheck : m_uCheck += 0x20 );
	}

void CKEBDIN2Driver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;

	m_uCheck ^= bData;
	}

void CKEBDIN2Driver::AddData(DWORD dData, DWORD dFactor)
{
	while( dFactor ) {

		AddByte( m_pHex[ (dData/dFactor) % 16 ] );

		dFactor /= 16;
		}
	}

void CKEBDIN2Driver::AddParameter( AREF Addr, DWORD dData, BOOL fIsWrite )
{
	if( Addr.a.m_Offset & 0x8000 ) {

		AddData( Addr.a.m_Offset & 0x7FFF, 0x1000 );

		if( !fIsWrite ) AddData( 1 << Addr.a.m_Extra, 0x10 ); // add Set number immediately

		else {
			if( Addr.a.m_Table == CM6 || Addr.a.m_Table == CM11 ) AddByte( '1' );
			}
		return;
		}

	switch( Addr.a.m_Table ) {

		case R16A:
		case R17A:
		case S16W:
		case S17W:
//*!*/		case S18W:
		case S48:
		case S49:
			return;

		case R12B:
		case R12C:
		case R12D:
			AddData( Addr.a.m_Offset, 0x1000 );
			AddData( m_pCtx->m_S12[0], 0x10 );
			AddData( 1, 0x10 );
			return;

//*!*/		case S2W:
//*!*/		case S4W:
//*!*/		case S5W:
//*!*/		case S7W:
//*!*/		case S10W:
//*!*/		case S12W:
//*!*/			AddData( dData, 0x1000 ); // Parameter address in data
//*!*/			return;

		default:
			AddData( Addr.a.m_Offset, 0x1000 ); // Parameter address in config
			return;
		}

	return;
	}

void CKEBDIN2Driver::AddWriteData( AREF Addr, PDWORD pData, UINT uCount )
{
	UINT i;
	UINT uLen;

	DWORD dData = PU4(pData)[0];

	switch( GetServiceByte() ) {

		case 'G':
			AddData( dData, 0x10000000 );
			AddData( 1 << Addr.a.m_Extra, 0x10 );
			return;

		case 'W':
			AddData( m_pCtx->m_S16[0], 0x10000000 );
			AddData( m_pCtx->m_S16[1], 0x10000000 );
			return;

		case 'X':
			for( i = 0; i < 4; i++ ) AddData( m_pCtx->m_S17[i], 0x1000 );
			return;

		case 'w':
			AddData( m_W48[0], 0x10000000 );
			AddData( m_W48[1], 0x10000000 );
			return;

		case 'x':
			for( i = 0; i < 4; i++ ) AddData( m_W49[i], 0x1000 );
			return;

//*!*/		case 'I':
//*!*/			AddData( m_pCtx->m_S02[0], 0x10000000 );
//*!*/			AddData( m_pCtx->m_S02[1], 0x10000000 );
//*!*/			return;

//*!*/		case 'J':
//*!*/			AddData( dData, 0x10000000 );
//*!*/			return;

//*!*/		case 'K':
//*!*/			AddData( m_pCtx->m_S04[0], 0x10000000 );
//*!*/			AddData( m_pCtx->m_S04[1], 0x10000000 );
//*!*/			return;

//*!*/		case 'L':
//*!*/			for( i = 0; i < 4; i++ ) AddData( m_pCtx->m_S05[i], 0x1000 );
//*!*/			return;

//*!*/		case 'M':
//*!*/		case 'R':
//*!*/			UINT uPtr;
//*!*/			UINT uShift;

//*!*/			char cc[64];

//*!*/			memset( cc, 0, 64 );

//*!*/			uLen = 0;

//*!*/			for( uPtr = 0; uPtr < uCount; uPtr++ ) {

//*!*/				uShift = 32;

//*!*/				BYTE b;

//*!*/				b = 0;

//*!*/				while( uShift ) {

//*!*/					uShift -= 8;

//*!*/					b = pData[uPtr] >> uShift;

//*!*/					cc[uLen] = b;

//*!*/					if( b ) uLen++;

//*!*/					if( !b || uLen > 62 ) uCount = 0;
//*!*/					}
//*!*/				}

//*!*/			if( !uLen ) {

//*!*/				AddData( 1, 0x10 );
//*!*/				AddByte( ' ' );
//*!*/				return;
//*!*/				}

//*!*/			AddData( uLen, 0x10 );

//*!*/			for( uPtr = 0; uPtr < uLen; uPtr++ ) AddByte( cc[uPtr] );

//*!*/			return;

//*!*/		case 'N':
//*!*/			for( i = 0; i < 4; i++ ) AddData( m_pCtx->m_S07[i], 0x1000 );
//*!*/			return;

//*!*/		case 'Q':
//*!*/			DWORD dText;

//*!*/			AddData( m_pCtx->m_S10[0], 0x10 );

//*!*/			dText = m_pCtx->m_S10[1];

//*!*/			AddByte( dText & 0xFF0000 ? dText >> 16 : ' ');
//*!*/			AddByte( dText & 0x00FF00 ? dText >>  8 : ' ');
//*!*/			AddByte( dText & 0x0000FF ? dText       : ' ');
//*!*/			AddData( m_pCtx->m_S10[2], 0x1000 );
//*!*/			AddData( m_pCtx->m_S10[3], 0x10 );
//*!*/			AddData( m_pCtx->m_S10[4], 0x10 );
//*!*/			return;

//*!*/		case 'S':
//*!*/			AddData( m_pCtx->m_S12[0], 0x10 );
//*!*/			AddByte( '0' );
//*!*/			AddData( m_pCtx->m_S12[1], 0x10000000 );
//*!*/			AddData( m_pCtx->m_S12[2], 0x10 );

//*!*/			uLen = min( strlen( m_pCtx->m_S12D ), sizeof( m_pCtx->m_S12D ) - 1 );

//*!*/			if( !uLen ) {

//*!*/				uLen = 1;
//*!*/				m_pCtx->m_S12D[0] = ' ';
//*!*/				}

//*!*/			AddData( uLen, 0x10 );

//*!*/			for( i = 0; i < uLen; i++ ) AddByte( m_pCtx->m_S12D[i] );

//*!*/			return;

//*!*/		case 'Y':
//*!*/			AddData( m_pCtx->m_S18[0], 0x1000 );
//*!*/			AddData( m_pCtx->m_S18[1], 0x10000000 );
//*!*/			AddData( m_pCtx->m_S18[2], 0x10000000 );
//*!*/			AddData( m_pCtx->m_S18[3], 0x1000 );
//*!*/			AddData( m_pCtx->m_S18[4], 0x1000 );
//*!*/			return;
		}
	}

// Transport Layer

UINT CKEBDIN2Driver::Transact(void)
{
	Send();

	return GetReply();
	}

void CKEBDIN2Driver::Send(void)
{
	m_pData->ClearRx();

	Put();
	}

UINT CKEBDIN2Driver::GetReply(void)
{
	UINT uPtr = 0;

	UINT uTimer = 1000;

	UINT uState = 0;

	WORD wData;

	m_bRx[0] = 0;

//**/	AfxTrace0("\r\n");

	SetTimer(uTimer);

	while( GetTimer() ) {

		if( ( wData = Get(uTimer)) == LOWORD(NOTHING) ) {

			continue;
			}

		uTimer = 1000;

		BYTE bData = LOBYTE(wData);

//**/		AfxTrace1("<%2.2x>", bData);

		switch( uState ) {

			case 0:
				m_bRx[uPtr++] = bData;

				switch( bData ) {

					case ETX:
						uState = 1;
						break;
					
					case ACK: // Write success

						if( m_bRx[0] == m_pHex[m_uIID] ) return COK;
						break;

					case EOT: // Read Error

						if( m_bRx[0] == m_pHex[m_uIID] ) return GetErrorControl();
						break;

					case NAK: // Write Error

						if( m_bRx[0] == m_pHex[m_uIID] ) return GetErrorControl();
						break;
					}
				break;

			case 1:
				if( m_bRx[0] == STX ) {

					if( m_bRx[RXIID] == m_pHex[m_uIID] ) {

						UINT uCheck;

						uCheck = 0;

						for( UINT i = 1; i < uPtr; i++ ) uCheck ^= m_bRx[i];

						if( uCheck < 0x20 ) uCheck += 0x20;

						return (LOBYTE(uCheck) == bData) ? COK : CFAIL;
						}
					}

				uState = 0;

				break;
			}

		if( uPtr >= sizeof(m_bRx) ) return 0;
		}

	return 0;
	}

BOOL CKEBDIN2Driver::GetResponse( PDWORD pData, AREF Addr, UINT uCount )
{
	UINT uTable = Addr.a.m_Table;
	UINT RxPos;
	UINT uPtr;

	if( uTable >= addrNamed ) {

		*pData = GetData( 3, 8 );

		return TRUE;
		}

	switch( uTable ) {

		case R2A:
		case S3A:
		case R4A:
		case R12B:	*pData = GetData( 3, 8 );	return TRUE;

		case R2B:
		case R4B:	*pData = GetData( 11, 8 );	return TRUE;

		case R5A:
		case R7A:
		case R18A:	*pData = GetData( 3, 4 );	return TRUE;

		case R5B:
		case R7B:	*pData = GetData( 7, 4 );	return TRUE;

		case R5C:
		case R7C:	*pData = GetData( 11, 4 );	return TRUE;

		case R5D:
		case R7D:	*pData = GetData( 15, 4 );	return TRUE;

		case R10A:	*pData = GetData( 3, 2 );	return TRUE;

		case R10C:	*pData = GetData( 8, 4 );	return TRUE;

		case R12C:	*pData = GetData( 11, 2 );	return TRUE;

		case R10D:	*pData = GetData( 12, 2 );	return TRUE;

		case R10E:	*pData = GetData( 14, 2 );	return TRUE;

		case R18B:	*pData = GetData( 7, 8 );	return TRUE;

		case R18C:	*pData = GetData( 15, 8 );	return TRUE;

		case R18D:	*pData = GetData( 23, 4 );	return TRUE;

		case R18E:	*pData = GetData( 27, 4 );	return TRUE;

		case R10B:
			*pData = (m_bRx[5]<<24) + (m_bRx[6]<<16) + (m_bRx[7]<<8);
			return TRUE;

		case R16A:
		case R17A:
			UINT uSize;

			uSize = uTable == R16A ? 8 : 4;

			RxPos = 3 + ((Addr.a.m_Offset-1) * uSize);

			for( uPtr = 0; uPtr < uCount; uPtr++, RxPos += uSize ) {

				pData[uPtr] = GetData( RxPos, uSize );
				}

			return TRUE;

		case CM6:
		case CM11:
		case R12D:

			DWORD dStrLen;
			DWORD dStrDat;
			UINT  uTableStart;
			UINT  uEnd;
			UINT  uShift;

			RxPos = Addr.a.m_Table == R12D ? 15 : 5;

			dStrLen = GetData( RxPos-2, 2 );

			if( dStrLen ) {

				uEnd = RxPos + min( 4 * uCount, dStrLen );

				uPtr = 0;

				BYTE b;

				while( uPtr < uCount ) {

					dStrDat = 0;

					uShift  = 32;

					while( uShift ) {

						uShift -= 8;

						b = RxPos < uEnd ? m_bRx[RxPos++] : 0;

						dStrDat += DWORD(b) << uShift;
						}

					pData[uPtr++] = dStrDat;
					}
				}

			else memset( pData, 0x20, 4 * uCount );

			return TRUE;

		case S48:
			m_R48[0] = GetData(  3, 8 );
			m_R48[1] = GetData( 11, 8 );
			m_R48[2] = GetDataValue(m_bRx[19]);
			return TRUE;

		case S49:
			m_R49[0] = GetData(  3, 4 );
			m_R49[1] = GetData(  7, 4 );
			m_R49[2] = GetData( 11, 4 );
			m_R49[3] = GetData( 15, 4 );
			m_R49[4] = GetDataValue(m_bRx[19]);
			return TRUE;
		}

	return FALSE;
	}

// Port Access

void CKEBDIN2Driver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

UINT CKEBDIN2Driver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

BYTE CKEBDIN2Driver::GetServiceByte(void)
{
	if( m_uTable >= addrNamed ) return 'G';

	switch( m_uTable ) {

//*!*/		case S2W:
		case R2A:
		case R2B:	return 'I';

		case S3A:	return 'J';

//*!*/		case S4W:
		case R4A:
		case R4B:	return 'K';

//*!*/		case S5W:
		case R5A:
		case R5B:
		case R5C:
		case R5D:	return 'L';

		case CM6:	return 'M';

//*!*/		case S7W:
//*!*/		case W7A:
//*!*/		case W7B:
//*!*/		case W7C:
//*!*/		case W7D:
		case R7A:
		case R7B:
		case R7C:
		case R7D:	return 'N';

//*!*/		case S10W:
//*!*/		case W10A:
//*!*/		case W10B:
//*!*/		case W10C:
//*!*/		case W10D:
//*!*/		case W10E:
		case R10A:
		case R10B:
		case R10C:
		case R10D:
		case R10E:	return 'Q';

		case CM11:	return 'R';

//*!*/		case S12W:
		case W12A:
//*!*/		case W12B:
//*!*/		case W12C:
//*!*/		case W12D:
		case R12B:
		case R12C:
		case R12D:	return 'S';

		case R16A:
		case S16W:	return 'W';

		case R17A:
		case S17W:	return 'X';

//*!*/		case S18W:
//*!*/		case W18A:
//*!*/		case W18B:
//*!*/		case W18C:
//*!*/		case W18D:
//*!*/		case W18E:
		case R18A:
		case R18B:
		case R18C:
		case R18D:
		case R18E:	return 'Y';

		case S48:	return 'w';

		case S49:	return 'x';
		}

	return 'G';
	}

UINT CKEBDIN2Driver::ReadNoTransmit( UINT uTable, UINT uOffset, PDWORD pData, UINT uCount )
{
	PDWORD pD;

	uOffset -= 1; // arrays are configured to start at address 1

	switch( uTable ) {

		case SERR:	
			pData[0] = m_Error;
			return RNTDATA;

		case R12B:
		case R12C:
		case R12D:
			if( !m_fS12ALoaded || m_pCtx->m_S12ALoaded != 0x12345678 ) {

				m_pCtx->m_S12ALoaded = 0xFFFFFFFF;

				m_fS12ALoaded = FALSE;

				return RNTDONE;
				}
			break;

		case W12A:
//*!*/		case W12B:
//*!*/		case W12C:
			pData[0] = m_pCtx->m_S12[uTable-W12A];

			return RNTDATA;

		case W16A:
			pD = &m_pCtx->m_S16[uOffset];
			memcpy( pData, pD, sizeof(DWORD) * uCount );
			return RNTARR;

		case W17A:
			pD = &m_pCtx->m_S17[uOffset];
			memcpy( pData, pD, sizeof(DWORD) * uCount );
			return RNTARR;

		case S16W:
		case S17W:
		case S18W:
		case S48:
		case S49:
			*pData = 0;
			return RNTDATA;

		case R48D:
			pD = &m_R48[uOffset];
			memcpy( pData, pD, sizeof(DWORD) * uCount );
			return RNTARR;

		case W48D:
			pD = &m_W48[uOffset];
			memcpy( pData, pD, sizeof(DWORD) * uCount );
			return RNTARR;

		case R49D:
			pD = &m_R49[uOffset];
			memcpy( pData, pD, sizeof(DWORD) * uCount );
			return RNTARR;

		case W49D:
			pD = &m_W49[uOffset];
			memcpy( pData, pD, sizeof(DWORD) * uCount );
			return RNTARR;

//*!*/		case W2A:
//*!*/		case W2B:
//*!*/			pData[0] = m_pCtx->m_S02[uTable-W2A];
//*!*/			return RNTDATA;

//*!*/		case W4A:
//*!*/		case W4B:
//*!*/			pData[0] = m_pCtx->m_S04[uTable-W4A];
//*!*/			return RNTDATA;

//*!*/		case W5A:
//*!*/		case W5B:
//*!*/		case W5C:
//*!*/		case W5D:
//*!*/			pData[0] = m_pCtx->m_S05[uTable-W5A];
//*!*/			return RNTDATA;

//*!*/		case W7A:
//*!*/		case W7B:
//*!*/		case W7C:
//*!*/		case W7D:
//*!*/			pData[0] = m_pCtx->m_S07[uTable-W7A];
//*!*/			return RNTDATA;

//*!*/		case W10A:
//*!*/		case W10B:
//*!*/		case W10C:
//*!*/		case W10D:
//*!*/		case W10E:
//*!*/			pData[0] = m_pCtx->m_S10[uTable-W10A];
//*!*/			return RNTDATA;

//*!*/		case W12D:
//*!*/			pD = PDWORD(m_pCtx->m_S12D);

//*!*/			memcpy( pData, pD, sizeof(DWORD) * min(uCount, DWSZS12) );

//*!*/			if( uCount > DWSZS12 ) {

//*!*/				memset( pData + DWSZS12, 0, sizeof(DWORD) * (uCount - DWSZS12) );
//*!*/				}

//*!*/			return RNTARR;

//*!*/		case W18A:
//*!*/		case W18B:
//*!*/		case W18C:
//*!*/		case W18D:
//*!*/		case W18E:
//*!*/			pData[0] = m_pCtx->m_S18[uTable-W18A];
//*!*/			return RNTDATA;

//*!*/		case S2W:
//*!*/		case S4W:
//*!*/		case S5W:
//*!*/		case S7W:
//*!*/		case S10W:
//*!*/		case S12W:	return RNTDONE;			
		}

	return RNTREAD;
	}

BOOL CKEBDIN2Driver::WriteNoTransmit( UINT uTable, UINT uOffset, PDWORD pData, UINT uCount )
{
	DWORD dData = PU4(pData)[0];

	PDWORD pD;

	uOffset -= 1; // arrays are configured to start at address 1

	switch( uTable ) {

		case SERR:
			m_Error = 0;
			return TRUE;

		case W12A:
//*!*/		case W12B:
//*!*/		case W12C:
			m_pCtx->m_S12[uTable-W12A] = dData;

			if( uTable == W12A ) {

				m_pCtx->m_S12ALoaded = 0x12345678;
				m_fS12ALoaded        = TRUE;
				}

			return TRUE;

		case W16A:
			pD = &m_pCtx->m_S16[uOffset];
			memcpy( pD, pData, sizeof(DWORD) * uCount );
			return TRUE;

		case W17A:
			pD = &m_pCtx->m_S17[uOffset];
			memcpy( pD, pData, sizeof(DWORD) * uCount );
			return TRUE;

		case R2A:
		case R2B:
		case R4A:
		case R4B:
		case R5A:
		case R5B:
		case R5C:
		case R5D:
		case R7A:
		case R7B:
		case R7C:
		case R7D:
		case R10A:
		case R10B:
		case R10C:
		case R10D:
		case R10E:
		case R12B:
		case R12C:
		case R12D:
		case R16A:
		case R17A:
		case R18A:
		case R18B:
		case R18C:
		case R18D:
		case R18E:
		case R48D:
		case R49D:
//*!*/ Disabled writing of S3A, CM6, CM11 Apr 06
/*!*/		case S3A:
/*!*/		case CM6:
/*!*/		case CM11:
			 return TRUE;

		case W48D:
			pD = &m_W48[uOffset];
			memcpy( pD, pData, sizeof(DWORD) * uCount );
			return TRUE;

		case W49D:
			pD = &m_W49[uOffset];
			memcpy( pD, pData, sizeof(DWORD) * uCount );
			return TRUE;

//*!*/		case W2A:
//*!*/		case W2B:
//*!*/			m_pCtx->m_S02[uTable-W2A] = dData;
//*!*/			return TRUE;

//*!*/		case W4A:
//*!*/		case W4B:
//*!*/			m_pCtx->m_S04[uTable-W4A] = dData;
//*!*/			return TRUE;

//*!*/		case W5A:
//*!*/		case W5B:
//*!*/		case W5C:
//*!*/		case W5D:
//*!*/			m_pCtx->m_S05[uTable-W5A] = dData;
//*!*/			return TRUE;

//*!*/		case W7A:
//*!*/		case W7B:
//*!*/		case W7C:
//*!*/		case W7D:
//*!*/			m_pCtx->m_S07[uTable-W7A] = dData;
//*!*/			return TRUE;

//*!*/		case W10A:
//*!*/		case W10B:
//*!*/		case W10C:
//*!*/		case W10D:
//*!*/		case W10E:
//*!*/			m_pCtx->m_S10[uTable-W10A] = dData;
//*!*/			return TRUE;

//*!*/		case W12D:
//*!*/			pD = PDWORD(m_pCtx->m_S12D);

//*!*/			memcpy( pD, pData, min(SZS12D, sizeof(DWORD) * uCount) );

//*!*/			if( DWSZS12 > uCount ) {

//*!*/				memset( pD + uCount, 0, sizeof(DWORD) * (DWSZS12 - uCount) );
//*!*/				}

//*!*/			else m_pCtx->m_S12D[SZS12D-1] = 0;

//*!*/			return TRUE;

//*!*/		case W18A:
//*!*/		case W18B:
//*!*/		case W18C:
//*!*/		case W18D:
//*!*/		case W18E:
//*!*/			m_pCtx->m_S18[uTable-W18A] = dData;
//*!*/			return TRUE;
		}

	return FALSE;
	}

DWORD CKEBDIN2Driver::GetData(UINT uPosition, UINT uCount)
{
	DWORD dResult = 0;

	for( UINT i = uPosition; i < uPosition + uCount; i++ ) {

		dResult <<= 4;

		dResult += GetDataValue( m_bRx[i] );
		}

	return dResult;
	}

DWORD CKEBDIN2Driver::GetDataValue( BYTE b )
{
	if( b >= '0' && b <= '9' ) return b - '0';

	if( b >= 'A' && b <= 'F' ) return b - '7';

	if( b >= 'a' && b <= 'f' ) return b - 'W';

	return 0;
	}

UINT CKEBDIN2Driver::GetErrorControl(void)
{
	switch( m_bRx[1] ) {

		case '1': // Not Ready
		case '5': // BCC error
		case '6': // Inverter Busy

			return CFAIL; // force a retry
		}

	return CERROR; // don't retry

//		case '2': // Address/Password invalid
//		case '3': // Data invalid
//		case '4': // Write Protect
//		case '7': // Service Not Available
//		case '8': // Password invalid
//		case '9': // Framing Error
//		case 'A': // Transmission Error
//		case 'B': // Set Identification invalid
//		case 'C': // Set Identification invalid
//		case 'D': // Address invalid
//		case 'E': // Operation not possible
//		case 'F': // Not Used
//		default:  // Unknown error code
	}

// End of File
