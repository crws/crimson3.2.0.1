
#include "intern.hpp"

#include "s5as511.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Master
//

// Instantiator

INSTANTIATE(CS5AS511Master);

// Constructor

CS5AS511Master::CS5AS511Master(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CS5AS511Master::~CS5AS511Master(void)
{
	}

// Configuration

void MCALL CS5AS511Master::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_fCache  = GetByte(pData);

		m_fAddr32 = GetByte(pData); 
		}
	}

// Device

CCODE MCALL CS5AS511Master::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pBase = (CBase *) pDevice->GetContext()) ) {

		m_pBase = new CBase;

		InitBase(m_pBase);
		
		m_pBase->m_fCache  = m_fCache;

		m_pBase->m_fAddr32 = m_fAddr32;
		
		pDevice->SetContext(m_pBase);

		return CCODE_SUCCESS;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CS5AS511Master::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		FreeBase();

		delete m_pBase;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Transport

void CS5AS511Master::ClearRx(void)
{
	m_pData->ClearRx();
	}

BOOL CS5AS511Master::Recv(PBYTE pData, UINT &uLen, BOOL fTerm)
{
	UINT uPtr = 0;
	
	BOOL fDLE = FALSE;

	SetTimer(TIMEOUT);
	
	while( GetTimer() ) {

		if( uPtr == uLen ) {

			Dump(pData, uLen, FALSE);

			return TRUE;
			}
	
		UINT uData = m_pData->Read(PAUSE);
		
		if( uData == NOTHING ) {
		
			continue;
			}

		SetTimer(TIMEOUT);
	
		if( !fDLE ) {

			if( uData == DLE ) {

				fDLE = TRUE;
				}
			}
		else {
			fDLE = FALSE;

			switch( uData ) {

				case DLE:
					continue;

				case ETX:
					uLen = uPtr - 1;

					Dump(pData, uLen, FALSE);

					return TRUE;

				case NAK:
					SetBusy();
					
					break;

				default:
					return FALSE;
				}
			}

		pData[ uPtr ++ ] = BYTE(uData);

		if( uPtr == uLen ) {

			Dump(pData, uLen, FALSE);

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CS5AS511Master::Send(PCBYTE pData, UINT uLen)
{
	Dump(pData, uLen, TRUE);

	return m_pData->Write(PBYTE(pData), uLen, FOREVER);
	}

// End of File
