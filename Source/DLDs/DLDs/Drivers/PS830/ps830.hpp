/////////////////////////////////////////////////////////////////////////
//
// Constants
//
 
#define FRAME_TIMEOUT	1000
#define RO		0x01
#define RIN		0x01
#define WIN		0x02
#define RFL		0x03
#define WFL		0x04
#define ECM		0x05

//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific 830 Serial Master Driver
//

class CPacSci830MasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CPacSci830MasterDriver(void);

		// Destructor
		~CPacSci830MasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			UINT m_uDrop;
			BOOL m_fBroadcast;
			
			};

		
		CContext * m_pCtx;

		// Data Members

		LPCTXT 	m_pHex;
		UINT	m_uPtr;
		UINT	m_uCount;
		BYTE	m_bTxBuff[32];
		BYTE	m_bRxBuff[32];
		BYTE	m_Com;
		CRC16	m_Crc;
		

		BOOL DoRead(UINT uOffset, UINT uType);
		BOOL DoWrite(UINT uOffset, UINT uType, PDWORD pData, UINT uCount);
		void PutReadCommand(UINT uOffset, UINT uType);
		void PutWriteCommand(UINT uOffset, UINT uType, PDWORD pData, UINT uCount);
		
		void Start(void);
		void End(void);
		void AddWord(WORD wWord);
		void AddCrc(WORD wWord);
		void AddByte(BYTE bByte);
		
		void Send(void);
		BOOL Recv(void);

			
	};

// End of File
