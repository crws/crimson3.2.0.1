
#include "intern.hpp"

#include "ps830.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific 830 Serial Master Driver
//

// Instantiator

INSTANTIATE(CPacSci830MasterDriver);

// Constructor

CPacSci830MasterDriver::CPacSci830MasterDriver(void)
{
	m_Ident         = DRIVER_ID;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex; 

	}

// Destructor

CPacSci830MasterDriver::~CPacSci830MasterDriver(void)
{
	}

// Configuration

void MCALL CPacSci830MasterDriver::Load(LPCBYTE pData)
{
	
	}
	
void MCALL CPacSci830MasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);		
	}
	
// Management

void MCALL CPacSci830MasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CPacSci830MasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CPacSci830MasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_uDrop      = GetByte(pData);

			m_pCtx->m_fBroadcast = !m_pCtx->m_uDrop ? TRUE : FALSE;

			m_pCtx->m_uDrop <<= 8;
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
		
	}

CCODE MCALL CPacSci830MasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CPacSci830MasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = 41;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	
	}

CCODE MCALL CPacSci830MasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if ( m_pCtx->m_fBroadcast ) {

		return uCount;
		}
	
	if ( Addr.a.m_Type == addrBitAsBit ) {	      

		return uCount;
		}

	MakeMin(uCount, 1);

	if ( DoRead(Addr.a.m_Offset, Addr.a.m_Type) ) {

		DWORD x = PDWORD(m_bRxBuff + 7)[0];

		pData[0] = IntelToHost(x);
		
		return uCount;
		}

	return CCODE_ERROR;

	}

CCODE MCALL CPacSci830MasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if ( Addr.a.m_Extra & RO ) {

		return uCount;
		}

	MakeMin(uCount, 1);

	if ( DoWrite(Addr.a.m_Offset, Addr.a.m_Type, pData, uCount ) ) {

		return uCount;
		}

	if ( Addr.a.m_Type == addrBitAsBit ) {

		for ( UINT u = 0; u < 3; u++ ) {

		   	if ( Recv() ) {

				return uCount;
				}
			}
		}

	return CCODE_ERROR;

	}

// Implementation

BOOL CPacSci830MasterDriver::DoRead(UINT uOffset, UINT uType)
{
	Start();

	PutReadCommand(uOffset, uType);
	
	End();

	return Recv();
	}

void CPacSci830MasterDriver::PutReadCommand(UINT uOffset, UINT uType)
{	
	AddWord(0x03);

	AddWord(0x00);

	UINT uPreCmd = m_pCtx->m_uDrop;

	uType == addrLongAsReal ? uPreCmd |= RFL : uPreCmd |= RIN;     

	m_Com = (uPreCmd & 0xFF) | 0x80;

	AddWord(uPreCmd);

	AddWord(uOffset & 0xFF);

	}

void CPacSci830MasterDriver::Start( void )
{
	m_uCount = 0;

	m_uPtr = 0;

	m_Crc.Preset();

	AddByte(0x1);

	}

BOOL CPacSci830MasterDriver::DoWrite(UINT uOffset, UINT uType, PDWORD pData, UINT uCount)
{
	Start();

	PutWriteCommand(uOffset, uType, pData, uCount);
	
	End();

	if( m_pCtx->m_fBroadcast ) {

		while( m_pData->Read(0) < NOTHING );

		return TRUE;
		}
	
	return Recv();
	}

void CPacSci830MasterDriver::PutWriteCommand(UINT uOffset, UINT uType, PDWORD pData, UINT uCount)
{	
	if ( uType == addrBitAsBit ) {

		AddWord(0x03);

		AddWord(0x00);

		AddWord(m_pCtx->m_uDrop + ECM);

		m_Com = ECM | 0x80;

		AddWord(uOffset & 0xFF);

		return;
		}

	AddWord(0x05);

	AddWord(0x00);

	if ( uType == addrLongAsReal ) {

		m_Com = WFL | 0x80;

		AddWord(m_pCtx->m_uDrop + WFL);
		}
	
	else {
		m_Com = WIN | 0x80;

		AddWord(m_pCtx->m_uDrop + WIN);
		}
	
	AddWord(uOffset & 0xFF);

	AddWord(LOWORD(pData[0]));

	AddWord(HIWORD(pData[0]));

	}

void CPacSci830MasterDriver::End(void)
{
	AddByte(LOBYTE(m_Crc.GetValue()));
	
	AddByte(HIBYTE(m_Crc.GetValue()));
	
	Send();
	}

	
void CPacSci830MasterDriver::AddWord(WORD wWord)
{
	AddByte(LOBYTE(wWord));

	AddByte(HIBYTE(wWord));

	AddCrc(wWord);
	}

void CPacSci830MasterDriver::AddCrc(WORD wWord)
{
	m_Crc.Add(LOBYTE(wWord));

	m_Crc.Add(HIBYTE(wWord));
	}

void CPacSci830MasterDriver::AddByte(BYTE bByte)
{
	if ( bByte == 0x1 && m_uCount != 0 ) {

		m_bTxBuff[m_uPtr++] = 0x10;

		m_bTxBuff[m_uPtr++] = 0x11;

		m_uCount += 2;

		}

	else if ( bByte == 0x10 ) {

		m_bTxBuff[m_uPtr++] = 0x10;

		m_bTxBuff[m_uPtr++] = 0;

		m_uCount += 2;

		}

	else {
		m_bTxBuff[m_uPtr++] = bByte;

		m_uCount++;
		}
	}

void CPacSci830MasterDriver::Send(void)
{
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);
	}
	
BOOL CPacSci830MasterDriver::Recv( void )
{
	UINT uPtr = 0;

	BOOL bDLE = FALSE;

	UINT uCRC = 0;

	UINT m_uSize = sizeof(m_bRxBuff);

	m_Crc.Preset();

	UINT uTimer = 0;

	UINT uByte = 0;
	
	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}
				
		if ( uByte != DLE ) {

			if ( !bDLE ) {
				
				m_bRxBuff[uPtr++] = uByte;
				}
			else {
				m_bRxBuff[uPtr++] = uByte ^ DLE; 
				
				bDLE = FALSE;
				}
			}

		else {
			bDLE = TRUE; 
			}

		if ( uPtr == 3 ) {
			
			m_uSize = 5 + (2 * (( m_bRxBuff[2] << 8 ) + m_bRxBuff[1]));
			}

		if ( uPtr >= m_uSize ) {

			if ( m_bRxBuff[5] != m_Com ) {
				
				return FALSE; 
				}

			for ( UINT i = 1; i < m_uSize - 2; i+=2 ) { 

				m_Crc.Add(BYTE(m_bRxBuff[i]));

				m_Crc.Add(BYTE(m_bRxBuff[i+1]));

				}
			
			uCRC = (m_bRxBuff[m_uSize - 1] << 8) + m_bRxBuff[m_uSize - 2];
			
			if ( uCRC == m_Crc.GetValue() ) {
				
				return TRUE; 
				}

			return FALSE;
			}

                }
	
	return FALSE;
	}

// End of File
