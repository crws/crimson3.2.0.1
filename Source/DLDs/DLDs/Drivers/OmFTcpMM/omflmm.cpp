
#include "intern.hpp"

#include "omflmm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Modicon Driver
//

// Constructor

COmniFlowBaseModiconMaster::COmniFlowBaseModiconMaster(void)
{
	m_uPtr = 0;

	m_bHead = 0;
	}

// Destructor

COmniFlowBaseModiconMaster::~COmniFlowBaseModiconMaster(void)
{
	
	}

// Entry Points

CCODE MCALL COmniFlowBaseModiconMaster::Ping(void)
{
	if( m_pBase->m_bUnit == 255 ) {

		return CCODE_SUCCESS;
		}

	if( m_pBase->m_PingReg ) {

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = SPACE_HR;
		
		Addr.a.m_Offset = m_pBase->m_PingReg;
	
		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;

		return DoWordRead(Addr, Data, 1);
		}
	
	return CCODE_SUCCESS;
	}

CCODE MCALL COmniFlowBaseModiconMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uIndex = 0;
	
	UINT uOffset = 0;

	switch( Addr.a.m_Table ) {
	
		case SPACE_CS:
		case SPACE_IS:

			return DoBitRead(Addr, pData, uCount);

		case SPACE_HR:
		case SPACE_IR:
		case SPACE_HRL:
		case SPACE_HRD:
		case SPACE_STR8:
		case SPACE_STR16:
		case SPACE_STR32:
				
			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordRead(Addr, pData, uCount);
				}

			return DoLongRead(Addr, pData, uCount);

		case SPACE_EC:

			pData[0] = m_pBase->m_Exception;

			return uCount;

		case SPACE_CPB:

			for( uIndex = 0, uOffset = Addr.a.m_Offset; uIndex < uCount; uIndex++, uOffset++ ) {

				pData[uIndex] = m_pBase->m_Custom[uOffset];
				}

			return uCount;

		case SPACE_CDP:

			pData[0] = m_pBase->m_CustomPt;

			return uCount;

		case SPACE_CDC:

			pData[0] = m_pBase->m_CustomQty;

			return uCount;

		case SPACE_CDR:

			pData[0] = 0;

			return uCount;

		case SPACE_CPD:

			return DoCustomRead(Addr, pData, uCount);
		
		case SPACE_BRP:

			pData[0] = m_pBase->m_ReadPt + 1;

			return uCount;

		case SPACE_BRR:

			pData[0] = 0;

			return uCount;

		case SPACE_RAB:

			for( uIndex = 0, uOffset = Addr.a.m_Offset; uIndex < uCount; uIndex++, uOffset++ ) {

				pData[uIndex] = m_pBase->m_ReadBuff[uOffset];
				}

			return uCount;

		case SPACE_BWP:

			pData[0] = m_pBase->m_WritePt + 1;

			return uCount;

		case SPACE_BWQ:

			pData[0] = m_pBase->m_WriteQty;

			return uCount;

		case SPACE_BWR:

			pData[0] = 0;

			return uCount;

		case SPACE_WAB:

			for( uIndex = 0, uOffset = Addr.a.m_Offset; uIndex < uCount; uIndex++, uOffset++ ) {

				pData[uIndex] = m_pBase->m_WriteBuff[uOffset];
				}

			return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL COmniFlowBaseModiconMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uIndex = 0;
	
	UINT uOffset = 0;

	switch( Addr.a.m_Table ) {
	
		case SPACE_CS:
			
			return DoBitWrite(Addr, pData, uCount);

		case SPACE_HR:
		case SPACE_HRL:
		case SPACE_HRD:
		case SPACE_STR8:
		case SPACE_STR16:
		case SPACE_STR32:
				
			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(Addr, pData, uCount);
				}

			return DoLongWrite(Addr, pData, uCount);

		case SPACE_EC:

			return uCount;

		case SPACE_CDP:

			m_pBase->m_CustomPt = pData[0];

			return uCount;

		case SPACE_CDC:

			m_pBase->m_CustomQty = pData[0];

			return uCount;

		case SPACE_CDR:

			if( pData[0] == 0 ) {

				return uCount;
				}

			if( COMMS_SUCCESS(DoCustomDataPacket()) ) {

				return uCount;
				}

			return CCODE_ERROR;

		case SPACE_CPD:

			return DoCustomWrite(Addr, pData, uCount);

		case SPACE_BRP:

			m_pBase->m_ReadPt = pData[0] - 1;

			return uCount;

		case SPACE_BRR:

			if( pData[0] == 0 ) {

				return uCount;
				}

			if( COMMS_SUCCESS(DoAsciiTextBufferRead()) ) {

				return uCount;
				}

			return CCODE_ERROR;

		case SPACE_BWP:

			m_pBase->m_WritePt = pData[0] - 1;

			return uCount;

		case SPACE_BWQ:

			m_pBase->m_WriteQty = pData[0];

			return uCount;

		case SPACE_WAB:

			for( uIndex = 0, uOffset = Addr.a.m_Offset; uIndex < uCount; uIndex++, uOffset++ ) {

				m_pBase->m_WriteBuff[uOffset] = pData[uIndex];
				}
			
			return uCount;

		case SPACE_BWR:

			if( pData[0] == 0 ) {

				return uCount;
				}

			if( COMMS_SUCCESS(DoAsciiTextBufferWrite()) )  {

				return uCount;
				}

			return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Frame Building

void COmniFlowBaseModiconMaster::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;
	}

void COmniFlowBaseModiconMaster::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {
	
		m_bTxBuff[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void COmniFlowBaseModiconMaster::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void COmniFlowBaseModiconMaster::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL COmniFlowBaseModiconMaster::Transact(void)
{
	m_fException = FALSE;

	if( SendFrame() && RecvFrame() ) {

		return CheckFrame();
		}
	
	return FALSE;
	}

BOOL COmniFlowBaseModiconMaster::SendFrame(void)
{
/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_uPtr; u++ ) {

		AfxTrace("%2.2x ", m_bTxBuff[u]);
		}
	
*/	return FALSE;
	}

BOOL COmniFlowBaseModiconMaster::RecvFrame(void)
{
	
	return FALSE;
	}

BOOL COmniFlowBaseModiconMaster::CheckFrame(void)
{	
	if( !(m_bRxBuff[1] & 0x80) ) {
	
		return TRUE;
		}

	m_fException = TRUE;

	return FALSE;
	}

// Read Handlers

CCODE COmniFlowBaseModiconMaster::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HR:
			StartFrame(0x03);
			break;
			
		case SPACE_IR:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	MakeMin(uCount, 64);
		
	AddWord(WORD(Addr.a.m_Offset - 1));
	
	AddWord(WORD(uCount));
	
	if( Transact() ) {
		
		GetWords(pData, uCount);

		return uCount;
		}

	if( SaveException(Addr.a.m_Table != SPACE_IR ? 3 : 4, Addr.a.m_Offset - 1) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE COmniFlowBaseModiconMaster::DoCustomRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_CPD:
			StartFrame(0x03);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	MakeMin(uCount, 125);
		
	AddWord(WORD(Addr.a.m_Offset - 1));
	
	AddWord(WORD(uCount));
	
	if( Transact() ) {

		UINT Count = m_bRxBuff[2];

		if( IsReal(Addr.a.m_Type) ) {

			GetReals(pData, Count / 4);
			}
		else {
			switch( Addr.a.m_Type ) {

				case addrWordAsWord:	GetWords(pData, Count / 2);	break;
				case addrWordAsLong:	GetLongs(pData, Count / 4);	break;
				}
			}

		return uCount;
		}

	if( SaveException(3, Addr.a.m_Offset - 1) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE COmniFlowBaseModiconMaster::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HR:
		case SPACE_HRL:
		case SPACE_HRD:
		case SPACE_STR8:
		case SPACE_STR16:
		case SPACE_STR32:
			StartFrame(0x03);
			break;
			
		case SPACE_IR:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	MakeMin(uCount, 32);
			
	AddWord(GetModiconOffset(Addr));

	AddWord(GetModiconCount(Addr.a.m_Table, uCount));
	
	if( Transact() ) {

		if( IsReal(Addr.a.m_Type) ) {

			GetReals(pData, uCount);

			return uCount;
			}

		GetLongs(pData, uCount);

		return uCount;
		}

	if( SaveException(Addr.a.m_Table != SPACE_IR ? 3 : 4, Addr.a.m_Offset - 1) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE COmniFlowBaseModiconMaster::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
			
		case SPACE_CS:
			StartFrame(0x01);
			break;
			
		case SPACE_IS:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	MakeMin(uCount, 512);

	AddWord(WORD(Addr.a.m_Offset - 1));

	AddWord(WORD(uCount));

	if( Transact() ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRxBuff[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	if( SaveException(Addr.a.m_Table != SPACE_IS ? 1 : 2, Addr.a.m_Offset - 1) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE COmniFlowBaseModiconMaster::DoCustomDataPacket(void)
{
	StartFrame(0x03);

	AddWord(m_pBase->m_CustomPt - 1);

	AddWord(m_pBase->m_CustomQty);

	memset(m_pBase->m_Custom, 0, elements(m_pBase->m_Custom));

	if( Transact() ) {

		memcpy(m_pBase->m_Custom, m_bRxBuff + 3, m_bRxBuff[2]);

		return 1;
		}

	if( SaveException(3, m_pBase->m_CustomPt) ) {

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE COmniFlowBaseModiconMaster::DoAsciiTextBufferRead(void)
{
	BOOL fEOF = FALSE;

	WORD wPacket = 0;

	UINT uPosition = 0;

	BOOL fInit = FALSE;

	while( !fEOF ) {

		StartFrame(65);

		AddWord(m_pBase->m_ReadPt);

		AddWord(wPacket);

		if( Transact() ) {

			if( !fInit ) {

				memset(m_pBase->m_ReadBuff, 0x00, elements(m_pBase->m_ReadBuff) * sizeof(DWORD));
				
				fInit = TRUE;
				}

			UINT uBytes = GetTextBuffer(m_pBase->m_ReadBuff + uPosition / 4, 128/4);

			fEOF = (uBytes < 128);

			if( !fEOF ) {

				fEOF = (wPacket == 255);
				}

			uPosition += 128;

			wPacket++;

			continue;
			}

		if( SaveException(65, m_pBase->m_ReadPt) ) {

			return 1;
			}

		return CCODE_ERROR;
		}

	return 1;
	}

// Write Handlers

CCODE COmniFlowBaseModiconMaster::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_HR ) {

		if( uCount == 1 && !m_pBase->m_Disable6 ) {

			StartFrame(6);

			AddWord(WORD(Addr.a.m_Offset - 1));

			SetWords(pData, uCount);
			}
		else {
			MakeMin(uCount, 64);

			StartFrame(16);

			AddWord(WORD(Addr.a.m_Offset - 1));
			
			AddWord(WORD(uCount));
			
			AddByte(BYTE(uCount * 2));

			SetWords(pData, uCount);
			}

		if( Transact() ) {
			
			return uCount;
			}

		if( SaveException(uCount == 1 ? 6 : 16, Addr.a.m_Offset - 1) ) {

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE COmniFlowBaseModiconMaster::DoCustomWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_CPD ) {

		MakeMin(uCount, 125);

		StartFrame(16);

		AddWord(WORD(Addr.a.m_Offset - 1));
			
		AddWord(WORD(uCount));
			
		if( IsReal(Addr.a.m_Type) ) {

			AddByte(BYTE(uCount * 4));

			SetReals(pData, uCount);
			}
		else {
			switch( Addr.a.m_Type ) {

				case addrWordAsWord:	AddByte(BYTE(uCount * 2));	SetWords(pData, uCount);	break;
				case addrWordAsLong:	AddByte(BYTE(uCount * 4));	SetLongs(pData, uCount);	break;
				}
			}
		
		if( Transact() ) {
			
			return uCount;
			}

		if( SaveException(16, Addr.a.m_Offset - 1) ) {

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}


CCODE COmniFlowBaseModiconMaster::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HR:
		case SPACE_HRL:
		case SPACE_HRD:
		case SPACE_STR8:
		case SPACE_STR16:
		case SPACE_STR32:
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	MakeMin(uCount, 32);

	StartFrame(16);
		
	AddWord(GetModiconOffset(Addr));

	AddWord(GetModiconCount(Addr.a.m_Table, uCount));

	BYTE bBytes = GetModiconBytes(Addr.a.m_Table, uCount);
		
	AddByte(bBytes);

	if( IsReal(Addr.a.m_Type) ) {

		SetReals(pData, uCount);
		}
	else {

		for( UINT n = 0; n < uCount; n++ ) {

			DWORD x = pData[n];

			AddLong(x);
			}

		while( n < bBytes / 4 ) {

			AddLong(0);

			n++;
			}
		}

	if( Transact() ) {

		return uCount;
		}

	if( SaveException(16, Addr.a.m_Offset - 1) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE COmniFlowBaseModiconMaster::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_CS ) {

		if( uCount == 1 && !m_pBase->m_Disable5 ) {

			StartFrame(5);

			AddWord(WORD(Addr.a.m_Offset - 1));
			
			AddWord(pData[0] ? WORD(0xFF00) : WORD(0x0000));
			}
		else {
			MakeMin(uCount, 512);

			StartFrame(15);

			AddWord(WORD(Addr.a.m_Offset - 1));

			AddWord(WORD(uCount));

			AddByte(BYTE((uCount + 7) / 8));

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(BYTE(b));

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(BYTE(b));
				}
			}

		if( Transact() ) {
			
			return uCount;
			}

		if( SaveException(uCount == 1 ? 5 : 15, Addr.a.m_Offset - 1) ) {

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE COmniFlowBaseModiconMaster::DoAsciiTextBufferWrite(void)
{
	BOOL fEOF = FALSE;

	WORD wPacket = 0;

	UINT uPosition = 0;

	while( !fEOF ) {

		StartFrame(66);

		AddWord(m_pBase->m_WritePt);

		AddWord(wPacket);

		UINT uBytes  =  min(128, m_pBase->m_WriteQty - uPosition);

		PDWORD pWork = PDWORD(m_pBase->m_WriteBuff + uPosition / 4);

		for( UINT u = 0; u < 128 / 4; u++, pWork++ ) {

			AddLong(*pWork);
			}

		if( uBytes < 128 ) {

			BYTE bHead = m_bHead + 4;

			PBYTE(m_bTxBuff + uBytes + bHead)[0] = 0x1A;

			fEOF = TRUE;

			m_uPtr = uBytes + bHead + 1;

			memset(m_bTxBuff + m_uPtr, 0x0, 140);

			m_uPtr = 128 + bHead;
			}
		
		if( wPacket == 255 ) {

			PBYTE(m_bTxBuff + m_uPtr)[0] = 0x1A;

			fEOF = TRUE;
			}

		if( Transact() ) {

			wPacket++;

			uPosition += 128;

			continue;
			}

		if( SaveException(66, m_pBase->m_WritePt) ) {

			return 1;
			}

		return CCODE_ERROR;
		}

	return 1;
	}

// Data Handlers

UINT COmniFlowBaseModiconMaster::GetTextBuffer(PDWORD pData, UINT uCount)
{
	PBYTE pBytes = PBYTE(m_bRxBuff + 6);

	UINT uBytes = uCount * sizeof(DWORD);

	PDWORD pWork = PDWORD(alloca(uBytes));

	PBYTE pSave = PBYTE(pWork);

	for( UINT u = 0; u < uBytes; u++ ) {

		if( pBytes[u] == 0x1A ) {

			while( u % 4 ) {

				pSave[u] = 0x20;

				u++;
				}

			uBytes = u;

			break;
			}

		pSave[u] = pBytes[u];
		}

	for( UINT n = 0; n < uBytes / 4; n++ ) {

		DWORD x  = PU4(pWork)[n];
			
		pData[n] = MotorToHost(x);
		}

	return uBytes;
	}

void COmniFlowBaseModiconMaster::GetBytes(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		pData[n]  = PBYTE(m_bRxBuff + 3)[n];
		}
	}

void COmniFlowBaseModiconMaster::GetWords(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		WORD x   = PU2(m_bRxBuff + 3)[n];
			
		pData[n] = LONG(SHORT(MotorToHost(x)));
		}
	}

void COmniFlowBaseModiconMaster::GetLongs(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x  = PU4(m_bRxBuff + 3)[n];
			
		pData[n] = MotorToHost(x);
		}
	}

void COmniFlowBaseModiconMaster::GetReals(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x  = PU4(m_bRxBuff + 3)[n];

		pData[n]  = MotorToHost(x);

		SwapWords(pData[n]);
		}
	}

void COmniFlowBaseModiconMaster::SetBytes(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		AddByte(pData[n] & 0xFF);
		}
	}

void COmniFlowBaseModiconMaster::SetWords(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		AddWord(pData[n] & 0xFFFF);
		}
	}

void COmniFlowBaseModiconMaster::SetLongs(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		AddLong(pData[n]);
		}
	}

void COmniFlowBaseModiconMaster::SetReals(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = pData[n];

		SwapWords(x);
		
		AddLong(x);
		}
	}

BOOL COmniFlowBaseModiconMaster::SaveException(UINT uFC, UINT uOffset)
{
	if( m_fException ) {

		m_pBase->m_Exception  = 0;

		m_pBase->m_Exception |= uFC << 24;

		m_pBase->m_Exception |= (uOffset & 0xFFFF) << 8;

		m_pBase->m_Exception |= (m_bRxBuff[2] & 0xFF);

		return TRUE;
		}

	return FALSE;
	}

// Helpers

BOOL COmniFlowBaseModiconMaster::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL COmniFlowBaseModiconMaster::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL COmniFlowBaseModiconMaster::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

BOOL COmniFlowBaseModiconMaster::IsReal(UINT uType)
{
	switch( uType ) {

		case addrWordAsReal:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

void COmniFlowBaseModiconMaster::SwapWords(DWORD &dwWord)
{
	WORD h = HIWORD(dwWord);

	WORD l = LOWORD(dwWord);

	dwWord = MAKELONG(h, l);
	}

WORD COmniFlowBaseModiconMaster::GetModiconCount(UINT uTable, UINT uCount)
{
	return uCount * 2;
	}

BYTE COmniFlowBaseModiconMaster::GetModiconBytes(UINT uTable, UINT uCount)
{
	return GetModiconCount(uTable, uCount) * 2;
	}

WORD COmniFlowBaseModiconMaster::GetModiconOffset(AREF Addr)
{
	WORD wOffset = Addr.a.m_Offset;

	UINT uDiv = 0;

	switch( Addr.a.m_Table ) {

		case SPACE_HRD:
		case SPACE_STR8:	uDiv = 2;	break;
		case SPACE_STR16:	uDiv = 4;	break;
		case SPACE_STR32:	uDiv = 8;	break;
		}

	if( uDiv ) {

		UINT uRaw = Addr.a.m_Offset | Addr.a.m_Extra << 16;

		wOffset = uRaw / uDiv;
		}

	return wOffset - 1;
	}


// End of File
