#ifndef INCLUDE_OMFLMM_HPP

#define INCLUDE_OMFLMM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Spaces
//

#define	SPACE_CS	3
#define	SPACE_IS	4
#define	SPACE_HR	1
#define	SPACE_IR	2
#define SPACE_EC	5
#define SPACE_CDP	6
#define SPACE_CPB	7
#define SPACE_CDC	8
#define SPACE_CPD	9
#define SPACE_BRP	10
#define SPACE_BRR	11
#define SPACE_RAB	12
#define SPACE_BWP	13
#define SPACE_BWQ	14
#define SPACE_BWR	15
#define SPACE_WAB	16
#define SPACE_HRL	17
#define SPACE_HRD	18
#define SPACE_STR8	19
#define SPACE_STR16	20
#define SPACE_STR32	21
#define SPACE_CDR	22


//////////////////////////////////////////////////////////////////////////
//
// Omni Base Master Driver
//

class COmniFlowBaseModiconMaster : public CMasterDriver
{
	public:
		// Constructor
		COmniFlowBaseModiconMaster(void);

		// Destructor
		~COmniFlowBaseModiconMaster(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CBaseCtx
		{
			BYTE	 m_bUnit;
			WORD	 m_PingReg;
			DWORD	 m_Exception;
			WORD	 m_CustomPt;
			WORD	 m_CustomQty;
			BYTE	 m_Custom[255];
			WORD	 m_ReadPt;
			DWORD	 m_ReadBuff[128/4*255];
			WORD	 m_WritePt;
			WORD	 m_WriteQty;
			DWORD	 m_WriteBuff[128/4*255];
			BYTE	 m_Disable5;
			BYTE	 m_Disable6;
			};

		// Data Members
		CBaseCtx * m_pBase;
		BYTE	   m_bTxBuff[600];
		BYTE	   m_bRxBuff[600];
		UINT	   m_uPtr;
		BYTE	   m_bHead;
		BOOL	   m_fException;
		
		// Frame Building
	virtual	void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
				
		// Transport Layer
	virtual BOOL Transact(void);
	virtual BOOL SendFrame(void);
	virtual BOOL RecvFrame(void);
	virtual BOOL CheckFrame(void);
			
		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoCustomRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoCustomDataPacket(void);
		CCODE DoAsciiTextBufferRead(void);

		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoCustomWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoAsciiTextBufferWrite(void);

		// Data Handlers
		UINT GetTextBuffer(PDWORD pData, UINT uCount);
		void GetBytes(PDWORD pData, UINT uCount);
		void GetWords(PDWORD pData, UINT uCount);
		void GetLongs(PDWORD pData, UINT uCount);
		void GetReals(PDWORD pData, UINT uCount);
		void SetBytes(PDWORD pData, UINT uCount);
		void SetWords(PDWORD pData, UINT uCount);
		void SetLongs(PDWORD pData, UINT uCount);
		void SetReals(PDWORD pData, UINT uCount);
		BOOL SaveException(UINT uFC, UINT uOffset);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
		BOOL IsReal(UINT uType);
		void SwapWords(DWORD &dwWord);
		WORD GetModiconCount(UINT uTable, UINT uCount);
		BYTE GetModiconBytes(UINT uTable, UINT uCount);
		WORD GetModiconOffset(AREF Addr);
		
	};

#endif

// End of File
