#ifndef INCLUDE_OMFTCPMM_HPP

#define INCLUDE_OMFTCPMM_HPP

#include "omflmm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omni Modbus Modicon TCP/IP Master Driver
//

class COmniFlowModiconTCPMaster : public COmniFlowBaseModiconMaster
{
	public:
		// Constructor
		COmniFlowModiconTCPMaster(void);

		// Destructor
		~COmniFlowModiconTCPMaster(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
				
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Context
		struct CContext : COmniFlowBaseModiconMaster::CBaseCtx
		{
			DWORD	 m_IP;
			WORD	 m_wPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL	 m_fDirty;
			};

		// Data Members
		CContext * m_pCtx;
		UINT m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Frame Building
		void StartFrame(BYTE bOpcode);

		// Transport Layer
		BOOL Transact(void);
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
	
		
		
	};

#endif

// End of File
