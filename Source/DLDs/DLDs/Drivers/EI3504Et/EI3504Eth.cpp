
#include "intern.hpp"

#include "ei3504eth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver
//

// Instantiator

INSTANTIATE(CEI3504TCPDriver);

// Constructor

CEI3504TCPDriver::CEI3504TCPDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_uKeep     = 0;

	m_pAddrArr	= NULL;
	m_pPosnArr	= NULL;
	m_pSortDataArr	= NULL;
	m_pSortPosnArr	= NULL;
	}

// Destructor

CEI3504TCPDriver::~CEI3504TCPDriver(void)
{
	ClearArr();
	}

// Configuration

void MCALL CEI3504TCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CEI3504TCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CEI3504TCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CEI3504TCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP1		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->uWriteErrCt	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEI3504TCPDriver::DeviceClose(BOOL fPersist)
{
	ClearArr();

	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CEI3504TCPDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;
			
	return 0;
	}

// Entry Points

CCODE MCALL CEI3504TCPDriver::Ping(void)
{
	if( OpenSocket() ) {

//**/		AfxTrace0("\r\nPing ");

		if( m_pCtx->m_bUnit == 255 ) {

			return CCODE_SUCCESS;
			}

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = SPACE_HOLD;

		Addr.a.m_Offset = 1;

		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;

		UINT uCount	= 1;

		return Read(Addr, Data, uCount);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEI3504TCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	if( !m_pCtx->m_bUnit ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

//**/	Sleep(100); // Slow down for debug

	m_uTickCount = GetTickCount();

	return HandleRead(Addr, pData, uCount);
	}

CCODE MCALL CEI3504TCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	return HandleWrite(Addr, pData, uCount);
	}

// Frame Building

void CEI3504TCPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);
	}

void CEI3504TCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {
	
		m_bTxBuff[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CEI3504TCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CEI3504TCPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CEI3504TCPDriver::Transact(BOOL fIgnore)
{
	if( SendFrame() && RecvFrame() ) {

		if( fIgnore ) {

			return TRUE;
			}

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CEI3504TCPDriver::SendFrame(void)
{
	m_bTxBuff[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

//**/		AfxTrace1("\r\nSend %d ", uSize); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("[%2.2x]", m_bTxBuff[k]);

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEI3504TCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

//**/			AfxTrace1("--- Recv %d\r\n", uSize); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("<%2.2x>", m_bRxBuff[k]);

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRxBuff[5];

				if( uPtr >= uTotal ) {

					if( m_bRxBuff[0] == m_bTxBuff[0] ) {

						if( m_bRxBuff[1] == m_bTxBuff[1] ) {

							memcpy(m_bRxBuff, m_bRxBuff + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRxBuff[n] = m_bRxBuff[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CEI3504TCPDriver::CheckFrame(void)
{
	if( !(m_bRxBuff[1] & 0x80) ) {
	
		return TRUE;
		}

	return FALSE;
	}

// 3504 Handling
CCODE CEI3504TCPDriver::HandleRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nHandle Read %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	CCODE c = CCODE_ERROR;

	if( Addr.a.m_Table == SPACE_HOLD || Addr.a.m_Table == SP_COMT ) {

		c = HandleModbusRead(Addr, pData, uCount);

		if( c & CCODE_ERROR ) {

			Sleep(40);

//**/			AfxTrace0("\r\n^^^^^^^^^^");

			c = HandleModbusRead(Addr, pData, uCount);
			}
		}

	else {
		c = Handle3504(Addr, pData, uCount, FALSE);

		if( c & CCODE_ERROR ) {

			if( GetTickCount() - m_uTickCount < 0x20 ) {

				Sleep(40);  // Serial sometimes needs a rest

//**/				AfxTrace0("\r\n&&&&&&&");

				c = Handle3504(Addr, pData, uCount, FALSE);
				}
			}
		}

	ClearArr();

	return c;
	}

CCODE CEI3504TCPDriver::HandleModbusRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\n\n### Modbus Read O=%d C=%d ", Addr.a.m_Offset, uCount );

	CAddress A;

	A.m_Ref = FixMAddr(Addr);

	if( A.a.m_Type == addrWordAsWord ) {

		return DoWordRead(A, pData, uCount);
		}

	return DoLongRead(A, pData, uCount);
	}

CCODE CEI3504TCPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nDoWordRead %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	m_bRxBuff[7] = 0;

	StartFrame(0x03);

	AddWord((WORD)Addr.a.m_Offset);
	
	AddWord((WORD)uCount);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU2(m_bRxBuff + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CEI3504TCPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nDoLongRead %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	m_bRxBuff[7] = 0;

	StartFrame(0x03);

	AddWord((WORD)Addr.a.m_Offset);
	
	AddWord((WORD)uCount * 2);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_bRxBuff + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CEI3504TCPDriver::HandleWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n++++++ Write +++++ T=%d O=%x D=%8.8lx ", Addr.a.m_Table, Addr.a.m_Offset, pData[0]);

	CCODE c = CCODE_ERROR;

	if( Addr.a.m_Table == SPACE_HOLD || Addr.a.m_Table == SP_COMT ) {

		CAddress A;

		A.m_Ref = FixMAddr(Addr);

		c = A.a.m_Type == WW ? DoWordWrite(A, pData, uCount) : DoLongWrite(A, pData, uCount);
		}

	else {
		c = Handle3504(Addr, pData, uCount, TRUE);
		}

	ClearArr();

	return c;
	}

CCODE CEI3504TCPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nWrite Word %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount); AfxTrace1("%u ", pData[0]);
	
	StartFrame(16);
		
	AddWord((WORD)Addr.a.m_Offset);
		
	AddWord((WORD)uCount);
		
	AddByte((BYTE)uCount * 2);

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = pData[n];

			AddWord(x);
		}

	if( Transact(TRUE) ) {

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}

	return ++m_pCtx->uWriteErrCt < 3 ? CCODE_ERROR : 1;
	}

CCODE CEI3504TCPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("Write Long %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	StartFrame(16);
		
	AddWord((WORD)Addr.a.m_Offset);
		
	AddWord((WORD)uCount * 2);
		
	AddByte((BYTE)uCount * 4);

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = pData[n];

		AddLong(x);
		}

	if( Transact(TRUE) ) {

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}

	return ++m_pCtx->uWriteErrCt < 3 ? CCODE_ERROR : 1;
	}

// Modbus Address Normalization
DWORD CEI3504TCPDriver::FixMAddr(AREF Addr)
{
	if( Addr.a.m_Table != SP_COMT ) {

		return Addr.m_Ref;
		}

	CAddress A;

	A.m_Ref = Addr.m_Ref;

	A.a.m_Offset += 15359; // actual 15360-15615

	return A.m_Ref;
	}

// 3504 Handling
CCODE CEI3504TCPDriver::Handle3504(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsWrite)
{
	uCount = fIsWrite ? 1 : min(uCount, MAXREAD);

	UINT uSCADA = IsSCADATable(Addr);

	if( uSCADA ) {	// offsets in SCADA items are in numerical order from base (= uSCADA)

		CAddress A;

		A.m_Ref      = Addr.m_Ref;

		A.a.m_Offset = uSCADA;

		return fIsWrite ? DoWordWrite(A, pData, uCount) : DoWordRead(A, pData, uCount);
		}

	MakeArr(Addr.a.m_Table);

	return fIsWrite ? Do3504Write(Addr, pData, uCount) : Do3504Read(Addr, pData, uCount);
	}

CCODE CEI3504TCPDriver::Do3504Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n******* Do3504Read T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount); AfxTrace1("%d ", Addr.a.m_Type);

	CAddress A;

	A.m_Ref     = Addr.m_Ref;

	UINT *pSAdd = m_pSortDataArr;
	UINT *pSPos = m_pSortPosnArr;
	UINT *pAdd  = m_pAddrArr;
	UINT *pPos  = m_pPosnArr;

	DWORD pWork[MAXREAD];

	UINT uDone = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		if( GetTickCount() - m_uTickCount > 0x20 ) {

//**/			AfxTrace2("\r\nTO - Tick = %d Done=%x ", GetTickCount() - m_uTickCount, uDone);

			m_uTickCount = GetTickCount();

			return CheckDoneCt(uDone, uCount);
			}

		UINT uThis = i + Addr.a.m_Offset;

		if( pPos[uThis] < RDONE ) {	// skip read if this address has been read

			UINT uRCt = MakeBlock(uThis, uCount - i); // Get all addresses within range of uThis

//**/			AfxTrace1("\r\n=== RCT = %d === ", uRCt);

			if( !uRCt ) return CheckDoneCt(uDone, i);

//**/			for( UINT xx = 0; xx < uRCt; xx++ ) AfxTrace2("{A=%d P=%d}", m_pSortDataArr[xx], m_pSortPosnArr[xx]);

			UINT uSpan   = pSAdd[uRCt-1] - pSAdd[0] + 1; // number of addresses to read

			A.a.m_Offset = pSAdd[0];

			CCODE uGot = CCODE_ERROR;

			if( Addr.a.m_Type == WW ) uGot = DoWordRead(A, pWork, uSpan);

			else uGot = DoLongRead(A, pWork, uSpan);

			// Code for Debug when register is not in slave

			if( uGot & CCODE_ERROR ) {

// Code used for debugging when slave has not been programmed with this register

//**/				if( m_bRxBuff[7] == 0x83 && m_bRxBuff[8] == 0x2 ) uGot = 1L; else // address not found

				return CheckDoneCt(uDone, uCount);
				}

			UINT uRcv = (UINT)uGot;

			UINT k    = 0;

			while( k < uRCt ) {					// run through sorted list

				UINT uWorkPos = pSAdd[k] - pSAdd[0];		// Data position in pWork

				if( uWorkPos < uRcv ) {				// Data was read

					if( pSPos[k] < RDONE ) {

						UINT uRspPos	= pSPos[k] - Addr.a.m_Offset;		// Position in pData

						pData[uRspPos]	= pWork[uWorkPos];

						pPos[pSPos[k]]	= RDONE;		// flag this address as done

						uDone |= (1 << uRspPos);		// bit for checking return count
						}
					}

				else break;					// register not read

				if( ++k > uRcv ) break;
				}
			}

///**/		else {
///**/			AfxTrace3("\r\n---Skip %d %d %d ", i, pSAdd[i], pSPos[i]);
///**/			}
		}

	return CheckDoneCt(uDone, uCount);
	}

UINT CEI3504TCPDriver::Check4Error(UINT ccode)
{
	if( !(ccode & CCODE_ERROR) ) {

		switch( m_bRxBuff[7] ) {

			case 0x3:	return RXGOOD;

			case 0x83:	return RXRERR; // address not found
			}
		}

	return RXBAD;
	}

CCODE CEI3504TCPDriver::CheckDoneCt(UINT uDone, UINT uCount)
{
	// Get the number of consecutive Data starting with pData[0]

	UINT i = 0;

	while( i < uCount ) {

		if( !( 1 & (uDone >> i) ) ) break;

		i++;
		}

	if( i ) return i;

	return CCODE_ERROR;
	}

CCODE CEI3504TCPDriver::Do3504Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress A;

	A.m_Ref	= Addr.m_Ref;

	A.a.m_Offset = m_pAddrArr[Addr.a.m_Offset];

	uCount  = 1;

	if( Addr.a.m_Type == WW ) return DoWordWrite(A, pData, uCount);

	return DoLongWrite(A, pData, uCount);
	}

// Helpers

void CEI3504TCPDriver::SetArrCount(UINT uTable)
{
	UINT uCount = 0;

	switch( uTable ) {

		case SP_ACC: uCount =  7;	break;
		case SP_ALA: uCount = 18 * 4;	break;
		case SP_ALB: uCount = 11 * 4;	break;
		case SP_ALS: uCount =  9;	break;
		case SP_BCD: uCount =  2;	break;
		case SP_COM: uCount =  3;	break;
		case SP_DAL: uCount =  8 * 8;	break;
		case SP_HUM: uCount =  9;	break;
		case SP_INS: uCount =  7;	break;
		case SP_IPM: uCount = 15;	break;
		case SP_LG2: uCount =  3 * 24;	break;
		case SP_LG8: uCount =  9 *  2;	break;
		case SP_LGI: uCount =  7;	break;
		case SP_LIN: uCount = 34;	break;
		case SP_LDG: uCount =  6 *  2;	break;
		case SP_LMN: uCount =  7 *  2;	break;
		case SP_LOP: uCount = 30 *  2;	break;
		case SP_PID: uCount = 24 *  2;	break;
		case SP_SET: uCount =  4 *  2;	break;
		case SP_SP:  uCount = 21 *  2;	break;
		case SP_TUN: uCount =  2 *  2;	break;
		case SP_MAT: uCount =  3 * 24;	break;
		case SP_MOD: uCount = 24;	break;
		case SP_MID: uCount =  6;	break;
		case SP_MUL: uCount = 12 *  2;	break;
		case SP_PGM: uCount = 64 *  2;	break;
		case SP_PV:  uCount = 10;	break;
		case SP_REC: uCount =  3;	break;
		case SP_RLY: uCount =  1;	break;
		case SP_SWO: uCount =  6;	break;
		case SP_TIM: uCount =  3 * 4;	break;
		case SP_TXD: uCount = 10 * 2;	break;
		case SP_USE: uCount =  1 * 16;	break;
		case SP_ZIR: uCount = 29;	break;
		}

	m_pCtx->uArrCount = uCount;
	}

void CEI3504TCPDriver::SetArrAddr(UINT uTable)
{
	UINT *pA	= m_pAddrArr;

	UINT uQty	= m_pCtx->uArrCount;

	UINT uInc1	= 0;
	UINT uInc2	= 0;

	UINT n		= 0;

	switch( uTable ) {

		case SP_ACC:
			
			pA[0]	= 515;
			pA[1]	= 629;
			pA[2]	= 147;
			pA[3]	= 199;
			pA[4]	= 279;
			pA[5]	= 514;
			pA[6]	= 554;

			break;

		case SP_ALA:

			UINT uBlk;

			uBlk = 0;

			while( n < uQty ) {

				switch( n % 18 ) {

					case  0: pA[n++] = 10250 + uInc2;
					case  1: pA[n++] =   544 + uInc1;
					case  2: pA[n++] = 10246 + uInc2;
					case  3: pA[n++] =   221 + uInc1;
					case  4: pA[n++] = 10248 + uInc2;

					case  5: pA[n++] = GetALASpec(5, uBlk);

					case  6: pA[n++] = 10242 + uInc2;
					case  7: pA[n++] = 10247 + uInc2;
					case  8: pA[n++] =   540 + uInc1;
					case  9: pA[n++] = 10244 + uInc2;
					case 10: pA[n++] =   294 + uInc1;
					case 11: pA[n++] = 10249 + uInc2;
					case 12: pA[n++] = 10245 + uInc2;
					case 13: pA[n++] = 10243 + uInc2;

					case 14: pA[n++] = GetALASpec(14, uBlk);

					case 15: pA[n++] = 10241 + uInc2;
					case 16: pA[n++] =   536 + uInc1;
					case 17: pA[n++] = 10240 + uInc2;
					}

				uBlk++;
				uInc1++;
				uInc2 += 16;
				}

			return;

		case SP_ALB:

			while( n < uQty ) {

				switch( n % 11 ) {

					case  0: pA[n++] = 10314 + uInc2;
					case  1: pA[n++] = 10310 + uInc2;
					case  2: pA[n++] = 10312 + uInc2;
					case  3: pA[n++] = 10306 + uInc2;
					case  4: pA[n++] = 10311 + uInc2;
					case  5: pA[n++] = 10308 + uInc2;
					case  6: pA[n++] = 10313 + uInc2;
					case  7: pA[n++] = 10309 + uInc2;
					case  8: pA[n++] = 10307 + uInc2;
					case  9: pA[n++] = 10305 + uInc2;
					case 10: pA[n++] = 10304 + uInc2;
					}

				uInc2  += 16;
				}

			return;

		case SP_ALS:

			pA[0]	= 10176;
			pA[1]	= 261;
			pA[2]	= 10213;
			pA[3]	= 10188;
			pA[4]	= 274;
			pA[5]	= 10214;
			pA[6]	= 260;
			pA[7]	= 10212;
			pA[8]	= 10200;

			break;

		case SP_BCD:

			pA[0]	= 96;
			pA[1]	= 105;

			break;

		case SP_COM:

			pA[0]	= 131;
			pA[1]	= 8192;
			pA[2]	= 523;

			break;

		case SP_DAL:

			while( n < uQty ) {

				switch( n % 8 ) {

					case 0: pA[n++] = 11274 + uInc2;
					case 1: pA[n++] = 11270 + uInc2;
					case 2: pA[n++] = 11272 + uInc2;
					case 3: pA[n++] = 11271 + uInc2;
					case 4: pA[n++] = 11268 + uInc2;
					case 5: pA[n++] = 11273 + uInc2;
					case 6: pA[n++] = 11269 + uInc2;
					case 7: pA[n++] = 11264 + uInc2;
					}

				uInc2  += 16;
				}

			return;

		case SP_HUM:

			pA[0]	= 13317;
			pA[1]	= 13318;
			pA[2]	= 13313;
			pA[3]	= 13315;
			pA[4]	= 13316;
			pA[5]	= 13320;
			pA[6]	= 13314;
			pA[7]	= 13312;
			pA[8]	= 13319;

			break;

		case SP_INS:

			pA[0]	= 73;
			pA[1]	= 201;
			pA[2]	= 106;
			pA[3]	= 516;
			pA[4]	= 121;
			pA[5]	= 122;
			pA[6]	= 107;

			break;

		case SP_IPM:

			pA[0]	= 133;
			pA[1]	= 4915;
			pA[2]	= 134;
			pA[3]	= 4916;
			pA[4]	= 140;
			pA[5]	= 4919;
			pA[6]	= 138;
			pA[7]	= 4917;
			pA[8]	= 139;
			pA[9]	= 4918;
			pA[10]	= 4920;
			pA[11]	= 4921;
			pA[12]	= 4924;
			pA[13]	= 4922;
			pA[14]	= 4923;

			break;

		case SP_LG2:

			while( n < uQty ) {

				switch( n % 3 ) {

					case  0: pA[n++] = 4822 + uInc2;
					case  1: pA[n++] = 4823 + uInc2;
					case  2: pA[n++] = 4824 + uInc2;
					}

				uInc2  += 3;
				}

			return;

		case SP_LG8:

			while( n < uQty ) {

				switch( n % 9 ) {

					case  0: pA[n++] = 4894 + uInc2;
					case  1: pA[n++] = 4895 + uInc2;
					case  2: pA[n++] = 4896 + uInc2;
					case  3: pA[n++] = 4897 + uInc2;
					case  4: pA[n++] = 4898 + uInc2;
					case  5: pA[n++] = 4899 + uInc2;
					case  6: pA[n++] = 4900 + uInc2;
					case  7: pA[n++] = 4901 + uInc2;
					case  8: pA[n++] = 4902 + uInc2;
					}

				uInc2  += 9;
				}

			return;

		case SP_LGI:

			pA[0]	= 124;
			pA[1]	= 123;
			pA[2]	=  45;
			pA[3]	=  54;
			pA[4]	= 361;
			pA[5]	=  89;
			pA[6]	= 362;

			break;

		case SP_LIN:

			pA[0]	= 618;
			pA[1]	= 602;
			pA[2]	= 603;
			pA[3]	= 604;
			pA[4]	= 605;
			pA[5]	= 606;
			pA[6]	= 607;
			pA[7]	= 608;
			pA[8]	= 609;
			pA[9]	= 610;
			pA[10]	= 611;
			pA[11]	= 612;
			pA[12]	= 613;
			pA[13]	= 614;
			pA[14]	= 615;
			pA[15]	= 616;
			pA[16]	= 601;
			pA[17]	= 619;
			pA[18]	= 622;
			pA[19]	= 623;
			pA[20]	= 624;
			pA[21]	= 625;
			pA[22]	= 626;
			pA[23]	= 627;
			pA[24]	= 628;
			pA[25]	= 630;
			pA[26]	= 631;
			pA[27]	= 632;
			pA[28]	= 633;
			pA[29]	= 634;
			pA[30]	= 635;
			pA[31]	= 636;
			pA[32]	= 637;
			pA[33]	= 621;

			break;

		case SP_LDG:

			while( n < uQty ) {

				switch( n % 6 ) {

					case  0: pA[n++] = 116 + uInc2;
					case  1: pA[n++] =  39 + uInc2;
					case  2: pA[n++] =  55 + uInc2;
					case  3: pA[n++] = 263 + uInc2;
					case  4: pA[n++] = 214 + uInc2;
					case  5: pA[n++] = 258 + uInc2;
					}

				uInc2  += 1024;
				}

			return;

		case SP_LMN:

			while( n < uQty ) {

				switch( n % 7 ) {

					case  0: pA[n++] =   4 + uInc2;
					case  1: pA[n++] = 273 + uInc2;
					case  2: pA[n++] = 268 + uInc2;
					case  3: pA[n++] =   1 + uInc2;
					case  4: pA[n++] = 289 + uInc2;
					case  5: pA[n++] =   2 + uInc2;
					case  6: pA[n++] =   5 + uInc2;
					}

				uInc2  += 1024;
				}

			return;

		case SP_LOP:

			while( n < uQty ) {

				switch( n % 30 ) {

					case  0: pA[n++] =  86 + uInc2;
					case  1: pA[n++] =  85 + uInc2;
					case  2: pA[n++] = 350 + uInc2;
					case  3: pA[n++] =  53 + uInc2;
					case  4: pA[n++] = 317 + uInc2;
					case  5: pA[n++] =  21 + uInc2;
					case  6: pA[n++] =  16 + uInc2;
					case  7: pA[n++] =  88 + uInc2;
					case  8: pA[n++] = 126 + uInc2;
					case  9: pA[n++] = 318 + uInc2;
					case 10: pA[n++] = 319 + uInc2;
					case 11: pA[n++] = 524 + uInc2;
					case 12: pA[n++] = 565 + uInc2;
					case 13: pA[n++] =  97 + uInc2;
					case 14: pA[n++] =  98 + uInc2;
					case 15: pA[n++] =  99 + uInc2;
					case 16: pA[n++] = 532 + uInc2;
					case 17: pA[n++] = 209 + uInc2;
					case 18: pA[n++] = 556 + uInc2;
					case 19: pA[n++] =   3 + uInc2;
					case 20: pA[n++] =  84 + uInc2;
					case 21: pA[n++] =  30 + uInc2;
					case 22: pA[n++] =  31 + uInc2;
					case 23: pA[n++] =  46 + uInc2;
					case 24: pA[n++] = 210 + uInc2;
					case 25: pA[n++] =  37 + uInc2;
					case 26: pA[n++] =  34 + uInc2;
					case 27: pA[n++] = 553 + uInc2;
					case 28: pA[n++] = 127 + uInc2;
					case 29: pA[n++] = 128 + uInc2;
					}

				uInc2  += 1024;
				}

			return;

		case SP_PID:

			while( n < uQty ) {

				switch( n % 24 ) {

					case  0: pA[n++] =  72 + uInc2;
					case  1: pA[n++] = 185 + uInc2;
					case  2: pA[n++] = 153 + uInc2;
					case  3: pA[n++] = 152 + uInc2;
					case  4: pA[n++] =  18 + uInc2;
					case  5: pA[n++] = 118 + uInc2;
					case  6: pA[n++] =  17 + uInc2;
					case  7: pA[n++] = 117 + uInc2;
					case  8: pA[n++] =   9 + uInc2;
					case  9: pA[n++] =  51 + uInc2;
					case 10: pA[n++] = 183 + uInc2;
					case 11: pA[n++] =   8 + uInc2;
					case 12: pA[n++] =  49 + uInc2;
					case 13: pA[n++] = 181 + uInc2;
					case 14: pA[n++] =  83 + uInc2;
					case 15: pA[n++] =  28 + uInc2;
					case 16: pA[n++] =  50 + uInc2;
					case 17: pA[n++] = 182 + uInc2;
					case 18: pA[n++] =   6 + uInc2;
					case 19: pA[n++] =  48 + uInc2;
					case 20: pA[n++] = 180 + uInc2;
					case 21: pA[n++] =  19 + uInc2;
					case 22: pA[n++] =  52 + uInc2;
					case 23: pA[n++] = 184 + uInc2;
					}

				uInc2  += 1024;
				}

			return;

		case SP_SET:

			while( n < uQty ) {

				switch( n % 4 ) {

					case  0: pA[n++] = 512 + uInc2;
					case  1: pA[n++] = 513 + uInc2;
					case  2: pA[n++] =   7 + uInc2;
					case  3: pA[n++] = 550 + uInc2;
					}

				uInc2  += 1024;
				}

			return;

		case SP_SP:

			while( n < uQty ) {

				switch( n % 21 ) {

					case  0: pA[n++] = 485 + uInc2;
					case  1: pA[n++] = 276 + uInc2;
					case  2: pA[n++] = 527 + uInc2;
					case  3: pA[n++] =  12 + uInc2;
					case  4: pA[n++] =  11 + uInc2;
					case  5: pA[n++] =  35 + uInc2;
					case  6: pA[n++] =  78 + uInc2;
					case  7: pA[n++] = 277 + uInc2;
					case  8: pA[n++] =  24 + uInc2;
					case  9: pA[n++] =  25 + uInc2;
					case 10: pA[n++] = 111 + uInc2;
					case 11: pA[n++] = 155 + uInc2;
					case 12: pA[n++] = 112 + uInc2;
					case 13: pA[n++] = 156 + uInc2;
					case 14: pA[n++] =  15 + uInc2;
					case 15: pA[n++] = 526 + uInc2;
					case 16: pA[n++] = 528 + uInc2;
					case 17: pA[n++] =  27 + uInc2;
					case 18: pA[n++] = 486 + uInc2;
					case 19: pA[n++] =  66 + uInc2;
					case 20: pA[n++] =  67 + uInc2;
					}

				uInc2  += 1024;
				}

			return;

		case SP_TUN:

			while( n < uQty ) {

				switch( n % 2 ) {

					case 0: pA[n++] = 270 + uInc2;
					case 1: pA[n++] = 269 + uInc2;
					}

				uInc2  += 1024;
				}

			return;

		case SP_MAT:

			while( n < uQty ) {

				switch( n % 3 ) {

					case 0: pA[n++] = 4750 + uInc2;
					case 1: pA[n++] = 4751 + uInc2;
					case 2: pA[n++] = 4752 + uInc2;
					}

				uInc2  += 3;
				}

			return;

		case SP_MOD:

			pA[0]	= 364;
			pA[1]	= 365;
			pA[2]	= 366;
			pA[3]	= 367;
			pA[4]	= 368;
			pA[5]	= 369;
			pA[6]	= 216;
			pA[7]	= 104;
			pA[8]	= 103;
			pA[9]	= 208;
			pA[10]	= 142;
			pA[11]	= 290;
			pA[12]	= 370;
			pA[13]	= 371;
			pA[14]	= 372;
			pA[15]	= 373;
			pA[16]	= 374;
			pA[17]	= 375;
			pA[18]	= 376;
			pA[19]	= 377;
			pA[20]	= 378;
			pA[21]	= 379;
			pA[22]	= 380;
			pA[23]	= 381;

			break;

		case SP_MID:

			pA[0]	= 12707;
			pA[1]	= 12771;
			pA[2]	= 12835;
			pA[3]	= 12899;
			pA[4]	= 12963;
			pA[5]	= 13027;

			break;

		case SP_MUL:

			while( n < uQty ) {

				switch( n % 12 ) {

					case  0: pA[n++] = 5017 + uInc2;
					case  1: pA[n++] = 5006 + uInc2;
					case  2: pA[n++] = 5007 + uInc2;
					case  3: pA[n++] = 5008 + uInc2;
					case  4: pA[n++] = 5009 + uInc2;
					case  5: pA[n++] = 5010 + uInc2;
					case  6: pA[n++] = 5011 + uInc2;
					case  7: pA[n++] = 5012 + uInc2;
					case  8: pA[n++] = 5013 + uInc2;
					case  9: pA[n++] = 5015 + uInc2;
					case 10: pA[n++] = 5016 + uInc2;
					case 11: pA[n++] = 5014 + uInc2;
					}

				uInc2  += 12;
				}

			return;

//		case SP_PGM:	// handled separately
//		case SP_PGS:	break;

		case SP_PV:

			pA[0]	= 534;
			pA[1]	= 215;
			pA[2]	= 38;
			pA[3]	= 101;
			pA[4]	= 202;
			pA[5]	= 141;
			pA[6]	= 360;
			pA[7]	= 548;
			pA[8]	= 549;
			pA[9]	= 578;

			break;

		case SP_REC:

			pA[1]	= 315;
			pA[2]	= 316;
			pA[3]	= 313;

			break;

		case SP_RLY:

			pA[0]	= 363;

			break;

		case SP_SWO:

			pA[0]	= 288;
			pA[1]	= 4927;
			pA[2]	= 286;
			pA[3]	= 4925;
			pA[4]	= 287;
			pA[5]	= 4926;

			break;

		case SP_TIM:

			while( n < uQty ) {

				switch( n % 3 ) {

					case 0: pA[n++] = 4995 + uInc2;
					case 1: pA[n++] = 4996 + uInc2;
					case 2: pA[n++] = 4994 + uInc2;
					}

				uInc2  += 3;
				}

			return;

		case SP_TXD:

			while( n < uQty ) {

				switch( n % 10 ) {

					case 0: pA[n++] = 237 + uInc2;
					case 1: pA[n++] = 238 + uInc2;
					case 2: pA[n++] = 233 + uInc2;
					case 3: pA[n++] = 232 + uInc2;
					case 4: pA[n++] = 235 + uInc2;
					case 5: pA[n++] = 234 + uInc2;
					case 6: pA[n++] = 226 + uInc1;
					case 7: pA[n++] = 231 + uInc2;
					case 8: pA[n++] = 225 + uInc1;
					case 9: pA[n++] = 236 + uInc2;
					}

				uInc1  += 2;
				uInc2  += 8;
				}

			return;

		case SP_USE:

			for( n = 0; n < uQty; n++ ) {

				pA[n] = 4962 + n;
				}
			break;

		case SP_ZIR:

			pA[0]	= 13256;
			pA[1]	= 13251;
			pA[2]	= 13248;
			pA[3]	= 13268;
			pA[4]	= 13252;
			pA[5]	= 13263;
			pA[6]	= 13274;
			pA[7]	= 13254;
			pA[8]	= 13253;
			pA[9]	= 13270;
			pA[10]	= 13255;
			pA[11]	= 13261;
			pA[12]	= 13260;
			pA[13]	= 13271;
			pA[14]	= 13259;
			pA[15]	= 13250;
			pA[16]	= 13262;
			pA[17]	= 13258;
			pA[18]	= 13275;
			pA[19]	= 13272;
			pA[20]	= 13257;
			pA[21]	= 13267;
			pA[22]	= 13273;
			pA[23]	= 13264;
			pA[24]	= 13269;
			pA[25]	= 13266;
			pA[26]	= 13249;
			pA[27]	= 13276;
			pA[28]	= 13265;

			break;
		}
	}

UINT CEI3504TCPDriver::GetALASpec(UINT uSel, UINT uBlk)
{
	switch( uBlk ) {

		case 1: return uSel == 5 ? 68 : 14;
		case 2: return uSel == 5 ? 69 : 81;
		case 3: return uSel == 5 ? 71 : 82;
		}

	return uSel == 5 ? 47 : 13;
	}

void CEI3504TCPDriver::ClearArr(void)
{
	if( m_pAddrArr ) {

		delete [] m_pAddrArr;

		m_pAddrArr = NULL;
		}

	if( m_pPosnArr ) {

		delete [] m_pPosnArr;

		m_pPosnArr = NULL;
		}

	if( m_pSortDataArr ) {

		delete [] m_pSortDataArr;

		m_pSortDataArr = NULL;
		}

	if( m_pSortPosnArr ) {

		delete [] m_pSortPosnArr;

		m_pSortPosnArr = NULL;
		}
	}

void CEI3504TCPDriver::MakeArr(UINT uTable)
{
	ClearArr();

	SetArrCount(uTable);

	MakeAddr();

	SetArrAddr(uTable);
	}

void CEI3504TCPDriver::MakeAddr(void)
{
	UINT uSize = m_pCtx->uArrCount;

	m_pAddrArr	= new UINT [uSize];

	m_pPosnArr	= new UINT [uSize];

	m_pSortDataArr	= new UINT [uSize];

	m_pSortPosnArr	= new UINT [uSize];

	for( UINT k = 0; k < uSize; k++ ) m_pPosnArr[k] = k; // initial positions

	uSize *= sizeof(UINT);

	memset(m_pAddrArr, 0, uSize);

	memset(m_pSortDataArr, 0, uSize);

	memset(m_pSortPosnArr, 0xFF, uSize);
	}

UINT CEI3504TCPDriver::IsSCADATable(CAddress Addr)
{
	switch( Addr.a.m_Table ) {

		case SP_PGM:
			return PGMBASE + Addr.a.m_Offset;

		case SP_PGS:
			return PGSBASE + Addr.a.m_Offset;
		}

	return 0;
	}

UINT CEI3504TCPDriver::MakeBlock(UINT uStart, UINT uCount)
{
	UINT *pA   = m_pAddrArr;

//**/	AfxTrace0("\r\nMakeBlock ");

	UINT uThis = pA[uStart];

	UINT *pP   = m_pPosnArr;

	if( uCount == 1 ) {

		m_pSortDataArr[0] = uThis;

		m_pSortPosnArr[0] = pP[uStart];

//**/		AfxTrace1("--- Only 1 %d ", uThis);

		return 1;
		}

	UINT uMaxP = uStart + uCount; // A useable Array Position must be < uMaxP

	UINT uCt   = 0;

	UINT uTarg;

	UINT pAWork[MAXREAD];
	UINT pPWork[MAXREAD];

	memset(pAWork, 0xFF, MAXREAD);
	memset(pPWork, 0xFF, MAXREAD);

	UINT uEnd = uStart + uCount;

//**/	UINT uMax = uThis;

	for( UINT i = uStart; i < uEnd; i++ ) { // check for offsets that are within range of uCount

		uTarg = pA[i];

		if( (uTarg >= uThis) && (uTarg < uThis + MAXREAD) && (pP[i] <= uMaxP) ) { // cache only addresses within range of start address and offset

			pAWork[uCt] = uTarg;
			pPWork[uCt] = pP[i]; // position in original list

//**/			if( !uCt ) AfxTrace1("%d - ", pAWork[0]);
//**/			if( pAWork[uCt] > uMax ) uMax = pAWork[uCt];

			uCt++; // quantity of results to return
			}
		}

//**/	AfxTrace1("%d ", uMax); // show range of addresses accessible

	DoAddrSort(uCt, pAWork, pPWork);

	return uCt;
	}

void CEI3504TCPDriver::DoAddrSort(UINT uCount, UINT * pASrc, UINT * pPSrc)
{
	UINT *pADest = m_pSortDataArr;
	UINT *pPDest = m_pSortPosnArr;

	for( UINT i = 0; i < uCount; i++ ) {

		pADest[i] = pASrc[i];
		pPDest[i] = pPSrc[i];
		}

	i = 0;

	UINT j = 1;

	while( j < uCount ) {

		if( pADest[i] > pADest[j] ) {

			SwapPositions(pADest, pPDest, i, j);

			i = 0;
			j = 1;
			}

		else {
			i++;
			j++;
			}
		}
	}

void CEI3504TCPDriver::SwapPositions(UINT *pAdd, UINT *pPos, UINT ui, UINT uj)
{
	SwapItem(pAdd, ui, uj);
	SwapItem(pPos, ui, uj);
	}

void CEI3504TCPDriver::SwapItem(UINT *p, UINT ui, UINT uj)
{
	UINT u = p[ui];
	p[ui]  = p[uj];
	p[uj]  = u;
	}

// Socket Management

BOOL CEI3504TCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	CloseSocket(FALSE);

	return FALSE;
	}

BOOL CEI3504TCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   Port;

		IP   = (IPADDR const &) m_pCtx->m_IP1;

		Port = WORD(m_pCtx->m_uPort);

		if( m_pCtx->m_pSock->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		return FALSE;
		}

	return FALSE;
	}

void CEI3504TCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
