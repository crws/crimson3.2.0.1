
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Data Block Ref Class
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

class CDatablockRef
{
	public:
		// Constructor
		CDatablockRef(DWORD dwRef, WORD wBlock, WORD wOffset, BYTE bType, WORD wExtent, IHelper * pHelp);

		// Destructor
		~CDatablockRef(void);

		// Access
		WORD  GetBlock(void);
		WORD  GetOffset(void);
		WORD  GetOffset(DWORD dwRef);
		BYTE  GetType(void);
		WORD  GetExtent(void);
		DWORD GetRef(void);
		UINT  GetMaxCount(DWORD dwRef);

		// Operations
		BOOL Match(DWORD dwRef);

		// Public Members
		CDatablockRef * m_pNext;
		CDatablockRef * m_pPrev;

	protected:

		// Data Members
		DWORD m_Ref;
		WORD  m_Block;
		WORD  m_Offset;
		BYTE  m_Type;
		WORD  m_Extent;

		// Helper
		IHelper * m_pHelper;

		// Implementation
		BYTE  GetSizeOfEntity(void);
		DWORD GetNextRef(void);
		void  Mask(DWORD &Ref);
	};


// End of File

