
#include "intern.hpp"

#include "s71ktcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series PLC TCP Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CS71KTcpMasterDriver);

// Constructor

CS71KTcpMasterDriver::CS71KTcpMasterDriver(void)
{
	m_Ident = DRIVER_ID;
	
	m_pDev  = NULL;

	m_uKeep = 0;
	}

// Destructor

CS71KTcpMasterDriver::~CS71KTcpMasterDriver(void)
{
	}

// Device

CCODE MCALL CS71KTcpMasterDriver::DeviceOpen(IDevice *pDevice)
{	
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pDev = (CS71KDev *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pDev  = new CS71KDev;

			m_pCtx  = m_pDev;

			m_pBase = m_pCtx;

			m_pDev->m_IP1		= GetAddr(pData);
			m_pDev->m_uPort		= GetWord(pData);
			m_pDev->m_fKeep		= GetByte(pData);
			m_pDev->m_fPing		= GetByte(pData);
			m_pDev->m_uTime1	= GetWord(pData);
			m_pDev->m_uTime2	= GetWord(pData);
			m_pDev->m_uTime3	= GetWord(pData);
			m_pDev->m_bType		= 0;
			m_pDev->m_bConn		= 0;
			m_pDev->m_bSlot		= GetByte(pData);
			m_pDev->m_bRack		= GetByte(pData);
			m_pDev->m_bClient	= 0x00;
			m_pDev->m_pSock		= NULL;
			m_pDev->m_uLast		= GetTickCount();
			m_pDev->m_fConnect	= FALSE;
			m_pDev->m_fConfirm	= FALSE;
			m_pDev->m_BlkOff	= 0;
			
			m_pDev->m_fDirty        = FALSE;
			m_pDev->m_fAux          = FALSE;
			m_pDev->m_IP2		= GetAddr(pData);
			m_pDev->m_fSwitchIP	= FALSE;

			m_pDev->m_bSlot2	= GetByte(pData);
			m_pDev->m_bRack2	= GetByte(pData);
			m_pDev->m_Blocks	= GetLong(pData);

			m_pDev->m_pHead		= NULL;
			m_pDev->m_pTail		= NULL;

			for( UINT b = 0; b < m_pDev->m_Blocks; b++ ) {

				DWORD dwRef  = GetLong(pData);
				WORD wBlock  = GetWord(pData);
				WORD wOffset = GetWord(pData);
				BYTE bType   = GetByte(pData);
				WORD wExtent = GetWord(pData);

				CDatablockRef * pBlock = new CDatablockRef(dwRef,
									   wBlock,
									   wOffset,
									   bType,
									   wExtent,
									   m_pHelper);

				if( pBlock ) {

					AfxListAppend(m_pDev->m_pHead,
						      m_pDev->m_pTail,
						      pBlock,
						      m_pNext,
						      m_pPrev);
					}
				}

			m_pCtx->m_TSAP	 = GetByte(pData);
			m_pCtx->m_STsap	 = GetWord(pData);
			m_pCtx->m_CTsap	 = GetWord(pData);
			m_pCtx->m_STsap2 = GetWord(pData);
			m_pCtx->m_CTsap2 = GetWord(pData);

			pDevice->SetContext(m_pDev);
		
			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx  = m_pDev;

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CS71KTcpMasterDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		RemoveBlocks();

		delete m_pDev;

		m_pDev  = NULL;
		 
		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CS71KTcpMasterDriver::Ping(void)
{
	if( m_pDev->m_fPing ) {

		DWORD IP;

		if( !m_pDev->m_fAux ) {

			IP = m_pDev->m_IP1;
			}
		else
			IP = m_pDev->m_IP2;
		
		if( CheckIP(IP, m_pDev->m_uTime2) == NOTHING ) {
			
			if( m_pDev->m_IP2 ) {

				m_pDev->m_fAux = !m_pDev->m_fAux;
				}

			return CCODE_ERROR; 
			}

		return CCODE_SUCCESS;		
		}

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = spaceMB;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrByteAsByte;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);  
	}

// Overridables

void CS71KTcpMasterDriver::AddParams(BYTE bService, UINT uType, UINT uTable, UINT uAddr, UINT uExtra, UINT& uCount, AREF Addr)
{
	CDatablockRef * pBlock = NULL;

	UINT uOffset  = 0;

	if( IsDataBlock(uTable) ) {

		pBlock = FindBlock(Addr.m_Ref);

		if( pBlock ) {

			uOffset = pBlock->GetOffset(Addr.m_Ref);

			MakeMin(uCount, pBlock->GetMaxCount(Addr.m_Ref));
			}
		}
		
	BYTE  bType  = FindType(uType, uTable);

	WORD  wBlock = pBlock ? pBlock->GetBlock() : 0;
	
	BYTE  bArea  = pBlock ? FindArea(spaceDB)  : FindArea(uTable);

	DWORD dwAddr = pBlock ? FindAddr(uType, wBlock, uOffset) : FindAddr(uType, uTable, uAddr);

	m_bParamHead = m_uPtr;

	UINT uBlocks = 1;

	AddByte(bService);		// SERVICE_ID

	AddByte(LOBYTE(uBlocks));	// NO_VAR

	for( UINT i = 0; i < uBlocks; i++ ) {

		AddByte(0x12);		// VAR_SPC
		AddByte(0x0A);		// VADDR_LG
		AddByte(0x10);		// SYNTAX_ID

		AddByte(bType);		// Type
		AddWord(uCount);	// Elements
		AddWord(wBlock);	// Block
		AddByte(bArea);		// Area

		AddByte(LOBYTE(HIWORD(dwAddr)));
		AddByte(HIBYTE(LOWORD(dwAddr)));
		AddByte(LOBYTE(LOWORD(dwAddr)));

		dwAddr++;
		}

	m_bParamTail = m_uPtr;
	}

BYTE CS71KTcpMasterDriver::FindType(UINT uType, UINT uTable)
{
	switch( uType ) {

		case addrBitAsBit:	return TC_BOOL;
		case addrByteAsByte:	return TC_BYTE;
		case addrByteAsWord:	return TC_WORD;
		case addrByteAsLong:	return TC_DWORD;
		case addrByteAsReal:	return TC_DWORD;
		}

	return 0;
	}

BYTE CS71KTcpMasterDriver::FindArea(UINT uTable)
{
	switch( uTable ) {

		case spaceDB:	return idDB;
		case spaceQB:	return idQB;
		case spaceIB:	return idIB;
		case spaceMB:	return idMB;
		}

	return 0;
	}

UINT CS71KTcpMasterDriver::FindBits(UINT uType, UINT uTable)
{
	switch( uType ) {

		case addrBitAsBit:	return 1;

		case addrByteAsByte:	return 8 * 1;
		case addrByteAsWord:	return 8 * 2;
		case addrByteAsLong:	return 8 * 4;
		case addrByteAsReal:	return 8 * 4;
		}

	return 0;
	}

void CS71KTcpMasterDriver::AddPadding(UINT uTable)
{
	}

BOOL  CS71KTcpMasterDriver::IsTimer(UINT uTable)
{
	return FALSE;
	}

BOOL  CS71KTcpMasterDriver::IsCounter(UINT uTable)
{
	return FALSE;
	}

BOOL CS71KTcpMasterDriver::EatWrite(UINT uTable)
{
	switch( uTable ) {

		case spaceIB:
				
			return TRUE;
		}

	return FALSE;
	}

UINT CS71KTcpMasterDriver::GetTable(UINT uTable)
{
	return uTable;
	}

UINT CS71KTcpMasterDriver::GetMaxBits(void)
{
	return 1600;
	}

BOOL  CS71KTcpMasterDriver::ConnectionRequest(void)
{
	if( m_pCtx->m_fConnect ) {

		return TRUE;
		}

	WORD wSTsap = m_pCtx->m_fAux ? m_pCtx->m_STsap2 : m_pCtx->m_STsap;

	WORD wCTsap = m_pCtx->m_fAux ? m_pCtx->m_CTsap2 : m_pCtx->m_CTsap;

	StartFrame();
	
	AddByte(0x11);	

	AddByte(0xE0);	

	AddWord(0x00);	

	AddByte(0x00);	

	AddByte(0x02);	 

	AddByte(0x00);

	AddByte(0xC1);

	AddByte(0x02);

	if( (m_pCtx->m_TSAP ) ) {

		AddWord(wCTsap);
		}
	else {
		AddByte(m_pCtx->m_bClient);

		AddByte(0x00);
		}
	
	AddByte(0xC2);

	AddByte(0x02);

	if( (m_pCtx->m_TSAP ) ) {

		AddWord(wSTsap);
		}
	else {
		AddByte(m_pDev->m_fAux ? m_pDev->m_bRack2 + 1 : m_pDev->m_bRack + 1); 

		AddByte(m_pDev->m_fAux ? m_pDev->m_bSlot2 + 0 : m_pDev->m_bSlot + 0); 
		}

	AddByte(0xC0);

	AddByte(0x01);

	AddByte(0x09);

	TermFrame();

	m_pCtx->m_fConnect = ( Transact() );

	return m_pCtx->m_fConnect;
	}


// Data Block List Operations

void CS71KTcpMasterDriver::RemoveBlocks(void)
{
	CDatablockRef * pBlock = m_pDev->m_pHead;

	while( pBlock ) {

		AfxListRemove(m_pDev->m_pHead, m_pDev->m_pTail, pBlock, m_pPrev, m_pNext);

		delete pBlock;

		pBlock = m_pDev->m_pHead;
		}
	}

CDatablockRef * CS71KTcpMasterDriver::FindBlock(DWORD dwRef)
{
	CDatablockRef * pBlock = m_pDev->m_pHead;

	while( pBlock ) {

		if( pBlock->Match(dwRef) ) {

			return pBlock;
			}

		pBlock = pBlock->m_pNext;
		}

	return NULL;
	}

// Helpers

BOOL CS71KTcpMasterDriver::IsDataBlock(UINT uTable)
{
	return uTable >= spaceDB;
	}

// End of File

