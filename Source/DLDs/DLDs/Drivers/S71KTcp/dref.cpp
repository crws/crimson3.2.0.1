//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 1000 Series Data Block Ref Class
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

#include "dref.hpp"

// Constructor

CDatablockRef::CDatablockRef(DWORD dwRef, WORD wBlock, WORD wOffset, BYTE bType, WORD wExtent, IHelper * pHelp)
{
	Mask(dwRef);

	m_Ref	  = dwRef;

	m_Block   = wBlock;

	m_Offset  = wOffset;

	m_Type    = bType;

	m_Extent  = wExtent;

	m_pHelper = pHelp;

	m_pNext	  = NULL;

	m_pPrev   = NULL;
	}

// Destructor

CDatablockRef::~CDatablockRef(void)
{
	}

// Access

WORD CDatablockRef::GetBlock(void)
{
	return m_Block;
	}

WORD CDatablockRef::GetOffset(void)
{
	return m_Offset;
	}

WORD CDatablockRef::GetOffset(DWORD dwRef)
{
	Mask(dwRef);

	if( Match(dwRef) ) {

		return m_Offset + (dwRef - m_Ref); 
		}

	return 0;
	}

BYTE CDatablockRef::GetType(void)
{
	return m_Type;
	}

WORD CDatablockRef::GetExtent(void)
{
	return m_Extent;
	}

DWORD CDatablockRef::GetRef(void)
{
	return m_Ref;
	}

UINT CDatablockRef::GetMaxCount(DWORD dwRef)
{
	Mask(dwRef);

	if( Match(dwRef) ) {

		UINT uMax = GetNextRef();

		if( uMax > dwRef ) {

			return max((uMax - dwRef) / GetSizeOfEntity(), 1);
			}
		}

	return 1;
	}

// Operations

BOOL CDatablockRef::Match(DWORD dwRef)
{
	Mask(dwRef);

	if( dwRef >= m_Ref ) {

		if( dwRef < GetNextRef() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Implementation

BYTE CDatablockRef::GetSizeOfEntity(void)
{
	switch( m_Type ) {

		case addrByteAsWord:	return 2;

		case addrByteAsReal:
		case addrByteAsLong:	return 4;		
		}

	return 1;
	}

DWORD CDatablockRef::GetNextRef(void)
{
	return m_Ref + m_Extent * GetSizeOfEntity();
	}

void CDatablockRef::Mask(DWORD &Ref)
{
	Ref &= 0x0FFFFFFF;
	}

// End of File
