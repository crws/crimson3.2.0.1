#include "s7isotcp.hpp"

#include "dref.hpp"

//////////////////////////////////////////////////////////////////////////
//
// S7 1000 Series Data Block Structure
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// S7 1000 Series Enumerations
//

enum Space {

	spaceQB	= 0x1,
	spaceIB = 0x2,
	spaceMB = 0x3,
	spaceDB = 0xA,
	};

enum Id {

	idQB = 0x82,
	idIB = 0x81,
	idMB = 0x83,
	idDB = 0x84,
	};


//////////////////////////////////////////////////////////////////////////
//
// S7 1000 Series TCP Driver
//

class CS71KTcpMasterDriver : public CS7IsoTcpMasterDriver
{
	public:
		// Constructor
		CS71KTcpMasterDriver(void);

		// Destructor
		~CS71KTcpMasterDriver(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
	   
		// Entry Points
		DEFMETH(CCODE) Ping (void);
				
	protected:
		
		// Device Context
		struct CS71KDev : CS7IsoTcpMasterDriver::CContext
		{
			BYTE	m_bSlot2;
			BYTE	m_bRack2;
			DWORD	m_Blocks;

			CDatablockRef *	m_pHead;
			CDatablockRef *	m_pTail;
			};

		// Data Members

		CS71KDev * m_pDev;

		// Overridables
		void AddParams(BYTE bService, UINT uType, UINT uTable, UINT uAddr, UINT uExtra, UINT& uCount, AREF Addr);
		UINT FindBits(UINT uType, UINT uTable);
		void AddPadding(UINT uTable);
		BYTE FindType(UINT uType, UINT uTable);
		BYTE FindArea(UINT uTable);
		BOOL IsTimer(UINT uTable);
		BOOL IsCounter(UINT uTable);
		BOOL EatWrite(UINT uTable);
		UINT GetTable(UINT uTable);
		UINT GetMaxBits(void);
		BOOL ConnectionRequest(void);

		// Data Block List Operations
		void		RemoveBlocks(void);
		CDatablockRef * FindBlock(DWORD dwRef);

		// Helpers
		BOOL IsDataBlock(UINT uTable);
	};
		

// End of File

