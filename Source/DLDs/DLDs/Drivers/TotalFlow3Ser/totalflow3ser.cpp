
#include "intern.hpp"

#include "totalflow3ser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Serial Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

INSTANTIATE(CTotalFlow3SerMasterDriver);

// Constructor

CTotalFlow3SerMasterDriver::CTotalFlow3SerMasterDriver(void)
{
	m_Ident	 = DRIVER_ID;

	m_pCtx	 = NULL;

	m_fDelay = FALSE;
	}

// Destructor

CTotalFlow3SerMasterDriver::~CTotalFlow3SerMasterDriver(void)
{
	}

// Configuration

void MCALL CTotalFlow3SerMasterDriver::Load(LPCBYTE pData)
{
	}

void MCALL CTotalFlow3SerMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);

	if( Config.m_uPhysical == physicalRS485 || Config.m_uPhysical == physicalRS422Master ) {

		m_fDelay = TRUE;
		}
	}

// Management

void MCALL CTotalFlow3SerMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CTotalFlow3SerMasterDriver::Open(void)
{
	}
		
// Device

CCODE MCALL CTotalFlow3SerMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CCtx;

			CTotalFlow2Master::m_pCtx   = m_pCtx;

			CTotalFlow3Master::m_pBase3 = m_pCtx;

			m_pCtx->m_Name		= GetString(pData);
			m_pCtx->m_Code		= GetString(pData);
			m_pCtx->m_uUpdate	= GetLong(pData);
			m_pCtx->m_uTimeout	= GetLong(pData);
			m_pCtx->m_uLastUpdate	= 0;
			m_pCtx->m_fForceUpdate	= FALSE;
			m_pCtx->m_fUseAppKey	= GetByte(pData) ? TRUE : FALSE;

			GetAppKey(pData, m_pCtx);

			GetSlots(pData, m_pCtx);

			m_pBase3->m_bStringSize = GetByte(pData) ? 16 : 6;

			m_pCtx->m_uEstab = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_wUnkey = GetWord(pData);

			pDevice->SetContext(m_pCtx);

			LookupApps();

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	CTotalFlow3Master::m_pCtx   = m_pCtx;

	CTotalFlow3Master::m_pBase3 = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CTotalFlow3SerMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		CleanupSlots();

		Free(m_pCtx->m_Name);

		Free(m_pCtx->m_Code);

		delete [] m_pCtx->m_pKey;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CTotalFlow3Master::DeviceClose(fPersist);
	}
		
// Entry Points

CCODE MCALL CTotalFlow3SerMasterDriver::Ping (void)
{
	AbortLink();

	return CCODE_SUCCESS;
	}

// Transport

BOOL CTotalFlow3SerMasterDriver::CheckLink(void)
{
	return TRUE;
	}

void CTotalFlow3SerMasterDriver::AbortLink(void)
{
	while( Recv(500) < NOTHING );
	}

BOOL CTotalFlow3SerMasterDriver::Send(PBYTE pBuff, UINT uLength)
{
	if( m_fDelay ) {

		Sleep(m_pCtx->m_wUnkey);
		}

	m_pData->ClearRx();

	m_pData->Write(pBuff, uLength, NOTHING);

	m_pData->Write(NULL, 0, NOTHING);

	return TRUE;
	}

UINT CTotalFlow3SerMasterDriver::Recv(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
