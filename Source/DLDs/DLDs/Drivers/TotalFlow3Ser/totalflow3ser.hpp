#ifndef	INCLUDE_totalflow3ser_HPP
	
#define	INCLUDE_totalflow3ser_HPP

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Serial Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#include "totalflow3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Serial Driver
//

class CTotalFlow3SerMasterDriver : public CTotalFlow3Master
{
	public:
		// Constructor
		CTotalFlow3SerMasterDriver(void);

		// Destructor
		~CTotalFlow3SerMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:

		struct CCtx : public CTotalFlow3Master::CBase3Ctx
		{			
			WORD m_wUnkey;
			};

		CCtx	* m_pCtx;
		BOOL	  m_fDelay;

		// Transport
		BOOL CheckLink(void);
		void AbortLink(void);
		BOOL Send(PBYTE pBuff, UINT uLength);
		UINT Recv(UINT uTime);
	};

#endif

// End of File
