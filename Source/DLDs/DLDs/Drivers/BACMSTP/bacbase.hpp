
//////////////////////////////////////////////////////////////////////////
//
// BACNet Base Driver
//

class CBACNetBase : public CMasterDriver
{
	public:
		// Constructor
		CBACNetBase(void);

		// Destructor
		~CBACNetBase(void);

		// User Functions
		DEFMETH(UINT) DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	public:
		// Network Address
		struct CNetAddr
		{
			WORD	m_NET;
			BYTE	m_LEN;
			BYTE	m_MAC[8];
			};

		// Device Context
		struct CBaseCtx
		{
			DWORD	 m_Device;
			UINT     m_nObj;
			PDWORD   m_pObj;
			BYTE     m_bTxSeq;
			BOOL	 m_fBroke;
			BOOL     m_fFound;
			CNetAddr m_Net;
			};

	protected:
		// Data Members
		CBaseCtx * m_pBase;
		CBuffer  * m_pTxBuff;
		PBYTE      m_pTxData;
		UINT       m_uTxPtr;
		CBuffer  * m_pRxBuff;
		CNetAddr   m_RxNet;
		UINT	   m_uBits;

		// Transport Hooks
		virtual BOOL SendFrame(BOOL fThis, BOOL fReply) = 0;
		virtual BOOL RecvFrame(void)                    = 0;

		// Configuration
		BOOL LoadConfig(PCBYTE &pData);

		// Frame Building
		BOOL InitFrame(void);
		void AddDone(void);
		void AddRawByte(BYTE bData);
		void AddRawWord(WORD wData);
		void AddRawTrip(DWORD dwData);
		void AddRawLong(DWORD dwData);
		void AddAppTag(BYTE bTag, BYTE bSize);
		void AddCtxTag(BYTE bTag, BYTE bSize);
		void AddCtxByte(BYTE bTag, BYTE bData);
		void AddCtxWord(BYTE bTag, WORD wData);
		void AddCtxTrip(BYTE bTag, DWORD dwData);
		void AddCtxLong(BYTE bTag, DWORD dwData);
		void AddCtxOpen(BYTE bTag);
		void AddCtxClose(BYTE bTag);
		void AddNetworkHeader(BOOL fThis, BOOL fReply);

		// Frame Parsing
		void RecvDone(void);
		BOOL StripNetworkHeader(void);

		// Read Requests
		BOOL BuildPropRead(DWORD ObjectID, BYTE PropID);
		BOOL ExtractValue (DWORD &Dest, BYTE bOurType, PCBYTE &pData);

		// Write Requests
		BOOL BuildPropWrite(DWORD ObjectID, BYTE PropID, BYTE TypeID, DWORD dwData);
		BOOL EncodeValue   (BYTE bNetType, DWORD dwData);

		// Priority Encoding
		BOOL DecodePriority(BYTE &PropID, BYTE &Priority);

		// Debugging
		void Dump(PCTXT pName, CBuffer *pBuff);
		void Show(PCTXT pName, PCTXT pError);
	};

//////////////////////////////////////////////////////////////////////////
//
// Time Support
//

extern DWORD Time(UINT h, UINT m, UINT s);
extern DWORD Date(UINT y, UINT m, UINT d);
extern void  GetWholeDate(DWORD t, UINT *p);
extern void  GetWholeTime(DWORD t, UINT *p);
extern UINT  GetYear(DWORD t);
extern UINT  GetMonth(DWORD t);
extern UINT  GetDate(DWORD t);
extern UINT  GetDays(DWORD t);
extern UINT  GetWeeks(DWORD t);
extern UINT  GetDay(DWORD t);
extern UINT  GetWeek(DWORD t);
extern UINT  GetWeekYear(DWORD t);
extern UINT  GetHours(DWORD t);
extern UINT  GetHour(DWORD t);
extern UINT  GetMin(DWORD t);
extern UINT  GetSec(DWORD t);
extern UINT  GetMonthDays(UINT y, UINT m);

// End of File
