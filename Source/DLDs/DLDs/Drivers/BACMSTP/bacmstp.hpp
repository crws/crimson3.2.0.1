
//////////////////////////////////////////////////////////////////////////
//
// BACnet Master Driver
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "bachand.hpp"

#include "bacbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Driver
//

class CBACNetMSTP : public CBACNetBase
{
	public:
		// Constructor
		CBACNetMSTP(void);

		// Destructor
		~CBACNetMSTP(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping(void);

		// Config Data
		BYTE m_bThisDrop;
		BYTE m_bLastDrop;
		BOOL m_fOptim1;
		BOOL m_fOptim2;

	protected:
		// Device Context
		struct CContext : CBACNetBase::CBaseCtx
		{
			UINT	m_uTime1;
			BOOL    m_fMaster;
			BYTE	m_bMac;

			};

		// Data Members
		CContext       * m_pCtx;
		CBACNetHandler * m_pHandler;
		BYTE		 m_bRxMac;
		BOOL		 m_fTxFast;
		BOOL		 m_fHwDelay;

		// Transport Hooks
		BOOL SendFrame(BOOL fThis, BOOL fReply);
		BOOL RecvFrame(void);
	};

// End of File
