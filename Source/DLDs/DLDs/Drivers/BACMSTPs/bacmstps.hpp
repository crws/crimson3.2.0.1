//////////////////////////////////////////////////////////////////////////
//
// BACnet Slave Driver
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//
//////////////////////////////////////////////////////////////////////////
//
// Base Class
//

#include "bachand.hpp"

#include "bacslave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet MS/TP Slave Driver
//

class CBACNetMSTPSlave : public CBACNetSlave
{
	public:
		// Constructor
		CBACNetMSTPSlave(void);

		// Destructor
		~CBACNetMSTPSlave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);

		// Config Data
		BYTE m_bThisDrop;
		BYTE m_bLastDrop;
		BOOL m_fOptim1;
		BOOL m_fOptim2;
		BOOL m_fTxFast;
		BOOL m_fHwDelay;

	protected:
		// Device Context
		struct CContext : CBACNetSlave::CBaseCtx
		{
			};

		// Data Members
		CContext       * m_pCtx;
		CBACNetHandler * m_pHandler;
		BYTE		 m_bRxMac;

		// Transport Hooks
		BOOL SendFrame(BOOL fThis, BOOL fReply);
		BOOL RecvFrame(void);
	};

// End of File
