
#include "intern.hpp"

#include "bachand.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACnet Port Handler
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Constructor 

CBACNetHandler::CBACNetHandler(BYTE bThisDrop, BYTE bLastDrop, BOOL fOptim1, BOOL fOptim2, BOOL fTxFast, BOOL fHwDelay)
{
	StdSetRef();

	m_bThisDrop   = bThisDrop;

	m_bLastDrop   = bLastDrop;

	m_fOptim1     = fOptim1;

	m_fOptim2     = fOptim2;

	m_fTxFast     = fTxFast;

	m_pPort       = NULL;

	m_fOpen       = FALSE;

	m_uAppTxCount = 0;

	m_uAppRxCount = 0;

	m_fHwDelay    = fHwDelay;

	CreateEvents();

	GetHelp();
}

// Destructor

CBACNetHandler::~CBACNetHandler(void)
{
	m_pAppTxFlag->Release();

	m_pAppRxFlag->Release();

	/*m_pExtra->Release();!!!C3!!!*/
}

// Application Calls

BOOL CBACNetHandler::IsOnline(void)
{
	return m_bNextDrop != m_bThisDrop || m_fSoleMaster;
}

BOOL CBACNetHandler::AppSend(BYTE bDest, PCBYTE pSend, UINT uSend, BOOL fReply)
{
	if( uSend && IsOnline() ) {

		if( m_pAppTxFlag->Wait(1000) ) {

			memcpy(m_bAppTxData, pSend, uSend);

			m_bAppTxDrop  = bDest;

			m_fAppTxWait  = fReply;

			m_uAppTxCount = uSend;

			return TRUE;
		}
	}

	return FALSE;
}

UINT CBACNetHandler::AppRecv(BYTE &bDrop, PBYTE pRecv, UINT uRecv, UINT uTime)
{
	if( uRecv && IsOnline() ) {

		uTime = uTime ? uTime : GetReplyTime();

		if( m_pAppRxFlag->Wait(uTime) ) {

			bDrop = m_bAppRxDrop;

			uRecv = min(uRecv, m_uAppRxCount);

			memcpy(pRecv, m_bAppRxData, uRecv);

			m_uAppRxCount = 0;

			return uRecv;
		}
	}

	return 0;
}

UINT CBACNetHandler::GetReplyTime(void)
{
	UINT n = 1000 / ToTicks(1000);

	if( m_uToken1 && m_uToken2 && m_uToken3 ) {

		UINT t1 = m_uToken3 - m_uToken2;

		UINT t2 = m_uToken2 - m_uToken1;

		UINT tt = n * (2 * (t1 + t2) + t_ReplyTimeout);

		return tt;
	}

	return n * 3 * t_ReplyTimeout / 2;
}

void CBACNetHandler::SetThisDrop(BYTE bDrop)
{
	m_bThisDrop = bDrop;
}

// IUnknown

HRESULT CBACNetHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
}

ULONG CBACNetHandler::AddRef(void)
{
	StdAddRef();
}

ULONG CBACNetHandler::Release(void)
{
	StdRelease();
}

// IPortHandler

void MCALL CBACNetHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
}

void MCALL CBACNetHandler::OnOpen(CSerialConfig const &Config)
{
	FindFormat(Config);

	LoadParams();

	CalibDelay();

	m_bNextDrop  = m_bThisDrop;

	m_uToken1    = 0;

	m_uToken2    = 0;

	m_uToken3    = 0;

	m_fTxBusy    = FALSE;

	m_uRxState   = rxIdle;

	m_uRxTime    = 0;

	m_uMacState  = macInitialize;

	m_uMacTimer  = t_Initialize;

	m_fOpen      = TRUE;
}

void MCALL CBACNetHandler::OnClose(void)
{
	m_fOpen = FALSE;
}

void MCALL CBACNetHandler::OnTimer(void)
{
	if( m_uMacTimer && !--m_uMacTimer ) {

		MacSignal(eventTimer);
	}

	if( m_uRxState ) {

		if( m_uRxTime && !--m_uRxTime ) {

			m_uRxState = rxIdle;

			MacSignal(eventError);
		}
	}
}

BOOL MCALL CBACNetHandler::OnTxData(BYTE &bData)
{
	if( m_fTxBusy ) {

		if( m_uTxPtr < m_uTxCount ) {

			bData = m_bTxData[m_uTxPtr++];

			return TRUE;
		}
		else {
			MacSignal(eventSent);

			if( m_uTxPtr == 0 ) {

				bData = m_bTxData[m_uTxPtr++];

				return TRUE;
			}

			m_fTxBusy = FALSE;
		}
	}

	return FALSE;
}

void MCALL CBACNetHandler::OnTxDone(void)
{
}

void MCALL CBACNetHandler::OnRxData(BYTE bData)
{
	RxByte(bData);
}

void MCALL CBACNetHandler::OnRxDone(void)
{
}

// IEventSink

void CBACNetHandler::OnEvent(UINT uLine, UINT uParam)
{
	m_pPort->Send(m_bTxData[0]);
}

// Frame Building

void CBACNetHandler::InitFrame(BYTE bDest, BYTE bType)
{
	m_bTxData[0] = 0x55;

	m_bTxData[1] = 0xFF;

	m_bTxData[2] = bType;

	m_bTxData[3] = bDest;

	m_bTxData[4] = m_bThisDrop;

	m_bTxData[5] = 0;

	m_bTxData[6] = 0;

	m_bTxData[7] = 0;

	m_uTxPtr     = 8;
}

void CBACNetHandler::AddByte(BYTE bData)
{
	m_bTxData[m_uTxPtr++] = bData;
}

void CBACNetHandler::AddData(PCBYTE pData, UINT uCount)
{
	memcpy(m_bTxData + m_uTxPtr, pData, uCount);

	m_uTxPtr += uCount;
}

void CBACNetHandler::SendFrame(void)
{
	UINT uData   = m_uTxPtr - 8;

	m_bTxData[5] = HIBYTE(uData);

	m_bTxData[6] = LOBYTE(uData);

	m_bTxData[7] = GetCRC08(m_bTxData + 2, 5);

	if( uData ) {

		WORD crc = GetCRC16(m_bTxData + 8, uData);

		AddByte(LOBYTE(crc));

		AddByte(HIBYTE(crc));
	}

	if( m_fTxBusy ) {

		m_uTxCount = m_uTxPtr;

		m_uTxPtr   = 0;

		return;
	}

	m_uTxCount = m_uTxPtr;

	m_uTxPtr   = 1;

	m_fTxBusy  = TRUE;

	TxDelay();
}

// Initialization

void CBACNetHandler::FindFormat(CSerialConfig const &Config)
{
	m_uBaud  = Config.m_uBaudRate;

	m_uBits  = 1;

	m_uBits += Config.m_uDataBits;

	m_uBits += Config.m_uStopBits;

	m_uBits += Config.m_uParity ? 1 : 0;
}

void CBACNetHandler::LoadParams(void)
{
	n_MaxInfoFrames = 1;

	n_MaxMaster     = m_bLastDrop;

	n_Poll1         = 50;

	n_Poll2         = 10;

	n_RetryToken    = 1;

	t_Initialize    = ToTicks(500);

	t_FrameAbort    = ToTicks(10);

	t_NoToken       = ToTicks(500);

	t_ReplyTimeout  = ToTicks(400);

	t_Slot          = ToTicks(10);

	t_UsageTimeout  = ToTicks(50 + GetTokenLength());
}

void CBACNetHandler::CreateEvents(void)
{
	m_pAppTxFlag = Create_Semaphore(1);

	m_pAppRxFlag = Create_AutoEvent();
}

void CBACNetHandler::GetHelp(void)
{
	/*m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);!!!C3!!!*/
}

// Rx State Machine

void CBACNetHandler::RxByte(BYTE bData)
{
	WORD crc;

	switch( m_uRxState ) {

		case rxIdle:

			if( bData == 0x55 ) {

				m_uRxState = rxPreamble;

				break;
			}
			break;

		case rxPreamble:

			if( bData == 0xFF ) {

				MacSignal(eventStart);

				m_uRxState = rxHeader;

				m_uRxPtr   = 0;

				break;
			}

			m_uRxState = rxIdle;

			break;

		case rxHeader:

			if( m_uRxPtr == 5 ) {

				if( GetCRC08(m_bRxData, 5) == bData ) {

					m_uRxCount = MAKEWORD(m_bRxData[4], m_bRxData[3]);

					if( m_uRxCount ) {

						m_uRxCount = m_uRxCount + m_uRxPtr;

						if( m_uRxCount >= sizeof(m_bRxData) ) {

							MacSignal(eventError);

							m_uRxState = rxIdle;

							break;
						}

						m_uRxState = rxData;

						break;
					}

					MacSignal(eventFrame);
				}
				else
					MacSignal(eventError);

				m_uRxState = rxIdle;

				break;
			}

			m_bRxData[m_uRxPtr++] = bData;

			break;

		case rxData:

			m_bRxData[m_uRxPtr++] = bData;

			if( m_uRxPtr == m_uRxCount ) {

				m_uRxState = rxCheck1;
			}
			break;

		case rxCheck1:

			m_wRxCheck = bData;

			m_uRxState = rxCheck2;

			break;

		case rxCheck2:

			m_wRxCheck = m_wRxCheck | (bData << 8);

			crc        = GetCRC16(m_bRxData + 5, m_uRxPtr - 5);

			if( m_wRxCheck == crc ) {

				MacSignal(eventFrame);
			}
			else
				MacSignal(eventError);

			m_uRxState = rxIdle;

			break;
	}

	m_uRxTime = t_FrameAbort;
}

// MAC Event Handlers

void CBACNetHandler::MacSignal(UINT uEvent)
{
	UINT uState = m_uMacState;

	MacDispatch(uEvent);

	while( m_uMacState != uState ) {

		uState = m_uMacState;

		MacDispatch(eventCheck);
	}
}

void CBACNetHandler::MacDispatch(UINT uEvent)
{
	switch( m_uMacState ) {

		case macInitialize:	MacInitialize(uEvent);	break;

		case macIdle:		MacIdle(uEvent);	break;

		case macUseToken:	MacUseToken(uEvent);	break;

		case macWaitForReply:	MacWaitForReply(uEvent);	break;

		case macDoneWithToken:	MacDoneWithToken(uEvent);	break;

		case macPassToken:	MacPassToken(uEvent);	break;

		case macNoToken:	MacNoToken(uEvent);	break;

		case macPollForMaster:	MacPollForMaster(uEvent);	break;

		case macAnswerRequest:	MacAnswerRequest(uEvent);	break;
	}
}

BOOL CBACNetHandler::MacSetState(UINT uState)
{
	m_uMacState = uState;

	switch( uState ) {

		case macIdle:		m_uMacTimer = t_NoToken;	break;

		case macWaitForReply:	m_uMacTimer = t_ReplyTimeout;	break;

		case macPassToken:	m_uMacTimer = t_UsageTimeout;	break;

		case macNoToken:	m_uMacTimer = t_NoToken;	break;

		case macPollForMaster:	m_uMacTimer = t_UsageTimeout;	break;
	}

	if( uState == macNoToken ) {

		m_uMacTimer += t_Slot * m_bThisDrop + t_Slot / 2;
	}

	return TRUE;
}

void CBACNetHandler::MacInitialize(UINT uEvent)
{
	if( uEvent == eventTimer ) {

		Action("DoneInitializing");

		m_bNextDrop   = m_bThisDrop;

		m_bPollDrop   = m_bThisDrop;

		m_uTokenCount = n_Poll1;

		m_fSoleMaster = FALSE;

		ClearPassTable();

		MacSetState(macIdle);

		return;
	}
}

void CBACNetHandler::MacIdle(UINT uEvent)
{
	if( uEvent == eventTimer ) {

		Action("LostToken");

		MacSetState(macNoToken);

		return;
	}

	if( uEvent == eventFrame ) {

		BYTE const &bType = m_bRxData[0];

		BYTE const &bDest = m_bRxData[1];

		BYTE const &bFrom = m_bRxData[2];

		if( bType == frameToken ) {

			m_bPassDrop[bFrom] = bDest;
		}

		if( bDest == m_bThisDrop ) {

			switch( bType ) {

				case frameToken:

					Action("ReceivedToken");

					m_uFrameCount = 0;

					m_fSoleMaster = FALSE;

					m_uToken1     = m_uToken2;

					m_uToken2     = m_uToken3;

					m_uToken3     = GetTickCount();

					MacSetState(macUseToken);

					return;

				case framePollForMaster:

					Action("ReceivedPFM");

					if( m_fOptim1 ) {

						// NOTE -- This is not in the spec but it improves
						// startup times by starting the poll at the last
						// station to which we saw our predecessor passing
						// the token. The skipping of the first poll makes
						// sure we see a full scan of the network before
						// we make such a decision.

						if( m_fFirstPoll ) {

							m_fFirstPoll = FALSE;

							break;
						}

						m_bPollDrop = (m_bPassDrop[bFrom] + n_MaxMaster) % (n_MaxMaster + 1);
					}

					ReplyToPoll(bFrom);

					break;

				case frameDataExReply:

					Action("ReceivedDataNeedingReply");

					MacSetState(macAnswerRequest);

					return;

				// LATER -- Handle test request?
			}
		}

		if( bDest == m_bThisDrop || bDest == 0xFF ) {

			switch( bType ) {

				case frameDataNoReply:

					Action("ReceivedDataNoReply");

					AppRxData();

					break;
			}
		}

		MacSetState(macIdle);

		return;
	}
}

void CBACNetHandler::MacUseToken(UINT uEvent)
{
	if( uEvent == eventCheck ) {

		if( m_uAppTxCount ) {

			UINT bType = m_fAppTxWait ? frameDataExReply : frameDataNoReply;

			InitFrame(m_bAppTxDrop, bType);

			AddData(m_bAppTxData, m_uAppTxCount);

			AppTxDone();

			SendFrame();

			if( m_fAppTxWait ) {

				Action("SendAndWait");

				m_uFrameCount = m_uFrameCount + 1;

				MacSetState(macWaitForReply);
			}
			else {
				// NOTE -- Stay in this state until sent.

				Action("SendNoWait");

				m_uFrameCount = m_uFrameCount + 1;
			}
		}
		else {
			Action("NothingToSend");

			m_uFrameCount = n_MaxInfoFrames;

			MacSetState(macDoneWithToken);
		}

		return;
	}

	if( uEvent == eventSent ) {

		MacSetState(macDoneWithToken);

		return;
	}
}

void CBACNetHandler::MacWaitForReply(UINT uEvent)
{
	if( uEvent == eventTimer ) {

		Action("ReplyTimeout");

		m_uFrameCount = n_MaxInfoFrames;

		MacSetState(macDoneWithToken);

		return;
	}

	if( uEvent == eventError ) {

		Action("InvalidFrame");

		MacSetState(macDoneWithToken);

		return;
	}

	if( uEvent == eventFrame ) {

		BYTE const &bType = m_bRxData[0];

		BYTE const &bDest = m_bRxData[1];

		BYTE const &bFrom = m_bRxData[2];

		if( bDest == m_bThisDrop ) {

			switch( bType ) {

				case frameDataNoReply:

					Action("ReceivedReply");

					AppRxData();

					MacSetState(macDoneWithToken);

					return;

				case frameReplyPostponed:

					Action("ReplyPostponed");

					MacSetState(macDoneWithToken);

					return;
			}
		}

		Action("InvalidFrame");

		ClearPassTable();

		MacSetState(macIdle);

		return;
	}
}

void CBACNetHandler::MacDoneWithToken(UINT uEvent)
{
	if( uEvent == eventCheck ) {

		if( m_uFrameCount < n_MaxInfoFrames ) {

			Action("SendAnotherFrame");

			MacSetState(macUseToken);

			return;
		}

		if( m_uTokenCount < n_Poll1 - 1 ) {

			if( m_fSoleMaster ) {

				Action("SoleMaster");

				m_uFrameCount = 0;

				m_uTokenCount = m_uTokenCount + 1;

				MacSetState(macUseToken);

				return;
			}

			Action("SendToken");

			m_uTokenCount = m_uTokenCount + 1;

			m_uRetryCount = 0;

			PassToken(m_bNextDrop);

			return;
		}

		if( (m_bThisDrop+1) % (n_MaxMaster+1) == m_bNextDrop ) {

			Action("SendToken");

			m_uTokenCount = m_uTokenCount + 1;

			m_uRetryCount = 0;

			PassToken(m_bNextDrop);

			return;
		}

		if( (m_bPollDrop+1) % (n_MaxMaster+1) == m_bNextDrop ) {

			if( m_fSoleMaster ) {

				Action("SoleMasterRestartMaintenancePFM");

				m_bPollDrop   = (m_bNextDrop+1) % (n_MaxMaster+1);

				m_uRetryCount = 0;

				m_uTokenCount = 1;

				LostSuccessor();

				PollForMaster(m_bPollDrop);

				return;
			}

			Action("ResetMaintenancePFM");

			m_bPollDrop   = m_bThisDrop;

			m_uRetryCount = 0;

			m_uTokenCount = 1;

			PassToken(m_bNextDrop);

			return;
		}

		Action("SendMaintenancePFM");

		m_bPollDrop   = (m_bPollDrop+1) % (n_MaxMaster+1);

		m_uRetryCount = 0;

		if( m_fOptim2 ) {

			// NOTE -- This reload of token count is not in the spec
			// but if improves performance by avoiding a complete scan
			// of intervening stations in a single burst. Rather, we
			// scan one station per Poll2 token receptions.

			m_uTokenCount = n_Poll1 - n_Poll2;
		}

		PollForMaster(m_bPollDrop);

		return;
	}
}

void CBACNetHandler::MacPassToken(UINT uEvent)
{
	if( uEvent == eventStart ) {

		Action("SawTokenUser");

		MacSetState(macIdle);

		return;
	}

	if( uEvent == eventTimer ) {

		if( m_uRetryCount < n_RetryToken ) {

			Action("RetrySendToken");

			m_uRetryCount = m_uRetryCount + 1;

			PassToken(m_bNextDrop);

			return;
		}

		Action("FindNewSuccessor");

		m_bPollDrop   = (m_bNextDrop+1) % (n_MaxMaster+1);

		m_uRetryCount = 0;

		m_uTokenCount = 0;

		LostSuccessor();

		PollForMaster(m_bPollDrop);

		return;
	}
}

void CBACNetHandler::MacNoToken(UINT uEvent)
{
	if( uEvent == eventStart ) {

		Action("SawFrame");

		MacSetState(macIdle);

		return;
	}

	if( uEvent == eventTimer ) {

		Action("GenerateToken");

		m_bPollDrop   = (m_bThisDrop+1) % (n_MaxMaster+1);

		m_uTokenCount = 0;

		m_uRetryCount = 0;

		LostSuccessor();

		PollForMaster(m_bPollDrop);

		return;
	}
}

void CBACNetHandler::MacPollForMaster(UINT uEvent)
{
	if( uEvent == eventFrame ) {

		BYTE const &bType = m_bRxData[0];

		BYTE const &bDest = m_bRxData[1];

		BYTE const &bFrom = m_bRxData[2];

		if( bDest == m_bThisDrop ) {

			switch( bType ) {

				case frameReplyToPoll:

					Action("ReceivedReplyToPFM");

					m_fSoleMaster = FALSE;

					m_bNextDrop   = bFrom;

					m_bPollDrop   = m_bThisDrop;

					m_uTokenCount = 0;

					m_uRetryCount = 0;

					PassToken(m_bNextDrop);

					return;
			}
		}

		Action("ReceivedUnexpectedFrame");

		ClearPassTable();

		MacSetState(macIdle);

		return;
	}

	if( uEvent == eventError || uEvent == eventTimer ) {

		if( m_fSoleMaster ) {

			Action("SoleMaster");

			m_uFrameCount = 0;

			MacSetState(macUseToken);

			return;
		}

		if( m_bNextDrop == m_bThisDrop ) {

			if( (m_bPollDrop+1) % (n_MaxMaster+1) == m_bThisDrop ) {

				Action("DeclareSoleMaster");

				m_fSoleMaster = TRUE;

				m_uFrameCount = 0;

				MacSetState(macUseToken);

				return;
			}

			Action("SendNextPFM");

			m_bPollDrop   = (m_bPollDrop+1) % (n_MaxMaster+1);

			m_uRetryCount = 0;

			PollForMaster(m_bPollDrop);

			return;
		}

		Action("DoneWithPFM");

		m_uRetryCount = 0;

		PassToken(m_bNextDrop);

		return;
	}
}

void CBACNetHandler::MacAnswerRequest(UINT uEvent)
{
	if( uEvent == eventCheck ) {

		AppRxData();

		// LATER -- Should we ever reply here?

		Action("DeferredReply");

		BYTE bFrom = m_bRxData[2];

		ReplyPostponed(bFrom);

		MacSetState(macIdle);

		return;
	}
}

// MAC Actions

void CBACNetHandler::Action(PCTXT pText)
{
/*	AfxTrace("S%u: %s\n", m_uMacState, pText);
*/
}

void CBACNetHandler::ReplyToPoll(BYTE bDest)
{
	InitFrame(bDest, frameReplyToPoll);

	SendFrame();
}

void CBACNetHandler::ReplyPostponed(BYTE bDest)
{
	InitFrame(bDest, frameReplyPostponed);

	SendFrame();
}

void CBACNetHandler::PassToken(BYTE bDest)
{
	InitFrame(bDest, frameToken);

	SendFrame();

	MacSetState(macPassToken);
}

void CBACNetHandler::PollForMaster(BYTE bDest)
{
	InitFrame(bDest, framePollForMaster);

	SendFrame();

	MacSetState(macPollForMaster);
}

void CBACNetHandler::ClearPassTable(void)
{
	for( UINT n = 0; n < elements(m_bPassDrop); n++ ) {

		m_bPassDrop[n] = n;
	}

	m_fFirstPoll = TRUE;

	AppTxDone();

	m_pAppRxFlag->Clear();
}

BOOL CBACNetHandler::AppRxData(void)
{
	if( !m_uAppRxCount ) {

		UINT uCount   = m_uRxPtr - 5;

		memcpy(m_bAppRxData, m_bRxData + 5, uCount);

		m_bAppRxDrop  = m_bRxData[2];

		m_uAppRxCount = uCount;

		m_pAppRxFlag->Set();

		return TRUE;
	}

	return FALSE;
}

BOOL CBACNetHandler::AppTxDone(void)
{
	if( m_uAppTxCount ) {

		m_uAppTxCount = 0;

		m_pAppTxFlag->Signal(1);

		return TRUE;
	}

	return FALSE;
}

void CBACNetHandler::LostSuccessor(void)
{
	m_bNextDrop = m_bThisDrop;

	m_uToken1   = 0;

	m_uToken2   = 0;

	m_uToken3   = 0;
}

// CRC Helpers

BYTE CBACNetHandler::GetCRC08(PCBYTE pData, UINT uCount)
{
	BYTE crc = 0xFF;

	while( uCount-- ) {

		crc = BYTE(CRCTable08[*pData++ ^ (crc & 0xFF)]);
	}

	return crc ^ 0xFF;
}

WORD CBACNetHandler::GetCRC16(PCBYTE pData, UINT uCount)
{
	WORD crc = 0xFFFF;

	while( uCount-- ) {

		crc = WORD(CRCTable16[*pData++ ^ (crc & 0xFF)] ^ (crc >> 8));
	}

	return crc ^ 0xFFFF;
}

// Transmit Timing

DWORD CBACNetHandler::GetTickCount(void)
{
	return ::GetTickCount();
}

void CBACNetHandler::CalibDelay(void)
{
	if( m_fHwDelay ) {

		UINT uMult = m_fTxFast ? 1 : 4;

		m_uTxDelay = (uMult * 1000 * 1000 * 40) / m_uBaud;

		MakeMin(m_uTxDelay, 5000);
	}
	else {
		Critical(TRUE);

		UINT uDelay = 0;

		UINT uTime = GetTickCount();

		while( uTime == GetTickCount() ) uDelay += 0;

		uTime += 1;

		while( uTime == GetTickCount() ) uDelay += 1;

		Critical(FALSE);

		UINT uMult = m_fTxFast ? 1 : 4;

		m_uTxDelay = uMult * uDelay * 40 * 200 / m_uBaud;

		MakeMin(m_uTxDelay, uDelay / 5 * 4);
	}
}

BOOL CBACNetHandler::TxDelay(void)
{
	if( m_fHwDelay ) {

		/*m_pExtra->RunAfterDelay(m_uTxDelay, this);!!!C3!!!*/
	}
	else {
		for( UINT uCount = m_uTxDelay; uCount--; GetTickCount() );

		m_pPort->Send(m_bTxData[0]);
	}

	return TRUE;
}

UINT CBACNetHandler::GetTokenLength(void)
{
	UINT n = m_uBits * 8;

	UINT t = n * 1000 / m_uBaud;

	return 5 * (t + 4) / 5;
}

UINT CBACNetHandler::ToTicks(UINT t)
{
	return t ? ((t < 5) ? 1 : (t / 5)) : 0;
}

// Debug

void CBACNetHandler::AfxTrace(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	::AfxTraceArgs(pText, pArgs);

	va_end(pArgs);
}

// End of File
