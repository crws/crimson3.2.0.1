
#include "intern.hpp"

#include "bacmstps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACnet Slave Driver
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CBACNetMSTPSlave);

// Constructor 

CBACNetMSTPSlave::CBACNetMSTPSlave(void)
{
	m_Ident     = DRIVER_ID;

	m_bThisDrop = 10;

	m_bLastDrop = 127;

	m_fOptim1   = FALSE;

	m_fOptim2   = FALSE;

	m_fTxFast   = FALSE;

	m_fHwDelay  = FALSE;
	}

// Destructor

CBACNetMSTPSlave::~CBACNetMSTPSlave(void)
{
	}

// Configuration

void MCALL CBACNetMSTPSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_pCtx      = new CContext;

		m_pBase     = m_pCtx;

		LoadConfig(pData);

		m_bThisDrop = GetByte(pData);

		m_bLastDrop = GetByte(pData);

		m_fOptim1   = GetByte(pData);

		m_fOptim2   = GetByte(pData);

		m_fTxFast   = GetByte(pData) ? TRUE : FALSE;

		m_fHwDelay  = GetByte(pData);

		m_pCtx->m_fRemote = FALSE;
		}
	}
	
void MCALL CBACNetMSTPSlave::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);

	Config.m_uFlags |= flagFastRx;
	}
	
// Management

void MCALL CBACNetMSTPSlave::Attach(IPortObject *pPort)
{
	m_pHandler = new CBACNetHandler( m_bThisDrop,
					 m_bLastDrop,
					 m_fOptim1,
					 m_fOptim2,
					 m_fTxFast,
					 m_fHwDelay
					 );

	pPort->Bind(m_pHandler);
	}

void MCALL CBACNetMSTPSlave::Detach(void)
{
	m_pHandler->Release();
	}

void MCALL CBACNetMSTPSlave::Open(void)
{
	CBACNetSlave::Open();
	}

// User Access

UINT MCALL CBACNetMSTPSlave::DrvCtrl(UINT uFunc, PCTXT Value)
{	
	UINT uValue = ATOI(Value);

	switch( uFunc ) {

		case 1:
			if( uValue <= 255 ) {

				m_bThisDrop =  uValue & 0xFF;

				m_pHandler->SetThisDrop(m_bThisDrop);

				return 1;
				}

			break;

		case 2: // Set Device ID

			if( uValue <= 4194303 ) {

				FreeObjectList();

				m_pBase->m_Device = uValue;

				BuildObjectList();

				return 1;
				}

			break;	

		case 3:	// Get Device ID

			return m_pCtx->m_Device;
		}

	return 0;
	}

// Transport Hooks

BOOL CBACNetMSTPSlave::SendFrame(BOOL fThis, BOOL fReply)
{
	if( m_pHandler->IsOnline() ) {

		AddNetworkHeader(fThis, fReply);

		Dump("tx", m_pTxBuff);

		PBYTE pData = m_pTxBuff->GetData();

		UINT  uSize = m_pTxBuff->GetSize();

		BYTE  bDrop = fThis ? m_bRxMac : 0xFF;

		if( m_pHandler->AppSend(bDrop, pData, uSize, fReply) ) {

			m_pTxBuff->Release();

			m_pTxBuff = NULL;

			return TRUE;
			}
		else
			Show("send-frame", "send timeout");
		}
	else
		Show("send-frame", "not online");

	m_pTxBuff->Release();

	m_pTxBuff = NULL;

	return FALSE;
	}

BOOL CBACNetMSTPSlave::RecvFrame(void)
{
	if( m_pHandler->IsOnline() ) {

		m_pRxBuff = CreateBuffer(600, TRUE);

		if( m_pRxBuff ) {

			m_pRxBuff->AddTail(600);

			PBYTE pData = m_pRxBuff->GetData();

			UINT  uSize = m_pRxBuff->GetSize();

			UINT  uRecv = m_pHandler->AppRecv(m_bRxMac, pData, uSize, 5000);

			if( uRecv ) {

				m_pRxBuff->StripTail(uSize - uRecv);

				Dump("rx", m_pRxBuff);

				if( StripNetworkHeader() ) {

					return TRUE;
					}
				}

			RecvDone();
			}
		else
			Show("recv-frame", "can't alloc buffer");
		}

	Sleep(100);

	return FALSE;
	}

// End of File
