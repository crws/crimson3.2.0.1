#include "intern.hpp"

#include "comlimst.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alfa Laval COMLI Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CCOMLIMasterDriver);

// Constructor

CCOMLIMasterDriver::CCOMLIMasterDriver(void)
{
	m_Ident     = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uWriteErr = 0;
	
	m_fEnhanced = FALSE;
	}

// Destructor

CCOMLIMasterDriver::~CCOMLIMasterDriver(void)
{
	}

// Configuration

void  MCALL CCOMLIMasterDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {
		
		m_fEnhanced = GetByte(pData);
		}
	}

// Management

void  MCALL CCOMLIMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void  MCALL CCOMLIMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CCOMLIMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop  = GetByte(pData);

			m_pCtx->m_bStamp = 0;

			m_pCtx->m_fAscii = FALSE;

			memset(m_pCtx->m_bDate,  0, sizeof(m_pCtx->m_bDate));

			memset(m_pCtx->m_dEvent, 0, sizeof(m_pCtx->m_dEvent));

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CCOMLIMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CCOMLIMasterDriver::Ping(void)
{
	m_pCtx->m_bStamp = 0;
	m_pCtx->m_fAscii = FALSE; // Select Binary Comms

	if( m_fEnhanced ) {

		DWORD    Data;

		CAddress Addr;

		Addr.a.m_Table  = SP_BITBYTES;
		Addr.a.m_Offset = 0;
		Addr.a.m_Type   = addrByteAsByte;
		Addr.a.m_Extra	= 0;

		if( DoRead(Addr, &Data, 1) == 1 ) {

			m_pCtx->m_fAscii = m_bRx[10] == '2' && m_bRx[13] == 3;
		
			return 1;
			}
		}
	else {

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = SP_BITBYTES;
		Addr.a.m_Offset = 0;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra	= 0;

		if( Read(Addr, Data, 1) == 1 ) {

			m_pCtx->m_fAscii = m_bRx[10] == '2' && m_bRx[13] == 3;

			return 1;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CCOMLIMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n*** READ T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( m_fEnhanced ) {

		switch( Addr.a.m_Table ) {
		
			case SP_DATAWORDS:

				return ReadDataWords(Addr, pData, min(uCount, 32));

			case SP_BITBYTES:

				return ReadBitBytes(Addr, pData, min(uCount, 8));

			case SP_BITBITS:

				return DoRead(Addr, pData, 1);

			default:
				break;
			}
		}

	return DoRead(Addr, pData, min(uCount, 10));
	}

CCODE MCALL CCOMLIMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\n\n*** WRITE T=%d O=%d\r\n", Addr.a.m_Table, Addr.a.m_Offset);

	MakeMin( uCount, 10 );

	return DoWrite(Addr, pData, Addr.a.m_Table != SP_BITBITS ? uCount : 1);
	}

// Implementation

// Frame Building

void  CCOMLIMasterDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddByte(STX);

	ClearCheck();

	BYTE bDrop = m_pCtx->m_bDrop;

	AddByte(m_pHex[bDrop / 16]);

	AddByte(m_pHex[bDrop % 16]);

	AddByte('0' + m_pCtx->m_bStamp);

	AddByte(bOpcode);

	KnockStamp();
	}

void  CCOMLIMasterDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr++]  = bData;

		AddToCheck(bData);
		}
	}

void  CCOMLIMasterDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void  CCOMLIMasterDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void  CCOMLIMasterDriver::AddDataByte(BYTE bData)
{
	if( m_pCtx->m_fAscii ) {

		AddByte(m_pHex[bData / 16]);
		AddByte(m_pHex[bData % 16]);
		}

	else AddByte(bData);
	}

void  CCOMLIMasterDriver::AddDataWord(WORD wData)
{
	AddDataByte(HIBYTE(wData));

	AddDataByte(LOBYTE(wData));
	}

void  CCOMLIMasterDriver::AddDataLong(DWORD dwData)
{
	AddDataWord(HIWORD(dwData));

	AddDataWord(LOWORD(dwData));
	}

void  CCOMLIMasterDriver::AddID(UINT uVal, UINT uQty)
{
	switch( uQty ) {

		case 4:
			AddByte(m_pHex[(uVal >> 12) & 0xF]);
		case 3:
			AddByte(m_pHex[(uVal >>  8) & 0xF]);
		case 2:
			AddByte(m_pHex[(uVal >>  4) & 0xF]);
		case 1:
			AddByte(m_pHex[uVal & 0xF]);
			return;
		}
	}

void  CCOMLIMasterDriver::EndFrame(void)
{
	AddByte(ETX);

	AddByte(m_bCheck);
	}

// Transport Layer

void  CCOMLIMasterDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL  CCOMLIMasterDriver::GetReply(void)
{
	WORD wState = 0;
	WORD wCount = 0;
	WORD wData;
	WORD wEnd   = sizeof(m_bRx);

	BYTE bCheck = 0;

	UINT uTime  = 2000;

	SetTimer(uTime);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (wData = Get(uTime)) == LOWORD(NOTHING) ) {

			continue;
			}

		BYTE bData = LOBYTE(wData);

//**/		AfxTrace1("<%2.2x>", bData );

		if( wCount < sizeof(m_bRx) ) m_bRx[wCount++] = bData;

		bCheck ^= bData;

		switch( wState ) {

			case 0:
				if( bData == STX ) {
					wState = 1;
					bCheck = 0;
					wCount = 1;
					}
				else {
					if( bData == 'S' ) {

						m_pCtx->m_bStamp = 0;

						return FALSE;
						}
					}
				break;

			case 1:
				if( wCount == 11 ) {

					wEnd = 12 + (xtob(m_bRx[9]) << 4) + xtob(m_bRx[10]);
					}

				if( bData == ETX ) {

					switch( m_bTx[4] ) { // these responses can have a byte = 3

						case OP_EVENTR:
							if( wCount > 69 ) wState = 2;
							break;

						case OP_BITR:
							if( wCount >= wEnd ) wState = 2;
							break;

						case OP_REGR:
							if( (m_bTx[5] >= '4') || wCount >= wEnd ) wState = 2;							
							break;

						default:
							wState = 2;
							break;
						}
					}
				break;

			case 2:
				return !bCheck;

			}
		}

	return FALSE;
	}

BOOL  CCOMLIMasterDriver::Transact()
{
	Send();

	return GetReply();
	}

// Port Access

void  CCOMLIMasterDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i] );

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

UINT  CCOMLIMasterDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Read Handlers

CCODE CCOMLIMasterDriver::DoRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uAdd = Addr.a.m_Offset;

	UINT uCt  = uCount;

	UINT uBPI = 2; // Bytes Per Item

	switch( Addr.a.m_Table ) {

		case SP_ANALOG:

			if( uAdd % 4 ) { // not a valid analog address

				return min(uCt, 4 - (uAdd % 4)); // don't send 
				}

			StartFrame(OP_ANALOGR);
			uCt = 1 + (uCt/4); // 1=1, 5=2, 9=3 (only every 4th address is valid)
			break;

		case SP_DATAWORDS:

			uCt *= 2;

			if( uAdd >= K_EXTREG ) StartFrame(OP_REGREXT);

			else {
				StartFrame(OP_REGR);
				uAdd = (uAdd << 4) + 0x4000;
				uCt = AdjCountForAscii(uCt);
				}

			break;

		case SP_BITBYTES:
			StartFrame(OP_REGR);
			uBPI  = 1;
			break;

		case SP_BITBITS:
			StartFrame(OP_BITR);
			uCt  = 0;
			uBPI = 1;
			break;

		case SP_FLOAT:
			StartFrame(OP_FLOATR);
			uBPI = 4;
			break;

		case SP_TIMER:
			StartFrame(OP_TIMERR);
			uBPI = 4;
			break;

		case SP_DATER:
			StartFrame(OP_DATER);
			uAdd   = 0;
			uCt    = 0;
			break;

		case SP_DATEW:
			GetDateFromCache(uAdd, uCount, pData);
			return uCount;

		case SP_EVENTC:
			GetEventFromCache(uAdd, uCount, pData);
			return uCount;

		case SP_EVENTG:
			*pData = 0;
			return 1;

		default: return uCount;
		}

	AddID(uAdd, 4);

	AddID(uCt, 2);

	if( Transact() ) {

		for( UINT n = 0; n < uCount; n++ ) {

			DWORD x;

			if( Addr.a.m_Table == SP_ANALOG ) {

				x = n % 4 ? 0 : GetResult( n/4, 2 );
				}

			else x = GetResult(n, uBPI);

			switch( Addr.a.m_Table ) {

				default:
				case SP_BITBYTES:
				case SP_FLOAT:
				case SP_TIMER:
					pData[n] = MotorToHost(x);
					break;

				case SP_DATAWORDS:
					pData[n] = MotorToHost(SwapWordBits(x));
					break;

				case SP_BITBITS:
					x = xtob(m_bRx[11]);
					pData[n] = MotorToHost(x);
					break;

				case SP_DATER:
					GetDate( Addr.a.m_Offset, min(uCount, 6), pData );
					return uCount;
				}

			
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CCOMLIMasterDriver::DoWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uAdd  = Addr.a.m_Offset;

	UINT uCt   = uCount;

	switch( Addr.a.m_Table ) {

		case SP_DATAWORDS:

			uCt *= 2;

			if( uAdd >= K_EXTREG ) StartFrame(OP_REGWEXT);

			else {
				StartFrame(OP_REGW);
				uAdd   = 0x4000 + (uAdd << 4);
				uCt = AdjCountForAscii(uCt);
				}
			break;

		case SP_BITBYTES:
			StartFrame(OP_REGW); // Count is number of data bytes
			uCt = AdjCountForAscii(uCt);
			break;

		case SP_BITBITS:
			StartFrame(OP_BITW);
			uCt    = 1;
			uCount = 1;
			break;

		case SP_FLOAT:
			StartFrame(OP_FLOATW);
			if( m_pCtx->m_fAscii ) uCt *= 2;
			break;

		case SP_TIMER:
			StartFrame(OP_TIMERW);
			uCt   = (m_pCtx->m_fAscii ? 8 : 4) * uCount;
			break;

		case SP_DATEW:
			PutDateIntoCache(uAdd, min(uCount, 7), pData);

			if( !m_pCtx->m_bDate[6] ) return uCount; // filling values only

			StartFrame(OP_DATEW);
			uAdd   =  0;
			uCt    = 12;
			uCount =  1;
			break;

		case SP_EVENTG:
			if( !(*pData) ) return 1;

			StartFrame(OP_EVENTR);
			uAdd  =  0;
			uCt   = 60;
			break;

		// Read Only Opcodes
		case SP_EVENTC:
		case SP_DATER:
		case SP_ANALOG:
		default:
			return uCount;
		}

	AddID(uAdd, 4);

	AddID(uCt, 2);

	for( UINT i = 0; i < uCount; i++ ) {

		UINT uData = pData[i];

		switch( Addr.a.m_Table ) {

			case SP_EVENTG:
				break;

			case SP_DATAWORDS:
				uData = SwapWordBits(uData);
				AddDataByte( HIBYTE(uData) );
				AddDataByte( LOBYTE(uData) );
				break;

			case SP_BITBYTES:
				AddDataByte(LOBYTE(uData));
				break;

			case SP_BITBITS:
				AddByte( uData ? '1' : '0' );
				break;

			case SP_TIMER:
			case SP_FLOAT:
				AddDataLong(pData[i]);
				break;

			case SP_DATEW:
				PutDate();
				break;
			}
		}

	memset(m_bRx, 0, 10);

	if( Transact() ) {

		if( Addr.a.m_Table == SP_EVENTG ) {

			PutEventIntoCache();

			return uCount;
			}

		else {
			if( m_bRx[5] == ACK ) {

				m_uWriteErr = 0;

				return uCount;
				}
			}
		}

	return (++m_uWriteErr) % 3 ? CCODE_ERROR : uCount;
	}

// Helpers

void  CCOMLIMasterDriver::KnockStamp(void)
{
	m_pCtx->m_bStamp = m_pCtx->m_bStamp ? 3 - m_pCtx->m_bStamp : 1;
	}

UINT  CCOMLIMasterDriver::AdjCountForAscii(UINT uStartCt)
{
	return m_pCtx->m_fAscii ? uStartCt * 2 : uStartCt;
	}

DWORD CCOMLIMasterDriver::GetResult(UINT uItem, UINT uSize)
{
	if( !m_pCtx->m_fAscii ) {

		switch( uSize ) {

			case 1:	return LONG(*PBYTE(&m_bRx[11 + uItem]));

			case 2: return LONG(SHORT(*PU2(&m_bRx[11 + (uItem * 2)])));

			case 4:	return *PU4(&m_bRx[11 + (uItem * 4)]);
			}
		}

	DWORD d = 0;

	PBYTE p = &m_bRx[11 + (uItem * uSize * 2)];

	for( UINT i = 0; i < uSize * 2; i++ ) {

		d <<= 4;

		d  += xtob(p[i]);
		}

	return d;
	}

WORD CCOMLIMasterDriver::SwapWordBits(UINT uData)
{
	return (SwapByteBits(HIBYTE(LOWORD(uData))) << 8) + SwapByteBits(LOBYTE(LOWORD(uData)));
	}

BYTE  CCOMLIMasterDriver::SwapByteBits(BYTE bData)
{
	WORD x = 0;

	WORD m = 1;

	WORD t = 0x80;

	for( UINT i = 0; i < 8; i++ ) {

		if( bData & t ) {

			x |= m;
			}

		t >>= 1;

		m <<= 1;
		}

	return x;
	}

UINT  CCOMLIMasterDriver::SwapBytes(UINT uData)
{
	return (LOBYTE(uData) << 8) + HIBYTE(uData);
	}

void  CCOMLIMasterDriver::GetDateFromCache(UINT uItem, UINT uCount, PDWORD pData)
{
	UINT n = 0;

	for( UINT i = uItem; n < uCount && i <= 6; i++, n++ ) {

		pData[n] = i != 6 ? DWORD(m_pCtx->m_bDate[i]) : 0;
		}
	}

void  CCOMLIMasterDriver::PutDateIntoCache(UINT uItem, UINT uCount, PDWORD pData)
{
	UINT n = 0;

	for( UINT i = uItem; n < uCount; i++, n++ ) {

		m_pCtx->m_bDate[i] = pData[n] % 100;
		}
	}

void  CCOMLIMasterDriver::GetDate(UINT uItem, UINT uCount, PDWORD pData)
{
	UINT n = 0;

	for( UINT i = 11 + (uItem * 2); n < uCount && i < 22; i += 2, n++ ) {

		pData[n] = MotorToHost(GetDateValue(i));
		}
	}

DWORD CCOMLIMasterDriver::GetDateValue(UINT uPos)
{
	return (10 * DWORD(xtob(m_bRx[uPos]))) + DWORD(xtob(m_bRx[uPos+1]));
	}

void  CCOMLIMasterDriver::PutDate(void)
{
	for( UINT i = 0; i < 6; i++ ) {

		AddByte( m_pHex[ m_pCtx->m_bDate[i] / 10 ] );
		AddByte( m_pHex[ m_pCtx->m_bDate[i] % 10 ] );
		}
	}

void  CCOMLIMasterDriver::GetEventFromCache(UINT uItem, UINT uCount, PDWORD pData)
{
	UINT n = 0;

	for( UINT i = uItem; i < uItem + uCount; i++, n++ ) {

		pData[n] = MotorToHost(m_pCtx->m_dEvent[i]);
		}
	}

void  CCOMLIMasterDriver::PutEventIntoCache(void)
{
	if( m_bRx[4] != OP_EVENTW ) return;

	m_pCtx->m_dEvent[0] = DWORD(xtob(m_bRx[6]));

	if( !m_pCtx->m_dEvent[0] ) return;

	UINT uLoop = m_pCtx->m_fAscii ? 3 : 6;

	PDWORD pC  = &m_pCtx->m_dEvent[1];

	PBYTE pR   = &m_bRx[11];

	for( UINT i = 0; i < uLoop; i++, pC += 9 ) {

		pC[0] = xtob(HexBytesToByte(&pR));
		pC[1] = (HexBytesToByte(&pR) << 8) + HexBytesToByte(&pR);

		for( UINT j = 2; j < 9; j++ ) {

			pC[j] = GetEventDec(&pR);
			}
		}
	}

DWORD CCOMLIMasterDriver::GetEventDec(PBYTE *ppR)
{
	BYTE b = HexBytesToByte(ppR);

	return (10 * (b >> 4)) + (b & 0xF);
	}

BYTE  CCOMLIMasterDriver::HexBytesToByte(PBYTE *ppR)
{
	BYTE b = *(*ppR);

	(*ppR)++;

	if( m_pCtx->m_fAscii ) {

		b   = xtob(b);

		b <<= 4;

		b  += xtob(*(*ppR));

		(*ppR)++;
		}

	return b;
	}

BYTE  CCOMLIMasterDriver::xtob(BYTE b)
{
	if( b >= '0' && b <= '9' ) return b - '0';
	if( b >= 'A' && b <= 'F' ) return b - '7';
	if( b >= 'a' && b <= 'f' ) return b - 'W';

	return 0;
	}

BOOL CCOMLIMasterDriver::Resp(void)
{
	UINT uQuant= 0;

	UINT uValue = 0;

	UINT uCount = 0; 

	UINT uState = 0;

	SetTimer(2000);

	while( GetTimer() ) {
		
		UINT uData;
		
		if( (uData = Get(2000)) == NOTHING ) {

			continue;
			}

		switch( uState ) {

			case 0:
				// Start Block

				if( uData == STX ) {

					uCount = 0;
					
					uState ++;

					uValue = 0;

					ClearCheck();
					}

				break;

			case 1:	
				// Start Block.Destination

				uValue *= 10;

				uValue += uData - 0x30;

				AddToCheck(uData);

				if( ++ uCount == 2 ) {

					uCount = 0;

					uValue = 0;
					
					uState ++;
					}

				break;

			case 2:
				// Start Block.Stamp

				AddToCheck(uData);

				uState ++;

				break;

			case 3:
				// Function Block.Message Type

				AddToCheck(uData);

				uValue = 0;

				uState ++;

				break;

			case 4:
				// Function Block.Start Address

				uValue *= 10;

				uValue += uData - 0x30;

				AddToCheck(uData);

				if( ++ uCount == 4 ) {

					uCount = 0;

					uValue = 0; 
					
					uState ++;
					}

				break;

			case 5:
				// Function Block.Quantity

				uValue *= 10;

				uValue += uData - 0x30;

				AddToCheck(uData);

				if( ++ uCount == 2 ) {

					uQuant = uValue;

					uCount = 0;

					m_uPtr = 0;
					
					uState ++;					
					}

				break;

			case 6:				
				// Function Block.Data

				AddToCheck(uData);

				m_bRx[m_uPtr++] = SwapByteBits(uData);

				if( ++ uCount == uQuant ) {

					uState ++;
					}

				break;

			case 7:
				// End Block

				AddToCheck(uData);

				if( uData == ETX ) {

					uState ++;
					}

				break;

			case 8:
				// End Block.Check Sum

				AddToCheck(uData);

				return m_bCheck == 0;
			}
		}

	return FALSE;
	}

UINT CCOMLIMasterDriver::GetWordData(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < m_uPtr; ) {

		if( m_pCtx->m_fAscii ) {

			DWORD dwData = 0;

			for( UINT i = 0; i < 2; i ++ ) {

				BYTE b = m_bRx[n++];

				dwData <<= 4;

				dwData += xtob(b);				
				}			

			*pData ++ = MotorToHost(dwData);
			}
		else {
			*pData ++ = MotorToHost(PU2(&m_bRx[n])[0]);

			n += 2;
			}
		}

	return m_uPtr / 2;
	}

UINT CCOMLIMasterDriver::GetByteData(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < m_uPtr; ) {

		if( m_pCtx->m_fAscii ) {
			}
		else {
			*pData ++ = SwapByteBits(m_bRx[n]);
			}

		n += 1;
		}

	return m_uPtr / 1;
	}

UINT CCOMLIMasterDriver::GetBitsData(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < m_uPtr; ) {

		if( m_pCtx->m_fAscii ) {
			}
		else {
			*pData ++ = m_bRx[n];
			}

		n += 1;
		}

	return m_uPtr / 1;
	}

CCODE CCOMLIMasterDriver::ReadBitBytes(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uSpace = Addr.a.m_Table;

	UINT  uAddr = Addr.a.m_Offset;

	UINT  uLump = uCount * 1;

	StartFrame(OP_REGR);

	AddID(uAddr, 4);

	AddID(uLump, 2);

	Send();

	if( Resp() ) {

		switch( Addr.a.m_Type ) {
			
			case addrByteAsByte:	return GetByteData(pData, uCount);

			case addrBitAsBit:	return GetBitsData(pData, uCount);

			default:		return CCODE_ERROR | CCODE_HARD;
			}
		}

	return CCODE_ERROR;
	}

CCODE CCOMLIMasterDriver::ReadDataWords(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Offset < K_EXTREG ) {

		UINT uAddr = (Addr.a.m_Offset << 4) + 0x4000;

		UINT uLump = AdjCountForAscii(uCount * 2);

		StartFrame(OP_REGR);

		AddID(uAddr, 4);

		AddID(uLump, 2);
		}
	else {
		UINT uAddr = Addr.a.m_Offset;

		UINT uLump = uCount * 2;

		StartFrame(OP_REGREXT);

		AddID(uAddr, 4);

		AddID(uLump, 2);
		}

	Send();

	if( Resp() ) {

		switch( Addr.a.m_Type) {
			
			case addrWordAsWord:	return GetWordData(pData, uCount);

			default:		return CCODE_ERROR | CCODE_HARD;
			}
		}

	return CCODE_ERROR;
	}

void CCOMLIMasterDriver::ClearCheck(void)
{
	m_bCheck = 0;
	}

void CCOMLIMasterDriver::AddToCheck(BYTE bData)
{
	m_bCheck ^= bData;
	}

// End of File
