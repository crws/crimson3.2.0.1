
#include "intern.hpp"

#include "modbus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver
//

// Instantiator

#if !defined(SHARED_CODE)

INSTANTIATE(CModbusDriver);

#endif

// Constructor

CModbusDriver::CModbusDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uMaxWords = 128;
	
	m_uMaxBits  = 2000;

	m_fAscii    = FALSE;

	m_fTrack    = FALSE;

	m_pTx       = NULL;

	m_pRx       = NULL;

	m_uTimeout  = 600;
	}

// Destructor

CModbusDriver::~CModbusDriver(void)
{
	FreeBuffers();
	}

// Configuration

void MCALL CModbusDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_fAscii   = GetByte(pData);

		m_fTrack   = GetByte(pData);

		m_uTimeout = GetWord(pData);
		}
	
	AllocBuffers();
	}
	
void MCALL CModbusDriver::CheckConfig(CSerialConfig &Config)
{
	if( !m_fAscii && Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}
	
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CModbusDriver::Attach(IPortObject *pPort)
{
	if( m_fAscii ) {

		m_pData = MakeDoubleDataHandler();

		pPort->Bind(m_pData);
		}
	else {
		m_pData = MakeSingleDataHandler();

		pPort->Bind(m_pData);
		}
	}

void MCALL CModbusDriver::Open(void)
{
	}

// Device

CCODE MCALL CModbusDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			// NOTE -- If you add extra fields here, you should probably
			// review dupmod, schmod and yaskmp as well to make sure they are
			// correctly initialized in the overriden implementations!!!

			m_pCtx->m_bDrop      = GetByte(pData);
			m_pCtx->m_fRLCAuto   = GetByte(pData);
			m_pCtx->m_fDisable15 = GetByte(pData);
			m_pCtx->m_fDisable16 = GetByte(pData);
			m_pCtx->m_uMax01     = GetWord(pData);
			m_pCtx->m_uMax02     = GetWord(pData);
			m_pCtx->m_uMax03     = GetWord(pData);
			m_pCtx->m_uMax04     = GetWord(pData);
			m_pCtx->m_uMax15     = GetWord(pData);
			m_pCtx->m_uMax16     = GetWord(pData);
			m_pCtx->m_uPing	     = GetWord(pData);
			m_pCtx->m_fDisable5  = GetByte(pData);
			m_pCtx->m_fDisable6  = GetByte(pData);
			m_pCtx->m_fNoCheck   = GetByte(pData);
			m_pCtx->m_fNoReadEx  = GetByte(pData);
			m_pCtx->m_uPoll	     = GetWord(pData);
			m_pCtx->m_fSwapCRC   = GetByte(pData);

			m_pCtx->m_fFlipLong	= GetByte(pData);
			m_pCtx->m_fFlipReal	= GetByte(pData);
			m_pCtx->m_fWriteReply	= GetByte(pData);

			SetLastPoll();
			
			Limit(m_pCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pCtx->m_uMax15, 1, m_pCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pCtx->m_uMax16, 1, m_pCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CModbusDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

UINT MCALL CModbusDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 )  {  // Drop Number

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bDrop = uValue;

			return 1;
			}
		}

	if( uFunc == 4 ) {  // Current target drop number 

		return pCtx->m_bDrop;
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL CModbusDriver::Ping(void)
{
	if( m_pCtx->m_uPing && m_pCtx->m_bDrop ) {
		
		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = SPACE_HOLD;
		
		Addr.a.m_Offset = m_pCtx->m_uPing;
		
		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;

		return DoWordRead(Addr, Data, 1);
		}

	return CMasterDriver::Ping();
	}

CCODE MCALL CModbusDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_bDrop ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	CheckPoll();
	
	switch( Addr.a.m_Table ) {

		case SPACE_HOLD:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax03));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax04));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));

		case SPACE_HOLD32:
			
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG32:
			
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));

		case SPACE_OUTPUT:
			
			return DoBitRead (Addr, pData, min(uCount, m_pCtx->m_uMax01));

		case SPACE_INPUT:
			
			return DoBitRead (Addr, pData, min(uCount, m_pCtx->m_uMax02));

		case SPACE_FILE:
			
			return FileRead (Addr, pData, min(uCount, m_pCtx->m_uMax03));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CModbusDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CheckPoll();
	
	switch( Addr.a.m_Table ) {

		case SPACE_HOLD:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(Addr, pData, min(uCount, m_pCtx->m_uMax16));
				}

			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_HOLD32:
			
			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_OUTPUT:
			
			return DoBitWrite (Addr, pData, min(uCount, m_pCtx->m_uMax15));

		case SPACE_FILE:
			
			return FileWrite (Addr, pData, min(uCount, m_pCtx->m_uMax15));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

void CModbusDriver::Limit(UINT &uData, UINT uMin, UINT uMax)
{
	if( uData < uMin ) uData = uMin;
	
	if( uData > uMax ) uData = uMax;
	}

void CModbusDriver::AllocBuffers(void)
{
	UINT c1 = 2 * m_uMaxWords;
	
	UINT c2 = (m_uMaxBits + 7) / 8;
	
	UINT c3 = (max(c1, c2) + 20) * (m_fAscii ? 2 : 1);

	m_uTxSize = c3;
	
	m_uRxSize = c3;
	
	m_pTx = new BYTE [ m_uTxSize ];

	m_pRx = new BYTE [ m_uRxSize ];
	}

void CModbusDriver::FreeBuffers(void)
{
	if( m_pTx ) {

		delete [] m_pTx;

		m_pTx = NULL;
		}

	if( m_pRx ) {

		delete [] m_pRx;

		m_pRx = NULL;
		}
	}
	
BOOL CModbusDriver::IsHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return TRUE;
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return TRUE;
		}
		
	return FALSE;
	}

WORD CModbusDriver::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return bData - '0';
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return bData - 'A' + 10;
		}
		
	return 0;
	}

BOOL CModbusDriver::IgnoreException(void)
{
	if( m_pRx[1] & 0x80 ) {

		if( m_pCtx->m_fNoReadEx ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Port Access

void CModbusDriver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CModbusDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CModbusDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;
	
	AddByte(m_pCtx->m_bDrop);
	
	AddByte(bOpcode);
	}

void CModbusDriver::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {
	
		m_pTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CModbusDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CModbusDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CModbusDriver::Transact(BOOL fIgnore, BOOL fExpectReply)
{
	if( PutFrame() ) {
	
		if( !m_pCtx->m_bDrop ) { 

			while( m_pData->Read(0) < NOTHING );

			Sleep(100);

			return TRUE;
			}
			
		if( GetFrame() ) {

			SetLastPoll();

			if( m_pRx[0] == m_pTx[0] ) {

				if( fIgnore ) {

					return TRUE;
					}

				if( m_pRx[1] & 0x80 ) {
		
					return FALSE;
					}

				return TRUE;
				}
			}
		
		else if( !fExpectReply ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL CModbusDriver::PutFrame(void)
{
	m_pData->ClearRx();
	
	return m_fAscii ? AsciiTx() : BinaryTx();
	}

BOOL CModbusDriver::GetFrame(void)
{
	return m_fAscii ? AsciiRx() : BinaryRx();
	}

BOOL CModbusDriver::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	if( !m_pCtx->m_fSwapCRC ) {

		AddByte(LOBYTE(wCRC));

		AddByte(HIBYTE(wCRC));
		}
	else {
		AddByte(HIBYTE(wCRC));

		AddByte(LOBYTE(wCRC));
		}

	m_pData->Write(m_pTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CModbusDriver::BinaryRx(void)
{
	UINT uByte = 0;

	UINT uSize = NOTHING;

	UINT uPtr  = 0;

	UINT uGap  = 0;

	UINT uEnd  = FindEndTime();

	SetTimer(m_uTimeout);

	while( GetTimer() ) {

		if( (uByte = RxByte(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

			if( uPtr == m_uRxSize ) {

				return FALSE;
				}

			if( uPtr == 4 ) {

				uSize = FindReplySize(m_fTrack);
				}

			uGap = 0;
			}
		else
			uGap = uGap + 1;

		if( uPtr >= uSize || uGap >= uEnd ) {

			if( uPtr >= 4 ) {

				if( m_pCtx->m_fNoCheck ) {

					return TRUE;
					}
				else {
					m_CRC.Preset();
				
					PBYTE p = m_pRx;
				
					UINT  n = uPtr - 2;
			
					for( UINT i = 0; i < n; i++ ) {

						m_CRC.Add(*(p++));
						}

					WORD c1 = IntelToHost(PU2(p)[0]);
				
					WORD c2 = m_CRC.GetValue();

					if( !m_pCtx->m_fSwapCRC ) {
					
						if( c1 == c2 ) {

							return TRUE;
							}
						}
					else {
						WORD Swap = MAKEWORD(HIBYTE(c2), LOBYTE(c2));

						if( c1 == Swap ) {

							return TRUE;
							}
						}
					}
				}

			uSize = NOTHING;
				
			uPtr  = 0;
			
			uGap  = 0;
			}
		}

	return FALSE;
	}

BOOL CModbusDriver::AsciiTx(void)
{
	BYTE bCheck = 0;
	
	TxByte(':');
		
	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		TxByte(m_pHex[m_pTx[i] / 16]);
		
		TxByte(m_pHex[m_pTx[i] % 16]);

		bCheck += m_pTx[i];
		}
		
	bCheck = BYTE(0x100 - WORD(bCheck));
	
	TxByte(m_pHex[bCheck / 16]);
	TxByte(m_pHex[bCheck % 16]);
	
	TxByte(CR);
	TxByte(LF);
	
	return TRUE;
	}

BOOL CModbusDriver::AsciiRx(void)
{
	UINT uState = 0;

	UINT uPtr   = 0;

	UINT uTimer = 0;
	
	BYTE bCheck = 0;

	SetTimer(m_uTimeout);
	
	while( (uTimer = GetTimer()) ) {

		UINT uByte;
		
		if( (uByte = RxByte(uTimer)) == NOTHING ) {
			
			continue;
			}
			
		switch( uState ) {
		
			case 0:
				if( uByte == ':' ) {

					uPtr   = 0;

					uState = 1;

					bCheck = 0;
					}
				break;
				
			case 1:
				if( IsHex(uByte) ) {
				
					m_pRx[uPtr] = FromHex(uByte) << 4;
				
					uState = 2;
					}
				else {
					if( uByte == CR ) {
					
						continue;
						}

					if( uByte == LF ) {

						if( m_pCtx->m_fNoCheck || bCheck == 0 ) {

							UINT uSize = FindReplySize(TRUE);

							if( uSize < NOTHING ) {

								if( uSize == uPtr + 1 ) {

									return TRUE;
									}

								return FALSE;
								}

							return TRUE;
							}
						}
					
					return FALSE;
					}
				break;
				
			case 2:
				if( IsHex(uByte) ) {

					m_pRx[uPtr] |= FromHex(uByte);
					
					bCheck += m_pRx[uPtr];

					if( ++uPtr == m_uRxSize ) {
						
						return FALSE;
						}
					
					uState = 1;

					break;
					}
				
				return FALSE;
			}
		}

	return FALSE;
	}

// Transport Helpers

UINT CModbusDriver::FindReplySize(BOOL fTrack)
{
	if( fTrack ) {

		switch( m_pRx[1] ) {

			case 1:
			case 2:
			case 3:
			case 4:
				return 5 + (m_pRx[2] ? m_pRx[2] : 256);

			case 5:
			case 6:
				return 8;

			case 15:
			case 16:
				return 8;
			}
		}
			
	return NOTHING;
	}

UINT CModbusDriver::FindEndTime(void)
{
	return ToTicks(25);
	}

// Read Handlers

CCODE CModbusDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD:
			
			StartFrame(0x03);
			
			break;
			
		case SPACE_ANALOG:
			
			StartFrame(0x04);
			
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(uCount);
	
	if( Transact(FALSE, TRUE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x   = PU2(m_pRx + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CModbusDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLD:
			
			StartFrame(0x03);
			
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			
			StartFrame(0x04);
			
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(Addr.a.m_Table == SPACE_HOLD32 ? uCount : uCount * 2);

	if( Transact(FALSE, TRUE) ) {

		BOOL fReal = IsReal(Addr.a.m_Type);

		BOOL fFlip = fReal ? m_pCtx->m_fFlipReal : m_pCtx->m_fFlipLong;

		if( uCount * 4 > m_pRx[2] ) {

			uCount = (m_pRx[2] + 2) / 4;
			}
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_pRx + 3)[n];
			
			pData[n] = MotorToHost(x);

			if( fFlip ) {

				x = pData[n];

				pData[n] = MAKELONG(HIWORD(x), LOWORD(x));
				}
			}

		return uCount;
		}
		
	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CModbusDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
			
		case SPACE_OUTPUT:
			
			StartFrame(0x01);
			
			break;
			
		case SPACE_INPUT:
			
			StartFrame(0x02);
			
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(Addr.a.m_Offset - 1);

	AddWord(uCount);

	if( Transact(FALSE, TRUE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_pRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CModbusDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_HOLD ) {

		if( uCount == 1  && !m_pCtx->m_fDisable6 ) {
			
			StartFrame(6);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(uCount);
			
			AddByte(uCount * 2);
			
			for( UINT n = 0; n < uCount; n++ ) {
				
				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE, m_pCtx->m_fWriteReply) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CModbusDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable = Addr.a.m_Table;

	if( m_pCtx->m_fDisable16 && ( uTable == SPACE_HOLD32 || uTable == SPACE_HOLD ) ) {

		BOOL fReal = IsReal(Addr.a.m_Type);

		BOOL fFlip = fReal ? m_pCtx->m_fFlipReal : m_pCtx->m_fFlipLong;

		CAddress Address;

		Address.m_Ref = Addr.m_Ref;

		for( UINT u = 0; u < uCount; u++ ) {

			DWORD    Data[1];

			Data[0] =  fFlip ? LOWORD(pData[u]) : HIWORD(pData[u]);
			
			if( COMMS_SUCCESS(DoWordWrite(Address, Data, 1)) ) {

				Address.a.m_Offset++;

				Data[0] = fFlip ? HIWORD(pData[u]) : LOWORD(pData[u]);
				
				if( COMMS_SUCCESS(DoWordWrite(Address, Data, 1)) ) {

					if( u == uCount - 1 ) {

						return uCount;
						}

					Address.a.m_Offset++;
					}
				}
			}

		return CCODE_ERROR;
		}
		
	else if( uTable == SPACE_HOLD32 || uTable == SPACE_HOLD ) {

		StartFrame(16);
		
		AddWord(Addr.a.m_Offset - 1);
		
		AddWord(uTable == SPACE_HOLD32 ? uCount : uCount * 2);
		
		AddByte(uCount * 4);

		BOOL fReal = IsReal(Addr.a.m_Type);

		BOOL fFlip = fReal ? m_pCtx->m_fFlipReal : m_pCtx->m_fFlipLong;
		
		for( UINT n = 0; n < uCount; n++ ) {

			DWORD x = pData[n];

			if( fFlip ) {

				x = MAKELONG(HIWORD(pData[n]), LOWORD(pData[n]));
				}
			
			AddLong(x);
			}

		if( Transact(TRUE, m_pCtx->m_fWriteReply) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CModbusDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_OUTPUT ) {

		if( uCount == 1 && !m_pCtx->m_fDisable5 ) {
			
			StartFrame(5);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			StartFrame(15);

			AddWord(Addr.a.m_Offset - 1);

			AddWord(uCount);

			AddByte((uCount + 7) / 8);

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b);
				}
			}

		if( Transact(TRUE, m_pCtx->m_fWriteReply) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// File Handlers

CCODE CModbusDriver::FileRead(AREF Addr, PDWORD pData, UINT uCount )
{
	StartFrame(FILE_READ);

	AddByte(7);
	
	AddByte(6);

	AddWord(Addr.a.m_Extra);
	
	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount);

	if( Transact(FALSE, TRUE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x   = PU2(m_pRx + 5)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}
 
		return uCount;
		}

	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CModbusDriver::FileWrite(AREF Addr, PDWORD pData, UINT uCount )
{
	StartFrame(FILE_WRITE);
	
	AddByte(7 + uCount*2);
	
	AddByte(6);

	AddWord(Addr.a.m_Extra);
	
	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount);
			
	for( UINT n = 0; n < uCount; n++ ) {

		AddWord(LOWORD(pData[n]));
		}

	if( Transact(TRUE, m_pCtx->m_fWriteReply) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

void CModbusDriver::SetLastPoll(void)
{
	if( m_pCtx->m_uPoll ) {

		m_uLast = GetTickCount();
		}
	}

void CModbusDriver::CheckPoll(void)
{
	if( m_pCtx->m_uPoll ) {

		UINT uTime  = GetTickCount() - m_uLast;

		UINT uLimit = GetPollTicks();

		if( uTime < uLimit ) {

			Sleep(uLimit - uTime);
			}
		}
	}

UINT CModbusDriver::GetPollTicks(void)
{
	return ToTicks(m_pCtx->m_uPoll * 100);
	}

BOOL CModbusDriver::IsReal(UINT uType)
{
	switch( uType ) {

		case addrWordAsReal:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}


// End of File
