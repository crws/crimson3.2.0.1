#include "intern.hpp"

#include "enmbtcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver
//

// Instantiator

INSTANTIATE(CEnModbusTCPMaster);

// Constructor

CEnModbusTCPMaster::CEnModbusTCPMaster(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uMaxWords = 120;
	
	m_uMaxBits  = 2000;
	}

// Destructor

CEnModbusTCPMaster::~CEnModbusTCPMaster(void)
{
	}

// Configuration

void MCALL CEnModbusTCPMaster::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CEnModbusTCPMaster::Attach(IPortObject *pPort)
{
	}

void MCALL CEnModbusTCPMaster::Open(void)
{
	}

// Device

CCODE MCALL CEnModbusTCPMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_fDisable15	= GetByte(pData);
			m_pCtx->m_fDisable16	= GetByte(pData);
			m_pCtx->m_fDisable5	= GetByte(pData);
			m_pCtx->m_fDisable6	= GetByte(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->m_fAscii	= GetByte(pData);
			m_pCtx->m_PingReg	= GetWord(pData);
			m_pCtx->m_uMax01	= GetWord(pData);
			m_pCtx->m_uMax02	= GetWord(pData);
			m_pCtx->m_uMax03	= GetWord(pData);
			m_pCtx->m_uMax04	= GetWord(pData);
			m_pCtx->m_uMax15	= GetWord(pData);
			m_pCtx->m_uMax16	= GetWord(pData);

			Limit(m_pCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pCtx->m_uMax15, 1, m_pCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pCtx->m_uMax16, 1, m_pCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEnModbusTCPMaster::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEnModbusTCPMaster::Ping(void)
{
	/*AfxTrace("Ping %u\n", m_pCtx->m_PingReg);*/
	
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}

		if( m_pCtx->m_PingReg == 0 || m_pCtx->m_bUnit == 255 ) {

			return CCODE_SUCCESS;
			}

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = SPACE_HOLDING;
		Addr.a.m_Offset = m_pCtx->m_PingReg;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;

		return Read(Addr, Data, 1);
		}

	return CCODE_ERROR; 
	}

CCODE MCALL CEnModbusTCPMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	UINT uType = Addr.a.m_Type;

	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG32:
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));
			
		case SPACE_HOLDING:

			if( uType == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax03));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG:

			if( uType == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax04));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));

		case SPACE_OUTPUT:
			return DoBitRead (Addr, pData, min(uCount, m_pCtx->m_uMax01));

		case SPACE_INPUT:
			return DoBitRead (Addr, pData, min(uCount, m_pCtx->m_uMax02));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CEnModbusTCPMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	switch( Addr.a.m_Table ) {

		case SPACE_HOLD32:
			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_HOLDING:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(Addr, pData, min(uCount, m_pCtx->m_fDisable16 ? 1 : m_pCtx->m_uMax16));
				}

			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_OUTPUT:
			return DoBitWrite (Addr, pData, min(uCount, m_pCtx->m_fDisable15 ? 1 : m_pCtx->m_uMax15));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Socket Management

BOOL CEnModbusTCPMaster::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CEnModbusTCPMaster::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CEnModbusTCPMaster::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

void CEnModbusTCPMaster::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	m_bCheck = 0;

	m_CRC.Preset();

	if( m_pCtx->m_fAscii ) {

		m_bTxBuff[m_uPtr++] = ':';
		}
	
	AddByte(m_pCtx->m_bUnit, TRUE);
	
	AddByte(bOpcode, TRUE);

	}

void CEnModbusTCPMaster::AddByte(BYTE bData, BOOL fCheck)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {

		if( !m_pCtx->m_fAscii ) {

			m_bTxBuff[m_uPtr++] = bData;
			
			if( fCheck ) {

				m_CRC.Add(bData);
				}

			return;
			}

		if( fCheck ) {

			m_bTxBuff[m_uPtr++] = m_pHex[bData / 16];

			m_bTxBuff[m_uPtr++] = m_pHex[bData % 16];
			
			m_bCheck += bData;

			return;
			}

		m_bTxBuff[m_uPtr++] = bData;
		}
	}

void CEnModbusTCPMaster::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData), TRUE);

	AddByte(LOBYTE(wData), TRUE);
	}

void CEnModbusTCPMaster::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void CEnModbusTCPMaster::AddCheck(void)
{
	if( !m_pCtx->m_fAscii ) {

		WORD wCRC = m_CRC.GetValue();

		AddByte(LOBYTE(wCRC), FALSE);

		AddByte(HIBYTE(wCRC), FALSE);

		return;
		}

	m_bCheck = BYTE(0x100 - WORD(m_bCheck));

	AddByte(m_pHex[m_bCheck / 16], FALSE);

	AddByte(m_pHex[m_bCheck % 16], FALSE);

	AddByte(CR, FALSE);

	AddByte(LF, FALSE);
	}

// Helpers

BOOL CEnModbusTCPMaster::IsHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return TRUE;
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return TRUE;
		}
		
	return FALSE;
	}

WORD CEnModbusTCPMaster::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return bData - '0';
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return bData - 'A' + 10;
		}
		
	return 0;
	}

 
// Transport Layer

BOOL CEnModbusTCPMaster::SendFrame(void)
{
	UINT uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEnModbusTCPMaster::RecvBinaryFrame(BOOL fWrite)
{
	SetTimer(m_pCtx->m_uTime2);
	
	UINT uPtr  = 0;

	UINT uSize = 0;

	UINT uGet = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 3 ) {

				if( !uGet ) {

					if( m_bRxBuff[0] == m_bTxBuff[0] ) {

						uGet = fWrite ? 8 : UINT(m_bRxBuff[2] + 5);
						}
					else {	
						return FALSE;
						}
					}

				if( uGet && uPtr >= uGet ) {

					m_CRC.Preset();
				
					PBYTE p = m_bRxBuff;
				
					for( UINT i = 0; i < uGet - 2; i++ ) {

						m_CRC.Add(*(p++));
						}
							
					WORD c1 = IntelToHost(PWORD(p)[0]);
				
					WORD c2 = m_CRC.GetValue();

					if( c1 == c2 ) {

						return TRUE;
						}

					return FALSE;
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CEnModbusTCPMaster::RecvAsciiFrame(BOOL fWrite)
{
	SetTimer(m_pCtx->m_uTime2);
	
	UINT uPtr  = 0;

	UINT uSize = 0;

	UINT uGet = 0; 

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 7 ) {

				if( !uGet ) {
					
					if( m_bRxBuff[0] == ':' ) {

						if( m_bRxBuff[1] == m_bTxBuff[1] && m_bRxBuff[2] == m_bTxBuff[2] ) {

							BYTE bRx = m_bRxBuff[5];

							if( IsHex(m_bRxBuff[5]) ) {

								bRx = FromHex(bRx);
								}
				
							bRx = bRx << 4;

							if( IsHex( m_bRxBuff[6]) ) {

								bRx |= FromHex(m_bRxBuff[6]);
								}
							else {
								bRx |= m_bRxBuff[6];
								}
							
							uGet = fWrite ? 17 : UINT(bRx * 2 + 11);
							}
						else {	
							return FALSE;
							}
						}
					}

				if( uGet && uPtr >= uGet ) {

					BYTE bCheck = 0;

					for( UINT x = 1, y = 1; y < uGet; y++ ) {

						BYTE bRx = m_bRxBuff[y];
					
						if( IsHex(bRx) ) {

							bRx = FromHex(bRx);
							}

						else {
							if ( bRx == CR ) {

								continue;
								}

							if ( bRx == LF ) {

								if ( bCheck == 0 ) {

									memcpy(m_bRxBuff, m_bRxBuff + 1, uGet - 1);

									return TRUE;
									}
								}

							return FALSE;
							}
									
						BOOL fCheck = TRUE;

						if( y % 2 == 1 ) {

							fCheck = FALSE;

							m_bRxBuff[x] = bRx << 4;
							}

						else {
							m_bRxBuff[x] |= bRx;
							}

						if( fCheck ) {

							bCheck += m_bRxBuff[x++];
						      	}
						}
					}
				}
			
			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}
	
	return FALSE;
	}

BOOL CEnModbusTCPMaster::RecvFrame(BOOL fWrite)
{
	if( !m_pCtx->m_fAscii ) {

		return RecvBinaryFrame(fWrite);
		}

	return RecvAsciiFrame(fWrite);

	}

BOOL CEnModbusTCPMaster::Transact(BOOL fIgnore)
{
	Sleep(10);
	
	AddCheck();

	if( SendFrame() && RecvFrame(fIgnore) ) {

		if( fIgnore ) {

			return TRUE;
			}

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CEnModbusTCPMaster::CheckFrame(void)
{
	if( !(m_bRxBuff[1] & 0x80) ) {
	
		return TRUE;
		}

	return FALSE;
	}

// Read Handlers

CCODE CEnModbusTCPMaster::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Read Word %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount); */
	
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLDING:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(uCount);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x = PU2(m_bRxBuff + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEnModbusTCPMaster::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Read Long %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);*/
		
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLDING:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(uCount * 2);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x = PU4(m_bRxBuff + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CEnModbusTCPMaster::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Read Bit %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);*/
		
	switch( Addr.a.m_Table ) {
			
		case SPACE_OUTPUT:
			StartFrame(0x01);
			break;
			
		case SPACE_INPUT:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(Addr.a.m_Offset - 1);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRxBuff[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CEnModbusTCPMaster::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Write Word %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);*/
       	
	if( Addr.a.m_Table == SPACE_HOLDING ) {

		if( ( uCount == 1 ) && !m_pCtx->m_fDisable6 ) {
			
			StartFrame(6);

			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);

			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(uCount);
			
			AddByte(uCount * 2, TRUE);
			
			for( UINT n = 0; n < uCount; n++ ) {
				
				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CEnModbusTCPMaster::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Write Long %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);*/
	
	UINT uTable = Addr.a.m_Table;
	
	if( ( uTable == SPACE_HOLD32 ) || ( uTable == SPACE_HOLDING ) ) {

		StartFrame(16);
		
		AddWord(Addr.a.m_Offset - 1);
		
		AddWord(uCount * 2);
		
		AddByte(uCount * 4, TRUE);
		
		for( UINT n = 0; n < uCount; n++ ) {
			
			AddLong(pData[n]);
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CEnModbusTCPMaster::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Write Bit %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);*/
	
	if( Addr.a.m_Table == SPACE_OUTPUT ) {

		if( ( uCount == 1 ) && !m_pCtx->m_fDisable5 ) {

			StartFrame(5);

			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			StartFrame(15);

			AddWord(Addr.a.m_Offset - 1);

			AddWord(uCount);

			AddByte((uCount + 7) / 8, TRUE);

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b, TRUE);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b, TRUE);
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Helpers

void CEnModbusTCPMaster::Limit(UINT &uData, UINT uMin, UINT uMax)
{
	if( uData < uMin ) uData = uMin;
	
	if( uData > uMax ) uData = uMax;
	}

// End of File
