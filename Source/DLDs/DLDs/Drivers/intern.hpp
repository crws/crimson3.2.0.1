
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 DLDs Internal Header
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3CORE_HPP

#define	INCLUDE_C3CORE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Disable C3Core
//

#define INCLUDE_C3CORE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <StdEnv.hpp>

#include <C3Basis.hpp>

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Version Information
//

#if defined(_DEBUG)

#include "../../../../../../Build/Bin/Version/Debug/version.hxx"

#else

#include "../../../../../../Build/Bin/Version/Release/version.hxx"

#endif

////////////////////////////////////////////////////////////////////////
//
// DLD Instantiator
//

#define INSTANTIATE(Name)						\
									\
	clink IUnknown * Create_Driver(PCSTR pName)			\
	{								\
		return (IUnknown *) (IDriver *) New Name;		\
		}							\
									\
	clink void AeonCmMain(void)					\
	{								\
		CPrintf Name("Driver%4.4X", DRIVER_ID);			\
									\
		piob->RegisterInstantiator(Name, Create_Driver);	\
		}							\
									\
	clink void AeonCmExit(void)					\
	{								\
		CPrintf Name("Driver%4.4X", DRIVER_ID);			\
									\
		piob->RevokeInstantiator(Name);				\
		}							\

////////////////////////////////////////////////////////////////////////
//
// Platform Macros
//

#define CODE_SEG	const

#define	CTEXT		static char CODE_SEG

#define	NewHere(p)	new(0, p)

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef signed int	C3INT;

typedef float		C3REAL;

//////////////////////////////////////////////////////////////////////////
//
// Type Conversion
//

inline C3REAL I2R(DWORD d)
{
	return *((C3REAL *) &d);
}

inline DWORD R2I(C3REAL r)
{
	return *((DWORD *) &r);
}

////////////////////////////////////////////////////////////////////////
//
// DLD Thunks
//

#define DEFMETH(x)	virtual x METHOD 

#define MCALL		METHOD

#define IDevice		ICommsDevice

#define IHelper		ICommsHelper

#define ATOI(x)		atoi(x)

#define ATOF(x)		R2I(atof(x))

#define RAND(x)		(rand() % (x))

#define HostAlignStack() // !!!

#define FAR

#define SLOW

#define stricmp		strcasecmp

#define strnicmp	strncasecmp

#define USING_STDENV	1

#define GuardTask(f)	GuardThread(f) // !!!

////////////////////////////////////////////////////////////////////////
//
// Bad Stuff
//

#define Critical(x)	// !!!

#define	AfxRaiseIRQL(x)	// !!!

#define	AfxLowerIRQL()	// !!!

#define CanTaskWait()	TRUE // !!!

#define HostMaxIPL()	// !!!

#define HostMinIPL()	// !!!

//////////////////////////////////////////////////////////////////////////
//
// Trap Support
//

#if !defined(HostTrap)

#define HostTrap(x)	((void) (x))

#endif

//////////////////////////////////////////////////////////////////////////
//
// Standard Properties for Tags
//

enum
{
	tpAsText	= 1,
	tpLabel		= 2,
	tpDescription	= 3,
	tpPrefix	= 4,
	tpUnits		= 5,
	tpSetPoint	= 6,
	tpMinimum	= 7,
	tpMaximum	= 8,
	tpForeColor	= 9,
	tpBackColor	= 10,
	tpName		= 11,
	tpIndex		= 12,
	tpAlarms	= 13,
	tpTextOff	= 14,
	tpTextOn	= 15,
	tpStateCount	= 16,
	tpDeadband	= 17,
	tpStateText	= 1000,
	};

//////////////////////////////////////////////////////////////////////////
//
// RLC Bitmap
//

struct BITMAP_RLC
{
	DWORD	Frame;
	DWORD	Format;
	DWORD	Width;
	DWORD	Height;
	DWORD	Stride;
	BYTE	Data[0];
	};

/////////////////////////////////////////////////////////////////////////
//
// Database Init Alignment
//

inline void Item_Align2(PCBYTE &pData)
{
	pData += (DWORD(pData) & 1);
	}

inline void Item_Align4(PCBYTE &pData)
{
	UINT n = (DWORD(pData) & 3);

	pData += n ? (4-n) : 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// 16-bit CRC Generator
//

class CRC16 
{
	public:
		// Constructor
		inline CRC16(void);
			
		// Attributes
		inline WORD GetValue(void) const;
			
		// Operations
		inline void Clear(void);
		inline void Preset(void);
		inline void Add(BYTE b);
			
	protected:
		// Lookup Data
		static WORD CODE_SEG m_Table[];

		// Data Members
		WORD m_CRC;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructor

inline CRC16::CRC16(void)
{
	m_CRC = 0;
	}
	
// Attributes

inline WORD CRC16::GetValue(void) const
{
	return m_CRC;
	}
	
// Operations

inline void CRC16::Clear(void)
{
	m_CRC = 0x0000;
	}
	
inline void CRC16::Preset(void)
{
	m_CRC = 0xFFFF;
	}

inline void CRC16::Add(BYTE b)
{
	m_CRC = WORD((m_CRC >> 8) ^ m_Table[b ^ (m_CRC & 0xFF)]);
	}

// End of File

#endif
