
#include "intern.hpp"

#include "ModbusTCP.hpp"

//////////////////////////////////////////////////////////////////////////
//
// EtherTrak3 Modbus TCP Driver
//

class CE3ModbusTCPDriver : public CModbusTCPMaster
{
	public:
		// Constructor
		CE3ModbusTCPDriver(void);

		// Destructor
		~CE3ModbusTCPDriver(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);

		// Entry Points
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
	};

// End of File
