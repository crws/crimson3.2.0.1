
#include "intern.hpp"

#include "impact.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Impact Camera Driver
//

// Timeouts

static UINT const timeSendTimeout  = 5000;

static UINT const timeSendDelay	   = 10;

static UINT const timeRecvTimeout  = 10000;

static UINT const timeRecvDelay	   = 10;

static UINT const timeBuffDelay    = 100;

// Instantiator

INSTANTIATE(CImpactCamera);

// Constructor

CImpactCamera::CImpactCamera(void)
{
	m_Ident = DRIVER_ID;

	m_pCtx  = NULL;

	m_uKeep = 0;

	m_pHead = NULL;

	m_pTail = NULL;

	m_pHelper = NULL;
	}

// Srcructor

CImpactCamera::~CImpactCamera(void)
{
	}

// Configuration

void MCALL CImpactCamera::Load(LPCBYTE pData)
{
	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	
	if ( GetWord( pData ) == 0x1234 ) {

		}
	}
	
// Management

void MCALL CImpactCamera::Attach(IPortObject *pPort)
{

	}

void MCALL CImpactCamera::Open(void)
{
	}

// Device

CCODE MCALL CImpactCamera::DeviceOpen(IDevice *pDevice)
{
	CCameraDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {
			
			m_pCtx = new CContext;

			m_pCtx->m_uDevice = GetWord(pData);
			m_pCtx->m_IP	  = GetAddr(pData);
			m_pCtx->m_uPort	  = GetWord(pData);
			m_pCtx->m_fKeep   = FALSE;
			m_pCtx->m_fPing   = TRUE;
			m_pCtx->m_uTime1  = 2000;
			m_pCtx->m_uTime2  = 1000;
			m_pCtx->m_uTime3  = 500;
			m_pCtx->m_uTime4  = GetWord(pData);
			m_pCtx->m_uLast3  = GetTickCount();
			m_pCtx->m_uLast4  = GetTickCount();
			m_pCtx->m_pSock	  = NULL;
			m_pCtx->m_pData   = NULL;
			m_pCtx->m_Scale   = GetByte(pData);
			m_pCtx->m_X	  = GetWord(pData);
			m_pCtx->m_Y	  = GetWord(pData);

			if( m_pCtx->m_Scale ) {

				SPrintf(PTXT(m_pCtx->m_Http), 

					PTXT("GET /Image/width=%u,height=%u/ HTTP/1.1\r\n"
					     "Accept: image/x-xbitmap, application/xaml+xml\r\n"
					     "Accept-Language: en-us\r\n"
					     "Connection: keep-alive\r\n"
					     "\r\n"), 

					     m_pCtx->m_X, 
					     m_pCtx->m_Y );
				}
			else {
				SPrintf(PTXT(m_pCtx->m_Http), 

					PTXT("GET /Image/ HTTP/1.1\r\n"
					     "Accept: image/x-xbitmap, application/xaml+xml\r\n"
					     "Accept-Language: en-us\r\n"
					     "Connection: keep-alive\r\n"
					     "\r\n"));
				}

			memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CImpactCamera::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {  
		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		CloseSocket(FALSE);

		delete m_pCtx->m_pData;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CCameraDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CImpactCamera::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CImpactCamera::ReadImage(PBYTE &pData)
{
	if( OpenSocket() ) {

		if ( GetImage(pData) ) {

			m_pCtx->m_uLast4 = GetTickCount();

			return CCODE_SUCCESS;
			}
		
		if( Ping() == CCODE_SUCCESS ) {

			UINT dt = GetTickCount() - m_pCtx->m_uLast4;

			UINT tt = 1000 * ToTicks(m_pCtx->m_uTime4);

			if( !tt || dt < tt ) {

				pData = NULL;

				return CCODE_SUCCESS;
				}
			}

		CloseSocket(FALSE);
		}	

	return CCODE_ERROR;
	}

void MCALL CImpactCamera::SwapImage(PBYTE pData)
{
	delete m_pCtx->m_pData;

	m_pCtx->m_uInfo[0] = GetTimeStamp();

	m_pCtx->m_uInfo[1] = ((BITMAP_RLC *) pData)->Frame;

	m_pCtx->m_pData    = pData;
	}

void MCALL CImpactCamera::KillImage(void)
{
	if( m_pCtx->m_pData ) {
		
		memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

		delete m_pCtx->m_pData;

		m_pCtx->m_pData = NULL;
		}
	}

PCBYTE MCALL CImpactCamera::GetData(UINT uDevice)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			return pCtx->m_pData;
			}
		}

	return NULL;
	}

UINT MCALL CImpactCamera::GetInfo(UINT uDevice, UINT uParam)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			if( uParam < elements(pCtx->m_uInfo) ) {

				return pCtx->m_uInfo[uParam];
				}
			}
		}

	return 0;
	}

BOOL MCALL CImpactCamera::SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	return FALSE;
	}

BOOL MCALL CImpactCamera::LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	return FALSE;
	}

BOOL MCALL CImpactCamera::UseSetup(PVOID pContext, UINT uIndex)
{
	return FALSE;
	}

// Socket Management

BOOL CImpactCamera::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactCamera::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast3;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {
			
					m_pCtx->m_uLast4 = GetTickCount();
					
					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CImpactCamera::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock  = NULL;

		m_pCtx->m_uLast3 = GetTickCount();

		m_uKeep--;
		}
	}

// Implementation

BOOL CImpactCamera::GetImage(PBYTE &pImage)
{
	if( Send(m_pCtx->m_pSock, m_pCtx->m_Http, PTXT(&m_pCtx->m_Http + 1)) ) {

		BOOL  fOk = FALSE;

		UINT  uPos  = 0;

		UINT  uBytes = m_pCtx->m_X * m_pCtx->m_Y * 3;
					
		PBYTE pWork = new BYTE[uBytes];

		memset(pWork,  0, uBytes);

		SetTimer(timeRecvTimeout);

		while( GetTimer() ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				UINT n = uBytes - uPos;
				
				if( m_pCtx->m_pSock->Recv(pWork + uPos, n) == S_OK ) {

					if( !fOk ) {
						
						for( UINT o = 0; o < n; o++ ) {

							if(pWork[o] == 'O' && pWork[o+1] == 'K') {

								fOk = TRUE;

								break;
								}
							}
						}
	
					else {	
						if( uPos == 0 ) {

							DWORD dwPNG = MotorToHost(PU4(pWork)[0]);

							if( dwPNG != PNGP ) {

								delete pWork;

								return FALSE;
								}
							}

						DWORD dwEnd = MotorToHost(PU4(pWork + uPos + n - 8)[0]);

						if( dwEnd == IEND ) {

							PBYTE pResult = m_pExtra->Png2RLCBmp(pWork, uPos + n);

							pImage = pResult;	

							delete pWork;

							return TRUE;
							}
								
						uPos += n;

						continue;
						}
					}
				}

			if( Phase == PHASE_CLOSING ) {

				break;
				}

			if( Phase == PHASE_ERROR ) {

				break;
				}

			Sleep(timeRecvDelay);
			}
						
		delete pWork;
		}
	
	return FALSE;
	}

BOOL CImpactCamera::Send(ISocket *pSock, PCTXT pText, PTXT pArgs)
{
	UINT uSize = strlen(pText);

	PTXT pWork = new char [ uSize + 256 ];

	SPrintf(pWork, pText, pArgs);

	if( Send(pSock, PCBYTE(pWork), strlen(pWork)) ) {

		delete pWork;

		return TRUE;
		}

	delete pWork;

	return FALSE;
	}

BOOL CImpactCamera::Send(ISocket *pSock, PCBYTE pText, UINT uSize)
{
	UINT uLimit = 1280;

	while( uSize ) {

		CBuffer *pBuff = CreateBuffer(uLimit, TRUE);

		if( pBuff ) {

			PBYTE pData = pBuff->GetData();

			UINT  uCopy = min(uLimit, uSize);

			memcpy(pData, pText, uCopy);

			pBuff->AddTail(uCopy);  

			SetTimer(timeSendTimeout);

			while( pSock->Send(pBuff) == E_FAIL ) {

				UINT Phase;

				pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN && GetTimer() ) {

					Sleep(timeSendDelay);

					continue;
					}

				BuffRelease(pBuff);
				
				return FALSE;
				}

			pText += uCopy;

			uSize -= uCopy;
			}
		else {  
			Sleep(timeBuffDelay);

			return FALSE;
			}
		}

	return TRUE;
	}

// End of File
