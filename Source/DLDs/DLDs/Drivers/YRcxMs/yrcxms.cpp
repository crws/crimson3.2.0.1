#include "intern.hpp"

#include "yrcxms.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series Master Serial Driver
//

// Instantiator

INSTANTIATE(CYamahaRcxMsDriver);

// Constructor

CYamahaRcxMsDriver::CYamahaRcxMsDriver(void)
{
	m_Ident         = DRIVER_ID; 
	
	}

// Destructor

CYamahaRcxMsDriver::~CYamahaRcxMsDriver(void)
{
	m_pExtra->Release();
	}

// Configuration

void MCALL CYamahaRcxMsDriver::Load(LPCBYTE pData)
{
	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}
	
void MCALL CYamahaRcxMsDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CYamahaRcxMsDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CYamahaRcxMsDriver::Open(void)
{
	}

// Device

CCODE MCALL CYamahaRcxMsDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pBase->m_uBrkLn = 0;

			m_pBase->m_uBrkPt = 0;

			memset(m_pBase->m_Error, 0, sizeof(m_pBase->m_Error));

			memset(m_pBase->m_Term, 0, sizeof(m_pBase->m_Term));

			m_pBase->m_Busy   = 0;

			m_pBase->m_fInit  = TRUE;

			m_pBase->m_Table  = NULL;

			m_pBase->m_uPoints = 0;

			m_pBase->m_fLogOff = FALSE;

			pDevice->SetContext(m_pCtx);

			SetError();

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CYamahaRcxMsDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) { 

		if( m_pBase->m_Table ) {

			delete [] m_pBase->m_Table;
			}

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Implementation

BOOL CYamahaRcxMsDriver::Transact(void)
{
	if( Send() ) {
		
		if( m_pItem->m_Resp == respWait ) {

			m_pBase->m_Busy = 1;

			return TRUE;
			}

		if( RecvFrame(FALSE) ) {
		
			return CheckFrame();
			}
		}

	return FALSE; 
	}

BOOL CYamahaRcxMsDriver::Send(void)
{
	m_pData->ClearRx();
	
	m_pData->Write(m_bTxBuff, m_uTxPtr, FOREVER);

/*	AfxTrace("\nTx ");

	for( UINT u = 0; u < m_uTxPtr; u++ ) {

		AfxTrace("%c", m_bTxBuff[u]);
		}  
*/
	return TRUE;
	}

BOOL CYamahaRcxMsDriver::RecvFrame(BOOL fLogin)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_uRxPtr = 0;

	UINT uCR = 0;

//	AfxTrace("\nRx ");

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

//		AfxTrace("%c", uData);
  
		if( uCR ) {

			if( m_pItem->m_Resp == respSync && uCR < 2 ) {

				continue;
				}

			return TRUE;
			}

		m_bRxBuff[m_uRxPtr++] = uData;

		if( uData == CR ) {
			
			uCR = 1;

			SetTimer(50);
			}
		}

	return uCR ? TRUE : FALSE;
	}

// End of File
