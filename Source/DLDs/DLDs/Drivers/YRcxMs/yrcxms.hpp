#include "rcxbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yamaha Robot Controller RCX Series Master Serial Driver
//

class CYamahaRcxMsDriver : public CYamahaRcxMbaseDriver
{
	public:
		// Constructor
		CYamahaRcxMsDriver(void);

		// Destructor
		~CYamahaRcxMsDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
	protected:
		// Device Context
		struct CContext : CYamahaRcxMbaseDriver::CBaseCtx
		{
		
			};

		// Data Members
		CContext * m_pCtx;

		// Transport
		BOOL Transact(void);
		
		BOOL Send(void);
		BOOL RecvFrame(BOOL fLogin);

		// Helpers
		

		
	};

// End of File
