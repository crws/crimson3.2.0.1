
#include "intern.hpp"

#include "gandlce.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Giddings and Lewis C/E Driver
//

// Instantiator

INSTANTIATE(CGandLCEDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CGandLCEDriver::CGandLCEDriver(void)
{
	m_Ident      = DRIVER_ID;
	}

// Destructor

CGandLCEDriver::~CGandLCEDriver(void)
{
	}

// Configuration

void MCALL CGandLCEDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CGandLCEDriver::CheckConfig(CSerialConfig &Config)
{
	}
	
// Management

void MCALL CGandLCEDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CGandLCEDriver::Open(void)
{
	}

// Device

CCODE MCALL CGandLCEDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CGandLCEDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CGandLCEDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = DV;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1 );
	}

CCODE MCALL CGandLCEDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	switch( NoReadTransmit( uOffset, pData ) ) {

		case RNT_DATA:
			return 1;

		case RNT_NODATA:
			return CCODE_ERROR | CCODE_NO_DATA;
		}

	StartFrame();

	AddByte('K');

	if( Transact(RDVSIZE) ) {

		GetResponse(uOffset, pData);

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CGandLCEDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if( NoWriteTransmit( uOffset, *pData ) ) {

		return 1;
		}

//**/	AfxTrace0("\r\n** WRITE ** ");

	UINT uSize = AddWrite(uOffset, *pData);

	if( SendStart() ) {

		if( m_bRx[0] == m_bCommand ) {

			if( m_bCommand == 'S' ) {

				return 1;
				}
	
			if( Transact( uSize ) && GetResponse( uOffset, pData ) ) {

				return 1;
				}
			}
		}
	
	return CCODE_ERROR;
	}

// PRIVATE METHODS

// Frame Building

void CGandLCEDriver::StartFrame(void)
{
	m_uPtr = 0;
	}

void CGandLCEDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CGandLCEDriver::AddWord(WORD wData)
{
	AddByte( LOBYTE(wData) );
	AddByte( HIBYTE(wData) );
	}

UINT CGandLCEDriver::AddWrite( UINT uCommand, DWORD dData )
{
	StartFrame();

	m_bCommand = GetCommand( uCommand );

	UINT i;

	switch( uCommand ) {

		case IEXE:
			AddByte( m_dICache[0] );
			AddByte( m_dICache[1] );
			for( i = IBS1-ISEL; i <= IOSP-ISEL; i++ ) {

				AddWord( m_dICache[i] );
				}

			return ISIZE;

		case PEXE:
			AddByte( m_dPCache[0] );
			AddWord( m_dPCache[1] );
			return PSIZE;

		case CEXE:
			AddByte( m_dCCache[0] );
			AddByte( m_dCCache[1] );
			for( i = CBCL-CBCT; i <= CDS-CBCT; i++ ) {

				AddWord( m_dCCache[i] );
				}

			return CSIZE;

		case SS:
			return SSSIZE;

		case DV:
			AddWord( LOWORD(dData) );
			return WDVSIZE;

		case SSV:
			AddByte( LOBYTE(dData) );
			m_bSSV = LOBYTE(dData);
			return SSVSIZE;
		}

	return 0;
	}

// Transport Layer

BOOL CGandLCEDriver::Transact(UINT uCount)
{
	Send();

	return GetReply(uCount);
	}

BOOL CGandLCEDriver::SendStart(void)
{
//**/	AfxTrace1("\r\n[%2.2x]", m_bCommand );

	m_pData->Write( m_bCommand, FOREVER );

	return GetReply(1);
	} 

void CGandLCEDriver::Send(void)
{
	m_pData->ClearRx();

	Put();
	}

BOOL CGandLCEDriver::GetReply(UINT uCount)
{
	UINT uRcv   = 0;

	UINT uTimer = 0;

	UINT uData;

	BYTE bData;

	SetTimer( TIMEOUT );

//**/	AfxTrace0("\r\n");

	while( (uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);

		m_bRx[uRcv++] = bData;

		if( uRcv == uCount ) {

			return TRUE;
			}
		}	

	return FALSE;
	}

BOOL CGandLCEDriver::GetResponse( UINT uCommand, PDWORD pData )
{
	switch( uCommand ) {

		case DV:
			*pData = MakeWord(0);
			break;

		case PEXE:
			for( UINT i = PSC - POIP; i <= PDSP - POIP; i++ ) {

				m_dPCache[i] = MakeWord( 2*(i - 2) );
				}

			break;
		}

	return TRUE;
	}

// Port Access

void CGandLCEDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k] );

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );
	}

UINT CGandLCEDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

UINT  CGandLCEDriver::NoReadTransmit( UINT uCommand, PDWORD pData )
{
	Sleep( 20 );

	UINT   uCPos = 0;

 	PDWORD pCache = GetCachePosition( uCommand, &uCPos );

	if( pCache ) {

		*pData = pCache[uCPos];

		return RNT_DATA;
		}

	switch( uCommand ) {

		case SSV:
			*pData = m_bSSV;
			return RNT_DATA;

		case SS:
		case IEXE:
		case PEXE:
		case CEXE:
			*pData = 0;
			return RNT_DATA;
		}

	return RNT_CONTINUE;
	}

BOOL  CGandLCEDriver::NoWriteTransmit( UINT uCommand, DWORD dData )
{
	if( uCommand >= PSC && uCommand <= PDSP ) {

		return TRUE;
		}

	UINT uCPos = 0;

	PDWORD pCache = GetCachePosition( uCommand, &uCPos );

	if( pCache ) {

		pCache[uCPos] = dData;

		return TRUE;
		}

	return FALSE;
	}

BYTE  CGandLCEDriver::GetCommand(UINT uCommand)
{
	switch( uCommand ) {

		case IEXE:	return 'I';
		case PEXE:	return 'P';
		case CEXE:	return 'C';
		case SS:	return 'S';
		case DV:	return 'k';
		case SSV:	return 's';
		}

	return 0;
	}

PDWORD CGandLCEDriver::GetCachePosition(UINT uCommand, UINT * pPos)
{
	if( uCommand >= ISEL && uCommand <= IOSP ) {

		*pPos = uCommand - ISEL;

		return PDWORD(&m_dICache[0]);
		}

	if( uCommand >= POIP && uCommand <= PDSP ) {

		*pPos = uCommand - POIP;

		return PDWORD(&m_dPCache[0]);
		}

	if( uCommand >= CBCT && uCommand <= CDS ) {

		*pPos = uCommand - CBCT;

		return PDWORD(&m_dCCache[0]);
		}

	return NULL;
	}

WORD CGandLCEDriver::MakeWord(UINT uPos)
{
	return (m_bRx[uPos+1] << 8) + m_bRx[uPos];
	}

// End of File
