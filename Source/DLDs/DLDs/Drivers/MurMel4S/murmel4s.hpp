//////////////////////////////////////////////////////////////////////////
//
// Murata MELSEC 4 Slave Driver
//
//

class CMurMel4SlaveDriver : public CSlaveDriver
{
	public:
		// Constructor
		CMurMel4SlaveDriver(void);

		// Destructor
		~CMurMel4SlaveDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Entry Points
		DEFMETH(void) Service(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);

	protected:

		// Data Members
		BYTE	m_Rx[256];
		BYTE	m_Tx[256];
		UINT	m_Ptr;
		LPCTXT	m_pHex;
		BYTE    m_Drop;
		BYTE	m_Melsec;
		BYTE	m_PC;
	
		// Implementation
		BOOL	Rx(void);
		void	HandleFrame(void);
		BOOL	CheckDrop(void);
		BOOL	CheckPC(void);
		BOOL	CheckCmd(void);
		BOOL	DoRead(void);
		BOOL	DoWrite(void);
		UINT	GetWords(PDWORD pData, UINT uCount);
		WORD	GetData(PCTXT pText, UINT uCount, BOOL fHex);
		void	SetWords(PDWORD pData, UINT uCount);
		void	AddHex(UINT uValue, UINT uMask);
		void	AddByte(BYTE bValue);
		void	Start(void);
		void	AddID(void);
		void	End(void);
		void	SendACK(void);
		void	SendNAK(BYTE bError);
		};

// End of file

