#include "intern.hpp"

#include "murmel4s.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Murata MELSEC 4 Slave Driver
//

// Instantiator

INSTANTIATE(CMurMel4SlaveDriver);

// Constructor

CMurMel4SlaveDriver::CMurMel4SlaveDriver(void)
{
	m_Ident		= DRIVER_ID;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;
       	}

// Destructor

CMurMel4SlaveDriver::~CMurMel4SlaveDriver(void)
{

	}

// Config

void MCALL CMurMel4SlaveDriver::Load(LPCBYTE pData)
{
	if ( GetWord( pData ) == 0x1234 ) {

		m_Drop	 = GetByte(pData);

		m_Melsec = GetByte(pData);	// NOTE:  ONLY MELSEC 4 is supported

		m_PC	 = GetByte(pData);

		return;
		}
	}

void MCALL CMurMel4SlaveDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, FALSE);
	}

void MCALL CMurMel4SlaveDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

// Entry Points

void MCALL CMurMel4SlaveDriver::Service(void)
{
	for(;;) {

		if( Rx() ) {

			HandleFrame();
			}
		}
	}

// Implementation

BOOL CMurMel4SlaveDriver::Rx(void)
{
	UINT uData = 0;

	m_Ptr	   = 0;

	BOOL fENQ  = FALSE;

	BOOL fCR   = FALSE;

	for(;;) {
	
		if( (uData = m_pData->Read(FOREVER)) == NOTHING ) {

			continue;
			}

		if( !fENQ && (uData == ENQ) ) {

			fENQ = TRUE;

			continue;
			}

		if( fENQ ) {

			if( !fCR && (uData == CR) ) {

				fCR = TRUE;

				continue;
				}

			if( fCR ) { 

				return (uData == LF );
				}

			if( uData == ENQ ) {

				return FALSE;
				}

			m_Rx[m_Ptr++] = uData;
			}
		}

	return FALSE;
	}

void CMurMel4SlaveDriver::HandleFrame(void)
{
	if( !CheckDrop() ) {

		return;
		}

	if( !CheckPC() ) {

		SendNAK(0x10);

		return;
		}

	if( !CheckCmd() ) {

		SendNAK(0x06);

		return;
		}
	}

BOOL CMurMel4SlaveDriver::CheckDrop(void)
{
	if( m_Rx[0] != m_pHex[m_Drop / 16] ) {

		return FALSE;
		}
	
	if( m_Rx[1] != m_pHex[m_Drop % 16] ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CMurMel4SlaveDriver::CheckPC(void)
{
	if( m_Rx[2] != m_pHex[m_PC / 16] ) {

		return FALSE;
		}

	if( m_Rx[3] != m_pHex[m_PC % 16] ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CMurMel4SlaveDriver::CheckCmd(void)
{
	if( m_Rx[4] != 'W' ) {

		return FALSE;
		}

	switch( m_Rx[5] ) {

		case 'R':	return DoRead();

		case 'W':	return DoWrite();
		}

	return FALSE;
	}

BOOL CMurMel4SlaveDriver::DoRead(void)
{
	CAddress Addr;

	Addr.a.m_Table	= m_Rx[7];

	Addr.a.m_Type   = addrWordAsWord;

	Addr.a.m_Offset	= GetData(PTXT(m_Rx + 8), 4, FALSE);

	Addr.a.m_Extra	= 0;

	UINT uCount     = GetData(PTXT(m_Rx + 12), 2, TRUE);

	PDWORD pData	= new DWORD [uCount];

	if( COMMS_SUCCESS(Read(Addr, pData, uCount)) ) {

		Start();

		AddByte(STX);

		AddID();

		SetWords(pData, uCount);
		
		AddByte(ETX);

		End();

		delete pData;

		return TRUE;
		}

	delete pData;

	return FALSE;
	}

BOOL CMurMel4SlaveDriver::DoWrite(void)
{
	CAddress Addr;

	Addr.a.m_Table	= m_Rx[7];

	Addr.a.m_Type   = addrWordAsWord;

	Addr.a.m_Offset	= GetData(PTXT(m_Rx + 8), 4, FALSE);

	Addr.a.m_Extra	= 0;

	UINT uCount     = GetData(PTXT(m_Rx + 12), 2, TRUE);

	PDWORD pData	= new DWORD [uCount];

	GetWords(pData, uCount);

	if( COMMS_SUCCESS(Write(Addr, pData, uCount)) ) {

		SendACK();

		delete pData;

		return TRUE;
		}

	SendNAK(0x06);

	delete pData;

	return FALSE;
	}

UINT CMurMel4SlaveDriver::GetWords(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = GetData(PCSTR(m_Rx + 14 + 4 * u), 4, TRUE);
		}

	return uCount;
	}

WORD CMurMel4SlaveDriver::GetData(PCTXT pText, UINT uCount, BOOL fHex)
{
	WORD wData = 0;

	WORD wMult = fHex ? 0x10 : 10;

	while( uCount-- )  {

		char cData = *(pText++);

		if( cData >= '0' && cData <= '9' ) {

			wData = wMult * wData + cData - '0';
			}

		else if ( cData >= 'A' && cData <= 'F' ) {

			wData = wMult * wData + cData - 'A' + 10;
			}

		else if ( cData >= 'a' && cData <= 'f' ) {

			wData = wMult * wData + cData - 'a' + 10;
			}

		else break;
		}

	return wData;
	}

void CMurMel4SlaveDriver::SetWords(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddHex(pData[u], 0x1000);
		}
	}

void CMurMel4SlaveDriver::AddHex(UINT uValue, UINT uMask)
{
	for( ; uMask; uMask /= 16 ) {

		AddByte(m_pHex[(uValue / uMask) % 16]);
		}
	}

void CMurMel4SlaveDriver::AddByte(BYTE bValue)
{
	m_Tx[m_Ptr++] = bValue; 
	}

void CMurMel4SlaveDriver::Start(void)
{
	memset(m_Tx, 0, sizeof(m_Tx));
	
	m_Ptr = 0;
	}

void CMurMel4SlaveDriver::AddID(void)
{
	AddHex(m_Drop, 0x10);

	AddHex(m_PC, 0x10);
	}

void CMurMel4SlaveDriver::End(void)
{
	AddByte(CR);

	AddByte(LF);

	m_pData->Write(m_Tx, m_Ptr, FOREVER);
	}

void CMurMel4SlaveDriver::SendACK(void)
{
	Start();

	AddByte(ACK);

	AddID();

	End();
  	}

void CMurMel4SlaveDriver::SendNAK(BYTE bError)
{
	Start();

	AddByte(NAK);

	AddID();

	AddHex(bError, 0x10);

	End();
	}

// End of file
