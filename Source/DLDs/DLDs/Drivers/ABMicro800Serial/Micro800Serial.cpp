
#include "intern.hpp"

#include "Micro800Serial.hpp"

// Shared Code

#include "../Df1Shared/df1ms.cpp"

#include "../ABMicro800/IOISegment.cpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Series Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CABMicro800SerialMaster);

// Constructor

CABMicro800SerialMaster::CABMicro800SerialMaster(void)
{
	m_Ident       = DRIVER_ID;
	
	m_fOpen       = FALSE;

	m_pABCtx      = NULL;
	}

// Management

void CABMicro800SerialMaster::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void CABMicro800SerialMaster::Detach(void)
{
	}

// Configuration

void CABMicro800SerialMaster::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bSrc  = GetByte(pData);

 		m_fHalf = GetByte(pData);

		m_fCRC  = GetByte(pData);
		}
	}

// Device

CCODE CABMicro800SerialMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pABCtx = (CMicro800Ctx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {
			
			m_pABCtx = new CMicro800Ctx;

			m_pBase  = m_pABCtx;

			m_pCtx   = m_pABCtx;

			GetTags(pData);

			BYTE bStation = GetByte(pData);

			WORD wTimeout = GetWord(pData);

			m_pABCtx->m_uHeader = headBase;

			m_pBase->m_bDest    = bStation;

			m_pABCtx->m_uTime1  = wTimeout;

			m_pABCtx->m_uTime2  = wTimeout;

			m_pCtx->m_wTrans    = WORD(RAND(m_pCtx->m_bDevice + 1));
			
			pDevice->SetContext(m_pABCtx);
			
			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase  = m_pABCtx;

	m_pCtx   = m_pABCtx;

	return CCODE_SUCCESS;
	}

CCODE CABMicro800SerialMaster::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		CleanupTagNames();

		delete m_pABCtx;

		m_pABCtx = NULL;

		m_pCtx   = NULL;

		m_pBase  = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

void  CABMicro800SerialMaster::Service(void)
{
	}

CCODE CABMicro800SerialMaster::Read (AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		UINT uRead     = uCount;

		CTagDesc *pTag = FindTagDesc(Addr);

		if( ReadData(pTag, Addr, uRead) ) {

			PBYTE pReply = m_bRxBuff + 6 + sizeof(DF1HEADBASE);

			BYTE   bType = pReply[0];
			
			PBYTE p      = NULL;

			if( IsSTR(GetCIPCode(pTag->m_CipType)) ) {

				p = pReply + 2;
				}
			else {
				BYTE   bMore = pReply[1];

				PBYTE pPlace = &pReply[2];

				p = &pPlace[bMore];
				}
			
			if( IsBYTE(bType) || IsBOOL(bType) ) {

				CopyByte(p, pData, uRead);
				}
			
			else if( IsWORD(bType) ) {

				CopyWord(p, pData, uRead);
				}

			else if( IsLONG(bType) || IsREAL(bType) || IsTIME(bType) ) {

				if( pTag->m_CipType == typeDATE ) {
					
					CopyDate(p, pData, uRead);
					}
				else {  
					CopyLong(p, pData, uRead);
					}
				}

			else if( IsDouble(bType) ) {

				CopyDouble(p, pData, uRead);
				}

			else if( IsSTR(bType) ) {

				CopyString(p, pData, uRead, pTag->m_Chars);
				}

			else if( IsDATE(bType) ) {

				CopyDate(p, pData, uRead);
				}
			else {	
				return CCODE_ERROR | CCODE_NO_DATA;
				}
			
			return uRead;
			}

		if( m_bError == 0x04 ) {

			return CCODE_ERROR | CCODE_NO_DATA;
			}

		return pTag ? CCODE_ERROR : CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_ERROR;
	}

CCODE CABMicro800SerialMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		UINT uWrite    = uCount;

		CTagDesc *pTag = FindTagDesc(Addr);

		if( IsSTR(GetCIPCode(pTag->m_CipType)) ) {

			return WriteString(pTag, Addr, pData, uWrite);
			}

		else if( WriteData(pTag, Addr, pData, uWrite) ) {
				
			return uWrite;
			}

		return pTag ? CCODE_ERROR : CCODE_ERROR | CCODE_HARD;
		}
	
	return CCODE_ERROR;
	}

// Implementation

BOOL CABMicro800SerialMaster::ReadData(CTagDesc * pTag, AREF Addr, UINT &uCount)
{
	if( pTag ) {

		UINT uMax = min(GetMaxCount(pTag), GetSize(pTag));

		MakeMin(uCount, uMax);

		if( IsSTR(GetCIPCode(pTag->m_CipType)) ) {

			uCount -= uCount % pTag->m_Chars;
			}

		CIOISegment Name(pTag->m_pName);

		if( IsArray(pTag) ) { 

			AddOffset(Name, pTag, Addr);
			}
	
		NewFrame(0x0B, 0x00);

		AddByte(0x00);

		AddCIPPath(CIP_READ, Name);

		AddCount(GetCount(pTag, uCount, Addr));

		if( Transact() ) {

			DF1HEADBASE &Head = (DF1HEADBASE &)m_bRxBuff;

			return Head.bData[4] == 0;
			}
		}

	return FALSE;
	}

BOOL CABMicro800SerialMaster::ReadTag(CTagDesc &Tag, AREF Addr, UINT &uCount)
{
	CIOISegment Name(Tag.m_pName);
	
	if( IsArray(&Tag) ) {
		
		AddOffset(Name, &Tag, Addr);
		}

	NewFrame(0x0B, 0x00);

	AddByte(0x00);

	AddCIPPath(CIP_READ, Name);
		
	AddCIPType(Tag.m_CipType);

	AddCount(GetCount(&Tag, uCount, Addr));

	return Transact();
	}

BOOL CABMicro800SerialMaster::WriteData(CTagDesc * pTag, AREF Addr, PDWORD pData, UINT &uCount)
{
	if( pTag ) {

		UINT uMax = min(GetMaxCount(pTag), GetSize(pTag));

		MakeMin(uCount, uMax);
		
		CIOISegment Name(pTag->m_pName);

		if( IsArray(pTag) ) {

			AddOffset(Name, pTag, Addr);
			}

		NewFrame(0x0B, 0x00);

		AddByte(0x00);

		AddCIPPath(CIP_WRITE, Name);

		AddCIPType(pTag->m_CipType);

		AddCount(GetCount(pTag, uCount, Addr));

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:
				AddBits(pData, uCount);
				break;

			case addrByteAsByte:
				AddByte(pData, uCount);
				break;

			case addrWordAsWord:
				AddWord(pData, uCount);
				break;

			case addrLongAsLong:
			case addrRealAsReal:

				if( pTag->m_CipType == typeDATE ) {

					AddDate(pData, uCount);
					}

				else if( IsDouble(GetCIPCode(pTag->m_CipType)) ) {

					AddDouble(pData, uCount);
					}
				else {
					AddLong(pData, uCount);
					}
				break;

			default:
				return CCODE_ERROR | CCODE_HARD;
			}
		
		if( Transact() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CABMicro800SerialMaster::WriteString(CTagDesc * pTag, AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uStrings = GetStrCount(pTag, uCount);

	if( uStrings < NOTHING ) {

		UINT uLength = uStrings * pTag->m_Chars;

		UINT uOffset = (Addr.m_Ref - pTag->m_uIndex) % pTag->m_Chars;

		UINT uChars  = uOffset + uCount;

		PDWORD pWork = new DWORD[uLength];

		for( UINT u = 0; u < uLength; u++ ) {

			pWork[u] = 0;
			}

		CAddress Read;

		Read.m_Ref = Addr.m_Ref - uOffset;

		if( !ReadData(pTag, Read, uLength) ) {

			return CCODE_ERROR;
			}

		CopyChar(m_bRxBuff + 9 + sizeof(DF1HEADBASE), pWork, (UINT)(m_bRxBuff + 8 + sizeof(DF1HEADBASE))[0]);

		for( UINT n = uOffset; n < uChars; n++ ) {

			pWork[n] = pData[n - uOffset];
			}
					
		CIOISegment Name(pTag->m_pName);

		if( IsArray(pTag) ) {

			AddOffset(Name, pTag, Addr);
			}

		NewFrame(0x0B, 0x00);

		AddByte(0x00);

		AddCIPPath(CIP_WRITE, Name);

		AddCIPType(pTag->m_CipType);

		AddWord(uStrings);

		for( UINT s = 0, c = 0; s < uStrings; s++, c += pTag->m_Chars ) {
			
			AddString(&pWork[c], RemoveTrailing(&pWork[c], 0x20, pTag->m_Chars));
			}

		if( Transact() ) {

			delete [] pWork;

			return uCount;
			}

		delete [] pWork;

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CTagDesc *CABMicro800SerialMaster::FindTagDesc(AREF Addr)
{
	CTagDesc Search;

	Search.m_uIndex = Addr.m_Ref;
	
	UINT uIndex     = GetMapIndex(Addr);
		        
	UINT uCount     = GetMapCount(Addr, uIndex);
		
	return (CTagDesc *) bsearch(&Search, &m_pABCtx->m_pTags[uIndex], uCount, sizeof(CTagDesc), CompareFunc);
}

void CABMicro800SerialMaster::GetTags(PCBYTE &pData)
{
	UINT uCount        = GetLong(pData);

	m_pABCtx->m_pTags  = new CTagDesc[uCount];

	m_pABCtx->m_uCount = uCount;

	for( UINT n = 0; n < uCount; n++ ) {

		m_pABCtx->m_pTags[n].m_uIndex  = GetLong(pData);

		m_pABCtx->m_pTags[n].m_Dims[0] = GetLong(pData);

		m_pABCtx->m_pTags[n].m_Dims[1] = GetLong(pData);

		m_pABCtx->m_pTags[n].m_Dims[2] = GetLong(pData);

		m_pABCtx->m_pTags[n].m_pName   = GetString(pData);

		m_pABCtx->m_pTags[n].m_CipType = GetWord(pData);

		m_pABCtx->m_pTags[n].m_Chars   = GetByte(pData);
		}

	qsort(m_pABCtx->m_pTags, m_pABCtx->m_uCount, sizeof(CTagDesc), SortFunc);

	PrintTags();

	MakeMap();
	}

PTXT CABMicro800SerialMaster::GetString(PCBYTE &pData)
{
	WORD wCount = GetWord(pData);

	PTXT p = PTXT(Alloc(wCount + 1));

	memset(p, 0, wCount + 1);

	for( UINT n = 0; n < wCount; n++ ) {

		p[n] = GetByte(pData);
		}

	return p;
	}

void CABMicro800SerialMaster::CleanupTagNames(void)
{
	for( UINT n = 0; n < m_pABCtx->m_uCount; n++ ) {

		Free(m_pABCtx->m_pTags[n].m_pName);
		}

	delete [] m_pABCtx->m_pTags;
	}

BOOL CABMicro800SerialMaster::IsArray(CTagDesc *pTag)
{
	return pTag->m_Dims[0] < NOTHING;
	}

void CABMicro800SerialMaster::AddOffset(CIOISegment &Name, CTagDesc *pTag, AREF Addr)
{
	UINT uDiff = Addr.m_Ref - pTag->m_uIndex;

	UINT uDims = 0;

	for( UINT n = dimsX; n <= dimsZ; n++ ) {

		if( pTag->m_Dims[n] != NOTHING ) {

			uDims++;
			}
		}

	if( pTag->m_Chars ) {

		uDiff /= pTag->m_Chars;
		}

	if( IsDouble(GetCIPCode(pTag->m_CipType)) ) {

		uDiff   /=  2;
		}

	for( UINT d = uDims; d > 0; d-- ) {

		UINT uDim = d == 3 ? pTag->m_Dims[uDims-2] * pTag->m_Dims[uDims-1] : pTag->m_Dims[uDims-1];

		if( d == 3 ) {

			Name.Append(uDiff / uDim);

			uDiff %= uDim;
			}

		else if( d == 2 ) {

			Name.Append(uDiff / uDim);
			}

		else if( d == 1 ) {
			
			Name.Append(uDiff % uDim);
			}
		}
	}

int CABMicro800SerialMaster::SortFunc(void const *p1, void const *p2)
{
	CTagDesc *pT1 = (CTagDesc *) p1;

	CTagDesc *pT2 = (CTagDesc *) p2;
	
	BYTE t1 = AREF(pT1->m_uIndex).a.m_Table;

	BYTE t2 = AREF(pT2->m_uIndex).a.m_Table;

	if( t1 > t2 ) {

		return +1;
		}

	if( t1 < t2 ) {

		return -1;
		}

	WORD o1 = AREF(pT1->m_uIndex).a.m_Offset;

	WORD o2 = AREF(pT2->m_uIndex).a.m_Offset;

	if( o1 > o2 ) {

		return +1;
		}

	if( o1 < o2 ) {

		return -1;
		}

	return 0;
	}

int CABMicro800SerialMaster::CompareFunc(void const *p1, void const *p2)
{
	CTagDesc *pT1 = (CTagDesc *) p1;

	CTagDesc *pT2 = (CTagDesc *) p2;
	
	BYTE t1 = AREF(pT1->m_uIndex).a.m_Table;

	BYTE t2 = AREF(pT2->m_uIndex).a.m_Table;

	if( t1 > t2 ) {

		return +1;
		}

	if( t1 < t2 ) {

		return -1;
		}

	WORD o1 = AREF(pT1->m_uIndex).a.m_Offset;

	WORD o2 = AREF(pT2->m_uIndex).a.m_Offset;

	if( o2 <= o1 ) {

		UINT uSize = 1;

		for( UINT n = dimsX; n <= dimsZ; n++ ) {

			if( pT2->m_Dims[n] != NOTHING ) {

				uSize *= pT2->m_Dims[n];
				}
			}

		if( pT2->m_Chars ) {

			uSize *= pT2->m_Chars;
			}

		if( o2 + uSize > o1 ) {

			return 0;
			}
		}

	return o1 - o2;
	}

void CABMicro800SerialMaster::AddData(PBYTE pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		AddByte(pData[n]);
		}
	}

void CABMicro800SerialMaster::AddDate(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD dwData = pData[n] + UTC_DIFF;

		AddWord(LOWORD(dwData));

		AddWord(HIWORD(dwData));
		}
	}

void CABMicro800SerialMaster::AddString(PDWORD pData, UINT uCount)
{
	AddByte(uCount);

	for( UINT n = 0; n < uCount; n++ ) {

		AddByte(BYTE(pData[n]));
		}
	}

void CABMicro800SerialMaster::AddDouble(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n+=2 ) {

		AddLong(&pData[n + 1], 1);

		AddLong(&pData[n + 0], 1);
		}
	}

void CABMicro800SerialMaster::CopyDouble(PBYTE pReply, PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n+=2 ) {

		CopyLong(pReply, &pData[n + 1], 1);

		pReply += 4;

		CopyLong(pReply, &pData[n + 0], 1);

		pReply += 4;
		}
	}

void CABMicro800SerialMaster::CopyDate(PBYTE pReply, PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD dwData = PU4(pReply)[n];

		pData[n]  = LONG(IntelToHost(dwData));
		
		pData[n] -= UTC_DIFF;
		}
	}

void CABMicro800SerialMaster::CopyChar(PBYTE pReply, PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = IsStringText(pReply[u]) ? pReply[u] : 0x20;
		}
	}

UINT CABMicro800SerialMaster::CopyString(PBYTE pReply, PDWORD &pData, UINT uCount, UINT uChars)
{	
	UINT Count = uCount / uChars;

	UINT uStr  = 0;

	UINT uLen  = 0;

	for( UINT n = 0; uStr < Count; uStr++, n += (uLen+1) ) {

		uLen    = PBYTE(pReply)[n];

		PBYTE p = pReply + n + 1;

		MakeMin(uLen, uChars);

		CopyChar(p, &pData[uStr * uChars], uLen);
		}

	return uStr * uChars;
	}

// Map Support

void CABMicro800SerialMaster::MakeMap(void)
{
	memset(&m_pABCtx->m_Map, 0xFF, elements(m_pABCtx->m_Map) * sizeof(DWORD));

	for( UINT m = 0; m < m_pABCtx->m_uCount; m++ ) {

		CAddress Addr;

		Addr.m_Ref = m_pABCtx->m_pTags[m].m_uIndex;

		if( Addr.a.m_Table ) {
			
			if( m_pABCtx->m_Map[Addr.a.m_Table - 1] == NOTHING ) {

				m_pABCtx->m_Map[Addr.a.m_Table - 1] = m;
				}
			}
		}

	PrintMap();
	}

UINT CABMicro800SerialMaster::GetMapIndex(AREF Addr)
{
	return m_pABCtx->m_Map[Addr.a.m_Table - 1];
	}

UINT CABMicro800SerialMaster::GetMapCount(AREF Addr, UINT uIndex)
{
	UINT uMaps = elements(m_pABCtx->m_Map);

	for( UINT u = Addr.a.m_Table; u < uMaps; u++ ) {

		if( m_pABCtx->m_Map[u] < NOTHING ) {

			return m_pABCtx->m_Map[u] - uIndex;
			}
		}

	return m_pABCtx->m_uCount - uIndex;
	}

// CIP Support

BOOL CABMicro800SerialMaster::AddCIPType(UINT uType)
{
	UINT uCode = GetCIPCode(uType);

	if( uCode < NOTHING ) {

		AddWord(uCode);

		return TRUE;
		}

	return FALSE;
	}

UINT CABMicro800SerialMaster::GetCIPCode(UINT uType)
{
	switch( uType ) {

		case typeBOOL :		return codeBOOL ; 
		case typeUSINT:		return codeUSINT;
		case typeINT  :		return codeINT  ;
		case typeDINT :		return codeDINT ;
		case typeREAL :		return codeREAL ;
		case typeSTR  :		return codeSTR  ;
		case typeSINT :		return codeSINT ; 
		case typeBYTE :		return codeBYTE ;
		case typeUINT :		return codeUINT ; 
		case typeWORD :		return codeWORD ;
		case typeUDINT:		return codeUDINT;
		case typeDWORD:		return codeDWORD;
		case typeTIME :		return codeTIME ; 
		case typeDATE :		return codeDATE ;
		case typeLINT :		return codeLINT ;
		case typeULINT:		return codeULINT;
		case typeLWORD:		return codeLWORD;
		case typeLREAL:		return codeLREAL;
		}

	return NOTHING;
	}

UINT CABMicro800SerialMaster::GetCount(CTagDesc * pDesc, UINT &uCount, AREF Addr)
{
	UINT uStr  = 0;

	UINT uCode = GetCIPCode(pDesc->m_CipType);

	if( IsSTR(uCode) ) {

		uStr = uCount / pDesc->m_Chars;

		if( !IsArray(pDesc) ) {

			return uStr;
			}
		}

	if( IsDouble(uCode) ) {

		uCount -= uCount % 2;

		if( !IsArray(pDesc) ) {

			return max(1, uCount / 2 + uCount % 2);
			}
		}

	if( IsArray(pDesc) ) {

		UINT uSize = GetSize(pDesc);

		UINT uDiff = Addr.m_Ref - pDesc->m_uIndex;

		if( uStr ) {
			
			MakeMin(uStr, uSize - uDiff);
			
			return uStr;
			}

		if( IsDouble(uCode) ) {

			uSize /= 2 ;

			uDiff /= 2;
			}

		MakeMin(uCount, uSize - uDiff);

		if( IsDouble(uCode) ) {

			return max(1, uCount / 2 + uCount % 2);
			}
		}

	return uCount;
	}

void CABMicro800SerialMaster::AddCIPPath(BYTE bService, CIOISegment &Name)
{
	AddByte(bService);

	BYTE bWords = !(Name.GetSize() % 2) ? Name.GetSize() / 2 : (Name.GetSize() / 2) + 1;

	AddByte(bWords);

	AddData(Name.GetData(), Name.GetSize());
	}

void CABMicro800SerialMaster::AddCount(UINT uCount)
{
	AddWord(uCount);
	}

void CABMicro800SerialMaster::AddBits(PDWORD pData, UINT uCount)
{
	DWORD dwMask = 1;

	for( UINT n = 0; n < uCount; n ++ ) {

		AddByte(pData[n % 32] & dwMask ? 0xFF : 0x00);
		
		dwMask = dwMask ? dwMask << 1 : 1;
		}
	}

// Helpers

BOOL CABMicro800SerialMaster::IsSTR(UINT uCode)
{
	return uCode == GetCIPCode(typeSTR);
	}

BOOL CABMicro800SerialMaster::IsTIME(UINT uCode)
{
	return uCode == GetCIPCode(typeTIME);
	}

BOOL CABMicro800SerialMaster::IsDATE(UINT uCode)
{
	return uCode == GetCIPCode(typeDATE);
	}

BOOL CABMicro800SerialMaster::IsBOOL(UINT uCode)
{
	return uCode == GetCIPCode(typeBOOL);
	}

BOOL CABMicro800SerialMaster::IsBYTE(UINT uCode)
{
	return uCode == GetCIPCode(typeBYTE) ||
	       uCode == GetCIPCode(typeSINT) ||
	       uCode == GetCIPCode(typeUSINT);
	}

BOOL CABMicro800SerialMaster::IsWORD(UINT uCode)
{
	return uCode == GetCIPCode(typeINT)  ||
	       uCode == GetCIPCode(typeUINT) ||
	       uCode == GetCIPCode(typeWORD);
	}

BOOL CABMicro800SerialMaster::IsLONG(UINT uCode)
{
	return uCode == GetCIPCode(typeDINT)  ||
	       uCode == GetCIPCode(typeUDINT) ||
	       uCode == GetCIPCode(typeDWORD);
	}

BOOL CABMicro800SerialMaster::IsREAL(UINT uCode)
{
	return uCode == GetCIPCode(typeREAL);
	}

BOOL CABMicro800SerialMaster::IsDouble(UINT uCode)
{
	return uCode == GetCIPCode(typeLINT)  ||
	       uCode == GetCIPCode(typeULINT) ||
	       uCode == GetCIPCode(typeLWORD) ||
	       uCode == GetCIPCode(typeLREAL);
	}

BOOL CABMicro800SerialMaster::IsStringText(UINT uChar)
{
	return (isalpha(uChar) || isdigit(uChar) || ispunct(uChar) || isspace(uChar));
	}

UINT CABMicro800SerialMaster::GetStrCount(CTagDesc * pDesc, UINT &uCount)
{
	if( pDesc ) {

		UINT uMax = min(252, GetSize(pDesc));

		MakeMin(uCount, uMax);

		UINT uStrings = uCount / pDesc->m_Chars;

		if( uCount % pDesc->m_Chars) {

			uStrings++;
			}

		MakeMin(uCount, uStrings * pDesc->m_Chars);

		return uStrings;
		}

	return NOTHING;
	}

UINT CABMicro800SerialMaster::GetCharCount(PDWORD pData, UINT uOffset)
{
	UINT uChars = 0;

	if( pData ) {

		while( IsStringText(BYTE(pData[uOffset + uChars])) ) {

			uChars++;
			}
		}

	return uChars;
	}

UINT CABMicro800SerialMaster::GetSize(CTagDesc * pDesc)
{
	UINT uSize = 0;

	if( pDesc ) {

		uSize++;

		UINT uCode = GetCIPCode(pDesc->m_CipType);

		if( IsDouble(uCode) ) {

			uSize++;
			}

		else if( IsSTR(uCode) ) {

			uSize = pDesc->m_Chars;
			}

		if( IsArray(pDesc) ) {

			for( UINT n = dimsX; n <= dimsZ; n++ ) {

				if( pDesc->m_Dims[n] != NOTHING ) {

					uSize *= pDesc->m_Dims[n];
					}
				}
			}
		}

	return uSize;
	}

UINT CABMicro800SerialMaster::GetMaxCount(CTagDesc * pDesc)
{
	UINT uCount = 252;

	UINT uCode  = GetCIPCode(pDesc->m_CipType);

	if( IsWORD(uCode) ) {

		uCount /= sizeof(WORD);
		}

	else if( IsLONG(uCode) || IsREAL(uCode) || IsTIME(uCode) ) {

		uCount /= sizeof(DWORD);
		}

	else if( IsDouble(uCode) ) {

		uCount /= sizeof(DWORD);
		}

	return uCount;
	}

UINT CABMicro800SerialMaster::RemoveTrailing(PDWORD pData,BYTE bByte, UINT uBytes)
{
	UINT uChars = pData ? uBytes - 1 : NOTHING;

	while( uChars ) {

		if( pData[uChars] == 0x20 ) {

			pData[uChars] = 0;

			uChars--;

			continue;
			}

		break;
		}

	return uChars + 1;
	}

void CABMicro800SerialMaster::PrintTags(void)
{
/*	AfxTrace("\nTag count %u", m_pABCtx->m_uCount);

	for( UINT n = 0; n < m_pABCtx->m_uCount; n++ ) {

		CTagDesc Tag = m_pABCtx->m_pTags[n];

		UINT uSize   = GetSize(&Tag);

		AfxTrace("\nTag %8.8x %s type %u chars %u size %u ",	Tag.m_uIndex,
									Tag.m_pName,
									Tag.m_CipType,
									Tag.m_Chars,
									uSize);

		}
*/	}

void CABMicro800SerialMaster::PrintMap(void)
{
/*	AfxTrace("\nMap");

	for( UINT n = 0; n < 255; n++ ) {

		if( m_pABCtx->m_Map[n] < NOTHING ) {

			AfxTrace("\nMap for table %2.2x index %u", n + 1, m_pABCtx->m_Map[n]);
			}
		}
*/	}


// End of File
