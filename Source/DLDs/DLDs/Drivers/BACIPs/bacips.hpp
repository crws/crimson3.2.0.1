
//////////////////////////////////////////////////////////////////////////
//
// Base Class
//

#include "bacslave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Slave
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

class CBACNetIPSlave : public CBACNetSlave
{
	public:
		// Constructor
		CBACNetIPSlave(void);

		// Destructor
		~CBACNetIPSlave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);

	protected:
		// MAC Address
		struct CMacAddr
		{
			IPADDR m_IP;
			WORD   m_Port;
		};

		// Device Context
		struct CContext : CBACNetSlave::CBaseCtx
		{
		};

		// Data Members
		IExtraHelper     * m_pExtra;
		CContext	 * m_pCtx;
		ISocket		 * m_pSock;
		CMacAddr	   m_RxMac;
		CMacAddr	   m_TxMac;

		// Transport Hooks
		BOOL SendFrame(BOOL fThis, BOOL fReply);
		BOOL RecvFrame(void);

		// Transport Header
		void AddTransportHeader(BOOL fThis);
		BOOL StripTransportHeader(void);
};

// End of File
