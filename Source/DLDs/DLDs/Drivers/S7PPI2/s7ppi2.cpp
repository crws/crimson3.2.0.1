
#include "intern.hpp"

#include "s7ppi2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// S7 PPI Driver Revision 2
//

// Instantiator

INSTANTIATE(CS7PPIDriver);

// Constructor

CS7PPIDriver::CS7PPIDriver(void)
{
	m_Ident    = DRIVER_ID;

	m_bSrcAddr = 0;	
	
	m_bLastFC  = 0x6C;

	m_wLastPDU = 0;

	m_fDebug   = FALSE;
	}

// Destructor

CS7PPIDriver::~CS7PPIDriver(void)
{
	}

// Configuration

void MCALL CS7PPIDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CS7PPIDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CS7PPIDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CS7PPIDriver::Open(void)
{
	}

// Device

CCODE MCALL CS7PPIDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx		= new CContext;

			m_pBase		= m_pCtx;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_fFast = TRUE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase		= m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CS7PPIDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pDevice->SetContext(NULL);

		m_pCtx = NULL;
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

BOOL CS7PPIDriver::Transact(void)
{
	for(;;) {

		if( m_fDebug ) {

			AfxTrace("Send: ");

			for( UINT i = 0; i < m_uPtr; i++ ) {

				AfxTrace("%2.2X ", m_bTxBuff[i]);
				}

			AfxTrace("\n");
			}

		Sleep(10);

		m_pData->ClearRx();

		m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

		switch( GetReply() ) {
				
			case RX_NAK:

				if( m_bNak == 0x02 ) {
				
					if( Poll(TRUE) ) {

						break;
						}
					}
					
				return FALSE;

			case RX_SC:

				if( Poll(FALSE) ) {
					
					if( CheckReply() ) {

						return TRUE;
						}
					}

				return FALSE;

			default:

				return FALSE;
			}
		}

	return FALSE;
	}

BOOL CS7PPIDriver::Poll(BOOL fFast)
{
	UINT uInit = GetTickCount();

	UINT uTest = 0;

	for(;;) {

		UINT uTime = GetTickCount();

		UINT uGone = uTime - uInit;

		if( uGone >= ToTicks(POLL_TIMEOUT) ) {

			break;
			}

		SendPoll();

		switch( GetReply() ) {

			case RX_FRAME:

				return TRUE;

			case RX_SC:

				if( !fFast ) {

					Sleep(POLL_RETRY);

					break;
					}

				return FALSE;

			default:

				return FALSE;
			}
		}
		
	return FALSE;
	}

void CS7PPIDriver::SendPoll(void)
{
	static BYTE bData[6];

	bData[0] = SD1;
		
	bData[1] = m_pCtx->m_bDrop;
		
	bData[2] = m_bSrcAddr;

	bData[3] = GetNextFC();

	bData[4] = bData[1] + bData[2] + bData[3];

	bData[5] = ED;

	if( m_fDebug ) {

		AfxTrace("Poll: ");

		for( UINT i = 0; i < 6; i++ ) {

			AfxTrace("%2.2X ", bData[i]);
			}

		AfxTrace("\n");
		}

	Sleep(10);

	m_pData->Write(bData, 6, FOREVER);
	}

UINT CS7PPIDriver::GetReply(void)
{
	if( m_fDebug ) {

		AfxTrace("Recv: ");
		}

	UINT uRxPtr = 0;
	
	UINT uCount = 0;
	
	BYTE bCheck = 0;

	BYTE bInit  = 0;

	UINT uState = 0;

	SetTimer(START_TIMEOUT);

	for(;;) {

		UINT uData;
	
		if( (uData = m_pData->Read(GetTimer())) == NOTHING ) {

			break;
			}

		if( m_fDebug ) {

			AfxTrace("%2.2X ", uData);
			}

		switch( uState ) {
			
			case 0:
				if( uData == SC ) {

					if( m_fDebug ) {
	
						AfxTrace("=SC\n");
						}

					return RX_SC;
					}
					
				if( uData == SD2 ) {

					SetTimer(FRAME_TIMEOUT);

					uState = 1;

					bInit  = BYTE(uData);
					}

				if( uData == SD1 ) {
				
					SetTimer(FRAME_TIMEOUT);

					uState = 4;
					
					uCount = 3;

					uRxPtr = 0;

					bCheck = 0;

					bInit  = BYTE(uData);
					}
				break;				
			
			case 1:
				uCount = uData;
				
				uState = 2;
				
				break;

			case 2:
				uState = 3;

				break;

			case 3:
				uRxPtr = 0;

				bCheck = 0;

				uState = 4;

				break;

			case 4:
				m_bRxBuff[uRxPtr++] = BYTE(uData);

				bCheck = bCheck     + BYTE(uData);

				if( uRxPtr == uCount ) {

					uState = 5;
					}
				break;

			case 5:
				bCheck = bCheck - BYTE(uData);

				uState = 6;

				break;

			case 6:
				if( uData == ED ) {

					if( !bCheck ) {

						if( bInit == SD2 ) {

							if( m_fDebug ) {

								AfxTrace("=FRAME\n");
								}

							return RX_FRAME;
							}

						m_bNak = m_bRxBuff[2];

						if( m_fDebug ) {

							AfxTrace("=NAK\n");
							}

						return RX_NAK;
						}
					}

				if( m_fDebug ) {

					AfxTrace("=ERROR\n");
					}

				return RX_ERROR;
			}
		}

	if( m_fDebug ) {

		AfxTrace("=NONE\n");
		}

	return RX_NONE;
	}

// End of File
