
#include "s7base2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// S7 PPI Driver Revision 2
//

class CS7PPIDriver : public CS7BaseDriver
{
	public:
		// Constructor
		CS7PPIDriver(void);

		// Destructor
		~CS7PPIDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:
		// Device Data
		struct CContext : CS7BaseDriver::CBaseCtx
		{
			};
		
		// Data Members
		CContext *m_pCtx;
		
		// Transport Layer
		virtual BOOL Transact(void);
		virtual UINT GetReply(void);
		virtual BOOL Poll(BOOL fFast);
		virtual void SendPoll(void);

		};

// End of File
