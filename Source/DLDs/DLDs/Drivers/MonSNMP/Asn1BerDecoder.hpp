
//////////////////////////////////////////////////////////////////////////
//
// SNMP Agent
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved.
//

#ifndef INCLUDE_SNMP_Asn1BerDecoder_HPP

#define INCLUDE_SNMP_Asn1BerDecoder_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Asn1Base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class COid;

//////////////////////////////////////////////////////////////////////////
//
// ASN.1 BER Decoder
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved


class CAsn1BerDecoder : public CAsn1
{
	public:
		// Constructor
		CAsn1BerDecoder(void);
		CAsn1BerDecoder(IHelper * pHelp);

		// Destructor
		~CAsn1BerDecoder(void);

		// Decoding
		bool Decode(PCBYTE pData, UINT uSize);

		// Reading
		bool ReadSequence(void);
		bool ReadConstructed(UINT &uTag);
		bool ReadEnd(void);
		bool ReadInteger(UINT &uData);
		bool ReadOctString(char *pData, UINT uSize);
		bool ReadObjectID(COid &Oid);
		bool ReadVarBindList(COid *pOid, UINT uSize);

	protected:
		// Data Item
		struct CItem
		{
			BYTE	m_bType;
			UINT	m_uTag;
			UINT	m_uLen;
			PCBYTE	m_pData;
			};

		// Data Members
		CItem *	  m_pList;
		UINT	  m_uCount;
		UINT	  m_uRead;
		IHelper * m_pHelper;

		// Implementation
		bool DecodeFrom(PCBYTE &pData, UINT uSize);
		bool CheckList(void);

		// Debug
		void AfxTrace(PCTXT pText, ...);
	};

// End of File

#endif
