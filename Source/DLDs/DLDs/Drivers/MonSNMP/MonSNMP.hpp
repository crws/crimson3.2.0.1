
//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "SnmpAgent.hpp"

#include "Asn1BerDecoder.hpp"

#include "Asn1BerEncoder.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Monico SNMP Driver
//

class CMonicoSNMP : public CSlaveDriver, public ISnmpSource
{
	public:
		// Constructor
		CMonicoSNMP(void);

		// Destructor
		~CMonicoSNMP(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Entry Points
		DEFMETH(void) Service(void);
		
	protected:
		// Configuration
		UINT    m_Port1;
		UINT    m_Port2;
		char    m_sComm[64];
		UINT	m_AclAddr1;
		UINT	m_AclMask1;
		UINT	m_AclAddr2;
		UINT	m_AclMask2;
		UINT	m_TrapFrom;
		UINT	m_TrapAddr1;
		UINT	m_TrapMode1;
		UINT	m_TrapAddr2;
		UINT	m_TrapMode2;		
		BOOL	m_fSysDefault;
		PTXT	m_pSysDesc;
		PTXT	m_pSysContact;
		PTXT	m_pSysName;
		PTXT	m_pSysLocation;

		// Data Members
		ISocket    * m_pSock;
		CSnmpAgent * m_pAgent;
		UINT         m_uTicks;
		UINT	     m_genData;
		UINT	     m_genTrap;
		UINT	     m_genNotify;
		UINT	     m_setData;
		UINT	     m_setTrap;
		UINT	     m_setNotify;
		BYTE	     m_Trap1[197];
		BYTE	     m_Trap2[200];
		UINT	     m_uBase1;
		UINT	     m_uBase2;

		// Implementation
		bool CheckRequest(void);
		bool AllowIP(DWORD IP);
		void CheckTraps(void);
		void CheckTraps(PBYTE pHist, UINT &uBase, UINT uTable, UINT uCount, UINT uNotify, UINT uTrap);
		bool SendTrap(UINT &Mode, UINT &Addr, UINT uTable, UINT uPos, UINT uNotify, UINT uTrap);
		void FindMappedTraps(void);
		void FindMappedTraps(PBYTE pHist, UINT uTable, UINT uCount);

		// Data Source
		bool IsSpace(COid const &Oid, UINT uTag, UINT uPos);
		bool GetData(CAsn1BerEncoder &rep, COid const &Oid, UINT uTag, UINT uPos);
	};

// End of File
