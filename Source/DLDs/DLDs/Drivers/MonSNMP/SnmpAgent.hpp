
//////////////////////////////////////////////////////////////////////////
//
// SNMP Agent
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved.
//

#ifndef INCLUDE_SNMP_SnmpAgent_HPP

#define INCLUDE_SNMP_SnmpAgent_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Oid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CAsn1BerDecoder;
class CAsn1BerEncoder;

//////////////////////////////////////////////////////////////////////////
//
// SMTP Data Source
//

class ISnmpSource
{
	public:
		// Methods
		virtual bool IsSpace( COid const      &Oid,
				      UINT            uTag,
				      UINT            uPos
				      ) = 0;

		virtual bool GetData( CAsn1BerEncoder &rep,
				      COid const      &Oid,
				      UINT            uTag,
				      UINT            uPos
				      ) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// SMNP Agent
//

class CSnmpAgent : public ISnmpSource
{
	public:
		// Source Tags
		enum
		{
			srcZero  = 100,
			srcIndex = 101
			};

		// Constructor
		CSnmpAgent(void);

		// Destructor
		~CSnmpAgent(void);

		// Operations
		void Bind(PCUINT pTicks, COid const &System, PCSTR pComm);
		UINT AddRangedSource(COid const &Oid, ISnmpSource *pSrc, UINT uTag, UINT uCount);
		UINT AddSingleSource(COid const &Oid, ISnmpSource *pSrc, UINT uTag);
		UINT AddNotifySource(COid const &Oid);
		void SetSysDesc(PCTXT pDesc);
		void SetSysContact(PCTXT pContact);
		void SetSysName(PCTXT pName);
		void SetSysLoc(PCTXT pLoc);

		// Processing
		bool OnRequest(CAsn1BerDecoder &req, CAsn1BerEncoder &rep);
		void SetTrapV1(CAsn1BerEncoder &rep, DWORD IP, UINT uType, UINT uCode, UINT uSrc, UINT uPos);
		void SetTrapV1(CAsn1BerEncoder &rep, DWORD IP, UINT uType, UINT uCode);
		void SetTrapV2(CAsn1BerEncoder &rep, UINT uBase, UINT uPos, UINT uSrc);
		void SetTrapV2(CAsn1BerEncoder &rep, UINT uBase, UINT uPos);
		void SetInfoV2(CAsn1BerEncoder &rep, UINT uBase, UINT uPos, UINT uSrc);
		void SetInfoV2(CAsn1BerEncoder &rep, UINT uBase, UINT uPos);

	protected:
		// Data Source
		struct CSource
		{
			COid	      m_Oid;
			UINT	      m_uCount;
			ISnmpSource * m_pSrc;
			UINT          m_uTag;
			};

		// Location
		struct CLoc
		{
			CSource const * m_pSrc;
			UINT            m_uPos;
			COid	        m_Oid;
			};

		// Search Key
		struct CKey
		{
			COid    const * m_pOid;
			CSource const * m_pRow;
			int             m_nVal;
			};

		// OID List
		PCUINT  m_pTicks;
		COid    m_System;
		char    m_sComm[32];
		CSource m_Src[256];
		UINT    m_uCount;
		bool    m_fDirty;
		COid    m_NotifyTime;
		COid    m_NotifyOid;
		UINT	m_uTraps;

		// System Data Configuration
		PCTXT	m_pSysDesc;
		PCTXT	m_pSysContact;
		PCTXT	m_pSysName;
		PCTXT	m_pSysLocation;

		// Implementation
		bool SortSourceList(void);
		bool FindItem(CLoc &Loc, COid const &Oid);
		bool FindNext(CLoc &Loc, COid const &Oid);
		bool ProcessGetData(CAsn1BerDecoder &req, CAsn1BerEncoder &rep, UINT uVer, bool fNext);
		bool ProcessGetBulk(CAsn1BerDecoder &req, CAsn1BerEncoder &rep, UINT uVer);
		bool ProcessSetData(CAsn1BerDecoder &req, CAsn1BerEncoder &rep, UINT uVer);
		void AddNotifyTime(CAsn1BerEncoder &rep);
		void AddNotifyCode(CAsn1BerEncoder &rep, UINT uSrc, UINT uPos);
		void AddTrapData(CAsn1BerEncoder &rep, UINT uSrc, UINT uPos);

		// System Source
		bool IsSpace(COid const &Oid, UINT uTag, UINT uPos);
		bool GetData(CAsn1BerEncoder &rep, COid const &Oid, UINT uTag, UINT uPos);
		bool GetDataForSystem(CAsn1BerEncoder &rep, UINT uPos);
		bool GetDataForSNMP(CAsn1BerEncoder &rep, UINT uPos);

		// Sort Function
		static int SortFunc(void const *p1, void const *p2);
		static int FindFunc(void const *p1, void const *p2);
	};

// End of File

#endif
