
#include "intern.hpp"

#include "monsnmp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Monico SNMP Driver
//

// Instantiator

INSTANTIATE(CMonicoSNMP);

// Helper
/*
UINT GetTickCount(void)
{
	return 0;
	}*/

// Constructor

CMonicoSNMP::CMonicoSNMP(void)
{
	m_Ident		= DRIVER_ID;

	m_fSysDefault	= FALSE;

	m_pSysDesc	= NULL;

	m_pSysContact	= NULL;

	m_pSysName	= NULL;

	m_pSysLocation	= NULL;
	}

// Destructor

CMonicoSNMP::~CMonicoSNMP(void)
{
	Free(m_pSysDesc);	

	Free(m_pSysContact);

	Free(m_pSysName);

	Free(m_pSysLocation);
	}

// Configuration

void MCALL CMonicoSNMP::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_Port1 = GetWord(pData);
		m_Port2 = GetWord(pData);

		strcpy(m_sComm, GetString(pData));

		m_AclMask1  = GetAddr(pData);
		m_AclAddr1  = GetAddr(pData);
		m_AclMask2  = GetAddr(pData);
		m_AclAddr2  = GetAddr(pData);
		m_TrapFrom  = GetAddr(pData);
		m_TrapAddr1 = GetAddr(pData);
		m_TrapMode1 = GetWord(pData);
		m_TrapAddr2 = GetAddr(pData);
		m_TrapMode2 = GetWord(pData);
		m_fSysDefault = GetByte(pData);

		if( !m_fSysDefault ) {

			m_pSysDesc	= GetString(pData);
			m_pSysContact	= GetString(pData);
			m_pSysName	= GetString(pData);
			m_pSysLocation	= GetString(pData);
			}
		}
	}

// Management

void MCALL CMonicoSNMP::Attach(IPortObject *pPort)
{
	m_pAgent = New CSnmpAgent;

	if( !m_fSysDefault ) {

		m_pAgent->SetSysDesc(m_pSysDesc);

		m_pAgent->SetSysContact(m_pSysContact);

		m_pAgent->SetSysName(m_pSysName);

		m_pAgent->SetSysLoc(m_pSysLocation);
		}

	m_pAgent->Bind(&m_uTicks, "1.3.6.1.4.1.36282.1.1", m_sComm);

	m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.1.1.1.1.1.1.0.0", m_pAgent, m_pAgent->srcIndex, 100);

	m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.1.1.1.2.1.1.0.0", m_pAgent, m_pAgent->srcIndex, 100);

	m_genData   = m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.1.1.1.1.1.2.0.0", this, 3, 200);

	m_genTrap   = m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.1.1.1.2.1.2.0.0", this, 4, 200);

	m_genNotify = m_pAgent->AddNotifySource("1.3.6.1.4.1.36282.1.1.1.3");

	m_setData   = m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.1.1.2.1.0.0", this, 1, 193);

	m_setTrap   = m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.1.1.2.2.0.0", this, 2, 167);

	m_setNotify = m_pAgent->AddNotifySource("1.3.6.1.4.1.36282.1.1.2.3");

	m_pSock     = CreateSocket(IP_UDP);

	m_uTicks    = GetTickCount() / 2;

	m_uBase1    = 0;

	m_uBase2    = 0;

	FindMappedTraps();
	}

void MCALL CMonicoSNMP::Detach(void)
{
	m_pSock->Release();

	delete m_pAgent;
	}

void MCALL CMonicoSNMP::Open(void)
{
	if( m_pSock ) {
		
		m_pSock->SetOption(OPT_RECV_QUEUE, 8);

		m_pSock->SetOption(OPT_ADDRESS,    2);

		m_pSock->Listen   (m_Port1);
		}
	}

// Entry Point

void MCALL CMonicoSNMP::Service(void)
{
	UINT uLast = GetTickCount();

	UINT uTime = 0;

	for(;;) {

		uTime    = GetTickCount();

		m_uTicks = uTime / 2;

		if( uTime - uLast > ToTicks(250) ) {

			if( m_TrapMode1 || m_TrapMode2 ) {

				CheckTraps();
				}

			uLast = GetTickCount();
			}

		for( UINT n = 0; n < 4; n++ ) {

			if( !CheckRequest() ) {

				break;
				}

			ForceSleep(10);
			}

		ForceSleep(50);
		}
	}

// Implementation

bool CMonicoSNMP::CheckRequest(void)
{
	CBuffer *pBuff = NULL;

	if( m_pSock->Recv(pBuff) == S_OK ) {

		BYTE bHead[12];

		memcpy( bHead,
			pBuff->StripHead(12),
			12
			);

		DWORD IP = MAKELONG(MAKEWORD(bHead[3],bHead[2]),MAKEWORD(bHead[1],bHead[0]));

		if( AllowIP(IP) ) {

			CAsn1BerDecoder req;

			PCBYTE pRecv = pBuff->GetData();

			UINT   uRecv = pBuff->GetSize();
			
			if( req.Decode(pRecv, uRecv) ) {

				CAsn1BerEncoder rep;

				if( m_pAgent->OnRequest(req, rep) ) {

					PCBYTE pSend = rep.GetData();

					UINT   uSend = rep.GetSize();

					pBuff->Release();

					if( (pBuff = CreateBuffer(1280, TRUE)) ) {
						
						memcpy( pBuff->AddTail(6),
							bHead,
							6
							);

						memcpy( pBuff->AddTail(uSend),
							pSend,
							uSend
							);

						SetTimer(500);

						while( GetTimer() ) {

							if( m_pSock->Send(pBuff) == S_OK ) {

								return true;
								}

							Sleep(20);
							}

						pBuff->Release();
						}

					return false;
					}
				}
			}

		pBuff->Release();
		}

	return false;
	}

bool CMonicoSNMP::AllowIP(DWORD IP)
{
	if( m_AclAddr1 && (IP & m_AclMask1) - (m_AclAddr1 & m_AclMask1) ) {

		return false;
		}

	if( m_AclAddr2 && (IP & m_AclMask2) - (m_AclAddr2 & m_AclMask2) ) {

		return false;
		}

	return true;
	}

void CMonicoSNMP::CheckTraps(void)
{
	CheckTraps(m_Trap1, m_uBase1, 2, 167, m_setNotify, m_setTrap);

	CheckTraps(m_Trap2, m_uBase2, 4, 200, m_genNotify, m_genTrap);
	}

void CMonicoSNMP::CheckTraps(PBYTE pHist, UINT &uBase, UINT uTable, UINT uCount, UINT uNotify, UINT uTrap)
{
	UINT uSend = 0;

	UINT uScan = 0;

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD Data;

		CAddress Addr;

		Addr.a.m_Type   = addrLongAsLong;

		Addr.a.m_Table  = uTable;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = 1 + uBase;

		CCODE  cCode = Read(Addr, &Data, 1);

		if( COMMS_SUCCESS(cCode) ) {

			BYTE bData = Data ? 1 : 0;

			if( pHist[uBase] != bData ) {

				UINT uPos = 1 + uBase;

				if( !SendTrap(m_TrapMode1, m_TrapAddr1, uTable, uPos, uNotify, uTrap) ) {

					break;
					}

				if( !SendTrap(m_TrapMode2, m_TrapAddr2, uTable, uPos, uNotify, uTrap) ) {

					break;
					}

				pHist[uBase] = bData;

				uSend++;
				}

			uScan++;
			}

		if( true ) {

			uBase = (uBase + 1) % uCount;
			}

		if( uScan == 25 || uSend == 8 ) {

			break;
			}
		}
	}

bool CMonicoSNMP::SendTrap(UINT &Mode, UINT &Addr, UINT uTable, UINT uPos, UINT uNotify, UINT uTrap)
{
	if( Mode ) {

		CAsn1BerEncoder rep;

		if( Mode == 1 ) {
			
			UINT uEnt = uPos + (uTable - 2) * 500;

			m_pAgent->SetTrapV1(rep, m_TrapFrom, 6, uEnt, uTrap, uPos);
			}

		if( Mode == 2 ) {

			m_pAgent->SetTrapV2(rep, m_setNotify, uPos, m_setTrap);
			}

		if( Mode == 3 ) {

			m_pAgent->SetInfoV2(rep, m_setNotify, uPos, m_setTrap);
			}

		PCBYTE   pSend = rep.GetData();

		UINT     uSend = rep.GetSize();

		CBuffer *pBuff = CreateBuffer(1280, TRUE);

		if( pBuff ) {

			BYTE bHead[6];

			bHead[0] = HIBYTE(HIWORD(Addr));
			bHead[1] = LOBYTE(HIWORD(Addr));
			bHead[2] = HIBYTE(LOWORD(Addr));
			bHead[3] = LOBYTE(LOWORD(Addr));
			
			bHead[4] = HIBYTE(m_Port2);
			bHead[5] = LOBYTE(m_Port2);
		
			memcpy( pBuff->AddTail(6),
				bHead,
				6
				);

			memcpy( pBuff->AddTail(uSend),
				pSend,
				uSend
				);

			SetTimer(500);

			while( GetTimer() ) {

				if( m_pSock->Send(pBuff) == S_OK ) {

					return true;
					}

				Sleep(10);
				}

			pBuff->Release();
			}

		return false;
		}

	return true;
	}

void CMonicoSNMP::FindMappedTraps(void)
{
	FindMappedTraps(m_Trap1, 2, 167);

	FindMappedTraps(m_Trap2, 4, 200);
	}

void CMonicoSNMP::FindMappedTraps(PBYTE pHist, UINT uTable, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		CAddress Addr;

		Addr.a.m_Type   = addrLongAsLong;

		Addr.a.m_Table  = uTable;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = 1 + n;

		DWORD Data  = 0;

		CCODE cCode = Read(Addr, &Data, 1);

		if( COMMS_SUCCESS(cCode) ) {

			pHist[n] = 0x55;

			continue;
			}

		pHist[n] = 0;
		}
	}

// Data Source

bool CMonicoSNMP::IsSpace(COid const &Oid, UINT uTag, UINT uPos)
{
	return FALSE;
	}

bool CMonicoSNMP::GetData(CAsn1BerEncoder &rep, COid const &Oid, UINT uTag, UINT uPos)
{
	CAddress Addr;

	Addr.a.m_Type   = addrLongAsLong;

	Addr.a.m_Table  = uTag;

	Addr.a.m_Extra  = 0;

	Addr.a.m_Offset = uPos;

	DWORD Data;

	if( COMMS_SUCCESS(Read(Addr, &Data, 1)) ) {

		if( uTag == 1 ) {

			switch( uPos ) {

				case 3:
				case 11:
				case 19:
				case 29:
				case 49:
				case 50:
				case 51:
				case 52:
				case 53:
				case 54:
				case 57:
				case 59:
				case 60:
				case 66:
				case 67:
				case 68:
				case 69:
				case 76:
				case 77:
				case 78:
				case 79:
				case 80:
				case 81:
				case 82:
				case 83:
				case 84:
				case 85:
				case 86:
				case 87:
				case 88:
				case 89:
				case 90:
				case 91:

					rep.AddFloat(Data);
					
					return true;
				}
			}
				
		rep.AddInteger(Data);

		return true;
		}

	rep.AddNull();

	return true;
	}

// End of File
