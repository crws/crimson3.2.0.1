
#include "Intern.hpp"

#include "SnmpAgent.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SNMP Agent
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved.
//

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Asn1BerDecoder.hpp"

#include "Asn1BerEncoder.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SMTP Agent
//

// Constructor

CSnmpAgent::CSnmpAgent(void)
{
	m_uCount = 0;

	m_uTraps = 0;
	
	AddRangedSource("1.3.6.1.2.1.1.0.0",  this,  1,  7);

	AddRangedSource("1.3.6.1.2.1.2.0.0",  this,  2,  1);

	AddRangedSource("1.3.6.1.2.1.4.0.0",  this,  4, 23);

	AddRangedSource("1.3.6.1.2.1.5.0.0",  this,  5, 26);

	AddRangedSource("1.3.6.1.2.1.6.0.0",  this,  6, 15);

	AddRangedSource("1.3.6.1.2.1.7.0.0",  this,  7,  4);

	AddRangedSource("1.3.6.1.2.1.8.0.0",  this,  8,  6);

	AddRangedSource("1.3.6.1.2.1.11.0.0", this, 11, 30);

	m_NotifyTime	= COid("1.3.6.1.2.1.1.3.0");

	m_NotifyOid	= COid("1.3.6.1.6.3.1.1.4.1.0");

	m_pSysDesc	= "Crimson 3.0 SNMP Agent";

	m_pSysContact	= "http://www.redlion.net";

	m_pSysName	= "Crimson 3.0 SNMP Agent";

	m_pSysLocation	= "";
	}

// Destructor

CSnmpAgent::~CSnmpAgent(void)
{
	}

// Operations

void CSnmpAgent::Bind(PCUINT pTicks, COid const &System, PCSTR pComm)
{
	m_pTicks = pTicks;

	m_System = System;

	strcpy(m_sComm, pComm);
	}

UINT CSnmpAgent::AddRangedSource(COid const &Oid, ISnmpSource *pSrc, UINT uTag, UINT uCount)
{
	m_Src[m_uCount].m_Oid    = Oid;

	m_Src[m_uCount].m_uCount = uCount;

	m_Src[m_uCount].m_pSrc   = pSrc;

	m_Src[m_uCount].m_uTag   = uTag;

	m_fDirty = TRUE;

	return m_uCount++;
	}

UINT CSnmpAgent::AddSingleSource(COid const &Oid, ISnmpSource *pSrc, UINT uTag)
{
	m_Src[m_uCount].m_Oid    = Oid;

	m_Src[m_uCount].m_uCount = 0;

	m_Src[m_uCount].m_pSrc   = pSrc;

	m_Src[m_uCount].m_uTag   = uTag;

	m_fDirty = TRUE;

	return m_uCount++;
	}

UINT CSnmpAgent::AddNotifySource(COid const &Oid)
{
	m_Src[m_uCount].m_Oid    = Oid;

	m_Src[m_uCount].m_uCount = 0;

	m_Src[m_uCount].m_pSrc   = NULL;

	m_Src[m_uCount].m_uTag   = 0;

	m_fDirty = TRUE;

	return m_uCount++;
	}

void CSnmpAgent::SetSysDesc(PCTXT pDesc)
{
	m_pSysDesc = pDesc;
	}

void CSnmpAgent::SetSysContact(PCTXT pContact)
{
	m_pSysContact = pContact;
	}

void CSnmpAgent::SetSysName(PCTXT pName)
{
	m_pSysName = pName;
	}

void CSnmpAgent::SetSysLoc(PCTXT pLoc)
{
	m_pSysLocation = pLoc;
	}

// Processing

bool CSnmpAgent::OnRequest(CAsn1BerDecoder &req, CAsn1BerEncoder &rep)
{
	SortSourceList();

	if( req.ReadSequence() ) {

		UINT uVer;

		if( req.ReadInteger(uVer) ) {

			if( uVer <= 1 ) {

				char sComm[32];

				if( req.ReadOctString(sComm, sizeof(sComm)) ) {

					if( !strcmp(sComm, m_sComm) ) {

						UINT uTag;

						if( req.ReadConstructed(uTag) ) {

							UINT uRequest;
					
							if( req.ReadInteger(uRequest) ) {

								UINT s1 = rep.AddSequence();

								rep.AddInteger(uVer);
						
								rep.AddOctString(sComm);

								UINT c1 = rep.AddConstructed(2);

								rep.AddInteger(uRequest);

								if( uTag == 5 ) {

									if( !ProcessGetBulk(req, rep, uVer) ) {

										return false;
										}
									}
								else {
									UINT uStatus;
						
									UINT uIndex;

									if( req.ReadInteger(uStatus) &&  req.ReadInteger(uIndex) ) {

										switch( uTag ) {

											case 0:
											case 1:
												if( !ProcessGetData(req, rep, uVer, uTag == 1) ) {

													return false;
													}
												break;

											case 3:
												if( !ProcessSetData(req, rep, uVer) ) {

													return false;
													}
												break;

											default:
												return false;
											}
										}
									}

								rep.AddEnd(c1);

								rep.AddEnd(s1);

								return true;
								}
							}
						}
					}
				}
			}
		}

	return false;
	}

void CSnmpAgent::SetTrapV1(CAsn1BerEncoder &rep, DWORD IP, UINT uType, UINT uCode, UINT uSrc, UINT uPos)
{
	SortSourceList();

	UINT s1 = rep.AddSequence();

	rep.AddInteger(0);
						
	rep.AddOctString(m_sComm);

	UINT c1 = rep.AddConstructed(4);

	rep.AddObjectID(m_System);

	rep.AddNetAddress(IP);

	rep.AddInteger(uType);

	rep.AddInteger(uCode);

	rep.AddTimeTicks(*m_pTicks);

	UINT s2 = rep.AddSequence();

	AddTrapData(rep, uSrc, uPos);
	
	rep.AddEnd(s2);

	rep.AddEnd(c1);

	rep.AddEnd(s1);

	m_uTraps++;
	}

void CSnmpAgent::SetTrapV1(CAsn1BerEncoder &rep, DWORD IP, UINT uType, UINT uCode)
{
	UINT s1 = rep.AddSequence();

	rep.AddInteger(0);
						
	rep.AddOctString(m_sComm);

	UINT c1 = rep.AddConstructed(4);

	rep.AddObjectID(m_System);

	rep.AddNetAddress(IP);

	rep.AddInteger(uType);

	rep.AddInteger(uCode);

	rep.AddTimeTicks(*m_pTicks);

	rep.AddEnd(rep.AddSequence());

	rep.AddEnd(c1);

	rep.AddEnd(s1);

	m_uTraps++;
	}

void CSnmpAgent::SetTrapV2(CAsn1BerEncoder &rep, UINT uBase, UINT uPos, UINT uSrc)
{
	SortSourceList();

	UINT s1 = rep.AddSequence();

	rep.AddInteger(1);
						
	rep.AddOctString(m_sComm);

	UINT c1 = rep.AddConstructed(7);

	rep.AddInteger(m_uTraps++);

	rep.AddInteger(0);

	rep.AddInteger(0);

	UINT s2 = rep.AddSequence();

	AddNotifyTime(rep);

	AddNotifyCode(rep, uBase, uPos);

	AddTrapData  (rep, uSrc,  uPos);

	rep.AddEnd(s2);

	rep.AddEnd(c1);

	rep.AddEnd(s1);
	}

void CSnmpAgent::SetTrapV2(CAsn1BerEncoder &rep, UINT uBase, UINT uPos)
{
	SortSourceList();

	UINT s1 = rep.AddSequence();

	rep.AddInteger(1);
						
	rep.AddOctString(m_sComm);

	UINT c1 = rep.AddConstructed(7);

	rep.AddInteger(m_uTraps++);

	rep.AddInteger(0);

	rep.AddInteger(0);

	UINT s2 = rep.AddSequence();

	AddNotifyTime(rep);

	AddNotifyCode(rep, uBase, uPos);

	rep.AddEnd(s2);

	rep.AddEnd(c1);

	rep.AddEnd(s1);
	}

void CSnmpAgent::SetInfoV2(CAsn1BerEncoder &rep, UINT uBase, UINT uPos, UINT uSrc)
{
	SortSourceList();

	UINT s1 = rep.AddSequence();

	rep.AddInteger(1);
						
	rep.AddOctString(m_sComm);

	UINT c1 = rep.AddConstructed(6);

	rep.AddInteger(m_uTraps++);

	rep.AddInteger(0);

	rep.AddInteger(0);

	UINT s2 = rep.AddSequence();

	AddNotifyTime(rep);

	AddNotifyCode(rep, uBase, uPos);

	AddTrapData  (rep, uSrc,  uPos);

	rep.AddEnd(s2);

	rep.AddEnd(c1);

	rep.AddEnd(s1);
	}

void CSnmpAgent::SetInfoV2(CAsn1BerEncoder &rep, UINT uBase, UINT uPos)
{
	SortSourceList();

	UINT s1 = rep.AddSequence();

	rep.AddInteger(1);
						
	rep.AddOctString(m_sComm);

	UINT c1 = rep.AddConstructed(6);

	rep.AddInteger(m_uTraps++);

	rep.AddInteger(0);

	rep.AddInteger(0);

	UINT s2 = rep.AddSequence();

	AddNotifyTime(rep);

	AddNotifyCode(rep, uBase, uPos);

	rep.AddEnd(s2);

	rep.AddEnd(c1);

	rep.AddEnd(s1);
	}

// Implementation

bool CSnmpAgent::SortSourceList(void)
{
	if( m_fDirty ) {

		qsort(m_Src, m_uCount, sizeof(CSource), SortFunc);

		m_fDirty = false;

		return true;
		}

	return false;
	}

bool CSnmpAgent::FindItem(CLoc &Loc, COid const &Oid)
{
	CKey Key;

	Key.m_pOid = &Oid;
	Key.m_pRow = NULL;
	Key.m_nVal = 0;

	CSource const *pSrc = (CSource const *) bsearch( &Key,
							 m_Src,
							 m_uCount,
							 sizeof(CSource),
							 FindFunc
							 );

	if( pSrc ) {

		if( pSrc->m_uCount ) {

			UINT uSym = Oid.GetSymCount() - 2;

			UINT uPos = Oid.GetSym(uSym);

			if( uPos > 0 && uPos <= pSrc->m_uCount ) {

				bool fOkay = true;

				if( pSrc->m_pSrc ) {

					if( pSrc->m_pSrc->IsSpace(pSrc->m_Oid, pSrc->m_uTag, uPos) ) {

						fOkay = false;
						}
					}

				if( fOkay ) {

					Loc.m_pSrc = pSrc;

					Loc.m_uPos = uPos;

					return true;
					}
				}
			}
		else {
			Loc.m_pSrc = pSrc;

			Loc.m_uPos = 0;

			return true;
			}
		}

	return false;
	}

bool CSnmpAgent::FindNext(CLoc &Loc, COid const &Oid)
{
	CKey Key;

	Key.m_pOid = &Oid;
	Key.m_pRow = NULL;
	Key.m_nVal = 0;

	CSource const *pSrc = (CSource const *) bsearch( &Key,
							 m_Src,
							 m_uCount,
							 sizeof(CSource),
							 FindFunc
							 );

	if( pSrc ) {

		if( pSrc->m_uCount ) {

			UINT uSym = Oid.GetSymCount() - 2;

			UINT uPos = Oid.GetSym(uSym);

			for(;;) {

				if( ++uPos > 0 && uPos <= pSrc->m_uCount ) {

					if( pSrc->m_pSrc ) {

						if( pSrc->m_pSrc->IsSpace(pSrc->m_Oid, pSrc->m_uTag, uPos) ) {

							continue;
							}
						}

					Loc.m_pSrc = pSrc;

					Loc.m_uPos = uPos;

					Loc.m_Oid  = pSrc->m_Oid;

					Loc.m_Oid.SetSym(uSym, uPos);
	
					return true;
					}

				break;
				}
			}

		pSrc++;
		}
	else {
		pSrc = (CSource const *) Key.m_pRow;
		
		if( Key.m_nVal > 0 ) {

			pSrc++;
			}
		}

	if( pSrc ) {

		while( pSrc < m_Src + m_uCount ) {

			if( pSrc->m_pSrc ) {

				if( pSrc->m_uCount ) {

					UINT uSym  = pSrc->m_Oid.GetSymCount() - 2;

					Loc.m_pSrc = pSrc;

					Loc.m_uPos = 1;

					Loc.m_Oid  = pSrc->m_Oid;

					Loc.m_Oid.SetSym(uSym, 1);
					}
				else {
					Loc.m_pSrc = pSrc;

					Loc.m_uPos = 0;

					Loc.m_Oid  = pSrc->m_Oid;
					}

				return true;
				}

			pSrc++;
			}
		}

	return false;
	}

bool CSnmpAgent::ProcessGetData(CAsn1BerDecoder &req, CAsn1BerEncoder &rep, UINT uVer, bool fNext)
{
	UINT  uList = 100;

	COid *pList = new COid [ uList ];

	if( req.ReadVarBindList(pList, uList) ) {

		UINT uPos = rep.GetSize();

		rep.AddInteger(0);

		rep.AddInteger(0);

		UINT s1 = rep.AddSequence();

		for( UINT n = 0; !pList[n].IsNull(); n++ ) {

			CLoc Loc;

			if( fNext ? FindNext(Loc, pList[n]) : FindItem(Loc, pList[n]) ) {

				UINT s2 = rep.AddSequence();

				BOOL fd = false;

				rep.AddObjectID(fNext ? Loc.m_Oid : pList[n]);

				if( Loc.m_pSrc->m_pSrc ) {

					if( Loc.m_pSrc->m_pSrc->GetData( rep,
									 Loc.m_pSrc->m_Oid,
									 Loc.m_pSrc->m_uTag,
									 Loc.m_uPos
									 ) ) {

						fd = true;
						}
					}

				if( !fd ) {

					rep.AddNull();
					}

				rep.AddEnd(s2);
				}
			else {
				if( uVer >= 1 ) {

					UINT s2 = rep.AddSequence();

					rep.AddObjectID(pList[n]);

					if( fNext ) {

						rep.AddEndOfView();
						}
					else
						rep.AddNoSuchObject();

					rep.AddEnd(s2);
					}
				else {
					rep.Rewind(uPos);

					rep.AddInteger(2);
			
					rep.AddInteger(1 + n);

					rep.AddVarBindList(pList);

					delete [] pList;

					return true;
					}
				}
			}

		rep.AddEnd(s1);

		delete [] pList;

		return true;
		}

	delete [] pList;

	return false;
	}

bool CSnmpAgent::ProcessGetBulk(CAsn1BerDecoder &req, CAsn1BerEncoder &rep, UINT uVer)
{
	if( uVer >= 1 ) {

		UINT uNone = 0;

		UINT uRept = 0;

		if( req.ReadInteger(uNone) && req.ReadInteger(uRept) ) {

			UINT  uList = 100;

			COid *pList = new COid [ uList ];

			if( req.ReadVarBindList(pList, uList) ) {

				rep.AddInteger(0);

				rep.AddInteger(0);

				UINT s1 = rep.AddSequence();

				for( UINT n = 0; !pList[n].IsNull(); n++ ) {

					UINT r   = (n < uNone) ? 1 : uRept;

					COid Oid = pList[n];

					for( UINT i = 0; i < r; i++ ) {

						CLoc Loc;

						if( FindNext(Loc, Oid) ) {

							UINT s2 = rep.AddSequence();

							BOOL fd = false;

							rep.AddObjectID(Loc.m_Oid);

							if( Loc.m_pSrc->m_pSrc ) {

								if( Loc.m_pSrc->m_pSrc->GetData( rep,
												 Loc.m_pSrc->m_Oid,
												 Loc.m_pSrc->m_uTag,
												 Loc.m_uPos
												 ) ) {

									fd = true;
									}
								}

							if( !fd ) {

								rep.AddNull();
								}

							rep.AddEnd(s2);

							Oid = Loc.m_Oid;
							}
						else {
							UINT s2 = rep.AddSequence();

							rep.AddObjectID(Oid);

							rep.AddEndOfView();

							rep.AddEnd(s2);

							break;
							}
						}
					}

				rep.AddEnd(s1);

				delete [] pList;

				return true;
				}

			delete [] pList;
			}
		}

	return false;
	}

bool CSnmpAgent::ProcessSetData(CAsn1BerDecoder &req, CAsn1BerEncoder &rep, UINT uVer)
{
	UINT  uList = 100;

	COid *pList = new COid [ uList ];

	if( req.ReadVarBindList(pList, uList) ) {

		rep.AddInteger(uVer ? 17 : 4);
			
		rep.AddInteger(1);

		rep.AddVarBindList(pList);

		delete [] pList;

		return true;
		}

	delete [] pList;

	return false;
	}

void CSnmpAgent::AddNotifyTime(CAsn1BerEncoder &rep)
{
	UINT s1 = rep.AddSequence();

	rep.AddObjectID(m_NotifyTime);

	rep.AddTimeTicks(*m_pTicks);

	rep.AddEnd(s1);
	}

void CSnmpAgent::AddNotifyCode(CAsn1BerEncoder &rep, UINT uSrc, UINT uPos)
{
	if( uSrc < m_uCount ) {

		CSource *pSrc = m_Src + uSrc;

		COid     Oid  = pSrc->m_Oid;

		Oid.Append(uPos);

		UINT s1 = rep.AddSequence();

		rep.AddObjectID(m_NotifyOid);

		rep.AddObjectID(Oid);

		rep.AddEnd(s1);
		}
	}

void CSnmpAgent::AddTrapData(CAsn1BerEncoder &rep, UINT uSrc, UINT uPos)
{
	if( uSrc < m_uCount ) {

		CSource *pSrc = m_Src + uSrc;

		COid     Oid  = pSrc->m_Oid;

		if( pSrc->m_uCount ) {

			UINT uSym = Oid.GetSymCount() - 2;

			Oid.SetSym(uSym, uPos);
			}

		UINT s1 = rep.AddSequence();

		BOOL fd = false;

		rep.AddObjectID(Oid);

		if( pSrc->m_pSrc ) {

			if( pSrc->m_pSrc->GetData( rep,
						   pSrc->m_Oid,
						   pSrc->m_uTag,
						   uPos
						   ) ) {

				fd = true;
				}
			}

		if( !fd ) {

			rep.AddNull();
			}

		rep.AddEnd(s1);
		}
	}

// System Source

bool CSnmpAgent::IsSpace(COid const &Oid, UINT uTag, UINT uPos)
{
	switch( uTag ) {

		case 4:
			switch( uPos ) {

				case 20:
				case 21:
				case 22:
					return true;
				}
			break;
		
		case 6:
			switch( uPos ) {

				case 13:
					return true;
				}
			break;
		
		case 8:
			switch( uPos ) {

				case 5:
					return true;
				}
			break;
		
		case 11:
			switch( uPos ) {

				case 7:
				case 23:
					return true;
				}
			break;
		}

	return false;
	}

bool CSnmpAgent::GetData(CAsn1BerEncoder &rep, COid const &Oid, UINT uTag, UINT uPos)
{
	switch( uTag ) {

		case srcZero:

			rep.AddInteger(0);

			return true;

		case srcIndex:

			rep.AddInteger(uPos);

			return true;
		}

	switch( uTag ) {

		case 1:
			return GetDataForSystem(rep, uPos);

		case 2:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
			// TODO -- Some of these data values are
			// actually Counter32s and not integers.

			rep.AddInteger(0);

			return true;

		case 11:
			return GetDataForSNMP(rep, uPos);
		}

	return false;
	}

bool CSnmpAgent::GetDataForSystem(CAsn1BerEncoder &rep, UINT uPos)
{
	switch( uPos ) {

		case 1:
			rep.AddOctString(m_pSysDesc);
			break;

		case 2:
			rep.AddObjectID(m_System);
			break;

		case 3:
			rep.AddTimeTicks(*m_pTicks);
			break;

		case 4:
			rep.AddOctString(m_pSysContact);
			break;

		case 5:
			rep.AddOctString(m_pSysName);
			break;

		case 6:

			rep.AddOctString(m_pSysLocation);
			break;

		case 7:
			rep.AddInteger(0x80);
			break;

		default:
			return false;
		}

	return true;
	}

bool CSnmpAgent::GetDataForSNMP(CAsn1BerEncoder &rep, UINT uPos)
{
	// TODO -- Return some real SNMP data as required.

	switch( uPos ) {

		case 29:
			rep.AddCounter(m_uTraps);
			break;

		case 30:
			rep.AddInteger(2);
			break;

		default:
			rep.AddCounter(0);
			break;
		}

	return true;
	}

// Sort Function

int CSnmpAgent::SortFunc(void const *p1, void const *p2)
{
	COid const &o1 = ((CSource *) p1)->m_Oid;

	COid const &o2 = ((CSource *) p2)->m_Oid;

	return o1.CompareExact(o2);
	}

int CSnmpAgent::FindFunc(void const *pk, void const *pv)
{
	CKey    *pKey = (CKey    *) pk;

	CSource *pSrc = (CSource *) pv;

	pKey->m_pRow = pSrc;

	pKey->m_nVal = pKey->m_pOid->CompareWithWildCards(pSrc->m_Oid);

	return pKey->m_nVal;
	}

// End of File
