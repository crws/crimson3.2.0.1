
//////////////////////////////////////////////////////////////////////////
//
// Emerson ER3000 Driver
//

#define	PBSZ	16

// Table Definitions
enum {
	PRT	= 231,
	PR1	= 232,
	PR2	= 233,
	PWR	= 234,
	PWT	= 235,
	PW1	= 236,
	PW2	= 237,
	};

#define	SCL_XHI	117
#define	SCL_MIN	150
#define	SCL_MAX	152
#define	SCL_DIF	(SCL_MAX - SCL_XHI)

class CEmersonER3KDriver : public CMasterDriver
{
	public:
		// Constructor
		CEmersonER3KDriver(void);

		// Destructor
		~CEmersonER3KDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Open(void);
		
		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);

		// User Function
		DEFMETH(UINT)	DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;

			// Cached Data Members
			BYTE	m_bWProfT[32];
			WORD	m_wWProf1[32];
			WORD	m_wWProf2[32];
			};

		// Data Members
		CContext *m_pCtx;
		static	WORD CODE_SEG ER3CRCTable[];
		WORD	m_CRC;

		BYTE	m_bTx[PBSZ];
		BYTE	m_bRx[PBSZ];
		UINT	m_uPtr;
		UINT	m_uWErrCt;
		
		// Frame Building
		void	StartFrame(BYTE bLen, BYTE bOp, BYTE bCommand);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	EndFrame(void);
		WORD	MakeCRC(PBYTE pBuf, UINT uCount);

		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		
		// Read Handlers
		BOOL	DoRead(AREF Addr, PDWORD pData, BOOL fStoreProf);
		BOOL	DoReadScale(UINT uOffset, PDWORD pData);
		void	GetProfileData(PDWORD pData, AREF Addr, BOOL fStoreProf);

		// Write Handlers
		BOOL	DoWrite(AREF Addr, PDWORD pData);
		BOOL	DoWriteScale(UINT uOffset, PDWORD pData);

		// Helpers
		BOOL	NoReadTransmit( AREF Addr, PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	IsSigned(AREF Addr);
		BOOL	IsProfileDataWrite(UINT uTable);
		BOOL	IsScale(AREF Addr);
		
		// Port Access
		void	Put(void);
		BOOL	GetReply(void);
		BOOL	CheckReply(void);
		UINT	Get(UINT uTime);

	};

// End of File
