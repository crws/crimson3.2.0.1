
#ifndef INCLUDE_DF1MS_HPP

#define INCLUDE_DF1MS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "df1m.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDF1SerialMaster;

//////////////////////////////////////////////////////////////////////////
//
// Status Codes
//

#define	repFailed	0

#define	repOkay		1

#define	repBusy		2

//////////////////////////////////////////////////////////////////////////
//
// DF1 Serial Master Driver
//

class CDF1SerialMaster : public CDF1BaseMaster
{
	public:
		// Constructor
		CDF1SerialMaster(void);

		// Destructor
		~CDF1SerialMaster(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:
		// Device Data
		struct CContext : CDF1BaseMaster::CBaseCtx
		{
			UINT  m_uTime1;
			UINT  m_uTime2;
			};

		// Data Members
		BOOL       m_fCRC;
		BOOL	   m_fHalf;
		CContext * m_pCtx;
		CRC16      m_CRC;
		BYTE       m_bCheck;
		BYTE	   m_bError;
		BOOL	   m_fNull;

		// Port Access
		void Put(BYTE b);
		UINT Get(UINT uTime);

		// Transport Layer
		BOOL Transact(void);
		BOOL TxFrame(void);
		BOOL TxFrameData(void);
		BOOL RxFrame(void);
		UINT RxCheck(void);
		BOOL CatchWrite(void);

		// Checksum Handler
		void ClearCheck(void);
		void AddToCheck(BYTE b);
		void AddInitToCheck(void);
		void AddTermToCheck(void);
		void SendCheck(void);
		
		// Data Link Layer
		void TxAck (void);
		void TxNak (void);
		void TxEnq (void);
		void TxPoll(void);
		UINT RxAck (void);
	};

// End of File

#endif
