
#ifndef INCLUDE_DF1M_HPP

#define INCLUDE_DF1M_HPP

//////////////////////////////////////////////////////////////////////////
//
// DF1 Device Types
//

enum {
	devPLC5  = 0,
	devSLC   = 1,
	devEIN   = 2
	};

//////////////////////////////////////////////////////////////////////////
//
// DF1 Header Types
//

enum {
	headBase = 0,
	headByte = 1,
	headWord = 2,
	headPCCC = 3,
	headCIP  = 4
	};

//////////////////////////////////////////////////////////////////////////
//
// DF1 Header Without Count (Standard DF1)
//

#pragma pack(1)

struct DF1HEADBASE
{
	BYTE	bDest;
	BYTE	bSource;
	BYTE	bComm;
	BYTE	bStatus;
	U2	wTrans;
	BYTE	bData[0];
	
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// DF1 Header with BYTE Count (DH-485)
//

#pragma pack(1)

struct DF1HEADBYTE
{
	BYTE	bDest;
	BYTE	bSource;
	BYTE	bCount;
	BYTE	bComm;
	BYTE	bStatus;
	U2	wTrans;
	BYTE	bData[0];
	
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// DF1 Header with WORD Count (Ethernet)
//

#pragma pack(1)

struct DF1HEADWORD
{
	BYTE	bDest;
	BYTE	bSource;
	U2	wCount;
	BYTE	bComm;
	BYTE	bStatus;
	U2	wTrans;
	BYTE	bData[0];
	
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// DF1 PCCC Header (EthernetIP)
//

#pragma pack(1)

struct DF1PCCCHEAD
{
	BYTE	bComm;
	BYTE	bStatus;
	U2	wTrans;
	BYTE	bData[0];
	
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// DF1 Definitions
//

#define	BASE_READ	0xA2
#define	BASE_WRITE	0xAA
#define	BASE_BITMASK	0xAB
#define	PLC5_WRITE	0x67
#define PLC5_READ	0x68
#define MAX_STRING	80
#define MAX_CACHE	256

//////////////////////////////////////////////////////////////////////////
//
// Basic DF1 Master Driver
//

class CDF1BaseMaster : public CMasterDriver
{
	public:
		// Constructor
		CDF1BaseMaster(void);

		// Destructor
		~CDF1BaseMaster(void);

		// Master Flags
		DEFMETH(WORD) GetMasterFlags(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Read  (AREF Addr, PDWORD pData, UINT  uCount);
		DEFMETH(CCODE) Write (AREF Addr, PDWORD pData, UINT  uCount);
		DEFMETH(CCODE) Atomic(AREF Addr, DWORD  Data,  DWORD dwMask);

	public:
		// Device Data
		struct CBaseCtx
		{
			UINT	 m_uHeader;
			BYTE	 m_bDevice;
			BYTE	 m_bDest;
			WORD	 m_wTrans;
			CAddress m_Addr  [MAX_CACHE];
			char     m_Cache [MAX_CACHE][MAX_STRING];
			BOOL     m_fWrite[MAX_CACHE];
			UINT     m_uCache;
			UINT     m_uCount;
			PTXT    *m_pTags;
			};

	protected:
		// Frame Data
		struct CFrameData
		{
			BYTE	m_bTxBuff[256];
			UINT	m_uPtr;
			};

		// Data Members
		BYTE       m_bSrc;
		CBaseCtx * m_pBase;
		BYTE	   m_bTxBuff[256];
		BYTE	   m_bRxBuff[256];
		UINT	   m_uPtr;

		// Frame Building
		void NewFrame(BYTE bComm, BYTE bCode);
		void NewFrame(BYTE bComm, BYTE bCode, WORD wTrans);
		void NewFrameBase(BYTE bComm, BYTE bCode, WORD wTrans);
		void NewFrameByte(BYTE bComm, BYTE bCode, WORD wTrans);
		void NewFrameWord(BYTE bComm, BYTE bCode, WORD wTrans);
		void NewFramePCCC(BYTE bComm, BYTE bCode, WORD wTrans);
		void NewFrameCIP (BYTE bComm, BYTE bCode, WORD wTrans);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddAddr(AREF Addr, BOOL fTyped);
		void AddAddr(AREF Addr);
		void AddAddr(WORD wAddr);
		void AddText(PCTXT pText);
		void AddTypeInfo(UINT uCount, BYTE bTypeID);
		void AddByte(PDWORD pData, UINT uCount);
		void AddWord(PDWORD pData, UINT uCount);
		void AddLong(PDWORD pData, UINT uCount);
		BOOL AddString(PBYTE pData, UINT uCount, CAddress Addr);

		// Frame Push
		PVOID PushFrame(void);
		void  PullFrame(PVOID pData);

		// Data Access
		PBYTE FindDataField(PBYTE pFrame, BOOL fIgnore);
		BYTE  ParseTypeInfo(PBYTE pData);
		void  CopyByte(PBYTE pReply, PDWORD pData, UINT uCount);
		void  CopyWord(PBYTE pReply, PDWORD pData, UINT uCount);
		void  CopyLong(PBYTE pReply, PDWORD pData, UINT uCount);
		void  CopyString(PBYTE pReply, PDWORD pData, UINT uCount, CAddress Addr);
		void  DecodeString(PBYTE pText, UINT uCount, CAddress Addr);

		// IO File Support
		BOOL HasFileIO(UINT uSpace, UINT uTable);
		BOOL IsBaseIO(UINT uSpace, UINT uTable);
		void AddIO(UINT uSpace, UINT uTable, UINT uOffset);
		void AddFile(UINT uSpace, UINT uTable, UINT uOffset);
		UINT GetOffset(UINT uSpace, UINT uTable, UINT uOffset);

		// Transport Layer
		virtual BOOL CheckLink(void);
		virtual BOOL Transact(void);

		// Helpers
		BOOL IsTriplet(UINT uSpace);
		BOOL IsLong(UINT uSpace);
		BOOL IsString(UINT uSpace);
		virtual void GetCount(UINT uSpace, UINT &uCount);

		// String Caching Support
		UINT	 FindCachedString(CAddress Addr, BOOL fRead = FALSE);
		BOOL	 PutCachedString(CAddress Addr, PBYTE pText, UINT uOffset, UINT uCount);
		void	 GetCachedString(CAddress Addr, PBYTE pText, UINT uOffset, UINT uCount);
		BOOL	 ReadFromCache(CAddress Addr, PBYTE pData, UINT uCount);
		BOOL	 WriteToCache(CAddress Addr, PDWORD pData, UINT uCount);
		BOOL	 WriteFromCache(CAddress Addr, BOOL &fWrite);
		CAddress GetBaseAddress(CAddress Addr);

		// Tag Name Support
		void  LoadTags(PCBYTE pData);
		PCTXT GetTagName(UINT uIndex);
	};

// End of File

#endif
