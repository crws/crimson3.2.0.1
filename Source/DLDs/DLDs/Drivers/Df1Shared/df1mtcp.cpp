
#include "intern.hpp"

#include "df1mtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Base Class
//

#include "df1m.cpp"

//////////////////////////////////////////////////////////////////////////
//
// DF1 TCP Master Driver
//

// Instantiator

#if DRIVER_ID == 0x3503

INSTANTIATE(CDF1TCPMaster);

#endif

// Constructor

CDF1TCPMaster::CDF1TCPMaster(void)
{
	m_Ident = DRIVER_ID;
	
	m_fCRC  = TRUE;

	m_pCtx  = NULL;

	m_uKeep = 0;
	}

// Destructor

CDF1TCPMaster::~CDF1TCPMaster(void)
{
	}

// Management

void MCALL CDF1TCPMaster::Attach(IPortObject *pPort)
{
	}

void MCALL CDF1TCPMaster::Open(void)
{
	}

// Device

CCODE MCALL CDF1TCPMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_uHeader = headWord;
			m_pCtx->m_bDevice = GetByte(pData);
			m_pCtx->m_IP	  = GetAddr(pData);
			m_pCtx->m_uPort	  = GetWord(pData);
			m_pCtx->m_fDirty  = FALSE;
			m_pCtx->m_bDest   = GetByte(pData);
			m_pCtx->m_fKeep   = GetByte(pData);
			m_pCtx->m_fPing   = GetByte(pData);
			m_pCtx->m_uTime1  = GetWord(pData);
			m_pCtx->m_uTime2  = GetWord(pData);
			m_pCtx->m_uTime3  = GetWord(pData);
			m_pCtx->m_wTrans  = WORD(RAND(m_pCtx->m_bDevice + 1));
			m_pCtx->m_pSock	  = NULL;
			m_pCtx->m_uLast   = GetTickCount();

			m_pCtx->m_uPort	  = 2222;

			m_pCtx->m_uCache  = 0;

			ClearConnection();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CDF1TCPMaster::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CDF1TCPMaster::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) {
		
		// Set Device IP Address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		pCtx->m_IP     = MotorToHost(dwValue);

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 4 ) { 
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP);
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL CDF1TCPMaster::Ping(void)
{
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

// Transport Layer

BOOL CDF1TCPMaster::CheckLink(void)
{
	if( OpenSocket() ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CDF1TCPMaster::Transact(void)
{
	if( m_pCtx->m_fNew ) {

		if( !MakeConnection() ) {

			return FALSE;
			}
		}

	if( SendFrame() ) {
		
		if( RecvFrame() ) {

			StripNetHead();

			if( !CheckFrame() ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}
		}

	CloseSocket(TRUE);

	return FALSE;
	}

// Socket Management

BOOL CDF1TCPMaster::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CDF1TCPMaster::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		m_pCtx->m_fDirty   = FALSE;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					ClearConnection();

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CDF1TCPMaster::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort ) {

			m_pCtx->m_pSock->Abort();
			}
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Helpers

BOOL CDF1TCPMaster::SendFrame(void)
{
	SetByteCount();

	CBuffer *pBuff = CreateBuffer(1280, TRUE);    

	if( pBuff ) {

		NETHEAD *pHead = BuffAddTail(pBuff, NETHEAD);

		memset(pHead, 0, sizeof(NETHEAD));

		pHead->bOne     = 1;

		pHead->bCommand = 7;

		pHead->bCount   = m_uPtr;
		
		pHead->wTrans   = HostToIntel(m_pBase->m_wTrans);
		
		pHead->dwConn   = m_pCtx->m_dwConn;  

		memcpy( pBuff->AddTail(m_uPtr),
			m_bTxBuff,
			m_uPtr
			);

		if( m_pCtx->m_pSock->Send(pBuff) == S_OK ) {
			
			return TRUE;
			}

		pBuff->Release();
		}
	
	return FALSE;
	}

BOOL CDF1TCPMaster::RecvFrame(void)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= sizeof(NETHEAD) ) {

				NETHEAD *pHead = (NETHEAD *) m_bRxBuff;

				UINT     uNeed = sizeof(NETHEAD) + pHead->bCount;

				if( uPtr >= uNeed ) {

					if( m_pCtx->m_fNew ) {
						
						if( IntelToHost(pHead->wTrans) == 0 ) {

							return TRUE;
							}
						}
					else {
						if( m_pBase->m_bDevice == devPLC5 ) {
						
							DF1HEADWORD &RxHead = (DF1HEADWORD &) m_bRxBuff[sizeof(NETHEAD)];

							if( IntelToHost(RxHead.wTrans) == m_pBase->m_wTrans ) {

								return TRUE;
								}

							return FALSE;
							}

						if( IntelToHost(pHead->wTrans) == m_pBase->m_wTrans ) {

							return TRUE;
							}
						}

					if( uPtr -= uNeed ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRxBuff[n] = m_bRxBuff[uNeed++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {
			
			return FALSE;
			}

		Sleep(10);
		}
	
	return FALSE;
	}

BOOL CDF1TCPMaster::CheckFrame(void)
{
	DF1HEADWORD &TxHead = (DF1HEADWORD &) m_bTxBuff[0];
			
	DF1HEADWORD &RxHead = (DF1HEADWORD &) m_bRxBuff[0];

	if( RxHead.bComm != (TxHead.bComm | (1 << 6)) ) {
		
		return FALSE;
		}
	
	return RxHead.bStatus ? FALSE : TRUE;
	}

void CDF1TCPMaster::SetByteCount(void)
{
	DF1HEADWORD &TxHead = (DF1HEADWORD &) m_bTxBuff[0];

	TxHead.wCount = 0;
	}

void CDF1TCPMaster::StripNetHead(void)
{
	NETHEAD *pHead = (NETHEAD *) m_bRxBuff;

	UINT     uSize = pHead->bCount;

	memcpy(m_bRxBuff, m_bRxBuff + sizeof(NETHEAD), uSize);
	}

// Link Management

BOOL CDF1TCPMaster::MakeConnection(void)
{
	if( SendConnectReq() ) {
		
		if( RecvFrame() ) {

			NETHEAD *pHead   = (NETHEAD *) m_bRxBuff;

			m_pCtx->m_fNew   = FALSE;

			m_pCtx->m_dwConn = pHead->dwConn;

			return TRUE;
			}
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CDF1TCPMaster::SendConnectReq(void)
{
	CBuffer *pBuff = CreateBuffer(1280, TRUE);

	if( pBuff ) {

		NETHEAD *pHead = BuffAddTail(pBuff, NETHEAD);

		memset(pHead, 0, sizeof(NETHEAD));

		pHead->bOne     = 1;

		pHead->bCommand = 1;

		pHead->bCount   = 0;
		
		pHead->wTrans   = 0;
		
		pHead->dwConn   = 0;

		pHead->bFour    = 4;

		pHead->bFive    = 5;

		if( m_pCtx->m_pSock->Send(pBuff) == S_OK ) {
			
			return TRUE;
			}

		pBuff->Release();
		}
	
	return FALSE;
	}

void CDF1TCPMaster::ClearConnection(void)
{
	m_pCtx->m_fNew   = TRUE;

	m_pCtx->m_dwConn = 0;
	}

void CDF1TCPMaster::GetCount(UINT uSpace, UINT &uCount)
{
	// Note:  uCount is limited to 50 in configs block.cpp

	UINT uMax = m_pBase->m_bDevice == devPLC5 ? 16 : 96;
	
	if( IsTriplet(uSpace) ) {

		if( m_pBase->m_bDevice == devPLC5 ) {	
										
			uMax  = 1;
			}
		else {
			uMax /= 3;
			}
		}

	else if( IsString(uSpace) )  {

		uCount = MAX_STRING; 

		return;
		}

	else if( IsLong(uSpace) ) {

		uMax /= 2;
		}

	MakeMin(uCount, uMax);
	}

// Helpers

BOOL CDF1TCPMaster::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CDF1TCPMaster::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CDF1TCPMaster::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
