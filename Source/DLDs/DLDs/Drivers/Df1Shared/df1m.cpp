
#include "intern.hpp"

#include "df1m.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Basic DF1 Master Driver
//

// Constructor

CDF1BaseMaster::CDF1BaseMaster(void)
{
	m_bSrc	= 1;

	m_pBase = NULL;
	}

// Destructor

CDF1BaseMaster::~CDF1BaseMaster(void)
{
	}

// Master Flags

WORD MCALL CDF1BaseMaster::GetMasterFlags(void)
{
	return MF_ATOMIC_WRITE;
	}

// Entry Points

CCODE MCALL CDF1BaseMaster::Ping(void)
{
	if( CheckLink() ) {

		// It appears that some AB OS's do not support the PING 
		// command.  In SLC's without this support, the PING 
		// command can cause AB port lockups.  Ping N7:0 first to 
		// prevent this occurrence.

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = 7;
		Addr.a.m_Offset = 0;
		Addr.a.m_Type   = 9;
		Addr.a.m_Extra  = 5;
		
		if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

			return CCODE_SUCCESS;
			}
		else {
			NewFrame(0x06, 0x00);

			PCTXT pData = "PING!!";

			UINT  uSize = strlen(pData);

			AddText(pData);

			if( Transact() ) {

				PBYTE pReply = FindDataField(m_bRxBuff, TRUE);
			      
				if( !memcmp(pReply, pData, uSize) ) {

					return CCODE_SUCCESS;
					}
				}
			}
		}

	return CCODE_ERROR;
	}

void MCALL CDF1BaseMaster::Service(void)
{
	if( m_pBase && CheckLink() ) {

		for( UINT u = 0;  u < m_pBase->m_uCache; u++ ) {

			WriteFromCache(m_pBase->m_Addr[u], m_pBase->m_fWrite[u]);
			}
		}
	}

CCODE MCALL CDF1BaseMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{	
	if( CheckLink() ) {

		UINT uBytes  = 2;

		UINT uExtra  = Addr.a.m_Extra;

		BOOL fString = IsString(uExtra);

		UINT Count   = uCount;

		GetCount(uExtra, uCount);

		CAddress Address = GetBaseAddress(Addr);

		if( fString ) {

			uBytes = 1;
			}

		else if( IsLong(uExtra) ) {

			uBytes = 4;
			}

		if( m_pBase->m_bDevice == devPLC5 ) {
				
			NewFrame(0x0F, PLC5_READ);

			AddWord(0x00);

			AddWord(fString ? uCount + 2 : uCount);

			AddAddr(Address, TRUE);

			AddWord(uCount);
			} 
		else {	
			NewFrame(0x0F, BASE_READ);

			AddByte(fString ? uBytes * uCount + 2 : uBytes * uCount);
		
			AddAddr(Address, FALSE);
			} 

		if( Transact() ) {

			PBYTE pReply = FindDataField(m_bRxBuff, FALSE);

			if( fString ) {

				UINT uBytes = pReply[0] | (pReply[1] << 8);

				pReply += 2;

				UINT uMin = min(Count, uCount);

				if( uBytes > uMin ) {

					uBytes = uMin;
					}

				DecodeString(pReply, uBytes, Addr);

				CopyString(pReply, pData, uBytes, Addr);

				return uMin;
				}
		
			if( uBytes == 2 ) {
			
				CopyWord(pReply, pData, uCount);
				}
			else {
				CopyLong(pReply, pData, uCount);
				} 

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CDF1BaseMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		UINT uBytes = 2;

		if( IsString(Addr.a.m_Extra) ) {

			uBytes = 1;
			}

		if( IsLong(Addr.a.m_Extra) ) {

			uBytes = 4;
			}

		UINT Count = uCount;

		GetCount(Addr.a.m_Extra, uCount);

		if( m_pBase->m_bDevice == devPLC5 ) {
				
			NewFrame(0x0F, PLC5_WRITE);

			AddWord(0x00);

			AddWord(uCount);

			AddAddr(Addr, TRUE);

			AddTypeInfo(uCount, Addr.a.m_Extra);

			if( uBytes == 2 ) {

				AddWord(pData, uCount);
				}
			else
				AddLong(pData, uCount);
			}
		else {	
			if( IsString(Addr.a.m_Extra) ) {

				if( WriteToCache(Addr, pData, min(Count, uCount)) ) {

					return min(Count, uCount);
					}

				return CCODE_ERROR;
				}
						
			NewFrame(0x0F, BASE_WRITE);

			AddByte(uBytes * uCount);

			AddAddr(Addr, FALSE);
				
			if( uBytes == 2 ) {

				AddWord(pData, uCount);
				}
			else
				AddLong(pData, uCount);
			}

		if( Transact() ) {

			return uCount;
			}
		}
	
	return CCODE_ERROR;
	}

CCODE MCALL CDF1BaseMaster::Atomic(AREF Addr, DWORD Data, DWORD dwMask)
{
	if( CheckLink() ) {
		
		if( IsString(Addr.a.m_Extra) ) {

			return CCODE_ERROR | CCODE_SILENT;
			}

		if( IsLong(Addr.a.m_Extra) ) {

			return CCODE_ERROR | CCODE_SILENT;
			}

		if( 1 || m_pBase->m_bDevice == devPLC5 ) {

			// TODO -- PLC5 Read-Modify-Write?

			return CCODE_ERROR | CCODE_SILENT;
			}
		else {
			NewFrame(0x0F, BASE_BITMASK);

			AddByte(2);

			AddAddr(Addr, FALSE);

			AddWord(WORD(dwMask));
				
			AddWord(WORD(Data ? dwMask : 0));
			}

		if( Transact() ) {

			return 1;
			}
		}

	return CCODE_ERROR;
	}

// Frame Building

void CDF1BaseMaster::NewFrame(BYTE bComm, BYTE bCode)
{
	NewFrame(bComm, bCode, ++m_pBase->m_wTrans);
	}

void CDF1BaseMaster::NewFrame(BYTE bComm, BYTE bCode, WORD wTrans)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));
	
	switch( m_pBase->m_uHeader ) {

		case headBase:
			
			NewFrameBase(bComm, bCode, wTrans);
			
			break;

		case headByte:
			
			NewFrameByte(bComm, bCode, wTrans);
			
			break;

		case headWord:
			
			NewFrameWord(bComm, bCode, wTrans);
			
			break;

		case headPCCC: 
			
			NewFramePCCC(bComm, bCode, wTrans);
			
			break;

		case headCIP: 
			
			NewFrameCIP(bComm, bCode, wTrans);
			
			break;
		}
	}

void CDF1BaseMaster::NewFrameBase(BYTE bComm, BYTE bCode, WORD wTrans)
{
	DF1HEADBASE &TxHead = (DF1HEADBASE &) m_bTxBuff[0];

	TxHead.bDest	= m_pBase->m_bDest;
	TxHead.bSource	= m_bSrc;
	TxHead.bComm	= bComm;
	TxHead.bStatus	= 0;
	TxHead.wTrans	= HostToIntel(wTrans);

	m_uPtr = sizeof(TxHead);

	AddByte(bCode);
	}

void CDF1BaseMaster::NewFrameByte(BYTE bComm, BYTE bCode, WORD wTrans)
{
	DF1HEADBYTE &TxHead = (DF1HEADBYTE &) m_bTxBuff[0];

	TxHead.bDest	= m_pBase->m_bDest;
	TxHead.bSource	= m_bSrc;
	TxHead.bCount	= 0;
	TxHead.bComm	= bComm;
	TxHead.bStatus	= 0;
	TxHead.wTrans	= HostToIntel(wTrans);

	m_uPtr = sizeof(TxHead);

	AddByte(bCode);
	}

void CDF1BaseMaster::NewFrameWord(BYTE bComm, BYTE bCode, WORD wTrans)
{
	DF1HEADWORD &TxHead = (DF1HEADWORD &) m_bTxBuff[0];

	TxHead.bDest	= m_pBase->m_bDest;
	TxHead.bSource	= m_bSrc;
	TxHead.wCount	= 0;
	TxHead.bComm	= bComm;
	TxHead.bStatus	= 0;
	TxHead.wTrans	= HostToIntel(wTrans);

	m_uPtr = sizeof(TxHead);

	AddByte(bCode);
	}

void CDF1BaseMaster::NewFramePCCC(BYTE bComm, BYTE bCode, WORD wTrans)
{
	DF1PCCCHEAD &TxHead = (DF1PCCCHEAD &) m_bTxBuff[0];

	TxHead.bComm	= bComm;
	TxHead.bStatus	= 0;
	TxHead.wTrans	= HostToIntel(wTrans);

	m_uPtr = sizeof(TxHead);

	AddByte(bCode);
	}

void CDF1BaseMaster::NewFrameCIP(BYTE bComm, BYTE bCode, WORD wTrans)
{
	m_uPtr = 0;
	}

void CDF1BaseMaster::AddByte(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;
	}

void CDF1BaseMaster::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CDF1BaseMaster::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CDF1BaseMaster::AddAddr(AREF Addr, BOOL fTyped)
{
	UINT uSpace  = Addr.a.m_Extra;
	
	UINT uTable  = Addr.a.m_Table;

	UINT uOffset = Addr.a.m_Offset;

	if( fTyped ) {

		if( IsTriplet(uSpace) ) {

			AddByte(0x0F);			// 4 Levels

			AddByte(0x00);			// Level 1

			AddByte(BYTE(uTable));		// Level 2

			AddAddr(WORD(uOffset / 3));	// Level 3

			AddByte(BYTE(uOffset % 3));	// Level 4

			return;
			}

		AddByte(0x07);

		AddByte(0x00);

		if( HasFileIO(uSpace, uTable) ) { 
			
			AddAddr(LOWORD(GetOffset(uSpace, uTable, uOffset)));	
			
			AddIO(uSpace, uTable, uOffset);					
			}
		else {		      	
			AddFile(uSpace, uTable, uOffset);

			AddAddr(LOWORD(GetOffset(uSpace, uTable, uOffset)));
			}

		return;
		}

	AddAddr(Addr);
	}

void CDF1BaseMaster::AddAddr(AREF Addr)
{
	BYTE bType   = 0;

	UINT uSpace  = Addr.a.m_Extra;

	UINT uTable  = Addr.a.m_Table;  

	UINT uOffset = Addr.a.m_Offset;

	switch( uSpace ) {
	
		case 1:  bType = 0x8B; 	break;
		case 2:  bType = 0x8C;  break;
		case 3:  bType = 0x84;	break;
		case 4:  bType = 0x85;	break;
		case 5:  bType = 0x89;	break;
		case 6:	 bType = 0x86;	break;
		case 7:	 bType = 0x87;	break;
		case 8:	 bType = 0x8A;	break;
		case 9:	 bType = 0x91;	break;
		case 10: bType = 0x8D;	break;
		
		default: return;
		}

	AddFile(uSpace, uTable, uOffset);

	AddByte(bType);

	if( bType == 0x86 || bType == 0x87 ) {

		AddAddr(LOBYTE(uOffset / 3));

		AddByte(BYTE(uOffset % 3));

		return;
		} 

	if( !HasFileIO(uSpace, uTable) && ( m_pBase->m_bDevice == devEIN || IsString(uSpace) ) ) {

		if( IsTriplet(uSpace) || IsLong(uSpace) ) {

			AddAddr(LOWORD(GetOffset(uSpace, uTable, uOffset)));
			}

		AddIO(uSpace, uTable, uOffset);
		}
	else { 
		AddAddr(LOWORD(GetOffset(uSpace, uTable, uOffset)));
		
		AddIO(uSpace, uTable, uOffset);
		}	
	}

void CDF1BaseMaster::AddAddr(WORD wAddr)
{
	if( wAddr > 0xFE ) {

		AddByte(0xFF);

		AddWord(wAddr);

		return;
		}

	AddByte(LOBYTE(wAddr));
	}

void CDF1BaseMaster::AddText(PCTXT pText)
{
	while( *pText ) {

		AddByte(*pText);

		pText++;
		}
	}

void CDF1BaseMaster::AddTypeInfo(UINT uCount, BYTE bTypeID)
{
	AddByte(0x99); 

	AddByte(0x09);
	
	if( bTypeID == 8 ) {

		AddByte(2 + 4 * uCount); 

		AddByte(0x94);		 

		AddByte(0x08);		 	
		}
	
	else if( bTypeID == 9 ) {

		AddByte(1 + 4 * uCount);

		AddByte(0x44);
		}
	else {
		AddByte(1 + 2 * uCount); 
		
		AddByte(0x42);		 
		}
	}

void CDF1BaseMaster::AddByte(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		BYTE bData = pData[n];

		AddByte(bData);
		}
	}

void CDF1BaseMaster::AddWord(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		WORD wData = pData[n];

		AddWord(wData);
		}
	}

void CDF1BaseMaster::AddLong(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD dwData = pData[n];

		AddWord(LOWORD(dwData));

		AddWord(HIWORD(dwData));
		}
	}

BOOL CDF1BaseMaster::AddString(PBYTE pData, UINT uCount, CAddress Addr)
{	
	BYTE bByte = 0;

	for( UINT n = 0; n < uCount; n++ ) {

		bByte =  n % 2 ? pData[n - 1] : pData[n + 1];

		AddByte( bByte );
		}
	
	return TRUE;
	}

// Frame Push

PVOID CDF1BaseMaster::PushFrame(void)
{
	CFrameData *pFrame = new CFrameData;

	memcpy(pFrame->m_bTxBuff, m_bTxBuff, sizeof(m_bTxBuff));

	pFrame->m_uPtr = m_uPtr;

	return pFrame;
	}

void CDF1BaseMaster::PullFrame(PVOID pData)
{
	CFrameData *pFrame = (CFrameData *) pData;

	memcpy(m_bTxBuff, pFrame->m_bTxBuff, sizeof(m_bTxBuff));

	m_uPtr = pFrame->m_uPtr;

	delete pFrame;
	}

// Data Access

PBYTE CDF1BaseMaster::FindDataField(PBYTE pFrame, BOOL fIgnore)
{
	switch( m_pBase->m_uHeader ) {

		case headBase:
			pFrame += sizeof(DF1HEADBASE);
			break;

		case headByte:
			pFrame += sizeof(DF1HEADBYTE);
			break;

		case headWord:
			pFrame += sizeof(DF1HEADWORD);
			break;

		case headPCCC:
			pFrame += sizeof(DF1PCCCHEAD);
			break;
		}

	if( m_pBase->m_bDevice == devPLC5 ) {
		
		if( !fIgnore ) {

			pFrame += ParseTypeInfo(pFrame);
			}
		}

	return pFrame;
	}

BYTE CDF1BaseMaster::ParseTypeInfo(PBYTE pData)
{
	BYTE bSize  = 0;
	
	BOOL fArray = FALSE;
	
	BYTE bFlag  = pData[bSize ++];

	if( bFlag & 0x80 ) {

		BYTE bTypeID = pData[ bSize ++ ];

		fArray = (bTypeID == 9); 
		}

	if( bFlag & 0x08 ) {

		bSize += (bFlag & 0x07);
		}

	if( fArray ) {

		BYTE bExtra = ParseTypeInfo(&pData[bSize]);

		return bSize + bExtra;
		}
	
	return bSize;
	}

void CDF1BaseMaster::CopyByte(PBYTE pReply, PDWORD pData, UINT uCount)
{	
	for(UINT n = 0; n < uCount; n++ ) {

		BYTE bData = PBYTE(pReply)[n];

		pData[n] = bData;
		}
	}

void CDF1BaseMaster::CopyWord(PBYTE pReply, PDWORD pData, UINT uCount)
{	
	for(UINT n = 0; n < uCount; n++ ) {

		WORD wData = PU2(pReply)[n];

		pData[n] = SHORT(IntelToHost(wData));
		}
	}

void CDF1BaseMaster::CopyLong(PBYTE pReply, PDWORD pData, UINT uCount)
{	
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD dwData = PU4(pReply)[n];

		pData[n] = LONG(IntelToHost(dwData));
		}
	}

void CDF1BaseMaster::CopyString(PBYTE pReply, PDWORD pData, UINT uCount, CAddress Addr)
{
	if( ReadFromCache(Addr, pReply, uCount) ) {

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = pReply[n];
			}
		}
	}

void CDF1BaseMaster::DecodeString(PBYTE pReply, UINT uCount, CAddress Addr)
{
	PBYTE pText = PBYTE(alloca(MAX_STRING));

	memset(pText, 0, MAX_STRING);

	MakeMin(uCount, MAX_STRING);

	for( UINT u = 0; u < uCount; u++ ) {

		pText[u] = u % 2 ? pReply[u - 1] : pReply[u + 1];
		}

	PutCachedString(Addr, pText, 0, MAX_STRING);  
	}

// IO File Support

BOOL CDF1BaseMaster::HasFileIO(UINT uSpace, UINT uTable) 
{
	if( uSpace == 1 || uSpace == 2 ) {

		if( uTable > 0 ) {

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CDF1BaseMaster::IsBaseIO(UINT uSpace, UINT uTable)
{
	if( uSpace == 1 || uSpace == 2 ) {

		if( uTable == 0 ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CDF1BaseMaster::AddIO(UINT uSpace, UINT uTable, UINT uOffset)
{
	if( IsString(uSpace) ) {

		AddByte(LOBYTE(uOffset / MAX_STRING));

		AddByte(0x00);

		return;
		}

	if( IsLong(uSpace) ) {

		AddByte(0x00);

		return;
		} 

	if( HasFileIO(uSpace, uTable) ) {

	 	AddByte(LOBYTE(uOffset));

		return;
		}

 	AddByte(0x00);

	if( m_pBase->m_bDevice == devEIN ) {
		
		AddAddr(uOffset);
		}
	}

void CDF1BaseMaster::AddFile(UINT uSpace, UINT uTable, UINT uOffset)
{
	if( HasFileIO(uSpace, uTable) ) {

		AddByte(0x00);

		return;
		}

	if( IsBaseIO(uSpace, uTable) || uTable == 0xE0 ) {

		AddByte(BYTE(uSpace) - 1);

		return;
		}

	AddByte(BYTE(uTable));
	}

UINT CDF1BaseMaster::GetOffset(UINT uSpace, UINT uTable, UINT uOffset)
{
	if( HasFileIO(uSpace, uTable) ) {

		return uTable;
		} 

	return uOffset;
	}

// Transport Layer
		
BOOL CDF1BaseMaster::CheckLink(void)
{
	return TRUE;
	}

BOOL CDF1BaseMaster::Transact(void)
{
	return FALSE;
	}

// Helpers 

BOOL CDF1BaseMaster::IsTriplet(UINT uSpace)
{
	if( uSpace == 6 || uSpace == 7 ) {

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CDF1BaseMaster::IsLong(UINT uSpace)
{
	if( uSpace == 8 || uSpace == 9 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CDF1BaseMaster::IsString(UINT uSpace)
{
	return uSpace == 10;
	}

void CDF1BaseMaster::GetCount(UINT uSpace, UINT &uCount)
{
	UINT uMax = 16;
	
	if( IsTriplet(uSpace) ) {

		if( m_pBase->m_bDevice == devPLC5 ) {	
										
			uMax  = 1;
			}
		else {
			uMax /= 3;
			}
		}

	else if( IsString(uSpace) )  {

		uCount = MAX_STRING; 

		return;
		}

	else if( IsLong(uSpace) ) {

		uMax /= 2;
		}
		
	MakeMin(uCount, uMax);
	}

// String Caching Support

UINT CDF1BaseMaster::FindCachedString(CAddress Addr, BOOL fRead)
{
	CAddress Base = GetBaseAddress(Addr);

	for( UINT u = 0; u < MAX_CACHE; u++ ) {

		if( u < m_pBase->m_uCache ) {

			if( m_pBase->m_Addr[u].m_Ref == Base.m_Ref ) {

				return u;
				}

			continue;
			}

		m_pBase->m_Addr  [u] = Base;

		m_pBase->m_fWrite[u] = FALSE;

		memset(m_pBase->m_Cache[u], 0, MAX_STRING);

		if( fRead ) {

			PDWORD pData = PDWORD(alloca(MAX_STRING * sizeof(DWORD)));

			Read(Base, pData, MAX_STRING);
			}

		m_pBase->m_uCache++;

		return u;
		}

	return 0;
	}

BOOL CDF1BaseMaster::PutCachedString(CAddress Addr, PBYTE pText, UINT uOffset, UINT uCount)
{
	UINT uFind = FindCachedString(Addr);

	if( WriteFromCache(m_pBase->m_Addr[uFind], m_pBase->m_fWrite[uFind]) ) {
	
		memcpy(m_pBase->m_Cache[uFind] + uOffset, pText, uCount);

		return TRUE;
		}

	return FALSE;
	}

void CDF1BaseMaster::GetCachedString(CAddress Addr, PBYTE pText, UINT uOffset, UINT uCount)
{
	memcpy(pText, m_pBase->m_Cache[FindCachedString(Addr)] + uOffset, uCount);
	}

BOOL CDF1BaseMaster::ReadFromCache(CAddress Addr, PBYTE pData, UINT uCount)
{
	CAddress Base    = GetBaseAddress(Addr);

	UINT     uOffset = (Addr.a.m_Offset % MAX_STRING);

	GetCachedString(Base, pData, uOffset, MAX_STRING - uOffset);
      
	if( uOffset && MAX_STRING > uOffset ) {

		Base.a.m_Offset += MAX_STRING;

		pData += (MAX_STRING - uOffset);

		GetCachedString(Base, pData, 0, uOffset);
      		} 
	
	return TRUE;
	}

BOOL CDF1BaseMaster::WriteToCache(CAddress Addr, PDWORD pData, UINT uCount)
{
	PBYTE pText = PBYTE(alloca(uCount));

	memset(pText, 0, uCount);

	for( UINT u = 0; u < uCount; u++ ) {

		if( pData[u] == 0x00 ) {

			break;
			} 
		
		pText[u] = pData[u] & 0xFF;
		}

	CAddress Base = GetBaseAddress(Addr);

	UINT uOffset = (Addr.a.m_Offset % MAX_STRING);

	UINT Count = min(uCount, MAX_STRING - uOffset);
	
	if( PutCachedString(Base, pText, uOffset, Count) )  {

		m_pBase->m_fWrite[FindCachedString(Base)] = TRUE;

		WriteFromCache(Base, m_pBase->m_fWrite[FindCachedString(Base)]);

		if( uOffset && Count < uCount ) {

			Base.a.m_Offset += MAX_STRING;

			pText           += Count;

			if( PutCachedString(Base, pText, 0, uOffset) )  {

				m_pBase->m_fWrite[FindCachedString(Base)] = TRUE;

				return TRUE;
				}
			}
			
		return TRUE;
		}

	return FALSE;
	}

BOOL CDF1BaseMaster::WriteFromCache(CAddress Addr, BOOL &fWrite)
{
       	if( !fWrite ) {

		return TRUE;
		}
	
	if( CheckLink() ) {

		NewFrame(0x0F, BASE_WRITE);

		PBYTE pData = PBYTE(alloca(MAX_STRING));
		
		GetCachedString(Addr, pData, 0, MAX_STRING);

		UINT uCount = MAX_STRING;

		for(UINT u = 0; u < uCount; u++ ) {

			if( pData[u] == 0x00 ) {

				uCount = u;

				if( u % 2 ) {

					uCount++;
					}
			       
				break;
				}
			}
		
		AddByte(uCount + 2);

		AddAddr(Addr, FALSE);

		AddWord(uCount);

		AddString(pData, uCount, Addr);

		if( Transact() ) {

			fWrite = FALSE;

			return TRUE;
			}
		}

	return FALSE;
	}

CAddress CDF1BaseMaster::GetBaseAddress(CAddress Addr)
{
	CAddress Base;

	Base = Addr;
	
	if( Addr.a.m_Extra == 10 ) {

		Base.a.m_Offset = Addr.a.m_Offset - (Addr.a.m_Offset % MAX_STRING);
		}

	return Base;
	}

// Tag Name Support

void CDF1BaseMaster::LoadTags(PCBYTE pData)
{
	UINT uCount       = GetWord(pData);

	m_pBase->m_uCount = uCount;

	m_pBase->m_pTags  = new PTXT [ uCount ];

	for( UINT n = 0; n < uCount; n ++ ) {
		
		m_pBase->m_pTags[n] = GetString(pData);
		}	
	}

PCTXT CDF1BaseMaster::GetTagName(UINT uIndex)
{
	if( uIndex < m_pBase->m_uCount ) {
		
		return m_pBase->m_pTags[uIndex];
		}

	return "";
	}

// End of File
