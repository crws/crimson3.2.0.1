
#include "intern.hpp"

#include "df1ms.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Base Class
//

#include "df1m.cpp"

//////////////////////////////////////////////////////////////////////////
//
// DF1 Serial Master Driver
//

// Instantiator

#if DRIVER_ID == 0x3365

INSTANTIATE(CDF1SerialMaster);

#endif

// Constructor

CDF1SerialMaster::CDF1SerialMaster(void)
{
	m_Ident = DRIVER_ID;
	
	m_fCRC  = TRUE;

	m_fHalf = FALSE;

	m_pCtx  = NULL;
	}

// Destructor

CDF1SerialMaster::~CDF1SerialMaster(void)
{
	}

// Configuration

void MCALL CDF1SerialMaster::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bSrc  = GetByte(pData);

 		m_fHalf = GetByte(pData);

		m_fCRC  = GetByte(pData);
		}
	}
	
void MCALL CDF1SerialMaster::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, FALSE);
	}
	
// Management

void MCALL CDF1SerialMaster::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CDF1SerialMaster::Open(void)
{
	}

// Device

CCODE MCALL CDF1SerialMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_uHeader = headBase;

			m_pCtx->m_bDevice = GetByte(pData);

			m_pCtx->m_bDest   = GetByte(pData);

			m_pCtx->m_uTime1  = GetWord(pData);

			m_pCtx->m_uTime2  = GetWord(pData);

			m_pCtx->m_wTrans  = WORD(RAND(m_pCtx->m_bDevice + 1));

			m_pCtx->m_uCache  = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CDF1SerialMaster::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Port Access

void CDF1SerialMaster::Put(BYTE b)
{
	m_pData->Write(b, FOREVER);
	}

UINT CDF1SerialMaster::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Transport Layer

BOOL CDF1SerialMaster::Transact(void)
{
	DF1HEADBASE &TxHead = (DF1HEADBASE &) m_bTxBuff[0];

	DF1HEADBASE &RxHead = (DF1HEADBASE &) m_bRxBuff[0]; 

	for(;;) {

		TxFrame();

		for(;;) {

			if( m_fHalf ) {

				for( UINT i = 0;; ) {

					TxPoll();
		
					if( RxFrame() ) {

						if( !m_fNull ) {

							if( CatchWrite() ) {

								continue;
								}

							break;
							}

						Sleep(50);
						}

					if( i++ < 4 ) {

						continue;
						}

					m_bError = 0;

					return FALSE;
					}
				}
			else {
				if( !RxFrame() ) {

					m_bError = 0;
			
					return FALSE;
					}
				}

			if( RxHead.wTrans == TxHead.wTrans ) {

				switch( RxCheck() ) {
				
					case repOkay:
						
					  	return TRUE;
						
					case repFailed:
						
						return FALSE;
					}
					
				break;
				}
			}
		}

	AfxTouch(RxHead);
	}

BOOL CDF1SerialMaster::TxFrame(void)
{
	if( m_fHalf ) {

		for( int i = 0; i < 4; i++ ) {
		
			TxFrameData();

			for( int j = 0; j < 4; j++ ) {

				switch( RxAck() ) {

					case ACK:
						return TRUE;

					case NAK:
						j = 4;
						break;

					case NOTHING:
						j = 4;
						break;
					}
				}
			}

		return FALSE;
		}
	else {
		for( int i = 0; i < 2; i++ ) {
		
			TxFrameData();

			for( int j = 0; j < 4; j++ ) {
				
				switch( RxAck() ) {
				
					case ACK:
						return TRUE;
						
					case NAK:
						j = 4;
						break;
						
					case STX:
						TxAck();
						break;
					}
			
				if( j < 3 ) {

					TxEnq();
					}
				}
			}

		return FALSE;
		}
	}

BOOL CDF1SerialMaster::TxFrameData(void)
{
	ClearCheck();

	if( m_fHalf ) {

		Put(DLE);
		Put(SOH);

		if( m_pCtx->m_bDest == DLE ) {

			Put(DLE);
			}

		BYTE b = m_pCtx->m_bDest;

		Put(b);

		AddToCheck(b);

		AddInitToCheck();
		}

	Put(DLE);
	Put(STX);

	for( UINT n = 0; n < m_uPtr; n++ ) {
	
		BYTE b = m_bTxBuff[n];

		if( b == DLE ) {

			Put(DLE);
			}

		Put(b);

		AddToCheck(b);
		}

	Put(DLE);
	Put(ETX);
	
	AddTermToCheck();
	
	SendCheck();

	return TRUE;
	}
	
BOOL CDF1SerialMaster::RxFrame(void)
{
	BOOL fDLE   = FALSE;
	
	UINT uState = 0;
	
	UINT uCount = 0;
	
	UINT uRetry = 2;

	UINT uTimer = 0;

	UINT uData  = 0;
	
	WORD wCheck = 0;
	
	SetTimer(m_pCtx->m_uTime1);

	while( (uTimer = GetTimer()) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {
			
			continue;
			}

		if( uData == DLE ) {
		
			if( !fDLE ) {
			
				if( uState < 2 ) {
				
					fDLE = TRUE;
					
					continue;
					}
				}
			else
				fDLE = FALSE;
			}
			
		switch( uState ) {
		
			case 0:
				if( fDLE ) {

					if( m_fHalf && uData == EOT ) {

						m_fNull = TRUE;

						return TRUE;
						}
				
					if( uData == ENQ ) {

						TxAck();
						}
						
					if( uData == STX ) {
					
						ClearCheck();

						m_fNull = FALSE;

						uCount  = 0;
						
						uState  = 1;
						}
					}
				break;
				
			case 1:
				if( fDLE ) {
				
					if( uData == ETX ) {
					
						AddTermToCheck();
						
						if( m_fCRC ) {

							uState = 2;
							}
						else
							uState = 3;
						}
					else
						return FALSE;
					}
				else {
					m_bRxBuff[uCount++] = uData;
					
					if( uCount == sizeof(m_bRxBuff) ) {

						return FALSE;
						}
						
					AddToCheck(uData);
					}
				break;
				
			case 2:
				wCheck = uData;
				
				uState = 3;
				
				break;
				
			case 3:
				if( m_fCRC ) {
				
					wCheck += (uData << 8);
				
					if( wCheck == m_CRC.GetValue() ) {
				
						TxAck();

						return TRUE;
						}
					}
				else {
					AddToCheck(uData);
				
					if( !m_bCheck ) {
				
						TxAck();

						return TRUE;
						}
					}

				if( uRetry-- ) {
					
					TxNak();
						
					SetTimer(m_pCtx->m_uTime1);
						
					uState = 0;
					}
				else {
					TxAck();
					
					return FALSE;
					}
				break;
			}
			
		fDLE = FALSE;
		}

	return FALSE;
	}

UINT CDF1SerialMaster::RxCheck(void)
{
	DF1HEADBASE &TxHead = (DF1HEADBASE &) m_bTxBuff[0];
			
	DF1HEADBASE &RxHead = (DF1HEADBASE &) m_bRxBuff[0];

	if( RxHead.bComm != (TxHead.bComm | (1 << 6)) ) {

		m_bError = 0;

		return repFailed;
		}

	if( RxHead.bDest != TxHead.bSource ) {
		
		m_bError = 0;

		return repFailed;
		}

	if( RxHead.bSource != TxHead.bDest ) {

		if( RxHead.bSource == TxHead.bSource ) {

			switch( RxHead.bStatus & 0x0F ) {

				case 0x01:
					return repBusy;
				}

			m_bError = 0;

			return repFailed;
			}

		m_bError = 0;

		return repFailed;
		}

	if( RxHead.bStatus ) {

		m_bError = RxHead.bStatus;
		
		return repFailed;
		}
	
	return repOkay;
	}

BOOL CDF1SerialMaster::CatchWrite(void)
{
	DF1HEADBASE &TxHead = (DF1HEADBASE &) m_bTxBuff[0];
			
	DF1HEADBASE &RxHead = (DF1HEADBASE &) m_bRxBuff[0];

	if( RxHead.bComm == 0x0F ) {

		if( RxHead.bStatus == 0x00 ) {

			if( RxHead.bData[0] == BASE_WRITE ) {

				PVOID pFrame = PushFrame();

				NewFrame(0x4F, 0x00, IntelToHost(RxHead.wTrans));

				TxFrame();

				RxAck();

				PullFrame(pFrame);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Checksum Handler

void CDF1SerialMaster::ClearCheck(void)
{
	if( m_fCRC ) {
	
		m_CRC.Clear();
		
		return;
		}
		
	m_bCheck = 0;
	}

void CDF1SerialMaster::AddToCheck(BYTE b)
{
	if( m_fCRC ) {
	
		m_CRC.Add(b);
		
		return;
		}
	
	m_bCheck += b;
	}

void CDF1SerialMaster::AddInitToCheck(void)
{
	if( m_fCRC && m_fHalf ) {
	
		m_CRC.Add(STX);
		
		return;
		}
	}

void CDF1SerialMaster::AddTermToCheck(void)
{
	if( m_fCRC ) {
	
		m_CRC.Add(ETX);
		
		return;
		}
	}

void CDF1SerialMaster::SendCheck(void)
{
	if( m_fCRC ) {

		Put(LOBYTE(m_CRC.GetValue()));
	
		Put(HIBYTE(m_CRC.GetValue()));
		
		return;
		}
	
	BYTE b = ((0x100 - m_bCheck) & 0xFF);
		
	Put(b);
	}

// Data Link Layer

void CDF1SerialMaster::TxAck(void)
{	
	Put(DLE);
	Put(ACK);
	}

void CDF1SerialMaster::TxNak(void)
{
	Put(DLE);
	Put(NAK);
	}

void CDF1SerialMaster::TxEnq(void)
{
	Put(DLE);
	Put(ENQ);
	}

void CDF1SerialMaster::TxPoll(void)
{
	// NOTE -- Always use BCC!

	BOOL c = m_fCRC;

	m_fCRC = FALSE;

	ClearCheck();

	Put(DLE);
	Put(ENQ);

	BYTE b = m_pCtx->m_bDest;

	Put(b);

	AddToCheck(b);

	SendCheck();

	m_fCRC = c;
	}

UINT CDF1SerialMaster::RxAck(void)
{
	SetTimer(m_pCtx->m_uTime2);
	
	BOOL fDLE   = FALSE;

	UINT uTimer = 0;

	UINT uData  = 0;
	
	while( (uTimer = GetTimer()) ) {

		if ((uData = Get(uTimer)) == NOTHING ) {

			continue;
			}
			
		if( fDLE ) {
		
			switch( uData ) {
			
				case ACK:
				case NAK:
				case STX:
				case EOT:
					return uData;
				}
				
			fDLE = FALSE;
			}
		else {
			if( uData == DLE ) {

				fDLE = TRUE;
				}
			}
		}
	
	return NOTHING;
	}
		
// End of File
