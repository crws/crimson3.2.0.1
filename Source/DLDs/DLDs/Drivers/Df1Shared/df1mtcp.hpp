
#ifndef INCLUDE_DF1MTCP_HPP

#define INCLUDE_DF1MTCP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "df1m.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDF1TCPMaster;

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Header
//

#pragma pack(1)

struct NETHEAD
{
	BYTE	bOne;
	BYTE	bCommand;
	BYTE	xx02;
	BYTE	bCount;
	DWORD	dwConn;
	BYTE	xx03;
	BYTE	xx04;
	BYTE	xx05;
	WORD	wTrans;
	BYTE	bFour;
	BYTE	xx06;
	BYTE	bFive;
	BYTE	xx07;
	BYTE	xx08;
	WORD	xx09;
	WORD	xx10;
	WORD	xx11;
	WORD	xx12;
	WORD	xx13;
	};

#pragma pack()

/////////////////////////////////////////////////////////////////////////
//
// DF1 TCP Master Driver
//

class CDF1TCPMaster : public CDF1BaseMaster
{
	public:
		// Constructor
		CDF1TCPMaster(void);

		// Destructor
		~CDF1TCPMaster(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT) DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE) Ping(void);

		// Device Data
		struct CContext : CDF1BaseMaster::CBaseCtx
		{
			DWORD	  m_IP;
			UINT	  m_uPort;
			BOOL	  m_fDirty;
			BOOL	  m_fKeep;
			BOOL	  m_fPing;
			UINT	  m_uTime1;
			UINT	  m_uTime2;
			UINT	  m_uTime3;
			ISocket * m_pSock;
			UINT	  m_uLast;
			BOOL	  m_fNew;
			DWORD	  m_dwConn;
			};

	protected:

		// Data Members
		CContext * m_pCtx;
		CRC16      m_CRC;
		BYTE       m_bCheck;
		UINT	   m_uKeep;
		BOOL       m_fCRC;
		
		// Transport Layer
		BOOL CheckLink(void);
		BOOL Transact(void);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Transport Helpers
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL CheckFrame(void);
		void SetByteCount(void);
		void StripNetHead(void);

		// Link Management
		BOOL MakeConnection(void);
		BOOL SendConnectReq(void);
		void ClearConnection(void);

		// Helpers
		virtual	void GetCount(UINT uSpace, UINT &uCount);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
	};

// End of File

#endif
