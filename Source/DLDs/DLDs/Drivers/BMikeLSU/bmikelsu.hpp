#ifndef INCLUDE_BMIKELSU_HPP

#define INCLUDE_BMIKELSU_HPP

#include "../BMikeLS/bmikels.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed UDP Master Driver
//

class CBMikeLSMasterUdpDriver : public CBMikeLSMasterDriver
{
	public:
		// Constructor
		CBMikeLSMasterUdpDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT ) DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(void ) Service(void);

				
	protected:

		struct CMacAddr
		{
			DWORD  m_IP;
			WORD   m_Port;
			};
		
		// Device Context
		struct CContext : CBaseCtx
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKickTB;
			BOOL	 m_fTB;
			};

		// Data Members
		CContext * m_pCtx;
		CBuffer  * m_pTxBuff;
		CBuffer  * m_pRxBuff;
		PBYTE      m_pTxData;
		CMacAddr   m_RxMac;
		ISocket  * m_pSock[2];
		
		// Implementation
		BOOL  Begin(UINT uSpace);
		void  AddByte(BYTE bByte);
		
		// Transport Layer
		BOOL Send(void);
		BOOL RecvFrame(void);
		BOOL CheckFrame(void);
		BOOL EndRealTime(CContext * pCtx);

		// Transport Header
		void AddTransportHeader(CContext * pCtx);
		BOOL StripTransportHeader(void);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
		BOOL IsRealTime(UINT uSpace);
	};


#endif

// End of File
