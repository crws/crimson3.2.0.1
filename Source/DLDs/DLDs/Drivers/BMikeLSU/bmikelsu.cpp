#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "bmikelsu.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Base Class
//

#include "../BMikeLS/bmikels.cpp"

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed UDP Master Driver
//

// Instantiator

#if DRIVER_ID == 0x409C

INSTANTIATE(CBMikeLSMasterUdpDriver);

#endif

// Constructor

CBMikeLSMasterUdpDriver::CBMikeLSMasterUdpDriver(void)
{
	m_Ident   = DRIVER_ID;

	m_fTB	  = FALSE;

	m_fTF     = FALSE;

	m_pTxData = NULL;

	m_pTxBuff = NULL;

	m_pRxBuff = NULL;

	m_bMask	  = 0xFF;

	m_pData   = NULL;
	}

// Configuration

void MCALL CBMikeLSMasterUdpDriver::Load(LPCBYTE pData)
{
	}

// Management

void MCALL CBMikeLSMasterUdpDriver::Attach(IPortObject *pPort)
{
	for( UINT u = 0; u < elements(m_pSock); u++ ) {

		m_pSock[u] = CreateSocket(IP_UDP);
		}
	}

void MCALL CBMikeLSMasterUdpDriver::Detach(void)
{
	for( UINT u = 0; u < elements(m_pSock); u++ ) {

		if( m_pSock[u] ) {

			m_pSock[u]->Release();

			m_pSock[u] = NULL;
			}
		}
	}

void MCALL CBMikeLSMasterUdpDriver::Open(void)
{
	if( m_pSock[0] ) {

		m_pSock[0]->SetOption(OPT_ADDRESS, 1);

		m_pSock[0]->SetOption(OPT_RECV_QUEUE, 2);

		m_pSock[0]->Listen(1001);
		}

	if( m_pSock[1] ) {

		m_pSock[1]->SetOption(OPT_ADDRESS, 1);

		m_pSock[1]->SetOption(OPT_RECV_QUEUE, 2);
		}
	}


// Device

CCODE MCALL CBMikeLSMasterUdpDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_Scan	    = ToTicks(GetWord(pData));
			m_pCtx->m_Timeout   = GetWord(pData);
			m_pBase->m_bAuto    = GetByte(pData);

			memset(m_pCtx->m_Time,   0, elements(m_pCtx->m_Time)   * sizeof(DWORD));
			memset(m_pCtx->m_Record, 0, elements(m_pCtx->m_Record) * sizeof(DWORD));
			memset(m_pCtx->m_Info,   0, elements(m_pCtx->m_Info)   * sizeof(DWORD));
			memset(m_pCtx->m_Comms,  0, elements(m_pCtx->m_Comms)  * sizeof(DWORD));

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);

			m_pCtx->m_fError = TRUE;

			m_pBase->m_bModel = 0;

			memset(m_pBase->m_ARB, 0, elements(m_pBase->m_ARB) * sizeof(DWORD));

			m_pBase->m_fInit = TRUE;

			m_pCtx->m_fKickTB = FALSE;

			m_pCtx->m_fTB = FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CBMikeLSMasterUdpDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		EndRealTime(m_pCtx);

		for( UINT u = 0; u < elements(m_pSock); u++ ) {

			if( m_pSock[u] ) {

				m_pSock[u]->Release();

				m_pSock[u] = NULL;
				}
			}
		
		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CBMikeLSMasterUdpDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		if( EndRealTime(pCtx) ) {
		
			pCtx->m_IP = MotorToHost(dwValue);

			Free(pText);
			
			return 1;    
			}

		return 0;
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			if( EndRealTime(pCtx) ) {

				pCtx->m_uPort   = uValue;

				return 1;
				}
			}

		return 0;
		}

	if( uFunc == 3 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP);
		}
	
	if( uFunc == 4 ) {
		
		// Get Current Port

		return pCtx->m_uPort;
		}
	
	return 0;
	}

// Entry Points

void MCALL CBMikeLSMasterUdpDriver::Service(void)
{
	if( !m_pCtx->m_fKickTB ) {

		if( m_pCtx->m_fTB ) {

			Begin(spaceRTL);

			End(spaceRTL);

			if( Send() ) {

				Sleep(5);

				m_pCtx->m_fKickTB = TRUE;
				}
			}
		}

	if( m_pCtx->m_fKickTB ) {

		BOOL fSuccess = FALSE;

		m_pSock[1]->Listen(WORD(m_pCtx->m_uPort));
				
		SetTimer(100);

		while( GetTimer() ) {
		
			m_pSock[1]->Recv(m_pRxBuff);

			if( m_pRxBuff ) {

				if( StripTransportHeader() ) {

					if( (m_RxMac.m_IP != m_pCtx->m_IP) || (m_RxMac.m_Port != 1002) ) {

						m_pRxBuff->Release();

						m_pRxBuff = NULL;

						continue;
						}
					
					PBYTE pData = m_pRxBuff->GetData();

					UINT uPtr = m_pRxBuff->GetSize();

					if( uPtr ) {

						m_uPtr = 0;

						for( UINT u = 0; u < uPtr; u++ ) {

							if( u < 5 ) {

								if( pData[u] == 0xFF ) {

									continue;
									}
								break;
								}

							if( u == 5 ) {

								if( pData[u] == 0xFF ) {

									break;
									}

								fSuccess = TRUE;
								}

							m_bRx[m_uPtr++] = pData[u];
							}

						if( fSuccess && ChecksumCheck() ) {

							// Cache data

							DWORD x = PU4(m_bRx + 1)[0];

							m_pBase->m_Record[spaceRTL] = MotorToHost(x);

							x = PU4(m_bRx + 6)[0];

							m_pBase->m_Record[spaceRTV] = MotorToHost(x);

							m_pBase->m_Record[spaceRTQF] = m_bRx[0];

							m_pBase->m_Record[spaceRTS] = m_bRx[5];

							m_pRxBuff->Release();

							m_pRxBuff = NULL;

							break;
							}

						}
					}
					
				m_pRxBuff->Release();

				m_pRxBuff = NULL;
				}

			Sleep(5);
			}

		m_pSock[1]->Close();
		
		m_pCtx->m_fKickTB = fSuccess;

		m_pCtx->m_fError  = !fSuccess;
		}
	}

// Implementation

BOOL CBMikeLSMasterUdpDriver::Begin(UINT uSpace)
{
	if( (m_pTxBuff = CreateBuffer(600, TRUE)) ) {

		m_pTxBuff->AddTail(600);

		m_pTxData = m_pTxBuff->GetData();

		CBMikeLSMasterDriver::Begin(uSpace);
		
		return TRUE;
		}
	
	Sleep(10);

	return FALSE;
	}

void CBMikeLSMasterUdpDriver::AddByte(BYTE bByte)
{
	m_pTxData[m_uPtr++] = bByte;	
	}

// Transport Layer

BOOL CBMikeLSMasterUdpDriver::Send(void)
{
	m_pTxBuff->StripTail(m_pTxBuff->GetSize() - m_uPtr + 1);
	
	AddTransportHeader(m_pCtx);
	
	for( UINT n = 0; n < 5; n++ ) {

		if( m_pSock[0]->Send(m_pTxBuff) == S_OK ) {

			m_pTxBuff = NULL;
			
			return TRUE;
			}
		
		Sleep(5);
		}

	m_pTxBuff->Release();

	m_pTxBuff = NULL;

	return FALSE;
	}

BOOL CBMikeLSMasterUdpDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_Timeout);

	while( GetTimer() ) {
		
		m_pSock[0]->Recv(m_pRxBuff);

		if( m_pRxBuff ) {

			if( StripTransportHeader() ) {

				PBYTE pData = m_pRxBuff->GetData();

				m_uPtr = m_pRxBuff->GetSize();

				if( m_uPtr ) {

					for( UINT u = 0; u < m_uPtr; u++ ) {

						m_bRx[u] = pData[u];
						}

					m_bRx[m_uPtr++] = CR;

					m_pRxBuff->Release();

					m_pRxBuff = NULL;
				
					return TRUE;
					}
				}
					
			m_pRxBuff->Release();

			m_pRxBuff = NULL;
			}

		Sleep(5);
		}

	return FALSE;
	}

BOOL CBMikeLSMasterUdpDriver::CheckFrame(void)
{
	return TRUE;
	}

BOOL CBMikeLSMasterUdpDriver::EndRealTime(CContext * pCtx)
{
	if( (m_pTxBuff = CreateBuffer(600, TRUE)) ) {

		m_pTxBuff->AddTail(600);

		m_pTxData = m_pTxBuff->GetData();
	
		m_uPtr = 0;

		AddByte('.');

		AddByte(CR);

		m_pTxBuff->StripTail(m_pTxBuff->GetSize() - m_uPtr + 1);
	
		AddTransportHeader(pCtx);
	
		for( UINT n = 0; n < 5; n++ ) {

			if( m_pSock[0]->Send(m_pTxBuff) == S_OK ) {

				m_pTxBuff = NULL;

				pCtx->m_fKickTB = FALSE;

				pCtx->m_fTB	= FALSE;

				return TRUE;
				}
		
			Sleep(5);
			}

		m_pTxBuff->Release();

		m_pTxBuff = NULL;
		}

	return FALSE;
	}

// Transport Header
		
void CBMikeLSMasterUdpDriver::AddTransportHeader(CContext * pCtx)
{
	DWORD IP    = pCtx->m_IP;

	WORD Port   = 1001;

	UINT  uData = m_pTxBuff->GetSize();

	UINT  uSize = 6;

	PBYTE pData = m_pTxBuff->AddHead(uSize);

	pData[0] = PBYTE(&IP)[0];
	pData[1] = PBYTE(&IP)[1];
	pData[2] = PBYTE(&IP)[2];
	pData[3] = PBYTE(&IP)[3];

	pData[4] = HIBYTE(Port);
	pData[5] = LOBYTE(Port);

	}

BOOL CBMikeLSMasterUdpDriver::StripTransportHeader(void)
{	
	PBYTE pData = m_pRxBuff->GetData();

	if( pData ) {

		((PBYTE) &m_RxMac.m_IP)[0] = pData[0];
		((PBYTE) &m_RxMac.m_IP)[1] = pData[1];
		((PBYTE) &m_RxMac.m_IP)[2] = pData[2];
		((PBYTE) &m_RxMac.m_IP)[3] = pData[3];

		m_RxMac.m_Port = MAKEWORD(pData[5], pData[4]);

		m_pRxBuff->StripHead(6);

		return TRUE;
		}

	return FALSE;
	}

// Helpers

BOOL CBMikeLSMasterUdpDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CBMikeLSMasterUdpDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CBMikeLSMasterUdpDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

BOOL CBMikeLSMasterUdpDriver::IsRealTime(UINT uSpace)
{	
	switch( uSpace ) {

		case spaceRTL:
		case spaceRTV:
		case spaceRTS:
			m_pCtx->m_fTB = TRUE;

			return  m_pCtx->m_fTB;

		case spaceRTQF:
			return m_pCtx->m_fTB;

		case spaceFL:
			m_pCtx->m_fTB = FALSE;
			
			return  m_pCtx->m_fTB;
	
		case spaceAQF:
			return m_pCtx->m_fTB;
		}

	return FALSE;
	}

// End of File
