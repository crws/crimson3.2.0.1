
#include "intern.hpp"

#include "brminis.hpp"

//////////////////////////////////////////////////////////////////////////
//
// B & R Mininet Serial Driver
//

// Instantiator

INSTANTIATE(CBRMininetSerialDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CBRMininetSerialDriver::CBRMininetSerialDriver(void)
{
	m_Ident		= 0x3322;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;
	}

// Destructor

CBRMininetSerialDriver::~CBRMininetSerialDriver(void)
{
	}

// Configuration

void MCALL CBRMininetSerialDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CBRMininetSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CBRMininetSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CBRMininetSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CBRMininetSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop		= GetByte(pData);

			m_pCtx->m_dWriteErr	= 0;

			m_pCtx->m_bIndex	= 0;

			m_pCtx->m_fIndex	= TRUE;

			m_pCtx->m_fReset	= TRUE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CBRMininetSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CBRMininetSerialDriver::Ping(void)
{
//**/	AfxTrace0("\r\nPING");

	if( CheckInit() ) {

		m_pCtx->m_fReset = FALSE;

		m_pCtx->m_fIndex = FALSE;

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = 1;
		Addr.a.m_Offset = 70;
		Addr.a.m_Type   = addrRealAsReal;
		Addr.a.m_Extra  = 0;

		return Read(Addr, Data, 1 );
		}

	return CCODE_ERROR;
	}

CCODE MCALL CBRMininetSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SP_SLAVERESET ) {

		*pData = 0;

		return (CCODE)1;
		}

	if( CheckInit() ) {

//**/	AfxTrace3("\r\nREAD T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);
//**/	Sleep(100);	// Slow access for debugging

		MakeMin( uCount, 16 );

		AddRead(Addr, pData, uCount);

		if( CheckResponse( Transact() ) ) {

			GetResponse( Addr, pData, uCount );

			return uCount;
			}

		CheckError();

		CheckInit();
		}

	return CCODE_ERROR;
	}

CCODE MCALL CBRMininetSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\nWRITE T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);
//**/	AfxTrace1("D=%8.8lx ", pData[0]);

	if( CheckInit() ) {

		if( Addr.a.m_Table == SP_SLAVERESET ) {

			ResetSlave();

			return 1;
			}

		AddWrite( Addr, pData, uCount );

		if( Transact() ) {

			NextIndex();

			return uCount;
			}

		CheckError();

		CheckInit();
		}

	if( ++m_pCtx->m_dWriteErr > 2 ) {

		m_pCtx->m_dWriteErr = 0;

		return 1;
		}

	return CCODE_ERROR;
	}

// Implementation

BOOL CBRMininetSerialDriver::ResetIndex(void)
{
	m_uPtr	= 0;

	m_pTx	= m_bTx;
	
	AddByte(STX);

	AddByte(0x00);

	AddByte(m_pCtx->m_bDrop);
	
	AddByte(0x3F);

	if( Transact() ) {

		if( m_bRx[1] != RESET_EXECUTED ) {

			m_pCtx->m_fReset = TRUE;
			}

		m_pCtx->m_bIndex = 0;

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CBRMininetSerialDriver::ResetSlave(void)
{
	m_pCtx->m_bIndex = 0;
	
	StartFrame();
	
	AddByte(ESC);

	AddByte('R');

	return (Transact() == 1) && (m_bRx[1] == RESET_EXECUTED);
	}

void CBRMininetSerialDriver::NextIndex(void)
{
	m_pCtx->m_bIndex = (BYTE)((m_pCtx->m_bIndex + 1) % 64);
	}

BOOL CBRMininetSerialDriver::CheckInit(void)
{
	if( m_pCtx->m_fIndex ) {

		if( !ResetIndex() ) return FALSE;

		m_pCtx->m_bIndex = 1;

		m_pCtx->m_fIndex = FALSE;
		}

	if( m_pCtx->m_fReset ) {

		if( !ResetSlave() ) return FALSE;

		NextIndex();

		m_pCtx->m_fReset = FALSE;
		}
	
	return TRUE;
	}

void CBRMininetSerialDriver::CheckError(void)
{
	switch( m_bRx[1] ) {

		case RESET_EXECUTED:
			m_pCtx->m_fReset = FALSE;
			break;

		case INDEX_ERROR:
			m_pCtx->m_fIndex = TRUE;
			break;

		case INDEX_CONTRAD:
		case UNRECOGNIZED:
		case DUPLICATE_CMD:
		case INVALID_PARAM:
			break;

		case INDEX_RESET:
			m_pCtx->m_fIndex = FALSE;
			break;
		}
	}

// Frame Building

void CBRMininetSerialDriver::StartFrame(void)
{
	m_pTx	= m_bTx;

	m_uPtr	= 0;

	AddByte( STX );

	AddByte( 0 );

	AddByte( m_pCtx->m_bDrop );

	AddByte(m_pCtx->m_bIndex | 0x40);
	}

void CBRMininetSerialDriver::EndFrame(void)
{
	m_pCtx->m_bCheck = 0;

	UINT uTxCt	= m_uPtr;

	m_bTx[1]	= LOBYTE(m_uPtr + 1);

	m_uPtr		= 0;

	m_pTx		= m_bSend;

	for( UINT i = 0; i < uTxCt; i++ ) {

		BYTE b = m_bTx[i];

		CheckAdd(b);

		AddByte(b);

		if( b == STX && i >= 5 ) {

			AddByte(0);
			}
		}

	AddByte(CheckRead());
	}

void CBRMininetSerialDriver::AddByte(BYTE bData)
{
	m_pTx[m_uPtr++] = bData;
	}

void CBRMininetSerialDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CBRMininetSerialDriver::AddLong(DWORD dData)
{
	AddWord(HIWORD(dData));

	AddWord(LOWORD(dData));
	}

void CBRMininetSerialDriver::AddWordRev(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CBRMininetSerialDriver::AddLongRev(DWORD dData)
{
	AddWord(LOWORD(dData));

	AddWord(HIWORD(dData));
	}

void CBRMininetSerialDriver::AddRead( AREF Addr, PDWORD pData, UINT uCount )
{
	StartFrame();

	AddByte( OP_REG_READ );

	AddByte( GetuCount(Addr.a.m_Type, uCount) );

	AddWord( Addr.a.m_Offset );	// address is byte location
	}

void CBRMininetSerialDriver::AddWrite( AREF Addr, PDWORD pData, UINT uCount )
{
	StartFrame();

	AddByte( OP_REG_WRITE );

	AddByte( GetuCount(Addr.a.m_Type, uCount) );

	AddWord(Addr.a.m_Offset);	// address is byte location

	BOOL fRev = Addr.a.m_Table == SP_BYTES_REV;

	for( UINT i = 0; i < uCount; i++ ) {

		switch( Addr.a.m_Type ) {

			case addrByteAsReal:
			case addrByteAsLong:

				fRev ? AddLongRev(pData[i]) : AddLong(pData[i]);
				return;

			case addrByteAsWord:

				fRev ? AddWordRev(LOWORD(pData[i])) : AddWord(LOWORD(pData[i]));
				return;

			case addrByteAsByte:

				AddByte(LOBYTE(LOWORD(pData[i])));
				return;
			}
		}
	}

BYTE CBRMininetSerialDriver::GetuCount(UINT uType, UINT uCount)
{
	switch( uType ) {

		case addrByteAsReal:
		case addrByteAsLong:

			uCount *= 4;
			break;

		case addrByteAsWord:

			uCount *= 2;
			break;
		}

	return uCount;
	}

// Transport Layer

UINT CBRMininetSerialDriver::Transact(void)
{
	EndFrame();

	Send();

	return GetReply();
	}

void CBRMininetSerialDriver::Send(void)
{
	m_pData->ClearRx();

	Put();
	}

UINT CBRMininetSerialDriver::GetReply(void)
{
	WORD wState = 0;

	WORD wIndex = 0;

	WORD wCount = 0;

	UINT uData;

	UINT uTimer;

	BYTE bData;

	SetTimer(1000);

//**/	AfxTrace0("\r\n");

	while( (uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);

		switch( wState ) {

			case 0:
				switch( bData ) {

					case STX:
						m_pCtx->m_bCheck = 0;

						CheckAdd(bData);

						wIndex = 0;

						wState = 1;

						break;

					case ACK:
						return 2;

					case NAK:
						return 0;
					}

				break;

			case 1:
				if( bData ) {

					CheckAdd(bData);

					wCount = bData - 2;

					wState = 2;
					}

				break;

			case 2:
				if( !--wCount ) {

					return (bData == CheckRead()) ? 1 : 0;
					}
				else {
					m_bRx[wIndex++] = bData;

					CheckAdd(bData);

					if( bData == STX ) wState = 3;
					}

				break;

			case 3:
				wState = 2;

				break;
			}
		}

	return 0;
	}

void CBRMininetSerialDriver::GetResponse( AREF Addr, PDWORD pData, UINT uCount )
{
	UINT i    = 0;

	UINT uPos = 3;

	BOOL fRev = Addr.a.m_Table == SP_BYTES_REV;

	while( i < uCount ) {

		switch( Addr.a.m_Type ) {

			case addrByteAsReal:
			case addrByteAsLong:

				pData[i] = fRev ? Get32Rev(&uPos) : Get32(&uPos);
				break;

			case addrByteAsWord:

				pData[i] = fRev ? Get16Rev(&uPos) : Get16(&uPos);
				break;

			default:
				pData[i] = (DWORD)m_bRx[uPos++];
				break;
			}

		i++;
		}

	NextIndex();
	}

DWORD CBRMininetSerialDriver::Get32(UINT *pPos)
{
	UINT uPos = *pPos;

	UINT uEnd = uPos + 3;

	*pPos    += uEnd + 1;

	DWORD d   = 0;

	for( UINT i = uPos; i <= uEnd; i++ ) {

		d <<= 8;

		d  += (DWORD)m_bRx[i];
		}

	return d;
	}

DWORD CBRMininetSerialDriver::Get32Rev(UINT *pPos)
{
	UINT uPos = *pPos;

	UINT uEnd = uPos + 3;

	*pPos     = uEnd + 1;

	DWORD d   = 0;

	for( UINT i = uEnd; i >= uPos; i-- ) {

		d <<= 8;

		d += (DWORD)m_bRx[i];
		}

	return d;
	}

WORD CBRMininetSerialDriver::Get16(UINT *pPos)
{
	UINT uPos = *pPos;

	*pPos    += 2;

	WORD w    = ((WORD)(m_bRx[uPos])) << 8;

	return w + (WORD)m_bRx[uPos + 1];
	}

WORD CBRMininetSerialDriver::Get16Rev(UINT *pPos)
{
	UINT uPos = *pPos;

	*pPos     = uPos + 2;

	WORD w    = ((WORD)(m_bRx[uPos + 1])) << 8;

	return w + (WORD)m_bRx[uPos];
	}

// Port Access

void CBRMininetSerialDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bSend[i]);

	m_pData->Write( PCBYTE(m_bSend), m_uPtr, FOREVER );
	}

UINT CBRMininetSerialDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers
void CBRMininetSerialDriver::CheckAdd(BYTE bData)
{
	WORD wCheck = (WORD)(m_pCtx->m_bCheck << 1);

	if( wCheck & 0x100 ) {

		wCheck += 1;

		wCheck &= 0xFF;
		}

	wCheck = wCheck + bData;

	m_pCtx->m_bCheck = (BYTE)(LOBYTE(wCheck) + HIBYTE(wCheck));
	}

BYTE CBRMininetSerialDriver::CheckRead(void)
{
	if( m_pCtx->m_bCheck == STX ) {

		m_pCtx->m_bCheck = 0xFD;
		}

	return m_pCtx->m_bCheck;
	}

BOOL CBRMininetSerialDriver::CheckResponse(UINT uResult)
{
	if( uResult ) {

		if( uResult != 2 && m_bRx[1] & 0x80 && m_bRx[1] & 0xC3 ) {

			m_pCtx->m_fIndex = TRUE;

			return FALSE;
			}

		else {
			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
