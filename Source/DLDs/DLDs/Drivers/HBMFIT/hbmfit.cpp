
#include "intern.hpp"

#include "hbmfit.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HBM FIT Driver
//		

//struct FAR HBMFITCmdDef {
//	char	Com[4];
//	UINT	Table;
//	};
HBMFITCmdDef CODE_SEG CHBMFITDriver::CL[] = {	
	{"ACL",	/*Auto Calibration",*/			ACL},
	{"ADR",	/*Address",*/				ADR},
	{"ASF",	/*Filter select Limit frequencies",*/	ASF},
	{"ASS",	/*Input Signal",*/			ASS},
	{"BRK",	/*Break",*/				BRK}, // FIT
	{"CAL",	/*Calibration",*/			CAL},
	{"CBK",	/*"Coarse Break Limit",*/		CBK}, // FIT
	{"CFD",	/*"Coarse Flow Disconnect",*/		CFD}, // FIT
	{"COF",	/*Output format (at MSV?)",*/		COF},
	{"CRC",	/*Check Sum",*/				CRC},

	{"CSN",	/*"Clear Cum. Weight/Batch Ctr",*/	CSN}, // FIT
	{"CWT",	/*Next Calibration weight",*/		CWN},
	{"CWT",	/*Last Calibration weight",*/		CWL},
	{"DPW",	/*Define password 1 - 4",*/		DP1},
	{"DPW",	/*Define password 5 - 7",*/		DP2},
	{"DPW",	/*Send Password,*/			DPW},
	{"ENU",	/*Engineering unit",*/			ENU},
	{"EPT",	/*"Emptying Time",*/			EPT}, // FIT
	{"ESR",	/*Status",*/				ESR},
	{"EWT",	/*"Empty Weight",*/			EWT}, // FIT

	{"FBK",	/*"Fine Break Difference",*/		FBK}, // FIT
	{"FFD",	/*"Fine Flow Disconnect",*/		FFD}, // FIT
	{"FFM",	/*"Fine Feed Minimum",*/		FFM}, // FIT
	{"FMD",	/*Filter mode",*/			FMD},
	{"FRS",	/*"Filling Result",*/			FRS}, // FIT
	{"FWT",	/*"Filling Weight",*/			FWT}, // FIT
	{"GRU",	/*Group Address",*/			GRU},
	{"ICR",	/*Measuring rate",*/			ICR},
	{"IDN",	/*IDN  1 -  4",*/			ID0},
	{"IDN",	/*IDN  5 -  8",*/			ID1},

	{"IDN",	/*IDN  9 - 12",*/			ID2},
	{"IDN",	/*IDN 13 - 16",*/			ID3},
	{"IDN",	/*IDN 17 - 20",*/			ID4},
	{"IDN",	/*IDN 21 - 24",*/			ID5},
	{"IDN",	/*IDN 25 - 28",*/			ID6},
	{"IDN",	/*IDN 29 - 32",*/			ID7},
	{"IDN",	/*IDN 33",*/				ID8},
	{"IMD",	/*Function of inputs",*/		IMD},
	{"LDW",	/*User LC Dead Weight Zero Command",*/	LDC},
	{"LDW",	/*User LC Dead Weight Zero Value",*/	LDV},

	{"LFT",	/*Legal for trade application",*/	LFT},
	{"LIC",	/*Linearization coefficient 0",*/	LC0},
	{"LIC",	/*Linearization coefficient 1",*/	LC1},
	{"LIC",	/*Linearization coefficient 2",*/	LC2},
	{"LIC",	/*Linearization coefficient 3",*/	LC3},
	{"LIV",	/*Read Limit 1 Monitoring on/off",*/	LR1M},
	{"LIV",	/*Read Limit 1 Input Value",*/		LR1I},
	{"LIV",	/*Read Limit 1 Switch On Value",*/	LR1O},
	{"LIV",	/*Read Limit 1 Switch Off Value",*/	LR1F},
	{"LIV",	/*Read Limit 2 Monitoring on/off",*/	LR2M},

	{"LIV",	/*Read Limit 2 Input Value",*/		LR2I},
	{"LIV",	/*Read Limit 2 Switch On Value",*/	LR2O},
	{"LIV",	/*Read Limit 2 Switch Off Value",*/	LR2F},
	{"LIV",	/*Write Limit 1 Monitoring on/off",*/	LW1M},
	{"LIV",	/*Write Limit 1 Input Value",*/		LW1I},
	{"LIV",	/*Write Limit 1 Switch On Value",*/	LW1O},
	{"LIV",	/*Write Limit 1 Switch Off Value",*/	LW1F},
	{"LIV",	/*Write Limit 2 Monitoring on/off",*/	LW2M},
	{"LIV",	/*Write Limit 2 Input Value",*/		LW2I},
	{"LIV",	/*Write Limit 2 Switch On Value",*/	LW2O},

	{"LIV",	/*Write Limit 2 Switch Off Value",*/	LW2F},
	{"LIV",	/*Limit value setting",*/		LIV},
	{"LTC",	/*"Lockout Time Coarse",*/		LTC}, // FIT
	{"LTF",	/*"Lockout Time Fine",*/		LTF}, // FIT
	{"LTL",	/*"Lower Tolerance Limit",*/		LTL}, // FIT
	{"LWT",	/*User LC Dead Weight - Command",*/	LWC},
	{"LWT",	/*User LC Dead Weight - Value",*/	LWV},
	{"MAV",	/*Measured value, trigger function",*/	MAV},
	{"MSV",	/*Current measured value",*/		MSV},
	{"MTD",	/*Motion Detect - Command",*/		MTD},

	{"NDS",	/*"Number of Dosings",*/		NDS}, // FIT
	{"NOV",	/*Nominal value scaling",*/		NOV},
	{"OMD",	/*"Output Mode",*/			OMD}, // FIT
	{"OSN",	/*"Optimisation",*/			OSN}, // FIT
	{"POR",	/*Read I/O (O1,O2,I1,I2*/		POR},
	{"POR",	/*Write Output 1",*/			PW1},
	{"POR",	/*Write Output 2",*/			PW2},
	{"RES",	/*Reset",*/				RES},
	{"RFT",	/*"Residual Flow Time",*/		RFT}, // FIT
	{"RSN",	/*"Resolution",*/			RSN}, // FIT

	{"RUN",	/*"Run",*/				RUN}, // FIT
	{"SDO",	/*"State of Dosing",*/			SDO}, // FIT
	{";S",	/*Select",*/				SEL},
	{"SFA",	/*Full Scale Adjust - Command",*/	SFC},
	{"SFA",	/*Full Scale Adjust - Value",*/		SFV},
	{"SPW",	/*Password entry 1 - 4",*/		SP1},
	{"SPW",	/*Password entry 5 - 7",*/		SP2},
	{"SPW",	/*Send SP1+SP2",*/			SPW},
	{"STR",	/*Bus termination - on/off",*/		STR},
	{"STT",	/*"Stablilization Time",*/		STT}, // FIT

	{"SUM",	/*"Cumulative Weight",*/		SUM}, // FIT
	{"SYD",	/*"Systematic Difference",*/		SYD}, // FIT
	{"SZA",	/*Sensor Zero Adjust - Command",*/	SZC},
	{"SZA",	/*Sensor Zero Adjust - Value",*/	SZV},
	{"TAD",	/*"Tare Delay",*/			TAD}, // FIT
	{"TAR",	/*Taring",*/				TAR},
	{"TAS",	/*Gross/net switch-over",*/		TAS},
	{"TAV",	/*Tare value",*/			TAV},
	{"TCR",	/*Trade Counter",*/			TCR},
	{"TDD",	/*Transmit Device Data",*/		TDD},

	{"TMD",	/*"Tare Mode",*/			TMD}, // FIT
	{"TRC",	/*Read Trigger setting On/Off",*/	TR1},
	{"TRC",	/*Read Trigger setting Ext/Level",*/	TR2},
	{"TRC",	/*Read Trigger setting Level",*/	TR3},
	{"TRC",	/*Read Trigger setting Delay",*/	TR4},
	{"TRC",	/*Read Trigger setting Period",*/	TR5},
	{"TRC",	/*Write Trigger setting On/Off",*/	TW1},
	{"TRC",	/*Write Trigger setting Ext/Level",*/	TW2},
	{"TRC",	/*Write Trigger setting Level",*/	TW3},
	{"TRC",	/*Write Trigger setting Delay",*/	TW4},

	{"TRC",	/*Write Trigger setting Period",*/	TW5},
	{"TRC",	/*Send Trigger Settings",*/		TRC},
	{"UTL",	/*"Upper Tolerance Limit",*/		UTL}, // FIT
	{"ZSE",	/*Initial zero setting",*/		ZSE},
	{"ZTR",	/*Automatic zero tracking",*/		ZTR},

	{"TEX",	/*Set Text Delimiter",*/		TEX}, // not user accessible
	};

// Instantiator

clink void CreateHBMFIT(void *pData, UINT *pSize)
{
	if( !pData ) {
		
		*pSize = sizeof(CHBMFITDriver);
		
		return;
		}
		
	NewHere(pData) CHBMFITDriver;
	}

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CHBMFITDriver::CHBMFITDriver(void)
{
	m_Ident     = HBMFIT_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_LongResponseCounter = 0;
	}

// Destructor

CHBMFITDriver::~CHBMFITDriver(void)
{
	}

// Configuration

void MCALL CHBMFITDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CHBMFITDriver::CheckConfig(CSerialConfig &Config)
{
	switch( Config.m_uPhysical ) {

		case 1:
			m_uPhysical = IS232;
			break;

		case 2:
			Make422(Config, TRUE);
			m_uPhysical = IS4WIRE;
			break;

		case 4:
			Make422(Config, TRUE);
			m_uPhysical = Config.m_uPhysMode ? IS4WIRE : IS2WIRE;
			break;

		case 5:
			m_uPhysical = IS20MPORT;
			break;

		case 6:
			m_uPhysical = ISETHPORT;
			break;

		case 3:
		default:
			m_uPhysical = NOPORT;
			break;
		}
	}
	
// Management

void MCALL CHBMFITDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CHBMFITDriver::Open(void)
{
	m_pCL = (HBMFITCmdDef FAR * )CL;
	}

// Device

CCODE MCALL CHBMFITDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop     = GetByte(pData);

			m_pCtx->m_dMAVCache = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CHBMFITDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CHBMFITDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	m_PreviousAddress = m_pCtx->m_bDrop;

	BYTE bPreviousAddress = m_PreviousAddress;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = COF;
	Addr.a.m_Type   = addrLongAsLong;

	if( Read(Addr, Data, 1) == 1 ) {

		if( InvalidCOF(Data[0]) ) {

			if( SetCOF() ) { // match COF with physical port

				if( Read(Addr, Data, 1 ) != 1 ) {

					return CCODE_ERROR;
					}
				}
			}

		m_COF = Data[0];

		return 1;
		}

	m_PreviousAddress = bPreviousAddress;

	return CCODE_ERROR;
	}

CCODE MCALL CHBMFITDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	SetpItem(Addr.a.m_Offset);

	if( m_pItem == NULL ) {

		return CCODE_ERROR | CCODE_HARD;
		}

	if( NoReadTransmit( pData ) ) {

		return 1;
		}

	SelectDevice();

	UINT uResponseType = PutReadInfo();

	if( Transact( uResponseType ) ) {

		if( GetResponse( pData, uResponseType ) ) {

			if( m_pItem->Table == MAV ) { // MAV response is one-time event

				if( pData[0] & MAVNODATA ) pData[0] = m_pCtx->m_dMAVCache;

				else m_pCtx->m_dMAVCache = pData[0];
				}

			return 1;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CHBMFITDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	SetpItem( Addr.a.m_Offset );

	if( m_pItem == NULL ) {

		return CCODE_ERROR;
		}

	if( NoWriteTransmit( *pData ) ) {

		return 1;
		}

	SelectDevice();

	Dbg( 1, 0 );

	UINT uResponseType = PutWriteInfo(*pData);

	if( uResponseType != LONGRESPONSE ) {

		m_LongResponseCounter = 0;
		}

	switch( uResponseType ) {

		case RESPONSEFAIL: // bad Ascii write

			return CCODE_ERROR | CCODE_NO_RETRY;

		case LONGRESPONSE:

			if( !m_LongResponseCounter++ ) {

				if( !Transact( uResponseType ) ) {

					return CCODE_ERROR | CCODE_BUSY;
					}
				}

			if( m_LongResponseCounter > LONGTIMEOUT ) {

				m_LongResponseCounter = 0;

				if( m_pItem->Table == RES ) {

					return 1;
					}

				return CCODE_ERROR;
				}

			if( GetReply() && GetResponse(pData, LONGRESPONSE) ) {

				m_LongResponseCounter = 0;

				return 1;
				}

			else {
				return CCODE_ERROR | CCODE_BUSY;
				}

		default:
			if( Transact( uResponseType ) ) {

				if( uResponseType == NORESPONSE ) {

					return CCODE_ERROR | CCODE_NO_DATA;
					}

				if( GetResponse( pData, uResponseType ) ) {

					if( m_pItem->Table == COF ) {

						Ping(); // get new COF
						}

					return 1;
					}
				}
		}
	
	return CCODE_ERROR;
	}

UINT CHBMFITDriver::PutReadInfo(void)
{
	UINT uResponseType = READRESPONSE;

	BYTE bAddLast      = '?';

	AddCommand();

	switch( m_pItem->Table ) {

		case ENU:
		case ID0:
		case ID1:
		case ID2:
		case ID3:
		case ID4:
		case ID5:
		case ID6:
		case ID7:
		case ID8:
			uResponseType = ASCIIRESPONSE;
			break;

		case LR1M:
		case LR1I:
		case LR1O:
		case LR1F:
			AddByte('?');
			bAddLast = '1';
			uResponseType = MULTIRESPONSE;
			break;

		case LR2M:
		case LR2I:
		case LR2O:
		case LR2F:
			AddByte('?');
			bAddLast = '2';
			uResponseType = MULTIRESPONSE;
			break;

		case CWL:
		case LC0:
		case LC1:
		case LC2:
		case LC3:
		case POR:
		case PW1:
		case PW2:
		case TR1:
		case TR2:
		case TR3:
		case TR4:
		case TR5:
			uResponseType = MULTIRESPONSE;
			break;

		case MAV:
		case MSV:
			uResponseType = (m_COF == BIN4WIRE || m_COF == BIN2WIRE) ? HEXRESPONSE : READRESPONSE;

			if( m_COF == ASCNOT2WIRE && m_uPhysical == IS4WIRE ) {

				AddByte('?');

				bAddLast = '2';
				}
			break;

		default:
			break;
		}

	AddByte(bAddLast);

	return uResponseType;
	}

UINT CHBMFITDriver::PutWriteInfo(DWORD dData)
{
	UINT uPWPos = 0;

	AddCommand();

	switch( m_pItem->Table ) {

		case ENU:
			return AddENU(dData) ? WRITERESPONSE : RESPONSEFAIL;

		case DPW:
			if( !IsValidPW( &uPWPos, &(m_pCtx->m_Password[0]) ) ) {

				return RESPONSEFAIL;
				}

			AddByte('"');

			while( uPWPos < 7 && m_pCtx->m_Password[uPWPos] ) {

				AddByte( m_pCtx->m_Password[uPWPos] );

				uPWPos++;
				}

			AddByte('"');

			return WRITERESPONSE;

		case SPW:
			if( !IsValidPW( &uPWPos, &(m_SetPW[0]) ) ) {

				return RESPONSEFAIL;
				}

			AddByte('"');

			while( uPWPos < 7 && m_SetPW[uPWPos] ) {

				AddByte( m_SetPW[uPWPos] );

				uPWPos++;
				}

			AddByte('"');

			return WRITERESPONSE;

		case SFC:
		case SZC:
		case LDC:
		case LWC:
		case CAL:
		case RES:
			return LONGRESPONSE;

		case LC0:
		case LC1:
		case LC2:
		case LC3:
			AddByte( '0' + m_pItem->Table - LC0 );

			AddByte( ',' );

			PutGeneric( CheckSign(dData) );

			return WRITERESPONSE;

		case TDD:
			PutGeneric( CheckSign(dData) );

			return LONGRESPONSE;

		case TRC:
			PutGeneric( CheckSign( m_TRC[0] ? 1 : 0 ) );
			AddByte(',');
			PutGeneric( CheckSign( m_TRC[1] ? 1 : 0 ) );
			AddByte(',');
			PutGeneric( CheckSign( m_TRC[2] ) );
			AddByte(',');
			PutGeneric( CheckSign( m_TRC[3] ) );
			AddByte(',');
			PutGeneric( CheckSign( m_TRC[4] ) );

			return WRITERESPONSE;

		case LIV:
			if( dData == 1 ) {

				AddByte('1');
				AddByte(',');
				PutGeneric( CheckSign( m_LIV1[0] ) );
				AddByte(',');
				PutGeneric( CheckSign( m_LIV1[1] ) );
				AddByte(',');
				PutGeneric( CheckSign( m_LIV1[2] ) );
				AddByte(',');
				PutGeneric( CheckSign( m_LIV1[3] ) );
				}

			if( dData == 2 ) {

				AddByte('2');
				AddByte(',');
				PutGeneric( CheckSign( m_LIV2[0] ) );
				AddByte(',');
				PutGeneric( CheckSign( m_LIV2[1] ) );
				AddByte(',');
				PutGeneric( CheckSign( m_LIV2[2] ) );
				AddByte(',');
				PutGeneric( CheckSign( m_LIV2[3] ) );
				}

			return WRITERESPONSE;

		case PW2:
			AddByte( ',' ); // then fall through			
		case PW1:
			AddByte( '1' );
			return WRITERESPONSE;

		case POR:
			AddByte(dData & 1 ? '1' : '0');
			AddByte(',');
			AddByte(dData & 2 ? '1' : '0');
			return WRITERESPONSE;

		default:
			switch( GetParameterSize() ) {

				case TBOOL:
					AddByte( dData ? '1' : '0' );
					break;
					
				case TNONE:
					break;

				case TINT:
					PutGeneric( CheckSign(dData) );
					break;
				}

			return WRITERESPONSE;
		}

	return NORESPONSE;
	}

// PRIVATE METHODS

// Frame Building

void CHBMFITDriver::StartFrame(void)
{
	m_uPtr = 0;

	AddByte(';');
	}

void CHBMFITDriver::EndFrame(void)
{
	m_bTx[m_uPtr++] = ';';
	}

void CHBMFITDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CHBMFITDriver::AddData(DWORD dData, DWORD dFactor)
{
	while ( dFactor ) {

		AddByte( m_pHex[(dData/dFactor) % 10] );

		dFactor /= 10;
		}
	}

void CHBMFITDriver::AddCommand(void)
{
	StartFrame();

	PutText( m_pItem->Com );

	return;
	}
	
void CHBMFITDriver::PutText(LPCTXT pCmd)
{
	for( UINT i=0; pCmd[i]; i++ ) {

		AddByte( BYTE(pCmd[i]) );
		}
	}

// Transport Layer

BOOL CHBMFITDriver::Transact(UINT uResponseType)
{
	Send();

	if(	 uResponseType == NORESPONSE ||
		(uResponseType == WRITERESPONSE && m_uPhysical == IS2WIRE) // bus mode
		) {

		m_pData->Read(0);

		Sleep(20);

		return TRUE;
		}

	return GetReply();
	}

void CHBMFITDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CHBMFITDriver::GetReply(void)
{
	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uData;

	UINT uLFCount = 1;

	if( IsMeasurementRequest() ) {

		switch( m_COF ) {

			case BIN4WIRE:
			case BIN2WIRE:
				uLFCount = 0;
				break;

			case ASCNOT2WIRE:
				if( m_uPhysical == IS4WIRE ) {

					uLFCount = 2;
					}
			}
		}

	SetTimer( TIMEOUT );

	Dbg( 0, 0 );

	while( (uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		SetTimer(30);

		Dbg(3, LOBYTE(uData));

		m_bRx[uCount++] = LOBYTE(uData);

		if( uData == LF ) {

			if( m_bRx[0] == '?' ) {

				Dbg(0,0);
				}

			switch( uLFCount ) {

				case 0:
					if( uCount == 6 ) {

						return TRUE;
						}
					break;

				case 1:
					return TRUE;

				case 2:
					uLFCount = 1;
					uCount  = 0;
					break;
				}
			}
		}	
 
	return FALSE;
	}

BOOL CHBMFITDriver::GetResponse( PDWORD pData, UINT uResponseType )
{
	DWORD dData = 0;
	DWORD d2    = 0;

	UINT uPos   = 0;
	UINT uItem   = 0;
	UINT uT     = m_pItem->Table;
	UINT  i;

	if( m_bRx[0] == '?' ) {

		return FALSE;
		}

	switch( uResponseType ) {

		case NORESPONSE:

			return TRUE;

		case ASCIIRESPONSE:

			switch( uT ) {

				case ENU:
					break; // start at 0;

				case ID0:
				case ID1:
				case ID2:
				case ID3:
				case ID4:
				case ID5:
				case ID6:
				case ID7:
				case ID8:
					uPos = (uT - ID0) * 4;
					break;
				}

			*pData = GetAscii( &uPos );

			return TRUE;

		case WRITERESPONSE:

			return (m_COF >= BIN2WIRE) ? TRUE : (m_bRx[0] == '0');

		case MULTIRESPONSE: // keeps getting data until the desired item is reached

			switch( uT ) {

				case CWL:
					uItem = 1;
					break;

				case LR1M:
				case LR1I:
				case LR1O:
				case LR1F:
					uItem = uT - LR1M + 1; // ignore 1st item (switch #)
					break;

				case LR2M:
				case LR2I:
				case LR2O:
				case LR2F:
					uItem = uT - LR2M + 1; // ignore 1st item (switch #)
					break;

				case LC0:
				case LC1:
				case LC2:
				case LC3:
					uItem = uT - LC0;
					break;

				case POR:
				case PW1:
				case PW2:
					for( i = 0; i < 4; i++ ) {

						if( !GetGeneric(&uPos, &d2) ) {

							return FALSE;
							}

						dData <<= 1;

						dData |= d2 ? 1 : 0;
						}

					switch( uT ) {

						case POR:
							*pData = dData;
							break;

						case PW1:
							*pData = dData & 8 ? 1 : 0;
							break;

						case PW2:
							*pData = dData & 4 ? 1 : 0;
							break;
						}

					return TRUE;

				case TR1:
				case TR2:
				case TR3:
				case TR4:
				case TR5:
					uItem = uT - TR1;
					break;

				default:
					return FALSE;
				}

			for( i = 0; i <= uItem; i++ ) {

				if( !GetGeneric(&uPos, &dData) ) {

					return FALSE;
					}
				}
			break;

		case HEXRESPONSE:

			for( i = 0; i < 3; i++ ) {

				dData <<= 8;

				dData += m_bRx[i];
				}

			if( dData & 0x800000 ) {

				dData |= 0xFF000000;
				}

			break;

		default:
			if( !GetGeneric(&uPos, &dData) ) {

				return FALSE;
				}

			break;
		}

	*pData = dData;

	return TRUE;
	}

// Port Access

void CHBMFITDriver::Put(void)
{
	Dbg( 0, 0 );

	Dbg( 2, 0 );

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );
	}

UINT CHBMFITDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

void CHBMFITDriver::SelectDevice(void)
{
	m_uPtr = 0;

	AddByte(';');
	AddByte('S');
	AddByte(m_pHex[m_pCtx->m_bDrop/10]);
	AddByte(m_pHex[m_pCtx->m_bDrop%10]);
	AddByte(';');

	Put();
	}

BOOL CHBMFITDriver::SetCOF(void)
{
	DWORD Data = m_uPhysical == IS2WIRE ? ASC2WIRE : ASCNOT2WIRE;

	CAddress A;

	A.a.m_Offset = COF;
	A.a.m_Table  = addrNamed;
	A.a.m_Extra  = 0;
	A.a.m_Type   = addrLongAsLong;

	return Write(A, &Data, 1) == 1 ? TRUE : FALSE;
	}

BOOL CHBMFITDriver::InvalidCOF(DWORD dData)
{
	switch( m_uPhysical ) {

		case IS2WIRE:
			return dData != BIN2WIRE && dData != ASC2WIRE;
		
		case IS232:
		case IS4WIRE:
			return dData != BIN4WIRE && dData != ASCNOT2WIRE;
		}

	return TRUE;
	}

BOOL CHBMFITDriver::NoReadTransmit( PDWORD pData )
{
	switch( m_pItem->Table ) {

		case BRK:
		case CAL:
		case CSN:
		case DPW:
		case LIV:
		case RES:
		case RUN:
		case TAR:
		case SPW:
		case TRC:
			*pData = 0;
			return TRUE;

		case DP1:
			GetPassword( pData, &(m_pCtx->m_Password[0]), 4 );
			return TRUE;

		case DP2:
			GetPassword( pData, &(m_pCtx->m_Password[4]), 3 );
			return TRUE;

		case SP1:
			GetPassword( pData, &(m_SetPW[0]), 4 );
			return TRUE;

		case SP2:
			GetPassword( pData, &(m_SetPW[4]), 3 );
			return TRUE;

		case LW1M:
		case LW1I:
		case LW1O:
		case LW1F:
			*pData = m_LIV1[m_pItem->Table - LW1M];
			return TRUE;

		case LW2M:
		case LW2I:
		case LW2O:
		case LW2F:
			*pData = m_LIV2[m_pItem->Table - LW2M];
			return TRUE;

		case SEL:
		case TDD:
			*pData = 0xFFFFFFFF;
			return TRUE;

		case TW1:
		case TW2:
		case TW3:
		case TW4:
		case TW5:
			*pData = m_TRC[m_pItem->Table - TW1];
			return TRUE;
		}

	return FALSE;
	}

BOOL CHBMFITDriver::NoWriteTransmit( DWORD dData )
{
	switch( m_pItem->Table ) {

		case ADR:
			return dData > 31;

		case BRK:
		case CAL:
		case CSN:
		case TRC:
		case PW1:
		case PW2:
		case RES:
		case RUN:
			return !dData;

		case COF:
			return InvalidCOF(dData);

		case LIV:
			return ( dData != 1 && dData != 2 );

		case CWL:
		case ESR:
		case FRS:
		case MAV:
		case MSV:
		case NDS:
		case SDO:
		case SUM:
		case TCR:
		case TR1:
		case TR2:
		case TR3:
		case TR4:
		case TR5:
		case ID0:
		case ID1:
		case ID2:
		case ID3:
		case ID4:
		case ID5:
		case ID6:
		case ID7:
		case ID8:
			return TRUE;

		case DP1:
			StorePassword( dData, &(m_pCtx->m_Password[0]), 4 );
			return TRUE;

		case DP2:
			StorePassword( dData, &(m_pCtx->m_Password[4]), 3 );
			return TRUE;
 
		case LW1M:
		case LW1I:
		case LW1O:
		case LW1F:
			m_LIV1[m_pItem->Table - LW1M] = dData;
			return TRUE;
 
		case LW2M:
		case LW2I:
		case LW2O:
		case LW2F:
			m_LIV2[m_pItem->Table - LW2M] = dData;
			return TRUE;

		case SP1:
			StorePassword( dData, &(m_SetPW[0]), 4 );
			return TRUE;

		case SP2:
			StorePassword( dData, &(m_SetPW[4]), 3 );
			return TRUE;

		case TW1:
		case TW2:
		case TW3:
		case TW4:
		case TW5:
			m_TRC[m_pItem->Table - TW1] = dData;
			return TRUE;

		}

	return FALSE;
	}

UINT CHBMFITDriver::GetParameterSize(void)
{
	switch( m_pItem->Table ) {

		case ACL:
		case FMD:
		case IMD:
		case LFT:
		case PW1:
		case PW2:
		case STR:
		case TAS:
		case TMD:
		case TW1:
		case TW2:
		case ZTR:
			return TBOOL;

		case ADR:
		case ASF:
		case ASS:
		case CFD:
		case CBK:
		case COF:
		case CRC:
		case CWN:
		case EPT:
		case EWT:
		case FBK:
		case FFD:
		case FFM:
		case FRS:
		case FWT:
		case GRU:
		case ICR:
		case LC0:
		case LC1:
		case LC2:
		case LC3:
		case LW1M:
		case LW1I:
		case LW1O:
		case LW1F:
		case LW2M:
		case LW2I:
		case LW2O:
		case LW2F:
		case LIV:
		case LTC:
		case LTF:
		case LTL:
		case LWV:
		case LDV:
		case MTD:
		case NDS:
		case NOV:
		case OMD:
		case OSN:
		case RFT:
		case RSN:
		case SDO:
		case SEL:
		case SFV:
		case STT:
		case SUM:
		case SYD:
		case SZV:
		case TAD:
		case TAV:
		case TDD:
		case TW3:
		case TW4:
		case TW5:
		case UTL:
		case ZSE:
		case TEX:
			return TINT;

		case DPW:
		case ENU:
		case ID0:
		case ID1:
		case ID2:
		case ID3:
		case ID4:
		case ID5:
		case ID6:
		case ID7:
		case ID8:
		case SPW:
			return TSTRING;

		case BRK:
		case CAL:
		case CSN:
		case LWC:
		case LDC:
		case RES:
		case RUN:
		case SFC:
		case SZC:
		case TAR:
			break;
		}

	return TNONE;
	}

void CHBMFITDriver::SetpItem(UINT uID)
{
	m_pItem = m_pCL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == m_pItem->Table ) return;

		m_pItem++;
		}

	m_pItem = NULL;
	}

void CHBMFITDriver::PutGeneric(DWORD dData)
{
	DWORD dDivisor = 1000000000;

	UINT u;

	BOOL fFirst = FALSE;

	while( dDivisor ) {

		u = (dData/dDivisor) % 10;

		if( u || fFirst ) {

			AddByte( m_pHex[u] );

			fFirst = TRUE;
			}

		dDivisor /= 10;
		}

	if( !fFirst ) {

		AddByte('0');
		}
	}

BOOL CHBMFITDriver::GetGeneric(UINT * pPos, PDWORD pData)
{
	DWORD d = 0L;

	UINT i = *pPos;

	BOOL fNeg = FALSE;

	switch( m_bRx[i] ) {

		case '-':
			fNeg = TRUE;
			// fall through
		case '+':
		case ' ':
		case ',':
			i++;

			break;
		}

	while( m_bRx[i] >= '0' && m_bRx[i] <= '9' ) {

		d *= 10;

		d += m_bRx[i++] - '0';

		if( i > sizeof(m_bRx) ) {

			return FALSE;
			}
		}

	*pPos = i+1;

	*pData = fNeg ? -d : d;

	return TRUE;
	}

DWORD CHBMFITDriver::GetAscii(UINT * pPos)
{
	UINT uPos = *pPos;

	DWORD d = 0;

	for( UINT i = uPos; i < uPos+4; i++ ) {

		d <<= 8;

		d += m_bRx[i];
		}

	return d;
	}

void CHBMFITDriver::StorePassword(DWORD dData, PBYTE pb, UINT uCt)
{
	BYTE b;

	UINT uShift = uCt == 4 ? 24 : 16;

	UINT i = 0;

	UINT uPos = uCt;

	while( uPos ) {

		b = (dData >> uShift) & 0xFF;

		if( IsValidPWChar(b) == 2 ) {

			pb[i++] = b;
			}

		uShift -= 8;

		uPos--;
		}

	while( i < uCt ) {

		pb[i++] = 0;
		}
	}

void CHBMFITDriver::GetPassword(PDWORD pData, PBYTE pb, UINT uCt)
{
	DWORD dData = 0;

	for( UINT i = 0; i < uCt; i++ ) {

		dData = (dData << 8) + pb[i];
		}

	if( uCt == 3 ) {

		dData <<= 8;
		}

	*pData = dData;
	}

UINT CHBMFITDriver::IsValidPWChar(BYTE b)
{
	return	(b >= '0' && b <= '9') ||
		(b >= 'A' && b <= 'Z') ||
		(b >= 'a' && b <= 'z')

		? 2 : !b ? 1 : 0;
	}

BOOL CHBMFITDriver::IsValidPW(UINT * pPos, PBYTE pPW)
{
	BOOL fFirst = FALSE;
	BOOL fNull  = FALSE;
	BOOL fOK    = TRUE;

	for( UINT i = 0; fOK && (i < 7); i++ ) {

		switch( IsValidPWChar(pPW[i]) ) {

			case 1:
				fNull = fFirst; // null following some valid character
				break;

			case 2:
				fOK = !fNull; // Check if null found in middle of string

				if( !fFirst ) {

					*pPos = i;

					fFirst = TRUE;
					}
				break;

			case 0:
			default:
				fOK = FALSE;
				break;
			}
		}

	if( fOK && fFirst ) {

		return TRUE;
		}

	ClearPW(pPW);

	return FALSE;
	}

void CHBMFITDriver::ClearPW(PBYTE pPW)
{
	for( UINT i = 0; i < 7; i++ ) {

		pPW[i] = 0;
		}
	}
BOOL CHBMFITDriver::AddENU(DWORD dData)
{
	AddByte('"');

	if( !DataToASCII(dData, 4) ) {

		return FALSE;
		}

	AddByte('"');

	return TRUE;
	}

BOOL CHBMFITDriver::DataToASCII(DWORD dData, UINT uCt)
{
	BYTE b;

	UINT uShiftStart = 8 * (uCt - 1);

	while( uCt-- ) {

		b = (dData >> uShiftStart) & 0xFF;

		if( !b ) {

			b = ' ';
			}

		if( b < ' ' || b > 0x7E ) {

			return FALSE;
			}

		else {
			AddByte(b);
			}

		uShiftStart -= 8;
		}

	return TRUE;
	}

DWORD CHBMFITDriver::CheckSign(DWORD dData)
{
	if( dData & 0x80000000 ) {

		AddByte('-');

		dData = (dData ^ 0xFFFFFFFF) + 1;
		}

	else {
		if( NeedPlus() ) {

			AddByte('+');
			}
		}

	return dData;
	}

BOOL CHBMFITDriver::NeedPlus(void)
{
	switch( m_pItem->Table ) {

		case LC0:
		case LC1:
		case LC2:
		case LC3:
		case TAV:
			return TRUE;
		}

	return FALSE;
	}

BOOL CHBMFITDriver::IsMeasurementRequest(void)
{
	return (m_pItem->Table == MAV || m_pItem->Table == MSV);
	}

void CHBMFITDriver::Dbg( UINT u, DWORD d )
{
///*
	UINT i;

	switch( u ) {

		case 0:
			AfxTrace0("\r\n");
			break;

		case 1:
			AfxTrace0("\r\n****\r\n");
			break;

		case 2:
			for( i = 0; i < m_uPtr; i++ ) {
				AfxTrace1("[%2.2x]", m_bTx[i]);
				}
			break;

		case 3:
			AfxTrace1("<%2.2x>", LOBYTE(d) );
			break;

		case 4:
			AfxTrace1("%4.4x ", LOWORD(d) );
			break;

		case 5:
			AfxTrace1("%8.8lx ", d );
			break;
		}
//*/
	}

// End of File
