
#include "intern.hpp"

#include "mtsmdbus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MTS Modbus Driver
//

// Instantiator

INSTANTIATE(CMTSModbusDriver);

// Constructor

CMTSModbusDriver::CMTSModbusDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uMaxWords = 120;
	
	m_uMaxBits  = 2000;

	m_fAscii    = FALSE;

	m_pTx       = NULL;

	m_pRx       = NULL;
	}

// Destructor

CMTSModbusDriver::~CMTSModbusDriver(void)
{
	delete [] m_pTx;

	delete [] m_pRx;
	}

// Configuration

void MCALL CMTSModbusDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_fAscii = GetByte(pData);

		}
	
	AllocBuffers();
	}
	
void MCALL CMTSModbusDriver::CheckConfig(CSerialConfig &Config)
{
	if( !m_fAscii && Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}
	
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CMTSModbusDriver::Attach(IPortObject *pPort)
{
	if( m_fAscii ) {

		m_pData = MakeDoubleDataHandler();

		pPort->Bind(m_pData);
		}
	else {
		m_pData = MakeSingleDataHandler();

		pPort->Bind(m_pData);
		}
	}

void MCALL CMTSModbusDriver::Open(void)
{
	}

// Device

CCODE MCALL CMTSModbusDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop       = GetWord(pData);
			m_pCtx->m_uActualDrop = m_pCtx->m_bDrop;
			m_pCtx->m_fRLCAuto    = GetByte(pData);
			m_pCtx->m_fDisable15  = GetByte(pData);
			m_pCtx->m_fDisable16  = GetByte(pData);
			m_pCtx->m_uMax01      = GetWord(pData);
			m_pCtx->m_uMax02      = GetWord(pData);
			m_pCtx->m_uMax03      = GetWord(pData);
			m_pCtx->m_uMax04      = GetWord(pData);
			m_pCtx->m_uMax15      = GetWord(pData);
			m_pCtx->m_uMax16      = GetWord(pData);
			m_pCtx->m_uPing	      = GetWord(pData);
			m_pCtx->m_fDisable5   = GetByte(pData);
			m_pCtx->m_fDisable6   = GetByte(pData);
			m_pCtx->m_fDisableCheck = GetByte(pData);

			Limit(m_pCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pCtx->m_uMax15, 1, m_pCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pCtx->m_uMax16, 1, m_pCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMTSModbusDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMTSModbusDriver::Ping(void)
{
	if(	!m_pCtx->m_uPing ||
		!m_pCtx->m_uActualDrop ||
		m_pCtx->m_uActualDrop > 255 ||
		m_pCtx->m_bDrop > 255
		) {

		return CCODE_SUCCESS;
		}
		
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPACE_HOLD;
	Addr.a.m_Offset = m_pCtx->m_uPing;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return DoWordRead(Addr, Data, 1);

	}

CCODE MCALL CMTSModbusDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CCODE cResp = CCODE_ERROR;

	if( Addr.a.m_Table == SPACE_ADDR ) {

		*pData = m_pCtx->m_uActualDrop;

		return 1;
		}

	if( !m_pCtx->m_uActualDrop || m_pCtx->m_uActualDrop > 255 ) {

		return uCount;
		}
	
	UINT uType = Addr.a.m_Type;
	
	switch( Addr.a.m_Table ) {

		case SPACE_HOLD:

			if( uType == addrWordAsWord ) {

				cResp = DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax03));
				}

			else {

				cResp = DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));
				}

			break;

		case SPACE_ANALOG:

			if( uType == addrWordAsWord ) {

				cResp = DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax04));
				}

			else {

				cResp = DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));
				}

			break;

		case SPACE_HOLD32:
			cResp = DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));
			break;

		case SPACE_ANALOG32:
			cResp = DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));
			break;

		case SPACE_OUTPUT:
			cResp = DoBitRead (Addr, pData, min(uCount, m_pCtx->m_uMax01));
			break;

		case SPACE_INPUT:
			cResp = DoBitRead (Addr, pData, min(uCount, m_pCtx->m_uMax02));
			break;

		case SPACE_FILE:
			cResp = FileRead (Addr, pData, min(uCount, m_pCtx->m_uMax03));
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	if( cResp & CCODE_ERROR ) {

		if( m_pCtx->m_bDrop > 255 ) {

			return CCODE_ERROR | CCODE_NO_RETRY;
			}
		}

	return cResp;
	}

CCODE MCALL CMTSModbusDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CCODE cResp = CCODE_ERROR;

	if( Addr.a.m_Table == SPACE_ADDR ) {

		if( *pData < 256 && m_pCtx->m_bDrop > 255 ) {

			m_pCtx->m_uActualDrop = LOWORD(*pData);
			}

		return 1;
		}

	if( m_pCtx->m_uActualDrop > 255 ) {

		return uCount;
		}

	switch( Addr.a.m_Table ) {

		case SPACE_HOLD:

			if( Addr.a.m_Type == addrWordAsWord ) {

				cResp = DoWordWrite(Addr, pData, min(uCount, m_pCtx->m_uMax16));
				}

			else {

				cResp = DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));
				}
			break;

		case SPACE_HOLD32:
			cResp = DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));
			break;

		case SPACE_OUTPUT:
			cResp = DoBitWrite (Addr, pData, min(uCount, m_pCtx->m_uMax15));
			break;

		case SPACE_FILE:
			cResp = FileWrite (Addr, pData, min(uCount, m_pCtx->m_uMax15));
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	if( cResp & CCODE_ERROR ) {

		if( m_pCtx->m_bDrop > 255 ) {

			return CCODE_ERROR | CCODE_NO_RETRY;
			}
		}

	return cResp;
	}

// Implementation

void CMTSModbusDriver::Limit(UINT &uData, UINT uMin, UINT uMax)
{
	if( uData < uMin ) uData = uMin;
	
	if( uData > uMax ) uData = uMax;
	}

void CMTSModbusDriver::AllocBuffers(void)
{
	UINT c1 = 2 * m_uMaxWords;
	
	UINT c2 = (m_uMaxBits + 7) / 8;
	
	UINT c3 = (max(c1, c2) + 20) * (m_fAscii ? 2 : 1);

	m_uTxSize = c3;
	
	m_uRxSize = c3;
	
	m_pTx = New BYTE [ m_uTxSize ];

	m_pRx = New BYTE [ m_uRxSize ];
	}
	
BOOL CMTSModbusDriver::IsHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return TRUE;
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return TRUE;
		}
		
	return FALSE;
	}

WORD CMTSModbusDriver::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return bData - '0';
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return bData - 'A' + 10;
		}
		
	return 0;
	}

// Port Access

void CMTSModbusDriver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CMTSModbusDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CMTSModbusDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;
	
	AddByte(m_pCtx->m_uActualDrop);
	
	AddByte(bOpcode);
	}

void CMTSModbusDriver::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {
	
		m_pTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CMTSModbusDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CMTSModbusDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CMTSModbusDriver::PutFrame(void)
{
	m_pData->ClearRx();
	
	return m_fAscii ? AsciiTx() : BinaryTx();
	}

BOOL CMTSModbusDriver::GetFrame(BOOL fWrite)
{
	return m_fAscii ? AsciiRx() : BinaryRx(fWrite);
	}

BOOL CMTSModbusDriver::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

	m_pData->Write( PCBYTE(m_pTx), m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CMTSModbusDriver::BinaryRx(BOOL fWrite)
{
	UINT uPtr = 0;

	UINT uGap = 0;

	SetTimer(500);

	while( GetTimer() ) {

		UINT uByte;
		
		if( (uByte = RxByte(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

			if( uPtr <= m_uRxSize ) {
			
				uGap = 0;
				
				continue;
				}
				
			return FALSE;
			}
		
		if( ++uGap >= 5 ) {
			
			if( uPtr >= 4 ) {

				if( m_pCtx->m_fDisableCheck ) {
					
					if( uPtr >= ( fWrite ? 8 : UINT(m_pRx[2] + 5) ) ) {

						return TRUE;
						}
					}
				else {
				
					m_CRC.Preset();
				
					PBYTE p = m_pRx;
				
					UINT  n = uPtr - 2;
			
					for( UINT i = 0; i < n; i++ ) {

						m_CRC.Add(*(p++));
						}

					WORD c1 = IntelToHost(PWORD(p)[0]);
				
					WORD c2 = m_CRC.GetValue();
					
					if( c1 == c2 ) {
					
						return TRUE;
						}
					}
				}
				
			uPtr = 0;
			
			uGap = 0;
			}
		}
		
	return FALSE;
	}

BOOL CMTSModbusDriver::AsciiTx(void)
{
	BYTE bCheck = 0;
	
	TxByte(':');
		
	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		TxByte(m_pHex[m_pTx[i] / 16]);
		
		TxByte(m_pHex[m_pTx[i] % 16]);

		bCheck += m_pTx[i];
		}
		
	bCheck = BYTE(0x100 - WORD(bCheck));
	
	TxByte(m_pHex[bCheck / 16]);
	TxByte(m_pHex[bCheck % 16]);
	
	TxByte(CR);
	TxByte(LF);
	
	return TRUE;
	}

BOOL CMTSModbusDriver::AsciiRx(void)
{
	UINT uState = 0;

	UINT uPtr   = 0;

	UINT uTimer = 0;
	
	BYTE bCheck = 0;

	SetTimer(1000);
	
	while( (uTimer = GetTimer()) ) {

		UINT uByte;
		
		if( (uByte = RxByte(uTimer)) == NOTHING ) {
			
			continue;
			}
			
		switch( uState ) {
		
			case 0:
				if( uByte == ':' ) {

					uPtr   = 0;

					uState = 1;

					bCheck = 0;
					}
				break;
				
			case 1:
				if( IsHex(uByte) ) {
				
					m_pRx[uPtr] = FromHex(uByte) << 4;
				
					uState = 2;
					}
				else {
					if( uByte == CR ) {
					
						continue;
						}

					if( uByte == LF ) {

						if( bCheck == 0 || m_pCtx->m_fDisableCheck ) {

							return TRUE;
							}
						}
					
					return FALSE;
					}
				break;
				
			case 2:
				if( IsHex(uByte) ) {

					m_pRx[uPtr] |= FromHex(uByte);
					
					bCheck += m_pRx[uPtr];

					if( ++uPtr == m_uRxSize ) {
						
						return FALSE;
						}
					
					uState = 1;
					}
				else
					return FALSE;
				break;
			}
		}
		
	return FALSE;
	}

BOOL CMTSModbusDriver::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {
	
		if( !m_pCtx->m_uActualDrop ) {

			return TRUE;
			}
			
		if( GetFrame(fIgnore) ) {
			
			return CheckReply(fIgnore);
			}
		}
		
	return FALSE;
	}

BOOL CMTSModbusDriver::CheckReply(BOOL fIgnore)
{
	if( m_pRx[0] == m_pTx[0] ) {

		if( fIgnore ) {

			return TRUE;
			}
	
		if( !(m_pRx[1] & 0x80) ) {
		
			return TRUE;
			}
		}
		
	return FALSE;
	}

// Read Handlers

CCODE CMTSModbusDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(uCount);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x = PWORD(m_pRx + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CMTSModbusDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLD:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(uCount * 2);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x = PDWORD(m_pRx + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CMTSModbusDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
			
		case SPACE_OUTPUT:
			StartFrame(0x01);
			break;
			
		case SPACE_INPUT:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(Addr.a.m_Offset - 1);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_pRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CMTSModbusDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_HOLD ) {

		if( ( uCount == 1 ) && !m_pCtx->m_fDisable6 ) {
			
			StartFrame(6);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(uCount);
			
			AddByte(uCount * 2);
			
			for( UINT n = 0; n < uCount; n++ ) {
				
				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CMTSModbusDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable = Addr.a.m_Table;
	
	if( ( uTable == SPACE_HOLD32 ) || ( uTable == SPACE_HOLD ) ) {

		StartFrame(16);
		
		AddWord(Addr.a.m_Offset - 1);
		
		AddWord(uCount * 2);
		
		AddByte(uCount * 4);
		
		for( UINT n = 0; n < uCount; n++ ) {
			
			AddLong(pData[n]);
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CMTSModbusDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_OUTPUT ) {

		if( ( uCount == 1 ) && !m_pCtx->m_fDisable5 ) {
			
			StartFrame(5);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			StartFrame(15);

			AddWord(Addr.a.m_Offset - 1);

			AddWord(uCount);

			AddByte((uCount + 7) / 8);

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b);
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE	CMTSModbusDriver::FileRead(AREF Addr, PDWORD pData, UINT uCount )
{
	StartFrame( FILE_READ );
	AddByte( 7 );
	AddByte( 6 );
	AddWord(Addr.a.m_Extra);
	AddWord(Addr.a.m_Offset);
	AddWord(uCount);

	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x = PWORD(m_pRx + 5)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}
 
		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE	CMTSModbusDriver::FileWrite(AREF Addr, PDWORD pData, UINT uCount )
{
	StartFrame( FILE_WRITE );
	AddByte( 7 + (uCount*2) );
	AddByte( 6 );
	AddWord(Addr.a.m_Extra);
	AddWord(Addr.a.m_Offset);
	AddWord(uCount);
			
	for( UINT n = 0; n < uCount; n++ ) {

		AddWord(LOWORD(pData[n]));
		}

	if( Transact(TRUE) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

// End of File
