//////////////////////////////////////////////////////////////////////////
//
//  Constants
//

#define SPACE_X		0x18
#define SPACE_Y		0x19
#define SPACE_I		0x09
#define SPACE_E		0x05
#define SPACE_M		0x0D
#define SPACE_T		0x14
#define SPACE_C		0x03
#define SPACE_L		0x0C
#define	SPACE_D		0x04
#define SPACE_B		0x02
#define SPACE_R		0x12
#define SPACE_V		0x16
#define SPACE_Z		0x1A
#define SPACE_W		0x17
#define SPACE_TS	0x20
#define SPACE_TP	0x21
#define SPACE_TI	0x25
#define SPACE_CS	0x30
#define SPACE_CP	0x31
#define SPACE_CI	0x35

//////////////////////////////////////////////////////////////////////////
//
// Operations
//

#define CMD_BR	0x01
#define CMD_BW	0x02
#define CMD_WR	0x11
#define CMD_WW	0x12

//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC Base Master Driver
//

class CFam3BaseMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CFam3BaseMasterDriver(void);

		// Destructor
		~CFam3BaseMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Context
		struct CBaseCtx
		{
			BYTE m_uMode;
			UINT m_uCpu;
			};

	protected:
		// Data Members
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		BYTE	   m_bCheck;
		CBaseCtx * m_pBase;
		LPCTXT 	   m_pHex;
		
		// Implementation
		CCODE GetBits(PDWORD pData, UINT uCount);
		CCODE GetASCIIBits(PDWORD pData, UINT uCount);
		BOOL  GetBitData(PCTXT pText);
		CCODE GetWords(PDWORD pData, UINT uCount);
		CCODE GetASCIIWords(PDWORD pData, UINT uCount);
		WORD  GetWordData(PCTXT pText, UINT uCount);
		CCODE GetLongs(PDWORD pData, UINT uCount);
		CCODE GetASCIILongs(PDWORD pData, UINT uCount);
		UINT  GetSubCommand(UINT Type);
		void  SetWords(PDWORD pData, UINT uCount);
		void  SetASCIIWords(PDWORD pData, UINT uCount);
		void  SetLongs(PDWORD pData, UINT uCount);
		void  SetASCIILongs(PDWORD pData, UINT uCount);
		void  SetBits(PDWORD pData, UINT uCount);
		void  SetASCIIBits(PDWORD pData, UINT uCount);
		BOOL  IsReadOnly(UINT uTable);
					
		// Frame Building
		void AddByte(BYTE bValue);
		void AddHex(UINT uValue, UINT uMask);
		void AddDec(UINT uValue, UINT uMask);
		void AddWord(WORD Word);
		void AddLong(DWORD dwWord);
		void AddDeviceName(UINT uTable, UINT uOffset);
		void AddAsciiDeviceName(UINT uTable, UINT uOffset);
		void AddAsciiPreamble(UINT uType, BOOL fWrite);

		// Overridables
		virtual BOOL Start(void);
		virtual	void AddPreamble(UINT uType, BOOL fWrite);
		virtual void AddCpuNumber(UINT uType, BOOL fWrite);
		virtual void AddWaitTime(void);
		virtual void AddCount(UINT uType, UINT uCount, BOOL fWrite);
		virtual void AddPointCount(UINT uType, UINT uCount, BOOL fWrite);
		virtual void AddTerm(void);
		virtual void GetCount(UINT &uCount, UINT uType);
		virtual void AddData(UINT uType, PDWORD pData, UINT uCount);
		virtual void GetData(UINT uType, PDWORD pData, UINT uCount);
		virtual BOOL Transact(UINT uCmd);
	};
		

// End of File
