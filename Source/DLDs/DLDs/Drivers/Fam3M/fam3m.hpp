#include "fam3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC Serial Master Driver
//

class CFam3MasterDriver : public CFam3BaseMasterDriver
{
	public:
		// Constructor
		CFam3MasterDriver(void);

		// Destructor
		~CFam3MasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:
		// Device Context
		struct CContext : CFam3BaseMasterDriver::CBaseCtx
		{
			UINT m_uStation;
			UINT m_uWait;
			BOOL m_fCheck;
			BOOL m_fTerm;
			};

		// Data Members
		CContext * m_pCtx;
					
		// Implementation
		BOOL SendFrame(void);
		BOOL Transact(UINT uCmd);
		BOOL RecvFrame(void);
		BOOL RecvAsciiFrame(void);
		BOOL CheckFrame(UINT uCmd);

		// Overridables
		BOOL Start(void);
       		void AddPreamble(UINT uType, BOOL fWrite);
		void AddWaitTime(void);
		void AddTerm(void);
		void GetCount(UINT &uCount, UINT uType);

		// Helpers
		BYTE FromAscii(BYTE bByte);

				
	};
		

// End of File

