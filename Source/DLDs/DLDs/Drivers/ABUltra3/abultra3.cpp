
#include "intern.hpp"

#include "abultra3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Ultra3000 Driver
//

// Instantiator

INSTANTIATE(CABUltra3Driver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CABUltra3Driver::CABUltra3Driver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Destructor

CABUltra3Driver::~CABUltra3Driver(void)
{
	}

// Configuration

void MCALL CABUltra3Driver::Load(LPCBYTE pData)
{

	}
	
void MCALL CABUltra3Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);

	}
	
// Management

void MCALL CABUltra3Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CABUltra3Driver::Open(void)
{

	}

// Device

CCODE MCALL CABUltra3Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_bBroadcast = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CABUltra3Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CABUltra3Driver::Ping(void)
{
	if( m_pCtx->m_bBroadcast ) {

		return 1; // cannot read this device
		}

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Offset = OP_DRIVE1;
	Addr.a.m_Extra  = 0;

	if( Read(Addr, Data, 1) == 1 ) {

		StoreText(STOREDRIVENAME);

		Addr.a.m_Offset = OP_MODEL1;

		if( Read(Addr, Data, 1) == 1 ) {

			StoreText(STOREMOTORMODEL);

			return 1;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CABUltra3Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	BOOL fHasIndex = Addr.a.m_Table != addrNamed;

	if( NoReadTransmit( pData, Addr ) ) {

		return 1;
		}

	Dbg( 0, 0 );

	UINT uCommand  = fHasIndex ? GetCommandFromTableNumber(Addr.a.m_Table) : Addr.a.m_Offset;

	UINT uFunction = Addr.a.m_Extra;

	if( uFunction == 1 ) {

		uFunction = 0; // select read with function 1 op's not trapped by NoReadTransmit()
		}

	AddCommand( uCommand, uFunction );

	if( fHasIndex && uFunction < 0xA ) { // array index to be added for these parameter functions

		AddData( Addr.a.m_Offset, 0x10 );
		}

	if( Transact() ) {

		if( CheckException() ) {

			UINT uPosition = 0;

			if( IsTextItem(Addr.a.m_Offset, &uPosition ) ) {

				GetTextResponse(pData, uPosition);

				return 1;
				}

			GetResponse( pData );

			if( Is2ByteData(uCommand) ) {

				if( *pData & 0x8000 ) {

					*pData |= 0xFFFF8000;
					}
				}

			return 1;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CABUltra3Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	BOOL fHasIndex = Addr.a.m_Table != addrNamed;

	if( NoWriteTransmit( *pData, Addr ) ) {

		return 1;
		}

	Dbg( 1, 0 );

	UINT uCommand  = fHasIndex ? GetCommandFromTableNumber(Addr.a.m_Table) : Addr.a.m_Offset;

	UINT uFunction = Addr.a.m_Extra;

	if( uFunction == 0 ) {

		uFunction = 1; // select write with function 0 op's not trapped by NoWriteTransmit()
		}

	AddCommand( uCommand, uFunction );

	if( fHasIndex && uFunction < 0xA ) { // array index to be added for these paramter functions

		AddData( Addr.a.m_Offset, 0x10 );
		}

	AddWriteData( *pData, Addr );

	if( Transact() ) {

		CheckException(); // Set CEXC and CEXP if necessary

		return 1;
		}

	return CCODE_ERROR;
	}

// PRIVATE METHODS

// Frame Building

void CABUltra3Driver::StartFrame(void)
{
	m_uPtr = 0;
	AddByte( ':' );
	AddByte( m_pHex[m_pCtx->m_bDrop/16] );
	AddByte( m_pHex[m_pCtx->m_bDrop%16] );
	}

void CABUltra3Driver::EndFrame(void)
{
	AddCheckSum();

	AddByte( CR );
	}

void CABUltra3Driver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CABUltra3Driver::AddData(DWORD dData, DWORD dFactor)
{
	while ( dFactor ) {

		AddByte( m_pHex[(dData/dFactor) % 16] );

		dFactor /= 16;
		}
	}

void CABUltra3Driver::AddCommand( UINT uCommand, UINT uFunction )
{
	StartFrame();

	UINT uPosition = 0;

	switch( IsTextItem(uCommand, &uPosition) ) {

		case TDRIVE:
			uCommand = 6;
			break;

		case TMODEL:
			uCommand = 0xEB;
			break;

		case TWRITE:
			uCommand = uPosition;
			break;
		}

	PutGeneric( uCommand, 0x100 );

	AddByte( m_pHex[uFunction] );
	}

void CABUltra3Driver::AddWriteData(DWORD dData, CAddress Addr)
{
	switch( Addr.a.m_Extra ) {

		case 4:
		case 5:
		case 7:
			return; // no data to add, command only function codes
		}

	UINT uSize = GetDataSize(Addr);

	DWORD dFactor = 1;

	switch( uSize ) {

		case 0:
			return;

		case 0xFE:
			TransferText(&(m_dDrive[0]), 8);
			return;

		case 0xFF:
			TransferText(&(m_dModel[0]), 8);
			return;

		default:
			while( uSize > 1 ) {

				dFactor *= 0x10;

				uSize >>= 4;
				}

			PutGeneric( dData, dFactor );
			break;
		}
	}

// Transport Layer

BOOL CABUltra3Driver::Transact(void)
{
	Send();

	return m_pCtx->m_bBroadcast ? TRUE : GetReply();
	}

void CABUltra3Driver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CABUltra3Driver::GetReply(void)
{
	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uData;

	BYTE bData;

	BOOL fRestart = FALSE;

	SetTimer( TIMEOUT );

	Dbg( 0, 0 );

	while( (uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

		Dbg(3, bData);

		m_bRx[uCount++] = bData;

		switch( uCount-1 ) {

			case FCOMM1:
				bData &= 0xF7; // mask exception bit. Restart only if otherwise unequal.
				// fall through
			case FSTART:
			case FADDR1:
			case FADDR2:
			case FCOMM2:
			case FCOMM3:
			case FFUNCT:

				fRestart |= (bData != m_bTx[uCount-1]);
				break;
			}

		if( bData == CR ) {

			if( fRestart ) { // not our response

				SetTimer( TIMEOUT );

				fRestart = FALSE;

				uCount = 0;
				}

			else {
				return VerifyChecksum(uCount-3);
				}
			}

		if( uCount > sizeof(m_bRx) ) return FALSE;
		}	
 
	return FALSE;
	}

BOOL CABUltra3Driver::CheckException(void)
{
	if( m_bRx[3] >= '8' ) { // exception response

		m_dCEXC = GetGeneric( CDATA, 2 );

		m_dCEXP = 0x7FF & GetGeneric( CPARAMETER, 3 );

		return FALSE;
		}

	return TRUE;
	}

BOOL CABUltra3Driver::VerifyChecksum(UINT uCount)
{
	BYTE bCheck = 0;

	for( UINT i = 1; i < uCount; i++ ) {

		bCheck += m_bRx[i];
		}

	DWORD dCheck = GetGeneric( uCount, 2 );

	return !( (bCheck + LOBYTE(dCheck)) & 0xFF);
	}

void CABUltra3Driver::GetResponse(PDWORD pData)
{
	*pData = GetGeneric(CDATA, 0);
	}

void CABUltra3Driver::GetTextResponse(PDWORD pData, UINT uPosition)
{ // need to pack 2 Hex Ascii chars into one byte
	DWORD dData = 0;

	BYTE bData;

	for( UINT i = CDATA + (uPosition*8); i < CDATA + 8 + (uPosition*8); i += 2 ) {

		bData = (xtoin(m_bRx[i]) << 4) + xtoin(m_bRx[i+1]);

		dData = (dData << 8) + bData;
		}

	*pData = dData;
	}

// Port Access

void CABUltra3Driver::Put(void)
{
	Dbg( 2, 0 );

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );
	}

UINT CABUltra3Driver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

BOOL CABUltra3Driver::NoReadTransmit(PDWORD pData, CAddress Addr)
{
	if( m_pCtx->m_bBroadcast ) {

		return TRUE;
		}

	if( ReservedOpcode(Addr) ) {

		*pData = 0;

		return TRUE;
		}

	if( Addr.a.m_Table == addrNamed ) {

		if( Addr.a.m_Offset == CEXC ) {

			*pData = m_dCEXC;

			return TRUE;
			}

		if( Addr.a.m_Offset == CEXP ) {

			*pData = m_dCEXP;

			return TRUE;
			}

		UINT uPosition = 0;

		switch( IsTextItem(Addr.a.m_Offset, &uPosition) ) {

			case TDRIVE:

				if( Addr.a.m_Extra == FSTORE ) {

					*pData = m_dDrive[uPosition];

					return TRUE;
					}

				break;

			case TMODEL:

				if( Addr.a.m_Extra == FSTORE ) {

					*pData = m_dModel[uPosition];

					return TRUE;
					}

				break;

			case TWRITE:

				return TRUE;
			}
		}

	switch( Addr.a.m_Extra ) {

		case FWRITEWORK:

			if( InvalidRWWorking(Addr) ) {

				switch( GetDataSize(Addr) ) {

					case 0:
					case 0xFE:
					case 0xFF:
						*pData = 0; // return 0 for commands
						break;
					}

				return TRUE;
				}

			return FALSE;

		case FWRITENONV:

			switch( GetDataSize(Addr) ) {

				case 0x10:
				case 0x1000:
					*pData = LARGE_DECIMAL_0;
					break;
				}

			return TRUE;

		case FNONV2WORK:
		case FWORK2NONV:
		case FDEF2NONV:
			return TRUE;
		}

	return FALSE;
	}

BOOL CABUltra3Driver::NoWriteTransmit(DWORD dData, CAddress Addr)
{
	if( ReservedOpcode(Addr) ) {

		return TRUE;
		}

	if( Addr.a.m_Table == addrNamed ) {

		if( Addr.a.m_Offset == CEXC || Addr.a.m_Offset == CEXP ) {

			m_dCEXC = 0;

			m_dCEXP = 0;

			return TRUE;
			}

		UINT uPosition = 0;

		switch( IsTextItem(Addr.a.m_Offset, &uPosition) ) {

			case TDRIVE:

				if( Addr.a.m_Extra == FSTORE ) {

					m_dDrive[uPosition] = dData;

					return TRUE;
					}
				break;

			case TMODEL:

				if( Addr.a.m_Extra == FSTORE ) {

					m_dModel[uPosition] = dData;

					return TRUE;
					}
				break;
			}
		}

	switch( Addr.a.m_Extra ) {

		case FREADWORK:
			return InvalidRWWorking(Addr);

		case FREADNONV:
		case FREADDEF:
		case FREADMIN:
		case FREADMAX:
		case FARRAYMIN:
		case FARRAYMAX:
			return TRUE;

		case FWRITEWORK:

			switch( GetDataSize(Addr) ) { // if a command only, will send only if data != 0

				case 0:
				case 0xFE:
				case 0xFF:
					return !dData;
				}

			break;
		}

	return FALSE;
	}

void CABUltra3Driver::PutGeneric(DWORD dData, DWORD dFactor)
{
	while( dFactor ) {

		AddByte( m_pHex[(dData/dFactor) % 16] );

		dFactor /= 16;
		}
	}

DWORD CABUltra3Driver::GetGeneric(UINT uPos, UINT uCount)
{
	DWORD d   = 0L;

	UINT i    = uPos;

	BOOL Done = FALSE;

	while( !Done ) {

		d <<= 4;

		d += xtoin(m_bRx[i++]);

		if( (uCount == 0 && m_bRx[i+2] == CR) || ( uCount && (i >= (uCount + uPos)) ) ) {

			Done = TRUE;
			}
		}

	return d;
	}

BYTE CABUltra3Driver::xtoin(BYTE b)
{
	if( b >= '0' && b <= '9' )
		return (b - '0');

	if( b >= 'a' && b <= 'f' )
		return (b - 'W');

	if( b >= 'A' && b <= 'F' )
		return (b - '7');

	return 0;
	}

DWORD CABUltra3Driver::GetDataSize(CAddress Addr)
{
	if( Addr.a.m_Table == addrNamed ) {

		switch( Addr.a.m_Offset ) { // only write commands are needed here

			case 0x4:
			case 0x6A:
			case 0x6F:
			case 0x70:
			case 0x71:
			case 0x79:
			case 0xCD:
				return 0;

			case 0x17:
			case 0x1C:
			case 0x1E:
			case 0x2C:
			case 0x2E:
			case 0x2F:
			case 0x30:
			case 0x3F:
			case 0x40:
			case 0x41:
			case 0x43:
			case 0x4B:
			case 0x52:
			case 0x53:
			case 0x58:
			case 0x5A:
			case 0x61:
			case 0x6B:
			case 0x95:
			case 0xA8:
			case 0xA9:
			case 0xAA:
			case 0xAB:
			case 0xB8:
			case 0xC4:
			case 0xC5:
			case 0xCA:
			case 0xCB:
			case 0xDD:
			case 0xDE:
			case 0xDF:
			case 0xE0:
			case 0xED:
			case 0xEE:
			case 0xEF:
			case 0x103:
			case 0x10D:
			case 0x10F:
			case 0x159:
			case 0x194:
				return 0x10;

			case 0x11:
			case 0x12:
			case 0x13:
			case 0x14:
			case 0x19:
			case 0x1F:
			case 0x20:
			case 0x21:
			case 0x27:
			case 0x28:
			case 0x29:
			case 0x2D:
			case 0x31:
			case 0x37:
			case 0x38:
			case 0x39:
			case 0x3A:
			case 0x3B:
			case 0x3D:
			case 0x3E:
			case 0x42:
			case 0x44:
			case 0x45:
			case 0x46:
			case 0x47:
			case 0x49:
			case 0x4C:
			case 0x4D:
			case 0x4F:
			case 0x59:
			case 0x6C:
			case 0xE2:
			case 0xE3:
			case 0xE6:
			case 0xEC:
			case 0x100:
			case 0x101:
			case 0x102:
			case 0x118:
			case 0x119:
			case 0x15F:
				return 0x1000;

			case 0x15:
			case 0x16:
			case 0x18:
			case 0x1D:
			case 0x22:
			case 0x23:
			case 0x25:
			case 0x3C:
			case 0x5D:
			case 0x5E:
			case 0x5F:
			case 0x60:
			case 0x6D:
			case 0x6E:
			case 0xB9:
			case 0xD7:
			case 0xD8:
			case 0xD9:
			case 0xDA:
			case 0xDB:
			case 0xC6:
			case 0xC7:
			case 0xC8:
			case 0xC9:
			case 0xCC:
			case 0xE1:
			case 0xE4:
			case 0xE7:
			case 0xE8:
			case 0xE9:
			case 0xEA:
			case 0xFF:
			case 0x10E:
			case 0x11F:
			case 0x193:
				return 0x10000000;

			case OP_WDRIVE: // Text Item
				return 0xFE;

			case OP_WMODEL: // Text Item
				return 0xFF;
			}

		if( Addr.a.m_Offset >= OP_DRIVE1 && Addr.a.m_Offset <= OP_DRIVE8 && Addr.a.m_Extra == FWRITENONV ) {

			return 0xFE;
			}

		if( Addr.a.m_Offset >= OP_MODEL1 && Addr.a.m_Offset <= OP_MODEL8 && Addr.a.m_Extra == FWRITENONV ) {

			return 0xFF;
			}
		}

	else {
		switch( Addr.a.m_Table ) {

			case IOP0AE:
			case IOP0B6:
			case IOP0B7:
			case IOP0E5:
			case IOP15E:
				return 0x10;

			case IOP01A:
			case IOP01B:
			case IOP05C:
			case IOP0B4:
			case IOP0B5:
				return 0x1000;

			case IOP00D:
			case IOP00E:
			case IOP02B:
			case IOP05B:
			case IOP069:
			case IOP093:
			case IOP0AF:
			case IOP0B0:
			case IOP0B1:
			case IOP0B2:
			case IOP0B3:
			case IOP0CE:
			case IOP15D:
				return 0x10000000;
			}
		}

	return 0;
	}

void CABUltra3Driver::AddCheckSum(void)
{
	BYTE bCheck = 0;

	for( UINT i = 1; i < m_uPtr; i++ ) {

		bCheck += m_bTx[i];
		}

	bCheck = 0x100 - (bCheck & 0xFF );

	AddByte(m_pHex[ bCheck/16 ]);
	AddByte(m_pHex[ bCheck%16 ]);
	}

BOOL CABUltra3Driver::InvalidRWWorking(CAddress Addr)
{ // don't permit write with function 0, or read with function 1

	if( Addr.a.m_Table != addrNamed ) { // all indexed values have read/write working values

		return FALSE;
		}

	switch( Addr.a.m_Offset ) {

		case 0x000:
		case 0x001:
		case 0x002:
		case 0x003:
		case 0x004:
		case 0x024:
		case 0x052:
		case 0x053:
		case 0x06A:
		case 0x06F:
		case 0x070:
		case 0x071:
		case 0x073:
		case 0x074:
		case 0x075:
		case 0x076:
		case 0x077:
		case 0x079:
		case 0x07A:
		case 0x07C:
		case 0x07D:
		case 0x07E:
		case 0x07F:
		case 0x080:
		case 0x081:
		case 0x082:
		case 0x083:
		case 0x084:
		case 0x085:
		case 0x086:
		case 0x087:
		case 0x088:
		case 0x089:
		case 0x08A:
		case 0x08B:
		case 0x08D:
		case 0x08E:
		case 0x08F:
		case 0x092:
		case 0x094:
		case 0x0AC:
		case 0x0AD:
		case 0x0CD:
		case 0x108:
		case 0x179:
		case 0x18F:
		case 0x19E:
		case OP_WDRIVE:
		case OP_WMODEL:
			return TRUE; // these do not both read/write working values
		}

	return FALSE;
	}

UINT CABUltra3Driver::GetCommandFromTableNumber(UINT uTable)
{
	switch( uTable ) {

		case IOP15D: return 0x15D;
		case IOP15E: return 0x15E;
		case IOP05B: return 0x05B;
		case IOP05C: return 0x05C;
		case IOP00D: return 0x00D;
		case IOP00E: return 0x00E;
		case IOP02B: return 0x02B;
		case IOP093: return 0x093;
		case IOP01B: return 0x01B;
		case IOP01A: return 0x01A;
		case IOP0AE: return 0x0AE;
		case IOP0AF: return 0x0AF;
		case IOP0B0: return 0x0B0;
		case IOP0B1: return 0x0B1;
		case IOP0B2: return 0x0B2;
		case IOP0B3: return 0x0B3;
		case IOP0B4: return 0x0B4;
		case IOP0B5: return 0x0B5;
		case IOP0B6: return 0x0B6;
		case IOP0B7: return 0x0B7;
		case IOP0E5: return 0x0E5;
		case IOP069: return 0x069;
		case IOP0CE: return 0x0CE;
		}

	return 0;
	}

UINT CABUltra3Driver::IsTextItem(UINT uCommand, UINT * pPosition)
{
	if( uCommand >= OP_DRIVE1 && uCommand <= OP_DRIVE8) {

		*pPosition = (uCommand & 0xF) - 1;

		return TDRIVE;
		}

	if( uCommand >= OP_MODEL1 && uCommand <= OP_MODEL8 ) {

		*pPosition = (uCommand & 0xF) - 1;

		return TMODEL;
		}

	if( uCommand == OP_WDRIVE || uCommand == OP_WMODEL ) {

		*pPosition = uCommand == OP_WDRIVE ? 6 : 0xEB;

		return TWRITE;
		}

	return 0;
	}

void CABUltra3Driver::TransferText(PDWORD pTable, UINT uCount)
{ // need to unpack 1 Ascii char into 2 hex ascii digits
	UINT uShift = 32;

	for( UINT i = 0; i < uCount; i++ ) {

		uShift = 32;

		while( uShift ) {

			uShift -= 4;
 
			AddByte( (m_pHex[0xF & ((*pTable) >> uShift)]) );
			}

		pTable++;
		}
	}

BOOL CABUltra3Driver::ReservedOpcode(CAddress Addr)
{
	UINT o = Addr.a.m_Offset;

	if( Addr.a.m_Table == addrNamed ) {

		switch( o ) {

			case 0x005:

			case 0x007:
			case 0x008:
			case 0x009:
			case 0x00A:
			case 0x00B:
			case 0x00C:

			case 0x00F:
			case 0x010:

			case 0x026:

			case 0x02A:

			case 0x032:
			case 0x033:
			case 0x034:
			case 0x035:
			case 0x036:

			case 0x048:

			case 0x04A:

			case 0x054:
			case 0x055:
			case 0x056:
			case 0x057:

			case 0x062:
			case 0x063:
			case 0x064:
			case 0x065:
			case 0x066:
			case 0x067:
			case 0x068:

			case 0x072:

			case 0x078:

			case 0x07B:

			case 0x08C:

			case 0x090:
			case 0x091:

			case 0x096:
			case 0x097:
			case 0x098:
			case 0x099:
			case 0x09A:
			case 0x09B:
			case 0x09C:
			case 0x09D:
			case 0x09E:
			case 0x09F:
			case 0x0A0:
			case 0x0A1:
			case 0x0A2:
			case 0x0A3:
			case 0x0A4:
			case 0x0A5:
			case 0x0A6:
			case 0x0A7:

			case 0x0BA:
			case 0x0BB:
			case 0x0BC:
			case 0x0BD:
			case 0x0BE:
			case 0x0BF:
			case 0x0C0:
			case 0x0C1:
			case 0x0C2:
			case 0x0C3:

			case 0x0CF:
			case 0x0D0:
			case 0x0D1:
			case 0x0D2:
			case 0x0D3:
			case 0x0D4:
			case 0x0D5:
			case 0x0D6:

			case 0x0DC:

			case 0x104:
			case 0x105:
			case 0x106:
			case 0x107:

			case 0x109:

			case 0x10B:
			case 0x10C:

			case 0x110:
			case 0x111:
			case 0x112:
			case 0x113:
			case 0x114:
			case 0x115:
			case 0x116:
			case 0x117:

			case 0x11A:
			case 0x11B:
			case 0x11C:
			case 0x11D:
			case 0x11E:

			case 0x120:
			case 0x121:
			case 0x122:
			case 0x123:
			case 0x124:
			case 0x125:
			case 0x126:
			case 0x127:
			case 0x128:
			case 0x129:
			case 0x12A:
			case 0x12B:
			case 0x12C:
			case 0x12D:
			case 0x12E:
			case 0x12F:
			case 0x130:
			case 0x131:
			case 0x132:
			case 0x133:
			case 0x134:
			case 0x135:
			case 0x136:
			case 0x137:
			case 0x138:
			case 0x139:
			case 0x13A:
			case 0x13B:
			case 0x13C:
			case 0x13D:
			case 0x13E:
			case 0x13F:
			case 0x140:
			case 0x141:
			case 0x142:
			case 0x143:
			case 0x144:
			case 0x145:
			case 0x146:
			case 0x147:
			case 0x148:
			case 0x149:
			case 0x14A:
			case 0x14B:
			case 0x14C:
			case 0x14D:
			case 0x14E:
			case 0x14F:
			case 0x150:
			case 0x151:
			case 0x152:
			case 0x153:
			case 0x154:
			case 0x155:
			case 0x156:
			case 0x157:
			case 0x158:

			case 0x15A:
			case 0x15B:
			case 0x15C:

			case 0x160:
			case 0x161:
			case 0x162:
			case 0x163:
			case 0x164:
			case 0x165:
			case 0x166:
			case 0x167:
			case 0x168:
			case 0x169:
			case 0x16A:
			case 0x16B:
			case 0x16C:
			case 0x16D:
			case 0x16E:
			case 0x16F:
			case 0x170:
			case 0x171:
			case 0x172:
			case 0x173:
			case 0x174:
			case 0x175:
			case 0x176:
			case 0x177:
			case 0x178:

			case 0x17A:
			case 0x17B:
			case 0x17C:
			case 0x17D:
			case 0x17E:
			case 0x17F:
			case 0x180:
			case 0x181:
			case 0x182:
			case 0x183:
			case 0x184:
			case 0x185:
			case 0x186:
			case 0x187:
			case 0x188:
			case 0x189:
			case 0x18A:
			case 0x18B:
			case 0x18C:
			case 0x18D:
			case 0x18E:

			case 0x190:
			case 0x191:
			case 0x192:

			case 0x195:
			case 0x196:
			case 0x197:
			case 0x198:
			case 0x199:
			case 0x19A:
			case 0x19B:
			case 0x19C:
			case 0x19D:

			case 0x19F:
				return TRUE;
			}

		if(	(o > 0x19F     && o < CEXC)	 ||
			(o > CEXP      && o < OP_WDRIVE) ||
			(o > OP_DRIVE8 && o < OP_WMODEL) ||
			(o > OP_MODEL8)
			) {

			return TRUE;
			}

		return FALSE;
		}

	return Addr.a.m_Table > ILAST;
	}

void CABUltra3Driver::StoreText(BOOL fDrive)
{
	DWORD * p = fDrive ? &(m_dDrive[0]) : &(m_dModel[0]);

	for( UINT i = 0; i < 8; i++ ) {

		GetTextResponse( &(p[i]), i);
		}
	}

BOOL CABUltra3Driver::Is2ByteData(UINT uCommand)
{
	switch( uCommand ) { // 2 byte signed data opcodes

		case 0x01A:
		case 0x021:
		case 0x028:
		case 0x029:
		case 0x045:
		case 0x046:
		case 0x047:
		case 0x049:
		case 0x04C:
		case 0x04D:
		case 0x04F:
		case 0x05C:
		case 0x06C:
		case 0x07A:
		case 0x07C:
		case 0x07D:
		case 0x087:
		case 0x088:
		case 0x08A:
		case 0x08D:
		case 0x08E:
		case 0x08F:
		case 0x101:
		case 0x118:
		case 0x119:
			return TRUE;
		}

	return FALSE;
	}

void CABUltra3Driver::Dbg( UINT u, DWORD d )
{
/*	UINT i;

	switch( u ) {

		case 0:
			AfxTrace0("\r\n");
			break;

		case 1:
			AfxTrace0("\r\n****\r\n");
			break;

		case 2:
			for( i = 0; i < m_uPtr; i++ ) {
				AfxTrace1("[%2.2x]", m_bTx[i]);
				}
			break;

		case 3:
			AfxTrace1("<%2.2x>", LOBYTE(d) );
			break;

		case 4:
			AfxTrace1("%4.4x ", LOWORD(d) );
			break;

		case 5:
			AfxTrace1("%8.8lx ", d );
			break;
		}
*/	}

// End of File
