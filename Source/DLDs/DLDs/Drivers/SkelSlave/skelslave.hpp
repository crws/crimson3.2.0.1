
//////////////////////////////////////////////////////////////////////////
//
// Skeleton Slave Driver
//

class CSkeletonSlaveDriver : public CSlaveDriver
{
	public:
		// Constructor
		CSkeletonSlaveDriver(void);

		// Destructor
		~CSkeletonSlaveDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Entry Points
		DEFMETH(void) Service(void);

	protected:
		// Data Members
		UINT m_Drop;

		// Implementation
		void AcceptRequest(void);
		void ProcessRead(UINT uAddr);
		void ProcessWrite(UINT uAddr, UINT uValue);
	};

// End of File
