#include "p6k.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Parker Compumotor 6K Master TCP Driver
//

class CParker6KMasterTCPDriver : public CParker6KBaseDriver
{
	public:
		// Constructor
		CParker6KMasterTCPDriver(void);

		// Destructor
		~CParker6KMasterTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Context
		struct CContext	: CParker6KBaseDriver::CBaseCtx
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			UINT	 m_uTimeWD;
			UINT	 m_uTickWD;
			BOOL	 m_fInit;
			};

		void SetWatchdog(void);

		// Data Members
		CContext * m_pCtx;
		UINT	   m_uKeep;
					
		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Frame Building
		BOOL PutRead(UINT uOffset, UINT uType);
		BOOL PutWrite(UINT uOffset, UINT uType, PDWORD pData, BOOL fGlobal);
		void Start(void);
		void End(void);

		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(BOOL fWrite);
		BOOL Transact(BOOL fWrite);
		BOOL CheckFrame(BOOL fWrite);

	
		
	  	
	};

// End of File
