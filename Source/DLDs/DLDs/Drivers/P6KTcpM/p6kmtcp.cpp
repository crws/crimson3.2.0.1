#include "intern.hpp"

#include "p6kmtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Parker Compumotor 6K Master TCP/IP Driver
//

// Instantiator

INSTANTIATE(CParker6KMasterTCPDriver);

// Constructor

CParker6KMasterTCPDriver::CParker6KMasterTCPDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_uKeep	    =   0;
	}

// Destructor

CParker6KMasterTCPDriver::~CParker6KMasterTCPDriver(void)
{
	}

// Configuration

void MCALL CParker6KMasterTCPDriver::Load(LPCBYTE pData)
{

	}
	
// Management

void MCALL CParker6KMasterTCPDriver::Attach(IPortObject *pPort)
{
	
	}

void MCALL CParker6KMasterTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CParker6KMasterTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();
			m_pCtx->m_uTimeWD= GetByte(pData);
			m_pCtx->m_uTickWD= GetByte(pData);
			m_pCtx->m_bDevice= GetByte(pData);

			m_pCtx->m_fInit	 = FALSE;

			memset(m_pCtx->m_Arg1, 0, sizeof(m_pCtx->m_Arg1));
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CParker6KMasterTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CParker6KMasterTCPDriver::Ping(void)
{
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			m_pCtx->m_fInit = FALSE;

			return CCODE_ERROR;
			}

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = SEL_A;
		Addr.a.m_Offset = 1;
		Addr.a.m_Type   = addrRealAsReal;
		Addr.a.m_Extra  = 0;

		return Read(Addr, Data, 1);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CParker6KMasterTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_fInit ) {

		SetWatchdog();
		}

	UINT uIndex = FindIndex(Addr.a.m_Table, Addr.a.m_Offset);

	FindCmd(uIndex);

	if( m_pItem ) {

		if( m_pItem->m_Access == ACC_W ) {

			return uCount;
			}

		UINT uOffset = Addr.a.m_Offset;

		UINT uMax = m_pItem->m_Form & FORM_STR ? uCount : 1;

		if( m_pItem->m_Form == (FORM_VAR | FORM_STR) ) {

			uOffset -= 64;

			if( uOffset % 13 != 0 ) {

				return uCount;
				}

			uMax = 13;
			}

		MakeMin(uCount, uMax);
	
		if( GetArg(pData, uCount) ) {

			return uCount;
			}

		Start();

		if( !PutRead(uOffset, Addr.a.m_Type) ) {

			*pData = 0;

			return uCount;
			}

		if( Transact(FALSE) ) {

			if( GetRead(uOffset, Addr.a.m_Type, pData, uCount ) ) {

				return uCount; 
				}
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CParker6KMasterTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_fInit ) {

		SetWatchdog();
		}

	UINT uIndex = FindIndex(Addr.a.m_Table, Addr.a.m_Offset);

	FindCmd(uIndex);

	if( m_pItem ) {

		if( m_pItem->m_Access == ACC_R ) {

			return uCount;
			}

		if ( m_pItem->m_Form == FORM_ARG && pData[0] == 0 ) {

			return uCount;
			}

		UINT uOffset = Addr.a.m_Offset;

		UINT uMax = m_pItem->m_Form & FORM_STR ? uCount : 1;

		if( m_pItem->m_Form == (FORM_VAR | FORM_STR) ) {

			uOffset -= 64;

			if( uOffset % 13 != 0 ) {

				return uCount;
				}

			uMax = 13;
			}

		MakeMin(uCount, uMax);

		if( SetArg(pData, uCount) ) {

			return uCount;
			}

		Start();

		if( PutWrite(uOffset, Addr.a.m_Type, pData, Addr.a.m_Extra & 0x01) ) {

			// Always verify writes by reading back value(s) when a read response can be expected.

			if( Transact(TRUE) ) {

				if( m_pItem->m_Response != RESP_NONE && m_pItem->m_Access == ACC_RW ) { 

					PDWORD Data = PDWORD(alloca(uCount * 4));
					
					Start();
					
					if( PutRead(uOffset, Addr.a.m_Type) ) {
						
						if( Transact(FALSE) ) {

							if( GetRead(uOffset, Addr.a.m_Type, Data, uCount ) ) {

								return uCount;
								}
							}
						}
					}
				else {
				
					return uCount;
					}
				}
			}    
		}

	return CCODE_ERROR;
	}

void CParker6KMasterTCPDriver::SetWatchdog(void)
{
	if( OpenSocket() ) {

		memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

		m_uPtr = 12;

		m_bTxBuff[1] = m_pCtx->m_uTimeWD;

		m_bTxBuff[3] = m_pCtx->m_uTickWD;

		if( SendFrame() ) {

			m_pCtx->m_fInit = TRUE;

			CloseSocket(TRUE);
			}
		} 
	 }
	
 // Frame Building

BOOL CParker6KMasterTCPDriver::PutRead(UINT uOffset, UINT uType)
{
	if( !PutReadCommand( uOffset, uType ) ) {
		
		return FALSE;
		}

	End();

	return TRUE;
	}

BOOL CParker6KMasterTCPDriver::PutWrite(UINT uOffset, UINT uType, PDWORD pData, BOOL fGlobal)
{
	if( !PutWriteCommand(uOffset, uType, pData, fGlobal) ) {

		return FALSE;
		}
	
	End();

	return TRUE;
	}

void CParker6KMasterTCPDriver::Start(void)
{
	m_uPtr = 0;

	PutChar('!');
	}

void CParker6KMasterTCPDriver::End(void)
{
    	PutChar( CR );

	}

// Socket Management

BOOL CParker6KMasterTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CParker6KMasterTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_fInit ? m_pCtx->m_uPort : 5004;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CParker6KMasterTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CParker6KMasterTCPDriver::SendFrame(void)
{	
	UINT uSize = m_uPtr;

	/*for( UINT u = 0; u < m_uPtr; u++ ) {

		AfxTrace("%c", m_bTxBuff[u]);
		}*/

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CParker6KMasterTCPDriver::RecvFrame(BOOL fWrite)
{
	UINT uPtr   = 0;

	UINT uBegin = 0;

	UINT uSize  = 0;
	
	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	//AfxTrace("\nRX : ");

	SetTimer(fWrite ? 100 : m_pCtx->m_uTime2 );

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( m_pItem ) {	 

			if( m_pItem->m_Response == RESP_NONE ) {

				Sleep(20);

				return TRUE;
				}
			}

		if( uSize ) {

			/*for( UINT i = uPtr; i < uPtr + uSize; i++ ) {

				AfxTrace("%c", m_bRxBuff[i]);
				}*/

			uPtr += uSize;

			if( uPtr >= sizeof(m_bRxBuff) ) {

				return FALSE;
				}
		
			for ( UINT u = 0; u < uPtr; u++ ) {

				if( !uBegin ) {

					if( m_bRxBuff[u] == '*' ) {
			
						uBegin = u + 1;
						}
					else {
						continue;
						}
					}

				if( m_bRxBuff[u] == CR ) {

					memcpy(m_bRxBuff, m_bRxBuff + uBegin, u - uBegin + 1);

					return TRUE;
					}
				}
					
			if( !uBegin ) {

				uPtr = 0;
				}
			}

		if( !CheckSocket() ) {

			return FALSE;
			}
		}

	return fWrite;
	}

BOOL CParker6KMasterTCPDriver::Transact(BOOL fWrite)
{
	if( OpenSocket() ) {

		if( SendFrame() && RecvFrame(fWrite) ) {

			return CheckFrame(fWrite);
			}

		CloseSocket(TRUE);
		}

	return FALSE;
	}

BOOL CParker6KMasterTCPDriver::CheckFrame(BOOL fWrite)
{
	if( m_pItem->m_Response == RESP_NONE || fWrite ) {
		
		return TRUE;
		}

	if( !memcmp(m_bTxBuff + 1, m_bRxBuff, m_uPtr - 2) )  {

		return TRUE;
		}

	return FALSE;
	}

// End of File
