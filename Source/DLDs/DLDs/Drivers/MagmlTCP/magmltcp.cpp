#include "intern.hpp"

#include "magmltcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN Driver
//

// Instantiator

INSTANTIATE(CMagMLANTCPDriver);

// Constructor

CMagMLANTCPDriver::CMagMLANTCPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CMagMLANTCPDriver::~CMagMLANTCPDriver(void)
{
	}

// Configuration

void  MCALL CMagMLANTCPDriver::Load(LPCBYTE pData)
{
	}

// Management

void  MCALL CMagMLANTCPDriver::Attach(IPortObject *pPort)
{
	}

void  MCALL CMagMLANTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CMagMLANTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_bBWF    = 0;
			m_pCtx->m_bSS0    = 0;
			m_pCtx->m_dRC     = 0L;
			m_pCtx->m_dWO     = 0L;
			m_pCtx->m_dOP     = 0L;
			m_pCtx->m_dSet[0] = READSET;
			m_pCtx->m_wBWW    = 0;
			m_pCtx->m_wVolt   = 0;
			m_pCtx->m_dNAK    = 0;

			memset( m_pCtx->m_dTOTR, 0, sizeof(m_pCtx->m_dTOTR) );

			for( UINT i = 1; i < 28; i++ ) m_pCtx->m_dSet[i] = 0;

			m_pCtx->m_IP		= GetAddr(pData);

			m_pCtx->m_bDrop		= GetByte(pData);

			m_pCtx->m_uPort		= GetWord(pData);

			m_pCtx->m_fKeep		= GetByte(pData);

			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);

			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CMagMLANTCPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry

CCODE MCALL CMagMLANTCPDriver::Ping(void)
{
	if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( OpenSocket() ) {

			DWORD    Data[1];

			CAddress Addr;

			Addr.a.m_Table  = SPTYP;
			Addr.a.m_Offset = 1;
			Addr.a.m_Type   = addrWordAsWord;
			Addr.a.m_Extra  = 0;

			if( Read(Addr, Data, 1) == 1 ) return 1;

			CloseSocket(TRUE);
			}
		}

	return CCODE_ERROR;
	}

BYTE CMagMLANTCPDriver::GetDevDrop(void)
{
	return m_pCtx->m_bDrop;
	}

// Transport Layer

BOOL CMagMLANTCPDriver::Transact(BOOL fIsWrite)
{
	if( !CheckSocket() ) return FALSE;

	EndFrame();

	return Send() && GetReply(fIsWrite);
	}

BOOL CMagMLANTCPDriver::Send(void)
{
	UINT uSize = m_uPtr;

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1(".%2.2x.", m_bTx[k] );

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) return TRUE;
		}

	return FALSE;
	}

BOOL CMagMLANTCPDriver::GetReply(BOOL fIsWrite)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr   = 0;

	UINT uSize  = 0;

	UINT uEnd   = fIsWrite ? 4 : RspFrame[RSPSIZE];

	BOOL fSSS   = m_bTx[1] == WSSS;

	UINT uOKRsp = !fIsWrite ? m_bTx[1] : fSSS && !m_bTx[2] ? m_bTx[1] : 48;

	UINT uNoTotal = (m_bTx[1] == RTOTR || m_bTx[1] == RTOTS) ? 2 * m_bTx[1] : 0;

//**/	AfxTrace0("\r\n"); 

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

//**/			for( UINT k = uPtr; k < uPtr + uSize; k++ ) AfxTrace1("-%2.2x-", m_bRx[k] );

			uPtr += uSize;

			if( uPtr > RspFrame[RSPSIZE] ) return FALSE;

			if( m_bTx[0] && m_bRx[0] != m_bTx[0] ) return FALSE; // bad address

			if( uPtr >= 4 ) {

				if( !fIsWrite && m_bRx[1] == 48 && m_bRx[2] == NAK ) return FALSE;
				}

			if( uNoTotal && uPtr >= 3 && m_bRx[1] == uNoTotal ) return TRUE;

			if( uPtr >= uEnd ) {

				if( m_bRx[1] != uOKRsp ) return FALSE;

				return !fIsWrite || m_bRx[2] == ACK || fSSS;
				}

			continue;
			}

		if( !CheckSocket() ) return FALSE;

		Sleep(10);
		}

	return FALSE;
	}

// Socket Management

BOOL CMagMLANTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CMagMLANTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CMagMLANTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
