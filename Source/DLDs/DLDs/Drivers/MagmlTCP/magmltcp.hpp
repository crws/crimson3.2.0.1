
#include "magmbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN TCP Driver
//

class CMagMLANTCPDriver : public CMagMLANBase
{
	public:
		// Constructor
		CMagMLANTCPDriver(void);

		// Destructor
		~CMagMLANTCPDriver(void);
	
		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry
		DEFMETH(CCODE) Ping(void);

	protected:
		// Device Data
		struct CContext : CMagMLANBase::CBaseCtx
		{
			// Metas
			DWORD	 m_IP;
			BYTE	 m_bDrop;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;

			// Device Storage
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			};

		// Data Members
		CContext *m_pCtx;

		UINT	  m_uKeep;

		// Drop Number
		BYTE	GetDevDrop(void);

		// Transport Layer
		BOOL	Send(void);
		BOOL	Transact(BOOL fIsWrite);
		BOOL	GetReply(BOOL fIsWrite);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
	};

// End of File
