

#include "intern.hpp"

#include "yaskaws7.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 Driver
//

// Instantiator

INSTANTIATE(CYaskawS7Driver);

// Constructor

CYaskawS7Driver::CYaskawS7Driver(void)
{
	m_Ident     = DRIVER_ID;
	}

// Destructor

CYaskawS7Driver::~CYaskawS7Driver(void)
{
	}

// Configuration

void MCALL CYaskawS7Driver::Load(LPCBYTE pData)
{
	}
	
void MCALL CYaskawS7Driver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) Config.m_uFlags |= flagFastRx;

	Make485(Config, TRUE);
	}
	
// Management

void MCALL CYaskawS7Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CYaskawS7Driver::Open(void)
{
	}

// Device

CCODE MCALL CYaskawS7Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_EC	= 0;
			m_pCtx->m_EV	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskawS7Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskawS7Driver::Ping(void)
{
	return LoopBack();
	}

CCODE MCALL CYaskawS7Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_bDrop ) return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;

	if( Addr.a.m_Type == addrBitAsBit ) { // Write only

		pData[0] = 0;

		return 1;
		}

	switch( Addr.a.m_Table ) {

		case SPEC:	*pData = m_pCtx->m_EC;	return 1;
		case SPEV:	*pData = m_pCtx->m_EV;	return 1;
		}

	CAddress A;

	A.a.m_Table = Addr.a.m_Table;
	A.a.m_Extra = 0;
	A.a.m_Type  = Addr.a.m_Type;

	A.a.m_Offset = SetAddress(Addr, &uCount);

	if( A.a.m_Offset || Addr.a.m_Table == SPDA ) {

		switch( Addr.a.m_Type ) {

			case addrWordAsWord:	return DoWordRead(A, pData, min(uCount, 8));

			case addrWordAsReal:
			case addrWordAsLong:	return DoLongRead(A, pData, min(uCount, 4));
			}
		}

	return uCount; // unspecified parameters
	}

CCODE MCALL CYaskawS7Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPEC:
		case SPEV:
			m_pCtx->m_EC = 0;
			m_pCtx->m_EV = 0;
			return 1;
		}

	CAddress A;

	A.a.m_Table = Addr.a.m_Table;
	A.a.m_Extra = 0;
	A.a.m_Type  = Addr.a.m_Type;

	A.a.m_Offset = SetAddress(Addr, &uCount);

	if( A.a.m_Offset || Addr.a.m_Table == SPDA ) {

		switch( A.a.m_Type) {

			case addrBitAsBit:
			case addrWordAsWord:	return DoWordWrite(A, pData, min(uCount, 16));

			case addrRealAsReal:
			case addrLongAsLong:	return DoLongWrite(A, pData, min(uCount,  8));
			}
		}

	return uCount; // unspecified parameters
	}

// Implementation

// Frame Building

void CYaskawS7Driver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;
	
	AddByte(m_pCtx->m_bDrop);
	
	AddByte(bOpcode);
	}

void CYaskawS7Driver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) m_bTx[m_uPtr++] = bData;
	}

void CYaskawS7Driver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CYaskawS7Driver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CYaskawS7Driver::PutFrame(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_bTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CYaskawS7Driver::GetFrame(BOOL fWrite)
{
	UINT uPtr = 0;

	UINT uGap = 0;

	SetTimer(500);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		UINT uByte;
		
		if( (uByte = m_pData->Read(5)) < NOTHING ) {

//**/			AfxTrace1("<%2.2x>", uByte);

			m_bRx[uPtr++] = uByte;

			if( uPtr <= sizeof(m_bRx) ) {
			
				uGap = 0;
				
				continue;
				}
				
			return FALSE;
			}
		
		if( ++uGap >= 5 ) {
			
			if( uPtr >= 4 ) {

				if( m_bRx[0] == m_bTx[0] ) { // process, if our response
				
					m_CRC.Preset();
				
					PBYTE p = m_bRx;
				
					UINT  n = uPtr - 2;
			
					for( UINT i = 0; i < n; i++ ) m_CRC.Add(*(p++));

					WORD c1 = IntelToHost(PU2(p)[0]);
				
					WORD c2 = m_CRC.GetValue();
					
					if( c1 == c2 ) return c1 == c2;
					}
				
				uPtr = 0;
			
				uGap = 0;
				}
			}
		}

	return FALSE;
	}

BOOL CYaskawS7Driver::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {
	
		if( !m_pCtx->m_bDrop ) { 

			while( m_pData->Read(0) < NOTHING );

			return TRUE;
			}
			
		if( GetFrame(fIgnore) ) {
			
			return CheckReply(fIgnore);
			}
		}
		
	return FALSE;
	}

BOOL CYaskawS7Driver::CheckReply(BOOL fIgnore)
{
	if( fIgnore ) return TRUE;
	
	return !(m_bRx[1] & 0x80);
	}

// Read Handlers

CCODE CYaskawS7Driver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(3);
		
	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount);

	if( Transact(FALSE) ) {

		uCount = min( uCount, m_bRx[2]/sizeof(WORD) );

		PU2 p = PU2(&m_bRx[3]);
		
		for( UINT n = 0; n < uCount; n++, p++ ) {
		
			WORD x = *p;
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CYaskawS7Driver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(3);
		
	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount * 2);

	if( Transact(FALSE) ) {

		uCount = min(uCount, (m_bRx[2]+2)/sizeof(DWORD));

		PU4 p = PU4(&m_bRx[3]);
		
		for( UINT n = 0; n < uCount; n++, p++ ) {
		
			DWORD x = *p;
			
			pData[n] = MotorToHost(x);
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CYaskawS7Driver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uType  = Addr.a.m_Type;

	BOOL fIsBit = uType == addrBitAsBit;

	BOOL fSingle = fIsBit || (uType == addrWordAsWord && uCount == 1);

	StartFrame(fSingle ? 6 : 0x10);

	AddWord(Addr.a.m_Offset);

	if( !fSingle ) {

		AddWord(uCount);

		AddByte(uCount*2);
		}

	else {
		if( fIsBit ) {

			if( *pData ) {

				*pData = 0;

				uCount = 1;
				}

			else return 1;
			}
		}

	for(UINT n = 0; n < uCount; n++ ) AddWord(LOWORD(pData[n]));

	return Transact(TRUE) ? uCount : CCODE_ERROR;
	}

CCODE CYaskawS7Driver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(0x10);

	AddWord(Addr.a.m_Offset);

	AddWord(uCount * 2);

	AddByte(uCount*4);

	for(UINT n = 0; n < uCount; n++ ) AddLong(pData[n]);

	return Transact(TRUE) ? uCount : CCODE_ERROR;
	}

// Connected
CCODE CYaskawS7Driver::LoopBack(void)
{
	m_uPtr = 0;

	AddByte(m_pCtx->m_bDrop);
	AddByte(0x8);
	AddWord(0);

	WORD wCheck = HostToMotor((WORD)0xA537);

	AddWord(wCheck);

	if( Transact(FALSE) ) {

		PU2 p = PU2(&m_bRx[2]);

		WORD x = (WORD)MotorToHost(*p);

		WORD y = (WORD)MotorToHost(p[1]);

		if( !x && y == wCheck ) return 1;
		}

	return CCODE_ERROR;
	}

UINT  CYaskawS7Driver::SetAddress(AREF Addr, UINT * pCount)
{
#define AMAX	12

	UINT	o = Addr.a.m_Offset; // config address

	UINT	b[AMAX];	// base modbus address
	UINT	lo[AMAX];	// low config address for range
	UINT	hi[AMAX];	// high config address for range

	for( UINT c = 0; c < AMAX; c++ ) { // Preset compare arrays

		b[c]  = 0;
		lo[c] = (c * 100) + 101;
		hi[c] = lo[c] + 98;
		}

	c = 0;

	switch( Addr.a.m_Table ) {

		case SPDA:
			return Addr.a.m_Offset;

		case SPEN:
		case SPAC:
			*pCount = 1;
			return Addr.a.m_Table == SPEN ? 0x900 : 0x910;

		case SPA:
			b[c]	= 0x100;
			lo[c]	= 100;
			hi[c]	= 105;
			b[++c]	= 0x106;
			break;

		case SPB:
			b[c]	= 0x180;
			hi[c]	= 108;
			b[++c]	= 0x189;
			hi[c]	= 208;
			b[++c]	= 0x191;
			hi[c]	= 314;
			b[++c]	= 0x1A3;
			hi[c]	= 402;
			b[++c]	= 0x1A5;
			hi[c]	= 517;
			b[++c]	= 0x1DC;
			lo[c]	= 518;
			hi[c]	= 519;
			b[++c]	= 0x1B6;
			lo[c]	= 601;
			hi[c]	= 604;
			b[++c]	= 0x1CA;
			lo[c]	= 701;
			hi[c]	= 702;
			b[++c]	= 0x1CC;
			lo[c]	= 801;
			hi[c]	= 806;
			b[++c]	= 0x1DA;
			lo[c]	= 901;
			break;

		case SPC:
			b[c]	= 0x200;
			hi[c]	= 111;
			b[++c]	= 0x20B;
			hi[c]	= 204;
			b[++c]	= 0x20F;
			hi[c]	= 305;
			b[++c]	= 0x215;
			hi[c]	= 405;
			b[++c]	= 0x21B;
			hi[c]	= 508;
			b[++c]	= 0x223;
			break;

		case SPD:
			b[c]	= 0x280; // 101 - 109
			hi[c]	= 109;
			b[++c]	= 0x28B; // 110 - 117
			lo[c]	= 110;
			hi[c]	= 117;
			b[++c]	= 0x289; // 201 - 202
			lo[c]	= 201;
			hi[c]	= 202;
			b[++c]	= 0x293; // 203
			lo[c]	= 203;
			hi[c]	= 203;
			b[++c]	= 0x294; // 301 - 304
			lo[c]	= 301;
			hi[c]	= 304;
			b[++c]	= 0x298; // 401 - 402
			lo[c]	= 401;
			hi[c]	= 402;
			b[++c]	= 0x29A; // 501 - 506
			lo[c]	= 501;
			hi[c]	= 506;
			b[++c]	= 0x2A0; // 601 - 606
			lo[c]	= 601;
			break;

		case SPE:
			b[c]	= 0x300;
			hi[c]	= 113;
			b[++c]	= 0x30E;
			hi[c]	= 211;
			b[++c]	= 0x328;
			lo[c]	= 212;
			hi[c]	= 212;
			b[++c]	= 0x319;
			lo[c]	= 301;
			hi[c]	= 308;
			b[++c]	= 0x321;
			lo[c]	= 401;
			break;

		case SPF:
			b[c]	= 0x380;
			hi[c]	= 114;
			b[++c]	= 0x38F;
			hi[c]	= 201;
			b[++c]	= 0x390;
			hi[c]	= 301;
			b[++c]	= 0x391;
			hi[c]	= 408;
			b[++c]	= 0x399;
			hi[c]	= 509;
			b[++c]	= 0x3A2;
			break;

		case SPH:
			b[c]	= 0x400;
			hi[c]	= 106;
			b[++c]	= 0x40B;
			hi[c]	= 205;
			b[++c]	= 0x410;
			hi[c]	= 312;
			b[++c]	= 0x41D;
			hi[c]	= 408;
			b[++c]	= 0x425;
			hi[c]	= 507;
			b[++c]	= 0x42C;
			break;

		case SPL:
			b[c]	= 0x480;
			hi[c]	= 105;
			b[++c]	= 0x485;
			hi[c]	= 208;
			b[++c]	= 0x48F;
			hi[c]	= 310;
			b[++c]	= 0x4C7;
			lo[c]	= 311;
			hi[c]	= 312;
			b[++c]	= 0x499;
			lo[c]	= 401;
			hi[c]	= 405;
			b[++c]	= 0x4C2;
			lo[c]	= 406;
			hi[c]	= 406;
			b[++c]	= 0x49E;
			lo[c]	= 501;
			hi[c]	= 502;
			b[++c]	= 0x4A1;
			lo[c]	= 601;
			hi[c]	= 606;
			b[++c]	= 0x4A7;
			lo[c]	= 701;
			hi[c]	= 706;
			b[++c]	= 0x4C9;
			lo[c]	= 707;
			hi[c]	= 707;
			b[++c]	= 0x4AD;
			lo[c]	= 801;
			break;

		case SPN:
			b[c]	= 0x580;
			hi[c]	= 102;
			b[++c]	= 0x584;
			hi[c]	= 203;
			b[++c]	= 0x588;
			break;

		case SPO:
			b[c]	= 0x500;
			hi[c]	= 105;
			b[++c]	= 0x505;
			hi[c]	= 214;
			b[++c]	= 0x515;
			break;

		case SPP:
			b[c]	= 0x600;
			hi[c]	= 110;
			b[++c]	= 0x60A;
			hi[c]	= 210;
			b[++c]	= 0x614;
			break;

		case SPT:
			b[c]	= 0x700;
			lo[c]	= 100;
			hi[c]	= 108;
			break;

		case SPU:
			b[c]	= 0x40;
			hi[c]	= 130;
			b[++c]	= 0x3C;
			lo[c]	= 131;
			hi[c]	= 131;
			b[++c]	= 0x5F;
			lo[c]	= 132;
			hi[c]	= 145;
			b[++c]	= 0x720;
			lo[c]	= 190;
			hi[c]	= 199;
			b[++c]	= 0x80;
			lo[c]	= 201;
			hi[c]	= 214;
			b[++c]	= 0x90;
			lo[c]	= 301;
			hi[c]	= 308;
			b[++c]	= 0x804;
			lo[c]	= 309;
			hi[c]	= 314;
			b[++c]	= 0x80E;
			lo[c]	= 315;
			break;
		}

	for( UINT i = 0; i <= c; i++ ) {

		if( o >= lo[i] && o <= hi[i] ) {

			if( o + *pCount - 1 > hi[i] ) *pCount = hi[i] - o + 1;

			return b[i] + o - lo[i];
			}

		if( o > hi[i] && o < lo[i+1] ) {

			*pCount = min( *pCount, lo[i+1] - o );

			return 0; // in between ranges
			}
		}

	return 0;
	}

// End of File
