
#include "..\ciasdos\ciasdo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CBaldorSDOSlave;

//////////////////////////////////////////////////////////////////////////
//
// Baldor CANOpen SDO Slave Driver
//

class CBaldorSDOSlave : public CCANOpenSDOSlave
{
	public:
		// Constructor
		CBaldorSDOSlave(void);

		// Destructor
		~CBaldorSDOSlave(void);

	protected:
		// Device Profile Support
		DWORD GetDeviceProfile(void);
	};

// End of File
