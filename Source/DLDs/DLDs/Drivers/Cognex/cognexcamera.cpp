#include "intern.hpp"

#include "cognexcamera.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cognex Camera Driver
//

// Timeouts

static UINT const timeSendTimeout  = 5000;

static UINT const timeSendDelay	   = 10;

static UINT const timeRecvDelay	   = 10;

static UINT const timeBuffDelay    = 100;

// Instantiator

INSTANTIATE(CCognexCamera);

// Constructor

CCognexCamera::CCognexCamera(void)
{
	m_Ident = DRIVER_ID;

	m_pCtx = NULL;

	m_uKeep = 0;

	m_pHead = NULL;
	
	m_pTail = NULL;
	}

// Destructor

CCognexCamera::~CCognexCamera(void)
{
	m_pDisp->Release();
	}

void MCALL CCognexCamera::Load(LPCBYTE pData)
{
	m_pHelper->MoreHelp(IDH_DISPLAY, (void **) &m_pDisp);
	}

// Device

CCODE MCALL CCognexCamera::DeviceOpen(IDevice *pDevice)
{
	CCameraDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234) {

			m_pCtx = new CContext;

			m_pCtx->m_uDevice  = GetWord(pData);
			m_pCtx->m_IP	   = GetAddr(pData);
			m_pCtx->m_uPort	   = GetWord(pData);
			m_pCtx->m_fKeep    = TRUE;
			m_pCtx->m_fPing    = TRUE;
			m_pCtx->m_uTime1   = 2000;
			m_pCtx->m_uTime2   = 1000;
			m_pCtx->m_uTime3   = 500;
			m_pCtx->m_uTime4   = GetWord(pData);
			m_pCtx->m_uLast3   = GetTickCount();
			m_pCtx->m_uLast4   = GetTickCount();
			m_pCtx->m_pSock	   = NULL;
			m_pCtx->m_pData    = NULL;
			m_pCtx->m_fCmdSent = FALSE;
			m_pCtx->m_fSession = FALSE;
			m_pCtx->m_uFrame   = 0;

			m_pCtx->m_pUser = GetString(pData);
			m_pCtx->m_pPass = GetString(pData);

			memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CCognexCamera::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if ( !m_pCtx->m_fKeep || m_uKeep > 4) {
			
			CloseSocket(FALSE);
			}
		}
	else {
		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		CloseSocket(FALSE);

		free(m_pCtx->m_pUser);

		free(m_pCtx->m_pPass);

		delete m_pCtx->m_pData;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CCommsDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CCognexCamera::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CCognexCamera::ReadImage(PBYTE &pData)
{
	if( OpenSocket() ) {

		if( GetImage(pData) ) {

			m_pCtx->m_uLast4 = GetTickCount();

			return CCODE_SUCCESS;
			}

		if( Ping() == CCODE_SUCCESS ) {

			UINT dt = GetTickCount() - m_pCtx->m_uLast4;

			UINT tt = ToTicks(m_pCtx->m_uTime4 * 1000);

			if( !tt || dt < tt ) {

				pData = NULL;
				
				return CCODE_SUCCESS;
				}
			}
		}

	CloseSocket(FALSE);

	EndSession(m_pCtx);

	return CCODE_ERROR;
	}

void MCALL CCognexCamera::SwapImage(PBYTE pData)
{
	delete m_pCtx->m_pData;

	m_pCtx->m_uInfo[0] = GetTimeStamp();

	m_pCtx->m_uInfo[1] = ((BITMAP_RLC *) pData)->Frame;

	m_pCtx->m_pData = pData;
	}

void MCALL CCognexCamera::KillImage(void)
{
	if( m_pCtx->m_pData ) {

		memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

		delete m_pCtx->m_pData;

		m_pCtx->m_pData = NULL;
		}
	}

PCBYTE MCALL CCognexCamera::GetData(UINT uDevice)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			return pCtx->m_pData;
			}
		}

	return NULL;
	}

UINT MCALL CCognexCamera::GetInfo(UINT uDevice, UINT uParam)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			if( uParam < elements(pCtx->m_uInfo) ) {

				return pCtx->m_uInfo[uParam];
				}
			}
		}

	return 0;
	}

// Socket Management

BOOL CCognexCamera::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCognexCamera::OpenSocket(void)
{
	if( CheckSocket() ) {
		
		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast3;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP = (IPADDR const &) m_pCtx->m_IP;
		
		UINT uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					m_pCtx->m_uLast4 = GetTickCount();

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CCognexCamera::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast3 = GetTickCount();

		m_uKeep--;
		}
	}

// Implementation

BOOL CCognexCamera::GetImage(PBYTE &pData)
{
	PBYTE pBmp = NULL;

	BYTE CheckSum[6];

	HEADER_BM Header = { };

	BYTE HeaderBuf[2 * sizeof(Header)] = { };

	if( !Login(m_pCtx) ) {
			
		return FALSE;
		}

	if( !SendCommand(m_pCtx) ) {

		return FALSE;
		}

	if( m_pCtx->m_fSession && m_pCtx->m_fCmdSent ) {

		UINT uBytes = m_pCtx->m_uImgSize;

		// Calculate space needed for ASCII encoded data + CRLF for every 80 characters
		UINT uNeed = uBytes + (2 * (uBytes / 80)) + ((uBytes % 80) ? 2 : 0) - sizeof(HeaderBuf);

		if( !Read(HeaderBuf, sizeof(HeaderBuf)) || !ReadHeader(HeaderBuf, Header) ) {

			return FALSE;
			}

		if( Header.cx > m_pDisp->GetCx() || Header.cy > m_pDisp->GetCy() ) {

			// The image is too big to be displayed, so we need to
			// consume all of the image data from the TCP stream.

			BYTE BitBucket[1024];

			for( int n = uNeed; n > 0; n -= sizeof(BitBucket) ) {

				if( !Read(BitBucket, min(n, sizeof(BitBucket))) ) {

					return FALSE;
					}
				}

			pData = CreateDummy(Header);

			return TRUE;
			}

		PBYTE pEncodedImg = new BYTE[uNeed];

		if( !Read(pEncodedImg, uNeed) || !Read(CheckSum, sizeof(CheckSum)) ) {

			delete pEncodedImg;

			return FALSE;
			}

		WORD CRC = 0;
		
		DecodeAscii(CheckSum, PBYTE(&CRC), 4);

		CRC = MotorToHost(CRC);

		if( !ValidateBitmap(HeaderBuf, pEncodedImg, uNeed, CRC) ) {

			delete pEncodedImg;

			return FALSE;
			}

		pBmp = new BYTE[uBytes / 2];

		DecodeAscii(pEncodedImg, pBmp, uNeed);

		delete pEncodedImg;

		if( (pData = CreateBitmap(Header, pBmp)) ) {

			delete pBmp;

			return TRUE;
			}
		}

	delete pBmp;

	return FALSE;
	}

BOOL CCognexCamera::ReadHeader(PBYTE pEncHead, HEADER_BM &Header)
{
	DecodeAscii(pEncHead, PBYTE(&Header), 2 * sizeof(Header));

	Header.Bits	= IntelToHost(Header.Bits);
	Header.cx	= IntelToHost(Header.cx);
	Header.cy	= IntelToHost(Header.cy);
	Header.Offset	= IntelToHost(Header.Offset);
	Header.Size	= IntelToHost(Header.Size);

	if( Header.Magic[0] != 'B' || Header.Magic[1] != 'M' ) {

		return FALSE;
		}

	return TRUE;
	}

// Dst should be at least (SrcSize / 2) bytes
void CCognexCamera::DecodeAscii(PBYTE Src, PBYTE Dst, UINT SrcSize)
{
	UINT i, n;
	BYTE Hi, Lo;

	for( i = 0, n = 0; i < SrcSize - 1; i += 2, n++ ) {

		if( Src[i] == '\r' || Src[i + 1] == '\r' ) {

			i += 2;
			}

		Hi = Src[i] > '9' ? Src[i] - 'A' + 10 : Src[i] - '0';
		Lo = Src[i+1] > '9' ? Src[i+1] - 'A' + 10 : Src[i+1] - '0';

		Dst[n] = (Hi << 4) | Lo;
		}
	}

WORD CCognexCamera::FindCRC16(WORD CRC, PBYTE Src, UINT Size)
{	
	// 16-bit CRC from Cognex documentation
	// Uses CCITT polynomial
	WORD c;

	for( int i = 0; i < Size; i++) {

		if( Src[i] == '\r' || Src[i] == '\n' )
			continue;

		c = Src[i] << 8;

		for( int j = 0; j < 8; j++) {

			if( (c & 0x8000) ^ (CRC & 0x8000) ) {

				CRC <<= 1;

				CRC ^= 4129;
				}
			else {
				CRC <<= 1;
				}

			c <<= 1;
			}
		}

	return CRC;
	}

BOOL CCognexCamera::Login(CContext *pCtx)
{
	if( m_pCtx->m_fSession ) {

		return TRUE;
		}

	SPrintf(pCtx->m_Cmd, "%s\r\n%s\r\n", pCtx->m_pUser, pCtx->m_pPass);

	if( Send(pCtx->m_pSock, pCtx->m_Cmd, NULL) ) {

		// Consume the two log-in response lines
		char c = 0;

		while( c != '\n') {

			Read(PBYTE(&c), 1);
			}

		c = 0;

		while( c != '\n') {

			Read(PBYTE(&c), 1);
			}

		pCtx->m_fSession = TRUE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCognexCamera::SendCommand(CContext *pCtx)
{
	SPrintf(pCtx->m_Cmd, "rb\r\n");

	if( Send(pCtx->m_pSock, pCtx->m_Cmd, NULL) ) {
			
		char Result[3]	= { };
			
		char Size[16]	= { };

		if( !Read(PBYTE(Result), 3) ) {

			return FALSE;
			}

		if( Result[0] != '1' ) {

			return FALSE;
			}

		char c = 0;

		for(int i = 0; c != '\n' && i < elements(Size) - 1; i++ ) {

			Read(PBYTE(&c), 1);

			Size[i] = c;
			}

		pCtx->m_uImgSize = strtol(Size, NULL, 10);

		pCtx->m_fCmdSent = TRUE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCognexCamera::ValidateBitmap(PBYTE pHead, PBYTE pEnc, UINT uSize, WORD CRC)
{
	WORD CalcCRC = 0;

	CalcCRC = FindCRC16(CalcCRC, pHead, 2 * sizeof(HEADER_BM));

	CalcCRC = FindCRC16(CalcCRC, pEnc, uSize);
	
	if( CalcCRC != CRC ) {

		return FALSE;
		}

	return TRUE;
	}

void CCognexCamera::EndSession(CContext *pCtx)
{
	pCtx->m_fSession = FALSE;

	pCtx->m_fCmdSent = FALSE;
	}

PBYTE CCognexCamera::CreateDummy(HEADER_BM &Header)
{
	BITMAP_RLC *pDummy = new BITMAP_RLC;

	pDummy->Format	= image8bitGreyScale;
	pDummy->Height	= Header.cy;
	pDummy->Width	= Header.cx;
	pDummy->Stride	= Header.cx;
	pDummy->Frame	= m_pCtx->m_uFrame++;

	return PBYTE(pDummy);
	}

PBYTE CCognexCamera::CreateBitmap(HEADER_BM &Header, PBYTE pImg)
{
	PBYTE pWork = NULL;

	PBYTE ImgData = pImg + Header.Offset - sizeof(Header);

	if( Header.Bits == 8 ) {

		UINT uSize = Header.cx * Header.cy;

		pWork = new BYTE[sizeof(BITMAP_RLC) + uSize];

		BITMAP_RLC *pBmpRLC = (BITMAP_RLC *) pWork;

		pBmpRLC->Format	= image8bitGreyScale;
		pBmpRLC->Frame	= m_pCtx->m_uFrame++;
		pBmpRLC->Height	= Header.cy;
		pBmpRLC->Width	= Header.cx;
		pBmpRLC->Stride = Header.cx;
			
		memcpy(pBmpRLC->Data, ImgData, uSize);
		}

	else if( Header.Bits == 24 ) {

		UINT uSize = Header.cx * Header.cy * 3;

		pWork = new BYTE[sizeof(BITMAP_RLC) + uSize];

		BITMAP_RLC *pBmpRLC = (BITMAP_RLC *) pWork;

		pBmpRLC->Format	= image24bitColor;
		pBmpRLC->Frame	= m_pCtx->m_uFrame++;
		pBmpRLC->Height	= Header.cy;
		pBmpRLC->Width	= Header.cx;
		pBmpRLC->Stride = Header.cx * 3;

		memcpy(pBmpRLC->Data, ImgData, uSize);
		}

	return pWork;
	}

BOOL CCognexCamera::SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	return FALSE;
	}

BOOL CCognexCamera::LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	return FALSE;
	}

BOOL CCognexCamera::UseSetup (PVOID pContext, UINT uIndex)
{
	return FALSE;
	}

BOOL CCognexCamera::Read(PBYTE pData, UINT uCount)
{
	UINT uTotal = 0;

	SetTimer(m_pCtx->m_uTime1);

	while( GetTimer() ) {

		if( uTotal == uCount ) {
			
			return TRUE;
			}

		UINT uAvail = uCount - uTotal;

		if( m_pCtx->m_pSock->Recv(pData + uTotal, uAvail) == S_OK ) {

			SetTimer(m_pCtx->m_uTime1);

			uTotal += uAvail;

			}
		else {
			UINT Phase = 0;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase != PHASE_OPEN ) {
				
				break;
				}
			}
		}

	return FALSE;
	}

BOOL CCognexCamera::Send(ISocket *pSock, PCTXT pText, PTXT pArgs)
{
	UINT uSize = strlen(pText);

	PTXT pWork = new char [ uSize + 256 ];

	SPrintf(pWork, pText, pArgs);

	if( Send(pSock, PCBYTE(pWork), strlen(pWork)) ) {

		delete pWork;

		return TRUE;
		}

	delete pWork;

	return FALSE;
	}

BOOL CCognexCamera::Send(ISocket *pSock, PCBYTE pText, UINT uSize)
{
	UINT uLimit = 1280;

	while( uSize ) {

		CBuffer *pBuff = CreateBuffer(uLimit, TRUE);

		if( pBuff ) {

			PBYTE pData = pBuff->GetData();

			UINT  uCopy = min(uLimit, uSize);

			memcpy(pData, pText, uCopy);

			pBuff->AddTail(uCopy);  

			SetTimer(timeSendTimeout);

			while( pSock->Send(pBuff) == E_FAIL ) {

				UINT Phase;

				pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN && GetTimer() ) {

					Sleep(timeSendDelay);

					continue;
					}

				BuffRelease(pBuff);
				
				return FALSE;
				}

			pText += uCopy;

			uSize -= uCopy;
			}
		else {  
			Sleep(timeBuffDelay);

			return FALSE;
			}
		}

	return TRUE;
	}

// End of File
