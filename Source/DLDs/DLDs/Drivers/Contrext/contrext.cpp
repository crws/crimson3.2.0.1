
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "contrext.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Contrext M-Trim/ML-Drive Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CContrexTrimDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CContrexTrimDriver::CContrexTrimDriver(void)
{
	m_Ident     = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
}

// Destructor

CContrexTrimDriver::~CContrexTrimDriver(void)
{
}

// Configuration

void MCALL CContrexTrimDriver::Load(LPCBYTE pData)
{
}

void MCALL CContrexTrimDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
}

// Management

void MCALL CContrexTrimDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
}

void MCALL CContrexTrimDriver::Open(void)
{
	m_fPing = FALSE;
}

// Device

CCODE MCALL CContrexTrimDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_bType = GetByte(pData);

			m_pCtx->m_dCommandRead = 0;

			m_pCtx->m_dError       = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CContrexTrimDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CContrexTrimDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 70;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra	= 0;

	m_fPing = TRUE;

	return Read(Addr, Data, 1);
}

CCODE MCALL CContrexTrimDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( NoTransmit(Addr, pData, FALSE) ) {

		return 1;
	}

//**/	AfxTrace2("\r\nREAD T=%d O=%d ", Addr.a.m_Table, Addr.a.m_Offset);

	AddRead(Addr.a.m_Offset);

	if( Transact() ) {

		if( GetResponse(pData) ) {

			return 1;
		}
	}

	return CCODE_ERROR;
}

CCODE MCALL CContrexTrimDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\n\nWRITE T=%d O=%d ", Addr.a.m_Table, Addr.a.m_Offset);

	if( NoTransmit(Addr, pData, TRUE) ) {

		return 1;
	}

	if( Addr.a.m_Table == 1 ) {

		AddRead(Addr.a.m_Offset);

		if( !Transact() || CheckError() ) {

			return 1; // couldn't read variable
		}
	}

	AddWrite(Addr, *pData);

	if( Transact() ) {

		CheckError();

		return 1;
	}

	return CCODE_ERROR;
}

// PRIVATE METHODS

// Frame Building

void CContrexTrimDriver::StartFrame(void)
{
	m_uPtr = 0;
	AddByte(STX);
	AddByte(m_pHex[m_pCtx->m_bDrop/10]);
	AddByte(m_pHex[m_pCtx->m_bDrop%10]);
}

void CContrexTrimDriver::EndFrame(void)
{
	m_bTx[11] = ETX;
}

void CContrexTrimDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
}

void CContrexTrimDriver::AddData(DWORD dData, DWORD dFactor)
{
	while( dFactor ) {

		AddByte(m_pHex[(dData/dFactor) % 10]);

		dFactor /= 10;
	}
}

void CContrexTrimDriver::AddRead(UINT uOffset)
{
	StartFrame();

	AddByte(INQUIRY);

	AddData(uOffset, 10);

	AddData(0, 1000);

	AddByte('0');
}

void CContrexTrimDriver::AddWrite(AREF Addr, DWORD dData)
{
	StartFrame();

	switch( Addr.a.m_Table ) {

		case 1:

			AddByte(PARSEND);

			AddData(Addr.a.m_Offset, 10); // Parameter Number

			AddRealData(dData, Addr.a.m_Offset); // Construct real data

			break;

		case addrNamed:

			m_pCtx->m_dCommandRead = dData;

			AddByte(COMMAND);

			AddData(dData, 100000); // param 00 + 2 more + 2 char command number

			AddByte('0');

			break;
	}
}

void CContrexTrimDriver::AddRealData(DWORD dData, UINT uOffset)
{
	BOOL fNeg    = FALSE;

	BOOL fVarFormat = FALSE;

	DWORD dExp;

	char cStart[64] = { 0 };

	fVarFormat = (uOffset >= 20) && (uOffset <= UINT(m_pCtx->m_bType ? 21 : 23));

	if( dData & 0x80000000 ) {

		dData &= 0x7FFFFFFF;

		fNeg = TRUE;
	}

	dExp = dData >> 23;

	if( dExp > 140 ) { // > 9999

		AddData(9999, 1000);
		AddByte((fNeg && fVarFormat) ? '4' : '0');
		return;
	}

	else { // note if dData == 0 this will be executed
		if( dExp < 117 ) { // < .001

			AddData(0, 10000); // 4 data + '0' format
			return;
		}
	}

	SPrintf(cStart, "%f", PassFloat(dData));

	PutRealData(cStart, fNeg, fVarFormat, uOffset);
}

// Transport Layer

BOOL CContrexTrimDriver::Transact(void)
{
	Send();

	return GetReply();
}

void CContrexTrimDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
}

BOOL CContrexTrimDriver::GetReply(void)
{
	if( m_pCtx->m_bDrop == 0 ) {

		return TRUE;
	}

	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uData;

	BYTE bData;

	SetTimer(TIMEOUT);

//**/	AfxTrace0("\r\n");

	while( (uTimer = GetTimer()) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
		}

		bData = LOBYTE(uData) & 0x7F;

//**/		AfxTrace1("<%2.2x>", bData);

		if( bData == STX ) uCount = 0;

		m_bRx[uCount++] = bData;

		if( bData == ETX ) {

			if( uCount == 12 ) return TRUE;

			else return FALSE;
		}

		if( uCount > 12 ) return FALSE;
	}

	return FALSE;
}

BOOL CContrexTrimDriver::GetResponse(PDWORD pData)
{
	for( UINT i = 0; i < 3; i++ ) {

		if( m_bRx[i] != m_bTx[i] )

			return FALSE;
	}

	if( !m_fPing ) {

		if( CheckError() ) {

			*pData = 0;

			return TRUE;
		}
	}

	else {
		m_fPing = FALSE;
	}

	GetRealData(pData);

	return TRUE;
}

// Port Access

void CContrexTrimDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < 12; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_pData->Write(PCBYTE(m_bTx), 12, FOREVER);
}

UINT CContrexTrimDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
}

// Helpers

BOOL  CContrexTrimDriver::NoTransmit(AREF Addr, PDWORD pData, BOOL fIsWrite)
{
	if( Addr.a.m_Table == addrNamed ) {

		switch( Addr.a.m_Offset ) {

			case 3: // ERR
				if( !fIsWrite ) {

					*pData = m_pCtx->m_dError;
				}

				else {
					m_pCtx->m_dError = *pData;
				}

				return TRUE;

			case 4: // Command
				if( !fIsWrite ) {

					*pData = m_pCtx->m_dCommandRead;

					return TRUE;
				}

				else
					return FALSE;

			default:
				if( !fIsWrite ) {

					*pData = 0;
				}

				return TRUE;
		}
	}

	if( !fIsWrite && m_pCtx->m_bDrop == 0 ) {

		*pData = 0;

		return TRUE;
	}

	if( Addr.a.m_Offset > 99 ) {

		*pData = 0;

		return TRUE;
	}

	switch( m_pCtx->m_bType ) {

		case 0: // M-Trim

			switch( Addr.a.m_Offset ) {

				case 7:
				case 8:
				case 9:
				case 24:
				case 25:
				case 26:
				case 27:
				case 28:
				case 39:
				case 87:
				case 88:
				case 89:
				case 90:
				case 91:
				case 92:
				case 93:
				case 94:
				case 95:
				case 96:
				case 97:
				case 98:
					*pData = 0;
					return TRUE;// invalid parameters
			}

			if( fIsWrite ) {

				if( (Addr.a.m_Offset >= 40 && Addr.a.m_Offset < 60) ||
				   Addr.a.m_Offset == 0  ||
				   Addr.a.m_Offset == 99
				   ) {

					return TRUE; // invalid write parameters
				}
			}
			break;

		case 1: // ML-Drive

			switch( Addr.a.m_Offset ) {

				case 0:
				case 7:
				case 8:
				case 9:
				case 11:
				case 18:
				case 19:
				case 22:
				case 23:
				case 24:
				case 25:
				case 26:
				case 27:
				case 28:
				case 29:
				case 32:
				case 35:
				case 36:
				case 37:
				case 38:
				case 39:
				case 42:
				case 60:
				case 63:
				case 69:
				case 75:
				case 76:
				case 77:
				case 78:
				case 79:
				case 84:
				case 85:
				case 86:
				case 87:
				case 88:
				case 89:
				case 90:
				case 91:
				case 92:
				case 93:
				case 94:
				case 95:
				case 96:
				case 97:
					*pData = 0;
					return TRUE;// invalid parameters
			}

			if( fIsWrite ) {

				if( (Addr.a.m_Offset >= 40 && Addr.a.m_Offset < 60) ||
				   Addr.a.m_Offset == 82 ||
				   Addr.a.m_Offset == 83 ||
				   Addr.a.m_Offset == 99
				   ) {

					return TRUE; // invalid write parameters
				}
			}
			break;

		case 2: // ML-Trim

			switch( Addr.a.m_Offset ) {

				case 7:
				case 8:
				case 9:
				case 11:
				case 18:
				case 19:
				case 22:
				case 23:
				case 24:
				case 25:
				case 26:
				case 27:
				case 28:
				case 29:
				case 32:
				case 35:
				case 36:
				case 37:
				case 38:
				case 39:
				case 42:
				case 49:
				case 60:
				case 63:
				case 68:
				case 69:
				case 75:
				case 76:
				case 77:
				case 78:
				case 79:
				case 80:
				case 81:
				case 82:
				case 83:
				case 84:
				case 85:
				case 86:
				case 87:
				case 88:
				case 89:
				case 90:
				case 91:
				case 92:
				case 93:
				case 94:
				case 95:
				case 96:
				case 97:
					*pData = 0;
					return TRUE;// invalid parameters
			}

			if( fIsWrite ) {

				if( (Addr.a.m_Offset >= 40 && Addr.a.m_Offset < 60) ||
				   Addr.a.m_Offset == 0  ||
				   Addr.a.m_Offset == 99
				   ) {

					return TRUE; // invalid write parameters
				}
			}
			break;
	}

	return FALSE;
}

DWORD CContrexTrimDriver::GetGeneric(PBYTE p, UINT uCt)
{
	DWORD d = 0L;
	BYTE b;

	for( UINT i = 0; i < uCt; i++ ) {

		d *= 10;

		b = p[i];

		if( b >= '0' && b <= '9' )
			d += (b - '0');
	}

	return d;
}

UINT CContrexTrimDriver::GetDPFromFormat(PBYTE pNeg)
{
	UINT uDP = m_bRx[10] - '0'; // format number

	*pNeg = 0;

	switch( uDP ) {

		case 4: // -xxxx
		case 5: // -xxx.x
		case 6: // -xx.xx
		case 7: // -x.xxx
			*pNeg = 1;
			return uDP - 4;

		case 8: // xx.xx
			return 2;

		case 9:  // xxx.x * 10 Is this a weird thing for them to do? x / 10 would be sensible
		case 10: // xx.xx * 10
		case 11: // x.xxx * 10
			return uDP - 9;

		case 0: // xxxx
		case 1: // xxx.x
		case 2: // xx.xx
		case 3: // x.xxx
		default:
			return uDP;
	}

}

void CContrexTrimDriver::GetRealData(PDWORD pData)
{
	unsigned char s[16] = { 0 };

	BYTE bNeg  = 0;

	UINT i     = 0;

	UINT uS    = 6;

	UINT uDP = GetDPFromFormat(&bNeg);

	while( uS < 10 ) {

		if( i == 4 - uDP ) {

			s[i] = '.';
		}

		else {
			s[i] = m_bRx[uS++];
		}

		i++;
	}

	if( m_bRx[10] == '8' ) { // used when format switches from 0 dp to 2, based on value (e.g. MV41)

		s[i] = '2';
	}

	pData[0] = ATOF((const char *) &s[0]);

	if( bNeg ) {

		pData[0] |= 0x80000000;
	}
}

void CContrexTrimDriver::PutRealData(char * pS, BOOL fNeg, BOOL fVarFormat, UINT uOffset)
{
	BOOL fFirst      = FALSE;
	BOOL fGotDP      = FALSE;
	BYTE bDummy      = 0;

	char cInt[8]     = { 0 };
	char cFrac[8]    = { 0 };
	char cRound;
	char c;

	UINT uDP         = 0;
	UINT uI		 = 0;
	UINT uF          = 0;
	UINT uMaxCharCt  = fNeg ? 3 : 4;

	if( m_pCtx->m_bType == 2 ) {

		if( uOffset == 16 || uOffset == 17 ) {

			uDP = 1;
		}
	}

	else {

		uDP = fVarFormat ? 0 : GetDPFromFormat(&bDummy);
	}

	for( UINT i = 0; pS[i]; i++ ) {

		c = pS[i];

		if( c != '0' || fFirst ) {

			fFirst = TRUE;

			if( c == '.' ) {

				fGotDP = TRUE;

				if( !uI ) {

					cInt[uI++] = '0';
				}
			}

			else {
				if( fGotDP ) {

					cFrac[uF++] = c;
				}

				else {

					cInt[uI++] = c;
				}
			}
		}

		if( uI > uMaxCharCt ) break;

		else {
			if( uF && (uI + uF >= uMaxCharCt) ) {

				i++; // to get rounding character

				break;
			}
		}
	}

	cRound = pS[i] | '0'; // makes null into '0', does nothing to a digit

	if( uI > uMaxCharCt - uDP ) {

		AddData(9999, 1000);

		AddByte((fVarFormat && fNeg) ? '4' : '0');

		return;
	}

// Otherwise set Decimal position counts

	if( !fVarFormat ) {

		if( uF != uDP ) {

			if( uF > uDP ) { // too many

				while( uF > uDP + 1 ) { // remove all but 1

					cFrac[--uF] = 0;
				}

				cRound = cFrac[--uF]; // use the remaining one for round

				cFrac[uF] = 0;
			}

			else { // too few

				while( uF < uDP ) {

					cFrac[uF++] = '0';
				}

				cRound = '0';
			}
		}
	}

	else { // Format must indicate # of dp positions

		if( uF + uI != uMaxCharCt ) {

			if( uF + uI > uMaxCharCt ) { // too many digits

				while( uF + uI > uMaxCharCt+1 ) {

					cFrac[--uF] = 0;
				}

				cRound = cFrac[--uF];

				cFrac[uF] = 0;
			}

			else { // too few

				if( uF ) {

					while( uF + uI < uMaxCharCt ) {

						cFrac[uF++] = '0';
					}
				}

				cRound = '0';
			}
		}
	}

	AddData(Round(cInt, cFrac, cRound, uF), 1000);

	if( !fVarFormat ) {

		AddByte('0');
	}

	else {

		AddByte(fNeg ? m_pHex[4 + uF] : m_pHex[uF]);
	}

	return;
}

DWORD CContrexTrimDriver::Round(char * pI, char * pF, char cRound, UINT uFSize)
{
	DWORD dInt = ATOI(pI);

	if( uFSize ) {

		for( UINT i = 0; i < uFSize; i++ ) {

			dInt *= 10;
		}

		dInt += ATOI(pF);
	}

	dInt *= 10;

	dInt += cRound - '+'; // = cRound - '0' + 5

	return dInt > 99999 ? 9999 : dInt / 10;
}

BOOL CContrexTrimDriver::CheckError(void)
{
	BYTE bR3 = m_bRx[3] & 0x3F;

	if( bR3 ) {

		if( m_bTx[3] == '1' ) {

			m_pCtx->m_dError = ((GetGeneric(&m_bTx[8], 2) + 100) << 16) + bR3;
		}

		else {
			if( bR3 == 0x10 ) {

				return FALSE; // don't signal lone parameter # error
			}

			m_pCtx->m_dError = (GetGeneric(&m_bTx[4], 2) << 16) + bR3;
		}

		return TRUE;
	}

	return FALSE;
}

// End of File
