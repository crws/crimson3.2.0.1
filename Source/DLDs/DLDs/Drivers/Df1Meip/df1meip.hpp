/////////////////////////////////////////////////////////////////////////
//
// DF1 PCCC Ethernet/IP Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//


//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "../Df1Shared/df1m.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDF1EIPMaster;

/////////////////////////////////////////////////////////////////////////
//
// PCCC Definitions
//

#define PCCC_SERVICE		0x4B

#define PCCC_CLASS		0x67

//////////////////////////////////////////////////////////////////////////
//
// CIP Requestor
//

#pragma pack(1)

struct CPCCCReqId
{
	BYTE	bSize;
	WORD	wVendorId;
	DWORD	dwSN;
	};

#pragma pack()

/////////////////////////////////////////////////////////////////////////
//
// DF1 PCCC Ethernet/IP Master Driver
//

class CDF1EIPMaster : public CDF1BaseMaster
{
	public:
		// Constructor
		CDF1EIPMaster(void);

		// Destructor
		~CDF1EIPMaster(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT) DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:
		// Device Data
		struct CContext : CDF1BaseMaster::CBaseCtx
		{
			DWORD	m_IP;
			WORD	m_wSlot;
			WORD    m_wTimeout;
			BOOL	m_fAbort;
			};

		// Data Members
		IEthernetIPHelper * m_pEnetHelper;
		IExplicit         * m_pExplicit;
		CContext          * m_pCtx;
		BOOL		    m_fOpen;
		
		// Transport Layer
		BOOL MakeLink(void);
		BOOL CheckLink(void);
		BOOL Transact(void);

		// Transport Helpers
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL CheckFrame(void);	

		// Helpers
	virtual	void GetCount(UINT uSpace, UINT &uCount);

		// DevCtrl Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
	};

// End of File
