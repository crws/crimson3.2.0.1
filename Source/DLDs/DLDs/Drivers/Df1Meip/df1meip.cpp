
#include "intern.hpp"

#include "df1meip.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Shared Base Class
//

#include "../Df1Shared/df1m.cpp"

/////////////////////////////////////////////////////////////////////////
//
// DF1 PCCC Ethernet/IP Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CDF1EIPMaster);

// Constructor

CDF1EIPMaster::CDF1EIPMaster(void)
{
	m_Ident       = DRIVER_ID;
	
	m_fOpen       = FALSE;
	
	m_pEnetHelper = NULL;

	m_pExplicit   = NULL;

	m_pCtx        = NULL;
	}

// Destructor

CDF1EIPMaster::~CDF1EIPMaster(void)
{
	}

// Management

void MCALL CDF1EIPMaster::Attach(IPortObject *pPort)
{
	if( !m_pEnetHelper && !m_fOpen ) {
		
		if( MoreHelp(IDH_ENETIP, (void **) &m_pEnetHelper) ) {

			m_fOpen = m_pEnetHelper->Open();
			}
		}
	}

void MCALL CDF1EIPMaster::Detach(void)
{
	if( m_pEnetHelper ) {

		m_pEnetHelper->Close();

		m_pEnetHelper->Release();

		m_pEnetHelper = NULL;

		m_pExplicit   = NULL;

		m_fOpen       = FALSE;
		}
	}

// Device

CCODE MCALL CDF1EIPMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_uHeader  = headPCCC;
			m_pCtx->m_bDevice  = GetByte(pData);
			m_pCtx->m_IP	   = GetAddr(pData);
			m_pCtx->m_wSlot	   = GetWord(pData);
			m_pCtx->m_wTimeout = GetWord(pData);
			m_pCtx->m_fAbort   = GetByte(pData);

			if( m_pCtx->m_bDevice == devEIN ) {

				m_pCtx->m_wSlot = EIP_EMPTY;
				}

			m_pCtx->m_uCache  = 0;

			pDevice->SetContext(m_pCtx);

			return MakeLink() ? CCODE_SUCCESS : CCODE_ERROR;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return MakeLink() ? CCODE_SUCCESS : CCODE_ERROR;
	}

CCODE MCALL CDF1EIPMaster::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CDF1EIPMaster::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) {
		
		// Set Device IP Address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));

		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		pCtx->m_IP     = MotorToHost(dwValue);

		Free(pText);
			
		return 1;    
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL CDF1EIPMaster::Ping(void)
{
	return CDF1BaseMaster::Ping();
	}

// Transport Layer

BOOL CDF1EIPMaster::MakeLink(void)
{	
	if( m_fOpen ) {

		m_pExplicit = m_pEnetHelper->GetExplicit();
		}

	return CheckLink();
	}

BOOL CDF1EIPMaster::CheckLink(void)
{
	return m_fOpen && m_pExplicit;
	}

BOOL CDF1EIPMaster::Transact(void)
{
	if( SendFrame() ) {

		if( RecvFrame() && CheckFrame() ) {

			return TRUE;
			}

		if( m_pCtx->m_fAbort ) {

			m_pExplicit->Abort(CIpAddr(m_pCtx->m_IP));
			}
		}

	return FALSE;
	}

// Transport Helpers

BOOL CDF1EIPMaster::SendFrame(void)
{
	UINT uCount = m_uPtr + sizeof(CPCCCReqId);

	CBuffer *pBuff = CreateBuffer(uCount, FALSE);

	if( pBuff ) {

		CIPADDR Addr;
		
		Addr.m_ip	  = (IPADDR const &) m_pCtx->m_IP;

		Addr.m_wPort	  = 1;
		
		Addr.m_wSlot	  = m_pCtx->m_wSlot;
		
		Addr.m_pPath	  = NULL;
		
		Addr.m_bService   = PCCC_SERVICE;
		
		Addr.m_wClass     = PCCC_CLASS;
		
		Addr.m_wInst	  = 1;
		
		Addr.m_wAttr	  = EIP_EMPTY;
		
		Addr.m_wMember    = EIP_EMPTY;

		Addr.m_uTagSize   = 0;
		
		CPCCCReqId *pHead = BuffAddTail(pBuff, CPCCCReqId);

		pHead->bSize	  = sizeof(CPCCCReqId);

		pHead->wVendorId  = HostToIntel(m_pEnetHelper->GetVendorId());

		pHead->dwSN	  = 0;

		memcpy( pBuff->AddTail(m_uPtr), 
			m_bTxBuff, 
			m_uPtr
			);

		m_pExplicit->SetTimeout(m_pCtx->m_wTimeout);
	
		if( m_pExplicit->Send(Addr, pBuff) ) {

			return TRUE;
			}

		pBuff->Release();
		}

	return FALSE;
	}

BOOL CDF1EIPMaster::RecvFrame(void)
{
	UINT uTimer = 0;

	SetTimer(m_pCtx->m_wTimeout);

	while( (uTimer = GetTimer()) ) {
       	
		CBuffer *pBuff;

		if( m_pExplicit->Recv(pBuff) && pBuff) {

			if( pBuff->GetSize() >= sizeof(CPCCCReqId) ) { 

				CPCCCReqId *pHead = BuffStripHead(pBuff, CPCCCReqId);

				WORD wVendorId    =  IntelToHost(pHead->wVendorId);
			
				if( wVendorId == m_pEnetHelper->GetVendorId() ) {

					if( pBuff->GetSize() <= elements(m_bRxBuff) ) {
				
						memcpy( m_bRxBuff, 
							pBuff->GetData(), 
							pBuff->GetSize()
							);
					
						pBuff->Release();
		
						return TRUE;
						}
					}
				}

			pBuff->Release();
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CDF1EIPMaster::CheckFrame(void)
{
	DF1PCCCHEAD &TxHead = (DF1PCCCHEAD &) m_bTxBuff[0];
			
	DF1PCCCHEAD &RxHead = (DF1PCCCHEAD &) m_bRxBuff[0];

	if( RxHead.bComm != (TxHead.bComm | (1 << 6)) ) {
		
		return FALSE;
		}

	if( RxHead.wTrans != TxHead.wTrans ) {

		return FALSE;
		} 
	
	return RxHead.bStatus ? FALSE : TRUE;
	}

void CDF1EIPMaster::GetCount(UINT uSpace, UINT &uCount)
{
	// Note: uCount is limited to 50 in configs block.cpp

	UINT uMax = m_pBase->m_bDevice == devPLC5 ? 16 : 96;
	
	if( IsTriplet(uSpace) ) {

		if( m_pBase->m_bDevice == devPLC5 ) {	
										
			uMax  = 1;
			}
		else {
			uMax /= 3;
			}
		}

	else if( IsString(uSpace) )  {

		uCount = MAX_STRING; 

		return;
		}

	else if( IsLong(uSpace) ) {

		uMax /= 2;
		}

	MakeMin(uCount, uMax);
	}

// DevCtrl Helpers

BOOL CDF1EIPMaster::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CDF1EIPMaster::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CDF1EIPMaster::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
