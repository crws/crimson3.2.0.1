
#include "intern.hpp"

#include "devnets2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Slave Driver
//

// Instantiator

INSTANTIATE(CDeviceNetSlave);

// Constructor

CDeviceNetSlave::CDeviceNetSlave(void)
{
	m_Ident     = 0x4077;

	m_pDevNet   = NULL;
	
	m_pBitRecv  = NULL;
	
	m_pBitSend  = NULL;
	
	m_pPollRecv = NULL;
	
	m_pPollSend = NULL;
	
	m_pDataRecv = NULL;
	
	m_pDataSend = NULL;

	m_fDBit	    = FALSE;
	
	m_fDPoll    = FALSE;

	m_fDData    = FALSE;
	}

// Destructor

CDeviceNetSlave::~CDeviceNetSlave(void)
{
	}

// Configuration

void MCALL CDeviceNetSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bMac      = GetByte(pData);

		m_fBit      = GetByte(pData);

		m_wBitRecv  = GetByte(pData);

		m_wBitSend  = GetByte(pData);

		m_fPoll     = GetByte(pData);

		m_wPollRecv = GetWord(pData);

		m_wPollSend = GetWord(pData);

		m_fData     = GetByte(pData);

		m_wDataRecv = GetWord(pData);

		m_wDataSend = GetWord(pData);

		m_fIntel    = GetByte(pData);
		}
	}

// Management

void MCALL CDeviceNetSlave::Attach(IPortObject *pPort)
{
	MoreHelp(IDH_DEVNET, (void **) &m_pDevNet);

	m_pDevNet->Attach(pPort);
	}

void MCALL CDeviceNetSlave::Detach(void)
{
	m_pDevNet->Close();
	
	m_pDevNet->Detach();
	
	m_pDevNet->Release();

	FreeBuffers();
	}

// Device

CCODE MCALL CDeviceNetSlave::DeviceOpen(IDevice *pDevice)
{
	CCommsDriver::DeviceOpen(pDevice);

	if( m_pDevNet ) {

		m_pDevNet->SetMac(m_bMac);

		if( m_fBit && !m_pDevNet->EnableBitStrobe(m_wBitRecv, m_wBitSend) ) {

			return CCODE_ERROR;
			}

		if( m_fPoll && !m_pDevNet->EnablePolled(m_wPollRecv, m_wPollSend) ) {

			return CCODE_ERROR;
			}

		if( m_fData && !m_pDevNet->EnableData(m_wDataRecv, m_wDataSend) ) {

			return CCODE_ERROR;
			}

		if( m_pDevNet->Open() ) {

			AllocBuffers();

			return CCODE_SUCCESS;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CDeviceNetSlave::DeviceClose(BOOL fPersist)
{
	FreeBuffers();
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

void MCALL CDeviceNetSlave::Service(void)
{
	if( m_pDevNet && m_pDevNet->IsOnLine() ) {

		DoWrites();

		DoReads();
		}
	}

CCODE MCALL CDeviceNetSlave::Ping(void)
{
	return m_pDevNet->IsOnLine() ? CCODE_SUCCESS : CCODE_ERROR;
	}

CCODE MCALL CDeviceNetSlave::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pDevNet->IsOnLine() ) {

		UINT  uSize = 0; 

		PBYTE pFind = FindLocale(Addr.a.m_Table, uSize);

		if( pFind ) {

			if( Addr.a.m_Type == addrWordAsWord ) {

				UINT uFrom = Addr.a.m_Offset * 2;

				UINT uCopy = min(uCount, (uSize - uFrom) / sizeof(WORD));

				if( m_fIntel ) {

					CIntelDataPacker Data(pData, uCopy, TRUE);

					Data.Unpack(PWORD(pFind + uFrom));
					}
				else {
					CMotorDataPacker Data(pData, uCopy, TRUE);

					Data.Unpack(PWORD(pFind + uFrom));
					}

				return uCount;
				}

			if( Addr.a.m_Type == addrWordAsLong ) {

				UINT uFrom = Addr.a.m_Offset * 2;

				UINT uCopy = min(uCount, (uSize - uFrom) / sizeof(DWORD));

				if( m_fIntel ) {

					CIntelDataPacker Data(pData, uCopy, TRUE);

					Data.Unpack(PDWORD(pFind + uFrom));
					}
				else {
					CMotorDataPacker Data(pData, uCopy, TRUE);

					Data.Unpack(PDWORD(pFind + uFrom));
					}

				return uCount;
				}
			}

		memset(pData, 0, 4 * uCount);

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CDeviceNetSlave::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pDevNet->IsOnLine() ) {

		UINT  uSize = 0;

		PBYTE pFind = FindLocale(Addr.a.m_Table, uSize);
      
		if( pFind ) {

			if( Addr.a.m_Type == addrWordAsWord ) {

				UINT uFrom = Addr.a.m_Offset * 2;

				UINT uCopy = min(uCount, (uSize - uFrom) / 2);

				if( m_fIntel ) {

					CIntelDataPacker Data(pData, uCopy, TRUE);

					Data.Pack(PWORD(pFind + uFrom));
					}
				else {
					CMotorDataPacker Data(pData, uCopy, TRUE);

					Data.Pack(PWORD(pFind + uFrom));
					}

				return uCount;
				}

			if( Addr.a.m_Type == addrWordAsLong ) {

				UINT uFrom = Addr.a.m_Offset * 2;

				UINT uCopy = min(uCount, (uSize - uFrom) / 4);

				if( m_fIntel ) {

					CIntelDataPacker Data(pData, uCopy, TRUE);

					Data.Pack(PDWORD(pFind + uFrom));
					}
				else {
					CMotorDataPacker Data(pData, uCopy, TRUE);

					Data.Pack(PDWORD(pFind + uFrom));
					}

				return uCount;
				}
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Buffers

void CDeviceNetSlave::AllocBuffers(void)
{
	AllocBuffer(m_pBitRecv , m_wBitRecv );
				           
	AllocBuffer(m_pBitSend , m_wBitSend );
				           
	AllocBuffer(m_pPollRecv, m_wPollRecv);
				           
	AllocBuffer(m_pPollSend, m_wPollSend);
				           
	AllocBuffer(m_pDataRecv, m_wDataRecv);
				           
	AllocBuffer(m_pDataSend, m_wDataSend);
	}

void CDeviceNetSlave::FreeBuffers(void)
{
	FreeBuffer(m_pBitRecv);
	
	FreeBuffer(m_pBitSend);
	
	FreeBuffer(m_pPollRecv);
	
	FreeBuffer(m_pPollSend);
	
	FreeBuffer(m_pDataRecv);
	
	FreeBuffer(m_pDataSend);
	}

void CDeviceNetSlave::AllocBuffer(PBYTE &pData, UINT uSize)
{
	if( uSize ) {

		pData = new BYTE [ uSize ];

		memset(pData, 0, uSize);

		return;
		}

	pData = NULL;
	}

void CDeviceNetSlave::FreeBuffer(PBYTE &pData)
{
	if( pData ) {

		delete pData;

		pData = NULL;
		}
	}

// Implementation

PBYTE CDeviceNetSlave::FindLocale(UINT uTable, UINT& uSize)
{
	PBYTE pFind = NULL;
	
	switch( uTable ) {

		case assyDataSend:

			pFind	 = m_pDataSend;
			uSize	 = m_wDataSend;
			m_fDData = TRUE;
			break;

		case assyDataRecv:

			pFind	 = m_pDataRecv;
			uSize	 = m_wDataRecv;
			break;
		
		case assyBitSend:

			pFind	 = m_pBitSend;
			uSize	 = m_wBitSend;
			break;
		
		case assyBitRecv:

			pFind	 = m_pBitRecv;
			uSize	 = m_wBitRecv;
			break;
		
		case assyPollSend:

			pFind	 = m_pPollSend;
			uSize	 = m_wPollSend;
			m_fDPoll = TRUE;
			break;
		
		case assyPollRecv:

			pFind = m_pPollRecv;
			uSize = m_wPollRecv;
			break;
		}

	return pFind;
	}

void CDeviceNetSlave::DoWrites(void)
{
	if( m_fDBit ) {

		m_fDBit = !COMMS_SUCCESS(m_pDevNet->Write(assyBitSend,  m_pBitSend,  m_wBitSend));
		}

	if( m_fDPoll ) {

		m_fDPoll = !COMMS_SUCCESS(m_pDevNet->Write(assyPollSend, m_pPollSend, m_wPollSend));
		}

	if( m_fDData ) {
	
		m_fDData = !COMMS_SUCCESS(m_pDevNet->Write(assyDataSend, m_pDataSend, m_wDataSend));
		}
	}


void CDeviceNetSlave::DoReads(void)
{
	UINT uCount = m_pDevNet->Read(assyBitRecv,  m_pBitRecv,  m_wBitRecv);

	if( COMMS_SUCCESS(uCount) && uCount ) {

		m_fDBit = TRUE;
		}
	
	m_pDevNet->Read(assyPollRecv, m_pPollRecv, m_wPollRecv);

	m_pDevNet->Read(assyDataRecv, m_pDataRecv, m_wDataRecv);
	}

// End of File
