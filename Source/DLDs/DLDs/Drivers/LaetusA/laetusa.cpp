#include "intern.hpp"

#include "laetusa.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Laetus Barcode Scanner Driver
//

// Command Definitions

//struct FAR LAETUSCmdDef {
//	UINT	uID;	// Table number
//	char	b[5];	// Transmit string
//	UINT	uOff;	// Use "uOffset" number of bytes
//	UINT	uResp;	// Response Type
//	};
	
LAETUSCmdDef CODE_SEG CLaetusSerialDriver::LaetusCL[] = {

	{SOM,	"1",	1,	RACK	},
	{LRS,	"LR",   0,	RBYT	},
	{LRD,	"LR",	0,	RSTR	},
	{LES,	"LE",	0,	RBYT	},
	{LED,	"LE",	0,	RSTR	},
	{LXC,	"LX",	0,	RACK	},
	{SRG,	"21",	0,	NORSP	},
	{MCF,	"3CV",	0,	RVAR	},
	{MC1,	"3CV",	0,	RSTR	},
	{MC2,	"3CV",	0,	RSTR	},
	{CCF,	"3CO",	0,	RVAR	},
	{CCS,	"3CO",	0,	RSTR	},
	{CCL,	"3CO",	0,	RSTR	},
	{DCF,	"3LK",	0,	RVAR	},
	{OPC,	"4CC",	4,	ROPC	},
	{HOC,	"3TF",	0,	RVAR	},
	{TFE,	"3TF",	0,	RVAR	},
	{TFH,	"3TF",	0,	RVAR	},
	{TFL,	"3TF",	0,	RVAR	},
	{TFM,	"3TF",	0,	RVAR	},
	{TFS,	"3TF",	0,	RVAR	},
	{TFT,	"3TF",	0,	RVAR	},
	{PE1,	"2TT",	0,	RSTR	},
	{PE2,	"2TT",	0,	RSTR	},
	{PE3,	"2TT",	0,	RSTR	},
	{PE4,	"2TT",	0,	RSTR	},
	{PEX,	"2TT",	0,	RLNG	},
	{SOC,	"3RO",	0,	RVAR	},
	{DST,	"15",	3,	RBYT	},
	{EEP,	"EEW",	0,	RACK	},

	{USR,	"ZZ",	0,	RSPC	},
	{USRC,	"ZZ",	0,	RSPC	},
	{USRR,	"ZZ",	0,	RSPC	},
	{SCK,	"ZZ",	0,	RSPC	},
	};

// Instantiator

INSTANTIATE(CLaetusSerialDriver);

// Constructor

CLaetusSerialDriver::CLaetusSerialDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_uWriteErr	= 0;

	m_pLItem	= NULL;

	m_uLRS1		= 0xFFFFFFFF;
	m_uLRS2		= 0xFFFFFFFF;

	m_uLES1		= 0xFFFFFFFF;
	m_uLES2		= 0xFFFFFFFF;

	m_uLCtr		= 0;

	CTEXT LMCFL[]	= "ABIJCDFGTR]";	// LLS570
	CTEXT LMCFC[]	= "ABIJCDFG]";		// COCAM7
	CTEXT LCCFL[]	= "@ALPMCDXST]";	// LLS570
	CTEXT LCCFC[]	= "@ALPMCXS]";		// COCAM7
	CTEXT LDCFL[]	= "LBSVMPQNOD]";	// LLS570
	CTEXT LDCFC[]	= "LSMRZPQNO]";		// COCAM7
	CTEXT LHOCL[]	= "HSTRLBFAMNE]";	// LLS570
	CTEXT LHOCC[]	= "HSTRLBFAM]";		// COCAM7
	CTEXT LSOCL[]	= "AWBXGVLOMNPQTRS]";	// LLS570
	CTEXT LSOCC[]	= "XRSTABCDEFGHIUVK]";	// COCAM7

	m_pMCFL		= LMCFL;
	m_pMCFC		= LMCFC;
	m_pCCFL		= LCCFL;
	m_pCCFC		= LCCFC;
	m_pDCFL		= LDCFL;
	m_pDCFC		= LDCFC;
	m_pHOCL		= LHOCL;
	m_pHOCC		= LHOCC;
	m_pSOCL		= LSOCL;
	m_pSOCC		= LSOCC;
	}

// Destructor

CLaetusSerialDriver::~CLaetusSerialDriver(void)
{
	}

// Configuration

void MCALL CLaetusSerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CLaetusSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}
	
// Management

void MCALL CLaetusSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CLaetusSerialDriver::Open(void)
{
	m_pLCL = (LAETUSCmdDef FAR *)LaetusCL;
	}

// Device

CCODE MCALL CLaetusSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop	= GetByte(pData);
			m_pCtx->m_bUnit	= GetByte(pData);

			m_pCtx->m_fSICK = FALSE;

			// Internal Status cache

			m_pCtx->m_dPEX  = 0xFFFFFFFF;

			memset(m_pCtx->m_bAckStatus, 0, sizeof(m_pCtx->m_bAckStatus));
			memset(m_pCtx->m_bSOMStatus, 0, sizeof(m_pCtx->m_bSOMStatus));

			m_pCtx->m_bCodeType = 'b';

			// Internal String Array caches
			memset(m_pCtx->m_dMCFC, 0, sizeof(m_pCtx->m_dMCFC));
			memset(m_pCtx->m_dMCFD, 0, sizeof(m_pCtx->m_dMCFD));
			memset(m_pCtx->m_dCCFC, 0, sizeof(m_pCtx->m_dCCFC));
			memset(m_pCtx->m_dCCFL, 0, sizeof(m_pCtx->m_dCCFL));
			memset(m_pCtx->m_dTFE,  0, sizeof(m_pCtx->m_dTFE ));
			memset(m_pCtx->m_dTFH,  0, sizeof(m_pCtx->m_dTFH ));
			memset(m_pCtx->m_dTFL,  0, sizeof(m_pCtx->m_dTFL ));
			memset(m_pCtx->m_dTFM,  0, sizeof(m_pCtx->m_dTFM ));
			memset(m_pCtx->m_dTFS,  0, sizeof(m_pCtx->m_dTFS ));
			memset(m_pCtx->m_dTFT,  0, sizeof(m_pCtx->m_dTFT ));
			memset(m_pCtx->m_bPE1,  0, sizeof(m_pCtx->m_bPE1 ));
			memset(m_pCtx->m_bPE2,  0, sizeof(m_pCtx->m_bPE2 ));
			memset(m_pCtx->m_bPE3,  0, sizeof(m_pCtx->m_bPE3 ));
			memset(m_pCtx->m_bPE4,  0, sizeof(m_pCtx->m_bPE4 ));
			memset(m_pCtx->m_bLRD,  0, sizeof(m_pCtx->m_bLRD ));
			memset(m_pCtx->m_bLED,  0, sizeof(m_pCtx->m_bLED ));
			memset(m_pCtx->m_dUSRC, 0, sizeof(m_pCtx->m_dUSRC));
			memset(m_pCtx->m_dUSRR, 0, sizeof(m_pCtx->m_dUSRR));

			pDevice->SetContext(m_pCtx);

			m_fIsLLS = m_pCtx->m_bUnit == LLS57;

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CLaetusSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		delete m_pLCL;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CLaetusSerialDriver::Ping(void)
{
//**/	AfxTrace0("PING ");

	m_fInPing = TRUE;

	return SetMode(1) ? 1 : CCODE_ERROR;
	}

CCODE MCALL CLaetusSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( NoReadTransmit(Addr, pData, uCount) ) {

		return uCount;
		}

//**/	AfxTrace3("\r\n\n*Read T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( DoSetMode(Addr.a.m_Table) ) {

		return DoRead(Addr.a.m_Offset, pData, uCount);
		}

	return CCODE_ERROR | (!m_pLItem ? CCODE_HARD : 0);
	}

CCODE MCALL CLaetusSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace0("\r\n##"); AfxTrace3("\r\n##### WRITE T=%d O=%d C=%d #####\r\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( NoWriteTransmit(Addr, pData, uCount) ) return uCount;

	if( DoSetMode(Addr.a.m_Table) ) {

		if( !IsStringItem(Addr.a.m_Table) ) uCount = 1;

		if( uCount != UINT(DoWrite(pData, Addr.a.m_Offset, uCount)) ) {

			if( ++m_uWriteErr < 2 ) return CCODE_ERROR;

			m_uWriteErr = 0;
			}

		return uCount;
		}

	return CCODE_ERROR | (!m_pLItem ? CCODE_HARD : 0);
	}

// Implementation

BOOL CLaetusSerialDriver::DoSetMode(UINT uTable)
{
	if( uTable == USR ) {

		m_pLItem = SetLaetusCommand(USR);

		return TRUE;
		}

	if( SetMode(1) ) {
		
		m_pLItem = SetLaetusCommand(uTable);

		LAETUSCmdDef * pItem = m_pLItem;

		BOOL fOk = TRUE;

		switch( pItem->b[0] ) {

			case '3': fOk = SetMode(3);	break;
			case '4': fOk = SetMode(4);	break;
			}

		m_pLItem = pItem;

		return fOk;
		}

	return FALSE;
	}

BOOL CLaetusSerialDriver::SetMode(UINT uMode)
{
	m_pLItem = SetLaetusCommand(SOM);

	StartFrame();

	AddByte(m_pHex[uMode]);

	BOOL fResult = FALSE;

	if( Transact() && (m_bRx[0] == m_pCtx->m_bDrop) ) {

		fResult = m_fInPing ? TRUE : m_bRx[1] == ACK;

		m_pCtx->m_bSOMStatus[1] = m_bRx[1];
		}

	m_fInPing = FALSE;

	return fResult;
	}

// Frame Building

void CLaetusSerialDriver::StartFrame(void)
{
	m_uPtr = 0;

	memset(m_bRx, 0, sizeof(m_bRx));

	AddByte(GetTxDelimiter(STARTDELIM));

	if( !m_pCtx->m_fSICK ) AddByte(m_pCtx->m_bDrop);

	if( m_pLItem->uResp != RSPC ) {

		if( m_pLItem->uID != SOC || m_fIsLLS ) {

			AddByte(m_pLItem->b[0]);
			}

		else {
			AddByte('E'); // SOC command different
			}
		}
	}

void CLaetusSerialDriver::EndFrame(void)
{
	AddByte(GetTxDelimiter(ENDDELIM));

	if( m_pCtx->m_fSICK ) return; // no checksum

	BYTE bChecksum = 0;

	for( UINT i = 0; i < m_uPtr; i++ ) {

		bChecksum ^= m_bTx[i];
		}

	AddHex(bChecksum);
	}

void CLaetusSerialDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) m_bTx[m_uPtr++] = bData;
	}

void CLaetusSerialDriver::AddHex(BYTE bData)
{
	AddByte(m_pHex[bData/16]);
	AddByte(m_pHex[bData%16]);
	}

void CLaetusSerialDriver::AddText(PCTXT pData)
{
	for( UINT i = 0; pData[i]; i++ ) {

		AddByte(pData[i]);
		}
	}
		
// Transport Layer

void CLaetusSerialDriver::PutFrame(void)
{
	EndFrame();

	m_pData->ClearRx();

//**/	AfxTrace0("\r\n");
//**/	UINT k = 0;
//**/	while( k < m_uPtr && m_bTx[k] >= ' ' ) {

//**/		AfxTrace1("%c", m_bTx[k]);
//**/		k++;
//**/		}
//**/	while( k < m_uPtr ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

BOOL CLaetusSerialDriver::GetFrame(void)
{
	m_uTerminator = sizeof(m_bRx);

	BOOL fAck     = m_pLItem->uResp == RACK;

	BYTE bLRC     = 0;
	BYTE bData;

	BYTE bStart   = GetRxDelimiter(STARTDELIM);
	BYTE bEnd     = GetRxDelimiter(ENDDELIM);

	UINT uPtr     = 0;
	UINT uTimer   = 1000;
	UINT uState   = 0;
	UINT uData;

//**/	AfxTrace0("\r\n");

	SetTimer(uTimer);

	while( (uTimer = GetTimer()) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

		if( uState < 2 ) bLRC ^= bData;

		if( uPtr < sizeof(m_bRx) ) m_bRx[uPtr++] = bData;

//**/		if( bData >= ' ' ) AfxTrace1("%c", bData);
//**/		else if( bData == ACK ) AfxTrace0("** ACK **");
//**/		else if( bData == NAK ) AfxTrace0("-- NAK --");
//**/		else AfxTrace1("<%2.2x>", bData);

		if( bData == ACK || bData == NAK ) {

			return TRUE;
			}

		switch( uState ) {

			case 0:
				if( bData == bStart || fAck ) {

					bLRC   = bData;

					uState = 1;
					}					

				break;

			case 1:
				if( bData == bEnd ) {

					m_uTerminator = uPtr-1;

					if( bEnd == ']' ) uState = 2;

					else return TRUE;
					}

				break;

			case 2:
				uState = m_pHex[bLRC >> 4] == bData ? 3 : 4;
				break;

			case 3:
				return m_pHex[bLRC & 0xF] == bData;

			case 4: return FALSE;
			}
		}

	return TRUE; //FALSE;
	}

BOOL CLaetusSerialDriver::Transact(void)
{
	PutFrame();

	return GetFrame();
	}

BOOL CLaetusSerialDriver::CheckRsp(void)
{
//	Simulate();

	if( m_pLItem->uResp == RACK ) {

		return (m_bRx[0] == m_pCtx->m_bDrop) && (m_bRx[1] == ACK || m_bRx[1] == NAK);
		}

	if( m_bRx[0] == m_pCtx->m_bDrop && m_bRx[1] == NAK && m_bTx[3] == '?' ) { // NAK on Read, handle in DoRead

		return TRUE;
		}

	if( m_bRx[0] != GetRxDelimiter(STARTDELIM) )	return FALSE;
	if( m_bRx[1] != m_pCtx->m_bDrop )		return FALSE;

	if( m_pLItem->uID == LRS || m_pLItem->uID == LES ) {

		return TRUE;
		}

	if( m_bRx[2] != m_pLItem->b[0]  )		return FALSE;

	return m_bRx[3] == m_pLItem->b[1];
	}

// Port Access

UINT CLaetusSerialDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Read Handlers *********************************************************
// ***********************************************************************

CCODE CLaetusSerialDriver::DoRead(UINT uOffset, PDWORD pData, UINT uCount)
{
	StartFrame();

	AddByte('?');

	if( m_pLItem->uID != SOC || m_fIsLLS ) {

		for( UINT i = 1; i < strlen(m_pLItem->b); i++ ) {

			AddByte(m_pLItem->b[i]);
			}
		}

	else {
		AddByte('X');
		}

	if( m_pLItem->uID == CCF ) {

		AddByte(m_pCtx->m_bCodeType);
		}

	if( Transact() && CheckRsp() ) {

		if( m_bRx[0] == m_pCtx->m_bDrop && m_bRx[1] == NAK ) {

			switch( m_pLItem->uID ) {

				case LRS:
					*pData = ToggleL(TRUE);
					break;

				case LES:
					*pData = ToggleL(FALSE);
					break;

				default:
					*pData = 0xFFFFFFFF;
					break;
				}

			return 1;
			}

		return GetReadResponse(uOffset, pData, uCount);
		}

	return CCODE_ERROR;
	}

CCODE CLaetusSerialDriver::GetReadResponse( UINT uOffset, PDWORD pData, UINT uCount )
{
	LAETUSCmdDef * p = m_pLItem;

	CCODE ccResult   = CCODE_ERROR;

	UINT uPos        = 5;

	UINT i;

	UINT uEnd        = m_uTerminator;

	SkipSpaceChars(&uPos);

	switch( p->uResp ) {

		case RBYT:
			if( m_pLItem->uID == LRS ) {

				*pData = DWORD(SaveReadString(m_pCtx->m_bLRD));

				m_uLRS1 = *pData;

				m_uLRS2 = m_uLRS1;

				return 1;
				}

			if( m_pLItem->uID == LES ) {

				*pData = (SaveReadString(m_pCtx->m_bLED));

				m_uLES1 = *pData;

				m_uLES2 = m_uLES1;

				return 1;
				}

			for( i = 0; i < uCount; i++ ) {

				pData[i] = MotorToHost(GetNextRespValue(&uPos, 2));
				}

			return uCount;

		case RWRD:
			for( i = 0; i < uCount; i++ ) {

				pData[i] = MotorToHost(GetNextRespValue(&uPos, 4));
				}

			return uCount;

		case RLNG:
			for( i = 0; i < uCount; i++ ) {

				pData[i] = MotorToHost(GetNextRespValue(&uPos, 8));
				}

		case RVAR:
			if( GetMultiData(uOffset, pData, uCount) ) return uCount;
			break;

		case ROPC:
			return GetOPCItem(uOffset, pData, uCount);
		}

	return ccResult ? ccResult : CCODE_ERROR;
	}

UINT  CLaetusSerialDriver::GetMultiData(UINT uOffset, PDWORD pData, UINT uCount)
{
	switch( m_pLItem->uID ) {

		case MCF:
			return GetMCFItem(uOffset, pData, uCount);

		case CCF:
			return GetCCFItem(uOffset, pData, uCount);

		case DCF:
			return GetDCFItem(uOffset, pData, uCount);

		case HOC:
			return GetHOCItem(uOffset, pData, uCount);

		case SOC:
			return GetSOCItem(uOffset, pData, uCount);
		}

	return 0;
	}

void CLaetusSerialDriver::ProcessMulti(PDWORD pData, UINT uOffset, UINT uCount, PBYTE pLst, PDWORD *pStrDest)
{
	PDWORD pDest = pStrDest[1];

	for( UINT i = 0; i < uCount; i++ ) {

		BOOL fFirst = TRUE;	// String Start Flag

		BYTE b = m_pList[uOffset+i];

		if( b == ']' ) { // end of list

			return;
			}

		BYTE bNext = m_pList[uOffset + i + 1];

		UINT uPos = FindCharMatch(b, m_fIsCCF);	// Find this letter in receive buffer

		if( uPos ) {

			BYTE bType;

			DWORD dData = 0;

			UINT uSize  = GetItemInfo(b, &bType);

			if( bType == STRITEM ) {

				UINT uStrSz;

				if( m_fIsCCF && (b == 'C' || b == 'L') ) {

					uStrSz = FindCharMatch(bNext, TRUE) - uPos - 1;
					}

				else uStrSz = GetStringLength(b, &uPos);

				uStrSz      = min(uStrSz, uSize);

				UINT uStrPos;

				if( (uStrPos = FindDestPos(pLst, b)) <= pLst[0]) {

					UINT uDWDCt = 0;

					if( fFirst ) {

						pData[i] = uStrSz;
	
						pDest    = pStrDest[uStrPos];

						memset(pDest, 0, uSize); // Clear cache contents

						if( uStrSz ) {

							uDWDCt = CHCT2DWORD(uStrSz);
							}

						fFirst   = FALSE;
						}

					if( uStrSz ) {

						StringToCache((PDWORD)&m_bRx[uPos], pDest, uDWDCt, uSize/sizeof(DWORD));
						}

					PBYTE p   = PBYTE(pDest);

					p[uStrSz] = 0;
					}
				}

			else {
				pData[i] = GetMultiDWD(pData, uSize, bType, &uPos);
				}
			}
		}
	}

UINT CLaetusSerialDriver::FindDestPos(PBYTE pLst, BYTE b)
{
	for( UINT i = 1; i <= pLst[0]; i++ ) {

		if( b == pLst[i] ) {

			return i;
			}
		}

	return 100;
	}

UINT CLaetusSerialDriver::GetMCFItem(UINT uOffset, PDWORD pData, UINT uCount)
{
	m_pList     = m_fIsLLS ? m_pMCFL : m_pMCFC;

	PDWORD dStrDest[3];
	dStrDest[1] = m_pCtx->m_dMCFC;
	dStrDest[2] = m_pCtx->m_dMCFD;

	BYTE bChar[3];
	bChar[0]    =  2;
	bChar[1]    = 'C';
	bChar[2]    = 'D';

	ProcessMulti(pData, uOffset, uCount, bChar, dStrDest);

	return uCount;
	}

UINT CLaetusSerialDriver::GetCCFItem(UINT uOffset, PDWORD pData, UINT uCount)
{
	if( !uOffset ) { // handle inital value that has no identifier

		UINT uPos = 5; // skip over [n3CO

		SkipSpaceChars(&uPos);

		BYTE b = m_bRx[uPos];

		*pData = U4(b);

		if( uCount == 1 ) {

			return 1;
			}

		uOffset++; // next offset

		pData++;   // next data
		}

	m_pList     = m_fIsLLS ? m_pCCFL : m_pCCFC;

	PDWORD dStrDest[3];
	dStrDest[1] = m_pCtx->m_dCCFC;
	dStrDest[2] = m_pCtx->m_dCCFL;

	BYTE bChar[3];
	bChar[0]    =  2;
	bChar[1]    = 'C';
	bChar[2]    = 'L';

	ProcessMulti(pData, uOffset, uCount, bChar, dStrDest);

	return uCount;
	}

UINT CLaetusSerialDriver::GetDCFItem(UINT uOffset, PDWORD pData, UINT uCount)
{
	m_pList  = m_fIsLLS ? m_pDCFL : m_pDCFC;

	BYTE bChar[1];
	bChar[0] = 0;

	PDWORD dStrDest[1];

	ProcessMulti(pData, uOffset, uCount, bChar, dStrDest);

	return uCount;
	}

UINT CLaetusSerialDriver::GetHOCItem(UINT uOffset, PDWORD pData, UINT uCount)
{
	m_pList     = m_fIsLLS ? m_pHOCL : m_pHOCC;

	PDWORD dStrDest[7];
	dStrDest[1] = m_pCtx->m_dTFE;
	dStrDest[2] = m_pCtx->m_dTFH;
	dStrDest[3] = m_pCtx->m_dTFL;
	dStrDest[4] = m_pCtx->m_dTFM;
	dStrDest[5] = m_pCtx->m_dTFS;
	dStrDest[6] = m_pCtx->m_dTFT;

	BYTE bChar[7];
	bChar[0]    =  6;
	bChar[1]    = 'E';
	bChar[2]    = 'H';
	bChar[3]    = 'L';
	bChar[4]    = 'M';
	bChar[5]    = 'S';
	bChar[6]    = 'T';

	ProcessMulti(pData, uOffset, uCount, bChar, dStrDest);

	return uCount;
	}

UINT CLaetusSerialDriver::GetSOCItem(UINT uOffset, PDWORD pData, UINT uCount)
{
	m_pList  = m_fIsLLS ? m_pSOCL : m_pSOCC;

	BYTE bChar[1];

	bChar[0] = 0;

	PDWORD dStrDest[1];

	ProcessMulti(pData, uOffset, uCount, bChar, dStrDest);

	return uCount;
	}

UINT CLaetusSerialDriver::GetOPCItem(UINT uOffset, PDWORD pData, UINT uCount)
{
	UINT uPos = 6 + ((uOffset - 1) * 9);

	for( UINT i = 0; i < uCount; i++, uPos += 1 ) {

		pData[i] = GetNextRespValue(&uPos, 8);
		}

	return uCount;
	}

DWORD CLaetusSerialDriver::GetMultiDWD(PDWORD pData, UINT uSize, BYTE bType, UINT *pPos)
{
	switch( bType ) {

		case DECITEM:	return GetDecRespValue(pPos, uSize);

		case CHITEMU:	return GetChrRespValue(pPos, uSize, 'A');

		case CHITEML:	return GetChrRespValue(pPos, uSize, 'a');

		case HEXITEM:	return GetHexRespValue(pPos, uSize);

		case MIXITEM:	return GetMixRespValue(pPos, uSize);
		}

	return 0;
	}

void CLaetusSerialDriver::SavePEXString(void)
{
	UINT uPos  = 5;
	UINT uSel  = 0;
	UINT uMax  = 80;

	PBYTE pbDat = m_pCtx->m_bPE1;

	while( uPos < m_uTerminator - 1 ) {

		uPos  = SaveStringData(pbDat, uPos, uMax, 0xD, 0xA);

		uSel++;

		switch( uSel ) {

			case 1:
				pbDat = m_pCtx->m_bPE2;
				uMax  = 32;
				break;

			case 2:
				pbDat = m_pCtx->m_bPE3;
				uMax  = 80;
				break;

			case 3:
				pbDat = m_pCtx->m_bPE4;
				uMax  = 80;
				break;

			case 4:
				return;
			}

		uPos += 2;
		}
	}

BYTE CLaetusSerialDriver::SaveReadString(PBYTE pData)
{
// The specification says the response echoes the command.
// The demo unit provided did not, so set the start position accordingly.
	UINT  uPos = m_bRx[2] == 'L' ? 4 : 2;

	BYTE bStat = FromHex(m_bRx[uPos++]);

	SkipSpaceChars(&uPos);

	SaveStringData(pData, uPos, MAXLRDSZ, 0, 0);

	return bStat;
	}

// Write Handlers ********************************************************
// ***********************************************************************

CCODE CLaetusSerialDriver::DoWrite( PDWORD pData, UINT uOffset, UINT uCount )
{
	UINT i;

	StartFrame();

	LAETUSCmdDef * p = m_pLItem;

	if( p->uID != SRG ) {

		for( i = 1; i < strlen(p->b); i++ ) AddByte(p->b[i]);
		}

	if( p->b[0] == 'Z' ) {

		HandleSpecial(pData, uCount, FALSE);
		}

	else {
		switch( p->uResp ) {

			case RVAR:

				switch( p->uID ) {

					case MCF: AddMCFData(pData, uOffset, uCount);	break;
					case CCF: AddCCFData(pData, uOffset, uCount);	break;
					case DCF: AddDCFData(pData, uOffset, uCount);	break;
					case HOC: AddHOCData(pData, uOffset, uCount);	break;
					case SOC: AddSOCData(pData, uOffset, uCount);	break;
					}

				break;

			case RSTR:

				UINT uMax;
				BYTE bOp;
				BOOL fMC;

				uMax = 50;
				bOp  = 'C';
				fMC  = FALSE;

				switch( p->uID ) {

					case MC1:		fMC =  TRUE;	break;
					case MC2: bOp  =   'D'; fMC =  TRUE;	break;
					case CCS:		uMax =  4;	break;
					case CCL: bOp  =   'L';	uMax = 17;	break;
					}

				if( !AddRSTRData(pData, uCount, uMax, bOp, fMC ) ) {

					return uCount;
					}
				break;

			default:
				AddWriteData(pData, uOffset, uCount);
				break;
			}
		}

	if( Transact() ) {

		if( m_bRx[1] != NAK ) m_uWriteErr = 0;

		switch( p->uResp ) {

			case RACK:
				SaveAckStatus(uOffset);
				break;

			case NORSP:
				if( m_fIsLLS ) {

					SaveAckStatus(uOffset);
					}
				break;

			case RLNG:
				if( m_pLItem->uID == PEX ) {

					if( m_bRx[1] != NAK ) {

						m_pCtx->m_dPEX = 0;

						SavePEXString();
						}

					else {
						m_pCtx->m_dPEX = 0xFFFFFFFF;
						}
					}
				break;

			case RBYT:
				if( m_pLItem->uID == DST ) {

					SetMode(1);
					}
				break;

			case RSPC:
				if( m_pLItem->uID == USR ) {

					memset(m_pCtx->m_dUSRR, 0, MAXUSRSZ * 4);

					StringToCache((PDWORD)m_bRx, m_pCtx->m_dUSRR, MAXUSRSZ * 4, MAXUSRSZ);
					}

				break;
			}

		return 1;
		}

	return CCODE_ERROR;
	}

void CLaetusSerialDriver::AddWriteData(PDWORD pData, UINT uOffset, UINT uCount)
{
	switch( m_pLItem->uID ) {

		case SOM: AddByte(m_pHex[uOffset & 0xF]); return;
		case DST: return;
		}

	for( UINT i = 0; i < uCount; i++ ) {

		switch( m_pLItem->uResp ) {

			case RBYT:	AddByte(m_pHex[LOBYTE(pData[i])]);	break;

			case NORSP:	AddByte(m_pHex[*pData]);		break;

			case RLNG:	break;
			}
		}
	}

void CLaetusSerialDriver::AddMultiData(PDWORD pData, UINT uOffset, UINT uCount, PBYTE pLst, PDWORD *pSrcStr)
{
	for( UINT i = 0; i < uCount; i++ ) {

		PBYTE pList = PBYTE(m_pList);

		BYTE b      = pList[uOffset + i];

		if( b == ']' ) return;

		AddByte(b);

		BYTE bType;
		BYTE bMax;

		UINT uSize = GetItemInfo(b, &bType);

		if( bType == STRITEM ) {

			for( UINT uItem = 0; uItem < uSize; uItem++ ) {

				if( b == pLst[uItem] ) {

					PDWORD p = pSrcStr[uItem];

					AddByte(BYTE(strlen((const char *)p)));

					for( UINT k = 0; p[k] && (k < uSize); k++ ) {

						AddByte(p[k]);
						}
					}
				}
			}

		else {
			DWORD dVal = pData[i];
			BYTE  bVal = LOBYTE(pData[i]);

			switch( bType ) {

				case DECITEM:
					PutDecimal(dVal, uSize);
					break;

				case CHITEMU:
					if( dVal > ' ' ) AddByte(bVal);

					else AddByte('A' + bVal);

					break;

				case CHITEML:
					if( dVal >= ' ' ) AddByte(bVal);

					else AddByte('a' + bVal);

					break;

				case HEXITEM:
					if( uSize == 2 ) AddByte(m_pHex[HIBYTE(LOWORD(pData[i])) / 16]);

					AddByte(m_pHex[bVal]);
					break;
				}
			}

		}
	}

void CLaetusSerialDriver::AddMCFData(PDWORD pData, UINT uOffset, UINT uCount)
{
	m_pList    = m_fIsLLS ? m_pMCFL : m_pMCFC;

	PDWORD pSource[3];
	pSource[1] = m_pCtx->m_dMCFC;
	pSource[2] = m_pCtx->m_dMCFD;

	BYTE bChar[3];

	bChar[0]   =  2;	// 2 elements
	bChar[1]   = 'C';
	bChar[2]   = 'D';

	AddMultiData(pData, uOffset, uCount, bChar, pSource);
	}

BOOL CLaetusSerialDriver::AddRSTRData(PDWORD pData, UINT uCount, UINT uMax, BYTE bOp, BOOL fMC)
{
	PBYTE	pByte;
	pByte = PBYTE(pData);

	UINT	uSize = uCount * sizeof(DWORD);

	uMax          = min(uSize, uMax);

	uSize         = 0;

	UINT        i = 0;

	while( i < uMax ) {

		if( pByte[i++] < ' ' ) {

			uSize = i - 1;
			break;
			}
		}

	if( !uSize ) {

		return FALSE;
		}

	AddByte(bOp);

	if( fMC ) {

		AddByte(m_pHex[uSize/10]);
		AddByte(m_pHex[uSize%10]);
		}

	memcpy(&m_bTx[m_uPtr], pByte, uSize);

	m_uPtr += uSize;

	if( !fMC ) {

		AddByte('0');
		AddByte('0');
		}

	return TRUE;
	}

void CLaetusSerialDriver::AddCCFData(PDWORD pData, UINT uOffset, UINT uCount)
{
	m_pList    = m_fIsLLS ? m_pCCFL : m_pCCFC;

	PDWORD pSource[3];
	pSource[1] = m_pCtx->m_dCCFC;
	pSource[2] = m_pCtx->m_dCCFL;

	BYTE bChar[3];

	bChar[0]   =  2; // two elements
	bChar[1]   = 'C';
	bChar[2]   = 'L';

	AddMultiData(pData, uOffset, uCount, bChar, pSource);
	}

void CLaetusSerialDriver::AddDCFData(PDWORD pData, UINT uOffset, UINT uCount)
{
	m_pList  = m_fIsLLS ? m_pDCFL : m_pDCFC;

	PDWORD pSource[1];

	BYTE bChar[1];

	bChar[0] = 0;

	AddMultiData(pData, uOffset, uCount, bChar, pSource);
	}

void CLaetusSerialDriver::AddHOCData(PDWORD pData, UINT uOffset, UINT uCount)
{
	m_pList    = m_fIsLLS ? m_pHOCL : m_pHOCC;

	PDWORD pSource[7];

	pSource[1] = m_pCtx->m_dTFE;
	pSource[2] = m_pCtx->m_dTFH;
	pSource[3] = m_pCtx->m_dTFL;
	pSource[4] = m_pCtx->m_dTFM;
	pSource[5] = m_pCtx->m_dTFS;
	pSource[6] = m_pCtx->m_dTFT;

	BYTE bChar[13];

	bChar[0]   =  6;
	bChar[1]   = 'E';
	bChar[2]   = 'H';
	bChar[3]   = 'L';
	bChar[4]   = 'M';
	bChar[5]   = 'S';
	bChar[6]   = 'T';

	AddMultiData(pData, uOffset, uCount, bChar, pSource);
	}

void CLaetusSerialDriver::AddSOCData(PDWORD pData, UINT uOffset, UINT uCount)
{
	m_pList  = m_fIsLLS ? m_pSOCL : m_pSOCC;

	PDWORD pSource[1];

	BYTE bChar[1];

	bChar[0] = 0;

	AddMultiData(pData, uOffset, uCount, bChar, pSource);
	}

UINT CLaetusSerialDriver::GetItemInfo(BYTE bItem, PBYTE pType)
{
	switch( m_pLItem->uID ) {

		case MCF:	return MCFItemInfo(bItem, pType);
		case CCF:	return CCFItemInfo(bItem, pType);
		case DCF:	return DCFItemInfo(bItem, pType);
		case HOC:	return HOCItemInfo(bItem, pType);
		case SOC:	return SOCItemInfo(bItem, pType);
		}

	*pType = STRITEM;
	return 1;
	}

UINT CLaetusSerialDriver::MCFItemInfo(BYTE bItem, PBYTE pType)
{
	if( bItem == 'C' || bItem == 'D' ) {

		*pType = STRITEM;
		return 50;
		}

	if( bItem == 'I' || bItem == 'J' ) {

		*pType = CHITEML;
		return 1;
		}

	*pType = DECITEM;
	return 1;
	}

UINT CLaetusSerialDriver::CCFItemInfo(BYTE bItem, PBYTE pType)
{
	*pType = DECITEM;

	switch( bItem ) {

		case '@':
			*pType = CHITEML;
			return 1;

		case 'A':
		case 'D':
		case 'P':
		case 'T':
		case 'X':
			return 1;

		case 'M':
			return 2;

		case 'S':
			return 3;

		case 'C':
			*pType = STRITEM;
			return 4;

		case 'L':
			*pType = STRITEM;
			return 16; // max for this item, could be shorter
		}

	return 0;
	}

UINT CLaetusSerialDriver::DCFItemInfo(BYTE bItem, PBYTE pType)
{
	*pType = DECITEM;

	switch( bItem ) {

		case 'D':
		case 'N':
		case 'R':
		case 'V':
			return 1;

		case 'S':
			*pType = CHITEMU;
			return 1;

		case 'M':
			*pType = MIXITEM;
			return 2;

		case 'Z':
			return 2;

		case 'B':
		case 'O':
		case 'P':
		case 'Q':
			return 3;

		case 'L':
			return 4;
		}

	return 0;
	}

UINT CLaetusSerialDriver::HOCItemInfo(BYTE bItem, PBYTE pType)
{
	*pType = DECITEM;

	switch( bItem ) {

		case 'A':
		case 'B':
			return 2;

		case 'E':
			*pType = STRITEM;
			return   HOCESIZE;

		case 'F':
			return 1;

		case 'H':
		case 'T':
			*pType = STRITEM;
			return   HOCHSIZE;

		case 'L':
			*pType = STRITEM;
			return   HOCLSIZE;

		case 'M':
			*pType = STRITEM;
			return   HOCMSIZE;

		case 'N':
			*pType = HEXITEM;
			return 2;

		case 'R':
			return 1;

		case 'S':
			*pType = STRITEM;
			return   HOCSSIZE;
		}

	return 0;
	}

UINT CLaetusSerialDriver::SOCItemInfo(BYTE bItem, PBYTE pType)
{
	*pType    = DECITEM;

	switch( bItem ) {

		case 'A':
		case 'B':
		case 'G':
			if( !m_fIsLLS ) {

				*pType = HEXITEM;

				return 1;
				}

			return 2;

		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'H':
		case 'U':
		case 'W':
			*pType = HEXITEM;

			return 1;

		case 'R':
			if( !m_fIsLLS ) *pType = HEXITEM;

			return 1;

		case 'X':
			if(  m_fIsLLS ) *pType = HEXITEM;
			
			return 1;

		case 'I':
			*pType = HEXITEM;
			
			return 2;

		case 'K':
		case 'N':
		case 'Q':
		case 'S':
		case 'V':
			return 1;

		case 'M':
		case 'P':
		case 'T':
			return 2;

		case 'L':
		case 'O':
			return 8;
		}

	return 0;
	}

UINT CLaetusSerialDriver::FindNextCR(UINT uStartP)
{
	BYTE bEnd   = GetRxDelimiter(ENDDELIM);

	for( UINT i = uStartP; i <= m_uTerminator; i++ ) {

		BYTE b = m_bRx[i];

		if( !b || b == bEnd ) {

			return m_uTerminator;
			}

		if( b == CR ) {

			return i;
			}
		}

	return m_uTerminator;
	}

// String and character handling *****************************************
// ***********************************************************************

UINT CLaetusSerialDriver::FindCharMatch(BYTE bList, BOOL fIgnoreCCFString)
{
	BYTE bEnd = GetRxDelimiter(ENDDELIM);

	UINT uPos = 5; // uPos -> skip "[mXXX" (XXX = MCF, CCF, DCF, HOC, SOC)

	if( m_fIsCCF ) {

		SkipSpaceChars(&uPos);

		uPos++; // skip item with no ID
		} 

	while( uPos < m_uTerminator && uPos < sizeof(m_bRx) ) {

		SkipSpaceChars(&uPos);

		BYTE bI = m_bRx[uPos++];

		if( bI == bList ) {

			return uPos; // first byte of data
			}

		if( fIgnoreCCFString ) {

			continue;
			}

		BYTE bT;

		UINT uSize = GetItemInfo(bI, &bT);

		UINT uOldP = uPos;

		if( bT == STRITEM ) {

			if( StringHasSize(bI) ) {

				uPos += GetLength(&uOldP) + 2;
				}

			else {
				uPos += GetStringLength(bI, &uOldP);
				}
			}

		else uPos += uSize;

		if( uPos <= uOldP ) {

			return 0;
			}
		}

	return 0;
	}

void CLaetusSerialDriver::SkipSpaceChars(UINT *pPos)
{
	UINT uPos = *pPos;

	for( UINT i = uPos; i < sizeof(m_bRx); i++ ) {

		if( m_bRx[i] != ' ' ) {

			*pPos = i;

			return;
			}
		}
	}

UINT  CLaetusSerialDriver::SaveStringData(PBYTE pData, UINT uPos, UINT uMax, BYTE b0, BYTE b1)
{
	BYTE bEnd = GetRxDelimiter(ENDDELIM);

	memset(pData, 0, uMax);

	UINT uStart = uPos;

	for( UINT i = 0; i < uMax; i++ ) {

		uPos   = uStart + i;

		BYTE b = GetNextRespChar(&uPos);

		if( b < ' ' || b == bEnd ) {

			b = 0;
			}

		if( b0 && b == b0 ) {

			if( !b1 || m_bRx[uPos] == b1 ) {

				b = 0;
				}
			}
		pData[i] = b;

		if( !b ) {

			return uPos - 1; // position of 1st terminating char
			}
		}

	return m_uTerminator;
	}

BOOL CLaetusSerialDriver::StringHasSize(BYTE b)
{
	switch( m_pLItem->uID ) {

		case MCF:
			return b == 'C' || b == 'D';
		}

	return FALSE;
	}

UINT CLaetusSerialDriver::GetStringLength(BYTE b, UINT *pPos)
{
	UINT uPos = *pPos;
	UINT i;

	switch( m_pLItem->uID ) {

		case MCF:
			return GetLength(pPos);

		case CCF:
			switch( b ) {

				case 'L':
					switch( m_bRx[uPos++] ) {

						case 'E': return 16;
						case 'I': return  7;
						case 'F': return  1;
						}

					return FindEndOfString(uPos);

				case 'C':
					return FindEndOfString(uPos+1);
				}
			break;

		case HOC:
			switch( b ) {

				case 'E':
					return FindDouble0(uPos, HOCESIZE);

				case 'H':
				case 'T':
					return FindDouble0(uPos, HOCHSIZE);

				case 'L':
					return FindDouble0(uPos, HOCLSIZE);

				case 'M':
					return FindDouble0(uPos, HOCMSIZE);

				case 'S':
					return FindDouble0(uPos, HOCSSIZE);
				}
			break;
		}

	return 0;
	}

UINT CLaetusSerialDriver::GetLength(UINT *pPos)
{
	UINT uPos = *pPos;

	UINT u    = (FromHex(m_bRx[uPos+0]) * 10) + FromHex(m_bRx[uPos+1]);

	*pPos     = uPos + 2;

	return u;
	}

UINT CLaetusSerialDriver::FindEndOfString(UINT uPos)
{
	for( UINT i = uPos; i < m_uTerminator; i++ ) {

		if( m_bRx[i] == StopChar(uPos+i) ) {

			return i;
			}
		}

	return 0;
	}

UINT CLaetusSerialDriver::FindDouble0(UINT uPos, UINT uMax)
{
	for( UINT i = uPos; i < uPos + uMax; i++ ) {

		if( m_bRx[i] == '0' && m_bRx[i+1] == '0' ) {

			return i + 2 - uPos;
			}
		}

	return 0;
	}

BOOL CLaetusSerialDriver::StopChar(UINT uPos)
{
	BYTE bEnd = GetRxDelimiter(ENDDELIM);

	if( m_bRx[uPos] == ' ' || m_bRx[uPos] == bEnd || uPos >= m_uTerminator ) {

		return TRUE;
		}

	if( m_bRx[uPos] >= 'A' && m_bRx[uPos] <= 'Z' ) {

		return TRUE;
		}

	return FALSE;
	}

void CLaetusSerialDriver::HandleSpecial(PDWORD pData, UINT uCount, BOOL fIsRead)
{
	switch( m_pLItem->uID ) {

		case LES:
			break;

		case USR:
			m_uPtr = 0;

			memset(m_bRx, 0, sizeof(m_bRx));

			AddByte(GetTxDelimiter(STARTDELIM));

			AddByte(m_pCtx->m_bDrop);

			UINT i;

			PBYTE pC;

			pC = PBYTE(m_pCtx->m_dUSRC);

			BYTE b;

			for( i = 0; i < MAXUSRSZ * 4; i++ ) {

				b = pC[i];

				if( !b ) {

					return;
					}

				AddByte(b);
				}
			break;
		}
	}

void CLaetusSerialDriver::GetCachedData(PDWORD pData, PDWORD pCache, UINT uCount)
{
	for( UINT i = 0; i < uCount; i++ ) {

		pData[i] = pCache[i];
		}
	}

void CLaetusSerialDriver::StringToCache(PDWORD pData, PDWORD pCache, UINT uCount, UINT uMax)
{
	memset(pCache, 0, uMax);

	for( UINT i = 0; i < uMax; i++ ) {

		pCache[i] = i < uCount ? pData[i] : 0;
		}

	if( pData[i-1] & 0xF && uCount < uMax ) {

		pData[i] = 0; // attach null when uCount == 0 mod 4
		}
	}

BOOL CLaetusSerialDriver::IsStringItem(UINT uTable)
{
	return m_pLItem->uResp == RSTR;
	}

// Helpers ***************************************************************
// ***********************************************************************

LAETUSCmdDef * CLaetusSerialDriver::SetLaetusCommand(UINT uTable)
{
	LAETUSCmdDef * p = m_pLCL;

	for( UINT i = 0; i < elements(LaetusCL); i++ ) {

		if( uTable == p->uID ) return p;

		p++;
		}

	return NULL;
	}

BOOL CLaetusSerialDriver::NoReadTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	PDWORD pCache	= m_pCtx->m_dUSRC;

	UINT uT		= Addr.a.m_Table;

	BOOL fCache	= FALSE;

	m_fIsCCF	= Addr.a.m_Table == CCF;

	UINT i;

	switch( uT ) {

		case SOM:
			for( i = 0; i < uCount; i++ ) {

				BYTE b   = m_pCtx->m_bSOMStatus[Addr.a.m_Offset + i];

				pData[i] = DWORD(b);
				}

			return TRUE;

		case LXC:
			*pData = DWORD(m_pCtx->m_bAckStatus[uT]);
			return TRUE;

		case SRG:
			*pData = m_fIsLLS ? DWORD(m_pCtx->m_bAckStatus[uT]) : 0;
			return TRUE;

		case LRD:
			pCache = (PDWORD)m_pCtx->m_bLRD;
			fCache = TRUE;
			break;

		case LED:
			pCache = (PDWORD)m_pCtx->m_bLED;
			fCache = TRUE;
			break;

		case SCK:
			m_pCtx->m_fSICK = *pData ? TRUE : FALSE;
			return TRUE;

		case MC1:
			pCache = m_pCtx->m_dMCFC;
			fCache = TRUE;
			break;

		case MC2:
			pCache = m_pCtx->m_dMCFD;
			fCache = TRUE;
			break;

		case CCS:
			pCache = m_pCtx->m_dCCFC;
			fCache = TRUE;
			break;

		case CCL:
			pCache = m_pCtx->m_dCCFL;
			fCache = TRUE;
			break;

		case TFE:
			pCache = m_pCtx->m_dTFE;
			fCache = TRUE;
			break;

		case TFH:
			pCache = m_pCtx->m_dTFH;
			fCache = TRUE;
			break;

		case TFL:
			pCache = m_pCtx->m_dTFL;
			fCache = TRUE;
			break;

		case TFM:
			pCache = m_pCtx->m_dTFM;
			fCache = TRUE;
			break;

		case TFS:
			pCache = m_pCtx->m_dTFS;
			fCache = TRUE;
			break;

		case TFT:
			pCache = m_pCtx->m_dTFT;
			fCache = TRUE;
			break;

		case PE1:
			pCache = (PDWORD)m_pCtx->m_bPE1;
			fCache = TRUE;
			break;

		case PE2:
			pCache = (PDWORD)m_pCtx->m_bPE2;
			fCache = TRUE;
			break;

		case PE3:
			pCache = (PDWORD)m_pCtx->m_bPE3;
			fCache = TRUE;
			break;

		case PE4:
			pCache = (PDWORD)m_pCtx->m_bPE4;
			fCache = TRUE;
			break;

		case PEX:
			*pData         = m_pCtx->m_dPEX;
			m_pCtx->m_dPEX = 0xFFFFFFFF;
			return TRUE;

		case DST:
			*pData = 0;
			return TRUE;

		case USR:
			*pData = 0;
			return TRUE;

		case USRC:
			pCache = m_pCtx->m_dUSRC;
			fCache = TRUE;
			break;

		case USRR:
			pCache = m_pCtx->m_dUSRR;
			fCache = TRUE;
			break;

		case LRS:
		case LES:
			m_uLCtr++;
			return FALSE;
		}

	if( fCache ) {

		if( pCache[0] & 0xFF000000 ) { // if at least one character in cache

			GetCachedData(pData, pCache, uCount);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CLaetusSerialDriver::NoWriteTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD dData = *pData;

	if( Addr.a.m_Table == CCF ) {

		m_fIsCCF = TRUE;

		if( !Addr.a.m_Offset ) {

			BYTE b = LOBYTE(*pData);

			if( b >= 'a' && b <= 'z' ) {

				m_pCtx->m_bCodeType = b;

				return uCount == 1;
				}
			}

		return FALSE;
		}

	switch( Addr.a.m_Table ) {

		case LRS:
		case LES:
			return TRUE;

		case LRD:
			if( IsClearString(dData) ) {

				m_pCtx->m_bLRD[0] = 0;
				}

			return TRUE;

		case LED:
			if( IsClearString(dData) ) {

				m_pCtx->m_bLED[0] = 0;
				}

			return TRUE;

		case SRG:
			return dData < 1 || dData > 2;

		case SOM:
			return !SetMode(1);

		case PE1:
		case PE2:
		case PE3:
		case PE4:
			return TRUE;

		case PEX:
			return dData != 1;

		case USR:
		case DST:
			return !dData;

		case USRC:
			if( pData[0] & 0xFF000000 ) {

				StringToCache(pData, m_pCtx->m_dUSRC, uCount/4, MAXUSRSZ);
				}

			return TRUE;

		case USRR:
			memset(m_pCtx->m_dUSRR, 0, MAXUSRSZ * 4);
			return TRUE;
			
		}

	return FALSE;
	}

BOOL CLaetusSerialDriver::IsClearString(DWORD dData)
{
	dData &= 0x5F5F0000; // make upper

	BYTE b1 = LOBYTE(dData >> 24);
	BYTE b2 = LOBYTE(dData >> 16);

	return b1 == 'C' && b2 == 'L';
	}

void CLaetusSerialDriver::PutDecimal(DWORD dValue, UINT uSize)
{
	switch( uSize ) {

		case 8:	AddByte(m_pHex[(dValue / 10000000)     ]);
		case 7:	AddByte(m_pHex[(dValue /  1000000) % 10]);
		case 6:	AddByte(m_pHex[(dValue /   100000) % 10]);
		case 5:	AddByte(m_pHex[(dValue /    10000) % 10]);
		case 4:	AddByte(m_pHex[(dValue /     1000) % 10]);
		case 3:	AddByte(m_pHex[(dValue /      100) % 10]);
		case 2:	AddByte(m_pHex[(dValue /       10) % 10]);
		case 1:	AddByte(m_pHex[ dValue             % 10]);
		}
	}

BYTE  CLaetusSerialDriver::GetTxDelimiter(BOOL fStart)
{
	return m_pCtx->m_fSICK ? GetSick(fStart) : GetWell(TRUE, fStart);
	}

BYTE  CLaetusSerialDriver::GetRxDelimiter(BOOL fStart)
{
	return m_pCtx->m_fSICK ? GetSick(fStart) : GetWell(FALSE, fStart);
	}

BYTE  CLaetusSerialDriver::GetSick(BOOL fStart)
{
	return fStart ? STX : ETX;
	}

BYTE  CLaetusSerialDriver::GetWell(BOOL fTx, BOOL fStart)
{
	return fTx ? (fStart ? '<' : '>') : (fStart ? '[' : ']');
	}

WORD CLaetusSerialDriver::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) return bData - '0';

	if( bData >= 'A' && bData <= 'F' ) return bData - '7';

	if( bData >= 'a' && bData <= 'f' ) return bData - 'W';
		
	return 0;
	}

DWORD CLaetusSerialDriver::GetDecRespValue(UINT *pPos, UINT uSize)
{
	DWORD   d = 0;

	for( UINT i = 0; i < uSize; i++ ) {

		BYTE b = PickNextChar(pPos);

		if( b >= '0' && b <= '9' ) {

			d *= 10;

			d += b - '0';
			}

		else return d;
		}

	return d;
	}

DWORD CLaetusSerialDriver::GetChrRespValue(UINT *pPos, UINT uSize, BYTE bLo)
{
	DWORD   d = 0;

	for( UINT i = 0; i < uSize; i++ ) {

		BYTE b = PickNextChar(pPos);

		if( b >= bLo && b <= bLo + 25 ) {

			d <<= 8;

			d  += b;
			}

		else return d;
		}

	return d;
	}

DWORD CLaetusSerialDriver::GetHexRespValue(UINT *pPos, UINT uSize)
{
	DWORD   d = 0;

	for( UINT i = 0; i < uSize; i++ ) {

		BYTE b = PickNextChar(pPos);

		if( b != ']' && b > ' ' ) {

			d    <<= 4;

			d     += b & 0xF;
			}

		else return d;
		}

	return d;
	}

DWORD CLaetusSerialDriver::GetMixRespValue(UINT *pPos, UINT uSize)
{
	BYTE b = m_bRx[*pPos];

	if( b >= '0' && b <= '9' )	return GetDecRespValue(pPos, uSize);

	if( b >= 'A' && b <= 'Z' )	return GetChrRespValue(pPos, uSize, 'A');

	return GetChrRespValue(pPos, uSize, 'a');
	}

BYTE CLaetusSerialDriver::PickNextChar(UINT *pPos)
{
	SkipSpaceChars(pPos);

	return GetNextRespChar(pPos);
	}

BYTE CLaetusSerialDriver::GetNextRespChar(UINT *pPos)
{
	UINT uPos = *pPos;

	BYTE    b = m_bRx[uPos++];

	*pPos     = uPos;

	return b;
	}

DWORD CLaetusSerialDriver::GetNextRespValue(UINT *pPos, UINT uCount)
{
	DWORD d = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		BYTE b = PickNextChar(pPos);

		if( b < '0' || b > '9' ) {

			return d;
			}

		d *= 10;

		d += DWORD(FromHex(b));
		}

	return d;
	}

void CLaetusSerialDriver::SaveAckStatus(UINT uOffset)
{
	BYTE  b = m_bRx[1];

	UINT uT = m_pLItem->uID;

	if( uT == SOM ) {

		m_pCtx->m_bSOMStatus[uOffset] = b;
		}

	else {
		m_pCtx->m_bAckStatus[uT]      = b;
		}
	}

UINT CLaetusSerialDriver::ToggleL(BOOL fIsLRS)
{
	UINT uNo = 0xFFFFFFFF;

	if( m_uLCtr > 10 ) {

		if( fIsLRS ) {

			m_uLRS1 = m_uLRS1 == uNo ? m_uLRS2 : uNo;

			m_uLCtr = m_uLRS1 == uNo ? 8 : 0;
			}

		else {
			m_uLES1 = m_uLES1 == uNo ? m_uLES2 : uNo;

			m_uLCtr = m_uLES1 == uNo ? 8 : 0;
			}
		}

	return fIsLRS ? m_uLRS1 : m_uLES1;
	}

// End of File
