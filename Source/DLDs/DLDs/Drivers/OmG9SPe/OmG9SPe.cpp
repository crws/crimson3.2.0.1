
#include "intern.hpp"

#include "omg9spe.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omron G9SP-Series via Fins UDP Master Driver
//

// Instantiator

INSTANTIATE(COmronFinsG9spMasterUDPDriver);

// Constructor

COmronFinsG9spMasterUDPDriver::COmronFinsG9spMasterUDPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_pTxBuff = NULL;

	m_pRxBuff = NULL;

	m_pTxData = NULL;
	}

// Configuration

void MCALL COmronFinsG9spMasterUDPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL COmronFinsG9spMasterUDPDriver::Attach(IPortObject *pPort)
{
	m_pSock = CreateSocket(IP_UDP);

	}

void MCALL COmronFinsG9spMasterUDPDriver::Detach(void)
{
	if( m_pSock ) {

		m_pSock->Release();

		m_pSock = NULL;
		} 
	}


void MCALL COmronFinsG9spMasterUDPDriver::Open(void)
{
	if( m_pSock ) {

		m_pSock->SetOption(OPT_ADDRESS, 1);
		
		m_pSock->SetOption(OPT_RECV_QUEUE, 4);
		}
	}

// Device

CCODE MCALL COmronFinsG9spMasterUDPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP1    = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_uLast  = GetTickCount();
			m_pCtx->m_bDna   = GetByte(pData);
			m_pCtx->m_bDa1	 = GetByte(pData);
			m_pCtx->m_bDa2	 = GetByte(pData);
			m_pCtx->m_bSna	 = GetByte(pData);
			m_pCtx->m_bSa1   = GetByte(pData);
			m_pCtx->m_bSa2	 = GetByte(pData);
			m_pCtx->m_bSid   = 0;
			m_pCtx->m_bMode	 = GetByte(pData);

			m_pCtx->m_fAux	 = FALSE;

			m_pCtx->m_IP2    = GetAddr(pData);
			m_pCtx->m_bDna2  = GetByte(pData);
			m_pCtx->m_bDa12	 = GetByte(pData);
			m_pCtx->m_bDa22	 = GetByte(pData);

			m_pCtx->m_fDirty = FALSE;

			m_pCtx->m_uPoll  = ToTicks(GetWord(pData));
			m_pCtx->m_uData  = 0;
			m_pCtx->m_uTime  = GetTickCount();

			m_pCtx->m_fConn  = FALSE;

			memset(m_pCtx->m_bBuff, 0, sizeof(m_pCtx->m_bBuff));

			if( m_pSock ) {

				m_pSock->Listen(m_pCtx->m_uPort);
				}

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL COmronFinsG9spMasterUDPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		if( m_pSock ) {

			m_pSock->Release();

			m_pSock = NULL;
			}

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);

	}

// User Access

UINT MCALL COmronFinsG9spMasterUDPDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx  = (CContext *) pContext;
       
	if( uFunc == 1 || uFunc == 5 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		if( uFunc == 1  ) {

			pCtx->m_IP1 = MotorToHost(dwValue);
			}

		if( uFunc == 5 ) {

			pCtx->m_IP2 = MotorToHost(dwValue);
			}

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort  = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 4 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP1);
		}
	
	if( uFunc == 6 ) {
		
		// Get Current Fallback IP Address

		return MotorToHost(pCtx->m_IP2);
		}

	CBaseCtx * pBase = pCtx;

	if( uFunc == 7 || uFunc == 13 ) {

		//  Set Destination Network

		UINT uValue = ATOI(Value);

		if( uValue >= 0 && uValue <= 127 ) {

			pCtx->m_fDirty = TRUE;

			if( uFunc == 7 ) {

				pBase->m_bDna = uValue;
				}

			if( uFunc == 13 ) {

				pCtx->m_bDna2 = uValue;
				}
			}

		return 0;
		}

	if( uFunc == 9 || uFunc == 15 ) {

		//  Set Destination Node

		UINT uValue = ATOI(Value);

		if( uValue >= 0 && uValue <= 255 ) {

			pCtx->m_fDirty = TRUE;

			if( uFunc == 9 ) {

				pBase->m_bDa1 = uValue;
				}

			if( uFunc == 15 ) {

				pCtx->m_bDa12 = uValue;
				}
			}

		return 0;
		}

	if( uFunc == 11 || uFunc == 17 ) {

		//  Set Destination Unit

		UINT uValue = ATOI(Value);

		if( uValue >= 0 && uValue <= 254 ) {

			pCtx->m_fDirty = TRUE;

			if( uFunc == 11 ) {

				pBase->m_bDa2 = uValue;
				}

			if( uFunc == 17 ) {

				pCtx->m_bDa22 = uValue;
				}
			}

		return 0;
		}

	if( uFunc == 8 ) {

		// Get Destination Network

		return pBase->m_bDna;
		}

	if( uFunc == 10 ) {

		// Get Destination Node

		return pBase->m_bDa1;
		}

	if( uFunc == 12 ) {

		// Get Destination Unit

		return pBase->m_bDa2;
		}

	if( uFunc == 14 ) {

		// Get Fallback Destination Network

		return pCtx->m_bDna2;
		}

	if( uFunc == 16 ) {

		// Get Fallback Destination Node

		return pCtx->m_bDa12;
		}

	if( uFunc == 18 ) {

		// Get Fallback Destination Unit

		return pCtx->m_bDa22;
		} 
	
	return 0;
	}


// Entry Points

CCODE MCALL COmronFinsG9spMasterUDPDriver::Ping(void)
{	
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 2;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrBitAsBit;
	Addr.a.m_Extra  = 0;

	if( Start() ) {

		PutFinsHeader();

		AddData();

		if( Transact() ) {

			m_pCtx->m_fConn = TRUE;
			
			return CCODE_SUCCESS;
			}
		}

	m_pCtx->m_fConn = FALSE;

	return CCODE_ERROR;
	}

CCODE MCALL COmronFinsG9spMasterUDPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
      	if( IsWriteOnly(Addr.a.m_Table, Addr.a.m_Offset) ) {

		return uCount;
		}

	if( !IsTimedOut() ) {

		return GetData(Addr, pData, uCount);
		} 

	if( Start() ) {

		PutFinsHeader();

		AddData();

		if( Transact() ) {

			return GetData(Addr, pData, uCount);
			}
		}
	
	return CCODE_ERROR;
	}

CCODE MCALL COmronFinsG9spMasterUDPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsWriteOnly(Addr.a.m_Table, Addr.a.m_Offset) ) {

		for( UINT u = 0; u < uCount; u++ ) {

			UINT uMask = (0x1 << (Addr.a.m_Offset + u));

			if( pData[u] & 0x1 ) {

				m_pCtx->m_uData |= uMask;
				}
			else{
				m_pCtx->m_uData &= ~uMask;
				}
			}

		if( COMMS_SUCCESS(Ping()) ) {
		
			return uCount;
			}
		}
	
	return uCount;
	}

void MCALL COmronFinsG9spMasterUDPDriver::Service(void)
{
	if( m_pCtx->m_fConn ) {
	
		if( IsTimedOut() ) {

			Ping();
			}
		}
	}


// Transport Layer

BOOL COmronFinsG9spMasterUDPDriver::Transact(void)
{
	if( Send() ) {

		if( RecvFrame() ) {

			if( CheckFrame() ) {

				Critical(TRUE);

				memcpy(m_pCtx->m_bBuff, m_bRxBuff + 18, 188);

				Critical(FALSE);

				m_pCtx->m_uTime = GetTickCount();

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL COmronFinsG9spMasterUDPDriver::Send(void)
{
	if( TRUE ) {

		AddTransportHeader();

		for( UINT n = 0; n < 5; n++ ) {

			if( m_pSock->Send(m_pTxBuff) == S_OK ) {

				m_pTxBuff = NULL;

				return TRUE;
				}

			Sleep(5);
			}
		}

	m_pTxBuff->Release();

	m_pTxBuff = NULL;

	return FALSE;

	}

BOOL COmronFinsG9spMasterUDPDriver::RecvFrame(void)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {
		
		m_pSock->Recv(m_pRxBuff);

		if( m_pRxBuff ) {

			if( StripTransportHeader() ) {

				PBYTE pData = m_pRxBuff->GetData();

				for( UINT u = 0; u < 206; u++ ) {

					m_bRxBuff[u] = pData[u];
					}

				m_pRxBuff->Release();

				m_pRxBuff = NULL;
				
				return TRUE;
				}
					
			m_pRxBuff->Release();

			m_pRxBuff = NULL;
			}

		Sleep(5);
		}

	if( m_pCtx->m_IP2 ) {

		m_pCtx->m_fAux = !m_pCtx->m_fAux;

		m_pCtx->m_fConn = FALSE;
		}
	 
	return FALSE;
	}

BOOL COmronFinsG9spMasterUDPDriver::CheckFrame(void)
{
	BYTE bBytes [] = {	0xC0, 
				0x00, 
				0x02, 
				0x00, 
				m_pCtx->m_bSa1, 
				0x00, 
				0x00, 
				m_pCtx->m_fAux ? m_pCtx->m_bDa12 : m_pCtx->m_bDa1, 
				0x00, 
				0x00, 
				0x28, 
				0x01, 
				0x00, 
				0x00, 
				0x00, 
				0xBE,
				0x00, 
				0xCB };

	for( UINT u = 0; u < elements(bBytes); u++ ) {

		if( m_bRxBuff[u] != bBytes[u] ) {

			return FALSE;
			}
		}
	
	return TRUE;
	}

// Implementation

BOOL COmronFinsG9spMasterUDPDriver::Start(void)
{
	if( (m_pTxBuff = CreateBuffer(600, TRUE)) ) {

		m_pTxBuff->AddTail(600);

		m_pTxData = m_pTxBuff->GetData();

		m_uPtr  = 0;

		return TRUE;
		}
	
	Sleep(10);

	return FALSE;
	}


void COmronFinsG9spMasterUDPDriver::AddData(void)
{
	WORD wHi = HIWORD(m_pCtx->m_uData);

	WORD wLo = LOWORD(m_pCtx->m_uData);

	AddByte(LOBYTE(wLo));

	AddByte(HIBYTE(wLo));

	AddByte(LOBYTE(wHi));

	AddByte(HIBYTE(wHi));
	
	AddByte(0);			// Echo

	AddByte(0);			// Reserved

	m_pTxBuff->StripTail(m_pTxBuff->GetSize() - m_uPtr);
	}

UINT COmronFinsG9spMasterUDPDriver::GetData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT At = Addr.a.m_Table;

	UINT Count = uCount;

	UINT Index = Addr.a.m_Offset;

	if( Addr.a.m_Table == 24 || Addr.a.m_Table == 48 ) {

		GetNibblesAt(pData, At, Index, Count);

		return Count;
		}
	
	if( Addr.a.m_Table == addrNamed ) {

		At = Addr.a.m_Offset;

		Count = 1;

		Index = 0;
		}

	if( Addr.a.m_Table <= 2 ) {

		At = 0;
		}

	switch( Addr.a.m_Type ) {

		case addrLongAsLong:	GetLongsAt(pData, At, Index, Count);	break;
		case addrWordAsWord:	GetWordsAt(pData, At, Index, Count);	break;
		case addrByteAsByte:	GetBytesAt(pData, At, Index, Count);	break;
		case addrBitAsBit:	GetBitsAt (pData, At, Index, Count);	break;
		}

	return Count;
       	}

void COmronFinsG9spMasterUDPDriver::GetLongsAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount)
{	
	for( UINT u = 0, i = 0; u < uCount + uIndex; u++ ) {

		if( u >= uIndex ) {

			DWORD x  = PU4(m_pCtx->m_bBuff + uAt)[u];
			
			pData[i] = IntelToHost(x);

			i++;
			}
		}
	}

void COmronFinsG9spMasterUDPDriver::GetWordsAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount)
{
	for( UINT u = 0, i = 0; u < uCount + uIndex; u++ ) {

		if( u >= uIndex ) {
	
			WORD x  = PU2(m_pCtx->m_bBuff + uAt)[u];
			
			pData[i] = LONG(SHORT(IntelToHost(x)));

			i++;
			}
		}
	}

void COmronFinsG9spMasterUDPDriver::GetBytesAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount)
{
	for( UINT u = 0, i = 0; u < uCount + uIndex; u++ ) {

		if( u >= uIndex ) {
				
			pData[i] = PBYTE(m_pCtx->m_bBuff + uAt)[u];

			i++;
			}
		}
	}

void COmronFinsG9spMasterUDPDriver::GetNibblesAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount)
{
	BYTE bNibble = 0;
	
	for( UINT u = 0, i = 0, b = 0; u < uCount + uIndex; u++ ) {

		if( u % 2 == 0 ) {

			bNibble = PBYTE(m_pCtx->m_bBuff + uAt)[b];

			b++;
			}

		if( u >= uIndex ) {
				
			pData[i] = ((bNibble >> ((u % 2) * 4)) & 0xF);

			i++;
			}
		}
	}

void COmronFinsG9spMasterUDPDriver::GetBitsAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount)
{
	BYTE bBits = 0;
	
	for( UINT u = 0, i = 0, b = 0; u < uCount + uIndex; u++ ) {

		if( u % 8 == 0 ) {

			bBits = PBYTE(m_pCtx->m_bBuff + uAt)[b];

			b++;
			}

		if( u >= uIndex ) {
	
			pData[i] = (bBits >> (u % 8)) & 0x1;

			i++;
			}
		}
	}

void COmronFinsG9spMasterUDPDriver::PutFinsHeader(void)
{
	AddByte(FINS_ICF);

	AddByte(FINS_RSV);

	AddByte(FINS_GCT);

	AddByte(0);

	AddByte(m_pCtx->m_fAux ? m_pCtx->m_bDa12 : m_pCtx->m_bDa1);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bSa1);

	AddByte(0);

	AddByte(0);

	AddByte(0x28);

	AddByte(0x01);

	AddByte(0x00);

	AddByte(0x4B);

	AddByte(0x03);

	AddByte(0x4D);

	AddByte(0x00);

	AddByte(0x01);
	}

void COmronFinsG9spMasterUDPDriver::AddByte(BYTE bByte)
{
	m_pTxData[m_uPtr++] = bByte;	
	}


BOOL COmronFinsG9spMasterUDPDriver::IsTimedOut(void)
{
	return int(GetTickCount() - m_pCtx->m_uTime - m_pCtx->m_uPoll) >= 0;
	}

BOOL COmronFinsG9spMasterUDPDriver::IsWriteOnly(UINT uTable, UINT uOffset)
{
	if( uTable == 1 ) {

		return TRUE;
		}

	return FALSE;
	}

// Helpers

BOOL COmronFinsG9spMasterUDPDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL COmronFinsG9spMasterUDPDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL COmronFinsG9spMasterUDPDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// Transport Header

void COmronFinsG9spMasterUDPDriver::AddTransportHeader(void)
{	
	DWORD IP;

	WORD   Port;

	if( !m_pCtx->m_fAux ) {
				
		IP   = m_pCtx->m_IP1;

		Port = WORD(m_pCtx->m_uPort);
		}
	else {
	
		IP   = m_pCtx->m_IP2;
		
		Port = WORD(m_pCtx->m_uPort);
		}
	
	UINT  uData = m_pTxBuff->GetSize();

	UINT  uSize = 6;

	PBYTE pData = m_pTxBuff->AddHead(uSize);

	pData[0] = PBYTE(&IP)[0];
	pData[1] = PBYTE(&IP)[1];
	pData[2] = PBYTE(&IP)[2];
	pData[3] = PBYTE(&IP)[3];

	pData[4] = HIBYTE(Port);
	pData[5] = LOBYTE(Port);
	}

BOOL COmronFinsG9spMasterUDPDriver::StripTransportHeader(void)
{
	PBYTE pData = m_pRxBuff->GetData();

	((PBYTE) &m_RxMac.m_IP)[0] = pData[0];
	((PBYTE) &m_RxMac.m_IP)[1] = pData[1];
	((PBYTE) &m_RxMac.m_IP)[2] = pData[2];
	((PBYTE) &m_RxMac.m_IP)[3] = pData[3];

	m_RxMac.m_Port = MAKEWORD(pData[5], pData[4]);

	m_pRxBuff->StripHead(6);

	return TRUE;
	}

// End of File
