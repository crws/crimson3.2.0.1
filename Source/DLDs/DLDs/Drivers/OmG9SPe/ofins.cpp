
#include "intern.hpp"

#include "ofins.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omron Fins Base Master Driver
//

// Constructor

COmronFinsMasterDriver::COmronFinsMasterDriver(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	m_pBase = NULL;

	}

// Destructor

COmronFinsMasterDriver::~COmronFinsMasterDriver(void)
{
	}

// Entry Points

CCODE MCALL COmronFinsMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 'D';
	Addr.a.m_Offset = 0x0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL COmronFinsMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
       	if( IsBroadcast() ) {

		return uCount;
		}

	if( Start() ) {

		UINT uType = Addr.a.m_Type;

		MakeMin(uCount, UINT(uType == addrWordAsWord ? 32 : 16)); 
	
		PutRead(Addr, uCount);

		if( Transact(FINS_MRC_PARAM, FINS_SRC_READ, uCount, uType) ) {

			GetFinsData(pData, uCount, uType);

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL COmronFinsMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Start() ) {

		MakeMin(uCount, UINT(Addr.a.m_Type == addrWordAsWord ? 32 : 16));
		
		PutWrite(Addr, pData, uCount);

		if( Transact(FINS_MRC_PARAM, FINS_SRC_WRITE, 0, 0) ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}
 
// Implementation

void COmronFinsMasterDriver::PutRead(AREF Addr, UINT uCount)
{
	PutFinsHeader();
	
	PutFinsCommand(Addr, uCount, FINS_MRC_PARAM, FINS_SRC_READ);
	
	}

void COmronFinsMasterDriver::PutWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	PutFinsHeader();

	PutFinsCommand(Addr, uCount, FINS_MRC_PARAM, FINS_SRC_WRITE);

	PutFinsData(pData, uCount, Addr.a.m_Type);

	}

void COmronFinsMasterDriver::PutFinsHeader(void)
{
	AddByte(FINS_ICF | ( IsBroadcast() ? 1 : 0 ));

	AddByte(FINS_RSV);

	AddByte(FINS_GCT);

	AddByte(m_pBase->m_bDna);

	AddByte(m_pBase->m_bDa1);

	AddByte(m_pBase->m_bDa2);

	AddByte(m_pBase->m_bSna);

	AddByte(m_pBase->m_bSa1);

	AddByte(m_pBase->m_bSa2);

	AddByte(m_pBase->m_bSid);

	m_pBase->m_bSid = m_pBase->m_bSid == 0xFF ? 0 : m_pBase->m_bSid + 1;
       
	}

void COmronFinsMasterDriver::PutFinsCommand(AREF Addr, UINT uCount, BYTE bMrc, BYTE bSrc)
{
	PutFinsCommand(bMrc, bSrc);

	AddByte(GetVariableType(Addr.a.m_Table));

	AddAddress(GetVariableOffset(Addr.a.m_Table, Addr.a.m_Offset));

	AddWord(Addr.a.m_Type == addrWordAsWord ? uCount : uCount * 2);
	}

void COmronFinsMasterDriver::PutFinsCommand(BYTE bMrc, BYTE bSrc)
{
	AddByte(bMrc);

	AddByte(bSrc);
	}

void COmronFinsMasterDriver::PutFinsData(PDWORD pData, UINT uCount, UINT uType)
{
	switch(uType) {

		case addrWordAsWord:

			PutWordWrite(pData, uCount);

			return;

		case addrWordAsLong:
		case addrWordAsReal:

			PutLongWrite(pData, uCount);

			return;
		}
	}

void COmronFinsMasterDriver::PutWordWrite(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddWord(LOWORD(pData[u]));
		}
	}

void COmronFinsMasterDriver::PutLongWrite(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x = pData[u];

		SwapWords(x);
		
		AddLong(x);
		}
	}

void COmronFinsMasterDriver::GetFinsData(PDWORD pData, UINT uCount, UINT uType)
{
	switch(uType) {

		case addrWordAsWord:

			GetWordRead(pData, uCount);

			return;

		case addrWordAsLong:
		case addrWordAsReal:

			GetLongRead(pData, uCount);

			return;
		}
	}

void COmronFinsMasterDriver::GetWordRead(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		WORD x = PU2(m_bRxBuff + 4)[u];

		pData[u] = LONG(SHORT(MotorToHost(x)));

		}
	}

void COmronFinsMasterDriver::GetLongRead(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x = PU4(m_bRxBuff + 4)[u];

		SwapWords(x);

		pData[u] = MotorToHost(x);

		}
	}

BYTE COmronFinsMasterDriver::GetVariableType(UINT uTable)
{

	switch(uTable) {

		case 'C':
			return !m_pBase->m_bMode ? 0xB0 : 0x80;
		case 'W':
			return 0xB1;
		case 'H': 
			return 0xB2;
		case 'A':
			return !m_pBase->m_bMode ? 0xB3 : 0x80;
		case 'T':
			return !m_pBase->m_bMode ? 0x89 : 0x81;
		case 'N':
			return !m_pBase->m_bMode ? 0x89 : 0x81;
		case 'D':
			return 0x82;
		case 'E':
			return 0x98;
		case 'R':
		case addrNamed:
			return !m_pBase->m_bMode ? 0xBC : 0x9C;
			
		default:
			return 0x00;
		}
	}

DWORD COmronFinsMasterDriver::GetVariableOffset(UINT uTable, UINT uOffset)
{
    	switch(uTable) {

		case 'N':
			return DWORD(uOffset + 0x8000);
		case 'R':
			return DWORD(uOffset + 0x200);
		case addrNamed:
			return DWORD(!m_pBase->m_bMode ? 0x0F0000 : 0x000600);
		}

	return DWORD(uOffset);

      	}

BOOL COmronFinsMasterDriver::IsBroadcast(void)
{
	return m_pBase->m_bDa1 == FINS_BROADCAST;
	}

void COmronFinsMasterDriver::SwapWords(DWORD &x)
{
	WORD h = HIWORD(x);

	WORD l = LOWORD(x);

	x = MAKELONG(h, l);
	
	}

// Frame Building

BOOL COmronFinsMasterDriver::Start(void)
{
	m_uPtr = 0;

	return TRUE;

	}

void COmronFinsMasterDriver::AddByte(BYTE bByte)
{
	m_bTxBuff[m_uPtr] = bByte;

	m_uPtr++;
	
	}

void COmronFinsMasterDriver::AddWord(WORD wWord)
{
	AddByte(HIBYTE(wWord));

	AddByte(LOBYTE(wWord));

	}

void COmronFinsMasterDriver::AddLong(DWORD dwWord)
{
	AddWord(HIWORD(dwWord));

	AddWord(LOWORD(dwWord));
	
	}

void COmronFinsMasterDriver::AddAddress(DWORD dwAddr)
{
	AddWord(LOWORD(dwAddr));

	AddByte(LOBYTE(HIWORD(dwAddr)));
	
	}

// Transport Layer

BOOL COmronFinsMasterDriver::Transact(BYTE bMRes, BYTE bSRes, UINT uCount, UINT uType)
{
	return FALSE;
	}

// End of File
