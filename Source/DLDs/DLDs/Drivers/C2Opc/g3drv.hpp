
//////////////////////////////////////////////////////////////////////////
//
// G3 OPC Server
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3DRV_HPP
	
#define	INCLUDE_G3DRV_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Definitions
//

class CG3OPCDriver;

//////////////////////////////////////////////////////////////////////////
//
// G3 OPC Protocol Commands
//

enum OPCCmd
{
	opcAck		  = 0x01,
	opcNak		  = 0x02,
	opcReply	  = 0x03,
	opcPing		  = 0x04,
	
	opcTagIndexWrLong = 0x10,
	opcTagIndexRdLong = 0x11,
	opcTagBlockWrLong = 0x12,
	opcTagBlockRdLong = 0x13,
	opcTagBlockWrBit  = 0x14,
	opcTagBlockRdBit  = 0x15,
	opcTagBlockWrReal = 0x16,
	opcTagBlockRdReal = 0x17,
	opcTagStringWr    = 0x18,
	opcTagStringRd    = 0x19,
	
	};

//////////////////////////////////////////////////////////////////////////
//
// G3 OPC Comms Driver
//

class CG3OPCDriver : public CMasterDriver
{
	public:
		// Constructor
		CG3OPCDriver(void);

		// Destructor
		~CG3OPCDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject  *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersistConnection);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

	protected:
		// Device Data
		struct CContext
		{
			DWORD	  m_IP;
			WORD	  m_wPort;
			BOOL	  m_fKeep;
			BOOL	  m_fPing;
			UINT	  m_uTime1;
			UINT	  m_uTime2;
			UINT	  m_uTime3;
			BOOL      m_fWide;
			ISocket * m_pSock;
			BYTE      m_bSeq;
			UINT	  m_uLast;
			BOOL      m_fDirty;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTxBuff[512];
		BYTE	   m_bRxBuff[512];
		UINT	   m_uPtr;
		UINT	   m_uKeep;

		// Read Handlers
		CCODE DoLongRead   (AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoRealRead   (AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead    (AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoStringRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoStringReadW(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoLongWrite   (AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoRealWrite   (AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite    (AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoStringWrite (AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoStringWriteW(AREF Addr, PDWORD pData, UINT uCount);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddSkip(UINT uSize);

		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL Transact(void);

		//Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
	};

// End of File

#endif
