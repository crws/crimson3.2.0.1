
#include "intern.hpp"

#include "emsonepp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson EPP Driver
//

// Instantiator

INSTANTIATE(CEmersonEPPDriver);

// Constructor

CEmersonEPPDriver::CEmersonEPPDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uMaxWords = 120;
	
	m_uMaxBits  = 2000;

	m_pTx       = NULL;

	m_pRx       = NULL;

	m_uTimeout  = 600;
	}

// Destructor

CEmersonEPPDriver::~CEmersonEPPDriver(void)
{
	FreeBuffers();
	}

// Configuration

void MCALL CEmersonEPPDriver::Load(LPCBYTE pData)
{
	AllocBuffers();
	}
	
void MCALL CEmersonEPPDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}
	
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CEmersonEPPDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEmersonEPPDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmersonEPPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_fDisableCheck = TRUE;
			m_pCtx->m_fIgnoreReadEx = TRUE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	Sleep(150);

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmersonEPPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmersonEPPDriver::Ping(void)
{
	if( !m_pCtx->m_bDrop ) {

		return CCODE_SUCCESS;
		}

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPACE_HOLD;
	Addr.a.m_Offset = 1;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return DoWordRead(Addr, Data, 1);
	}

CCODE MCALL CEmersonEPPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_bDrop ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	CAddress CAdd;

	if( !ConvertAddress(Addr, &CAdd) ) return CCODE_ERROR | CCODE_NO_RETRY;

	switch( CAdd.a.m_Table ) {

		case SPACE_HOLD:

			if( CAdd.a.m_Type == addrWordAsWord ) {

				return DoWordRead(CAdd, pData, 1);
				}

			return DoLongRead(CAdd, pData, uCount);

		case SPACE_ANALOG:

			if( CAdd.a.m_Type == addrWordAsWord ) {

				return DoWordRead(CAdd, pData, 1);
				}

			return DoLongRead(CAdd, pData, 1);

		case SPACE_HOLD32:
			
			return DoLongRead(CAdd, pData, 1);

		case SPACE_ANALOG32:
			
			return DoLongRead(CAdd, pData, 1);

		case SPACE_OUTPUT:
			
			return DoBitRead (CAdd, pData, 1);

		case SPACE_INPUT:
			
			return DoBitRead (CAdd, pData, 1);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CEmersonEPPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress CAdd;

//**/	AfxTrace3("\r\n** WRITE** Tab=%d Off=%d Ct=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( !ConvertAddress(Addr, &CAdd) ) return uCount;

	switch( CAdd.a.m_Table ) {

		case SPACE_HOLD:

			if( CAdd.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(CAdd, pData, 1);
				}

			return DoLongWrite(CAdd, pData, uCount);

		case SPACE_HOLD32:
			
			return DoLongWrite(CAdd, pData, 1);

		case SPACE_OUTPUT:
			
			return DoBitWrite (CAdd, pData, 1);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

void CEmersonEPPDriver::AllocBuffers(void)
{
	UINT c1 = 2 * m_uMaxWords;
	
	UINT c2 = (m_uMaxBits + 7) / 8;
	
	UINT c3 = max(c1, c2) + 20;

	m_uTxSize = c3;
	
	m_uRxSize = c3;
	
	m_pTx = new BYTE [ m_uTxSize ];

	m_pRx = new BYTE [ m_uRxSize ];
	}

void CEmersonEPPDriver::FreeBuffers(void)
{
	if( m_pTx ) {

		delete [] m_pTx;

		m_pTx = NULL;
		}

	if( m_pRx ) {

		delete [] m_pRx;

		m_pRx = NULL;
		}
	}
	
BOOL CEmersonEPPDriver::IsHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return TRUE;
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return TRUE;
		}
		
	return FALSE;
	}

WORD CEmersonEPPDriver::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return bData - '0';
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return bData - 'A' + 10;
		}
		
	return 0;
	}

BOOL CEmersonEPPDriver::IgnoreException(void)
{
	return (m_pCtx->m_fIgnoreReadEx && (m_pRx[1] & 0x80));
	}

BOOL CEmersonEPPDriver::ConvertAddress(AREF Addr, CAddress *pAddr)
{
	pAddr->a.m_Extra  = 0;
	pAddr->a.m_Type   = Addr.a.m_Type;

	UINT u = Addr.a.m_Offset;

	switch( u / 10000 ) {

		case 0:	pAddr->a.m_Table = SPACE_OUTPUT;	break;
		case 1: pAddr->a.m_Table = SPACE_INPUT;		break;
		case 3: pAddr->a.m_Table = SPACE_ANALOG;	break;
		case 4: pAddr->a.m_Table = SPACE_HOLD;		break;

		default: return FALSE;
		}

	pAddr->a.m_Offset = u % 10000;

	return TRUE;
	}

// Port Access

void CEmersonEPPDriver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CEmersonEPPDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CEmersonEPPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr   = 0;

	m_pRx[1] = 0;
	
	AddByte(m_pCtx->m_bDrop);
	
	AddByte(bOpcode);
	}

void CEmersonEPPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {
	
		m_pTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CEmersonEPPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CEmersonEPPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CEmersonEPPDriver::PutFrame(void)
{
	m_pData->ClearRx();
	
	return BinaryTx();
	}

BOOL CEmersonEPPDriver::GetFrame(BOOL fWrite)
{
	return BinaryRx(fWrite);
	}

BOOL CEmersonEPPDriver::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_pTx[k]);

	m_pData->Write(m_pTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CEmersonEPPDriver::BinaryRx(BOOL fWrite)
{
	UINT uPtr = 0;

	UINT uGap = 0;

	SetTimer(m_uTimeout);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		UINT uByte;
		
		if( (uByte = RxByte(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

//**/			AfxTrace1("<%2.2x>", uByte);

			if( uPtr <= m_uRxSize ) {
			
				uGap = 0;
				
				continue;
				}
				
			return FALSE;
			}
		
		if( ++uGap >= 5 ) {
			
			if( uPtr >= 4 ) {

				if( m_pCtx->m_fDisableCheck ) {
					
					if( uPtr >= ( fWrite ? 8 : UINT(m_pRx[2] + 5) ) ) {

						return TRUE;
						}
					}
				else {
				
					m_CRC.Preset();
				
					PBYTE p = m_pRx;
				
					UINT  n = uPtr - 2;
			
					for( UINT i = 0; i < n; i++ ) {

						m_CRC.Add(*(p++));
						}

					WORD c1 = IntelToHost(PWORD(p)[0]);
				
					WORD c2 = m_CRC.GetValue();
					
					if( c1 == c2 ) {
						 
						return TRUE;
						}
					}
				}
				
			uPtr = 0;
			
			uGap = 0;
			}
		}

	return FALSE;
	}

BOOL CEmersonEPPDriver::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {
	
		if( !m_pCtx->m_bDrop ) { 

			while( m_pData->Read(0) < NOTHING );

			return TRUE;
			}
			
		if( GetFrame(fIgnore) ) {

			return CheckReply(fIgnore);
			}
		}

	return FALSE;
	}

BOOL CEmersonEPPDriver::CheckReply(BOOL fIgnore)
{
	if( m_pRx[0] == m_pTx[0] ) {

		if( fIgnore ) {

			return TRUE;
			}
	
		if( !(m_pRx[1] & 0x80) ) {
		
			return TRUE;
			}
		}
		
	return FALSE;
	}

// Read Handlers

CCODE CEmersonEPPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(uCount);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x = PWORD(m_pRx + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	if( (m_pRx[1] & 0x80) && IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEmersonEPPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLD:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(Addr.a.m_Table == SPACE_HOLD32 ? uCount : uCount * 2);
	
	if( Transact(FALSE) ) {

		if( uCount * 4 > m_pRx[2] ) {

			uCount = (m_pRx[2] + 2) / 4;
			}
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x = PDWORD(m_pRx + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		return uCount;
		}
		
	if( (m_pRx[1] & 0x80) && IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEmersonEPPDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
			
		case SPACE_OUTPUT:
			StartFrame(0x01);
			break;
			
		case SPACE_INPUT:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(Addr.a.m_Offset - 1);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_pRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	if( (m_pRx[1] & 0x80) && IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CEmersonEPPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_HOLD ) {

		if( uCount == 1 ) {
			
			StartFrame(6);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(uCount);
			
			AddByte(uCount * 2);
			
			for( UINT n = 0; n < uCount; n++ ) {
				
				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CEmersonEPPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable = Addr.a.m_Table;
		
	if( ( uTable == SPACE_HOLD32 ) || ( uTable == SPACE_HOLD ) ) {

		StartFrame(16);
		
		AddWord(Addr.a.m_Offset - 1);
		
		AddWord(uTable == SPACE_HOLD32 ? uCount : uCount * 2);
		
		AddByte(uCount * 4);
		
		for( UINT n = 0; n < uCount; n++ ) {
			
			AddLong(pData[n]);
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CEmersonEPPDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_OUTPUT ) {

		if( uCount == 1 ) {
			
			StartFrame(5);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			StartFrame(15);

			AddWord(Addr.a.m_Offset - 1);

			AddWord(uCount);

			AddByte((uCount + 7) / 8);

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b);
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// End of File
