
//////////////////////////////////////////////////////////////////////////
//
// Modbus Data Spaces
//

#define	SPACE_HOLD	0x01
#define	SPACE_ANALOG	0x02
#define	SPACE_OUTPUT	0x03
#define	SPACE_INPUT	0x04
#define	SPACE_HOLD32	0x05
#define	SPACE_ANALOG32	0x06

//////////////////////////////////////////////////////////////////////////
//
// Emerson EPP Driver
//

class CEmersonEPPDriver : public CMasterDriver
{
	public:
		// Constructor
		CEmersonEPPDriver(void);

		// Destructor
		~CEmersonEPPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			BOOL m_fDisableCheck;
			BOOL m_fIgnoreReadEx;
			};

		// Data Members
		UINT	   m_uMaxWords;
		UINT	   m_uMaxBits;
		CContext * m_pCtx;
		LPCTXT	   m_pHex;
		UINT	   m_uTxSize;
		UINT	   m_uRxSize;
		BYTE     * m_pTx;
		BYTE     * m_pRx;
		UINT	   m_uPtr;
		CRC16	   m_CRC;
		UINT	   m_uTimeout;
				
		// Implementation
		void AllocBuffers(void);
		void FreeBuffers(void);
		BOOL IsHex(BYTE bData);
		WORD FromHex(BYTE bData);
		BOOL IgnoreException(void);
		BOOL ConvertAddress(AREF Addr, CAddress * pAddr);

		// Port Access
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		
		// Transport Layer
		BOOL PutFrame(void);
		BOOL GetFrame(BOOL fWrite);
		BOOL BinaryTx(void);
		BOOL BinaryRx(BOOL fWrite);
		BOOL Transact(BOOL fIgnore);
		BOOL CheckReply(BOOL fIgnore);

		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
	};

// End of File
