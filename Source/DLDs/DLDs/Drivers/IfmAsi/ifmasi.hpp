//////////////////////////////////////////////////////////////////////////
//
// IFM ASI CodeDeSys SP Master Serial Driver
//

class CIfmAsiDriver : public CMasterDriver
{
	public:
		// Constructor
		CIfmAsiDriver(void);

		// Destructor
		~CIfmAsiDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Data Members
		UINT		m_uPtr;
		BYTE		m_bCheck;
		BYTE		m_bRxBuff[128];
		BYTE		m_bTxBuff[128];

		// Implementation
		BOOL RtsCheckTargetId(void);
		BOOL RtsLogin(BOOL fLogin);
		BOOL GetData(UINT uCount, PDWORD pData, UINT uType);
		BOOL GetByteData(UINT uCount, PDWORD pData);
		BOOL GetWordData(UINT uCount, PDWORD pData);
		BOOL GetLongData(UINT uCount, PDWORD pData);
		BOOL PutData(PDWORD pData, UINT uCount, UINT uType);
		BOOL PutByteData(UINT uCount, PDWORD pData);
		BOOL PutWordData(UINT uCount, PDWORD pData);
		BOOL PutLongData(UINT uCount, PDWORD pData);
		BOOL CheckFrame(void);

		// Transport
		BOOL Transact(void);
		void SendFrame(void);
		BOOL RecvAck(void);
		void SendAck(void);
		BOOL RecvFrame(void);

		// Frame Building
		BOOL Start(void);
		void AddByte(BYTE bByte);
		void AddWord(WORD wWord);
		void AddDouble(DWORD dwWord);
		void SetDataSize(void);
		void SetCheckSum(void);

		//Helpers
		UINT GetType(UINT uType);
	};

// End of File
