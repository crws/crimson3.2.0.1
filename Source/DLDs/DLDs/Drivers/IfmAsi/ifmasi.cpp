#include "intern.hpp"

#include "ifmasi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// IFM ASI CoDeSys SP Master Serial Driver
//

// Instantiator

INSTANTIATE(CIfmAsiDriver);

// Constructor

CIfmAsiDriver::CIfmAsiDriver(void)
{
	m_Ident = DRIVER_ID;

	}

// Destructor

CIfmAsiDriver::~CIfmAsiDriver(void)
{
	}

// Configuration

void MCALL CIfmAsiDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CIfmAsiDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CIfmAsiDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CIfmAsiDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CIfmAsiDriver::DeviceOpen(IDevice *pDevice)
{
	return CMasterDriver::DeviceOpen(pDevice);
	}

CCODE MCALL CIfmAsiDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CIfmAsiDriver::Ping(void)
{
	if ( RtsCheckTargetId() ) {

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CIfmAsiDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);
	
	Start();

	AddByte(46);

	AddByte(0);	

	AddDouble(0x01);
	
	AddWord(Addr.a.m_Extra % 3);
	
	UINT uType = Addr.a.m_Type;

	UINT uMult = GetType(uType);

	AddDouble(Addr.a.m_Offset * uMult);

	AddDouble(uMult);

	if( Transact( ) ) {

		if( CheckFrame() ) {

			GetData(uCount, pData, uType);

			return uCount;
			}
		}
	
	return CCODE_ERROR;
	}

CCODE MCALL CIfmAsiDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);
	
	Start();

	AddByte(60);

	AddByte(0);	

	AddDouble(0x01);
	
	AddWord(Addr.a.m_Extra % 3);

	UINT uType = Addr.a.m_Type;

	UINT uMult = GetType(uType);
		
	AddDouble(Addr.a.m_Offset * uMult);
	
	AddDouble(uMult);

	if( PutData(pData, uCount, uType) ) {

		if( Transact( ) ) {

			if( CheckFrame() ) {

				return uCount;
				}
			}
		}
	
	return CCODE_ERROR;
	}

// Implementation

BOOL CIfmAsiDriver::RtsCheckTargetId(void)
{
	Start();

	AddByte(0x42);

	AddDouble(RAND(0x1234));

	return Transact();

	}

BOOL CIfmAsiDriver::RtsLogin(BOOL fLogin)
{
	Start();

	AddByte(fLogin ? 0x01 : 0x02 );

	AddDouble(0);

	AddDouble(0);

	AddDouble(0);

	return Transact();
	}

BOOL CIfmAsiDriver::GetData(UINT uCount, PDWORD pData, UINT uType)
{
	if( m_bRxBuff[14] == 1 ) {

		if( uType == addrByteAsByte ) {

			memcpy(m_bRxBuff, m_bRxBuff + 15, 1);

			return GetByteData(uCount, pData);
			}

		if( uType == addrByteAsWord || uType == addrWordAsWord ) {

			memcpy(m_bRxBuff, m_bRxBuff + 15, 2);

			return GetWordData(uCount, pData);
			}

		memcpy(m_bRxBuff, m_bRxBuff + 15, 4);

		return GetLongData(uCount, pData);
		}

	return FALSE;
	}

BOOL CIfmAsiDriver::GetByteData(UINT uCount, PDWORD pData)
{
	pData[0] = PBYTE(m_bRxBuff + 15)[0];

	return TRUE;
	}

BOOL CIfmAsiDriver::GetWordData(UINT uCount, PDWORD pData)
{
	WORD x = PWORD(m_bRxBuff + 15)[0];
	
	pData[0] = LONG(SHORT(IntelToHost(x)));

	return TRUE;
	}

BOOL CIfmAsiDriver::GetLongData(UINT uCount, PDWORD pData)
{
	DWORD x = PDWORD(m_bRxBuff + 15)[0];
	
	pData[0] = IntelToHost(x);

	return TRUE;
	}

BOOL CIfmAsiDriver::PutData(PDWORD pData, UINT uCount, UINT uType)
{
	if( uType == addrWordAsWord ) {

		return PutWordData(uCount, pData);
		}
	
	return PutLongData(uCount, pData);
	}

BOOL CIfmAsiDriver::PutByteData(UINT uCount, PDWORD pData)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddByte(LOBYTE(LOWORD(pData[u])));
		}

	return TRUE;
	}

BOOL CIfmAsiDriver::PutWordData(UINT uCount, PDWORD pData)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddWord(LOWORD(pData[u]));
		}

	return TRUE;
	}

BOOL CIfmAsiDriver::PutLongData(UINT uCount, PDWORD pData)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddDouble(pData[u]);
		}

	return TRUE;
	}

BOOL CIfmAsiDriver::CheckFrame(void) 
{
	if( m_bRxBuff[8 + 0] != 0 || m_bRxBuff[8 + 1] != 0 ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CIfmAsiDriver::Transact(void)
{
	SetDataSize();

	SetCheckSum();

	SendFrame();

	if( RecvAck() ) {

		SendAck();

		if( RecvFrame() ) {

			SendAck();

			if( RecvAck() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CIfmAsiDriver::SendFrame(void)
{
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	}

BOOL CIfmAsiDriver::RecvAck(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	UINT uPtr = 0;

	BOOL fAlign = FALSE;

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( uData == 0x55 ) {

			fAlign = TRUE;
			}

		if( !fAlign ) {

			continue;
			}

		m_bRxBuff[uPtr++] = uData;

		if( uPtr >= 4 ) {

			if( m_bRxBuff[2] == 0x0A ) {
				
				return TRUE;
				}
			}
		}

	return FALSE;

	}

void CIfmAsiDriver::SendAck(void)
{
	m_uPtr = 0;

	AddWord(0x5555);

	AddWord(0x0A);

	AddWord(0x01);

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	}

BOOL CIfmAsiDriver::RecvFrame(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	BOOL fAlign = FALSE; 

	UINT uPtr = 0;

	BYTE bCheck = 0; 

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( uData == 0xAA ) {

			fAlign = TRUE;
			}

		if( !fAlign ) {

			continue;
			}

		if( uPtr != 6 ) {

			bCheck += uData;
			}

		m_bRxBuff[uPtr++] = uData;

		if( uPtr >= UINT(m_bRxBuff[2] + 8) ) {

			if( bCheck == m_bRxBuff[6] ) {
				
				return TRUE;
				}
			
			return FALSE;
			}
		}
	
	return FALSE;
	}

BOOL CIfmAsiDriver::Start(void)
{
	m_uPtr = 0;

	m_bCheck = 0;
	
	AddWord(0xAAAA);	// wIdentity

	AddWord(0);		// wSize     m_bTxBuff[2] & m_bTxBuff[3]

	AddWord(1);		// wBlock

	AddByte(0);		// byCheck   m_bTxBuff[6]

	AddByte(1);		// byLast
	
	return TRUE;

	}

void CIfmAsiDriver::AddByte(BYTE bByte)
{
       	m_bTxBuff[m_uPtr++] = bByte;

	m_bCheck += bByte;

	}
	

void CIfmAsiDriver::AddWord(WORD wWord)
{
	AddByte(LOBYTE(wWord));
	
	AddByte(HIBYTE(wWord));
	
	}

void CIfmAsiDriver::AddDouble(DWORD dwWord)
{
	AddWord(LOWORD(dwWord));

	AddWord(HIWORD(dwWord));
	
	}

void CIfmAsiDriver::SetDataSize(void)
{
	m_bTxBuff[2] = m_uPtr - 8;

	m_bCheck += m_bTxBuff[2];

	}

void CIfmAsiDriver::SetCheckSum(void)
{
	m_bTxBuff[6] = m_bCheck;
	}

// Helpers

UINT CIfmAsiDriver::GetType(UINT uType)
{
       switch(uType) {
		
		case addrByteAsWord:
		case addrWordAsWord:

			return 2;

		case addrByteAsLong:
		case addrWordAsLong:

			return 4;

		}

	return 1;
	}

// End of File
