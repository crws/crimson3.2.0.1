
#include "intern.hpp"

#include "EIM8tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver
//

// Instantiator

INSTANTIATE(CEIMini8TCPDriver);

// Constructor

CEIMini8TCPDriver::CEIMini8TCPDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_uKeep     = 0;

	m_pAddrArr	= NULL;
	m_pPosnArr	= NULL;
	m_pSortDataArr	= NULL;
	m_pSortPosnArr	= NULL;
	}

// Destructor

CEIMini8TCPDriver::~CEIMini8TCPDriver(void)
{
	ClearArr();
	}

// Configuration

void MCALL CEIMini8TCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CEIMini8TCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CEIMini8TCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CEIMini8TCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP1		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->uWriteErrCt	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEIMini8TCPDriver::DeviceClose(BOOL fPersist)
{
	ClearArr();

	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CEIMini8TCPDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;
			
	return 0;
	}

// Entry Points

CCODE MCALL CEIMini8TCPDriver::Ping(void)
{
	if( OpenSocket() ) {

//**/		AfxTrace1("\r\nPing Ping Ping Ping Ping %d\r\n", m_pCtx->m_bDrop);

		if( m_pCtx->m_bUnit == 255 ) {

			return CCODE_SUCCESS;
			}

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = SPACE_HOLD;

		Addr.a.m_Offset = 1;

		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;

		UINT uCount	= 1;

		return Read(Addr, Data, uCount);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEIMini8TCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	if( !m_pCtx->m_bUnit ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

//**/	Sleep(100); // Slow down for debug

	m_uTickCount = GetTickCount();

	return HandleRead(Addr, pData, uCount);
	}

CCODE MCALL CEIMini8TCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	return HandleWrite(Addr, pData, uCount);
	}

// Frame Building

void CEIMini8TCPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);
	}

void CEIMini8TCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {
	
		m_bTxBuff[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CEIMini8TCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CEIMini8TCPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CEIMini8TCPDriver::Transact(BOOL fIgnore)
{
	if( SendFrame() && RecvFrame() ) {

		if( fIgnore ) {

			return TRUE;
			}

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CEIMini8TCPDriver::SendFrame(void)
{
	m_bTxBuff[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

//**/		AfxTrace1("\r\nSend %d ", uSize); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("[%2.2x]", m_bTxBuff[k]);

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEIMini8TCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

//**/			AfxTrace1("--- Recv %d\r\n", uSize); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("<%2.2x>", m_bRxBuff[k]);

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRxBuff[5];

				if( uPtr >= uTotal ) {

					if( m_bRxBuff[0] == m_bTxBuff[0] ) {

						if( m_bRxBuff[1] == m_bTxBuff[1] ) {

							memcpy(m_bRxBuff, m_bRxBuff + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRxBuff[n] = m_bRxBuff[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CEIMini8TCPDriver::CheckFrame(void)
{
	if( !(m_bRxBuff[1] & 0x80) ) {
	
		return TRUE;
		}

	return FALSE;
	}

// Mini8 Handling
CCODE CEIMini8TCPDriver::HandleRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\n\nHandle Read %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	CCODE c = CCODE_ERROR;

	if( Addr.a.m_Table == SPACE_HOLD || Addr.a.m_Table == SP_COMT ) {

		c = HandleModbusRead(Addr, pData, uCount);

		if( c & CCODE_ERROR ) {

			Sleep(40);

//**/			AfxTrace0("\r\n^^^^^^^^^^");

			c = HandleModbusRead(Addr, pData, uCount);
			}
		}

	else {
		c = HandleMini8(Addr, pData, uCount, FALSE);

		if( c & CCODE_ERROR ) {

			if( GetTickCount() - m_uTickCount < 0x20 ) {

				Sleep(40);  // Mini8 Serial sometimes needs a rest

//**/				AfxTrace0("\r\n&&&&&&&");

				c = HandleMini8(Addr, pData, uCount, FALSE);
				}
			}
		}

	ClearArr();

	return c;
	}

CCODE CEIMini8TCPDriver::HandleModbusRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\n\n### Modbus Read O=%d C=%d ", Addr.a.m_Offset, uCount );

	CAddress A;

	A.m_Ref = FixMAddr(Addr);

	if( A.a.m_Type == addrWordAsWord ) {

		return DoWordRead(A, pData, uCount);
		}

	return DoLongRead(A, pData, uCount);
	}

CCODE CEIMini8TCPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nDoWordRead %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	m_bRxBuff[7] = 0;

	StartFrame(0x03);

	AddWord((WORD)Addr.a.m_Offset);
	
	AddWord((WORD)uCount);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU2(m_bRxBuff + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CEIMini8TCPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nDoLongRead %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	m_bRxBuff[7] = 0;

	StartFrame(0x03);

	AddWord((WORD)Addr.a.m_Offset);
	
	AddWord((WORD)uCount * 2);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_bRxBuff + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CEIMini8TCPDriver::HandleWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n++++++ Write +++++ T=%d O=%x D=%8.8lx ", Addr.a.m_Table, Addr.a.m_Offset, pData[0]);

	CCODE c = CCODE_ERROR;

	if( Addr.a.m_Table == SPACE_HOLD || Addr.a.m_Table == SP_COMT ) {

		CAddress A;

		A.m_Ref = FixMAddr(Addr);

		c = A.a.m_Type == WW ? DoWordWrite(A, pData, uCount) : DoLongWrite(A, pData, uCount);
		}

	else {
		c = HandleMini8(Addr, pData, uCount, TRUE);
		}

	ClearArr();

	return c;
	}

CCODE CEIMini8TCPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nWrite Word %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount); AfxTrace1("%u ", pData[0]);
	
	StartFrame(16);
		
	AddWord((WORD)Addr.a.m_Offset);
		
	AddWord((WORD)uCount);
		
	AddByte((BYTE)uCount * 2);

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = pData[n];

		AddWord(x);
		}

	if( Transact(TRUE) ) {

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}

	return ++m_pCtx->uWriteErrCt < 3 ? CCODE_ERROR : 1;
	}

CCODE CEIMini8TCPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("Write Long %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	StartFrame(16);
		
	AddWord((WORD)Addr.a.m_Offset);
		
	AddWord((WORD)uCount * 2);
		
	AddByte((BYTE)uCount * 4);

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = pData[n];

		AddLong(x);
		}

	if( Transact(TRUE) ) {

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}

	return ++m_pCtx->uWriteErrCt < 3 ? CCODE_ERROR : 1;
	}

// Modbus Address Normalization
DWORD CEIMini8TCPDriver::FixMAddr(AREF Addr)
{
	if( Addr.a.m_Table != SP_COMT ) {

		return Addr.m_Ref;
		}

	CAddress A;

	A.m_Ref = Addr.m_Ref;

	A.a.m_Offset += 15359; // actual 15360-15615

	return A.m_Ref;
	}

// Mini8 Handling
CCODE CEIMini8TCPDriver::HandleMini8(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsWrite)
{
	uCount = fIsWrite ? 1 : min(uCount, MAXREAD);

	UINT uSCADA = IsSCADATable(Addr);

	if( uSCADA ) {	// offsets in SCADA items are in numerical order from base (= uSCADA)

		CAddress A;

		A.m_Ref      = Addr.m_Ref;

		A.a.m_Offset = uSCADA;

		return fIsWrite ? DoWordWrite(A, pData, uCount) : DoWordRead(A, pData, uCount);
		}

	MakeArr(Addr.a.m_Table);

	return fIsWrite ? DoMini8Write(Addr, pData, uCount) : DoMini8Read(Addr, pData, uCount);
	}

CCODE CEIMini8TCPDriver::DoMini8Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n******* DoMini8Read T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount); AfxTrace1("%d ", Addr.a.m_Type);

	CAddress A;

	A.m_Ref     = Addr.m_Ref;

	UINT *pSAdd = m_pSortDataArr;
	UINT *pSPos = m_pSortPosnArr;
	UINT *pAdd  = m_pAddrArr;
	UINT *pPos  = m_pPosnArr;

	DWORD pWork[MAXREAD];

	UINT uDone = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		if( GetTickCount() - m_uTickCount > 0x20 ) {

//**/			AfxTrace2("\r\nTO - Tick = %d Done=%x ", GetTickCount() - m_uTickCount, uDone);

			m_uTickCount = GetTickCount();

			return CheckDoneCt(uDone, uCount);
			}

//**/		AfxTrace1("-%d-", i);

		UINT uThis = i + Addr.a.m_Offset;

		if( pPos[uThis] < RDONE ) {	// skip read if this address has been read

			UINT uRCt = MakeBlock(uThis, uCount - i); // Get all addresses within range of uThis

//**/			AfxTrace1("\r\n=== RCT = %d === ", uRCt);

			if( !uRCt ) return CheckDoneCt(uDone, i);

//**/			AfxTrace0("\r\n"); for( UINT xx = 0; xx < uRCt; xx++ ) AfxTrace2("{A=%d P=%d}", m_pSortDataArr[xx], m_pSortPosnArr[xx]);

			UINT uSpan   = pSAdd[uRCt-1] - pSAdd[0] + 1; // number of addresses to read

			A.a.m_Offset = pSAdd[0];

			CCODE uGot = CCODE_ERROR;

			if( Addr.a.m_Type == WW ) uGot = DoWordRead(A, pWork, uSpan);

			else uGot = DoLongRead(A, pWork, uSpan);

			// Code for Debug when register is not in slave

			if( uGot & CCODE_ERROR ) {

// Code used for debugging when slave has not been programmed with this register

//**/				if( m_bRxBuff[7] == 0x83 && m_bRxBuff[8] == 0x2 ) uGot = 1L; else // address not found

				return CheckDoneCt(uDone, uCount);
				}

			UINT uRcv = (UINT)uGot;

			UINT k    = 0;

//**/			AfxTrace1("\r\nRCt = %d ", uRCt);

			while( k < uRCt ) {					// run through sorted list

				UINT uWorkPos = pSAdd[k] - pSAdd[0];		// Data position in pWork

				if( uWorkPos < uRcv ) {				// Data was read

					if( pSPos[k] < RDONE ) {

						UINT uRspPos	= pSPos[k] - Addr.a.m_Offset;		// Position in pData

						pData[uRspPos]	= pWork[uWorkPos];

						pPos[pSPos[k]]	= RDONE;		// flag this address as done

						uDone |= (1 << uRspPos);		// bit for checking return count

//**/						AfxTrace2("{%d %x}", pSAdd[k], uDone);
						}
					}

				else break;					// register not read

				if( ++k > uRcv ) break;
				}
			}

///**/		else {
///**/			AfxTrace3("\r\n---Skip %d %d %d ", i, pSAdd[i], pSPos[i]);
///**/			}
		}

	return CheckDoneCt(uDone, uCount);
	}

UINT CEIMini8TCPDriver::Check4Error(UINT ccode)
{
	if( !(ccode & CCODE_ERROR) ) {

		switch( m_bRxBuff[7] ) {

			case 0x3:	return RXGOOD;

			case 0x83:	return RXRERR; // address not found
			}
		}

	return RXBAD;
	}

CCODE CEIMini8TCPDriver::CheckDoneCt(UINT uDone, UINT uCount)
{
	// Get the number of consecutive Data starting with pData[0]

	UINT i = 0;

	while( i < uCount ) {

		if( !( 1 & (uDone >> i) ) ) break;

		i++;
		}

	if( i ) return i;

	return CCODE_ERROR;
	}

CCODE CEIMini8TCPDriver::DoMini8Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress A;

	A.m_Ref	= Addr.m_Ref;

	A.a.m_Offset = m_pAddrArr[Addr.a.m_Offset];

	uCount  = 1;

	if( Addr.a.m_Type == WW ) return DoWordWrite(A, pData, uCount);

	return DoLongWrite(A, pData, uCount);
	}

// Helpers

void CEIMini8TCPDriver::SetArrCount(UINT uTable)
{
	UINT uCount = 0;

	switch( uTable ) {

		case SP_ACC: uCount =  2;	break;
		case SP_ALA: uCount = 10 * 32;	break;
//		case SP_ALB: uCount = 11 * 4;	break;
		case SP_ALS: uCount =  22;	break;
		case SP_BCD: uCount =  2;	break;
		case SP_COM: uCount =  1;	break;
		case SP_DAL: uCount =  7 * 32;	break;
		case SP_HUM: uCount =  9;	break;
		case SP_INS: uCount =  7;	break;
		case SP_IPM: uCount = 10;	break;
		case SP_LG2: uCount =  3 * 24;	break;
		case SP_LG8: uCount =  36;	break;
//		case SP_LGI: uCount =  7;	break;
		case SP_LIN: uCount = 34;	break;
		case SP_LDG: uCount = 18 *  16;	break;
		case SP_LMN: uCount =  6 *  16;	break;
		case SP_LOP: uCount = 27 *  16;	break;
		case SP_PID: uCount = 36 *  16;	break;
		case SP_SET: uCount =  6 *  16;	break;
		case SP_SP:  uCount = 19 *  16;	break;
		case SP_TUN: uCount =  8 *  16;	break;
		case SP_MAT: uCount =  3 * 24;	break;
		case SP_MOD: uCount =  7 * 32;	break;
		case SP_MID: uCount =  4;	break;
		case SP_MUL: uCount = 12 *  4;	break;
//		case SP_PGM: uCount = 21;	break;
//		case SP_PV:  uCount = 10;	break;
		case SP_REC: uCount =  3;	break;
//		case SP_RLY: uCount =  1;	break;
		case SP_SWO: uCount =  3;	break;
		case SP_TIM: uCount =  3 * 4;	break;
//		case SP_TXD: uCount = 10 * 2;	break;
		case SP_USE: uCount = 32;	break;
		case SP_ZIR: uCount = 29;	break;
		case SP_CMC: uCount = 16;	break;
		case SP_CML: uCount =  5 * 16;	break;
		case SP_CMS: uCount = 19;	break;
		case SP_IOF: uCount =  4;	break;
		}

	m_pCtx->uArrCount = uCount;
	}

void CEIMini8TCPDriver::SetArrAddr(UINT uTable)
{
	UINT *pA	= m_pAddrArr;

	UINT uQty	= m_pCtx->uArrCount;

	UINT uInc1	= 0;
	UINT uInc2	= 0;

	UINT n		= 0;

	switch( uTable ) {

		case SP_ACC:
			
			pA[0]	= 4739;
			pA[1]	=  199;

			break;

		case SP_ALA:

			while( n < uQty ) {

				switch( n % 10 ) {

					case  0: pA[n++] = 10250 + uInc2;
					case  1: pA[n++] = 10246 + uInc2;
					case  2: pA[n++] = 10248 + uInc2;
					case  3: pA[n++] = 10242 + uInc2;
					case  4: pA[n++] = 10247 + uInc2;
					case  5: pA[n++] = 10244 + uInc2;
					case  6: pA[n++] = 10249 + uInc2;
					case  7: pA[n++] = 10243 + uInc2;
					case  8: pA[n++] = 10241 + uInc2;
					case  9: pA[n++] = 10240 + uInc2;
					}

				uInc2 += 16;
				}

			return;

//		case SP_ALB:

//			while( n < uQty ) {

//				switch( n % 11 ) {

//					case  0: pA[n++] = 10314 + uInc2;
//					case  1: pA[n++] = 10310 + uInc2;
//					case  2: pA[n++] = 10312 + uInc2;
//					case  3: pA[n++] = 10306 + uInc2;
//					case  4: pA[n++] = 10311 + uInc2;
//					case  5: pA[n++] = 10308 + uInc2;
//					case  6: pA[n++] = 10313 + uInc2;
//					case  7: pA[n++] = 10309 + uInc2;
//					case  8: pA[n++] = 10307 + uInc2;
//					case  9: pA[n++] = 10305 + uInc2;
//					case 10: pA[n++] = 10304 + uInc2;
//					}

//				uInc2  += 16;
//				}

//			return;

		case SP_ALS:

			pA[0]	= 10176;
			pA[1]	= 10177;
			pA[2]	= 10178;
			pA[3]	= 10179;
			pA[4]	= 10213;
			pA[5]	=  4192;
			pA[6]	=  4193;
			pA[7]	=  4194;
			pA[8]	=  4195;
			pA[9]	= 10188;
			pA[10]	= 10189;
			pA[11]	= 10190;
			pA[12]	= 10191;
			pA[13]	= 10214;
			pA[14]	= 10212;
			pA[15]	=  4196;
			pA[16]	= 10215;
			pA[17]	=  4197;
			pA[18]	= 10200;
			pA[19]	= 10201;
			pA[20]	= 10202;
			pA[21]	= 10203;

			break;

		case SP_BCD:

			pA[0]	= 5072;
			pA[1]	= 5073;

			break;

		case SP_COM:

			pA[0]	= 12963;

			break;

		case SP_DAL:

			while( n < uQty ) {

				switch( n % 7 ) {

					case 0: pA[n++] = 11274 + uInc2;
					case 1: pA[n++] = 11270 + uInc2;
					case 2: pA[n++] = 11272 + uInc2;
					case 3: pA[n++] = 11271 + uInc2;
					case 4: pA[n++] = 11268 + uInc2;
					case 5: pA[n++] = 11273 + uInc2;
					case 6: pA[n++] = 11264 + uInc2;
					}

				uInc2  += 16;
				}

			return;

		case SP_HUM:

			pA[0]	= 13317;
			pA[1]	= 13318;
			pA[2]	= 13313;
			pA[3]	= 13315;
			pA[4]	= 13316;
			pA[5]	= 13320;
			pA[6]	= 13314;
			pA[7]	= 13312;
			pA[8]	= 13319;

			break;

		case SP_INS:

			pA[0]	=  4737;
			pA[1]	=  4736;
			pA[2]	= 13027;
			pA[3]	=   121;
			pA[4]	=   122;
			pA[5]	=   107;
			pA[6]	=  4738;

			break;

		case SP_IPM:

			pA[0]	= 4915;
			pA[1]	= 4916;
			pA[2]	= 4919;
			pA[3]	= 4917;
			pA[4]	= 4918;
			pA[5]	= 4920;
			pA[6]	= 4921;
			pA[7]	= 4924;
			pA[8]	= 4922;
			pA[9]	= 4923;

			break;

		case SP_LG2:

			while( n < uQty ) {

				switch( n % 3 ) {

					case  0: pA[n++] = 4822 + uInc2;
					case  1: pA[n++] = 4823 + uInc2;
					case  2: pA[n++] = 4824 + uInc2;
					}

				uInc2  += 3;
				}

			return;

		case SP_LG8:

			pA[0]	= 4894;
			pA[1]	= 4895;
			pA[2]	= 4896;
			pA[3]	= 4897;
			pA[4]	= 4898;
			pA[5]	= 4899;
			pA[6]	= 4900;
			pA[7]	= 4901;
			pA[8]	= 4902;
			pA[9]	= 4903;
			pA[10]	= 4904;
			pA[11]	= 4905;
			pA[12]	= 4906;
			pA[13]	= 4907;
			pA[14]	= 4908;
			pA[15]	= 4909;
			pA[16]	= 4910;
			pA[17]	= 4911;
			pA[18]	= 5054;
			pA[19]	= 5055;
			pA[20]	= 5056;
			pA[21]	= 5057;
			pA[22]	= 5058;
			pA[23]	= 5059;
			pA[24]	= 5060;
			pA[25]	= 5061;
			pA[26]	= 5062;
			pA[27]	= 5063;
			pA[28]	= 5064;
			pA[29]	= 5065;
			pA[30]	= 5066;
			pA[31]	= 5067;
			pA[32]	= 5068;
			pA[33]	= 5069;
			pA[34]	= 5070;
			pA[35]	= 5071;

			return;

//		case SP_LGI:

//			pA[0]	= 124;
//			pA[1]	= 123;
//			pA[2]	=  45;
//			pA[3]	=  54;
//			pA[4]	= 361;
//			pA[5]	=  89;
//			pA[6]	= 362;

//			break;

		case SP_LIN:

			pA[0]	= 4960;
			pA[1]	= 4929;
			pA[2]	= 4930;
			pA[3]	= 4931;
			pA[4]	= 4932;
			pA[5]	= 4933;
			pA[6]	= 4934;
			pA[7]	= 4935;
			pA[8]	= 4936;
			pA[9]	= 4937;
			pA[10]	= 4938;
			pA[11]	= 4939;
			pA[12]	= 4940;
			pA[13]	= 4941;
			pA[14]	= 4942;
			pA[15]	= 4943;
			pA[16]	= 4928;
			pA[17]	= 4961;
			pA[18]	= 4945;
			pA[19]	= 4946;
			pA[20]	= 4947;
			pA[21]	= 4948;
			pA[22]	= 4949;
			pA[23]	= 4950;
			pA[24]	= 4951;
			pA[25]	= 4952;
			pA[26]	= 4953;
			pA[27]	= 4954;
			pA[28]	= 4955;
			pA[29]	= 4956;
			pA[30]	= 4957;
			pA[31]	= 4958;
			pA[32]	= 4959;
			pA[33]	= 4944;

			break;

		case SP_LDG:

			while( n < uQty ) {

				switch( n % 18 ) {

					case  0: pA[n++] = 119 + uInc2;
					case  1: pA[n++] = 113 + uInc2;
					case  2: pA[n++] = 118 + uInc2;
					case  3: pA[n++] = 116 + uInc2;
					case  4: pA[n++] = 114 + uInc2;
					case  5: pA[n++] = 117 + uInc2;
					case  6: pA[n++] = 120 + uInc2;
					case  7: pA[n++] = 32  + uInc2;
					case  8: pA[n++] = 33  + uInc2;
					case  9: pA[n++] = 35  + uInc2;
					case 10: pA[n++] = 34  + uInc2;
					case 11: pA[n++] = 37  + uInc2;
					case 12: pA[n++] = 38  + uInc2;
					case 13: pA[n++] = 29  + uInc2;
					case 14: pA[n++] = 36  + uInc2;
					case 15: pA[n++] = 31  + uInc2;
					case 16: pA[n++] = 30  + uInc2;
					case 17: pA[n++] = 115 + uInc2;
					}

				uInc2  += 256;
				}

			return;

		case SP_LMN:

			while( n < uQty ) {

				switch( n % 6 ) {

					case  0: pA[n++] =  4 + uInc2;
					case  1: pA[n++] = 10 + uInc2;
					case  2: pA[n++] = 20 + uInc2;
					case  3: pA[n++] =  1 + uInc2;
					case  4: pA[n++] =  2 + uInc2;
					case  5: pA[n++] =  5 + uInc2;
					}

				uInc2  += 256;
				}

			return;

		case SP_LOP:

			while( n < uQty ) {

				switch( n % 27 ) {

					case  0: pA[n++] =  84 + uInc2;
					case  1: pA[n++] =  82 + uInc2;
					case  2: pA[n++] =  16 + uInc2;
					case  3: pA[n++] =  85 + uInc2;
					case  4: pA[n++] =  83 + uInc2;
					case  5: pA[n++] =  93 + uInc2;
					case  6: pA[n++] =  91 + uInc2;
					case  7: pA[n++] =  95 + uInc2;
					case  8: pA[n++] =  96 + uInc2;
					case  9: pA[n++] =  97 + uInc2;
					case 10: pA[n++] =  94 + uInc2;
					case 11: pA[n++] =  98 + uInc2;
					case 12: pA[n++] = 103 + uInc2;
					case 13: pA[n++] =  90 + uInc2;
					case 14: pA[n++] =   3 + uInc2;
					case 15: pA[n++] =  92 + uInc2;
					case 16: pA[n++] =  80 + uInc2;
					case 17: pA[n++] =  81 + uInc2;
					case 18: pA[n++] =  86 + uInc2;
					case 19: pA[n++] =  87 + uInc2;
					case 20: pA[n++] = 102 + uInc2;
					case 21: pA[n++] = 101 + uInc2;
					case 22: pA[n++] =  89 + uInc2;
					case 23: pA[n++] = 123 + uInc2;
					case 24: pA[n++] =  88 + uInc2;
					case 25: pA[n++] = 100 + uInc2;
					case 26: pA[n++] =  99 + uInc2;
					}

				uInc2  += 256;
				}

			return;

		case SP_PID:

			while( n < uQty ) {

				switch( n % 36 ) {

					case  0: pA[n++] = 28 + uInc2;
					case  1: pA[n++] = 26 + uInc2;
					case  2: pA[n++] = 27 + uInc2;
					case  3: pA[n++] = 18 + uInc2;
					case  4: pA[n++] = 46 + uInc2;
					case  5: pA[n++] = 56 + uInc2;
					case  6: pA[n++] = 17 + uInc2;
					case  7: pA[n++] = 47 + uInc2;
					case  8: pA[n++] = 57 + uInc2;
					case  9: pA[n++] =  9 + uInc2;
					case 10: pA[n++] = 45 + uInc2;
					case 11: pA[n++] = 55 + uInc2;
					case 12: pA[n++] =  8 + uInc2;
					case 13: pA[n++] = 44 + uInc2;
					case 14: pA[n++] = 54 + uInc2;
					case 15: pA[n++] = 40 + uInc2;
					case 16: pA[n++] = 49 + uInc2;
					case 17: pA[n++] = 59 + uInc2;
					case 18: pA[n++] = 39 + uInc2;
					case 19: pA[n++] = 48 + uInc2;
					case 20: pA[n++] = 58 + uInc2;
					case 21: pA[n++] = 64 + uInc2;
					case 22: pA[n++] = 41 + uInc2;
					case 23: pA[n++] = 51 + uInc2;
					case 24: pA[n++] = 61 + uInc2;
					case 25: pA[n++] = 42 + uInc2;
					case 26: pA[n++] = 52 + uInc2;
					case 27: pA[n++] = 62 + uInc2;
					case 28: pA[n++] =  6 + uInc2;
					case 29: pA[n++] = 43 + uInc2;
					case 30: pA[n++] = 53 + uInc2;
					case 31: pA[n++] = 19 + uInc2;
					case 32: pA[n++] = 50 + uInc2;
					case 33: pA[n++] = 60 + uInc2;
					case 34: pA[n++] = 65 + uInc2;
					case 35: pA[n++] = 63 + uInc2;
					}

				uInc2  += 256;
				}

			return;

		case SP_SET:

			while( n < uQty ) {

				switch( n % 6 ) {

					case  0: pA[n++] = 22 + uInc2;
					case  1: pA[n++] = 23 + uInc2;
					case  2: pA[n++] =  7 + uInc2;
					case  3: pA[n++] = 25 + uInc2;
					case  4: pA[n++] = 21 + uInc2;
					case  5: pA[n++] = 24 + uInc2;
					}

				uInc2  += 256;
				}

			return;

		case SP_SP:

			while( n < uQty ) {

				switch( n % 19 ) {

					case  0: pA[n++] = 68 + uInc2;
					case  1: pA[n++] = 69 + uInc2;
					case  2: pA[n++] = 75 + uInc2;
					case  3: pA[n++] = 12 + uInc2;
					case  4: pA[n++] = 11 + uInc2;
					case  5: pA[n++] = 70 + uInc2;
					case  6: pA[n++] = 71 + uInc2;
					case  7: pA[n++] = 79 + uInc2;
					case  8: pA[n++] = 13 + uInc2;
					case  9: pA[n++] = 14 + uInc2;
					case 10: pA[n++] = 66 + uInc2;
					case 11: pA[n++] = 67 + uInc2;
					case 12: pA[n++] = 15 + uInc2;
					case 13: pA[n++] = 76 + uInc2;
					case 14: pA[n++] = 72 + uInc2;
					case 15: pA[n++] = 73 + uInc2;
					case 16: pA[n++] = 74 + uInc2;
					case 17: pA[n++] = 77 + uInc2;
					case 18: pA[n++] = 78 + uInc2;
					}

				uInc2  += 256;
				}

			return;

		case SP_TUN:

			while( n < uQty ) {

				switch( n % 8 ) {

					case 0: pA[n++] = 108 + uInc2;
					case 1: pA[n++] = 105 + uInc2;
					case 2: pA[n++] = 106 + uInc2;
					case 3: pA[n++] = 111 + uInc2;
					case 4: pA[n++] = 112 + uInc2;
					case 5: pA[n++] = 110 + uInc2;
					case 6: pA[n++] = 109 + uInc2;
					case 7: pA[n++] = 104 + uInc2;
					}

				uInc2  += 256;
				}

			return;

		case SP_MAT:

			while( n < uQty ) {

				switch( n % 3 ) {

					case 0: pA[n++] = 4750 + uInc2;
					case 1: pA[n++] = 4751 + uInc2;
					case 2: pA[n++] = 4752 + uInc2;
					}

				uInc2  += 3;
				}

			return;

		case SP_MOD:

			while( n < uQty ) {

				switch( n % 7 ) {

					case 0:	pA[n++]	= 4260 + uInc2;
					case 1:	pA[n++]	= 4420 + uInc2;
					case 2:	pA[n++]	= 4388 + uInc2;
					case 3:	pA[n++]	= 4356 + uInc2;
					case 4:	pA[n++]	= 4324 + uInc2;
					case 5:	pA[n++]	= 4292 + uInc2;
					case 6:	pA[n++]	= 4228 + uInc2;
					}

				uInc2++;
				}

			break;

		case SP_MID:

			pA[0]	= 12707;
			pA[1]	= 12771;
			pA[2]	= 12835;
			pA[3]	= 12899;

			break;

		case SP_MUL:

			while( n < uQty ) {

				switch( n % 12 ) {

					case  0: pA[n++] = 5017 + uInc2;
					case  1: pA[n++] = 5006 + uInc2;
					case  2: pA[n++] = 5007 + uInc2;
					case  3: pA[n++] = 5008 + uInc2;
					case  4: pA[n++] = 5009 + uInc2;
					case  5: pA[n++] = 5010 + uInc2;
					case  6: pA[n++] = 5011 + uInc2;
					case  7: pA[n++] = 5012 + uInc2;
					case  8: pA[n++] = 5013 + uInc2;
					case  9: pA[n++] = 5015 + uInc2;
					case 10: pA[n++] = 5016 + uInc2;
					case 11: pA[n++] = 5014 + uInc2;
					}

				uInc2  += 12;
				}

			return;

//		case SP_PGM:	// handled separately
//		case SP_PGS:	break;

//		case SP_PV:

//			pA[0]	= 534;
//			pA[1]	= 215;
//			pA[2]	= 38;
//			pA[3]	= 101;
//			pA[4]	= 202;
//			pA[5]	= 141;
//			pA[6]	= 360;
//			pA[7]	= 548;
//			pA[8]	= 549;
//			pA[9]	= 578;

//			break;

		case SP_REC:

			pA[0]	= 4913;
			pA[1]	= 4914;
			pA[2]	= 4912;

			break;

//		case SP_RLY:

//			pA[0]	= 363;

//			break;

		case SP_SWO:

			pA[0]	= 4927;
			pA[1]	= 4925;
			pA[2]	= 4926;

			break;

		case SP_TIM:

			while( n < uQty ) {

				switch( n % 3 ) {

					case 0: pA[n++] = 4995 + uInc2;
					case 1: pA[n++] = 4996 + uInc2;
					case 2: pA[n++] = 4994 + uInc2;
					}

				uInc2  += 3;
				}

			return;

//		case SP_TXD:

//			while( n < uQty ) {

//				switch( n % 10 ) {

//					case 0: pA[n++] = 237 + uInc2;
//					case 1: pA[n++] = 238 + uInc2;
//					case 2: pA[n++] = 233 + uInc2;
//					case 3: pA[n++] = 232 + uInc2;
//					case 4: pA[n++] = 235 + uInc2;
//					case 5: pA[n++] = 234 + uInc2;
//					case 6: pA[n++] = 226 + uInc1;
//					case 7: pA[n++] = 231 + uInc2;
//					case 8: pA[n++] = 225 + uInc1;
//					case 9: pA[n++] = 236 + uInc2;
//					}

//				uInc1  += 2;
//				uInc2  += 8;
//				}

//			return;

		case SP_USE:

			for( n = 0; n < uQty; n++ ) {

				pA[n] = 4962 + n;
				}
			break;

		case SP_ZIR:

			pA[0]	= 13256;
			pA[1]	= 13251;
			pA[2]	= 13248;
			pA[3]	= 13268;
			pA[4]	= 13252;
			pA[5]	= 13263;
			pA[6]	= 13274;
			pA[7]	= 13254;
			pA[8]	= 13253;
			pA[9]	= 13270;
			pA[10]	= 13255;
			pA[11]	= 13261;
			pA[12]	= 13260;
			pA[13]	= 13271;
			pA[14]	= 13259;
			pA[15]	= 13250;
			pA[16]	= 13262;
			pA[17]	= 13258;
			pA[18]	= 13275;
			pA[19]	= 13272;
			pA[20]	= 13257;
			pA[21]	= 13267;
			pA[22]	= 13273;
			pA[23]	= 13264;
			pA[24]	= 13269;
			pA[25]	= 13266;
			pA[26]	= 13249;
			pA[27]	= 13276;
			pA[28]	= 13265;

			break;

		case SP_CMC:

			pA[0]	= 4170;
			pA[1]	= 4171;
			pA[2]	= 4172;
			pA[3]	= 4196;
			pA[4]	= 4197;
			pA[5]	= 4103;
			pA[6]	= 4198;
			pA[7]	= 4104;
			pA[8]	= 4199;
			pA[9]	= 4105;
			pA[10]	= 4200;
			pA[11]	= 4099;
			pA[12]	= 4098;
			pA[13]	= 4100;
			pA[14]	= 4101;
			pA[15]	= 4102;

			break;

		case SP_CML:

			while( n < uQty ) {

				switch( n % 5 ) {

					case 0: pA[n++] = 4107 + uInc2;
					case 1: pA[n++] = 4106 + uInc2;
					case 2: pA[n++] = 4109 + uInc2;
					case 3: pA[n++] = 4108 + uInc2;
					case 4: pA[n++] = 4201 + uInc1;
					}

				uInc1 += 1;
				uInc2 += 4;
				}

			break;

		case SP_CMS:

			for( n = 0; n < 19; n++ ) {

				pA[n] = 4173 + n;
				}

			break;

		case SP_IOF:

			pA[0]	= 4226;
			pA[1]	= 4227;
			pA[2]	= 4224;
			pA[3]	= 4225;

			break;
		}
	}

void CEIMini8TCPDriver::ClearArr(void)
{
	if( m_pAddrArr ) {

		delete [] m_pAddrArr;

		m_pAddrArr = NULL;
		}

	if( m_pPosnArr ) {

		delete [] m_pPosnArr;

		m_pPosnArr = NULL;
		}

	if( m_pSortDataArr ) {

		delete [] m_pSortDataArr;

		m_pSortDataArr = NULL;
		}

	if( m_pSortPosnArr ) {

		delete [] m_pSortPosnArr;

		m_pSortPosnArr = NULL;
		}
	}

void CEIMini8TCPDriver::MakeArr(UINT uTable)
{
	ClearArr();

	SetArrCount(uTable);

	MakeAddr();

	SetArrAddr(uTable);
	}

void CEIMini8TCPDriver::MakeAddr(void)
{
	UINT uSize = m_pCtx->uArrCount;

	m_pAddrArr	= new UINT [uSize];

	m_pPosnArr	= new UINT [uSize];

	m_pSortDataArr	= new UINT [uSize];

	m_pSortPosnArr	= new UINT [uSize];

	for( UINT k = 0; k < uSize; k++ ) m_pPosnArr[k] = k; // initial positions

	uSize *= sizeof(UINT);

	memset(m_pAddrArr, 0, uSize);

	memset(m_pSortDataArr, 0, uSize);

	memset(m_pSortPosnArr, 0xFF, uSize);
	}

UINT CEIMini8TCPDriver::IsSCADATable(CAddress Addr)
{
	switch( Addr.a.m_Table ) {

		case SP_PGM:
			return PGMBASE + Addr.a.m_Offset;

		case SP_PGS:
			return PGSBASE + Addr.a.m_Offset;
		}

	return 0;
	}

UINT CEIMini8TCPDriver::MakeBlock(UINT uStart, UINT uCount)
{
	UINT *pA   = m_pAddrArr;

//**/	AfxTrace0("\r\nMakeBlock ");

	UINT uThis = pA[uStart];

	UINT *pP   = m_pPosnArr;

	if( uCount == 1 ) {

		m_pSortDataArr[0] = uThis;

		m_pSortPosnArr[0] = pP[uStart];

//**/		AfxTrace1("--- Only 1 %d ", uThis);

		return 1;
		}

	UINT uMaxP = uStart + uCount; // A useable Array Position must be < uMaxP

	UINT uCt   = 0;

	UINT uTarg;

	UINT pAWork[MAXREAD];
	UINT pPWork[MAXREAD];

	memset(pAWork, 0xFF, MAXREAD);
	memset(pPWork, 0xFF, MAXREAD);

	UINT uEnd = uStart + uCount;

//**/	UINT uMax = uThis;

	for( UINT i = uStart; i < uEnd; i++ ) { // check for offsets that are within range of uCount

		uTarg = pA[i];

		if( (uTarg >= uThis) && (uTarg < uThis + MAXREAD) && (pP[i] <= uMaxP) ) { // cache only addresses within range of start address and offset

			pAWork[uCt] = uTarg;
			pPWork[uCt] = pP[i]; // position in original list

//**/			if( !uCt ) AfxTrace1("%d - ", pAWork[0]);
//**/			if( pAWork[uCt] > uMax ) uMax = pAWork[uCt];

			uCt++; // quantity of results to return
			}
		}

//**/	AfxTrace1("%d ", uMax); // show range of addresses accessible

	DoAddrSort(uCt, pAWork, pPWork);

	return uCt;
	}

void CEIMini8TCPDriver::DoAddrSort(UINT uCount, UINT * pASrc, UINT * pPSrc)
{
	UINT *pADest = m_pSortDataArr;
	UINT *pPDest = m_pSortPosnArr;

	for( UINT i = 0; i < uCount; i++ ) {

		pADest[i] = pASrc[i];
		pPDest[i] = pPSrc[i];
		}

	i = 0;

	UINT j = 1;

	while( j < uCount ) {

		if( pADest[i] > pADest[j] ) {

			SwapPositions(pADest, pPDest, i, j);

			i = 0;
			j = 1;
			}

		else {
			i++;
			j++;
			}
		}
	}

void CEIMini8TCPDriver::SwapPositions(UINT *pAdd, UINT *pPos, UINT ui, UINT uj)
{
	SwapItem(pAdd, ui, uj);
	SwapItem(pPos, ui, uj);
	}

void CEIMini8TCPDriver::SwapItem(UINT *p, UINT ui, UINT uj)
{
	UINT u = p[ui];
	p[ui]  = p[uj];
	p[uj]  = u;
	}

// Socket Management

BOOL CEIMini8TCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	CloseSocket(FALSE);

	return FALSE;
	}

BOOL CEIMini8TCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   Port;

		IP   = (IPADDR const &) m_pCtx->m_IP1;

		Port = WORD(m_pCtx->m_uPort);

		if( m_pCtx->m_pSock->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		return FALSE;
		}

	return FALSE;
	}

void CEIMini8TCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
