//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Invensys Mini8x Data Spaces
//

#define	SPACE_HOLD	0x04
// The following map to Modbus Holding Registers
#define	SP_COMT	99	// Indirect Register Access
#define	SP_ACC	10	// Access
#define	SP_ALA	11	// Alarm.1 - Alarm.4
#define	SP_ALB	12	// Alarm.5 - Alarm.8
#define	SP_ALS	13	// Alarm Summary
#define	SP_BCD	14	// BCDInput
#define	SP_COM	15	// Comms
#define	SP_DAL	16	// DigAlarm
#define	SP_HUM	17	// Humidity
#define	SP_INS	18	// Instrument
#define	SP_IPM	19	// IPMonitor
#define	SP_LG2	20	// Lgc2
#define	SP_LG8	21	// Lgc8
#define	SP_LGI	22	// LgcIO
#define	SP_LIN	23	// Lin16
#define	SP_LDG	24	// Loop Diag
#define	SP_LMN	25	// Loop Main
#define	SP_LOP	26	// Loop OP
#define	SP_PID	27	// Loop PID
#define	SP_SET	28	// Loop Setup
#define	SP_SP	29	// Loop SP
#define	SP_TUN	30	// Loop Tune
#define	SP_MAT	31	// Math2
#define	SP_MOD	32	// Mod
#define	SP_MID	33	// ModIDs
#define	SP_MUL	34	// MultiOper
#define	SP_PGM	35	// Programmer
#define	SP_PV	36	// PV
#define	SP_REC	37	// Recipe
#define	SP_RLY	38	// RelayAA
#define	SP_SWO	39	// SwitchOver
#define	SP_TIM	40	// Timer
#define	SP_TXD	41	// Txdr
#define	SP_USE	42	// UsrVal
#define	SP_ZIR	43	// Zirconia
// different from 350x
#define	SP_CMC	44	// IO.CurrentMonitor.Config Global
#define	SP_CML	45	// IO.CurrentMonitor.Config.Load
#define	SP_CMS	46	// IO.CurrentMonitor.Status
#define	SP_IOF	47	// IO.FixedIO

// Added May 2011
#define	SP_PGS	48	// Prgr Segments
// End Space definitions

#define	PGCNT	 8	// 8 Programmers

// Programmer size
#define	PGMCNT	64	// 64 data items per Programmer General Data

// Prgr sizes
#define	SEGCNT	16	// 16 segments are allocated per Programmer
#define	SEGMAX	32	// 32 modbus addresses are allocated per segment
#define	PGSMAX	512	// 512 modbus addresses are allocated per programmer

#define	PGMBASE	5568
#define	PGSBASE	6080

#define	MAXREAD	20
#define	RDONE	0x0FFF

#define	RXGOOD	0
#define	RXRERR	1	// register not found
#define	RXBAD	2	// no response or unintelligible

#define	WW	addrWordAsWord
#define	LL	addrLongAsLong

//////////////////////////////////////////////////////////////////////////
//
// Modbus TCP/IP Master Driver
//

class CEIMini8TCPDriver : public CMasterDriver
{
	public:
		// Constructor
		CEIMini8TCPDriver(void);

		// Destructor
		~CEIMini8TCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
				
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP1;
			UINT	 m_uPort;
			BYTE	 m_bUnit;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL     m_fDirty;

			UINT	uArrCount;
			UINT	uWriteErrCt;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		UINT	   m_uKeep;
		DWORD	   m_uTickCount;

		// Modbus Address Arrays
		UINT	*m_pAddrArr;
		UINT	*m_pPosnArr;
		UINT	*m_pSortDataArr;
		UINT	*m_pSortPosnArr;

		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
				
		// Transport Layer
		BOOL Transact(BOOL fIgnore);
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL CheckFrame(void);
		
		// Read Handlers
		CCODE	HandleRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	HandleModbusRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	HandleWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Modbus Address Normalization
		DWORD	FixMAddr(AREF Addr);

		// Mini8 handling
		CCODE	HandleMini8 (AREF Addr, PDWORD pData, UINT uCount, BOOL fIsWrite);
		CCODE	DoMini8Read (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoMini8Write(AREF Addr, PDWORD pData, UINT uCount);
		UINT	Check4Error(UINT ccode);
		CCODE	CheckDoneCt(UINT uDone, UINT uCount);

		// Helpers
		void	SetArrCount(UINT uTable);
		void	SetArrAddr(UINT uTable);
		void	ClearArr(void);
		void	MakeArr (UINT uTable);
		void	MakeAddr(void);
		UINT	IsSCADATable(CAddress Addr);
		UINT	MakeBlock(UINT uStart, UINT uCount);
		void	DoAddrSort(UINT uCount, UINT * pA, UINT * pP);
		void	SwapPositions(UINT *pAdd, UINT *pPos, UINT ui, UINT uj);
		void	SwapItem(UINT *p, UINT ui, UINT uj);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
	};

// End of File
