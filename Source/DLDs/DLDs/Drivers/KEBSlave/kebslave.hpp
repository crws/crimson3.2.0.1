
//////////////////////////////////////////////////////////////////////////
//
// KEB Slave Driver
//

// Receive States
#define	RCV0	0
#define	RCVAD1	1
#define	RCVAD2	2
#define	RCVIID	3
#define	RCVDAT	4
#define	RCVCKS	5

class CKEBSlave : public CSlaveDriver
{
	public:
		// Constructor
		CKEBSlave(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
		// Entry Points
		DEFMETH(void) Service(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:
		// Device Data
		struct CContext
		{
			WORD	m_Model;
			BYTE	m_bRcvDrop;
			};

		CContext *	m_pCtx;

		// Data
		LPCTXT	m_pHex;
		BYTE	m_bDrop;
		BYTE	m_bTx[32];
		BYTE	m_bRx[32];

		BYTE	m_bRcvComm;
		BYTE	m_bIID;
		BYTE	m_bCheck;

		UINT	m_uPtr;

		BOOL	m_fIsWrite;
		BOOL	m_fLongCommand;

		// Frame Handlers
		void	HandleRead(void);
		void	HandleWrite(void);
		
		// Frame Building
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		
		// Transport Layer & Port Access
		void	PutFrame(void);
		BOOL	GetFrame(void);

		DWORD	FromHexRx( UINT uPos, UINT uCount );
		BYTE	GetHexDigit(BYTE b);
		void	InitModelTags(void);
	};

// End of File
