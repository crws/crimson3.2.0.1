
#include "intern.hpp"

#include "iqan.hpp" 

//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN Driver
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CIqanDriver);

// Constructor

CIqanDriver::CIqanDriver(void)
{
	m_Ident    = DRIVER_ID;

	m_uRPDO    = 0;

	m_uTPDO    = 0;	

	m_fInit    = FALSE;
       	}

// Destructor

CIqanDriver::~CIqanDriver(void)
{
	}

// Configuration

void MCALL CIqanDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bDrop    = GetByte(pData);

		m_bOp	   = GetByte(pData) ? 0 : 1;

		m_fGuard   = 0;

		m_bDrop    = 0;

		m_bDecode  = 0;
		
		m_bProfile = 0;
		}
	}

void MCALL CIqanDriver::CheckConfig(CSerialConfig &Config)
{
	Config.m_bDrop = m_bDrop;

	Config.m_uFlags |= flagPrivate;
	}
	
// Management

void MCALL CIqanDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CIqanPDOHandler(m_pHelper);

	m_pHandler->SetDrop(m_bDrop);

	m_pHandler->SetDecode(m_bDecode);
	
	m_pHandler->SetGuard(m_fGuard);

	m_pHandler->SetProfile(m_bProfile);

	m_pHandler->SetOp(m_bOp);

	m_pHandler->AllocObjects(TRUE, 64);

	m_pHandler->AllocObjects(FALSE, 64);
	
	pPort->Bind(m_pHandler);
	}

void MCALL CIqanDriver::Detach(void)
{
	m_pHandler->Release();
	}

void MCALL CIqanDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CIqanDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			for( UINT u = 0; u < elements(m_pCtx->m_Send); u++ ) {

				m_pCtx->m_Send[u] = GetByte(pData);

				m_pCtx->m_Rate[u] = GetLong(pData);
				}

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CIqanDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE CIqanDriver::Ping(void)
{
	if( !m_fInit ) {

		return CCODE_SUCCESS;
		}

	for( UINT u = 0; u < m_uRPDO; u++ ) {

		PDO * pPDO = m_pHandler->GetRPDO(u);

		if( pPDO ) {

			if( !m_pHandler->IsTimedOut(pPDO) ) {

				return CCODE_SUCCESS;
				}
			}
		}			

	return CCODE_ERROR;
	}

CCODE CIqanDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsReadOnly(Addr.a.m_Table) ) {

		DWORD dwID = (Addr.a.m_Offset >> 6) + 0x181;

		BYTE  bBit = (Addr.a.m_Offset & 0x3F);

		PDO * pPDO = m_pHandler->FindPDORecord(dwID, RPDO);

		if( !pPDO ) {

			pPDO = m_pHandler->GetRPDO(m_uRPDO);

			if( pPDO ) {

				memset(pPDO->bData, 0, 8);

				m_pHandler->InitPDORecord(pPDO, dwID >> 7, dwID, 3000);

				if( pPDO ) {

					m_uRPDO++;

					m_fInit = TRUE;
					}
				}
			}

		if( pPDO ) {

			if( m_pHandler->IsTimedOut(pPDO) ) {

				return CCODE_ERROR;
				}

			GetData(Addr, pPDO, pData, bBit, uCount);
			}
		}

	return uCount;
	}

CCODE CIqanDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{     
	if( IsWriteOnly(Addr.a.m_Table) ) {

		DWORD dwID = (Addr.a.m_Offset >> 6) + 0x181;

		BYTE  bBit = (Addr.a.m_Offset & 0x3F);

		PDO * pPDO = m_pHandler->FindPDORecord(dwID, TPDO);

		if( !pPDO ) {

			pPDO = m_pHandler->GetTPDO(m_uTPDO);

			if( pPDO ) {

				memset(pPDO->bData, 0, 8);

				UINT uPDO = FindPDOType(dwID);

				if( uPDO < NOTHING ) {

					m_pHandler->InitPDORecord(pPDO, dwID >> 7, dwID, m_pCtx->m_Send[uPDO] ? m_pCtx->m_Rate[uPDO] : 0);
					}
				else {
					m_pHandler->InitPDORecord(pPDO, dwID >> 7, dwID, 0);
					}	

				m_uTPDO++;
				}
			}

		if( pPDO ) {

			PutData(Addr, pPDO, pData, bBit, uCount);

			pPDO->fDirty = TRUE;
			}
		}

	return uCount;
	}

void MCALL CIqanDriver::Service(void)
{
	}

BOOL CIqanDriver::IsReadOnly(UINT uTable)
{
	return uTable % 2 == 0 ? TRUE : FALSE;
	}

BOOL CIqanDriver::IsWriteOnly(UINT uTable)
{
	return uTable % 2 ? TRUE : FALSE;
	}

UINT CIqanDriver::FindPDOType(DWORD dwID)
{
	if( dwID < 0x0280 ) {

		return 0;
		}

	if( dwID < 0x0380 ) {

		return 1;
		}

	if( dwID < 0x0480 )  {

		return 2;
		}

	if( dwID < 0x0580 ) {

		return 3;
		}

	return NOTHING;
	}

void CIqanDriver::GetData(CAddress Addr, PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount)
{
	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	GetDataBits (pPDO, pData, uOffset, uCount);	break;
		
		case addrByteAsByte:
		case addrBitAsByte:	GetDataBytes(pPDO, pData, uOffset, uCount);	break;
		
		case addrBitAsWord:
		case addrByteAsWord:
		case addrWordAsWord:	GetDataWords(pPDO, pData, uOffset, uCount);	break;
		
		case addrBitAsLong:
		case addrByteAsLong:
		case addrWordAsLong:	GetDataLongs(pPDO, pData, uOffset, uCount);	break;
		}
	}

void CIqanDriver::PutData(CAddress Addr, PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount)
{
	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	PutDataBits (pPDO, pData, uOffset, uCount);	break;

		case addrByteAsByte:
		case addrBitAsByte:	PutDataBytes(pPDO, pData, uOffset, uCount);	break;

		case addrBitAsWord:
		case addrByteAsWord:
		case addrWordAsWord:	PutDataWords(pPDO, pData, uOffset, uCount);	break;

		case addrBitAsLong:
		case addrByteAsLong:
		case addrWordAsLong:	PutDataLongs(pPDO, pData, uOffset, uCount);	break;
		}	
	}

void CIqanDriver::GetDataBits(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount)
{	
	for( UINT u = 0, i = uOffset; u < uCount; u++, i++ ) {
	
		pData[u] = (pPDO->bData[i / 8] >> i % 8) & 0x1; 
		}
	}

void CIqanDriver::GetDataBytes(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount)
{
	for( UINT u = 0, i = uOffset; u < uCount; u++, i += 8 ) {
	
		if( i % 8 == 0 ) {

			pData[u] = pPDO->bData[i / 8]; 
			}
		else {
			UINT uMod = i % 8;

			WORD x = PU2(pPDO->bData + i / 8)[0];

			x = SHORT(IntelToHost(x));

			pData[u] = (x >> uMod) & 0xFF;
			}
		}
	}

void CIqanDriver::GetDataWords(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount)
{
	for( UINT u = 0, i = uOffset; u < uCount; u++, i += 16 ) {
	
		if( i % 8 == 0 ) {

			WORD x   = PU2(pPDO->bData + i / 8)[0];

			pData[u] = LONG(SHORT(IntelToHost(x))); 
			}
		else {
			UINT uMod = i % 8;

			DWORD x = PU4(pPDO->bData + i / 8)[0];

			x = IntelToHost(x);

			pData[u] = (x >> uMod) & 0xFFFF;
			}
		}
	}

void CIqanDriver::GetDataLongs(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount)
{
	for( UINT u = 0, i = uOffset; u < uCount; u++, i += 32 ) {
	
		if( i % 8 == 0 ) {

			DWORD x = PU4(pPDO->bData + i / 8)[0];

			pData[u] = IntelToHost(x);  
			}
		else {
			UINT uMod = i % 8;
			
			DWORD x = PU4(pPDO->bData + i / 8)[0];

			DWORD y = PU4(pPDO->bData + i / 8 + sizeof(DWORD))[0];

			x = IntelToHost(x);

			y = IntelToHost(y);

			pData[u] = ((x >> uMod) | (y << (32 - uMod)));
			}
		}
	}

void CIqanDriver::PutDataBits(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount)
{	
	for( UINT u = 0, i = uOffset; u < uCount; u++, i++ ) {

		BYTE bMask = 1 << (i % 8);

		if( pData[u] ) {

			pPDO->bData[i / 8] |= bMask;
			}
		else {
			pPDO->bData[i / 8] &= ~bMask;
			}
		}
	}

void CIqanDriver::PutDataBytes(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount)
{
	for( UINT u = 0, i = uOffset; u < uCount; u++, i += 8 ) {
	
		if( i % 8 == 0 ) {

			pPDO->bData[i / 8] = pData[u] & 0xFF;
			}
		else {
			WORD x = LOBYTE(LOWORD(pData[u]));

			UINT uMod = i % 8;

			x = x << uMod;

			BYTE bMask = 0xFF << uMod;

			pPDO->bData[i / 8] &= ~bMask;

			pPDO->bData[i / 8] |= LOBYTE(x);

			bMask = 0xFF >> uMod;

			pPDO->bData[i / 8 + 1] &= ~bMask;

			pPDO->bData[i / 8 + 1] |= HIBYTE(x);

			i += 8;
			}
		}
	}

void CIqanDriver::PutDataWords(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount)
{
	for( UINT u = 0, i = uOffset; u < uCount; u++ ) {
	
		if( i % 8 == 0 ) {

			WORD x = LOWORD(pData[u]);

			pPDO->bData[i / 8] = LOBYTE(x);

			pPDO->bData[i / 8 + 1] = HIBYTE(x);
			
			i += 16;
			}
		else {
			DWORD x = LOWORD(pData[u]);

			UINT uMod = i % 8;

			x = x << uMod;

			BYTE bMask = 0xFF << uMod;

			pPDO->bData[i / 8] &= ~bMask;

			pPDO->bData[i / 8] |= LOBYTE(x);

			pPDO->bData[i / 8 + 1] = HIBYTE(x);

			bMask = 0xFF >> uMod;

			pPDO->bData[i / 8 + 2] &= ~bMask;

			pPDO->bData[i / 8 + 2] |= LOBYTE(HIWORD(x));

			i += 16;
			}
		}
	}

void CIqanDriver::PutDataLongs(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount)
{
	for( UINT u = 0, i = uOffset; u < uCount; u++ ) {
	
		if( i % 8 == 0 ) {

			DWORD x = HostToIntel(pData[u]);

			memcpy(PBYTE(pPDO->bData + i / 8), PBYTE(&x), sizeof(DWORD));

			i += 32;
			}
		else {
			UINT uMod = i % 8;

			DWORD x = pData[u] << uMod;

			BYTE bMask = 0xFF << uMod;

			pPDO->bData[i / 8] &= ~bMask;

			pPDO->bData[i / 8] |= LOBYTE(LOWORD(x));

			pPDO->bData[i / 8 + 1] = HIBYTE(LOWORD(x));

			pPDO->bData[i / 8 + 2] = LOBYTE(HIWORD(x));

			pPDO->bData[i / 8 + 3] = HIBYTE(HIWORD(x));

			pPDO->bData[i / 8 + 4] &= ~bMask;

			pPDO->bData[i / 8 + 4] |= (HIBYTE(HIWORD(pData[u])) >> (8 - uMod));

			i += 32;
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN PDO Port Handler
//

// Base

#include "..\ciapdos\ciapdo.cpp"

// Constructor

CIqanPDOHandler::CIqanPDOHandler(IHelper *pHelper) : CCANPortPDOHandler(pHelper)
{
	}

void METHOD CIqanPDOHandler::OnTimer(void)
{
	if( m_pTPDO ) {

		if( m_fOp ) {

			for( UINT u = 0; u < m_uTPDO; u++ ) {

				PDO * pPDO = &m_pTPDO[u];

				if( pPDO->uEvent || pPDO->fDirty ) {

					UINT uTimer = GetTickCount();

					if( pPDO->fDirty || IsTimedOut(pPDO) ) {

						Start(transTimer);

						m_Tx[transTimer].bCount = 8;

						m_Tx[transTimer].wID    = pPDO->dwCobID;

						memcpy(m_Tx[transTimer].bData, pPDO->bData, 8);

						PutFrame(transTimer); 
							
						pPDO->uTimer = GetTickCount();

						pPDO->fDirty = FALSE;
						}
					}
				}
			}
		}  
	}

void CIqanPDOHandler::InitPDORecord(PDO * pPDO, UINT uPDO, DWORD dwMap, UINT uTimeOut)
{
	if( pPDO ) {
		
		pPDO->dwCobID = dwMap;

		pPDO->uEvent  = ToTicks(uTimeOut);

		pPDO->uTimer  = GetTickCount();
		}
	}

PDO * CIqanPDOHandler::GetTPDO(UINT uIndex)
{
	if( uIndex < m_uTPDO ) {

		return &m_pTPDO[uIndex];
		}

	return NULL;
	}

BOOL CIqanPDOHandler::HandleRPDO(void)
{
	if( IsRPDO() ) {

		PDO * pPDO = FindPDORecord(m_Rx.wID, RPDO);

		memcpy(pPDO->bData, m_Rx.bData, 8);

		pPDO->uTimer = GetTickCount();

		return TRUE;
		}

  	return FALSE;
	}


BOOL CIqanPDOHandler::IsSync(void)
{
	return ( m_Rx.wID == 0x80 );
	}

BOOL CIqanPDOHandler::IsNMT(void)
{
	return ( (m_Rx.bCount >= 0x2) && (m_Rx.wID == 0) ); 
	}

BOOL CIqanPDOHandler::IsRPDO(void)
{
	BYTE bCode = m_Rx.wID >> 7;

	return ( (bCode < 0x0C) && (bCode >= 3) );
	}

BOOL CIqanPDOHandler::DoListen(void)
{
	return TRUE;
	}

BOOL CIqanPDOHandler::IsTimedOut(PDO * pPDO)
{
	return int(GetTickCount() - pPDO->uTimer - pPDO->uEvent) >= 0;
	}

BOOL CIqanPDOHandler::GetOp(void)
{
	return m_fOp;
	}

// End of File


