
#include "..\ciapdos\ciapdo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN PDO Handler
//

class CIqanPDOHandler : public CCANPortPDOHandler
{	
	public:
		// Constructor
		CIqanPDOHandler(IHelper *pHelper);

		//IPortHandler
		void METHOD OnTimer(void);

		// PDO Access
		void	InitPDORecord(PDO * pPDO, UINT uPDO, DWORD dwMap, UINT uTimeOut);
		PDO *	GetTPDO(UINT uIndex);
		BOOL	IsTimedOut(PDO * pPDO);
		BOOL    GetOp(void);
		
	protected:

		BOOL HandleRPDO(void);
		BOOL IsSync(void);
		BOOL IsNMT(void);
		BOOL IsRPDO(void);
		BOOL DoListen(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN Driver
//

class CIqanDriver : public CMasterDriver
{
	public:
		// Constructor
		CIqanDriver(void);

		// Destructor
		~CIqanDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
						
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(void)	Service(void);

	protected:
		// Handler
		CIqanPDOHandler * m_pHandler;

		struct CContext {

			BYTE m_Send[4];
			UINT m_Rate[4];
			};

		CContext * m_pCtx;

		// Data Members
		UINT m_uRPDO;
		UINT m_uTPDO;
		BYTE m_bDrop;
		BYTE m_bProfile;
		BYTE m_bOp;
		BOOL m_fGuard;
		BYTE m_bDecode;
		BOOL m_fInit;
				
		// Implementation
		BOOL IsReadOnly(UINT uTable);
		BOOL IsWriteOnly(UINT uTable);
		UINT FindPDOType(DWORD dwID);
		void GetData(CAddress Addr, PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount);
		void PutData(CAddress Addr, PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount);

		void GetDataBits (PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount);
		void GetDataBytes(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount);
		void GetDataWords(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount);
		void GetDataLongs(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount);

		void PutDataBits (PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount);
		void PutDataBytes(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount);
		void PutDataWords(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount);
		void PutDataLongs(PDO * pPDO, PDWORD pData, UINT uOffset, UINT uCount);
	};	

// End of file
