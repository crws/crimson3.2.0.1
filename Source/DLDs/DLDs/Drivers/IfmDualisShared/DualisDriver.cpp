
//////////////////////////////////////////////////////////////////////////
//
// Dualis Vision Sensor Driver
//

// Macro

#define IsNum(c)	((c) >= '0' && (c) <= '9')

// Configuration

void MCALL CDualisDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CDualisDriver::Open(void)
{
	}

// Entry Points

CCODE MCALL CDualisDriver::Ping(void)
{
	Trace("Ping\n");

	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				Trace("Ping:no socket\n");

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}

		Trace("Ping:no ip\n");

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

// Socket Management

BOOL CDualisDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			Trace("phase error\n");

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CDualisDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast3;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {
			
					m_pCtx->m_uLast4 = GetTickCount();

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CDualisDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		Trace("close socket %s\n", fAbort ? "abort" : "close");

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock  = NULL;

		m_pCtx->m_uLast3 = GetTickCount();

		m_uKeep--;
		}
	}

// Implementation

CCODE CDualisDriver::Parse(PCTXT pCmd, PDWORD pData)
{
	PTXT pReply = m_pCtx->m_sRecv;

	Trace("reply [%s]<%s>\n", pCmd, pReply);

	if( !strcmp(pCmd, "a") ) {

		if( *pReply == '!' ) {
				
			return CCODE_ERROR | CCODE_NO_DATA;
			}

		PTXT p = strtok(pReply, " ");

		for( UINT n = 0; p; n ++ ) {

			if( n == 1 ) {				
				
				*pData = ATOI(p+1);

				return 1;
				}

			p = strtok(NULL, " ");
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	if( !strcmp(pCmd, "t") || 
	    !strcmp(pCmd, "p") || 
	    !strcmp(pCmd, "c") ||
	    !strcmp(pCmd, "v") ){

		if( *pReply == '!' ) {
				
			return CCODE_ERROR | CCODE_NO_DATA;
			}

		if( *pReply == '*' ) {

			if( !strcmp(pCmd, "v") ) {
				
				m_pCtx->m_uProtocol = *pData;
				}

			return 1;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	if( !strcmp(pCmd, "V") ) {

		PTXT p = strtok(pReply, " ");

		for( UINT n = 0; p; n ++ ) {

			if( n == 0 ) {
				
				*pData = ATOI(p);

				return 1;
				}

			p = strtok(NULL, " ");
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	if( !strcmp(pCmd, "E") ) {

		*pData = ATOI(pReply);
		
		return 1;
		}

	if( !strcmp(pCmd, "I") || 
	    !strcmp(pCmd, "F") ){

		if( *pReply == '!' ) {

			return CCODE_ERROR | CCODE_NO_DATA;
			}
		
		if( *pReply == '?' ) {

			return CCODE_ERROR | CCODE_HARD;
			}

		return CCODE_SUCCESS;
		}	

	if( !strcmp(pCmd, "T") ||
	    !strcmp(pCmd, "R") ){

		memset(&m_pCtx->m_Result, 0, sizeof(m_pCtx->m_Result));

		if( *pReply == '!' ) {

			return CCODE_ERROR | CCODE_NO_DATA;
			}
		
		if( *pReply == '?' ) {

			return CCODE_ERROR | CCODE_HARD;
			}

		PTXT p = strtok(pReply, " ");

		for( UINT n = 0; p; n ++ ) {

			if( n == 0 ) {				
				
				m_pCtx->m_Result.m_fResult = !strcmp(p, "PASS");
				}

			if( n == 1 ) {
				
				m_pCtx->m_Result.m_uMatch = ParseFixed(p);
				}

			p = strtok(NULL, " ");
			}

		m_pCtx->m_Result.m_fValid = TRUE;

		return CCODE_SUCCESS;
		}
	
	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CDualisDriver::Parse(PCTXT pCmd, PDWORD pData, UINT uIndex, UINT uCount)
{
	PTXT pReply = m_pCtx->m_sRecv;

	Trace("reply [%s]<%s>\n", pCmd, pReply);

	if( !strcmp("s", pCmd) || 
	    !strcmp("a", pCmd) ){

		if( *pReply == '!' ) {
			
			return CCODE_ERROR | CCODE_NO_DATA;
			}

		PTXT p = strtok(pReply, " ");

		for( UINT n = 0; p; n ++ ) {

			if( n == uIndex ) {
				
				*pData ++ = ATOI(p);

				uIndex ++;
				}

			p = strtok(NULL, " ");
			}

		return uCount;
		}

	if( !strcmp("R", pCmd) ) {

		if( *pReply == '!' ) {
			
			return CCODE_ERROR | CCODE_NO_DATA;
			}

		PTXT p = strtok(pReply, " ");

		for( UINT n = 0; p; n ++ ) {

			if( n == uIndex ) {

				switch( n ) {
					
					case 0:
						*pData ++ = !strcmp("PASS", p) ? 1 : 0;
						break;

					case 1:
						*pData ++ = ParseFixed(p);
						break;
					}

				uIndex ++;
				}

			p = strtok(NULL, " ");
			}

		return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

UINT CDualisDriver::FindFactor(PCTXT p)
{
	while( *p && IsNum(*p) && !(*p - '0') ) p ++;

	for( UINT uFact = 1; *p; p++ ) {

		if( IsNum(*p) ) {			
		
			uFact *= 10;
			}
		}	

	return uFact/10;
	}

UINT CDualisDriver::ParseFixed(PCTXT p)
{
	while( *p && IsNum(*p) && !(*p - '0') ) p ++;	

	UINT uFactor = FindFactor(p);		

	for( UINT uValue = 0; *p; p ++ ) {		

		if( IsNum(*p) ) {
		
			uValue  += (*p - '0') * uFactor;

			uFactor /= 10;
			}
		}

	return uValue;
	}

void CDualisDriver::PunchTicket(void)
{
	do {
		m_pCtx->m_uTicket = (m_pCtx->m_uTicket + 1) % 1000;

		} while( !m_pCtx->m_uTicket );
	}

BOOL CDualisDriver::Send(PCTXT pCmd, BOOL fRqst)
{
	PunchTicket();

	m_pCtx->m_sSend[0] = '\0';

	char sBuff[128] = { 0 };	

	switch( m_pCtx->m_uProtocol ) {

		case 1:
			SPrintf(sBuff, fRqst ? "%s?\r\n" : "%s\r\n", 
						pCmd
						);

			HostPrintf( "%s", 
				    sBuff);

			break;

		case 2:
			SPrintf(sBuff, fRqst ? "%s?\r\n" : "%s\r\n", 
						pCmd
						);

			HostPrintf( "%04d%s", 
				    m_pCtx->m_uTicket, 
				    sBuff
				    );

			break;

		case 3:
			SPrintf(sBuff, fRqst ? "%04d%s?\r\n" : "%04d%s\r\n", 
						m_pCtx->m_uTicket, 
						pCmd
						);

			HostPrintf( "%04dL%09d\r\n%s", 
				    m_pCtx->m_uTicket, 
				    strlen(sBuff),
				    sBuff
				    );

			break;

		default:
			return FALSE;
		}

	return SendFrame();
	}

BOOL CDualisDriver::SendFrame(void)
{
	UINT uSize = strlen(m_pCtx->m_sSend);

	if( m_pCtx->m_pSock->Send(PBYTE(m_pCtx->m_sSend), uSize) == S_OK ) {

		Trace(">>> %d bytes sent <%s>\n", uSize, m_pCtx->m_sSend);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDualisDriver::RecvImage(PBYTE &pImage, UINT uProtocol)
{
	Trace("recvimage %d\n", uProtocol);

	if( uProtocol == 1 ) {

		return RecvImage(pImage);
		}

	if( uProtocol == 2 ) {

		UINT uTicket;

		if( (uTicket = ReadTicket()) < NOTHING ) {

			if( uTicket == 0 ) {
				
				return RecvResult(pImage);
				}

			if( RecvImage(pImage) ) {
			
				if( uTicket == m_pCtx->m_uTicket ) {
					
					return TRUE;
					}

				Trace("recv image - mismatched ticket\n");

				return TRUE;
				}
			}
		}

	if( uProtocol == 3 ) {

		PCTXT pFmt = "1234L123456789\r\n";

		char sBuff[32] = { 0 };

		if( Read(PBYTE(sBuff), strlen(pFmt)) ) {
			
			return RecvImage(pImage, 2);
			}
		}

	return FALSE;
	}

BOOL CDualisDriver::RecvImage(PBYTE &pImage)
{
	Trace("recvimage\n");

	char sBuff[128] = { 0 };

	UINT       uPtr = 0;

	UINT     uState = stateStart;

	SetTimer(m_pCtx->m_uTime1);

	while( GetTimer() ) {

		char cData;

		UINT uSize = sizeof(cData);

		m_pCtx->m_pSock->Recv(PBYTE(&cData), uSize);

		if( uSize ) {

			SetTimer(m_pCtx->m_uTime1);

			if( uPtr > elements(sBuff) ) {

				break;
				}

			sBuff[uPtr ++] = cData;

			sBuff[uPtr]    = '\0';

			if( uState == stateStart ) {

				m_pCtx->m_sRecv[0] = '\0';

				if( !strcmp(sBuff, "!") || 
				    !strcmp(sBuff, "?") ){

					strcpy(m_pCtx->m_sRecv, sBuff);

					Trace("<<< <%s>\n", sBuff);

					uPtr   = 0;

					uState = stateCRLF;
					
					continue;
					}				

				if( uPtr ) {

					if( IsNum(sBuff[uPtr - 1]) ) {
						
						uState = stateLength;
						}
					else
						break;
					}
				
				continue;
				}
			
			if( uState == stateLength ) {

				if( strlen(sBuff) < 9 ) {
				
					if( !IsNum(sBuff[uPtr - 1]) ) {
											
						break;
						}

					continue;
					}

				Trace("<<< length <%s>\n", sBuff);				

				if( FALSE ) {

					// Development

					UINT uLength = ATOI(sBuff);

					PBYTE pWork = new BYTE [uLength];

					if( Read(pWork, uLength) ) {

						pImage = pWork;

						uPtr   = 0;
					
						uState = stateCRLF;

						continue;
						}

					delete pWork;

					return FALSE;
					}

				if( ReadBitmap(pImage) ) {

					uPtr   = 0;
					
					uState = stateCRLF;

					continue;
					}

				uPtr   = 0;

				continue;
				}			
			
			if( uState == stateCRLF ) {				

				if( !strcmp(sBuff, "\r\n") ) {

					Trace("<<< <CRLF>\n");

					return TRUE;
					}

				continue;
				}
			}		
		else {
			UINT Phase = 0;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				Sleep(10);
				}
			else
				break;
			}
		}

	return FALSE;
	}

BOOL CDualisDriver::ReadHeader(CBMPHeader &Header)
{
	if( Read(PBYTE(&Header), sizeof(Header)) ) {

		Header.Size   = IntelToHost(Header.Size);

		Header.Offset = IntelToHost(Header.Offset);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDualisDriver::ReadHeader(CDIBHeader &Header)
{
	if( Read(PBYTE(&Header), sizeof(Header)) ) {

		Header.HeadSize    = IntelToHost(Header.HeadSize);

		Header.Width       = IntelToHost(Header.Width);
		
		Header.Height      = IntelToHost(Header.Height);

		Header.Planes      = IntelToHost(Header.Planes);

		Header.Bits        = IntelToHost(Header.Bits);

		Header.Compression = IntelToHost(Header.Compression);

		Header.DataSize    = IntelToHost(Header.DataSize);

		Header.Colors      = IntelToHost(Header.Colors);

		Header.HorzRes     = IntelToHost(Header.HorzRes);

		Header.VertRes     = IntelToHost(Header.VertRes);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDualisDriver::ReadBitmap(PBYTE &pImage)
{
	CBMPHeader File;

	if( ReadHeader(File) ) {

		CDIBHeader Info;

		if( ReadHeader(Info) ) {

			UINT  uSkip = sizeof(File) + sizeof(Info);
			
			UINT uAlloc = File.Size - uSkip;

			PBYTE pData = new BYTE [ uAlloc ];

			if( Read(pData, uAlloc) ) {

				PBYTE pSrc = pData + File.Offset - uSkip;

				if( Info.Bits == 8 ) {
				
					UINT          uSize = Info.Width * Info.Height;

					PBYTE         pWork = new BYTE [ sizeof(BITMAP_RLC) + uSize ];
					
					BITMAP_RLC *pBitmap = (BITMAP_RLC *) pWork;

					pBitmap->Frame      = m_pCtx->m_uFrame ++;

					pBitmap->Format     = image8bitGreyScale;

					pBitmap->Width      = Info.Width;

					pBitmap->Height     = Info.Height;

					pBitmap->Stride     = 0;

					memcpy(pBitmap->Data, pSrc, uSize);

					delete pData;

					pImage = pWork;

					return TRUE;
					}

				Trace("image bits %d not supported\n", Info.Bits);				
				}

			delete pData;
			}
		}

	return FALSE;
	}

BOOL CDualisDriver::RecvFrame(UINT uProtocol)
{
	Trace("recvframe %d\n", uProtocol);

	if( uProtocol == 1 ) {

		return RecvFrame();
		}

	if( uProtocol == 2 ) {

		UINT uTicket;

		do {
			if( (uTicket = ReadTicket()) == 0 ) {

				PBYTE pImage = NULL;

				if( !RecvResult(pImage) ) {

					delete pImage;

					return FALSE;
					}

				delete pImage;
				}

			ForceSleep(20);

			} while( uTicket == 0 );

		return RecvFrame(1);
		}

	if( uProtocol == 3 ) {

		PCTXT pFmt = "1234L123456789\r\n";

		char sBuff[32] = { 0 };

		if( Read(PBYTE(sBuff), strlen(pFmt)) ) {
			
			return RecvFrame(2);
			}		
		}

	return FALSE;
	}

BOOL CDualisDriver::RecvFrame(void)
{
	UINT uPtr  = 0;

	SetTimer(m_pCtx->m_uTime1);

	while( GetTimer() ) {		

		UINT uSize = elements(m_pCtx->m_sRecv);

		m_pCtx->m_pSock->Recv(PBYTE(m_pCtx->m_sRecv), uSize);

		if( uSize ) {

			m_pCtx->m_sRecv[uSize] = '\0';

			Trace("<<< <%s>\n", m_pCtx->m_sRecv);

			if( CheckCRLF() ) {
				
				return TRUE;
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CDualisDriver::RecvResult(PBYTE &pImage, UINT uProtocol)
{
	Trace("recvresult %d\n", uProtocol);

	if( uProtocol == 1 ) {

		return RecvResult(pImage);
		}

	if( uProtocol == 2 ) {

		UINT uTicket;

		if( (uTicket = ReadTicket()) < NOTHING ) {

			if( uTicket == 0 ) {
			
				return RecvResult(pImage, 1);
				}

			if( uTicket == m_pCtx->m_uTicket ) {
			
				return RecvResult(pImage, 1);
				}
			}
		}

	if( uProtocol == 3 ) {

		PCTXT pFmt = "1234L123456789\r\n";

		char sBuff[32] = { 0 };

		if( Read(PBYTE(sBuff), strlen(pFmt)) ) {
		
			return RecvResult(pImage, 2);
			}
		}

	return FALSE;
	}

BOOL CDualisDriver::RecvResult(PBYTE &pImage)
{
	Trace("recvresult\n");

	char sBuff[128] = { 0 };

	UINT       uPtr = 0;

	UINT     uState = stateStart;

	UINT     uField = 0;

	UINT    uLength = 0;

	UINT    uFormat = formatBMP;

	SetTimer(m_pCtx->m_uTime1);

	while( GetTimer() ) {

		char cData;

		UINT uSize = sizeof(cData);

		m_pCtx->m_pSock->Recv(PBYTE(&cData), uSize);

		if( uSize ) {

			SetTimer(m_pCtx->m_uTime1);

			if( uPtr > elements(sBuff) ) {

				return FALSE;
				}

			sBuff[uPtr ++] = cData;

			sBuff[uPtr]   = '\0';

			if( uState == stateStart ) {

				m_pCtx->m_sRecv[0] = '\0';

				if( !strcmp(sBuff, "!") ) {

					Trace("<<< start <%s>\n", sBuff);

					strcpy(m_pCtx->m_sRecv, sBuff);
						
					uPtr   = 0;

					uState = stateCRLF;

					continue;
					}

				if( !strcmp(sBuff, m_pCtx->m_pStart) ) {

					Trace("<<< start <%s>\n", sBuff);

					uField = 0;
						
					uPtr   = 0;

					uState = stateResult;

					continue;
					}
					
				continue;
				}
				
			if( uState == stateResult ) {

				if( !strcmp(&sBuff[uPtr - strlen(m_pCtx->m_pStop)], m_pCtx->m_pStop) ) {

					Trace("<<< result <%s>\n", sBuff);

					uPtr   = 0;

					uState = stateCRLF;

					continue;
					}

				if( !strcmp(&sBuff[uPtr - strlen(m_pCtx->m_pSep)], m_pCtx->m_pSep) ) {

					sBuff[uPtr - strlen(m_pCtx->m_pSep)] = 0;//
						
					Trace("<<< result <%s> %d\n", sBuff, uField);

					strcat(m_pCtx->m_sRecv, sBuff);

					strcat(m_pCtx->m_sRecv, " ");

					if( ++ uField > 1 ) {

						uField = 0;
							
						uPtr   = 0;
						
						uState = stateOption;

						continue;
						}

					uPtr = 0;

					continue;
					}
					
				continue;
				}

			if( uState == stateDefault ) {

				if( !strcmp(sBuff, m_pCtx->m_pStop) ) {

					Trace("<<< default <%s>\n", sBuff);

					uPtr   = 0;

					uState = stateCRLF;

					continue;
					}
					
				if( !strcmp(sBuff, m_pCtx->m_pSep) ) {
					
					Trace("<<< default <%s>\n", sBuff);

					uField = 0;

					uPtr   = 0;

					uState = stateOption;

					continue;
					}

				continue;
				}

			if( uState == stateCRLF ) {

				if( !strcmp(sBuff, "\r\n") ) {

					Trace("<<< <CRLF> fmt %d %d\n", uFormat, formatBMP);
					
					return TRUE;
					}

				continue;
				}

			if( uState == stateOption ) {

				if( !strcmp(&sBuff[uPtr - strlen(m_pCtx->m_pStop)], m_pCtx->m_pStop) ) {

					Trace("<<< option <%s>\n", sBuff);

					uPtr   = 0;

					uState = stateCRLF;

					continue;
					}

				if( !strcmp(&sBuff[uPtr - strlen(m_pCtx->m_pSep)], m_pCtx->m_pSep) ) {

					sBuff[uPtr - strlen(m_pCtx->m_pSep)] = 0;//
					
					Trace("<<< option <%s>\n", sBuff);					

					if( strlen(sBuff) == 2 ) {

						uField ++;

						uPtr   = 0;

						uState = stateModel;

						continue;
						}

					if( strlen(sBuff) == 3 ) {

						uFormat = NOTHING;

						if( !strcmp(sBuff, "BMP") ) {

							uFormat = formatBMP;
							}

						if( !strcmp(sBuff, "RAW") ) {

							uFormat = formatRAW;
							}

						if( uFormat < NOTHING ) {

							uField += 2;
	
							uPtr   = 0;

							uState = stateImage;
							}
						}					

					uPtr = 0;						
					}					

				continue;
				}

			if( uState == stateImage ) {

				if( !strcmp(&sBuff[uPtr - strlen(m_pCtx->m_pSep)], m_pCtx->m_pSep) ) {					

					sBuff[uPtr - strlen(m_pCtx->m_pSep)] = 0;

					Trace("<<< image <%s> field %d\n", sBuff, uField);

					if( uField == 2 ) {
						
						uLength = ATOI(sBuff);
						}

					if( ++ uField > 2 ) {

						if( uFormat == formatBMP ) {

							if( ReadBitmap(pImage) ) {

								uPtr   = 0;
						
								uState = stateDefault;

								continue;
								}
							}

						else if( uFormat == formatRAW ) {

							PBYTE pTrash = New BYTE [ uLength ];

							if( Read(pTrash, uLength) ) {

								delete pTrash;

								uPtr   = 0;
						
								uState = stateDefault;

								continue;
								}

							delete pTrash;
							}
												
						return FALSE;
						}

					uPtr = 0;					
					}

				continue;
				}

			if( uState == stateModel ) {

				if( !strcmp(&sBuff[uPtr - strlen(m_pCtx->m_pStop)], m_pCtx->m_pStop) ) {

					sBuff[uPtr - strlen(m_pCtx->m_pStop)] = 0;

					Trace("<<< field %d <%s>\n", sBuff);

					uPtr   = 0;

					uState = stateDefault;						
						
					continue;
					}

				if( !strcmp(&sBuff[uPtr - strlen(m_pCtx->m_pSep)], m_pCtx->m_pSep) ) {						

					sBuff[uPtr - strlen(m_pCtx->m_pSep)] = 0;

					Trace("<<< model <%s>\n", sBuff);

					uPtr = 0;

					if( ++ uField > 4 ) {

						uField = 0;
							
						uState = stateOption;
						}

					continue;
					}

				continue;
				}
			}
		else {
			UINT Phase = 0;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				Sleep(10);
				}
			else
				break;
			}
		}

	return FALSE;
	}

UINT CDualisDriver::ReadTicket(void)
{
	PCTXT pFmt = "1234";

	char sBuff[32] = { 0 };

	if( Read(PBYTE(sBuff), strlen(pFmt)) ) {

		Trace("ticket [%s]\n", sBuff);

		return ATOI(sBuff);
		}

	Trace("read ticket - no ticket\n");

	return NOTHING;
	}

BOOL CDualisDriver::ReadCRLF(void)
{
	PCTXT    pCRLF = "\r\n";

	char sBuff[32] = { 0 };

	if( Read(PBYTE(sBuff), strlen(pCRLF)) ) {
					
		if( !strcmp(sBuff, pCRLF) ) {

			return TRUE;
			}		
		}

	return FALSE;
	}

BOOL CDualisDriver::Read(PBYTE pData, UINT uCount)
{
	UINT uTotal = 0;

	SetTimer(m_pCtx->m_uTime1);

	while( GetTimer() ) {

		if( uTotal == uCount ) {

			return TRUE;
			}

		UINT uAvail = uCount - uTotal;

		if( m_pCtx->m_pSock->Recv(pData + uTotal, uAvail) == S_OK ) {

			SetTimer(m_pCtx->m_uTime1);

			uTotal += uAvail;
			}
		else {
			UINT Phase = 0;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				Sleep(10);
				}
			else
				break;
			}
		}

	return FALSE;
	}

BOOL CDualisDriver::CheckCRLF(void)
{
	UINT uSize = strlen(m_pCtx->m_sRecv);

	if( uSize >= 2 ) {

		uSize -= 2;

		if( !strncmp(&m_pCtx->m_sRecv[uSize], "\r\n", strlen("\r\n")) ) {
					
			m_pCtx->m_sRecv[uSize] = '\0';

			return TRUE;
			}
		}

	return FALSE;
	}

void CDualisDriver::Trace(PCTXT pText, ...)
{
	if( false ) {

		va_list pArgs;

		va_start(pArgs, pText);

		AfxTrace("%s:", m_pName);

		AfxTrace(pText, pArgs);

		va_end(pArgs);
		}
	}

void CDualisDriver::HostAppend(PCTXT pText)
{
	strcat(m_pCtx->m_sSend, pText);
	}

void CDualisDriver::HostPrintf(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	VSPrintf(m_pCtx->m_sSend + strlen(m_pCtx->m_sSend), pText, pArgs);

	va_end(pArgs);
	}

// End of File
