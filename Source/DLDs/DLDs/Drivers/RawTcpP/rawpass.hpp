
//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Passive Driver
//

class CRawTCPPassiveDriver : public CRawPortDriver
{
	public:
		// Constructor
		CRawTCPPassiveDriver(void);

		// Destructor
		~CRawTCPPassiveDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Entry Points
		DEFMETH(void) Disconnect(void);
		DEFMETH(UINT) Read (UINT uTime);
		DEFMETH(UINT) Write(BYTE bData, UINT uTime);
		DEFMETH(UINT) Write(PCBYTE pData, UINT uCount, UINT uTime);

	protected:
		// Data Members
		UINT	 m_uPort;
		BOOL	 m_fKeep;
		ISocket *m_pSock;

		// Implementation
		BOOL CheckSocket(void);
	};

// End of File
