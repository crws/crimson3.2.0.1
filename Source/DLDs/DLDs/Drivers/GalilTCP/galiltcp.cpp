
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "galiltcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Galil TCP Driver : GalilTCP Driver
//

// Instantiator

INSTANTIATE(CGalilTCPDriver);

// Constants

// Constructor

CGalilTCPDriver::CGalilTCPDriver(void)
{
	m_Ident     = DRIVER_ID;
	}

// Destructor

CGalilTCPDriver::~CGalilTCPDriver(void)
{
	}

// Configuration

void MCALL CGalilTCPDriver::Load(LPCBYTE pData)
{ 
	}
	
// Management

void MCALL CGalilTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CGalilTCPDriver::Open(void)
{
	CYaskawaSMCTCPDriver::Open();
	}

// Device

CCODE MCALL CGalilTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	return CYaskawaSMCTCPDriver::DeviceOpen(pDevice);
	}

CCODE MCALL CGalilTCPDriver::DeviceClose(BOOL fPersist)
{
	CCODE c = CYaskawaSMCTCPDriver::DeviceClose(fPersist);

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CGalilTCPDriver::Ping(void)
{
	return CYaskawaSMCTCPDriver::Ping();
	}

CCODE MCALL CGalilTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return CYaskawaSMCTCPDriver::Read(Addr, pData, uCount);
	}

CCODE MCALL CGalilTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return CYaskawaSMCTCPDriver::Write(Addr, pData, uCount);
	}

UINT MCALL CGalilTCPDriver::DevCtrl(void * pContext, UINT uFunc, PCTXT Value)
{
	return CYaskawaSMCTCPDriver::DevCtrl(pContext, uFunc, Value);
	}

// End of File
