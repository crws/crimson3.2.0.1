
//////////////////////////////////////////////////////////////////////////
//
// Galil TCP Driver : YaskawaSMCTCP Driver
//

#include "yunitcpm.hpp"

#define GALILTCP_ID 0x3520

class CGalilTCPDriver : public CYaskawaSMCTCPDriver
{
	public:
		// Constructor
		CGalilTCPDriver(void);

		// Destructor
		~CGalilTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(UINT)  DevCtrl(void * pContext, UINT uFunc, PCTXT Value);

	protected:
	};

// End of File
