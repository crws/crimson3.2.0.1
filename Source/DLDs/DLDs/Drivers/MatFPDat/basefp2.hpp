
//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP via MEWTOCOL Master Base
//

// Data Spaces

#define SPACE_DT	1
#define SPACE_SDT	2
#define SPACE_LD	3
#define SPACE_FL	4
#define SPACE_IX	5
#define SPACE_IY	6
#define SPACE_SV	7
#define SPACE_EV	8
#define SPACE_C		9
#define SPACE_T		10
#define SPACE_R		11
#define SPACE_SR	12
#define SPACE_LR	13
#define SPACE_X		14
#define SPACE_Y		15
#define SPACE_WR	16	
#define SPACE_WL	18
#define SPACE_WX	19
#define SPACE_WY	20

class CMatFP2BaseDriver : public CMasterDriver {

	public:
		// Constructor
		CMatFP2BaseDriver(void);

		// Destructor
		~CMatFP2BaseDriver(void);

		// Entry Points

		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			};

	protected:

		CContext *	m_pCtx;

		// Data Members
		LPCTXT	m_pHex;

		BYTE	m_bTxBuff[300];
		BYTE	m_bRxBuff[300];
		UINT	m_uPtr;
		BYTE	m_bCheck;

		// Read Handlers
		virtual CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		        CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		virtual CCODE DoBitRead (AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		virtual CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		        CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		virtual CCODE DoBitWrite (AREF Addr, PDWORD pData, UINT uCount);
		
		// Implementation
			void AddByte(BYTE bData);
			void AddCheck(void);
			void AddAddress(UINT uSpace, UINT uOffset);
		virtual void AddAddressRange(UINT uSpace, UINT uOffset, UINT uCount);
			void GetData(UINT uType, UINT uCount, PDWORD pData);
			void PutData(UINT uType, UINT uCount, PDWORD pData);
			void AddGeneric(DWORD dData, UINT uRadix, UINT uFactor);
			      
		// Transport
		virtual BOOL SendFrame(void);
		virtual BOOL GetFrame(void);
			BOOL CheckReply(UINT uCount);
		virtual	void StartFrame(void);
		virtual	BOOL Transact(void);

		// Helpers
		DWORD xtoin(PCTXT pText, UINT uCount);
		WORD  SwapBytes(WORD wData);
		DWORD SwapLong(DWORD dData);
		BOOL  IsLong(UINT uType);
		BOOL  IsBit(UINT uType);
		UINT  GetByteCount(UINT uType);
		BOOL  IsRelay(UINT uSpace);
		BOOL  IsData(UINT uSpace);
		BOOL  IsIndex(UINT uSpace);
		BOOL  CheckSpace(UINT uSpace);
		BOOL  IsWriteable(UINT uSpace);

	};

// End of File
