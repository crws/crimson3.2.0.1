
#include "intern.hpp"

#include "mitatcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A TCP Master Driver
//

// Instantiator

//INSTANTIATE(CMitAQTCPMaster);

// Constructor

CMitAQTCPMaster::CMitAQTCPMaster(void)
{
	m_Ident = 0x3507;
	}

// Destructor

CMitAQTCPMaster::~CMitAQTCPMaster(void)
{
	}

// Implementation

BOOL CMitAQTCPMaster::RecvFrame(UINT uTotal)
{      
	UINT uPtr    = 0;

	UINT uSize   = 0;

	UINT uOffset = 0;
	
	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= uTotal ) {

				return TRUE;
				}
			
			for( UINT u = uPtr - uSize; u < uPtr - uOffset; u++ ) {

				m_bRxBuff[u] = m_bRxBuff[u + uOffset];
				}

			uOffset = 2;

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;

  
	}

BOOL CMitAQTCPMaster::CheckFrame(void)
{
	if( (m_bRxBuff[0] & 0x80) && (m_bRxBuff[1] == 0x00) ) {	

		return TRUE;
		}

	return FALSE;

	}

void CMitAQTCPMaster::AddCommand(UINT uType, UINT uCommand, UINT uCount)
{
	AddByte(uCommand);			

	AddByte(m_pCtx->m_bNet);			

	AddWord(m_pCtx->m_uMonitor);
	
	}

void CMitAQTCPMaster::AddAddress(AREF Addr)
{
	AddOffset(Addr.a.m_Offset);
	
	AddPrefix(Addr.a.m_Table);

	}

void CMitAQTCPMaster::AddOffset(UINT uOffset)
{
	AddWord(LOWORD(uOffset));

	AddWord(0x0000);
	}

WORD CMitAQTCPMaster::GetCommand(UINT uType, BOOL fWrite)
{
	if( fWrite ) {

		return uType == addrBitAsBit ? BIT_WRITE : WORD_WRITE;
		}

	return uType == addrBitAsBit ? BIT_READ : WORD_READ;
	}

// End of File
