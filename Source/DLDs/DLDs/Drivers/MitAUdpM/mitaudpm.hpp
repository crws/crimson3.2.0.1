#include "mitatcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A Series PLC UDP Master Driver
//

class CMitAQUDPMaster : public CMitAQTCPMaster 
{
	public:
		// Constructor
		CMitAQUDPMaster(void);

		// Destructor
		~CMitAQUDPMaster(void);

	protected:

		// Socket Management
		BOOL OpenSocket(void); 		
       	};
   
// End of File
