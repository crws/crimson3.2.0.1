
//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP Master Driver
//

class CProfibusDPMaster : public CMasterDriver
{
	public:
		// Constructor
		CProfibusDPMaster(void);

		// Destructor
		~CProfibusDPMaster(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Entry Points
		DEFMETH(void)  Service(void);
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Data Members
		IProfibusPort * m_pPort;
		BYTE            m_bStation;
		PBYTE           m_pData;
		UINT            m_uSize;

		// Implementation
		void AllocData(UINT uSize);
		void FreeData(void);
		BOOL CheckLimit(UINT uOffset, UINT &uCount, UINT uType, BOOL fInput);
		UINT SizeOf(UINT uType);
	};

// End of File
