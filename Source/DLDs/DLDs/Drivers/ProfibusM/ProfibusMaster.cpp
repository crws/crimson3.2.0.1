
#include "intern.hpp"

#include "ProfibusMaster.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP Master Driver
//

// Instantiator

INSTANTIATE(CProfibusDPMaster);

// Constructor

CProfibusDPMaster::CProfibusDPMaster(void)
{
	m_Ident = DRIVER_ID;
	
	m_pPort = NULL;

	m_uSize = 0;

	m_pData = NULL;
	}

// Destructor

CProfibusDPMaster::~CProfibusDPMaster(void)
{
	}

// Configuration

void MCALL CProfibusDPMaster::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bStation = GetByte(pData);
		}
	}

void MCALL CProfibusDPMaster::CheckConfig(CSerialConfig &Config)
{
	Config.m_bDrop = m_bStation;
	}

// Management

void MCALL CProfibusDPMaster::Attach(IPortObject *pPort)
{
	m_pPort = (IProfibusPort *) pPort;
	}

void MCALL CProfibusDPMaster::Detach(void)
{
	m_pPort->Close();

	FreeData();
	}

void MCALL CProfibusDPMaster::Open(void)
{
	}

// Entry Points

void MCALL CProfibusDPMaster::Service(void)
{
	//m_pPort->Poll();
	}

CCODE CProfibusDPMaster::Ping(void)
{
	if( m_pPort->IsOnline() ) {

		UINT uSize = m_pPort->GetInputSize();

		if( m_uSize != uSize ) {
			
			FreeData();

			AllocData(uSize);
			
			memset(m_pData, 0, uSize);
			}

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE CProfibusDPMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pPort->IsOnline() ) {

		return CCODE_ERROR;
		}

	UINT  uBlock = Addr.a.m_Table;

	UINT   uType = Addr.a.m_Type;

	UINT uOffset = Addr.a.m_Offset;

	if( uBlock == 1 ) {

		return uCount;
		}

	if( uBlock == 2 ) {

		UINT  uSize = uCount;
		
		PBYTE pWork = m_pPort->GetOutputBuffer();

		if( pWork ) {

			if( CheckLimit(uOffset, uSize, uType, FALSE) ) {

				if( uType == addrByteAsByte ) {

					PBYTE  p = PBYTE(pWork) + uOffset;

					CMotorDataPacker(pData, uSize, FALSE).Unpack(p);
					}

				if( uType == addrWordAsWord ) {

					PWORD  p = PWORD(pWork) + uOffset;

					CMotorDataPacker(pData, uSize, FALSE).Unpack(p);
					}

				if( uType == addrLongAsLong ) {

					PDWORD p = PDWORD(pWork) + uOffset;

					CMotorDataPacker(pData, uSize, FALSE).Unpack(p);
					}

				if( uType == addrRealAsReal ) {

					PDWORD p = PDWORD(pWork) + uOffset;

					CMotorDataPacker(pData, uSize, FALSE).Unpack(p);
					}
				}

			return uCount;
			}
		else { 
			return CCODE_ERROR | CCODE_BUSY;
			}
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CProfibusDPMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pPort->IsOnline() ) {

		return CCODE_ERROR;
		}

	UINT  uBlock = Addr.a.m_Table;

	UINT   uType = Addr.a.m_Type;

	UINT uOffset = Addr.a.m_Offset;

	if( uBlock == 1 ) {

		UINT uSize = uCount;
		
		if( CheckLimit(uOffset, uSize, uType, TRUE) ) {
		
			if( uType == addrByteAsByte ) {

				PBYTE  p = PBYTE(m_pData) + uOffset;

				CMotorDataPacker(pData, uSize, FALSE).Pack(p);
				}

			if( uType == addrWordAsWord ) {

				PWORD  p = PWORD(m_pData) + uOffset;

				CMotorDataPacker(pData, uSize, FALSE).Pack(p);
				}

			if( uType == addrLongAsLong ) {

				PDWORD p = PDWORD(m_pData) + uOffset;

				CMotorDataPacker(pData, uSize, FALSE).Pack(p);
				}

			if( uType == addrRealAsReal ) {

				PDWORD p = PDWORD(m_pData) + uOffset;

				CMotorDataPacker(pData, uSize, FALSE).Pack(p);
				}

			if( m_pPort->PutInputData(m_pData, m_uSize) ) {

				return uCount;
				}

			return CCODE_ERROR | CCODE_NO_RETRY;
			}

		return uCount;
		}

	if( uBlock == 2 ) {

		return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

void CProfibusDPMaster::AllocData(UINT uSize)
{
	m_pData = New BYTE[ uSize ];

	m_uSize = uSize;
	}

void CProfibusDPMaster::FreeData(void)
{
	if( m_pData ) {

		delete [] m_pData;

		m_pData = NULL;

		m_uSize = 0;
		}
	}

BOOL CProfibusDPMaster::CheckLimit(UINT uOffset, UINT &uCount, UINT uType, BOOL fInput)
{
	UINT  uSize = fInput ? m_pPort->GetInputSize() : m_pPort->GetOutputSize();

	UINT  uBase = uOffset * SizeOf(uType);

	UINT  uSpan =  uCount * SizeOf(uType);
	
	if( uBase < uSize ) {

		uSize -= uBase;

		MakeMin(uSpan, uSize);

		uCount = uSpan / SizeOf(uType);

		return uCount ? TRUE : FALSE;
		}

	return FALSE;	
	}

UINT CProfibusDPMaster::SizeOf(UINT uType)
{
	switch( uType ) {
		
		case addrWordAsWord:	return sizeof(WORD);
		case addrLongAsLong:	
		case addrRealAsReal:	return sizeof(DWORD);
		}

	return sizeof(BYTE);
	}

// End of File
