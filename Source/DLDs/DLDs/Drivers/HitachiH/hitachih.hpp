

//////////////////////////////////////////////////////////////////////////
//
// Hitachi H Series
//

// Task Codes
#define OCC	0x16
#define OCCFRC	0x17
#define RIO	0xA0
#define WIO	0xA2
#define OCCREL	0
#define OCCRD	1
#define OCCWR	2

// Response Codes
#define	RNORMAL	0 // Successful
#define	RTSKERR	1 // Task Code Error
#define	RWARN	2 // Warning
#define	RNOEXEC	3 // Unexecutable
#define	RBUSY	4 // Busy
#define	RNETERR	5 // Network Error

// Data Spaces

#define	SPWL0	 1
#define	SPWL1	 2
#define	SPWX	 3
#define	SPWY	 4
#define	SPWR	 5
#define	SPWM	 6
#define	SPTCP	 7
#define	SPTCA	 8
#define	SPL0	 9
#define	SPL1	10
#define	SPL2	11
#define	SPL3	12
#define	SPX0	13
#define	SPX1	14
#define	SPX2	15
#define	SPX3	16
#define	SPX4	17
#define	SPY0	18
#define	SPY1	19
#define	SPY2	20
#define	SPY3	21
#define	SPY4	22
#define	SPM	23
#define	SPR0	24
#define	SPR1	25
#define	SPR2	26
#define	SPR3	27
#define	SPR4	28
#define	SPR5	29
#define	SPR6	30
#define	SPR7	31
#define	SPR8	32
#define	SPR9	33
#define	SPRA	34
#define	SPRB	35
#define	SPRC	36
#define	SPRD	37
#define	SPRE	38
#define	SPRF	39
#define	SPCL	40
#define	SPERR	41
#define	SPDR	42
#define	SPADJ	43

// Memory Codes
#define MX	0x0
#define MY	0x1
#define MR	0x2
#define ML	0x3
#define MM	0x4
#define MTP	0x5
#define MCL	0x6
#define MWX	0x8
#define MWY	0x9
#define MWR	0xA
#define MWL	0xB
#define MWM	0xC
#define MTA	0xD

// Bit caching
#define	BCACHESZ	129
#define	BCACHECT	  8

// Word caching
#define	WCACHESZ	33
#define	WCACHECT	 8

// Caching Control
#define	CUSETHIS  0
#define	CUSEOLD   1
#define	CMAKENEW  2

class CHitachiHDriver : public CMasterDriver {

	public:
		// Constructor
		CHitachiHDriver(void);

		// Destructor
		~CHitachiHDriver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
			
		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bLoop;
			BYTE	m_bUnit;
			BYTE	m_fCheck;
			BYTE	m_fMulti;
			BYTE	m_bDrop;
			BYTE	m_bRspDelay;
			UINT	m_uErrPos;
			UINT	m_uReqDelay;

			// Hack for Australia
			BOOL	m_fAllowCache;

			BOOL	m_Bits0[BCACHESZ];
			BOOL	m_Bits1[BCACHESZ];
			BOOL	m_Bits2[BCACHESZ];
			BOOL	m_Bits3[BCACHESZ];
			BOOL	m_Bits4[BCACHESZ];
			BOOL	m_Bits5[BCACHESZ];
			BOOL	m_Bits6[BCACHESZ];
			BOOL	m_Bits7[BCACHESZ];
			UINT	m_uBitAddr[BCACHECT];
			BYTE	m_bBitType[BCACHECT];
			BOOL	m_fDoCacheRead;
			UINT	m_uNextBCache;

			WORD	m_WR0[WCACHESZ];
			WORD	m_WR1[WCACHESZ];
			WORD	m_WR2[WCACHESZ];
			WORD	m_WR3[WCACHESZ];
			WORD	m_WR4[WCACHESZ];
			WORD	m_WR5[WCACHESZ];
			WORD	m_WR6[WCACHESZ];
			WORD	m_WR7[WCACHESZ];
			UINT	m_uWDAddr[WCACHECT];
			BYTE	m_bWDType[WCACHECT];
			UINT	m_uNextWCache;
			};

		CContext	* m_pCtx;

		// Data Members
		LPCTXT	m_pHex;

		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		UINT	m_uPtr;
		WORD	m_wCount;
		DWORD	m_dError;

		BOOL	m_fInPing;

		UINT	m_uCTxx;

		// Execute Read
		CCODE	MCALL Read(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	PutRead(AREF Addr,WORD wCount);

		// Execute Write
		CCODE	MCALL Write(AREF Addr, PDWORD pData, UINT uCount);
		void	PutWrite(AREF Addr, UINT uCount, PDWORD pData);

		// Command Exchange
		BOOL	CommandSequence(void);
		BYTE	SetENQ1(void);

		// Frame Building
		void	StartFrame(void);
		void	SetDrop(void);
		void	PutLUMP(void);
		void	PutMemoryCode(WORD wTable);
		void	PutCodeAndAddr(AREF Addr);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dData);

		// Transact
		BOOL	Transact(void);
		void	SendFrame(void);
		BOOL	GetFrame(void);
		BOOL	ValidateChecksum(void);
		BOOL	CheckResponseError(void);

		// Response Data Handling
		BYTE	FormByte(UINT uPos);
		WORD	FormWord(UINT uPos);
		DWORD	FormLong(UINT uPos);
		BYTE	MakeByte(PCBYTE pText);

		// Port Access
		void	TxByte(BYTE bData);
		UINT	RxByte(void);

		// Helpers
		BOOL	NoReadTransmit(AREF Addr, PDWORD pData, UINT *pCount);
		BOOL	NoWriteTransmit(AREF Addr, DWORD  dData);
		void	ResetCaches(void);

		// Bit Caching
		BOOL	TestForDonahoeTrigger(UINT uOffset, UINT uCount);
		UINT	SelectBCache(AREF Addr, UINT *pFunc);
		BOOL	CheckBRange(BYTE bTable, BYTE bType, UINT uV1, UINT uV2);
		BOOL *	SelectBitBuffer(UINT uSel);
		void	UpdateBitInfo(UINT uSel, UINT uOff, BYTE bTab);
		BOOL	StockBits(AREF Addr, UINT uSel);
		void	UpdateBitWrite(AREF Addr, BOOL fData);
		// Word Caching
		UINT	SelectWCache(AREF Addr, UINT *pFunc);
		BOOL	CheckWRange(UINT uSel, UINT uV1, UINT uV2);
		PWORD	SelectWDBuffer(UINT uSel);
		void	UpdateWDInfo(UINT uSel, UINT uOff);
		BOOL	StockWords(AREF Addr, UINT uSel);
		BOOL	CheckInRange(AREF Addr);
		void	UpdateWDWrite(AREF Addr, DWORD dData);
	};

// End of File
