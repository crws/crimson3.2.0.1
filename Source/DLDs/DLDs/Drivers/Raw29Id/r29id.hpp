
#ifndef INCLUDE_R29ID_HPP

#define INCLUDE_R29ID_HPP

//////////////////////////////////////////////////////////////////////////
//
// Raw 29-bit ID Entry Driver
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#include "..\j1939\j1939.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw Driver Context Structures
//

struct ID29 : PGN
{
	DWORD m_Ref;
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw 29-bit Identifier Entry Handler
//

class CCAN29bitIdEntryRawHandler : public CCANJ1939Handler
{	
	public:
		// Constructor
		CCAN29bitIdEntryRawHandler(IHelper *pHelper);

		// IPortHandler
		void METHOD OnOpen(CSerialConfig const &Config);

		// FRAME_EXT Access
		BOOL SendPDU(PDU * pPDU, BOOL fRetry = FALSE, UINT uTrans = transHandler);

		// Data Access
		PDU* FindNextPDU(ID * id, PDU * pLast);
		UINT GetMask(void);
		void SetSA(BYTE bSA);

	protected:

		// Implementation
		BOOL DoListen(void);
		BOOL HandleRequest(void);
		BOOL HandleSpecificRequest(PDU * pPDU, BOOL fRTR);
		BOOL HandleRequest(PDU * pPDU);
		BOOL HandleClaimAddressReq(PDU * pPDU);
		BOOL HandleClaimAddress(void);
		BOOL HandleTransport(void);
		BOOL HandleRapidFire(PDU * pPDU);
		void DoNak(void);
		void FlopBytes(PDU * pPDU, UINT uCount);

		// Overridables
		virtual	BOOL IsThisNode(void);

		// Helpers
		BOOL IsDestinationSpecific(ID id);
		BOOL IsBroadcast(ID id);
		BOOL IsDM(PDU * pPDU);
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw 29-bit ID Entry Driver
//

class CCAN29bitIdEntryRawDriver : public CCANJ1939Driver
{
	public:
		// Constructor
		CCAN29bitIdEntryRawDriver(void);

		// Destructor
		~CCAN29bitIdEntryRawDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);
					
	protected:

		// Device Data
		struct CIdent29 : CCANJ1939Driver::CContext
		{
			ID29 * m_pIDs;
			};

		CIdent29 * m_pIdent29;

		// Implementation
		BOOL   FindID(ID &id, UINT uID, UINT uPriority);
		BOOL   FindID(ID &id, AREF Addr);
		PGN  * FindPG(PDU * pPDU);
		ID29 * FindPG(ID id, AREF Addr, PDU * &pPDU);
		BOOL   IsDM(PGN * pPGN);
		BOOL   IsRef(ID29 * pID, AREF Addr);

		// Helpers
		BOOL	SkipUpdate(LPCBYTE &pData);		
	};

// End of file

#endif