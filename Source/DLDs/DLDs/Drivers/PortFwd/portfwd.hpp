
//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

//typedef IPADDR const & IPREF;

//////////////////////////////////////////////////////////////////////////
//
// Port Forwarding Driver
//

class CPortForwardDriver : public CCommsDriver
{
	public:
		// Constructor
		CPortForwardDriver(void);

		// Destructor
		~CPortForwardDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Entry Points
		DEFMETH(void) Service(void);

	protected:
		// Data Members
		UINT      m_Protocol;
		DWORD     m_TargetIP;
		WORD      m_TargetPort;
		WORD      m_ExposePort;
		ISocket * m_pTarget;
		ISocket * m_pExpose;

		// Implementation
		void NewSession(void);
		BOOL WaitConnect(void);
		void ForwardData(void);
	};

// End of File
