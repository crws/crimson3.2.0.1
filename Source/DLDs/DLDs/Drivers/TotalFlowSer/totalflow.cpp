
#include "intern.hpp"

#include "totalflow.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Serial Driver
//

// Instantiator

INSTANTIATE(CTotalFlowSerialMasterDriver);

// Constructor

CTotalFlowSerialMasterDriver::CTotalFlowSerialMasterDriver(void)
{
	m_Ident = DRIVER_ID;

	m_pCtx  = NULL;
	}

// Destructor

CTotalFlowSerialMasterDriver::~CTotalFlowSerialMasterDriver(void)
{
	}

// Configuration

void MCALL CTotalFlowSerialMasterDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CTotalFlowSerialMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CTotalFlowSerialMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CTotalFlowSerialMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CTotalFlowSerialMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CCtx;

			strcpy(m_pCtx->m_sName, GetString(pData));

			strcpy(m_pCtx->m_sCode, GetString(pData));

			m_pCtx->m_bApp = GetByte(pData);

			m_pCtx->m_uEstab = GetWord(pData);

			m_pCtx->m_uTime2 = GetWord(pData);

			CTotalFlowMasterDriver::m_pCtx = m_pCtx;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	CTotalFlowMasterDriver::m_pCtx = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CTotalFlowSerialMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CTotalFlowSerialMasterDriver::Ping(void)
{
	AbortLink();

	return CCODE_SUCCESS;
	}

// Transport

BOOL CTotalFlowSerialMasterDriver::CheckLink(void)
{
	return TRUE;
	}

void CTotalFlowSerialMasterDriver::AbortLink(void)
{
	while( Recv(500) < NOTHING );
	}

BOOL CTotalFlowSerialMasterDriver::Send(CBuffer *pBuff)
{
	m_pData->ClearRx();

	m_pData->Write(pBuff->GetData(), pBuff->GetSize(), NOTHING);

	m_pData->Write(NULL, 0, NOTHING);

	pBuff->Release();

	return TRUE;
	}

UINT CTotalFlowSerialMasterDriver::Recv(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
