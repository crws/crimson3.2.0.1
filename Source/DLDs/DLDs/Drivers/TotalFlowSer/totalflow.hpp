
//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "totalbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Serial Driver
//

class CTotalFlowSerialMasterDriver : public CTotalFlowMasterDriver
{
	public:
		// Constructor
		CTotalFlowSerialMasterDriver(void);

		// Destructor
		~CTotalFlowSerialMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:
		// Device Config
		struct CCtx : public CTotalFlowMasterDriver::CCtx
		{
			};

		// Current Device
		CCtx * m_pCtx;

		// Transport
		BOOL CheckLink(void);
		void AbortLink(void);
		BOOL Send(CBuffer *pBuff);
		UINT Recv(UINT uTime);
	};

// End of File
