#include "paloop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop TCP/IP Driver
//

class CPAMasterTCPDriver : public CAcroloopDriver
{
	public:
		// Constructor
		CPAMasterTCPDriver(void);

		// Destructor
		~CPAMasterTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		
	protected:
		// Device Context
		struct CContext : CAcroloopDriver::CBaseCtx
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL	 m_fInit;
			ISocket *m_pWatch;
			BYTE     m_bTimeWD;
			BYTE	 m_bTickWD;
			};

		// Data Members
		CContext * m_pCtx;
		UINT	   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Transport Layer
		BOOL Send(void);
		BOOL RecvFrame(UINT uBytes);
		BOOL Transact(WORD CheckBytes);

		// Implementation
		BOOL IsAwake(AREF Addr);
		void AddLong(DWORD dwData, PBYTE pBuff);
		BOOL SetWatchdog(void);
		void KillWatchdog(void);
		BOOL BinaryTx(void);
		UINT GetSystemPointer(void);
		UINT GetPeek(UINT uIndex);
		void FixupDataIn(DWORD &dwData);
		void FixupDataOut(DWORD &dwData);
	};

// End of File
