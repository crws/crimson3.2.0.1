
#include "intern.hpp"

#include "koyod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Koyo DirectNet Driver
//

// Instantiator

INSTANTIATE(CKoyoDirectNetDriver);

// Constructor

CKoyoDirectNetDriver::CKoyoDirectNetDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;

	m_uPtr = 0;
	}

// Destructor

CKoyoDirectNetDriver::~CKoyoDirectNetDriver(void)
{
	}

// Configuration

void MCALL CKoyoDirectNetDriver::Load(LPCBYTE pData)
{ 
	if( GetWord(pData) == 0x1234 ) {

		m_MasterID = GetByte(pData);
		}
	}
	
void MCALL CKoyoDirectNetDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CKoyoDirectNetDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CKoyoDirectNetDriver::Open(void)
{
	}

// Device

CCODE MCALL CKoyoDirectNetDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_PingEnable = GetByte(pData);

			m_pCtx->m_PingReg = GetWord(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKoyoDirectNetDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CKoyoDirectNetDriver::Ping(void)
{
	if( m_pCtx->m_PingEnable ) {
	
		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = 1;
		Addr.a.m_Offset = m_pCtx->m_PingReg;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;

		return Read(Addr, Data, 1);
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKoyoDirectNetDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType = 0;

	UINT uAddr = 0;

	UINT uSize = 0;

	switch( Addr.a.m_Table ) {
	
		case 1:		uAddr = 0x0001; bType = '1';	break;	/* Data Registers	*/
		case 2:		uAddr = 0x0001; bType = '1';	break;	/* Current Time		*/
		case 3:		uAddr = 0x0201; bType = '1';	break;	/* Current Count	*/
		case 4:		uAddr = 0x0101; bType = '2';	break;	/* I - X		*/
		case 5:		uAddr = 0x0181; bType = '2';	break;	/* I - GX		*/
		case 6:		uAddr = 0x01AD; bType = '2';	break;	/* I - Special Relay	*/
		case 7:		uAddr = 0x0101; bType = '3';	break;	/* O - Y		*/
		case 8:		uAddr = 0x0181; bType = '3';	break;	/* O - C		*/
		case 9:		uAddr = 0x0281; bType = '3';	break;	/* O - Stage Bits	*/
		case 10:	uAddr = 0x0301; bType = '3';	break;	/* O - TMR Status Bits	*/
		case 11:	uAddr = 0x0321; bType = '3';	break;	/* O - CTR Status Bits	*/
	    	
		default:	return CCODE_ERROR | CCODE_HARD;

		}

	BOOL fLong = IsLong(Addr.a.m_Type);

	if( bType == '1' ) {

		uAddr += Addr.a.m_Offset;

		MakeMin(uCount, FindCount(Addr.a.m_Type));

		uSize  = uCount * (fLong ? 4 : 2);
		}
	else {
		uAddr += Addr.a.m_Offset / 8;

		MakeMin(uCount, 128);

		uSize  = uCount;
		}

	if( InitSequence(m_pCtx->m_bDrop) ) {

		SendHeader(m_pCtx->m_bDrop, 0x30, uAddr, uSize, bType);
	
		if( GetAck() ) {

			if( GetFrame(uSize) ) {

				SendCtrl(ACK);
					
				if( GetEOT() ) {
					
					SendCtrl(EOT);

					if( fLong ) {

						return GetLong(pData, uCount);
						}
					
					UINT uPos = 0;

					BYTE b1   = 0;
					
					BYTE b2   = 0;

					for( UINT i = 0; i < uCount; i++ ) {

						b1 = m_bRx[uPos++];

						if( bType == '1' ) {

							b2 = m_bRx[uPos++];
							}

						*pData++ = MAKEWORD(b1, b2);
						}

					return uCount;
					}
				}
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CKoyoDirectNetDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType = 0;

	UINT uAddr = 0;

	UINT uSize = 0;

	switch( Addr.a.m_Table ) {
	
		case 1:		uAddr = 0x0001; bType = '1';	break;	/* Data Registers	*/
		case 2:		uAddr = 0x0001; bType = '1';	break;	/* Current Time		*/
		case 3:		uAddr = 0x0201; bType = '1';	break;	/* Current Count	*/
		case 7:		uAddr = 0x0101; bType = '3';	break;	/* O - Y		*/
		case 8:		uAddr = 0x0181; bType = '3';	break;	/* O - C		*/
		case 9:		uAddr = 0x0281; bType = '3';	break;	/* O - Stage Bits	*/
		case 10:	uAddr = 0x0301; bType = '3';	break;	/* O - TMR Status Bits	*/
		case 11:	uAddr = 0x0321; bType = '3';	break;	/* O - CTR Status Bits	*/

	     	default:	return uCount;

		}

	BOOL fLong = IsLong(Addr.a.m_Type);

	if( bType == '1' ) {

		uAddr += Addr.a.m_Offset;

		uCount = min(uCount, FindCount(Addr.a.m_Type));

		uSize  = uCount * (fLong ? 4 : 2);
		}
	else {
		uAddr += Addr.a.m_Offset / 8;

		uCount = min(uCount, 128);

		uSize  = uCount;
		}

	if( InitSequence(m_pCtx->m_bDrop) ) {

		SendHeader(m_pCtx->m_bDrop, 0x38, uAddr, uSize, bType);
	
		if( GetAck() ) {
		
			fLong ? PutLong(pData, uCount, bType) : PutFrame(pData, uCount, bType);

			if( GetAck() ) {

				SendCtrl(EOT);

				return uCount;
				}
			}
		}

	return CCODE_ERROR;
	}

// Frame Building

void CKoyoDirectNetDriver::SendCtrl(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

void CKoyoDirectNetDriver::SendByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;

	m_bCheck       ^= bData;
	}

void CKoyoDirectNetDriver::SendWord(WORD wData)
{
	SendByte(LOBYTE(wData));

	SendByte(HIBYTE(wData));
	}

void CKoyoDirectNetDriver::SendLong(DWORD dwData)
{
	SendWord(LOWORD(dwData));

	SendWord(HIWORD(dwData));
	}

void CKoyoDirectNetDriver::SendData(BYTE bData)
{
	SendByte(m_pHex[bData / 16]);

	SendByte(m_pHex[bData % 16]);
	}

void CKoyoDirectNetDriver::SendPacket(void)
{
	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	m_uPtr = 0;
	}

// Transport Layer

BOOL CKoyoDirectNetDriver::InitSequence(BYTE bDrop)
{
	m_pData->ClearRx();

	SendByte('N');
	
	SendByte(0x20 + bDrop);
	
	SendByte(ENQ);

	SendPacket();

	UINT uState = 0;

	UINT uTimer = 0;

	UINT uData  = 0;
	
	SetTimer(TIMEOUT);
	
	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uData == 'N' ) {

					uState = 1;
					}
				break;
				
			case 1:
				if( uData == UINT(0x20 + bDrop) ) {

					uState = 2;

					break;
					}

				return FALSE;
				
			case 2:
				if( uData == ACK ) {

					return TRUE;
					}

				return FALSE;
			}
		}

	SendByte(EOT);
		
	return FALSE;
	}

void CKoyoDirectNetDriver::SendHeader(BYTE bDrop, BYTE bCode, UINT uAddr, UINT uCount, BYTE bType)
{
	SendByte(SOH);

	m_bCheck = 0;
	
	SendData(bDrop);

	SendByte(bCode);

	SendByte(bType);
	
	SendData(HIBYTE(uAddr));

	SendData(LOBYTE(uAddr));
	
	SendData(0);

	SendData(uCount);

	SendData(m_MasterID);				// Master ID ( 0 or 1 )
	
	BYTE bResult = m_bCheck;
	
	SendByte(ETB);
	
	SendByte(bResult);

	SendPacket();
	}

BOOL CKoyoDirectNetDriver::GetAck(void)
{
	UINT uTimer = 0;

	UINT uData  = 0;

	SetTimer(TIMEOUT);
	
	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( uData == ACK ) {

			return TRUE;
			}
		}

	SendByte(EOT);
		
	return FALSE;
	}

BOOL CKoyoDirectNetDriver::GetEOT(void)
{
	UINT uTimer = 0;

	UINT uData  = 0;

	SetTimer(TIMEOUT);
	
	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( uData == EOT ) {

			return TRUE;
			}
		}

	SendCtrl(EOT);
		
	return FALSE;
	}

BOOL CKoyoDirectNetDriver::GetFrame(UINT uCount)
{
	UINT uState = 0;
	
	UINT uPtr   = 0;

	BYTE bCheck = 0;

	UINT uData  = 0;

	UINT uTimer = 0;

	SetTimer(TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uData == STX ) {

					bCheck = 0;

					uState = 1;
					}
				break;
				
			case 1:
				if( uPtr == uCount ) {

					if( uData == ETX ) {

						uState = 2;
						}
					else
						return FALSE;

					break;
					}

				m_bRx[uPtr++] = uData;

				bCheck       ^= uData;

				break;
				
			case 2:
				return uData == bCheck;
			}
		}

	SendCtrl(EOT);
		
	return FALSE;
	}

void CKoyoDirectNetDriver::PutFrame(PDWORD pData, UINT uCount, BYTE bType)
{
	SendByte(STX);
	
	m_bCheck = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		SendByte(LOBYTE(pData[i]));

		if( bType == '1' ) {

			SendByte(HIBYTE(pData[i]));
			}
		}
		
	BYTE bResult = m_bCheck;
	
	SendByte(ETX);
	
	SendByte(bResult);

	SendPacket();
	}

void CKoyoDirectNetDriver::PutLong(PDWORD pData, UINT uCount, BYTE bType)
{
	SendByte(STX);
	
	m_bCheck = 0;

	for( UINT u = 0; u < uCount; u++ ) {

		SendLong(pData[u]);
		}
		
	BYTE bResult = m_bCheck;
	
	SendByte(ETX);
	
	SendByte(bResult);

	SendPacket();
	}

// Helpers

BOOL CKoyoDirectNetDriver::IsLong(UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:
		case addrWordAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CKoyoDirectNetDriver::GetLong(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x = PU4(m_bRx)[u];
			
		pData[u] = IntelToHost(x); 
		}

	return TRUE;
	}

UINT CKoyoDirectNetDriver::FindCount(UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:
		case addrWordAsReal:

			return 32;
		}

	return 64;
	}

// End of File
