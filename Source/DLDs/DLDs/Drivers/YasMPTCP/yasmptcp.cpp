
#include "intern.hpp"

#include "yasmptcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Series Master TCP Driver
//

// Instantiator

INSTANTIATE(CYaskawaMPMasterTCPDriver);

// Constructor

CYaskawaMPMasterTCPDriver::CYaskawaMPMasterTCPDriver(void)
{
	m_Ident	= DRIVER_ID;

	m_uKeep	= 0;
	}

// Destructor

CYaskawaMPMasterTCPDriver::~CYaskawaMPMasterTCPDriver(void)
{
	}

// Configuration

void MCALL CYaskawaMPMasterTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CYaskawaMPMasterTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CYaskawaMPMasterTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CYaskawaMPMasterTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;
 
			m_pCtx->m_IP		= HostToMotor(GetLong(pData));
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);

			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskawaMPMasterTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) CloseSocket(FALSE);
		}

	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskawaMPMasterTCPDriver::Ping(void)
{
	if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) return CCODE_ERROR;

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = 1;
		Addr.a.m_Offset = 0x00;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;

		return Read(Addr, Data, 1);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CYaskawaMPMasterTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	switch( Addr.a.m_Table ) {

		case SPACE_MW:
		case SPACE_IW:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, 16));
				}
			
			return DoLongRead(Addr, pData, min(uCount, 8));

		case SPACE_MB:
		case SPACE_IB:

			return DoBitRead(Addr, pData, min(uCount, 16));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CYaskawaMPMasterTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	switch( Addr.a.m_Table ) {

		case SPACE_MW:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(Addr, pData, min(uCount, 16));
				}

			return DoLongWrite(Addr, pData, min(uCount, 8));

		case SPACE_MB:
			
			return DoBitWrite(Addr, pData, min(uCount, 16));

		case SPACE_IW:
		case SPACE_IB:

			return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Socket Management

BOOL CYaskawaMPMasterTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaMPMasterTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {
 
			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CYaskawaMPMasterTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

void CYaskawaMPMasterTCPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddByte( m_pCtx->m_bUnit );

	AddByte(bOpcode);
	}

void CYaskawaMPMasterTCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) m_bTx[m_uPtr++] = bData;
	}

void CYaskawaMPMasterTCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CYaskawaMPMasterTCPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CYaskawaMPMasterTCPDriver::SendFrame(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_bTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

	UINT uSize   = m_uPtr;

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k] );

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		return uSize == m_uPtr;
		}

	return FALSE;
	}

BOOL CYaskawaMPMasterTCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(&m_bRx[uPtr], uSize);

		if( uSize ) {

//**/	for( UINT k = uPtr; k < uSize + uPtr; k++ ) AfxTrace1("<%2.2x>", m_bRx[k] );

			uPtr += uSize;

			return m_bRx[0] == m_bTx[0] && m_bRx[1] == m_bTx[1];
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CYaskawaMPMasterTCPDriver::Transact(BOOL fIgnore)
{
	if( SendFrame() && RecvFrame() ) {

		return fIgnore || CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CYaskawaMPMasterTCPDriver::CheckFrame(void)
{
	return !(m_bRx[1] & 0x80);
	}

// Read Handlers

CCODE CYaskawaMPMasterTCPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bOpcode = Addr.a.m_Table == SPACE_IW ? 4 : 3;
	
	StartFrame( bOpcode );

	AddWord(Addr.a.m_Offset);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		uCount = max(m_bRx[2]/sizeof(WORD), 1);

		PWORD p = PWORD(&m_bRx[3]);

		for( UINT n = 0; n < uCount; n++, p++ ) {
		
			WORD x = *p;

			pData[n] = LONG(SHORT(MotorToHost((WORD)x)));
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CYaskawaMPMasterTCPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(Addr.a.m_Table == SPACE_IW ? 4 : 3);

	AddWord(Addr.a.m_Offset);

	AddWord(uCount * 2);
	
	if( Transact(FALSE) ) {

		uCount = max(m_bRx[2]/sizeof(DWORD),1);

		PDWORD p = PDWORD(&m_bRx[3]);

		for( UINT n = 0; n < uCount; n++, p++ ) {
		
			DWORD x = *p << 16;

			x |= *p >> 16;

			pData[n] = MotorToHost((DWORD)x);
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CYaskawaMPMasterTCPDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame( Addr.a.m_Table == SPACE_MB ? 1 : 2);

	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount);

	if( Transact(FALSE) ) {

		uCount = min(uCount, m_bRx[2] * 8u);

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CYaskawaMPMasterTCPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame( uCount == 1 ? 6 : 0x10 );

	AddWord( Addr.a.m_Offset );

	if( uCount > 1 ) {
			
		AddWord(uCount);

		AddByte(uCount * 2);
		}
			
	for( UINT n = 0; n < uCount; n++ ) {

		AddWord(LOWORD(pData[n]));
		}
	
	return Transact(TRUE) ? uCount : CCODE_ERROR;
	}

CCODE CYaskawaMPMasterTCPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(0x10);
			
	AddWord(Addr.a.m_Offset);

	AddWord(uCount * 2);
			
	AddByte(uCount * 4);
			
	for( UINT n = 0; n < uCount; n++ ) {
				
		AddWord(LOWORD(pData[n]));

		AddWord(HIWORD(pData[n]));
		}
	
	return Transact(TRUE) ? uCount : CCODE_ERROR;
	}

CCODE CYaskawaMPMasterTCPDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame( uCount == 1 ? 5 : 0xF );

	AddWord( Addr.a.m_Offset );

        if( uCount == 1 ) AddWord(pData[0] ? 0xFF00 : 0x0000);
	
	else {
		AddWord(uCount);
		
		AddByte((uCount + 7) / 8);

		UINT b = 0;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			if( pData[n] ) b |= m;

			if( !(m <<= 1) ) {

				AddByte(b);

				b = 0;

				m = 1;
				}
			}
		
		if( m > 1 ) AddByte(b);
		}
	
	return Transact(TRUE) ? uCount : CCODE_ERROR;
	}

// End of File
