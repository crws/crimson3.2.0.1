
//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Memobus Spaces
//

#define	SPACE_MW	1
#define	SPACE_IW	2
#define	SPACE_MB	3
#define	SPACE_IB	4

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Series Master TCP Driver
//

class CYaskawaMPMasterTCPDriver : public CMasterDriver
{
	public:
		// Constructor
		CYaskawaMPMasterTCPDriver(void);

		// Destructor
		~CYaskawaMPMasterTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BYTE	 m_bUnit;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			UINT	 m_uMax01;
			UINT	 m_uMax02;
			UINT	 m_uMax03;
			UINT	 m_uMax04;
			UINT	 m_uMax15;
			UINT	 m_uMax16;
			};

		// Data Members
		CContext * m_pCtx;

		BYTE	m_bTx[300];
		BYTE	m_bRx[300];
		UINT	m_uPtr;
		UINT	m_uKeep;
		CRC16	m_CRC;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
				
		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL Transact(BOOL fIgnore);
		BOOL CheckFrame(void);

		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
	
		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
	};
