#ifndef	INCLUDE_ML1SERT_HPP
	
#define	INCLUDE_ML1SERT_HPP

#include "../ML1Ser/ml1serv.hpp"

#include "../ML1Ser/ml1base.cpp"

#include "../ML1Ser/ml1serv.cpp"

#include "../ML1Ser/ml1blk.cpp"

//////////////////////////////////////////////////////////////////////////
//
//  Debugging Help
//

static BOOL ML1_SER_TEST = 1;

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Tester Implementation
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

class CML1Tester : public CML1Server
{
	public:
		// Constructor
		CML1Tester(void);

		// Destructor
		~CML1Tester(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value); 
		DEFMETH(UINT) DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
				
	protected:

		// Help
		IExtraHelper *	m_pExtra;

		// Implementation
		BOOL SendBlockRequest(CML1Dev * pDev, CML1Blk * pBlock, BYTE bOp);
		BOOL DoBlockRead(CML1Dev * pDev, CML1Blk * pBlock);
		BOOL DoBlockWrite(CML1Dev * pDev, CML1Blk * pBlock, UINT uOffset = 0, UINT uCount = 0);
				
		// Read Handlers
		BOOL DoWordRead(CML1Dev * pDev, AREF Addr, UINT uCount);
		BOOL DoLongRead(CML1Dev * pDev, AREF Addr, UINT uCount);
		BOOL DoBitRead (CML1Dev * pDev, AREF Addr, UINT uCount);
		BOOL DoFileRead(CML1Dev * pDev, AREF Addr, UINT uCount);

		// Write Handlers
		BOOL DoWordWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);
		BOOL DoLongWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);
		BOOL DoBitWrite (CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);
		BOOL DoFileWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);

		// Authentication Support
		BOOL GetMacId(void);
		BOOL IsAuthenticated(CML1Dev * pDev);
		BOOL IsAuthPollTimedOut(CML1Dev * pDev);
		void SetAuthPoll(CML1Dev * pDev);
		BOOL PshAuthentication(CML1Dev * pDev);
		BOOL FrcAuthentication(CML1Dev * pDev, UINT uCount);
		
	};

//////////////////////////////////////////////////////////////////////////
//
//  ML1 Serial Tester Driver
//

class CML1SerialTester : public CML1Tester
{
	public:
		// Constructor
		CML1SerialTester(void);

		// Destructor
		~CML1SerialTester(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Configuration
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);				
							
	protected:

		// Data Members
		CRC16	m_CRC;
		UINT	m_Baud;
		BOOL	m_fTrack;

		// Transport
	virtual BOOL Send(void);
	virtual BOOL Recv(void);
	virtual BOOL Respond(PBYTE pBuff);
	virtual BOOL Respond(CML1Dev * pDev);

		// Transport Helpers
		UINT FindRxSize(void);
					
	};

#endif

// End of File
