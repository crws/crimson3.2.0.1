
//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Base Driver
//

#ifndef	BBBSAPBASEINC
#define	BBBSPABASEINC

//////////////////////////////////////////////////////////////////////////
//
// Space Definitions
//
enum {
	SPBIT	=  1,
	SPBYTE	=  2,
	SPWORD	=  3,
	SPLONG	=  4,
	SPREAL	=  5,
	};

enum { // Message function codes
	MFCPEIPC  =  0x3, // for HMI
	MFCPEIRTU = 0x04, // to RTU
	MFCRDDB   = 0x0B, // Read Database
	MFCWRDB   = 0x0C, // Write Database
	MFCRDBYN  = 0x0D, // Read By Name in example file from BB
	MFCCOMREQ = 0x70, // Command Handler Requests
	MFCRDBACC = 0xA0, // remote database access
	};

enum { // Operational function codes
	OFCRDBYN  =  0x4, // Read By Name
	OFCWRBYN  = 0x84, // Write By Name
	};

enum { // Field Select 1 codes
	FS1TYPINH = 0x80,
	FS1VALUE  = 0x40,
	FS1UNITS  = 0x20,
	FS1MSDADD = 0x10,
	FS1NAME   = 0x08,
	FS1ALARM  = 0x04,
	FS1DESC   = 0x02,
	FS1ONOFF  = 0x01,
	};

enum { // Polls and Acks
	RTURESP  = 0x40, // Data Response
	POLLMSG  = 0x85, // Poll message
	POLLDOWN = 0x86, // Down Transmit ACK, Slave ACKing message
	POLLNONE = 0x87, // ACK No Data, in response to a Poll message
	NRTSEND  = 0x88, // Send Node routing Table
	NRTREQ   = 0x89, // Request for NRT message
	POLLUPSL = 0x8A, // UP-ACK with poll (recognized by VSAT Slave)
	POLLUPMS = 0x8B, // UP-ACK, Master ACKing message
	POLLNAK  = 0x95, // NAK, No buffer available for received data
	ALARMACK = 0xA8, // Alarm Acknowledge
	ALARMNOT = 0xAA, // Alarm notification
	};

enum { // Transaction States

	STPOLL    = 1,
	STPWTDATA = 2,
	};

enum { // response return values

	PRNORSP		= 0,
	PRWRONG		= 1,
	PRACK		= 2,
	PRNAK		= 3,
	PRDATA		= 4,
	};

enum { // Write Field Descriptors
	WFDALDIS = 0x1,
	WFDALEN  = 0x2,
	WFDCNDIS = 0x3,
	WFDCNEN  = 0x4,
	WFDMNDIS = 0x5,
	WFDMNEN  = 0x6,
	WFDQUSET = 0x7,
	WFDQUCLR = 0x8,
	WFDLVON  = 0x9,
	WFDLVOFF = 0xA,
	WFDANALG = 0xB,
	WFDSTRNG = 0xD,
	WFDRDSEC = 0xE,
	WFDWRSEC = 0xF,
	};

// UDP Receive positions SH=Standard Header, EH = Extended Header
enum {  // Common Header
	UDPHSZ	= 0,	// Message Header Size
	UDPPCT	= 2,	// Number of Packets
	UDPFLG	= 4,	// Flags
	UDPSEQ	= 6,	// Send Sequence Number
	UDPLAK	= 10,	// Last Ack'ed Seq Number
	};

enum {  // Extra for extended header
	UDPRIP	= 14,	// Sender IP
	UDP000	= 18,	// Filler
	};

enum {  // Standard Header Subpackets
	UDPSSZ	= 14,	// Subpacket size
	UDPSTY	= 16,	// Type (BSAP = 0)
	UDPSMX	= 18,	// Exchange Code
	UDPSMS	= 19,	// Message Seq #
	UDPSGA	= 21,	// Global Address of Sender
	UDPSER	= 23,	// Error if not 0
	UDPSCT	= 24,	// Number of Items (= 1 for good reply)
	UDPSDT	= 25,	// Data start
	};

enum {  // Extended Header Subpackets
	UDPESZ	= 22,	// Subpacket size
	UDPETY	= 24,	// Type (BSAP = 0)
	UDPEMX	= 26,	// Exchange Code
	UDPEMS	= 27,	// Message Seq #
	UDPEGA	= 29,	// Global Address of Sender
	UDPEER	= 31,	// Error if not 0
	UDPECT	= 32,	// Number of Items (= 1 for good reply)
	UDPEDT	= 33,	// Data start
	};

// Receive Frame Check codes
enum {
	FRCONT	= 0,
	FRDONE	= 1,
	FRERR	= 2
	};

#define	SZHDREXT	((WORD)22)
#define	SZHDRSTD	((WORD)14)
#define	POLLPOSN	23

#define	FLAGNEW		6
#define	FLAGOLD		4

struct BSHeader { // Serial Comms Header
	BYTE	bAdd;
	BYTE	bSNum;
	BYTE	bDFC;
	BYTE	bSQL;
	BYTE	bSQH;
	BYTE	bSFC;
	BYTE	bNSB;
	};

class CBBBSAPBaseDriver : public CMasterDriver
{
	public:
		// Constructor
		CBBBSAPBaseDriver(void);

		// Destructor
		~CBBBSAPBaseDriver(void);
		
		// Entry Points
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Context
		struct CBaseCtx
		{
			// Protocol information
			UINT	 m_uAppSeq;

			// Tag information
			PBYTE	 m_pbDevName;
			BOOL	 m_fUDP;
			BOOL	 m_fHasTags;
			UINT	 m_uCnt[5];
			PWORD	 m_pwfAddrList;
			PWORD	 m_pwbAddrList;
			PWORD	 m_pwwAddrList;
			PWORD	 m_pwdAddrList;
			PWORD	 m_pwrAddrList;
			PWORD	 m_pwfAddrIndex;
			PWORD	 m_pwbAddrIndex;
			PWORD	 m_pwwAddrIndex;
			PWORD	 m_pwdAddrIndex;
			PWORD	 m_pwrAddrIndex;
			PBYTE	 m_pbNameList;
			PWORD	 m_pwNameIndex;
			};

	protected:
		// Data Members
		CBaseCtx *m_pBase;

		CRC16	m_CRC;

		PBYTE	m_pTx;
		PBYTE	m_pRx;

		UINT	m_uPtr;
		UINT	m_uWriteError;
		UINT	m_uState;

		WORD	m_wAddress;
		
		// Frame Building
		virtual	void	StartFrame(BOOL fFunc, BOOL fIsWrite);
		virtual	void	PutStart(BOOL fFunc);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddReal(DWORD dwData);
		void	AddText(PCTXT pText);
		BOOL	AddVarName(UINT uTable, UINT uOffset);
		UINT	FindNamePos(PWORD pAddrIndex, PWORD pAddrOffset, UINT uListSize, UINT uOffset);

		// Read Handlers
		BOOL	DoBitRead (AREF Addr, PDWORD pData);
		BOOL	DoByteRead(AREF Addr, PDWORD pData);
		BOOL	DoWordRead(AREF Addr, PDWORD pData);
		BOOL	DoLongRead(AREF Addr, PDWORD pData);
		BOOL	DoRealRead(AREF Addr, PDWORD pData);
		BOOL	AddReadByName(UINT uTable, UINT uOffset);
		void	AddReadByNameHeader(void);

		// Write Handlers
		BOOL	DoBitWrite (PDWORD pData);
		BOOL	DoByteWrite(PDWORD pData);
		BOOL	DoWordWrite(PDWORD pData);
		BOOL	DoLongWrite(PDWORD pData);
		BOOL	DoRealWrite(PDWORD pData);
		BOOL	AddWriteByName(UINT uTable, UINT uOffset);
		BYTE	AddDescriptor(UINT uTable, UINT uOffset, BOOL fBitData);
		void	AddWriteData(UINT uType, DWORD dData);

		// Tags
		BOOL	LoadTags(LPCBYTE &pData);
		BOOL	StoreDevName(LPCBYTE &pData);
		void	GetTypeCounts(LPCBYTE &pData, PDWORD pAddrList, UINT uCount);
		void	StoreAddrIndices(PDWORD pAddrList, UINT uCount);
		void	StoreNames(LPCBYTE &pData, UINT uCount);

		// Transport Layer
		virtual	UINT	Send(void);
		virtual BOOL	Transact(void);
		virtual	BOOL	RecvFrame(void);

		// Helpers
		virtual	BOOL	CheckHeader(void);
		virtual	BOOL	HasTags(PDWORD pData, UINT uCount);
		WORD		GetRcvWord (UINT uPos);
		DWORD		GetRcvDWord(UINT uPos);
	};

/*
Serial:
Request Message Format:
DLE STX
DADD SERN SFC SQL SQH DFC NSB
04 FSS FS1 <FS2...FS4> SEC #ELE
NAME
DLE ETX CRCL CRCH

Response Data Message
DLE STX
DADD SERN SFC SQL SQH DFC NSB
RER #ELE
<DATA>
DLE ETX CRCL CRCH

Write Message Format
DLE STX
DADD SERN SFC SQL SQH DFC NSB
84 SEC #ELE
NAME
WFD1 <DATA>

FUN Function code
bit 7 function bit
0 read function
1 write function
bits 6-4 = data type code
000 signal data
001 data array data
010 direct I/O data
100 memory data
111 Extended RDB function
bits 3-2 = select code
00 select via address or data array number, control
01 select via name or data array number, short form
10 select via match or data array number, general form
11 select via list
bits 1-0 = signal READ return code, WRITE Memory code
For READ:
00 return first match
01 return next match/continue list
10 return packed logical data (logical value data only)
For WRITE Memory,
00 Write Data to memory
01 OR Data with memory
10 AND Data with memory

NOTE:FIELD SELECTOR BYTES ARE ONLY USED IN ACCESSING SIGNAL
DATA WHEN PACKED LOGICAL DATA IS NOT USED.
FSS Field select selector defines which field select bytes are included in the request:
BITS 7-4 not used at this time (reserved for priority)
BIT 3 Field select byte 4 is used
BIT 2 Field select byte 3 is used
BIT 1 Field select byte 2 is used
BIT 0 Field select byte 1 is used

FS1 Field selector byte one (most common options):
BIT 7 Type and Inhibits byte
BIT 6 Value
BIT 5 Units/Logical text
BIT 4 Msd address
BIT 3 Name Text (includes Base.Extension.Attribute)
BIT 2 Alarm status
BIT 1 Descriptor text
BIT 0 Logical ON/OFF text

FS2 Field selector byte two:
BIT 7 Protection byte
BIT 6 Alarm priority
BIT 5 Base Name
BIT 4 Extension
BIT 3 Attribute
BIT 2 Low alarm deadband MSD address
BIT 1 High alarm deadband MSD address
BIT 0 MSD Version number

FS3 Field selector byte three:
BIT 7 Low alarm limit MSD address
BIT 6 High alarm limit MSD address
BIT 5 Extreme low alarm limit MSD address
BIT 4 Extreme high alarm limit MSD address
BIT 3 Reserved for Report by exception
BIT 2 Reserved (old Lock bit)
BIT 1 Signal value; Analogs return no Q-bit in FP mantissa.
BIT 0 When set, allows long signal names to be returned, instead of standard
length names. (Requires ControlWave firmware version 2.2 or newer)

FS4 Field selector byte four:
This byte is reserved for future use.
*/

// End of File

#endif
