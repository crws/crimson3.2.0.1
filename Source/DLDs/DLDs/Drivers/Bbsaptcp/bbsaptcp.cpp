
#include "intern.hpp"

#include "bbsaptcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock UDP Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CBBBSAPTCPDriver);

// Constructor

CBBBSAPTCPDriver::CBBBSAPTCPDriver(void)
{
	m_Ident		= DRIVER_ID;

	m_pTxBuff	= NULL;

	m_pRxBuff	= NULL;

	m_pTxData	= NULL;

	memset(m_bRx, 0, sizeof(m_bRx));

	m_pRx           = m_bRx;

	m_fReqNotDone	= FALSE;

	m_fExtended	= FALSE;

	m_fDebug        = FALSE;

	m_fListDebug    = FALSE;

	m_uListNumber	= 0;

	m_uListEntry	= 0;

	m_dListResp	= 0;

	m_pRFrame	= &m_RFrame;
	}

// Destructor

CBBBSAPTCPDriver::~CBBBSAPTCPDriver(void)
{
	}

// Configuration

void MCALL CBBBSAPTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CBBBSAPTCPDriver::Attach(IPortObject *pPort)
{
	m_pSock = CreateSocket(IP_UDP);
	}

void MCALL CBBBSAPTCPDriver::Detach(void)
{
	if( m_pSock ) {

		m_pSock->Release();

		m_pSock = NULL;
		} 
	}

void MCALL CBBBSAPTCPDriver::Open(void)
{
	if( m_pSock ) {

		m_pSock->SetOption(OPT_ADDRESS, 1);
		
		m_pSock->SetOption(OPT_RECV_QUEUE, 4);
		}
	}

// Device

CCODE MCALL CBBBSAPTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_fHasTags	= FALSE;
			m_pCtx->m_wTagCount     = 0;
			m_pCtx->m_uStrings      = 0;
			m_pCtx->m_pdAddrList    = NULL;
			m_pCtx->m_pwNameIndex   = NULL;
			m_pCtx->m_pbNameList    = NULL;
			m_pCtx->m_pwMSDList     = NULL;
			m_pCtx->m_pfMSDList     = NULL;
			m_pCtx->m_pStrings      = NULL;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_dTrans	= 0;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->m_ThisIP	= 0;
			m_pCtx->m_fDirty	= FALSE;

			WORD wKey = GetWord(pData);

			switch( wKey ) {

				case 0xABCD: m_pCtx->m_bIsC3 = 2; break;
				case 0xBCDE: m_pCtx->m_bIsC3 = 3; break;

				default: return CCODE_ERROR | CCODE_HARD;
				}

			if( LoadTags(pData) ) {

				LoadStringSupport(pData);
				}

			if( m_pSock ) {

				m_pSock->Listen(m_pCtx->m_uPort);
				}

			pDevice->SetContext(m_pCtx);

			m_pCtx->m_uAppSeq	= 1;
			m_pCtx->m_wMsgSeq	= 0;
			m_pCtx->m_dLastAck	= 0;
			m_pCtx->m_fOpen		= FALSE;
			m_pCtx->m_fMSD		= FALSE;
			m_pCtx->m_uMSDVers	= m_Ident;
			m_pCtx->m_fInit		= FALSE;

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CBBBSAPTCPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		CloseDown();

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

UINT MCALL CBBBSAPTCPDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		pCtx->m_IP = MotorToHost(dwValue);

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort  = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 4 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP);
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL CBBBSAPTCPDriver::Ping(void)
{
	if( m_fDebug ) {
		
		AfxTrace0("\r\nPING ");
		}

	m_pCtx->m_fMSD  = !m_pCtx->m_fMSD;
	
	m_pCtx->m_fInit = FALSE;
	
	if( m_pSock && SendPoll() ) {
		
		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

// Entry Points

CCODE MCALL CBBBSAPTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_fDebug ) {
		
		AfxTrace3("\r\nRead Entry T = %d, O = %d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);
		}

	if( !HasTags(pData, uCount) ) {

		return uCount;
		}

	if( m_pRx[UDPFLG] & 0x30 ) {

		m_pRx[UDPFLG] = 0;

		return CCODE_ERROR | CCODE_BUSY; // Low memory in RTU, skip one send
		}

	if( m_fListDebug ) {

		CCODE List = DoListRead(Addr, pData, uCount);

		if( COMMS_SUCCESS(List) ) {

			return List;
			}
		}

	if( m_pCtx->m_fMSD ) {

		m_uIndex = GetItemIndex(Addr);

		if( m_uIndex < NOTHING ) {

			BOOL fMSD = HasMSD(Addr, uCount);

			m_pRFrame->fReqMSD = !fMSD;

			m_pRFrame->fDoMSD  = fMSD;

			if( fMSD ) {

				return DoReadByAddr(Addr, pData, uCount);
				}
			}
		}

	return DoReadByName(Addr, pData, uCount);
	}

CCODE MCALL CBBBSAPTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_fDebug ) {
		
		AfxTrace3("\r\n\n**** WRITE T = %d, O = %d D = %8.8lx\r\n", Addr.a.m_Table, Addr.a.m_Offset, *pData);
		}

	if( !HasTags(pData, uCount) ) return uCount;

	if( m_pRx[UDPFLG] & 0x20 ) {

		m_pRx[UDPFLG] = 0;

		return CCODE_ERROR | CCODE_BUSY; // Low memory in RTU, skip one send
		}

	if( m_fListDebug ) {

		CCODE List = DoListWrite(Addr, pData, uCount);

		if( COMMS_SUCCESS(List) ) {

			return List;
			}
		}

	CCODE cCode = CCODE_ERROR;

	if( m_pCtx->m_fMSD ) {

		m_uIndex = GetItemIndex(Addr);

		if( m_uIndex < NOTHING ) {

			BOOL fMSD = HasMSD(Addr, uCount);

			if( !fMSD ) {

				PDWORD pRead = PDWORD(alloca(uCount));

				cCode = DoReadByName(Addr, pRead, uCount);

				if( cCode == uCount ) {

					m_uIndex = GetItemIndex(Addr);

					fMSD = HasMSD(Addr, uCount);
					}
				else {  
					cCode = DoWriteByName(Addr, pData, uCount);
					}
				}

			if( fMSD ) {
				
				cCode = DoWriteByAddr(Addr, pData, uCount);
				}
			}
		}
	else {  
		cCode = DoWriteByName(Addr, pData, uCount);
		}

	if( COMMS_SUCCESS(cCode) ) {

		return cCode;
		}

	if( ++m_uWriteError > 2 ) {

		m_uWriteError = 0;

		return uCount;
		}

	return cCode;
	}

// Frame Building

BOOL CBBBSAPTCPDriver::StartFrame(void)
{
	if( (m_pTxBuff = CreateBuffer(600, TRUE)) ) {

		m_pTxBuff->AddTail(600);

		m_pTxData = m_pTxBuff->GetData();

		m_uPtr  = 0;

		AddUDPHeader(1, m_pCtx->m_uAppSeq, m_pCtx->m_dLastAck);

		AddUDPPacketHeader();

		return TRUE;
		}
		
	Sleep(10);

	return FALSE;
	}

void CBBBSAPTCPDriver::EndFrame(void)
{
	}

void CBBBSAPTCPDriver::AddUDPHeader(UINT uPktCt, DWORD dSeq, DWORD dAck)
{
	WORD wFlags = m_pCtx->m_fOpen ? FLAGOLD : FLAGNEW;

	AddWord(m_fExtended ? SZHDREXT : SZHDRSTD); // Length
	AddWord(LOWORD(uPktCt)); // # of sub packets
	AddWord(wFlags); // new conversation or Ack old
	AddLong(dSeq);   // sequence number
	AddLong(dAck);   // latest sequence number received

	if( m_fExtended ) {

		AddLong(m_pCtx->m_ThisIP);
		AddLong(0);
		}
	}

void CBBBSAPTCPDriver::AddUDPPacketHeader(void)
{
	UpdateMsgSeq();

	AddWord(0x0000); // will be count
	AddWord(0x0000); // BSAP
	AddByte(0x04);   // exchange code
	AddWord(m_pCtx->m_wMsgSeq);
	AddWord(0x0000); // Global Address
	AddByte(0xA0);   // Remote Data Base Access
	AddByte(0x00);   // Routing Table Version
	}

void CBBBSAPTCPDriver::AddByte(BYTE bData)
{
	m_pTxData[m_uPtr++] = bData;
	}

void CBBBSAPTCPDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CBBBSAPTCPDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CBBBSAPTCPDriver::AddReal(DWORD dwData)
{
	AddLong(dwData);
	}

void CBBBSAPTCPDriver::AddText(PCTXT pText)
{
	if( m_fDebug ) {
		
		AfxTrace0("\r\nName: ");
		
		for( UINT d = 0; d < strlen(pText); d++ ) {
			
			AfxTrace1("%C", pText[d]);
			}
		}

	for( UINT i = 0; i < strlen(pText); i++ ) {

		AddByte(pText[i]);
		}
	}

UINT CBBBSAPTCPDriver::AddVarName(AREF Addr, UINT uIndex, UINT uBytes)
{
	uIndex = GetNamePos(Addr, uIndex);
	
	if( uIndex < 0xFFFF ) {

		WORD wIndex = m_pCtx->m_pwNameIndex[uIndex];

		PBYTE pName = &m_pCtx->m_pbNameList[wIndex];

		if( pName ) {

			if( CanAddToRequest(pName, uBytes) ) {

				AddText((PCTXT)pName);

				AddByte(0);

				return VADDOK;
				}

			return VTXFULL;
			}
		}

	return VNONAME;
	}

UINT CBBBSAPTCPDriver::AddVarAddr(UINT uOffset, UINT uBytes)
{
	if( m_uIndex < NOTHING ) {

		if( CanAddToRequest(NULL, sizeof(WORD)) ) {

			AddWord(uOffset);

			return VADDOK;
			}

		return VTXFULL;
		}

	return VNONAME;
	}

void  CBBBSAPTCPDriver::AddSize(void)
{
	WORD wSize = m_uPtr - m_pTxData[0];

	BYTE bData = m_pTxData[0];

	m_pTxData[bData++] = LOBYTE(wSize);

	m_pTxData[bData  ] = HIBYTE(wSize);
	}

UINT CBBBSAPTCPDriver::GetNamePos(AREF Addr, UINT uIndex)
{
	UINT uOffset = FindOffset(Addr, uIndex);

	for( UINT i = 0; i < m_pCtx->m_wTagCount; i++ ) {

		if( uOffset == LOWORD(m_pCtx->m_pdAddrList[i]) ) {

			return i;
			}
		}

	return 0xFFFF;
	}

BOOL CBBBSAPTCPDriver::DoCommandMsg(BYTE bCommand, BOOL fIsPoll)
{
	if( StartFrame() ) {

		AddByte(bCommand);
		
		AddWord(0);

		AddSize();
		
		UpdateMsgSeq();

		if( fIsPoll ) {

			return Transact(); // get the receive
			}

		return SendFrame(); // Acks - no response expected
		}

	return FALSE;
	}

// Polling

BOOL CBBBSAPTCPDriver::SendPoll(void)
{
	if( m_fDebug ) {
		
		AfxTrace0("\r\nPOLL ");
		}

	BOOL f = DoCommandMsg(POLLMSG, TRUE);

	if( m_fDebug ) {
		
		AfxTrace0("\r\nPOLL END ");
		}

	return f;
	}

void CBBBSAPTCPDriver::DiscardRetry(void)
{
	if( RecvFrame() ) {

		DWORD dRxAck = GetRcvDWord(UDPLAK);
	
		DWORD dTxSeq = GetTxDWord (UDPSEQ);

		if( dRxAck == dTxSeq ) {

			m_pCtx->m_dLastAck = GetRcvDWord(UDPSEQ);
			}
		}
	}

// Read Handlers

CCODE CBBBSAPTCPDriver::DoDataRead(AREF Addr)
{
	UINT uPos       = 0;

	if( (BOOL)(uPos = Send()) ) {

		WORD wSeq = GetRcvWord(uPos - 6);

		if( wSeq == 0 ) {

			// Ack only, no data.

			return FALSE;
			}

		if( wSeq == m_pCtx->m_wMsgSeq ) {

			return GetRead(uPos, Addr);
			}
		}

	return CCODE_ERROR;
	}

DWORD CBBBSAPTCPDriver::GetReadData(AREF Addr, UINT *pOffset, UINT uIndex)
{
	UINT uPos  = *pOffset;

	UINT uDPos = uPos + 1;

	BYTE bType = (m_bRx[uPos]) & 0x3;

	DWORD d    = 0;

	switch( bType ) {

		case 0:	// type is bit

			uPos += 2;

			BOOL f;

			f = (BOOL)m_bRx[uDPos];

			d = (DWORD)(f ? (Addr.a.m_Type == addrRealAsReal ? 0x3F800000 : 1) : 0);

			break;

		case 2:	// type is analog

			uPos += 5;

			d = (DWORD)IntelToHost(*(PU4)&m_bRx[uDPos]);

			break;

		case 3:	// type is string

			d = GetStringData(Addr, uPos, uDPos, uIndex);
			
			break;
		}

	if( m_pRFrame->fReqMSD ) {

		WORD wMSD = (WORD)IntelToHost(*(PU2)&m_bRx[uPos]);

		m_pCtx->m_pwMSDList[m_uIndex] = wMSD;

		m_pCtx->m_pfMSDList[m_uIndex] = TRUE;

		m_pCtx->m_uMSDVers = (WORD)IntelToHost(*(PU2)&m_bRx[uPos+2]);

		uPos += 4;

		m_uIndex++;
		}

	*pOffset = uPos;

	if( !IsString(Addr.a.m_Table) ) {

		float Real = I2R(d);

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	return (DWORD)((BOOL)d);
			case addrByteAsByte:	return (DWORD)LOBYTE(Real);
			case addrWordAsWord:	return (DWORD)LOWORD(Real);
			case addrLongAsLong:	return (DWORD)Real;
			}
		}

	return d;
	}

DWORD CBBBSAPTCPDriver::GetStringData(AREF Addr, UINT &uPos, UINT uDPos, UINT uIndex)
{
	UINT uStart  = ExpandCount(Addr, uIndex);

	UINT uChar   = GetCharCount((PCTXT)m_bRx, uDPos);

	UINT uAdd    = uChar % 4 ? 1 : 0;

	UINT uCopy   = min(uChar / sizeof(DWORD) + uAdd, MAX_LTEXT);

	for( UINT u  = uStart; u < uStart + uCopy; u++, uDPos += 4 ) {

		m_pRFrame->pData[u] = (DWORD)IntelToHost(*(PU4)&m_bRx[uDPos]);
		}

	PTXT pText   = (PTXT)(m_pRFrame->pData + uStart);
	
	CAddress Address;
	
	Address.m_Ref = Addr.m_Ref;
	
	Address.a.m_Offset += uStart;
	
	SetStringContext((AREF)Address, (PCTXT)pText, min(uChar, uCopy * sizeof(DWORD)));

	uPos += uChar + 1;
	
	uPos++;

	return m_pRFrame->pData[0];
	}

// Read Handlers

CCODE CBBBSAPTCPDriver::DoReadByName(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !StartFrame() ) {

		return CCODE_ERROR;
		}

	m_pCtx->m_fMSD ? AddReadByNameHeaderMSD() : AddReadByNameHeader();
	
	m_pRFrame->uAddr   = Addr.a.m_Offset;
	m_pRFrame->pData   = pData;
	m_pRFrame->uCount  = uCount;
	m_pRFrame->uGood   = 0;
	
	CCODE cCode = AddReadByName(Addr);

	if( COMMS_SUCCESS(cCode) ) {
		
		cCode = DoDataRead(Addr);

		if( !COMMS_SUCCESS(cCode) ) {

			return cCode;
			}
		
		if( !m_bRx[UDPPCT] ) {
			
			if( m_fDebug ) {
				
				AfxTrace0("\r\nNO DATA\r\n");
				}

			return CCODE_ERROR | CCODE_NO_DATA;
			}

		if( !m_pCtx->m_fInit && m_pCtx->m_fMSD ) {
			
			m_pCtx->m_fInit = TRUE;
			}
		
		return cCode;
		}

	ClearTxBuffer();
				
	return cCode;
	}

CCODE CBBBSAPTCPDriver::AddReadByName(AREF Addr)
{
	READFRAME *p = m_pRFrame;

	UINT uCode   = NOTHING;

	UINT uGood   = 0;

	UINT uOk     = 0;

	UINT uPos    = m_uPtr;

	AddByte(0);

	UINT uCount = GetCount(Addr, p->uCount);

	for( UINT u = 0; u < uCount; u++ ) {

		uCode = AddVarName(Addr, u);

		if( uCode == VADDOK ) {

			uOk++;

			uGood |= (1 << u);

			continue;
			}

		if( uCode == VTXFULL || uCode == VNONAME ) {

			p->uCount = uOk;
			}

		break;
		}

	if( uOk ) {

		p->uGood  = uGood;

		AddSize();

		m_pTxData[uPos] = uOk;

		return ExpandCount(Addr, uOk);
		}
	
	return uCode == VNONAME ? CCODE_ERROR | CCODE_HARD : CCODE_ERROR;
	}

void CBBBSAPTCPDriver::AddReadByNameHeader(void)
{
	AddByte(OFCRDBYN);				// Read Signal by Name

	AddByte(0x01);					// FSS quantity

	AddByte(FS1VALUE | FS1TYPINH);			// FSS Value

	AddByte(0xFF);					// Security level

	m_pRFrame->fReqMSD = FALSE;

	m_pRFrame->fDoMSD  = FALSE;
	}

void CBBBSAPTCPDriver::AddReadByNameHeaderMSD(void)
{
	AddByte(OFCRDBYN);				// Read Signal by Name

	AddByte(0x03);					// FSS quantity

	AddByte(FS1VALUE | FS1TYPINH | FS1MSDADD);	// FSS Value

	AddByte(FS2MSDVER);			

	AddByte(0xFF);					// Security level

	m_pRFrame->fReqMSD = TRUE;

	m_pRFrame->fDoMSD  = FALSE;
	}

CCODE CBBBSAPTCPDriver::DoReadByAddr(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !StartFrame() ) {

		return CCODE_ERROR;
		}

	m_pRFrame->uAddr  = Addr.a.m_Offset;
	m_pRFrame->pData  = pData;
	m_pRFrame->uCount = uCount;
	m_pRFrame->uGood  = 0;

	AddReadByAddrHeader();

	CCODE cCode = AddReadByAddr(Addr);

	if( COMMS_SUCCESS(cCode) ) {
	
		cCode = DoDataRead(Addr);

		if( !COMMS_SUCCESS(cCode) ) {

			return cCode;
			}

		if( !m_bRx[UDPPCT] ) { 

			return CCODE_ERROR | CCODE_NO_DATA;
			}
		
		return cCode;
		}

	ClearTxBuffer();

	return cCode;
	}

void CBBBSAPTCPDriver::AddReadByAddrHeader(void)
{
	AddByte(OFCRDBYA);

	AddByte(1);

	AddByte(FS1TYPINH | FS1VALUE);

	AddWord(m_pCtx->m_uMSDVers);

	AddByte(0xFF);
	}

CCODE CBBBSAPTCPDriver::AddReadByAddr(AREF Addr)
{
	READFRAME *p = m_pRFrame;

	UINT uCode   = NOTHING;

	UINT uGood   = 0;

	UINT uOk     = 0;

	UINT uPos    = m_uPtr;

	PWORD pMSD   = &m_pCtx->m_pwMSDList[m_uIndex];

	UINT uCount  = GetCount(Addr, p->uCount);

	AddByte(uCount);

	for( UINT u = 0; u < uCount; u++ ) {

		uCode  = AddVarAddr(pMSD[u]);

		if( uCode == VADDOK ) {

			uOk++;

			uGood |= (1 << u);

			continue;
			}
		
		if( uCode == VTXFULL || uCode == VNONAME ) {

			p->uCount = uOk;
			}

		break;
		}

	if( uOk ) {

		p->uGood  = uGood;

		AddSize();

		return ExpandCount(Addr, uOk);
		}
	
	return uCode == VNONAME ? CCODE_ERROR | CCODE_HARD : CCODE_ERROR;
	}

CCODE  CBBBSAPTCPDriver::GetRead(UINT uPos, AREF Addr)
{
	READFRAME * p = m_pRFrame;

	PDWORD pData  = p->pData;

	UINT uCount   = p->uCount;

	BOOL fHasErr  = m_bRx[uPos - 2] & 0x80;				// error(s) present code

	uCount	      = min( m_bRx[uPos - 1], p->uCount );		// item count to process

	for( UINT i = 0; i < uCount; i++, pData++ ) {

		DWORD dData = 0;

		BOOL fErr = fHasErr && (BOOL)m_bRx[uPos];

		if( fHasErr ) {
			
			uPos += 1;					// allow for leading error byte
			}

		if( !fErr ) {

			dData = GetReadData(Addr, &uPos, i);
			
			if( !IsString(Addr.a.m_Table) ) {

				*pData = dData;
				}

			continue;
			}

		break;
		}

	return i ? ExpandCount(Addr, i) : CCODE_ERROR;
	}

// Write Handlers

CCODE CBBBSAPTCPDriver::DoWriteByName(AREF Addr, PDWORD pData, UINT uCount)
{	
	if( !StartFrame() ) {

		return CCODE_ERROR;
		}

	AddByte(OFCWRBYN);	// write by name opcode

	AddByte(0xFF);		// Security level

	AddByte(0x00);

	CCODE cCode = AddWriteByName(Addr, pData, uCount);

	if( COMMS_SUCCESS(cCode) ) {

		AddWriteElementCount((BYTE)cCode);
	
		if( Transact() ) {

			m_uWriteError = 0;
		
			return cCode;
			}

		return CCODE_ERROR;
		}

	ClearTxBuffer();

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CBBBSAPTCPDriver::AddWriteByName(AREF Addr, PDWORD pData, UINT uCount)
{	
	UINT Count = GetCount(Addr, uCount);		
	
	for(UINT u = 0; u < Count; u++ ) { 

		UINT uBytes = GetDataByteCount(Addr, uCount);

		UINT uCode  = AddVarName(Addr, u, uBytes);

		if( uCode == VADDOK ) {

			if( IsString(Addr.a.m_Table) ) {

				AddStringData(Addr, pData, u, uCount); 

				continue;
				}

			AddWriteData(Addr, pData[u]);

			continue;
			}

		return u ? ExpandCount(Addr, u) : uCode == VNONAME ? CCODE_ERROR | CCODE_HARD : CCODE_ERROR; 
		}

	return uCount;
	}

void CBBBSAPTCPDriver::AddWriteData(AREF Addr, DWORD dData)
{
	switch( Addr.a.m_Type ) {

		case addrByteAsByte:	dData &= 0xFF;		break;

		case addrWordAsWord:	dData &= 0xFFFF;	break;
		}

	float Real = float(LONG(dData));

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			AddByte(dData ? 9 : 10);
			break;

		case addrByteAsByte:

			AddByte(WFDANALG);
			AddLong(R2I(Real));
			break;

		case addrWordAsWord:

			AddByte(WFDANALG);
			AddLong(R2I(Real));
			break;

		case addrLongAsLong:

			AddByte(WFDANALG);
			AddLong(R2I(Real));
			break;

		case addrRealAsReal:

			AddByte(WFDANALG);
			AddLong(dData);
			break;
		}

	AddSize();
	}

void CBBBSAPTCPDriver::AddStringData(AREF Addr, PDWORD pData, UINT &uIndex, UINT uCount)
{	
	UINT uMax    = MAX_LTEXT;

	UINT uFrom   = ExpandCount(Addr, uIndex);

	UINT uOffset = (Addr.a.m_Offset + uFrom) % uMax;

	PBYTE pCopy  = PBYTE(alloca(MAX_CHARS));

	memset(pCopy, 0, MAX_CHARS);

	MakeMin(uCount, uMax);

	if( uOffset ) {

		CAddress Address;

		Address.m_Ref = Addr.m_Ref + uFrom - uOffset;

		UINT uString  = GetStringContextIndex((AREF)Address);

		if( uString < NOTHING ) {

			memcpy(pCopy, m_pCtx->m_pStrings[uString].m_Text, MAX_CHARS);
			}
		}

	PDWORD pStr  = PDWORD(pCopy);

	if( uIndex && uOffset ) {

		uFrom  -= uOffset;

		uOffset = 0;
		}

	for( UINT n = 0; n < uCount; n++ ) {

		pStr[n + uOffset] = HostToIntel(pData[n + uFrom]);
		}

	RemoveTrailing(pCopy, 0x20, MAX_CHARS);

	AddByte(WFDSTRNG);

	UINT uChar = GetCharCount((PCTXT)pCopy, 0);

	UINT uAdd  = uChar % 4 ? 1 : 0;

	UINT uCopy = uChar / sizeof(DWORD) + uAdd;

	for( UINT u = 0; u < uCopy; u++ ) {

		AddLong(pStr[u]);
		}

	while( m_pTxData[m_uPtr - 1] == 0 ) {

		m_uPtr--;
		}

	AddByte(0);

	AddSize();
	}

void CBBBSAPTCPDriver::AddWriteElementCount(BYTE bCount, BYTE bPos)
{
	m_pTxData[UDPSCT + 3 + bPos] = bCount;
	}

CCODE CBBBSAPTCPDriver::DoWriteByAddr(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !StartFrame() ) {

		return CCODE_ERROR;
		}

	AddByte(OFCWRBYA);			// write by address opcode

	AddByte(LOBYTE(m_pCtx->m_uMSDVers));	// Version

	AddByte(HIBYTE(m_pCtx->m_uMSDVers));

	AddByte(0xFF);				// Security level

	AddByte(0);

	CCODE cCode = AddWriteByAddr(Addr, pData, uCount);

	if( COMMS_SUCCESS(cCode) ) {

		AddWriteElementCount((BYTE)cCode, 2);
	
		if( Transact() ) {

			m_uWriteError = 0;
		
			return cCode;
			}

		return CCODE_ERROR;
		}

	ClearTxBuffer();

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CBBBSAPTCPDriver::AddWriteByAddr(AREF Addr, PDWORD pData, UINT uCount)
{	
	PWORD pMSD = &m_pCtx->m_pwMSDList[m_uIndex];

	UINT Count = GetCount(Addr, uCount);		
	
	for(UINT u = 0;  u < Count; u++ ) {	

		UINT uBytes = GetDataByteCount(Addr, uCount);

		UINT uCode  = AddVarAddr(pMSD[u], uBytes);

		if( uCode == VADDOK ) {

			if( IsString(Addr.a.m_Table) ) {

				AddStringData(Addr, pData, u, uCount); 

				continue;
				}

			AddWriteData(Addr, pData[u]);

			continue;
			}

		return u ? ExpandCount(Addr, u) : uCode == VNONAME ? CCODE_ERROR | CCODE_HARD : CCODE_ERROR; 
		}

	return uCount;
	}

// String Support

UINT CBBBSAPTCPDriver::GetStringContextIndex(AREF Addr)
{
	if( IsString(Addr.a.m_Table) ) {

		UINT uString = Addr.a.m_Offset / MAX_LTEXT;

		if( uString < m_pCtx->m_uStrings ) {

			return uString;
			}
		}

	return NOTHING;
	}

void CBBBSAPTCPDriver::SetStringContext(AREF Addr, PCTXT pText, UINT uCount)
{
	CStringTag String;

	UINT uIndex = GetStringContextIndex(Addr);

	if( uIndex < NOTHING ) {

		memcpy(m_pCtx->m_pStrings[uIndex].m_Text, pText, uCount);
		}
	}

UINT CBBBSAPTCPDriver::GetStringCount(AREF Addr, UINT uPos, UINT uCount)
{
	UINT uSize    = MAX_LTEXT;

	UINT uOffset  = Addr.a.m_Offset % uSize;

	if( uPos == 0 ) {

		return uSize - uOffset;
		}

	return min(uCount, uSize);
	}

UINT CBBBSAPTCPDriver::GetCharCount(PCTXT pText, UINT uOffset)
{
	UINT uChars = 0;

	if( pText ) {

		while( IsStringText(pText[uOffset + uChars]) ) {

			uChars++;
			}
		}

	return uChars;
	}

void CBBBSAPTCPDriver::RemoveTrailing(PBYTE &pByte, BYTE bByte, UINT uBytes)
{
	UINT uRemove = pByte? uBytes - 1 : 0;

	while( uRemove > 0 ) {

		if( pByte[uRemove] == 0x20 ) {

			pByte[uRemove] = 0;

			uRemove--;

			continue;
			}
		break;
		}
	}

// Acks

void CBBBSAPTCPDriver::SendAck(void)
{
	if( m_bTx[POLLPOSN] != POLLMSG ) {

		if( m_fDebug ) {
			
			AfxTrace0("\r\n\nACK ");
			}

		DoCommandMsg(POLLUPMS, FALSE);

		UINT uAck = m_bRx[2];

		for( UINT i = 1; i < uAck; i++ ) {

			DoCommandMsg(POLLUPMS, FALSE); // ack all extra packets
			}

		UpdateAppSeq();
		}

	if( m_fDebug ) {
		
		AfxTrace0("\r\nACK END\r\n");
		}
	}

void CBBBSAPTCPDriver::SendAlarmAck(void)
{
	DoCommandMsg(ALARMACK, FALSE);
	}

// Transport Layer

BOOL CBBBSAPTCPDriver::Transact(void)
{
	if( SendFrame() && RecvFrame() ) {

		return FrameOK();
		}

	m_pCtx->m_fOpen = FALSE;

	return FALSE;
	}

UINT CBBBSAPTCPDriver::Send(void)
{
	m_fReqNotDone = FALSE;

	if( !Transact() ) {
		
		return 0;
		}

	if( m_fExtended ) {
		
		return UDPEDT;
		}
	
	return UDPSDT;
	}

BOOL CBBBSAPTCPDriver::SendFrame(void)
{	
	if( TRUE ) {

		memcpy(m_bTx, m_pTxData, m_uPtr);

		m_pTxBuff->StripTail(m_pTxBuff->GetSize() - m_uPtr);

		if( m_fDebug ) {
		
			AfxTrace0("\r\nUDP Send Frame");
		
			AfxTrace0("\r\n  Header:\r\n");
		
			for(UINT k = 0; k < m_uPtr; k++ ) {
			
				if( k == (UINT)m_bTx[0] ) {
				
					AfxTrace0("\r\n   Packet:\r\n");
					}
			
				if( k == (UINT)m_bTx[0] + 11 ) {
				
					AfxTrace0("\r\n  Data:\r\n");
					}
			
				AfxTrace1("[%2.2x]", m_bTx[k]);
				}
			}

		AddTransportHeader();

		for( UINT n = 0; n < 5; n++ ) {

			if( m_pSock->Send(m_pTxBuff) == S_OK ) {

				m_pTxBuff = NULL;

				return TRUE;
				}

			Sleep(5);
			}

		ClearTxBuffer();
		}

	return FALSE;
	}

BOOL CBBBSAPTCPDriver::RecvFrame(void)
{
	memset(m_bRx, 0, sizeof(m_bRx));

	UINT uPtr = 0;

	if( m_fListDebug ) {

		ListRecv();
		}

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {
		
		m_pSock->Recv(m_pRxBuff);

		if( m_pRxBuff ) {

			if( StripTransportHeader() ) {

				if( m_pCtx->m_IP != m_RxMac.m_IP ) {

					ClearRxBuffer();

					continue;
					}

				uPtr = m_pRxBuff->GetSize();

				if( uPtr >= SZHDRSTD && uPtr < elements(m_bRx) ) {

					memcpy(m_pRx, m_pRxBuff->GetData(), uPtr);

					if( m_fListDebug ) {
						
						ListRecvDump(uPtr);
						}

					ClearRxBuffer();

					switch( RcvDone(uPtr) ) {

						case FRDONE: return TRUE;

						case FRERR:  return FALSE;
						}
					}
				}

			ClearRxBuffer();
			}

		Sleep(5);
		}

	if( m_fDebug ) {
		
		AfxTrace0("\r\n\nTIMEOUT\r\n\n");
		}

	return FALSE;
	}

UINT CBBBSAPTCPDriver::RcvDone(UINT uSize)
{
	UINT uCheckSize = GetRcvWord(UDPHSZ);

	if( uCheckSize != SZHDREXT && uCheckSize != SZHDRSTD ) return FRERR;

	UINT uPacketCount = GetRcvWord(UDPPCT);

	while( uPacketCount ) {

		uPacketCount--;

		uCheckSize += GetRcvWord(uCheckSize); // add next packet size

		if( uSize < uCheckSize ) return FRCONT; // not enough received
		}

	return uSize >= uCheckSize ? FRDONE : FRCONT;
	}

BOOL CBBBSAPTCPDriver::FrameOK(void)
{
	DWORD dRxAck = GetRcvDWord(UDPLAK);
	
	DWORD dTxSeq = GetTxDWord (UDPSEQ);

	DWORD dRxSeq = GetRcvDWord(UDPSEQ);

	if( dRxSeq == 0 ) {

		UpdateAppSeq();

		return TRUE;
		}

	if( dRxAck != dTxSeq ) {

		DiscardRetry();

		return FALSE;
		}

	BOOL fTExt = m_bTx[0] == SZHDREXT;
	BOOL fRExt = m_bRx[0] == SZHDREXT;

	if( fTExt && !fRExt ) {

		m_fExtended = FALSE;

		return FALSE; // frame mismatch
		}

	m_fExtended   = m_bRx[UDPFLG] & 0x80;	
	
	m_pCtx->m_dLastAck = dRxSeq;

	UpdateAppSeq();

	m_pCtx->m_fOpen = TRUE;

	return TRUE;
	}

WORD CBBBSAPTCPDriver::GetTxWord(UINT uPos)
{
	return (WORD)IntelToHost((WORD)*(PU2)&m_bTx[uPos]);
	}

DWORD CBBBSAPTCPDriver::GetTxDWord(UINT uPos)
{
	return (DWORD)IntelToHost((DWORD)*(PU4)&m_bTx[uPos]);
	}	

// Tags

BOOL CBBBSAPTCPDriver::LoadTags(LPCBYTE &pData)
{
	if( StoreDevName(pData) ) {

		WORD wCount = GetWord(pData);

		if( wCount ) {

			m_pCtx->m_wTagCount  = wCount;

			m_pCtx->m_fHasTags   = TRUE;

			m_pCtx->m_pdAddrList = new DWORD [wCount + 1];

			for( WORD i = 0; i < wCount; i++ ) {

				m_pCtx->m_pdAddrList[i] = GetLong(pData);
				}

			m_pCtx->m_pdAddrList[wCount] = 0; // end marker

			UINT uTotalSize = GetWord(pData) + wCount; // adding nulls

			m_pCtx->m_pwNameIndex = new WORD [wCount + 1];
			m_pCtx->m_pbNameList  = new BYTE [uTotalSize];

			WORD wNamePosition = 0;

			for( i = 0; i < wCount; i++ ) {

				StoreName(pData, i, &wNamePosition);
				}

			m_pCtx->m_pwNameIndex[wCount] = uTotalSize; // end marker

			m_pCtx->m_pwMSDList = new WORD [wCount + 1];

			m_pCtx->m_pfMSDList = new BOOL [wCount + 1];

			for( i = 0; i < wCount; i++ ) {

				m_pCtx->m_pwMSDList[i] = 0;	
				
				m_pCtx->m_pfMSDList[i] = FALSE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

void CBBBSAPTCPDriver::LoadStringSupport(LPCBYTE &pData)
{
	m_pCtx->m_uStrings = GetWord(pData);

	m_pCtx->m_pStrings = new CStringTag[m_pCtx->m_uStrings];

	for( UINT u = 0; u < m_pCtx->m_uStrings; u++ ) {

		m_pCtx->m_pStrings[u].m_Index = GetWord(pData);

		memset(m_pCtx->m_pStrings[u].m_Text, 0, MAX_CHARS);
		}
	}

BOOL CBBBSAPTCPDriver::StoreDevName(LPCBYTE &pData)
{
	UINT uCount = GetWord(pData); // size of device name

	if( uCount ) {

		m_pCtx->m_pbDevName = new BYTE[uCount];

		memset(m_pCtx->m_pbDevName, 0, uCount);

		BOOL fWide = m_pCtx->m_bIsC3 == 3;

		for( UINT i = 0; i < uCount; i++ ) {

			m_pCtx->m_pbDevName[i] = fWide ? LOBYTE(GetWord(pData)) : GetByte(pData);
			}

		return TRUE;
		}

	return FALSE;
	}

void CBBBSAPTCPDriver::StoreName(LPCBYTE &pData, WORD wItem, PWORD pPosition)
{
	UINT uByteCount	= GetWord(pData);

	if( uByteCount ) {

		WORD wPos	= *pPosition;

		*pPosition	= wPos + uByteCount;

		m_pCtx->m_pwNameIndex[wItem] = wPos; // position of start of name

		BOOL fWide = m_pCtx->m_bIsC3 == 3;

		for( WORD i = 0; i < uByteCount; i++ ) {

			m_pCtx->m_pbNameList[wPos+i] = fWide ? LOBYTE(GetWord(pData)) : GetByte(pData);
			}
		}
	}

// Helpers

BOOL CBBBSAPTCPDriver::HasTags(PDWORD pData, UINT uCount)
{
	if( m_pCtx->m_fHasTags ) return TRUE;

	for( UINT i = 0; i < uCount; i++ ) *pData = 0xFFFFFFFF;

	return FALSE;
	}

BOOL CBBBSAPTCPDriver::HasMSD(AREF Addr, UINT uCount)
{
	BOOL fMSD = FALSE;

	UINT uInc = IsString(Addr.a.m_Table) ? MAX_LTEXT : 1;

	for( UINT u = 0, i = 0; u < uCount; i++, u += uInc ) {

		fMSD = m_pCtx->m_pfMSDList[m_uIndex + i];

		if( !fMSD ) {

			break;
			}
		}

	return fMSD;
	}

void CBBBSAPTCPDriver::UpdateMsgSeq(void)
{
	m_pCtx->m_wMsgSeq = max(m_pCtx->m_wMsgSeq + 1, 1);
	}


void CBBBSAPTCPDriver::UpdateAppSeq(void)
{
	m_pCtx->m_uAppSeq = max(m_pCtx->m_uAppSeq + 1, 1);
	}

WORD CBBBSAPTCPDriver::GetRcvWord(UINT uPos)
{
	WORD x = *((PU2)&m_pRx[uPos]);

	return IntelToHost((WORD)x);
	}

DWORD CBBBSAPTCPDriver::GetRcvDWord(UINT uPos)
{
	DWORD x = *((PU4)&m_pRx[uPos]);

	return IntelToHost((DWORD)x);
	}

BOOL CBBBSAPTCPDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CBBBSAPTCPDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CBBBSAPTCPDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

BOOL CBBBSAPTCPDriver::IsWord(UINT uType)
{
	return uType == addrWordAsWord;
	}

BOOL CBBBSAPTCPDriver::IsLong(UINT uType)
{
	return uType == addrLongAsLong || uType == addrRealAsReal;
	}

BOOL CBBBSAPTCPDriver::IsString(UINT uTable)
{
	return uTable == SPSTR;
	}

BOOL CBBBSAPTCPDriver::IsStringText(UINT uChar)
{
	return (isalpha(uChar) || isdigit(uChar) || ispunct(uChar) || isspace(uChar));
	}

BOOL CBBBSAPTCPDriver::CanAddToRequest(PBYTE pName, UINT uBytes)
{
	WORD wLen  = pName ? (WORD)strlen((const char *)pName) : 0;

	WORD wSize = wLen + m_uPtr + uBytes;

	return wSize < SZDATA;
	}

UINT CBBBSAPTCPDriver::GetDataByteCount(AREF Addr, UINT uCount)
{	
	if( IsString(Addr.a.m_Table) ) {

		MakeMax(uCount, MAX_LTEXT);
		}

	if( IsWord(Addr.a.m_Type) ) {

		return uCount * sizeof(WORD);
		}

	if( IsLong(Addr.a.m_Type) ) {

		return uCount * sizeof(DWORD);
		}

	return uCount;
	}

UINT CBBBSAPTCPDriver::GetItemIndex(AREF Addr)
{
	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	Address.a.m_Offset = FindOffset(Addr, 0);

	for( UINT u = 0; u < m_pCtx->m_wTagCount; u++ ) {

		if( Address.m_Ref == m_pCtx->m_pdAddrList[u] ) {

			if( IsString(Address.a.m_Table) ) {

				UINT uIndex = GetStringContextIndex(Address);

				if( uIndex < NOTHING ) {

					return m_pCtx->m_pStrings[uIndex].m_Index - 1;
					}

				return NOTHING;
				}

			return u;
			}
		}

	return NOTHING;
	}

UINT  CBBBSAPTCPDriver::FindOffset(AREF Addr, UINT uIndex)
{
	if( IsString(Addr.a.m_Table) ) {

		UINT uSize = MAX_LTEXT;

		return Addr.a.m_Offset + (uIndex * uSize) - (Addr.a.m_Offset % uSize);
		}

	return Addr.a.m_Offset + uIndex;
	}

UINT CBBBSAPTCPDriver::GetCount(CAddress Addr, UINT uCount)
{
	if( IsString(Addr.a.m_Table) ) {

		uCount /= MAX_LTEXT;

		if( Addr.a.m_Offset % MAX_LTEXT ) {

			uCount++;
			}

		return uCount;
		}

	return uCount;
	}

UINT CBBBSAPTCPDriver::ExpandCount(CAddress Addr, UINT uCount, UINT uMax)
{
	if( IsString(Addr.a.m_Table) ) {

		uCount *= MAX_LTEXT;

		if( uMax ) {

			return min(uCount, uMax);
			}

		return uCount;
		}

	return uCount;
	}

void CBBBSAPTCPDriver::ClearTxBuffer(void)
{	
	if( m_pTxBuff ) {

		m_pTxBuff->Release();

		m_pTxBuff = NULL;
		}
	}

void  CBBBSAPTCPDriver::ClearRxBuffer(void)
{	
	if( m_pRxBuff ) {

		m_pRxBuff->Release();

		m_pRxBuff = NULL;
		}
	}

// Transport Header

void CBBBSAPTCPDriver::AddTransportHeader(void)
{	
	DWORD IP  = m_pCtx->m_IP;

	WORD Port = WORD(m_pCtx->m_uPort);

	UINT  uSize = 6;

	PBYTE pData = m_pTxBuff->AddHead(uSize);

	pData[0] = PBYTE(&IP)[0];
	pData[1] = PBYTE(&IP)[1];
	pData[2] = PBYTE(&IP)[2];
	pData[3] = PBYTE(&IP)[3];

	pData[4] = HIBYTE(Port);
	pData[5] = LOBYTE(Port);
	}

BOOL CBBBSAPTCPDriver::StripTransportHeader(void)
{
	PBYTE pData = m_pRxBuff->GetData();

	((PBYTE) &m_RxMac.m_IP)[0] = pData[0];
	((PBYTE) &m_RxMac.m_IP)[1] = pData[1];
	((PBYTE) &m_RxMac.m_IP)[2] = pData[2];
	((PBYTE) &m_RxMac.m_IP)[3] = pData[3];

	m_RxMac.m_Port = MAKEWORD(pData[5], pData[4]);

	m_pRxBuff->StripHead(6);

	return TRUE;
	}

// Delete created buffers
void CBBBSAPTCPDriver::CloseDown(void)
{
	if( m_pCtx->m_pbDevName ) {

		delete [] m_pCtx->m_pbDevName;

		m_pCtx->m_pbDevName = NULL;
		}

	if( m_pCtx->m_pdAddrList ) {

		delete [] m_pCtx->m_pdAddrList;

		m_pCtx->m_pdAddrList = NULL;
		}

	if( m_pCtx->m_pbNameList ) {

		delete [] m_pCtx->m_pbNameList;

		m_pCtx->m_pbNameList = NULL;
		}

	if( m_pCtx->m_pwNameIndex ) {

		delete [] m_pCtx->m_pwNameIndex;

		m_pCtx->m_pwNameIndex = NULL;
		}

	if( m_pCtx->m_pwMSDList ) {

		delete [] m_pCtx->m_pwMSDList;

		m_pCtx->m_pwMSDList = NULL;
		}

	if( m_pCtx->m_pfMSDList ) {

		delete [] m_pCtx->m_pfMSDList;

		m_pCtx->m_pfMSDList = NULL;
		}

	if( m_pCtx->m_pStrings ) {

		delete [] m_pCtx->m_pStrings;

		m_pCtx->m_pStrings = NULL;
		}
	}

// List Testing

// Handlers

CCODE CBBBSAPTCPDriver::DoListRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOff = Addr.a.m_Offset;

	UINT uEnd = uOff + uCount;

	if( uEnd > 0xA ) { // count might include a list request

		if( uOff < 0xA ) uCount -= uEnd - 0xA; // read only Off to 9

		else {
			if( uOff <= 0xB ) { // list request, or just List response

				BOOL fB	= uEnd > 0xB; // Read includes response

				m_uListNumber = 0;

				if( uOff == 0xA ) *pData++ = 0;

				if( fB ) *pData = m_dListResp;

				return (CCODE)((uOff == 0xA && fB) ? 2 : 1); 
				}
			}
		}

	return CCODE_ERROR;
	}

CCODE CBBBSAPTCPDriver::DoListWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Offset == 0xA ) {

		if( m_fDebug ) {

			AfxTrace1("\r\n\n>>>>LIST>>>> %8.8lx\r\n", pData[0]);
			}

		Sleep(100);

		MakeListRequest(Addr, pData, uCount);

		return 1;
		}

	if( Addr.a.m_Offset + uCount - 1 >= 0xA ) {

		uCount -= 1;
		}

	return CCODE_ERROR;
	}

// Implementation

void CBBBSAPTCPDriver::MakeListRequest(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame();

	UINT uList  = Addr.a.m_Offset;

	if( uCount ) {

		m_uListNumber = LOBYTE(pData[0]);

		AddReadByListHeader();

		m_pRFrame->uAddr  = uList;
		m_pRFrame->pData  = pData;
		m_pRFrame->uCount = uCount;
		m_pRFrame->uGood  = 0;

		if( AddReadByList() ) {

			DoListRead();
			}
		}

	for( UINT k = 0; k < uCount; k++ ) pData[k] = (DWORD)k;
	}

void CBBBSAPTCPDriver::AddReadByListHeader(void)
{
	AddByte(m_uListEntry ? OFCRDBYLC : OFCRDBYL);
	AddByte(0x01);	// 1 Field to request
	AddByte(0x08);	// Request Names
	AddWord(0x00);	// Version Number
	AddByte(0xFF);	// Security Level
	}

BOOL CBBBSAPTCPDriver::AddReadByList(void)
{
	UINT uCount = 18;

	AddByte(m_uListNumber);

	if( m_uListEntry ) { // initial request does not get entry #

		AddByte(m_uListEntry);

		uCount++;
		}

	m_bTx[14] = LOBYTE(uCount);
	m_bTx[15] = HIBYTE(uCount);

	return TRUE;
	}

BOOL CBBBSAPTCPDriver::DoListRead(void)
{
	READFRAME *p = m_pRFrame;

	PDWORD pData = p->pData;

	UINT uPos    = 0;

	if( (BOOL)(uPos = Send()) ) {

		if( m_bRx[uPos-2] & 0x80 ) {
			
			return FALSE;
			}

		UINT uCount  = min(m_bRx[uPos - 1], p->uCount);

		p->uCount    = uCount;

		if( m_fDebug ) {

			for( UINT i = 0; i < uCount; i++ ) {

				UINT uCt = (UINT)m_pRx[uPos++];

				AfxTrace1("\r\nCt=%d ", uCt);

				for( UINT j = 0; j < uCt; j++ ) {

					AfxTrace1("<%2.2x>", m_pRx[uPos]);

					uPos++;
					}
				}
			}

		m_dListResp = *((PU4)&m_pRx[32]); // approx 3rd byte of name 1

		return TRUE;
		}

	if( m_fDebug ) {

		AfxTrace0("\r\nSend FAIL ");
		}

	return FALSE;
	}

void CBBBSAPTCPDriver::ListRecv(void)
{
	if( m_fDebug ) {

		if( m_uListNumber ) {

			AfxTrace0("\r\nUDP Receive Frame");

			AfxTrace0("\r\n  Header:\r\n");
			}
		}
	}

void CBBBSAPTCPDriver::ListRecvDump(UINT uPtr)
{
	if( m_fDebug ) {

		if( m_uListNumber ) {
							
			for( UINT k = 0; k < uPtr; k++ ) {
								
				if( k == (UINT)m_bRx[0] ) {
									
					AfxTrace0("\r\n   Packet:\r\n");
					}

				if( k == (UINT)m_bRx[0] + 11) {
									
					AfxTrace0("\r\n  Data:\r\n");
					}
							
				AfxTrace1("<%2.2x>", m_bRx[k] );
				}
			}
		}
	}

// End of File
