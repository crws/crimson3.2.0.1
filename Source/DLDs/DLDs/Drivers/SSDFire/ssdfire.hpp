
//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef float C2REAL;

typedef int   C2INT;

//////////////////////////////////////////////////////////////////////////
//
// Conversion Macros
//

#define R2I(x) *((C2INT  *) &x)

#define I2R(x) *((C2REAL *) &x)

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CSSDFireDriver;

class CSSDFireHandler;

//////////////////////////////////////////////////////////////////////////
//
// Request Types
//

enum
{
	reqNone		= 0,
	reqRead		= 1,
	reqWrite	= 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Codes from Card
//

enum
{
	codeUp		= 1,
	codeDown	= 2,
	codeData	= 3,
	codeAck		= 4,
	codeDead	= 5
	};

//////////////////////////////////////////////////////////////////////////
//
// Rx Machine States
//

enum
{
	stateIdle	= 0,
	stateSelfHi	= 1,
	stateSelfLo	= 2,
	stateCount	= 3,
	stateData	= 4,
	stateSkip	= 5,
	stateAck	= 6,
	};

//////////////////////////////////////////////////////////////////////////
//
// SSD Drives via FireWire Driver
//

class CSSDFireDriver: public CMasterDriver
{
	public:
		// Constructor
		CSSDFireDriver(void);

		// Destructor
		~CSSDFireDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
			
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE       m_bDrop;
			};
		
		// Data Members
		CSSDFireHandler * m_pHandler;
		CContext        * m_pCtx;
	};

//////////////////////////////////////////////////////////////////////////
//
// SSD Drives via FireWire Handler
//

class CSSDFireHandler : public IPortHandler
{
	public:
		// Constructor
		CSSDFireHandler(IHelper *pHelper);
		
		// Destructor
		~CSSDFireHandler(void);

		// App Calls
		BOOL IsKnown(BYTE bDrop);
		UINT GetData(BYTE bDrop, DWORD dwAddr, UINT uCount, PDWORD pData);
		UINT SetData(BYTE bDrop, DWORD dwAddr, UINT uCount, PDWORD pData);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Binding
		void MCALL Bind(IPortObject *pPort);

		// Event Handlers
		void MCALL OnOpen(CSerialConfig const &Config);
		void MCALL OnClose(void);
		void MCALL OnTimer(void);
		BOOL MCALL OnTxData(BYTE &bData);
		void MCALL OnTxDone(void);
		void MCALL OnRxData(BYTE bData);
		void MCALL OnRxDone(void);

		
	protected:
		// Helper
		IHelper *m_pHelper;
		
		// Port Object
		IPortObject * m_pPort;
		CSerialConfig m_Config;

		// Data Members
		ULONG   m_uRefs;
		IEvent *m_pEvent;
		WORD	m_ARP[256];
		BYTE    m_bRxData[64];
		BYTE    m_bTxData[64];
		BYTE    m_bSvData[64];
		DWORD	m_dwData;
		UINT	m_uState;
		WORD	m_wSelf;
		BOOL	m_fLink;
		BOOL	m_fDead;
		UINT    m_uRxCount;
		UINT    m_uRxPtr;
		UINT    m_uTxCount;
		UINT    m_uTxPtr;
		BYTE	m_bTxSeq;
		UINT    m_uSvCount;
		UINT    m_uSvPtr;
		BYTE	m_bAck;
		UINT	m_uReq;
		BOOL	m_fOkay;

		// Frame Assembly
		void NewReq(WORD dest, BYTE tcode);
		void NewRep(PDWORD pq, BYTE tcode);
		void AddByte(BYTE b);
		void AddWord(WORD w);
		void AddQuad(DWORD q);
		void SendPacket(void);
		void PushTx(void);
		void PullTx(void);

		// Link Events
		void OnLinkDown(void);
		void OnLinkUp(void);
		void OnData(void);
		void OnAck(void);

		// Frame Events
		void OnSelfID(void);
		void OnIsoFrame(void);
		void OnPhyFrame(void);
		void OnAsyncFrame(void);

		// App Layer
		void OnReadQuad(void);
		void OnWriteQuad(void);
		void OnReadReply(void);

		// Data Access
		DWORD GetQuadData(WORD hi, DWORD lo);
		void  SetQuadData(WORD hi, DWORD lo, DWORD qd);

		// ARP Helpers
		void ClearARP(void);
		void AddARP(BYTE bDrop, WORD wID);
		WORD GetID(BYTE bDrop);

		// Implementation
		void CreateEvent(void);

		// Debug
		void AfxTrace(PCTXT pText, ...);
	};
		
// End of File
