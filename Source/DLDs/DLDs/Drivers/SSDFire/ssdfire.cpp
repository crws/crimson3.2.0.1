
#include "intern.hpp"

#include "ssdfire.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SSD Drives via FireWire Driver
//

// Instantiator

INSTANTIATE(CSSDFireDriver);

// Constructor

CSSDFireDriver::CSSDFireDriver(void)
{
	m_Ident	= DRIVER_ID;
	}

// Destructor

CSSDFireDriver::~CSSDFireDriver(void)
{
	}

// Configuration

void MCALL CSSDFireDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CSSDFireDriver::CheckConfig(CSerialConfig &Config)
{
	}
	
// Management

void MCALL CSSDFireDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CSSDFireHandler(m_pHelper);

	pPort->Bind(m_pHandler);
	}

void MCALL CSSDFireDriver::Detach(void)
{
	m_pHandler->Release();
	}

void MCALL CSSDFireDriver::Open(void)
{	
	}

// Device

CCODE MCALL CSSDFireDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx          = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS; 
	}

CCODE MCALL CSSDFireDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CSSDFireDriver::Ping(void)
{
	if( m_pHandler->IsKnown(m_pCtx->m_bDrop) ) {

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CSSDFireDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD dwData = 0;

	if( m_pHandler->GetData(m_pCtx->m_bDrop, Addr.a.m_Offset, 1, &dwData) ) {

		if( Addr.a.m_Type == addrLongAsReal ) {

			C2REAL r = LONG(dwData);

			r        = r / C2REAL(65536);

			pData[0] = R2I(r);
			}
		else
			pData[0] = dwData;

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CSSDFireDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD dwData = pData[0];	

	if( Addr.a.m_Type == addrLongAsReal ) {

		C2REAL r = I2R(dwData);

		r        = r * C2REAL(65536);

		dwData   = DWORD(C2INT(r));
		}

	if( m_pHandler->SetData(m_pCtx->m_bDrop, Addr.a.m_Offset, 1, &dwData) ) {

		return 1;
		}

	return CCODE_ERROR;
	}

//////////////////////////////////////////////////////////////////////////
//
// SSD Drives via FireWire Handler
//

// Constructor

CSSDFireHandler::CSSDFireHandler(IHelper *pHelper)
{
	StdSetRef();

	m_pHelper = pHelper;

	m_pPort   = NULL;

	m_fDead   = FALSE;

	CreateEvent();
	}
		
// Destructor

CSSDFireHandler::~CSSDFireHandler(void)
{
	m_pEvent->Release();
	}

// App Calls

BOOL CSSDFireHandler::IsKnown(BYTE bDrop)
{
	if( m_fDead ) {

		AfxTrace("LINK DEAD\n");

		m_pPort->Close();

		m_pPort->Open (m_Config);

		return FALSE;
		}

	return m_fLink && GetID(bDrop);
	}

UINT CSSDFireHandler::GetData(BYTE bDrop, DWORD dwAddr, UINT uCount, PDWORD pData)
{
	WORD ID;

	if( m_fLink && (ID = GetID(bDrop)) ) {

		m_uReq = reqRead;

		NewReq(ID, 0x04);

		AddWord(0x0000);

		AddQuad(0xF0020000 + 4 * (dwAddr-1));

		SendPacket();

		if( m_pEvent->Wait(500) ) {

			if( m_fOkay ) {

				pData[0] = m_dwData;

				return 1;
				}

			return 0;
			}

		m_uReq = reqNone;
		}

	return 0;
	}

UINT CSSDFireHandler::SetData(BYTE bDrop, DWORD dwAddr, UINT uCount, PDWORD pData)
{
	WORD ID;

	if( m_fLink && (ID = GetID(bDrop)) ) {

		m_uReq = reqWrite;

		NewReq(ID, 0x00);

		AddWord(0x0000);

		AddQuad(0xF0020000 + 4 * (dwAddr-1));

		AddQuad(pData[0]);

		SendPacket();

		if( m_pEvent->Wait(500) ) {

			if( m_fOkay ) {

				return 1;
				}

			return 0;
			}

		m_uReq = reqNone;
		}

	return 0;
	}

// IUnknown

HRESULT CSSDFireHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG CSSDFireHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG CSSDFireHandler::Release(void)
{
	StdRelease();
	}

// Binding

void MCALL CSSDFireHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
	}

// Event Handlers

void MCALL CSSDFireHandler::OnOpen(CSerialConfig const &Config)
{
	m_Config  = Config;

	m_uState  = stateIdle;

	m_fLink   = FALSE;

	m_fDead   = FALSE;

	m_bTxSeq  = 0;

	m_uReq    = reqNone;

	ClearARP();
	}

void MCALL CSSDFireHandler::OnClose(void)
{
	m_fLink = FALSE;
	}

void MCALL CSSDFireHandler::OnTimer(void)
{
	}

BOOL MCALL CSSDFireHandler::OnTxData(BYTE &bData)
{
	if( m_uTxPtr < m_uTxCount ) {
		
		bData = m_bTxData[m_uTxPtr++];

		return TRUE;
		}

	return FALSE;
	}

void MCALL CSSDFireHandler::OnTxDone(void)
{
	}

void MCALL CSSDFireHandler::OnRxData(BYTE bData)
{
	switch( m_uState ) {

		case stateIdle:

			switch( bData ) {

				case codeUp:

					if( m_fLink ) {

						m_fLink = FALSE;

						OnLinkDown();
						}

					m_uState = stateSelfHi;

					break;

				case codeDown:

					if( m_fLink ) {

						OnLinkDown();

						m_fLink = FALSE;
						}

					break;

				case codeData:

					m_uState = stateCount;

					break;

				case codeAck:

					m_uState = stateAck;

					break;

				case codeDead:

					m_fDead = TRUE;

					break;
				}
			
			break;

		case stateSelfHi:

			m_wSelf  = (bData << 8);

			m_uState = stateSelfLo;

			break;

		case stateSelfLo:

			m_wSelf |= bData;

			m_fLink  = TRUE;

			m_uState = stateIdle;

			OnLinkUp();

			break;

		case stateCount:

			if( (m_uRxCount = 4 * bData) ) {

				m_uRxPtr = 0;

				if( m_uRxCount > elements(m_bRxData) ) {

					m_uState = stateSkip;
					}
				else
					m_uState = stateData;
				}
			else
				m_uState = stateIdle;
			
			break;

		case stateData:

			m_bRxData[m_uRxPtr] = bData;

			if( ++m_uRxPtr == m_uRxCount ) {

				m_uState = stateIdle;

				OnData();
				}
			break;

		case stateSkip:

			if( ++m_uRxPtr == m_uRxCount ) {

				m_uState = stateIdle;
				}
			break;

		case stateAck:

			m_bAck   = bData;

			m_uState = stateIdle;

			OnAck();

			break;
		}
	}

void MCALL CSSDFireHandler::OnRxDone(void)
{
	}

// Frame Assembly

void CSSDFireHandler::NewReq(WORD dest, BYTE tcode)
{
	m_uTxCount = 0;

	AddByte(codeData);

	AddByte(0);

	AddWord(0);

	BYTE tlab = (++m_bTxSeq & 0x3F);

	BYTE rt   = 0x01;

	AddWord((tlab << 10) | (rt << 8) | (tcode << 4));

	AddWord(dest);
	}

void CSSDFireHandler::NewRep(PDWORD pq, BYTE tcode)
{
	m_uTxCount = 0;

	AddByte(codeData);

	AddByte(0);

	AddWord(0);

	BYTE tlab = ((pq[0] >> 10) & 0x3F);

	BYTE rt   = 0x01;

	AddWord((tlab << 10) | (rt << 8) | (tcode << 4));

	AddWord(HIWORD(pq[1]));
	}

void CSSDFireHandler::AddByte(BYTE b)
{
	m_bTxData[m_uTxCount++] = b;
	}

void CSSDFireHandler::AddWord(WORD w)
{
	AddByte(HIBYTE(w));
	
	AddByte(LOBYTE(w));
	}

void CSSDFireHandler::AddQuad(DWORD q)
{
	AddWord(HIWORD(q));
	
	AddWord(LOWORD(q));
	}

void CSSDFireHandler::SendPacket(void)
{
	m_bTxData[1] = (m_uTxCount - 2) / 4;

	m_uTxPtr     = 1;

	m_pPort->Send(m_bTxData[0]);
	}

void CSSDFireHandler::PushTx(void)
{
	m_uSvCount = m_uTxCount;

	m_uSvPtr   = m_uTxPtr;

	memcpy(m_bSvData, m_bTxData, 32);
	}

void CSSDFireHandler::PullTx(void)
{
	m_uTxCount = m_uSvCount;

	m_uTxPtr   = m_uSvPtr;

	memcpy(m_bTxData, m_bSvData, 32);
	}

// Link Events

void CSSDFireHandler::OnLinkDown(void)
{
	AfxTrace("LINK DOWN\n");
	
	ClearARP();
	}

void CSSDFireHandler::OnLinkUp(void)
{
	AfxTrace("\n\nLINK UP (%4.4X)\n", m_wSelf);
	}

void CSSDFireHandler::OnData(void)
{
	PDWORD pq = PDWORD(m_bRxData);

	if( (pq[0] >> 30) == 2 ) {

		OnSelfID();
		}
	else {
		BYTE tcode = ((pq[0] >> 4) & 0x0F);

		switch( tcode ) {

			case 0x0A:

				OnIsoFrame();

				break;

			case 0x0E:
			
				OnPhyFrame();

				break;

			default:

				OnAsyncFrame();

				break;
			}
		}
	}

void CSSDFireHandler::OnAck(void)
{
	if( m_uReq == reqWrite ) {

		if( m_bAck == 1 ) {

			m_fOkay = TRUE;

			m_uReq  = reqNone;
			}
		else {
			// LATER -- Handle write replies?

			m_fOkay = FALSE;

			m_uReq  = reqNone;
			}

		m_pEvent->Set();
		}
	}

// Frame Events

void CSSDFireHandler::OnSelfID(void)
{
	}

void CSSDFireHandler::OnIsoFrame(void)
{
	PDWORD pq = PDWORD(m_bRxData);

	if( LOWORD(pq[1]) == 0x0000 ) {

		if( pq[2] == 0x5E000001 ) {

			if( LOWORD(pq[3]) == 0x806 ) {

				if( LOWORD(pq[5]) == 0x0002 ) {

					WORD src = HIWORD(pq[ 1]);

					BYTE id  = LOBYTE(pq[10]);

					AddARP(id, src);

					AfxTrace("Drop %2.2u : ID = %4.4X\n", id, src);
					}
				}
			}
		}
	}

void CSSDFireHandler::OnPhyFrame(void)
{
	}

void CSSDFireHandler::OnAsyncFrame(void)
{
	PDWORD pq   = PDWORD(m_bRxData);

	WORD   dest = HIWORD(pq[0]);

	WORD   src  = HIWORD(pq[1]);

	if( dest == m_wSelf || dest == 0xFFFF ) {

		BYTE tcode = ((pq[0] >> 4) & 0x0F);

		switch( tcode ) {

			case 0x00:

				OnWriteQuad();

				break;

			case 0x04:

				OnReadQuad();
				
				break;

			case 0x06:

				OnReadReply();

				break;
			}
		}
	}

// App Layer

void CSSDFireHandler::OnReadQuad(void)
{
	PDWORD pq   = PDWORD(m_bRxData);
	
	WORD   dest = HIWORD(pq[0]);

	WORD   src  = HIWORD(pq[1]);
	       
	WORD   hi   = LOWORD(pq[1]);

	DWORD  lo   = pq[2];

	DWORD  qd   = GetQuadData(hi, lo);
	
	PushTx();

	NewRep(pq, 0x06);

	AddWord(0);

	AddQuad(0);

	AddQuad(qd);

	SendPacket();

	PullTx();
	}

void CSSDFireHandler::OnWriteQuad(void)
{
	PDWORD pq   = PDWORD(m_bRxData);
	
	WORD   dest = HIWORD(pq[0]);

	WORD   src  = HIWORD(pq[1]);

	WORD   hi   = LOWORD(pq[1]);

	DWORD  lo   = pq[2];

	DWORD  qd   = pq[3];

	SetQuadData(hi, lo, qd);
	}

void CSSDFireHandler::OnReadReply(void)
{
	if( m_uReq == reqRead ) {

		PDWORD pq    = PDWORD(m_bRxData);

		BYTE   tlab  = ((pq[0] >> 10) & 0x3F);
		
		BYTE   rcode = ((pq[1] >> 12) & 0x0F);

		if( tlab == (m_bTxSeq & 0x3F) ) {

			if( rcode == 0 ) {

				m_dwData = pq[3];

				m_fOkay  = TRUE;

				m_uReq   = reqNone;
				}
			else {
				m_fOkay  = FALSE;

				m_uReq   = reqNone;
				}

			m_pEvent->Set();
			}
		}
	}

// Data Access

DWORD CSSDFireHandler::GetQuadData(WORD hi, DWORD lo)
{
	AfxTrace("Get %4.4X-%8.8X\n", hi, lo);

	if( hi == 0xFFFF ) {

		switch( lo ) {

			// LATER -- What really goes here?

			case 0xF0000400: return 0x0104AAAA;
			case 0xF0000404: return 0x31333934;
			case 0xF0000408: return 0x40646000;
			case 0xF000040C: return 0x00000000;
			case 0xF0000410: return 0x00000000;
			}
		}

	return 0;
	}

void CSSDFireHandler::SetQuadData(WORD hi, DWORD lo, DWORD qd)
{
	AfxTrace("Set %4.4X-%8.8X\n", hi, lo);
	}

// ARP Helpers

void CSSDFireHandler::ClearARP(void)
{
	memset(m_ARP, 0, sizeof(m_ARP));
	}

void CSSDFireHandler::AddARP(BYTE bDrop, WORD wID)
{
	m_ARP[bDrop] = wID;
	}

WORD CSSDFireHandler::GetID(BYTE bDrop)
{
	return m_ARP[bDrop];
	}

// Implementation

void CSSDFireHandler::CreateEvent(void)
{
	ISyncHelper *pSync = NULL;

	m_pHelper->MoreHelp(IDH_SYNC, (void **) &pSync);

	m_pEvent = pSync->CreateAutoEvent();

	pSync->Release();
	}

// Debug

void CSSDFireHandler::AfxTrace(PCTXT pText, ...)
{
	#if 0

	va_list pArgs;

	va_start(pArgs, pText);

	m_pHelper->AfxTrace(pText, pArgs);

	va_end(pArgs);

	#endif
	}

// End of File
