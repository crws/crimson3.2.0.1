
#include "intern.hpp"

#include "srtpmtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// GE TCP/IP Master via SRTP Driver
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CGeSrtpTCPMaster);

// Constructor

CGeSrtpTCPMaster::CGeSrtpTCPMaster(void)
{
	m_Ident = DRIVER_ID;
	
	m_uKeep = 0;

	}

// Destructor

CGeSrtpTCPMaster::~CGeSrtpTCPMaster(void)
{
	}

// Configuration

void MCALL CGeSrtpTCPMaster::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CGeSrtpTCPMaster::Attach(IPortObject *pPort)
{
	}

void MCALL CGeSrtpTCPMaster::Open(void)
{
	}

// Device

CCODE MCALL CGeSrtpTCPMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
		       	m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_bTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();
			m_pCtx->m_fAux   = FALSE;
			m_pCtx->m_IP2	 = GetAddr(pData);
			m_pCtx->m_fDirty = FALSE;

			m_pCtx->m_fInit = FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CGeSrtpTCPMaster::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CGeSrtpTCPMaster::Ping(void)
{	
	if( m_pCtx->m_fPing ) {
		
		DWORD IP;

		if( !m_pCtx->m_fAux ) {

			IP = m_pCtx->m_IP;
			}
		else 
			IP = m_pCtx->m_IP2;
			
		if( CheckIP(IP, m_pCtx->m_uTime2) == NOTHING )  {
			
			if( m_pCtx->m_IP2 ) {
				
				m_pCtx->m_fAux = !m_pCtx->m_fAux;
				}
			
			return CCODE_ERROR;
			}
		}

	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = 'R';
		Addr.a.m_Offset = 1;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;

		return DoWordRead(Addr, Data, 1);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CGeSrtpTCPMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	UINT uType = Addr.a.m_Type;

	if( uType == addrWordAsWord ) {

		return DoWordRead(Addr, pData, min(uCount, 32));
		}

	if( uType == addrBitAsBit ) {

		return DoBitRead(Addr, pData, min(uCount, 256));
		}

	if( uType == addrWordAsLong || uType == addrWordAsReal ) {

		return DoLongRead(Addr, pData, min(uCount, 16));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CGeSrtpTCPMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	if( IsWritable(Addr.a.m_Table) ) {

		UINT uType = Addr.a.m_Type;

		if( uType == addrWordAsWord ) {

			return DoWordWrite(Addr, pData, min(uCount, 32));
			}

		if( uType == addrBitAsBit ) {

			return DoBitWrite(Addr, pData, min(uCount, 48));
			}

		if( uType == addrWordAsLong || uType == addrWordAsReal ) {

			return DoLongWrite(Addr, pData, min(uCount, 16));
			}
		}

	if( IsReadOnly(Addr.a.m_Table) ) {

		return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

UINT MCALL CGeSrtpTCPMaster::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx  = (CContext *) pContext;
	
	if(uFunc == 1 || uFunc == 5 ) {
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);
					
					return 0;
					}
				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {
				
				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}
		
		if( uFunc == 1 )  {
			
			pCtx->m_IP = MotorToHost(dwValue);
			}	
		
		if( uFunc == 5 )  {
			
			pCtx->m_IP2 = MotorToHost(dwValue);
			}	
		
		pCtx->m_fDirty = TRUE;
		
		Free(pText);
			
		return 1;    
		} 

	if( uFunc == 4 )  {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP);
		}

	if( uFunc == 6 )  {

		// Get Current IP Address
		
		return MotorToHost(pCtx->m_IP2);
		}

	if( uFunc == 7 )  {

		// Get Fallback Status

		return pCtx->m_fAux;
		}
	
	return 0;
	}

// Socket Management

BOOL CGeSrtpTCPMaster::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {
		 
			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}
			
			return TRUE;
			}
		
		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CGeSrtpTCPMaster::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		m_pCtx->m_fAux   = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		UINT   uPort;

		if( !m_pCtx->m_fAux )  {
			
			IP   = (IPADDR const &) m_pCtx->m_IP;
			
		        uPort = m_pCtx->m_uPort;
			}

		else {
			IP    = (IPADDR const &) m_pCtx->m_IP2;
			
			uPort = m_pCtx->m_uPort;
			}
		
		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			if( m_pCtx->m_IP2 ) {

				m_pCtx->m_fAux = !m_pCtx->m_fAux;
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		
		if( m_pCtx->m_IP2 ) {

			m_pCtx->m_fAux = !m_pCtx->m_fAux;
			}
		
		return FALSE;
		}

	return FALSE;
	}

void CGeSrtpTCPMaster::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

void CGeSrtpTCPMaster::StartFrame()
{
	m_uPtr = 0;

	}

void CGeSrtpTCPMaster::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {
	
		m_bTxBuff[m_uPtr] = bData;

		m_uPtr++;
		}
	}

void CGeSrtpTCPMaster::AddRepeatedByte(int uCount, BYTE bData)
{
	if( uCount > 0 ) {

		while( uCount-- ) {

			AddByte(bData);
			}
		}
	}

void CGeSrtpTCPMaster::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CGeSrtpTCPMaster::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void CGeSrtpTCPMaster::AddHeader(BYTE bBegin, UINT uBytes, UINT uOp, BYTE bLong)
{
	StartFrame();

	AddByte(bBegin);

	AddByte(0x00);

	AddByte(++m_pCtx->m_bTrans);

	AddWord(uBytes);

	AddRepeatedByte(4, 0x00);

	AddByte(bLong);

	AddRepeatedByte(7, 0x00);

	AddByte(0x01);

	AddRepeatedByte(12, 0x00);

	AddByte(m_pCtx->m_bTrans);

	AddByte(uOp);

	AddRepeatedByte(4, 0x00);

	AddByte(0x10);

	AddByte(0x0E);

	AddWord(0x00);

	AddRepeatedByte(2, 0x01);
	
	}

void CGeSrtpTCPMaster::AddRegSpec(UINT uTable, UINT uAddr, UINT uCount, UINT uType)
{
	AddByte( GetTypeID(uTable, uType) );

	WORD wAddr = uAddr - 1;

	SwapBytes(wAddr);

	AddWord(wAddr);

	WORD wCount = uCount;

	SwapBytes(wCount);

	AddWord(wCount);

	}

void CGeSrtpTCPMaster::EndFrame(void)
{
	AddRepeatedByte(DATA_MIN - m_uPtr, 0x00);
	
	}

void CGeSrtpTCPMaster::SwapBytes(WORD &wData)
{
	BYTE Hi = HIBYTE(wData);
	BYTE Lo = LOBYTE(wData);

	wData = MAKEWORD(Hi, Lo);

	}

		
// Transport Layer

BOOL CGeSrtpTCPMaster::SendFrame(void)
{
	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CGeSrtpTCPMaster::RecvFrame(void)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 8 ) {

				if( m_bRxBuff[0] == 0x05 ) {

					return FALSE;
					}

				UINT uTotal = MAKEWORD( m_bRxBuff[4], m_bRxBuff[3]) + DATA_MIN;

				if( uPtr >= uTotal ) {

					if( m_bRxBuff[2] == m_bTxBuff[2] ) {

						return TRUE;
						}
					
					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRxBuff[n] = m_bRxBuff[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CGeSrtpTCPMaster::Transact(void)
{
	if( SendFrame() && RecvFrame() ) {

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CGeSrtpTCPMaster::CheckFrame(void)
{
	return TRUE;
	}

BOOL CGeSrtpTCPMaster::EstablishSession(void)
{
	AddHeader(0x08, 0x00, OP_SING, 0x01);

	AddByte(CODE_ESTAB);

	AddByte(0x01);

	EndFrame();

	return Transact();
	
	}

BOOL CGeSrtpTCPMaster::ChangeAccessLevel(void)
{ 
	AddHeader(0x02, 0x00, OP_SING, 0x01);

	AddByte(CODE_ACCESS);

	AddByte(0x02);

	EndFrame();

	return Transact();
	
	}

// Initialization

CCODE CGeSrtpTCPMaster::DoInit(void)
{
	StartFrame();

	AddRepeatedByte(DATA_MIN, 0x00);

	if( Transact() ) {

		if( EstablishSession() && ChangeAccessLevel() ) {

			m_pCtx->m_fInit = TRUE;

			return CCODE_SUCCESS;
			}
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Read Handlers

CCODE CGeSrtpTCPMaster::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_fInit ) {
		
		if( DoInit() != CCODE_SUCCESS ) {

			return CCODE_ERROR;
			}
		}

	AddHeader(0x02, 0x00, OP_SING, 0x01);

	AddByte(CODE_READ);

	AddRegSpec(Addr.a.m_Table, Addr.a.m_Offset, uCount, Addr.a.m_Type);

	EndFrame();
	
	if( Transact() ) {

		UINT uOffset = IsSmall(uCount) ? DATA_SMALL : DATA_OFFSET;

		for( UINT n = 0; n < uCount; n++ ) {

			WORD x = PU2(m_bRxBuff + (n * 2) + uOffset)[0];

			pData[n] = LONG(SHORT(IntelToHost(x)));
		
			}

		return uCount;
		}

	m_pCtx->m_fInit = FALSE;
		
	return CCODE_ERROR;
	}

CCODE CGeSrtpTCPMaster::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_fInit ) {
		
		if( DoInit() != CCODE_SUCCESS ) {

			return CCODE_ERROR;
			}
		}

	AddHeader(0x02, 0x00, OP_SING, 0x01);

	AddByte(CODE_READ);

	AddRegSpec(Addr.a.m_Table, Addr.a.m_Offset, uCount * 2, Addr.a.m_Type);

	EndFrame();
	
	if( Transact() ) {

		UINT uOffset = IsSmall(uCount * 2) ? DATA_SMALL : DATA_OFFSET;

		for( UINT n = 0; n < uCount; n++ ) {

			DWORD x = PU4(m_bRxBuff + (n * 4) + uOffset)[0];

			pData[n] = IntelToHost(x);
		
			}

		return uCount;
		}

	m_pCtx->m_fInit = FALSE;
		
	return CCODE_ERROR;
	}

CCODE CGeSrtpTCPMaster::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_fInit ) {
		
		if( DoInit() != CCODE_SUCCESS ) {

			return CCODE_ERROR;
			}
		}

	AddHeader(0x02, 0x00, OP_SING, 0x01);

	AddByte(CODE_READ);

	UINT uOffset = ((Addr.a.m_Offset - 1) % 8);

	AddRegSpec(Addr.a.m_Table, Addr.a.m_Offset - uOffset, uOffset + uCount, Addr.a.m_Type);

	EndFrame();

	if( Transact() ) {

		UINT b = 0;

		BYTE m = 1;

		m <<= uOffset;

		uOffset = (uOffset + uCount) <= 48 ? DATA_SMALL: DATA_OFFSET;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRxBuff[b + uOffset] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	m_pCtx->m_fInit = FALSE;

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CGeSrtpTCPMaster::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_fInit ) {
		
		if( DoInit() != CCODE_SUCCESS ) {

			return CCODE_ERROR;
			}
		}

	WORD Bytes = 0x00;

	BYTE Long = 0x01;

	BOOL fSmall = IsSmall(uCount);

	if( !fSmall ) {

		Bytes = uCount * 2;

		Long = 0x02;
		}

	AddHeader(0x02, Bytes, fSmall ? OP_SING : OP_TEXT, Long);

	if( !fSmall ) {

		SwapBytes(Bytes);

		AddWord(Bytes);

		AddRepeatedByte(4, 0x00);

		AddRepeatedByte(2, 0x01);
		}

	AddByte(CODE_WRITE);

	AddRegSpec(Addr.a.m_Table, Addr.a.m_Offset, uCount, Addr.a.m_Type);

	for( UINT n = 0; n < uCount; n++ ) {
				
		WORD x = LOWORD(pData[n]);

		AddByte(LOBYTE(x));

		AddByte(HIBYTE(x));
		}
	
	EndFrame();

	if( Transact() ) {

		return uCount;
		}

	m_pCtx->m_fInit = FALSE;

	return CCODE_ERROR;
	}

CCODE CGeSrtpTCPMaster::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_fInit ) {
		
		if( DoInit() != CCODE_SUCCESS ) {

			return CCODE_ERROR;
			}
		}

	WORD Bytes = 0x00;

	BYTE Long = 0x01;

	BOOL fSmall = IsSmall(uCount * 2);

	if( !fSmall ) {

		Bytes = uCount * 4;

		Long = 0x02;
		}

	AddHeader(0x02, Bytes, fSmall ? OP_SING : OP_TEXT, Long);

	if( !fSmall ) {

		SwapBytes(Bytes);

		AddWord(Bytes);

		AddRepeatedByte(4, 0x00);

		AddRepeatedByte(2, 0x01);
		}

	AddByte(CODE_WRITE);

	AddRegSpec(Addr.a.m_Table, Addr.a.m_Offset, uCount * 2, Addr.a.m_Type);

	for( UINT n = 0; n < uCount; n++ ) {
				
		DWORD x = pData[n];

		WORD lo = LOWORD(x);

		WORD hi = HIWORD(x);

		AddByte(LOBYTE(lo));

		AddByte(HIBYTE(lo));

		AddByte(LOBYTE(hi));

		AddByte(HIBYTE(hi));
		}
	
	EndFrame();

	if( Transact() ) {

		return uCount;
		}

	m_pCtx->m_fInit = FALSE;

	return CCODE_ERROR;
	}

CCODE CGeSrtpTCPMaster::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_fInit ) {
		
		if( DoInit() != CCODE_SUCCESS ) {

			return CCODE_ERROR;
			}
		}

	WORD Bytes = 0x00;

	BYTE Long = 0x01;

	BOOL fSmall = IsSmall( uCount / 16 );

	if( !fSmall ) {

		Bytes = uCount / 8;

		Long = 0x02;
		}
	
	AddHeader(0x02, Bytes, fSmall ? OP_SING : OP_TEXT, Long);

	if( !fSmall ) {

		SwapBytes(Bytes);

		AddWord(Bytes);

		AddRepeatedByte(4, 0x00);

		AddRepeatedByte(2, 0x01);
		}

	AddByte(CODE_WRITE);

	AddRegSpec(Addr.a.m_Table, Addr.a.m_Offset, uCount, Addr.a.m_Type);

	UINT b = 0;

	BYTE m = 1;
	
	m <<= ((Addr.a.m_Offset - 1) % 8);

	for( UINT n = 0; n < uCount; n++ ) {

		if( pData[n] ) {
					
			b |= m;
			}

		if( !(m <<= 1) ) {

			AddByte(b);

			b = 0;

			m = 1;
			}
		}

	if( m > 1 ) {

		AddByte(b);

		}
	

	EndFrame();

	if( Transact() ) {

		return uCount;
		}

	m_pCtx->m_fInit = FALSE;

	return CCODE_ERROR;
	}

BOOL CGeSrtpTCPMaster::IsWritable(UINT uTable)
{
	switch( uTable ) {

		case 'R':
		case 'A':
		case 'B':
		case 'I':
		case 'M':
		case 'Q':
		case 'T':
		case 'E':
		case 'D':
		case 'C':
		case 'G':
			return TRUE;
		}

	return FALSE;
	}

BOOL CGeSrtpTCPMaster::IsReadOnly(UINT uTable)
{
	switch( uTable ) {

		case 'S':
			return TRUE;
		}

	return FALSE;
	}

BYTE CGeSrtpTCPMaster::GetTypeID(UINT uTable, UINT uType)
{
	BOOL fBit = ( uType == addrBitAsBit ) ? TRUE : FALSE;

	switch( uTable ) {
	
		case 'Q':		return fBit ? 0x48 : 0x12;
		case 'T':		return fBit ? 0x4A : 0x14;
		case 'M':		return fBit ? 0x4C : 0x16;
		case 'B':		return fBit ? 0xFF : 0x0C;
		case 'R':		return fBit ? 0xFF : 0x08;
		case 'S':		return fBit ? 0x54 : 0x1E;
		case 'E':		return fBit ? 0x4E : 0x18;
		case 'D':		return fBit ? 0x50 : 0x1A;
		case 'C':		return fBit ? 0x52 : 0x1C;
		case 'I':		return fBit ? 0x46 : 0x10;
		case 'A':		return fBit ? 0xFF : 0x0A;
		case 'G':		return fBit ? 0x56 : 0x38;
		}
	
	return 0xFF;
	}

BOOL CGeSrtpTCPMaster::IsSmall(UINT uCount)
{
	return (uCount < 4);
	}

//Helpers

BOOL  CGeSrtpTCPMaster::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL  CGeSrtpTCPMaster::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL  CGeSrtpTCPMaster::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
