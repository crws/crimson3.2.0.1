//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// BETA LaserMike LaserSpeed Final Length Mode Slave Driver
//

class CBMikeLSFinalLenSlaveDriver : public CSlaveDriver
{
	public:
		// Constructor
		CBMikeLSFinalLenSlaveDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
		// Entry Points
		DEFMETH(void) Service(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);

	protected:
		BYTE   m_bRx  [300];
		BYTE   m_bTx  [300];
		UINT   m_uPtr;
		BOOL   m_fTF;

		// Implementation
		DWORD GetReal(void);
		DWORD GetDec(void);
		BYTE  FromAscii(BYTE bByte);
		
		// Transport Layer
		virtual BOOL Send(void);
		virtual BOOL RecvFrame(void);
	};

// End of File
