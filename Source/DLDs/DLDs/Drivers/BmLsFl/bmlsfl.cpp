#include "intern.hpp"

#include "bmlsfl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Final Length Mode Slave Driver
//

// Instantiator

INSTANTIATE(CBMikeLSFinalLenSlaveDriver);

// Constructor

CBMikeLSFinalLenSlaveDriver::CBMikeLSFinalLenSlaveDriver(void)
{
	m_Ident  = DRIVER_ID;

	m_fTF    = FALSE;

	m_uPtr   = 0;
	}

// Config

void MCALL CBMikeLSFinalLenSlaveDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		return;
		}
	}

void MCALL CBMikeLSFinalLenSlaveDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, FALSE);
	}

// Management

void MCALL CBMikeLSFinalLenSlaveDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

// Entry Points

void MCALL CBMikeLSFinalLenSlaveDriver::Service(void)
{
	for(;;) {

		if( !m_fTF ) {

			m_uPtr = 0;

			m_bTx[m_uPtr++] = '.';
			m_bTx[m_uPtr++] = CR;

			Send();

			Sleep(20);

			m_uPtr = 0;

			m_bTx[m_uPtr++] = 'T';
			m_bTx[m_uPtr++] = 'F';
			m_bTx[m_uPtr++] = CR;

			Send();
			}

		if( RecvFrame() ) {

			DWORD dwFL[1]; 
			DWORD dwQF[1]; 

			dwFL[0] = GetReal();
			dwQF[0] = GetDec();

			CAddress Addr;

			Addr.a.m_Table = addrNamed;
			Addr.a.m_Extra = 0;
			Addr.a.m_Offset = 2;
			Addr.a.m_Type = addrRealAsReal;

			Write(Addr, dwFL, 1);

			Addr.a.m_Offset++;
			Addr.a.m_Type = addrByteAsByte;

			Write(Addr, dwQF, 1); 

			m_fTF = TRUE;
			}

		ForceSleep(20);
		}
	}

// Implementation

DWORD CBMikeLSFinalLenSlaveDriver::GetReal(void)
{
	float dwFloat = 0.0;

	BOOL fNeg = FALSE;

	BOOL fDP  = FALSE;

	UINT uDiv = 10;

	for( m_uPtr = 0; m_bRx[m_uPtr] != CR && m_uPtr < sizeof(m_bRx); m_uPtr++ ) {

		if( m_bRx[m_uPtr] == '-' ) {

			fNeg = TRUE;

			continue;
			}

		if( m_bRx[m_uPtr] == '.' ) {

			fDP = TRUE;

			continue;
			}

		if( m_bRx[m_uPtr] == ',' ) {		

			break;
			}

		BYTE bByte = m_bRx[m_uPtr] - 0x30;

		if( bByte <= 9 ) {

			if( !fDP ) {

				dwFloat *= 10;

				dwFloat += bByte;
				}
			else {
				float Add = float(bByte) / float(uDiv);
				
				dwFloat += Add;

				uDiv *= 10;
				}
	   		}
		}

	if( fNeg ) {

		dwFloat = -dwFloat;
		}

	return R2I(dwFloat);
	}

DWORD CBMikeLSFinalLenSlaveDriver::GetDec(void)
{
	BOOL fNeg = FALSE;

	DWORD dwData = 0;

	while( m_bRx[m_uPtr] != CR && m_uPtr < sizeof(m_bRx) ) {

		if( m_bRx[m_uPtr] == '+' ) {

			m_uPtr++;

			continue;
			}

		if( m_bRx[m_uPtr] == '-' ) {

			fNeg = TRUE;

			m_uPtr++;

			continue;
			}

		if( m_bRx[m_uPtr] == '=' ) {

			dwData = 0;
			
			m_uPtr++;

			continue;
			}

		if( m_bRx[m_uPtr] == ',' ) {

			m_uPtr++;

			continue;
			}

		BYTE bByte = FromAscii(m_bRx[m_uPtr]);

		if( bByte < 10 ) {

			dwData = dwData * 10;

			dwData += FromAscii(m_bRx[m_uPtr]);
			}

		m_uPtr++;
		}

	return fNeg ? -dwData : dwData;
	}

BYTE CBMikeLSFinalLenSlaveDriver::FromAscii(BYTE bByte)
{
	if( bByte >= '0' ) {
	
		if( bByte <= '9' ) {

			return bByte - '0';
			}

		return	bByte - '@' + 9;
		}

	return bByte;
	}

// Transport

BOOL CBMikeLSFinalLenSlaveDriver::Send(void)
{
	m_pData->ClearRx();
	
	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CBMikeLSFinalLenSlaveDriver::RecvFrame(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_uPtr = 0;

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {

		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		m_bRx[m_uPtr] = uData;

		m_uPtr++;

		if( uData == CR ) {

			return TRUE;
			}	
		}

	return FALSE;
	}




// End of File
