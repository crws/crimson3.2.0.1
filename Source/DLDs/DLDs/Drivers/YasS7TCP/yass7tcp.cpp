

#include "intern.hpp"

#include "yass7tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 TCP Driver
//

// Instantiator

INSTANTIATE(CYaskawaS7TCPDriver);

// Constructor

CYaskawaS7TCPDriver::CYaskawaS7TCPDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CYaskawaS7TCPDriver::~CYaskawaS7TCPDriver(void)
{
	}

// Configuration

void MCALL CYaskawaS7TCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CYaskawaS7TCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CYaskawaS7TCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CYaskawaS7TCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_uUnit  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();

			m_pCtx->m_uErrCom = 0;
			m_pCtx->m_uErrVal = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskawaS7TCPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskawaS7TCPDriver::Ping(void)
{
	if( OpenSocket() ) {

		DWORD    Data[1];
	
		CAddress Addr;

		Addr.a.m_Table  = SPA;
		Addr.a.m_Offset = 0x100;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;
	
		return DoWordRead(Addr, Data, 1);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CYaskawaS7TCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	if( !m_pCtx->m_uUnit ) return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;

	if( Addr.a.m_Type == addrBitAsBit ) { // Write only

		pData[0] = 0;

		return 1;
		}

	switch( Addr.a.m_Table ) {

		case SPEC:	*pData = m_pCtx->m_uErrCom;	return 1;
		case SPEV:	*pData = m_pCtx->m_uErrVal;	return 1;
		}

	CAddress A;

	A.a.m_Table = Addr.a.m_Table;
	A.a.m_Extra = 0;
	A.a.m_Type  = Addr.a.m_Type;

	A.a.m_Offset = SetAddress(Addr, &uCount);

	if( A.a.m_Offset || Addr.a.m_Table == SPDA ) {

		switch( Addr.a.m_Type ) {

			case addrWordAsWord:	return DoWordRead(A, pData, min(uCount, 8));

			case addrWordAsReal:
			case addrWordAsLong:	return DoLongRead(A, pData, min(uCount, 4));
			}
		}

	return uCount; // unspecified parameters
	}

CCODE MCALL CYaskawaS7TCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	switch( Addr.a.m_Table ) {

		case SPEC:
		case SPEV:	m_pCtx->m_uErrCom = 0; return 1;
		}

	CAddress A;

	A.a.m_Table = Addr.a.m_Table;
	A.a.m_Extra = 0;
	A.a.m_Type  = Addr.a.m_Type;

	A.a.m_Offset = SetAddress(Addr, &uCount);

	if( A.a.m_Offset || Addr.a.m_Table == SPDA ) {

		switch( A.a.m_Type) {

			case addrBitAsBit:
			case addrWordAsWord:	return DoWordWrite(A, pData, min(uCount, 16));

			case addrRealAsReal:
			case addrLongAsLong:	return DoLongWrite(A, pData, min(uCount,  8));
			}
		}

	return uCount; // unspecified parameters
	}

// Implementation

// Socket Management

BOOL CYaskawaS7TCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaS7TCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CYaskawaS7TCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

void CYaskawaS7TCPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(LOBYTE(m_pCtx->m_uUnit));
	
	AddByte(bOpcode);
	}

void CYaskawaS7TCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) m_bTx[m_uPtr++] = bData;
	}

void CYaskawaS7TCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CYaskawaS7TCPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CYaskawaS7TCPDriver::PutFrame(void)
{
	m_bTx[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CYaskawaS7TCPDriver::GetFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

//**/			AfxTrace0("\r\n"); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("<%2.2x>", m_bRx[k]);

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRx[5];

				if( uPtr >= uTotal ) {

					if( m_bRx[0] == m_bTx[0] ) {

						if( m_bRx[1] == m_bTx[1] ) {

							memcpy(m_bRx, m_bRx + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRx[n] = m_bRx[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}	

	return FALSE;
	}

BOOL CYaskawaS7TCPDriver::Transact(void)
{
	if( PutFrame() ) {
			
		if( GetFrame() ) {
			
			return CheckReply();
			}
		}
		
	return FALSE;
	}

BOOL CYaskawaS7TCPDriver::CheckReply(void)
{
	if( m_bRx[0] == m_bTx[6] ) {
	
		if( !(m_bRx[7] & 0x80) ) return TRUE;

		m_pCtx->m_uErrCom = *( (PWORD) &m_bTx[8] );
		m_pCtx->m_uErrVal = UINT(m_bRx[8]);
		}
		
	return FALSE;
	}

// Read Handlers

CCODE CYaskawaS7TCPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(3);
		
	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount);

	if( Transact() ) {

		uCount = min( uCount, m_bRx[2]/sizeof(WORD) );

		PWORD p = (WORD *)&m_bRx[3];
		
		for( UINT n = 0; n < uCount; n++, p++ ) {
		
			WORD x = *(PU2)p;
			
			pData[n] = LONG(SHORT(MotorToHost((WORD)x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CYaskawaS7TCPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(3);
		
	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount * 2);

	if( Transact() ) {

		uCount = min(uCount, (m_bRx[2]+2)/sizeof(DWORD));

		PDWORD p = PDWORD(&m_bRx[3]);
		
		for( UINT n = 0; n < uCount; n++, p++ ) {
		
			DWORD x = *(PU4)p;
			
			pData[n] = MotorToHost((DWORD)x);
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CYaskawaS7TCPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uType  = Addr.a.m_Type;

	BOOL fIsBit = uType == addrBitAsBit;

	BOOL fSingle = fIsBit || (uType == addrWordAsWord && uCount == 1);

	StartFrame(fSingle ? 6 : 0x10);

	AddWord(Addr.a.m_Offset);

	if( !fSingle ) {

		AddWord(uCount);

		AddWord(uCount*2);
		}

	else {
		if( fIsBit ) {

			if( *pData ) {

				*pData = 0;

				uCount = 1;
				}

			else return 1;
			}
		}

	for(UINT n = 0; n < uCount; n++ ) AddWord(LOWORD(pData[n]));

	return Transact() ? uCount : CCODE_ERROR;
	}

CCODE CYaskawaS7TCPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(0x10);

	AddWord(Addr.a.m_Offset);

	AddWord(uCount * 2);

	AddWord(uCount*4);

	for(UINT n = 0; n < uCount; n++ ) AddLong(pData[n]);

	return Transact() ? uCount : CCODE_ERROR;
	}

// Connected
CCODE CYaskawaS7TCPDriver::LoopBack(void)
{
	m_uPtr = 0;

	UINT uUnit = m_pCtx->m_uUnit;

	m_pCtx->m_uUnit = 1;

	StartFrame(8);

	AddWord(0);
	AddWord(0xA537);

	m_pCtx->m_uUnit = uUnit;

	return Transact() ? 1 : CCODE_ERROR;
	}

UINT  CYaskawaS7TCPDriver::SetAddress(AREF Addr, UINT * pCount)
{
#define AMAX	12

	UINT	o = Addr.a.m_Offset; // config address

	UINT	b[AMAX];	// base modbus address
	UINT	lo[AMAX];	// low config address for range
	UINT	hi[AMAX];	// high config address for range

	for( UINT c = 0; c < AMAX; c++ ) { // Preset compare arrays

		b[c]  = 0;
		lo[c] = (c * 100) + 101;
		hi[c] = lo[c] + 98;
		}

	c = 0;

	switch( Addr.a.m_Table ) {

		case SPDA:
			return Addr.a.m_Offset;

		case SPEN:
		case SPAC:
			*pCount = 1;
			return Addr.a.m_Table == SPEN ? 0x8FF : 0x90F;

		case SPA:
			b[c]	= 0x100;
			lo[c]	= 100;
			hi[c]	= 105;
			b[++c]	= 0x106;
			break;

		case SPB:
			b[c]	= 0x180;
			hi[c]	= 108;
			b[++c]	= 0x189;
			hi[c]	= 208;
			b[++c]	= 0x191;
			hi[c]	= 314;
			b[++c]	= 0x1A3;
			hi[c]	= 402;
			b[++c]	= 0x1A5;
			hi[c]	= 517;
			b[++c]	= 0x1DC;
			lo[c]	= 518;
			hi[c]	= 519;
			b[++c]	= 0x1B6;
			lo[c]	= 601;
			hi[c]	= 604;
			b[++c]	= 0x1CA;
			lo[c]	= 701;
			hi[c]	= 702;
			b[++c]	= 0x1CC;
			lo[c]	= 801;
			hi[c]	= 806;
			b[++c]	= 0x1DA;
			lo[c]	= 901;
			break;

		case SPC:
			b[c]	= 0x200;
			hi[c]	= 111;
			b[++c]	= 0x20B;
			hi[c]	= 204;
			b[++c]	= 0x20F;
			hi[c]	= 305;
			b[++c]	= 0x215;
			hi[c]	= 405;
			b[++c]	= 0x21B;
			hi[c]	= 508;
			b[++c]	= 0x223;
			break;

		case SPD:
			b[c]	= 0x280; // 101 - 109
			hi[c]	= 109;
			b[++c]	= 0x28B; // 110 - 117
			lo[c]	= 110;
			hi[c]	= 117;
			b[++c]	= 0x289; // 201 - 202
			lo[c]	= 201;
			hi[c]	= 202;
			b[++c]	= 0x293; // 203
			lo[c]	= 203;
			hi[c]	= 203;
			b[++c]	= 0x294; // 301 - 304
			lo[c]	= 301;
			hi[c]	= 304;
			b[++c]	= 0x298; // 401 - 402
			lo[c]	= 401;
			hi[c]	= 402;
			b[++c]	= 0x29A; // 501 - 506
			lo[c]	= 501;
			hi[c]	= 506;
			b[++c]	= 0x2A0; // 601 - 606
			lo[c]	= 601;
			break;

		case SPE:
			b[c]	= 0x300;
			hi[c]	= 113;
			b[++c]	= 0x30E;
			hi[c]	= 211;
			b[++c]	= 0x328;
			lo[c]	= 212;
			hi[c]	= 212;
			b[++c]	= 0x319;
			lo[c]	= 301;
			hi[c]	= 308;
			b[++c]	= 0x321;
			lo[c]	= 401;
			break;

		case SPF:
			b[c]	= 0x380;
			hi[c]	= 114;
			b[++c]	= 0x38F;
			hi[c]	= 201;
			b[++c]	= 0x390;
			hi[c]	= 301;
			b[++c]	= 0x391;
			hi[c]	= 408;
			b[++c]	= 0x399;
			hi[c]	= 509;
			b[++c]	= 0x3A2;
			break;

		case SPH:
			b[c]	= 0x400;
			hi[c]	= 106;
			b[++c]	= 0x40B;
			hi[c]	= 205;
			b[++c]	= 0x410;
			hi[c]	= 312;
			b[++c]	= 0x41D;
			hi[c]	= 408;
			b[++c]	= 0x425;
			hi[c]	= 507;
			b[++c]	= 0x42C;
			break;

		case SPL:
			b[c]	= 0x480;
			hi[c]	= 105;
			b[++c]	= 0x485;
			hi[c]	= 208;
			b[++c]	= 0x48F;
			hi[c]	= 310;
			b[++c]	= 0x4C7;
			lo[c]	= 311;
			hi[c]	= 312;
			b[++c]	= 0x499;
			lo[c]	= 401;
			hi[c]	= 405;
			b[++c]	= 0x4C2;
			lo[c]	= 406;
			hi[c]	= 406;
			b[++c]	= 0x49E;
			lo[c]	= 501;
			hi[c]	= 502;
			b[++c]	= 0x4A1;
			lo[c]	= 601;
			hi[c]	= 606;
			b[++c]	= 0x4A7;
			lo[c]	= 701;
			hi[c]	= 706;
			b[++c]	= 0x4C9;
			lo[c]	= 707;
			hi[c]	= 707;
			b[++c]	= 0x4AD;
			lo[c]	= 801;
			break;

		case SPN:
			b[c]	= 0x580;
			hi[c]	= 102;
			b[++c]	= 0x584;
			hi[c]	= 203;
			b[++c]	= 0x588;
			break;

		case SPO:
			b[c]	= 0x500;
			hi[c]	= 105;
			b[++c]	= 0x505;
			hi[c]	= 214;
			b[++c]	= 0x515;
			break;

		case SPP:
			b[c]	= 0x600;
			hi[c]	= 110;
			b[++c]	= 0x60A;
			hi[c]	= 210;
			b[++c]	= 0x614;
			break;

		case SPT:
			b[c]	= 0x700;
			lo[c]	= 100;
			hi[c]	= 108;
			break;

		case SPU:
			b[c]	= 0x40;
			hi[c]	= 130;
			b[++c]	= 0x3C;
			lo[c]	= 131;
			hi[c]	= 131;
			b[++c]	= 0x5F;
			lo[c]	= 132;
			hi[c]	= 145;
			b[++c]	= 0x720;
			lo[c]	= 190;
			hi[c]	= 199;
			b[++c]	= 0x80;
			lo[c]	= 201;
			hi[c]	= 214;
			b[++c]	= 0x90;
			lo[c]	= 301;
			hi[c]	= 308;
			b[++c]	= 0x804;
			lo[c]	= 309;
			hi[c]	= 314;
			b[++c]	= 0x80E;
			lo[c]	= 315;
			break;
		}

	for( UINT i = 0; i <= c; i++ ) {

		if( o >= lo[i] && o <= hi[i] ) {

			if( o + *pCount - 1 > hi[i] ) *pCount = hi[i] - o + 1;

			return b[i] + o - lo[i] - 1; // note -1, different from serial
			}

		if( o > hi[i] && o < lo[i+1] ) {

			*pCount = min( *pCount, lo[i+1] - o );

			return 0; // in between ranges
			}
		}

	return 0;
	}

// End of File
