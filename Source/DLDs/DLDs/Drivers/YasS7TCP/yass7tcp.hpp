

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 TCP Driver
//

class CYaskawaS7TCPDriver : public CMasterDriver
{
	public:
		// Constructor
		CYaskawaS7TCPDriver(void);

		// Destructor
		~CYaskawaS7TCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BYTE	 m_uUnit;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			UINT	 m_uErrCom;
			UINT	 m_uErrVal;	 
			};

		// Data Members
		CContext * m_pCtx;

		BYTE	m_bTx[300];
		BYTE	m_bRx[300];
		UINT	m_uPtr;
		UINT	m_uKeep;
				
		// Implementation

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		
		// Transport Layer
		BOOL	PutFrame(void);
		BOOL	GetFrame(void);
		BOOL	Transact(void);
		BOOL	CheckReply(void);

		// Read Handlers
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Connected
		CCODE	LoopBack(void);

		// Address Conversion
		UINT	SetAddress(AREF Addr, UINT * pCount);
	};

// Space Table
#define	SPDA	1
#define	SPA	2
#define	SPB	3
#define	SPC	4
#define	SPD	5
#define	SPE	6
#define	SPF	7
#define	SPH	8
#define	SPL	9
#define	SPN	10
#define	SPO	11
#define	SPP	12
#define	SPT	13
#define	SPU	14
#define	SPEN	19
#define	SPAC	20
#define	SPEC	30
#define	SPEV	31

// End of File
