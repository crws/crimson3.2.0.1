
#include "intern.hpp"

#include "modmona.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus ASCII Monitor Driver
//

// Instantiator

INSTANTIATE(CModbusMonitorASCII);

// Constructor

CModbusMonitorASCII::CModbusMonitorASCII(void)
{
	m_Ident     = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Config

void MCALL CModbusMonitorASCII::Load(LPCBYTE pData)
{
	}

void MCALL CModbusMonitorASCII::CheckConfig(CSerialConfig &Config)
{	
	Make485(Config, FALSE);
	}

void MCALL CModbusMonitorASCII::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

// Entry Points

void MCALL CModbusMonitorASCII::Service(void)
{
	AllocBuffers();

	for(;;) {

		if( !GetFrame() ) {

			continue;
			}

		m_dDrop = (DWORD(m_pRx[0])) << 16;

		switch( m_pRx[1] ) {

			case 0x01:
				HandleRead(3);
				break;

			case 0x03:
				HandleRead(2);
				break;
					
			case 0x04:
				HandleRead(1);
				break;
					
			case 0x05:
				HandleSingleWrite(3);
				break;
					
			case 0x06:
				HandleSingleWrite(2);
				break;
					
			case 0xF:
				HandleMultiWrite(3);
				break;
					
			case 0x10:
				HandleMultiWrite(2);
				break;

			default:
				break;
			}
		}
	}

// Frame Handlers

BOOL CModbusMonitorASCII::HandleRead(UINT uTable)
{
	if( m_uRcvSize == 8 ) {

		m_uReadAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

		m_uReadCount = MotorToHost(PWORD(m_pRx + 4)[0]);

		if( GetFrame() && m_uRcvSize != 8 ) {

			if( m_dDrop == (DWORD( m_pRx[0] ) << 16) ) {

				if( m_uRcvSize == (5 + unsigned(m_pRx[2]) ) ) {

					HandleMultiWrite( uTable + 100 );
					}
				}
			}
		}

	return TRUE;
	}

BOOL CModbusMonitorASCII::HandleMultiWrite(UINT uTable)
{
	UINT uAddr;
	UINT uCount;
	UINT uStart = 7;

	if( uTable > 100 ) { // Read function

		uAddr  = m_uReadAddr;

		uCount = m_uReadCount;

		uStart = 3;

		uTable -= 100;
		}

	else {
		if( m_uRcvSize == 8 ) {

			return TRUE;
			}

		uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

		uCount = MotorToHost(PWORD(m_pRx + 4)[0]);
		}

	if( uCount > 125 || uAddr + uCount >  0xFFFF ) {

		return FALSE;
		}
	else {
		PDWORD pWork = new DWORD [ uCount ];

		CAddress Addr;

		Addr.a.m_Table  = uTable;
		Addr.a.m_Offset = 1 + uAddr;

		if( uTable != 3 ) {

			Addr.a.m_Type = addrLongAsLong;

			for( UINT n = 0; n < uCount; n++ ) {

				pWork[n] = HostToMotor(PWORD(m_pRx + uStart)[n]);

				pWork[n] |= m_dDrop;
				}
			}

		else {
			Addr.a.m_Type = addrBitAsLong;

			UnpackBits( pWork, uCount );
			}

		if( COMMS_SUCCESS(Write(Addr, pWork, uCount)) ) {

			delete pWork;

			return TRUE;
			}

		delete pWork;

		return FALSE;
		}
	}

BOOL CModbusMonitorASCII::HandleSingleWrite(UINT uTable)
{
	UINT  uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	DWORD dwData = MotorToHost(PWORD(m_pRx + 4)[0]);

	CAddress Addr;

	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Table  = uTable;
	Addr.a.m_Offset = 1 + uAddr;

	if( uTable == 3 ) {

		Addr.a.m_Type = addrBitAsLong;

		if( !LOWORD(dwData) ) {

			dwData = 0;
			}

		else {

			if( LOWORD(dwData) == 0xFF00 ) {
				
				dwData = 1;
				}

			else {
				return FALSE;
				}
			}
		}

	dwData |= m_dDrop;

	if( COMMS_SUCCESS(Write(Addr, &dwData, 1)) ) {

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CModbusMonitorASCII::AllocBuffers(void)
{
	m_uTxSize = 512;
	
	m_uRxSize = 512;
	
	m_pTx = New BYTE [ m_uTxSize ];

	m_pRx = New BYTE [ m_uRxSize ];
	}
	
BOOL CModbusMonitorASCII::IsHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return TRUE;
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return TRUE;
		}
		
	return FALSE;
	}

WORD CModbusMonitorASCII::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return bData - '0';
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return bData - 'A' + 10;
		}
		
	return 0;
	}

// Port Access

// Frame Building
		
// Transport Layer

BOOL CModbusMonitorASCII::GetFrame(void)
{
	return AsciiRx();
	}

BOOL CModbusMonitorASCII::AsciiRx(void)
{
	UINT uState = 0;

	m_uRcvSize   = 0;

	BYTE bCheck = 0;

	while( TRUE ) {

		UINT uByte;
		
		if( (uByte = m_pData->Read(200)) == NOTHING ) {

			uState = 0;
			
			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == ':' ) {

					m_uRcvSize   = 0;

					uState = 1;

					bCheck = 0;
					}
				break;
				
			case 1:
				if( IsHex(uByte) ) {
				
					m_pRx[m_uRcvSize] = FromHex(uByte) << 4;
				
					uState = 2;
					}
				else {
					if( uByte == CR ) {
					
						continue;
						}

					if( uByte == LF ) {
												
						if( bCheck == 0 ) {

							return TRUE;
							}
						}
					
					return FALSE;
					}
				break;
				
			case 2:
				if( IsHex(uByte) ) {

					m_pRx[m_uRcvSize] |= FromHex(uByte);
					
					bCheck += m_pRx[m_uRcvSize];
					
					if( ++m_uRcvSize == m_uRxSize ) {
						
						return FALSE;
						}
					
					uState = 1;
					}
				else
					return FALSE;
				break;
			}
		}
		
	return FALSE;
	}

// Response Helper

void CModbusMonitorASCII::UnpackBits(PDWORD pWork, UINT uCount)
{
	UINT b = 7;

	BYTE m = 1;

	for( UINT n = 0; n < uCount; n++ ) {

		pWork[n] = ( m_pRx[b] & m ) ? 1 : 0;

		pWork[n] |=  m_dDrop;

		if( !(m <<= 1) ) {

			b++;

			m = 1;
			}
		}
	}

// End of File
