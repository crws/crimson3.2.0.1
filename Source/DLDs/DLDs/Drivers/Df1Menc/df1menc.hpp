
//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "../Df1Shared/df1mtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Status Codes
//

#define	repFailed	0

#define	repOkay		1

#define	repBusy		2

/////////////////////////////////////////////////////////////////////////
//
// DF1 TCP Master Driver
//

class CEncDF1TCPMaster : public CDF1TCPMaster 
{
	public:
		// Constructor
		CEncDF1TCPMaster(void);

		// Destructor
		~CEncDF1TCPMaster(void);

		// Configuration
		void MCALL Load(LPCBYTE pData);

		// Device
		CCODE MCALL DeviceOpen(IDevice *pDevice);
		CCODE MCALL DeviceClose(BOOL fPersist);

		// Device Data
		struct CEncContext : CDF1TCPMaster::CContext
		{
			UINT m_uTimeD;
			UINT m_uTimeA;
			};

	protected:

		// Data Members
		CEncContext * m_pEnc;
		
		BOOL m_fHalf;
		BOOL m_fCRC;
		BOOL m_fNull;
		BYTE m_pTx[300];
		BYTE m_pRx[300];
		UINT m_uTx;
		UINT m_uRx;
		UINT m_uSize;

		void Put(BYTE b);
		UINT Get(UINT uTime);

		// Transport Layer
		BOOL Transact(void);
		BOOL TxFrame(void);
		BOOL TxFrameData(void);
		BOOL RxFrame(void);
		UINT RxCheck(void);
		BOOL CatchWrite(void);
		BOOL Send(void);

		// Checksum Handler
		void ClearCheck(void);
		void AddToCheck(BYTE b);
		void AddInitToCheck(void);
		void AddTermToCheck(void);
		void SendCheck(void);
		
		// Data Link Layer
		void TxAck (void);
		void TxNak (void);
		void TxEnq (void);
		void TxPoll(void);
		UINT RxAck (void);

		// Helpers
		void GetCount(UINT uSpace, UINT &uCount);
	};

// End of File
