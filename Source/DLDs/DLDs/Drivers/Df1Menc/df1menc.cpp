
#include "intern.hpp"

#include "df1menc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Base Class
//

#include "../Df1Shared/df1mtcp.cpp"

//////////////////////////////////////////////////////////////////////////
//
// Encapsulated DF1 TCP Master Driver
//

// Instantiator

INSTANTIATE(CEncDF1TCPMaster);

// Constructor

CEncDF1TCPMaster::CEncDF1TCPMaster(void)
{
	m_Ident = DRIVER_ID;
	
	m_pEnc  = NULL;

	m_uKeep = 0;

	m_fHalf = FALSE;

	m_fCRC  = FALSE;

	m_fNull = FALSE;

	m_uTx   = 0;

	m_uRx	= 0;

	m_uSize = 0;
	}

// Destructor

CEncDF1TCPMaster::~CEncDF1TCPMaster(void)
{
	}

// Configuration

void MCALL CEncDF1TCPMaster::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bSrc  = GetByte(pData);

		m_fHalf = GetByte(pData);

		m_fCRC  = GetByte(pData);
		}
	}

// Device

CCODE MCALL CEncDF1TCPMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pEnc = (CEncContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pEnc  = new CEncContext;

			m_pCtx  = m_pEnc;

			m_pBase = m_pEnc;

			m_pEnc->m_uHeader = headBase;
			m_pEnc->m_bDevice = GetByte(pData);
			m_pEnc->m_IP	  = GetAddr(pData);
			m_pEnc->m_uPort   = GetWord(pData);
			m_pEnc->m_bDest   = GetByte(pData);
			m_pEnc->m_fKeep   = GetByte(pData);
			m_pEnc->m_fPing   = GetByte(pData);
			m_pEnc->m_uTime1  = GetWord(pData);
			m_pEnc->m_uTime2  = GetWord(pData);
			m_pEnc->m_uTime3  = GetWord(pData);
			m_pEnc->m_uTimeD  = GetWord(pData);
			m_pEnc->m_uTimeA  = GetWord(pData);
			m_pEnc->m_wTrans  = WORD(RAND(m_pCtx->m_bDevice + 1));
			m_pEnc->m_pSock   = NULL;
			m_pEnc->m_uLast   = GetTickCount();

			m_pEnc->m_uCache  = 0;
		
			pDevice->SetContext(m_pEnc);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx  = m_pEnc;

	m_pBase = m_pEnc;
       
	return CCODE_SUCCESS;
	}

CCODE MCALL CEncDF1TCPMaster::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pEnc->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pEnc;

		m_pEnc = NULL;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

	
// Implementation

void CEncDF1TCPMaster::Put(BYTE b)
{
	m_pTx[m_uTx] = b;

	m_uTx++;
	}

UINT CEncDF1TCPMaster::Get(UINT uTime)
{
	if( m_uSize > 0 ) {

		m_uRx++;

		m_uSize--;
		
		return m_pRx[m_uRx - 1];
		}

	m_uRx   = 0;

	m_uSize = sizeof(m_pRx) - m_uRx;

	m_pEnc->m_pSock->Recv(m_pRx + m_uRx, m_uSize);

	if( m_uSize ) {

		m_uRx++;

		m_uSize--;

		return m_pRx[m_uRx - 1];
		}

	return NOTHING;
	}

BOOL CEncDF1TCPMaster::Send(void)
{
	CBuffer *pBuff = CreateBuffer(1280, TRUE);    

	if( pBuff ) {

		memcpy(pBuff->AddTail(m_uTx), m_pTx, m_uTx);

		if( m_pEnc->m_pSock->Send(pBuff) == S_OK ) {
			
			return TRUE;
			}

		pBuff->Release();
		}
	
	return FALSE;
	}

// Transport Layer

BOOL CEncDF1TCPMaster::Transact(void)
{
	DF1HEADBASE &TxHead = (DF1HEADBASE &) m_bTxBuff[0];

	DF1HEADBASE &RxHead = (DF1HEADBASE &) m_bRxBuff[0]; 

	for(;;) {

		TxFrame();

		for(;;) {

			if( m_fHalf ) {

				for( UINT i = 0;; ) {

					TxPoll();
		
					if( RxFrame() ) {

						if( !m_fNull ) {

							if( CatchWrite() ) {

								continue;
								}

							break;
							}

						Sleep(50);
						}

					if( i++ < 4 ) {

						continue;
						}

					return FALSE;
					}
				}
			else {
				if( !RxFrame() ) {
			
					return FALSE;
					}
				}

			if( RxHead.wTrans == TxHead.wTrans ) {

				switch( RxCheck() ) {
				
					case repOkay:
						
					  	return TRUE;
						
					case repFailed:
						
						return FALSE;
					}
					
				break;
				}
			}
		}

	AfxTouch(RxHead);

	return TRUE;
	}

BOOL CEncDF1TCPMaster::TxFrame(void)
{
	if( m_fHalf ) {

		for( int i = 0; i < 4; i++ ) {
		
			TxFrameData();

			for( int j = 0; j < 4; j++ ) {

				switch( RxAck() ) {

					case ACK:
						return TRUE;

					case NAK:
						j = 4;
						break;

					case NOTHING:
						j = 4;
						break;
					}
				}
			}

		return FALSE;
		}
	else {
		for( int i = 0; i < 2; i++ ) {
		
			TxFrameData();

			for( int j = 0; j < 4; j++ ) {
				
				switch( RxAck() ) {
				
					case ACK:	
						return TRUE;
						
					case NAK:
						j = 4;
						break;
						
					case STX:
						TxAck();
						break;
					}
			
				if( j < 3 ) {

					TxEnq();
					}
				}
			}

		return FALSE;
		}
	}

BOOL CEncDF1TCPMaster::TxFrameData(void)
{
	ClearCheck();

	if( m_fHalf ) {

		Put(DLE);
		Put(SOH);

		if( m_pEnc->m_bDest == DLE ) {

			Put(DLE);
			}

		BYTE b = m_pEnc->m_bDest;

		Put(b);

		AddToCheck(b);

		AddInitToCheck();
		}

	Put(DLE);
	Put(STX);

	for( UINT n = 0; n < m_uPtr; n++ ) {
	
		BYTE b = m_bTxBuff[n];

		if( b == DLE ) {

			Put(DLE);
			}

		Put(b);

		AddToCheck(b);
		}

	Put(DLE);
	Put(ETX);
	
	AddTermToCheck();
	
	SendCheck();

	Send();

	return TRUE;
	}
	
BOOL CEncDF1TCPMaster::RxFrame(void)
{
	BOOL fDLE   = FALSE;
	
	UINT uState = 0;
	
	UINT uCount = 0;
	
	UINT uRetry = 2;

	UINT uTimer = 0;

	UINT uData  = 0;
	
	WORD wCheck = 0;

	SetTimer(m_pEnc->m_uTimeD);

	while( (uTimer = GetTimer()) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {
			
			continue;
			}

		if( uData == DLE ) {

			if( m_uRx == 1 ) {	

				uState = 0;

				fDLE = FALSE;
				}
		
			if( !fDLE ) {
			
				if( uState < 2 ) {
				
					fDLE = TRUE;
					
					continue;
					}
				}
			else
				fDLE = FALSE;
			}
			
		switch( uState ) {
		
			case 0:
				if( fDLE ) {

					if( m_fHalf && uData == EOT ) {

						m_fNull = TRUE;

						return TRUE;
						}
				
					if( uData == ENQ ) {

						TxAck();
						}
						
					if( uData == STX ) {
					
						ClearCheck();

						m_fNull = FALSE;

						uCount  = 0;
						
						uState  = 1;
						}
					}
				break;
				
			case 1:
				if( fDLE ) {
				
					if( uData == ETX ) {
					
						AddTermToCheck();
						
						if( m_fCRC ) {

							uState = 2;
							}
						else
							uState = 3;
						}
					else
						return FALSE;
					}
				else {
					m_bRxBuff[uCount++] = uData;
					
					if( uCount == sizeof(m_bRxBuff) ) {

						return FALSE;
						}
						
					AddToCheck(uData);
					}
				break;
				
			case 2:
				wCheck = uData;
				
				uState = 3;
				
				break;
				
			case 3:
				if( m_fCRC ) {
				
					wCheck += (uData << 8);
				
					if( wCheck == m_CRC.GetValue() ) {
				
						TxAck();

						return TRUE;
						}
					}
				else {
					AddToCheck(uData);
				
					if( !m_bCheck ) {
				
						TxAck();

						return TRUE;
						}
					}

				if( uRetry-- ) {
					
					TxNak();
						
					SetTimer(m_pEnc->m_uTimeD);
						
					uState = 0;
					}
				else {
					TxAck();
					
					return FALSE;
					}
				break;
			}
			
		fDLE = FALSE;
		}

	return FALSE;
	}

UINT CEncDF1TCPMaster::RxCheck(void)
{
	DF1HEADBASE &TxHead = (DF1HEADBASE &) m_bTxBuff[0];
			
	DF1HEADBASE &RxHead = (DF1HEADBASE &) m_bRxBuff[0];

	if( RxHead.bComm != (TxHead.bComm | (1 << 6)) ) {

		return repFailed;
		}

	if( RxHead.bDest != TxHead.bSource ) {

		return repFailed;
		}

	if( RxHead.bSource != TxHead.bDest ) {

		if( RxHead.bSource == TxHead.bSource ) {

			switch( RxHead.bStatus & 0x0F ) {

				case 0x01:
					return repBusy;

				default:
					return repFailed;
				}
			}

		return repFailed;
		}
	
	return RxHead.bStatus ? repFailed : repOkay;
	}

BOOL CEncDF1TCPMaster::CatchWrite(void)
{
	DF1HEADBASE &TxHead = (DF1HEADBASE &) m_bTxBuff[0];
			
	DF1HEADBASE &RxHead = (DF1HEADBASE &) m_bRxBuff[0];

	if( RxHead.bComm == 0x0F ) {

		if( RxHead.bStatus == 0x00 ) {

			if( RxHead.bData[0] == BASE_WRITE ) {

				PVOID pFrame = PushFrame();

				NewFrame(0x4F, 0x00, IntelToHost(RxHead.wTrans));

				TxFrame();

				RxAck();

				PullFrame(pFrame);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Checksum Handler

void CEncDF1TCPMaster::ClearCheck(void)
{
	m_uTx = 0;
	
	if( m_fCRC ) {
	
		m_CRC.Clear();
		
		return;
		}
		
	m_bCheck = 0;
	}

void CEncDF1TCPMaster::AddToCheck(BYTE b)
{
	if( m_fCRC ) {
	
		m_CRC.Add(b);
		
		return;
		}
	
	m_bCheck += b;
	}

void CEncDF1TCPMaster::AddInitToCheck(void)
{
	if( m_fCRC && m_fHalf ) {
	
		m_CRC.Add(STX);
		
		return;
		}
	}

void CEncDF1TCPMaster::AddTermToCheck(void)
{
	if( m_fCRC ) {
	
		m_CRC.Add(ETX);
		
		return;
		}
	}

void CEncDF1TCPMaster::SendCheck(void)
{
	if( m_fCRC ) {

		Put(LOBYTE(m_CRC.GetValue()));
	
		Put(HIBYTE(m_CRC.GetValue()));
		
		return;
		}
	
	BYTE b = ((0x100 - m_bCheck) & 0xFF);
		
	Put(b);
	}

// Data Link Layer

void CEncDF1TCPMaster::TxAck(void)
{	
	m_uTx = 0;
	
	Put(DLE);
	Put(ACK);

	Send();
	}

void CEncDF1TCPMaster::TxNak(void)
{
	m_uTx = 0;

	Put(DLE);
	Put(NAK);

	Send();
	}

void CEncDF1TCPMaster::TxEnq(void)
{
	m_uTx = 0;
	
	Put(DLE);
	Put(ENQ);

	Send();
	}

void CEncDF1TCPMaster::TxPoll(void)
{
	// NOTE -- Always use BCC!

	BOOL c = m_fCRC;

	m_fCRC = FALSE;

	ClearCheck();

	Put(DLE);
	Put(ENQ);

	BYTE b = m_pEnc->m_bDest;

	Put(b);

	AddToCheck(b);

	SendCheck();

	m_fCRC = c;

	Send();
	}

UINT CEncDF1TCPMaster::RxAck(void)
{
	SetTimer(m_pEnc->m_uTimeA);
	
	BOOL fDLE   = FALSE;

	UINT uTimer = 0;

	UINT uData  = 0;
	
	while( (uTimer = GetTimer()) ) {

		if ((uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		if( fDLE ) {
		
			switch( uData ) {
			
				case ACK:
				case NAK:
				case STX:
				case EOT:
					return uData;
				}
				
			fDLE = FALSE;
			}
		else {
			if( uData == DLE ) {

				fDLE = TRUE;
				}
			}
		}
	
	return NOTHING;
	}

void CEncDF1TCPMaster::GetCount(UINT uSpace, UINT &uCount)
{
	UINT uMax = 16;
	
	if( IsTriplet(uSpace) ) {

		if( m_pBase->m_bDevice == devPLC5 ) {	
										
			uMax  = 1;
			}
		else {
			uMax /= 3;
			}
		}

	else if( IsString(uSpace) )  {

		uCount = MAX_STRING; 

		return;
		}

	else if( IsLong(uSpace) ) {

		uMax /= 2;
		}
		
	MakeMin(uCount, uMax);
	}

// End of File
 
