
#include "intern.hpp"

#include "IdecTcpM.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Idec MicroSmart TCP/IP Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved

// Instantiator

INSTANTIATE(CIdecMicroSmartTcpMasterDriver);

// Constructor

CIdecMicroSmartTcpMasterDriver::CIdecMicroSmartTcpMasterDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_uKeep     = 0;
	}

// Device

CCODE MCALL CIdecMicroSmartTcpMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pTcp = (CTcpCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pTcp = new CTcpCtx;

			m_pCtx = m_pTcp;

			m_pTcp->m_IP1		= GetAddr(pData);
			m_pTcp->m_IP2		= GetAddr(pData);	
			m_pTcp->m_uPort		= GetWord(pData);
			m_pTcp->m_bDrop		= GetByte(pData);
			m_pTcp->m_fKeep		= GetByte(pData);
			m_pTcp->m_fPing		= GetByte(pData);
			m_pTcp->m_uTime1	= GetWord(pData);
			m_pTcp->m_uTime2	= GetWord(pData);
			m_pTcp->m_uTime3	= GetWord(pData);
			m_pTcp->m_pSock		= NULL;
			m_pTcp->m_uLast		= GetTickCount();
			m_pTcp->m_fDirty	= FALSE;
			m_pTcp->m_fAux          = FALSE;

			pDevice->SetContext(m_pTcp);
			
			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx = m_pTcp;

	return CCODE_SUCCESS;
	}

CCODE MCALL CIdecMicroSmartTcpMasterDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pTcp->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pTcp;

		m_pTcp = NULL;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CIdecMicroSmartTcpMasterDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CTcpCtx * pCtx = (CTcpCtx *) pContext;

	if( uFunc == 1 || uFunc == 5 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		if( uFunc == 1  ) {

			pCtx->m_IP1 = MotorToHost(dwValue);
			}

		if( uFunc == 5 ) {

			pCtx->m_IP2 = MotorToHost(dwValue);
			}

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort  = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 3 )  { 
		
		// Set Drop Number

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bDrop  = BYTE(uValue);

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 4 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP1);
		}
	
	if( uFunc == 6 ) {
		
		// Get Current IP Address
		
		return MotorToHost(pCtx->m_IP2);
		}
		
	if( uFunc == 7 ) {
		
		//Get Fallback Status

		return pCtx->m_fAux;
		}
	
	return 0;
	}

	// Entry Points

CCODE MCALL CIdecMicroSmartTcpMasterDriver::Ping(void)
{
	if( m_pTcp->m_fPing ) {
		
		DWORD IP;

		if( !m_pTcp->m_fAux ) {

			IP = m_pTcp->m_IP1;
			}
		else
			IP = m_pTcp->m_IP2;
		
		if( CheckIP(IP, m_pTcp->m_uTime2) == NOTHING ) {
			
			if( m_pTcp->m_IP2 ) {

				m_pTcp->m_fAux = !m_pTcp->m_fAux;
				}

			return CCODE_ERROR; 
			}
		}

	if( OpenSocket() ) {
						
		return CIdecBaseDriver::Ping();
		}

	return CCODE_ERROR;
	}

// Socket Management

BOOL CIdecMicroSmartTcpMasterDriver::CheckSocket(void)
{
	if( m_pTcp->m_pSock ) {

		if( !m_pTcp->m_fDirty ) {

			UINT Phase;

			m_pTcp->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CIdecMicroSmartTcpMasterDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pTcp->m_fDirty ) {

		m_pTcp->m_fDirty = FALSE;

		m_pTcp->m_fAux   = FALSE;

		return FALSE;
		}

	if( !m_pTcp->m_fKeep ) {

		UINT dt = GetTickCount() - m_pTcp->m_uLast;

		UINT tt = ToTicks(m_pTcp->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pTcp->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   Port;

		if( !m_pTcp->m_fAux ) {

			IP   = (IPADDR const &) m_pTcp->m_IP1;

			Port = WORD(m_pTcp->m_uPort);
			}
		else {
			IP   = (IPADDR const &) m_pTcp->m_IP2;

			Port = WORD(m_pTcp->m_uPort);
			}

		if( m_pTcp->m_pSock->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pTcp->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pTcp->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pTcp->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			if( m_pTcp->m_IP2 ) {

				m_pTcp->m_fAux = !m_pTcp->m_fAux;
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		if( m_pTcp->m_IP2 ) {

			m_pTcp->m_fAux = !m_pTcp->m_fAux;
			}

		return FALSE;
		}

	return FALSE;
	}

void CIdecMicroSmartTcpMasterDriver::CloseSocket(BOOL fAbort)
{
	if( m_pTcp->m_pSock ) {

		if( fAbort )
			m_pTcp->m_pSock->Abort();
		else
			m_pTcp->m_pSock->Close();

		m_pTcp->m_pSock->Release();

		m_pTcp->m_pSock = NULL;

		m_pTcp->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Overridable Methods

BOOL CIdecMicroSmartTcpMasterDriver::Send(void)
{
	if( OpenSocket() ) {

		UINT uSize = m_uPtr;

		if( m_pTcp->m_pSock->Send(m_bTxBuff, m_uPtr) == S_OK ) {

			if( uSize == m_uPtr ) {

				return TRUE;
				}
			}

		CloseSocket(TRUE);
		}

	return FALSE;
	}

BYTE CIdecMicroSmartTcpMasterDriver::GetFrame(void)
{
	SetTimer(m_pTcp->m_uTime2);

	UINT uPtr   = 0;

	UINT uState = 0;

	BYTE bType  = 0;

	while( GetTimer() ) {

		BYTE     b = 0;

		UINT uSize = 1;

		if( m_pTcp->m_pSock->Recv(&b, uSize) == S_OK ) {

			switch( uState ) {
		
				case 0:
					switch( b ) {
				
						case ACK:
						case NAK:

							bType    = b;
							m_bCheck = b;
							uPtr     = 0;
							uState   = 1;

							break;
						}
					break;
				
				case 1:
					if( b == CR ) {
					
						m_bCheck ^= m_bRxBuff[uPtr - 1];
					
						m_bCheck ^= m_bRxBuff[uPtr - 2];
					
						if( m_bCheck == GetValue(uPtr - 2, 2, 16) ) {
						 
							return bType;
							}
						
						return FALSE;
						}
					else {
						m_bCheck ^= b;
					
						m_bRxBuff[uPtr++] = b;
					
						if( uPtr >= sizeof(m_bRxBuff) ){

							return FALSE;
							}
						}
					break;
				}

			continue;
			}
		
		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

UINT CIdecMicroSmartTcpMasterDriver::GetMaxWords(void)
{
	return 72;
	}

UINT CIdecMicroSmartTcpMasterDriver::GetMaxBits(void)
{
	return 320;
	}

// Helpers

BOOL CIdecMicroSmartTcpMasterDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CIdecMicroSmartTcpMasterDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CIdecMicroSmartTcpMasterDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
