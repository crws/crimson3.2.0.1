
//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "../Df1Shared/df1m.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDH485Driver;
class CDH485Handler;

//////////////////////////////////////////////////////////////////////////
//
// DH485 Driver
//

class CDH485Driver : public CDF1BaseMaster
{
	public:
		// Constructor
		CDH485Driver(void);

		// Destructor
		~CDH485Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:
		// Device Data
		struct CContext : CDF1BaseMaster::CBaseCtx
		{
			BYTE m_bDrop;
			};

		// Data Members
		BYTE	        m_bThis;
		BYTE	        m_bLast;
		BOOL		m_fMake;
		CDH485Handler * m_pHandler;
		CContext      * m_pCtx;

		// Transport Layter
		BOOL CheckLink(void);
		BOOL Transact(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// DH485 Handler
//

class CDH485Handler : public IPortHandler
{
	public:
		// Constructor
		CDH485Handler(IHelper *pHelper, BYTE bThis, BYTE bLast, BOOL fMake);
		
		// Destructor
		~CDH485Handler(void);

		// Operations
		BOOL PutAppData(BYTE bDrop, PCBYTE pData, UINT uCount);
		UINT GetAppData(PBYTE pData, UINT uCount, UINT uWait);
		
		// Attributes
		DWORD GetActiveList(void) const;
		BOOL  IsActive(BYTE bDrop) const;
		BOOL  HadToken(void) const;
		
		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);

	protected:
		// Helper
		IHelper *m_pHelper;
		
		// Port Object
		IPortObject *m_pPort;

		// Data
		ULONG   m_uRefs;
		
		// Drop Context
		BYTE	m_bThisDrop;
		BYTE	m_bNextDrop;
		BYTE	m_bLastDrop;
		BYTE	m_bInvite;
		BOOL	m_fMake;
		
		// Rx State Machine
		UINT	m_uRxState;
		UINT	m_uRxPtr;
		WORD	m_wRxCheck;

		// Tx State Machine
		BOOL	m_fTxEnable;
		UINT	m_uTxPtr;
		UINT	m_uTxScan;
		UINT	m_uTxDelay;
		UINT	m_uTxHack;

		// Other Context
		UINT	m_uMacState;
		UINT	m_uTokenState;
		UINT	m_uPassRetry;
		UINT	m_uTimer;
		BOOL	m_fWaitReply;
		BOOL	m_fToggle;
		BOOL	m_fSendInvite;
		BOOL	m_fSeenInvite;
		UINT    m_uToken;

		// CRC Generator
		CRC16	m_CRC;
		
		// Active Map
		DWORD	m_dwActive;
		DWORD	m_dwWork;

		// Application Data
		UINT	m_uTxAppCount;
		UINT	m_uRxAppCount;
		BYTE	m_bTxAppDrop;
		IEvent *m_pRxAppFlag;
		BYTE	m_bTxApp[160];
		BYTE	m_bRxApp[160];
		
		// Data Buffers
		BYTE	m_bRxBuff[160];
		BYTE	m_bTxBuff[160];

		// Frame Assembly
		void StartFrame(BYTE bDest, BYTE bCode);
		void AddData(PBYTE pData, UINT uCount);
		void AddByte(BYTE bData);
		void SendFrame(void);
		
		// Receive Machine
		void RxByte(BYTE bData);
		void CheckFrame(void);
		
		// Event Handlers
		void Signal(UINT uEvent);
		void HandleVirgin(UINT uEvent);
		void HandleReset(UINT uEvent);
		void HandleIdle(UINT uEvent);
		void HandleWaitAck(UINT uEvent);
		void HandleWaitAccept(UINT uEvent);
		void HandleCheck(UINT uEvent);
		
		// Token Usage
		void TakeToken(void);
		void UseToken(void);
		BOOL DoSend(void);
		BOOL DoInvite(void);
		BOOL DoPass(UINT uCount);
		
		// Event Support
		void Invalid(void);
		void ResetMac(void);
		void GoIdle(void);
		void SendReset(void);
		void SendAck(void);
		void AcceptInvite(void);
		void SendInvite(BYTE bDrop);
		void SendData(void);
		void SendPass(void);
		void NewSuccessor(void);
		void SeenInvite(void);
		void WaitAppReply(void);
		void TakeData(void);

		// General Support
		BOOL MatchDrop(void);
		void SetTimeout(UINT uTime);
		void ClearTimeout(void);
		void CreateSyncObject(void);
	
		// Tansmit Timing
		PCTXT WhoGetName(void);
		void  CalibDelay(void);
		BOOL  TxDelay(void);
		UINT  ToTicks(UINT t);
	};	      

// End of File
