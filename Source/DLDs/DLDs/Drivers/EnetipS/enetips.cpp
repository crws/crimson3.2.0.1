
#include "intern.hpp"

#include "enetips.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ethernet IP Slave Driver
// 
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CEthernetIPSlave);

// Constructor

CEthernetIPSlave::CEthernetIPSlave(void)
{
	m_Ident       = DRIVER_ID;

	m_pEnetHelper = NULL;

	m_pImplicit   = NULL;

	m_fOpen       = FALSE;

	Init();
	}

// Destructor

CEthernetIPSlave::~CEthernetIPSlave(void)
{
	FreeHostBuf();

	FreeNetBuf();

	FreeInfo();
	}

// Configuration

void MCALL CEthernetIPSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bRxHdr = GetByte(pData);
		}
	}

// Management

void MCALL CEthernetIPSlave::Attach(IPortObject *pPort)
{
	StackOpen();
	}

void MCALL CEthernetIPSlave::Detach(void)
{
	StackClose();
	}

// Entry Point

void MCALL CEthernetIPSlave::Service(void)
{
	for(;;) {

		if( MakeLink() ) {

			for(;;) {
	
				if( CheckLink() ) {

					CheckWrites();
		
					CheckReads();

					ForceSleep(50);

					continue;
					}

				Sleep(FOREVER);
				}
			}

		ForceSleep(50);
		}
	}

// Implementation

void CEthernetIPSlave::CheckReads(void)
{
	IProducer *pConn = NULL;

	for(;;) {

		if( m_pImplicit->GetNextProducer(pConn) ) {

			if( pConn->IsActive() && pConn->GetSize() > 1 ) {

				UINT uIdent = pConn->GetIdent();

				if( FindInfo(uIdent) ) {

					UINT uType     = GetType(uIdent);

					UINT uTypeSize = GetTypeSize(uType); 

					UINT uCount    = pConn->GetSize() / uTypeSize; 

					UINT uSize     = uCount * uTypeSize;

					Allocate(uCount, uSize);
				
					CAddress Addr;

					Addr.a.m_Table  = uIdent;

					Addr.a.m_Extra  = 0;

					Addr.a.m_Type   = uType;
						
					Addr.a.m_Offset = 0;

					if( COMMS_SUCCESS(Read(Addr, m_pdwHostBuf, uCount)) ) {

						CIntelDataPacker Data(m_pdwHostBuf, uCount, TRUE);
					
						switch( uType ) {

							case addrByteAsByte:

								Data.Pack(PBYTE(m_pbNetBuf));

								break;

							case addrWordAsWord:

								Data.Pack(PWORD(m_pbNetBuf));

								break;

							case addrLongAsLong:

								Data.Pack(PDWORD(m_pbNetBuf));

								break;
							}
						
						pConn->Send(m_pbNetBuf, uSize);
						}
					}
				}
			
			continue;
			}

		break;
		}
	}

void CEthernetIPSlave::CheckWrites(void)
{
	IConsumer *pConn = NULL;

	for(;;) {

		if( m_pImplicit->GetNextConsumer(pConn) ) {

			UINT uExtra = m_bRxHdr ? sizeof(DWORD) : 0;
			
			if( pConn->IsActive() && pConn->HasNewData() && pConn->GetSize() > 1 ) {

				pConn->ClearNewData();

				UINT uIdent = pConn->GetIdent();

				if( FindInfo(uIdent) ) {

					UINT uType     = GetType(uIdent);

					UINT uTypeSize = GetTypeSize(uType); 

					UINT uCount    = pConn->GetSize() / uTypeSize; 

					UINT uSize     = uCount * uTypeSize;

					Allocate(uCount, uSize);

					uCount = pConn->Recv(m_pbNetBuf, uSize);

					if( uCount > uExtra ) {

						if( uExtra ) {	
						
							DWORD dwHdr = IntelToHost((DWORD &) m_pbNetBuf[0]);

							if( !(dwHdr & 0x1) ) {

								continue;
								}

							uCount -= uExtra;
							}
					
						CAddress Addr;

						Addr.a.m_Table  = uIdent;

						Addr.a.m_Extra  = 0;

						Addr.a.m_Type   = uType;
							
						Addr.a.m_Offset = 0;

						uCount /= uTypeSize;

						CIntelDataPacker Data(m_pdwHostBuf, uCount, TRUE);

						switch( uType ) {

							case addrByteAsByte:

								Data.Unpack(m_pbNetBuf + uExtra);

								break;

							case addrWordAsWord:

								Data.Unpack(PWORD(m_pbNetBuf + uExtra));

								break;

							case addrLongAsLong:

								Data.Unpack(PDWORD(m_pbNetBuf + uExtra));

								break;
							}
				
						Write(Addr, m_pdwHostBuf, uCount);
						}
					}
				}
			
			continue;
			}

		break;
		}
	}

// Stack Helpers

void CEthernetIPSlave::StackOpen(void)
{
	if( !m_pEnetHelper && !m_fOpen ) {
		
		if( MoreHelp(IDH_ENETIP, (void **) &m_pEnetHelper) ) {

			m_fOpen = m_pEnetHelper->Open();
			}
		}
	}

void CEthernetIPSlave::StackClose(void)
{
	if( m_pEnetHelper ) {

		m_pEnetHelper->Close();

		m_pEnetHelper->Release();

		m_pEnetHelper = NULL;

		m_pImplicit   = NULL;

		m_fOpen       = FALSE;
		}

	FreeInfo();
	}

// Transport Layer

BOOL CEthernetIPSlave::MakeLink(void)
{	
	if( m_fOpen ) {

		if( m_pImplicit ) {

			return TRUE;
			}

		if( (m_pImplicit = m_pEnetHelper->GetImplicit()) ) {

			InitInfo();

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEthernetIPSlave::CheckLink(void)
{
	return m_fOpen && m_pImplicit;
	}

// Buffers

void CEthernetIPSlave::Init(void)
{
	m_pbNetBuf   = NULL;

	m_pdwHostBuf = NULL;

	m_uAllocHost = 0;

	m_uAllocNet  = 0;

	m_pInfo      = NULL;
	}

void CEthernetIPSlave::InitInfo(void)
{
	FreeInfo();

	m_pInfo = new CInfo [ 256 ];

	memset(m_pInfo, 0, sizeof(CInfo) * 256);
	}

void CEthernetIPSlave::Allocate(UINT uCount, UINT uSize)
{
	if( uCount > m_uAllocHost ) {
		
		FreeHostBuf();

		m_uAllocHost = ((uCount + 7) / 8) * 8;

		m_pdwHostBuf = new DWORD[ m_uAllocHost ];
		}

	if( uSize > m_uAllocNet ) {
		
		FreeNetBuf();

		m_uAllocNet = ((uSize + 7) / 8) * 8;
		
		m_pbNetBuf  = new BYTE[ m_uAllocNet ];
		}
	}

void CEthernetIPSlave::FreeInfo(void)
{
	if( m_pInfo ) {

		delete [] m_pInfo;

		m_pInfo = NULL;
		}
	}

void CEthernetIPSlave::FreeHostBuf(void)
{
	if( m_pdwHostBuf ) {

		delete [] m_pdwHostBuf;

		m_pdwHostBuf = NULL;

		m_uAllocHost = 0;
		}
	}

void CEthernetIPSlave::FreeNetBuf(void)
{
	if( m_pbNetBuf ) {

		delete [] m_pbNetBuf;

		m_pbNetBuf  = NULL;

		m_uAllocNet = 0;
		}
	}

// Type Helpers

BOOL CEthernetIPSlave::FindInfo(UINT Id)
{
	if( Id < 256 ) {

		CInfo &i = m_pInfo[Id];
		
		if( !i.m_bValid ) {

			CAddress Addr;

			Addr.a.m_Table  = Id;

			Addr.a.m_Extra  = 0;

			Addr.a.m_Offset = 0;
						
			for( UINT n = 0; n < 3; n ++ ) {
			
				switch( n ) {

					case 0: 
						Addr.a.m_Type = addrByteAsByte;

						break;

					case 1: 
						Addr.a.m_Type = addrWordAsWord;

						break;

					case 2: 
						Addr.a.m_Type = addrLongAsLong;

						break;
					}

				if( COMMS_SUCCESS(Read(Addr, NULL, 1)) ) {

					i.m_bValid = TRUE;

					i.m_bType  = Addr.a.m_Type;

					return TRUE;
					}
				}

			i.m_bValid = TRUE;

			i.m_bType  = addrReserved;

			return FALSE;
			}
		
		return i.m_bType != addrReserved;
		}

	return FALSE;
	}

UINT CEthernetIPSlave::GetType(UINT Id) const
{
	return Id < 256 ? m_pInfo[Id].m_bType : addrReserved;
	}

UINT CEthernetIPSlave::GetTypeSize(BYTE bType) const
{
	switch( bType ) {

		case addrByteAsByte: return sizeof(BYTE);
		case addrWordAsWord: return sizeof(WORD);
		case addrLongAsLong: return sizeof(DWORD);
		}

	return 1;
	}

// End of File
