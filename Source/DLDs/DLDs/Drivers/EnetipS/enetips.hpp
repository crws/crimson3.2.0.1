
//////////////////////////////////////////////////////////////////////////
//
// Ethernet IP Slave
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

class CEthernetIPSlave : public CSlaveDriver
{
	public:
		// Constructor
		CEthernetIPSlave(void);

		// Destructor
		~CEthernetIPSlave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Entry Points
		DEFMETH(void) Service(void);

	protected:
		// Custom Type
		struct CInfo
		{
			BYTE m_bType  : 4;
			BYTE m_bValid : 1;
			BYTE m_bPad   : 3;
			};
		
		// Data
		IEthernetIPHelper * m_pEnetHelper;
		IImplicit         * m_pImplicit;
		BOOL                m_fOpen;
		PBYTE 		    m_pbNetBuf;
		PDWORD		    m_pdwHostBuf;
		UINT		    m_uAllocHost;
		UINT                m_uAllocNet;
		BYTE		    m_bRxHdr;
		CInfo             * m_pInfo;

		// Implementation
		void CheckReads (void);
		void CheckWrites(void);

		// Stack Helpers
		void StackOpen(void);
		void StackClose(void);

		// Transport Layer
		BOOL MakeLink(void);
		BOOL CheckLink(void);

		// Buffer
		void Init(void);
		void InitInfo(void);
		void Allocate(UINT uCount, UINT uSize);
		void FreeInfo(void);
		void FreeHostBuf(void);
		void FreeNetBuf(void);

		// Type Helpers
		BOOL FindInfo(UINT Id);
		UINT GetType(UINT Id) const;
		UINT GetTypeSize(BYTE bType) const;
	};

// End of File
