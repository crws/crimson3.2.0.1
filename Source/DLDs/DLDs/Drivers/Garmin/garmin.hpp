
//////////////////////////////////////////////////////////////////////////
//
// Garmin GPS Driver
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CGarminHandler;
class CGarminDriver;

//////////////////////////////////////////////////////////////////////////
//
// Garmin Property IDs
//

enum
{
	propTime	= 1,
	propLat		= 2,
	propLong	= 3,
	propAlt		= 4,
	propSpeed	= 5,
	propCourse	= 6,
	propDate	= 7,
	propTimeDate	= 8,
	propMagVar	= 9,
	propValid	= 10,
	propWindSpeed   = 11,
	propWindAngle   = 12,
	propWindRef     = 13,
	propWindValid   = 14,
	propParallel	= 15,
	propCount	= 16,
};

//////////////////////////////////////////////////////////////////////////
//
// Garmin GPS Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

class CGarminDriver : public CMasterDriver
{
	public:
		// Constructor
		CGarminDriver(void);

		// Destructor
		~CGarminDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Data Members
		CGarminHandler *m_pData;
		LPCBYTE         m_pConfig;
};

//////////////////////////////////////////////////////////////////////////
//
// Garmin Handler
//

class CGarminHandler : public IPortHandler
{
	public:
		// Constructor
		CGarminHandler(IHelper *pHelper);

		// Destructor
		~CGarminHandler(void);

		// Attributes
		BOOL IsOnline(void) const;

		// Operations
		void Load(LPCBYTE pData);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject * pPort);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnTimer(void);
		UINT GetMode(void);

		// Comms Data
		DWORD m_dwData[propCount];

	protected:
		// Helper
		IHelper      *m_pHelper;
		IExtraHelper *m_pExtra;

		// Config Data
		UINT m_uDegForm;
		UINT m_uUnitSpeed;
		UINT m_uUnitAlt;

		// Data Members
		ULONG        m_uRefs;
		IPortObject *m_pPort;
		BOOL	     m_fOnline;
		UINT	     m_uOnline;
		BYTE         m_bRx[512];
		UINT	     m_uState;
		UINT         m_uPtr;
		BOOL	     m_fRxCheck;
		BYTE	     m_bRxCheck;
		BYTE	     m_bTxCheck;

		// Implementation
		BOOL   IsHex(char cData);
		UINT   FromHex(char cData);
		void   CheckFrame(void);
		BOOL   ParseFrame(PCTXT p);
		BOOL   ParseGPRMC(PCTXT p);
		BOOL   ParseGPGGA(PCTXT p);
		BOOL   ParseGPMWV(PCTXT p);
		BOOL   ParseGPVPW(PCTXT p);
		DWORD  ClearProp(UINT n);
		double Flip(double v, PCTXT p, char c);
		double ToDeg(double v);
		double ToAlt(double v);
		double ToSpeed(double v, char c);
		DWORD  ToTime(DWORD t);
		DWORD  ToDate(DWORD t);

		// Conversions
		double LongToReal(DWORD  i);
		DWORD  RealToLong(double r);

		// Timing
		UINT   ToTicks(UINT t);
};

// End of File
