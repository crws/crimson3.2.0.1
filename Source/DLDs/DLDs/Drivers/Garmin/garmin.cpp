
#include "intern.hpp"

#include "garmin.hpp"

#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Garmin GPS Driver
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CGarminDriver);

// Constructor

CGarminDriver::CGarminDriver(void)
{
	m_Ident   = DRIVER_ID;

	m_pConfig = NULL;
}

// Destructor

CGarminDriver::~CGarminDriver(void)
{
}

// Configuration

void MCALL CGarminDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_pConfig = pData;
	}
}

void MCALL CGarminDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
}

// Management

void MCALL CGarminDriver::Attach(IPortObject *pPort)
{
	m_pData = new CGarminHandler(m_pHelper);

	pPort->Bind(m_pData);

	m_pData->Load(m_pConfig);
}

void MCALL CGarminDriver::Detach(void)
{
	m_pData->Release();
}

void MCALL CGarminDriver::Open(void)
{
}

// Device

CCODE MCALL CGarminDriver::DeviceOpen(IDevice *pDevice)
{
	return CMasterDriver::DeviceOpen(pDevice);
}

CCODE MCALL CGarminDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CGarminDriver::Ping(void)
{
	if( m_pData->IsOnline() ) {

		return 1;
	}

	return CCODE_ERROR;
}

CCODE MCALL CGarminDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pData->IsOnline() ) {

		UINT n = Addr.a.m_Offset;

		if( n < propCount ) {

			*pData = m_pData->m_dwData[n];

			return 1;
		}

		return CCODE_ERROR;
	}

	return CCODE_ERROR;
}

CCODE MCALL CGarminDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return uCount;
}

//////////////////////////////////////////////////////////////////////////
//
// Garmin Handler
//

// Constructor

CGarminHandler::CGarminHandler(IHelper *pHelper)
{
	StdSetRef();

	m_pHelper    = pHelper;

	m_pPort      = NULL;

	m_uDegForm   = 0;

	m_uUnitSpeed = 0;

	m_uUnitAlt   = 0;
}

// Destructor

CGarminHandler::~CGarminHandler(void)
{
}

// Attributes

BOOL CGarminHandler::IsOnline(void) const
{
	return m_fOnline;
}

// Operations

void CGarminHandler::Load(LPCBYTE pData)
{
	if( pData ) {

		m_uDegForm   = *pData++;

		m_uUnitSpeed = *pData++;

		m_uUnitAlt   = *pData++;
	}

	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
}

// IUnknown

HRESULT CGarminHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
}

ULONG CGarminHandler::AddRef(void)
{
	StdAddRef();
}

ULONG CGarminHandler::Release(void)
{
	StdRelease();
}

// IPortHandler

void CGarminHandler::Bind(IPortObject * pPort)
{
	m_pPort = pPort;
}

void CGarminHandler::OnOpen(CSerialConfig const &Config)
{
	m_pExtra->SetGPSTime(NOTHING);

	for( UINT n = 0; n < propCount; n++ ) {

		m_dwData[n] = ClearProp(n);
	}

	m_uState  = 0;

	m_fOnline = FALSE;

	m_uOnline = 0;
}

void CGarminHandler::OnClose(void)
{
}

void CGarminHandler::OnRxData(BYTE bData)
{
	switch( m_uState ) {

		case 0:
			if( bData == '$' ) {

				m_uState   = 1;

				m_uPtr     = 0;

				m_fRxCheck = FALSE;

				m_bRxCheck = 0;
			}
			break;

		case 1:
			if( bData == '*' ) {

				m_uState = 2;
			}
			else {
				if( m_uPtr < sizeof(m_bRx) ) {

					m_bRx[m_uPtr++] = bData;

					m_bRxCheck     ^= bData;
				}
				else
					m_uState = 0;
			}
			break;

		case 2:
			if( bData == CR ) {

				m_uState = 5;
			}
			else {
				if( IsHex(bData) ) {

					m_bTxCheck = FromHex(bData);

					m_uState   = 3;
				}
				else
					m_uState = 0;
			}
			break;

		case 3:
			if( IsHex(bData) ) {

				m_bTxCheck *= 16;

				m_bTxCheck += FromHex(bData);

				m_fRxCheck  = TRUE;

				m_uState    = 4;
			}
			else
				m_uState = 0;
			break;

		case 4:
			if( bData == CR ) {

				m_uState = 5;
			}
			else
				m_uState = 0;
			break;

		case 5:
			if( bData == LF ) {

				CheckFrame();
			}

			m_uState = 0;

			break;
	}
}

void CGarminHandler::OnRxDone(void)
{
}

BOOL CGarminHandler::OnTxData(BYTE &bData)
{
	return FALSE;
}

void CGarminHandler::OnTxDone(void)
{
}

void CGarminHandler::OnTimer(void)
{
	if( m_uOnline && !--m_uOnline ) {

		m_fOnline = FALSE;
	}
}

UINT CGarminHandler::GetMode(void)
{
	return 0;
}

// Implementation

BOOL CGarminHandler::IsHex(char cData)
{
	if( cData >= '0' && cData <= '9' ) {

		return TRUE;
	}

	if( cData >= 'A' && cData <= 'F' ) {

		return TRUE;
	}

	if( cData >= 'a' && cData <= 'f' ) {

		return TRUE;
	}

	return FALSE;
}

UINT CGarminHandler::FromHex(char cData)
{
	if( cData >= '0' && cData <= '9' ) {

		return cData - '0';
	}

	if( cData >= 'A' && cData <= 'F' ) {

		return cData - 'A' + 10;
	}

	if( cData >= 'a' && cData <= 'f' ) {

		return cData - 'a' + 10;
	}

	return 0;
}

void CGarminHandler::CheckFrame(void)
{
	if( m_fRxCheck == FALSE || m_bRxCheck == m_bTxCheck ) {

		m_bRx[m_uPtr++] = ',';

		m_bRx[m_uPtr++] = 0;

		if( ParseFrame(PCTXT(m_bRx)) ) {

			m_fOnline = TRUE;

			m_uOnline = ToTicks(5000);
		}
	}
}

BOOL CGarminHandler::ParseFrame(PCTXT p)
{
	if( !strncmp(p, "GPRMC", 5) ) {

		ParseGPRMC(p + 6);

		return TRUE;
	}

	if( !strncmp(p, "GPGGA", 5) ) {

		ParseGPGGA(p + 6);

		return TRUE;
	}

	if( !strncmp(p+2, "MWV", 3) ) {

		ParseGPMWV(p + 6);

		return TRUE;
	}

	if( !strncmp(p+2, "VPW", 3) ) {

		ParseGPVPW(p + 6);

		return TRUE;
	}

	return FALSE;
}

BOOL CGarminHandler::ParseGPRMC(PCTXT p)
{
	double t = 0;

	for( UINT n = 1; n <= 12; n++ ) {

		PTXT c = PTXT(strchr(p, ','));

		if( c ) {

			*c = 0;

			switch( n ) {

				case 1:
					m_dwData[propTime] = ToTime(ATOF(p));
					break;

				case 9:
					m_dwData[propDate] = ToDate(ATOI(p));
					break;

				case 2:
					m_dwData[propValid] = (*p == 'A');
					break;

				case 3:
				case 5:
					t = ToDeg(ATOF(p));
					break;

				case 10:
					t = ATOF(p);
					break;

				case 4:
					m_dwData[propLat] = RealToLong(Flip(t, p, 'S'));
					break;

				case 6:
					m_dwData[propLong] = RealToLong(Flip(t, p, 'W'));
					break;

				case 11:
					m_dwData[propMagVar] = RealToLong(Flip(t, p, 'E'));
					break;

				case 7:
					m_dwData[propSpeed] = RealToLong(ToSpeed(ATOF(p), 'N'));
					break;

				case 8:
					m_dwData[propCourse] = RealToLong(ATOF(p));
					break;
			}

			p = c + 1;

			continue;
		}

		return FALSE;
	}

	if( m_dwData[propDate] ) {

		if( m_dwData[propTime] ) {

			m_dwData[propTimeDate] = m_dwData[propDate] + m_dwData[propTime];

			m_pExtra->SetGPSTime(m_dwData[propTimeDate]);
		}
	}

	return TRUE;
}

BOOL CGarminHandler::ParseGPGGA(PCTXT p)
{
	for( UINT n = 1; n <= 12; n++ ) {

		PTXT c = PTXT(strchr(p, ','));

		if( c ) {

			*c = 0;

			switch( n ) {

				case 9:
					m_dwData[propAlt] = RealToLong(ToAlt(ATOF(p)));
			}

			p = c + 1;

			continue;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CGarminHandler::ParseGPMWV(PCTXT p)
{
	double t = 0;

	for( UINT n = 1; n <= 12; n++ ) {

		PTXT c = PTXT(strchr(p, ','));

		if( c ) {

			*c = 0;

			switch( n ) {

				case 1:
					m_dwData[propWindAngle] = RealToLong(ATOF(p));
					break;

				case 2:
					m_dwData[propWindRef] = (*p == 'T');
					break;

				case 3:
					t = ATOF(p);
					break;

				case 4:
					m_dwData[propWindSpeed] = RealToLong(ToSpeed(t, *p));
					break;

				case 5:
					m_dwData[propWindValid] = (*p == 'A');
					break;
			}

			p = c + 1;

			continue;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CGarminHandler::ParseGPVPW(PCTXT p)
{
	double t = 0;

	for( UINT n = 1; n <= 12; n++ ) {

		PTXT c = PTXT(strchr(p, ','));

		if( c ) {

			*c = 0;

			switch( n ) {

				case 1:
					t = ATOF(p);
					break;

				case 2:
					m_dwData[propParallel] = RealToLong(ToSpeed(t, *p));
					break;
			}

			p = c + 1;

			continue;
		}

		return FALSE;
	}

	return TRUE;
}

DWORD CGarminHandler::ClearProp(UINT n)
{
	switch( n ) {

		case propTime:
		case propDate:
		case propTimeDate:
		case propValid:
			return 0;
	}

	return RealToLong(0);
}

double CGarminHandler::Flip(double v, PCTXT p, char c)
{
	return (*p == c) ? -v : +v;
}

double CGarminHandler::ToDeg(double v)
{
	int    d = int(v / 100);

	double m = v - d * 100;

	switch( m_uDegForm ) {

		case 0:
			return d + m /  60.0;	// dd.mmmmmm

		case 1:
			return d + m / 100.0;	// dd.dddddd

		case 2:
			return v;		// ddmm.dddd
	}

	return 0;
}

double CGarminHandler::ToAlt(double v)
{
	switch( m_uUnitAlt ) {

		case 0:
			return v * 3.2808399;	// Feet

		case 1:
			return v;		// Meters
	}

	return 0;
}

double CGarminHandler::ToSpeed(double v, char c)
{
	if( c == 'N' ) {

		// From Knots

		switch( m_uUnitSpeed ) {

			case 0:
				return v / 1.0;		// Knots

			case 1:
				return v / 0.868976242;	// MPH

			case 2:
				return v / 0.539956803;	// KPH

			case 3:
				return v / 1.94384449;	// m/s
		}
	}

	if( c == 'K' ) {

		// From KPH

		switch( m_uUnitSpeed ) {

			case 0:
				return v / 1.85200;	// Knots

			case 1:
				return v / 1.609344;	// MPH

			case 2:
				return v / 1.0;		// KPH

			case 3:
				return v / 3.6;		// m/s
		}
	}

	if( c == 'M' ) {

		// From m/s

		switch( m_uUnitSpeed ) {

			case 0:
				return v / 0.51444444;	// Knots

			case 1:
				return v / 0.44704;	// MPH

			case 2:
				return v / 0.27777778;	// KPH

			case 3:
				return v / 1.0;		// m/s
		}
	}

	return 0;
}

DWORD CGarminHandler::ToTime(DWORD t)
{
	DWORD h = (t / 10000) % 100;

	DWORD m = (t / 100) % 100;

	DWORD s = (t) % 100;

	DWORD r = 3600 * h + 60 * m + s;

	return r;
}

DWORD CGarminHandler::ToDate(DWORD t)
{
	DWORD d = (t / 10000) % 100;

	DWORD m = (t / 100) % 100;

	DWORD y = (t) % 100;

	DWORD r = ::Date(y, m, d);

	return r;
}

// Conversions

double CGarminHandler::LongToReal(DWORD i)
{
	return I2R(i);
}

DWORD CGarminHandler::RealToLong(double r)
{
	float f = float(r);

	return R2I(f);
}

// Timing

UINT CGarminHandler::ToTicks(UINT t)
{
	return t ? ((t < 5) ? 1 : (t / 5)) : 0;
}

// End of File
