#include "intern.hpp"

#include "flowm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// FlowComm Master Serial Driver
//

// Instantiator

INSTANTIATE(CFlowCommMasterSerialDriver);

// Constructor

CFlowCommMasterSerialDriver::CFlowCommMasterSerialDriver(void)
{
	m_Ident         = DRIVER_ID; 
	
	}

// Destructor

CFlowCommMasterSerialDriver::~CFlowCommMasterSerialDriver(void)
{
	}

// Configuration

void MCALL CFlowCommMasterSerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CFlowCommMasterSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CFlowCommMasterSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CFlowCommMasterSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CFlowCommMasterSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop  = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CFlowCommMasterSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CFlowCommMasterSerialDriver::Ping(void)
{
	return CCODE_SUCCESS;
	}

CCODE MCALL CFlowCommMasterSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("\nRead %2.2x Count %u", Addr.a.m_Offset, uCount);*/

	memset(pData, ' ', FLOW_MAX);

	UINT uOffset = FindCode(Addr.a.m_Offset);

	uCount = FLOW_MAX;

	SendHeader(BYTE(READ(m_pCtx->m_bDrop)), uOffset);
	
	if( RecvData() ) {

		UINT Count = m_uPtr;

		SendAck();

		MakeMin(Count, FLOW_MAX);

		for( UINT u = 0; u < Count; u++ ) {

			pData[u] = m_bRxBuff[u];
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CFlowCommMasterSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("\nWrite %c Count %u", Addr.a.m_Offset, uCount);*/

	UINT uOffset = FindCode(Addr.a.m_Offset);

	SendHeader(BYTE(WRITE(m_pCtx->m_bDrop)), uOffset);

	if( RecvAck() ) {

		if( SendData(pData, min(uCount, FLOW_MAX)) ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

// Implementation

void CFlowCommMasterSerialDriver::AddByte(BYTE bByte)
{
	m_bTxBuff[m_uPtr++] = bByte;
	}

void CFlowCommMasterSerialDriver::SendHeader(BYTE bCommand, BYTE bInstruct)
{
	m_uPtr = 0;

	AddByte(EOT);

	AddByte(bCommand);

	AddByte(bInstruct);

	AddByte(ENQ);

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);
	}

void CFlowCommMasterSerialDriver::SendAck(void)
{
	m_uPtr = 0;

	AddByte(ACK);

	AddByte(EOT);

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);
	}

BOOL CFlowCommMasterSerialDriver::SendData(PDWORD pData, UINT uCount)
{
	m_uPtr = 0;

	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

	AddByte(STX);

	for( UINT u = 0; u < uCount; u++ ) {

		AddByte(BYTE(pData[u]));
		}

	BYTE bSPLRC = CalcSPLRC(m_bTxBuff + 1);

	AddByte(ETX);

	AddByte(bSPLRC);

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	if( RecvAck( ) ) {

		m_uPtr = 0;

		AddByte(EOT);

		m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

		return TRUE;
		}

	return FALSE;
	}

BOOL CFlowCommMasterSerialDriver::RecvData(void)
{
	UINT uTime = 2000;
	
	UINT uTimer = 0;

	UINT uData = 0;

	BOOL fSTX = FALSE;

	BOOL fETX = FALSE;

	m_uPtr = 0;

	SetTimer(uTime);

	while( (uTimer = GetTimer()) ) {

		if( !fSTX && uTimer < uTime - FLOW_TIMEOUT ) {

			return FALSE;
			}

		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( !fSTX ) {

			if( uData == NAK ) {

				return FALSE;
				}

			if( uData == EOT ) {

				return FALSE;
				}

			if( uData == STX ) {

				fSTX = TRUE;
				}
			}

		else if( !fETX ) {

			if( uData == ETX ) {

				fETX = TRUE;
				}
			else
				m_bRxBuff[m_uPtr++] = uData;
			}

		else if( fETX ) {

			return ( uData == CalcSPLRC(m_bRxBuff) );
			}
		
		Sleep(5);
		}

	return FALSE;
	}

BOOL CFlowCommMasterSerialDriver::RecvAck(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	SetTimer(FLOW_TIMEOUT);

	while( (uTimer = GetTimer()) ) {

		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( uData == ACK ) {

			return TRUE;
			}

		if( uData == EOT ) {

			return FALSE;
			}

		Sleep(5);
		}

	return FALSE;
	}

BYTE CFlowCommMasterSerialDriver::CalcSPLRC(PBYTE pBuff)
{
	BYTE bSPLRC = 0;

	for( UINT u = 0; u < m_uPtr; u++ ) {

		bSPLRC = pBuff[u] ^ bSPLRC;
		}

	return bSPLRC <= 0x20 ? bSPLRC + 0x20 : bSPLRC;
	}

UINT CFlowCommMasterSerialDriver::FindCode(UINT uOffset)
{
	return LOBYTE((uOffset / FLOW_MAX) + FLOW_OFFSET);
	}
