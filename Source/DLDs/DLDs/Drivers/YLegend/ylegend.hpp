
//////////////////////////////////////////////////////////////////////////
//
// YaskawaLegend Driver
//

#define YLEGEND_ID 0x3391

// Command Types
#define CM_READ1	0	// no axis specifier attached
#define CM_READ2	1	// axis specifier to be attached
#define CM_BOTH0	2	// send 0 with axis pattern for write
#define CM_BOTH1	3	// send 1 with axis pattern for write
#define CM_BOTH2	4	// no axis specifier attached
#define CM_BOTH3	5	// axis specifier to be attached
#define CM_BOTH4	6	// readable, write only command acronym
#define CM_WRITE1	7	// no axis specifier attached
#define CM_WRITE2	8	// axis specifier to be attached
#define CM_COM0		9	// send 0 with command
#define CM_COM1		10	// send 1 with command
#define CM_COM2		11	// no axis specifier attached
#define CM_COM3		12	// axis specifier to be attached
#define CM_COM4		13	// Command Acronym only
#define CM_SPEC		14	// TCP/IP function

#define POSITIONFORMAT	91
#define VARIABLEFORMAT	92
#define FIRSTUSERVAR	93	// uID of YP

#define IHIH		0
#define	IHIP		1
#define	IHPORT		2
#define	IHTYPE		3

#define	CHAXIS		0
#define	CHSEND		1
#define	CHRCV		2

struct FAR YLEGENDCmdDef {
	char 	sName[4];
	WORD	Type;
	UINT	uID;	
	};

class CYaskawaLegendDriver : public CMasterDriver
{
	public:
		// Constructor
		CYaskawaLegendDriver(void);

		// Destructor
		~CYaskawaLegendDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			DWORD	m_PositionFormat;
			DWORD	m_VariableFormat;
			BOOL	m_fEchoOffSent;
			UINT	m_IHHandle;
			DWORD	m_IHIP;
			UINT	m_IHPort;
			UINT	m_IHProtocol;
			UINT	m_CHAxis;
			UINT	m_CHSend;
			UINT	m_CHReceive;
			};
		CContext * m_pCtx;

		// Data Members
		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		WORD	m_wPtr;
		BYTE	m_bCheck;
		static	YLEGENDCmdDef CODE_SEG CL[];
		YLEGENDCmdDef FAR * m_pCL;
		YLEGENDCmdDef FAR * m_pItem;

		// Hex Lookup
		LPCTXT	m_pHex;
		char	cFormat[32];
		DWORD	m_dPrec;
		DWORD	m_Error;

		// Initiate
		BOOL	SendEcho(void);
		BOOL	SendFormat(void);
		BOOL	SendPFormat(void);
		BOOL	SendVFormat(void);
	
		// Opcode Handlers
		UINT	Write(AREF Addr, PDWORD pData);
		BOOL	Read(AREF Addr);
		void	AddVarRead( UINT uAd );
		void	AddCommand( void );
		void	AddParam( char * cParam );
		void	PutText(LPCTXT pCmd);
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(PDWORD pData, WORD wCount);
		
		// Transport Layer
		BOOL	Transact(BOOL fWantReply);
		void	Send(void);
		BOOL	GetReply(UINT uTime);
		BOOL	GetResponse(PDWORD pData, UINT uType);
		
		// Port Access
		void	Put(void);
		WORD	Get(void);

		void	SetpItem( UINT uID );
		void	SetAxisLetter( UINT uAxes );
		void	SetAxisData( UINT uAxes, DWORD dData, DWORD uMaxFactor );
		void	SetPrec( UINT uID );
		void	PutNumber( DWORD wNum );
		DWORD	GetNumber( void );
		BOOL	IsUserVariable( void );
		DWORD	DWordToFloat( DWORD dDword, DWORD dDP );
		DWORD	FloatToDWord( float fFloat );
		void	AdjustNumber( char * cStart, char * cReturn );
		void	UpdateFormats( char * s );
		BOOL	EthernetReadResponse( PDWORD pData );
		DWORD	IPToDWORD( void );
		void	IPToString( DWORD dIP );
		BOOL	GetHexDigit( UINT * p );
		UINT	SetHandleLetter( UINT uH, UINT uMax );
	};

#define	CAB	1
#define	CAB1	2
#define	CAC	3
#define	CAD	4
#define	CAF	5
#define	CAF1	6
#define	CAI	7
#define	CAL	8
#define	CAM	9
#define	CAO	10
#define	CAP	11
#define	CAR	12
#define	CAS	13
#define	CAT	14
#define	CBG	15
#define	CBL	16
#define	CBN	17
#define	CBP	18
#define	CBV	19
#define	CCB	20
#define	CCD	21
#define	CCE	22
#define	CCM	23
#define	CDC	24
#define	CDE	25
#define	CDP	26
#define	CDT	27
#define	CDV	28
#define	CEA	29
#define	CEB	30
#define	CEC	31
#define	CEG	32
#define	CEM	33
#define	CEQ	34
#define	CER	35
#define	CFA	36
#define	CFE	37
#define	CFI	38
#define	CFL	39
#define	CFV	40
#define	CGA	41
#define	CGR	42
#define	CHM	43
#define	CHX	44
#define	CIL	45
#define	CIP	46
#define	CIT	47
#define	CJG	48
#define	CKD	49
#define	CKI	50
#define	CKP	51
#define	CMC	52
#define	CMF	53
#define	CMM	54
#define	CMO	55
#define	CMR	56
#define	CMT	57
#define	CNA	58
#define	COB	59
#define	COE	60
#define	COF	61
#define	COP	62
#define	CPA	63
#define	CPR	64
#define	CRL	65
#define	CRS	66
#define	CSB	67
#define	CSH	68
#define	CSP	69
#define	CST	70
#define	CTB	71
#define	CTC	72
#define	CTD	73
#define	CTE	74
#define	CTI	75
#define	CTL	76
#define	CTM	77
#define	CTP	78
#define	CTS	79
#define	CTT	80
#define	CTV	81
#define	CTW	82
#define	CVA	83
#define	CVD	84
#define	CVE	85
#define	CVR	86
#define	CVS	87
#define	CVT	88
#define	CWC	89
#define	CWT	90
#define	CPF	POSITIONFORMAT
#define	CVF	VARIABLEFORMAT
#define	CZP	FIRSTUSERVAR
#define	CZQ	FIRSTUSERVAR+1
#define	CZR	FIRSTUSERVAR+2
#define	CZS	FIRSTUSERVAR+3
#define	CZT	FIRSTUSERVAR+4
#define	CZU	FIRSTUSERVAR+5
#define	CZV	FIRSTUSERVAR+6
#define	CZW	FIRSTUSERVAR+7
#define	CZX	FIRSTUSERVAR+8
#define	CZY	FIRSTUSERVAR+9
#define	CIA	103
#define	CIH	104
#define	CIHA	105
#define	CIHB	106
#define	CIHC	107
#define	CIHD	108
#define	CIHE	109
#define	CCH	110
#define	CCHA	111
#define	CCHB	112
#define	CCHC	113
#define	CXQ	114

// End of File
