#include "intern.hpp"

#include "wpmii.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Stiebel WPMII Master Serial Driver
//

// Instantiator

INSTANTIATE(CWpmIIMasterSerialDriver);

// Constructor

CWpmIIMasterSerialDriver::CWpmIIMasterSerialDriver(void)
{
	m_Ident         = DRIVER_ID;

	m_Ptr		= 0;

	m_Check		= 0;
	
	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx));
	}

// Destructor

CWpmIIMasterSerialDriver::~CWpmIIMasterSerialDriver(void)
{
	}

// Configuration

void MCALL CWpmIIMasterSerialDriver::Load(LPCBYTE pData)
{
	}

	
void MCALL CWpmIIMasterSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CWpmIIMasterSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CWpmIIMasterSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CWpmIIMasterSerialDriver::DeviceOpen(IDevice *pDevice)
{
	return CMasterDriver::DeviceOpen(pDevice);
	}

CCODE MCALL CWpmIIMasterSerialDriver::DeviceClose(BOOL fPersist)
{
      	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CWpmIIMasterSerialDriver::Ping(void)
{	
	if( SendInit() ) {

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CWpmIIMasterSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uAdd = FindAdd(Addr.a.m_Table, Addr.a.m_Offset);

	if( !IsRead(uAdd) ) {

		pData[0] = 0;

		return uCount;
		}
		
	Begin();

	AddByte(FindByte3(uAdd, Addr.a.m_Table));

	AddByte(1);	// BYTE4

	AddByte(FindByte5(Addr.a.m_Offset, Addr.a.m_Table));

	AddByte(250);	// BYTE6

	AddWord(uAdd);	

	AddWord(0);	// BYTE9-10

	if( Transact() ) {

		WORD x = PU2(m_bRx + 8)[0];

		pData[0] = LONG(SHORT(MotorToHost(x)));

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CWpmIIMasterSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uAdd = FindAdd(Addr.a.m_Table, Addr.a.m_Offset);

	if( !IsWrite(uAdd) ) {

		return uCount;
		}
	
	Begin();

	AddByte(FindByte3(uAdd, Addr.a.m_Table));

	AddByte(0);	// BYTE4

	AddByte(FindByte5(Addr.a.m_Offset, Addr.a.m_Table));

	AddByte(250);	// BYTE6

	AddWord(uAdd);	

	AddWord(LOWORD(pData[0]));	

	if( Transact() ) {

		Sleep(500);

		return 1;
		}

	return CCODE_ERROR;
	}

// Implementation

BOOL CWpmIIMasterSerialDriver::SendInit(void)
{
	Begin();

	AddByte(13);	// BYTE3

	AddByte(1);	// BYTE4

	AddByte(0);	// BYTE5

	AddByte(11);	// BYTE6

	AddWord(0);	// BYTE7-8

	AddWord(0);	// BYTE9-10

	if( Transact() ) {

		return TRUE;
		}

	return FALSE;
	}

void CWpmIIMasterSerialDriver::Begin(void)
{
	m_Ptr = 0;

	m_Check = 0;

	AddByte(13);

	AddByte(0);
	}

void CWpmIIMasterSerialDriver::AddByte(BYTE bByte)
{
	m_bTx[m_Ptr++] = bByte;

	m_Check += bByte;
	}

void CWpmIIMasterSerialDriver::AddWord(WORD wWord)
{
	AddByte(HIBYTE(wWord));

	AddByte(LOBYTE(wWord));
	}

void CWpmIIMasterSerialDriver::End(void)
{
	WORD wCheck = m_Check;
	
	AddWord(wCheck);
	}

BOOL CWpmIIMasterSerialDriver::Transact(void)
{
	End();
	
	if( Send() ) {

		if( Recv() ) {

			if( CheckFrame() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CWpmIIMasterSerialDriver::Send(void)
{
	m_pData->Write(m_bTx, m_Ptr, FOREVER);

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_Ptr; u++ ) {

		AfxTrace("<%u>", m_bTx[u]);
		}		

*/	return TRUE;
	}

BOOL CWpmIIMasterSerialDriver::Recv(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_Ptr = 0;

	m_Check = 0;

//	AfxTrace("\nRx : ");

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

//		AfxTrace("<%u>", uData);

		m_bRx[m_Ptr++] = uData;
		
		if( m_Ptr < (sizeof(m_bRx) - 1) ) {

			m_Check += uData;
			}

		if( m_Ptr == sizeof(m_bRx) ) {

			WORD x = PU2(m_bRx + 10)[0];

			if( m_Check == LONG(SHORT(MotorToHost(x))) ) {

				return TRUE;
				}

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CWpmIIMasterSerialDriver::CheckFrame(void)
{
	if( m_bTx[2] == 13 ) {	// Init	

		return (m_bRx[6] == 0x55);
		}

	if( m_bTx[3] == 1 ) {	// Read

		if( ( m_bRx[2] ==  13 )		&&
		    ( m_bRx[3] ==   2 )		&&
		    ( m_bRx[4] ==   0 )		&&
		    ( m_bRx[5] == 250 )		&&
		    ( m_bTx[6] == m_bRx[6] )	&&
		    ( m_bTx[7] == m_bRx[7] ) )  {

			return TRUE;
			}

		return FALSE;
		}

	if( m_bTx[3] == 0 ) {	// Write

		return (m_bRx[0] == 0x55);
		}

	return FALSE;
	}

// Helpers

BOOL CWpmIIMasterSerialDriver::IsRead(UINT uAdd)
{
	if( ( uAdd > 2815 ) && ( uAdd < 2956 ) ) {

		return TRUE;
		}
	
	switch( uAdd ) {

		case  274:  return TRUE;
		case   12:  return TRUE;
		case   14:  return TRUE;
		case    3:  return TRUE;
		case   15:  return TRUE;
		case    4:  return TRUE;
		case   17:  return TRUE;
		case   18:  return TRUE;
		case   22:  return TRUE;
		case  471:  return TRUE;
		case  448:  return TRUE;
		case  469:  return TRUE;
		case  470:  return TRUE;
		case  488:  return TRUE;
		case  468:  return TRUE;
		case  432:  return TRUE;
		case  374:  return TRUE;
		case  412:  return TRUE;

		case  270:  return TRUE;
		case   19:  return TRUE;
		case 2566:  return TRUE;
		case    5:  return TRUE;
		case    8:  return TRUE;

		case 1640:  return TRUE;
		case 1641:  return TRUE;

		}

	return FALSE;
	}

BOOL CWpmIIMasterSerialDriver::IsWrite(UINT uAdd)
{
	switch( uAdd ) {

		case  274:  return TRUE;
		case  270:  return TRUE;
		case   19:  return TRUE;
		case 2566:  return TRUE;
		case    5:  return TRUE;
		case    8:  return TRUE;

		case  251:  return TRUE;

		}

	return FALSE;
	}

BYTE CWpmIIMasterSerialDriver::FindByte3(UINT uAdd, UINT uTable)
{
	if( uTable >= addrNamed ) {
	
		switch( uAdd ) {

			case  17:
			case  18:
			case 270:
			case   5:
			case   8:
				    return 6;

			case 251:
				    return 9;
			}
		}

	return 3;
	}

BYTE CWpmIIMasterSerialDriver::FindByte5(UINT uAdd, UINT uTable)
{
	if( uTable >= addrNamed ) {
	
		UINT uAddr = uAdd & 0x7FFF;

		switch( uAddr ) {

			case 17:
	       		case 18:
			case 270:
	       		case 5:
	       		case 8:

				return ((uAdd & 0x8000) ? 2 : 1);
			}
		}

	return 0;
	}

UINT CWpmIIMasterSerialDriver::FindAdd(UINT uTable, UINT uAdd)
{
	if( uTable == addrNamed ) {

		return (uAdd & 0x7FFF);
		}

	MakeMin(uAdd, 20);

	UINT uNew = 2815;
	
	uNew += (( uAdd - 1 ) * 7);

	uNew += uTable;

	return uNew;
	}

// End of file

