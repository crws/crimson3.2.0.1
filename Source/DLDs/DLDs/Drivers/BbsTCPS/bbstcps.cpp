
#include "intern.hpp"

#include "bbstcps.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock UDP Slave Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CBBBSAPTCPSDriver);

// Constructor

CBBBSAPTCPSDriver::CBBBSAPTCPSDriver(void)
{
	m_Ident	= DRIVER_ID;

	InitGlobal();

	fIsListReq = FALSE;
	}

// Destructor

CBBBSAPTCPSDriver::~CBBBSAPTCPSDriver(void)
{
	CloseDown();
	}

// Configuration

void MCALL CBBBSAPTCPSDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uPort  = GetWord(pData);

		m_uCount = GetWord(pData);
		}

	m_pSockList = new SOCKET [m_uCount];
	}
	
// Management

void MCALL CBBBSAPTCPSDriver::Attach(IPortObject *pPort)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		m_pSock = &m_pSockList[n];

		OpenSocket(n);
		}
	}

void MCALL CBBBSAPTCPSDriver::Detach()
{
	CloseDown();

	CSlaveDriver::Detach();
	}

// Device

CCODE MCALL CBBBSAPTCPSDriver::DeviceOpen(IDevice *pDevice)
{
	CSlaveDriver::DeviceOpen(pDevice);

	PCBYTE pData = pDevice->GetConfig();

	if( GetWord(pData) == 0x1234 ) {

		WORD wKey = GetWord(pData);

		switch( wKey ) {

			case 0xABCD:	m_bIsC3 = 2; break;
			case 0xBCDE:	m_bIsC3 = 3; break;

			default:	return CCODE_ERROR | CCODE_HARD;
			}

		LoadTags(pData);
		
		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CBBBSAPTCPSDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {
	
		CloseDown();
		}

	CloseListBuffers();

	return CSlaveDriver::DeviceClose(fPersist);
	}

// Entry Point

void MCALL CBBBSAPTCPSDriver::Service(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		m_pSock = &m_pSockList[n];

//**/		Sleep(200);	// slow down for debug

		if( m_pSock->m_pSocket ) {

			UINT Phase;

			m_pSock->m_pSocket->GetPhase(Phase);

			switch( Phase ) {

				case PHASE_OPEN:

//**/					if( bLastPhase != 'P' ) { AfxTrace0(" P"); bLastPhase = 'P'; }

					if( !m_pSock->m_fBusy ) {

						m_pSock->m_uTime = GetTickCount();

						m_pSock->m_fBusy = TRUE;
						}

					ReadData();

					break;

				case PHASE_CLOSING:

//**/					if( bLastPhase != 'C' ) { AfxTrace0(" C"); bLastPhase = 'C'; }

					m_pSock->m_pSocket->Close();

					break;

				case PHASE_ERROR:

//**/					if( bLastPhase != 'E' ) { AfxTrace0(" E"); bLastPhase = 'E'; }

					CloseSocket();

					OpenSocket(n);

					break;
				default:

//**/					if( bLastPhase != '0' ) { AfxTrace0(" 0"); bLastPhase = '0'; }

					if( UINT(GetTickCount() - m_pSock->m_uTime) >= ToTicks(20000) ) {

						CloseSocket();

						Sleep(20);

						OpenSocket(n);

//**/						AfxTrace0(".");
						}
					break;
				}
			}
		else
			OpenSocket(n);
		}

	ForceSleep(20);
	}

// Receive Processing

void CBBBSAPTCPSDriver::ReadData(void)
{
	UINT uSize = 300 - m_pSock->m_wPtr;

	while( uSize ) {

		m_pSock->m_pSocket->Recv(m_pSock->m_pData + m_pSock->m_wPtr, uSize);

		if( uSize ) {

//**/			UINT ux = m_pSock->m_wPtr;
//**/			PBYTE p = m_pSock->m_pData;
//**/			UINT k  = 0;
//**/			UINT uState = 0;
//**/			UINT z = (UINT)p[0];
//**/			while( k < uSize ) {
//**/				BYTE b = p[k];
//**/				switch( uState ) {
//**/					case 0:
//**/						AfxTrace3("\r\nHeader %d %d:\r\n<%2.2x>", m_pSock->m_wPtr, uSize, b);
//**/						uState = 1;
//**/						break;
//**/					case 1:
//**/						if( k < z ) AfxTrace1("<%2.2x>", b);
//**/						else if( k == z ) {
//**/							AfxTrace1("\r\nPacket Header:\r\n<%2.2x>", b);
//**/							uState = 2;
//**/							}
//**/						break;
//**/					case 2:
//**/						if( k < z + 11 ) {
//**/							AfxTrace1("<%2.2x>", b);
//**/							}
//**/						else if( k == z + 11 ) {
//**/							AfxTrace1("\r\nData:<%2.2x>", b);
//**/							uState = 3;
//**/							}
//**/						break;
//**/					case 3:
//**/						AfxTrace1("<%2.2x>", b);
//**/						break;
//**/					}
//**/				k++;
//**/				}

			m_pSock->m_wPtr += uSize;

			m_pSock->m_uTime = GetTickCount();

			if( m_pSock->m_wPtr >= SZHDRSTD ) {

				UINT u = RcvDone(m_pSock->m_wPtr);

				if( u == 2 ) {

					if( DoPackets() ) {

						m_pSock->m_wPtr = 0;

						m_pSock->m_fBusy = FALSE;

						return;
						}

					Sleep(30);

					uSize = 300;

					m_pSock->m_wPtr = 0;
					}

				else if( u == 1 ) {

					m_pSock->m_wPtr = 0;
					}

				else {
					Sleep(30);
					}
				}
			}
		}

	if( (UINT)(GetTickCount() - m_pSock->m_uTime) >= ToTicks(20000) ) {

		m_pSock->m_pSocket->Close();

		m_pSock->m_fBusy = FALSE;

		m_pSock->m_wPtr  = 0;

//**/		AfxTrace0(".");

		return;
		}
	}

WORD CBBBSAPTCPSDriver::RcvDone(WORD wSize)
{
	PBYTE p		= m_pSock->m_pData;

	GetMainHeader();

	if( !(BOOL)m_InHeader.PacketCt ) {

		return 1;
		}

	MAINHDR * pMH = m_pInHeader;

	WORD wSrce = pMH->HeadSize;				// point to 1st packet header

	WORD wDest = 0;

	for( WORD wPktNum = 0; wPktNum < pMH->PacketCt; wPktNum++ ) {

		PBYTE pData	= &p[wSrce];			// start of packet header

		UINT uPktSize	= GetPktHeader(pData, wPktNum);

		if( wSize < wSrce + uPktSize ) {

			return 0;				// not enough received
			}

		m_pInD = m_pInPktData[wPktNum];

		m_pInD->PktNum	= wPktNum;			// this packet number

		m_pInD->Position = wDest;			// first data position

		m_pInD->Operand	= pData[11];			// function to execute

		wSize = uPktSize - 11;				// size of data field

		memcpy( &m_bRx[wDest], &pData[wSrce - pMH->HeadSize + 11], wSize );

		wSrce += uPktSize;				// start of next packet header

		wDest  = wSize;					// next packet data position
		}

	m_wEndPos = wDest;

	m_bRER    = 0;						// set no receive errors

	return 2;						// process received data
	}

void CBBBSAPTCPSDriver::GetMainHeader(void)
{
	WORD wPos	= 0;

	MAINHDR *pHead	= m_pInHeader;

	PBYTE pData	= m_pSock->m_pData;

	pHead->HeadSize	= GetRxWord (pData, &wPos);
	pHead->PacketCt	= GetRxWord (pData, &wPos);
	pHead->MsgFlags	= GetRxWord (pData, &wPos);
	pHead->PktSeq	= GetRxDWord(pData, &wPos);
	pHead->PktAck	= GetRxDWord(pData, &wPos);

	MakeMin(pHead->PacketCt, 4);

	if( pHead->HeadSize == SZHDREXT ) {

		m_fExtend	= TRUE;
		pHead->ThisIP	= GetRxDWord(pData, &wPos);
		pHead->Spare	= GetRxDWord(pData, &wPos);
		}

	memcpy(m_pOutHeader, m_pInHeader, sizeof(MAINHDR));
	}

UINT CBBBSAPTCPSDriver::GetPktHeader(PBYTE pData, WORD wPktItem)
{
	m_pInH			= m_pInPktHead[wPktItem];

	INPKTH * pHead		= m_pInH;

	WORD w = 0;

	pHead->PktDataLen	= GetRxWord(pData, &w);
	pHead->BSAP		= GetRxWord(pData, &w);
	pHead->MEC		= GetRxByte(pData, &w);
	pHead->MsgSeqNum	= GetRxWord(pData, &w);
	pHead->GlobAddr		= GetRxWord(pData, &w);
	pHead->FuncCode		= GetRxByte(pData, &w);
	pHead->NRT		= GetRxByte(pData, &w);

	return pHead->PktDataLen;
	}

BOOL CBBBSAPTCPSDriver::DoPackets(void)
{
	BOOL fSend = FALSE;

	m_wTPtr    = 0;

	for( WORD i = 0; i < m_InHeader.PacketCt; i++ ) {

		m_pInH		= m_pInPktHead[i];
		m_pInD		= m_pInPktData[i];

		m_pOutH		= m_pOutPktHead[i];
		m_pOutD		= m_pOutPktData[i];

		m_pOutH->BSAP		= m_pInH->BSAP;
		m_pOutH->MEC		= m_pInH->MEC;
		m_pOutH->MsgSeqNum	= m_pInH->MsgSeqNum;
		m_pOutH->GlobAddr	= m_pInH->GlobAddr;

		if( !m_pInH->BSAP ) { // is BSAP packet

			BYTE b = m_pInD->Operand;

			if( IsPollOrAck(b) ) {

				b == POLLMSG ? AddPollAck() : AddAckNak(b);

				m_pInD->Operand		= POLLMSG;
				m_pOutD->PollValue	= ACKPOLL;
				m_pOutH->PktDataLen	= 3;

				fSend = TRUE;
				}

			else {
				WORD wSize	= 0;

				if( b & 0x80 ) {

					switch( b ) {

						case OFCWRBYA:

							if( WriteByAddress(i, &wSize) ) fSend = TRUE;

							else {
								if( wSize > MAXSEND ) m_OutHeader.PacketCt = i;
								}

							break;

						case OFCWRBYN:

							if( WriteByName(i, &wSize) ) fSend = TRUE;

							else {
								if( wSize > MAXSEND ) m_OutHeader.PacketCt = i;
								}

							break;
						}
					}

				else {
					SetFieldSelects(i);

					switch( b ) {

						case OFCRDBYA:

							if( ReadByAddress(i, &wSize, FALSE) ) fSend = TRUE;

							else {
								if( wSize > MAXSEND ) m_OutHeader.PacketCt = i;
								}

							break;

						case OFCRDBYN:

							if( ReadByName(i, &wSize, FALSE) ) fSend = TRUE;

							else {
								if( wSize > MAXSEND ) m_OutHeader.PacketCt = i;
								}

							break;

						case OFCRDBYL:
						case OFCRDBYLC:

							fIsListReq = TRUE;

							if( ReadByList(b == OFCRDBYL, i, &wSize) ) fSend = TRUE;

							else {
								if( wSize > MAXSEND ) m_OutHeader.PacketCt = i;
								}

							fIsListReq = FALSE;
							break;
						}
					}
				}
			}

		else {
			AddNak();

			fSend = TRUE;
			}
		}

	if( fSend ) Send();

	return (BOOL)m_InHeader.PacketCt;
	}

// Read Routines
BOOL CBBBSAPTCPSDriver::ReadByList(BOOL fFirst, UINT uPktNum, PWORD pSize)
{
	INPKTD *pPktHdr = m_pInPktData[uPktNum];

	if( !(pPktHdr->FSS[1] & 0x8) ) { // check if names are requested

		m_wNameListItem = 0;

		m_bRER = RERINVDB;

		return TRUE;
		}

	WORD wPkt0 = m_wTPtr;

	WORD wPos;

	if( fFirst ) { // code is c -> first request

		m_wNameListItem = 0;

		m_bList         = m_bRx[pPktHdr->Position + 5 + pPktHdr->FSS[0]]; // list number

		InitRBL(pPktHdr->PktNum);

		if( !m_wListCount ) {

			m_bRER = RERINVDB;

			return TRUE;
			}
		}

	else { // code is d -> continue request
		wPos		= pPktHdr->Position + 7 + pPktHdr->FSS[0];

		BYTE bList	= GetRxByte(m_bRx, &wPos); // list number to get

		if( bList != m_bList ) {

			m_bRER = RERINVDB;

			CloseListBuffers();

			return TRUE;
			}

		m_wNameListItem = GetRxWord(m_bRx, &wPos); // Item in list for start
		}

	UINT uLI        = (UINT)m_wNameListItem;
	UINT uLC	= (UINT)m_wListCount;

	BOOL fHasRoom   = TRUE;

	UINT uThisCount = 0;

	m_pOutH->PktDataLen = wPkt0;

	UINT i = 0;

	for( i = uLI; i < uLC; i++ ) {

		if( fHasRoom ) {

			GetVarInfo(i);

			if( AddFieldSels() ) uThisCount++;

			m_pOutH->PktDataLen = m_wTPtr - wPkt0;
			}

		else {
			i = uLC;
			}
		}

	if( uThisCount ) { // at least one item to send

		if( !fHasRoom ) {

			m_bRER = 1; // server has to request more data

			m_wNameListItem += uThisCount;

			if( i == m_wListCount ) {

				m_wNameListItem = 0;
				}
			}

		else {
			m_wNameListItem = 0;
			}

		m_bQuantity = uThisCount;
		}

	CloseListBuffers();

	return CheckTxSize(wPkt0, pSize);
	}

BOOL CBBBSAPTCPSDriver::ReadByName(UINT uPktNum, PWORD pSize, BOOL fHasErr)
{
	INPKTD *pPktHdr = m_pInPktData[uPktNum];

//**/	AfxTrace2("\r\n\nRead By NAME Pos=%d Ct=%d", pPktHdr->Position, m_pOutHeader->PacketCt);

	WORD wPkt0 = m_wTPtr;

	WORD wPos  = pPktHdr->Position + pPktHdr->FSS[0] + 3;	// FC Pos + FSS count + FSS's + SecCode -> Num

	m_bQty      = m_bRx[wPos++]; // number of names requested

	m_bQuantity = m_bQty;

	VARINFO **pVI;

	pVI = new VARINFO * [m_bQty];

	for( UINT k = 0; k < m_bQty; k++ ) {

		pVI[k] = new VARINFO;
		}

	m_wVarSel    = 0;

	for( UINT i = 0; i < m_bQty; i++ ) {

		m_pVarInfo = pVI[m_wVarSel];

		WORD wLen  = 0;

		BYTE bBad  = LoadInfoFromName(wPos, &wLen) ? RERMATCH : 0;

		if( fHasErr ) {

			if( !bBad && m_bRER ) {

				AddByte(0);
				}
			}

		else {
			if( bBad ) { // error found, restart

				m_bRER  = RERMATCH;

				m_wTPtr = wPkt0;

				for( k = 0; k < m_bQty; k++ ) {

					delete pVI[k];
					}

				delete pVI;

				return ReadByName(uPktNum, pSize, TRUE);
				}
			}

		PutPacketData(bBad);

		wPos += wLen;

		m_wVarSel++;
		}

	for( k = 0; k < m_bQty; k++ ) {

		delete pVI[k];
		}

	delete pVI;

	m_pOutH->PktDataLen = m_wTPtr - wPkt0;

	return CheckTxSize(wPkt0, pSize);
	}

BOOL CBBBSAPTCPSDriver::ReadByAddress(UINT uPktNum, PWORD pSize, BOOL fHasErr)
{
	INPKTD *pPktHdr = m_pInPktData[uPktNum];

//**/	AfxTrace2("\r\n\nRead By ADDR Pos=%d Ct=%d", pPktHdr->Position, m_pOutHeader->PacketCt);

	WORD wPkt0 = m_wTPtr;

	WORD wPos  = pPktHdr->Position + pPktHdr->FSS[0] + 5;

	m_bQty       = m_bRx[wPos++]; // number of addresses requested

	m_bQuantity  = m_bQty;

	VARINFO **pVI;

	pVI = new VARINFO * [m_bQty];

	for( UINT k = 0; k < m_bQty; k++ ) {

		pVI[k] = new VARINFO;
		}

	m_wVarSel    = 0;

	for( UINT i = 0; i < m_bQty; i++ ) {

		m_pVarInfo = pVI[m_wVarSel];

		BYTE bBad  = LoadInfoFromAddr(wPos, TRUE);

		if( fHasErr ) {

			if( !bBad ) AddByte(0);
			}

		else {
			if( bBad ) { // error found, restart

				m_bRER  = RERMATCH;

				m_wTPtr = wPkt0;

				for( k = 0; k < m_bQty; k++ ) {

					delete pVI[k];
					}

				delete pVI;

				return ReadByAddress(uPktNum, pSize, TRUE);
				}
			}

		PutPacketData(bBad);

		wPos += 2;

		m_wVarSel++;
		}

	for( k = 0; k < m_bQty; k++ ) {

		delete pVI[k];
		}

	delete pVI;

	m_pOutH->PktDataLen    = m_wTPtr - wPkt0;

	return CheckTxSize(wPkt0, pSize);
	}

// Read Helpers
void CBBBSAPTCPSDriver::InitRBL(WORD wPktNum)
{
	m_wNameListItem	= 0;

	PDWORD pAddrTemp = new DWORD [m_wTagCount];
	PDWORD pDataTemp = new DWORD [m_wTagCount];
	PWORD  pPosnTemp = new WORD  [m_wTagCount];

	CountListElements(pAddrTemp, pDataTemp, pPosnTemp, wPktNum);

	CloseListBuffers();

	m_pListAddr = new DWORD [m_wListCount];
	m_pListData = new DWORD [m_wListCount];
	m_pListPosn = new WORD  [m_wListCount];

	m_LBMagic   = 0x12345678;

	if( m_pListAddr && m_pListData && m_pListPosn ) {

		memcpy(m_pListAddr, pAddrTemp, m_wListCount * 4);
		memcpy(m_pListData, pDataTemp, m_wListCount * 4);
		memcpy(m_pListPosn, pPosnTemp, m_wListCount * 2);
		}

	else {
		CloseListBuffers();
		}

	delete pAddrTemp;
	delete pDataTemp;
	delete pPosnTemp;

	pAddrTemp = NULL;
	pDataTemp = NULL;
	pPosnTemp = NULL;
	}

void CBBBSAPTCPSDriver::CountListElements(PDWORD pA, PDWORD pD, PWORD pP, WORD wPktNum)
{
	WORD w  = 0;

	for( UINT i = 0; i < m_wTagCount; i++ ){

		DWORD d = m_pdAddrList[i];

		if( (LOWORD(d) >> 10) == m_bList ) {

			CAddress Addr;

			DWORD Data[1];

			Addr.m_Ref = d;

			if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) { // add address if data is accessible

				pP[w]	= i; // position in full list

				pD[w]	= LongToReal(Addr.a.m_Type, *Data);

				pA[w++]	= d; // Addr.m_Ref
				}
			}
		}

	m_wListCount = w;
	}

void CBBBSAPTCPSDriver::GetVarInfo(UINT uIndex)
{
	DWORD d = m_pListAddr[uIndex];

	m_pVarInfo = &SVarInfo;

	m_pVarInfo->Addr    = d;
	m_pVarInfo->Data    = m_pListData[uIndex];
	m_pVarInfo->Logical = !(d >> 24);
	m_pVarInfo->MSDAddr = LOWORD(d);
	m_pVarInfo->NameInx = m_pwNameIndex[m_pListPosn[uIndex]];
	}

BOOL CBBBSAPTCPSDriver::ParseVarName(PCTXT Name, UINT uSelect, PBYTE pItem)
{
	UINT uNameLen = strlen((const char *)Name);

	memset(pItem, 0, uNameLen);

	for( UINT i = 0, j = 0; i < uNameLen; i++ ) {

		BYTE b = Name[i];

		if( !uSelect ) {

			if( b == '.' ) return (BOOL)pItem[0];

			pItem[j++] = b;
			}

		else {
			if( b == '.' ) uSelect--;
			}

		return (BOOL)pItem[0];
		}

	return FALSE;
	}

void CBBBSAPTCPSDriver::SetFieldSelects(UINT uPktNum)
{
	INPKTD *pPktDat = m_pInPktData[uPktNum];

	WORD wPos = pPktDat->Position + 1; // Packet Data FSS byte

	BYTE bFSS = m_bRx[wPos] & 0x7; // bit 4 is not defined in spec

	if( !bFSS ) return; // no response field selected

	memset(pPktDat->FSS, 0, 5);

	BYTE bCount = 0;

	BYTE bMask  = 1;

	for( WORD i = 1; i < 4; i++, bMask <<= 1 ) {

		if( bFSS & bMask ) {

			pPktDat->FSS[i] = m_bRx[wPos + i];

			bCount++;
			}
		}

	pPktDat->FSS[0] = bCount;
	}

BOOL CBBBSAPTCPSDriver::AddFieldSels(void)
{
	WORD wPkt0 = m_wTPtr;

	AddField1();
	AddField2();
	AddField3();

	if( m_wTPtr < MAXSEND ) return TRUE;

	m_wTPtr = wPkt0; // discard this item;

	return FALSE;
	}

void CBBBSAPTCPSDriver::AddField1(void)
{
	VARINFO *p	= m_pVarInfo;

 	BYTE b     = m_pInD->FSS[1];

	if( b ) {

		DWORD dData = p->Data;

		BOOL fLog   = p->Logical;
		BOOL fTrue  = (BOOL)(dData);

		if( b & 0x80 ) AddByte(fLog ? 0 : 2);		// Type = logical or analog
 
		if( b & 0x40 ) {								       

			if( fLog ) AddByte(fTrue ? 1 : 0);	// digital value

			else AddLong(dData);			// analog value
			}

		if( b & 0x20 ) {				// Units Text = 6 spaces
			AddLong(0x20202020);
			AddWord(0x2020);
			}

		if( b & 0x10 ) AddWord(p->MSDAddr);		// Msd Address

		if( b & 0x08 ) {

			AddTextPlusNull((PCTXT)&m_pbNameList[p->NameInx]);
			}

		if( b & 0x04 ) AddByte(0);			// Alarm Status

		if( b & 0x02 ) {

			AddByte('?');			// Descriptor = 1 space
			AddByte(0);
			}

		if( b & 0x01 ) {

			if( fLog ) {

				AddByte('O');
				AddByte('N');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				AddByte('O');
				AddByte('F');
				AddByte('F');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				}
			}
		}
	}

void CBBBSAPTCPSDriver::AddField2(void)
{
	VARINFO *p = m_pVarInfo;

 	BYTE b     = m_pInD->FSS[2];

	if( b ) {

		if( b & 0x80 ) AddByte(0);
		if( b & 0x40 ) AddByte(0);

		if( b & 0x38 ) {

			PBYTE pName = &m_pbNameList[p->NameInx];

			PBYTE pItem = new BYTE [strlen((const char *)pName)];

			UINT uSkip  = pName[0] == '@' ? 1 : 0;

			if( b & 0x20 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)&pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			uSkip++;

			if( b & 0x10 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)&pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			uSkip++;

			if( b & 0x08 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)&pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			delete pItem;

			pItem = NULL;
			}

		if( b & 0x04) AddWord(0);
		if( b & 0x02) AddWord(0);
		if( b & 0x01) AddWord(0);
		}
	}

void CBBBSAPTCPSDriver::AddField3(void)
{
	VARINFO *p = m_pVarInfo;

 	BYTE b     = m_pInD->FSS[3];

	if( b ) {

		if( b & 0x80 ) AddWord(0);
		if( b & 0x40 ) AddWord(0);
		if( b & 0x20 ) AddWord(0);
		if( b & 0x10 ) AddWord(0);
		if( b & 0x08 ) AddByte(0);
		if( b & 0x04 ) AddByte(0);

		if( b & 0x02 ) {

			if( p->Logical ) AddByte(p->Data ? 1 : 0);

			else AddLong(p->Data);
			}

		if( b & 0x01 ) AddWord(0);
		}
	}

void CBBBSAPTCPSDriver::PutPacketData(BYTE bBad)
{
	if( bBad ) AddByte(EERMATCH); // signal error on this item

	else AddFieldSels();
	}

// Item info
BYTE CBBBSAPTCPSDriver::LoadInfoFromName(WORD wPos, PWORD pLen)
{
	const char * p	= (const char *)&m_bRx[wPos];

	WORD wLen	= strlen(p);

	WORD wIndex	= 0;

	while( wIndex < m_wTagCount ) {

		WORD wPos1 = m_pwNameIndex[wIndex];

		const char * pPos = (const char *)&m_pbNameList[wPos1];

		if( (WORD)strlen(pPos) == wLen ) {

			BOOL f = strnicmp(p, pPos, (UINT)wLen);

			if( !strnicmp(p, pPos, (UINT)wLen) ) {

				if( GetReadInfo(wIndex) ) {

					*pLen = wLen + 1;

					return 0;
					}

				else return RERMATCH;
				}
			}

		wIndex++;
		}

	return RERMATCH;
	}

BYTE CBBBSAPTCPSDriver::LoadInfoFromAddr(WORD wPos, BOOL fRead)
{
	WORD wAR = m_bRx[wPos] + (m_bRx[wPos + 1] << 8); // received address

	wPos -= 4;

	m_pVarInfo->MSDVers = GetRxWord((PBYTE)m_bRx, &wPos);

	WORD wIndex = 0;

	while( wIndex < m_wTagCount ) {

		if( wAR == LOWORD(m_pdAddrList[wIndex]) ) {

			if( fRead ) {

				return GetReadInfo(wIndex) ? 0 : RERSET;
				}

			FillInfo(wIndex, 0); // only index is relevant for write

			return 0;
			}

		wIndex++;
		}

	return RERSET;
	}

BOOL CBBBSAPTCPSDriver::GetReadInfo(WORD wIndex)
{
	DWORD d = m_pdAddrList[wIndex];

	CAddress A;

	A.m_Ref = d;

	DWORD Data[1];

	if( COMMS_SUCCESS(Read(A, Data, 1)) ) {

		FillInfo(wIndex, LongToReal(A.a.m_Type, Data[0]));

		return TRUE;
		}

	return FALSE;
	}

void CBBBSAPTCPSDriver::FillInfo(WORD wIndex, DWORD dData)
{
	CAddress Addr;

	Addr.m_Ref = m_pdAddrList[wIndex];

	m_pVarInfo->Addr    = Addr.m_Ref;
	m_pVarInfo->Data    = dData;
	m_pVarInfo->Logical = !(BOOL)Addr.a.m_Type;
	m_pVarInfo->MSDAddr = LOWORD(Addr.a.m_Offset);
	m_pVarInfo->NameInx = m_pwNameIndex[wIndex];
	}

// Write routines
BOOL CBBBSAPTCPSDriver::WriteByName(UINT uPktNum, PWORD pSize)
{
	INPKTD *pPktHdr = m_pInPktData[uPktNum];

//**/	AfxTrace0("\r\n\n***** Write by NAME *****");

	WORD wPkt0 = m_wTPtr;

	WORD wPos  = pPktHdr->Position;

	m_bQty     = m_bRx[wPos + 2];

	if( m_bQty ) {

		WORD wErrCt = 0;

		wPos       += 3; // beginning of name 1

		BOOL fOk    = FALSE;

		PBYTE pErr  = new BYTE [m_bQty];

		memset(pErr, 0, m_bQty);

		m_pVarInfo  = &SVarInfo;

		for( WORD i = 0; i < m_bQty; i++ ) {

			WORD wLen = 0;

			BYTE bBad = LoadInfoFromName(wPos, &wLen) ? RERMATCH : 0;

			wPos     += wLen; // point to descriptor

			if( bBad ) {

				m_bRER = bBad;
				}

			else {
				if( !MakeWriteData(&wPos) ) {

					if( m_wTPtr > wPkt0 ) { // valid request, couldn't write

						pErr[i] = EERMATCH;

						wErrCt++;
						}
					}
				}

			if( wPkt0 ) {

				if( !CheckTxSize(wPkt0, pSize) ) {

					m_wTPtr = wPkt0;

					m_bQty  = i;
					}
				}
			}

		for( i = 0; i < m_bQty; i++ ) {

			AddByte(pErr[i]);
			}

		OUTPKTH	*o = m_pOutPktHead[uPktNum];

		o->PktDataLen = m_wTPtr - wPkt0;

		m_pOutHeader->PacketCt = m_bQty;

		*pSize = o->PktDataLen;

		delete pErr;
		}

	m_bQuantity = m_bQty;

	BOOL fOk    = wPkt0 ? CheckTxSize(wPkt0, pSize) : TRUE;

	return fOk;
	}

// Write Routines

BOOL CBBBSAPTCPSDriver::WriteByAddress(UINT uPktNum, PWORD pSize)
{
	INPKTD *pPktDat = m_pInPktData[uPktNum];

//**/	AfxTrace2("\r\n\n***** Write by ADDRESS ***** Sz=%d HPos=%d ", pSize, pPktDat->Position);

	WORD wPkt0 = m_wTPtr;

	WORD wPos  = pPktDat->Position;

	m_bQty     = m_bRx[wPos + 4];

	if( m_bQty ) {

		WORD wErrCt = 0;

		wPos       += 5; // point to address

		BOOL fOk    = FALSE;

		PBYTE pErr  = new BYTE [m_bQty];

		memset(pErr, 0, m_bQty);

		for( WORD i = 0; i < m_bQty; i++ ) {

			BYTE bBad = LoadInfoFromAddr(wPos, FALSE);

			wPos     += 2; // point to descriptor

			if( bBad ) {

				m_bRER = bBad;
				}

			else {
				if( !MakeWriteData(&wPos) ) {

					if( m_wTPtr > wPkt0 ) { // valid request, couldn't write

						pErr[i] = EERMATCH;

						wErrCt++;
						}
					}
				}

			if( wPkt0 ) {

				if( !CheckTxSize(wPkt0, pSize) ) {

					m_wTPtr = wPkt0;

					m_bQty  = i;
					}
				}
			}

		for( i = 0; i < m_bQty; i++ ) {

			AddByte(pErr[i]);
			}

		OUTPKTH *o  = m_pOutPktHead[pPktDat->PktNum];

		o->PktDataLen = m_wTPtr - wPkt0;

		delete pErr;
		}

	m_bQuantity = m_bQty;

	m_pOutHeader->PacketCt = m_bQty;

	BOOL fOk    = wPkt0 ? CheckTxSize(wPkt0, pSize) : TRUE;

	return fOk;
	}

// Write Helpers
BOOL CBBBSAPTCPSDriver::MakeWriteData(PWORD pPos)
{
	WORD wPos = *pPos;

	DWORD Data[1];

	DWORD d;

	switch( m_bRx[wPos++] ) { // type of write

		case 9:	// set logical value on
			if( m_pVarInfo->Logical ) {

				*Data = 1;
				}

			else return FALSE;

			break;

		case 10: // set logical value off
			if( m_pVarInfo->Logical ) {

				*Data = 0;
				}

			else return FALSE;

			break;

		case 11: // set analog value
			if( !m_pVarInfo->Logical ) {

				UINT uType = AREF(m_pVarInfo->Addr).a.m_Type;

				*Data      = RealToLong(uType, (DWORD)(IntelToHost(*PU4(&m_bRx[wPos]))));

				wPos      += 4;

				break;
				}

			else return FALSE;

		default: return FALSE;
		}

	*pPos = wPos; // point to next item

	CAddress Addr;

	Addr.m_Ref = m_pVarInfo->Addr;

	return COMMS_SUCCESS(Write(Addr, Data, 1));
	}

// Transport Layer
void CBBBSAPTCPSDriver::Send(void)
{
	m_pSend    = new BYTE [sizeof(m_bTx)];

	WORD wPos0 = 0;

	AddUDPHeader();

	for( WORD i = 0; i < m_pOutHeader->PacketCt; i++ ) {

		OUTPKTH * pH	= m_pOutPktHead[i];
		OUTPKTD * pD	= m_pOutPktData[i];
		INPKTD	* piD	= m_pInPktData[i];

		if( pH->PktDataLen && pH->PktDataLen < MAXSEND ) {

			WORD wPkt0 = m_wSendPtr;

			AddUDPPacketHeader(pH);

			AddSendByte(m_bRER);

			AddSendByte(m_bQuantity);

			switch( piD->Operand ) {

				case OFCRDBYA:
				case OFCRDBYN:

//					AddDeviceName();
					break;

				case OFCRDBYL:
				case OFCRDBYLC:

					AddSendByte(m_bList);
					AddSendWord(m_wNameListItem);
					break;

				default:
					break;
				}

			UINT uEnd = pH->PktDataLen + wPos0;

			for( UINT j = wPos0; j < uEnd; j++ ) AddSendByte(m_bTx[j]);

			wPos0 += j;

			m_pSend[wPkt0] = m_wSendPtr - wPkt0; // update packet size
			}

		EndFrame();

		SendFrame();
		}

	delete m_pSend;

	m_pSend = NULL;
	}

BOOL CBBBSAPTCPSDriver::SendFrame(void)
{
	UINT uSize0 = (UINT)m_wSendPtr;
	UINT uSize1 = uSize0;

//*xx*/	if( fIsListReq ) {
//**/	AfxTrace0("\r\nSend Frame\r\n  Header:\r\n");
//**/	UINT u0 = m_pSend[0];
//**/	for(UINT k = 0; k < uSize0; k++ ) {
//**/		if( k == u0 ) AfxTrace0("\r\n  Packet Header:\r\n");
//**/		if( k == u0 + 9 ) AfxTrace0("\r\nPacket Data:\r\n");
//**/		AfxTrace1("[%2.2x]", m_pSend[k]);
//**/		}
//*xx*/	}

	if( m_pSock->m_pSocket->Send(m_pSend, uSize1) == S_OK ) {

		if( uSize0 == uSize1 ) {

			m_wMsgFlag = 4;

//**/			AfxTrace0("\r\n*********\n");

			return TRUE;
			}
		}

	return FALSE;
	}

// Ack
void CBBBSAPTCPSDriver::AddAckNak(BYTE bType)
{
	m_bQuantity = 3;
	AddByte(3); // packet length
	AddByte(bType);
	AddByte(0);
	}

void CBBBSAPTCPSDriver::AddPollAck(void)
{
	AddAckNak(ACKPOLL);
	}

void CBBBSAPTCPSDriver::AddDownAck(void)
{
	AddAckNak(ACKDOWN);
	}

void CBBBSAPTCPSDriver::AddAlarmAck(void)
{
	}

void CBBBSAPTCPSDriver::AddNak(void)
{
	AddAckNak(NAKDATA);
	}

void CBBBSAPTCPSDriver::SendAck(void)
{
	UINT uHSz = m_fExtend ? SZHDREXT : SZHDRSTD;

	m_pSend = new BYTE [uHSz];

	m_wSendPtr = 0;

	m_OutHeader.PacketCt = 0;

	m_wSendPtr = 0;

	AddSendWord(uHSz);
	AddSendWord(0);
	AddSendWord(m_wMsgFlag);
	AddSendLong(m_InHeader.PktAck);
	AddSendLong(m_InHeader.PktSeq);

	if( m_fExtend ) {

		AddSendLong(m_InHeader.ThisIP);
		AddSendLong(0);
		}

	SendFrame();

	delete m_pSend;
	}

// Frame Building

void CBBBSAPTCPSDriver::StartFrame(void)
{
	m_wTPtr = 0;
	}

void CBBBSAPTCPSDriver::EndFrame(void)
{
	}

void CBBBSAPTCPSDriver::AddUDPHeader(void)
{
	m_wSendPtr = 0;
	AddSendWord(m_fExtend ? SZHDREXT : SZHDRSTD);
	AddSendWord(m_OutHeader.PacketCt);
	AddSendWord(m_wMsgFlag);

	m_OutHeader.PktSeq = NextDWSeq(m_InHeader.PktAck);

	AddSendLong(m_OutHeader.PktSeq);

	AddSendLong(m_InHeader.PktSeq);

	if( m_fExtend ) {

		AddSendLong(m_InHeader.ThisIP);
		AddSendLong(0);
		}
	}

void CBBBSAPTCPSDriver::AddUDPPacketHeader(OUTPKTH * pPktHdr)
{
	AddSendWord(pPktHdr->PktDataLen);

	AddSendWord(pPktHdr->BSAP);

	AddSendByte(pPktHdr->MEC);

	AddSendWord(pPktHdr->MsgSeqNum);

	AddSendWord(pPktHdr->GlobAddr);
	}

void CBBBSAPTCPSDriver::AddDeviceName(void)
{
	PBYTE p = m_pbDevName;

	for( WORD i = 0; i < strlen((const char *)p); i++ ) AddSendByte(p[i]);

	AddSendByte(0);
	}

void CBBBSAPTCPSDriver::AddByte(BYTE bData)
{
	m_bTx[m_wTPtr++] = bData;
	}

void CBBBSAPTCPSDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CBBBSAPTCPSDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CBBBSAPTCPSDriver::AddText(PCTXT pText)
{
	for( WORD i = 0; i < strlen(pText); i++ ) {

		AddByte(pText[i]);
		}
	}

void CBBBSAPTCPSDriver::AddTextPlusNull(PCTXT pText)
{
	AddText(pText);
	AddByte(0);
	}

void CBBBSAPTCPSDriver::AddSendByte(BYTE bData)
{
	m_pSend[m_wSendPtr++] = bData;
	}

void CBBBSAPTCPSDriver::AddSendWord(WORD wData)
{
	AddSendByte(LOBYTE(wData));

	AddSendByte(HIBYTE(wData));
	}

void CBBBSAPTCPSDriver::AddSendLong(DWORD dwData)
{
	AddSendWord(LOWORD(dwData));

	AddSendWord(HIWORD(dwData));
	}

// Helpers

BOOL CBBBSAPTCPSDriver::HasTags(PDWORD pData, WORD wCount)
{
	if( m_fHasTags ) return TRUE;

	for( WORD i = 0; i < wCount; i++ ) *pData = 0xFFFFFFFF;

	return FALSE;
	}

DWORD CBBBSAPTCPSDriver::NextDWSeq(DWORD dSeqNum)
{
	return max(dSeqNum + 1L, 1L);
	}

WORD CBBBSAPTCPSDriver::NextWSeq(WORD wSeqNum)
{
	return max(wSeqNum + 1L, 1L);
	}

void CBBBSAPTCPSDriver::InitGlobal(void)
{
	m_pbDevName	= NULL;
	m_pdAddrList	= NULL;
	m_pbNameList	= NULL;
	m_pwNameIndex	= NULL;

	m_pListAddr	= NULL;
	m_pListData	= NULL;
	m_pListPosn	= NULL;

	m_wTagCount	= 0;
	m_fHasTags	= FALSE;

	m_wNameListItem	= 0;

	m_wMsgFlag	= 0x6;

	m_fExtend	= FALSE;

	m_wState	= 0;

	m_pVarInfo	= &SVarInfo;

	m_pInHeader	= &m_InHeader;

	m_pOutHeader	= &m_OutHeader;

	m_LBMagic	= 0;

	for( UINT i = 0; i < 4; i++ ) {

		m_pInPktHead[i]	= &m_InPktHead[i];
		m_pInPktData[i]	= &m_InPktData[i];

		m_pOutPktHead[i] = &m_OutPktHead[i];
		m_pOutPktData[i] = &m_OutPktData[i];
		}
	}

BOOL CBBBSAPTCPSDriver::IsPollOrAck(BYTE bData)
{
	if( bData >= POLLMSG && bData <= UPACK ) {

		return TRUE;
		}

	return bData == ALARMACK || bData == ALARMNOT;
	}

void CBBBSAPTCPSDriver::PutTxWord(WORD wPos, WORD wData)
{
	m_bTx[wPos]	= LOBYTE(wData);
	m_bTx[wPos + 1]	= HIBYTE(wData);
	}

void CBBBSAPTCPSDriver::PutTxDWord(WORD wPos, DWORD dData)
{
	PutTxWord(wPos,   LOWORD(dData));
	PutTxWord(wPos+2, HIWORD(dData));
	}

BYTE CBBBSAPTCPSDriver::GetRxByte(PBYTE pData, PWORD pPos)
{
	WORD p	= *pPos;

	BYTE b	= pData[p];

	*pPos	= p + 1;

	return b;
	}

WORD CBBBSAPTCPSDriver::GetRxWord(PBYTE pData, PWORD pPos)
{
	WORD p	= *pPos;

	WORD w	= (WORD)IntelToHost(*((PU2)(&pData[p])));

	*pPos	= p + 2;

	return w;
	}

DWORD CBBBSAPTCPSDriver::GetRxDWord(PBYTE pData, PWORD pPos)
{
	WORD p	= *pPos;

	DWORD d	= (DWORD)IntelToHost(*((PU4)(&pData[p])));

	*pPos   = p + 4;

	return d;
	}

BOOL CBBBSAPTCPSDriver::CheckHeaderSize(UINT uSize)
{
	return uSize == SZHDRSTD || uSize == SZHDREXT;
	}

BOOL CBBBSAPTCPSDriver::CheckTxSize(WORD wStart, PWORD pSize)
{
	if( m_wTPtr > MAXSEND ) {

		m_wTPtr = wStart;

		*pSize  = MAXSEND;
		}

	return m_wTPtr > wStart;
	}

// Tag Data Loading
void CBBBSAPTCPSDriver::LoadTags(LPCBYTE &pData)
{
	if( StoreDevName(pData) ) {

		UINT uCount = GetWord(pData);

		if( uCount ) {

			m_wTagCount  = (WORD)uCount;

			m_fHasTags   = TRUE;

			m_pdAddrList = new DWORD [uCount + 1];

			for( WORD i = 0; i < uCount; i++ ) {

				m_pdAddrList[i] = GetLong(pData);
				}

			m_pdAddrList[uCount] = 0; // end marker

			UINT uTotalSize	     = GetWord(pData) + uCount; // adding nulls

			m_pwNameIndex = new WORD [uCount + 1];
			m_pbNameList  = new BYTE [uTotalSize];

			WORD wNamePosition = 0;

			for( i = 0; i < uCount; i++ ) {

				StoreName(pData, i, &wNamePosition);
				}

			m_pwNameIndex[uCount] = uTotalSize; // end marker
			}
		}
	}

BOOL CBBBSAPTCPSDriver::StoreDevName(LPCBYTE &pData)
{
	UINT uCount = GetWord(pData); // size of device name

	if( uCount ) {

		BOOL fWide  = m_bIsC3 == 3;

		m_pbDevName = new BYTE[uCount];

		memset(m_pbDevName, 0, uCount);

		for( UINT i = 0; i < uCount; i++ ) {

			m_pbDevName[i] = fWide ? LOBYTE(GetWord(pData)) : GetByte(pData);
			}

		return TRUE;
		}

	return FALSE;
	}

void CBBBSAPTCPSDriver::StoreName(LPCBYTE &pData, WORD wItem, PWORD pPosition)
{
	UINT uByteCount	= GetWord(pData);

	WORD wPos	= *pPosition;

	*pPosition	+= uByteCount; // next start position

	m_pwNameIndex[wItem] = wPos; // position of start of name

	BOOL fWide  = m_bIsC3 == 3;

	for( WORD i = 0; i < uByteCount; i++ ) {

		m_pbNameList[wPos+i] = fWide ? LOBYTE(GetWord(pData)) : GetByte(pData); // store name char
		}
	}

// Socket Management

void CBBBSAPTCPSDriver::OpenSocket(UINT n)
{
	m_pSock->m_pSocket = CreateSocket(IP_UDP);

	if( m_pSock->m_pSocket ) {

		m_pSock->m_pSocket->Listen(m_uPort);

		m_pSock->m_pData = new BYTE [ 300 ];

		m_pSock->m_wPtr  = 0;

		m_pSock->m_fBusy = FALSE;
		}

	}

void CBBBSAPTCPSDriver::CloseSocket(void)
{
	m_pSock->m_pSocket->Release();

	if( m_pSock->m_pData ) {

		delete m_pSock->m_pData;

		m_pSock->m_pData = NULL;
		}

	m_pSock->m_pSocket = NULL;
	}

// Delete created buffers
void CBBBSAPTCPSDriver::CloseListBuffers(void)
{
	if( m_LBMagic == 0x12345678 ) {

		if( m_pListAddr ) delete m_pListAddr;
		if( m_pListData ) delete m_pListData;
		if( m_pListPosn ) delete m_pListPosn;
		}

	m_LBMagic   = 0;

	m_pListAddr = NULL;
	m_pListData = NULL;
	m_pListPosn = NULL;
	}

void CBBBSAPTCPSDriver::CloseDown(void)
{
	if( m_pbDevName ) {

		delete m_pbDevName;

		m_pbDevName = NULL;
		}

	if( m_pbNameList ) {

		delete m_pbNameList;

		m_pbNameList = NULL;
		}

	if( m_pwNameIndex ) {

		delete m_pwNameIndex;

		m_pwNameIndex = NULL;
		}

	if( m_pdAddrList ) {

		delete m_pdAddrList;

		m_pdAddrList = NULL;
		}

	CloseListBuffers();
	}

// Conversions

DWORD CBBBSAPTCPSDriver::LongToReal(UINT uType, DWORD Data)
{
	switch(uType) {

		case addrByteAsByte:	Data &= 0xFF;	break;

		case addrWordAsWord:	Data &= 0xFFFF;	break;
		}

	float Real = float(LONG(Data));

	switch( uType ) {

		case addrBitAsBit:	return (DWORD)((BOOL)Data);
		case addrByteAsByte:	return  R2I(Real);
		case addrWordAsWord:	return  R2I(Real);
		case addrLongAsLong:	return  R2I(Real);
		}

	return Data;
	}

DWORD CBBBSAPTCPSDriver::RealToLong(UINT uType, DWORD Data)
{
	float Real = I2R(Data);

	switch( uType ) {

		case addrBitAsBit:	return (DWORD)((BOOL)Data);
		case addrByteAsByte:	return (DWORD)LOBYTE(Real);
		case addrWordAsWord:	return (DWORD)LOWORD(Real);
		case addrLongAsLong:	return (DWORD)Real;
		}

	return Data;
	}

// End of File
