//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock UDP Slave Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

class CBBBSAPTCPSDriver : public CSlaveDriver
{
	public:
		// Constructor
		CBBBSAPTCPSDriver(void);

		// Destructor
		~CBBBSAPTCPSDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Detach(void);

		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(void)	Service (void);

	protected:
		// Socket Context
		struct	SOCKET
		{
			ISocket	*m_pSocket;
			WORD	m_wPtr;
			PBYTE	m_pData;
			BOOL	m_fBusy;
			UINT	m_uTime;
			};

		// BSAP UDP structures
		struct	MAINHDR	// Message Headers
		{
			WORD	HeadSize;
			WORD	PacketCt;
			WORD	MsgFlags;
			DWORD	PktSeq;
			DWORD	PktAck;
			DWORD	ThisIP;
			DWORD	Spare;
			};

			// Packet Headers
		struct	INPKTH
		{
			WORD	PktDataLen;
			WORD	BSAP;
			BYTE	MEC;
			WORD	MsgSeqNum;
			WORD	GlobAddr;
			BYTE	FuncCode;
			BYTE	NRT;      // last byte in actual header
			};

		struct	OUTPKTH
		{
 			WORD	PktDataLen;
			WORD	BSAP;
			BYTE	MEC;
			WORD	MsgSeqNum;
			WORD	GlobAddr;
			};

		// Packet Data Control
		struct	INPKTD
		{
			BYTE	Position;	// start position in receive buffer
			BYTE	PktNum;		// this packet's number
			BYTE	Operand;	// Function to execute
			BYTE	FSS[5];		// Function Select Bytes
			};

		struct	OUTPKTD
		{
			BYTE	DataType;
			BYTE	BitValue;
			DWORD	AnalogValue;
			BYTE	PollValue;
			};

		struct	VARINFO
		{
			DWORD	Addr;
			DWORD	Data;
			BOOL	Logical;
			UINT	NameInx;
			WORD	MSDAddr;
			WORD	MSDVers;	// MSD Version for using MSD
			};

		// Data Members
		PBYTE		m_pbDevName;
		PDWORD		m_pdAddrList;
		PBYTE		m_pbNameList;
		PWORD		m_pwNameIndex;

		BYTE		m_bIsC3;

		WORD		m_wTagCount;	// Tag information
		BOOL		m_fHasTags;	// Tag information

		BYTE bLastPhase; // debugging byte
		BOOL fIsListReq; // List Debugging

		// Variable info
		VARINFO	SVarInfo;
		VARINFO	*m_pVarInfo;
		WORD	m_wVarSel;

		WORD	m_wTPtr;
		BYTE	m_bTx [300];
		BYTE	m_bRx [300];
		WORD	m_wState;
		WORD	m_wEndPos;
		WORD	m_wSendPtr;

		WORD	m_wNameListItem;
		WORD	m_wListCount;
		BYTE	m_bList;

		// Allocated pointers
		PBYTE	m_pPktData[4];

		PBYTE	m_pSend;
		PDWORD	m_pListAddr;
		PDWORD	m_pListData;
		PWORD	m_pListPosn;

		DWORD	m_LBMagic;

		// Field Selects
		BYTE	m_bFSS[5];

		// Message Header
		MAINHDR	m_InHeader;
		MAINHDR	*m_pInHeader;
		MAINHDR	m_OutHeader;
		MAINHDR	*m_pOutHeader;
		WORD	m_wMsgFlag;
		BYTE	m_bRER;

		// Packet Information
		BYTE	m_bQuantity;
		INPKTH	m_InPktHead[4];
		INPKTH	*m_pInPktHead[4];
		INPKTH	*m_pInH;
		INPKTD	m_InPktData[4];
		INPKTD	*m_pInPktData[4];
		INPKTD	*m_pInD;
		OUTPKTH	m_OutPktHead[4];
		OUTPKTH	*m_pOutPktHead[4];
		OUTPKTH	*m_pOutH;
		OUTPKTD	m_OutPktData[4];
		OUTPKTD	*m_pOutPktData[4];
		OUTPKTD	*m_pOutD;
		BOOL	m_fExtend;
		BYTE	m_bEER;
		BYTE	m_bQty;

		// UDP Data Members
		UINT	m_uPort;
		UINT	m_uCount;
		SOCKET	*m_pSockList;
		SOCKET	*m_pSock;

		// Receive Processing
		void	ReadData(void);
		WORD	RcvDone(WORD wSize);
		BOOL	DoPackets(void);
		void	GetMainHeader(void);
		UINT	GetPktHeader(PBYTE pData, WORD wPktItem);

		// Read Routines
		BOOL	ReadByList(BOOL fFirst, UINT uPktNum, PWORD pSize);
		BOOL	ReadByName(UINT uPktNum, PWORD pSize, BOOL fHasErr);
		BOOL	ReadByAddress(UINT uPktNum, PWORD pSize, BOOL fHasErr);

		// Read Helpers
		void	InitRBL(WORD wPktNum);
		void	CountListElements(PDWORD pA, PDWORD pD, PWORD pP, WORD wPktNum);
		void	GetVarInfo(UINT uIndex);
		BOOL	ParseVarName(PCTXT Name, UINT uSelect, PBYTE pItem);
		void	SetFieldSelects(UINT uPktNum);
		BOOL	AddFieldSels(void);
		void	AddField1(void);
		void	AddField2(void);
		void	AddField3(void);
		void	PutPacketData(BYTE bBad);

		// Item info
		BYTE	LoadInfoFromName(WORD wPos, PWORD pLen);
		BYTE	LoadInfoFromAddr(WORD wPos, BOOL fRead);
		BOOL	GetReadInfo(WORD wIndex);
		void	FillInfo(WORD wIndex, DWORD dData);

		// Write routines
		BOOL	WriteByName(UINT uPktNum, PWORD pSize);
		BOOL	WriteByAddress(UINT uPktNum, PWORD pSize);

		// Write Helpers
		BOOL	MakeWriteData(PWORD pPos);

		// Transport Layer
		void	Send();
		BOOL	SendFrame();

		// Ack
		void	AddAckNak(BYTE bType);
		void	AddPollAck(void);
		void	AddDownAck(void);
		void	AddAlarmAck(void);
		void	AddNak(void);
		void	SendAck(void);

		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddUDPHeader(void);
		void	AddUDPPacketHeader(OUTPKTH * pPktHdr);
		void	AddDeviceName(void);

		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddText(PCTXT pText);
		void	AddTextPlusNull(PCTXT pText);
		void	AddSendByte(BYTE bData);
		void	AddSendWord(WORD wData);
		void	AddSendLong(DWORD dwData);

		// Helpers
		BOOL	HasTags(PDWORD pData, WORD wCount);
		DWORD	NextDWSeq(DWORD dSeqNum);
		WORD	NextWSeq (WORD  wSeqNum);
		void	InitGlobal(void);
		BOOL	IsPollOrAck(BYTE bData);
		void	PutTxWord( WORD wPos, WORD  wData);
		void	PutTxDWord(WORD wPos, DWORD dData);
		BYTE	GetRxByte( PBYTE pData, PWORD pPos);
		WORD	GetRxWord( PBYTE pData, PWORD pPos);
		DWORD	GetRxDWord(PBYTE pData, PWORD pPos);
		BOOL	CheckHeaderSize(UINT uSize);
		BOOL	CheckTxSize(WORD wStart, PWORD pSize);

		// Tag Data Loading
		void	LoadTags(LPCBYTE &pData);
		BOOL	StoreDevName(LPCBYTE &pData);
		void	StoreName(LPCBYTE &pData, WORD wItem, PWORD pPosition);

		// Socket Management
		void	OpenSocket(UINT n);
		void	CloseSocket(void);

		// delete created buffers
		void	CloseListBuffers(void);
		void	CloseDown(void);

		// Conversions
		DWORD LongToReal(UINT uType, DWORD Data);
		DWORD RealToLong(UINT uType, DWORD Data);
	};

enum { // Message function codes
	MFCPEIPC  =  0x3, // for HMI
	MFCPEIRTU = 0x04, // to RTU
	MFCRDDB   = 0x0B, // Read Database
	MFCWRDB   = 0x0C, // Write Database
	MFCRDBYN  = 0x0D, // Read By Name in example file from BB
	MFCCOMREQ = 0x70, // Command Handler Requests
	MFCRDBACC = 0xA0, // remote database access
	};

enum { // Operational function codes
	OFCRDBYA  =  0x0, // Read Signal by Address
	OFCRDBYN  =  0x4, // Read Signal By Name
	OFCRDBYL  = 0x0C, // First call to read Signal(s) by list
	OFCRDBYLC = 0x0D, // Continue to read Signal(s) by list
	OFCWRBYA  = 0x80, // Write Signal by Address
	OFCWRBYN  = 0x84, // Write By Name
	};

enum { // Field Select 1 codes
	FS1TYPINH = 0x80,
	FS1VALUE  = 0x40,
	FS1UNITS  = 0x20,
	FS1MSDADD = 0x10,
	FS1NAME   = 0x08,
	FS1ALARM  = 0x04,
	FS1DESC   = 0x02,
	FS1ONOFF  = 0x01,
	};

enum { // Polls and Acks
	RTURESP  = 0x40, // Data Response
	POLLMSG  = 0x85, // Poll message
	ACKDOWN  = 0x86, // Down Transmit ACK, Slave ACKing message
	ACKPOLL  = 0x87, // ACK No Data, in response to a Poll message
	NRTSEND  = 0x88, // Send Node routing Table
	NRTREQ   = 0x89, // Request for NRT message
	POLLUPSL = 0x8A, // UP-ACK with poll (recognized by VSAT Slave)
	UPACK    = 0x8B, // UP-ACK, Master ACKing message
	NAKDATA  = 0x95, // NAK, No buffer available for received data
	ALARMACK = 0xA8, // Alarm Acknowledge
	ALARMNOT = 0xAA, // Alarm notification
	};

enum { // Transaction States

	STPOLL    = 1,
	STPWTDATA = 2,
	};

enum { // response return values

	PRNORSP		= 0,
	PRWRONG		= 1,
	PRACK		= 2,
	PRNAK		= 3,
	PRDATA		= 4,
	};

enum { // Write Field Descriptors
	WFDALDIS = 0x1,
	WFDALEN  = 0x2,
	WFDCNDIS = 0x3,
	WFDCNEN  = 0x4,
	WFDMNDIS = 0x5,
	WFDMNEN  = 0x6,
	WFDQUSET = 0x7,
	WFDQUCLR = 0x8,
	WFDLVON  = 0x9,
	WFDLVOFF = 0xA,
	WFDANALG = 0xB,
	WFDSTRNG = 0xD,
	WFDRDSEC = 0xE,
	WFDWRSEC = 0xF,
	};

enum { // RER Codes for standard requests
	RERMORE  = 0x81,	// Multiple errors in response
	RERMATCH = 0x84,	// no match on name, or too big
	RERAVPKD = 0x88,	// Analog value in packed logical
	RERNOMAT = 0x90,	// no match found
	RERVERNM = 0xA0,	// Version number mismatch
	RERINVDB = 0xC0,	// invalid data base function requested
	RERSET   = 0x80		// Error exists
	};

enum { // RER Codes for array request
	RERBOUND = 0x82,	// Array bounds exceeded
	RERARRCF = 0x84,	// Invalid array reference
	RERROARR = 0x88,	// write to a read-only array
//	RERINVDB = 0xC0		// invalid data base function request
	};

enum { // EER Codes
	EERRONLY = 0x02,	// attempt to write to read only
	EERIDENT = 0x04,	// Invalid name, list, MSDAddr
	EERSECUR = 0x05,	// Security violation
	EERINHIB = 0x06,	// Write a manual inhibited signal
	EERINVWR = 0x07,	// Invalid write attempt (eg analog=ON)
	EEREND_D = 0x08,	// Premature End_of_Data encountered
	EERINCOM = 0x09,	// Incomplete Remote DB request
	EERMATCH = 0x10		// No Match for Signal Name
	};

// UDP Receive positions SH=Standard Header, EH = Extended Header
enum {  // Common Header
	UDPHSZ	= 0,	// Message Header Size
	UDPPCT	= 2,	// Number of Packets
	UDPFLG	= 4,	// Flags
	UDPSEQ	= 6,	// Send Sequence Number
	UDPLAK	= 10,	// Last Ack'ed Seq Number
	};

enum {  // Extra for extended header
	UDPRIP	= 14,	// Sender IP
	UDP000	= 18,	// Filler
	};

enum {  // Standard Header Subpackets
	UDPSSZ	= 14,	// Subpacket size
	UDPSTY	= 16,	// Type (BSAP = 0)
	UDPSMX	= 18,	// Exchange Code
	UDPSMS	= 19,	// Message Seq #
	UDPSGA	= 21,	// Global Address of Sender
	UDPSER	= 23,	// Error if not 0
	UDPSCT	= 24,	// Number of Items (= 1 for good reply)
	UDPSDT	= 25,	// Data start
	};

enum {  // Extended Header Subpackets
	UDPESZ	= 22,	// Subpacket size
	UDPETY	= 24,	// Type (BSAP = 0)
	UDPEMX	= 26,	// Exchange Code
	UDPEMS	= 27,	// Message Seq #
	UDPEGA	= 29,	// Global Address of Sender
	UDPEER	= 31,	// Error if not 0
	UDPECT	= 32,	// Number of Items (= 1 for good reply)
	UDPEDT	= 33,	// Data start
	};

// Receive Frame Check codes
enum {
	FRCONT	= 0,
	FRDONE	= 1,
	FRERR	= 2
	};

//////////////////////////////////////////////////////////////////////////
//
// Useful Macros
//

#define	I2R(x)	(*((float *) &(x)))

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	SZHDREXT	((WORD)22)
#define	SZHDRSTD	((WORD)14)
#define	POLLPOSN	23

#define	FLAGNEW		6
#define	FLAGOLD		4
#define	FLAGCONV	2

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	SZVARI	(sizeof(VARINFO))

#define	MAXSEND	220

/*
FUN Function code
bit 7 function bit
0 read function
1 write function
bits 6-4 = data type code
000 signal data
001 data array data
010 direct I/O data
100 memory data
111 Extended RDB function
bits 3-2 = select code
00 select via address or data array number, control
01 select via name or data array number, short form
10 select via match or data array number, general form
11 select via list
bits 1-0 = signal READ return code, WRITE Memory code
For READ:
00 return first match
01 return next match/continue list
10 return packed logical data (logical value data only)
For WRITE Memory,
00 Write Data to memory
01 OR Data with memory
10 AND Data with memory

NOTE:FIELD SELECTOR BYTES ARE ONLY USED IN ACCESSING SIGNAL
DATA WHEN PACKED LOGICAL DATA IS NOT USED.
FSS Field select selector defines which field select bytes are included in the request:
BITS 7-4 not used at this time (reserved for priority)
BIT 3 Field select byte 4 is used
BIT 2 Field select byte 3 is used
BIT 1 Field select byte 2 is used
BIT 0 Field select byte 1 is used

FS1 Field selector byte one (most common options):
BIT 7 Type and Inhibits byte
BIT 6 Value
BIT 5 Units/Logical text
BIT 4 Msd address
BIT 3 Name Text (includes Base.Extension.Attribute)
BIT 2 Alarm status
BIT 1 Descriptor text
BIT 0 Logical ON/OFF text

FS2 Field selector byte two:
BIT 7 Protection byte
BIT 6 Alarm priority
BIT 5 Base Name
BIT 4 Extension
BIT 3 Attribute
BIT 2 Low alarm deadband MSD address
BIT 1 High alarm deadband MSD address
BIT 0 MSD Version number

FS3 Field selector byte three:
BIT 7 Low alarm limit MSD address
BIT 6 High alarm limit MSD address
BIT 5 Extreme low alarm limit MSD address
BIT 4 Extreme high alarm limit MSD address
BIT 3 Reserved for Report by exception
BIT 2 Reserved (old Lock bit)
BIT 1 Signal value; Analogs return no Q-bit in FP mantissa.
BIT 0 When set, allows long signal names to be returned, instead of standard
length names. (Requires ControlWave firmware version 2.2 or newer)

FS4 Field selector byte four:
This byte is reserved for future use.
*/

// End of File
