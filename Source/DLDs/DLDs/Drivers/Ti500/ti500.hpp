
#define	FRAME_TIMEOUT	500

//////////////////////////////////////////////////////////////////////////
//
// Siemens TI-500 Master Serial Driver
//

class CTi500Driver : public CMasterDriver
{
	public:
		// Constructor
		CTi500Driver(void);

		// Destructor
		~CTi500Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		
	protected:

		UINT   m_uPtr;
		BYTE   m_bTxBuff[256];
		BYTE   m_bRxBuff[256];
		WORD   m_wCheck;
		LPCTXT m_pHex;
		
		void Clear(BYTE bOpcode);
		void AddByte(BYTE bValue);
		void AddWord(WORD wValue);
		void AddLong(DWORD dwValue);
		void GetBits(PDWORD pData, UINT uCount);
		void GetWords(PDWORD pData, UINT uCount);
		void GetLongs(PDWORD pData, UINT uCount);
		void Send(void);
		BOOL Receive(void);
		WORD xval(BYTE bByte);
		WORD Swap(WORD wData);
		BOOL Verify(void);
		UINT GetWordCode(UINT uTable);
		UINT GetPageSize(UINT uTable);
		UINT GetFirstLocation(UINT uTable);
		UINT GetOffset(UINT uTable, UINT uOffset);
		UINT GetSpecial(UINT uTable);

		
		// Helpers
		BOOL IsLong(UINT uType);

		
	};

// End of File
