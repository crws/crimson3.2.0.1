
#include "intern.hpp"

#include "ti500.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens TI-500 Master Serial Driver
//
//

// Instantiator

INSTANTIATE(CTi500Driver);

// Constructor

CTi500Driver::CTi500Driver(void)
{
	m_Ident  = DRIVER_ID;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex; 

	}

// Destructor

CTi500Driver::~CTi500Driver(void)
{
	}

// Entry Points

CCODE MCALL CTi500Driver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 1;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrWordAsWord;

	return Read(Addr, Data, 1);
	}

// Configuration

void MCALL CTi500Driver::Load(LPCBYTE pData)
{
	}
	
void MCALL CTi500Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CTi500Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CTi500Driver::Open(void)
{	
	}

// Device

CCODE MCALL CTi500Driver::DeviceOpen(IDevice *pDevice)
{
	return CMasterDriver::DeviceOpen(pDevice); 
	}

CCODE MCALL CTi500Driver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CTi500Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{		
	BOOL fBit = Addr.a.m_Type == addrBitAsBit;
	
	Clear(fBit ? 0x12 : 0x01);

	UINT uTable = Addr.a.m_Table;

	BOOL fLong = IsLong(Addr.a.m_Type);

	if( fLong ) {

		MakeMin(uCount, 3);
		}
	else {
		MakeMin(uCount, 7);
		}

	UINT Count = fLong ? uCount * 2 : uCount;

	UINT uCode = GetWordCode(uTable);

	UINT uOffset = GetOffset(uTable, Addr.a.m_Offset);

	UINT uSize = GetPageSize(uTable);

	UINT uSpecial = GetSpecial(uTable);

	BYTE bShift = fBit ? 0xE : 0xF;

	for( UINT u = 0; u < Count; u++ ) {

		if( Addr.a.m_Offset > uSize ) {

			AddWord((bShift << 11) + ((uOffset / uSize) & 0x3FF));

			AddWord(uCode + (uOffset % uSize) + uSpecial);
			}
		else {
			AddWord(uCode + (uOffset & 0x7FF) + uSpecial);
			}

		uOffset++;
		}

	Send();

	if( !Receive() ) {

		return CCODE_ERROR;
		}
	
	if( fBit ) {

		GetBits(pData, uCount);

		return uCount;
		}

	fLong ? GetLongs(pData, uCount) : GetWords(pData, uCount);

	return uCount; 
	}

CCODE MCALL CTi500Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	BOOL fBit = Addr.a.m_Type == addrBitAsBit;
	
	Clear(fBit ? 0x14 : 0x02);

	UINT uTable = Addr.a.m_Table;

	UINT uAddr = Addr.a.m_Offset - 1;

	BOOL fLong = IsLong(Addr.a.m_Type);

	MakeMin(uCount, UINT(fLong || fBit ? 1 : 3));

	UINT Count = fLong ? uCount * 2 : uCount;

	UINT uCode = GetWordCode(uTable);

	UINT uOffset = GetOffset(uTable, Addr.a.m_Offset);

	UINT uSize = GetPageSize(uTable);

	UINT uSpecial = GetSpecial(uTable);

	BYTE bShift = fBit ? 0xE : 0xF;
	
	for( UINT u = 0; u < Count; u++ ) {

		if( Addr.a.m_Offset > uSize ) {

			AddWord((bShift << 11) + ((uOffset / uSize) & 0x3FF));

			AddWord(uCode + (uOffset % uSize) + uSpecial);
			}
		else
			AddWord(uCode + (uOffset & 0x7FF) + uSpecial);
		
		if( fLong ) {

			if( u == 0 ) {

				AddWord(HIWORD(pData[u]));
				}
			else
				AddWord(LOWORD(pData[u - 1]));
			}

		else if( fBit ) {

			WORD wWrite = (pData[u] ? 0x3 : 0x2);

			AddByte(LOBYTE(wWrite));

			m_wCheck += Swap(wWrite);
			}
		else
			AddWord(pData[u]);
			
		uOffset++;
		}
	
	Send();

	if( !Receive() ) {

		return CCODE_ERROR;
		}
	
	return uCount;
	}

// Implementation

void CTi500Driver::Clear(BYTE bOpcode)
{
	m_uPtr = 0;

	m_wCheck = 0;

	m_bTxBuff[m_uPtr++] = ':';

	AddWord(bOpcode);
	}

void CTi500Driver::AddByte(BYTE bValue)
{
	m_bTxBuff[m_uPtr++] = m_pHex[bValue / 16];
	m_bTxBuff[m_uPtr++] = m_pHex[bValue % 16];
	}
	
void CTi500Driver::AddWord(WORD wValue)
{
	AddByte(wValue / 256);

	AddByte(wValue % 256);

	m_wCheck += wValue;
	}

void CTi500Driver::AddLong(DWORD dwValue)
{
	AddWord(HIWORD(dwValue));

	AddWord(LOWORD(dwValue));
	}

void CTi500Driver::GetBits(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		pData[n] = (m_bRxBuff[n + 2] & 0x1);
		}
	}

void CTi500Driver::GetWords(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		WORD x = PU2(m_bRxBuff + 2)[n];
			
		pData[n] = LONG(SHORT(MotorToHost(x)));
		}
	}

void CTi500Driver::GetLongs(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		DWORD x = PU4(m_bRxBuff + 2)[n];
			
		pData[n] = MotorToHost(x);
		}
	}

 
void CTi500Driver::Send(void)
{
	m_pData->ClearRx();

	WORD Len = m_uPtr + 5;

	m_bTxBuff[1] = m_pHex[Len / 16];

	m_bTxBuff[2] = m_pHex[Len % 16];

	m_wCheck += Swap(Len);

	AddWord(-m_wCheck);

	m_bTxBuff[m_uPtr++] = ';';
	
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER); 
	}

BOOL CTi500Driver::Receive(void)
{
	UINT uState = 0;
	
	UINT uDigit = 0;

	UINT uTimer = 0;

	UINT uValue  = 0;

	UINT uByte = 0;

	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == ':' ) {
					m_uPtr = 0;
					uState = 1;
					}
				break;
				
			case 1:
				if( uByte == ';' )
					uState = 3;
				else {
					if( (uDigit = xval(uByte)) == NOTHING ) {

						return FALSE;
						}
					
					uValue = (uDigit << 4);
					uState = 2;
					}
				break;
				
			case 2:
				if( (uDigit = xval(uByte)) == NOTHING ) {

					return FALSE;
					}
					
				m_bRxBuff[m_uPtr++] = (uValue | uDigit);

				uState = 1;
				break;

			case 3:
				if( uByte == 0x0D )
					uState = 4;
				else
					return FALSE;
				break;
			
			case 4:
				if( uByte == 0x0A ) { 

					if( Verify() ) {

						return TRUE;
						}

					return FALSE;
					}
				
				return FALSE;
			}
		}

	return FALSE;
	}

WORD CTi500Driver::xval(BYTE bByte)
{
	if( bByte >= '0' && bByte <= '9' )
		return bByte - '0';
	
	if( bByte >= 'A' && bByte <= 'F' )
		return bByte - 'A' + 10;
	
	return WORD(NOTHING);
	}

WORD CTi500Driver::Swap(WORD wData)
{
	return ((wData & 0x00FF) << 8) | ((wData & 0xFF00) >> 8);
	}

BOOL CTi500Driver::Verify(void)
{
	UINT uCount = m_uPtr * 2 + 2;
	
	if( m_bRxBuff[0] != uCount)
		return FALSE;

	WORD wTarget = *(PU2(&m_bRxBuff[m_uPtr -= 2]));

	m_bRxBuff[m_uPtr] = 0;

	WORD wCheck = 0;

	for( UINT u = 0; u < m_uPtr; u += sizeof(WORD) ) {

		WORD x = PU2(m_bRxBuff + u)[0];
			
		wCheck += (SHORT(MotorToHost(x)));
		}

	wCheck = -wCheck & 0xFFFF;

	if( wTarget != MotorToHost(wCheck) ) {

		return FALSE;
		}
		
	return TRUE;
	} 

UINT CTi500Driver::GetWordCode(UINT uTable)
{
	if( uTable == 8 ) {

		uTable++;
		}

	if( uTable > 0x20 ) {

		return (((uTable - 1) & 0xF) << 12);
		}

	return (uTable - 1) << 11;
	}

UINT CTi500Driver::GetPageSize(UINT uTable)
{
	switch( uTable ) {

		case 3:		return 480;
		case 8:
		case 9:		return 128;

		case 0x21:	return 512;
		}
	
	return 1024;
	}

UINT CTi500Driver::GetFirstLocation(UINT uTable)
{
	switch( uTable ) {
		
		case 1:
		case 2:
		case 6:		return	  0;
		case 3:		return 0x10;
		
		}

	return 1;
	}

UINT CTi500Driver::GetOffset(UINT uTable, UINT uOffset) 
{
	UINT uFirst = GetFirstLocation(uTable);

	if( uFirst == 0 || uTable == 8 || uTable == 9 ) {

		uOffset -= 1;
		}

	else if ( uFirst == 0x10 ) {

		uOffset += 9;
		}  

	return uOffset;
	}

UINT CTi500Driver::GetSpecial(UINT uTable)
{
	switch( uTable ) {

		case 8:		return 129;
		case 9:		return 1;
		}

	return 0;
	}
 
// Helpers

BOOL CTi500Driver::IsLong(UINT uType)
{
	return (uType == addrWordAsLong || uType == addrWordAsReal);
	}

// End of File
