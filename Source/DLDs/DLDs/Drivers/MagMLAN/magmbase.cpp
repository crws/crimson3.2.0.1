
#include "intern.hpp"

#include "magmbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN Driver Base
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Constructor

CMagMLANBase::CMagMLANBase(void)
{
	m_pBase     = NULL;

	m_uWriteErr = 0;
	}

// Destructor

CMagMLANBase::~CMagMLANBase(void)
{
	}

// Entry Points

CCODE MCALL CMagMLANBase::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n*** READ T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	if( ReadNoTransmit(Addr, pData, uCount) ) return uCount;

	return DoRead(Addr, pData, Addr.a.m_Table != AN ? uCount : 1);
	}

CCODE MCALL CMagMLANBase::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\n\n*** WRITE T=%d O=%d\r\n", Addr.a.m_Table, Addr.a.m_Offset);

	if( WriteNoTransmit(Addr, *pData) ) return uCount;

	return DoWrite(Addr, pData, Addr.a.m_Table != AN ? uCount : 1);
	}

// Implementation

// Frame Building

void  CMagMLANBase::StartFrame(BYTE bOpcode)
{
	m_uPtr   = 0;

	m_bCheck = 0xFF;

	AddByte(GetDevDrop());

	AddByte(bOpcode);
	}

void  CMagMLANBase::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr]  = bData;

		m_bCheck      -= bData;

		m_uPtr++;
		}
	}

void  CMagMLANBase::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void  CMagMLANBase::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void  CMagMLANBase::EndFrame(void)
{
	AddByte(m_bCheck);
	}

// Read Handlers

CCODE CMagMLANBase::DoRead(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bOpcode = GetReadOpcode(Addr);

	StartFrame( bOpcode );

	switch(bOpcode) {

		case RPAR0:

			ExpandPar(Addr.a.m_Offset, Addr.a.m_Table == SPPAR0);
			AddWord(0);
			break;

		case RSTAR:
			AddByte(0);
			break;
		}

	SetReadResponseData(Addr);

	if( Transact(FALSE) ) {

		if( Addr.a.m_Table == AN ) {

			*pData = GetItemData(RspFrame[ITEMPOS], RspFrame[ITEMLEN]);

			return 1;
			}

		else {
			GetTableRead(Addr, pData, uCount);

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

BYTE  CMagMLANBase::GetReadOpcode(AREF Addr)
{
	switch( Addr.a.m_Table ) {

		case SPPAR0:
		case SPPAR1:	return RPAR0;
		case SPSET:	return RSET;
		case SPSTAT:	return RSTAT;
		case SPVER:	return RVER;
		case SPTOTS:	return RTOTS;
		case SPTOTR:	return RTOTR;
		case SPGBI:	return RGBI;
		case SPCWW:	return RCWW;
		case SPCWL:	return RCWL;
		case SPLST:	return RLST;
		case SPRATE:	return RRATE;
		case SPSTAR:	return RSTAR;
		case SPTAR:	return RTAR;
		case SPTYP:	return RTYP;
		case SPYST:	return RYST;
		}

	return LOBYTE(Addr.a.m_Offset % 100);
	}

void  CMagMLANBase::SetReadResponseData(AREF Addr)
{
	UINT *pS = &RspFrame[RSPSIZE];
	UINT *pP = &RspFrame[ITEMPOS];
	UINT *pL = &RspFrame[ITEMLEN];

	if( Addr.a.m_Table != AN ) {

		switch( Addr.a.m_Table ) {

			case SPPAR0:
			case SPPAR1:
			case SPTYP:
				*pS = 5;
				return;

			case SPSET:
				*pS = 49;
				return;

			case SPSTAT:
			case SPRATE:
				*pS = 7;
				return;

			case SPSTAR:
				*pS = 23;
				return;

			case SPTOTS:
			case SPTOTR:
				*pS = 59;
				return;

			case SPCWW:
			case SPCWL:
			case SPVER:
				*pS = 9;
				return;

			case SPGBI:
			case SPLST:
				*pS = 11;
				return;

			case SPTAR:
			case SPYST:
				*pS = 8;
				return;
			}

		*pS = sizeof(m_bRx);
		return;
		}

	switch( Addr.a.m_Offset ) {

		case RADD:
			*pS = 6;
			*pP = 3;
			*pL = 1;
			return;

		case RECM:
			*pS = 5;
			*pP = 3;
			*pL = 1;
			return;

		case RSSR:
			*pS = 7;
			*pP = 2;
			*pL = 4;
			return;

		case RWTU:
			*pS = 4;
			*pP = 2;
			*pL = 1;
			return;
		}
	}

void  CMagMLANBase::GetTableRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uO = Addr.a.m_Offset;

	switch( Addr.a.m_Table ) {

		case SPPAR0:
		case SPPAR1:	*pData = GetItemData( 2, 2 );		return;

		case SPSET:	GetReadSettings(uO, pData, uCount);	return;

		case SPSTAT:	GetReadStatus(  uO, pData, uCount);	return;

		case SPTOTR:
		case SPTOTS:	GetReadTotals(  uO, pData, uCount);	return;

		case SPVER:	GetReadVersion( uO, pData, uCount);	return;

		case SPGBI:	GetBatch( uO, pData, uCount);		return;

		case SPCWW:
		case SPCWL:	GetCycle( Addr, pData, uCount);		return;

		case SPLST:	GetLineS( uO, pData, uCount );		return;

		case SPRATE:	GetRate( uO, pData, uCount );		return;

		case SPSTAR:	GetStar( uO, pData, uCount );		return;

		case SPTYP:	GetType( uO, pData, uCount);		return;

		case SPTAR:
		case SPYST:	GetTARYield( uO, pData, uCount);	return;
		}
	}

void  CMagMLANBase::GetCycle(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uSize = Addr.a.m_Table == SPCWW ? 2 : 4;

	pData[0] = GetItemData( Addr.a.m_Offset == 1 ? 2 : 2 + uSize, uSize );

	if( uCount == 2 ) pData[1] = GetItemData( 2 + uSize, 4 );
	}

void  CMagMLANBase::GetBatch(UINT uItem, PDWORD pData, UINT uCount)
{
	for( UINT uScan = 0; uScan < uCount; uScan++, pData++, uItem++ ) {

		*pData = GetItemData( 2 * uItem, 2 );

		if( uItem == 2 ) m_pBase->m_wBWW = *pData;
		}
	}

void  CMagMLANBase::GetLineS(UINT uItem, PDWORD pData, UINT uCount)
{
	for( UINT uScan = 0; uScan < uCount; uScan++, pData++, uItem++ ) {

		*pData = GetItemData( 2 * uItem, uItem == 3 ? 4 : 2 );
		}
	}

void  CMagMLANBase::GetRate(UINT uItem, PDWORD pData, UINT uCount)
{
	for( UINT uScan = 0; uScan < uCount; uScan++, pData++, uItem++ ) {

		*pData = GetItemData( 2 * uItem, 2 );
		}
	}

void  CMagMLANBase::GetStar(UINT uItem, PDWORD pData, UINT uCount)
{
	for( UINT uScan = 0; uScan < uCount; uScan++, pData++, uItem++ ) {

		*pData = GetItemData( 1 + (2 * uItem), 1 );
		}
	}

void  CMagMLANBase::GetTARYield(UINT uItem, PDWORD pData, UINT uCount)
{
	for( UINT uScan = 0; uScan < uCount; uScan++, pData++, uItem++ ) {

		BOOL f1 = uItem == 1;

		*pData = GetItemData( f1 ? 2 : 3, f1 ? 1 : 4 );
		}
	}

void  CMagMLANBase::GetType(UINT uItem, PDWORD pData, UINT uCount)
{
	for( UINT uScan = 0; uScan < uCount; uScan++, pData++, uItem++ ) {

		*pData = GetItemData( uItem == 1 ? 2 : 3, 1 );
		}
	}

void  CMagMLANBase::GetReadSettings(UINT uItem, PDWORD pData, UINT uCount)
{
	for( UINT uScan = 0; uScan < uCount; uScan++, pData++, uItem++ ) {

		*pData = GetItemData( GetSettingPos(uItem), GetSettingLen(uItem) );

		m_pBase->m_dSet[uItem] = *pData;
		}
	}

UINT  CMagMLANBase::GetSettingPos(UINT uItem)
{
	switch( uItem ) { // find m_bRx start position, skipping "Types"

		case  0: return  0; // Data will be 0
		case 25: return 40; // Recipe #
		case 26: return 42; // Operator #
		case 27: return 44; // Work Order #
		}
	
	UINT uMult = (uItem/2) * 3;

	return uItem % 2 ? 4 + uMult : 2 + uMult;
	}

UINT  CMagMLANBase::GetSettingLen(UINT uItem)
{
	if( uItem < 25 ) return uItem % 2 ? 1 : 2;

	switch( uItem ) {

		case 25: return 2;
		case 26: return 2;
		case 27: return 4;
		}

	return 4;
	}

void  CMagMLANBase::GetReadStatus(UINT uItem, PDWORD pData, UINT uCount)
{
	for( UINT uScan = 0; uScan < uCount; uScan++, uItem++, pData++ ) {

		BOOL f = uItem == 1;

		*pData = GetItemData( f ? 2 : uItem + 2, f ? 2 : 1 );
		}
	}

void  CMagMLANBase::GetReadTotals(UINT uItem, PDWORD pData, UINT uCount)
{
	if( m_bRx[1] == 32 || m_bRx[1] == 34 ) { // No Totals response

		memset( pData, 0, uCount * sizeof(DWORD) );

		return;
		}

	for( UINT uScan = 0; uScan < uCount; uScan++, uItem++, pData++ ) {

		if( !uItem ) *pData = 0;

		else if( uItem == 1 || uItem == 2 ) *pData = GetItemData( uItem + 1, 1 );

		else if( uItem < 6 ) *pData = GetItemData( (2 * uItem) - 2, 2 ); // O3->Pos4,O5->Pos8

		else *pData = GetItemData( (4 * uItem) - 14, 4 ); // O6->Pos10,O7->Pos14,O8->Pos18...
		}
	}

void  CMagMLANBase::GetReadVersion(UINT uItem, PDWORD pData, UINT uCount)
{
	if( !uItem ) {

		*pData = GetItemData( 2, 4 );

		if( uCount == 1 ) return;

		pData++;
		}

	*pData = GetItemData( 6, 2 ) << 16;
	}

// Write Handlers

CCODE CMagMLANBase::DoWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bOpcode = GetWriteOpcode(Addr);

	StartFrame(bOpcode);

	if( Addr.a.m_Table == AN ) AddNamedData(Addr.a.m_Offset, *pData);

	else AddTableData(Addr, pData);

	memset( m_bRx, 0xFF, 2 );

	if( Transact(TRUE) ) {

		if( bOpcode == WSSS && !(*pData) ) m_pBase->m_bSS0 = m_bRx[2];

		m_uWriteErr = 0;

		return 1;
		}

	return HandleNAK();
	}

BYTE  CMagMLANBase::GetWriteOpcode(AREF Addr)
{
	switch( Addr.a.m_Table ) {

		case SPPAR0:
		case SPPAR1:	return WPAR0;
		case SPSET:	return WSET;
		case SPSKEY:	return WSKEY;
		case SPTAR:	return WTAR;
		case SPRATE:	return WRATE;
		case SPSTAR:	return WSTAR;
		case SPTAG:	return WTAG;
		}

	return GetNamedWOpcode(Addr.a.m_Offset);
	}

BYTE  CMagMLANBase::GetNamedWOpcode(UINT uOffset)
{
	switch( uOffset ) {

		case RSSR:	return WSSR;
		case RWTU:	return WWTU;
		}

	return uOffset % 100;
	}

void  CMagMLANBase::AddNamedData(UINT uOffset, DWORD dData)
{
	switch( uOffset ) {

		case WACR:
		case RWTU:
		case WSSS:
			AddByte(LOBYTE(dData));
			break;

		case WVOLT:
			AddWord(LOWORD(dData));
			break;

		case RSSR:
			AddLong(dData);
			break;

		case WCLT:
		case WCTI:
		case WALM:
			break;

		case WBW:
			AddWord(m_pBase->m_wBWW);
			AddByte(m_pBase->m_bBWF);
			break;

		case WMMS:
			AddWord(0);
			AddByte(dData ? 1 : 0);
			break;

		case WPCC:
		case WSRK:
			AddByte(dData ? 1 : 0);
			break;

		case WSSY:
			AddWord(0);
			AddWord(LOWORD(dData));
			break;
		}
	}

void  CMagMLANBase::AddTableData(AREF Addr, PDWORD pData)
{
	UINT i;
	UINT uO = Addr.a.m_Offset;

	switch( Addr.a.m_Table ) {

		case SPPAR0:
		case SPPAR1:
			ExpandPar(uO, Addr.a.m_Table == SPPAR0);
			AddWord(LOWORD(*pData));
			return;

		case SPSET:

			PDWORD p;

			p = m_pBase->m_dSet;

			for( i = 1; i < 25; i += 2 ) {

				AddByte(LOBYTE(*(p+i)));
				AddWord(LOWORD(*(p+i+1)));
				}

			AddWord(*(p+25));
			AddWord(*(p+26));
			AddLong(*(p+27));

			*p = 0;
			return;

		case SPSKEY:
			AddByte(LOBYTE(*pData));
			return;

		case SPTAR:
			AddLong(*pData);
			return;

		case SPRATE:
			AddWord(*pData);
			return;

		case SPSTAR:
			AddByte( uO == 1 ? 0x82 : 0x52 );
			AddWord(LOWORD(*pData));
			return;

		case SPTAG:
			switch( uO ) {

				case 1:
					AddByte( 'R' );
					AddByte( 'C' );
					break;

				case 2:
					AddByte( 'W' );
					AddByte( 'O' );
					break;

				case 3:
					AddByte( 'O' );
					AddByte( 'P' );
					break;
				}

			AddLong(*pData);
			return;
		}

	}

CCODE CMagMLANBase::HandleNAK(void)
{			   
	PDWORD d = &m_pBase->m_dNAK;

	BYTE   t = m_bTx[1];
	BYTE   b = LOBYTE(*d);

	if( t != b ) { // a new command failed

		PBYTE  r = m_bRx;
		UINT   u = 0;

		if( *r == m_bTx[0] && *PWORD(&r[1]) == 0x3015 ) u = t; // NAK

		else {
			if( *PWORD(r) == 0xFFFF ) { // No Response

				u    = 0xFF00 + t;
				*d <<= 8;
				}
			}

		if( u ) { // No Response or NAK

			*d <<= 8;
			*d  += u;

			return 1;
			}
		}

	return (++m_uWriteErr) % 3 ? CCODE_ERROR : 1;
	}

// Helpers

BOOL  CMagMLANBase::ReadNoTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uT = Addr.a.m_Table;
	UINT uO = Addr.a.m_Offset;

	UINT i;

	switch( uT ) {

		case AN: break;

		case SPTOTR:

			for( i = 0; i < uCount; i++, pData++ ) {

				*pData = m_pBase->m_dTOTR[uO + i];
				}

			return TRUE;

		case SPSET:

			if( !m_pBase->m_dSet[0] ) return FALSE;

			for( i = 0; i < uCount; i++, pData++ ) {

				*pData = m_pBase->m_dSet[uO + i];
				}

			return TRUE;

		case SPTAG:
			switch( uO ) {

				case 1:	*pData = m_pBase->m_dRC;	break;
				case 2:	*pData = m_pBase->m_dWO;	break;
				case 3:	*pData = m_pBase->m_dOP;	break;
				}

			return TRUE;

		default:
			*pData = 0;
			return Addr.a.m_Table == SPSKEY;
		}

	switch( uO ) {

		case WACR:
		case WCLT:
		case WCTI:
		case WBW:
		case WALM:
			*pData = 0;
			return TRUE;

		case WBWW:
			*pData = m_pBase->m_wBWW;
			return TRUE;

		case WBWF:
			*pData = m_pBase->m_bBWF;
			return TRUE;

		case WVOLT:
			*pData = m_pBase->m_wVolt;
			return TRUE;

		case WSS0:
			*pData = m_pBase->m_bSS0;
			return TRUE;

		case WMMS:
		case WPCC:
		case WSRK:
		case WSSY:
		case WSSS:
			*pData = RTNZERO;
			return TRUE;

		case OPNAK:
			*pData = m_pBase->m_dNAK;
			return TRUE;
		}

	return FALSE;
	}

BOOL  CMagMLANBase::WriteNoTransmit(AREF Addr, DWORD dData)
{
	UINT uO = Addr.a.m_Offset;

	switch( Addr.a.m_Table ) {

		case AN: break;

		case SPSTAT:
		case SPTOTS:
		case SPVER:
		case SPGBI:
		case SPCWW:
		case SPCWL:
		case SPLST:
		case SPTYP:
		case SPYST:
			return TRUE;

		case SPTOTR:
			if( !uO && dData == 1 ) {

				CAddress A;

				A.a.m_Table  = SPTOTR;
				A.a.m_Offset = 0;

				DWORD Data[18];

				if( DoRead( A, Data, 18 ) == 18 ) {

					memcpy(&m_pBase->m_dTOTR[0], Data, sizeof(DWORD) * 18);
					}

//				m_pBase->m_dTOTR[0] = 0;
				}

			return TRUE;

		case SPSET:
			m_pBase->m_dSet[uO] = dData;
			return m_pBase->m_dSet[0] != SENDSET;

		case SPTAG:
			switch( uO ) {

				case 1:	m_pBase->m_dRC = dData;	break;
				case 2:	m_pBase->m_dWO = dData;	break;
				case 3:	m_pBase->m_dOP = dData;	break;
				}

			return FALSE;

		case SPRATE:
		case SPTAR:
			return uO == 2;

		case SPSKEY:
			return dData < ' ' || dData > 'z';

		default:
			return FALSE;
		}

	switch( uO ) {

		case WACR:
			return !(dData == 1 || dData == 2);

		case WCLT:
		case WCTI:
		case WBW:
			return !dData;

		case WBWW:
			m_pBase->m_wBWW  = LOWORD(dData);
			return TRUE;

		case WBWF:
			m_pBase->m_bBWF  = LOBYTE(dData);
			return TRUE;

		case WVOLT:
			m_pBase->m_wVolt = LOWORD(dData);
			return FALSE;

		case WSSS:
			return dData > 2;

		case WSS0:
			m_pBase->m_bSS0 = 0;
			return TRUE;

		case RADD:
		case RECM:
			return TRUE;

		case OPNAK:
			m_pBase->m_dNAK = 0;
			return TRUE;
		}

	return FALSE;
	}

void  CMagMLANBase::ExpandPar(UINT uOffset, BOOL fIsPar0)
{
	BYTE b = LOW5(uOffset >> 10);

	AddByte( fIsPar0 ? b : 'A' + b );

	AddByte( LOW5(uOffset >>  5) + 'A' );

	b = LOW5(uOffset);

	if( b ) AddByte( b < 27 ? 'A' + b - 1 : '1' + b - 27 );
	}

DWORD CMagMLANBase::GetItemData(UINT uPos, UINT uLen)
{
	if( uPos ) {

		switch( uLen ) {
			
			case 1:	return DWORD(m_bRx[uPos]);
			case 2: return MotorToHost(*(PU2(&m_bRx[uPos])));
			case 3: return MotorToHost(*(PU4(&m_bRx[uPos])) >> 8);
			case 4: return MotorToHost(*(PU4(&m_bRx[uPos])));
			}
		}

	return 0L;
	}

// Drop Number
BYTE CMagMLANBase::GetDevDrop(void)
{
	return 0;
	}

// Transport Layer
BOOL CMagMLANBase::Transact(BOOL fIsWrite)
{
	return FALSE;
	}

// End of File
