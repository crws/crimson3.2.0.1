#include "magmbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN Driver
//

class CMagMLANDriver : public CMagMLANBase
{
	public:
		// Constructor
		CMagMLANDriver(void);

		// Destructor
		~CMagMLANDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry
		DEFMETH(CCODE) Ping(void);

	protected:
		// Device Data
		struct CContext : CMagMLANBase::CBaseCtx
		{
			BYTE m_bDrop;
			};

		// Data Members
		CContext *m_pCtx;

		// Drop Number
		BYTE	GetDevDrop(void);

		// Transport Layer
		void	Send(void);
		BOOL	Transact(BOOL fIsWrite);
		BOOL	GetReply(BOOL fIsWrite);

		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);
	};

// End of File
