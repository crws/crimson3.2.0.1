#include "intern.hpp"

#include "magmlan.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN Driver
//

// Instantiator

INSTANTIATE(CMagMLANDriver);

// Constructor

CMagMLANDriver::CMagMLANDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_pCtx      = NULL;
	}

// Destructor

CMagMLANDriver::~CMagMLANDriver(void)
{
	}

// Configuration

void  MCALL CMagMLANDriver::Load(LPCBYTE pData)
{
	}

// Management

void  MCALL CMagMLANDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void  MCALL CMagMLANDriver::Open(void)
{
	}

// Device

CCODE MCALL CMagMLANDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_bDrop  = GetByte(pData);

			m_pCtx->m_bBWF    = 0;
			m_pCtx->m_dRC     = 0L;
			m_pCtx->m_dWO     = 0L;
			m_pCtx->m_dOP     = 0L;
			m_pCtx->m_dSet[0] = READSET;
			m_pCtx->m_wBWW    = 0;
			m_pCtx->m_wVolt   = 0;
			m_pCtx->m_dNAK    = 0;

			memset( m_pCtx->m_dTOTR, 0, sizeof(m_pCtx->m_dTOTR) );

			for( UINT i = 1; i < 28; i++ ) m_pCtx->m_dSet[i] = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CMagMLANDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry

CCODE MCALL CMagMLANDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPTYP;
	Addr.a.m_Offset = RTYP;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

// Drop Number
BYTE CMagMLANDriver::GetDevDrop(void)
{
	return m_pCtx->m_bDrop;
	}

// Transport Layer

void  CMagMLANDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL  CMagMLANDriver::Transact(BOOL fIsWrite)
{
	Send();

	return GetReply(fIsWrite);
	}

BOOL  CMagMLANDriver::GetReply(BOOL fIsWrite)
{
	UINT uPtr   = 0;
	UINT uState = 0;
	UINT uTime  = 1200;
	UINT uData;

	if( fIsWrite ) RspFrame[RSPSIZE] = 4;

	SetTimer(uTime);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (uData = Get(uTime)) < NOTHING ) {

			BYTE bData = LOBYTE(uData);

//**/			AfxTrace1("<%2.2x>", bData);

			m_bRx[uPtr++] = bData;

			if( uPtr >= sizeof(m_bRx) ) return FALSE;

			switch( uState ) {

				case 0:
					uState = !m_bTx[0] || bData == m_bTx[0] ? 1 : 10;
					break;

				case 1:
					if( bData == m_bTx[1] ) uState = 2;

					else {
						if( fIsWrite && bData == 48 ) uState = 2;

						else {
							switch( m_bTx[1] ) {

								case RTOTR:
								case RTOTS:
									if( bData == 2 * m_bTx[1] ) {

										RspFrame[RSPSIZE] = 3;
										uState = 2;
										}

									else uState = 10;
									break;

								default: uState = 10;
								}
							}
						}

				case 2:
					if( uPtr >= RspFrame[RSPSIZE] ) {

						if( fIsWrite && m_bTx[1] == WSSS ) return TRUE;

						return !fIsWrite ? TRUE : m_bRx[2] == ACK;
						}
					break;

				case 10:
					if( uPtr == 4 ) {

						if( m_bRx[1] == 48 && m_bRx[2] == NAK ) return FALSE;
						}

					break;
				}
			}
		}

	return FALSE;
	}

// Port Access

void  CMagMLANDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i] );

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

UINT  CMagMLANDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
