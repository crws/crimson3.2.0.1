#include "intern.hpp"

#include "ctinitpe.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CTI NITP Master TCP/IP Driver
//

// Instantiator

INSTANTIATE(CCtiNitpTcpM);

// Constructor

CCtiNitpTcpM::CCtiNitpTcpM(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CCtiNitpTcpM::~CCtiNitpTcpM(void)
{
	}

// End of File
