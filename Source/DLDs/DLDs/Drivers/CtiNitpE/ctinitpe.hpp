#include "t500tcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CTI NITP Master TCP/IP Driver
//

class CCtiNitpTcpM : public CT500TcpM
{
	public:
		// Constructor
		CCtiNitpTcpM(void);

		// Destructor
		~CCtiNitpTcpM(void);
	};

// End of File
