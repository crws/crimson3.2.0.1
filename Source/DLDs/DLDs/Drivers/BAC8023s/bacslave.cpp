
#include "intern.hpp"

#include "bacslave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet Slave Driver
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

// Debug Flag

#define	TRACE 0

// Constructor

CBACNetSlave::CBACNetSlave(void)
{
	m_pTrace  = NULL;

	m_pTxBuff = NULL;

	m_pBase   = NULL;
	}

// Destructor

CBACNetSlave::~CBACNetSlave(void)
{
	FreeObjectList();
	}

// Management

void MCALL CBACNetSlave::Open(void)
{
	BuildObjectList();
	}

// Entry Points

void MCALL CBACNetSlave::Service(void)
{
	Debug("\n\nHello World from Device %u\n\n", m_pBase->m_Device);

	for(;;) {

		if( !m_pBase->m_fEnable ) {

			if( GetTimeStamp() >= m_pBase->m_dwTime ) {

				m_pBase->m_fEnable = TRUE;
				}
			}

		if( RecvFrame() ) {

			Process();

			m_pRxBuff->Release();

			m_pBase->m_dwAct = GetTimeStamp();
			}

		if( m_pBase->m_fRemote ) {

			if( !m_pBase->m_fIam || int(GetTimeStamp() - 30 - m_pBase->m_dwAct) >= 0) {

				m_pBase->m_fIam = OnSendIAm();
				}
			}
		}
	}

// Request Processing

BOOL CBACNetSlave::Process(void)
{
	BYTE bCode = *BuffStripHead(m_pRxBuff, BYTE);

	switch( bCode >> 4 ) {

		case 1:	return OnUnconfirmedRequest();

		case 0: return OnConfirmedRequest();
		}

	ShowRequest("UnknownRequest");

	return FALSE;
	}

BOOL CBACNetSlave::OnUnconfirmedRequest(void)
{
	m_bService = *BuffStripHead(m_pRxBuff, BYTE);

	if( m_pBase->m_fEnable ) {

		switch( m_bService ) {

			case 7: return OnWhoHasRequest();

			case 8: return OnWhoIsRequest();
			}

		ShowRequest("UnknownUnconfirmed");
		}

	return FALSE;
	}

BOOL CBACNetSlave::OnWhoHasRequest(void)
{
	ShowRequest("WhoHas");

	DWORD DevMin = 0;

	DWORD DevMax = 4194303;

	DWORD Object = 0;

	NAME  Name   = { 0 };

	while( m_pRxBuff->GetSize() ) {

		BOOL t = StripCtxUnsigned(0, DevMin) ||

			 StripCtxUnsigned(1, DevMax) ||
			
			 StripCtxUnsigned(2, Object) ||
			
			 StripCtxString  (3, Name)   ||
			
			 StripCtxUnknown ();

		if( !t ) {
			
			return FALSE;
			}
		}

	if( Name[0] ) {

		// TODO -- If we return tag names here, we need to somehow
		// provide a mechanism to look them up when someone sends
		// as WhoHas request with a name in it...

		if( !DecodeName(Name, Object) ) {

			return FALSE;
			}
		}

	if( Object ) {

		Debug("  From   = %u\n", DevMin);

		Debug("  To     = %u\n", DevMax);

		Debug("  Object = %u\n", Object);

		if( m_pBase->m_Device >= DevMin && m_pBase->m_Device <= DevMax ) {

			BOOL fOkay = FALSE;

			UINT Type  = (Object >> 22);

			UINT Inst  = (Object &  0x003FFFFF);

			if( Type == typeDevice ) {
				
				if( Inst == m_pBase->m_Device ) {

					EncodeDeviceName(Name);

					fOkay = TRUE;
					}
				}
			else {
				IMetaData *pm = NULL;

				if( HasValue(Type, Inst, &pm) ) {

					EncodeObjectName(Name, Type, Inst, pm);

					fOkay = TRUE;
					}

				if( pm ) {

					pm->Release();
					}
				}

			if( fOkay && InitFrame() ) {

				AddRawByte(0x10);

				AddRawByte(0x01);

				AddAppObjectID(m_pBase->m_Device | 0x02000000);

				AddAppObjectID(Object);

				AddAppString(Name);

				AddDone();

				return SendFrame(FALSE, FALSE);
				}
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::OnWhoIsRequest(void)
{
	ShowRequest("WhoIs");

	DWORD DevMin   = 0;

	DWORD DevMax   = 4194303;

	while( m_pRxBuff->GetSize() ) {

		BOOL t = StripCtxUnsigned(0, DevMin) ||
		
			 StripCtxUnsigned(1, DevMax) ||
			
			 StripCtxUnknown ();

		if( !t ) {
			
			return FALSE;
			}
		}

	Debug("  From = %u\n", DevMin);

	Debug("  To   = %u\n", DevMax);

	if( m_pBase->m_Device >= DevMin && m_pBase->m_Device <= DevMax ) {

		if( OnSendIAm() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::OnConfirmedRequest(void)
{
	m_bMaximum = *BuffStripHead(m_pRxBuff, BYTE);

	m_bInvoke  = *BuffStripHead(m_pRxBuff, BYTE);

	m_bService = *BuffStripHead(m_pRxBuff, BYTE);

	switch( m_bService ) {

		case 20: return OnReinitializeDevice();

		case 17: return OnDeviceCommunicationControl();
		}

	if( m_pBase->m_fEnable ) {

		switch( m_bService ) {

			case 15: return OnWritePropertyRequest();

			case 12: return OnReadPropertyRequest();

			case 14: return OnReadPropertyMultipleRequest();
			}

		ShowRequest("UnknownConfirmed");

		return OnSendReject(rejectUnknownService);
		}

	return FALSE;
	}

BOOL CBACNetSlave::OnReinitializeDevice(void)
{
	ShowRequest("ReinitializeDevice");

	DWORD Action   = NOTHING;

	NAME  Password = { 0 };

	while( m_pRxBuff->GetSize() ) {

		BOOL t = StripCtxUnsigned(0, Action  ) ||
			
			 StripCtxString  (1, Password) ||

			 StripCtxUnknown ();

		if( !t ) {

			OnSendReject(rejectInvalidTag);
			
			return FALSE;
			}
		}

	if( !strcmp(Password, m_pBase->m_sPass) ) {

		if( Action == 0 ) {

			if( OnSendSimpleAck() ) {

				HostTrap(0);

				return TRUE;
				}

			return FALSE;
			}

		if( Action == 1 ) {

			if( OnSendSimpleAck() ) {

				HostTrap(0);

				return TRUE;
				}

			return FALSE;
			}

		return OnSendError(classDevice, errorServiceDenied);
		}

	return OnSendError(classSecurity, errorAuthentication);
	}

BOOL CBACNetSlave::OnDeviceCommunicationControl(void)
{
	ShowRequest("DeviceCommunicationControl");

	DWORD Duration = NOTHING;

	DWORD Action   = NOTHING;

	NAME  Password = { 0 };

	while( m_pRxBuff->GetSize() ) {

		BOOL t = StripCtxUnsigned(0, Duration) ||

			 StripCtxUnsigned(1, Action  ) ||

			 StripCtxString  (2, Password) ||
			
			 StripCtxUnknown ();

		if( !t ) {

			OnSendReject(rejectInvalidTag);
			
			return FALSE;
			}
		}

	if( !strcmp(Password, m_pBase->m_sPass) ) {

		if( Action == 0 ) {

			m_pBase->m_fEnable = TRUE;

			return OnSendSimpleAck();
			}

		if( Action == 1 ) {

			if( Duration == NOTHING ) {

				m_pBase->m_dwTime  = NOTHING;

				m_pBase->m_fEnable = FALSE;
				}
			else {
				m_pBase->m_dwTime  = GetTimeStamp() + Duration * 60;

				m_pBase->m_fEnable = FALSE;
				}

			return OnSendSimpleAck();
			}

		if( Action == 2 ) {

			return OnSendSimpleAck();
			}

		return OnSendError(classDevice, errorServiceDenied);
		}

	return OnSendError(classSecurity, errorAuthentication);
	}

BOOL CBACNetSlave::OnWritePropertyRequest(void)
{
	ShowRequest("WriteProperty");

	DWORD Object   = 0;

	DWORD Prop     = 0;

	DWORD Index    = NOTHING;

	DWORD Priority = NOTHING;

	DWORD Data     = NOTHING;

	BOOL  fNull    = FALSE;

	while( m_pRxBuff->GetSize() ) {

		BOOL t = StripCtxUnsigned(0, Object     ) ||

			 StripCtxUnsigned(1, Prop       ) ||

			 StripCtxUnsigned(2, Index      ) ||

			 StripCtxAppData (3, Data, fNull) ||

			 StripCtxUnsigned(4, Priority   ) ||
			
			 StripCtxUnknown ();

		if( !t ) {

			OnSendReject(rejectInvalidTag);
			
			return FALSE;
			}
		}

	if( Object ) {

		UINT Type = (Object >> 22);

		UINT Inst = (Object &  0x003FFFFF);

		if( Type == typeDevice ) {

			return OnSendError(classProperty, errorWriteDenied);
			}

		if( IsAnalog(Type) || IsBinary(Type) || IsMultiState(Type) ) {

			if( HasValue(Type, Inst) ) {

				if( IsCommandable(Type) ) {

					if( Prop == propPresentValue ) {

						CCmd *pCmd = FindCommand(Type, Inst);

						if( pCmd ) {

							Debug("  Priority array in use\n");

							UINT Slot = 15;

							WORD Mask = 0x8000;

							if( Priority >= 1 && Priority <= 16 ) {

								Slot = Priority - 1;

								Mask = (1 << Slot);

								Debug("  Priority is %u\n", Priority);
								}
							else
								Debug("  Priority is default\n");

							if( !fNull ) {

								pCmd->m_Mask      |= Mask;

								pCmd->m_Data[Slot] = Data;

								Debug("  Data is valid\n");
								}
							else {
								pCmd->m_Mask &= ~Mask;
								
								Debug("  Data is null\n");
								}

							if( pCmd->m_Mask ) {

								Debug("  Mask is now %4.4X\n", pCmd->m_Mask);

								Slot = 0;

								Mask = 1;

								while( !(pCmd->m_Mask & Mask) ) {

									Mask <<= 1;

									Slot  += 1;
									}

								Debug("  Found data at %u\n", Slot);

								SetValue(Type, Inst, pCmd->m_Data[Slot]);
								}
							}
						else {
							Debug("  Priority array disabled\n");

							if( !fNull ) {

								Debug("  Data is valid\n");

								SetValue(Type, Inst, Data);
								}
							else
								Debug("  Data is null\n");
							}

						return OnSendSimpleAck();
						}
					}

				return OnSendError(classProperty, errorWriteDenied);
				}
			}
		}

	return OnSendError(classObject, errorUnknownObject);
	}

BOOL CBACNetSlave::OnReadPropertyRequest(void)
{
	ShowRequest("ReadProperty");

	DWORD Object = 0;

	DWORD Prop   = 0;

	DWORD Index  = NOTHING;

	while( m_pRxBuff->GetSize() ) {

		BOOL t = StripCtxUnsigned(0, Object) ||

			 StripCtxUnsigned(1, Prop  ) ||

			 StripCtxUnsigned(2, Index ) ||
			
			 StripCtxUnknown ();

		if( !t ) {

			OnSendReject(rejectInvalidTag);

			return FALSE;
			}
		}

	if( Object ) {

		UINT Type = (Object >> 22);

		UINT Inst = (Object &  0x003FFFFF);

		if( InitFrame() ) {

			AddRawByte(0x30);

			AddRawByte(m_bInvoke);

			AddRawByte(m_bService);

			AddCtxLong(0, Object);

			if( OnReadProperty(1, Type, Inst, Prop, Index) ) {

				AddDone();

				return SendFrame(TRUE, FALSE);
				}

			return OnSendError(m_bClass, m_bError);
			}
		}

	return OnSendError(classObject, errorUnknownObject);
	}

BOOL CBACNetSlave::OnReadPropertyMultipleRequest(void)
{
	ShowRequest("ReadPropertyMultiple");

	if( InitFrame() ) {

		AddRawByte(0x30);

		AddRawByte(m_bInvoke);

		AddRawByte(m_bService);

		for(;;) {

			DWORD Object = 0;

			if( !m_pRxBuff->GetSize() || !StripCtxUnsigned(0, Object) ) {

				break;
				}

			if( !StripCtxOpen(1) ) {

				return OnSendReject(rejectInvalidTag);
				}

			UINT Type = (Object >> 22);

			UINT Inst = (Object &  0x003FFFFF);

			AddCtxLong(0, Object);

			AddCtxOpen(1);

			while( m_pRxBuff->GetSize() ) {

				DWORD Prop  = 0;

				DWORD Index = NOTHING;

				if( !StripCtxUnsigned(0, Prop) ) {

					break;
					}
				
				if( Prop == propAll || Prop == propRequired || Prop == propOptional ) {

					switch( Prop ) {

						case propAll:
							
							Debug("  All\n");

							break;

						case propRequired: 

							Debug("  Required\n");

							break;

						case propOptional: 

							Debug("  Optional\n");

							break;
						}

					if( *m_pRxBuff->GetData() != 0x1F ) {

						if( StripCtxUnsigned(1, Index) ) {

							return OnSendReject(rejectInvalidTag);
							}
						}

					if( !OnReadAll(Type, Inst, Prop) ) {

						AddCtxByte(2, BYTE(Prop));

						AddCtxOpen(5);

						AddAppEnum(classObject);

						AddAppEnum(errorUnknownObject);

						AddCtxClose(5);
						}

					continue;
					}

				if( *m_pRxBuff->GetData() != 0x1F ) {

					StripCtxUnsigned(1, Index);
					}

				if( !OnReadProperty(2, Type, Inst, Prop, Index) ) {

					if( m_bClass == classAbort) {

						Debug("  Abort\n");

						return OnSendAbort(m_bError);
						}

					if( m_bClass == classReject ) {

						Debug("  Reject\n");

						return OnSendReject(m_bError);
						}

					Debug("  Missing\n");

					AddCtxOpen(5);

					AddAppEnum(m_bClass);

					AddAppEnum(m_bError);

					AddCtxClose(5);
					}
				}

			if( !StripCtxClose(1) ) {

				return OnSendReject(rejectInvalidTag);
				}

			AddCtxClose(1);
			}

		AddDone();

		return SendFrame(TRUE, FALSE);
		}

	return OnSendError(classObject, errorUnknownObject);
	}

// Read Helpers

BOOL CBACNetSlave::OnReadAll(UINT Type, UINT Inst, BYTE Prop)
{
	if( Type == typeDevice ) {

		if( Inst == m_pBase->m_Device ) {

			static BYTE const List[] = {

				propObjectIdentifer,
				propObjectName,
				propObjectType,
				propSystemStatus,
				propVendorName,
				propVendorIdentifier,
				propModelName,
				propFirmwareRevision,
				propApplicationSoftwareVersion,
				propProtocolVersion,
				propProtocolRevision,
				propProtocolServicesSupported,
				propProtocolObjectTypesSupported,
				propObjectList,
				propMaxAPDULengthAccepted,
				propSegmentationSupport,
				propAPDUTimeout,
				propNumberOfAPDURetries,
				propDeviceAddressBinding,
				propDatabaseRevision,
				0,
				propMaxSegmentsAccepted,
				propLocalDate,
				propLocalTime,
				};

			OnReadAll(Type, Inst, Prop, List, elements(List));

			return TRUE;
			}
		}
	else {
		if( HasValue(Type, Inst) ) {

			if( IsCommandable(Type) ) { 

				static BYTE const List[] = {

					propRelinquishDefault,
					propPriorityArray,
					};

				OnReadAll(Type, Inst, Prop, List, elements(List));
				}

			if( IsAnalog(Type) ) {

				static BYTE const List[] = {

					propObjectIdentifer,
					propObjectName,
					propObjectType,
					propPresentValue,
					propStatusFlags,
					propEventState,
					propOutOfService,
					propUnits,
					0,
					propDescription,
					propHighLimit,
					propLowLimit,
					};

				OnReadAll(Type, Inst, Prop, List, elements(List));

				return TRUE;
				}

			if( IsBinary(Type) ) {

				static BYTE const List[] = {

					propObjectIdentifer,
					propObjectName,
					propObjectType,
					propPresentValue,
					propStatusFlags,
					propEventState,
					propOutOfService,
					propPolarity,
					0,
					propDescription,
					propActiveText,
					propInactiveText,
					};

				OnReadAll(Type, Inst, Prop, List, elements(List));

				return TRUE;
				}
			
			if( IsMultiState(Type) ) {

				static BYTE const List[] = {

					propObjectIdentifer,
					propObjectName,
					propObjectType,
					propPresentValue,
					propStatusFlags,
					propEventState,
					propOutOfService,
					propNumberOfStates,
					0,
					propDescription,
					propStateText,
					};

				OnReadAll(Type, Inst, Prop, List, elements(List));

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::OnReadAll(UINT Type, UINT Inst, BYTE Prop, PCBYTE pList, UINT uCount)
{
	BOOL fUse = TRUE;

	if( Prop == propOptional ) {

		fUse = FALSE;
		}

	for( UINT n = 0; n < uCount; n++ ) {

		if( !pList[n] ) {

			if( Prop == propRequired ) {

				break;
				}

			if( Prop == propOptional ) {

				fUse = TRUE;
				}

			continue;
			}

		if( fUse ) {
		
			if( OnReadProperty(2, Type, Inst, pList[n], NOTHING) ) {

				continue;
				}

			Debug("  Failed???\n");
			}
		}

	return TRUE;
	}

BOOL CBACNetSlave::OnReadProperty(BYTE bTag, UINT Type, UINT Inst, DWORD Prop, DWORD Index)
{
	Debug("  Read %u.%u.%-3u", Type, Inst, Prop);

	AddCtxByte(bTag + 0, BYTE(Prop));

	SaveFrame();

	if( Index < NOTHING ) {

		AddCtxUnsigned(bTag + 1, Index);
		}

	AddCtxOpen(bTag + 2);

	if( Type == typeDevice ) {

		if( Inst == m_pBase->m_Device ) {

			if( OnReadDeviceProperty(Prop, Index) ) {

				Debug("  Device\n");

				AddCtxClose(bTag + 2);

				return TRUE;
				}

			if( OnReadStdProperty(Type, Inst, Prop) ) {

				Debug("  Device Std\n");

				AddCtxClose(bTag + 2);

				return TRUE;
				}

			LoadFrame();

			return FALSE;
			}
		}
	else {
		IMetaData *pm = NULL;

		if( HasValue(Type, Inst, &pm) ) {

			if( OnReadObjectProperty(Type, Inst, Prop, Index, pm) ) {

				if( pm ) {

					pm->Release();
					}

				Debug("  Object\n");

				AddCtxClose(bTag + 2);

				return TRUE;
				}

			if( pm  ) {
				
				pm->Release();
				}

			if( OnReadStdProperty(Type, Inst, Prop) ) {

				Debug("  Object Std\n");

				AddCtxClose(bTag + 2);

				return TRUE;
				}

			LoadFrame();

			return FALSE;
			}
		}

	m_bClass = classObject;
	
	m_bError = errorUnknownObject;

	LoadFrame();

	return FALSE;
	}

BOOL CBACNetSlave::OnReadStdProperty(UINT Type, UINT Inst, BYTE Prop)
{
	if( Prop == propObjectIdentifer ) {

		DWORD Object = ((Type << 22) | Inst);

		AddAppObjectID(Object);

		return TRUE;
		}

	if( Prop == propObjectName ) {

		NAME Name;

		EncodeObjectName(Name, Type, Inst);

		AddAppString(Name);

		return TRUE;
		}

	if( Prop == propObjectType ) {

		AddAppUnsigned(Type);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBACNetSlave::OnReadDeviceProperty(BYTE Prop, UINT Index)
{
	if( Prop == propSystemStatus ) {

		AddAppEnum(0);

		return TRUE;
		}

	if( Prop == propVendorName ) {

		AddAppString("Red Lion Controls Inc.");

		return TRUE;
		}

	if( Prop == propVendorIdentifier ) {

		AddAppUnsigned(VENDOR_ID);

		return TRUE;
		}

	if( Prop == propModelName ) {

		AddAppString("G3");

		return TRUE;
		}

	if( Prop == propFirmwareRevision ) {

		AddAppString("1.10");

		return TRUE;
		}

	if( Prop == propApplicationSoftwareVersion ) {

		AddAppString("UNKNOWN");

		return TRUE;
		}

	if( Prop == propProtocolVersion ) {

		AddAppUnsigned(1);

		return TRUE;
		}

	if( Prop == propProtocolRevision ) {

		AddAppUnsigned(4);

		return TRUE;
		}

	if( Prop == propProtocolServicesSupported ) {

		AddAppBitString(40, 12, 14, 15, 17, 20, 26, 27, 33, 34, NOTHING);

		return TRUE;
		}

	if( Prop == propProtocolObjectTypesSupported ) {

		AddAppBitString(24, 0, 1, 2, 3, 4, 5, 8, 13, 14, 19, NOTHING);

		return TRUE;
		}

	if( Prop == propObjectList ) {

		if( Index ) {

			if( Index == NOTHING ) {

				if( m_pBase->m_uCount < 80 ) {

					for( UINT n = 0; n < m_pBase->m_uCount; n++ ) {

						AddAppObjectID(m_pBase->m_pList[n].m_Object);
						}

					return TRUE;
					}

				m_bClass = classAbort;

				m_bError = abortSegmentation;

				return FALSE;
				}

			if( Index <= m_pBase->m_uCount) {

				AddAppObjectID(m_pBase->m_pList[Index - 1].m_Object);

				return TRUE;
				}

			m_bClass = classProperty;

			m_bError = errorInvalidIndex;

			return FALSE;
			}

		AddAppUnsigned(m_pBase->m_uCount);

		return TRUE;
		}

	if( Prop == propMaxAPDULengthAccepted ) {

		AddAppUnsigned(500);

		return TRUE;
		}

	if( Prop == propSegmentationSupport ) {

		AddAppEnum(3);

		return TRUE;
		}

	if( Prop == propMaxSegmentsAccepted ) {

		AddAppUnsigned(1);

		return TRUE;
		}

	if( Prop == propAPDUTimeout ) {

		AddAppUnsigned(60000);

		return TRUE;
		}

	if( Prop == propNumberOfAPDURetries ) {

		AddAppUnsigned(3);

		return TRUE;
		}

	if( Prop == propDeviceAddressBinding ) {

		return TRUE;
		}

	if( Prop == propDatabaseRevision ) {

		AddAppUnsigned(1);

		return TRUE;
		}

	if( Prop == propLocalDate ) {

		AddAppDate(GetTimeStamp());

		return TRUE;
		}

	if( Prop == propLocalTime ) {

		AddAppTime(GetTimeStamp());

		return TRUE;
		}

	m_bClass = classProperty;

	m_bError = errorUnknownProperty;

	return FALSE;
	}

BOOL CBACNetSlave::OnReadObjectProperty(UINT Type, UINT Inst, BYTE Prop, UINT Index, IMetaData *pm)
{
	if( Prop == propPresentValue ) {

		OnReadObjectPresentValue(Type, Inst);

		return TRUE;
		}

	if( Prop == propStatusFlags ) {

		AddAppBitString(4, NOTHING);

		return TRUE;
		}

	if( Prop == propEventState ) {

		AddAppEnum(0);

		return TRUE;
		}

	if( Prop == propOutOfService ) {

		AddAppBoolean(FALSE);

		return TRUE;
		}

	if( Prop == propObjectName ) {

		NAME Name;

		EncodeObjectName(Name, Type, Inst, pm);

		AddAppString(Name);

		return TRUE;
		}

	if( Prop == propDescription ) {

		if( pm ) {

			if( m_pBase->m_Desc ) {

				if( m_pBase->m_Desc == 1 ) {

					UINT Props[] = { tpDescription,
							 tpLabel,
							 tpName
							 };

					for( UINT n = 0; n < elements(Props); n++ ) {

						if( GetMetaString(pm, Props[n], m_sText, elements(m_sText)) ) {

							if( m_sText[0] ) {

								AddAppString(m_sText);

								return TRUE;
								}
							}
						}
					}
				else {
					UINT Props[] = { tpName,
							 tpLabel,
							 tpDescription
							 };

					if( GetMetaString(pm, Props[m_pBase->m_Desc - 2], m_sText, elements(m_sText)) ) {

						if( m_sText[0] ) {

							AddAppString(m_sText);

							return TRUE;
							}
						}
					}
				}
			}

		AddAppString("");

		return TRUE;
		}

	if( IsAnalog(Type) ) {

		if( Prop == propUnits ) {

			AddAppEnum(95);

			return TRUE;
			}

		if( Prop == propLowLimit ) {

			UINT  Local = NOTHING;

			DWORD Data  = 0;

			if( m_pBase->m_Rich ) {
				
				if( pm ) {

					GetValue(Type, Inst, Data, Local);

					Data = GetMetaNumber(pm, tpMinimum);
					}
				}

			OnReadObjectDataValue(Data, Local);

			return TRUE;
			}

		if( Prop == propHighLimit ) {

			UINT  Local = NOTHING;

			DWORD Data  = 0;

			if( m_pBase->m_Rich ) {
				
				if( pm ) {

					GetValue(Type, Inst, Data, Local);

					Data = GetMetaNumber(pm, tpMaximum);
					}
				}

			OnReadObjectDataValue(Data, Local);

			return TRUE;
			}
		}

	if( IsBinary(Type) ) {

		if( Prop == propActiveText ) {

			if( m_pBase->m_Rich ) {
				
				if( pm ) {

					if( GetMetaString(pm, tpTextOn, m_sText, elements(m_sText)) ) {

						if( m_sText[0] ) {

							AddAppString(m_sText);

							return TRUE;
							}
						}
					}
				}

			AddAppString("ON");

			return TRUE;
			}

		if( Prop == propInactiveText ) {

			if( m_pBase->m_Rich ) {
				
				if( pm ) {

					if( GetMetaString(pm, tpTextOff, m_sText, elements(m_sText)) ) {

						if( m_sText[0] ) {

							AddAppString(m_sText);

							return TRUE;
							}
						}
					}
				}

			AddAppString("OFF");

			return TRUE;
			}

		if( Prop == propPolarity ) {

			AddAppEnum(0);

			return TRUE;
			}
		}

	if( IsMultiState(Type) ) {

		if( Prop == propNumberOfStates ) {

			UINT uCount = GetMetaNumber(pm, tpStateCount);

			if( !uCount ) {

				uCount = 3;
				}

			AddAppEnum(uCount);

			return TRUE;
			}

		if( Prop == propStateText ) {
			
			UINT uCount = GetMetaNumber(pm, tpStateCount);

			if( !uCount ) {

				uCount = 3;
				}

			if( Index ) {
	
				if( Index == NOTHING ) {

					if( uCount < 20 ) {

						for( UINT n = 0; n < uCount; n++ ) {

							NAME Text;

							if( !GetMetaString(pm, tpStateText + n, Text, sizeof(Text)) ) {

								SPrintf(Text, "STATE%u", n + 1);
								}

							AddAppString(Text);
							}

						return TRUE;
						}

					m_bClass = classAbort;

					m_bError = abortSegmentation;

					return FALSE;
					}
				else {
					if( Index <= uCount ) {

						NAME Text;

						if( !GetMetaString(pm, tpStateText + Index, Text, sizeof(Text)) ) {

							SPrintf(Text, "STATE%u", Index);
							}

						AddAppString(Text);

						return TRUE;
						}

					m_bClass = classProperty;

					m_bError = errorInvalidIndex;

					return FALSE;
					}
				}
			else {
				AddAppUnsigned(uCount);

				return TRUE;
				}
			}
		}

	if( IsCommandable(Type) ) {

		if( Prop == propRelinquishDefault ) {

			OnReadObjectPresentValue(Type, Inst);

			return TRUE;
			}

		if( Prop == propPriorityArray ) {

			CCmd *pCmd = FindCommand(Type, Inst);

			if( pCmd ) {

				if( Index ) {

					DWORD Dummy;

					UINT  Local;

					if( !GetValue(Type, Inst, Dummy, Local) ) {

						Local = addrLongAsLong;
						}
						
					if( Index == NOTHING ) {

						WORD Mask = 1;

						for( UINT Slot = 0; Slot < 16; Slot++ ) {

							if( pCmd->m_Mask & Mask ) {

								OnReadObjectDataValue(pCmd->m_Data[Slot], Local);
								}
							else
								AddAppNull();

							Mask <<= 1;
							}

						return TRUE;
						}

					if( Index <= 16 ) {

						UINT Slot = Index - 1;
						
						UINT Mask = (1 << Slot);

						if( pCmd->m_Mask & Mask ) {

							OnReadObjectDataValue(pCmd->m_Data[Slot], Local);
							}
						else
							AddAppNull();
						
						return TRUE;
						}

					m_bClass = classProperty;

					m_bError = errorInvalidIndex;

					return FALSE;
					}

				AddAppUnsigned(16);

				return TRUE;
				}
			}
		}

	m_bClass = classProperty;

	m_bError = errorUnknownProperty;

	return FALSE;
	}

BOOL CBACNetSlave::OnReadObjectPresentValue(UINT Type, UINT Inst)
{
	DWORD Data;

	UINT  Local;

	if( !GetValue(Type, Inst, Data, Local) ) {

		Local = addrLongAsLong;

		Data  = 0;
		}

	if( Local == addrRealAsReal ) {

		AddAppReal(Data);
		}

	if( Local == addrLongAsLong ) {

		AddAppReal(LongToReal(Data));
		}

	if( Local <= addrBitAsLong ) {

		AddAppEnum(Data);
		}

	return TRUE;
	}

BOOL CBACNetSlave::OnReadObjectDataValue(DWORD Data, UINT Local)
{
	if( Local == addrRealAsReal ) {

		AddAppReal(Data);

		return TRUE;
		}

	if( Local == addrLongAsLong ) {

		AddAppReal(LongToReal(Data));

		return TRUE;
		}

	if( Local <= addrBitAsLong ) {

		AddAppEnum(Data);
		
		return TRUE;
		}

	Debug("  Null???\n");

	AddAppNull();

	return FALSE;
	}

BOOL CBACNetSlave::OnSendError(BYTE bClass, BYTE bError)
{
	if( KillFrame() ) {

		Debug("  SendError %u.%u\n", bClass, bError);

		AddRawByte(0x50);

		AddRawByte(m_bInvoke);

		AddRawByte(m_bService);

		AddAppEnum(bClass);

		AddAppEnum(bError);

		AddDone();

		return SendFrame(TRUE, FALSE);
		}

	return FALSE;
	}

BOOL CBACNetSlave::OnSendReject(BYTE bReject)
{
	if( KillFrame() ) {

		Debug("  SendReject %u\n", bReject);

		AddRawByte(0x60);

		AddRawByte(m_bInvoke);

		AddRawByte(bReject);

		AddDone();

		return SendFrame(TRUE, FALSE);
		}

	return FALSE;
	}

BOOL CBACNetSlave::OnSendAbort(BYTE bAbort)
{
	if( KillFrame() ) {

		Debug("  SendAbort %u\n", bAbort);

		AddRawByte(0x71);

		AddRawByte(m_bInvoke);

		AddRawByte(bAbort);

		AddDone();

		return SendFrame(TRUE, FALSE);
		}

	return FALSE;
	}

BOOL CBACNetSlave::OnSendSimpleAck(void)
{
	if( InitFrame() ) {

		AddRawByte(0x20);

		AddRawByte(m_bInvoke);

		AddRawByte(m_bService);

		AddDone();

		return SendFrame(TRUE, FALSE);
		}

	return FALSE;
	}

BOOL CBACNetSlave::OnSendIAm(void)
{
	if( InitFrame() ) {

		AddRawByte(0x10);

		AddRawByte(0x00);

		AddAppObjectID(m_pBase->m_Device | 0x02000000);

		AddAppUnsigned(500);

		AddAppEnum(3);

		AddAppUnsigned(VENDOR_ID);

		AddDone();

		return SendFrame(FALSE, FALSE);
		}

	return FALSE;
	}

// Configuration

BOOL CBACNetSlave::LoadConfig(PCBYTE &pData)
{
	m_pBase->m_Device  = GetLong(pData);

	strcpy(m_pBase->m_DevName, GetString(pData));

	m_pBase->m_uLimit  = GetWord(pData);

	m_pBase->m_Name    = GetByte(pData);

	m_pBase->m_Desc    = GetByte(pData);

	m_pBase->m_Rich    = GetByte(pData);

	m_pBase->m_fEnable = TRUE;

	strcpy(m_pBase->m_sPass, GetString(pData));

	m_pBase->m_uPort   = GetWord(pData);

	m_pBase->m_fIam    = FALSE;

	m_pBase->m_dwAct   = GetTimeStamp();

	m_pBase->m_fRemote = TRUE;

	return TRUE;
	}

// Basic Frame Building

BOOL CBACNetSlave::InitFrame(void)
{
	UINT uSize = FindMaxBufferSize();

	if( (m_pTxBuff = CreateBuffer(uSize, TRUE)) ) {

		m_pTxBuff->AddTail(uSize);

		m_pTxData = m_pTxBuff->GetData();

		m_uTxPtr  = 0;

		return TRUE;
		}
	
	Sleep(10);

	return FALSE;
	}

BOOL CBACNetSlave::KillFrame(void)
{
	if( m_pTxBuff ) {

		m_uTxPtr  = 0;

		return TRUE;
		}

	return InitFrame();
	}

void CBACNetSlave::SaveFrame(void)
{
	m_uTxSave = m_uTxPtr;
	}

void CBACNetSlave::LoadFrame(void)
{
	m_uTxPtr = m_uTxSave;
	}

void CBACNetSlave::AddRawData(PCTXT pText)
{
	while( *pText ) {

		AddRawByte(*pText++);
		}
	}

void CBACNetSlave::AddRawData(PBYTE pData, UINT uSize)
{
	while( uSize-- ) {

		AddRawByte(*pData++);
		}
	}

void CBACNetSlave::AddRawByte(BYTE bData)
{
	if( m_uTxPtr < m_pTxBuff->GetSize() ) {

		m_pTxData[m_uTxPtr++] = bData;
		}
	}

void CBACNetSlave::AddRawWord(WORD wData)
{
	AddRawByte(HIBYTE(wData));

	AddRawByte(LOBYTE(wData));
	}

void CBACNetSlave::AddRawTrip(DWORD dwData)
{
	AddRawByte(LOBYTE(HIWORD(dwData)));

	AddRawWord(LOWORD(dwData));
	}

void CBACNetSlave::AddRawLong(DWORD dwData)
{
	AddRawWord(HIWORD(dwData));

	AddRawWord(LOWORD(dwData));
	}

void CBACNetSlave::AddDone(void)
{
	m_pTxBuff->StripTail(m_pTxBuff->GetSize() - m_uTxPtr);
	}

void CBACNetSlave::AddNetworkHeader(BOOL fThis, BOOL fReply)
{
	if( fThis ) {

		if( m_RxNet.m_NET ) {

			UINT  uSize = 6 + m_RxNet.m_LEN;

			PBYTE pData = m_pTxBuff->AddHead(uSize);

			pData[0] = 0x01;

			pData[1] = fReply ? 0x24 : 0x20;

			pData[2] = HIBYTE(m_RxNet.m_NET);

			pData[3] = LOBYTE(m_RxNet.m_NET);

			pData[4] = m_RxNet.m_LEN;

			memcpy(pData + 5, m_RxNet.m_MAC, m_RxNet.m_LEN);

			pData[5 + m_RxNet.m_LEN] = 200;
			}
		else {
			UINT  uSize = 2;

			PBYTE pData = m_pTxBuff->AddHead(uSize);

			pData[0] = 0x01;
		
			pData[1] = fReply ? 0x04 : 0x00;
			}
		}
	else {
		UINT  uSize = 6;

		PBYTE pData = m_pTxBuff->AddHead(uSize);

		pData[0] = 0x01;
		
		pData[1] = fReply ? 0x24 : 0x20;

		pData[2] = 0xFF;

		pData[3] = 0xFF;

		pData[4] = 0;

		pData[5] = 200;
		}
	}

// Context Tag Building

void CBACNetSlave::AddCtxTag(BYTE bTag, BYTE bSize)
{
	AddRawByte((bTag << 4) | 0x08 | bSize);
	}

void CBACNetSlave::AddCtxByte(BYTE bTag, BYTE bData)
{
	AddCtxTag (bTag, sizeof(BYTE));

	AddRawByte(bData);
	}

void CBACNetSlave::AddCtxWord(BYTE bTag, WORD wData)
{
	AddCtxTag (bTag, sizeof(WORD));

	AddRawWord(wData);
	}

void CBACNetSlave::AddCtxTrip(BYTE bTag, DWORD dwData)
{
	AddCtxTag (bTag, sizeof(WORD) + sizeof(BYTE));

	AddRawTrip(dwData);
	}

void CBACNetSlave::AddCtxLong(BYTE bTag, DWORD dwData)
{
	AddCtxTag(bTag, sizeof(DWORD));

	AddRawLong(dwData);
	}

void CBACNetSlave::AddCtxUnsigned(BYTE bTag, DWORD dwData)
{
	if( (dwData & 0xFFFFFF00) == 0x00000000 ) {

		AddCtxTag (bTag, 1);

		AddRawByte(BYTE(dwData));

		return;
		}

	if( (dwData & 0xFFFF0000) == 0x00000000 ) {

		AddCtxTag (bTag, 2);

		AddRawWord(WORD(dwData));

		return;
		}

	if( (dwData & 0xFF000000) == 0x00000000 ) {

		AddCtxTag (bTag, 3);

		AddRawTrip(dwData);

		return;
		}

	AddCtxTag (bTag, 4);

	AddRawLong(dwData);
	}

void CBACNetSlave::AddCtxOpen(BYTE bTag)
{
	AddCtxTag(bTag, 6);
	}

void CBACNetSlave::AddCtxClose(BYTE bTag)
{
	AddCtxTag(bTag, 7);
	}

// Application Tag Building

void CBACNetSlave::AddAppTag(BYTE bType, BYTE bSize)
{
	if( bSize < 5 ) {

		AddRawByte((bType << 4) | bSize);

		return;
		}

	AddRawByte((bType << 4) | 0x05);

	AddRawByte(bSize);
	}

void CBACNetSlave::AddAppNull(void)
{
	AddAppTag(0, 0);
	}

void CBACNetSlave::AddAppBoolean(BOOL fData)
{
	AddAppTag(1, fData ? 1 : 0);
	}

void CBACNetSlave::AddAppUnsigned(BYTE bType, DWORD dwData)
{
	if( (dwData & 0xFFFFFF00) == 0x00000000 ) {

		AddAppTag (bType, 1);

		AddRawByte(BYTE(dwData));

		return;
		}

	if( (dwData & 0xFFFF0000) == 0x00000000 ) {

		AddAppTag (bType, 2);

		AddRawWord(WORD(dwData));

		return;
		}

	if( (dwData & 0xFF000000) == 0x00000000 ) {

		AddAppTag (bType, 3);

		AddRawTrip(dwData);

		return;
		}

	AddAppTag (bType, 4);

	AddRawLong(dwData);
	}

void CBACNetSlave::AddAppSigned(BYTE bType, DWORD dwData)
{
	if( (dwData & 0xFFFFFF80) == 0x00000000 || (dwData & 0xFFFFFF80) == 0xFFFFFF80 ) {

		AddAppTag (bType, 1);

		AddRawByte(BYTE(dwData));

		return;
		}

	if( (dwData & 0xFFFF8000) == 0x00000000 || (dwData & 0xFFFF8000) == 0xFFFF8000 ) {

		AddAppTag (bType, 2);

		AddRawWord(WORD(dwData));

		return;
		}

	if( (dwData & 0xFF800000) == 0x00000000 || (dwData & 0xFF800000) == 0xFF800000 ) {

		AddAppTag (bType, 3);

		AddRawTrip(dwData);

		return;
		}

	AddAppTag (bType, 4);

	AddRawLong(dwData);
	}

void CBACNetSlave::AddAppUnsigned(DWORD dwData)
{
	AddAppUnsigned(2, dwData);
	}

void CBACNetSlave::AddAppSigned(DWORD dwData)
{
	AddAppSigned(3, dwData);
	}

void CBACNetSlave::AddAppReal(DWORD dwData)
{
	AddAppTag (4, sizeof(DWORD));

	AddRawLong(dwData);
	}

void CBACNetSlave::AddAppBitString(UINT uBits, ...)
{
	UINT  uSize = (uBits + 7) / 8;

	PBYTE pData = PBYTE(alloca(uSize));

	memset(pData, 0, uSize);

	va_list pArgs;

	va_start(pArgs, uBits);

	for(;;) {

		UINT uBit = va_arg(pArgs, UINT);

		if( uBit == NOTHING ) {

			break;
			}
		else {
			UINT n = (uBit / 8);

			UINT m = (0x80 >> (uBit % 8));

			pData[n] |= m;
			}
		}

	va_end(pArgs);

	AddAppTag (8, 1 + uSize);

	AddRawByte(uSize * 8 - uBits);

	AddRawData(pData, uSize);
	}

void CBACNetSlave::AddAppEnum(DWORD dwData)
{
	AddAppSigned(9, dwData);
	}

void CBACNetSlave::AddAppObjectID(DWORD Object)
{
	AddAppTag (12, sizeof(DWORD));

	AddRawLong(Object);
	}

void CBACNetSlave::AddAppString(PCTXT pData)
{
	AddAppTag (7, strlen(pData) + 1);

	AddRawByte(0);

	AddRawData(pData);
	}

void CBACNetSlave::AddAppDate(DWORD Date)
{
	BYTE y = GetYear (Date) - 1900;

	BYTE m = GetMonth(Date);

	BYTE d = GetDate (Date);

	AddAppTag (10, 4);

	AddRawByte(y);
	
	AddRawByte(m);
	
	AddRawByte(d);
	
	AddRawByte(0xFF);
	}

void CBACNetSlave::AddAppTime(DWORD Time)
{
	BYTE h = GetHour(Time);

	BYTE m = GetMin (Time);

	BYTE s = GetSec (Time);

	AddAppTag (11, 4);

	AddRawByte(h);
	
	AddRawByte(m);
	
	AddRawByte(s);
	
	AddRawByte(0);
	}

// Basic Frame Parsing

BOOL CBACNetSlave::StripNetworkHeader(void)
{
	if( !m_pRxBuff->GetSize() ) {

		return FALSE;
		}

	PBYTE pData = m_pRxBuff->GetData();

	UINT  uPtr  = 0;

	if( pData[uPtr++] == 0x01 ) {

		BYTE bFlags = pData[uPtr++];

		if( !(bFlags & 0x80) ) {

			if( bFlags & 0x20 ) {

				BYTE DLEN = pData[uPtr+2];

				uPtr      = uPtr + 3 + DLEN;
				}

			if( bFlags & 0x08 ) {

				m_RxNet.m_NET = MAKEWORD(pData[uPtr+1], pData[uPtr+0]);

				m_RxNet.m_LEN = pData[uPtr+2];

				memcpy(m_RxNet.m_MAC, pData + uPtr + 3, m_RxNet.m_LEN);

				uPtr = uPtr + 3 + m_RxNet.m_LEN;
				}
			else {
				BYTE bNull = 0;

				UINT uSize = sizeof(m_RxNet);

				memset(&m_RxNet, bNull, uSize);
				}

			if( bFlags & 0x20 ) {

				uPtr = uPtr + 1;
				}

			m_pRxBuff->StripHead(uPtr);

			return TRUE;
			}

		return FALSE;
		}

	Show("strip", "invalid network header");

	return FALSE;
	}

void CBACNetSlave::StripSize(UINT &uSize)
{
	if( uSize == 5 ) {

		uSize = *BuffStripHead(m_pRxBuff, BYTE);

		switch( uSize ) {

			case 254:
				uSize = MotorToHost(*BuffStripHead(m_pRxBuff, WORD));
				break;

			case 255:
				uSize = MotorToHost(*BuffStripHead(m_pRxBuff, DWORD));
				break;
			}
		}
	}

void CBACNetSlave::RecvDone(void)
{
	m_pRxBuff->Release();

	m_pRxBuff = NULL;
	}

// Context Tag Parsing

BOOL CBACNetSlave::StripCtx(BYTE bTag, UINT &uSize)
{
	if( m_pRxBuff->GetSize() ) {

		BYTE bCode = *m_pRxBuff->GetData();

		if( (bCode & 0xF8) == ((bTag << 4) | 0x08) ) {

			uSize = (bCode & 0x07);

			m_pRxBuff->StripHead(1);

			StripSize(uSize);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::StripCtxUnsigned(BYTE bTag, DWORD &dwData)
{
	UINT uSize;

	if( StripCtx(bTag, uSize) ) {

		if( uSize <= sizeof(DWORD) ) {

			dwData = 0;

			while( uSize-- ) {

				dwData <<= 8;

				dwData  += *BuffStripHead(m_pRxBuff, BYTE);
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::StripCtxSigned(BYTE bTag, DWORD &dwData)
{
	UINT uSize;

	if( StripCtx(bTag, uSize) ) {

		if( uSize <= sizeof(DWORD) ) {

			DWORD dwTest = (0x00000080 << 8 * (uSize - 1));

			DWORD dwFill = (0xFFFFFF00 << 8 * (uSize - 1));

			dwData       = 0;

			while( uSize-- ) {

				dwData <<= 8;

				dwData  += *BuffStripHead(m_pRxBuff, BYTE);
				}

			if( dwFill && (dwData & dwTest) ) {

				dwData |= dwFill;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::StripCtxString(BYTE bTag, PTXT pData)
{
	UINT uSize;

	if( StripCtx(bTag, uSize) ) {

		if( uSize-- ) {

			BYTE bCode = *BuffStripHead(m_pRxBuff, BYTE);

			PTXT pFrom = PTXT(m_pRxBuff->StripHead(uSize));

			if( bCode == 0 ) {

				memcpy(pData, pFrom, uSize);

				pData[uSize] = 0;

				return TRUE;
				}

			return FALSE;
			}
		else {
			*pData = 0;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::StripCtxAppData(BYTE bTag, DWORD &Data, BOOL &fNull)
{
	UINT uSize;

	if( StripCtx(bTag, uSize) ) {

		if( uSize == 6 ) {

			PCBYTE pData = m_pRxBuff->GetData();

			PCBYTE pInit = pData;

			if( ExtractValue(pData, Data, fNull) ) {

				m_pRxBuff->StripHead(pData - pInit);

				if( StripCtx(bTag, uSize) ) {

					if( uSize == 7 ) {

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::StripCtxOpen(BYTE bTag)
{
	if( m_pRxBuff->GetSize() ) {

		BYTE bCode = *m_pRxBuff->GetData();

		if( bCode == ((bTag << 4) | 0x0E) ) {

			m_pRxBuff->StripHead(1);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::StripCtxClose(BYTE bTag)
{
	if( m_pRxBuff->GetSize() ) {

		BYTE bCode = *m_pRxBuff->GetData();

		if( bCode == ((bTag << 4) | 0x0F) ) {

			m_pRxBuff->StripHead(1);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::StripCtxUnknown(void)
{
	if( !m_pRxBuff->GetSize() ) {

		return FALSE;
		}

	BYTE bCode = *m_pRxBuff->GetData();

	BYTE bTag1 = (bCode >> 4);

	if( bCode & 0x08 ) {

		UINT uSize = (bCode & 0x07);

		if( uSize <= 4 ) {

			m_pRxBuff->StripHead(1 + uSize);

			return TRUE;
			}

		if( uSize == 5 ) {

			m_pRxBuff->StripHead(1);

			StripSize(uSize);

			m_pRxBuff->StripHead(uSize);

			return TRUE;
			}

		if( uSize == 6 ) {

			m_pRxBuff->StripHead(1);

			while( m_pRxBuff->GetSize() ) {

				BYTE bCode = *m_pRxBuff->GetData();

				BYTE bTag2 = (bCode >> 4);

				if( bCode & 0x08 ) {

					UINT uSize = (bCode & 0x07);

					if( uSize == 7 ) {

						if( bTag1 == bTag2 ) {

							m_pRxBuff->StripHead(1);

							break;
							}

						Show("strip", "mismatched block");

						return FALSE;
						}
					}
				else {
					if( !StripAppUnknown() ) {

						return FALSE;
						}
					}

				StripCtxUnknown();
				}

			return TRUE;
			}

		Show("strip", "unknown type");

		return FALSE;
		}
	else {
		if( !StripAppUnknown() ) {

			return FALSE;
			}

		return TRUE;
		}
	}

// Application Tag Parsing

BOOL CBACNetSlave::StripAppUnknown(void)
{
	if( !m_pRxBuff->GetSize() ) {

		return FALSE;
		}

	BYTE bCode = *m_pRxBuff->GetData();

	BYTE bType = (bCode >> 4);

	UINT uSize = (bCode  & 7);

	if( bType == 1 ) {

		m_pRxBuff->StripHead(1);

		return TRUE;
		}

	if( uSize <= 4 ) {

		m_pRxBuff->StripHead(1 + uSize);

		return TRUE;
		}

	if( uSize == 5 ) {

		m_pRxBuff->StripHead(1);

		StripSize(uSize);

		m_pRxBuff->StripHead(uSize);

		return TRUE;
		}

	Show("strip", "unknown type");

	return FALSE;
	}

// Value Extraction

BOOL CBACNetSlave::ExtractValue(PCBYTE &pData, DWORD &Data, BOOL &fNull)
{
	BYTE bType = (*pData >> 4);

	BYTE bSize = (*pData & 15);

	pData++;

	if( bType == 0 ) {

		// Null

		Data  = 0;

		fNull = TRUE;

		return TRUE;
		}

	if( bType == 1 ) {

		// Boolean

		Data  = bSize ? 1 : 0;

		fNull = FALSE;

		return TRUE;
		}

	if( bType == 2 || bType == 9 ) {

		// Unsigned Integer or Enumeration
			
		Data  = 0;

		fNull = FALSE;

		while( bSize-- ) {

			Data <<= 8;

			Data  += *pData++;
			}

		return TRUE;
		}

	if( bType == 3 ) {

		// Signed Integer

		Data  = 0;

		fNull = FALSE;

		if( bSize ) {

			DWORD Test = (0x00000080 << 8 * (bSize - 1));

			DWORD Fill = (0xFFFFFF00 << 8 * (bSize - 1));

			while( bSize-- ) {

				Data <<= 8;

				Data  += *pData++;
				}

			if( Fill && (Data & Test) ) {

				Data |= Fill;
				}
			}

		return TRUE;
		}

	if( bType == 4 ) {

		// Real Number

		if( bSize == 4 ) {

			Data   = MotorToHost(PDWORD(pData)[0]);

			fNull  = FALSE;

			pData += bSize;

			return TRUE;
			}

		Show("extract", "bad real");

		return FALSE;
		}

	Show("extract", "bad type");

	return FALSE;
	}

// Object Types

BOOL CBACNetSlave::IsAnalog(UINT Type)
{
	switch( Type ) {

		case typeAnalogInput:
		case typeAnalogOutput:
		case typeAnalogValue:

			return TRUE;
		}

	return FALSE;
	}

BOOL CBACNetSlave::IsBinary(UINT Type)
{
	switch( Type ) {

		case typeBinaryInput:
		case typeBinaryOutput:
		case typeBinaryValue:

			return TRUE;
		}

	return FALSE;
	}

BOOL CBACNetSlave::IsMultiState(UINT Type)
{
	switch( Type ) {

		case typeMultiStateInput:
		case typeMultiStateOutput:
		case typeMultiStateValue:

			return TRUE;
		}

	return FALSE;
	}

BOOL CBACNetSlave::IsCommandable(UINT Type)
{
	switch( Type ) {

		case typeAnalogOutput:
		case typeAnalogValue:
		case typeBinaryOutput:
		case typeBinaryValue:
		case typeMultiStateOutput:
		case typeMultiStateValue:

			return TRUE;
		}

	return FALSE;
	}

// Command Lookup

CBACNetSlave::CCmd * CBACNetSlave::FindCommand(UINT Type, UINT Inst)
{
	DWORD Object = ((Type << 22) | Inst);

	for( UINT n = 0; n < m_pBase->m_uCount; n++ ) {

		if( m_pBase->m_pList[n].m_Object == Object ) {

			return &m_pBase->m_pList[n].m_Cmd;
			}
		}

	return NULL;
	}

// Data Access

BOOL CBACNetSlave::HasValue(UINT Type, UINT Inst)
{
	return HasValue(Type, Inst, NULL);
	}

BOOL CBACNetSlave::HasValue(UINT Type, UINT Inst, IMetaData **ppm)
{
	DWORD &Data = PDWORD(0)[0];

	UINT  Local;

	return GetValue(Type, Inst, Data, Local, ppm);
	}

BOOL CBACNetSlave::GetValue(UINT Type, UINT Inst, DWORD &Data, UINT &Local)
{
	return GetValue(Type, Inst, Data, Local, NULL);
	}

BOOL CBACNetSlave::GetValue(UINT Type, UINT Inst, DWORD &Data, UINT &Local, IMetaData **ppm)
{
	CAddress Addr;

	Addr.a.m_Table  = 100 + Type;

	Addr.a.m_Extra  = 0;

	Addr.a.m_Offset = Inst;

	if( Type >= 3 ) {

		UINT uType[] = { addrBitAsBit, addrBitAsByte, addrBitAsWord, addrBitAsLong };

		UINT uMask[] = { 0x1, 0xFF, 0xFFFF, 0xFFFFFFFF };

		for( UINT t = 0; t < elements(uType); t++ ) {

			Addr.a.m_Type = uType[t];

			if( COMMS_SUCCESS(Read(Addr, &Data, ppm)) ) {

				Local  = Addr.a.m_Type;

				if( Local == addrBitAsBit ) {

					// Ensure existing behavior remains.

					Data  = Data ? 1 : 0;
					}
				else {
					Data &= uMask[t];
					}

				return TRUE;
				}
			}

		return FALSE;
		}
	else {
		Addr.a.m_Type = addrRealAsReal;

		if( COMMS_SUCCESS(Read(Addr, &Data, ppm)) ) {

			Local = Addr.a.m_Type;

			return TRUE;
			}

		Addr.a.m_Type = addrLongAsLong;
		}

	if( COMMS_SUCCESS(Read(Addr, &Data, ppm)) ) {

		Local = Addr.a.m_Type;

		return TRUE;
		}

	return FALSE;
	}

BOOL CBACNetSlave::SetValue(UINT Type, UINT Inst, DWORD Data)
{
	CAddress Addr;

	Addr.a.m_Table  = 100 + Type;

	Addr.a.m_Extra  = 0;

	Addr.a.m_Offset = Inst;

	if( Type >= 3 ) {

		UINT uType[] = { addrBitAsBit, addrBitAsByte, addrBitAsWord, addrBitAsLong };

		for( UINT t = 0; t < elements(uType); t++ ) {

			Addr.a.m_Type = uType[t];

			if( COMMS_SUCCESS(Write(Addr, &Data, 1)) ) {

				return TRUE;
				}
			}

		return FALSE;
		}
	else {
		Addr.a.m_Type = addrRealAsReal;

		if( COMMS_SUCCESS(Write(Addr, &Data, 1)) ) {

			return TRUE;
			}

		Addr.a.m_Type = addrLongAsLong;

		Data          = RealToLong(Data);
		}

	if( COMMS_SUCCESS(Write(Addr, &Data, 1)) ) {

		return TRUE;
		}

	return FALSE;
	}

void CBACNetSlave::BuildObjectList(void)
{
	for( UINT uPass = 0; uPass < 2; uPass++ ) {

		UINT uScan = 1;

		for( UINT Scan = 0; Scan < 9; Scan++ ) {

			UINT Type = NOTHING;

			switch( Scan ) {

				case 0: Type = typeAnalogInput;		break;
				case 1: Type = typeAnalogOutput;	break;
				case 2: Type = typeAnalogValue;		break;
				case 3: Type = typeBinaryInput;		break;
				case 4: Type = typeBinaryOutput;	break;
				case 5: Type = typeBinaryValue;		break;
				case 6: Type = typeMultiStateInput;	break;
				case 7: Type = typeMultiStateOutput;	break;
				case 8: Type = typeMultiStateValue;	break;
				}

			for( UINT Inst = 1; Inst < m_pBase->m_uLimit; Inst++ ) {

				IMetaData *pm = NULL;

				if( HasValue(Type, Inst, uPass ? &pm : NULL) ) {
					
					if( uPass == 0 ) {

						uScan++;
						}
					else {
						CEntry & Entry     = m_pBase->m_pList[uScan++];

						Entry.m_Object     = (Type << 22) | Inst;

						Entry.m_Cmd.m_Mask = 0;

						EncodeObjectName(Entry.m_Name, Type, Inst, pm);
						}

					if( pm ) {

						pm->Release();
						}
					}
				}
			}

		if( uPass == 0 ) {
			
			m_pBase->m_uCount = uScan;

			m_pBase->m_pList  = New CEntry [ uScan ];

			CEntry & Device   = m_pBase->m_pList[0];

			Device.m_Object   = m_pBase->m_Device | 0x02000000;

			EncodeDeviceName(Device.m_Name);
			}

		if( uPass == 1 ) {

			qsort( m_pBase->m_pList,
			       m_pBase->m_uCount,
			       sizeof(CEntry),
			       ListSort
			       );
			}

		Sleep(10);
		}
	}

void CBACNetSlave::FreeObjectList(void)
{
	if( m_pBase ) {

		delete [] m_pBase->m_pList;

		m_pBase->m_fIam = FALSE;

		m_pBase->m_fEnable = FALSE;

		m_pBase->m_dwTime = GetTimeStamp() + 5;
		}
	}

// Name Handling

BOOL CBACNetSlave::DecodeName(PTXT pName, DWORD &Object)
{
	NAME Name;

	EncodeDeviceName(Name);

	if( !strnicmp(pName, Name, strlen(Name)) ) {

		CEntry const *pFind = (CEntry const *) bsearch( pName,
								m_pBase->m_pList,
								m_pBase->m_uCount,
								sizeof(CEntry),
								ListSort
								);

		if( pFind ) {

			Object = pFind->m_Object;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBACNetSlave::EncodeObjectName(PTXT pName, UINT Type, UINT Inst)
{
	return EncodeObjectName(pName, Type, Inst, NULL);
	}

BOOL CBACNetSlave::EncodeObjectName(PTXT pName, UINT Type, UINT Inst, IMetaData *pm)
{
	EncodeDeviceName(pName);

	if( Type != typeDevice ) {

		EncodeDeviceName(pName);

		strcat(pName, ".");

		pName += strlen(pName);

		if( m_pBase->m_Name ) {

			UINT Props[] = { tpName,
					 tpLabel,
					 tpDescription
					 };

			if( GetMetaString(pm, Props[m_pBase->m_Name - 1], pName, sizeof(NAME)) ) {

				if( *pName ) {

					return TRUE;
					}
				}
			}

		switch( Type ) {

			case typeAnalogInput:      SPrintf(pName, "AI%u", Inst); return FALSE;
			case typeAnalogOutput:     SPrintf(pName, "AO%u", Inst); return FALSE;
			case typeAnalogValue:      SPrintf(pName, "AV%u", Inst); return FALSE;
			case typeBinaryInput:      SPrintf(pName, "BI%u", Inst); return FALSE;
			case typeBinaryOutput:     SPrintf(pName, "BO%u", Inst); return FALSE;
			case typeBinaryValue:	   SPrintf(pName, "BV%u", Inst); return FALSE;
			case typeMultiStateInput:  SPrintf(pName, "MI%u", Inst); return FALSE;
			case typeMultiStateOutput: SPrintf(pName, "MO%u", Inst); return FALSE;
			case typeMultiStateValue:  SPrintf(pName, "MV%u", Inst); return FALSE;
			}
		
		SPrintf(pName, "T%u.%u", Type, Inst);

		return FALSE;
		}

	return TRUE;
	}

void CBACNetSlave::EncodeDeviceName(PTXT pName)
{
	if( m_pBase->m_DevName[0] ) {

		strcpy(pName, m_pBase->m_DevName);

		return;
		}

	SPrintf(pName, "G3.%u", m_pBase->m_Device);
	}

// Conversions

DWORD CBACNetSlave::LongToReal(DWORD Data)
{
	float Real = float(LONG(Data));

	return R2I(Real);
	}

DWORD CBACNetSlave::RealToLong(DWORD Data)
{
	float Real = I2R(Data);

	return LONG(Real);
	}

// Debugging

void CBACNetSlave::FindTrace(void)
{
	MoreHelp(IDH_TRACE, (void **) &m_pTrace);
	}

BOOL CBACNetSlave::Trace(PCTXT pText, ...)
{
	#if TRACE > 0

	if( m_pTrace ) {

		va_list pArgs;

		va_start(pArgs, pText);

		m_pTrace->AfxTrace(pText, pArgs);

		va_end(pArgs);
		
		return TRUE;
		}

	#endif

	return FALSE;
	}

void CBACNetSlave::Debug(PCTXT pName, ...)
{
	#if TRACE > 0

	va_list pArgs;

	va_start(pArgs, pName);

	m_pHelper->AfxTrace(pName, pArgs);

	va_end(pArgs);

	#endif
	}

void CBACNetSlave::Dump(PCTXT pName, CBuffer *pBuff)
{
	#if TRACE > 1

	UINT  uSize = pBuff->GetSize();

	PBYTE pRecv = pBuff->GetData();

	Debug("%4.4X : %6u : %-10.10s : ", m_Ident, m_pBase->m_Device, pName);

	for( UINT n = 0; n < uSize; n++ ) {

		Debug("%2.2X ", pRecv[n]);
		}

	Debug("\n");

	#endif
	}

void CBACNetSlave::Show(PCTXT pName, PCTXT pError)
{
	#if TRACE > 0

	Debug("%4.4X : %6u : %-10.10s : %s", m_Ident, m_pBase->m_Device, pName, pError);

	Debug("\n");

	#endif
	}

void CBACNetSlave::ShowRequest(PCTXT pName)
{
	#if TRACE > 0

	Debug("%4.4X : ", m_RxNet.m_NET);

	if( m_RxNet.m_LEN ) {

		for( UINT n = 0; n < m_RxNet.m_LEN; n++ ) {

			Debug("%2.2X ", m_RxNet.m_MAC[n]);
			}

		Debug(": ");
		}

	Debug("%s\n", pName);

	#endif
	}

// Sort Function

int CBACNetSlave::ListSort(PCVOID p1, PCVOID p2)
{
	CEntry const *e1 = (CEntry const *) p1;

	CEntry const *e2 = (CEntry const *) p2;

	return stricmp(e1->m_Name, e2->m_Name);
	}

// Buffer Sizes

UINT CBACNetSlave::FindMaxBufferSize(void)
{
	switch( m_bMaximum & 0xF ) {

		case 0:	return 50;
		case 1:	return 128;
		case 2: return 206;
		case 3: return 480;
		case 4: return 1024;
		case 5: return 1476;
		}
	
	return 600;
	}

// End of File
