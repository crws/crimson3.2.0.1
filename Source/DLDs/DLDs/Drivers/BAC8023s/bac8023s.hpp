
//////////////////////////////////////////////////////////////////////////
//
// Base Class
//

#include "bacslave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet IEEE 802.3 Slave
//

class CBACNet8023Slave : public CBACNetSlave
{
	public:
		// Constructor
		CBACNet8023Slave(void);

		// Destructor
		~CBACNet8023Slave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);


	protected:
		// MAC Address
		struct CMacAddr
		{
			BYTE m_MAC[6];
			};

		// Device Context
		struct CContext : CBACNetSlave::CBaseCtx
		{
			};

		// Data Members
		CContext * m_pCtx;
		ISocket  * m_pSock;
		CMacAddr   m_RxMac;

		// Transport Hooks
		BOOL SendFrame(BOOL fThis, BOOL fReply);
		BOOL RecvFrame(void);

		// Transport Header
		void AddTransportHeader(BOOL fThis);
		BOOL StripTransportHeader(void);
	};

// End of File
