//////////////////////////////////////////////////////////////////////////
//
// BACNet Slave Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// BACNet Vendor ID
//

#define	VENDOR_ID 233

//////////////////////////////////////////////////////////////////////////
//
// BACNet Abort, Reject and Error Codes
//

enum
{
	abortOther        = 0,
	abortSegmentation = 4,
	};

enum
{
	rejectOther          = 0,
	rejectInvalidTag     = 4,
	rejectUnknownService = 9,
	};

enum
{
	classDevice   = 0,
	classObject   = 1,
	classProperty = 2,
	classSecurity = 4,
	classServices = 5,
	classAbort    = 250,
	classReject   = 251,
	};

enum
{			     
	errorGeneral	     =  0,
	errorAuthentication  =  1,
	errorServiceDenied   = 29,
	errorUnknownObject   = 31,
	errorUnknownProperty = 32,
	errorWriteDenied     = 40,
	errorInvalidIndex    = 42,
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet Object Type Code
//

enum
{
	typeAnalogInput		= 0,
	typeAnalogOutput	= 1,
	typeAnalogValue		= 2,
	typeBinaryInput		= 3,
	typeBinaryOutput	= 4,
	typeBinaryValue		= 5,
	typeDevice		= 8,
	typeMultiStateInput	= 13,
	typeMultiStateOutput	= 14,
	typeMultiStateValue	= 19,
	};

//////////////////////////////////////////////////////////////////////////
//
// BACNet Property Identifiers
//

enum
{
	propActiveText				=  4,
	propAll					=  8,
	propAPDUTimeout				= 11,
	propApplicationSoftwareVersion		= 12,
	propDescription				= 28,
	propDeviceAddressBinding		= 30,
	propEventState				= 36,
	propFirmwareRevision			= 44,
	propHighLimit				= 45,
	propInactiveText			= 46,
	propLocalDate				= 56,
	propLocalTime				= 57,
	propLowLimit				= 59,
	propMaxAPDULengthAccepted		= 62,
	propModelName				= 70,
	propNumberOfAPDURetries			= 73,
	propNumberOfStates			= 74,
	propObjectIdentifer			= 75,
	propObjectList				= 76,
	propObjectName				= 77,
	propObjectType				= 79,
	propOptional				= 80,
	propOutOfService			= 81,
	propPolarity				= 84,
	propPresentValue			= 85,
	propPriorityArray			= 87,
	propProtocolObjectTypesSupported	= 96,
	propProtocolServicesSupported		= 97,
	propProtocolVersion			= 98,
	propRelinquishDefault			= 104,
	propRequired				= 105,
	propSegmentationSupport			= 107,
	propStateText				= 110,
	propStatusFlags				= 111,
	propSystemStatus			= 112,
	propUnits				= 117,
	propVendorIdentifier			= 120,
	propVendorName				= 121,
	propProtocolRevision			= 139,
	propDatabaseRevision			= 155,
	propMaxSegmentsAccepted			= 167
	};

//////////////////////////////////////////////////////////////////////////
//
// Useful Macros
//

#define	I2R(x)	(*((float *) &(x)))

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// BACNet Slave Driver
//

class CBACNetSlave : public CSlaveDriver
{
	public:
		// Constructor
		CBACNetSlave(void);

		// Destructor
		~CBACNetSlave(void);

		// Management
		DEFMETH(void) Open(void);

		// Entry Points
		DEFMETH(void) Service(void);

	public:
		// Type Definitions
		typedef char NAME[64];

		// Network Address
		struct CNetAddr
		{
			WORD	m_NET;
			BYTE	m_LEN;
			BYTE	m_MAC[8];
			};

		// Command Data
		struct CCmd
		{
			WORD	m_Mask;
			DWORD	m_Data[16];
			};

		// List Entry
		struct CEntry
		{
			NAME  m_Name;
			DWORD m_Object;
			CCmd  m_Cmd;
			};

		// Device Context
		struct CBaseCtx
		{
			DWORD	 m_Device;
			NAME	 m_DevName;
			UINT	 m_uLimit;
			UINT	 m_Name;
			UINT	 m_Desc;
			UINT	 m_Rich;
			UINT	 m_uCount;
			CEntry * m_pList;
			NAME	 m_sPass;
			BOOL	 m_fEnable;
			DWORD	 m_dwTime;
			UINT	 m_uPort;
			BOOL	 m_fIam;
			DWORD	 m_dwAct;
			BOOL	 m_fRemote;
			};

	protected:
		// Data Members
		CBaseCtx     * m_pBase;
		ITraceHelper * m_pTrace;
		CBuffer      * m_pTxBuff;
		PBYTE          m_pTxData;
		UINT           m_uTxPtr;
		UINT	       m_uTxSave;
		CBuffer      * m_pRxBuff;
		CNetAddr       m_RxNet;
		BYTE	       m_bMaximum;
		BYTE	       m_bInvoke;
		BYTE	       m_bService;
		BYTE	       m_bClass;
		BYTE	       m_bError;
		char	       m_sText[128];

		// Transport Hooks
		virtual BOOL SendFrame(BOOL fThis, BOOL fReply) = 0;
		virtual BOOL RecvFrame(void)                    = 0;

		// Request Handling
		BOOL Process(void);
		BOOL OnUnconfirmedRequest(void);
		BOOL OnWhoHasRequest(void);
		BOOL OnWhoIsRequest(void);
		BOOL OnConfirmedRequest(void);
		BOOL OnReinitializeDevice(void);
		BOOL OnDeviceCommunicationControl(void);
		BOOL OnWritePropertyRequest(void);
		BOOL OnReadPropertyRequest(void);
		BOOL OnReadPropertyMultipleRequest(void);

		// Request Helpers
		BOOL OnReadAll(UINT Type, UINT Inst, BYTE Prop);
		BOOL OnReadAll(UINT Type, UINT Inst, BYTE Prop, PCBYTE pList, UINT uCount);
		BOOL OnReadProperty(BYTE bTag, UINT Type, UINT Inst, DWORD Prop, DWORD Index);
		BOOL OnReadStdProperty(UINT Type, UINT Inst, BYTE Prop);
		BOOL OnReadDeviceProperty(BYTE Prop, UINT Index);
		BOOL OnReadObjectProperty(UINT Type, UINT Inst, BYTE Prop, UINT Index, IMetaData *pm);
		BOOL OnReadObjectPresentValue(UINT Type, UINT Inst);
		BOOL OnReadObjectDataValue(DWORD Data, UINT Local);
		BOOL OnSendError(BYTE bClass, BYTE bError);
		BOOL OnSendReject(BYTE bReject);
		BOOL OnSendAbort(BYTE bReject);
		BOOL OnSendSimpleAck(void);
		BOOL OnSendIAm(void);

		// Configuration
		BOOL LoadConfig(PCBYTE &pData);

		// Basic Frame Building
		BOOL InitFrame(void);
		BOOL KillFrame(void);
		void SaveFrame(void);
		void LoadFrame(void);
		void AddRawData(PCTXT pText);
		void AddRawData(PBYTE pData, UINT uSize);
		void AddRawByte(BYTE bData);
		void AddRawWord(WORD wData);
		void AddRawTrip(DWORD dwData);
		void AddRawLong(DWORD dwData);
		void AddDone(void);
		void AddNetworkHeader(BOOL fThis, BOOL fReply);

		// Context Tag Building
		void AddCtxTag(BYTE bTag, BYTE bSize);
		void AddCtxByte(BYTE bTag, BYTE bData);
		void AddCtxWord(BYTE bTag, WORD wData);
		void AddCtxTrip(BYTE bTag, DWORD dwData);
		void AddCtxLong(BYTE bTag, DWORD dwData);
		void AddCtxUnsigned(BYTE bTag, DWORD dwData);
		void AddCtxSigned(BYTE bTag, DWORD dwData);
		void AddCtxOpen(BYTE bTag);
		void AddCtxClose(BYTE bTag);

		// Application Tag Building
		void AddAppTag(BYTE bType, BYTE bSize);
		void AddAppNull(void);
		void AddAppBoolean(BOOL fData);
		void AddAppUnsigned(BYTE bType, DWORD dwData);
		void AddAppSigned(BYTE bType, DWORD dwData);
		void AddAppUnsigned(DWORD dwData);
		void AddAppSigned(DWORD dwData);
		void AddAppReal(DWORD dwData);
		void AddAppBitString(UINT uBits, ...);
		void AddAppEnum(DWORD dwData);
		void AddAppObjectID(DWORD Object);
		void AddAppString(PCTXT pData);
		void AddAppDate(DWORD Date);
		void AddAppTime(DWORD Time);

		// Basic Frame Parsing
		BOOL StripNetworkHeader(void);
		void StripSize(UINT &uSize);
		void RecvDone(void);

		// Context Tag Parsing
		BOOL StripCtx(BYTE bTag, UINT &uSize);
		BOOL StripCtxUnsigned(BYTE bTag, DWORD &dwData);
		BOOL StripCtxSigned(BYTE bTag, DWORD &dwData);
		BOOL StripCtxString(BYTE bTag, PTXT pData);
		BOOL StripCtxAppData(BYTE bTag, DWORD &Data, BOOL &fNull);
		BOOL StripCtxOpen(BYTE bTag);
		BOOL StripCtxClose(BYTE bTag);
		BOOL StripCtxUnknown(void);

		// Application Tag Parsing
		BOOL StripAppUnknown(void);

		// Value Extraction
		BOOL ExtractValue(PCBYTE &pData, DWORD &Data, BOOL &fNull);

		// Object Types
		BOOL IsAnalog(UINT Type);
		BOOL IsBinary(UINT Type);
		BOOL IsMultiState(UINT Type);
		BOOL IsCommandable(UINT Type);

		// Command Lookup
		CCmd * FindCommand(UINT Type, UINT Inst);

		// Data Access
		BOOL HasValue(UINT Type, UINT Inst);
		BOOL HasValue(UINT Type, UINT Inst, IMetaData **ppm);
		BOOL GetValue(UINT Type, UINT Inst, DWORD &Data, UINT &Local);
		BOOL GetValue(UINT Type, UINT Inst, DWORD &Data, UINT &Local, IMetaData **ppm);
		BOOL SetValue(UINT Type, UINT Inst, DWORD Data);
		void BuildObjectList(void);
		void FreeObjectList(void);

		// Name Handling
		BOOL DecodeName(PTXT pName, DWORD &Object);
		BOOL EncodeObjectName(PTXT pName, UINT Type, UINT Inst);
		BOOL EncodeObjectName(PTXT pName, UINT Type, UINT Inst, IMetaData *pm);
		void EncodeDeviceName(PTXT pName);

		// Conversions
		DWORD LongToReal(DWORD Data);
		DWORD RealToLong(DWORD Data);

		// Debugging
		void FindTrace(void);
		BOOL Trace(PCTXT pText, ...);
		void Debug(PCTXT pText, ...);
		void Dump(PCTXT pName, CBuffer *pBuff);
		void Show(PCTXT pName, PCTXT pError);
		void ShowRequest(PCTXT pName);

		// Sort Function
		static int ListSort(PCVOID p1, PCVOID p2);

		// Buffer Sizes
		UINT FindMaxBufferSize(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Time Support
//

extern DWORD Time(UINT h, UINT m, UINT s);
extern DWORD Date(UINT y, UINT m, UINT d);
extern void  GetWholeDate(DWORD t, UINT *p);
extern void  GetWholeTime(DWORD t, UINT *p);
extern UINT  GetYear(DWORD t);
extern UINT  GetMonth(DWORD t);
extern UINT  GetDate(DWORD t);
extern UINT  GetDays(DWORD t);
extern UINT  GetWeeks(DWORD t);
extern UINT  GetDay(DWORD t);
extern UINT  GetWeek(DWORD t);
extern UINT  GetWeekYear(DWORD t);
extern UINT  GetHours(DWORD t);
extern UINT  GetHour(DWORD t);
extern UINT  GetMin(DWORD t);
extern UINT  GetSec(DWORD t);
extern UINT  GetMonthDays(UINT y, UINT m);

// End of File
