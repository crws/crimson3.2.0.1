
//////////////////////////////////////////////////////////////////////////
//
// CANOpen PDO Slave Driver
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CIAPDO_HPP
	
#define	INCLUDE_CIAPDO_HPP

//////////////////////////////////////////////////////////////////////////
//
// CAN Frame
//

struct FRAME
{
	BYTE	bCount;
	BYTE	bZero;
	WORD	wID;
	BYTE	bData[8];
	};

//////////////////////////////////////////////////////////////////////////
//
// PDO
//

struct PDO
{
	DWORD	 dwCobID;
	BYTE 	 bType;
	UINT	 uInhibit;
	UINT	 uEvent;
	DWORD	 dwMap[64];
	BYTE	 bData[8];
	UINT     uTimer;
	UINT     uOffset;
	BOOL     fDirty;
	BOOL	 fManual;
	CAddress Addr;
	};

/////////////////////////////////////////////////////////////////////////
//
// Other Enumerations
//

enum Transport 
{
	transHandler = 0,
	transDriver  = 1,
	transTimer   = 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Device Profiles
//

enum 
{
	profileNone    = 0,
	profile401     = 1,
	};

//////////////////////////////////////////////////////////////////////////////////////////
//
// CANOpen PDO Slave Constants
//

#define TPDO		1
#define RPDO		2

#define PRM_RPDO	0x14
#define PRM_TPDO	0x18
#define MAP_RPDO	0x16
#define MAP_TPDO	0x1A

#define MAX_PDO		128

#define NMTS_BOOT	0
#define NMTS_STOP	4
#define NMTS_OP		5
#define NMTS_PRE_OP	127

#define NMT_COB_ID	1792
#define NMT_START	1
#define NMT_STOP	2
#define NMT_PRE_OP	128
#define NMT_RESET	129
#define NMT_RESET_COMM	130

//////////////////////////////////////////////////////////////////////////
//
// CAN Port PDO Handler
//

class CCANPortPDOHandler : public IPortHandler
{	
	public:
		// Constructor
		CCANPortPDOHandler(IHelper *pHelper);

		// Destructor
		~CCANPortPDOHandler(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);
				
		// PDO Access
		void AllocObjects(BOOL fProducer, UINT uCount);
		void InitPDORecord(PDO * pPDO, UINT uPDO, DWORD dwMap, UINT uSize);
		void MapPDO(PDO &pPDO);
		void LoadPDOs(void);
		PDO* FindPDORecord(DWORD dwID, UINT uPDO);
		PDO* FindNextPDO(UINT uPDO);
		PDO* GetRPDO(UINT uIndex);

		// Member Access
		void SetOp(BOOL fOp);
		void SetDrop(BYTE bDrop);
		void SetProfile(BYTE bProfile);
		void SetGuard(BOOL fGuard);
		void SetDecode(BYTE m_bDecode);
		UINT GetState(void);
		BOOL IsTimedOut(UINT uTime, UINT uSpan);
		BOOL IsPrn(PDO * PDO);
		UINT GetTimerTick(void);	
	       				
	protected:
		// Data
		ULONG		m_uRefs;
		BOOL		m_fOp;
		BYTE		m_bDrop;
		BYTE		m_bProfile;
		BOOL		m_fGuard;
		BYTE		m_bDecode;

		PDO *		m_pRPDO;
		PDO *		m_pTPDO;
		UINT		m_uRPDO;
		UINT		m_uTPDO;
								
		UINT		m_uLast;
		UINT		m_uGuardTime;
		UINT		m_uLifeTime;
		UINT		m_uHeartBeat;
		UINT		m_uState;
		 		
		IPortObject   * m_pPort;
		UINT		m_uTxCount;
		UINT		m_uRxByte;
		FRAME		m_Rx;
		FRAME		m_Tx[3];
		PBYTE		m_pRxData;
		PCBYTE		m_pTxData;
		FRAME		m_Send[64];
		UINT		m_uQueue;
		UINT		m_uSend;
		IHelper       * m_pHelper;
		IExtraHelper  * m_pExtraHelper;
		BOOL		m_fTxEnable;
		
		// Implementation
		void Start(UINT uTrans = transHandler);
		BOOL PutFrame(UINT uTrans = transHandler);
		void SendFrame(void);
		void HandleFrame(void);
		BOOL HandleBootup(void);
		BOOL HandleSync(void);
		BOOL HandleNMT(void);
		BOOL HandleNodeGuard(void);
	virtual	BOOL HandleRPDO(void);
		BOOL HandleHeartBeat(void);
		BOOL HandleSDO(void);
		BOOL HandleRead(void);
		BOOL HandleWrite(void);
		BOOL HandleEMCY(void);
		BOOL HandleNMTError(void);
	virtual	BOOL IsSync(void);
		BOOL IsNodeGuard(void);
	virtual BOOL IsNMT(void);
	virtual	BOOL IsRPDO(void);
		BOOL IsHeartBeat(void);
		BOOL IsSDO(void);
	virtual	BOOL DoListen(void);
		BOOL IsDrop(BYTE bDrop, BOOL fWrite);

		// PDO Implementation
		void InitObject(PDO * pPDO, UINT uOffset);
		PDO* FindRPDORecord(DWORD dwID);
		PDO* FindTPDORecord(DWORD dwID);
		PDO* FindNextRPDO(void);
		PDO* FindNextTPDO(void);
		void HandleParameter(UINT uPDO);
		void HandleMapping(UINT uPDO);
		
		// Helpers
		DWORD GetRxData(void);
		UINT  GetSpec(WORD wIndex, BYTE bSub, DWORD &Data);
		BOOL  PutSpec(WORD wIndex, BYTE bSub, UINT uSize, DWORD Data);
		void  SaveData(WORD wIndex, DWORD Data);
		DWORD GetDeviceProfile(void);
		void  SetState(UINT uState);
		void  Increment(UINT &uIndex);
		BOOL  IsPrnDirty(PDO * pPDO);
		BOOL  CheckUpdate(PDO * pPDO);
	};

//////////////////////////////////////////////////////////////////////////
//
// CANOpen PDO Slave Driver
//

class CCANOpenPDOSlave : public CMasterDriver
{
	public:
		// Constructor
		CCANOpenPDOSlave(void);

		// Destructor
		~CCANOpenPDOSlave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
						
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);
			
	protected:
		// Handler
		CCANPortPDOHandler * m_pHandler;
		
		// Other data members
		BYTE m_bDrop;
		BYTE m_bProfile;
		BYTE m_bOp;
		BOOL m_fGuard;
		BYTE m_bDecode;
		PDO *m_pPDO;
		UINT m_uPDO;
	};

// End of file

#endif
