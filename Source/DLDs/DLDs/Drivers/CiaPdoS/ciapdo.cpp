
#include "intern.hpp"

#include "ciapdo.hpp" 

//////////////////////////////////////////////////////////////////////////
//
// CANOpen PDO Slave Driver
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CCANOpenPDOSlave::CCANOpenPDOSlave(void)
{
	m_Ident    = DRIVER_ID;

	m_bDrop    = 8;

	m_bDecode  = 0;

	m_fGuard   = FALSE;

	m_bProfile = 0;

	m_bOp	   = 0;

	m_pPDO     = NULL;
	}

// Destructor

CCANOpenPDOSlave::~CCANOpenPDOSlave(void)
{
	delete this;
	}

// Configuration

void MCALL CCANOpenPDOSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bDrop    = GetByte(pData);

		m_bDecode  = GetByte(pData);

		m_fGuard   = GetByte(pData) ? TRUE : FALSE;

		m_bProfile = GetByte(pData);

		m_bOp	   = GetByte(pData);

		UINT uPDO = GetWord(pData);

		if( uPDO > 0 ) {

			m_pPDO = new PDO[uPDO];

			for( m_uPDO = 0; m_uPDO < uPDO; m_uPDO++ ) {

				BOOL fType = GetByte(pData);

				UINT uNum  = GetWord(pData);

				uNum = 2 * (uNum - 1) + (fType ? 3 : 4);
												
				m_pPDO[m_uPDO].dwCobID = (uNum << 7) | m_bDrop;

				UINT uElements = GetWord(pData);

				for( UINT u = 0; u < uElements; u++ ) {

					m_pPDO[m_uPDO].dwMap[u] =  GetWord(pData) << 16;

					m_pPDO[m_uPDO].dwMap[u] |= GetByte(pData) << 8;

					UINT uSize = GetByte(pData);

					switch( uSize ) {

						case 1:
							uSize = 0x08;
							break;
						case 2:
							uSize = 0x10;
							break;
						case 4:
							uSize = 0x20;
							break;
						}

					m_pPDO[m_uPDO].dwMap[u] |= uSize;

					memset(m_pPDO[m_uPDO].bData, 0, 8);
					}

				m_pPDO[m_uPDO].fManual = GetByte(pData) ? TRUE : FALSE;

				m_pPDO[m_uPDO].uEvent  = GetWord(pData);
				}
			}
		}
	}
	
void MCALL CCANOpenPDOSlave::CheckConfig(CSerialConfig &Config)
{
	Config.m_bDrop = m_bDrop;

	Config.m_uFlags |= flagPrivate;
	}
	
// Management

void MCALL CCANOpenPDOSlave::Attach(IPortObject *pPort)
{
	m_pHandler = new CCANPortPDOHandler(m_pHelper);

	m_pHandler->SetDrop(m_bDrop);

	m_pHandler->SetDecode(m_bDecode);
	
	m_pHandler->SetGuard(m_fGuard);

	m_pHandler->SetProfile(m_bProfile);

	m_pHandler->SetOp(m_bOp);
	
	if( m_pPDO ) {

		UINT uProducer = 0;

		for( UINT u = 0; u < m_uPDO; u++ ) {

			if( (m_pPDO[u].dwCobID >> 7) % 2 > 0 ) {

				uProducer++;
				}
			}

		m_pHandler->AllocObjects(TRUE, uProducer);

		m_pHandler->AllocObjects(FALSE, m_uPDO - uProducer);
		
		for( UINT n = 0; n < m_uPDO; n++ ) {

			m_pHandler->MapPDO(m_pPDO[n]);
			}

		delete [] m_pPDO;

		m_uPDO = 0;
		}

	pPort->Bind(m_pHandler);
	}

void MCALL CCANOpenPDOSlave::Detach(void)
{
	m_pHandler->Release();
	}

void MCALL CCANOpenPDOSlave::Open(void)
{	
	//m_pHandler->LoadPDOs();
	}

// Device

CCODE MCALL CCANOpenPDOSlave::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	return CCODE_SUCCESS;
	}

CCODE MCALL CCANOpenPDOSlave::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE CCANOpenPDOSlave::Ping(void)
{
	return CCODE_SUCCESS;
	}

CCODE CCANOpenPDOSlave::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD dwID = (Addr.a.m_Offset & 0x7FFF) >> 3;

	dwID = 2 * (dwID - 1) + (Addr.a.m_Extra ? 3 : 4);

	dwID = (dwID << 7) | m_bDrop;
		
	PDO * pPDO = m_pHandler->FindPDORecord(dwID, RPDO);

	if( pPDO ) {

		UINT uBit    = 0;

		UINT uSize   = 0;

		UINT Count   = 0;

		BOOL fHit    = FALSE;

		for( UINT x = 0, y = 0, z = 0; x < 8 && Count < uCount; x++ ) {

			uBit   = pPDO->dwMap[x] & 0xFF;

			uSize += uBit;
			
			if( ( x == (Addr.a.m_Offset & 0x07) || fHit ) && Count < uCount ) {

				z++;

				fHit = TRUE;
				}
			
			if( fHit ) {

				Count++;
				}

			switch( uBit ) {

				case 8:
					if( fHit ) {

						pData[z - 1] = pPDO->bData[y];
						}
					y++;

					break;

				case 16:
					if( fHit ) {

						pData[z - 1]  = pPDO->bData[y];			y++;
						pData[z - 1] |= (pPDO->bData[y] << 8);		y++;
						}
					else {
						y += 2;
						}
					break;

				case 32:
					if( fHit ) {

						pData[z - 1]  = pPDO->bData[y];			y++;
						pData[z - 1] |= (pPDO->bData[y] << 8);		y++;
						pData[z - 1] |= (pPDO->bData[y] << 16);		y++;
						pData[z - 1] |= (pPDO->bData[y] << 24);		y++;
						}
					else {
						y += 4;
						}
					break;
				}

			if( uSize >= 64 ) {

				break;
				}
			} 
	
		if( fHit ) {

			MakeMin(uCount, Count);

			return uCount;
			}
		} 

	return CCODE_ERROR;
	}

CCODE CCANOpenPDOSlave::Write(AREF Addr, PDWORD pData, UINT uCount)
{      
	DWORD dwID = (Addr.a.m_Offset & 0x7FFF) >> 3;

	dwID = 2 * (dwID - 1) + (Addr.a.m_Extra ? 3 : 4);

	dwID = (dwID << 7) | m_bDrop;
		
	PDO * pPDO = m_pHandler->FindPDORecord(dwID, TPDO);

	if( pPDO ) {

		DWORD dwData = 0;

		UINT uBit    = 0;

		UINT uSize   = 0;

		UINT Count   = 0;

		BOOL fHit    = FALSE;

		for( UINT x = 0, y = 0, z = 0; x < 8 && Count < uCount; x++ ) {

			uBit   = pPDO->dwMap[x] & 0xFF;

			uSize += uBit;

			dwData = 0;

			if( ( x == (Addr.a.m_Offset & 0x07) || fHit ) && Count < uCount ) {

				dwData = pData[z];

				z++;

				fHit = TRUE;
				}
			
			if( fHit ) {

				Count++;
				}

			switch( uBit ) {

				case 8:
					if( fHit ) {

						pPDO->bData[y] = LOBYTE(LOWORD(dwData));
						}
					y++;

					break;

				case 16:
					if( fHit ) {

						pPDO->bData[y] = LOBYTE(LOWORD(dwData));	y++;
						pPDO->bData[y] = HIBYTE(LOWORD(dwData));	y++;
						}
					else {
						y += 2;
						}
					break;

				case 32:
					if( fHit ) {

						pPDO->bData[y] = LOBYTE(LOWORD(dwData));	y++;
						pPDO->bData[y] = HIBYTE(LOWORD(dwData));	y++;
						pPDO->bData[y] = LOBYTE(HIWORD(dwData));	y++;
						pPDO->bData[y] = HIBYTE(HIWORD(dwData));	y++;
						}
					else {
						y += 4;
						}
					break;
				}
			
			if( uSize >= 64 ) {

				break;
				}
			} 
	
		if( fHit ) {

			MakeMin(uCount, Count);

			pPDO->fDirty = TRUE;

			if( m_pHandler->IsPrn(pPDO) ) {

				pPDO->uTimer = m_pHandler->GetTimerTick();
				}				 

			return uCount;
			}
		} 

	return CCODE_ERROR;
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN PDO Port Handler
//

// Constructor

CCANPortPDOHandler::CCANPortPDOHandler(IHelper *pHelper)
{
	StdSetRef();

	m_pHelper    = pHelper;
	
	m_pPort      = NULL;

	m_pTPDO	     = NULL;

	m_pRPDO      = NULL;

	m_uTPDO	     = 0;

	m_uRPDO      = 0;

	m_fOp        = FALSE;

	m_bDrop      = 8;

	m_bDecode    = 0;

	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtraHelper);
	}

// Destructor

CCANPortPDOHandler::~CCANPortPDOHandler(void)
{
	}

// IUnknown

HRESULT METHOD CCANPortPDOHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG METHOD CCANPortPDOHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG METHOD CCANPortPDOHandler::Release(void)
{
	StdRelease();
	}

// IPortHandler

void CCANPortPDOHandler::Bind(IPortObject * pPort)
{
	m_pPort = pPort;
	}

void CCANPortPDOHandler::OnOpen(CSerialConfig const &Config)
{
	m_pTxData    = NULL;

	m_uTxCount   = 0;
	
	m_uRxByte    = 0;

	m_uQueue     = 0;
	
	m_uSend      = 0;

	m_uState     = NMTS_BOOT;

	m_uLast	     = 0;

	m_uGuardTime = 0;

	m_uLifeTime  = 0;

	m_uHeartBeat = 0;

	m_fTxEnable  = FALSE;
	}

void CCANPortPDOHandler::OnClose(void)
{
	}

void CCANPortPDOHandler::OnRxData(BYTE bData)
{
	if( m_uRxByte < sizeof(FRAME) ) {

		PBYTE pRx = PBYTE(&m_Rx);

		pRx[m_uRxByte] = bData;

		m_uRxByte++;
		}
	}

void CCANPortPDOHandler::OnRxDone(void)
{
	if( m_uRxByte == sizeof(FRAME) ) {

		HandleFrame();

		m_uRxByte = 0;
		}
	}

BOOL CCANPortPDOHandler::OnTxData(BYTE &bData)
{
	if( m_uTxCount > 0 ) {

		if( m_pTxData ) {

		 	m_uTxCount--;

			bData = *m_pTxData++;

			return TRUE;
			}
		}

	Increment(m_uSend);

	m_pTxData = NULL;

	return FALSE;
	}

void CCANPortPDOHandler::OnTxDone(void)
{
	if( !m_pTxData && m_fTxEnable ) { 

		if( m_uSend != m_uQueue ) {

			SendFrame();
			}
		}
	}

void CCANPortPDOHandler::OnTimer(void)
{
	if( m_pTPDO ) {

		if( m_fOp ) {

			for( UINT u = 0; u < m_uTPDO; u++ ) {

				PDO * pPDO = &m_pTPDO[u];

				if( CheckUpdate(pPDO) ) {

					UINT uTimer = GetTickCount();

					UINT uEvent = 0;

					if ( pPDO->uEvent > 0 ) {

						uEvent = pPDO->uEvent; 
						}
										
					if ( pPDO->uInhibit > 0 ) {
						
						uEvent = pPDO->uInhibit;
						}
				
					if( IsTimedOut(pPDO->uTimer, uEvent) ) {

						uTimer = GetTickCount();

						Start(transTimer);

						m_Tx[transTimer].bCount = 8;

						m_Tx[transTimer].wID = pPDO->dwCobID;

						memcpy(m_Tx[transTimer].bData, pPDO->bData, 8);

						PutFrame(transTimer); 
							
						pPDO->uTimer = uTimer ? uTimer : 1;
						}
					}
				}
			}
		}  
	}

// Implementation

void CCANPortPDOHandler::Start(UINT uTrans)
{
	memset(&m_Tx[uTrans], 0, sizeof(FRAME));
	}

BOOL CCANPortPDOHandler::PutFrame(UINT uTrans)
{
	AfxRaiseIRQL(IRQL_HARDWARE + ExpGetIPL(0, 4));
	
	UINT uQueue = m_uQueue;
	
	Increment(m_uQueue);
	
	if( m_uQueue != m_uSend ) {
		
		m_fTxEnable = false;
		
		AfxLowerIRQL();
		
		memcpy(&m_Send[uQueue], &m_Tx[uTrans], sizeof(FRAME));
		
		m_fTxEnable = true;
		
		AfxRaiseIRQL(IRQL_HARDWARE + ExpGetIPL(0, 4));
		
		if( !m_pTxData ) {
			
			SendFrame();
			}
		
		AfxLowerIRQL();
		
		return TRUE;
		}
	
	m_uQueue = uQueue;
	
	AfxLowerIRQL();
	
	return FALSE;
	}

void CCANPortPDOHandler::SendFrame(void)
{
	m_pTxData = PCBYTE(&m_Send[m_uSend]);

	m_uTxCount = sizeof(FRAME) - 1;

	m_pPort->Send(*m_pTxData++);
	}

void CCANPortPDOHandler::HandleFrame(void)
{
	if( DoListen() ) {

		if( HandleNodeGuard() ) {

			return;
			}

		if( HandleSync() ) {
		
			return;
			}

		if( HandleNMT() )  {
			
			return;
			}

		if( HandleRPDO() ) {
			
			return;
			}

		if( HandleHeartBeat() ) {
			
			return;
			}

		if( HandleSDO() ) {
			
			return;
			}
	
		if( HandleEMCY() ) {
			
			return;
			}

		if( HandleNMTError() ) {
			
			return;
			}
		}
	}

BOOL CCANPortPDOHandler::HandleBootup(void)
{
	if( m_uState == NMTS_BOOT ) {

		m_uLast  = GetTickCount();

		Start();
	
		m_Tx[transHandler].bCount = 1;

		m_Tx[transHandler].wID = NMT_COB_ID | m_bDrop;

		m_uState = NMTS_PRE_OP;

		PutFrame();
		} 

	return TRUE;	
	}

BOOL CCANPortPDOHandler::HandleSync(void)
{
	if( IsSync() ) {

		if( m_fOp ) {

			for( UINT u = 0; u < m_uTPDO; u++ ) {

				PDO * pPDO = &m_pTPDO[u];

				if( pPDO->bType < 252 ) {

					if( pPDO->bType > 0 && pPDO->uTimer < UINT(pPDO->bType - 1) ) {

						pPDO->uTimer++;

						continue;
						}

					if( pPDO->bType == 0 && !pPDO->fDirty ) {

						continue;
						}					

					Start();

					m_Tx[transHandler].bCount = 8;

					m_Tx[transHandler].wID = pPDO->dwCobID;

					memcpy(m_Tx[transHandler].bData, pPDO->bData, 8);

					PutFrame();

					pPDO->fDirty = FALSE;

					pPDO->uTimer = 0;
					}
				}
			 }
			 		
		return TRUE;
		}

	return FALSE;
	}


BOOL CCANPortPDOHandler::HandleNMT(void)
{
	if( IsNMT() ) {

		switch( m_Rx.bData[0] ) {

			case NMT_START:
				SetState(NMTS_OP);
				m_fOp = TRUE;
				break;

			case NMT_STOP:
				SetState(NMTS_STOP);
				m_fOp = FALSE;
				break;
		       			
			case NMT_PRE_OP:
				SetState(NMTS_PRE_OP);
				m_fOp = FALSE;
				break;

			case NMTS_BOOT:
			case NMT_RESET:
			case NMT_RESET_COMM:
				m_uState = NMTS_BOOT;
				m_fOp = FALSE;
				break;
			}

		return TRUE;
		}
 
	return FALSE;
	}

BOOL CCANPortPDOHandler::HandleNodeGuard(void)
{
	UINT uNow = GetTickCount();

	UINT uLife = ToTicks(m_uGuardTime * m_uLifeTime);
	
	if( IsNodeGuard() ) {

		if( m_uState == NMTS_BOOT ) {

			return HandleBootup();
			} 
	
		Start();

		m_Tx[transHandler].bCount = 1;

		m_Tx[transHandler].wID = NMT_COB_ID | m_bDrop;

		m_uState = m_uState & 0x80 ? m_uState & ~0x80 : m_uState | 0x80;
	
		m_Tx[transHandler].bData[0] = m_uState;
	
		PutFrame();

		m_uLast = uNow;

		return TRUE;
		}

	else if( m_fGuard && uLife && (uNow - m_uLast > uLife) ) {

		// TODO:  Life guarding event
		} 

	return FALSE;
	}

BOOL CCANPortPDOHandler::HandleRPDO(void)
{
	if( IsRPDO() ) {

		PDO * pPDO = FindPDORecord(m_Rx.wID, RPDO);

		memcpy(pPDO->bData, m_Rx.bData, 8);

		pPDO->uTimer = GetTickCount();

		return TRUE;
		}

  	return FALSE;
	}

BOOL CCANPortPDOHandler::HandleHeartBeat(void)
{
	if( IsHeartBeat() ) {

		if( m_uState == NMTS_BOOT ) {

			return HandleBootup();
			}

		UINT uNow = GetTickCount();
	
		if( uNow - m_uLast <= ToTicks(m_uHeartBeat) ) {

			m_uLast = uNow;
			}

		else {	
			// TODO:  HeartBeat event
			}


		return TRUE;
		} 

	return FALSE;
	}

BOOL CCANPortPDOHandler::HandleSDO(void)
{
	if( IsSDO() ) {

		switch( m_Rx.bData[0] >> 5 ) {
	
			case 0x2:
				
				HandleRead();
				break;
			
			case 0x1:
				
				HandleWrite();
				break;
			}

		switch(  m_Rx.bData[2] ) {

			case PRM_RPDO:
			case PRM_RPDO + 0x1:
				HandleParameter(RPDO);
				break;

			case PRM_TPDO:
			case PRM_TPDO + 0x1:
				HandleParameter(TPDO);
				break;

			case MAP_RPDO:
			case MAP_RPDO + 0x1:
				HandleMapping(RPDO);
				break;

			case MAP_TPDO:
			case MAP_TPDO + 0x1:
				HandleMapping(TPDO);
				break;
		       	}
					
		return TRUE;
		}

	return FALSE;
	}

BOOL CCANPortPDOHandler::HandleRead(void)
{
	WORD  wIndex = MAKEWORD(m_Rx.bData[1], m_Rx.bData[2]);

	BYTE  bSub   = m_Rx.bData[3];

	DWORD lData  = 0;

	UINT uSize = GetSpec(wIndex, bSub, lData);

	Start();

	m_Tx[transHandler].bCount   = 8;

	m_Tx[transHandler].bZero    = 0;

	m_Tx[transHandler].wID = 0xB << 7 | m_bDrop;

	m_Tx[transHandler].bData[0] = 0x2 << 5 | (4 - uSize) << 2 | 3;

	m_Tx[transHandler].bData[1] = LOBYTE(wIndex);
	m_Tx[transHandler].bData[2] = HIBYTE(wIndex);
	m_Tx[transHandler].bData[3] = bSub;

	m_Tx[transHandler].bData[4] = LOBYTE(LOWORD(lData));
	m_Tx[transHandler].bData[5] = HIBYTE(LOWORD(lData));
	m_Tx[transHandler].bData[6] = LOBYTE(HIWORD(lData));
	m_Tx[transHandler].bData[7] = HIBYTE(HIWORD(lData));

	PutFrame();

	return TRUE;
	}

BOOL CCANPortPDOHandler::HandleWrite(void)
{
	WORD  wIndex = MAKEWORD(m_Rx.bData[1], m_Rx.bData[2]);

	BYTE  bSub   = m_Rx.bData[3];

	UINT  uSize  = 4 - ((m_Rx.bData[0] >> 2) & 0x03);

	DWORD lMask  = 0xFFFFFFFF >> (8 * (4 - uSize));

	DWORD lData  = (IntelToHost(PDWORD(m_Rx.bData + 4)[0]) & lMask);

	SaveData(wIndex, lData);
	
	Start();

	m_Tx[transHandler].bCount   = 8;

	m_Tx[transHandler].bZero    = 0;

	m_Tx[transHandler].wID = 0xB << 7 | m_bDrop;

	m_Tx[transHandler].bData[0] = 0x3 << 5;

	m_Tx[transHandler].bData[1] = LOBYTE(wIndex);
	m_Tx[transHandler].bData[2] = HIBYTE(wIndex);
	m_Tx[transHandler].bData[3] = bSub;

	m_Tx[transHandler].bData[4] = 0;
	m_Tx[transHandler].bData[5] = 0;
	m_Tx[transHandler].bData[6] = 0;
	m_Tx[transHandler].bData[7] = 0;

	PutFrame();

	return TRUE;
	}

BOOL CCANPortPDOHandler::HandleEMCY(void)
{
	// ID is found @ Object 1014h

	return FALSE;
	}

BOOL CCANPortPDOHandler::HandleNMTError(void)
{
	// FC 0xE (1110b)

	return FALSE;
	}
 
BOOL CCANPortPDOHandler::IsSync(void)
{
	return ( (m_Rx.bCount == 0 || m_Rx.bCount == 1) && m_Rx.wID == 0x80 );
	}

BOOL CCANPortPDOHandler::IsNodeGuard(void)
{
	return ( m_fGuard && (m_Rx.wID == NMT_COB_ID + m_bDrop) );
	}

BOOL CCANPortPDOHandler::IsNMT(void)
{
	return ( (m_Rx.bCount == 0x2) && (m_Rx.wID == 0) && (IsDrop(m_Rx.bData[1], TRUE)) ); 
	}

BOOL CCANPortPDOHandler::IsRPDO(void)
{
	BYTE bCode = m_Rx.wID >> 7;

	return ( (bCode < 0x0C) && (bCode >= 4) && (bCode % 2 == 0) );
	}

BOOL CCANPortPDOHandler::IsHeartBeat(void)
{
	return ( !m_fGuard && (m_Rx.wID == NMT_COB_ID + m_bDrop) );
	}

BOOL CCANPortPDOHandler::IsSDO(void)
{	
	BYTE bCode = m_Rx.wID >> 7;
	
	return ( bCode == 0xC ); 
	}

BOOL CCANPortPDOHandler::DoListen(void)
{
	BYTE bDrop = m_Rx.wID & 0x7F;

	return ( bDrop == m_bDrop || bDrop == 0x0 );
	}

BOOL CCANPortPDOHandler::IsDrop(BYTE bDrop, BOOL fWrite)
{
	if( fWrite && bDrop == 0 ) {

		return TRUE;
		}

	return ( bDrop == m_bDrop );
	}

// PDO Access

void CCANPortPDOHandler::AllocObjects(BOOL fProducer, UINT uCount)
{
	PDO  * pXPDO = fProducer ?  m_pTPDO :  m_pRPDO;

	UINT * uXPDO = fProducer ? &m_uTPDO : &m_uRPDO;

	if( !pXPDO && uCount < MAX_PDO ) {

		(*uXPDO) = uCount;
	
		pXPDO = new PDO [(*uXPDO)];

		if( fProducer ) {

			delete [] m_pTPDO;

			m_pTPDO = pXPDO;
			}
		else {
			delete [] m_pRPDO;
			
			m_pRPDO = pXPDO;
			}

		for( UINT u = 0; u < (*uXPDO); u++ ) {

			InitObject(&pXPDO[u], u);
		      	}
		}
	}

void CCANPortPDOHandler::MapPDO(PDO & pPDO)
{
	UINT uPDO = pPDO.dwCobID >> 7;

	PDO  * pXPDO = FindNextPDO(uPDO % 2 ? TPDO : RPDO );

	if( pXPDO ) {

		UINT uSize = 0;

		for( UINT u = 0; u < 8; u++ ) {

			pXPDO->dwMap[u] = pPDO.dwMap[u];

			uSize += pXPDO->dwMap[u] & 0xFF;

			if( uSize >= 64 ) {

				break;
				}
			}

		pXPDO->dwCobID	       = pPDO.dwCobID;

		pXPDO->Addr.a.m_Table  = 0x10;

		pXPDO->Addr.a.m_Type   = addrLongAsLong;

		pXPDO->Addr.a.m_Extra  = uPDO % 2;

		uPDO = (uPDO - (uPDO % 2 ? 3 : 4)) / 2 + 1;

		pXPDO->Addr.a.m_Offset = uPDO << 3;

		if( pPDO.fManual ) {

			pXPDO->uEvent  = pPDO.uEvent;

			pXPDO->fManual = TRUE;
			}
		else {
			pXPDO->fManual = FALSE;
			}
		}
	}

void  CCANPortPDOHandler::LoadPDOs(void)
{
	UINT u, uPDO;
	
	switch(m_bProfile) {

		case profile401:

			AllocObjects(TRUE, 4);

			AllocObjects(FALSE, 4);

			for( u = 0, uPDO = 3; u < 4; u++ ) {

				switch( u + 1 ) {

					case 1:
						InitPDORecord(&m_pTPDO[u], uPDO, 0x60000108, 8);	uPDO++;
						InitPDORecord(&m_pRPDO[u], uPDO, 0x62000108, 8);	uPDO++;
						break;

					case 2:
						InitPDORecord(&m_pTPDO[u], uPDO, 0x64010110, 4);	uPDO++;
						InitPDORecord(&m_pRPDO[u], uPDO, 0x64110110, 4);	uPDO++;
						break;

					case 3:
						InitPDORecord(&m_pTPDO[u], uPDO, 0x64010510, 4);	uPDO++;
						InitPDORecord(&m_pRPDO[u], uPDO, 0x64110510, 4);	uPDO++;
					       	break;
					
					case 4:
						InitPDORecord(&m_pTPDO[u], uPDO, 0x64010910, 4);	uPDO++;
						InitPDORecord(&m_pRPDO[u], uPDO, 0x64110910, 4);	uPDO++;
						break;
					} 
				}
			break;

		case profileNone:

			break;
		}
	}

PDO * CCANPortPDOHandler::FindPDORecord(DWORD dwID, UINT uPDO)
{
	if( uPDO == TPDO ) {

		return FindTPDORecord(dwID);
		}
	
	return FindRPDORecord(dwID);
	}

PDO * CCANPortPDOHandler::FindNextPDO(UINT uPDO)
{
	if( uPDO == TPDO ) {

		return FindNextTPDO();
		}

	return FindNextRPDO();
	}

PDO * CCANPortPDOHandler::GetRPDO(UINT uIndex)
{
	if( uIndex < m_uRPDO ) {

		return &m_pRPDO[uIndex];
		}

	return NULL;
	}

// Member Access

void CCANPortPDOHandler::SetOp(BOOL fOp)
{
	m_fOp = fOp;
	}

void CCANPortPDOHandler::SetDrop(BYTE bDrop)
{
	m_bDrop = bDrop;
	}

void CCANPortPDOHandler::SetProfile(BYTE bProfile)
{
	m_bProfile = bProfile;
	}

void CCANPortPDOHandler::SetGuard(BOOL fGuard)
{
       	m_fGuard = fGuard;
	}

void CCANPortPDOHandler::SetDecode(BYTE bDecode)
{
	m_bDecode = bDecode;
	}

BOOL CCANPortPDOHandler::IsTimedOut(UINT uTime, UINT uSpan)
{
	return int(GetTickCount() - uTime - ToTicks(uSpan)) >= 0;
	}

BOOL CCANPortPDOHandler::IsPrn(PDO * pPDO)
{
	if( pPDO ) {

		return (pPDO->fManual && pPDO->uEvent == 0);
		}

	return FALSE;
	}

UINT CCANPortPDOHandler::GetTimerTick(void)
{
	UINT uTick = GetTickCount();

	return uTick ? uTick : 1;
	}

// PDO Implementaion

void CCANPortPDOHandler::InitObject(PDO * pPDO, UINT uOffset)
{
	pPDO->dwCobID  = 0;

	pPDO->bType    = 255;

	pPDO->uInhibit = 0;  
		
	pPDO->uEvent   = 0;

	UINT uTimer    = GetTickCount() - ToTicks(pPDO->uEvent);

	pPDO->uTimer   = uTimer ? uTimer : 1;

	pPDO->uOffset  = uOffset;

	pPDO->fDirty   = FALSE;

	memset(PBYTE(pPDO->dwMap), 0, 32);

	memset(pPDO->bData, 0, 8);
	}

void CCANPortPDOHandler::InitPDORecord(PDO * pPDO, UINT uPDO, DWORD dwMap, UINT uSize)
{
	if( pPDO ) {
	
		pPDO->dwCobID = uPDO << 7 | m_bDrop;
	
		for( UINT u = 0; u < uSize; u++, dwMap += 0x100 ) {

			pPDO->dwMap[u] = dwMap;
			}
		}
	}

PDO * CCANPortPDOHandler::FindRPDORecord(DWORD dwID)
{
	for( UINT u = 0; u < m_uRPDO; u++ ) {

		if( m_pRPDO[u].dwCobID == dwID ) {

			return &m_pRPDO[u];
			}
		}
	
	return NULL;
	}

PDO * CCANPortPDOHandler::FindTPDORecord(DWORD dwID)
{
	for( UINT u = 0; u < m_uTPDO; u++ ) {

		if( m_pTPDO[u].dwCobID == dwID ) {

			return &m_pTPDO[u];
			}
		}
	
	return NULL;
	}

PDO * CCANPortPDOHandler::FindNextRPDO(void)
{
	for( UINT x = 0; x < m_uRPDO; x++ ) {

		if( m_pRPDO[x].dwCobID == 0 ) {

			return &m_pRPDO[x];
			}
		}

	return NULL;
	}


PDO * CCANPortPDOHandler::FindNextTPDO(void)
{
	for( UINT x = 0; x < m_uTPDO; x++ ) {

		if( m_pTPDO[x].dwCobID == 0 ) {

			return &m_pTPDO[x];
			}
		}

	return NULL;
	}

void CCANPortPDOHandler::HandleParameter(UINT uPDO)
{
	UINT uOffset = m_Rx.bData[1];
		
	UINT uSub    = m_Rx.bData[3];
		     
	UINT *  uXPDO = uPDO == TPDO ? &m_uTPDO : &m_uRPDO;

	PDO  *  pXPDO = uPDO == TPDO ?  m_pTPDO :  m_pRPDO;

	PDO * pPDO = NULL;

	for( UINT u = 0; u < (*uXPDO); u++ ) {

		if( pXPDO[u].uOffset == uOffset ) {

			pPDO = &pXPDO[u];

			break;
			}
		}

	if( pPDO ) {

		switch( uSub ) {

			case 0:  // Largest sub-index supported
			
				break;

			case 1:  // COB-ID used by PDO
			
				pPDO->dwCobID = GetRxData();
				break;

			case 2:  // Transmission Type

				pPDO->bType   = LOBYTE(LOWORD(GetRxData()));
				break;

			case 3:  // Inhibit Time

				pPDO->uInhibit = GetRxData();
				break;

			case 5:  // Event Timer
				
				pPDO->uEvent   = GetRxData();
				break;
			}
		}
	}

void CCANPortPDOHandler::HandleMapping(UINT uPDO)
{
	UINT uOffset = m_Rx.bData[1];
		
	UINT uSub    = m_Rx.bData[3];
		     
	UINT *  uXPDO = uPDO == TPDO ? &m_uTPDO : &m_uRPDO;

	PDO  *  pXPDO = uPDO == TPDO ?  m_pTPDO :  m_pRPDO;

	for( UINT u = 0; u < (*uXPDO); u++ ) {

		if( pXPDO[u].uOffset == uOffset ) {

			PDO * pPDO = &pXPDO[u];

			pPDO->dwMap[uSub] = GetRxData();

			if( pPDO->dwCobID == 0 ) {	

				pPDO->dwCobID = ((uOffset + 3) << 7) | m_bDrop;
				}

			break;
			}
		} 
	}

// Helpers

DWORD CCANPortPDOHandler::GetRxData(void)
{
	WORD wHi = MAKEWORD(m_Rx.bData[6], m_Rx.bData[7]);
			
	WORD wLo = MAKEWORD(m_Rx.bData[4], m_Rx.bData[5]);

	return MAKELONG( wLo, wHi );
	}

UINT CCANPortPDOHandler::GetSpec(WORD wIndex, BYTE bSub, DWORD &Data)
{
	PCTXT pText;
	
	switch( wIndex ) {

		case 0x1000:

			switch( bSub ) {

				case 0:
					// Device Profile

					Data = GetDeviceProfile();
					
					return sizeof(DWORD);
				}
			break;
		
		case 0x1001:
			
			switch( bSub ) {
				
				case 0:
					// Error Flags

					Data = 0x00;
					
					return sizeof(BYTE);
				}
			break;

		case 0x1008:

			switch( bSub ) {

				case 0:
					// Manufacturer Device Name

					Data = MAKEWORD('G', '3');
					
					return sizeof(WORD);
				}
			break;

		case 0x1009:

			switch( bSub ) {

				case 0:
					// Manufacturer Hardware Version

					Data = MAKEWORD('1', '0');
					
					return sizeof(WORD);
				}
			break;

		case 0x100A:

			switch( bSub ) {

				case 0:
					// Manufacturer Software Version

					Data = MAKEWORD('2', '0');
					
					return sizeof(WORD);
				}
			break;

		
		case 0x1018:
			
			switch( bSub ) {
				
				case 0:
					// Identity Count

					Data = 0x04;
					
					return sizeof(BYTE);
				
				case 1:
					// Vendor ID

					Data = 0x0000022C; 
					
					return sizeof(DWORD);
				
				case 2:
					// Product Code

					pText = m_pExtraHelper->WhoGetName(FALSE);

					Data = MAKELONG(MAKEWORD(pText[0], pText[1]), 
							MAKEWORD(pText[2], pText[3]));
					
					return sizeof(DWORD);
				
				case 3:
					// Revision

					Data = 0x00010000;
					
					return sizeof(DWORD);
				
				case 4:
					// Serial Number

					MACADDR Mac;

					m_pExtraHelper->GetMacId(0, PBYTE(&Mac)); 

					Data = MotorToHost((DWORD &) Mac.m_Addr[2]);  

					return sizeof(DWORD);
				}
			break;
		}

	return 0;
	}

BOOL CCANPortPDOHandler::PutSpec(WORD wIndex, BYTE bSub, UINT uSize, DWORD Data)
{
	return FALSE;
	}

void CCANPortPDOHandler::SaveData(WORD wIndex, DWORD Data)
{
	switch( wIndex ) {

		case 0x100C:

			m_uGuardTime = Data;

			m_fGuard = TRUE;

			break;
		
		case 0x100D:

			m_uLifeTime  = Data;

			m_fGuard = TRUE;

			break;

		case 0x1016:

			if( Data >> 16 == m_bDrop ) {

				m_uHeartBeat = Data & 0xFFFF;

				m_fGuard = FALSE;
				}
			break;
		}
	}

DWORD CCANPortPDOHandler::GetDeviceProfile(void)
{
	switch(m_bProfile) {

		case profile401:	
						
			return 0xF << 16 | 0x0191;
		}

	return 0x00000000;
	}

void CCANPortPDOHandler::SetState(UINT uState)
{
	m_uState = (m_uState & 0x80) | uState;
	}

UINT CCANPortPDOHandler::GetState(void)
{
	return m_uState & 0x7F;
	}

void CCANPortPDOHandler::Increment(UINT &uIndex)
{
	uIndex = (uIndex + 1) % elements(m_Send);
	}

BOOL CCANPortPDOHandler::IsPrnDirty(PDO * pPDO)
{
	if( IsPrn(pPDO) ) {

		if( pPDO->fDirty ) {

			pPDO->fDirty = FALSE;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCANPortPDOHandler::CheckUpdate(PDO * pPDO)
{
	if( pPDO ) {

		if( pPDO->bType > 253 && pPDO->uTimer ) {
			
			if( pPDO->uEvent || pPDO->uInhibit || IsPrnDirty(pPDO) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// End of File

