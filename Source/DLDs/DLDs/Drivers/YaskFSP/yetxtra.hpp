
//////////////////////////////////////////////////////////////////////////
//
// YET Xtra Drive Driver
//

class CYETXtraDriver : public CMasterDriver
{
	public:
		// Constructor
		CYETXtraDriver(void);

		// Destructor
		~CYETXtraDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			};

		CContext *	m_pCtx;

		// Data Members

		// Hex Lookup
		LPCTXT	m_pHex;

		// Comm Frames
		BYTE	m_bTx[64];
		BYTE	m_bRx[64];

		UINT	m_uPtr;
		UINT	m_uStatus;
		BYTE	m_bCheck;
		DWORD	m_dError;
		BOOL	m_fIsPoll;
		UINT	m_uBaseCommand;
		BYTE	m_uSequence;
		BOOL	m_fSeqMismatch;
		BOOL	m_fHasChecksum;
		UINT	m_uPollCount;
		UINT	m_uRcvCount;

		// Cached Item Data Storage
		// Immediate
		DWORD	m_dECPI[5];
		DWORD	m_dECSI[3];
		DWORD	m_dFOSI[3];
		DWORD	m_dREAI[2];
		DWORD	m_dS1OI[2];
		DWORD	m_dSNOI[2];
		DWORD	m_dSTXI[2];
		DWORD	m_dTQLI[2];
		DWORD	m_dWRII[2];
// -0- Exclude code if indirect Variable needed
// -1- Include code if indirect Variable needed
// -2- Include code if Sequential Mode needed
// -3- Exclude code if Sequential Mode needed
/* -2-
		// Sequential
		DWORD	m_dECES[2];
		DWORD	m_dENGS[2];
		DWORD	m_dFOSS[3];
		DWORD	m_dGOAS[2];
		DWORD	m_dGODS[2];
		DWORD	m_dHARS[2];
		DWORD	m_dHMSS[2];
		DWORD	m_dHSCS[2];
		DWORD	m_dMVAS[2];
		DWORD	m_dMVDS[2];
		DWORD	m_dREAS[2];
		DWORD	m_dS1OS[2];
		DWORD	m_dSNOS[2];
		DWORD	m_dSTXS[2];
		DWORD	m_dTQLS[2];
		DWORD	m_dWAIS[4];
		DWORD	m_dWAVS[3];
		DWORD	m_dWRIS[2];
-2- */
		// Execute Polling		
		CCODE	DoPoll(PDWORD pData);

		// Frame Building
		void	StartFrame(BYTE bMode, BOOL fIsPoll);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dData);
		BOOL	AddRead(AREF Addr);
		BOOL	AddWrite(AREF Addr, DWORD dData);
		BOOL	AddWriteData(UINT uOffset, DWORD dData);
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		UINT	GetReadResponse(AREF Addr, PDWORD pData);
		BOOL	GetWriteResponse(void);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		// Internal Access
		BOOL	ReadNoTransmit(AREF Addr, PDWORD pData);
		BOOL	WriteNoTransmit(AREF Addr, PDWORD pData);
		BOOL	GetCachedData(PDWORD pData, UINT uCommand);
		BOOL	PutCachedData(DWORD dData, UINT uCommand);
		BYTE	GetDataSource(UINT uOffset, DWORD dData);

		// Command Mode Check
		BYTE	GetMode(UINT uOffset, UINT uTable);

		// Response Data
		DWORD	GetValue(UINT uPos, UINT uCount);

		// Reply Verification
		BOOL	CheckReply(void);
		BOOL	VerifyChecksum(void);

		// Response Verification
		BOOL	ThisCommandResponse(void);
		BOOL	IsSeqAndFunc(void);
		UINT	SetLength(UINT uCommand);
		BOOL	CheckLength(UINT uCheckVal);
		UINT	CheckErrorByte(AREF Addr);

		// Utilities
		UINT	xtoin(BYTE b);
		BYTE	MakeRxByte(UINT uRxPos);

	};

// Command definitions
#define	SEQOFF	0x2000
#define	CP1	0x100
#define	CP2	0x200
#define	CP3	0x300
#define	CP4	0x400
#define	CP5	0x500

#define	CP1S	0x100 + SEQOFF
#define	CP2S	0x200 + SEQOFF
#define	CP3S	0x300 + SEQOFF
#define	CP4S	0x400 + SEQOFF
#define	CP5S	0x500 + SEQOFF

// Value, not parameter
#define	INTVAL	0
#define	VARPAR	2 // 2nd argument of 

#define MODEPOLL	0x9
#define MODEIMMEDIATE	0xA
#define MODESEQUENTIAL	0xB

#define	TABLEIMMHIGH	0x3

// Transmit data positions
#define	TX_ADDR		 1
#define	TX_MODE		 2
#define	TX_SEQH		 3
#define	TX_SEQL		 4
#define	TX_FNCH		 5
#define	TX_FNCL		 6

// Read response data positions
#define	RSP_ADDR	 0
#define	RSP_STAT	 1
#define	RSP_SEQ		 2
#define	RSP_FNC		 4
#define	RSP_PAR		10
#define	RSP_TABLE	 8
#define	RSP_VERSION	 6
#define	RSP_POLL	 6
#define	RSP_POLLCHECK	 4

// Machine Status
#define	RSP_MSTATUS	 6

// Error Definition
#define	ERR_INVPAR	27
#define	ERR_NONE	 0
#define	ERR_NODATA	 1
#define	ERR_ERROR	 2
#define	ERR_SIZECK	 4
#define	POLL_LEN	13
#define	VER_LEN		13
#define	PAR_LEN		17
#define	VAR_LEN		19
#define	ARR_LEN		21

#define	NEXT_ITEM	CCODE_NO_DATA | CCODE_ERROR

// Read Response Status
#define	RSTAT_POLL	'0'
#define	RSTAT_ERROR	'1'
#define	RSTAT_READDATA	'2'
#define	RSTAT_WATCHVAR	'3'
#define	RSTAT_UPLOAD	'5'

// Table Items
//   Immediate
#define	CMGFAI	1
#define	CMPARI	2
#define	CMVARI	3
//   Sequential
#define	CMGFAS	CMGFAI+TABLEIMMHIGH
#define	CMPARS	CMPARI+TABLEIMMHIGH
#define	CMVARS	CMVARI+TABLEIMMHIGH

#define	OPGFA	160
#define	OPPARR	 85
#define	OPVARR	 72
#define	OPPARW	 80
#define	OPVARW	 81

// Named Items - Immediate Codes, Sequential adds 0x400
#define	CMPOL	0
#define	CMGTV	63
#define	CMACC	64
#define	OPACC	0x005
#define	CMCON	69
#define	CMEND	70
#define	CMGAI	71
#define	CMJRK	74
#define	OPJRK	0x2A6
#define	CMRUN	78
#define	CMS1O	79
#define	CMSPA	OPPARW
#define	CMSVA	OPVARW
#define	CMSTA	82
#define	CMSPD	83
#define	OPSPD	0x003
#define	CMSTP	84
#define	CMTQL	87
#define	OPTQF	0x402
#define	OPTQR	0x403
#define	CMCLE	94
#define	CMSZA	95
#define	CMSAV	96
#define	CMSTM	99
#define	CMSPC	100
#define	CMSLN	102
#define	CMTQA	103
#define	CMSNO	107
#define	CMWAI	109
#define	CMWAV	110
#define	CMGOA	112
#define	CMMVA	113
#define	CMSLD	115
#define	CMTQE	116
#define	CMGOH	117
#define	CMMVH	118
#define	CMMVR	119
#define	CMECE	121
#define	CMECD	122
#define	CMECT	123
#define	CMECR	124
#define	CMECS	125
#define	CMECP	126
#define	CMECZ	127
#define	CMGOD	128
#define	CMMVD	129
#define	CMHSC	130
#define	CMHAR	131
#define	CMHMS	132
#define	CMHMC	133
#define	CMMAT	134
#define	CMENG	136
#define	CMDEL	144
#define	CMWEX	145
#define	CMWFS	146
#define	CMWAS	148
#define	CMREG	151
#define	CMLAT	152
#define	CMSTX	153
#define	CMFOS	154
#define	CMWRI	158
#define	CMRFA	159
#define	CMERR	199

// Parameter Selection Item postions

// Immediate
#define	FOSIV	 0
#define	MATIV	 1
#define	REAIV	 2
#define	S1OIV	 3
#define	SNOIV	 4
#define	SVAIV	 5
#define	WRIIV	 6
// Sequential
#define	DELSV	 7
#define	ECESV	 8
#define	ENGSV	 9
#define	FOSSV	10
#define	GOASV	11
#define	GODSV	12
#define	GOHSV	13
#define	HARSV	14
#define	HMCSV	15
#define	HMSSV	16
#define	HSCSV	17
#define	MATSV	18
#define	MVASV	19
#define	MVDSV	20
#define	MVHSV	21
#define	MVRSV	22
#define	REASV	23
#define	REGSV	24
#define	S1OSV	25
#define	SNOSV	26
#define	SVASV	27
#define	SLDSV	28
#define	TQESV	29
#define	WEXSV	30
#define	WAISV	31
#define	WASSV	32
#define	WAVSV	33
#define	WRISV	34

// End of File
