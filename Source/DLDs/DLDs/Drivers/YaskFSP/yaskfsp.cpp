#include "intern.hpp"

#include "yaskfsp.hpp"

// -0- Exclude code if indirect Variable needed
// -1- Include code if indirect Variable needed
// -2- Include code if Sequential Mode needed
// -3- Exclude code if Sequential Mode needed

//////////////////////////////////////////////////////////////////////////
//
// YaskawaFSP Drive Driver
//

// Instantiator

INSTANTIATE(CYaskawaFSPDriver);

// Constants

static UINT const TIMEOUT = 200;

// Constructor

CYaskawaFSPDriver::CYaskawaFSPDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex		= Hex;

	m_bSequence	= 0xB5;

	m_fSeqMismatch	= FALSE;

	m_uSleepTime	= 100;

	m_uWriteError	= 0;

	memset(m_dRFAI, 0, sizeof(m_dRFAI));
	memset(m_dS1OI, 0, sizeof(m_dS1OI));
	memset(m_dSNOI, 0, sizeof(m_dSNOI));
	memset(m_dSTXI, 0, sizeof(m_dSTXI));
	memset(m_dTQLI, 0, sizeof(m_dTQLI));
	memset(m_dWRII, 0, sizeof(m_dWRII));
	}

// Destructor

CYaskawaFSPDriver::~CYaskawaFSPDriver(void)
{
	}

// Configuration

void MCALL CYaskawaFSPDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CYaskawaFSPDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CYaskawaFSPDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CYaskawaFSPDriver::Open(void)
{
	}

// Device

CCODE MCALL CYaskawaFSPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = LOBYTE(GetWord(pData));

			memset( m_pCtx->m_Table,  0, sizeof(m_pCtx->m_Table) );
			memset( m_pCtx->m_Recent, 0, sizeof(m_pCtx->m_Recent));

			m_pCtx->m_uTransactFail = 0;
			m_pCtx->m_dError	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskawaFSPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskawaFSPDriver::Ping(void)
{
	DWORD Data[1];

	m_fHasChecksum = TRUE;

	m_uWriteError  = 0;

	UINT uPoll = DoPoll(Data);

	if( uPoll == 1 || uPoll == 3 ) {

		DoPoll(Data); //  allow for change of checksum setting

//		for( UINT i = 0; i < 3; i++ ) {

//			AddWatchPoll();
//			Transact(FALSE);
//			if( m_pRx[1] == '3' ) break;
//			} // clear previous data commands

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CYaskawaFSPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	m_uWriteError = 0;

	Sleep(30);

	if( ReadNoTransmit( Addr.a.m_Table, pData ) ) {

		return 1;
		}

	DWORD Data;

	if( Addr.a.m_Table == TCMPOL ) {

		UINT uPollRet;

		for( UINT i = 0; i < 2; i++ ) {

			if( (uPollRet = DoPoll(&Data)) == 1 ) return 1;
			}

		return CCODE_ERROR;
		}

	if( DoRead(Addr, pData) == 1 ) {

		DoPoll(&Data);

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CYaskawaFSPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( WriteNoTransmit( Addr.a.m_Table, *pData ) ) {

		Sleep(30);

		return 1;
		}

//**/	AfxTrace0("\r\n ** WRITE ** ");

	DWORD d;

	DoPoll(&d);

	if( DoWrite(Addr, pData) == 1 ) {

		DoPoll(pData);

		return 1;
		}

	return CCODE_ERROR;
	}

// Process

CCODE CYaskawaFSPDriver::DoRead(AREF Addr, PDWORD pData)
{
	m_uBaseCommand = 0;

	UINT uRepeat   = 0;

	while( uRepeat < READREPEAT ) {

		uRepeat++;

		if ( AddRead(Addr) ) {

			if( Transact(FALSE) ) {

				m_pCtx->m_uTransactFail = 0;

//				switch( m_pTx[2] - '0' ) {

//					case MODEWATCHPOLL:
//					case MODEWATCH:
//					case MODECNTL:

//						GetWatchResponse(Addr, pData);
//						return 1;
//					}

				if( CheckReadResponse() ) {

					switch( GetReadResponse(Addr, pData) ) {

						case ERR_NONE:

							m_fSeqMismatch = FALSE;
							return 1;

						case ERR_NODATA:

							return NEXT_ITEM;

						case ERR_ERROR:
						default:

							return CCODE_ERROR;
						}
					}

//**/				else AfxTrace0("***");
				}

			if( ++m_pCtx->m_uTransactFail > 3 ) return CCODE_ERROR;

			if( uRepeat == READREPEAT/2 ) {
			
				DWORD Data;

				DoPoll(&Data);

				Sleep(100);
				}

			m_fSeqMismatch = TRUE;
			}

		else return CCODE_ERROR;
		}

//	DoReadWrite();

	m_fSeqMismatch = TRUE;

	return NEXT_ITEM;
	}

CCODE CYaskawaFSPDriver::DoWrite(AREF Addr, PDWORD pData)
{
	UINT uRepeat = 0;

	BYTE bSeq    = m_bSequence;

	while( uRepeat < WRITEREPEAT ) {

		m_bSequence = bSeq;

		uRepeat++;

		if( AddWrite(Addr, *pData) ) {

			if( Transact(FALSE) ) {

				if( GetWriteResponse() ) {

					m_uWriteError = 0;
					return 1;
					}

				else {
					if( (m_bRx[1] == '1') && GetValue( RSP_FNC, 1 ) ) return 1; // fault
					}
				}
			}
		}

	if( ++m_uWriteError > 1 ) return 1;

	return CCODE_ERROR;
	}

// Execute Polling
UINT CYaskawaFSPDriver::DoPoll(PDWORD pData)
{
	m_pTx	     = m_bPollTx;
	m_pRx	     = m_bPollRx;;

	StartFrame( MODEPOLL );

	AddByte(CMPOL);

	UINT uReturn = 0;

	if( Transact(TRUE) ) {

		switch( GetResponseStatus() ) {

			case 0:
			case 1:
			case 3:
				*pData         = GetValue( RSP_POLL, 2 ) & 0xFFFF;
				m_fHasChecksum = BOOL( GetValue(RSP_POLLCS, 1) );
				uReturn        = 1;
				break;

			case 2:

				memcpy(m_bRx, m_bPollRx, RXSIZE);
				uReturn = 2;
				break;

			default:
				uReturn = 3;
				break;
			}
		}

	m_pTx = m_bTx;
	m_pRx = m_bRx;

	return uReturn;
	}

BOOL CYaskawaFSPDriver::DoReadWrite(void)
{
	// A Write has been found to reduce the number of read retries.

	CAddress Addr;
	DWORD Data[1];

	Addr.a.m_Table  = TCMPARI;
	Addr.a.m_Offset = 1;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrWordAsWord;

	return (Read( Addr, Data, 1 ) == 1) && (Write( Addr, Data, 1 ) == 1);
	}

// Frame Building

void CYaskawaFSPDriver::StartFrame(BYTE bMode)
{
	m_uPtr   = 0;

	m_bCheck = 0;

	m_pTx[m_uPtr++] = 'N';

	AddByte( (m_pCtx->m_bDrop << 4) + (bMode & 0xF) );

	if( bMode == MODEPOLL ) AddByte(0);

	else {
		if( !m_fSeqMismatch ) {

			m_bSequence += 1;

			if( m_bSequence > 0xCF ) m_bSequence = 0xB5;
			}

		AddByte( m_bSequence );
		}
	}

void CYaskawaFSPDriver::EndFrame(void)
{
	m_bCheck = m_fHasChecksum ? 0x100 - m_bCheck : 0;

	AddByte( m_bCheck );

	m_pTx[m_uPtr++] = CR;
	}

void CYaskawaFSPDriver::AddByte(BYTE bData)
{
	m_bCheck += bData;

	if( m_uPtr < sizeof(m_bTx) ) {

		m_pTx[m_uPtr++] = m_pHex[bData/16];

		m_pTx[m_uPtr++] = m_pHex[bData%16];
		}
	}

void CYaskawaFSPDriver::AddWord(WORD wData)
{
	AddByte( HIBYTE( wData ) );
	AddByte( LOBYTE( wData ) );
	}

void CYaskawaFSPDriver::AddLong(DWORD dData)
{
	AddWord( HIWORD( dData ) );
	AddWord( LOWORD( dData ) );
	}

BOOL CYaskawaFSPDriver::AddRead(AREF Addr)
{
	UINT uOffset = Addr.a.m_Offset;
	UINT uCount  = 0;
	UINT i;

	StartFrame( MODEIMMEDIATE );

	switch( Addr.a.m_Table ) {

		case TCMGFAI: // Get From Array
			AddByte( OPGFA );
			AddByte( INTVAL );
			AddWord( uOffset );
			break;

		case TCMPARI: // Get Parameter
			AddByte( OPPARR );
			AddByte( INTVAL );
			AddWord( uOffset );
			break;

		case TCMVARI:
			AddByte( OPVARR );
			AddByte( INTVAL );
			AddByte( uOffset );
/*/
			StartFrame(MODECNTL);
			if( AddToWatch( Addr ) ) {
				AddByte( OPVARF );
				uCount  = GetWatchCount();
				AddByte( uCount );

				for( i = 0; i < 4; i++ ) {

					if( m_pCtx->m_Table[i] ) {

						AddWord( m_pCtx->m_Offset[i] );
						}
					}
				}

			else AddWatchPoll();
*/
			break;

		case TCMGTV: // Get Version
			AddByte( CMGTV );
			AddByte( INTVAL );
			break;

		default:
			return FALSE;
		}

	SetBaseCommand();

	return TRUE;
	}

BOOL CYaskawaFSPDriver::AddWrite( AREF Addr, DWORD dData )
{
	StartFrame( MODEIMMEDIATE );

	switch( Addr.a.m_Table ) {

		case TCMPARI:
		case TCMPARS:

			AddByte( OPPARW );
			AddByte( 0 );
			AddWord( Addr.a.m_Offset );
			AddWord( dData );
			break;

		case TCMVARI:
			AddByte( OPVARW );
			AddByte( HIBYTE(Addr.a.m_Offset) );
			AddByte( LOBYTE(Addr.a.m_Offset) );
			AddLong( dData );
			break;

		default:
			AddByte( GetOpcode(Addr.a.m_Table ) );
			AddByte( GetDataSource(Addr.a.m_Table, dData) );
			return AddWriteData( Addr.a.m_Table, dData );
		}

	return TRUE;
	}

BOOL CYaskawaFSPDriver::AddWriteData( UINT uTable, DWORD dData )
{
	UINT i;

	switch( uTable ) {
		// 0 Arguments
//		case TCMECT:
//		case TCMECZ:
//		case TCMEND:
//		case TCMSAV:
		case TCMSTA:
		case TCMSTM:
			return TRUE; // no data

		// 1 Argument
//		case TCMCLE:
		case TCMCON:
//		case TCMECR:
		case TCMRUN:
		case TCMSTP:
		case TCMSZA:
			AddByte( LOBYTE(dData) );
			return TRUE;

		case TCMGAI:
			AddWord( LOWORD(dData) );
			return TRUE;

		case TCMACC:
		case TCMJRK:
		case TCMSPD:
			AddLong( dData );
			return TRUE;

		case TCMRFA:
			if( dData ) {
				AddWord(LOWORD(m_dRFAI[0]));
				AddByte(LOBYTE(m_dRFAI[1]));
				return TRUE;
				}
			break;

		case TCMS1O:
			if( dData ) {
				AddWord(LOWORD(m_dS1OI[0]));
				AddByte(LOBYTE(m_dS1OI[1]));
				return TRUE;
				}
			break;

		case TCMSNO:
			if( dData ) {
				AddLong(m_dSNOI[0]);
				AddLong(m_dSNOI[1]);
				return TRUE;
				}
			break;

		case TCMSTX:
			if( dData ) {
				AddByte(LOBYTE(m_dSTXI[0]));
				AddByte(LOBYTE(m_dSTXI[1]));
				return TRUE;
				}
			break;

		case TCMTQL:
			if( dData ) {
				AddWord(LOWORD(m_dTQLI[0]));
				AddWord(LOWORD(m_dTQLI[1]));
				return TRUE;
				}
			break;

		case TCMWRI:
			if( dData ) {
				AddWord(LOWORD(m_dWRII[0]));
				AddLong(m_dWRII[1]);
				return TRUE;
				}
			break;

		// 3 Arguments
//		case TCMECS:
//			if( dData ) {
//				AddLong(m_dECSI[0]);
//				AddWord(LOWORD(m_dECSI[1]));
//				AddByte(LOBYTE(m_dECSI[2]));
//				return TRUE;
//				}
//			break;

//		case TCMFOS:
//			if( dData ) {
//				AddByte(LOBYTE(m_dFOSI[0]));
//				AddByte(LOBYTE(m_dFOSI[1]));
//				AddLong(m_dFOSI[2]);
//				return TRUE;
//				}
//			break;

		// up to 5 Arguments
//		case TCMECP:
//			if( dData ) {
//				AddByte(LOBYTE(m_dECPI[0]));

//				for( i = 1; i <= m_dECPI[0]; i++ ) {
	
//					AddWord(LOWORD(m_dECPI[i]));
//					}

//				return TRUE;
//				}
//			break;
		}

	return FALSE;
	}
/*
BOOL CYaskawaFSPDriver::AddToWatch(AREF Addr)
{
	CContext * p = m_pCtx;

	UINT uLeastRecent = 0;
	UINT uMaxRecent   = 0;
	UINT uFirstFree   = 99;

	for( UINT i = 0; i < 4; i++ ) {

		p->m_Recent[i]++;

		if( p->m_Table[i] == Addr.a.m_Table && p->m_Offset[i] == Addr.a.m_Offset ) {

			p->m_Recent[i] = 0;

			uFirstFree == 99;

//			return FALSE; // exists in watch list
			}

		if( !p->m_Table[i] ) {

			if( uFirstFree == 99 ) uFirstFree = i;
			}

		if( p->m_Recent[i] > uMaxRecent ) {

			uMaxRecent   = p->m_Recent[i];
			uLeastRecent = i;
			}
		}

	if( uFirstFree == 99 ) { // no empty spaces

		uFirstFree = uLeastRecent;
		}

	p->m_Table[uFirstFree]  = Addr.a.m_Table;
	p->m_Offset[uFirstFree] = Addr.a.m_Offset;
	p->m_Recent[uFirstFree] = 0;

	return TRUE; // new item added to watch list
	}

UINT CYaskawaFSPDriver::GetWatchCount(void)
{
	UINT uCount = 0;

	for( UINT i = 0; i < 4; i++ ) if( m_pCtx->m_Table[i] ) uCount++;

	return uCount;
	}

void CYaskawaFSPDriver::AddWatchPoll(void)
{
	m_pTx  = m_bTx;
	m_pRx  = m_bRx;

	StartFrame(MODEWATCHPOLL);
	}
*/
// Transport Layer

BOOL CYaskawaFSPDriver::Transact(BOOL fIsPoll)
{
//	Sleep(300);
	if( !fIsPoll ) Sleep( m_uSleepTime );

	else Sleep(100);

	Send();

	return GetReply();
	}

void CYaskawaFSPDriver::Send(void)
{
	m_pData->ClearRx();

	EndFrame();

	Put();
	}

BOOL CYaskawaFSPDriver::GetReply(void)
{
	m_uRcvCount = 0;

	UINT uTimer = 0;

	UINT uState = 0;

	UINT uData;

	BYTE bData;

	SetTimer( TIMEOUT );

//**/	AfxTrace0("\r\n"); if( m_pRx != &m_bRx[0] ) AfxTrace0("POLLR ");

	while( (uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData );

		switch( uState ) {

			case 0:
				switch( bData ) {

					case 'N':
						uState      = 1;
						m_uRcvCount = 0;
						break;

					case CR:
						return FALSE;

					default:
						BYTE bb;

						bb = xtoin( bData );

						if( bb == m_pCtx->m_bDrop ) {

							uState      = 1;
							m_uRcvCount = 1;
							m_pRx[0]    = bData;
							}

						break;
					}

				break;

			case 1:
				m_pRx[m_uRcvCount++] = bData;

//**/				if( m_uRcvCount == 2 || m_uRcvCount == 4 || m_uRcvCount == 6 ) AfxTrace0("_");

				if( bData == CR ) return CheckReply();

				break;
			}

		if( m_uRcvCount >= RXSIZE ) return FALSE;
		}	
 
	return FALSE;
	}

// Reply Verification
BOOL  CYaskawaFSPDriver::CheckReply(void)
{
	if( m_pCtx->m_bDrop && (xtoin(m_pRx[0]) != m_pCtx->m_bDrop) ) return FALSE;

	return VerifyChecksum();
	}

BOOL  CYaskawaFSPDriver::VerifyChecksum(void)
{
	if( !m_fHasChecksum ) return TRUE;

	BYTE bCheck   = MakeRxByte(0);

	BYTE bByte    = 0;

	BOOL fVersion = (m_pRx[RSP_FNC] == CMGTV);

	UINT i        = 2;

	while( m_pRx[i] != CR ) {				

		bByte = MakeRxByte(i);

		if( i == RSP_VERSION && fVersion ) {

			bCheck += m_pRx[6];
			bCheck += m_pRx[7];
			bCheck += m_pRx[8];
			bCheck += m_pRx[9];
			i = 8;
			}

		else bCheck += bByte;

		i += 2;
		}

	return !bCheck || (m_pRx[m_uRcvCount-3] == '0' && m_pRx[m_uRcvCount-2] == '0');
	}

// Handle Responses
UINT CYaskawaFSPDriver::GetReadResponse( AREF Addr, PDWORD pData )
{
	UINT uCheck = CheckErrorByte(Addr);

	if( uCheck & 0x3 ) return uCheck & 0x3;

	DWORD dData = 0;

	switch( m_uBaseCommand ) {

		case OPPARR:	dData = GetValue( RSP_PAR, 2 );		break;

		case OPGFA:	dData = GetValue( RSP_PAR, 4 );		break;

		case OPVARR:	dData = GetValue( RSP_TABLE, 4 );	break;

		case CMGTV:

			UINT i;

			for( i = RSP_VERSION; i < RSP_VERSION+4; i++ ) {

				dData <<= 8;

				dData += m_pRx[i];
				}

			break;

		default:
			return ERR_ERROR;
		}

	if( uCheck == ERR_SIZECK ) {

		if( !CheckLength( SetLength(m_uBaseCommand) ) ) return ERR_NODATA;
		}

	*pData = dData;

	return ERR_NONE;
	}

BOOL CYaskawaFSPDriver::GetWriteResponse( void )
{
	switch( GetResponseStatus() ) {

		case RSTAT_POLL:
		case RSTAT_WATCHVAR:

			return TRUE;

		case RSTAT_ERROR:

			m_pCtx->m_dError = GetValue( RSP_FNC, 1 ) +
					DWORD(xtoin(m_bTx[TX_FNCH])<<20) +
					DWORD(xtoin(m_bTx[TX_FNCL])<<16);

			break;
		}

	return FALSE;
	}
/*
void CYaskawaFSPDriver::GetWatchResponse(AREF Addr, PDWORD pData)
{
	UINT uCt    = GetValue( RSP_WATCHCT, 1 );

	UINT uPos   = RSP_WATCHDATA;

	CContext *p = m_pCtx;

	for( UINT i = 0; i < 4 && uCt; i++ ) {

		if( p->m_Table[i] ) {

			DWORD d = GetValue( uPos, 4 );

			p->m_Data[i] = d;

			if( p->m_Table[i] == Addr.a.m_Table && p->m_Offset[i] == Addr.a.m_Offset ) {

				*pData = d;
				}

			uCt--;

			uPos += 8;
			}
		}
	}
*/
// Response Verification
BOOL CYaskawaFSPDriver::CheckReadResponse(void)
{
	BYTE bS = MakeRxByte(RSP_SEQ);

	if( bS == m_bSequence ) {

		switch( GetResponseStatus() ) {

			case RSTAT_ERROR:

				if( GetValue(RSP_FNC, 1) == RSTAT_OVERRUN ) {

					m_uSleepTime = min( m_uSleepTime+10, 100 );
					}
				break;

			case RSTAT_READDATA:
				if( ThisCommandResponse() ) return TRUE;
				break;

			case RSTAT_POLL:
				return FALSE;
			}
		}

	if( IsThisFunction() ) {

		BYTE b = bS == 0xCF ? 0xB5 : bS + 1;

		if( m_bSequence == b ) {

			m_bSequence = bS;

			return TRUE;
			}
		}

	m_fSeqMismatch = TRUE;

	return FALSE;
	}

BOOL  CYaskawaFSPDriver::ThisCommandResponse(void)
{
	DWORD dData;

	if( IsThisFunction() ) return TRUE;

	m_fSeqMismatch = TRUE;

	return FALSE;
	}

BOOL  CYaskawaFSPDriver::IsThisFunction(void)
{
	switch( m_uBaseCommand ) {

		case OPPARR:
		case OPVARR:
		case OPGFA:

			if( MakeRxByte(RSP_FNC) != m_uBaseCommand ) return FALSE;

			UINT uSz = m_uBaseCommand == OPVARR ? 2 : 4;

			return !memcmp( &m_bRx[RSP_PARNO], &m_bTx[TX_PARNO], uSz );
		}

	return TRUE;
	}

UINT  CYaskawaFSPDriver::SetLength(UINT uCommand)
{
	switch( uCommand ) {

		case OPPARR:	return PAR_LEN;

		case OPGFA:	return ARR_LEN;

		case OPVARR:	return VAR_LEN;

		case CMGTV:	return VER_LEN;

		case CMPOL:	return POLL_LEN;
		}

	return 0;
	}

BOOL  CYaskawaFSPDriver::CheckLength(UINT uCheckVal)
{
	return m_uRcvCount == uCheckVal;
	}

UINT  CYaskawaFSPDriver::CheckErrorByte(AREF Addr)
{
	switch( GetResponseStatus() ) { // check for invalid responses

		case RSTAT_POLL:

			if( !(Addr.a.m_Table == addrNamed && Addr.a.m_Offset == CMPOL) ) return ERR_ERROR;

			return BOOL(!m_uBaseCommand) ? ERR_SIZECK : ERR_NONE;

		case RSTAT_ERROR:

			if( Addr.a.m_Table < addrNamed ) {

				if( MakeRxByte(RSP_FNC) == ERR_INVPAR ) return ERR_NODATA;
				}

			return ERR_ERROR;

		case RSTAT_READDATA:

			return ERR_SIZECK;

		case RSTAT_WATCHVAR:
		case RSTAT_UPLOAD:
		default:
			break;
		}

	return ERR_NODATA;
	}

// Port Access

void CYaskawaFSPDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); if( m_pRx != &m_bRx[0] ) AfxTrace0("POLLT ");
//**/	for( UINT i = 0; i < m_uPtr; i++ ) {

//**/		if( i == TX_FNCH || i == TX_FNCH+2 || i == TX_FNCH+4 || i == m_uPtr-3 ) {

//**/			AfxTrace0("_");
//**/			}

//**/		if( i == TX_SEQH ) AfxTrace0("_");

//**/		AfxTrace1("[%2.2x]", m_pTx[i] );
//**/		}

	m_pData->Write( PCBYTE(m_pTx), m_uPtr, FOREVER );
	}

UINT CYaskawaFSPDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

// Internal Access

BOOL CYaskawaFSPDriver::ReadNoTransmit(UINT uTable, PDWORD pData)
{
	if( GetCachedData(uTable, pData) ) {

		return TRUE;
		}

	switch( uTable ) {

		case TCMACC:
//		case TCMEND:
		case TCMGAI:
		case TCMJRK:
		case TCMRUN:
		case TCMS1O:
		case TCMSTA:
		case TCMSPD:
		case TCMSTP:
		case TCMTQL:
//		case TCMCLE:
//		case TCMSAV:
		case TCMSTM:
		case TCMSPC:
		case TCMSLN:
		case TCMTQA:
		case TCMSNO:
		case TCMWAI:
		case TCMWAV:
		case TCMGOA:
		case TCMMVA:
		case TCMSLD:
		case TCMTQE:
		case TCMGOH:
		case TCMMVH:
		case TCMMVR:
		case TCMECE:
		case TCMECD:
//		case TCMECT:
//		case TCMECR:
//		case TCMECS:
//		case TCMECP:
//		case TCMECZ:
		case TCMGOD:
		case TCMMVD:
		case TCMHSC:
		case TCMHAR:
		case TCMHMS:
		case TCMHMC:
		case TCMENG:
		case TCMDEL:
		case TCMWEX:
		case TCMWFS:
		case TCMWAS:
		case TCMREG:
		case TCMLAT:
		case TCMSTX:
		case TCMFOS:
		case TCMWRI:
		case TCMRFA:
			*pData = 0;
			return TRUE;

		case TCMERR:
			*pData = m_pCtx->m_dError;
			return TRUE;

		case TCMCON:
		case TCMSZA:
			*pData = RTNZERO;
			return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaFSPDriver::WriteNoTransmit(UINT uTable, DWORD dData)
{
	if( PutCachedData(uTable, dData) ) {

		return TRUE;
		}

	switch( uTable ) {

		case TCMPARI:
		case TCMVARI:
		case TCMACC:
		case TCMCON:
		case TCMGAI:
		case TCMJRK:
		case TCMRFA:
		case TCMRUN:
		case TCMS1O:
		case TCMSNO:
		case TCMSZA:
		case TCMSPD:
		case TCMSTA:
		case TCMSTP:
		case TCMSTX:
		case TCMSTM:
		case TCMTQL:
		case TCMWRI:
/* - 2 -
		case TCMPARS:
		case TCMVARS:
		case TCMACCS:
		case TCMCONS:
		case TCMDEL:
		case TCMECD:
		case TCMECE:
		case TCMENG:
		case TCMFOS:
		case TCMGAIS:
		case TCMGOA:
		case TCMGOD:
		case TCMGOH:
		case TCMHAR:
		case TCMHMC:
		case TCMHMS:
		case TCMHSC:
		case TCMLAT:
		case TCMMVA:
		case TCMMVD:
		case TCMMVH:
		case TCMMVR:
		case TCMRFAS:
		case TCMRFAS1:
		case TCMRFAS2:
		case TCMREG:
		case TCMRUNS:
		case TCMS1OS:
		case TCMSNOS:
		case TCMSZAS:
		case TCMSLD:
		case TCMSLN:
		case TCMSPDS:
		case TCMSPC:
		case TCMSTPS:
		case TCMSTXS:
		case TCMSTMS:
		case TCMTQE:
		case TCMTQA:
		case TCMWEX:
		case TCMWFS:
		case TCMWAI:
		case TCMWAS:
		case TCMWAV:
		case TCMWRIS:
- 2 - */
			return !dData;

		case TCMERR:	m_pCtx->m_dError = 0;
		case TCMGFAI:
// - 2 -	case TCMGFAS:
		case TCMPOL:
		case TCMGTV:	return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaFSPDriver::GetCachedData(UINT uTable, PDWORD pData)
{
	switch( uTable ) {

		case TCMRFA1:
		case TCMRFA2:
			*pData = m_dRFAI[uTable - TCMRFA1];
			return TRUE;

		case TCMS1O1:
		case TCMS1O2:
			*pData = m_dS1OI[uTable - TCMS1O1];
			return TRUE;

		case TCMSNO1:
		case TCMSNO2:
			*pData = m_dSNOI[uTable - TCMSNO1];
			return TRUE;

		case TCMSTX1:
		case TCMSTX2:
			*pData = m_dSTXI[uTable - TCMSTX1];
			return TRUE;

		case TCMTQL1:
		case TCMTQL2:
			*pData = m_dTQLI[uTable - TCMTQL1];
			return TRUE;

		case TCMWRI1:
		case TCMWRI2:
			*pData = m_dWRII[uTable - TCMWRI1];
			return TRUE;
/* - 2 -
		case TCMECE1:
		case TCMECE2:
			*pData = m_dECES[uTable - TCMECE1];
			return TRUE;

		case TCMENG1:
		case TCMENG2:
			*pData = m_dENGS[uTable - TCMENG1];
			return TRUE;

		case TCMFOS1:
		case TCMFOS2:
		case TCMFOS3:
			*pData = m_dFOSS[uTable - TCMFOS1];
			return TRUE;

		case TCMGOA1:
		case TCMGOA2:
			*pData = m_dGOAS[uTable - TCMGOA1];
			return TRUE;

		case TCMGOD1:
		case TCMGOD2:
			*pData = m_dGODS[uTable - TCMGOD1];
			return TRUE;

		case TCMHAR1:
		case TCMHAR2:
			*pData = m_dHARS[uTable - TCMHAR1];
			return TRUE;

		case TCMHMS1:
		case TCMHMS2:
			*pData = m_dHMSS[uTable - TCMHMS1];
			return TRUE;

		case TCMHSC1:
		case TCMHSC2:
			*pData = m_dHSCS[uTable - TCMHSC1];
			return TRUE;

		case TCMMVA1:
		case TCMMVA2:
			*pData = m_dMVAS[uTable - TCMMVA1];
			return TRUE;

		case TCMMVD1:
		case TCMMVD2:
			*pData = m_dMVDS[uTable - TCMMVD1];
			return TRUE;

		case TCMRFAS1:
		case TCMRFAS2:
			*pData = m_dRFAS[uTable - TCMRFAS1];
			return TRUE;

		case TCMS1OS1:
		case TCMS1OS2:
			*pData = m_dS1OS[uTable - TCMS1OS1];
			return TRUE;

		case TCMSNOS1:
		case TCMSNOS2:
			*pData = m_dSNOS[uTable - TCMSNOS1];
			return TRUE;

		case TCMSTXS1:
		case TCMSTXS2:
			*pData = m_dSTXS[uTable - TCMSTXS1];
			return TRUE;

		case TCMWAI1:
		case TCMWAI2:
		case TCMWAI3:
		case TCMWAI4:
			*pData = m_dWAIS[uTable - TCMWAI1];
			return TRUE;

		case TCMWAV1:
		case TCMWAV2:
		case TCMWAV2:
			*pData = m_dWAVS[uTable - TCMWAV1];
			return TRUE;

		case TCMWRIS1:
		case TCMWRIS2:
			*pData = m_dWRIS[uTable - TCMWRIS1];
			return TRUE;
- 2 - */
		}

	return FALSE;
	}

BOOL CYaskawaFSPDriver::PutCachedData(UINT uTable, DWORD dData)
{
	switch( uTable ) {

		case TCMRFA1:
		case TCMRFA2:
			m_dRFAI[uTable - TCMRFA1] = dData;
			return TRUE;

		case TCMS1O1:
		case TCMS1O2:
			m_dS1OI[uTable - TCMS1O1] = dData;
			return TRUE;

		case TCMSNO1:
		case TCMSNO2:
			m_dSNOI[uTable - TCMSNO1] = dData;
			return TRUE;

		case TCMSTX1:
		case TCMSTX2:
			m_dSTXI[uTable - TCMSTX1] = dData;
			return TRUE;

		case TCMTQL1:
		case TCMTQL2:
			m_dTQLI[uTable - TCMTQL1] = dData;
			return TRUE;

		case TCMWRI1:
		case TCMWRI2:
			m_dWRII[uTable - TCMWRI1] = dData;
			return TRUE;
/* - 2 -
		case TCMECE1:
		case TCMECE2:
			m_dECES[uTable - TCMECE1] = dData;
			return TRUE;

		case TCMENG1:
		case TCMENG2:
			m_dENGS[uTable - TCMENG1] = dData;
			return TRUE;

		case TCMFOS1:
		case TCMFOS2:
		case TCMFOS3:
			m_dFOSS[uTable - TCMFOS1] = dData;
			return TRUE;

		case TCMGOA1:
		case TCMGOA2:
			m_dGOAS[uTable - TCMGOA1] = dData;
			return TRUE;

		case TCMGOD1:
		case TCMGOD2:
			m_dGODS[uTable - TCMGOD1] = dData;
			return TRUE;

		case TCMHAR1:
		case TCMHAR2:
			m_dHARS[uTable - TCMHAR1] = dData;
			return TRUE;

		case TCMHMS1:
		case TCMHMS2:
			m_dHMSS[uTable - TCMHMS1] = dData;
			return TRUE;

		case TCMHSC1:
		case TCMHSC2:
			m_dHSCS[uTable - TCMHSC1] = dData;
			return TRUE;

		case TCMMVA1:
		case TCMMVA2:
			m_dMVAS[uTable - TCMMVA1] = dData;
			return TRUE;

		case TCMMVD1:
		case TCMMVD2:
			m_dMVDS[uTable - TCMMVD1] = dData;
			return TRUE;

		case TCMRFAS1:
		case TCMRFAS2:
			m_dRFAS[uTable - TCMRFAS1] = dData;
			return TRUE;

		case TCMS1OS1:
		case TCMS1OS2:
			m_dS1OS[uTable - TCMS1OS1] = dData;
			return TRUE;

		case TCMSNOS1:
		case TCMSNOS2:
			m_dSNOS[uTable - TCMSNOS1] = dData;
			return TRUE;

		case TCMSTXS1:
		case TCMSTXS2:
			m_dSTXS[uTable - TCMSTXS1] = dData;
			return TRUE;

		case TCMWAI1:
		case TCMWAI2:
		case TCMWAI3:
		case TCMWAI4:
			m_dWAIS[uTable - TCMWAI1] = dData;
			return TRUE;

		case TCMWAV1:
		case TCMWAV2:
		case TCMWAV2:
			m_dWAVS[uTable - TCMWAV1] = dData;
			return TRUE;

		case TCMWRIS1:
		case TCMWRIS2:
			m_dWRIS[uTable - TCMWRIS1] = dData;
			return TRUE;
- 2 - */
		}

	return FALSE;
	}

BYTE  CYaskawaFSPDriver::GetDataSource(UINT uTable, DWORD dData)
{
 	// Possible indirect assignments

	switch( uTable ) {
		// Immediate
		case TCMFOS:
			return( dData == 2 ? 4 : 0 );

		case TCMRFA:
		case TCMS1O:
			return( dData == 2 ? 1 : 0 );

		case TCMSNO:
		case TCMWRI:
			switch( dData ) {

				case 2:	return 1;

				case 3:	return 2;

				case 4:	return 3;

				case 1:
				default: break;
				}

			return 0;
		}

	return 0;
	}

// Response Data
DWORD CYaskawaFSPDriver::GetValue(UINT uPos, UINT uCount)
{
	DWORD dData = 0;

	for( UINT i = 0; i < uCount * 2; i += 2 ) {

		dData <<= 8;

		dData += MakeRxByte(uPos+i);
		}

	return dData;
	}

// Utilities
UINT  CYaskawaFSPDriver::xtoin( BYTE b )
{
	if( b >= '0' && b <= '9' ) return b - '0';

	if( b >= 'A' && b <= 'F' ) return b - '7';

	if( b >= 'a' && b <= 'f' ) return b - 'W';

	return b;
	}

BYTE  CYaskawaFSPDriver::MakeRxByte( UINT uRxPos )
{
	return (xtoin(m_pRx[uRxPos]) << 4) + xtoin(m_pRx[uRxPos+1]);
	}

BYTE  CYaskawaFSPDriver::GetResponseStatus(void)
{
	return m_pRx[RSP_STAT] & 0xF;
	}

void  CYaskawaFSPDriver::SetBaseCommand(void)
{
	if( !m_uBaseCommand ) {

		m_uBaseCommand = (xtoin(m_bTx[TX_FNCH]) << 4) + xtoin(m_bTx[TX_FNCL]);
		}
	}

UINT  CYaskawaFSPDriver::GetOpcode(UINT uTable)
{
	switch( uTable ) {

		case TCMPOL:	return CMPOL;	// 10
		case TCMACC:	return CMACC;	// 11
		case TCMCON:	return CMCON;	// 12
		case TCMGAI:	return CMGAI;	// 13
		case TCMGTV:	return CMGTV;	// 14
		case TCMJRK:	return CMJRK;	// 15
		case TCMRFA:			// 16
		case TCMRFA1:			// 17	// Cached
		case TCMRFA2:	return CMRFA;	// 18	// Cached
		case TCMRUN:	return CMRUN;	// 19
		case TCMS1O:			// 20
		case TCMS1O1:			// 21	// Cached
		case TCMS1O2:	return CMS1O;	// 22	// Cached
		case TCMSNO:			// 23
		case TCMSNO1:			// 24	// Cached
		case TCMSNO2:	return CMSNO;	// 25	// Cached
		case TCMSZA:	return CMSZA;	// 26
		case TCMSPD:	return CMSPD;	// 27
		case TCMSTA:	return CMSTA;	// 28
		case TCMSTP:	return CMSTP;	// 29
		case TCMSTX:			// 30
		case TCMSTX1:			// 31	// Cached
		case TCMSTX2:	return CMSTX;	// 32	// Cached
		case TCMSTM:	return CMSTM;	// 33
		case TCMTQL:			// 34
		case TCMTQL1:			// 35	// Cached
		case TCMTQL2:	return CMTQL;	// 36	// Cached
		case TCMWRI:			// 37
		case TCMWRI1:			// 38	// Cached
		case TCMWRI2:	return CMWRI;	// 39	// Cached
/* - 2 -
		case TCMACCS:	return CMACCS;	// 100
		case TCMCONS:	return CMCONS;	// 101
		case TCMDEL:	return CMDEL;	// 102
		case TCMECD:	return CMECD;	// 103
		case TCMECE:			// 104
		case TCMECE1:			// 105	// Cached
		case TCMECE2:	return CMECE;	// 106	// Cached
		case TCMENG:			// 107
		case TCMENG1:			// 108	// Cached
		case TCMENG2:	return CMENG;	// 109	// Cached
		case TCMFOS:			// 110
		case TCMFOS1:			// 111	// Cached
		case TCMFOS2:			// 112	// Cached
		case TCMFOS3:	return CMFOS;	// 113	// Cached
		case TCMGAIS:	return CMGAIS;	// 114
		case TCMGOA:			// 115
		case TCMGOA1:			// 116	// Cached
		case TCMGOA2:	return CMGOA;	// 117	// Cached
		case TCMGOD:			// 118
		case TCMGOD1:			// 119	// Cached
		case TCMGOD2:	return CMGOD;	// 120	// Cached
		case TCMGOH:	return CMGOH;	// 121
		case TCMHAR:			// 122
		case TCMHAR1:			// 123	// Cached
		case TCMHAR2:	return CMHAR;	// 124	// Cached
		case TCMHMC:	return CMHMC;	// 125
		case TCMHMS:			// 126
		case TCMHMS1:			// 127	// Cached
		case TCMHMS2:	return CMHMS;	// 128	// Cached
		case TCMHSC:			// 129
		case TCMHSC1:			// 130	// Cached
		case TCMHSC2:	return CMHSC;	// 131	// Cached
		case TCMJRKS:	return CMJRK	// 132
		case TCMLAT:	return CMLAT;	// 133
		case TCMMVA:			// 134
		case TCMMVA1:			// 135	// Cached
		case TCMMVA2:	return CMMVA;	// 136	// Cached
		case TCMMVD:			// 137
		case TCMMVD1:			// 138	// Cached
		case TCMMVD2:	return CMMVD;	// 139	// Cached
		case TCMMVH:	return CMMVH;	// 140
		case TCMMVR:	return CMMVR;	// 141
		case TCMRFAS:			// 142
		case TCMRFAS1:			// 143
		case TCMRFAS2:	return CMRFAS;	// 144
		case TCMREG:	return CMREG;	// 145
		case TCMRUNS:	return CMRUNS;	// 146
		case TCMS1OS:			// 147
		case TCMS1OS1:			// 148	// Cached
		case TCMS1OS2:	return CMS1OS;	// 149	// Cached
		case TCMSNOS:			// 150
		case TCMSNOS1:			// 151	// Cached
		case TCMSNOS2:	return CMSNOS;	// 152	// Cached
		case TCMSZAS:	return CMSZAS;	// 153
		case TCMSLD:	return CMSLD;	// 154
		case TCMSLN:	return CMSLN;	// 155
		case TCMSPDS:	return CMSPDS;	// 156
		case TCMSPC:	return CMSPC;	// 157
		case TCMSTPS:	return CMSTPS;	// 158
		case TCMSTXS:			// 159
		case TCMSTXS1:			// 160	// Cached
		case TCMSTXS2:	return CMSTXS;	// 161	// Cached
		case TCMSTMS:	return CMSTMS;	// 162
		case TCMTQE:	return CMTQE;	// 163
		case TCMTQA:	return CMTQA;	// 164
		case TCMWEX:	return CMWEX;	// 168
		case TCMWFS:	return CMWFS;	// 169
		case TCMWAI:			// 170
		case TCMWAI1:			// 171	// Cached
		case TCMWAI2:			// 172	// Cached
		case TCMWAI3:			// 173	// Cached
		case TCMWAI4:	return CMWAI;	// 174	// Cached
		case TCMWAS:	return CMWAS;	// 175
		case TCMWAV:			// 176
		case TCMWAV1:			// 177	// Cached
		case TCMWAV2:			// 178	// Cached
		case TCMWAV3:	return CMWAV;	// 179	// Cached
		case TCMWRIS:			// 180
		case TCMWRIS1:			// 181	// Cached
		case TCMWRIS2:	return CMWRIS;	// 182	// Cached
- 2 - */
		}

	return 0;
	}

// End of File

