
#include "intern.hpp"

#include "yetxtra.hpp"

// -0- Exclude code if indirect Variable needed
// -1- Include code if indirect Variable needed
// -2- Include code if Sequential Mode needed
// -3- Exclude code if Sequential Mode needed

//////////////////////////////////////////////////////////////////////////
//
// YET Xtra Drive Driver
//

// Constants

static UINT const TIMEOUT = 200;

// Constructor

CYETXtraDriver::CYETXtraDriver(void)
{
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uSequence = 1;

	m_fSeqMismatch = FALSE;
	}

// Destructor

CYETXtraDriver::~CYETXtraDriver(void)
{
	}

// Configuration

void MCALL CYETXtraDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CYETXtraDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CYETXtraDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CYETXtraDriver::Open(void)
{
	}

// Device

CCODE MCALL CYETXtraDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = LOBYTE(GetWord(pData));

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYETXtraDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYETXtraDriver::Ping(void)
{
	DWORD Data[1];

	CAddress Addr;

	m_uBaseCommand = 0;

	if( DoPoll(Data) == 1 ) {

		m_fHasChecksum = FALSE;

		Data[0] = 0;

		Addr.a.m_Table  = CMPARI;
		Addr.a.m_Offset = 0x2C6;
		Addr.a.m_Extra  = 0;

		if( Read(Addr, Data, 1) == 1 ) {

			m_fHasChecksum = Data[0] & 1;

			return 1;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CYETXtraDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( ReadNoTransmit( Addr, pData ) ) {

		Sleep(30);

		return 1;
		}

	if( Addr.a.m_Table == addrNamed && Addr.a.m_Offset == CMPOL ) {

		m_uBaseCommand = 0;

		CCODE c = DoPoll(pData);

		if( c != 1 ) {

			c = DoPoll(pData);
			}

		return c;
		}

	UINT uDone = 0;

	UINT uTimeoutCount = 0;

	while( ++uDone < 5 ) {

		m_uBaseCommand = 0;

		m_uPollCount   = 0;

		if ( AddRead(Addr) ) {

			if( Transact() ) {

				if( ThisCommandResponse() ) {

					switch( GetReadResponse(Addr, pData) ) {

						case ERR_NONE:
							m_fSeqMismatch = FALSE;

							return 1;

						case ERR_NODATA:
							break;

						case ERR_ERROR:
						default:

							return CCODE_ERROR;
						}
					}
				}

			else {
				if( ++uTimeoutCount > 1 ) {

					return CCODE_ERROR;
					}
				}
			}

		else {

			return 1;
			}
		}

	return NEXT_ITEM;
	}

CCODE MCALL CYETXtraDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( WriteNoTransmit( Addr, pData ) ) {

		Sleep(30);

		return 1;
		}

//	AfxTrace0("\r\n ** WRITE ** ");

	if( AddWrite(Addr, *pData) ) {

		if( Transact() ) {

			GetWriteResponse(); // set error code, if any

			return 1;
			}

		return CCODE_ERROR;
		}

	return 1;
	}

// PRIVATE METHODS

// Execute Polling
CCODE CYETXtraDriver::DoPoll(PDWORD pData)
{
	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = CMPOL;
	Addr.a.m_Type   = addrWordAsWord;

	UINT uDone          = 0;

	UINT uTimeoutCount = 0;

	while( ++uDone < 5 ) {

		StartFrame( MODEPOLL, TRUE );

		AddByte(CMPOL);

		if( Transact() ) {

			switch( GetReadResponse(Addr, pData) ) {

				case ERR_NONE:

					return 1;

				case ERR_NODATA:

					if( m_uBaseCommand && IsSeqAndFunc() ) {

						return 1;
						}
					break;

				case ERR_ERROR:
				default:
					Sleep(50);

					return CCODE_ERROR;
				}
			}

		else {
			if( m_uBaseCommand ) {

				return 1;
				}

			if( ++uTimeoutCount > 1 ) {

				Sleep(50);

				return CCODE_ERROR;
				}
			}
		}

	return NEXT_ITEM;
	}

// Frame Building

void CYETXtraDriver::StartFrame(BYTE bMode, BOOL fIsPoll)
{
	m_uPtr   = 0;

	m_bCheck = 0;

	m_bTx[m_uPtr++] = 'N';

	AddByte( (m_pCtx->m_bDrop << 4) + (bMode & 0xF) );

	if( fIsPoll ) {

		AddByte( 0 );
		}

	else {
		if( !m_fSeqMismatch ) {

			m_uSequence++;

			if( !LOBYTE(m_uSequence) ) {

				m_uSequence = 1;
				}
			}

		AddByte( m_uSequence );
		}
	}

void CYETXtraDriver::EndFrame(void)
{
	m_bCheck = 0x100 - m_bCheck;

	AddByte( m_bCheck );

	m_bTx[m_uPtr++] = CR;
	}

void CYETXtraDriver::AddByte(BYTE bData)
{
	m_bCheck += bData;

	m_bTx[m_uPtr++] = m_pHex[bData/16];

	m_bTx[m_uPtr++] = m_pHex[bData%16];
	}

void CYETXtraDriver::AddWord(WORD wData)
{
	AddByte( HIBYTE( wData ) );
	AddByte( LOBYTE( wData ) );
	}

void CYETXtraDriver::AddLong(DWORD dData)
{
	AddWord( HIWORD( dData ) );
	AddWord( LOWORD( dData ) );
	}

BOOL CYETXtraDriver::AddRead(AREF Addr)
{
	UINT uOffset = Addr.a.m_Offset;

	StartFrame( GetMode(uOffset, Addr.a.m_Table), FALSE );

	switch( Addr.a.m_Table ) {

		case CMGFAI: // Get From Array
// -2-		case CMGFAS:
			AddByte( OPGFA );
			AddByte( INTVAL );
			AddWord( uOffset );
			break;

		case CMPARI: // Get Parameter
// -2-		case CMPARS:
			AddByte( OPPARR );
			AddByte( INTVAL );
			AddWord( uOffset );
			break;

		case CMVARI:
// -2-		case CMVARS:  // Get Variable
			AddByte( OPVARR );
			AddByte( INTVAL );
			AddByte( uOffset );
			break;;

		case addrNamed: // Get Version
			AddByte( LOBYTE(uOffset) );
			AddByte( INTVAL );
			break;

		default:
			return FALSE;
		}

	if( !m_uBaseCommand ) {

		m_uBaseCommand = (xtoin(m_bTx[TX_FNCH]) << 4) + xtoin(m_bTx[TX_FNCL]);
		}

	return TRUE;
	}

BOOL CYETXtraDriver::AddWrite( AREF Addr, DWORD dData )
{
	StartFrame( GetMode(Addr.a.m_Offset, Addr.a.m_Table), FALSE );

	switch( Addr.a.m_Table ) {

		case CMPARI:
		case CMPARS:

			AddByte( OPPARW );
			AddByte( 0 );
			AddWord( Addr.a.m_Offset );
			AddWord( dData );
			break;

		case CMVARI:
			AddByte( OPVARW );
/* -0- */		AddByte( 0 );
// -1-			AddByte( dData == 2 ? 2 : 0 );
			AddByte( Addr.a.m_Offset );
			AddLong( dData );
			break;
/* -3-
		case CMVARS:
			AddByte( OPVARW );
-0-			AddByte( 0 );
-1-			AddByte( dData == 2 ? 2 : 0 );
			AddByte( Addr.a.m_Offset );
			AddLong( dData );
			break;
-3- */

		default:
			AddByte( Addr.a.m_Offset & 0xFF );
			AddByte( GetDataSource(Addr.a.m_Offset, dData) );
			return AddWriteData( Addr.a.m_Offset, dData );
		}

	return TRUE;
	}

BOOL CYETXtraDriver::AddWriteData( UINT uOffset, DWORD dData )
{
	UINT i;

	switch( uOffset ) {
		// 0 Arguments
		case CMECT:
		case CMECZ:
		case CMEND:
		case CMSAV:
		case CMSTA:
		case CMSTM:
/* -2-
		case CMECD+SEQOFF:
		case CMSLN+SEQOFF:
		case CMSTM+SEQOFF:
		case CMTQA+SEQOFF:
		case CMWFS+SEQOFF:
-2- */
			return TRUE; // no data

		// 1 Argument
		case CMCLE:
		case CMCON:
		case CMECR:
		case CMRUN:
		case CMSTP:
		case CMSZA:
/* -2-
		case CMCON+SEQOFF:
		case CMLAT+SEQOFF:
		case CMRUN+SEQOFF:
		case CMSPC+SEQOFF:
		case CMSTP+SEQOFF:
		case CMSZA+SEQOFF:
-2- */
			AddByte( LOBYTE(dData) );
			return TRUE;

		case CMGAI:
/* -2-
		case CMGAI+SEQOFF:
		case CMTQE+SEQOFF:
-2- */
			AddWord( LOWORD(dData) );
			return TRUE;

		case CMACC:
		case CMJRK:
		case CMSPD:
/* -2-
		case CMACC+SEQOFF:
		case CMDEL+SEQOFF:
		case CMGOH+SEQOFF:
		case CMHMC+SEQOFF:
		case CMJRK+SEQOFF:
		case CMMVH+SEQOFF:
		case CMMVR+SEQOFF:
		case CMSLD+SEQOFF:
		case CMREG+SEQOFF:
		case CMWAS+SEQOFF:
		case CMWEX+SEQOFF:
-2- */
			AddLong( dData );
			return TRUE;

/* -2-
		// 2 Arguments
		// Sequential
		case CMECE+SEQOFF:
			if( dData ) {
				AddByte(LOBYTE(m_dECES[0]));
				AddByte(LOBYTE(m_dECES[1]));
				return TRUE;
				}
			break;

		case CMENG+SEQOFF:
			if( dData ) {
				AddByte(LOBYTE(m_dENGS[0]));
				AddByte(LOBYTE(m_dENGS[1]));
				return TRUE;
				}
			break;

		case CMGOA+SEQOFF:
			if( dData ) {
				AddLong(m_dGOAS[0]);
				AddLong(m_dGOAS[1]);
				return TRUE;
				}
			break;

		case CMGOD+SEQOFF:
			if( dData ) {
				AddLong(m_dGODS[0]);
				AddLong(m_dGODS[1]);
				return TRUE;
				}
			break;

		case CMHAR+SEQOFF:
			if( dData ) {
				AddWord(LOWORD(m_dHARS[0]));
				AddLong(m_dHARS[1]);
				return TRUE;
				}
			break;

		case CMHMS+SEQOFF:
			if( dData ) {
				AddLong(m_dHMSS[0]);
				AddLong(m_dHMSS[1]);
				return TRUE;
				}
			break;

		case CMHSC+SEQOFF:
			if( dData ) {
				AddLong(m_dHSCS[0]);
				AddLong(m_dHSCS[1]);
				return TRUE;
				}
			break;

		case CMMVA+SEQOFF:
			if( dData ) {
				AddLong(m_dMVAS[0]);
				AddLong(m_dMVAS[1]);
				return TRUE;
				}
			break;

		case CMMVD+SEQOFF:
			if( dData ) {
				AddLong(m_dMVDS[0]);
				AddLong(m_dMVDS[1]);
				return TRUE;
				}
			break;

		case CMRFA+SEQOFF:
			if( dData ) {
				AddWord(LOWORD(m_dREAS[0]));
				AddByte(LOBYTE(m_dREAS[1]));
				return TRUE;
				}
			break;

		case CMS1O+SEQOFF:
			if( dData ) {
				AddWord(LOWORD(m_dS1OS[0]));
				AddByte(LOBYTE(m_dS1OS[1]));
				return TRUE;
				}
			break;

		case CMSNO+SEQOFF:
			if( dData ) {
				AddLong(m_dSNOS[0]);
				AddLong(m_dSNOS[1]);
				return TRUE;
				}
			break;

		case CMSTX+SEQOFF:
			if( dData ) {
				AddByte(LOBYTE(m_dSTXS[0]));
				AddByte(LOBYTE(m_dSTXS[1]));
				return TRUE;
				}
			break;

		case CMTQL+SEQOFF:
			if( dData ) {
				AddWord(LOWORD(m_dTQLS[0]));
				AddWord(LOWORD(m_dTQLS[1]));
				return TRUE;
				}
			break;

		case CMWRI+SEQOFF:
			if( dData ) {
				AddWord(LOWORD(m_dWRIS[0]));
				AddLong(m_dWRIS[1]);
				return TRUE;
				}
			break;
-2- */
		// Immediate
		case CMRFA:
			if( dData ) {
				AddWord(LOWORD(m_dREAI[0]));
				AddByte(LOBYTE(m_dREAI[1]));
				return TRUE;
				}
			break;

		case CMS1O:
			if( dData ) {
				AddWord(LOWORD(m_dS1OI[0]));
				AddByte(LOBYTE(m_dS1OI[1]));
				return TRUE;
				}
			break;

		case CMSNO:
			if( dData ) {
				AddLong(m_dSNOI[0]);
				AddLong(m_dSNOI[1]);
				return TRUE;
				}
			break;

		case CMSTX:
			if( dData ) {
				AddByte(LOBYTE(m_dSTXI[0]));
				AddByte(LOBYTE(m_dSTXI[1]));
				return TRUE;
				}
			break;

		case CMTQL:
			if( dData ) {
				AddWord(LOWORD(m_dTQLI[0]));
				AddWord(LOWORD(m_dTQLI[1]));
				return TRUE;
				}
			break;

		case CMWRI:
			if( dData ) {
				AddWord(LOWORD(m_dWRII[0]));
				AddLong(m_dWRII[1]);
				return TRUE;
				}
			break;

		// 3 Arguments
		case CMECS:
			if( dData ) {
				AddLong(m_dECSI[0]);
				AddWord(LOWORD(m_dECSI[1]));
				AddByte(LOBYTE(m_dECSI[2]));
				return TRUE;
				}
			break;

		case CMFOS:
			if( dData ) {
				AddByte(LOBYTE(m_dFOSI[0]));
				AddByte(LOBYTE(m_dFOSI[1]));
				AddLong(m_dFOSI[2]);
				return TRUE;
				}
			break;

/* -2-
		case CMFOS+SEQOFF:
			if( dData ) {
				AddByte(LOBYTE(m_dFOSS[0]));
				AddByte(LOBYTE(m_dFOSS[1]));
				AddLong(m_dFOSS[2]);
				return TRUE;
				}
			break;

		case CMWAV+SEQOFF:
			if( dData ) {
				AddByte(LOBYTE(m_dWAVS[0]));
				AddByte(LOBYTE(m_dWAVS[1]));
				AddLong(m_dWAVS[2]);
				return TRUE;
				}
			break;

		// 4 Arguments
		case CMWAI+SEQOFF:
			if( dData ) {
				AddByte(LOBYTE(m_dWAIS[0]));
				AddByte(LOBYTE(m_dWAIS[1]));
				AddByte(LOBYTE(m_dWAIS[2]));
				AddLong(m_dWAIS[3]);
				return TRUE;
				}
			break;
-2- */
		// up to 5 Arguments
		case CMECP:
			if( dData ) {
				AddByte(LOBYTE(m_dECPI[0]));

				for( i = 1; i <= m_dECPI[0]; i++ ) {
	
					AddWord(LOWORD(m_dECPI[i]));
					}

				return TRUE;
				}
			break;
		}

	return FALSE;
	}

// Transport Layer

BOOL CYETXtraDriver::Transact(void)
{
	Send();

	return GetReply();
	}

void CYETXtraDriver::Send(void)
{
	m_pData->ClearRx();

	EndFrame();

	Put();
	}

BOOL CYETXtraDriver::GetReply(void)
{
	m_uRcvCount = 0;

	UINT uTimer = 0;

	UINT uState = 0;

	UINT uData;

	BYTE bData;

	SetTimer( TIMEOUT );

//	AfxTrace0("\r\n");

	while( (uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

//		AfxTrace1("<%2.2x>", bData );

		switch( uState ) {

			case 0:
				switch( bData ) {

					case 'N':
						uState      = 1;
						m_uRcvCount = 0;
						break;

					case CR:
						return FALSE;

					default:
						BYTE bb;

						bb = xtoin( bData );

						if( bb == m_pCtx->m_bDrop ) {

							uState      = 1;
							m_uRcvCount = 1;
							m_bRx[0]    = bData;
							}

						break;
					}

				break;

			case 1:
				m_bRx[m_uRcvCount++] = bData;

				if( m_uRcvCount == 6 ) {

//					AfxTrace0("_");
					}

				if( bData == CR ) {

					return CheckReply();
					}
				break;
			}

		if( m_uRcvCount >= sizeof(m_bRx) ) {

			return FALSE;
			}
		}	
 
	return FALSE;
	}

UINT CYETXtraDriver::GetReadResponse( AREF Addr, PDWORD pData )
{
	UINT uCheck = CheckErrorByte(Addr);

	if( uCheck & 0x3 ) {

		return uCheck & 0x3;
		}

	DWORD dData = 0;

	switch( m_uBaseCommand ) {

		case OPPARR:	dData = GetValue( RSP_PAR, 2 );		break;

		case OPGFA:	dData = GetValue( RSP_PAR, 4 );		break;

		case OPVARR:	dData = GetValue( RSP_TABLE, 4 );	break;

		case CMGTV:

			UINT i;

			for( i = RSP_VERSION; i < RSP_VERSION+4; i++ ) {

				dData <<= 8;

				dData += m_bRx[i];
				}

			break;

		case CMPOL:

			if( GetValue( RSP_POLLCHECK, 1 ) ) {

				return ERR_ERROR;
				}

			dData = GetValue( RSP_POLL, 2 ) & 0xFFFF;

			break;

		default:
			return ERR_ERROR;
		}

	if( uCheck == ERR_SIZECK ) {

		if( !CheckLength( SetLength(m_uBaseCommand) ) ) {

//			switch( m_uBaseCommand ) { // dropped byte(s) in response
//				case OPPARR: AfxTrace0("\r\nPAR\r\n"); break;
//				case OPVARR: AfxTrace0("\r\nVAR VAR\r\n"); break;
//				case OPGFA : AfxTrace0("\r\nARR ARR ARR\r\n"); break;
//				case CMGTV : AfxTrace0("\r\nVER VER VER VER VER"); break;
//				case CMPOL : AfxTrace0("\r\nPOLL POLL POLL POLL POLL POLL"); break;
//				}

			return ERR_NODATA;
			}
		}

	*pData = dData;

	return ERR_NONE;
	}

BOOL CYETXtraDriver::GetWriteResponse( void )
{
	m_uStatus = GetValue( RSP_MSTATUS, 2 );

	switch( m_bRx[RSP_STAT] ) {

		case RSTAT_POLL:

			return TRUE;

		case RSTAT_ERROR:

			m_dError =	GetValue( RSP_FNC, 1 ) +
					DWORD(xtoin(m_bTx[TX_FNCH])<<20) +
					DWORD(xtoin(m_bTx[TX_FNCL])<<16);

			break;
		}

	return FALSE;
	}

// Port Access

void CYETXtraDriver::Put(void)
{
//	AfxTrace0("\r\n");
//	for( UINT i = 0; i < m_uPtr; i++ ) {

//		if( i == TX_FNCH || i == TX_FNCH+2 || i == TX_FNCH+4 || i == m_uPtr-3 ) {

//			AfxTrace0("_");
//			}

//		AfxTrace1("[%2.2x]", m_bTx[i] );
//		}

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );
	}

UINT CYETXtraDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

// Internal Access

BOOL CYETXtraDriver::ReadNoTransmit( AREF Addr, PDWORD pData )
{
	if( Addr.a.m_Table == addrNamed ) {

		UINT uCommand = Addr.a.m_Offset & (SEQOFF-1);

		if( uCommand < CP1 ) {

			switch( uCommand ) {

				case CMACC:
				case CMEND:
				case CMGAI:
				case CMJRK:
				case CMRUN:
				case CMS1O:
				case CMSTA:
				case CMSPD:
				case CMSTP:
				case CMTQL:
				case CMCLE:
				case CMSZA:
				case CMSAV:
				case CMSTM:
				case CMSPC:
				case CMSLN:
				case CMTQA:
				case CMSNO:
				case CMWAI:
				case CMWAV:
				case CMGOA:
				case CMMVA:
				case CMSLD:
				case CMTQE:
				case CMGOH:
				case CMMVH:
				case CMMVR:
				case CMECE:
				case CMECD:
				case CMECT:
				case CMECR:
				case CMECS:
				case CMECP:
				case CMECZ:
				case CMGOD:
				case CMMVD:
				case CMHSC:
				case CMHAR:
				case CMHMS:
				case CMHMC:
				case CMENG:
				case CMDEL:
				case CMWEX:
				case CMWFS:
				case CMWAS:
				case CMREG:
				case CMLAT:
				case CMSTX:
				case CMFOS:
				case CMWRI:
				case CMRFA:
					*pData = 0;
					return TRUE;

				case CMGTV: // Get Version
					return FALSE;

				case CMERR:
					*pData = m_dError;
					return TRUE;

				case CMCON:
					return TRUE;
				}
			}

		else {
			if( Addr.a.m_Offset != CMPOL ) {

				return GetCachedData( pData, Addr.a.m_Offset );
				}
			}
		}

	return FALSE;
	}

BOOL CYETXtraDriver::WriteNoTransmit( AREF Addr, PDWORD pData )
{
	switch( Addr.a.m_Table ) {

		case CMPARI:
// -2-		case CMPARS:
		case CMVARI:
// -2-		case CMVARS:
			return FALSE;

		case CMGFAI:
// -2-		case CMGFAS:
			return TRUE;
		}

	switch( Addr.a.m_Offset ) {

		case CMERR:
			m_dError = 0;
			return TRUE;

		case CMPOL:
		case CMGTV:
			return TRUE;
		}

	UINT uOffset = Addr.a.m_Offset & (SEQOFF - 1);

	if( uOffset < CP1 ) { // Command or data value

		return FALSE;
		}

	return PutCachedData( *pData, Addr.a.m_Offset );
	}

BOOL CYETXtraDriver::GetCachedData( PDWORD pData, UINT uCommand)
{
	UINT uCache = 0;

	UINT uCmd = uCommand & (SEQOFF-1);

	if( uCmd >= CP1 && uCmd < CP5+0x100 ) {

		uCache = (uCmd >> 8) - 1;

		uCmd &= 0xFF; // Base Command
/* -2-
		if( uCommand & SEQOFF ) { // Sequential

			switch( uCmd ) {

				case CMECE:
					*pData = m_dECES[uCache];
					return TRUE;

				case CMENG:
					*pData = m_dENGS[uCache];
					return TRUE;

				case CMFOS:
					*pData = m_dFOSS[uCache];
					return TRUE;

				case CMGOA:
					*pData = m_dGOAS[uCache];
					return TRUE;

				case CMGOD:
					*pData = m_dGODS[uCache];
					return TRUE;

				case CMHAR:
					*pData = m_dHARS[uCache];
					return TRUE;

				case CMHMS:
					*pData = m_dHMSS[uCache];
					return TRUE;

				case CMHSC:
					*pData = m_dHSCS[uCache];
					return TRUE;

				case CMMVA:
					*pData = m_dMVAS[uCache];
					return TRUE;

				case CMMVD:
					*pData = m_dMVDS[uCache];
					return TRUE;

				case CMRFA:
					*pData = m_dREAS[uCache];
					return TRUE;

				case CMS1O:
					*pData = m_dS1OS[uCache];
					return TRUE;

				case CMSNO:
					*pData = m_dSNOS[uCache];
					return TRUE;

				case CMSTX:
					*pData = m_dSTXS[uCache];
					return TRUE;

				case CMTQL:
					*pData = m_dTQLS[uCache];
					return TRUE;

				case CMWAI:
					*pData = m_dWAIS[uCache];
					return TRUE;

				case CMWAV:
					*pData = m_dWAVS[uCache];
					return TRUE;

				case CMWRI:
					*pData = m_dWRIS[uCache];
					return TRUE;
				}
			}

		else {
-2- */
			switch( uCmd ) {

				case CMECP:
					*pData = m_dECPI[uCache];
					return TRUE;

				case CMECS:
					*pData = m_dECSI[uCache];
					return TRUE;

				case CMFOS:
					*pData = m_dFOSI[uCache];
					return TRUE;

				case CMRFA:
					*pData = m_dREAI[uCache];
					return TRUE;

				case CMS1O:
					*pData = m_dS1OI[uCache];
					return TRUE;

				case CMSNO:
					*pData = m_dSNOI[uCache];
					return TRUE;

				case CMSTX:
					*pData = m_dSTXI[uCache];
					return TRUE;

				case CMTQL:
					*pData = m_dTQLI[uCache];
					return TRUE;

				case CMWRI:
					*pData = m_dWRII[uCache];
					return TRUE;
				}
// -2-			}
		}

	return FALSE;
	}

BOOL CYETXtraDriver::PutCachedData( DWORD dData, UINT uCommand)
{
	UINT uCache = 0;

	UINT uCmd = uCommand & (SEQOFF-1);

	if( uCmd >= CP1 && uCmd < CP5+0x100 ) {

		uCache = (uCmd >> 8) - 1;

		uCmd &= 0xFF; // Base Command
/* -2-
		if( uCommand & SEQOFF ) { // Sequential

			switch( uCmd ) {

				case CMECE:
					m_dECES[uCache] = dData;
					return TRUE;

				case CMENG:
					m_dENGS[uCache] = dData;
					return TRUE;

				case CMFOS:
					m_dFOSS[uCache] = dData;
					return TRUE;

				case CMGOA:
					m_dGOAS[uCache] = dData;
					return TRUE;

				case CMGOD:
					m_dGODS[uCache] = dData;
					return TRUE;

				case CMHAR:
					m_dHARS[uCache] = dData;
					return TRUE;

				case CMHMS:
					m_dHMSS[uCache] = dData;
					return TRUE;

				case CMHSC:
					m_dHSCS[uCache] = dData;
					return TRUE;

				case CMMVA:
					m_dMVAS[uCache] = dData;
					return TRUE;

				case CMMVD:
					m_dMVDS[uCache] = dData;
					return TRUE;

				case CMRFA:
					m_dREAS[uCache] = dData;
					return TRUE;

				case CMS1O:
					m_dS1OS[uCache] = dData;
					return TRUE;

				case CMSNO:
					m_dSNOS[uCache] = dData;
					return TRUE;

				case CMSTX:
					m_dSTXS[uCache] = dData;
					return TRUE;

				case CMTQL:
					m_dTQLS[uCache] = dData;
					return TRUE;

				case CMWAI:
					m_dWAIS[uCache] = dData;
					return TRUE;

				case CMWAV:
					m_dWAVS[uCache] = dData;
					return TRUE;

				case CMWRI:
					m_dWRIS[uCache] = dData;
					return TRUE;
				}
			}

		else {
-2- */
			switch( uCmd ) {

				case CMECP:
					m_dECPI[uCache] = dData;
					return TRUE;

				case CMECS:
					m_dECSI[uCache] = dData;
					return TRUE;

				case CMFOS:
					m_dFOSI[uCache] = dData;
					return TRUE;

				case CMRFA:
					m_dREAI[uCache] = dData;
					return TRUE;

				case CMS1O:
					m_dS1OI[uCache] = dData;
					return TRUE;

				case CMSNO:
					m_dSNOI[uCache] = dData;
					return TRUE;

				case CMSTX:
					m_dSTXI[uCache] = dData;
					return TRUE;

				case CMTQL:
					m_dTQLI[uCache] = dData;
					return TRUE;

				case CMWRI:
					m_dWRII[uCache] = dData;
					return TRUE;
				}
// -2-			}
		}

	return FALSE;
	}

BYTE  CYETXtraDriver::GetDataSource(UINT uOffset, DWORD dData)
{
 	// Possible indirect assignments

	switch( uOffset ) {
		// Immediate
		case CMFOS:
			return( dData == 2 ? 4 : 0 );

		case CMRFA:
		case CMS1O:
			return( dData == 2 ? 1 : 0 );

		case CMSNO:
		case CMWRI:
			switch( dData ) {

				case 2:	return 1;

				case 3:	return 2;

				case 4:	return 3;

				case 1:
				default: break;
				}

			return 0;

/* -2- // Must do return values based on actual data settings, similar to above
		// Sequential
		case CMDEL+SEQOFF:	return( 0 );

		case CMECE+SEQOFF:	return( 0 );

		case CMENG+SEQOFF:	return( 0 );

		case CMFOS+SEQOFF:	return( 0 );

		case CMGOA+SEQOFF:	return( 0 );

		case CMGOD+SEQOFF:	return( 0 );

		case CMGOH+SEQOFF:	return( 0 );

		case CMHAR+SEQOFF:	return( 0 );

		case CMHMC+SEQOFF:	return( 0 );

		case CMHMS+SEQOFF:	return( 0 );

		case CMHSC+SEQOFF:	return( 0 );

		case CMMVA+SEQOFF:	return( 0 );

		case CMMVD+SEQOFF:	return( 0 );

		case CMMVH+SEQOFF:	return( 0 );

		case CMMVR+SEQOFF:	return( 0 );

		case CMRFA+SEQOFF:	return( 0 );

		case CMREG+SEQOFF:	return( 0 );

		case CMS1O+SEQOFF:	return( 0 );

		case CMSNO+SEQOFF:	return( 0 );

		case CMSVA+SEQOFF:	return( 0 );

		case CMSLD+SEQOFF:	return( 0 );

		case CMTQE+SEQOFF:	return( 0 );

		case CMWEX+SEQOFF:	return( 0 );

		case CMWAI+SEQOFF:	return( 0 );

		case CMWAS+SEQOFF:	return( 0 );

		case CMWAV+SEQOFF:	return( 0 );

		case CMWRI+SEQOFF:	return( 0 );
-2- */
		}

	return 0;
	}

// Command Mode Check
BYTE  CYETXtraDriver::GetMode(UINT uOffset, UINT uTable)
{
// -3-
	return MODEIMMEDIATE;
// -3-
// -2-	if( uTable == addrNamed ) {

// -2-		return uOffset & SEQOFF ? MODESEQUENTIAL : MODEIMMEDIATE;
// -2-		}

// -2-	return uTable <= TABLEIMMHIGH ? MODEIMMEDIATE : MODESEQUENTIAL;
	}

// Response Data
DWORD CYETXtraDriver::GetValue(UINT uPos, UINT uCount)
{
	DWORD dData = 0;

	for( UINT i = 0; i < uCount * 2; i += 2 ) {

		dData <<= 8;

		dData += MakeRxByte(uPos+i);
		}

	return dData;
	}

// Reply Verification
BOOL  CYETXtraDriver::CheckReply(void)
{
	if( m_pCtx->m_bDrop && (xtoin(m_bRx[0]) != m_pCtx->m_bDrop) ) {

		return FALSE;
		}

	return VerifyChecksum();
	}

BOOL  CYETXtraDriver::VerifyChecksum(void)
{
	if( !m_fHasChecksum ) {

		return TRUE;
		}

	BYTE bCheck   = MakeRxByte(0);

	BYTE bByte    = 0;

	BOOL fVersion = (m_bRx[RSP_FNC] == CMGTV);

	UINT i        = 2;

	while( m_bRx[i] != CR ) {				

		bByte = MakeRxByte(i);

		if( i == RSP_VERSION && fVersion ) {

			bCheck += m_bRx[6];
			bCheck += m_bRx[7];
			bCheck += m_bRx[8];
			bCheck += m_bRx[9];
			i = 8;
			}

		else {
			bCheck += bByte;
			}

		i += 2;
		}

	return !bCheck || (m_bRx[m_uRcvCount-3] == '0' && m_bRx[m_uRcvCount-2] == '0');
	}

// Response Verification
BOOL  CYETXtraDriver::ThisCommandResponse(void)
{
	DWORD dData;

	while( m_uPollCount < 4 ) {

		if( IsSeqAndFunc() ) {

			return TRUE;
			}

		m_fSeqMismatch = TRUE;

		DoPoll(&dData);

		m_uPollCount++;
		}

	return FALSE;
	}

BOOL  CYETXtraDriver::IsSeqAndFunc(void)
{
	return MakeRxByte(RSP_SEQ) == m_uSequence && MakeRxByte(RSP_FNC) == m_uBaseCommand;
	}

UINT  CYETXtraDriver::SetLength(UINT uCommand)
{
	switch( uCommand ) {

		case OPPARR:	return PAR_LEN;

		case OPGFA:	return ARR_LEN;

		case OPVARR:	return VAR_LEN;

		case CMGTV:	return VER_LEN;

		case CMPOL:	return POLL_LEN;
		}

	return 0;
	}

BOOL  CYETXtraDriver::CheckLength(UINT uCheckVal)
{
	return m_uRcvCount == uCheckVal;
	}

UINT  CYETXtraDriver::CheckErrorByte(AREF Addr)
{
	switch( m_bRx[RSP_STAT] ) { // check for invalid responses

		case RSTAT_POLL:

			if( !(Addr.a.m_Table == addrNamed && Addr.a.m_Offset == CMPOL) ) {

				return ERR_ERROR;
				}

			return BOOL(!m_uBaseCommand) ? ERR_SIZECK : ERR_NONE;

		case RSTAT_ERROR:

			if( Addr.a.m_Table < addrNamed ) {

				if( MakeRxByte(RSP_FNC) == ERR_INVPAR ) {

					return ERR_NODATA;
					}
				}

			return ERR_ERROR;

		case RSTAT_READDATA:

			return ERR_SIZECK;

		case RSTAT_WATCHVAR:
		case RSTAT_UPLOAD:
		default:
			break;
		}

	return ERR_NODATA;
	}

// Utilities
UINT  CYETXtraDriver::xtoin( BYTE b )
{
	if( b >= '0' && b <= '9' ) {

		return b - '0';
		}

	if( b >= 'A' && b <= 'F' ) {

		return b - '7';
		}

	if( b >= 'a' && b <= 'f' ) {

		return b - 'W';
		}

	return b;
	}

BYTE  CYETXtraDriver::MakeRxByte( UINT uRxPos )
{
	return (xtoin(m_bRx[uRxPos]) << 4) + xtoin(m_bRx[uRxPos+1]);
	}

// End of File

