
#include "mitfx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Base TCP Driver
//

class CMitFx2TcpMasterDriver : public CMitFXMasterDriver
{
	public:
		// Constructor
		CMitFx2TcpMasterDriver(void);

		// Destructor
		~CMitFx2TcpMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:

		// Device Data
		struct CContext	
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			

			BYTE	m_bDrop;
			};

		// Data Members
		CContext * m_pCtx; 
		UINT	   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Implementation
		BOOL SendFrame(void);
		BOOL Transact(void);
		BOOL RecvFrame(void);
		BOOL CheckFrame(void);
		

	};
	

// End of File
