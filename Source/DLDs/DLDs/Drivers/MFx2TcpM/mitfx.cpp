
#include "intern.hpp"

#include "mitfx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX Series PLC Master Driver
//

// Constructor

CMitFXMasterDriver::CMitFXMasterDriver(void)
{
	m_Ident         = DRIVER_ID;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex; 
	}

// Destructor

CMitFXMasterDriver::~CMitFXMasterDriver(void)
{
	}

// Configuration

void MCALL CMitFXMasterDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CMitFXMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);		
	}
	
// Management

void MCALL CMitFXMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMitFXMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CMitFXMasterDriver::DeviceOpen(IDevice *pDevice)
{
	return 	CMasterDriver::DeviceOpen(pDevice);
	}

CCODE MCALL CMitFXMasterDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMitFXMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 'D';
	Addr.a.m_Offset = 0x0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CMitFXMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTarget = 0;

	UINT uAddr = Addr.a.m_Offset;

	BOOL  fSpec = FALSE;

	BOOL fSpecM = FALSE;

	BOOL fLongC = FALSE;

	switch( Addr.a.m_Table ) {

		case 'S':
			uTarget = 0x0000 + uAddr / 8;
			break;

		case 'X':
			uTarget = 0x0080 + uAddr / 8;
			break;

		case 'Y':
			uTarget = 0x00A0 + uAddr / 8;
			break;

		case 'M':
			uTarget = 0x0100 + uAddr / 8;
			break;
		
		case 'T':
			uTarget = 0x0800 + 2 * uAddr;
			break;
		
		case 'C':
			uTarget = 0x0A00 + 2 * uAddr;
			break;

		case 'D':
			if( uAddr >= 8000 && uAddr <= 8255 ) {

				uTarget = 0x0E00 + 2 * (uAddr - 8000);
				} 

			else if ( uAddr < 1000 ) {

				uTarget = 0x1000 + 2 * uAddr;
				}

			else {
				uTarget = 0x4000 + 2 * uAddr;

				fSpec = TRUE;
				}
			break;

		case 'B':
		       
			uTarget = 0xE00 + uAddr;

			fSpecM = TRUE;

			break;

		case 'L':

			uTarget = uAddr - 200;

			fLongC = TRUE;
					
			break;
				
		default:
			return uCount;
		}

	if( fSpecM ) {

		MakeMin(uCount, 1);

		if( RegisterRelay(uTarget, uCount) ) {

			if( TriggerRead(uCount * 8) ) { 

				for( UINT u = 0; u < uCount; u++ ) {

					WORD x = xtoin(PCSTR(m_bRxBuff + u * 4), 4);

					SwapBytes(x);
					
					pData[u] = x;
					}

				End();

				return uCount;
				}

			End();
			}

		return CCODE_ERROR;
		}

	if( fLongC ) {

		MakeMin(uCount, 1);

		if( RegisterCounter(uTarget, uCount) ) {

			if( TriggerRead(uCount * 5) ) {

				for( UINT i = 0, u = 0; u < uCount * 2; i++, u++ ) {

					WORD Lo = xtoin(PCSTR(m_bRxBuff + u * 4), 4);

					SwapBytes(Lo);

					u++;

					WORD Hi = xtoin(PCSTR(m_bRxBuff + u * 4), 4);

					SwapBytes(Hi);

					pData[i] = MAKELONG(Lo, Hi);
					}
				
				End();

				return uCount;
				}

			End();
			}

		return CCODE_ERROR;
		}
	
	NewPacket();

	if( fSpec ) {

		AddByte('E');

		AddByte('0');
		}
	
	AddByte('0');

	AddHex(uTarget, 0x1000);

	switch( Addr.a.m_Type ) {

		case addrBitAsWord:
		case addrWordAsWord:

			return ReadWord(pData, uCount);

		case addrBitAsLong:
		case addrWordAsLong:
		
			return ReadLong(pData, uCount);

		default:

			return CCODE_ERROR;
		}
	}

CCODE MCALL CMitFXMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTarget = 0;

	UINT uAddr = Addr.a.m_Offset;

	MakeMin(uCount, 32);

	BOOL fSpec = FALSE;

	BOOL fSpecM = FALSE;

	BOOL fLongC = FALSE;
	
	switch( Addr.a.m_Table ) {

		case 'S':
			uTarget = 0x0000 + uAddr / 8;
			break;

		case 'Y':
			uTarget = 0x00A0 + uAddr / 8;
			break;
		
		case 'M':
			uTarget = 0x0100 + uAddr / 8;
			break;

		case 'T':
			uTarget = 0x0800 + 2 * uAddr;
			break;
		
		case 'C':
			uTarget = 0x0A00 + 2 * uAddr;
			break;
		
		case 'D':
			if( uAddr >= 8000 && uAddr <= 8255 ) {

				uTarget = 0x0E00 + 2 * (uAddr - 8000);
				} 
			
			else if ( uAddr < 1000 ) {

				uTarget = 0x1000 + 2 * uAddr;
				}
			
			else {
				uTarget = 0x4000 + 2 * uAddr;  

				fSpec = TRUE;
				}
			break;

		case 'B':

			uTarget = 0x0E00 + uAddr;

			fSpecM = TRUE;

			break;

		case 'L':

			uTarget = 0x400 + (4 * (uAddr - 200) );

			fLongC = TRUE;

			break;

			
		default:
			return uCount;
		}

	if( fSpecM ) {

		MakeMin(uCount, 1);

		for( UINT b = 0, m = 1; b < 16; b++, m = m << 1 ) {

			NewPacket();

			AddByte('E');

			AddByte( pData[0] & m ? '7' : '8' );

			AddHex((uTarget + b) % 256, 0x10);

			AddHex((uTarget + b) / 256, 0x10);

			if( !Transact() ) {

				return CCODE_ERROR;
				}
			}
			
		return uCount;
		}

	if( fLongC ) {

		MakeMin(uCount, 1);

		NewPacket();

		AddByte( 'E' );
		
		AddHex( 0x100C, 0x1000 );
		
		AddHex( uTarget % 256, 0x10 );
		
		AddHex( uTarget / 256, 0x10 );
		
		AddHex( LOWORD(pData[0]) % 256, 0x10 );

		AddHex( LOWORD(pData[0]) / 256, 0x10 );
		
		AddHex( HIWORD(pData[0]) % 256, 0x10 );
		
		AddHex( HIWORD(pData[0]) / 256, 0x10 );

		if( Transact() ) {

			return uCount;
			}

		return CCODE_ERROR;
		}
		
	NewPacket();

	if( fSpec ) {

		AddByte('E');

		AddByte('1');

		AddByte('0');
		}
	else {
	
		AddByte('1');
		}

	AddHex(uTarget, 0x1000);

	switch( Addr.a.m_Type ) {

		case addrBitAsWord:
		case addrWordAsWord:

			return WriteWord(pData, uCount);

		case addrBitAsLong:
		case addrWordAsLong:
			
			return WriteLong(pData, uCount);

		default:

			return CCODE_ERROR;
		}
	}

// Implementation

void CMitFXMasterDriver::NewPacket(void)
{
	m_uPtr = 0;

	AddByte(STX);
	}
	
void CMitFXMasterDriver::AddHex(WORD wData, UINT uMask)
{
	while( uMask ) {
	
		WORD wIndex = (wData / uMask) % 16;
	
		AddByte(m_pHex[wIndex]);

		uMask >>= 4;
		}
	}

void CMitFXMasterDriver::AddByte(BYTE bNew)
{
	m_bTxBuff[m_uPtr++] = bNew;
	}

void CMitFXMasterDriver::SwapBytes(WORD &wSwap)
{
	BYTE bHi = HIBYTE(wSwap);

	BYTE bLo = LOBYTE(wSwap);

	wSwap = MAKEWORD(bHi, bLo);
	}

void CMitFXMasterDriver::AddWord(WORD wNew)
{
	SwapBytes(wNew);
	
	AddHex(wNew, 0x1000);
	
	}

void CMitFXMasterDriver::AddLong(DWORD dwNew)
{
	AddWord(LOWORD(dwNew));

	AddWord(HIWORD(dwNew));
      
	}

BOOL CMitFXMasterDriver::Transact(void)
{
	return TxPacket() && RxPacket();
	}

BOOL CMitFXMasterDriver::TxPacket(void)
{
	AddByte(ETX);

	BYTE bCheck = 0;
	
	for( UINT i = 1; i < m_uPtr; i++ ) {
	
		bCheck += m_bTxBuff[i];

		}

	AddByte(m_pHex[bCheck / 16]);

	AddByte(m_pHex[bCheck % 16]);

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CMitFXMasterDriver::RxPacket(void)
{
	BYTE bCheck = 0;

	BOOL fOkay = FALSE;

	UINT uState = 0;

	UINT uTimer = 0;

	UINT uByte = 0;
	
	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == NAK )
					return FALSE;
				
				if( uByte == ACK )
					return TRUE;
					
				if( uByte == STX ) {
					uState = 1;
					bCheck = 0;
					m_uPtr   = 0;
					}
				break;
				
			case 1:
				bCheck += uByte;
				
				if( uByte == ETX ) {
					fOkay  = TRUE;
					uState = 2;
					}
				else {
					if( m_uPtr < sizeof(m_bRxBuff) ) {

						m_bRxBuff[m_uPtr] = uByte;

						m_uPtr++;
						}
					else
						uState = 0;
					}
				break;
				
			case 2:
				if( uByte != (UINT)m_pHex[bCheck / 16] )
					fOkay = FALSE;
				
				uState = 3;
				break;
				
			case 3:
				if( uByte != (UINT)m_pHex[bCheck % 16] )
					fOkay = FALSE;
				
				return fOkay;
			}
		}
	
	return FALSE;
	}

WORD CMitFXMasterDriver::xtoin(PCTXT pText, UINT uCount)
{
	WORD wData = 0;
	
	while( uCount-- ) {
	
		char cData = *(pText++);
		
		if( cData >= '0' && cData <= '9' )
			wData = 16 * wData + cData - '0';

		else if( cData >= 'A' && cData <= 'F' )
			wData = 16 * wData + cData - 'A' + 10;

		else if( cData >= 'a' && cData <= 'f' )
			wData = 16 * wData + cData - 'a' + 10;

		else break;

		}
		
	return wData;
	}

CCODE CMitFXMasterDriver::ReadWord(PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 32);

	AddHex(uCount * sizeof(WORD), 0x10);

	if( !Transact() ) {

		return CCODE_ERROR;
		}

	for( UINT i = 0; i < uCount; i++ ) {

		WORD x = xtoin(PCSTR(m_bRxBuff + i * 4), 4);

		SwapBytes(x);

		pData[i] = x;
		}
		
	return uCount;
	}

CCODE CMitFXMasterDriver::ReadLong(PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 16);

	AddHex(uCount * sizeof(DWORD), 0x10);

	if( !Transact() ) {

		return CCODE_ERROR;
		}
		
	for( UINT i = 0, u = 0; u < uCount * 2; u++, i++ ) {

		WORD Lo = xtoin(PCSTR(m_bRxBuff + u * 4), 4);

		SwapBytes(Lo);

		u++;

		WORD Hi = xtoin(PCSTR(m_bRxBuff + u * 4), 4);

		SwapBytes(Hi);
		
		pData[i] = MAKELONG(Lo, Hi);
		}
		
	return uCount;
	}

CCODE CMitFXMasterDriver::WriteWord(PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 32);

	AddHex(uCount * sizeof(WORD), 0x10);
	
	for( UINT i = 0; i < uCount; i++ ) {
		
		WORD wData = pData[i];

		AddWord(wData);
		}

	if( !Transact() ) {

		return CCODE_ERROR;
		}
		
	return uCount;
	}

CCODE CMitFXMasterDriver::WriteLong(PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 16);

	AddHex(uCount * sizeof(DWORD), 0x10);
	
	for( UINT i = 0; i < uCount; i++ ) {
		
		DWORD dwData = pData[i];

		AddLong(dwData);
		}

	if( !Transact() ) {

		return CCODE_ERROR;
		}
		
	return uCount;
	}

BOOL CMitFXMasterDriver::RegisterRelay(UINT uTarget, UINT uCount)
{
	Begin();

	AddHex(4 + uCount * 16 * 2, 0x10);

	AddByte('0');

	AddByte('0');

	AddByte('0');

	AddByte('0');

	AddHex(uCount * 16, 0x10);

	AddByte('0');

	AddByte('0');

	for(UINT x = 0; x < uCount * 16; x++ ) {

		AddHex( (uTarget + x) % 256, 0x10 );

		AddHex( (uTarget + x) / 256, 0x10 );
		}

	return Confirm();
	}

BOOL CMitFXMasterDriver::RegisterCounter(UINT uTarget, UINT uCount)
{
	Begin();

	AddHex(4 + uCount * 4, 0x10);

	AddByte('0');

	AddByte('1');

	AddByte('0');

	AddByte('0');

	AddHex(uCount, 0x10);

	AddByte('0');

	AddByte('0');

	AddHex( 4 * uTarget, 0x10 );

	AddHex( 0xC, 0x10 );

	AddHex( (uTarget + 200) % 256, 0x10 );

	AddHex( 0xF, 0x10 );

	return Confirm();
	}
 
BOOL CMitFXMasterDriver::TriggerRead(UINT uCount)
{
	NewPacket();

	AddByte('E');

	AddByte('0');

	AddByte('0');

	AddByte('1');

	AddByte('7');

	AddByte('9');

	AddByte('0');

	AddHex( uCount, 0x10 );

	return Transact();
	}

BOOL CMitFXMasterDriver::Confirm(void)
{
	if( Transact() ) {

		NewPacket();

		AddByte('E');

		AddByte('1');

		AddByte('0');

		AddByte('1');

		AddByte('4');

		AddByte('0');

		AddByte('1');

		AddByte('0'); 

		AddByte('1');

		AddByte('8');

		AddByte('1');

		return Transact();
		}

	return FALSE;
	}

BOOL CMitFXMasterDriver::Begin(void)
{
	NewPacket();

	AddByte('E');

	AddByte('1');

	AddByte('0');

	AddByte('1');

	AddByte('4');

	AddByte('0');

	AddByte('0');

	return TRUE;
	}

BOOL CMitFXMasterDriver::End(void)
{
	NewPacket();

	AddByte('E');

	AddByte('1');

	AddByte('0');

	AddByte('1');

	AddByte('4');

	AddByte('0');

	AddByte('1');

	AddByte('0');

	AddByte('1');

	AddByte('0');

	AddByte('0');

	return Transact();
	}

// End of File
