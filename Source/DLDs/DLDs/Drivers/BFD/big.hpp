
//////////////////////////////////////////////////////////////////////////
//
// Shared Files
//

#pragma pack(1)

#include "comms.h"

#pragma pack()

////////////////////////////////////////////////////////////////////////
//
// Big Display Driver
//

class CBigDisplayDriver : public CDisplayDriver 
{
	public:
		// Constructor
		CBigDisplayDriver(void);

		// Destructor
		~CBigDisplayDriver(void);

		// Destruction
		DEFMETH(UINT) Release(void);

		// Configuration
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Entry Points
		DEFMETH(UINT) GetSize(int cx, int cy);
		DEFMETH(void) Capture(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait);
		DEFMETH(void) Service(UINT uDrop, PBYTE pPrev, PBYTE pData, UINT uSize);

	protected:
		// Data
		UINT		 m_uDelay;
		BYTE		 m_bSend[64];
		UINT		 m_uPtr;
		CFrame	       * m_pFrame;
		UINT		 m_uForce[10];
		UINT		 m_uCount;
		UINT		 m_uType;
		IDisplayHelper * m_pDisplayHelper;

		// Commands
		BOOL Load(  UINT uDrop, PCBYTE pData, UINT uSize);
		BOOL Update(UINT uDrop);
		BOOL Who(   UINT uDrop);

		// Frame Building
		void NewFrame(BYTE bDrop, BYTE bCode);

		// Transport
		BOOL Transact(void);
		BOOL PutFrame(void);
		BOOL GetAck(void);

		// Implmentaiton
		void GetHelp(void);
		void CaptureVGA (PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize);
		void CaptureMono(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize);
	};

// End of File
