
#include "intern.hpp"

#include "big.hpp"

////////////////////////////////////////////////////////////////////////
//
// Big Display Driver
//

// Instantiator

INSTANTIATE(CBigDisplayDriver);

// Constructor

CBigDisplayDriver::CBigDisplayDriver(void)
{
	m_Ident  = DRIVER_ID;

	m_pFrame = (CFrame *) (m_bSend + 2);

	m_uDelay = 25;

	m_uCount = 32;

	m_pDisplayHelper = NULL;
	}

// Destructor

CBigDisplayDriver::~CBigDisplayDriver(void)
{
	m_pDisplayHelper = NULL;
	}

// Destruction

UINT MCALL CBigDisplayDriver::Release(void)
{
	delete this;

	return 0;
	}

// Configuration
	
void MCALL CBigDisplayDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, FALSE);

	switch( Config.m_uBaudRate ) {

		case 19200:
			m_uDelay = 25;
			break;

		case 115200:
			m_uDelay = 10;
			break;
		}

	Config.m_uDataBits = 9;

	Config.m_uParity   = 0;
	}

// Management

void MCALL CBigDisplayDriver::Attach(IPortObject *pPort)
{
	if( pPort ) {

		m_pData = MakeSingleDataHandler();

		pPort->Bind(m_pData);
		}
	}

void MCALL CBigDisplayDriver::Detach(void)
{
	if( m_pData ) {
	
		m_pData->Release();

		m_pData = NULL;
		}
	}

void MCALL CBigDisplayDriver::Open(void)
{
	GetHelp();
	
	for( UINT n = 0; n < elements(m_uForce); n ++ ) {

		m_uForce[n] = 0;
		}
	}

// Entry Points

UINT MCALL CBigDisplayDriver::GetSize(int cx, int cy)
{
	return (cx * cy + 7) / 8;
	}

void MCALL CBigDisplayDriver::Capture(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait)
{
	switch( m_uType ) {

		case 0:

			CaptureMono(pDest, pSrc, px, py, cx, cy, uSize);

			break;

		case 1 :

			CaptureVGA (pDest, pSrc, px, py, cx, cy, uSize);

			break;
		}
	}

void MCALL CBigDisplayDriver::Service(UINT uDrop, PBYTE pPrev, PBYTE pData, UINT uSize)
{
	if( uDrop < elements(m_uForce) ) {
	
		for( UINT n = 0; n < m_uCount; n ++ ) {

			if( n == m_uForce[uDrop] || memcmp(pData, pPrev, 32) ) {

				Load(n | (uDrop << 5), pData, 32);
				}

			pData += 32;

			pPrev += 32;
			}
		
		Update(0xFF);

		m_uForce[uDrop] = (m_uForce[uDrop] + 1) % m_uCount;
		}
	}

// Commands

BOOL CBigDisplayDriver::Load(UINT uDrop, PCBYTE pData, UINT uSize)
{
	NewFrame(uDrop, opLoad);

	memcpy(m_pFrame->x.bData, pData, uSize);

	m_uPtr += uSize;

	return Transact();
	}

BOOL CBigDisplayDriver::Update(UINT uDrop)
{
	NewFrame(uDrop, opUpdate);

	return Transact();
	}

BOOL CBigDisplayDriver::Who(UINT uDrop)
{
	NewFrame(uDrop, opIdent);

	return Transact();
	}

// Frame Building

void CBigDisplayDriver::NewFrame(BYTE bDrop, BYTE bCode)
{
	m_bSend[0] = bDrop;

	m_bSend[1] = 0;
	
	m_uPtr     = sizeof(CHeader);

	m_pFrame->Header.m_bCode = bCode;
	}

// Transport

BOOL CBigDisplayDriver::Transact(void)
{
	m_pData->ClearRx();

	return PutFrame() && GetAck();
	}

BOOL CBigDisplayDriver::PutFrame(void)
{
	BYTE bCheck = 0xAA;

	for( UINT n = 0; n < m_uPtr; n++ ) {

		bCheck += m_bSend[2+n];
		}

	m_bSend[1  ] = BYTE(m_uPtr);

	m_bSend[2+n] = bCheck;

	return m_pData->Write(m_bSend, 3+n, FOREVER);
	}

BOOL CBigDisplayDriver::GetAck(void)
{
	BOOL fNoAck = TRUE;
	
	if( fNoAck ) {

		Sleep(m_uDelay);
		
		return TRUE;
		}
	
	if( m_bSend[0] == 0xFF ) {

		Sleep(m_uDelay);

		return TRUE;
		}

	for(;;) {

		switch( m_pData->Read(m_uDelay) ) {

			case ACK:
				return TRUE;

			case NOTHING:
				return FALSE;
			}
		}
	}

// Implementation

void CBigDisplayDriver::GetHelp(void)
{
	if( !m_pDisplayHelper ) {
		
		if( MoreHelp(IDH_DISPLAY, (void **) &m_pDisplayHelper) ) {

			switch( m_pDisplayHelper->GetType() ) {

				case 0:
					m_uType = 0;
					break;

				case 1:
				case 2:
				case 3:
					m_uType = 1;
					break;
				}
			}
		}
	}

void CBigDisplayDriver::CaptureVGA(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize)
{
	pSrc += ((py) * uSize) + px;

	int ds = (uSize - 8) * 16;

	for( int r = 0; r < cy / 16; r ++ ) {

		for( int i = 0; i < cx / 16; i ++ ) {

			PBYTE   pd0 = pDest;

			pDest += 16;

			PBYTE   pd1 = pDest;

			pDest += 16;

			for( int n = 0; n < 16; n ++ ) {

				PCBYTE ps0 = pSrc + 0;

				PCBYTE ps1 = pSrc + (uSize * 8);

				*pd0 = 0;

				*pd1 = 0;

				for( BYTE bMask = 0x01; bMask; bMask <<= 1 ) {
					
					if( *ps0 )
						*pd0 |= bMask;			
					
					if( *ps1 )
						*pd1 |= bMask;			

					ps0 += uSize;

					ps1 += uSize;
					}

				pd0 ++;

				pd1 ++;

				pSrc ++;
				}
			}

		pSrc += ds;
		}
	}

void CBigDisplayDriver::CaptureMono(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize)
{
	PCBYTE p1 = pSrc + 0;

	PCBYTE p2 = pSrc + 128;

	PBYTE  pw = pDest;

	for( int r = 0; r < cy / 16; r ++ ) {

		for( int i = 0; i < cx / 16; i ++) {

			memcpy(pw +  0, p1, 16);

			memcpy(pw + 16, p2, 16);

			pw += 32;

			p1 += 16;

			p2 += 16;
			}

		p1 += 128;

		p2 += 128;
		}
	}

// End of File
