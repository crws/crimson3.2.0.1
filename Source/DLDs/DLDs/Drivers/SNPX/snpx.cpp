
#include "intern.hpp"

#include "snpx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// GE SNP-X Driver
//

// Instantiator

INSTANTIATE(CSNPXDriver);

// Constructor

CSNPXDriver::CSNPXDriver(void)
{
	m_Ident         = DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_wTxSize	= sizeof(m_bTxBuff);
	
	m_wRxSize	= sizeof(m_bRxBuff);
	
	m_IntHelp	= 0;
	}

// Destructor

CSNPXDriver::~CSNPXDriver(void)
{
	}

// Configuration

void MCALL CSNPXDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CSNPXDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}
	
// Management

void MCALL CSNPXDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CSNPXDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CSNPXDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_wIdLen = pData[1];

			if ( m_pCtx->m_wIdLen > SNP_ID_LENGTH )

				m_pCtx->m_wIdLen = SNP_ID_LENGTH;
				
			m_pCtx->m_Drop = GetString(pData);

			m_pCtx->m_fBreakFree = GetByte(pData);

			m_pCtx->m_bSlot	     = GetByte(pData);

			m_pCtx->m_wState  = STATE_IDLE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx->m_wState  = STATE_IDLE;

	return CCODE_SUCCESS;
	}
		

CCODE MCALL CSNPXDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CSNPXDriver::Ping(void)
{
	if( Attach() ) {

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CSNPXDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, UINT(IsLong(Addr.a.m_Type) ? 16 : 32));
	
	if( Attach() ) {

		Start();

		AddByte(0x1);

		AddRegSpec(Addr, uCount);

		AddWord(0x0);

		End();

		m_pCtx->m_wState = STATE_WAITX;

		if( Transact() ) {

			switch(Addr.a.m_Type) {

				case addrBitAsBit:

					GetBitRead(pData, uCount, Addr.a.m_Offset);
					break;

				case addrWordAsWord:

					GetWordRead(pData, uCount);
					break;

				case addrWordAsLong:
				case addrWordAsReal:

					GetLongRead(pData, uCount);
					break;
				}

			m_pCtx->m_wState = STATE_OPEN;

			return uCount;
			}
		}

	m_pCtx->m_wState = STATE_IDLE;

	return CCODE_ERROR;
	}

CCODE MCALL CSNPXDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, UINT(IsLong(Addr.a.m_Type) ? 16 : 32));

	if( Addr.a.m_Type == addrBitAsBit && uCount <= 2 * 8 ) {

		return DoSmallWrite(Addr, pData, uCount);
		}

	if( Addr.a.m_Type == addrWordAsWord && uCount == 1 ) {

		return DoSmallWrite(Addr, pData, uCount);
		}
			
	if( Attach() ) {

		Start();

		AddByte(0x2);

		AddRegSpec(Addr, uCount);

		AddWord(0x0);

		AddByte(ETB);

		AddByte('T');

		UINT uBytes = 0;

		switch(Addr.a.m_Type) {

			case addrBitAsBit:

				uBytes = FindBitsByteCount(Addr.a.m_Offset, uCount);
				break;

			case addrWordAsWord:

				uBytes = uCount * 2;
				break;

			case addrWordAsLong:
			case addrWordAsReal:

				uBytes = uCount * 4;
				break;
			}

		AddWord(uBytes + 8);

		AddByte(0x0);

		m_pCtx->m_wState = STATE_WAITI;

		m_IntHelp = 0;

		if( Transact() ) {

			m_uPtr = 0;

			AddByte(ESC);

			AddByte('T');

			UINT u = 0;

			switch(Addr.a.m_Type) {

				case addrBitAsBit:

					AddBits(pData, uCount, Addr.a.m_Offset);
					break;

				case addrWordAsWord:

					for( u = 0; u < uCount; u++ ) {

						AddWord(LOWORD(pData[u]));
						}
					break;

				case addrWordAsLong:
				case addrWordAsReal:

					for( u = 0; u < uCount; u++ ) {

						AddLong(pData[u]);
						}
					break;	
				}

			End();

			m_pCtx->m_wState = STATE_WAITX;

			if( Transact() ) {

				m_pCtx->m_wState = STATE_OPEN;

				return uCount;
				}
			}
		}  
	
	m_pCtx->m_wState = STATE_IDLE;

	return CCODE_ERROR;
	}

// Entry point helper
 
CCODE CSNPXDriver::DoSmallWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Attach() ) {

		Start();

		AddByte(0x2);

		AddRegSpec(Addr, uCount);

		switch(Addr.a.m_Type) {

			case addrBitAsBit:
				
				AddBits(pData, uCount, Addr.a.m_Offset);
				break;

			case addrWordAsWord:

				AddWord(LOWORD(pData[0]));
				break;
			}
		
		End();

		m_pCtx->m_wState = STATE_WAITX;

		if( Transact() ) {

			m_pCtx->m_wState = STATE_OPEN;

			return uCount;
			}
		}

	m_pCtx->m_wState = STATE_IDLE;
	
	return CCODE_ERROR;
	} 

// Implementation

BOOL CSNPXDriver::Attach(void)
{
	if( m_pCtx->m_wState != STATE_OPEN ) {

		SendLongBreak();
	
		Start();

		AddByte(0x0);

		for( UINT u = 0; u < 7; u++ ) {

			AddByte(0x0);
			}

		End();

		m_pCtx->m_wState = STATE_WAITA;

		if( !Transact() ) {

			m_pCtx->m_wState = STATE_IDLE;

			return FALSE;
			}

		m_pCtx->m_wState = STATE_OPEN;
		}

	return TRUE;
	}

void CSNPXDriver::SendLongBreak(void)
{
	if( !m_pCtx->m_fBreakFree ) {
	
		m_pData->SetBreak(TRUE);

		Sleep(T0_PERIOD);

		m_pData->SetBreak(FALSE);

		Sleep(T4_PERIOD);
		}
	}

void CSNPXDriver::Start(void)
{
	m_uPtr = 0;
	
	AddByte(ESC);

	AddByte('X');

	UINT uCount = SNP_ID_LENGTH;

	PBYTE pID = PBYTE(alloca(uCount));

	memset(pID, 0, uCount);

	memcpy(pID, m_pCtx->m_Drop, m_pCtx->m_wIdLen);

	for( UINT u = 0; u < uCount; u++ ) {

		AddByte(pID[u]);
		}
	}

void CSNPXDriver::End(void)
{
	AddByte(ETB);

	AddByte(0x0);

	AddByte(0x0);

	AddByte(0x0);

	AddByte(0x0);
	}

void CSNPXDriver::AddByte(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;
	}

void CSNPXDriver::AddWord(WORD wData)
{
	m_bTxBuff[m_uPtr++] = LOBYTE(wData);
	
	m_bTxBuff[m_uPtr++] = HIBYTE(wData);
	}

void CSNPXDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CSNPXDriver::AddBits(PDWORD pData, UINT uCount, UINT uOffset)
{
	UINT b = 0;

	BYTE m = 1;
	
	m <<= ((uOffset - 1) % 8);

	UINT uBytes = 0;

	for( UINT n = 0; n < uCount; n++ ) {

		if( pData[n] ) {
					
			b |= m;

			}

		if( !(m <<= 1) ) {

			AddByte(b);

			uBytes++;

			b = 0;

			m = 1;
			}
		}

	if( m > 1 ) {

		AddByte(b);

		uBytes++;

		}

	UINT Count = FindBitsByteCount(uOffset, uCount);

	Count = Count > 1 ? Count : 2;

	for ( ; uBytes < Count; uBytes++) {

		AddByte(0);
		}
	}

BOOL CSNPXDriver::Transact(void)
{
	return PutFrame() && GetFrame()	&& CheckFrame();
	}

BOOL CSNPXDriver::PutFrame(void)
{
	m_Check.Clear();

	m_Check.Calc(m_bTxBuff, m_uPtr);
	
	AddByte(m_Check.Get());
		
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CSNPXDriver::GetFrame(void)
{
	UINT uState = 0;

	UINT uTimer = 0;

	UINT uByte = 0;
	
	UINT uCount = 0;

	m_uPtr = 0;

	m_Check.Clear();

	SetTimer(T2_PERIOD);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uByte = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == ESC ) {
					
					m_bRxBuff[m_uPtr++] = uByte;

					m_Check.AddByte(uByte);
					
					uState = 1;
					}
				break;
				
			case 1:
				if( m_uPtr == 8 ) {

					uCount = uByte;

					uState = 2;
					}

				m_bRxBuff[m_uPtr++] = uByte;
		
				m_Check.AddByte(uByte);

				break;

			case 2:
				if( uCount ) {

					uCount--;
					}
				else {
					if( m_bRxBuff[m_uPtr - 5] == ETB ) {

						if( m_Check.Get() == uByte ) {
							
							return TRUE;
							}
						}
					}
				
				m_bRxBuff[m_uPtr++] = uByte;
		
				m_Check.AddByte(uByte);

				break;
			}
		}

	return FALSE;
	}

BOOL CSNPXDriver::CheckFrame(void)
{
	switch( m_pCtx->m_wState ) {

		case STATE_WAITA:
						
			if( CheckID() )	{
					 
				return (m_bRxBuff[10] == 0x80);
				}

			return FALSE;

		case STATE_WAITI:

			m_IntHelp = m_bTxBuff[10];

			return (m_bRxBuff[2] == m_bTxBuff[10] + 0x80);
		}

	if( m_IntHelp > 0 ) {
	
		BOOL fSuccess = m_bRxBuff[2] == m_IntHelp + 0x80;

		m_IntHelp = 0;

		return fSuccess;
		}

	return (m_bRxBuff[2] == m_bTxBuff[10] + 0x80);
	} 

void CSNPXDriver::AddRegSpec(AREF Addr, UINT uCount)
{
	BOOL fBit = ( Addr.a.m_Type == addrBitAsBit );
		
	AddByte(GetTypeID(Addr.a.m_Table, fBit));

	AddWord(Addr.a.m_Offset - 1);

	if( Addr.a.m_Type == addrWordAsLong || Addr.a.m_Type == addrWordAsReal ) {

		AddWord(uCount * 2);
		}
	else {
		AddWord(uCount);
		}
	}

BYTE CSNPXDriver::GetTypeID(WORD wType, BOOL fBit)
{
	switch( wType ) {
	
		case 'Q':		return fBit ? 0x48 : 0x12;
		case 'T':		return fBit ? 0x4A : 0x14;
		case 'M':		return fBit ? 0x4C : 0x16;
		case 'B':		return fBit ? 0xFF : 0x0C;
		case 'R':		return fBit ? 0xFF : 0x08;
		case 'S':		return fBit ? 0x54 : 0x1E;
		case 'E':		return fBit ? 0x4E : 0x18;
		case 'D':		return fBit ? 0x50 : 0x1A;
		case 'C':		return fBit ? 0x52 : 0x1C;
		case 'I':		return fBit ? 0x46 : 0x10;
		case 'A':		return fBit ? 0xFF : 0x0A;
		case 'G':		return fBit ? 0x56 : 0x38;
		}
	
	return 0xFF;
	}  

BOOL CSNPXDriver::CheckID(void)
{
	UINT uCount = SNP_ID_LENGTH;

	PBYTE pID = PBYTE(alloca(uCount));

	memcpy(pID, m_bRxBuff + 2, uCount);

	for( UINT u = 0; u < uCount; u++ ) {

		if( u >= m_pCtx->m_wIdLen ) {

			if( pID[u] != 0x0 ) {

				return FALSE;
				}
			}

		else if( pID[u] != m_pCtx->m_Drop[u] ) {

			return FALSE;
			}
		}
			
	return (m_bRxBuff[10] == 0x80);
	}

void CSNPXDriver::GetBitRead(PDWORD pData, UINT uCount, UINT uOffset)
{
	UINT b = 0;

	BYTE m = 1;

	m <<= ((uOffset - 1) % 8);

	for( UINT n = 0; n < uCount; n++ ) {

		pData[n] = (m_bRxBuff[b + 9] & m) ? TRUE : FALSE;

		if( !(m <<= 1) ) {

			b = b + 1;

			m = 1;
			}
		}
	}

void CSNPXDriver::GetWordRead(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		WORD x = PU2(m_bRxBuff + (n * 2) + 9)[0];

		pData[n] = LONG(SHORT(IntelToHost(x)));

		}
	}

void CSNPXDriver::GetLongRead(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = PU4(m_bRxBuff + (n * 4) + 9)[0];

		pData[n] = IntelToHost(x);

		}
	}

UINT CSNPXDriver::FindBitsByteCount(UINT uOffset, UINT uCount)
{
	BYTE m = 1;
	
	m <<= ((uOffset - 1) % 8);

	UINT uBytes = 0;

	for( UINT n = 0; n < uCount; n++ ) {

		if( !(m <<= 1) ) {

			uBytes++;

			m = 1;
			}
		}

	if( m > 1 ) {

		uBytes++;
		}

	return uBytes;
	}

BOOL CSNPXDriver::IsLong(UINT uType)
{
	return uType == addrWordAsLong || uType == addrWordAsReal;
	}

 

