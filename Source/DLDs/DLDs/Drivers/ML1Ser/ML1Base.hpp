#ifndef	INCLUDE_ML1BASE_HPP
	
#define	INCLUDE_ML1BASE_HPP

#include "ml1blk.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Base Shared Functions
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
//  Debugging Help
//

static BOOL ML1_DEBUG = 0;

//////////////////////////////////////////////////////////////////////////
//
//  ML1 Enumerations
//

enum Spaces {

	spaceHR = 1,
	spaceAI = 2,
	spaceDC = 3,
	spaceDI = 4,
	spaceFR = 5,
	spaceSR = 6,
	};

enum Exception {

	exFunction = 1,
	exAddress  = 2,
	exData	   = 3,
	};

enum Authentication {

	authPush   = 129,
	authAck	   = 130,
	authPoll   = 131,
	authResp   = 132,
	};

enum SpecialRegs {

	srServer  = 0,	// BYTE
	srUnit	  = 1,	// 24-bit
	srAuth    = 3,  // 4-BYTE Authority
	srUTC     = 5,  // Time DWORD - on change set time to UTC value
	srTO	  = 7,  // WORD
	srRetry   = 8,  // WORD
	srPoll	  = 9,  // Client SR Poll Interval (secs) WORD
	srServe   = 10, // Server SR Write Enable 0-disable, 1-enable
	srIP1	  = 11, // DWORD
	srPort    = 13, // WORD
	srICMP	  = 14, // 0-off, 1-on
	srTime1	  = 15, // Connection Timeout WORD 
	srTime2   = 16, // Transaction Timeout WORD
	};


//////////////////////////////////////////////////////////////////////////
//
//  Forward Declarations
//

class CCodedItem;
class CML1Blk;

//////////////////////////////////////////////////////////////////////////
//
//  ML1 Constants
//

#define ML1_DEVS	255
#define SR_MIN		64999
#define SR_MAX		65015

//////////////////////////////////////////////////////////////////////////
//
//  ML1 Base Driver
// 

class CML1Base : public CMasterDriver
{
	public:

		// Constructor
		CML1Base(void);

		// Destructor
		~CML1Base(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);

	protected:

		// MAC Data
		struct CMacAddr
		{
			BYTE m_Addr[6];
			};

		// Device Data
		struct CML1Dev
		{
			BYTE	 m_Server;
			WORD	 m_wTrans;
			DWORD	 m_Unit;
			DWORD	 m_Authority;
			CMacAddr m_Mac;
			WORD	 m_PingReg;
			WORD     m_TO;
			BYTE     m_Retry;
			WORD	 m_Blocks;
			CML1Blk *m_pHead;
			CML1Blk *m_pTail;
			BOOL	 m_fOnline;
			UINT	 m_uAuthPoll;
			BOOL	 m_fAuthentic;
			BOOL	 m_fPush;
			CML1Dev* m_pNext;
			CML1Dev* m_pPrev;
			UINT     m_uSrPoll;
			UINT     m_uSrLast;
			BOOL     m_fSrServe;
			};

		// Help
		IExtraHelper *	m_pExtra;

		// Data Members
		UINT	   m_uTxPtr;
		BYTE	   m_pTx[600];
		BYTE	   m_pRx[600];
		BYTE	   m_pBuff[600];
		CRC16	   m_Key;
		CML1Dev *  m_pDev;
		CML1Dev *  m_pHead;
		CML1Dev *  m_pTail;
		CMacAddr   m_Mac;
		BYTE	   m_Server;
		DWORD	   m_Unit;
		DWORD	   m_Authority;
		UINT	   m_AuthInterval;
		BOOL	   m_fAuthentic;
		BOOL	   m_fPush;
		UINT       m_uAuthPoll;
		BOOL       m_fClient;

		// Implementation
		void	  InitDev(void);
		PCBYTE    LoadBlocks(IExtraHelper * pExtra, PCBYTE pData);
		void	  RemoveBlocks(void);
		CML1Blk * FindBlock(CML1Dev * pDev, AREF Addr);
		void	  StartFrame(CML1Dev * pDev, BYTE bOp, WORD wTrans = 0);
		void	  AddHeader(CML1Dev * pDev, WORD wTrans);
		void	  AddByte(BYTE bData);
		void	  AddWord(WORD wData);
		void	  AddLong(DWORD dwData);
		void	  AddMac(CML1Dev * pDev = NULL);
		void	  AddKey(PBYTE pBuff);
		BYTE	  GetKeyHead(void);
		BYTE	  GetKeyTail(PBYTE pBuff);
		
		// Special Block Support
		void	  MakeSpecial(void);
		CML1Blk * FindSpecial(CML1Dev * pDev);
	virtual	void      InitSpecial(void);
	virtual void      SetSr(CML1Dev * pDev, UINT& uOffset, PWORD pData);
	virtual	void	  AddSpecial(CML1Dev * pDev, BYTE bCount);
	virtual	BYTE	  GetSpecialExtra(void);
	virtual BYTE	  FindSpecialWriteCount(UINT uOffset);
		BYTE	  GetSpecialCount(PBYTE pBuff);
		BYTE	  GetSpecialBytes(BYTE bCount);
		BOOL	  IsSrLong(UINT uOffset);
		BOOL	  IsSrBase(UINT uOffset);
		void	  SetTime(DWORD dwTime);
							
		// Transport
	virtual BOOL Send(void);
	virtual BOOL Recv(void);
	virtual BOOL Listen(void);
	virtual BOOL Respond(PBYTE pBuff);
	virtual BOOL Respond(CML1Dev * pDev);

		// Helpers
		void GetEntitySize(AREF Addr, UINT& uSize);
		BOOL IsLong(AREF Addr);
		void MakeMaxCount(AREF Addr, UINT& uMax);
		BOOL IsTimedOut(UINT uTime, UINT uSpan);
		BOOL IsBroadcast(BYTE bId);
		BOOL IsFileRecord(AREF Addr);
		UINT GetFileNumber(AREF Addr);
		UINT GetFileRecord(AREF Addr);
		BOOL IsMacNULL(CML1Dev * pDev = NULL);
		BOOL IsSpecial(AREF Addr);
		void IncTrans(CML1Dev * pDev);
		
		// Debugging Help
		void ShowDebug(PCTXT pName, ...);
	};

#endif

// End of File
