
#include "ml1ser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CML1SerialDriver);

// Constructor

CML1SerialDriver::CML1SerialDriver(void)
{
	m_Ident   = DRIVER_ID;	

	m_fTrack  = TRUE;

	m_pDev	  = NULL;

	m_fClient = TRUE;
	}

// Destructor

CML1SerialDriver::~CML1SerialDriver(void)
{
	
	}

// Configuration

void MCALL CML1SerialDriver::Load(LPCBYTE pData)
{
	CML1Driver::Load(pData);

	m_fTrack = GetByte(pData);
	}

void MCALL CML1SerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);

	Config.m_uFlags |= flagNoCheckError;

	m_Baud = Config.m_uBaudRate;
	}

// Management

void MCALL CML1SerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);

	CML1Base::Attach(pPort);
	}

void MCALL CML1SerialDriver::Open(void)
{

	}

// Device

CCODE MCALL CML1SerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pDev = (CML1Dev *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pDev  = new CML1Dev;

			m_pDev->m_PingReg = GetWord(pData);

			m_pDev->m_TO	  = GetWord(pData);

			m_pDev->m_Retry   = GetByte(pData);

			m_pDev->m_Blocks  = GetWord(pData);

			// Server and Unit ID's should always be initialized at 0 and is set at authentication.

			m_pDev->m_Server = 0;

			m_pDev->m_Unit   = 0;

			// At present, ML1 is a point to point protocol with the Server ID residing on the 
			// device level, likewise, the Unit ID resides on the driver level.  This 
			// implementation (Server ID and UINT ID residing on both levels) is maintained
			// in event that ML1 evolves to a multi drop protocol!

			InitDev();

			LoadBlocks(m_pExtra, pData);

			InitSpecial();

			AfxListAppend(m_pHead, m_pTail, m_pDev, m_pNext, m_pPrev);

			pDevice->SetContext(m_pDev);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CML1SerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		RemoveBlocks();

		AfxListRemove(m_pHead, m_pTail, m_pDev, m_pNext, m_pPrev);
		
		m_pDev = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Transport

BOOL CML1SerialDriver::Send(void)
{
	if( !IsAuthentication(NULL, m_pTx) && !IsAuthenticationReq(m_pTx) ) {

		m_pTx[5] = BYTE(m_uTxPtr - 6);
		}

	m_CRC.Preset();

	for( UINT i = 0; i < m_uTxPtr; i++ ) {
	
		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));
		
	m_pData->Write(m_pTx, m_uTxPtr, FOREVER);

	if( ML1_DEBUG ) {

		AfxTrace("\nTx %u ptr %u: ", GetTickCount(), m_uTxPtr);

		for( UINT u = 0; u < m_uTxPtr; u++ ) {

			AfxTrace("%2.2x ", m_pTx[u]);
			}
		}

	return TRUE;
	}

BOOL CML1SerialDriver::Recv(void)
{
	UINT uByte = 0;

	UINT uSize = NOTHING;

	UINT uPtr  = 0;

	UINT uGap  = 0;

	UINT uEnd  = ToTicks(25);

	if( ML1_DEBUG ) {

		AfxTrace("\nSer Rx : ");
		}

	SetTimer(600);

	while( GetTimer() ) {

		if( (uByte = m_pData->Read(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

			if( ML1_DEBUG ) {

				AfxTrace("%2.2x ", m_pRx[uPtr - 1]);
				}

			if( uPtr == elements(m_pRx) ) {

				return FALSE;
				}

			if( uPtr == 8 ) {

				uSize = FindRxSize();
				}

			if( uPtr == 16 ) {

				uSize = FindRxSize();
				}

			if( m_fTrack ) {

				SetTimer(100);
				}

			uGap = 0;
			}
		else
			uGap = uGap + 1;

		if( uPtr >= uSize || uGap >= uEnd ) {

			if( uPtr >= 9 ) {

				m_CRC.Preset();
				
				PBYTE p = m_pRx;
				
				UINT  n = uPtr - 2;
			
				for( UINT i = 0; i < n; i++ ) {

					m_CRC.Add(*(p++));
					}

				WORD c1 = IntelToHost(PU2(p)[0]);
				
				WORD c2 = m_CRC.GetValue();

				if( c1 == c2 ) {

					memcpy(m_pBuff, m_pRx, uPtr);

					return HandleFrame(m_pBuff);
					}
				}

			uSize = NOTHING;
				
			uPtr  = 0;
			
			uGap  = 0;
			}
		}

	return FALSE;
	}

BOOL CML1SerialDriver::Respond(PBYTE pBuff)
{
	return Send();
	}

BOOL CML1SerialDriver::Respond(CML1Dev * pDev)
{
	return Send();
	}

// Transport Helpers

UINT CML1SerialDriver::FindRxSize(void)
{
	if( m_fTrack ) {

		if( IsAuthentication(NULL, m_pRx) ) {

			switch( m_pRx[0] ) {

				case authPush:	return 20 + GetSpecialBytes(GetSpecialCount(m_pRx));

				case authAck:	return 20 + GetSpecialBytes(GetSpecialCount(m_pRx));

				case authPoll:	return  9;

				case authResp:	return 20 + GetSpecialBytes(GetSpecialCount(m_pRx));
				}
			}

		return m_pRx[5] + 6 + 2;
		}
			
	return NOTHING;
	}

// End of File
