
#include "ml1base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Base Shared Functions
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CML1Base::CML1Base(void)
{
	m_pExtra     = NULL;

	m_pHead	     = NULL;

	m_pTail      = NULL;

	m_pDev       = NULL;

	m_uTxPtr     = 0;

	m_Unit       = 0;

	m_Authority  = 0;

	m_fPush	     = FALSE;

	m_fAuthentic = FALSE;

	m_uAuthPoll  = 0;

	m_fClient    = TRUE;

	m_AuthInterval = 0;
	
	memset(m_Mac.m_Addr, 0, sizeof(m_Mac.m_Addr));
	}

// Destructor

CML1Base::~CML1Base(void)
{
	RemoveBlocks();

	AfxListRemove(m_pHead, m_pTail, m_pDev, m_pNext, m_pPrev);

	m_pExtra->Release();
	}

// Management

void MCALL CML1Base::Attach(IPortObject *pPort)
{
	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

// Implementation

void CML1Base::InitDev(void)
{
	if( m_pDev ) {

		m_pDev->m_wTrans     = 0;

		m_pDev->m_Authority  = 0;

		m_pDev->m_pHead      = NULL;

		m_pDev->m_pTail      = NULL;
	
		m_pDev->m_fOnline    = FALSE;

		m_pDev->m_uAuthPoll  = 0;
	
		m_pDev->m_fAuthentic = FALSE;

		m_pDev->m_fPush	     = FALSE;

		m_pDev->m_pNext	     = NULL;

		m_pDev->m_pPrev	     = NULL;

		m_pDev->m_pHead	     = NULL;

		m_pDev->m_pTail	     = NULL;

		m_pDev->m_uSrPoll    = 0;

		m_pDev->m_uSrLast    = 0;

		m_pDev->m_fSrServe   = FALSE;

		memset(PBYTE(&m_pDev->m_Mac), 0, sizeof(CMacAddr));
		}
	}

PCBYTE CML1Base::LoadBlocks(IExtraHelper * pExtra, PCBYTE pData)
{
	if( m_pDev->m_Blocks ) {

		UINT uSpec = 0;

		for( UINT b = 0; b < m_pDev->m_Blocks; b++ ) {

			if( GetWord(pData) == 0x1234 ) {

				DWORD dwAddr = GetLong(pData);

				WORD   wSize = GetWord(pData);

				BYTE    bCOV = GetByte(pData);

				WORD     wTO = GetWord(pData);

				BYTE  bRetry = GetByte(pData);

				BYTE  bExtra = GetByte(pData);

				PVOID pCoded = NULL;
			
				if( bExtra ) {
				
					pExtra->GetCodedItem(pData, pCoded);
					}

				if( !IsSpecial(AREF(dwAddr)) ) {

					CML1Blk * pBlk = new CML1Blk(m_pHelper);
			
					if( pBlk ) {

						pBlk->SetAddr(dwAddr);

						pBlk->SetSize(wSize);
		
						pBlk->SetCOV(bCOV);
	
						// Monico Requested Device Level Timeout

						pBlk->SetTO(m_pDev->m_TO);
	
						// Monico Requested Device Level Retry

						pBlk->SetRetry(m_pDev->m_Retry);
	
						pBlk->SetCoded(pCoded);
					
						pBlk->Init(); 
	
						AfxListAppend(m_pDev->m_pHead,
							      m_pDev->m_pTail,
						              pBlk,
							      m_pNext,
							      m_pPrev);
						}
					}
				else {
					uSpec++;
					}
				}
			}

		m_pDev->m_Blocks -= uSpec;

		MakeSpecial();
		}

	return pData;
	}

void CML1Base::RemoveBlocks(void)
{
	CML1Blk * pBlock = m_pDev->m_pHead;

	while( pBlock ) {

		AfxListRemove(m_pDev->m_pHead, m_pDev->m_pTail, pBlock, m_pPrev, m_pNext);

		pBlock = m_pDev->m_pHead;
		}	
	}

CML1Blk * CML1Base::FindBlock(CML1Dev * pDev, AREF Addr)
{
	CML1Blk * pBlock = pDev->m_pHead;

	while( pBlock ) {

		DWORD Ref = pBlock->GetAddr().m_Ref;

		UINT Size = pBlock->GetSize();

		GetEntitySize(Addr, Size);
				
		if( Addr.m_Ref >= Ref && Addr.m_Ref < Ref + Size ) {

			return pBlock;
			}
		
		pBlock = pBlock->m_pNext;
		}

	return NULL;
	}

void CML1Base::StartFrame(CML1Dev * pDev, BYTE bOp, WORD wTrans)
{
	AddHeader(pDev, wTrans);

	AddByte(pDev->m_Server);
	
	AddByte(bOp);
	}

void CML1Base::AddHeader(CML1Dev * pDev, WORD wTrans)
{
	m_uTxPtr = 0;

	if( wTrans == 0 ) {

		IncTrans(pDev);

		AddWord(pDev->m_wTrans);
		}
	else {
		AddWord(wTrans);
		}

	AddByte(LOBYTE(HIWORD(m_Unit)));

	AddWord(LOWORD(m_Unit));

	AddByte(0);	
	}

void CML1Base::AddByte(BYTE bData)
{
	if( m_uTxPtr < sizeof(m_pTx) ) {
	
		m_pTx[m_uTxPtr] = bData;
		
		m_uTxPtr++;
		}
	}

void CML1Base::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CML1Base::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void CML1Base::AddMac(CML1Dev * pDev)
{
	UINT uMac = sizeof(m_Mac.m_Addr);
	
	for( UINT m = 0; m < uMac; m++ ) {

		BYTE bByte = pDev ? pDev->m_Mac.m_Addr[m] : m_Mac.m_Addr[m];

		AddByte(bByte);
		}
	}

void CML1Base::AddKey(PBYTE pBuff)
{
	if( pBuff ) {

		m_Key.Clear();

		for( UINT k = GetKeyHead(); k < GetKeyTail(pBuff); k++ ) {

			m_Key.Add(pBuff[k]);
			}

		AddWord(m_Key.GetValue());
		}
	}

BYTE CML1Base::GetKeyHead(void)
{
	return 2;
	}

BYTE CML1Base::GetKeyTail(PBYTE pBuff)
{
	return 15 + GetSpecialCount(pBuff);
	}

// Special Block Support

void CML1Base::MakeSpecial(void)
{
	CAddress Addr;

	Addr.a.m_Table  = spaceSR;

	Addr.a.m_Extra  = 0;

	Addr.a.m_Type   = addrWordAsWord;

	Addr.a.m_Offset = SR_MIN;

	CML1Blk * pBlk = new CML1Blk(m_pHelper);

	if( pBlk ) {

		pBlk->SetAddr(Addr.m_Ref + 1);

		pBlk->SetSize((SR_MAX - SR_MIN) * 2);

		pBlk->Init();

		AfxListAppend(m_pDev->m_pHead,
			      m_pDev->m_pTail, 
			      pBlk, 
			      m_pNext, 
			      m_pPrev);

		m_pDev->m_Blocks++;
		}
	}

CML1Blk * CML1Base::FindSpecial(CML1Dev * pDev)
{
	CAddress Addr;

	Addr.a.m_Table = spaceSR;

	Addr.a.m_Extra = 0;

	Addr.a.m_Type  = addrWordAsWord;

	Addr.a.m_Offset = SR_MIN;

	return FindBlock(pDev, Addr);
	}

void CML1Base::InitSpecial(void)
{
	CML1Blk * pBlk = FindSpecial(m_pDev);

	if( pBlk ) {

		UINT uTime = m_pExtra ? m_pExtra->GetNow() : 0;

		for( UINT u = SR_MIN; u < SR_MAX; u++ ) {

			switch( u - SR_MIN ) {

				case srServer:	pBlk->SetRxWord(u - SR_MIN, m_pDev->m_Server);		break;

				case srUnit:	pBlk->SetRxWord(u - SR_MIN, HIWORD(m_Unit));		u++;

						pBlk->SetRxWord(u - SR_MIN, LOWORD(m_Unit));		break;

				case srAuth:    pBlk->SetRxWord(u - SR_MIN, HIWORD(m_Authority));	u++;

						pBlk->SetRxWord(u - SR_MIN, LOWORD(m_Authority));	break;

				case srTO:	pBlk->SetRxWord(u - SR_MIN, m_pDev->m_TO);		break;

				case srRetry:	pBlk->SetRxWord(u - SR_MIN, m_pDev->m_Retry);		break;

				case srUTC:	pBlk->SetRxWord(u - SR_MIN, HIWORD(uTime));		u++;

						pBlk->SetRxWord(u - SR_MIN, LOWORD(uTime));		break;

				case srPoll:	pBlk->SetRxWord(u - SR_MIN, m_pDev->m_uSrPoll);		break;

				case srServe:	pBlk->SetRxWord(u - SR_MIN, m_pDev->m_fSrServe);	break;
				}
			}
		}
	}

void CML1Base::SetSr(CML1Dev * pDev, UINT& uOffset, PWORD pData)
{
	CML1Blk * pBlock = FindSpecial(pDev);

	if( pBlock ) {

		WORD x = SHORT(MotorToHost(PU2(pData)[0]));

		pBlock->SetRxWord(uOffset, x);

		if( IsSrLong(uOffset) ) {

			WORD y = SHORT(MotorToHost(PU2(pData)[1]));

			pBlock->SetRxWord(uOffset + 1, y);

			DWORD Data = MotorToHost(PU4(pData)[0]);

			if( uOffset == srUnit ) {

				if( pDev->m_Unit != Data || m_Unit != Data ) {

					pDev->m_Unit = Data;

					m_Unit       = Data;

					m_fAuthentic = FALSE;
					}
				}

			else if ( uOffset == srAuth ) {

				pDev->m_Authority = Data;

				m_Authority       = Data;
				}

			else if( uOffset == srUTC ) {

				SetTime(Data);
				}

			uOffset++;

			return;
			}

		if( uOffset == srServer ) {

			if( pDev->m_Server != x || m_Server != x ) {

				pDev->m_Server = x;

				m_Server       = x;

				m_fAuthentic   = FALSE;
				}

			return;
			}

		if( uOffset == srTO ) {

			pDev->m_TO = x;

			// Monico Requested Device Level Timeout

			CML1Blk * pBlock = pDev->m_pHead;

			while( pBlock ) {

				pBlock->m_TO = x;

				pBlock = pBlock->m_pNext;
				}

			return;
			}

		if( uOffset == srRetry ) {

			pDev->m_Retry = x;

			// Monico Requested Device Level Retry

			CML1Blk * pBlock = pDev->m_pHead;

			while( pBlock ) {

				pBlock->m_Retry = x;

				pBlock = pBlock->m_pNext;
				}

			return;
			}

		if( uOffset == srPoll ) {

			pDev->m_uSrPoll = x;

			return;
			}

		if( uOffset == srServe ) {

			pDev->m_fSrServe = x;

			return;
			}
		}
	}

void CML1Base::AddSpecial(CML1Dev * pDev, BYTE bCount)
{
	CML1Blk * pBlk = FindSpecial(pDev);

	if( pBlk && bCount ) {

		AddByte(bCount);

		UINT uTime = m_pExtra ? m_pExtra->GetNow() : 0;

		for( UINT u = srUTC, c = 0; c < bCount; u++, c++ ) {

			switch( u ) {

				case srUTC:	AddLong(uTime);			u++;	break;

				case srTO:	AddWord(pDev->m_TO);			break;

				case srRetry:	AddWord(pDev->m_Retry);			break;

				case srPoll:	AddWord(pDev->m_uSrPoll);		break;

				case srServe:	AddWord(pDev->m_fSrServe);		break;
				}
			}

		return;
		}

	AddByte(0);
	}

BYTE CML1Base::GetSpecialExtra(void)
{
	return 5;
	}

BYTE CML1Base::FindSpecialWriteCount(UINT uOffset)
{
	BYTE bCount = 0;

	switch( uOffset ) {

		case srUTC:	bCount = 1;	break;

		case srTO:	bCount = 2;	break;

		case srRetry:	bCount = 3;	break;

		case srPoll:	bCount = 4;	break;

		case srServe:	bCount = 5;	break;
		}

	return bCount;
	}

BYTE CML1Base::GetSpecialCount(PBYTE pBuff)
{
	return pBuff[15];
	}

BYTE CML1Base::GetSpecialBytes(BYTE bCount)
{
	BYTE bBytes = 0;

	for( BYTE b = srUTC; b < bCount + srUTC; b++ ) {

		if( IsSrLong(b) ) {

			bBytes +=4;
	
			b++;

			bCount++;

			continue;
			}

		bBytes += 2;
		}
	
	return bBytes;
	}

BOOL CML1Base::IsSrLong(UINT uOffset)
{
	switch( uOffset ) {

		case srUnit:
		case srAuth:
		case srUTC:
		case srIP1:
			
			return TRUE;
		}

	return FALSE;
	}

BOOL CML1Base::IsSrBase(UINT uOffset)
{
	switch( uOffset ) {

		case srServer:
		case srUnit:
		case srTO:
		case srRetry:
		case srUTC:
		case srPoll:
		case srServe:
		case srAuth:
			
			return TRUE;
		}

	return FALSE;
	}

void CML1Base::SetTime(DWORD dwTime)
{	
	if( m_pExtra ) {

		m_pExtra->SetNow(dwTime);
		}
	}

// Transport

BOOL CML1Base::Send(void)
{
	return FALSE;
	}

BOOL CML1Base::Recv(void)
{
	return FALSE;
	}

BOOL CML1Base::Listen(void)
{
	return FALSE;
	}

BOOL CML1Base::Respond(PBYTE pBuff)
{
	return FALSE;
	}

BOOL CML1Base::Respond(CML1Dev * pDev)
{
	return FALSE;
	}

// Helpers

void CML1Base::GetEntitySize(AREF Addr,  UINT &uSize)
{
	if( IsLong(Addr) ) {

		uSize *= 2;
		}
	}

BOOL CML1Base::IsLong(AREF Addr)
{	
	return Addr.a.m_Type > addrWordAsWord;
	}

void CML1Base::MakeMaxCount(AREF Addr, UINT& uMax)
{
	UINT uBytes = 240;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	MakeMin(uMax, uBytes * 8);	break;
		case addrWordAsWord:	MakeMin(uMax, uBytes / 2);	break;
		case addrWordAsLong:
		case addrWordAsReal:	MakeMin(uMax, uBytes / 4);	break;
		}
	}

BOOL CML1Base::IsTimedOut(UINT uTime, UINT uSpan)
{
	return int(GetTickCount() - uTime - ToTicks(uSpan)) >= 0;
	}

BOOL CML1Base::IsBroadcast(BYTE bId)
{
	return bId == 255;
	}

BOOL CML1Base::IsFileRecord(AREF Addr)
{
	return Addr.a.m_Table == spaceFR;
	}

UINT CML1Base::GetFileNumber(AREF Addr)
{
	UINT uFile = Addr.a.m_Extra | ((Addr.a.m_Offset & 0xC000) >> 10);

	return uFile + 1;
	}

UINT CML1Base::GetFileRecord(AREF Addr)
{
	return Addr.a.m_Offset & 0x3FFF;
	}

BOOL CML1Base::IsMacNULL(CML1Dev * pDev)
{
	CMacAddr Null;

	UINT uMac = sizeof(Null.m_Addr);

	memset(Null.m_Addr, 0, uMac);

	if( pDev ) {

		return !memcmp(pDev->m_Mac.m_Addr, Null.m_Addr, uMac);
		}

	return !memcmp(m_Mac.m_Addr, Null.m_Addr, uMac);
	}

BOOL CML1Base::IsSpecial(AREF Addr)
{
	if( Addr.a.m_Table == spaceSR ) {

		UINT uOffset = Addr.a.m_Offset;

		return ((uOffset >= SR_MIN) && (uOffset <= SR_MAX));
		}

	return FALSE;
	}

void CML1Base::IncTrans(CML1Dev * pDev)
{
	if( pDev ) {

		pDev->m_wTrans++;

		if( pDev->m_wTrans > 0x7FFF ) {

			pDev->m_wTrans = 1;
			}
		}
	}

// Debugging Help

void CML1Base::ShowDebug(PCTXT pName, ...)
{
	if( ML1_DEBUG ) {

		va_list pArgs;

		va_start(pArgs, pName);

		AfxTrace(pName, pArgs);

		va_end(pArgs);
		}
	}

// End of File
