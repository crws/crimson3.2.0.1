
#include "ml1blk.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Block Implementation
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CML1Blk::CML1Blk(IHelper * pHelp)
{
	m_pHelp = pHelp;

	m_Addr	= 0;

	m_Size	= 0;

	m_Bytes = 0;

	m_COV	= 0;

	m_TO	= 0;

	m_Retry	= 0;

	m_pRBE	= NULL;

	m_Flags	= 0;

	m_pTx	= NULL;

	m_pRx	= NULL;

	m_pPrev = NULL;

	m_pNext = NULL;

	memset( &m_Next, 0, elements(m_Next  ) * sizeof(BYTE));

	memset( &m_Pend, 0, elements(m_Pend  ) * sizeof(BYTE));

	memset( &m_Tran, 0, elements(m_Tran  ) * sizeof(WORD));

	memset(m_RxTime, 0, elements(m_RxTime) * sizeof(UINT)); 

	memset(m_TxTime, 0, elements(m_RxTime) * sizeof(UINT));	
	}

// Destructor

CML1Blk::~CML1Blk(void)
{
	if( m_pRx ) {

		delete m_pRx;
		}

	if( m_pTx ) {

		delete m_pTx;
		}
	}

// Initialization

void CML1Blk::Init(void)
{
	UINT uSize = m_Size;

	AREF Addr  = (AREF)m_Addr;
	
	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	uSize *= 1;	break;

		case addrWordAsWord:	uSize *= 2;	break;


		case addrWordAsReal:
		case addrWordAsLong:	uSize *= 4;	break;
		}

	m_Bytes	= uSize;

	m_pRx	= new BYTE[uSize];
	
	m_pTx	= new BYTE[uSize];

	memset(m_pRx, 0, uSize);

	memset(m_pTx, 0, uSize);

	UINT uTO   = ToTicks(m_TO);

	UINT uTry  = ToTicks(m_Retry);

	UINT uTime = GetTickCount() - uTO * uTry;

	for( UINT o = 0; o < opWrite; o++ ) {

		m_Pend[o]   = 0;

		m_RxTime[o] = uTime;

		m_TxTime[o] = uTime;
		}

	Show();
	}

// Data Access

AREF CML1Blk::GetAddr(void)
{
	return AREF(m_Addr);
	}

void CML1Blk::SetAddr(DWORD dwAddr)
{
	m_Addr = dwAddr - 1;
	}

WORD CML1Blk::GetSize(void)
{
	return m_Size;
	}

void CML1Blk::SetSize(WORD wSize)
{
	m_Size = wSize;
	}
	
void CML1Blk::SetCOV(BYTE bCOV)
{
	m_COV = bCOV;
	}

void CML1Blk::SetTO(WORD wTO)
{
	m_TO = wTO * 1000;	
	}

void CML1Blk::SetRetry(BYTE bRetry)
{
	m_Retry = bRetry;
	}

void CML1Blk::SetCoded(void * pCoded)
{
	m_pRBE = pCoded;
	}

void CML1Blk::SetRxTime(BYTE bMode)
{
	if( bMode < elements(m_RxTime) ) {

		m_RxTime[bMode] = GetTickCount();
		}
	}

void CML1Blk::SetTxTime(BYTE bMode)
{
	if( bMode < elements(m_TxTime) ) {

		m_TxTime[bMode] = GetTickCount();
		}
	}

UINT CML1Blk::GetData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uResult = 0;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			uResult = GetBitData (Addr, pData, uCount);	break;

		case addrWordAsWord:
			
			uResult = GetWordData(Addr, pData, uCount);	break;	

		case addrWordAsLong:
		case addrWordAsReal:
			
			uResult = GetLongData(Addr, pData, uCount);	break;
		}

	return uResult;
	}


UINT CML1Blk::SetData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uResult = 0;
	
	switch( Addr.a.m_Type ) {

		case addrBitAsBit:
			
			uResult = SetBitData (Addr, pData, uCount); break;

		case addrWordAsWord:	
			
			uResult = SetWordData(Addr, pData, uCount); break;

		case addrWordAsLong:
		case addrWordAsReal:
			
			uResult = SetLongData(Addr, pData, uCount); break;
		}

	return uResult;
	}

BYTE CML1Blk::GetTxByte(UINT uOffset)
{
	return m_pTx[uOffset];
	}

WORD CML1Blk::GetTxWord(UINT uOffset)
{
	return PU2(m_pTx)[uOffset];
	}

DWORD CML1Blk::GetTxLong(UINT uOffset)
{
	return PU4(m_pTx)[uOffset];
	}

void CML1Blk::SetRxByte(UINT uOffset, BYTE bData)
{
	m_pRx[uOffset] = bData;
	}

void CML1Blk::SetRxWord(UINT uOffset, WORD wData)
{
	PWORD(m_pRx + 0)[uOffset] = SHORT(MotorToHost(wData));
	}	

void CML1Blk::SetRxBuff(UINT uOffset, PBYTE pData, UINT uBytes)
{
	memcpy(PBYTE(m_pRx + uOffset), pData, uBytes);
	}

BYTE CML1Blk::SendOnChange(void)
{
	return m_COV;
	}

BOOL CML1Blk::FindOpParams(AREF Addr, UINT &uOffset, UINT &uCount)
{
	AREF Block = (AREF)m_Addr;

	if( Block.a.m_Offset <= Addr.a.m_Offset ) {

		uOffset = Addr.a.m_Offset - Block.a.m_Offset;

		INT Limit = m_Size - uOffset;

		if( Limit > 0 ) {

			MakeMin(uCount, Limit);
			}
			
		return TRUE;
		}

	return FALSE;
	}

BOOL CML1Blk::IsPending(BYTE bOp)
{
	if( bOp && bOp <= opWrite ) {

		return m_Pend[bOp - 1];
		}

	return FALSE;
	}

BOOL CML1Blk::IsTrans(BYTE bOp, WORD wTrans)
{
	if( bOp < elements(m_Tran) ) {
		
		return m_Tran[bOp] == wTrans || m_Tran[bOp] == 0;
		}

	return FALSE;
	}

BOOL CML1Blk::IsTimedOut(BYTE bOp, BOOL fMode)
{
	UINT uTime = (bOp == opRead) ? m_RxTime[fMode] : m_TxTime[fMode];

	return INT(GetTickCount() - uTime - ToTicks(m_TO)) >= 0;
	}

BOOL CML1Blk::IsExhausted(BYTE bOp)
{
	if( bOp && bOp <= opWrite ) {

		return m_Pend[bOp - 1] > m_Retry;
		}

	return TRUE;
	}

BOOL CML1Blk::Force(void)
{
	return m_Flags & opForce;
	}

void CML1Blk::SetNext(BYTE bOp)
{
	m_Next[bOp - 1] = 1;
	}

UINT CML1Blk::GetNext(UINT uOffset, BYTE bOp)
{
	if( m_Next[bOp - 1] ) {

		AREF Addr  = (AREF)m_Addr;

		UINT uSize = m_Size * (IsLong() ? 2 : 1);

		int nRem   = Addr.a.m_Offset + uSize - uOffset;

		if( nRem > 0 ) {

			return uOffset - Addr.a.m_Offset;
			}

		m_Next[bOp - 1] = 0;
		}

	return 0;
	}

BOOL CML1Blk::IsLong(void)
{
	AREF Addr = (AREF)m_Addr;

	return Addr.a.m_Type > addrWordAsWord;
	}

// Operations

BOOL CML1Blk::Execute(IExtraHelper * pExtra)
{
	BOOL fSend = FALSE;

	if( m_pRBE ) {

		if( !pExtra->ExecuteCoded(m_pRBE, 1 ) ) {

			ClrFlags(opSShot);
			}
		else {
			if( !(m_Flags & opSShot) ) {

				SetFlags(opSShot);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CML1Blk::Request(BYTE bOp, BOOL fSend)
{
	if( !fSend && (m_Flags & bOp) ) {

		fSend = IsTimedOut(bOp, modeReqst) && IsPending(bOp);

		if( fSend && IsExhausted(bOp) ) {

			ClrPend(bOp);

			return FALSE;
			}
							
		return fSend;
		}

	if( fSend && (m_Flags & bOp) && !IsPending(bOp) ) {

		return TRUE;
		}

	return FALSE;
	}

void CML1Blk::MarkComplete(BYTE bOp, BYTE bMode)
{
	ClrPend(bOp);

	if( bOp == opRead ) {

		SetRxTime(bMode);

		return;
		}

	SetTxTime(bMode);
	}

void CML1Blk::Mark(BYTE bOp)
{
	m_Flags |= bOp;
	}

BOOL CML1Blk::ClrPend(BYTE bOp)
{
	if( bOp && bOp <= opWrite ) {

		m_Pend[bOp - 1] = 0;

		m_Tran[bOp - 1] = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CML1Blk::IncPend(BYTE bOp)
{
	if( bOp && bOp <= opWrite ) {

		m_Pend[bOp - 1]++;

		m_Tran[bOp - 1] = MAKEWORD(m_pTx[0], m_pTx[1]);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

UINT CML1Blk::GetBitData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT Offset = 0;

	UINT Count  = uCount;

	if( FindOpParams(Addr, Offset, Count) ) {

		for( UINT n = 0; n < Count; n++ ) {

			pData[n] = m_pRx[Offset + n] ? 1 : 0;
			}

		return Count;
		}
	
	return 0;
	}

UINT CML1Blk::GetWordData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT Offset = 0;

	UINT Count  = uCount;

	if( FindOpParams(Addr, Offset, Count) ) {

		for( UINT n = 0; n < Count; n++ ) {
		
			WORD x   = PU2(m_pRx)[Offset + n];

			pData[n] = MotorToHost(x);
			}

		return Count;
		}

	return 0;
	}

UINT CML1Blk::GetLongData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT Offset = 0;

	UINT Count  = uCount;

	if( FindOpParams(Addr, Offset, Count) ) {

		for( UINT n = 0; n < Count; n++ ) {

			DWORD x  = PU4(m_pRx + Offset)[n];

			pData[n] = MotorToHost(x);
			}

		return Count;
		}

	return 0;
	}

UINT CML1Blk::SetBitData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT Offset = 0;

	UINT Count  = uCount;

	if( FindOpParams(Addr, Offset, Count) ) {

		for( UINT n = 0; n < Count; n++, Offset++ ) {

			m_pTx[Offset] = pData[n] & 0xFF;
			}

		return Count;
		}

	return 0;
	}

UINT CML1Blk::SetWordData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT Offset = 0;

	UINT Count  = uCount;

	if( FindOpParams(Addr, Offset, Count) ) {

		Offset *= sizeof(WORD);

		PBYTE pBuff = m_pTx;

		for( UINT n = 0; n < Count; n++ ) {

			PWORD(pBuff + Offset)[n] = (pData[n] & 0xFFFF);
			}
		
		return Count;
		}

	return 0;
	}

UINT CML1Blk::SetLongData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT Offset = 0;

	UINT Count  = uCount;

	if( FindOpParams(Addr, Offset, Count) ) {

		Offset *= sizeof(WORD);

		PBYTE pBuff = m_pTx;

		for( UINT n = 0; n < Count; n++ ) {

			PDWORD(pBuff + Offset)[n] = pData[n];
			}

		return Count;
		}

	return 0;
	}

BOOL CML1Blk::SetFlags(BYTE bFlags)
{
	m_Flags |= bFlags;

	return TRUE;
	}

BOOL CML1Blk::ClrFlags(BYTE bFlags)
{
	m_Flags &= ~bFlags;

	return TRUE;
	}

// Debugging Help

void CML1Blk::ShowDebug(PCTXT pName, ...)
{
	if( ML1_BLK_DEBUG ) {

		va_list pArgs;

		va_start(pArgs, pName);

		AfxTrace(pName, pArgs);

		va_end(pArgs);
		}
	}

void CML1Blk::Show(void)
{
	if( ML1_BLK_DEBUG ) {
	
		AREF Addr = (AREF)m_Addr;

		ShowDebug("\nBlock %8.8x %u %u %u %u", m_Addr, m_Size, m_COV, m_TO, m_Retry);
		}
	}

// End of File
