
#include "ml1drv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Driver Protocol
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//


// Constructor

CML1Driver::CML1Driver(void)
{
	
	}	

// Destructor

CML1Driver::~CML1Driver(void)
{
	
	}

// Configuration

void MCALL CML1Driver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_AuthInterval	= GetLong(pData) * 1000;

		// Server is set via authentication!

		m_Server	= 0;
		}
	}

// Entry Points

void MCALL CML1Driver::Service(void)
{
	// Server Servicing

	Recv();

	Listen();

	if( !IsAuthenticated(NULL) ) {

		return;
		}

	// Client Servicing

	CML1Dev * pDev = m_pHead;
	
	while( pDev ) {

		if( IsAuthenticated(m_fClient ? NULL : pDev) && pDev->m_fOnline ) {

			CML1Blk * pBlock = pDev->m_pHead;

			while( pBlock ) {

				BOOL fSend = pBlock->Execute(m_pExtra);

				if( pBlock->Request(opWrite, fSend) ) {

					pBlock->SetFlags(opForce);

					fSend = FALSE;					
					}

				pBlock = pBlock->m_pNext;
				}
			}

		pDev = pDev->m_pNext;
		}

	pDev = m_pHead;

	while( pDev ) {

		if( IsAuthenticated(m_fClient ? NULL : pDev) && pDev->m_fOnline ) {

			CML1Blk * pBlock = pDev->m_pHead;

			while( pBlock ) {

				if( pBlock->Force() ) {

					SendBlockRequest(pDev, pBlock, opWrite);

					pBlock->SetNext(opWrite);

					pBlock->ClrFlags(opForce);

					Listen();

					ForceSleep(100);
					}

				pBlock = pBlock->m_pNext;
				}

			DoAuthenticationPoll(pDev);
			}

		pDev = pDev->m_pNext;
		}
	}

CCODE MCALL CML1Driver::Ping(void)
{
	if( GetMacId() ) {

		if( IsAuthenticated(NULL) ) {

			m_pDev->m_fOnline = COMMS_SUCCESS(CMasterDriver::Ping());

			return m_pDev->m_fOnline ? CCODE_SUCCESS : CCODE_ERROR;
			}

		ReqAuthentication(NULL);

		return CCODE_ERROR;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CML1Driver::Read(AREF Address, PDWORD pData, UINT uCount)
{
	if( !IsAuthenticated(m_fClient ? NULL : m_pDev) ) {

		return CCODE_ERROR;
		}

	if( !m_pDev->m_fOnline ) {

		return CCODE_ERROR;
		}

	CAddress Addr;

	Addr.m_Ref = Address.m_Ref - 1;

	if( IsSpecial(Addr) ) {

		CML1Blk * pBlock = CML1Base::FindBlock(m_pDev, Addr);

		if( !pBlock && IsLong(Addr) ) {

			Addr.a.m_Type = addrWordAsWord;

			pBlock = CML1Base::FindBlock(m_pDev, Addr);

			if( pBlock ) {

				DWORD Data[2];

				pBlock->GetWordData(Addr, Data, 2);

				pData[0] = MAKELONG(WORD(Data[1]), WORD(Data[0]));

				return 1;
				}
			}

		if( pBlock ) {

			pBlock->GetData(Addr, pData, uCount);

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CML1Driver::Write(AREF Address, PDWORD pData, UINT uCount)
{
	if( !IsAuthenticated(m_fClient ? NULL : m_pDev) ) {

		return CCODE_ERROR;
		}

	if( !m_pDev->m_fOnline ) {

		return CCODE_ERROR;
		}

	CAddress Addr;

	Addr.m_Ref = Address.m_Ref - 1;

	CML1Blk * pBlock = CML1Base::FindBlock(m_pDev, Addr);

	if( pBlock ) {

		pBlock->Mark(opWrite);

		UINT uReturn = pBlock->SetData(Addr, pData, uCount);

		if( pBlock->SendOnChange() ) {

			UINT Offset = 0;

			UINT Count  = uReturn;

			if( pBlock->FindOpParams(Addr, Offset, Count) ) {

				pBlock->ClrPend(opWrite);

				if( DoBlockWrite(m_pDev, pBlock, Offset, Count) ) {

					pBlock->SetTxTime(modeReqst);

					GetEntitySize(Addr, uReturn);

					MakeMaxCount(Addr, uReturn);

					if( uReturn < uCount ) {

						pBlock->SetNext(opWrite);
						}

					return uCount;
					}
				}
			}

		if( pBlock->IsTimedOut(opWrite, modeReply) ) {

			if( pBlock->IsExhausted(opWrite) ) {

				return CCODE_ERROR;
				}
			}

		return uReturn;
		}
	
	return CCODE_ERROR | CCODE_HARD;
	}

// Special Register Support

void CML1Driver::DoSpecialRead(CML1Dev * pDev)
{
	if( pDev && pDev->m_uSrPoll ) {

		if( IsTimedOut(pDev->m_uSrLast, pDev->m_uSrPoll * 60 * 1000) ) {

			CAddress Addr;

			Addr.a.m_Table = spaceSR;

			Addr.a.m_Extra = 0;

			Addr.a.m_Type  = addrWordAsWord;

			Addr.a.m_Offset = SR_MIN;

			CML1Blk * pBlk = CML1Base::FindBlock(pDev, Addr);

			if( pBlk ) {

				pBlk->Mark(opRead);

				if( DoBlockRead(pDev, pBlk, 0) ) {

					pDev->m_uSrLast = GetTickCount();
					}
				}
			}
		}
	}

void CML1Driver::DoAuthenticationPoll(CML1Dev * pDev)
{
	if( pDev && pDev->m_uSrPoll ) {

		if( IsTimedOut(pDev->m_uSrLast, pDev->m_uSrPoll * 60 * 1000) ) {

			m_uTxPtr = 0;

			AddByte(authPoll);

			AddMac(pDev);

			if( Send() ) {

				pDev->m_uSrLast = GetTickCount();
				}
			}
		}
	}


// Authentication Support

BOOL CML1Driver::GetMacId(void)
{
	if( IsMacNULL() ) {

		if( m_fClient ) {

			m_pExtra->GetMacId(0, m_pDev->m_Mac.m_Addr); 
			}

		return m_pExtra->GetMacId(0, m_Mac.m_Addr);
		}

	return TRUE;
	}

BOOL CML1Driver::IsAuthenticated(CML1Dev * pDev)
{
	if( pDev ) {

		return pDev->m_fAuthentic && pDev->m_Unit > 0 && !IsMacNULL(pDev) ? TRUE : FALSE;
		}

	return m_fAuthentic && m_Unit > 0 && !IsMacNULL() ? TRUE : FALSE;
	}

BOOL CML1Driver::IsAuthPollTimedOut(CML1Dev * pDev)
{
	if( pDev ) {

		return m_AuthInterval && IsTimedOut(pDev->m_uAuthPoll, m_AuthInterval);
		}

	return m_AuthInterval && IsTimedOut(m_uAuthPoll, m_AuthInterval);
	}

void CML1Driver::SetAuthPoll(CML1Dev * pDev)
{
	if( pDev ) {

		pDev->m_uAuthPoll = GetTickCount();

		return;
		}

	m_uAuthPoll = GetTickCount();
	}

BOOL CML1Driver::ReqAuthentication(CML1Dev * pDev)
{
	if( IsAuthPollTimedOut(pDev) ) {

		m_uTxPtr = 0;

		AddByte(authPoll);

		AddMac(pDev);

		if( Send() ) {

			SetAuthPoll(pDev);

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
