
//////////////////////////////////////////////////////////////////////////
//
// Quicksilver Driver
//
#include "quickdef.hpp" // Get defines

//////////////////////////////////////////////////////////////////////////
//
// Quicksilver Driver
//

class CQuicksilverDriver : public CMasterDriver
{
	public:
		// Constructor
		CQuicksilverDriver(void);

		// Destructor
		~CQuicksilverDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_bActiveDrop;
			BYTE	m_bPingCount;
			DWORD	m_dDropStartTime;
			DWORD	m_dDropInterval;

			// Data Cache
			DWORD	Cache[APSIZE];

			// Cache Pointer
			PDWORD	m_pArray;
			};

		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[XSIZE];
		BYTE	m_bRx[XSIZE];
		UINT	m_uPtr;
		BYTE	UserR1[USRSIZE];
		BYTE	UserR2[USRSIZE];
		BYTE	UserR3[USRSIZE];
		BYTE	UserR4[USRSIZE];
		BYTE	UserW1[USRSIZE];
		BYTE	UserW2[USRSIZE];
		BYTE	UserW3[USRSIZE];
		BYTE	UserW4[USRSIZE];
		BYTE	UserW5[USRSIZE];
		BYTE	UserW6[USRSIZE];
		BYTE	UserW7[USRSIZE];
		BYTE	UserW8[USRSIZE];

		// Device Specifiers
		BYTE	m_bGroup;

		// Time values
		DWORD	m_t4000;
		DWORD	m_t1000;

		// Operation Specifiers
		UINT	m_uAddress;
		UINT	m_uCmd;

		// Response Fields
		UINT	m_AddrResponse;
		UINT	m_CmndResponse;
		DWORD	m_DataResponse;

		// Hex Lookup
		LPCTXT	m_pHex;

		// Comm Error info
		DWORD	m_dErrorCount;
		BYTE	m_bWriteErr;

		// Command Definition Table
		static QUICKSILVERCmdDef CODE_SEG CL[];
		QUICKSILVERCmdDef FAR * m_pCL;
		QUICKSILVERCmdDef FAR * m_pQItem;
	
		// Opcode Handlers
		BOOL	Read(void);
		UINT	Write(DWORD dData);
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, UINT uCount, BOOL fSigned);
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	CheckResponse(void);
		
		// Port Access
		void	Put(BYTE b);
		UINT	Get(UINT uTime);

		// Find Command Item
		void	SetpItem( AREF Addr );

		// Helpers
		BOOL	IsCommandOnly(void);
		BOOL	OneWriteSize(void);
		DWORD	GetValue(UINT * pPos);
		BOOL	FindSpace( UINT * pPos );
		UINT	BaseCmd(void);
		BOOL	NoReadTransmit(PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(PDWORD pData, UINT uCount);
		void	SelectUserArray(UINT uID, UINT uArrPos);
		void	GetUserArray(PDWORD pData, UINT uCount);
		void	PutUserArray(PDWORD pData, UINT uCount);
		UINT	UserToTx(UINT uID);
		void	RxToUser(void);
		UINT	FindUserEnd(PBYTE pBuff);
		void	ClearUserStrings(void);
		BOOL	IsReadOrSendCommand(void);
		BOOL	IsUserCommand(void);
	};

// End of File
