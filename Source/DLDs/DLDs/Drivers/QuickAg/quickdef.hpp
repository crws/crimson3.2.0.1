
//////////////////////////////////////////////////////////////////////////
//
// Quicksilver Driver
//

#define QUICKSILVER_ID 0x3356

// Command Types

#define RO	0	// Read Only
#define RW	1	// Read and Write
#define WO	2	// Write only - return 0 on read
#define	WO0	3	// Write only - return RTNZEROS on read
#define	WC	4	// Command with no data

#define	RTNZEROS	409600000 // 4 hex 0's & 5 dec. 0's as LSD's

#define	XSIZE	80	// Tx and Rx Size

// Cache Constants
#define CACHED	0xF000 // data cached for multiple write
#define DUPCMD	0xA000 // opcode duplicate, data dependent operation

#define CACHE1	0x1000
#define	CACHE2	0x2000
#define	CACHE3	0x3000
#define	CACHE4	0x4000
#define	CACHE5	0x5000
#define	CACHE6	0x6000
#define	CACHE7	0x7000
#define	CACHE8	0x8000


// Basic Command Formats - Transmitted numbers are in decimal, Responses are in Hex
// Command        = "@ <unit addr> <command number> [write data] <CR>"
// Read Response  = "# <unit addr> <command number> <data> <CR>"
// Write Response = "* <unit addr> <CR>"
// NAK Response   = "! <unit addr> <command number> <error number> <CR>"

// Command Definitions

struct FAR QUICKSILVERCmdDef {
	UINT	uID;		// Config Table/Offset
	UINT	uCmd;		// Quicksilver command value/Cache position indicator
	UINT	uWrite;		// Read/Write Status
	UINT	uSize;		// Integer size, or # of cached items
	BOOL	fSigned;	// Signed int
	UINT	uArrPos;	// Array position
	};

// NAK
#define	ERRNAK	'!'

#define	OPPOL	0
#define	OPRPB	6
#define OPREG	12
#define	OPRIS	20
#define OPRIO	21
#define	OPCIO	188
#define	OPCII	31

// Commands Only
#define	OPCPL	1
#define	OPHLT	2
#define	OPRST	4
#define	OPSDL	9
#define	OPRUN	10
#define	OPSSP	17
#define	OPSSE	18
#define	OPEND	128
#define OPWDL	141
#define OPGCL	142
#define OPGOP	143
#define OPZTG	144
#define OPZTP	145
#define OPTTP	146
#define OPCME	147
#define	OPCIS	163
#define	OPEEM	170
#define	OPDDB	171
#define	OPDEM	171
#define	OPKED	182
#define	OPKDD	183
#define	OPEDL	187
#define	OPMDC	190
#define	OPSCF	195
#define OPEMT	225
#define OPDMT	226
#define OPEMD	227
#define OPDMD	228
#define OPHSM	229
#define OPPCG	231
#define OPPCM	232
#define OPPMX	242
#define OPDLC	243
#define OPSLC	244
#define	OPEDH	251
#define	OPIMS	253
#define	OPIMQ	254
#define	OPRSP	255


// Write One Value
#define	OPSTP	3
#define	OPSPR	13
#define	OPADX	64	// Added Oct 06
#define	OPCER	65	// Added Oct 06
#define	OPT1F	75	// Added Oct 06
#define	OPT2K	77	// Added Oct 06
#define	OPSEF	130
#define	OPLVP	131
#define	OPDLY	140
#define	OPWRP	154
#define	OPOLP	152
#define	OPLRP	156
#define	OPCLM	166
#define	OPADL	173
#define	OPSSD	180
#define	OPKMR	181
#define	OPDIR	184
#define	OPSIF	186
#define	OPMDT	191
#define	OPSOB	205
#define	OPCOB	206
#define	OPPLR	208
#define	OPLVT	212
#define	OPOVT	213
#define	OPMTT	214
#define	OPAHM	219
#define	OPSSL	221
#define	OPRSD	223
#define	OPAHD	230
#define	OPGOC	237
#define	OPPCP	245
#define	OPPLG	300
#define OPPUN	301
#define OPERR	302

// Cached data commands
#define	OPLPR	14
#define	OPLPRA	CACHE1+OPLPR
#define	OPLPRC	CACHE2+OPLPR
#define	OPVMI	15
#define	OPVMIA	CACHE1+OPVMI
#define	OPVMIV	CACHE2+OPVMI
#define	OPVMIE	CACHE3+OPVMI
#define	OPVMIS	CACHE4+OPVMI
#define	OPWRX	30
#define	OPWRXO	CACHE1+OPWRX
#define	OPWRXR	CACHE2+OPWRX
#define	OPWRXD	CACHE3+OPWRX
#define	OPRRW	32
#define	OPRRWO	CACHE1+OPRRW
#define	OPRRWR	CACHE2+OPRRW
#define	OPRRWD	CACHE3+OPRRW
#define	OPPUP	33
#define	OPPUPC	CACHE1+OPPUP
#define	OPPUPA	CACHE2+OPPUP
#define	OPPWO	129	
#define	OPPWOR	CACHE1+OPPWO
#define	OPPWOM	CACHE2+OPPWO
#define	OPMAV	134
#define	OPMAVP	CACHE1+OPMAV
#define	OPMAVA	CACHE2+OPMAV
#define	OPMAVV	CACHE3+OPMAV
#define	OPMAVE	CACHE4+OPMAV
#define	OPMAVS	CACHE5+OPMAV
#define	OPMRV	135
#define	OPMRVD	CACHE1+OPMRV
#define	OPMRVA	CACHE2+OPMRV
#define	OPMRVV	CACHE3+OPMRV
#define	OPMRVE	CACHE4+OPMRV
#define	OPMRVS	CACHE5+OPMRV
#define	OPWCL	138
#define	OPWCLR	CACHE1+OPWCL
#define	OPWCLA	CACHE2+OPWCL
#define	OPWCW	139
#define	OPWCWR	CACHE1+OPWCW
#define	OPWCWA	CACHE2+OPWCW
#define	OPCTC	148
#define	OPCTC1	CACHE1+OPCTC
#define	OPCTC2	CACHE2+OPCTC
#define	OPCTCV	CACHE3+OPCTC
#define	OPCTCB	CACHE4+OPCTC
#define	OPCTCF	CACHE5+OPCTC
#define	OPCTCP	CACHE6+OPCTC
#define	OPCTCI	CACHE7+OPCTC
#define	OPTQL	149
#define	OPTQLCH	CACHE1+OPTQL
#define	OPTQLCM	CACHE2+OPTQL
#define	OPTQLOH	CACHE3+OPTQL
#define	OPTQLOM	CACHE4+OPTQL
#define	OPAHC	150
#define	OPAHCOC	CACHE1+OPAHC
#define	OPAHCCO	CACHE2+OPAHC
#define	OPERL	151
#define	OPERLM	CACHE1+OPERL
#define	OPERLH	CACHE2+OPERL
#define	OPERLD	CACHE3+OPERL
#define	OPWRF	154
#define	OPWRFR	CACHE1+OPWRF
#define	OPWRFD	CACHE2+OPWRF
#define	OPIDT	155
#define	OPIDTG	CACHE1+OPIDT
#define	OPIDTU	CACHE2+OPIDT
#define	OPCLX	158
#define	OPCLX1	CACHE1+OPCLX
#define	OPCLX2	CACHE2+OPCLX
#define	OPCLXO	CACHE3+OPCLX
#define	OPCLXA	CACHE4+OPCLX
#define	OPVMP	159
#define	OPVMPA	CACHE1+OPVMP
#define	OPVMPV	CACHE2+OPVMP
#define	OPVMPE	CACHE3+OPVMP
#define	OPVMPS	CACHE4+OPVMP
#define	OPRAV	160
#define	OPRAVD	CACHE1+OPRAV
#define	OPRAVA	CACHE2+OPRAV
#define	OPRAVV	CACHE3+OPRAV
#define	OPRAVE	CACHE4+OPRAV
#define	OPRAVS	CACHE5+OPRAV
#define	OPRRV	161
#define	OPRRVD	CACHE1+OPRRV
#define	OPRRVA	CACHE2+OPRRV
#define	OPRRVV	CACHE3+OPRRV
#define	OPRRVE	CACHE4+OPRRV
#define	OPRRVS	CACHE5+OPRRV
#define	OPJMP	162
#define	OPJMPE	CACHE1+OPJMP
#define	OPJMPS	CACHE2+OPJMP
#define	OPJMPA	CACHE3+OPJMP
#define	OPJOI	DUPCMD+162
#define	OPJOIC	CACHE1+OPJOI
#define	OPJOIS	CACHE2+OPJOI
#define	OPJOIA	CACHE3+OPJOI
#define	OPCKS	164
#define	OPCKSE	CACHE1+OPCKS
#define	OPCKSS	CACHE2+OPCKS
#define	OPCLCA	165
#define	OPCLC1	CACHE1+OPCLCA
#define	OPCLC2	CACHE2+OPCLCA
#define	OPKMC	167
#define	OPKMCE	CACHE1+OPKMC
#define	OPKMCS	CACHE2+OPKMC
#define	OPMCT	168
#define	OPMCT1	CACHE1+OPMCT
#define	OPMCT2	CACHE2+OPMCT
#define	OPMCT3	CACHE3+OPMCT
#define	OPMCT4	CACHE4+OPMCT
#define	OPMCT5	CACHE5+OPMCT
#define	OPMCT6	CACHE6+OPMCT
#define	OPMCT7	CACHE7+OPMCT
#define	OPMCT8	CACHE8+OPMCT
#define	OPFLC	169
#define	OPFLC1	CACHE1+OPFLC
#define	OPFLC2	CACHE2+OPFLC
#define	OPFLCA	CACHE3+OPFLC
#define	OPPAC	172
#define	OPPAC1A	CACHE1+OPPAC
#define	OPPAC2A	CACHE2+OPPAC
#define	OPPACL	CACHE3+OPPAC
#define	OPMAT	176
#define	OPMATP	CACHE1+OPMAT
#define	OPMATA	CACHE2+OPMAT
#define	OPMATT	CACHE3+OPMAT
#define	OPMATE	CACHE4+OPMAT
#define	OPMATS	CACHE5+OPMAT
#define	OPMRT	177
#define	OPMRTD	CACHE1+OPMRT
#define	OPMRTA	CACHE2+OPMRT
#define	OPMRTT	CACHE3+OPMRT
#define	OPMRTE	CACHE4+OPMRT
#define	OPMRTS	CACHE5+OPMRT
#define	OPRAT	178
#define	OPRATD	CACHE1+OPRAT
#define	OPRATA	CACHE2+OPRAT
#define	OPRATT	CACHE3+OPRAT
#define	OPRATE	CACHE4+OPRAT
#define	OPRATS	CACHE5+OPRAT
#define	OPRRT	179
#define	OPRRTD	CACHE1+OPRRT
#define	OPRRTA	CACHE2+OPRRT
#define	OPRRTT	CACHE3+OPRRT
#define	OPRRTE	CACHE4+OPRRT
#define	OPRRTS	CACHE5+OPRRT
#define	OPMDS	189
#define	OPMDSC	CACHE1+OPMDS
#define	OPMDSE	CACHE2+OPMDS
#define	OPMDSF	CACHE3+OPMDS
#define	OPSEE	192
#define	OPSEEI	CACHE1+OPSEE
#define	OPSEES	CACHE2+OPSEE
#define	OPSEEE	CACHE3+OPSEE
#define	OPARI	193
#define	OPARIC	CACHE1+OPARI
#define	OPARID	CACHE2+OPARI
#define	OPWBS	194
#define	OPWBSC	CACHE1+OPWBS
#define	OPWBSS	CACHE2+OPWBS
#define	OPRSM	196
#define	OPRSMN	CACHE1+OPRSM
#define	OPRSMD	CACHE2+OPRSM
#define	OPRSMA	CACHE3+OPRSM
#define	OPRLM	197
#define	OPRLMN	CACHE1+OPRLM
#define	OPRLMD	CACHE2+OPRLM
#define	OPRLMA	CACHE3+OPRLM
#define	OPRSN	198
#define	OPRSND	CACHE1+OPRSN
#define	OPRSNA	CACHE2+OPRSN
#define	OPRLN	199
#define	OPRLND	CACHE1+OPRLN
#define	OPRLNA	CACHE2+OPRLN
#define	OPCLD	200
#define	OPCLDR	CACHE1+OPCLD
#define	OPCLDD	CACHE2+OPCLD
#define	OPCLDO	CACHE3+OPCLD
#define	OPCLDA	CACHE4+OPCLD
#define	OPPCL	201
#define	OPPCLL	CACHE1+OPPCL
#define	OPPCLE	CACHE2+OPPCL
#define	OPPCLS	CACHE3+OPPCL
#define	OPPCI	DUPCMD+201
#define	OPPCIL	CACHE1+OPPCI
#define	OPPCIE	CACHE2+OPPCI
#define	OPPCIS	CACHE3+OPPCI
#define	OPPRT	202
#define	OPPRTE	CACHE1+OPPRT
#define	OPPRTS	CACHE2+OPPRT
#define	OPPRI	DUPCMD+202
#define	OPPRIE	CACHE1+OPPRI
#define	OPPRIS	CACHE2+OPPRI
#define	OPWBE	204
#define	OPWBEC	CACHE1+OPWBE
#define	OPWBET	CACHE2+OPWBE
#define	OPACR	207
#define	OPACRC	CACHE1+OPACR
#define	OPACRD	CACHE2+OPACR
#define	OPCLC	215
#define	OPCLCO	CACHE1+OPCLC
#define	OPCLCD	CACHE2+OPCLC
#define	OPCTW	215
#define	OPCTWO	CACHE1+OPCTW
#define	OPCTWR	CACHE2+OPCTW
#define	OPPIM	216
#define	OPPIMF	CACHE1+OPPIM
#define	OPPIME	CACHE2+OPPIM
#define	OPPIMS	CACHE3+OPPIM
#define	OPVIM	217
#define	OPVIMF	CACHE1+OPVIM
#define	OPVIME	CACHE2+OPVIM
#define	OPVIMS	CACHE3+OPVIM
#define	OPTIM	218
#define	OPTIMF	CACHE1+OPTIM
#define	OPTIME	CACHE2+OPTIM
#define	OPTIMS	CACHE3+OPTIM
#define	OPKMX	220
#define	OPKMX1	CACHE1+OPKMX
#define	OPKMX2	CACHE2+OPKMX
#define	OPKMX3	CACHE3+OPKMX
#define	OPKMX4	CACHE4+OPKMX
#define	OPKMX5	CACHE5+OPKMX
#define	OPKMX6	CACHE6+OPKMX
#define	OPTRU	222
#define	OPTRUF	CACHE1+OPTRU
#define	OPTRUI	CACHE2+OPTRU
#define	OPXRV	233
#define	OPXRVD	CACHE1+OPXRV
#define	OPXRVE	CACHE2+OPXRV
#define	OPXRVS	CACHE3+OPXRV
#define	OPXAV	234
#define	OPXAVD	CACHE1+OPXAV
#define	OPXAVE	CACHE2+OPXAV
#define	OPXAVS	CACHE3+OPXAV
#define	OPXRT	235
#define	OPXRTD	CACHE1+OPXRT
#define	OPXRTE	CACHE2+OPXRT
#define	OPXRTS	CACHE3+OPXRT
#define	OPXAT	236
#define	OPXATD	CACHE1+OPXAT
#define	OPXATE	CACHE2+OPXAT
#define	OPXATS	CACHE3+OPXAT
#define	OPJAN	250
#define	OPJANE	CACHE1+OPJAN
#define	OPJANS	CACHE2+OPJAN
#define	OPJANL	CACHE3+OPJAN
#define	OPJNA	238
#define	OPJNAE	CACHE1+OPJNA
#define	OPJNAS	CACHE2+OPJNA
#define	OPJNAL	CACHE3+OPJNA
#define	OPJOR	239
#define	OPJORE	CACHE1+OPJOR
#define	OPJORS	CACHE2+OPJOR
#define	OPJORL	CACHE3+OPJOR
#define	OPPMC	240
#define	OPPMCE	CACHE1+OPPMC
#define	OPPMCS	CACHE2+OPPMC
#define	OPPMV	241
#define	OPPMVE	CACHE1+OPPMV
#define	OPPMVS	CACHE2+OPPMV
#define	OPATR	248
#define	OPATRA	CACHE1+OPATR
#define	OPATRD	CACHE2+OPATR
#define	OPPMO	249
#define	OPPMOE	CACHE1+OPPMO
#define	OPPMOS	CACHE2+OPPMO
#define	OPDIF	252
#define	OPDIFL	CACHE1+OPDIF
#define	OPDIFF	CACHE2+OPDIF

// Added Oct 06
#define	OPIMW	25
#define	OPIMWT	CACHE1+OPIMW
#define	OPIMWP	CACHE2+OPIMW
#define	OPIMWA	CACHE3+OPIMW
#define	OPIMWV	CACHE4+OPIMW
#define	OPETP	66
#define	OPETP1	CACHE1+OPETP
#define	OPETP2	CACHE2+OPETP
#define	OPETP3	CACHE3+OPETP
#define	OPETP4	CACHE4+OPETP
#define	OPETP5	CACHE5+OPETP
#define	OPETP6	CACHE6+OPETP
#define	OPETN	67
#define	OPETN1	CACHE1+OPETN
#define	OPETN2	CACHE2+OPETN
#define	OPETN3	CACHE3+OPETN
#define	OPETN4	CACHE4+OPETN
#define	OPETN5	CACHE5+OPETN
#define	OPETN6	CACHE6+OPETN
#define	OPF2L	68
#define	OPF2LD	CACHE1+OPF2L
#define	OPF2LS	CACHE2+OPF2L
#define	OPF2LA	CACHE3+OPF2L
#define	OPF2LV	CACHE4+OPF2L
#define	OPF2L1	CACHE5+OPF2L
#define	OPF2L2	CACHE6+OPF2L
#define	OPVLL	69
#define	OPVLLM	CACHE1+OPVLL
#define	OPVLLH	CACHE2+OPVLL
#define	OPC2T	70
#define	OPC2T1	CACHE1+OPC2T
#define	OPC2T2	CACHE2+OPC2T
#define	OPC2TV	CACHE3+OPC2T
#define	OPC2TB	CACHE4+OPC2T
#define	OPC2TC	CACHE5+OPC2T
#define	OPC2TF	CACHE6+OPC2T
#define	OPC2TP	CACHE7+OPC2T
#define	OPC2TI	CACHE8+OPC2T
#define	OPT2S	76
#define	OPT2SA	CACHE1+OPT2S
#define	OPT2SZ	CACHE2+OPT2S
#define	OPELR	90
#define	OPELRP	CACHE1+OPELR
#define	OPELRE	CACHE2+OPELR
#define	OPRGG	91
#define	OPRGGM	CACHE1+OPRGG
#define	OPRGGR	CACHE2+OPRGG
#define	OPRGGC	CACHE3+OPRGG
#define	OPSSI	92
#define	OPSSIM	CACHE1+OPSSI
#define	OPSSIR	CACHE2+OPSSI
#define	OPSSIO	CACHE3+OPSSI
#define	OPPVC	93
#define	OPPVCM	CACHE1+OPPVC
#define	OPPVCR	CACHE2+OPPVC
#define	OPPVCE	CACHE3+OPPVC
#define	OPPVCS	CACHE4+OPPVC
#define	OPEMN	192
#define	OPEMNM	CACHE1+OPEMN
#define	OPEMNI	CACHE2+OPEMN
#define	OPEMNR	CACHE3+OPEMN

#define	IDCIO	1
#define	IDRPB	2
#define	IDREG	3
#define	IDWRP	4
#define	IDCII	5
#define	IDCIE	6
#define	IDCIIE	7

#define	USRMIN	20
#define	USRMAX	44

#define	USRCMD	44
#define	USRRSP	USRCMD
#define	USRSIZE	USRCMD * 2
#define	USRQTY	USRCMD/sizeof(DWORD)

#define	ID_CR1	20 + 400
#define	ID_RR1	21 + 400
#define	ID_CR2	22 + 400
#define	ID_RR2	23 + 400
#define	ID_CR3	24 + 400
#define	ID_RR3	25 + 400
#define	ID_CR4	26 + 400
#define	ID_RR4	27 + 400
#define	ID_CW1	28 + 400
#define	ID_RW1	29 + 400
#define	ID_CW2	30 + 400
#define	ID_RW2	31 + 400
#define	ID_CW3	32 + 400
#define	ID_RW3	33 + 400
#define	ID_CW4	34 + 400
#define	ID_RW4	35 + 400
#define	ID_CW5	36 + 400
#define	ID_RW5	37 + 400
#define	ID_CW6	38 + 400
#define	ID_RW6	39 + 400
#define	ID_CW7	40 + 400
#define	ID_RW7	41 + 400
#define	ID_CW8	42 + 400
#define	ID_RW8	43 + 400
#define	ID_SEND	44 + 400
// Address Named ID's
#define	IDADL	8
#define	IDAHC	9
#define	IDAHCOC	10
#define	IDAHCCO	11
#define	IDAHD	12
#define	IDARI	13
#define	IDARIC	14
#define	IDARID	15
#define	IDATR	16
#define	IDATRA	17
#define	IDATRD	18
#define	IDCIS	19
#define	IDCKS	20
#define	IDCKSE	21
#define	IDCKSS	22
#define	IDCLC	23
#define	IDCLCO	24
#define	IDCLCD	25
#define	IDCME	26
#define	IDCOB	27
#define	IDCPL	28
#define	IDCTC	29
#define	IDCTC1	30
#define	IDCTC2	31
#define	IDCTCV	32
#define	IDCTCF	33
#define	IDCTCB	34
#define	IDCTCP	35
#define	IDCTCI	36
#define	IDDEM	37
#define	IDDDB	38
#define	IDDIF	39
#define	IDDIFL	40
#define	IDDIFF	41
#define	IDDIR	42
#define	IDDLC	43
#define	IDDLY	44
#define	IDDMD	45
#define	IDDMT	46
#define	IDEDH	47
#define	IDEDL	48
#define	IDEEM	49
#define	IDEMD	50
#define	IDEMT	51
#define	IDEND	52
#define	IDERL	53
#define	IDERLM	54
#define	IDERLH	55
#define	IDERLD	56
#define	IDFLC	57
#define	IDFLC1	58
#define	IDFLC2	59
#define	IDFLCA	60
#define	IDGCL	61
#define	IDGOC	62
#define	IDGOP	63
#define	IDHSM	64
#define	IDIDT	65
#define	IDIDTG	66
#define	IDIDTU	67
#define	IDJAN	68
#define	IDJANE	69
#define	IDJANS	70
#define	IDJANL	71
#define	IDJMP	72
#define	IDJMPE	73
#define	IDJMPS	74
#define	IDJMPA	75
#define	IDJNA	76
#define	IDJNAE	77
#define	IDJNAS	78
#define	IDJNAL	79
#define	IDJOI	80
#define	IDJOIC	81
#define	IDJOIS	82
#define	IDJOIA	83
#define	IDJOR	84
#define	IDJORE	85
#define	IDJORS	86
#define	IDJORL	87
#define	IDKDD	88
#define	IDKED	89
#define	IDKMC	90
#define	IDKMCE	91
#define	IDKMCS	92
#define	IDKMR	93
#define	IDLPR	94
#define	IDLPRA	95
#define	IDLPRC	96
#define	IDLRP	97
#define	IDLVT	98
#define	IDMAT	99
#define	IDMATP	100
#define	IDMATA	101
#define	IDMATT	102
#define	IDMATE	103
#define	IDMATS	104
#define	IDMAV	105
#define	IDMAVP	106
#define	IDMAVA	107
#define	IDMAVV	108
#define	IDMAVE	109
#define	IDMAVS	110
#define	IDMDC	111
#define	IDMDS	112
#define	IDMDSC	113
#define	IDMDSE	114
#define	IDMDSF	115
#define	IDMDT	116
#define	IDMRT	117
#define	IDMRTD	118
#define	IDMRTA	119
#define	IDMRTT	120
#define	IDMRTE	121
#define	IDMRTS	122
#define	IDMRV	123
#define	IDMRVD	124
#define	IDMRVA	125
#define	IDMRVV	126
#define	IDMRVE	127
#define	IDMRVS	128
#define	IDMTT	129
#define	IDOLP	130
#define	IDOVT	131
#define	IDPAC	132
#define	IDPAC1A	133
#define	IDPAC2A	134
#define	IDPACL	135
#define	IDPCG	136
#define	IDPCI	137
#define	IDPCIL	138
#define	IDPCIE	139
#define	IDPCIS	140
#define	IDPCL	141
#define	IDPCLL	142
#define	IDPCLE	143
#define	IDPCLS	144
#define	IDPCM	145
#define	IDPCP	146
#define	IDPIM	147
#define	IDPIMF	148
#define	IDPIME	149
#define	IDPIMS	150
#define	IDPLG	151
#define	IDPLR	152
#define	IDPMC	153
#define	IDPMCE	154
#define	IDPMCS	155
#define	IDPMO	156
#define	IDPMOE	157
#define	IDPMOS	158
#define	IDPMV	159
#define	IDPMVE	160
#define	IDPMVS	161
#define	IDPMX	162
#define	IDPOL	163
#define	IDPRI	164
#define	IDPRIE	165
#define	IDPRIS	166
#define	IDPRT	167
#define	IDPRTE	168
#define	IDPRTS	169
#define	IDPUN	170
#define	IDRAT	171
#define	IDRATD	172
#define	IDRATA	173
#define	IDRATT	174
#define	IDRATE	175
#define	IDRATS	176
#define	IDRAV	177
#define	IDRAVD	178
#define	IDRAVA	179
#define	IDRAVV	180
#define	IDRAVE	181
#define	IDRAVS	182
#define	IDRIO	183
#define	IDRIS	184
#define	IDRLM	185
#define	IDRLMN	186
#define	IDRLMD	187
#define	IDRLMA	188
#define	IDRLN	189
#define	IDRLND	190
#define	IDRLNA	191
#define	IDRRT	192
#define	IDRRTD	193
#define	IDRRTA	194
#define	IDRRTT	195
#define	IDRRTE	196
#define	IDRRTS	197
#define	IDRRV	198
#define	IDRRVD	199
#define	IDRRVA	200
#define	IDRRVV	201
#define	IDRRVE	202
#define	IDRRVS	203
#define	IDRSD	204
#define	IDRSM	205
#define	IDRSMN	206
#define	IDRSMD	207
#define	IDRSMA	208
#define	IDRSN	209
#define	IDRSND	210
#define	IDRSNA	211
#define	IDRST	212
#define	IDRUN	213
#define	IDSCF	214
#define	IDSEE	215
#define	IDSEEI	216
#define	IDSEES	217
#define	IDSEEE	218
#define	IDSIF	219
#define	IDSLC	220
#define	IDSOB	221
#define	IDSSD	222
#define	IDSSE	223
#define	IDSSL	224
#define	IDSSP	225
#define	IDSTP	226
#define	IDTQL	227
#define	IDTQLCH	228
#define	IDTQLCM	229
#define	IDTQLOH	230
#define	IDTQLOM	231
#define	IDTRU	232
#define	IDTRUF	233
#define	IDTRUI	234
#define	IDTTP	235
#define	IDVIM	236
#define	IDVIMF	237
#define	IDVIME	238
#define	IDVIMS	239
#define	IDVMI	240
#define	IDVMIA	241
#define	IDVMIV	242
#define	IDVMIE	243
#define	IDVMIS	244
#define	IDVMP	245
#define	IDVMPA	246
#define	IDVMPV	247
#define	IDVMPE	248
#define	IDVMPS	249
#define	IDWBE	250
#define	IDWBEC	251
#define	IDWBET	252
#define	IDWBS	253
#define	IDWBSC	254
#define	IDWBSS	255
#define	IDWDL	256
#define	IDXAT	257
#define	IDXATD	258
#define	IDXATE	259
#define	IDXATS	260
#define	IDXAV	261
#define	IDXAVD	262
#define	IDXAVE	263
#define	IDXAVS	264
#define	IDXRT	265
#define	IDXRTD	266
#define	IDXRTE	267
#define	IDXRTS	268
#define	IDXRV	269
#define	IDXRVD	270
#define	IDXRVE	271
#define	IDXRVS	272
#define	IDZTG	273
#define	IDZTP	274
#define	IDERR	275
#define	IDAHM	276
#define	IDCLM	277
#define	IDLVP	278
#define	IDSEF	279
#define	IDSPR	280
#define	IDIMQ	281
#define	IDIMS	282
#define	IDRSP	284
#define	IDPWO	286
#define	IDPWOR	287
#define	IDPWOM	288
#define	IDKMX	289
#define	IDKMX1	290
#define	IDKMX2	291
#define	IDKMX3	292
#define	IDKMX4	293
#define	IDKMX5	294
#define	IDKMX6	295
#define	IDMCT	296
#define	IDMCT1	297
#define	IDMCT2	298
#define	IDMCT3	299
#define	IDMCT4	300
#define	IDMCT5	301
#define	IDMCT6	302
#define	IDMCT7	303
#define	IDMCT8	304
#define	IDSDL	305
#define	IDCLCA	306
#define	IDCLC1	307
#define	IDCLC2	308
#define	IDCLD	309
#define	IDCLDR	310
#define	IDCLDD	311
#define	IDCLDO	312
#define	IDCLDA	313
#define	IDCLX	314
#define	IDCLX1	315
#define	IDCLX2	316
#define	IDCLXO	317
#define	IDCLXA	318
#define	IDCTW	319
#define	IDCTWO	320
#define	IDCTWR	321
#define	IDRRW	322
#define	IDRRWO	323
#define	IDRRWR	324
#define	IDRRWD	325
#define	IDWCL	326
#define	IDWCLR	327
#define	IDWCLA	328
#define	IDWCW	329
#define	IDWCWR	330
#define	IDWCWA	331
#define	IDWRF	332
#define	IDWRFR	333
#define	IDWRFD	334
#define	IDWRX	335
#define	IDWRXO	336
#define	IDWRXR	337
#define	IDWRXD	338
#define	IDPUP	339
#define	IDPUPC	340
#define	IDPUPA	341
#define	IDTIM	342
#define	IDTIMF	343
#define	IDTIME	344
#define	IDTIMS	345
#define	IDHLT	346
#define	IDADX	347
#define	IDCER	348
#define	IDT1F	349
#define	IDT2K	350
#define	IDC2T	351
#define	IDC2T1	352
#define	IDC2T2	353
#define	IDC2TV	354
#define	IDC2TB	355
#define	IDC2TC	356
#define	IDC2TF	357
#define	IDC2TP	358
#define	IDC2TI	359
#define	IDT2S	360
#define	IDT2SA	361
#define	IDT2SZ	362
#define	IDEMN	363
#define	IDEMNM	364
#define	IDEMNI	365
#define	IDEMNR	366
#define	IDELR	367
#define	IDELRP	368
#define	IDELRE	369
#define	IDETN	370
#define	IDETN1	371
#define	IDETN2	372
#define	IDETN3	373
#define	IDETN4	374
#define	IDETN5	375
#define	IDETN6	376
#define	IDETP	377
#define	IDETP1	378
#define	IDETP2	379
#define	IDETP3	380
#define	IDETP4	381
#define	IDETP5	382
#define	IDETP6	383
#define	IDF2L	384
#define	IDF2LD	385
#define	IDF2LS	386
#define	IDF2LA	387
#define	IDF2LV	388
#define	IDF2L1	389
#define	IDF2L2	390
#define	IDIMW	391
#define	IDIMWT	392
#define	IDIMWP	393
#define	IDIMWA	394
#define	IDIMWV	395
#define	IDPVC	396
#define	IDPVCM	397
#define	IDPVCR	398
#define	IDPVCE	399
#define	IDPVCS	400
#define	IDRGG	401
#define	IDRGGM	402
#define	IDRGGR	403
#define	IDRGGC	404
#define	IDACR	405
#define	IDACRC	406
#define	IDACRD	407
#define	IDVLL	409
#define	IDVLLM	410
#define	IDVLLH	411
#define	IDSSI	412
#define	IDSSIM	413
#define	IDSSIR	414
#define	IDSSIO	415

// Cache Array Postions // 1 - 10
#define	APLPRA	1
#define	APLPRC	APLPRA+1
#define	APVMIA	APLPRC+1
#define	APVMIV	APVMIA+1
#define	APVMIE	APVMIV+1
#define	APVMIS	APVMIE+1
#define	APMAVP	APVMIS+1
#define	APMAVA	APMAVP+1
#define	APMAVV	APMAVA+1
#define	APMAVE	APMAVV+1
 // 11 - 20
#define	APMAVS	APMAVE+1
#define	APMRVD	APMAVS+1
#define	APMRVA	APMRVD+1
#define	APMRVV	APMRVA+1
#define	APMRVE	APMRVV+1
#define	APMRVS	APMRVE+1
#define	APCTC1	APMRVS+1
#define	APCTC2	APCTC1+1
#define	APCTCV	APCTC2+1
#define	APCTCB	APCTCV+1
 // 21 - 30
#define	APCTCF	APCTCB+1
#define	APCTCP	APCTCF+1
#define	APCTCI	APCTCP+1
#define	APTQLCH	APCTCI+1
#define	APTQLCM	APTQLCH+1
#define	APTQLOH	APTQLCM+1
#define	APTQLOM	APTQLOH+1
#define	APAHCOC	APTQLOM+1
#define	APAHCCO	APAHCOC+1
#define	APERLM	APAHCCO+1
 // 31 - 40
#define	APERLH	APERLM+1
#define	APERLD	APERLH+1
#define	APVMPA	APERLD+1
#define	APVMPV	APVMPA+1
#define	APVMPE	APVMPV+1
#define	APVMPS	APVMPE+1
#define	APRAVD	APVMPS+1
#define	APRAVA	APRAVD+1
#define	APRAVV	APRAVA+1
#define	APRAVE	APRAVV+1
 // 41 - 50
#define	APRAVS	APRAVE+1
#define	APRRVD	APRAVS+1
#define	APRRVA	APRRVD+1
#define	APRRVV	APRRVA+1
#define	APRRVE	APRRVV+1
#define	APRRVS	APRRVE+1
#define	APJMPE	APRRVS+1
#define	APJMPS	APJMPE+1
#define	APJMPA	APJMPS+1
#define	APJOIC	APJMPA+1
 // 51 - 60
#define	APJOIS	APJOIC+1
#define	APJOIA	APJOIS+1
#define	APCKSE	APJOIA+1
#define	APCKSS	APCKSE+1
#define	APKMCE	APCKSS+1
#define	APKMCS	APKMCE+1
#define	APFLC1	APKMCS+1
#define	APFLC2	APFLC1+1
#define	APFLCA	APFLC2+1
#define	APPAC1A	APFLCA+1
 // 61 - 70
#define	APPAC2A	APPAC1A+1
#define	APPACL	APPAC2A+1
#define	APMATP	APPACL+1
#define	APMATA	APMATP+1
#define	APMATT	APMATA+1
#define	APMATE	APMATT+1
#define	APMATS	APMATE+1
#define	APMRTD	APMATS+1
#define	APMRTA	APMRTD+1
#define	APMRTT	APMRTA+1
 // 71 - 80
#define	APMRTE	APMRTT+1
#define	APMRTS	APMRTE+1
#define	APRATD	APMRTS+1
#define	APRATA	APRATD+1
#define	APRATT	APRATA+1
#define	APRATE	APRATT+1
#define	APRATS	APRATE+1
#define	APRRTD	APRATS+1
#define	APRRTA	APRRTD+1
#define	APRRTT	APRRTA+1
 // 81 - 90
#define	APRRTE	APRRTT+1
#define	APRRTS	APRRTE+1
#define	APMDSC	APRRTS+1
#define	APMDSE	APMDSC+1
#define	APMDSF	APMDSE+1
#define	APSEEI	APMDSF+1
#define	APSEES	APSEEI+1
#define	APSEEE	APSEES+1
#define	APARIC	APSEEE+1
#define	APARID	APARIC+1
 // 91 - 100
#define	APWBSC	APARID+1
#define	APWBSS	APWBSC+1
#define	APRSMN	APWBSS+1
#define	APRSMD	APRSMN+1
#define	APRSMA	APRSMD+1
#define	APRLMN	APRSMA+1
#define	APRLMD	APRLMN+1
#define	APRLMA	APRLMD+1
#define	APRSND	APRLMA+1
#define	APRSNA	APRSND+1
 // 101 - 110
#define	APRLND	APRSNA+1
#define	APRLNA	APRLND+1
#define	APPCLL	APRLNA+1
#define	APPCLE	APPCLL+1
#define	APPCLS	APPCLE+1
#define	APPCIL	APPCLS+1
#define	APPCIE	APPCIL+1
#define	APPCIS	APPCIE+1
#define	APPRTE	APPCIS+1
#define	APPRTS	APPRTE+1
 // 111 - 120
#define	APPRIE	APPRTS+1
#define	APPRIS	APPRIE+1
#define	APWBEC	APPRIS+1
#define	APWBET	APWBEC+1
#define	APACRC	APWBET+1
#define	APACRD	APACRC+1
#define	APCLCO	APACRD+1
#define	APCLCD	APCLCO+1
#define	APPIMF	APCLCD+1
#define	APPIME	APPIMF+1
 // 121 - 130
#define	APPIMS	APPIME+1
#define	APVIMF	APPIMS+1
#define	APVIME	APVIMF+1
#define	APVIMS	APVIME+1
#define	APTRUF	APVIMS+1
#define	APTRUI	APTRUF+1
#define	APXRVD	APTRUI+1
#define	APXRVE	APXRVD+1
#define	APXRVS	APXRVE+1
#define	APXAVD	APXRVS+1
 // 131 - 140
#define	APXAVE	APXAVD+1
#define	APXAVS	APXAVE+1
#define	APXRTD	APXAVS+1
#define	APXRTE	APXRTD+1
#define	APXRTS	APXRTE+1
#define	APXATD	APXRTS+1
#define	APXATE	APXATD+1
#define	APXATS	APXATE+1
#define	APJANE	APXATS+1
#define	APJANS	APJANE+1
 // 141 - 150
#define	APJANL	APJANS+1
#define	APJNAE	APJANL+1
#define	APJNAS	APJNAE+1
#define	APJNAL	APJNAS+1
#define	APJORE	APJNAL+1
#define	APJORS	APJORE+1
#define	APJORL	APJORS+1
#define	APPMCE	APJORL+1
#define	APPMCS	APPMCE+1
#define	APPMVE	APPMCS+1
 // 151 - 160
#define	APPMVS	APPMVE+1
#define	APATRA	APPMVS+1
#define	APATRD	APATRA+1
#define	APPMOE	APATRD+1
#define	APPMOS	APPMOE+1
#define	APDIFL	APPMOS+1
#define	APDIFF	APDIFL+1
#define	APIDTG	APDIFF+1
#define	APIDTU	APIDTG+1
#define	APPWOR	APIDTU+1
 // 161 - 170
#define	APPWOM	APPWOR+1
#define	APKMX1	APPWOM+1
#define	APKMX2	APKMX1+1
#define	APKMX3	APKMX2+1
#define	APKMX4	APKMX3+1
#define	APKMX5	APKMX4+1
#define	APKMX6	APKMX5+1
#define	APMCT1	APKMX6+1
#define	APMCT2	APMCT1+1
#define	APMCT3	APMCT2+1
 // 171 - 180
#define	APMCT4	APMCT3+1
#define	APMCT5	APMCT4+1
#define	APMCT6	APMCT5+1
#define	APMCT7	APMCT6+1
#define	APMCT8	APMCT7+1
#define	APTIMF	APMCT8+1
#define	APTIME	APTIMF+1
#define	APTIMS	APTIME+1
#define	APCLC1	APTIMS+1
#define	APCLC2	APCLC1+1
 // 181 - 190
#define	APCLDR	APCLC2+1
#define	APCLDD	APCLDR+1
#define	APCLDO	APCLDD+1
#define	APCLDA	APCLDO+1
#define	APCLX1	APCLDA+1
#define	APCLX2	APCLX1+1
#define	APCLXO	APCLX2+1
#define	APCLXA	APCLXO+1
#define	APRRWO	APCLXA+1
#define	APRRWR	APRRWO+1
 // 191 - 200
#define	APRRWD	APRRWR+1
#define	APWCLR	APRRWD+1
#define	APWCLA	APWCLR+1
#define	APWCWR	APWCLA+1
#define	APWCWA	APWCWR+1
#define	APWRFR	APWCWA+1
#define	APWRFD	APWRFR+1
#define	APWRXO	APWRFD+1
#define	APWRXR	APWRXO+1
#define	APWRXD	APWRXR+1
 // 201 - 210
#define	APPUPC	APWRXD+1
#define	APPUPA	APPUPC+1
#define	APETP1	APPUPA+1
#define	APETP2	APETP1+1
#define	APETP3	APETP2+1
#define	APETP4	APETP3+1
#define	APETP5	APETP4+1
#define	APETP6	APETP5+1
#define	APETN1	APETP6+1
#define	APETN2	APETN1+1
 // 211 - 220
#define	APETN3	APETN2+1
#define	APETN4	APETN3+1
#define	APETN5	APETN4+1
#define	APETN6	APETN5+1
#define	APF2LD	APETN6+1
#define	APF2LS	APF2LD+1
#define	APF2LA	APF2LS+1
#define	APF2LV	APF2LA+1
#define	APF2L1	APF2LV+1
#define	APF2L2	APF2L1+1
 // 221 - 230
#define	APVLLM	APF2L2+1
#define	APVLLH	APVLLM+1
#define	APC2T1	APVLLH+1
#define	APC2T2	APC2T1+1
#define	APC2TV	APC2T2+1
#define	APC2TB	APC2TV+1
#define	APC2TC	APC2TB+1
#define	APC2TF	APC2TC+1
#define	APC2TP	APC2TF+1
#define	APC2TI	APC2TP+1
 // 231 - 240
#define	APT2SA	APC2TI+1
#define	APT2SZ	APT2SA+1
#define	APELRP	APT2SZ+1
#define	APELRE	APELRP+1
#define	APRGGM	APELRE+1
#define	APRGGR	APRGGM+1
#define	APRGGC	APRGGR+1
#define	APSSIM	APRGGC+1
#define	APSSIR	APSSIM+1
#define	APSSIO	APSSIR+1
 // 241 - 250
#define	APPVCM	APSSIO+1
#define	APPVCR	APPVCM+1
#define	APPVCE	APPVCR+1
#define	APPVCS	APPVCE+1
#define	APIMWT	APPVCS+1
#define	APIMWP	APIMWT+1
#define	APIMWA	APIMWP+1
#define	APIMWV	APIMWA+1
#define	APEMNM	APIMWV+1
#define	APEMNI	APEMNM+1
 // 251 -
#define	APEMNR	APEMNI+1
// Size of Array
#define	APSIZE	APEMNR+1

// End of File
