//////////////////////////////////////////////////////////////////////////
//
// Banner Modbus Data Spaces
//
//
// Spaces
// Starting Modbus Address = 00001
#define	CIT	0x101
#define	CIR	0x102
#define	CIP	0x103
#define	CIS	0x109

// Starting Modbus Address = 10001
#define	COP	0x201
#define	COF	0x202
#define	COE	0x203
#define	COR	0x205
#define	CIO1	0x209
#define	CIO2	0x20A
#define	CIO3	0x20B
#define	CIO4	0x20C
#define	CIO5	0x20D
#define	CIO6	0x20E

// Starting Modbus Address = 10017
#define	CAT	0x300 + 17
#define	CAR	0x300 + 18
#define	CAP	0x300 + 19
#define	CAS	0x300 + 25

// Starting Modbus Address = 30001
#define	CO3O	0x401
#define	CO3A	0x402
#define	CO3I	0x403
#define	CO3E	0x404
#define	CO3M	0x405
#define	CO3P	0x407
#define	CO3F	0x409
#define	CO3T	0x40B
#define	CO3C	0x40D
#define	CO3L	4	// Longs starting at 30033

// Starting Modbus Address = 41001
#define	CO4O	0x501
#define	CO4A	0x502
#define	CO4I	0x503
#define	CO4E	0x504
#define	CO4M	0x505
#define	CO4P	0x507
#define	CO4F	0x509
#define	CO4T	0x50B
#define	CO4C	0x50D
#define	CO4L	5	// Longs starting at 41033

// Starting Modbus Address = 40001
#define	CIRI	0x601
#define	CIRP	0x602
#define	CIRC	6	// String chars 40040 - 40139
#define	CIRM	7	// String chars 40140 - 40239

// Any Modbus Address
#define	CREG	1

// Modbus Spaces
#define	SPI	0x01 // SPACE_INPUT	// addr 1xxxx
#define	SPO	0x02 // SPACE_OUTPUT	// addr 0xxxx
#define	SPH	0x03 // SPACE_HOLDING	// addr 4xxxx
#define	SPA	0x04 // SPACE_ANALOG	// addr 3xxxx

#define	WRITE_NO	FALSE
#define	WRITE_YES	TRUE
//////////////////////////////////////////////////////////////////////////
//
// Banner Modbus TCP/IP Master Driver
//

class CBannerModbusTCP : public CMasterDriver
{
	public:
		// Constructor
		CBannerModbusTCP(void);

		// Destructor
		~CBannerModbusTCP(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BYTE	 m_bUnit;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTx[300];
		BYTE	   m_bRx[300];
		UINT	   m_uPtr;
		UINT	   m_uKeep;

		// Socket Management
		BOOL	CheckSocket(void);
		BOOL	OpenSocket(void);
		void	CloseSocket(BOOL fAbort);
		
		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
				
		// Transport Layer
		BOOL	SendFrame(void);
		BOOL	RecvFrame(void);
		BOOL	Transact(BOOL fIgnore);
		BOOL	CheckFrame(void);

		// Read Handlers
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
		BOOL	GetModbusConfig(AREF Addr, CAddress * A);

	};

// End of File
