	
#include "intern.hpp"

#include "DualisCameraDriver.hpp"

// Instantiator

INSTANTIATE(CDualisDriver);

// Timeouts

static UINT const timeWaitTimeout  = 5000;

static UINT const timeWaitDelay    = 10;

static UINT const timeSendTimeout  = 5000;

static UINT const timeSendDelay	   = 10;

static UINT const timeRecvTimeout  = 20000;

static UINT const timeRecvDelay	   = 100;

static UINT const timeBuffDelay    = 100;

// Constructor

CDualisDriver::CDualisDriver(void)
{
	m_Ident = DRIVER_ID;

	m_pCtx  = NULL;

	m_uKeep = 0;

	m_pHead = NULL;

	m_pTail = NULL;

	m_pName = "cmra";
	}

// Destructor

CDualisDriver::~CDualisDriver(void)
{
	m_pMutex->Release();
	}
	
// Management

void MCALL CDualisDriver::Attach(IPortObject *pPort)
{
	CreateObjects();
	}

// Device

CCODE MCALL CDualisDriver::DeviceOpen(IDevice *pDevice)
{
	CCameraDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_uDevice = GetWord(pData);
			m_pCtx->m_IP	  = GetAddr(pData);
			m_pCtx->m_uPort	  = GetWord(pData);
			m_pCtx->m_fKeep   = FALSE;
			m_pCtx->m_fPing   = TRUE;
			m_pCtx->m_uTime1  = 2000;
			m_pCtx->m_uTime2  = 1000;
			m_pCtx->m_uTime3  =  500;
			m_pCtx->m_uTime4  = GetWord(pData);
			m_pCtx->m_uLast3  = GetTickCount();
			m_pCtx->m_uLast4  = GetTickCount();
			m_pCtx->m_pSock	  = NULL;
			m_pCtx->m_pData   = NULL;

			m_pCtx->m_uGroup    = 0;
			m_pCtx->m_uTicket   = 0;

			m_pCtx->m_uMethod   = GetByte(pData);

			m_pCtx->m_pStart    = GetString(pData);
			m_pCtx->m_pStop     = GetString(pData);
			m_pCtx->m_pSep      = GetString(pData);
			m_pCtx->m_uProtocol = GetByte(pData);
			m_pCtx->m_pFormat   = "BMP";			
			m_pCtx->m_uFrame    = 0;
			m_pCtx->m_dwError   = 0; 
			m_pCtx->m_uTrigger  = 0;

			memset(m_pCtx->m_sSend,   0, sizeof(m_pCtx->m_sSend));
			memset(m_pCtx->m_sRecv,   0, sizeof(m_pCtx->m_sRecv));

			memset(m_pCtx->m_uInfo,   0, sizeof(m_pCtx->m_uInfo));

			memset(&m_pCtx->m_Result, 0, sizeof(m_pCtx->m_Result));

			memset(&m_pCtx->m_Stats,  0, sizeof(m_pCtx->m_Stats));

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CDualisDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		CloseSocket(FALSE);

		delete m_pCtx->m_pData;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CCameraDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CDualisDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) {

		if( pCtx->m_uTrigger != 2 ) {

			pCtx->m_uTrigger = 2;

			return 1;
			}
		
		return 0;
		}

	if( uFunc == 2 ) {

		return pCtx->m_uTrigger;
		}

	if( uFunc == 3 ) {

		if( pCtx->m_Result.m_fValid ) {

			if( !strcmp("result", Value) ) {
			
				return pCtx->m_Result.m_fResult;
				}

			if( !strcmp("match", Value) ) {
			
				return pCtx->m_Result.m_uMatch;
				}
			}

		return 0;
		}

	if( uFunc == 4 ) {
		
		return pCtx->m_dwError;
		}

	if( uFunc == 5 ) {

		CStatistic &Stats = pCtx->m_Stats;

		UINT uIndex;

		if( !strcmp("total", Value) ) {
			
			uIndex = 0;
			}

		else if( !strcmp("good", Value) ) {
			
			uIndex = 1;
			}

		else if( !strcmp("bad", Value) ) {
			
			uIndex = 2;
			}
		else
			uIndex = elements(Stats.m_Data);

		return  uIndex < elements(Stats.m_Data) ? Stats.m_Data[uIndex] : 0;
		}

	if( uFunc == 6 ) {

		CStatistic &Stats = pCtx->m_Stats;

		if( !strcmp("enable", Value) ) {
			
			Stats.m_fRequest = TRUE;
			}

		if( !strcmp("disable", Value) ) {
			
			Stats.m_fRequest = FALSE;
			}

		return Stats.m_fRequest;  
		}
	
	return 0;
	}

// Entry Points

void MCALL CDualisDriver::Service(void)
{
	if( m_pCtx->m_uTrigger == 2 ) {

		if( m_pCtx->m_uMethod ) {

			if( OpenSocket() ) {

				CCmdDef Cmd[] = {
			
					{ "t", FALSE },
					{ "p", FALSE },
				
					};

				char sCmd[128] = { 0 };

				SPrintf(sCmd, "%s",	Cmd[0].pCode);
				
				PBYTE pData = NULL;

				if( Transact(Cmd[0].pCode, Cmd[0].fRqst, pData) ) {

					CCODE Code = Parse(Cmd[0].pCode, NULL);

					if( !COMMS_SUCCESS(Code) ) {

						m_pCtx->m_uTrigger = 0;

						return;					
						}
					
					m_pCtx->m_uTrigger = 1;

					return;
					}			
				
				CloseSocket(FALSE);

				m_pCtx->m_uTrigger = 0;
				}
			}
		}
	}

CCODE MCALL CDualisDriver::ReadImage(PBYTE &pData)
{
	if( OpenSocket() ) {

		if( m_pCtx->m_uMethod == 0 ) {

			if( RecvResult(pData, m_pCtx->m_uProtocol) ) {

				CCODE Code = Parse("R", NULL);

				Sleep(timeRecvDelay);

				return Code;
				}

			return CCODE_ERROR | CCODE_NO_DATA;
			}

		PCTXT pCmnd[] = { "I", "F", "R", "T" };		

		PCTXT   pCode = pCmnd[m_pCtx->m_uMethod - 1];

		if( Transact(pCode, TRUE, pData) ) {
			
			m_pCtx->m_uLast4 = GetTickCount();

			CCODE Code = Parse(pCode, NULL);

			if( !COMMS_SUCCESS(Code) ) {

				Sleep(timeRecvDelay);

				GetError();
				}
			else {
				Sleep(timeRecvDelay);

				m_pCtx->m_dwError    = 0;
				
				GetStats();
				}

			return Code;
			}

		if( Ping() == CCODE_SUCCESS ) {

			UINT dt = GetTickCount() - m_pCtx->m_uLast4;

			UINT tt = 1000 * ToTicks(m_pCtx->m_uTime4);

			if( !tt || dt < tt ) {

				pData = NULL;

				return CCODE_SUCCESS;
				}
			}

		CloseSocket(FALSE);
		}

	return CCODE_ERROR;
	}

void MCALL CDualisDriver::SwapImage(PBYTE pData)
{
	delete m_pCtx->m_pData;

	m_pCtx->m_uInfo[0] = GetTimeStamp();

	m_pCtx->m_uInfo[1] = ((BITMAP_RLC *) pData)->Frame;

	m_pCtx->m_pData    = pData;
	}

void MCALL CDualisDriver::KillImage(void)
{
	if( m_pCtx->m_pData ) {
		
		memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

		delete m_pCtx->m_pData;

		m_pCtx->m_pData = NULL;
		}
	}

PCBYTE MCALL CDualisDriver::GetData(UINT uDevice)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			return pCtx->m_pData;
			}
		}

	return NULL;
	}

UINT MCALL CDualisDriver::GetInfo(UINT uDevice, UINT uParam)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			if( uParam < elements(pCtx->m_uInfo) ) {

				return pCtx->m_uInfo[uParam];
				}
			}
		}

	return 0;
	}

BOOL MCALL CDualisDriver::SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	return CCameraDriver::SaveSetup(pContext, uIndex, pFile);
	}

BOOL MCALL CDualisDriver::LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	return CCameraDriver::LoadSetup(pContext, uIndex, pFile);
	}

BOOL MCALL CDualisDriver::UseSetup(PVOID pContext, UINT uIndex)
{
	return CCameraDriver::UseSetup(pContext, uIndex);
	}

// Initialization

void CDualisDriver::CreateObjects(void)
{
	ISyncHelper *pSync = NULL;

	m_pHelper->MoreHelp(IDH_SYNC, (void **) &pSync);

	m_pMutex = pSync->CreateMutex();

	pSync->Release();
	}

BOOL CDualisDriver::GetError(void)
{
	PBYTE pData = NULL;

	if( Transact("E", TRUE, pData) ) {
	
		Parse("E", &m_pCtx->m_dwError);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDualisDriver::GetStats(void)
{
	if( m_pCtx->m_Stats.m_fRequest ) {

		PBYTE pData = NULL;

		if( Transact("s", TRUE, pData) ) {

			CCODE Code = Parse("s", m_pCtx->m_Stats.m_Data, 0, 2);

			if( !COMMS_SUCCESS(Code) ) {
					
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CDualisDriver::Transact(PCTXT pCode, BOOL fRqst, PBYTE &pData)
{
	m_pMutex->Wait(FOREVER);

	if( Send(pCode, fRqst) ) {
			
		if( !strcmp(pCode, "R") || 
		    !strcmp(pCode, "T") ){

			BOOL fResult = RecvResult(pData, m_pCtx->m_uProtocol);

			m_pMutex->Free();			
				
			return fResult;
			}

		if( !strcmp(pCode, "I") || 
		    !strcmp(pCode, "F") ){

			BOOL fResult = RecvImage(pData, m_pCtx->m_uProtocol);

			m_pMutex->Free();
				
			return fResult;
			}

		if( !strcmp(pCode, "t") || 
		    !strcmp(pCode, "p") ||
		    !strcmp(pCode, "s") ||
		    !strcmp(pCode, "E") ){

			BOOL fResult = RecvFrame(m_pCtx->m_uProtocol);

			m_pMutex->Free();
				
			return fResult;
			}		
		}

	m_pMutex->Free();

	return FALSE;
	}

// Shared Code

#include "..\IfmDualisShared\DualisDriver.cpp"

// End of File
