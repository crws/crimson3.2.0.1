
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDualisDriver;

//////////////////////////////////////////////////////////////////////////
//
// Bitmap Header
//

#pragma pack(2)

struct CBMPHeader {

	char	ID[2];
	DWORD	Size;
	WORD	Pad1;
	WORD	Pad2;
	DWORD   Offset;
	};

#pragma pack()

#pragma pack(2)

struct CDIBHeader {

	DWORD HeadSize;
	DWORD Width;
	DWORD Height;
	WORD  Planes;
	WORD  Bits;
	DWORD Compression;
	DWORD DataSize;
	DWORD HorzRes;
	DWORD VertRes;
	DWORD Colors;
	DWORD Important;
	};

#pragma pack()

/////////////////////////////////////////////////////////////////////////
//
// Dualis Vision Sensor Camera Driver
//

class CDualisDriver : public CCameraDriver
{
	public:
		// Constructor
		CDualisDriver(void);

		// Destructor
		~CDualisDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
				
		// Entry Points
		DEFMETH(CCODE ) Ping     (void);
		DEFMETH(void  ) Service  (void);
		DEFMETH(CCODE ) ReadImage(PBYTE &pData);
		DEFMETH(void  ) SwapImage(PBYTE pData);
		DEFMETH(void  ) KillImage(void);
		
		DEFMETH(PCBYTE) GetData  (UINT uDevice);
		DEFMETH(UINT  ) GetInfo  (UINT uDevice, UINT uParam);
		
		DEFMETH(BOOL  ) SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile);
		DEFMETH(BOOL  ) LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile);
		DEFMETH(BOOL  ) UseSetup (PVOID pContext, UINT uIndex);

	protected:
		// Message States
		enum {
			stateStart,
			stateResult,
			stateDefault,
			stateOption,
			stateImage,
			stateLength,
			stateBitmap,
			stateModel,
			stateCRLF,
			};

		// Image formats
		enum {
			formatBMP = 1,
			formatRAW = 2,
			};

		// Command Definition
		struct CCmdDef {

			PCTXT pCode;
			BOOL  fRqst;
			};

		// Result
		struct CResult {

			BOOL	m_fResult;
			UINT	m_uMatch;
			BOOL	m_fValid;
			};

		// Statistic Data
		struct CStatistic {

			BOOL	   m_fRequest;
			DWORD	   m_Data[3];
			};

		// Device Data
		struct CContext
		{
			CContext * m_pNext;
			CContext * m_pPrev;
			UINT	   m_uDevice;
			DWORD	   m_IP;
			UINT	   m_uPort;
			BOOL	   m_fKeep;
			BOOL	   m_fPing;
			UINT	   m_uTime1;
			UINT	   m_uTime2;
			UINT	   m_uTime3;
			UINT	   m_uTime4;
			UINT	   m_uLast3;
			UINT	   m_uLast4;
			ISocket  * m_pSock;
			char	   m_sSend[128];
			char	   m_sRecv[128];
			PBYTE	   m_pData;
			UINT	   m_uInfo[4];
			UINT	   m_uPtr;
			UINT       m_uGroup;
			UINT	   m_uProtocol;
			UINT	   m_uTicket;
			UINT	   m_uFrame;
			CResult	   m_Result;
			CStatistic m_Stats;
			DWORD	   m_dwError;
			UINT	   m_uTrigger;
								
			// Protocol Config
			BOOL	   m_uMethod;
			PCTXT      m_pStart;
			PCTXT      m_pStop;
			PCTXT      m_pSep;
			PCTXT      m_pFormat;
			};

		// Data Members
		CContext * m_pCtx;
		CContext * m_pHead;
		CContext * m_pTail;
		UINT	   m_uKeep;
		PCTXT	   m_pName;
		IMutex   * m_pMutex;		

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Initialization
		void CreateObjects(void);

		//
		BOOL GetError(void);
		BOOL GetStats(void);
		BOOL Transact(PCTXT pCode, BOOL fRqst, PBYTE &pData);

		// Implementation
		CCODE Parse(PCTXT pCmd, PDWORD pData);
		CCODE Parse(PCTXT pCmd, PDWORD pData, UINT uIndex, UINT uCount);
		UINT  ParseFixed(PCTXT p);
		UINT  FindFactor(PCTXT p);

		void PunchTicket(void);
		
		BOOL Send(PCTXT pCmd, BOOL fRqst);
		
		BOOL SendFrame(void);

		BOOL RecvFrame(UINT uProtocol);
		BOOL RecvFrame (void);

		BOOL RecvResult(PBYTE &pImage, UINT uProtocol);
		BOOL RecvResult(PBYTE &pImage);
		
		BOOL RecvImage(PBYTE &pImage, UINT uProtocol);
		BOOL RecvImage(PBYTE &pImage);

		BOOL ReadHeader(CBMPHeader &Header);
		BOOL ReadHeader(CDIBHeader &Header);
		BOOL ReadBitmap(PBYTE &pImage);

		UINT ReadTicket(void);
		BOOL ReadCRLF(void);

		BOOL CheckCRLF(void);

		void HostAppend(PCTXT pText);
		void HostPrintf(PCTXT pText, ...);

		BOOL Read(PBYTE pData, UINT uCount);

		void Trace(PCTXT pText, ...);
	};

// End of File
