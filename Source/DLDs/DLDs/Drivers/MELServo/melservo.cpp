#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "melservo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi MELServo Driver
//

// Command Definitions

//struct FAR MELSERVOCmdDef {
//	UINT	uID;
//	UINT	uRCmd;
//	UINT	uWCmd;
//	UINT	uDataNumber;
//	UINT	uReadWrite;
//	UINT	uReplySize;
//	UINT	uReadDataSize;
//	};
	
MELSERVOCmdDef CODE_SEG CMELServoDriver::CL[] = {

	{SPPARLP,	CPARR,	CPARW,	DVARI,	8,	6},
	{SPPARA,	CPARR,	CPARW,	DVARI,	8,	6},
	{SPPARLPH,	CPARR,	CPARW,	DVARI,	8,	6},
	{SPPARAH,	CPARR,	CPARW,	DVARI,	8,	6},
	{SPPAR3,	CPARR,	CPARW,	DVARI,	8,	6},
	{SPPAR3H,	CPARR,	CPARW,	DVARI,	8,	6},
	{SPPARU,	CPARU,	CPARX,	DVARI,	8,	6},
	{SPPARUH,	CPARU,	CPARX,	DVARI,	8,	6},
	{SPPARL,	CPARL,	CPARX,	DVARI,	8,	6},
	{SPPARLH,	CPARL,	CPARX,	DVARI,	8,	6},
	{SPPARAB,	CPARA,	CPARX,	DVARI,	8,	6},
	{SPPAREN,	CPARE,	CPARX,	DVARI,	8,	6},
	{SPSTSL,	CSTSR,	CSTSW,	DVARI,	12,	8},
	{SPSTSA,	CSTSR,	CSTSW,	DVARI,	12,	8},
	{SPSTSP,	CSTSR,	CSTSW,	DVARI,	12,	8},
	{SPSTS3U,	CSTSR,	CSVRW,	DVARI,	16,	5},
	{SPSTS3N,	CSTSR,	CSVRW,	DVARI,	16,	9},
	{SPGRP,		CGPSR,	CGPSW,	DGRP,	4,	4},
	{SPALN,		CALNR,	CALNW,	DVARI,	4,	2},
	{SPALT,		CALTR,	CALTW,	DVARI,	8,	8},
	{SPALSL,	CALSR,	CALSW,	DVARI,	12,	8},
	{SPALSA,	CALSR,	CALSW,	DVARI,	12,	8},
	{SPALSP,	CALSR,	CALSW,	DVARI,	12,	8},
	{SPALSU,	CALSR,	CALSW,	DVARI,	16,	5},
	{SPALSN,	CALSR,	CALSW,	DVARI,	16,	9},
	{SPACU,		CACUR,	CACUW,	DACU,	4,	2},
	{SPEXT0,	CEXTR,	CEX0W,	DVARI,	8,	8},
	{SPEXTA,	CEXTR,	CEX1W,	DVARI,	8,	8},
	{SPLAT,		CLATR,	CLATW,	DLAT,	12,	8},
	{SPGPRR,	CGPRR,	CGPRW,	DVARI,	8,	6},
	{SPGPDR,	CGPDR,	CGPDW,	DVARI,	8,	6},
	{SPGSV,		CGSVR,	CGSVW,	DGSV,	4,	2},
	{SPSEP,		CSEPR,	CSEPW,	DSEPR,	8,	8},
	{SPCUP,		CCUPR,	CCUPW,	DCUPR,	8,	8},
	{SPSVR,		CSVRR,	CSVRW,	DSVRR,	16,	16},
	{SPACL,		CACLR,	CACLW,	DACLW,	6,	0},
	{SPSCL,		CSCLR,	CSCLW,	DSCLW,	6,	0},
	{SPOMS,		COMSR,	COMSW,	DOMSW,	6,	0},
	{SPTOM,		CTOMR,	CTOMW,	DVARI,	6,	0},
	{SPTOM3M,	CTOMR3,NOXMIT,	DTOM2,	4,	4},
	{SPTOM3S,	CTOMR3,NOXMIT,	DTOM5,	8,	8},
	{SPEIS,		CEISR,	CEISW,	DVARI,	6,	0},
	{SPEOS,		CEOSR,	CEOSW,	DVARI,	6,	0},
	{SPWPEC,	CPARR,	CWPEW,	DVARI,	8,	6},
	{SPWPEA,	CPARR,	CWPEW,	DVARI,	8,	6},
	{SPWGER,	CGPRR,	CWGRW,	DVARI,	8,	6},
	{SPWGED,	CGPDR,	CWGDW,	DVARI,	8,	6},
	{SPCACHE,	    0, NOXMIT,	DVARI,	6,	0},
	{SPPOS,		CPOSR,  CPOSW,	DVARI,	8,	6},
	{SPSPD,		CSPDR,  CSPDW,	DVARI,	8,	6},
	{SPACC,		CACCR,  CACCW,	DVARI,	8,	6},
	{SPDEC,		CDECR,  CDECW,	DVARI,	8,	6},
	{SPDWL,		CDWLR,  CDWLW,	DVARI,	8,	6},
	{SPAUX,		CAUXR,  CAUXW,	DVARI,	8,	6},
	{SPWEEP,	CEEPR,  CEEPW,	DVARI,  0,      0},
	{SPGENR,	CGENR,  CGENW,	DVARI,	4,	4},
	{SPGENW,	    0,  CGENW,	DVARI,	4,	4},
	};

// Instantiator

INSTANTIATE(CMELServoDriver);

// Constructor

CMELServoDriver::CMELServoDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_bGroup	= 0;

	m_uWriteErr	= 0;

	m_dWEEPResult	= 0;

	CTEXT TOM3_6[]	= "STOPGO  CLR ";

	m_pMItem	= NULL;

	m_pTom		= TOM3_6;

	m_uJ3Group	= 0;
	}

// Destructor

CMELServoDriver::~CMELServoDriver(void)
{
	}

// Configuration

void MCALL CMELServoDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CMELServoDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}
	
// Management

void MCALL CMELServoDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMELServoDriver::Open(void)
{
	m_pMCL = (MELSERVOCmdDef FAR *)CL;
	}

// Device

CCODE MCALL CMELServoDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_uStation   = GetWord(pData);
			m_pCtx->m_uGroup     = GetWord(pData);
			m_pCtx->m_uServoType = GetWord(pData);

			ClearGenWrite();

			SetStationNumber();

			if( m_pCtx->m_dCache[CMAGIC] != 0x12345678 ) {

				memset(m_pCtx->m_dCache, 0xFFFFFFFF, sizeof(m_pCtx->m_dCache) );

				m_pCtx->m_dCache[CMAGIC] = 0x12345678;
				}

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMELServoDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		delete m_pMCL;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMELServoDriver::Ping(void)
{
	SendCommsReset();

	if( IsJ3Device() ) {

		m_uJ3Group = 0;

		m_pMItem   = m_pMCL;

		return ChangeGroup() ? 1 : CCODE_ERROR;
		}

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPALN;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CMELServoDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nRead T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( Addr.a.m_Table == SPCACHE ) {

		for( UINT i = 0; i < uCount; i++ ) {

			pData[i] = m_pCtx->m_dCache[Addr.a.m_Offset + i];
			}

		return uCount;
		}

	if( m_pCtx->m_uGroup == ISALLUNITS ) {

		if( !m_bGroup ) return uCount;
		}

	if( m_pCtx->m_uGroup == ISGROUP ) { // Check if group response enabled

		if( !GroupResponseSet() ) return uCount;
		}

	if( NoReadTransmit(Addr, pData, uCount) ) return uCount;

	m_pMItem = SetMELSCommand(Addr.a.m_Table);

	if( m_pMItem ) {

		if( m_pMItem->uRCmd == NOXMIT ) { // in case this is a read before a write

			switch( m_pMItem->uID ) {

				case SPOMS:
				case SPTOM:
				case SPEIS:
				case SPEOS:
					*pData = 0xFFFFFFFF;
					return 1;
				}

			*pData = 0;

			return 1;
			}

		if( IsJ3NewGroup(Addr.a.m_Table, Addr.a.m_Extra) ) {

			if( !ChangeGroup() ) return CCODE_ERROR | CCODE_NO_RETRY;
			}

		if( Addr.a.m_Table == SPPARAB ) uCount = 1;

		return DoRead(Addr, pData, uCount);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CMELServoDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n***** WRITE T=%d O=%d C=%d *****\r\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	m_pMItem = SetMELSCommand(Addr.a.m_Table);

	if( m_pMItem ) {

		if( NoWriteTransmit(Addr, *pData) ) return 1;

		if( m_pCtx->m_uGroup == ISSTATION ||
		   (m_pCtx->m_uGroup == ISGROUP && GroupResponseSet()) ) {

			if( m_pCtx->m_uGroup == ISSTATION ) {

				if( IsJ3NewGroup(Addr.a.m_Table, Addr.a.m_Extra) ) {

					if( !ChangeGroup() ) return CCODE_ERROR | CCODE_NO_RETRY;
					}
				}

			DWORD dRead[1];

			if( m_pMItem->uID != SPGENW ) Read(Addr, dRead, uCount);
			}

		if( !IsStringItem(Addr.a.m_Table) ) uCount = 1;

		if( DoWrite(Addr, pData, uCount) != 1 ) {

			if( ++m_uWriteErr < 2 ) return CCODE_ERROR;

			m_uWriteErr = 0;
			}

		return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

WORD CMELServoDriver::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) return bData - '0';

	if( bData >= 'A' && bData <= 'F' ) return bData - '7';

	if( bData >= 'a' && bData <= 'f' ) return bData - 'W';
		
	return 0;
	}

// Port Access

UINT CMELServoDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CMELServoDriver::StartFrame(BYTE bCommand, BYTE bDataNumber)
{
	m_bTx[0] = SOH;

	m_uPtr = 1;

	AddByte(m_pCtx->m_bStation);

	AddHex(bCommand);

	AddByte(STX);

	AddHex(bDataNumber);
	}

void CMELServoDriver::EndFrame(void)
{
	AddByte(ETX);

	BYTE bChecksum = 0;

	for( UINT i = 1; i < m_uPtr; i++ ) {

		bChecksum += m_bTx[i];
		}

	AddHex(bChecksum);
	}

void CMELServoDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) m_bTx[m_uPtr++] = bData;
	}

void CMELServoDriver::AddWord(WORD wData)
{
	AddHex(HIBYTE(wData));

	AddHex(LOBYTE(wData));
	}

void CMELServoDriver::AddLong(DWORD dData)
{
	AddWord(HIWORD(dData));

	AddWord(LOWORD(dData));
	}

void CMELServoDriver::AddHex(BYTE bData)
{
	AddByte(m_pHex[bData/16]);
	AddByte(m_pHex[bData%16]);
	}

void CMELServoDriver::AddWordAddHex(WORD w, BYTE h)
{
	AddWord(w);
	AddHex(h);
	}
		
// Transport Layer

void CMELServoDriver::PutFrame(void)
{
	EndFrame();

	m_pData->ClearRx();

	memset(m_bRx, 0, 3);

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

BOOL CMELServoDriver::GetFrame(BOOL fIsRead)
{
	UINT uState = IsGeneric(m_pMItem->uID) ? 3 : 0;

	UINT uPtr   = 0;

	UINT uTimer = 500;

	UINT uData;

	BYTE bData;

//**/	AfxTrace0("\r\n");

	SetTimer(uTimer);

	while( (uTimer = GetTimer()) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);

		switch( uState ) {

			case 0:
				if( bData == STX ) uState = 1;
				break;

			case 1:
				uState = bData == m_pCtx->m_bStation ? 2 : 0;
				break;

			case 2:
				uState = (bData == 'A' || bData == 'a') ? 3 : 10;
				uPtr   = 0;
				break;

			case 3:
				if( bData == ETX ) {

					uState = 4;

					if( !IsGeneric(m_pMItem->uID) ) bData = 0;
					}

				m_bRx[uPtr++] = bData;
				break;

			case 4:
				memset(&m_bRx[uPtr], 0, sizeof(m_bRx) - uPtr);
				uState = 5;
				break;

			case 5:
				return TRUE;

			case 10:
				if( bData == ETX ) uState = 11;
				break;

			case 11:
				uState = 12;
				break;

			case 12:
				return FALSE;
			}

		if( uPtr >= sizeof(m_bRx) ) return FALSE;
		}

	if( !m_bRx[0] ) SendCommsReset();

	return FALSE;
	}

BOOL CMELServoDriver::Transact(BOOL fIsRead)
{
	PutFrame();

	BOOL fWantResponse = FALSE;

	switch( m_pCtx->m_uGroup ) {

		case ISSTATION:
			fWantResponse = TRUE;
			break;

		case ISGROUP:
			fWantResponse = GroupResponseSet();
			break;

		case ISALLUNITS:
			fWantResponse = BOOL(m_bGroup);
			break;

		default:
			break;
		}

	if( !fWantResponse ) {

		while( m_pData->Read(0) < NOTHING );

		return TRUE;
		}
			
	return GetFrame(fIsRead);
	}
// Read Handlers

CCODE CMELServoDriver::DoRead(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bCommand    = m_pMItem->uRCmd;

	BYTE bDataNumber = m_pMItem->uDataNumber;

	UINT uTable      = Addr.a.m_Table;
	UINT uOffset     = Addr.a.m_Offset;

	if( !IsStringItem(uTable) ) uCount = 1;

	switch( uTable ) {

		case SPPARLP:
		case SPPARA:
		case SPPARLPH:
		case SPPARAH:
		case SPPAR3:
		case SPPAR3H:
		case SPPARU:
		case SPPARUH:
		case SPPARL:
		case SPPARLH:
		case SPPARAB:
		case SPPAREN:
		case SPGPRR:
		case SPGPDR:
		case SPWPEC:
		case SPWPEA:
		case SPWGER:
		case SPWGED:
			bDataNumber = uOffset;
			break;

		case SPSTS3U:
		case SPSTS3N:
		case SPALSU:
		case SPALSN:
			bDataNumber = Addr.a.m_Extra;
			break;

		case SPSTSL:
		case SPSTSA:
		case SPSTSP:
			bDataNumber = uOffset + OFFSTS;
			break;

		case SPALN:
			bDataNumber = uOffset + OFFALN;
			break;

		case SPALT:
			bDataNumber = uOffset + OFFALT;
			break;

		case SPALSL:
		case SPALSA:
		case SPALSP:
			bDataNumber = uOffset + OFFALS;
			break;

		case SPEXT0:
			switch( uOffset ) {

				case 0: bDataNumber = DEXT0;	break;
				case 1: bDataNumber = DEXT1;	break;
				case 2: bDataNumber = DEXT2;	break;
				case 3: bDataNumber = DEXT3;	break;
				case 4: bDataNumber = DEXT4;	break;
				}
			break;

		case SPEXTA:
			bDataNumber = uOffset ? DEXT4 : DEXT1;
			break;

		case SPPOS:
		case SPSPD:
		case SPACC:
		case SPDEC:
		case SPDWL:
		case SPAUX:
			bDataNumber = uOffset & 0x1F;
			break;

		case SPGENR:
			bCommand    = HIBYTE(uOffset);
			bDataNumber = LOBYTE(uOffset);
			break;

		default:
			break;
		}

	StartFrame(bCommand, bDataNumber);

	if( Transact(TRUE) ) {

		GetReadResponse(pData, uCount);

		return uCount;
		}

	return CCODE_ERROR;
	}

void  CMELServoDriver::GetReadResponse( PDWORD pData, UINT uCount )
{
	UINT uScan;

	DWORD dData = 0;

	UINT uTable = m_pMItem->uID;

	UINT uPos   = m_pMItem->uReplySize - m_pMItem->uReadDataSize;

	PU4  p      = PU4(m_bRx);

	switch( uTable ) {

		case SPSVR:
		case SPGENR:

			for( uScan = 0; uScan < uCount; uScan++, p++, pData++ ) *pData = *p;

			return;

		case SPSTS3U:
		case SPALSU:

			for( uScan = 0; uScan < uCount; uScan++ ) {

				switch( uScan ) {

					case 0:
						pData[0] = *((PU4) &m_bRx[2]);
						break;

					case 1:
						pData[1] = m_bRx[6] << 24;
						break;

					default:
						pData[uScan] = 0;
						break;
					}
				}

			return;
			
		case SPSTS3N:
		case SPALSN:

			for( uScan = 0; uScan < uCount; uScan++ ) {

				switch( uScan ) {

					case 0:
					case 1:
						pData[uScan] = *((PU4)&m_bRx[7 + (uScan * 4)]);
						break;

					case 2:
						pData[2] = m_bRx[15] << 24;
						break;

					default:
						pData[uScan] = 0;
						break;
					}
				}
			return;

		case SPPARAB:

			pData[0] = *((PU4) &m_bRx[2]);

			for( uScan = 1; uScan < uCount; uScan++ ) {

				pData[uScan] = 0;
				}

			return;
		}

	BOOL fMakeReal  = TRUE;
	BOOL fKeepAsHex = FALSE;

	m_uDecimal      = 0;

	UINT uT         = m_pMItem->uID;

	switch( uT ) {

		case SPPARLP:
		case SPPARA:
		case SPPAR3:
		case SPPARAB:
		case SPPAREN:
		case SPWPEC:
		case SPWPEA:
		case SPPOS:
			m_uDecimal = m_bRx[1] & 0x7;
			fKeepAsHex = m_bRx[0] & 1;
			break;

		case SPPARU:
		case SPPARL:
		case SPPAR3H:
			m_uDecimal = m_bRx[1] & 0x7;
			fKeepAsHex = TRUE;
			fMakeReal  = uT != SPPAR3H;
			break;

		case SPPARLPH:
		case SPPARAH:
		case SPPARUH:
		case SPPARLH:
		case SPGRP:
		case SPSEP:
		case SPCUP:
		case SPEXT0:
		case SPEXTA:
		case SPALT:
		case SPTOM3M:
		case SPTOM3S:
			fKeepAsHex = TRUE;
			fMakeReal  = FALSE;
			break;

		case SPGPRR:
		case SPGPDR:
		case SPWGER:
		case SPWGED:
			fKeepAsHex = TRUE;
			break;

		case SPSPD:
		case SPACC:
		case SPDEC:
		case SPDWL:
		case SPAUX:
			fKeepAsHex = m_bRx[0] & 1;
			fMakeReal  = FALSE;
			break;

		case SPSTSL:
		case SPSTSA:
		case SPSTSP:
		case SPALSL:
		case SPALSA:
		case SPALSP:
		case SPLAT:
			m_uDecimal = m_bRx[2] & 0x7;
			fKeepAsHex = !(m_bRx[3] & 1);
			break;

		case SPALN:
		case SPACU:
			fKeepAsHex = IsJ3Device();
			fMakeReal  = FALSE;
			break;

		default:
			fMakeReal  = FALSE;
			break;
		}

	for( UINT i = uPos; i < uPos + m_pMItem->uReadDataSize; i++ ) {

		dData  = fKeepAsHex ? dData << 4 : dData * 10;

		dData += FromHex(m_bRx[i]);
		}

	if( m_uDecimal && fKeepAsHex && !fMakeReal) { // adjust hex value using decimal place

		while( m_uDecimal > 1 ) { // 0 & 1 are no decimal places

			dData /= 10;

			m_uDecimal--;
			}
		}

	if( uT == SPGRP ) m_uJ3Group = dData;

	*pData = fMakeReal ? MakeRealValue(dData) : dData;
	}

DWORD CMELServoDriver::MakeRealValue(DWORD dData)
{
	char ss[13] = {0};

	if( !dData || dData == 0x800000 ) return 0;

	BOOL fNeg = FALSE;

	if( dData & 0x800000 ) {

		dData |= 0xFF000000;
		dData = -dData;
		fNeg  = TRUE;
		}

	if( m_uDecimal <= 1 ) {

		SPrintf( ss,
			"%d",
			dData
			);
		}

	else {
		UINT uMult  = 1;
		DWORD dInt;
		DWORD dFrac;

		for( UINT i = 1; i < m_uDecimal; i++ ) {

			uMult *= 10;
			}

		dInt  = dData / uMult;
		dFrac = dData % uMult;

		char sFormat[12] = {0};

		SPrintf( sFormat,
			"%s%d.%dd",
			"%d.%",
			m_uDecimal - 1,
			m_uDecimal - 1
			);

		SPrintf( ss,
			sFormat,
			dInt,
			dFrac
			);
		}

	dData = ATOF( ss );

	return fNeg ? dData | 0x80000000 : dData;
	}

// Write Handlers

CCODE CMELServoDriver::DoWrite( AREF Addr, PDWORD pData, UINT uCount )
{
	UINT uT          = Addr.a.m_Table;
	UINT uO          = Addr.a.m_Offset;

	BYTE bCommand    = m_pMItem->uWCmd;
	BYTE bDataNumber = m_pMItem->uDataNumber;

	DWORD dData      = *pData;

	switch( uT ) {

		case SPPARLP:
		case SPPARA:
		case SPPARLPH:
		case SPPARAH:
		case SPWPEC:
		case SPWPEA:
		case SPPAR3:
		case SPPAR3H:
			StartFrame(CPARW, uO);

			AddByte(uT != SPWPEC && uT != SPWPEA ? '3' : '0');

			if( m_uDecimal != 0xFF ) {

				AddByte(m_uDecimal + '0');

				switch( uT ) {

					case SPPARLPH:
					case SPPARAH:
					case SPPAR3H:
						AddWordAddHex(dData >> 8, LOBYTE(dData));
						break;

					default:
						MakeHexFromFloat(dData);
						break;
					}
				}

			else AddWordAddHex(dData >> 8, LOBYTE(dData));
			break;

		case SPGRP:
			StartFrame(CGPSW, 0);
			AddWord(LOWORD(dData));
			break;

		case SPPOS:
			StartFrame(CPOSW, uO & 0x1F);

			AddByte('1');

			if( m_uDecimal != 0xFF ) {

				AddByte(m_uDecimal + '0');
				MakeHexFromFloat(dData);
				}

			else AddWordAddHex(dData >> 8, LOBYTE(dData));

			break;

		case SPSPD:
		case SPACC:
		case SPDEC:
		case SPDWL:
		case SPAUX:
			StartFrame(bCommand, LOBYTE(uO));

			AddByte('1');
			AddByte('0');

			AddWordAddHex(dData >> 8, LOBYTE(dData));

			break;

		case SPEXT0:
			StartFrame(CEX0W, DEXTW);

			AddLong(dData);

			break;

		case SPTOM:
			DoTOM(uO, &bCommand, &bDataNumber, dData);
			break;

		case SPACU:
		case SPACL:
		case SPSCL:
			WriteCommand(bCommand, bDataNumber);
			break;

		case SPEIS:
			bDataNumber = !dData ? DEIS0 : DEIS1;

			WriteCommand(bCommand, bDataNumber);
			break;

		case SPEOS:
			bDataNumber = !dData ? DEOS0 : DEOS1;

			WriteCommand(bCommand, bDataNumber);
			break;

		case SPGSV:
			SetGroupResponse(LOBYTE(dData));

			StartFrame(CGSVW, DGSV);

			AddHex(LOBYTE(dData) & 4 ? 1 : 0);
			AddHex(LOBYTE(dData) & 0xF);
			break;

		case SPOMS:
			StartFrame(bCommand, bDataNumber);
			AddWord(LOWORD(dData));
			break;

		case SPGPRR:
		case SPGPDR:
		case SPWGER:
			StartFrame(bCommand, uO); 

			AddByte(uT == SPWGER ? KWROM : KWRAM);

			AddByte(m_uDecimal + '0');

			MakeHexFromFloat(dData);

			break;

		case SPWEEP:
			m_dWEEPResult = WritePointToEEPROM(LOBYTE(dData));
			return 1;

		case SPGENW:
			if( !GetWGenCodes(dData, &bCommand, &bDataNumber) ) return uCount;

			StartFrame(bCommand, bDataNumber);

			SetWGenData(pData, uCount);

			m_pCtx->m_dGenWrite[0] = dData;

			break;
		}

	if( Transact(FALSE) ) {

		if( uT == SPGENW ) m_bGenWAck = m_bRx[2];

		return 1;
		}

	return CCODE_ERROR;
	}

void CMELServoDriver::WriteCommand(BYTE bCommand, BYTE bDataNumber)
{
	StartFrame(bCommand, bDataNumber);
	AddWord(KACT);
	}

void CMELServoDriver::DoTOM(UINT uOffset, PBYTE pCNum, PBYTE pDNum, DWORD dData)
{
	BOOL fJ3 = IsJ3Device();

	switch( uOffset ) {

		case 0: *pDNum = DTOM0;	break;
		case 1: *pDNum = DTOM1;	break;
		case 2: *pDNum = fJ3 ?  DTOMJ2 : DTOM2;	break;
		case 3: *pDNum = fJ3 ?  DTOMJ3 : DTOM3;	break;
		case 4: *pDNum = fJ3 ?  DTOMJ4 : DTOM4;	break;

		case 5:
			if( fJ3 ) *pDNum = DTOMJ5;

			else {
				*pCNum = CEX0W;
				*pDNum = DTOMI;
				}
			break;

		case 6:
			*pCNum = CEX0W;
			*pDNum = fJ3 ? DTOMI : DTOMO;
			break;

		case 7:
			*pCNum = CEX0W;
			*pDNum = DTOMO;
			break;
		}

	StartFrame(*pCNum, *pDNum);

	switch( uOffset ) {

		case 0:
			AddWord(LOWORD(dData));
			break;

		case 1:	AddLong(dData);
			break;

		case 2:
			fJ3 ? AddLong(dData) : AddWord(KACT);
			break;

		case 3:
			if( fJ3 ) {

				UINT u;

				u = 0;

				switch( dData ) {

					case 10: u = 0;	break;
					case 11: u = 1;	break;
					case 20: u = 4;	break;
					case 21: u = 5;	break;
					}

				AddWord(u);
				}

			else AddLong(dData);
			break;

		case 4:
			AddWord(KACT);
			break;

		case 5:
			if( fJ3 ) {

				UINT i;
				UINT u;

				u = 4 * (dData-1);

				for( i = 0; i < 4; i++, u++ ) AddByte(m_pTom[u]);
				}

			else AddLong(dData);
			break;

		case 6:
		case 7:
			AddLong(dData);
			break;
		}
	}

void CMELServoDriver::MakeHexFromFloat(DWORD dData)
{
	if( !dData || dData == 0x80000000 ) {

		AddWordAddHex(0, 0);
		return;
		}

	char cFloat[32] = {0};

	DWORD dResult = 0;

	BOOL fNeg     = FALSE;

	if( dData & 0x80000000 ) {

		fNeg   = TRUE;
		dData &= 0x7FFFFFFF;
		}

	SPrintf(cFloat, "%f", PassFloat(dData));

	UINT i     = 0;
	UINT uLen  = min(strlen(cFloat), sizeof(cFloat) - 1);

	cFloat[uLen] = 0;

	BOOL fDone = FALSE;

	while( !fDone ) {

		BYTE b = cFloat[i++];

		if( b == '.' ) fDone = TRUE;

		else {
			dResult *= 10;
			dResult += (b - '0');
			}

		if( i > uLen ) fDone = TRUE;
		}

	UINT uDP = m_uDecimal == 0 || m_uDecimal == 1 ? 0 : m_uDecimal - 1;

	fDone = (i >= uLen) || !uDP;

	while( !fDone ) {

		if( cFloat[i] ) {

			if( uDP ) {

				dResult *= 10;
				dResult += cFloat[i] - '0';
				uDP--;
				}

			else {
				if( cFloat[i] >= '5' ) dResult++;
				fDone = TRUE;
				}
			}

		else {
			while( uDP ) {

				dResult *= 10;
				uDP--;
				}

			fDone = TRUE;
			}

		if( dResult > 0x7FFFFF ) {

			AddWordAddHex(fNeg ? 0xFFFF : 0x7FFF, 0xFF);
			return;
			}

		i++;
		}

	if( fNeg ) dResult = -dResult;

	AddWordAddHex(dResult >> 8, LOBYTE(dResult));
	}

// Helpers
MELSERVOCmdDef * CMELServoDriver::SetMELSCommand(UINT uTable)
{
	MELSERVOCmdDef * p;

	p = m_pMCL;

	for( UINT i = 0; i < elements(CL); i++ ){

		if( p->uID == uTable ) return p;

		p++;
		}

	return NULL;
	}

void CMELServoDriver::SetStationNumber(void)
{
	UINT uStation = m_pCtx->m_uStation;

	switch( m_pCtx->m_uGroup ) {

		case ISSTATION:

			if( uStation < 10 ) m_pCtx->m_bStation = '0' + uStation;

			else m_pCtx->m_bStation = 'A' + uStation - 10;

			break;

		case ISGROUP:

			m_pCtx->m_bStation = 'a' + (uStation % 7); // a - f valid

			break;

		case ISALLUNITS:

			m_pCtx->m_bStation = '*';

			break;
		}
	}

BOOL CMELServoDriver::NoReadTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPWEEP:
			*pData = m_dWEEPResult;
			return TRUE;

		case SPGENW:
			UINT i;
			UINT j;
			DWORD d;

			memset(pData, 0, uCount * sizeof(DWORD));

			for( i = 0; i < uCount; i++, pData++ ) {

				*pData = m_pCtx->m_dGenWrite[i];

				d = ((DWORD) m_bGenWAck) << 24;

				for( j = 0xFF000000; j; j >>= 8, d >>= 8 ) {

					if( !(j & *pData) ) {

						*pData |= d;

						return TRUE;
						}
					}
				}

			return TRUE;

		case SPEXT0:
		case SPEXTA:
			if( Addr.a.m_Offset > 4 ) {

				*pData = 0;
				return TRUE;
				}

			break;

		case SPGRP:
			return Addr.a.m_Offset > 3;

		case SPTOM:
			*pData = IsJ3Device() ? 0 : 0xFFFFFFFF;
			return TRUE;

		case SPBAD:
			return TRUE;
		}

	return FALSE;
	}

BOOL CMELServoDriver::NoWriteTransmit(AREF Addr, DWORD dData)
{
	if( m_pMItem->uWCmd == NOXMIT ) return TRUE;

	UINT uO = Addr.a.m_Offset;

	switch( Addr.a.m_Table ) {

		case SPACU:
		case SPACL:
		case SPSCL:
			return !dData;

		case SPOMS:
			m_pCtx->m_dCache[COMS0] = dData; // save data for read via cache
			return FALSE; // proceed with writing data to drive

		case SPTOM:
			if( IsJ3Device() ) {

				if( uO == 3 ) {

					switch( dData ) {

						case 10:
						case 11:
						case 20:
						case 21:
							return FALSE;
						}

					return TRUE;
					}

				if( uO == 5 ) {

					return dData < 1 || dData > 3; // invalid values
					}

				return FALSE;
				}

			switch( uO ) { // save relevant TOM data for read via cache

				case 0:
				case 1:
					m_pCtx->m_dCache[CTOM0 + Addr.a.m_Offset] = dData;
					break;

				case 3:
					m_pCtx->m_dCache[CTOM3] = dData;
					break;
				}

			return FALSE; // proceed with writing data to drive

		case SPGSV:
			return dData < 0xA || (dData > 0xF && dData < 0x1A) || dData > 0x1F;

		case SPWEEP:
			if( !dData ) m_dWEEPResult = 0;

			return dData < 1 || dData > 31;

		case SPGRP:
			return uO > 3;

		case SPSTS3U:
		case SPSTS3N:
		case SPPARU:
		case SPPARUH:
		case SPPARL:
		case SPPARLH:
		case SPPARAB:
		case SPPAREN:
		case SPALSU:
		case SPALSN:
		case SPTOM3M:
		case SPTOM3S:
		case SPGENR:
		case SPBAD:
			return TRUE;
		}

	return FALSE;
	}

BOOL CMELServoDriver::GroupResponseSet(void)
{
	if( m_pCtx->m_uStation >= 10 && m_pCtx->m_uStation <= 15 ) {

		return m_bGroup & (1 << (m_pCtx->m_uStation - 10));
		}

	return FALSE;
	}

void CMELServoDriver::SetGroupResponse(BYTE bData)
{
	if( bData >= 0x1A && bData <= 0x1F ) {

		m_bGroup |= (1 << (m_pCtx->m_uStation - 10));
		}

	else {
		if( bData >= 0xA && bData <= 0xF ) {

			m_bGroup &= ~(1 << (m_pCtx->m_uStation - 10));
			}
		}
	}

DWORD CMELServoDriver::WritePointToEEPROM(BYTE bPointNumber)
{
	BYTE  bEEPPoint[42] = {0};

	DWORD uResult = 0;

	UINT uPos;
	UINT uPtPos = 0;

	for( UINT uOp = SPPOS; !uResult && uOp <= SPAUX; uOp++ ) {

		StartFrame(SetMELSCommand(uOp)->uRCmd, bPointNumber);

		if( Transact(TRUE) ) {

			for( uPos = 1; uPos <= 7; uPos++, uPtPos++ ) { // 0=Display Type, not needed

				bEEPPoint[uPtPos] = m_bRx[uPos];
				}
			}
		
		else uResult = uOp + 101 - SPPOS;
		}

	uPtPos = 0;

	for( uOp = SPPOS; !uResult && uOp <= SPAUX; uOp++ ) {

		StartFrame(SetMELSCommand(uOp)->uWCmd, bPointNumber);

		AddByte('0'); // write to EEPROM - replaces Display Type of Read

		for( uPos = 0; uPos < 7; uPos++, uPtPos++ ) {

			AddByte(bEEPPoint[uPtPos]);
			}

		if( !Transact(FALSE) ) uResult = uOp + 201 - SPPOS;
		}

	m_pMItem = SetMELSCommand(SPWEEP);

	return uResult;
	}

BOOL CMELServoDriver::IsGeneric(UINT uTable)
{
	return uTable == SPGENR || uTable == SPGENW;
	}

BOOL CMELServoDriver::GetWGenCodes(DWORD d, PBYTE pC, PBYTE pD)
{
	for( UINT i = 0xFF000000; i; i >>= 8 ) {

		if( !(d & i) ) return FALSE;
		}

	*pC = (FromHex((d >> 24) & 0xFF) << 4) + (FromHex((d >> 16) & 0xFF));
	*pD = (FromHex((d >>  8) & 0xFF) << 4) + (FromHex(d & 0xFF));

	return TRUE;
	}

void CMELServoDriver::SetWGenData(PDWORD p, UINT uCount)
{
	ClearGenWrite();

	for( UINT i = 1; i < uCount; i++ ) {

		DWORD d = p[i];

		m_pCtx->m_dGenWrite[i] = d;

		UINT j = 32;

		while( j ) {

			j -= 8;

			BYTE b = d >> j;

			if( b ) AddByte(b);

			else return;
			}
		}
	}

void CMELServoDriver::ClearGenWrite(void)
{
	memset(m_pCtx->m_dGenWrite, 0, sizeof(m_pCtx->m_dGenWrite));
	}

void CMELServoDriver::SendCommsReset(void)
{
//**/	AfxTrace0("\r\nEOT");

	m_pData->Write( EOT, FOREVER );

	Sleep(100); // defined by specification
	}

BOOL CMELServoDriver::IsStringItem(UINT uTable)
{
	switch( uTable ) {

		case SPSTS3U:
		case SPSTS3N:
		case SPALSU:
		case SPALSN:
		case SPSVR:
		case SPGENW:
			return TRUE;
		}

	return FALSE;
	}

BOOL CMELServoDriver::IsUnitString(UINT uTable)
{
	return uTable == SPSTS3U || uTable == SPALSU;
	}

BOOL CMELServoDriver::IsJ3NewGroup(UINT uTable, UINT uExtra)
{
	switch( uTable ) {

		case SPPAR3:
		case SPPAR3H:
		case SPPARU:
		case SPPARUH:
		case SPPARL:
		case SPPARLH:
		case SPPARAB:
		case SPPAREN:

			if( uExtra != m_uJ3Group ) {

//**/				AfxTrace2("\r\nChange Group Old=%d New=%d ", m_uJ3Group, uExtra);

				m_uJ3Group = uExtra;

				return TRUE;
				}
			break;
		}

	return FALSE;
	}

BOOL CMELServoDriver::ChangeGroup(void)
{
//**/	AfxTrace1("\r\nGroup Change %d ", m_uJ3Group);

	UINT uOldID = m_pMItem->uID;

	SetMELSCommand(SPGRP);

	BOOL fOkay = FALSE;

	StartFrame(CGPSW, 0);

	AddWord(m_uJ3Group);

	if( Transact(FALSE) ) {

		StartFrame(CGPSR, 1);

		if( Transact(TRUE) ) {

			fOkay = FromHex(m_bRx[3]) == m_uJ3Group;
			}
		}

	SetMELSCommand(uOldID);

	return fOkay;
	}

BOOL CMELServoDriver::IsJ3Device(void)
{
	return m_pCtx->m_uServoType == J3A;
	}

// End of File
