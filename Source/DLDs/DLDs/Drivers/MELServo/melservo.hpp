
//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi MELServo
//

// Device ID's
enum {
	J2SCL	= 0,
	J2SA	= 1,
	J2SCP	= 2,
	J3A	= 3,
	};

// Data Spaces
enum {
	SPPARLP	= 1,	// Parameter in CL
	SPPARA	= 2,	// Parameter in A
	SPSTSL	= 3,	// Status in CL
	SPSTSA	= 4,	// Status in A
	SPALN	= 5,	// Alarm History Number
	SPALT	= 6,	// Alarm History Time
	SPALSL	= 7,	// Status @ Alarm Occurrence CL
	SPALSA	= 8,	// Status @ Alarm Occurrence A
	SPACU	= 9,	// Current Alarm Number
	SPEXT0	= 10,	// External I/O Signals CL
	SPEXTA	= 11,	// External I/O Signals A
	SPLAT	= 12,	// Current Position Latch Data
	SPGPRR	= 13,	// General Purpose R Registers
	SPGPDR	= 14,	// General Purpose D Registers
	SPGSV	= 15,	// Group Setting Value
	SPSEP	= 16,	// Servo Absolute Position
	SPCUP	= 17,	// Command Absolute Position
	SPSVR	= 18,	// Software Version
	SPACL	= 19,	// Alarm History Clear
	SPSCL	= 20,	// Status Display Clear
	SPOMS	= 21,	// Operation Mode Select
	SPTOM	= 22,	// Test Operation Mode
	SPEIS	= 23,	// External Input Signal Enable/Disable
	SPEOS	= 24,	// External Output Signal Enable/Disable
	SPWPEC	= 25,	// Write Parameter to EEPROM CL
	SPWPEA	= 26,	// Write Parameter to EEPROM A
	SPWGER	= 27,	// Write General Purpose R Register to EEPROM
// Following is not enabled in the configuration
	SPWGED	= 28,	// Write General Purpose D Register to EEPROM

	SPCACHE	= 30,	// Storage of written, unreadable values

// Additions for Model CP Apr 08
	SPSTSP	= 31,
	SPALSP	= 32,
	SPPOS	= 33,
	SPSPD	= 34,
	SPACC	= 35,
	SPDEC	= 36,
	SPDWL	= 37,
	SPAUX	= 38,
	SPWEEP	= 39,

// Generic Commands Apr 08
	SPGENR	= 40,
	SPGENW	= 41,

// Parameters as Integers
	SPPARLPH= 42,
	SPPARAH	= 43,

// July 25 2008 additions for MR-J3
	SPSTS3U	= 44,	// 01 00-0E Unit
	SPSTS3N	= 45,	// 01 00-0E Name
	SPGRP	= 46,	// 04 01 Parameter Group Read
	SPPAR3	= 47,	// 05 00-FF Parameter Read Real
	SPPAR3H	= 48,	// 05 00-FF Parameter Read Long
	SPPARU	= 49,	// 06 00-FF Parameter Upper Limit Real
	SPPARUH	= 50,	// 06 00-FF Parameter Upper Limit Long
	SPPARL	= 51,	// 07 00-FF Parameter Lower Limit Real
	SPPARLH	= 52,	// 07 00-FF Parameter Lower Limit Long
	SPPARAB	= 53,	// 08 00-FF Parameter Abbreviation
	SPPAREN	= 54,	// 09 00-FF Write enable/disable of Parameters
	SPALSU	= 55,	// 35 00-0E Unit
	SPALSN	= 56,	// 35 00-0E Name
	SPTOM3M	= 57,	// 00 12 Test Operation Mode Read
	SPTOM3S	= 58,	// 00 21 Read TOM status

	SPBAD	= 99,	// Invalid Selection for Gateway Block
	};

// Transmit enable/disable
#define	NOXMIT	0xAA

// Command/Data Number Definitions
// Read Commands
enum {
	CTOMR	= 0,
	CTOMR3	= 0,	// J3 Read Test Operation, mode or status
	CSTSR	= 0x1,
	CACUR	= 0x2,
	CSEPR	= 0x2,
	CCUPR	= 0x2,
	CSVRR	= 0x2,
	CGPSR	= 0x4,	// J3 Parameter Group Read
	CPARR	= 0x5,
	CPARU	= 0x6,	// J3 Read Upper Limit
	CPARL	= 0x7,	// J3 Read Lower Limit
	CPARA	= 0x8,	// J3 Read Abbreviation
	CPARE	= 0x9,	// J3 Read Write Enable/Disable
	CEXTR	= 0x12,
	CGSVR	= 0x1F,
	CALNR	= 0x33,
	CALTR	= 0x33,
	CALSR	= 0x35,
	CPOSR	= 0x40,
	CSPDR	= 0x50,
	CACCR	= 0x54,
	CDECR	= 0x58,
	CDWLR	= 0x60,
	CAUXR	= 0x64,
	CLATR	= 0x6C,
	CGPRR	= 0x6D,
	CGPDR	= 0x6E,
	CGENR	= 0xFF,
	};

// Commands that cannot be read
#define	CACLR	NOXMIT
#define	CARSR	NOXMIT
#define	CSCLR	NOXMIT
#define	COMSR	NOXMIT
#define	CEISR	NOXMIT
#define	CEOSR	NOXMIT
#define	CEEPR	NOXMIT

// Write Commands
enum {		   
	CSCLW	= 0x81,
	CACUW	= 0x82,
	CACLW	= 0x82,
	CPARW	= 0x84,
	CWPEW	= 0x84,
	CGPSW	= 0x85,	// J3 Parameter Group Write
	COMSW	= 0x8B,
	CEISW	= 0x90,
	CEOSW	= 0x90,
	CEX0W	= 0x92,
	CGSVW	= 0x9F,
	CTOMW	= 0xA0,
	CGPRW	= 0xB9,
	CWGRW	= 0xB9,
	CGPDW	= 0xBA,
	CWGDW	= 0xBA,
	CPOSW	= 0xC0,
	CSPDW	= 0xC6,
	CACCW	= 0xC7,
	CDECW	= 0xC8,
	CDWLW	= 0xCA,
	CAUXW	= 0xCB,
	CEEPW	= 0xFF,
	CGENW	= 0xFF,
	};

// Commands that cannot be written
#define	CSTSW	NOXMIT
#define	CALNW	NOXMIT
#define	CALTW	NOXMIT
#define	CALSW	NOXMIT
#define	CEX1W	NOXMIT
#define	CLATW	NOXMIT
#define	CSEPW	NOXMIT
#define	CCUPW	NOXMIT
#define	CSVRW	NOXMIT
#define	CPARX	NOXMIT

// Data Number Offsets
enum {
	OFFACU	= 0,
	OFFGP3	= 1,	// J3 Read Group setting
	OFFALN	= 0x10,
	OFFTOM	= 0x10,
	OFFALT	= 0x20,
	OFFT32	= 0x20, // J3 Test Operation Mode Write, Item 2
	OFFT33	= 0x21, // J3 Test Operation Mode Write, Item 3
	OFFT34	= 0x40, // J3 Test Operation Mode Write, Item 4
	OFFT35	= 0x41, // J3 Test Operation Mode Write, Item 5
	OFFSTS	= 0x80,
	OFFALS	= 0x80,
	};

// Fixed Data Numbers
enum {
	DACU	= 0,
	DGSV	= 0,
	DSCLW	= 0,
	DOMSW	= 0,
	DTOMI	= 0,
	DEIS0	= 0,
	DEXT0	= 0,
	DSTS3U	= 0,
	DLAT	= 1,
	DGRP	= 1,
	DSTS3N	= 1,
	DEOS0	= 3,
	DTOM0	= 0x10,
	DEIS1	= 0x10,
	DTOM1	= 0x11,
	DTOM2	= 0x12,
	DTOM3	= 0x13,
	DEOS1	= 0x13,
	DTOM4	= 0x15,
	DACLW	= 0x20,
	DTOMJ2	= 0x20,
	DTOM5	= 0x21,
	DTOMJ3	= 0x21,
	DEXT1	= 0x40,
	DTOMJ4	= 0x40,
	DTOMJ5	= 0x41,
	DEXTW	= 0x60,
	DEXT2	= 0x60,
	DSVRR	= 0x70,
	DEXT3	= 0x80,
	DSEPR	= 0x90,
	DCUPR	= 0x91,
	DTOMO	= 0xA0,
	DEXT4	= 0xC0,
// Variable Data Numbers
	DVARI	= 0xFF,
	};

// Device Context
enum {
	ISSTATION	= 0,
	ISGROUP		= 1,
	ISALLUNITS	= 2,
	};

// TOM, OMS data storage
enum {
	CTOM0	= 0,
	CTOM1	= 1,
	CTOM3	= 2,
	COMS0	= 3,
	CMAGIC	= 5,
	};

// Command Definitions

struct FAR MELSERVOCmdDef {
	UINT	uID;
	UINT	uRCmd;
	UINT	uWCmd;
	UINT	uDataNumber;
	UINT	uReplySize;
	UINT	uReadDataSize;
	};

// Data Constants
#define	KACT	0x1EA5
#define	KWRAM	'3'
#define	KWROM	'0'
#define	KNODP	'0'

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi MELServo Driver
//

class CMELServoDriver : public CMasterDriver
{
	public:
		// Constructor
		CMELServoDriver(void);

		// Destructor
		~CMELServoDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			UINT	m_uStation;
			UINT	m_uGroup;
			BYTE	m_bStation;
			UINT	m_uServoType;
			DWORD	m_dCache[6];
			DWORD	m_dGenWrite[4];
			};

		// Data Members
		CContext *	m_pCtx;
		LPCTXT		m_pHex;
		LPCTXT		m_pTom;

		BYTE	m_bTx[64];
		BYTE	m_bRx[64];
		BYTE	m_bGroup;
		BYTE	m_bGenWAck;

		UINT	m_uPtr;
		UINT	m_uChecksum;
		UINT	m_uDecimal;
		UINT	m_uWriteErr;
		UINT	m_uJ3Group;

		DWORD	m_dWEEPResult;

		// Command Definition Table
		static MELSERVOCmdDef CODE_SEG CL[];
		MELSERVOCmdDef FAR * m_pMCL;
		MELSERVOCmdDef FAR * m_pMItem;
				
		// Implementation

		WORD	FromHex(BYTE bData);

		// Port Access
		UINT	Get(UINT uTime);
		
		// Frame Building
		void	StartFrame(BYTE bCommand, BYTE bDataNumber);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dData);
		void	AddHex(BYTE bData);
		void	AddWordAddHex(WORD w, BYTE h);
		
		// Transport Layer
		void	PutFrame(void);
		BOOL	GetFrame(BOOL fIsRead);
		BOOL	Transact(BOOL fIsRead);

		// Read Handlers
		CCODE	DoRead(AREF Addr, PDWORD pData, UINT uCount);
		void	GetReadResponse(PDWORD pData, UINT uCount);
		DWORD	MakeRealValue(DWORD dData);

		// Write Handlers
		CCODE	DoWrite(AREF Addr, PDWORD pData, UINT uCount);
		void	WriteCommand(BYTE bCommand, BYTE bDataNumber);
		void	DoTOM(UINT uOffset, PBYTE pCNum, PBYTE pDNum, DWORD dData);
		void	MakeHexFromFloat(DWORD dData);

		// Helpers
		MELSERVOCmdDef * SetMELSCommand(UINT uTable);
		void	SetStationNumber(void);
		BOOL	NoReadTransmit(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(AREF Addr, DWORD dData);
		BOOL	GroupResponseSet(void);
		void	SetGroupResponse(BYTE bData);
		DWORD	WritePointToEEPROM(BYTE bPointNumber);
		BOOL	IsGeneric(UINT uTable);
		BOOL	GetWGenCodes(DWORD d, PBYTE pC, PBYTE pD);
		void	SetWGenData(PDWORD p, UINT uCount);
		void	ClearGenWrite(void);
		void	SendCommsReset(void);
		BOOL	IsStringItem(UINT uTable);
		BOOL	IsUnitString(UINT uTable);
		BOOL	IsJ3NewGroup(UINT uTable, UINT uExtra);
		BOOL	ChangeGroup(void);
		BOOL	IsJ3Device(void);
	};

// End of File
