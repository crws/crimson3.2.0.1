
#include "s5base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Master
//

#define	TIMEOUT		500

#define	GAP_LENGTH	100 / PAUSE

#define	PAUSE		10

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Master
//

class CS5AS511Master : public CS5AS511Base
{
	public:
		// Constructor
		CS5AS511Master(void);

		// Destructor
		~CS5AS511Master(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:
		// Data
		BOOL m_fCache;
		BOOL m_fAddr32;

		// Transport
		void ClearRx(void);
		BOOL Send(PCBYTE pData, UINT uLen);
		BOOL Recv(PBYTE  pData, UINT &uLen, BOOL fTerm);
	};

// End of File
