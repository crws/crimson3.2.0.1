
#include "intern.hpp"

#include "WebCamera.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Web Camera Driver
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Timeouts

static UINT const timeSendTimeout  = 5000;

static UINT const timeSendDelay	   = 5;

static UINT const timeRecvTimeout  = 10000;

static UINT const timeRecvDelay	   = 5;

static UINT const timeBuffDelay    = 100;

// Instantiator

INSTANTIATE(CWebCamera);

// Constructor

CWebCamera::CWebCamera(void)
{
	m_uCount   = 0;
	
	m_pCtx     = NULL;

	m_pHead    = NULL;

	m_pTail    = NULL;

	m_pHelper  = NULL;
	}

// Destructor

CWebCamera::~CWebCamera(void)
{
	if( m_pExtra ) {

		m_pExtra->Release();
		}
	}

// Management

void MCALL CWebCamera::Attach(IPortObject *pPort)
{
	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

void MCALL CWebCamera::Detach(void)
{
	}

void MCALL CWebCamera::Open(void)
{	
	}

// Device

CCODE MCALL CWebCamera::DeviceOpen(IDevice *pDevice)
{
	CCameraDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {
			
			m_pCtx = new CContext;

			m_pCtx->m_uDevice    = GetWord(pData);
			m_pCtx->m_fPing      = TRUE;
			m_pCtx->m_IP         = GetAddr(pData);
			m_pCtx->m_uPort      = GetWord(pData);
			m_pCtx->m_Mode       = GetByte(pData);
			m_pCtx->m_uTime1     = 2000;
			m_pCtx->m_uTime2     = 1000;
			m_pCtx->m_uTime3     = 500;
			m_pCtx->m_uTime4     = GetWord(pData);
			m_pCtx->m_uLast3     = GetTickCount();
			m_pCtx->m_uLast4     = GetTickCount();
			m_pCtx->m_pSock      = NULL;
			m_pCtx->m_pData      = NULL;
			m_pCtx->m_Path       = GetString(pData);
			m_pCtx->m_uFrame     = 1;
			m_pCtx->m_uAuth      = GetByte(pData);
			m_pCtx->m_uScale     = GetLong(pData);
			m_pCtx->m_User       = GetString(pData);
			m_pCtx->m_Pass       = GetString(pData);
			m_pCtx->m_fDirty     = FALSE;
			m_pCtx->m_fForce     = FALSE;
			m_pCtx->m_uUpdate    = GetByte(pData);
			m_pCtx->m_uPeriod    = GetLong(pData);
			m_pCtx->m_uLastImage = 0;

			m_pCtx->m_pHttpHead = NULL;
			m_pCtx->m_pHttpTail = NULL;
			m_pCtx->m_Auth[0]   = NULL;
			m_pCtx->m_Auth[1]   = NULL;

			memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			BuildHttpGetRequest(m_pCtx, httpAuthNone);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CWebCamera::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {  
		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		CloseSocket(FALSE);

		Free(m_pCtx->m_Path);

		Free(m_pCtx->m_User);

		Free(m_pCtx->m_Pass);

		delete m_pCtx->m_Auth[0];

		delete m_pCtx->m_Auth[1];

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CCameraDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CWebCamera::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CWebCamera::ReadImage(PBYTE &pData)
{
	if( ShouldUpdateImage() ) {

		if( OpenSocket() ) {		

			if( GetImage(pData) ) {

				m_pCtx->m_uLast4 = GetTickCount();

				if( m_pCtx->m_uUpdate == updateManual ) {

					m_pCtx->m_fForce = FALSE;
					}				
				else {
					m_pCtx->m_uLastImage = GetTickCount();
					}

				return CCODE_SUCCESS;
				}
			}
		}

	if( Ping() == CCODE_SUCCESS ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast4;

		UINT tt = ToTicks(1000 * m_pCtx->m_uTime4);

		if( !tt || dt < tt ) {

			pData = NULL;

			return CCODE_SUCCESS;
			}
		}

	CloseSocket(FALSE);

	return CCODE_ERROR;
	}

void MCALL CWebCamera::SwapImage(PBYTE pData)
{
	delete m_pCtx->m_pData;

	m_pCtx->m_uInfo[0] = GetTimeStamp();

	m_pCtx->m_uInfo[1] = ((BITMAP_RLC *) pData)->Frame;

	m_pCtx->m_pData    = pData;
	}

void MCALL CWebCamera::KillImage(void)
{
	if( m_pCtx->m_pData ) {
		
		memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

		delete m_pCtx->m_pData;

		m_pCtx->m_pData = NULL;
		}
	}

void MCALL CWebCamera::Service(void)
{
	}

PCBYTE MCALL CWebCamera::GetData(UINT uDevice)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			return pCtx->m_pData;
			}
		}

	return NULL;
	}

UINT MCALL CWebCamera::GetInfo(UINT uDevice, UINT uParam)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			if( uParam < elements(pCtx->m_uInfo) ) {

				return pCtx->m_uInfo[uParam];
				}
			}
		}

	return 0;
	}

// Helpers

void CWebCamera::Format(PTXT p, LPCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	VSPrintf(p, pText, pArgs);

	va_end(pArgs);
	}

void CWebCamera::Trace(LPCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	AfxTrace(pText, pArgs);

	va_end(pArgs);
	}

void CWebCamera::GetRandomBytes(PBYTE pRandom, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		pRandom[n] = rand() & 0xFF;
		}
	}

// User Access

UINT MCALL CWebCamera::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		pCtx->m_IP     = MotorToHost(dwValue);
		
		pCtx->m_fDirty = TRUE;

		BuildHttpGetRequest(pCtx, httpAuthNone);

		Free(pText);

		return 1;    
		} 
	
	if( uFunc == 2 ) {
		
		// Get Current IP Address
		
		return MotorToHost(pCtx->m_IP);
		}

	if( uFunc == 3 ) {

		// Trigger Image Update

		if( pCtx->m_uUpdate == updateManual ) {

			pCtx->m_fForce = TRUE;
			}
		}
	
	return 0;
	}

// Socket Management

BOOL CWebCamera::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CWebCamera::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast3;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT       uPort   = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {
			
					m_pCtx->m_uLast4 = GetTickCount();
					
					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CWebCamera::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )

			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock  = NULL;

		m_pCtx->m_uLast3 = GetTickCount();

		m_uKeep--;
		}
	}

// Implementation

BOOL CWebCamera::GetImage(PBYTE &pData)
{
	if( OpenSocket() && Send(m_pCtx->m_pSock, m_pCtx->m_Http, PTXT(&m_pCtx->m_Http + 1)) ) {

		UINT uTicks = GetTickCount();

		UINT uCode  = RecvHttpResponse();

		if( uCode == 401 ) {

			for( UINT n = 0; n < 2; n++ ) {

				PCTXT pField = GetHttpField("www-authenticate", n);

				if( pField ) {

					if( !m_pCtx->m_Auth[n] ) {

						m_pCtx->m_Auth[n] = new CHttpAuth(this, m_pCtx->m_User, m_pCtx->m_Pass, m_pCtx->m_Path);
						}

					ParseAuth(m_pCtx->m_Auth[n], pField);
					}
				}

			BuildHttpGetRequest(m_pCtx, m_pCtx->m_uAuth);

			ClearHttpFields();

			pData = NULL;
			
			return FALSE;
			}

		if( uCode == 200 ) {

			PCTXT pLength = GetHttpField("content-length", NOTHING);

			if( pLength ) {

				UINT  uLen = ATOI(pLength);

				PBYTE pImg = RecvImageData(uLen);

				if( pImg ) {

					PBYTE pResult = NULL;

					UINT  uFormat = FindImageFormat();

					UINT  uScale  = m_pCtx->m_Mode == imgModeJPG ? m_pCtx->m_uScale : 10000;

					switch( uFormat ) {

						case imgFormatJPG:

							pResult = m_pExtra->Jpeg2RLCBmp(pImg, uLen, uScale);

							break;

						case imgFormatBMP:

							pResult = m_pExtra->WinBmp2RLCBmp(pImg, uLen);

							break;
						}

					if( pResult ) {

						BITMAP_RLC *pBitmap = (BITMAP_RLC *) pResult;

						pBitmap->Frame = m_pCtx->m_uFrame++;

						pData = pResult;

						ClearHttpFields();

						delete [] pImg;

						return TRUE;
						}

					ClearHttpFields();

					delete [] pImg;
					}
				}
			}

		ClearHttpFields();
		}

	return FALSE;
	}

UINT CWebCamera::FindImageFormat(void)
{
	switch( m_pCtx->m_Mode ) {

		case imgModeJPG:

			return imgFormatJPG;

		case imgModePNG:

			return imgFormatPNG;

		case imgModeBMP:

			return imgFormatBMP;

		case imgModeAuto:

			return GuessImageFormat();
		}

	return imgFormatUnknown;
	}

UINT CWebCamera::GuessImageFormat(void)
{
	UINT uFormat    = imgFormatUnknown;

	PCTXT pContent  = GetHttpField("content-type", NOTHING);

	if( pContent ) {

		if( !stricmp(pContent, "image/jpeg") ) {

			uFormat = imgFormatJPG;
			}

		else if( !stricmp(pContent, "image/png") ) {

			uFormat = imgFormatPNG;
			}

		else if( !stricmp(pContent, "image/bmp") || !stricmp(pContent, "image/x-ms-bmp") ) {

			uFormat = imgFormatBMP;
			}
		}

	return uFormat;
	}

BOOL CWebCamera::ShouldUpdateImage(void)
{
	if( m_pCtx->m_uUpdate == updateManual ) {

		return m_pCtx->m_fForce;
		}
	else {
		UINT uPeriod = ToTicks(m_pCtx->m_uPeriod * 1000);

		UINT uDiff   = GetTickCount() - m_pCtx->m_uLastImage;

		if( !uPeriod || uDiff >= uPeriod ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CWebCamera::Send(ISocket *pSock, PCTXT pText, PTXT pArgs)
{
	UINT uSize = strlen(pText);

	PTXT pWork = new char [ uSize + 512 ];

	SPrintf(pWork, pText, pArgs);

	if( Send(pSock, PCBYTE(pWork), strlen(pWork)) ) {

		delete [] pWork;

		return TRUE;
		}

	delete [] pWork;

	return FALSE;
	}

BOOL CWebCamera::Send(ISocket *pSock, PCBYTE pText, UINT uSize)
{
	UINT uLimit = 1280;

	while( uSize ) {

		CBuffer *pBuff = CreateBuffer(uLimit, TRUE);

		if( pBuff ) {

			PBYTE pData = pBuff->GetData();

			UINT  uCopy = min(uLimit, uSize);

			memcpy(pData, pText, uCopy);

			pBuff->AddTail(uCopy);  

			SetTimer(timeSendTimeout);

			while( pSock->Send(pBuff) == E_FAIL ) {

				UINT Phase;

				pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN && GetTimer() ) {

					Sleep(timeSendDelay);

					continue;
					}

				BuffRelease(pBuff);
				
				return FALSE;
				}

			pText += uCopy;

			uSize -= uCopy;
			}
		else {  
			Sleep(timeBuffDelay);

			return FALSE;
			}
		}

	return TRUE;
	}

void CWebCamera::GetValue(PCTXT &pVal, PCTXT pText)
{
	PTXT pCopy = strdup(pText);

	UINT uLen  = strlen(pCopy);

	pCopy[uLen-1] = '\0';

	memmove(pCopy, pCopy+1, uLen);

	if( pVal ) {

		free(PVOID(pVal));
		}

	pVal = pCopy;
	}

// Limited HTTP Support

void CWebCamera::BuildHttpGetRequest(CContext *pCtx, UINT uAuth)
{	
	memset(pCtx->m_Http, 0, HTTP_SIZE);

	IPADDR const &IP = (IPADDR const &) pCtx->m_IP;

	char Host[128] = {0};

	SPrintf( Host,
		"%u.%u.%u.%u",
		IP.m_b1,
		IP.m_b2,
		IP.m_b3,
		IP.m_b4
		);

	SPrintf( pCtx->m_Http,
		"GET %s HTTP/1.1\r\n"
		"Accept: image/jpeg, image/bmp\r\n"
		"Accept-Language: en-us\r\n"
		"Accept-Encoding: identity\r\n"
		"Host: %s\r\n"
		"Connection: Keep-Alive\r\n"
		"User-Agent: Red Lion Controls - Image Grabber - 1.0\r\n",

		pCtx->m_Path,
		Host
		);

	if( uAuth == httpAuthNone ) {

		strcat(pCtx->m_Http, "\r\n");
		}

	else if ( uAuth == httpAuthBasic ) {

		AddBasicAuth();
		}

	else if( uAuth == httpAuthDigest ) {

		AddDigestAuth();
		}
	}

void CWebCamera::AddBasicAuth(void)
{
	CPrintf Comb("%s:%s", m_pCtx->m_User, m_pCtx->m_Pass);

	CString Code = CBase64::ToBase64(Comb);

	strcat(m_pCtx->m_Http, Code);
	}

void CWebCamera::AddDigestAuth(void)
{
	CHttpAuth *pAuth = NULL;

	if( m_pCtx->m_Auth[0]->m_uType == httpAuthDigest ) {

		pAuth = m_pCtx->m_Auth[0];
		}
	
	if( m_pCtx->m_Auth[1]->m_uType == httpAuthDigest ) {

		pAuth = m_pCtx->m_Auth[1];
		}

	if( pAuth ) {

		pAuth->CalculateResponse();

		strcat  (m_pCtx->m_Http, "Authorization: Digest ");

		AddParam(m_pCtx->m_Http, "username", m_pCtx->m_User,             TRUE);

		AddParam(m_pCtx->m_Http, "realm",    pAuth->GetRealm(),          TRUE);

		AddParam(m_pCtx->m_Http, "nonce",    pAuth->GetNonce(),          TRUE);

		AddParam(m_pCtx->m_Http, "uri",      m_pCtx->m_Path,             TRUE);

		AddParam(m_pCtx->m_Http, "response", pAuth->CalculateResponse(), TRUE);

		AddParam(m_pCtx->m_Http, "nc",       pAuth->GetNonceCount(),     FALSE);

		AddParam(m_pCtx->m_Http, "cnonce",   pAuth->GetCNonce(),         TRUE);

		AddParam(m_pCtx->m_Http, "qop",      pAuth->GetQop(),            FALSE);

		strcat(m_pCtx->m_Http, "\r\n\r\n");
		}
	}

void CWebCamera::AddParam(PTXT pDest, PCTXT pName, PCTXT pValue, BOOL fQuoted)
{
	UINT uLen  = strlen(pName) + strlen(pValue);

	uLen += fQuoted ? 5 : 3;

	PTXT pText = new char[ uLen ];

	if( fQuoted ) {

		SPrintf( pText,
			 "%s=\"%s\" ",
			 pName,
			 pValue
			 );
		}
	else {
		SPrintf( pText,
			 "%s=%s ",
			 pName,
			 pValue
			 );
		}
	
	strcat(m_pCtx->m_Http, pText);

	delete [] pText;
	}

UINT CWebCamera::RecvHttpResponse(void)
{
	UINT uCode  = 0;

	UINT uState = 0;

	UINT uLine  = 2 * 1024;

	PTXT pLine  = new char[uLine];

	memset(pLine, 0, uLine);

	UINT n = 0;

	BOOL fFirst = TRUE;

	BOOL fQuick = FALSE;

	SetTimer(timeRecvTimeout);

	while( GetTimer() ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {

			BYTE     b = 0;

			UINT uSize = 1;

			if( m_pCtx->m_pSock->Recv(&b, uSize) == S_OK ) {

				fQuick = TRUE;

				if( n >= uLine ) {

					delete [] pLine;

					return 0;
					}

				char c = (char) b;

				if( c == '\r' ) {

					pLine[n++] = c;

					uState = 1;
					}

				else if( c == '\n' ) {

					if( uState == 1 ) {

						pLine[n-1] = '\0';

						pLine[n++] = '\0';

						if( strlen(pLine) == 0 ) {

							delete [] pLine;

							return uCode;
							}

						else if( fFirst ) {

							uCode  = GetRepsonseCode(pLine);

							fFirst = FALSE;
							}
						else {
							PCTXT pName  = FindAndAllocName(pLine);

							PCTXT pValue = FindAndAllocValue(pLine);

							CHttpHeaderField *pField = new CHttpHeaderField;

							memset(pField, 0, sizeof(*pField));

							pField->m_pName  = pName;

							pField->m_pValue = pValue;

							AddHttpField(pField);
							}

						uState = 0;

						n = 0;

						memset(pLine, 0, uLine);
						}
					else {
						pLine[n++] = c;
						}
					}
				else {
					pLine[n++] = c;

					uState = 0;
					}
				}
			}

		if( Phase == PHASE_CLOSING ) {

			break;
			}

		if( Phase == PHASE_ERROR ) {

			break;
			}
		
		if( !fQuick ) {

			Sleep(timeRecvDelay);
			}
		}

	delete [] pLine;

	return 0;
	}

PBYTE CWebCamera::RecvImageData(UINT uLen)
{
	if( !OpenSocket() ) {

		return NULL;
		}

	if( uLen > 0 ) {

		UINT uPos = 0;

		PBYTE pWork = new BYTE[uLen];

		SetTimer(timeRecvTimeout);

		while( GetTimer() ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				UINT n = uLen - uPos;
				
				if( m_pCtx->m_pSock->Recv(pWork + uPos, n) == S_OK ) {

					if( uPos + n >= uLen ) {

						return pWork;
						}
								
					uPos += n;

					continue;
					}
				}

			if( Phase == PHASE_CLOSING ) {

				break;
				}

			if( Phase == PHASE_ERROR ) {

				break;
				}
			}
		}

	return NULL;
	}

void CWebCamera::AddHttpField(CHttpHeaderField *pField)
{
	AfxListAppend(  m_pCtx->m_pHttpHead,
			m_pCtx->m_pHttpTail,
			pField,
			m_pNext,
			m_pPrev
			);
	}

PCTXT CWebCamera::GetHttpField(PCTXT pName, UINT uIndex)
{
	if( pName ) {

		UINT uPos = 0;

		CHttpHeaderField *pField = m_pCtx->m_pHttpHead;

		while( pField ) {
		
			if( !stricmp(pField->m_pName, pName) ) {

				if( uIndex == NOTHING || uPos == uIndex ) {

					return pField->m_pValue;
					}

				uPos++;
				}

			pField = pField->m_pNext;
			}
		}

	return NULL;
	}

void CWebCamera::ClearHttpFields(void)
{
	CHttpHeaderField *pField;
		
	while( (pField = m_pCtx->m_pHttpHead ) ) {

		AfxListRemove(  m_pCtx->m_pHttpHead,
				m_pCtx->m_pHttpTail,
				pField,
				m_pNext,
				m_pPrev
				);

		Free(PVOID(pField->m_pName));

		Free(PVOID(pField->m_pValue));

		delete pField;
		}


	}

UINT CWebCamera::GetRepsonseCode(PCTXT pText)
{
	if( !strncmp(pText, "HTTP/1.1", 8) || !strncmp(pText, "HTTP/1.0", 8) ) {

		PCTXT p = pText + 9;

		while( isspace(*p) ) {

			p++;
			}

		UINT uCode = ATOI(p);

		return uCode;
		}

	return 0;
	}

PCTXT CWebCamera::FindAndAllocName(PCTXT pText)
{
	PCTXT pFind = strchr(pText, ':');

	if( pFind ) {

		UINT  uLen = pFind - pText;

		PTXT pName = PTXT(Alloc(uLen + 1));

		memcpy(PVOID(pName), pText, uLen);

		pName[uLen] = '\0';

		return pName;
		}

	return NULL;
	}

PCTXT CWebCamera::FindAndAllocValue(PCTXT pText)
{
	PCTXT pFind = strchr(pText, ':');

	if( pFind ) {

		PCTXT p = pFind + 1;

		while( isspace(*p) ) {

			p++;
			}

		PCTXT pValue = strdup(p);

		return pValue;
		}

	return NULL;
	}

BOOL CWebCamera::ParseAuth(CHttpAuth *pAuth, PCTXT pText)
{
	PCTXT p = pText;

	UINT uPos = 0;

	if( !strnicmp(p, "Basic", 5) ) {

		p += 5;

		uPos = 0;

		pAuth->m_uType = httpAuthBasic;
		}

	else if( !strnicmp(p, "Digest", 6) ) {

		p += 6;

		uPos = 1;

		pAuth->m_uType = httpAuthDigest;
		}

	while( isspace(*p) ) {

		p++;
		}

	if( pAuth->m_uType == httpAuthDigest ) {
		
		UINT uLen  = strlen(p);

		PTXT pCopy = new char[ uLen + 1 ];

		strcpy(pCopy, p);

		PTXT pParam     = NULL;

		PTXT pSaveParam = NULL;

		pParam = strtok_r(pCopy, ",", &pSaveParam);

		while( pParam ) {

 			PTXT   pValue   = NULL;

			PTXT   pSaveVal = NULL;

			pValue = strtok_r(pParam, "=", &pSaveVal);

			while( isspace(*pValue) ) {

				pValue++;
				}

			if( !stricmp(pValue, "nonce") ) {

				pValue = strtok_r(NULL, "=", &pSaveVal);

				GetValue(pAuth->m_pNonce, pValue);
				}

			else if( !stricmp(pValue, "realm") ) {

				pValue = strtok_r(NULL, "=", &pSaveVal);

				GetValue(pAuth->m_pRealm, pValue);
				}

			else if( !stricmp(pValue, "qop") ) {

				pValue = strtok_r(NULL, "=", &pSaveVal);

				GetValue(pAuth->m_pQop, pValue);
				}
			else {
				pValue = strtok_r(NULL, "=", &pSaveVal);
				}

			pParam = strtok_r(NULL, ",", &pSaveParam);
			}

		delete [] pCopy;
		}

	return TRUE;
	}

/////////////////////////////////////////////////////////////////////////
//
// HTTP Authentication Wrapper
//

// Constructor

CHttpAuth::CHttpAuth(CWebCamera *pDriver, PCTXT pUser, PCTXT pPass, PCTXT pPath)
{
	m_pDriver     = pDriver;

	m_uType       = httpAuthNone;
	m_uNonceCount = 0;

	m_pRealm      = NULL;
	m_pNonce      = NULL;
	m_pQop        = NULL;

	m_pUser       = pUser;
	m_pPass       = pPass;
	m_pUri        = pPath;

	m_pNC         = new char[9];
	m_pCNonce     = new char[HASH_SIZE];
	m_pResponse   = new char[HASH_SIZE];
	}

// Destructor

CHttpAuth::~CHttpAuth(void)
{
	free(PVOID(m_pRealm));
	free(PVOID(m_pNonce));
	free(PVOID(m_pQop));

	delete [] m_pNC;
	delete [] m_pCNonce;
	delete [] m_pResponse;
	}

// Authorization Helpers

PCTXT CHttpAuth::CalculateResponse(void)
{
	UINT uLen1 = strlen(m_pUser) + strlen(m_pRealm) + strlen(m_pPass) + 3;
	UINT uLen2 = strlen(m_pUri ) + 5;

	PTXT D1    = new char[ uLen1 ];
	PTXT D2    = new char[ uLen2 ];

	UpdateCNonce();

	m_pDriver->Format( D1,
		           "%s:%s:%s",
			   m_pUser,
			   m_pRealm,
			   m_pPass
			   );

	m_pDriver->Format( D2,
			   "%s:%s",
			   "GET",
			   m_pUri
			   );

	char H1[HASH_SIZE] = {0};

	char H2[HASH_SIZE] = {0};

	BYTE bHash[16];

	GetHash(D1, bHash);

	BytesToHex(bHash, 16, H1);

	GetHash(D2, bHash);

	BytesToHex(bHash, 16, H2);

	UINT uLen3 =   strlen(H1)
		     + strlen(H2)
		     + strlen(m_pNonce)
		     + strlen(m_pQop)
		     + HASH_SIZE
		     + 14;

	PTXT D3    = new char[ uLen3 ];

	m_pDriver->Format(D3,
			  "%s:%s:%s:%s:%s:%s",
			  H1,
			  m_pNonce,
			  GetNonceCount(),
			  GetCNonce(),
			  m_pQop,
			  H2
			  );

	GetHash(D3, bHash);

	memset(m_pResponse, 0, HASH_SIZE);

	BytesToHex(bHash, 16, m_pResponse);

	delete [] D1;
	delete [] D2;
	delete [] D3;

	return m_pResponse;
	}

PCTXT CHttpAuth::GetRealm(void)
{
	if( m_pRealm ) {

		return m_pRealm;
		}

	return "";
	}

PCTXT CHttpAuth::GetNonce(void)
{
	if( m_pNonce ) {

		return m_pNonce;
		}

	return "";
	}

PCTXT CHttpAuth::GetNonceCount(void)
{
	if( m_pNC ) {

		return m_pNC;
		}

	return "";
	}

PCTXT CHttpAuth::GetCNonce(void)
{
	if( m_pCNonce ) {

		return m_pCNonce;
		}

	return "";
	}

PCTXT CHttpAuth::GetQop(void)
{
	if( m_pQop ) {

		return m_pQop;
		}

	return "";
	}

BOOL CHttpAuth::GetHash(PCTXT pText, PBYTE pHash)
{
	UINT uLen = strlen(pText);

	md5(PBYTE(pText), uLen, pHash);

	return TRUE;
	}

void CHttpAuth::BytesToHex(PBYTE pSrc, UINT uCount, PTXT pDest)
{
	PCTXT pHex = "0123456789abcdef";

	for( UINT n = 0, c = 0; n < 16; n++ ) {

		pDest[c++] = pHex[pSrc[n]/16];
		
		pDest[c++] = pHex[pSrc[n]%16];
		}
	}

void CHttpAuth::UpdateCNonce(void)
{
	m_uNonceCount++;

	m_pDriver->Format(m_pNC,
			  "%8.8x",
			  m_uNonceCount
			  );


	BYTE bRandom[16];

	m_pDriver->GetRandomBytes(bRandom, 16);

	memset(m_pCNonce, 0, HASH_SIZE);

	BytesToHex(bRandom, 16, m_pCNonce);
	}

// Helpers

BOOL CWebCamera::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CWebCamera::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CWebCamera::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
