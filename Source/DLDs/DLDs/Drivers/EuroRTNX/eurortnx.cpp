
#include "intern.hpp"

#include "eurortnx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm RTNX Driver
//

// Instantiator

INSTANTIATE(CEurothermrtnxDriver);

// Constructor

CEurothermrtnxDriver::CEurothermrtnxDriver(void)
{
	m_Ident	    = 0x400C;
	}

// Destructor

CEurothermrtnxDriver::~CEurothermrtnxDriver(void)
{
	}

// Configuration

void MCALL CEurothermrtnxDriver::Load(LPCBYTE pData)
{
	BuildCRCTable();
	}
	
void MCALL CEurothermrtnxDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CEurothermrtnxDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEurothermrtnxDriver::Open(void)
{
	}

// Device

CCODE MCALL CEurothermrtnxDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_uDestNode = GetWord(pData);

			m_pCtx->m_fIs32 = FALSE; // 16 bit used for Ping

			UINT u = m_pCtx->m_uTimeout;

			if( u < 500 || u > 3000 ) m_pCtx->m_uTimeout = 500;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEurothermrtnxDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEurothermrtnxDriver::Ping(void)
{
	DWORD dData;

	if( !StartSession(&dData) ) {

		return CCODE_ERROR;
		}

	if( !GetFirmwareStatus(&dData) ) {

		return CCODE_ERROR;
		}

	if( !GetHardwareType(&dData) ) {

		return CCODE_ERROR;
		}

	m_pCtx->m_fIs32 = ((dData & 0xFF) > 9);

	return 1;
	}

CCODE MCALL CEurothermrtnxDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == 1 ) {

		*pData = m_pCtx->m_uTimeout;

		return 1;
		}

	UINT uID = (Addr.a.m_Extra << 8) + LOBYTE(Addr.a.m_Offset );

	if( !uID ) {

		return 1;
		}

	if( DoRead( uID, pData) == 1 ) {

		switch( Addr.a.m_Type ) {

			case addrRealAsReal:

				*pData = MakeRTNXIntoFloat( *pData );

				break;

			case addrBitAsBit:
			case addrLongAsLong:

				break;
			}

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEurothermrtnxDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == 1 ) {

		UINT u = *pData;

		if( u >= 500 && u <= 3000 ) {

			m_pCtx->m_uTimeout = u;
			}

		return 1;
		}

	UINT uID = ( (Addr.a.m_Table & 0xF) << 8) + HIBYTE(Addr.a.m_Offset );

	if( !uID ) {

		return 1;
		}

	Dbg( DSTAR, 0 );

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			return DoWrite(uID, (*pData ? 1 : 0), RTNXBIT );

		case addrLongAsLong:

			return DoWrite(uID, *pData, RTNXORDINAL);

		case addrRealAsReal:

			double d;

			DWORD p[2];

			d = (I2R(*pData) * (double( m_pCtx->m_fIs32 ? KF32 : KF16 )));

			memcpy( PBYTE(p), PBYTE(&d), 8);

			return DoWrite(uID, int( I2R( f64To32(p) ) ), RTNXVALUE);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

BOOL CEurothermrtnxDriver::StartSession(PDWORD pData)
{
	m_uPtr = 0;

	AddByte(0);
	AddByte(0);
	AddByte(0);
	AddByte(0xB1);

	m_pCtx->m_fIs32 = FALSE;

	if( Transact(FALSE) ) {

		*pData = GetResponse();

		return TRUE;
		}

	return FALSE;
	}

BOOL CEurothermrtnxDriver::GetHardwareType(PDWORD pData)
{
	m_uPtr = 0;

	AddByte(0);
	AddByte(0);
	AddByte(1);
	AddByte(0x68);

	m_pCtx->m_fIs32 = FALSE;

	if( Transact(FALSE) ) {

		*pData = (GetResponse() >> 8) & 0xFF;

		return TRUE;
		}

	return FALSE;
	}

BOOL CEurothermrtnxDriver::GetFirmwareStatus(PDWORD pData)
{
	m_uPtr = 0;

	AddByte(0);
	AddByte(0);
	AddByte(5);
	AddByte(0x78);

	m_pCtx->m_fIs32 = FALSE;

	if( Transact(FALSE) ) {

		*pData = (GetResponse() >> 8) & 0xFF;

		return TRUE;
		}

	return FALSE;
	}

// Port Access

UINT CEurothermrtnxDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CEurothermrtnxDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {
	
		m_bTx[m_uPtr++] = bData;
		}
	}

void CEurothermrtnxDriver::AddData(DWORD dData, UINT uSize)
{

	AddByte( LOBYTE(LOWORD( dData )) );

	AddByte( HIBYTE(LOWORD( dData )) );

	if( uSize == 4 ) {

		AddByte( LOBYTE(HIWORD( dData )) );

		AddByte( HIBYTE(HIWORD( dData )) );
		}
	}

// Transport Layer

BOOL CEurothermrtnxDriver::Transact(BOOL fIsWrite)
{
	Dbg( DCRLF, 0 );
	Dbg( DTX, m_uPtr );

	m_pData->ClearRx();

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);

	return GetReply(fIsWrite);
	}

BOOL CEurothermrtnxDriver::GetReply(BOOL fIsWrite)
{ 
	UINT uCount = 0;

	UINT uEnd;

	UINT uTimer = 0;

	UINT uData  = 0;

	BYTE bData;

	Dbg(DCRLF,0);

	uEnd = (!fIsWrite && m_pCtx->m_fIs32) ? 6 : 4;

	SetTimer(m_pCtx->m_uTimeout);

	while( (uTimer = GetTimer()) ) {
		
		if( (uData = Get(uTimer)) == NOTHING ) {
			
			continue;
			}

		bData = LOBYTE(uData);

		Dbg( DBY, bData ); 

		if( uCount >= sizeof(m_bRx) ) {

			return FALSE;
			}

		m_bRx[uCount++] = bData;

		if( uCount >= uEnd ) {

			if( fIsWrite ) {

				return TRUE;
				}

			if( m_bTx[0] && !(m_bRx[uEnd-2] & 8) ) {

				return FALSE;
				}

			switch( uEnd ) {

				case 6:
//					return ( CalcCRC( m_bRx, uEnd - 1 ) == m_bRx[uEnd-1] );
					return TRUE;

				case 4:

//					bData = m_bRx[3];

//					m_bRx[3] = 0x7E;

					return TRUE; //( ( calc_RTNX_chk(m_bRx) | 0x7E ) == bData );

				default:
					return FALSE;
				}
			}
		}

	return FALSE;
	}

// Read and Write Handlers

CCODE CEurothermrtnxDriver::DoRead(UINT uID, PDWORD pData)
{
	m_uPtr = 0;

	AddByte( 8 );
	AddByte( 0x10);
	AddByte( 0 );
	AddByte( 0xFE );

	AddData( DWORD(uID), 2 );

	AddByte( CalcCRC(&m_bTx[4], m_uPtr - 4 ) );

	if( Transact(FALSE) ) {

		*pData = GetResponse();

		return 1;
		}
		
	return CCODE_ERROR;
	}

CCODE CEurothermrtnxDriver::DoWrite(UINT uID, DWORD dData, UINT uType)
{
	m_uPtr = 0;

	AddData( DWORD(m_pCtx->m_uDestNode), 2 );

	AddByte( uType );

	m_bTx[3] = 7;

	AddByte( calc_RTNX_chk(m_bTx) | 7 );

	switch( uType ) {

		case RTNXVALUE:
		case RTNXORDINAL:
			AddData( dData, m_pCtx->m_fIs32 ? 4 : 2 );
			break;

		case RTNXBIT:
			AddByte( dData ? 1 : 0 );
			break;
		}

	AddData( DWORD(uID), 2 );

	AddByte( CalcCRC(&m_bTx[4], m_uPtr - 4 ) );

	if( Transact(TRUE) ) {

		return 1;
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

DWORD CEurothermrtnxDriver::GetResponse(void)
{
	DWORD d = 0;

	UINT uCt = m_pCtx->m_fIs32 ? 4 : 2;


	while( uCt ) {

		d <<= 8;

		d += m_bRx[uCt-1];

		uCt--;
		}

	return (m_pCtx->m_fIs32 || !(d & 0x8000) ) ? d : d | 0xFFFF0000;
	}

// Helpers

void CEurothermrtnxDriver::BuildCRCTable(void)
{
	for( UINT i = 0; i < 256; i++ ) {

		UINT u = ( i ^ ( i<<4 )) & 0xFF;

		m_CRCTable[i] = (u<<8) ^ (u<<3) ^ (u>>4);
		}
	}

BYTE CEurothermrtnxDriver::CalcCRC(PBYTE p, UINT uCt)
{
	UINT * t = m_CRCTable;

	UINT uCRC = 0xFFFF;

	for( UINT i = 0; i < uCt; i++ ) {

		uCRC = (uCRC >> 8) ^ ( t[ ( uCRC ^ p[i] ) & 0xFF ] );
		}

	return LOBYTE( uCRC );
	}

BYTE CEurothermrtnxDriver::calc_RTNX_chk( PBYTE p )
{
	BYTE babyte = 5;

	for( UINT i = 0; i < 4; i++ ) {

		BYTE binpb = p[i];

		UINT uwrk = babyte;

		babyte = (babyte<<1) + (binpb & 0xF);

		if( uwrk & 8 ) {

			babyte++;
			}

		babyte &= 0xF;

		if( i <  3 ) {

			binpb >>= 4;

			uwrk = babyte;

			babyte = (babyte<<1) + (binpb & 0xF);

			if( uwrk & 8 ) {

				babyte++;
				}
			}

		babyte &= 0xF;
		}

	return (babyte << 4);
	}

DWORD CEurothermrtnxDriver::MakeRTNXIntoFloat( DWORD dDWord )
{
	double d;

	DWORD p[2];

	BOOL fNeg = FALSE;

	if( dDWord & 0x80000000 ) {

		fNeg = TRUE;

		dDWord = -dDWord;
		}

	d = float( dDWord / double( m_pCtx->m_fIs32 ? KF32 : KF16 ) );

	memcpy( PBYTE(p), PBYTE(&d), 8 );

	if( fNeg ) {

		p[0] |= 0x80000000;
		}

	return f64To32(p);
	}

DWORD CEurothermrtnxDriver::f64To32( PDWORD p )
{
	DWORD dOut;

	DWORD dExp;

	if( p[0] == 0 && p[1] == 0 ) {

		return 0;
		}

	dExp = (p[HIDWORD] >> 20) & 0x7FF; // Raw Exponent

	dOut = p[HIDWORD] & 0x80000000; // Sign Bit

	dOut += (dExp - 896) << 23;

	return dOut | ( (p[HIDWORD] & 0xFFFFF) << 3 ) | ( p[LODWORD] >> 29 );
	}

void CEurothermrtnxDriver::Dbg( UINT u, DWORD d )
{
/*
	UINT i;

	switch( u ) {

		case DCRLF:
			AfxTrace0("\r\n");
			break;

		case DSTAR:
			AfxTrace0("\r\n****\r\n");
			break;

		case DTX:
			for( i = 0; i < d; i++ ) {
				AfxTrace1("[%2.2x]", m_bTx[i]);
				}
			break;

		case DBY:
			AfxTrace1("<%2.2x>", LOBYTE(d) );
			break;

		case DWD:
			AfxTrace1("%4.4x ", LOWORD(d) );
			break;

		case DDW:
			AfxTrace1("%8.8lx ", d );
			break;
		}
*/
	}

// End of File
