

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm RTNX Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

class CEurothermrtnxDriver : public CMasterDriver
{
	public:
		// Constructor
		CEurothermrtnxDriver(void);

		// Destructor
		~CEurothermrtnxDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
	
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			UINT m_uDestNode;
			BOOL m_fIs32;
			UINT m_uTimeout;
		};

		CContext *	m_pCtx;

		UINT		m_uPtr;
		BYTE		m_bTx[12];
		BYTE		m_bRx[8];

		UINT		m_CRCTable[256];

		// Implementation

		BOOL	StartSession(PDWORD pData);
		BOOL	GetHardwareType(PDWORD pData);
		BOOL	GetFirmwareStatus(PDWORD pData);

		// Port Access
		UINT	Get(UINT uTime);

		// Frame Building
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, UINT uSize);

		// Transport Layer
		BOOL	Transact(BOOL fIsWrite);
		BOOL	GetReply(BOOL fIsWrite);

		// Read and Write Handlers
		CCODE	DoRead(UINT uID, PDWORD pData);
		CCODE	DoWrite(UINT uID, DWORD dData, UINT uType);
		DWORD	GetResponse(void);

		// Helpers
		void	BuildCRCTable(void);
		BYTE	CalcCRC(PBYTE p, UINT uCt);
		BYTE	calc_RTNX_chk(PBYTE p);
		DWORD	MakeRTNXIntoFloat(DWORD dDWord);
		DWORD	f64To32(PDWORD p);

		void	Dbg(UINT u, DWORD d);
};

// Parameter Constants
#define RTNXVALUE	0
#define	RTNXORDINAL	1
#define	RTNXBIT		2

// Float Constants
#define	KF32	65536
#define	KF16	327.67
#define	HIDWORD	0
#define	LODWORD	1

// Debug Constants
#define	DCRLF	0
#define	DSTAR	1
#define	DTX	2
#define	DBY	3
#define	DWD	4
#define	DDW	5

// End of File
