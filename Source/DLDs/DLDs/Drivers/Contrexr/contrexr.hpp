
//////////////////////////////////////////////////////////////////////////
//
// Contrex M-Rotary Driver
//

class CContrexRotaryDriver : public CMasterDriver
{
	public:
		// Constructor
		CContrexRotaryDriver(void);

		// Destructor
		~CContrexRotaryDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_bType;
			DWORD	m_dError;
			DWORD	m_dCommandRead;
			};
		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[32];
		BYTE	m_bRx[32];
		UINT	m_uPtr;
		BOOL	m_fPing;

		// Hex Lookup
		LPCTXT	m_pHex;
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, DWORD dFactor);
		void	AddRead(UINT uOffset);
		void	AddWrite(AREF Addr, DWORD dData);
		void	AddRealData( DWORD dData, UINT uOffset );
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	GetResponse(PDWORD pData);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		BOOL	NoTransmit( AREF Addr, PDWORD pData, BOOL fIsWrite );
		DWORD	GetGeneric(PBYTE p, UINT uCt);
		UINT	GetDPFromFormat(PBYTE pNeg);
		BOOL	GetDPRange(UINT uOffset, UINT * uMax, BOOL * pFixed);
		void	GetRealData(PDWORD pData);
		void	PutRealData( char * pS, BOOL fNeg, UINT uOffset );
		DWORD	Round( char * pI, char * pF, char cRound, UINT uFSize );
		BOOL	CheckError(void);

	};

#define	COMMAND	'1'
#define	INQUIRY	'2'
#define	PARSEND	'3'

// End of File
