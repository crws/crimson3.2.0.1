
//////////////////////////////////////////////////////////////////////////
//
// Check Sum
//

class CCheckSum {

	public:
		// Constructor
		CCheckSum(void);

		// Initialise
		void Clear(void);
		void Clear(BYTE bData);

		void AddByte(BYTE bData);
		void Calc(PCBYTE bData, UINT uLen);

		BYTE Get(void);
		
	protected:

		// Data Members
		BYTE bCheck;
	};

// End of File
