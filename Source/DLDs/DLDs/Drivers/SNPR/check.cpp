
#include "intern.hpp"

#include "check.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Check Sum
//

// Constructor

CCheckSum::CCheckSum(void)
{
	Clear();
	}

// Initialise

void CCheckSum::Clear(void)
{
	bCheck = 0;
	}

void CCheckSum::Clear(BYTE bData)
{
	bCheck = bData;
	}

void CCheckSum::AddByte(BYTE bData)
{
	bCheck = bCheck ^ bData;
		
	bCheck = (bCheck << 1) | ((bCheck & 0x80) >> 7);

	}

BYTE CCheckSum::Get(void)
{
	return bCheck;
	}
		
void CCheckSum::Calc(PCBYTE pData, UINT uLen)
{
	for(UINT i = 0; i < uLen; i++) {
		
		AddByte( pData[i] );
		}
	}

// End of File
