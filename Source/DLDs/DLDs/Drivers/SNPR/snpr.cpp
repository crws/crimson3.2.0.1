
#include "intern.hpp"

#include "snpr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SNP Driver
//

// Instantiator

INSTANTIATE(CSNPDriver);

// Constants

static BYTE const Ack[2] = { ACK, NUL };

// Constructor

CSNPDriver::CSNPDriver(void)
{
	m_Ident         = DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_wTxSize	= sizeof(m_bTxBuff);
	
	m_wRxSize	= sizeof(m_bRxBuff); 

	memset(m_sID, 0, sizeof(m_sID));
	}

// Destructor

CSNPDriver::~CSNPDriver(void)
{
	}

// Entry Points

CCODE MCALL CSNPDriver::Ping(void)
{
	if ( !PerformInitLink(m_pCtx->m_Drop) ) {
		
		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

// Configuration

void MCALL CSNPDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CSNPDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}
	
// Management

void MCALL CSNPDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CSNPDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CSNPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_wIdLen = pData[1];

			if ( m_pCtx->m_wIdLen > SNP_ID_LENGTH )

				m_pCtx->m_wIdLen = SNP_ID_LENGTH;
				
			m_pCtx->m_Drop = GetString(pData);

			m_pCtx->m_fBreakFree = GetByte(pData);

			m_pCtx->m_bSlot	     = GetByte(pData);

			m_pCtx->m_wLinkState	= LS_REINIT;

			m_pCtx->m_bSequence	= 0;
	
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}
		

CCODE MCALL CSNPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CSNPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{	
	if( !PerformInitLink(m_pCtx->m_Drop) ) {

		return CCODE_ERROR;
		}

	if ( Addr.a.m_Type == addrBitAsBit ) {
		
		return DoBitRead(Addr, pData, uCount);
		}

	SetAttach(FALSE); 

	UINT uMult = Addr.a.m_Type != addrWordAsWord ? 2 : 1;

	MakeMin(uCount, 32 / uMult);

	if( PutRead(Addr, uCount * uMult) && GetFrame(m_bRxBuff, 40) ) {

		UINT uOffset;
		
		if( uCount * uMult > 3 ) {

			if( !ValidateMail(MB_ACK_TEXT, TRUE) ) {
			
				m_pCtx->m_wLinkState = LS_REINIT;

				return CCODE_ERROR;
				} 
				
			UINT uCount = m_bRxBuff[36];
			
			if( !GetFrame(m_bRxBuff, uCount) ) {

				return CCODE_ERROR;
				}
				
			if( !ValidateData() ) {

				return CCODE_ERROR;
				}
				
			uOffset = 2;
			}
		else {
			if( !ValidateMail(MB_ACK_SING, TRUE) ) {

				m_pCtx->m_wLinkState = LS_REINIT;

				return CCODE_ERROR;
				} 
				
			uOffset = 22;
			}

		if ( Addr.a.m_Type == addrWordAsWord ) {

			GetWordRead(pData, uCount, uOffset);
			}

		else {
			GetLongRead(pData, uCount, uOffset);
			}

		return uCount;

		}

	return CCODE_ERROR;
	}

CCODE MCALL CSNPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{	
	if( Addr.a.m_Table == 'S' ) {

		return uCount;
		}
		
	if( !PerformInitLink(m_pCtx->m_Drop) ) {

		return CCODE_ERROR;
		}

	if ( Addr.a.m_Type == addrBitAsBit ) {
		
		return DoBitWrite(Addr, pData, uCount);
		}

	UINT uMult = Addr.a.m_Type != addrWordAsWord ? 2 : 1;

	MakeMin(uCount, 32 / uMult); 

	if( uCount * uMult >= 4 ) {

		SetAttach(FALSE);

		if( PutWriteHeader(Addr, uCount * uMult) ) {

			if( PutWriteData(Addr, pData, uCount) ) {
				
				if( GetFrame(m_bRxBuff, 40) ) {
				
					if( ValidateMail(MB_ACK_SING, FALSE) ) {

						return uCount;
						}
					
					m_pCtx->m_wLinkState = LS_REINIT;
					}
				}
			}
		}
	else {
		SetAttach(FALSE);

		if( PutWriteSmall(Addr, pData, uCount * uMult) ) {
		
			if( GetFrame(m_bRxBuff, 40) ) {
			
				if( ValidateMail(MB_ACK_SING, TRUE) ) {

					return uCount;
					}
					
				m_pCtx->m_wLinkState = LS_REINIT;
				}
			}
		}
		
	return CCODE_ERROR;

	}

CCODE CSNPDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	SetAttach(FALSE); 

	MakeMin(uCount, 32);
	
	if( PutRead(Addr, uCount) && GetFrame(m_bRxBuff, 40) ) { 
	
		UINT uOffset; 
		
		if( !ValidateMail(MB_ACK_SING, TRUE) ) { 

			m_pCtx->m_wLinkState = LS_REINIT;

			return CCODE_ERROR;
			}   
			
		uOffset = 22;
		
		UINT b = 0;

		BYTE m = 1;

		m <<= ((Addr.a.m_Offset - 1) % 8);

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRxBuff[b + uOffset] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}	

	return CCODE_ERROR;
	}

CCODE CSNPDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	SetAttach(FALSE);

	MakeMin(uCount, 32);

	if( PutWriteBit(Addr, pData, uCount) ) {

		if( GetFrame(m_bRxBuff, 40) ) {

			if( ValidateMail(MB_ACK_SING, TRUE) ) {

				return uCount;  
				}
				
			m_pCtx->m_wLinkState = LS_REINIT;
			}
		}

	return CCODE_ERROR;
	} 

// Application Layer

BOOL CSNPDriver::PerformInitLink(PTXT pDrop)
{
	m_pData->ClearRx();

	m_pData->ClearTx();

	if( m_pCtx->m_wLinkState == LS_OKAY ) {

		if( strcmp(PSTR(m_sID), PSTR(pDrop)) ) {

			m_pCtx->m_wLinkState = LS_ATTACH;
			}
		else {
			return TRUE;
			}		
		}

	memset(m_sID, 0, sizeof(m_sID));

	memcpy(m_sID, m_pCtx->m_Drop, m_pCtx->m_wIdLen);

	if( PerformAttach() ) {
	
		if( m_pCtx->m_wLinkState == LS_ATTACH ) {
		
			m_pCtx->m_wLinkState = LS_OKAY;
			
			return TRUE;
			}
		
		if( PerformSetParams() && PerformSetPrivilege() ) {
			
			m_pCtx->m_wLinkState = LS_OKAY;
				
			return TRUE;
			}
		} 
				
	return FALSE;
	}

BOOL CSNPDriver::PutRead(AREF Addr, UINT uCount)
{
	AddMailBoxHeader(MB_INT_SING, FALSE);

	AddByte(0x04);

	AddRegSpec(Addr, uCount);

	AddRepeatedByte(8, 0x00);

	EndFrame(0, 0);
	
	return PutFrame();
	}

BOOL CSNPDriver::PutWriteSmall(AREF Addr, PDWORD pData, UINT uCount)
{
	AddMailBoxHeader(MB_INT_SING, FALSE);

	AddByte(0x07);
	
	AddRegSpec(Addr, uCount);

	UINT uScan = 0;

	if ( Addr.a.m_Type == addrWordAsWord ) {
	
		for( ; uScan < uCount; uScan++ ) {
		
			AddByte(LOBYTE(pData[uScan]));
	
			AddByte(HIBYTE(pData[uScan]));
			}
		}

	else {
		for( ; uScan < uCount; uScan++ ) {
		
			AddWord(LOWORD(pData[uScan]));
	
			AddWord(HIWORD(pData[uScan]));
			}

		uScan *= 2;
		}
			
	for( ; uScan < 4; uScan++ ) {
				
		AddWord(0);
		}

	EndFrame(0, 0);
	
	return PutFrame();
	}
	
BOOL CSNPDriver::PutWriteBit(AREF Addr, PDWORD pData, UINT uCount)
{
	AddMailBoxHeader(MB_INT_SING, FALSE);

	AddByte(0x07);
	
	AddRegSpec(Addr, uCount); 
	
	UINT b = 0;

	BYTE m = 1;
	
	m <<= ((Addr.a.m_Offset - 1) % 8);

	UINT uBytes = 0;

	for( UINT n = 0; n < uCount; n++ ) {

		if( pData[n] ) {
					
			b |= m;

			}

		if( !(m <<= 1) ) {

			AddByte(b);

			uBytes++;

			b = 0;

			m = 1;
			}
		}

	if( m > 1 ) {

		AddByte(b);

		uBytes++;

		}

	for ( ; uBytes < 8; uBytes++) {

		AddByte(0);
		}

	EndFrame(0, 0);

	return PutFrame();
	}
	
BOOL CSNPDriver::PutWriteHeader(AREF Addr, UINT uCount)
{
	AddMailBoxHeader(MB_INT_TEXT, FALSE);
	
	AddWord(uCount * 2);
	
	AddRepeatedByte(4, 0x00);

	AddRepeatedByte(2, 0x01);

	AddByte(0x07);
	
	AddRegSpec(Addr, uCount);

	EndFrame('T', (uCount * 2) + 8);
	
	return PutFrame();
	}
	
BOOL CSNPDriver::PutWriteData(AREF Addr, PDWORD pData, UINT uCount)
{
	NewFrame('T');

	if ( Addr.a.m_Type == addrWordAsWord ) {

		for( UINT n = 0; n < uCount; n++ ) {
				
			AddWord(LOWORD(pData[n]));
			}
		}

	else {
		for( UINT n = 0; n < uCount; n++ ) {
				
			AddLong(pData[n]);
			}
		}
		
	EndFrame(0, 0);
	
	return PutFrame();
	}
	
void CSNPDriver::AddRegSpec(AREF Addr, UINT uCount)
{
	BOOL fBit = ( Addr.a.m_Type == addrBitAsBit ) ? TRUE : FALSE;
		
	AddByte(GetTypeID(Addr.a.m_Table, fBit));

	AddWord(Addr.a.m_Offset - 1);
		
	AddWord(uCount);
		 
	}

BYTE CSNPDriver::GetTypeID(WORD wType, BOOL fBit)
{
	switch( wType ) {
	
		case 'Q':		return fBit ? 0x48 : 0x12;
		case 'T':		return fBit ? 0x4A : 0x14;
		case 'M':		return fBit ? 0x4C : 0x16;
		case 'B':		return fBit ? 0xFF : 0x0C;
		case 'R':		return fBit ? 0xFF : 0x08;
		case 'S':		return fBit ? 0x54 : 0x1E;
		case 'E':		return fBit ? 0x4E : 0x18;
		case 'D':		return fBit ? 0x50 : 0x1A;
		case 'C':		return fBit ? 0x52 : 0x1C;
		case 'I':		return fBit ? 0x46 : 0x10;
		case 'A':		return fBit ? 0xFF : 0x0A;
		case 'G':		return fBit ? 0x56 : 0x38;
		}
	
	return 0xFF;
	}

void CSNPDriver::GetWordRead(PDWORD pData, UINT uCount, UINT uOffset)
{
	for( UINT n = 0; n < uCount; n++ ) {

		WORD x = PU2(m_bRxBuff + (n * 2) + uOffset)[0];

		pData[n] = LONG(SHORT(IntelToHost(x)));

		}
	}

void CSNPDriver::GetLongRead(PDWORD pData, UINT uCount, UINT uOffset)
{
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = PU4(m_bRxBuff + (n * 4) + uOffset)[0];

		pData[n] = IntelToHost(x);

		}
	}

// Network Layer Code

BOOL CSNPDriver::PerformAttach(void)
{
	SetAttach(TRUE);
	
	if( PutAttach() ) {
		
		if( GetFrame(m_bRxBuff, 24) )  {

			return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL CSNPDriver::PutAttach(void)
{
	if ( !m_pCtx->m_fBreakFree ) {
	
		SendLongBreak();
		} 
	
	NewFrame('A');
	
	AddCurrentID();
	
	AddFourDigitHex(T1_PERIOD);
	
	AddByte('0');
	
	AddRepeatedByte(3, 0x20);
	
	EndFrame(0, 0);	

	return PutFrame(); 
	} 

BOOL CSNPDriver::PerformSetParams(void)
{
	SetAttach(FALSE);
	
	if( PutSetParams() ) {

		if( GetFrame(m_bRxBuff, 40) ) {

			if( ValidateMail(MB_ACK_SING, TRUE) )
				
				return TRUE;
			}
		}
		
	return FALSE;
	}
	
BOOL CSNPDriver::PutSetParams(void)
{
	AddMailBoxHeader(MB_INT_SING, TRUE);

	AddByte(0x00);

	AddByte(0x00);
	
	AddWord(T2_PERIOD);

	AddWord(T3_PERIOD);
	
	AddWord(m_wRxSize - 32);
	
	AddWord(1);
	
	AddRepeatedByte(4, 0x00);
	
	EndFrame(0, 0);
	
	return PutFrame();
	} 

BOOL CSNPDriver::PerformSetPrivilege(void)
{
	SetAttach(FALSE);
	
	if( PutSetPrivilege() )	{

		if( GetFrame(m_bRxBuff, 40) ) {

			if( ValidateMail(MB_ACK_SING, TRUE) )

				return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL CSNPDriver::PutSetPrivilege(void)
{
	AddMailBoxHeader(MB_INT_SING, FALSE);

	AddByte(0x21);

	AddByte(0x02);
	
	AddRepeatedByte(12, 0x00);

	EndFrame(0, 0);
	
	return PutFrame();
	}

void CSNPDriver::AddCurrentID(void)
{
	for( UINT uScan = 0; uScan < sizeof(m_sID); uScan++ ) { 
		
		AddByte(m_sID[uScan]);

		}
	}

void CSNPDriver::AddMailBoxHeader(BYTE bType, BOOL fSNP)
{
	NewFrame('M');
	
	AddRepeatedByte(6, 0x00);
	
	AddByte(m_pCtx->m_bSequence++);
	
	AddByte(bType);
	
	AddMailBoxSource();

	AddMailBoxDest(fSNP);

	if( bType == MB_INT_TEXT || bType == MB_ACK_TEXT )
		AddByte(0x00);
	else
		AddByte(0x01);

	AddByte(0x01);
	}

void CSNPDriver::AddMailBoxSource(void)
{
	AddWord(MA_SRC_TASK | (m_pCtx->m_bSlot << 4));

	AddWord(0x0000);	
	}	

void CSNPDriver::AddMailBoxDest(BOOL fSNP)
{
	if( fSNP )
		AddWord(MA_SNP_TASK);
	else
		AddWord(MA_REQ_TASK | (m_pCtx->m_bSlot << 4));

	AddWord(0x0000);	
	}	

BOOL CSNPDriver::ValidateMail(BYTE bType, BOOL fCheck)
{
	if( m_bRxBuff[1] == 'M' && m_bRxBuff[9] == bType ) {

		if( fCheck ) {

			if( m_bTxBuff[8] != m_bRxBuff[8] ) {

				return FALSE;
				}
						
			if( memcmp(m_bTxBuff + 10, m_bRxBuff + 14, 4) )	{

				return FALSE;
				}
	
			if( memcmp(m_bTxBuff + 14, m_bRxBuff + 10, 4) )	{

				return FALSE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CSNPDriver::ValidateData(void)
{
	return m_bRxBuff[1] == 'T';
	}

// Data Link Layer Code

void CSNPDriver::NewFrame(char cType)
{
	m_uPtr = 0;
	
	m_bTxBuff[m_uPtr++] = ESC;
	
	m_bTxBuff[m_uPtr++] = BYTE(cType);
	}

void CSNPDriver::AddByte(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;
	}
	
void CSNPDriver::AddRepeatedByte(int nCount, BYTE bData)
{
	while( nCount-- )
		m_bTxBuff[m_uPtr++] = bData;
	}

void CSNPDriver::AddFourDigitHex(WORD wData)
{
	m_bTxBuff[m_uPtr++] = m_pHex[(wData / 0x0010) % 16];
	m_bTxBuff[m_uPtr++] = m_pHex[(wData / 0x0001) % 16];
	m_bTxBuff[m_uPtr++] = m_pHex[(wData / 0x1000) % 16];
	m_bTxBuff[m_uPtr++] = m_pHex[(wData / 0x0100) % 16];
	}

void CSNPDriver::AddWord(WORD wData)
{
	m_bTxBuff[m_uPtr++] = LOBYTE(wData);
	m_bTxBuff[m_uPtr++] = HIBYTE(wData);
	}

void CSNPDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));

	}

void CSNPDriver::EndFrame(char cNextType, WORD wNextSize)
{
	m_bTxBuff[m_uPtr++] = ETB;
	
	m_bTxBuff[m_uPtr++] = BYTE(cNextType);
	
	m_bTxBuff[m_uPtr++] = LOBYTE(wNextSize);
	m_bTxBuff[m_uPtr++] = HIBYTE(wNextSize);

	if( !m_pCtx->m_fAttach )
		m_bTxBuff[m_uPtr++] = NUL;
	}

void CSNPDriver::SetAttach(BOOL fAttach)
{
	m_pCtx->m_fAttach = fAttach;
	}

BOOL CSNPDriver::PutFrame()
{
	m_pData->ClearRx();

	m_Check.Clear();

	m_Check.Calc(m_bTxBuff, m_uPtr);
	
	BYTE bCheck = m_Check.Get();

	if( !m_pCtx->m_fAttach ) {

		AddByte( bCheck );
		}
	else {
		AddByte(m_pHex[bCheck / 16]);

		AddByte(m_pHex[bCheck % 16]);
		
		}
	
	Sleep(T1_PERIOD);

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	if( !m_pCtx->m_fAttach && !GetAck() ) {
		
		return FALSE;
		}

	return TRUE;
	}

BOOL CSNPDriver::GetAck(void)
{
	WORD wState = 0;

	SetTimer(T2_PERIOD);
	
	while( GetTimer() ) {

		UINT uByte;
				
		if( (uByte = RxByte(10)) < NOTHING ) {

			switch( wState ) {
		
				case 0:
					if( uByte == NAK ) {

						return FALSE;
						}
						
					if( uByte == ACK )
						wState = 1;
					break;
				
				case 1:	
					return TRUE;
				}
			}
		}
	
	m_pCtx->m_wLinkState = LS_ATTACH;

	return FALSE;
	}

BOOL CSNPDriver::GetFrame(PBYTE pData, WORD wSize)
{
	UINT uState = 0;

	UINT uIndex = 0;

	UINT uTimer = 0;

	UINT uByte = 0;

	SetTimer(T2_PERIOD);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uByte = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == ESC ) {
					
					pData[uIndex++] = uByte;
					
					uState = 1;
					}
				break;
				
			case 1:
				pData[uIndex++] = uByte;
		
				if( uIndex == wSize ) {
					
					if( ValidateFrame(m_bRxBuff, wSize) ) {
					
						if( !m_pCtx->m_fAttach ) {
						
							Sleep(T1_PERIOD);

							m_pData->Write(Ack, sizeof(Ack), FOREVER);
							}
						
						return TRUE;
						}
						
					return FALSE;
					}
				break;
			}
		}

	m_pCtx->m_wLinkState = LS_ATTACH;

	return FALSE;
	}

BOOL CSNPDriver::ValidateFrame(PCBYTE pData, WORD wSize)
{
	if( pData[wSize - 6] == ETB ) {

		m_Check.Clear();

		m_Check.Calc(pData, m_pCtx->m_fAttach ? wSize - 2 : wSize - 1);

		BYTE bCheck = m_Check.Get();

		if( m_pCtx->m_fAttach ) {
		
			if( pData[wSize - 2] != m_pHex[bCheck / 16] )
				return FALSE;

			if( pData[wSize - 1] != m_pHex[bCheck % 16] )
				return FALSE;
				
			return TRUE;
			}
		else {
			if( pData[wSize - 1] != bCheck )
				return FALSE;
				
			return TRUE;
			}
		}
		
	return FALSE;
	}

// Physical Layer

void CSNPDriver::SendLongBreak(void)
{
	m_pData->SetBreak(TRUE);

	Sleep(T0_PERIOD);

	m_pData->SetBreak(FALSE);

	Sleep(T4_PERIOD);
	} 

// Port Access

void CSNPDriver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CSNPDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
