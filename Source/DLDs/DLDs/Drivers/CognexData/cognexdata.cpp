#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "cognexdata.hpp"

// Instantiator

INSTANTIATE(CCognexData)

CCognexData::CCognexData(void)
{
	m_Ident = DRIVER_ID;

	m_pCtx = NULL;

	m_uKeep = 0;

	m_pHead = NULL;

	m_pTail = NULL;
	}

// Destructor
CCognexData::~CCognexData(void)
{
	m_pMutex->Release();
	}

// Configuration
void CCognexData::Load(LPCBYTE pData)
{
	m_pHelper->MoreHelp(IDH_SYNC, (void **)&m_pSync);

	m_pMutex = m_pSync->CreateMutex();
	}

// Management
void CCognexData::Attach(IPortObject *pPort)
{

	}

void CCognexData::Open(void)
{

	}


// Device
CCODE CCognexData::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234) {

			m_pCtx = new CContext;

			m_pCtx->m_uDevice  = GetWord(pData);
			m_pCtx->m_IP	   = GetAddr(pData);
			m_pCtx->m_uPort	   = GetWord(pData);
			m_pCtx->m_fKeep    = GetByte(pData);
			m_pCtx->m_fPing    = GetByte(pData);
			m_pCtx->m_uTime1   = GetWord(pData);
			m_pCtx->m_uTime2   = GetWord(pData);
			m_pCtx->m_uTime3   = GetWord(pData);
			m_pCtx->m_uLast	   = GetTickCount();
			m_pCtx->m_pSock	   = NULL;
			m_pCtx->m_pData    = NULL;
			m_pCtx->m_fSession = FALSE;

			SPrintf(m_pCtx->m_User, GetString(pData));
			SPrintf(m_pCtx->m_Pass, GetString(pData));

			memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE CCognexData::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if ( !m_pCtx->m_fKeep || m_uKeep > 4) {
			
			CloseSocket(FALSE);
			}
		}
	else {
		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		CloseSocket(FALSE);

		delete m_pCtx->m_pData;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CCommsDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CCognexData::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CContext *pCtx = (CContext *) pContext;

	char Cmd[128] = { };

	char Result[4] = { };

	switch( uFunc ) {

		case 1:
			SPrintf(Cmd, "LF%s\r\n", Value);
			break;

		case 2:
			SPrintf(Cmd, "TF%s\r\n", Value);
			break;

		case 3:
			SPrintf(Cmd, "DF%s\r\n", Value);
			break;

		default:
			return 0;
		}

	if( OpenSocket() ) {

		if( Login(pCtx) ) {

			m_pMutex->Wait(FOREVER);

			if( SendFrame(pCtx->m_pSock, Cmd, NULL) ) {
		
				if( RecvFrame(PBYTE(&Result), 3) ) {

					if( Result[0] != '1' ) {

						RecvFrame(PBYTE(DWORD(Result[3])), 1);
						}

					m_pMutex->Free();

					return ATOI(Result);
					}
				}
			}
		}
	
	return 0;
	}


// Entry Points

CCODE CCognexData::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime3) < NOTHING) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE CCognexData::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenSocket() ) {

		if( Login(m_pCtx) ) {

			if( IsReadCommand(Addr) ) {

				return DoReadCommand(Addr, pData, uCount);
				}

			if( Addr.a.m_Extra ) {

				return ReadString(Addr, pData, uCount);
				}

			else {
				switch( Addr.a.m_Type ) {

					case addrWordAsLong:
					case addrWordAsWord:

						return ReadNumeric(Addr, pData, uCount, FALSE);

					case addrWordAsReal:
					case addrLongAsReal:
					case addrRealAsReal:

						return ReadNumeric(Addr, pData, uCount, TRUE);
					}
				}
			}
		}

	EndSession(m_pCtx);

	return CCODE_ERROR;
	}

CCODE CCognexData::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenSocket() ) {

		if( Login(m_pCtx) ) {

			if( IsWriteCommand(Addr) ) {

				return DoWriteCommand(Addr, pData, uCount);
				}

			if( Addr.a.m_Extra ) {

				return WriteString(Addr, pData, uCount);
				}

			else {
				switch( Addr.a.m_Type ) {

					case addrWordAsWord:
					case addrWordAsLong:

						return WriteNumeric(Addr, pData, uCount, FALSE);

					case addrLongAsReal:
					case addrWordAsReal:
					case addrRealAsReal:

						return WriteNumeric(Addr, pData, uCount, TRUE);
					}
				}
			}
		}

	EndSession(m_pCtx);

	return CCODE_ERROR;
	}


// Socket Management
BOOL CCognexData::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCognexData::OpenSocket(void)
{
	if( CheckSocket() ) {
		
		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP = (IPADDR const &) m_pCtx->m_IP;
		
		UINT uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CCognexData::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Implementation

BOOL CCognexData::Login(CContext *pCtx)
{
	if( pCtx->m_fSession ) {

		return TRUE;
		}

	SPrintf(pCtx->m_Cmd, "%s\r\n%s\r\n", pCtx->m_User, pCtx->m_Pass);

	if( SendFrame(pCtx->m_pSock, pCtx->m_Cmd, NULL) ) {

		// Consume the two log-in response lines
		char c = 0;

		while( c != '\n' ) {

			RecvFrame(PBYTE(&c), 1);
			}

		c = 0;

		while( c != '\n') {

			RecvFrame(PBYTE(&c), 1);
			}


		pCtx->m_fSession = TRUE;

		return TRUE;
		}

	return FALSE;
	}

void CCognexData::EndSession(CContext *pCtx)
{
	pCtx->m_fSession = FALSE;

	CloseSocket(FALSE);
	}

UINT CCognexData::ReadNumeric(AREF Addr, PDWORD pData, UINT uCount, BOOL IsReal)
{
	char Result[4]	= { };

	char Buf[128]	= { };

	char Cmd[16]	= { };

	char Col = (char) (Addr.a.m_Offset >> 10) + 'A';

	UINT Row = Addr.a.m_Offset & 0x3FF;

	SPrintf(Cmd, "GV%c%3.3u", Col, Row);

	m_pMutex->Wait(FOREVER);

	if( SendCommand(m_pCtx, Cmd) ) {

		if( RecvFrame(PBYTE(&Result), 3) ) {

			if( Result[0] != '1' ) {

				RecvFrame(PBYTE(DWORD(Result[3])), 1);

				EndSession(m_pCtx);

				m_pMutex->Free();

				return CCODE_ERROR | CCODE_NO_DATA;
				}

			char c = 0;

			for(int i = 0; c != '\n' && i < elements(Buf); i++ ) {

				RecvFrame(PBYTE(&c), 1);

				Buf[i] = c;
				}

			if( IsReal )
				pData[0] = ATOF(Buf);
			else 
				pData[0] = ATOI(Buf);

			m_pMutex->Free();

			return uCount;
			}
		}
	m_pMutex->Free();

	return CCODE_ERROR;
	}

UINT CCognexData::ReadString(AREF Addr, PDWORD pData, UINT uCount)
{
	char Cmd[16]	= { };

	char Str[256]	= { };

	char Result[4]	= { };

	char Col = (char) (Addr.a.m_Offset >> 10) + 'A';

	UINT Row = Addr.a.m_Offset & 0x3FF;

	SPrintf(Cmd, "GV%c%3.3u", Col, Row);

	m_pMutex->Wait(FOREVER);

	if( SendCommand(m_pCtx, Cmd) ) {

		if( RecvFrame(PBYTE(Result), 3) ) {

			if( Result[0] == '1' ) {

				char c = 0;

				int n, i;

				for( i = 0; i < elements(Str) && c != '\n'; i++ ) {

					RecvFrame(PBYTE(&c), 1);

					if( c == '\r' )
						continue;

					Str[i] = c;
					}

				for( i = 0, n = 0; n < uCount; n++, i += 4 ) {
					
					pData[n]  = Str[i] << 24;
					pData[n] |= Str[i + 1] << 16;
					pData[n] |= Str[i + 2] << 8;
					pData[n] |= Str[i + 3];
					}

				m_pMutex->Free();

				return uCount;
				}
			else {
				RecvFrame(PBYTE(DWORD(Result[3])), 1);

				EndSession(m_pCtx);

				m_pMutex->Free();

				return CCODE_ERROR | CCODE_NO_DATA;
				}
			}
		}

	m_pMutex->Free();

	return CCODE_ERROR;
	}

UINT CCognexData::WriteNumeric(AREF Addr, PDWORD pData, UINT uCount, BOOL IsReal)
{
	char Cmd[32]	= { };

	char Result[4]	= { };

	char Col = (char) (Addr.a.m_Offset >> 10) + 'A';

	UINT Row = Addr.a.m_Offset & 0x3FF;
	
	if( IsReal )
		SPrintf(Cmd, "SF%c%3.3u%f", Col, Row, PassFloat(pData[0]));
	else
		SPrintf(Cmd, "SI%c%3.3u%d", Col, Row, pData[0]);

	m_pMutex->Wait(FOREVER);

	if( SendCommand(m_pCtx, Cmd) ) {

		if( RecvFrame(PBYTE(Result), 3) ) {

			if( Result[0] == '1' ) {

				m_pMutex->Free();

				return uCount;
				}
			else {
				RecvFrame(PBYTE(DWORD(Result[3])), 1);

				m_pMutex->Free();

				return CCODE_ERROR | CCODE_HARD;
				}
			}
		}

	m_pMutex->Free();

	return CCODE_ERROR;
	}

UINT CCognexData::WriteString(AREF Addr, PDWORD pData, UINT uCount)
{
	char *pStr = NULL;

	char Result[4] = { };

	char Col = (char) ((Addr.a.m_Offset >> 10) + 'A');

	UINT Row = Addr.a.m_Offset & 0x3FF;

	pStr = new char[7 + (uCount * 4)];

	memset(pStr, 0, 7 + (uCount * 4));

	SPrintf(pStr, "SS%c%3.3u", Col, Row);

	int i, u;

	for( i = 0, u = 6; i < uCount; u += 4, i++ ) {

		pStr[u]	= (PU4(pData)[i] & 0xFF000000) >> 24;
		pStr[u + 1] = (PU4(pData)[i] & 0x00FF0000) >> 16;
		pStr[u + 2] = (PU4(pData)[i] & 0x0000FF00) >> 8;
		pStr[u + 3] = (PU4(pData)[i] & 0x000000FF);	
		}

	m_pMutex->Wait(FOREVER);

	if( SendCommand(m_pCtx, pStr) ) {

		if( RecvFrame(PBYTE(Result), 3) ) {

			if( Result[0] == '1' ) {

				delete[] pStr;

				m_pMutex->Free();

				return uCount;
				}
			else {
				RecvFrame(PBYTE(DWORD(Result[3])), 1);

				delete[] pStr;

				m_pMutex->Free();

				return CCODE_ERROR | CCODE_HARD;
				}
			}
		}

	delete[] pStr;

	m_pMutex->Free();

	return CCODE_ERROR;
	}

BOOL CCognexData::SendCommand(CContext *pCtx, PCTXT Cmd)
{
	char *CmdBuf = NULL;

	if( pCtx->m_fSession ) {

		CmdBuf = new char[512];

		memset(CmdBuf, 0, 512);

		SPrintf(CmdBuf, "%s\r\n", Cmd);

		if( SendFrame(pCtx->m_pSock, CmdBuf, NULL) ) {

			delete[] CmdBuf;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCognexData::RecvFrame(PBYTE pData, UINT uCount)
{
	UINT uTotal = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		if( uTotal == uCount ) {
			
			return TRUE;
			}

		UINT uAvail = uCount - uTotal;

		if( m_pCtx->m_pSock->Recv(pData + uTotal, uAvail) == S_OK ) {

			SetTimer(m_pCtx->m_uTime3);

			uTotal += uAvail;

			}
		else {
			UINT Phase = 0;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {
				
				Sleep(10);
				}
			else {
				break;
				}
			}
		}

	return FALSE;
	}

BOOL CCognexData::SendFrame(ISocket *pSock, PCTXT pText, PTXT pArgs)
{
	UINT uSize = strlen(pText);

	PTXT pWork = new char [ uSize + 256 ];

	SPrintf(pWork, pText, pArgs);

	if( SendFrame(pSock, PCBYTE(pWork), strlen(pWork)) ) {

		delete pWork;

		return TRUE;
		}

	delete pWork;

	return FALSE;
	}

BOOL CCognexData::SendFrame(ISocket *pSock, PCBYTE pText, UINT uSize)
{
	UINT uLimit = 1280;

	while( uSize ) {

		CBuffer *pBuff = CreateBuffer(uLimit, TRUE);

		if( pBuff ) {

			PBYTE pData = pBuff->GetData();

			UINT  uCopy = min(uLimit, uSize);

			memcpy(pData, pText, uCopy);

			pBuff->AddTail(uCopy);  

			SetTimer(m_pCtx->m_uTime1);

			while( pSock->Send(pBuff) == E_FAIL ) {

				UINT Phase;

				pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN && GetTimer() ) {

					Sleep(10);

					continue;
					}

				BuffRelease(pBuff);
				
				return FALSE;
				}

			pText += uCopy;

			uSize -= uCopy;
			}
		else {  
			Sleep(10);

			return FALSE;
			}
		}

	return TRUE;
	}

// Command Handling

UINT CCognexData::DoReadCommand(AREF Addr, PDWORD pData, UINT uCount)
{
	char Cmd[8]	= { };

	char Res[32]    = { };

	char Str[256]	= { };

	switch( Addr.a.m_Table ) {

		case OL:
			SPrintf(Cmd, "GO");
			break;
		case GF:
			SPrintf(Cmd, "GF");
			break;
		}

	m_pMutex->Wait(FOREVER);

	if( SendCommand(m_pCtx, Cmd) ) {

		if( RecvFrame(PBYTE(Res), 3) ) {

			if( Res[0] != '-' ) {

				if ( Addr.a.m_Extra ) {

					char c = 0;

					int n, i;
					
					for( i = 0; c != '\n' && i < sizeof(Str); i++ ) {

						RecvFrame(PBYTE(&c), 1);

						if( c == '\r' )
							continue;

						Str[i] = c;
						}

					for( i = 0, n = 0; n < uCount; n++, i += 4 ) {
					
						pData[n]  = Str[i] << 24;
						pData[n] |= Str[i + 1] << 16;
						pData[n] |= Str[i + 2] << 8;
						pData[n] |= Str[i + 3];
						}

					m_pMutex->Free();
				
					return uCount;
					}
				else {
					pData[0] = ATOI(Res);

					m_pMutex->Free();

					return uCount;
					}
				}
			else {
				RecvFrame(PBYTE(DWORD(Res[3])), 1);

				EndSession(m_pCtx);

				m_pMutex->Free();

				return CCODE_ERROR | CCODE_NO_DATA;
				}
			}
		}

	m_pMutex->Free();

	return CCODE_ERROR;
	}

UINT CCognexData::DoWriteCommand(AREF Addr, PDWORD pData, UINT uCount)
{
	char Cmd[256] = { };

	char Res[4]   = { };

	char Str[128] = { };

	switch( Addr.a.m_Table ) {

		case OL:
			SPrintf(Cmd, "SO%1.1d", pData[0]);
			break;

		case SE:
			SPrintf(Cmd, "SE%1.1d", pData[0]);
			break;
		}

	m_pMutex->Wait(FOREVER);

	if( SendCommand(m_pCtx, Cmd) ) {

		if( RecvFrame(PBYTE(Res), 3) ) {

			if( Res[0] == '1' ) {

				m_pMutex->Free();

				return uCount;
				}
			else {
				RecvFrame(PBYTE(DWORD(Res[3])), 1);

				m_pMutex->Free();

				return CCODE_ERROR | CCODE_HARD;
				}
			}
		}

	m_pMutex->Free();

	return CCODE_ERROR;
	}

// Address type helpers

BOOL CCognexData::IsWriteCommand(AREF Addr)
{
	switch( Addr.a.m_Table ) {

		case OL:
		case SE:		
			return TRUE;
		}

	return FALSE;
	}

BOOL CCognexData::IsReadCommand(AREF Addr)
{
	switch( Addr.a.m_Table ) {

		case OL:
		case GF:
			return TRUE;
		}

	return FALSE;
	}
