
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCognexData;

// Commands
#define	GF	128 // Get File
#define	SE	241 // Set Event (Trigger Event)
#define OL	242 // Online Status



//////////////////////////////////////////////////////////////////////////
//
// Cognex Data Driver
//

class CCognexData : public CMasterDriver
{
	public:

		// Constructor
		CCognexData(void);

		// Destructor
		~CCognexData(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)	DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device data
		struct CContext 
		{
			CContext * m_pNext;
			CContext * m_pPrev;
			UINT	   m_uDevice;
			DWORD	   m_IP;
			UINT	   m_uPort;
			BOOL	   m_fKeep;
			BOOL	   m_fPing;
			UINT	   m_uTime1;
			UINT	   m_uTime2;
			UINT	   m_uTime3;
			UINT	   m_uLast;
			PBYTE	   m_pData;
			UINT	   m_uInfo[4];
			ISocket  * m_pSock;
			BOOL	   m_fSession;
			char	   m_User[16];
			char	   m_Pass[16];
			char	   m_Cmd[32];
			};

		// Data Members
		CContext	* m_pCtx;
		CContext	* m_pHead;
		CContext	* m_pTail;
		UINT		m_uType;
		UINT		m_uKeep;
		IMutex		*m_pMutex;
		ISyncHelper	*m_pSync;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Implementation
		BOOL Login(CContext *pCtx);
		void EndSession(CContext *pCtx);

		// Reading
		UINT ReadNumeric(AREF Addr, PDWORD pData, UINT uCount, BOOL IsReal);
		UINT ReadString(AREF Addr, PDWORD pData, UINT uCount);

		// Writing
		UINT WriteReal(AREF Addr, PDWORD pData, UINT uCount);
		UINT WriteNumeric(AREF Addr, PDWORD pData, UINT uCount, BOOL IsReal);
		UINT WriteString(AREF Addr, PDWORD pData, UINT uCount);

		BOOL SendCommand(CContext *pCtx, PCTXT Cmd);
		BOOL RecvFrame(PBYTE pData, UINT uCount);
		BOOL SendFrame(ISocket *pSock, PCTXT pText, PTXT pArgs);
		BOOL SendFrame(ISocket *pSock, PCBYTE pText, UINT uSize);

		// Command Handling
		UINT DoWriteCommand(AREF Addr, PDWORD pData, UINT uCount);
		UINT DoReadCommand(AREF Addr, PDWORD pData, UINT uCount);

		// Address type helpers
		BOOL IsWriteCommand(AREF Addr);
		BOOL IsReadCommand(AREF Addr);
	};
