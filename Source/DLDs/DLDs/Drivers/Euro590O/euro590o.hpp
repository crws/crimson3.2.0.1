
//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 590 Data Spaces
//

#define	TAG_BOOL	1
#define	TAG_INT		2
#define	TAG_REAL	3
#define	PARAM_BOOL	4
#define	PARAM_WORD	5
#define	PARAM_REAL	6

// Tags require indirection
// Write Tag # in Register TAG_PTR_PNO.  Register TAG_DATA holds the data
#define	TAG_PTR_PNO	111
#define	TAG_DATA	127

#define	ISHEX		TRUE
#define	NOTHEX		FALSE
#define	ISWRITE		TRUE
#define	NOTWRITE	FALSE

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Old 590 Driver
//

class CEurothermOld590Driver : public CMasterDriver
{
	public:
		// Constructor
		CEurothermOld590Driver(void);

		// Destructor
		~CEurothermOld590Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE m_GID;
			BYTE m_UID;
			};

		CContext * m_pCtx;

		LPCTXT	m_pHex;
		UINT	m_uPtr;
		BYTE	m_bTx[32];
		BYTE	m_bRx[32];
		
		// Implementation

		// Read Handlers
		CCODE	DoBitRead(UINT Addr, PDWORD pData);
		CCODE	DoWordRead(UINT Addr, UINT uTable, PDWORD pData);
		CCODE	DoRealRead(UINT Addr, PDWORD pData);

		// Write Handlers
		CCODE	DoBitWrite(UINT Addr, UINT uTable, DWORD dData);
		CCODE	DoWordWrite(UINT Addr, UINT uTable, DWORD dData);
		CCODE	DoRealWrite(UINT Addr, DWORD dData);
		
		// Frame Building

		void	StartFrame(void);
		void	EndFrame(BOOL fIsWrite);
		void	AddByte(BYTE bData);
		void	AddValue( char * pV );
		void	PutInteger(DWORD dData, BOOL fNotHex);
		void	PutIdentifier(UINT uOffset, BOOL fIsWrite);
		
		// Transport Layer
		BOOL	Transact(BOOL fIsWrite);
		BOOL	Send(void);
		BOOL	GetReply(BOOL fIsWrite);

		// Port Access
		UINT	RxByte(UINT uTime);

		// Helpers
		BOOL	IsADigit( BYTE b, BOOL fIsHex );
		CCODE	SetCCError(BOOL fIsWrite);
	};

// End of File
