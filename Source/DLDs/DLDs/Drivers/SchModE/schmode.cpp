#include "intern.hpp"

#include "schmode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Schneider PLC via Modbus TCP/IP Driver
//

// Instantiator

INSTANTIATE(CSchModEDriver);

// Constructor

CSchModEDriver::CSchModEDriver(void)
{
	m_Ident     = DRIVER_ID;
	}

// Device

CCODE MCALL CSchModEDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pSCtx = (CSchntext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pSCtx = new CSchntext;

			m_pCtx = m_pSCtx;

			m_pSCtx->m_IP1		= GetAddr(pData);
			m_pSCtx->m_uPort	= GetWord(pData);
			m_pSCtx->m_bUnit	= GetByte(pData);
			m_pSCtx->m_fKeep	= GetByte(pData);
			m_pSCtx->m_fPing	= GetByte(pData);
			m_pSCtx->m_uTime1	= GetWord(pData);
			m_pSCtx->m_uTime2	= GetWord(pData);
			m_pSCtx->m_uTime3	= GetWord(pData);
			m_pSCtx->m_fDisable15	= GetByte(pData);
			m_pSCtx->m_fDisable16	= GetByte(pData);
			m_pSCtx->m_fDisable5	= GetByte(pData);
			m_pSCtx->m_fDisable6	= GetByte(pData);
			m_pSCtx->m_PingReg	= GetWord(pData);
			m_pSCtx->m_wTrans	= 0;
			m_pSCtx->m_pSock	= NULL;
			m_pSCtx->m_uLast	= GetTickCount();
			m_pSCtx->m_uMax01	= GetWord(pData);
			m_pSCtx->m_uMax02	= GetWord(pData);
			m_pSCtx->m_uMax03	= GetWord(pData);
			m_pSCtx->m_uMax04	= GetWord(pData);
			m_pSCtx->m_uMax15	= GetWord(pData);
			m_pSCtx->m_uMax16	= GetWord(pData);
			m_pSCtx->m_bByteR	= GetByte(pData);
			m_pSCtx->m_bByteL	= GetByte(pData);
			m_pSCtx->m_fDirty	= FALSE;
			m_pSCtx->m_fAux         = FALSE;

			#if DRIVER_ID == 0x3505

			m_pSCtx->m_IP2		= GetAddr(pData);
			m_pSCtx->m_fNoReadEx	= GetByte(pData);
			m_pSCtx->m_fFlipLong	= GetByte(pData);
			m_pSCtx->m_fFlipReal	= GetByte(pData);

			#else

			m_pSCtx->m_IP2		= 0;
			m_pSCtx->m_fNoReadEx	= FALSE;
			m_pSCtx->m_fFlipLong	= FALSE;
			m_pSCtx->m_fFlipReal	= FALSE;

			#endif

			Limit(m_pSCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pSCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pSCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pSCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pSCtx->m_uMax15, 1, m_pSCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pSCtx->m_uMax16, 1, m_pSCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pSCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx = m_pSCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CSchModEDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pSCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pSCtx = NULL;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CSchModEDriver::Ping(void)
{
	return CModbusTCPMaster::Ping();
	}

CCODE MCALL CSchModEDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress Address;

	Address.a.m_Table  = Addr.a.m_Table;
		
	Address.a.m_Offset = Addr.a.m_Offset + 1;
		
	Address.a.m_Type   = Addr.a.m_Type;

	Address.a.m_Extra  = Addr.a.m_Extra;
	
	return ReadSchneider(Address, pData, uCount);
	}

CCODE MCALL CSchModEDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress Address;

	Address.a.m_Table  = Addr.a.m_Table;
		
	Address.a.m_Offset = Addr.a.m_Offset + 1;
		
	Address.a.m_Type   = Addr.a.m_Type;

	Address.a.m_Extra  = Addr.a.m_Extra;
	
	return WriteSchneider(Address, pData, uCount);
	}

CCODE CSchModEDriver::ReadSchneider(AREF Addr, PDWORD pData, UINT uCount)
{
	// TODO -- Should we sleep here to yield processor?

	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	UINT uType = Addr.a.m_Type;

	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
			
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG32:
			
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));
			
		case SPACE_HOLDING:

			if( uType == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax03));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG:

			if( uType == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax04));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));

		case SPACE_OUTPUT:

			return DoBitRead(Addr, pData, min(uCount, m_pCtx->m_uMax01));

		case SPACE_INPUT:
			
			return DoBitRead(Addr, pData, min(uCount, m_pCtx->m_uMax02));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CSchModEDriver::WriteSchneider(AREF Addr, PDWORD pData, UINT uCount)
{
	// TODO -- Should we sleep here to yield processor?

	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	switch( Addr.a.m_Table ) {

		case SPACE_HOLD32:
			
			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_HOLDING:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(Addr, pData, min(uCount, m_pCtx->m_fDisable16 ? UINT(1) : m_pCtx->m_uMax16));
				}

			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_OUTPUT:
			
			return DoBitWrite(Addr, pData, min(uCount, m_pCtx->m_fDisable15 ? UINT(1) : m_pCtx->m_uMax15));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CSchModEDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Read Long %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount); */
	
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLDING:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(WORD(Addr.a.m_Offset - 1));
	
	AddWord(WORD(Addr.a.m_Table == SPACE_HOLD32 ? uCount : uCount * 2));
	
	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_bRxBuff + 3)[n];
			
			pData[n] = OrderBytes(MotorToHost(x), Addr.a.m_Type);
			}

		return uCount;
		}

	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CSchModEDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Write Long %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount); */ 
	
	UINT uTable = Addr.a.m_Table;
	
	if( uTable == SPACE_HOLD32 || uTable == SPACE_HOLDING ) {

		StartFrame(16);
		
		AddWord(WORD(Addr.a.m_Offset - 1));
		
		AddWord(WORD(uTable == SPACE_HOLD32 ? uCount : uCount * 2));
		
		AddByte(BYTE(uCount * 4));

		for( UINT n = 0; n < uCount; n++ ) {

			DWORD x = OrderBytes(pData[n], Addr.a.m_Type);

			AddLong(x);
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		if( IgnoreException() ) {

			memset(pData, 0, uCount * sizeof(DWORD));

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

DWORD CSchModEDriver::OrderBytes(DWORD dwValue, UINT uType)
{
	UINT uOrder = ((uType == addrWordAsReal) ? m_pSCtx->m_bByteR : m_pSCtx->m_bByteL);

	switch( uOrder ) {

		case 0:
			return SwapWords(dwValue);

		case 1:
			return SwapBytes(SwapWords(dwValue));

		case 3:
			return SwapBytes(dwValue);
		}

	return dwValue;
	}

DWORD CSchModEDriver::SwapWords(DWORD dwWord)
{
	return MAKELONG(HIWORD(dwWord), LOWORD(dwWord));
	}

DWORD CSchModEDriver::SwapBytes(DWORD dwWord)
{
	WORD wHi = HIWORD(dwWord);

	WORD wLo = LOWORD(dwWord);

	wHi = MAKEWORD(HIBYTE(wHi), LOBYTE(wHi));

	wLo = MAKEWORD(HIBYTE(wLo), LOBYTE(wLo));

	return MAKELONG(wLo, wHi);
	}

// End of File
