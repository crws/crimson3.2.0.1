//////////////////////////////////////////////////////////////////////////
//
// TSX 57 Data Spaces
//

#define	SPACE_HOLDING	0x01 // %MW
#define	SPACE_ANALOG	0x02 // %IW
#define	SPACE_OUTPUT	0x03 // %M
#define	SPACE_INPUT	0x04 // %I
#define	SPACE_HOLD32	0x05 // not used
#define	SPACE_ANALOG32	0x06 // not used
#define	SPACE_TSXLONG	0x07
#define	SPACE_TSXREAL	0x08

//////////////////////////////////////////////////////////////////////////
//
// TSX 57 TCP/IP Master Driver
//

class CTSX57TCPMaster : public CMasterDriver
{
	public:
		// Constructor
		CTSX57TCPMaster(void);

		// Destructor
		~CTSX57TCPMaster(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
				
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BYTE	 m_bUnit;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL	 m_fDisable15;
			BOOL	 m_fDisable16;
			BOOL	 m_fDisable5;
			BOOL	 m_fDisable6;
			UINT	 m_PingReg;
			UINT	 m_uMax01;
			UINT	 m_uMax02;
			UINT	 m_uMax03;
			UINT	 m_uMax04;
			UINT	 m_uMax15;
			UINT	 m_uMax16;
			BOOL     m_fDirty;

			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		UINT	   m_uKeep;
		UINT	   m_uMaxWords;
		UINT	   m_uMaxBits;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
				
		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL Transact(BOOL fIgnore);
		BOOL CheckFrame(void);

		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
		void Limit(UINT &uData, UINT uMin, UINT uMax);
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);

	};

// End of File
