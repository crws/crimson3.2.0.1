
#include "intern.hpp"

#include "tsx57tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver
//

// Instantiator

INSTANTIATE(CTSX57TCPMaster);

// Constructor

CTSX57TCPMaster::CTSX57TCPMaster(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;

	m_uMaxWords = 120;
	
	m_uMaxBits  = 2000;
	}

// Destructor

CTSX57TCPMaster::~CTSX57TCPMaster(void)
{
	}

// Configuration

void MCALL CTSX57TCPMaster::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CTSX57TCPMaster::Attach(IPortObject *pPort)
{
	}

void MCALL CTSX57TCPMaster::Open(void)
{
	}

// Device

CCODE MCALL CTSX57TCPMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_fDisable15	= GetByte(pData);
			m_pCtx->m_fDisable16	= GetByte(pData);
			m_pCtx->m_fDisable5	= GetByte(pData);
			m_pCtx->m_fDisable6	= GetByte(pData);
			m_pCtx->m_PingReg	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->m_uMax01	= GetWord(pData);
			m_pCtx->m_uMax02	= GetWord(pData);
			m_pCtx->m_uMax03	= GetWord(pData);
			m_pCtx->m_uMax04	= GetWord(pData);
			m_pCtx->m_uMax15	= GetWord(pData);
			m_pCtx->m_uMax16	= GetWord(pData);
			m_pCtx->m_fDirty	= FALSE;

			Limit(m_pCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pCtx->m_uMax15, 1, m_pCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pCtx->m_uMax16, 1, m_pCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CTSX57TCPMaster::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CTSX57TCPMaster::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) { // Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		pCtx->m_IP = MotorToHost(dwValue);

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 2 )  {  // Set target port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 3 )  {  // Drop Number

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bUnit = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	
	return 0;
	}

// Entry Points

CCODE MCALL CTSX57TCPMaster::Ping(void)
{
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}
						
		if( m_pCtx->m_PingReg == 0 || m_pCtx->m_bUnit == 255 ) {

			return CCODE_SUCCESS;
			}
	

		DWORD    Data[1];
	
		CAddress Addr;

		Addr.a.m_Table  = SPACE_HOLDING;
		
		Addr.a.m_Offset = m_pCtx->m_PingReg;
		
		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;
	
		return DoWordRead(Addr, Data, 1);
		}

	return CCODE_ERROR; 
	}

CCODE MCALL CTSX57TCPMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	// TODO -- Should we sleep here to yield processor?

	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	UINT uType = Addr.a.m_Type;

	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
			
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG32:
			
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));
			
		case SPACE_HOLDING:
		case SPACE_TSXLONG:
		case SPACE_TSXREAL:

			if( uType == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax03));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG:

			if( uType == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax04));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));

		case SPACE_OUTPUT:

			return DoBitRead(Addr, pData, min(uCount, m_pCtx->m_uMax01));

		case SPACE_INPUT:
			
			return DoBitRead(Addr, pData, min(uCount, m_pCtx->m_uMax02));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CTSX57TCPMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace0("\r\n\n**** WRITE ****");

	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	switch( Addr.a.m_Table ) {

		case SPACE_HOLD32:
			
			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_HOLDING:
		case SPACE_TSXLONG:
		case SPACE_TSXREAL:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(Addr, pData, min(uCount, m_pCtx->m_fDisable16 ? UINT(1) : m_pCtx->m_uMax16));
				}

			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_OUTPUT:
			
			return DoBitWrite(Addr, pData, min(uCount, m_pCtx->m_fDisable15 ? UINT(1) : m_pCtx->m_uMax15));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Socket Management

BOOL CTSX57TCPMaster::CheckSocket(void)
{
	if( !m_pCtx->m_fDirty && m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CTSX57TCPMaster::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			if( m_pCtx->m_fDirty ) {

				SetTimer(0);

				m_pCtx->m_fDirty = FALSE;
				}
			else {
				SetTimer(m_pCtx->m_uTime1);
				}

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CTSX57TCPMaster::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

void CTSX57TCPMaster::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);
	}

void CTSX57TCPMaster::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {
	
		m_bTxBuff[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CTSX57TCPMaster::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CTSX57TCPMaster::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CTSX57TCPMaster::SendFrame(void)
{
	m_bTxBuff[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

//**/		AfxTrace0("\r\n"); for (UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTxBuff[k]);

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTSX57TCPMaster::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

//**/			AfxTrace0("\r\n"); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("<%2.2x>", m_bRxBuff[k]);

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRxBuff[5];

				if( uPtr >= uTotal ) {

					if( m_bRxBuff[0] == m_bTxBuff[0] ) {

						if( m_bRxBuff[1] == m_bTxBuff[1] ) {

							memcpy(m_bRxBuff, m_bRxBuff + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRxBuff[n] = m_bRxBuff[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CTSX57TCPMaster::Transact(BOOL fIgnore)
{
	if( SendFrame() && RecvFrame() ) {

		if( fIgnore ) {

			return TRUE;
			}

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CTSX57TCPMaster::CheckFrame(void)
{
	if( !(m_bRxBuff[1] & 0x80) ) {
	
		return TRUE;
		}

	return FALSE;
	}

// Read Handlers

CCODE CTSX57TCPMaster::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nRead Word %u %u %u", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLDING:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(WORD(Addr.a.m_Offset));
	
	AddWord(WORD(uCount));
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x   = PU2(m_bRxBuff + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CTSX57TCPMaster::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nRead Long %u %u %u", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLDING:
		case SPACE_TSXLONG:
		case SPACE_TSXREAL:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(WORD(Addr.a.m_Offset));
	
	AddWord(WORD(Addr.a.m_Table == SPACE_HOLD32 ? uCount : uCount * 2));
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_bRxBuff + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CTSX57TCPMaster::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nRead Bit %u %u %u", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	switch( Addr.a.m_Table ) {
			
		case SPACE_OUTPUT:
			StartFrame(0x01);
			break;
			
		case SPACE_INPUT:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(WORD(Addr.a.m_Offset));

	AddWord(WORD(uCount));

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRxBuff[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CTSX57TCPMaster::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nWrite Word %u %u %u", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	if( Addr.a.m_Table == SPACE_HOLDING ) {

		if( ( uCount == 1 ) && !m_pCtx->m_fDisable6 ) {
			
			StartFrame(6);

			AddWord(WORD(Addr.a.m_Offset));
			
			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);

			AddWord(WORD(Addr.a.m_Offset));
			
			AddWord(WORD(uCount));
			
			AddByte(BYTE(uCount * 2));
			
			for( UINT n = 0; n < uCount; n++ ) {
				
				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CTSX57TCPMaster::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nWrite Long %u %u %u", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	UINT uTable = Addr.a.m_Table;
	
	if(	uTable == SPACE_HOLD32  ||
		uTable == SPACE_HOLDING ||
		uTable == SPACE_TSXLONG ||
		uTable == SPACE_TSXREAL
		) {

		StartFrame(16);
		
		AddWord(WORD(Addr.a.m_Offset));
		
		AddWord(WORD(uTable == SPACE_HOLD32 ? uCount : uCount * 2));
		
		AddByte(BYTE(uCount * 4));
		
		for( UINT n = 0; n < uCount; n++ ) {
			
			AddLong(pData[n]);
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CTSX57TCPMaster::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nWrite Bit %u %u %u", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	if( Addr.a.m_Table == SPACE_OUTPUT ) {

		if( (uCount == 1) && !m_pCtx->m_fDisable5 ) {

			StartFrame(5);

			AddWord(WORD(Addr.a.m_Offset));
			
			AddWord(pData[0] ? WORD(0xFF00) : WORD(0x0000));
			}
		else {
			StartFrame(15);

			AddWord(WORD(Addr.a.m_Offset));

			AddWord(WORD(uCount));

			AddByte(BYTE((uCount + 7) / 8));

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(BYTE(b));

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(BYTE(b));
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Helpers

void CTSX57TCPMaster::Limit(UINT &uData, UINT uMin, UINT uMax)
{
	if( uData < uMin ) uData = uMin;
	
	if( uData > uMax ) uData = uMax;
	}

BOOL CTSX57TCPMaster::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CTSX57TCPMaster::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CTSX57TCPMaster::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
