
#ifndef INCLUDE_IOI_SEGMENT

#define INCLUDE_IOI_SEGMENT

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// IOI Segment
//

class CIOISegment
{
	public:
		// Constructor
		CIOISegment(void);
		CIOISegment(PCTXT pText);

		// Operations
		void Append(UINT uIndex);
		void Append(PCTXT pText);
		void Encode(PCTXT pText);
		void Encode(PCTXT pText, UINT uIndex);
		void Encode(PCTXT pText, UINT uIndex, PCTXT pMore);
		void Encode(PCTXT pText, PCTXT pMore);

		// Attributes
		PBYTE	GetData(void);
		UINT	GetSize(void);

	protected:
		// Data
		BYTE	m_Data[64];
		PBYTE	m_pData;
		UINT	m_uSize;

		// Implementation
		void Init(void);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddText(PCTXT pText);
		void AddIOI (CIOISegment &Name);
	};

#endif

// End of File
