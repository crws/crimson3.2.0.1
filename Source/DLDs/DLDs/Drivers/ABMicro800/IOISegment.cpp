
#include "IOISegment.hpp"

//////////////////////////////////////////////////////////////////////////
//
// IOI Segment
//

// Constructors

CIOISegment::CIOISegment(void)
{
	}

CIOISegment::CIOISegment(PCTXT pText)
{
	Encode(pText); 
	}

// Operations

void CIOISegment::Append(PCTXT pText)
{
	CIOISegment More;

	More.Encode(pText);

	AddIOI(More);
	}

void CIOISegment::Append(UINT uIndex)
{
	if( uIndex < 0xFF ) {

		AddByte(0x28);
		
		AddByte(uIndex);
		}
	else {
		AddWord(0x29);

		AddWord(uIndex);
		}
	}

void CIOISegment::Encode(PCTXT pText)
{
	Init();
	
	AddText(pText);
	}

void CIOISegment::Encode(PCTXT pText, UINT uIndex)
{
	Encode(pText);

	Append(uIndex);
	}

void CIOISegment::Encode(PCTXT pText, UINT uIndex, PCTXT pMore)
{
	Encode(pText, uIndex);

	CIOISegment More;

	More.Encode(pMore);

	AddIOI(More);
	}

void CIOISegment::Encode(PCTXT pText, PCTXT pMore)
{
	Encode(pText);

	CIOISegment More;

	More.Encode(pMore);

	AddIOI(More);
	}

// Attributes

PBYTE CIOISegment::GetData(void)
{
	return m_Data;
	}

UINT CIOISegment::GetSize(void)
{
	return m_uSize;
	}

// Implementation

void CIOISegment::Init(void)
{
	m_pData = m_Data;

	m_uSize = 0;
	}

void CIOISegment::AddByte(BYTE bData)
{
	*m_pData = bData;

	m_pData++;

	m_uSize++;
	}

void CIOISegment::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CIOISegment::AddText(PCTXT pText)
{
	UINT uSize = strlen(pText);

	AddByte(0x91);

	AddByte(uSize);

	memcpy(m_pData, pText, uSize);

	m_pData += uSize;

	m_uSize += uSize;

	if( uSize % 2 ) {

		AddByte(0);
		}
	}

void CIOISegment::AddIOI(CIOISegment &Name)
{
	UINT  uSize = Name.GetSize();

	PBYTE pData = Name.GetData();

	memcpy(m_pData, pData, uSize);

	m_pData += uSize;

	m_uSize += uSize;
	}

// End of File
