

#include "intern.hpp"

#include "yasactcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa AC Drives TCP Driver
//

// Instantiator
		
INSTANTIATE(CYaskawaACTCPDriver);

// Constructor

CYaskawaACTCPDriver::CYaskawaACTCPDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CYaskawaACTCPDriver::~CYaskawaACTCPDriver(void)
{
	}

// Configuration

void MCALL CYaskawaACTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CYaskawaACTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CYaskawaACTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CYaskawaACTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_uUnit  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();

			m_pCtx->m_EC	= 0;
			m_pCtx->m_EV	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskawaACTCPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskawaACTCPDriver::Ping(void)
{
	if( OpenSocket() ) {

		DWORD    Data[1];
	
		CAddress Addr;

		Addr.a.m_Table  = SPA;
		Addr.a.m_Offset = 0x101;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;
	
		return DoWordRead(Addr, Data, 1);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CYaskawaACTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	if( !m_pCtx->m_uUnit ) return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;

	UINT uTable = Addr.a.m_Table;

	if( IsEnterOrAccept(uTable) ) { // Write only

		pData[0] = 0;

		return 1;
		}

	if( IsErrCommandOrValue(uTable) ) {

		*pData = uTable == SPEC ? m_pCtx->m_EC : m_pCtx->m_EV;

		return 1;
		}

	if( IsYACParam(uTable) && Addr.a.m_Offset == 0 ) {

		return 1;	// list not defined
		}

//**/	Sleep(100);	// slow comms for debug
//**/	AfxTrace3("\r\nT=%d O=%d T=%d ", Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Type); AfxTrace1("C=%d ", uCount);

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	return DoBitRead(Addr, pData, min(uCount, 8));

		case addrWordAsWord:	return DoWordRead(Addr, pData, min(uCount, 8));

		case addrWordAsReal:
		case addrWordAsLong:	return DoLongRead(Addr, pData, min(uCount, 4));
		}

	return uCount; // unspecified parameters
	}

CCODE MCALL CYaskawaACTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	if( IsErrCommandOrValue(Addr.a.m_Table) ) {

		m_pCtx->m_EC = 0;
		m_pCtx->m_EV = 0;

		return 1;
		}

	if( IsEnterOrAccept(Addr.a.m_Table) ) {

		DWORD Data = 0;

		return DoWordWrite(Addr, &Data, 1);
		}

//**/	AfxTrace3("\r\n\n****** WRITE ******\r\nT=%d O=%d D=%8.8lx ", Addr.a.m_Table, Addr.a.m_Offset, *pData);

	UINT u = CheckCommParams(Addr, uCount);

	if( !u ) {	// Address is a comms parameter, don't write

		return 1;
		}

	uCount = u;	// uCount will be reduced if request includes a comms parameter

	BOOL f = IsYACParam(Addr.a.m_Table);

	if( f && Addr.a.m_Offset == 0 ) {

		return 1;	// list not defined
		}

	switch( Addr.a.m_Type) {

		case addrBitAsBit:

			if( Addr.a.m_Table == SPDA0 ) {
				
				return DoBitWrite(Addr, pData, min(uCount, 32));
				}
			break;
// Since parameters are not necessarily consecutive, restrict writes to one for YAC parameters.
		case addrWordAsWord:

			if( f || Addr.a.m_Table == SPDA4 ) {

				return DoWordWrite(Addr, pData, f ? 1 : min(uCount, 16));
				}
			break;

		case addrRealAsReal:
		case addrLongAsLong:

			if( f || Addr.a.m_Table == SPDA4 ) {

				return DoLongWrite(Addr, pData, f ? 1 : min(uCount, 8));
				}
			break;
		}

	return uCount; // unspecified parameters
	}

// Implementation

// Frame Building

void CYaskawaACTCPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(LOBYTE(m_pCtx->m_uUnit));
	
	AddByte(bOpcode);
	}

void CYaskawaACTCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) m_bTx[m_uPtr++] = bData;
	}

void CYaskawaACTCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CYaskawaACTCPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CYaskawaACTCPDriver::PutFrame(void)
{
	m_bTx[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CYaskawaACTCPDriver::GetFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

//**/			AfxTrace0("\r\n"); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("<%2.2x>", m_bRx[k]);

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRx[5];

				if( uPtr >= uTotal ) {

					if( m_bRx[0] == m_bTx[0] ) {

						if( m_bRx[1] == m_bTx[1] ) {

							memcpy(m_bRx, m_bRx + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRx[n] = m_bRx[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}	

	return FALSE;
	}

BOOL CYaskawaACTCPDriver::Transact(void)
{
	if( PutFrame() ) {
			
		if( GetFrame() ) {
			
			return CheckReply();
			}
		}
		
	return FALSE;
	}

BOOL CYaskawaACTCPDriver::CheckReply(void)
{
	if( m_bRx[0] == m_bTx[6] ) {

		if( m_bRx[7] & 0x80 ) {

			m_pCtx->m_EC = *( (PWORD) &m_bTx[8] );

			m_pCtx->m_EV = UINT(m_bRx[8]);
			}

		return TRUE;
		}
		
	return FALSE;
	}

// Read Handlers

CCODE CYaskawaACTCPDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	switch( Addr.a.m_Table ) {

		case SPDA0: StartFrame(0x01); break;
		case SPDA1: StartFrame(0x02); break;
		default:    return uCount;
		}

	AddWord(uOffset - 1);

	AddWord(uCount);

	if( Transact() ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CYaskawaACTCPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPDA3: StartFrame(4); break;
		case SPDA4: StartFrame(3); break;
		default:
			if( IsYACParam(Addr.a.m_Table) ) {

				StartFrame(3);
				}

			else {
				return uCount;
				}
			break;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(uCount);

	if( Transact() ) {

		uCount = min( uCount, m_bRx[2]/sizeof(WORD) );

		PU2 p = PU2(&m_bRx[3]);
		
		for( UINT n = 0; n < uCount; n++, p++ ) {
		
			WORD x = *p;
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CYaskawaACTCPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPDA3: StartFrame(4); break;
		case SPDA4: StartFrame(3); break;
		default:
			if( IsYACParam(Addr.a.m_Table) ) {

				StartFrame(3);
				}

			else {
				return uCount;
				}
			break;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(uCount * 2);

	if( Transact() ) {

		uCount = min(uCount, (m_bRx[2]+2)/sizeof(DWORD));

		PU4 p = PU4(&m_bRx[3]);
		
		for( UINT n = 0; n < uCount; n++, p++ ) {
		
			DWORD x = *p;
			
			pData[n] = MotorToHost(x);
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CYaskawaACTCPDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( uCount == 1 ) {
			
		StartFrame(5);
			
		AddWord(Addr.a.m_Offset - 1);
			
		AddWord(pData[0] ? 0xFF00 : 0x0000);
		}
	else {
		StartFrame(15);

		AddWord(Addr.a.m_Offset - 1);

		AddWord(uCount);

		AddByte((uCount + 7) / 8);

		UINT b = 0;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			if( pData[n] ) {
					
				b |= m;
				}

			if( !(m <<= 1) ) {

				AddByte(b);

				b = 0;

				m = 1;
				}
			}

		if( m > 1 ) {

			AddByte(b);
			}
		}

	return Transact() ? uCount : CCODE_ERROR;
	}

CCODE CYaskawaACTCPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( uCount == 1 ) {
			
		StartFrame(6);
			
		AddWord(Addr.a.m_Offset - 1);
			
		AddWord(LOWORD(pData[0]));
		}
	else {
		StartFrame(16);
			
		AddWord(Addr.a.m_Offset - 1);
			
		AddWord(uCount);
			
		AddByte(uCount * 2);
			
		for( UINT n = 0; n < uCount; n++ ) {
				
			AddWord(LOWORD(pData[n]));
			}
		}

	return Transact() ? uCount : CCODE_ERROR;
	}

CCODE CYaskawaACTCPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(16);

	AddWord(Addr.a.m_Offset - 1);

	AddWord(uCount * 2);

	AddByte(uCount*4);

	for(UINT n = 0; n < uCount; n++ ) AddLong(pData[n]);

	return Transact() ? uCount : CCODE_ERROR;
	}

// Connected
CCODE CYaskawaACTCPDriver::LoopBack(void)
{
	m_uPtr = 0;

	UINT uUnit = m_pCtx->m_uUnit;

	m_pCtx->m_uUnit = 1;

	StartFrame(8);

	AddWord(0);
	AddWord(0xA537);

	m_pCtx->m_uUnit = uUnit;

	return Transact() ? 1 : CCODE_ERROR;
	}

// Write Only
BOOL CYaskawaACTCPDriver::IsEnterOrAccept(UINT uTable)
{
	return uTable == SPEN || uTable == SPAC;
	}

// Cached
BOOL CYaskawaACTCPDriver::IsErrCommandOrValue(UINT uTable)
{
	return uTable == SPEC || uTable == SPEV;
	}

// Helpers
BOOL CYaskawaACTCPDriver::IsYACParam(UINT uTable)
{
	switch( uTable ) {

		case SPA:
		case SPB:
		case SPC:
		case SPD:
		case SPE:
		case SPF:
		case SPH:
		case SPL:
		case SPN:
		case SPO:
		case SPP:
		case SPQ:
		case SPT:
		case SPU:
			return TRUE;
		}

	return FALSE;
	}

UINT CYaskawaACTCPDriver::CheckCommParams(AREF Addr, UINT uCount)
{
	if( Addr.a.m_Table == SPH ) {

		UINT uLo = Addr.a.m_Offset;
		UINT uHi = Addr.a.m_Offset + uCount - 1;

		if( uLo <= COMBAUD && uHi >= COMBAUD ) {

			return COMBAUD - uLo;
			}

		if( uLo == COMPAR ) return 0;
		}

	return uCount;
	}

// Socket Management

BOOL CYaskawaACTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaACTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CYaskawaACTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
