#ifndef INCLUDE_OMFLSMM_HPP

#define INCLUDE_OMFLSMM_HPP

#include "../OmFTcpMM/omflmm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Master Serial Driver
//

class COmniFlowModiconMaster : public COmniFlowBaseModiconMaster
{
	public:
		// Constructor
		COmniFlowModiconMaster(void);

		// Destructor
		~COmniFlowModiconMaster(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
	protected:

		// Device Context
		struct CContext : COmniFlowBaseModiconMaster::CBaseCtx
		{
			};
		
		// Data Members
		CContext * m_pCtx;
		BOOL	   m_fAscii;
		LPCTXT	   m_pHex;
		CRC16	   m_CRC;
		UINT	   m_uTimeout;
		UINT	   m_uLast;
				
		// Implementation
		BOOL IsHex(BYTE bData);
		WORD FromHex(BYTE bData);
		
		// Port Access
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
				
		// Transport Layer
		BOOL Transact(void);
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL CheckFrame(void);
		BOOL BinaryTx(void);
		BOOL BinaryRx(void);
		BOOL AsciiTx(void);
		BOOL AsciiRx(void);

		// Transport Helpers
		UINT FindEndTime(void);
	};

#endif

// End of File
