
#include "intern.hpp"

#include "m2mbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Slave Driver
//

// Constructor

CM2MDataBase::CM2MDataBase(void)
{
	}

// Destructor

CM2MDataBase::~CM2MDataBase(void)
{
	}

UINT CM2MDataBase::CheckRecv(PBYTE pData, UINT uLen)
{
	if( pData[PSTART] != MSTART ) return NOTM2M;

	UINT uID = pData[PID];

	if( uID == IDACK) return pData[4] == MEND ? IDACK : BADFRM;

	UINT uRcvLen = (pData[PLENH] << 8) + pData[PLENL];

	if( uRcvLen != uLen ) return BADFRM;

	if( pData[uRcvLen - 1] != MEND ) return BADFRM;

	switch( uID ) {

		case IDDEM:
		case IDCTL: return uID;

		default:
			if( IsFILID(LOBYTE(uID)) ) return uID;
			break;
		}

	return BADREQ;
	}

BOOL CM2MDataBase::VerifyRTUAccess(UINT uCommand, DWORD dNewTime)
{
	if( m_fTrigDis[uCommand] ) {

		if( dNewTime == m_dOldTrig[uCommand] ) return FALSE; // may be Read-Only trigger

		m_fTrigDis[uCommand] = FALSE;
		}

	m_dOldTrig[uCommand] = 0;

	return TRUE;
	}

void CM2MDataBase::ResetCommand(UINT uCommand, DWORD dValue, DWORD dTrigger)
{
	CAddress Addr;

	Addr.a.m_Table  = uCommand;
	Addr.a.m_Offset = uCommand == TSCHE ? 0 : 1;
	Addr.a.m_Type   = LL;
	Addr.a.m_Extra  = 0;

	DWORD Data[1];

	Data[0] = uCommand == TSCHE ? 0 : dValue;

	if( !COMMS_SUCCESS(Write(Addr, Data, 1)) ) { // reset trigger attempt

		m_dOldTrig[uCommand] = dTrigger;

		m_fTrigDis[uCommand] = TRUE; // require new time before retrying function

		InsertRunTimeError(ENOWRITE);

		return;
		}

	m_dOldTrig[uCommand] = 0L;
	m_fTrigDis[uCommand] = FALSE;
	}

void CM2MDataBase::DoErrors(void)
{
	CAddress Addr;

	Addr.a.m_Table  = TERROR;
	Addr.a.m_Offset = 0;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = LL;

	DWORD Data[3];

	Data[0] = m_dError[0];
	Data[1] = m_dError[1];
	Data[2] = m_dError[2];

	Write(Addr, Data, 3);
	}

void CM2MDataBase::DoTimeZone(void)
{
	DWORD Data[1] = {0};

	CAddress Addr;

	Addr.a.m_Table  = TZONE;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = LL;
	Addr.a.m_Extra  = 0;

	m_dTZOffset     = 0;

	if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

		m_dTZOffset = *Data * 3600;
		}
	}

// DEM/SCH Helpers
BOOL CM2MDataBase::FindBlockID(BYTE bID)
{
	UINT uDest = BinaryFind(bID);

	if( m_pbIDNums[uDest] == bID ) {

		m_uListSel = uDest;

		return TRUE;
		}

	return FALSE; // ID not found
	}

UINT CM2MDataBase::BinaryFind(BYTE bID)
{
	UINT uFirst = 0;
	UINT uLast  = m_LDEMCt - 1;

	UINT uMid   = 0;

	PBYTE pList = m_pbIDNums;

	while( uFirst <= uLast ) {

		uMid = (uFirst + uLast) / 2;

		BYTE bTar = pList[uMid];

		if( bID == bTar ) return uMid;

		else if( bID > bTar ) uFirst = uMid + 1;

		else if( uMid ) uLast = uMid - 1;

		else return 0xFF;
		}

	return uMid;
	}

UINT CM2MDataBase::GetBlockData(PDWORD pResp, UINT uQty, BOOL fIsDEM)
{
	CAddress Addr;

	Addr.a.m_Table  = fIsDEM ? TDEMA : TSCHA;
	Addr.a.m_Offset = m_pbStrNum[m_uListSel] << POSSTN;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = LL;

	if( fIsDEM ) {

		if( COMMS_SUCCESS(Read(Addr, pResp, uQty)) ) return uQty;

		return 0;
		}

	UINT uZero	= Addr.a.m_Offset;

	UINT uPos	= 0;

	DWORD dBuff[10];

	while( uPos < uQty ) {

		memset(dBuff, 0, sizeof(dBuff));

		UINT uCount = min(10, uQty - uPos);

		Addr.a.m_Offset = uZero + uPos;

		if( COMMS_SUCCESS(Read(Addr, dBuff, uCount)) ) {

			memcpy(&pResp[uPos], dBuff, uCount * sizeof(DWORD));

			uPos += uCount;
			}

		else return uPos;
		}

	return uQty;
	}

UINT CM2MDataBase::GetIDListData(PBYTE pDest, PDWORD pSrc, UINT uLen)
{
	for( UINT i = 0, j = 0; i < uLen; i++ ) {

		UINT uType  = GetTypeSize(i);

		DWORD dData = pSrc[i];

		switch( uType ) {

			case 4:
				pDest[j++] = HIBYTE(HIWORD(dData));
				pDest[j++] = LOBYTE(HIWORD(dData));
			case 2:
				pDest[j++] = HIBYTE(LOWORD(dData));
			default:
				pDest[j++] = LOBYTE(LOWORD(dData));
				break;
			}
		}

	return j;
	}

UINT CM2MDataBase::GetTypeSize(UINT uPos)
{
	PBYTE p = m_pbIDType[m_uListSel];

	switch( p[uPos] ) {

		case WW: return 2;

		case YY:
		case BB: return 1;
		}

	return 4;
	}

void CM2MDataBase::AddDEMHeader(DEMHEAD *pHead)
{
	AddByte(MSTART);
	AddByte(pHead->bID);
	AddByte(m_bCtr);
	AddByte(STATY);

	MakeTimeStamp(pHead->dTime);
	}

// FILE - Got Request from Server

BOOL CM2MDataBase::InitiateFileTransfer(DEMHEAD *pHead)
{
	BYTE bID = pHead->bID;

	FCBLK *p = m_pFCB;

	if( bID > LOBYTE(p->FCount) + 209 ) { // requested ID out of range

		return FALSE;
		}

	UINT uSize = p->FCBSize;

	if( !uSize ) {

		return FALSE; // no file configuration
		}

	CAddress Addr;

	Addr.a.m_Table  = TDEMA;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = LL;
	Addr.a.m_Extra  = 0;

	PDWORD pData	= new DWORD [uSize];

	BOOL fOk	= FALSE;

	UINT n		= GetBlockData(pData, uSize, TRUE);

	if( n == uSize ) {

		p->wFCount  = 1;			// File Request Count

		fOk         = TRUE;

		UINT uPos   = (2 * (bID - 210)) + 1;	// Position of ID data in read response

		WORD uSize  = LOWORD(pData[uPos]);	// Size of file
		DWORD dTime = pData[uPos+1];		// Creation time

		p->bID      = bID;			// ID to get
		p->FSize    = uSize;
		p->FTime    = dTime;
		p->UFI++;				// Unique File Identifier
		p->uTryCt   = 0;

		p->pDAT->bChunkNo  = 1;
		p->pDAT->bChunkQty = 1 + (p->FSize / MAXCHK);
		p->pDAT->wSent     = 0;
		}

	delete pData;

	return fOk;
	}

BOOL CM2MDataBase::SetFileTrigger(BYTE bID, BOOL fContinue, BOOL fClear)
{
	CAddress Addr;

	Addr.a.m_Table	= TFILE;
	Addr.a.m_Offset	= fContinue ? RTUTRIG : RTUID;
	Addr.a.m_Type	= LL;
	Addr.a.m_Extra	= 0;

	UINT uCount     = fClear ? 2 : 1;

	PDWORD pData	= new DWORD [uCount];

	if( fClear || fContinue ) {

		memset(pData, 0, uCount * 4);
		}

	else {
		*pData = bID;
		}

	BOOL f = COMMS_SUCCESS(Write(Addr, pData, uCount));

	delete pData;

	return f;
	}

BOOL CM2MDataBase::ToggleTrigger(BYTE bID)
{
	if( SetFileTrigger(0, FALSE, FALSE) ) {

		if( SetFileTrigger(bID, FALSE, FALSE) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BYTE CM2MDataBase::GetFileTrigger(void) // check for count being written
{
	CAddress Addr;

	Addr.a.m_Table  = TFILE;
	Addr.a.m_Offset = RTUTRIG;
	Addr.a.m_Type   = LL;
	Addr.a.m_Extra  = 0;

	DWORD Data[1];

	SetTimer(2000);

	while( GetTimer() ) {

		if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

			if( *Data ) {

				return min(MAXCHK, LOBYTE(Data[0]));
				}
			}

		Sleep(10);
		}

	return 0;
	}

BOOL CM2MDataBase::MakeFileReadFrame(PDWORD pData, UINT uCount)
{
	if( ReadFileData(pData, uCount) ) {

		FCBLK * pF = m_pFCB;

		m_uPtr = 0;

		AddByte(MSTART);
		AddByte(pF->bID);
		AddByte(m_bCtr);
		AddWord(9 + (uCount * 4));
		AddByte(pF->pDAT->bChunkNo);
		AddByte(pF->pDAT->bChunkQty);
		AddByte(pF->UFI);

		for( UINT i = 0; i < uCount; i++ ) {

			AddLong(pData[i]);
			}

		AddByte(MEND);

		return TRUE;
		}

	return FALSE;
	}

BOOL CM2MDataBase::ReadFileData(PDWORD pData, UINT uCount)
{
	CAddress Addr;

	Addr.a.m_Table	= m_pFCB->bID;
	Addr.a.m_Offset	= 0;
	Addr.a.m_Type	= LL;
	Addr.a.m_Extra	= 0;

	return COMMS_SUCCESS(Read(Addr, pData, uCount));
	}

// FILE Helpers

BOOL CM2MDataBase::IsFILID(BYTE b)
{
	return b >= TF210 && b <= TF235;
	}

void CM2MDataBase::InitFCB(WORD wCount)
{
	m_pFCB		= &m_FCBLK;

	FCBLK *p	= m_pFCB;

	p->pDAT		= &m_DAT;

	ResetFCB();

	p->UFI		= 1;			// Unique File Identifier
	p->FCount	= (wCount - 1) / 2;	// total number of files
	p->FCBSize	= wCount;		// size of File Control Block
	}

void CM2MDataBase::ResetFCB(void)
{
	FCBLK *p = m_pFCB;

	p->bID		= 0;			// ID of File
	p->FSize	= 0;			// size of file
	p->FTime	= 0;			// File creation time
	p->fBusy	= FALSE;		// Sending File
	p->wFCount	= 0;			// number of files to send

	p->pDAT->bChunkNo	= 0;
	p->pDAT->bChunkQty	= 0;
	p->pDAT->wSent		= 0;
	}

void CM2MDataBase::ClearFileRead(void)
{
	ResetFCB();
	SetFileTrigger(0, FALSE, TRUE);
	}

BOOL CM2MDataBase::InvalidFileReq(DEMHEAD *pHead)
{
	WORD w = m_pFCB->wFCount;

	if( !w ) {

		return TRUE;
		}

	AddDEMHeader(pHead);

	AddWord(w);

	for( UINT i = 0; i < w; i++ ) {

		AddWord(0);
		AddLong(0);
		}

	return FALSE;
	}

// Helpers

void CM2MDataBase::AddByte(BYTE b)
{
	m_bTx[m_uPtr++] = b;
	}

void CM2MDataBase::AddWord(WORD w)
{
	AddByte(HIBYTE(w));
	AddByte(LOBYTE(w));
	}

void CM2MDataBase::AddLong(DWORD d)
{
	AddWord(HIWORD(d));
	AddWord(LOWORD(d));
	}

void CM2MDataBase::ClearLoadArrays(void)
{
	memset(m_pbStrNum, 0, MAXIDQ);
	memset(m_pbIDNums, 0, MAXIDQ);
	memset(m_pwIDCnt,  0, sizeof(m_pwIDCnt));
	memset(m_pbIDType, 0, sizeof(m_pbIDType));
	}

void CM2MDataBase::AllocTypeBuffer(UINT uBuff, UINT uSize)
{
	if( uSize ) {

		m_pbIDType[uBuff] = new BYTE [ uSize ];

		memset(m_pbIDType[uBuff], 0, uSize);

		return;
		}

	m_pbIDType[uBuff] = NULL;
	}

void CM2MDataBase::DeleteTypeBuffers(void)
{
	for( UINT i = 0; i < MAXIDQ; i++ ) {

		if( m_pbIDType[i] ) {

			delete m_pbIDType[i];

			m_pbIDType[i] = NULL;
			}
		}

	m_LDEMCt = 0;
	}

void CM2MDataBase::MakeAck(BYTE bCtr, BYTE bCode)
{
	m_uPtr = 0;

	AddByte(MSTART);
	AddByte(IDACK);
	AddByte(bCtr);
	AddByte(bCode);
	AddByte(MEND);
	}

// Numeric manipulation
UINT CM2MDataBase::GetHexValue(PBYTE p, UINT uCt)
{
	UINT u = 0;

	for( UINT i = 0; i < uCt; i++ ) {

		u <<= 8;

		u += p[i];
		}

	return u;
	}

BOOL CM2MDataBase::CheckTimeVal(PDWORD pdTime)
{
	DWORD dTime = *pdTime;

	if( dTime == 1 ) dTime = GetTimeStamp();

	else {
		if( dTime < YEAR2K ) {

			InsertRunTimeError(ENOTIME);
			return FALSE;
			}
		}

	*pdTime = dTime;

	return TRUE;
	}

void CM2MDataBase::MakeTimeStamp(DWORD dTime)
{
	DWORD d = dTime - YEAR2K - m_dTZOffset;

	AddByte(HIBYTE(HIWORD(d)));
	AddByte(LOBYTE(HIWORD(d)));
	AddByte(HIBYTE(LOWORD(d)));
	AddByte(LOBYTE(LOWORD(d)));
	}

void CM2MDataBase::InsertRunTimeError(DWORD dError)
{
	m_dError[2] = m_dError[1];
	m_dError[1] = m_dError[0];
	m_dError[0] = dError;
	}

// End of File
