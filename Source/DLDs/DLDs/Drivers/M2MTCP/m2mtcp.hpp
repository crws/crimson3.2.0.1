
//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp TCP Slave
//

#include "m2mbase.hpp"

#ifndef M2MDATATCPINC
#define M2MDATATCPINC

class CM2MDataTCP : public CM2MDataBase
{
	public:
		// Constructor
		CM2MDataTCP(void);

		// Destructor
		~CM2MDataTCP(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Entry Points
		DEFMETH(void) Service(void);
		
	protected:

		BOOL	m_fReadDataNotDone;

		UINT	m_uCheckPhase;

		// Stack ASID
		PBYTE	m_pASID;
		UINT	m_uASIDCt;
		UINT	m_uASIDNext;

		// Stack RBE
		UINT	*m_pRBEItem;
		PDWORD	m_pRBETime;
		UINT	m_uRBECt;
		UINT	m_uRBENext;

		// Implementation

		void	DoServerAccess(SOCKET &Sock);
		BOOL	DoRTUAccess(void); // SCH / RBE / FIL
		BOOL	GetRTUFlag(UINT uTable, PDWORD pData, UINT uCount);
		void	SetASIDFlag(BYTE bOldItem, BOOL fOld, PDWORD pData);
		void	SetRBEFlag(UINT uOldItem, BOOL fOld, PDWORD pData);
		BOOL	DoSCHAccess(SOCKET &Sock);
		BOOL	DoRBEAccess(SOCKET &Sock);
		void	FinishRTUAccess(SOCKET &Sock, UINT uType, DWORD dTime);

		// Opcode Handlers

		// DEMAND POLL
		void	DoDEMRead (SOCKET &Sock, UINT uLen);

		// SCHEDULED DATA
		BOOL	DoSCHRead(SOCKET &Sock, PDWORD pData);

		//  DEM/SCH Buffer Prep
		BOOL	MakeDEMSendBuffer(SOCKET &Sock, DEMHEAD *pHead);

		// Send DEM/SCH
		BOOL	SendDEMSendBuffer(SOCKET &Sock);

		// CONTROL
		void	DoCTLWrite(SOCKET &Sock, UINT uLen);

		// REPORT BY EXCEPTION
		BOOL	DoRBERead (SOCKET &Sock, PDWORD pData);

		// CONTROL / REPORT BY EXCEPTION Buffer Prep
		BOOL	DoRBEResp(SOCKET &Sock, BOOL fGoodData, CTLINFO * pCtl);

		// Send CONTROL / REPORT BY EXCEPTION
		BOOL	SendRBEResp(SOCKET &Sock);

		// FILE - Data From RTU to Server
		BOOL	DoFILRead(SOCKET &Sock);
		BOOL	ContinueFILRead(SOCKET &Sock);
		
		// FILE - Data From RTU to Server
		BOOL	SendFILRead(SOCKET &Sock);

		// FILE - Request From Server for RTU File
		void	DoFILWrite(SOCKET &Sock, UINT uLen);

		// FILE - Request From Server for RTU File
		BOOL	SendFILWrite(SOCKET &Sock);

		// ACK/NAK/STATUS
		void	DoAckNack(SOCKET &Sock, UINT uCtr, BOOL fIsAck);
		void	SendInvalidStatus(SOCKET &Sock, BYTE bID, BYTE bCtr, DWORD dTime);
		UINT	WaitForAck(SOCKET &Sock);

		// ASID and RBE Stacks
		void	AppendToASID(BYTE bItem);
		void	AppendToRBE (UINT uItem, UINT uTime);
		void	TakeFromASID(BYTE bItem);
		void	TakeFromRBE (UINT uItem);
		UINT	ASIDIsDuplicated (BYTE bItem);
		DWORD	RBEIsDuplicated (UINT uItem);
		void	DeleteStacks(void);

		// Check Receive
		void	ReadData(SOCKET &Sock);

		// TCP Functions and Port Access
		UINT	HandlePhase(SOCKET &Sock);
		UINT	DoMasterPhase(SOCKET &Sock);
		BOOL	OpenCommandPort(SOCKET &Sock);
		void	OpenSocket(UINT n, SOCKET &Sock);
		void	InitSocket(SOCKET &Sock);
		BOOL	OpenMaster(SOCKET &Sock);
		BOOL	ConnectToServer(SOCKET &Sock);
		BOOL	CheckAccess(SOCKET &Sock);
		BOOL	ExecuteSend(SOCKET &Sock);
		void	CloseSocket(SOCKET &Sock);
		void	RestartConn(SOCKET &Sock);
		void	CloseMaster(SOCKET &Sock);
	};

// End of File

#endif
