
#include "intern.hpp"

#include "smam.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SMA Serial Master Driver
//

// Instantiator

INSTANTIATE(CSmaMasterDriver);

// Constructor

CSmaMasterDriver::CSmaMasterDriver(void)
{
	m_Ident   = DRIVER_ID;	
	}

// Destructor

CSmaMasterDriver::~CSmaMasterDriver(void)
{
	}

// Configuration

void MCALL CSmaMasterDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_Src = GetWord(pData);
		}

	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

void MCALL CSmaMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);

	m_uBaud = Config.m_uBaudRate;

	Config.m_uFlags |= flagNoCheckError;
	}

// Management

void MCALL CSmaMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CSmaMasterDriver::Open(void)
{

	}

// Device

CCODE MCALL CSmaMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_fInit = FALSE;

			m_pCtx->m_Mode = GetByte(pData);

			m_pCtx->m_Addr = GetByte(pData);

			m_pCtx->m_Addr2 = GetByte(pData);

			m_pCtx->m_Poll = ToTicks(GetWord(pData));

			memset(m_pCtx->m_Time, 0, elements(m_pCtx->m_Time) * sizeof(UINT));

			m_pCtx->m_uParam = GetWord(pData);

			m_pCtx->m_pParam = NULL;
			
			m_pCtx->m_pParam = new Param[m_pCtx->m_uParam];

			for( UINT u = 0; u < m_pCtx->m_uParam; u++ ) {

				m_pCtx->m_pParam[u].m_Name = GetString(pData);

				m_pCtx->m_pParam[u].m_Addr.m_Ref = GetLong(pData);

				m_pCtx->m_pParam[u].m_Channel = 0;
				}

			m_pCtx->m_pBook = NULL;

			memset(m_pCtx->m_pDoPoll, 0, elements(m_pCtx->m_pDoPoll));

			m_pCtx->m_fError = TRUE;

			m_pCtx->m_SN     = 0;

			m_pCtx->m_fMemStore = GetByte(pData) ? TRUE : FALSE;

			// Unsafe for field use - disable !

			m_pCtx->m_fMemStore = FALSE;

			// Fall back support

			m_pCtx->m_fAux   = FALSE;

			m_pCtx->m_pParam2 = NULL;

			m_pCtx->m_pParam2 = new Param[m_pCtx->m_uParam];

			memcpy(m_pCtx->m_pParam2, m_pCtx->m_pParam, sizeof(Param) * m_pCtx->m_uParam);

			m_pCtx->m_pBook2 = NULL;

			m_pCtx->m_fReset = FALSE;

			m_pCtx->m_fMemStore2 = GetByte(pData) ? TRUE : FALSE;

			// Unsafe for field use - disable !

			m_pCtx->m_fMemStore2 = FALSE;

			pDevice->SetContext(m_pCtx);

			//PrintDeviceList();

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;

	}

CCODE MCALL CSmaMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete [] m_pCtx->m_pBook;

		delete [] m_pCtx->m_pParam;

		delete [] m_pCtx->m_pBook2;

		delete [] m_pCtx->m_pParam2;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Transport

BOOL CSmaMasterDriver::Send(void)
{
	if( !(m_pTx[2] & 0x40) ) {

		UINT uTick = GetTickCount();

		UINT uCount = ToTicks(30);

		while( int(GetTickCount() - uTick - uCount) < 0   ) {

			if( m_pData->Read(5) < NOTHING ) {

				uTick = GetTickCount();
				}

			if( uTick > GetTickCount() ) {

				break;
				}
			}
		}

	m_pData->ClearRx();
		
	m_pData->Write(m_pTx, m_uPtr, FOREVER);

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_uPtr; u++ ) {

		AfxTrace(" %2.2x ", m_pTx[u]);
		}

*/	return TRUE;
	}

BOOL CSmaMasterDriver::Recv(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	UINT uTxPtr = m_uPtr;

	m_uPtr = 0;

	m_uBuff = 0;

	BOOL fEscape = FALSE;

	BOOL fStart  = FALSE;

	UINT uLoop   = 4;

	for( UINT uTry = 0; uTry < uLoop; uTry++ ) {

	//	AfxTrace("\nRx ");

		fStart = FALSE;

		SetTimer(500);

		while( (uTimer = GetTimer()) ) {
	
			if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

				continue;
				}

		//	AfxTrace("%2.2x ", uData);

			if( !fStart ) {
				
				if( uData != 0x7E ) {

					continue;
					}

				fStart = TRUE;

				SetTimer(2500);
				}

			if( uData == 0x7D ) {

				fEscape = TRUE;

				continue;
				}

			if( fEscape ) {

				if( uData == 0x7E ) {

					m_pRx[m_uPtr++] = uData;

					fEscape = FALSE;
					
					continue;
					}

				fEscape = FALSE;

				if( uData & 0x20 ) {

					uData &= ~0x20;
					}
				else {
					uData |= 0x20;
					}
				}
			else {
				if( uData == 0x7E ) {
				
					if( m_uPtr != 0 ) {

						if( Check() ) {

							if( IsMultiPacket() ) {

								UINT uBytes = m_uPtr - 11 - 2;
								
								// Save buffer

								memcpy(m_pBuff + m_uBuff, m_pRx + 11, uBytes);

								m_uBuff += uBytes;
								
								fEscape = FALSE;

								Delay();

								SendAck();

								m_uPtr = 0;

								uTry   = 0;

								uLoop  = 8;

							//	AfxTrace("\nRx: ");

								fStart = FALSE;

								SetTimer(500);

								continue;
								}
							
							return TRUE;
							}
						else {	
							return FALSE;
							}
						}

					continue;
					}
				}

			if( m_uPtr == 4 ) {	// Check Source

				if( uData != (m_pBase->m_fAux ? m_pBase->m_Addr2 : m_pBase->m_Addr) ) {

					m_uPtr = 0;

					fStart = FALSE;

					SetTimer(2500);

					continue;
					}
				}

			if( m_uPtr == 6 ) {	// Check Destination

				if( uData != m_Src ) {

					m_uPtr = 0;

					fStart = FALSE;

					SetTimer(2500);

					continue;
					}
				}

			m_pRx[m_uPtr++] = uData;

			if( m_uPtr >= sizeof(m_pRx) ) {

				return FALSE;
				}
			}

		m_uPtr = uTxPtr;

		Delay();

		Send();

		m_uPtr = 0;

		fStart = FALSE;

		SetTimer(500);
		}

	return FALSE;
	}

// End of File
