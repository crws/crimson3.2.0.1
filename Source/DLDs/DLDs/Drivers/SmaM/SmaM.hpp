#include "sma.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  SMA Serial Master Driver
//

class CSmaMasterDriver : public CSmaDriver
{
	public:
		// Constructor
		CSmaMasterDriver(void);

		// Destructor
		~CSmaMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
					
	protected:

		// Device Data
		struct CContext : CSmaDriver::CBaseCtx
		{
			
			};

		
		CContext * m_pCtx;

		// Transport
	virtual BOOL Send(void);
	virtual BOOL Recv(void);


				
	};

// End of File
