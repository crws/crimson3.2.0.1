
#include "intern.hpp"

#include "BSAPExtS.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Extended Serial Slave Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

static BYTE crctbl[] = {
	0x087,0x00F,0x00E,0x01E,0x095,0x02C,0x01C,0x03D,
	0x0A3,0x049,0x02A,0x058,0x0B1,0x06A,0x038,0x07B,
	0x0CF,0x083,0x046,0x092,0x0DD,0x0A0,0x054,0x0B1,
	0x0EB,0x0C5,0x062,0x0D4,0x0F9,0x0E6,0x070,0x0F7,
	0x006,0x01F,0x08F,0x00E,0x014,0x03C,0x09D,0x02D,
	0x022,0x059,0x0AB,0x048,0x030,0x07A,0x0B9,0x06B,
	0x04E,0x093,0x0C7,0x082,0x05C,0x0B0,0x0D5,0x0A1,
	0x06A,0x0D5,0x0E3,0x0C4,0x078,0x0F6,0x0F1,0x0E7,
	0x085,0x02E,0x00C,0x03F,0x097,0x00D,0x01E,0x01C,
	0x0A1,0x068,0x028,0x079,0x0B3,0x04B,0x03A,0x05A,
	0x0CD,0x0A2,0x044,0x0B3,0x0DF,0x081,0x056,0x090,
	0x0E9,0x0E4,0x060,0x0F5,0x0FB,0x0C7,0x072,0x0D6,
	0x004,0x03E,0x08D,0x02F,0x016,0x01D,0x09F,0x00C,
	0x020,0x078,0x0A9,0x069,0x032,0x05B,0x0BB,0x04A,
	0x04C,0x0B2,0x0C5,0x0A3,0x05E,0x091,0x0D7,0x080,
	0x068,0x0F4,0x0E1,0x0E5,0x07A,0x0D7,0x0F3,0x0C6,
	0x083,0x04D,0x00A,0x05C,0x091,0x06E,0x018,0x07F,
	0x0A7,0x00B,0x02E,0x01A,0x0B5,0x028,0x03C,0x039,
	0x0CB,0x0C1,0x042,0x0D0,0x0D9,0x0E2,0x050,0x0F3,
	0x0EF,0x087,0x066,0x096,0x0FD,0x0A4,0x074,0x0B5,
	0x002,0x05D,0x08B,0x04C,0x010,0x07E,0x099,0x06F,
	0x026,0x01B,0x0AF,0x00A,0x034,0x038,0x0BD,0x029,
	0x04A,0x0D1,0x0C3,0x0C0,0x058,0x0F2,0x0D1,0x0E3,
	0x06E,0x097,0x0E7,0x086,0x07C,0x0B4,0x0F5,0x0A5,
	0x081,0x06C,0x008,0x07D,0x093,0x04F,0x01A,0x05E,
	0x0A5,0x02A,0x02C,0x03B,0x0B7,0x009,0x03E,0x018,
	0x0C9,0x0E0,0x040,0x0F1,0x0DB,0x0C3,0x052,0x0D2,
	0x0ED,0x0A6,0x064,0x0B7,0x0FF,0x085,0x076,0x094,
	0x000,0x07C,0x089,0x06D,0x012,0x05F,0x09B,0x04E,
	0x024,0x03A,0x0AD,0x02B,0x036,0x019,0x0BF,0x008,
	0x048,0x0F0,0x0C1,0x0E1,0x05A,0x0D3,0x0D3,0x0C2,
	0x06C,0x0B6,0x0E5,0x0A7,0x07E,0x095,0x0F7,0x084,
	0x08F,0x08B,0x006,0x09A,0x09D,0x0A8,0x014,0x0B9,
	0x0AB,0x0CD,0x022,0x0DC,0x0B9,0x0EE,0x030,0x0FF,
	0x0C7,0x007,0x04E,0x016,0x0D5,0x024,0x05C,0x035,
	0x0E3,0x041,0x06A,0x050,0x0F1,0x062,0x078,0x073,
	0x00E,0x09B,0x087,0x08A,0x01C,0x0B8,0x095,0x0A9,
	0x02A,0x0DD,0x0A3,0x0CC,0x038,0x0FE,0x0B1,0x0EF,
	0x046,0x017,0x0CF,0x006,0x054,0x034,0x0DD,0x025,
	0x062,0x051,0x0EB,0x040,0x070,0x072,0x0F9,0x063,
	0x08D,0x0AA,0x004,0x0BB,0x09F,0x089,0x016,0x098,
	0x0A9,0x0EC,0x020,0x0FD,0x0BB,0x0CF,0x032,0x0DE,
	0x0C5,0x026,0x04C,0x037,0x0D7,0x005,0x05E,0x014,
	0x0E1,0x060,0x068,0x071,0x0F3,0x043,0x07A,0x052,
	0x00C,0x0BA,0x085,0x0AB,0x01E,0x099,0x097,0x088,
	0x028,0x0FC,0x0A1,0x0ED,0x03A,0x0DF,0x0B3,0x0CE,
	0x044,0x036,0x0CD,0x027,0x056,0x015,0x0DF,0x004,
	0x060,0x070,0x0E9,0x061,0x072,0x053,0x0FB,0x042,
	0x08B,0x0C9,0x002,0x0D8,0x099,0x0EA,0x010,0x0FB,
	0x0AF,0x08F,0x026,0x09E,0x0BD,0x0AC,0x034,0x0BD,
	0x0C3,0x045,0x04A,0x054,0x0D1,0x066,0x058,0x077,
	0x0E7,0x003,0x06E,0x012,0x0F5,0x020,0x07C,0x031,
	0x00A,0x0D9,0x083,0x0C8,0x018,0x0FA,0x091,0x0EB,
	0x02E,0x09F,0x0A7,0x08E,0x03C,0x0BC,0x0B5,0x0AD,
	0x042,0x055,0x0CB,0x044,0x050,0x076,0x0D9,0x067,
	0x066,0x013,0x0EF,0x002,0x074,0x030,0x0FD,0x021,
	0x089,0x0E8,0x000,0x0F9,0x09B,0x0CB,0x012,0x0DA,
	0x0AD,0x0AE,0x024,0x0BF,0x0BF,0x08D,0x036,0x09C,
	0x0C1,0x064,0x048,0x075,0x0D3,0x047,0x05A,0x056,
	0x0E5,0x022,0x06C,0x033,0x0F7,0x001,0x07E,0x010,
	0x008,0x0F8,0x081,0x0E9,0x01A,0x0DB,0x093,0x0CA,
	0x02C,0x0BE,0x0A5,0x0AF,0x03E,0x09D,0x0B7,0x08C,
	0x040,0x074,0x0C9,0x065,0x052,0x057,0x0DB,0x046,
	0x064,0x032,0x0ED,0x023,0x076,0x011,0x0FF,0x000
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Extended Serial Slave Driver
//

// Instantiator

INSTANTIATE(CBSAPExtSDriver);

// Constructor

CBSAPExtSDriver::CBSAPExtSDriver(void)
{
	m_Ident	= DRIVER_ID;

	InitGlobal(TRUE);
	}

// Destructor

CBSAPExtSDriver::~CBSAPExtSDriver(void)
{
	m_pExtra->Release();
	}

// Configuration

void MCALL CBSAPExtSDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_BDrvCfg.ThisLocal	= GetByte(pData);
		m_BDrvCfg.ThisGlobal	= GetWord(pData);
		m_BDrvCfg.NetLevel	= GetByte(pData);
		}

	MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

void MCALL CBSAPExtSDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {

		Config.m_uFlags |= flagFastRx;
		}

	Make485(Config, FALSE);

	}
	
// Management

void MCALL CBSAPExtSDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CBSAPExtSDriver::Detach(void)
{
	if( m_pCtx != NULL ) {

		CloseDown(m_pCtx);
		}

	CSlaveDriver::Detach();
	}

// Master Flags
WORD MCALL CBSAPExtSDriver::GetMasterFlags(void)
{
	return MF_NO_SPANNING;
	}

// Device

CCODE MCALL CBSAPExtSDriver::DeviceOpen(IDevice *pDevice)
{
	CSlaveDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx    = new CContext;

			WORD wKey = GetWord(pData);

			switch( wKey ) {

				case 0xBCDE:	m_bIsC3 = 3; break;

				default:	return CCODE_ERROR | CCODE_HARD;
				}

			m_pCtx->m_dTGMagic = 0;
			m_pCtx->m_dARMagic = 0;

			LoadTags(pData);

			m_pCtx->m_bHasNRT  = 0;

			m_pCtx->m_uMSDVers = m_wVersion;
						
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CBSAPExtSDriver::DeviceClose(BOOL fPersist)
{
	// don't delete names, but delete all other buffers

	DeleteVarInfo();

	CloseListBuffers();

	return CSlaveDriver::DeviceClose(fPersist);
	}

// Entry Point

void MCALL CBSAPExtSDriver::Service(void)
{
	ReadData();

	Sleep(20);
	}

// Receive Processing

void CBSAPExtSDriver::ReadData(void)
{
	UINT uTime = 200;

	m_uRPtr    = 0;

	while( (BOOL)uTime ) {

		UINT uData = m_pData->Read(uTime);

		if( uData < NOTHING ) {

			uTime      = 50;

			BYTE bData = LOBYTE(uData);

			switch( m_uState ) {

				case 0:
					if( bData == DLE ) {

						m_uState = 1;

//**/						AfxTrace0("\r\nSlave Recv: <10>");
						}

					break;

				case 1:
//**/					AfxTrace1("<%2.2x>--", bData);

					switch( bData ) {

						case STX:
							m_uState = 2;
							m_uRPtr  = 0;
							break;

						case DLE: break;	// stay here to check next byte

						default:
							m_uState = 0;	// wait for start of valid frame
							break;
						}

					break;

				case 2:
					if( bData == DLE ) {

//**/						AfxTrace0("<#>");

						m_uState = 3;
						}

					else {
//**/						AfxTrace1("<%2.2x>", bData);

						m_bRx[m_uRPtr++] = bData;
						}

					break;

				case 3:
					if( bData == ETX ) {

//**/						AfxTrace1(" ETX--%d ", m_uRPtr);

						m_bRx[m_uRPtr]	= ETX;

						m_uETXPos	= m_uRPtr;

						m_uState	= 4;
						}

					else {
//**/						AfxTrace1("<%2.2x>", bData);

						m_bRx[m_uRPtr++] = bData;

						m_uState         = 2;
						}

					break;

				case 4: // CRC does *not* get DLE'd
//**/					AfxTrace1("_%2.2x_", bData);

					m_uState = 5; // CRC byte 1
					break;

				case 5:
//**/					AfxTrace1("_%2.2x", bData);

					m_uState = 0;

					ProcReceive();

					return;
				}

			if( m_uRPtr >= sizeof(m_bRx) ) {

				m_uState = 0;

				m_pData->ClearRx();

				return;
				}
			}
		}
	}

void CBSAPExtSDriver::ProcReceive(void) {

	m_fIsThis = CheckDrop(m_bRx[RXDADD]);

	m_uTPtr = 0;

	UINT uFCPosition  = RXDFC;

	if( m_fGlobal ) {

		uFCPosition = RXGDFC;
		}

	BYTE bFuncType = m_bRx[uFCPosition];

	if( IsNRTRecv(m_bRx[uFCPosition]) ) {	// NRT always local

		m_pTx   = m_bPollTx;

		m_uTPtr = 0;

		AddByte(0);
		AddByte(m_bRx[1]);
		AddByte(ACKDOWN);
		AddByte(m_BDrvCfg.ThisLocal);
		AddByte(0);
		AddByte(3);

		Send();

		m_uHasNRT = 10;

		WriteNRT();

		return;
		}

	if( IsReqForNRT() ) {

		return;
		}

	if( IsReqForTMS() ) {

		m_pTx = m_bTMSTx;

		m_pCtx->m_bSerNum = m_bRx[RXSERN];
		m_pCtx->m_uAppSeq = (UINT)m_bRx[RXLSQL] + (UINT)(((UINT)m_bRx[RXLSQH]) << 8);

		BuildTimeSync();

		GetTMS();

		return;
		}

	if( IsRespOfNRT() ) {

		SaveNRT();

		m_pCtx->m_bHasNRT |= 1;

		return;
		}

	if( IsRespOfTMS() ) {

		SaveTMS();

		m_pCtx->m_bHasNRT |= 2;

		return;
		}

	if( IsPollOrAck(bFuncType) || bFuncType != MFCRDBACC ) {

		if( bFuncType == POLLMSG ) {

			PollReceived.Addr	= m_bRx[RXDADD];
			PollReceived.MsgSer	= m_bRx[RXSERN];

			SendPollAck();

			return;
			}

		if( IsAlarm(bFuncType) ) {

			if( bFuncType == OFCALMIN ) {

				SendAlarmInitAck();
				}

			if( bFuncType == OFCALACK ) {

				SendAlarmAck();
				}
			}
		return;
		}

	m_pCtx->m_bSerNum = m_bRx[RXSERN];
	m_pCtx->m_uAppSeq = *(PU2)&m_bRx[RXLSQL];

	UINT uSizeR	= 0;

	UINT uRAdj	= 0;

	BYTE bOC	= m_bRx[uFCPosition + 5];	// operation code

	m_uReqType	= bOC;

	BOOL fArray	= IsArrayOpcode(bOC);

	if( !(bOC & 0x80) ) {				// operation is a read, has field selects if not array

		if( !fArray ) {

			switch( m_bRx[uFCPosition + 6] ) {

				case 1: uRAdj = 3; break;
				case 3: uRAdj = 2; break;
				case 7: uRAdj = 1; break;
				default: return;
				}
			}

		}

	switch( bOC ) {

		case OFCRDBYA:	uSizeR = sizeof(RBYADDRRX) - uRAdj;	break;
		case OFCRDBYN:	uSizeR = sizeof(RBYNAMERX) - uRAdj;	break;
		case OFCRDBYL:	uSizeR = sizeof(RBYLISTRX) - uRAdj - 2;	break;
		case OFCRDBYLC:	uSizeR = sizeof(RBYLISTRX) - uRAdj;	break;
		case OFCRARRS:	uSizeR = sizeof(RXSHORTARRAY);		break;
		case OFCRARRG:	uSizeR = sizeof(RXGENERALARRAY);	break;
		case OFCRARRSZ:	uSizeR = sizeof(RXARRAYSIZE);		break;

		case OFCWRBYA:	uSizeR = sizeof(WBYADDRRX);		break;
		case OFCWRBYN:	uSizeR = sizeof(WBYNAMERX);		break;
		case OFCWRBYL:	uSizeR = sizeof(WBYLISTRX) - 2;		break;
		case OFCWRBYLC:	uSizeR = sizeof(WBYLISTRX);		break;
		case OFCWARRS:	uSizeR = sizeof(RXSHORTARRAY);		break;
		case OFCWARRG:	uSizeR = sizeof(RXGENERALARRAY);	break;

		default:
			SendNak();
			return;
		}

	UINT uSizeH = m_fGlobal ? sizeof(GLBMSGHEAD) : sizeof(LOCMSGHEAD);

	UINT uSizeE = sizeof(ENDHEADLIST);

	UINT uSizeD = m_uETXPos - uSizeH - uSizeE - uSizeR + 2 + (fArray ? 2 : 0);

	if( uSizeD || IsArrayOpcode(LOBYTE(m_uReqType)) ) {

		UINT uPos   = 0;

		MakeReqBuffer(m_HeadList, uSizeH, &uPos);
		MakeReqBuffer(m_FuncList, uSizeE, &uPos);
		MakeReqBuffer(m_ReqList,  uSizeR, &uPos);
		MakeReqBuffer(m_DataList, uSizeD, &uPos);

//**/		AfxTrace0("\r\nHead "); for( uRAdj = 0; uRAdj < uSizeH; uRAdj++) AfxTrace1("%x ", m_HeadList[uRAdj]);
//**/		AfxTrace0("\r\nFunc "); for( uRAdj = 0; uRAdj < uSizeE; uRAdj++) AfxTrace1("%x ", m_FuncList[uRAdj]);
//**/		AfxTrace0("\r\nRequ "); for( uRAdj = 0; uRAdj < uSizeR; uRAdj++) AfxTrace1("%x ", m_ReqList[uRAdj]);
//**/		AfxTrace0("\r\nData "); for( uRAdj = 0; uRAdj < uSizeD; uRAdj++) AfxTrace1("%x ", m_DataList[uRAdj]);

		DoReceive();
		}
	}

void CBSAPExtSDriver::DoReceive(void)
{
	BYTE bOpcode = m_pReqList[0];

	m_bRER = 0;

	m_uReqType = bOpcode;

	if( bOpcode & 0x80 ) {

		switch( bOpcode ) {

			case OFCWRBYA:
				WriteByAddress();
				return;

			case OFCWRBYN:
				WriteByName();
				return;

			case OFCWRBYL:
			case OFCWRBYLC:
				WriteByList(bOpcode == OFCWRBYL);
				return;

			case OFCWARRS:
				WriteShortArray();
				return;

			case OFCWARRG:
				WriteGeneralArray();
				return;
			}
		}

	else {
		SetFieldSelects();

		switch( bOpcode ) {

			case OFCRDBYA:
			case OFCRDBYN:
				ReadSignal(bOpcode);
				return;

			case OFCRDBYL:
			case OFCRDBYLC:
				ReadByList(bOpcode == OFCRDBYL);
				return;

			case OFCRARRS:
				ReadShortArray();
				return;

			case OFCRARRG:
				ReadGeneralArray();
				return;

			case OFCRARRSZ:
				ReadArraySize();
				return;
				}
		}
	}

void CBSAPExtSDriver::MakeReqBuffer(PBYTE pDest, UINT uSize, UINT *pPos)
{
	UINT uPos = *pPos;

	*pPos     = uPos + uSize;

	memcpy(pDest, &m_bRx[uPos], uSize);
	}

// Read Routines
void CBSAPExtSDriver::ReadByList(BOOL fFirst)
{	
	RLISTCNTL *pL = m_pListCntl;

	UINT uLC      = pL->TotalSize;

	BOOL fHasList = (BOOL)uLC;

	UINT uLN    = 6;	// find position of List Number (position 6 if 1 FS byte)

	switch( m_ReqList[1] ) {

		case 15: uLN++;	// 4 FS bytes
		case 7:	uLN++;	// 3 FS bytes
		case 3: uLN++;	// 2 FS bytes
		case 1:
			break;

		default:
			SendNak();	// invalid Field Select Value
			return;
		}

	pL->Start    = fFirst ? 1 : (WORD)IntelToHost(*(PU2)&m_ReqList[uLN + 1]);

	uLN          = m_ReqList[uLN];		// get list number

	pL->Selected = NOTHING;
	pL->uListCt  = 0;

	UINT i       = pL->Start - 1;

	BOOL fFound  = FALSE;

	while( i < uLC ) {

		DWORD d = m_pdLNumList[i];

		UINT u  = HIBYTE(LOWORD(d));		// List Number

		if( uLN == u ) {		// found an item in list

			if( !fFound ) {

				pL->Selected	= HIBYTE(LOWORD(d));	// first List Number
				pL->LNPos	= i;
				pL->uListCt	= 1;
				fFound		= TRUE;
				}

			else {
				pL->uListCt++;
				}
			}

		else {
			if( u > uLN ) {

				break;
				}
			}

		i++;
		}

	fHasList	= pL->Selected < NOTHING;

	UINT uHasOne	= 0;

	UINT uQty	= fHasList ? pL->uListCt : 0;

	UINT uSel;

	UINT u1stErr	= 256;

	DWORD Data[1];

	if( fHasList ) {

		CloseListBuffers();

		m_dLBMagic	= 0x12345678;

		pL->pMSDAddr	= new  WORD [uQty];
		pL->pListAddr	= new DWORD [uQty];
		pL->pListData	= new DWORD [uQty];
		pL->pListType	= new  BYTE [uQty];
		pL->pItemBad	= new  BYTE [uQty];
		pL->pIndex	= new  UINT [uQty];

		memset(pL->pItemBad, RERMATCH, uQty);

		m_bRER		= 0;

		for( uSel = 0; uSel < uQty; uSel++ ) {	// assemble data, log any errors

			UINT uPos = pL->LNPos + uSel;

			UINT uInx = HIWORD(m_pdLNumList[uPos]);

			CAddress A;

			A.m_Ref	= m_pdAddrList[uInx];

			if( COMMS_SUCCESS(Read(A, Data, 1)) ) {

				uHasOne = 1;

				pL->pMSDAddr[uSel]	= m_pwMSDList[uInx];
				pL->pListAddr[uSel]	= A.m_Ref;
				pL->pListData[uSel]	= LongToReal(A.a.m_Type, Data[0]);
				pL->pListType[uSel]	= m_pbTypeList[uInx];
				pL->pItemBad[uSel]	= 0;
				pL->pIndex[uSel]	= uInx;
				}

			else {
				if( u1stErr == 256 ) {

					u1stErr = uSel;
					}

				pL->pItemBad[uSel] = RERMATCH;

				m_bRER = RERMATCH;
				}
			}
		}

	fHasList = uHasOne;

	if( !uHasOne && m_dLBMagic == 0x12345678 ) {

		CloseListBuffers();
		}

	UINT uStart	= pL->Start - 1;

	UINT uFieldsQty	= 0;

	UINT uFrameSize	= 14;

	UINT uContinue  = 0;

	UINT uActualQty	= 0;

	PBYTE *pFields	= NULL;

	if( uHasOne ) {

		m_dVIMagic  = 0x12345678;

		m_upVICount	= uQty;

		m_pVI		= new PVARINFO [uQty];

		pFields	= new PBYTE [uQty];

		for( uSel = uStart; uSel < uQty; uSel++ ) {	// make field select data

			m_pVI[uSel] = new VARINFO;

			m_pVarInfo  = m_pVI[uSel];

			m_pVarInfo->pData = Data;

			if( !(BOOL)pL->pItemBad[uSel] ) {

				*m_pVarInfo->pData  = pL->pListData[uSel];
				m_pVarInfo->Logical = !pL->pListType[uSel];
				m_pVarInfo->MSDAddr = pL->pMSDAddr[uSel];
				m_pVarInfo->NameInx = m_pwNameIndex[pL->pIndex[uSel]];

				PBYTE pTemp	= new BYTE [MAXSEND];

				m_pTx		= pTemp;

				m_uTPtr		= 0;

				UINT uDataSize	= AddFieldSels();

				uFrameSize	+= uDataSize;

				if( uSel >= u1stErr ) {

					uFrameSize++;

					if( uSel == u1stErr ) uFrameSize += uSel;
					}

				if( uFrameSize < MAXSEND ) {

					pFields[uSel] = new BYTE [uDataSize];

					PBYTE pF = pFields[uSel];

					memcpy(pF, pTemp, uDataSize);

					m_pVarInfo->uFieldSize = uDataSize;

					uFieldsQty++;

					uActualQty++;
					}

				else {
					uContinue = uSel - uStart + 1;	// start for continuation read
					}

				delete pTemp;
				}
			}

		uQty = uActualQty;
		}

	m_pTx = m_bTx;

	AddSerialHeader();

	BOOL fErr = !(u1stErr == 256);

	m_bRER = fHasList && (BOOL)uContinue ? 1 : 0;		// Request Error Code = 1 if more needed

	if( fErr ) {

		m_bRER |= RERNOMAT;			// Request Error Code (0 if no errors)
		}

	AddByte(m_bRER);				// RER - Request Error Code Position

	AddByte(uQty);					// NME - Number of Message Elements

	AddByte(pL->Selected);				// List Number

	AddWord(fHasList ? uContinue : 0);		// Next List Entry Number (0 if end of list)

	if( fHasList ) {

		for( uSel = uStart; uSel < uQty; uSel++ ) {

			m_pVarInfo = m_pVI[uSel];

			if( fErr ) AddByte(pL->pItemBad[uSel]);

			PBYTE pF = pFields[uSel];

			memcpy(&m_pTx[m_uTPtr], pF, m_pVarInfo->uFieldSize);

			m_uTPtr += m_pVarInfo->uFieldSize;
			}

		for( uSel = 0; uSel < uFieldsQty; uSel++ ) {

			if( pFields[uSel] ) {

				delete pFields[uSel];
				}
			}

		delete pFields;
		}

	else {
		AddByte(EERIDENT);
		}

	Send();

	if( fHasList ) {

		DeleteVarInfo();

		CloseListBuffers();
		}
	}

void CBSAPExtSDriver::ReadSignal(BYTE bType)
{
// TODO - set up Read by name and read by address as separate functions, even though it takes more code.
	m_pTx = m_bTx;

	BOOL fName = (BOOL)bType;			// is read by name

	UINT uRPos = m_bFSS[0] + (fName ? 3 : 5);	// position of element count

	UINT uQty  = m_pReqList[uRPos];

	uRPos      = 0;					// first data byte

	// This code may not be correct for all units
	BOOL fPing = fName &&
		m_FuncList[3] == 0x09 &&
		m_DataList[0] == '#' &&
		m_DataList[1] == 'O' &&
		m_DataList[2] == 'N' &&
		m_DataList[3] == '.' &&
		m_DataList[4] == '.';
	// Set if Name is "ON.."
	m_pVI       = new PVARINFO [uQty];

	m_pVIData   = new DWORD    [uQty];

	m_dVIMagic  = 0x12345678;

	m_upVICount = uQty;

	UINT uSel;

	for( uSel = 0; uSel < uQty; uSel++ ) {

		UINT uLen = 2;		// assume read by address

		m_pVI[uSel]   = new VARINFO;

		m_pVarInfo  = m_pVI[uSel];

		m_pVarInfo->uDataCt = 1;

		m_pVarInfo->pData = &m_pVIData[uSel];

		BYTE bBad = 0;

		if( fName ) {

			uLen = 0;

			char *pName = (char *)&m_pDataList[uRPos];

			uLen = (strlen((const char *)pName) + 1);	// Name + Null

			m_pVarInfo->Length = uLen - 1;

			if( FindName(uRPos, pName) ) {

				bBad = LoadInfoAboutItem();

				if( bBad ) {

					m_bRER = RERMATCH;
					}
				}

			else {
				m_bRER = RERMATCH;	// name not found

				bBad   = EERIDENT;	// EER for this item
				}
			}

		else {
			m_pVarInfo->Length = 2;

			if( FindAddr(uRPos) ) {

				bBad = LoadInfoAboutItem();

				if( bBad ) {

					m_bRER = RERNOMAT;
					}
				}

			else {
				m_bRER = RERNOMAT;

				bBad   = EERIDENT;
				}
			}

		m_pVarInfo->EER = bBad;

		uRPos += uLen;	// next item position
		}

	if( !fPing ) {
	
		ConstructSignalSend(m_pVI, uQty);
		}

	else {
		m_uTPtr = 0;
		AddSerialHeader();
		AddByte(0);
		AddByte(1);
		AddByte(0x44);
		}

	if( m_uTPtr ) Send(); // There is data to send

	DeleteVarInfo();
	}

BOOL CBSAPExtSDriver::SetArrayIndex(void) {

	UINT uNum = m_ReqList[2];	// array number of request is in same location for all requests

	UINT n    = 0;

	while( n < m_bArrCnt ) {

		if( uNum == m_pArrNum[n] ) {

			RXARRAY *p = &m_RxArray;

			p->uType     = (UINT)m_pReqList[1];
			p->uNumber   = uNum;
			p->uIndex    = m_pArrInx[n];	// initial index for config lists
			p->uColumnCt = m_pArrCol[n];	// user configured array column count

			return TRUE;
			}

		n++;
		}

	return FALSE;
	}

BOOL CBSAPExtSDriver::SetShortArrayParams(void) {

	if( SetArrayIndex() ) {

		RXARRAY *p	= &m_RxArray;

		p->uStartRow	= (UINT)m_pReqList[3];
		p->uStartCol	= (UINT)m_pReqList[4];
		p->uReqCount	= (UINT)m_pReqList[5];

		p->uIndex	+= ((p->uStartRow - 1) * p->uColumnCt) + p->uStartCol - 1;

		return TRUE;
		}

	return FALSE;
	}

BOOL CBSAPExtSDriver::SetGenerArrayParams(void) {

	if( SetArrayIndex() ) {

		RXARRAY *p	= &m_RxArray;

		p->uStartRow	= (UINT)((WORD)IntelToHost(*(PU2)&m_pReqList[3]));
		p->uStartCol	= (UINT)((WORD)IntelToHost(*(PU2)&m_pReqList[5]));
		p->uReqCount	= (UINT)m_pReqList[7];

		p->uIndex	+= ((p->uStartRow - 1) * p->uColumnCt) + p->uStartCol - 1;

		return TRUE;
		}

	return FALSE;
	}

void CBSAPExtSDriver::ReadArraySize(void) {

	BOOL fOk   = FALSE;

	UINT uType = m_pReqList[1] == 1 ? 0 : 2;	// 1=want bit array, 0=want analog array
	UINT uNum  = m_pReqList[2];			// array number
	UINT uTotl = 0;
	UINT n     = 0;
	WORD wRow  = 0;
	WORD wCol  = 0;

	if( m_pCtx->m_dARMagic == 0x12345678 ) {

		while( n < m_bArrCnt ) {

			if( uNum == m_pArrNum[n] ) {

				if( uType != m_pbTypeList[m_pArrInx[n]] ) break;	// Array is of wrong data type

				if( !(BOOL)wCol && (BOOL)m_pArrCol[n] ) {

					wCol = (WORD)m_pArrCol[n];
					}

				uTotl++;
				}

			n++;
			}

		if( (BOOL)wCol ) {

			wRow = 1 + (WORD)(uTotl / wCol);

			fOk  = TRUE;
			}
		}

	m_pTx   = m_bTx;

	m_uTPtr = 0;

	AddSerialHeader();

	if( fOk ) {

		AddByte(0);	// found
		AddByte(1);	// NME = always 1
		AddByte(LOBYTE(uNum));	// Array Number
		AddWord(wRow);
		AddWord(wCol);
		AddByte(1);	// always read/write
		}

	else {
		AddByte(RERMATCH);
		AddByte(0);
		}

	Send();
	}

void CBSAPExtSDriver::ReadShortArray(void) {

	if( SetShortArrayParams() ) {

		if( ReadDefaultArray(FALSE) ) return;
		}

	SendReject();
	}

void CBSAPExtSDriver::ReadGeneralArray(void) {

	if( SetGenerArrayParams() ) {

		if( ReadDefaultArray(TRUE) ) return;
		}

	SendReject();
	}

BOOL CBSAPExtSDriver::ReadDefaultArray(BOOL fGeneral) {

	UINT uCnt = GetInitialCount();	// number of sequential items programmed

	if( uCnt == NOTHING ) return FALSE;

	RXARRAY	*p	= &m_RxArray;

	p->uItemCt = uCnt;

	m_pTx      = m_bTx;

	AddDevAndSerNum(m_pCtx->m_bSerNum);	// STX, Serial Number

	AddSerialHeader();		// SFC, AppSeq, DFC, Node Status

	CAddress Addr;

	Addr.m_Ref	= m_pdAddrList[p->uIndex];

	PDWORD pData	= new DWORD [uCnt];

	BOOL fOk	= COMMS_SUCCESS(Read(Addr, pData, uCnt));

	if( fOk	) {

		UINT uRow = 0;

		UINT uCol = 0;

		GetNextRowAndCol(&uRow, &uCol);

		if( uRow * p->uColumnCt > uCnt ) {

			uRow = 0;	// more request than available.  e.g. 15 requested, 14 available
			}

		AddByte(0);	// RER code

		AddByte(p->uReqCount);

		AddByte(p->uNumber);

		if( fGeneral ) {

			AddWord(uRow);
			AddWord(uCol);
			}

		else {
			AddByte(uRow);	// next row to request
			AddByte(uCol);	// next column to request
			}

		AddArrData(Addr.a.m_Type, pData, uCnt);

		Send();
		}

	delete pData;

	return fOk;
	}

void CBSAPExtSDriver::GetNextRowAndCol(UINT *pRow, UINT *pCol) {

	RXARRAY * p = &m_RxArray;

	UINT uCount = p->uItemCt;		// number of bits, or reals
	UINT uCol   = p->uStartCol;
	UINT uCC    = p->uColumnCt;
	BOOL fBit   = (BOOL)p->uType;

	UINT uCols  = fBit ? uCount / 8 : uCount;	// number of additional columns used

	UINT uMore  = uCol + uCols;			// if >= uCC, additional row will be needed

	UINT uRows  = uMore / uCC;			// number of additional rows used

	if( (BOOL)uRows ) {

		*pRow = p->uStartRow + uRows;

		*pCol = uMore % uCC;
		}

	else {
		*pRow = 0;
		*pCol = 0;
		}
	}

void CBSAPExtSDriver::AddArrData(UINT uType, PDWORD pData, UINT uCount) {

	UINT n = 0;

	UINT uTotal = m_RxArray.uReqCount;

	if( (BOOL)m_RxArray.uType ) {

		BYTE m  = 0x80;

		DWORD d = pData[0];

		BYTE b  = 0;

		while( n < uCount ) {

			if( (BOOL)d ) {

				b |= m;
				}

			m >>= 1;

			if( !m || n == uCount - 1) {

				AddByte( b );

				m = 0x80;
				b = 0;
				}

			n++;
			d = pData[n];
			}

		while( n < uTotal ) {

			AddByte(0);

			n += 8;
			}
		}

	else {

		while( n < uCount ) {

			AddLong(LongToReal(uType, pData[n++]));
			}

		while( n < uTotal ) {

			AddLong(0);

			n++;
			}
		}
	}

UINT CBSAPExtSDriver::GetInitialCount(void) {

	UINT uIndex = m_RxArray.uIndex;

	UINT uMaxCt = m_RxArray.uReqCount;

	if( uIndex + uMaxCt - 1 > m_uTagCount ) {

		uMaxCt = m_uTagCount - uIndex;
		}

	if( (BOOL)m_RxArray.uType ) {

		MakeMin(uMaxCt, 512);	// up to 512 bits = 32 bytes
		}

	else {
		MakeMin(uMaxCt, 20);	// up to 20 reals = 80 bytes
		}

	UINT n = 1;

	PDWORD pAdd = &m_pdAddrList[uIndex];

	UINT uAddr0 = *pAdd;

	while( n <= uMaxCt ) {

		if( pAdd[n] != uAddr0 + n ) {	// accept only n sequential items

			return n;
			}

		if( n == uMaxCt ) return n;

		n++;
		}

	return NOTHING;
	}

void CBSAPExtSDriver::CopyHeaderForResp(void) {

	AddDevAndSerNum(m_pCtx->m_bSerNum);

	UINT uPos = m_fGlobal ? (UINT)RXGDFC : (UINT)RXDFC;

	AddByte(m_bRx[uPos + 3]);	// copy received source funtion to destination fc
	AddByte(m_bRx[uPos + 1]);	// copy received App Seq number byte
	AddByte(m_bRx[uPos + 2]);	// copy received App Seq number byte
	AddByte(m_bRx[uPos]);		// copy received destination function code to source fc
	AddByte(0);			// node status
	}

// Write routines
void CBSAPExtSDriver::WriteByName(void)
{
	m_pTx		= m_bTx;

	m_bRER		= 0;

	UINT uQty	= m_pReqList[2];

	UINT uRPos	= 0;	// position of first name

	m_pVI		= new PVARINFO [uQty];

	m_dVIMagic	= 0x12345678;

	PDWORD pData	= new DWORD [uQty];

	m_upVICount	= uQty;

	UINT uECt	= 0;

	UINT uSel;

	for( uSel = 0; uSel < uQty; uSel++ ) {

		m_pVI[uSel] = new VARINFO;

		m_pVarInfo  = m_pVI[uSel];

		m_pVarInfo->pData = &pData[uSel];

		char *pName = (char *)&m_pDataList[uRPos];

		UINT uNameLength   = (strlen((const char *)pName));

		m_pVarInfo->Length = uNameLength;

		if( !FindName(uRPos, pName) ) {

			m_bRER = RERMATCH;

			m_pVarInfo->EER = RERMATCH;

			uECt++;
			}

		else {
			m_pVarInfo->EER = 0;

			uRPos += uNameLength + 1;	// point to descriptor

			BYTE bDesc = m_pDataList[uRPos++];

			if( !SetWriteData(uRPos, &pData[uSel], bDesc) ) {

				uQty   = 0;
				}

			else {
				if( bDesc < 9 ) {

					m_pVarInfo->EER = 0xAA;
					}

				else {
					*m_pVarInfo->pData = pData[uSel];
					}
				}
			}

		uRPos += GetNextWriteItem(uRPos);	// RPos points to start of data
		}

	ExecuteWrite(m_pVI, uQty);

	ConstructWriteReply(m_pVI, uQty);

	delete pData;

	m_pVarInfo->pData = NULL;

	DeleteVarInfo();

	Send();
	}

void CBSAPExtSDriver::WriteByAddress(void)
{
	m_pTx = m_bTx;

	m_pCtx->m_uMSDVers = (m_pReqList[1] << 16) + m_pReqList[2];

	UINT uQty  = m_pReqList[4];

	UINT uRPos = 0;	// position of first address

	m_pVI = new PVARINFO [uQty];

	m_dVIMagic = 0x12345678;

	PDWORD pData = new DWORD [uQty];

	m_upVICount = uQty;

	UINT uECt = 0;

	UINT uSel;

	for( uSel = 0; uSel < uQty; uSel++ ) {

		m_pVI[uSel] = new VARINFO;

		m_pVarInfo  = m_pVI[uSel];

		m_pVarInfo->pData = &pData[uSel];

		if( !FindAddr(uRPos) ) {

			m_bRER = RERMATCH;

			m_pVarInfo->EER = RERMATCH;

			uECt++;
			}

		else {
			m_pVarInfo->EER = 0;

			uRPos += 2;	// point to descriptor

			BYTE bDesc = m_pDataList[uRPos++];

			if( !SetWriteData(uRPos, &pData[uSel], bDesc) ) {

				uQty = 0;
				}

			else {
				if( bDesc < 9 ) {

					m_pVarInfo->EER = 0xAA;
					}

				else {
					*m_pVarInfo->pData = pData[uSel];
					}
				}
			}

		uRPos += GetNextWriteItem(uRPos);
		}

	ExecuteWrite(m_pVI, uQty);

	ConstructWriteReply(m_pVI, uQty);

	delete pData;

	m_pVarInfo->pData = NULL;

	DeleteVarInfo();

	Send();
	}

void CBSAPExtSDriver::WriteByList(BOOL fFirst) {
	
	DWORD d = 0;

	switch( m_pDataList[0] ) {

		case 0x9: d = 1; break;
		case 0xA: d = 0; break;
		case 0xB: d = (DWORD)IntelToHost(*(PU4)&m_pDataList[1]); break;
		default: SendNak(); return;
		}

	UINT uListNum	= m_pReqList[4];

	UINT uItem	= fFirst ? 1 : (WORD)IntelToHost(*(PU2)&m_pReqList[5]);

	UINT k = 0;

	while( k < m_pListCntl->TotalSize ) {

		WORD wSel = LOWORD(m_pdLNumList[k]);

		if( HIBYTE(wSel) != uListNum || LOBYTE(wSel) != uItem ) {

			k++;

			continue;
			}

		break;		// requested list item found
		}

	UINT uIndex = (UINT)HIWORD(m_pdLNumList[k]);

	CAddress Addr;

	Addr.m_Ref = m_pdAddrList[k];

	d = RealToLong(Addr.a.m_Type, d);

	BYTE bErr = COMMS_SUCCESS(Write(Addr, &d, 1)) ? 0 : 0x84;

	AddSerialHeader();

	AddByte( bErr );

	AddByte(1);

	AddByte( bErr );

	Send();
	}

void CBSAPExtSDriver::WriteShortArray(void) {

	if( SetShortArrayParams() ) {

		if( WriteDefaultArray() ) return;
		}

	SendReject();
	}

void CBSAPExtSDriver::WriteGeneralArray(void) {

	if( SetGenerArrayParams() ) {

		if( WriteDefaultArray() ) return;
		}

	SendReject();
	}

BOOL CBSAPExtSDriver::WriteDefaultArray(void) {

	UINT uCnt = GetInitialCount();

	if( uCnt == NOTHING ) return FALSE;

	RXARRAY *p  = &m_RxArray;

	p->uItemCt  = uCnt;

	UINT uIndex = p->uIndex;

	if( MakeArrayWrite((BOOL)p->uType) ) {

		m_pTx = m_bTx;

		AddDevAndSerNum(m_pCtx->m_bSerNum);

		AddSerialHeader();

		AddByte(0);

		Send();

		return TRUE;
		}

	SendReject();

	return FALSE;
	}

BOOL CBSAPExtSDriver::MakeArrayWrite(BOOL fIsBit) {

	CAddress Addr;

	RXARRAY * p  = &m_RxArray;

	UINT uCount  = p->uItemCt;

	UINT uIndex  = p->uIndex;

	PDWORD pAdd  = &m_pdAddrList[uIndex];

	Addr.m_Ref   = *pAdd;

	PDWORD pData = new DWORD [uCount];

	UINT n = 0;

	if( fIsBit ) {

		PBYTE pSB = m_pDataList;

		while( n < uCount ) {

			BYTE bMask = 0x80;

			BYTE bData = *pSB++;

			for( UINT k = 0; k < 8; k++, pAdd++, bMask >>= 1 ) {

				Addr.m_Ref = *pAdd;

				if( !(bMask & bData) ) pData[n] = 0; /*!!!CHECK!!!*/

				else *pData = Addr.a.m_Type == addrBitAsBit ? 0xFFFFFFFF : 0x3F800000;
				}

			n += 8;
			}
		}

	else {
		PDWORD pSD = (PDWORD)m_pDataList;

		while( n < uCount ) {

			Addr.m_Ref = *pAdd++;

			DWORD d = (DWORD)IntelToHost(*pSD);

			if( Addr.a.m_Type == addrRealAsReal ) pData[n] = d;

			else pData[n] = (BOOL)d ? 0xFFFFFFFF : 0;

			pSD++;

			n++;
			}
		}

	Addr.m_Ref = m_pdAddrList[uIndex];

	BOOL fOk = COMMS_SUCCESS( Write(Addr, pData, uCount) );

	delete pData;

	return fOk;
	}

BOOL CBSAPExtSDriver::SetWriteData(UINT uPos, PDWORD pData, BYTE bDesc)
{
	if( FormWriteData(uPos, pData, bDesc) ) {

		return TRUE;
		}

	m_pVarInfo->EER = RERMATCH;

	m_bRER = RERINVDB;

	return FALSE;
	}

BOOL CBSAPExtSDriver::FormWriteData(UINT uPos, PDWORD pData, BYTE bDesc) {

	m_pVarInfo->uDataCt = 1;

	if( (BOOL)bDesc && bDesc < 9 ) {

		StoreSignalControls(bDesc);
		}

	else {
		switch( bDesc ) {

			case  9:
				*pData = m_pVarInfo->Logical ? 0xFFFFFFFF : 0x3F800000;
				return TRUE;

			case 10:
				*pData = 0;
				return TRUE;

			case 11:	// analog
				*pData = (DWORD)IntelToHost(*(PU4)&m_pDataList[uPos]);
				return TRUE;

			case 13:	// string
				HandleString(uPos, pData);
				return TRUE;

			case 14:
			case 15:	// security
				HandleSecurity(m_pDataList[uPos], bDesc == 14);
				return TRUE;

			default:
				return FALSE;
			}
		}

	return FALSE;
	}

void CBSAPExtSDriver::ExecuteWrite(PVARINFO *pVI, UINT uQty)
{
	for( UINT uSel = 0; uSel < uQty; uSel++ ) {

		m_pVarInfo = pVI[uSel];

		if( !m_pVarInfo->EER ) {

			CAddress Addr;

			Addr.m_Ref  = m_pVarInfo->Addr;

			DWORD Data[1];

			Data[0] = RealToLong(Addr.a.m_Type, *m_pVarInfo->pData);

			UINT uC = m_pVarInfo->uDataCt;

			if( !COMMS_SUCCESS(Write(Addr, Data, m_pVarInfo->uDataCt)) ){

				m_pVarInfo->EER = EEREND_D;
				}
			}

		else {
			if( m_pVarInfo->EER == 0xAA ) {		// control byte was saved, eer was 0

				m_pVarInfo->EER = 0;
				}
			}
		}
	}

void CBSAPExtSDriver::StoreSignalControls(BYTE bDesc) {

	}

void CBSAPExtSDriver::HandleArrays(BYTE bType, PDWORD pData, UINT uDataPos) {


	}

void CBSAPExtSDriver::HandleString(UINT uDataPos, PDWORD pData) {

	}

void CBSAPExtSDriver::HandleSecurity(BYTE bData, BOOL fRead) {

	PBYTE p	= &m_pbSecList[m_pVarInfo->Index];

	BYTE b	= (*p) & (fRead ? 0xF0 : 0x0F);

	b	|= fRead ? bData & 0xF : bData & 0xF0;

	*p	= b;
	}

UINT CBSAPExtSDriver::GetNextWriteItem(UINT uPos) {

	switch( m_uReqType ) {

		case OFCWARRS:
		case OFCWARRG:
			return GetWriteArraySize(m_pReqList[5]); // quantity
		}

	BYTE bDesc = m_pDataList[uPos - 1];			// write descriptor for signals

	switch( bDesc ) {

		case 11:
			return 4;				// analog value

		case 13:					// string
			const char *pStr;

			pStr   = (const char *)&m_pDataList[uPos];

			return strlen(pStr) + 1;

		case 14:
		case 15:
			return 1;				// security code change
		}

	return 0;						// all others
	}

UINT CBSAPExtSDriver::GetWriteArraySize(UINT uQty) {

	return (BOOL)m_pDataList[1] ? uQty : uQty * 4;
	}

// NRT/TMS Routines

void CBSAPExtSDriver::RequestNRT(void) {

//**/	AfxTrace0("\r\n\nRequestNRT");

	m_pTx	= m_bNRTTx;

	m_uTPtr	= 0;

	m_bRx[0] = 0;

	m_pCtx->m_bSerNum = m_bRx[RXSERN];

	UINT u = m_pCtx->m_uAppSeq += m_pCtx->m_uAppSeq >= 0xFFFF ? 2 : 1;

	AddByte(0);
	AddByte(m_pCtx->m_bSerNum);
	AddByte(MFCNRTREQ);
	AddByte(LOBYTE(m_pCtx->m_uAppSeq));
	AddByte(HIBYTE(m_pCtx->m_uAppSeq));
	AddByte(MFCNRTMSG);
	AddByte(0);
	AddByte(m_BDrvCfg.ThisLocal);
	AddByte(0x1);

	Send();
	}

void CBSAPExtSDriver::ReceiveNRT(void) {

	if( m_bRx[RXDADD] == m_BDrvCfg.ThisLocal ) {

		m_pCtx->m_bSerNum = m_bRx[RXSERN];

		m_pCtx->m_uAppSeq = (WORD)IntelToHost(*(PU2)&m_bRx[RXLSQL]);

		PBYTE p    = &m_bRx[RXLDAT0];
		PBYTE pNRT = (PBYTE)&m_pNRTRx;

		memcpy(pNRT, p, 32);

		m_uHasNRT = 10;

		m_pNRTRx->Level++;

		UINT uLevelShift = m_pNRTRx->LSC[m_pNRTRx->Level];

		WORD wLocAddr = ((WORD)m_bRx[RXDADD] << uLevelShift);

		WORD wNRTItem = wLocAddr | (WORD)IntelToHost(*(PU2)&m_pNRTRx->GlobAddrLo);

		m_pNRTTx->GlobAddrHi = HIBYTE(wNRTItem);
		m_pNRTTx->GlobAddrLo = LOBYTE(wNRTItem);

		wNRTItem = (WORD)IntelToHost(*(PU2)&m_pNRTRx->UpDnMaskLo);

		m_pNRTTx->UpDnMaskHi = HIBYTE(wNRTItem);
		m_pNRTTx->UpDnMaskLo = LOBYTE(wNRTItem);
		}
	}

void CBSAPExtSDriver::RequestTMS(void) {

//**/	AfxTrace0("\r\nRequestTMS");

	m_pTx = m_bTMSTx;

//	MakeTMSSendHeader();

	Send();
	}

void CBSAPExtSDriver::WriteNRT(void) {

	if( m_fIsThis ) {
		
		CAddress Addr;
		Addr.a.m_Table	= SPNRT0;
		Addr.a.m_Type	= addrLongAsLong;
		Addr.a.m_Extra	= 0;
		Addr.a.m_Offset	= 0x8000;

		DWORD Data[8];

		PDWORD p = new DWORD [8];

		memcpy(p, &m_bRx[RXLDAT0], 32);

		for( UINT n = 0; n < 8; n++ ) {

			Data[n] = (DWORD)IntelToHost(p[n]);
			}

		memcpy(m_pNRTRx, p, 32);

		delete p;

		Write(Addr, Data, 8);

		m_uHasNRT = 10;
		}
	}

void CBSAPExtSDriver::SaveNRT(void) {

//**/	AfxTrace0("\r\nSaveNRT");

	PBYTE p = &m_bRx[14];

	NRTFRAME      *N = m_pNRTRx;

	N->Version	= *p++;
	N->GlobAddrLo	= *p++;
	N->GlobAddrHi	= *p++;
	N->UpDnMaskLo	= *p++;
	N->UpDnMaskHi	= *p++;
	N->Level	= *p++;

	for( UINT k = 0; k < 7; k++ ) {

		N->LSC[k] = *p++;
		N->LBM[k] = *p++;
		}
	}

void CBSAPExtSDriver::SaveTMS(void) {

//**/	AfxTrace0("\r\nSaveTMS");

	PBYTE p = &m_bRx[11];

	TIMESYNCFRAME *T = &m_pNRTTx->TSF;

	T->Day		= *p++;
	T->Month	= *p++;
	T->YearLo	= *p++;
	T->YearHi	= *p++;
	T->Hour		= *p++;
	T->Minute	= *p++;
	T->Second	= *p++;
	T->JulianDayLo	= *p++;
	T->JulianDayHi	= *p++;
	T->Julian4SecLo	= *p++;
	T->Julian4SecHi	= *p++;
	T->Julian20mSec	= *p++;
	}

void CBSAPExtSDriver::GetNRT(void) {

	}

void CBSAPExtSDriver::GetTMS(void) {

	MakeTMSRespHeader();

	TIMESYNCFRAME *pT = &m_TMSFrame;

	AddByte(pT->Day);
	AddByte(pT->Month);
	AddByte(pT->YearLo);
	AddByte(pT->YearHi);
	AddByte(pT->Hour);
	AddByte(pT->Minute);
	AddByte(pT->Second);
	AddByte(pT->JulianDayLo);
	AddByte(pT->JulianDayHi);
	AddByte(pT->Julian4SecLo);
	AddByte(pT->Julian4SecHi);
	AddByte(pT->Julian20mSec);

	Send();
	}

void CBSAPExtSDriver::RespondNRT(void) {

//**/	AfxTrace0("\r\nRespondNRT");

	NRTFRAME *pN = m_pNRTTx;

	AddByte(pN->Version);
	AddByte(pN->GlobAddrLo);
	AddByte(pN->GlobAddrHi);
	AddByte(pN->UpDnMaskLo);
	AddByte(pN->UpDnMaskHi);
	AddByte(pN->Level);

	for( UINT k = 0; k < 7; k++ ) {

		AddByte(pN->LSC[k]);
		AddByte(pN->LBM[k]);
		}

	Send();
	}

void CBSAPExtSDriver::MakeNRTSendHeader(void) {

	m_pCtx->m_bSerNum += m_pCtx->m_bSerNum == 0xFF ? 2 : 1;	// new serial number for slave generated request

	AddDevAndSerNum(m_pCtx->m_bSerNum);				// if !fReq, sernum updated by master's received request

	AddByte(MFCRDBACC);

	++m_pCtx->m_uAppSeq;					// new sequence number, also

	if( !(BOOL)m_pCtx->m_uAppSeq ) m_pCtx->m_uAppSeq = 1;

	AddWord(m_pCtx->m_uAppSeq);

	AddByte(MFCNRTREQ);

	AddByte(0);

/*	// using Extended functions
	AddByte(MFCRDBACC);

	++m_pCtx->m_uAppSeq;					// new sequence number, also

	if( !(BOOL)m_pCtx->m_uAppSeq ) m_pCtx->m_uAppSeq = 1;

	AddWord(m_pCtx->m_uAppSeq);

	AddByte(MFCCOMREQ);					// set source code for received frame

	AddByte(0);						// Node Status

	// end of 7 byte local header
	// add operational function codes

	AddByte(OFCREQNRT);					// Operational Function code

	AddByte(OFCEXTREQ);					// Extended Request Function Code

	AddByte(OFCEXTFC);					// sub function code
*/
	}

void CBSAPExtSDriver::MakeNRTRespHeader(void) {

	// add 7 byte local header
	m_pCtx->m_bSerNum = m_bRx[1];

	CopyHeaderForResp();

	// add operational function codes
	AddWord(0);						// Primary and Secondary Error Status
	AddWord(m_pCtx->m_uACCOLVers);
	AddByte(OFCREQNRT);
	AddByte(OFCEXTREQ);
	AddByte(OFCEXTFC);
	}

void CBSAPExtSDriver::MakeTMSSendHeader(void) {

	m_pCtx->m_bSerNum += m_pCtx->m_bSerNum == 0xFF ? 2 : 1;	// new serial number for slave generated request

	AddDevAndSerNum(m_pCtx->m_bSerNum);				// if !fReq, sernum updated by master's received request

	AddByte(MFCEXTACC);					// request NRT or Date/Time from destination

	++m_pCtx->m_uAppSeq;					// new sequence number, also

	if( !(BOOL)m_pCtx->m_uAppSeq ) m_pCtx->m_uAppSeq = 1;

	AddWord(m_pCtx->m_uAppSeq);

	AddByte(MFCEXTACC);					// set source code for received frame

	AddByte(0);						// Node Status

	// end of 7 byte local header
	// add operational function codes

	AddByte(OFCEXTFC);					// Date/Time Extended Request Function Code

	AddByte(OFCEXTSFC);					// sub function code
	}

void CBSAPExtSDriver::MakeTMSRespHeader(void) {	// return Date/Time data

	// add 7 byte local header
	CopyHeaderForResp();

	AddWord(0);						// Primary and Secondary Error Status

	// add operational function codes
	AddByte(OFCEXTFC);			// Date/Time Extended Request Function Code
	AddByte(OFCEXTSFC);			// sub function code
	}

// Item info
BOOL CBSAPExtSDriver::FindName(UINT uPos, char *pSrce)
{
	UINT uLen   = m_pVarInfo->Length;

	UINT uIndex = 0;

	while( uIndex < m_uTagCount ) {

		WORD wPos1 = m_pwNameIndex[uIndex];

		const char * pPos = (const char *)&m_pbNameList[wPos1];

		if( strlen(pPos) == uLen ) {

			UINT v = strnicmp(pSrce, pPos, uLen);

			if( !v ) {

				GetVarInfo(uIndex);

				return TRUE;
				}
			}

		uIndex++;
		}

	return FALSE;
	}

BOOL CBSAPExtSDriver::FindAddr(UINT uPos)
{
	WORD wAdd   = (WORD)IntelToHost(*(PU2)&m_pDataList[uPos]);

	UINT uIndex = 0;

	while( uIndex < m_uTagCount ) {

		if( m_pwMSDList[uIndex] == wAdd ) {

			GetVarInfo(uIndex);

			return TRUE;
			}

		uIndex++;
		}

	return FALSE;
	}

BYTE CBSAPExtSDriver::LoadInfoAboutItem(void)
{
	if( GetReadInfo(m_pVarInfo->Index) ) {

		return 0;
		}

	return EEREND_D;
	}

BOOL CBSAPExtSDriver::GetReadInfo(UINT uIndex)
{
	DWORD d = m_pdAddrList[uIndex];

	CAddress A;

	A.m_Ref = d;

	UINT uQty    = m_pVarInfo->uDataCt;

	PDWORD pData = new DWORD [uQty];

	if( COMMS_SUCCESS(Read(A, pData, uQty)) ) {

		FillInfo(uIndex, pData);

		delete pData;

		pData = NULL;

		return TRUE;
		}

	delete pData;

	return FALSE;
	}

void CBSAPExtSDriver::FillInfo(UINT uIndex, PDWORD pData)
{
	CAddress Addr;

	Addr.m_Ref		= m_pdAddrList[uIndex];

	m_pVarInfo->Addr	= Addr.m_Ref;
	m_pVarInfo->Logical	= !(BOOL)Addr.a.m_Type;
	m_pVarInfo->Offset	= Addr.a.m_Offset;
	m_pVarInfo->NameInx	= m_pwNameIndex[uIndex];

	for( UINT i = 0; i < m_pVarInfo->uDataCt; i++ ) {

		m_pVarInfo->pData[i] = LongToReal(Addr.a.m_Type, pData[i]);
		}
	}

void CBSAPExtSDriver::DeleteVarInfo(void) {

	if( m_dVIMagic != 0x12345678 ) return;

	m_dVIMagic = 0;

	if( m_pVI ) {

		for( UINT i = 0; i < m_upVICount; i++ ) {

			VARINFO *p = m_pVI[i];

			if( p ) delete m_pVI[i];

			m_pVI[i] = NULL;
			}

		delete m_pVI;
		}

	if( m_pVIData ) delete m_pVIData;

	m_pVarInfo = NULL;

	m_pVIData = NULL;
	m_upVICount = 0;
	}

// Read Helpers
void CBSAPExtSDriver::ConstructSignalSend(PVARINFO *pVI, UINT uQty)
{
	BOOL fAddEER = (BOOL)m_bRER;

	UINT uRERPos = AddSerialHeader();

	AddByte(m_bRER);

	AddByte(LOBYTE(uQty));

	BOOL fBuffFull = FALSE;

	for( UINT uSel = 0; uSel < uQty && !fBuffFull; uSel++ ) {

		m_pVarInfo = pVI[uSel];

		if( fAddEER ) AddByte(m_pVarInfo->EER);

		if( !m_pVarInfo->EER ) {		// this item is ok

			AddFieldSels();
			}

		if( m_uTPtr > MAXSEND ) {		// running out of buffer space

			uQty = uSel + 1;		// this will be the last one

			fBuffFull = TRUE;

			m_pTx[uRERPos]     |= 1;	// not all data fits in this response
			}
		}

	m_pTx[uRERPos + 1]  = uQty;			// final item quantity for packet
	}

void CBSAPExtSDriver::GetVarInfo(UINT uIndex)
{
	DWORD d = m_pdAddrList[uIndex];

	m_pVarInfo->Addr	= d;

	m_pVarInfo->Logical	= !m_pbTypeList[uIndex];

	m_pVarInfo->NameInx	= m_pwNameIndex[uIndex];

	m_pVarInfo->Offset	= LOWORD(d);

	m_pVarInfo->MSDAddr	= m_pwMSDList[uIndex];

	m_pVarInfo->Index	= uIndex;

	m_pVarInfo->EER		= 0;
	}

// Write Helpers
void CBSAPExtSDriver::ConstructWriteReply(PVARINFO *pVI, UINT uQty)
{
	AddSerialHeader();

	AddByte(m_bRER);

	AddByte(uQty);

	PVARINFO p;

	for( UINT uSel = 0; uSel < uQty; uSel++ ) {

		p = pVI[uSel];

		AddByte(p->EER);
		}
	}

// Construct Frame
void CBSAPExtSDriver::AddDevAndSerNum(BYTE bSerial) {

	m_pData->ClearRx();

	m_uTPtr = 0;
	AddByte(0 | (m_bRx[0] & 0x80));		// Master device number
	AddByte(bSerial);			// if response, bSerial comes from command, else next serial number
	}

UINT CBSAPExtSDriver::AddSerialHeader(void)
{
	AddDevAndSerNum(m_pHeadList[1]);

	m_pCtx->m_bSerNum += m_pCtx->m_bSerNum == 0xFF ? 2 : 1;

	if( m_fGlobal ) {		// Global (12 byte) Header

		AddByte(m_pHeadList[4]);
		AddByte(m_pHeadList[5]);
		AddByte(m_pHeadList[2]);
		AddByte(m_pHeadList[3]);
		AddByte(m_pHeadList[6]);
		}

	AddSerialFunc();

	return m_fGlobal ? 12 : 7;	// position of RER
	}

void CBSAPExtSDriver::AddSerialFunc(void)
{
	AddByte(m_pFuncList[3]);	// SrceFC->DestFC);	// exchange SrceFC & DestFC for response
	AddByte(m_pFuncList[1]);	// AppSeqL);
	AddByte(m_pFuncList[2]);	// AppSeqH);
	AddByte(m_pFuncList[0]);	// DestFC->SrceFC);
	AddByte(0);			// NodeST);
	}

// Field Select Helpers
BOOL CBSAPExtSDriver::ParseVarName(PCTXT Name, UINT uSelect, PBYTE pItem)
{
	UINT uNameLen = strlen((const char *)Name);

	memset(pItem, 0, uNameLen);

	for( UINT i = 0, j = 0; i < uNameLen; i++ ) {

		BYTE b = Name[i];

		if( !uSelect ) {

			if( b == '.' ) return (BOOL)pItem[0];

			pItem[j++] = b;
			}

		else {
			if( b == '.' ) uSelect--;
			}

		return (BOOL)pItem[0];
		}

	return FALSE;
	}

void CBSAPExtSDriver::SetFieldSelects()
{
	BYTE b = m_pReqList[1] & 0x7; // bit 4 is not defined in spec

	if( !b ) return;

	memset(m_bFSS, 0, 5);

	PBYTE p = &m_pReqList[2];

	BYTE bCount = 1;

	while( b & 1 ) {

		m_bFSS[bCount] = *p++;

		bCount++;

		b >>= 1;
		}

	m_bFSS[0] = bCount - 1;
	}

UINT CBSAPExtSDriver::AddFieldSels(void)
{
	WORD wPkt0 = m_uTPtr;

	UINT uCount = AddField1();
	uCount     += AddField2();
	uCount     += AddField3();

	return uCount;
	}

UINT CBSAPExtSDriver::AddField1(void)
{
	VARINFO *p  = m_pVarInfo;

	UINT uStart = m_uTPtr;

	BYTE b      = m_bFSS[1];

	if( b ) {

		DWORD d    = p->pData[0];

		BOOL fLog  = p->Logical;
		BOOL fTrue = (BOOL)(d);
		BOOL fASt  = b & 0x04;
		BYTE x     = fASt ? 0x44 : 0;	// alarm status requested, set A Inhibit/Is Alarm flag

		if( b & 0x80 ) {

			AddByte(x | (fLog ? 0 : 2));	// Alarm Stat | logical or analog
			}

		if( b & 0x40 ) {

			if( fLog ) AddByte(fTrue ? 1 : 0);	// digital value

			else AddLong(d);			// analog value
			}

		if( b & 0x20 ) {				// Units Text = 6 spaces
			AddLong(0x20202020);
			AddWord(0x2020);
			}

		if( b & 0x10 ) AddWord(p->MSDAddr);		// Msd Address

		if( b & 0x08 ) {
			const char * pN = (const char *)&m_pbNameList[p->NameInx];

			UINT uLen = strlen(pN);

			AddTextPlusNull((PCTXT)pN);
			}

		if( b & 0x04 ) {
			AddByte(0);			// Alarm Status
			if( !fLog ) AddByte(0);		// Analog alarm - nothing to report
			}

		if( b & 0x02 ) {				// Descriptor = 1 char

			AddByte('?');
			AddByte(0);
			}

		if( b & 0x01 ) {

			if( fLog ) {

				AddByte('O');
				AddByte('N');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				AddByte('O');
				AddByte('F');
				AddByte('F');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				}
			}
		}

	return m_uTPtr - uStart;
	}

UINT CBSAPExtSDriver::AddField2(void)
{
	VARINFO *p  = m_pVarInfo;

	UINT uStart = m_uTPtr;

	BYTE b      = m_bFSS[2];

	if( b ) {

		if( b & 0x80 ) AddByte(0);
		if( b & 0x40 ) AddByte(0);

		if( b & 0x38 ) {

			PBYTE pName = &m_pbNameList[p->NameInx];

			PBYTE pItem = new BYTE [strlen((const char *)pName)];

			UINT uSkip  = pName[0] == '@' ? 1 : 0;

			if( b & 0x20 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			uSkip++;

			if( b & 0x10 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			uSkip++;

			if( b & 0x08 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			delete pItem;

			pItem = NULL;
			}

		if( b & 0x04) AddWord(0);
		if( b & 0x02) AddWord(0);
		if( b & 0x01) AddWord(0x4CD5);//m_wVersion);
		}

	return m_uTPtr - uStart;
	}

UINT CBSAPExtSDriver::AddField3(void)
{
	VARINFO *p  = m_pVarInfo;

	UINT uStart = m_uTPtr;

	BYTE b      = m_bFSS[3];

	if( b > 1) {

		if( b & 0x80 ) AddWord(0);
		if( b & 0x40 ) AddWord(0);
		if( b & 0x20 ) AddWord(0);
		if( b & 0x10 ) AddWord(0);
		if( b & 0x08 ) AddByte(0);
		if( b & 0x04 ) AddByte(0);

		if( b & 0x02 ) {

			if( p->Logical ) AddByte(p->pData[0] ? 1 : 0);

			else AddLong(p->pData[0]);
			}

		if( b & 0x01 ) AddWord(0);
		}

	return m_uTPtr - uStart;
	}

// Frame Building

void CBSAPExtSDriver::StartFrame(void)
{
	m_pSend[0]  = DLE;

	m_pSend[1]  = STX;

	m_uTPtr = 2;
	}

void CBSAPExtSDriver::EndFrame(void)
{
	m_pSend[m_uTPtr++] = DLE;

	m_pSend[m_uTPtr++] = ETX;

	AddCRC();
	}

void CBSAPExtSDriver::AddCRC(void)
{
	BYTE crc_l = 0;
	BYTE crc_h = 0xFF;

	BOOL bUseDLE = FALSE;
	BOOL bSkip   = FALSE;

	for(UINT i = 2; i < m_uTPtr; i++ ) { // skip DLE, STX

		BYTE b = m_pSend[i];

		bSkip  = b == DLE && !bUseDLE; // skip 1st DLE if present

		if( !bSkip ) {

			UINT TPos  = 2 * (b ^ crc_l);

			crc_l = (crc_h ^ crctbl[TPos]);
			crc_h = crctbl[TPos+1];

			bUseDLE = FALSE;
			}

		else bUseDLE = TRUE;
		}

	m_pSend[m_uTPtr++] = ~crc_l;
	m_pSend[m_uTPtr++] = crc_h;
	}

void CBSAPExtSDriver::AddByte(BYTE bData)
{
	m_pTx[m_uTPtr++] = bData;
	}

void CBSAPExtSDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CBSAPExtSDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CBSAPExtSDriver::AddReal(DWORD dwData)
{
	AddLong(dwData);
	}

void CBSAPExtSDriver::AddText(PCTXT pText)
{
	for( UINT i = 0; i < strlen(pText); i++ ) {

		AddByte(pText[i]);
		}
	}

void CBSAPExtSDriver::AddTextPlusNull(PCTXT pText)
{
	AddText(pText);
	AddByte(0);
	}

// Transport Layer

void CBSAPExtSDriver::Send(void)
{
	PBYTE pTx  = m_pTx;

	m_pSend    = new BYTE [sizeof(m_bTx)];

	if( m_pSend ) m_dSendMagic = 0x12345678;

	else return;

	UINT uEnd  = m_uTPtr;	// end of frame

	StartFrame();

	for( UINT i = 0, j = 2; i < uEnd; i++, j++ ) {

		BYTE b = pTx[i];

		if( b == DLE ) {

			m_pSend[j++] = DLE;
			}

		m_pSend[j] = b;
		}

	m_uTPtr = j;

	EndFrame();

	SendFrame();

	delete m_pSend;

	m_dSendMagic = 0;

	m_pSend = NULL;
	}

void CBSAPExtSDriver::SendFrame(void)
{
//**/	AfxTrace0("\r\nSlave Send (%d) "); for( UINT k = 0; k < m_uTPtr; k++ ) AfxTrace1("[%2.2x]", m_pSend[k]);

	m_pData->Write((PCBYTE)m_pSend, m_uTPtr, FOREVER);
	}

void CBSAPExtSDriver::SendReject(void) {

	m_pTx = m_bPollTx;

	AddDevAndSerNum(m_pHeadList[1]);
	AddSerialFunc();
	AddByte(RERMATCH);
	Send();
	}

// Ack
void CBSAPExtSDriver::SendAckNak(BYTE bType)
{
	m_pTx = m_bPollTx;

	AddDevAndSerNum(m_pHeadList[1]);
	AddByte(bType);
	AddByte(m_BDrvCfg.ThisLocal);
	AddByte(0);
	AddByte(1);

	Send();
	}

void CBSAPExtSDriver::SendPollAck(void)
{
	m_pTx = m_bPollTx;

	AddDevAndSerNum(PollReceived.MsgSer);
	AddByte(ACKPOLL);
	AddByte(m_BDrvCfg.ThisLocal);
	AddByte(0);
	AddByte(1);

	Send();
	}

void CBSAPExtSDriver::SendDownAck(void)
{
	SendAckNak(ACKDOWN);
	}

void CBSAPExtSDriver::SendAlarmAck(void)
{
	SetAlarmAck();

	AddByte(0);	// RER

	UINT uEnd = 9 + ((UINT)m_bRx[9] * 2);

	for( UINT n = 7; n <= uEnd; n++ ) {

		AddByte(m_bRx[n]);
		}

	Send();
	}

void CBSAPExtSDriver::SendAlarmInitAck(void) {

	SetAlarmAck();
	Send();
	}

void CBSAPExtSDriver::SetAlarmAck(void) {

	m_pTx = m_bPollTx;

	AddDevAndSerNum(m_bRx[1]);	// echo serial
	AddByte(m_bRx[5]);	// DFC->SFC
	AddByte(m_bRx[3]);	// App Seq Lo
	AddByte(m_bRx[4]);	// App Seq Hi
	AddByte(m_bRx[2]);	// SFC->DFC
	AddWord(0);		// Node Status and RER
	}

void CBSAPExtSDriver::SendNak(void)
{
	SendAckNak(NAKDATA);
	}

BOOL CBSAPExtSDriver::CheckNak(UINT uVal, UINT uFail) {

	if( uVal == uFail ) {

		SendNak();
		return TRUE;
		}

	return FALSE;
	}

void CBSAPExtSDriver::SendDiscard(BYTE bSerNum) {

	m_pTx   = m_bPollTx;

	m_uTPtr = 0;

	AddByte(0);
	AddByte(bSerNum);
	AddByte(ACKDSCD);
	AddByte(m_BDrvCfg.ThisLocal);
	AddByte(0x0);
	AddByte(0x3);

	Send();
	}

// RER
void CBSAPExtSDriver::SetRER(BYTE bRER)
{
	BYTE b = m_pTx[TXRERPOS];

	m_pTx[TXRERPOS] = b ? RERMORE : bRER;
	}

// Helpers

void CBSAPExtSDriver::InitGlobal(BOOL fInit)
{
	if( fInit ) {

		m_pListCntl	= &m_ListCntl;

		RLISTCNTL *pL;

		pL = m_pListCntl;

		pL->TotalSize	= 0;
		pL->Selected	= 0;
		pL->Start	= 1;		// start for selected list processing
		pL->pMSDAddr	= NULL;		// MSD Address
		pL->pListAddr	= NULL;		// Address of current item
		pL->pListData	= NULL;		// Data of current item
		pL->pListType	= NULL;		// Position of current item
		pL->pItemBad	= NULL;		// Item error flag
		pL->pIndex	= NULL;		// Item index value

		m_uState	= 0;

		m_uTagCount	= 0;

		m_fGlobal	= FALSE;
		m_wVersion	= m_Ident;

		m_pVarInfo	= &SVarInfo;
		m_pVI		= NULL;
		m_pVIData	= NULL;
		m_upVICount	= 0;

		m_pHeadList	= m_HeadList;
		m_pFuncList	= m_FuncList;
		m_pReqList	= m_ReqList;
		m_pDataList	= m_DataList;

		memset(m_bDevAddList, 0, sizeof(m_bDevAddList));
		memset(m_bLevAddList, 0, sizeof(m_bLevAddList));

		m_pbDevAddList	= m_bDevAddList;
		m_pbLevAddList	= m_bLevAddList;

		m_pNRTRx	= &m_NRTFrameR;
		m_pNRTTx	= &m_NRTFrameT;

		m_pNRTRx->Version = 0;
		m_pNRTTx->Version = 0;

		m_uHasNRT	= 0;

		m_dLBMagic	= 0;
		m_dVIMagic	= 0;
		m_dSendMagic	= 0;

/////**/		Test code for delayed responses
/////**/		bDelay = 2;
/////**/		m_bSaveCommand1[0] = 0;
/////**/		m_bSaveCommand2[0] = 0;
/////**/		m_uSaveETXPos1 = 0;
/////**/		m_uSaveETXPos2 = 0;
/////**/		m_uSaveSelect  = 0;
		}
	}

void CBSAPExtSDriver::BuildTimeSync(void) {

	DWORD t = m_pExtra->GetNow();

	BYTE bS	= LOBYTE(GetSec(t));

	if( bS > m_bTimeSync ) {

		m_bTimeSync = bS;
		return;
		}

	TIMESYNCFRAME * p = &m_TMSFrame;

	p->Day		= LOBYTE(GetDate(t));
	p->Month	= LOBYTE(GetMonth(t));
	WORD w		= LOWORD(GetYear(t));
	p->YearLo	= LOBYTE(w);
	p->YearHi	= HIBYTE(w);

	BYTE bItem	= LOBYTE(GetHour(t));
	p->Hour		= bItem;

	WORD wJulian	= (WORD)bItem * 900;	// number of 4 second intervals at top of hour

	bItem		= LOBYTE(GetMin(t));
	p->Minute	= bItem;
	wJulian		+= (WORD)bItem * 15;	// number of 4 second intervals to this minute of the hour

	p->Second	= bS;
	wJulian		+= bS / 4;		// number of 4 second intervals to this second

	w		= LOWORD(GetDays(t + 631152000)); // 0 is 1 Jan 1997, BSAP 0 is 1 Jan 1977
	p->JulianDayLo	= LOBYTE(w);
	p->JulianDayHi	= HIBYTE(w);
	p->Julian4SecLo	= LOBYTE(wJulian);
	p->Julian4SecHi	= HIBYTE(wJulian);
	p->Julian20mSec	= 0;
	}

BOOL CBSAPExtSDriver::CheckDrop(BYTE bData)
{
	m_fGlobal = bData & 0x80;

	if( m_fGlobal ) {

		WORD wAdd    = (WORD)m_BDrvCfg.ThisLocal;

		return wAdd == (WORD)IntelToHost(*PU2(&m_bRx[RXGDAL]));
		}

	else {
		return ((BYTE)bData & 0x7F) == m_BDrvCfg.ThisLocal;
		}
	}

BOOL CBSAPExtSDriver::IsPollOrAck(BYTE bData)
{
	return bData >= POLLMSG && bData <= UPACK;
	}

BOOL CBSAPExtSDriver::IsAlarm(BYTE bData) {

	return bData == OFCALACK || bData == OFCALMIN;
	}

BOOL CBSAPExtSDriver::IsNRTRecv(BYTE bData) {

	return bData == MFCNRTMSG;
	}

BOOL CBSAPExtSDriver::IsReqForNRT(void) {

	return m_bRx[RXDFC] == MFCNRTREQ;
	}

BOOL CBSAPExtSDriver::IsReqForTMS(void) {

	return	m_bRx[RXDFC]   == MFCEXTACC &&
		m_bRx[RXLSFC]  == MFCEXTACC &&
		m_bRx[RXLDAT0] == OFCEXTFC  &&
		m_bRx[RXTSFXC] == OFCEXTSFC;
	}

BOOL CBSAPExtSDriver::IsRespOfNRT(void) {

	return	m_bRx[RXLSFC]  == MFCRDBACC &&
		m_bRx[RXNFC]   == MFCCOMREQ &&
		m_bRx[RXXFCN]  == OFCEXTREQ &&
		m_bRx[RXSFXCN] == OFCEXTFC;
	}

BOOL CBSAPExtSDriver::IsRespOfTMS(void) {

	return	m_bRx[RXLSFC]  == MFCEXTACC &&
		m_bRx[RXTXFC]  == OFCEXTFC &&
		m_bRx[RXTSFXC] == OFCEXTSFC;
	}

UINT CBSAPExtSDriver::IsArrayOpcode(BYTE b) {

	switch( b ) {

		case OFCRARRS:
		case OFCWARRS:
			return 1;

		case OFCRARRG:
		case OFCWARRG:
		case OFCRARRSZ:
			return 2;
		}

	return 0;
	}

#define	DAYS	(60L * 60L * 24L)

#define	QUAD	(3 * 365 + 366)

DWORD CBSAPExtSDriver::Date(UINT y, UINT m, UINT d)
{
	d = d - 1;

	m = GetCummDays(y, m);

	y = y % 100;

	y = (y < 97) ? (y + 3) : (y - 97);

	y = y * 365 + y / 4;
	
	return (m + d + y) * DAYS;
	}

UINT CBSAPExtSDriver::GetYear(DWORD t)
{
	UINT d = GetDays(t);

	UINT r = d % QUAD / 365;

	UINT q = d / QUAD;

	if( r > 3 ) r = 3;

	return 1997 + r + 4 * q;
	}

UINT CBSAPExtSDriver::GetMonth(DWORD t)
{
	UINT y = GetYear(t);
	
	UINT d = (t - Date(y, 1, 1)) / DAYS;
	
	return GetFromDays(y, d);
	}

UINT CBSAPExtSDriver::GetDate(DWORD t)
{
	UINT y = GetYear(t);
	
	UINT m = GetMonth(t);
	
	UINT d = (t - Date(y, m, 1)) / DAYS;
	
	return 1 + d;
	}

UINT CBSAPExtSDriver::GetHour(DWORD t)
{
	return (UINT) (t / 3600 % 24);
	}

UINT CBSAPExtSDriver::GetMin(DWORD t)
{
	return (UINT) (t / 60 % 60);
	}

UINT CBSAPExtSDriver::GetSec(DWORD t)
{
	return (UINT) (t % 60);
	}

UINT CBSAPExtSDriver::GetDays(DWORD t)
{
	return t / DAYS;
	}

UINT CBSAPExtSDriver::GetCummDays(UINT y, UINT m)
{
	if( y % 4 ) {
	
		switch( m ) {
		
			case  1: return 0;
			case  2: return 31;
			case  3: return 31+28;
			case  4: return 31+28+31;
			case  5: return 31+28+31+30;
			case  6: return 31+28+31+30+31;
			case  7: return 31+28+31+30+31+30;
			case  8: return 31+28+31+30+31+30+31;
			case  9: return 31+28+31+30+31+30+31+31;
			case 10: return 31+28+31+30+31+30+31+31+30;
			case 11: return 31+28+31+30+31+30+31+31+30+31;
			case 12: return 31+28+31+30+31+30+31+31+30+31+30;
			
			}
			
		return 365;
		}
	else {
		switch( m ) {
		
			case  1: return 0;
			case  2: return 31;
			case  3: return 31+29;
			case  4: return 31+29+31;
			case  5: return 31+29+31+30;
			case  6: return 31+29+31+30+31;
			case  7: return 31+29+31+30+31+30;
			case  8: return 31+29+31+30+31+30+31;
			case  9: return 31+29+31+30+31+30+31+31;
			case 10: return 31+29+31+30+31+30+31+31+30;
			case 11: return 31+29+31+30+31+30+31+31+30+31;
			case 12: return 31+29+31+30+31+30+31+31+30+31+30;
		
			}
			
		return 366;
		}
		
	return 0;
	}

UINT CBSAPExtSDriver::GetFromDays(UINT y, UINT d)
{
	UINT m = 1;

	for( m = 1; m < 12; m++ ) {
	
		if( d >= GetCummDays(y, 1 + m) ) {

			continue;
			}
		
		break;
		}
		
	return m;
	}

// Tag Data Loading
void CBSAPExtSDriver::LoadTags(LPCBYTE &pData)
{
	if( StoreDevName(pData) ) {

		m_pCtx->m_dTGMagic = 0x12340000;

		UINT uCount = GetWord(pData); // number of Bit/Real names

		UINT i = 0;

		if( uCount ) {

			m_pCtx->m_dTGMagic = 0x12345678;

			m_uTagCount  = uCount;

			m_pdAddrList = new DWORD [uCount + 1];

			m_pwMSDList  = new WORD  [uCount + 1];

			m_pbTypeList = new BYTE  [uCount + 1];

			m_pwGlobList = new WORD  [uCount + 1];

			m_pbSecList  = new BYTE  [uCount + 1];

			BYTE b = 0;
			WORD w = 0;

//****/AfxTrace0("\r\nAddr: ");
			PDWORD pd = m_pdAddrList;
			for( i = 0; i < uCount; i++ ) {		// reference address

				pd[i] = GetLong(pData);
//****/AfxTrace2("[%d %8.8lx]", i, pd[i]);
				}

			pd[uCount] = 0xFFFFFFFF;

//****/AfxTrace0("\r\nMSD: ");
			PWORD pw = m_pwMSDList;
			for( i = 0; i < uCount; i++ ) {		// MSD address == LOWORD of AddrList value

				w = GetWord(pData);

				pw[i] = w;
//****/AfxTrace2("[%d %4.4x]", i, w);
				}

			pw[uCount] = 0xFFFF;

//****/AfxTrace0("\r\nList: ");
			UINT uLCt  = GetWord(pData);		// count of items assigned to Lists
//****/AfxTrace1("LCt = %d\r\n", uLCt);
			m_ListCntl.TotalSize = uLCt;

			if( (BOOL)uLCt ) {

				m_pdLNumList = new DWORD [uLCt + 1];

				pd = m_pdLNumList;

				i = 0;

				while( i < uLCt ) {

					pd[i] = GetLong(pData);
//****/if( !(i % 8) ) AfxTrace0("\r\n"); AfxTrace1("[%8.8lx]", pd[i]);
					i++;
					}

				pd[uLCt] = 0xFFFFFFFF;
				}

			else m_pdLNumList = NULL;

//****/AfxTrace0("\r\nType: ");
			PBYTE pb = m_pbTypeList;
			for( i = 0; i < uCount; i++ ) {	// Type

				pb[i] = GetByte(pData);
//****/AfxTrace2("[%d %2.2x]", i, pb[i]);
				}

			pb[uCount] = 0xFF;

//****/AfxTrace0("\r\nGlob: ");
			pw = m_pwGlobList;
			for( i = 0; i < uCount; i++ ) {		// Global address

				pw[i] = GetWord(pData);
//****/AfxTrace2("[%d %d]", i, pw[i]);
				}

			pw[uCount] = 0xFFFF;

//****/AfxTrace2("\r\nDevLevAdd %8.8lx %8.8lx ", m_pbDevAddList, m_pbLevAddList);
			for( i = 0; i < 15; i++ ) {

				w = GetWord(pData);
//****/AfxTrace2("[%x %x]", LOBYTE(w), HIBYTE(w));
				m_pbDevAddList[i] = LOBYTE(w);
				m_pbLevAddList[i] = HIBYTE(w);
				}

			w = GetWord(pData);	// number of configured arrays
//****/AfxTrace1("\r\nArrays Ct=%d ", w);
			m_bArrCnt = w;

			if( (BOOL)w ) {

				m_pArrInx = new WORD [w];
				m_pArrNum = new BYTE [w];
				m_pArrCol = new BYTE [w];

				m_pCtx->m_dARMagic = 0x12345678;

				for( i = 0; i < (UINT)w; i++ ) {

					m_pArrInx[i] = GetWord(pData);
					m_pArrNum[i] = GetByte(pData);
					m_pArrCol[i] = GetByte(pData);
//****/AfxTrace0("\r\n"); AfxTrace3("I=%x, N=%d, C=%d ", m_pArrInx[i], m_pArrNum[i], m_pArrCol[i]);
					}
				}

			else m_pCtx->m_dARMagic = 0;

			UINT uTotalSize	= GetWord(pData); // adding nulls

			m_pwNameIndex	= new WORD  [uCount + 1];
			m_pbNameList	= new BYTE  [uTotalSize + 1];

			UINT uPos	= 0;
//****/AfxTrace1("\r\nSLAVE Names Size = %d ", uTotalSize);
			for( i = 0; i < uCount; i++ ) {

				StoreName( pData, &uPos, i );
				}

			m_pwNameIndex[uCount]	= uTotalSize; // end marker

			// Initialize Security codes to all enable
			for( i = 0; i < uCount; i++ ) {

				m_pbSecList[i] = 0xFF;
				}

			m_pbSecList[uCount] = 0;
			}
		}

	else {
		m_pCtx->m_dTGMagic = 0;
		}
	}

BOOL CBSAPExtSDriver::StoreDevName(LPCBYTE &pData)
{
	UINT uCount = GetWord(pData); // size of device name

	if( uCount ) {

		m_pbDevName = new BYTE[uCount];

		memset(m_pbDevName, 0, uCount);

		for( UINT i = 0; i < uCount; i++ ) {

			m_pbDevName[i] = LOBYTE(GetWord(pData));
			}

		return TRUE;
		}

	return FALSE;
	}

void CBSAPExtSDriver::StoreName(LPCBYTE &pData, UINT *pPosition, UINT uItem)
{
	UINT uByteCount	= GetWord(pData);

	UINT uPos	= *pPosition;

	*pPosition	+= uByteCount;

	m_pwNameIndex[uItem] = uPos;

	PBYTE p		= &m_pbNameList[uPos];

	BYTE b		= LOBYTE(GetWord(pData));

//****/AfxTrace3("\r\nSize=%d Pos=%d %c", uByteCount, uPos, b);

	if( b != '.' ) {

		*p++ = MakeUpper(b);
		}

	for( UINT i = 1; i < uByteCount; i++ ) {

		*p++ = MakeUpper(LOBYTE(GetWord(pData)));

//****/AfxTrace1("%c", *(p-1));
		}
	}

BYTE CBSAPExtSDriver::MakeUpper(BYTE b) {

	if( b >= 'a' && b <= 'z' ) {

		b &= 0xDF;
		}

	return b;
	}

// Delete created buffers
void CBSAPExtSDriver::CloseListBuffers(void)
{
	if( m_dLBMagic == 0x12345678 ) {

		RLISTCNTL * p = m_pListCntl;

		if( p->pMSDAddr )	{ delete p->pMSDAddr;		p->pMSDAddr  = NULL; }

		if( p->pListAddr )	{ delete p->pListAddr;		p->pListAddr = NULL; }

		if( p->pListData )	{ delete p->pListData;		p->pListData = NULL; }

		if( p->pListType )	{ delete p->pListType;		p->pListType = NULL; }

		if( p->pItemBad )	{ delete p->pItemBad;		p->pItemBad  = NULL; }

		if( p->pIndex )		{ delete p->pIndex;		p->pIndex    = NULL; }
		}

	m_dLBMagic = 0;
	}

void CBSAPExtSDriver::CloseDown(CContext *pPtr)
{
	if( m_dSendMagic == 0x12345678 ) { delete m_pSend; m_pSend = NULL; m_dSendMagic = 0; }

	CloseListBuffers();

	DeleteVarInfo();

	if( pPtr == NULL ) { return;}

	if( HIWORD(pPtr->m_dTGMagic) == 0x1234 ) {

		if( m_pbDevName    ) { delete m_pbDevName;	m_pbDevName = NULL;    }
		}

	pPtr->m_dTGMagic = pPtr->m_dTGMagic & 0xFFFF;

	if( pPtr->m_dTGMagic == 0x5678 ) {

		if( m_pdAddrList   ) { delete m_pdAddrList;	m_pdAddrList	= NULL;  }

		if( m_pdLNumList   ) { delete m_pdLNumList;	m_pdLNumList	= NULL;  }

		if( m_pbNameList   ) { delete m_pbNameList;	m_pbNameList	= NULL;  }

		if( m_pwNameIndex  ) { delete m_pwNameIndex;	m_pwNameIndex	= NULL;  }

		if( m_pwMSDList    ) { delete m_pwMSDList;	m_pwMSDList	= NULL;  }

		if( m_pbTypeList   ) { delete m_pbTypeList;	m_pbTypeList	= NULL; }

		if( m_pwGlobList   ) { delete m_pwGlobList;	m_pwGlobList	= NULL; }

		if( m_pbSecList    ) { delete m_pbSecList;	m_pbSecList	= NULL; }
		}

	pPtr->m_dTGMagic = 0;

	if( pPtr->m_dARMagic == 0x12345678 ) {

		delete m_pArrInx;
		delete m_pArrNum;
		delete m_pArrCol;

		m_pArrInx = NULL;
		m_pArrNum = NULL;
		m_pArrCol = NULL;
		}

	pPtr->m_dARMagic = 0;

//**/	AfxTrace0("\r\nClosing Down ");

	InitGlobal(TRUE);

//**/	AfxTrace0("\r\nDONE ");
	}

// Conversions

DWORD CBSAPExtSDriver::LongToReal(UINT uType, DWORD Data)
{
	switch(uType) {

		case addrByteAsByte:	Data &= 0xFF;	break;

		case addrWordAsWord:	Data &= 0xFFFF;	break;
		}

	float Real = float(LONG(Data));

	switch( uType ) {

		case addrBitAsBit:	return (DWORD)((BOOL)Data);
		case addrByteAsByte:	return  R2I(Real);
		case addrWordAsWord:	return  R2I(Real);
		case addrLongAsLong:	return  R2I(Real);
		}

	return Data;
	}

DWORD CBSAPExtSDriver::RealToLong(UINT uType, DWORD Data)
{
	float Real = I2R(Data);

	switch( uType ) {

		case addrBitAsBit:	return (DWORD)((BOOL)Data);
		case addrByteAsByte:	return (DWORD)LOBYTE(Real);
		case addrWordAsWord:	return (DWORD)LOWORD(Real);
		case addrLongAsLong:	return (DWORD)Real;
		}

	return Data;
	}

// End of File
