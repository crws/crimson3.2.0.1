//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Extended Serial Slave Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

class CBSAPExtSDriver;

struct BSAPDRVSCFG {

	BYTE	NetLevel;
	BYTE	ThisLocal;
	WORD	ThisGlobal;
	};

class CBSAPExtSDriver : public CSlaveDriver
{
	public:
		// Constructor
		CBSAPExtSDriver(void);

		// Destructor
		~CBSAPExtSDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);
		DEFMETH(void)	CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Detach(void);
		DEFMETH(WORD)	GetMasterFlags(void);

		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(void)	Service (void);

	protected:
		// Device Context
		struct CContext {

			BYTE	m_bSerNum;

			// Protocol information
			UINT	m_uMsgSeq;
			UINT	m_uAppSeq;
			UINT	m_uACCOLVers;
			UINT	m_uMSDVers;

			BYTE	m_bIsC3;
			BYTE	m_bHasNRT;

			DWORD	m_dTGMagic;
			DWORD	m_dARMagic;
			};

		CContext * m_pCtx;

		struct POLLRCVD
		{
			BYTE	Addr;
			BYTE	MsgSer;
			BYTE	DestFC;
			BYTE	PollPri;
			};

		struct	POLLRESP
		{
			BYTE	Addr;	// always 0;
			BYTE	MsgSer;
			BYTE	DestFC;
			BYTE	SlvAdd;
			BYTE	NodeSB;
			BYTE	NumBuff;
			};

		struct LOCMSGHEAD
		{
			BYTE	Addr;
			BYTE	MsgSer;
			};

		struct GLBMSGHEAD
		{
			BYTE	Addr;
			BYTE	MsgSer;
			BYTE	DAddrL;
			BYTE	DAddrH;
			BYTE	SAddrL;
			BYTE	SAddrH;
			BYTE	Control;
			};

		struct	ENDHEADLIST
		{
			BYTE	DestFC;
			BYTE	AppSeqL;
			BYTE	AppSeqH;
			BYTE	SrceFC;
			BYTE	NodeST;
			};

		struct	RBYADDRRX
		{
			BYTE	FuncCode;
			BYTE	FSS[5];
			BYTE	VersL;
			BYTE	VersH;
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct	RBYNAMERX
		{
			BYTE	FuncCode;
			BYTE	FSS[5];
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct	RSIGNALTX
		{
			BYTE	NodeStat;
			BYTE	RER;
			BYTE	NumElem;
			PBYTE	Elements;
			};

		struct	RBYLISTRX
		{
			BYTE	FuncCode;
			BYTE	FSS[5];
			BYTE	VersL;
			BYTE	VersH;
			BYTE	SecLevel;
			BYTE	ListNum;
			BYTE	ListNum2L;
			BYTE	ListNum2H;
			};

		struct	RLISTCNTL
		{
			WORD	TotalSize;
			UINT	Selected;
			UINT	Start;
			UINT	LNPos;
			UINT	Index;
			UINT	uListCt;
			PWORD	pMSDAddr;
			PDWORD	pListAddr;
			PDWORD	pListData;
			PBYTE	pListType;
			UINT	*pIndex;
			PBYTE	pItemBad;
			};

		struct	RBYLISTTX
		{
			BYTE	NodeStat;
			BYTE	RER;
			BYTE	NumElem;
			BYTE	ListNum;
			};

		struct	WBYADDRRX
		{
			BYTE	FuncCode;
			BYTE	VersL;
			BYTE	VersH;
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct	WBYNAMERX
		{
			BYTE	FuncCode;
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct	WBYLISTRX
		{
			BYTE	FuncCode;
			BYTE	VersL;
			BYTE	VersH;
			BYTE	SecLevel;
			BYTE	ListNum;
			BYTE	ContinLo;
			BYTE	ContinHi;
			};

		struct VARINFO
		{
			DWORD	Addr;
			PDWORD	pData;
			BOOL	Logical;
			UINT	NameInx;
			WORD	Offset;
			WORD	MSDAddr;
			WORD	Length;
			WORD	Index;
			BYTE	EER;
			BYTE	FSS[5];
			UINT	uDataCt;
			UINT	uFieldSize;
			};

		struct READFRAME
		{
			UINT	uAddr;
			PDWORD	pData;
			UINT	uCount;
			UINT	uGood;
			BOOL	fDoMSD;
			UINT	uIndex;
			};

		struct BSHeader { // Serial Comms Header
			BYTE	bAdd;
			BYTE	bSNum;
			BYTE	bDFC;
			BYTE	bSQL;
			BYTE	bSQH;
			BYTE	bSFC;
			BYTE	bNSB;
			};

		struct RXARRAY {
			UINT	uType;
			UINT	uNumber;
			UINT	uStartRow;
			UINT	uStartCol;
			UINT	uItemCt;
			UINT	uIndex;
			UINT	uColumnCt;
			UINT	uReqCount;
			};

		struct RXARRAYSIZE {
			BYTE	FuncCode;
			BYTE	ArrayType;
			BYTE	ArrayNum;
			};

		struct RXSHORTARRAY {
			BYTE	FuncCode;
			BYTE	Type;
			BYTE	Number;
			BYTE	Row;
			BYTE	Column;
			BYTE	Count;
			};

		struct RXGENERALARRAY {
			BYTE	FuncCode;
			BYTE	Type;
			BYTE	Number;
			BYTE	RowLo;
			BYTE	RowHi;
			BYTE	ColumnLo;
			BYTE	ColumnHi;
			BYTE	Count;
			};

		struct TXSHORTARRAY {
			BYTE	Count;
			BYTE	Number;
			BYTE	NextRow;
			BYTE	NextColumn;
			};

		struct TXGENERALARRAY {
			BYTE	Count;
			BYTE	Number;
			WORD	NextRow;
			WORD	NextColumn;
			};

		struct	ALARMACKREQ {

			WORD	LoadVersion;
			BYTE	Count;
			PWORD	pAlarmAddress;
			};

		struct	ALARMACKRESP {

			BYTE	RER; //0=Success,32=Load Version mismatch, 128=Illegal Address
			BYTE	Count;
			PWORD	pAlarmAddress;
			};

		struct	TIMESYNCFRAME {
			BYTE	Day;
			BYTE	Month;
			BYTE	YearLo;
			BYTE	YearHi;
			BYTE	Hour;
			BYTE	Minute;
			BYTE	Second;
			BYTE	JulianDayLo;	// JD 1 = 1 Jan 1977
			BYTE	JulianDayHi;	// JD 1 = 1 Jan 1977
			BYTE	Julian4SecLo;
			BYTE	Julian4SecHi;
			BYTE	Julian20mSec;
			};

		struct	NRTFRAME {
			TIMESYNCFRAME TSF;
			BYTE	Version;
			BYTE	GlobAddrLo;
			BYTE	GlobAddrHi;
			BYTE	UpDnMaskLo;
			BYTE	UpDnMaskHi;
			BYTE	Level;
			BYTE	LSC[7];
			BYTE	LBM[7];
			};

		READFRAME	m_RFrame;
		READFRAME	*m_pRFrame;
		NRTFRAME	m_NRTFrameR;
		NRTFRAME	m_NRTFrameT;
		TIMESYNCFRAME	m_TMSFrame;
		NRTFRAME	*m_pNRTRx;
		NRTFRAME	*m_pNRTTx;

		// Header and Control Structures
		BSHeader	 Head;
		BSHeader	*m_pHead;

		typedef VARINFO * PVARINFO;

		POLLRCVD	PollReceived;
		DWORD		m_dLBMagic;
		DWORD		m_dSendMagic;
		DWORD		m_dVIMagic;

		// BSAP Device Config
		BSAPDRVSCFG	m_BDrvCfg;

		// Config Lists
		PBYTE		m_pbDevName;
		PDWORD		m_pdAddrList;
		PDWORD		m_pdLNumList;
		PWORD		m_pwMSDList;
		PBYTE		m_pbIntlList;
		PBYTE		m_pbTypeList;
		PBYTE		m_pbDevList;
		PBYTE		m_pbLevList;
		PWORD		m_pwGlobList;
		PBYTE		m_pbDevAddList;
		PBYTE		m_pbLevAddList;
		BYTE		m_bDevAddList[16];
		BYTE		m_bLevAddList[16];
		PWORD		m_pArrInx;
		PBYTE		m_pArrNum;
		PBYTE		m_pArrCol;
		BYTE		m_bArrCnt;
		PBYTE		m_pbNameList;
		PWORD		m_pwNameIndex;
		PBYTE		m_pbSecList;
		UINT		m_uTagCount;

		RLISTCNTL	m_ListCntl;
		RLISTCNTL	*m_pListCntl;

		BYTE		m_bIsC3;

		// Master Controls
		BOOL		m_fIsThis;
		BOOL		m_fGlobal;
		BYTE		m_bRER;		// Master Error Flag
		WORD		m_wVersion;	// Tag Version
		BYTE		m_NodeStatus;
		BYTE		m_bNRTVersion;
		BYTE		m_bTimeSync;
		UINT		m_uHasNRT;

		// Buffer Pointers
		PBYTE		m_pSave;
		PBYTE		m_pHeadList;
		PBYTE		m_pFuncList;
		PBYTE		m_pReqList;
		PBYTE		m_pDataList;
		BYTE		m_HeadList[7];
		BYTE		m_FuncList[5];
		BYTE		m_ReqList[16];
		BYTE		m_DataList[240];

		// Variable info
		VARINFO	SVarInfo;
		VARINFO	*m_pVarInfo;
		PVARINFO *m_pVI;
		PDWORD	m_pVIData;
		UINT	m_upVICount;
		WORD	m_wVarSel;
		UINT	m_uReqType;

		// Comms
		UINT	m_uTPtr;
		UINT	m_uRPtr;
		BYTE	m_bTx [300];
		BYTE	m_bRx [300];
		BYTE	m_bPollTx[64];
		BYTE	m_bNRTTx[40];
		BYTE	m_bTMSTx[40];

		PBYTE	m_pTx;
		UINT	m_uState;
		UINT	m_uETXPos;

		RXARRAY	m_RxArray;

		// Allocated pointers
		PBYTE	m_pSend;

		// Field Selects
		BYTE	m_bFSS[5];

		// Extra Help
		IExtraHelper   * m_pExtra;

////**/		Test variables for delayed responses
////**/		BYTE	bDelay;
////**/		UINT	m_uSaveSelect;
////**/		BYTE	m_bSaveCommand1[50];
////**/		BYTE	m_bSaveCommand2[50];
////**/		UINT	m_uSaveETXPos1;
////**/		UINT	m_uSaveETXPos2;

		// Receive Processing
		void	ReadData(void);
		void	ProcReceive();
		void	DoReceive(void);
		void	MakeReqBuffer(PBYTE pDest, UINT uSize, UINT *pPos);

		// Read Routines
		void	ReadByList(BOOL fFirst);
		void	ReadSignal(BYTE bType);
		void	CopyHeaderForResp(void);

		// Array Handling
		BOOL	SetArrayIndex(void);
		BOOL	SetShortArrayParams(void);
		BOOL	SetGenerArrayParams(void);

		// Read Arrays
		void	ReadArraySize(void);
		void	ReadShortArray(void);
		void	ReadGeneralArray(void);
		BOOL	ReadDefaultArray(BOOL fGeneral);
		void	GetNextRowAndCol(UINT *pRow, UINT *pCol);
		UINT	GetInitialCount(void);
		void	AddArrData(UINT uType, PDWORD pData, UINT uCount);

		// Write Arrays
		void	WriteShortArray(void);
		void	WriteGeneralArray(void);
		BOOL	WriteDefaultArray(void);
		BOOL	MakeBitData(void);
		BOOL	MakeRealData(void);
		BOOL	MakeArrayWrite(BOOL fIsBit);

		// Write routines
		void	WriteByName(void);
		void	WriteByAddress(void);
		void	WriteByList(BOOL fFirst);
		BOOL	SetWriteData(UINT uPos, PDWORD pData, BYTE bDesc);
		BOOL	FormWriteData(UINT uPos, PDWORD pData, BYTE bDesc);
		void	ExecuteWrite(PVARINFO *pVI, UINT uQty);
		void	StoreSignalControls(BYTE bDesc);
		void	HandleArrays(BYTE bType, PDWORD pData, UINT uPos);
		void	HandleString(UINT uDataPos, PDWORD pData);
		void	HandleSecurity(BYTE bData, BOOL fRead);
		UINT	GetNextWriteItem(UINT uPos);
		UINT	GetWriteArraySize(UINT uQty);

		// NRT/TMS Routines
		void	RequestNRT(void);
		void	ReceiveNRT(void);
		void	RequestTMS(void);
		void	WriteNRT(void);
		void	SaveNRT(void);
		void	SaveTMS(void);
		void	GetNRT(void);
		void	GetTMS(void);
		void	RespondNRT(void);
		void	MakeNRTSendHeader(void);
		void	MakeNRTRespHeader(void);
		void	MakeTMSSendHeader(void);
		void	MakeTMSRespHeader(void);

		// Item info
		BOOL	FindName(UINT uPos, char *pSrce);
		BOOL	FindAddr(UINT uPos);
		BYTE	LoadInfoAboutItem(void);
		BOOL	GetReadInfo(UINT uIndex);
		void	FillInfo(UINT uIndex, PDWORD pData);
		void	DeleteVarInfo(void);

		// Read Helpers
		void	ConstructSignalSend(PVARINFO *pVI, UINT uQty);
		void	GetVarInfo(UINT uIndex);

		// Write Helpers
		void	ConstructWriteReply(PVARINFO *pVI, UINT uQty);

		// Construct Slave Frame
		void	AddDevAndSerNum(BYTE bSerial);
		UINT	AddSerialHeader(void);
		void	AddSerialFunc(void);

		// Field Select Helpers
		BOOL	ParseVarName(PCTXT Name, UINT uSelect, PBYTE pItem);
		void	SetFieldSelects(void);
		UINT	AddFieldSels(void);
		UINT	AddField1(void);
		UINT	AddField2(void);
		UINT	AddField3(void);

		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddCRC(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddReal(DWORD dwData);
		void	AddText(PCTXT pText);
		void	AddTextPlusNull(PCTXT pText);

		// Transport Layer
		void	Send(void);
		void	SendFrame(void);
		void	SendReject(void);

		BOOL	PollSlave(void);

		// Ack
		void	SendAckNak(BYTE bType);
		void	SendPollAck(void);
		void	SendDownAck(void);
		void	SendAlarmAck(void);
		void	SendAlarmInitAck(void);
		void	SetAlarmAck(void);
		void	SendNak(void);
		BOOL	CheckNak(UINT uVal, UINT uFail);
		void	SendDiscard(BYTE bSerNum);

		// RER
		void	SetRER(BYTE bRER);
		void	AddEER(BYTE bError);

		// Helpers
		void	InitGlobal(BOOL fInit);
		void	BuildTimeSync(void);
		BOOL	CheckDrop(BYTE bData);
		BOOL	IsPollOrAck(BYTE bData);
		BOOL	IsAlarm(BYTE bData);
		BOOL	IsNRTRecv(BYTE bData);
		BOOL	IsReqForNRT(void);
		BOOL	IsReqForTMS(void);
		BOOL	IsRespOfNRT(void);
		BOOL	IsRespOfTMS(void);
		UINT	IsArrayOpcode(BYTE b);

		// Date and Time
		DWORD	Date(UINT y, UINT m, UINT d);
		UINT	GetYear(DWORD t);
		UINT	GetMonth(DWORD t);
		UINT	GetDate(DWORD t);
		UINT	GetHour(DWORD t);
		UINT	GetMin(DWORD t);
		UINT	GetSec(DWORD t);
		UINT	GetDays(DWORD t);
		UINT	GetCummDays(UINT y, UINT m);
		UINT	GetFromDays(UINT y, UINT d);

		// Tag Data Loading
		void	LoadTags(LPCBYTE &pData);
		BOOL	StoreDevName(LPCBYTE &pData);
		void	StoreName(LPCBYTE &pData, UINT *pPosition, UINT uItem);
		BYTE	MakeUpper(BYTE b);

		// delete created buffers
		void	CloseListBuffers(void);
		void	CloseDown(CContext *pPtr);

		// Conversions
		DWORD LongToReal(UINT uType, DWORD Data);
		DWORD RealToLong(UINT uType, DWORD Data);
	};

enum {	// Poll Response Positions: DOWN, NODATA, NAK (= No Buffers Available)
	PLADDR	= 0,	// Address of Master (0)
	PLSERN	= 1,	// Serial Num of Poll
	PLFC	= 2,	// Function code of response
	PLSLV	= 3,	// Address of slave responding
	PLNSB	= 4,	// Node Status Byte
	PLBUFF	= 5,	// Number of buffers in use
	};

enum { // Local Serial Rx Header Positions

	RXDADD	= 0,	// Destination Drop - same for global
	RXSERN	= 1,	// Serial Number - same for global
	RXDFC	= 2,	// Destination Function code
	RXSPRI	= 3,	// POLL Priority Code (=0)
	RXLSQL	= 3,	// App Sequence Number LO
	RXLSQH	= 4,	// App Sequence Number HI
	RXLSFC	= 5,	// Source Function Code
	RXLNSB	= 6,	// Node Status Byte
	RXLDAT0	= 7,	// Packet Data Start
	RXNXFC	= 8,	// NRT request Extended Function Code
	RXTSFXC	= 8,	// TMS request Extended subfunction code
	RXNXSFC	= 9,	// NRT request Extended subfunction code
	RXTMSD	= 9,	// DEL at end of TMS request
	RXTXFC	= 9,	// Extended Function code in TMS response
	RXTXSFC	= 10,	// Extended subfunction code in TMS response
	RXTMSE	= 10,	// ETX at end of TMS request
	RXNFC	= 11,	// NRT Function Code
	RXXFCN	= 12,	// Extended function code in response
	RXSFXCN	= 13,	// Extended subfunction code in response
	};

enum { // Global Serial Rx Header Positions

//	RXDADD	= 0,	// Destination Drop - same for global as local
//	RXSERN	= 1,	// Serial Number - same for global as local
	RXGDAL	= 2,	// Destination Addr Low
	RXGDAH	= 3,	// Destination Addr High
	RXGSAL	= 4,	// Source Addr Low
	RXGSAH	= 5,	// Source Addr High
	RXGCTL	= 6,	// Control Byte
	RXGDFC	= 7,	// Destination Function Code
	RXGSQL	= 8,	// Application Sequence Number Low
	RXGSQH	= 9,	// Application Sequence Number High
	RXGSFC	= 10,	// Source Function Code
	RXGNSB	= 11,	// Node Status Byte
	RXGDAT0	= 12	// Packet Data Start
	};

enum { // List Serial Rx Data Positions

	RXSTRQ	= 0,	// List Command 0xC = Start, 0xD = Continue
	RXFUN	= 1,	// FSS define
	RXFSS1	= 2,	// FSS1
	RXFSS2	= 3,	// FSS2
	RXFSS3	= 4,	// FSS3
	RXFSS4	= 5,	// FSS4
	RXVERL	= 6,	// Version Low
	RXVERH	= 7,	// Version High
	RXSECC	= 8,	// Security Code
	RXLIST	= 9	// List Number
	};

#define	TXRERPOS	9

enum { // Message function codes
	MFCPEIPC  =  0x3, // for HMI
	MFCPEIRTU = 0x04, // to RTU
	MFCRDDB   = 0x0B, // Read Database
	MFCWRDB   = 0x0C, // Write Database
	MFCRDBYN  = 0x0D, // Read By Name in example file from BB
	MFCCOMREQ = 0x70, // Command Handler Requests
	MFCNRTMSG = 0x88, // TS/NRT Message (for Slave Ports only)
	MFCNRTREQ = 0x89, // Request for TS/NRT Message
	MFCRDBACC = 0xA0, // remote database access
	MFCEXTACC = 0xA1, // extended rdb
	};

enum { // Operational function codes
	OFCRDBYA  =  0x0, // Read Signal by Address
	OFCEXTSFC =  0x1, // Extended Request Date/Time Sub Function code
	OFCEXTFC  =  0x2, // Extended Request Function Code / NRT SubFunction Code
	OFCRDBYN  =  0x4, // Read Signal By Name
	OFCEXTREQ =  0xA, // Extended Request Function Code
	OFCRARRSZ = 0x10, // Read Array Size
	OFCRARRS  = 0x14, // Read Array, Short Form
	OFCRARRG  = 0x18, // Read Array, General Form,
	OFCRDBYL  = 0x0C, // First call to read Signal(s) by list
	OFCRDBYLC = 0x0D, // Continue to read Signal(s) by list
	OFCREQNRT = 0x70, // Read NRT Function code
	OFCWRBYA  = 0x80, // Write Signal by Address
	OFCWRBYN  = 0x84, // Write By Name
	OFCWRBYL  = 0x8C, // Write By List
	OFCWRBYLC = 0x8D, // Write By List Continued (Single Items in List)
	OFCWARRS  = 0x94, // Write Array, Short Form
	OFCWARRG  = 0x98, // Write Array, General Form
	OFCALACK  = 0xA8, // Alarm Acknowledgement Req & Rsp.
	OFCALMIN  = 0xA9, // Alarm Initialization Request
	};

enum { // Field Select 1 codes
	FS1TYPINH = 0x80,
	FS1VALUE  = 0x40,
	FS1UNITS  = 0x20,
	FS1MSDADD = 0x10,
	FS1NAME   = 0x08,
	FS1ALARM  = 0x04,
	FS1DESC   = 0x02,
	FS1ONOFF  = 0x01,
	};

enum { // Field Select 2 codes
	FS2PROTECT = 0x80,
	FS2ALMPRI  = 0x40,
	FS2BNAME   = 0x20,
	FS2BEXT    = 0x10,
	FS2BATT    = 0x08,
	FS2ALMDBLO = 0x04,
	FS2ALMDBHI = 0x02,
	FS2MSDVER  = 0x01,
	};

enum { // Field Select 3 codes
	FS3ALMLMLO = 0x80,
	FS3ALMLMHI = 0x40,
	FS3ALMEXLO = 0x20,
	FS3ALMEXHI = 0x10,
	FS3RBERSVD = 0x08,
	FS3LCKRSVD = 0x04,
	FS3SIGVAL  = 0x02,
	FS3NAMELNG = 0x01,
	};

enum { // Polls and Acks
	RTURESP  = 0x40, // Data Response
	ACKDSCD  = 0x83, // Ack, but discarded
	POLLMSG  = 0x85, // Poll message
	ACKDOWN  = 0x86, // Down Transmit ACK, Slave ACKing message
	ACKPOLL  = 0x87, // ACK No Data, in response to a Poll message
	NRTSEND  = 0x88, // Return Node routing Table
	NRTREQ   = 0x89, // Request for NRT message
	POLLUPSL = 0x8A, // UP-ACK with poll (recognized by VSAT Slave)
	UPACK    = 0x8B, // UP-ACK, Master ACKing message
	NAKDATA  = 0x95, // NAK, No buffer available for received data
	ALARMACK = 0xA8, // Alarm Acknowledge
	ALARMNOT = 0xAA, // Alarm notification
	};

enum { // Write Field Descriptors
	WFDALDIS = 0x1,
	WFDALEN  = 0x2,
	WFDCNDIS = 0x3,
	WFDCNEN  = 0x4,
	WFDMNDIS = 0x5,
	WFDMNEN  = 0x6,
	WFDQUSET = 0x7,
	WFDQUCLR = 0x8,
	WFDLVON  = 0x9,
	WFDLVOFF = 0xA,
	WFDANALG = 0xB,
	WFDSTRNG = 0xD,
	WFDRDSEC = 0xE,
	WFDWRSEC = 0xF,
	};

enum { // RER Codes for standard requests
	RERMORE  = 0x81,	// Multiple errors in response
	RERMATCH = 0x84,	// no match on name, or too big
	RERAVPKD = 0x88,	// Analog value in packed logical
	RERNOMAT = 0x90,	// no match found
	RERVERNM = 0xA0,	// Version number mismatch
	RERINVDB = 0xC0,	// invalid data base function requested
	RERSET   = 0x80		// Error exists
	};

enum { // RER Codes for array request
	RERBOUND = 0x82,	// Array bounds exceeded
	RERARRCF = 0x84,	// Invalid array reference
	RERROARR = 0x88,	// write to a read-only array
//	RERINVDB = 0xC0		// invalid data base function request
	};

enum { // EER Codes
	EERRONLY = 0x02,	// attempt to write to read only
	EERIDENT = 0x04,	// Invalid name, list, MSDAddr
	EERSECUR = 0x05,	// Security violation
	EERINHIB = 0x06,	// Write a manual inhibited signal
	EERINVWR = 0x07,	// Invalid write attempt (eg analog=ON)
	EEREND_D = 0x08,	// Premature End_of_Data encountered
	EERINCOM = 0x09,	// Incomplete Remote DB request
	EERMATCH = 0x10		// No Match for Signal Name
	};

enum {				// table numbers
	SPBIT0		= 10,
	SPBIT1		= 11,
	SPBIT2		= 12,
	SPBIT3		= 13,
	SPBIT4		= 14,
	SPBIT5		= 15,
	SPBIT6		= 16,
	SPREAL0		= 20,
	SPREAL1		= 21,
	SPREAL2		= 22,
	SPREAL3		= 23,
	SPREAL4		= 24,
	SPREAL5		= 25,
	SPREAL6		= 26,
	SPSTRING0	= 30,
	SPSTRING1	= 31,
	SPSTRING2	= 32,
	SPSTRING3	= 33,
	SPSTRING4	= 34,
	SPSTRING5	= 35,
	SPSTRING6	= 36,
	SPBOOLACK	= 70,
	SPREALACK	= 71,
	SPNRT0		= 90,
	SPNRT1		= 91,
	SPNRT2		= 92,
	SPNRT3		= 93,
	SPNRT4		= 94,
	SPNRT5		= 95,
	SPNRT6		= 96,
	SPBYTE0		= 110,
	SPBYTE1		= 111,
	SPBYTE2		= 112,
	SPBYTE3		= 113,
	SPBYTE4		= 114,
	SPBYTE5		= 115,
	SPBYTE6		= 116,
	SPWORD0		= 120,
	SPWORD1		= 121,
	SPWORD2		= 122,
	SPWORD3		= 123,
	SPWORD4		= 124,
	SPWORD5		= 125,
	SPDWORD0	= 130,
	SPDWORD1	= 131,
	SPDWORD2	= 132,
	SPDWORD3	= 133,
	SPDWORD4	= 134,
	SPDWORD5	= 135,
	SPDWORD6	= 136,
	SPBYTEINT	= 140,
	SPWORDINT	= 150,
	SPDWORDINT	= 160,
	};

// type and inhibits bit definition positions
enum {
	TITYPE	= 0x03,		// mask
	TIALEX	= 0x04,		// 0=not alarm, 1=alarm
	TIDYNM	= 0x08,		// 0=dynamic, 1=constant
	TIMANL	= 0x10,		// 0=manual enabled, 1=manual inhibited
	TICTRL	= 0x20,		// 0=control enabled, 1=control inhibited
	TIALMI	= 0x40,		// 0=alarm enabled, 1=alarm inhibited
	TIQUES	= 0x80,		// 0=ok data, 1=questionable data
	};

// Request Type
enum {
	RQTNAME	= 1,	// request using Name
	RQTADDR	= 2,	// request using MSD Addr
	RQTARR	= 3,	// request using Array
	RQTLIST	= 4,	// request List
	};

//////////////////////////////////////////////////////////////////////////
//
// Useful Macros
//

#define	I2R(x)	(*((float *) &(x)))

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// Constants
//


// Slave Address Fields
#define	AMASK	0x7FFF	// MSD Address = 0 - 1023
#define	LSHIFT	10	// List number is bit 10+
#define	NUMBUFF	 8	// maximum number of pending transactions
#define	MAXBUFF	(NUMBUFF - 1)

// Polling Alarm Enable Values
#define	YESALARMS	0
#define	NOALARMS	0x10

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	MAXSEND	240

/*
FUN Function code
bit 7 function bit
0 read function
1 write function
bits 6-4 = data type code
000 signal data
001 data array data
010 direct I/O data
100 memory data
111 Extended RDB function
bits 3-2 = select code
00 select via address or data array number, control
01 select via name or data array number, short form
10 select via match or data array number, general form
11 select via list
bits 1-0 = signal READ return code, WRITE Memory code
For READ:
00 return first match
01 return next match/continue list
10 return packed logical data (logical value data only)
For WRITE Memory,
00 Write Data to memory
01 OR Data with memory
10 AND Data with memory

NOTE:FIELD SELECTOR BYTES ARE ONLY USED IN ACCESSING SIGNAL
DATA WHEN PACKED LOGICAL DATA IS NOT USED.
FSS Field select selector defines which field select bytes are included in the request:
BITS 7-4 not used at this time (reserved for priority)
BIT 3 Field select byte 4 is used
BIT 2 Field select byte 3 is used
BIT 1 Field select byte 2 is used
BIT 0 Field select byte 1 is used

FS1 Field selector byte one (most common options):
BIT 7 Type and Inhibits byte
BIT 6 Value
BIT 5 Units/Logical text
BIT 4 Msd address
BIT 3 Name Text (includes Base.Extension.Attribute)
BIT 2 Alarm status
BIT 1 Descriptor text
BIT 0 Logical ON/OFF text

FS2 Field selector byte two:
BIT 7 Protection byte
BIT 6 Alarm priority
BIT 5 Base Name
BIT 4 Extension
BIT 3 Attribute
BIT 2 Low alarm deadband MSD address
BIT 1 High alarm deadband MSD address
BIT 0 MSD Version number

FS3 Field selector byte three:
BIT 7 Low alarm limit MSD address
BIT 6 High alarm limit MSD address
BIT 5 Extreme low alarm limit MSD address
BIT 4 Extreme high alarm limit MSD address
BIT 3 Reserved for Report by exception
BIT 2 Reserved (old Lock bit)
BIT 1 Signal value; Analogs return no Q-bit in FP mantissa.
BIT 0 When set, allows long signal names to be returned, instead of standard
length names. (Requires ControlWave firmware version 2.2 or newer)

FS4 Field selector byte four:
This byte is reserved for future use.
*/

// End of File
