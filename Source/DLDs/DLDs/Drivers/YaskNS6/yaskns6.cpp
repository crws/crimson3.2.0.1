
#include "intern.hpp"

#include "yaskns6.hpp"

/*struct FAR YASKNS6CmdDef {
	char	cOP[10];
	UINT	Type;
	UINT	uID;
	};

// Type definitions
#define RO	0 //Read Only
#define RON	1 //Read Only with Numeric
#define RWP	2 //Read/Write with numeric
#define CM	3 //Command with no data
#define	CMDN	4 //Command with numeric, no data
#define CMD	5 //Command with numeric & data,
#define CMWN	6 //Command with data, no sign
#define CMW	7 //Command with data, +/- sign
#define RWD	8 //Read/Write, add data directly to command string (i.e. no =)
#define WC	9 //Write with same uID / differing command strings, no data
#define CMWX	10 // Write data bits as character
#define	POST	11 // special handing for Program Table Positions
*/
	
YASKNS6CmdDef CODE_SEG CYaskNS6Driver::CL[] = {

	{"ALM",		RON,	1}, // alarm history
	{"JSPDT",	RWP,	2},
	{"PRM",		RWP,	3},
	{"TRM",		CMD,	4},
	{"RDSTT",	RWP,	5},
	{"RSPDT",	RWP,	6},
	{"SPDT",	RWP,	7},
	{"LOOPT",	RWP,	11},
	{"NEXTT",	RWP,	12},
//	{"EVENTT",	RWP,	13},
//	{"EVENTT",	RWP,	14},
	{"ZONENT",	RWP,	15},
	{"ZONEPT",	RWP,	16},
	{"POST",	POST,	PTRF},
	{"POST",	POST,	PTRV},
	{"POST",	POST,	PTWS},
	{"POST",	POST,	PTWF},
	{"POST",	POST,	PTWV},
// Named Items below this line
	{"ABSPGRES",	CM,	20},
	{"ACC",		CMWN,	21},
	{"ALM",		RO,	22}, // Alarm data
	{"ARES",	CM,	23},
	{"ALMTRCCLR",	CM,	24},
	{"INERTIA",	RO,	25},
	{"TUNESTORE",	CM,	26},
	{"CURZERO",	CM,	27},
	{"DBRMS",	RO,	28},
	{"DEC",		CMWN,	29},
	{"ERR",		RO,	30},
	{"ZRN",		CM,	31},
	{"JOGP",	CMWN,	38},
	{"JOGN",	CMWN,	39},
	{"JSPDINIT",	CM,	40},
	{"JSPDSTORE",	CM,	41},
	{"MLTLIMSET",	CM,	42},
	{"MON1",	RO,	43},
	{"MON2",	RO,	44},
	{"MON3",	RO,	45},
	{"MON4",	RO,	46},
	{"MON5",	RO,	47},
	{"MON6",	RO,	48},
	{"MON7",	RO,	49},
	{"MON8",	RO,	50},
	{"MON9",	RO,	51},
	{"MON10",	RO,	52},
	{"MON11",	RO,	53},
	{"MTSIZE",	RO,	54},
	{"MTTYPE",	RO,	55},
	{"IN2",		RO,	56},
	{"IN2TEST",	CMWX,	57},
	{"OUT2",	RO,	58},
	{"OUT2TEST",	CMWX,	59},
	{"TYPE",	RO,	60},
	{"VER",		RO,	61},
	{"YSPEC",	RO,	62},
	{"PRMINIT",	CM,	63},
	{"PGTYPE",	RO,	64},
	{"PGVER",	RO,	65},
	{"PGMINIT",	CM,	66},
	{"PGMSTEP",	RO,	67},
	{"LOOP",	RO,	68},
	{"PGMRES",	CM,	69},
	{"START",	CM,	70},
	{"START",	CMDN,	71},
	{"PGMSTORE",	CM,	72},
	{"STOP",	CM,	73},
	{"EVTIME",	RO,	74},
	{"STA",		CMW,	75},
	{"STI",		CMW,	76},
	{"ST",		CM,	77},
	{"POSA",	CMW,	78},
	{"POSI",	CMW,	79},
	{"SKIP",	CM,	80},
	{"HOLD",	CM,	81},
	{"RSA",		CMW,	83},
	{"RSI",		CMW,	84},
	{"RS",		CM,	85},
	{"SPD",		CMWN,	86},
	{"RGRMS",	RO,	87},
	{"RJOGP",	CMWN,	88},
	{"RJOGN",	CMWN,	89},
	{"RDST",	CMWN,	90},
	{"RSPD",	CMWN,	91},
	{"RES",		CM,	92},
	{"ZSET",	CMW,	93},
	{"STIFF",	RWD,	94},
	{"IN1",		RO,	95},
	{"OUT1",	RO,	96},
	{"SVTYPE",	RO,	97},
	{"SVVER",	RO,	98},
	{"SVYSPEC",	RO,	99},
	{"SVON",	WC,	100},
	{"TRMS",	RO,	101},
	{"ZONEINIT",	CM,	102},
	{"ZONESTORE",	CM,	103},
	{"SVOFF",	WC,	200} // sent when Turn Servo Off is requested
	};

//////////////////////////////////////////////////////////////////////////
//
// YaskNS6 Driver
//

// Instantiator

INSTANTIATE(CYaskNS6Driver);

// Constructor

CYaskNS6Driver::CYaskNS6Driver(void)
{
	m_Ident      = DRIVER_ID;
	
	CTEXT Hex[]  = "0123456789ABCDEF";

	m_pHex       = Hex;

	memset(m_fClearPOST, TRUE, sizeof(m_fClearPOST));
	}

// Destructor

CYaskNS6Driver::~CYaskNS6Driver(void)
{
	}

// Configuration

void MCALL CYaskNS6Driver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CYaskNS6Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CYaskNS6Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CYaskNS6Driver::Open(void)
{
	m_pCL = (YASKNS6CmdDef FAR * )CL;
	}

// Device

CCODE MCALL CYaskNS6Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop     = GetByte(pData);

			m_pCtx->m_bDropText = GetDropText( m_pCtx->m_bDrop );

			m_pCtx->m_uEndCount = 2;

			UINT uDrop = m_pCtx->m_bDrop >> 1;

			if( m_fClearPOST[uDrop] ) {

				memset( m_pCtx->m_PTWF, 0, sizeof( m_pCtx->m_PTWF ) );
				memset( m_pCtx->m_PTWV, 0, sizeof( m_pCtx->m_PTWV ) );
				m_fClearPOST[uDrop] = FALSE;
				}

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskNS6Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskNS6Driver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 4;
	Addr.a.m_Offset	= 0x802; // Always set Answer On
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	Data[0] = 1; // turn on echo

	m_fIsPing = TRUE; // enable write of this parameter

	if( Write( Addr, Data, 1) == 1 ) {

		Addr.a.m_Table = 3;
		Addr.a.m_Offset = 0x802;
		Addr.a.m_Type = addrLongAsLong;

		Data[0] = 0;

		if( Read( Addr, Data, 1 ) ) { // verify parameter setting

			if( Data[0] == 1 ) return 1;
			}
		} 

	return CCODE_ERROR;
	}

CCODE MCALL CYaskNS6Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
 	m_pItem = GetpItem( LOBYTE(Addr.a.m_Table == addrNamed ? Addr.a.m_Offset : Addr.a.m_Table ) );

	if( m_pItem == NULL ) return CCODE_ERROR;

	if( Addr.a.m_Table == 3 || Addr.a.m_Table == 4 ) m_uParameterValue = Addr.a.m_Offset;

	if( ReadNoTransmit(pData, Addr.a.m_Offset) ) return 1;

	ReadOpcode( Addr );

	if( Transact(ISREAD) ) {

		if( GetResponse( Addr, uCount, pData ) ) return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CYaskNS6Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case addrNamed:

			m_pItem = GetpItem( (Addr.a.m_Offset==100 && pData[0]==0) ? 200 : Addr.a.m_Offset );

			break;

		default:
			if( Addr.a.m_Table == 3 || Addr.a.m_Table == 4 ) {

				if( Addr.a.m_Offset == 0x800 || Addr.a.m_Offset == 0x801 )

					return 1; // Disallow write of port setup and baud rate

				else {
					if( Addr.a.m_Offset == 0x802 ) {

						if( !m_fIsPing ) {

							return 1; // Disallow user write of Answer Back
							}
						}
					}

				m_uParameterValue = Addr.a.m_Offset; // parameter data types vary
				}

			m_pItem = GetpItem( Addr.a.m_Table );

			break;
		}

	if( m_pItem == NULL ) return CCODE_ERROR;

	if( WriteNoTransmit(pData, Addr.a.m_Offset) ) return 1;

//**/	AfxTrace0("\r\n** WRITE **\r\n");

	WriteOpcode(Addr, pData);

	if( Transact(ISWRITE) ) return 1;

	return CCODE_ERROR;
	}

// Opcode Handlers

void CYaskNS6Driver::ReadOpcode(AREF Addr)
{
	StartFrame( FALSE );

	AddText( m_pItem->cOP );

	switch( m_pItem->Type ) {

		case RO:
		case RWD:
			break;

		case RON:
			AddByte( (Addr.a.m_Offset % 10) + '0' );
			break;

		case RWP:
			PutRWPParam(Addr.a.m_Table, Addr.a.m_Offset);
			break;

		case POST:
			AddData( Addr.a.m_Offset, 100, 10, NOSIGN);
			break;
		}

	return;
	}

BOOL CYaskNS6Driver::WriteOpcode(AREF Addr, PDWORD pData)
{
	StartFrame( TRUE );

	AddText(m_pItem->cOP);

	switch ( m_pItem->Type ) {

		case RWP:
		case CMD:
			PutRWPData(Addr.a.m_Table, Addr.a.m_Offset, pData[0]);
			break;

		case WC:
		case CM:
			// Nothing to add
			break;

		case CMDN:
			AddData( pData[0], 100, 10, NOSIGN );
			break;

		case CMW:
			AddData( pData[0], 10000000, 10, YESSIGN );
			break;

		case CMWN:
		case RWD:
			AddData( pData[0], 10000000, 10, NOSIGN );
			break;

		case CMWX:
			switch( m_pItem->uID ) {

				case 57:
					AddBinaryData( pData[0], 8 );
					break;

				case 59:
					AddBinaryData( pData[0], 6 );
					break;
				}
			break;

		case POST:
			AddData(*pData, 100, 10, NOSIGN);
			PutSpaceEqualSpace();
			AddPOSTData( *pData );
			break;
		}

	return TRUE;
	}

// Frame Building

void CYaskNS6Driver::StartFrame(BOOL fIsRegWrite)
{
	m_uPtr = 0;

	AddByte( m_pCtx->m_bDropText );
	}

void CYaskNS6Driver::EndFrame(void)
{
	AddByte( CR );
	}

void CYaskNS6Driver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CYaskNS6Driver::AddData(DWORD dData, DWORD dFactor, UINT uBase, BOOL fIsSigned)
{
	BOOL fFirst	= FALSE;
	BYTE b;

	if( fIsSigned ) {

		if( dData & 0x80000000 ) {

			AddByte('-');

			dData ^= 0xFFFFFFFF;

			dData += 1;
			}

		else {
			AddByte('+');
			}
		}

	while( dFactor ) {

		b = m_pHex[(dData / dFactor) % uBase];

		if( b != '0' || fFirst ) {

			AddByte( b );

			fFirst = TRUE;
			}
		
		dFactor /= uBase;
		}

	if( !fFirst ) AddByte('0');
	}

void CYaskNS6Driver::AddText( LPCTXT pText )
{
	for( UINT i = 0; pText[i]; i++ ) AddByte( pText[i] );
	}

// Transport Layer

BOOL CYaskNS6Driver::Transact(BOOL fIsWrite)
{
	Send();

	if( m_pCtx->m_bDropText == '*' ) {

		m_pData->Read(0);

		return TRUE; // Global Axis Selected
		}

	return GetReply(fIsWrite);
	}

void CYaskNS6Driver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for(UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i] );

	Put();
	}

BOOL CYaskNS6Driver::GetReply(BOOL fIsWrite)
{
	UINT uTimer	= 0;
	UINT uCount	= 0;
	UINT uState	= 0;
	UINT uEndCt	= m_pCtx->m_uEndCount;
	UINT uData;

	BYTE bEnd	= CR;
	BYTE bData;

	BOOL fRspRcv	= FALSE;

	SetTimer(1000);

//**/	AfxTrace0("\r\n");

	while( (uTimer = GetTimer()) && !fRspRcv ) {
		
		if( (uData = Get(uTimer)) == NOTHING ) {
			
			continue;
			}

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData ); 

		if( uCount >= sizeof(m_bRx) ) return FALSE;

		m_bRx[uCount++] = bData;

		switch( uState ) {

			case 0:
				if( bData == m_pCtx->m_bDropText ) { // this drop number

					if( fIsWrite ) {

						if(	m_pItem->Type == RWP ||
							m_pItem->Type == CMD
							) {

							uState = 10; // spaces in good response
							}

						else {
							if( m_pItem->uID == 23 ) uState = 15;

							else uState = 3; // spaces only in error response
							}
						}

					else uState = 1;
					}

				else {
					if( TestEnd( bData, bEnd, &uEndCt ) ) return FALSE;
					}

				break;

			case 1:

				if( bData == '=' || bData == ' ' ) { // start of data

					uCount = 0;

					uState = 2;

					uEndCt = 1;
					}

				break;

			case 2:
				fRspRcv = TestEnd( bData, bEnd, &uEndCt );

				break;

			case 3:
				if( bData == 'O' ) {

					uState   = 4;

					m_bRx[0] = 'O';

					uCount   = 1;
					}

				else {
					if( bData == ' ' ) {

						m_bRx[0] = m_bRx[uCount-5];
						m_bRx[1] = m_bRx[uCount-4];
						m_bRx[2] = m_bRx[uCount-3];
						m_bRx[3] = m_bRx[uCount-2];

						uCount = 4;

						uState = 2;
						}
					}

				break;

			case 4:
				if( bData == 'K' ) {
					
					uState = 2;

					uEndCt = 1;
					}

				else uState = 3;

				break;

			case 10:
				if( bData == '=' ) uState = 11;
				break;

			case 11:
				uState = 12;
				break;

			case 12:
				if( bData == 'O' ) uState = 4;

				else {
					if( bData == ' ' ) {

						m_bRx[0] = m_bRx[uCount-5];
						m_bRx[1] = m_bRx[uCount-4];
						m_bRx[2] = m_bRx[uCount-3];
						m_bRx[3] = m_bRx[uCount-2];

						uCount = 4;

						uState = 2;
						}
					}

				break;

			case 15:
				if( bData == 'O' ) uState = 4;

				else {
					if ( bData == ' ' ) {

						SetTimer( 1000 );

						uState = 2;
						}
					}
				break;
			}

		if( !fRspRcv ) {

			if ( TestEnd( bData, bEnd, &uEndCt ) ) return FALSE; // got to end without valid response
			}

		else {
			m_bRx[uCount] = 0;

			m_uRcvCt = uCount;
			}
		}

	if( fIsWrite ) {

		if(	m_bRx[0] == m_pCtx->m_bDropText &&
			m_bRx[1] == 'O'			&&
			m_bRx[2] == 'K'			&&
			uEndCt == 1
			) { // then only 1 CR being received
			m_pCtx->m_uEndCount = 1;
			}

		return TRUE;
		}

	if( !fRspRcv ) return FALSE; // Timeout

	return !( m_pItem->uID != 30 && CheckError() );
	}

BOOL CYaskNS6Driver::TestEnd(BYTE b, BYTE bEnd, UINT * puEnd)
{
	UINT uEnd = *puEnd;

	if( b == bEnd ) {

		uEnd--;

		*puEnd = uEnd;
		}

	return (uEnd == 0);
	}

// Response Handling
BOOL CYaskNS6Driver::GetResponse(AREF Addr, UINT uCount, PDWORD pData)
{
	switch ( FindDataType() ) {

		case 0:
			pData[0] = ProcessAscii();
			break;

		case 1:
			pData[0] = ProcessAsciiOrNumeric();
			break;

		case 2:
			pData[0] = ProcessAsciiOrBits();
			break;

		case 3:
			pData[0] = ProcessBits();
			break;

		case 4:
			pData[0] = ProcessHex();
			break;

		case 5:
			pData[0] = ProcessPRM();
			break;

		case 6:
			pData[0] = ProcessPOST(m_pItem->uID == PTRF);
			break;

		default:
			pData[0] = ProcessDecimal(0);
			break;
		}

	return TRUE;
	}

// Response Processing
DWORD CYaskNS6Driver::ProcessAscii(void)
{
	DWORD d = 0;
	BYTE b;
	WORD w = 0;

	for( UINT i = 0; i < 4; i++ ) {

		d <<= 8;

		b = GetNextChar( &w );

		if( b == CR ) return d;

		if( b ) d += b;

		else d += ' ';
		}

	return d;
	}

DWORD CYaskNS6Driver::ProcessAsciiOrNumeric(void) // i.e. NEXTT
{
	DWORD d = 0;
	WORD w = 0;
	BYTE b;
	BYTE bDigit;

	while( w < m_uRcvCt ) {

		b = GetNextChar( &w );

		if( b == CR ) return d;

		if( IsDecDigit( b, &bDigit ) ) {

			d *= 10;

			d += bDigit;
			}

		else {
			d = 0x454E44; // "END"

			return d;
			}
		}

	return d;
	}

DWORD CYaskNS6Driver::ProcessAsciiOrBits(void)
{
	DWORD d = 0;
	WORD w = 0;
	BYTE b;
	BYTE bDigit;

	while( w < m_uRcvCt ) {

		b = GetNextChar( &w );

		if( b == CR ) return d;

		if( IsDecDigit( b, &bDigit ) ) {

			d <<= 1;

			d += (bDigit == 1) ? 1 : 0;
			}

		else {
			d = bDigit;

			return d;
			}
		}

	return d;
	}

DWORD CYaskNS6Driver::ProcessBits(void)
{
	DWORD d = 0;
	WORD w = 0;
	BYTE b;

	while( w < m_uRcvCt ) {

		b = GetNextChar( &w );

		if( b == CR ) return d;

		d <<= 1;

		d += (b == '1') ? 1 : 0;
		}

	return d;
	}

DWORD CYaskNS6Driver::ProcessHex(void)
{
	DWORD d = 0;
	WORD w = 0;
	BYTE b;
	BYTE bDigit;

	while( w < m_uRcvCt ) {

		b = GetNextChar( &w );

		if( b == CR ) return d;

		if( IsHexDigit( b, &bDigit ) ) {

			d *= 16;

			d += (bDigit);
			}
		}

	return d;
	}

DWORD CYaskNS6Driver::ProcessPRM(void)
{
	if( GetParameterDataType() == 1 ) return ProcessHex();

	return ProcessDecimal(0);
	}

DWORD CYaskNS6Driver::ProcessDecimal(UINT uStart)
{
	DWORD d = 0;
	WORD w  = uStart;
	BYTE b;
	BYTE bDigit;
	BOOL fNeg = FALSE;

	while( w < m_uRcvCt ) {

		b = GetNextChar( &w );

		if( b == CR ) return fNeg ? -d : d;

		if( b == '-' ) fNeg = TRUE;

		else {
			if( IsDecDigit( b, &bDigit ) ) {

				d *= 10;

				d += (bDigit);
				}
			}
		}

	return fNeg ? -d : d;
	}

DWORD CYaskNS6Driver::ProcessPOST(BOOL IsFunctionItem)
{
	UINT uPos = 0;

	while( m_bRx[uPos] == ' ' || m_bRx[uPos] == '=' ) uPos++;

	switch( m_bRx[uPos] ) {

		case 'S': return IsFunctionItem ? POSTSTOP : 0;
		case '-': return IsFunctionItem ? POSTNINF : 0x80000001;
		case '+': return IsFunctionItem ? POSTPINF : 0x7FFFFFFF;
		case 'A': return IsFunctionItem ? POSTABS  : ProcessDecimal(uPos + 1);
		case 'I': return IsFunctionItem ? POSTREL  : ProcessDecimal(uPos + 1);
		}

	return IsFunctionItem ? POSTEMPTY : 0;
	}

BOOL CYaskNS6Driver::CheckError(void)
{
	BYTE b;

	if(	m_bRx[0] == 'E' &&
		m_bRx[3] == 'E' &&
		IsHexDigit(m_bRx[1], &b) &&
		IsHexDigit(m_bRx[2], &b)
		) {

			return TRUE;
			}

	return FALSE;
	}

// Port Access

void CYaskNS6Driver::Put(void)
{
	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

UINT CYaskNS6Driver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers
BYTE CYaskNS6Driver::GetDropText(BYTE bDrop)
{
	if( bDrop < 20 ) return '0' + (bDrop>>1);

	if( bDrop < 33 ) return 'A' + (bDrop>>1) - 10;

	return '*';
	}
YASKNS6CmdDef * CYaskNS6Driver::GetpItem(UINT uID)
{
	YASKNS6CmdDef * p;

	p = m_pCL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == p->uID ) return p;

		p++;
		}

	return NULL;
	}

BOOL CYaskNS6Driver::ReadNoTransmit(PDWORD pData, UINT uOffset)
{
	if( m_pCtx->m_bDropText == '*' ) { // global command

		pData[0] = 0xFFFFFFFF;

		return TRUE;
		}

	if( m_pItem->uID == 4 ) {

		pData[0] = 0xFFFFFFFF;

		return TRUE;
		}

	switch( m_pItem->Type ) {

		case CM: // write only operations
		case CMDN:
		case CMD:
		case CMW:
		case WC:
		case CMWN:
		case CMWX:
			pData[0] = 0xFFFFFFFF;
			return TRUE;

		case POST:
			switch( m_pItem->uID ) {

				case PTWS:
					*pData = 0xFFFFFFFF;
					return TRUE;

				case PTWF:
					*pData = m_pCtx->m_PTWF[uOffset];
					return TRUE;

				case PTWV:
					*pData = m_pCtx->m_PTWV[uOffset];
					return TRUE;
				}

			break;
		}

	return FALSE;
	}

BOOL CYaskNS6Driver::WriteNoTransmit(PDWORD pData, UINT uOffset)
{
	switch( m_pItem->Type ) {

		case RO:
		case RON:
			return TRUE;

		case POST:

			switch( m_pItem->uID ) {

				case PTRF:
				case PTRV:
					return TRUE;

				case PTWF:
					if( *pData <= POSTNINF ) m_pCtx->m_PTWF[uOffset] = *pData;
					return TRUE;

				case PTWV:
					m_pCtx->m_PTWV[uOffset] = *pData;
					return TRUE;

				case PTWS:
					return !(*pData >= 0 && *pData < 128);
				}
			break;
		}

	return FALSE;
	}

void CYaskNS6Driver::PutRWPParam(UINT uTable, UINT uOffset)
{
	switch( uTable ) {

		case 2:  // JSPDT
		case 15: // ZONENT
		case 16: // ZONEPT
			AddData( uOffset, 10, 10, NOSIGN );
			break;

		case 3: // PRM
		case 4: // TRM
			AddData( uOffset, 0x100, 16, NOSIGN );
			break;

		default:
			AddData( uOffset, 1000000000, 10, NOSIGN );
			break;
		}
	}

void CYaskNS6Driver::PutRWPData(UINT uTable, UINT uOffset, DWORD dData)
{
	PutRWPParam( uTable, uOffset );
	PutSpaceEqualSpace();

	switch( uTable ) {

		case 2: // JSPDT
		case 6: // RSPDT
		case 7: // SPDT
			AddData( dData, 10000000, 10, NOSIGN );
			return;

		case 5: // RDSTT
			if( dData & 0x80000000 ) {

				AddByte( '-' );
				}

			else {
				AddData( dData, 10000000, 10, NOSIGN );
				}
			return;

		case 15: // ZONENT
		case 16: // ZONEPT
			AddData( dData, 10000000, 10, YESSIGN );
			return;

		case 11: // LOOPT
		case 12: // NEXTT
			AddData( dData, 10000, 10, NOSIGN );
			return;

		case 3: // PRM
		case 4: // TRM

			UINT uType = GetParameterDataType();

			switch( uType ) {

				case 0:
					AddData( dData, 10000000, 10, YESSIGN );
					break;

				case 1:
					AddData( dData, 0x10000000, 16, NOSIGN );
					break;

				default:
					AddData( dData, 10000000, 10, NOSIGN );
					break;
				}
			return;
		}
	}

UINT CYaskNS6Driver::FindDataType(void)
{
	switch( m_pItem->uID ) {

		case 1:	 // Alarm History
		case 22: // Alarms
		case 30: // Error
			return 0; // Ascii type response

		case 12: // NextT
			return 1; // Ascii or numeric

		case 56: // In2
		case 58: // Out2
		case 95: // In1
		case 96: // Out1
			return 2; // Ascii or Characters as bits

		case 48: // Mon6
			return 3; // Characters as bits

		case 54: // Mtsize
		case 55: // Mttype
		case 60: // Type
		case 61: // Ver
		case 64: // Pgtype
		case 65: // Pgver
		case 97: // Svtype
		case 98: // Svversion
			return 4; // Hex data

		case 3: // PRM
			return 5; // Varying data types

		case PTRF:
		case PTRV:
			return 6; // POST Read response

		default:
			return 255; // Decimal response


		}
	}

UINT CYaskNS6Driver::GetParameterDataType(void)
{
	switch( m_uParameterValue ) {

		case 0x81B:
		case 0x81C:
		case 0x81D:
			return 0;

		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 0x10B:
		case 0x110:
		case 0x200:
		case 0x207:
		case 0x408:
		case 0x50A:
		case 0x50B:
		case 0x50C:
		case 0x50D:
		case 0x50E:
		case 0x50F:
		case 0x510:
		case 0x511:
		case 0x512:
			return 1;

		default:
			return 2;
		}
	}

BYTE CYaskNS6Driver::GetNextChar(PWORD pPtr)
{
	BYTE b;
	BOOL fGot = FALSE;
	WORD u = *pPtr;

	while( !fGot ) {

		b = m_bRx[u];

		fGot = b != ' ';

		u++;

		if( u > m_uRcvCt ) return CR;
		}

	*pPtr = u;

	return b;
	}

BOOL CYaskNS6Driver::IsDecDigit(BYTE b, PBYTE pDigit)
{
	if( b >= '0' && b <= '9' ) {

		*pDigit = b - '0';

		return TRUE;
		}

	return FALSE;
	}

BOOL CYaskNS6Driver::IsHexDigit(BYTE b, PBYTE pDigit)
{
	if( IsDecDigit( b, pDigit ) ) {

		return TRUE;
		}

	if( b >= 'A' && b <= 'F' ) {

		*pDigit = b - '7';

		return TRUE;
		}

	return FALSE;
	}

void CYaskNS6Driver::AddBinaryData(DWORD dData, UINT uCount)
{
	DWORD dMask = 1;

	dMask <<= (uCount-1);

	while ( dMask ) {

		AddByte( (dMask & dData) ? '1' : '0' );

		dMask >>= 1;
		}
	}

void CYaskNS6Driver::AddPOSTData(DWORD dData)
{
	DWORD d = m_pCtx->m_PTWV[dData];

	switch( m_pCtx->m_PTWF[dData] ) {

		case POSTSTOP:
			AddByte('S');
			AddByte('T');
			AddByte('O');
			AddByte('P');
			return;

		case POSTABS:
			AddByte('A');
			break;

		case POSTREL:
			AddByte('I');
			break;

		case POSTPINF:
			AddByte('+');
			AddInfinite();
			return;

		case POSTNINF:
			AddByte('-');
			AddInfinite();
			return;

		case POSTEMPTY:
		default:
			AddByte('-');
			return;
		}

	AddData( d, 10000000, 10, YESSIGN );
	}

void CYaskNS6Driver::AddInfinite(void)
{
	AddByte('I');
	AddByte('n');
	AddByte('f');
	AddByte('i');
	AddByte('n');
	AddByte('i');
	AddByte('t');
	AddByte('e');
	}

void CYaskNS6Driver::PutSpaceEqualSpace(void)
{
	AddByte(' ');
	AddByte('=');
	AddByte(' ');
	}

// End of File
