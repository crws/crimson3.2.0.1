
//////////////////////////////////////////////////////////////////////////
//
// Yaskawa NS600 Driver
//

#define YASKNS6_ID 0x3384

// Type definitions
#define RO	0 //Read Only
#define RON	1 //Read Only with Numeric
#define RWP	2 //Read/Write with numeric
#define CM	3 //Command with no data
#define	CMDN	4 //Command with numeric, no data
#define CMD	5 //Command with numeric & data,
#define CMWN	6 //Command with data, no sign
#define CMW	7 //Command with data, +/- sign
#define RWD	8 //Read/Write, add data directly to command string (i.e. no =)
#define WC	9 //Write with same uID / differing command strings, no data
#define CMWX	10 // Write data bits as characters
#define	POST	11 // special handing for Program Table Position

// POST ID's
#define	PTRF	110
#define	PTRV	111
#define	PTWS	112
#define	PTWF	113
#define	PTWV	114

#define	POSTEMPTY	0
#define	POSTREL		1
#define	POSTABS		2
#define	POSTSTOP	3
#define	POSTPINF	4
#define	POSTNINF	5

struct FAR YASKNS6CmdDef {
	char	cOP[10];
	UINT	Type;
	UINT	uID;
	};

#define NOSIGN	FALSE
#define	YESSIGN	TRUE

#define ISREAD	FALSE
#define ISWRITE	TRUE

class CYaskNS6Driver : public CMasterDriver
{
	public:
		// Constructor
		CYaskNS6Driver(void);

		// Destructor
		~CYaskNS6Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_bDropText;
			UINT	m_uEndCount;
			DWORD	m_PTWF[128];
			DWORD	m_PTWV[128];
			};

		CContext * m_pCtx;

		// Data Members
		BOOL	m_fIsPing;
		BOOL	m_fClearPOST[17];
		BYTE	m_bTx[64];
		BYTE	m_bRx[64];
		UINT	m_uPtr;
		UINT	m_uRcvCt;
		UINT	m_uParameterValue;
		LPCTXT  m_pHex;
		static	YASKNS6CmdDef CODE_SEG CL[];
		YASKNS6CmdDef FAR * m_pCL;
		YASKNS6CmdDef FAR * m_pItem;
	
		// Opcode Handlers
		void	ReadOpcode(AREF Addr);
		BOOL	WriteOpcode(AREF Addr, PDWORD pData);
		
		// Frame Building
		void	StartFrame(BOOL fIsRegWrite );
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, DWORD dFactor, UINT uBase, BOOL fIsSigned);
		void	AddText( LPCTXT pText );
		
		// Transport Layer
		BOOL	Transact(BOOL fIsWrite);
		void	Send(void);
		BOOL	GetReply(BOOL fIsWrite);
		BOOL	TestEnd( BYTE b, BYTE bEnd, UINT * puEnd );

		// Response Handling
		BOOL	GetResponse(AREF Addr, UINT uCount, PDWORD pData);

		// Response Processing
		DWORD	ProcessAscii(void);
		DWORD	ProcessAsciiOrNumeric(void);
		DWORD	ProcessAsciiOrBits(void);
		DWORD	ProcessBits(void);
		DWORD	ProcessHex(void);
		DWORD	ProcessPRM(void);
		DWORD	ProcessDecimal(UINT uStart);
		DWORD	ProcessPOST(BOOL IsFunctionItem);
		BOOL	CheckError(void);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		BYTE	GetDropText(BYTE bDrop);
		YASKNS6CmdDef *	GetpItem( UINT uID );
		BOOL	ReadNoTransmit(PDWORD pData, UINT uOffset);
		BOOL	WriteNoTransmit(PDWORD pData, UINT uOffset);
		void	PutRWPParam( UINT uTable, UINT uOffset );
		void	PutRWPData( UINT uTable, UINT uOffset, DWORD dData );
		UINT	FindDataType(void);
		UINT	GetParameterDataType(void);
		BYTE	GetNextChar(PWORD pPtr);
		BOOL	IsDecDigit( BYTE b, PBYTE pDigit );
		BOOL	IsHexDigit( BYTE b, PBYTE pDigit );
		void	AddBinaryData(DWORD dData, UINT uCount);
		void	AddPOSTData(DWORD dData);
		void	AddInfinite(void);
		void	PutSpaceEqualSpace(void);

	};

// End of File
