
//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "../YUnivSMC/yunivsmc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Galil Serial Driver : Yaskawa Universal SMC Driver
//

class CGalilSerialDriver : public CYaskawaUnivSMCDriver
{
	public:
		// Constructor
		CGalilSerialDriver(void);
	};

// End of File
