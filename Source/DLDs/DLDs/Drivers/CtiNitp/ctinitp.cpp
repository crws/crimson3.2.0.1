#include "intern.hpp"

#include "ctinitp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CTI NITP Master Serial Driver
//

// Instantiator

INSTANTIATE(CCtiNitpMaster);

// Constructor

CCtiNitpMaster::CCtiNitpMaster(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CCtiNitpMaster::~CCtiNitpMaster(void)
{
	}

// End of File
