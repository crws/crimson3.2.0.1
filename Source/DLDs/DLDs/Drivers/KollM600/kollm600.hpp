
#ifndef	KOLLMORGEN600BASE_HPP
#include "km6base.hpp"
#endif

//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen 600 Driver
//
class CKollmorgen600BaseDriver;

class CKollmorgen600Driver : public CKollmorgen600BaseDriver
{
	public:
		// Constructor
		CKollmorgen600Driver(void);

		// Destructor
		~CKollmorgen600Driver(void);

	protected:
		// Transport Specific

		// Start Frame
		void	StartFrame(UINT * uCount);

		// Transport Layer
		void	Send(PCBYTE pTx, UINT uCount);
		BOOL	GetReply(PBYTE pRx, UINT uMax, BYTE bTerminator);

		// Port Access
		UINT	Get(UINT uTime);
	};

// End of File
