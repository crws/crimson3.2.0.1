
//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen 600 Base Driver
//
#include "koll6def.hpp" // Get defines

#ifndef	KOLLMORGEN600BASE_HPP
#define KOLLMORGEN600BASE_HPP
#endif

//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen 600 Base Driver
//

class CKollmorgen600BaseDriver : public CMasterDriver
{
	public:
		// Constructor
		CKollmorgen600BaseDriver(void);

		// Destructor
		~CKollmorgen600BaseDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			DWORD	m_UserText[MAXUSRBYTES + 1];
			DWORD	m_RspErr[MAXUSRBYTES + 1];
			DWORD	m_MTMUX[MAXMTMUX+1];
			DWORD	m_OCOPY[MAXOCOPY+1];
			BYTE	m_OCOPYS;
			BYTE	m_OCOPYD;
			};
		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[MAXTXBUFFER];
		BYTE	m_bRx[MAXRXBUFFER];
		PBYTE	m_pTx;
		PBYTE	m_pRx;
		UINT	m_uPtr;

		// Hex Lookup
		LPCTXT	m_pHex;

		// Command Definition Table
		static KOLLMORGEN600CmdDef CODE_SEG CL[];
		KOLLMORGEN600CmdDef	FAR * m_pCL;
		KOLLMORGEN600CmdDef	FAR * m_pItem;

		// Execute Read/Write
		BOOL	 Read(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL	Write(AREF Addr, PDWORD pData, UINT * pCount);

		// Frame Building
		void	AddCommand(UINT uOffset);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddByteAsc(BYTE bData);
		void	AddWord(WORD wData);
		void	AddWordAsc(WORD wData);
		void	AddLong(DWORD dData);
		void	AddLongAsc(DWORD dData);
		void	AddText(PCTXT pChar);
		void	AddData(PDWORD dData, UINT uType, UINT uCount);
		void	AddOCOPYData(BYTE bData);

		// Data String Building
		void	MakeLongData(PDWORD pData, UINT uCount);
		void	MakeText( char * c, PDWORD pData, UINT uCount );
		void	MakeRealData(DWORD  dData, UINT uCount);
		void	MakeUserText(PDWORD pData, UINT uCount);

		// Response Handling
		void	GetResponse(PDWORD pData, UINT uType, UINT *pCount);
		void	GetStringResponse(PDWORD pData, UINT uCount);
		void	GetStatusResponse(PDWORD pData, UINT uOffset, UINT uCount);
		void	GetIOStatusResponse(PDWORD pData, UINT uOffset, UINT uCount);
		void	GetListResponse(PDWORD pData, UINT uOffset, UINT uCount);

		// Find Command Item
		BOOL	SetpItem( AREF Addr );

		// Helpers
		DWORD	GetValue(UINT * pPos, BOOL fIsReal);
		DWORD	GetHexValue(UINT *pPos);
		BOOL	NoReadTransmit(UINT uType, PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(PDWORD pData);
		UINT	FindRxData(UINT * pPos);
		BOOL	CheckIfNumeric(UINT uPos);
		BOOL	IsNumeric(BYTE b);
		BOOL	GetHex(BYTE b, PBYTE r);
		BOOL	IsStringItem(void);
		BOOL	IsSignedItem(void);
		BOOL	FindNextValue(UINT *pPos);
		BYTE	DoPrompt(UINT uPrompt);
		BOOL	DoRespErr(void);
		BOOL	CheckPrompt(void);

		// Transport Specific

		// Start Frame
		virtual	void	StartFrame(UINT * uCount);

		// Transport Layer
		virtual	void	Send(PCBYTE pTx, UINT uCount);
		virtual	BOOL	GetReply(PBYTE pRx, UINT uMax, BYTE bTerminator);
	};

// End of File
