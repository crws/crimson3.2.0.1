//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen 600 Driver Definitions
//

// Data Type Abbreviations
#define	AN	addrNamed
#define	BT	addrBitAsBit
#define	BB	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	RO	1 // Read Only
#define	RW	2 // Read Write
#define	WO	3 // Write Only, with data
#define	WC	4 // Command, no data

#define	RTNZERO	40960000

#define	MAXCOMMAND	12
#define	MAXTXBUFFER	80
#define	MAXRXBUFFER	80

// Command Definitions

struct FAR KOLLMORGEN600CmdDef {
	UINT	uID;			// Config Table/Offset
	char	cCmd[MAXCOMMAND];	// Text string
	UINT	uRW;			// Read/Write Status
	BOOL	fTbl;			// Table Opcode
	};

// Selection ID's

#define	ACC		1	// "Acceleration", @@WW
#define	ACCR		2	// "Acceleration - Home/Jog", @@WW
#define	ACCUNIT		3	// "Type of acceleration command", @@LL
#define	ACTFAULT	4	// "Active Fault Mode", @@BB
#define	ACTIVE		5	//R "Output stage active/inhibited", @@BB
#define	ACTRS232	6	// "Activate RS232 Watchdog", @@BB
#define	ADDR		7	// "Multidrop Address", @@BB
#define	ADDRFB		8	// "Fieldbus Address - Drive 400 Slave", @@WW
#define	AENA		9	// "Auto-Enable", @@BB
#define	ALIAS		10	// "Drive Name String", @@LL
#define	AN10TX		11	// "Additional Torque Tuning Value", @@WW
#define	AN11NR		12	// "Max. # of change-able INxTRIG", @@BB
#define	AN11RANGE	13	// "Range of the analog change of INxTRIG", @@LL
#define	ANTRIG		14	//Table "Analog Output Scaling", @@LL
#define	ANCNFG		15	// "Analog Input Configuration", @@BB
#define	ANDB		16	// "Dead Band of Analog Velocity Input", @@RR1
#define	ANIN		17	//Table "Analog Input Voltage", @@LL
#define	ANOFF		18	//Table "Analog Input Offset", @@WW
#define	ANOUT		19	//Table "Analog Output Configuration", @@BB
#define	ANZERO		20	//Table W "Analog Input Zero", @@BIT
#define	AUTOHOME	21	// "Automatic Homing", @@BB
#define	AVZ		22	//Table "Analog Output Filter Time Constant", @@RR1

#define	CALCRK		23	//W "Calculate Resolver Parameters", @@BIT
#define	CBAUD		24	// "CAN Bus Baud Rate", @@WW
#define	CLRFAULT	25	//W "Clear Drive Fault", @@BIT
#define	CLRHR		26	//W "Bit 5 of STAT is cleared", @@BIT
#define	CLRORDER	27	//W "Delete a Motion Task", @@WW
#define	CLRWARN		28	// "Warning Mode", @@BB
#define	COLDSTART	29	//W "Drive Reset", @@BIT

#define	DAOFFSET	30	//Table "Analog Output Offset", @@WW
#define	DEC		31	// "Deceleration", @@WW
#define	DECDIS		32	// "Deceleration - Disable Output", @@WW
#define	DECR		33	// "Deceleration Ramp - Home/Jog", @@WW
#define	DECSTOP		34	// "Fast Stop Ramp", @@WW
#define	DEVICE		35	// Table "Device ID Numeric Data", @@LL
#define	DICONT		36	//R "Continuous Current", @@RR1
#define	DIPEAK		37	//R "Peak Rated Current", @@RR1
#define	DIR		38	// "Count Direction", @@BB
#define	DIS		39	//W "Disable", @@BIT
#define	DREF		40	// "Homing Direction", @@BB
#define	DRVSTAT		41	// "Internal Status", @@LL
#define	EN		42	// "Enable", @@BIT
#define	ENCIN		43	// "Encoder Pulse Input", @@LL
#define	ENCLINES	44	// "SinCos Encoder Resolution", @@WW
#define	ENCMODE		45	// "Encoder Emulation", @@BB
#define	ENCOUT		46	// "Encoder Emulation Resolution", @@LL
#define	ENZERO		47	// "Zero Pulse Offset", @@WW
#define	ERRCODE		48	// Table "Fault Message String (16 Chars.)", @@LL
#define	ERRCODEA	49	// "Output Error Register", @@LL
#define	EXTMUL		50	// "External Encoder Multiplier", @@WW
#define	EXTWD		51	// "External Fieldbus Watchdog", @@LL
#define	ERSP		226	// "Response Error String", @@LL

#define	FBTYPE		52	// "Encoder/Resolver Selection", @@BB
#define	FLASH		53	// "Send Data to External Flash", @@BB
#define	FLTCNT		54	//R Table33 "Fault Frequency", @@LL
#define	FLTHIST		55	//R Table20 "Fault History", @@LL
#define	FLUXM		56	// "Rated Flux", @@RR

#define	GEARI		57	// "Gearing Input Factor", @@WW
#define	GEARMODE	58	// "Secondary Position Source", @@BB
#define	GEARO		59	// "Gearing Output Factor", @@WW
#define	GP		60	// "Proportional Gain - Position Loop", @@RR1
#define	GPFBT		61	// "Feed Forward - Actual Current", @@RR1
#define	GPFFV		62	// "Feed Forward - Velocity", @@RR1
#define	GPTN		63	// "Integral - Position Loop", @@RR1
#define	GPV		64	// "Proportional Gain - Velocity Controller", @@RR1
#define	GV		65	// "Proportional Gain - Velocity Loop", @@RR1
#define	GVFBT		66	// "First Order TC - Velocity Loop", @@RR1
#define	GVFILT		67	// "% Output Filtered - Velocity Loop", @@BB
#define	GVFR		68	// "Feed Forward - Actual Velocity", @@RR1
#define	GVT2		69	// "Second TC - Velocity Loop", @@RR1
#define	GVTN		70	// "Integral - Velocity Loop", @@RR1

#define	HVER		71	//R Table13 "Hardware Version String", @@LL

#define	ICURR		72	//R "Current", @@RR
#define	I2T		73	//R "RMS Current Loading %", @@LL
#define	I2TLIM		74	// "I2T Warning %", @@BB
#define	ICMD		75	//R "Current Command", @@RR
#define	ICONT		76	// "Rated Current", @@RR
#define	ID		77	//R "D-Component of Current Monitor", @@RR
#define	IMAX		78	//R "Current Limit for Drive/Motor", @@RR
#define	INAD		225	//R "A/D Channels Input counts"
#define	INS		79	//R Table4 "Digital Input Status", @@BB
#define	INMODE		80	//Table4 "Digital Input Function", @@BB
#define	INTRIG		81	//Table4 "INMODE Trigger Data", @@LL
#define	INPOS		82	//R "In-Position Status", @@BB
#define	INPT		83	// "In-Position Delay", @@WW
#define	IPEAK		84	// "Peak Current - Application", @@RR1
#define	IQ		85	//R "Q-Component of Current Monitor", @@RR
#define	ISCALE		86	//Table2 "Analog Current Scaling", @@RR1

#define	KILL		87	//W "Kill", @@BIT
#define	KC		88	// "I-Controller Prediction Current", @@RR1
#define	KEYLOCK		89	// "Lock the Push Buttons", @@BB
#define	KTN		90	// "Integral - Current Controller", @@RR1

#define	LIND		91	// "Stator Inductance", @@RR1
#define	LATCH		92	//R Table2 "Latched 32/16-Bit Position (DRVSTAT)", @@LL
#define	LATCHX		93	//R Table2 "Latched 32/16-Bit Position (TRJSTAT)", @@LL
#define	LTCH16		220	//  Table2 "16 bit Position @ INx Rising" NOV 2008 S300
#define	LTCH32		221	//  Table2 "32 bit Position @ INx Rising" NOV 2008 S300
#define	LED		94	// Table 3 "LED Display", @@BB
#define	LEDSTAT		95	// "Display Page", @@WW
#define	LOAD		96	//W "Load Parameters from EEPROM", @@BIT
#define	MAXTEMPE	97	// "Switch off - Ambient �C", @@WW
#define	MAXTEMPH	98	// "Switch off - Heat Sink �C", @@WW
#define	MAXTEMPM	99	// "Switch off - Motor (Ohms)", @@RR1
#define	MBRAKE		100	// "Motor Holding Brake Select", @@BB
#define	MDBCNT		101	//R "Number of Motor Data Sets", @@BB
#define	MDBGET		102	//R "Get Actual Motor Data Set String", @@LL
#define	MDBSET		103	// "Set Actual Motor Data Set", @@WW
#define	MH		104	//W "Start Homing", @@BIT
#define	MICONT		105	// "Motor Continuous Current Rating", @@RR1
#define	MIPEAK		106	// "Motor Peak Current Rating", @@RR1
#define	MJOG		107	//W "Start Jog Mode", @@BIT
#define	MKT		108	// "Motor KT", @@RR1
#define	MLGC		109	// "Adaptive Gain Q-rated - Current Loop", @@RR1
#define	MLGD		110	// "Adaptive Gain D - Current Loop", @@RR1
#define	MLGP		111	// "Adaptive Gain Q-peak - Current Loop", @@RR1
#define	MLGQ		112	// "Adaptive Gain Absolute - Current Loop", @@RR1
#define	MNAME		113	// "Motor Name String", @@LL
#define	MNUMBER		114	// "Motor Number", @@WW
#define	MONITOR		115	// Table2 "Monitor Output Voltage", @@WW
#define	MOVE		116	// "Start Motion Task", @@WW
#define	MPHASE		117	// "Motor Phase, Feedback Offset", @@WW
#define	MPOLES		118	// "Number of Motor Poles", @@BB
#define	MRD		119	//W "Homing to Resolver Zero, Mode 5", @@BIT
#define	MRESBW		120	// "Resolver Bandwidth", @@WW
#define	MRESPOLES	121	// "Number of Resolver Poles", @@BB
#define	MSPEED		122	// "Maximum Rated Motor Velocity", @@RR1
#define	MTANGLP		123	// "Current Lead", @@WW
#define	MTMUX		214	// "Presetting for Motion Task", @@WW
#define	MTMUXR		218	// "MTMUX Response String"
#define	MUNIT		124	// "Units for Velocity Parameters", @@BB
#define	MVANGLB		125	// "Velocity Lead (Start Phi)", @@LL
#define	MVANGLF		126	// "Velocity Lead (Limit Phi)", @@WW
#define	MVANGLP		127	// "Velocity Lead (Commutation Angle)", @@WW

#define	NONBTB		128	// "Mains-BTB Check On/Off", @@BB
#define	NREF		129	// "Homing Mode", @@BB

#define	O_ACC		130	// Table2 "Acceleration Time - Motion Task 0", @@WW
#define	O_C		131	// "Control Variable - Motion Task 0", @@WW
#define	O_DEC		132	// Table2 "Deceleration Time - Motion Task 0", @@WW
#define	O_FN		133	// "Next Task Number - Motion Task 0", @@WW
#define	O_FT		134	// "Delay before Next Motion Task", @@WW
#define	O_P		135	// "Target Position - Motion Task 0", @@LL
#define	O_V		136	// "Target Speed - Motion Task 0", @@LL
#define	OUTS		137	//  Table3 "Digital Output Status", @@BB
#define	OCOPYQ		215	// "Execute OCOPY <data> = Quantity", @BB
#define	OCOPYS		216	// "...OCOPY Source Task Number", @BB
#define	OCOPYD		217	// "...OCOPY Destination Task Number", @BB
#define	OCOPYR		219	// "OCOPY Response String"
#define	OMODE		138	//Table3 "Digital Output Function", @@BB
#define	OTRIG		139	//Table3 "OMODE Trigger Data", @@LL
#define	OPMODE		140	// "Operating Mode", @@BB
#define	OPTION		141	//R "Option Slot ID", @@WW

#define	PASSCM		222	// "Parameter Change Password" NOV 2008 S300
#define	PBAL		142	// "Regen Power - Actual", @@LL
#define	PBALMAX		143	// "Regen Power - Maximum", @@LL
#define	PBALRES		144	// "Regen Resistor - Select", @@BB
#define	PBAUD		145	// "Profibus Baud Rate", @@RR1
#define	PE		146	//R "Following Error - Actual", @@LL
#define	PEINPOS		147	// "In-Position Window", @@LL
#define	PEMAX		148	// "Following Error - Maximum", @@LL
#define	PFB		149	// "Actual Position from Feedback", @@LL
#define	PFB0		150	//R "Position from External Encoder", @@LL
#define	PGEARI		151	// "Position Resolution - Numerator", @@LL
#define	PGEARO		152	// "Position Resolution - Denominator", @@LL
#define	PMODE		153	// "Line Phase Error Mode", @@BB
#define	POSCNFG		154	// "Axes Type", @@BB
#define	PRD		155	//R "20-bit Position Feedback", @@LL
#define	PSTATE		156	//R "Profibus State String", @@LL
#define	PTMIN		157	// "Min. Acceleration for Motion Tasks", @@WW
#define	PUNIT		158	// "Position Resolution", @@LL
#define	PV		159	//R "Actual Velocity - Position Loop", @@LL
#define	PVMAX		160	// "Max. Velocity - Position Loop", @@LL
#define	PVMAXN		161	// "Max. Neg Velocity - Position Loop", @@LL
#define	PVMAXP		223	// "Maximum Positive Velocity" NOV 2008 S300

#define	READY		162	//R "Software Enable Status", @@BB
#define	REFIP		163	// "Peak Rated Current for Homing 7", @@RR1
#define	REFPOS		164	// "Reference Switch Position", @@LL
#define	REMOTE		165	//R "Hardware Enable Status", @@BB
#define	RESPHASE	166	// "Resolver Phase", @@WW
#define	RK		167	// "Resolver Sine Gain Adjust", @@WW
#define	ROFFS		168	// "Reference Offset", @@LL
#define	ROFFSA		224	// "Offset to Encoder Position" NOV 2008 S300
#define	RS232T		169	// "RS232 Watchdog Time", @@
#define	RSTVAR		170	//W "Restore Variables to Default", @@BIT

#define	S_STOP		171	//W "Stop Motor and Disable Drive", @@BIT
#define	SAVE		172	//W "Save Data in EEPROM", @@BIT
#define	SCANX		173	//W "Restart Communications", @@BIT
#define	SERIALNO	174	//R "Drive Serial Number", @@LL
#define	SETREF		175	//W "Set Reference Point", @@BIT
#define	SLOTIO		176	//R "I/O States - Expansion Card", @@LL
#define	SSIGRAY		177	// "SSI Code Select", @@BB
#define	SSIINV		178	// "SSI Clock", @@BB
#define	SSIMODE		179	// "SSI Mode", @@BB
#define	SSIOUT		180	// "SSI Baud Rate", @@BB
#define	STAT		181	//R "Drive Status Word", @@WW
#define	STATCODE	182	//R "Status Variable Warnings", @@LL
#define	STATIO		183	//R Table8 "I/O Status", @@BB
#define	STATUS		184	//R Table5 "Detailed Amplifier Status", @@WW
#define	STOP		185	//W "Stop Motion Task", @@BIT
#define	SWCNFG		186	// "Position Register 1...4 Configuration", @@WW
#define	SWCNFG2		187	// "Position Register 0 & 5 Configuration", @@WW
#define	SWE		188	//Table 6 "Position Register Data", @@LL
#define	SWEN		189	//Table 6 "Cam Position Register Data", @@LL
#define	TCURR		190	// "Digital Current Command", @@RR1
#define	TEMPE		191	//R "Ambient Temperature", @@LL
#define	TEMPH		192	//R "Heat Sink Temperature", @@LL
#define	TEMPM		193	//R "Motor Temperature", @@LL
#define	TRJSTAT		194	//R "Status 2", @@LL
#define	TRUN		195	//R "Run-Time Counter Value (secs)", @@LL
#define	UID		196	// "User ID", @@WW
#define	VEL		197	//R "Actual Velocity", @@RR
#define	VBUS		198	//R "DC-bus Voltage", @@LL
#define	VBUSBAL		199	// "Maximum Line Voltage", @@WW
#define	VBUSMAX		200	// "Maximum DC-bus Voltage", @@LL
#define	VBUSMIN		201	// "Minimum DC-bus Voltage", @@LL
#define	VCMD		202	//R "Internal Velocity RPM", @@RR
#define	VER		203	//R "Firmware Version String", @@LL
#define	VJOG		204	// "Jog Mode Speed", @@LL
#define	VLIM		205	// "Maximum Velocity", @@RR1
#define	VMAX		206	//R "Maximum System Speed", @@RR
#define	VMIX		207	// "Velocity Mix", @@RR1
#define	VMUL		208	// "Velocity Scale Factor", @@LL
#define	VOSPD		209	// "Overspeed", @@RR1
#define	VREF		210	// "Homing Speed", @@LL
#define	VSCALE		211	// Table2 "Velocity Scaling - Analog Input", @@WW

#define	WMASK		212	// "Warning as Fault Mask", @@LL

#define	USRDEF		213	//W Table10 "User Defined Command String", @@LL

#define	MAXUSRDEF	 10	// Maximum permitted User String Response is 39 characters.
#define	MAXUSRBYTES	 79
#define	MAXMTMUXDEF	 13
#define	MAXMTMUX	 51
#define	MAXOCOPYDEF	 13
#define	MAXOCOPY	 51

#define	PROMPT		300	// Prompt to initialize comms
