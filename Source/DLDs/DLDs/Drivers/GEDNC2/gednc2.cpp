
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "gednc2.hpp"
	
GEDNC2CmdDef CODE_SEG CGEDNC2MasterDriver::CL[] = {

	{ RMP,  "MP",  TRT	}, // Tool Position - Machine
	{ RWP,  "WP",  TRT	}, // Tool Position - Absolute
	{ RSP,  "SP",  TRT	}, // Tool Position - Skip Signal Detect
	{ RSE,  "SE",  TRT	}, // Servo Delay for Axis
	{ RAE,  "AE",  TRT	}, // Accel / Decel Delay for Axis
	{ RMI,  "MI",  TRT	}, // Machine Interface Signals
	{ RPN,  "PN",  TRM	}, // Current Program Number
	{ RSN,  "SN",  TRM	}, // Current Sequence Number
	{ RPAP, "PA",  TRPT	}, // Read CNC Parameter Value
	{ RPAA, "PA",  TRPT	}, // CNC Parameter Axis Exponent
	{ WPA,  "PA",  TRPR	}, // Execute CNC Parameter Write...
	{ WPAP, "PA",  TRC	}, //   CNC Parameter Value to Write
	{ WPAA, "PA",  TRC	}, //   CNC Axis Exponent to Write
	{ RWPE, "PE",  TRPRW	}, // Pitch Error Compensation Value
	{ RTOD, "TO",  TRPT	}, // Read Tool Offset Cutter Wear
	{ RTOK, "TO",  TRPT	}, // Read Tool Offset Cutter Geometry
	{ RTOH, "TO",  TRPT	}, // Read Tool Offset Length Wear
	{ RTOL, "TO",  TRPT	}, // Read Tool Offset Length Geometry
	{ RTOX, "TO",  TRPT	}, // Read Tool Offset X Axis Wear
	{ RTOU, "TO",  TRPT	}, // Read Tool Offset X Axis Geometry
	{ RTOY, "TO",  TRPT	}, // Read Tool Offset Y Axis Wear
	{ RTOV, "TO",  TRPT	}, // Read Tool Offset Y Axis Geometry
	{ RTOZ, "TO",  TRPT	}, // Read Tool Offset Z Axis Wear
	{ RTOW, "TO",  TRPT	}, // Read Tool Offset Z Axis Geometry
	{ RTOR, "TO",  TRPT	}, // Read Tool Offset Tip Radius Wear
	{ RTOP, "TO",  TRPT	}, // Read Tool Offset Tip Radius Geometry
	{ RTOQ, "TO",  TRPT	}, // Read Tool Offset Virtual Direction
	{ WTON, "TO",  TRPR	}, // Write Tool Offset Number...
	{ WTOB, "TO",  TRC	}, //   Select Bit Pattern VPWUQYRZXLHKD(Bit 0)
	{ WTOD, "TO",  TRC	}, //   Cutter Wear to write
	{ WTOK, "TO",  TRC	}, //   Cutter Geometry to write
	{ WTOH, "TO",  TRC	}, //   Length Wear to write
	{ WTOL, "TO",  TRC	}, //   Length Geometry to write
	{ WTOX, "TO",  TRC	}, //   X Axis Wear to write
	{ WTOU, "TO",  TRC	}, //   X Axis Geometry to write
	{ WTOY, "TO",  TRC	}, //   Y Axis Wear to write
	{ WTOV, "TO",  TRC	}, //   Y Axis Geometry to write
	{ WTOZ, "TO",  TRC	}, //   Z Axis Wear to write
	{ WTOW, "TO",  TRC	}, //   Z Axis Geometry to write
	{ WTOR, "TO",  TRC	}, //   Tip Radius Wear to write
	{ WTOP, "TO",  TRC	}, //   Tip Radius Geometry to write
	{ WTOQ, "TO",  TRC	}, //   Virtual Direction to write
	{ RWMV, "MV",  TRPRW	}, // Custom Macro Variable Value
	{ RTLL, "TL",  TRPT	}, // Tool Life Value for Group
	{ RTLQ, "TL",  TRPT	}, // Tool Life Count for Group
	{ RTLT, "TL",  TRPT	}, // Tool Number for Group
	{ RTLH, "TL",  TRPT	}, // Tool Life H Code for Group
	{ RTLD, "TL",  TRPT	}, // Tool Life D Code for Group
	{ RTLC, "TL",  TRPT	}, // Tool Information for Group
	{ RMDG, "MD",  TRT	}, // Modal Information G for Block
	{ RMDD, "MD",  TRT	}, // Modal Information D for Block
	{ RMDE, "MD",  TRT	}, // Modal Information E for Block
	{ RMDH, "MD",  TRT	}, // Modal Information H for Block
	{ RMDL, "MD",  TRT	}, // Modal Information L for Block
	{ RMDM, "MD",  TRT	}, // Modal Information M for Block
	{ RMDN, "MD",  TRT	}, // Modal Information N for Block
	{ RMDO, "MD",  TRT	}, // Modal Information O for Block
	{ RMDS, "MD",  TRT	}, // Modal Information S for Block
	{ RMDT, "MD",  TRT	}, // Modal Information T for Block
	{ RMDF, "MD",  TRT	}, // Modal Information F for Block
	{ RAF,  "AF",  TRT	}, // Feed Rate for Axis
	{ RADI, "AD",  TRT	}, // A/D - General Analog Input
	{ RADS, "AD",  TRT	}, // A/D Load Voltage for Spindle
	{ RADN, "AD",  TRT	}, // A/D Load Voltage for Control Axis
	{ RAL,  "AL",  TRT	}, // Alarm Information
	{ RST,  "ST",  TRT	}, // Status Information
	{ WSL,  "SL",  TRM	}, // Select Part Program
	{ WCS,  "CS",  TRM	}, // Execute a Program
	{ WCC,  "CC",  TRM	}, // Reset
	{ WDI,  "DI",  TRM	}, // Send Operator Message String
	{ WDIS, "DI",  TRC	}, // Set Up Operator Message String
	{ RID,  "ID",  TRT	}, // System ID String
	{ ERR,  "NA",  TRC	}, // Latest NAK (Internal)
	};

//////////////////////////////////////////////////////////////////////////
//
// GE DNC2 Master Driver
//

// Instantiator

INSTANTIATE(CGEDNC2MasterDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CGEDNC2MasterDriver::CGEDNC2MasterDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	CTEXT Tool[]	= " DKHLXUYVZWRPQ";

	m_pHex		= Hex;

	m_pTool		= Tool;

	m_dNak		= 0;
	}

// Destructor

CGEDNC2MasterDriver::~CGEDNC2MasterDriver(void)
{
	}

// Configuration

void MCALL CGEDNC2MasterDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CGEDNC2MasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CGEDNC2MasterDriver::Open(void)
{
	m_pCL = (GEDNC2CmdDef FAR * )CL;
	}

// Device

CCODE MCALL CGEDNC2MasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			memset(m_pCtx->m_dWriteParam, 0, sizeof(m_pCtx->m_dWriteParam) );

			memset(m_pCtx->m_dWriteTool,  0, sizeof(m_pCtx->m_dWriteTool) );

			memset(m_pCtx->m_dMessage,    0, sizeof(m_pCtx->m_dMessage) );

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CGEDNC2MasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CGEDNC2MasterDriver::Ping(void)
{
	DWORD Data[1];

	CAddress Addr;

	Addr.a.m_Table  = RST;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = WW;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CGEDNC2MasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	SetpItem( Addr.a.m_Table );

	if( m_pItem == NULL ) {

		return CCODE_ERROR | CCODE_HARD;
		}

	if( ReadNoTransmit(pData, uCount) ) {

		return (m_pItem->uID == WDI) ? uCount : 1;
		}

	return DoRead(Addr, pData, uCount);
	}

CCODE MCALL CGEDNC2MasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	SetpItem( Addr.a.m_Table );

/**/	AfxTrace3("\r\n*****WRITE***** %d %d %d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( m_pItem == NULL ) {

		return CCODE_ERROR | CCODE_HARD;
		}

	return WriteNoTransmit(pData, uCount) ? uCount : DoWrite(Addr, pData, uCount);
	}

UINT CGEDNC2MasterDriver::DevCtrl(void * pContext, UINT uFunc, PCTXT Value)
{
	m_uPtr = 0;

	PutText(Value);

	return Transact(RECVD) ? 1 : 0;
	}

// Read Handler

CCODE CGEDNC2MasterDriver::DoRead(AREF Addr, PDWORD pData, UINT uCount)
{
	CCODE Result = CCODE_ERROR;

	SelectProcess(FALSE);

	while( m_uState != PROCEND ) {

		if( InitiateLink() ) {

			UINT uPos;

			switch( m_uState ) {

				case PROCRD:
					MakeReadDatagram(Addr);
					m_uState = SendDatagram(RECVD) ? PROCRD1 : PROCEND;
					break;

				case PROCRD1:
					SendMOK(RECVE);
					m_uState = PROCEND;

					if( m_pItem->uID == RID ) {

						PDWORD pp = PDWORD(&m_bRx[4]);

						for( UINT k = 0; k < uCount; k++ ) {

							pData[k] = pp[k];
							}

						Result = uCount;
						}

					else {
						if( IsModalCmd() ) {

							*pData = GetModalData(Addr.a.m_Type);

							return 1;
							}

						*pData = GetData(Addr.a.m_Type, FindRcvPos() );
						Result = 1;
						}

					return Result;

				case PROCPR:
					MakeReadDatagram(Addr);
					m_uState = SendDatagram(RECVPRA) ? PROCPR1 : PROCEND;
					break;

				case PROCPR1:
					m_uState = SendTNB(RECVD) ? PROCPR2 : PROCEND;
					break;

				case PROCPR2:
					m_uState = SendTNB(RECVT0) ? PROCPR3 : PROCEND;
					break;

				case PROCPR3:
					SendMOK(RECVE);
					m_uState = PROCEND;
					*pData = GetData(Addr.a.m_Type, FindRcvPos());
					return 1;
				}
			}

		}

	return Result;
	}

// Write Handler

CCODE CGEDNC2MasterDriver::DoWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	CCODE Result = CCODE_ERROR;

	SelectProcess(TRUE);

	BOOL fSingle = FALSE;
	UINT uSingle = RECVT0;

	while( m_uState != PROCEND ) {

		if( InitiateLink() ) {

			switch( m_uState ) {

				case PROCPW:
					m_uState = StartWrite() ? PROCPW1 : PROCEND;
					break;

				case PROCPW1:

					if( MakeWriteDatagram(Addr, pData, uCount) ) {

						fSingle = TRUE;
						uSingle = RECVM;
						}

					m_uState = !SendDatagram(uSingle) || fSingle ? PROCEND : PROCPW2;

					break;

				case PROCPW2:
					m_uState = SendTFD(RECVM) ? PROCPW3 : PROCEND;
					break;

				case PROCPW3:
					return m_pItem->uID == WDI ? uCount : 1;
				}
					
			}
		}

	return CCODE_ERROR;
	}

// Start Process
void CGEDNC2MasterDriver::SelectProcess(BOOL fIsWrite)
{
	switch( m_pItem->uFirst ) {

		case TRPT:	m_uState = PROCPR;	return;
		case TRPR:	m_uState = PROCPW;	return;
		case TRPRW:	m_uState = fIsWrite ? PROCPW : PROCPR;	return;
		}

	m_uState = PROCRD;
	}

BOOL CGEDNC2MasterDriver::Start(BOOL fIsWrite)
{
	if( InitiateLink() ) {

		StartFrame();

		return TRUE;
		}

	return FALSE;
	}

BOOL CGEDNC2MasterDriver::InitiateLink(void)
{
	m_pData->ClearRx();

	PutByte(ENQ);

	return GetReply(RSTATE0);
	}

BOOL CGEDNC2MasterDriver::SendDatagram(UINT uState)
{
	return Transact(uState);
	}

void CGEDNC2MasterDriver::SendDLE(BYTE b0or1)
{
	m_uPtr = 0;
	AddByte(DLE);
	AddByte(b0or1);
	PutFrame();
	}

BOOL CGEDNC2MasterDriver::SendMOK(UINT uState)
{
	StartFrame();

	AddByte('M');
	AddByte(' ');
	AddByte('O');
	AddByte('K');

	return Transact(uState);
	}

BOOL CGEDNC2MasterDriver::SendTNB(UINT uState)
{
	StartFrame();

	AddByte('T');
	AddByte(' ');
	AddByte('N');
	AddByte('B');

	return Transact(uState);
	}

BOOL CGEDNC2MasterDriver::SendTFD(UINT uState)
{
	StartFrame();

	AddByte('T');
	AddByte(' ');
	AddByte('F');
	AddByte('D');

	return Transact(uState);
	}

// Frame Building

void CGEDNC2MasterDriver::StartFrame()
{
	m_uPtr = 0;

	AddByte(DLE);

	AddByte(STX);

	m_bBCC = 0;
	}

void CGEDNC2MasterDriver::EndFrame(void)
{
	AddByte(DLE);

	AddByte(ETX);

	AddByte(m_bBCC);
	}

void CGEDNC2MasterDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr++] = bData;

		m_bBCC ^= bData;
		}
	}

void CGEDNC2MasterDriver::AddDecWord(UINT uData, UINT uSize)
{
	switch( uSize ) {
		case 10:
			AddByte( m_pHex[(uData/1000000000) % 10] );
		case 9:
			AddByte( m_pHex[(uData/100000000)  % 10] );
		case 8:
			AddByte( m_pHex[(uData/10000000)   % 10] );
		case 7:
			AddByte( m_pHex[(uData/1000000)    % 10] );
		case 6:					 
			AddByte( m_pHex[(uData/100000)     % 10] );
		case 5:
			AddByte( m_pHex[(uData/10000)      % 10] );
		case 4:
			AddByte( m_pHex[(uData/1000)       % 10] );
		case 3:
			AddByte( m_pHex[(uData/100)        % 10] );
		case 2:
			AddByte( m_pHex[(uData/10)         % 10] );
		case 1:
			AddByte( m_pHex[uData              % 10] );
		}
	}

void CGEDNC2MasterDriver::AddHexWord(UINT uData)
{
	AddByte('0');
	AddByte('X');
	AddByte(m_pHex[ (uData>>12) & 0xF ]);
	AddByte(m_pHex[ (uData>> 8) & 0xF ]);
	AddByte(m_pHex[ (uData>> 4) & 0xF ]);
	AddByte(m_pHex[  uData      & 0xF ]);
	}

void CGEDNC2MasterDriver::AddCommand(BOOL fIsWrite)
{
	BOOL AddSpace = FALSE;

	switch( m_pItem->uFirst ) {

		case TRT: AddByte('T');	AddSpace = TRUE; break;
		case TRR: AddByte('R');	AddSpace = TRUE; break;
		case TRM: AddByte('M');	AddSpace = TRUE; break;

		case TRPT:
			AddByte('P');
			AddByte('T');
			break;

		case TRPR:
			AddByte('P');
			AddByte('R');
			break;

		case TRPRW:
			AddByte('P');
			AddByte(fIsWrite ? 'R' : 'T');
			break;
		}

	if( AddSpace ) AddByte(' ');

	PutText( m_pItem->sName );
	}

void CGEDNC2MasterDriver::AddParam( char * cParam )
{
	for( UINT i = 0; i < strlen(cParam); i++ ) {

		AddByte(cParam[i]);
		}
	}
	
void CGEDNC2MasterDriver::PutText(LPCTXT pCmd)
{
	for( UINT i=0; pCmd[i]; i++ ) {

		AddByte( BYTE(pCmd[i]) );
		}
	}

void CGEDNC2MasterDriver::MakeReadDatagram(AREF Addr)
{
	StartFrame();

	AddCommand(FALSE);

	DoCNCPar(Addr);

	DoToolOff();

	DoToolLife(Addr.a.m_Offset);

	DoModal(Addr.a.m_Offset);

	switch( m_pItem->uID ) {

		case RWP:
		case RMP:
		case RSP:
		case RSE:
		case RAE:
			AddAxisPattern(Addr.a.m_Offset);
			break;

		case RMI:
			AddDecWord(Addr.a.m_Offset, 3);
			break;

		case RWPE:
			AddDecWord(Addr.a.m_Offset, 4);
			break;

		case RWMV:
			AddDecWord(Addr.a.m_Offset, 5);
			break;

		case RPN:
		case RSN:
			break;
		}
	}

UINT CGEDNC2MasterDriver::MakeAxisPattern(UINT uAxisNumber)
{
	return 1 << (uAxisNumber - 1);
	}

void CGEDNC2MasterDriver::AddAxisPattern(UINT uAxisNumber)
{
	AddHexWord(MakeAxisPattern(uAxisNumber));
	}

BOOL CGEDNC2MasterDriver::StartWrite(void)
{
	m_uPtr = 0;

	AddByte('P');
	AddByte('R');
	PutText(m_pItem->sName);

	return Transact(RECVPRA);
	}

BOOL CGEDNC2MasterDriver::MakeWriteDatagram(AREF Addr, PDWORD pData, UINT uCount)
{
	AddByte(m_pItem->uFirst != TRM ? 'R' : 'M');
	AddByte(' ');
	PutText(m_pItem->sName);

	switch( m_pItem->uID ) {

		case WPA:
			MakeCNCParWrite(*pData);
			return FALSE;

		case RWPE:
			MakePitchErrWrite(Addr.a.m_Offset, *pData);
			return FALSE;

		case WTON:
			MakeToolOffWrite(*pData);
			return FALSE;

		case RWMV:
			MakeCustomVarWrite(Addr.a.m_Offset, *pData);
			return FALSE;

		case WDI:
			MakeOperatorMessage(Addr.a.m_Offset, pData, uCount);
			return TRUE;

		case WSL:
			AddDecWord(*pData, 4);
			return TRUE;

		case WCS:
			if( *pData ) AddDecWord(*pData, 4);
			return TRUE;

		case WCC:
			return TRUE;
		}

	return FALSE;
	}

void CGEDNC2MasterDriver::MakeCNCParWrite(DWORD dData)
{
	char c[12];

	UINT uA = m_pCtx->m_dWriteParam[1];

	if( uA ) SPrintf( c, "N%4.4dP%dA%d", m_pCtx->m_dWriteParam[0], dData, uA );

	else SPrintf( c, "N%4.4dP%d", m_pCtx->m_dWriteParam[0], dData );

	PutText(LPCTXT(c));
	}

void CGEDNC2MasterDriver::MakePitchErrWrite(UINT uOffset, DWORD dData)
{
	char c[12];

	SPrintf( c, "N%4.4dV%1.1d", uOffset, dData );

	PutText(LPCTXT(c));
	}

void CGEDNC2MasterDriver::MakeToolOffWrite(DWORD dData)
{
	char c[150] = {};
	char b[12]  = {};

	UINT uMask  = 1;

	AddByte('N');
	AddDecWord(dData, 4);

	for( UINT i = 0; i < strlen(m_pTool); i++ ) {

		if( uMask & m_pCtx->m_dWriteTool[0] ) {

			AddByte(m_pTool[i]);

			DWORD d = m_pCtx->m_dWriteTool[i];

			memset( b, 0, sizeof(b) );

			if( m_pTool[i] != 'Q' ) {

				SPrintf( b, "%9.9f", PassFloat(d) );
				}

			else SPrintf( b, "%1.1u", d );
			}

		strcat(c, b);
		}

	PutText(LPCTXT(c));
	}

void CGEDNC2MasterDriver::MakeCustomVarWrite(UINT uOffset, DWORD dData)
{
	char c[20] = {0};

	SPrintf( c, "N%5.5uP%9.9f", uOffset, PassFloat(dData) );

	PutText(LPCTXT(c));
	}

void CGEDNC2MasterDriver::MakeOperatorMessage(UINT uOffset, PDWORD pData, UINT uCount)
{
	BOOL fClr = uOffset >= 6 && uOffset <= 10; // set clear message
	BYTE bMsg = fClr ? uOffset - 5 : uOffset;; // actual message number

	if( fClr ) AddByte('-');

	AddByte(bMsg);

	char c[33] = {0};

	memcpy( c, &m_pCtx->m_dMessage[0], min( 32, uCount * 4 ) );

	PutText( LPCTXT(c) );
	}

// Transport Layer

BOOL CGEDNC2MasterDriver::Transact(UINT uState)
{
	EndFrame();

	Send();

	return uState != RECVE ? GetReply(uState) : TRUE;
	}

void CGEDNC2MasterDriver::Send(void)
{
	m_pData->ClearRx();

	PutFrame();
	}

BOOL CGEDNC2MasterDriver::GetReply(UINT uState)
{
	BOOL fBCCOK = FALSE;
	BYTE bBCC   = 0;

	UINT uCount = 0;
	UINT uTimer = 500;

	UINT uInState = uState;

	uState = RSTATE0;

	memset(m_bRx, 0, 2);

	SetTimer(uTimer);

/**/	AfxTrace0("R\r\n");

	WORD wData;

	while( GetTimer() ) {

		if( (wData = Get(uTimer)) == LOWORD(NOTHING) ) {

			continue;
			}

		BYTE bData = LOBYTE(wData);

/**/		AfxTrace1("<%2.2x>", bData);

		switch( uState ) {

			case RSTATE0:
				switch( bData ) {

					case ENQ:
						SendDLE('0');
						break;
					case DLE:
						uState = RECVDLE;
						break;
					default :
						return FALSE;
					}
				break;

			case RECVDLE:

				switch( bData ) {

					case '0':
						return
						uInState == RSTATE0;

					case '1':
						PutByte(EOT);
						uState = RSTATE0;
						if( uInState != RECVD ) return TRUE;
						break;

					case STX:	uState = uInState;	break;
					case ETX:	uState = RECVBCC;	break;
					default:	uState = RSTATE0;	break;
					}
				break;

			case RECVD:
				m_bRx[uCount++] = bData;

				bBCC ^= bData;

				if( bData == DLE ) uState = RECVD1;

				break;

			case RECVD1:
				bBCC ^= bData;

				if( bData == ETX ) uState = RECVBCC;

				else { // will find error in CheckReply
					m_bRx[uCount++] = bData;

					uState = RECVD;
					}
				break;

			case RECVBCC:
				SendDLE('1');
				uState = RECVE;
				fBCCOK = TRUE;//!(bData ^ bBCC);
				break;

			case RECVE:
				return bData == EOT && fBCCOK && CheckReply(uInState);

			default:
				return FALSE;
			}

		if( uCount >= sizeof(m_bRx) ) return FALSE;
		}
AfxTrace0("\r\n Timeout ");
	return FALSE;
	}

BOOL CGEDNC2MasterDriver::CheckReply(UINT uState)
{
	BYTE bN0   = m_bRx[0];
	BYTE bN1   = m_bRx[1];
	BYTE bN2   = m_bRx[2];
	BYTE bN3   = m_bRx[3];
	BOOL fFail = FALSE;

	if( bN2 != m_bTx[4] || bN3 != m_bTx[5] ) return FALSE;

	switch( bN0 ) {

		case 'T':
			if( (bN2 == 'B' && bN3 == 'D') ||
			    (bN2 == 'N' && bN3 == 'P')
			    ) 
			    fFail = TRUE;
			break;

		case 'M':
			if( bN2 == 'E' && bN3 == 'R' ) fFail = TRUE;

			else {
				if( bN2 == 'N' && (bN3 == 'R' || bN3 == 'P')) fFail = TRUE;

				else {
					if( bN2 == 'I' && bN3 == 'L' ) fFail = TRUE;
					}
				}
			break;
		}

	if( !fFail ) {

		fFail = TRUE;

		switch( uState ) {

			case RECVD:
				if( bN0 == 'R' && bN1 == ' ' ) return TRUE;

			case RECVPRA:
				if( bN0 == 'M' && bN1 == ' ' &&
				    bN2 == 'R' && bN3 == 'T'
				    )
				    return TRUE;

				break;

			case RECVT0:
				if( bN0 == 'T' && bN1 == ' ' ) {

					if( (bN2 == 'N' && bN3 == 'B') ||
					    (bN2 == 'F' && bN3 == 'D')
					    )
					    return TRUE;
					}

				break;

			case RECVM:
				if( bN0 == 'M' && bN1 == ' ' &&
				    bN2 == 'O' && bN3 == 'K'
				    )
				    return TRUE;

				break;
			}
		}

	if( fFail ) {

		SetNakCode();

		return FALSE;
		}

	return TRUE;
	}

// Port Access

void CGEDNC2MasterDriver::PutByte(BYTE bData)
{
/**/	AfxTrace1("\r\n-%2.2x-", bData);

	m_pData->Write( bData, FOREVER);
	}

void CGEDNC2MasterDriver::PutFrame(void)
{
/**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

WORD CGEDNC2MasterDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

void CGEDNC2MasterDriver::SetpItem(UINT uID)
{
	m_pItem = m_pCL;

	GEDNC2CmdDef * pFail = NULL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == m_pItem->uID ) return;

		m_pItem++;
		}

	m_pItem = pFail;
	}

DWORD CGEDNC2MasterDriver::GetData(UINT uType, UINT uPos)
{
	return uType == RR ? GetReal(uPos) : GetNumber(uPos);
	}

DWORD CGEDNC2MasterDriver::GetNumber(UINT uPos)
{
	char s1[32];
	char *s2 = s1;

	UINT uRadix = GetDefaultRadix();

	if( m_bRx[uPos] == '0' && m_bRx[uPos+1] == 'X' ) {
		uPos   +=  2;
		uRadix  = 16;
		}

	return strtol( (char *)(&m_bRx[uPos]), &s2, uRadix );
	}

DWORD CGEDNC2MasterDriver::GetReal(UINT uPos)
{
	return ATOF( (const char * )&m_bRx[uPos] );
	}

UINT CGEDNC2MasterDriver::FindRcvPos(void)
{
	UINT uID = m_pItem->uID;

	if( (uID >= RTOD) && (uID <= RTOQ) ) return GetToolPos();

	if( (uID >= RTLL) && (uID <= RTLC) ) return GetLifePos();

	if( (uID >= RMDG) && (uID <= RMDF) ) return GetModalPos();

	switch( uID ) {

		case RPAP:
		case RWPE:
		case RWMV:
			return FindKey('P');

		case RPAA:
			return FindKey('A');

		case RMP:
		case RSE:
		case RAE:
		case RMI:
		case RPN:
		case RSN:
		case RAF:
		case RADI:
		case RADS:
		case RADN:
		case RAL:
		case RST:
		case RID:
			return 4;

		default:

			break;
		}

	return 0;
	}

UINT CGEDNC2MasterDriver::GetToolPos(void)
{
	BYTE bKey = 0;

	switch( m_pItem->uID ) {

		case RTOD:	bKey = 'D';	break;
		case RTOK:	bKey = 'K';	break;
		case RTOH:	bKey = 'H';	break;
		case RTOL:	bKey = 'L';	break;
		case RTOX:	bKey = 'X';	break;
		case RTOU:	bKey = 'U';	break;
		case RTOY:	bKey = 'Y';	break;
		case RTOV:	bKey = 'V';	break;
		case RTOZ:	bKey = 'Z';	break;
		case RTOW:	bKey = 'W';	break;
		case RTOR:	bKey = 'R';	break;
		case RTOP:	bKey = 'P';	break;
		case RTOQ:	bKey = 'Q';	break;
		}

	return FindKey(bKey);
	}

UINT CGEDNC2MasterDriver::GetLifePos(void)
{
	BYTE bKey = 0;

	switch( m_pItem->uID ) {

		case RTLL:	bKey = 'L';	break;
		case RTLQ:	bKey = 'Q';	break;
		case RTLT:	bKey = 'T';	break;
		case RTLH:	bKey = 'H';	break;
		case RTLD:	bKey = 'D';	break;
		case RTLC:	bKey = 'C';	break;
		}

	return FindKey(bKey);
	}

UINT CGEDNC2MasterDriver::GetModalPos(void)
{
	BYTE bKey = 0;

	switch( m_pItem->uID ) {

		case RMDG:	bKey = 'G';	break;
		case RMDD:	bKey = 'D';	break;
		case RMDE:	bKey = 'E';	break;
		case RMDH:	bKey = 'H';	break;
		case RMDL:	bKey = 'L';	break;
		case RMDM:	bKey = 'M';	break;
		case RMDN:	bKey = 'N';	break;
		case RMDO:	bKey = 'O';	break;
		case RMDS:	bKey = 'S';	break;
		case RMDT:	bKey = 'T';	break;
		case RMDF:	bKey = 'F';	break;
		}

	return FindKey(bKey);
	}

BOOL CGEDNC2MasterDriver::IsModalCmd(void)
{
	return m_pItem->uID >= RMDG && m_pItem->uID <= RMDF;
	}

DWORD CGEDNC2MasterDriver::GetModalData(UINT uType)
{
	UINT uPos = GetModalPos();

	char *p = (char *)&m_bRx[uPos];

	return uType == addrRealAsReal ? ATOF(p) : ATOI(p);
	}

UINT CGEDNC2MasterDriver::FindKey(BYTE bKey)
{
	for( UINT i = 4; m_bRx[i] != DLE; i++ ) {

		if( m_bRx[i] == bKey ) {

			return i+1;
			}
		}

	return 0;
	}

BOOL CGEDNC2MasterDriver::ReadNoTransmit(PDWORD pData, UINT uCount)
{
	switch( m_pItem->uID ) {

		case WPA:
		case WTON:
		case WSL:
		case WCS:
		case WCC:
			*pData = 0;
			return TRUE;

		case WPAP:
			*pData = m_pCtx->m_dWriteParam[1];
			return TRUE;

		case WPAA:
			*pData = m_pCtx->m_dWriteParam[2];
			return TRUE;

		case WTOB:
		case WTOD:
		case WTOK:
		case WTOH:
		case WTOL:
		case WTOX:
		case WTOU:
		case WTOY:
		case WTOV:
		case WTOZ:
		case WTOW:
		case WTOR:
		case WTOP:
		case WTOQ:
			*pData = m_pCtx->m_dWriteTool[(m_pItem->uID) - WTON];
			return TRUE;

		case WDIS:
			UINT i;
			for( i = 0; i < uCount; i++ ) {

				pData[i] = m_pCtx->m_dMessage[i];
				}

			return TRUE;

		case ERR:
			*pData = m_dNak;
			return TRUE;
		}

	return FALSE;
	}

BOOL CGEDNC2MasterDriver::WriteNoTransmit(PDWORD pData, UINT uCount)
{
	DWORD dData = *pData;

	UINT u      = m_pItem->uID;

	if( (u >= RMP) && (u <= RPAA) ) {

		return TRUE;
		}

	if( (u >= RTOD) && (u <= RTOQ) ) {

		return TRUE;
		}

	if( (u >= RTLL) && (u <= RST) ) {

		return TRUE;
		}

	switch( u ) {

		case WPA:
			return BOOL(dData);

		case WPAP:
		case WPAA:
			m_pCtx->m_dWriteParam[u - WPAP + 1] = dData;
			return TRUE;

		case WTOB:
		case WTOD:
		case WTOK:
		case WTOH:
		case WTOL:
		case WTOX:
		case WTOU:
		case WTOY:
		case WTOV:
		case WTOZ:
		case WTOW:
		case WTOR:
		case WTOP:
		case WTOQ:
			m_pCtx->m_dWriteTool[u - WTON] = dData;
			return TRUE;

		case WSL:
		case WCS:
		case WCC:
		case WDI:
			return !(BOOL(dData));

		case RID:
			return TRUE;

		case WDIS:
			for( u = 0; u < uCount; u++ ) m_pCtx->m_dMessage[u] = pData[u];
			return TRUE;

		case ERR:
			m_dNak = 0;
			return TRUE;
		}

	return FALSE;
	}

void CGEDNC2MasterDriver::DoCNCPar(AREF Addr)
{
	if( m_pItem->uID == RPAP || m_pItem->uID == RPAA ) {

		AddAxisPattern(Addr.a.m_Extra);
		}
	}

void CGEDNC2MasterDriver::DoToolOff(void)
{
	switch( m_pItem->uID ) {

		case RTOD:	AddHexWord(0x0001);	break;
		case RTOK:	AddHexWord(0x0002);	break;
		case RTOH:	AddHexWord(0x0100);	break;
		case RTOL:	AddHexWord(0x0200);	break;
		case RTOX:	AddHexWord(0x0004);	break;
		case RTOU:	AddHexWord(0x0400);	break;
		case RTOY:	AddHexWord(0x0020);	break;
		case RTOV:	AddHexWord(0x2000);	break;
		case RTOZ:	AddHexWord(0x0008);	break;
		case RTOW:	AddHexWord(0x0800);	break;
		case RTOR:	AddHexWord(0x0010);	break;
		case RTOP:	AddHexWord(0x1000);	break;
		case RTOQ:	AddHexWord(0x0040);	break;
		}
	}

void CGEDNC2MasterDriver::DoToolLife(UINT uOffset)
{
	switch ( m_pItem->uID ) {

		case RTLL:
		case RTLQ:
		case RTLT:
		case RTLH:
		case RTLD:
		case RTLC:
			AddDecWord(uOffset, 4);
			break;
		}
	}

void CGEDNC2MasterDriver::DoModal(UINT uOffset)
{
	if( IsModalCmd() ) {

		AddByte(m_pHex[uOffset % 3]);
		}
	}

UINT CGEDNC2MasterDriver::GetDefaultRadix(void)
{
	if( m_pItem->uID >= RTLL && m_pItem->uID <= RMDF ) return 10;

	switch( m_pItem->uID ) {

		case RMI:
		case RPN:
		case RWPE:
		case RADI:
		case RADS:
		case RADN:
			return 10;
		}

	return 16;
	}

UINT CGEDNC2MasterDriver::GetPosition(void)
{
	return 2;
	}

void CGEDNC2MasterDriver::GetP1Size(PDWORD pType, BOOL fIsWrite)
{
	DWORD u;

	switch( m_pItem->uID ) {

		case RMP:
		case RWP:
		case RSP:
		case RSE:
		case RAE:
			*pType  = (DWORD(TYPBB) << 8) + 4;
			return;

		case RMI:
			*pType  = (DWORD(TYPUI) << 8) + 3;
			return;

//		case RPA:
			*pType  = (DWORD(TYPUI) <<  8) + 4; // Parameter Number
			*pType += (DWORD(TYPBB) << 24) + 4; // T
			return;
		}

	*pType = 0;
	}

void CGEDNC2MasterDriver::SetNakCode(void)
{
	UINT uPos = FindKey('X');

	char c[16];

	char *sErr = c;

	m_dNak = uPos && uPos < 9 ? strtoul((char *)(&m_bRx[uPos+1]), &sErr, 16) : 0;
	}

// End of File
