
#include "intern.hpp"

#include "modmonr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus RTU Monitor Driver
//

// Instantiator

INSTANTIATE(CModbusMonitorRTU);

// Constructor

CModbusMonitorRTU::CModbusMonitorRTU(void)
{
	m_Ident  = DRIVER_ID;
	}

// Config

void MCALL CModbusMonitorRTU::Load(LPCBYTE pData)
{
	}

void MCALL CModbusMonitorRTU::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, FALSE);
	}

void MCALL CModbusMonitorRTU::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

// Entry Points

void MCALL CModbusMonitorRTU::Service(void)
{
	AllocBuffers();

	for(;;) {

		if( !GetFrame() ) {

			continue;
			}

		m_dDrop = DWORD( m_pRx[0] ) << 16;

		switch( m_pRx[1] ) {

			case 0x01:
				HandleRead(3);
				break;

			case 0x03:
				HandleRead(2);
				break;
					
			case 0x04:
				HandleRead(1);
				break;
					
			case 0x05:
				HandleSingleWrite(3);
				break;
					
			case 0x06:
				HandleSingleWrite(2);
				break;
					
			case 0xF:
				HandleMultiWrite(3);
				break;
					
			case 0x10:
				HandleMultiWrite(2);
				break;

			default:
				break;
			}
		}
	}

// Frame Handlers

BOOL CModbusMonitorRTU::HandleRead(UINT uTable)
{
	if( m_uRcvSize == 8 ) {

		m_uReadAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

		m_uReadCount = MotorToHost(PWORD(m_pRx + 4)[0]);

		if( GetFrame() && m_uRcvSize != 8 ) {

			if( m_dDrop == (DWORD( m_pRx[0] ) << 16) ) {

				if( m_uRcvSize == (5 + unsigned(m_pRx[2]) ) ) {

					HandleMultiWrite( uTable + 100 );
					}
				}
			}
		}

	return TRUE;
	}

BOOL CModbusMonitorRTU::HandleMultiWrite(UINT uTable)
{
	UINT uAddr;
	UINT uCount;
	UINT uStart = 7;

	if( uTable > 100 ) { // Read function

		uAddr  = m_uReadAddr;

		uCount = m_uReadCount;

		uStart = 3;

		uTable -= 100;
		}

	else {
		if( m_uRcvSize == 8 ) {

			return TRUE;
			}

		uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

		uCount = MotorToHost(PWORD(m_pRx + 4)[0]);
		}

	if( uCount > 125 || uAddr + uCount >  0xFFFF ) {

		return FALSE;
		}

	else {
		PDWORD pWork = new DWORD [ uCount ];

		CAddress Addr;

		Addr.a.m_Table  = uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Offset = 1 + uAddr;
		Addr.a.m_Type   = addrLongAsLong;

		if( uTable != 3 ) {

			for( UINT n = 0; n < uCount; n++ ) {

				pWork[n]  = HostToMotor(PWORD(m_pRx + uStart)[n]);

				pWork[n] |= m_dDrop;

				}
			}

		else {
			UnpackBits( pWork, &uCount );
			}

		if( COMMS_SUCCESS(Write(Addr, pWork, uCount)) ) {

			delete pWork;

			return TRUE;
			}

		delete pWork;

		return FALSE;
		}
	}

BOOL CModbusMonitorRTU::HandleSingleWrite(UINT uTable)
{
	UINT  uAddr  = MotorToHost(PWORD(m_pRx + 2)[0]);

	DWORD dwData = MotorToHost(PWORD(m_pRx + 4)[0]);

	CAddress Addr;

	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Table  = uTable;
	Addr.a.m_Offset = 1 + uAddr;

	if( uTable == 3 ) {

		if( !LOWORD(dwData) ) {

			dwData = 0;
			}

		else {

			if( LOWORD(dwData) == 0xFF00 ) {
				
				dwData = 1;
				}

			else {
				return FALSE;
				}
			}
		}

	dwData |= m_dDrop;

	if( COMMS_SUCCESS(Write(Addr, &dwData, 1)) ) {

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CModbusMonitorRTU::AllocBuffers(void)
{
	m_uTxSize = 512;
	
	m_uRxSize = 512;
	
	m_pTx = New BYTE [ m_uTxSize ];

	m_pRx = New BYTE [ m_uRxSize ];
	}

// Port Access

// Frame Building
		
// Transport Layer

BOOL CModbusMonitorRTU::GetFrame(void)
{
	return BinaryRx();
	}

BOOL CModbusMonitorRTU::BinaryRx(void)
{
	m_uRcvSize = 0;

	UINT uGap  = 0;

	while( TRUE ) {

		UINT uByte;
		
		if( (uByte = m_pData->Read(5)) < NOTHING ) {

			m_pRx[m_uRcvSize++] = uByte;

			if( m_uRcvSize <= m_uRxSize ) {
			
				uGap = 0;
				
				continue;
				}
				
			return FALSE;
			}
		
		if( ++uGap >= 2 ) {
			
			if( m_uRcvSize >= 4 ) {

				m_pData->ClearRx();
				
				m_CRC.Preset();
				
				PBYTE p = m_pRx;
				
				UINT  n = m_uRcvSize - 2;
			
				for( UINT i = 0; i < n; i++ ) {

					m_CRC.Add(*(p++));
					}

				WORD c1 = IntelToHost(PWORD(p)[0]);
				
				WORD c2 = m_CRC.GetValue();

				return (c1 == c2);
				}

			m_uRcvSize = 0;

			uGap       = 0;
			}
		}

	return FALSE;
	}

// Response Helper

void CModbusMonitorRTU::UnpackBits(PDWORD pWork, UINT * pCount)
{
	UINT uCt = 0;

	UINT uSz = min( 15, min( unsigned(m_pRx[2]), (*pCount/8) + 1 ) );

	for( UINT i = 0; i < uSz; i++ ) {

		UINT u = i * 8;

		BYTE m = 1;

		for( UINT j = 0; j < 8; j++ ) {

			pWork[u + j]  = m_pRx[3 + i] & m ? 1 : 0;
			pWork[u + j] |= m_dDrop;

			uCt++;

			if( uCt == *pCount ) {

				return;
				}

			m <<= 1;
			}
		}

	*pCount = uCt;

	return;
	}

// End of File
