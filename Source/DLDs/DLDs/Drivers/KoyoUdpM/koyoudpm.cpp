
#include "intern.hpp"

#include "koyoudpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Koyo udp Master Driver
//

// Instantiator

INSTANTIATE(CKoyoUdpM);

// Constructor

CKoyoUdpM::CKoyoUdpM(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CKoyoUdpM::~CKoyoUdpM(void)
{
	}

// Configuration

void MCALL CKoyoUdpM::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CKoyoUdpM::Attach(IPortObject *pPort)
{
	}

void MCALL CKoyoUdpM::Open(void)
{
	}

// Device

CCODE MCALL CKoyoUdpM::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->m_bBytes        = 0;
			m_pCtx->m_PingEnable    = GetByte(pData);
			m_pCtx->m_PingReg       = GetWord(pData);
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKoyoUdpM::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CKoyoUdpM::Ping(void)
{
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			} 

		if( m_pCtx->m_PingEnable ) {

			DWORD    Data[1];

			CAddress Addr;

			Addr.a.m_Table  = 1;
			Addr.a.m_Offset = m_pCtx->m_PingReg;
			Addr.a.m_Type   = addrWordAsWord;
			Addr.a.m_Extra  = 0;

			return Read(Addr, Data, 1);
			}

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR; 
	}

CCODE MCALL CKoyoUdpM::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		} 
		
	UINT uTable = Addr.a.m_Table;

	UINT uType = Addr.a.m_Type;

	UINT uID = 0;

	uCount = min(uCount, FindCount(uType));

	switch( uTable ) {
	
		case 1:		uID = 0x0001; uTable = 1;	break;	/* Data Registers	*/
		case 2:		uID = 0x0001; uTable = 1;	break;	/* Current Time		*/
		case 3:		uID = 0x0201; uTable = 1;	break;	/* Current Count	*/
		case 4:		uID = 0x0101; uTable = 2;	break;	/* I - X		*/
		case 5:		uID = 0x0181; uTable = 2;	break;	/* I - GX		*/
		case 6:		uID = 0x01AD; uTable = 2;	break;	/* I - Special Relay	*/
		case 7:		uID = 0x0101; uTable = 3;	break;	/* O - Y		*/
		case 8:		uID = 0x0181; uTable = 3;	break;	/* O - C		*/
		case 9:		uID = 0x0281; uTable = 3;	break;	/* O - Stage Bits	*/
		case 10:	uID = 0x0301; uTable = 3;	break;	/* O - TMR Status Bits	*/
		case 11:	uID = 0x0321; uTable = 3;	break;	/* O - CTR Status Bits	*/
		
		default:	return CCODE_ERROR | CCODE_HARD;

		}
       
	return Read(uID, Addr.a.m_Offset, pData, uCount, uType, uTable);
	}

CCODE MCALL CKoyoUdpM::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}
	
	UINT uTable = Addr.a.m_Table;

	UINT uType = Addr.a.m_Type;

	UINT uID = 0;

	uCount = min(uCount, FindCount(uType));

	switch( uTable ) {

		case 1:		uID = 0x0001; uTable = 1;	break;	/* Data Registers	*/
		case 2:		uID = 0x0001; uTable = 1;	break;	/* Current Time		*/
		case 3:		uID = 0x0201; uTable = 1;	break;	/* Current Count	*/
		case 7:		uID = 0x0101; uTable = 3;	break;	/* O - Y		*/
		case 8:		uID = 0x0181; uTable = 3;	break;	/* O - C		*/
		case 9:		uID = 0x0281; uTable = 3;	break;	/* O - Stage Bits	*/
		case 10:	uID = 0x0301; uTable = 3;	break;	/* O - TMR Status Bits	*/
		case 11:	uID = 0x0321; uTable = 3;	break;	/* O - CTR Status Bits	*/
	    	
		default:	return uCount;

		}
	
	return Write(uID, Addr.a.m_Offset, pData, uCount, uType, uTable);
	}

// Socket Management

BOOL CKoyoUdpM::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}  

		return TRUE;
		}

	return FALSE;
	}

BOOL CKoyoUdpM::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_UDP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		m_pCtx->m_pSock->SetOption(OPT_RECV_QUEUE, sizeof(m_bRxBuff));
		
		if( m_pCtx->m_pSock->Connect(IP, 0x7070, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10); 
				} 

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CKoyoUdpM::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CKoyoUdpM::SendFrame(void)
{
	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CKoyoUdpM::RecvFrame(BOOL fWrite)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	UINT uTotal = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 15 ) {

				if( MAKEWORD(m_bRxBuff[3], m_bRxBuff[4]) == m_pCtx->m_wTrans - 1 ) {

					uTotal = m_bRxBuff[7];
					
					if( m_bRxBuff[9] == 0x22 ) {

						if( uPtr > uTotal ) {

							if( fWrite ) {

								return TRUE;
								}

							BYTE bBytes = m_pCtx->m_bBytes;

							if( bBytes == uTotal - 4 ) {

								memcpy(m_bRxBuff, m_bRxBuff + uPtr - bBytes, bBytes);
															
								return TRUE;
								}
							}
						}
					else {	
						uPtr = 0;
						} 
					}
				else {

					return FALSE;
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}  

	return FALSE;
	}

BOOL CKoyoUdpM::Transact(BOOL fWrite)
{
	SetByteCount();
	
	if( SendFrame() && RecvFrame(fWrite) ) {

		return TRUE;
		}

	CloseSocket(TRUE);

	return FALSE;
	}

// Frame Building

void CKoyoUdpM::StartHeader(UINT uID)
{
	m_uPtr = 0;

	AddByte(0x48);

	AddByte(0x41);

	AddByte(0x50);
	
	AddWord(m_pCtx->m_wTrans++);

	AddWord(0x00);

	AddByte(0x00);

	AddByte(0x00);

	AddByte(0x19);

	AddByte(0x00);

	AddByte(0x01);
	}

void CKoyoUdpM::AddByte(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;
	}

void CKoyoUdpM::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CKoyoUdpM::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CKoyoUdpM::AddData(PDWORD pData, UINT uCount, UINT uType)
{
	if(  IsLong(uType ) ) {

		AddLongData(pData, uCount);

		return;
		}
	
	for( UINT i = 0; i < uCount; i++ ) {

		m_bTxBuff[m_uPtr++] = LOBYTE(*pData);

		m_bTxBuff[m_uPtr++] = HIBYTE(*pData);

		pData++;
		}
	}

void CKoyoUdpM::AddLongData(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddLong(pData[u]);
		}
	}

// Opcode Handlers

CCODE CKoyoUdpM::Read(UINT uID, UINT uOffset, PDWORD pData, UINT uCount, UINT uType, UINT uTable)
{
	StartHeader(uID);

	AddByte(0x1E);

	m_pCtx->m_bBytes = uCount * FindByteCount(uType);

	AddByte(m_pCtx->m_bBytes);

	AddWord(GetOffset(uID, uOffset, uType));

	AddByte(uTable + 0x30);
	
	if( Transact(FALSE) ) {

		return GetData(uType, pData, uCount);
		}

	return CCODE_ERROR;
	}

CCODE CKoyoUdpM::Write(UINT uID, UINT uOffset, PDWORD pData, UINT uCount, UINT uType, UINT uTable)
{
	StartHeader(uID);

	AddByte(0x20);

	m_pCtx->m_bBytes = uCount * FindByteCount(uType);

	AddByte(m_pCtx->m_bBytes);

	AddWord(GetOffset(uID, uOffset, uType));

	AddByte(uTable + 0x30);

	AddData(pData, uCount, uType);

	if( Transact(TRUE) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

// Helpers

UINT CKoyoUdpM::GetData(UINT uType, PDWORD pData, UINT uCount)
{
	if( IsLong(uType) ) {

		return GetLongData(pData, uCount); 
		}
	
	return GetData(pData, uCount);
	}

UINT CKoyoUdpM::GetData(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		WORD x = PU2(m_bRxBuff)[u];
			
		pData[u] = IntelToHost(x);

		}

	return uCount;
	}

BOOL CKoyoUdpM::IsLong(UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:
		case addrWordAsReal:
		case addrBitAsLong:

			return TRUE;
		}

	return FALSE;
	}

UINT CKoyoUdpM::GetLongData(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x = PU4(m_bRxBuff)[u];
			
		pData[u] = IntelToHost(x);

		}

	return uCount;
	}

UINT CKoyoUdpM::FindCount(UINT uType)
{
	switch( uType ) {

		case addrBitAsWord:
		case addrWordAsWord:

			return 32;

		case addrBitAsLong:
		case addrWordAsLong:
		case addrWordAsReal:

			return 16;
		}

	return 64;
	}

UINT CKoyoUdpM::FindByteCount(UINT uType)
{
	switch( uType ) {

		case addrBitAsByte:

			return 1;

		case addrBitAsWord:
		case addrWordAsWord:

			return 2;
		}

	return 4;
	}

void CKoyoUdpM::SetByteCount(void)
{
	m_bTxBuff[7] = m_uPtr - 9;
	}

UINT CKoyoUdpM::GetOffset(UINT uID, UINT uOffset, UINT uType)
{
	switch( uType )  {

		case addrBitAsByte:
		case addrBitAsWord:
		case addrBitAsLong:

			return uID + ( uOffset >> 3 );
		} 

	return uID + uOffset;
	}

// End of File
