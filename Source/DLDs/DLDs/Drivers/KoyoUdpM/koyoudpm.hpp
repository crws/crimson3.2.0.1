//////////////////////////////////////////////////////////////////////////
//
// Koyo UDP Master Driver
//

class CKoyoUdpM : public CMasterDriver
{
	public:
		// Constructor
		CKoyoUdpM(void);

		// Destructor
		~CKoyoUdpM(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BYTE	 m_bBytes;
			BYTE m_PingEnable;
			WORD m_PingReg;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		UINT	   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(BOOL fWrite);
		BOOL Transact(BOOL fWrite);
		
		// Frame Building
		void StartHeader(UINT uID);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddData(PDWORD pData, UINT uCount, UINT uType);
		void AddLongData(PDWORD pData, UINT uCount);
		
		// Opcode Handlers
		CCODE Read (UINT uID, UINT uOffset, PDWORD pData, UINT uCount, UINT uType, UINT uTable);
		CCODE Write(UINT uID, UINT uOffset, PDWORD pData, UINT uCount, UINT uType, UINT uTable);
			
		// Helpers
		UINT GetData(UINT uType, PDWORD pData, UINT uCount);
		UINT GetData(PDWORD pData, UINT uCount);
		BOOL IsLong(UINT uType);
		UINT GetLongData(PDWORD pData, UINT uCount);
		UINT FindCount(UINT uType);
		UINT FindByteCount(UINT uType);
		void SetByteCount(void);
		UINT GetOffset(UINT uID, UINT uOffset, UINT uType);
		

	};

// End of File
