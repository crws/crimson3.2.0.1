
//////////////////////////////////////////////////////////////////////////
//
// Modbus Data Spaces
//

#define	SPACE_HOLD	0x01
#define	SPACE_ANALOG	0x02
#define	SPACE_OUTPUT	0x03
#define	SPACE_INPUT	0x04
#define	SPACE_HOLD32	0x05
#define	SPACE_ANALOG32	0x06

// String Item Table Numbers
#define	TFPN	23	// Firmware Part Number
#define	TBPN	24	// Boot Part Number Drive
#define	TFRB	25	// Firmware Revision Base
#define	TFRO	26	// Firmware Revision Option (FM)
#define	TBRS	27	// Boot Revision
#define	TDAN	28	// Drive Axis Name String
#define	TUUS	29	// User Unit
#define	TUDM	30	// User Defined Motor Name
#define	TPSN	31	// Product Serial Number

// Table Numbers for special commands
#define	IAC	40	// Index Acceleration
#define	ICR	41	// Index Control
#define	IXC	42	// Index Count
#define	IDC	43	// Index Deceleration
#define	IDS	44	// Index Distance
#define	IDW	45	// Index Dwell
#define	IXT	46	// Index Type
#define	IXV	47	// Index Velocity
#define	RGO	48	// Registration Offset
#define	CNX	49	// Chain Next
#define	FPC	50	// Fault Power Up Count
#define	FPT	51	// Fault Power Up Time
#define	FTY	52	// Fault Type
#define	MCM	53	// Motion Command

// Solutions Module Table Addresses
#define	M15	60
#define	M16	61
#define	M17	62
#define	MOFFSET	415 - M15

// Motion Controller Table Addresses
#define	MCO	68
#define	MCI	69
#define	MCH	70

// String Item Offsets
#define	OFPN	39901	// Firmware Part Number
#define	OFPNL	39906	// Firmware Part Number Last
#define	OBPN	39910	// Boot Part Number Drive
#define	OBPNL	39916	// Boot Part Number Drive Last
#define	OFRB	39988	// Firmware Revision Base
#define	OFRBL	39989	// Firmware Revision Base Last
#define	OFRO	39990	// Firmware Revision Option (FM)
#define	OFROL	39991	// Firmware Revision Option (FM) Last
#define	OBRS	39995	// Boot Revision
#define	OBRSL	39996	// Boot Revision Last
#define	ODAN	40005	// Drive Axis Name String
#define	ODANL	40016	// Drive Axis Name String Last
#define	OUUS	41029	// User Unit
#define	OUUSL	41031	// User Unit Last
#define	OUDM	42101	// User Defined Motor Name
#define	OUDML	42106	// User Defined Motor Name Last
#define	OPSN	49903	// Product Serial Number
#define	OPSNL	49910	// Product Serial Number Last

/*
// Table addresses for headers
#define	THDO	80	// Coil Select
#define	THDI	81	// Input Select
#define	THAI	82	// Input Reg Select
#define	THHR	83	// Holding Reg Select
#define	TLFN	84	// Function List Select
#define	TLRO	85	// Register Order List Select
#define	TLAL	86	// Alphabetic List Select
#define	KEYW	87	// List By Key Word
#define	TABA	88	// A Header
#define	TABB	89	// B Header
#define	TABC	90	// C Header
#define	TABD	91	// D Header
#define	TABE	92	// E Header
#define	TABF	93	// F Header
#define	TABG	94	// G Header
#define	TABH	95	// H Header
#define	TABI	96	// I Header
#define	TABJ	97	// J Header
#define	TABK	98	// K Header
#define	TABL	99	// L Header
#define	TABM	100	// M Header
#define	TABN	101	// N Header
#define	TABO	102	// O Header
#define	TABP	103	// P Header
#define	TABQ	104	// Q Header
#define	TABR	105	// R Header
#define	TABS	106	// S Header
#define	TABT	107	// T Header
#define	TABU	108	// U Header
#define	TABV	109	// V Header
#define	TABW	110	// W Header
#define	TABX	111	// X Header
#define	TABY	112	// Y Header
#define	TABZ	113	// Z Header
// Operations Sorting
#define	TDIA	114	// Diagnostics
#define	THOM	115	// Homing
#define	TIOS	116	// I/O
#define	TINX	117	// Index
#define	TJOG	118	// Jog
#define	TMOT	119	// Motion
#define	TPUL	120	// Pulse
#define	TREG	121	// Registration
#define	TSTT	122	// Status
#define	TSYS	123	// System
#define	TLCC	124	// Machine Function Sort Select
*/

/*/ Modbus base headers
#define	HDO	0xD000	// Coil Header Maximum
#define	HDI	0xD001	// Digital Input Header Maximum
#define	HAI	0xD003	// Input Register 16 Header Maximum
#define	HHR	0xD004	// Holding Register 16 Header Maximum

// Control Techniques base Headers
#define	HDIA	0xD005	// Diagnostics Operations
#define	HHOM	0xD006	// Homing Operations
#define	HIOS	0xD007	// I/O Operations
#define	HINX	0xD008	// Index Operations
#define	HJOG	0xD009	// Jog Operations
#define	HMOT	0xD00A	// Motion Operations
#define	HPUL	0xD00B	// Pulse Operations
#define	HREG	0xD00C	// Registration Operations
#define	HSTT	0xD00D	// Status Operations
#define	HSYS	0xD00E	// System Operations

#define	LFN	0xD030	// Function List Selection
#define	LRO	0xD031	// Register Order List Selection
#define	LAL	0xD032	// Alphabetic List Selection
*/

// Generic Modbus Address Table Numbers
#define	GMBO	32	// 0xxx bit modbus address
#define	GMBI	33	// 1xxx bit modbus address
#define	GMBA	34	// 3xxx word modbus address
#define	GMBB	35	// 3xxx two word 32 bit modbus address
#define	GMBC	36	// 3xxx one word 32 bit modbus address
#define	GMBD	37	// 4xxx word modbus address
#define	GMBE	38	// 4xxx two word 32 bit modbus address
#define	GMBF	39	// 4xxx one word 32 bit modbus address

// Device ID's - TCP and Serial
#define	DVEPP	1
#define	DVSP	2
#define	DVSK	3
#define	DVGP	4
#define	DVST	5
#define	DVMC	6
#define	DVBR	7
#define	DVMP	8
// Devices - Serial Only
#define	DVEPI	20
#define	DVEPB	21

// Invalid Registers Handling
#define	DONOTCALLMAX	50
#define	INVALIDADDRESS	 2

// Register/Bit Limits
#define	EPMAXWORDS	16
#define	EPMAXLONG	 8
#define	EPMAXBITS	24

#define	BB	addrBitAsBit
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP Driver
//

class CEmersonEPDriver : public CMasterDriver
{
	public:
		// Constructor
		CEmersonEPDriver(void);

		// Destructor
		~CEmersonEPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	 m_bDrop;
			BYTE	 m_bType;
			BOOL	 m_fDisableCheck;
			BOOL	 m_fGetSingles;
			BOOL	 m_fInDNCList;
			UINT	 m_uDoNotCallPos;
			UINT	 m_uDoNotCall[DONOTCALLMAX];
			UINT	 m_uDoNotCallCt[DONOTCALLMAX];
			UINT	 m_uInitCount;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	m_bTx[128];
		BYTE	m_bRx[128];
		UINT	m_uPtr;
		CRC16	m_CRC;
		UINT	m_uTimeout;
		UINT	m_ReadDelay;
		UINT	m_uWriteErrCt;
		BOOL	m_fAdjustedAddr;
		BOOL	m_fInPing;

		// Implementation
		BOOL ConvertAddress(AREF Addr, CAddress * pAddr);
		BOOL GetAddressFromItem(UINT uTable, UINT uOffset, UINT * pAddress);

		// String Item Handling
		BOOL StringAllocBad(AREF Addr);

		// Port Access
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		
		// Transport Layer
		BOOL PutFrame(void);
		BOOL GetFrame(BOOL fWrite);
		BOOL BinaryTx(void);
		BOOL BinaryRx(BOOL fWrite);
		BOOL Transact(BOOL fIgnore);
		BOOL CheckReply(BOOL fIgnore);
		BOOL HasReadException(void);

		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
		UINT  AdjustAddress(AREF Addr, UINT *pCount);
		BOOL  IsGenericAddress(UINT uTable);
		BOOL  IsCommander(void);
		UINT  IsCmdrSolModule(UINT uTable);

		// Do Not Call
		UINT  DoNotCall(UINT uAddress, UINT uCount);
		UINT  FindDNCItem(UINT uAddress);
		void  RemoveDNC(UINT uAddress);
		void  AddDoNotCall(UINT uAddress);
		BOOL  ReadDNCAnyway(UINT uAddress);
		CCODE CheckDoNotCall(AREF Addr, UINT uCount);
		void  InitDoNotCall(void);
	};

// End of File
