#include "intern.hpp"

#include "emsonep.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP Driver
//

// Instantiator

INSTANTIATE(CEmersonEPDriver);

// Constructor

CEmersonEPDriver::CEmersonEPDriver(void)
{
	m_Ident		= DRIVER_ID;

	m_uTimeout	= 600;

	m_uWriteErrCt	= 0;

	m_fAdjustedAddr	= FALSE;
	}

// Destructor

CEmersonEPDriver::~CEmersonEPDriver(void)
{
	}

// Configuration

void  MCALL CEmersonEPDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) m_ReadDelay = GetWord(pData);
	}
	
void  MCALL CEmersonEPDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}
	
	Make485(Config, TRUE);
	}
	
// Management

void  MCALL CEmersonEPDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void  MCALL CEmersonEPDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmersonEPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);
			m_pCtx->m_bType = GetByte(pData);

			m_pCtx->m_fDisableCheck = TRUE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmersonEPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmersonEPDriver::Ping(void)
{
	if( !m_pCtx->m_bDrop ) {

		return CCODE_SUCCESS;
		}

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPACE_HOLD;

	Addr.a.m_Type   = addrWordAsWord;

	Addr.a.m_Extra  = 0;

	InitDoNotCall();

	m_fInPing	= TRUE;

	for( UINT i = 1; i < 202; i += 100 ) { // Ping 1, then 101, 201 if necessary

		Addr.a.m_Offset = i;

		if( DoWordRead(Addr, Data, 1) == (CCODE)1 ) {

			m_fInPing = FALSE;

			return 1;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEmersonEPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_ReadDelay ) Sleep(m_ReadDelay);

	if( !m_pCtx->m_bDrop ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	CAddress CAdd;

	if( !(ConvertAddress(Addr, &CAdd)) ) { // string item, allocation exceeded

		for( UINT k = 0; k < uCount; k++ ) *pData = 0;

		return uCount;
		}

	UINT uCt = uCount;

	if( IsCommander() ) {

		m_pCtx->m_fInDNCList = FALSE;

		UINT uMax = CAdd.a.m_Type == WW ? EPMAXWORDS : CAdd.a.m_Type == LL ? EPMAXLONG : EPMAXBITS;

		uCt = DoNotCall(CAdd.a.m_Offset, min(uCt, uMax));

		if( !uCt ) { // this address is in list

			if( !ReadDNCAnyway(CAdd.a.m_Offset) ) return CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY;

			m_pCtx->m_fGetSingles = FALSE;

			m_pCtx->m_fInDNCList  = TRUE;

			uCt = 1;
			}
		}

	CCODE ccReturn;

	switch( CAdd.a.m_Table ) {

		case SPACE_HOLD:

			if( CAdd.a.m_Type == addrWordAsWord ) {

				ccReturn = DoWordRead(CAdd, pData, uCt);
				}

			else
				ccReturn = DoLongRead(CAdd, pData, uCt);
			break;

		case SPACE_ANALOG:

			if( CAdd.a.m_Type == addrWordAsWord ) {

				ccReturn = DoWordRead(CAdd, pData, uCt);
				}

			else
				ccReturn = DoLongRead(CAdd, pData, uCt);
			break;

		case SPACE_HOLD32:
			
			ccReturn = DoLongRead(CAdd, pData, uCt);
			break;

		case SPACE_ANALOG32:
			
			ccReturn = DoLongRead(CAdd, pData, uCt);
			break;

		case SPACE_OUTPUT:
			
			ccReturn = DoBitRead (CAdd, pData, uCt);
			break;

		case SPACE_INPUT:
			
			ccReturn = DoBitRead (CAdd, pData, uCt);
			break;

		default: return CCODE_ERROR | CCODE_HARD;
		}

	if( !(ccReturn & CCODE_ERROR) ) {

		if( ccReturn > 1 ) m_pCtx->m_fGetSingles = FALSE;

		if( m_pCtx->m_fInDNCList ) RemoveDNC(CAdd.a.m_Offset);
		}

	return ccReturn;
	}

CCODE MCALL CEmersonEPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress CAdd;

//**/	AfxTrace3("\r\n** WRITE** Tab=%d Off=%d Ct=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( !(ConvertAddress(Addr, &CAdd)) ) return uCount; // string item, allocation exceeded

	if( IsCommander() ) {

		if( !DoNotCall(CAdd.a.m_Offset, 1) ) return 1; // previously logged invalid address
		}

	switch( CAdd.a.m_Table ) {

		case SPACE_HOLD:

			if( CAdd.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(CAdd, pData, 1);
				}

			return DoLongWrite(CAdd, pData, 1);

		case SPACE_HOLD32:
			
			return DoLongWrite(CAdd, pData, 1);

		case SPACE_OUTPUT:
			
			return DoBitWrite (CAdd, pData, 1);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

BOOL  CEmersonEPDriver::ConvertAddress(AREF Addr, CAddress *pAddr)
{
	UINT uTab  = Addr.a.m_Table;
	UINT u;

	if( m_pCtx->m_bType == DVMC ) {

		switch( uTab ) {

			case MCO:	uTab = SPACE_OUTPUT;	break;
			case MCI:	uTab = SPACE_INPUT;	break;
			case MCH:	uTab = SPACE_HOLD;	break;
			default:	return FALSE;
			}

		pAddr->a.m_Table  = uTab;

		pAddr->a.m_Offset = Addr.a.m_Offset + 1; // MC Config addr are 0 based
		pAddr->a.m_Type   = Addr.a.m_Type;

		return TRUE;
		}

	if( IsGenericAddress(uTab) ) {

		u = Addr.a.m_Offset;

		switch( uTab ) {

			case GMBO:	uTab = SPACE_OUTPUT;	break;

			case GMBI:	uTab = SPACE_INPUT;	break;

			case GMBA:
			case GMBB:	uTab = SPACE_ANALOG;	break;

			case GMBC:	uTab = SPACE_ANALOG32;	break;

			case GMBD:
			case GMBE:	uTab = SPACE_HOLD;	break;

			case GMBF:	uTab = SPACE_HOLD32;	break;
			}
		}

	else {
		if( StringAllocBad(Addr) ) return FALSE;

		if( !GetAddressFromItem(uTab, Addr.a.m_Offset, &u) ) u = Addr.a.m_Offset;

		switch( u/10000 ) {

			case 0:	uTab = SPACE_OUTPUT;	break;
			case 1: uTab = SPACE_INPUT;	break;
			case 3: uTab = SPACE_ANALOG;	break;

			case 4:
			case 5:
				uTab = SPACE_HOLD;

				if( IsCommander() && Addr.a.m_Type == addrLongAsLong ) {

					uTab = SPACE_HOLD32;
					}

				break;

			default:			return FALSE;
			}
		}

	pAddr->a.m_Table  = uTab;
	pAddr->a.m_Extra  = 0;
	pAddr->a.m_Type   = Addr.a.m_Type;
	pAddr->a.m_Offset = u % 10000;

	return TRUE;
	}

BOOL  CEmersonEPDriver::GetAddressFromItem(UINT uTable, UINT uOffset, UINT * pAddress)
{
	UINT uBase = 0;

	m_fAdjustedAddr = FALSE;

	switch( uTable ) {

		case IAC:
			uBase = 43006;
			break;

		case ICR:
			uBase = 43014;
			break;

		case IXC:
			uBase = 43013;
			break;

		case IDC:
			uBase = 43008;
			break;

		case IDS:
			uBase = 43002;
			break;

		case IDW:
			uBase = 43012;
			break;

		case IXT:
			uBase = 43001;
			break;

		case IXV:
			uBase = 43004;
			break;

		case RGO:
			uBase = 43010;
			break;

		case CNX:
			uBase = 43015;
			break;

		case FPC:
			m_fAdjustedAddr = TRUE;
			*pAddress = 31002 + ( (10 - uOffset) * 4 );
			return TRUE;

		case FPT:
			m_fAdjustedAddr = TRUE;
			*pAddress = 31003 + ( (10 - uOffset) * 4 );
			return TRUE;

		case FTY:
			m_fAdjustedAddr = TRUE;
			*pAddress = 31001 + ( (10 - uOffset) * 4 );
			return TRUE;

		case M15:
		case M16:
		case M17:
			if( IsCommander() ) {

				*pAddress = uOffset + ((uTable + MOFFSET) * 100);

				return TRUE;
				}

			return FALSE;
		}

	if( uBase ) {

		*pAddress = uBase + (uOffset * 25);

		m_fAdjustedAddr = TRUE;

		return TRUE;
		}

	return FALSE;
	}

// String Item Handling
BOOL  CEmersonEPDriver::StringAllocBad(AREF Addr)
{
	if( m_pCtx->m_bType == DVEPB || m_pCtx->m_bType == DVEPI ) {

		UINT uOff = Addr.a.m_Offset;

		switch( Addr.a.m_Table ) {

			case TFPN:	return uOff > OFPNL;
			case TBPN:	return uOff > OBPNL;
			case TFRB:	return uOff > OFRBL;
			case TFRO:	return uOff > OFROL;
			case TBRS:	return uOff > OBRSL;
			case TDAN:	return uOff > ODANL;
			case TUUS:	return uOff > OUUSL;
			case TUDM:	return uOff > OUDML;
			case TPSN:	return uOff > OPSNL;
			}
		}

	return FALSE;
	}

// Port Access

void  CEmersonEPDriver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT  CEmersonEPDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void  CEmersonEPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr   = 0;

	m_bRx[1] = 0;
	
	AddByte(m_pCtx->m_bDrop);
	
	AddByte(bOpcode);
	}

void  CEmersonEPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {
	
		m_bTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void  CEmersonEPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void  CEmersonEPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL  CEmersonEPDriver::PutFrame(void)
{
	m_pData->ClearRx();
	
	return BinaryTx();
	}

BOOL  CEmersonEPDriver::GetFrame(BOOL fWrite)
{
	return BinaryRx(fWrite);
	}

BOOL  CEmersonEPDriver::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_bTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL  CEmersonEPDriver::BinaryRx(BOOL fWrite)
{
	UINT uPtr = 0;

	UINT uGap = 0;

	SetTimer(m_uTimeout);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		UINT uByte;
		
		if( (uByte = RxByte(5)) < NOTHING ) {

			m_bRx[uPtr++] = uByte;

//**/			AfxTrace1("<%2.2x>", uByte);

			if( uPtr <= sizeof(m_bRx) ) {
			
				uGap = 0;
				
				continue;
				}
				
			return FALSE;
			}
		
		if( ++uGap >= 5 ) {
			
			if( uPtr >= 4 ) {

				if( m_pCtx->m_fDisableCheck ) {
					
					if( uPtr >= ( fWrite ? 8 : UINT(m_bRx[2] + 5) ) ) {

						return TRUE;
						}
					}
				else {
				
					m_CRC.Preset();
				
					PBYTE p = m_bRx;
				
					UINT  n = uPtr - 2;
			
					for( UINT i = 0; i < n; i++ ) {

						m_CRC.Add(*(p++));
						}

					WORD c1 = IntelToHost(PU2(p)[0]);
				
					WORD c2 = m_CRC.GetValue();
					
					if( c1 == c2 ) {
						 
						return TRUE;
						}
					}
				}
				
			uPtr = 0;
			
			uGap = 0;
			}
		}

	return FALSE;
	}

BOOL  CEmersonEPDriver::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {
	
		if( !m_pCtx->m_bDrop ) { 

			while( m_pData->Read(0) < NOTHING );

			return TRUE;
			}
			
		if( GetFrame(fIgnore) ) {

			return CheckReply(fIgnore);
			}
		}

	return FALSE;
	}

BOOL  CEmersonEPDriver::CheckReply(BOOL fIgnore)
{
	if( m_bRx[0] == m_bTx[0] ) {

		if( fIgnore ) {

			return TRUE;
			}
	
		if( !HasReadException() ) {
		
			return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL  CEmersonEPDriver::HasReadException(void)
{
	return m_bRx[1] & 0x80;
	}

// Read Handlers

CCODE CEmersonEPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(AdjustAddress(Addr, &uCount));
	
	AddWord(uCount);

	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x = PU2(m_bRx + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return m_fInPing ? CCODE_ERROR : CheckDoNotCall(Addr, uCount);
	}

CCODE CEmersonEPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable    = Addr.a.m_Table;

	BOOL fSingle32 = uTable == SPACE_HOLD32 || uTable == SPACE_ANALOG32;

	BOOL fSP       = IsCommander();

	switch( uTable ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLD:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	UINT uAdd   = AdjustAddress(Addr, &uCount);

	UINT uEffCt = uCount;

	if( fSP && fSingle32 ) {

		uAdd   += 16384; // 32 bit holding registers get 4000 hex offset

		uEffCt *= 2; // each single address requests 2 words
		}

	else {
		uEffCt += 1; // 1 gets 1&2, 2 gets 2&3...
		}
		
	AddWord(uAdd);
	
	AddWord(uEffCt); // set 16 bit word count
	
	if( Transact(FALSE) ) {

		if( m_bRx[2] < uEffCt ) {

			uCount = m_bRx[2] / 2; // byte count returned
			}

		for( UINT n = 0; n < uCount; n++ ) {

			UINT u   = 3 + (fSingle32 && fSP ? 4 * n : 2 * n);

			DWORD x  = *(PU4(&m_bRx[u]));

			pData[n] = MotorToHost(x);
			}

		return uCount;
		}

	return m_fInPing ? CCODE_ERROR : CheckDoNotCall(Addr, uCount);
	}

CCODE CEmersonEPDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
			
		case SPACE_OUTPUT:
			StartFrame(0x01);
			break;
			
		case SPACE_INPUT:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(Addr.a.m_Offset - 1);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b    = 3;

		BYTE m    = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	return m_fInPing ? CCODE_ERROR : CheckDoNotCall(Addr, uCount);
	}

// Write Handlers

CCODE CEmersonEPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_HOLD ) {

		if( uCount == 1 && m_pCtx->m_bType != DVMC ) {
			
			StartFrame(6);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);
			
			AddWord(AdjustAddress(Addr, &uCount));
			
			AddWord(uCount);
			
			AddByte(uCount * 2);
			
			for( UINT n = 0; n < uCount; n++ ) {
				
				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE) || (m_uWriteErrCt++ > 3) ) {

			m_uWriteErrCt = 0;

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CEmersonEPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable    = Addr.a.m_Table;

	BOOL fSingle32 = uTable == SPACE_HOLD32;
		
	if( fSingle32 || ( uTable == SPACE_HOLD ) ) {

		StartFrame(16);

		UINT uAdd = AdjustAddress(Addr, &uCount);

		if( fSingle32 && IsCommander() ) {

			uAdd += 16384;
			}
		
		AddWord(uAdd);
		
		AddWord(2 * uCount);
		
		AddByte(uCount * 4);

		for( UINT n = 0; n < uCount; n++ ) {

			AddLong(pData[n]);
			}

		if( Transact(TRUE) || (m_uWriteErrCt++ > 3) ) {

			m_uWriteErrCt = 0;

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CEmersonEPDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_OUTPUT ) {

		if( uCount == 1 ) {
			
			StartFrame(5);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			StartFrame(15);

			AddWord(Addr.a.m_Offset - 1);

			AddWord(uCount);

			AddByte((uCount + 7) / 8);

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b);
				}
			}

		if( Transact(TRUE) || (m_uWriteErrCt++ > 3) ) {

			m_uWriteErrCt = 0;

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Helpers
UINT  CEmersonEPDriver::AdjustAddress(AREF Addr, UINT *pCount)
{
	if( m_fAdjustedAddr ) *pCount = 1;

	m_fAdjustedAddr = 0;

	return Addr.a.m_Offset - 1;
	}

BOOL  CEmersonEPDriver::IsGenericAddress(UINT uTable)
{
	return uTable >= GMBO && uTable <= GMBF;
	}

BOOL  CEmersonEPDriver::IsCommander(void)
{
	return m_pCtx->m_bType >= DVSP && m_pCtx->m_bType <= DVMP;
	}

UINT  CEmersonEPDriver::IsCmdrSolModule(UINT uTable)
{
	if( uTable >= M15 && uTable <= M17 ) return (15 + uTable - M15) * 100;

	return 0;
	}

// Do Not Call

UINT  CEmersonEPDriver::DoNotCall(UINT uAddress, UINT uCount)
{
	UINT uMinFound = 10000;

	for( UINT i = 0; i < DONOTCALLMAX; i++ ) {

		UINT uDNCAddr = m_pCtx->m_uDoNotCall[i];

		if( (uDNCAddr >= uAddress) && (uDNCAddr <= uAddress + uCount - 1) ) {

			MakeMin(uMinFound, uDNCAddr);
			}
		}

	return uMinFound == 10000 ? (m_pCtx->m_fGetSingles ? 1 : uCount) : uMinFound - uAddress;
	}

UINT  CEmersonEPDriver::FindDNCItem(UINT uAddress)
{
	for( UINT uPos = 0; uPos < DONOTCALLMAX; uPos++ ) {
		
		if( m_pCtx->m_uDoNotCall[uPos] == uAddress ) return uPos;
		}

	return DONOTCALLMAX;
	}

void  CEmersonEPDriver::RemoveDNC(UINT uAddress)
{
	UINT uFind = FindDNCItem(uAddress);

	if( uFind >= DONOTCALLMAX ) return; // address not found

	UINT *pAd = m_pCtx->m_uDoNotCall;
	UINT *pCt = m_pCtx->m_uDoNotCallCt;

	UINT uCpy = (DONOTCALLMAX - uFind - 1) * sizeof(UINT);

	memcpy( &pAd[uFind], &pAd[uFind+1], uCpy );
	memcpy( &pCt[uFind], &pCt[uFind+1], uCpy );

	uFind = 0;

	while( pAd[uFind] && uFind < DONOTCALLMAX ) {

		uFind++;
		}

	UINT uArrSz = (DONOTCALLMAX - uFind - 1) * sizeof(UINT);

	memset( &pAd[uFind], 0, uArrSz );
	memset( &pCt[uFind], 0, uArrSz );

	m_pCtx->m_uDoNotCallPos = uFind;
	}

void  CEmersonEPDriver::AddDoNotCall(UINT uAddress)
{
	if( DoNotCall(uAddress, 1) ) { // address is not in list

		UINT u = m_pCtx->m_uDoNotCallPos;

		m_pCtx->m_uDoNotCall[u]   = uAddress;

		m_pCtx->m_uDoNotCallCt[u] = m_pCtx->m_uInitCount;

		m_pCtx->m_uInitCount = (m_pCtx->m_uInitCount + 1) % 16;

		u = (u + 1) % DONOTCALLMAX;

		m_pCtx->m_uDoNotCallPos = u;
		}
	}

BOOL  CEmersonEPDriver::ReadDNCAnyway(UINT uAddress)
{
	UINT uFind = FindDNCItem(uAddress);

	if( uFind >= DONOTCALLMAX ) return TRUE;

	m_pCtx->m_uDoNotCallCt[uFind] = (m_pCtx->m_uDoNotCallCt[uFind] + 1) % 16;

	return !m_pCtx->m_uDoNotCallCt[uFind];
	}

CCODE CEmersonEPDriver::CheckDoNotCall(AREF Addr, UINT uCount)
{
	if( HasReadException() && (m_bRx[2] != INVALIDADDRESS) ) {

		return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;
		}

	DWORD    Data[1];

	CAddress CAdd;

	CAdd.a.m_Table  = Addr.a.m_Table;
	CAdd.a.m_Offset = Addr.a.m_Offset;
	CAdd.a.m_Type   = Addr.a.m_Type;

	CCODE c = CCODE_ERROR;

	if( uCount > 1 ) {

		switch( Addr.a.m_Type ) {

			case addrWordAsWord:	c = DoWordRead(CAdd, Data, 1);	break;
			case addrLongAsLong:	c = DoLongRead(CAdd, Data, 1);	break;
			case addrBitAsBit:	c = DoBitRead(CAdd, Data, 1);	break;
			}

		if( c == 1 ) { // This address is valid

			m_pCtx->m_fGetSingles = TRUE; // Read one at a time

			return 1;
			}
		}

	if( uCount == 1 ) {

		CAdd.a.m_Table  = SPACE_HOLD;
		CAdd.a.m_Offset = 1;
		CAdd.a.m_Type   = addrWordAsWord;

		m_fInPing = TRUE;

		if( DoWordRead(CAdd, Data, 1) != 1 ) return Ping(); // disconnect

		m_fInPing = FALSE;

		AddDoNotCall(Addr.a.m_Offset); // invalid address to add to list

		m_pCtx->m_fGetSingles = FALSE;

		if( m_pCtx->m_fInDNCList ) return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;
		}

	return 1; // pretend valid read was done
	}

void  CEmersonEPDriver::InitDoNotCall(void)
{
	m_pCtx->m_fGetSingles	= FALSE;

	m_pCtx->m_fInDNCList	= FALSE;

	m_pCtx->m_uDoNotCallPos = 0;

	m_pCtx->m_uInitCount    = 0;

	UINT uArrSz = DONOTCALLMAX * sizeof(UINT);

	memset( m_pCtx->m_uDoNotCall,   0, uArrSz );

	memset( m_pCtx->m_uDoNotCallCt, 0, uArrSz );
	}

// End of File

