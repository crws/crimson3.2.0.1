#include "intern.hpp"

#include "tex40p.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Toshiba EX40+ PLC Serial Driver 
//

// Instantiator

INSTANTIATE(CToshEx40pMasterDriver);

// Constructor

CToshEx40pMasterDriver::CToshEx40pMasterDriver(void)
{
	m_Ident = DRIVER_ID;

	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx));

	m_pCtx = NULL;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;
	}

// Destructor

CToshEx40pMasterDriver::~CToshEx40pMasterDriver(void)
{
	}

// Configuration

void MCALL CToshEx40pMasterDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CToshEx40pMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CToshEx40pMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CToshEx40pMasterDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CToshEx40pMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop	  = GetByte(pData);

			m_pCtx->m_fCheck  = TRUE;
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;

	}

CCODE MCALL CToshEx40pMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CToshEx40pMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 0x0;
	Addr.a.m_Type   = addrBitAsByte;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CToshEx40pMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
       	while( GetByteCount(Addr.a.m_Type, uCount) > 32 ) {

		uCount--;
		}
	
	Begin(TRUE);

	AddTextData(Addr, uCount, NULL);

	if( Transact() ) {

		if( m_bRx[6] != '0' ) {

			return CCODE_ERROR;
			}

		return GetRead(Addr, pData, uCount);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CToshEx40pMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	while( GetByteCount(Addr.a.m_Type, uCount) > 32 ) {

		uCount--;
		}

	Begin(FALSE);

	AddTextData(Addr, uCount, pData);

	if( Transact() ) {

		if( m_bRx[6] != '0' ) {

			return CCODE_ERROR | CCODE_NO_RETRY;
			}

		return uCount;
		}

	return CCODE_ERROR;
	}
 
// Implementation

UINT CToshEx40pMasterDriver::GetRead(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsWord(Addr.a.m_Type) ) {

		return GetWordData(pData, uCount);
		}

	if( IsLong(Addr.a.m_Type) ) {

		return GetLongData(pData, uCount);
		}

	return GetByteData(pData, uCount);
	}

UINT CToshEx40pMasterDriver::GetByteData(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		BYTE bPos = 11 + (u * 2);

		if( UINT(bPos + 2) > m_bPtr - 2 ) {

			return u ? u : CCODE_ERROR;
			}   

		PCTXT pText = PTXT(m_bRx + bPos);

		pData[u] = xtoin(pText, 2);
		}

	return uCount;
	}

UINT CToshEx40pMasterDriver::GetWordData(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		BYTE bPos = 11 + u * 4;

		if( UINT(bPos + 4) > m_bPtr - 2 ) {

			return u ? u : CCODE_ERROR;
			}
		
		BYTE bLo = xtoin(PTXT(m_bRx + bPos + 0), 2);

		BYTE bHi = xtoin(PTXT(m_bRx + bPos + 2), 2);

		pData[u] = MAKEWORD(bLo, bHi); 
		}

	return uCount;
	}

UINT CToshEx40pMasterDriver::GetLongData(PDWORD pData,  UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		BYTE bPos = 11 + u * 8;

		if( UINT(bPos + 8) > m_bPtr - 2 ) {

			return u ? u : CCODE_ERROR;
			}
		
		BYTE bLo1 = xtoin(PTXT(m_bRx + bPos + 0), 2);

		BYTE bHi1 = xtoin(PTXT(m_bRx + bPos + 2), 2);

		BYTE bLo2 = xtoin(PTXT(m_bRx + bPos + 4), 2);

		BYTE bHi2 = xtoin(PTXT(m_bRx + bPos + 6), 2);

		pData[u] = MAKELONG(MAKEWORD(bLo1, bHi1), MAKEWORD(bLo2, bHi2)); 
		}

	return uCount;
	}

UINT CToshEx40pMasterDriver::GetByteCount(UINT uType, UINT uCount)
{
	switch( uType ) {

		case addrBitAsWord:
		case addrByteAsWord:	return 2 * uCount;
		case addrBitAsLong:
		case addrBitAsReal:
		case addrByteAsLong:
		case addrByteAsReal:	return 4 * uCount;
		}

	return uCount;
	}

void CToshEx40pMasterDriver::Begin(BOOL fRead)
{
	m_bPtr = 0;

	AddByte('*');

	AddHex(m_pCtx->m_bDrop, 0x10);

	AddHex(~m_pCtx->m_bDrop, 0x10);

	AddByte('[');

	AddByte(fRead ? 'A' : 'H');

	AddByte('0');
	}

void CToshEx40pMasterDriver::AddByte(BYTE bByte)
{
	m_bTx[m_bPtr++] = bByte;

	m_bCheck += bByte;
	}

void CToshEx40pMasterDriver::AddHex(UINT n, UINT d)
{
   	while( d ) {

		AddByte(m_pHex[ (n / d) % 16 ]);

		d /= 16;
		}
	}

void CToshEx40pMasterDriver::AddDec(UINT n, UINT d)
{
	while( d )  {

		AddByte('0' + ( n / d ) % 10);

		d /= 10;
	   	}
	}

void CToshEx40pMasterDriver::AddTextData(AREF Addr, UINT uCount, PDWORD pData)
{	
	m_bCheck = 0;

	UINT uOffset = Offset[Addr.a.m_Table - 1] + Addr.a.m_Offset * 2;

	if( IsBitReg(Addr.a.m_Type) ) {

		uOffset = Offset[Addr.a.m_Table - 1] + Addr.a.m_Offset / 8; 
	       	}
	
	AddHex(uOffset, 0x1000);
	
	if( pData ) {

		UINT d = 0x10;

		if( !IsByte(Addr.a.m_Type) ) {

			d *= 0x100;
			}
		
		if( IsLong(Addr.a.m_Type) ) {
			
			d *= 0x10000;
			}

		for( UINT u = 0; u < uCount; u++ ) {

			AddHex(pData[u], d);
			}
		}
	else {
		AddDec(GetByteCount(Addr.a.m_Type, uCount), 10);
		}

	AddHex((0x100 - m_bCheck) & 0xF, 0x1);

	AddByte(']');
	}

BOOL CToshEx40pMasterDriver::Transact(void)
{
	if( Send() && Recv() && Check() ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CToshEx40pMasterDriver::Send(void)
{
	m_pData->ClearRx();

	m_pData->Write(m_bTx, m_bPtr, FOREVER);

	return TRUE;
	}

BOOL CToshEx40pMasterDriver::Recv(void)
{
	UINT uState = 0;

	UINT uData = 0;

	UINT uTimer = 0;

	m_bPtr = 0;

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uData == '*' ) {

					uState++;
					}
				break;
				
			case 1:
				if( uData == CR ) {
	
					return TRUE;
					}
				else {
                                        if( m_bPtr == sizeof(m_bRx) ) {

						return FALSE;
						}
					
					m_bRx[m_bPtr++] = uData;
					}
				break;
			}
		}

	return FALSE;
	}

BOOL CToshEx40pMasterDriver::Check(void)
{
	for( UINT x = 0; x < 10; x++ ) {

		if( x == 4 ) {

			if( m_bRx[x] != '(' ) {

				return FALSE;
				}

			continue;
			}

		if( x == 6 ) {

			continue;
			}

		if( m_bRx[x] != m_bTx[x + 1] ) {

			return FALSE;
			}
		}

	m_bCheck = 0;

	for( UINT y = 7; y < m_bPtr - 2; y++ ) {

		m_bCheck += FromAscii(m_bRx[y]);
		}

	if( m_pHex[((0x100 - m_bCheck) & 0xF)] == m_bRx[m_bPtr - 2] ) {

		return TRUE;
		}

	return FALSE;
	}

// Helpers

BOOL CToshEx40pMasterDriver::IsBitReg(UINT uType)
{
	switch( uType ) {

		case addrBitAsByte:
		case addrBitAsWord:
		case addrBitAsLong:
		case addrBitAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CToshEx40pMasterDriver::IsByte(UINT uType)
{
	switch( uType ) {

		case addrBitAsByte:

			return TRUE;
		}

	return FALSE;
	}

BOOL CToshEx40pMasterDriver::IsWord(UINT uType)
{
	switch( uType ) {

		case addrBitAsWord:
		case addrByteAsWord:

			return TRUE;
		}
	
	return FALSE;
	}

BOOL CToshEx40pMasterDriver::IsLong(UINT uType)
{
	switch( uType ) {

		case addrBitAsLong:
		case addrBitAsReal:
		case addrByteAsLong:
		case addrByteAsReal:

			return TRUE;
		}
	
	return FALSE;
	}

DWORD CToshEx40pMasterDriver::xtoin(PCTXT pText, UINT uCount)
{
	DWORD t = 0;

	while( uCount-- ) {
	
		char c = *(pText++);
		
		if( c >= '0' && c <= '9' )
			t = 16 * t + c - '0';

		else if( c >= 'A' && c <= 'F' )
			t = 16 * t + c - 'A' + 10;

		else if( c >= 'a' && c <= 'f' )
			t = 16 * t + c - 'a' + 10;
		}
		
	return t;
	}

BYTE CToshEx40pMasterDriver::FromAscii(BYTE bByte)
{
	if( bByte >= '0' ) {
	
		if( bByte <= '9' ) {

			return bByte - '0';
			}

		return	bByte - '@' + 9;
		}

	return bByte;
	}

// End of file
