
//////////////////////////////////////////////////////////////////////////
//
// Coriolis Modbus Shared Code Import
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#define SHARED_CODE

#include "../Modbus/modbus.cpp"

// End of File
