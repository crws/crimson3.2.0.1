
//////////////////////////////////////////////////////////////////////////
//
// Coriolis Modbus Serial Driver
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//
 
#include "CoriolisMMISerial.hpp"

// Instantiator

INSTANTIATE(CCoriolisSerialDriver);

// Constructor

CCoriolisSerialDriver::CCoriolisSerialDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_pCtx      = NULL;

	m_uMaxWords = 128;
	
	m_uMaxBits  = 2000;
	}

// Destructor

CCoriolisSerialDriver::~CCoriolisSerialDriver(void)
{
	}

// Device

CCODE MCALL CCoriolisSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pSerialCtx = (CSerialContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pSerialCtx = new CSerialContext;

			m_pCtx       = m_pSerialCtx;

			m_pSerialCtx->m_uPing		= GetWord(pData);
			m_pSerialCtx->m_bDrop		= GetByte(pData);

			m_pSerialCtx->m_uNumCoilRegs	= GetWord(pData);

			m_pSerialCtx->m_pCoilRegisters  = new CAddress[m_pSerialCtx->m_uNumCoilRegs];

			for( UINT d = 0; d < m_pSerialCtx->m_uNumCoilRegs; d++ ) {

				CAddress *Reg		= &m_pSerialCtx->m_pCoilRegisters[d];

				Reg->m_Ref		= GetLong(pData);
				}

			m_pSerialCtx->m_uNumHoldingRegs	= GetWord(pData);

			m_pSerialCtx->m_pHoldingRegisters = new CAddress[m_pSerialCtx->m_uNumHoldingRegs];

			for( UINT e = 0; e < m_pSerialCtx->m_uNumHoldingRegs; e++ ) {

				CAddress *Reg		= &m_pSerialCtx->m_pHoldingRegisters[e];

				Reg->m_Ref		= GetLong(pData);
				}

			m_pSerialCtx->m_fFlipLong	= 0;
			m_pSerialCtx->m_fFlipReal	= 0;

			m_pSerialCtx->m_uPoll		= 0;

			m_pSerialCtx->m_uMax01		= 512;
			m_pSerialCtx->m_uMax02		= 512;
			m_pSerialCtx->m_uMax03		= 32;
			m_pSerialCtx->m_uMax04		= 32;
			m_pSerialCtx->m_uMax15		= 512;
			m_pSerialCtx->m_uMax16		= 32;

			m_pSerialCtx->m_fRLCAuto	= FALSE;
			m_pSerialCtx->m_fDisable15	= FALSE;
			m_pSerialCtx->m_fDisable16	= FALSE;
			m_pSerialCtx->m_fDisable5	= FALSE;
			m_pSerialCtx->m_fDisable6	= FALSE;
			m_pSerialCtx->m_fNoCheck	= FALSE;
			m_pSerialCtx->m_fNoReadEx	= FALSE;
			m_pSerialCtx->m_fSwapCRC	= FALSE;
			m_pSerialCtx->m_fWriteReply	= FALSE;

			SetLastPoll();

			Limit(m_pSerialCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pSerialCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pSerialCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pSerialCtx->m_uMax04, 1, m_uMaxWords);
			Limit(m_pSerialCtx->m_uMax15, 1, m_pSerialCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pSerialCtx->m_uMax16, 1, m_pSerialCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pSerialCtx);
					       
			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	m_pCtx = m_pSerialCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CCoriolisSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pSerialCtx->m_pCoilRegisters; 
			
		m_pSerialCtx->m_pCoilRegisters = NULL;

		delete m_pSerialCtx->m_pHoldingRegisters;

		m_pSerialCtx->m_pHoldingRegisters = NULL;

		delete m_pSerialCtx;

		m_pSerialCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CCoriolisSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return CModbusDriver::Read(Addr, pData, uCount);
	}

CCODE MCALL CCoriolisSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{	
	if( GetMaxCount(Addr, uCount) ) {

		return CModbusDriver::Write(Addr, pData, uCount);
		}

	return uCount;
	}

// Implemetation

BOOL CCoriolisSerialDriver::GetMaxCount(AREF Address, UINT &uCount) 
{
	BOOL fCoils	= IsCoils(Address.a.m_Table);

	CAddress *pRegs	= GetRegArray(fCoils);

	UINT uRegs	= GetRegTotal(fCoils);

	UINT uOffset	= Address.a.m_Offset;

	UINT uSize	= GetSize(Address);

	UINT Count	= 0;

	for( UINT i = 0; i < uRegs; i++ ) {

		CAddress Reg  = pRegs[i];

		if( uOffset == Reg.a.m_Offset ) {

			if( Reg.a.m_Extra ) {
				
				if( Reg.m_Ref != Address.m_Ref ) {

					return FALSE;
					}
				
				MakeMin(uCount, GetSize(Reg));

				return TRUE;
				}

			UINT uSpan = 0;

			while ( uOffset <= Reg.a.m_Offset && i < uRegs && Count < uCount ) {

				uSpan = Reg.a.m_Offset - uOffset;
				
				if( uSpan ) {
					
					break;
					}
				else {
					Count++;
					}

				uOffset += uSize;

				i++;

				Reg = pRegs[i];
				}

			MakeMin(uCount, Count);

			return TRUE;
			}

		while ( uOffset < Reg.a.m_Offset ) {

			Count++;

			uOffset += uSize;

			if( uOffset >= Reg.a.m_Offset || Count == uCount ) {

				MakeMin(uCount, Count);

				return FALSE;
				}
			}
		}

	MakeMin(uCount, 1);

	return FALSE;
	}

UINT CCoriolisSerialDriver::GetSize(AREF Addr)
{
	if( Addr.a.m_Extra ) {

		return Addr.a.m_Extra + 1;
		}

	switch( Addr.a.m_Type ) {

		case addrWordAsLong:
		case addrWordAsReal:	return 2;

		case addrLongAsLong:
		case addrRealAsReal:	return 4;
		}

	return 1;
	}

UINT CCoriolisSerialDriver::GetRegTotal(BOOL fCoils)
{
	return fCoils ? m_pSerialCtx->m_uNumCoilRegs   : m_pSerialCtx->m_uNumHoldingRegs;
	}


CAddress * CCoriolisSerialDriver::GetRegArray(BOOL fCoils)
{
	return fCoils ? m_pSerialCtx->m_pCoilRegisters : m_pSerialCtx->m_pHoldingRegisters;
	}

// Helpers

BOOL CCoriolisSerialDriver::IsCoils(UINT uTable)
{
	return ((uTable != inputRegisters) && (uTable != holdingRegisters));	
	}

// End of File
