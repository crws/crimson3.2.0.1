//////////////////////////////////////////////////////////////////////////
//
// Space Definitions
//
//

#define SPACE_DM	1
#define SPACE_TM	2
#define SPACE_CC	3
#define SPACE_CP	4
#define SPACE_TC	5
#define SPACE_TP	6

//////////////////////////////////////////////////////////////////////////
//
// Keyence KV Series Master Serial Driver
//

class CKeyKVDriver : public CMasterDriver
{
	public:
		// Constructor
		CKeyKVDriver(void);

		// Destructor
		~CKeyKVDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		
	protected:

		// Device Data
		struct CContext
		{
			BYTE m_bConn;
			BYTE m_bDrop;
			};
		CContext *	m_pCtx;

		BYTE   m_bTxBuff[64];
		BYTE   m_bRxBuff[64];
		BOOL   m_fBreak;

		// Implementation

		void SendBreak(void);
		BOOL SendInit(void);
		BOOL SendTerm(void);
		BOOL Transact(BOOL fCheck);
		BOOL TxFrame(void);
		BOOL RxFrame(BOOL fCheck);
		BOOL CheckFrame(void);
		BOOL IsDigit(char c);

		
		
	};

// End of File
