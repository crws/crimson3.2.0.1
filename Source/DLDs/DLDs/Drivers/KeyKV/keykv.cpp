
#include "intern.hpp"

#include "keykv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Keyence KV Series Master Serial Driver
//
//

// Instantiator

INSTANTIATE(CKeyKVDriver);

// Constructor

CKeyKVDriver::CKeyKVDriver(void)
{
	m_Ident  = DRIVER_ID;
	}

// Destructor

CKeyKVDriver::~CKeyKVDriver(void)
{
	}

// Entry Points

CCODE MCALL CKeyKVDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed + 1;
	Addr.a.m_Offset = 0;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrWordAsWord;

	return Read(Addr, Data, 1);
	}

// Configuration

void MCALL CKeyKVDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CKeyKVDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}
	
// Management

void MCALL CKeyKVDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CKeyKVDriver::Open(void)
{	
	}

// Device

CCODE MCALL CKeyKVDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice); 

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bConn = GetByte(pData);

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKeyKVDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CKeyKVDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{		
	MakeMin(uCount, 1); 
	
	LPCSTR pFormat;
	
	int nReply;

	CTEXT s0[] = "RD DM%4.4u";
	CTEXT s1[] = "RD TM%2.2u";
	CTEXT s2[] = "RD C%3.3u";
	CTEXT s3[] = "RD T%3.3u";

	switch( Addr.a.m_Table & 0x0F ) {

		case SPACE_DM:	pFormat = s0;
				nReply  = 0;
				break;
				
		case SPACE_TM:	pFormat = s1;
				nReply  = 0;
				break;

		case SPACE_CC:	pFormat = s2;
				nReply  = 2;
				break;

		case SPACE_CP:	pFormat = s2;
				nReply  = 8;
				break;
		
		case SPACE_TC:	pFormat = s3;
				nReply  = 2;
				break;
		
		case SPACE_TP:	pFormat = s3;
				nReply  = 8;
				break;
		
		default:	return CCODE_ERROR;
		}

	if( m_fBreak ) {

		SendBreak();
		}
	
	if( SendInit() ) {
	
		SPrintf(PSTR(m_bTxBuff), pFormat, Addr.a.m_Offset);

		strcat(PSTR(m_bTxBuff), "\r\n");

		if( Transact(TRUE) ) {

			if( IsDigit(m_bRxBuff[nReply]) ) { 

				m_bRxBuff[nReply + 5] = 0;
						
				pData[0] = ATOI(PSTR(m_bRxBuff + nReply));

				if( SendTerm() ) {
			
					return uCount;
					}
				}
			}
		}	

	m_fBreak = TRUE;		
	
	return CCODE_ERROR;
	}

CCODE MCALL CKeyKVDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	LPCSTR pFormat;

	CTEXT s0[] = "WR DM%4.4u %5.5u";
	CTEXT s1[] = "WR TM%2.2u %5.5u";
	CTEXT s2[] = "WR C%3.3u %5.5u";
	CTEXT s3[] = "WS C%3.3u %5.5u";
	CTEXT s4[] = "WR T%3.3u %5.5u";
	CTEXT s5[] = "WS T%3.3u %5.5u";

	switch( Addr.a.m_Table & 0x0F ) {

		case SPACE_DM:	pFormat = s0;
				break;
				
		case SPACE_TM:	pFormat = s1;
				break;

		case SPACE_CC:	pFormat = s2;
				break;
		
		case SPACE_CP:	pFormat = s3;
				break;
		
		case SPACE_TC:	pFormat = s4;
				break;
		
		case SPACE_TP:	pFormat = s5;
				break;
		
		default:	return CCODE_ERROR;
		}

	if( m_fBreak ) {

		SendBreak();
		}
	
	if( SendInit() ) {
	
		SPrintf(PSTR(m_bTxBuff), pFormat, Addr.a.m_Offset, LOWORD(pData[0]));

		strcat(PSTR(m_bTxBuff), "\r\n");
		
		if( Transact(FALSE) ) {

			if( m_bRxBuff[0] == 'O' && m_bRxBuff[1] == 'K' ) {
				
				if( SendTerm() ) {

					return uCount;
					}
				}
			}
		}	

	m_fBreak = TRUE;

	return CCODE_ERROR;
	}

// Implementation

void CKeyKVDriver::SendBreak(void)
{
	m_pData->SetBreak(TRUE);

	Sleep(120);

	m_pData->SetBreak(FALSE);

	Sleep(10);
	
	m_fBreak = FALSE;
	}

BOOL CKeyKVDriver::SendInit(void)
{
	if( m_pCtx->m_bConn ) {
		
		SPrintf(PSTR(m_bTxBuff), "CR_%2.2u", m_pCtx->m_bDrop);
		}
	else
		strcpy(PSTR(m_bTxBuff), "CR");
	
	strcat(PSTR(m_bTxBuff), "\r\n");

	for ( UINT utry = 0; utry < 2; utry++ ) {

		if( Transact(FALSE) ) {

			BOOL fSuccess = m_bRxBuff[0] == 'C' && m_bRxBuff[1] == 'C';

			if( fSuccess && m_pCtx->m_bConn ) {

				fSuccess = m_bRxBuff[3] == m_pCtx->m_bDrop / 10 && 
					   m_bRxBuff[4] == m_pCtx->m_bDrop % 10;
				}

			return fSuccess;
			}
		}

	return FALSE;
	}
	
BOOL CKeyKVDriver::SendTerm(void)
{
	CTEXT s0[] = "CQ";

	strcpy(PSTR(m_bTxBuff), s0);

	strcat(PSTR(m_bTxBuff), "\r\n");

        for ( UINT u = 0; u < 2; u++) {

		if( Transact(FALSE) ) {
			
			return m_bRxBuff[0] == 'C' && m_bRxBuff[1] == 'F';
			}
		}
	return FALSE;
	}
	
BOOL CKeyKVDriver::Transact(BOOL fCheck)
{
	return TxFrame() && RxFrame(fCheck); 

	}
	
BOOL CKeyKVDriver::TxFrame(void)
{
	UINT uPos = 0;

	while( m_bTxBuff[uPos] ) {

		uPos++;
		}

	m_pData->Write(m_bTxBuff, uPos, FOREVER);
	
	return TRUE;
	}

BOOL CKeyKVDriver::RxFrame(BOOL fCheck)
{
	UINT uState = 0;
	
	UINT uPtr = 0;

	UINT uTimer = 0;

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {

		UINT uByte;
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == CR ) {

					uState = 1;
					}
				else {
					if( uPtr < sizeof(m_bRxBuff) ) {

						m_bRxBuff[uPtr++] = uByte;
						}
					else
						return FALSE;
					}
				break;
				
			case 1:
				if( uByte == LF ) {

					return fCheck ? CheckFrame() : TRUE;
					}
				break;
			}
		}

	m_fBreak = TRUE;

	return FALSE;
	}

BOOL CKeyKVDriver::CheckFrame(void)
{
	return IsDigit(m_bRxBuff[0]);
	}

BOOL CKeyKVDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

// End of File
