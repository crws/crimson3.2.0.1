
#include "modbus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Dupline Data Spaces
//

#define	DUP_SPACE_OUTPUT	0x01
#define DUP_SPACE_INPUT		0x02
#define DUP_SPACE_ANALINK	0x03
#define DUP_SPACE_COUNTER	0x04
#define DUP_SPACE_MUX_IN	0x05
#define DUP_SPACE_MUX_OUT	0x06
#define DUP_SPACE_RESET_CTR	0x07
#define DUP_SPACE_DIG_WORD	0x08
#define DUP_SPACE_DIGITAL	0x09
#define	DUP_SPACE_DIGIN		0x0A
#define DUP_SPACE_DIGOUT	0x0B
#define DUP_SPACE_ANALIMIT	0x0C
#define DUP_SPACE_REALTIME	0x0D
#define DUP_SPACE_REALTIMETEXT  0x0E
#define DUP_SPACE_ANALIMITONLY	0x0F
#define DUP_SPACE_CLOCKTEXT	0x10

//////////////////////////////////////////////////////////////////////////
//
// Dupline Modbus Counter Offsets
//

#define	READ_OFFSET_COUNTER	512
	

//////////////////////////////////////////////////////////////////////////
//
// Dupline Modbus Write Offsets
//

#define WRITE_OFFSET_INPUT	0x0F00
#define WRITE_OFFSET_OUTPUT	0x1000
#define WRITE_OFFSET_MUX_IN	0x1000
#define WRITE_OFFSET_RS_CTR	0x9C40
#define WRITE_OFFSET_DIGITAL	0x1000
#define WRITE_OFFSET_DIGOUT	0x1010
#define WRITE_OFFSET_DIGIN	0x0FF0 
#define	WRITE_OFFSET_CLOCK	0x0010	

//////////////////////////////////////////////////////////////////////////
//
// Dupline Modbus Driver
//

class CDuplineModbusDriver : public CModbusDriver
{
	public:
		// Constructor
		CDuplineModbusDriver(void);

		// Destructor
		~CDuplineModbusDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount, BOOL fLimit = FALSE);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoClockRead(AREF Addr, PDWORD pData, UINT uCount);
	
		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoClockWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
		DWORD FromBCD(DWORD d);
		WORD  FromBCD(WORD w);
		WORD  ToBCD(WORD w);
		void  AddWords(UINT uTable, PDWORD pData, UINT uCount, UINT uOffset);
		void  GetWords(UINT uTable, PDWORD pData, UINT uCount, UINT uOffset, BOOL fLimit = FALSE);
		void  AdjustSpecial(UINT uTable, UINT &uCount, UINT &uOffset);
		void  AddClock(UINT uTable, PDWORD pData);
		void  GetClock(UINT uTable, PDWORD pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Time Support
//

extern DWORD Time(UINT h, UINT m, UINT s);
extern DWORD Date(UINT y, UINT m, UINT d);
extern void  GetWholeDate(DWORD t, UINT *p);
extern void  GetWholeTime(DWORD t, UINT *p);
extern UINT  GetYear(DWORD t);
extern UINT  GetMonth(DWORD t);
extern UINT  GetDate(DWORD t);
extern UINT  GetDays(DWORD t);
extern UINT  GetWeeks(DWORD t);
extern UINT  GetDay(DWORD t);
extern UINT  GetWeek(DWORD t);
extern UINT  GetWeekYear(DWORD t);
extern UINT  GetHours(DWORD t);
extern UINT  GetHour(DWORD t);
extern UINT  GetMin(DWORD t);
extern UINT  GetSec(DWORD t);
extern UINT  GetMonthDays(UINT y, UINT m);
extern UINT  GetCummDays(UINT y, UINT m);

// End of File
