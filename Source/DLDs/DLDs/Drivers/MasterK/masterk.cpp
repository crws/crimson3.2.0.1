#include "intern.hpp"

#include "masterk.hpp"

//////////////////////////////////////////////////////////////////////////
//
// LS Industrial Systems Master-K via CNET Serial Driver
//

// Instantiator

INSTANTIATE(CLSMasterKDriver);

// Constructor

CLSMasterKDriver::CLSMasterKDriver(void)
{
	m_Ident         = DRIVER_ID;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex; 
	}

// Destructor

CLSMasterKDriver::~CLSMasterKDriver(void)
{
	}

// Configuration

void MCALL CLSMasterKDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CLSMasterKDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}
	
// Management

void MCALL CLSMasterKDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CLSMasterKDriver::Open(void)
{
	}

// Device

CCODE MCALL CLSMasterKDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CLSMasterKDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CLSMasterKDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 'D';
	Addr.a.m_Offset = 0x0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return ReadSingle(Addr, Data);
	}

CCODE MCALL CLSMasterKDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsMulti(Addr.a.m_Type, uCount) ) {

		return ReadMultiple(Addr, pData, uCount);
		}
	
	return ReadSingle(Addr, pData);
	}

CCODE MCALL CLSMasterKDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == 'F' ) {

		return uCount;
		}

	if( IsMulti(Addr.a.m_Type, uCount) ) {

		return WriteMultiple(Addr, pData, uCount);
		}

	return WriteSingle(Addr, pData);
	}

// Read/Write Handlers

CCODE CLSMasterKDriver::ReadSingle(AREF Addr, PDWORD pData)
{
	Start();

	AddByte(ENQ);

	AddAsAscii(m_pCtx->m_bDrop);

	AddByte(OP_READ);

	AddWord(OP_SINGLE);

	AddAsAscii(1);

	AddAsAscii(DEV_LEN);

	AddDeviceDef(Addr);

	AddByte(EOT);

	if( Transact() ) {

		pData[0] = FindData(0, Addr.a.m_Type);
		
		return 1;
		}

	return CCODE_ERROR;
	}

CCODE CLSMasterKDriver::ReadMultiple(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT Count = uCount;

	if( Addr.a.m_Type > addrWordAsWord ) {

		Count *= 2;
		}

	MakeMin(Count, 16);
	
	Start();

	AddByte(ENQ);
	
	AddAsAscii(m_pCtx->m_bDrop);

	AddByte(OP_READ);

	AddWord(OP_MULTI);

	AddAsAscii(DEV_LEN);

	AddDeviceDef(Addr);

	AddAsAscii(Count);

	AddByte(EOT);

	if( Transact() ) {

		if( Addr.a.m_Type > addrWordAsWord ) {

			Count /= 2;
			}
		
		for( UINT u = 0; u < Count; u++ ) {

			pData[u] = FindData(u, Addr.a.m_Type);
			}  

		return Count;
		}
	
	return CCODE_ERROR;
	}

CCODE CLSMasterKDriver::WriteSingle(AREF Addr, PDWORD pData)
{
	Start();

	AddByte(ENQ);

	AddAsAscii(m_pCtx->m_bDrop);

	AddByte(OP_WRITE);

	AddWord(OP_SINGLE);

	AddAsAscii(1);

	AddAsAscii(DEV_LEN);

	AddDeviceDef(Addr);

	AddData(pData[0], Addr.a.m_Type);

	AddByte(EOT);

	if( Transact() ) {

		return 1;
		}

	return CCODE_ERROR;

	}

CCODE CLSMasterKDriver::WriteMultiple(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT Count = uCount;

	if( Addr.a.m_Type > addrWordAsWord ) {

		Count *= 2;
		}

	MakeMin(Count, 16);
	
	Start();

	AddByte(ENQ);

	AddAsAscii(m_pCtx->m_bDrop);

	AddByte(OP_WRITE);

	AddWord(OP_MULTI);

	AddAsAscii(DEV_LEN);

	AddDeviceDef(Addr);

	AddAsAscii(Count);

	if( Addr.a.m_Type > addrWordAsWord ) {

		Count /= 2;
		}
	
	for(UINT u = 0; u < Count; u++ ) {

		AddData(pData[u], Addr.a.m_Type);
		}

	AddByte(EOT);

	if( Transact() ) {

		return Count;
		}
	
	return CCODE_ERROR;
	}

// Implementation

void CLSMasterKDriver::Start(void)
{
	m_uPtr = 0;
	}

void CLSMasterKDriver::AddByte(BYTE bByte)
{
	m_bTx[m_uPtr++] = bByte;
	}

void CLSMasterKDriver::AddWord(WORD wWord)
{
	AddByte(HIBYTE(wWord));

	AddByte(LOBYTE(wWord));
	}

void CLSMasterKDriver::AddAsAscii(BYTE bByte)
{
	AddByte(m_pHex[bByte / 16]);

	AddByte(m_pHex[bByte % 16]);
	}

void CLSMasterKDriver::AddDeviceDef(AREF Addr)
{
	AddByte(OP_START);

	AddDevice(Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Type);
	}

void CLSMasterKDriver::AddDevice(UINT uTable, UINT uOffset, UINT uType)
{
	switch( uTable ) {

		case 'T':
		case 'C':
			AddByte(uTable);
			
			AddType(uType);

			AddNumber(uOffset * 2);
			break;

		case 'U':

			AddByte('T');

			AddType(uType);

			AddNumber(uOffset);
			break;

		case 'B':

			AddByte('C');

			AddType(uType);

			AddNumber(uOffset);
			break;

		default:

			AddByte(uTable);

			AddType(uType);

			AddNumber(uOffset);
			break;
		}
	}

void CLSMasterKDriver::AddNumber(UINT uNum)
{
	UINT uMask = 1000;

	for(	; uMask; uMask /= 10 ) {

		AddByte(m_pHex[(uNum / uMask) % 10]);
		}
	}

void CLSMasterKDriver::AddType(UINT uType)
{
	if( uType == addrBitAsBit ) {

		AddByte(OP_BIT);

		return;
		}

	AddByte(OP_WORD);
	}

void CLSMasterKDriver::AddBCC(void)
{
	BYTE bBCC = 0;

	for( UINT u = 0; u < m_uPtr; u++ ) {

		bBCC += m_bTx[u];
		}
		
	AddAsAscii(bBCC);
	}

LONG CLSMasterKDriver::FindData(UINT uPos, UINT uType)
{
	if( uType == addrBitAsBit ) {

		return FindBit(uPos);
		}

	if( uType == addrWordAsWord ) {

		return FindWord(uPos);
		}

	return FindLong(uPos);
	}

BOOL CLSMasterKDriver::FindBit(UINT uPos)
{
	WORD x = xtoin(PCSTR(m_bRx + DATA_BEGIN + uPos * 2), 2);

	return SHORT((x)) ? 1 : 0;
	}

WORD CLSMasterKDriver::FindWord(UINT uPos)
{
	WORD x = xtoin(PCSTR(m_bRx + DATA_BEGIN + uPos * 4), 4);

	return LONG(SHORT((x)));
	}

LONG CLSMasterKDriver::FindLong(UINT uPos)
{
	WORD x  = FindWord(uPos);

	WORD y  = FindWord(uPos + 1);

	DWORD d = x | ( y << 16 );

	return LONG((d));
	}

void CLSMasterKDriver::AddData(DWORD dwData, UINT uType)
{
	if( uType < addrWordAsWord ) {

		AddAsAscii(LOBYTE(LOWORD(dwData)) ? 1 : 0);
		}

	else if( uType == addrWordAsWord ) {

		AddAsAscii(HIBYTE(LOWORD(dwData)));

		AddAsAscii(LOBYTE(LOWORD(dwData)));
		}
	else {
		AddAsAscii(HIBYTE(LOWORD(dwData)));

		AddAsAscii(LOBYTE(LOWORD(dwData)));
		
		AddAsAscii(HIBYTE(HIWORD(dwData)));
		
		AddAsAscii(LOBYTE(HIWORD(dwData)));
		} 
       }

// Transport

BOOL CLSMasterKDriver::Transact(void)
{
	return PutFrame() && GetFrame() && CheckFrame();
	}

BOOL CLSMasterKDriver::PutFrame(void)
{
	AddBCC();
	
	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CLSMasterKDriver::GetFrame(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_uPtr = 0;

	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		m_bRx[m_uPtr] = uData;

		if( m_bRx[m_uPtr - 2] == ETX ) {

			return TRUE;
			}

		m_uPtr++;
		}

	return FALSE;
	}

BOOL CLSMasterKDriver::CheckFrame(void)
{
	if( m_bRx[0] == ACK ) {

		if( !memcmp(m_bTx + 1, m_bRx + 1, 5) ) {

			BYTE bBCC = 0;

			for( UINT u = 0; u < m_uPtr - 1; u++ ) {

				bBCC += m_bRx[u];
				}

			return bBCC == xtoin(PCSTR(m_bRx + m_uPtr - 1), 2);
			}
		}

	return FALSE;
	}

// Helpers

DWORD CLSMasterKDriver::xtoin(PCTXT pText, UINT uCount)
{
	DWORD dwData = 0;
	
	while( uCount-- ) {
	
		char cData = *(pText++);
		
		if( cData >= '0' && cData <= '9' )
			dwData = 16 * dwData + cData - '0';

		else if( cData >= 'A' && cData <= 'F' )
			dwData = 16 * dwData + cData - 'A' + 10;

		else if( cData >= 'a' && cData <= 'f' )
			dwData = 16 * dwData + cData - 'a' + 10;

		else break;
		}
		
	return dwData;
	}

BOOL CLSMasterKDriver::IsMulti(UINT uType, UINT uCount)
{
	return ( (uType == addrWordAsWord && uCount > 1) || uType > addrWordAsWord );
	}

// End of File
