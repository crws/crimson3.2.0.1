/////////////////////////////////////////////////////////////////////////
//
// Constants
//
 
#define	FRAME_TIMEOUT	2000
#define OP_SINGLE	0x5353
#define OP_MULTI	0x5342
#define OP_READ		'r'
#define OP_WRITE	'w'
#define OP_START	'%'
#define OP_BIT		'X'
#define OP_WORD		'W'
#define DEV_LEN		7
#define DATA_BEGIN	10

//////////////////////////////////////////////////////////////////////////
//
// LS Industrial Systems Master-K Serial Driver
//

class CLSMasterKDriver : public CMasterDriver
{
	public:
		// Constructor
		CLSMasterKDriver(void);

		// Destructor
		~CLSMasterKDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Context
		struct CContext 
		{
			BYTE m_bDrop;
			};

		// Data Members
		CContext * m_pCtx;
		UINT	   m_uPtr;
		LPCTXT 	   m_pHex;
		BYTE	   m_bTx[256];
		BYTE	   m_bRx[256];
		
		// Read/Write Handlers
		CCODE ReadSingle(AREF Addr, PDWORD pData);
		CCODE ReadMultiple(AREF Addr, PDWORD pData, UINT uCount);
		CCODE WriteSingle(AREF Addr, PDWORD pData);
		CCODE WriteMultiple(AREF Addr, PDWORD pData, UINT uCount);

		// Implementation
		void Start(void);
		void AddByte(BYTE bByte);
		void AddWord(WORD wWord);
		void AddAsAscii(BYTE bByte);
		void AddDeviceDef(AREF Addr);
		void AddDevice(UINT uTable, UINT uOffset, UINT uType);
		void AddNumber(UINT uNum);
		void AddType(UINT uType);
		void AddBCC(void);
		LONG FindData(UINT uPos, UINT uType);
		BOOL FindBit(UINT uPos);
		WORD FindWord(UINT uPos);
		LONG FindLong(UINT uPos);
		void AddData(DWORD dwData, UINT uType);

		
		// Transport
		BOOL Transact(void);
		BOOL PutFrame(void);
		BOOL GetFrame(void);
		BOOL CheckFrame(void);

		// Helpers
		DWORD xtoin(PCTXT pText, UINT uCount);
		BOOL  IsMulti(UINT uType, UINT uCount);

				
	};

// End of File
