
#include "intern.hpp"

#include "kebtcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// KEB Din 2 Master TCP Driver
//

// Instantiator

INSTANTIATE(CKEBTcpM);

// Constructor

CKEBTcpM::CKEBTcpM(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CKEBTcpM::~CKEBTcpM(void)
{
	}

// Configuration

void MCALL CKEBTcpM::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CKEBTcpM::Attach(IPortObject *pPort)
{
	}

void MCALL CKEBTcpM::Open(void)
{
	}

// Device

CCODE MCALL CKEBTcpM::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_bDrop  = GetByte(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();
			m_pCtx->m_uError = 0;

			memset(PBYTE(m_pCtx->m_PDL), 0, elements(m_pCtx->m_PDL) * 4);
			memset(PBYTE(m_pCtx->m_PDW), 0, elements(m_pCtx->m_PDW) * 4);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CKEBTcpM::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);

	}

// Entry Points

CCODE MCALL CKEBTcpM::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}
		}

	return CKEBBaseMasterDriver::Ping();
	
	}

// Socket Management

BOOL CKEBTcpM::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CKEBTcpM::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {
	
		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}
				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}
				
			CloseSocket(TRUE); 

			return FALSE;
			}
		}

	return FALSE;
	}

void CKEBTcpM::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CKEBTcpM::Transact(void)
{
	if( Send() && RecvFrame() ) {
		
		return CheckFrame();
		}

	return FALSE; 
	}

BOOL CKEBTcpM::Send(void)
{
	if( OpenSocket() ) {
	
		UINT uSize = m_uPtr;

		/*AfxTrace("\nTx : ");

		for( UINT u = 0; u < m_uPtr; u++ ) {

			AfxTrace("%2.2x ", m_bTxBuff[u]);
			} */

		
		if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

			if( uSize == m_uPtr ) {

				return TRUE;
				}
			}

		CloseSocket(TRUE);
		}
	
	return FALSE;
	}

BOOL CKEBTcpM::RecvFrame(void)
{
	m_uPtr  = 0;

	UINT uSize = 0;

	UINT uTotal = 0;

//	AfxTrace("\nRx : ");

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - m_uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + m_uPtr, uSize);

		if( uSize ) {

			m_uPtr += uSize;
			
			for( UINT u = m_uPtr - uSize; u < m_uPtr; u++ ) {

			//	AfxTrace("%2.2x ", m_bRxBuff[u]);

				if( m_bRxBuff[u] == ETX ) {

					if( m_uPtr > u ) {

						return (FromAscii(m_bRxBuff[2]) == GetIID());
						}

					continue;
					}

				if( m_bRxBuff[u] == ACK ) {
					
					if( FromAscii(m_bRxBuff[u - 1]) == GetIID() ) {
					       
						return TRUE;
						}
					}

				if( m_bRxBuff[u] == NAK ) {

					if( FromAscii(m_bRxBuff[u - 2]) == GetIID() ) {

						return FALSE;
						}
					}

				if( m_bRxBuff[u] == EOT ) {

					m_bLastDrop = 0xFF;

					if( FromAscii(m_bRxBuff[u - 2]) == GetIID() )  {

						return FALSE;
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {
			
			return FALSE;
			}

		Sleep(10);
		} 
	
	return FALSE;
	}

// End of File
