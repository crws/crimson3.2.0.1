//////////////////////////////////////////////////////////////////////////
//
// KEB Master Defs
//

#define ETD	0x1 << 2
#define STD	0x2 << 2
#define LMT	0x3 << 2
#define DEF	0x4 << 2
#define CHR	0x5 << 2
#define DSP	0x6 << 2
#define NAM	0x7 << 2
#define PLI	0x8 << 2
#define PD1    0x11 << 2
#define PD2    0x12 << 2

#define ERR	0xF << 4

#define MULTI	240
#define SEND	2

//////////////////////////////////////////////////////////////////////////
//
// KEB Master Base Driver
//

class CKEBBaseMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CKEBBaseMasterDriver(void);

		// Destructor
		~CKEBBaseMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Context
		struct CBaseCtx
		{
			BYTE	m_bDrop;
			UINT    m_uError;
			UINT    m_PDL[2];
			UINT	m_PDW[4];
			};

	protected:
		// Data Members
		CBaseCtx * m_pBase;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		LPCTXT	   m_pHex;
		BYTE	   m_bBCC;
		BYTE	   m_bLastDrop;
		BYTE	   m_bIID;
			
		// Implementation

		void NewAddress(void);
		void Begin(BYTE bService, BOOL fWrite);
	     	void End(BOOL fWrite);
		void AddByte(BYTE bByte);
		void AddAsciiByte(BYTE bByte);
		void AddAsciiWord(WORD wWord);
		void AddAsciiLong(DWORD dwLong);
		void AddText(DWORD dwLong);
		void AddAsciiData(DWORD dwLong, UINT uType);
		void AddData(AREF Addr, PDWORD pData, UINT uCount);
		void SaveProcessData(AREF Addr, PDWORD pData, UINT uCount);
		void GetWordData(AREF Addr, PDWORD pData, UINT uCount);
		void GetLongData(AREF Addr, PDWORD pData, UINT uCount);
		BYTE GetService(UINT uTable);
		WORD GetOffset(AREF Addr);
		void AddSet(AREF Addr);
		BYTE GetSet(AREF Addr);
		BYTE GetPosition(AREF Addr, BOOL fAscii);
		BOOL NeedRead(AREF Addr);
		BOOL CheckFrame(void);
		void BuffFromAscii(void);
		void MakeRead(AREF Addr);
		void AccumBCC(UINT uPos);
		BYTE GetIID(void);
		
		CCODE SetError(AREF Addr);

		// Helpers

		void Increment(BYTE &bByte);
		BYTE FromAscii(BYTE bByte);
		BOOL IsBroadcast(void);
		BOOL IsText(UINT uTable);
		BOOL IsProcessData(UINT uTable);

								
		// Transport Layer
		virtual BOOL Transact(void);
		virtual BOOL Send(void);
	
	};

// End of File
