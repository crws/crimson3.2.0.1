
#include "intern.hpp"

#include "BSAPExtSM.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Extended Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

static BYTE crctbl[] = {
	0x087,0x00F,0x00E,0x01E,0x095,0x02C,0x01C,0x03D,
	0x0A3,0x049,0x02A,0x058,0x0B1,0x06A,0x038,0x07B,
	0x0CF,0x083,0x046,0x092,0x0DD,0x0A0,0x054,0x0B1,
	0x0EB,0x0C5,0x062,0x0D4,0x0F9,0x0E6,0x070,0x0F7,
	0x006,0x01F,0x08F,0x00E,0x014,0x03C,0x09D,0x02D,
	0x022,0x059,0x0AB,0x048,0x030,0x07A,0x0B9,0x06B,
	0x04E,0x093,0x0C7,0x082,0x05C,0x0B0,0x0D5,0x0A1,
	0x06A,0x0D5,0x0E3,0x0C4,0x078,0x0F6,0x0F1,0x0E7,
	0x085,0x02E,0x00C,0x03F,0x097,0x00D,0x01E,0x01C,
	0x0A1,0x068,0x028,0x079,0x0B3,0x04B,0x03A,0x05A,
	0x0CD,0x0A2,0x044,0x0B3,0x0DF,0x081,0x056,0x090,
	0x0E9,0x0E4,0x060,0x0F5,0x0FB,0x0C7,0x072,0x0D6,
	0x004,0x03E,0x08D,0x02F,0x016,0x01D,0x09F,0x00C,
	0x020,0x078,0x0A9,0x069,0x032,0x05B,0x0BB,0x04A,
	0x04C,0x0B2,0x0C5,0x0A3,0x05E,0x091,0x0D7,0x080,
	0x068,0x0F4,0x0E1,0x0E5,0x07A,0x0D7,0x0F3,0x0C6,
	0x083,0x04D,0x00A,0x05C,0x091,0x06E,0x018,0x07F,
	0x0A7,0x00B,0x02E,0x01A,0x0B5,0x028,0x03C,0x039,
	0x0CB,0x0C1,0x042,0x0D0,0x0D9,0x0E2,0x050,0x0F3,
	0x0EF,0x087,0x066,0x096,0x0FD,0x0A4,0x074,0x0B5,
	0x002,0x05D,0x08B,0x04C,0x010,0x07E,0x099,0x06F,
	0x026,0x01B,0x0AF,0x00A,0x034,0x038,0x0BD,0x029,
	0x04A,0x0D1,0x0C3,0x0C0,0x058,0x0F2,0x0D1,0x0E3,
	0x06E,0x097,0x0E7,0x086,0x07C,0x0B4,0x0F5,0x0A5,
	0x081,0x06C,0x008,0x07D,0x093,0x04F,0x01A,0x05E,
	0x0A5,0x02A,0x02C,0x03B,0x0B7,0x009,0x03E,0x018,
	0x0C9,0x0E0,0x040,0x0F1,0x0DB,0x0C3,0x052,0x0D2,
	0x0ED,0x0A6,0x064,0x0B7,0x0FF,0x085,0x076,0x094,
	0x000,0x07C,0x089,0x06D,0x012,0x05F,0x09B,0x04E,
	0x024,0x03A,0x0AD,0x02B,0x036,0x019,0x0BF,0x008,
	0x048,0x0F0,0x0C1,0x0E1,0x05A,0x0D3,0x0D3,0x0C2,
	0x06C,0x0B6,0x0E5,0x0A7,0x07E,0x095,0x0F7,0x084,
	0x08F,0x08B,0x006,0x09A,0x09D,0x0A8,0x014,0x0B9,
	0x0AB,0x0CD,0x022,0x0DC,0x0B9,0x0EE,0x030,0x0FF,
	0x0C7,0x007,0x04E,0x016,0x0D5,0x024,0x05C,0x035,
	0x0E3,0x041,0x06A,0x050,0x0F1,0x062,0x078,0x073,
	0x00E,0x09B,0x087,0x08A,0x01C,0x0B8,0x095,0x0A9,
	0x02A,0x0DD,0x0A3,0x0CC,0x038,0x0FE,0x0B1,0x0EF,
	0x046,0x017,0x0CF,0x006,0x054,0x034,0x0DD,0x025,
	0x062,0x051,0x0EB,0x040,0x070,0x072,0x0F9,0x063,
	0x08D,0x0AA,0x004,0x0BB,0x09F,0x089,0x016,0x098,
	0x0A9,0x0EC,0x020,0x0FD,0x0BB,0x0CF,0x032,0x0DE,
	0x0C5,0x026,0x04C,0x037,0x0D7,0x005,0x05E,0x014,
	0x0E1,0x060,0x068,0x071,0x0F3,0x043,0x07A,0x052,
	0x00C,0x0BA,0x085,0x0AB,0x01E,0x099,0x097,0x088,
	0x028,0x0FC,0x0A1,0x0ED,0x03A,0x0DF,0x0B3,0x0CE,
	0x044,0x036,0x0CD,0x027,0x056,0x015,0x0DF,0x004,
	0x060,0x070,0x0E9,0x061,0x072,0x053,0x0FB,0x042,
	0x08B,0x0C9,0x002,0x0D8,0x099,0x0EA,0x010,0x0FB,
	0x0AF,0x08F,0x026,0x09E,0x0BD,0x0AC,0x034,0x0BD,
	0x0C3,0x045,0x04A,0x054,0x0D1,0x066,0x058,0x077,
	0x0E7,0x003,0x06E,0x012,0x0F5,0x020,0x07C,0x031,
	0x00A,0x0D9,0x083,0x0C8,0x018,0x0FA,0x091,0x0EB,
	0x02E,0x09F,0x0A7,0x08E,0x03C,0x0BC,0x0B5,0x0AD,
	0x042,0x055,0x0CB,0x044,0x050,0x076,0x0D9,0x067,
	0x066,0x013,0x0EF,0x002,0x074,0x030,0x0FD,0x021,
	0x089,0x0E8,0x000,0x0F9,0x09B,0x0CB,0x012,0x0DA,
	0x0AD,0x0AE,0x024,0x0BF,0x0BF,0x08D,0x036,0x09C,
	0x0C1,0x064,0x048,0x075,0x0D3,0x047,0x05A,0x056,
	0x0E5,0x022,0x06C,0x033,0x0F7,0x001,0x07E,0x010,
	0x008,0x0F8,0x081,0x0E9,0x01A,0x0DB,0x093,0x0CA,
	0x02C,0x0BE,0x0A5,0x0AF,0x03E,0x09D,0x0B7,0x08C,
	0x040,0x074,0x0C9,0x065,0x052,0x057,0x0DB,0x046,
	0x064,0x032,0x0ED,0x023,0x076,0x011,0x0FF,0x000
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Extended Serial Driver
//

// Instantiator

INSTANTIATE(CBSAPExtSMDriver);

// Constructor

CBSAPExtSMDriver::CBSAPExtSMDriver(void)
{
	m_Ident	= DRIVER_ID;

	InitGlobal(TRUE);
	}

// Destructor

CBSAPExtSMDriver::~CBSAPExtSMDriver(void)
{
	}

// Configuration

void MCALL CBSAPExtSMDriver::Load(LPCBYTE pData)
{
	WORD w = GetWord(pData);

	if( w == 0x1234 ) {

		m_pDC = &m_BDrvCfg;

		m_pDC->NetLevel   = GetByte(pData);
		m_pDC->ThisLocal  = GetByte(pData);
		m_pDC->ThisGlobal = GetWord(pData);
		m_pDC->PollHigh   = GetByte(pData);

		m_pDC->LevelBitMask[0] = 0;

		for( UINT k = 1; k < 7; k++ ) {

			m_pDC->LevelBitMask[k] = GetByte(pData);
			}

		m_pDC->SourceGlobal	= 0;

		m_pDC->SendGlobal	= 0;

		m_pDC->NRTSent		= FALSE;

		m_pDC->NRTRcvd		= FALSE;

		m_pDC->NThis.Level	= m_pDC->NetLevel;

		m_pDC->GlobalDest	= FALSE;

		m_pDC->NRTCount		= 0;

		MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
		}
	}

void MCALL CBSAPExtSMDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {

		Config.m_uFlags |= flagFastRx;
		}

	switch( Config.m_uBaudRate ) {

		case  9600: m_pDC->PollTimeout = 200;	break;
		case 19200: m_pDC->PollTimeout = 160;	break;
		case 38400: m_pDC->PollTimeout = 120;	break;
		default:    m_pDC->PollTimeout = 100;	break;
		}

	Make485(Config, FALSE);
	}
	
// Management

void MCALL CBSAPExtSMDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

// Master Flags
WORD MCALL CBSAPExtSMDriver::GetMasterFlags(void)
{
	return MF_NO_SPANNING;
	}

void MCALL CBSAPExtSMDriver::Detach() {

	UINT n = 0;

	while( n < 15 ) {

		CContext *pPtr = ContextList[n];
		IDevice  *pDev = DeviceList[n];

		if( pPtr != NULL ) {

			CloseDown(pPtr);

			ContextList[n]	= NULL;
			DeviceList[n]	= NULL;
			}

		n++;
		}

	CMasterDriver::Detach();
	}

// Device

CCODE MCALL CBSAPExtSMDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx           = new CContext;

			AddContextPtr(m_pCtx, pDevice);

			m_pCtx->m_bLocal	= GetByte(pData);
			m_pCtx->m_wGlobal	= GetWord(pData);
			m_pCtx->m_bLevel	= GetByte(pData);
			m_pCtx->m_bPath		= GetByte(pData);
			m_pCtx->m_bMLevel	= GetByte(pData);

			WORD wKey		= GetWord(pData);

			switch( wKey ) {

				case 0xBCDE:	m_pCtx->m_bIsC3 = 3; break;

				default:	return CCODE_ERROR | CCODE_HARD;
				}

			InitGlobal(FALSE);

			LoadTags(pData);

			InitStringSupport(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

UINT MCALL CBSAPExtSMDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 )  {  // Drop Number

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bLocal = uValue;

			return 1;
			}
		}

	if( uFunc == 4 ) {  // Current target drop number 

		return pCtx->m_bLocal;
		}
	
	return 0;
	}

CCODE MCALL CBSAPExtSMDriver::DeviceClose(BOOL fPersist)
{
	return CCODE_SUCCESS;
	}

// Entry Point

// Master Entry Points
CCODE CBSAPExtSMDriver::Ping(void) {

//**/	AfxTrace2("\r\nPING Dev=%d Tags=%d ", m_pCtx->m_bLocal, m_pCtx->m_uTagCount);
	
	if( m_pCtx->m_bLevel > m_pDC->NetLevel ) {

		return 1;	// assume lower levels are connected
		}

	if( m_pCtx->m_uTagCount ) {

		m_pRFrame->fDoMSD  = FALSE;
		m_pRFrame->fReqMSD = FALSE;

		switch( MainPoll() ) {

			case ACKDOWN:
			case ACKPOLL:

				return 1;

			case MFCNRTREQ:

				if( m_BDrvCfg.NetLevel ) {

					if( m_BDrvCfg.NRTRcvd != 0xFF ) {

						return 1;
						}
					}

				if( BuildNRT() ) {

					SendNRT();

					MainPoll();
					}

				return 1;

			default:

				if( m_bRx[0] ) return 1;	// got a response

				for( UINT i = 0; i < m_pCtx->m_uTagCount; i++ ) {

					m_pCtx->m_pwMSDList[i]	= 0;	// force read by name
					m_pCtx->m_pfMSDList[i]  = FALSE;
					m_pCtx->m_uMSDVers	= 0;	// get new version
					}
				break;
			}

		}
	
	return CCODE_ERROR;
	}

CCODE CBSAPExtSMDriver::Read(AREF Addr, PDWORD pData, UINT uCount) {

//**/	if( m_pCtx->m_bLocal == 1 ) AfxTrace0("\r\n*** 1 ***"); else AfxTrace1("\r\n___%d___", m_pCtx->m_bLocal);
//**/	AfxTrace2("READ A=%8.8lx Ct=%d ", Addr.m_Ref, uCount);

	if( !m_pCtx->m_uTagCount ) {

		return CCODE_ERROR | CCODE_NO_DATA;
		}

	switch( Addr.a.m_Table ) {

		case SPREALACK:

			*pData = m_pCtx->m_dAutoAckReal;
			return 1;

		case SPBOOLACK:

			*pData = m_pCtx->m_dAutoAckBool;
			return 1;

		default:
			if( IsNRT(Addr.a.m_Table) ) {

				if( m_pDC->NetLevel > 0 ) {

					if( !(BOOL)m_pDC->NThat.Version ) return 1;

					memcpy(&m_pDC->NThis.TSF, &m_pDC->NThat.TSF, 32);
					}

				else {
					BuildLSC();

					BuildTimeSync(FALSE);

					memcpy(&m_pDC->NThis.Version, &m_pDC->NMast.Version, 20);
					}

				PDWORD p = (PDWORD)&m_pDC->NThis;

				MakeMin(uCount, 0x8008 - Addr.a.m_Offset);

				for( UINT i = 0; i < uCount; i++ ) {

					pData[i] = p[Addr.a.m_Offset + i - 0x8000];
					}

				Sleep(20);

				return uCount;
				}

			break;
		}

//**/	Sleep(100);		// debugging slow down

	CAddress AddrA;

	AddrA.m_Ref = Addr.m_Ref;

	BOOL fIsNotNextLevel = FALSE;

	UINT uGotDataCount   = 0;

	UINT uReqCount       = uCount;

	switch( PrepOperation(Addr, &AddrA.m_Ref, &uCount, &uGotDataCount) ) {

		case NOTHING:		// invalid address
			*pData = 0;
			return 1;

		case 20000:		// not this local address
			return CCODE_ERROR | CCODE_BUSY;

		case 10000:
			fIsNotNextLevel = TRUE;
			break;
		}

	if( !AddrA.a.m_Table ) {

		m_pCtx->m_uMSDVers            = 0;

		if( m_uIndex < m_pCtx->m_uTagCount ) {

			m_pCtx->m_pwMSDList[m_uIndex] = 0;

			m_pCtx->m_pfMSDList[m_uIndex] = FALSE;
			}

		return CCODE_ERROR | CCODE_NO_DATA;
		}

	UINT uNamePos = 0xFFFF;

	m_pRFrame->uCount = GetCount(Addr, uCount);
	m_pRFrame->uAddr  = AddrA.a.m_Offset;
	m_pRFrame->pData  = pData;

	if( fIsNotNextLevel ) {

		if( m_pCtx->m_bPath == m_pCtx->m_pbLevAddList[m_uIndex] + 1 ) {

			return HandlePassThrough(AddrA, pData, uCount, TRUE);
			}

		return CCODE_ERROR | CCODE_NO_DATA;
		}

	if( IsNRT(AddrA.a.m_Table) ) {

		BuildTimeSync(TRUE);

		return HandleNRT(AddrA, pData, uCount);
		}

	if( IsArrayRead(&uNamePos) ) {

		return DoArrayRead(Addr, pData, uGotDataCount, uNamePos);
		}

	if( IsListRead(&uNamePos) ) {

		return DoListRead(Addr, pData, uGotDataCount, uNamePos);
		}

	CCODE c = DoDataRead(AddrA, uGotDataCount, uReqCount);

	if( !(c & CCODE_ERROR) ) {

		if( m_pCtx && m_pCtx->m_pPend && m_pCtx->m_pPend->pCurrent ) {

			m_pCtx->m_pPend->pCurrent->uState = 0;
			}
		
		AlarmInit();

		if( MainPoll() == ALARMNOT ) {

			SendAlarmReportAck();
			}
		}

	return c;
	}

CCODE CBSAPExtSMDriver::Write(AREF Addr, PDWORD pData, UINT uCount) {
//**/	AfxTrace3("\r\n\nWrite\r\nT=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);
//**/	for( UINT ii = 0; ii < uCount; ii++ ) AfxTrace1("%8.8lx ", pData[ii]);

	if( !m_pCtx->m_uTagCount ) {

		return CCODE_ERROR | CCODE_NO_DATA;
		}

	if( IsNRT(Addr.a.m_Table ) ) {

		return HandleNRT(Addr, pData, uCount);
		}

	UINT uCnt = CheckAlarms(Addr, pData, uCount);

	if( uCnt < NOTHING ) {

		return (BOOL)uCnt ? uCnt : 1;
		}

	m_uIndex  = SetItemIndex(Addr);

	uCnt      = GetItemCount(Addr, uCount);

	BOOL fIsNotNextLevel = FALSE;

	switch( uCnt ) {

		case NOTHING:
		case 20000:
			return uCount;

		case 10000:
			fIsNotNextLevel = TRUE;
			break;
		}

	if( fIsNotNextLevel ) {

		if( m_pCtx->m_bPath == m_pCtx->m_pbLevAddList[m_uIndex] + 1 ) {

			return HandlePassThrough(Addr, pData, uCount, FALSE);
			}

		return CCODE_ERROR | CCODE_NO_DATA;
		}

	return DoDataWrite(Addr, pData, uCnt);
	}

// Global Pass Through
CCODE CBSAPExtSMDriver::HandlePassThrough(CAddress Addr, PDWORD pData, UINT uCount, BOOL fIsRead) {

	m_fGlobal = TRUE;

	if( fIsRead ) {

		UINT uGotData = 0;

		return DoDataRead(Addr, uGotData, uCount);
		}

	else {
		return DoDataWrite(Addr, pData, uCount);
		}

	return CCODE_ERROR;
	}

UINT CBSAPExtSMDriver::SetItemIndex(AREF Addr) {

	UINT uIndex  = NOTHING;

	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	if( IsSTR(Addr.a.m_Table) ) {

		Address.a.m_Offset = Addr.a.m_Offset - (Addr.a.m_Offset % MAX_LTEXT); 
		}

	if( SearchValue(Address.m_Ref, m_pCtx->m_uTagCount, m_pCtx->m_pdAddrList, NULL, &uIndex) == 1 ) {

		return uIndex;
		}

	return NOTHING;
	}

UINT CBSAPExtSMDriver::GetItemCount(AREF Addr, UINT uMax) {

	DWORD dSrc  = Addr.m_Ref;

	UINT uExtra = Addr.a.m_Extra;

	if( m_pCtx->m_pbLevAddList[uExtra] != m_pCtx->m_bLevel + 1 ) {

		return 10000;	// item on another level
		}

	if( m_pCtx->m_pbDevAddList[uExtra] != m_pCtx->m_bLocal ) {

		return 20000;	// not this device address
		}

	UINT uInx = m_uIndex;

	BOOL fMSD = m_pCtx->m_pfMSDList[uInx];

	UINT uCnt = uMax;

	m_pRFrame->fReqMSD = !fMSD;
	m_pRFrame->fDoMSD  = fMSD;

	if( uMax > 1 ) {

		PDWORD pa  = &m_pCtx->m_pdAddrList[uInx + 1];
		PWORD  pm  = &m_pCtx->m_pwMSDList [uInx + 1];

		UINT i     = 1;

		while( i < uMax ) {

			BOOL fItem = m_pCtx->m_pfMSDList[uInx + 1];

			UINT uInc  = ExpandCount(Addr, i);

			if( (*pa == dSrc + uInc) && fItem == fMSD ) {

				i++;	// include item with sequential address and equivalent MSD status
				pa++;
				pm++;

				continue;
				}

			else {
				uMax = i;
				break;
				}
			}
		}

	return ExpandCount(Addr, uMax, uCnt);
	}

void CBSAPExtSMDriver::MakeBaseHeader(BYTE bSFC, BYTE bDFC, BOOL fIncSer) {

	m_pTx   = m_bTx;

	m_uTPtr = 0;

	AddByte(m_pCtx->m_bLocal);

	if( fIncSer ) {

		if( !(BOOL)(++m_pCtx->m_bSerNum) ) m_pCtx->m_bSerNum = 1;
		}

	AddByte(m_pCtx->m_bSerNum);
	AddByte(bSFC);
	AddByte(LOBYTE(m_pCtx->m_uAppSeq));
	AddByte(HIBYTE(m_pCtx->m_uAppSeq));
	AddByte(bDFC);
	AddByte(0);
	}

UINT CBSAPExtSMDriver::PrepOperation(CAddress Addr, PDWORD pNewAddr, UINT *pCount, UINT *pGotData) {

	m_uIndex  = SetItemIndex(Addr);

	UINT uRtn = GetItemCount(Addr, *pCount);

	switch( uRtn ) {

		case NOTHING:		// invalid address
		case 20000:		// not this device address
			return uRtn;
		}

	BOOL fIsNextLevel = uRtn == 10000;

	*pCount = uRtn;

	if( m_pCtx->m_pPend->uPendPtrCt ) {

		CAddress AddrA;

		AddrA.m_Ref = GetPendingRef(Addr, pCount, pGotData);

		*pNewAddr = AddrA.m_Ref;
		}

	else *pNewAddr = Addr.m_Ref;

	return fIsNextLevel ? uRtn : *pCount;
	}

DWORD CBSAPExtSMDriver::GetPendingRef(AREF Addr, UINT *pCount, UINT *pGotData) {

	CAddress AddrA;

	AddrA.m_Ref  = Addr.m_Ref;

	PENDING * pRP = m_pCtx->m_pPend->ppPendList[0];

	m_pCtx->m_pPend->pCurrent = pRP;

	if( !(SearchPending(AddrA, FALSE, FALSE)) ) {

		return Addr.m_Ref;
		}

	if( m_pCtx->m_pPend->pCurrent == NULL ) {

		return 0;	// out of memory
		}

	pRP = m_pCtx->m_pPend->pCurrent;

	if( !(BOOL)pRP->uState ) {	// no previous entry in table for this request

		pRP->dAddr	= Addr;
		pRP->uIndex	= m_uIndex;
		pRP->uCount	= *pCount;
		pRP->LevDiff	= m_pCtx->m_pbLevAddList[Addr.a.m_Extra] - m_pCtx->m_bLevel + 1;
		pRP->uState	= 1;
		pRP->wAppSeq	= m_pCtx->m_uAppSeq;
		}

	else {
		UINT uPP = PollPending(pRP);

		if( (BOOL)uPP ) {

			if( (WORD)(IntelToHost(*(PU2)&m_bRx[RXLSQL])) == pRP->wAppSeq ) {

				*pGotData = pRP->uCount;

				m_uIndex  = pRP->uIndex;

				pRP->uState = 0;

				return AddrA.m_Ref;
				}

			AddrA    = pRP->dAddr;
			*pCount  = pRP->uCount;
			m_uIndex = pRP->uIndex;
			NextSeq((UINT *)&m_pCtx->m_bSerNum, TRUE);
			}

		else {
			if( !(BOOL)(--pRP->LevDiff) ) {

				pRP->uState = 0;
				}

			return 0;
			}
		}

	return AddrA.m_Ref;
	}

BOOL CBSAPExtSMDriver::SearchPending(AREF Addr, BOOL fAdd, BOOL fKill) {

	UINT uListSize = m_pCtx->m_pPend->uListCt;

	PENDING *pOpenSpace = NULL;

	UINT i = 0;

	BOOL fFound = FALSE;

	PENDING *p  = NULL;

	while( i < uListSize ) {

		p = m_pCtx->m_pPend->ppPendList[i];

		if( p->dAddr.m_Ref != Addr.m_Ref ) {

			i++;

			if( !(BOOL)p->uState && pOpenSpace == NULL ) {

				pOpenSpace = p;
				}

			continue;
			}

		fFound = TRUE;

		break;
		}

	if( fFound ) {

		if( fKill ) {

			p->uState	= 0;

			p->dAddr.m_Ref	= 0;

			return FALSE;
			}

		m_pCtx->m_pPend->pCurrent = p;

		return fAdd;
		}

	if( fAdd ) {

		if( pOpenSpace || ExpandppList(pOpenSpace, uListSize) ) {

			m_pCtx->m_pPend->uPendPtrCt++;

			m_pCtx->m_pPend->pCurrent = pOpenSpace;

			memset(m_pCtx->m_pPend->pCurrent, 0, PENDSIZE);

			return TRUE;
			}

		else {

			m_pCtx->m_pPend->pCurrent = NULL;
			}
		}

	return fAdd;
	}

BOOL CBSAPExtSMDriver::ExpandppList(PENDING *pOpen, UINT uListSize) {

	UINT uNewSize = (uListSize + 1) * sizeof(PENDING *);

	PENDING ** pp = (PENDING **) new BYTE [uNewSize];

	if( pp ) {

		UINT i = 0;

		PENDING *p;

		for( i = 0; i < uListSize; i++ ) {

			p = m_pCtx->m_pPend->ppPendList[i];

			memcpy(pp, p, sizeof(PENDING *));
			}

		memset(pp[uListSize], 0, sizeof(PENDING));

		if( m_pCtx->m_pPend->ppPendList ) {

			delete [] m_pCtx->m_pPend->ppPendList;
			}

		m_pCtx->m_pPend->ppPendList = (PENDING **) new BYTE [uNewSize];

		memcpy(m_pCtx->m_pPend->ppPendList, pp, uNewSize);

		m_pCtx->m_pPend->uListCt = uNewSize / PENDSIZE;

		delete [] pp;

		return TRUE;
		}

	return FALSE;
	}

// Read Handlers
void CBSAPExtSMDriver::MakeReadHeader(void) {

	PutStart(m_fGlobal, m_pCtx->m_bLocal);

	AddByte(MFCRDDB);

	m_pRFrame->bSrcFC = MFCRDDB;

	AddByte(0);
	}

CCODE CBSAPExtSMDriver::DoDataRead(AREF Addr, UINT uGotDataCount, UINT uReqCount)
{	
	UINT uState	= 1;

	READFRAME * p	= m_pRFrame;

	BOOL fGotData	= (BOOL)uGotDataCount;

	if( !fGotData ) {

		m_pTx = m_bTx;

		MakeReadHeader();

		if( p->fDoMSD ) {

			if( !AddReadByAddr() ) {

				return CCODE_ERROR;
				}
			}

		else {
			if( !AddReadByName() ) {

				return CCODE_ERROR;
				}
			}

		uState	= Transact(TRUE);
		}

	PDWORD pData	= p->pData;

	UINT uCount	= fGotData ? uGotDataCount : p->uCount;

	UINT uPos	= m_fGlobal ? 11 : 6;

	m_fInAlarm	= m_bRx[uPos] & 1;

	UINT uGood	 = 0;

	if( uState == 1 ) {

		uPos		= m_fGlobal ? 12 : 7;

		BOOL fHasErr	= m_bRx[uPos] & 0x80;				// error(s) present code

		uCount		= min( m_bRx[uPos + 1], p->uCount );		// item count to process

		p->uCount	= uCount;

		BYTE bEndString	= 0;

		uPos += 2;

		UINT uGot = 0;

		for( UINT i = 0; i < uCount; i++, pData++ ) {

			DWORD dData = 0;

			BOOL fErr = fHasErr && (BOOL)m_bRx[uPos];

			if( fHasErr ) uPos += 1;	// allow for leading error byte

			if( !fErr ) {

				dData = GetReadData(Addr, &uPos, &bEndString, m_uIndex + i, uReqCount);

				uGot++;

				if( !IsSTR(Addr.a.m_Table) ) {

					*pData = dData;
					}
				}

			else {

				i = uCount;	// break loop if error detected
				}

			if( (BOOL)bEndString ) {

				return uCount;
				}
			}

		if( uGot ) {

			if( p->fReqMSD ) {

				p->fDoMSD  = TRUE;
				p->fReqMSD = FALSE;
				}

			return ExpandCount(Addr, uGot, uReqCount);
			}

		return CCODE_ERROR | CCODE_NO_DATA;
		}

	CAddress A;

	A.m_Ref = m_pCtx->m_pdAddrList[m_uIndex];

	if( uState == 2 ) {	// ack'd but no data

		SearchPending( A, TRUE, FALSE );
		}

	if( uState == 4 ) {	// Ack, but message discarded

		PPEND *p = m_pCtx->m_pPend;

		UINT uLC = p->uListCt;

		if( uLC ) {

			SearchPending(A, FALSE, TRUE);
			}

		return 1;	// generally response from NRT send
		}

	if( (BOOL)uState ) {

		return CCODE_ERROR | CCODE_NO_DATA;
		}

	return CCODE_ERROR;
	}

DWORD CBSAPExtSMDriver::GetReadData(AREF Addr, UINT *pOffset, PBYTE pEndString, UINT uIndex, UINT uReqCount)
{
	UINT uPos  = *pOffset;

	UINT uDPos = uPos + 1;

	BYTE bType = (m_bRx[uPos++]) & 0x3;

	DWORD d    = 0;

	BYTE bNull = 0xFF;

	switch( bType ) {

		case 0:	// type is bit

			BOOL f;

			f = (BOOL)m_bRx[uPos++];

			d = (DWORD)(f ? (Addr.a.m_Type == addrRealAsReal ? 0x3F800000 : 1) : 0);

			break;

		case 2:	// type is analog

			d = (DWORD)IntelToHost(*(PU4)&m_bRx[uPos]);

			uPos += 4;

			break;

		case 3:	// type is string

			d = GetStringData(Addr, uPos, uDPos, uIndex - m_uIndex, uReqCount);

			break;
		}

	if( m_pRFrame->fReqMSD ) {

		WORD wMSD = (WORD)IntelToHost(*(PU2)&m_bRx[uPos]);

		m_pCtx->m_pwMSDList[uIndex] = wMSD;

		m_pCtx->m_pfMSDList[uIndex] = TRUE;
			
		m_pCtx->m_uMSDVers = (WORD)IntelToHost(*(PU2)&m_bRx[uPos+2]);

		uPos += 4;
		}

	*pOffset = uPos;

	if( !IsSTR(Addr.a.m_Table) ) {

		float Real = I2R(d);

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	return (DWORD)((BOOL)d);
			case addrByteAsByte:	return (DWORD)LOBYTE(Real);
			case addrWordAsWord:	return (DWORD)LOWORD(Real);
			case addrLongAsLong:	return (DWORD)Real;
			}
		}

	return d;
	}

DWORD CBSAPExtSMDriver::GetStringData(AREF Addr, UINT &uPos, UINT uDPos, UINT uIndex, UINT uReqCount)
{	
	UINT uStart  = ExpandCount(Addr, uIndex);

	UINT uChar   = GetCharCount((PCTXT)m_bRx, uDPos);

	UINT uAdd    = uChar % 4 ? 1 : 0;

	UINT uCopy   = min(uChar / sizeof(DWORD) + uAdd, MAX_LTEXT);

	MakeMin(uCopy, uReqCount);

	for( UINT u  = uStart; u < uStart + uCopy; u++, uDPos += 4 ) {

		m_pRFrame->pData[u] = (DWORD)IntelToHost(*(PU4)&m_bRx[uDPos]);
		}

	PTXT pText   = (PTXT)(m_pRFrame->pData + uStart);
	
	CAddress Address;
	
	Address.m_Ref = Addr.m_Ref;
	
	Address.a.m_Offset += uStart;
	
	SetStringContext((AREF)Address, (PCTXT)pText, min(uChar, uCopy * sizeof(DWORD)));

	uPos += uChar + 1;

	return m_pRFrame->pData[0];
	}

BOOL CBSAPExtSMDriver::AddReadByName(void)
{
	AddByte(OFCRDBYN);

	AddByte(3);

	AddByte(FS1TYPINH | FS1VALUE | FS1MSDADD);

	AddByte(FS2MSDVER);

	AddByte(0xFF);

	m_pRFrame->fReqMSD = TRUE;

	m_pRFrame->fDoMSD  = FALSE;

	UINT uPos = m_uTPtr;	// position of count

	AddByte(0);		// will be count

	UINT uCount = m_pRFrame->uCount;

	PCTXT pN   = (PCTXT)m_pCtx->m_pbNameList;
	PWORD pI   = m_pCtx->m_pwNameIndex;
	UINT uInx  = m_uIndex;

	UINT n     = 0;

	UINT uLast = m_uTPtr;	// position at start of name

	while( n < uCount ) {

		AddTextPlusNull( &pN[pI[uInx+n]] );

		if( m_uTPtr > MAXSEND ) {	// too many characters

			m_pRFrame->uCount = n;

			m_uTPtr = uLast;

			break;
			}

		n++;
		}

	m_pTx[uPos] = n;

	return TRUE;
	}

BOOL CBSAPExtSMDriver::AddReadByAddr(void) {

	MakeMin(m_pRFrame->uCount, 32);

	UINT uCount = m_pRFrame->uCount;

	StartReadByAddr(uCount);

	m_pRFrame->fReqMSD = FALSE;

	PWORD pW    = &m_pCtx->m_pwMSDList[m_uIndex];

	for( UINT n = 0; n < uCount; n++ ) {

		AddWord(pW[n]);
		}

	return TRUE;
	}

void CBSAPExtSMDriver::StartReadByAddr(UINT uCount) {

	AddByte(OFCRDBYA);
	AddByte(1);
	AddByte(FS1TYPINH | FS1VALUE);
	AddWord(m_pCtx->m_uMSDVers);
	AddByte(0xFF);
	AddByte(uCount);
	}

BOOL CBSAPExtSMDriver::IsArrayRead(UINT *pPos) {

	UINT uPos = m_pCtx->m_pwNameIndex[m_uIndex];

	char s[11] = "_READARRAY";
	s[10] = 0;

	char n[11];
	n[10] = 0;

	memcpy(n, &m_pCtx->m_pbNameList[uPos], 10);

	*pPos = uPos;

	return !strnicmp(n, s, 10);
	}

CCODE CBSAPExtSMDriver::DoArrayRead(CAddress Addr, PDWORD pData, UINT uCount, UINT uNamePos) {

	m_pTx = m_bTx;

	MakeReadHeader();

	AddByte(OFCRARRG);

	AddByte(IsBIT(Addr.a.m_Table) ? 1 : 0);

	const char *pName = (const char *)&m_pCtx->m_pbNameList[uNamePos];

	UINT uArrayNum = ATOI(&pName[10]);	// get initial array number

	if( !(BOOL)uArrayNum ) return CCODE_ERROR | CCODE_NO_DATA;	// invalid configuration

	AddByte(LOBYTE(LOWORD(uArrayNum)));

	WORD wConfig[3];	// Configured Num, Row, Col

	wConfig[0] = uArrayNum;

	if( !FindArraySpec(pName, wConfig) ) return CCODE_ERROR | CCODE_NO_DATA;

	AddWord(wConfig[1]);	// starting row
	AddWord(wConfig[2]);	// starting column

	CAddress A;

	A.m_Ref = Addr.m_Ref;

	UINT n = 1;

	while( n < uCount ) {	// count consecutive items

		WORD wRow0 = wConfig[1];	// previous row
		WORD wCol0 = wConfig[2];	// previous col

		A.a.m_Offset++;		// next offset

		UINT uIndex = SetItemIndex(Addr);

		if( uIndex == NOTHING ) break;

		pName = (const char *)&m_pCtx->m_pbNameList[m_pCtx->m_pwNameIndex[uIndex]];

		if( !FindArraySpec(pName, wConfig) ) break;

		WORD wRow1 = wConfig[1];
		WORD wCol1 = wConfig[2];

		if( wRow1 != wRow0 ) {

			if( wRow1 != wRow0 + 1 ) break;

			if( wCol1 != 1 ) break;
			}

		else {
			if( wCol1 != wCol0 + 1 ) break;
			}
		n++;
		}

	UINT uCountToGet = n;

	AddByte(n);		// number of array items to get
	
	if( Transact(TRUE) ) {

		UINT uPos = m_fGlobal ? 12 : 7;

		if( (BOOL)m_bRx[uPos++] ) return CCODE_ERROR | CCODE_NO_DATA;	// response error

		MakeMin(uCount, m_bRx[uPos]);		// min of request and number of items actually returned

		uPos += 4;	// skip over count, array number, next row, next column to get to data

		if( Addr.a.m_Type == addrRealAsReal ) {

			for( n = 0; n < uCount; n++, uPos += 4 ) {

				pData[n] = (DWORD)(IntelToHost(*(PU4)&m_bRx[uPos]));
				}
			}

		else {
			BYTE b = 0;

			for( n = 0; n < uCount; n++, uPos++ ) {

				if( !(BOOL)(n % 8) ) {

					b   = m_bRx[uPos];
					}

				pData[n] = (DWORD)(b & (0x80 >> (n % 8)) ? TRUE : FALSE);
				}
			}

		return uCount;
		}

	return CCODE_ERROR | CCODE_NO_DATA;
	}

BOOL CBSAPExtSMDriver::IsListRead(UINT *pPos) {

	UINT uPos = m_pCtx->m_pwNameIndex[m_uIndex];

	char s[10] = "_READLIST";
	s[9] = 0;

	char n[10];
	n[9] = 0;

	memcpy(n, &m_pCtx->m_pbNameList[uPos], 9);

	*pPos = uPos;

	return !strnicmp(n, s, 9);
	}

CCODE CBSAPExtSMDriver::DoListRead(CAddress Addr, PDWORD pData, UINT uCount, UINT uNamePos) {

	m_pTx = m_bTx;

	MakeReadHeader();

	AddByte(OFCRDBYLC);

	AddByte(1);

	AddByte(0x4);	// read value only

	AddByte(m_pCtx->m_uMSDVers);

	AddByte(0xFF);

	const char *pName = (const char *)&m_pCtx->m_pbNameList[uNamePos];

	UINT uPos = 9;

	if( FindChar((char *)pName, &uPos, 200) ) {

		AddByte(LOBYTE(LOWORD(ATOI(&pName[uPos]))));

		if( Transact(TRUE) ) {


			}
		}

	return CCODE_ERROR | CCODE_NO_DATA;
	}

BOOL CBSAPExtSMDriver::FindArraySpec(const char *pName, PWORD pConfig) {

	UINT uPos = 10;

	char c;

	char *pN = (char *)pName;

	if( !FindChar( pN, &uPos, 200 ) ) return FALSE;	// no Command Number

	if( LOWORD(ATOI(&pN[uPos])) != pConfig[0] ) return FALSE;	// Array Number change

	pConfig[1] = (WORD)FindChar( pN, &uPos, 210 ) ? LOWORD(ATOI(&pN[uPos])) : 0;	// Item Row
	pConfig[2] = (WORD)FindChar( pN, &uPos, 220 ) ? LOWORD(ATOI(&pN[uPos])) : 0;	// Item Col

	return FALSE;
	}

BOOL CBSAPExtSMDriver::FindChar(char *pName, UINT *pPos, UINT uCharType) {

	UINT uPos = *pPos;

	BOOL fOk  = FALSE;

	while( uPos < 32 ) {

		char c = pName[uPos];

		if( !(BOOL)c ) return FALSE;	// end of string

		switch( uCharType ) {

			case 200: fOk = isdigit(c);	break;
			case 210: fOk = c == 'R' || c == 'r'; break;
			case 220: fOk = c == 'C' || c == 'c'; break;
			}

		if( fOk ) {

			*pPos = uPos;
			return TRUE;
			}

		uPos++;
		}

	return FALSE;
	}

// Write routines
CCODE CBSAPExtSMDriver::DoDataWrite(AREF Addr, PDWORD pData, UINT uCount) {

	UINT uMSD = m_pCtx->m_pwMSDList[m_uIndex];
	UINT uVer = m_pCtx->m_uMSDVers;

	if( !(m_pCtx->m_pfMSDList[m_uIndex] || (BOOL)uVer) ) {

		if( Read(Addr, pData, uCount) & CCODE_ERROR ) {

			return uCount;	// item inaccessible
			}
		}

	return WriteByAddress(Addr, pData, uCount) ? uCount : CCODE_ERROR;
	}

BOOL CBSAPExtSMDriver::WriteByAddress(AREF Addr, PDWORD pData, UINT uCount)
{
	AddWriteHeader();

	AddByte(OFCWRBYA);			// Write by address

	AddByte(LOBYTE(m_pCtx->m_uMSDVers));	// Version
	AddByte(HIBYTE(m_pCtx->m_uMSDVers));

	AddByte(0xFF);				// security level

	BOOL fStr  = IsSTR(Addr.a.m_Table);

	UINT Count = GetCount(Addr, uCount);

	AddByte(LOBYTE(Count));

	PWORD  pM = &m_pCtx->m_pwMSDList[m_uIndex];
	PBYTE  pT = &m_pCtx->m_pbTypeList[m_uIndex];

	UINT k = 0;

	while( k < Count ) {
		
		AddByte(LOBYTE(pM[k]));
		AddByte(HIBYTE(pM[k]));

		if( !fStr ) {
			
			AddWriteData(Addr.a.m_Type, *pData);
			}
		else {
			AddStringData(Addr, pData, k, uCount);
			}
		
		k++;
		}

	return Transact(TRUE);
	}

void CBSAPExtSMDriver::AddWriteData(UINT uType, DWORD dData)
{	
	switch(uType) {

		case addrByteAsByte:	dData &= 0xFF;		break;

		case addrWordAsWord:	dData &= 0xFFFF;	break;
		}

	float Real = float(LONG(dData));

	switch(uType) {

		case addrBitAsBit:

			AddByte(dData ? 9 : 10);
			break;

		case addrByteAsByte:

			AddByte(WFDANALG);
			AddLong(R2I(Real));
			break;

		case addrWordAsWord:

			AddByte(WFDANALG);
			AddLong(R2I(Real));
			break;

		case addrLongAsLong:

			AddByte(WFDANALG);
			AddLong(R2I(Real));
			break;

		case addrRealAsReal:

			AddByte(WFDANALG);
			AddLong(dData);
			break;
		}
	}

void CBSAPExtSMDriver::AddStringData(AREF Addr, PDWORD pData, UINT uIndex, UINT uCount)
{	
	UINT uMax    = MAX_LTEXT;

	UINT uFrom   = ExpandCount(Addr, uIndex);

	UINT uOffset = (Addr.a.m_Offset + uFrom) % uMax;

	PBYTE pCopy  = PBYTE(alloca(MAX_CHARS));

	memset(pCopy, 0, MAX_CHARS);

	MakeMin(uCount, uMax);

	if( uOffset ) {

		CAddress Address;

		Address.m_Ref = Addr.m_Ref + uFrom - uOffset;

		UINT uString  = GetStringContextIndex((AREF)Address);

		if( uString < NOTHING ) {

			memcpy(pCopy, m_pCtx->m_pStrings[uString].m_Text, MAX_CHARS);
			}
		}

	PDWORD pStr = PDWORD(pCopy);

	if( uIndex && uOffset ) {

		uFrom  -= uOffset;

		uOffset = 0;
		}

	for( UINT n = 0; n < uCount; n++ ) {

		pStr[n + uOffset] = HostToIntel(pData[n + uFrom]);
		}

	RemoveTrailing(pCopy, 0x20, MAX_CHARS);

	AddByte(WFDSTRNG);

	UINT uChar = GetCharCount((PCTXT)pCopy, 0);

	UINT uAdd  = uChar % 4 ? 1 : 0;

	UINT uCopy = uChar / sizeof(DWORD) + uAdd;

	for( UINT u = 0; u < uCopy; u++ ) {

		AddLong(pStr[u]);
		}

	while( m_pTx[m_uTPtr - 1] == 0 ) {

		m_uTPtr--;
		}

	AddByte(0);
	}

BOOL CBSAPExtSMDriver::WriteByName(void)
{
	// not used. Doing Read before write to get MSD and MSD Version
	return FALSE;
	}

void CBSAPExtSMDriver::AddWriteHeader(void) {

	PutStart(FALSE, m_pCtx->m_bLocal);	// Basic Header Information

	AddByte(MFCWRDB);			// Source Function Code
	AddByte(0);				// Node Status
	}

// String Support

UINT CBSAPExtSMDriver::GetStringContextIndex(AREF Addr)
{
	if( IsSTR(Addr.a.m_Table) ) {

		UINT uString = Addr.a.m_Offset / MAX_LTEXT;

		if( uString < m_pCtx->m_uStrings ) {

			return uString;
			}
		}

	return NOTHING;
	}

void CBSAPExtSMDriver::SetStringContext(AREF Addr, PCTXT pText, UINT uCount)
{
	CStringTag String;

	UINT uIndex = GetStringContextIndex(Addr);

	if( uIndex < NOTHING ) {

		memset(m_pCtx->m_pStrings[uIndex].m_Text, 0, MAX_CHARS);

		memcpy(m_pCtx->m_pStrings[uIndex].m_Text, pText, uCount);
		}
	}

UINT CBSAPExtSMDriver::GetStringCount(AREF Addr, UINT uPos, UINT uCount)
{
	UINT uSize    = MAX_LTEXT;

	UINT uOffset  = Addr.a.m_Offset % uSize;

	if( uPos == 0 ) {

		return uSize - uOffset;
		}

	return min(uCount, uSize);
	}

UINT CBSAPExtSMDriver::GetCharCount(PCTXT pText, UINT uOffset)
{
	UINT uChars = 0;

	if( pText ) {

		while( IsSTRText(pText[uOffset + uChars]) ) {

			uChars++;
			}
		}

	return uChars;
	}

void CBSAPExtSMDriver::RemoveTrailing(PBYTE &pByte, BYTE bByte, UINT uBytes)
{
	UINT uRemove = pByte? uBytes - 1 : 0;

	while( uRemove > 0 ) {

		if( pByte[uRemove] == 0x20 ) {

			pByte[uRemove] = 0;

			uRemove--;

			continue;
			}
		break;
		}
	}

// Polling
UINT CBSAPExtSMDriver::MainPoll(void)
{
	BuildTimeSync(FALSE);

//**/	Sleep(100);

	StartPoll();

//**/	AfxTrace0("\r\nMain Poll ");

	AddByte(POLLMSG);

	AddByte(0x10);

	UINT u = Transact(TRUE);

	if( (BOOL)u ) {

		m_pCtx->m_bSerNum += (m_pCtx->m_bSerNum < 0xFF) ? 1 : 2;

		BYTE bFC = m_bRx[RXDFC];

		switch( bFC ) {

			case ACKDOWN:
			case ACKPOLL:
			case NRTREQ:
			case NAKDATA:
			case ALARMACK:
			case ALARMNOT:
				return bFC;
			}

		return 1;	// data response
		}

	return 0;
	}

UINT CBSAPExtSMDriver::PollPending(PENDING *pPending) {

	BYTE b = m_pCtx->m_bSerNum;

	UINT u = MainPoll();

	return u == 1 ? u : 0;
	}

void CBSAPExtSMDriver::StartPoll(void)
{
	m_pTx = m_bPoll;

	m_pData->Write(0, FOREVER);

	m_uTPtr = 0;

	AddByte(m_pCtx->m_bLocal);
	AddByte(m_pCtx->m_bSerNum);

	m_bRx[0] = 0;
	}

BOOL CBSAPExtSMDriver::SynchPolls(void)
{
//**/	AfxTrace0("\r\n\nSync Polls ");

	UINT u = 20;

	while( TRUE ) {

		UINT uPoll = MainPoll();

		if( !uPoll ) {

			return FALSE;
			}

		BYTE b = m_bRx[RXDFC];

		if( !((BOOL)b & 0x80) ) {

			m_pCtx->m_uMsgSeq = m_bRx[RXSERN] + 1;

			SendDataAck();
			}

		else {
			switch( b ) {

				case POLLMSG:	return FALSE;
				case ACKPOLL:	return TRUE;
				case ALARMNOT:	SendAlarmAck();	return TRUE;
				case ACKDOWN:	break;
				default:	return FALSE;
				}
			}

		if( !(BOOL)(u--) ) {

			return FALSE;
			}
		}

	return FALSE;
	}

void CBSAPExtSMDriver::SendDataAck(void)
{
	StartPoll();

	AddByte(UPACK);

	AddByte(m_bRx[RXSERN]);

	Transact(FALSE);
	}

void CBSAPExtSMDriver::SendAlarmAck(void)
{
	MakeBaseHeader(ALARMACK, ALARMACK, TRUE);

	NextSeq(&m_pCtx->m_uAppSeq, FALSE);

	AddByte(1);		// Ack one at a time

	AddByte(m_bRx[10]);	// Load Version A
	AddByte(m_bRx[11]);	// Load Version B
	AddByte(m_bRx[12]);	// MSD Address A
	AddByte(m_bRx[13]);	// MSD Address B

	Transact(TRUE);
	}

BOOL CBSAPExtSMDriver::GetACCOLVersion(void)
{
//**/	AfxTrace0("\r\n\nGet Version ");

	m_pTx = m_bTx;

	PutStart(FALSE, m_pCtx->m_bLocal);

	AddByte(MFCCOMREQ);
	AddByte(8);
	AddByte(0);

	if( Transact(TRUE) ) {
	
		if( m_bRx[RXDFC] == ACKPOLL ) {

			MainPoll();
			}

		if( m_bRx[RXDFC] == MFCRDDB ) {

			m_pCtx->m_uACCOLVers = m_bRx[9] + (m_bRx[10] << 8);

			SendDataAck();
			}
		}

	return TRUE;
	}

void CBSAPExtSMDriver::SendListRequest(UINT uList)
{
	m_pTx = m_bTx;

//**/	AfxTrace0("\r\nSend List Request ");

	PutStart(FALSE, m_pCtx->m_bLocal);

	AddByte(0xC);
	AddByte(1);
	AddByte(0x08);
	AddByte(LOBYTE(m_pCtx->m_uACCOLVers));
	AddByte(HIBYTE(m_pCtx->m_uACCOLVers));
	AddByte(0xFF);
	AddByte(uList);

	m_bRx[RXDFC] = 0;

	if( Transact(TRUE) ) {

//**/		AfxTrace0("\r\n\nSent List Request ");

		if( m_bRx[RXDFC] == ACKDOWN ) {

			if( MainPoll() ) {		// get data

				SendDataAck();		// send thanks

				MainPoll();	// re-poll
				}
			}
		}
	}

void CBSAPExtSMDriver::PutStart(BOOL fGlobal, BYTE bLocal)
{
	m_uTPtr = 0;

	BYTE a  = bLocal;

	if( fGlobal ) {

		AddByte(a | 0x80);
		AddByte(m_pCtx->m_bSerNum);

		WORD wGlobal	= 0;
		WORD wDestLevel	= m_pCtx->m_bLevel;
		WORD wThisLevel	= m_pDC->NetLevel;

		UINT uD = m_pCtx->m_bLevel;
		UINT uS = m_pDC->NetLevel;

		UINT uCt = 0;

		while( uS + uCt < uD ) {
		
			wGlobal |= m_pDC->NMast.LBM[uS + uCt] << m_pDC->NMast.LSC[uS + uCt];

			uCt++;
			}

		AddWord(wGlobal);
		AddWord(m_pCtx->m_wGlobal);
		AddByte(0);				// Control Byte
		}

	else {
		AddByte(a);
		AddByte(m_pCtx->m_bSerNum);
		}

	AddByte(MFCRDBACC);

	AddByte(LOBYTE(m_pCtx->m_uAppSeq));
	AddByte(HIBYTE(m_pCtx->m_uAppSeq));				// get database access

	NextSeq(&m_pCtx->m_uAppSeq, FALSE);
	}

void CBSAPExtSMDriver::NextSeq(UINT *pSeq, BOOL fByte) {

	UINT uSeq = *pSeq + 1;

	if( !(BOOL)uSeq || (fByte && !(BOOL)LOBYTE(uSeq)) ) {

		uSeq = 1;
		}

	*pSeq = uSeq;
	}

WORD CBSAPExtSMDriver::DoCRC(PBYTE pBuff) {

	BYTE crc_l = 0;
	BYTE crc_h = 0xFF;

	BOOL bUseDLE = FALSE;
	BOOL bSkip   = FALSE;

	UINT n = 2;

	BOOL f = FALSE;

	while( !f ) {

		BYTE b	= pBuff[n];

		bSkip = b == DLE && !bUseDLE;	// skip 1st DLE if present;

		f = b == ETX && bUseDLE;	// done once ETX after DLE

		if( !bSkip ) {

			UINT t	= 2 * (b ^ crc_l);

			crc_l	= (crc_h ^ crctbl[t]);
			crc_h	= crctbl[t+1];

			bUseDLE	= FALSE;
			}

		else bUseDLE = TRUE;

		n++;
		}

	return (WORD)(crc_h << 8) | (WORD)(0xFF & (~crc_l));
	}

// Field Select Helpers
BOOL CBSAPExtSMDriver::ParseVarName(PCTXT Name, UINT uSelect, PBYTE pItem)
{
	UINT uNameLen = strlen((const char *)Name);

	memset(pItem, 0, uNameLen);

	for( UINT i = 0, j = 0; i < uNameLen; i++ ) {

		BYTE b = Name[i];

		if( !uSelect ) {

			if( b == '.' ) return (BOOL)pItem[0];

			pItem[j++] = b;
			}

		else {
			if( b == '.' ) uSelect--;
			}

		return (BOOL)pItem[0];
		}

	return FALSE;
	}

BOOL CBSAPExtSMDriver::AddFieldSels(void)
{
	WORD wPkt0 = m_uTPtr;

	AddField1();
	AddField2();
	AddField3();

	return TRUE;
	}

void CBSAPExtSMDriver::AddField1(void)
{
	VARINFO *p = m_pVarInfo;

	BYTE b     = m_bFSS[1];

	if( b ) {

		DWORD d    = p->Data;

		BOOL fLog  = p->Logical;
		BOOL fTrue = (BOOL)(d);

		if( b & 0x80 ) AddByte(fLog ? 0 : 2);		// Type = logical or analog

		if( b & 0x40 ) {

			if( fLog ) AddByte(fTrue ? 1 : 0);	// digital value

			else AddLong(d);			// analog value
			}

		if( b & 0x20 ) {				// Units Text = 6 spaces
			AddLong(0x20202020);
			AddWord(0x2020);
			}

		if( b & 0x10 ) AddWord(p->MSDAddr);		// Msd Address

		if( b & 0x08 ) {

			AddTextPlusNull(PCTXT(&m_pCtx->m_pbNameList[p->NameInx])); /*!!!CHECK!!!*/
			}

		if( b & 0x04 ) AddByte(0);			// Alarm Status

		if( b & 0x02 ) {

			AddByte('?');			// Descriptor = 1 space
			AddByte(0);
			}

		if( b & 0x01 ) {

			if( fLog ) {

				AddByte('O');
				AddByte('N');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				AddByte('O');
				AddByte('F');
				AddByte('F');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				}
			}
		}
	}

void CBSAPExtSMDriver::AddField2(void)
{
	VARINFO *p = m_pVarInfo;

	BYTE b = m_bFSS[2];

	if( b ) {

		if( b & 0x80 ) AddByte(0);
		if( b & 0x40 ) AddByte(0);

		if( b & 0x38 ) {

			PBYTE pName = &m_pCtx->m_pbNameList[p->NameInx];

			PBYTE pItem = new BYTE [strlen((const char *)pName)];

			UINT uSkip  = pName[0] == '@' ? 1 : 0;

			if( b & 0x20 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			uSkip++;

			if( b & 0x10 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			uSkip++;

			if( b & 0x08 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			delete [] pItem;

			pItem = NULL;
			}

		if( b & 0x04) AddWord(0);
		if( b & 0x02) AddWord(0);
		if( b & 0x01) AddWord(m_pCtx->m_uMSDVers);
		}
	}

void CBSAPExtSMDriver::AddField3(void)
{
	VARINFO *p = m_pVarInfo;

	BYTE b = m_bFSS[3];

	if( b ) {

		if( b & 0x80 ) AddWord(0);
		if( b & 0x40 ) AddWord(0);
		if( b & 0x20 ) AddWord(0);
		if( b & 0x10 ) AddWord(0);
		if( b & 0x08 ) AddByte(0);
		if( b & 0x04 ) AddByte(0);

		if( b & 0x02 ) {

			if( p->Logical ) AddByte(p->Data ? 1 : 0);

			else AddLong(p->Data);
			}

		if( b & 0x01 ) AddWord(0);
		}
	}

// Frame Building

void CBSAPExtSMDriver::StartFrame(void)
{
	m_pSend[0]  = DLE;

	m_pSend[1]  = STX;

	m_uTPtr = 2;
	}

void CBSAPExtSMDriver::EndFrame(void)
{
	m_pSend[m_uTPtr++] = DLE;
	m_pSend[m_uTPtr++] = ETX;

	WORD w = DoCRC(m_pSend);

	m_pSend[m_uTPtr++] = LOBYTE(w);
	m_pSend[m_uTPtr++] = HIBYTE(w);
	}

void CBSAPExtSMDriver::AddByte(BYTE bData)
{
	m_pTx[m_uTPtr++] = bData;
	}

void CBSAPExtSMDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CBSAPExtSMDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CBSAPExtSMDriver::AddReal(DWORD dwData)
{
	AddLong(dwData);
	}

void CBSAPExtSMDriver::AddText(PCTXT pText)
{
	for( UINT i = 0; i < strlen(pText); i++ ) {

		AddByte(pText[i]);
		}
	}

void CBSAPExtSMDriver::AddTextPlusNull(PCTXT pText)
{
	AddText(pText);
	AddByte(0);
	}

// Transport Layer

void CBSAPExtSMDriver::MakeSend(void)
{
	PBYTE pTx  = m_pTx;

	m_pSend    = new BYTE [sizeof(m_bTx)];

	MakeMin(m_uTPtr, sizeof(m_bTx) - 10);

	UINT uEnd  = m_uTPtr;	// end of frame

	StartFrame();

	UINT j = 2;

	for( UINT i = 0; i < uEnd; i++, j++ ) {

		BYTE b = pTx[i];

		if( b == DLE ) {

			m_pSend[j++] = DLE;
			}

		m_pSend[j] = b;
		}

	m_uTPtr = j;

	EndFrame();

	SendFrame();
	
	delete [] m_pSend;

	m_pSend = NULL;
	}

UINT CBSAPExtSMDriver::SendFrame(void)
{
//**/	AfxTrace2("\r\nMaster [%2.2x][%2.2x]--", m_pSend[0], m_pSend[1]);
//**/	UINT u = m_uTPtr - 2;
//**/	if( m_uTPtr > 11 ) { 
//**/		for( UINT k = 2; k < 9; k++) AfxTrace1("[%2.2x]", m_pSend[k]);
//**/		AfxTrace0("--"); for( k = 9; k < u; k++ ) AfxTrace1("[%2.2x]", m_pSend[k]);
//**/		}
//**/	else {
//**/		for( UINT k = 2; k < u; k++ ) AfxTrace1("[%2.2x]", m_pSend[k]);
//**/		}
//**/	AfxTrace2("--[%2.2x][%2.2x]", m_pSend[u], m_pSend[u+1]);

	m_pData->ClearRx();

	m_pData->Write((PCBYTE)m_pSend, m_uTPtr, FOREVER);

	return TRUE;
	}

BOOL CBSAPExtSMDriver::RecvFrame(void)
{
	UINT uPtr   = 0;
	UINT uState = 0;
	UINT uTime  = m_pDC->PollTimeout * (m_uTPtr > 11 ? 4 : 1);
	UINT uData;

	BYTE bData;

	BOOL fIsAck = FALSE;

	SetTimer(uTime);

//**/	AfxTrace1("\r\nMASTER %d ", uTime);

	while( GetTimer() ) {

		if( (uData = m_pData->Read(uTime)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);
		
//**/		AfxTrace1("<%2.2x>", bData);

		switch( uState ) {

			case 0:
				if( bData == DLE ) {

					uState = 1;
					}

				break;

			case 1:
				switch( bData ) {

					case STX:
						uState	= 2;
						uPtr	= 0;
//**/				AfxTrace0("...\r\n");
						break;

					case DLE:
						break;	// stay here for next byte

					default:
						uState	= 0;
						break;
					}
				
				break;

			case 2:
				if( bData == DLE ) uState = 3;

				else m_bRx[uPtr++] = bData;

//**/				if( uPtr == 8 ) AfxTrace0("\r\n");

				break;

			case 3:
				m_bRx[uPtr++] = bData;

				uState = (bData == ETX) ? 4 : 2;

				break;

			case 4: // CRC 1

				uState = 5;

				break;

			case 5: // CRC 2

				return TRUE;
			}

		if( uPtr >= sizeof(m_bRx) - 20 ) return FALSE;
		}

	return FALSE;
	}

UINT CBSAPExtSMDriver::Transact(BOOL fWantReply)
{
	MakeSend();

	if( fWantReply ) {

		if( RecvFrame() ) {

			UINT uReturn = 1;

			BYTE b = m_bRx[RXDFC];

			switch( b ) {

				case ACKDOWN:
					return m_fGlobal ? 3 : 2;

				case NRTRETR:
				case NRTREQ:
				case NAKDATA:
				case ALARMACK:
				case ALARMNOT:
					return b;

				case ACKPOLL:	// ack received, but no data;
					uReturn = 2;
					break;

				case ACKDSCD:	// request discarded
					return 4;
				}

			if( uReturn != 2 ) {

				SendDataAck();
				}

			m_pCtx->m_bSerNum += m_pCtx->m_bSerNum == 0xFF ? 2 : 1;

			return uReturn;
			}
		}

	else {
		Sleep(40);

		if( m_bRx[RXDFC] == ACKDSCD ) {	// request discarded

			return 4;
			}

		return 1;
		}

	return 0;
	}

// Handle Alarms
UINT CBSAPExtSMDriver::CheckAlarms(CAddress Addr, PDWORD pData, UINT uCount) {

	BYTE bType  = 0;

	DWORD dData = *pData;

	BOOL fCfg   = !(BOOL)dData || dData > 9999;

	switch( Addr.a.m_Table ) {

		case SPREALACK:

			if( fCfg ) {

				m_pCtx->m_dAutoAckReal = dData;
				return 0;
				}

			bType = 2;
			break;

		case SPBOOLACK:

			if( fCfg ) {

				m_pCtx->m_dAutoAckBool = dData;
				return 0;
				}

			break;

		default:
			return NOTHING;
		}

	return SendAlarmAck(pData, uCount, bType);
	}

UINT CBSAPExtSMDriver::SendAlarmAck(PDWORD pData, UINT uCount, BYTE bType) {

	PWORD pMSD = new WORD [uCount];

	UINT uRtn  = 0;

	PDWORD pA  = m_pCtx->m_pdAddrList;
	PBYTE  pT  = m_pCtx->m_pbTypeList;

	for( UINT n = 0; n < uCount; n++ ) {

		WORD wItem = LOWORD(pData[n]);

		UINT k = 0;

		while( k < m_pCtx->m_uTagCount ) {

			if( wItem == LOWORD(pA[k]) && bType == pT[k] ) {

				UINT m = m_pCtx->m_pwMSDList[k];

				if( m_pCtx->m_pfMSDList[k] ) {

					pMSD[uRtn++] = m;
					}

				break;
				}

			k++;
			}
		}

	if( MakeAlarmAckFrame(OFCALACK, OFCALACK, pMSD, uRtn) ) {

		Transact(TRUE);
		}

	delete [] pMSD;

	return uRtn;
	}

BOOL CBSAPExtSMDriver::MakeAlarmAckFrame(BYTE bSFC, BYTE bDFC, PWORD pMSD, UINT uCount) {

	if( (BOOL)uCount ) {

		MakeBaseHeader(bSFC, bDFC, TRUE);

		AddWord(m_pCtx->m_uMSDVers);

		AddByte(uCount);

		for( UINT n = 0; n < uCount; n++ ) {

			AddWord(pMSD[n]);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CBSAPExtSMDriver::AlarmInit(void) {

	m_pTx	= m_bAlm;

	m_uTPtr	= 0;

	AddByte(m_pCtx->m_bLocal);
	AddByte(m_pCtx->m_bSerNum);
	AddByte(OFCALMIN);
	AddByte(LOBYTE(m_pCtx->m_uAppSeq));
	AddByte(HIBYTE(m_pCtx->m_uAppSeq));
	AddByte(OFCALMIN);
	AddByte(0);

	return Transact(TRUE) ? 1 : 0;
	}

void CBSAPExtSMDriver::SendAlarmReportAck(void) {

	if( m_pCtx->m_dAutoAckBool > 9999 || m_pCtx->m_dAutoAckReal > 9999 ) {

		UINT uCount = m_bRx[9];

		MakeMin(uCount, 64);

		PWORD pAlm  = new WORD [uCount];

		PWORD pwDat = new WORD [uCount];

		memcpy(pAlm, &m_bRx[10], uCount * sizeof(WORD));

		for( UINT k = 0; k < uCount; k++ ) {

			pAlm[k] = (WORD)IntelToHost((WORD)pwDat[k]);
			}

		delete [] pwDat;

		if( MakeAlarmAckFrame(m_bRx[5], m_bRx[2], pAlm, uCount) ) {

			Transact(TRUE);
			}

		delete [] pAlm;
		}
	}

void CBSAPExtSMDriver::MakeThisGlobal(BOOL fDefault) {

	NRTFRAME *pM	= &m_pDC->NMast;
	NRTFRAME *pT	= &m_pDC->NThis;

	UINT uLevel	= m_pDC->NetLevel;

	UINT uLSC	= pT->LSC[uLevel];	// this level's shift count

	WORD wAddr	= m_pDC->ThisLocal << uLSC;

	WORD wUpDn	= m_pDC->LevelBitMask[uLevel] << uLSC;

	pT->GlobAddrLo	= wAddr | fDefault ? 0 : pM->GlobAddrLo;
	pT->GlobAddrHi	= wAddr | fDefault ? 0 : pM->GlobAddrHi;

	pT->UpDnMaskLo	= wUpDn | fDefault ? 0 : pM->UpDnMaskLo;
	pT->UpDnMaskHi	= wUpDn | fDefault ? 0 : pM->UpDnMaskHi;
	}

void CBSAPExtSMDriver::BuildTimeSync(BOOL fForce) {

	DWORD t = m_pExtra->GetNow();

	BYTE bS = GetSec(t);

	m_bTimeSync++;

	if( !fForce ) {

		if( m_bTimeSync < 60 ) {

			return;
			}
		}

	m_bTimeSync = 60;

	TIMESYNCFRAME * p = &m_pDC->NThis.TSF;

	p->Day		= LOBYTE(GetDate(t));
	p->Month	= LOBYTE(GetMonth(t));
	WORD w		= LOWORD(GetYear(t));
	p->YearLo	= LOBYTE(w);
	p->YearHi	= HIBYTE(w);

	BYTE bItem	= LOBYTE(GetHour(t));
	p->Hour		= bItem;

	WORD wJulian	= (WORD)bItem * 900;	// number of 4 second intervals at top of hour

	bItem		= LOBYTE(GetMin(t));
	p->Minute	= bItem;
	wJulian		+= (WORD)bItem * 15;	// number of 4 second intervals to this minute of the hour

	p->Second	= bS;
	wJulian		+= bS >> 2;		// number of 4 second intervals to this second

	w		= LOWORD(GetDays(t + 631152000)); // 0 is 1 Jan 1997, BSAP 0 is 1 Jan 1977
	p->JulianDayLo	= LOBYTE(w);
	p->JulianDayLo	= HIBYTE(w);
	p->Julian4SecLo	= LOBYTE(wJulian);
	p->Julian4SecLo	= HIBYTE(wJulian);
	p->Julian20mSec	= 0;
	}

BOOL CBSAPExtSMDriver::BuildNRT(void) {

	BYTE bLevel = m_pDC->NetLevel;

	if( !bLevel ) {			// This is System Master

		BuildLSC();

		NRTFRAME *pMast = &m_pDC->NMast;

		pMast->Version		= 1;
		pMast->GlobAddrLo	= 0;
		pMast->GlobAddrHi	= 0;
		pMast->UpDnMaskLo	= 0;
		pMast->UpDnMaskHi	= 0;
		pMast->Level	= 0;

		for( UINT k = 0; k < 7; k++ ) {

			pMast->LSC[k] = m_pDC->NMast.LSC[k];
			pMast->LBM[k] = m_pDC->NMast.LBM[k];
			}
		}

	UINT uLSC = m_pDC->NThis.LSC[bLevel];

	NRTFRAME *pSrce	= &m_pDC->NThis;
	NRTFRAME *pDest	= &m_pDC->NThat;

	if( pSrce->Version ) {						// have received an NRT

		memcpy(&m_pDC->NThat.TSF, &m_pDC->NThis.TSF, 12);	// copy TMS Data

		m_pDC->NThat.Version	= m_pDC->NThis.Version;
		m_pDC->NThat.Level	= bLevel;

		UINT uLevShiftCt	= m_pDC->NThis.LSC[bLevel];

		WORD wNRTItem		= *(PWORD)&pSrce->GlobAddrLo | (m_pCtx->m_bLocal << uLevShiftCt);

		m_pDC->NThat.GlobAddrHi	= HIBYTE(wNRTItem);
		m_pDC->NThat.GlobAddrLo	= LOBYTE(wNRTItem);

		wNRTItem		= m_pDC->NThis.LBM[bLevel] << uLevShiftCt;

		wNRTItem		|= *(PWORD)&pSrce->UpDnMaskLo;

		m_pDC->NThat.UpDnMaskHi	= HIBYTE(wNRTItem);
		m_pDC->NThat.UpDnMaskLo = LOBYTE(wNRTItem);

		memcpy(&m_pDC->NThat.LBM, &m_pDC->NThis.LBM, 7);
		memcpy(&m_pDC->NThat.LSC, &m_pDC->NThis.LSC, 7);

		return TRUE;
		}

	return FALSE;
	}

void CBSAPExtSMDriver::MakeThisNRT(void) {

	NRTFRAME *pMast = &m_pDC->NMast;
	NRTFRAME *pThis = &m_pDC->NThis;

	MakeThisGlobal(FALSE);

	pThis->Version	= pMast->Version + 1;

	for( UINT k = 0; k < 7; k++ ) {

		pThis->LSC[k] = pMast->LSC[k];
		pThis->LBM[k] = pMast->LSC[k];
		}
	}

void CBSAPExtSMDriver::BuildLSC(void) {

	NRTFRAME *p = &m_pDC->NMast;			// default LSC's if no NRT request received

	UINT n      = 6;

	BYTE bMask  = 0;

	BYTE bShift = 0;

	while( n ) {

		bShift   += bMask;

		bMask     = m_pDC->LevelBitMask[n];	// max number of devices on given level

		p->LBM[n] = bMask;			// save mask

		p->LSC[n] = bShift;			// level's shift count = sum of all previous shift counts

		bMask     = GetNRTLSC(bMask);	// shift count = number of bits in the mask

		n--;
		}

	p->LSC[0] = p->LSC[1];				// level 0 = level1
	p->LBM[0] = p->LBM[1];
	}

BYTE CBSAPExtSMDriver::GetNRTLSC(BYTE bLevel) {

	switch( bLevel ) {

		case 1:		return 1;
		case 3:		return 2;
		case 7:		return 3;
		case 15:	return 4;
		case 31:	return 5;
		case 63:	return 6;
		case 127:	return 7;
		}

	return 0;
	}

void CBSAPExtSMDriver::SendNRT(void) {

	m_pTx	= m_bNRT;

	m_uTPtr	= 0;

	AddByte(m_pCtx->m_bLocal);
	AddByte(m_pCtx->m_bSerNum);
	AddByte(MFCNRTMSG);
	AddByte(LOBYTE(m_pCtx->m_uAppSeq));
	AddByte(HIBYTE(m_pCtx->m_uAppSeq));
	AddByte(MFCNRTMSG);
	AddByte(0);

	memcpy(&m_pTx[7], &m_BDrvCfg.NThat, 32);

	Transact(FALSE);

	m_pDC->NRTSent = TRUE;
	}

// Handle NRT
CCODE CBSAPExtSMDriver::HandleNRT(AREF Addr, PDWORD pData, UINT uCount) {

	if( m_BDrvCfg.NetLevel ) {	// TMS/NRT values transferred in from HMI Slave from Local Master

		PDWORD p = (PDWORD)&m_BDrvCfg.NThis.TSF;

		UINT uStart = LOBYTE(Addr.a.m_Offset);

		p += uStart;

		for( UINT i = 0; i < uCount; i++ ) {

			*p++ = pData[i];

			m_BDrvCfg.NRTCount |= (1 << uStart);
			}

		if( m_BDrvCfg.NRTCount == 0xFF ) {	// all NRT received

			m_BDrvCfg.NRTRcvd = TRUE;

			m_BDrvCfg.NRTSent = FALSE;
			}
		}

	return uCount;
	}

// RER
void CBSAPExtSMDriver::SetRER(BYTE bRER)
{
	BYTE b = m_bTx[TXRERPOS];

	m_bTx[TXRERPOS] = b ? RERMORE : bRER;
	}

// Helpers

void CBSAPExtSMDriver::InitGlobal(BOOL fInit)
{
	if( fInit ) {

		m_uState	= 0;

		m_fGlobal	= FALSE;

		m_pVarInfo	= &SVarInfo;

		m_pRFrame	= &m_RFrame;
		m_pTx		= m_bPoll;

		m_bTimeSync	= 0;

		memset(&m_RFrame, 0, sizeof(m_RFrame));

		for( UINT n = 0; n < 15; n++ ) {

			ContextList[n]	= NULL;
			DeviceList[n]	= NULL;
			}
		}

	else {
		InitDevGlobal();
		}
	}

void CBSAPExtSMDriver::InitDevGlobal(void) {

	m_pCtx->m_uTagCount	= 0;

	m_pCtx->m_bSerNum	= 1;

	m_pCtx->m_uAppSeq	= 1;

	m_pCtx->m_uMSDVers	= m_Ident;

	m_pCtx->m_pPend		= &m_pCtx->m_Pend;

	m_pCtx->m_dAutoAckReal	= 0;

	m_pCtx->m_dAutoAckBool	= 0;

	m_pCtx->m_dCfgMagic	= 0;

	if( (BOOL)(m_pCtx->m_dPendMagic != 0x12345678) ) {

		PPEND *p = m_pCtx->m_pPend;

		p->uPendPtrCt = 0;

		p->uListCt    = 4;

		p->ppPendList = (PENDING **) new BYTE [4 * sizeof(PENDING *)];

		p->pCurrent   = NULL;

		for( UINT k = 0; k < 4; k++ ) {

			p->ppPendList[k] = (PENDING *) new BYTE [PENDSIZE];

			memset(p->ppPendList[k], 0, PENDSIZE);
			}

		m_pCtx->m_dPendMagic   = 0x12345678;
		}
	}

BOOL CBSAPExtSMDriver::CheckDrop(BYTE bData)
{
	if( BYTE(bData & 0x7F) == m_bDrop ) {

		m_fGlobal = bData & 0x80;

		return TRUE;
		}

	return FALSE;
	}

UINT CBSAPExtSMDriver::IsNRT(UINT uTable) {

	if( uTable >= SPNRT0 && uTable <= SPNRT6 ) {

		return 1 + uTable - SPNRT0;
		}

	return 0;
	}

UINT CBSAPExtSMDriver::IsBIT(UINT uTable) {

	if( uTable >= SPBIT0 && uTable <= SPBIT6 ) {

		return 1 + uTable - SPBIT0;
		}

	return 0;
	}

UINT CBSAPExtSMDriver::IsFLT(UINT uTable) {

	if( uTable >= SPNRT0 && uTable <= SPREAL6 ) {

		return 1 + uTable - SPREAL0;
		}

	return 0;
	}

UINT CBSAPExtSMDriver::IsSTR(UINT uTable) {

	if( uTable >= SPSTRING0 && uTable <= SPSTRING6 ) {

		return 1 + uTable - SPSTRING0;
		}

	return 0;
	}

BOOL CBSAPExtSMDriver::IsSTRText(UINT uChar)
{
	return (isalpha(uChar) || isdigit(uChar) || ispunct(uChar) || isspace(uChar));
	}

UINT CBSAPExtSMDriver::SearchValue(UINT uValue, UINT uEnd, PDWORD pL, PWORD pW, UINT *pFound) {

	BOOL fShort = pL == NULL;

	UINT uBeg   = 0;

	while( uEnd > uBeg ) {

		UINT uMid  = (uEnd + uBeg) / 2;

		UINT uTest = fShort ? (UINT)pW[uMid] : (UINT)pL[uMid];

		*pFound = uMid;

		if( uValue < uTest ) {

			if( !uMid || (uValue > (fShort ? (UINT)pW[uMid-1] : (UINT)pL[uMid-1])) ) {

				return 2;	// enable insert item at this location
				}

			uEnd = uMid;

			continue;
			}

		if( uValue == uTest ) {

			return 1;	// found match
			}

		uBeg = uMid + 1;
		}

	return NOTHING;
	}

void CBSAPExtSMDriver::AddContextPtr(CContext *pPtr, IDevice *pDev) {

	UINT n = 0;

	while( n < 15 ) {

		if( ContextList[n] == NULL ) {

			ContextList[n]	= pPtr;
			DeviceList[n]	= pDev;

			return;
			}

		n++;
		}
	}

UINT CBSAPExtSMDriver::GetCount(CAddress Addr, UINT uCount)
{
	if( IsSTR(Addr.a.m_Table) ) {

		uCount /= MAX_LTEXT;

		if( Addr.a.m_Offset % MAX_LTEXT ) {

			uCount++;
			}

		return uCount;
		}

	return uCount;
	}

UINT CBSAPExtSMDriver::ExpandCount(CAddress Addr, UINT uCount, UINT uMax)
{
	if( IsSTR(Addr.a.m_Table) ) {

		uCount *= MAX_LTEXT;

		if( uMax ) {

			return min(uCount, uMax);
			}

		return uCount;
		}

	return uCount;
	}

// Tag Data Loading
void CBSAPExtSMDriver::LoadTags(LPCBYTE &pData)
{
	if( StoreDevName(pData) ) {

		m_pCtx->m_dCfgMagic = 0x12345678;

		UINT uCount = GetWord(pData); // number of Bit/Real names

		UINT i = 0;

		if( uCount ) {

			m_pCtx->m_uTagCount  = uCount;

			m_pCtx->m_pdAddrList = new DWORD [uCount + 1];

			m_pCtx->m_pwMSDList  = new WORD  [uCount + 1];

			m_pCtx->m_pfMSDList  = new BOOL  [uCount + 1];

//			m_pCtx->m_pdLNumList = new BYTE  [uCount + 1];	// list number not used in master

			m_pCtx->m_pbTypeList = new BYTE  [uCount + 1];

			m_pCtx->m_pwGlobList = new WORD  [uCount + 1];

			BYTE b = 0;
			WORD w = 0;

//****/AfxTrace0("\r\n\nAddr: "); AfxTrace1("-- %d ", m_pCtx->m_bLocal);

			m_pCtx->m_uStrings = 0;

			PDWORD pd = m_pCtx->m_pdAddrList;

			for( i = 0; i < uCount; i++ ) {		// reference address

				pd[i] = GetLong(pData);

				if( IsSTR(AREF(pd[i]).a.m_Table) ) {

					m_pCtx->m_uStrings++;
					}
//****/AfxTrace2("[%d %8.8lx]", i, pd[i]);
				}
			pd[uCount] = 0xFFFFFFFF;

//****/AfxTrace0("\r\nMSD: ");
			PWORD pw = m_pCtx->m_pwMSDList;
			for( i = 0; i < uCount; i++ ) {		// MSD address

				m_pCtx->m_pfMSDList[i] = FALSE;

				pw[i] = GetWord(pData);
//****/AfxTrace2("[%d %d]", i, pw[i]);
				}
			pw[uCount] = 0xFFFF;

//****/AfxTrace0("\r\nList: ");
			w = GetWord(pData);		// count of items assigned to lists (not used in master)
//****/AfxTrace1("%d ", w);
			for( i = 0; i < w; i++ ) {	// List Number (sent here, but not used

				DWORD d = GetLong(pData);

//				m_pCtx->m_pdLNumList[i] = b;
//****/AfxTrace2("[%d %8.8x]", i, d);
				}
//****/AfxTrace0("\r\nType: ");
			PBYTE pb = m_pCtx->m_pbTypeList;
			for( i = 0; i < uCount; i++ ) {	// Type

				pb[i] = GetByte(pData);
//****/AfxTrace2("[%d %2.2x]", i, pb[i]);
				}

			pb[uCount] = 0xFF;
//****/AfxTrace0("\r\nGlob: ");
			pw = m_pCtx->m_pwGlobList;
			for( i = 0; i < uCount; i++ ) {	// Global Address of device

				pw[i] = GetWord(pData);
//****/AfxTrace2("[%d %d]", i, pw[i]);
				}

			pw[uCount] = 0xFFFF;

			m_pCtx->m_pbDevAddList = m_pCtx->m_bDevAddList;
			m_pCtx->m_pbLevAddList = m_pCtx->m_bLevAddList;
//****/AfxTrace2("\r\nDevLevAdd %8.8lx %8.8lx ", m_pCtx->m_pbDevAddList, m_pCtx->m_pbLevAddList);
			for( i = 0; i < 15; i++ ) {

				w = GetWord(pData);
//****/AfxTrace2("[%x %x]", LOBYTE(w), HIBYTE(w));
				m_pCtx->m_pbDevAddList[i] = LOBYTE(w);
				m_pCtx->m_pbLevAddList[i] = HIBYTE(w);
				}

//****/AfxTrace0("\r\nArray Data not used in Master");
			w = GetWord(pData);

			UINT uTotalSize	= GetWord(pData); // adding nulls
//****/AfxTrace1("\r\nName Total=%d ", uTotalSize);
			m_pCtx->m_pwNameIndex	= new WORD  [uCount + 1];
			m_pCtx->m_pbNameList	= new BYTE  [uTotalSize + 1];

			UINT uPos	= 0;

			for( i = 0; i < uCount; i++ ) {

				StoreName( pData, &uPos, i );
				}

			m_pCtx->m_pwNameIndex[uCount] = uTotalSize; // end marker

			UINT uWords = uCount * sizeof(WORD);

			memset(m_pCtx->m_pwGlobList, 0, uWords);

			m_pCtx->m_pwGlobList[uCount] = 0xFFFF;
			}
		}
	}

void CBSAPExtSMDriver::InitStringSupport(LPCBYTE &pData)
{
	m_pCtx->m_pStrings = new CStringTag[m_pCtx->m_uStrings];

	for( UINT u = 0; u < m_pCtx->m_uStrings; u++ ) {

		memset(m_pCtx->m_pStrings[u].m_Text, 0, MAX_CHARS);
		}
	}

BOOL CBSAPExtSMDriver::StoreDevName(LPCBYTE &pData)
{
	UINT uCount = GetWord(pData); // size of device name

	if( uCount ) {

		m_pCtx->m_pbDevName = new BYTE[uCount];

		PBYTE p = m_pCtx->m_pbDevName;

		memset(p, 0, uCount);

		for( UINT i = 0; i < uCount; i++ ) {

			WORD w = GetWord(pData);

			p[i] = LOBYTE(w);

			}

		return TRUE;
		}

	return FALSE;
	}

void CBSAPExtSMDriver::StoreName(LPCBYTE &pData, UINT *pPosition, UINT uItem)
{
	UINT uByteCount	= GetWord(pData);

	UINT uPos	= *pPosition;

	*pPosition	+= uByteCount;

	m_pCtx->m_pwNameIndex[uItem] = uPos;

	PBYTE p		= &m_pCtx->m_pbNameList[uPos];

//****/AfxTrace3("\r\nInx=%d BC=%d Pos=%d ", uItem, uByteCount, uPos );

	for( UINT i = 0; i < uByteCount; i++ ) {

		BYTE b = LOBYTE(GetWord(pData));

//****/if(b) AfxTrace1("%c", b); else AfxTrace0(" N");

		p[i] = b;
		}
	}

// Delete created buffers
void CBSAPExtSMDriver::CloseDown(CContext *pPtr)
{
	if( pPtr ) {

		if( pPtr->m_dCfgMagic == 0x12345678 ) {

//			AfxTrace1("CLOSE DOWN %d ", pPtr->m_bLocal);  

			if( pPtr->m_pbDevName    ) { delete [] pPtr->m_pbDevName;		pPtr->m_pbDevName = NULL; }

			if( pPtr->m_pdAddrList   ) { delete [] pPtr->m_pdAddrList;		pPtr->m_pdAddrList = NULL;   }

			if( pPtr->m_pbNameList   ) { delete [] pPtr->m_pbNameList;		pPtr->m_pbNameList = NULL;   }

			if( pPtr->m_pwNameIndex  ) { delete [] pPtr->m_pwNameIndex;		pPtr->m_pwNameIndex = NULL;  }

			if( pPtr->m_pwMSDList    ) { delete [] pPtr->m_pwMSDList;		pPtr->m_pwMSDList = NULL;    }

			if( pPtr->m_pfMSDList    ) { delete [] pPtr->m_pfMSDList;		pPtr->m_pfMSDList = NULL;    }

			if( pPtr->m_pbTypeList   ) { delete [] pPtr->m_pbTypeList;		pPtr->m_pbTypeList = NULL;   }

			if( pPtr->m_pwGlobList   ) { delete [] pPtr->m_pwGlobList;		pPtr->m_pwGlobList = NULL; }

			if( pPtr->m_pStrings     ) { delete [] pPtr->m_pStrings;		pPtr->m_pStrings   = NULL; }
			}

		pPtr->m_dCfgMagic = 0;

		PendDeAlloc(pPtr);
		}
	}

void CBSAPExtSMDriver::PendDeAlloc(CContext *pPtr) {

	PPEND   *p1 = pPtr->m_pPend;

	if( p1 != NULL ) {

		UINT u = p1->uListCt;

		PENDING **p = p1->ppPendList;

		for( UINT i = 0; i < u; i++ ) {

			if( p[i] ) {

				delete p[i];

				p[i] = NULL;
				}
			}

		delete p1->ppPendList;

		p1->ppPendList = NULL;
		}
	}

// Test code for Arrays
BOOL CBSAPExtSMDriver::DoArrayTest(CAddress Addr, PDWORD pData, UINT uCount, BOOL fIsRead) {

	BOOL fBit = Addr.a.m_Type == addrBitAsBit && Addr.a.m_Offset == 16;

	if( Addr.a.m_Type == addrRealAsReal ) {

		switch( Addr.a.m_Offset ) {

			case 5: break;

			default: return FALSE;
			}
		}

	else {
		if( !fBit ) return FALSE;
		}

	m_uIndex = SetItemIndex(Addr);

	m_pTx = m_bTx;

	BOOL f  = (Addr.a.m_Offset == 4) || (Addr.a.m_Offset == 16);
	BOOL fb = Addr.a.m_Table >= SPBIT0 && Addr.a.m_Table <= SPBIT6;

	if( fIsRead ) {

		MakeReadHeader();
		AddByte(f ? OFCRARRS : OFCRARRG);
		}

	else {
		AddWriteHeader();
		AddByte(f ? OFCWARRS : OFCWARRG);
		}

	AddByte(fb ? 1 : 0);	// select analog
	AddByte(fb ? 2 : 1);	// array number

	if( f ) {
		AddByte(0);	// start row
		AddByte(0);	// start column
		}

	else {
		AddWord(1);
		AddWord(1);
		}

	AddByte(uCount);

	UINT i;

	if( !fIsRead ) {

		for( i = 0; i < uCount; i++ ) {

			AddLong(pData[i]);
			}
		}

	Transact(TRUE);

	if( fIsRead ) {

		UINT uPos = f ? 12 : 14;

		if( !fb ) {

			for( i = 0; i < uCount; i++, uPos += 4 ) {

				DWORD x  = (DWORD)IntelToHost(*(PU4)&m_bRx[uPos]);
				pData[i] = x;
				}
			}

		else {
			i = 0;

			BYTE bMask = 0x80;

			BYTE b = m_bRx[uPos++];

			while( i < uCount ) {

				pData[i] = b & bMask ? 0xFFFFFFFF : 0;

				bMask >>= 1;

				if( !bMask ) {

					bMask = 0x80;
					b     = m_bRx[uPos++];
					}

				i++;
				}
			}
		}

	return uCount;
	}

// End of File
