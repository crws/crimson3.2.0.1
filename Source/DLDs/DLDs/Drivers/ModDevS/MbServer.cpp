
#include "intern.hpp"

#include "MbServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Data Server
//

// Constructor

CModbusServer::CModbusServer(void)
{
	m_pDriver = NULL;

	m_pHead   = NULL;

	m_pTail   = NULL;
	}

// Destructor

CModbusServer::~CModbusServer(void)
{
	CBlock *pBlock;

	while( (pBlock = m_pHead) ) {

		AfxListRemove(m_pHead, m_pTail, pBlock, m_pNext, m_pPrev);
		
		delete pBlock;
		}
	}

// Master Access

UINT CModbusServer::MasterRead(AREF Addr, PDWORD pData, UINT uCount)
{
	return Transfer(Addr, pData, uCount, ioMaster | ioGet);
	}

UINT CModbusServer::MasterWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	return Transfer(Addr, pData, uCount, ioMaster | ioPut);
	}

// Slave Access

UINT CModbusServer::SlaveRead(AREF Addr, PDWORD pData, UINT uCount)
{
	return Transfer(Addr, pData, uCount, ioSlave | ioGet);
	}

UINT CModbusServer::SlaveWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	return Transfer(Addr, pData, uCount, ioSlave | ioPut);
	}

// Implementation

UINT CModbusServer::Transfer(AREF Addr, PDWORD pData, UINT uCount, UINT uMode)
{
	if( uMode & ioSlave ) {

		UINT c = 0;

		CBlock *pBlock = m_pHead;

		while( pBlock ) {

			if( pBlock->FindAddress(Addr, uCount) ) {

				c += Transfer(pBlock, Addr, pData, uCount, uMode);
				}

			pBlock = pBlock->m_pNext;
			}

		return c;
		}

	if( uMode & ioMaster ) {
		
		CBlock *pBlock = m_pHead;

		while( pBlock ) {

			if( pBlock->AcceptAddress(Addr, uCount) ) {

				break;
				}

			pBlock = pBlock->m_pNext;
			}

		if( !pBlock ) {

			pBlock = New CBlock(this, Addr, uCount);

			AfxListAppend(m_pHead, m_pTail, pBlock, m_pNext, m_pPrev);
			}

		Transfer(pBlock, Addr, pData, uCount, uMode);

		return uCount;
		}

	return 0;
	}

UINT CModbusServer::Transfer(CBlock *pBlock, AREF Addr, PDWORD pData, UINT uCount, UINT uMode)
{
	UINT    Scale = pBlock->m_uFactor;

	//
	UINT ReqStart = Addr.a.m_Offset;

	UINT   ReqEnd = Addr.a.m_Offset + uCount * Scale;

	//
	UINT BlkStart = pBlock->m_Offset;

	UINT BlkCount = pBlock->m_uCount;

	UINT   BlkEnd = pBlock->m_Offset + BlkCount * Scale;

	//	
	UINT s = Max(ReqStart, BlkStart);
				
	UINT e = Min(ReqEnd, BlkEnd);
	//

	UINT ad = abs(INT((ReqStart - BlkStart) % Scale));

	UINT bo = (s - BlkStart) / Scale;

	UINT ro = (s - ReqStart) / Scale;

	UINT bc = (e - s) / Scale;

	return pBlock->Transfer( bo + ad, 
				 pData + ro, 
				 bc, 
				 uMode
				);
	}

// Debug

void CModbusServer::BindDriver(CMasterDriver *pDriver)
{
	m_pDriver = pDriver;
	}

PCTXT CModbusServer::GetTypeName(UINT uType)
{
	PCTXT pType[] = {
		
		"BitAsBit",	//addrBitAsBit	= 0x00,
		"BitAsByte",	//addrBitAsByte	= 0x01,
		"BitAsWord",	//addrBitAsWord	= 0x02,
		"BitAsLong",	//addrBitAsLong	= 0x03,
		"BitAsReal",	//addrBitAsReal	= 0x04,
		"ByteAsByte",	//addrByteAsByte	= 0x05,
		"ByteAsWord",	//addrByteAsWord	= 0x06,
		"ByteAsLong",	//addrByteAsLong	= 0x07,
		"ByteAsReal",	//addrByteAsReal	= 0x08,
		"WordAsWord",	//addrWordAsWord	= 0x09,
		"WordAsLong",	//addrWordAsLong	= 0x0A,
		"WordAsReal",	//addrWordAsReal	= 0x0B,
		"LongAsLong",	//addrLongAsLong	= 0x0C,
		"LongAsReal",	//addrLongAsReal	= 0x0D,
		"RealAsReal",	//addrRealAsReal	= 0x0E,
		"Reserved",	//addrReserved	= 0x0F,
		};

	return pType[uType];
	}

void CModbusServer::Dump(CBlock *pBlock)
{
	AfxTrace("block, %d size %d\t%s\n", 
		 pBlock->m_Offset, 
		 pBlock->m_uCount,
		 GetTypeName(pBlock->m_Type)
		 );

	Dump(pBlock->m_pData, pBlock->m_uCount);
	}

void CModbusServer::Dump(PDWORD pData, UINT uCount)
{
	if( pData ) {

		for( UINT n = 0; n < uCount; n++ ) {

			if( n && !(n%8) ) {
			
				AfxTrace("\n");
				}

			AfxTrace("%8.8X ", pData[n]);
			}

		AfxTrace("\n");
		}
	}

void CModbusServer::DumpBlocks(void)
{
	AfxTrace("blocks\n");

	UINT n = 0;

	CBlock *pBlock = m_pHead;

	while( pBlock ) {

		AfxTrace(" %d\t", n);

		AfxTrace("table %d, offset %d count %d %s\n", 
			 pBlock->m_Table, 
			 pBlock->m_Offset, 
			 pBlock->m_uCount,
			 GetTypeName(pBlock->m_Type)
			 );

		pBlock = pBlock->m_pNext;
		
		n ++;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Block
//

#define Trace	m_pDriver->AfxTrace

// Constructor

CBlock::CBlock(void)
{
	m_pServer = NULL;

	m_Table   = 0;

	m_Offset  = 0;

	m_Type    = 0;

	m_Extra   = 0;

	m_uFactor = GetTypeScale(m_Type);

	m_uCount  = 0;

	m_pData	  = NULL;

	//
	m_pNext  = NULL;

	m_pPrev	 = NULL;
	}

CBlock::CBlock(CModbusServer *pServer, AREF Addr, UINT uCount)
{
	m_pServer = pServer;

	//
	m_Table   = Addr.a.m_Table;

	m_Offset  = Addr.a.m_Offset;

	m_Type    = Addr.a.m_Type;

	m_Extra   = Addr.a.m_Extra;

	m_pData   = NULL;

	m_uCount   = 0;

	//
	m_uFactor = GetTypeScale(m_Type);

	Alloc(uCount);

	//
	m_pNext	= NULL;

	m_pPrev	= NULL;	
	}

// Destructor

CBlock::~CBlock(void)
{
	delete m_pData;
	}

// Attributes

BOOL CBlock::FindAddress(AREF Addr, UINT uCount)
{
	if( Addr.a.m_Table == m_Table ) {
		
		if( Addr.a.m_Type == m_Type) {

			UINT Scale = m_uFactor;

			//
			UINT ReqStart = Addr.a.m_Offset;

			UINT ReqEnd   = Addr.a.m_Offset + uCount * Scale;

			//
			UINT BlkStart  = m_Offset;

			UINT BlkEnd    = m_Offset + m_uCount * Scale;

			if( ReqEnd > BlkStart ) {

				if( ReqStart < BlkEnd ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CBlock::AcceptAddress(AREF Addr, UINT uCount)
{
	if( Addr.a.m_Table == m_Table ) {
		
		if( Addr.a.m_Type == m_Type) {

			UINT Scale = m_uFactor;

			//
			UINT ReqStart = Addr.a.m_Offset;

			UINT ReqEnd   = Addr.a.m_Offset + uCount * Scale;

			//
			UINT BlkStart  = m_Offset;

			UINT BlkEnd    = m_Offset + m_uCount * Scale;

			if( ReqEnd > BlkStart ) {

				if( ReqStart < BlkEnd ) {

					if( ReqEnd > BlkEnd ) {

						UINT uAlloc = m_uCount + ReqEnd - BlkEnd;

						Alloc(uAlloc);
						}

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Operations

UINT CBlock::Transfer(UINT uIndex, PDWORD pData, UINT uCount, UINT uMode)
{
	if( uMode & CModbusServer::ioGet ) {

		if( pData ) {

			memcpy( pData, 
				m_pData + uIndex, 
				uCount * sizeof(DWORD)
				);
			}
		
		return uCount;
		}

	if( uMode & CModbusServer::ioPut ) {

		memcpy( m_pData + uIndex, 
			pData, 
			uCount * sizeof(DWORD)
			);

		return uCount;
		}

	return 0;
	}

// Implementaiotn

void CBlock::Alloc(UINT uAlloc)
{
	PDWORD pData = New DWORD [ uAlloc ];	

	memset(pData, 0, sizeof(DWORD) * uAlloc);

	if( m_pData ) {		

		memcpy(pData, m_pData, m_uCount * sizeof(DWORD));

		delete m_pData;
		}

	m_pData  = pData;

	m_uCount = uAlloc;								
	}

UINT CBlock::GetTypeScale(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrByteAsByte:
		case addrWordAsWord:
		case addrLongAsLong:
		case addrRealAsReal:
		case addrLongAsReal:

			return 1;

		case addrByteAsWord:
		case addrWordAsLong:
		case addrWordAsReal:

			return 2;
		
		case addrByteAsLong:
		case addrByteAsReal:

			return 4;

		case addrBitAsByte:

			return 8;

		case addrBitAsWord:

			return 16;

		case addrBitAsLong:
		case addrBitAsReal:

			return 32;
		}

	return 0;
	}

// Debug

PCTXT CBlock::GetTypeName(UINT uType)
{
	PCTXT pType[] = {
		
		"BitAsBit",	//addrBitAsBit	= 0x00,
		"BitAsByte",	//addrBitAsByte	= 0x01,
		"BitAsWord",	//addrBitAsWord	= 0x02,
		"BitAsLong",	//addrBitAsLong	= 0x03,
		"BitAsReal",	//addrBitAsReal	= 0x04,
		"ByteAsByte",	//addrByteAsByte	= 0x05,
		"ByteAsWord",	//addrByteAsWord	= 0x06,
		"ByteAsLong",	//addrByteAsLong	= 0x07,
		"ByteAsReal",	//addrByteAsReal	= 0x08,
		"WordAsWord",	//addrWordAsWord	= 0x09,
		"WordAsLong",	//addrWordAsLong	= 0x0A,
		"WordAsReal",	//addrWordAsReal	= 0x0B,
		"LongAsLong",	//addrLongAsLong	= 0x0C,
		"LongAsReal",	//addrLongAsReal	= 0x0D,
		"RealAsReal",	//addrRealAsReal	= 0x0E,
		"Reserved",	//addrReserved	= 0x0F,
		};

	return pType[uType];
	}

#undef Trace

// End of File
