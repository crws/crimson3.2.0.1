
#include "intern.hpp"

#include "MbServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Exception Codes
//

#define	ILLEGAL_FUNCTION	0x01

#define ILLEGAL_ADDRESS		0x02

#define	ILLEGAL_DATA		0x03

//////////////////////////////////////////////////////////////////////////
//
// Modbus Device Server Driver over Ethernet
//
//

class CModbusDeviceServerTCPDriver : public CMasterDriver
{
	public:
		// Constructor
		CModbusDeviceServerTCPDriver(void);

		// Destructor
		~CModbusDeviceServerTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(void ) Service(void);

		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);
		DEFMETH(UINT) DevCtrl(void * pContext, UINT uFunc, PCTXT Value);
			    
	protected:

		struct CContext
		{
			BYTE		m_bDrop;
			BOOL		m_fFlipLong;
			BOOL		m_fFlipReal;
			BOOL		m_fEnable;
			CModbusServer * m_pServer;
			CContext *	m_pNext;
			CContext *	m_pPrev;
			};
		
		struct SOCKET
		{
			ISocket	 * m_pSocket;
			UINT	   m_uPtr;
			PBYTE	   m_pData;
			BOOL	   m_fBusy;
			UINT	   m_uTime;
			};

		// Data Members
		CContext *	m_pCtx;
		SOCKET *	m_pSock;
		UINT		m_uPort;
		UINT		m_uCount;
		UINT		m_uRestrict;
		DWORD		m_SecData;
		DWORD		m_SecMask;
		BOOL		m_fFlipLong;
		BOOL		m_fFlipReal;
		CContext      * m_pHead;
		CContext      * m_pTail;
		CContext      * m_pThis;

	  	// Implementation
		void OpenSocket(UINT n, SOCKET &Sock);
		void CloseSocket(SOCKET &Sock);
		void ReadData(SOCKET &Sock);
		BOOL CheckAccess(SOCKET &Sock, BOOL fWrite);
		BOOL DoFrame(SOCKET &Sock, PBYTE pData, UINT uSize);

		// Opcode Handlers
		BOOL DoStdRead (SOCKET &Sock, PBYTE pData, UINT uSize, UINT uCode);
		BOOL DoStdWrite(SOCKET &Sock, PBYTE pData, UINT uSize, UINT uCode);
		BOOL DoBitRead (SOCKET &Sock, PBYTE pData, UINT uSize, UINT uCode);
		BOOL DoBitWrite(SOCKET &Sock, PBYTE pData, UINT uSize, UINT uCode);
		BOOL DoFuncXX  (SOCKET &Sock, PBYTE pData, UINT uSize, UINT uCode);

		// Helpers
		void	Make16BitSigned(DWORD &dwData);
		BOOL	IsLongReg(UINT uType);
		BOOL	IsReal(UINT uType);
	};

// End of File
