
#ifndef	INCLUDE_MBSERVER_HPP

#define	INCLUDE_MBSERVER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Modbus Data Server
//
// Macros

#if !defined(USING_STDENV)

#define	Min	min

#define	Max	max

#endif

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CBlock;
class CRange;

//////////////////////////////////////////////////////////////////////////
//
// Server
//

class CModbusServer
{
	public:
		// Constructor
		CModbusServer(void);

		// Destructor
		~CModbusServer(void);

		// Master Access
		UINT MasterRead (AREF Addr, PDWORD pData, UINT uCount);
		UINT MasterWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Slave Access
		UINT SlaveRead  (AREF Addr, PDWORD pData, UINT uCount);
		UINT SlaveWrite (AREF Addr, PDWORD pData, UINT uCount);

		// Transfer Mode
		enum {
			ioPut    = ( 1 << 0),
			ioGet    = ( 1 << 1),
			ioMaster = ( 1 << 2),
			ioSlave =  ( 1 << 3),
			};

		// Debug Data
		CMasterDriver *m_pDriver;

		// Debug
		void BindDriver(CMasterDriver *pDriver);

	protected:
		// Data
		CBlock * m_pHead;
		CBlock * m_pTail;

		// Implementation
		UINT Transfer(AREF Addr, PDWORD pData, UINT uCount, UINT uMode);
		UINT Transfer(CBlock *pBlock, AREF Addr, PDWORD pData, UINT uCount, UINT uMode);

		// Debug
		PCTXT GetTypeName(UINT uType);
		void  Dump(CBlock *pBlock);
		void  Dump(PDWORD pData, UINT uCount);
		void  DumpBlocks(void);

		// Friends
		friend class CBlock;
	};

//////////////////////////////////////////////////////////////////////////
//
// Block
//

class CBlock
{
	public:
		// Constructor
		CBlock(CModbusServer *pServer, AREF Addr, UINT uCount);

		// Destructor
		~CBlock(void);

		// Attributes
		BOOL FindAddress(AREF Addr, UINT uCount);
		BOOL AcceptAddress(AREF Addr, UINT uCount);

		// Operations
		UINT Transfer(UINT uIndex, PDWORD pData, UINT uCount, UINT uMode);

		// Public Data
		UINT	m_Table;
		UINT	m_Offset;
		UINT	m_Extra;
		UINT	m_Type;
		PDWORD  m_pData;
		UINT    m_uCount;
		UINT	m_uFactor;

		// Linked List
		CBlock * m_pNext;
		CBlock * m_pPrev;

		// Debug
		CModbusServer * m_pServer;

	protected:
		// Data		

		// Implementation
		void  Alloc(UINT uCount);
		UINT  GetTypeScale(UINT uType);

		// Debug
		PCTXT GetTypeName(UINT uType);

	private:
		// Constructor
		CBlock(void);
	};

// End of File

#endif
