/////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Logix5000 Driver - EIP Master
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABLogix5EIPMaster;

class CIOISegment;

/////////////////////////////////////////////////////////////////////////
//
// CIP Services
//

#define CIP_READ		0x4C

#define CIP_WRITE		0x4D

//////////////////////////////////////////////////////////////////////////
//
// Address Types
//

enum {
	addrString	= 1,
	addrTimer	= 2,
	addrCounter	= 3,
	addrControl	= 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Logix5000 Driver - EIP Master
//

class CABLogix5EIPMaster : public CMasterDriver
{
	public:
		// Constructor
		CABLogix5EIPMaster(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Text Loading
		PTXT GetString(PCBYTE &pData);

	protected:
		// Device Data
		struct CContext
		{
			DWORD	  m_IP;
			WORD	  m_wSlot;
			WORD      m_wTimeout;
			UINT      m_uCount;
			PTXT    * m_pTags;
			};

		// Data Members
		IEthernetIPHelper * m_pEnetHelper;
		IExplicit         * m_pExplicit;
		CContext          * m_pCtx;
		BOOL		    m_fOpen;
		BYTE		    m_bRxBuff[512];
		BYTE		    m_bTxBuff[512];
		UINT		    m_uPtr;
		BYTE		    m_bError;

		// Implementation
		BOOL ReadData(AREF Addr, UINT uCount);
		BOOL WriteData(AREF Addr, PDWORD pData, UINT uCount);
		void LimitLump(AREF Addr, UINT &uCount);

		// CIP Support
		BOOL Send(BYTE bCode, CIOISegment &Segment);
		BOOL Recv(void);
		BOOL AddCIPType(AREF Addr);
		void AddCount  (AREF Addr, UINT uCount);
		void AddBits(PDWORD pData, UINT uCount);

		// Transport Layer
		BOOL MakeLink(void);
		BOOL CheckLink(void);

		PCTXT FindTagName(AREF Addr);

		BOOL IsTable(AREF Addr);
		BOOL IsArray(AREF Addr);
		BOOL IsTriplet(AREF Addr);
		BOOL IsControl(AREF Addr);
		BOOL IsString(AREF Addr);

		// Tag Name Support
		void  LoadTags(LPCBYTE &pData);
		PCTXT GetTagName(UINT uIndex);

		// DF1 Legacy
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddByte(PDWORD pData, UINT uCount);
		void AddWord(PDWORD pData, UINT uCount);
		void AddLong(PDWORD pData, UINT uCount);
		void CopyByte(PBYTE pReply, PDWORD pData, UINT uCount);
		void CopyWord(PBYTE pReply, PDWORD pData, UINT uCount);
		void CopyLong(PBYTE pReply, PDWORD pData, UINT uCount);
	};

//////////////////////////////////////////////////////////////////////////
//
// IOI Segment
//

class CIOISegment
{
	public:
		// Constructor
		CIOISegment(void);
		CIOISegment(PCTXT pText);

		// Operations
		void Append(UINT uIndex);
		void Append(PCTXT pText);
		void Encode(PCTXT pText);
		void Encode(PCTXT pText, UINT uIndex);
		void Encode(PCTXT pText, UINT uIndex, PCTXT pMore);
		void Encode(PCTXT pText, PCTXT pMore);

		// Attributes
		PBYTE	GetData(void);
		UINT	GetSize(void);

	protected:
		// Data
		BYTE	m_Data[64];
		PBYTE	m_pData;
		UINT	m_uSize;

		// Implementation
		void Init(void);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddText(PCTXT pText);
		void AddIOI (CIOISegment &Name);
	};

// End of File
