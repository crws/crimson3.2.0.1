
#include "intern.hpp"

#include "ablgxeip.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Logix5000 Driver - EIP Master
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator 

INSTANTIATE(CABLogix5EIPMaster);

// Constructor

CABLogix5EIPMaster::CABLogix5EIPMaster(void)
{
	m_Ident       = DRIVER_ID;
	
	m_fOpen       = FALSE;
	
	m_pEnetHelper = NULL;

	m_pExplicit   = NULL;

	m_pCtx        = NULL;
	}

// Management

void MCALL CABLogix5EIPMaster::Attach(IPortObject *pPort)
{
	if( !m_pEnetHelper && !m_fOpen ) {
		
		if( MoreHelp(IDH_ENETIP, (void **) &m_pEnetHelper) ) {

			m_fOpen = m_pEnetHelper->Open();
			}
		}
	}

void MCALL CABLogix5EIPMaster::Detach(void)
{
	if( m_pEnetHelper ) {

		m_pEnetHelper->Close();

		m_pEnetHelper->Release();

		m_pEnetHelper = NULL;

		m_pExplicit   = NULL;

		m_fOpen       = FALSE;
		}
	}

// Device

CCODE MCALL CABLogix5EIPMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {
			
			m_pCtx  = new CContext;

					     GetByte(pData);
			m_pCtx->m_IP	   = GetAddr(pData);
			m_pCtx->m_wSlot	   = GetWord(pData);
			m_pCtx->m_wTimeout = GetWord(pData);

			LoadTags(pData);

			pDevice->SetContext(m_pCtx);
			
			return MakeLink() ? CCODE_SUCCESS : CCODE_ERROR;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return MakeLink() ? CCODE_SUCCESS : CCODE_ERROR;
	}

CCODE MCALL CABLogix5EIPMaster::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx->m_pTags;

		m_pCtx->m_pTags = NULL;

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CABLogix5EIPMaster::Ping(void)
{
	return CheckLink() ? CCODE_SUCCESS : CCODE_ERROR;
	}

void MCALL CABLogix5EIPMaster::Service(void)
{
	}

CCODE MCALL CABLogix5EIPMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		LimitLump(Addr, uCount);

		if( ReadData(Addr, uCount) ) {

			UINT uOffset = Addr.a.m_Offset;

			BYTE   bType = m_bRxBuff[0];

			BYTE   bMore = m_bRxBuff[1];

			PBYTE pPlace = &m_bRxBuff[2];

			PBYTE      p = &pPlace[bMore];

			switch( bType ) {

				case 0xC1:
				case 0xC2:
					CopyByte(p, pData, uCount);
					break;

				case 0xC3:
					CopyWord(p, pData, uCount);
					break;

				case 0xC4:
				case 0xCA:
				case 0xD3:
					CopyLong(p, pData, uCount);
					break;

				case 0xA0:
					if( IsTriplet(Addr) ) {
						
						uOffset = uOffset % 3;

						PDWORD pd = &PDWORD(p)[uOffset];
						
						CopyLong(PBYTE(pd), pData, uCount);
						}
					
					break;

				default:
					return CCODE_ERROR | CCODE_NO_DATA;
				}

			return uCount;
			}

		if( m_bError == 0x04 ) {

			return CCODE_ERROR | CCODE_NO_DATA;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CABLogix5EIPMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( CheckLink() ) {

		if( WriteData(Addr, pData, uCount) ) {
				
			return uCount;
			}
		}
	
	return CCODE_ERROR;
	}

// Text Loading

PTXT CABLogix5EIPMaster::GetString(PCBYTE &pData)
{
	WORD wLength = GetWord(pData);

	PTXT String = PTXT(Alloc(wLength + 1));

	memset(String, 0, wLength + 1);

	for( UINT n = 0; n < wLength; n++ ) {

		String[n] = GetByte(pData);
		}

	return String;
	}

// Read/Write Data

BOOL CABLogix5EIPMaster::ReadData(AREF Addr, UINT uCount)
{
	PCTXT pTag = FindTagName(Addr);

	if( pTag ) {

		UINT uOffset = Addr.a.m_Offset;

		CIOISegment Name(pTag);

		if( IsArray(Addr) ) {

			if( IsTriplet(Addr) ) {

				uOffset /= 3;
				}

			Name.Append(uOffset);
			}
		
		m_uPtr = 0;

		AddCount(Addr, uCount);

		if( Send(CIP_READ, Name) ) {
			
			if( Recv() ) {

				return TRUE;
				}

			return FALSE;
			}

		m_bError = 0;
		}

	return FALSE;
	}

BOOL CABLogix5EIPMaster::WriteData(AREF Addr, PDWORD pData, UINT uCount)
{
	PCTXT pTag = FindTagName(Addr);

	if( pTag ) {

		UINT uIndex = Addr.a.m_Offset;

		if( IsTriplet(Addr) ) {
			
			uIndex /= 3;
			}

		CIOISegment Name(pTag);

		if( IsArray(Addr) ) {

			Name.Append(uIndex);
			}

		if( IsTriplet(Addr) ) {

			PCTXT pSub1[] = { "ctl", "pre", "acc" };

			PCTXT pSub2[] = { "ctl", "len", "pos" };

			PCTXT *pSub = IsControl(Addr) ? pSub2 : pSub1;

			UINT  uOffset = Addr.a.m_Offset % 3;

			Name.Append(pSub[uOffset]);
			}

		m_uPtr = 0;

		AddCIPType(Addr);

		AddCount(Addr, uCount);

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:
				AddBits(pData, uCount);
				break;

			case addrByteAsByte:
				AddByte(pData, uCount);
				break;

			case addrWordAsWord:
				AddWord(pData, uCount);
				break;

			case addrLongAsLong:
			case addrRealAsReal:
				AddLong(pData, uCount);
				break;

			default:
				return CCODE_ERROR | CCODE_HARD;
			}

		return Send(CIP_WRITE, Name) && Recv();
		}

	return FALSE;
	}

void CABLogix5EIPMaster::LimitLump(AREF Addr, UINT &uCount)
{
	UINT  uType = Addr.a.m_Type;

	switch( uType ) {

		case addrWordAsWord:
		case addrWordAsLong:
		case addrWordAsReal:

			MakeMin(uCount, 240);

			break;

		case addrLongAsLong:
		case addrLongAsReal:
		case addrRealAsReal:

			MakeMin(uCount, 120);

			break;
		}
	}

// Transport Layer

BOOL CABLogix5EIPMaster::MakeLink(void)
{	
	if( m_fOpen ) {

		m_pExplicit = m_pEnetHelper->GetExplicit();
		}

	return CheckLink();
	}

BOOL CABLogix5EIPMaster::CheckLink(void)
{
	return m_fOpen && m_pExplicit;
	}

// CIP Support

BOOL CABLogix5EIPMaster::Send(BYTE bCode, CIOISegment &Name)
{
	CBuffer *pBuff = CreateBuffer(m_uPtr, FALSE);

	if( pBuff ) {

		CIPADDR Addr;
		
		Addr.m_ip	  = (IPADDR const &) m_pCtx->m_IP;
		
		Addr.m_wPort	  = 1;
		
		Addr.m_wSlot	  = m_pCtx->m_wSlot;
		
		Addr.m_pPath	  = NULL;

		Addr.m_bService   = bCode;
		
		Addr.m_wClass     = EIP_EMPTY;
		
		Addr.m_wInst	  = EIP_EMPTY;
		
		Addr.m_wAttr	  = EIP_EMPTY;
		
		Addr.m_wMember    = EIP_EMPTY;

		Addr.m_bTagData   = Name.GetData();

		Addr.m_uTagSize   = Name.GetSize();

		memcpy( pBuff->AddTail(m_uPtr), 
			m_bTxBuff, 
			m_uPtr
			);

		m_pExplicit->SetTimeout(m_pCtx->m_wTimeout);

		if( m_pExplicit->Send(Addr, pBuff) ) {
			
			return TRUE;
			}

		pBuff->Release();
		}

	return FALSE;
	}

BOOL CABLogix5EIPMaster::Recv(void)
{
	CBuffer *pBuff;

	if( m_pExplicit->Recv(pBuff) ) {
		
		if( pBuff) {

			memcpy( m_bRxBuff, 
				pBuff->GetData(), 
				pBuff->GetSize()
				);

			pBuff->Release();
			
			return TRUE;
			}

		m_bError = 0;

		return FALSE;
		}

	m_bError = m_pExplicit->GetLastError();

	return FALSE;
	}

BOOL CABLogix5EIPMaster::AddCIPType(AREF Addr)
{
	UINT uOffset = Addr.a.m_Offset;
	
	if( IsTriplet(Addr) && (uOffset % 3) == 0 ) {

		AddWord(0xD3);

		return TRUE;		
		}

	switch( Addr.a.m_Type ) {
		
		case addrBitAsBit:
			AddWord(0xC1);
			break;

		case addrByteAsByte:
			AddWord(0xC2);
			break;

		case addrWordAsWord:
			AddWord(0xC3);
			break;

		case addrLongAsLong:
			AddWord(0xC4);
			break;

		case addrRealAsReal:
			AddWord(0xCA);
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

void CABLogix5EIPMaster::AddCount(AREF Addr, UINT uCount)
{
	if( IsTriplet(Addr) ) {
		
		AddWord((uCount + 2) / 3);

		return;
		}

	AddWord(uCount);
	}

void CABLogix5EIPMaster::AddBits(PDWORD pData, UINT uCount)
{
	DWORD dwMask = 1;

	for( UINT n = 0; n < uCount; n ++ ) {

		AddByte(pData[n % 32] & dwMask ? 0xFF : 0x00);
		
		dwMask = dwMask ? dwMask << 1 : 1;
		}
	}

PCTXT CABLogix5EIPMaster::FindTagName(AREF Addr)
{
	if( IsTable(Addr) ) {
		
		return GetTagName(Addr.a.m_Table);
		}
	else
		return GetTagName(Addr.a.m_Offset);
	}

BOOL CABLogix5EIPMaster::IsTable(AREF Addr)
{
	return Addr.a.m_Table < addrNamed;
	}

BOOL CABLogix5EIPMaster::IsArray(AREF Addr)
{
	switch( Addr.a.m_Extra & 0x07 ) {

		case addrTimer:
		case addrCounter:
		case addrControl:
		case addrString:
			return Addr.a.m_Extra & 0x08 ? TRUE : FALSE;
		
		default:
			return IsTable(Addr);
		}
	}

BOOL CABLogix5EIPMaster::IsTriplet(AREF Addr)
{
	switch( Addr.a.m_Extra & 0x07 ) {

		case addrTimer:
		case addrCounter:
		case addrControl:

			return TRUE;
		}

	return FALSE;
	}

BOOL CABLogix5EIPMaster::IsControl(AREF Addr)
{
	switch( Addr.a.m_Extra & 0x07 ) {

		case addrControl:

			return TRUE;
		}

	return FALSE;
	}

BOOL CABLogix5EIPMaster::IsString(AREF Addr)
{
	switch( Addr.a.m_Extra & 0x07 ) {

		case addrString:

			return TRUE;
		}

	return FALSE;
	}

// Tag Name Support

void CABLogix5EIPMaster::LoadTags(LPCBYTE &pData)
{
	UINT uCount       = GetWord(pData);

	m_pCtx->m_uCount = uCount;

	m_pCtx->m_pTags  = new PTXT [ uCount ];

	for( UINT n = 0; n < uCount; n ++ ) {
		
		m_pCtx->m_pTags[n] = GetString(pData);
		}	
	}

PCTXT CABLogix5EIPMaster::GetTagName(UINT uIndex)
{
	if( uIndex < m_pCtx->m_uCount ) {
		
		return m_pCtx->m_pTags[uIndex];
		}

	return "";
	}

// DF1 Legacy

void CABLogix5EIPMaster::AddByte(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;
	}

void CABLogix5EIPMaster::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CABLogix5EIPMaster::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CABLogix5EIPMaster::AddByte(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		BYTE bData = pData[n];

		AddByte(bData);
		}
	}

void CABLogix5EIPMaster::AddWord(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		WORD wData = pData[n];

		AddWord(wData);
		}
	}

void CABLogix5EIPMaster::AddLong(PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD dwData = pData[n];

		AddWord(LOWORD(dwData));

		AddWord(HIWORD(dwData));
		}
	}

void CABLogix5EIPMaster::CopyByte(PBYTE pReply, PDWORD pData, UINT uCount)
{	
	for(UINT n = 0; n < uCount; n++ ) {

		BYTE bData = PBYTE(pReply)[n];

		pData[n] = bData;
		}
	}

void CABLogix5EIPMaster::CopyWord(PBYTE pReply, PDWORD pData, UINT uCount)
{	
	for(UINT n = 0; n < uCount; n++ ) {

		WORD wData = PU2(pReply)[n];

		pData[n] = SHORT(IntelToHost(wData));
		}
	}

void CABLogix5EIPMaster::CopyLong(PBYTE pReply, PDWORD pData, UINT uCount)
{	
	for( UINT n = 0; n < uCount; n++ ) {

		DWORD dwData = PU4(pReply)[n];

		pData[n] = LONG(IntelToHost(dwData));
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// IOI Segment
//

// Constructors

CIOISegment::CIOISegment(void)
{
	}

CIOISegment::CIOISegment(PCTXT pText)
{
	Encode(pText);
	}

// Operations

void CIOISegment::Append(PCTXT pText)
{
	CIOISegment More;

	More.Encode(pText);

	AddIOI(More);
	}

void CIOISegment::Append(UINT uIndex)
{
	if( uIndex < 0xFF ) {

		AddByte(0x28);
		
		AddByte(uIndex);
		}
	else {
		AddWord(0x29);

		AddWord(uIndex);
		}
	}

void CIOISegment::Encode(PCTXT pText)
{
	Init();
	
	AddText(pText);
	}

void CIOISegment::Encode(PCTXT pText, UINT uIndex)
{
	Encode(pText);

	Append(uIndex);
	}

void CIOISegment::Encode(PCTXT pText, UINT uIndex, PCTXT pMore)
{
	Encode(pText, uIndex);

	CIOISegment More;

	More.Encode(pMore);

	AddIOI(More);
	}

void CIOISegment::Encode(PCTXT pText, PCTXT pMore)
{
	Encode(pText);

	CIOISegment More;

	More.Encode(pMore);

	AddIOI(More);
	}

// Attributes

PBYTE CIOISegment::GetData(void)
{
	return m_Data;
	}

UINT CIOISegment::GetSize(void)
{
	return m_uSize;
	}

// Implementation

void CIOISegment::Init(void)
{
	m_pData = m_Data;

	m_uSize = 0;
	}

void CIOISegment::AddByte(BYTE bData)
{
	*m_pData = bData;

	m_pData++;

	m_uSize++;
	}

void CIOISegment::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CIOISegment::AddText(PCTXT pText)
{
	UINT uSize = strlen(pText);

	AddByte(0x91);

	AddByte(uSize);

	memcpy(m_pData, pText, uSize);

	m_pData += uSize;

	m_uSize += uSize;

	if( uSize % 2 ) {

		AddByte(0);
		}
	}

void CIOISegment::AddIOI(CIOISegment &Name)
{
	UINT  uSize = Name.GetSize();

	PBYTE pData = Name.GetData();

	memcpy(m_pData, pData, uSize);

	m_pData += uSize;

	m_uSize += uSize;
	}

// End of File
