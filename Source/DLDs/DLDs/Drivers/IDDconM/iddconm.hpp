//////////////////////////////////////////////////////////////////////////
//
// DCON Defs
//

#define ADDR	1
#define BAUD	2
#define CDIR	3
#define SUME	4
#define CODE	16

#define WHOK	5
#define WDGS	6
#define WDGT	7
#define WDGE	8

#define PVAL	9
#define SVAL	10

#define PSET	14
#define SSET	15

#define ANAP	19

#define LL	11
#define HL	12
#define CL	13

#define AO	17
#define AI	18

//////////////////////////////////////////////////////////////////////////
//
// ICP DAS DCON Master Driver 
//

class CIDDconMDriver : public CMasterDriver
{
	public:
		// Constructor
		CIDDconMDriver(void);

		// Destructor
		~CIDDconMDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BOOL	m_fCheck;
	       		};

		// Data Members
		
		LPCTXT		m_pHex;
		BYTE		m_bTx[64];
		BYTE		m_bRx[64];
		BYTE		m_bCheck;
		UINT		m_uPtr;
		CContext *	m_pCtx;
	

		// Implementation
		UINT DoConfigWrite(AREF Addr, PDWORD pData, UINT uCount);
		UINT DoWatchDogStatusWrite(AREF Addr, PDWORD pData, UINT uCount);
		UINT DoWatchDogTimeoutWrite(AREF Addr, PDWORD pData, UINT uCount);
		UINT DoInputClear(AREF Addr, PDWORD pData, UINT uCount);
		void GetRead(AREF Addr, PDWORD pData, UINT uCount);
		void GetReal(UINT uOffset, UINT uTable, PDWORD pData);

		// Transport
		void Send(void);
		BOOL Recv(UINT uOffset, UINT uTable);
				
		// Frame Building
		void Begin(UINT uOffset, UINT uTable, BOOL fWrite);
		void AddCommand(UINT uOffset, UINT uTable, PDWORD pData);
		void AddData(PDWORD pData, UINT uCount);
		void AddReal(PDWORD pItem, BOOL fLeading);
	      	void End(UINT uOffset, UINT uTable);
		void AddByte(BYTE bByte);
		void AddHex(BYTE bByte);
		void AddChar(char Char);
	     		
		// Helpers
		BOOL IsReadOnly(UINT uOffset, UINT uTable);
		BOOL IsWriteOnly(UINT uOffset, UINT uTable);
		BOOL IsBroadcast(UINT uOffset, UINT uTable);
		BOOL IsDropValid(UINT uOffset, UINT uTable);
		BOOL IsCheckValid(UINT uPtr, BYTE bCheck);
		BOOL IsDigit(BYTE bByte);
		BOOL IsHex(BYTE bByte);
		UINT FromAscii(BYTE bByte);
		UINT FindFirst(UINT uOffset, UINT uTable);
		UINT FindLast(UINT uOffset, UINT uTable);
		BOOL IsDiscrete(UINT uTable);
		UINT FindState(UINT uOffset, UINT uTable);
		BOOL IsHexExpected(UINT uOffset, UINT uTable);
		BOOL IsConfig(UINT uOffset, UINT uTable);
		BOOL IsWatchDogStatus(UINT uOffset, UINT uTable);
		BOOL IsWatchDogTimeout(UINT uOffset, UINT uTable);
		BOOL IsInputClear(UINT uOffset, UINT uTable);
	};

// End of File
