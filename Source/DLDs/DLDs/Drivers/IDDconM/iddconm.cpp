#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "iddconm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ICP DAS DCON Master Driver
//

// Instantiator

INSTANTIATE(CIDDconMDriver);

// Constructor

CIDDconMDriver::CIDDconMDriver(void)
{
	m_Ident         = DRIVER_ID;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	}

// Destructor

CIDDconMDriver::~CIDDconMDriver(void)
{
	}

// Configuration

void MCALL CIDDconMDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CIDDconMDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CIDDconMDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CIDDconMDriver::Open(void)
{
	}

// Device

CCODE MCALL CIDDconMDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop      = GetByte(pData);
			m_pCtx->m_fCheck     = GetByte(pData);
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;

			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CIDDconMDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

UINT MCALL CIDDconMDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == ADDR )  {  // Drop Number

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bDrop = uValue;

			return 1;
			}
		}

	if( uFunc == SUME ) {  // Check Sum Enable

		pCtx->m_fCheck = ATOI(Value) ? TRUE : FALSE;

		return 1;
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL CIDDconMDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = ADDR; 
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CIDDconMDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{	
	UINT uTable = Addr.a.m_Table;

	UINT uOffset = Addr.a.m_Offset;

	if( IsWriteOnly(uOffset, uTable) ) {

		pData[0] = 0;

		return uCount;
		}

	Begin(uOffset, uTable, FALSE);

	AddCommand(uOffset, uTable, NULL);

	End(uOffset, uTable);

	if( Recv(uOffset, uTable) ) {

		GetRead(Addr, pData, uCount);

		return IsDiscrete(uTable) ? uCount : 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CIDDconMDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable = Addr.a.m_Table;

	UINT uOffset = Addr.a.m_Offset;
	
	if( IsReadOnly(uOffset, uTable) ) {

		return uCount;
		}

	if( IsConfig(uOffset, uTable) ) {

		return DoConfigWrite(Addr, pData, uCount);
		}

	if( IsWatchDogStatus(uOffset, uTable) ) {

		return DoWatchDogStatusWrite(Addr, pData, uCount);
		}

	if( IsWatchDogTimeout(uOffset, uTable) ) {

		return DoWatchDogTimeoutWrite(Addr, pData, uCount);
		}
	
	if( IsInputClear(uOffset, uTable) ) {

		return DoInputClear(Addr, pData, uCount);
		}

	Begin(uOffset, uTable, TRUE);

	AddCommand(uOffset, uTable, pData);

	End(uOffset, uTable);

	if( Recv(uOffset, uTable) ) {

		return 1;
		}
	
	return CCODE_ERROR;
	}

// Implementation

UINT CIDDconMDriver::DoConfigWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD Data[1];
	       
	if( !COMMS_SUCCESS(Read(Addr, Data, 4)) ) {

		return CCODE_ERROR;
		}

	switch( Addr.a.m_Offset ) {

		case ADDR:

			Data[0]  = Data[0] & 0x00FFFFFF;

			Data[0] |= ((pData[0] & 0xFF) << 24);

			break;

		case CODE:

			Data[0]  = Data[0] & 0xFF00FFFF;

			Data[0] |= ((pData[0] & 0xFF) << 16);

			break;

		case BAUD:

			Data[0]  = Data[0] & 0xFFFF00FF;

			Data[0] |= ((pData[0] & 0xFF) << 8);

			break;

		case CDIR:

			Data[0]  = Data[0] & 0xFFFFFF7F;
		
			Data[0] |= ((pData[0] & 0x1) << 7);

			break;

		case SUME:

			Data[0]  = Data[0] & 0xFFFFFFBF;

			Data[0] |= ((pData[0] & 0x1) << 6);

			break;
		}
		
	Begin(Addr.a.m_Offset, Addr.a.m_Table, TRUE);

	AddData(Data, 8);

	End(Addr.a.m_Offset, Addr.a.m_Table);

	if( Recv(Addr.a.m_Offset, Addr.a.m_Table) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

UINT CIDDconMDriver::DoWatchDogStatusWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( pData[0] != 0 ) {

		return uCount;
		}

	Begin(Addr.a.m_Offset, Addr.a.m_Table, TRUE);

	AddByte('1');

	End(Addr.a.m_Offset, Addr.a.m_Table);

	if( Recv(Addr.a.m_Offset, Addr.a.m_Table) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

UINT CIDDconMDriver::DoWatchDogTimeoutWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD Data[1];
	       
	if( !COMMS_SUCCESS(Read(Addr, Data, 4)) ) {

		return CCODE_ERROR;
		}

	switch( Addr.a.m_Offset ) {

		case WDGT:	
			
			Data[0]  = Data[0] & 0x100;

			Data[0]	|= (pData[0] & 0xFF);

			break;

		case WDGE:

			Data[0]  = Data[0] & 0xFF;

			if( pData[0] ) {

				Data[0] |= 1 << 8;
				}
			
			break;

		}
		
	Begin(Addr.a.m_Offset, Addr.a.m_Table, TRUE);

	AddByte('3');

	AddData(Data, 3);

	End(Addr.a.m_Offset, Addr.a.m_Table);

	if( Recv(Addr.a.m_Offset, Addr.a.m_Table) ) {

		return uCount;
		} 

	return CCODE_ERROR;
	}

UINT CIDDconMDriver::DoInputClear(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == 5 && pData[0] != 0 ) {

		return uCount;
		}
	
	Begin(Addr.a.m_Offset, Addr.a.m_Table, TRUE);

	AddByte('C');

	if( Addr.a.m_Table < addrNamed ) {

		AddHex(Addr.a.m_Offset);
		}

	End(Addr.a.m_Offset, Addr.a.m_Table);

	if( Recv(Addr.a.m_Offset, Addr.a.m_Table) ) {

		return 1;
		}

	return CCODE_ERROR;
	}

void CIDDconMDriver::GetRead(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD dData  = 0;

	UINT uOffset = Addr.a.m_Offset;

	UINT uTable  = Addr.a.m_Table; 

	UINT u = FindFirst(uOffset, uTable);

	UINT uPtr = FindLast(uOffset, uTable); 

	if( IsHexExpected(uOffset, uTable) ) {

		for(	; u <= uPtr; u++ ) {

			dData <<= 4;

			dData += FromAscii(m_bRx[u]);
		       	}

		if( IsDiscrete(uTable) ) {

			for( UINT i = 0; i < uCount; i++ ) {

				pData[i] = (dData & (1 << (uOffset + i))) ? 1 : 0;
				}

			return;
			}

		if( IsConfig(uOffset, uTable) && uCount != 4 ) {

			switch( uOffset ) {

				case ADDR:	pData[0] = ((dData >> 24) & 0xFF);	return;

				case CODE:	pData[0] = ((dData >> 16) & 0xFF);	return;

				case BAUD:	pData[0] = ((dData >> 8 ) & 0xFF);	return;

				case CDIR:	pData[0] = ((dData & 0x80) ? 1 : 0);	return;

				case SUME:	pData[0] = ((dData & 0x40) ? 1 : 0);	return;
				}
			}

		if( IsWatchDogTimeout(uOffset, uTable) && uCount != 4 ) {

			switch( uOffset ) {

				case WDGT:	pData[0] = ((dData >> 0)  & 0xFF);	return;

				case WDGE:	pData[0] = ((dData >> 8 ) & 0x01);	return;

				}
			}
		
		pData[0] = dData;

		return;
		}
	
	if( Addr.a.m_Type == addrRealAsReal || Addr.a.m_Type == addrLongAsReal ) {

		GetReal(uOffset, uTable, pData);

		return;
		}

	BOOL fNeg = FALSE; 

	for( ; u <= uPtr; u++ ) {

		if( IsDigit(m_bRx[u]) ) {

			dData = 10 * dData + (m_bRx[u] - '0');
			}

		else if ( m_bRx[u] == '-' ) {

			fNeg = TRUE;
			}
		}

	pData[0] = fNeg ? -dData : +dData; 
	}

void CIDDconMDriver::GetReal(UINT uOffset, UINT uTable, PDWORD pData)
{
	UINT u = FindFirst(uOffset, uTable);

	UINT uPtr = FindLast(uOffset, uTable); 

	DWORD dwDP = 0;

	float Real = 0.0;
	
	BOOL fNeg = FALSE;

	BOOL fDP = FALSE;

	for ( ; u <= uPtr; u++ ) {

		if ( m_bRx[u] == '-' ) {

			fNeg = TRUE;

			continue;
			
			}

		if ( m_bRx[u] == '.' ) {

			fDP = TRUE;

			dwDP = 10;

			continue;

			}

		if( ( m_bRx[u] >= '0' ) && ( m_bRx[u] <= '9' ) ) {

			UINT uDigit = m_bRx[u] - '0'; 

			float u = uDigit;

			if( fDP ) {

				Real += ( u / dwDP );

				dwDP *= 10;
				}

			else {
				Real *= 10.0;
				
				Real += u;
				}
			}

		if ( u == uPtr ) {

			Real = fNeg ? -Real : +Real;

			pData[0] = *((DWORD*) &Real);
			}
		}  
	}

void CIDDconMDriver::Send(void)
{
	m_pData->ClearRx();

	m_pData->Write(m_bTx, m_uPtr, FOREVER);

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_uPtr; u++ ) {

		AfxTrace("%2.2x ", m_bTx[u]);
		}  */
	}

BOOL CIDDconMDriver::Recv(UINT uOffset, UINT uTable)
{
	if( IsBroadcast(uOffset, uTable) ) {

		while( m_pData->Read(0) < NOTHING );

		Sleep(20);

		return TRUE;
		}
	
	UINT uTimer = 0;

	UINT uData = 0;

	BYTE bCheck = 0;

	m_uPtr = 0;

	UINT uState = 0;

	BOOL fValid = FALSE;

	memset(m_bRx, 0, sizeof(m_bRx));

//	AfxTrace("\nRx ");

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

//		AfxTrace("%2.2x ", uData);

		bCheck += ( uData & 0xFF );

		switch( uState ) {
		
			case 0:
				if( uData == '?' ) {

					return FALSE;
					}

				if( uData == '!' ) {

					fValid = TRUE;

					uState = FindState(uOffset, uTable);
					}

				else if( uData == '>' ) {

					fValid = TRUE;

					uState = 3;
					}

				break;

			case 1:

				if( IsHex(uData) ) {

					m_bRx[m_uPtr++] = uData & 0xFF;
					
					uState = 2;
					}
				break;
				
				
			case 2:

				if( IsHex(uData) ) {
				
					m_bRx[m_uPtr++] = uData & 0xFF;

					if( IsDropValid(uOffset, uTable) ) {

					      	m_bRx[m_uPtr++] = 0;
					      				
						uState = 3;
						
						break;
						}
					}
					
				return 0;
				
			case 3:

				if( uData == CR ) {
				
					m_bRx[m_uPtr] = 0;
					
					if ( fValid && IsCheckValid( m_uPtr, bCheck ) ) {

						return TRUE;
						}
					}
				else {	
					m_bRx[m_uPtr] = uData & 0xFF;

					if( ++m_uPtr == sizeof(m_bRx) ) {

						return FALSE;
						}
					}
				break;
			}
		}
	
	return FALSE;
	}

// Frame Building

void CIDDconMDriver::Begin(UINT uOffset, UINT uTable, BOOL fWrite)
{
	m_uPtr = 0;

	m_bCheck = 0;
       
	if( uTable >= addrNamed ) {

		switch( uOffset ) {

			case ADDR:
			case BAUD:
			case CDIR:
			case SUME:
			case CODE:	fWrite ? AddByte('%') : AddByte('$');	break;
			
			case WHOK:
			case WDGS:
			case WDGT:
			case WDGE:
			case PVAL:
			case SVAL:
			case PSET:
			case SSET:	AddByte('~');	break;

			case ANAP:
			case LL:
			case HL:
			case CL:	AddByte('$');	break;

			case AO:	fWrite ? AddByte('#') : AddByte('$');	break;
			
			case AI:	AddByte('#');	break;
			}
		}
	else {
		switch( uTable ) {

			case 1:
			case 3:
			case 4:		!fWrite ? AddByte('$') : AddByte('#');	break;

			case 2:
			case 5:		!fWrite ? AddByte('#') : AddByte('$');	break;

			case 6:
			case 8:		AddByte('~');	break;

			case 7:		
			case 9:		AddByte('$');	break;
		
			}
		}
	
	AddByte(m_pHex[(m_pCtx->m_bDrop / 16) & 0x0F]);

	AddByte(m_pHex[(m_pCtx->m_bDrop % 16) & 0x0F]);
	}	

void CIDDconMDriver::AddCommand(UINT uOffset, UINT uTable, PDWORD pData)
{
	if( uTable >= addrNamed ) {
	
		switch( uOffset ) {

			case ADDR:
			case BAUD:
			case CDIR:
			case SUME:
			case CODE:	AddByte('2');	return;	
			
			
			case WHOK:	AddByte('*');	
					AddByte('*');	return;

			case WDGS:	AddByte('0');	return;

			case WDGT:
			case WDGE:	AddByte('2'); 	return;

			case PVAL:	AddByte('4');
					AddByte('P');	return;

			case SVAL:	AddByte('4');
				       	AddByte('S');	return;

			case PSET:	AddByte('5');
					AddByte('P');	return;

			case SSET:	AddByte('5');
					AddByte('S');	return;

			case ANAP:	AddByte('4');	return;

			case LL:	AddByte('L');
					AddByte('0');	return;
			
			case HL:	AddByte('L');
					AddByte('1');	return;

			case AO:	pData ? AddReal(pData, FALSE) : AddByte('6');	return;
			case AI:	return;

			}

		return;
		}

	if( IsDiscrete(uTable) ) {

		if( pData ) {

			if( uOffset > 7 ) {

				AddByte('B');

				AddHex(uOffset - 8);
				}
			else {
				AddByte('A');

				AddHex(uOffset);
				}

			AddData(pData, 2);

			return;
			}
		
		AddByte('6');

		return;
		}

	if( pData ) {

		switch( uTable ) {

			case 1:
			case 2:	
				AddHex(uOffset);

				AddReal(pData, TRUE);	
				
				break;

			case 8:
				AddByte('5');

				AddHex(uOffset);

				break;

			case 9:
				AddByte('4');

				AddHex(uOffset);

				break;
			}

		return;
		}

	switch( uTable ) {

		case 1:
			AddByte('8');	

			AddHex(uOffset);

			break;

		case 2:
		case 5:	AddHex(uOffset);
			
			break;

		case 6:	AddByte('4');

			AddHex(uOffset);

			break;

		case 7:	AddByte('7');

			AddHex(uOffset);

			break;
		}
	}

void CIDDconMDriver::AddData(PDWORD pItem, UINT uCount)
{
	DWORD dwItem = *pItem;

	for( UINT u = uCount; u > 0; u-- ) {

		AddByte(m_pHex[(dwItem/(0x01 << ( 4 * (u - 1) ))) & 0x0F]);
		}
	}

void CIDDconMDriver::AddReal(PDWORD pItem, BOOL fLeading)
{
	char * pFloat = (char *)alloca(10);

	SPrintf(pFloat, "%f", PassFloat(pItem[0]));

	UINT uEnd = 10;

	BOOL fAdd = FALSE;

	for( UINT u = 0; u < uEnd; u++ ) {

		if( u == 0 ) {

			if( fLeading ) {
				
				if( pFloat[u] != '+' && pFloat[u] != '-' ) {

					AddChar('+');

					if( pFloat[u + 1] == '.' ) {

						AddChar('0');
						}
					}
				else {
					fAdd = TRUE;
					}
				}
			else {
				AddChar('0');
				}
			}

		AddChar(pFloat[u]);

		if( fAdd ) {

			AddChar('0');

			fAdd = FALSE;
			}

		if( pFloat[u] == 0x2E ) {

			uEnd = u + 4;
			}
		}
	}

void CIDDconMDriver::End(UINT uOffset, UINT uTable)
{
	if( m_pCtx->m_fCheck ) {
		
		m_bTx[m_uPtr++] = m_pHex[m_bCheck / 16];

		m_bTx[m_uPtr++] = m_pHex[m_bCheck & 0x0F];
		}

	if( !IsBroadcast(uOffset, uTable) ) {

		AddByte(CR);
		}

	Send();

	}

void CIDDconMDriver::AddByte(BYTE bByte)
{
	m_bTx[m_uPtr++] = bByte;

	m_bCheck += bByte;
	}

void CIDDconMDriver::AddHex(BYTE bByte)
{
	BYTE bChar = m_pHex[bByte & 0xF];
	
	AddByte(bChar);
	}

void CIDDconMDriver::AddChar(char Char)
{
	m_bTx[m_uPtr++] = Char;

	m_bCheck += Char;
	}

// Helpers

BOOL CIDDconMDriver::IsWriteOnly(UINT uOffset, UINT uTable)
{
	if( uTable == 8 || uTable == 9 ) {

		return TRUE;
		}

	
	if( uTable >= addrNamed ) {
	
		switch( uOffset ) {

			case WHOK:
			case PSET:
			case SSET:
			case CL:
			case ANAP:

				return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CIDDconMDriver::IsReadOnly(UINT uOffset, UINT uTable)
{
	if( uTable == 2 || uTable == 4 ||
	    uTable == 6 || uTable == 7	) {

		return TRUE;
		}

	if( uTable >= addrNamed ) {

		switch( uOffset ) {

			case BAUD:

			case PVAL:
			case SVAL:

			case LL:
			case HL:

			case AI:

				return TRUE;
			}
		}

	return FALSE;
	}

BOOL CIDDconMDriver::IsBroadcast(UINT uOffset, UINT uTable)
{
	if( uTable >= addrNamed ) {

		switch( uOffset ) {

			case WHOK:

				return TRUE;
			}
		}

	return FALSE;
	}

BOOL CIDDconMDriver::IsDropValid(UINT uOffset, UINT uTable)
{
	if( uTable >= addrNamed ) {
			
		switch( uOffset ) {
		
			case LL:
			case HL:
			
				return TRUE;
			}
		}   

	if( m_bRx[0] == m_pHex[(m_pCtx->m_bDrop / 16) & 0x0F] ) {

		if( m_bRx[1] ==	m_pHex[(m_pCtx->m_bDrop % 16) & 0x0F] ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CIDDconMDriver::IsCheckValid(UINT uPtr, BYTE bCheck)
{
        if( !m_pCtx->m_fCheck ) {

		return TRUE;
		}
	
	BYTE bCheckReply;

	bCheck -= BYTE(CR + m_bRx[ uPtr - 1] + m_bRx[ uPtr - 2]);

	bCheckReply  = BYTE(FromAscii( m_bRx[ uPtr - 2 ] ) * 16 );

	bCheckReply += BYTE(FromAscii( m_bRx[ uPtr - 1 ] ));

	m_bRx[ uPtr - 1 ] = 0;

	m_bRx[ uPtr - 2 ] = 0;

	return( bCheck == bCheckReply );
	}

BOOL CIDDconMDriver::IsDigit(BYTE bByte)
{
	return( bByte >= '0' && bByte <= '9' );
	}

BOOL CIDDconMDriver::IsHex(BYTE bByte)
{
	return( bByte >= '0' && bByte <= '9' ) || ( bByte >= 'A' && bByte <= 'F' );
	}

UINT CIDDconMDriver::FromAscii(BYTE bByte)
{
	UINT uResult = 0;

	if(( bByte >= 0x30 ) && ( bByte <= 0x39 )) {

		uResult = ( bByte - 0x30 ) & 0xFF;
		}
	else if(( bByte >= 0x41 ) && ( bByte <= 0x46 )) {

		uResult = ( bByte - 0x37 ) & 0xFF;
		}

	return uResult;
	}

UINT CIDDconMDriver::FindFirst(UINT uOffset, UINT uTable)
{
	UINT uFirst = 3;

	if( uTable == 2 ) {

		return 0;
		}

	if( IsDiscrete(uTable) ) {

		return 0;
		}

	if( uTable >= addrNamed ) {

		switch( uOffset ) {

			case ADDR:
			case BAUD:
			case CDIR:
			case SUME:
			case CODE:
		     
				return 1;

			case LL:
			case HL:
			case AI:

				return 0;
			}
		}

	return uFirst;
	}

UINT CIDDconMDriver::FindLast(UINT uOffset, UINT uTable)
{
	if( IsDiscrete(uTable) ) {

		return ( m_pCtx->m_fCheck ? m_uPtr - 5 : m_uPtr - 3 );
		}

	if( uTable >= addrNamed ) {

		switch( uOffset ) {

			case LL:
			case HL:
			
				return ( m_pCtx->m_fCheck ? m_uPtr - 5 : m_uPtr - 3 );
			}
		}

	return ( m_pCtx->m_fCheck ? m_uPtr - 3 : m_uPtr - 1 );			
	}

BOOL CIDDconMDriver::IsDiscrete(UINT uTable)
{
	return ( (uTable == 3) || (uTable == 4) );
	}

UINT CIDDconMDriver::FindState(UINT uOffset, UINT uTable)
{
	if( IsDiscrete(uTable) ) {

		return 3;
		}

	if( uTable >= addrNamed ) {

		switch( uOffset ) {

			case ADDR:
			case BAUD:
			case CDIR:
			case SUME:
			case CODE:

			case LL:
			case HL:
				
				return 3;
			}
		}

	return 1;
	}

BOOL CIDDconMDriver::IsHexExpected(UINT uOffset, UINT uTable)
{
	if( IsDiscrete(uTable) ) {
		
		return TRUE;
		}

	if( uTable >= addrNamed ) {

		switch( uOffset ) {

			case ADDR:
			case BAUD:
			case CDIR:
			case SUME:
			case CODE:

			case WDGS:
			case WDGT:
			case WDGE:

			case LL:
			case HL:

				return TRUE;

			case PVAL:
			case SVAL:

				for( UINT u = 0; u <= FindLast(uOffset, uTable); u++ ) {

					if( m_bRx[u] == 0x2E || m_bRx[u] == 0x2B ) {

						return FALSE;
						}
					}

				return TRUE;
			}
		}

	return FALSE;
	}

BOOL CIDDconMDriver::IsConfig(UINT uOffset, UINT uTable)
{
	if( uTable >= addrNamed ) {

		return ( ((uOffset >= ADDR) && (uOffset <= SUME)) || uOffset == CODE );
		}

	return FALSE;
	}

BOOL CIDDconMDriver::IsWatchDogStatus(UINT uOffset, UINT uTable)
{
	if( uTable >= addrNamed ) {

		return uOffset == WDGS;
		}

	return FALSE;
	}

BOOL CIDDconMDriver::IsWatchDogTimeout(UINT uOffset, UINT uTable)
{
	if( uTable >= addrNamed ) {

		return ( (uOffset == WDGT) || (uOffset == WDGE) );
		}

	return FALSE;
	}

BOOL CIDDconMDriver::IsInputClear(UINT uOffset, UINT uTable)
{
	if( uTable >= addrNamed ) {

		if( uOffset == CL ) {

			return TRUE;
			}
		}
	
	return ( uTable == 5 );
	}

// End of File
