
#include "intern.hpp"

#include "mittcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi TCP Master Driver
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Constructor

CMitTCPMasterDriver::CMitTCPMasterDriver(void)
{
	}

// Destructor

CMitTCPMasterDriver::~CMitTCPMasterDriver(void)
{
	m_pCtx  = NULL;

	m_uKeep = 0;
	}

// Configuration

void MCALL CMitTCPMasterDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CMitTCPMasterDriver::Attach(IPortObject *pPort)
{

	}

void MCALL CMitTCPMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CMitTCPMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fCode		= GetByte(pData);
			m_pCtx->m_uMonitor	= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->m_bNet		= GetByte(pData);
			m_pCtx->m_bPlc		= GetByte(pData);
			m_pCtx->m_bCpu		= GetByte(pData);

			m_pCtx->m_fDirty        = FALSE;

			m_pBase->m_fCode = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;

	}

CCODE MCALL CMitTCPMasterDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMitTCPMasterDriver::Ping(void)
{
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}

		return CMelBaseMasterDriver::Ping();
		}

	return CCODE_ERROR; 
	}

// User Access

UINT MCALL CMitTCPMasterDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CContext * pCtx  = (CContext *) pContext;
	
	if( uFunc == 1 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		pCtx->m_IP = MotorToHost(dwValue);
		
		pCtx->m_fDirty = TRUE;

		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort  = uValue;
			
			pCtx->m_fDirty = TRUE;
			
			return 1;
			}
		}
	
	if ( uFunc == 3 )  {

		//Get IP Address

		return MotorToHost(pCtx->m_IP);
		}
	
	if ( uFunc == 4 )  {
		
		//Get Target Port
		
		return pCtx->m_uPort;
		}

	return 0;
	}

// Socket Management

BOOL CMitTCPMasterDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}
		
		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CMitTCPMasterDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		return FALSE;
		}
	
	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CMitTCPMasterDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

BOOL CMitTCPMasterDriver::Transact(UINT uTotal)
{      
	if( SendFrame() && RecvFrame(uTotal) ) {
		
		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CMitTCPMasterDriver::SendFrame(void)
{
	UINT uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE; 
	}

// Overidables

BOOL CMitTCPMasterDriver::CheckFrame(void)
{
	return FALSE;
	}

BOOL CMitTCPMasterDriver::RecvFrame(UINT uTotal)
{
	return FALSE;
	}

BOOL CMitTCPMasterDriver::Start(void)
{
	if( !OpenSocket() ) {

		return FALSE;
		}

	return CMelBaseMasterDriver::Start();
	}

// Helpers

void CMitTCPMasterDriver::AddCpu(void)
{
	switch( m_pCtx->m_bCpu ) {

		case 1: AddWord(cpu1);		return;
		case 2:	AddWord(cpu2);		return;
		case 3: AddWord(cpu3);		return;
		case 4:	AddWord(cpu4);		return;
		case 5:	AddWord(cpuCtrl);	return;
		case 6:	AddWord(cpuStdBy);	return;
		case 7:	AddWord(cpuSysA);	return;
		case 8: AddWord(cpuSysB);	return;
		}

	AddWord(cpuLocal);
	}

BOOL CMitTCPMasterDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL  CMitTCPMasterDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CMitTCPMasterDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
