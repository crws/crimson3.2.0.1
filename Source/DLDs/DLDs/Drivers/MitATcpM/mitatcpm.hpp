
#include "mittcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  Constants
//

#define BIT_READ	0
#define BIT_WRITE	2
#define WORD_READ	1
#define WORD_WRITE	3

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A Series PLC TCP Master Driver
//

class CMitAQTCPMaster : public CMitTCPMasterDriver 
{
	public:
		// Constructor
		CMitAQTCPMaster(void);

		// Destructor
		~CMitAQTCPMaster(void);

	protected:
		// Overridables
		void AddCommand(UINT uType, UINT uCommand, UINT uCount);
		void AddAddress(AREF Addr);
		BOOL RecvFrame(UINT uTotal);
		BOOL CheckFrame(void);
		void AddOffset(UINT uOffset);
		WORD GetCommand(UINT uType, BOOL fWrite);
       	};
   
// End of File
