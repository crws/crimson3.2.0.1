
#include "melbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CPU Access Codes
//
//

enum {
	cpuLocal = 0x03FF,
	cpu1	 = 0x03E0,
	cpu2	 = 0x03E1,
	cpu3	 = 0x03E2,
	cpu4	 = 0x03E3,
	cpuCtrl  = 0x03D0,
	cpuStdBy = 0x03D1,
	cpuSysA  = 0x03D2,
	cpuSysB	 = 0x03D3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Base TCP Driver
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

class CMitTCPMasterDriver : public CMelBaseMasterDriver
{
	public:
		// Constructor
		CMitTCPMasterDriver(void);

		// Destructor
		~CMitTCPMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		
		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
	
	protected:
		// Device Data
		struct CContext	: CMelBaseMasterDriver::CBaseCtx
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fCode;
			UINT	 m_uMonitor;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BYTE	 m_bNet;
			BYTE	 m_bPlc;
			BYTE	 m_bCpu;
			BOOL     m_fDirty;
			};

		// Data Members
		CContext * m_pCtx; 
		UINT	   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		virtual BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Implementation
		BOOL SendFrame(void);
		BOOL Transact(UINT uTotal);
		
		// Overridables
		virtual BOOL RecvFrame(UINT uTotal);
		virtual BOOL CheckFrame(void);
		virtual	BOOL Start(void);

		// Helpers
		void AddCpu(void);

		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);

	};
		

// End of File
