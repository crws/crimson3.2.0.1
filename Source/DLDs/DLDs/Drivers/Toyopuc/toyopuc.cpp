
#include "intern.hpp"

#include "toyopuc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC Driver
//

// Instantiator

INSTANTIATE(CToyodapucDriver);

// Constructor

CToyodapucDriver::CToyodapucDriver(void)
{
	m_Ident       = DRIVER_ID;
	
	CTEXT Hex[]   = "0123456789ABCDEF";

	m_pHex        = Hex;

	m_dErrorCode  = 0;

	m_dWriteTimeA = 0;
	m_dWriteTimeB = 0;
	}

// Destructor

CToyodapucDriver::~CToyodapucDriver(void)
{
	}

// Configuration

void MCALL CToyodapucDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CToyodapucDriver::CheckConfig(CSerialConfig &Config)
{
	UINT u = Config.m_uBaudRate;

	switch( u ) {

		case 300:	m_uResponseDelay =  6;	break;
		case 600:	m_uResponseDelay =  3;	break;
		case 1200:	m_uResponseDelay =  2;	break;
		default:	m_uResponseDelay =  1;	break;
		}

	Make485(Config, TRUE);
	}
	
// Management

void MCALL CToyodapucDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CToyodapucDriver::Open(void)
{
	}

// Device

CCODE MCALL CToyodapucDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop      = GetByte(pData);
			m_pCtx->m_fEnProgram = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CToyodapucDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CToyodapucDriver::Ping(void)
{
	return SendWriteEnable() ? 1 : CCODE_ERROR;
	}

CCODE MCALL CToyodapucDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( ReadNoTransmit(Addr.a.m_Table, pData) ) return 1;

	uCount = SetCommand( Addr, TRUE, uCount );

	return Transact(Addr.a.m_Table, Addr.a.m_Type, pData, uCount, TRUE) ? uCount : CCODE_ERROR;
	}

CCODE MCALL CToyodapucDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( WriteNoTransmit(Addr.a.m_Table, *pData) ) return 1;

	uCount = SetCommand( Addr, FALSE, uCount );

	AddWriteData(Addr.a.m_Table, Addr.a.m_Type, pData, uCount);

	DWORD d;

	return Transact(Addr.a.m_Table, Addr.a.m_Type, &d, uCount, FALSE) ? uCount : CCODE_ERROR;
	}

// Frame Building

UINT	CToyodapucDriver::SetCommand(AREF Addr, BOOL fIsRead, UINT uCount)
{
	StartFrame();

	AddCommand(Addr, fIsRead);

	UINT r = SpecifyCount(Addr, uCount);

	m_uEndOfCommand = IsTimerCounter(Addr.a.m_Table) ? m_uPtr - 4 : m_uPtr;

	return r;
	}

void	CToyodapucDriver::StartFrame(void)
{
	m_uPtr   = 0;

	AddByte(':');
	AddByte(':');

	m_bCheck = 0;

	AddByte( m_pHex[m_pCtx->m_bDrop / 8] );
	AddByte( m_pHex[m_pCtx->m_bDrop % 8] );

	AddByte( '?' );

	AddByte( m_pHex[m_uResponseDelay] );
	}

void	CToyodapucDriver::AddCommand(AREF Addr, BOOL fIsRead)
{
	UINT uOffset = Addr.a.m_Offset;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	AddBitCommand( Addr, fIsRead);	break;
		case addrWordAsWord:	AddWordCommand(Addr, fIsRead);	break;

		case addrLongAsLong:	AddLongCommand(Addr, fIsRead);	return;

		case addrByteAsByte:
			AddByteCommand(Addr, fIsRead);
			uOffset &= 0x7FFF;
			break;
		}

	AddIdentifier(Addr.a.m_Table);

	AddData(uOffset, 4);

	if( Addr.a.m_Type == addrByteAsByte ) AddByte(Addr.a.m_Offset & 0x8000 ? 'H' : 'L');
	}

void	CToyodapucDriver::AddIdentifier(UINT uTable)
{
	switch( uTable ) {

		case CMEX:	AddByte('E');
		case CMX:	AddByte('X');	return;

		case CMEY:	AddByte('E');
		case CMY:	AddByte('Y');	return;

		case CMEM:	AddByte('E');
		case CMM:	AddByte('M');	return;

		case CMEK:	AddByte('E');
		case CMK:	AddByte('K');	return;

		case CMEV:	AddByte('E');
		case CMV:	AddByte('V');	return;

		case CMET:	AddByte('E');
		case CMT:	AddByte('T');	return;

		case CMEC:	AddByte('E');
		case CMC:	AddByte('C');	return;

		case CMEL:	AddByte('E');
		case CML:	AddByte('L');	return;

		case CMEP:	AddByte('E');
		case CMP:	AddByte('P');	return;

//		case CMEN:	AddByte('E');		// not supported in protocol
		case CMZ:
		case CMN:			return; // no identifier

		case CMES:	AddByte('E');
		case CMS:	AddByte('S');	return;

		case CMD:	AddByte('D');	return;

		case CMR:	AddByte('R');	return;

//		case CMH:	AddByte('H');	return; // not supported in protocol

		case CMU:	AddByte('U');	return;

		case CMGX:	AddByte('G');	AddByte('X');	return;

		case CMGY:	AddByte('G');	AddByte('Y');	return;

		case CMGM:	AddByte('G');	AddByte('M');	return;

		case CMEB:	AddByte('E');
		case CMB:	AddByte('B');	return;
		}
	}

UINT	CToyodapucDriver::SpecifyCount(AREF Addr, UINT uCount)
{
	if( IsTimerCounter(Addr.a.m_Table) ) return 1;

	UINT uSz = min( uCount, 8 );

	switch( Addr.a.m_Type ) {

		case addrByteAsByte:

			AddData( uSz - 1, 2 );
			return uSz;

		case addrWordAsWord:

			AddData( Addr.a.m_Offset + uSz - 1, 4 );
			return uSz;
		}

	return 1;
	}

void	CToyodapucDriver::AddWriteData(UINT uTable, UINT uType, PDWORD pData, UINT uCount)
{
	UINT u;

	switch( uType ) {

		case addrBitAsBit:
			AddByte( (*pData) & 1 ? '1' : '0' );
			return;

		case addrByteAsByte:
			for( u = 0; u < uCount; u++ ) AddData( pData[u], 2 );
			return;

		case addrWordAsWord:

			if( !IsTimerCounter(uTable) ) for( u = 0; u < uCount; u++ ) AddData( pData[u], 4 );

			else AddBCDData( pData[0], 10000 );

			return;

		case addrLongAsLong:

			switch( uTable ) {

				case CMWTS:

					u = 32;

					while( u ) {

						u -= 8;

						AddBCDData( (m_dWriteTimeA >> u) & 0xFF, 10 );
						}

					AddBCDData( (m_dWriteTimeB >> 12) & 0xFF, 10 );
					AddBCDData( (m_dWriteTimeB >>  4) & 0xFF, 10 );
					AddBCDData( (m_dWriteTimeB & 0xF),  1 );
					break;
				}
			return;
		}
	}

void	CToyodapucDriver::AddChecksum(void)
{
	AddData( 0x100 - UINT(m_bCheck), 2 );
	}

void	CToyodapucDriver::EndFrame(void)
{
	AddByte( CR );
	}

void	CToyodapucDriver::AddByte(BYTE b)
{
	m_bTx[m_uPtr++] = b;

	m_bCheck += b;
	}

void	CToyodapucDriver::AddData(DWORD u, UINT uCount)
{
	while( uCount ) {

		uCount--;

		AddByte( m_pHex[ (u >> (4 * uCount)) % 16 ] );
		}
	}

void	CToyodapucDriver::AddBCDData(DWORD u, UINT uSize)
{
	while( uSize ) {

		AddByte( m_pHex[ (u/uSize) % 10] );

		uSize /= 10;
		}
	}

void	CToyodapucDriver::AddBitCommand(AREF Addr, BOOL fIsRead)
{
	AddProgramNumber(Addr.a.m_Table, Addr.a.m_Extra);

	AddByte( fIsRead ? 'M' : 'S' );
	AddByte( 'R');
	AddByte( fIsRead ? 'L' : 'R' );
	}

void	CToyodapucDriver::AddByteCommand(AREF Addr, BOOL fIsRead)
{
	AddProgramNumber(Addr.a.m_Table, Addr.a.m_Extra);
 
	AddByte( fIsRead ? 'M' : 'S' );
	AddByte( 'R' );
	AddByte( 'B' );
	}

void	CToyodapucDriver::AddWordCommand(AREF Addr, BOOL fIsRead)
{
	AddProgramNumber(Addr.a.m_Table, Addr.a.m_Extra);
 
	if( IsTimerCounter(Addr.a.m_Table) ) {

		AddByte( fIsRead ? 'T' : Addr.a.m_Table == CMN ? 'P' : 'S' );
		AddByte( fIsRead ? 'C' : 'P' );
		AddByte( fIsRead ? 'R' : 'W' );
		return;
		}

	AddByte( fIsRead ? 'R' : 'W' );
	AddByte( 'D' );
	AddByte( 'R' );
	}

void	CToyodapucDriver::AddLongCommand(AREF Addr, BOOL fIsRead)
{
	switch( Addr.a.m_Table ) {

		case CMMPE:
			AddByte('M');
			AddByte('P');
			AddByte('E');
			return;

		case CMWTL:
		case CMWTH:
		case CMWTS:
			AddByte('W');
			AddByte('T');
			AddByte(fIsRead ? 'R' : 'C');
			return;
		}
	}

// Transport Layer

BOOL	CToyodapucDriver::Transact(UINT uTable, UINT uType, PDWORD pData, UINT uCount, BOOL fIsRead)
{
	Send();

	if( GetReply() ) {

		GetResponse(uTable, uType, pData, fIsRead ? uCount : 1);

		return TRUE;
		}

	return FALSE;
	}

void	CToyodapucDriver::Send(void)
{
	AddChecksum();

	EndFrame();

	m_pData->ClearRx();

	Put();
	}

// Port Access

void	CToyodapucDriver::Put(void)
{
/**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

UINT	CToyodapucDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Receive Processing

BOOL	CToyodapucDriver::GetReply(void)
{
	WORD wData;

	UINT uDelay = 100 * m_uResponseDelay;

	UINT uTimer = 1000 + uDelay;

	UINT uPtr   = 0;

	UINT uState = 0;

	UINT u      = m_pCtx->m_fEnProgram ? 7 : 6;

	SetTimer(uTimer);

/**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (wData = Get(uTimer)) == LOWORD(NOTHING) ) {

			continue;
			}

		uTimer = uDelay;

		SetTimer(uTimer);

		BYTE bData    = LOBYTE(wData);

		m_bRx[uPtr++] = bData;

		if( uPtr >= sizeof(m_bRx) ) return FALSE;

/**/		AfxTrace1("<%2.2x>", bData);

		switch( uState ) {

			case 0:
				if( bData == ':' ) uState = 1;

				else uPtr = 0;
				break;

			case 1:
				uState = bData == ':' ? 2 : 10;
				break;

			case 2: // Address H
				uState = bData == m_pHex[m_pCtx->m_bDrop/8] ? 3 : 10;
				break;

			case 3: // Address L
				uState = bData == m_pHex[m_pCtx->m_bDrop%8] ? 4 : 10;
				break;

			case 4: // ACK = '#', else NAK
				uState = bData == '#' || bData == '%' ? 5 : 10;
				break;

			case 5: // Response Delay
				uState = m_pCtx->m_fEnProgram ? 6 : 7;
				break;

			case 6: // Program Number, if enabled
				uState = 7;
				break;

			case 7:
				if( bData == CR ) {

					if( uPtr <= 10 ) return FALSE;

					if( m_bRx[4] == '#' ) {

						while( u < m_uEndOfCommand ) {

							if( m_bRx[u] != m_bTx[u] ) return FALSE;

							u++;
							}

						return CheckResponse(uPtr);
						}

					UINT i;

					for( i = u; i < u+3; i++ ) {

						m_dErrorCode <<= 8;

						m_dErrorCode  += m_bTx[i];
						}

					BYTE b;

					b = m_bTx[u+3];

					if( m_bTx[u] == 'T' ) b = m_bTx[u+6]; // LSD of T/C address

					else {
						if( m_bTx[u+3] == 'E' || m_bTx[u+3] == 'G' ) b = m_bTx[u+4];
						}

					m_dErrorCode  = (m_dErrorCode << 8) + b;

					m_dErrorValue = GetData( 6, 2 );						

					return FALSE;
					}

				break;

			case 10:
				uPtr = 0;
				if( bData == CR ) return FALSE;
				break;
			}
		}

	return FALSE;
	}

BOOL	CToyodapucDriver::CheckResponse(UINT uEnd)
{
	UINT uData = 0;

	for( UINT u = 2; u < uEnd - 3; u++ ) uData -= m_bRx[u];

	if( m_pHex[LOBYTE(uData)/16] != m_bRx[uEnd-3] ) return FALSE;
	if( m_pHex[LOBYTE(uData)%16] != m_bRx[uEnd-2] ) return FALSE;

//	for( UINT k = m_uEndOfCommand; k < uEnd - 3; k++ ) m_bRx[k] = m_pHex[k % 16];
	return TRUE;
	}

void	CToyodapucDriver::GetResponse(UINT uTable, UINT uType, PDWORD pData, UINT uCount)
{
	if( IsTimerCounter(uTable) ) {

		pData[0] = GetBCDData( m_uEndOfCommand + (uTable == CMN ? 5 : 0), 5 );

		return;
		}

	UINT uSize  = 1;
	UINT uStart = m_uEndOfCommand;
	UINT i;

	switch( uType ) {

		case addrByteAsByte:	uSize = 2;	break;
		case addrWordAsWord:	uSize = 4;	break;

		case addrLongAsLong:

			DWORD d;

			switch( uTable ) {

				case CMWTH:

					uStart += 8;

					d = GetBCDData( uStart, 2 );
					d = (d << 8) + GetBCDData( uStart+2, 2);
					d = (d << 4) + m_bRx[uStart+4] - '0';

					pData[0] = d;

					return;

				case CMWTL:

					d = 0;

					for( i = uStart; i < uStart + 8; i += 2 ) {

						d <<= 8;
						d  += GetBCDData( i, 2 );
						}

					pData[0] = d;

					return;

				default: uSize = 8;	break;
				}

			break;
		}

	for( i = 0; i < uCount; i++ ) pData[i] = GetData(uStart + (i * uSize), uSize);
	}

DWORD	CToyodapucDriver::GetData(UINT uPos, UINT uSize)
{
	DWORD d   = 0;

	UINT uEnd = uPos + uSize;

	while( uPos < uEnd ) {

		BYTE b = m_bRx[uPos++];

		if( b >= '0' && b <= '9' ) b -= '0';

		else {
			if( b >= 'A' && b <= 'F' ) b -= '7';

			else {
				if( b >= 'a' && b <= 'f' ) b -= 'W';

				else b = 0;
				}
			}

		d = (16 * d) + b;
		}

	return d;
	}

DWORD	CToyodapucDriver::GetBCDData(UINT uPos, UINT uSize)
{
	DWORD d   = 0;

	UINT uEnd = uPos + uSize;

	while( uPos < uEnd ) d = (10 * d) + m_bRx[uPos++] - '0';

	return d;
	}

// Helpers

void	CToyodapucDriver::AddProgramNumber(UINT uTable, UINT uExtra)
{
	if( !m_pCtx->m_fEnProgram ) return;

	switch( uTable ) {

		case CMX:
		case CMY:
		case CMM:
		case CMK:
		case CMV:
		case CMT:
		case CMC:
		case CML:
		case CMP:
		case CMD:
		case CMR:
		case CMS:
		case CMN:
		case CMZ:
			AddByte('0' + uExtra);
			return;

		case CMEX:
		case CMEY:
		case CMEM:
		case CMEK:
		case CMEV:
		case CMET:
		case CMEC:
		case CMEL:
		case CMEP:
		case CMES:
//		case CMEN:	// not supported in protocol
//		case CMH:	// not supported in protocol
		case CMGX:
		case CMGY:
		case CMGM:
			AddByte('0');
			return;

		case CMU:
			AddByte('8');
			return;
		}
	}

BOOL	CToyodapucDriver::IsTimerCounter(UINT uTable)
{
	return uTable == CMN || uTable == CMZ;
	}

BOOL	CToyodapucDriver::SendWriteEnable(void)
{
	StartFrame();
	AddByte('E');
	AddByte('W');
	AddByte('R');

	m_uEndOfCommand = m_uPtr;

	AddByte('D');

	Send();

	return GetReply();
	}

BOOL	CToyodapucDriver::ReadNoTransmit(UINT uTable, PDWORD pData)
{
	switch( uTable ) {

		case CMERC:
			*pData = m_dErrorCode;
			return TRUE;

		case CMERR:
			*pData = m_dErrorValue;
			return TRUE;

		case CMWTS:
			*pData = 0;
			return TRUE;

		case CMWTA:
			*pData = m_dWriteTimeA;
			return TRUE;

		case CMWTB:
			*pData = m_dWriteTimeB;
			return TRUE;
		}

	return FALSE;
	}

BOOL	CToyodapucDriver::WriteNoTransmit(UINT uTable, DWORD dData)
{
	switch( uTable ) {

		case CMERC:
		case CMERR:
			m_dErrorCode  = 0;
			m_dErrorValue = 0;
			return TRUE;

		case CMWTA:
			m_dWriteTimeA = dData;
			return TRUE;

		case CMWTB:
			m_dWriteTimeB = dData;
			return TRUE;

		case CMMPE:
		case CMWTH:
		case CMWTL:
			return TRUE;
		}

	return FALSE;
	}

// End of File
