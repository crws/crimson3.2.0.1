
//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC Driver
//

class CToyodapucDriver : public CMasterDriver
{
	public:
		// Constructor
		CToyodapucDriver(void);

		// Destructor
		~CToyodapucDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			BOOL m_fEnProgram;
			};
		CContext *	m_pCtx;

		// Hex Lookup
		LPCTXT m_pHex;
		
		// Comms Data
		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		BYTE	m_bCheck;
		UINT	m_uPtr;
		UINT	m_uEndOfCommand;
		UINT	m_uResponseDelay;
		DWORD	m_dErrorCode;
		DWORD	m_dErrorValue;
		DWORD	m_dWriteTimeA;
		DWORD	m_dWriteTimeB;

		// Frame Building
		UINT	SetCommand(AREF Addr, BOOL fIsRead, UINT uCount);
		void	StartFrame(void);
		void	AddCommand(AREF Addr, BOOL fIsRead);
		void	AddIdentifier(UINT uTable);
		UINT	SpecifyCount(AREF Addr, UINT uCount);
		void	AddWriteData(UINT uTable, UINT uType, PDWORD pData, UINT uCount);
		void	AddChecksum(void);
		void	EndFrame(void);
		void	AddByte(BYTE b);
		void	AddData(DWORD u, UINT uCount);
		void	AddBCDData(DWORD u, UINT uSize);
		void	AddBitCommand( AREF Addr, BOOL fIsRead);
		void	AddByteCommand(AREF Addr, BOOL fIsRead);
		void	AddWordCommand(AREF Addr, BOOL fIsRead);
		void	AddLongCommand(AREF Addr, BOOL fIsRead);

		// Transport Layer
		BOOL	Transact(UINT uTable, UINT uType, PDWORD pData, UINT uCount, BOOL fIsRead);
		void	Send(void);

		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Receive Processing
		BOOL	GetReply(void);
		BOOL	CheckResponse(UINT uEnd);
		void	GetResponse(UINT uTable, UINT uType, PDWORD pData, UINT uCount);
		DWORD	GetData(UINT uPos, UINT uSize);
		DWORD	GetBCDData(UINT uPos, UINT uSize);

		// Helpers
		void	AddProgramNumber(UINT uTable, UINT uExtra);
		BOOL	IsTimerCounter(UINT uTable);
		BOOL	SendWriteEnable(void);
		BOOL	ReadNoTransmit(UINT uTable, PDWORD pData);
		BOOL	WriteNoTransmit(UINT uTable, DWORD dData);

	};

#define	CMX	1
#define	CMY	2
#define	CMM	3
#define	CMK	4
#define	CMV	5
#define	CMT	6
#define	CMC	7
#define	CML	8
#define	CMP	9
#define	CMD	10
#define	CMR	11
#define	CMN	12
#define	CMZ	32
#define	CMS	13
#define	CMEX	14
#define	CMEY	15
#define	CMEM	16
#define	CMEK	17
#define	CMEV	18
#define	CMET	19
#define	CMEC	20
#define	CMEL	21
#define	CMEP	22
#define	CMEN	23 // not supported in protocol
#define	CMH	24 // not supported in protocol
#define	CMES	25
#define	CMGX	26
#define	CMGY	27
#define	CMGM	28
#define	CMU	29
#define	CMEB	30
#define	CMB	31
// following have no address field
#define	CMMPE	40
#define	CMWTL	41
#define	CMWTH	42
#define	CMWTS	43
#define	CMWTA	44
#define	CMWTB	45
#define	CMERC	98
#define	CMERR	99

// End of File
