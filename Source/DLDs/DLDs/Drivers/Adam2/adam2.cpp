
#include "adam2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series v2 Module Master Driver
//

// Instantiator

INSTANTIATE(CAdam4000v2Driver);

// Constructor

CAdam4000v2Driver::CAdam4000v2Driver(void)
{
	m_Ident = DRIVER_ID;

	m_pHex	= PCTXT("0123456789ABCDEF");
	}

// Destructor

CAdam4000v2Driver::~CAdam4000v2Driver(void)
{
	}

// Configuration

void MCALL CAdam4000v2Driver::Load(LPCBYTE pData)
{
	}
	
void MCALL CAdam4000v2Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CAdam4000v2Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CAdam4000v2Driver::Open(void)
{
	}

// Device

CCODE MCALL CAdam4000v2Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_uCmds = GetWord(pData);

			m_pCtx->m_pCmds = new CCmd[m_pCtx->m_uCmds];

			for(UINT u = 0; u < m_pCtx->m_uCmds; u++ ) {

				m_pCtx->m_pCmds[u].m_pRead   = GetString(pData);

				m_pCtx->m_pCmds[u].m_pWrite  = GetString(pData);

				m_pCtx->m_pCmds[u].m_bChan   = GetByte(pData);

				m_pCtx->m_pCmds[u].m_bForm   = GetByte(pData);

				m_pCtx->m_pCmds[u].m_bMisc   = GetByte(pData);

				m_pCtx->m_pCmds[u].m_Ref     = GetLong(pData);

				SetAccess(m_pCtx->m_pCmds[u]);

				ClearWriteRetry(m_pCtx->m_pCmds[u]);
				}

			m_pCtx->m_fCheck = TRUE;
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;

			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CAdam4000v2Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete [] m_pCtx->m_pCmds;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CAdam4000v2Driver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CContext * pCtx = (CContext *) pContext;

	if( pCtx ) {

		if( uFunc == 1 ) {

			pCtx->m_bDrop = strtol(Value, NULL, 10);

			return 1;
			}
		}

	return 0;
	}


// Entry Points

CCODE MCALL CAdam4000v2Driver::Ping(void)
{
	DWORD Data[1];

	for( UINT u = 0; u < 2; u++ ) {
	
		if( COMMS_SUCCESS(ReadConfig(Data)) ) {

			return 1;
			}

		m_pCtx->m_fCheck = !m_pCtx->m_fCheck;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CAdam4000v2Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CCmd * pCmd = FindCmd(Addr.m_Ref);

	if( pCmd ) {

		if( IsWriteOnly(pCmd) ) {

			return 1;
			}

		if( IsConfig(pCmd->m_pRead, FALSE) ) {

			return ReadConfig(pData);
			}

		AddCmd(pCmd->m_pRead, pCmd->m_bChan, FALSE);

		if( Transact(pCmd->m_pRead, pCmd->m_bMisc) ) {

			return GetData(Addr.a.m_Type, pCmd->m_bForm, pData, uCount);
			}

		return CCODE_ERROR;
		}
		
	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CAdam4000v2Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CCmd * pCmd = FindCmd(Addr.m_Ref);

	if( pCmd ) {

		if( IsReadOnly(pCmd) ) {

			return 1;
			}

		if( IsClearedWrite(*pCmd, pData[0]) ) {

			ClearWriteRetry(*pCmd);

			return 1;
			}

		if( IsConfig(pCmd->m_pWrite, TRUE) ) {

			if( COMMS_SUCCESS(WriteConfig(pData)) ) {

				ClearRetry(*pCmd);

				return 1;
				}

			SetWriteFailed(*pCmd, pData[0]);
		
			return CCODE_ERROR;
			}

		DWORD Data[1];

		if( IsCmd(pCmd->m_bForm) ) {

			if( pData[0] == 0 ) {

				ClearRetry(*pCmd);

				return 1;
				}
			}
		else {
			if( !COMMS_SUCCESS(ReadConfig(Data)) ) {

				return CCODE_ERROR;
				}
			}

		AddCmd(pCmd->m_pWrite, pCmd->m_bChan, TRUE);

		AddData(pCmd->m_bMisc, pCmd->m_bForm, Addr.a.m_Type, pData);

		if( Transact(pCmd->m_pWrite, pCmd->m_bMisc) ) {

			if( IsCmd(pCmd->m_bForm) ) {

				while( !COMMS_SUCCESS(Ping()) ) {

					Sleep(1000);

					continue;
					}
				}

			ClearRetry(*pCmd);
					
			return 1;
			}

		SetWriteFailed(*pCmd, pData[0]);
		
		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

CCODE CAdam4000v2Driver::ReadConfig(PDWORD pData)
{
	PCTXT pSyntax = GetConfigReadCmd();

	AddCmd(pSyntax, cNothing, FALSE);

	if( Transact(pSyntax, 0) ) {

		GetData(addrLongAsLong, formHex, pData);

		m_pCtx->m_Config = pData[0];

		SetCheck(pData[0]);

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE CAdam4000v2Driver::WriteConfig(PDWORD pData)
{
	PCTXT pSyntax = GetConfigWriteCmd();

	AddCmd(pSyntax, cNothing, TRUE);

	char Data[20];

	memset(Data, 0, sizeof(Data));

	SPrintf(Data, "%8.8X", pData[0]);

	AddText(PCTXT(Data));

	if( Transact(pSyntax, 0) ) {

		m_pCtx->m_Config = pData[0];

		SetCheck(pData[0]);

		// Module requires 7 seconds to load new config.

		UINT uTime = /*m_pHelper->*/GetTickCount();

		while( !IsTimedOut(uTime, 7000) ) {

			Sleep(1000);
			}

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE CAdam4000v2Driver::GetData(BYTE bType, BYTE bForm, PDWORD pData, UINT uCount)
{
	if( IsReal(bType) ) {

		return GetReal(pData);
		}

	if( bForm == formHex ) {

		return GetHex(pData);
		}

	if( bForm == formText ) {

		return GetText(pData, uCount);
		}
	
	return GetInteger(pData);
	}

CCODE CAdam4000v2Driver::GetHex(PDWORD pData)
{
	DWORD dwData = 0;

	for( UINT u = 0; u < m_uPtr - 1; u++ ) {

		dwData <<= 4;

		dwData = dwData + (FromASCII(m_bRx[u]));
		}

	pData[0] = dwData;

	return 1;
	}

CCODE CAdam4000v2Driver::GetReal(PDWORD pData)
{
	char Data[20];

	memset(Data, 0, sizeof(Data));

	memcpy(Data, m_bRx, m_uPtr);

	pData[0] = ATOF(Data);

	return 1;
	}

CCODE CAdam4000v2Driver::GetInteger(PDWORD pData)
{
	BOOL fNeg = FALSE; 

	DWORD dwData = 0;

	for( UINT u = 0; u < m_uPtr - 1; u++ ) {

		if( isdigit(m_bRx[u]) ) { 

			dwData = 10 * dwData + (FromASCII(m_bRx[u]));
			}

		else if ( m_bRx[u] == '-' ) {

			fNeg = TRUE;
			}
		}

	pData[0] = fNeg ? -dwData : +dwData;

	return 1;
	}

CCODE CAdam4000v2Driver::GetText(PDWORD pData, UINT uCount)
{
	strcpy(PTXT(pData), PTXT(m_bRx));

	MakeMin(uCount, 85);

	return uCount;
	}

CCmd * CAdam4000v2Driver::FindCmd(DWORD dwRef)
{
	for( UINT u = 0; m_pCtx->m_uCmds; u++ ) {

		if( m_pCtx->m_pCmds[u].m_Ref == dwRef ) {

			return &m_pCtx->m_pCmds[u];
			}
		}

	return NULL;
	}

BOOL CAdam4000v2Driver::IsConfig(PCTXT pSyntax, BOOL fWrite)
{	
	return !strcmp(fWrite ? GetConfigWriteCmd() : GetConfigReadCmd(), pSyntax);
	}

BOOL CAdam4000v2Driver::IsReadOnly(CCmd * pCmd)
{
	if( pCmd ) {

		return pCmd->m_bAccess == acReadOnly;
		}

	return TRUE;
	}

BOOL CAdam4000v2Driver::IsWriteOnly(CCmd * pCmd)
{
	if( pCmd ) {

		return pCmd->m_bAccess == acWriteOnly;
		}

	return TRUE;
	}

BOOL CAdam4000v2Driver::IsBroadcast(PCTXT pSyntax)
{
	return !strcmp(PCTXT("#**"), pSyntax);
	}

BOOL CAdam4000v2Driver::IsCmd(BYTE bForm)
{
	return bForm == formCmd;
	}

BOOL CAdam4000v2Driver::IsText(BYTE bForm)
{
	return bForm == formText;
	}

BOOL CAdam4000v2Driver::IsHex(BYTE bForm)
{
	return bForm == formHex;
	}

BOOL CAdam4000v2Driver::IsReal(BYTE bType)
{
	return bType == addrRealAsReal;
	}

DWORD CAdam4000v2Driver::GetDigitMask(BYTE bForm)
{
	if( ((bForm & 0xF0) == formDigitM) ) {

		BYTE bDigits = bForm & 0xF;

		if( bDigits > 0 ) {

			return bDigits;
			}
		}

	return NOTHING;
	}

void CAdam4000v2Driver::SetAccess(CCmd &Cmd)
{
	if( !strcmp(Cmd.m_pRead, "\0") ) {

		Cmd.m_bAccess = acWriteOnly;

		return;
		}

	if( !strcmp(Cmd.m_pWrite, "\0") ) {

		Cmd.m_bAccess = acReadOnly;

		return;
		}

	Cmd.m_bAccess = acReadWrite;
	}

void CAdam4000v2Driver::ClearWriteRetry(CCmd &Cmd)
{
	Cmd.m_WriteFailed = NOTHING;

	Cmd.m_fWriteRetry = FALSE;
	}

void CAdam4000v2Driver::SetWriteFailed(CCmd &Cmd, DWORD dwWrite)
{
	Cmd.m_WriteFailed = dwWrite;

	Cmd.m_fWriteRetry = TRUE;
	}

void CAdam4000v2Driver::ClearRetry(CCmd &Cmd)
{
	Cmd.m_fWriteRetry = FALSE;
	}

BOOL CAdam4000v2Driver::IsClearedWrite(CCmd &Cmd, DWORD dwWrite)
{
	if( Cmd.m_WriteFailed == dwWrite ) {

		return !Cmd.m_fWriteRetry;
		}

	return FALSE;
	}

// Frame Building
		
void CAdam4000v2Driver::Start(PCTXT pSyntax)
{
	m_uPtr	 = 0;

	m_bCheck = 0;

	memset(m_bTx, 0, sizeof(m_bTx));

	AddByte(pSyntax[0]);

	if( !IsBroadcast(pSyntax) ) {

		AddHex(m_pCtx->m_bDrop);
		}
	}

BOOL CAdam4000v2Driver::AddCmd(PCTXT pSyntax, BYTE bChan, BOOL fWrite)
{
	Start(pSyntax);

	if( !IsBroadcast(pSyntax) ) {

		char Cmd[32];

		memset(Cmd, 0, sizeof(Cmd));

		strcpy(Cmd, PTXT(pSyntax + 3));

		// Check for channel definition

		if( bChan < cNothing ) {

			if( !strcmp("#AAN", pSyntax) ) {

				SPrintf(Cmd, "%u", bChan);
				}
			else {

				PTXT ch[] = {"Cn", "Ci"};

				for( UINT u = 0; u < elements(ch); u++ ) {

					char * pFind = strchr(Cmd, ch[u][0]);

					if( pFind ) {

						if( pFind[1] == ch[u][1] ) {

							strcpy(pFind, "%u");

							SPrintf(Cmd, Cmd, bChan);

							break;
							}
						}
					}
				}
			}

		// Check for data definition

		if( fWrite ) {

			UINT uFind = 0;

			if( IsConfig(pSyntax, TRUE) ) {

				return TRUE;
				}
		
			while( isalnum(Cmd[uFind]) ) {

				if( !isdigit(Cmd[uFind])  ) {

					if( !isupper(Cmd[uFind]) || Cmd[uFind] == 'T' ) {

						Cmd[uFind] = '\0';

						break;
						}
					}

				uFind++;
				}
			}

		AddText(Cmd);

		return TRUE;
		}

	AddText(PCTXT(pSyntax + 1));

	return TRUE;
	}

void CAdam4000v2Driver::AddData(BYTE bFormat, BYTE bForm, BYTE bType, PDWORD pData)
{
	if( IsCmd(bForm) ) {

		return;
		}

	if( IsFormatted(bFormat) ) {

		AddFormattedData(bForm, bType, pData);

		return;
		}

	if( IsReal(bType) ) {

		AddRealData(bForm, pData);

		return;
		}

	if ( IsHex(bForm) ) {

		AddHexData(bFormat, bType, pData);

		return;
		}

	if ( IsText(bForm) ) {

		AddTextData(bType, pData);

		return;
		}

	AddIntegerData(bType, bForm, pData);
	}

BOOL CAdam4000v2Driver::AddFormattedData(BYTE bForm, BYTE bType, PDWORD pData)
{
	BOOL fSign   = FALSE;

	UINT uDigits = 0;

	UINT uPrec   = 0;

	if( GetFormatDetails(bForm, bType, fSign, uDigits, uPrec) ) {

		BOOL fHex    = !IsReal(bType);

		PTXT pDigits = GetFormat(fHex, uDigits);

		PTXT pPrec   = GetFormat(uPrec);

		if( fHex ) {

			if( pDigits ) {
			
				AddHexData(pDigits, pData);

				return TRUE;
				}

			return FALSE;
			}

		if( pDigits && pPrec ) {
		
			AddRealData(pDigits, pPrec, pData, fSign);
			
			return TRUE;
			}
		}

	return FALSE;
	}

void CAdam4000v2Driver::AddHexData(PTXT pFormat, PDWORD pData)
{
	char Data[32];

	memset(Data, 0, sizeof(Data));

	SPrintf(Data, pFormat, pData[0]);

	AddText(PCTXT(Data));
	}

void CAdam4000v2Driver::AddHexData(BYTE bFormat, BYTE bType, PDWORD pData)
{
	char Data[32];

	memset(Data, 0, sizeof(Data));

	if( bFormat & misc3Bytes ) {

		SPrintf(Data, "%6.6X", pData[0]);
		}

	else if( bType == addrBitAsBit ) {

		SPrintf(Data, "%1.1X", pData[0]);
		}

	else if( bType == addrByteAsByte ) {

		SPrintf(Data, "%2.2X", pData[0]); 
		}

	else if ( bType == addrWordAsWord ) {

		SPrintf(Data, "%4.4X", pData[0]);
		}

	else {  
		SPrintf(Data, "%8.8X", pData[0]); 
		}

	AddText(PCTXT(Data));
	}

void  CAdam4000v2Driver::AddRealData(PTXT pDigits, PTXT pPrec, PDWORD pData, BOOL fSign)
{
	HostAlignStack();

	char Data[32], Work[20], Digit[8], Prec[8];

	memset(Data, 0, sizeof(Data));

	memset(Work, 0, sizeof(Work));

	memset(Digit, 0, sizeof(Digit));

	memset(Prec, 0, sizeof(Prec));

	SPrintf(Work, "%f", PassFloat(pData[0]));

	int nNum     = ATOI(Work);
		
	char * pDec  = strchr(Work, '.');

	if( fSign && nNum >= 0 ) {

		strcat(Data, "+");
		}

	SPrintf(Digit, pDigits, nNum);

	SPrintf(Prec,  pPrec,   pDec);

	strcat(Data, Digit);

	strcat(Data, Prec);

	AddText(PCTXT(Data));
	}

void  CAdam4000v2Driver::AddRealData(BYTE bForm, PDWORD pData)
{
	HostAlignStack();

	char Data[7], Work[20];

	memset(Data, 0, sizeof(Data));

	memset(Work, 0, sizeof(Work));

	SPrintf(Work, "%f", PassFloat(pData[0]));

	char * pDec = strchr(Work, '.');

	memcpy(Data, Work, pDec ? bForm : bForm + 1);

	AddText(PCTXT(Data));
	}

void CAdam4000v2Driver::AddIntegerData(BYTE bType, BYTE bForm, PDWORD pData)
{
	char Data[16];

	memset(Data, 0, sizeof(Data));

	UINT uMask = GetDigitMask(bForm);

	if( uMask < NOTHING ) {

		switch( uMask ) {

			case 1:	SPrintf(Data, "%1.1u", pData[0]);	break;
			case 2:	SPrintf(Data, "%2.2u", pData[0]);	break;
			case 3:	SPrintf(Data, "%3.3u", pData[0]);	break;
			case 4:	SPrintf(Data, "%4.4u", pData[0]);	break;
			case 5:	SPrintf(Data, "%5.5u", pData[0]);	break;
			case 6:	SPrintf(Data, "%6.6u", pData[0]);	break;
			case 7:	SPrintf(Data, "%7.7u", pData[0]);	break;
			case 8:	SPrintf(Data, "%8.8u", pData[0]);	break;
			case 9:	SPrintf(Data, "%9.9u", pData[0]);	break;
			}
		}
	else {  
		if( bType == addrBitAsBit ) {

			SPrintf(Data, "%1.1u", pData[0]);
			}

		else if( bType == addrByteAsByte ) {

			SPrintf(Data, "%3.3d", pData[0]); 
			}

		else if ( bType == addrWordAsWord  ) {

			SPrintf(Data, "%5.5d", pData[0]);
			}
		else {	
			SPrintf(Data, "%d", pData[0]);
			}
		}

	AddText(PCTXT(Data));
	}

void CAdam4000v2Driver::AddTextData(BYTE bType, PDWORD pData)
{
	char Data[32];

	memset(Data, 0, sizeof(Data));

	if( bType == addrByteAsByte ) {

		SPrintf(Data, "%c", pData[0]);
		}
	else {
		strcpy(Data, PTXT(pData));
		}

	AddText(PCTXT(Data));
	}

void CAdam4000v2Driver::Finish(PCTXT pSyntax)
{
	if( m_pCtx->m_fCheck ) {
		
		AddHex(m_bCheck);
		}

	if( !IsBroadcast(pSyntax) ) {

		AddByte(CR);
		}
	}

void CAdam4000v2Driver::AddByte(BYTE bByte)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr++] = bByte;

		m_bCheck += bByte;
		}
	}

void CAdam4000v2Driver::AddHex(BYTE bByte)
{
	AddByte(m_pHex[(bByte / 16) & 0x0F]);

	AddByte(m_pHex[(bByte % 16) & 0x0F]);
	}

void CAdam4000v2Driver::AddText(PCTXT pText)
{
	PCTXT pAdd = pText;

	while( pAdd[0] ) {

		AddByte(pAdd[0]);

		pAdd++;
		}
	}

// Transport

BOOL CAdam4000v2Driver::Transact(PCTXT pSyntax, BYTE bMisc)
{
	Finish(pSyntax);

	if( Send() ) {

		if( Recv(pSyntax) ) {

			return Check(pSyntax, bMisc);
			}
		}

	return FALSE;
	}

BOOL CAdam4000v2Driver::Send(void)
{
	m_pData->ClearRx();

	m_pData->Write(m_bTx, m_uPtr, FOREVER);	 

	return TRUE;
	}

BOOL CAdam4000v2Driver::Recv(PCTXT pSyntax)
{
	if( IsBroadcast(pSyntax) ) {

		while( m_pData->Read(0) < NOTHING );
		
		Sleep(20);

		return TRUE;
		}

	UINT uTimer = 0;

	UINT uData = 0;

	BYTE bCheck = 0;

	m_uPtr = 0;

	memset(m_bRx, 0, sizeof(m_bRx));

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			Sleep(5);

			continue;
			}

		if( uData == CR ) {

			return Validate(bCheck - (m_bRx[m_uPtr - 1] + m_bRx[m_uPtr - 2]));
			}

		bCheck += (uData & 0xFF);

		m_bRx[m_uPtr] = uData;

		m_uPtr++;

		if( m_uPtr >= sizeof(m_bRx) ) {

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CAdam4000v2Driver::Check(PCTXT pSyntax, BYTE bMisc)
{
	if( IsBroadcast(pSyntax) ) {

		return TRUE;
		}

	if( m_bRx[0] == '?' ) {

		return FALSE;
		}

	if( m_bRx[0] == '>' ) {

		strcpy((PTXT)m_bRx, PTXT(m_bRx + 1));

		return TRUE;
		}

	if( bMisc & miscMalResp ) {

		if( m_bRx[0] == '!' ) {

			strcpy((PTXT)m_bRx, PTXT(m_bRx + 1));
			
			return TRUE;
			}
		}

	UINT uShift = IsConfig(pSyntax, TRUE) ? 3 : 1;

	if( !memcmp(PBYTE(m_bTx + uShift), m_bRx + 1, 2) ) {

		uShift = IsConfig(pSyntax, FALSE) ? 1 : 3;

		strcpy((PTXT)m_bRx, PTXT(m_bRx + uShift));

		if( uShift > 1 ) {

			m_uPtr -= 2;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CAdam4000v2Driver::Validate(BYTE bCheck)
{
	if( !m_pCtx->m_fCheck ) {

		return TRUE;
		}
	
	BYTE bCheckReply = 0;
	
	bCheckReply += FromASCII(m_bRx[m_uPtr - 2]) << 4;

	bCheckReply += FromASCII(m_bRx[m_uPtr - 1]) << 0;

	m_uPtr -= 2;

	return (bCheck == bCheckReply);
	}

// Helpers

BYTE CAdam4000v2Driver::FromASCII(BYTE bByte)
{
	if( bByte >= '0' && bByte <= '9' ) {

		return bByte - '0';
		}

	return	bByte - '@' + 9;
	}

void CAdam4000v2Driver::SetCheck(DWORD dwData)
{
	m_pCtx->m_fCheck = (dwData & 0x40) ? TRUE : FALSE;
	}

PCTXT CAdam4000v2Driver::GetConfigReadCmd(void)
{
	return PCTXT("$AA2\0");
	}

PCTXT CAdam4000v2Driver::GetConfigWriteCmd(void)
{
	return PCTXT("%AANNTTCCFF\0");
	}

BOOL CAdam4000v2Driver::IsTimedOut(UINT uTime, UINT uSpan)
{
	return int(/*m_pHelper->*/GetTickCount() - uTime - /*m_pHelper->*/ToTicks(uSpan)) >= 0;
	}

DWORD CAdam4000v2Driver::GetRange(void)
{
	if( m_pCtx ) {

		return (m_pCtx->m_Config >> 16) & 0xFF;
		}

	return NOTHING;	
	}

DWORD CAdam4000v2Driver::GetFormat(void)
{
	if( m_pCtx ) {

		return m_pCtx->m_Config & 0x3;
		}

	return NOTHING;
	}

BOOL CAdam4000v2Driver::IsFormatted(BYTE bFormat)
{
	if( bFormat & miscInpForm ) {
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CAdam4000v2Driver::GetFormatDetails(BYTE bForm, BYTE bType, BOOL &fSign, UINT &uDigits, UINT &uPrec)
{
	UINT uRange  = GetRange();

	if( uRange < NOTHING ) {

		UINT uFormat = GetFormat();

		if( uFormat < NOTHING ) {

			return GetFormatDetails(uRange, uFormat, bType, fSign, uDigits, uPrec);
			}
		}

	return FALSE;
	}

BOOL CAdam4000v2Driver::GetFormatDetails(UINT uRange, UINT uFormat, BYTE bType, BOOL &fSign, UINT &uDigits, UINT &uPrec)
{
	if( uFormat == dataFsr || uFormat == dataOhm ) {

		fSign   = TRUE;

		uDigits = 3;

		uPrec   = 3;

		return TRUE;
		}

	if( !IsReal(bType) ) {

		fSign   = FALSE;

		uDigits = uRange >= 0x30 && uRange <= 0x32 ? 3 : 4;

		return TRUE;
		}

	switch( uRange ) {

		case 0x00:
		case 0x01:	fSign = TRUE;	uDigits = 2;	uPrec = 3;	return TRUE;

		case 0x02:
		case 0x03:
		case 0x04:	fSign = TRUE;	uDigits = 3;	uPrec = 2;	return TRUE;

		case 0x05:	fSign = TRUE;	uDigits = 1;	uPrec = 4;	return TRUE;

		case 0x06:
		case 0x07:
		case 0x08:	fSign = TRUE;	uDigits = 2;	uPrec = 3;	return TRUE;

		case 0x09:
		case 0x0A:	fSign = TRUE;	uDigits = 1;	uPrec = 4;	return TRUE;

		case 0x0B:
		case 0x0C:	fSign = TRUE;	uDigits = 3;	uPrec = 2;	return TRUE;

		case 0x0D:
		case 0x0E:	fSign = TRUE;	uDigits = 2;	uPrec = 3;	return TRUE;

		case 0x0F:	fSign = TRUE;	uDigits = 4;	uPrec = 1;	return TRUE;

		case 0x10:	fSign = TRUE;	uDigits = 3;	uPrec = 2;	return TRUE;

		case 0x11:
		case 0x12:
		case 0x13:
		case 0x14:	fSign = TRUE;	uDigits = 4;	uPrec = 1;	return TRUE;

		case 0x20:
		case 0x21:
		case 0x22:
		case 0x23:
		case 0x24:
		case 0x25:
		case 0x26:
		case 0x27:
		case 0x28:
		case 0x29:	fSign = TRUE;	uDigits = 3;	uPrec = 2;	return TRUE;

		case 0x30:
		case 0x31:
		case 0x32:	fSign = FALSE;	uDigits = 2;	uPrec = 3;	return TRUE;

		}

	return FALSE;
	}

PTXT CAdam4000v2Driver::GetFormat(BOOL fHex, UINT uDigits)
{
	if( fHex ) {

		switch( uDigits ) {

			case 1:	return PTXT("%1X");
			case 2: return PTXT("%2X");
			case 3: return PTXT("%3X");
			case 4: return PTXT("%4X");
			}
		
		return NULL;
		}

	switch( uDigits ) {

		case 1:	return PTXT("%1.1d");
		case 2: return PTXT("%2.2d");
		case 3: return PTXT("%3.3d");
		case 4: return PTXT("%4.4d");
		}

	return NULL;
	}

PTXT CAdam4000v2Driver::GetFormat(UINT uPrec)
{
	switch( uPrec ) {

		case 1:	return PTXT("%2.2s");
		case 2: return PTXT("%3.3s");
		case 3: return PTXT("%4.4s");
		case 4: return PTXT("%5.5s");
		}

	return NULL;
	}

// End of File
