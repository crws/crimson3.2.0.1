
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Command Structure
//

struct CCmd
{
	PCTXT	m_pRead;
	PCTXT   m_pWrite;
	BYTE    m_bChan;
	DWORD   m_Ref;
	BYTE    m_bAccess;
	BYTE    m_bForm;
	DWORD   m_WriteFailed;
	BOOL	m_fWriteRetry;
	BYTE    m_bMisc;
	};

//////////////////////////////////////////////////////////////////////////
//
// Enumerations
//

enum access {

	acReadOnly	= 1,
	acWriteOnly	= 2,
	acReadWrite	= 3,
	};

enum cfg {

	cNothing	= 255,
	cRead		= 0,
	cWrite		= 1,
	};

enum form {
	formInt	    = 0,
	formReal    = 1,
	formHex	    = 2,
	formText    = 3,
	formCmd     = 4,
	formDigitM  = 0x10,
	};

enum data {

	dataEng	    = 0,
	dataFsr	    = 1,
	dataHex	    = 2,
	dataOhm	    = 3,
	};

enum misc {

	miscInpForm = 1,
	miscMalResp = 2,
	misc3Bytes  = 4,
	};
		
//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series v2 Module Master Serial Driver
//

class CAdam4000v2Driver : public CMasterDriver
{
	public:
		// Constructor
		CAdam4000v2Driver(void);

		// Destructor
		~CAdam4000v2Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			UINT    m_uCmds;
			CCmd *  m_pCmds;
			BOOL    m_fCheck;
			DWORD   m_Config;
			};

		// Data Members
		
		PCTXT		m_pHex;
		BYTE		m_bTx[64];
		BYTE		m_bRx[64];
		BYTE		m_bCheck;
		UINT		m_uPtr;
		CContext *	m_pCtx;

		// Implementation

		CCODE ReadConfig(PDWORD pData);
		CCODE WriteConfig(PDWORD pData);
		CCODE GetData(BYTE bType, BYTE bForm, PDWORD pData, UINT uCount = 1);
		CCODE GetHex(PDWORD pData);
		CCODE GetReal(PDWORD pData);
		CCODE GetInteger(PDWORD pData);
		CCODE GetText(PDWORD pData, UINT uCount);	

		CCmd *	FindCmd(DWORD dwRef);
		BOOL	IsConfig(PCTXT pSyntax, BOOL fWrite);
		BOOL	IsReadOnly(CCmd * pCmd);
		BOOL	IsWriteOnly(CCmd * pCmd);
		BOOL	IsBroadcast(PCTXT pSyntax);
		BOOL	IsCmd(BYTE bForm);
		BOOL	IsText(BYTE bForm);
		BOOL	IsHex(BYTE bForm);
		BOOL	IsReal(BYTE bType);
		DWORD   GetDigitMask(BYTE bForm);
		void	SetAccess(CCmd &Cmd);

		// Write Recovery Mechanism
		void	ClearWriteRetry(CCmd &Cmd);
		void	SetWriteFailed(CCmd &Cmd, DWORD dwWrite);
		void    ClearRetry(CCmd &Cmd);
		BOOL    IsClearedWrite(CCmd &Cmd, DWORD dwWrite);		
		
		// Frame Building
		void Start(PCTXT pSyntax);
		BOOL AddCmd(PCTXT pSyntax, BYTE bChan, BOOL fWrite);
		void AddData(BYTE bFormat, BYTE bForm, BYTE bType, PDWORD pData);
		BOOL AddFormattedData(BYTE bForm, BYTE bType, PDWORD pData);
		void AddHexData(PTXT pFormat, PDWORD pData);
		void AddHexData(BYTE bFormat, BYTE bType, PDWORD pData);
		void AddRealData(PTXT pDigits, PTXT pPrec, PDWORD pData, BOOL fSign);
		void AddRealData(BYTE bForm, PDWORD pData);
		void AddIntegerData(BYTE bType, BYTE bForm, PDWORD pData);
		void AddTextData(BYTE bType, PDWORD pData);
		void Finish(PCTXT pSyntax);
		void AddByte(BYTE bByte);
		void AddHex(BYTE bByte);
		void AddText(PCTXT pText);

		// Transport
		BOOL Transact(PCTXT pSyntax, BYTE bMisc);
		BOOL Send(void);
		BOOL Recv(PCTXT pSyntax);
		BOOL Check(PCTXT pSyntax, BYTE bMisc);
		BOOL Validate(BYTE bCheck);

		// Helpers
		BYTE FromASCII(BYTE bByte);
		void SetCheck(DWORD dwData);
		PCTXT GetConfigReadCmd(void);
		PCTXT GetConfigWriteCmd(void);
		BOOL  IsTimedOut(UINT uTime, UINT uSpan);
		DWORD GetRange(void);
		DWORD GetFormat(void);
		BOOL  IsFormatted(BYTE bFormat);
		BOOL  GetFormatDetails(BYTE bForm, BYTE bType, BOOL &fSign, UINT &uDigits, UINT &uPrec);
		BOOL  GetFormatDetails(UINT uRange, UINT uFormat, BYTE bType, BOOL &fSign, UINT &uDigits, UINT &uPrec);
		PTXT  GetFormat(BOOL fHex, UINT uDigits);
		PTXT  GetFormat(UINT uPrec);
	};
