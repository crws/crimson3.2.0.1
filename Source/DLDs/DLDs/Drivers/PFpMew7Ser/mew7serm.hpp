#include "mew7base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 PLC MEWTOCOL7 Serial Master Driver
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

class CPanFpMewtocol7SerialMasterDriver : public CPanFpMewtocol7BaseMasterDriver
{
	public:
		// Constructor
		CPanFpMewtocol7SerialMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
	protected:
			
		// Implementation
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		
		// Overridables
		BOOL Transact(void);
		};
		

// End of File

