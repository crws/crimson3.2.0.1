
#include "mew7serm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 PLC MEWTOCOL7 Serial Master Driver
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CPanFpMewtocol7SerialMasterDriver);

// Constructor

CPanFpMewtocol7SerialMasterDriver::CPanFpMewtocol7SerialMasterDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Configuration

void MCALL CPanFpMewtocol7SerialMasterDriver::Load(LPCBYTE pData)
{
	}

void MCALL CPanFpMewtocol7SerialMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CPanFpMewtocol7SerialMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	m_pData->SetRxSize(m_uRxSize);

	m_pData->SetTxSize(m_uTxSize);

	pPort->Bind(m_pData);
	}

void MCALL CPanFpMewtocol7SerialMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CPanFpMewtocol7SerialMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pBase = (CBaseCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pBase = new CBaseCtx;

			MakeMap(pData);

			m_pBase->m_dwStation = GetWord(pData);

			InitErrorCode();

			InitErrorRequest();

			pDevice->SetContext(m_pBase);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;

	}

CCODE MCALL CPanFpMewtocol7SerialMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		DeleteMap();

		delete m_pBase;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}


// Implementation

BOOL CPanFpMewtocol7SerialMasterDriver::SendFrame(void)
{
	m_pData->ClearRx();

	m_pData->Write(m_pTx, m_uPtr, FOREVER);

	return TRUE; 
	}

BOOL CPanFpMewtocol7SerialMasterDriver::RecvFrame(void)
{
	UINT uPtr   = 0;

	UINT uTimer = 0;

	BOOL fBegin = FALSE;

	UINT uByte  = 0;

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uByte = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		if( !fBegin ) {

			fBegin = ( uByte == '>' );
			}

		if( fBegin ) {

			if( uByte == CR ) {

				if( CheckFrame(uPtr - 4) ) {
						
					return TRUE;
					}
					
				return FALSE;
				}
				
			m_pRx[uPtr++] = uByte;
				
			if( uPtr == m_uRxSize ) {
					
				return FALSE;
				}
			}
		}
		
	return FALSE;
	}

// Overidables

BOOL CPanFpMewtocol7SerialMasterDriver::Transact(void)
{      
	if( SendFrame() ) {
		
		return RecvFrame();
		}

	return FALSE;
	}

// End of File
