
//////////////////////////////////////////////////////////////////////////
//
// Animatics Driver
//

#define ANIMATICS_ID 0x3355

// Data type controls <O>=<offset>, <D>=<data>
// Class 4
#define	TRO	0	// W = none		R=data
#define	TWO	1	// W = CMD no data	R=0xFFFFFFFF
#define	TRW	2	// W = CMD=<D>		R=data
#define	TGS	3	// W = CMD<string #>	R=0xFFFFFFFF
#define	TGN	4	// W = CMD<subr #>	R=0xFFFFFFFF
#define	TGC	5	// W = none/clear	R=cached subr response
#define	TVS	6	// W = CMD(<D>)		R=cached
#define	TGR	7	//			R=cached subr resp #
#define	TAA	8	// W = CMD<new drop>	R=drop
// Class 5
#define	TPO	9	// W = CMD(<O>)		R=0		 // no data
#define	TPD	10	// W = CMD(<D>)		R=cached	 // no data
#define	TDC	11	// W = CMD no data	R=0
#define	TAR	12	// W = CMD[<O>]		R=CMD[<O>]	 // add data
#define	TCC	13	// C4 Selection with Change of command
#define	TPR	14	// W = none		R=CMD(<O>)
#define	TPC	15	// W = CMDA(<O>)	R=CMDB(<O>)	 // add data
#define	TPX	16	// W = CMD(<O>=<data>	R=0xFFFFFFFF
#define	TWX	17	// W = CMD=<D>		R=0xFFFFFFFF

// Maximum real size
#define	SZRMAX	12

// Look-up
struct FAR ANIMATICSCmdDef {
	char 	sName[12];
	UINT	uID;
	UINT	Type;
	};

class CAnimaticsDriver : public CMasterDriver
{
	public:
		// Constructor
		CAnimaticsDriver(void);

		// Destructor
		~CAnimaticsDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_bClass;
			DWORD	m_dStringPending;
			DWORD	m_dCurrentResponse;
			BOOL	m_fHasSubrResponse;
			DWORD	m_dVoltageSetting[7];
			// Caches
			DWORD	m_dResp[16];
			DWORD	m_dMFA;
			DWORD	m_dMFD;
			DWORD	m_dMFS;
			DWORD	m_dOSH;
			};
		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		WORD	m_uPtr;
		DWORD	m_Error;
		BOOL	m_fIs485Port;
		

		static	ANIMATICSCmdDef CODE_SEG CL[];
		ANIMATICSCmdDef FAR * m_pCL;
		ANIMATICSCmdDef FAR * m_pItem;
		ANIMATICSCmdDef	m_CmdDef;

		// Hex Lookup
		LPCTXT	m_pHex;

		// Opcode Handlers
		BOOL	ReadOne(CAddress Addr, PDWORD pData);
		BOOL	Read(AREF Addr);
		UINT	Write(AREF Addr, PDWORD pData);
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData);
		void	AddSignedData(DWORD dData);
		void	AddParam(DWORD dPar1);
		void	AddCommand(AREF Addr, DWORD dData);
		void	PutText(LPCTXT pCmd);
		void	PutReal(DWORD dData);
		
		// Transport Layer
		BOOL	Transact(BOOL fWantReply);
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	IsNumericChar(BYTE bData, BOOL fFirst);
		void	AddToString(UINT *pCheck, PDWORD pStr, BYTE bData, BOOL fInit);
		BOOL	GetResponse(PDWORD pData, UINT uType);
		
		// Port Access
		void	Put(void);
		WORD	Get(void);

		// Helpers
		BOOL	SetpItem( AREF Addr );
		DWORD	FixClass( AREF Addr );
		BOOL	ScanCmdDef(UINT uID);
		BOOL	IsClass5( void );
		BOOL	NoReadTransmit(UINT uOffset, PDWORD pData, UINT uCount );
		BOOL	NoWriteTransmit(UINT uOffset, DWORD dData );
		BOOL	ReadCached(UINT uOffset, PDWORD pData, UINT uCount);
		void	WriteCached(UINT uOffset, DWORD dData);
		void	InitCached(void);
		void	InitStrings(void);
		BOOL	SendAnimWake(BOOL fAll);
	};

#define	CMP		  1	// TRW
#define	CMV		  2	// TRW
#define	CMD		  3	// TRW
#define	CMBt		  4	// TRO
#define	CMBe		  5	// TRO
#define	CMBo		  6	// TRO
#define	CMI		  7	// TRO
#define	CMPE		  8	// TRO
#define	CME		  9	// TRW
#define	CMA		 10	// TRW
#define	CMUGC		 11	// TWO
#define	CMRUN		 12	// TWO
#define	CMZ		 13	// TWO
#define	CMW		 14	// TRO
#define	CMT		 15	// TRW
#define	CMRSB		 16	// TRO
#define	CMAMPS		 17	// TRW
#define	CMCLK		 18	// TRW
#define	CMCTR		 19	// TRO
#define	CMSLEEP		 20	// TWO
#define	CMSLEEP1	 21	// TWO
#define	CMWAKE		 22	// TWO
#define	CMWAKE1		 23	// TWO
#define	CMECHO		 24	// TWO
#define	CMECHO_OFF	 25	// TWO
#define	CMSILENT1	 26	// TWO
#define	CMTALK1		 27	// TWO
#define	CMG		 28	// TWO
#define	CMS		 29	// TWO
#define	CMX		 30	// TWO
#define	CMMF0		 31	// TWO
#define	CMMF1		 32	// TWO
#define	CMMF2		 33	// TWO
#define	CMMF4		 34	// TWO
#define	CMMFR		 35	// TWO
#define	CMMS		 36	// TWO
#define	CMMSR		 37	// TWO
#define	CMMS0		 38	// TWO
#define	CMMP		 39	// TWO
#define	CMMV		 40	// TWO
#define	CMMT		 41	// TWO
#define	CMUAI		 42	// TWO
#define	CMUBI		 43	// TWO
#define	CMUCI		 44	// TWO
#define	CMUDI		 45	// TWO
#define	CMUEI		 46	// TWO
#define	CMUFI		 47	// TWO
#define	CMUGI		 48	// TWO
#define	CMUAO		 49	// TWO
#define	CMUBO		 50	// TWO
#define	CMUCO		 51	// TWO
#define	CMUDO		 52	// TWO
#define	CMUEO		 53	// TWO
#define	CMUFO		 54	// TWO
#define	CMUGO		 55	// TWO
#define	CMUCP		 56	// TWO
#define	CMUDM		 57	// TWO
#define	CMUA		 58	// TVS
#define	CMUB		 59	// TVS
#define	CMUC		 60	// TVS
#define	CMUD		 61	// TVS
#define	CMUE		 62	// TVS
#define	CMUF		 63	// TVS
#define	CMUG		 64	// TVS
#define	CMMFMUL		 65	// TRW
#define	CMMFDIV		 66	// TRW
#define	CMKA		 67	// TRW
#define	CMKD		 68	// TRW
#define	CMKG		 69	// TRW
#define	CMKI		 70	// TRW
#define	CMKL		 71	// TRW
#define	CMKP		 72	// TRW
#define	CMKS		 73	// TRW
#define	CMKV		 74	// TRW
#define	CMF		 75	// TWO
#define	CMOFF		 76	// TWO
#define	CMBRKENG	 77	// TWO
#define	CMBRKRLS	 78	// TWO
#define	CMBRKSRV	 79	// TWO
#define	CMBw		 80	// TWO
#define	CMZa		 81	// TWO
#define	CMZb		 82	// TWO
#define	CMZc		 83	// TWO
#define	CMZd		 84	// TWO
#define	CMZf		 85	// TWO
#define	CMZl		 86	// TWO
#define	CMZr		 87	// TWO
#define	CMZs		 88	// TWO
#define	CMZu		 89	// TWO
#define	CMZw		 90	// TWO
#define	CMZS		 91	// TWO
#define	CMSTACK		 92	// TWO
#define	CMEND		 93	// TWO
#define	CMa		 94	// TRW
#define	CMb		 95	// TRW
#define	CMc		 96	// TRW
#define	CMd		 97	// TRW
#define	CMe		 98	// TRW
#define	CMf		 99	// TRW
#define	CMg		100	// TRW
#define	CMh		101	// TRW
#define	CMi		102	// TRW
#define	CMj		103	// TRW
#define	CMk		104	// TRW
#define	CMl		105	// TRW
#define	CMm		106	// TRW
#define	CMn		107	// TRW
#define	CMo		108	// TRW
#define	CMp		109	// TRW
#define	CMq		110	// TRW
#define	CMr		111	// TRW
#define	CMs		112	// TRW
#define	CMt		113	// TRW
#define	CMu		114	// TRW
#define	CMv		115	// TRW
#define	CMw		116	// TRW
#define	CMx		117	// TRW
#define	CMy		118	// TRW
#define	CMz		119	// TRW
#define	CMaa		120	// TRW
#define	CMbb		121	// TRW
#define	CMcc		122	// TRW
#define	CMdd		123	// TRW
#define	CMee		124	// TRW
#define	CMff		125	// TRW
#define	CMgg		126	// TRW
#define	CMhh		127	// TRW
#define	CMii		128	// TRW
#define	CMjj		129	// TRW
#define	CMkk		130	// TRW
#define	CMll		131	// TRW
#define	CMmm		132	// TRW
#define	CMnn		133	// TRW
#define	CMoo		134	// TRW
#define	CMpp		135	// TRW
#define	CMqq		136	// TRW
#define	CMrr		137	// TRW
#define	CMss		138	// TRW
#define	CMtt		139	// TRW
#define	CMuu		140	// TRW
#define	CMvv		141	// TRW
#define	CMww		142	// TRW
#define	CMxx		143	// TRW
#define	CMyy		144	// TRW
#define	CMzz		145	// TRW
#define	CMaaa		146	// TRW
#define	CMbbb		147	// TRW
#define	CMccc		148	// TRW
#define	CMddd		149	// TRW
#define	CMeee		150	// TRW
#define	CMfff		151	// TRW
#define	CMggg		152	// TRW
#define	CMhhh		153	// TRW
#define	CMiii		154	// TRW
#define	CMjjj		155	// TRW
#define	CMkkk		156	// TRW
#define	CMlll		157	// TRW
#define	CMmmm		158	// TRW
#define	CMnnn		159	// TRW
#define	CMooo		160	// TRW
#define	CMppp		161	// TRW
#define	CMqqq		162	// TRW
#define	CMrrr		163	// TRW
#define	CMsss		164	// TRW
#define	CMttt		165	// TRW
#define	CMuuu		166	// TRW
#define	CMvvv		167	// TRW
#define	CMwww		168	// TRW
#define	CMxxx		169	// TRW
#define	CMyyy		170	// TRW
#define	CMzzz		171	// TRW
#define	CMO		172	// TRW
#define	CMGOSUB		173	// TGN
#define	CMSUBRS		174	// TGS
#define	CMSUBRR		175	// TGR
#define	CMSUBR1		176	// TGC
#define	CMSUBR2		177	// TGC
#define	CMSUBR3		178	// TGC
#define	CMSUBR4		179	// TGC
#define	CMSUBR5		180	// TGC
#define	CMSUBR6		181	// TGC
#define	CMSUBR7		182	// TGC
#define	CMSUBR8		183	// TGC
#define	CMSUBR9		184	// TGC
#define	CMSUBR10	185	// TGC
#define	CMSUBR11	186	// TGC
#define	CMSUBR12	187	// TGC
#define	CMSUBR13	188	// TGC
#define	CMSUBR14	189	// TGC
#define	CMSUBR15	190	// TGC
#define	CMSUBR16	191	// TGC
#define	CMSADDR		192	// TAA
#define	LASTCLASS4	192

// Class 5 commands
//#define	CM5AA		193	// TRO obsoleted by Animatics
#define	CM5AT		194	// TRW
#define	CM5DEA		195	// TRW
#define	CM5DEL		196	// TRW
#define	CM5DT		197	// TRW
#define	CM5EILN		198	// TDC
#define	CM5EILP		199	// TDC
#define	CM5EISM		200	// TDC
#define	CM5MFA		201	// TPD
#define	CM5MFD		202	// TPD
#define	CM5MFS		203	// TPD
#define	CM5MFX		204	// TDC
#define	CM5MINV0	205	// TFF
#define	CM5MINV1	206	// TFF
#define	CM5OSH		207	// TPD
#define	CM5PA		208	// TRO
#define	CM5PC		209	// TRO
#define	CM5PRT		210	// TRW
#define	CM5SLD		211	// TFF
#define	CM5SLE		212	// TFF
#define	CM5SLM0		213	// TFF
#define	CM5SLM1		214	// TFF
#define	CM5SLN		215	// TRW
#define	CM5SLP		216	// TRW
#define	CM5VA		217	// TRO
#define	CM5PRA		218	// TRO
#define	CM5EA		219	// TRO

// following have table numbers
#define	CM5AB		221	// TAR
#define	CM5AF		222	// TAR
#define	CM5AL		223	// TAR
#define	CM5AW		224	// TAR
#define	CM5EIGN		225	// TPO
#define	CM5EOBK		226	// TPO
#define	CM5IO		227	// TPC - uses OR/OS for Write
#define	CM5INA		228	// TPR
#define	CM5INV		229	// TPR
#define	CM5IOW		230	// TPC - uses ORW/OSW for Write
#define	CM5W5		231	// TPR
#define	CM5Z5W		232	// TPD
#define	CM5OUT		233	// TDC
#define	CM5CTR		234	// TPR
#define	CM5GSTR		235	// GOSUB String Response
// Change of command between Read and Write
#define	CM5OR		241	// TPC
#define	CM5OS		242	// TPC
#define	CM5ORW		243	// TPD
#define	CM5OSW		244	// TPD
// Change of command vis-a-vis C4
#define	CMA5		255	// TRW C4 = A
#define	CME5		254	// TRW C4 = E
#define	CMP5		253	// TRW C4 = P
#define	CMV5		252	// TRW C4 = V		

// End of File
