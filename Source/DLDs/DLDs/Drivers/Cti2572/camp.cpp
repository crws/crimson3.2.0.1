#include "intern.hpp"

#include "camp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CTI CAMP Protocol Master Driver
//

// Constructor

CCampMasterDriver::CCampMasterDriver(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;

	m_ID   = 0xFF;
  	}

// Destructor

CCampMasterDriver::~CCampMasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CCampMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 1;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra	= 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CCampMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsError(Addr.a.m_Table) ) {

		pData[0] = m_pBase->m_Error;

		return uCount;
		}

	Begin();

	AddType(FALSE);

	AddErrChar();

	AddID();

	UINT uOffset = GetOffset(Addr.a.m_Offset, Addr.a.m_Table);

	AddClass(Addr.a.m_Table, Addr.a.m_Extra);

	AddOffset(Addr.a.m_Offset);

	AddAsciiByte(1);

	UINT Count = GetCount(uCount, Addr.a.m_Table, Addr.a.m_Type);

	BOOL fLong = IsLong(Addr.a.m_Type);
	
	if( fLong ) {

		Count *= 2;
		}

	MakeMin(Count, 120);

	AddCount(Count);

	End();

	if( Transact() ) {

		if( (m_bRxBuff[1] == 5) && (m_bRxBuff[2] != 0xF) ) {

			Count = GetRecvCount();

			UINT uData = FindDataOffset(Addr.a.m_Offset, Addr.a.m_Table);

			if( fLong ) {

				MakeMin(uCount, Count / 2);

				GetLongs(pData, uCount, uData);

				return uCount;
				}

			MakeMin(uCount, Count);

			GetWords(pData, uCount, uData);

			return uCount;
			}

		m_pBase->m_Error = GetError(Addr); 
		}
	
	return CCODE_ERROR;
	}

CCODE MCALL CCampMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsReadOnly(Addr.a.m_Table) ) {

		return uCount;
		}

	if( IsError(Addr.a.m_Table) ) {

		if( pData[0] == 0 ) {

			m_pBase->m_Error = 0;
			}

		return uCount;
		}
	
	PDWORD Data = NULL;

	if( NeedRead(Addr.a.m_Table) ) {

		Data = PDWORD(alloca(16 * 4));

		CAddress Drum;

		Drum.a.m_Table  = Addr.a.m_Table;
		Drum.a.m_Type   = Addr.a.m_Type;
		Drum.a.m_Extra  = Addr.a.m_Extra;
		Drum.a.m_Offset = GetOffset(Addr.a.m_Offset, Addr.a.m_Table);
		
		if( !COMMS_SUCCESS(Read(Drum, Data, IsLong(Addr.a.m_Type) ? 8 : 16)) ) {

			return CCODE_ERROR;
			}
		}
	
	Begin();

	AddType(TRUE);

	AddErrChar();

	AddID();

	UINT uOffset = GetOffset(Addr.a.m_Offset, Addr.a.m_Table);

	AddClass(Addr.a.m_Table, Addr.a.m_Extra);

	AddOffset(uOffset);

	UINT Count = GetCount(uCount, Addr.a.m_Table, Addr.a.m_Type);

	BOOL fLong = IsLong(Addr.a.m_Type);
	
	if( fLong ) {

		Count *= 2;
		}

	MakeMin(Count, 120);

	AddAsciiByte(Count);

	MakeMin(uCount, fLong ? Count / 2 : Count);

	Count = GetCount(uCount, Addr.a.m_Table, Addr.a.m_Type);

	uOffset = (Addr.a.m_Offset % 16) - 1;

	for( UINT u = 0, i = 0; u < Count; u++ ) {

		BOOL fData = FALSE;
		
		if( fLong ) {

			if( Data ) {

				fData = ((u < uOffset / 2) || (u > (uOffset + uCount) / 2));
				}

			AddAsciiLong(fData ? Data[u] : pData[i]);

			if( !fData ) {

				i++;
				}

			continue;
			}

		if( Data ) {

			fData = ((u < uOffset) || u > (uOffset + uCount));
			}

		AddAsciiWord(fData ? Data[u] : pData[i]);

		if( !fData ) {

			i++;
			}
		}

	End();

	if( Transact() ) {

		if ( (m_bRxBuff[1] == 7) && (m_bRxBuff[2] != 0xF) ) {

			return uCount;
			}

		m_pBase->m_Error = GetError(Addr);
		}
	
	return CCODE_ERROR;
	}

// Implementation

void CCampMasterDriver::Begin(void)
{
	m_ID++;
	
	m_uPtr = 0;
	
	AddByte('[');
    
	m_wBCC = 0;
	}

void CCampMasterDriver::AddType(BOOL fWrite)
{
	AddAsciiByte(fWrite ? 6 : 4);
	}

void CCampMasterDriver::AddErrChar(void)
{
	AddByte('0');
	}

void CCampMasterDriver::AddID(void)
{
	AddAsciiChar(m_ID);
	}

void CCampMasterDriver::AddClass(UINT uTable, UINT uExtra)
{
	AddAsciiByte(uTable & 0xFF);

	AddAsciiByte(uExtra & 0xF);
	}

void CCampMasterDriver::AddOffset(UINT uOffset)
{
	AddAsciiWord(uOffset);
	}

void CCampMasterDriver::AddCount(UINT uCount)
{
	AddAsciiWord(uCount & 0xFFFF);
	}

void CCampMasterDriver::End(void)
{
	AddAsciiWord(m_wBCC);

	AddByte(']');
	}

void CCampMasterDriver::AddByte(BYTE bByte)
{
	m_bTxBuff[m_uPtr] = bByte;

	m_uPtr++;

	m_wBCC = m_wBCC + (bByte & 0x7F);
	}

void CCampMasterDriver::AddAsciiChar(BYTE bByte)
{
	AddByte(m_pHex[bByte % 0x10]);
	}

void CCampMasterDriver::AddAsciiByte(BYTE bByte)
{
	AddByte(m_pHex[bByte / 0x10]);
	
	AddByte(m_pHex[bByte % 0x10]);
	}

void CCampMasterDriver::AddAsciiWord(WORD wWord)
{
	AddAsciiByte(HIBYTE(wWord));

	AddAsciiByte(LOBYTE(wWord));
	}

void CCampMasterDriver::AddAsciiLong(DWORD dwLong)
{
	AddAsciiWord(HIWORD(dwLong));

	AddAsciiWord(LOWORD(dwLong));
	}

BYTE CCampMasterDriver::FromAscii(BYTE bByte)
{
	if( bByte >= '0' ) {
	
		if( bByte <= '9' ) {

			return bByte - '0';
			}

		return	bByte - '@' + 9;
		}

	return bByte;
	}

void CCampMasterDriver::BuffFromAscii(void)
{
	for( UINT u = 0;  u < m_uPtr; u++ ) {

		m_bRxBuff[u] = FromAscii(m_bRxBuff[u]);
		}
	}

BOOL CCampMasterDriver::CheckFrame(void)
{
	m_wBCC = 0;

	for( UINT u = 0; u < m_uPtr - 4; u++ ) {

		m_wBCC = m_wBCC + (m_bRxBuff[u] & 0x7F);
		}

	BuffFromAscii();

	return m_wBCC == GetData(m_bRxBuff + m_uPtr - 4, 4);
	}

UINT CCampMasterDriver::GetRecvCount(void)
{
	WORD x = PU2(m_bRxBuff + 12)[0];

	return LONG(SHORT(MotorToHost(x)));
	}

void CCampMasterDriver::GetWords(PDWORD pData, UINT uCount, UINT uData)
{
	for( UINT u = 0, o = uData; u < uCount; u++, o += 4 ) {
	
		pData[u] = GetData(m_bRxBuff + o, 4);
		}
	}

void CCampMasterDriver::GetLongs(PDWORD pData, UINT uCount, UINT uData)
{
	for( UINT u = 0, o = uData; u < uCount; u++, o += 8 ) {
	
		pData[u] = GetData(m_bRxBuff + o, 8);
		}
	}

// Helpers

BOOL CCampMasterDriver::IsLong(UINT uType)
{
	switch( uType ) {

		case addrLongAsLong:
		case addrWordAsLong:
		case addrWordAsReal:

			return TRUE;
		}
	
	return FALSE;
	} 

BOOL CCampMasterDriver::IsReadOnly(UINT uTable)
{
	switch( uTable ) {

		case 0x1B:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCampMasterDriver::IsError(UINT uTable)
{
	return (uTable == 0xF0);
	}

DWORD CCampMasterDriver::GetData(PBYTE pByte, UINT uCount)
{
	DWORD dData = 0;
	
	while( uCount-- ) {
	
		BYTE bByte = *(pByte++);

		dData = 16 * dData + bByte;
		}

	return dData;
	}

DWORD CCampMasterDriver::GetError(AREF Addr)
{
	DWORD dwErr = GetData(m_bRxBuff + 22, 4) & 0xFF;

	dwErr      |= Addr.a.m_Table << 24;

	dwErr	   |= Addr.a.m_Offset << 8;

	return dwErr;
	}

UINT CCampMasterDriver::GetOffset(UINT uOffset, UINT uTable)
{
	switch( uTable ) {

		case 0x12:

			return uOffset / 16 + 1;
		}

	return uOffset;
	}

UINT CCampMasterDriver::GetCount(UINT uCount, UINT uTable, UINT uType)
{
	switch( uTable ) {

		case 0x12:

			return (uType == addrWordAsWord) ? 16 : 8;
		}

	return uCount;
	}

UINT CCampMasterDriver::FindDataOffset(UINT uOffset, UINT uTable)
{
	switch( uTable ) {

		case 0x12:

			return 14 + ((uOffset % 16 - 1) * 4);
		}

	return 14;		
	}

BOOL CCampMasterDriver::NeedRead(UINT uTable)
{
	return (uTable == 0x12);
	}
	
	
// Transport

BOOL CCampMasterDriver::Transact(void)
{
	return FALSE;
	}

// End of File
