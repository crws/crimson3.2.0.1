
#include "dnp3ips.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Tcp Slave Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CDnp3IpSlave::CDnp3IpSlave(void)
{
	m_Ident   = DRIVER_ID;
}

// Destructor

CDnp3IpSlave::~CDnp3IpSlave(void)
{
}

// Device

CCODE MCALL CDnp3IpSlave::DeviceOpen(IDevice *pDevice)
{
	CDnp3Base::DeviceOpen(pDevice);

	if( !(m_pIpCtx = (CIpCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pIpCtx = new CIpCtx;

			m_pCtx = m_pIpCtx;

			m_pIpCtx->m_Dest     = GetWord(pData);

			m_pIpCtx->m_TO	     = GetWord(pData);

			m_pIpCtx->m_Link     = GetLong(pData);

			m_pIpCtx->m_eMask    = GetByte(pData);

			m_pCtx->m_pEvent     = NULL;

			GetEventConfig(pData);

			m_pCtx->m_bAiCalc    = GetByte(pData);

			m_pIpCtx->m_IP1      = GetAddr(pData);

			m_pIpCtx->m_TcpPort  = GetWord(pData);

			m_pIpCtx->m_UdpPort  = GetWord(pData);

			m_pIpCtx->m_uTime1   = GetWord(pData);

			m_pIpCtx->m_IP2      = GetAddr(pData);

			m_pIpCtx->m_pSession = NULL;

			m_pIpCtx->m_pChannel = NULL;

			IPADDR IP1;

			IPADDR IP2;

			memcpy(&IP1, &m_pIpCtx->m_IP1, sizeof(IPADDR));

			memcpy(&IP2, &m_pIpCtx->m_IP2, sizeof(IPADDR));

			m_pIpCtx->m_pConfig = m_pDnp->GetConfig(IP1,
								IP2,
								m_pIpCtx->m_TcpPort,
								m_pIpCtx->m_UdpPort,
								m_pIpCtx->m_uTime1);

			pDevice->SetContext(m_pIpCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	m_pCtx = m_pIpCtx;

	m_pChannel = m_pIpCtx->m_pChannel;

	return CCODE_SUCCESS;
}

CCODE MCALL CDnp3IpSlave::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete[] m_pCtx->m_pEvent;

		delete m_pIpCtx;

		m_pIpCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CDnp3Base::DeviceClose(fPersist);
}

// Channels

void CDnp3IpSlave::OpenChannel(void)
{
	if( m_pDnp ) {

		if( !m_pIpCtx->m_pChannel ) {

			if( (m_pChannel = m_pDnp->OpenChannel(m_pIpCtx->m_pConfig, m_Source, FALSE)) ) {

				m_fOpen = TRUE;

				m_pIpCtx->m_pChannel = m_pChannel;
			}
		}
	}
}

BOOL CDnp3IpSlave::CloseChannel(void)
{
	if( CDnp3Base::CloseChannel() ) {

		if( m_pDnp->CloseChannel(m_pIpCtx->m_pConfig, m_Source, FALSE) ) {

			m_pChannel = NULL;

			m_fOpen    = FALSE;

			m_pIpCtx->m_pChannel = m_pChannel;
		}
	}

	return !m_fOpen;
}

// End of File

