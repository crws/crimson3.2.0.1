
//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Base Slave
//

#ifndef M2MDATABASEINC
#define M2MDATABASEINC

#define	MAXIDQ	30
#define	MAXFIL	26

class CM2MDataBase : public CSlaveDriver
{
	public:
		// Constructor
		CM2MDataBase(void);

		// Destructor
		~CM2MDataBase(void);
		
	protected:
		// Socket Context
		struct SOCKET
		{
			ISocket	* m_pSocket;
			UINT	  m_uPtr;
			PBYTE	  m_pData;
			BOOL	  m_fBusy;
			UINT	  m_uTime;
			};

		struct	DEMHEAD
		{
			BOOL	fIsDEM;
			BYTE	bID;
			BYTE	bCtr;
			DWORD	dTime;
			};

		struct	CTLINFO
		{
			BOOL	fIsCTL;
			BYTE	bCtr;
			UINT	uSize;
			PWORD	pAddr;
			PWORD	pData;
			DWORD	dTime;
			};

		struct	FDAT
		{
			BYTE	bChunkNo;
			BYTE	bChunkQty;
			WORD	wSent;
			};

		struct	FCBLK // file control block
		{
			WORD	wFCount; // number of files to send (always 1)
			WORD	FSize;	 // size of file
			DWORD	FTime;	 // time stamp
			BYTE	UFI;	 // unique file identifier
			BYTE	bID;	 // current ID
			FDAT	*pDAT;	 // Chunk management
			BOOL	fBusy;	 // Transfer in progress
			UINT	FCount;  // Number of Files configured
			UINT	FCBSize; // Total size of FCB
			UINT	uTryCt;	 // Retry Count
			};

		// Data Members
		SOCKET	*m_pSock;

		DWORD	 m_ServerIP;
		DWORD	 m_dPollTime;

		DWORD	 m_dTZOffset;
		DWORD	 m_dLocalTime;
		DWORD	 m_dOldTrig[3];
		DWORD	 m_dTimeout;

		UINT	 m_uSendPort;
		UINT	 m_uCount;
		UINT	 m_uSocketNumber;
		UINT	 m_uListenPort;

		UINT	 m_uRcvLen;
		UINT	 m_uPtr;
		UINT	 m_uRTUState;

		BOOL	 m_fMaster;

		BOOL	 m_fWaitAck;
		BOOL	 m_fIsAck;
		BOOL	 m_fRTUAccess1;
		BOOL	 m_fTrigDis[3];

		DEMHEAD	*m_pHeadD;
		CTLINFO	*m_pInfoC;

		BYTE	 m_bTx[512];
		BYTE	 m_bRx[512];

		BYTE	 m_bIDOut;
		BYTE	 m_bCtr;

		// Run Time Errors
		DWORD	 m_dError[3];

		// File transfer
		struct	FCBLK	m_FCBLK;
		struct	FCBLK	*m_pFCB;
		FDAT	m_DAT;

		// Driver Configuration members
		BYTE	m_LDEMCt;
		UINT	m_uListSel;

		BYTE	m_pbStrNum [MAXIDQ];
		BYTE	m_pbIDNums [MAXIDQ];
		WORD	m_pwIDCnt  [MAXIDQ];
		PBYTE	m_pbIDType [MAXIDQ];

		// Implementation

		UINT	CheckRecv(PBYTE pData, UINT uLen);

		BOOL	VerifyRTUAccess(UINT uCommand, DWORD dNewTime);
		void	ResetCommand(UINT uCommand, DWORD dValue, DWORD dTime);
		void	DoErrors(void);
		void	DoTimeZone(void);

		// DEM/SCH Helpers
		BOOL	FindBlockID(BYTE bID);
		UINT	BinaryFind(BYTE bID);
		UINT	GetBlockData(PDWORD pResp, UINT uQty, BOOL fIsDEM);
		UINT	GetIDListData(PBYTE pDest, PDWORD pSrc, UINT uLen);
		UINT	GetTypeSize(UINT uPos);
		void	AddDEMHeader(DEMHEAD *pHead);

		// FILE - Got Request from Server
		BOOL	InitiateFileTransfer(DEMHEAD *pHead);
		BOOL	SetFileTrigger(BYTE uID, BOOL fContinue, BOOL fClear);
		BOOL	ToggleTrigger(BYTE bID);
		BYTE	GetFileTrigger(void);
		BOOL	MakeFileReadFrame(PDWORD pData, UINT uCount);
		BOOL	ReadFileData(PDWORD pData, UINT uCount);

		// FILE Helpers
		BOOL	IsFILID(BYTE b);
		void	InitFCB(WORD wCount);
		void	ResetFCB(void);
		void	ClearFileRead(void);
		BOOL	InvalidFileReq(DEMHEAD *pHead);

		// Helpers
		void	AddByte(BYTE  b);
		void	AddWord(WORD  w);
		void	AddLong(DWORD d);
		void	ClearLoadArrays(void);
		void	AllocTypeBuffer(UINT uBuff, UINT uSize);
		void	DeleteTypeBuffers(void);
		void	MakeAck(BYTE bCtr, BYTE bCode);

		// Numeric manipulation
		UINT	GetHexValue(PBYTE p, UINT uCt);
		BOOL	CheckTimeVal(PDWORD pdTime);
		void	MakeTimeStamp(DWORD dTime);
		void	InsertRunTimeError(DWORD dError);
	};

#define	MSTART	0xF0	// Start of Message
#define	MEND	0xF1	// End of Message

// Ack/Nak
#define	ISACK	0x80
#define	ISNAK	0x81

// Frame Check
#define	NOTM2M	1000
#define	BADFRM	1001
#define	BADREQ	1002

// Message Status
#define	STATY	0xC0
#define	STATN	0x41

#define	PSTART	0
#define	PID	1
#define	PCTR	2
#define	PLEN	3
#define	PACKNAK	3
#define	PLENH	3
#define	PLENL	4
#define	PACKEND	4
#define	PDAT	5

// File equates
#define	MAXCHK	80  // maximum chunk size

#define	RR	addrRealAsReal
#define	LL	addrLongAsLong
#define	WW	addrWordAsWord
#define	YY	addrByteAsByte
#define	BB	addrBitAsBit

// TimeStamp Offset (1 Jan 1997 - 1 Jan 2000)
#define	YEAR2K	0x5A39A80

enum {
	RTUID	= 0,
	RTUTRIG	= 1,
	RTUCNT	= 2
	};

#define	FCBDEM	6 // Demand poll ID for File Control Block

// Table Numbers
enum {

// Signal
	TSCHE	=  1,
	TRBEE	=  2,
	TFILE	=  3,

	TZONE	=  8,
	TERROR	=  9,
	TMOUT	= 10,

// Demand Poll
	TDEMA	= 11,

// Scheduled Data
	TSCHA	= 21,

// Report By Exception Data
	TRBEL	= 31,
	TRBEW	= 32,
	TRBEY	= 33,
	TRBEB	= 34,
	TRBER	= 35,

// Control
	TCTLL	= 41,
	TCTLW	= 42,
	TCTLY	= 43,
	TCTLB	= 44,
	TCTLR	= 45,

// File
	TF210	= 210,
	TF211	= 211,
	TF212	= 212,
	TF213	= 213,
	TF214	= 214,
	TF215	= 215,
	TF216	= 216,
	TF217	= 217,
	TF218	= 218,
	TF219	= 219,
	TF220	= 220,
	TF221	= 221,
	TF222	= 222,
	TF223	= 223,
	TF224	= 224,
	TF225	= 225,
	TF226	= 226,
	TF227	= 227,
	TF228	= 228,
	TF229	= 229,
	TF230	= 230,
	TF231	= 231,
	TF232	= 232,
	TF233	= 233,
	TF234	= 234,
	TF235	= 235,
	};

// Defined Server IDs
enum {
	IDACK	= 0xC8,	// Ack/Nak
	IDDEM	= 0xC9,	// Demand Poll
	IDRBE	= 0xCA,	// Report By Exception
	IDCTL	= 0xCB,	// Control Write
	IDRTY	= 0xCC,	// Data Retry Request
	IDINV	= 0xCD,	// Invalid Poll
	IDSDR	= 0xCE,	// Specific Data point Request
	IDDYR	= 0xCF,	// Dynamic Read
	IDDYW	= 0xD0	// Dynamic Write
	};

// Cfg String Types
#define	CSLONG	'L'	// &= 0x40 -> 0xc = LongAsLong
#define	CSWORD	'I'	// &= 0x40 -> 0x9 = WordAsWord
#define	CSBYTE	'E'	// &= 0x40 -> 0x5 = ByteAsByte
#define	CSBIT	'@'	// &= 0x40 -> 0x0 = BitAsBit

// Type Position
#define	POSSTN	11

// Run Time Error Values
enum {
	EBADID		= 1L, // ID not found
	ENOWRITE	= 2L, // Device to G3 not selected
	ENOCONN		= 3L, // No Port Access
	ENOMEM		= 4L, // Insufficient memory
	ENOTIME		= 5L, // Bad Time Value
	ENOEXEC		= 6L, // Bad Execute Send
	ENOTOTAL	= 7L, // File Total Size = 0
	};

// End of File

#endif
