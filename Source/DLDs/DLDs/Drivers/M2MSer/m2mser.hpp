
//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Serial Slave
//

#include "m2mbase.hpp"

#ifndef M2MDATASERINC
#define M2MDATASERINC

class CM2MDataSerial : public CM2MDataBase
{
	public:
		// Constructor
		CM2MDataSerial(void);

		// Destructor
		~CM2MDataSerial(void);

		// Configuration
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Entry Points
		DEFMETH(void) Service(void);
		
	protected:

		// Implementation

		void	DoServerAccess(void);

		void	DoRTUAccess(void); // SCH / RBE / FIL

		void	DoTimeOut(void);

		// Opcode Handlers

		// DEMAND POLL
		void	DoDEMRead (void);

		// SCHEDULED DATA
		void	DoSCHRead(PDWORD pData);

		// DEM/SCH Buffer Prep
		BOOL	MakeDEMSendBuffer(DEMHEAD *pHead);

		// Send DEM/SCH
		BOOL	SendDEMSendBuffer(void);

		// CONTROL
		void	DoCTLWrite(UINT uLen);

		// REPORT BY EXCEPTION
		void	DoRBERead (PDWORD pData);

		// CONTROL / REPORT BY EXCEPTION Buffer Prep
		BOOL	DoRBEResp(BOOL fGoodData, CTLINFO * pCtl);

		// Send CONTROL / REPORT BY EXCEPTION
		BOOL	SendRBEResp(void);

		// FILE - Data From RTU to Server
		BOOL	DoFILRead(void);
		BOOL	ContinueFILRead(void);

		// FILE - Data From RTU to Server
		BOOL	SendFILRead(void);

		// FILE - Request From Server for RTU File
		void	DoFILWrite(UINT uLen);

		// FILE - Request From Server for RTU File
		BOOL	SendFILWrite(void);

		// ACK/NAK/STATUS
		void	DoAckNack(UINT uCtr, BOOL fIsAck);
		void	SendInvalidStatus(BYTE bID, BYTE bCtr, DWORD dTime);
		UINT	WaitForAck(void);

		// Check Receive
		void	ReadData(BOOL fUseTO);

		// Serial Port Access
		BOOL	Transact(void);
		void	SerialSend(void);
		BOOL	GetReply(void);
		UINT	Get(UINT uTime);
	};

// End of File

#endif
