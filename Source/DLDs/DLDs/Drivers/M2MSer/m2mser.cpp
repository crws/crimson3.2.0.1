
#include "intern.hpp"

#include "m2mser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Slave Driver
//

// Instantiator

INSTANTIATE(CM2MDataSerial);

// Constructor

CM2MDataSerial::CM2MDataSerial(void)
{
	m_Ident		= DRIVER_ID;
	}

// Destructor

CM2MDataSerial::~CM2MDataSerial(void)
{
	}

// Configuration

void MCALL CM2MDataSerial::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, FALSE);
	}

void MCALL CM2MDataSerial::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		DWORD	m_ServerIP	= GetAddr(pData); // Driver config sends it, but serial doesn't use.

		UINT	m_uLPort	= GetWord(pData);

		UINT	m_uSPort	= GetWord(pData);

		m_LDEMCt = GetByte(pData); // number of configured strings

		m_LDEMCt = min(m_LDEMCt, MAXIDQ);

		ClearLoadArrays();

		for( UINT i = 0; i < m_LDEMCt; i++ ) {

			m_pbStrNum[i] = GetByte(pData);	// high 5 bits of Addr.a.m_Offset
			m_pbIDNums[i] = GetByte(pData);	// ID number reference

			WORD wCount   = GetWord(pData);	// Number of type entries for ID

			m_pwIDCnt[i]  = wCount;

			AllocTypeBuffer(i, wCount);

			if( m_pbIDNums[i] == FCBDEM ) {

				InitFCB(wCount);
				}

			if( m_pbIDType[i] ) {

				for( UINT j = 0; j < wCount; j++ ) {

					m_pbIDType[i][j] = GetByte(pData); // Type list items
					}
				}
			}
		}

	m_bCtr		= 1;

	m_dTZOffset	= 0;

	m_uRcvLen	= 0;

	m_fWaitAck	= FALSE;

	m_fRTUAccess1	= FALSE;

	m_uRTUState	= TSCHE;

	m_dTimeout	= 20000;

	m_pFCB		= &m_FCBLK;

	m_pFCB->fBusy	= FALSE;
	m_pFCB->bID	= 0;
	m_pFCB->FCBSize	= 0;

	memset(m_dOldTrig, 0, sizeof(m_dOldTrig));
	memset(m_fTrigDis, 0, sizeof(m_fTrigDis));
	memset(m_dError,   0, sizeof(m_dError));
	}

// Management
void MCALL CM2MDataSerial::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CM2MDataSerial::Detach(void)
{
	if( m_LDEMCt ) {

		DeleteTypeBuffers();
		}

	CSlaveDriver::Detach();
	}

// Entry Point

void MCALL CM2MDataSerial::Service(void)
{
	DoRTUAccess();

	ReadData(FALSE);
	}

// Implementation

void CM2MDataSerial::DoServerAccess(void)
{
	if( !m_dTZOffset ) {

		DoTimeZone();
		}

	UINT uLen = m_uRcvLen;

	m_uRcvLen = 0;

	PBYTE p   = m_bRx;

	UINT uCmd = CheckRecv(p, uLen);

	switch( uCmd ) {

		case NOTM2M: return;

		case BADFRM: // invalid frame construction
			DoAckNack(p[PCTR], FALSE);
			return;

		case IDACK:
//**/			AfxTrace1("*A/N %X **", p[PACKNAK]);
			return;

		case IDDEM: // Req from Server for RTU Data
			DoDEMRead();
			return;

		case IDCTL: // Req from Server to write RTU Data
			DoCTLWrite(uLen);
			return;

		default:
			if( IsFILID(uCmd) ) { // Req from Server to write RTU Data

				DoFILWrite(uLen);
				}
			return;
		}
	}

void CM2MDataSerial::DoRTUAccess(void)
{
	if( !m_fRTUAccess1 ) {

		m_fRTUAccess1 = TRUE;

		DoTimeZone();

		DoTimeOut();

		m_pData->ClearRx();

		return;
		}

	if( !m_dTZOffset ) {

		DoTimeZone();
		}

	DoErrors();

	DWORD Data[3] = {0};

	if( m_pFCB->fBusy ) { // continue File data transfer

		DoFILRead();

		return;
		}

	m_uRTUState = m_uRTUState == TSCHE ? TRBEE : TSCHE;

	UINT t      = m_uRTUState;

	m_fWaitAck  = FALSE;

	UINT Loop   = 2;

	while( Loop-- ) {

		CAddress Addr;

		Addr.a.m_Table	= t;
		Addr.a.m_Offset = 0;
		Addr.a.m_Type   = LL;

		UINT uCt = t == TSCHE ? 1 : 2;

		if( COMMS_SUCCESS(Read(Addr, Data, uCt)) ) {

			DWORD dTime = 0;

			if( t == TSCHE ) {

				Data[RTUTRIG] = Data[0] ? GetTimeStamp() : 0;
				}

			dTime = Data[RTUTRIG];

			BOOL fOk = TRUE; // could read

			if( dTime ) { // send RTU data to server

				fOk = VerifyRTUAccess(t, dTime); // might be read-only, however

				if( fOk ) {

					switch( t ) {

						case TSCHE:
							DoSCHRead(Data);
							break;

						case TRBEE:
							DoRBERead(Data);
							break;
						}

					ResetCommand(t, 0L, dTime);

					if( m_fWaitAck ) {

						WaitForAck(); // function sent
						}

					m_fWaitAck = FALSE;

					return; // process just one RTU Access
					}

				ResetCommand(t, 0, dTime);
				}
			}

		t = t == TSCHE ? TRBEE : TSCHE; // continue checking for RTU Access
		}

	DoTimeOut();
	}

void CM2MDataSerial::DoTimeOut(void)
{
	DWORD Data[1] = {0};

	CAddress Addr;

	Addr.a.m_Table  = TMOUT;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = LL;
	Addr.a.m_Extra  = 0;

	if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

		DWORD d = *Data;

		if( d != m_dTimeout ) {

			if( d >= 2000L ) {

				if( COMMS_SUCCESS(Write(Addr, Data, 1)) ) {

					m_dTimeout = d;
					}
				}

			else {
				Data[0] = m_dTimeout;

				Write(Addr, Data, 1);
				}
			}
		}
	}

// Opcode Handlers
// xxxRead  - Get data from RTU, send to Server
// xxxWrite - Get data from Server, send to RTU

// DEMAND POLL
void CM2MDataSerial::DoDEMRead(void)
{
//**/	AfxTrace1("\r\nDoDEMRead ID=%d ", m_bRx[PDAT]);

	PBYTE p  = m_bRx;

	BYTE bID = p[PDAT];

	BOOL fF  = FALSE;

	if( IsFILID(bID) ) {

		if( !m_pFCB->FCBSize ) {

			DoAckNack(p[PCTR], FALSE);

			return;
			}

		fF = SetFileTrigger(0, FALSE, TRUE); // clear triggers

		if( !FindBlockID(FCBDEM) ) {

			DoAckNack(p[PCTR], FALSE);

			return;
			}
		}

	else {
		if( !FindBlockID(bID) ) {

			DoAckNack(p[PCTR], FALSE);

			return;
			}
		}

	DEMHEAD OH;

	m_pHeadD         = &OH;

	m_pHeadD->fIsDEM = TRUE;

	m_pHeadD->bID    = bID;

	m_pHeadD->bCtr   = p[PCTR];

	m_pHeadD->dTime  = m_dLocalTime;

	if( MakeDEMSendBuffer(m_pHeadD) ) {

		m_fWaitAck = FALSE;

		if( fF ) {

			if( SetFileTrigger(bID, FALSE, FALSE) ) { // enable ok to send file data

				m_pFCB->pDAT->bChunkNo	= 0;
				m_pFCB->pDAT->bChunkQty	= m_pFCB->FSize / MAXCHK;
				m_pFCB->pDAT->wSent	= 0;
				m_pFCB->fBusy		= TRUE;
				m_pFCB->UFI++;
				}
			}

		return;
		}

	DoAckNack(p[PCTR], FALSE);
	}

 // SCHEDULED DATA
void CM2MDataSerial::DoSCHRead(PDWORD pData)
{
//**/	AfxTrace2("\r\nSCH Write D0=%8.8lx D1=%8.8lx ", pData[RTUID], pData[RTUTRIG]);

	DEMHEAD OH;

	m_pHeadD = &OH;

	m_pHeadD->bID  = LOBYTE(pData[RTUID]); // Item to access

	if( !FindBlockID(m_pHeadD->bID) ) {

		m_fWaitAck = FALSE;
		return;
		}

	m_pHeadD->bCtr   = m_bCtr;

	m_pHeadD->fIsDEM = FALSE;

	DWORD dTime      = pData[RTUTRIG];

	if( !CheckTimeVal(&dTime) ) return;

	m_pHeadD->dTime  = pData[RTUTRIG];

	m_fWaitAck	 = MakeDEMSendBuffer(m_pHeadD);	
	}

// Send DEM/SCH
BOOL CM2MDataSerial::MakeDEMSendBuffer(DEMHEAD * pHead)
{
//**/	AfxTrace1("\r\nDemSendBuff %d ", pHead->bID);

	BYTE bID = pHead->bID;

	if( IsFILID(bID) ) {

		if( InitiateFileTransfer(pHead) ) {

			DoAckNack(pHead->bCtr, TRUE);

			return DoFILRead();
			}

		return FALSE;
		}

	PBYTE  pdList	 = NULL;

	BOOL   fIsDEM	 = pHead->fIsDEM;

	UINT   uSize	 = fIsDEM ? 14 : 9; // Ack included for demand poll
	UINT   uListSize = 0;

	BOOL   fOk	 = FALSE;

	UINT uQty	 = m_pwIDCnt[m_uListSel]; // number of data types for ID

	if( uQty ) {

		PDWORD pResp = new DWORD [uQty]; // allocate read data size

		if( pResp ) {

			UINT n = GetBlockData(pResp, uQty, fIsDEM); // get read data

			if( n == uQty ) {

				PBYTE pD  = new BYTE [n * sizeof(DWORD)]; // max number of data bytes needed

				if( pD ) {

					uListSize = GetIDListData(pD, pResp, n); // number of bytes actually used

					if( uListSize ) {

						fOk = TRUE; // has data

						if( fOk ) {

							if( uSize + uListSize < sizeof(m_bTx) ) {

								pdList  = new BYTE [uListSize];

								if( pdList ) {

									memcpy(pdList, pD, uListSize);
									}

								else {
									InsertRunTimeError(ENOMEM);
									fOk = FALSE;
									}
								}

							else {
								InsertRunTimeError(ENOMEM);
								fOk = FALSE;
								}
							}
						}

					else {
						InsertRunTimeError(EBADID);
						}

					delete pD;
					}

				else {
					InsertRunTimeError(ENOMEM);
					}
				}

			else {
				m_uPtr = 0;

				if( fIsDEM ) {

					MakeAck(pHead->bCtr, ISACK);
					}

				SendInvalidStatus(pHead->bID, pHead->bCtr, pHead->dTime);

				InsertRunTimeError(EBADID);

				delete pResp;

				return TRUE; // skip sending additional NAK
				}

			delete pResp;
			}
		}

	else {
		InsertRunTimeError(EBADID);
		}

	m_uPtr = 0;

	if( fIsDEM ) {

		MakeAck(pHead->bCtr, fOk ? ISACK : ISNAK);
		}

	if( fOk ) {

		AddDEMHeader(pHead);

		memcpy(&m_bTx[m_uPtr], pdList, uListSize);

		m_uPtr += uListSize;

		AddByte(MEND);
		}

	delete pdList;

	if( fOk && SendDEMSendBuffer() ) {

		m_bCtr++;

		return TRUE;
		}

	return FALSE;
	}

// Send DEM/SCH
BOOL CM2MDataSerial::SendDEMSendBuffer(void)
{
	return Transact();
	}

// CONTROL
void CM2MDataSerial::DoCTLWrite(UINT uLen)
{
	PBYTE p = m_bRx;

	CTLINFO IC;

	m_pInfoC = &IC;

	m_pInfoC->fIsCTL = TRUE;

	m_pInfoC->bCtr = p[PCTR];

	p         += PLEN;

	WORD wLen  = MotorToHost((WORD)*(PU2)p);

	p         += 2;

	UINT uQty    = (wLen - 6) / 4; // number of 16 bit addresses, and 16 bit data words.

	UINT uItemCt = 0;

	if( uQty ) {

		PWORD pAddr = new WORD [uQty];
		PWORD pData = new WORD [uQty];

		if( !(pAddr && pData) ) {

			if( pAddr ) delete pAddr;
			if( pData ) delete pData;

			InsertRunTimeError(ENOMEM);

			DoAckNack(p[PCTR], FALSE);

			return;
			}

		CAddress Addr;
		DWORD Data[1];

		Addr.a.m_Table  = TCTLW;
		Addr.a.m_Type   = WW;
		Addr.a.m_Extra  = 0;

		BOOL fSuccess   = FALSE;

		for( UINT i = 0; i < uQty; i++, p += 4 ) {

			Addr.a.m_Offset	= MotorToHost((WORD)*(PU2)p);

			*Data		= (DWORD)MotorToHost((WORD)*(PU2)(p+2));

			if( COMMS_SUCCESS(Write(Addr, Data, 1)) ) {

				if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

					pAddr[uItemCt] = Addr.a.m_Offset;

					pData[uItemCt] = LOWORD(*Data);

					uItemCt++;

					fSuccess = TRUE;
					}
				}
			}

		if( fSuccess ) { // at least one successful W/R

			m_pInfoC->uSize = uItemCt;
			m_pInfoC->pAddr = pAddr;
			m_pInfoC->pData = pData;
			m_pInfoC->dTime = m_dLocalTime;

			DoRBEResp(TRUE, m_pInfoC);
			}

		else {
			MakeAck(m_pInfoC->bCtr, ISACK);

			SendInvalidStatus(IDCTL, m_pInfoC->bCtr, m_pInfoC->dTime);
			}

		m_fWaitAck = FALSE;

		delete pAddr;
		delete pData;

		return;
		}

	m_fWaitAck = FALSE;

	DoRBEResp(FALSE, m_pInfoC);
	}

// REPORT BY EXCEPTION
void CM2MDataSerial::DoRBERead(PDWORD pData)
{
//**/	AfxTrace2("\r\nRBE Write %8.8lx %8.8lx ", pData[RTUID], pData[RTUTRIG] );

	CTLINFO IC;

	m_pInfoC	= &IC;

	CTLINFO *pCtl	= m_pInfoC;

	pCtl->fIsCTL	= FALSE;
	pCtl->bCtr	= m_bCtr;

	CAddress Addr;

	DWORD Data[1];

	Addr.a.m_Table	= TRBEW;
	Addr.a.m_Type	= WW;
	Addr.a.m_Extra	= 0;
	Addr.a.m_Offset	= LOWORD(pData[RTUID]);

	if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

		WORD wAdd   = Addr.a.m_Offset;
		WORD wData  = LOWORD(*Data);

		pCtl->uSize = 1;
		pCtl->pAddr = &wAdd;
		pCtl->pData = &wData;

		DWORD dTime = pData[RTUTRIG];

		if( !CheckTimeVal(&dTime) ) return;

		m_pInfoC->dTime = dTime;

		m_fWaitAck = DoRBEResp(TRUE, m_pInfoC) && !pCtl->fIsCTL;

		return;
		}

	InsertRunTimeError(EBADID);

	m_fWaitAck = FALSE;;

	m_uPtr = 0;

	SendInvalidStatus((BYTE)IDRBE, pCtl->bCtr, pCtl->dTime);
	}

// Send CONTROL / REPORT BY EXCEPTION
BOOL CM2MDataSerial::DoRBEResp(BOOL fGoodData, CTLINFO * pCtl)
{
	WORD wLen = 5; // Ack Frame Size

	if( fGoodData ) {

		wLen = 10 + (4 * pCtl->uSize); // F0,ID,Ctr,LenH,LenL,TimeStamp(4),F1 + Data Size

		if( pCtl->fIsCTL ) wLen += 5; // Prefixing ACK frame to good data
		}

	BOOL fCtl = pCtl->fIsCTL;

	m_uPtr    = 0;

	if( fCtl || !fGoodData ) { // Add, or use only, Ack Frame

		MakeAck(pCtl->bCtr, fGoodData ? ISACK : ISNAK);

		wLen  -= 5;
		}

	if( fGoodData ) { // Add Data if Access succeeded

		AddByte(MSTART);
		AddByte(IDRBE);
		AddByte(m_bCtr);
		AddByte(HIBYTE(wLen));
		AddByte(LOBYTE(wLen));

		MakeTimeStamp(pCtl->dTime);

		PWORD pAddr = pCtl->pAddr;
		PWORD pData = pCtl->pData;

		for( UINT i = 0; i < pCtl->uSize; i++ ) {

			AddByte(HIBYTE(pAddr[i]));
			AddByte(LOBYTE(pAddr[i]));
			AddByte(HIBYTE(pData[i]));
			AddByte(LOBYTE(pData[i]));
			}

		AddByte(MEND);
		}

	if( SendRBEResp() && fGoodData ) {

		m_bCtr++;

		return TRUE;
		}

	if( !fCtl ) { // retry if RBE

		if( SendRBEResp() && fGoodData ) {

			m_bCtr++;

			return TRUE;
			}
		}

	return FALSE;
	}

// Send CONTROL / REPORT BY EXCEPTION
BOOL CM2MDataSerial::SendRBEResp(void)
{
	return Transact();
	}

// FILE - Data From RTU to Server
BOOL CM2MDataSerial::DoFILRead(void)
{
//**/	AfxTrace1("\r\nDo FILE Read - Busy=%x ", m_pFCB->fBusy);

	FCBLK *p = m_pFCB;

	if( !p->pDAT->wSent ) {

		if( !ToggleTrigger(p->bID) ) { // set AF0 to 0 then to ID

			return FALSE; // can't access triggers
			}
		}

	return ContinueFILRead();
	}

BOOL CM2MDataSerial::ContinueFILRead()
{
	FCBLK *p = m_pFCB;

	BYTE bSize = GetFileTrigger();

	if( !bSize ) {

		if( p->uTryCt++ > 5 ) {

			ClearFileRead();
			}

		return FALSE;
		}

	UINT uRegCt  = (3 + bSize) / 4;

	PDWORD pData = new DWORD [uRegCt];

	BOOL fOk     = FALSE;

	if( (fOk = MakeFileReadFrame(pData, uRegCt)) ) {

		fOk = SendFILRead();

		if( fOk && m_fIsAck ) {

			m_bCtr++;

			p->pDAT->wSent += bSize;

			if( p->pDAT->wSent >= p->FSize ) { // done

				ClearFileRead();
				}

			else {
				SetFileTrigger(0, TRUE, FALSE); // reset count for next data read

				p->fBusy = TRUE;

				p->pDAT->bChunkNo++;
				}
			}

		else {
			ClearFileRead();
			}
		}

	delete pData;

	return fOk;
	}

// FILE - Data From RTU to Server
BOOL CM2MDataSerial::SendFILRead(void)
{
	return Transact();
	}

// FILE - Request from Server for RTU file
void CM2MDataSerial::DoFILWrite(UINT uLen)
{

	}

// File - Data From Server to RTU;
BOOL CM2MDataSerial::SendFILWrite(void)
{
	return Transact();
	}

// ACK/NAK/STATUS
void CM2MDataSerial::DoAckNack(UINT uCtr, BOOL fIsAck)
{
	MakeAck(LOBYTE(uCtr), fIsAck ? ISACK : ISNAK);

	SerialSend();
	}

void CM2MDataSerial::SendInvalidStatus(BYTE bID, BYTE bCtr, DWORD dTime)
{
	AddByte(MSTART);
	AddByte(bID);
	AddByte(bCtr);
	AddByte((BYTE)STATN);

	MakeTimeStamp(dTime);

	AddByte(MEND);

	SerialSend();
	}

UINT CM2MDataSerial::WaitForAck(void)
{
	m_fWaitAck  = TRUE; // set return from ReadData through this routine

	BOOL fOk    = FALSE;

	UINT uTimer = m_dTimeout;

	m_uRcvLen   = 0;

	SetTimer(uTimer);

	while( GetTimer() ) {

		m_fIsAck = FALSE;

		ReadData(TRUE);

		if( m_uRcvLen ) { // full frame received

			return (UINT)m_bRx[PACKNAK];
			}

		Sleep(20);
		}

	return 0;
	}

// Check Receive
void CM2MDataSerial::ReadData(BOOL fUseTO)
{
	UINT uState  = 0;
	UINT uTime   = fUseTO ? m_dTimeout : 1000;
	UINT uLen    = 0;
	UINT uPtr    = 0;
	UINT uData;

	BYTE bID     = 0;

	BOOL fIsAck  = FALSE;

	m_uRcvLen    = 0;

//**/	BOOL fFirst  = FALSE;

	SetTimer(uTime);

	while( GetTimer() ) {

		if( (uData = Get(uTime)) < NOTHING ) {

			BYTE bData  = LOBYTE(uData);

//**/			if( !fFirst ) { AfxTrace0("\r\n"); fFirst = TRUE; } AfxTrace1("<%2.2x>", bData);

			m_bRx[uPtr++] = bData;

			switch( uState ) {

				case 0:
					if( bData == MSTART ) {

						uState = 1;
						}

					else uPtr = 0;
					break;

				case 1:
					if( (bID = bData) == IDACK ) {

						fIsAck = TRUE;

						uLen   = 5;

						uState = 4;
						}

					else uState = 2;
					break;

				case 2:
					if( uPtr == PLENH + 1 ) {

						uLen   = ((UINT)bData) << 8;

						uState = 3;
						}

					break;

				case 3:
					uLen  += (UINT)bData;

					uState = 4;

					break;

				case 4:
					if( bData == MEND ) {

						if( uPtr >= uLen ) {

							m_dLocalTime = GetTimeStamp();

							m_fIsAck     = fIsAck;

							m_uRcvLen    = uPtr;

							if( !IsFILID(bID) ) {

								switch( bID ) {

									case IDDEM:
									case IDCTL:
										break;

									default: return;
									}
								}

							DoServerAccess(); // Command from Server

							return;
							}
						}

					break;
				}

			if( uPtr >= sizeof(m_bRx) ) return;
			}
		}
	}

// Serial Port Access

BOOL CM2MDataSerial::Transact(void)
{
	SerialSend();

	return GetReply();
	}

void CM2MDataSerial::SerialSend(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

BOOL CM2MDataSerial::GetReply(void)
{
	m_uRcvLen   = 0;

	ReadData(TRUE);

	if( m_uRcvLen ) return TRUE;

	return FALSE;
	}

UINT CM2MDataSerial::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
