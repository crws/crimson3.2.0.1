
#include "intern.hpp"

#include "monsnmp2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Source Files
//

#include "../MonSNMP/SnmpAgent.cpp"

#include "../MonSNMP/Asn1BerDecoder.cpp"

#include "../MonSNMP/Asn1BerEncoder.cpp"

#include "../MonSNMP/oid.cpp"

//////////////////////////////////////////////////////////////////////////
//
// String Constants
//

static char pExtOidBase [] = "1.3.6.1.4.1.36282.%u.";

static char pExtSuffix1 [] = "%u.0.0";

static char pExtSuffix2 [] = "%u";

//////////////////////////////////////////////////////////////////////////
//
// Monico SNMP Driver v2.0
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved

// Instantiator

INSTANTIATE(CMonicoSNMP2);

// Helper
/*
UINT GetTickCount(void)
{
	return 0;
	}*/

// Constructor

CMonicoSNMP2::CMonicoSNMP2(void)
{
	m_Ident		= DRIVER_ID;

	m_fSysDefault	= FALSE;

	m_pSysDesc	= NULL;

	m_pSysContact	= NULL;

	m_pSysName	= NULL;

	m_pSysLocation	= NULL;
	}

// Destructor

CMonicoSNMP2::~CMonicoSNMP2(void)
{
	Free(m_pSysDesc);	

	Free(m_pSysContact);

	Free(m_pSysName);

	Free(m_pSysLocation);
	}

// Configuration

void MCALL CMonicoSNMP2::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_Port1 = GetWord(pData);
		m_Port2 = GetWord(pData);

		strcpy(m_sComm, GetString(pData));

		m_AclMask1  = GetLong(pData);
		m_AclAddr1  = GetLong(pData);
		m_AclMask2  = GetLong(pData);
		m_AclAddr2  = GetLong(pData);
		m_TrapFrom  = GetLong(pData);
		m_TrapAddr1 = GetLong(pData);
		m_TrapMode1 = GetWord(pData);
		m_TrapAddr2 = GetLong(pData);
		m_TrapMode2 = GetWord(pData);
		m_fSysDefault = GetByte(pData);

		if( !m_fSysDefault ) {

			m_pSysDesc	= GetString(pData);
			m_pSysContact	= GetString(pData);
			m_pSysName	= GetString(pData);
			m_pSysLocation	= GetString(pData);
			}
		}
	}

// Management

void MCALL CMonicoSNMP2::Attach(IPortObject *pPort)
{
	m_pAgent = New CSnmpAgent;

	InitOidSets();

	if( !m_fSysDefault ) {

		m_pAgent->SetSysDesc(m_pSysDesc);

		m_pAgent->SetSysContact(m_pSysContact);

		m_pAgent->SetSysName(m_pSysName);

		m_pAgent->SetSysLoc(m_pSysLocation);
		}

	m_pAgent->Bind(&m_uTicks, "1.3.6.1.4.1.36282", m_sComm);

	m_genData16	= m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.4.2.1.0.0", this, 21, 1000);

	m_genData32	= m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.4.2.2.0.0", this, 22, 500);

	m_genTrap	= m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.4.2.3.0.0", this, 23, 1000);

	m_genNotify	= m_pAgent->AddNotifySource("1.3.6.1.4.1.36282.4.2.4");

	m_setData16	= m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.4.1.1.0.0", this, 11, 500);

	m_setData32	= m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.4.1.2.0.0", this, 12, 250);

	m_setTrap	= m_pAgent->AddRangedSource("1.3.6.1.4.1.36282.4.1.3.0.0", this, 13, 1000);

	m_setNotify	= m_pAgent->AddNotifySource("1.3.6.1.4.1.36282.4.1.4");

	AddExtendedOIDs();

	m_pSock     = CreateSocket(IP_UDP);

	m_uTicks    = GetTickCount() / 2;

	m_uBase1    = 0;

	m_uBase2    = 0;

	FindMappedTraps();
	}

void MCALL CMonicoSNMP2::Detach(void)
{
	KillOidSets();

	m_pSock->Release();

	delete m_pAgent;
	}

void MCALL CMonicoSNMP2::Open(void)
{
	if( m_pSock ) {
		
		m_pSock->SetOption(OPT_RECV_QUEUE, 8);

		m_pSock->SetOption(OPT_ADDRESS,    2);

		m_pSock->Listen   (m_Port1);
		}
	}

// Entry Point

void MCALL CMonicoSNMP2::Service(void)
{
	UINT uLast = GetTickCount();

	UINT uTime = 0;

	for(;;) {

		uTime    = GetTickCount();

		m_uTicks = uTime / 2;

		if( uTime - uLast > ToTicks(250) ) {

			if( m_TrapMode1 || m_TrapMode2 ) {

				CheckTraps();
				}

			uLast = GetTickCount();
			}

		for( UINT n = 0; n < 4; n++ ) {

			if( !CheckRequest() ) {

				break;
				}

			ForceSleep(10);
			}

		ForceSleep(50);
		}
	}

// Implementation

bool CMonicoSNMP2::CheckRequest(void)
{
	CBuffer *pBuff = NULL;

	if( m_pSock->Recv(pBuff) == S_OK ) {

		BYTE bHead[12];

		memcpy( bHead,
			pBuff->StripHead(12),
			12
			);

		DWORD IP = MAKELONG(MAKEWORD(bHead[3],bHead[2]),MAKEWORD(bHead[1],bHead[0]));

		if( AllowIP(IP) ) {

			CAsn1BerDecoder req(m_pHelper);

			PCBYTE pRecv = pBuff->GetData();

			UINT   uRecv = pBuff->GetSize();

			UINT   uBuff = 1476;

			if( uRecv <= uBuff ) {
			
				if( req.Decode(pRecv, uRecv) ) {

					CAsn1BerEncoder rep;

					if( m_pAgent->OnRequest(req, rep) ) {

						PCBYTE pSend = rep.GetData();

						UINT   uSend = rep.GetSize();

						pBuff->Release();

						if( uSend <= uBuff ) {

							if( (pBuff = CreateBuffer(uBuff, TRUE)) ) {
						
								memcpy( pBuff->AddTail(6),
									bHead,
									6
									);

								memcpy( pBuff->AddTail(uSend),
									pSend,
									uSend
									);

								SetTimer(500);

								while( GetTimer() ) {

									if( m_pSock->Send(pBuff) == S_OK ) {

										return true;
										}

									Sleep(20);
									}

								pBuff->Release();
								}
							}

						return false;
						}
					}
				}
			}

		pBuff->Release();
		}

	return false;
	}

bool CMonicoSNMP2::AllowIP(DWORD IP)
{
	if( m_AclAddr1 && (IP & m_AclMask1) - (m_AclAddr1 & m_AclMask1) ) {

		return false;
		}

	if( m_AclAddr2 && (IP & m_AclMask2) - (m_AclAddr2 & m_AclMask2) ) {

		return false;
		}

	return true;
	}

void CMonicoSNMP2::CheckTraps(void)
{
	CheckTraps(m_Trap1, m_uBase1, 13, 1000, m_setNotify, m_setTrap);

	CheckTraps(m_Trap2, m_uBase2, 23, 1000, m_genNotify, m_genTrap);

	CheckExtendedTraps();
	}

void CMonicoSNMP2::CheckTraps(PBYTE pHist, UINT &uBase, UINT uTable, UINT uCount, UINT uNotify, UINT uTrap)
{
	UINT uSend = 0;

	UINT uScan = 0;

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD Data;

		CAddress Addr;

		Addr.a.m_Type   = addrLongAsLong;

		Addr.a.m_Table  = uTable;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = 1 + uBase;

		CCODE  cCode = Read(Addr, &Data, 1);

		if( COMMS_SUCCESS(cCode) ) {

			BYTE bData = Data ? 1 : 0;

			if( pHist[uBase] != bData ) {

				UINT uPos = 1 + uBase;

				if( !SendTrap(m_TrapMode1, m_TrapAddr1, uTable, uPos, uNotify, uTrap) ) {

					break;
					}

				if( !SendTrap(m_TrapMode2, m_TrapAddr2, uTable, uPos, uNotify, uTrap) ) {

					break;
					}

				pHist[uBase] = bData;

				uSend++;
				}

			uScan++;
			}

		if( true ) {

			uBase = (uBase + 1) % uCount;
			}

		if( uScan == 25 || uSend == 8 ) {

			break;
			}
		}
	}

bool CMonicoSNMP2::SendTrap(UINT &Mode, UINT &Addr, UINT uTable, UINT uPos, UINT uNotify, UINT uTrap)
{
	if( Mode ) {

		CAsn1BerEncoder rep;

		if( Mode == 1 ) {
			
			UINT uEnt = uPos + GetCode(uTable) * 500;

			m_pAgent->SetTrapV1(rep, m_TrapFrom, 6, uEnt, uTrap, uPos);
			}

		if( Mode == 2 ) {

			m_pAgent->SetTrapV2(rep, uNotify, uPos, uTrap);
			}

		if( Mode == 3 ) {

			m_pAgent->SetInfoV2(rep, uNotify, uPos, uTrap);
			}

		PCBYTE   pSend = rep.GetData();

		UINT     uSend = rep.GetSize();

		CBuffer *pBuff = CreateBuffer(1280, TRUE);

		if( pBuff ) {

			BYTE bHead[6];

			bHead[0] = HIBYTE(HIWORD(Addr));
			bHead[1] = LOBYTE(HIWORD(Addr));
			bHead[2] = HIBYTE(LOWORD(Addr));
			bHead[3] = LOBYTE(LOWORD(Addr));
			
			bHead[4] = HIBYTE(m_Port2);
			bHead[5] = LOBYTE(m_Port2);
		
			memcpy( pBuff->AddTail(6),
				bHead,
				6
				);

			memcpy( pBuff->AddTail(uSend),
				pSend,
				uSend
				);

			SetTimer(500);

			while( GetTimer() ) {

				if( m_pSock->Send(pBuff) == S_OK ) {

					return true;
					}

				Sleep(10);
				}

			pBuff->Release();
			}

		return false;
		}

	return true;
	}

void CMonicoSNMP2::FindMappedTraps(void)
{
	FindMappedTraps(m_Trap1, 13, 1000);

	FindMappedTraps(m_Trap2, 23, 1000);

	FindExtendedMappedTraps();
	}

void CMonicoSNMP2::FindMappedTraps(PBYTE pHist, UINT uTable, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		CAddress Addr;

		Addr.a.m_Type   = addrLongAsLong;

		Addr.a.m_Table  = uTable;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = 1 + n;

		DWORD Data  = 0;

		CCODE cCode = Read(Addr, &Data, 1);

		if( COMMS_SUCCESS(cCode) ) {

			pHist[n] = 0x55;

			continue;
			}

		pHist[n] = 0;
		}
	}

UINT CMonicoSNMP2::GetCode(UINT uTable)
{
	switch( uTable ) {

		case 23:
			
			return 2;
		}

	return 0;
	}

// Data Source

bool CMonicoSNMP2::IsSpace(COid const &Oid, UINT uTag, UINT uPos)
{
	return FALSE;
	}

bool CMonicoSNMP2::GetData(CAsn1BerEncoder &rep, COid const &Oid, UINT uTag, UINT uPos)
{
	CAddress Addr;

	Addr.a.m_Type   = addrLongAsLong;

	Addr.a.m_Table  = uTag;

	Addr.a.m_Extra  = 0;

	Addr.a.m_Offset = uPos;

	DWORD Data;

	if( COMMS_SUCCESS(Read(Addr, &Data, 1)) ) {

		rep.AddInteger(Data);

		return true;
		}

	rep.AddNull();

	return true;
	}

// Extended OID Support

void CMonicoSNMP2::AddExtendedOIDs(void)
{
	AddCummins();

	AddGenerac();

	AddKohler();

	AddMTU();

	AddATS();
	}

void CMonicoSNMP2::FindExtendedMappedTraps(void)
{
	for( UINT s = 0; s < setTotal; s++ ) {

		if( m_pOidSet[s] ) {

			for( UINT u = 0; u < m_uExtSet[s]; u++ ) {

				CSet Set = m_pOidSet[s][u];

				FindMappedTraps(Set.m_pTrap,
						Set.m_uTable,
						Set.m_uTraps);	
				}
			}
		}
	}


void CMonicoSNMP2::CheckExtendedTraps(void)
{
	for( UINT s = 0; s < setTotal; s++ ) {

		if( m_pOidSet[s] ) {

			for( UINT u = 0; u < m_uExtSet[s]; u++ ) {

				CSet Set = m_pOidSet[s][u];

				CheckTraps(Set.m_pTrap,
					   Set.m_uBase,
					   Set.m_uTable,
					   Set.m_uTraps,
					   Set.m_uNotify,
					   Set.m_uTrap);
				}
			}
		}
	}

void CMonicoSNMP2::AddCummins(void)
{
	UINT Data [] = { 100, 500, 500 };

	UINT Trap [] = { 200, 500, 500 };

	AddOidSet(setCummins, 31, 5, elements(Data), Data, Trap);
	}

void CMonicoSNMP2::AddGenerac(void)
{
	UINT Data [] = {  500, 100 };

	UINT Trap [] = { 1000, 200 };

	AddOidSet(setGenerac, 41, 6, elements(Data), Data, Trap);
	}

void CMonicoSNMP2::AddKohler(void)
{
	UINT Data [] = { 350, 150 };

	UINT Trap [] = { 500, 150 };

	AddOidSet(setKohler, 51, 7, elements(Data), Data, Trap);
	}

void CMonicoSNMP2::AddMTU(void)
{
	UINT Data [] = {  500 };

	UINT Trap [] = { 1000 };

	AddOidSet(setMTU, 61, 8, elements(Data), Data, Trap);
	}

void CMonicoSNMP2::AddATS(void)
{
	UINT Data [] = { 1000 };

	UINT Trap [] = { 1000 };

	AddOidSet(setATS, 71, 9, elements(Data), Data, Trap);
	}

void CMonicoSNMP2::GetOid(char * pOid, char * pBase, UINT uOid, char * pSuffix, UINT uSuffix)
{
	char sBase[32];

	SPrintf(sBase, pBase, uOid + 1);

	SPrintf(pOid, sBase, 1);

	if( pSuffix ) {

		char sExt[32];
		
		SPrintf(sExt, pSuffix, uSuffix);

		strcat(pOid, sExt);
		}
	}

void CMonicoSNMP2::AddOidSet(UINT uIndex, UINT uTable, UINT uBase, UINT uSets, UINT * pData, UINT * pTraps)
{
	m_pOidSet[uIndex] = new CSet[uSets];

	m_uExtSet[uIndex] = uSets;

	char sBase[32];

	SPrintf(sBase, pExtOidBase, uBase);

	strcat(sBase, "%u.");

	for( UINT s = 0; s < uSets; s++, uTable += 2 ) {

		char sOid[32];

		GetOid(sOid, sBase, s, pExtSuffix1, 1);

		m_pOidSet[uIndex][s].m_uData   = m_pAgent->AddRangedSource(sOid, this, uTable + 0, pData[s]);

		GetOid(sOid, sBase, s, pExtSuffix1, 2);

		m_pOidSet[uIndex][s].m_uTrap   = m_pAgent->AddRangedSource(sOid, this, uTable + 1, pTraps[s]);

		GetOid(sOid, sBase, s, pExtSuffix2, 3);

		m_pOidSet[uIndex][s].m_uNotify = m_pAgent->AddNotifySource(sOid);

		m_pOidSet[uIndex][s].m_uBase   = 0;

		m_pOidSet[uIndex][s].m_uTable  = uTable + 1;

		m_pOidSet[uIndex][s].m_uTraps  =  pTraps[s];

		m_pOidSet[uIndex][s].m_pTrap   = new BYTE[pTraps[s]];
		}
	}

void CMonicoSNMP2::InitOidSets(void)
{
	for( UINT s = 0; s < setTotal; s++ ) {

		m_pOidSet[s] = NULL;

		m_uExtSet[s] = 0;
		}
	}

void CMonicoSNMP2::KillOidSets(void)
{
	for( UINT s = 0; s < setTotal; s++ ) {

		KillOidSet(s);
		}
	}

void CMonicoSNMP2::KillOidSet(UINT uIndex)
{
	for( UINT o = 0; o < m_uExtSet[uIndex]; o++ ) {

		delete [] m_pOidSet[uIndex][o].m_pTrap;
		}

	delete m_pOidSet[uIndex];

	m_pOidSet[uIndex] = NULL;

	m_uExtSet[uIndex] = 0;
	}

// End of File
