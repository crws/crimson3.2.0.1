
//////////////////////////////////////////////////////////////////////////
//
// IMO Loader Port

// Offset is ddx
#define	OPM	1
#define	OPP	2
#define	OPK	3
#define	OPL	4
#define	OPF	5
#define	HIOPS	OPF+1
// Offset is decimal
#define	OPD	6
#define	OPTV	7
#define	OPTB	8
#define	OPCV	9
#define	OPCB	10
#define ERRCODE 11

// High Byte of Base Addresses - K series
#define	AM0	0x5D
#define	AP0	0x5C
#define	AK0	0x60
#define	AL0	0x5F
#define	AF0	0x63
#define	AD0	0x00
#define	ATV0	0x50
#define	ATB0	0x61
#define	ACV0	0x52
#define	ACB0	0x62

class CIMOLoaderPortDriver : public CMasterDriver {

	public:
		// Constructor
		CIMOLoaderPortDriver(void);

		// Destructor
		~CIMOLoaderPortDriver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
			
		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);
		CCODE MCALL Read(AREF Addr, PDWORD pData, UINT uCount);
		CCODE MCALL Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Data Members
		LPCTXT	m_pHex;

		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		UINT	m_uPtr;
		UINT	m_uSendCount;
		BYTE	m_bCheck;
		UINT	m_uLastByte;
		DWORD	m_ErrorCode;
		
		// Implementation

		// Frame Building
		void ConfigureRead(AREF Addr, UINT uCount);
		void ConfigureWrite(AREF Addr, UINT uCount, PDWORD pData);
		void StartFrame(BYTE bOpcode);
		void EndFrame(void);
		void PutAddress(AREF Addr);
		void PutCount(AREF Addr, UINT uCount);
		void AddWriteData(AREF Addr, PDWORD pData, UINT uCount);

		// Data Transfer
		BOOL Transact(void);
		BOOL GetReply(void);

		// Response Handlers
		CCODE GetResponse(PDWORD pData, AREF Addr, UINT uReturnCount);
		BOOL GetBitResponse(PDWORD pData, AREF Addr, UINT * pReturnCount);
		BOOL GetWordResponse(PDWORD pData, AREF Addr, UINT * pReturnCount);

		// Port Access
		void Put(void);
		UINT Get(void);

		// Helpers
		void AddByte(BYTE bData);
		void AddData(UINT uData, UINT uMask);
		UINT GetHex(UINT p, UINT n);
		UINT GetMaxCount( AREF Addr, UINT uCount );
		UINT SwapLoWordBytes( UINT uData );
	};

// End of File
