
#include "intern.hpp"

#include "omflm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Base Class
//

#include "../OmFlTcpM/omflbase.cpp"

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow Master Serial Driver
//

// Instantiator

INSTANTIATE(COmniFlowMaster);

// Constructor

COmniFlowMaster::COmniFlowMaster(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_fAscii    = FALSE;

	m_uTimeout  = 600;
	}

// Destructor

COmniFlowMaster::~COmniFlowMaster(void)
{
	}

// Configuration

void MCALL COmniFlowMaster::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_fAscii   = GetByte(pData);
		}
	}
	
void MCALL COmniFlowMaster::CheckConfig(CSerialConfig &Config)
{
	if( !m_fAscii && Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}
	
	Make485(Config, TRUE);
	}
	
// Management

void MCALL COmniFlowMaster::Attach(IPortObject *pPort)
{
	if( m_fAscii ) {

		m_pData = MakeDoubleDataHandler();

		pPort->Bind(m_pData);
		}
	else {
		m_pData = MakeSingleDataHandler();

		pPort->Bind(m_pData);
		}
	}

void MCALL COmniFlowMaster::Open(void)
{
	}

// Device

CCODE MCALL COmniFlowMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_PingReg	= GetWord(pData);
			m_pCtx->m_Disable5	= GetByte(pData);
			m_pCtx->m_Disable6	= GetByte(pData);

			m_pCtx->m_Exception	= 0;
			m_pCtx->m_CustomPt	= 0;
			m_pCtx->m_CustomQty	= 0;

			memset(m_pCtx->m_Custom, 0, elements(m_pCtx->m_Custom));

			m_pCtx->m_ReadPt	= 0;
			
			memset(m_pCtx->m_ReadBuff, 0x0, elements(m_pCtx->m_ReadBuff) * sizeof(DWORD));
			
			m_pCtx->m_WritePt	= 0;

			m_pCtx->m_WriteQty	= 0;

			memset(m_pCtx->m_WriteBuff, 0x0, elements(m_pCtx->m_WriteBuff) * sizeof(DWORD));
						
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL COmniFlowMaster::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

UINT MCALL COmniFlowMaster::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 )  {  // Drop Number

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bUnit = uValue;

			return 1;
			}
		}

	if( uFunc == 4 ) {  // Current target drop number 

		return pCtx->m_bUnit;
		}
	
	return 0;
	}

// Implementation

BOOL COmniFlowMaster::IsHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return TRUE;
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return TRUE;
		}
		
	return FALSE;
	}

WORD COmniFlowMaster::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return bData - '0';
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return bData - 'A' + 10;
		}
		
	return 0;
	}

// Port Access

void COmniFlowMaster::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT COmniFlowMaster::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void COmniFlowMaster::StartFrame(BYTE bOpcode)
{
	COmniFlowBaseMaster::StartFrame(bOpcode);
	
	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);

	m_bHead = m_uPtr;
	}

// Transport Layer

BOOL COmniFlowMaster::Transact(void)
{
	m_fException = FALSE;

	if( SendFrame() ) {
	
		if( !m_pCtx->m_bUnit ) { 

			while( m_pData->Read(0) < NOTHING );

			Sleep(100);

			return TRUE;
			}
			
		if( RecvFrame() ) {

			return CheckFrame();
			}
		}
		
	return FALSE;
	}

BOOL COmniFlowMaster::SendFrame(void)
{
	COmniFlowBaseMaster::SendFrame();

	m_pData->ClearRx();
	
	return m_fAscii ? AsciiTx() : BinaryTx();
	}

BOOL COmniFlowMaster::RecvFrame(void)
{
	return m_fAscii ? AsciiRx() : BinaryRx();
	}

BOOL COmniFlowMaster::CheckFrame(void)
{
	if( m_bRxBuff[0] == m_bTxBuff[0] ) {

		return COmniFlowBaseMaster::CheckFrame();
		}

	return FALSE;
	}

BOOL COmniFlowMaster::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_bTxBuff[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));
		
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL COmniFlowMaster::BinaryRx(void)
{
	UINT uByte = 0;

	UINT uSize = NOTHING;

	UINT uPtr  = 0;

	UINT uGap  = 0;

	UINT uEnd  = FindEndTime();

//	AfxTrace("\nRx :");

	SetTimer(m_uTimeout);

	while( GetTimer() ) {

		if( (uByte = RxByte(5)) < NOTHING ) {

			m_bRxBuff[uPtr++] = uByte;

//			AfxTrace("%2.2x ", uByte);

			if( uPtr == sizeof(m_bRxBuff) ) {

				return FALSE;
				}

			uGap = 0;
			}
		else
			uGap = uGap + 1;

		if( uPtr >= uSize || uGap >= uEnd ) {

			if( uPtr >= 4 ) {

				m_CRC.Preset();
				
				PBYTE p = m_bRxBuff;
				
				UINT  n = uPtr - 2;
			
				for( UINT i = 0; i < n; i++ ) {

					m_CRC.Add(*(p++));
					}

				WORD c1 = IntelToHost(PU2(p)[0]);
				
				WORD c2 = m_CRC.GetValue();

				if( c1 == c2 ) {

					return TRUE;
					}
				}
			
			uSize = NOTHING;
				
			uPtr  = 0;
			
			uGap  = 0;
			}
		}

	return FALSE;
	}

BOOL COmniFlowMaster::AsciiTx(void)
{
	BYTE bCheck = 0;
	
	TxByte(':');
		
	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		TxByte(m_pHex[m_bTxBuff[i] / 16]);
		
		TxByte(m_pHex[m_bTxBuff[i] % 16]);

		bCheck += m_bTxBuff[i];
		}
		
	bCheck = BYTE(0x100 - WORD(bCheck));
	
	TxByte(m_pHex[bCheck / 16]);
	TxByte(m_pHex[bCheck % 16]);
	
	TxByte(CR);
	TxByte(LF);
	
	return TRUE;
	}

BOOL COmniFlowMaster::AsciiRx(void)
{
	UINT uState = 0;

	UINT uPtr   = 0;

	UINT uTimer = 0;
	
	BYTE bCheck = 0;

//	AfxTrace("\nRx :");

	SetTimer(m_uTimeout);
	
	while( (uTimer = GetTimer()) ) {

		UINT uByte;
		
		if( (uByte = RxByte(uTimer)) == NOTHING ) {
			
			continue;
			}

//		AfxTrace("%2.2x ", uByte);
			
		switch( uState ) {
		
			case 0:
				if( uByte == ':' ) {

					uPtr   = 0;

					uState = 1;

					bCheck = 0;
					}
				break;
				
			case 1:
				if( IsHex(uByte) ) {
				
					m_bRxBuff[uPtr] = FromHex(uByte) << 4;
				
					uState = 2;
					}
				else {
					if( uByte == CR ) {
					
						continue;
						}

					if( uByte == LF ) {

						if( bCheck == 0 ) {

							Sleep(10);

							return TRUE;
							}
						}
					
					return FALSE;
					}
				break;
				
			case 2:
				if( IsHex(uByte) ) {

					m_bRxBuff[uPtr] |= FromHex(uByte);
					
					bCheck += m_bRxBuff[uPtr];

					if( ++uPtr == sizeof(m_bRxBuff) ) {
						
						return FALSE;
						}
					
					uState = 1;

					break;
					}
				
				return FALSE;
			}
		}

	return FALSE;
	}

// Transport Helpers

UINT COmniFlowMaster::FindEndTime(void)
{
	return ToTicks(25);
	}

// End of File
