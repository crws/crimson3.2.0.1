
#include "Intern.hpp"

#include "Oid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SNMP Agent
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved.
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OID Wrapper
//

// Constructors

COid::COid(PCSTR pText)
{
	m_uSize = 0;

	Parse(pText);
	}

// Attributes

UINT COid::GetWildCardCount(void) const
{
	if( m_uSize > 1 ) {

		UINT c = m_uSize - 2;

		for( UINT n = 0; n <= c; n++ ) {

			if( m_uData[c - n] ) {

				break;
				}
			}

		return n;
		}

	return 0;
	}

void COid::GetAsText(char *pText, UINT uSize) const
{
	strcpy(pText, "<null>");
	}

int COid::CompareWithWildCards(COid const &That) const
{
	if( m_uSize > 2 ) {

		UINT uWild = That.GetWildCardCount();

		if( uWild ) {

			if( m_uSize == That.m_uSize ) {

				if( m_uData[m_uSize - 1] == 0 ) {

					UINT uComp = m_uSize - 1 - uWild;

					int  nTest = memcmp( m_uData,
							     That.m_uData,
							     uComp * sizeof(m_uData[0])
							     );

					if( !nTest ) {

						return 0;
						}
					}
				}
			}
		}

	return CompareExact(That);
	}

int COid::CompareExact(COid const &That) const
{
	UINT uComp = min( m_uSize,
			  That.m_uSize
			  );

	int  nTest = memcmp( m_uData,
			     That.m_uData,
			     uComp * sizeof(m_uData[0])
			     );

	if( !nTest ) {

		if( m_uSize < That.m_uSize ) {

			return -1;
			}

		if( m_uSize > That.m_uSize ) {

			return +1;
			}
		}

	return nTest;
	}

// Implementation

bool COid::Parse(PCSTR pText)
{
	if( pText ) {

		Empty();

		if( *pText == '.' ) {

			pText++;
			}

		for(;;) {

			PSTR pEnd  = NULL;

			UINT uData = strtoul(pText, &pEnd, 10);

			if( *pEnd == 0 || *pEnd == '.' ) {

				if( !Append(uData) ) {

					return false;
					}

				if( *pEnd ) {

					pText = pEnd + 1;
					
					continue;
					}

				return true;
				}

			return false;
			}
		}

	return false;
	}

// End of File
