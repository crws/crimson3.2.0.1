
//////////////////////////////////////////////////////////////////////////
//
// SNMP Agent
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved.
//

#ifndef INCLUDE_SNMP_Oid_HPP

#define INCLUDE_SNMP_Oid_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OID Wrapper
//

class COid
{
	public:
		// Constructors
		COid(void);
		COid(COid const &That);
		COid(PCSTR pText);

		// Assignment
		COid const & operator = (COid const &That);

		// Attributes
		bool IsNull(void) const;
		UINT GetSymCount(void) const;
		UINT GetSym(UINT uSym) const;
		UINT GetWildCardCount(void) const;
		void GetAsText(char *pText, UINT uSize) const;
		int  CompareWithWildCards(COid const &That) const;
		int  CompareExact(COid const &That) const;

		// Operations
		void Empty(void);
		bool SetSym(UINT uSym, UINT uData);
		bool Append(UINT uData);

		// Debugging
		void Print(void) const;

	protected:
		// Data Members
		UINT m_uData[32];
		UINT m_uSize;

		// Implementation
		bool Parse(PCSTR pText);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructors

inline COid::COid(void)
{
	m_uSize = 0;
	}

inline COid::COid(COid const &That)
{
	memcpy( m_uData,
		That.m_uData,
		That.m_uSize * sizeof(m_uData[0])
		);

	m_uSize = That.m_uSize;
	}

// Assignment

inline COid const & COid::operator = (COid const &That)
{
	memcpy( m_uData,
		That.m_uData,
		That.m_uSize * sizeof(m_uData[0])
		);

	m_uSize = That.m_uSize;

	return *this;
	}

// Attributes

inline bool COid::IsNull(void) const
{
	return m_uSize ? false : true;
	}

inline UINT COid::GetSymCount(void) const
{
	return m_uSize;
	}

inline UINT COid::GetSym(UINT uSym) const
{
	return m_uData[uSym];
	}

// Operations

inline void COid::Empty(void)
{
	m_uSize = 0;
	}

inline bool COid::Append(UINT uData)
{
	if( m_uSize < elements(m_uData) ) {

		m_uData[m_uSize++] = uData;

		return true;
		}

	return false;
	}

inline bool COid::SetSym(UINT uSym, UINT uData)
{
	if( uSym < m_uSize ) {

		m_uData[uSym] = uData;

		return true;
		}

	return false;
	}

// End of File

#endif
