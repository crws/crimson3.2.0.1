
#include "intern.hpp"

#include "SnmpDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SNMP Driver
//

// Instantiator

INSTANTIATE(CSnmpDriver);

// Helper

/*UINT GetTickCount(void)
{
	return 0;
	}*/

// Constructor

CSnmpDriver::CSnmpDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CSnmpDriver::~CSnmpDriver(void)
{
	}

// Configuration

void MCALL CSnmpDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_Port1 = GetWord(pData);
		m_Port2 = GetWord(pData);

		strcpy(m_sComm, GetString(pData));

		m_TabLimit  = GetWord(pData);
		m_AclMask1  = GetLong(pData);
		m_AclAddr1  = GetLong(pData);
		m_AclMask2  = GetLong(pData);
		m_AclAddr2  = GetLong(pData);
		m_TrapFrom  = GetLong(pData);
		m_TrapAddr1 = GetLong(pData);
		m_TrapMode1 = GetWord(pData);
		m_TrapAddr2 = GetLong(pData);
		m_TrapMode2 = GetWord(pData);
		m_ChgLimit  = GetWord(pData);
		}
	}

// Management

void MCALL CSnmpDriver::Attach(IPortObject *pPort)
{
	m_pAgent = New CSnmpAgent;

	m_pAgent->Bind(&m_uTicks, "1.3.6.1.4.1.38113.1.1", m_sComm);

	m_pAgent->AddRangedSource("1.3.6.1.4.1.38113.1.1.1.1.1.1.0.0", m_pAgent, m_pAgent->srcIndex, m_TabLimit);

	m_pAgent->AddRangedSource("1.3.6.1.4.1.38113.1.1.1.2.1.1.0.0", m_pAgent, m_pAgent->srcIndex, m_TabLimit);

	m_pAgent->AddRangedSource("1.3.6.1.4.1.38113.1.1.1.4.1.1.0.0", m_pAgent, m_pAgent->srcIndex, m_TabLimit);

	m_genData   = m_pAgent->AddRangedSource("1.3.6.1.4.1.38113.1.1.1.1.1.2.0.0", this, 3, m_TabLimit);

	m_genTrap   = m_pAgent->AddRangedSource("1.3.6.1.4.1.38113.1.1.1.2.1.2.0.0", this, 4, m_TabLimit);

	m_genNotify = m_pAgent->AddNotifySource("1.3.6.1.4.1.38113.1.1.1.3");

	m_genTrapA  = m_pAgent->AddRangedSource("1.3.6.1.4.1.38113.1.1.1.4.1.2.0.0", this, 6, m_TabLimit);

	m_genNotifyA= m_pAgent->AddNotifySource("1.3.6.1.4.1.38113.1.1.1.5");

	m_pSock     = CreateSocket(IP_UDP);

	m_uTicks    = GetTickCount() / 2;

	m_uBase1    = 0;

	m_uBase2    = 0;

	m_uBaseA    = 0;

	memset(m_Change, 0xFF, elements(m_Change) * sizeof(WORD));

	FindMappedTraps();
	}

void MCALL CSnmpDriver::Detach(void)
{
	m_pSock->Release();

	delete m_pAgent;
	}

void MCALL CSnmpDriver::Open(void)
{
	if( m_pSock ) {
		
		m_pSock->SetOption(OPT_RECV_QUEUE, 8);

		m_pSock->SetOption(OPT_ADDRESS,    2);

		m_pSock->Listen   (m_Port1);
		}
	}

// Entry Point

void MCALL CSnmpDriver::Service(void)
{
	UINT uLast = GetTickCount();

	UINT uTime = 0;

	for(;;) {

		uTime    = GetTickCount();

		m_uTicks = uTime / 2;

		if( uTime - uLast > ToTicks(250) ) {

			if( m_TrapMode1 || m_TrapMode2 ) {

				CheckTraps();
				}

			uLast = GetTickCount();
			}

		for( UINT n = 0; n < 4; n++ ) {

			if( !CheckRequest() ) {

				break;
				}

			ForceSleep(10);
			}

		ForceSleep(50);
		}
	}

// Implementation

bool CSnmpDriver::CheckRequest(void)
{
	CBuffer *pBuff = NULL;

	if( m_pSock->Recv(pBuff) == S_OK ) {

		BYTE bHead[12];

		memcpy( bHead,
			pBuff->StripHead(12),
			12
			);

		DWORD IP = MAKELONG(MAKEWORD(bHead[3],bHead[2]),MAKEWORD(bHead[1],bHead[0]));

		if( AllowIP(IP) ) {

			CAsn1BerDecoder req;

			PCBYTE pRecv = pBuff->GetData();

			UINT   uRecv = pBuff->GetSize();
			
			if( req.Decode(pRecv, uRecv) ) {

				CAsn1BerEncoder rep;

				if( m_pAgent->OnRequest(req, rep) ) {

					PCBYTE pSend = rep.GetData();

					UINT   uSend = rep.GetSize();

					pBuff->Release();

					if( (pBuff = CreateBuffer(1280, TRUE)) ) {
						
						memcpy( pBuff->AddTail(6),
							bHead,
							6
							);

						memcpy( pBuff->AddTail(uSend),
							pSend,
							uSend
							);

						SetTimer(500);

						while( GetTimer() ) {

							if( m_pSock->Send(pBuff) == S_OK ) {

								return true;
								}

							Sleep(20);
							}

						pBuff->Release();
						}

					return false;
					}
				}
			}

		pBuff->Release();
		}

	return false;
	}

bool CSnmpDriver::AllowIP(DWORD IP)
{
	if( m_AclAddr1 && (IP & m_AclMask1) - (m_AclAddr1 & m_AclMask1) ) {

		return false;
		}

	if( m_AclAddr2 && (IP & m_AclMask2) - (m_AclAddr2 & m_AclMask2) ) {

		return false;
		}

	return true;
	}

void CSnmpDriver::CheckTraps(void)
{
	CheckTraps(m_Trap2, m_uBase2, 4, m_TabLimit, m_genNotify,  m_genTrap);

	CheckTraps(m_TrapA, m_uBaseA, 6, m_TabLimit, m_ChgLimit, m_genNotifyA, m_genTrapA);
	}

void CSnmpDriver::CheckTraps(PBYTE pHist, UINT &uBase, UINT uTable, UINT uCount, UINT uNotify, UINT uTrap)
{
	UINT uSend = 0;

	UINT uScan = 0;

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD Data;

		CAddress Addr;

		SetAddr(Addr, uTable, uBase + 1);

		CCODE  cCode = Read(Addr, &Data, 1);

		if( COMMS_SUCCESS(cCode) ) {

			BYTE bData = Data ? 1 : 0;

			if( pHist[uBase] != bData ) {

				UINT uPos = 1 + uBase;

				if( !SendTrap(m_TrapMode1, m_TrapAddr1, uTable, uPos, uNotify, uTrap) ) {

					break;
					}

				if( !SendTrap(m_TrapMode2, m_TrapAddr2, uTable, uPos, uNotify, uTrap) ) {

					break;
					}

				pHist[uBase] = bData;

				uSend++;
				}

			uScan++;
			}

		if( true ) {

			uBase = (uBase + 1) % uCount;
			}

		if( uScan == 25 || uSend == 8 ) {

			break;
			}
		}
	}

void CSnmpDriver::CheckTraps(PDWORD pHist, UINT &uBase, UINT uTable, UINT uCount, UINT uChange, UINT uNotify, UINT uTrap)
{
	UINT uSend = 0;

	UINT uScan = 0;

	for( UINT n = 0; n < uCount; n++ ) {

		if( IsFound(uBase) ) {

			DWORD Data;

			CAddress Addr;

			SetAddr(Addr, uTable, uBase + 1);

			CCODE  cCode = Read(Addr, &Data, 1);

			if( COMMS_SUCCESS(cCode) ) {

				if( IsChanged(pHist[uBase], Data, m_Change[uBase]) ) {

					UINT uPos = 1 + uBase;

					if( !SendTrap(m_TrapMode1, m_TrapAddr1, uTable, uPos, uNotify, uTrap) ) {

						break;
						}

					if( !SendTrap(m_TrapMode2, m_TrapAddr2, uTable, uPos, uNotify, uTrap) ) {

						break;
						}

					pHist[uBase] = Data;

					uSend++;
					}

				uScan++;
				}
			}

		if( true ) {

			uBase = (uBase + 1) % uCount;
			}

		if( uScan == 25 || uSend == 8 ) {

			break;
			}
		}
	}

bool CSnmpDriver::SendTrap(UINT &Mode, UINT &Addr, UINT uTable, UINT uPos, UINT uNotify, UINT uTrap)
{
	if( Mode ) {

		CAsn1BerEncoder rep;

		if( Mode == 1 ) {
			
			UINT uEnt = uPos + (uTable - 2) * 500;

			m_pAgent->SetTrapV1(rep, m_TrapFrom, 6, uEnt, uTrap, uPos);
			}

		PCBYTE   pSend = rep.GetData();

		UINT     uSend = rep.GetSize();

		CBuffer *pBuff = CreateBuffer(1280, TRUE);

		if( pBuff ) {

			BYTE bHead[6];

			bHead[0] = HIBYTE(HIWORD(Addr));
			bHead[1] = LOBYTE(HIWORD(Addr));
			bHead[2] = HIBYTE(LOWORD(Addr));
			bHead[3] = LOBYTE(LOWORD(Addr));
			
			bHead[4] = HIBYTE(m_Port2);
			bHead[5] = LOBYTE(m_Port2);
		
			memcpy( pBuff->AddTail(6),
				bHead,
				6
				);

			memcpy( pBuff->AddTail(uSend),
				pSend,
				uSend
				);

			SetTimer(500);

			while( GetTimer() ) {

				if( m_pSock->Send(pBuff) == S_OK ) {

					return true;
					}

				Sleep(10);
				}

			pBuff->Release();
			}

		return false;
		}

	return true;
	}

void CSnmpDriver::FindMappedTraps(void)
{
	FindMappedTraps(m_Trap2, 4, m_TabLimit);

	FindMappedTraps(m_TrapA, 6, m_TabLimit);
	}

void CSnmpDriver::FindMappedTraps(PBYTE pHist, UINT uTable, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		CAddress Addr;

		SetAddr(Addr, uTable, 1 + n);

		DWORD Data  = 0;

		CCODE cCode = Read(Addr, &Data, 1);

		if( COMMS_SUCCESS(cCode) ) {

			pHist[n] = 0x55;

			continue;
			}

		pHist[n] = 0;
		}
	}

void CSnmpDriver::FindMappedTraps(PDWORD pHist, UINT uTable, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		BOOL fFound = FALSE;

		for( UINT i = 0; i < m_ChgLimit; i++ ) {

			CAddress Addr;

			Addr.a.m_Type   = addrLongAsLong;

			Addr.a.m_Table  = uTable;

			Addr.a.m_Extra  = i & 0xF;

			Addr.a.m_Offset = ((1 + n) | ((i & 0x3F0) << 6));

			DWORD Data  = 0;

			CCODE cCode = Read(Addr, &Data, 1);

			if( COMMS_SUCCESS(cCode) ) {

				m_Change[n] = i;

				pHist[n] = Data;

				fFound = TRUE;

				break;
				}
			}

		if( !fFound ) {

			pHist[n] = 0;
			}
		}
	}

// Data Source

bool CSnmpDriver::IsSpace(COid const &Oid, UINT uTag, UINT uPos)
{
	return FALSE;
	}

bool CSnmpDriver::GetData(CAsn1BerEncoder &rep, COid const &Oid, UINT uTag, UINT uPos)
{
	CAddress Addr;

	SetAddr(Addr, uTag, uPos);

	DWORD Data;

	if( COMMS_SUCCESS(Read(Addr, &Data, 1)) ) {

		if( uTag == 1 ) {

			switch( uPos ) {

				case 3:
				case 11:
				case 19:
				case 29:
				case 49:
				case 50:
				case 51:
				case 52:
				case 53:
				case 54:
				case 57:
				case 59:
				case 60:
				case 66:
				case 67:
				case 68:
				case 69:
				case 76:
				case 77:
				case 78:
				case 79:
				case 80:
				case 81:
				case 82:
				case 83:
				case 84:
				case 85:
				case 86:
				case 87:
				case 88:
				case 89:
				case 90:
				case 91:

					rep.AddFloat(Data);
					
					return true;
				}
			}
				
		rep.AddInteger(Data);

		return true;
		}

	rep.AddNull();

	return true;
	}

// Helpers

void CSnmpDriver::SetAddr(CAddress &Addr, UINT uTable, UINT uPos)
{
	UINT uChg = uTable == 6 ? m_Change[uPos - 1] : 0;

	Addr.a.m_Type   = addrLongAsLong;

	Addr.a.m_Table  = uTable;

	Addr.a.m_Extra  = uChg & 0xF;

	Addr.a.m_Offset = uPos | ((uChg & 0x3F0) << 6);
	}

bool CSnmpDriver::IsChanged(UINT uLast, UINT uCurrent, UINT uChg)
{
	if( int(uCurrent - uLast - uChg) > 0 ) {

		return TRUE;
		}

	return int(uLast - uCurrent - uChg) > 0;
	}

bool CSnmpDriver::IsFound(UINT uPos)
{
	return m_Change[uPos] != 0xFFFF;
	}

// End of File
