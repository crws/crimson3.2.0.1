
#include "Intern.hpp"

#include "Asn1BerEncoder.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SNMP Agent
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved.
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Oid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ASN.1 BER Encoder
//

// Constructor

CAsn1BerEncoder::CAsn1BerEncoder(void)
{
	m_uPtr = 0;
	}

// Destructor

CAsn1BerEncoder::~CAsn1BerEncoder(void)
{
	}

// Attributes

bool CAsn1BerEncoder::IsNull(void) const
{
	return m_uPtr == 0;
	}

UINT CAsn1BerEncoder::GetSize(void) const
{
	return m_uPtr;
	}

PCBYTE CAsn1BerEncoder::GetData(void) const
{
	return m_bData;
	}

// Operations

void CAsn1BerEncoder::Rewind(UINT uPos)
{
	m_uPtr = uPos;
	}

// Writing

UINT CAsn1BerEncoder::AddSequence(void)
{
	m_bData[m_uPtr++] = 0x30;

	m_bData[m_uPtr++] = 0xCC;

	return m_uPtr;
	}

UINT CAsn1BerEncoder::AddConstructed(BYTE bTag)
{
	m_bData[m_uPtr++] = BYTE(0xA0 | bTag);

	m_bData[m_uPtr++] = 0xCC;

	return m_uPtr;
	}

void CAsn1BerEncoder::AddEnd(UINT uPos)
{
	UINT uSize = m_uPtr - uPos;

	if( uSize < 128 ) {

		m_bData[uPos - 1] = BYTE(uSize);
		}
	else {
		if( uSize < 256 ) {

			memmove( m_bData + uPos + 1,
				 m_bData + uPos + 0,
				 uSize
				 );

			m_bData[uPos - 1] = 0x81;

			m_bData[uPos + 0] = BYTE(uSize);

			m_uPtr += 1;
			}
		else {
			memmove( m_bData + uPos + 2,
				 m_bData + uPos + 0,
				 uSize
				 );

			m_bData[uPos - 1] = 0x82;

			m_bData[uPos + 0] = HIBYTE(uSize);

			m_bData[uPos + 1] = LOBYTE(uSize);

			m_uPtr += 2;
			}
		}
	}

void CAsn1BerEncoder::AddInteger(UINT uData)
{
	AddInteger(asnInteger, uData);
	}

void CAsn1BerEncoder::AddFloat(DWORD Data)
{
	m_bData[m_uPtr++] = 0x44;
	m_bData[m_uPtr++] = 0x07;

	m_bData[m_uPtr++] = 0x9F;
	m_bData[m_uPtr++] = 0x78;
	m_bData[m_uPtr++] = 0x04;

	m_bData[m_uPtr++] = PBYTE(&Data)[0];
	m_bData[m_uPtr++] = PBYTE(&Data)[1];
	m_bData[m_uPtr++] = PBYTE(&Data)[2];
	m_bData[m_uPtr++] = PBYTE(&Data)[3];
	}

void CAsn1BerEncoder::AddNetAddress(UINT uData)
{
	AddInteger(0x40, uData);
	}

void CAsn1BerEncoder::AddCounter(UINT uData)
{
	AddInteger(0x41, uData);
	}

void CAsn1BerEncoder::AddTimeTicks(UINT uData)
{
	AddInteger(0x43, uData);
	}

void CAsn1BerEncoder::AddInteger(BYTE bTag, UINT uData)
{
	m_bData[m_uPtr++] = bTag;

	if( !(uData & 0xFFFFFF80) ) {

		m_bData[m_uPtr++] = 0x01;

		m_bData[m_uPtr++] = LOBYTE(LOWORD(uData));

		return;
		}

	if( !(uData & 0xFFFF8000) ) {

		m_bData[m_uPtr++] = 0x02;

		m_bData[m_uPtr++] = HIBYTE(LOWORD(uData));
		m_bData[m_uPtr++] = LOBYTE(LOWORD(uData));

		return;
		}

	if( !(uData & 0xFF800000) ) {

		m_bData[m_uPtr++] = 0x03;

		m_bData[m_uPtr++] = LOBYTE(HIWORD(uData));
		m_bData[m_uPtr++] = HIBYTE(LOWORD(uData));
		m_bData[m_uPtr++] = LOBYTE(LOWORD(uData));

		return;
		}

	m_bData[m_uPtr++] = 0x04;

	m_bData[m_uPtr++] = HIBYTE(HIWORD(uData));
	m_bData[m_uPtr++] = LOBYTE(HIWORD(uData));
	m_bData[m_uPtr++] = HIBYTE(LOWORD(uData));
	m_bData[m_uPtr++] = LOBYTE(LOWORD(uData));
	}

void CAsn1BerEncoder::AddOctString(PCSTR pText)
{
	AddOctString(PCBYTE(pText), strlen(pText));
	}

void CAsn1BerEncoder::AddOctString(PCBYTE pData, UINT uSize)
{
	m_bData[m_uPtr++] = asnOctString;

	m_bData[m_uPtr++] = BYTE(uSize);

	memcpy(m_bData + m_uPtr, pData, uSize);

	m_uPtr += uSize;
	}

bool CAsn1BerEncoder::AddObjectID(COid &Oid)
{
	UINT uCount = Oid.GetSymCount();

	if( uCount >= 3 ) {

		UINT uSave  = m_uPtr;

		UINT uTemp  = 0;

		m_bData[m_uPtr++] = asnObjectID;

		m_bData[m_uPtr++] = 0;

		for( UINT uSym = 0; uSym < uCount; uSym++ ) {

			UINT uData = Oid.GetSym(uSym);

			switch( uSym ) {

				case 0:
					if( uData >= 0 && uData <= 2 ) {

						uTemp = uData;

						break;
						}

					return false;

				case 1:
					if( uData >= 0 && uData <= 39 ) {

						m_bData[m_uPtr++] = BYTE(40 * uTemp + uData);

						break;
						}

					return false;

				default:
					if( uData >= 128 * 128 ) {

						m_bData[m_uPtr++] = BYTE(128 | (uData / 128 / 128));
						}

					if( uData >= 128 ) {

						m_bData[m_uPtr++] = BYTE(128 | (uData / 128 % 128));
						}

					m_bData[m_uPtr++] = BYTE(uData % 128);

					break;
				}
			}

		m_bData[uSave + 1] = BYTE(m_uPtr - uSave - 2);

		return true;
		}

	return false;
	}

void CAsn1BerEncoder::AddVarBindList(COid *pOid)
{
	UINT s1 = AddSequence();

	if( pOid ) {

		for( UINT n = 0; !pOid[n].IsNull(); n++ ) {

			UINT s2 = AddSequence();

			AddObjectID(pOid[n]);

			AddNull();

			AddEnd(s2);
			}
		}

	AddEnd(s1);
	}

void CAsn1BerEncoder::AddNull(void)
{
	m_bData[m_uPtr++] = asnNull;

	m_bData[m_uPtr++] = 0;
	}

void CAsn1BerEncoder::AddNoSuchObject(void)
{
	m_bData[m_uPtr++] = 0x80;

	m_bData[m_uPtr++] = 0;
	}

void CAsn1BerEncoder::AddEndOfView(void)
{
	m_bData[m_uPtr++] = 0x82;

	m_bData[m_uPtr++] = 0;
	}

// End of File
