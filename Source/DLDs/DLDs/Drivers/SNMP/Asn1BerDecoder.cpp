
#include "Intern.hpp"

#include "Asn1BerDecoder.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SNMP Agent
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved.
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Oid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ASN.1 BER Decoder
//

// Constructor

CAsn1BerDecoder::CAsn1BerDecoder(void)
{
	// TODO -- This class can be improved by allow it
	// to act directly on the byte stream rather than
	// parsing it ahead of time. This will save both
	// time and memory, although it does mean that we
	// must be careful that the stream still exists!
	}

// Destructor

CAsn1BerDecoder::~CAsn1BerDecoder(void)
{
	}

// Decoding

bool CAsn1BerDecoder::Decode(PCBYTE pData, UINT uSize)
{
	m_uCount = 0;

	m_uRead  = 0;

	return DecodeFrom(pData, uSize);
	}

// Reading

bool CAsn1BerDecoder::ReadSequence(void)
{
	CItem const &Item = m_List[m_uRead];

	if( Item.m_bType == asnSequence ) {

		m_uRead++;

		return true;
		}

	return false;
	}

bool CAsn1BerDecoder::ReadConstructed(UINT &uTag)
{
	CItem const &Item = m_List[m_uRead];

	if( Item.m_bType == asnConstructed ) {

		uTag = Item.m_uTag;

		m_uRead++;

		return true;
		}

	return false;
	}

bool CAsn1BerDecoder::ReadEnd(void)
{
	CItem const &Item = m_List[m_uRead];

	if( Item.m_bType == asnClose ) {

		m_uRead++;

		return true;
		}

	return false;
	}

bool CAsn1BerDecoder::ReadInteger(UINT &uData)
{
	CItem const &Item = m_List[m_uRead];

	if( Item.m_bType == asnInteger ) {

		uData = 0;

		for( UINT n = 0; n < Item.m_uLen; n++ ) {

			uData *= 256;

			uData += Item.m_pData[n];
			}

		m_uRead++;

		return true;
		}

	return false;
	}

bool CAsn1BerDecoder::ReadOctString(char *pData, UINT uSize)
{
	CItem const &Item = m_List[m_uRead];

	if( Item.m_bType == asnOctString ) {

		if( 1 + Item.m_uLen < uSize ) {

			memcpy( pData,
				Item.m_pData,
				Item.m_uLen
				);

			pData[Item.m_uLen] = 0;

			m_uRead++;

			return true;
			}
		}

	return false;
	}

bool CAsn1BerDecoder::ReadObjectID(COid &Oid)
{
	CItem const &Item = m_List[m_uRead];

	if( Item.m_bType == asnObjectID ) {

		UINT v1 = Item.m_pData[0] / 40;

		UINT v2 = Item.m_pData[0] % 40;

		Oid.Empty();

		if( Oid.Append(v1) && Oid.Append(v2) ) {

			for( UINT n = 1; n < Item.m_uLen; ) {

				UINT uData = 0;

				for(;;) {

					BYTE bRead = Item.m_pData[n++];

					uData *= 0x80;

					uData += (bRead & 0x7F);

					if( bRead & 0x80 ) {

						continue;
						}

					if( !Oid.Append(uData) ) {

						return false;
						}

					break;
					}
				}

			m_uRead++;

			return true;
			}
		}

	return false;
	}

bool CAsn1BerDecoder::ReadVarBindList(COid *pOid, UINT uSize)
{
	if( ReadSequence() ) {

		UINT uItem = 0;

		while( ReadSequence() ) {

			if( ReadObjectID(pOid[uItem++]) ) {

				if( uItem < uSize ) {

					m_uRead++;

					if( ReadEnd() ) {

						continue;
						}
					}

				return false;
				}
			}

		if( ReadEnd() ) {

			return true;
			}
		}

	return false;
	}

// Implementation

bool CAsn1BerDecoder::DecodeFrom(PCBYTE &pData, UINT uSize)
{
	PCBYTE pDone = pData + uSize;

	while( pData < pDone ) {

		BYTE bType = *pData++;

		UINT uTag  = 0;

		UINT uLen  = 0;

		if( (bType & 0x1F) < 0x1F ) {

			uTag = (bType & 0x1F);
			}
		else {
			for(;;) {

				BYTE bRead = *pData++;

				uTag *= 0x80;

				uTag += (bRead & 0x7F);

				if( bRead & 0x80 ) {

					continue;
					}

				break;
				}
			}
			
		if( (bType & 0x20) == 0x00 ) {

			m_List[m_uCount].m_bType = BYTE(uTag);
		
			m_List[m_uCount].m_uTag  = 0;
			}
		else {
			if( (bType & 0xC0) == 0x80 ) {

				m_List[m_uCount].m_bType = asnConstructed;
		
				m_List[m_uCount].m_uTag  = BYTE(uTag);
				}
			else {
				m_List[m_uCount].m_bType = BYTE(uTag);
		
				m_List[m_uCount].m_uTag  = 0;
				}
			}

		if( (uLen = *pData++) & 0x80 ) {

			UINT uNum;
			
			if( (uNum = (uLen & 0x7F)) ) {

				uLen = 0;

				while( uNum-- ) {

					uLen *= 256;

					uLen += *pData++;
					}
				}
			else {
				// TODO -- Variable length encoding.

				return false;
				}
			}

		if( true ) {

			m_List[m_uCount].m_uLen  = uLen;

			m_List[m_uCount].m_pData = pData;

			switch( m_List[m_uCount++].m_bType ) {

				case asnSequence:
				case asnConstructed:

					if( DecodeFrom(pData, uLen) ) {

						continue;
						}

					return false;
				}

			pData += uLen;
			}
		}

	m_List[m_uCount].m_bType = asnClose;

	m_List[m_uCount].m_uTag  = 0;

	m_List[m_uCount].m_uLen  = 0;

	m_List[m_uCount].m_pData = NULL;

	m_uCount++;

	return true;
	}

// End of File
