
#include "intern.hpp"

#include "fatekudp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC Serial Driver
//

// Instantiator

INSTANTIATE(CFatekPLCUDPDriver);

// Constructor

CFatekPLCUDPDriver::CFatekPLCUDPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CFatekPLCUDPDriver::~CFatekPLCUDPDriver(void)
{
	}

// Configuration

void MCALL CFatekPLCUDPDriver::Load(LPCBYTE pData)
{ 
	}
	
// Management

void MCALL CFatekPLCUDPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CFatekPLCUDPDriver::Open(void)
{
	}

// Device

CCODE MCALL CFatekPLCUDPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();

			BYTE b = GetByte(pData);

			m_pBase->Drop[0]   = m_pHex[b/16];
			m_pBase->Drop[1]   = m_pHex[b%16];

			m_pBase->m_fGlobal = !b;

			m_pBase->m_dLatestError = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CFatekPLCUDPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Transport Layer

BOOL CFatekPLCUDPDriver::Transact(void)
{
	if( OpenSocket() ) {

		return Send() && GetReply();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CFatekPLCUDPDriver::Send(void)
{
	EndFrame();

	UINT uSize = m_uPtr;

//**/	AfxTrace0("\r\n");
//**/	for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k] );
//**/	AfxTrace0("\r\n");

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CFatekPLCUDPDriver::GetReply(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - m_uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

//**/			AfxTrace0("\r\n");
//**/			for( UINT k = 0; k < uPtr; k++ ) AfxTrace1("<%2.2x>", m_bRx[k] );

			if( m_bRx[uPtr-1] == ETX ) {

				return CheckReply(uPtr);
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}	

	return FALSE;
	}

// Socket Management

BOOL CFatekPLCUDPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CFatekPLCUDPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_UDP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		}

	return FALSE;
	}

void CFatekPLCUDPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort ) {

			m_pCtx->m_pSock->Abort();
			}

		else {
			m_pCtx->m_pSock->Close();
			}

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
