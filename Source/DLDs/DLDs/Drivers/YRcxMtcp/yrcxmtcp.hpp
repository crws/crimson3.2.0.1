#include "rcxbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yamaha RCX Series Master TCP Driver
//

class CYRcxMtcp : public CYamahaRcxMbaseDriver
{
	public:
		// Constructor
		CYRcxMtcp(void);

		// Destructor
		~CYRcxMtcp(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		
	protected:
		// Device Context
		struct CContext : CYamahaRcxMbaseDriver::CBaseCtx
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BYTE	 m_Login;
			BYTE     m_UBytes;
			PCTXT 	 m_User;
			BYTE	 m_PBytes;
			PCTXT 	 m_PWord;
			BOOL	 m_fEcho;
			BOOL	 m_fInit;
			};

		// Data Members
		CContext * m_pCtx;
		UINT	   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Transport Layer
		BOOL Send(void);
		BOOL RecvFrame(BOOL fLogin);
		BOOL Transact(void);

		// Implementation
		BOOL SendLogin(void);
		BOOL SendPass(void);
		BOOL ClearEcho(void);
		BOOL SendBye(void);
	};

// End of File
