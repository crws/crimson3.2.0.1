#include "intern.hpp"

#include "yrcxmtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yamaha RCX Series Master TCP Driver
//

// Instantiator

INSTANTIATE(CYRcxMtcp);

// Constructor

CYRcxMtcp::CYRcxMtcp(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CYRcxMtcp::~CYRcxMtcp(void)
{
	m_pExtra->Release();
	}

// Configuration

void MCALL CYRcxMtcp::Load(LPCBYTE pData)
{
	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}
	
// Management

void MCALL CYRcxMtcp::Attach(IPortObject *pPort)
{
	}

void MCALL CYRcxMtcp::Open(void)
{
	}

// Device

CCODE MCALL CYRcxMtcp::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			
			m_pCtx->m_Login  = GetByte(pData);

			#ifdef _M_ARM
		
			m_pCtx->m_UBytes = GetWord(pData);

			pData -= sizeof(WORD);

			#else

			m_pCtx->m_UBytes = pData[1];

			#endif

			m_pCtx->m_User   = GetString(pData);

			#ifdef _M_ARM

			m_pCtx->m_PBytes = GetWord(pData);

			pData -= sizeof(WORD);

			#else

			m_pCtx->m_PBytes = pData[1];

			#endif

			m_pCtx->m_PWord  = GetString(pData);

			m_pCtx->m_fEcho  = GetByte(pData) ? TRUE : FALSE;

			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();

			m_pBase->m_uBrkLn = 0;
			m_pBase->m_uBrkPt = 0;
			m_pBase->m_fInit  = FALSE;

			m_pBase->m_Table  = NULL;

			m_pBase->m_uPoints = 0;

			memset(m_pBase->m_Error, 0, sizeof(m_pBase->m_Error));
			 			
			memset(m_pBase->m_Term, 0, sizeof(m_pBase->m_Term));

			m_pBase->m_Busy   = 0;

			m_pBase->m_fLogOff = FALSE;
		       					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CYRcxMtcp::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		if( m_pBase->m_Table ) {

			delete [] m_pBase->m_Table;
			}

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);

	}

// Entry Points

CCODE MCALL CYRcxMtcp::Ping(void)
{	
	if( m_pCtx->m_fLogOff ) {

		return CCODE_SUCCESS;
		}
	
	m_pBase->m_fInit = FALSE;
	
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}
			}
		}

	if( COMMS_SUCCESS(CYamahaRcxMbaseDriver::Ping()) ) {

		m_pBase->m_fInit = TRUE;
			
		return CCODE_SUCCESS;
		}

	if( !m_pCtx->m_Login ) {

		UINT uTry = m_pCtx->m_uPort == 23 ? 6 : 3;

		for( UINT u = 0, s = 0; u < uTry; u++ ) {
		
			if( RecvFrame(TRUE) && m_uRxPtr ) {

				if( s == 0 ) {

					BOOL fOk = FALSE;

					for( UINT x = 0; x <= 3; x++ ) {
					
						if( !memcmp(m_bRxBuff + x, PBYTE("login:"), 6) ) {

							if( SendLogin() ) {

								s++;

								u = 0;
								
								fOk = TRUE;
								}

							break;
							}
						}
					
					if( fOk ) {

						continue;
						}
					}

				if( s == 1 && !memcmp(m_bRxBuff, PBYTE("Pass"), 4) ) {
					
					if( SendPass() ) {
					
						s++;

						u = 0;

						continue;
						} 
					}

				if( s == 2 && !memcmp(m_bRxBuff, PBYTE("OK"), 2) ) {
					
					m_pBase->m_fInit = TRUE;
					
					return CCODE_SUCCESS;
					}

				if( !memcmp(m_bRxBuff, PBYTE("login incorrect"), 15) ) {

					u = 0;

					s = 0;

					continue;
					}
				}
			}

		CloseSocket(TRUE);
		}

	return CCODE_ERROR;
	}

// Socket Management

BOOL CYRcxMtcp::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CYRcxMtcp::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {
	
		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			UINT uTime = m_pCtx->m_uTime1;

			SetTimer(uTime);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}
				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}
				
			CloseSocket(TRUE); 

			return FALSE;
			}
		}

	return FALSE;
	}

void CYRcxMtcp::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CYRcxMtcp::Transact(void)
{
	if( OpenSocket() ) {

		if( Send() ) {
		
			if( m_pItem && m_pItem->m_Resp == respWait ) {

				m_pBase->m_Busy = 1;

				return TRUE;
				}

			if( RecvFrame(FALSE) ) {

				return TRUE;
				}
			}

		CloseSocket(TRUE);
		}

	return FALSE; 
	}

BOOL CYRcxMtcp::Send(void)
{
	UINT uSize = m_uTxPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uTxPtr ) {

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CYRcxMtcp::RecvFrame(BOOL fLogin)
{
	m_uRxPtr    = 0;

	UINT uSize  = 0;

	UINT uCR    = 0;

	BOOL fEcho  = FALSE;

	if ( !OpenSocket() ) {

		return FALSE;
		}

	UINT uTime = m_pCtx->m_uTime2;

	SetTimer(uTime);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - m_uRxPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + m_uRxPtr, uSize);

		if( uSize ) {

			for( UINT u = m_uRxPtr; u < m_uRxPtr + uSize; u++ ) {

				if( m_bRxBuff[u] == CR && m_bRxBuff[u + 1] == LF ) {

					if( !m_pBase->m_fInit && m_pCtx->m_Login ) {
						
						if( m_pItem && m_pItem->m_Resp != respOK )  {
							
							if( !memcmp(m_bRxBuff, PBYTE("OK"), 2) ) {

								m_uRxPtr = 0;

								uSize = 0;

								break;
								}
							}
						}

					uCR++;

					if( m_pItem && m_pItem->m_Resp == respSync ) {
						
						if( !memcmp(m_bRxBuff, PBYTE("current"), 7) && uCR < 2 ) {

							SetTimer(m_pCtx->m_uTime2);

							break;
							}
						}

					m_uRxPtr = u + 1;

					if( !fLogin ) {

						if( !CheckFrame() ) {

							if( uSize > m_uRxPtr + 1) {

								uSize = uSize - m_uRxPtr + 1;

								memcpy(m_bRxBuff, m_bRxBuff + m_uRxPtr + 1, uSize);
								
								u = 0;

								m_uRxPtr = 0;

								continue;
								}

							return FALSE;
							}
						}
									
					return TRUE;
					} 

				if( fLogin && m_bRxBuff[u] == ':' ) {

					m_uRxPtr += uSize;

					return TRUE;
					} 
				}

			m_uRxPtr += uSize;
			
			continue;
			}

		if( !CheckSocket() ) {
			
			return FALSE;
			}
	       
		Sleep(10);
		} 

	return FALSE;
	}

// Implementation

BOOL CYRcxMtcp::SendLogin(void)
{
	if( OpenSocket() ) {
	
		m_uTxPtr = 0;

		memcpy(m_bTxBuff, m_pCtx->m_User, m_pCtx->m_UBytes);

		m_uTxPtr += m_pCtx->m_UBytes;

		End();

		if( Send() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CYRcxMtcp::SendPass(void)
{
	if( OpenSocket() ) {
	
		m_uTxPtr = 0;

		memcpy(m_bTxBuff, m_pCtx->m_PWord, m_pCtx->m_PBytes);

		m_uTxPtr += m_pCtx->m_PBytes;
	
		End();

		if( Send() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CYRcxMtcp::ClearEcho(void)
{
	if( OpenSocket() ) {
	
		m_uTxPtr = 0;
	
		memcpy(m_bTxBuff, PBYTE("@?ETHER_ECHO_0"), 14);

		m_uTxPtr += 14;

		End();

		if( Send() && RecvFrame(FALSE) ) {

			if( !memcmp(m_bRxBuff, PBYTE("OK"), 2) ) {

				return TRUE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CYRcxMtcp::SendBye(void)
{
	if( OpenSocket() ) {
	
		m_uTxPtr = 0;

		memcpy(m_bTxBuff, PBYTE("BYE"), 3);

		m_uTxPtr += 3;

		End();

		if( Send() ) {

			return TRUE;
			}
		}
  
	return FALSE;
	}
 
// End of File
