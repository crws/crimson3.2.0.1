
#include "intern.hpp"

#include "kingbus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// King	Bus ASCII Communications Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CKingBusASCIIDriver);

// Constructor

CKingBusASCIIDriver::CKingBusASCIIDriver(void)
{
	m_Ident  = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;

	m_pList   = NULL;

	m_pPoll   = NULL;

	m_uTimeout = 1000;

	m_uPollMin = 0;

	m_uPollMax = 0;

	m_uTimeMin = 0;

	m_uTimeMax = 2;

	m_fInit = FALSE;

}

// Destructor

CKingBusASCIIDriver::~CKingBusASCIIDriver(void)
{
	delete[] m_pList;

}

// Entry Points

CCODE MCALL CKingBusASCIIDriver::Ping(void)
{
	return CCODE_SUCCESS;
}

// Configuration

void MCALL CKingBusASCIIDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uTimeout = GetWord(pData);
	}
}

void MCALL CKingBusASCIIDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
}

// Management

void MCALL CKingBusASCIIDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);

	m_pList = new CPollAddr *[KING_MAX];

	memset(m_pList, 0, KING_MAX);
}

void MCALL CKingBusASCIIDriver::Open(void)
{
}

// Device

CCODE MCALL CKingBusASCIIDriver::DeviceOpen(IDevice *pDevice)
{
	return CMasterDriver::DeviceOpen(pDevice);
}

CCODE MCALL CKingBusASCIIDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
}

CCODE MCALL CKingBusASCIIDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	UINT uType = Addr.a.m_Type;

	UINT uOffset = Addr.a.m_Offset;

	Register(uOffset);

	UINT uTable = Addr.a.m_Table & 0x0F;

	if( DoPoll() ) {

		Start();

		AddPollingAddress(uOffset);

		AddTerm();

		if( Transact() ) {

			GetRead(uTable, uType, pData);

			ResetRetry();

			return uCount;
		}

		return Retry() ? uCount : CCODE_ERROR;
	}

	GetSavedData(uTable, uType, pData);

	return uCount;
}

CCODE MCALL CKingBusASCIIDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	UINT uTable = Addr.a.m_Table & 0x0F;

	switch( uTable ) {

		case SPACE_S:
		case SPACE_UW:

			break;

		default:
			return uCount;
	}

	Start();

	AddPollingAddress(Addr.a.m_Offset);

	AddWrite(uTable, Addr.a.m_Type, pData);

	AddTerm();

	UINT uResult = CCODE_ERROR;

	if( Tx() ) {

		Sleep(1000);

		DWORD    Data[1];

		CAddress pAddr;

		pAddr.a.m_Table  = GetReadTable(uTable);
		pAddr.a.m_Offset = Addr.a.m_Offset;
		pAddr.a.m_Extra  = 0;
		pAddr.a.m_Type   = addrLongAsLong;

		uResult = Read(Addr, Data, 1);

		if( uResult == uCount ) {

			Register(Addr.a.m_Offset);

			m_pPoll->m_uTime = 0;
		}

		if( uTable != GetReadTable(uTable) ) {

			return uResult;
		}

		if( Data[0] == pData[0] ) {

			return uCount;
		}
	}

	return uResult;
}

// Frame Building

void  CKingBusASCIIDriver::Start(void)
{
	m_uPtr = 0;

	m_wCheck = 0;

	AddByte(0x23);
}

void  CKingBusASCIIDriver::AddByte(BYTE bByte)
{
	m_bTx[m_uPtr++] = bByte;

	m_wCheck += bByte;
}

void  CKingBusASCIIDriver::AddSpace(void)
{
	AddByte(0x20);
}

void  CKingBusASCIIDriver::AddTerm(void)
{
	AddByte(0x2A);
}

void  CKingBusASCIIDriver::AddPollingAddress(UINT uAddr)
{
	for( UINT u = 100; u >= 1; u /= 10 ) {

		AddByte(m_pHex[uAddr / u % 10]);
	}
}

void CKingBusASCIIDriver::AddLong(UINT uTable, PDWORD pData)
{
	UINT uSize = GetSize(uTable);

	LONG lDP = 1;

	UINT uDP = GetDecimalPosition(uTable);

	UINT uAdj = uDP ? 2 : 1;

	for( UINT u = 0; u < uSize - uAdj; u++ ) {

		lDP *= 10;
	}

	if( uTable != SPACE_S ) {

		if( I2R(pData[0] < 0.0) ) {

			AddByte('-');

			lDP /= 10;

			uSize -= 1;
		}
	}

	for( UINT n = 0; n < uSize; lDP /= 10, n++ ) {

		if( uDP && n == uDP ) {

			AddByte('.');

			uSize -= 1;
		}

		AddByte(m_pHex[pData[0] / lDP % 10]);
	}
}

void CKingBusASCIIDriver::AddReal(UINT uTable, PDWORD pData)
{
	UINT uPtr = m_uPtr;

	C3REAL Data = I2R(pData[0]);

	if( Data < 0.0 ) {

		AddByte('-');

		Data = Data * -1;
	}

	UINT uSize = GetSize(uTable);

	LONG lDP = 1;

	for( UINT u = 0; u < uSize; u++ ) {

		lDP *= 10;
	}

	BOOL fFirst = FALSE;

	while( lDP ) {

		UINT uDigit = UINT(Data / lDP);

		if( uDigit && !fFirst ) {

			fFirst = TRUE;
		}

		if( fFirst ) {

			AddByte(m_pHex[uDigit]);

			Data -= (uDigit * lDP);

		}

		lDP /= 10;

	}

	if( !fFirst ) {

		AddByte('0');
	}

	uSize += uPtr;

	if( m_uPtr < uSize ) {

		AddByte('.');

		uSize -= 1;
	}

	while( m_uPtr < uSize ) {

		UINT uDigit = UINT(Data / 0.1);

		AddByte(m_pHex[uDigit]);

		Data -= (uDigit * 0.1);

		Data *= 10;
	}
}

void  CKingBusASCIIDriver::AddWrite(UINT uTable, UINT uType, PDWORD pData)
{
	AddSpace();

	if( uTable == SPACE_UW ) {

		AddByte(m_pHex[pData[0] % 10]);

		return;
	}

	switch( uType ) {

		case addrLongAsLong:

			AddLong(uTable, pData);
			break;

		case addrLongAsReal:
		case addrRealAsReal:

			AddReal(uTable, pData);
			break;
	}
}

// Transport Layer

BOOL  CKingBusASCIIDriver::Transact(void)
{
	if( Tx() && Rx() ) {

		if( CheckFrame() ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL  CKingBusASCIIDriver::Tx(void)
{
	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
}


BOOL  CKingBusASCIIDriver::Rx(void)
{
	UINT uTimer    = 0;

	UINT uData     = 0;

	UINT uState    = 0;

	UINT uSize     = 3;

	UINT uPos      = 0;

	UINT uShift    = 12;

	WORD wCheck    = 0;

	m_wCheck       = 0;

	m_uPtr	       = 0;

	SetTimer(m_uTimeout);

	while( (uTimer = GetTimer()) ) {

		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
		}

		if( uData == LF ) {

			return (m_wCheck == wCheck);
		}

		if( uSize == 0 ) {

			wCheck |= (FromASCII(uData) << uShift);

			uShift -= 4;
		}

		if( m_uPtr - uPos < uSize ) {

			m_bRx[m_uPtr++] = uData;

			m_wCheck += uData;

			if( uState != SPACE_X ) {

				continue;
			}
		}

		if( IsMarker(m_uPtr) ) {

			m_wCheck += uData;
		}

		uPos = m_uPtr;

		uSize = GetSize(++uState);

		Sleep(10);
	}

	return FALSE;
}

BOOL  CKingBusASCIIDriver::CheckFrame(void)
{
	for( UINT u = 0; u < 3; u++ ) {

		if( m_bTx[u + 1] != m_bRx[u] ) {

			return FALSE;
		}
	}

	return TRUE;
}


// Implementation

UINT  CKingBusASCIIDriver::GetSize(UINT uTable)
{
	switch( uTable ) {

		case SPACE_S:

			return 5;

		case SPACE_X:

			return 1;

		case SPACE_L:

			return 8;

		case SPACE_U:

			return 4;
	}

	return 0;
}


UINT  CKingBusASCIIDriver::GetOffset(UINT uTable)
{
	switch( uTable ) {

		case SPACE_S:

			return 3;

		case SPACE_X:

			return 8;

		case SPACE_L:

			return 9;

		case SPACE_U:

			return 17;
	}

	return 0;
}

BOOL  CKingBusASCIIDriver::IsMarker(UINT uPos)
{
	switch( uPos ) {

		case 3:
		case 8:
		case 17:
			return TRUE;
	}

	return FALSE;
}

void  CKingBusASCIIDriver::ResetRetry(void)
{
	if( m_pPoll ) {

		m_pPoll->m_bRetry = 0;
	}
}

BOOL  CKingBusASCIIDriver::Retry(void)
{
	if( m_pPoll ) {

		m_pPoll->m_bRetry++;

		if( m_pPoll->m_bRetry > 8 ) {

			ResetRetry();

			return FALSE;
		}

		else if( m_pPoll->m_bRetry > 7 ) {

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

void  CKingBusASCIIDriver::Register(UINT uPoll)
{
	for( UINT u = 0; u < KING_MAX; u++ ) {

		if( m_pList[u] ) {

			if( m_pList[u]->m_bPoll == uPoll - 1 ) {

				m_pPoll = m_pList[u];

				if( m_pPoll->m_bPoll == m_uPollMin ) {

					m_uTimeMin = GetTimeStamp();
				}

				else if( m_pPoll->m_bPoll == m_uPollMax ) {

					m_uTimeMax = GetTimeStamp();
				}

				return;
			}

			continue;
		}

		m_pList[u] = new CPollAddr;

		m_pPoll = m_pList[u];

		m_pPoll->m_bPoll = uPoll - 1;

		if( !m_fInit ) {

			m_uPollMin = m_pPoll->m_bPoll;

			m_fInit = TRUE;
		}

		m_uPollMax = m_pPoll->m_bPoll;

		m_pPoll->m_uSpaceL = 0;

		m_pPoll->m_uSpaceS = 0;

		m_pPoll->m_uSpaceU = 0;

		m_pPoll->m_uSpaceX = 0;

		m_pPoll->m_uTime = 0;

		m_pPoll->m_bRetry = 0;

		return;
	}
}

BOOL  CKingBusASCIIDriver::DoPoll(void)
{
	if( m_pPoll ) {

		if( m_pPoll->m_uTime != 0 ) {

			if( m_pPoll->m_uTime > GetTimeStamp() - (m_uTimeMax - m_uTimeMin) ) {

				return FALSE;
			}
		}
	}

	return TRUE;
}

void  CKingBusASCIIDriver::SaveData(PDWORD pData)
{
	if( m_pPoll ) {

		BYTE bData[10];

		PBYTE pRead = &bData[0];

		UINT uSize = GetSize(SPACE_L);

		memcpy(pRead, m_bRx + GetOffset(SPACE_L), uSize);

		m_pPoll->m_uSpaceL = MotorToHost(GetReal(pRead, uSize));

		uSize = GetSize(SPACE_S);

		memcpy(pRead, m_bRx + GetOffset(SPACE_S), uSize);

		m_pPoll->m_uSpaceS = MotorToHost(GetLong(pRead, uSize, SPACE_S));

		memcpy(pRead, m_bRx + GetOffset(SPACE_X), GetSize(SPACE_X));

		m_pPoll->m_uSpaceX = pRead[0];

		uSize = GetSize(SPACE_U);

		memcpy(pRead, m_bRx + GetOffset(SPACE_U), uSize);

		m_pPoll->m_uSpaceU = MotorToHost(GetString(pRead, uSize));

		m_pPoll->m_uTime = GetTimeStamp();
	}
}

void CKingBusASCIIDriver::GetSavedData(UINT uTable, UINT uType, PDWORD pData)
{
	pData[0] = 0;

	if( m_pPoll ) {

		switch( uTable ) {

			case SPACE_L:

				pData[0] = m_pPoll->m_uSpaceL;
				break;

			case SPACE_S:

				pData[0] = m_pPoll->m_uSpaceS;
				break;

			case SPACE_X:

				pData[0] = m_pPoll->m_uSpaceX;
				break;

			case SPACE_U:

				pData[0] = m_pPoll->m_uSpaceU;
				break;
		}
	}
}

// Helpers

BOOL CKingBusASCIIDriver::IsASCII(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return TRUE;
	}

	if( bData >= 'A' && bData <= 'F' ) {

		return TRUE;
	}

	return FALSE;
}


BYTE CKingBusASCIIDriver::FromASCII(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return bData - '0';
	}

	if( bData >= 'A' && bData <= 'F' ) {

		return bData - 'A' + 10;
	}

	return 0;
}

void  CKingBusASCIIDriver::GetRead(UINT uTable, UINT uType, PDWORD pData)
{
	BYTE bData[10];

	PBYTE pRead = &bData[0];

	SaveData(pData);

	memcpy(pRead, m_bRx + GetOffset(uTable), GetSize(uTable));

	UINT uSize = GetSize(uTable);

	switch( uTable ) {

		case SPACE_S:
		case SPACE_L:

			pData[0] = MotorToHost(uType == addrLongAsLong ? GetLong(pRead, uSize, uTable)
					       : GetReal(pRead, uSize));
			break;

		case SPACE_X:

			pData[0] = pRead[0];
			break;

		case SPACE_U:

			pData[0] = MotorToHost(GetString(pRead, uSize));
			break;

	}
}

DWORD  CKingBusASCIIDriver::GetReal(PBYTE pData, UINT uSize)
{
	DWORD dwData = 0;

	C3REAL Real = 0.0;

	LONG lDP = 0;

	BOOL fNeg = FALSE;

	for( UINT u = 0; u < uSize; u++ ) {

		if( pData[u] == '-' ) {

			fNeg = TRUE;

			continue;
		}

		else if( pData[u] == '.' ) {

			lDP = 10;

			continue;
		}

		if( (pData[u] >= '0') && (pData[u] <= '9') ) {

			if( !lDP ) {

				Real = (Real * 10) + (pData[u] - '0');

			}

			else {
				UINT uDigit = pData[u] - '0';

				if( uDigit ) {

					C3REAL u = uDigit;

					C3REAL l = lDP;

					Real += (u / l);

				}

				lDP *= 10;

			}
		}
	}

	Real = fNeg ? Real * -1 : Real;

	dwData = R2I(Real);

	return dwData;
}

DWORD  CKingBusASCIIDriver::GetLong(PBYTE pData, UINT uSize, UINT uTable)
{
	DWORD dwData = 0;

	LONG lDP = 1;

	UINT u = 0;

	UINT uAdj = GetDecimalPosition(uTable) ? 2 : 1;

	for( u = 0; u < uSize - uAdj; u++ ) {

		lDP *= 10;
	}

	for( u = 0; u < uSize; u++ ) {

		if( IsASCII(pData[u]) ) {

			dwData += (FromASCII(pData[u]) * lDP);

			lDP /= 10;
		}
	}

	return dwData;
}

DWORD  CKingBusASCIIDriver::GetString(PBYTE pData, UINT uSize)
{
	DWORD dwData = 0;

	LONG lDP = 0;

	for( UINT u = 0; u < uSize; u++ ) {

		dwData |= (pData[u] << lDP);

		lDP += 8;
	}

	return dwData;
}

UINT   CKingBusASCIIDriver::GetReadTable(UINT uTable)
{
	switch( uTable ) {

		case SPACE_UW:

			return SPACE_U;
	}

	return uTable;
}

UINT  CKingBusASCIIDriver::GetDecimalPosition(UINT uTable)
{
	switch( uTable ) {

		case SPACE_S:

			return 1;
	}

	return 0;
}


// End of File
