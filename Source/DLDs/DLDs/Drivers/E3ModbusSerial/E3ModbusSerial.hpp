
#include "intern.hpp"

#include "Modbus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// EtherTrak3 Modbus Serial Driver
//

class CE3ModbusSerialDriver : public CModbusDriver
{
	public:
		// Constructor
		CE3ModbusSerialDriver(void);

		// Destructor
		~CE3ModbusSerialDriver(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);

		// Entry Points
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
	};

// End of File
