//////////////////////////////////////////////////////////////////////////
//
// Beckhoff TCP Data Spaces
//

// Table Items
#define	SP_MEMBYTE	0x01
#define	SP_INPUTS	0x02
#define	SP_OUTPUTS	0x03

// Named Items
#define	SP_ADSSTATE	0x0A
#define	SP_DEVSTATE	0x0B
#define	SP_WRCONT	0x0C
#define	SP_WRCADS	0x0D
#define	SP_WRCDEV	0x0E

// Command ID's
#define	CID_RDEVINFO	1 // Read Device Info
#define	CID_READ	2 // Read
#define	CID_WRITE	3 // Write
#define	CID_RSTATE	4 // Read State
#define	CID_WCONTROL	5 // Write Control 
#define	CID_ADEVN	6 // Add Device Notification
#define	CID_DDEVN	7 // Delete Device Notification
#define	CID_DEVN	8 // Device Notification
#define	CID_RW		9 // Read Write

// State Flags
#define	S_REQ	0
#define	S_RSP	1
#define	S_ADS	4

// Comm Buffer Equates
#define	B_TARGETID	 6
#define	B_TARGETPORT	12
#define	B_SOURCEID	14
#define	B_SOURCEPORT	20
#define	B_COMMAND	22
#define	B_STATE		24
#define	B_LENGTH	26
#define	B_ERROR		30
#define	B_INVOKE	34
#define	B_DATA		38
#define	B_DATA_ERR	38
#define	B_DATALEN	B_DATA + 4
#define	B_ADSSTATE	B_DATA + 4
#define	B_DEVSTATE	B_DATA + 6
#define	B_DATAPOS	B_DATA + 8

// Response Codes
#define	NORETRY		CCODE_ERROR | CCODE_NO_RETRY
#define	RFAIL		CCODE_ERROR

// Miscellaneous
#define	FIXEDRWSIZE	12
#define	FIXEDWCSIZE	 8
#define	MEMBYTEADDR	0x20400000 // 4020 swapped
#define	INPBYTEADDR	0x20F00000 // F020 swapped
#define	INPBITADDR	0x21F00000 // F021 swapped
#define	OUTBYTEADDR	0x30F00000 // F030 swapped
#define	OUTBITADDR	0x31F00000 // F031 swapped

// Error Codes
/*
0x000 0 no error 
0x001 1 Internal error 
0x002 2 No Rtime 
0x003 3 Allocation locked memory error 
0x004 4 Insert mailbox error 
0x005 5 Wrong receive HMSG 
0x006 6 target port not found 
0x007 7 target machine not found 
0x008 8 Unknown command ID 
0x009 9 Bad task ID 
0x00A 10 No IO 
0x00B 11 Unknown AMS command 
0x00C 12 Win 32 error 
0x00D 13 Port not connected 
0x00E 14 Invalid AMS length 
0x00F 15 Invalid AMS Net ID 
0x010 16 Low Installation level 
0x011 17 No debug available 
0x012 18 Port disabled 
0x013 19 Port already connected 
0x014 20 AMS Sync Win32 error 
0x015 21 AMS Sync Timeout 
0x016 22 AMS Sync AMS error 
0x017 23 AMS Sync no index map 
0x018 24 Invalid AMS port 
0x019 25 No memory 
0x01A 26 TCP send error 
0x01B 27 Host unreachable 
   
0x500 1280 Router: no locked memory 
0x502 1282 Router: mailbox full 
   
0x700 1792 error class <device error> 
0x701 1793 Service is not supported by server 
0x702 1794 invalid index group 
0x703 1795 invalid index offset 
0x704 1796 reading/writing not permitted 
0x705 1797  parameter size not correct 
0x706 1798 invalid parameter value(s) 
0x707 1799 device is not in a ready state 
0x708 1800 device is busy 
0x709 1801 invalid context (must be in Windows) 
0x70A 1802 out of memory 
0x70B 1803 invalid parameter value(s) 
0x70C 1804 not found (files, ...) 
0x70D 1805 syntax error in command or file 
0x70E 1806 objects do not match 
0x70F 1807 object already exists 
0x710 1808 symbol not found 
0x711 1809 symbol version invalid 
0x712 1810 server is in invalid state 
0x713 1811 AdsTransMode not supported 
0x714 1812 Notification handle is invalid 
0x715 1813 Notification client not registered 
0x716 1814 no more notification handles 
0x717 1815 size for watch too big 
0x718 1816 device not initialized 
0x719 1817 device has a timeout 
0x71A 1818 query interface failed 
0x71B 1819 wrong interface required 
0x71C 1820 class ID is invalid 
0x71D 1821 object ID is invalid 
0x71E 1822 request is pending 
0x71F 1823 request is aborted 
0x720 1824 signal warning 
0x721 1825 invalid array index 
0x740 1856 Error class <client error> 
0x741 1857 invalid parameter at service 
0x742 1858 polling list is empty 
0x743 1859 var connection already in use 
0x744 1860 invoke ID in use 
0x745 1861 timeout elapsed 
0x746 1862 error in win32 subsystem 
0x748 1864 ads-port not opened 
0x750 1872 internal error in ads sync 
0x751 1873 hash table overflow 
0x752 1874 key not found in hash 
0x753 1875 no more symbols in cache 
*/

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff TCP/IP Master Driver
//

#pragma pack(1)

struct BeckhoffHead
{
	WORD	rsv;	// reserved, always 0
	LONG	ltotal; // Total Packet Length
	BYTE	tid[6];	// Target ID
	WORD	tport;	// Target Port
	BYTE	sid[6];	// Source ID
	WORD	sport;	// Source Port
	WORD	cmd;	// Command ID
	WORD	state;	// State Flags
	LONG	ldata;	// Length of Data
	LONG	error;	// Error Code
	LONG	invoke;	// Invoke ID
	};

struct BeckhoffData
{
	LONG	group;
	LONG	offset;
	LONG	length;
	};

struct BeckhoffWCntl
{
	WORD	ADS;
	WORD	DEV;
	LONG	length;
	};

#pragma pack()

#define	HSZ	sizeof(BeckhoffHead)
#define	DSZ	sizeof(BeckhoffData)
#define	WSZ	sizeof(BeckhoffWCntl)

class CBeckhoffPlusTCPDriver : public CMasterDriver
{
	public:
		// Constructor
		CBeckhoffPlusTCPDriver(void);

		// Destructor
		~CBeckhoffPlusTCPDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Open(void);
		
		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			UINT	 m_AMSPort;
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			DWORD	 m_dTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			UINT	 m_WRCADS;
			UINT	 m_WRCDEV;

			PBYTE	 m_pDevName;
			UINT	 m_uAddrCount;
			PDWORD	 m_pAddrList;
			UINT	 m_uNameCount;
			PDWORD	 m_pNameList;
			};

		// Data Members
		CContext * m_pCtx;

		BYTE	m_bTx[300];
		BYTE	m_bRx[300];
		UINT	m_uPtr;
		UINT	m_uKeep;
		IPADDR	m_IP;

		struct	BeckhoffHead	BPH;
		struct	BeckhoffData	BPD;
		struct	BeckhoffWCntl	BPC;
		
		// Frame Building
		void	PutRWHeader(AREF Addr, UINT uCmd, UINT uSize, UINT uDFLen);
		void	StartFrame(UINT uCmdID);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddReal(DWORD dwData);
				
		// Transport Layer
		BOOL	SendFrame(void);
		BOOL	RecvFrame(void);
		BOOL	Transact(void);
		CCODE	CheckFrame(UINT uCount);
		BOOL	MisCompare(UINT u1, UINT u2, BOOL fIsDWORD);

		// Read Handlers
		BOOL	DoBitRead(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL	DoByteRead(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL	DoWordRead(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL	DoLongRead(AREF Addr, PDWORD pData, UINT * pCount, BOOL fIsReal);

		// Write Handlers
		BOOL	DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	DoByteWrite(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	DoLongWrite(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsReal);

		// Helpers
		void	AddNetInfo(void);
		void	AddNetID(void);
		void	AddNetID(DWORD dID, PBYTE pDest);
		void	AddPort(void);
		void	AddCmd(UINT uCmdID);
		void	AddState(WORD wState);
		void	AddInvoke(void);
		void	AddGroup(UINT uTable, BOOL fIsByte);
		void	AddIndex(UINT uOffset);
		void	PutDataLength(UINT uLen);
		void	AddDataFrameLength(DWORD dLength);
		UINT	PutTotalLength(void);
		DWORD	GetRxLong(UINT uOffset, BOOL fIsReal);
		WORD	GetRxWord(UINT uOffset);
		DWORD	SwapLong(DWORD dData);
		WORD	SwapWord(WORD wData);
		void	CopyToTx(PBYTE pSource, UINT uSize);
		BOOL	NoError(void);

		// Socket Management
		BOOL	CheckSocket(void);
		BOOL	OpenSocket(void);
		void	CloseSocket(BOOL fAbort);
	};

// End of File
