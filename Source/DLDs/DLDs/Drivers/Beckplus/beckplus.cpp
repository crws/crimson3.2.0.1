
#include "intern.hpp"

#include "beckplus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Plus TCP Driver
//

// Instantiator

INSTANTIATE(CBeckhoffPlusTCPDriver);

// Constructor

CBeckhoffPlusTCPDriver::CBeckhoffPlusTCPDriver(void)
{
	m_Ident  = DRIVER_ID;

	m_uKeep  = 0;
	}

// Destructor

CBeckhoffPlusTCPDriver::~CBeckhoffPlusTCPDriver(void)
{
	}

// Configuration

void MCALL CBeckhoffPlusTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CBeckhoffPlusTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CBeckhoffPlusTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CBeckhoffPlusTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_AMSPort	= GetWord(pData);
			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_dTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			m_pCtx->m_WRCADS	= 0;
			m_pCtx->m_WRCDEV	= 0;

			UINT i;

			UINT uCount		= GetWord(pData);
AfxTrace1("\r\n%d ", uCount);
			m_pCtx->m_pDevName	= new BYTE[uCount];

			for( i = 0; i < uCount; i++ ) {

				m_pCtx->m_pDevName[i] = GetByte(pData);
AfxTrace1("[%2.2x]", m_pCtx->m_pDevName[i]);
				}

/*			uCount			= GetWord(pData);

			m_pCtx->m_uAddrCount	= uCount;

			m_pCtx->m_pAddrList	= new DWORD[uCount];

			for( i = 0; i < uCount; i++ ) {

				m_pCtx->m_pAddrList[i] = GetLong(pData);
				}

			uCount			= GetWord(pData);

			m_pCtx->m_uNameCount	= uCount;

			m_pCtx->m_pNameList	= new DWORD[uCount];

			for( i = 0; i < uCount; i++ ) {

				m_pCtx->m_pNameList[i] = GetLong(pData);
				}
*/
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CBeckhoffPlusTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CBeckhoffPlusTCPDriver::Ping(void)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	DWORD    Data[1];
	
	CAddress Addr;

	Addr.a.m_Table  = SP_MEMBYTE;
		
	Addr.a.m_Offset = 0;
		
	Addr.a.m_Type   = addrByteAsByte;

	Addr.a.m_Extra	= 0;
	
	return Read(Addr, Data, 1);
	}

CCODE MCALL CBeckhoffPlusTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	UINT uType = Addr.a.m_Type;

	switch( uType ) {

		case addrBitAsBit:

			if( Addr.a.m_Table == addrNamed ) {

				pData[0] = 0; // write only
				return 1;
				}

			uCount = 1; // protocol returns only one bit

			if( !DoBitRead(Addr, pData, &uCount) ) return CCODE_ERROR;

			break;

		case addrByteAsByte:

			MakeMin( uCount, 32 );

			if( !DoByteRead(Addr, pData, &uCount) ) return CCODE_ERROR;

			break;

		case addrWordAsWord:

			MakeMin( uCount, 16 );

			if( !DoWordRead(Addr, pData, &uCount) ) return CCODE_ERROR;

			break;

		case addrLongAsLong:
		case addrRealAsReal:

			MakeMin( uCount, 8 );

			if( !DoLongRead(Addr, pData, &uCount, uType == addrRealAsReal) ) return CCODE_ERROR;

			break;

		default: return CCODE_ERROR | CCODE_HARD;
		}

	return CheckFrame(uCount);
	}

CCODE MCALL CBeckhoffPlusTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

//**/	AfxTrace0("\r\n***** WRITE *****");

	if( Addr.a.m_Table == SP_INPUTS )  return uCount;

	UINT uType = Addr.a.m_Type;

	switch( uType ) {

		case addrBitAsBit:

			uCount = 1;

			if( !DoBitWrite(Addr, pData, uCount) ) return CCODE_ERROR;

			break;

		case addrByteAsByte:

			MakeMin( uCount, 32 );
			
			if( !DoByteWrite(Addr, pData, uCount) ) return CCODE_ERROR;

			break;

		case addrWordAsWord:

			MakeMin( uCount, 16 );
			
			if( !DoWordWrite(Addr, pData, uCount) ) return CCODE_ERROR;

			break;

		case addrLongAsLong:
		case addrRealAsReal:

			MakeMin( uCount, 8 );

			if( !DoLongWrite(Addr, pData, uCount, uType == addrRealAsReal) ) return CCODE_ERROR;

			break;

		default: return CCODE_ERROR | CCODE_HARD;
		}

	return CheckFrame(uCount);
	}

// Socket Management

BOOL CBeckhoffPlusTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					m_pCtx->m_pSock->GetLocal(m_IP);

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		}

	return FALSE;
	}

void CBeckhoffPlusTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

void CBeckhoffPlusTCPDriver::PutRWHeader(AREF Addr, UINT uCmd, UINT uSize, UINT uDFLen)
{
	StartFrame(uCmd);

	AddDataFrameLength( uDFLen );

	PutDataLength(uSize);

	AddGroup(Addr.a.m_Table, Addr.a.m_Type == addrByteAsByte);

	AddIndex(Addr.a.m_Offset);

	CopyToTx( PBYTE(&BPD), DSZ );
	}

void CBeckhoffPlusTCPDriver::StartFrame(UINT uCmdID)
{
	m_uPtr		= 0;

	BPH.rsv		= 0;

	BPH.ltotal	= 0;

	BPH.ldata	= 0;

	BPH.error	= 0;

	AddNetInfo();

	AddCmd(uCmdID);

	AddState(S_ADS);

	AddInvoke();
	}

void CBeckhoffPlusTCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {
	
		m_bTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CBeckhoffPlusTCPDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CBeckhoffPlusTCPDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CBeckhoffPlusTCPDriver::AddReal(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CBeckhoffPlusTCPDriver::SendFrame(void)
{
	UINT uSize  = PutTotalLength();
	UINT uTotal = uSize;

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("[%2.2x]", m_bTx[k] );

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == uTotal ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	UINT uTotal = 0;

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

//**/			for( UINT k = 0; k < uSize; k++ ) AfxTrace1("<%2.2x>", m_bRx[k] );

			uPtr += uSize;

			if( !uTotal ) {

				if( uPtr >= 6 ) {

					uTotal = GetRxLong(2, FALSE) + 6;
					}
				}

			if( uTotal ) {

				if( uPtr >= uTotal ) return TRUE;
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTCPDriver::Transact(void)
{
	if( SendFrame() && RecvFrame() ) return NoError();

	CloseSocket(TRUE);

	return FALSE;
	}

CCODE CBeckhoffPlusTCPDriver::CheckFrame(UINT uCount)
{
	if( *PDWORD(m_bRx + B_ERROR) ) return FALSE;

	if( GetRxWord(B_STATE) != (S_ADS | S_RSP) ) return NORETRY;

	if( MisCompare( B_TARGETID, B_SOURCEID, TRUE ) ) return NORETRY;

	if( MisCompare( B_TARGETID+4, B_SOURCEID+4, TRUE ) ) return NORETRY;

	if( MisCompare( B_SOURCEID, B_TARGETID, TRUE ) ) return NORETRY;

	if( MisCompare( B_SOURCEID+4, B_TARGETID+4, TRUE ) ) return NORETRY;

	if( MisCompare( B_INVOKE, B_INVOKE, TRUE ) ) return NORETRY;

	if( MisCompare( B_COMMAND, B_COMMAND, FALSE ) ) return NORETRY;

	return uCount;
	}

BOOL CBeckhoffPlusTCPDriver::MisCompare(UINT u1, UINT u2, BOOL fIsDWORD )
{
	if( fIsDWORD ) return *PDWORD(m_bRx + u1) != *PDWORD(m_bTx + u2 );

	return *PWORD(m_bRx + u1) != *PWORD(m_bTx + u2);
	}

// Read Handlers

BOOL CBeckhoffPlusTCPDriver::DoBitRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	if( Addr.a.m_Table == addrNamed ) return FALSE;

	PutRWHeader( Addr, CID_READ, *pCount, FIXEDRWSIZE );
	
	if( Transact() ) {

		UINT uCt = min( *pCount, LOWORD(GetRxLong(B_DATALEN, FALSE)) );

		for( UINT n = 0; n < uCt; n++ ) {
			
			pData[n] = m_bRx[B_DATAPOS + n] ? 1 : 0;
			}

		*pCount = uCt;

		return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTCPDriver::DoByteRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	if( Addr.a.m_Table == addrNamed ) return FALSE;

	PutRWHeader( Addr, CID_READ, *pCount, FIXEDRWSIZE );
	
	if( Transact() ) {

		UINT uCt = min( *pCount, LOWORD(GetRxLong(B_DATALEN, FALSE)) );

		for( UINT n = 0; n < uCt; n++ ) {
			
			pData[n] = m_bRx[B_DATAPOS + n];
			}

		*pCount = uCt;

		return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTCPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uOffset  = Addr.a.m_Offset;
	UINT uItemPos = B_ADSSTATE;

	switch( Addr.a.m_Table ) {
	
		case SP_MEMBYTE:
		case SP_INPUTS:
		case SP_OUTPUTS:

			PutRWHeader( Addr, CID_READ, *pCount + 1, FIXEDRWSIZE );

			break;
			
		case addrNamed:

			switch( uOffset ) {

				case SP_DEVSTATE:

					uItemPos = B_DEVSTATE; // then fall through

				case SP_ADSSTATE:
				
					StartFrame(CID_RSTATE);

					AddDataFrameLength(0);

					memcpy(m_bTx, &BPH, HSZ);

					m_uPtr = HSZ;

					break;

				case SP_WRCADS:

					*pData  = DWORD( m_pCtx->m_WRCADS );

					*pCount = 1;

					return TRUE;

				case SP_WRCDEV:

					*pData  = DWORD( m_pCtx->m_WRCDEV );

					*pCount = 1;

					return TRUE;

				default:
					return FALSE;
				}

			break;
			
		default:
			return FALSE;
		}
	
	if( Transact() ) {

		if( Addr.a.m_Table == addrNamed ) {

			pData[0] = DWORD( GetRxWord(uItemPos) );

			*pCount = 1;

			return TRUE;
			}

		UINT uCt = min( *pCount, GetRxLong(B_DATALEN, FALSE) - 1 );
		
		for( UINT n = 0; n < uCt; n++ ) {
		
			WORD x   = GetRxWord(B_DATAPOS + n );
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		*pCount = uCt;

		return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTCPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT * pCount, BOOL fIsReal)
{
	if( Addr.a.m_Table == addrNamed ) return FALSE;

	PutRWHeader( Addr, CID_READ, *pCount + 3, FIXEDRWSIZE );
	
	if( Transact() ) {

		UINT uCt = min( *pCount, GetRxLong(B_DATALEN, FALSE) - 3 );
		
		for( UINT n = 0; n < uCt; n++ ) {
		
			DWORD x  = GetRxLong(B_DATAPOS + n, fIsReal );
			
			pData[n] = MotorToHost(x);
			}

		*pCount = uCt;

		return TRUE;
		}

	return FALSE;
	}

// Write Handlers

BOOL CBeckhoffPlusTCPDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == addrNamed ) {

		if( Addr.a.m_Offset == SP_WRCONT ) {

			StartFrame(CID_WCONTROL);

			AddDataFrameLength(FIXEDWCSIZE);

			BPC.ADS = SwapWord(m_pCtx->m_WRCADS);
			BPC.DEV = SwapWord(m_pCtx->m_WRCDEV);
			BPC.length = 0;

			CopyToTx( PBYTE(&BPC), WSZ );
			}

		else return FALSE;
		}

	else {
		PutRWHeader( Addr, CID_WRITE, uCount, FIXEDRWSIZE + uCount );

		for( UINT i = 0; i < uCount; i++ ) {

			AddByte( pData[i] ? 1 : 0 );
			}
		}

	return Transact();
	}

BOOL CBeckhoffPlusTCPDriver::DoByteWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	PutRWHeader( Addr, CID_WRITE, uCount, FIXEDRWSIZE + uCount );

	for( UINT i = 0; i < uCount; i++ ) {

		AddByte( LOBYTE(pData[i]) );
		}

	return Transact();
	}

BOOL CBeckhoffPlusTCPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	UINT i = 0;

	UINT uDataBytes = uCount * sizeof(WORD);

	switch( Addr.a.m_Table ) {

		case SP_MEMBYTE:
		case SP_OUTPUTS:

			PutRWHeader( Addr, CID_WRITE, uDataBytes, FIXEDRWSIZE + uDataBytes );

			for( i = 0; i < uCount; i++ ) {

				AddWord( LOWORD(pData[i]) );
				}

			return Transact();

		case addrNamed:

			switch( uOffset ) {

				case SP_WRCADS:

					m_pCtx->m_WRCADS = *pData;
					break;

				case SP_WRCDEV:
					m_pCtx->m_WRCDEV = *pData;
					break;

				case SP_ADSSTATE:
				case SP_DEVSTATE:
					break;
				}

			return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTCPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsReal)
{
	UINT uDataBytes = uCount * sizeof(DWORD);

	PutRWHeader( Addr, CID_WRITE, uDataBytes, FIXEDRWSIZE + uDataBytes );

	for( UINT i = 0; i < uCount; i++ ) !fIsReal ? AddLong( pData[i] ) : AddReal( pData[i] );

	return Transact();
	}

// Helpers

void CBeckhoffPlusTCPDriver::AddNetInfo(void)
{
	AddNetID();

	AddPort();
	}

void CBeckhoffPlusTCPDriver::AddNetID(void)
{
	AddNetID( *PDWORD(&m_IP), &(BPH.sid[0]) );

	AddNetID( m_pCtx->m_IP, &(BPH.tid[0]) );
	}

void CBeckhoffPlusTCPDriver::AddNetID(DWORD dID, PBYTE pDest)
{
	for( UINT i = 0; i < 4; i++ ) {

		*(pDest + i) = HIBYTE(HIWORD(dID));

		dID <<= 8;
		}

	*(pDest + 4) = 1;
	*(pDest + 5) = 1;

	}

void CBeckhoffPlusTCPDriver::AddPort(void)
{
	BPH.tport = SwapWord(m_pCtx->m_AMSPort);
	BPH.sport = SwapWord(m_pCtx->m_AMSPort + 1);
	}

void CBeckhoffPlusTCPDriver::AddCmd(UINT uCmdID)
{
	BPH.cmd = SwapWord(uCmdID);
	}

void CBeckhoffPlusTCPDriver::AddState(WORD wState)
{
	BPH.state = SwapWord(WORD(S_ADS));
	}

void CBeckhoffPlusTCPDriver::AddInvoke(void)
{
	BPH.invoke = SwapLong(m_pCtx->m_dTrans++);
	}

void CBeckhoffPlusTCPDriver::AddGroup(UINT uTable, BOOL fIsByte)
{
	switch( uTable ) {

		case SP_MEMBYTE:	BPD.group = MEMBYTEADDR; break;
		case SP_INPUTS:		BPD.group = fIsByte ? INPBYTEADDR : INPBITADDR; break;
		case SP_OUTPUTS:	BPD.group = fIsByte ? OUTBYTEADDR : OUTBITADDR; break;
		}
	}

void CBeckhoffPlusTCPDriver::AddIndex(UINT uOffset)
{
	BPD.offset = SwapLong(DWORD(uOffset));
	}

void CBeckhoffPlusTCPDriver::PutDataLength(UINT uLen)
{
	BPD.length = SwapLong(DWORD(uLen));
	}

void CBeckhoffPlusTCPDriver::AddDataFrameLength(DWORD dLength)
{
	BPH.ldata  = SwapLong(dLength);
	}

UINT CBeckhoffPlusTCPDriver::PutTotalLength(void)
{
	UINT uTotal = m_uPtr - 6;

	m_bTx[2] = LOBYTE(LOWORD(uTotal));
	m_bTx[3] = HIBYTE(LOWORD(uTotal));
	m_bTx[4] = LOBYTE(HIWORD(uTotal));
	m_bTx[5] = HIBYTE(HIWORD(uTotal));

	return uTotal + 6;
	}

DWORD CBeckhoffPlusTCPDriver::GetRxLong(UINT uOffset, BOOL fIsReal)
{
	DWORD d = SwapLong(*PDWORD(m_bRx + uOffset));

	return !fIsReal ? d : (LOWORD(d) << 16) + HIWORD(d);
	}

WORD CBeckhoffPlusTCPDriver::GetRxWord(UINT uOffset)
{
	return SwapWord(*PWORD(m_bRx + uOffset));
	}

DWORD CBeckhoffPlusTCPDriver::SwapLong(DWORD dData)
{
	return SwapWord(HIWORD(dData)) + ( SwapWord(LOWORD(dData)) << 16 );
	}

WORD CBeckhoffPlusTCPDriver::SwapWord(WORD wData)
{
	return WORD(HIBYTE(wData)) + (WORD(LOBYTE(wData)) << 8);
	}

void CBeckhoffPlusTCPDriver::CopyToTx(PBYTE pSource, UINT uSize)
{
	memcpy(m_bTx, &BPH, HSZ);
	memcpy(m_bTx + HSZ, pSource, uSize);

	m_uPtr = HSZ + uSize;
	}

BOOL CBeckhoffPlusTCPDriver::NoError(void)
{
	return !(*PDWORD(m_bRx + B_DATA_ERR));
	}

// End of File
