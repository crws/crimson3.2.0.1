
#include "intern.hpp"

#include "cruisair.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cruisair Driver
//

// Instantiator

INSTANTIATE(CCruisairDriver);

// Constructor

CCruisairDriver::CCruisairDriver(void)
{
	m_Ident	= DRIVER_ID;

	m_uDisconnect = 0;
	}

// Destructor

CCruisairDriver::~CCruisairDriver(void)
{
	}

// Configuration

void MCALL CCruisairDriver::Load(LPCBYTE pData)
{
	m_uTimeout = GetWord(pData) == 0x1234 ? GetWord(pData) : 5000;
	}
	
// Management

void MCALL CCruisairDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CCruisairDriver::Open(void)
{
	}

// Device

CCODE MCALL CCruisairDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_uChiller = GetWord(pData);

			m_pCtx->m_dPing    = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CCruisairDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CCruisairDriver::Ping(void)
{
	if( !m_pCtx->m_dPing ) m_pCtx->m_dPing = 1;

	if( m_pCtx->m_dPing != 0x12345678 ) {

		if( ModeCheck() ) return CCODE_ERROR; // wait for remote display mode to clear

		if( WakeUp() ) {

			m_pCtx->m_dPing = 0x12345678;

			return 1;
			}
		}

	// Wakeup doesn't respond to repeated calls, so try requesting data.

	DWORD Data[1];

	CAddress Addr;

	Addr.a.m_Table  = AN;
	Addr.a.m_Offset = CSN;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = ISLOCAL;

	if( Read( Addr, Data, 1 ) == 1 ) {

		m_pCtx->m_dPing = 0x12345678;

		return 1;
		}

	Addr.a.m_Table  = AN;
	Addr.a.m_Offset = ERD;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = CGLOBAL;

	Read( Addr, Data, 1 );
	Read( Addr, Data, 1 ); // Double dummy read to resync device

	WakeUp();
	return CCODE_ERROR;
	}

CCODE MCALL CCruisairDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( ReadNoTransmit(Addr.a.m_Table, Addr.a.m_Offset, pData) ) return 1;

//**/	AfxTrace3("\r\n%d %d %d ", m_pCtx->m_uChiller, Addr.a.m_Offset, Addr.a.m_Extra);

	Read( GetParameterNumber(Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Extra) );

	if( Transact(FALSE) ) {

		DWORD dData = ATOI( (char *) &m_bRx[6] );

		*pData = Addr.a.m_Type == LL ? dData : dData ? 1 : 0;

		return 1;
		}

	if( m_pCtx->m_dPing != 0x12345678 ) return CCODE_ERROR;

	if( m_bRx[2] && (m_bRx[2] != m_uChiller + '0') ) return CCODE_ERROR;

	if( m_uDisconnect > 2 ) {

		m_pCtx->m_dPing = 0;

		return CCODE_ERROR; // allow 3 tries of global
		}

	return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;
	}

CCODE MCALL CCruisairDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nWRITE %d %d %d ", m_pCtx->m_uChiller, Addr.a.m_Offset, Addr.a.m_Extra);

	UINT uParam = GetParameterNumber(Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Extra);

	if( !IsWriteable(uParam) ) return uCount;

	GetBitData( Addr, pData );

	Write( uParam, *pData );

	if( Transact(FALSE) ) return 1;

	if( m_pCtx->m_dPing != 0x12345678 ) return CCODE_ERROR;

	if( m_bRx[2] != m_uChiller + '0' ) return CCODE_ERROR;

	if( m_uDisconnect > 2 ) {

		m_pCtx->m_dPing = 0;

		return CCODE_ERROR; // allow 3 tries of global
		}

	return 1;
	}

// Opcode Handlers

void CCruisairDriver::Read(UINT uOpcode)
{
	StartFrame('R');

	char s[6] = {0};

	SPrintf( (char *)s, "%1.1d%3.3d#", m_uChiller, uOpcode );

	AddText( (char *)s );
	}

void CCruisairDriver::Write(UINT uOpcode, DWORD dData)
{
	StartFrame('C');

	char s[11] = {0};

	SPrintf( (char *)s, "%1.1d%3.3d%5.5d#", m_uChiller, uOpcode, dData );

	AddText( (char *)s );
	}

// Frame Building

void CCruisairDriver::StartFrame(BYTE bOp)
{
	m_uPtr = 0;

	AddByte('$');

	AddByte( bOp );
	}

void CCruisairDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CCruisairDriver::AddText(char * sText)
{
	for( UINT i = 0; i < strlen(sText); i++ ) AddByte( sText[i] );
	}

// Transport Layer

BOOL CCruisairDriver::Transact(BOOL fIsWakeup)
{
	Send();

	return GetReply(fIsWakeup);
	}

void CCruisairDriver::Send(void)
{
	m_pData->ClearRx();

	memset(m_bRx, 0, 3);

	Put();
	}

BOOL CCruisairDriver::GetReply(BOOL fIsWakeup)
{
	UINT uTime;

	if( fIsWakeup ) uTime = 500;

	else {
		uTime = m_pCtx->m_dPing == 1 ? 1000 : m_uTimeout;
		}

	UINT uState = fIsWakeup ?  10 : 0;

	UINT uPtr = 0;
	WORD wData;

	SetTimer(uTime);

//**/	AfxTrace0("\r\n");

	while( (uTime = GetTimer()) ) {

		if( (wData = Get(uTime) ) == LOWORD(NOTHING) ) {
		
			Sleep(20);
			
			continue;
			}

		BYTE bData = LOBYTE(wData);

//**/		AfxTrace1("<%2.2x>", bData);

		m_bRx[uPtr++] = bData;

		if( bData == '&' ) { // controller in modem state

			uState = 13;
			uPtr   =  0;
			}

		switch( uState ) {

			case 0:
				if( bData == '%' ) uState = 1;
				else uPtr = 0;
				break;

			case 1:
				if( bData == '@' ) uState = 2;
				break;

			case 2:
				if( bData == LF ) return CheckReply();
				break;

			// Wakeup response
			case 10:
				if( bData == 0x7B ) uState = 11;

				break;

			case 11: if( bData == 'd' ) uState = 12; break;

			case 12: return bData == 'a';

			case 13: if( bData == CR ) {

					WakeUp();

					return TRUE;
					}
			}

		if( uPtr > sizeof(m_bRx) ) return FALSE;
		}

	if( m_bTx[2] == '0' ) m_uDisconnect++; // allow 3 tries of global

	return FALSE;
	}

BOOL CCruisairDriver::CheckReply(void)
{
	m_uDisconnect = 0;

	if( m_bRx[1] == 'N' ) return FALSE;

	for( UINT i = 2; i < 6; i++ ) {

		if( m_bRx[i] != m_bTx[i] ) return FALSE;
		}

	return TRUE;
	}

// Port Access

void CCruisairDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i] );

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

UINT CCruisairDriver::Get(UINT uTimer)
{
	return m_pData->Read(uTimer);
	}

// Helpers

BOOL	CCruisairDriver::ModeCheck(void)
{
	UINT uTime  = 500;
	WORD wData;

	SetTimer(uTime);

	while( GetTimer() ) {

		if( (wData = Get(uTime) ) != LOWORD(NOTHING) ) {

			switch( LOBYTE(wData) ) {

				case 0x7B:
				case  'i':
				case  'h':
				case  'A':
				case  'T': return FALSE; // assume Modem

				default: // assume Remote Display Mode
					m_uPtr = 0;
					AddByte( 0x7B );
					AddByte( 'h'  );
					AddByte( 'a'  );
					Put();
					Sleep(100);
					m_pData->ClearRx();
					return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL	CCruisairDriver::WakeUp(void)
{
	m_uPtr = 0;
	AddByte( 0x7B );
	AddByte( 'd' );
	AddByte( 's' );

	return Transact(TRUE);
	}

UINT	CCruisairDriver::GetParameterNumber(UINT uTable, UINT uOffset, UINT uExtra)
{
	SetChillerNumber(uTable, uOffset, uExtra);

	switch( uTable ) { 

		case AN:	return uOffset;

		case PGEN:	return uOffset % 1000;

		case HSP:
		case HDF:
		case CSP:
		case CDF:	return uTable + ( 4 * (uOffset-1) ); // Stage Values
		}

	return NET;
	}

void	CCruisairDriver::SetChillerNumber(UINT uTable, UINT uOffset, UINT uExtra)
{
	if( uTable == PGEN ) {

		m_uChiller = uOffset / 1000;

		return;
		}

	if( uOffset >= 49 && uOffset <= 85 ) m_uChiller = 0;

	else if( uOffset >= 256 && uOffset <= 265 ) m_uChiller = 0;

	else if( uOffset >= 271 && uOffset <= 271 ) m_uChiller = 0;

	else if( uOffset == 273 || uOffset == 275 ) m_uChiller = 0;

	else if( uOffset >= 277 && uOffset <= 279 ) m_uChiller = 0;

	else if( uOffset == 281 || uOffset == 283 ) m_uChiller = 0;

	else if(  uTable >= HSP &&  uTable <= CDF ) m_uChiller = 0;

	else m_uChiller = m_pCtx->m_uChiller;
	}

BOOL	CCruisairDriver::ReadNoTransmit(UINT uTable, UINT uOffset, PDWORD pData)
{
	if( uTable == AN ) uTable = uOffset;

	switch( uTable ) {

		case SOFF:
		case COOL:
		case HEAT:
		case BOTH:
		case RESH:
		case FAHR:
		case CENT:
		case HILD:
		case LOLD:
		case CINIT:
		case RDEF:
		case TGR:
		case ISN:
		case DSN:
		case TGC:
		case CALL:
		case C1LG:
		case C1FH:
		case CAFH:
		case C1IO:
		case CAIO:
		case CLPH:
		case CSPH:
		case C1HH:
		case CAHH:
		case C1CH:
		case CACH:
		case CDFT:
			*pData = 0;
			return TRUE;

		case BUZZ:
			*pData = RTNZEROS;
			return TRUE;
		}

	return FALSE;
	}

BOOL	CCruisairDriver::IsWriteable(UINT uParam)
{
	if( uParam >= MINWP && uParam <= MAXWP ) return TRUE;

	return (uParam >= MINWC && uParam <= MAXWC );
	}

void	CCruisairDriver::GetBitData(AREF Addr, PDWORD pData)
{
	if( Addr.a.m_Type != BB || Addr.a.m_Table != AN ) return;

	*pData = (Addr.a.m_Offset != BUZZ) ? 11111 : *pData ? 1 : 0;
	}

// End of File
