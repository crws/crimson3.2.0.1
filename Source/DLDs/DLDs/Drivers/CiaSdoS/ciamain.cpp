
#include "intern.hpp"

#include "ciasdo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Slave Driver
//

// Instantiator

INSTANTIATE(CCANOpenSDOSlave);

// End of File