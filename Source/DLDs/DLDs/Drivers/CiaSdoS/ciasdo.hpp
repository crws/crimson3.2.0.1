
#ifndef INCLUDE_CIASDO_HPP

#define INCLUDE_CIASDO_HPP

#include "..\ciasdom\ciabase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define TRANSFER_EXP	0
#define TRANSFER_NORM	1

#define NMTS_BOOT	0
#define NMTS_STOP	4
#define NMTS_OP		5
#define NMTS_PRE_OP	127

#define NMT_COB_ID	1792
#define NMT_START	1
#define NMT_STOP	2
#define NMT_PRE_OP	128
#define NMT_RESET	129
#define NMT_RESET_COMM	130
 
//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Slave Driver
//

class CCANOpenSDOSlave : public CSlaveDriver
{
	public:
		// Constructor
		CCANOpenSDOSlave(void);

		// Destructor
		~CCANOpenSDOSlave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Service
		DEFMETH(void) Service(void);

	protected:
		// Data Members
		CCANPortHandler * m_pData;
		BYTE		  m_bDrop;
		BYTE		  m_bDecode;
		FRAME             m_TxData;
		FRAME             m_RxData;
		UINT		  m_uTime;
		UINT		  m_uState;
		BOOL		  m_fGuard;
		UINT		  m_uGuardTime;
		UINT		  m_uLifeTime;
		UINT		  m_uHeartBeat;
		UINT		  m_uLast;
		
		// Implementation
		BOOL GetFrame(void);
		BOOL PutFrame(UINT uWait = 0);
		void StartSDO(void);
		BOOL SDOAbort(DWORD dwCode, WORD wIndex, BYTE bSub);
		BOOL HandleRead(void);
		BOOL HandleWrite(void);
		UINT GetData(WORD wIndex, BYTE bSub, DWORD &Data);
		BOOL PutData(WORD wIndex, BYTE bSub, UINT uSize, DWORD Data);
		UINT GetSpec(WORD wIndex, BYTE bSub, DWORD &Data);
		BOOL PutSpec(WORD wIndex, BYTE bSub, UINT uSize, DWORD Data);
		BOOL EncodeAddr(CAddress &Addr, WORD wIndex, BYTE bSub);

		// Device Profile Support
		virtual DWORD GetDeviceProfile(void);

		// Extended Slave Support
		virtual void  SaveData(WORD wIndex, DWORD Data);

		// NodeGuarding Support
		BOOL IsNodeGuard(void);
		BOOL HandleNodeGuard(void);

		// NMT Support
		BOOL HandleBootup(void);
		BOOL IsHeartBeat(void);
		BOOL HandleHeartBeat(void);
		BOOL HandleNMT(void);
		
		// Helpers
		void Start(void);
		void SetState(UINT uState);
		BOOL IsDrop(BYTE bDrop, BOOL fWrite);
		BOOL IsThisNode(void);
	};

// End of File

#endif

