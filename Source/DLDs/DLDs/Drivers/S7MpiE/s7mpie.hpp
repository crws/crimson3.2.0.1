#include "s7isotcp.hpp"

///////////////////////////////////////////////////////////////////////////
//
// S7 Constants
//

#define	SPACE_OUTPUT	0x01
#define	SPACE_INPUT	0x02
#define	SPACE_FLAG	0x03
#define	SPACE_TIMER	0x04
#define	SPACE_COUNTER	0x05
#define	SPACE_DATA	0x06

#define ID_QB		0x82
#define ID_IB		0x81
#define ID_MB		0x83
#define ID_T		29
#define ID_C		28
#define ID_DB		0x84

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 MPI Macro's
//

#define DATA_BLOCK(a,b)		(a + ((b & 0x78) << 1) )

//////////////////////////////////////////////////////////////////////////
//
// S7 MPI Extended DB TCP Driver
//

class CS7ExtTcpMasterDriver : public CS7IsoTcpMasterDriver
{
	public:
		// Constructor
		CS7ExtTcpMasterDriver(void);

		// Destructor
		~CS7ExtTcpMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping(void);
		
	protected:

		void AddParams(BYTE bService, UINT uType, UINT uTable, UINT uAddr, UINT uExtra, UINT uCount, AREF Addr);
		UINT FindBits(UINT uType, UINT uTable);
		void AddPadding(UINT uTable);
		BYTE FindType(UINT uType, UINT uTable);
		BYTE FindArea(UINT uTable);
		BOOL IsTimer(UINT uTable);
		BOOL IsCounter(UINT uTable);
		BOOL EatWrite(UINT uTable);
		UINT GetTable(UINT uTable);

		// Overridables
		UINT GetMaxBits(void);

	};
		

// End of File

