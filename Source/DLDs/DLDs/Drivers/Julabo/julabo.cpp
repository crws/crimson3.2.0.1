
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "julabo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Julabo Serial Driver
//

// Instantiator

INSTANTIATE(CJulaboSerialDriver);

// Constructor

CJulaboSerialDriver::CJulaboSerialDriver(void)
{
	m_Ident		= DRIVER_ID; // 4068

	CTEXT Hex[]	= "0123456789ABCDEF";
	CTEXT cIn[]	= "in_";
	CTEXT cOut[]	= "out_";
	CTEXT Stat[]	= "status";
	CTEXT Vers[]	= "version";
	CTEXT cSP[]	= "sp_";
	CTEXT cPar[]	= "par_";
	CTEXT cPV[]	= "pv_";
	CTEXT cHil[]	= "hil_";
	CTEXT cMode[]	= "mode_";
	CTEXT cDisab[]	= "Disabled";


	m_pHex		= Hex;
	m_pIn		= cIn;
	m_pOut		= cOut;
	m_pStat		= Stat;
	m_pVers		= Vers;
	m_pSP		= cSP;
	m_pPar		= cPar;
	m_pPV		= cPV;
	m_pHil		= cHil;
	m_pMode		= cMode;
	m_pDisabled	= cDisab;

	m_fList		= FALSE;
	m_fPrevListItem	= FALSE;
	}

// Destructor

CJulaboSerialDriver::~CJulaboSerialDriver(void)
{
	}

// Configuration

void MCALL CJulaboSerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CJulaboSerialDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}	
	}
	
// Management

void MCALL CJulaboSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CJulaboSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CJulaboSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx			= new CContext;

			m_pCtx->m_bDrop		= GetByte(pData);

			m_pCtx->m_uUser		= GetWord(pData);

			m_pCtx->m_fUseAddress	= FALSE;

			SPrintf( m_pCtx->m_Drop, "%3.3d\0", m_pCtx->m_bDrop);

			m_pCtx->m_uWriteErrCt	= 0;

			ClearBuff(m_pCtx->m_pbUSRCMD);
			ClearBuff(m_pCtx->m_pbUSRRSP);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CJulaboSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CJulaboSerialDriver::Ping(void)
{
	DWORD Data[1];

	CAddress Addr;

	Addr.a.m_Table	= JSTSTR;
	Addr.a.m_Offset	= 0;
	Addr.a.m_Type	= addrLongAsLong;
	Addr.a.m_Extra	= 0;

	m_pCtx->m_fUseAddress = TRUE;

	if( Read(Addr, Data, 1) == 1 ) return 1;

	m_pCtx->m_fUseAddress = FALSE;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CJulaboSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	m_uID = Addr.a.m_Table;

	if( NoReadTransmit(pData, uCount) ) {

		return uCount;
		}

	if( uCount > 1 && !m_fList ) {

		m_fList         = TRUE;
		m_fPrevListItem = FALSE;
		}

	m_fRead = TRUE;

//**/	AfxTrace3("\r\nRead T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	StartFrame(Addr.a.m_Offset);

	if( Transact(TRUE) ) {

		if( m_fList ) CheckList(uCount);

		if( GetResponse(pData, &uCount) ) return uCount;
		}

	if( m_fPrevListItem ) { // maybe invalid request in a list

		CheckList(uCount);

		return CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CJulaboSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	m_uID = Addr.a.m_Table;

	if( NoWriteTransmit(Addr, pData, uCount) ) {

		return uCount;
		}

	m_fRead = FALSE;

//**/	AfxTrace3("\r\n\n*** WRITE *** %x %x %x ", m_uID, Addr.a.m_Offset, uCount);

	StartFrame(Addr.a.m_Offset);

	if( !IsStringCmd() ) {

		AddWriteData(*pData, Addr.a.m_Type);
		}

	BOOL fOk = Transact(m_uID == JUSRS);

	if( m_uID == JUSRS ) {

		StoreStringResponse();

		return uCount;
		}

	return fOk ? 1 : CheckWrite(uCount);
	}

// Frame Building

void CJulaboSerialDriver::StartFrame(UINT uOffset)
{
	m_uTxPtr = 0;

	AddUnitAddress();

	AddCommand(uOffset);
	}

void CJulaboSerialDriver::AddUnitAddress(void)
{
	if( m_pCtx->m_fUseAddress ) {

		AddByte('A');
		AddText(m_pCtx->m_Drop);
		AddByte('_');
		}
	}

void CJulaboSerialDriver::AddCommand(UINT uOffset)
{
	if( IsSendStringReq() ) {

		AddStringCmd();

		return;
		}

	BOOL f = m_uID != JSTAT;

	if( f ) AddInOut();

	AddTypeCmd();

	if( f ) AddParameter(uOffset);
	}

void CJulaboSerialDriver::AddInOut(void)
{
	char * pc = m_fRead ? (char *)m_pIn : (char *)m_pOut;

	AddText( pc );
	}

void CJulaboSerialDriver::AddStringCmd(void)
{
	char * pc = NULL;

	switch( m_uID ) {

		case JUSRS:
			pc = (char *)m_pCtx->m_pbUSRCMD;
			break;

		case JSTSTR:
			pc = (char *)m_pStat;
			break;

		case JVERS:
			pc = (char *)m_pVers;
			break;
		}

	AddText(pc);
	}

void CJulaboSerialDriver::AddTypeCmd(void)
{
	char * pc = NULL;

	switch( m_uID ) {

		case JSPT:	pc = (char *)m_pSP;	break;
		case JPAR:	pc = (char *)m_pPar;	break;
		case JPV:	pc = (char *)m_pPV;	break;
		case JHIL:	pc = (char *)m_pHil;	break;
		case JMODE:	pc = (char *)m_pMode;	break;
		case JSTAT:	pc = (char *)m_pStat;	break;
		}

	AddText(pc);
	}

void CJulaboSerialDriver::AddParameter(UINT uOffset)
{
	AddByte(m_pHex[uOffset / 10]);
	AddByte(m_pHex[uOffset % 10]);
	}

void CJulaboSerialDriver::AddWriteData(DWORD dData, UINT uType)
{
	char c[12] = {0};

	switch( uType ) {

		case addrLongAsLong:

			SPrintf(c, " %d", dData);
			break;

		case addrRealAsReal:

			SPrintf(c, " %f", PassFloat(dData));
			break;
		}

	AddText(c);
	}

void CJulaboSerialDriver::AddByte(BYTE bData)
{
	if( m_uTxPtr < sizeof(m_bTx) ) {

		m_bTx[m_uTxPtr++] = bData;
		}
	}

void CJulaboSerialDriver::AddText(char * sText)
{
	for( UINT i = 0; i < strlen(sText); i++ ) AddByte( sText[i] );
	}

void CJulaboSerialDriver::EndFrame(void)
{
	AddByte(CR);
	}

// Transport Layer

BOOL CJulaboSerialDriver::Transact(BOOL fDataReply)
{
	EndFrame();

	if( m_fRead ) {

		Sleep(10);
		}

	if( m_pCtx->m_fUseAddress && fDataReply ) {

		Sleep(20);
		}

	Send();

	return GetReply(fDataReply);
	}

void CJulaboSerialDriver::Send(void)
{
	m_pData->ClearRx();

	ClearBuff(m_bRx);

	Put();
	}

BOOL CJulaboSerialDriver::GetReply(BOOL fDataReply)
{
	BOOL fAdd   = FALSE;

	BYTE bEnd   = m_fRead ? LF : CR;

	UINT uPtr   = 0;

	UINT XCnt   = 0;

	UINT uState = 0;

	UINT uTime  = fDataReply && !m_fPrevListItem ? 1000 : 250;

	SetTimer(uTime);

//**/	AfxTrace1("\r\n%2.2x ", bEnd);

	while( (uTime = GetTimer()) ) {

		WORD wData;

		if( (wData = Get(uTime) ) == LOWORD(NOTHING) ) {

			Sleep(20);

			continue;
			}

		BYTE bData = LOBYTE(wData);

//**/		AfxTrace1("<%2.2x>", bData);

		m_bRx[uPtr++] = bData;

		if( bData == 0x11 && XCnt == 1 ) {

			if( !fDataReply ) return TRUE; // write response;

			if( uPtr == 2 ) return FALSE; // XOFF + no char + XON
			}

		switch( uState ) {

			case 0:
				switch( bData ) {

					case 0x13:
						XCnt   = 1;
						uState = 2; // XON/XOFF only on 232
						break;

					case 'A':
						uState = 1;
						fAdd   = TRUE;
						break;

					default:
						if( bData > ' ' ) {

							fAdd   = FALSE;
							uState = 2;
							}
						break;
					}

				break;

			case 1:
				if( uPtr == 5 + XCnt ) {

					if( CheckAddrHeader(XCnt) ) {

						uState = 2;
						fAdd   = TRUE;
						}

					else uState = 4; // not our address

					uPtr = 0;
					}
				break;

			case 2:
				if( bData == bEnd ) {

					if( !(BOOL)(m_uRxPtr = uPtr) ) return FALSE;

					if( XCnt == 1 ) uState = 3; // an XOFF was received

					else { // hardware handshake

						if( !fDataReply ) Sleep(250); // recommended delay after write

						m_pCtx->m_fUseAddress = fAdd;

						return TRUE;
						}
					}
				break;

			case 3:
				if( bData == 0x11 ) {

					m_pCtx->m_fUseAddress = fAdd;

					return TRUE;
					}
				break;

			case 4:
				if( bData == bEnd ) {

					uPtr   = 0;

					uState = XCnt == 1 ? 5 : 0;
					}

				break;

			case 5:
				if( bData == 0x11 ) {

					uPtr   = 0;
					uState = 0;
					}

				break;
			}

		if( uPtr >= SZBUFF ) {

			break;
			}
		}

	m_uRxPtr = uPtr;

	return !fDataReply;
	}

BOOL CJulaboSerialDriver::CheckAddrHeader(UINT uStart)
{
	UINT uEnd = uStart + 5;

	for( UINT i = uStart; i < uEnd; i++ ) {

		if( m_bRx[i] != m_bTx[i] ) return FALSE;
		}

	return TRUE;
	}

BOOL CJulaboSerialDriver::GetResponse(PDWORD pData, UINT *pCount)
{
	char *p = (char *)m_bRx;

	if( IsXON_XOFF(*p) ) {

		p++;

		m_uRxPtr--;

		if( IsXON_XOFF(*p) ) {

			p++;

			m_uRxPtr--;
			}
		}

	if( IsStringCmd() ) {

		CopyStringData(pData, m_bRx, min(m_uRxPtr, (*pCount) * 4));

		return TRUE;
		}

	if( !IsNumChar(p) ) return FALSE;

	*pCount = 1;

	switch( m_uID ) {

		case JSPT:
		case JPAR:
		case JPV:
			*pData = ATOF(p);

			if( *pData == NEGZERO ) {

				*pData = 0;
				}

			return TRUE;

		case JHIL:
		case JMODE:
		case JSTAT:
			*pData = ATOI(p);

			return TRUE;
		}

	return FALSE;
	}

void  CJulaboSerialDriver::StoreStringResponse(void)
{
	DoBuffer((char *)m_pCtx->m_pbUSRRSP, (char *)m_bRx, m_uRxPtr, SZBUFF);
	}

// Port Access

void CJulaboSerialDriver::Put(void)
{
	m_uRxPtr = 0;

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uTxPtr; k++ ) if(m_bTx[k] >= ' ') AfxTrace1("%c", m_bTx[k]); else AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write(m_bTx, m_uTxPtr, FOREVER);
	}

UINT CJulaboSerialDriver::Get(UINT uTimer)
{
	return m_pData->Read(uTimer);
	}

// Internal access
BOOL CJulaboSerialDriver::NoReadTransmit(PDWORD pData, UINT uCount)
{
	PBYTE pSrc;

	switch( m_uID ) {

		case JUSRS:
			*pData = 0;
			return TRUE;

		case JUSRC:
			pSrc   = m_pCtx->m_pbUSRCMD;
			break;

		case JUSRR:
			pSrc   = m_pCtx->m_pbUSRRSP;
			break;

		default:
			return FALSE;
		}

	CopyStringData(pData, pSrc, min(SZBUFF, 4 * uCount));

	return TRUE;
	}

BOOL CJulaboSerialDriver::NoWriteTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( m_uID ) {

		case JPV:
		case JSTSTR:
		case JVERS:
			return TRUE;

		case JUSRR:
			ClearBuff(m_pCtx->m_pbUSRRSP);
			return TRUE;

		case JUSRC:
			if( m_pCtx->m_uUser == USRALLOW ) {

				CopyStringData(PDWORD(m_pCtx->m_pbUSRCMD), PBYTE(pData), min(uCount * 4, SZBUFF));
				}

			else ClearBuff(m_pCtx->m_pbUSRCMD);

			return TRUE;

		case JUSRS:
			if( m_pCtx->m_uUser == USRALLOW && *pData == USRALLOW ) return FALSE;

			memcpy(m_pCtx->m_pbUSRRSP, m_pDisabled, 8);

			m_pCtx->m_pbUSRRSP[8] = 0;

			return TRUE;
		}

	return FALSE;
	}

// Helpers
BOOL CJulaboSerialDriver::IsStringCmd(void)
{
	switch(m_uID) {

		case JSTSTR:
		case JVERS:
		case JUSRC:
		case JUSRR:
		case JUSRS:
			return TRUE;
		}

	return FALSE;
	}

BOOL CJulaboSerialDriver::IsSendStringReq(void)
{
	switch( m_uID ) {

		case JSTSTR:
		case JVERS:
		case JUSRS:
			return TRUE;
		}

	return FALSE;
	}

CCODE CJulaboSerialDriver::CheckWrite(UINT uCount)
{
	if( ++m_pCtx->m_uWriteErrCt > 2 ) {

		m_pCtx->m_uWriteErrCt = 0;

		return uCount;
		}

	return CCODE_ERROR;
	}

BOOL CJulaboSerialDriver::IsNumChar(char *p)
{
	for( UINT uPos = 0; uPos < SZBUFF; uPos++ ) {

		char c = p[uPos];

		if( c != ' ' ) {

			if( IsDecDigit(c) ) return TRUE;

			return (c == '-' || c == '+') ? IsDecDigit(p[uPos+1]) : FALSE;
			}
		}

	return FALSE;
	}

BOOL CJulaboSerialDriver::IsDecDigit(char c)
{
	return c >= '0' && c <= '9';
	}

void CJulaboSerialDriver::DoBuffer(char *pDest, char *pSrc, UINT uCount, UINT uMax)
{
	memset(pDest, 0, uMax);

	if( pSrc[0] ) {

		memcpy(pDest, pSrc, min(uCount, uMax));
		}
	}

BOOL CJulaboSerialDriver::IsXON_XOFF(BYTE b)
{
	return b == 0x13 || b == 0x11;
	}

void CJulaboSerialDriver::CheckList(UINT uCount)
{
	m_fList         = uCount > 1;
	m_fPrevListItem = m_fList;
	}

void CJulaboSerialDriver::ClearBuff(PBYTE pBuff)
{
	memset(pBuff, 0, SZBUFF);
	}

void CJulaboSerialDriver::CopyStringData(PDWORD pData, PBYTE pSrc, UINT uByteCount)
{
	memset(pData, 0, uByteCount);

	UINT uEnd = (uByteCount + 3 ) / 4;

	BYTE b = *pSrc;

	if( b ) {

		for( UINT i = 0; i < uEnd; i++ ) {

			DWORD x  = PU4(pSrc)[i];

			pData[i] = MotorToHost(x);
			}
		}
	}

// End of File
