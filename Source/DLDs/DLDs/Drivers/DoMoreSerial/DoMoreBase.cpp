
#include "intern.hpp"

#include "DoMoreBase.hpp"

#include "MxAppProtocol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC Base Driver
//

// Constructor

CDoMoreBaseDriver::CDoMoreBaseDriver(void)
{
	m_pBaseCtx = NULL;
	}
		
// Destructor

CDoMoreBaseDriver::~CDoMoreBaseDriver(void)
{
	}

// Entry Points

CCODE CDoMoreBaseDriver::Ping(void)
{
	if( !CheckSession() ) {

		return CCODE_ERROR;
		}

	if( !m_pBaseCtx->m_fUsePing ) {

		return 1;
		}

	CAddress Addr;

	Addr.m_Ref = 0;

	Addr.a.m_Table	= TBL_V;
	Addr.a.m_Offset	= m_pBaseCtx->m_uPingReg;
	Addr.a.m_Type	= addrWordAsWord;

	DWORD Data[1];
	
	return Read(Addr, Data, 1);
	}

CCODE CDoMoreBaseDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !CheckSession() ) {

		return CCODE_ERROR;
		}

	if( IsStructured(Addr.a.m_Table) ) {

		return DoStructRead(Addr, pData, uCount);
		}

	if( IsString(Addr.a.m_Table) ) {

		return DoStringRead(Addr, pData, uCount);
		}

	UINT uSpan = min(uCount, 16);
		
	BYTE bTypeSize = GetTypeSize(Addr.a.m_Type);

	CMxAppData ReadData(bTypeSize, uSpan, Addr.a.m_Offset, Addr.a.m_Table);

	if( StartRead(m_pBaseCtx, Addr, &ReadData) ) {

		CMxAppMultiReadReply Reply(&ReadData);

		if( Reply.Parse(m_bRxBuff, m_uPtr) ) {

			CMxAppData * pRead = Reply.GetDataObject();

			if( pRead ) {

				for( UINT n = 0; n < uSpan; n++ ) {

					DWORD dwData = pRead->GetData(n);

					if( Addr.a.m_Type <= addrBitAsByte ) {

						UINT uBitOff = Addr.a.m_Offset % 8;

						dwData &= (1 << uBitOff);

						pData[n] = !!dwData;
						}
					else {
						pData[n] = dwData;
						}
					}

				return uSpan;
				}
			}
		else {
			CloseSession();

			return CCODE_ERROR;
			}
		}

	return CCODE_ERROR;
	}

CCODE CDoMoreBaseDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !CheckSession() ) {

		return CCODE_ERROR;
		}

	if( IsStructured(Addr.a.m_Table) ) {

		return DoStructWrite(Addr, pData, uCount);
		}

	if( IsString(Addr.a.m_Table) ) {

		return DoStringWrite(Addr, pData, uCount);
		}

	BYTE bTypeSize = GetTypeSize(Addr.a.m_Type);

	CMxAppData WriteData(bTypeSize, uCount, Addr.a.m_Offset, Addr.a.m_Table);

	for( UINT n = 0; n < uCount; n++ ) {

		WriteData.SetData(pData[n], n);
		}

	if( Addr.a.m_Type <= addrBitAsByte ) {

		return DoBitWrite(Addr, uCount, &WriteData);
		}
	else {
		return DoWrite(Addr, uCount, &WriteData);
		}
	}

// Session Management

BOOL CDoMoreBaseDriver::CheckSession(void)
{
	if( !m_pBaseCtx->m_fSessionOpen ) {

		if( StartSession(m_pBaseCtx) ) {

			return OpenSession(m_pBaseCtx);
			}
		}
	
	return TRUE;
	}

BOOL CDoMoreBaseDriver::StartSession(CBaseCtx *pCtx)
{
	BYTE bKey = GetRandomKey();

	CMxAppRegisterRequest Request(pCtx->m_pPassword, bKey);

	return StartRequest(Request);
	}

BOOL CDoMoreBaseDriver::OpenSession(CBaseCtx *pCtx)
{
	CMxAppRegisterReply Reply;

	if( Reply.Parse(m_bRxBuff, m_uPtr) ) {

		pCtx->m_wSession = Reply.GetSession();

		pCtx->m_fSessionOpen = TRUE;

		return TRUE;
		}

	return FALSE;
	}

void CDoMoreBaseDriver::CloseSession(void)
{
	m_pBaseCtx->m_fSessionOpen = FALSE;
	}

BYTE CDoMoreBaseDriver::GetRandomKey(void)
{
	BYTE bKey = RAND(GetTickCount()) & 0xFF;

	if( bKey < 0x80 )
		bKey += 0x80;

	return bKey;
	}

// Read Requests

BOOL CDoMoreBaseDriver::StartRead(CBaseCtx *pCtx, AREF Addr, CMxAppData *pRead)
{
	CMxAppMultiReadRequest Request(pCtx->m_wSession);

	Request.AddRead(pRead);

	return StartRequest(Request);
	}

BOOL CDoMoreBaseDriver::StartBulkRead(CBaseCtx *pCtx, AREF Addr, CMxAppData *pRead)
{
	CMxAppBulkReadRequest Request(pCtx->m_wSession);

	Request.AddRead(pRead);

	return StartRequest(Request);
	}

// Write Request

BOOL CDoMoreBaseDriver::StartWrite(CBaseCtx *pCtx, AREF Addr, CMxAppData *pWrite)
{
	CMxAppMultiWriteRequest Request(pCtx->m_wSession);

	Request.AddWrite(pWrite);

	return StartRequest(Request);
	}


BOOL CDoMoreBaseDriver::StartBulkWrite(CBaseCtx *pCtx, AREF Addr, CMxAppData *pWrite)
{
	CMxAppBulkWriteRequest Request(pCtx->m_wSession);

	Request.AddWrite(pWrite);

	return StartRequest(Request);
	}

BOOL CDoMoreBaseDriver::StartMaskedWrite(CBaseCtx *pCtx, AREF Addr, CMxAppData *pWrite, BOOL fIsBit, UINT uBitOffset)
{
	CMxAppMultiWriteMaskRequest Request(pCtx->m_wSession, fIsBit, uBitOffset);

	Request.AddWrite(pWrite);

	return StartRequest(Request);
	}

// Address Helpers
		
UINT CDoMoreBaseDriver::GetTypeSize(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrBitAsByte:
		case addrByteAsByte:

			return 1;

		case addrWordAsWord:

			return 2;

		case addrLongAsLong:
		case addrRealAsReal:

			return 4;
		}

	return 1;
	}

BOOL CDoMoreBaseDriver::IsStructured(UINT uTable)
{
	switch( uTable ) {

		case TBL_SDT:
		case TBL_T:
		case TBL_CT:
		case TBL_UDT:

			return TRUE;
		}

	return FALSE;
	}


BOOL CDoMoreBaseDriver::IsString(UINT uTable)
{
	switch( uTable ) {

		case TBL_SS:
		case TBL_SL:
		case TBL_LASTERR:
		case TBL_LASTMSG:

			return TRUE;
		}

	return FALSE;
	}

WORD CDoMoreBaseDriver::GetMaxStringLen(UINT uTable)
{
	switch( uTable ) {

		case TBL_SS:

			return 64;

		case TBL_LASTERR:
		case TBL_LASTMSG:

			return 128;

		case TBL_SL:

			return 256;
		}

	return 0;
	}

// Frame Building

void CDoMoreBaseDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {

		m_bTxBuff[m_uPtr] = bData;

		m_uPtr++;
		}
	}

void CDoMoreBaseDriver::AddWord(WORD wData)
{	
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CDoMoreBaseDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CDoMoreBaseDriver::AddData(PBYTE pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		AddByte(pData[n]);
		}
	}

// Reading

CCODE CDoMoreBaseDriver::DoStringRead(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bTypeSize = GetTypeSize(Addr.a.m_Type);

	UINT uOffset = Addr.a.m_Offset / MAX_STRING;

	CMxAppData ReadData(bTypeSize, uCount + 4, uOffset, Addr.a.m_Table);

	if( StartBulkRead(m_pBaseCtx, Addr, &ReadData) ) {

		CMxAppBulkReadReply Reply(&ReadData);

		if( Reply.Parse(m_bRxBuff, m_uPtr) ) {

			CMxAppData * pRead = Reply.GetDataObject();

			if( pRead ) {

				for( UINT n = 0; n < uCount; n++ ) {

					pData[n] = pRead->GetData(n + 4);
					}
				}

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE CDoMoreBaseDriver::DoStructRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uPacked = Addr.a.m_Extra << 16 | Addr.a.m_Offset;

	UINT uOffset = uPacked & 0x7F;

	UINT uBit = (uPacked & 0x0F80) >> 7;

	UINT uDWORD = uPacked >> 12;

	BYTE bTypeSize = 4;

	CMxAppData ReadData(bTypeSize, 1, uOffset, Addr.a.m_Table);

	ReadData.m_bHeapOffset = uDWORD * 4;

	if( StartRead(m_pBaseCtx, Addr, &ReadData) ) {

		CMxAppMultiReadReply Reply(&ReadData);

		if( Reply.Parse(m_bRxBuff, m_uPtr) ) {

			CMxAppData * pRead = Reply.GetDataObject();

			if( pRead ) {

				for( UINT n = 0; n < uCount; n++ ) {
					
					DWORD dwData = pRead->GetData(n);

					DWORD dwMask = 0;

					if( Addr.a.m_Type <= addrBitAsByte ) {

						dwData &= (1 << uBit);

						pData[n] = !!dwData;
						}
					else {
						for( UINT u = 0; u < GetTypeSize(Addr.a.m_Type); u++ ) {

							dwMask |= (0xFF << (8 * u));
							}

						dwData = (dwData >> uBit) & dwMask;

						pData[n] = dwData;
						}
					}

				return 1;
				}
			}
		else {
			CloseSession();

			return CCODE_ERROR;
			}
		}

	return CCODE_ERROR;
	}

// Writing

CCODE CDoMoreBaseDriver::DoBitWrite(AREF Addr, UINT uSpan, CMxAppData *pWrite)
{
	UINT uBitOffset = Addr.a.m_Offset % 8;

	if( StartMaskedWrite(m_pBaseCtx, Addr, pWrite, TRUE, uBitOffset) ) {

		CMxAppMultiWriteMaskReply Reply;

		if( Reply.Parse(m_bRxBuff, m_uPtr) ) {			

			return uSpan;
			}
		}

	return CCODE_ERROR;
	}

CCODE CDoMoreBaseDriver::DoWrite(AREF Addr, UINT uSpan, CMxAppData *pWrite)
{
	if( StartWrite(m_pBaseCtx, Addr, pWrite) ) {

		CMxAppMultiWriteReply Reply;

		if( Reply.Parse(m_bRxBuff, m_uPtr) ) {			

			return uSpan;
			}
		}

	return CCODE_ERROR;
	}

CCODE CDoMoreBaseDriver::DoStructWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bTypeSize = GetTypeSize(Addr.a.m_Type);

	UINT uPacked = Addr.a.m_Extra << 16 | Addr.a.m_Offset;

	UINT uOffset = uPacked & 0x7F;

	UINT uBit = (uPacked & 0x0F80) >> 7;

	UINT uDWORD = uPacked >> 12;

	CMxAppData WriteData(bTypeSize, uCount, uOffset, Addr.a.m_Table);

	WriteData.m_bHeapOffset = uDWORD * 4;

	for( UINT n = 0; n < uCount; n++ ) {

		WriteData.SetData(pData[n], n);
		}

	if( StartMaskedWrite(m_pBaseCtx, Addr, &WriteData, FALSE, uBit) ) {

		CMxAppMultiWriteMaskReply Reply;

		if( Reply.Parse(m_bRxBuff, m_uPtr) ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE CDoMoreBaseDriver::DoStringWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	WORD wMaxLen = GetMaxStringLen(Addr.a.m_Table);

	UINT uOffset = Addr.a.m_Offset / MAX_STRING;

	UINT uPos = Addr.a.m_Offset % MAX_STRING;

	PDWORD pBuff = new DWORD[ wMaxLen ];

	PTXT pString = new char [ wMaxLen ];

	memset(pBuff,   0, wMaxLen);

	memset(pString, 0, wMaxLen);

	if( DoStringRead(Addr, pBuff, wMaxLen) ) {

		for( UINT n = 0; n < uCount; n++ ) {

			if( (n + uPos) < wMaxLen ) {

				pBuff[n + uPos] = pData[n];
				}
			}

		for( UINT u = 0; u < wMaxLen; u++ ) {

			pString[u] = pBuff[u];
			}

		CMxAppData WriteData(1, wMaxLen + 4, uOffset, Addr.a.m_Table);

		WORD wStrLen = strlen(pString);

		WriteData.SetData(LOBYTE(wMaxLen), 0);
		WriteData.SetData(HIBYTE(wMaxLen), 1);

		WriteData.SetData(LOBYTE(wStrLen), 2);
		WriteData.SetData(HIBYTE(wStrLen), 3);

		for( UINT k = 4; k < wMaxLen + 4; k++ ) {

			if( pString[k - 4] ) {

				WriteData.SetData(pString[k - 4], k);
				}
			else {
				WriteData.SetData(0, k);
				}
			}

		if( StartBulkWrite(m_pBaseCtx, Addr, &WriteData) ) {

			CMxAppBulkWriteReply Reply;

			if( Reply.Parse(m_bRxBuff, m_uPtr) ) {

				delete [] pString;

				delete [] pBuff;

				return uCount;
				}
			}
		}

	delete [] pString;

	delete [] pBuff;

	return CCODE_ERROR;
	}

// End of File
