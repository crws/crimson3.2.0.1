
#ifndef	INCLUDE_MX_PROTOCOL

#define INCLUDE_MX_PROTOCOL

// Data Table Definitions

#define	TBL_ST		1
#define	TBL_DST		2
#define	TBL_SDT		4
#define	TBL_X		5
#define	TBL_Y		6
#define	TBL_WX		7
#define	TBL_WY		8
#define	TBL_C		9
#define	TBL_V		10
#define	TBL_N		11
#define	TBL_D		12
#define	TBL_R		13
#define	TBL_T		14
#define	TBL_CT		15
#define	TBL_SS		16
#define	TBL_SL		17
#define	TBL_UDT		18
#define	TBL_PL		19
#define	TBL_DLX		20
#define	TBL_DLY		21
#define	TBL_DLC		22
#define	TBL_DLV		23
#define	TBL_MI		24
#define	TBL_MC		25
#define	TBL_MIR		26
#define	TBL_MHR		27
#define	TBL_LASTMSG	28
#define	TBL_LASTERR	29
#define	TBL_HEAP	240

#define	MX_BUFF_SIZE	1024

//////////////////////////////////////////////////////////////////////////
//
// Do-More Password Encryption Support
//

static BYTE EncryptionKey[8][128] = 
{  
	{38,95,54,177,200,141,165,140,52,59,223,216,65,63,33,215,251,135,196,53,106,82,173,96,171,146,250,230,205,32,170,156,129,99,72,247,182,38,142,35,157,209,16,42,208,216,162,114,79,165,100,160,199,217,230,150,98,208,25,25,74,144,130,128,135,165,108,242,109,105,177,35,172,162,169,6,71,135,178,90,211,179,167,212,46,31,165,190,211,13,123,101,222,74,205,170,42,106,114,108,128,78,156,161,72,11,210,13,254,125,210,198,213,94,105,37,188,185,187,251,66,110,109,239,41,240,30,136},
	{116,164,39,38,90,104,88,218,123,38,21,178,74,236,146,125,250,91,82,50,188,238,178,49,50,218,189,170,82,212,103,227,244,167,81,76,123,6,151,90,243,221,214,4,32,208,78,34,135,91,185,200,137,248,170,233,153,185,57,125,155,123,78,206,210,88,186,244,10,23,201,191,92,211,90,216,209,195,58,17,169,101,51,64,58,139,104,111,156,77,134,9,114,89,89,170,133,90,108,222,115,205,189,58,164,158,202,171,242,62,177,222,129,26,86,230,220,211,170,171,213,88,208,204,53,156,68,89},
	{129,50,235,74,181,67,145,113,253,73,236,74,238,50,125,56,109,228,77,15,200,151,200,70,5,10,220,158,251,219,181,61,145,250,100,200,122,31,35,215,159,236,161,250,83,54,154,130,192,18,200,9,119,219,4,29,253,118,241,77,16,199,4,32,54,127,167,22,188,62,207,180,204,37,172,86,50,99,69,14,6,61,237,88,183,94,88,92,150,66,86,83,213,113,56,109,2,227,112,165,136,42,146,206,106,209,83,179,197,113,218,44,101,248,64,93,196,152,170,201,38,35,62,4,18,243,10,27},
	{23,127,111,158,79,174,87,15,84,62,232,206,223,25,39,238,38,15,92,31,136,200,62,45,253,251,54,48,17,113,215,222,63,162,204,154,29,1,234,25,152,231,164,167,98,30,247,245,131,217,21,243,16,86,85,59,246,37,1,79,144,15,211,88,21,200,153,215,221,164,57,178,15,39,238,57,91,132,209,120,187,170,157,253,36,214,65,116,85,244,108,177,171,62,236,1,204,221,185,157,144,62,137,81,100,208,75,97,144,232,9,60,9,19,229,4,222,168,151,210,243,162,239,163,72,6,66,242},
	{69,14,96,72,34,230,124,138,161,251,200,31,241,83,223,35,135,137,188,144,47,3,19,251,233,113,205,174,4,46,47,106,138,241,165,9,102,158,87,248,32,219,36,203,210,128,89,6,166,17,61,165,190,43,37,242,8,216,95,85,17,23,184,4,174,212,212,143,86,114,104,11,131,177,58,54,127,88,31,18,42,195,65,3,224,98,195,112,140,22,24,20,191,79,188,127,247,39,184,218,47,233,210,37,54,225,186,100,178,246,105,224,124,40,71,158,10,86,231,27,191,145,254,146,139,122,49,110},
	{163,2,129,75,95,1,152,86,101,32,241,198,247,174,121,196,18,58,156,223,207,12,169,40,17,68,142,43,230,32,39,248,22,242,245,67,243,40,48,247,136,32,233,198,48,196,174,147,38,19,89,183,135,229,162,63,180,251,191,165,217,232,20,203,37,115,87,10,65,172,177,14,138,166,128,50,20,254,3,180,215,234,89,55,160,149,247,254,152,189,72,31,24,55,69,230,164,45,111,70,69,78,41,93,159,32,221,20,99,109,215,178,57,132,182,157,57,45,237,81,166,130,124,42,42,82,38,134},
	{179,145,44,164,171,242,180,203,170,194,201,39,229,236,124,204,86,121,111,77,211,81,199,252,161,244,111,186,1,199,210,147,194,216,5,73,108,206,156,91,42,108,67,11,13,181,82,91,55,181,31,174,97,203,91,236,181,61,42,15,134,83,154,57,64,40,175,98,20,118,167,252,46,230,234,242,165,173,27,237,173,74,225,208,8,108,221,23,187,187,242,223,253,168,52,20,241,4,219,150,137,157,209,24,187,27,210,163,165,72,220,130,131,55,125,79,144,48,40,36,250,119,202,102,158,107,252,105},
	{162,150,232,219,234,108,74,202,220,55,162,152,14,164,175,143,1,96,128,162,24,145,99,101,202,170,32,215,1,47,242,95,91,189,110,60,170,7,106,80,69,65,19,251,142,120,119,32,96,215,213,231,129,241,55,139,61,187,39,97,174,35,110,71,60,121,77,205,171,184,119,244,119,202,115,32,9,101,72,241,37,114,78,81,181,78,143,206,138,209,19,176,88,38,235,195,14,212,223,252,30,237,150,157,236,170,76,24,95,223,117,233,40,186,232,230,30,185,62,74,46,243,248,17,78,4,68,89}
	};


//////////////////////////////////////////////////////////////////////////
//
// MX APP Protocol Support for Automation Direct Do-More PLC
//

enum {
	mxProgramInfo		= 0x03,
	mxRequestSession	= 0x04,
	mxMultiRead		= 0x13,
	mxMultiWrite		= 0x14,
	mxMultiWriteMask	= 0x15,
	mxReadBulk		= 0x2C,
	mxWriteBulk		= 0x2D,
	mxError			= 0xFF
	};

// Read/Write Flags

enum {
	mxFlagNone	= 0,
	mxFlagArray	= 1,
	mxFlagHeap	= 2
	};

//////////////////////////////////////////////////////////////////////////
//
// MX APP Protocol Base Object
//

class CMxAppCommsObject
{
	public:
		// Constructor
		CMxAppCommsObject(void);

		// Comms Function
		UINT GetFuncCode(void) const;

	protected:
		UINT m_uFunc;
	};

//////////////////////////////////////////////////////////////////////////
//
// MX APP Request Base Class
//

class CMxAppRequest : public CMxAppCommsObject
{
	public:
		// Constructor
		CMxAppRequest(void);

		// Destructor
		~CMxAppRequest(void);

		virtual WORD Encode(PBYTE pBuff, UINT uSize);

	protected:

		// Data Members
		WORD m_wSize;
		BYTE m_Buff[MX_BUFF_SIZE];

		// Buffer Operations
		void AddByte(BYTE b);
		void AddWord(WORD w);
		void AddLong(DWORD dw);

		virtual void Process(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Register Session Request
//

class CMxAppRegisterRequest : public CMxAppRequest
{
	public:
		// Constructor
		CMxAppRegisterRequest(PTXT pPassword, BYTE bKey);

		// Destructor
		~CMxAppRegisterRequest(void);

	protected:

		// Data Members
		BYTE m_bKey;
		CHAR m_Pass[9];

		// Password Support
		BYTE EncryptChar(CHAR Char, UINT uPos, BYTE bKey);
		void EncryptString(BYTE bKey, PCTXT pStr);

		// Implementation
		void Process(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Encapsulation
//

class CMxAppData
{
	public:
		// Constructor
		CMxAppData(BYTE bTypeSize, UINT uCount, WORD wOffset, BYTE bTable);
		
		// Destructor
		~CMxAppData(void);

		// Data Access
		DWORD GetData(UINT uPos);
		BOOL SetData(DWORD dwData, UINT uPos);
		WORD GetRequestSize(void);
		UINT GetRequestCount(void);

		// Data Members
		BYTE	m_bTable;
		WORD	m_wMemOffset;
		BYTE	m_bFlags;
		BYTE	m_bHeapOffset;
		BYTE	m_bTypeSize;

		// Linked List
		CMxAppData * m_pNext;
		CMxAppData * m_pPrev;

	protected:

		UINT	m_uCount;
		UINT	m_uSize;
		PDWORD	m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi-Read Request
//

class CMxAppMultiReadRequest : public CMxAppRequest
{
	public:
		// Constructor
		CMxAppMultiReadRequest(WORD wSession);

		// Destructor
		~CMxAppMultiReadRequest(void);

		// Read List
		void AddRead(CMxAppData *pReq);

	protected:

		// Data Members
		UINT	m_uCount;
		WORD	m_wSession;

		// Read Request List
		CMxAppData * m_pHead;
		CMxAppData * m_pTail;
		
		// Implementation
		void Process(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bulk Read Request
//

class CMxAppBulkReadRequest : public CMxAppRequest
{
	public:
		// Constructor
		CMxAppBulkReadRequest(WORD wSession);

		// Destructor
		~CMxAppBulkReadRequest(void);

		// Read List
		void AddRead(CMxAppData *pReq);

	protected:

		// Data Members
		WORD	m_wSession;

		// Read Request List
		CMxAppData * m_pRead;
		
		// Implementation
		void Process(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi-Write Request
//

class CMxAppMultiWriteRequest : public CMxAppRequest
{
	public:
		// Constructor
		CMxAppMultiWriteRequest(WORD wSession);

		// Destructor
		~CMxAppMultiWriteRequest(void);

		// Read List
		void AddWrite(CMxAppData *pReq);

	protected:

		// Data Members
		UINT	m_uCount;
		WORD	m_wSession;

		// Write Request List
		CMxAppData * m_pHead;
		CMxAppData * m_pTail;
		
		// Implementation
		void Process(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi-Write with Mask Request 
//

class CMxAppMultiWriteMaskRequest : public CMxAppRequest
{
	public:
		// Constructor
		CMxAppMultiWriteMaskRequest(WORD wSession, BOOL fIsBit, UINT uBitOffset);

		// Destructor
		~CMxAppMultiWriteMaskRequest(void);

		// Read List
		void AddWrite(CMxAppData *pReq);

	protected:

		// Data Members
		UINT	m_uCount;
		WORD	m_wSession;
		UINT	m_uBitOffset;
		BOOL	m_fIsBit;

		// Write Request List
		CMxAppData * m_pHead;
		CMxAppData * m_pTail;
		
		// Implementation
		void Process(void);
		DWORD GetMask(UINT uSize);
		void AddData(UINT uSize, DWORD dwData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Bulk Write Request
//

class CMxAppBulkWriteRequest : public CMxAppRequest
{
	public:
		// Constructor
		CMxAppBulkWriteRequest(WORD wSession);

		// Destructor
		~CMxAppBulkWriteRequest(void);

		void AddWrite(CMxAppData *pReq);

	protected:

		// Data Members
		WORD	m_wSession;

		// Write Request
		CMxAppData * m_pWrite;
		
		// Implementation
		void Process(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// MX APP Reply Base Class
//

class CMxAppReply : public CMxAppCommsObject
{
	public:
		// Constructor
		CMxAppReply(void);

		// Destructor
		~CMxAppReply(void);

		virtual BOOL Parse(PBYTE pBuff, UINT uSize) = 0;
		BYTE GetError(void);

	protected:

		// Implementation
		BOOL CheckStatus(PBYTE pBuff, UINT uCount);

		// Data Members
		BYTE	m_bError;
	};

//////////////////////////////////////////////////////////////////////////
//
// Register Session Reply
//

class CMxAppRegisterReply : public CMxAppReply
{
	public:
		// Constructor
		CMxAppRegisterReply(void);

		// Destructor
		~CMxAppRegisterReply(void);

		// Reply Parsing
		BOOL Parse(PBYTE pBuff, UINT uSize);

		WORD GetSession(void);
		WORD GetPermLevel(void);
		PCTXT GetUser(void);

	protected:

		WORD m_wSession;
		WORD m_wPermLevel;
		CHAR m_User[17];
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi Read Reply
//

class CMxAppMultiReadReply : public CMxAppReply
{
	public:
		// Constructor
		CMxAppMultiReadReply(CMxAppData *pRead);

		// Destructor
		~CMxAppMultiReadReply(void);

		// Reply Parsing
		BOOL Parse(PBYTE pBuff, UINT uSize);

		// Data Access
		CMxAppData * GetDataObject();

	protected:

		CMxAppData	* m_pRead;
	};

//////////////////////////////////////////////////////////////////////////
//
// Bulk Read Reply
//

class CMxAppBulkReadReply : public CMxAppReply
{
	public:
		// Constructor
		CMxAppBulkReadReply(CMxAppData *pRead);

		// Destructor
		~CMxAppBulkReadReply(void);

		// Reply Parsing
		BOOL Parse(PBYTE pBuff, UINT uSize);

		// Data Access
		CMxAppData * GetDataObject();

	protected:

		CMxAppData	* m_pRead;
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi Write Reply
//

class CMxAppMultiWriteReply : public CMxAppReply
{
	public:
		// Constructor
		CMxAppMultiWriteReply(void);

		// Destructor
		~CMxAppMultiWriteReply(void);

		// Reply Parsing
		BOOL Parse(PBYTE pBuff, UINT uSize);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi Write Mask Reply
//

class CMxAppMultiWriteMaskReply : public CMxAppReply
{
	public:
		// Constructor
		CMxAppMultiWriteMaskReply(void);

		// Destructor
		~CMxAppMultiWriteMaskReply(void);

		// Reply Parsing
		BOOL Parse(PBYTE pBuff, UINT uSize);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Bulk Write Reply
//

class CMxAppBulkWriteReply : public CMxAppReply
{
	public:
		// Constructor
		CMxAppBulkWriteReply(void);

		// Destructor
		~CMxAppBulkWriteReply(void);

		// Reply Parsing
		BOOL Parse(PBYTE pBuff, UINT uSize);

	protected:
	};

#endif

// End of File
