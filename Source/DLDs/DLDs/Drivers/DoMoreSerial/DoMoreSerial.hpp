
#ifndef INCLUDE_DO_MORE_SERIAL

#define INCLUDE_DO_MORE_SERIAL

#include "DoMoreBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC Serial Driver
//

class CDoMoreSerialDriver : public CDoMoreBaseDriver
{
	public:		
		// Constructor
		CDoMoreSerialDriver(void);

		// Destructor
		~CDoMoreSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:

		struct CContext : public CBaseCtx
		{
			BYTE	m_bDrop;
			UINT	m_uTimeout;
			};

		CContext	* m_pCtx;
		WORD		  m_wCheck;

		// Port Access
		void Put(BYTE b);
		UINT Get(UINT uTimeout);

		// Transport Layer
		BOOL Transact(void);
		BOOL PutFrame(void);
		BOOL GetFrame(void);

		// Frame Building
		void StartHeader(void);
		BOOL StartRequest(CMxAppRequest &Req);

		// Checksum
		WORD GetChecksum(PBYTE pData, UINT uLen);
		void AddToChecksum(BYTE bData);
		void ClearChecksum(void);
	};

#endif

// End of File
