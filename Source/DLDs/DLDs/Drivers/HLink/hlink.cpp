#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "hlink.hpp"

// Instantiator

INSTANTIATE(CHardyLinkSerialMasterDriver);

//////////////////////////////////////////////////////////////////////////
//
// Hardy Instruments HardyLink Master Serial Driver
//
//

// Constructor

CHardyLinkSerialMasterDriver::CHardyLinkSerialMasterDriver(void)
{
	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx)); 

	m_Ident = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;
	}

// Destructor

CHardyLinkSerialMasterDriver::~CHardyLinkSerialMasterDriver(void)
{
	}

// Configuration

void MCALL CHardyLinkSerialMasterDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CHardyLinkSerialMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CHardyLinkSerialMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CHardyLinkSerialMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CHardyLinkSerialMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bID    = GetByte(pData);

			m_pCtx->m_bMode  = GetByte(pData);

			m_pCtx->m_Check  = GetByte(pData) > 0 ? FALSE : TRUE;

			m_pCtx->m_Error  = 0;
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CHardyLinkSerialMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CHardyLinkSerialMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = 2;
	Addr.a.m_Type   = addrRealAsReal;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CHardyLinkSerialMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;
	
	if( uOffset == T_ERR ) {

		pData[0] = m_pCtx->m_Error;

		return uCount;
		}

	BOOL fNamed = Addr.a.m_Table == addrNamed;

	UINT uTable = Addr.a.m_Table;

	if( IsWriteOnly(fNamed ? uOffset : uTable) ) {

		return uCount;
		}
	
	Start();

	AddByte('X');

	AddByte(0x20);

	AddSub(fNamed ? uOffset : uTable);
	
	End();
	
	if( Transact() ) {

		if( IsError() ) {

			pData[0] = 0;

			return uCount;
			}

		UINT uChars = GetChars(uOffset);

		BOOL fRaw   = IsRaw(uOffset);

		uOffset = fNamed ? 0 : uOffset;

		BOOL fReal   = IsReal(Addr.a.m_Type);

		for( UINT i = uOffset, u = 0; u < uCount; u++, i++ ) {

			pData[u] = fReal ? GetReal(i, uChars) : GetInt(i, uChars, fRaw);
		     	}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CHardyLinkSerialMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	BOOL fNamed  = Addr.a.m_Table == addrNamed;

	UINT uOffset = Addr.a.m_Offset;

	UINT uTable  = Addr.a.m_Table;

	if( uOffset == T_ERR ) {

		m_pCtx->m_Error = pData[0];

		return uCount;
		}
	
	if( IsReadOnly(fNamed ? uOffset : uTable) ) {

		return uCount;
		}

	if( IsCommand(fNamed ? uOffset : uTable) ) {

		if( pData[0] == 0 ) {

			return uCount;
			}

		Start();

		AddByte( IsAuto(uOffset) ? 'A' : 'C');

		AddByte(0x20);

		AddSub(fNamed ? uOffset : uTable);

		End();

		if( Transact() ) {

			return uCount;
			}

		return CCODE_ERROR;
		}

	MakeMin(uCount, 1);
	
	Start();

	AddByte('S');

	AddByte(0x20);

	AddSub(fNamed ? uOffset : uTable);

	AddByte(0x20);

	AddByte(uOffset + 0x30);

	AddByte(0x20);

	AddData(pData, Addr.a.m_Type);

	End();

	if( Transact() ) {

		return uCount;
		}
		
	return CCODE_ERROR;
	}
 
// Implementation

void CHardyLinkSerialMasterDriver::Start(void)
{
	m_uPtr = 0;

	AddByte('>');

	m_bCheck = 0;

	AddByte(m_pHex[m_pCtx->m_bID / 10]);

	AddByte(m_pHex[m_pCtx->m_bID % 10]);

	AddByte(0x20); 
	}

void CHardyLinkSerialMasterDriver::End(void)
{
	AddByte(CR);

	BYTE bCheck = m_bCheck;
	
	AddByte(m_pHex[bCheck / 0x10]);

	AddByte(m_pHex[bCheck % 0x10]);

	AddByte(EOT);
	}

void CHardyLinkSerialMasterDriver::AddByte(BYTE bByte)
{
	m_bTx[m_uPtr] = bByte;

	m_uPtr++;

	m_bCheck += bByte;
	}

void CHardyLinkSerialMasterDriver::AddSub(UINT uOffset)
{
	switch( uOffset ) {

		case T_A:	AddByte('A');					break;
		case T_G:	AddByte('G');					break;
		case T_N:	AddByte('N');					break;
		case T_T:	AddByte('T');					break;
		case T_DE:	AddByte('D');	AddByte('E');			break;
		case T_P:	AddByte('P');					break;
		case T_S:	AddByte('S');					break;
		case T_DI:	AddByte('D');	AddByte('I');			break;
		case T_L:	AddByte('L');					break;
		case T_REL:	AddByte('R');	AddByte('E');	AddByte('L');	break;
		case T_REM:	AddByte('R');	AddByte('E');	AddByte('M');	break;
		case T_C:	AddByte('C');					break;
		case T_E:	AddByte('E');					break;
		case T_I:	AddByte('I');					break;
		case T_IREM:	AddByte('R');	AddByte('E');	AddByte('M');	break;
		case T_M:	AddByte('M');					break;
		case T_U:	AddByte('U');					break;
		case T_H:	AddByte('H');					break;
		case T_AT:	AddByte('T');					break;
		case T_AZ:	AddByte('Z');					break;
		case T_NTOT:	AddByte('N');					break;
		}

	}

void CHardyLinkSerialMasterDriver::AddData(PDWORD pData, UINT uType)
{	
	memset(m_Data, 0x20, sizeof(m_Data));

	UINT uBytes = 6;
	
	if( IsReal(uType) ) {

		float Data = I2R(pData[0]);

		if( m_pCtx->m_bMode == M_LB ) {

			Data = Data * 0.45359237;
			}

		SPrintf(m_Data, "%f", PassFloat(R2I(Data)));
		
		uBytes++;
		}

	else {
		SPrintf(m_Data, "%d", pData[0]);
		}

	for( UINT u = 0; u < uBytes; u++ ) {

		AddByte(m_Data[u]);
		}
	}

// Transport Layer

BOOL CHardyLinkSerialMasterDriver::Transact(void)
{
	if( Send() && Recv() && CheckFrame() ) {

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CHardyLinkSerialMasterDriver::Send(void)
{
	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CHardyLinkSerialMasterDriver::Recv(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	BOOL fStart = FALSE;

	m_uPtr = 0;  

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( !fStart && uData == '>' ) {

			fStart = TRUE;

			continue;
			}

		if( fStart ) {

			if( uData == EOT ) {

				return TRUE;
				}

			m_bRx[m_uPtr] = uData;

			m_uPtr++;
			}
		}
	
	return FALSE;
	}

BOOL CHardyLinkSerialMasterDriver::CheckFrame(void)
{
	m_bCheck = 0;

	for( UINT u = 0; u < m_uPtr - 2; u++ ) {

		m_bCheck += m_bRx[u];
		}

	BYTE bCheck = ByteFromAscii(m_bRx[m_uPtr - 2]) << 4;

	bCheck     |= ByteFromAscii(m_bRx[m_uPtr - 1]);
	
	if( m_pCtx->m_Check && (m_bCheck != bCheck) ) {

		return FALSE;
		}

	BOOL fSuccess = IsAck() ? TRUE : !memcmp(m_bTx + 1, m_bRx, 6);

	if( !fSuccess ) {

		fSuccess = !memcmp(m_bTx + 1, m_bRx, 2);

		if( fSuccess ) {

			if( IsError() ) {

				m_pCtx->m_Error = 0;

				for( UINT u = 4; m_bRx[u] != 0xD; u++ ) {

					BYTE bByte = m_bRx[u] - 0x30;

					if( bByte <= 9 ) {

						m_pCtx->m_Error = (m_pCtx->m_Error << 4) | bByte;
						}
					}

				return TRUE;
				}
			}

		return FALSE;
		}

	return fSuccess;
	}

// Helpers

BOOL CHardyLinkSerialMasterDriver::IsReadOnly(UINT uOffset)
{
	switch( uOffset ) {

		case T_A:
		case T_G:
		case T_N:
		case T_T:
		case T_DI:
		case T_L:
		case T_REL:
		case T_REM:
		case T_C:
		case T_E:
		case T_I:
		case T_IREM:
			
			break;
		
		}

	return FALSE;
	}

BOOL CHardyLinkSerialMasterDriver::IsWriteOnly(UINT uOffset)
{
	switch( uOffset ) {

		case T_M:
		case T_U:
		case T_H:
		case T_AT:
		case T_AZ:
		case T_NTOT:

			return TRUE;
		
		}

	return FALSE;
	}

BOOL CHardyLinkSerialMasterDriver::IsCommand(UINT uOffset)
{
	switch( uOffset ) {

		case T_M:
		case T_U:
		case T_H:
		case T_AT:
		case T_AZ:
		case T_NTOT:

			return TRUE;
		
		}

	return FALSE;
	}

BOOL CHardyLinkSerialMasterDriver::IsAuto(UINT uOffset)
{
	switch( uOffset ) {

		case T_AT:
		case T_AZ:
	
			return TRUE;
		}

	return FALSE;
	}

BOOL CHardyLinkSerialMasterDriver::IsReal(UINT uType)
{
	switch( uType ) {

		case addrLongAsReal:
		case addrRealAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CHardyLinkSerialMasterDriver::IsRaw(UINT uOffset)
{
	switch( uOffset ) {

		case T_E:

			return TRUE;
		}
	
	return FALSE;
	}

DWORD CHardyLinkSerialMasterDriver::GetReal(UINT uIndex, UINT uChars)
{
	float dwFloat = 0.0;

	BOOL fNeg = FALSE;

	BOOL fDP  = FALSE;

	UINT uDiv = 10;

	for( UINT u = FindData(uIndex) + 5 + uChars; m_bRx[u] != 0xA; u++ ) {

		if( m_bRx[u] == 0x20 ) {

			continue;
			}

		if( m_bRx[u] == '-' ) {

			fNeg = TRUE;

			continue;
			}

		if( m_bRx[u] == '.' ) {

			fDP = TRUE;

			continue;
			}

		BYTE bByte = m_bRx[u] - 0x30;

		if( bByte <= 9 ) {

			if( !fDP ) {

				dwFloat *= 10;

				dwFloat += bByte;
				}
			else {
				float Add = float(bByte) / float(uDiv);
				
				dwFloat += Add;

				uDiv *= 10;
				}

			continue;
	   		}

		if( fNeg ) {

			dwFloat = -dwFloat;
			}
    
		return Convert(R2I(dwFloat), u);
		}       
	       	    
	return 0;
	}

DWORD CHardyLinkSerialMasterDriver::GetInt(UINT uIndex, UINT uChars, BOOL fRaw)
{
	DWORD dwData = 0;

	BOOL fNeg = FALSE;

	BOOL fDP  = FALSE;
	
	for( UINT u = FindData(uIndex) + 5 + uChars; m_bRx[u] != 0xA; u++ ) {

		if( m_bRx[u] == 0x20 ) {

			continue;
			}
		 
		if( m_bRx[u] == '-' ) {

			fNeg = TRUE;

			continue;
			}

		if( m_bRx[u] == '.' ) {

			fDP = TRUE;

			continue;
			}

		BYTE bByte = fRaw ? m_bRx[u] : ByteFromAscii(m_bRx[u]);

		if( !fRaw && bByte <= 0xF  ) {

			dwData *= 16;

			dwData += bByte;

			continue;
			}
		
		if( fRaw ) {

			if( bByte == 0xD ) {

				return dwData;
				}

			dwData <<= 8;

			dwData += (bByte << 8);

			continue;
			}

		if( fNeg ) {

			dwData = -dwData;
			}

		return dwData;
		}
		
	return 0;   
	}

DWORD CHardyLinkSerialMasterDriver::Convert(DWORD Data, UINT uIndex)
{     
	BYTE bMode[2];
	
	for( UINT u = uIndex, i = 0; i < 2; u++, i++ ) {

		bMode[i] = m_bRx[u];
		}

	BOOL fKg = bMode[0] == 'k' && bMode[1] == 'g';

	float dwFloat = 0.0;

	if( fKg ) {

		if( m_pCtx->m_bMode == M_LB ) {
			
			dwFloat = I2R(Data) *  2.20462262185;

			return R2I(dwFloat);
			}

		return Data;
		}
	
	BOOL flb = bMode[0] == 'L' && bMode[1] == 'B';

	if( flb ) {

		if( m_pCtx->m_bMode == M_KG ) {

			dwFloat  = I2R(Data) *  0.45359237; 

			return R2I(dwFloat);
			}

		return Data;
		}

	return 0;
	}

BYTE CHardyLinkSerialMasterDriver::ByteFromAscii(BYTE bByte)
{
	if( bByte >= '0' && bByte <= '9' )

		return bByte - '0';

	return	bByte - '@' + 9;
	}

BOOL CHardyLinkSerialMasterDriver::IsError(void)
{
	return m_bRx[3] == 'N'; 
	}

BOOL CHardyLinkSerialMasterDriver::IsAck(void)
{
	return m_bRx[3] == 'A'; 
	}

UINT CHardyLinkSerialMasterDriver::FindData(UINT uIndex)
{
	if( uIndex > 0 ) {

		for( UINT u = 0, i = 0; u < m_uPtr; i++, u++ ) {

			while( m_bRx[u] != 0xA ) {

				u++;
				}

			if( i == uIndex - 1 ) {

				return u + 2;
				}
			}
		}

	return 0;
	}

UINT CHardyLinkSerialMasterDriver::GetChars(UINT uOffset)
{
	switch( uOffset ) {
			
		case T_DE:	
		case T_DI:
			
			return 2;

		case T_REL:	
		case T_REM:
		case T_IREM:

			return 3;
		}

	return 1;
	}

// End of File

