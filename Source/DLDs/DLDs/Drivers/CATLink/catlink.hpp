
//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CATLINK_HPP
	
#define	INCLUDE_CATLINK_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCatLinkBase;

class CCatLinkDriver;

class CCatLinkHandler;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	pollLimitLow	4

#define	pollLimitStd	15

#define	slotCount	8

//////////////////////////////////////////////////////////////////////////
//
// Special PIDs
//

#define	pidRPM		0x0040

#define	pidVoltage	0xF464

#define	pidSerial	0xF810

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	IsSelf(x)	((x) == m_bSelf)

//////////////////////////////////////////////////////////////////////////
//
// Timeous
//

#define	timeMissing	4000	// Time between read and attempt to find PID
#define	timeRetry	100	// Time to wait if too many polls outstanding
#define	timeImmediate	10	// Time to force an immediate response
#define	timePoll	2000	// Time to between two polls for same PID
#define	timePresent	8000	// Time limit between broadcasts to avoid poll
#define	timeReply	200	// Time to wait for reply to poll message
#define	timeDefer	20000	// Time to wait between scans for missing PIDs
#define	timeInitFault	30000	// Time to listen for fault broadcasts
#define	timeNextFault	10000	// Time to listen between fault broadcasts
#define	timeDetails	5000	// Time to wait for fault details reply

//////////////////////////////////////////////////////////////////////////
//
// MID Status
//

enum
{
	midIdle		= 0,
	midSeen		= 1,
	midForce	= 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Fault States
//

enum
{
	faultIdle	= 0,
	faultListen	= 1,
	faultScan	= 2,
	faultReply	= 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Item States
//

enum
{
	itemIdle	= 0,
	itemMissing	= 1,
	itemFindBusy    = 2,
	itemDefer	= 3,
	itemFind	= 4,
	itemFound	= 5,
	itemPolled	= 6,
	itemPollBusy	= 7,
	itemPresent	= 8,
	itemSent        = 9,
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Shared Functions
//

class CCatLinkBase
{
	protected:
		// Data and Name Access
		UINT GetNameLen(BYTE bData);
		UINT GetDataLen(BYTE bData);
		UINT GetNameLen(UINT uName);
		UINT GetDataLen(UINT uName);
		UINT GetNameBytes(PBYTE pName, UINT uLen);
		UINT GetDataBytes(PBYTE pData, UINT uLen);
		void SetNameBytes(PBYTE pName, UINT uName, UINT uLen);
		void SetDataBytes(PBYTE pData, UINT uData, UINT uLen);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Driver
//

class CCatLinkDriver : public CMasterDriver, public CCatLinkBase
{
	public:
		// Constructor
		CCatLinkDriver(void);

		// Destructor
		~CCatLinkDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Ping   (void);
		DEFMETH(CCODE) Read   (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write  (AREF Addr, PDWORD pData, UINT uCount);

		// Handler Calls
		BOOL SeeDrop(BYTE bSrc, BOOL fForce);
		BOOL SetData(BYTE bSrc, BYTE bDest, UINT uName, UINT uData);
		BOOL SetData(PCTXT pSerial);
		UINT GetSend(PBYTE pData);
		BOOL GetNext(void);
		BOOL OnFault(BYTE bSrc, PBYTE pData, UINT uCount);
		BOOL WriteOK(BYTE bSrc, UINT uName);

	protected:
		// Scan Set
		struct CScanSet
		{
			BOOL	m_fEnable;
			BYTE	m_bFrom;
			BYTE	m_bTo;
			UINT	m_uDelay;
			};

		// Data Item
		struct CDataItem
		{
			UINT	m_uName;
			WORD	m_wState;
			WORD	m_wTries;
			BYTE	m_bSrc;
			BYTE	m_bTx;
			BYTE	m_bRx;
			BYTE	m_fSkip:1;
			BOOL	m_fSend:1;
			BOOL	m_fDone:1;
			BYTE	m_uPoll:5;
			UINT	m_uTime;
			DWORD	m_Data;
			DWORD	m_Write;
			};

		// Fault State
		struct CFaultState
		{
			WORD	m_wState;
			UINT	m_uTime;
			UINT	m_uScan;
			UINT	m_uPartial;
			UINT	m_uBusy;
			BYTE	m_bRx[slotCount][100];
			};

		// Fault Data
		struct CFaultData
		{
			BYTE	m_bSrc;
			BYTE	m_bSubcode;
			BYTE	m_bFlags;
			BYTE	m_bRaw;
			WORD	m_wCode;
			WORD	m_wCount;
			DWORD	m_dwFirst;
			DWORD	m_dwLast;
			};

		// Config Data
		BYTE     m_bSelf;
		BOOL     m_fUseFaults;
		BOOL     m_fUseDetail;
		BOOL     m_fManual;
		CScanSet m_Scan[5];

		// Data Members
		CCatLinkHandler * m_pHandler;
		CDataItem	* m_pData[0x4100];
		BYTE		  m_bSeen[256];
		UINT		  m_uPend[256];
		CDataItem *	  m_pPoll[1024];
		char		  m_sSerial[12];
		UINT		  m_uActive;
		UINT		  m_uScan;
		UINT		  m_uHead;
		UINT		  m_uTail;
		BOOL		  m_fFaults;
		BOOL		  m_fDetail;
		BOOL		  m_fChange;
		UINT		  m_uHWM;
		CFaultState *	  m_pFaultState[256];
		CFaultData	  m_FaultList  [80];
		CFaultData	  m_FaultWork  [80];
		UINT		  m_uPause;
		UINT		  m_uLast1;
		UINT		  m_uLast2;
		UINT		  m_uStart;
		UINT		  m_uManual;
		BOOL		  m_fTest;
		BYTE		  m_bECM;
		BYTE		  m_bEMCP;

		// Fault State Machine
		void UpdateFaults(void);
		void PresentFaults(void);
		void UpdateFault(BYTE bDrop, CFaultState *pFault);
		BOOL ProcessFault(CFaultState *pFault, UINT uRx);
		BOOL ProcessDetails(CFaultState *pFault, UINT uRx);
		void ClearFaults(BYTE bDrop, CFaultState *pFault, BOOL fDiag);
		UINT AllocFault(void);
		void SetTime(CFaultState *pFault, UINT uTime);
		BOOL TimeOut(CFaultState *pFault);
		BOOL QueuePoll(BYTE bDrop, BYTE bCode);

		// Item State Machine
		void UpdateItems(void);
		void UpdateItem(UINT uName, CDataItem *pItem);
		BOOL OnPresentData(CDataItem *pItem);
		BOOL OnPolledData(CDataItem *pItem);
		void SetTime(CDataItem *pItem, UINT uTime);
		BOOL TimeOut(CDataItem *pItem);
		BOOL FindInitTarget(CDataItem *pItem);
		void FindNextTarget(CDataItem *pItem);
		BOOL IsValidTarget(UINT uName, BYTE bDrop);
		BOOL IsValidTarget(BYTE bDrop);
		BOOL IsValidECM(BYTE bDrop);
		BOOL IsValidEMCP(BYTE bDrop);
		void StepTarget(BYTE &bDrop);
		void AddRange(BYTE bFrom, BYTE bTo);
		void AddPoll(CDataItem *pItem);
		void SubPoll(CDataItem *pItem);
		BOOL QueuePoll(CDataItem *pItem);
		BOOL HasLowLimit(BYTE bDrop);

		// Data Item Access
		CDataItem * & GetItem(UINT uName);

		// Item Name Helpers
		BOOL IsValidName   (UINT uName);
		BOOL IsKeyName     (UINT uName);
		BOOL IsWritable    (UINT uName);
		BYTE GetInitialMID (UINT uName);
		BOOL SignExtendByte(UINT uName);
		BOOL SignExtendWord(UINT uName);
		UINT GetName       (UINT uIndex);

		// Implementation
		void CheckPause(BOOL fSleep);
		BOOL FindNextScanSet(void);

		// Fault Sorting
		static int SortFunc(PCVOID p1, PCVOID p2);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Handler
//

class CCatLinkHandler : public IPortHandler, public CCatLinkBase
{	
	public:
		// Constructor
		CCatLinkHandler(IHelper *pHelper, CCatLinkDriver *pDriver, BYTE bSelf);

		// Destructor
		~CCatLinkHandler(void);

		// Operations
		void Start(void);
		void Stop(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);

	protected:
		// Data Members
		ULONG            m_uRefs;
		IHelper        * m_pHelper;
		CCatLinkDriver * m_pDriver;
		BYTE		 m_bSelf;
		IPortObject    * m_pPort;
		BYTE		 m_bRxData[128];
		BYTE		 m_bTxData[128];
		UINT		 m_uRxPtr;
		UINT		 m_uTxPtr;
		UINT		 m_uTxSize;
		BOOL		 m_fRun;
		BOOL		 m_fSync;
		BOOL		 m_fSelf;

		// Implementation
		void Process(BYTE bSrc, BYTE bDest, PBYTE pData, UINT uCount);
		BOOL IsUnknown(PBYTE pData);
		void ClearContention(void);
		BOOL CheckContention(void);
		void StartEdgeTimer(BOOL fSlow);
	};

// End of File

#endif
