
#include "intern.hpp"

#include "catlink.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CCatLinkDriver);

// Constructor

CCatLinkDriver::CCatLinkDriver(void)
{
	m_Ident      = DRIVER_ID;

	m_bSelf      = 0xAA;

	m_fManual    = FALSE;

	m_uLast1     = 0;

	m_uLast2     = 0;

	m_fUseFaults = FALSE;

	m_fUseDetail = FALSE;
	}

// Destructor

CCatLinkDriver::~CCatLinkDriver(void)
{
	}

// Configuration

void MCALL CCatLinkDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bSelf      = GetByte(pData);
		
		m_fUseFaults = GetByte(pData);
		
		m_fUseDetail = GetByte(pData);
		
		m_fManual    = GetByte(pData);

		m_uManual    = NOTHING;

		if( m_fManual ) {
			
			for( UINT n = 0; n < elements(m_Scan); n++ ) {

				m_Scan[n].m_fEnable = GetByte(pData);
				
				m_Scan[n].m_bFrom   = GetByte(pData);
				
				m_Scan[n].m_bTo     = GetByte(pData);
				
				m_Scan[n].m_uDelay  = GetWord(pData);
				}

			if( !FindNextScanSet() ) {

				m_fManual = FALSE;
				}
			}

		return;
		}
	}
	
void MCALL CCatLinkDriver::CheckConfig(CSerialConfig &Config)
{
	// NOTE : Link actually runs at 62,500 Baud, but the
	// port object doesn't know it's got a 4MHz crystal.

	Config.m_uBaudRate = 56000;
	Config.m_uDataBits = 8;
	Config.m_uStopBits = 1;
	Config.m_uParity   = parityNone;
	Config.m_uFlags    = flagFastRx | flagTimeout;
	Config.m_uPhysical = 2;
	}
	
// Management

void MCALL CCatLinkDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CCatLinkHandler(m_pHelper, this, m_bSelf);

	pPort->Bind(m_pHandler);
	}

void MCALL CCatLinkDriver::Detach(void)
{
	m_pHandler->Stop();

	m_pHandler->Release();
	}

void MCALL CCatLinkDriver::Open(void)
{
	memset(m_bSeen, 0, sizeof(m_bSeen));

	memset(m_uPend, 0, sizeof(m_uPend));

	memset(m_pData, 0, sizeof(m_pData));

	m_uScan   = 0;

	m_uActive = 0;

	m_uHead   = 0;

	m_uTail   = 0;

	memset(m_pFaultState, 0, sizeof(m_pFaultState));

	memset(m_FaultList,   0, sizeof(m_FaultList));

	memset(m_FaultWork,   0, sizeof(m_FaultWork));

	memset(m_sSerial,     0, sizeof(m_sSerial));

	m_uHWM    = 2;

	m_fFaults = FALSE;

	m_fDetail = FALSE;

	m_fChange = FALSE;

	m_fTest   = FALSE;

	m_bECM    = 0;

	m_bEMCP   = 0;

	m_uStart  = GetTimeStamp();

	m_pHandler->Start();
	}

// Device

CCODE MCALL CCatLinkDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	return CCODE_SUCCESS;
	}

CCODE MCALL CCatLinkDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

void MCALL CCatLinkDriver::Service(void)
{
	if( m_fManual ) {

		if( !m_bECM ) {

			if( m_uManual < elements(m_Scan) ) {

				CScanSet &Scan = m_Scan[m_uManual];

				UINT     uTime = GetTimeStamp();

				if( uTime - m_uStart > Scan.m_uDelay ) {

					AddRange(Scan.m_bFrom, Scan.m_bTo);

					if( !FindNextScanSet() ) {

						for( UINT n = 0; n < elements(m_pData); n++ ) {

							if( m_pData[n] ) {

								m_pData[n]->m_uPoll = 0;
								}
							}
						}

					m_uStart = uTime;
					}
				}
			}
		}
	else {
		if( !m_uActive ) {

			if( GetTimeStamp() - m_uStart > 10 ) {

				AddRange(0x0E, 0x13);

				AddRange(0x21, 0x29);

				AddRange(0x58, 0x5F);
				}
			}
		}

	UpdateItems();
	}

CCODE MCALL CCatLinkDriver::Ping(void)
{
	return CCODE_SUCCESS;
	}

CCODE MCALL CCatLinkDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == 100 ) {

		UINT s = Addr.a.m_Offset;

		for( UINT n = 0; n < uCount; n++ ) {

			if( s < elements(m_bSeen) ) { 

				pData[n] = m_bSeen[s];
				}
			else
				pData[n] = 0;

			s++;
			}

		return uCount;
		}

	if( Addr.a.m_Table == 101 ) {

		CDataItem * & pItem = GetItem(pidSerial);

		if( !pItem ) {

			CDataItem *pMake = new CDataItem;

			memset(pMake, 0, sizeof(CDataItem));

			pMake->m_uName  = pidSerial;

			pMake->m_bSrc   = GetInitialMID(pidSerial);

			pMake->m_wState = itemMissing;

			pMake->m_Write  = 0xAAAAAAAA;

			SetTime(pMake, timeMissing);

			pItem = pMake;
			}

		if( TRUE ) {

			UINT s = Addr.a.m_Offset;

			for( UINT n = 0; n < uCount; n++ ) {

				if( s < elements(m_sSerial) ) { 

					pData[n] = BYTE(m_sSerial[s]);
					}
				else
					pData[n] = 0;

				s++;
				}
			}

		return uCount;
		}

	if( Addr.a.m_Table >= 1 && Addr.a.m_Table <= 7 ) {

		if( m_fUseFaults ) {

			m_fFaults  = TRUE;

			UINT uItem = Addr.a.m_Offset;

			if( uItem < elements(m_FaultList) ) {

				CFaultData *pList = m_FaultList + uItem;

				if( Addr.a.m_Table > 5 ) {

					if( m_fUseDetail ) {

						m_fDetail = TRUE;
						}
					}

				switch( Addr.a.m_Table ) {

					case  1: *pData = pList->m_bSrc;     return 1;
					case  2: *pData = pList->m_wCode;    return 1;
					case  3: *pData = pList->m_bSubcode; return 1;
					case  4: *pData = pList->m_bFlags;   return 1;
					case  5: *pData = pList->m_wCount;   return 1;
					case  6: *pData = pList->m_dwFirst;  return 1;
					case  7: *pData = pList->m_dwLast;   return 1;
					}
				}
			}

		*pData = 0;

		return 1;
		}

	if( Addr.a.m_Table >= addrNamed ) {

		UINT uName = Addr.a.m_Offset;

		UINT uType = (Addr.a.m_Table - addrNamed) % 3 + 1;

		UINT uFunc = (Addr.a.m_Table - addrNamed) / 3 + 0;

		switch( uType ) {

			case 1:	uName += 0x000000; break;
			case 2:	uName += 0xD00000; break;
			case 3:	uName += 0xD10000; break;
			}

		if( IsValidName(uName) ) {

			CDataItem * & pItem = GetItem(uName);

			if( !pItem ) {

				CDataItem *pMake = new CDataItem;

				memset(pMake, 0, sizeof(CDataItem));

				pMake->m_uName = uName;

				pMake->m_bSrc  = GetInitialMID(uName);

				pMake->m_Write = 0xAAAAAAAA;

				pItem = pMake;
				}

			if( uFunc == 0 ) {

				if( !pItem->m_fDone ) {

					UpdateItem(uName, pItem);

					pItem->m_fDone = TRUE;
					}

				switch( pItem->m_wState ) {

					case itemIdle:	

						pItem->m_wState = itemMissing;

						SetTime(pItem, timeMissing);

						break;

					case itemFound:
					case itemPolled:
					case itemPollBusy:
					case itemPresent:

						*pData = pItem->m_Data;

						CheckPause(FALSE);

						return 1;
					}

				*pData = 0;

				CheckPause(FALSE);

				return 1;
				}

			if( uFunc == 1 ) {

				switch( pItem->m_wState ) {

					case itemFound:
					case itemPolled:
					case itemPollBusy:
					case itemPresent:

						*pData = MAKEWORD(pItem->m_bSrc, pItem->m_wState);

						return 1;
					}
				
				*pData = MAKEWORD(0, pItem->m_wState);

				return 1;
				}

			if( uFunc == 2 ) {

				*pData = !pItem->m_fSkip;

				return 1;
				}
			}
		}

	*pData = 0;

	return 1;
	}

CCODE MCALL CCatLinkDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table >= addrNamed ) {

		UINT uName = Addr.a.m_Offset;

		UINT uType = (Addr.a.m_Table - addrNamed) % 3 + 1;

		UINT uFunc = (Addr.a.m_Table - addrNamed) / 3 + 0;

		switch( uType ) {

			case 1:	uName += 0x000000; break;
			case 2:	uName += 0xD00000; break;
			case 3:	uName += 0xD10000; break;
			}

		if( IsValidName(uName) ) {

			CDataItem * & pItem = GetItem(uName);

			if( pItem ) {

				if( uFunc == 0 ) {

					if( IsWritable(uName) ) {

						switch( pItem->m_wState ) {

							case itemFound:
							case itemPolled:
							case itemPollBusy:
							case itemPresent:

								if( pItem->m_Data != *pData ) {

									pItem->m_Write = *pData;

									pItem->m_fSend = TRUE;
									}
								break;
							}
						}

					return 1;
					}

				if( uFunc == 2 ) {

					pItem->m_fSkip = !*pData;

					return 1;
					}
				}
			}
		}

	return 1;
	}

// Fault State Machine

void CCatLinkDriver::UpdateFaults(void)
{
	if( m_fFaults ) {

		BOOL fOkay = TRUE;

		for( UINT n = 0x0E; n < 0xF0; n++ ) {

			if( IsValidTarget(n) ) {

				CFaultState *pFault = m_pFaultState[n];

				if( !pFault ) {

					pFault = new CFaultState;

					memset(pFault, 0, sizeof(CFaultState));

					m_pFaultState[n] = pFault;
					}

				UpdateFault(n, pFault);
					
				if( pFault->m_uPartial ) {

					fOkay = FALSE;
					}
				}
			else {
				CFaultState *pFault = m_pFaultState[n];

				if( pFault ) {
					
					if( pFault->m_wState > faultIdle ) {

						ClearFaults(n, pFault, TRUE);

						ClearFaults(n, pFault, FALSE);

						pFault->m_wState = faultIdle;
						}
					}
				}
			}

		if( m_fChange && fOkay ) {

			PresentFaults();

			m_fChange = FALSE;
			}
		}
	}

void CCatLinkDriver::PresentFaults(void)
{
	qsort (m_FaultWork, m_uHWM, sizeof(CFaultData), SortFunc);

	Critical(TRUE);

	memcpy(m_FaultList, m_FaultWork, m_uHWM * sizeof(CFaultData));

	Critical(FALSE);
	}

void CCatLinkDriver::UpdateFault(BYTE bDrop, CFaultState *pFault)
{
	BOOL fHit = FALSE;

	UINT n;

	switch( pFault->m_wState ) {

		case faultIdle:

			SetTime(pFault, timeInitFault);

			pFault->m_wState = faultListen;

			break;

		case faultListen:

			for( n = 0; n < slotCount; n++ ) {

				PBYTE pSlot = pFault->m_bRx[n];

				if( pSlot[0] ) {

					ProcessFault(pFault, n);

					pSlot[0] = 0;

					fHit     = TRUE;
					}
				}

			if( fHit ) {

				if( pFault->m_uPartial ) {
					
					if( !pFault->m_uBusy ) {

						pFault->m_wState = faultScan;

						pFault->m_uScan  = 0;

						SetTime(pFault, timeImmediate);

						break;
						}
					}

				SetTime(pFault, timeNextFault);

				break;
				}

			if( TimeOut(pFault) ) {

				QueuePoll(bDrop, 0x0D);

				QueuePoll(bDrop, 0x0E);

				SetTime(pFault, timeInitFault);
				}
			
			break;

		case faultScan:

			if( pFault->m_uPartial ) {

				UINT &uScan = pFault->m_uScan;

				for(;;) {

					CFaultData *pWork = m_FaultWork + uScan;

					if( pWork->m_bSrc == bDrop ) {

						if( pWork->m_wCount == 0 ) {

							QueuePoll(bDrop, 0x00);

							pFault->m_wState = faultReply;

							break;
							}
						}

					uScan = (uScan+1) % m_uHWM;
					}

				break;
				}

			SetTime(pFault, timeNextFault);

			pFault->m_wState = faultListen;

			break;

		case faultReply:

			for( n = 0; n < slotCount; n++ ) {

				PBYTE pSlot = pFault->m_bRx[n];

				if( pSlot[0] ) {

					if( pFault->m_wState == faultReply ) {

						if( ProcessDetails(pFault, n) ) {

							pFault->m_wState = faultScan;

							SetTime(pFault, timeImmediate);

							fHit = TRUE;
							}

						pSlot[0] = 0;

						continue;
						}

					ProcessFault(pFault, n);

					pSlot[0] = 0;
					}
				}

			if( !fHit ) {
				
				if( TimeOut(pFault) ) {

					pFault->m_wState = faultScan;

					SetTime(pFault, timeImmediate);
					
					break;
					}
				}

			break;
		}
	}

BOOL CCatLinkDriver::ProcessFault(CFaultState *pFault, UINT uRx)
{
	BYTE  bSrc  = pFault->m_bRx[uRx][0];

	PBYTE pData = pFault->m_bRx[uRx] + 1;

	if( pData[0] == 0xFA && pData[1] == 0x0D ) {

		// Diagnostic Codes

		BYTE bSize = pData[2] - 2;

		BYTE bType = pData[4];

		if( bType == 0x80 ) {

			return FALSE;
			}

		if( bType == 0x70 || bType == 0x60 ) {

			ClearFaults(bSrc, pFault, TRUE);
			}

		if( bType == 0x60 ) {

			pFault->m_uBusy |=  1;
			}

		if( bType == 0x30 ) {

			pFault->m_uBusy &= ~1;
			}

		UINT uPtr = 5;

		while( bSize ) {

			UINT        uFind = AllocFault();

			CFaultData *pWork = m_FaultWork + uFind;

			pWork->m_bSrc     = bSrc;
			
			pWork->m_wCode    = MAKEWORD(pData[uPtr+1], pData[uPtr+0]);

			pWork->m_bSubcode = (pData[uPtr+2]&15);

			pWork->m_bFlags   = (0x80 | ((~pData[uPtr+3]&2)>>1));

			pWork->m_bRaw     = pData[uPtr+2];

			pWork->m_wCount   = m_fDetail ? 0 : 1;

			pWork->m_dwFirst  = 0;

			pWork->m_dwLast   = 0;

			if( m_fDetail ) {
				
				pFault->m_uPartial++;
				}

			uPtr  += 4;

			bSize -= 4;
			}

		return TRUE;
		}

	if( pData[0] == 0xFA && pData[1] == 0x0E ) {

		// Event Codes

		BYTE bSize = pData[2] - 2;

		BYTE bType = pData[4];

		if( bType == 0x80 ) {

			return FALSE;
			}

		if( bType == 0x70 || bType == 0x60 ) {

			ClearFaults(bSrc, pFault, FALSE);
			}

		if( bType == 0x60 ) {

			pFault->m_uBusy |=  2;
			}

		if( bType == 0x30 ) {

			pFault->m_uBusy &= ~2;
			}

		UINT uPtr = 5;

		while( bSize ) {

			UINT        uFind = AllocFault();

			CFaultData *pWork = m_FaultWork + uFind;

			pWork->m_bSrc     = bSrc;
			
			pWork->m_wCode    = MAKEWORD(pData[uPtr+1], pData[uPtr+0]);

			pWork->m_bSubcode = (pData[uPtr+2]>>5);

			pWork->m_bFlags   = (0x00 | ((~pData[uPtr+2]&2)>>1));

			pWork->m_bRaw     = pData[uPtr+2];

			pWork->m_wCount   = m_fDetail ? 0 : 1;

			pWork->m_dwFirst  = 0;

			pWork->m_dwLast   = 0;

			if( m_fDetail ) {
				
				pFault->m_uPartial++;
				}

			uPtr  += 3;

			bSize -= 3;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::ProcessDetails(CFaultState *pFault, UINT uRx)
{
	PBYTE pData = pFault->m_bRx[uRx] + 1;

	if( pData[0] == 0xFA && pData[1] == 0x10 ) {

		CFaultData *pWork = m_FaultWork + pFault->m_uScan;

		if( (pWork->m_bFlags & 0x80) == 0x00 ) {

			if( pData[3] == HIBYTE(pWork->m_wCode) ) {

				if( pData[4] == LOBYTE(pWork->m_wCode) ) {

					if( pWork->m_wCount == 0 ) {

						pWork->m_wCount  = IntelToHost(PWORD (pData+7)[0]);

						pWork->m_dwFirst = IntelToHost(PDWORD(pData+9)[0]);

						pWork->m_dwLast  = IntelToHost(PDWORD(pData+9)[1]);

						if( pWork->m_wCount == 0 ) pWork->m_wCount = 666;

						pFault->m_uPartial--;
						}

					return TRUE;
					}
				}
			}
		}

	if( pData[0] == 0xFA && pData[1] == 0x12 ) {

		CFaultData *pWork = m_FaultWork + pFault->m_uScan;

		if( (pWork->m_bFlags & 0x80) == 0x80 ) {

			if( pData[3] == HIBYTE(pWork->m_wCode) ) {

				if( pData[4] == LOBYTE(pWork->m_wCode) ) {

					if( pWork->m_wCount == 0 ) {

						pWork->m_wCount  = IntelToHost(PWORD (pData+7)[0]);

						pWork->m_dwFirst = IntelToHost(PDWORD(pData+9)[0]);

						pWork->m_dwLast  = IntelToHost(PDWORD(pData+9)[1]);

						if( pWork->m_wCount == 0 ) pWork->m_wCount = 666;

						pFault->m_uPartial--;
						}

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

void CCatLinkDriver::ClearFaults(BYTE bSrc, CFaultState *pFault, BOOL fDiag)
{
	BYTE bMask = 0x80;

	BYTE bData = fDiag ? 0x80 : 0x00;

	for( UINT n = 0; n < m_uHWM; n++ ) {

		if( m_FaultWork[n].m_bSrc == bSrc ) {

			if( (m_FaultWork[n].m_bFlags & bMask) == bData ) {

				if( !m_FaultWork[n].m_wCount ) {

					pFault->m_uPartial--;
					}

				memset(m_FaultWork + n, 0, sizeof(CFaultData));
				}
			}
		}

	m_fChange = TRUE;
	}

UINT CCatLinkDriver::AllocFault(void)
{
	for( UINT n = 0; n < elements(m_FaultWork); n++ ) {

		if( m_FaultWork[n].m_bSrc ) {

			continue;
			}

		if( n >= m_uHWM ) {
			
			m_uHWM = n + 1;
			}

		return n;
		}

	// TODO -- What do we do here? We're out of codes!

	return 0;
	}

void CCatLinkDriver::SetTime(CFaultState *pFault, UINT uTime)
{
	pFault->m_uTime = GetTickCount() + ToTicks(uTime);
	}

BOOL CCatLinkDriver::TimeOut(CFaultState *pFault)
{
	return int(GetTickCount() - pFault->m_uTime) >= 0;
	}

BOOL CCatLinkDriver::QueuePoll(BYTE bDrop, BYTE bCode)
{
	UINT uNext = (m_uTail + 1) % elements(m_pPoll);

	if( uNext != m_uHead ) {

		m_pPoll[m_uTail] = (CDataItem *) DWORD(MAKEWORD(bDrop, bCode));

		m_uTail          = uNext;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::HasLowLimit(BYTE bDrop)
{
	switch( bDrop ) {

		case 0x6B:
		case 0x6F:
			return TRUE;
		}

	return FALSE;
	}

// Item State Machine

void CCatLinkDriver::UpdateItems(void)
{
	for( UINT n = 0; n < elements(m_pData); n++ ) {

		UINT uName = GetName(n);

		if( IsValidName(uName) ) {

			CDataItem *pItem = m_pData[n];

			if( pItem ) {

				if( pItem->m_wState ) {

					if( !pItem->m_fDone ) {

						UpdateItem(uName, pItem);
						}
					}

				pItem->m_fDone = FALSE;
				}
			}
		}

	CheckPause(TRUE);
	}

void CCatLinkDriver::UpdateItem(UINT uName, CDataItem *pItem)
{
	switch( pItem->m_wState ) {

		case itemMissing:

			if( pItem->m_bTx ) {

				if( m_fTest || m_bECM || m_bEMCP ) {

					if( IsValidTarget(uName, pItem->m_bTx) ) {

						if( !IsKeyName(uName) && !IsWritable(uName) ) {

							OnPresentData(pItem);

							break;
							}
						}
					}
				
				pItem->m_bTx = 0;
				}

			if( TimeOut(pItem) ) {

				if( !m_fTest && !m_bECM && !m_bEMCP ) {

					if( !IsKeyName(uName) ) {

						SetTime(pItem, timeMissing);

						break;
						}
					}

				if( !pItem->m_fSkip ) {

					if( !m_uActive ) {

						SetTime(pItem, timeRetry);
						}
					else {
						if( !FindInitTarget(pItem) ) {

							SetTime(pItem, timeMissing);

							break;
							}

						if( QueuePoll(pItem) ) {

							pItem->m_wState = itemFind;
							}
						else {
							SetTime(pItem, timeRetry);

							pItem->m_wState = itemFindBusy;
							}
						}

					break;
					}

				SetTime(pItem, timeMissing);

				break;
				}

			break;

		case itemFindBusy:

			if( TimeOut(pItem) ) {

				if( QueuePoll(pItem) ) {

					pItem->m_wState = itemFind;
					}
				else
					SetTime(pItem, timeRetry);

				break;
				}
			break;

		case itemDefer:

			if( TimeOut(pItem) ) {

				if( QueuePoll(pItem) ) {

					pItem->m_wState = itemFind;
					}
				else {
					SetTime(pItem, timeRetry);

					pItem->m_wState = itemFindBusy;
					}

				break;
				}
			break;

		case itemFind:

			if( pItem->m_bTx ) {

				if( OnPolledData(pItem) ) {

					break;
					}
				}

			if( !m_fTest && !m_bECM && !m_bEMCP ) {

				if( !IsKeyName(uName) ) {

					pItem->m_wState = itemMissing;

					SetTime(pItem, timeMissing);

					break;
					}
				}

			if( TimeOut(pItem) ) {

				SubPoll(pItem);

				if( !pItem->m_fSkip ) {

					FindNextTarget(pItem);

					UINT uDone = 2 * m_uActive;

					if( IsKeyName(uName) || pItem->m_uPoll <= uDone ) {

						if( QueuePoll(pItem) ) {

							pItem->m_wState = itemFind;
							}
						else {
							SetTime(pItem, timeRetry);

							pItem->m_wState = itemFindBusy;
							}
						}
					else {
						UINT t1 = timeDefer * (pItem->m_uPoll - uDone);

						UINT t2 = 1000      * (uName % 20);

						SetTime(pItem, t1 + t2);

						pItem->m_wState = itemDefer;
						}

					break;
					}

				pItem->m_wState = itemIdle;

				break;
				}

			break;

		case itemFound:

			if( pItem->m_bTx ) {

				if( FALSE ) {

					// NOTE -- Enable this to allow us to switch back
					// to listen mode if we see an item while we're
					// waiting to poll it. This isn't too stable as the
					// data item might not be around for long.

					OnPresentData(pItem);

					break;
					}

				pItem->m_bTx = 0;
				}

			if( !m_fTest && !m_bECM && !m_bEMCP ) {

				if( !IsKeyName(uName) ) {

					pItem->m_wState = itemMissing;

					SetTime(pItem, timeMissing);

					break;
					}
				}

			if( TimeOut(pItem) ) {

				if( !pItem->m_fSkip ) {

					if( !IsValidTarget(uName, pItem->m_bSrc) ) {

						if( uName == pidRPM ) {

							m_bECM  = 0;

							m_bEMCP = 0;
							}

						pItem->m_wState = itemMissing;

						pItem->m_fSend  = FALSE;
		
						SetTime(pItem, timeImmediate);

						break;
						}

					if( QueuePoll(pItem) ) {

						pItem->m_wTries = 4;

						pItem->m_wState = itemPolled;
						}
					else
						SetTime(pItem, timeRetry);

					break;
					}

				pItem->m_wState = itemIdle;

				break;
				}

			break;

		case itemPolled:

			if( pItem->m_bTx ) {

				if( OnPolledData(pItem) ) {

					break;
					}
				}

			if( TimeOut(pItem) ) {

				SubPoll(pItem);

				if( --pItem->m_wTries ) {

					if( QueuePoll(pItem) ) {
				
						pItem->m_wState = itemPolled;
						}
					else {
						SetTime(pItem, timeRetry);

						pItem->m_wState = itemPollBusy;
						}
					}
				else {
					if( uName == pidRPM ) {

						m_bECM  = 0;

						m_bEMCP = 0;
						}

					pItem->m_wState = itemMissing;

					pItem->m_fSend  = FALSE;

					SetTime(pItem, timeImmediate);
					}

				break;
				}

			break;

		case itemPollBusy:

			if( TimeOut(pItem) ) {

				if( QueuePoll(pItem) ) {

					pItem->m_wState = itemPolled;
					}
				else
					SetTime(pItem, timeRetry);

				break;
				}

			break;

		case itemPresent:

			if( pItem->m_bTx ) {

				if( IsValidTarget(uName, pItem->m_bTx) ) {

					OnPresentData(pItem);
					}
				else {
					pItem->m_wState = itemMissing;

					SetTime(pItem, timeMissing);
					}

				break;
				}

			if( TimeOut(pItem) ) {

				pItem->m_wState = itemMissing;

				SetTime(pItem, timeImmediate);

				break;
				}

			break;
		}
	}

BOOL CCatLinkDriver::OnPresentData(CDataItem *pItem)
{
	if( !IsSelf(pItem->m_bRx) ) {

		if( pItem->m_uName == pidSerial ) {

			pItem->m_wState = itemIdle;

			return TRUE;
			}

		pItem->m_bSrc   = pItem->m_bTx;

		pItem->m_wState = itemPresent;

		SetTime(pItem, timePresent);

		return TRUE;
		}

	pItem->m_bTx = 0;

	return FALSE;
	}

BOOL CCatLinkDriver::OnPolledData(CDataItem *pItem)
{
	if( IsSelf(pItem->m_bRx) ) {

		if( pItem->m_bTx == pItem->m_bSrc ) {

			SubPoll(pItem);

			if( pItem->m_uName == pidSerial ) {

				pItem->m_wState = itemIdle;

				return TRUE;
				}

			pItem->m_wState = itemFound;

			pItem->m_uPoll  = 0;

			SetTime(pItem, timePoll);

			return TRUE;
			}
		}

	pItem->m_bTx = 0;

	return FALSE;
	}

void CCatLinkDriver::SetTime(CDataItem *pItem, UINT uTime)
{
	pItem->m_bTx   = 0;

	pItem->m_bRx   = 0;

	pItem->m_uTime = GetTickCount() + ToTicks(uTime);
	}

BOOL CCatLinkDriver::TimeOut(CDataItem *pItem)
{
	return int(GetTickCount() - pItem->m_uTime) >= 0;
	}

BOOL CCatLinkDriver::FindInitTarget(CDataItem *pItem)
{
	for( UINT n = 0; n < 256; n++ ) {

		if( IsValidTarget(pItem->m_uName, pItem->m_bSrc) ) {

			if( pItem->m_uPoll < 30 ) {

				pItem->m_uPoll++;
				}

			return TRUE;
			}

		StepTarget(pItem->m_bSrc);
		}

	return FALSE;
	}

void CCatLinkDriver::FindNextTarget(CDataItem *pItem)
{
	for(;;) {

		StepTarget(pItem->m_bSrc);

		if( IsValidTarget(pItem->m_uName, pItem->m_bSrc) ) {

			if( pItem->m_uPoll < 30 ) {

				pItem->m_uPoll++;
				}

			break;
			}
		}
	}

BOOL CCatLinkDriver::IsValidTarget(BYTE bDrop)
{
	return IsValidTarget(pidVoltage, bDrop);
	}

BOOL CCatLinkDriver::IsValidTarget(UINT uName, BYTE bDrop)
{
	if( IsValidECM(bDrop) ) {

		if( m_bECM ) {

			if( bDrop == m_bECM ) {

				return TRUE;
				}

			return FALSE;
			}

		if( uName == pidRPM ) {

			return !!m_bSeen[bDrop];
			}

		return FALSE;
		}

	if( IsValidEMCP(bDrop) ) {

		if( m_bEMCP ) {

			if( bDrop == m_bEMCP ) {

				return TRUE;
				}

			return FALSE;
			}

		if( uName == pidVoltage ) {

			return !!m_bSeen[bDrop];
			}

		return FALSE;
		}

	return !!m_bSeen[bDrop];
	}

BOOL CCatLinkDriver::IsValidECM(BYTE bDrop)
{
	if( bDrop >= 0x0E && bDrop <= 0x13 ) {

		return TRUE;
		}

	if( bDrop >= 0x21 && bDrop <= 0x29 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::IsValidEMCP(BYTE bDrop)
{
	if( bDrop >= 0x58 && bDrop <= 0x5F ) {

		return TRUE;
		}

	return FALSE;
	}

void CCatLinkDriver::StepTarget(BYTE &bDrop)
{
	if( bDrop >= 0x58 && bDrop <= 0x5E ) {

		bDrop++;

		return;
		}

	if( bDrop == 0x5F ) {

		bDrop = 0x57;

		return;
		}

	if( bDrop == 0x60 ) {

		bDrop = 0x58;

		return;
		}

	if( bDrop == 0x2A ) {

		bDrop = 0x24;

		return;
		}

	if( bDrop == 0x24 ) {

		bDrop = 0x29;

		return;
		}

	if( bDrop == 0x25 ) {

		bDrop = 0x23;

		return;
		}

	bDrop--;
	}

void CCatLinkDriver::AddRange(BYTE bFrom, BYTE bTo)
{
	for( UINT b = bFrom; b <= bTo; b++ ) {

		SeeDrop(b, TRUE);
		}
	}

void CCatLinkDriver::AddPoll(CDataItem *pItem)
{
	m_uPend[pItem->m_bSrc]++;
	}

void CCatLinkDriver::SubPoll(CDataItem *pItem)
{
	m_uPend[pItem->m_bSrc]--;
	}

BOOL CCatLinkDriver::QueuePoll(CDataItem *pItem)
{
	UINT uActive = m_uPend[pItem->m_bSrc];

	BOOL fLower  = HasLowLimit(pItem->m_bSrc);

	UINT uLimit  = fLower ? pollLimitLow : pollLimitStd;

	if( uActive < uLimit ) {

		UINT uNext = (m_uTail + 1) % elements(m_pPoll);

		if( uNext != m_uHead ) {

			AddPoll(pItem);

			m_pPoll[m_uTail] = pItem;

			m_uTail          = uNext;

			return TRUE;
			}
		}

	return FALSE;
	}

// Handler Calls

BOOL CCatLinkDriver::SeeDrop(BYTE bDrop, BOOL fForce)
{
	if( bDrop >= 0x0E ) {

		if( m_bSeen[bDrop] == midIdle || m_bSeen[bDrop] == midForce ) {

			if( !fForce) {

				if( !m_fManual || m_bSeen[bDrop] == midForce ) {

					if( IsValidECM(bDrop) ) {

						if( !m_bECM || bDrop > m_bECM || bDrop == 0x24 ) {

							m_bECM = bDrop;
							}
						}

					if( IsValidEMCP(bDrop) ) {

						if( !m_bEMCP || bDrop < m_bEMCP ) {

							m_bEMCP = bDrop;
							}
						}

					if( bDrop == 0x55 ) {

						m_fTest = TRUE;
						}
					}
				}
			}

		if( m_bSeen[bDrop] == midIdle ) {

			if( !fForce ) {

				if( !m_fManual ) {

					m_bSeen[bDrop] = midSeen;

					m_uActive      = m_uActive + 1;

					return TRUE;
					}

				return FALSE;
				}
			else {
				m_bSeen[bDrop] = midForce;

				m_uActive      = m_uActive + 1;

				return TRUE;
				}
			}


		if( !fForce ) {

			m_bSeen[bDrop] = midSeen;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::SetData(BYTE bSrc, BYTE bDest, UINT uName, UINT uData)
{
	if( IsValidName(uName) ) {

		CDataItem *pItem = GetItem(uName);

		if( pItem ) {

			if( !pItem->m_bTx ) {

				if( SignExtendByte(uName) ) {

					typedef signed char SCHAR;

					uData = UINT(LONG(SHORT(SCHAR(uData))));
					}

				if( SignExtendWord(uName) ) {

					uData = UINT(LONG(SHORT(uData)));
					}

				pItem->m_Data = uData;

				pItem->m_bTx  = bSrc;

				pItem->m_bRx  = bDest;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CCatLinkDriver::SetData(PCTXT pSerial)
{
	strcpy(m_sSerial, pSerial);

	return TRUE;
	}

UINT CCatLinkDriver::GetSend(PBYTE pData)
{
	while( m_uHead != m_uTail ) {

		CDataItem *pItem = m_pPoll[m_uHead];

		UINT       uSize = 0;

		if( HIWORD(pItem) ) {

			UINT uName = pItem->m_uName;

			BYTE bDrop = pItem->m_bSrc;

			pData[0] = m_bSelf;

			pData[1] = bDrop;

			// NOTE -- Double check on writability!

			if( pItem->m_fSend && IsWritable(uName) ) {

				UINT uData    = pItem->m_Write;

				UINT uNameLen = GetNameLen(uName);

				UINT uDataLen = GetDataLen(uName);

				UINT uNamePos = 4;

				UINT uDataPos = 4 + uNameLen;

				pData[2] = 0xB2;

				pData[3] = uNameLen + uDataLen;

				SetNameBytes(pData + uNamePos, uName, uNameLen);

				SetDataBytes(pData + uDataPos, uData, uDataLen);

				uSize = 4 + pData[3];
				}
			else {
				if( !uSize && !(uName & 0xFFFFFF00) ) {

					pData[2] = 0x20;

					SetNameBytes(pData + 3, uName, 1);

					uSize = 4;
					}

				if( !uSize && !(uName & 0xFFFF0000) ) {

					pData[2] = 0x60;

					SetNameBytes(pData + 3, uName, 2);

					uSize = 5;
					}

				if( !uSize && !(uName & 0xFF000000) ) {

					pData[2] = 0xCF;

					pData[3] = 0x00;

					SetNameBytes(pData + 4, uName, 3);

					uSize = 7;
					}
				}

			SetTime(pItem, timeReply);
			}
		else {
			BYTE bDest = LOBYTE(DWORD(pItem));

			if( HIBYTE(pItem) ) {

				pData[0] = m_bSelf;

				pData[1] = bDest;

				pData[2] = 0x60;

				pData[3] = 0xFA;

				pData[4] = HIBYTE(pItem);

				uSize    = 5;
				}
			else {
				CFaultState *pFault = m_pFaultState[bDest];

				CFaultData  *pWork  = m_FaultWork + pFault->m_uScan;

				if( pWork->m_bSrc == bDest && pWork->m_wCount == 0 ) {

					pData[0] = m_bSelf;

					pData[1] = bDest;

					if( pWork->m_bFlags & 0x80 ) {

						pData[2] = 0xFA;

						pData[3] = 0x11;

						pData[4] = 0x04;

						pData[5] = HIBYTE(pWork->m_wCode);

						pData[6] = LOBYTE(pWork->m_wCode);

						pData[7] = (pWork->m_bRaw & 0xFF);

						pData[8] = 0x02;

						uSize    = 9;
						}
					else {
						pData[2] = 0xFA;

						pData[3] = 0x0F;

						pData[4] = 0x04;

						pData[5] = HIBYTE(pWork->m_wCode);

						pData[6] = LOBYTE(pWork->m_wCode);

						pData[7] = (pWork->m_bRaw & 0xF0);

						pData[8] = 0x02;

						uSize    = 9;
						}

					SetTime(pFault, timeDetails);
					}
				else {
					GetNext();

					continue;
					}
				}
			}

		if( uSize ) {

			BYTE bSum = 0;

			for( UINT n = 0; n < uSize; n++ ) {

				bSum += pData[n];
				}

			pData[n] = BYTE(256 - bSum);

			uSize    = uSize + 1;
			}

		return uSize;
		}

	return 0;
	}

BOOL CCatLinkDriver::GetNext(void)
{
	if( m_uHead != m_uTail ) {

		m_uHead = (m_uHead + 1) % elements(m_pPoll);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::OnFault(BYTE bSrc, PBYTE pData, UINT uCount)
{
	CFaultState *pFault = m_pFaultState[bSrc];

	if( pFault ) {

		if( pFault->m_wState > faultIdle ) {

			for( UINT n = 0; n < slotCount; n++ ) {

				PBYTE pSlot = pFault->m_bRx[n];

				if( !pSlot[0] ) {

					memcpy(pSlot + 1, pData, uCount);

					pSlot[0] = bSrc;

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CCatLinkDriver::WriteOK(BYTE bSrc, UINT uName)
{
	if( IsValidName(uName) ) {

		CDataItem *pItem = GetItem(uName);

		if( pItem ) {

			if( bSrc == pItem->m_bSrc ) {

				pItem->m_fSend = FALSE;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Data Item Access

CCatLinkDriver::CDataItem * & CCatLinkDriver::GetItem(UINT uName)
{
	if( uName >= 0x000000 && uName <= 0x0000FF ) {

		return m_pData[0x0000 + (uName - 0x000000)];
		}

	if( uName >= 0xD00000 && uName <= 0xD00FFF ) {

		return m_pData[0x0100 + (uName - 0xD00000)];
		}
	
	if( uName >= 0xD10000 && uName <= 0xD10FFF ) {

		return m_pData[0x1100 + (uName - 0xD10000)];
		}

	if( uName >= 0x00F000 && uName <= 0x00FFFF ) {

		return m_pData[0x2100 + (uName - 0x00F000)];
		}

	if( uName >= 0xD01000 && uName <= 0xD01FFF ) {

		return m_pData[0x3100 + (uName - 0xD01000)];
		}

	static CDataItem *null = NULL;

	return null;
	}

// Item Name Helpers

BOOL CCatLinkDriver::IsValidName(UINT uName)
{
	switch( uName ) {

		case 0x0001:
		case 0x0002:
		case 0x0003:
		case 0x0007:
		case 0x0008:
		case 0x000C:
		case 0x000D:
		case 0x000E:
		case 0x000F:
		case 0x0011:
		case 0x0015:
		case 0x0016:
		case 0x0017:
		case 0x0018:
		case 0x0040:
		case 0x0041:
		case 0x0042:
		case 0x0044:
		case 0x0045:
		case 0x0046:
		case 0x0047:
		case 0x004B:
		case 0x004D:
		case 0x004E:
		case 0x0053:
		case 0x0054:
		case 0x0055:
		case 0x0058:
		case 0x0059:
		case 0x005A:
		case 0x005B:
		case 0x005C:
		case 0x005D:
		case 0x005E:
		case 0x005F:
		case 0x0080:
		case 0x0082:
		case 0x0083:
		case 0x0084:
		case 0x0086:
		case 0x00C8:
		case 0xF001:
		case 0xF002:
		case 0xF00D:
		case 0xF00E:
		case 0xF013:
		case 0xF014:
		case 0xF016:
		case 0xF018:
		case 0xF01B:
		case 0xF021:
		case 0xF022:
		case 0xF023:
		case 0xF024:
		case 0xF025:
		case 0xF026:
		case 0xF027:
		case 0xF029:
		case 0xF02A:
		case 0xF02B:
		case 0xF02C:
		case 0xF02D:
		case 0xF031:
		case 0xF032:
		case 0xF062:
		case 0xF066:
		case 0xF074:
		case 0xF07C:
		case 0xF08F:
		case 0xF090:
		case 0xF094:
		case 0xF09C:
		case 0xF0A6:
		case 0xF0A8:
		case 0xF0A9:
		case 0xF0AA:
		case 0xF0AC:
		case 0xF0B0:
		case 0xF0B1:
		case 0xF0B2:
		case 0xF0B3:
		case 0xF0B4:
		case 0xF0B5:
		case 0xF0B6:
		case 0xF0BA:
		case 0xF0C1:
		case 0xF0C2:
		case 0xF0E8:
		case 0xF0F1:
		case 0xF0F2:
		case 0xF0FD:
		case 0xF108:
		case 0xF109:
		case 0xF10A:
		case 0xF10B:
		case 0xF10C:
		case 0xF10D:
		case 0xF111:
		case 0xF112:
		case 0xF113:
		case 0xF115:
		case 0xF116:
		case 0xF117:
		case 0xF118:
		case 0xF119:
		case 0xF11B:
		case 0xF11C:
		case 0xF11D:
		case 0xF121:
		case 0xF122:
		case 0xF123:
		case 0xF124:
		case 0xF125:
		case 0xF127:
		case 0xF129:
		case 0xF14F:
		case 0xF153:
		case 0xF154:
		case 0xF189:
		case 0xF191:
		case 0xF192:
		case 0xF1AD:
		case 0xF1C4:
		case 0xF1D0:
		case 0xF1D1:
		case 0xF1D3:
		case 0xF1D4:
		case 0xF1D5:
		case 0xF1D6:
		case 0xF212:
		case 0xF213:
		case 0xF22A:
		case 0xF22B:
		case 0xF22C:
		case 0xF231:
		case 0xF24D:
		case 0xF24F:
		case 0xF257:
		case 0xF287:
		case 0xF28A:
		case 0xF290:
		case 0xF299:
		case 0xF2B3:
		case 0xF2C3:
		case 0xF2C6:
		case 0xF2CB:
		case 0xF2CC:
		case 0xF2CE:
		case 0xF2D4:
		case 0xF2D5:
		case 0xF2D6:
		case 0xF2D7:
		case 0xF2DD:
		case 0xF2DE:
		case 0xF2F6:
		case 0xF2F7:
		case 0xF2FA:
		case 0xF2FC:
		case 0xF2FD:
		case 0xF30B:
		case 0xF30C:
		case 0xF312:
		case 0xF313:
		case 0xF402:
		case 0xF40E:
		case 0xF410:
		case 0xF411:
		case 0xF412:
		case 0xF413:
		case 0xF415:
		case 0xF417:
		case 0xF419:
		case 0xF41C:
		case 0xF41F:
		case 0xF420:
		case 0xF421:
		case 0xF422:
		case 0xF425:
		case 0xF426:
		case 0xF427:
		case 0xF428:
		case 0xF42A:
		case 0xF42C:
		case 0xF42E:
		case 0xF430:
		case 0xF431:
		case 0xF432:
		case 0xF433:
		case 0xF434:
		case 0xF435:
		case 0xF436:
		case 0xF437:
		case 0xF438:
		case 0xF439:
		case 0xF43A:
		case 0xF43B:
		case 0xF43C:
		case 0xF43D:
		case 0xF43E:
		case 0xF43F:
		case 0xF440:
		case 0xF441:
		case 0xF442:
		case 0xF443:
		case 0xF444:
		case 0xF445:
		case 0xF446:
		case 0xF447:
		case 0xF448:
		case 0xF449:
		case 0xF44A:
		case 0xF44B:
		case 0xF44C:
		case 0xF44D:
		case 0xF44E:
		case 0xF44F:
		case 0xF450:
		case 0xF451:
		case 0xF452:
		case 0xF453:
		case 0xF455:
		case 0xF456:
		case 0xF457:
		case 0xF45B:
		case 0xF45C:
		case 0xF45D:
		case 0xF45E:
		case 0xF45F:
		case 0xF460:
		case 0xF461:
		case 0xF462:
		case 0xF463:
		case 0xF464:
		case 0xF465:
		case 0xF466:
		case 0xF467:
		case 0xF468:
		case 0xF469:
		case 0xF46A:
		case 0xF46B:
		case 0xF46C:
		case 0xF46D:
		case 0xF46F:
		case 0xF477:
		case 0xF478:
		case 0xF48D:
		case 0xF48F:
		case 0xF4A0:
		case 0xF4A2:
		case 0xF4A4:
		case 0xF4A5:
		case 0xF4B2:
		case 0xF4B3:
		case 0xF4B4:
		case 0xF4B5:
		case 0xF4B6:
		case 0xF4C3:
		case 0xF4C4:
		case 0xF4C7:
		case 0xF4C8:
		case 0xF4C9:
		case 0xF4CA:
		case 0xF4CB:
		case 0xF4CF:
		case 0xF4D0:
		case 0xF4D1:
		case 0xF4D2:
		case 0xF4D3:
		case 0xF4D4:
		case 0xF4D5:
		case 0xF4E1:
		case 0xF4EA:
		case 0xF4F7:
		case 0xF4F8:
		case 0xF4FE:
		case 0xF4FF:
		case 0xF508:
		case 0xF509:
		case 0xF50A:
		case 0xF50B:
		case 0xF50C:
		case 0xF50D:
		case 0xF50E:
		case 0xF50F:
		case 0xF510:
		case 0xF511:
		case 0xF512:
		case 0xF513:
		case 0xF514:
		case 0xF515:
		case 0xF516:
		case 0xF517:
		case 0xF518:
		case 0xF519:
		case 0xF51A:
		case 0xF51B:
		case 0xF51C:
		case 0xF51D:
		case 0xF51E:
		case 0xF51F:
		case 0xF520:
		case 0xF524:
		case 0xF525:
		case 0xF527:
		case 0xF53E:
		case 0xF540:
		case 0xF541:
		case 0xF542:
		case 0xF54F:
		case 0xF557:
		case 0xF55A:
		case 0xF55B:
		case 0xF55C:
		case 0xF55D:
		case 0xF55E:
		case 0xF55F:
		case 0xF560:
		case 0xF561:
		case 0xF562:
		case 0xF563:
		case 0xF564:
		case 0xF565:
		case 0xF566:
		case 0xF567:
		case 0xF568:
		case 0xF569:
		case 0xF56A:
		case 0xF56B:
		case 0xF56C:
		case 0xF56D:
		case 0xF56E:
		case 0xF56F:
		case 0xF570:
		case 0xF571:
		case 0xF572:
		case 0xF573:
		case 0xF574:
		case 0xF575:
		case 0xF577:
		case 0xF578:
		case 0xF579:
		case 0xF57B:
		case 0xF57C:
		case 0xF57E:
		case 0xF57F:
		case 0xF58B:
		case 0xF58D:
		case 0xF58E:
		case 0xF593:
		case 0xF594:
		case 0xF595:
		case 0xF596:
		case 0xF597:
		case 0xF598:
		case 0xF599:
		case 0xF59A:
		case 0xF59B:
		case 0xF59C:
		case 0xF59D:
		case 0xF59E:
		case 0xF59F:
		case 0xF5A0:
		case 0xF5A1:
		case 0xF5A2:
		case 0xF5A3:
		case 0xF5A4:
		case 0xF5A5:
		case 0xF5A6:
		case 0xF5A7:
		case 0xF5A8:
		case 0xF5A9:
		case 0xF5AA:
		case 0xF5AB:
		case 0xF5B1:
		case 0xF5B7:
		case 0xF5B8:
		case 0xF5BA:
		case 0xF5BB:
		case 0xF5C4:
		case 0xF5C9:
		case 0xF5D1:
		case 0xF5D6:
		case 0xF5D8:
		case 0xF5D9:
		case 0xF5DA:
		case 0xF5DB:
		case 0xF5E0:
		case 0xF602:
		case 0xF603:
		case 0xF604:
		case 0xF605:
		case 0xF61E:
		case 0xF62B:
		case 0xF636:
		case 0xF701:
		case 0xF702:
		case 0xF703:
		case 0xF704:
		case 0xF705:
		case 0xF706:
		case 0xF707:
		case 0xF708:
		case 0xF709:
		case 0xF70A:
		case 0xF70B:
		case 0xF70C:
		case 0xF70D:
		case 0xF70E:
		case 0xF70F:
		case 0xF710:
		case 0xF711:
		case 0xF7FA:
		case 0xF7FC:
		case 0xF803:
		case 0xF806:
		case 0xF810:
		case 0xF811:
		case 0xF814:
		case 0xF81A:
		case 0xF81C:
		case 0xFC07:
		case 0xFC08:
		case 0xFC09:
		case 0xFC0D:
		case 0xFC0F:
		case 0xFC10:
		case 0xFC11:
		case 0xFC12:
		case 0xFC13:
		case 0xFC14:
		case 0xFC15:
		case 0xFC16:
		case 0xFC17:
		case 0xFC18:
		case 0xFC19:
		case 0xFC1A:
		case 0xFC1B:
		case 0xFC1C:
		case 0xFC1D:
		case 0xFC1E:
		case 0xFC1F:
		case 0xFC21:
		case 0xFC22:
		case 0xFC27:
		case 0xFC28:
		case 0xFC29:
		case 0xFC2D:
		case 0xFC2E:
		case 0xFC32:
		case 0xFC33:
		case 0xFC88:
		case 0xD00020:
		case 0xD00021:
		case 0xD00022:
		case 0xD00023:
		case 0xD00024:
		case 0xD00025:
		case 0xD00026:
		case 0xD00027:
		case 0xD00028:
		case 0xD00029:
		case 0xD0002A:
		case 0xD0002B:
		case 0xD0002C:
		case 0xD0002D:
		case 0xD0002E:
		case 0xD0002F:
		case 0xD00030:
		case 0xD00031:
		case 0xD00032:
		case 0xD00033:
		case 0xD00040:
		case 0xD00041:
		case 0xD00042:
		case 0xD00043:
		case 0xD00044:
		case 0xD00045:
		case 0xD00046:
		case 0xD00047:
		case 0xD00048:
		case 0xD00049:
		case 0xD0004A:
		case 0xD0004B:
		case 0xD0004C:
		case 0xD0004D:
		case 0xD0004E:
		case 0xD0004F:
		case 0xD00050:
		case 0xD00051:
		case 0xD00052:
		case 0xD00053:
		case 0xD000EB:
		case 0xD000EC:
		case 0xD000ED:
		case 0xD000EE:
		case 0xD000EF:
		case 0xD000F0:
		case 0xD000F1:
		case 0xD000F2:
		case 0xD000F3:
		case 0xD000F4:
		case 0xD000F5:
		case 0xD000F6:
		case 0xD000F7:
		case 0xD000F8:
		case 0xD000F9:
		case 0xD000FA:
		case 0xD000FB:
		case 0xD000FC:
		case 0xD000FD:
		case 0xD000FE:
		case 0xD00109:
		case 0xD0010A:
		case 0xD0012F:
		case 0xD00130:
		case 0xD00131:
		case 0xD00148:
		case 0xD00149:
		case 0xD0014A:
		case 0xD0014B:
		case 0xD0014C:
		case 0xD0014D:
		case 0xD0014E:
		case 0xD0014F:
		case 0xD00150:
		case 0xD00151:
		case 0xD00188:
		case 0xD00189:
		case 0xD001A1:
		case 0xD001C7:
		case 0xD00259:
		case 0xD00260:
		case 0xD00261:
		case 0xD00262:
		case 0xD0027D:
		case 0xD0027E:
		case 0xD0027F:
		case 0xD00281:
		case 0xD002AE:
		case 0xD0036F:
		case 0xD00370:
		case 0xD00371:
		case 0xD00375:
		case 0xD00377:
		case 0xD00378:
		case 0xD00379:
		case 0xD003A5:
		case 0xD003A6:
		case 0xD003E0:
		case 0xD003E1:
		case 0xD003E2:
		case 0xD003E3:
		case 0xD003E4:
		case 0xD003E5:
		case 0xD003E6:
		case 0xD003E7:
		case 0xD003E8:
		case 0xD003E9:
		case 0xD003EA:
		case 0xD003EB:
		case 0xD003EC:
		case 0xD003ED:
		case 0xD003EE:
		case 0xD003EF:
		case 0xD003F0:
		case 0xD003F1:
		case 0xD003F2:
		case 0xD003F3:
		case 0xD003F4:
		case 0xD003F5:
		case 0xD003F6:
		case 0xD003F7:
		case 0xD003F8:
		case 0xD003F9:
		case 0xD003FA:
		case 0xD003FB:
		case 0xD003FC:
		case 0xD003FD:
		case 0xD003FE:
		case 0xD003FF:
		case 0xD00400:
		case 0xD00401:
		case 0xD00402:
		case 0xD00403:
		case 0xD00404:
		case 0xD00405:
		case 0xD00406:
		case 0xD00407:
		case 0xD00418:
		case 0xD00419:
		case 0xD0041A:
		case 0xD00453:
		case 0xD00478:
		case 0xD00479:
		case 0xD0047A:
		case 0xD0047B:
		case 0xD0047C:
		case 0xD0047D:
		case 0xD0047E:
		case 0xD0047F:
		case 0xD00480:
		case 0xD00481:
		case 0xD00482:
		case 0xD00483:
		case 0xD004B5:
		case 0xD004B6:
		case 0xD004B7:
		case 0xD004DB:
		case 0xD004DC:
		case 0xD0050B:
		case 0xD0050C:
		case 0xD0050D:
		case 0xD0050E:
		case 0xD0050F:
		case 0xD00510:
		case 0xD00511:
		case 0xD00512:
		case 0xD00513:
		case 0xD00514:
		case 0xD00515:
		case 0xD00516:
		case 0xD00517:
		case 0xD00518:
		case 0xD00519:
		case 0xD0051A:
		case 0xD005B1:
		case 0xD005B2:
		case 0xD005B3:
		case 0xD005F5:
		case 0xD006BB:
		case 0xD0098D:
		case 0xD0099E:
		case 0xD0099F:
		case 0xD009A0:
		case 0xD009A1:
		case 0xD009A2:
		case 0xD009A3:
		case 0xD00AF4:
		case 0xD00AF5:
		case 0xD00AF6:
		case 0xD00B01:
		case 0xD00B18:
		case 0xD00B3D:
		case 0xD00B3E:
		case 0xD00BB3:
		case 0xD00BB7:
		case 0xD00C81:
		case 0xD00D14:
		case 0xD00D15:
		case 0xD00D16:
		case 0xD00D17:
		case 0xD00D18:
		case 0xD00D19:
		case 0xD00D3B:
		case 0xD00D5A:
		case 0xD00D61:
		case 0xD00E9D:
		case 0xD00EEC:
		case 0xD00F5D:
		case 0xD00F5E:
		case 0xD00F5F:
		case 0xD00F60:
		case 0xD00F75:
		case 0xD00FCA:
		case 0xD00FCB:
		case 0xD01075:
		case 0xD01076:
		case 0xD01078:
		case 0xD01079:
		case 0xD01092:
		case 0xD0109E:
		case 0xD0109F:
		case 0xD010F0:
		case 0xD010F1:
		case 0xD010F2:
		case 0xD010F3:
		case 0xD01124:
		case 0xD01125:
		case 0xD0112C:
		case 0xD0112D:
		case 0xD0113A:
		case 0xD0113B:
		case 0xD0113C:
		case 0xD0113D:
		case 0xD0113E:
		case 0xD0113F:
		case 0xD01140:
		case 0xD01141:
		case 0xD0114A:
		case 0xD0114B:
		case 0xD0114C:
		case 0xD0114D:
		case 0xD01152:
		case 0xD01154:
		case 0xD01153:
		case 0xD01155:
		case 0xD0115A:
		case 0xD0115C:
		case 0xD0115B:
		case 0xD0115D:
		case 0xD0115E:
		case 0xD01160:
		case 0xD0115F:
		case 0xD01161:
		case 0xD01164:
		case 0xD01165:
		case 0xD01192:
		case 0xD01209:
		case 0xD0120A:
		case 0xD0120B:
		case 0xD0120C:
		case 0xD0120D:
		case 0xD0120E:
		case 0xD0120F:
		case 0xD01255:
		case 0xD01257:
		case 0xD01258:
		case 0xD016E9:
		case 0xD016EA:
		case 0xD016EB:
		case 0xD016EC:
		case 0xD016ED:
		case 0xD016EE:
		case 0xD016EF:
		case 0xD016F0:
		case 0xD016F1:
		case 0xD016F2:
		case 0xD016F3:
		case 0xD016F4:
		case 0xD016F5:
		case 0xD016F6:
		case 0xD016F7:
		case 0xD10018:
		case 0xD1004A:
		case 0xD10050:
		case 0xD10051:
		case 0xD1005D:
		case 0xD10060:
		case 0xD10066:
		case 0xD1009A:
		case 0xD100A0:
		case 0xD100AA:
		case 0xD100AB:
		case 0xD100C5:
		case 0xD100C6:
		case 0xD100C7:
		case 0xD100C8:
		case 0xD100C9:
		case 0xD100CA:
		case 0xD100CD:
		case 0xD100CE:
		case 0xD100CF:
		case 0xD100EC:
		case 0xD10104:
		case 0xD10120:
		case 0xD1013A:
		case 0xD10167:
		case 0xD101EF:
		case 0xD101FE:
		case 0xD10230:
		case 0xD10293:
		case 0xD10986:
		case 0xD10987:
		case 0xD10D21:
		case 0xD10D22:
		case 0xD10D23:
		case 0xD10D24:
		case 0xD10D25:
		case 0xD10D26:
		case 0xD10D27:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::IsKeyName(UINT uName)
{
	switch( uName ) {

		case pidRPM:
		case pidVoltage:
			
			return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::IsWritable(UINT uName)
{
	switch( uName ) {
		
		case 0x000D:	// 7
		case 0x0047:	// 74
		case 0xF0B0:	// 22
		case 0xF0B1:	// 23
		case 0xF0B2:	// 24
		case 0xF0C2:	// 30
		case 0xF11C:	// 47
		case 0xF11D:	// 48
		case 0xF125:	// 53
		case 0xF213:	// 63
		case 0xF44D:	// 91
		case 0xF51A:    // 120
		case 0xF524:	// 310
		case 0xF57C:	// 352
		case 0xF5B1:	// 354
		case 0xFC2E:	// 672
		case 0xFC32:	// 693
		case 0xFC33:	// 694
		case 0xD00281:	// 383
		case 0xD00D61:	// 708
		case 0xD100A0:	// 206
		case 0xD10167:  // 209
		case 0xD00478:  // 391
		case 0xD00479:	// 392
		case 0xD0047A:	// 393
		case 0xD005B3:	// 409
		case 0xD006BB:	// 697
		case 0xD01078:  // 634
		case 0xD01079:	// 703
		case 0xD0109E:	// 704
		case 0xD0109F:	// 706

			return TRUE;
		}

	return FALSE;
	}

BYTE CCatLinkDriver::GetInitialMID(UINT uName)
{
	switch( uName ) {

		case 0x0040:

			return 0x24;

		case 0x0042:
		case 0xF1B3:
		case 0xF1B4:
		case 0xF1D3:
		case 0xF1D4:
		case 0xF1D5:
		case 0xF1D6:
		case 0xF2CB:
		case 0xF2CC:
		case 0xF2D6:
		case 0xF2D7:
		case 0xF442:
		case 0xF443:
		case 0xF444:
		case 0xF445:
		case 0xF446:
		case 0xF447:
		case 0xF448:
		case 0xF449:
		case 0xF44A:
		case 0xF44D:
		case 0xF460:
		case 0xF461:
		case 0xF462:
		case 0xF463:
		case 0xF464:
		case 0xF465:
		case 0xF466:
		case 0xF467:
		case 0xF468:
		case 0xF469:
		case 0xF46A:
		case 0xF46B:
		case 0xF46C:
		case 0xF4C3:
		case 0xF4C4:
		case 0xF4C7:
		case 0xF4C8:
		case 0xF4C9:
		case 0xF4CA:
		case 0xF4CB:
		case 0xF4CF:
		case 0xF4D0:
		case 0xF4D1:
		case 0xF4D2:
		case 0xF557:
		case 0xFC0D:
		case 0xFC0F:
		case 0xFC10:
		case 0xFC11:
		case 0xFC12:
		case 0xFC13:
		case 0xFC14:
		case 0xFC15:
		case 0xFC16:
		case 0xFC17:
		case 0xFC18:
		case 0xFC19:
		case 0xFC1A:
		case 0xFC1B:
		case 0xFC1C:
		case 0xFC1D:
		case 0xFC1E:
		case 0xFC1F:
			
			return 0x58;
		}
	
	return 0x24;
	}

BOOL CCatLinkDriver::SignExtendByte(UINT uName)
{
	switch( uName ) {

		case 0x000D:
		case 0xF108:
		case 0xF109:
		case 0xF10A:
		case 0xF10B:
		case 0xF10C:
		case 0xF10D:
		case 0xF11C:
		case 0xF11D:
		case 0xF1D0:


			return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::SignExtendWord(UINT uName)
{
	switch( uName ) {

		case 0x000C:
		case 0x0044:
		case 0x004D:
		case 0xF023:
		case 0xF024:
		case 0xF02B:
		case 0xF02D:
		case 0xF032:
		case 0xF2FD:
		case 0xF420:
		case 0xF421:
		case 0xF425:
		case 0xF426:
		case 0xF427:
		case 0xF428:
		case 0xF430:
		case 0xF431:
		case 0xF432:
		case 0xF433:
		case 0xF434:
		case 0xF435:
		case 0xF436:
		case 0xF437:
		case 0xF438:
		case 0xF439:
		case 0xF43A:
		case 0xF43B:
		case 0xF43C:
		case 0xF43D:
		case 0xF43E:
		case 0xF43F:
		case 0xF440:
		case 0xF441:
		case 0xF451:
		case 0xF452:
		case 0xF456:
		case 0xF45C:
		case 0xF46F:
		case 0xF4A0:
		case 0xF4A2:
		case 0xF4C7:
		case 0xF4C8:
		case 0xF4C9:
		case 0xF4CA:
		case 0xF4CB:
		case 0xF4E1:
		case 0xF509:
		case 0xF511:
		case 0xF51D:
		case 0xF527:
		case 0xF53E:
		case 0xF542:
		case 0xF54F:
		case 0xF557:
		case 0xF55C:
		case 0xF55D:
		case 0xF55E:
		case 0xF55F:
		case 0xF560:
		case 0xF561:
		case 0xF562:
		case 0xF563:
		case 0xF564:
		case 0xF565:
		case 0xF566:
		case 0xF567:
		case 0xF568:
		case 0xF569:
		case 0xF56A:
		case 0xF56B:
		case 0xF56C:
		case 0xF56D:
		case 0xF56E:
		case 0xF56F:
		case 0xF570:
		case 0xF571:
		case 0xF572:
		case 0xF573:
		case 0xF574:
		case 0xF575:
		case 0xF58B:
		case 0xF593:
		case 0xF594:
		case 0xF595:
		case 0xF596:
		case 0xF597:
		case 0xF598:
		case 0xF599:
		case 0xF59A:
		case 0xF59B:
		case 0xF5BA:
		case 0xF5BB:
		case 0xF5C9:
		case 0xF5D9:
		case 0xF5DB:
		case 0xD00040:
		case 0xD00041:
		case 0xD00042:
		case 0xD00043:
		case 0xD00044:
		case 0xD00045:
		case 0xD00046:
		case 0xD00047:
		case 0xD00048:
		case 0xD00049:
		case 0xD0004A:
		case 0xD0004B:
		case 0xD0004C:
		case 0xD0004D:
		case 0xD0004E:
		case 0xD0004F:
		case 0xD00050:
		case 0xD00051:
		case 0xD00052:
		case 0xD00053:
		case 0xD001A1:
		case 0xD00377:
		case 0xD00378:
		case 0xD003F4:
		case 0xD003F5:
		case 0xD003F6:
		case 0xD003F7:
		case 0xD003F8:
		case 0xD003F9:
		case 0xD003FA:
		case 0xD003FB:
		case 0xD003FC:
		case 0xD003FD:
		case 0xD003FE:
		case 0xD003FF:
		case 0xD00400:
		case 0xD00401:
		case 0xD00402:
		case 0xD00403:
		case 0xD00404:
		case 0xD00405:
		case 0xD00406:
		case 0xD00407: 
		case 0xD00418:
		case 0xD00419:
		case 0xD0041A:
		case 0xD00453:
		case 0xD00478:
		case 0xD00479:
		case 0xD0047A:
		case 0xD0047B:
		case 0xD0047C:
		case 0xD0047D:
		case 0xD0047E:
		case 0xD0047F:
		case 0xD00480:
		case 0xD00481:
		case 0xD00482:
		case 0xD00483:
		case 0xD004B5:
		case 0xD005B3:
		case 0xD0099F:
		case 0xD009A0:
		case 0xD009A2:
		case 0xD009A3:
		case 0xD00B3D:
		case 0xD00B3E:
		case 0xD0099E:
		case 0xD009A1:
		case 0xD01075:
		case 0xD01076:
		
			return TRUE;
		}

	return FALSE;
	}

UINT CCatLinkDriver::GetName(UINT uIndex)
{
	if( uIndex >= 0x0000 && uIndex <= 0x00FF ) {

		return uIndex - 0x0000 + 0x000000;
		}

	if( uIndex >= 0x0100 && uIndex <= 0x10FF ) {

		return uIndex - 0x0100 + 0xD00000;
		}

	if( uIndex >= 0x1100 && uIndex <= 0x20FF ) {

		return uIndex - 0x1100 + 0xD10000;
		}

	if( uIndex >= 0x2100 && uIndex <= 0x30FF ) {

		return uIndex - 0x2100 + 0x00F000;
		}

	if( uIndex >= 0x3100 && uIndex <= 0x40FF ) {

		return uIndex - 0x3100 + 0xD01000;
		}

	return 0;
	}

// Implementation

void CCatLinkDriver::CheckPause(BOOL fSleep)
{
	UINT t = GetTickCount();

	if( t - m_uLast1 >= ToTicks(200) ) {

		UpdateFaults();

		m_uLast1 = t;

		m_uLast2 = t;

		Sleep(10);

		return;
		}

	if( t - m_uLast2 >= ToTicks(50) ) {

		m_uLast2 = t;

		Sleep(10);

		return;
		}

	if( fSleep ) {

		Sleep(5);
		}
	}

BOOL CCatLinkDriver::FindNextScanSet(void)
{
	while( ++m_uManual < elements(m_Scan) ) {

		if( m_Scan[m_uManual].m_fEnable ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Fault Sorting

int CCatLinkDriver::SortFunc(PCVOID p1, PCVOID p2)
{
	CFaultData *f1 = (CFaultData *) p1;

	CFaultData *f2 = (CFaultData *) p2;

	BYTE        a1 = (f1->m_bFlags & 0x01);
	
	BYTE        a2 = (f2->m_bFlags & 0x01);

	////////

	if( a1 < a2 ) return +1;

	if( a1 > a2 ) return -1;

	////////

	if( f1->m_dwLast < f2->m_dwLast ) return +1;
	
	if( f1->m_dwLast > f2->m_dwLast ) return -1;

	////////

	if( f1->m_bSrc < f2->m_bSrc ) return -1;

	if( f1->m_bSrc > f2->m_bSrc ) return +1;

	////////

	if( f1->m_wCode < f2->m_wCode ) return -1;

	if( f1->m_wCode > f2->m_wCode ) return +1;

	////////

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Custom Port Handler
//

// Constructor

CCatLinkHandler::CCatLinkHandler(IHelper *pHelper, CCatLinkDriver *pDriver, BYTE bSelf)
{
	StdSetRef();

	m_pHelper = pHelper;

	m_pDriver = pDriver;

	m_bSelf   = bSelf;

	m_fRun    = FALSE;
	}

// Destructor

CCatLinkHandler::~CCatLinkHandler(void)
{
	}

// Operations

void CCatLinkHandler::Start(void)
{
	m_fSync   = FALSE;

	m_fSelf   = FALSE;

	m_uRxPtr  = 0;

	m_uTxPtr  = 0;

	m_uTxSize = 0;

	m_fRun    = TRUE;

	StartEdgeTimer(TRUE);
	}

void CCatLinkHandler::Stop(void)
{
	m_fRun = FALSE;
	}

// IUnknown

HRESULT CCatLinkHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG CCatLinkHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG CCatLinkHandler::Release(void)
{
	StdRelease();
	}

// IPortHandler

void CCatLinkHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
	}

void CCatLinkHandler::OnRxData(BYTE bData)
{
	if( m_fRun && m_fSync ) {

		if( m_uRxPtr < elements(m_bRxData) ) {

			m_bRxData[m_uRxPtr++] = bData;
			}
		}
	}

void CCatLinkHandler::OnRxDone(void)
{
	if( m_fRun ) {

		StartEdgeTimer(FALSE);

		if( m_fSync ) {

			if( m_uRxPtr ) {

				if( m_fSelf ) {

					if( !CheckContention() ) {

						m_pDriver->GetNext();
						}

					m_fSelf = FALSE;
					}

				if( m_uRxPtr == elements(m_bRxData) ) {

					m_fSync = FALSE;

					return;
					}
				else {

					BYTE bSum = 0;

					for( UINT n = 0; n < m_uRxPtr; n++ ) {

						bSum += m_bRxData[n];
						}

					if( !bSum ) {

						BYTE  bSrc   = m_bRxData[0];

						BYTE  bDest  = m_bRxData[1];

						PBYTE pFrame = m_bRxData + 2;

						UINT  uSize  = m_uRxPtr  - 3;

						if( !IsSelf(bSrc) ) {

							if( !IsSelf(bDest) ) {

								m_pDriver->SeeDrop(bDest, FALSE);
								}
							
							m_pDriver->SeeDrop(bSrc, FALSE);

							Process(bSrc, bDest, pFrame, uSize);
							}
						}
					}
				}
			}

		m_fSync  = TRUE;

		m_fSelf  = FALSE;

		m_uRxPtr = 0;
		}
	}

BOOL CCatLinkHandler::OnTxData(BYTE &bData)
{
	if( m_uTxPtr < m_uTxSize ) {

		bData = m_bTxData[m_uTxPtr++];

		return TRUE;
		}

	return FALSE;
	}

void CCatLinkHandler::OnTxDone(void)
{
	}

void CCatLinkHandler::OnOpen(CSerialConfig const &Config)
{
	}

void CCatLinkHandler::OnClose(void)
{
	}

void CCatLinkHandler::OnTimer(void)
{
	if( m_fRun ) {

		if( (m_uTxSize = m_pDriver->GetSend(m_bTxData)) ) {

			ClearContention();

			m_uTxPtr = 1;

			m_fSelf  = TRUE;

			m_pPort->Send(m_bTxData[0]);
			}
		else
			StartEdgeTimer(TRUE);
		}
	}

// Implementation

void CCatLinkHandler::Process(BYTE bSrc, BYTE bDest, PBYTE pData, UINT uCount)
{
	if( pData[0] == 0xFA ) {

		if( bDest == m_bSelf || bDest == 0x08 ) {

			m_pDriver->OnFault(bSrc, pData, uCount);
			}

		return;
		}

	if( pData[0] == HIBYTE(pidSerial) ) {

		if( pData[1] == LOBYTE(pidSerial) ) {

			if( pData[2] > 8 ) {

				m_pDriver->SetData("INVALID");
				}
			else {
				char s[12];

				memset(s, 0, sizeof(s));

				memcpy(s, pData + 3, pData[2]);

				m_pDriver->SetData(s);
				}

			m_pDriver->SetData(bSrc, bDest, pidSerial, 0);

			return;
			}
		}

	if( !IsUnknown(pData) ) {

		if( pData[0] == 0x20 ) {

			if( uCount == 2 ) {

				// Poll with 1-byte PID

				return;
				}

			return;
			}

		if( pData[0] == 0x60 ) {

			if( uCount == 3 ) {

				// Poll with 2-byte PID

				return;
				}

			return;
			}

		if( pData[0] == 0xCF && pData[1] == 0x00 ) {

			if( uCount == 5 ) {

				// Poll with 3-byte PID

				return;
				}

			return;
			}

		for( UINT uPtr = 0; uPtr < uCount; ) {

			BYTE bCode = pData[uPtr];

			if( bCode == 0xBE ) {

				// Write reply sequence

				UINT uName = GetNameLen(pData[uPtr+2]);

				uPtr += 2;
				
				uPtr += uName;

				uPtr += 1;

				m_pDriver->WriteOK(bSrc, uName);
				}
			else {
				UINT uNameLen = GetNameLen(bCode);

				UINT uDataLen = GetDataLen(bCode);

				if( uNameLen && uDataLen ) {

					// Data value extraction

					UINT uName = GetNameBytes(pData + uPtr, uNameLen);

					uPtr += uNameLen;

					UINT uData = GetDataBytes(pData + uPtr, uDataLen);

					uPtr += uDataLen;

					m_pDriver->SetData(bSrc, bDest, uName, uData);

					continue;
					}

				break;
				}
			}
		}
	}

BOOL CCatLinkHandler::IsUnknown(PBYTE pData)
{
	if( pData[0] == 0x82 ) {

		return TRUE;
		}

	if( pData[0] == 0xA2 ) {

		return TRUE;
		}

	if( pData[0] == 0xBC ) {

		return TRUE;
		}

	if( pData[0] == 0xCF ) {

		return TRUE;
		}

	if( pData[0] == 0xE0 ) {

		return TRUE;
		}

	if( pData[0] == 0xCF && pData[1] == 0x80 ) {

		return TRUE;
		}

	if( pData[0] >= 0xF8 && pData[0] <= 0xFB ) {

		return TRUE;
		}

	return FALSE;
	}

void CCatLinkHandler::ClearContention(void)
{
	m_pPort->SetOutput(0x80, 0);
	}

BOOL CCatLinkHandler::CheckContention(void)
{
	return m_pPort->GetInput(0x80);
	}

void CCatLinkHandler::StartEdgeTimer(BOOL fSlow)
{
	m_pPort->SetOutput(0x81, fSlow);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Shared Functions
//

// Data and Name Access

UINT CCatLinkBase::GetNameLen(BYTE bData)
{
	if( bData >= 0x01 && bData <= 0x1F ) {

		// Data with 1-byte PID and 1-byte data field

		return 1;
		}

	if( bData >= 0x21 && bData <= 0x5F ) {

		// Data with 1-byte PID and 2-byte data field

		return 1;
		}

	if( bData >= 0xC8 && bData <= 0xC8 ) {

		// Data with 1-byte PID and 4-byte data field

		return 1;
		}

	if( bData >= 0xF0 && bData <= 0xF3 ) {

		// Data with 2-byte PID and 1-byte data field

		return 2;
		}

	if( bData >= 0xF4 && bData <= 0xF7 ) {

		// Data with 2-byte PID and 2-byte data field

		return 2;
		}

	if( bData >= 0xFC ) {

		// Data with 2-byte PID and 4-byte data field

		return 2;
		}

	if( bData >= 0xD0 && bData <= 0xD1 ) {

		// Data with 3-byte PID and 2-byte data field

		return 3;
		}

	return 0;
	}

UINT CCatLinkBase::GetDataLen(BYTE bData)
{
	if( bData >= 0x01 && bData <= 0x1F ) {

		// Data with 1-byte PID and 1-byte data field

		return 1;
		}

	if( bData >= 0x21 && bData <= 0x5F ) {

		// Data with 1-byte PID and 2-byte data field

		return 2;
		}

	if( bData >= 0xC8 && bData <= 0xC8 ) {

		// Data with 1-byte PID and 4-byte data field

		return 4;
		}

	if( bData >= 0xF0 && bData <= 0xF3 ) {

		// Data with 2-byte PID and 1-byte data field

		return 1;
		}

	if( bData >= 0xF4 && bData <= 0xF7 ) {

		// Data with 2-byte PID and 2-byte data field

		return 2;
		}

	if( bData >= 0xFC ) {

		// Data with 2-byte PID and 4-byte data field

		return 4;
		}

	if( bData >= 0xD0 && bData <= 0xD1 ) {

		// Data with 3-byte PID and 2-byte data field

		return 2;
		}

	return 0;
	}

UINT CCatLinkBase::GetNameLen(UINT uName)
{
	if( uName >= 0x000001 && uName <= 0x00001F ) {

		// Data with 1-byte PID and 1-byte data field

		return 1;
		}

	if( uName >= 0x000021 && uName <= 0x00005F ) {

		// Data with 1-byte PID and 2-byte data field

		return 1;
		}

	if( uName >= 0x0000C8 && uName <= 0x0000C8 ) {

		// Data with 1-byte PID and 4-byte data field

		return 1;
		}

	if( uName >= 0x00F000 && uName <= 0x00F3FF ) {

		// Data with 2-byte PID and 1-byte data field

		return 2;
		}

	if( uName >= 0x00F400 && uName <= 0x00F7FF ) {

		// Data with 2-byte PID and 2-byte data field

		return 2;
		}

	if( uName >= 0x00FC00 && uName <= 0x00FFFF ) {

		// Data with 2-byte PID and 4-byte data field

		return 2;
		}

	if( uName >= 0xD00000 && uName <= 0xD1FFFF ) {

		// Data with 3-byte PID and 2-byte data field

		return 3;
		}

	return 0;
	}

UINT CCatLinkBase::GetDataLen(UINT uName)
{
	if( uName >= 0x000001 && uName <= 0x00001F ) {

		// Data with 1-byte PID and 1-byte data field

		return 1;
		}

	if( uName >= 0x000021 && uName <= 0x00005F ) {

		// Data with 1-byte PID and 2-byte data field

		return 2;
		}

	if( uName >= 0x0000C8 && uName <= 0x0000C8 ) {

		// Data with 1-byte PID and 4-byte data field

		return 4;
		}

	if( uName >= 0x00F000 && uName <= 0x00F3FF ) {

		// Data with 2-byte PID and 1-byte data field

		return 1;
		}

	if( uName >= 0x00F400 && uName <= 0x00F7FF ) {

		// Data with 2-byte PID and 2-byte data field

		return 2;
		}

	if( uName >= 0x00FC00 && uName <= 0x00FFFF ) {

		// Data with 2-byte PID and 4-byte data field

		return 4;
		}

	if( uName >= 0xD00000 && uName <= 0xD1FFFF ) {

		// Data with 3-byte PID and 2-byte data field

		return 2;
		}

	return 0;
	}

UINT CCatLinkBase::GetNameBytes(PBYTE pName, UINT uLen)
{
	if( uLen == 1 ) {

		return pName[0];
		}

	if( uLen == 2 ) {

		return MAKEWORD( pName[1],
				 pName[0]
				 );
		}
	
	if( uLen == 3 ) {

		return MAKELONG( MAKEWORD(pName[2], pName[1]),
				 MAKEWORD(pName[0], 0)
				 );
		}

	return NOTHING;
	}

UINT CCatLinkBase::GetDataBytes(PBYTE pData, UINT uLen)
{
	if( uLen == 1 ) {

		return pData[0];
		}

	if( uLen == 2 ) {

		return MAKEWORD( pData[0],
				 pData[1]
				 );
		}
	
	if( uLen == 4 ) {

		return MAKELONG( MAKEWORD(pData[0], pData[1]),
				 MAKEWORD(pData[2], pData[3])
				 );
		}

	return 999999;
	}

void CCatLinkBase::SetNameBytes(PBYTE pName, UINT uName, UINT uLen)
{
	if( uLen == 1 ) {

		pName[0] = LOBYTE(uName);
		}

	if( uLen == 2 ) {

		pName[0] = HIBYTE(uName);
		pName[1] = LOBYTE(uName);
		}
	
	if( uLen == 3 ) {

		pName[0] = LOBYTE(HIWORD(uName));
		pName[1] = HIBYTE(LOWORD(uName));
		pName[2] = LOBYTE(LOWORD(uName));
		}
	}

void CCatLinkBase::SetDataBytes(PBYTE pData, UINT uData, UINT uLen)
{
	if( uLen == 1 ) {

		pData[0] = LOBYTE(uData);
		}

	if( uLen == 2 ) {

		pData[0] = LOBYTE(uData);
		pData[1] = HIBYTE(uData);
		}
	
	if( uLen == 4 ) {

		pData[0] = LOBYTE(LOWORD(uData));
		pData[1] = HIBYTE(LOWORD(uData));
		pData[2] = LOBYTE(HIWORD(uData));
		pData[3] = HIBYTE(HIWORD(uData));
		}
	}

// End of File
