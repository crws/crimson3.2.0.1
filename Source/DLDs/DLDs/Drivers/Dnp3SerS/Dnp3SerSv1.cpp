
#include "Dnp3SerSv1.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Serial Slave v1 Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CDnp3SerialSlave);

// End of File

