
#ifndef	INCLUDE_DNP3S_HPP
	
#define	INCLUDE_DNP3S_HPP

#include "dnp3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Slave Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Slave Structures
//

struct CEventConfig {

	BYTE m_bMode;
	WORD m_wLimit;
	};

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Slave Driver
//

class CDnp3Slave : public CDnp3Base
{
	public:
		// Constructor
		CDnp3Slave(void);

		// Destructor
		~CDnp3Slave(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
						
	protected:

		struct CContext {

			IDnpSlaveSession * m_pSession;
			CEventConfig     * m_pEvent;
			WORD m_Dest;
			WORD m_TO;
			LONG m_Link;
			BYTE m_eMask;
			WORD m_wEvents;
			BYTE m_bAiCalc;
			};

		// Data Members
		CContext * m_pCtx;

		// Configuration
		void GetEventConfig(PCBYTE &pData);
				
		// Implementation
		BOOL  Validate(BYTE o, WORD i, BYTE t, UINT &uCount);
		CCODE GetData(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount);
		CCODE SetData(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount);
		
		// Sessions
		virtual	void OpenSession(void);
		virtual	void CloseSession(void);

		// Helpers
		BOOL IsReadOnly(BYTE bObject, BYTE e);
		BOOL IsWriteOnly(BYTE bObject, BYTE e);
	};

#endif

// End of File
