
#ifndef	INCLUDE_DNP3SERS_HPP

#define	INCLUDE_DNP3SERS_HPP

#include "dnp3s.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Serial Slave Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

class CDnp3SerialSlave : public CDnp3Slave
{
public:
	// Constructor
	CDnp3SerialSlave(void);

	// Destructor
	~CDnp3SerialSlave(void);

	// Management
	DEFMETH(void) Attach(IPortObject *pPort);

	// Configuration
	DEFMETH(void) CheckConfig(CSerialConfig &Config);

	// Device
	DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
	DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:

		// Channels
		virtual	void OpenChannel(void);
		virtual BOOL CloseChannel(void);
};

#endif

// End of File

