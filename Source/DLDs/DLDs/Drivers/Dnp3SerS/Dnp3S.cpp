
#include "dnp3s.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Slave Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CDnp3Slave::CDnp3Slave(void)
{
	m_pCtx = NULL;
	}

// Destructor

CDnp3Slave::~CDnp3Slave(void)
{
	
	}

// Entry Points

CCODE MCALL CDnp3Slave::Ping(void)
{
	CDnp3Base::Ping();

	if( m_pCtx->m_pSession ) {

		if( m_pCtx->m_pSession->Ping() ) {

			return CCODE_SUCCESS;
			}

		CloseSession();
		}

	CDnp3Base::Service();

	return CCODE_ERROR;
	}

CCODE MCALL CDnp3Slave::Read(AREF Address, PDWORD pData, UINT uCount)
{
	CDnp3Base::Ping();

	if( m_pCtx->m_pSession ) {

		BYTE o = FindObject(Address);

		WORD i = FindIndex(Address);

		BYTE e = Address.a.m_Extra;

		if( IsWriteOnly(o, e) ) {

			return uCount;
			}

		BYTE t = FindType(Address);

		if( Validate(o, i, t, uCount) ) {

			return GetData(o, i, t, e, pData, uCount);
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CDnp3Slave::Write(AREF Address, PDWORD pData, UINT uCount)
{
	CDnp3Base::Ping();

	if( m_pCtx->m_pSession ) {

		BYTE o = FindObject(Address);

		BYTE e = Address.a.m_Extra;
	
		if( IsReadOnly(o, e) ) {
			
			return uCount;
			}

		WORD i = FindIndex(Address);

		BYTE t = FindType(Address);

		if( Validate(o, i, t, uCount) ) {

			return SetData(o, i, t, e, pData, uCount);
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_ERROR;
	}

// Configuration

void CDnp3Slave::GetEventConfig(PCBYTE &pData)
{
	if( GetWord(pData) == 0x1234)  {

		m_pCtx->m_wEvents = GetWord(pData);
		
		if( m_pCtx->m_wEvents > 0 ) {
			
			m_pCtx->m_pEvent = new CEventConfig[m_pCtx->m_wEvents + 1];
			
			for( UINT u = 0; u < m_pCtx->m_wEvents; u++ ) {

				if( GetWord(pData) == 0x1234)  {
				
					m_pCtx->m_pEvent[u].m_bMode  = GetByte(pData);

					m_pCtx->m_pEvent[u].m_wLimit = GetWord(pData);
					}
				}

			m_pCtx->m_pEvent[m_pCtx->m_wEvents].m_bMode = GetByte(pData);
			}
		}
	}

// Implementation

BOOL  CDnp3Slave::Validate(BYTE o, WORD i, BYTE t, UINT &uCount)
{
	if( m_pCtx->m_pSession ) {

		if( (uCount = m_pCtx->m_pSession->Validate(o, i, t, FindCount(t, uCount, FALSE))) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

CCODE CDnp3Slave::GetData(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount)
{
	CCODE Result = CCODE_ERROR | CCODE_HARD;

	switch( e ) {

		case eValue: Result = m_pCtx->m_pSession->GetValue    (o, i, t, pData, uCount);	break;

		case eFlags: Result = m_pCtx->m_pSession->GetFlags    (o, i,    pData, uCount);	break;

		case eTimeS: Result = m_pCtx->m_pSession->GetTimeStamp(o, i,    pData, uCount);	break;

		case eClass: Result = m_pCtx->m_pSession->GetClass    (o, i,    pData, uCount);	break;

		case eBinOn: Result = m_pCtx->m_pSession->GetOnTime   (o, i,    pData, uCount);	break;

		case eBinOf: Result = m_pCtx->m_pSession->GetOffTime  (o, i,    pData, uCount);	break;
		}

	return COMMS_SUCCESS(Result) ? FindCount(t, Result, TRUE) : CCODE_ERROR;
	}

CCODE CDnp3Slave::SetData(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount)
{
	CCODE Result = CCODE_ERROR | CCODE_HARD;

	BYTE  bMask  = m_pCtx->m_bAiCalc;

	switch( e ) {

		case eValue: Result = m_pCtx->m_pSession->SetValue  (o, i, t, pData, uCount, bMask);	break;

		case eFlags: Result = m_pCtx->m_pSession->SetFlags  (o, i,    pData, uCount, bMask);	break;

		case eTimeS: Result = uCount;								break;

		case eClass: Result = m_pCtx->m_pSession->SetClass  (o, i,    pData, uCount);		break;
				
		case eBinOn: Result = m_pCtx->m_pSession->SetOnTime (o, i,    pData, uCount);		break;

		case eBinOf: Result = m_pCtx->m_pSession->SetOffTime(o, i,    pData, uCount);		break;
		}
	
	return COMMS_SUCCESS(Result) ? FindCount(t, Result, TRUE) : CCODE_ERROR;
	}

// Session

void CDnp3Slave::OpenSession(void)
{
	if( m_pDnp && !m_pCtx->m_pSession ) {

		m_pCtx->m_pSession = (IDnpSlaveSession *) m_pDnp->OpenSession(m_pChannel,
									      m_pCtx->m_Dest,
									      m_pCtx->m_TO,
									      m_pCtx->m_Link,
									      m_pCtx->m_pEvent);
		}
	}

void CDnp3Slave::CloseSession(void)
{
	if( m_pDnp && m_pCtx->m_pSession ) {

		if( m_pDnp->CloseSession(m_pCtx->m_pSession) ) {
		
			m_pCtx->m_pSession = NULL;
			}
		}
	}


// Helpers

BOOL CDnp3Slave::IsReadOnly(BYTE bObject, BYTE e)
{
	return e == 2;
	}

BOOL CDnp3Slave::IsWriteOnly(BYTE bObject, BYTE e)
{
	return FALSE;
	}

// End of File
