
#include "rlc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Red Lion Instrument Driver
//

// Instantiator

INSTANTIATE(CRedLionSerialDriver);

// Constructor

CRedLionSerialDriver::CRedLionSerialDriver(void)
{
	m_Ident = DRIVER_ID;

	}

// Destructor

CRedLionSerialDriver::~CRedLionSerialDriver(void)
{
	}

// Configuration

void MCALL CRedLionSerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CRedLionSerialDriver::CheckConfig(CSerialConfig &Config)
{
	if ( Config.m_uDataBits == 7 && Config.m_uParity == parityOdd )	{
	
		Config.m_uFlags |= flagNoCheckParity;
		}
	
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CRedLionSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CRedLionSerialDriver::Open(void)
{
		
	}

// Device

CCODE MCALL CRedLionSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_fAbbr = GetByte(pData);

			m_pCtx->m_Delay[delayStdRead]	= GetWord(pData);

			m_pCtx->m_Delay[delayStdWrite]	= GetWord(pData);

			m_pCtx->m_Delay[delayNdxRead]	= GetWord(pData);
			
			m_pCtx->m_Delay[delayNdxWrite]	= GetWord(pData);

			m_pCtx->m_Delay[delayCommand]	= GetWord(pData);

			m_pCtx->m_fFastResp		= GetByte(pData);

			m_pCtx->m_Device		= GetByte(pData);

			m_pCtx->m_Cub5Type		= GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	
	}

CCODE MCALL CRedLionSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CRedLionSerialDriver::Ping(void)
{
	if ( m_pCtx->m_Device != DEVICE_MDIADI ) {

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = addrNamed;
		Addr.a.m_Offset = 'A';
		Addr.a.m_Type   = addrLongAsLong;
		Addr.a.m_Extra  = 0;

		return Read(Addr, Data, 1);
		}

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = 0x4901;
	Addr.a.m_Extra  = 2;
	Addr.a.m_Type   = addrLongAsLong;

	return Read(Addr, Data, 1); 
	}

CCODE MCALL CRedLionSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uData = UINT(Addr.a.m_Offset);

	if ( IsAddressValid(uData, DEVICE_READ) ) {

		m_sName[0] = LOBYTE(uData);
		m_sName[1] = HIBYTE(uData);
		m_sName[2] = 0;

		return DoStdRead(pData, uCount, uData, Addr.a.m_Extra);
		}

	// don't return error on 'read' of reset or set output

	if( LOBYTE(uData) == 'R' || ( m_pCtx->m_Device == DEVICE_C48 && LOBYTE(uData) == 'S') ) { 

		*pData = 0;

		return uCount;
		}

	if ( ( Addr.a.m_Extra == 2 ) && ( m_pCtx->m_Device == DEVICE_MDIADI ) ) {

		SPrintf(m_sName, "I%2.2u", uData);
			
		return DoNdxRead(pData, uCount);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CRedLionSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uData = UINT(Addr.a.m_Offset);

	if ( IsAddressValid(uData, DEVICE_WRITE) ) {
		
		m_sName[0] = LOBYTE(uData);
		m_sName[1] = HIBYTE(uData);
		m_sName[2] = 0;
		
		return DoStdWrite(pData, uCount, uData, Addr.a.m_Extra);
		}

	if ( ( Addr.a.m_Extra == 2 ) && ( m_pCtx->m_Device == DEVICE_MDIADI ) ) {

		SPrintf(m_sName, "I%2.2u", uData);
			
		return DoNdxWrite(pData, uCount);
		}

	if ( IsCommandValid(uData) ) {

		m_sName[0] = LOBYTE(uData);
		m_sName[1] = HIBYTE(uData);
		m_sName[2] = 0;

		return DoCommand(pData, uCount);
		}

	if ( IsResetValid(uData) ) {

		m_sName[0] = LOBYTE(uData);
		m_sName[1] = HIBYTE(uData);
		m_sName[2] = 0;

		return DoReset(pData, uCount);
		}

	if( IsAddressValid(uData, DEVICE_READ) ) {

		return uCount; // do nothing on write attempt to Read-Only
		}

	return CCODE_ERROR;
	}

// Implementation

CCODE CRedLionSerialDriver::DoStdWrite(PDWORD pData, UINT uCount, UINT uOffset, UINT uExtra)
{
	char sWork[32];

	MakeMin(uCount, 1);

	UINT uFixed = GetRegisterFixedWidth(uOffset);

	DWORD dData = *pData;

	char cTerm = m_pCtx->m_fFastResp ? '$' : '*';

	if( uFixed && uExtra == 3 ) {

		dData = ToBinary(dData, uFixed);
		}
		
	if( uFixed ) {

		PCTXT sTemp = ((uFixed == 5) ? "N%2.2uV%s%5.5lu%c" : "N%2.2uV%s%4.4lu%c"); 

		SPrintf(sWork, sTemp, m_pCtx->m_bDrop, m_sName, dData, cTerm);
		}
	
	else if( IsChar(uOffset) ) {

		if( dData == 0 ) {

			dData = 128;
			}

		SPrintf(sWork, "N%2.2uV%s%c%c", m_pCtx->m_bDrop, m_sName, LOBYTE(LOWORD(dData)), cTerm);
		}
	
	else {
	      	SPrintf(sWork, "N%2.2uV%s%ld%c", m_pCtx->m_bDrop, m_sName, dData, cTerm);
		}

	Send(sWork);

	while( m_pData->Read(0) < NOTHING );

	Sleep(m_pCtx->m_Delay[delayStdWrite]);

	if( IsChar(uOffset) ) {

		return uCount;
		}
		
	DWORD Read = 0;

	if( IsChangingValueWrite(uOffset) ) {

		return uCount; // Value may have changed by the time read is complete
		}

	UINT uResult = DoStdRead(&Read, uCount, uOffset, uExtra);

	if( uResult == uCount ) {

		if( Read == dData || uFixed ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE CRedLionSerialDriver::DoStdRead(PDWORD pData, UINT uCount, UINT uOffset, UINT uExtra)
{
	char sWork[32];

	MakeMin(uCount, 1);

	char cTerm = m_pCtx->m_fFastResp ? '$' : '*';
	
	SPrintf(sWork, "N%2.2uT%s%c", m_pCtx->m_bDrop, m_sName, cTerm);
	
	Send(sWork);

	UINT uDataStartPos;

	if( GetStdReply() ) {

		if( m_bRx[0] == 'E' ) {

			return CCODE_ERROR;
			}

		if( CheckAddress() && GetDataStart(&uDataStartPos) ) {

			*pData = GetRLCData(uDataStartPos);
			}

		else {	
			return CCODE_ERROR;
			}

		UINT uFixed = GetRegisterFixedWidth(uOffset);

		if( uFixed && uExtra == 3 ) {

			*pData = ToBinary(*pData, uFixed);
			}

		Sleep(m_pCtx->m_Delay[delayStdRead]);

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CRedLionSerialDriver::DoNdxWrite(PDWORD pData, UINT uCount)
{
	char sWork[32];

	MakeMin(uCount, 1);

	char cTerm = m_pCtx->m_fFastResp ? '$' : '*';

	SPrintf(sWork, "N%2.2u%s:%lu%c", m_pCtx->m_bDrop, m_sName, *pData, cTerm);
	
	Send(sWork);

	Sleep(m_pCtx->m_Delay[delayNdxWrite]);
	
	DWORD Read = 0;

	UINT uResult = DoNdxRead(&Read, uCount);
	
	if( uResult == uCount ) {
	
		if( Read == *pData ) {

			return uCount;
			}

		return CCODE_ERROR;
		}
		
	return CCODE_ERROR;
	}

CCODE CRedLionSerialDriver::DoNdxRead(PDWORD pData, UINT uCount)
{
	char sWork[32];

	MakeMin(uCount, 1);

	char cTerm = m_pCtx->m_fFastResp ? '$' : '*';

	SPrintf(sWork, "N%2.2u%s%c", m_pCtx->m_bDrop, m_sName, cTerm);
	
	Send(sWork);
	
	if( GetNdxReply() ) {

		*pData = ATOI(PCTXT(m_bRx));

		Sleep(m_pCtx->m_Delay[delayNdxRead]);
		
		return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CRedLionSerialDriver::DoCommand(PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	if( *pData == 0 ) {

		return uCount;
		}
	
	char sWork[32];

	char cTerm = m_pCtx->m_fFastResp ? '$' : '*';

	SPrintf(sWork, "N%2.2u%s%lu%c", m_pCtx->m_bDrop, m_sName, *pData, cTerm);
	
	Send(sWork);

	Sleep(m_pCtx->m_Delay[delayCommand]);
	
	m_pData->ClearRx();
	
	return uCount;
	}

CCODE CRedLionSerialDriver::DoReset(PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);
	
	if( *pData == 0 ) {

		return uCount;
		}
	
	char sWork[32];

	char cTerm = m_pCtx->m_fFastResp ? '$' : '*';

	SPrintf(sWork, "N%2.2u%s%c", m_pCtx->m_bDrop, m_sName, cTerm);
	
	Send(sWork);

	Sleep(m_pCtx->m_Delay[delayCommand]);
	
	m_pData->ClearRx();
	
	return uCount;
	}

void CRedLionSerialDriver::Send(PCTXT pText)
{
	m_pData->ClearRx();

	m_pData->Write(PBYTE(pText), strlen(pText), FOREVER);
	}

BOOL CRedLionSerialDriver::GetStdReply(void)
{
	UINT uState = 0;

	UINT uCount = 0;

	UINT uData  = 0;

	UINT uTimer = 0;

	BOOL fCR    = FALSE;

	SetTimer(2000);

	while( ( uTimer = GetTimer() ) ) {
			
		if( ( uData = m_pData->Read(uTimer) ) == NOTHING ) {
		
			continue;
			}

		if( uCount >= sizeof(m_bRx) ) {

			return FALSE;
			}

		switch( uState ) {
		
			case 0:
				if( uData == CR ) {

					fCR    = TRUE;

					uState = 1;
					}

				else {
					m_bRx[uCount++] = uData;
					}

				break;
				
			case 1:	
				if( fCR && uData == LF ) {

					m_bRx[uCount] = 0;

					return uCount > 0;
					}
				// previous uData == CR was valid character (CSR register in PAX)
				m_bRx[uCount++] = CR;

				if( uData != CR ) {

					m_bRx[uCount++] = uData;

					uState = 0;

					fCR = FALSE;
					}
				
				break;
			}
		
		} 

	return FALSE;
	}

BOOL CRedLionSerialDriver::GetNdxReply(void)
{
	UINT uState = 0;
	
	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uData = 0;

	SetTimer(500);
	
	while( ( uTimer = GetTimer() ) ) {
	
		if ( ( uData = m_pData->Read(uTimer) ) == NOTHING )	{
		       		
			continue;
			}
			
		switch( uState ) {
		
			case 0:
				if( uData == 'N' )
					uState = 1;
				else
					uState = 0;
				break;

			case 1:
				if( uData == 'D' )
					uState = 2;
				else
					uState = 0;
				break;

			case 2:
				if( uData == 'X' )
					uState = 3;
				else
					uState = 0;
				break;
				
			case 3:
				if( uData == '<' ) {
					
					uCount = 0;
					
					uState = 4;
					}
				break;
				
			case 4:
				if( uData == '>' ) {
				
					m_bRx[uCount] = 0;
					
					uState = 5;
					
					return TRUE;
					}
				else {
					m_bRx[uCount] = uData;
					
					if( ++uCount == sizeof(m_bRx) )
						return FALSE;
					}
				break;
				
			case 5:
				if( uData == '*' )
					return TRUE;
				else {
					if( uData == 32 )
						break;
						
					if( uData == CR )
						break;
						
					if( uData == LF )
						break;
					
					return FALSE;
					}
				break;
			}
		}

	return FALSE;
	}

// Helpers

BOOL CRedLionSerialDriver::IsAddressValid(UINT uData, UINT uOp)
{	
	switch (m_pCtx->m_Device) {

		case DEVICE_C48:
			
			switch (uData) {

				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				
					return TRUE;
				}

			return FALSE;
		
		case DEVICE_LEGEND:

			if ( uOp == DEVICE_READ ) {

				switch (uData) {

					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'I':
					case 'J':
					case 'K':
					case 'L':
					case 'O':
					case 'Q':
						
						return TRUE;
					}
				}

			if ( uOp == DEVICE_WRITE ) {

				switch (uData) {

					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'K':
					case 'L':
					case 'O':
					case 'Q':

						return TRUE;
					}
				}

			return FALSE;
			
		case DEVICE_MDIADI:

			return FALSE;

		case DEVICE_PAX:

			if ( uOp == DEVICE_READ ) {

				switch (uData) {

					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'I':
					case 'J':
					case 'K':
					case 'L':
					case 'Q': 
											
						return TRUE;
					}
				}

			if ( uOp == DEVICE_WRITE ) {

				switch (uData) {

					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'I':
					case 'J':
					case 'K':
					case 'L':
					
						return TRUE;
					}
				}

			return FALSE;

		case DEVICE_PAXI:

			switch (uData) {

				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
				case 'K':
				case 'L':
				case 'M':
				case 'O':
				case 'Q':
				case 'S':
				case 'U':
				case 'W':
				case 'X':
											
					return TRUE;
				}

			return FALSE;

		case DEVICE_TP48:
			
			if ( uOp == DEVICE_READ ) {
				
				switch (uData) {
					
					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'I':
					case 'J':
					case 'K':
					case 'L':
					case 'M':
					case 0x4242:
					case 0x4348:
					case 'W':

						return TRUE;
					}
				}

			if ( uOp == DEVICE_WRITE ) {

				switch (uData) {

					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'J':
					case 'K':
					case 'L':
					case 'M':

						return TRUE;
					}
				}

			return FALSE;

		case DEVICE_TPCU:

			if ( uOp == DEVICE_READ ) {

				switch (uData) {

					case 'A':
					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'I':
					case 'J':
					case 'K':
					case 'L':
					case 'M':
					case 'O':
					case 'Q':
					case 'W':
					case 'X':
					case 'Y':
					case 'Z':
					case 0x4141:
					case 0x4242:
					case 0x4348:

						return TRUE;
					}
				}

			if ( uOp == DEVICE_WRITE ) {

				switch (uData) {

					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'J':
					case 'K':
					case 'L':
					case 'M':
					case 'O':
					case 'Q':
					case 'X':
					case 'Y':
					case 'Z':

						return TRUE;
					}
				}

			return FALSE;
		
		case DEVICE_CUB5:

			switch( m_pCtx->m_Cub5Type ) {

				case DEVICE_CUB5C:

					if ( uOp == DEVICE_READ ) {

						switch ( uData ) {

							case 'A':
							case 'B':
							case 'C':
							case 'D':
							case 'E':
							case 'F':
							case 'G':
							case 'H':
							
								return TRUE;
							}
						}

					if ( uOp == DEVICE_WRITE ) {

						switch (uData) {

							case 'A':
							case 'B':
							case 'C':
							case 'D':
							case 'E':
							case 'F':
							case 'G':
							case 'H':
							
								return TRUE;
							}
						}

					return FALSE;
		
				case DEVICE_CUB5TM:

					switch ( uData ) {

						case 'A':
						case 'B':
						case 'C':
						case 'D':
						case 'E':

							return TRUE;
						}

					return FALSE;

				case DEVICE_CUBANA:

					switch ( uData ) {

						case 'A':
						case 'B':
						case 'C':
						case 'D':
						case 'E':
						
							return TRUE;
						}

					return FALSE;
				}

			return FALSE;
		}
		
	return FALSE;
	}

BOOL CRedLionSerialDriver::IsCommandValid(UINT uData)
{
	if ( LOBYTE(uData) == 'C' ) {

		switch( m_pCtx->m_Device ) {

			case DEVICE_TP48:

				switch (HIBYTE(uData)) {

					case 'S':
					case 'T':
					case 'U':
				
						return TRUE;
					}

				return FALSE;

			case DEVICE_TPCU:

				switch (HIBYTE(uData)) {

					case 'S':
					case 'U':

						return TRUE;
					}

				return FALSE;
			}
		}

	return FALSE;
	}

BOOL CRedLionSerialDriver::IsResetValid(UINT uData)
{
	if ( LOBYTE(uData) == 'R' ) {

		switch( m_pCtx->m_Device ) {

			case DEVICE_TP48:
			case DEVICE_TPCU:

				switch (HIBYTE(uData)) {

					case 'G':
					case 'H':
				
						return TRUE;
					}

				return FALSE;

			case DEVICE_LEGEND:

				switch (HIBYTE(uData)) {

					case 'E':
					case 'F':
					case 'G':
					case 'I':
					case 'J':
					case 'O':
					case '1':
					case '2':
					case '3':
					case '4':

						return TRUE;
					}

				return FALSE;

			case DEVICE_C48:

				switch (HIBYTE(uData)) {

					case 'E':
					case 'F':
					case '1':
					case '2':
					case '3':
				
						return TRUE;
					}

				return FALSE;

			case DEVICE_PAX:

				switch (HIBYTE(uData)) {

					case 'B':
					case 'C':
					case 'D':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'I':
					case 'J':
				      			
						return TRUE;
					}

				return FALSE;

			case DEVICE_PAXI:

				switch (HIBYTE(uData)) {

					case 'A':
					case 'B':
					case 'C':
					case 'E':
					case 'F':
					case 'G':
					case 'H':
					case 'M':
					case 'O':
					case 'Q':
					case 'S':
				      			
						return TRUE;
					}

				return FALSE;

			case DEVICE_CUB5:

				switch( m_pCtx->m_Cub5Type ) {

					case DEVICE_CUB5C:

						switch (HIBYTE(uData)) {

							case 'A':
							case 'B':
							case 'F':
							case 'G':
				      			
								return TRUE;
							}

						return FALSE;

					case DEVICE_CUB5TM:

						switch (HIBYTE(uData)) {

							case 'A':
							case 'B':
							case 'E':

								return TRUE;
							}

						return FALSE;

					case DEVICE_CUBANA:

						switch ( HIBYTE(uData) ) {

							case 'A':
							case 'B':
							case 'C':
							case 'D':
							case 'E':
						
								return TRUE;
							}

						return FALSE;
					     }

				return FALSE;
			}
		}

	else if( LOBYTE(uData) == 'S' ) {

		if( m_pCtx->m_Device == DEVICE_C48 ) {

			switch (HIBYTE(uData)) {

				case '1':
				case '2':
				case '3':
				
				return TRUE;
				}
			}
		}
 	
	return FALSE;
	}

UINT  CRedLionSerialDriver::GetRegisterFixedWidth(UINT uOffset)
{
	if ( m_pCtx->m_Device == DEVICE_PAXI ) {

		switch( LOBYTE(uOffset) ) {

			case 'U':
				return 5;
			case 'X':
				return 4;

			}
		}
	
	return 0;
	}

DWORD CRedLionSerialDriver::ToBinary(DWORD dData, UINT uFixed)
{
	DWORD dBinary = 0;

	for( UINT uBit = 1, y = 0; dData && y < uFixed; uBit*=10, y++ ) {

		if( dData & 0x01 ) {

			dBinary += uBit;
			}

		dData = dData >> 1;
		}

	return dBinary;

	}

BOOL  CRedLionSerialDriver::IsChar(UINT uOffset) 
{
	if ( m_pCtx->m_Device == DEVICE_PAX ) {

		switch( LOBYTE(uOffset) ) {

			case 'J':
				return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL  CRedLionSerialDriver::IsChangingValueWrite(UINT uOffset)
{
// These values may change in between writing and reading.
	BYTE b = LOBYTE(uOffset);

	if( !HIBYTE(uOffset) ) {

		switch( m_pCtx->m_Device ) {

			case DEVICE_C48:

				return b == 'E' || b == 'F';

			case DEVICE_LEGEND:

				return b == 'E' || b == 'F' || b == 'G';

			case DEVICE_PAXI:

				return	b == 'A' || b == 'B' || b == 'C' ||
					b == 'D' || b == 'M' || b == 'O' ||
					b == 'Q' || b == 'S' || b == 'X';

			case DEVICE_CUB5:

				switch( m_pCtx->m_Cub5Type ) {

					case DEVICE_CUB5C:
					case DEVICE_CUB5TM:

						return b == 'A' || b == 'B';
					}

			case DEVICE_TP48:
			case DEVICE_TPCU:

				return b == 'C' || b == 'G' || b == 'H';

			}
		}

	return FALSE;
	}

BOOL  CRedLionSerialDriver::CheckAddress(void)
{
	if( !m_pCtx->m_fAbbr ) {

		char c[3];

		c[0] = m_bRx[0];
		c[1] = m_bRx[1];
		c[2] = 0;

		return m_pCtx->m_bDrop == ATOI(PCTXT(c));
		}

	return TRUE;
	}

DWORD CRedLionSerialDriver::GetRLCData(UINT uPos)
{
// would like TODO - detect F,C,S,%... at the end of the string and either
// do d = (d << 8) + c, or ignore it altogether.  But it'd break existing apps.
	DWORD d       = 0;

	BOOL  fNeg    = FALSE;

	UINT c;

	switch( m_bRx[uPos] ) {

		case '-':
			fNeg = TRUE;
		case '+':
		case '*':
		case ' ':
		case '.':
			uPos++;
			break;
		}

	while( m_bRx[uPos] ) {

		c = m_bRx[uPos];

		if( isdigit(c) ) {

			d = d * 10 + (c - '0');
			}

		else {
			if( c != '.' && c != ' ' ) {

				d = d * 16 + c;
				}
			}

		uPos++;
		}

	return fNeg ? -d : d;
	}

BOOL  CRedLionSerialDriver::GetDataStart(UINT * pPos)
{
	UINT u = 0;

	if( m_pCtx->m_fAbbr ) {

		while( u < sizeof(m_bRx) ) {

			if( m_bRx[u] != ' ' ) {

				*pPos = u;

				return TRUE;
				}
			u++;
			}

		return FALSE;
		}

	u = m_bRx[2] == ' ' ? 6 : 5; // 2 Addr +? space + 3 Mnemonic

	while( m_bRx[u] == ' ' ) {

		u++;
		}

	while( u < sizeof(m_bRx) ) {

		switch( m_bRx[u] ) {

			case ' ':

				break;

			case '*':
			case '+':
			case '.':
				*pPos = u + 1;
				return TRUE;

			default:
				*pPos = u;
				return TRUE;
			}
		u++;
		}

	return FALSE;
	}

// End of File
