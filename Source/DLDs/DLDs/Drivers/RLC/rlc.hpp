//////////////////////////////////////////////////////////////////////////
//
// Include
//

#include "intern.hpp"

#include <ctype.h>

//////////////////////////////////////////////////////////////////////////
//
// Generic RLC Instrument Constants
//
// Device Selections
#define DEVICE_C48		0
#define DEVICE_LEGEND		1
#define DEVICE_PAX		2
#define DEVICE_PAXI		3
#define DEVICE_TP48		4
#define DEVICE_TPCU		5
#define	DEVICE_CUB5		6 // Master Cub5 Selection
// Cub5 Sub Menu
#define	DEVICE_CUB5C		0
#define	DEVICE_CUB5TM		1
#define DEVICE_CUBANA		2
//#define	DEVICE_CUB5T	? // Temperature
//#define	DEVICE_CUB5S	? // Strain Gage
//#define	DEVICE_CUB5P	? // Process
//#define	DEVICE_CUB5H	? // AC
//#define	DEVICE_CUB5D	? // DC
//#define	DEVICE_CUB5R	? // Rate
#define DEVICE_MDIADI		0xFF // not currently configurable, was 6

// Opcodes				     
#define DEVICE_READ		0
#define DEVICE_WRITE		1

//////////////////////////////////////////////////////////////////////////
//
// Generic RLC Instrument
//

class CRedLionSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CRedLionSerialDriver(void);

		// Destructor
		~CRedLionSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		
	protected:
		// Delays
		enum {
			delayStdRead	= 0,
			delayStdWrite	= 1,
			delayNdxRead	= 2,
			delayNdxWrite	= 3,
			delayCommand	= 4
			};

		// Device Data
		struct CContext
		{
			BOOL m_fAbbr;
			BOOL m_fFastResp;
			BYTE m_bDrop;
			UINT m_Device;
			UINT m_Cub5Type;
			UINT m_Delay[5];
			
			};

		// Data Members
		CContext * m_pCtx;
		char	   m_sName[8];
		BYTE       m_bRx[64];
		
		// Implementation
		CCODE DoStdWrite(PDWORD pData, UINT uCount, UINT uOffset, UINT uExtra);
		CCODE DoStdRead(PDWORD pData, UINT uCount, UINT uOffset, UINT uExtra);
		CCODE DoNdxWrite(PDWORD pData, UINT uCount);
		CCODE DoNdxRead(PDWORD pData, UINT uCount);
		CCODE DoCommand(PDWORD pData, UINT uCount);
		CCODE DoReset(PDWORD pData, UINT uCount);
		void Send(PCTXT pText);
		BOOL GetStdReply(void);
		BOOL GetNdxReply(void);	

		// Helpers
		BOOL  IsAddressValid(UINT uData, UINT uOp);
		BOOL  IsCommandValid(UINT uData);
		BOOL  IsResetValid(UINT uData);
		UINT  GetRegisterFixedWidth(UINT uOffset);
		DWORD ToBinary(DWORD dData, UINT uFixed);
		BOOL  IsChar(UINT uOffset);
		BOOL  IsChangingValueWrite(UINT uOffset);
		BOOL  CheckAddress(void);
		DWORD GetRLCData(UINT uPos);
		BOOL  GetDataStart(UINT * pPos);
	};

// End of File
