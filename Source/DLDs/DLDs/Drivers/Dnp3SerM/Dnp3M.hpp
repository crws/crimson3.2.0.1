
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Master Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DNP3M_HPP
	
#define	INCLUDE_DNP3M_HPP

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "dnp3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  Enumerations
//

enum Limit {

	limitFB = 100,
	};

//////////////////////////////////////////////////////////////////////////
//
//  Structures
//

struct Feedback {

	BYTE  m_bObject;
	WORD  m_wPoint;
	DWORD m_uFlags;
	DWORD m_uTimeStamp;
	};

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Master Driver
//

class CDnp3Master : public CDnp3Base
{
	public:
		// Constructor
		CDnp3Master(void);

		// Destructor
		~CDnp3Master(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// User Access
		DEFMETH(UINT) DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
						
	protected:

		struct CContext {

			IDnpMasterSession * m_pSession;
			WORD m_Dest;
			WORD m_TO;
			LONG m_Link;
			BYTE m_Poll;
			UINT m_Class[4];
			UINT m_Time [4];
			};

		// Data Members
		CContext * m_pCtx;
		Feedback * m_pFeedBack;
		UINT	   m_uFbNext;
				
		// Implementation
		BOOL  Validate(BYTE o, WORD i, BYTE t, UINT &uCount);
		BOOL  PollClass(void);
		CCODE GetData(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount);
		CCODE HandleNamedCmd(BYTE o, WORD i, PDWORD pData);
		CCODE HandleCmd(BYTE o, WORD i, BYTE t, BYTE e, PDWORD pData, UINT uCount);
		BOOL  SetFeedBack(BYTE bObject, WORD wIndex);
		void  GetFeedBack(WORD i, BYTE e, PDWORD pData, UINT uCount);
		BOOL  IsTimedOut(UINT uClass);
		
		// Sessions
	virtual	void OpenSession(void);
	virtual	void CloseSession(void);

		// Helpers
		BOOL IsEvent(BYTE o);
		BOOL IsFeedback(BYTE o);
		BOOL IsValue(BYTE e);
		BOOL IsNamedCmd(BYTE o);
		BOOL IsReadOnly(BYTE o, BYTE e);
		BOOL IsWriteOnly(BYTE o, BYTE e);


	};

// End of File

#endif
