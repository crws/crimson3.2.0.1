
#include "Intern.hpp"

#include "dnp3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Base Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CDnp3Base::CDnp3Base(void)
{
	m_pChannel	= NULL;

	m_fOpen		= FALSE;

	m_pDnp		= NULL;

	m_pExtra	= NULL;
	}

// Destructor

CDnp3Base::~CDnp3Base(void)
{
	}

// Config

void MCALL CDnp3Base::Load(LPCBYTE pData)
{	
	if( GetWord(pData) == 0x1234 ) {

		m_Source = GetWord(pData);
				
		return;
		}
	}

// Management

void MCALL CDnp3Base::Attach(IPortObject *pPort)
{
	if( !m_pDnp && !m_fOpen ) {

		m_pHelper->MoreHelp(IDH_DNP,   (void **) &m_pDnp);

		m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
		}
	}

void MCALL CDnp3Base::Open(void) 
{
	}

void MCALL CDnp3Base::Detach(void)
{
	if( m_pDnp ) {

		m_pDnp->Release();
		}

	if( m_pExtra ) {

		m_pExtra->Release();
		}
	}

void MCALL CDnp3Base::Close(void) 
{
	CloseChannel();
	}

// User Access

UINT MCALL CDnp3Base::DrvCtrl(UINT uFunc, PCTXT Value)
{
	return 0;
	}

// Entry Points

CCODE MCALL CDnp3Base::Ping(void)
{
	OpenChannel();

	OpenSession();

	return CCODE_SUCCESS;
	}

void MCALL CDnp3Base::Service(void)
{
	if( m_pDnp ) {

		m_pDnp->Service();

		if( m_pChannel ) {

			m_pChannel->Service();
			}
		}

	Sleep(100);
	}

// Implementation

BYTE CDnp3Base::FindObject(AREF Addr)
{
	switch( Addr.a.m_Table ) {

		case 233:	return 34;
		case 234:	return 30;
		case 235:	return 32;
		case 236:	return 40;	
		case 237:	return 42;
		case 238:	return 43;
		}

	return Addr.a.m_Table;
	}

WORD CDnp3Base::FindIndex(AREF Addr)
{
	if( IsDouble(FindType(Addr)) ) {

		return Addr.a.m_Offset / 2;
		}

	return Addr.a.m_Offset;
	}

BYTE CDnp3Base::FindType(AREF Addr)
{
	switch( Addr.a.m_Table ) {

		case 233:
		case 234:	
		case 235:	
		case 236:		
		case 237:	
		case 238:	return typeDouble;
		}


	return Addr.a.m_Type;
	}

UINT CDnp3Base::FindCount(BYTE bType, UINT uCount, BOOL fResult)
{
	if( IsDouble(bType) ) {

		if( fResult ) {

			return uCount * 2;
			}

		if( uCount > 1 ) {

			return uCount / 2;
			}
		}
	
	return uCount;
	}

BOOL CDnp3Base::IsDouble(BYTE bType)
{
	return bType == typeDouble;
	}

// Channels

void CDnp3Base::OpenChannel(void)
{
	
	}

BOOL CDnp3Base::CloseChannel(void)
{
	if( m_pDnp ) {

		if( m_pChannel ) {
			
			m_pChannel->CloseSessions();

			return TRUE;
			}
		}

	return FALSE;
	}

// Session

void CDnp3Base::OpenSession(void)
{
	
	}

void CDnp3Base::CloseSession(void)
{
	}

// End of File
