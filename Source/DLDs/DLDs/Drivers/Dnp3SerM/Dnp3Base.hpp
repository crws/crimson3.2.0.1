
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Base Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DNP3BASE_HPP
	
#define	INCLUDE_DNP3BASE_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Debugging Help
//

static BOOL DNP3_DEBUG = 0;

//////////////////////////////////////////////////////////////////////////
//
//  Enumerations
//

enum Port {
	
	portTCP = 0,
	portUDP = 1,
	portIP  = 2,
	};

enum Extra {

	eValue = 0,
	eFlags = 1,
	eTimeS = 2,
	eClass = 3,
	eBinOn = 4,
	eBinOf = 5,
	};

//////////////////////////////////////////////////////////////////////////
//
//  Structures
//

struct Config {

	DWORD	 m_IP;
	WORD	 m_Tcp;
	WORD	 m_Udp;
	WORD	 m_TO;
	};

//////////////////////////////////////////////////////////////////////////
//
//  Constants
//

#define typeDouble	addrRealAsReal + 1

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Base Driver
//

class CDnp3Base : public CMasterDriver
{
	public:
		// Constructor
		CDnp3Base(void);

		// Destructor
		~CDnp3Base(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Close(void);

		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value); 
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(void ) Service(void);

	protected:
		// Library Object
		IDnpHelper   * m_pDnp;
		IExtraHelper * m_pExtra;
		
		// Port
		IDnpChannel * m_pChannel;

		// Data Members
		WORD m_Source;
		
		// Implementation
		BYTE FindObject(AREF Addr);
		WORD FindIndex(AREF Addr);
		BYTE FindType(AREF Addr);
		UINT FindCount(BYTE bType, UINT uCount, BOOL fResult);
		BOOL IsDouble(BYTE bType);
				
		// Channels
	virtual	void OpenChannel(void);
	virtual	BOOL CloseChannel(void);

		// Sessions
	virtual	void OpenSession(void);
	virtual	void CloseSession(void);

	};

// End of File

#endif
