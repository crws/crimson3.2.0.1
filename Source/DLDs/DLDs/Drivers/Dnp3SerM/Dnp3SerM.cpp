
#include "Intern.hpp"

#include "dnp3serm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Serial Master Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CDnp3SerialMaster);

// Constructor

CDnp3SerialMaster::CDnp3SerialMaster(void)
{
	m_Ident   = 0x40C0;	
	}

// Destructor

CDnp3SerialMaster::~CDnp3SerialMaster(void)
{
	}

// Management

void MCALL CDnp3SerialMaster::Attach(IPortObject *pPort)
{
	CDnp3Master::Attach(pPort);

	m_pData = MakeDoubleDataHandler();

	m_pData->SetRxSize(2048);

	pPort->Bind(m_pData);
	}

// Config

void MCALL CDnp3SerialMaster::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}

// Device

CCODE MCALL CDnp3SerialMaster::DeviceOpen(IDevice *pDevice)
{
	CDnp3Base::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_pSession = NULL;

			m_pCtx->m_Dest     = GetWord(pData);

			m_pCtx->m_TO       = GetWord(pData);

			m_pCtx->m_Link	   = GetLong(pData);

			m_pCtx->m_Poll     = GetByte(pData);

			for( UINT u= 0; u < elements(m_pCtx->m_Class); u++ ) {

				m_pCtx->m_Class[u] = ToTicks(GetLong(pData));

				m_pCtx->m_Time [u] = 0;
				}

			m_pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CDnp3SerialMaster::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		CloseSession();

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CDnp3Base::DeviceClose(fPersist);
	}


// Channels

void CDnp3SerialMaster::OpenChannel(void)
{
	if( m_pDnp ) {

		if( !m_pChannel ) {

			if( (m_pChannel = m_pDnp->OpenChannel(m_pData, m_Source, TRUE)) ) {

				m_fOpen = TRUE;
				}
			}
		}
	}

BOOL CDnp3SerialMaster::CloseChannel(void)
{
	if( CDnp3Base::CloseChannel() ) {

		if( m_pDnp->CloseChannel(m_pData, m_Source, TRUE) ) {

			m_pChannel = NULL;

			m_fOpen    = FALSE;
			}
		}

	return !m_fOpen;
	}

// End of File
