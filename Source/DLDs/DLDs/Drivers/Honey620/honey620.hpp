
//////////////////////////////////////////////////////////////////////////
//
// Honeywell IPC620 Data Spaces
//

#define	SPACED	0x01
#define	SPACEI	0x02

// Reply states for Loader/Terminal port
#define	LTSTATE0	20
#define	LTSTATE1	21
#define	LTSTATE2	22
#define	LTSTATE3	23
#define	LTSTATE4	24

// Max Comm Rate Setting
#define	COMMDELAY	200 // PLC performance drops with fast comms

//////////////////////////////////////////////////////////////////////////
//
// Honeywell IPC620 Driver
//

class CHoneywellIPC620Driver : public CMasterDriver
{
	public:
		// Constructor
		CHoneywellIPC620Driver(void);

		// Destructor
		~CHoneywellIPC620Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Driver Data
		WORD	m_wLTConnection;

		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			};

		// Data Members
		CContext * m_pCtx;

		BYTE	m_bTx[32];
		BYTE	m_bRx[256];
		UINT	m_uPtr;
		UINT	m_uNext;
		UINT	m_uTime;
				
		// Implementation

		// Port Access
		UINT	RxByte(UINT uTime);
		
		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(UINT uData);
		
		// Transport Layer
		BOOL	GetReply(void);
		BOOL	Transact(void);

		// Read Handlers
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Control Update Timing
		BOOL	TimeToExecuteComms(void);

		// Connection to Loader/Terminal Port
		CCODE	SendStatusRequest(void);
	};

// End of File
