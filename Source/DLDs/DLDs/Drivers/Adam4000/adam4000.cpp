#include "intern.hpp"

#include "adam4000.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series Module Master Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CAdam4000Driver);

// Constructor

CAdam4000Driver::CAdam4000Driver(void)
{
	m_Ident         = DRIVER_ID;

	CTEXT Hex[]	= "0123456789ABCDEF";

	pHex		= Hex;

}

// Destructor

CAdam4000Driver::~CAdam4000Driver(void)
{
}

// Configuration

void MCALL CAdam4000Driver::Load(LPCBYTE pData)
{
}

void MCALL CAdam4000Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
}

// Management

void MCALL CAdam4000Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
}

void MCALL CAdam4000Driver::Open(void)
{
}

// Device

CCODE MCALL CAdam4000Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop      = GetByte(pData);
			m_pCtx->m_fCheck     = GetByte(pData);
			m_pCtx->m_bDevice    = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;

		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CAdam4000Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}


// Entry Points

CCODE MCALL CAdam4000Driver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = ((m_pCtx->m_bDevice <= MODEL_4018) ? SPACE_CHANSTAT : SPACE_CONSTAT);
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
}

CCODE MCALL CAdam4000Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if( IsWriteOnly(uOffset) ) {

		return uCount;
	}

	MakeMin(uCount, 1);

	Begin(uOffset, TRUE);

	AddCommand(uOffset, NULL);

	End(uOffset);

	if( Recv(uOffset) ) {

		Addr.a.m_Type == addrRealAsReal ? GetReal(uOffset, pData) : GetRead(uOffset, pData);

		return uCount;
	}

	if( uOffset == SPACE_READSYN ) {

		pData[0] = 0;

		return uCount;
	}

	return CCODE_ERROR;

}

CCODE MCALL CAdam4000Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if( IsReadOnly(uOffset) ) {

		return uCount;
	}

	MakeMin(uCount, 1);

	Begin(uOffset, FALSE);

	AddCommand(uOffset, pData);

	End(uOffset);

	if( Recv(uOffset) ) {

		return uCount;
	}

	return CCODE_ERROR;

}

// Implementation

void CAdam4000Driver::GetRead(UINT uOffset, PDWORD pData)
{
	DWORD dData = 0;

	UINT u = FindFirst(uOffset);

	UINT uPtr = FindLast(uOffset);

	switch( uOffset ) {

		case SPACE_CONSTAT:
		case SPACE_CHANSTAT:
		case SPACE_DIGIN:
		case SPACE_DIGOUT:
		case SPACE_READSYN:
		case SPACE_RSTSTAT:
		case SPACE_CH0L:
		case SPACE_CH1L:

			for( ; u <= uPtr; u++ ) {

				dData <<= 4;

				dData += FromAscii(m_bRx[u]);
			}

			pData[0] = dData;

			return;

	}

	BOOL fNeg = FALSE;

	for( ; u <= uPtr; u++ ) {

		if( IsDigit(m_bRx[u]) ) {

			dData = 10 * dData + (m_bRx[u] - '0');
		}

		else if( m_bRx[u] == '-' ) {

			fNeg = TRUE;
		}
	}

	pData[0] = fNeg ? -dData : +dData;
}

void CAdam4000Driver::GetReal(UINT uOffset, PDWORD pData)
{
	UINT u = FindFirst(uOffset);

	UINT uPtr = FindLast(uOffset);

	DWORD dwDP = 0;

	C3REAL Real = 0.0;

	BOOL fNeg = FALSE;

	BOOL fDP = FALSE;

	for( ; u <= uPtr; u++ ) {

		if( m_bRx[u] == '-' ) {

			fNeg = TRUE;

			continue;

		}

		if( m_bRx[u] == '.' ) {

			fDP = TRUE;

			dwDP = 10;

			continue;

		}

		if( (m_bRx[u] >= '0') && (m_bRx[u] <= '9') ) {

			UINT uDigit = m_bRx[u] - '0';

			C3REAL u = uDigit;

			if( fDP ) {

				Real += (u / dwDP);

				dwDP *= 10;
			}

			else {
				Real *= 10.0;

				Real += u;
			}
		}

		if( u == uPtr ) {

			Real = fNeg ? -Real : +Real;

			pData[0] = R2I(Real);

		}
	}
}

void CAdam4000Driver::Send(void)
{
	m_pData->ClearRx();

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
}

BOOL CAdam4000Driver::Recv(UINT uOffset)
{
	if( IsBroadcast(uOffset) ) {

		while( m_pData->Read(0) < NOTHING );

		Sleep(20);

		return TRUE;
	}

	UINT uTimer = 0;

	UINT uData = 0;

	BYTE bCheck = 0;

	m_uPtr = 0;

	UINT uState = 0;

	BOOL fValid = FALSE;

	memset(m_bRx, 0, sizeof(m_bRx));

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {

		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
		}

		bCheck += (uData & 0xFF);

		switch( uState ) {

			case 0:

				if( uData == '!' || uData == '?' || uData == '>' ) {

					if( (uData == '!') || (uData == '>') ) {

						fValid = TRUE;
					}

					if( uOffset == SPACE_DIGIN || uOffset == SPACE_READSYN || (m_pCtx->m_bDevice == MODEL_4050 && uOffset == SPACE_DIGOUT) ) {

						uState = 3;
					}
					else {
						uState = uData == '>' ? 3 : 1;
					}
				}
				break;

			case 1:

				if( IsHex(uData) ) {

					m_bRx[m_uPtr++] = uData & 0xFF;

					uState = 2;
				}
				break;


			case 2:

				if( IsHex(uData) ) {

					m_bRx[m_uPtr++] = uData & 0xFF;

					if( IsDropValid(uOffset) ) {

						m_bRx[m_uPtr++] = 0;

						uState = 3;

						break;
					}
				}

				return 0;

			case 3:

				if( uData == CR ) {

					m_bRx[m_uPtr] = 0;

					if( fValid && IsCheckValid(m_uPtr, bCheck) ) {

						return TRUE;
					}
				}
				else {
					m_bRx[m_uPtr] = uData & 0xFF;

					if( ++m_uPtr == sizeof(m_bRx) ) {

						return FALSE;
					}
				}
				break;
		}
	}

	return FALSE;
}




// Frame Building

void CAdam4000Driver::Begin(UINT uOffset, BOOL fRead)
{
	m_uPtr = 0;

	m_bCheck = 0;

	switch( uOffset ) {

		case SPACE_CONFIG:

			AddByte('%');
			break;

		case SPACE_CONSTAT:
		case SPACE_VERSION:
		case SPACE_NAME:
		case SPACE_CHANSTAT:
		case SPACE_SPAN:
		case SPACE_OFFSET:
		case SPACE_READSYN:
		case SPACE_DIGIN:
		case SPACE_RSTSTAT:

			AddByte('$');
			break;

		case SPACE_SYNSAMP:
		case SPACE_CH0:
		case SPACE_CH1:
		case SPACE_CH2:
		case SPACE_CH3:
		case SPACE_CH4:
		case SPACE_CH5:
		case SPACE_CH6:
		case SPACE_CH7:
		case SPACE_CH0L:
		case SPACE_CH1L:

			AddByte('#');
			break;

		case SPACE_DIGOUT:

			fRead ? AddByte('$') : AddByte('#');
			break;

	}

	if( !IsBroadcast(uOffset) ) {

		AddByte(pHex[(m_pCtx->m_bDrop / 16) & 0x0F]);

		AddByte(pHex[(m_pCtx->m_bDrop % 16) & 0x0F]);
	}

}


void CAdam4000Driver::AddCommand(UINT uOffset, PDWORD pData)
{
	switch( uOffset ) {

		case SPACE_CONFIG:

			AddData(pData, 8);
			return;

		case SPACE_CONSTAT:

			AddByte('2');
			return;

		case SPACE_VERSION:

			AddByte('F');
			return;

		case SPACE_NAME:

			AddByte('M');
			return;

		case SPACE_SPAN:

			AddByte('0');
			return;

		case SPACE_OFFSET:

			AddByte('1');
			return;

		case SPACE_CHANSTAT:

			if( pData ) {

				AddByte('5');

				AddData(pData, 2);
				return;
			}

			AddByte('6');
			return;

		case SPACE_CH0:
		case SPACE_CH1:
		case SPACE_CH2:
		case SPACE_CH3:
		case SPACE_CH4:
		case SPACE_CH5:
		case SPACE_CH6:
		case SPACE_CH7:

			AddByte(char('0' + (uOffset - SPACE_CH0)));
			return;

		case SPACE_DIGIN:

			AddByte('6');
			return;

		case SPACE_SYNSAMP:

			AddByte('*');
			AddByte('*');
			return;

		case SPACE_READSYN:

			AddByte('4');
			return;

		case SPACE_RSTSTAT:

			AddByte('5');
			return;

		case SPACE_DIGOUT:

			if( pData ) {

				AddData(pData, 4);
			}
			else {
				AddByte('6');
			}
			return;

		case SPACE_CH0L:
		case SPACE_CH1L:

			AddByte(char('0' + (uOffset - SPACE_CH0L)));
			return;

	}
}

void CAdam4000Driver::AddData(PDWORD pItem, UINT uCount)
{
	DWORD dwItem = *pItem;

	for( UINT u = uCount; u > 0; u-- ) {

		AddByte(pHex[(dwItem/(0x01 << (4 * (u - 1)))) & 0x0F]);
	}
}

void CAdam4000Driver::End(UINT uOffset)
{
	if( m_pCtx->m_fCheck ) {

		m_bTx[m_uPtr++] = pHex[m_bCheck / 16];

		m_bTx[m_uPtr++] = pHex[m_bCheck & 0x0F];
	}

	if( !IsBroadcast(uOffset) ) {

		AddByte(CR);
	}

	Send();

}

void CAdam4000Driver::AddByte(BYTE bByte)
{
	m_bTx[m_uPtr++] = bByte;

	m_bCheck += bByte;
}

// Helpers

BOOL CAdam4000Driver::IsWriteOnly(UINT uOffset)
{
	switch( uOffset ) {

		case SPACE_DIGOUT:

			if( m_pCtx->m_bDevice == MODEL_4050 ) {

				return FALSE;
			}

		case SPACE_CONFIG:
		case SPACE_SPAN:
		case SPACE_OFFSET:
		case SPACE_SYNSAMP:


			return TRUE;
	}

	return FALSE;
}

BOOL CAdam4000Driver::IsReadOnly(UINT uOffset)
{
	switch( uOffset ) {

		case SPACE_CONSTAT:
		case SPACE_VERSION:
		case SPACE_NAME:
		case SPACE_CH0:
		case SPACE_CH1:
		case SPACE_CH2:
		case SPACE_CH3:
		case SPACE_CH4:
		case SPACE_CH5:
		case SPACE_CH6:
		case SPACE_CH7:
		case SPACE_DIGIN:
		case SPACE_READSYN:
		case SPACE_RSTSTAT:
		case SPACE_CH0L:
		case SPACE_CH1L:

			return TRUE;
	}


	return FALSE;
}

BOOL CAdam4000Driver::IsBroadcast(UINT uOffset)
{
	switch( uOffset ) {

		case SPACE_SYNSAMP:

			return TRUE;
	}

	return FALSE;
}

BOOL CAdam4000Driver::IsDropValid(UINT uOffset)
{
	switch( uOffset ) {

		case SPACE_READSYN:

			return TRUE;
	}

	if( m_bRx[0] == pHex[(m_pCtx->m_bDrop / 16) & 0x0F] ) {

		if( m_bRx[1] ==	pHex[(m_pCtx->m_bDrop % 16) & 0x0F] ) {

			return TRUE;
		}
	}

	return FALSE;
}


BOOL CAdam4000Driver::IsCheckValid(UINT uPtr, BYTE bCheck)
{
	if( !m_pCtx->m_fCheck ) {

		return TRUE;
	}

	BYTE bCheckReply;

	bCheck -= BYTE(CR + m_bRx[uPtr - 1] + m_bRx[uPtr - 2]);

	bCheckReply  = BYTE(FromAscii(m_bRx[uPtr - 2]) * 16);

	bCheckReply += BYTE(FromAscii(m_bRx[uPtr - 1]));

	m_bRx[uPtr - 1] = 0;

	m_bRx[uPtr - 2] = 0;

	return(bCheck == bCheckReply);
}

BOOL CAdam4000Driver::IsDigit(BYTE bByte)
{
	return(bByte >= '0' && bByte <= '9');
}

BOOL CAdam4000Driver::IsHex(BYTE bByte)
{
	return(bByte >= '0' && bByte <= '9') || (bByte >= 'A' && bByte <= 'F');
}

UINT CAdam4000Driver::FromAscii(BYTE bByte)
{
	UINT uResult = 0;

	if( (bByte >= 0x30) && (bByte <= 0x39) ) {

		uResult = (bByte - 0x30) & 0xFF;
	}
	else if( (bByte >= 65) && (bByte <= 70) ) {

		uResult = (bByte - 55) & 0xFF;
	}

	return uResult;
}

UINT CAdam4000Driver::FindFirst(UINT uOffset)
{
	UINT uFirst = 3;

	if( m_pCtx->m_bDevice == MODEL_4050 ) {

		switch( uOffset ) {

			case SPACE_DIGIN:	return 2;
			case SPACE_DIGOUT:	return 0;
		}
	}

	switch( uOffset ) {

		case SPACE_CH0:
		case SPACE_CH1:
		case SPACE_CH2:
		case SPACE_CH3:
		case SPACE_CH4:
		case SPACE_CH5:
		case SPACE_CH6:
		case SPACE_CH7:
		case SPACE_DIGIN:
		case SPACE_CH0L:
		case SPACE_CH1L:

			uFirst = 0;
			break;

		case SPACE_READSYN:

			switch( m_pCtx->m_bDevice ) {

				case MODEL_4050:
				case MODEL_4051:
				case MODEL_4053:
				case MODEL_4060:

					uFirst = 1;
					break;
			}
	}

	return uFirst;
}

UINT CAdam4000Driver::FindLast(UINT uOffset)
{
	if( m_pCtx->m_bDevice == MODEL_4050 ) {

		if( uOffset == SPACE_DIGOUT ) {

			return (m_pCtx->m_fCheck ? m_uPtr - 7 : m_uPtr - 5);
		}

		if( uOffset == SPACE_DIGIN ) {

			return (m_pCtx->m_fCheck ? m_uPtr - 5 : m_uPtr - 3);
		}
	}

	return (m_pCtx->m_fCheck ? m_uPtr - 3 : m_uPtr - 1);

}

// End of File
