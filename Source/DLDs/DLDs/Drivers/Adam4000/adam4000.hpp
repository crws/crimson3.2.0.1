//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series v1 Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Adam Spaces
//

#define SPACE_CONFIG	1
#define SPACE_CONSTAT	2
#define	SPACE_VERSION	3
#define SPACE_NAME	4
#define SPACE_SPAN	5
#define SPACE_OFFSET	6
#define SPACE_CHANSTAT	7
#define	SPACE_CH0	8
#define SPACE_CH1	9
#define SPACE_CH2	10
#define SPACE_CH3	11
#define SPACE_CH4	12
#define SPACE_CH5	13
#define SPACE_CH6	14
#define SPACE_CH7	15
#define SPACE_DIGIN	16
#define SPACE_SYNSAMP	17
#define SPACE_READSYN	18
#define	SPACE_RSTSTAT	19 
#define SPACE_DIGOUT	20
#define SPACE_CH0L	21
#define SPACE_CH1L	22

//////////////////////////////////////////////////////////////////////////
//
// Adam Device Models
//

#define MODEL_4017	0
#define MODEL_4018	1
#define MODEL_4050	2
#define MODEL_4051	3
#define MODEL_4053	4
#define MODEL_4060	5
#define MODEL_4080	6


//////////////////////////////////////////////////////////////////////////
//
// Adam 4000 Series Module Master Serial Driver
//

class CAdam4000Driver : public CMasterDriver
{
	public:
		// Constructor
		CAdam4000Driver(void);

		// Destructor
		~CAdam4000Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_bDevice;
			BOOL	m_fCheck;
		};

		// Data Members

		LPCTXT		pHex;
		BYTE		m_bTx[64];
		BYTE		m_bRx[64];
		BYTE		m_bCheck;
		UINT		m_uPtr;
		CContext *	m_pCtx;


		// Implementation
		void GetRead(UINT uOffset, PDWORD pData);
		void GetReal(UINT uOffset, PDWORD pData);

		// Transport
		void Send(void);
		BOOL Recv(UINT uOffset);

		// Frame Building
		void Begin(UINT uOffset, BOOL fRead);
		void AddCommand(UINT uOffset, PDWORD pData);
		void AddData(PDWORD pData, UINT uCount);
		void End(UINT uOffset);
		void AddByte(BYTE bByte);

		// Helpers
		BOOL IsReadOnly(UINT uOffset);
		BOOL IsWriteOnly(UINT uOffset);
		BOOL IsBroadcast(UINT uOffset);
		BOOL IsDropValid(UINT uOffset);
		BOOL IsCheckValid(UINT uPtr, BYTE bCheck);
		BOOL IsDigit(BYTE bByte);
		BOOL IsHex(BYTE bByte);
		UINT FromAscii(BYTE bByte);
		UINT FindFirst(UINT uOffset);
		UINT FindLast(UINT uOffset);

};
