
#include "intern.hpp"

#include "s7ppi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// S7PPI Driver
//

// Instantiator

INSTANTIATE(CS7PPIDriver);

// Constructor

CS7PPIDriver::CS7PPIDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_bSrcAddr = 0;	

	m_wPDU_REF = 0;
	
	m_bLastFC  = 0x6C;

	m_wLastRx  = 0;

	m_Status = 0L;
	}

// Destructor

CS7PPIDriver::~CS7PPIDriver(void)
{
	}

// Configuration

void MCALL CS7PPIDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CS7PPIDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CS7PPIDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CS7PPIDriver::Open(void)
{
	}

// Device

CCODE MCALL CS7PPIDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CS7PPIDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CS7PPIDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 0x0;
	Addr.a.m_Type   = addrByteAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CS7PPIDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if ( Addr.a.m_Table == AQ ) { // can't read analog outputs

		for ( UINT q = 0; q < uCount; q++ ) {
			
			pData[q] = 0L;
			}

		return uCount;
		}

	NewRequest(MSG_READ);

	UINT uType = Addr.a.m_Type;

	BOOL fLong = IsLong(uType);

	UINT uMax = fLong ? 4 : 8;

	MakeMin( uCount, uMax);
	
	UINT uDataLen = 12 + 12 * uCount;

	UINT uParaLen =  2 + 12 * uCount;

	StartFrame(uDataLen);
		
	AddHeader(uParaLen, 0);
	
	AddParameterBlock(0x04, uCount, Addr);
	
	if( Transact() ) {

		Wait(POLL_DELAY);
		
		if( Poll(FALSE) ) {

			if( ProcessRead(pData, uType) ) {

				if( !fLong ) {

					switch( Addr.a.m_Table ) {
				
						case IW:
						case QW:
							for ( UINT i = 0; i < uCount; i++ ) {

								DWORD w;

								w = (*(pData+i) & 0xFF00 ) >> 8;

								*(pData+i) = w | ((*(pData+i) & 0xFF)<<8);
								}

							break;
						}
					}
			
				return uCount;			
				}
			}
		}
	m_Status++;

	return CCODE_ERROR;
	}

CCODE MCALL CS7PPIDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	NewRequest(MSG_WRITE);

	UINT uType = Addr.a.m_Type;

	BOOL fLong = IsLong(uType);

	UINT uMax = fLong ? 4 : 8;

	if( uType == addrWordAsWord ) {

		uMax = 1;
		}

	MakeMin( uCount, uMax );

	UINT uActual = fLong ? uCount * 2 : uCount;

	UINT uAdd = GetAddedBytes(Addr.a.m_Table);

	UINT uDataLen = 12 + 12 * uActual + 6 * uActual + ( uAdd * uActual );

	UINT uParaLen =  2 + 12 * uActual;	

	StartFrame(uDataLen);
		
	AddHeader(uParaLen, 6 * uActual + ( uAdd * uActual ));
	
	AddParameterBlock( 0x05, uActual, Addr ); 

	BOOL fSwap = FALSE;

	if( !fLong ) {
	
		switch( Addr.a.m_Table ) {
	
			case IW:
			case QW:
				fSwap = TRUE;
				break;
			default:
				break;
			}
		}

	for(UINT i = 0; i < uActual; i++) {

		AddByte( 0x00 );		//Reserved			

		AddByte( 0x04 );		//Data_Type

		AddWord((uAdd + sizeof(WORD)) * 8);
		
		DWORD dwData = pData[i];

		if( fSwap ) {

			WORD w = (dwData & 0xFF) << 8;

			dwData = w | ( (dwData & 0xFF00 ) >> 8);
			}
		
		if ( fLong ) {

			UINT uIndex = i / 2;

			dwData = ( i % 2 == 1 ) ? LOWORD(pData[uIndex]) : HIWORD(pData[uIndex]);
			}

		for( UINT u = 0; u < uAdd; u++ ) {

			AddByte(0x00);
			}

		AddWord(LOWORD(dwData));
		}
	
	if( Transact() ) {

		Wait(POLL_DELAY);

		if( Poll(FALSE) ) {

			if( ProcessWrite() ) {
				
				return uCount;
				}
			}
		}
	m_Status++;

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

// Implementation

void CS7PPIDriver::StartFrame(BYTE bLen)
{
	ClearFrame();

	LowByte(SD2);			// SD2
	
	LowByte(bLen + 3);    		// LE
	
	LowByte(bLen + 3);		// LEr
	
	LowByte(SD2);			// SD2
	
	AddByte(m_pCtx->m_bDrop);	// DA

	AddByte(m_bSrcAddr);		// SA

	AddByte(GetFC());		// FC toggle
	}

// Implementation
	
BOOL CS7PPIDriver::ProcessWrite(void)
{
	BOOL fResult = TRUE;
	
	UINT uNumberVariables = m_bRxBuff[NUM_VARS];

	BYTE *p = &m_bRxBuff[ACCESS_RESULT];
	
	for(UINT i = 0; i < uNumberVariables; i++){
	
		BYTE bAccessResult = *p++;
	
		if(bAccessResult != 0xFF) {

			fResult = FALSE;
			}
		}

	return fResult;
	}

BOOL CS7PPIDriver::ProcessRead(PDWORD pData, UINT uType)
{
	BOOL fResult =TRUE;

	BOOL fLong = IsLong(uType);

	UINT uNumberVariables = m_bRxBuff[NUM_VARS];
				
	BYTE *p = &m_bRxBuff[ACCESS_RESULT];				

	for(UINT i = 0; i < uNumberVariables; i++){

		BYTE bAccessResult = *p++;
			
		if(bAccessResult != 0xFF) {

			fResult = FALSE;
			}
		p++;				//data type
					
		WORD wNumBytes;
		
		p = GetpWord(&wNumBytes, p);

		if( uType != addrWordAsWord ) {
		
			wNumBytes /= 8;	//get byte count
			}
					
		WORD wValue; //

		DWORD dwValue;

		switch(wNumBytes){

			case 1://
			case 0://no data to be had
				dwValue = 0;	//tidy the return value up
				wValue = 0;
				
				fResult = FALSE;
				break;
						
			case 5://timer
				p+=2;
			case 3://counter
				p++;							
							
				p = GetpWord(&wValue, p);
					
				p++;	//fill byte						
				break;
						
			default:
				p = fLong ? GetpDword(&dwValue, p) : GetpWord(&wValue, p);
				break;						
			}
					
		pData[i] = fLong? dwValue : wValue;
		}
		
	return fResult;
	}

void CS7PPIDriver::NewRequest(WORD wType)
{
	m_wPDU_REF++;

	m_wMsgType = wType;
	}

void CS7PPIDriver::ClearFrame(void)
{
	m_bTxPtr = 0;
	
	m_bCheck = 0;
	}

BYTE CS7PPIDriver::GetFC(void)
{
	switch( m_bLastFC ) {

		case 0x5C:
			m_bLastFC = 0x7C;
			break;
			
		case 0x7C:
			m_bLastFC = 0x5C;
			break;
		
		default:
			m_bLastFC = 0x5C;
			break;
		}

	return m_bLastFC;
	}

void CS7PPIDriver::AddByte(BYTE bData)
{
	LowByte(bData);
	
	m_bCheck += bData;
	}
	
void CS7PPIDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));
	
	AddByte(LOBYTE(wData));
	}

void CS7PPIDriver::AddDword(DWORD dwData)
{
	AddWord(HIWORD(dwData));
	
	AddWord(LOWORD(dwData));
	}

void CS7PPIDriver::LowByte(BYTE bData)
{
	m_bTxBuff[m_bTxPtr++] = bData;
	}

void CS7PPIDriver::EndFrame(void)
{
	LowByte(m_bCheck);	// FCS

	LowByte(ED);		// ED
	}

void CS7PPIDriver::TxFrame(void)
{
	m_pData->Write(m_bTxBuff, m_bTxPtr, FOREVER);
	}

BOOL CS7PPIDriver::Transact(void)
{
	EndFrame();

	if( m_wLastRx ) {
		
		UINT uGone = GetTickCount() - m_wLastRx;
		
		if( uGone < BACKOFF_TIME ) {
		
			UINT uTime = BACKOFF_TIME - uGone;
			
			Sleep(uTime);
			}
			
		m_wLastRx = 0;
		}
	
	for( UINT n = 0; n < 4; n++ ) {

		m_pData->ClearRx();
		
		TxFrame();

		WORD w = RxData();
		
		switch( w ) {

			case RX_FRAME:
			case RX_SC:
				return TRUE;
				
			case RX_NAK:
				if( m_bNakCode == 0x02 ) {
				
					Poll(TRUE);
					
					return TRUE;
					}
					
				Sleep(500);
					
				return FALSE;
			}
		}
	
	return FALSE;
	}

void CS7PPIDriver::AddHeader(UINT uParaLen, UINT uDataLen)
{
	AddByte(0x32);			// PROT_ID
	
	AddByte(0x01);			// ROSCTR
	
	AddWord(0x0000);		// RED_ID
	
	AddWord(m_wPDU_REF);		// PDU_REF
	
	AddWord(uParaLen);		// PAR_LG

	AddWord(uDataLen);		// DAT_LG
	}

void CS7PPIDriver::AddParameterBlock(BYTE bService, UINT uCount, AREF Addr)
{
	BYTE bType = 0;
	BYTE bArea = 0;
	WORD wSubArea  = 0;

	WORD wAddress = Addr.a.m_Offset;

	UINT uType = Addr.a.m_Type;

	BOOL fLong = ( bService == 4 && IsLong(uType) ) ? TRUE : FALSE;

	switch( Addr.a.m_Table ) {

		case IW:
		
			// Input Image Register

			bType = fLong ? GetType(uType) : TC_WORD;
			bArea = AC_I;
			break;
			
		case QW:
		
			// Output Image Register

			bType = fLong ? GetType(uType) : TC_WORD;
			bArea = AC_Q;
			break;

		case VW:
		
			// Variable Memory Area
			
			bType = fLong ? GetType(uType) : TC_WORD;
			bArea = AC_V;
			wSubArea = 1;
			break;

		case MW:
		
			// Bit Memory Area
			
			bType = fLong ? GetType(uType) : TC_WORD;
			bArea = AC_M;
			break;

		case 'S':
		
			// Sequence Control Area
			
			bType = TC_WORD;
			bArea = AC_S;
			break;

		case 'P':
		
			// Special Memory Area
			
			bType = TC_WORD;
			bArea = AC_SM;
			break;

		case TM:
		
			// Timer Memory Area
			
			bType = TC_TIMER;
			bArea = AC_T;
			break;

		case CN:
		
			// Counter Memory Area
			
			bType = TC_COUNTER;
			bArea = AC_C;
			break;

		case AI:
		
			// Analog Inputs
			
			bType = TC_WORD;
			bArea = AC_AI;
			break;

		case AQ:
		
			// Analog Outputs
			
			bType = TC_WORD;
			bArea = AC_AQ;
			break;

		case 'D':
		
			// Accumulator
			
			bType = TC_WORD;
			bArea = AC_V;
			break;

		case 'H':
		
			// High Speed Counters
			
			bType = TC_HSC;
			bArea = AC_HC;
			break;
		}
	
	WORD wElements = GetElements(bType);
	
	AddByte(bService);				// SERVICE_ID
	
	AddByte(LOBYTE(uCount));			// NO_VAR
		
	for( UINT i = 0; i < uCount; i++ ) {
		
		// Variable Address

		AddByte(0x12);				// VAR_SPC
		AddByte(0x0A);				// VADDR_LG

		// ANY Pointer Variable

		AddByte(0x10);				// SYNTAX_ID	
		AddByte(bType);				// Type	
		AddWord(wElements);			// Number_Elements
		AddWord(wSubArea);			// Subarea
		AddByte(bArea);				// Area
			
		DWORD dwOffset = GetOffset(wAddress, bType);

		AddByte( (dwOffset & 0xFF0000) >> 16 );
		AddByte( (dwOffset & 0xFF00) >> 8 );
		AddByte( dwOffset & 0xFF );
			
		wAddress = NextAddress(wAddress, bType);
		}
	}

WORD CS7PPIDriver::GetElements(BYTE bType)
{
	switch( bType ){

		case TC_BOOL:		return 1;
		case TC_BYTE:		return 1;
		case TC_WORD:		return 1;
		case TC_DWORD:		return 1;
		case TC_COUNTER:	return 1;
		case TC_TIMER:		return 1;
		case TC_HSC:		return 1;

		default :		return 0;

		}
	}	

DWORD CS7PPIDriver::GetOffset(WORD wAddr, BYTE bType)
{
	switch( bType ) {

		case TC_BOOL:
			return OffBit(wAddr, 0);
		
		case TC_BYTE:
		case TC_WORD:
		case TC_DWORD:
			return OffWord(wAddr);

		case TC_COUNTER:
		case TC_TIMER:
		case TC_HSC:
			return wAddr;
		
		default:
			return wAddr;
		}
		
	}

WORD CS7PPIDriver::NextAddress(WORD wAddr, BYTE bType)
{
	switch( bType ) {

		case TC_BOOL:		return wAddr + 0;
		case TC_BYTE:		return wAddr + 1;
		case TC_WORD:		return wAddr + 2;
		case TC_DWORD:		return wAddr + 4;
		case TC_COUNTER:	return wAddr + 1;
		case TC_TIMER:		return wAddr + 1;
		case TC_HSC:		return wAddr + 1;

		default:		return wAddr + 0;

		}
	}	

void CS7PPIDriver::Wait(UINT uPeriod)
{
	Sleep(uPeriod);
	}

BOOL CS7PPIDriver::Poll(BOOL fQuick)
{
 	SetPollTimer(10000);

	while( GetPollTimer() ){

		ClearFrame();

		LowByte(SD1);

		AddByte(m_pCtx->m_bDrop);

		AddByte(m_bSrcAddr);

		AddByte(GetFC());
		
		EndFrame();
		
		TxFrame();

		WORD w = RxData();
	
		switch( w ) {

			case RX_FRAME:

				m_wLastRx = GetTickCount();

				m_wLastRx = m_wLastRx + !m_wLastRx;

				return CheckSD2Response();
				
			case RX_SC:
			case RX_NAK:

				return FALSE;
				
			case RX_NONE:

				Sleep(100);
			}
		
		if( fQuick ) break;
		}
		
	return FALSE;
	}

void CS7PPIDriver::SetPollTimer(WORD wSetting)
{
	m_wPollBase  = GetTickCount();
	
	m_wPollTimer = ToTicks(wSetting);
	}
	
WORD CS7PPIDriver::GetPollTimer(void)
{
	WORD wPassed = GetTickCount() - m_wPollBase;
	
	if( wPassed >= m_wPollTimer ) return 0;
		
	return m_wPollTimer - wPassed;
	}

WORD CS7PPIDriver::RxData(void)
{
	UINT uRxPtr    = 0;
	
	UINT uDataLen  = 0;
	
	UINT uCount    = 0;
	
	BYTE bCheckSum = 0;
	
	BOOL fOkay     = FALSE;
	
	BOOL fNAK      = FALSE;

	UINT uState    = 0;

	UINT uTimer    = 0;

	UINT uData     = 0;

	SetTimer(START_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
			
			case 0:
				if( uData == SC ) {

					return RX_SC;
					}
					
				if( uData == SD1 ) {
				
					SetTimer(FRAME_TIMEOUT);

					uState   = 3;
					
					uDataLen = 3;
					
					fNAK     = TRUE;
					}

				if( uData == SD2 ) {

					SetTimer(FRAME_TIMEOUT);
					
					uState = 1;
					}
				break;				
			
			case 1:
				uDataLen = uData;
				uCount   = 0;
				uState   = 2;
				break;
			
			case 2:
				if( ++uCount == 2 ) {
					
					uState = 3;
					
					uRxPtr = 0;
					}
				break;
			
			case 3:
				if( uRxPtr < uDataLen ) {

					m_bRxBuff[uRxPtr++] = LOBYTE(uData);
				
					bCheckSum += uData;
					}
				else {
					if( uData == bCheckSum ) {

						fOkay = TRUE;
						}

					uState = 4;
					}
				break;

			case 4:
				if( uData == ED ) {
				
					if( fOkay ) {
	
						if( fNAK ) {

							m_bNakCode = m_bRxBuff[2];
				
							return RX_NAK;
							}

						return RX_FRAME;
						}
					}
			
				return RX_ERROR;
			}
		}

	return RX_NONE;
	}

BOOL CS7PPIDriver::CheckSD2Response(void)
{
	BOOL fResult = TRUE;

	WORD wLastPDU_REF;
	
	GetpWord(&wLastPDU_REF, &m_bRxBuff[PDU_REF]);
	
	if(wLastPDU_REF != m_wPDU_REF)
		fResult = FALSE;

	if(m_bRxBuff[ROCSTR] == 0x02){		//ROCSTR
		//negative response
		fResult = FALSE;

		}
			
	if(m_bRxBuff[ROCSTR] == 0x03){		//ROCSTR
		//positive response
                fResult = TRUE;
		}

	return fResult;
	}

PBYTE CS7PPIDriver::GetpWord(PWORD pw, PBYTE b)
{
	*((BYTE*)pw) = *b++;
	
	*((BYTE*)pw+1) = *b++;
	
	return b;
	}

PBYTE CS7PPIDriver::GetpDword(PDWORD pdw, PBYTE b)
{
	*((BYTE*)pdw) = *b++;
	
	*((BYTE*)pdw+1) = *b++;	

	*((BYTE*)pdw+2) = *b++;
	
	*((BYTE*)pdw+3) = *b++;
	
	return b;
	}

BOOL  CS7PPIDriver::IsLong(UINT uType)
{
	switch( uType ) {

		case addrByteAsLong:
		case addrByteAsReal:

			return TRUE;
		}
	
	return FALSE;
	}

UINT  CS7PPIDriver::GetType(UINT uType)
{

	switch( uType ) {

		case addrByteAsLong:
		case addrByteAsReal:

			return TC_DWORD;
		}

	return TC_WORD;
	}

UINT  CS7PPIDriver::GetAddedBytes(UINT uTable)
{
	switch( uTable ) {

		case CN:
			return 1;
		case TM:
			return 3;
	
		}

	return 0;
	}

// End of File
