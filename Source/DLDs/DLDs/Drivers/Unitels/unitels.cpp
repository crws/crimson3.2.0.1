
#include "intern.hpp"

#include "unitels.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Unitelway App Master/Network Slave Driver
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CUnitelwaysDriver);

// Constructor

CUnitelwaysDriver::CUnitelwaysDriver(void)
{
	m_Ident = DRIVER_ID;

	m_pOpFrame  = &m_OpFrame;
}

// Destructor

CUnitelwaysDriver::~CUnitelwaysDriver(void)
{
}

// Configuration

void MCALL CUnitelwaysDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_ThisDrop = GetWord(pData);

		m_Category = GetWord(pData);

		m_Category = 7;

		m_fMaster  = (BOOL) (GetWord(pData));
	}
}

void MCALL CUnitelwaysDriver::CheckConfig(CSerialConfig &Config)
{
	#ifndef _M_ARM

	if( Config.m_uBaudRate < 19200 ) {

		Config.m_uFlags |= flagFastRx;
	}
	#endif

	Make485(Config, FALSE);
}

// Management

void MCALL CUnitelwaysDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CUnitelwaysHandler(m_pHelper);

	pPort->Bind(m_pHandler);
}

void MCALL CUnitelwaysDriver::Detach(void)
{
	m_pHandler->Release();
}

void MCALL CUnitelwaysDriver::Open(void)
{
}

// Device

CCODE MCALL CUnitelwaysDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_Drop = GetWord(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CUnitelwaysDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CUnitelwaysDriver::Ping(void)
{
	if( m_pHandler ) {

		m_pHandler->SetDrops(m_ThisDrop, m_pCtx->m_Drop, m_fMaster);

		if( m_pHandler->IsActive() ) return 1;
	}

	return CCODE_ERROR;
}

CCODE MCALL CUnitelwaysDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pHandler == NULL ) return CCODE_ERROR | CCODE_HARD;

	else m_pHandler->SetDrops(m_ThisDrop, m_pCtx->m_Drop, m_fMaster);

	if( Addr.a.m_Table == POLL_ONLY ) {

		return 1;
	}

	m_pOpFrame->bThisDrop = m_ThisDrop;

	m_pOpFrame->bThatDrop = m_pCtx->m_Drop;

	m_pOpFrame->uCategory = m_Category;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			if( BitRead(Addr, pData, &uCount) )
				return uCount;
			break;

		case addrByteAsByte:

			if( Addr.a.m_Table <= TPRS ) {

				if( ByteRead(Addr, pData, &uCount) )
					return uCount;
			}

			else {
				if( TCByteRead(Addr, pData, &uCount) )
					return uCount;
			}
			break;

		case addrWordAsWord:

			if( WordRead(Addr, pData, &uCount) ) {

				return uCount;
			}
			break;

		case addrLongAsLong:
		case addrRealAsReal:

			if( LongRead(Addr, pData, &uCount) )
				return uCount;
			break;
	}

	return CCODE_ERROR;
}

CCODE MCALL CUnitelwaysDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pHandler == NULL ) return CCODE_ERROR | CCODE_HARD;

	else m_pHandler->SetDrops(m_ThisDrop, m_pCtx->m_Drop, m_fMaster);

	if( Addr.a.m_Table == POLL_ONLY ) {

		return 1;
	}

	m_pOpFrame->bThisDrop = m_ThisDrop;

	m_pOpFrame->bThatDrop = m_pCtx->m_Drop;

	m_pOpFrame->uCategory = m_Category;

	if( Addr.a.m_Table == CWORD || Addr.a.m_Table == CLONG || Addr.a.m_Table == CREAL )
		return uCount;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			if( BitWrite(Addr, pData, &uCount) )
				return uCount;
			break;

		case addrByteAsByte:

			if( ByteWrite(Addr, pData, &uCount) )
				return uCount;
			break;

		case addrWordAsWord:

			if( WordWrite(Addr, pData, &uCount) )
				return uCount;

			else {
				if( m_pOpFrame->bSeg == TCSEG ) {

					if( m_bRx[6] )
						return 1; // system reject, user must retry
				}
			}
			break;

		case addrLongAsLong:
		case addrRealAsReal:

			if( LongWrite(Addr, pData, &uCount) )
				return uCount;
			break;
	}

	return CCODE_ERROR;
}

// Opcode Handlers

BOOL CUnitelwaysDriver::BitRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uCount = FormBitRead(Addr, *pCount);

	UINT uStart = Addr.a.m_Offset % 8;

	if( Transact(FALSE) ) {

		if( GetBitsResponse(pData, &uCount, uStart) ) {

			*pCount = uCount;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CUnitelwaysDriver::ByteRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	CAddress A;

	A.a.m_Table  = MBIT;
	A.a.m_Type   = addrBitAsBit;
	A.a.m_Offset = Addr.a.m_Offset * 8;

	DWORD Data[1];

	*pCount     = 1;

	UINT uCount = 1;

	if( BitRead(A, Data, &uCount) ) {

		*pData = (DWORD) (m_bRx[1]);

		return TRUE;
	}

	return FALSE;
}

BOOL CUnitelwaysDriver::TCByteRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	FormTCByteRead(Addr);

	if( !Transact(FALSE) || m_bRx[0] != (0x30 + m_pOpFrame->bOp) ) {

		return FALSE;
	}

	if( !m_bRx[7] ) {

		*pData = m_bRx[8];

		*pCount = 1;

		return TRUE;
	}

	return FALSE;
}

BOOL CUnitelwaysDriver::WordRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uCount = FormWordRead(Addr, *pCount);

	if( Transact(FALSE) ) {

		if( GetWordResponse(pData, &uCount) ) {

			*pCount = uCount;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CUnitelwaysDriver::LongRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uCount = FormLongRead(Addr, *pCount);

	if( Transact(Addr.a.m_Table == MREAL || Addr.a.m_Table == CREAL) ) {

		if( GetLongResponse(pData, &uCount) ) {

			*pCount = uCount;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CUnitelwaysDriver::BitWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	FormBitWrite(Addr);

	AddByte(pData[0] & 1 ? 1 : 0);

	Transact(FALSE);

	if( m_bRx[0] == 0xFE ) {

		*pCount = 1;

		return TRUE;
	}

	return FALSE;
}

BOOL CUnitelwaysDriver::ByteWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	if( Addr.a.m_Table != MBYTE && Addr.a.m_Table != SBYTE ) {

		return TRUE; // Read Only Byte information
	}

	CAddress A;

	A.a.m_Table   = Addr.a.m_Table == MBYTE ? MBIT : SBIT;
	A.a.m_Type    = addrBitAsBit;

	BYTE bData    = LOBYTE(LOWORD(*pData));

	*pCount       = 1;

	DWORD Data[1];

	for( UINT i = 0; i < 8; i++ ) {

		*Data = (DWORD) (bData & 1);

		bData >>= 1;

		UINT uCount   = 1;

		A.a.m_Offset  = (Addr.a.m_Offset * 8) + i;

		if( !BitWrite(A, Data, &uCount) ) {

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CUnitelwaysDriver::WordWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	if( Addr.a.m_Table == TVAL ) return TRUE; // T Value is Read Only

	UINT uCount = FormWordWrite(Addr, *pCount);

	for( UINT i = 0; i < uCount; i++ ) {

		AddData(LOWORD(pData[i]));
	}

	Transact(FALSE);

	*pCount = uCount;

	if( m_pOpFrame->bSeg == TCSEG ) {

		return (!m_bRx[6]) && (m_bRx[0] == (0x30 + m_pOpFrame->bOp));
	}

	else return m_bRx[0] == 0xFE;
}

BOOL CUnitelwaysDriver::LongWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uCount = FormLongWrite(Addr, *pCount);

	for( UINT i = 0; i < uCount; i++ ) {

		AddData(LOWORD(pData[i]));
		AddData(HIWORD(pData[i]));
	}

	Transact(FALSE);

	if( m_bRx[0] == 0xFE ) {

		*pCount = uCount;

		return TRUE;
	}

	return FALSE;
}

// Transport Layer

BOOL CUnitelwaysDriver::Transact(BOOL fWait)
{
	DoCheck();

	if( m_pHandler->PutAppData(m_bTx, m_uPtr, m_pOpFrame->Transaction) ) {

		UINT uTime = 4000;

		UINT uSize = 0;

		for( ;;) {

			uSize = sizeof(m_bRx);

			uSize = m_pHandler->GetAppData(m_bRx, uSize, uTime);

			return uSize > 1;
		}
	}

	return FALSE;
}

void CUnitelwaysDriver::DoCheck(void)
{
	UINT uScan;
	UINT uCheck = 0;

	if( m_bTx[3] == DLE ) {

		for( UINT i = m_uPtr+1; i > 3; i-- ) {

			m_bTx[i] = m_bTx[i-1];
		}

		m_uPtr++;
	}

	for( uScan = 0; uScan < m_uPtr; uScan++ ) {

		uCheck += m_bTx[uScan];
	}

	m_bTx[m_uPtr++] = LOBYTE(uCheck);
}

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Data Handler
//

// Event Codes

#define	EVENT_NULL	0
#define	EVENT_START	1
#define	EVENT_FRAME	2
#define	EVENT_TIMEOUT	3
#define	EVENT_ERROR	4
#define	EVENT_POLL_ACK	5
#define	EVENT_POLL_ACK2	6

// MAC Status Codes
#define	MS_IDLE		0x02
#define	MS_WAIT_RESP	0x03
#define MS_WAIT_ACK	0x04

// Constructor

CUnitelwaysHandler::CUnitelwaysHandler(IHelper *pHelper)
{
	StdSetRef();

	m_pHelper	= pHelper;

	m_pPort         = NULL;

	m_uRxState	= 0;

	m_uTxAppCount	= 0;

	m_uIsActive	= 0;

	m_fWaitReply	= FALSE;

	m_fNotNAK	= TRUE;

	m_bThisDrop	= 0;

	m_bThatDrop	= 0;

	m_bThisPoll	= 0;

	m_fIgnoreFrame	= FALSE;

	SpeedCheck();

	CreateEvents();

	ResetMac();
}

// Destructor

CUnitelwaysHandler::~CUnitelwaysHandler(void)
{
	m_pRxAppFlag->Release();
}

// Operations

BOOL CUnitelwaysHandler::PutAppData(PCBYTE pData, UINT uCount, BYTE bTransaction)
{
	if( m_uMacState != MS_IDLE || !IsActive() ) {

		return FALSE;
	}

	m_uTxAppCount = 0;

	m_fWaitReply  = FALSE;

	m_Transaction = bTransaction;

	memcpy(m_bTxApp, pData, uCount);

	m_pRxAppFlag->Clear();

	m_uRxAppCount = 0;

	m_uTxAppCount = uCount;

	return TRUE;
}

UINT CUnitelwaysHandler::GetAppData(PBYTE pData, UINT uCount, UINT uWait)
{
	if( m_pRxAppFlag->Wait(uWait) ) {

		UINT uData = min(uCount, m_uRxAppCount);

		if( pData && m_bRxApp[0] && m_fNotNAK ) {

			memcpy(pData, m_bRxApp, uData);
		}

		m_uRxAppCount = 0;

		return uData;
	}

//**/	AfxTrace0("\r\nBad ");

	ResetMac();

	return 0;
}

// IUnknown

HRESULT CUnitelwaysHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
}

ULONG CUnitelwaysHandler::AddRef(void)
{
	StdAddRef();
}

ULONG CUnitelwaysHandler::Release(void)
{
	StdRelease();
}

// Binding

void MCALL CUnitelwaysHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
}

void CUnitelwaysHandler::CreateEvents(void)
{
	ISyncHelper *pSync = NULL;

	m_pHelper->MoreHelp(IDH_SYNC, (void **) &pSync);

	m_pRxAppFlag = pSync->CreateAutoEvent();

	pSync->Release();
}

// Event Handlers

void MCALL CUnitelwaysHandler::OnOpen(CSerialConfig const &Config)
{
}

void MCALL CUnitelwaysHandler::OnClose(void)
{
}

void MCALL CUnitelwaysHandler::OnTimer(void)
{
	if( m_uSetTimer ) {

		m_uTimer    = m_uSetTimer;

		m_uSetTimer = 0;
	}

	if( !m_fTxEnable && m_uTimer ) {

		if( !(--m_uTimer) ) {

			Signal(EVENT_TIMEOUT);
		}
	}
}

BOOL MCALL CUnitelwaysHandler::OnTxData(BYTE &bData)
{
	if( m_fTxEnable ) {

		bData = m_bTxApp[m_uTxScan++];

//**/		AfxTrace("[%2.2x]", bData);

		if( m_uTxScan >= m_uTxPtr ) {

			m_fTxEnable = FALSE;

			m_uMacState = MS_WAIT_ACK;

			Signal(EVENT_START);
		}

		return TRUE;
	}

	return FALSE;
}

void MCALL CUnitelwaysHandler::OnTxDone(void)
{

}

void MCALL CUnitelwaysHandler::OnRxData(BYTE bData)
{
	RxByte(bData);
}

void MCALL CUnitelwaysHandler::OnRxDone(void)
{
}

void CUnitelwaysHandler::SetDrops(BYTE bThis, BYTE bThat, BOOL fMaster)
{
	m_bThisDrop = bThis;
	m_bThatDrop = bThat;
	m_fMaster   = fMaster;
}

BOOL CUnitelwaysHandler::IsActive(void)
{
	return m_uIsActive > 2;
}

// Transmit Machine

void CUnitelwaysHandler::SendData(void)
{
	m_fWaitReply   = TRUE;

	m_uResendCount = m_uTxAppCount;

	m_uTxPtr       = m_uTxAppCount;

	m_uTxAppCount  = 0;

	m_fNotNAK      = TRUE;

	m_fIgnoreFrame = FALSE;

	m_uTxScan      = 1;

	m_WaitAckCount = 0;

	m_uRxState     = 0;

	SetTimeout(DLL_REPLY_TIMEOUT);

//**/	AfxTrace("\r\n[10]");

	m_fTxEnable    = TRUE;

	m_pPort->Send(DLE);
}

// Receive Machine

void CUnitelwaysHandler::RxByte(BYTE bData)
{
//**/	if( bData == DLE && m_uRxState == 0 ) AfxTrace("\r\n"); AfxTrace("<%2.2x>", bData );

	switch( m_uRxState ) {

		case 0:
			switch( bData ) {

				case DLE:
					m_bRxCheck = 0;
					m_uRxState = 1;

					m_fIgnoreCheck = FALSE;
					break;

				case ACK:
					if( m_uMacState == MS_WAIT_ACK ) {

						m_uMacState = MS_WAIT_RESP;

						Signal(EVENT_START);
					}

					return;

				case NAK:
					if( m_uMacState == MS_WAIT_ACK ) {

						m_fNotNAK = FALSE;

						Signal(EVENT_ERROR);
					}

					return;

				default:

					return;
			}
			break;

		case 1:
			m_bThisPoll = 0;

			switch( bData ) {

				case ENQ:
					m_uRxState  = 2;
					break;

				case STX:
					m_uRxState  = 3;
					break;

				default:
					m_uRxState  = 0;
					break;
			}
			break;

		case 2:
			m_uRxState  = 0;

			m_bThisPoll = bData;

			if( bData == m_bThisDrop ) {

				DelayABit();

				Signal(EVENT_POLL_ACK);
			}

			else {
				if( bData == m_bThatDrop ) {

					if( m_uMacState == MS_WAIT_ACK ) {

						Signal(EVENT_POLL_ACK2);
					}
				}
			}

			return;

		case 3:
			m_bSourceDrop = bData;

			m_uRxState    = bData <= 8 ? 4 : 0;

			m_uRxPtr      = 0;

			break;

		case 4:
			if( bData == DLE ) {

				m_uRxState = 5;
			}

			else {
//**/				AfxTrace("\r\n");

				m_uRxEnd = bData;

				m_uRxState  = 6;
			}
			break;

		case 5:
			if( bData == DLE ) {

//**/				AfxTrace("\r\n");

				m_uRxEnd = bData;

				m_uRxState  = 6;
			}

			else {
				m_uRxState = 0;
			}

			break;

		case 6:
			if( bData == DLE ) {

				m_uRxState = 7;
			}

			else {
				HandleResponseData(bData);

				if( m_uRxPtr >= m_uRxEnd ) {

					m_uRxState = 8;
				}
			}
			break;

		case 7:
			if( bData == DLE ) {

				HandleResponseData(bData);

				m_uRxState = (m_uRxPtr >= m_uRxEnd) ? 8 : 6;
			}

			else {
				m_fIgnoreCheck = TRUE;

				m_uRxPtr     = 0;

				m_uRxState   = bData == ENQ ? 2 : bData == STX ? 3 : 0;
			}

			break;

		case 8:
			m_fIgnoreFrame = TRUE;

			BOOL fMaster;
			BOOL fSlave;

			fMaster = (m_bHead[2] == MASTERDEVICE && m_bSourceDrop == m_bThisDrop);
			fSlave  = (m_bHead[1] == m_bThisDrop  && m_bHead[3]    == m_bThatDrop);

			m_uRxState = fSlave && !m_fMaster ? 11 : 0; // PLC Master sends ACK

			if( fMaster || fSlave ) {

				if( m_fIgnoreCheck || (m_bRxCheck == bData) ) {

					if( fMaster || m_fMaster ) {

						switch( ProcessFrame() ) {

							case 0:
								m_pPort->Send(ACK);
//**/								AfxTrace("[06]");

							case 1:
								m_uMacState = MS_WAIT_RESP;
								Signal(EVENT_FRAME);
								break;

							case 2:
								m_uMacState = MS_IDLE;
								Signal(EVENT_ERROR);
								break;
						}
					}

					return;
				}

				m_pPort->Send(NAK);

//**/				AfxTrace("[15]");
			}

			return;

		case 9:
			switch( bData ) {

				case EOT:
					m_uRxState = m_bThisPoll == m_bThisDrop ? 10 : 0;

					m_uRxPtr = 0;

					return;

				case DLE:
					m_uRxState = 1;

					m_bRxCheck = DLE;

					return;
			}

			m_uRxState = 0;

			return;

		case 10:
			m_uRxState = CheckContinuation(bData);

			return;

		case 11:
			if( bData == ACK ) {

				switch( ProcessFrame() ) {

					case 0:
					case 1:
						m_uMacState = MS_WAIT_RESP;
						Signal(EVENT_FRAME);
						break;

					case 2:
						m_uMacState = MS_IDLE;
						Signal(EVENT_ERROR);
						break;
				}
			}

			else {
				m_uMacState = MS_IDLE;
			}

			m_uRxState = 0;

			return;
	}

	m_bRxCheck += bData;
}

UINT CUnitelwaysHandler::ProcessFrame(void)
{
	if( m_bHead[6] == 0xF0 ) {

		if( m_bHead[7] == m_Transaction ) {

			m_fIgnoreFrame = FALSE;

			m_uRxAppCount  = m_uRxPtr - sizeof(m_bHead);

			m_fNotNAK      = TRUE;

			return 0;
		}

		else {
			return 1; // wait for correct frame
		}
	}

	return m_bHead[0] == 0x22 && m_bHead[6] == 0xFF ? 1 : 2; // rejected=1, error=2
}

// Event Handlers

void CUnitelwaysHandler::Signal(UINT uEvent)
{
	switch( m_uMacState ) {

		case MS_IDLE:
			HandleIdle(uEvent);
			break;

		case MS_WAIT_RESP:
			HandleWaitResponse(uEvent);
			break;

		case MS_WAIT_ACK:
			HandleWaitAck(uEvent);
			break;
	}
}

void CUnitelwaysHandler::HandleIdle(UINT uEvent)
{
	switch( uEvent ) {

		case EVENT_START:

			m_uRxState = 0;

			break;

		case EVENT_POLL_ACK:

			if( IsActive() ) {

				if( !m_fWaitReply && m_uTxAppCount ) {

					SendData();

					return;
				}
			}

			SendEOT();

			if( ++m_uIsActive >= 2 ) {

				m_uIsActive = 255;
			}

			m_fTxEnable = FALSE;

			break;

		case EVENT_FRAME:

			m_uRxState = 0;

			break;

		case EVENT_TIMEOUT:

			ResetMac();

			m_fIgnoreFrame = FALSE;

			break;

		case EVENT_ERROR:

			ResetMac();

			break;
	}
}

void CUnitelwaysHandler::HandleWaitResponse(UINT uEvent)
{
	switch( uEvent ) {

		case EVENT_POLL_ACK:

			if( m_bThisDrop == m_bThisPoll ) {

				SendEOT();
			}

			m_uMacState = MS_IDLE;

			break;

		case EVENT_START:

			m_uRxState = 0;

			break;

		case EVENT_FRAME:

			if( !m_fIgnoreFrame ) {

				m_fWaitReply = FALSE;

				m_fTxEnable  = FALSE;

				m_uMacState = MS_IDLE;

				m_uRxState = 0;

				m_pRxAppFlag->Set();
			}

			break;

		case EVENT_TIMEOUT:

			ResetMac();

			break;

		case EVENT_ERROR:

			m_fWaitReply = FALSE;

			m_uRxAppCount = 1;

			m_bRxApp[0]   = 0;

			m_pRxAppFlag->Set();

			m_uRxState = 0;

			m_uMacState = MS_IDLE;

			break;
	}

	return;
}

void CUnitelwaysHandler::HandleWaitAck(UINT uEvent)
{
	switch( uEvent ) {

		case EVENT_POLL_ACK: // returned to poll without Ack received

			if( ++m_WaitAckCount >= 1 ) {

				m_uTxAppCount = m_uResendCount;

				m_fWaitReply = FALSE;

				HandleIdle(EVENT_POLL_ACK); // resend request
			}

			else {
				SendEOT();
			}

			break;

		case EVENT_START:

			m_uRxState = 0;

			SetTimeout(DLL_REPLY_TIMEOUT);

			break;

		case EVENT_POLL_ACK2:

			m_uMacState = MS_WAIT_RESP;

			SetTimeout(DLL_REPLY_TIMEOUT);

			break;

		case EVENT_FRAME:

			HandleWaitResponse(EVENT_FRAME);

			break;

		case EVENT_TIMEOUT:

			HandleWaitResponse(EVENT_TIMEOUT);

			break;

		case EVENT_ERROR:

			HandleWaitResponse(EVENT_ERROR);

			break;
	}

	return;
}

// Event Support

void CUnitelwaysHandler::ResetMac(void)
{
	m_fTxEnable  = FALSE;

	m_fWaitReply = FALSE;

	m_uRxState = 0;

	m_pRxAppFlag->Clear();

	SetTimeout(2000);

	m_uMacState = MS_IDLE;
}

void CUnitelwaysHandler::SpeedCheck(void)
{
	m_uEOTDelay = 0;

	#ifndef _M_ARM

	UINT uA = GetTickCount() + ToTicks(100);

	UINT uB = 0;

	while( GetTickCount() < uA ) uB++;

	m_uEOTDelay = uB / 200;

//**/	AfxTrace1("\r\nB=%d ", uB);

	#endif

	//**/	AfxTrace1("\r\nDly=%d ", m_uEOTDelay);
}

void CUnitelwaysHandler::DelayDecr(UINT *pData)
{
	*pData -= 1;
}

void CUnitelwaysHandler::DelayABit(void)
{
	UINT u = m_uEOTDelay;

	while( u ) {

		DelayDecr(&u);
	}
}

// General Support

void CUnitelwaysHandler::SetTimeout(UINT uTime)
{
	m_uSetTimer = ToTicks(uTime);
}

void CUnitelwaysHandler::ClearTimeout(void)
{
	m_uTimer = 0;
}

void CUnitelwaysHandler::SendEOT(void)
{
	if( m_bThisDrop == m_bThisPoll ) {

		m_pPort->Send(EOT);

//**/		AfxTrace("\r\n[04]");
	}
}

UINT CUnitelwaysHandler::CheckContinuation(BYTE bData)
{
	if( bData == STX || ((bData & 0xC0) == 0xC0) ) {

		m_bRxCheck = 0x12; // DLE + STX

		return 3;
	}

	if( bData == DLE || bData & 0x80 ) {

		m_bRxCheck = DLE;

		return 1;
	}

	if( bData == ENQ ) {

		m_bRxCheck = 0x15;

		return 2;
	}

	return 0;
}

void CUnitelwaysHandler::HandleResponseData(BYTE bData)
{
	if( m_uRxPtr < 8 ) {

		m_bHead[m_uRxPtr]    = bData;
	}

	else {
		m_bRxApp[m_uRxPtr-8] = bData;
	}

//**/	if( m_uRxPtr == 7 ) AfxTrace("\r\n* ");

	m_uRxPtr++;
}

// Timing

UINT CUnitelwaysHandler::ToTicks(UINT t)
{
	return t ? ((t < 5) ? 1 : (t / 5)) : 0;
}

// End of File
