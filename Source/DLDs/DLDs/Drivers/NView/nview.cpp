
#include "intern.hpp"

#include "nview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// N-View Master Driver
//

// Instantiator

INSTANTIATE(CNViewMasterDriver);

// Constructor

CNViewMasterDriver::CNViewMasterDriver(void)
{
	m_Ident = DRIVER_ID;

	m_pHead = NULL;

	m_pTail = NULL;
	}

// Destructor

CNViewMasterDriver::~CNViewMasterDriver(void)
{
	}

// Master Flags

WORD MCALL CNViewMasterDriver::GetMasterFlags(void)
{
	return MF_SERVICE_TASK;
	}
		
// Configuration

void MCALL CNViewMasterDriver::Load(LPCBYTE pData)
{
	m_fError = GetByte(pData);
	}
	
void MCALL CNViewMasterDriver::CheckConfig(CSerialConfig &Config)
{
	}
	
// Management

void MCALL CNViewMasterDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CNViewMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CNViewMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CCtx;

			m_pCtx->m_Mode = GetByte(pData);

			switch( GetByte(pData) ) {

				case 0:
					m_pCtx->m_FindMAC[0] = 0x00;
					m_pCtx->m_FindMAC[1] = 0x07;
					m_pCtx->m_FindMAC[2] = 0xAF;
					break;
				}

			m_pCtx->m_FindMAC[3] = GetByte(pData);
			m_pCtx->m_FindMAC[4] = GetByte(pData);
			m_pCtx->m_FindMAC[5] = GetByte(pData);
			m_pCtx->m_FindIP     = GetAddr(pData);

			strcpy(m_pCtx->m_sName, GetString(pData));

			m_pCtx->m_uTime      = GetWord(pData);
			m_pCtx->m_uLast      = 0;
			m_pCtx->m_fUsed      = FALSE;
			m_pCtx->m_uUsed	     = 0;
			m_pCtx->m_uVersion   = NOTHING;

			memset(m_pCtx->m_bHead, 0, sizeof(m_pCtx->m_bHead));

			memset(m_pCtx->m_Ports, 0, sizeof(m_pCtx->m_Ports));

			Critical(TRUE);

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			Critical(FALSE);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CNViewMasterDriver::DeviceClose(BOOL fPersist)
{
	if( FALSE && !fPersist ) {

		// NOTE -- Can't allow deletion as it breaks things.

		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CNViewMasterDriver::Ping(void)
{
	if( !m_fError || m_pCtx->m_fUsed ) {

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

void MCALL CNViewMasterDriver::Service(void)
{
	ISocket *pSock = CreateSocket(IP_RAW);

	if( pSock ) {

		IPADDR IP;

		IP.m_b1 = 224;
		IP.m_b2 = 1;
		IP.m_b3 = 1;
		IP.m_b4 = 0;

		pSock->Connect(IP, 0x8874);

		for(;;) {

			SetTimer(100);

			while( GetTimer() ) {

				CBuffer *pBuff = NULL;

				pSock->Recv(pBuff);

				if( pBuff ) {

					UINT uSize  = pBuff->GetSize();

					PBYTE pData  = pBuff->GetData();

					CCtx *pFind = FindDevice(pData);

					if( pFind ) {

						// Determine which version the frame we received.

						if( (uSize - 14) >= sizeof(CAcFrameVersion1) ) {

							pFind->m_uVersion = IntelToHost(((CAcFrameVersion1 *)(pData + 14))->m_nVersion);
							}

						Critical(TRUE);

						memcpy(pFind->m_bHead, pData, sizeof(pFind->m_bHead));

						Critical(FALSE);

						if( FindPortData(uSize, pData, pFind) ) {

							pFind->m_fUsed = TRUE;
							}

						pFind->m_uLast = GetTickCount();
						}

					pBuff->Release();

					continue;
					}

				Sleep(10);
				}

			for( CCtx *pFind = m_pHead; pFind; pFind = pFind->m_pNext ) {

				if( pFind->m_fUsed ) {

					if( GetTickCount() - pFind->m_uLast > ToTicks(pFind->m_uTime) ) {

						pFind->m_fUsed = FALSE;

						pFind->m_uUsed = 0;

						memset(m_pCtx->m_Ports, 0, sizeof(m_pCtx->m_Ports));
						}
					}
				}
			}
		}
	}

CCODE MCALL CNViewMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pCtx->m_fUsed ) {

		UINT uSlot  = Addr.a.m_Offset / 1000;

		UINT uItem  = Addr.a.m_Offset % 1000;

		for( UINT n = 0; n < uCount; n++ ) {

			DWORD Value = 0;

			UINT uVer = m_pCtx->m_uVersion;

			if( !uSlot ) {

				// Global Items

				CAcFrameVersion1 *pFrame = (CAcFrameVersion1 *) (m_pCtx->m_bHead + 14);

				switch( uItem ) {

					case 0:
						Value = m_pCtx->m_fUsed;
						break;

					case 1:
						Value = m_pCtx->m_uUsed;
						break;

					case 2:
						Value = (uVer == NOTHING) ? 0 : MotorToHost(pFrame->m_nIpAddress);	
						break;

					case 3:
						Value = (uVer == NOTHING) ? 0 : PBYTE(&pFrame->m_oRing)[1];
						break;
					}

				if( uItem >= 10 && uItem < 16 ) {

					UINT n = uItem - 10;

					Value  = m_pCtx->m_bHead[6 + n];
					}

				if( uVer != NOTHING && uItem >= 50 && uItem < 66 ) {

					UINT n = uItem - 50;

					Value  = pFrame->m_acModel[n];					
					}
				}
			else {
				// Port Items

				CPort const &Port = m_pCtx->m_Ports[uSlot - 1];

				switch( uItem ) {

					case 0:
						Value = Port.m_Type.number.chip_id;
						break;

					case 1:
						Value = Port.m_Type.number.port_id;
						break;

					case 2:
						Value = PBYTE(&Port.m_Type.state)[0];
						break;

					case 3:
						Value = Port.m_Type.state.link_status ? IntelToHost(Port.m_Data.m_nSpeedMb) : 0;
						break;
					}

				if( uItem >= 100 ) {

					UINT n = uItem - 100;

					Value  = IntelToHost(PDWORD(&Port.m_Mib)[n]);
					}
				}
	
			*pData = Value;

			pData  = pData + 1;

			uItem  = uItem + 1;
			}

		return uCount;
		}

	if( !m_fError ) {

		memset(pData, 0, sizeof(DWORD) * uCount);

		return uCount;
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE MCALL CNViewMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	// NOTE -- Eat writes without complaint.

	return uCount;
	}

// Implementation

CNViewMasterDriver::CCtx * CNViewMasterDriver::FindDevice(PBYTE pData)
{
	CAcFrameVersion1 *pFrame = (CAcFrameVersion1 *) (pData + 14);

	CCtx            *pFind  = NULL;

	for( UINT p = 0; !pFind && p < 2; p++ ) {

		for( pFind = m_pHead; pFind; pFind = pFind->m_pNext ) {

			if( p == 0 ) {

				if( pFind->m_Mode == 0 ) {

					if( !memcmp(pData + 6, pFind->m_bHead + 6, 6) ) {

						break;
						}
					}

				if( pFind->m_Mode == 1 ) {

					if( !memcmp(pData + 6, pFind->m_FindMAC, 6) ) {

						break;
						}
					}

				if( pFind->m_Mode == 2 ) {

					if( pFind->m_FindIP == pFrame->m_nIpAddress ) {

						break;
						}
					}

				if( pFind->m_Mode == 3 ) {

					if( !stricmp(pFind->m_sName, PTXT(pFrame->m_acModel)) ) {

						break;
						}
					}
				}

			if( p == 1 ) {

				if( pFind->m_Mode == 0 ) {

					if( !pFind->m_fUsed ) {

						break;
						}
					}
				}
			}
		}

	return pFind;
	}

BOOL CNViewMasterDriver::FindPortData(UINT uSize, PBYTE pData, CCtx *pFind)
{
	CAcPortFrame *pPort = NULL;

	AcPortExtDataType PortExtData;

	memset(&PortExtData, 0, sizeof(PortExtData));

	if( pFind->m_uVersion == NOTHING ) {

		pPort = (CAcPortFrame *) (pData + 14);
		}

	else if( pFind->m_uVersion == 1 ) {

		CAcFrameVersion1 *pFrame = (CAcFrameVersion1 *) (pData + 14);

		pPort = &pFrame->m_Port;
		}

	else if( pFind->m_uVersion == 3 ) {

		CAcFrameVersion3 *pFrame = (CAcFrameVersion3 *) (pData + 14);

		PortExtData = pFrame->m_oPortExtData;

		pPort = &pFrame->m_Port;
		}

	if( pPort ) {

		BYTE bPort = pPort->m_oPort.number.port_id;

		BYTE bChip = pPort->m_oPort.number.chip_id;

		BYTE bSlot = (bPort | (bChip << 4));

		Critical(TRUE);

		pFind->m_Ports[bSlot].m_Data = PortExtData;

		pFind->m_Ports[bSlot].m_Type = pPort->m_oPort;

		pFind->m_Ports[bSlot].m_Mib  = pPort->m_oMib;

		Critical(FALSE);

		MakeMax(pFind->m_uUsed, bSlot + 1);

		return TRUE;
		}

	return FALSE;
	}

// End of File
