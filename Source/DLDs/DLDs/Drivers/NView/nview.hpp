
//////////////////////////////////////////////////////////////////////////
//
// N-View Master Typedefs
//

typedef BYTE	NT_U8;
typedef WORD	NT_U16;
typedef DWORD	NT_U32;
typedef INT64   NT_U64;
typedef	UINT	NT_STATUS;
typedef	VOID	NT_VOID;
typedef UINT	port_NumberType;

//////////////////////////////////////////////////////////////////////////
//
// N-View Master Header
//

#pragma  pack(1)

#define  NTOS_PACKED

#include "autocast.h"

#pragma  pack()

//////////////////////////////////////////////////////////////////////////
//
// N-View Master Driver
//

class CNViewMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CNViewMasterDriver(void);

		// Destructor
		~CNViewMasterDriver(void);

		// Master Flags
		DEFMETH(WORD) GetMasterFlags(void);
		
		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Structures for different versions
		#pragma pack(1)

		struct CAcPortFrame
		{
			AcPortType	m_oPort;
			AcMibType	m_oMib;
			};

		struct CAcFrameVersion1
		{
			NT_U16		m_nVersion;
			NT_U32		m_nIpAddress;
			AcRingData	m_oRing;
			NT_U8		m_acModel[NT_AC_MODEL_MAX_SIZE];
			CAcPortFrame	m_Port;
			};


		struct CAcFrameVersion3
		{		
			NT_U16			m_nVersion;
			NT_U32			m_nIpAddress;
			AcRingData		m_oRing;
			NT_U8			m_acModel[NT_AC_MODEL_MAX_SIZE];
			AcPortExtDataType	m_oPortExtData;
			CAcPortFrame		m_Port;
			};

		#pragma pack()

		// Port Data
		struct CPort
		{
			AcPortExtDataType m_Data;
			AcPortType	  m_Type;
			AcMibType	  m_Mib;
			};

		// Device Config
		struct CCtx
		{
			UINT	m_Mode;
			BYTE	m_FindMAC[6];
			DWORD	m_FindIP;
			UINT	m_uTime;
			UINT	m_uLast;
			char	m_sName[32];
			BOOL	m_fUsed;
			UINT	m_uUsed;
			BYTE    m_bHead[54];
			CPort   m_Ports[64];
			UINT	m_uVersion;
			CCtx *  m_pNext;
			CCtx *  m_pPrev;
			};

		// Current Device
		CCtx * m_pCtx;

		// Data Members
		BOOL   m_fError;
		CCtx * m_pHead;
		CCtx * m_pTail;

		// Implementation
		CCtx * FindDevice(PBYTE pData);
		BOOL   FindPortData(UINT uSize, PBYTE pData, CCtx *pFind);
	};

// End of File
