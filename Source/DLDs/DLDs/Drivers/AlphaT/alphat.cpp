
#include "intern.hpp"

#include "alphat.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alpha Gear Ternary Driver
//

// Instantiator

INSTANTIATE(CAlphaTernaryDriver);

// Constructor

CAlphaTernaryDriver::CAlphaTernaryDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	CTEXT VS[]	= "adgijlmnopqrstvxz";

	m_pHex		= Hex;

	m_pVS		= VS;

	m_uWErrCt	= 0;
	}

// Destructor

CAlphaTernaryDriver::~CAlphaTernaryDriver(void)
{
	}

// Configuration

void MCALL CAlphaTernaryDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CAlphaTernaryDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CAlphaTernaryDriver::Open(void)
{
	}

// Device

CCODE MCALL CAlphaTernaryDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			InitDirectCommands();

			m_pCtx->m_uPollCount = 0;

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CAlphaTernaryDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CAlphaTernaryDriver::Ping(void)
{
	m_uWErrCt = 0;

	return GetStatus();
	}

CCODE MCALL CAlphaTernaryDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\nREAD %x %x", Addr.a.m_Table, Addr.a.m_Offset);

	if( NoReadTransmit(Addr, pData) ) return 1;

	CCODE Result = SendRead(Addr, pData);

	if( Result == 1 ) {

		if( !(m_pCtx->m_uPollCount = (m_pCtx->m_uPollCount+1) % 5) ) GetStatus();
		}

	return Result;
	}

CCODE MCALL CAlphaTernaryDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nWRITE %x %x %x", Addr.a.m_Table, Addr.a.m_Offset, *pData);

	if( NoWriteTransmit(Addr, *pData) ) return 1;

	CCODE Result = SendWrite(Addr, pData);

	if( Result == 1 ) {

		if( Addr.a.m_Table == CD ) GetStatus(); // update status on register write
		}

	return Result;
	}

// User Function
UINT MCALL CAlphaTernaryDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	if( pContext ) {

		if( uFunc < 16 ) {

			m_uPtr = 0;

			AddByte(STX);

			for( UINT i = 0; i < min(strlen(Value)-1, PEND); i++ ) AddByte(Value[i]);

			return Transact() ? GetValue(uFunc, min(8, 16 - uFunc)) : 0;
			}
		}

	return 0;
	}

// Frame Building

void CAlphaTernaryDriver::StartFrame(char op1, char op2, char op3)
{
	m_uPtr = 0;

	AddByte(STX);

	AddByte(m_pHex[m_pCtx->m_bDrop]);

	AddByte(BYTE(op1));

	if( op2 ) AddByte(BYTE(op2));

	if( op3 ) AddByte(BYTE(op3));
	}

void CAlphaTernaryDriver::AddByte(BYTE bData)
{
	if( m_uPtr < PBSZ ) {
	
		m_bTx[m_uPtr++] = bData;
		}
	}

void CAlphaTernaryDriver::EndFrame(void)
{
	while( m_uPtr <= PEND ) AddByte('0');

	BYTE ck = LOBYTE(GetSum(m_bTx, PEND));

	m_bTx[PCS1] = m_pHex[ck / 16];
	m_bTx[PCS2] = m_pHex[ck % 16];
	m_bTx[PETX] = ETX;
	}
		
// Transport Layer

BOOL CAlphaTernaryDriver::Transact(void)
{
	EndFrame();

	Put();

	return GetReply();
	}

BOOL CAlphaTernaryDriver::CheckReply(void)
{
	if( m_bRx[PRCU] != 'U' ) return FALSE;

	if( m_bTx[PTCA] != 'x' ) {

		if( m_bRx[PRCA] != m_bTx[PTCA] ) return FALSE;

		if( !CheckFuncNum() ) return FALSE;
		}

	UINT u = GetSum(m_bRx, PEND);

	if( m_pHex[u / 16]    != m_bRx[PCS1] ) return FALSE;

	return m_pHex[u % 16] == m_bRx[PCS2];
	}

BOOL CAlphaTernaryDriver::CheckFuncNum(void)
{
	BYTE b1 = m_bTx[PTFC];
	BYTE b2 = m_bTx[PTFN];

	switch( b1 ) {

		case 'R':
		case 'T':
		case 'W':
		case 'V':
			return (m_bRx[PRFC] == b1) && (m_bRx[PRFN] == b2);
		}

	return !(m_bRx[PRST] & 0x80);
	}

// Read Handlers

CCODE CAlphaTernaryDriver::SendRead(AREF Addr, PDWORD pData)
{
	switch( Addr.a.m_Table ) {

		case CD:
			StartFrame('R', '4', 0);
			break;

		case CT:
			StartFrame('T', '4', 0);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	PutNum(DWORD(Addr.a.m_Offset), PG8);

	if( Transact() ) {

		*pData    = GetValue(PRCD, 8);

		m_uWErrCt = 0;

		return 1;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CAlphaTernaryDriver::SendWrite(AREF Addr, PDWORD pData)
{
	if( !DoWriteCommand(Addr.a.m_Table, Addr.a.m_Offset, *pData) ) {

		GetStatus();

		m_uWErrCt = 0;

		return 1;
		}

	if( Transact() ) {

		m_uWErrCt = 0;

		switch( m_bRx[PRFC] ) {

			case 'W':
			case 'T':
			case 'V':
				return 1;

			default:
				HandleStatus();

				return 1;
			}
		}

	if( m_uWErrCt++ > 2 ) {

		m_uWErrCt = 0;

		return 1;
		}

	return (Addr.a.m_Table == WAX) ? 1 : CCODE_ERROR;
	}

BOOL CAlphaTernaryDriver::DoWriteCommand(UINT uTable, UINT uOffset, DWORD dData)
{
	DWORD dQ2Q4 = dData ? 1 : 0;

	switch( uTable ) {

		case CW:    StartFrame('W', '4', 0); PutNum(dData, PG8);	return TRUE;
		case CQ1C:  StartFrame('Q', '1', 0); PutCommon();		return TRUE;
		case CQ1P:  StartFrame('Q', '1', 0); PutPoint(dData);		return TRUE;
		case CQ2:   StartFrame('Q', '2', 0); PutQ2Q4(dData);		return TRUE;
		case CQ3C:  StartFrame('Q', '3', 0); PutCommon();		return TRUE;
		case CQ3P:  StartFrame('Q', '3', 0); PutPoint(dData);		return TRUE;
		case CQ4:   StartFrame('Q', '4', 0); PutQ2Q4(dData);		return TRUE;
		case CV5C:  StartFrame('V', '5', 0); PutCommon();		return TRUE;
		case CV5P:  StartFrame('V', '5', 0); PutPoint(dData);		return TRUE;
		case CV6C:  StartFrame('V', '6', 0); PutCommon();		return TRUE;
		case CV6P:  StartFrame('V', '6', 0); PutPoint(dData);		return TRUE;

		case CDS: return DoSLCCommand(LOWORD(dData));

		case WAX: // special transmit sequence
			StartFrame(m_pHex[m_pCtx->m_xn], '0', '0');

			m_bTx[PTCA] = 'x'; // replace axis address with command x

			PutNum(m_pCtx->m_xs, PG8);

			return TRUE;

		case CD: // Double Command
			StartFrame('T', '4', 0); // Set Write Address

			PutNum(uOffset, PG8);

			AddByte('0');

			return Transact() ? DoWriteCommand(CW, 0, dData) : FALSE;
		}

	return FALSE;
	}

void CAlphaTernaryDriver::PutCommon(void)
{
	PutNum(BCOMMON, PG2);
	PutNum(BCOMMON, PG2);
	}

void CAlphaTernaryDriver::PutPoint(DWORD dData)
{
	PutNum(BPOINT, PG2);
	PutNum( dData, PG2);
	}

void CAlphaTernaryDriver::PutQ2Q4(DWORD dData)
{
	PutNum(dData ? BPOINT : BCOMMON, PG2);
	}

BOOL CAlphaTernaryDriver::DoSLCCommand(UINT uData)
{
	char c = GetValidSLC(uData);

	if( c ) {

		PDWORD p = SelectDirect(uData);

		if( !p ) return FALSE;

		StartFrame(c, 0, 0);

		switch( c ) {

			case 'a':
			case 'i':
			case 'j':
			case 'm':
				PutNum(*p, PG8);
			case 'n':
			case 't':
				return TRUE;

			case 's':
				AddByte('0');
				AddByte('0');
			case 'd':
			case 'o':
			case 'r':
				PutNum(*p, PG2);
				return TRUE;

			case 'g':
				AddByte('0');
				PutNum(m_pCtx->m_g, PG2);
				return TRUE;

			case 'l':
				PutNum(m_pCtx->m_l[0], PG2);
				PutNum(m_pCtx->m_l[1], PG2);
				return TRUE;

			case 'p':
				AddByte('t');
				AddByte('r');
				AddByte('w');
				PutNum(m_pCtx->m_p, PG2);
				return TRUE;

			case 'q':
				AddByte(m_pHex[*p]);
				return TRUE;

			case 'v':
				AddByte(m_pCtx->m_v[0]);
				PutNum( m_pCtx->m_v[1], PG4);
				PutNum( m_pCtx->m_v[2], PG4);
				return TRUE;

			case 'x':
				m_bTx[PTCA] = 'x'; // Change Address to Generic
				AddByte('s');
				PutNum( m_pCtx->m_x, PG1 );
				return TRUE;

			case 'z':
				PutNum( m_pCtx->m_z[0], PG8);
				AddByte(m_pCtx->m_z[1]);
				return TRUE;
			}
		}

	return FALSE;
	}

CCODE CAlphaTernaryDriver::GetStatus(void)
{
	DoSLCCommand(UINT('n'));

	if( Transact() ) {

		HandleStatus();
		return 1;
		}

	return CCODE_ERROR;
	}

// Helpers
BOOL CAlphaTernaryDriver::NoReadTransmit(AREF Addr, PDWORD pData)
{
	switch( Addr.a.m_Table ) {

		case CQ1C:
		case CQ3C:
		case CV5C:
		case CV6C:
		case WAX:	*pData = 0;		return TRUE;

		case CQ1P:
		case CQ2:
		case CQ3P:
		case CQ4:
		case CV5P:
		case CV6P:	*pData = 0xFFFFFFFF;	return TRUE;

		case CW:
		case CDS:	return TRUE; // don't change data

		case CST:	*pData = m_Stat;		return TRUE;
		case CAL:	*pData = m_Alarm;		return TRUE;
		case CPI:	*pData = m_PI;			return TRUE;
		case CPO:	*pData = m_PO;			return TRUE;

		case CL2:	*pData = m_pCtx->m_l[1];	return TRUE;
		case CV2:	*pData = m_pCtx->m_v[1];	return TRUE;
		case CV3:	*pData = m_pCtx->m_v[2];	return TRUE;
		case CZ2:	*pData = m_pCtx->m_z[1];	return TRUE;

		case AXN:	*pData = m_pCtx->m_xn;		return TRUE;
		case AXS:	*pData = m_pCtx->m_xs;		return TRUE;

		case CDC:
			PDWORD p;

			p = SelectDirect(Addr.a.m_Offset);

			if( p ) *pData = *p;

			return TRUE;
		}

	return FALSE;
	}

BOOL CAlphaTernaryDriver::NoWriteTransmit(AREF Addr, DWORD dData)
{
	PDWORD pItem = NULL;

	switch( Addr.a.m_Table ) {

		case CQ1C:
		case CQ3C:
		case CV5C:
		case CV6C:
		case WAX:	return !dData;

		case CQ1P:
		case CQ3P:
		case CV5P:
		case CV6P:	return dData > POINTMAX; // point number range is 0-15

		case CST:
		case CAL:
		case CPI:
		case CPO:	return TRUE;

		case CL2:	m_pCtx->m_l[1] = dData;	return TRUE;
		case CV2:	m_pCtx->m_v[1] = dData;	return TRUE;
		case CV3:	m_pCtx->m_v[2] = dData;	return TRUE;
		case CZ2:	m_pCtx->m_z[1] = dData; return TRUE;

		case CDS:	return !GetValidSLC(LOWORD(dData));

		case AXS:	m_pCtx->m_xs = dData;	return TRUE;

		case AXN:
			if( dData <= POINTMAX ) m_pCtx->m_xn = LOBYTE(LOWORD(dData));

			return TRUE;

		case CDC:

			PDWORD p;

			p = SelectDirect(Addr.a.m_Offset);

			if( p ) {

				if( Addr.a.m_Offset == 'x' && dData > DEVMAX ) dData = m_pCtx->m_bDrop;

				*p = dData;
				}

			return TRUE;
		}

	return FALSE;
	}

void CAlphaTernaryDriver::HandleStatus(void)
{
	m_Stat  = GetValue( PRST, 2 );
	m_Alarm	= GetValue( PRAL, 2 );
	m_PI	= GetValue( PRPI, 2 );
	m_PO	= GetValue( PRPO, 2 );
	}

void CAlphaTernaryDriver::PutNum(DWORD dData, DWORD dFactor)
{
	while( dFactor ) {

		AddByte( m_pHex[(dData/dFactor) % 16] );

		dFactor /= 16;
		}
	}

DWORD CAlphaTernaryDriver::GetValue(UINT RxPos, UINT uCt)
{
	UINT uD = 0;
	UINT uP = 0;

	BYTE bVal;

	while( (uP < uCt) && GetHex(m_bRx[RxPos + uP], &bVal) ) {

		uD = (uD << 4) + bVal;

		uP++;
		}

	return uD;
	}

BOOL CAlphaTernaryDriver::GetHex(BYTE bData, PBYTE pbVal)
{
	if( bData >= '0' && bData <= '9' ) {

		*pbVal = bData - '0';
		return TRUE;
		}

	if( bData >= 'A' && bData <= 'F' ) {

		*pbVal = bData - '7';
		return TRUE;
		}

	if( bData >= 'a' && bData <= 'f' ) {

		*pbVal = bData - 'W';
		return TRUE;
		}

	return FALSE;
	}

UINT CAlphaTernaryDriver::GetSum(PBYTE pSrc, UINT uCt)
{
	BYTE b = 0;

	for( UINT i = 1; i <= uCt; i++ ) b += pSrc[i];

	return 256 - b;
	}

char CAlphaTernaryDriver::GetValidSLC(UINT uSel)
{
	BYTE bCmd = uSel | ('a' - 'A'); // make lower

	if( bCmd >= 'a' && bCmd <= 'z' ) {

		for( UINT i = 0; m_pVS[i]; i++ ) {

			if( bCmd == BYTE(m_pVS[i]) ) return char(bCmd);
			}
		}

	return 0;
	}

void CAlphaTernaryDriver::InitDirectCommands(void)
{
	if( m_pCtx->m_DirectMagic == 0x12345678 ) return;

	m_pCtx->m_a 	= 0;
	m_pCtx->m_d 	= 0;
	m_pCtx->m_g 	= 0;
	m_pCtx->m_i 	= 0;
	m_pCtx->m_j 	= 0;
	m_pCtx->m_l[0] 	= 0;
	m_pCtx->m_l[1] 	= 0;
	m_pCtx->m_m 	= 0;
	m_pCtx->m_n 	= 0;
	m_pCtx->m_o 	= 0;
	m_pCtx->m_p 	= 0;
	m_pCtx->m_q 	= 0;
	m_pCtx->m_r 	= 0;
	m_pCtx->m_s 	= 0;
	m_pCtx->m_t 	= 0;
	m_pCtx->m_v[0] 	= 0;
	m_pCtx->m_v[1] 	= 0;
	m_pCtx->m_v[2]	= 0;
	m_pCtx->m_x	= 0;
	m_pCtx->m_z[0]	= 0;
	m_pCtx->m_z[1]	= 0;

	m_pCtx->m_xn	= 0;
	m_pCtx->m_xs	= 0;

	m_pCtx->m_DirectMagic = 0x12345678;
	}

PDWORD CAlphaTernaryDriver::SelectDirect(UINT uSel)
{
	switch( uSel ) {

		case 'a':  return &m_pCtx->m_a;	break;
		case 'd':  return &m_pCtx->m_d;	break;
		case 'g':  return &m_pCtx->m_g;	break;
		case 'i':  return &m_pCtx->m_i;	break;
		case 'j':  return &m_pCtx->m_j;	break;
		case 'l':  return  m_pCtx->m_l;	break;
		case 'm':  return &m_pCtx->m_m;	break;
		case 'n':  return &m_pCtx->m_n;	break;
		case 'o':  return &m_pCtx->m_o;	break;
		case 'p':  return &m_pCtx->m_p;	break;
		case 'q':  return &m_pCtx->m_q;	break;
		case 'r':  return &m_pCtx->m_r;	break;
		case 's':  return &m_pCtx->m_s;	break;
		case 't':  return &m_pCtx->m_t;	break;
		case 'v':  return  m_pCtx->m_v;	break;
		case 'x':  return &m_pCtx->m_x;	break;
		case 'z':  return  m_pCtx->m_z;	break;
		}

	return NULL;
	}

// Port Access
void CAlphaTernaryDriver::Put(void)
{
	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < PBSZ; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_pData->Write( m_bTx, PBSZ, FOREVER );
	}

BOOL CAlphaTernaryDriver::GetReply(void)
{
	UINT uState   = 0;
	UINT uTimeout = 1000;
	UINT uPtr     = 0;

	UINT uData;

	SetTimer(uTimeout);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (uData = Get(uTimeout)) < NOTHING ) {

			BYTE bData  = LOBYTE(uData);

			m_bRx[uPtr] = bData;

//**/			AfxTrace1("<%2.2x>", bData);

			switch( uState ) {

				case 0:
					if( bData == STX ) uState = 1;
					else uPtr = 0;
					break;

				case 1:
					if( bData == ETX ) return CheckReply();
					break;
				}

			uPtr++;

			if( bData == ETX || uPtr >= sizeof(m_bRx) ) return FALSE;
			}
		}

	return FALSE;
	}

UINT CAlphaTernaryDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
