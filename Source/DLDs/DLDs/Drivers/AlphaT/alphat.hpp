
//////////////////////////////////////////////////////////////////////////
//
// Alpha Gear Ternary Driver
//

#define	PBSZ	16	// Size of Transmit/Receive String Buffer

class CAlphaTernaryDriver : public CMasterDriver
{
	public:
		// Constructor
		CAlphaTernaryDriver(void);

		// Destructor
		~CAlphaTernaryDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Open(void);
		
		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);

		// User Function
		DEFMETH(UINT)	DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			DWORD	m_DirectMagic;
			DWORD	m_a;
			DWORD	m_d;
			DWORD	m_g;
			DWORD	m_i;
			DWORD	m_j;
			DWORD	m_l[2];
			DWORD	m_m;
			DWORD	m_n;
			DWORD	m_o;
			DWORD	m_p;
			DWORD	m_q;
			DWORD	m_r;
			DWORD	m_s;
			DWORD	m_t;
			DWORD	m_v[3];
			DWORD	m_z[2];
			DWORD	m_x;
			BYTE	m_xn;
			DWORD	m_xs;
			UINT	m_uPollCount;
			};

		// Data Members
		CContext *m_pCtx;

		LPCTXT	m_pHex;
		LPCTXT	m_pVS;
		BYTE	m_bTx[PBSZ];
		BYTE	m_bRx[PBSZ];
		UINT	m_uPtr;
		UINT	m_uWErrCt;

		// Cached Data Members
		DWORD	m_Stat;
		DWORD	m_Alarm;
		DWORD	m_PI;
		DWORD	m_PO;
		
		// Frame Building
		void StartFrame(char op1, char op2, char op3);
		void AddByte(BYTE bData);
		void EndFrame(void);

		// Transport Layer
		BOOL Transact(void);
		BOOL CheckReply(void);
		BOOL CheckFuncNum(void);
		
		// Read Handlers
		CCODE SendRead(AREF Addr, PDWORD pData);

		// Write Handlers
		CCODE SendWrite(AREF Addr, PDWORD pData);
		BOOL  DoWriteCommand(UINT uTable, UINT uOffset, DWORD dData);
		void  PutCommon(void);
		void  PutPoint(DWORD dData);
		void  PutQ2Q4(DWORD dData);
		BOOL  DoSLCCommand(UINT uData);
		CCODE GetStatus(void);

		// Helpers
		BOOL   NoReadTransmit( AREF Addr, PDWORD pData);
		BOOL   NoWriteTransmit(AREF Addr,  DWORD dData);
		void   HandleStatus(void);
		void   PutNum(DWORD dValue, DWORD dFactor);
		DWORD  GetValue(UINT RxPos, UINT uCt);
		BOOL   GetHex(BYTE bData, PBYTE pbVal);
		UINT   GetSum(PBYTE pSrc, UINT uCt);
		char   GetValidSLC(UINT uSel);
		void   InitDirectCommands(void);
		PDWORD SelectDirect(UINT uSel);
		
		// Port Access
		void Put(void);
		BOOL GetReply(void);
		UINT Get(UINT uTime);

	};

#define	CD	1  // Read/Write Register
#define	CST	2  // Internal Status Flag (R/O)
#define	CAL	3  // Current Alarm/Warning Code (R/O)
#define	CPI	4  // Monitored Input PIO Signal Status (R/O)
#define	CPO	5  // Monitored Output PIO Signal Status (R/O)
#define	CQ1C	6  // Non-Volatile Common -> Window Area
#define	CQ1P	7  // [Non-Volatile Point] -> Window Area
#define	CQ2	8  // Window Area -> Execution (INFO2)
#define	CQ3C	9  // Non-Volatile Common -> Execution
#define	CQ3P	10 // [Non-Volatile Point] -> Execution
#define	CQ4	11 // Execution to Window (INFO2)
#define	CV5C	12 // Window Area Common -> Non-Volatile
#define	CV5P	13 // Window Area Point -> [Non-Volatile]
#define	CV6C	14 // Execution Common -> Non-Volatile
#define	CV6P	15 // Execution Point -> [Non-Volatile]
#define	CDS	16 // Direct Command [Direct Command Letter]
#define	CDC	17 //   [DC_ Parameter 1] for 'a'-'z' selection
#define	CL2	18 //   [DC_ Parameter l2 (moving current limit)]
#define	CV2	19 //   [DC_ Parameter v2 (velocity)]
#define	CV3	20 //   [DC_ Parameter v3 (acceleration)]
#define	CZ2	21 //   [DC_ Parameter z2 (direction selector)]
#define	WAX	22 // Change Axis Address via AXN and AXS
#define	AXN	23 //   [New Axis Address] (INFO5)
#define	AXS	24 //   [Serial Number of unit] (INFO5)

// Reserved
#define	CT	98 // Set Register Address for Write
#define	CW	99 // Write to Register set by T command

// Bank Numbers
#define	BCOMMON	0
#define	BPOINT	1

// Point
#define	POINTMAX	15

// Device Address
#define	DEVMAX	15

// Transmit/Receive Buffer Constants
// Both
#define	PEND	12	// Last Byte of Data
#define	PCS1	13	// Checksum Char 1
#define	PCS2	14	// Checksum Char 2
#define	PETX	15	// ETX

// Transmit Positions
#define	PTCA	1	// Axis Address
#define	PTFC	2	// Function Character
#define	PTFN	3	// Function Number
#define	PTDS	4	// Transmit Data Start

// Receive Positions
#define	PRCU	1	// 'U' = Receive Ack
#define	PRCA	2	// Axis Address
#define	PRFC	3	// Function Character
#define PRFN	4	// RWTV Function Number
#define	PRST	4	// Status char 1 for Direct Response
#define	PRCD	5	// RWTV Data Start
#define	PRAL	6	// Alarm char 1 for Direct Response
#define	PRPI	8	// PI char 1 for Direct Response
#define	PRPO	10	// PO char 1 for Direct Response

// Put Generic
#define	PG1	0x1
#define	PG2	0x10
#define	PG4	0x1000
#define	PG8	0x10000000

// End of File
