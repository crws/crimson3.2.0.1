
#ifndef	INCLUDE_RWISS_HPP

#define	INCLUDE_RWISS_HPP

/////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CRWISSDriver;

//////////////////////////////////////////////////////////////////////////
//
// Comms Characters
//

#define NEW_MSG_CHAR          0x21  // '!'
#define ACK_CHAR              0x25  // '%'
#define NCK_CHAR              0x2A  // '*'

//////////////////////////////////////////////////////////////////////////
//
// Commmand Codes
//

#define CC_OPEN_LEFT          0x30
#define CC_OPEN_RIGHT         0x31
#define CC_STOP               0x32
#define CC_MOVE_LEFT          0x33
#define CC_MOVE_RIGHT         0x34
#define CC_MOVE_REQ           0x35
#define CC_MOVE_REQ_OK        0x36
#define CC_MOVE               0x37
#define CC_MOVE_DONE          0x38
#define CC_ALL_CLEAR          0x39
#define CC_ALARM              0x3A
#define CC_CAR_SLOW           0x3B
#define CC_CAR_HALT           0x3C
#define CC_CAR_MAX            0x3D
#define CC_AISLE_STATUS       0x3E
#define CC_AISLE_PRESENCE     0x3F
#define CC_AISLE_MOVEMENT     0x40
#define CC_STATIONARY         0x41
#define CC_PARK_MODE          0x42
#define CC_NIGHT_PARK         0x43
#define CC_FIRE_PARK          0x44
#define CC_PARK_OPEN          0x45
#define CC_DRIVE_MODE         0x46
#define CC_AUXIN_FUNC         0x47
#define CC_AUXOUT_FUNC        0x48
#define CC_SET_STOP_DIST      0x49
#define CC_SET_SLOW_DIST      0x4A
#define CC_SET_OPEN_DIST      0x4B
#define CC_SET_MOTOR_TO       0x4C
#define CC_SET_LIGHT_TO       0x4D
#define CC_SET_RASL           0x4E
#define CC_SET_LASL           0x4F
#define CC_SECURITY_ACCESS    0x50
#define CC_CAR_STATE          0x51
#define CC_PSB_STATE          0x52
#define CC_PSB_CONFIG         0x53
#define CC_SYS_CONFIG         0x54
#define CC_PING               0x55
#define CC_CAR_NUMBER         0x56
#define CC_TOTAL_CARS         0x57
#define CC_CONNECT            0x58
#define CC_DISCONNECT         0x59
#define CC_DEBUG              0x5A
#define CC_SYS_LOCK           0x5B
#define CC_ENABLE             0x5C
#define CC_WARNING            0x5D
#define CC_DRIVE_LEFT         0x5E
#define CC_MOTOR_CURRENT      0x5F
#define CC_RIGHT_CONFIG       0x60
#define CC_LEFT_CONFIG        0x61
#define CC_ALARM_STATUS_1     0x62
#define CC_ALARM_STATUS_2     0x63
#define CC_CONFIG_ERR_1       0x64
#define CC_COMMS_ERR          0x65
#define CC_UART_ERR           0x66
#define CC_AISLE_STATE        0x67
#define CC_STOPPER_SIZE       0x68
#define CC_READ_DIST          0x69
#define CC_RESET_FLAGS        0x6A
#define CC_MAX_MOVE_LIMIT     0x6B
#define CC_RESET_USI          0x6C
#define CC_LEFT_SECURE        0x6D
#define CC_RESET_TO_DEFAULT   0x6E
#define CC_CONFIG_ERR_2       0x6F
#define CC_DRIVE_RIGHT        0x70
#define CC_SAFETY_SWEEP       0x71
#define CC_DEL_USER           0x72
#define CC_NEW_USER_1         0x73
#define CC_NEW_USER_2         0x74
#define CC_NEW_USER_3         0x75
#define CC_USER_SEC_1         0x76
#define CC_USER_SEC_2         0x77
#define CC_USER_SEC_3         0x78
#define CC_USER_PIN_1         0x79
#define CC_USER_PIN_2         0x80
#define CC_USER_PIN_3         0x81
#define CC_USER_AA_1          0x82
#define CC_USER_AA_2          0x83
#define CC_USER_AA_3          0x84
#define CC_USER_AA_4          0x85
#define CC_USER_AA_5          0x86
#define CC_MAX_MOTOR_DUTY     0x87
#define CC_RESET_SENSORS      0x88

//////////////////////////////////////////////////////////////////////////
//
// Richards - Wilcox Intelligent Stroage System Driver
//

class CRWISSDriver : public CMasterDriver
{
	public:
		// Constructor
		CRWISSDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);		

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		//DEFMETH(CCODE) Ping (void);
		DEFMETH(void)  Service (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Config Data

	protected:
		// Cache
		struct CCache {

			BYTE	m_bData;
			BOOL	m_fValid;
			};
		// Data
		UINT	m_uTimeout;
		CCache	m_Cache[255];

		// Cache
		void Kill(void);
		void Read(UINT uIndex, CCache &Data);
		void Save(UINT uIndex, CCache  Data);

		// Implementation
		BOOL Send(UINT  uCode, UINT  uData, UINT  uSrce, UINT  uDest);
		BOOL Recv(UINT &uCode, UINT &uData, UINT &uSrce, UINT &uDest);

		void  Sniff(void);
		PTXT CmdCode(UINT uCode);

		// Port Access
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);
	};

// End of File

#endif
