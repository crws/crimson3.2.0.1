
#include "intern.hpp"

#include "rwiss.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Richards - Wilcox Intelligent Stroage System Driver
//

// Instantiator

INSTANTIATE(CRWISSDriver);

// Constructor

CRWISSDriver::CRWISSDriver(void)
{
	m_Ident       = DRIVER_ID;
	
	m_uTimeout    = 200;
	}

// Configuration

void MCALL CRWISSDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {
		
		}
	}

void MCALL CRWISSDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}

// Device

CCODE MCALL CRWISSDriver::DeviceOpen(IDevice *pDevice)
{
	Kill();
	
	return CMasterDriver::DeviceOpen(pDevice);
	}

CCODE MCALL CRWISSDriver::DeviceClose(BOOL fPersist)
{
	Kill();

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

void MCALL CRWISSDriver::Service(void)
{
	if( FALSE ) {

		Sniff();

		return;
		}

	UINT uCode = 0;
	
	UINT uData = 0;

	UINT uSrce = 0;

	UINT uDest = 0;

	if( Recv(uCode, uData, uSrce, uDest) ) {

		if( uDest && 0x80 ) {
			
			CCache Data;

			Data.m_fValid = TRUE;

			Data.m_bData  = uData;

			Save(uCode, Data);
			}
		else {
			PTXT p = CmdCode(uCode);

			/*AfxTrace("%s\n", p);*/

			free(p);
			}

		switch( uCode ) {
			
			case CC_PING:

				Kill();

				if( Send(CC_CONNECT, 0, 0, 1) ) {
					
					}

				break;
			
			default:
				
				break;
			}
		}
	}

CCODE MCALL CRWISSDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uCode = Addr.a.m_Offset;
	
	UINT uData = 0;

	CCache Data;

	Read(uCode, Data);

	if( Data.m_fValid ) {
		
		pData[0] = Data.m_bData;
		
		return uCount;
		}
	else
		return uCount;		
		
	return CCODE_ERROR | CCODE_NO_DATA;
	}

CCODE MCALL CRWISSDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uCode = Addr.a.m_Offset;

	UINT uData = pData[0];

	UINT uSrce;
	
	UINT uDest;

	PTXT p = CmdCode(uCode);
	
	//AfxTrace("write, code %s data %2.2X\n", p, uData);

	free(p);

	if( Send(uCode, uData, 0, 1) ) {

		return uCount;
		}		
	
	return CCODE_ERROR;
	}

// Implementation

BOOL CRWISSDriver::Send(UINT uCode, UINT uData, UINT uSrce, UINT uDest)
{
	TxByte(NEW_MSG_CHAR);

	TxByte(BYTE(uCode));
	TxByte(BYTE(uData));
	TxByte(BYTE(uSrce));
	TxByte(BYTE(uDest));

	TxByte(NEW_MSG_CHAR + uCode + uData + uSrce + uDest);

	/*AfxTrace("\n");*/
	
	UINT uRepCode;
	
	UINT uRepData;

	UINT uRepDest;

	UINT uRepSrce;
	
	return Recv(uRepCode, uRepData, uRepSrce, uRepDest);
	}

BOOL CRWISSDriver::Recv(UINT &uCode, UINT &uData, UINT &uSrce, UINT &uDest)
{
	UINT  uState = 0;
	
	BYTE  bCheck = 0;
	
	SetTimer(m_uTimeout);

	while( GetTimer() ) {

		UINT uRead;
		
		if( (uRead = RxByte(5)) < NOTHING ) {

			BYTE bByte = uRead;

			/*AfxTrace("<%2.2X>", bByte);*/

			switch( uState ) {
				
				case 0:
					if( bByte == NEW_MSG_CHAR ) {
						
						uState ++;

						bCheck = NEW_MSG_CHAR;
						}

					if( bByte == ACK_CHAR ) {

						return TRUE;
						}

					if( bByte == NCK_CHAR ) {

						return FALSE;
						}

					break;
				
				case 1:
					uState ++;

					bCheck += bByte;

					uCode   = bByte;

					break;
				
				case 2:
					uState ++;

					bCheck += bByte;

					uData   = bByte;

					break;
				
				case 3:
					uState ++;

					bCheck += bByte;

					uSrce   = bByte;
					
					break;
				
				case 4:
					uState ++;

					bCheck += bByte;

					uDest   = bByte;
					
					break;
				
				case 5:
					if( bByte == bCheck ) {

						TxByte(ACK_CHAR);
						
						return TRUE;
						}
					else {
						TxByte(NCK_CHAR);
						
						return FALSE;
						}
					
				default:
					return FALSE;
				}
			}
		}

	return FALSE;
	}

// Cache

void CRWISSDriver::Kill(void)
{
	memset(m_Cache, 0, sizeof(m_Cache));
	}

void CRWISSDriver::Read(UINT uIndex, CCache &Data)
{
	Data = m_Cache[uIndex];
	}

void CRWISSDriver::Save(UINT uIndex, CCache  Data)
{
	PTXT pCode = CmdCode(uIndex);

	free(pCode);

	m_Cache[uIndex] = Data;
	}

void CRWISSDriver::Sniff(void)
{
/*	UINT uCode = 0xFF;

	UINT uData = 0xFF;

	UINT uDest = 0xFF;

	UINT uState = 0;

	BYTE bCheck = 0;
	
	SetTimer(m_uTimeout);

	while( GetTimer() ) {

		UINT uRead;
		
		if( (uRead = RxByte(5)) < NOTHING ) {

			UINT bByte = uRead;

			AfxTrace("<%2.2X>", bByte);

			switch( uState ) {
				
				case 0:
					if( bByte == NEW_MSG_CHAR ) {
						
						uState ++;

						bCheck = NEW_MSG_CHAR;
						}

					if( bByte == ACK_CHAR ) {

						AfxTrace("ACK\n");
						}

					if( bByte == NCK_CHAR ) {

						AfxTrace("\n");
						}

					break;
				
				case 1:
					uState ++;

					bCheck += bByte;

					uCode   = bByte;

					break;
				
				case 2:
					uState ++;

					bCheck += bByte;

					uData   = bByte;

					break;
				
				case 3:
					uState ++;

					bCheck += bByte;

					break;
				
				case 4:
					uState ++;

					bCheck += bByte;
					
					uDest   = bByte;
					
					break;
				
				case 5:
					uState = 0;
					
					if( bByte == bCheck ) {
						
						}
					else {
						}						

					break;
					
				default:
					AfxTrace("\n");
				}
			}
		}
*/	}

// Debug

PTXT CRWISSDriver::CmdCode(UINT uCode)
{
	char sText[32] = { 0 };
	
	switch( uCode ) {
		
		case CC_OPEN_LEFT:		SPrintf(sText, "%s", "Open Left");	break;
		case CC_OPEN_RIGHT:		SPrintf(sText, "%s", "Open Right");	break;
		case CC_STOP:			SPrintf(sText, "%s", "Stop");		break;
		case CC_MOVE_LEFT:		SPrintf(sText, "%s", "Move Left");	break;
		case CC_MOVE_RIGHT:		SPrintf(sText, "%s", "Move Right");	break;
		//case CC_MOVE_REQ           0x35
		//case CC_MOVE_REQ_OK        0x36
		//case CC_MOVE               0x37
		//case CC_MOVE_DONE          0x38
		//case CC_ALL_CLEAR          0x39
		case CC_ALARM:			SPrintf(sText, "%s", "Alarm");		break;
		//case CC_CAR_SLOW           0x3B
		//case CC_CAR_HALT           0x3C
		//case CC_CAR_MAX            0x3D
		case CC_AISLE_STATUS:		SPrintf(sText, "%s", "Aisle Status");	break;
		//case CC_AISLE_PRESENCE     0x3F
		//case CC_AISLE_MOVEMENT     0x40
		case CC_STATIONARY:		SPrintf(sText, "%s", "Stationary");	break;
		//case CC_PARK_MODE          0x42
		//case CC_NIGHT_PARK         0x43
		//case CC_FIRE_PARK          0x44
		//case CC_PARK_OPEN          0x45
		case CC_DRIVE_MODE:		SPrintf(sText, "%s", "Drive Mode");	break;
		//case CC_AUXIN_FUNC         0x47
		//case CC_AUXOUT_FUNC        0x48
		//case CC_SET_STOP_DIST      0x49
		//case CC_SET_SLOW_DIST      0x4A
		//case CC_SET_OPEN_DIST      0x4B
		//case CC_SET_MOTOR_TO       0x4C
		//case CC_SET_LIGHT_TO       0x4D
		//case CC_SET_RASL           0x4E
		//case CC_SET_LASL           0x4F
		//case CC_SECURITY_ACCESS    0x50
		case CC_CAR_STATE:		SPrintf(sText, "%s", "Car State");	break;
		case CC_PSB_STATE:		SPrintf(sText, "%s", "PSB State");	break;
		case CC_PSB_CONFIG:		SPrintf(sText, "%s", "PSB Config");	break;
		case CC_SYS_CONFIG:		SPrintf(sText, "%s", "Sys Config");	break;
		case CC_PING:			SPrintf(sText, "%s", "Ping");		break;
		//case CC_CAR_NUMBER         0x56
		case CC_TOTAL_CARS:		SPrintf(sText, "%s", "Total cars");	break;
		case CC_CONNECT:		SPrintf(sText, "%s", "Connect");	break;
		case CC_DISCONNECT:		SPrintf(sText, "%s", "Disconnenct");	break;
		//case CC_DEBUG              0x5A
		//case CC_SYS_LOCK           0x5B
		case CC_ENABLE:			SPrintf(sText, "%s", "Enable");		break;
		//case CC_WARNING            0x5D
		case CC_DRIVE_LEFT:		SPrintf(sText, "%s", "Drive Left");	break;
		case CC_MOTOR_CURRENT:		SPrintf(sText, "%s", "Moto Current");	break;
		case CC_RIGHT_CONFIG:		SPrintf(sText, "%s", "Right Config");	break;
		case CC_LEFT_CONFIG:		SPrintf(sText, "%s", "Left Config");	break;
		case CC_ALARM_STATUS_1:		SPrintf(sText, "%s", "Arlarm Status 1");break;
		case CC_ALARM_STATUS_2:		SPrintf(sText, "%s", "Arlarm Status 2");	break;
		//case CC_CONFIG_ERR_1       0x64
		//case CC_COMMS_ERR          0x65
		//case CC_UART_ERR           0x66
		//case CC_AISLE_STATE        0x67
		//case CC_STOPPER_SIZE       0x68
		//case CC_READ_DIST          0x69
		//case CC_RESET_FLAGS        0x6A
		//case CC_MAX_MOVE_LIMIT     0x6B
		//case CC_RESET_USI          0x6C
		//case CC_LEFT_SECURE        0x6D
		//case CC_RESET_TO_DEFAULT   0x6E
		//case CC_CONFIG_ERR_2       0x6F
		case CC_DRIVE_RIGHT:		SPrintf(sText, "%s", "Drive Right");		break;
		//case CC_SAFETY_SWEEP       0x71
		//case CC_DEL_USER           0x72
		//case CC_NEW_USER_1         0x73
		//case CC_NEW_USER_2         0x74
		//case CC_NEW_USER_3         0x75
		//case CC_USER_SEC_1         0x76
		//case CC_USER_SEC_2         0x77
		//case CC_USER_SEC_3         0x78
		//case CC_USER_PIN_1         0x79
		//case CC_USER_PIN_2         0x80
		//case CC_USER_PIN_3         0x81
		//case CC_USER_AA_1          0x82
		//case CC_USER_AA_2          0x83
		//case CC_USER_AA_3          0x84
		//case CC_USER_AA_4          0x85
		//case CC_USER_AA_5          0x86
		//case CC_MAX_MOTOR_DUTY     0x87
		//case CC_RESET_SENSORS      0x88

		default:			SPrintf(sText, "<%2.2X>", uCode);	break;
		}

	

	return strdup(sText);
	}

// Port Access

void CRWISSDriver::TxByte(BYTE bData)
{
	/*AfxTrace("[%2.2X]", bData);*/

	m_pData->Write(bData, FOREVER);
	}

UINT CRWISSDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
