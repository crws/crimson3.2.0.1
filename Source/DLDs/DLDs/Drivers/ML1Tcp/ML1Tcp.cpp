
#include "ml1tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 TCP/IP Implementation
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CML1TcpDriver);

// Constructor

CML1TcpDriver::CML1TcpDriver(void)
{
	m_Ident	  = DRIVER_ID;	

	m_fTrack  = TRUE;

	m_uKeep	  = 0;

	m_fClient = TRUE;
	}

// Destructor

CML1TcpDriver::~CML1TcpDriver(void)
{
	}

// Device

CCODE MCALL CML1TcpDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pTcp = (CML1Tcp *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pTcp  = new CML1Tcp;

			m_pDev = m_pTcp;
		
			m_pTcp->m_PingReg    = GetWord(pData);

			m_pTcp->m_TO	     = GetWord(pData);

			m_pTcp->m_Retry      = GetByte(pData);

			m_pTcp->m_Blocks     = GetWord(pData);

			// Server and Unit ID's should always be initialized at 0 and is set at authentication.

			m_pDev->m_Server = 0;

			m_pDev->m_Unit   = 0;

			// At present, ML1 is a point to point protocol with the Server ID residing on the 
			// device level, likewise, the Unit ID resides on the driver level.  This 
			// implementation (Server ID and UINT ID residing on both levels) is maintained
			// in event that ML1 evolves to a multi drop protocol!

			InitDev();

			pData		  = LoadBlocks(m_pExtra, pData);

			m_pTcp->m_bIpSel  = GetByte(pData);

			m_pTcp->m_IP1     = GetAddr(pData);

			m_pTcp->m_pAddr   = GetString(pData);
			
			m_pTcp->m_uPort	  = GetWord(pData);

			m_pTcp->m_fKeep	  = GetByte(pData);

			m_pTcp->m_fPing	  = GetByte(pData);

			m_pTcp->m_uTime1  = GetLong(pData);

			m_pTcp->m_uTime2  = GetWord(pData);

			m_pTcp->m_uTime3  = GetWord(pData);

			m_pTcp->m_IP2	  = GetAddr(pData);

			m_pTcp->m_fClose  = FALSE;

			m_pTcp->m_fRemote = FALSE;

			GetIP(m_pTcp);

			InitTcp();

			InitSpecial();

			AfxListAppend(m_pHead, m_pTail, m_pTcp, m_pNext, m_pPrev);

			pDevice->SetContext(m_pTcp);

			for(UINT s = 0; s < m_pTcp->m_bSockets; s++ ) {

				OpenListener(&m_pTcp->m_pListen[s]);
				}

			return CCODE_SUCCESS;
			}
		
		return CCODE_ERROR | CCODE_HARD;
		}

	m_pDev = m_pTcp;

	return CCODE_SUCCESS;
	}

CCODE MCALL CML1TcpDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pTcp->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(m_pTcp, FALSE);
			}
		}
	else {
		CloseSocket(m_pTcp, FALSE);

		for( UINT s = 0; s < m_pTcp->m_bSockets; s++ ) {

			CloseListener(&m_pTcp->m_pListen[s]);
			}

		delete m_pTcp->m_pClient;

		m_pTcp->m_pClient = NULL;

		delete [] m_pTcp->m_pListen;

		m_pTcp->m_pListen = NULL;

		RemoveBlocks();

		AfxListRemove(m_pHead, m_pTail, m_pTcp, m_pNext, m_pPrev);

		m_pTcp = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CML1TcpDriver::Ping(void)
{
	if( m_pTcp->m_fPing ) {
		
		DWORD IP;

		if( !m_pTcp->m_fAux ) {

			IP = m_pTcp->m_IP1;
			}
		else {
			IP = m_pTcp->m_IP2;
			}
		
		if( CheckIP(IP, m_pTcp->m_uTime2) == NOTHING ) {
			
			if( m_pTcp->m_IP2 ) {

				m_pTcp->m_fAux = !m_pTcp->m_fAux;
				}

			return CCODE_ERROR; 
			}
		}

	return CML1Driver::Ping();
	}

void CML1TcpDriver::InitSpecial(void)
{
	CML1Blk * pBlk = FindSpecial(m_pDev);

	if( pBlk ) {

		for( UINT u = SR_MIN; u < SR_MAX; u++ ) {

			switch( u - SR_MIN ) {

				case srIP1:	SetIP(m_pTcp, pBlk, m_pTcp->m_IP1);	u++;		break;
				
				case srPort:	pBlk->SetRxWord(u - SR_MIN, m_pTcp->m_uPort);		break;

				case srICMP:	pBlk->SetRxWord(u - SR_MIN, m_pTcp->m_fPing);		break;

				case srTime1:	pBlk->SetRxWord(u - SR_MIN, m_pTcp->m_uTime1);		break;

				case srTime2:	pBlk->SetRxWord(u - SR_MIN, m_pTcp->m_uTime2);		break;
				}
			}
		}

	CML1Base::InitSpecial();
	}

void CML1TcpDriver::SetSr(CML1Dev * pDev, UINT& uOffset, PWORD pData)
{
	if( IsSrBase(uOffset) ) {

		return CML1Base::SetSr(pDev, uOffset, pData);
		}

	CML1Tcp * pTcp = (CML1Tcp *)pDev;

	if( pTcp ) {

		CML1Blk * pBlock = FindSpecial(pTcp);

		if( pBlock ) {

			if( IsSrLong(uOffset) ) {

				if( uOffset == srIP1 ) {

					DWORD Data = PU4(pData)[0];

					if( pTcp->m_IP1 != Data ) {

						if( SetIP(pTcp, pBlock, Data) ) {

							pTcp->m_fClose  = TRUE;

							pTcp->m_fRemote = TRUE;
							}
						}
					}

				uOffset++;

				return;
				}

			WORD x = SHORT(MotorToHost(PU2(pData)[0]));

			pBlock->SetRxWord(uOffset, x);

			if( uOffset == srPort ) {

				if( pTcp->m_uPort  !=  x ) {

					pTcp->m_uPort  = pData[0];

					pTcp->m_fClose = TRUE;
					}

				return;
				}
	
			if( uOffset == srICMP ) {

				pTcp->m_fPing = x;

				return;
				}

			if( uOffset == srTime1 ) {

				pTcp->m_uTime1 = x;

				return;
				}

			if( uOffset == srTime2 ) {

				pTcp->m_uTime2 = x;

				return;
				}
			}
		}
	}

void CML1TcpDriver::AddSpecial(CML1Dev * pDev, BYTE bCount)
{
	CML1Base::AddSpecial(pDev, bCount);

	CML1Blk * pBlk = FindSpecial(pDev);

	CML1Tcp * pTcp = (CML1Tcp *)pDev;

	if( pBlk && pTcp ) {

		BYTE bBase = CML1Base::GetSpecialExtra();

		if( bCount > bBase ) {

			bCount -= bBase;

			for( UINT u = srIP1, c = 0; c < bCount; u++, c++ ) {

				switch( u ) {

					case srIP1:	AddLong(MotorToHost(pTcp->m_IP1)); u++;	break;
				
					case srPort:	AddWord(pTcp->m_uPort);			break;

					case srICMP:	AddWord(pTcp->m_fPing);			break;

					case srTime1:	AddWord(pTcp->m_uTime1);		break;

					case srTime2:	AddWord(pTcp->m_uTime2);		break;
					}
				}
			}
		}
	}

BYTE CML1TcpDriver::GetSpecialExtra(void)
{
	return 5 + CML1Base::GetSpecialExtra();
	}

// Authentication Support

BOOL CML1TcpDriver::DoPostAuthentication(CML1Dev * pDev)
{
	CML1Tcp * pTcp = (CML1Tcp *)pDev;

	if( pTcp ) {

		if( pTcp->m_fClose ) {

			CloseSocket(pTcp, FALSE);

			CloseListener(pTcp->m_pListen);

			pTcp->m_fClose = FALSE;
			}
		}

	return TRUE;
	}

// Socket Management

BOOL CML1TcpDriver::CheckSocket(CML1Tcp * pCtx, SOCKET * pSock, BOOL fListen)
{
	if( fListen ) {

		return CheckListener(pSock);
		}

	return CheckSocket(pCtx);
	}


// Client Socket Management

BOOL CML1TcpDriver::CheckSocket(CML1Tcp * pCtx)
{
	if( pCtx ) {

		if( pCtx->m_pClient->m_pSocket ) {

			if( !pCtx->m_fDirty ) {

				UINT Phase;

				pCtx->m_pClient->m_pSocket->GetPhase(Phase);

				if( Phase == PHASE_ERROR ) {

					CloseSocket(pCtx, TRUE);

					return FALSE;
					}

				if( Phase == PHASE_CLOSING ) {

					CloseSocket(pCtx, FALSE);

					return FALSE;
					}

				return TRUE;
				}

			CloseSocket(pCtx, FALSE);

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CML1TcpDriver::OpenSocket(CML1Tcp * pCtx)
{
	if( CheckSocket(pCtx) ) {

		return TRUE;
		}

	if( pCtx->m_fDirty ) {

		pCtx->m_fDirty = FALSE;

		pCtx->m_fAux   = FALSE;

		return FALSE;
		}

	if( !pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - pCtx->m_uLast;

		UINT tt = ToTicks(pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (pCtx->m_pClient->m_pSocket = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   Port;

		GetIP(pCtx);

		if( !pCtx->m_fAux ) {

			IP   = (IPADDR const &) pCtx->m_IP1;

			Port = WORD(pCtx->m_uPort);
			}
		else {
			IP   = (IPADDR const &) pCtx->m_IP2;

			Port = WORD(pCtx->m_uPort);
			}

		if( pCtx->m_pClient->m_pSocket->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(pCtx->m_uTime2);

			while( GetTimer() ) {

				UINT Phase;

				pCtx->m_pClient->m_pSocket->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					if( ML1_DEBUG ) {

						AfxTrace("\nTarget IP is %2.2x%2.2x%2.2x%2.2x %u %8.8x %8.8x", IP.m_b1,IP.m_b2,IP.m_b3,IP.m_b4, Port, pCtx->m_IP1, pCtx->m_IP2);

						pCtx->m_pClient->m_pSocket->GetRemote(IP, Port);

						AfxTrace("\nOpen rem %2.2x%2.2x%2.2x%2.2x %u", IP.m_b1,IP.m_b2,IP.m_b3,IP.m_b4, Port);

						pCtx->m_pClient->m_pSocket->GetLocal(IP, Port);

						AfxTrace(" loc %2.2x%2.2x%2.2x%2.2x %u", IP.m_b1,IP.m_b2,IP.m_b3,IP.m_b4, Port);
						}

					pCtx->m_pClient->m_uIdle = GetTickCount();

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					pCtx->m_pClient->m_pSocket->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			if( pCtx->m_IP2 ) {

				pCtx->m_fAux = !pCtx->m_fAux;
				}

			CloseSocket(pCtx, TRUE);

			return FALSE;
			}

		if( pCtx->m_IP2 ) {

			pCtx->m_fAux = !pCtx->m_fAux;
			}

		return FALSE;
		}

	return FALSE;
	}

void CML1TcpDriver::CloseSocket(CML1Tcp * pCtx, BOOL fAbort)
{
	if( pCtx->m_pClient->m_pSocket ) {

		if( fAbort )
			pCtx->m_pClient->m_pSocket->Abort();
		else
			pCtx->m_pClient->m_pSocket->Close();

		pCtx->m_pClient->m_pSocket->Release();

		pCtx->m_pClient->m_pSocket = NULL;

		pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

void CML1TcpDriver::CheckIdle(CML1Tcp * pCtx)
{
	if( IsTimedOut(pCtx->m_pClient->m_uIdle, pCtx->m_uTime1) ) {

		CloseSocket(pCtx, FALSE);
		}
	}

// Server Socket Management

BOOL CML1TcpDriver::CheckListener(SOCKET * pSock)
{
	UINT Phase;

	if( pSock ) {

		if( pSock->m_pSocket ) {

			pSock->m_pSocket->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				return TRUE;
				}

			if( Phase == PHASE_ERROR ) {

				CloseListener(pSock);

				return OpenListener(pSock);
				}

			if( Phase == PHASE_CLOSING ) {

				pSock->m_pSocket->Close();

				return FALSE;
				}

			return FALSE;
			}

		OpenListener(pSock);
		}

	return FALSE;
	}

BOOL CML1TcpDriver::OpenListener(SOCKET * pSock)
{
	if( pSock ) {
		
		if( !pSock->m_pSocket ) { 

			pSock->m_pSocket = CreateSocket(IP_TCP);
			}
				
		if( pSock->m_pSocket ) {

			if( pSock->m_pSocket->Listen(m_pTcp->m_uPort) == S_OK ) {

				pSock->m_uIdle = GetTickCount();

				return TRUE;
				}
			}
		}
	
	return FALSE;
	}

void CML1TcpDriver::CloseListener(SOCKET * pSock)
{
	if( pSock && pSock->m_pSocket) {

		pSock->m_pSocket->Close();

		pSock->m_pSocket->Release();

		pSock->m_pSocket = NULL;
		}
	}

void CML1TcpDriver::CheckSilent(CML1Tcp * pCtx, SOCKET * pSock)
{
	if( IsTimedOut(pSock->m_uIdle, pCtx->m_uTime1) ) {

		CloseListener(pSock);

		OpenListener(pSock);
		}
	}

// Implementation

void CML1TcpDriver::InitTcp(void)
{
	if( m_pTcp ) {

		m_pTcp->m_uLast	   = GetTickCount();

		m_pTcp->m_fAux	   = FALSE;

		m_pTcp->m_fDirty   = FALSE;

		m_pTcp->m_bSockets = 1;
		
		m_pTcp->m_pClient  = new SOCKET;

		m_pTcp->m_pListen  = new SOCKET[m_pTcp->m_bSockets];

		m_pTcp->m_pClient->m_pSocket = NULL;

		m_pTcp->m_pListen->m_pSocket = NULL;
		}
	}


BOOL CML1TcpDriver::TxData(CML1Tcp * pCtx, SOCKET * pSock, BOOL fRespond)
{
	if( pSock ) {

		if( fRespond && !CheckSocket(pCtx, pSock, fRespond) ) {

			return FALSE;
			}

		if( !IsAuthentication(NULL, m_pTx) && !IsAuthenticationReq(m_pTx) ) {

			m_pTx[5] = BYTE(m_uTxPtr - 6);
			}
			
		UINT uSize   = m_uTxPtr;

		if( pSock->m_pSocket->Send(m_pTx, uSize) == S_OK ) {

			if( uSize == m_uTxPtr ) {

				if( ML1_DEBUG ) {

					AfxTrace("\nTx %u : ", m_uTxPtr);

					for( UINT u = 0; u < uSize; u++ ) {

						AfxTrace("%2.2x ", m_pTx[u]);
						}
					}
					
				return TRUE;
				}
			}
		}
			
	return FALSE;
	}

BOOL CML1TcpDriver::RxData(CML1Tcp * pCtx, SOCKET * pSock, BOOL fListen)
{
	if( pSock ) {

		SetTimer(pCtx->m_uTime2);

		UINT uPtr = 0;

		UINT uSize = 0;

		BOOL fRx   = FALSE;

		BOOL fContinue = FALSE;

		if( ML1_DEBUG ) {

			AfxTrace("\nRx %u : ", pCtx->m_uTime2);
			}

		do {
			uSize = sizeof(m_pRx) - uPtr;

			pSock->m_pSocket->Recv(m_pRx + uPtr, uSize);

			if( uSize || uPtr >= 6 + 6 ) {

				if( ML1_DEBUG ) {

					for( UINT u = uPtr; u < uPtr + uSize; u++ ) {

						AfxTrace("%2.2x ", m_pRx[u]);
						}
					}
					
				uPtr += uSize;
				
				if ( uPtr >= 6 + 6 ) {

					 UINT uTotal = FindRxSize();

					if( uPtr >= uTotal ) {

						for( UINT u = 0; u < uTotal; u++ ) {

							pSock->m_pBuff[u] = m_pRx[u];
							}
						
						fRx = CML1Server::HandleFrame(pSock->m_pBuff);

						if( uPtr -= uTotal ) {

							for( UINT n = 0; n < uPtr; n++ ) {

								m_pRx[n] = m_pRx[uTotal++];
								}

							fContinue = TRUE;

							continue;
							}

						return fRx;
						}
					}

				fContinue = FALSE;
				
				continue;
				}

			if( !fListen && !CheckSocket(pCtx, pSock, fListen) ) {

				return FALSE;
				}
			
			Sleep(10);
			
			} while (fContinue || (!fListen && GetTimer()));
		}
	
	return FALSE;
	}

// Transport 

BOOL CML1TcpDriver::Send(void)
{
	CML1Tcp * pCtx = (CML1Tcp *)FindContext();

	if( pCtx ) {

		if( OpenSocket(pCtx) ) {

			BOOL fTx = TxData(pCtx, pCtx->m_pClient, FALSE);

			if( fTx ) {

				pCtx->m_pClient->m_uIdle = GetTickCount();
				}

			return fTx;
			}
		}

	return FALSE;
	}

BOOL CML1TcpDriver::Recv(void)
{
	BOOL fRx = FALSE;

	CML1Tcp * pDev = (CML1Tcp *)m_pHead;

	while( pDev ) {

		if( CheckSocket(pDev) ) {

			if( RxData(pDev, pDev->m_pClient, FALSE) ) {

				pDev->m_pClient->m_uIdle = GetTickCount();

				fRx = TRUE;

				continue;
				}

			if( !IsAuthentication(NULL, m_pRx) ) {

				CheckIdle(pDev);
				}
			}

		pDev = (CML1Tcp *)pDev->m_pNext;
		}
		
	return fRx;
	}

BOOL CML1TcpDriver::Listen(void)
{
	CML1Tcp * pDev = (CML1Tcp *)m_pHead;

	while( pDev ) {

		for( UINT s = 0; s < pDev->m_bSockets; s++ ) {

			if( CheckListener(&pDev->m_pListen[s]) ) {

				if( RxData(pDev, &pDev->m_pListen[s], TRUE) ) {

					pDev->m_pListen[s].m_uIdle = GetTickCount();

					return TRUE;
					}

				if( !IsAuthentication(NULL, m_pTx) ) {

					CheckSilent(pDev, &pDev->m_pListen[s]);
					}
				}
			}

		pDev = (CML1Tcp *)pDev->m_pNext;
		}
			
	return FALSE;
	}

BOOL CML1TcpDriver::Respond(PBYTE pBuff)
{
	CML1Tcp * pCtx = (CML1Tcp *)DoListen(GetID(pBuff), GetUnitID(pBuff));

	if( pCtx ) {

		if( OpenSocket(pCtx) ) {

			return TxData(pCtx, pCtx->m_pClient, FALSE);
			}
		}

	return FALSE;
	}

BOOL CML1TcpDriver::Respond(CML1Dev * pDev)
{
	CML1Tcp * pCtx = (CML1Tcp *) pDev;

	if( pCtx ) {

		for( UINT s = 0; s < m_pTcp->m_bSockets; s++ ) {

			if( TxData(pCtx, &pCtx->m_pListen[s], TRUE) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Transport Helpers

UINT CML1TcpDriver::FindRxSize(void)
{
	if(IsAuthentication(NULL, m_pRx) ) {

		switch( m_pRx[0] ) {

			case authPush:	return 18 + GetSpecialBytes(GetSpecialCount(m_pRx));

			case authAck:	return 18 + GetSpecialBytes(GetSpecialCount(m_pRx));

			case authPoll:	return  7;

			case authResp:	return 18 + GetSpecialBytes(GetSpecialCount(m_pRx));
			}
		}
	
	return m_pRx[5] + 6;
	}

PVOID CML1TcpDriver::FindContext(void)
{
	return m_pDev;
	}

// Helpers

BOOL CML1TcpDriver::SetIP(CML1Tcp * pTcp, CML1Blk * pBlock, DWORD dwData)
{
	if( pTcp && pBlock ) {

		DWORD dwIP = MotorToHost(dwData);

		pBlock->SetRxWord(srIP1 + 0, HIWORD(dwIP));

		pBlock->SetRxWord(srIP1 + 1, LOWORD(dwIP));

		pTcp->m_IP1 = dwData;

		return TRUE;
		}

	return FALSE;
	}

void CML1TcpDriver::GetIP(CML1Tcp * pTcp)
{
	if( !pTcp->m_fRemote ) {

		if( m_pTcp->m_bIpSel == 1 ) {

			DWORD dwIP = m_pExtra->GetDnsAddress(pTcp->m_pAddr, 0);

			if( dwIP != pTcp->m_IP1 ) {

				pTcp->m_IP1 = dwIP;

				InitSpecial();
				}
			}
		}

	if( ML1_DEBUG ) {

		AfxTrace("\nIP addr %8.8x rem %u", pTcp->m_IP1, pTcp->m_fRemote);
		}
	}

// End of File
