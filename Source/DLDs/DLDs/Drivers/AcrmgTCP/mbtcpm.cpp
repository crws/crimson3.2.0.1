
#include "intern.hpp"

#include "mbtcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver
//

// Constructor

CModbusTCPMaster::CModbusTCPMaster(void)
{
	m_uKeep     = 0;

	m_uMaxWords = 120;
	
	m_uMaxBits  = 2000;
	}

// Destructor

CModbusTCPMaster::~CModbusTCPMaster(void)
{
	}

// Configuration

void MCALL CModbusTCPMaster::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CModbusTCPMaster::Attach(IPortObject *pPort)
{
	}

void MCALL CModbusTCPMaster::Open(void)
{
	}

// Device

CCODE MCALL CModbusTCPMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP1		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_fDisable15	= GetByte(pData);
			m_pCtx->m_fDisable16	= GetByte(pData);
			m_pCtx->m_fDisable5	= GetByte(pData);
			m_pCtx->m_fDisable6	= GetByte(pData);
			m_pCtx->m_PingReg	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->m_uMax01	= GetWord(pData);
			m_pCtx->m_uMax02	= GetWord(pData);
			m_pCtx->m_uMax03	= GetWord(pData);
			m_pCtx->m_uMax04	= GetWord(pData);
			m_pCtx->m_uMax15	= GetWord(pData);
			m_pCtx->m_uMax16	= GetWord(pData);
			m_pCtx->m_fDirty	= FALSE;
			m_pCtx->m_fAux          = FALSE;

			#if DRIVER_ID == 0x3505

			m_pCtx->m_IP2		= GetAddr(pData);
			m_pCtx->m_fNoReadEx	= GetByte(pData);
			m_pCtx->m_fFlipLong	= GetByte(pData);
			m_pCtx->m_fFlipReal	= GetByte(pData);

			#else

			m_pCtx->m_IP2		= 0;
			m_pCtx->m_fNoReadEx	= FALSE;
			m_pCtx->m_fFlipLong	= FALSE;
			m_pCtx->m_fFlipReal	= FALSE;

			#endif

			Limit(m_pCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pCtx->m_uMax15, 1, m_pCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pCtx->m_uMax16, 1, m_pCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CModbusTCPMaster::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CModbusTCPMaster::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 || uFunc == 5 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		if( uFunc == 1  ) {

			pCtx->m_IP1 = MotorToHost(dwValue);
			}

		if( uFunc == 5 ) {

			pCtx->m_IP2 = MotorToHost(dwValue);
			}

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort  = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 3 )  { 
		
		// Set Drop Number

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bUnit  = BYTE(uValue);

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 4 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP1);
		}
	
	if( uFunc == 6 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP2);
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL CModbusTCPMaster::Ping(void)
{
	if( m_pCtx->m_fPing ) {
		
		DWORD IP;

		if( !m_pCtx->m_fAux ) {

			IP = m_pCtx->m_IP1;
			}
		else
			IP = m_pCtx->m_IP2;
		
		if( CheckIP(IP, m_pCtx->m_uTime2) == NOTHING ) {
			
			if( m_pCtx->m_IP2 ) {

				m_pCtx->m_fAux = !m_pCtx->m_fAux;
				}

			return CCODE_ERROR; 
			}
		}

	if( OpenSocket() ) {
						
		if( m_pCtx->m_bUnit == 255 ) {

			return CCODE_SUCCESS;
			}

		if( m_pCtx->m_PingReg ) {

			DWORD    Data[1];

			CAddress Addr;

			Addr.a.m_Table  = SPACE_HOLDING;
			
			Addr.a.m_Offset = m_pCtx->m_PingReg;
			
			Addr.a.m_Type   = addrWordAsWord;

			Addr.a.m_Extra  = 0;

			return DoWordRead(Addr, Data, 1);
			}

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CModbusTCPMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	// TODO -- Should we sleep here to yield processor?

	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	UINT uType = Addr.a.m_Type;

	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
			
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG32:
			
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));
			
		case SPACE_HOLDING:

			if( uType == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax03));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG:

			if( uType == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax04));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));

		case SPACE_OUTPUT:

			return DoBitRead(Addr, pData, min(uCount, m_pCtx->m_uMax01));

		case SPACE_INPUT:
			
			return DoBitRead(Addr, pData, min(uCount, m_pCtx->m_uMax02));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CModbusTCPMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	// TODO -- Should we sleep here to yield processor?

	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	switch( Addr.a.m_Table ) {

		case SPACE_HOLD32:
			
			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_HOLDING:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(Addr, pData, min(uCount, m_pCtx->m_fDisable16 ? UINT(1) : m_pCtx->m_uMax16));
				}

			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_OUTPUT:
			
			return DoBitWrite(Addr, pData, min(uCount, m_pCtx->m_fDisable15 ? UINT(1) : m_pCtx->m_uMax15));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Socket Management

BOOL CModbusTCPMaster::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CModbusTCPMaster::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		m_pCtx->m_fAux   = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   Port;

		if( !m_pCtx->m_fAux ) {

			IP   = (IPADDR const &) m_pCtx->m_IP1;

			Port = WORD(m_pCtx->m_uPort);
			}
		else {
			IP   = (IPADDR const &) m_pCtx->m_IP2;

			Port = WORD(m_pCtx->m_uPort);
			}

		if( m_pCtx->m_pSock->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			if( m_pCtx->m_IP2 ) {

				m_pCtx->m_fAux = !m_pCtx->m_fAux;
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		if( m_pCtx->m_IP2 ) {

			m_pCtx->m_fAux = !m_pCtx->m_fAux;
			}

		return FALSE;
		}

	return FALSE;
	}

void CModbusTCPMaster::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

void CModbusTCPMaster::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);
	}

void CModbusTCPMaster::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {
	
		m_bTxBuff[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CModbusTCPMaster::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CModbusTCPMaster::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CModbusTCPMaster::SendFrame(void)
{
	m_bTxBuff[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CModbusTCPMaster::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRxBuff[5];

				if( uPtr >= uTotal ) {

					if( m_bRxBuff[0] == m_bTxBuff[0] ) {

						if( m_bRxBuff[1] == m_bTxBuff[1] ) {

							memcpy(m_bRxBuff, m_bRxBuff + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRxBuff[n] = m_bRxBuff[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CModbusTCPMaster::Transact(BOOL fIgnore)
{
	if( SendFrame() && RecvFrame() ) {

		if( fIgnore ) {

			return TRUE;
			}

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CModbusTCPMaster::CheckFrame(void)
{
	if( !(m_bRxBuff[1] & 0x80) ) {
	
		return TRUE;
		}

	return FALSE;
	}

// Read Handlers

CCODE CModbusTCPMaster::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Read Word %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount); */ 
	
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLDING:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(WORD(Addr.a.m_Offset - 1));
	
	AddWord(WORD(uCount));
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x   = PU2(m_bRxBuff + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CModbusTCPMaster::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Read Long %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount); */
	
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLDING:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(WORD(Addr.a.m_Offset - 1));
	
	AddWord(WORD(Addr.a.m_Table == SPACE_HOLD32 ? uCount : uCount * 2));
	
	if( Transact(FALSE) ) {

		BOOL fReal = IsReal(Addr.a.m_Type);

		BOOL fFlip = fReal ? m_pCtx->m_fFlipReal : m_pCtx->m_fFlipLong;
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_bRxBuff + 3)[n];
			
			pData[n] = MotorToHost(x);

			if( fFlip ) {

				x = pData[n];

				pData[n] = MAKELONG(HIWORD(x), LOWORD(x));
				}
			}

		return uCount;
		}

	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CModbusTCPMaster::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Read Bit %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount); */ 
	
	switch( Addr.a.m_Table ) {
			
		case SPACE_OUTPUT:
			StartFrame(0x01);
			break;
			
		case SPACE_INPUT:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(WORD(Addr.a.m_Offset - 1));

	AddWord(WORD(uCount));

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRxBuff[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CModbusTCPMaster::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Write Word %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount); */ 
	
	if( Addr.a.m_Table == SPACE_HOLDING ) {

		if( ( uCount == 1 ) && !m_pCtx->m_fDisable6 ) {
			
			StartFrame(6);

			AddWord(WORD(Addr.a.m_Offset - 1));
			
			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);

			AddWord(WORD(Addr.a.m_Offset - 1));
			
			AddWord(WORD(uCount));
			
			AddByte(BYTE(uCount * 2));
			
			for( UINT n = 0; n < uCount; n++ ) {
				
				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		if( IgnoreException() ) {

			memset(pData, 0, uCount * sizeof(DWORD));

			return uCount;
			} 

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CModbusTCPMaster::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Write Long %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount); */ 
	
	UINT uTable = Addr.a.m_Table;
	
	if( uTable == SPACE_HOLD32 || uTable == SPACE_HOLDING ) {

		StartFrame(16);
		
		AddWord(WORD(Addr.a.m_Offset - 1));
		
		AddWord(WORD(uTable == SPACE_HOLD32 ? uCount : uCount * 2));
		
		AddByte(BYTE(uCount * 4));

		BOOL fReal = IsReal(Addr.a.m_Type);

		BOOL fFlip = fReal ? m_pCtx->m_fFlipReal : m_pCtx->m_fFlipLong;
		
		for( UINT n = 0; n < uCount; n++ ) {

			DWORD x = pData[n];

			if( fFlip ) {

				x = MAKELONG(HIWORD(pData[n]), LOWORD(pData[n]));
				}
			
			AddLong(x);
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		if( IgnoreException() ) {

			memset(pData, 0, uCount * sizeof(DWORD));

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CModbusTCPMaster::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	/*AfxTrace("Write Bit %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount); */ 
	
	if( Addr.a.m_Table == SPACE_OUTPUT ) {

		if( (uCount == 1) && !m_pCtx->m_fDisable5 ) {

			StartFrame(5);

			AddWord(WORD(Addr.a.m_Offset - 1));
			
			AddWord(pData[0] ? WORD(0xFF00) : WORD(0x0000));
			}
		else {
			StartFrame(15);

			AddWord(WORD(Addr.a.m_Offset - 1));

			AddWord(WORD(uCount));

			AddByte(BYTE((uCount + 7) / 8));

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(BYTE(b));

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(BYTE(b));
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		if( IgnoreException() ) {

			memset(pData, 0, uCount * sizeof(DWORD));

			return uCount;
			}  

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Helpers

void CModbusTCPMaster::Limit(UINT &uData, UINT uMin, UINT uMax)
{
	if( uData < uMin ) uData = uMin;
	
	if( uData > uMax ) uData = uMax;
	}

BOOL CModbusTCPMaster::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CModbusTCPMaster::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CModbusTCPMaster::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

BOOL CModbusTCPMaster::IgnoreException(void)
{
	if( m_bRxBuff[1] & 0x80 ) {

		if( m_pCtx->m_fNoReadEx ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CModbusTCPMaster::IsReal(UINT uType)
{
	switch( uType ) {

		case addrWordAsReal:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

// End of File
