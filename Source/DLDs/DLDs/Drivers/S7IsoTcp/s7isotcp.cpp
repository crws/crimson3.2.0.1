
#include "intern.hpp"

#include "s7isotcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 ISO TCP/IP Master Driver
//

// Instantiator

INSTANTIATE(CS7IsoTcpMasterDriver);

// Constructor

CS7IsoTcpMasterDriver::CS7IsoTcpMasterDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CS7IsoTcpMasterDriver::~CS7IsoTcpMasterDriver(void)
{
	}

// Configuration

void MCALL CS7IsoTcpMasterDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CS7IsoTcpMasterDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CS7IsoTcpMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CS7IsoTcpMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP1		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_bType		= GetByte(pData) + 1;
			m_pCtx->m_bConn		= GetByte(pData);
			m_pCtx->m_bSlot		= GetByte(pData);
			m_pCtx->m_bRack		= GetByte(pData);
			m_pCtx->m_bClient	= GetByte(pData);
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->m_fConnect	= FALSE;
			m_pCtx->m_fConfirm	= FALSE;
			m_pCtx->m_BlkOff	= GetWord(pData);
			
			m_pCtx->m_fDirty        = FALSE;
			m_pCtx->m_fAux          = FALSE;
			m_pCtx->m_IP2		= GetAddr(pData);

			m_pCtx->m_TSAP		= GetByte(pData);
			m_pCtx->m_STsap		= GetWord(pData);
			m_pCtx->m_CTsap		= GetWord(pData);
			m_pCtx->m_STsap2	= GetWord(pData);
			m_pCtx->m_CTsap2	= GetWord(pData);

			pDevice->SetContext(m_pCtx);
		
			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CS7IsoTcpMasterDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CS7IsoTcpMasterDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 || uFunc == 5 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		if( uFunc == 1  ) {

			pCtx->m_IP1 = MotorToHost(dwValue);
			}

		if( uFunc == 5 ) {

			pCtx->m_IP2 = MotorToHost(dwValue);
			}

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		}
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort  = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 4 ) {
		
		// Get Primary IP Address

		return MotorToHost(pCtx->m_IP1);
		}
	
	if( uFunc == 6 ) {
		
		// Get Secondary IP Address

		return MotorToHost(pCtx->m_IP2);
		}

	if( uFunc == 7 ) {

		// Get Fallback Status

		return pCtx->m_fAux;
		}

	return 0;
	}

// Entry Points

CCODE MCALL CS7IsoTcpMasterDriver::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP1, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}
		}

	return CS7BaseDriver::Ping(); 
	}

// Socket Management

BOOL CS7IsoTcpMasterDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CS7IsoTcpMasterDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		m_pCtx->m_fAux   = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   uPort;

		if( !m_pCtx->m_fAux ) {

			IP    = (IPADDR const &) m_pCtx->m_IP1;

			uPort = WORD(m_pCtx->m_uPort);
			}
		else {
			IP    = (IPADDR const &) m_pCtx->m_IP2;

			uPort = WORD(m_pCtx->m_uPort);
			}

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}
				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			if( m_pCtx->m_IP2 ) {

				m_pCtx->m_fAux = !m_pCtx->m_fAux;
				}
				
			CloseSocket(TRUE); 

			return FALSE;
			}

		if( m_pCtx->m_IP2 ) {

			m_pCtx->m_fAux = !m_pCtx->m_fAux;
			}

		return FALSE;
		}

	return FALSE;
	}

void CS7IsoTcpMasterDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CS7IsoTcpMasterDriver::Transact(void)
{
	if( OpenSocket() ) {

		if( Send() && GetReply() ) {

			if( m_bRxBuff[2] > 7 ) {

				if( CheckReply() ) {

					return TRUE;
					}

				return FALSE;
				} 

			return TRUE;
			}

		CloseSocket(TRUE); 
		}

	return FALSE; 
	}

BOOL CS7IsoTcpMasterDriver::Send(void)
{
	UINT uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}
	
	return FALSE;
	}

UINT CS7IsoTcpMasterDriver::GetReply(void)
{	
	UINT uPtr  = 0;

	UINT uSize = 0;	 
	
	UINT uBytes = 0;

	BOOL fAck = FALSE;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			if( uSize <= 3 ) {

				uPtr += uSize;

				continue;
				}

			UINT uTotal = uPtr + uSize;

			uBytes = m_bRxBuff[3] + 0x100 * m_bRxBuff[2];

			while( uBytes < uTotal ) {

				uTotal -= uBytes;

				memcpy(m_bRxBuff, m_bRxBuff + uBytes, uTotal);

				uBytes = m_bRxBuff[3] + 0x100 * m_bRxBuff[2];
				}

			if( uBytes > 7 ) {

				if( !m_pCtx->m_fConnect ) {

					return m_bRxBuff[5] == 0xD0;
					}
					
				if( m_bRxBuff[5] == 0xF0 && m_bRxBuff[6] == 0x80 ) {

					if( !m_pCtx->m_fConfirm ) {

						return TRUE;
						}

					memcpy(m_bRxBuff, m_bRxBuff + 4, uBytes - 4);

					return TRUE;
					}

				uPtr += uSize;
				}
			else {
				uPtr = 0;
				}
					
			continue;
			}

		if( !CheckSocket() ) {
			
			return FALSE;
			} 

		Sleep(10);
		}
	
	return FALSE;
	}

void  CS7IsoTcpMasterDriver::StartFrame(void)
{
	m_uPtr = 0;

	AddByte(0x03);

	AddWord(0x00);

	AddByte(0x00);
	}

void  CS7IsoTcpMasterDriver::AddPPIHeader(void)
{
	StartFrame();
	
	AddByte(0x02);     

	AddByte(0xF0);	   

	AddByte(0x80);
	}

void  CS7IsoTcpMasterDriver::SetPDUHeader(void)
{
       if( m_uPtr > 14 && m_pCtx->m_fConnect && m_pCtx->m_fConfirm ) {

		m_bTxBuff[14] = m_bParamTail - m_bParamHead;
		} 

       if( m_uPtr > 16 && m_pCtx->m_fConnect && m_pCtx->m_fConfirm ) {

		m_bTxBuff[16] = m_uPtr - m_bParamTail;
		} 
	}

void  CS7IsoTcpMasterDriver::TermFrame(void)
{
	m_bTxBuff[3] = m_uPtr % 0x100;

	m_bTxBuff[2] = m_uPtr / 0x100;

	SetPDUHeader();
	}

BOOL  CS7IsoTcpMasterDriver::Connect(void)
{
	return ConnectionRequest() && ConnectionConfirm();
	}

BOOL  CS7IsoTcpMasterDriver::Ack(void)
{
	StartFrame();

	AddByte(0x02);

	AddByte(0xF0);

	AddByte(0x00);

	TermFrame();

	Transact();

	return TRUE;
	}

BOOL  CS7IsoTcpMasterDriver::ConnectionRequest(void)
{
	if( m_pCtx->m_fConnect ) {

		return TRUE;
		}

	BOOL fISO = m_pCtx->m_bType == CPU_ISO;

	WORD wSTsap = m_pCtx->m_STsap;

	WORD wCTsap = m_pCtx->m_CTsap;

	if( m_pCtx->m_fAux ) {

		if( m_pCtx->m_STsap2 ) {

			wSTsap = m_pCtx->m_STsap2;
			}

		if( m_pCtx->m_CTsap2 ) {

			wCTsap = m_pCtx->m_CTsap2;
			}
		}

	StartFrame();
	
	AddByte(0x11);	

	AddByte(0xE0);	

	AddWord(0x00);	

	AddByte(0x00);	

	AddByte(fISO ? 0x1 : 0x2);	 

	AddByte(0x00);

	AddByte(0xC1);

	AddByte(0x02);

	if( (m_pCtx->m_TSAP || m_pCtx->m_fAux) && wCTsap ) {

		AddWord(wCTsap);
		}
	else {
		AddByte(m_pCtx->m_bClient);

		AddByte(0x00);
		}

	AddByte(0xC2);

	AddByte(0x02);

	if( (m_pCtx->m_TSAP || m_pCtx->m_fAux) && wSTsap ) {

		AddWord(wSTsap);
		}
	else {
		AddByte(fISO ? m_pCtx->m_bRack + 1 : m_pCtx->m_bConn);

		AddByte(m_pCtx->m_bSlot);
		}

	AddByte(0xC0);

	AddByte(0x01);

	AddByte(0x09);

	TermFrame();

	m_pCtx->m_fConnect = ( Transact() );

	return m_pCtx->m_fConnect;
	}

BOOL  CS7IsoTcpMasterDriver::ConnectionConfirm(void)
{
	if( m_pCtx->m_fConfirm ) {

		return TRUE;
		}
	
	StartFrame();

	AddByte(0x02);	

	AddByte(0xF0);	// Code

	AddByte(0x80);	// Credit

	AddByte(0x32);

	AddByte(0x01);	// Op

	AddWord(0x00);
		
	AddByte(0x02);

	AddWord(0x00);

	AddByte(0x08);

	AddWord(0x00);

	AddByte(0xF0);
	
	AddWord(0x00);

	AddByte(0x01);

	AddByte(0x00);

	AddByte(0x01);

	AddByte(0x01);

	AddByte(0xE0);

	TermFrame();

	m_pCtx->m_fConfirm = ( Transact() );

	if( !m_pCtx->m_fConfirm ) {

		m_pCtx->m_fConnect = FALSE;
		}

	return m_pCtx->m_fConfirm;
	}

BOOL  CS7IsoTcpMasterDriver::DisconnectRequest(void)
{
	if( !m_pCtx->m_fConnect ) {

		return TRUE;
		}
	
	StartFrame();

	AddByte(0x02);

	AddByte(0xF0);	// Code

	AddByte(0x80);	// Credit

	AddByte(0x32);

	AddByte(0x01);  // Op

	AddWord(0x00);

	AddByte(0x17);

	AddWord(0x00);

	AddByte(0x08);

	AddWord(0x00);

	AddByte(0x1E);

	AddWord(0x00);

	AddWord(0x00);

	AddWord(0x00);

	AddByte(0x07);

	TermFrame();

	m_pCtx->m_fConnect = ( !Transact() );

	return !m_pCtx->m_fConnect;
	}

BOOL  CS7IsoTcpMasterDriver::DisconnectConfirm(void)
{
	if( !m_pCtx->m_fConfirm ) {

		return TRUE;
		}
	
	StartFrame();

	AddByte(0x02);

	AddByte(0xF0);	// Code

	AddByte(0x80);	// Credit

	AddByte(0x32);

	AddByte(0x01);	// Op

	AddWord(0x00);

	AddByte(0x18);

	AddWord(0x00);

	AddByte(0x08);

	AddWord(0x00);

	AddByte(0x1F);

	AddWord(0x00);

	AddWord(0x00);

	AddWord(0x00);

	AddByte(0x07);

	TermFrame();

	m_pCtx->m_fConfirm = ( !Transact() );

	return !m_pCtx->m_fConfirm;

	}

BOOL  CS7IsoTcpMasterDriver::Disconnect(void)
{
	if( m_pCtx->m_fConnect ) {

		if( DisconnectRequest() ) {

			if( m_pCtx->m_fConfirm )  {

				DisconnectConfirm();
				}
			}
		}

	m_pCtx->m_fConfirm = FALSE;

	m_pCtx->m_fConnect = FALSE;

	return TRUE;
	}

// Helpers

BOOL CS7IsoTcpMasterDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CS7IsoTcpMasterDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CS7IsoTcpMasterDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
