#include "s7base2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// S7 CPU's
//

#define	CPU_ISO		0x01
#define	CPU_243		0x02

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 ISO TCP/IP Master Driver
//

class CS7IsoTcpMasterDriver : public CS7BaseDriver
{
	public:
		// Constructor
		CS7IsoTcpMasterDriver(void);

		// Destructor
		~CS7IsoTcpMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		
	protected:
		// Device Context
		struct CContext : CS7BaseDriver::CBaseCtx
		{
			DWORD	 m_IP1;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL	 m_fConnect;
			BOOL     m_fConfirm;
			BYTE	 m_bType;
			BYTE     m_bConn;
			BYTE	 m_bSlot;
			BYTE     m_bRack;
			BYTE     m_bClient;
			WORD	 m_BlkOff;
			BOOL	 m_fAux;
			BOOL	 m_fDirty;
			DWORD	 m_IP2;
			BYTE	 m_TSAP;
			WORD     m_STsap;
			WORD     m_CTsap;
			WORD     m_STsap2;
			WORD	 m_CTsap2;
			};

		// Data Members
		CContext * m_pCtx;
		UINT	   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Implementation
		void StartFrame(void);
		void TermFrame(void);
		void AddPPIHeader(void);
		void SetPDUHeader(void);
				
		// Transport Layer
		BOOL Connect(void);
		BOOL Disconnect(void);
		BOOL Transact(void);
		BOOL Send(void);
		UINT GetReply(void);
				
		// Helpers
		BOOL ConnectionRequest(void);
		BOOL ConnectionConfirm(void);
		BOOL DisconnectRequest(void);
		BOOL DisconnectConfirm(void);
		BOOL Ack(void);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
	};

// End of File
