#ifndef	INCLUDE_IdecBase_HPP
	
#define	INCLUDE_IdecBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Idec Base Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

//// Data Spaces	
#define DATA_REG	'1'
#define TIMER_CURRENT	'2'
#define TIMER_PRESET	'3'	
#define COUNTER_CURRENT	'4'
#define COUNTER_PRESET	'5'
#define INPUT		'6'
#define OUTPUT		'7'
#define ORD_INT_RELAY	'8'
#define SPC_INT_RELAY	'9'
#define SHIFT_REG	'A'
#define INPUT_BYTES	'B'
#define OUTPUT_BYTES	'C'
#define ORD_INT_BYTES	'D'
#define SPC_INT_BYTES	'E'
#define	SHIFT_REG_BYTES	'F'
#define CLOCK		'W'		    
#define COMMERRORCOUNT	'Z'
#define X_DATA_REG	'X'

#define BITSZ		1
#define WORDSZ		2
#define LONGSZ		4

class CIdecBaseDriver : public CMasterDriver
{
	public:
		// Constructor
		CIdecBaseDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			};
		CContext *	m_pCtx;

		// Hex Lookup
		LPCTXT	m_pHex;
		
		// Comms Data
		BYTE	m_bTxBuff[300];
		BYTE	m_bRxBuff[300];
		BYTE	m_bCheck;
		UINT	m_uPtr;
		UINT	m_uError;

		// Read Handlers
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
		void	PutRead(UINT uTable, UINT uOffset, UINT uType, UINT uCount, UINT uSize);
		
		// Write Handlers
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
		void	PutWrite(AREF Addr, UINT uOffset, PDWORD pData, UINT uCount, UINT uSize);

		// Implementation
		void	StartFrame(void);
		void	PutDataTypeCode(WORD wType, WORD wDataType);//new
		void	PutOperand(WORD wType, WORD wAddr);
		void	PutRelayAddress(WORD wAddr, WORD uFactor);
		void	PutDataLength(WORD wType, WORD wCount, WORD wDataType);//new

		void	AddByte(BYTE bData);
		void	AddValue(WORD wData, WORD wBase, UINT uFactor);
		void	AddLong(DWORD dwData, WORD wBase);
		void	PutBCC(void);

		// Transport 
		BOOL	Transact(void);

		// Transport Hooks
		virtual BOOL Send(void)		= 0;
		virtual BYTE GetFrame(void)	= 0;
		
		// Overridable Methods
		virtual UINT GetMaxWords(void);
		virtual UINT GetMaxBits(void);
				
		//Helpers
		WORD	GetValue(WORD wPtr, UINT uCount, WORD wRadix);
		DWORD   GetLongValue(WORD wPtr, UINT uCount, WORD wRadix);
		BYTE	GetHexValue(BYTE bData);
		WORD	GetBit(WORD wPtr);
		WORD	GetData(WORD wScan, WORD wDataType);
		DWORD	GetLong(WORD wScan);
		//
		BOOL	IsSingleBit(UINT uOP);
		BOOL	MaskData(UINT uOP);
		BOOL	ValidWrite(UINT uOP);
		void	AdjustOffset( UINT * pOffset, UINT uTable );

	};

#endif

// End of File
