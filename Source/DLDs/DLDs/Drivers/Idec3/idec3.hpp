#ifndef	INCLUDE_Idec3_HPP

#define	INCLUDE_Idec3_HPP

#include "idecBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Idec3 Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

class CIdec3Driver : public CIdecBaseDriver
{
	public:
		// Constructor
		CIdec3Driver(void);

		// Configuration
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:

		// Overridable Methods
		BOOL Send(void);
		BYTE GetFrame(void);
};

#endif

// End of File
