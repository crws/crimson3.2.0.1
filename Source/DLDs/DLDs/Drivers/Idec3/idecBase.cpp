
#include "intern.hpp"

#include "idecBase.hpp"

#define COMMERRORTEST FALSE

//////////////////////////////////////////////////////////////////////////
//
// Idec3 Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CIdecBaseDriver::CIdecBaseDriver(void)
{
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uError = 0;
}

// Entry Points

CCODE MCALL CIdecBaseDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = DATA_REG;
	Addr.a.m_Offset = 0x0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
}

CCODE MCALL CIdecBaseDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == COMMERRORCOUNT ) {
		*pData = DWORD(m_uError);
		return 1;
	}

	switch( Addr.a.m_Type ) {

		case addrWordAsWord:	return DoWordRead(Addr, pData, uCount);

		case addrWordAsLong:
		case addrWordAsReal:	return DoLongRead(Addr, pData, uCount);

		case addrByteAsByte:
		case addrBitAsBit:	return DoBitRead(Addr, pData, uCount);
	}

	if( COMMERRORTEST ) m_uError++;

	return CCODE_ERROR | CCODE_HARD;
}

CCODE MCALL CIdecBaseDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == COMMERRORCOUNT ) {
		m_uError = 0;
		return 1;
	}

	if( ValidWrite(Addr.a.m_Table) ) {

		switch( Addr.a.m_Type ) {

			case addrWordAsWord:	return DoWordWrite(Addr, pData, uCount);

			case addrWordAsLong:
			case addrWordAsReal:	return DoLongWrite(Addr, pData, uCount);

			case addrBitAsBit:
			case addrByteAsByte:	return DoBitWrite(Addr, pData, uCount);
		}
	}

	if( COMMERRORTEST ) m_uError++;

	return CCODE_ERROR | CCODE_HARD;
}

// Read Handlers

CCODE CIdecBaseDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uCt = min(uCount, GetMaxWords());

	PutRead(Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Type, uCt, WORDSZ);

	if( Transact() ) {

		if( m_bRxBuff[2] != '0' ) return CCODE_ERROR;

		BOOL fBit  = IsSingleBit(Addr.a.m_Table);

		BOOL fMask = MaskData(Addr.a.m_Table);

		for( UINT i = 0; i < uCt; i++ ) {

			WORD wData = GetData(i*2, WORDSZ);

			if( fMask ) wData &= 0x3FFF;

			if( fBit ) {

				WORD w = LOBYTE(wData & 0xFF) << 8;

				wData = ((wData >> 8) & 0xFF) | w;
			}

			pData[i] = wData < 0x8000 ? DWORD(wData) : DWORD(wData) | 0xFFFF0000;

		}

		return uCt;
	}

	if( COMMERRORTEST ) m_uError++;

	return CCODE_ERROR;
}

CCODE CIdecBaseDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uCt   = min(uCount, GetMaxWords()/2);

	PutRead(Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Type, uCt, LONGSZ);

	if( Transact() ) {

		if( m_bRxBuff[2] != '0' ) {

			return CCODE_ERROR;
		}

		for( UINT i = 0; i < uCt; i++ ) {

			pData[i] = GetLong(i * 2);
		}

		return uCt;
	}

	if( COMMERRORTEST ) {

		m_uError++;
	}

	return CCODE_ERROR;
}

CCODE CIdecBaseDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uCt     = 0;;

	UINT uReqCt  = 0;

	UINT uOffset = Addr.a.m_Offset;

	AdjustOffset(&uOffset, Addr.a.m_Table);

	if( Addr.a.m_Type == addrBitAsBit ) {

		uCt    = min(uCount, GetMaxBits());

		uReqCt = (7+uCt)/8;
	}
	else {
		uCt    = min(uCount, GetMaxBits()/8);

		uReqCt = uCt;
	}

	if( Addr.a.m_Type == addrBitAsBit && uCt < 8 ) {

		uReqCt = uCt = 1;

		PutRead(Addr.a.m_Table, uOffset, addrBitAsBit, uReqCt, BITSZ);
	}
	else {
		PutRead(Addr.a.m_Table, uOffset, addrByteAsByte, uReqCt, BITSZ);
	}

	if( Transact() ) {

		if( m_bRxBuff[2] != '0' ) {

			if( Addr.a.m_Type == addrBitAsBit ) {

				if( uReqCt > 1 ) {

					uReqCt -= 1;

					uCt    -= 8;

					PutRead(Addr.a.m_Table, uOffset, addrByteAsByte, uReqCt, BITSZ);

					if( Transact() ) {

						if( m_bRxBuff[2] != '0' ) {

							m_uError++;

							return CCODE_ERROR;
						}
					}
				}
			}
			else {

				m_uError++;

				return CCODE_ERROR;
			}
		}

		if( Addr.a.m_Type == addrByteAsByte ) {

			for( UINT j = 0; j < uReqCt; j++ ) {

				UINT uData = GetData(j*2, BITSZ);

				pData[j]   = (DWORD) uData;
			}

			return uCt;
		}

		if( uCt == 1 ) {

			pData[0] = GetData(0, BITSZ) ? 1L : 0L;

			return uCt;
		}

		for( UINT j = 0, uPtr = 0; j < uReqCt; j++ ) {

			UINT uData = GetData(2*j, BITSZ);

			UINT uMask = 1;

			while( uMask & 0xFF && uPtr < uCt ) {

				pData[uPtr++] = (DWORD) (uData & uMask ? 1L : 0L);

				uMask <<= 1;
			}
		}

		return uCt;
	}

	m_uError++;

	return CCODE_ERROR;
}

// Implementation

void CIdecBaseDriver::PutRead(UINT uTable, UINT uOffset, UINT uType, UINT uCount, UINT uSize)
{
	StartFrame();

	AddByte('0');

	AddByte('R');

	PutDataTypeCode(uTable, uType);

	PutOperand(uTable, uOffset);

	PutDataLength(uType, uCount, uSize);

	PutBCC();

	AddByte(CR);
}

// Write Handlers

CCODE CIdecBaseDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uCt;

	if( Addr.a.m_Type == addrBitAsBit ) {

		uCt = 1;
	}
	else {
		uCt = min(uCount, GetMaxWords());
	}

	PutWrite(Addr, Addr.a.m_Offset, pData, uCt, WORDSZ);

	if( Transact() ) {

		if( m_bRxBuff[2] != '0' ) {

			return CCODE_ERROR;
		}

		return uCt;
	}

	m_uError++;

	return CCODE_ERROR;
}

CCODE CIdecBaseDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uCt = min(uCount, GetMaxWords()/2);

	PutWrite(Addr, Addr.a.m_Offset, pData, uCt, LONGSZ);

	if( Transact() ) {

		if( m_bRxBuff[2] != '0' ) {

			return CCODE_ERROR;
		}

		return uCt;
	}

	m_uError++;

	return CCODE_ERROR;
}

CCODE CIdecBaseDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uCt;

	UINT uOffset = Addr.a.m_Offset;

	if( Addr.a.m_Type == addrBitAsBit ) {

		uCt = 1;
	}
	else {
		uCt = min(uCount, 16);

		AdjustOffset(&uOffset, Addr.a.m_Table);
	}

	PutWrite(Addr, uOffset, pData, uCt, BITSZ);

	if( Transact() ) {

		if( m_bRxBuff[2] != '0' ) {

			return CCODE_ERROR;
		}

		return uCt;
	}

	m_uError++;

	return CCODE_ERROR;
}

void CIdecBaseDriver::PutWrite(AREF Addr, UINT uOffset, PDWORD pData, UINT uCount, UINT uSize)
{
	StartFrame();

	AddByte('0');

	AddByte('W');

	PutDataTypeCode(Addr.a.m_Table, Addr.a.m_Type);

	PutOperand(Addr.a.m_Table, uOffset);

	PutDataLength(Addr.a.m_Type, uCount, uSize);

	UINT i = 0;

	if( Addr.a.m_Type == addrBitAsBit ) {

		AddByte(pData[0] ? '1' : '0');
	}

	else if( Addr.a.m_Type == addrByteAsByte ) {

		for( i = 0; i < uCount; i++ ) {

			AddValue(WORD(LOBYTE(pData[i])), 16, 16);
		}
	}

	else {
		for( i = 0; i < uCount; i++ ) {

			if( Addr.a.m_Type > addrWordAsWord ) {

				AddLong(pData[i], 16);

				continue;
			}

			WORD wData = LOWORD(pData[i]);

			if( MaskData(Addr.a.m_Table) ) {

				wData &= 0x3FFF;
			}

			if( IsSingleBit(Addr.a.m_Table) ) {

				WORD w = (wData << 8) & 0xFF00;

				wData >>= 8;

				wData |= w;
			}

			AddValue(wData, 16, 4096);
		}
	}

	PutBCC();

	AddByte(CR);
}

void CIdecBaseDriver::StartFrame(void)
{
	m_uPtr = 0;

	m_bCheck = 0;

	AddByte(ENQ);

	AddByte(m_pHex[(m_pCtx->m_bDrop) / 16]);
	AddByte(m_pHex[(m_pCtx->m_bDrop) % 16]);
}

void CIdecBaseDriver::PutDataTypeCode(WORD wType, WORD wDataType)
{
	switch( wType )
	{
		case INPUT:
		case INPUT_BYTES:
			if( wDataType == addrBitAsBit )
				AddByte('x');
			else
				AddByte('X');
			break;

		case OUTPUT:
		case OUTPUT_BYTES:
			if( wDataType == addrBitAsBit )
				AddByte('y');
			else
				AddByte('Y');
			break;

		case SPC_INT_RELAY:
		case ORD_INT_RELAY:
		case SPC_INT_BYTES:
		case ORD_INT_BYTES:
			if( wDataType == addrBitAsBit )
				AddByte('m');
			else
				AddByte('M');
			break;

		case SHIFT_REG:
		case SHIFT_REG_BYTES:
			if( wDataType == addrBitAsBit )
				AddByte('r');
			else
				AddByte('R');
			break;

		case TIMER_CURRENT:

			AddByte('t');

			break;

		case TIMER_PRESET:

			AddByte('T');

			break;

		case COUNTER_CURRENT:

			AddByte('c');

			break;

		case COUNTER_PRESET:

			AddByte('C');

			break;

		case DATA_REG:

			AddByte('D');

			break;

		case CLOCK:

			AddByte('W');

			break;

		case X_DATA_REG:

			AddByte('A');

			break;
	}
}

void CIdecBaseDriver::PutOperand(WORD wType, WORD wAddr)
{

	switch( wType )
	{
		case INPUT:
		case OUTPUT:
		case ORD_INT_RELAY:
		case INPUT_BYTES:
		case OUTPUT_BYTES:
		case ORD_INT_BYTES:

			PutRelayAddress(wAddr, 100);
			break;

		case SPC_INT_RELAY:

			AddByte('8');
			PutRelayAddress(wAddr, 10);
			break;

		case SPC_INT_BYTES:

			AddByte('8');
			PutRelayAddress(wAddr, 10);
			break;

		case SHIFT_REG:
		case SHIFT_REG_BYTES:
		case TIMER_CURRENT:
		case TIMER_PRESET:
		case COUNTER_CURRENT:
		case COUNTER_PRESET:
		case CLOCK:
		case DATA_REG:

			AddValue(wAddr, 10, 1000);
			break;

		case X_DATA_REG:

			AddValue(wAddr, 0x10, 0x1000);
			break;
	}
}

void CIdecBaseDriver::PutDataLength(WORD wType, WORD wCount, WORD wSize)
{
	if( wType != addrBitAsBit ) AddValue(wSize * wCount, 16, 16);
}

void CIdecBaseDriver::PutRelayAddress(WORD wAddr, WORD uFactor)
{
	AddValue(wAddr/8, 10, uFactor);

	AddValue(wAddr % 8, 8, 1);
}

void CIdecBaseDriver::PutBCC(void)
{
	m_bTxBuff[m_uPtr++] = m_pHex[m_bCheck/16];
	m_bTxBuff[m_uPtr++] = m_pHex[m_bCheck%16];
}

void CIdecBaseDriver::AddByte(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;

	m_bCheck ^= bData;
}

void CIdecBaseDriver::AddValue(WORD wData, WORD wBase, UINT uFactor)
{
	while( uFactor ) {

		AddByte(m_pHex[(wData / uFactor) % wBase]);

		uFactor /= wBase;
	}
}

void CIdecBaseDriver::AddLong(DWORD dwData, WORD wBase)
{
	AddValue(HIWORD(dwData), wBase, 4096);

	AddValue(LOWORD(dwData), wBase, 4096);
}

BOOL CIdecBaseDriver::Transact(void)
{
	if( Send() ) {

		return (GetFrame() == ACK);
	}

	return FALSE;
}

// Overridables

UINT CIdecBaseDriver::GetMaxWords(void)
{
	return 16;
}

UINT CIdecBaseDriver::GetMaxBits(void)
{
	return 64;
}

// Helpers

BYTE CIdecBaseDriver::GetHexValue(BYTE bData)
{
	if( bData >= '0' && bData <= '9' )
		return bData - '0';

	if( bData >= 'A' && bData <= 'F' )
		return bData - 'A' + 10;

	if( bData >= 'a' && bData <= 'f' )
		return bData - 'a' + 10;

	return 0;
}

WORD CIdecBaseDriver::GetValue(WORD wPtr, UINT uCount, WORD wRadix)
{
	WORD wData = 0;

	while( uCount-- ) {

		wData *= wRadix;

		wData += GetHexValue(m_bRxBuff[wPtr++]);
	}

	return wData;
}

DWORD CIdecBaseDriver::GetLongValue(WORD wPtr, UINT uCount, WORD wRadix)
{
	DWORD dwData = 0;

	while( uCount-- ) {

		dwData *= wRadix;

		dwData += GetHexValue(m_bRxBuff[wPtr++]);
	}

	return dwData;
}

WORD CIdecBaseDriver::GetBit(WORD wPtr)
{
	return GetHexValue(m_bRxBuff[wPtr]);
}

WORD CIdecBaseDriver::GetData(WORD wScan, WORD wSize)
{
	WORD wData = GetValue(3 + wSize * wScan, wSize * 2, 16);

	return wData;
}

DWORD CIdecBaseDriver::GetLong(WORD wScan)
{
	return GetLongValue(3 + 4 * wScan, 4 * 2, 16);
}

BOOL CIdecBaseDriver::IsSingleBit(UINT uOP)
{
	switch( uOP )
	{
		case INPUT:
		case INPUT_BYTES:
		case OUTPUT:
		case OUTPUT_BYTES:
		case ORD_INT_RELAY:
		case ORD_INT_BYTES:
		case SHIFT_REG:
		case SHIFT_REG_BYTES:
		case SPC_INT_RELAY:
		case SPC_INT_BYTES:
			return TRUE;

		case TIMER_CURRENT:
		case TIMER_PRESET:
		case COUNTER_CURRENT:
		case COUNTER_PRESET:
		case DATA_REG:
		case X_DATA_REG:
			return FALSE;

		default:
			return FALSE;
	}
}

BOOL CIdecBaseDriver::MaskData(UINT uOP)
{
	switch( uOP )
	{
		case TIMER_CURRENT:
		case TIMER_PRESET:
		case COUNTER_CURRENT:
		case COUNTER_PRESET:
			return TRUE;

		default:
			return FALSE;
	}
}

BOOL CIdecBaseDriver::ValidWrite(UINT uOP)
{
	switch( uOP )
	{
		case INPUT:
		case INPUT_BYTES:
		case OUTPUT:
		case OUTPUT_BYTES:
		case ORD_INT_RELAY:
		case ORD_INT_BYTES:
		case SHIFT_REG:
		case SHIFT_REG_BYTES:
		case SPC_INT_RELAY:
		case SPC_INT_BYTES:
			return TRUE;

		case TIMER_CURRENT:	return FALSE;

		case TIMER_PRESET:	return TRUE;

		case COUNTER_CURRENT:	return FALSE;

		case COUNTER_PRESET:	return TRUE;

		case DATA_REG:		return TRUE;

		case X_DATA_REG:	return TRUE;

		default:		return FALSE;
	}
}

void CIdecBaseDriver::AdjustOffset(UINT * pOffset, UINT uTable)
{
	switch( uTable ) { // make bit address for bit 0 of bytes

		case INPUT_BYTES:
		case OUTPUT_BYTES:
		case ORD_INT_BYTES:
		case SPC_INT_BYTES:
		case SHIFT_REG_BYTES:

			*pOffset = (*pOffset)<<3;  // decimal address * 8
			break;
	}
}
/*
WORD CIdecBaseDriver::DoSetTime(PBYTE pTime)
{
	PutWriteTime(pTime);

	if( GetFrame() == ACK )
		return CC_OKAY;

	return CC_OKAY;
	}

void CIdecBaseDriver::PutWriteTime(PBYTE pTime)
{
	StartFrame();

	AddByte('0');

	AddByte('W');

	WORD wType = 'W' - '@';

	PutDataTypeCode(wType, typeUInt16);

	PutOperand(wType, 0);

	PutDataLength(wType, 7, typeUInt16);

	AddValue(pTime[5], 10, 1000);	//year

	AddValue(pTime[4], 10, 1000);	//month

	AddValue(pTime[3], 10, 1000);	//day

	AddValue(pTime[6], 10, 1000);	//day of week

	AddValue(pTime[2], 10, 1000);	//hour

	AddValue(pTime[1], 10, 1000);	//minute

	AddValue(pTime[0], 10, 1000);	//second

	PutBCC();

	AddByte( CR );

	Send();
	}

WORD CIdecBaseDriver::DoGetTime(PBYTE pTime)
{
	PutRead('W' - '@', 0, 7, typeUInt16);

	if( GetFrame() == ACK ) {

		pTime[6] = (DWORD)GetValue(3 + 4 * 3, 4, 10);	//day of week
		pTime[5] = (DWORD)GetValue(3 + 4 * 0, 4, 10);	//year
		pTime[4] = (DWORD)GetValue(3 + 4 * 1, 4, 10);	//month
		pTime[3] = (DWORD)GetValue(3 + 4 * 2, 4, 10);	//day
		pTime[2] = (DWORD)GetValue(3 + 4 * 4, 4, 10);	//hour
		pTime[1] = (DWORD)GetValue(3 + 4 * 5, 4, 10);	//minute
		pTime[0] = (DWORD)GetValue(3 + 4 * 6, 4, 10);	//second

		return CC_OKAY;
		}

	return CC_ERROR;
	}
*/

// End of File
