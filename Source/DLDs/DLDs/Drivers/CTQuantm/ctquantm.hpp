
//////////////////////////////////////////////////////////////////////////
//
// Control Techniques Quantum Driver : Control Techniques Mentor II Serial Driver
//

#include "mentor2.hpp"

#define CTQUANTUM_ID 0x403F

class CCTQuantumDriver : CMentor2MasterDriver
{
	public:
		// Constructor
		CCTQuantumDriver(void);

		// Destructor
		~CCTQuantumDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:		
	};

// End of File
