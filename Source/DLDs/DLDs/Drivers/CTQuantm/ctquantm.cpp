
#include "intern.hpp"

#include "ctquantm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Control Techniques Quantum Driver : Control Techniques Mentor II Master Serial Driver
//

// Instantiator

INSTANTIATE(CCTQuantumDriver);

// Constructor

CCTQuantumDriver::CCTQuantumDriver(void)
{
	m_Ident = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;

	m_fWide = FALSE;	
	}

// Destructor

CCTQuantumDriver::~CCTQuantumDriver(void)
{
	}

// Configuration

void MCALL CCTQuantumDriver::Load(LPCBYTE pData)
{
	CMentor2MasterDriver::Load(pData);
	}
	
void MCALL CCTQuantumDriver::CheckConfig(CSerialConfig &Config)
{
	CMentor2MasterDriver::CheckConfig(Config);
	}
	
// Management

void MCALL CCTQuantumDriver::Attach(IPortObject *pPort)
{
	CMentor2MasterDriver::Attach(pPort);
	}

void MCALL CCTQuantumDriver::Open(void)
{	
	CMentor2MasterDriver::Open();
	}

// Device

CCODE MCALL CCTQuantumDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	return CMentor2MasterDriver::DeviceOpen(pDevice);
	}

CCODE MCALL CCTQuantumDriver::DeviceClose(BOOL fPersist)
{
	CCODE c = CMentor2MasterDriver::DeviceClose(fPersist);
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CCTQuantumDriver::Ping(void)
{
	return CMentor2MasterDriver::Ping();
	}

CCODE MCALL CCTQuantumDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return CMentor2MasterDriver::Read(Addr, pData, uCount);
	}

CCODE MCALL CCTQuantumDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return CMentor2MasterDriver::Write(Addr, pData, uCount);
	}

// End of File
