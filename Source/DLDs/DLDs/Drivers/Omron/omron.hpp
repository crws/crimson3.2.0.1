
//////////////////////////////////////////////////////////////////////////
//
// Omron Driver
//

class COmronDriver : public CMasterDriver
{
	public:
		// Constructor
		COmronDriver(void);

		// Destructor
		~COmronDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			};
		CContext *	m_pCtx;

		// Hex Lookup
		LPCTXT m_pHex;
		
		// Comms Data
		BYTE m_bTx[256];
		BYTE m_bRx[256];
		BYTE m_bCheck;
		UINT m_uPtr;
		UINT m_uError;

		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		
		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Implementation
		void  StartFrame(BYTE bCmd, BYTE bType);
		void  AddByte(BYTE bData);
		void  AddDec(UINT uData, UINT uFactor);
		void  AddHex(UINT uData, UINT uFactor);
		void  TxByte(BYTE bData);
		BOOL  Transact(void);
		BOOL  TxFrame(void);
		BOOL  RxFrame(void);
		BOOL  SelectMonitor(void);
		UINT  xtoin(PTXT pData, UINT uCount);
		WORD  ToBin(WORD wData);
		DWORD ToBin(DWORD dwData);
		WORD  ToBCD(WORD wData);
		DWORD ToBCD(DWORD dwData);
		DWORD SwapWords(DWORD dwData);

	};

// End of File
