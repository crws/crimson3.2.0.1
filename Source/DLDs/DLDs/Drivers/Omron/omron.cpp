
#include "intern.hpp"

#include "omron.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omron Driver
//

// Instantiator

INSTANTIATE(COmronDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

COmronDriver::COmronDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Destructor

COmronDriver::~COmronDriver(void)
{
	}

// Configuration

void MCALL COmronDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL COmronDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}
	
// Management

void MCALL COmronDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL COmronDriver::Open(void)
{
	}

// Device

CCODE MCALL COmronDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL COmronDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL COmronDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 'D';
	Addr.a.m_Offset = 0x0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL COmronDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Type ) {

		case addrWordAsWord:	return DoWordRead(Addr, pData, uCount);

		case addrWordAsLong:	return DoLongRead(Addr, pData, uCount);

		case addrWordAsReal:	return DoLongRead(Addr, pData, uCount);
		}
	
	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL COmronDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Type ) {

		case addrWordAsWord:	return DoWordWrite(Addr, pData, uCount);

		case addrWordAsLong:	return DoLongWrite(Addr, pData, uCount);

		case addrWordAsReal:	return DoLongWrite(Addr, pData, uCount);
		}
	
	return CCODE_ERROR | CCODE_HARD;
	}

// Read Handlers

CCODE COmronDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType = 0;

	switch( Addr.a.m_Table ) {
	
		case 'I':	bType = 'R';	break;	/* IR bin */
		case 'J':	bType = 'R';	break;	/* IR bcd */
		case 'D':	bType = 'D';	break;	/* DM bin */
		case 'B':	bType = 'D';	break;	/* DM bcd */
		case 'A':	bType = 'J';	break;	/* AR bin */
		case 'C':	bType = 'J';	break;	/* AR bcd */
		case 'H':	bType = 'H';	break;	/* HR bin */
		case 'K':	bType = 'H';	break;	/* HR bcd */
		case 'L':	bType = 'L';	break;	/* LR bin */
		case 'M':	bType = 'L';	break;	/* LR bcd */
		case 'P':	bType = 'C';	break;	/* PV bcd */
		case 'E':	bType = 'E';	break;	/* EM bin */
		case 'F':	bType = 'E';	break;	/* EM bcd */
		case 'O':	bType = 'R';	break;	/* IO bin */
		case 'Q':	bType = 'R';	break;	/* IO bcd */
	    	
		default:	return CCODE_ERROR | CCODE_HARD;
		}

	uCount = min(uCount, 24);

	StartFrame('R', bType);

	AddDec(Addr.a.m_Offset, 1000);

	AddDec(uCount,          1000);

	if( Transact() ) {

		for( UINT n = 0; n < uCount; n++ ) {

			WORD wData = xtoin(PTXT(m_bRx + 4 * n), 4);
			
			switch( Addr.a.m_Table ) {
			
				case 'B':
				case 'J':
				case 'C':
				case 'K':
				case 'M':
				case 'P':
				case 'F':
				case 'Q':
					wData = ToBin(wData);
					break;
				}
			
			pData[n] = wData;
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE COmronDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType = 0;

	switch( Addr.a.m_Table ) {
	
		case 'I':	bType = 'R';	break;	/* IR bin */
		case 'J':	bType = 'R';	break;	/* IR bcd */
		case 'D':	bType = 'D';	break;	/* DM bin */
		case 'B':	bType = 'D';	break;	/* DM bcd */
		case 'A':	bType = 'J';	break;	/* AR bin */
		case 'C':	bType = 'J';	break;	/* AR bcd */
		case 'H':	bType = 'H';	break;	/* HR bin */
		case 'K':	bType = 'H';	break;	/* HR bcd */
		case 'L':	bType = 'L';	break;	/* LR bin */
		case 'M':	bType = 'L';	break;	/* LR bcd */		
		case 'P':	bType = 'C';	break;	/* PV bcd */		
		case 'E':	bType = 'E';	break;	/* EM bin */
		case 'F':	bType = 'E';	break;	/* EM bcd */		
		case 'O':	bType = 'R';	break;	/* IO bin */
		case 'Q':	bType = 'R';	break;	/* IO bcd */
	    	
		default:	return CCODE_ERROR | CCODE_HARD;
		}

	uCount = min(uCount, 12);

	StartFrame('R', bType);

	AddDec(Addr.a.m_Offset, 1000);

	AddDec(uCount * 2,      1000);

	if( Transact() ) {

		for( UINT n = 0; n < uCount; n++ ) {

			DWORD dwData = xtoin(PTXT(m_bRx + 8 * n), 8);

			dwData = SwapWords(dwData);
			
			switch( Addr.a.m_Table ) {
			
				case 'J':
				case 'B':
				case 'C':
				case 'K':
				case 'M':
				case 'P':
				case 'F':
				case 'Q':
					dwData = ToBin(dwData);
					break;

				default:
					break;
				}

			pData[n] = dwData;
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE COmronDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType = 0;

	switch( Addr.a.m_Table ) {
	
		case 'I':	bType = 'R';	break;	/* IR bin */
		case 'J':	bType = 'R';	break;	/* IR bcd */
		case 'D':	bType = 'D';	break;	/* DM bin */
		case 'B':	bType = 'D';	break;	/* DM bcd */
		case 'A':	bType = 'J';	break;	/* AR bin */
		case 'C':	bType = 'J';	break;	/* AR bcd */
		case 'H':	bType = 'H';	break;	/* HR bin */
		case 'K':	bType = 'H';	break;	/* HR bcd */
		case 'L':	bType = 'L';	break;	/* LR bin */
		case 'M':	bType = 'L';	break;	/* LR bcd */		
		case 'P':	bType = 'C';	break;	/* PV bcd */		
		case 'E':	bType = 'E';	break;	/* EM bin */
		case 'F':	bType = 'E';	break;	/* EM bcd */		
		case 'O':	bType = 'R';	break;	/* IO bin */
		case 'Q':	bType = 'R';	break;	/* IO bcd */

	     	default:	return CCODE_ERROR | CCODE_HARD;
		}

	uCount = min(uCount, 24);

	for( UINT t = 0; t < 2; t++ ) {

		StartFrame('W', bType);

		AddDec(Addr.a.m_Offset, 1000);

		for( UINT n = 0; n < uCount; n++ ) {

			WORD wData = pData[n];

			switch( Addr.a.m_Table ) {
			
				case 'J':
				case 'B':
				case 'C':
				case 'K':
				case 'M':
				case 'P':
				case 'F':
				case 'Q':
					wData = ToBCD(wData);
					break;
				}

			AddHex(wData, 0x1000);
			}

		if( Transact() ) {

			return uCount;
			}
		
		if( m_uError == 0x0001 && SelectMonitor() ) {

			continue;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR;
	}

CCODE COmronDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType = 0;

	switch( Addr.a.m_Table ) {
	
		case 'I':	bType = 'R';	break;	/* IR bin */
		case 'J':	bType = 'R';	break;	/* IR bcd */
		case 'D':	bType = 'D';	break;	/* DM bin */
		case 'B':	bType = 'D';	break;	/* DM bcd */
		case 'A':	bType = 'J';	break;	/* AR bin */
		case 'C':	bType = 'J';	break;	/* AR bcd */
		case 'H':	bType = 'H';	break;	/* HR bin */
		case 'K':	bType = 'H';	break;	/* HR bcd */
		case 'L':	bType = 'L';	break;	/* LR bin */
		case 'M':	bType = 'L';	break;	/* LR bcd */		
		case 'P':	bType = 'C';	break;	/* PV bcd */		
		case 'E':	bType = 'E';	break;	/* EM bin */
		case 'F':	bType = 'E';	break;	/* EM bcd */		
		case 'O':	bType = 'R';	break;	/* IO bin */
		case 'Q':	bType = 'R';	break;	/* IO bcd */

	     	default:	return CCODE_ERROR | CCODE_HARD;
		}

	uCount = min(uCount, 12);

	for( UINT t = 0; t < 2; t++ ) {

		StartFrame('W', bType);

		AddDec(Addr.a.m_Offset, 1000);

		for( UINT n = 0; n < uCount; n++ ) {

			DWORD dwData = pData[n];

			switch( Addr.a.m_Table ) {
			
				case 'J':
				case 'B':
				case 'C':
				case 'K':
				case 'M':
				case 'P':
				case 'F':
				case 'Q':
					dwData = ToBCD(dwData);
					break;
				}

			AddHex(LOWORD(dwData), 0x1000);

			AddHex(HIWORD(dwData), 0x1000);
			}

		if( Transact() ) {

			return uCount;
			}
		
		if( m_uError == 0x0001 && SelectMonitor() ) {

			continue;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR;
	}

// Implementation

void COmronDriver::StartFrame(BYTE bCmd, BYTE bType)
{
	m_uPtr   = 0;

	m_bCheck = 0;

	AddByte(0x00);
	
	AddByte('@');

	AddByte('0' + (m_pCtx->m_bDrop / 10));
	AddByte('0' + (m_pCtx->m_bDrop % 10));
	
	AddByte(bCmd);
	
	AddByte(bType);
	}

void COmronDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;

	m_bCheck ^= bData;
	}

void COmronDriver::AddDec(UINT uData, UINT uFactor)
{
	while( uFactor ) {

		AddByte('0' + (uData / uFactor) % 10);

		uFactor /= 10;
		}
	}

void COmronDriver::AddHex(UINT uData, UINT uFactor)
{
	while( uFactor ) {

		AddByte(m_pHex[(uData / uFactor) % 16]);

		uFactor /= 16;
		}
	}

BOOL COmronDriver::Transact(void)
{
	return TxFrame() && RxFrame();
	}

BOOL COmronDriver::TxFrame(void)
{
	m_bTx[m_uPtr++] = m_pHex[m_bCheck / 16];
	
	m_bTx[m_uPtr++] = m_pHex[m_bCheck % 16];

	m_bTx[m_uPtr++] = '*';

	m_bTx[m_uPtr++] = '\r';

	m_pData->ClearRx();

	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL COmronDriver::RxFrame(void)
{
	BYTE bCheck = 0;

	UINT uPtr   = 0;

	UINT uState = 0;

	UINT uTimer = 0;

	UINT uData  = 0;

	SetTimer(TIMEOUT);

	m_uError = NOTHING;

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( uState < 7 ) {

			bCheck ^= uData;
			}
			
		switch( uState ) {
		
			case 0:
				if( uData == '@' ) {
					
					bCheck = uData;
					
					uState = 1;
					}
				break;
				
			case 1:
				if( uData == UINT('0' + (m_pCtx->m_bDrop / 10)) )
					uState = 2;
				else
					uState = 9;
				break;
				
			case 2:
				if( uData == UINT('0' + (m_pCtx->m_bDrop % 10)) )
					uState = 3;
				else
					uState = 9;
				break;
				
			case 3:
				if( uData == m_bTx[4] )
					uState = 4;
				else
					uState = 9;
				break;
				
			case 4:
				if( uData == m_bTx[5] )
					uState = 5;
				else
					uState = 9;
				break;
				
			case 5:
				m_uError = uData - '0';
				
				uState = 6;
				
				break;
				
			case 6:
				if( (m_uError = 10 * m_uError + uData - '0') )
					uState = 9;
				else {
					uPtr   = 0;
					
					uState = 7;
					}
				break;
				
			case 7:
				if( uData == '*' )
					uState = 8;
				else {
					if( uData == '\r' ) {
						
						SetTimer(TIMEOUT);

						m_pData->Write(uData, FOREVER);
						
						bCheck = 0;
						
						uPtr  -= 2;
						}
					else {
						if( uPtr < sizeof(m_bRx) ) {

							m_bRx[uPtr++] = uData;
							}
						else
							uState = 9;
						}
					}
				break;
				
			case 8:
				if( uData == '\r' ) {
					
					for( UINT n = 0; n < uPtr - 2; n++ ) {

						bCheck ^= m_bRx[n];
						}
						
					if( m_bRx[uPtr - 2] != m_pHex[bCheck / 16] ) {

						return FALSE;
						}
						
					if( m_bRx[uPtr - 1] != m_pHex[bCheck % 16] ) {

						return FALSE;
						}
						
					m_bRx[uPtr - 2] = 0;

					return TRUE;
					}

				return FALSE;
				
			case 9:
				if( uData == '\r' ) {

					return FALSE;
					}
				break;
			}
		}

	return FALSE;
	}

BOOL COmronDriver::SelectMonitor(void)
{
	StartFrame('S', 'C');

	AddByte('0');
	
	AddByte('2');

	return Transact();
	}

UINT COmronDriver::xtoin(PTXT pData, UINT uCount)
{
	char c = pData[uCount];

	pData[uCount] = 0;

	UINT d = strtoul(pData, NULL, 16);

	pData[uCount] = c;

	return d;
	}

WORD COmronDriver::ToBin(WORD wData)
{
	WORD wWork = 0;
	
	WORD wHex  = 0x1000;

	WORD wDec  = 1000;
	
	while( wHex ) {
	
		wWork += wDec * ((wData / wHex) % 16);

		wHex /= 16;
		wDec /= 10;
		}
	
	return wWork;
	}

DWORD COmronDriver::ToBin(DWORD dwData)
{
	DWORD dwWork = 0;
	
	DWORD dwHex  = 0x10000000;

	DWORD dwDec  = 10000000;
	
	while( dwHex ) {
	
		dwWork += dwDec * ((dwData / dwHex) % 16);

		dwHex /= 16;
		dwDec /= 10;
		}
	
	return dwWork;
	}

WORD COmronDriver::ToBCD(WORD wData)
{
	WORD wWork = 0;
	
	WORD wHex  = 0x1000;

	WORD wDec  = 1000;
	
	while( wHex ) {
	
		wWork += wHex * ((wData / wDec) % 10);

		wHex /= 16;
		wDec /= 10;
		}
	
	return wWork;
	}

DWORD COmronDriver::ToBCD(DWORD dwData)
{
	DWORD dwWork = 0;
	
	DWORD dwHex  = 0x10000000;

	DWORD dwDec  = 10000000;
	
	while( dwHex ) {
	
		dwWork += dwHex * ((dwData / dwDec) % 10);

		dwHex /= 16;
		dwDec /= 10;
		}
	
	return dwWork;
	}

DWORD COmronDriver::SwapWords(DWORD dwData)
{
	return MAKELONG(HIWORD(dwData), LOWORD(dwData));
	}

// End of File
