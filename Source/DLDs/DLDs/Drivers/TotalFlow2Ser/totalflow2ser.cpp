
#include "intern.hpp"

#include "totalflow2ser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Serial Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

INSTANTIATE(CTotalFlow2SerMasterDriver);

// Constructor

CTotalFlow2SerMasterDriver::CTotalFlow2SerMasterDriver(void)
{
	m_Ident	 = DRIVER_ID;

	m_pCtx	 = NULL;

	m_fDelay = FALSE;
	}

// Destructor

CTotalFlow2SerMasterDriver::~CTotalFlow2SerMasterDriver(void)
{
	}

// Configuration

void MCALL CTotalFlow2SerMasterDriver::Load(LPCBYTE pData)
{
	}

void MCALL CTotalFlow2SerMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);

	if( Config.m_uPhysical == physicalRS485 || Config.m_uPhysical == physicalRS422Master ) {

		m_fDelay = TRUE;
		}
	}

// Management

void MCALL CTotalFlow2SerMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CTotalFlow2SerMasterDriver::Open(void)
{
	}
		
// Device

CCODE MCALL CTotalFlow2SerMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CCtx;

			m_pCtx->m_Name		= GetString(pData);
			m_pCtx->m_Code		= GetString(pData);
			m_pCtx->m_uUpdate	= GetLong(pData);
			m_pCtx->m_uTimeout	= GetLong(pData);
			m_pCtx->m_uLastUpdate	= 0;
			m_pCtx->m_fForceUpdate	= FALSE;
			m_pCtx->m_fUseAppKey	= GetByte(pData) ? TRUE : FALSE;

			GetAppKey(pData, m_pCtx);

			GetSlots(pData, m_pCtx);

			m_pCtx->m_uEstab = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_wUnkey = GetWord(pData);

			CTotalFlow2Master::m_pCtx = m_pCtx;

			pDevice->SetContext(m_pCtx);

			LookupApps();

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	CTotalFlow2Master::m_pCtx = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CTotalFlow2SerMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		CleanupSlots();

		Free(m_pCtx->m_Name);

		Free(m_pCtx->m_Code);

		delete [] m_pCtx->m_pKey;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CTotalFlow2Master::DeviceClose(fPersist);
	}
		
// Entry Points

CCODE MCALL CTotalFlow2SerMasterDriver::Ping (void)
{
	AbortLink();

	return CCODE_SUCCESS;
	}

// Transport

BOOL CTotalFlow2SerMasterDriver::CheckLink(void)
{
	return TRUE;
	}

void CTotalFlow2SerMasterDriver::AbortLink(void)
{
	while( Recv(500) < NOTHING );
	}

BOOL CTotalFlow2SerMasterDriver::Send(PBYTE pBuff, UINT uLength)
{
	if( m_fDelay ) {

		Sleep(m_pCtx->m_wUnkey);
		}

	m_pData->ClearRx();

	m_pData->Write(pBuff, uLength, NOTHING);

	m_pData->Write(NULL, 0, NOTHING);

	return TRUE;
	}

UINT CTotalFlow2SerMasterDriver::Recv(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
