
#include "totalflow2base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Serial Driver
//

class CTotalFlow2SerMasterDriver : public CTotalFlow2Master
{
	public:
		// Constructor
		CTotalFlow2SerMasterDriver(void);

		// Destructor
		~CTotalFlow2SerMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:

		struct CCtx : public CBaseCtx
		{			
			WORD	  m_wUnkey;
			};

		CCtx	* m_pCtx;
		BOOL	  m_fDelay;

		// Transport
		BOOL CheckLink(void);
		void AbortLink(void);
		BOOL Send(PBYTE pBuff, UINT uLength);
		UINT Recv(UINT uTime);
	};

// End of File
