
//////////////////////////////////////////////////////////////////////////
//
// Euro631 Driver
//

#define EURO631_ID 0x400D

#define	TRO	0
#define	TWO	1
#define	TRW	2
#define TRC	3 // cached value for read operation
#define TWC	4 // cached value for write operation
#define TWW	5 // write cache
#define	TWR	6 // first position in cache is sent for a read
#define TWX	7 // don't transmit on write
#define	TFC	8 // Read Data From Cache
#define	TTC	9 // Read Data Into Cache
#define	TXX	10 // Error Code

#define OPRD	0
#define OPWR	1

struct FAR EURO631CmdDef {
	UINT	uID;
	/* Description */
	BYTE	bOP;
	UINT	uInfo;
// uInfo depends on Type
// TRW - size of data in bits 0-3
// TRO - size of data in bits 0-3, position of data in read response in bits 4-11
// TWO - size of data in bits 0-3
// TWC - size of data in bits 0-3, position of data in read response in bits 4-11
// TWW - uID of first item in list for writing cache
// TRC - Value to be sent for read operation.
// TWR - size of data in bits 0-3, position of data in read response in bits 4-11, cache[0] sent on read
	BYTE	bSize;
	BYTE	Type;	
	};

class CEuro631Driver : public CMasterDriver
{
	public:
		// Constructor
		CEuro631Driver(void);

		// Destructor
		~CEuro631Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Data Members
		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		WORD	m_uPtr;
		BYTE	m_bDrop;
		BYTE	m_bCheck;
		DWORD	m_Error;
		static	EURO631CmdDef CODE_SEG CL[];
		EURO631CmdDef FAR * m_pCL;
		EURO631CmdDef FAR * m_pItem;

	#pragma pack(1)

		struct W23 { // Positioning Command 
			BYTE A;
			WORD B;
			WORD C;
			WORD D;
			WORD E;
			DWORD F;
			};

		struct W65 { // Configuration
			BYTE A;
			WORD B;
			BYTE C;
			BYTE D;
			WORD E;
			WORD F;
			WORD G;
			WORD H;
			WORD I;
			WORD J;
			WORD K;
			WORD L;
			WORD M;
			WORD N;
			WORD O;
			WORD P;
			WORD Q;
			WORD R;
			};

		struct W66 {
			WORD A;
			WORD B;
			WORD C;
			WORD D;
			WORD E;
			WORD F;
			WORD G;
			WORD H;
			WORD I;
			WORD J;
			WORD K;
			};

		struct W67 {
			WORD A;
			WORD B;
			WORD C;
			WORD D;
			WORD E;
			WORD F;
			WORD G;
			WORD H;
			WORD I;
			};

		struct W68 {
			WORD A;
			WORD B;
			WORD C;
			WORD D;
			WORD E;
			WORD F;
			WORD G;
			};

		struct W69 {
		//	BYTE A; // Saved in m_TRCCache
			BYTE B;
			WORD C;
			WORD D;
			WORD E;
			WORD F;
			DWORD G;
			};

		struct W72 {
		//	BYTE A; // Saved in m_TRCCache
			BYTE B;
			BYTE C;
			WORD D;
			WORD E;
			WORD F;
			WORD G;
			WORD H;
			WORD I;
			WORD J;
			WORD K;
			WORD L;
			WORD M;
			WORD N;
			WORD O;
			WORD P;
			DWORD Q;
			DWORD R;
			DWORD S;
			DWORD T;
			DWORD U;
			DWORD V;
			BYTE W;
			DWORD X;
			DWORD Y;
			DWORD Z;
			};

		struct W73 {
		//	BYTE A; // Saved in m_TRCCache
			DWORD B;
			DWORD C;
			DWORD D;
			DWORD E;
			DWORD F;
			DWORD G;
			DWORD H;
			DWORD I;
			DWORD J;
			DWORD K;
			DWORD L;
			DWORD M;
			DWORD N;
			DWORD O;
			DWORD P;
			DWORD Q;
			};

		struct W74 {
			BYTE A;
			BYTE B;
			BYTE C;
			BYTE D;
			BYTE E;
			BYTE F;
			BYTE G;
			BYTE H;
			BYTE I;
			BYTE J;
			BYTE K;
			BYTE L;
			};

		struct W76 {
		//	WORD A; // Saved in m_TRCCache
			BYTE B;
			BYTE C;
			BYTE D;
			BYTE E;
			BYTE F;
			BYTE G;
			BYTE H;
			BYTE I;
			};

		struct W78 {
			WORD A;
			BYTE B;
			BYTE C;
			WORD D;
			WORD E;
			WORD F;
			BYTE G;
			BYTE H;
			WORD I;
			};

		#pragma pack()

		struct W23	s23;
		struct W65	s65;
		struct W66	s66;
		struct W67	s67;
		struct W68	s68;
		struct W69	s69;
		struct W72	s72;
		struct W73	s73;
		struct W74	s74;
		struct W76	s76;
		struct W78	s78;

		WORD	m_TRCCache[4];
		BYTE	m_ReadControl[20];

		PBYTE m_pCache;
	
		// Opcode Handlers
		void	ReadOpcode(AREF Addr);
		void	ReadNamedOpcode(void);
		BOOL	DoFGPOpcode(UINT uAddr, UINT uCount, PDWORD pData);
		void	WriteOpcode(AREF Addr, PDWORD pData);
		void	WriteNamedOpcode(PDWORD pData);
		
		// Frame Building
		void	StartFrame(BOOL fIsRegWrite );
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, UINT uSize);
		void	AddCachedData(void);
		void	Add67Data(void);
		void	AddCommand( BOOL fIsRegWrite );
		void	AddTRCData(void);
		
		// Transport Layer
		BOOL	Transact(BYTE bSize);
		void	Send(void);
		BOOL	GetReply(BYTE bSize);

		// Response Handling
		BOOL	GetRegResponse(AREF Addr, UINT uCount, PDWORD pData);
		void	GetFGPResponse(PDWORD pRsp);
		BOOL	GetNamedResponse(PDWORD pData);
		
		// Port Access
		void	Put(void);
		WORD	Get(void);

		// Helpers
		EURO631CmdDef *	GetpItem( UINT uID );
		EURO631CmdDef * GetpFirstItem( void );
		EURO631CmdDef * GetpReadControl( void );
		BOOL	ReadNoTransmit( PDWORD pData );
		BOOL	WriteNoTransmit( PDWORD pData );
		void	PutDataInCache( UINT uPos, UINT uCt, DWORD dData );
		UINT	GetReadPosition(EURO631CmdDef * p);
		UINT	GetItemSize(EURO631CmdDef * p);
		BYTE	GetRxSize(void);
		void	SetCachePointer(void);
		DWORD	PickData(BOOL bFromCache);
		void	ReadDataIntoCache(void);
		BOOL	CheckCacheRead( PDWORD pData );
		DWORD	Reverse( DWORD dData );
		WORD	Reverse( WORD wData );
	};

// Table comms definitions
#define	VAR	1
#define	FLG	2
#define	FGP	3

// Cache Sizes

#define Z23 sizeof(W23)
#define Z65 sizeof(W65)
#define Z66 sizeof(W66)
#define Z67 sizeof(W67)
#define Z68 sizeof(W68)
#define Z69 sizeof(W69)
#define Z72 sizeof(W72)
#define Z73 sizeof(W73)
#define Z74 sizeof(W74)
#define Z76 sizeof(W76)
#define Z78 sizeof(W78)


// End of File
