#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "indraclc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Driver
//

// Instantiator

INSTANTIATE(CIndramatCLCDriver);

// Constructor

CIndramatCLCDriver::CIndramatCLCDriver(void)
{
	m_Ident	    = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex	    = Hex;
	}

// Destructor

CIndramatCLCDriver::~CIndramatCLCDriver(void)
{
	}

// Configuration

void MCALL CIndramatCLCDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CIndramatCLCDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CIndramatCLCDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CIndramatCLCDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CIndramatCLCDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			memset( bERR, 0, sizeof(bERR) );
			memset( bTWC, 0, sizeof(bTWC) );
			memset( bTRC, 0, sizeof(bTRC) );
			memset( bRXD, 0, sizeof(bRXD) );

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS; 
	}

CCODE MCALL CIndramatCLCDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CIndramatCLCDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPCARD;
	Addr.a.m_Offset = PINGVALUE;
	Addr.a.m_Type   = addrLongAsLong; // config type is real, need int to test for chksum
	Addr.a.m_Extra  = 0;

	if( Read(Addr, Data, 1) == 1 ) {

		m_fChecksumOn = Data[0] & 1;

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CIndramatCLCDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( AccessCache( Addr, pData, &uCount, TRUE ) ) {

		return uCount;
		}

	m_ReqCount = uCount;

	SetParamDef(Addr);

	return DoRead(Addr, pData);
	}

CCODE MCALL CIndramatCLCDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace0("\r\n**** WRITE ****");

	if( AccessCache( Addr, pData, &uCount, FALSE ) ) {

		return uCount;
		}

	if( Addr.a.m_Table == SPTXW || Addr.a.m_Table == SPTXR ) {

		ExecuteTextFunction(Addr, pData);

		return 1;
		}

	SetParamDef(Addr);

	if( DoRead( Addr, NULL ) == 1 ) {

		return DoWrite(Addr, *pData);
		}

	return m_fErr ? CCODE_NO_RETRY : CCODE_ERROR;
	}

CCODE CIndramatCLCDriver::DoRead(AREF Addr, PDWORD pData)
{
	StartFrame();

	if( Transact(FALSE) ) {

		GetDataDef(m_uPtr);

		if( pData ) { // pData = NULL if setting data type for Write

			*pData = GetData(Addr.a.m_Table == SPIOB, Addr.a.m_Type);
			}

		return 1;
		}

	if( m_fErr ) {

		if( m_ReqCount > 1 ) {

			return 1; // found invalid parameter in list of parameters
			}

		return CCODE_NO_RETRY;
		}

	return CCODE_ERROR;
	}

CCODE CIndramatCLCDriver::DoWrite(AREF Addr, DWORD dData)
{
	if( Addr.a.m_Table == SPIOB ) {

		m_ParamDef.bSubClass = SCIOB;
		}

	StartFrame();

	AddWriteData( Addr.a.m_Type, dData );

	if( Transact(FALSE) ) {

		if( Addr.a.m_Table == SPCARD && Addr.a.m_Offset == 3 ) { // Checksum change?

			if( Ping() != 1 ) {

				return CCODE_NO_RETRY;
				}
			}

		return 1;
		}

	return CCODE_NO_RETRY;
	}

// Application Layer

// Frame Building

void CIndramatCLCDriver::StartFrame(void)
{
	m_uPtr = 0;

	AddByte( '>' );
	AddByte( m_pHex[m_pCtx->m_bDrop] );
	AddByte( ' ' );
	AddByte( m_ParamDef.bClass );
	AddByte( m_ParamDef.bSubClass );
	AddByte( ' ' );

	if( m_ParamDef.bSet[1] ) {

		AddByte( m_ParamDef.bSet[1] );
		}

	AddByte( m_ParamDef.bSet[0] );

	AddByte( '.' );
	AddValue( m_ParamDef.uAddr );
	AddByte( ' ' );
	return;
	}

void CIndramatCLCDriver::EndFrame(void)
{
	AddByte(CR);
	AddByte(LF);
	}

void CIndramatCLCDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CIndramatCLCDriver::AddValue(DWORD dValue)
{
	memset( m_sWork, 0, sizeof( m_sWork ) );

	SPrintf( m_sWork, "%d", dValue );

	AddString();
	}

void CIndramatCLCDriver::AddBin(DWORD dData)
{
	PutGeneric( HIBYTE(LOWORD(dData)), 2, 128 );
	PutGeneric( LOBYTE(LOWORD(dData)), 2, 128 );
	}

void CIndramatCLCDriver::MakeDec(DWORD dData)
{
	memset( m_sWork, 0, sizeof(m_sWork) );

	if( dData == 0x80000000 ) {

		dData = 0;
		}

	SPrintf( m_sWork, "%f", PassFloat(dData) );
	}

void CIndramatCLCDriver::AddHex16(DWORD dData)
{
	AddByte('0');
	AddByte('x');
	PutGeneric( LOWORD(dData), 16, 4096 );
	}

void CIndramatCLCDriver::AddHex32(DWORD dData)
{
	AddHex16(HIWORD(dData));
	PutGeneric(LOWORD(dData), 16, 4096 );
	}

void CIndramatCLCDriver::WriteOneBit(DWORD dData)
{
	UINT uMask = 1 << m_ParamDef.bBitPos;

	UINT uData = dData ? uMask : 0;

	AddHex16( uMask );

	PutGeneric( LOWORD(uData), 16, 4096 );
	}

void CIndramatCLCDriver::AddString(void)
{
	UINT len = strlen(m_sWork);

	memcpy(&m_bTx[m_uPtr], m_sWork, len);

	m_uPtr += len;
	}

void CIndramatCLCDriver::AddWriteData(UINT uType, DWORD dData)
{
	if( uType == addrRealAsReal && m_DataDef.uType != CLC_DEC ) {

		dData = RealToInt(dData);
		}

	if( m_ParamDef.bSubClass == SCIOB ) { // single bit write

		WriteOneBit( dData ? 1 : 0 );
		}

	else {
		switch( m_DataDef.uType ) {

			case CLC_DEC:
				if( uType == addrRealAsReal ) {

					MakeDec(dData);

					AddString();
					}

				else {
					AddValue(dData);
					}
				break;

			case CLC_BIN:
				AddBin(dData);
				break;

			case CLC_HEX16:
				AddHex16(dData);
				break;

			case CLC_HEX32:
				AddHex32(dData);
				break;

			case CLC_INT:
				AddValue(dData);
				break;
			}
		}

	AddWriteChecksum();
	}

void CIndramatCLCDriver::AddWriteChecksum(void)
{
	if( m_fChecksumOn ) {

		AddByte( ' ' );

		BYTE bCheck = MakeTxChecksum();

		AddByte( '$' );
		AddByte( m_pHex[bCheck/16] );
		AddByte( m_pHex[bCheck%16] );
		}
	}

// Text Function Execution
void CIndramatCLCDriver::ExecuteTextFunction(AREF Addr, PDWORD pData)
{
	BOOL fIsRead = Addr.a.m_Table == SPTXR;

	FillWorkString(fIsRead);

	m_uPtr = 0;

	AddByte( '>' );
	AddByte( m_pHex[m_pCtx->m_bDrop] );
	AddByte( ' ' );
	AddString();

	if( !fIsRead ) AddWriteChecksum();

	memset( m_bRx, 0, sizeof(m_bRx) );

	if( Transact(TRUE) && fIsRead ) FillRcvCache();
	}

void CIndramatCLCDriver::FillWorkString(BOOL fIsRead)
{
	UINT	uPos  = 0;
	BYTE	b;

	while( uPos < (fIsRead ? sizeof(bTRC) : sizeof(bTWC)) - 1 ) {

		b = fIsRead ? bTRC[uPos] : bTWC[uPos];

		if( !b ) break;

		m_sWork[uPos++] = b;
		}

	m_sWork[uPos++] = 0;
	}

void CIndramatCLCDriver::FillRcvCache(void)
{
	UINT uEnd;

	if( FindChar( 0, &uEnd, CR ) ) {

		UINT uStart;

		if( FindChar( 6, &uStart, ' ' ) ) { // Start data 1st space after header and set#

			memset( bRXD, 0, sizeof(bRXD) );

			for( UINT i = 0; i < uEnd - uStart; i++ ) {

				bRXD[i] = m_bRx[uStart + i];
				}

			return;
			}
		}

	memset( bRXD, '?', 3 );
	bRXD[3] = 0;
	}

// Transport
	
BOOL CIndramatCLCDriver::Transact(BOOL fIsText)
{
	EndFrame();

	Send();
				
	return GetFrame(fIsText);
	}
	
void CIndramatCLCDriver::Send(void)
{
	m_pData->ClearRx();

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);

//**/	AfxTrace0("\r\n"); for(UINT k=0; k<m_uPtr; k++) AfxTrace1("[%2.2x]", m_bTx[k]);
	}

BOOL CIndramatCLCDriver::GetFrame(BOOL fIsText)
{
	UINT uState = fIsText ? 1 : 0;

	UINT uTimer = 500;

	UINT uData;

	BYTE bData;
	
	UINT uPtr = 0;
	
	SetTimer(uTimer);

//**/	AfxTrace0("\r\n");

	while( ( uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		if( uPtr >= sizeof( m_bRx ) ) return FALSE;

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);
		
		switch( uState ) {
		
			case 0:
				if( bData == '>' ) {
					
					uPtr          = 0;
					
					uState        = 1;

					m_bRx[uPtr++] = '>';
					}
				
				break;

			case 1:
				m_bRx[uPtr++] = bData;

				if( bData == LF ) return fIsText ? TRUE : CheckReply();

				break;
			}
		}
		
	return FALSE;
	}

// Response Processing

BOOL CIndramatCLCDriver::CheckReply(void)
{
	m_fErr = FALSE;

	if( !CheckReplyDrop())		return FALSE;

	if( !CheckReplyNoError())	return FALSE;

	UINT uPos = 2;

	if( !CheckReplyClass(&uPos))	return FALSE;
	if( !CheckReplySubClass(&uPos))	return FALSE;
	if( !CheckReplySet(&uPos))	return FALSE;
	if( !CheckReplyAddr(&uPos))	return FALSE;

	m_uPtr = m_bRx[uPos] == ' ' ? uPos+1 : uPos;

	return TRUE;
	}

BOOL CIndramatCLCDriver::CheckReplyDrop(void)
{
	return xtoin(m_bRx[1]) == m_pCtx->m_bDrop;
	}

BOOL CIndramatCLCDriver::CheckReplyClass(UINT * pPos)
{
	return GetACharacter(pPos) == m_ParamDef.bClass;
	}

BOOL CIndramatCLCDriver::CheckReplySubClass(UINT * pPos)
{
	return GetACharacter(pPos) == m_ParamDef.bSubClass;
	}

BOOL CIndramatCLCDriver::CheckReplySet(UINT * pPos)
{
	BYTE b0 = m_ParamDef.bSet[0];
	BYTE bR = GetACharacter(pPos);

	if( m_ParamDef.bClass != CLTASK ) {

		BYTE b1 = m_ParamDef.bSet[1];

		if( b1 ) {

			if( b1 != bR ) return FALSE;

			bR = GetACharacter(pPos);
			}
		}

	return bR == b0;
	}

BOOL CIndramatCLCDriver::CheckReplyAddr(UINT * pPos)
{
	if( m_bRx[*pPos] == '.' ) {

		*pPos = (*pPos) + 1;

		return GetAnInteger(pPos) == m_ParamDef.uAddr;
		}

	return FALSE;
	}

BOOL CIndramatCLCDriver::CheckReplyNoError(void)
{
	UINT uFound  = 0;

	m_fErr = FALSE;

	if( FindChar( uFound, &uFound, '!' ) ) {

		m_fErr = TRUE;

		if( m_ReqCount > 1 ) {

			if( GetGeneric( 10, 2, uFound ) == INVALID_PARNO ) {

				return FALSE; // Don't log inv. par in list of par's.
				}
			}

		UINT uPut = 0;

		uFound    = 1;

		BYTE b;

		while( uPut < sizeof(bERR) ) {

			b = m_bRx[uFound++];

			if( b == '$' || b == CR || !b ) {

				bERR[uPut] = 0;

				return FALSE;
				}

			bERR[uPut++] = b;
			}

		bERR[sizeof(bERR)-1] = 0;

		return FALSE;
		}

	return TRUE;
	}

// Port Access

UINT CIndramatCLCDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

void CIndramatCLCDriver::PutGeneric(DWORD dData, UINT uBase, UINT uFactor)
{
	while( uFactor ) {
	
		AddByte( m_pHex[(dData / uFactor) % uBase] );
		
		uFactor /= uBase;
		}
	}

DWORD CIndramatCLCDriver::GetGeneric(UINT uBase, UINT uLength, UINT uOffset)
{
	BYTE *pData = m_bRx + uOffset;

	DWORD dData = 0;
	
	BOOL fNeg   = FALSE;
	
	while( uLength-- ) {
	
		char cData = (char) *(pData++);
		
		if( cData == '-' ) fNeg = TRUE;

		else {
			if( IsHexDigit(cData) ) dData = dData * uBase + xtoin(cData);

			else break;
			}
		}

	return fNeg ? -dData : dData;
	}

void CIndramatCLCDriver::SetParamDef(AREF Addr)
{
	m_ParamDef.uAddr     = Addr.a.m_Offset & 0xFFF;
	m_ParamDef.bSubClass = SCPAR;
	m_ParamDef.bBitPos   = 0;

	UINT u = (Addr.a.m_Offset >> 12);
	if( Addr.a.m_Extra == 1 ) u += 16;

	m_ParamDef.bSet[0] = m_pHex[u % 10];
	m_ParamDef.bSet[1] = m_pHex[u / 10];

	switch( Addr.a.m_Table ) {

		case SPINT:	m_ParamDef.bClass = CLINT;	return;
		case SPREAL:	m_ParamDef.bClass = CLREAL;	return;
		case SPGINT:	m_ParamDef.bClass = CLGINT;	return;
		case SPGREAL:	m_ParamDef.bClass = CLGREAL;	return;
		case SPAXIS:	m_ParamDef.bClass = CLAXIS;	return;
		case SPCARD:	m_ParamDef.bClass = CLCARD;	return;
		case SPSER:	m_ParamDef.bClass = CLSER;	return;

		case SPIOW:
		case SPIOB:
			m_ParamDef.bSet[0]	= '0';
			m_ParamDef.bSet[1]	= 0;
			m_ParamDef.bBitPos	= u;
			m_ParamDef.bClass	= CLIOW;
			m_ParamDef.bSubClass	= SCIOW;
			return;

		case SPTASK:
			m_ParamDef.bSet[0]	= (u % 4) + 'A'; // spec says A-D, only
			m_ParamDef.bSet[1]	= 0;
			m_ParamDef.bClass	= CLTASK;
			return;
		}
	}

void CIndramatCLCDriver::GetDataDef(UINT uPos)
{
	InitDataDef();

	BOOL fZero   = FALSE;

	BOOL fBinary = TRUE;

	BYTE *pData  = m_bRx + uPos;

	UINT uState  = 0;

	for(;;) {

		char cData = (char) *(pData++);

		if( cData == ' ' || cData == '$' ) break;

		m_DataDef.uLen++;

		switch( uState ) {

			case 0:
				if( cData == '.' ) {

					uState = 2;

					m_DataDef.uType = CLC_DEC;

					break;
					}

				if( fZero ) {

					if( cData == 'x' ) {

						m_uPtr += 2;

						m_DataDef.uLen = 0;

						m_DataDef.uType = CLC_HEX16;

						uState = 3;

						break;
						}

					else fZero = FALSE;
					}

				if( cData == '0' ) fZero = TRUE;

				if( fBinary && !IsBinary(cData) ) fBinary = FALSE;

				break;

			case 2:
				break;

			case 3:
				break;
			}
		}		

	if( m_DataDef.uLen == 16 && fBinary ) {

		m_DataDef.uType = CLC_BIN;

		return;
		}

	if( m_DataDef.uLen == 8 && m_DataDef.uType == CLC_HEX16 ) {
		
		m_DataDef.uType = CLC_HEX32;

		return;
		}
	}

void CIndramatCLCDriver::InitDataDef(void)
{
	m_DataDef.uType = CLC_INT;

	m_DataDef.uLen = 0;
	}

DWORD CIndramatCLCDriver::GetData(BOOL fWantBit, UINT uType)
{
	DWORD d;

	switch(m_DataDef.uType) {
	
		case CLC_HEX32:
		case CLC_HEX16:	d = GetGeneric(16, m_DataDef.uLen, m_uPtr);	break;

		case CLC_BIN:	d = GetGeneric( 2, m_DataDef.uLen, m_uPtr);	break;

		case CLC_INT:	d = GetGeneric(10, m_DataDef.uLen, m_uPtr);	break;
			
		case CLC_DEC:	return GetRealData();

		default:	return uType != addrRealAsReal ? 0xFFFFFFFF : 0xBF800000;
		}

	if( uType == addrRealAsReal ) return HexToReal(d);

	return fWantBit ? GetBitData(d) : d;
	}

DWORD CIndramatCLCDriver::GetBitData(DWORD dData)
{
	return (1 << m_ParamDef.bBitPos) & LOWORD(dData) ? 1 : 0;
	}

DWORD CIndramatCLCDriver::GetRealData(void)
{
	BOOL fNeg = FALSE;
	DWORD d;

	if( m_bRx[m_uPtr] == '-' ) {

		fNeg = TRUE;
		m_uPtr++;
		}

	d = ATOF( (const char *)(m_bRx + m_uPtr) );

	return fNeg ? d | 0x80000000 : d;
	}

DWORD CIndramatCLCDriver::HexToReal(DWORD dHex)
{
	BOOL fNeg = FALSE;

	char s[32];

	DWORD d;

	if( dHex & 0x80000000 ) {

		dHex = -dHex;
		fNeg = TRUE;
		}

	SPrintf( s, "%d", dHex );

	d = ATOF( (const char *)(s) );

	return fNeg ? d | 0x80000000 : d;
	}

DWORD CIndramatCLCDriver::RealToInt(DWORD dReal)
{
	MakeDec(dReal);

	BOOL fDone = FALSE;

	UINT i     = 0;

	dReal      = 0;

	while( !fDone ) {

		switch( m_sWork[i] ) {

			case 0:	fDone = TRUE;	break;

			case '-':		break;

			case '.':
				if( m_sWork[i+1] > '4' ) dReal++;

				fDone = TRUE;

				break;

			default:
				if( IsDigit( m_sWork[i] ) ) dReal = (dReal * 10) + xtoin(m_sWork[i]);

				else fDone = TRUE;

				break;
			}

		if( ++i >= strlen(m_sWork) ) fDone = TRUE;
		}

	return m_sWork[0] == '-' ? -dReal : dReal;
	}

BOOL CIndramatCLCDriver::IsIO(UINT uTable)
{
	return uTable == SPIOB || uTable == SPIOW;
	}

BOOL CIndramatCLCDriver::IsHexDigit(char cData)
{
	if( cData >= '0' && cData <= '9' ) return TRUE;
	if( cData >= 'A' && cData <= 'F' ) return TRUE;
	if( cData >= 'a' && cData <= 'f' ) return TRUE;

	return FALSE;
	}

BOOL CIndramatCLCDriver::IsDigit(char cData)
{
	return( cData >= '0' && cData <= '9' );
	}

BOOL CIndramatCLCDriver::IsBinary(char cData)
{
	return( cData == '0' || cData == '1' );
	}

UINT CIndramatCLCDriver::GetAnInteger(UINT * pRxPos)
{
	BYTE b;
	BOOL fGot1 = FALSE;
	DWORD d    = 0;

	for( UINT i = *pRxPos; i < sizeof(m_bRx); i++ ) {

		b = m_bRx[i];

		if( IsDigit(b) ) {

			d     *= 10;
			d     += xtoin(b);
			fGot1  = TRUE;
			}

		else {
			if( fGot1 ) {

				*pRxPos = i;

				return d;
				}

			else {
				if( b != ' ' ) return INVALID_INT;
				}
			}
		}

	return INVALID_INT;
	}

BYTE CIndramatCLCDriver::GetACharacter(UINT * pRxPos)
{
	UINT i = *pRxPos;

	BYTE b;

	while( i < sizeof(m_bRx) ) {

		b = m_bRx[i++];

		if( b != ' ' ) {

			*pRxPos = i;

			return b;
			}
		}

	return INVALID_CHAR;
	}

UINT CIndramatCLCDriver::xtoin(char cData)
{
	if( cData >= '0' && cData <= '9' ) return DIGI(cData);
	if( cData >= 'A' && cData <= 'F' ) return HEXU(cData);
	if( cData >= 'a' && cData <= 'f' ) return HEXL(cData);

	return 0;
	}		

BOOL CIndramatCLCDriver::FindChar(UINT uStart, UINT * pFound, BYTE bData)
{
	for( UINT i = uStart; i < sizeof(m_bRx); i++ ) {

		BYTE b = m_bRx[i];

		switch( b ) {

			case 0:
				return FALSE;

			case CR:
				if( bData == CR ) {

					*pFound = i;
					return TRUE;
					}

				return FALSE;

			case '$':
				if( bData != CR ) return FALSE;
				break;

			default:
				if( b == bData ) {

					*pFound = i + 1;

					return TRUE;
					}
				break;
			}
		}

	return FALSE;
	}

BYTE CIndramatCLCDriver::MakeTxChecksum(void)
{
	UINT uCk = 0;

	for( UINT i = 0; i < m_uPtr; i++ ) {

		uCk += m_bTx[i];
		}

	return MAKECS(uCk);
	}

BYTE CIndramatCLCDriver::MakeRxChecksum(void)
{
	UINT uCk = 0;

	for( UINT i = 0; m_bRx[i] != '$' && m_bRx[i] != CR; i++ ) {

		uCk += m_bRx[i];
		}

	return MAKECS(uCk);
	}

BOOL CIndramatCLCDriver::AccessCache(AREF Addr, PDWORD pData, UINT * pCount, BOOL fIsRead)
{
	PBYTE pD     = NULL;
	UINT  uDSz   = 8;

	switch( Addr.a.m_Table ) {

		case SPTXW:
		case SPTXR:
			if( fIsRead ) {

				*pData = 0;

				return TRUE;
				}

			return BOOL( !(*pData) );

		case SPERR:
			if( !fIsRead ) {

				memset( &bERR[0], 0, sizeof(bERR) );
				return TRUE;
				}

			uDSz = sizeof(bERR)/sizeof(DWORD);

			pD = bERR;
			
			break;

		case SPTWC:	pD = bTWC;	uDSz = sizeof(bTWC)/sizeof(DWORD);	break;

		case SPTRC:	pD = bTRC;	uDSz = sizeof(bTRC)/sizeof(DWORD);	break;

		case SPRXD:	pD = bRXD;	uDSz = sizeof(bRXD)/sizeof(DWORD);	break;

		default:			return FALSE;
		}

	UINT uDataStart = Addr.a.m_Offset & 0xF;
	UINT uDataCt    = min( *pCount, uDSz );
	UINT uDataPtr;

	UINT uArrStart;
	UINT uArrPtr;

	BYTE b1;
	BOOL fEndMarkFound = FALSE;
	UINT uShift   = 24;

	for( uDataPtr = 0; uDataPtr < uDataCt; uDataPtr++ ) {

		uArrStart  = (uDataStart + uDataPtr) * 4;

		if( fIsRead ) pData[uDataPtr] = 0;

		for( uArrPtr = uArrStart; uArrPtr < uArrStart + 4; uArrPtr++ ) {

			b1 = fIsRead ? pD[uArrPtr] : (pData[uDataPtr]) >> uShift;

			if( !b1 ) fEndMarkFound = TRUE;

			if( fEndMarkFound ) b1 = 0;

			fIsRead ? pData[uDataPtr] += DWORD(b1) << uShift : pD[uArrPtr] = b1;

			uShift  ? uShift -= 8 : uShift = 24;
			}
		}

	*pCount = uDataCt;

	return TRUE;
	}

// End of File
