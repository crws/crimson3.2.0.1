
//////////////////////////////////////////////////////////////////////////
//
// Indramat CLC Driver
//

// Data Spaces
#define	SPINT		1	// User Int
#define	SPREAL		2	// User Real
#define	SPGINT		3	// Global Int
#define	SPGREAL		4	// Global Real
#define	SPIOW		5	// I/O Word
#define	SPIOB		6	// I/O Bit
#define	SPAXIS		7	// Axis Parameter
#define	SPCARD		8	// Card Parameter
#define	SPTASK		9	// Task Parameter
#define	SPSER		10	// SERCOS Parameter
#define	SPERR		20	// Latest Error
#define	SPTWC		21	// User Write Command Text
#define	SPTXW		22	// Send User Write Command Text
#define	SPTRC		23	// User Read Command Text
#define	SPTXR		24	// Send User Read Command Text
#define	SPRXD		25	// Generic Command Response

// Class defs
#define	CLINT		'I'
#define	CLREAL		'F'
#define	CLGINT		'G'
#define	CLGREAL		'H'
#define	CLIOW		'R'
#define	CLIOB		'R'
#define	CLAXIS		'A'
#define	CLCARD		'C'
#define	CLTASK		'T'
#define	CLSER		'D'

// Subclass defs
#define	SCPAR		'P'
#define	SCIOW		'X'
#define	SCIOB		'M'

// Data Processing Types
#define CLC_DEC		0
#define CLC_HEX16	1
#define CLC_BIN		2
#define CLC_INT		3
#define CLC_HEX32	4

// Invalid Response Values
#define	INVALID_INT	NOTHING
#define	INVALID_CHAR	0
#define	INVALID_PARNO	2

// Hex digit Macros
#define	DIGI(w)	(w - 0x30) // '0'-'9' -> 0 - 9
#define	HEXU(w)	(w - 0x37) // 'A'-'F' -> 0xA - 0xF
#define	HEXL(w) (w - 0x57) // 'a'-'f' -> 0xA - 0xF

// Checksum Macro
#define	MAKECS(w) (LOBYTE(0x10000 - LOBYTE(w) - HIBYTE(w)))

// PING Setup
#define	PINGVALUE	3

// BYTE getting macro
#define BYTE4(d)	(HIBYTE(HIWORD(d)))
#define BYTE3(d)	(LOBYTE(HIWORD(d)))
#define BYTE2(d)	(HIBYTE(LOWORD(d)))
#define BYTE1(d)	(LOBYTE(LOWORD(d)))

class CIndramatCLCDriver : public CMasterDriver {

	public:
		// Constructor
		CIndramatCLCDriver(void);

		// Destructor
		~CIndramatCLCDriver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
			
		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);
		CCODE MCALL Read(AREF Addr, PDWORD pData, UINT uCount);
		CCODE MCALL Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			};
		CContext *	m_pCtx;

		// Data Members
		LPCTXT	m_pHex;

		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		UINT	m_uPtr;
		char	m_sWork[32];
		BOOL	m_fChecksumOn;
		BOOL	m_fErr;
		BOOL	m_ReqCount;

		struct	TParamDef {
			BYTE	bClass;
			BYTE	bSubClass;
			BYTE	bSet[2];
			UINT	uAddr;
			BYTE	bBitPos;
			};

		TParamDef	m_ParamDef;
		
		struct TDataDef {
			
			UINT	uType;
			UINT	uLen;
			};

		TDataDef	m_DataDef;

		// Internal Storage
		BYTE	bERR[40];
		BYTE	bTWC[32];
		BYTE	bTRC[32];
		BYTE	bRXD[64];
		
		// Implementation

		CCODE	DoRead(AREF Addr, PDWORD pData);
		CCODE	DoWrite(AREF Addr, DWORD dData);

		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddValue(DWORD dValue);
		void	AddBin(DWORD dData);
		void	WriteOneBit(DWORD dData);
		void	MakeDec(DWORD dData);
		void	AddHex16(DWORD dData);
		void	AddHex32(DWORD dData);
		void	AddString(void);
		void	AddWriteData(UINT uType, DWORD dData);
		void	AddWriteChecksum(void);

		// Text Function Execution
		void	ExecuteTextFunction(AREF Addr, PDWORD pData);
		void	FillWorkString(BOOL fIsRead);
		void	FillRcvCache(void);

		// Transport
		BOOL	Transact(BOOL fIsText);
		void	Send( void );
		BOOL	GetFrame(BOOL fIsText);

		// Response Processing
		BOOL	CheckReply(void);
		BOOL	CheckReplyDrop(void);
		BOOL	CheckReplyClass(UINT * pPos);
		BOOL	CheckReplySubClass(UINT * pPos);
		BOOL	CheckReplySet(UINT * pPos);
		BOOL	CheckReplyAddr(UINT * pPos);
		BOOL	CheckReplyNoError(void);

		// PortAccess
		UINT	Get(UINT uTime);

		// Helpers
		void	PutGeneric(DWORD dData, UINT uBase, UINT uFactor);
		DWORD	GetGeneric(UINT uBase, UINT uLength, UINT uOffset);

		void	SetParamDef(AREF Addr);
		void	GetDataDef(UINT uPos);
		void	InitDataDef(void);
		DWORD	GetData(BOOL fWantBit, UINT uType);
		DWORD	GetBitData(DWORD dData);
		DWORD	GetRealData(void);
		DWORD	HexToReal(DWORD dHex);
		DWORD	RealToInt(DWORD dReal);
		BOOL	IsIO(UINT uTable);
		BOOL	IsHexDigit(char cData);
		BOOL	IsDigit(char cData);
		BOOL	IsBinary(char cData);
		UINT	GetAnInteger(UINT * pRxPos);
		BYTE	GetACharacter(UINT * pRxPos);
		UINT	xtoin(char cData);
		BOOL	FindChar(UINT uStart, UINT * pFound, BYTE bData);
		BYTE	MakeTxChecksum(void);
		BYTE	MakeRxChecksum(void);

		// Internal Storage
		BOOL	AccessCache(AREF Addr, PDWORD pData, UINT * pCount, BOOL fIsRead);
	};

// End of File
