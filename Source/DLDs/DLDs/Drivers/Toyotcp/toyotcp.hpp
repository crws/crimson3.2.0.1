
//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	TIMEOUT 500

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC TCP Driver
//

class CToyodatcpDriver : public CMasterDriver
{
	public:
		// Constructor
		CToyodatcpDriver(void);

		// Destructor
		~CToyodatcpDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BYTE	 m_bUnit;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			};
		CContext *	m_pCtx;

		// Hex Lookup
		LPCTXT m_pHex;
		
		// Comms Data
		BYTE	m_bTx[300];
		BYTE	m_bRx[300];
		BYTE	m_bCheck;
		UINT	m_uPtr;
		UINT	m_uEndOfCommand;
		UINT	m_uKeep;

		// Socket Management
		BOOL	CheckSocket(void);
		BOOL	OpenSocket(void);
		void	CloseSocket(BOOL fAbort);

		// Frame Building
		UINT	SetCommand(AREF Addr, BOOL fIsRead, UINT uCount);
		void	StartFrame(void);
		void	AddCommand(AREF Addr, BOOL fIsRead);
		void	AddIdentifier(UINT uTable);
		void	AddAddress(UINT uOffset);
		UINT	SpecifyCount(AREF Addr, UINT uCount);
		void	AddWriteData(UINT uTable, UINT uType, PDWORD pData, UINT uCount);
		void	AddChecksum(void);
		void	EndFrame(void);
		void	AddByte(BYTE b);
		void	AddData(DWORD u, UINT uCount);
		void	AddBitCommand( AREF Addr, BOOL fIsRead);
		void	AddByteCommand(AREF Addr, BOOL fIsRead);
		void	AddWordCommand(AREF Addr, BOOL fIsRead);

		// Transport Layer
		BOOL	Transact(UINT uTable, UINT uType, PDWORD pData, UINT uCount, BOOL fIsRead);
		BOOL	Send(void);

		// Receive Processing
		BOOL	GetReply(void);
		BOOL	CheckResponse(UINT uEnd);
		void	GetResponse(UINT uTable, UINT uType, PDWORD pData, UINT uCount);
		DWORD	GetData(UINT uPos, UINT uSize);

		// Helpers
		void	AddProgramNumber(UINT uTable, UINT uExtra);
		BOOL	IsTimerCounter(UINT uTable);

	};

#define	CMX	1
#define	CMY	2
#define	CMM	3
#define	CMK	4
#define	CMV	5
#define	CMT	6
#define	CMC	7
#define	CML	8
#define	CMP	9
#define	CMD	10
#define	CMR	11
#define	CMN	12
#define	CMZ	32
#define	CMS	13
#define	CMEX	14
#define	CMEY	15
#define	CMEM	16
#define	CMEK	17
#define	CMEV	18
#define	CMET	19
#define	CMEC	20
#define	CMEL	21
#define	CMEP	22
#define	CMEN	23 // not supported in protocol
#define	CMH	24 // not supported in protocol
#define	CMES	25
#define	CMGX	26
#define	CMGY	27
#define	CMGM	28
#define	CMU	29
#define	CMEB	30
#define	CMB	31

// End of File
