
#include "intern.hpp"

#include "bbssers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Slave Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

static BYTE crctbl[] = {
	0x087,0x00F,0x00E,0x01E,0x095,0x02C,0x01C,0x03D,
	0x0A3,0x049,0x02A,0x058,0x0B1,0x06A,0x038,0x07B,
	0x0CF,0x083,0x046,0x092,0x0DD,0x0A0,0x054,0x0B1,
	0x0EB,0x0C5,0x062,0x0D4,0x0F9,0x0E6,0x070,0x0F7,
	0x006,0x01F,0x08F,0x00E,0x014,0x03C,0x09D,0x02D,
	0x022,0x059,0x0AB,0x048,0x030,0x07A,0x0B9,0x06B,
	0x04E,0x093,0x0C7,0x082,0x05C,0x0B0,0x0D5,0x0A1,
	0x06A,0x0D5,0x0E3,0x0C4,0x078,0x0F6,0x0F1,0x0E7,
	0x085,0x02E,0x00C,0x03F,0x097,0x00D,0x01E,0x01C,
	0x0A1,0x068,0x028,0x079,0x0B3,0x04B,0x03A,0x05A,
	0x0CD,0x0A2,0x044,0x0B3,0x0DF,0x081,0x056,0x090,
	0x0E9,0x0E4,0x060,0x0F5,0x0FB,0x0C7,0x072,0x0D6,
	0x004,0x03E,0x08D,0x02F,0x016,0x01D,0x09F,0x00C,
	0x020,0x078,0x0A9,0x069,0x032,0x05B,0x0BB,0x04A,
	0x04C,0x0B2,0x0C5,0x0A3,0x05E,0x091,0x0D7,0x080,
	0x068,0x0F4,0x0E1,0x0E5,0x07A,0x0D7,0x0F3,0x0C6,
	0x083,0x04D,0x00A,0x05C,0x091,0x06E,0x018,0x07F,
	0x0A7,0x00B,0x02E,0x01A,0x0B5,0x028,0x03C,0x039,
	0x0CB,0x0C1,0x042,0x0D0,0x0D9,0x0E2,0x050,0x0F3,
	0x0EF,0x087,0x066,0x096,0x0FD,0x0A4,0x074,0x0B5,
	0x002,0x05D,0x08B,0x04C,0x010,0x07E,0x099,0x06F,
	0x026,0x01B,0x0AF,0x00A,0x034,0x038,0x0BD,0x029,
	0x04A,0x0D1,0x0C3,0x0C0,0x058,0x0F2,0x0D1,0x0E3,
	0x06E,0x097,0x0E7,0x086,0x07C,0x0B4,0x0F5,0x0A5,
	0x081,0x06C,0x008,0x07D,0x093,0x04F,0x01A,0x05E,
	0x0A5,0x02A,0x02C,0x03B,0x0B7,0x009,0x03E,0x018,
	0x0C9,0x0E0,0x040,0x0F1,0x0DB,0x0C3,0x052,0x0D2,
	0x0ED,0x0A6,0x064,0x0B7,0x0FF,0x085,0x076,0x094,
	0x000,0x07C,0x089,0x06D,0x012,0x05F,0x09B,0x04E,
	0x024,0x03A,0x0AD,0x02B,0x036,0x019,0x0BF,0x008,
	0x048,0x0F0,0x0C1,0x0E1,0x05A,0x0D3,0x0D3,0x0C2,
	0x06C,0x0B6,0x0E5,0x0A7,0x07E,0x095,0x0F7,0x084,
	0x08F,0x08B,0x006,0x09A,0x09D,0x0A8,0x014,0x0B9,
	0x0AB,0x0CD,0x022,0x0DC,0x0B9,0x0EE,0x030,0x0FF,
	0x0C7,0x007,0x04E,0x016,0x0D5,0x024,0x05C,0x035,
	0x0E3,0x041,0x06A,0x050,0x0F1,0x062,0x078,0x073,
	0x00E,0x09B,0x087,0x08A,0x01C,0x0B8,0x095,0x0A9,
	0x02A,0x0DD,0x0A3,0x0CC,0x038,0x0FE,0x0B1,0x0EF,
	0x046,0x017,0x0CF,0x006,0x054,0x034,0x0DD,0x025,
	0x062,0x051,0x0EB,0x040,0x070,0x072,0x0F9,0x063,
	0x08D,0x0AA,0x004,0x0BB,0x09F,0x089,0x016,0x098,
	0x0A9,0x0EC,0x020,0x0FD,0x0BB,0x0CF,0x032,0x0DE,
	0x0C5,0x026,0x04C,0x037,0x0D7,0x005,0x05E,0x014,
	0x0E1,0x060,0x068,0x071,0x0F3,0x043,0x07A,0x052,
	0x00C,0x0BA,0x085,0x0AB,0x01E,0x099,0x097,0x088,
	0x028,0x0FC,0x0A1,0x0ED,0x03A,0x0DF,0x0B3,0x0CE,
	0x044,0x036,0x0CD,0x027,0x056,0x015,0x0DF,0x004,
	0x060,0x070,0x0E9,0x061,0x072,0x053,0x0FB,0x042,
	0x08B,0x0C9,0x002,0x0D8,0x099,0x0EA,0x010,0x0FB,
	0x0AF,0x08F,0x026,0x09E,0x0BD,0x0AC,0x034,0x0BD,
	0x0C3,0x045,0x04A,0x054,0x0D1,0x066,0x058,0x077,
	0x0E7,0x003,0x06E,0x012,0x0F5,0x020,0x07C,0x031,
	0x00A,0x0D9,0x083,0x0C8,0x018,0x0FA,0x091,0x0EB,
	0x02E,0x09F,0x0A7,0x08E,0x03C,0x0BC,0x0B5,0x0AD,
	0x042,0x055,0x0CB,0x044,0x050,0x076,0x0D9,0x067,
	0x066,0x013,0x0EF,0x002,0x074,0x030,0x0FD,0x021,
	0x089,0x0E8,0x000,0x0F9,0x09B,0x0CB,0x012,0x0DA,
	0x0AD,0x0AE,0x024,0x0BF,0x0BF,0x08D,0x036,0x09C,
	0x0C1,0x064,0x048,0x075,0x0D3,0x047,0x05A,0x056,
	0x0E5,0x022,0x06C,0x033,0x0F7,0x001,0x07E,0x010,
	0x008,0x0F8,0x081,0x0E9,0x01A,0x0DB,0x093,0x0CA,
	0x02C,0x0BE,0x0A5,0x0AF,0x03E,0x09D,0x0B7,0x08C,
	0x040,0x074,0x0C9,0x065,0x052,0x057,0x0DB,0x046,
	0x064,0x032,0x0ED,0x023,0x076,0x011,0x0FF,0x000
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Slave Driver
//

// Instantiator

INSTANTIATE(CBBBSAPSerialSDriver);

// Constructor

CBBBSAPSerialSDriver::CBBBSAPSerialSDriver(void)
{
	m_Ident	= DRIVER_ID;

	InitGlobal();
	}

// Destructor

CBBBSAPSerialSDriver::~CBBBSAPSerialSDriver(void)
{
	}

// Configuration

void MCALL CBBBSAPSerialSDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bDrop = LOBYTE(GetWord(pData));
		}
	}

void MCALL CBBBSAPSerialSDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {

		Config.m_uFlags |= flagFastRx;
		}

	Make485(Config, FALSE);
	}
	
// Management

void MCALL CBBBSAPSerialSDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CBBBSAPSerialSDriver::Detach(void)
{
	CloseDown();

	CSlaveDriver::Detach();
	}

// Device

CCODE MCALL CBBBSAPSerialSDriver::DeviceOpen(IDevice *pDevice)
{
	CSlaveDriver::DeviceOpen(pDevice);

	PCBYTE pData = pDevice->GetConfig();

	if( GetWord(pData) == 0x1234 ) {

		WORD wKey = GetWord(pData);

		switch( wKey ) {

			case 0xABCD:	m_bIsC3 = 2; break;
			case 0xBCDE:	m_bIsC3 = 3; break;

			default:	return CCODE_ERROR | CCODE_HARD;
			}

		LoadTags(pData);

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CBBBSAPSerialSDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		CloseDown();
		}

	// don't delete names, but delete all other buffers

	CloseListBuffers();

	ListDeAlloc();

	return CSlaveDriver::DeviceClose(fPersist);
	}

// Entry Point

void MCALL CBBBSAPSerialSDriver::Service(void)
{
	ReadData();

	Sleep(20);
	}

// Receive Processing

void CBBBSAPSerialSDriver::ReadData(void)
{
	UINT uTime = 100;

	m_uRPtr    = 0;

	while( (BOOL)uTime ) {

		UINT uData = m_pData->Read(uTime);

		if( uData < NOTHING ) {

			uTime      = 50;

			BYTE bData = LOBYTE(uData);

			switch( m_uState ) {

				case 0:
					if( bData == DLE ) {

						m_uState = 1;

//**/						AfxTrace0("\r\n<10>");
						}

					break;

				case 1:
//**/					AfxTrace1("<%2.2x>\r\n", bData);

					if( bData == STX ) {

						m_uState = 2;

						m_uRPtr  = 0;
						}

					else {
						if( bData == ETX ) {

							return;
							}

						m_uState = 0;
						}

					break;

				case 2:
					if( bData == DLE ) {

//**/						AfxTrace0(" ");

						m_uState = 3;
						}

					else {
//**/						AfxTrace1("<%2.2x>", bData);

						m_bRx[m_uRPtr++] = bData;
						}

					break;

				case 3:
					if( bData == ETX ) {

//**/						AfxTrace1("\r\nETX--%d ", m_uRPtr);

						m_bRx[m_uRPtr]	= ETX;

						m_uETXPos	= m_uRPtr;

						m_uState	= 4;
						}

					else {
//**/						AfxTrace1("<%2.2x>", bData);

						m_bRx[m_uRPtr++] = bData;

						m_uState         = 2;
						}

					break;

				case 4: // CRC does *not* get DLE'd
//**/					AfxTrace1("_%2.2x_", bData);

					m_uState = 5; // CRC byte 1
					break;

				case 5:
//**/					AfxTrace1("_%2.2x", bData);

					m_uState = 0;

					ProcReceive();

					return;
				}

			if( m_uRPtr >= sizeof(m_bRx) ) {

				m_uState = 0;

				m_pData->ClearRx();

				return;
				}
			}
		}
	}

void CBBBSAPSerialSDriver::ProcReceive(void)
{
	if( !CheckDrop(m_bRx[RXDADD]) ) return; // not this drop

	m_uTPtr = 0;

	UINT uFCPosition = RXDFC;

	if( m_fGlobal ) {

		uFCPosition = RXGDFC;
		}

	BYTE bFuncType   = m_bRx[uFCPosition];

	ListDeAlloc();

	if( IsPollOrAck(bFuncType) || bFuncType != MFCRDBACC ) {

		if( bFuncType == POLLMSG ) {

			SendPollAck();
			}

		return;
		}

	UINT uSizeR	= 0;

	UINT uRAdj	= 0;

	BYTE bOC	= m_bRx[uFCPosition + 5];	// operation code

	if( !(bOC & 0x80) ) {				// operation is a read, has field selects

		switch( m_bRx[uFCPosition + 6] ) {

			case 1: uRAdj = 3; break;
			case 3: uRAdj = 2; break;
			case 7: uRAdj = 1; break;
			default: return;
			}
		}

	switch( bOC ) {

		case OFCRDBYA:	uSizeR = sizeof(RBYADDRRX) - uRAdj;	break;
		case OFCRDBYN:	uSizeR = sizeof(RBYNAMERX) - uRAdj;	break;
		case OFCRDBYL:	uSizeR = sizeof(RBYLISTRX) - uRAdj - 2;	break;
		case OFCRDBYLC:	uSizeR = sizeof(RBYLISTRX) - uRAdj;	break;

		case OFCWRBYA:	uSizeR = sizeof(WBYADDRRX);	break;
		case OFCWRBYN:	uSizeR = sizeof(WBYNAMERX);	break;

		default:
			SendAckNak(NAKDATA);
			return;
		}


	UINT uSizeH = sizeof(LOCMSGHEAD);

	if( m_fGlobal ) {

		uSizeH = sizeof(GLBMSGHEAD);
		}

	UINT uSizeE = sizeof(ENDHEADLIST);

	UINT uSizeD = m_uETXPos - uSizeH - uSizeE - uSizeR + 1;

	if( uSizeD ) {

		UINT uPos   = 0;

		m_pHeadList = MakeReqBuffer(uSizeH,     &uPos);
		m_pFuncList = MakeReqBuffer(uSizeE - 1, &uPos);
		m_pReqList  = MakeReqBuffer(uSizeR,     &uPos);
		m_pDataList = MakeReqBuffer(uSizeD + 1, &uPos);
		m_pDataList[uSizeD] = 0;

		m_pFuncList[uSizeE] = uSizeD;

		DoReceive();
		}
	}

void CBBBSAPSerialSDriver::DoReceive(void)
{
	BYTE bOpcode = m_pReqList[0];

	m_bRER = 0;

	BYTE bFunc = m_pFuncList[3];

	if( bFunc == MFCRDBACC || bFunc == MFCRDDB || bFunc == MFCWRDB ) {

		if( bOpcode & 0x80 ) {

			switch( bOpcode ) {

				case OFCWRBYA:

					WriteByAddress();
					return;

				case OFCWRBYN:

					WriteByName();
					return;
				}
			}

		else {
			SetFieldSelects();

			switch( bOpcode ) {

				case OFCRDBYA:
				case OFCRDBYN:

					ReadSignal(bOpcode);
					return;

				case OFCRDBYL:
				case OFCRDBYLC:

					ReadByList(bOpcode == OFCRDBYL);
					return;
				}
			}
		}
	}

PBYTE CBBBSAPSerialSDriver::MakeReqBuffer(UINT uSize, UINT *pPos)
{
	UINT uPos = *pPos;

	*pPos     = uPos + uSize;

	m_dRCMagic = 0x12345678;

	PBYTE p   = new BYTE [uSize];

	memcpy(p, &m_bRx[uPos], uSize);

	return p;
	}

// Read Routines
void CBBBSAPSerialSDriver::ReadByList(BOOL fFirst)
{
	if( fFirst ) {

		m_uNameListItem = 0;
		}

	InitRBL();

	if( !m_uListCount || !(m_bFSS[1] & 0x8) ) { // check if names are requested

		CloseListBuffers();

		m_uNameListItem = 0;

		SendNak();

		return;
		}

	BOOL fHasRoom = TRUE;

	VARINFO **pVI = new VARINFO *[m_uListCount];

	UINT i;

	for( i = 0; fHasRoom && (i < m_uListCount); i++ ) {	// find errors

		pVI[i] = new VARINFO;

		m_pVarInfo = pVI[i];

		if( m_pListAddr[i] ) {	// valid read

			GetVarInfo(m_pListPosn[i]);

			m_pVarInfo->Data = m_pListData[i];

			fHasRoom = AddFieldSels();
			}

		else {
			m_bRER = RERMATCH;

			m_pVarInfo->EER = RERMATCH;

			m_uTPtr++;
			}
		}

	m_uTPtr = 0;	// reset Tx pointer to start

	if( i ) {	// fill buffer

		m_uNameListItem = fHasRoom ? m_uNameListItem + i : 0;

		AddListStart( i, m_uNameListItem );

		for( UINT k = 0; k < i; k++ ) {

			m_pVarInfo = pVI[k];

			if( m_bRER ) {

				AddByte(m_pVarInfo->EER);
				}

			if( !m_pVarInfo->EER ) {

				AddFieldSels();
				}

			else {
				AddByte(RERMATCH);
				}
			}

		Send();

		if( i < m_uListCount ) {

			m_uListCount -= i;

			return;	// don't clear RBL lists
			}
		}

	else {
		SendAckNak(NAKDATA);
		}

	for( i = 0; i < m_uListCount; i++ ) {

		VARINFO *p = pVI[i];

		if( p ) delete pVI[i];
		}

	delete pVI;

	CloseListBuffers();
	}

void CBBBSAPSerialSDriver::ReadSignal(BYTE bType)
{
	BOOL fName = (BOOL)bType;			// is read by name

	UINT uRPos = m_bFSS[0] + (fName ? 3 : 5);	// position of element count

	UINT uQty  = m_pReqList[uRPos];

	uRPos      = 0;					// first data byte

	PVARINFO *pVI = new PVARINFO [uQty];

	UINT uSel;

	for( uSel = 0; uSel < uQty; uSel++ ) {

		UINT uLen = 2;		// assume read by address

		pVI[uSel] = new VARINFO;

		m_pVarInfo  = pVI[uSel];

		BYTE bBad = 0;

		if( fName ) {
		
			const char *pName = (const char *)&m_pDataList[uRPos];

			uLen = (strlen(pName) + 1);	// Name + Null

			if( FindName(uRPos) ) {

				bBad = LoadInfoAboutItem();

				if( bBad ) {

					m_bRER = RERMATCH;
					}
				}

			else {
				m_bRER = RERMATCH;	// name not found

				bBad   = EERIDENT;	// EER for this item
				}
			}

		else {
			if( FindAddr(uRPos) ) {

				bBad = LoadInfoAboutItem();

				if( bBad ) {

					m_bRER = RERNOMAT;
					}
				}

			else {
				m_bRER = RERNOMAT;

				bBad   = EERIDENT;
				}
			}

		m_pVarInfo->EER = bBad;

		uRPos += uLen;	// next item position
		}

	ConstructSignalSend(pVI, uQty);

	for( uSel = 0; uSel < uQty; uSel++ ) {

		VARINFO *p = pVI[uSel];

		if( p ) delete pVI[uSel];
		}

	delete pVI;

	if( m_uTPtr > TXRERPOS + 1 ) Send(); // There is data to send
	}

// Write routines
void CBBBSAPSerialSDriver::WriteByName(void)
{
	UINT uQty = m_pReqList[2];

	UINT uRPos = 0;

	PVARINFO *pVI = new PVARINFO [uQty];

	UINT uECt = 0;

	UINT uSel;

	for( uSel = 0; uSel < uQty; uSel++ ) {

		pVI[uSel] = new VARINFO;

		m_pVarInfo  = pVI[uSel];

		if( !FindName(uRPos) ) {

			m_bRER = RERMATCH;

			m_pVarInfo->EER = RERMATCH;

			uECt++;
			}

		else {
			uRPos += m_pVarInfo->Length + 1;	// Allow for null after name

			m_pVarInfo->EER = 0;

			if( !SetWriteData(&uRPos) ) {

				uQty   = 0;
				}
			}
		}

	ExecuteWrite(pVI, uQty);

	ConstructWriteReply(pVI, uQty);

	for( uSel = 0; uSel < uQty; uSel++ ) {

		VARINFO *p = pVI[uSel];

		if( p ) delete pVI[uSel];
		}

	delete pVI;

	Send();
	}

// Write Routines

void CBBBSAPSerialSDriver::WriteByAddress(void)
{
	UINT uQty = m_pDataList[4];

	UINT uRPos = 0;

	PVARINFO *pVI = new PVARINFO [uQty];

	UINT uECt = 0;

	UINT uSel;

	for( uSel = 0; uSel < uQty; uSel++ ) {

		pVI[uSel] = new VARINFO;

		m_pVarInfo  = pVI[uSel];

		if( !FindAddr(uRPos) ) {

			m_bRER = RERMATCH;

			m_pVarInfo->EER = RERMATCH;

			uECt++;
			}

		else {
			uRPos += 2;			// point to Descriptor

			if( !SetWriteData(&uRPos) ) {

				uQty   = 0;
				}
			}
		}

	ExecuteWrite(pVI, uQty);

	ConstructWriteReply(pVI, uQty);

	for( uSel = 0; uSel < uQty; uSel++ ) {

		VARINFO *p = pVI[uSel];

		if( p ) delete pVI[uSel];
		}

	delete pVI;

	Send();
	}

BOOL CBBBSAPSerialSDriver::SetWriteData(UINT * pPos)
{
	UINT uPos = *pPos;

	BYTE b    = m_pDataList[uPos++];		// Write Field Descriptor

	m_pVarInfo->FSS[0] = b;

	DWORD dData = 0;

	if( CheckWriteDesc(b, WFDANALG) ) {

		dData  = (DWORD)IntelToHost(*(PU4)&m_pDataList[uPos]);

		uPos += 4;
		}

	else if( CheckWriteDesc(b, WFDLVON) ) {

		dData = m_pVarInfo->Logical ? 0xFFFFFFFF : 0x3F800000;	// value of 1
		}

	else if( CheckWriteDesc(b, WFDLVOFF) ) {

		dData = 0;
		}

	else {
		m_pVarInfo->EER = RERMATCH;

		m_bRER		= RERINVDB;

		dData = 0;

		return FALSE;
		}

	*pPos = uPos;

	m_pVarInfo->Data = dData;

	return TRUE;
	}

BOOL CBBBSAPSerialSDriver::CheckWriteDesc(BYTE bDesc, UINT uItem)
{
	BYTE bItem = LOBYTE(uItem);

       	return (bDesc & bItem) == bItem;
	}

void CBBBSAPSerialSDriver::ExecuteWrite(PVARINFO *pVI, UINT uQty)
{
	for( UINT uSel = 0; uSel < uQty; uSel++ ) {

		m_pVarInfo = pVI[uSel];

		if( !m_pVarInfo->EER ) {

			CAddress Addr;

			Addr.m_Ref = m_pVarInfo->Addr;

			DWORD Data[1];

			*Data = RealToLong(Addr.a.m_Type, m_pVarInfo->Data);

			if( !COMMS_SUCCESS(Write(Addr, Data, 1)) ){

				m_pVarInfo->EER = EEREND_D;
				}
			}
		}
	}

// Item info
BOOL CBBBSAPSerialSDriver::FindName(UINT uPos)
{
	const char * p	= (const char *)&m_pDataList[uPos];

	UINT uLen	= strlen(p);

	UINT uIndex	= 0;

	while( uIndex < m_uTagCount ) {

		WORD wPos1 = m_pwNameIndex[uIndex];

		const char * pPos = (const char *)&m_pbNameList[wPos1];

		if( strlen(pPos) == uLen ) {

			if( !strnicmp(p, pPos, uLen) ) {

				GetVarInfo(uIndex);

				m_pVarInfo->Length = uLen;

				return TRUE;
				}
			}

		uIndex++;
		}

	return FALSE;
	}

BOOL CBBBSAPSerialSDriver::FindAddr(UINT uPos)
{
	WORD wAdd   = (WORD)IntelToHost(*(PU2)&m_pDataList[uPos]);

	UINT uIndex = 0;

	while( uIndex < m_uTagCount ) {

		if( m_pwMSDList[uIndex] == wAdd ) {

			GetVarInfo(uIndex);

			return TRUE;
			}

		uIndex++;
		}

	return FALSE;
	}

BYTE CBBBSAPSerialSDriver::LoadInfoAboutItem(void)
{
	if( GetReadInfo(m_pVarInfo->Index) ) {

		return 0;
		}

	return EEREND_D;
	}

BOOL CBBBSAPSerialSDriver::GetReadInfo(UINT uIndex)
{
	DWORD d = m_pdAddrList[uIndex];

	CAddress A;

	A.m_Ref = d;

	DWORD Data[1];

	if( COMMS_SUCCESS(Read(A, Data, 1)) ) {

		FillInfo(uIndex, LongToReal(A.a.m_Type, Data[0]));

		return TRUE;
		}

	return FALSE;
	}

void CBBBSAPSerialSDriver::FillInfo(UINT uIndex, DWORD dData)
{
	CAddress Addr;

	Addr.m_Ref		= m_pdAddrList[uIndex];

	m_pVarInfo->Addr	= Addr.m_Ref;
	m_pVarInfo->Data	= dData;
	m_pVarInfo->Logical	= !(BOOL)Addr.a.m_Type;
	m_pVarInfo->Offset	= Addr.a.m_Offset & AMASK;
	m_pVarInfo->NameInx	= m_pwNameIndex[uIndex];
	}

// Read Helpers
void CBBBSAPSerialSDriver::ConstructSignalSend(PVARINFO *pVI, UINT uQty)
{
	BOOL fAddEER = (BOOL)m_bRER;

	UINT uRERPos = AddSerialHeader();

	AddByte(m_bRER);

	AddByte(LOBYTE(uQty));

	BOOL fBuffFull = FALSE;

	for( UINT uSel = 0; uSel < uQty && !fBuffFull; uSel++ ) {

		m_pVarInfo = pVI[uSel];

		if( fAddEER ) AddByte(m_pVarInfo->EER);

		AddFieldSels();

		if( m_uTPtr > MAXSEND ) {		// running out of buffer space

			uQty = uSel + 1;		// this will be the last one

			fBuffFull = TRUE;

			m_bTx[uRERPos]     |= 1;	// not all data fits in this response
			}
		}

	m_bTx[uRERPos + 1]  = uQty;			// final item quantity for packet
	}

void CBBBSAPSerialSDriver::InitRBL(void)
{
	UINT uMaxSize = m_uTagCount - m_uNameListItem;

	PDWORD pAddrTemp = new DWORD [uMaxSize];
	PDWORD pDataTemp = new DWORD [uMaxSize];
	PWORD  pPosnTemp = new WORD  [uMaxSize];

	if( !pAddrTemp || !pDataTemp || !pPosnTemp ) {

		m_uListCount = 0;

		if( pAddrTemp ) delete pAddrTemp;
		if( pDataTemp ) delete pDataTemp;
		if( pPosnTemp ) delete pPosnTemp;

		return;
		}

	CountListElements(pAddrTemp, pDataTemp, pPosnTemp);

	CloseListBuffers();

	if( m_uListCount ) {

		m_pListAddr = new DWORD [m_uListCount];
		m_pListData = new DWORD [m_uListCount];
		m_pListPosn = new WORD  [m_uListCount];

		if( m_pListAddr && m_pListData && m_pListPosn ) {

			m_dLBMagic	 = 0x12345678;

			memcpy(m_pListAddr, pAddrTemp, m_uListCount * 4);
			memcpy(m_pListData, pDataTemp, m_uListCount * 4);
			memcpy(m_pListPosn, pPosnTemp, m_uListCount * 2);
			}

		else {
			if( m_pListAddr ) { delete m_pListAddr; m_pListAddr = NULL; }
			if( m_pListData ) { delete m_pListData; m_pListData = NULL; }
			if( m_pListPosn ) { delete m_pListPosn; m_pListPosn = NULL; }
			}

		delete pAddrTemp;
		delete pDataTemp;
		delete pPosnTemp;

		pAddrTemp = NULL;
		pDataTemp = NULL;
		pPosnTemp = NULL;
		}
	}

void CBBBSAPSerialSDriver::CountListElements(PDWORD pA, PDWORD pD, PWORD pP)
{
	UINT u  = 0;

	m_bList = m_bRx[m_uETXPos - 1];

	for( UINT i = m_uNameListItem; i < m_uTagCount; i++ ){

		DWORD d = m_pdAddrList[i];

		if( (LOWORD(d) >> LSHIFT) == m_bList ) {

			CAddress Addr;

			DWORD Data[1];

			Addr.m_Ref = d;

			if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) { // add address if data is accessible

				pP[u]	= i; // position in full list

				pD[u]	= LongToReal(Addr.a.m_Type, *Data);

				pA[u++]	= d; // Addr.m_Ref
				}

			else {
				pA[u++] = 0;
				}
			}
		}

	m_uListCount = u;
	}

void CBBBSAPSerialSDriver::AddListStart(UINT uCount, UINT uNext)
{
	AddByte(0);		// Node Status
	AddByte(m_bRER);	// RER
	AddByte(uCount);	// Count;
	AddByte(m_bList);	// ListNumber;

	if( uNext ) {

		AddWord(uNext);	// Next entry - will be deleted if no overrun
		}
	}

void CBBBSAPSerialSDriver::GetVarInfo(UINT uIndex)
{
	DWORD d = m_pdAddrList[uIndex];

	m_pVarInfo->Addr	= d;

	m_pVarInfo->Logical	= !(d >> 24);

	m_pVarInfo->NameInx	= m_pwNameIndex[uIndex];

	m_pVarInfo->Offset	= d & AMASK;

	m_pVarInfo->MSDAddr	= m_pwMSDList[uIndex];

	m_pVarInfo->Index	= uIndex;

	m_pVarInfo->EER		= 0;
	}

// Write Helpers
void CBBBSAPSerialSDriver::ConstructWriteReply(PVARINFO *pVI, UINT uQty)
{
	AddSerialHeader();

	AddByte(m_bRER);

	AddByte(uQty);

	PVARINFO p;

	for( UINT uSel = 0; uSel < uQty; uSel++ ) {

		p = pVI[uSel];

		AddByte(p->EER);
		}
	}

// Construct Frame
UINT CBBBSAPSerialSDriver::AddSerialHeader(void)
{
	m_uTPtr = 0;

	AddByte(0);			// Master Device Number
	AddByte(m_pHeadList[1]);	// Master's message serial number

	UINT n = 2;

	if( m_fGlobal ) {		// Global (12 byte) Header

		AddByte(m_pHeadList[2]);	// SAddrL);
		AddByte(m_pHeadList[3]);	// SAddrH);
		AddByte(m_pHeadList[4]);	// DAddrL);
		AddByte(m_pHeadList[5]);	// DAddrH);
		AddByte(m_pHeadList[6]);	// Control);
		}

	AddSerialFunc();

	return m_fGlobal ? 12 : 7;	// position of RER
	}

void CBBBSAPSerialSDriver::AddSerialFunc(void)
{
	AddByte(m_pFuncList[3]);	// SrceFC->DestFC);	// exchange SrceFC & DestFC for response
	AddByte(m_pFuncList[1]);	// AppSeqL);
	AddByte(m_pFuncList[2]);	// AppSeqH);
	AddByte(m_pFuncList[0]);	// DestFC->SrceFC);
	AddByte(m_pFuncList[4]);	// NodeST);
	}

void CBBBSAPSerialSDriver::FinishTxBuffer(UINT uCount)
{
	UINT uPos       = m_fGlobal ? 14 : 9;

	m_bTx[uPos]     = m_uNameListItem ? 1 : 0;

	m_bTx[uPos + 1] = uCount;

	m_bTx[uPos + 3] = LOBYTE(LOWORD(m_uNameListItem));

	m_bTx[uPos + 4] = HIBYTE(LOWORD(m_uNameListItem));
	}

BOOL CBBBSAPSerialSDriver::PutItemEnd(BYTE bBad)
{
	if( bBad ) AddByte(EERMATCH);

	else AddFieldSels();

	return m_uTPtr >= MAXSEND;
	}

// Field Select Helpers
BOOL CBBBSAPSerialSDriver::ParseVarName(PCTXT Name, UINT uSelect, PBYTE pItem)
{
	UINT uNameLen = strlen((const char *)Name);

	memset(pItem, 0, uNameLen);

	for( UINT i = 0, j = 0; i < uNameLen; i++ ) {

		BYTE b = Name[i];

		if( !uSelect ) {

			if( b == '.' ) return (BOOL)pItem[0];

			pItem[j++] = b;
			}

		else {
			if( b == '.' ) uSelect--;
			}

		return (BOOL)pItem[0];
		}

	return FALSE;
	}

void CBBBSAPSerialSDriver::SetFieldSelects()
{
	BYTE b = m_pReqList[1] & 0x7; // bit 4 is not defined in spec

	if( !b ) return;

	memset(m_bFSS, 0, 5);

	PBYTE p = &m_pReqList[2];

	BYTE bCount = 1;

	while( b & 1 ) {

		m_bFSS[bCount] = *p++;

		bCount++;

		b >>= 1;
		}

	m_bFSS[0] = bCount - 1;
	}

BOOL CBBBSAPSerialSDriver::AddFieldSels(void)
{
	WORD wPkt0 = m_uTPtr;

	AddField1();
	AddField2();
	AddField3();

	return TRUE;
	}

void CBBBSAPSerialSDriver::AddField1(void)
{
	VARINFO *p = m_pVarInfo;

	BYTE b     = m_bFSS[1];

	if( b ) {

		DWORD d    = p->Data;

		BOOL fLog  = p->Logical;
		BOOL fTrue = (BOOL)(d);

		if( b & 0x80 ) AddByte(fLog ? 0 : 2);		// Type = logical or analog

		if( b & 0x40 ) {

			if( fLog ) AddByte(fTrue ? 1 : 0);	// digital value

			else AddLong(d);			// analog value
			}

		if( b & 0x20 ) {				// Units Text = 6 spaces
			AddLong(0x20202020);
			AddWord(0x2020);
			}

		if( b & 0x10 ) AddWord(p->MSDAddr);		// Msd Address

		if( b & 0x08 ) {

			AddTextPlusNull(PCTXT(&m_pbNameList[p->NameInx])); /*!!!CHECK!!!*/
			}

		if( b & 0x04 ) AddByte(0);			// Alarm Status

		if( b & 0x02 ) {

			AddByte('?');			// Descriptor = 1 space
			AddByte(0);
			}

		if( b & 0x01 ) {

			if( fLog ) {

				AddByte('O');
				AddByte('N');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				AddByte('O');
				AddByte('F');
				AddByte('F');
				AddByte(' ');
				AddByte(' ');
				AddByte(' ');
				}
			}
		}
	}

void CBBBSAPSerialSDriver::AddField2(void)
{
	VARINFO *p = m_pVarInfo;

	BYTE b = m_bFSS[2];

	if( b ) {

		if( b & 0x80 ) AddByte(0);
		if( b & 0x40 ) AddByte(0);

		if( b & 0x38 ) {

			PBYTE pName = &m_pbNameList[p->NameInx];

			PBYTE pItem = new BYTE [strlen((const char *)pName)];

			UINT uSkip  = pName[0] == '@' ? 1 : 0;

			if( b & 0x20 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			uSkip++;

			if( b & 0x10 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			uSkip++;

			if( b & 0x08 ) {

				if( ParseVarName((PCTXT)pName, uSkip, pItem) ) {

					AddTextPlusNull((PCTXT)pItem);
					}

				else AddTextPlusNull((PCTXT)" ");
				}

			delete pItem;

			pItem = NULL;
			}

		if( b & 0x04) AddWord(0);
		if( b & 0x02) AddWord(0);
		if( b & 0x01) AddWord(m_wVersion);
		}
	}

void CBBBSAPSerialSDriver::AddField3(void)
{
	VARINFO *p = m_pVarInfo;

	BYTE b = m_bFSS[3];

	if( b ) {

		if( b & 0x80 ) AddWord(0);
		if( b & 0x40 ) AddWord(0);
		if( b & 0x20 ) AddWord(0);
		if( b & 0x10 ) AddWord(0);
		if( b & 0x08 ) AddByte(0);
		if( b & 0x04 ) AddByte(0);

		if( b & 0x02 ) {

			if( p->Logical ) AddByte(p->Data ? 1 : 0);

			else AddLong(p->Data);
			}

		if( b & 0x01 ) AddWord(0);
		}
	}

// Frame Building

void CBBBSAPSerialSDriver::StartFrame(void)
{
	m_pSend[0]  = DLE;

	m_pSend[1]  = STX;

	m_uTPtr = 2;
	}

void CBBBSAPSerialSDriver::EndFrame(void)
{
	m_pSend[m_uTPtr++] = DLE;

	m_pSend[m_uTPtr++] = ETX;

	AddCRC();
	}

void CBBBSAPSerialSDriver::AddCRC(void)
{
	BYTE crc_l = 0;
	BYTE crc_h = 0xFF;

	BOOL bUseDLE = FALSE;
	BOOL bSkip   = FALSE;

	for(UINT i = 2; i < m_uTPtr; i++ ) { // skip DLE, STX

		BYTE b = m_pSend[i];

		bSkip  = b == DLE && !bUseDLE; // skip 1st DLE if present

		if( !bSkip ) {

			UINT TPos  = 2 * (b ^ crc_l);

			crc_l = (crc_h ^ crctbl[TPos]);
			crc_h = crctbl[TPos+1];

			bUseDLE = FALSE;
			}

		else bUseDLE = TRUE;
		}

	m_pSend[m_uTPtr++] = ~crc_l;
	m_pSend[m_uTPtr++] = crc_h;
	}

void CBBBSAPSerialSDriver::AddByte(BYTE bData)
{
	m_bTx[m_uTPtr++] = bData;
	}

void CBBBSAPSerialSDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CBBBSAPSerialSDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CBBBSAPSerialSDriver::AddReal(DWORD dwData)
{
	AddLong(dwData);
	}

void CBBBSAPSerialSDriver::AddText(PCTXT pText)
{
	for( UINT i = 0; i < strlen(pText); i++ ) {

		AddByte(pText[i]);
		}
	}

void CBBBSAPSerialSDriver::AddTextPlusNull(PCTXT pText)
{
	AddText(pText);
	AddByte(0);
	}

// Transport Layer

void CBBBSAPSerialSDriver::Send(void)
{
	PBYTE pTx  = m_bTx;

	m_pSend    = new BYTE [sizeof(m_bTx)];

	UINT uEnd  = m_uTPtr;	// end of frame

	StartFrame();

	for( UINT i = 0, j = 2; i < uEnd; i++, j++ ) {

		BYTE b = pTx[i];

		if( b == DLE ) {

			m_pSend[j++] = DLE;
			}

		m_pSend[j] = b;
		}

	m_uTPtr = j;

	EndFrame();

	SendFrame();

	delete m_pSend;

	m_pSend = NULL;
	}

void CBBBSAPSerialSDriver::SendFrame(void)
{
//**/	AfxTrace0("\r\nSend: "); for( UINT k = 0; k < m_uTPtr; k++ ) AfxTrace1("[%2.2x]", m_pSend[k]);
	m_pData->Write(PCBYTE(m_pSend), m_uTPtr, FOREVER);
	}

// Ack
void CBBBSAPSerialSDriver::SendAckNak(BYTE bType)
{
	AddByte(0);			// Master Node Address
	AddByte(m_pHeadList[1]);	// Received SerialNumber
	AddByte(bType);
	AddByte(m_bDrop);
	AddByte(0);
	AddByte(1);

	Send();
	}

void CBBBSAPSerialSDriver::SendPollAck(void)
{
	SendAckNak(ACKPOLL);
	}

void CBBBSAPSerialSDriver::SendDownAck(void)
{
	SendAckNak(ACKDOWN);
	}

void CBBBSAPSerialSDriver::SendAlarmAck(void)
{
	}

void CBBBSAPSerialSDriver::SendNak(void)
{
	SendAckNak(NAKDATA);
	}

// RER
void CBBBSAPSerialSDriver::SetRER(BYTE bRER)
{
	BYTE b = m_bTx[TXRERPOS];

	m_bTx[TXRERPOS] = b ? RERMORE : bRER;
	}

// Helpers

BOOL CBBBSAPSerialSDriver::HasTags(PDWORD pData, UINT uCount)
{
	if( m_fHasTags ) return TRUE;

	for( UINT i = 0; i < uCount; i++ ) *pData = 0xFFFFFFFF;

	return FALSE;
	}

void CBBBSAPSerialSDriver::InitGlobal(void)
{
	m_pbDevName	= NULL;
	m_pdAddrList	= NULL;
	m_pbNameList	= NULL;
	m_pwNameIndex	= NULL;
	m_pwMSDList	= NULL;

	m_pListAddr	= NULL;
	m_pListData	= NULL;
	m_pListPosn	= NULL;

	m_uState	= 0;

	m_uTagCount	= 0;
	m_fHasTags	= FALSE;

	m_uNameListItem	= 0;

	m_fGlobal	= FALSE;
	m_wVersion	= m_Ident;

	m_pVarInfo	= NULL;
	m_pHeadList	= NULL;
	m_pReqList	= NULL;
	m_pDataList	= NULL;

	m_dLBMagic	= 0;
	m_dRCMagic	= 0;
	}

BOOL CBBBSAPSerialSDriver::CheckDrop(BYTE bData)
{
	if( BYTE(bData & 0x7F) == m_bDrop ) {

		m_fGlobal = bData & 0x80;

		return TRUE;
		}

	return FALSE;
	}

BOOL CBBBSAPSerialSDriver::IsPollOrAck(BYTE bData)
{
	if( bData >= POLLMSG && bData <= UPACK ) {

		return TRUE;
		}

	return bData == ALARMACK || bData == ALARMNOT;
	}

// Tag Data Loading
void CBBBSAPSerialSDriver::LoadTags(LPCBYTE &pData)
{
	if( StoreDevName(pData) ) {

		UINT uCount = GetWord(pData); // number of names

		if( uCount ) {

			m_uTagCount  = uCount;

			m_fHasTags   = TRUE;

			m_pdAddrList = new DWORD [uCount + 1];

			m_pwMSDList  = new WORD  [uCount + 1];

			for( UINT i = 0; i < uCount; i++ ) {

				m_pdAddrList[i] = GetLong(pData);

				m_pwMSDList[i]  = m_pdAddrList[i] & AMASK;
				}

			m_pdAddrList[uCount] = 0; // end marker
			m_pdAddrList[uCount] = 0xFFFF;

			UINT uTotalSize	= GetWord(pData) + uCount; // adding nulls

			m_pwNameIndex	= new WORD  [uCount + 1];
			m_pbNameList	= new BYTE  [uTotalSize + 1];

			UINT uNamePosition	  = 0;

			for( i = 0; i < uCount; i++ ) {

				StoreName(pData, i, &uNamePosition);
				}

			m_pwNameIndex[uCount]	= uTotalSize; // end marker
			}
		}
	}

BOOL CBBBSAPSerialSDriver::StoreDevName(LPCBYTE &pData)
{
	UINT uCount = GetWord(pData); // size of device name

	if( uCount ) {

		BOOL fWide  = m_bIsC3 == 3;

		m_pbDevName = new BYTE[uCount];

		memset(m_pbDevName, 0, uCount);

		for( UINT i = 0; i < uCount; i++ ) {

			m_pbDevName[i] = fWide ? LOBYTE(GetWord(pData)) : GetByte(pData);
			}

		return TRUE;
		}

	return FALSE;
	}

void CBBBSAPSerialSDriver::StoreName(LPCBYTE &pData, UINT uItem, UINT *pPosition)
{
	UINT uByteCount	= GetWord(pData);

	UINT uPos	= *pPosition;

	*pPosition	+= uByteCount;

	m_pwNameIndex[uItem] = uPos; // position of start of name

	BOOL fWide      = m_bIsC3 == 3;

	BYTE b		= GetByte(pData);

	if( b ) {
		fWide	= FALSE;
		}

	else {
		b = GetByte(pData);
		}

	BOOL fUpper = b == '.';		// 3300 tag name

	if( !fUpper ) m_pbNameList[uPos++] = b;	// add first char if not '.'

	for( UINT i = 1; i < uByteCount; i++ ) {

		BYTE b = fWide ? LOBYTE(GetWord(pData)) : GetByte(pData); // get name char

		m_pbNameList[uPos++] = fUpper ? MakeUpper(b) : b;
		}
	}

BYTE CBBBSAPSerialSDriver::MakeUpper(BYTE b)
{
	if( b >= 'a' && b <= 'z' ) b &= 0x5F;

	return b;
	}

// Delete created buffers
void CBBBSAPSerialSDriver::CloseListBuffers(void)
{
	if( m_dLBMagic == 0x12345678 ) {

		if( m_pListAddr ) { delete m_pListAddr; m_pListAddr = NULL; }

		if( m_pListData ) { delete m_pListData; m_pListData = NULL; }

		if( m_pListPosn ) { delete m_pListPosn; m_pListPosn = NULL; }
		}
	}

void CBBBSAPSerialSDriver::CloseDown(void)
{
	if( m_pbDevName   ) { delete m_pbDevName;   m_pbDevName = NULL;   }

	if( m_pbNameList  ) { delete m_pbNameList;  m_pbNameList = NULL;  }

	if( m_pwNameIndex ) { delete m_pwNameIndex; m_pwNameIndex = NULL; }

	if( m_pdAddrList  ) { delete m_pdAddrList;  m_pdAddrList = NULL;  }

	if( m_pwMSDList   ) { delete m_pwMSDList;   m_pwMSDList = NULL;   }

	CloseListBuffers();

	ListDeAlloc();
	}

void CBBBSAPSerialSDriver::ListDeAlloc(void)
{
	if( m_dRCMagic == 0x12345678 ) {

		if( m_pHeadList ) { delete m_pHeadList; m_pHeadList = NULL; }

		if( m_pFuncList ) { delete m_pFuncList; m_pFuncList = NULL; }

		if( m_pReqList  ) { delete m_pReqList;  m_pReqList  = NULL; }

		if( m_pDataList ) { delete m_pDataList; m_pDataList = NULL; }
		}
	}

// Conversions

DWORD CBBBSAPSerialSDriver::LongToReal(UINT uType, DWORD Data)
{
	switch(uType) {

		case addrByteAsByte:	Data &= 0xFF;	break;

		case addrWordAsWord:	Data &= 0xFFFF;	break;
		}

	float Real = float(LONG(Data));

	switch( uType ) {

		case addrBitAsBit:	return (DWORD)((BOOL)Data);
		case addrByteAsByte:	return  R2I(Real);
		case addrWordAsWord:	return  R2I(Real);
		case addrLongAsLong:	return  R2I(Real);
		}

	return Data;
	}

DWORD CBBBSAPSerialSDriver::RealToLong(UINT uType, DWORD Data)
{
	float Real = I2R(Data);

	switch( uType ) {

		case addrBitAsBit:	return (DWORD)((BOOL)Data);
		case addrByteAsByte:	return (DWORD)LOBYTE(Real);
		case addrWordAsWord:	return (DWORD)LOWORD(Real);
		case addrLongAsLong:	return (DWORD)Real;
		}

	return Data;
	}

// End of File
