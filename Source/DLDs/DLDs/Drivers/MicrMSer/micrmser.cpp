
#include "intern.hpp"

#include "micrmser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Micromod Serial Driver
//

// Instantiator

INSTANTIATE(CMicromodSerialDriver);

// Constructor

CMicromodSerialDriver::CMicromodSerialDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	m_pTx		= m_bTx;

	m_pRx		= m_bRx;

	m_uTimeout	= 1000;

	m_dRDBO		= 0;

	m_bNAKResp	= 0;

	m_bSeq		= 1;
	}

// Destructor

CMicromodSerialDriver::~CMicromodSerialDriver(void)
{
	if( m_pCtx ) {

		if( m_pCtx->m_wArraySize ) {

			delete m_pCtx->m_pLSPArray;
			delete m_pCtx->m_pOFFArray;
			delete m_pCtx->m_pSZEArray;
			delete m_pCtx->m_pBITArray;
			}
		}
	}

// Configuration

void MCALL CMicromodSerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CMicromodSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CMicromodSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMicromodSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CMicromodSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop		= GetByte(pData);
			m_pCtx->m_bDataBaseType	= GetByte(pData) << 6;
			m_pCtx->m_uUnlock       = GetLong(pData);

			m_pCtx->m_wArraySize	= GetWord(pData);

			m_pCtx->m_uACKSHUTPosition = 0xFFFF;

			ClearPackFG();

			if( (BOOL)m_pCtx->m_wArraySize ) { // entry count + list terminator

				AddArrays(pData);
				}

			m_pCtx->m_uWriteErr	= 0;
			m_pCtx->m_dLatestErr	= 0;

			// Generic Command Cache
			memset(m_pCtx->m_bHead, 0,  4);
			memset(m_pCtx->m_dCDAT, 0, GENSIZE * 4);
			memset(m_pCtx->m_dRDAT, 0, GENSIZE * 4);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMicromodSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		if( m_pCtx->m_wArraySize ) {

			delete m_pCtx->m_pLSPArray;
			delete m_pCtx->m_pOFFArray;
			delete m_pCtx->m_pSZEArray;
			}

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMicromodSerialDriver::Ping(void)
{
	if( m_pCtx->m_bDrop ) {

//**/		AfxTrace0("\r\nPING\r\n");

		DWORD    Data[1];

		CAddress Addr;

		CCODE cc = CCODE_ERROR;

		if( IsValid65Access(0) ) {

			Addr.a.m_Table	= SP_65;
			Addr.a.m_Offset	= 0;
			Addr.a.m_Extra	= 0;
			Addr.a.m_Type	= addrLongAsLong;

			cc = Read(Addr, Data, 1);
			}

		if( cc != 1 ) {

			Addr.a.m_Table	= SP_HOLD;
			Addr.a.m_Offset	= 1;
			Addr.a.m_Extra	= 0;
			Addr.a.m_Type	= addrWordAsWord;

			cc = Read(Addr, Data, 1);
			}

		return cc;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMicromodSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_bDrop ) {

		return uCount;
		}

//**/	Sleep(100); // slow debug
//**/	AfxTrace3("\r\n\nRead T=%d O=%x E=%x ", Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Extra);
//**/	AfxTrace2("t=%x, C=%d ", Addr.a.m_Type, uCount);

	if( IsFuncCode65Item(Addr.a.m_Table) ) {

		if( IsValid65Access(Addr.a.m_Offset) ) { // has a valid database

			if( NoReadTransmit(Addr, &uCount) ) { // if Address IF_141 - return 0 when read

				*pData = 0;

				return 1;
				}

			MakeMin(uCount, 40);

			return Handle65(Addr, pData, uCount, TRUE);
			}

		m_pCtx->m_dLatestErr = (DWORD)0xFFFFFFFF;

		return 1;	// ignore this item if not valid
		}

	SetModbusData(Addr);

	CCODE cc = CCODE_ERROR;

	switch( m_uTable ) {

		case SP_HOLD:

			if( m_uType == addrWordAsWord ) {

				MakeMin(uCount, 16);

				cc = DoWordRead(pData, uCount);
				}

			else {
				MakeMin(uCount, 8);

				cc = DoLongRead(pData, uCount);
				}
			break;

		case SP_ANALOG:

			if( m_uType == addrWordAsWord ) {

				MakeMin(uCount, 16);

				cc = DoWordRead(pData, uCount);
				}

			else {
				MakeMin(uCount, 8);

				cc = DoLongRead(pData, uCount);
				}
			break;

		case SP_HOLD32:

			MakeMin(uCount, 8);
			
			cc = DoLongRead(pData, uCount);
			break;

		case SP_ANALOG32:

			MakeMin(uCount, 8);
			
			cc = DoLongRead(pData, uCount);
			break;

		case SP_OUTPUT:

			MakeMin(uCount, 16);
			
			cc = DoBitRead (pData, uCount);
			break;

		case SP_INPUT:

			MakeMin(uCount, 16);
			
			cc = DoBitRead (pData, uCount);
			break;

		case SP_ERR:

			*pData = m_pCtx->m_dLatestErr;

			return 1;

		case SP_NAK:

			*pData = m_bNAKResp;

			return 1;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	return cc;
	}

CCODE MCALL CMicromodSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n*** WRITE *** T=%d O=%x E=%x ", Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Extra);
//**/	AfxTrace2("t=%x, C=%d ", Addr.a.m_Type, uCount);

	UINT uTable = Addr.a.m_Table;

	if( IsFuncCode65Item(uTable) ) {

		if( IsValid65Access(Addr.a.m_Offset) ) {

			if( !Is65Command(uTable) ) {

				if( IsAckShutdown(Addr.a.m_Offset) ) {	// special command

					if( *pData == 1 ) {

						SendAckShutdown();
						}

					return 1;
					}
				}

			return Handle65(Addr, pData, 1, FALSE);
			}

		return 1;
		}

	SetModbusData(Addr);

	switch( m_uTable ) {

		case SP_HOLD:

			if( m_uType == addrWordAsWord ) {

				MakeMin(uCount, 16);

				return DoWordWrite(pData, uCount);
				}

			MakeMin(uCount, 8);

			return DoLongWrite(pData,uCount);

		case SP_HOLD32:

			MakeMin(uCount, 8);
			
			return DoLongWrite(pData,uCount);

		case SP_OUTPUT:

			MakeMin(uCount, 16);
			
			return DoBitWrite (pData, uCount);

		case SP_ERR:

			m_pCtx->m_dLatestErr = 0;

			return 1;

		case SP_NAK:

			m_bNAKResp = 0;

			return 1;

		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

// Port Access

UINT CMicromodSerialDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CMicromodSerialDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;
	
	AddByte(m_pCtx->m_bDrop);
	
	AddByte(bOpcode);
	}

void CMicromodSerialDriver::AddByte(BYTE bData)
{
	if( m_uPtr < BUFFSIZE ) {
	
		m_pTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CMicromodSerialDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CMicromodSerialDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CMicromodSerialDriver::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {
	
		if( !m_pCtx->m_bDrop ) { 

			while( m_pData->Read(0) < NOTHING );

			return TRUE;
			}
			
		if( GetFrame() ) {

			if( m_pRx[0] == m_pTx[0] ) {

				if( fIgnore ) {

					return TRUE;
					}

				if( m_pRx[1] & 0x80 ) {

					m_bNAKResp = m_pRx[2];
		
					return FALSE;
					}

				return TRUE;
				}
			}
		}
		
	return FALSE;
	}

BOOL CMicromodSerialDriver::PutFrame(void)
{
	Sleep(20);

	m_pData->ClearRx();
	
	return BinaryTx();
	}

BOOL CMicromodSerialDriver::GetFrame(void)
{
	return BinaryRx();
	}

BOOL CMicromodSerialDriver::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

//**/	AfxTrace0("\r\n"); for(UINT k=0; k<m_uPtr; k++) AfxTrace1("[%2.2x]", m_pTx[k]);

	m_pData->Write(m_pTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CMicromodSerialDriver::BinaryRx(void)
{
	UINT uByte  = 0;

	UINT uPtr   = 0;

	UINT uGap   = 0;

	UINT uEnd   = FindEndTime();

	UINT u65End = BUFFSIZE;

	BOOL f65    = IsFuncCode65Item(m_uTable);

	if( f65 ) uEnd *= 2;

	SetTimer(m_uTimeout);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (uByte = RxByte(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

//**/			AfxTrace1("<%2.2x>", uByte);

			if( uPtr >= BUFFSIZE ) return FALSE;

			uGap = 0;

			if( f65 && uPtr == 4 ) {

				u65End = uByte + 6; // Header(4) + data size + CRC(2)
				}
			}
		else
			uGap = uGap + 1;

		if( uGap >= uEnd || uPtr >= u65End ) {

			if( m_bRx[0] == m_pCtx->m_bDrop ) {

				if( uPtr >= 4 ) {

					m_CRC.Preset();

					PBYTE p = m_pRx;

					UINT  n = uPtr - 2;

					for( UINT i = 0; i < n; i++ ) {

						m_CRC.Add(*(p++));
						}

					WORD c1 = IntelToHost(PU2(p)[0]);

					WORD c2 = m_CRC.GetValue();

					return c1 == c2;
					}
				}

			uPtr  = 0;
			
			uGap  = 0;
			}
		}

	return FALSE;
	}

// Transport Helpers

UINT CMicromodSerialDriver::FindEndTime(void)
{
	return ToTicks(25);
	}

// Read Handlers

CCODE CMicromodSerialDriver::DoWordRead(PDWORD pData, UINT uCount)
{
	switch( m_uTable ) {
	
		case SP_HOLD:
			
			StartFrame(0x03);
			
			break;
			
		case SP_ANALOG:
			
			StartFrame(0x04);
			
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(m_uOffset - 1);
	
	AddWord(uCount);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x   = PU2(m_pRx + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CMicromodSerialDriver::DoLongRead(PDWORD pData, UINT uCount)
{
	UINT uTable = m_uTable;

	switch( uTable ) {
	
		case SP_HOLD32:
		case SP_HOLD:
			
			StartFrame(0x03);
			
			break;
			
		case SP_ANALOG32:
		case SP_ANALOG:
			
			StartFrame(0x04);
			
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(m_uOffset - 1);
	
	AddWord(uTable == SP_HOLD32 ? uCount : uCount * 2);
	
	if( Transact(FALSE) ) {

		if( uCount * 4 > m_pRx[2] ) {

			uCount = (m_pRx[2] + 2) / 4;
			}
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_pRx + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CMicromodSerialDriver::DoBitRead(PDWORD pData, UINT uCount)
{
	switch( m_uTable ) {
			
		case SP_OUTPUT:
			
			StartFrame(0x01);
			
			break;
			
		case SP_INPUT:
			
			StartFrame(0x02);
			
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(m_uOffset - 1);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_pRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CMicromodSerialDriver::DoWordWrite(PDWORD pData, UINT uCount)
{
	UINT uOffset  = m_uOffset - 1;

	if( m_uTable == SP_HOLD ) {

		if( uCount == 1 ) {
			
			StartFrame(6);
			
			AddWord(uOffset);
			
			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);
			
			AddWord(uOffset);
			
			AddWord(uCount);
			
			AddByte(uCount * 2);
			
			for( UINT n = 0; n < uCount; n++ ) {
				
				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CMicromodSerialDriver::DoLongWrite(PDWORD pData, UINT uCount)
{
	UINT uTable = m_uTable;
		
	if( uTable == SP_HOLD32 || uTable == SP_HOLD ) {

		StartFrame(16);
		
		AddWord(m_uOffset - 1);
		
		AddWord(uTable == SP_HOLD32 ? uCount : uCount * 2);
		
		AddByte(uCount * 4);
		
		for( UINT n = 0; n < uCount; n++ ) {
			
			AddLong(pData[n]);
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CMicromodSerialDriver::DoBitWrite(PDWORD pData, UINT uCount)
{
	UINT uOffset  = m_uOffset - 1;

	if( m_uTable == SP_OUTPUT ) {

		if( uCount == 1 ) {
			
			StartFrame(5);
			
			AddWord(uOffset);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			StartFrame(15);

			AddWord(uOffset);

			AddWord(uCount);

			AddByte((uCount + 7) / 8);

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b);
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Function Code 65 Handlers

CCODE CMicromodSerialDriver::Handle65(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsRead)
{
	UINT uTable = Addr.a.m_Table;

	switch( uTable ) {

		case SP_RDB:
			if( !fIsRead ) {

				return uCount;
				}

			MakeMin(uCount, 10);

			m_uPtr = SetRDBCommand(Addr, pData, uCount);
			break;

		case SP_RDBO:
			if( fIsRead ) *pData = m_dRDBO;

			else m_dRDBO = *pData;

			return 1;

		default:
			fIsRead ? ReadList(Addr, uCount) : WriteItems(Addr, *pData);
			break;
		}

	memset(m_bRx, 0, 4);

	if( Transact(FALSE) ) {

		if( m_bRx[0] == m_pCtx->m_bDrop && m_bRx[1] == 0x41 ) {

			if( fIsRead ) {

				return Get65Data(Addr, pData, uCount);
				}

			m_dWriteErr = 0;

			return 1;
			}
		}

	if( !fIsRead ) {

		if( IsDuplicateError() ) {

			if( HandleDuplicateSeqNum(Addr, *pData) ) {

				m_dWriteErr = 0;

				return 1;
				}
			}

		if( (m_bRx[1] & 0x80) || (++m_dWriteErr >= 3) ) {

			m_dWriteErr = 0;

			return 1;
			}
		}

	return (m_bRx[1] & 0x80) ? CCODE_ERROR | CCODE_NO_RETRY : CCODE_ERROR;
	}

void CMicromodSerialDriver::StartFrame65(void)
{
	m_uPtr = 0;

	AddByte(m_pCtx->m_bDrop);

	AddByte(0x41);

	AddByte(9);
	}

UINT CMicromodSerialDriver::SetRDBCommand(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame65();

	AddByte(QMFCRDB);

	AddByte((Addr.a.m_Offset >> 4) & 0xC0); // database to access

	AddByte(5);		// total 

	Put65Data(m_dRDBO, 4);	// address to access

	AddByte(uCount * 4);	// number of bytes to read

	return 11; // frame size
	}

void CMicromodSerialDriver::ReadList(AREF Addr, UINT uCount)
{
	StartFrame65();

	AddByte(QMFCMFG);

	AddByte(0);

	UINT uFirst	= Addr.a.m_Offset;

	UINT uRecvSize	= GetDataSize(uFirst, &uCount);

	AddByte(6);	// size for sections

	m_pCtx->m_uPackFG[0] = 1;

	m_pCtx->m_uPackFG[1] = uFirst;

	AddByte(1);

	AddByte(1);	// read sections

	AddWord(m_pCtx->m_pOFFArray[uFirst]);	// starting offset

	AddWord(LOWORD(uRecvSize));	// byte count to read
	}

void CMicromodSerialDriver::WriteItems(AREF Addr, DWORD dData)
{
	StartFrame65();

	AddByte(QMFCSWA);

	AddByte(0);

	UINT uItem = Addr.a.m_Offset;

	UINT uSize = m_pCtx->m_pSZEArray[uItem];

	AddByte(6 + uSize);

	AddLong(m_pCtx->m_pLSPArray[uItem]);

	AddByte(0);

	AddByte((m_pCtx->m_bDrop << 4) + m_bSeq);

	m_bSeq = (m_bSeq + 5) % 16;

	switch( uSize ) {

		case 1:
			AddByte(LOBYTE(dData));
			break;

		case 2:
			AddWord(LOWORD(dData));
			break;

		case 3:
			AddByte(LOBYTE(HIWORD(dData)));
			AddWord(LOWORD(dData));
			break;

		default:
			AddLong(dData);

			while( uSize > 4 ) {

				AddByte(0);

				uSize--;
				}
			break;
		}
	}

void CMicromodSerialDriver::Put65Data(DWORD dData, UINT uCount)
{
	switch( uCount ) {

		case 4: AddByte(HIBYTE(HIWORD(dData)));
		case 3: AddByte(LOBYTE(HIWORD(dData)));
		case 2: AddByte(HIBYTE(LOWORD(dData)));
		case 1: AddByte(LOBYTE(LOWORD(dData)));
		}
	}

BOOL CMicromodSerialDriver::HandleDuplicateSeqNum(AREF Addr, DWORD dData)
{
	for( UINT i = 0; i < 5; i++ ) {

		m_bSeq++;

		if( DoDuplicateWrite(Addr, dData) ) {

			return TRUE;
			}

		if( !IsDuplicateError() ) {

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CMicromodSerialDriver::DoDuplicateWrite(AREF Addr, DWORD dData)
{
	WriteItems(Addr, dData);

	if( Transact(FALSE) ) {

		if( m_bRx[1] == EXTMOD ) return TRUE;
		}

	return FALSE;
	}

BOOL CMicromodSerialDriver::IsDuplicateError(void)
{
	return (m_bRx[1] == (EXTMOD | 0x80)) && (m_bRx[2] == 0x71);
	}

// Special Command - uses Write_Attribute instead of Seq_Write_Attribute

void CMicromodSerialDriver::SendAckShutdown(void)
{
	StartFrame65();

	// Write Attribute command
	AddByte(QMFATW);
	// Qualifier
	AddByte(0);
	// Byte count following
	AddByte(5);
	// LSP of IF_141(1)
	AddByte(0);
	AddByte(0x40);
	AddByte(0);
	AddByte(141);
	// Data
	AddByte(1);

	Transact(TRUE); // ignore response
	}

// Receive Handling

CCODE CMicromodSerialDriver::Get65Data(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SP_RDB:	return GetRDBData(pData, uCount);
		}

	return GetAttributeData(Addr, pData, uCount);
	}

CCODE CMicromodSerialDriver::GetAttributeData(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bRcvSz  = m_bRx[RX_COUNT];	// number of received data bytes

	PBYTE pRcvD  = &m_bRx[RX_DATA];

	if( pRcvD[0] == 1 ) { // first item is in error

		SetADError(Addr, 0);

		return 1;	// try next
		}

	UINT uAdd = Addr.a.m_Offset;

	UINT uEnd = pRcvD[0];	// if !0, valid data up to this point

	if( !(BOOL)uEnd ) {

		uEnd = bRcvSz;	// all are valid data
		}

	uEnd--;			// adjust for error byte

	pRcvD++;		// first data byte

	UINT uItemsDone	= 0;

	UINT uByteTotal	= 0;

	for( UINT i = 0; i < uCount; i++ ) {

		UINT uBit = m_pCtx->m_pBITArray[uAdd + i];

		if( uBit < 0xFF ) { // might be multiple bits in this byte

			UINT uThisO = m_pCtx->m_pOFFArray[uAdd + i];

			BYTE b = pRcvD[uByteTotal++];

			UINT uMask = 1 << uBit;

			while( i < uCount ) {

				if( LOBYTE(uMask) ) {

					pData[i] = (DWORD)((uMask & b) ? 0xFFFFFFFF : 0);

					uItemsDone++;

					if( uThisO == m_pCtx->m_pOFFArray[uAdd + i + 1] ) {

						i++;

						uMask <<= 1;
						}

					else uMask = 0;
					}

				else break;
				}
			}

		else {
			UINT uItemSize	= m_pCtx->m_pSZEArray[uAdd + i];

			UINT uPos	= uByteTotal + RX_DATA + 1;

			switch( uItemSize ) {

				case 1:
					pData[i] = pRcvD[uByteTotal];
					break;

				case 2:
					pData[i] = GetDataWORD(uPos);
					break;

				case 3:
					pData[i] = GetDataDWORD(uPos);
					pData[i] >>= 8;
					break;

				default:
					pData[i] = GetDataDWORD(uPos);
					break;
				}

			uByteTotal += uItemSize;

			uItemsDone++;
			}

		if( uByteTotal >= uEnd ) {	// hit the next error or end of buffer

			return uItemsDone;
			}
		}

	return CCODE_ERROR;
	}

void CMicromodSerialDriver::SetADError(AREF Addr, UINT uPos)
{
	m_pCtx->m_dLatestErr = m_pCtx->m_pLSPArray[Addr.a.m_Offset + uPos]; // store latest error LSP
	}

CCODE CMicromodSerialDriver::GetRDBData(PDWORD pData, UINT uCount)
{
	for( UINT i = 0; i < uCount; i++ ) {

		pData[i] = GetDataDWORD(4 + (4 * i));
		}

	return uCount;
	}

DWORD CMicromodSerialDriver::GetDataDWORD(UINT uPos)
{
	return (DWORD)MotorToHost(*PU4(&m_bRx[uPos]));
	}

DWORD CMicromodSerialDriver::GetDataDWORD(PBYTE pData)
{
	return (DWORD)MotorToHost(*PU4(pData));
	}

WORD CMicromodSerialDriver::GetDataWORD(UINT uPos)
{
	return (WORD)MotorToHost(*PU2(&m_bRx[uPos]));
	}

// Helpers

void CMicromodSerialDriver::SetModbusData(AREF Addr)
{
	m_uTable  = Addr.a.m_Table;
	m_uOffset = Addr.a.m_Offset;
	m_uExtra  = Addr.a.m_Extra;
	m_uType   = Addr.a.m_Type;
	}

BOOL CMicromodSerialDriver::IsFuncCode65Item(UINT uTable)
{
	switch( uTable ) {

		case SP_65:
		case SP_RDB:
		case SP_RDBO:

			return TRUE;
		}

	return FALSE;
	}

BOOL CMicromodSerialDriver::Is65Command(UINT uTable)
{
	switch( uTable ) {

		case SP_RDB:
		case SP_RDBO:
			return TRUE;
		}

	return FALSE;
	}

BOOL CMicromodSerialDriver::NoReadTransmit(AREF Addr, UINT *pCount)
{
	UINT uACK = m_pCtx->m_uACKSHUTPosition;

	UINT uAdd = Addr.a.m_Offset;

	if( uAdd != uACK ) {

		if( uAdd < uACK && uAdd + *pCount > uACK ) {

			*pCount = uACK - uAdd;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CMicromodSerialDriver::IsAckShutdown(UINT uOffset)
{
	return m_pCtx->m_pLSPArray[uOffset] == ACKSHUTLSP;
	}

BOOL CMicromodSerialDriver::IsValid65Access(UINT uItem)
{
	return m_pCtx->m_wArraySize > 1 && m_pCtx->m_wArraySize > uItem;
	}

// Array Help

UINT CMicromodSerialDriver::GetTableNum(UINT uArrayItem)
{
	DWORD dLSP = m_pCtx->m_pLSPArray[uArrayItem];

	return (dLSP & LSP_BLK_MASK) >> LSP_BLK_POS; 
	}

UINT CMicromodSerialDriver::GetParamNum(UINT uArrayItem)
{
	DWORD dLSP = m_pCtx->m_pLSPArray[uArrayItem];

	return dLSP & LSP_PAR_MASK;
	}

UINT CMicromodSerialDriver::GetDataSize(UINT uStart, UINT *pCount)
{
	if( (BOOL)m_pCtx->m_uPackFG[0] ) {		// controller saved the entire block that is being read in parts

		if( uStart != m_pCtx->m_uPackFG[1] ) {	// system is not requesting the next packed item, clear pack flag

			ClearPackFG();
			}
		}

	PWORD pOFF	= m_pCtx->m_pOFFArray;
	PWORD pSZE	= m_pCtx->m_pSZEArray;
	PWORD pBIT	= m_pCtx->m_pBITArray;

	UINT uMax	= sizeof(m_bRx) - 16;	// leave room for one more after uMax byte count is reached

	UINT uTotalOFF	= 0;

	UINT uCnt	= *pCount;

	UINT uLast	= uStart + uCnt;

	if( uLast > (UINT)m_pCtx->m_wArraySize - 1 ) {

		uLast = (UINT)m_pCtx->m_wArraySize - 1;	// don't examine terminator
		}

	UINT uItem	= uStart;

	while( uItem < uLast ) {

		UINT uSize = pSZE[uItem];

		UINT uOff0 = pOFF[uItem];

		if( uTotalOFF < uMax ) {			// response will fit in rcv buffer

			if( pBIT[uItem] < 0xFF ) {		// handle multiple bits in one offset

				UINT uFirst = uItem;

				while( uItem < uLast ) {

					uItem++;

					if( uOff0 != pOFF[uItem] ) { // next item has different offset

						break;
						}
					}

				uTotalOFF++;
				}

			else {
				if( uOff0 + uSize == pOFF[uItem+1] ) {	// next offset is contiguous to this one

					uTotalOFF += uSize;

					uItem++;
					}

				else {					// next offset is not contiguous, stop this read here
					*pCount = uItem - uStart + 1;

					ClearPackFG();			// next read will not be of packed item

					return uTotalOFF + uSize;
					}
				}
			}

		else {
			m_pCtx->m_uPackFG[0]++;		// expect to get packed data next

			m_pCtx->m_uPackFG[1] = uItem;	// next item to be called in packed list

			break;
			}
		}

	*pCount = uItem - uStart;

	return uTotalOFF;
	}

void CMicromodSerialDriver::ClearPackFG(void)
{
	memset(m_pCtx->m_uPackFG, 0, sizeof(m_pCtx->m_uPackFG));
	}

// Array Loading

void CMicromodSerialDriver::AddArrays(LPCBYTE &pData)
{
	UINT uSize	= m_pCtx->m_wArraySize;

	m_pCtx->m_pLSPArray	= new DWORD [uSize];
	m_pCtx->m_pOFFArray	= new WORD  [uSize];
	m_pCtx->m_pSZEArray	= new WORD  [uSize];
	m_pCtx->m_pBITArray	= new WORD  [uSize];

	BOOL fNoAckShutdown	= TRUE;

	for( UINT i = 0; i < uSize; i++ ) {

		DWORD d = GetLong(pData);

		m_pCtx->m_pLSPArray[i] = d;

		if( fNoAckShutdown ) {

			if( IsAckShutdown(i) ) {

				m_pCtx->m_uACKSHUTPosition = i;

				fNoAckShutdown = FALSE;
				}
			}

		if( d == 0xFFFFFFFF ) break;
		}

	for( i = 0; i < uSize; i++ ) {

		WORD w = GetWord(pData);

		m_pCtx->m_pOFFArray[i] = w;

		if( w == 0xFFFF ) break;
		}

	for( i = 0; i < uSize; i++ ) {

		WORD w = GetWord(pData);

		m_pCtx->m_pSZEArray[i] = w;

		if( w == 0xFFFF ) break;
		}

	for( i = 0; i < uSize; i++ ) {

		WORD w = GetWord(pData);

		m_pCtx->m_pBITArray[i] = w;

		if( w == 0xFFFF ) break;
		}
	}

// End of File
