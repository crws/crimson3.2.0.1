
//////////////////////////////////////////////////////////////////////////
//
// Micromod Serial Data Spaces
//

#define	SP_HOLD		0x01
#define	SP_ANALOG	0x02
#define	SP_OUTPUT	0x03
#define	SP_INPUT	0x04
#define	SP_HOLD32	0x05
#define	SP_ANALOG32	0x06

// Function code 65 Functions
enum {
	SP_65	=  9,
	SP_IF	= 10,
	SP_SE	= 11,
	SP_ICN	= 12,
	SP_OMC	= 13,
	SP_ES	= 14,
	SP_LP	= 15,
	SP_CL	= 16,
	SP_08	= 17,
	SP_NM	= 18,
	SP_TM	= 19,
	SP_TOT	= 20,
	SP_LN	= 21,
	SP_PW	= 22,
	SP_PA	= 23,
	SP_EX	= 24,
	SP_IC	= 25,
	SP_OC	= 26,
	SP_DI	= 27,
	SP_VCI	= 28,
	SP_CJI	= 29,
	SP_TI	= 30,
	SP_TTI	= 31,
	SP_DIM	= 32,
	SP_DOM	= 33,
	SP_DDOM	= 34,
	SP_WDOM	= 35,
	SP_VCIM	= 36,
	SP_CJIM	= 37,
	SP_TIM	= 38,
	SP_AOM	= 39,
	SP_RTI	= 40,
	SP_RIM	= 41,
	SP_RTTI	= 42,
	SP_WRIM	= 43,
	SP_PID	= 44,
	SP_RI	= 45,
	SP_MSC	= 46,
	SP_ML	= 47,
	SP_SM	= 48,
	SP_SEQ	= 49,
	SP_RIO	= 50,
	SP_RDIM	= 51,
	SP_RDOM	= 52,
	SP_44	= 53,
	SP_45	= 54,
	SP_46	= 55,
	SP_47	= 56,
	SP_AIN	= 57,
	SP_AOUT	= 58,
	SP_DISP	= 59,
	SP_DIF	= 60,
	SP_PAD	= 61,
	SP_TL	= 62,
	SP_ST	= 63,
	SP_TMPL	= 64,
	SP_RSK	= 65,

	// Commands
	SP_RDB	= 66,
	SP_RDBO	= 67,

	// Generic Command
	SP_GNR	= 95,
	SP_CMQC	= 96,
	SP_CDAT	= 97,
	SP_RDAT	= 98,

	SP_ERR	= 99,
	SP_NAK	= 100
	};

#define	LSP_BLK_POS	22
#define	LSP_OCC_POS	10

#define	LSP_BLK_MASK	0x3FC00000
#define	LSP_OCC_MASK	0x003FFC00
#define	LSP_PAR_MASK	0x000003FF

// Comms buffer positions
// Common to Tx and Rx
#define	XR_ADDR		0
#define	XR_MFC		1
#define	XR_CONT		2

// Tx buffer positions
#define	TX_IMFC		3
#define	TX_QUAL		4
#define	TX_COUNT	5
#define	TX_LSP		6
#define	TX_CRC		10

// Rx buffer positions
#define	RX_SLVADD	0
#define	RX_MFC		1
#define	RX_CB		2
#define RX_COUNT	3
#define	RX_DATA		4

// Function Code 65 Qualifiers
#define	EXTMOD		0x41	// Function code

// Control Byte
#define	QCONSOLE	0x03	// Local Control Panel Console

// Read MFC's
#define	QMFCAT		0x00	// Read Attribute
#define	QMFCRAQ		0x01	// Read Attribute with Quality
#define	QMFCREQ		0x02	// Read Event Queue
#define	QMFCRDB		0x03	// Read Database
#define	QMFCRDS		0x06	// Read Diagnostic Status
#define	QMFCRFD		0x07	// Refresh Diagnostic Status
#define	QMFCSA		0x14	// Read String Attribute
#define	QMFCIV		0x18	// Read Instrument Version
#define	QMFCMFG		0x1A	// Read MSC Foreground

// Write MFC's
#define	QMFATW		0x09	// Write Attribute
#define	QMFCAA		0x0F	// Alarm Ack
#define	QMFCDA		0x10	// Diagnostic Ack
#define	QMFCSWA		0x17	// Seq Write Attribute

// Bad Command
#define	QMFCFAIL	99

#define	BUFFSIZE	300
#define	GENSIZE		 10	// DWORDS

#define	ACKSHUT		141	// Ack shutdown parameter number
#define	ACKSHUTLSP	(0x0040008D)

#define	F65READ		0
#define	MDBREAD		1
#define	F65WRITE	2
#define	MDBWRITE	3

//////////////////////////////////////////////////////////////////////////
//
// Micromod Serial Driver
//

class CMicromodSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CMicromodSerialDriver(void);

		// Destructor
		~CMicromodSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_bDataBaseType;
			UINT	m_uWriteErr;
			DWORD	m_dLatestErr;
			UINT	m_uUnlock;

			WORD	m_wArraySize;

			PDWORD	m_pLSPArray;
			PWORD	m_pOFFArray;
			PWORD	m_pSZEArray;
			PWORD	m_pBITArray;

			UINT	m_uACKSHUTPosition;

			UINT	m_uPackFG[2];

			// Generic Command Cache
			BYTE	m_bHead[ 4];
			DWORD	m_dCDAT[GENSIZE];
			DWORD	m_dRDAT[GENSIZE];
			};

		// Data Members
		CContext * m_pCtx;

		BYTE	m_bSeq;

		BYTE	m_bTx[BUFFSIZE];
		BYTE	m_bRx[BUFFSIZE];
		BYTE	* m_pTx;
		BYTE	* m_pRx;

		// NAK response
		BYTE	m_bNAKResp;

		UINT	m_uPtr;
		CRC16	m_CRC;
		UINT	m_uTimeout;

		UINT	m_uTable;
		UINT	m_uOffset;
		UINT	m_uExtra;
		UINT	m_uType;
		UINT	m_u65Table;

		DWORD	m_dWriteErr;
		DWORD	m_dRDBO;
				
		// Implementation

		// Port Access
		UINT	RxByte(UINT uTime);
		
		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddExtended(void);
		
		// Transport Layer
		BOOL	Transact(BOOL fIgnore);
		BOOL	PutFrame(void);
		BOOL	GetFrame(void);
		BOOL	BinaryTx(void);
		BOOL	BinaryRx(void);

		// Transport Helpers
		UINT	FindEndTime(void);
		
		// Read Handlers
		CCODE	DoWordRead(PDWORD pData, UINT uCount);
		CCODE	DoLongRead(PDWORD pData, UINT uCount);
		CCODE	DoBitRead (PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	DoWordWrite(PDWORD pData, UINT uCount);
		CCODE	DoLongWrite(PDWORD pData, UINT uCount);
		CCODE	DoBitWrite (PDWORD pData, UINT uCount);

		// Function code 65 Handlers
		// Frame Building & Transmit
		CCODE	Handle65(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsRead);
		void	StartFrame65(void);
		UINT	SetRDBCommand(AREF Addr, PDWORD pData, UINT uCount);
		void	ReadList(AREF Addr, UINT uCount);
		void	WriteItems(AREF Addr, DWORD dData);
		void	Put65Data(DWORD dData, UINT uCount);
		BOOL	HandleDuplicateSeqNum(AREF Addr, DWORD dData);
		BOOL	DoDuplicateWrite(AREF Addr, DWORD dData);
		BOOL	IsDuplicateError(void);

		// Special Command
		void	SendAckShutdown(void);

		// Receive Handling
		CCODE	Get65Data(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	GetAttributeData(AREF Addr, PDWORD pData, UINT uCount);
		void	SetADError(AREF Addr, UINT uPos);
		CCODE	GetRDBData(PDWORD pData, UINT uCount);
		DWORD	GetDataDWORD(UINT uPos);
		DWORD	GetDataDWORD(PBYTE pData);
		WORD	GetDataWORD(UINT uPos);

		// Helpers
		void	SetModbusData(AREF Addr);
		BOOL	IsFuncCode65Item(UINT uTable);
		BOOL	Is65Command(UINT uTable);
		BOOL	NoReadTransmit(AREF Addr, UINT *pCount);
		BOOL	IsAckShutdown(UINT uOffset);
		BOOL	IsValid65Access(UINT uItem);

		// Array Help
		UINT	GetTableNum(UINT uArrayItem);
		UINT	GetParamNum(UINT uArrayItem);
		UINT	GetDataSize(UINT uStart, UINT *pCount);
		void	ClearPackFG(void);

		// Array Loading
		void	AddArrays(LPCBYTE &pData);
	};

// End of File
