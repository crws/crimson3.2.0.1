//////////////////////////////////////////////////////////////////////////
//
// Omron FINS Constants
//

#define	FINS_RSV	0x00
#define FINS_GCT	0x02
#define FINS_ICF	0x80
#define FINS_MRC_PARAM	0x01
#define FINS_SRC_READ	0x01
#define FINS_SRC_WRITE	0x02
#define FINS_MRC_IP	0x27
#define FINS_SRC_PING	0x20
#define FINS_BROADCAST	0xFF

//////////////////////////////////////////////////////////////////////////
//
// Omron FINS Base Driver
//

class COmronFinsMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		COmronFinsMasterDriver(void);

		// Destructor
		~COmronFinsMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Data
		struct CBaseCtx
		{
			BYTE	m_bDna;
			BYTE	m_bDa1;
			BYTE	m_bDa2;
			BYTE	m_bSna;
			BYTE	m_bSa1;
			BYTE	m_bSa2;
			BYTE	m_bSid;
			BYTE	m_bMode;
				
			};

	protected:

		CBaseCtx * m_pBase;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;

		// Implementation

		void PutRead(AREF Addr, UINT uCount);
		void PutWrite(AREF Addr, PDWORD pData, UINT uCount);

		virtual void PutFinsHeader(void);
		void PutFinsCommand(AREF Addr, UINT uCount, BYTE bMrc, BYTE bSrc);
		void PutFinsCommand(BYTE bMrc, BYTE bSrc);
		void PutFinsData(PDWORD pData, UINT uCount, UINT uType);
		void PutWordWrite(PDWORD pData, UINT uCount);
		void PutLongWrite(PDWORD pdata, UINT uCount);
		
		void GetFinsData(PDWORD pData, UINT uCount, UINT uType);
		void GetWordRead(PDWORD pData, UINT uCount);
		void GetLongRead(PDWORD pData, UINT uCount);
		
		BYTE GetVariableType(UINT uTable);
		DWORD GetVariableOffset(UINT uTable, UINT uOffset);
		BOOL IsBroadcast(void);
		void SwapWords(DWORD &x);

	
		// Frame Building
		virtual BOOL Start(void);
		void AddByte(BYTE bByte);
		void AddWord(WORD wWord);
		void AddLong(DWORD dwWord);
		void AddAddress(DWORD dwAddr);
		
		// Transport Layer
		virtual BOOL Transact(BYTE bMRes, BYTE bSRes, UINT uCount, UINT uType);
	
	};

// End of File
