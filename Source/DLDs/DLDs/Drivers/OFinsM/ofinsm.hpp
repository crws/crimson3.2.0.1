#include "ofins.hpp"
//////////////////////////////////////////////////////////////////////////
//
// Omron FINS Serial Constants
//

#define HEADER_LONG	35
#define HEADER_SHORT	27

//////////////////////////////////////////////////////////////////////////
//
// Omron FINS Master Serial Driver
//

class COmronFinsMasterSerialDriver : public COmronFinsMasterDriver
{
	public:
		// Constructor
		COmronFinsMasterSerialDriver(void);

		// Destructor
		~COmronFinsMasterSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
	protected:

		// Device Context
		struct CContext : COmronFinsMasterDriver::CBaseCtx
		{
		
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bCheck;

		// Hex Lookup
		LPCTXT m_pHex;
		
		// Transport

		BOOL Transact(BYTE bMRes, BYTE bSRes, UINT uCount, UINT uType);
		BOOL Send(void);
		BOOL RecvFrame(UINT uTotal);
		BOOL CheckFrame(BYTE bMRes, BYTE bSRes);
		
		
		// Frame Building
		BOOL Start(void); 
		void PutHeader(void);
		void PutFinsHeader(void);

			
		
		// Conversion
		
		void FrameToAscii(void);
		void FrameFromAscii(void);
		BYTE ByteFromAscii(BYTE bByte);
	};

// End of File
