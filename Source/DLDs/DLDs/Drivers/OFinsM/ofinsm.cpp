
#include "intern.hpp"

#include "ofinsm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omron FINS Protocol Master Serial Driver
//

// Instantiator

INSTANTIATE(COmronFinsMasterSerialDriver);

// Constructor

COmronFinsMasterSerialDriver::COmronFinsMasterSerialDriver(void)
{
	m_Ident         = DRIVER_ID; 

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	}

// Destructor

COmronFinsMasterSerialDriver::~COmronFinsMasterSerialDriver(void)
{
	}

// Configuration

void MCALL COmronFinsMasterSerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL COmronFinsMasterSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL COmronFinsMasterSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL COmronFinsMasterSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL COmronFinsMasterSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_bDna   = GetByte(pData);
			m_pCtx->m_bDa1	 = GetByte(pData);
			m_pCtx->m_bDa2	 = GetByte(pData);
			m_pCtx->m_bSna	 = GetByte(pData);
			m_pCtx->m_bSa1   = GetByte(pData);
			m_pCtx->m_bSa2	 = GetByte(pData);
			m_pCtx->m_bSid   = 0;
			m_pCtx->m_bMode	 = GetByte(pData);
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL COmronFinsMasterSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Implementation

BOOL COmronFinsMasterSerialDriver::Start(void) {
  
	if( COmronFinsMasterDriver::Start() ) {

		PutHeader();

		return TRUE;
		}
	
	return FALSE;
	}

BOOL COmronFinsMasterSerialDriver::Transact(BYTE bMRes, BYTE bSRes, UINT uCount, UINT uType) {

	if( Send() ) {
		
		if( IsBroadcast() ) {

			while( m_pData->Read(0) < NOTHING );
			
			Sleep(10);
			
			return TRUE;
			}

		UINT uMult = UINT(uType == addrWordAsWord ? 4 : 8);
		
		if( RecvFrame(uCount * uMult) ) {

			return CheckFrame(bMRes, bSRes);
			}
		}

	return FALSE;
       	}

BOOL COmronFinsMasterSerialDriver::Send(void)
{
	FrameToAscii();

	m_pData->ClearRx();
	
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL COmronFinsMasterSerialDriver::RecvFrame(UINT uTotal)
{
	UINT uTimer = 0;

	UINT uData = 0;

	UINT uCheck = 0;

	m_uPtr = 0;

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( uData != '@' && !m_uPtr ) {

			continue;
			}

		m_bRxBuff[m_uPtr++] = uData;

		if( m_uPtr <= uTotal + HEADER_LONG ) {
		
			uCheck ^= uData;

			}

		if( uData == CR ) {

			if( m_pHex[(uCheck & 0xFF) / 16] == m_bRxBuff[uTotal + HEADER_LONG] ) {

				if( m_pHex[uCheck % 16] == m_bRxBuff[uTotal + HEADER_LONG + 1] ) {
					
					return TRUE;
					}
				}
			}
		}
	
	return FALSE;
	}

BOOL COmronFinsMasterSerialDriver::CheckFrame(BYTE bMRes, BYTE bSRes)
{		
	if( (m_bTxBuff[HEADER_SHORT - 3] == m_bRxBuff[HEADER_SHORT - 2]) &&
	    (m_bTxBuff[HEADER_SHORT - 2] == m_bRxBuff[HEADER_SHORT - 1]) ) {

		if( ByteFromAscii(m_bRxBuff[1]) == m_pBase->m_bDa2 / 16 ) {

			if( ByteFromAscii(m_bRxBuff[2]) == m_pBase->m_bDa2 % 16 ) {

				FrameFromAscii();
	
				if( (m_bRxBuff[0] == bMRes) && (m_bRxBuff[1] == bSRes) ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;

	}

void COmronFinsMasterSerialDriver::PutHeader(void)
{
	AddByte('@');
	
	AddByte(m_pCtx->m_bDa2);

	AddByte(0xFA);

	AddByte(0x30);

	}

void COmronFinsMasterSerialDriver::PutFinsHeader(void)
{
	AddByte(FINS_ICF | ( IsBroadcast() ? 1 : 0 ));

	AddByte(FINS_RSV);

	AddByte(FINS_GCT);

	AddByte(m_pBase->m_bDna);

	AddByte(m_pBase->m_bDa1);

	AddByte(0x00);

	AddByte(0x00);

	AddByte(0x00);

	AddByte(m_pBase->m_bSa2);

	AddByte(m_pBase->m_bSid);

	m_pBase->m_bSid = m_pBase->m_bSid == 0xFF ? 0 : m_pBase->m_bSid + 1;
       
	}

void COmronFinsMasterSerialDriver::FrameToAscii(void)
{
	PBYTE pTemp = PBYTE(alloca(300));

	UINT n = 0;

	m_bCheck = 0;

	for( UINT u = 0; u < m_uPtr; u++, n++ ) {

		if( !u || (n == 5) ) {

			pTemp[n] = m_bTxBuff[u];

			m_bCheck ^= pTemp[n];

			continue;
			}

		pTemp[n] = m_pHex[m_bTxBuff[u] / 16];

		m_bCheck ^= pTemp[n];

		n++;

		pTemp[n] = m_pHex[m_bTxBuff[u] % 16];

		m_bCheck ^= pTemp[n];
		}

	pTemp[n++] = m_pHex[m_bCheck / 16];

	pTemp[n++] = m_pHex[m_bCheck % 16];

	pTemp[n++] = '*';

	pTemp[n++] = CR;

	m_uPtr = n;

	memcpy(m_bTxBuff, pTemp, m_uPtr);
	
	}

void COmronFinsMasterSerialDriver::FrameFromAscii(void)
{
	if( m_uPtr - 4 <= HEADER_SHORT ) {

		return;
		}
	
	PBYTE pTemp = PBYTE(alloca(300));

	UINT n = 0;

	for( UINT u = HEADER_SHORT; u < m_uPtr - 4; u++ ) {

		pTemp[n] = ByteFromAscii(m_bRxBuff[u]) << 4;

		u++;
		
		pTemp[n] |= ByteFromAscii(m_bRxBuff[u]);

		n++;

		}

	memcpy(m_bRxBuff, pTemp, n);
	}

BYTE COmronFinsMasterSerialDriver::ByteFromAscii(BYTE bByte)
{
	if( bByte >= '0' && bByte <= '9' )

		return bByte - '0';

	return	bByte - '@' + 9;
	}

// End of File
