
#include "intern.hpp"

#include "s7base2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// S7 PPI Driver Revision 2 Base
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CS7BaseDriver::CS7BaseDriver(void)
{
	m_bSrcAddr = 0;	
	
	m_bLastFC  = 0x6C;

	m_wLastPDU = 0;

	m_fDebug   = FALSE;

	m_pBase	   = NULL;
	}

// Destructor

CS7BaseDriver::~CS7BaseDriver(void)
{
	}

// Entry Points

CCODE MCALL CS7BaseDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = TABLE_VX;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrByteAsByte;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);  
	}

CCODE MCALL CS7BaseDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable = GetTable(Addr.a.m_Table);

	UINT uAddr  = Addr.a.m_Offset;
	
	UINT uType  = Addr.a.m_Type;

	if( FakeRead(uTable) ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	if( m_fDebug ) {
		
		AfxTrace("\n");
		}

	if( uType == addrBitAsBit ) {

		if( FALSE && uAddr % 8 ) {

			// NOTE: Not needed now that DoReadMultipleBits
			// can handle data on a non-aligned address.

			return DoReadSingleBits(Addr, pData, uCount);
			}

		return DoReadMultipleBits(Addr, pData, uCount);
		}

	return DoReadStandard(Addr, pData, uCount);
	}

CCODE MCALL CS7BaseDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable = GetTable(Addr.a.m_Table);

	UINT uType  = Addr.a.m_Type;

	if( EatWrite(uTable) ) {

		return uCount;
		}

	if( m_fDebug ) {
		
		AfxTrace("\n");
		}

	if( uType == addrBitAsBit ) {

		return DoWriteSingleBit(Addr, pData, uCount);
		}

	return DoWriteStandard(Addr, pData, uCount);
	}

// Read Helpers

CCODE CS7BaseDriver::DoReadSingleBits(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Connect() ) {
	
		UINT uTable = GetTable(Addr.a.m_Table);

		UINT uAddr  = Addr.a.m_Offset;
	
		UINT uType  = Addr.a.m_Type;

		UINT uExtra = Addr.a.m_Extra;
			
		UINT uLimit = 8 - uAddr % 8;

		MakeMin(uCount, uLimit);

		InitFrame();

		AddParams(0x04, uType, uTable, uAddr, uExtra, uCount, Addr);

		TermFrame();

		if( Transact() ) {

			BYTE bCount = m_bRxBuff[NUM_VARS];

			if( bCount == uCount ) {

				PBYTE pScan = m_bRxBuff + MSG_RESULT;

				for( UINT i = 0; i < uCount; i++ ) {

					if( pScan[0] == 0xFF ) {

						BYTE bSize = pScan[3];

						if( bSize == 1 ) {

							pScan += 4;

							pData[i] = *pScan ? 1 : 0;

							pScan += 2;
							}

						continue;
						}

					return CCODE_ERROR;
					}

				return uCount;
				}
			}
		}

	Disconnect();

	return CCODE_ERROR;
	}

CCODE CS7BaseDriver::DoReadMultipleBits(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Connect() ) {
		
		UINT uTable = GetTable(Addr.a.m_Table) - 10;

		UINT uAddr  = Addr.a.m_Offset / 8;

		UINT uInit  = Addr.a.m_Offset % 8;
	
		UINT uType  = addrByteAsByte;

		UINT uExtra = Addr.a.m_Extra;

		MakeMin(uCount, GetMaxBits());

		UINT uBytes = (uInit + uCount + 7) / 8;

		InitFrame();
		
		AddParams(0x04, uType, uTable, uAddr, uExtra, uBytes, Addr);

		TermFrame();

		if( Transact() ) {

			BYTE bCount = m_bRxBuff[NUM_VARS];

			if( bCount == 1 ) {

				PBYTE pScan = m_bRxBuff + MSG_RESULT;

				if( pScan[0] == 0xFF ) {

					UINT uSize = MotorToHost(PWORD(pScan+2)[0]);

					if( uSize == 8 * uBytes ) {

						pScan += 4;

						BYTE bMask = 0x01 << uInit;

						for( UINT i = 0; i < uCount; i++ ) {

							pData[i] = (*pScan & bMask) ? 1 : 0;

							if( !(bMask<<=1) ) {

								pScan += 1;

								bMask  = 0x01;
								}
							}

						return uCount;
						}
					}
				}
			}
		}
	
	return CCODE_ERROR;
	}

CCODE CS7BaseDriver::DoReadStandard(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Connect() ) {
	
		UINT uTable = GetTable(Addr.a.m_Table);

		UINT uAddr  = Addr.a.m_Offset;
	
		UINT uType  = Addr.a.m_Type;

		UINT uExtra = Addr.a.m_Extra;

		UINT uLimit = GetMaxBits() / FindBits(uType, uTable);

		MakeMin(uCount, uLimit);

		InitFrame();

		AddParams(0x04, uType, uTable, uAddr, uExtra, uCount, Addr);

		TermFrame();

		if( Transact() ) {

			BYTE bCount = m_bRxBuff[NUM_VARS];

			if( bCount == 1 ) {

				PBYTE pScan = m_bRxBuff + MSG_RESULT;

				if( pScan[0] == 0xFF ) {

					UINT uSize = MotorToHost(PWORD(pScan+2)[0]);

					if( uSize == uCount * FindBits(uType, uTable) / 8 ) {

						pScan += 4;

						UINT uOffset = FindBits(uType, uTable) / 8 - 2;

						if( IsTimer(uTable) ) {

							for( UINT i = 0; i < uCount; i++ ) {

								WORD wData = PWORD(pScan + uOffset)[0];

								pData[i]   = MotorToHost(wData);

								pScan += FindBits(uType, uTable) / 8;
								}

							return uCount;
							}

					     	else if( IsCounter(uTable) ) {

							for( UINT i = 0; i < uCount; i++ ) {

								WORD wData = PWORD(pScan + uOffset)[0];

								pData[i]   = MotorToHost(wData);

								pScan += FindBits(uType, uTable) / 8;
								}

							return uCount;
							}
						}
					
					else if( uSize == uCount * FindBits(uType, uTable) ) {

						pScan += 4;

				 		CMotorDataPacker Pack(pData, uCount, FALSE);

						switch( uType ) {
						
							case addrByteAsByte:

								Pack.Unpack(PBYTE(pScan));

								break;

							case addrByteAsWord:

								Pack.Unpack(PWORD(pScan));

								break;

							case addrByteAsLong:
							case addrByteAsReal:

								Pack.Unpack(PDWORD(pScan));

								break;
							}

						return uCount;
						}
					}
				}
			}
		}

	Disconnect();
	
	return CCODE_ERROR;
	}

// Write Helpers

CCODE CS7BaseDriver::DoWriteSingleBit(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable = GetTable(Addr.a.m_Table);

	UINT uAddr  = Addr.a.m_Offset;
	
	UINT uType  = Addr.a.m_Type;

	UINT uExtra = Addr.a.m_Extra;

	MakeMin(uCount, 1);

	InitFrame();

	AddParams(0x05, uType, uTable, uAddr, uExtra, uCount, Addr);

	AddByte(0);

	AddByte(0x03);

	AddWord(uCount * FindBits(uType, uTable));

	for( UINT i = 0; i < uCount; i++ ) {

		AddByte(pData[i] ? 1 : 0);
		}

	TermFrame();

	if( Transact() ) {

		BYTE bCount = m_bRxBuff[NUM_VARS];

		if( bCount == 1 ) {

			PBYTE pScan = m_bRxBuff + MSG_RESULT;

			if( pScan[0] == 0xFF ) {

				return uCount;
				}
			}
		}

	return CCODE_ERROR;
	}

CCODE CS7BaseDriver::DoWriteStandard(AREF Addr, PDWORD pData, UINT uCount)
{	
	if( Connect() ) {

		UINT uTable = GetTable(Addr.a.m_Table);

		UINT uType  = Addr.a.m_Type;

		UINT uAddr  = Addr.a.m_Offset;

		UINT uExtra = Addr.a.m_Extra;

		UINT uLimit = uType != addrWordAsWord ? GetMaxBits() / FindBits(uType, uTable) : 1;

		MakeMin(uCount, uLimit);

		InitFrame();

		AddParams(0x05, uType, uTable, uAddr, uExtra, uCount, Addr);

		AddByte(0);

		AddByte(IsTimer(uTable) || IsCounter(uTable) ? 0x09 : 0x04);

		UINT uBits = uCount * FindBits(uType, uTable);
		
		AddWord(IsTimer(uTable) || IsCounter(uTable) ? uBits / 8 : uBits);	   

		AddPadding(uTable);

		CMotorDataPacker Pack(pData, uCount, FALSE);

		switch( uType ) {

			case addrByteAsByte:

				Pack.Pack(PBYTE(m_bTxBuff + m_uPtr));

				m_uPtr += uCount * sizeof(BYTE);

				break;

			case addrByteAsWord:
			case addrWordAsWord:
		  
				Pack.Pack(PWORD(m_bTxBuff + m_uPtr));

				m_uPtr += uCount * sizeof(WORD);

				break;

			case addrByteAsLong:
			case addrByteAsReal: 

				Pack.Pack(PDWORD(m_bTxBuff + m_uPtr));

				m_uPtr += uCount * sizeof(DWORD);
			
				break;
			}
		  
		TermFrame();

		if( Transact() ) {

			BYTE bCount = m_bRxBuff[NUM_VARS];

			if( bCount == 1 ) {

				PBYTE pScan = m_bRxBuff + MSG_RESULT;

				if( pScan[0] == 0xFF ) {

					return uCount;
					}
				}
			}

		}

	Disconnect();

	return CCODE_ERROR;
	}

// Implementation

BOOL CS7BaseDriver::FakeRead(UINT uTable)
{
	return FALSE;
	}

BOOL CS7BaseDriver::EatWrite(UINT uTable)
{
	switch( uTable ) {

		case TABLE_IX:
		case TABLE_AI:
		
			return TRUE;
		}

	return FALSE;
	}

void CS7BaseDriver::InitFrame(void)
{
	m_uPtr = 0;

	AddPPIHeader();

	AddPDUHeader();
	}

void CS7BaseDriver::TermFrame(void)
{
	SetPPIHeader();

	SetPDUHeader();

	AddByte(CalcCheck());	// FCS

	AddByte(ED);		// ED
	}

void CS7BaseDriver::AddByte(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;
	}
	
void CS7BaseDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));
	
	AddByte(LOBYTE(wData));
	}

void CS7BaseDriver::AddLong(LONG lData)
{
	AddWord(HIWORD(lData));
	
	AddWord(LOWORD(lData));
	}

BYTE CS7BaseDriver::GetNextFC(void)
{
	switch( m_bLastFC ) {

		case 0x5C:
			m_bLastFC = 0x7C;
			break;
			
		case 0x7C:
			m_bLastFC = 0x5C;
			break;
		
		default:
			m_bLastFC = 0x5C;
			break;
		}

	return m_bLastFC;
	}

BYTE CS7BaseDriver::CalcCheck(void)
{
	BYTE bCheck = 0;

	for( UINT i = 4; i < m_uPtr; bCheck += m_bTxBuff[i++] );

	return bCheck;
	}

void CS7BaseDriver::AddPPIHeader(void)
{
	AddByte(SD2);			// SD2
	
	AddByte(0);    			// LE
	
	AddByte(0);			// LEr
	
	AddByte(SD2);			// SD2
	
	AddByte(m_pBase->m_bDrop);	// DA

	AddByte(m_bSrcAddr);		// SA

	AddByte(GetNextFC());		// FC  
	}

void CS7BaseDriver::AddPDUHeader(void)
{
	AddByte(0x32);			// PROT_ID
	
	AddByte(0x01);			// ROSCTR
	
	AddWord(0x0000);		// RED_ID

	AddWord(++m_wLastPDU);		// PDU_REF
	
	AddWord(0);			// PAR_LG

	AddWord(0);			// DAT_LG
	}

void CS7BaseDriver::SetPPIHeader(void)
{
	m_bTxBuff[1] = m_uPtr - 4;

	m_bTxBuff[2] = m_uPtr - 4;
	}

void CS7BaseDriver::SetPDUHeader(void)
{
	WORD wParams = m_bParamTail - m_bParamHead;

	WORD wData   = m_uPtr     - m_bParamTail;

	m_bTxBuff[m_bParamHead - 4] = HIWORD(wParams);
	
	m_bTxBuff[m_bParamHead - 3] = LOWORD(wParams);

	m_bTxBuff[m_bParamHead - 2] = HIWORD(wData);

	m_bTxBuff[m_bParamHead - 1] = LOWORD(wData);  
	}

void CS7BaseDriver::AddParams(BYTE bService, UINT uType, UINT uTable, UINT uAddr, UINT uExtra, UINT& uCount, AREF Addr)
{
	BYTE  bType  = FindType (uType, uTable);

	WORD  wBlock = FindBlock(uTable);

	BYTE  bArea  = FindArea (uTable);

	DWORD dwAddr = FindAddr (uType, uTable, uAddr);

	m_bParamHead = m_uPtr;

	UINT uBlocks = (uType == addrBitAsBit) ? uCount : 1;

	UINT uExtent = (uType == addrBitAsBit) ? 1 : uCount;

	AddByte(bService);		// SERVICE_ID

	AddByte(LOBYTE(uBlocks));	// NO_VAR

	for( UINT i = 0; i < uBlocks; i++ ) {

		AddByte(0x12);		// VAR_SPC
		AddByte(0x0A);		// VADDR_LG
		AddByte(0x10);		// SYNTAX_ID

		AddByte(bType);		// Type
		AddWord(uExtent);	// Elements
		AddWord(wBlock);	// Block
		AddByte(bArea);		// Area

		AddByte(LOBYTE(HIWORD(dwAddr)));
		AddByte(HIBYTE(LOWORD(dwAddr)));
		AddByte(LOBYTE(LOWORD(dwAddr)));

		dwAddr++;
		}

	m_bParamTail = m_uPtr;
	}

BOOL CS7BaseDriver::Transact(void)
{
	return FALSE;
	}

BOOL CS7BaseDriver::CheckReply(void)
{
	BYTE bHi = m_bRxBuff[PDU_REF + 0];

	BYTE bLo = m_bRxBuff[PDU_REF + 1];

	if( MAKEWORD(bLo, bHi) == m_wLastPDU ) {

		if( m_bRxBuff[ROC_STR] == 0x03 ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CS7BaseDriver::Poll(BOOL fFast)
{
	return FALSE;
	}

void CS7BaseDriver::SendPoll(void)
{
	
	}

UINT CS7BaseDriver::GetReply(void)
{
	return RX_NONE;
	}

BYTE CS7BaseDriver::FindType(UINT uType, UINT uTable)
{
	switch( uTable ) {

		case TABLE_T:		return TC_TIMER;
		case TABLE_C:		return TC_COUNTER;
		}

	switch( uType ) {

		case addrBitAsBit:	return TC_BOOL;

		case addrByteAsByte:	return TC_BYTE;
		case addrByteAsWord:	return TC_WORD;
		case addrByteAsLong:	return TC_DWORD;
		case addrByteAsReal:	return TC_DWORD;
		}

	return 0;
	}

WORD CS7BaseDriver::FindBlock(UINT uTable)
{
	switch( uTable ) {
		
		case TABLE_VX:		return 1;
		}

	return 0;
	}

BYTE CS7BaseDriver::FindArea(UINT uTable)
{
	switch( uTable ) {

		case TABLE_VX:		return AC_V;
		case TABLE_IX:		return AC_I;
		case TABLE_QX:		return AC_Q;
		case TABLE_MX:		return AC_M;
		case TABLE_AI:		return AC_AI;
		case TABLE_AQ:		return AC_AQ;
		case TABLE_T:		return AC_T;
		case TABLE_C:		return AC_C;
		case TABLE_I:		return AC_I;
		case TABLE_Q:		return AC_Q;
		case TABLE_M:		return AC_M;
		}

	return 0;
	}

DWORD CS7BaseDriver::FindAddr(UINT uType, UINT uTable, UINT uAddr)
{
	switch( uType ) {

		case addrWordAsWord:	
		case addrBitAsBit:	return uAddr;
		}

	return uAddr << 3;
	}

UINT CS7BaseDriver::FindBits(UINT uType, UINT uTable)
{
	switch( uTable ) {

		case TABLE_T:		return 8 * 5;
		case TABLE_C:		return 8 * 3;
		}

	switch( uType ) {

		case addrBitAsBit:	return 1;

		case addrByteAsByte:	return 8 * 1;
		case addrByteAsWord:	return 8 * 2;
		case addrByteAsLong:	return 8 * 4;
		case addrByteAsReal:	return 8 * 4;
		}

	return 0;
	}

void  CS7BaseDriver::AddPadding(UINT uTable)
{
	switch( uTable ) {

		case TABLE_T:
			AddWord(0x00);

		case TABLE_C:
			AddByte(0x00);
			break;
		}
	}

BOOL  CS7BaseDriver::Connect(void)
{
	return TRUE;
	}

BOOL  CS7BaseDriver::Disconnect(void)
{
	return TRUE;
	}

BOOL  CS7BaseDriver::IsTimer(UINT uTable)
{
	return uTable == TABLE_T;
	}

BOOL  CS7BaseDriver::IsCounter(UINT uTable)
{
	return uTable == TABLE_C;
	}

UINT  CS7BaseDriver::GetTable(UINT uTable)
{
	return uTable;
	}

UINT CS7BaseDriver::GetMaxBits(void)
{
	return 512;
	}
	

// End of File
