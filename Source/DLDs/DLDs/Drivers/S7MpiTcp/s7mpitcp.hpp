#include "s7isotcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 PLC TCP Master Driver
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

///////////////////////////////////////////////////////////////////////////
//
// S7 Constants
//

#define	SPACE_OUTPUT	0x01
#define	SPACE_INPUT	0x02
#define	SPACE_FLAG	0x03
#define	SPACE_TIMER	0x04
#define	SPACE_COUNTER	0x05
#define	SPACE_DATA	0x0A

#define ID_QB		0x82
#define ID_IB		0x81
#define ID_MB		0x83
#define ID_T		29
#define ID_C		28
#define ID_DB		0x84

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 MPI Macro's
//

#define DATA_BLOCK(a,b,c)	(a + (b & 0xF0) + ((c & 0xE000) >> 5))
#define INDEX(a)		(a & 0x1FFF)

//////////////////////////////////////////////////////////////////////////
//
// S7 MPI TCP Driver
//

class CS7TcpMasterDriver : public CS7IsoTcpMasterDriver
{
	public:
		// Constructor
		CS7TcpMasterDriver(void);

		// Destructor
		~CS7TcpMasterDriver(void);
	   
		// Entry Points
		DEFMETH(CCODE) Ping(void);
		
	protected:
		// Implementation
		void AddParams(BYTE bService, UINT uType, UINT uTable, UINT uAddr, UINT uExtra, UINT& uCount, AREF Addr);
		UINT FindBits(UINT uType, UINT uTable);
		void AddPadding(UINT uTable);
		BYTE FindType(UINT uType, UINT uTable);
		BYTE FindArea(UINT uTable);
		BOOL IsTimer(UINT uTable);
		BOOL IsCounter(UINT uTable);
		BOOL EatWrite(UINT uTable);
		UINT GetTable(UINT uTable);

		// Overridables
		UINT GetMaxBits(void);

	};
		

// End of File

