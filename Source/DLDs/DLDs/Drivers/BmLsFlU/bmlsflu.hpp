//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// BETA LaserMike LaserSpeed Final Length Mode Slave Driver
//

class CBMikeLSFinalLenSlaveUdpDriver : public CMasterDriver
{
	public:
		// Constructor
		CBMikeLSFinalLenSlaveUdpDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);

		// User Access
		DEFMETH(UINT ) DrvCtrl(UINT uFunc, PCTXT Value);
						
		// Entry Points
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

	protected:
		struct CMacAddr
		{
			DWORD  m_IP;
			WORD   m_Port;
			};

		BYTE	   m_bRx  [300];
		BYTE	   m_bTx  [300];
		UINT	   m_uPtr;
		BOOL	   m_fTF;
		ISocket  * m_pSock[2];
		CBuffer  * m_pTxBuff;
		CBuffer  * m_pRxBuff;
		PBYTE      m_pTxData;
		CMacAddr   m_RxMac;
		DWORD	   m_dwFL; 
		DWORD	   m_dwQF;

		// Implementation
		DWORD GetReal(void);
		DWORD GetDec(void);
		BYTE  FromAscii(BYTE bByte);
		BOOL  Begin(void);
		void  AddByte(BYTE bByte);
		BOOL EndRealTime(void);
		
		// Transport Layer
		virtual BOOL Send(void);
		virtual BOOL RecvFrame(void);

		// Transport Header
		void AddTransportHeader(void);
		BOOL StripTransportHeader(void);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
	};

// End of File
