
//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Invensys 350x Data Spaces
//
// The following are Modbus Holding Registers
#define	SP_ALSC	1	// Alarm Summary - Channel
#define	SP_ALSG	2	// Alarm Summary - Global
#define	SP_ALSS	3	// Alarm Summary - System
#define	SP_BCD	4	// BCDInput
#define	SP_CALA	5	// Channel Alarm
#define	SP_CHAS	6	// Channel/Alarm Ack and Status
#define	SP_CHMN	7	// Channel Main
#define	SP_CMSG	8	// Custom Message/Triggers
#define	SP_DCO	9	// DC Output
#define	SP_DIO	10	// Digital IO
#define	SP_GRCD	11	// Group Recording
#define	SP_GTRD	12	// Group Trend
#define	SP_HUM	13	// Humidity
#define	SP_INSG	14	// Instrument - General
#define	SP_INSS	15	// Instrument - Strings
#define	SP_LGC2	16	// Logic 2
#define	SP_LGC8	17	// Logic 8
#define	SP_LDG1	18	// Loop 1 Diagnostics/Main
#define	SP_LDG2	19	// Loop 2 Diagnostics/Main
#define	SP_LOP1	20	// Loop OP1
#define	SP_LOP2	21	// Loop OP2
#define	SP_LPD1	22	// Loop PID1
#define	SP_LPD2	23	// Loop PID2
#define	SP_LSET	24	// Loop Setup
#define	SP_LSP1	25	// Loop SP1
#define	SP_LSP2	26	// Loop SP2
#define	SP_LTUN	27	// Loop Tune
#define	SP_MATH	28	// Math2
#define	SP_MUX	29	// Mux8
#define	SP_NANO	30	// Nano access
#define	SP_NET	31	// Network Data
#define	SP_NETS	32	// Network Strings
#define	SP_OR	33	// OR Block access
#define	SP_STER	34	// Sterilizer
#define	SP_STRS	35	// String responses
#define	SP_TIME	36	// Timers
#define	SP_USRL	37	// User Linearize
#define	SP_USRV	38	// User Values
#define	SP_VAAS	39
#define	SP_VALA	40	// Virt Alarm
#define	SP_VMT	41	// Virt Main/Trend
#define	SP_ZIR	42	// Zirconia

#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	XSIZE	300

#define	ISAWORD	0
#define	ISALONG	1
#define	ISATIME	2

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Invensys TCP Driver
//

class CEINanoDacTCPDriver : public CMasterDriver
{
	public:
		// Constructor
		CEINanoDacTCPDriver(void);

		// Destructor
		~CEINanoDacTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		DEFMETH(WORD) GetMasterFlags(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			DWORD	 m_IP1;
			UINT	 m_uPort;
			BYTE	 m_bUnit;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL     m_fDirty;
			BOOL	 m_fAuxIP;

			UINT	uWriteErrCt;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE       m_bTx[XSIZE];
		BYTE       m_bRx[XSIZE];
		PBYTE	   m_pTx;
		PBYTE	   m_pRx;
		UINT	   m_uPtr;
		UINT	   m_uKeep;

		BOOL m_fCustomMsgTrig;

		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		
		// Transport Layer
		BOOL	Transact(BOOL fIgnore);
		
		// Read Handlers
		CCODE	HandleRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoRealRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	HandleWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoRealWrite(AREF Addr, PDWORD pData, UINT uCount);

		// NanoDac handling
		DWORD	GetModbusStart(CAddress Addr, UINT *pCount);
		DWORD	Get_ALSC(CAddress Addr, UINT *pCount);
		DWORD	Get_ALSG(CAddress Addr, UINT *pCount);
		DWORD	Get_ALSS(CAddress Addr, UINT *pCount);
		DWORD	Get_BCD(CAddress Addr, UINT *pCount);
		DWORD	Get_CALA(CAddress Addr, UINT *pCount);
		DWORD	Get_CHAS(CAddress Addr, UINT *pCount);
		DWORD	Get_CHMN(CAddress Addr, UINT *pCount);
		DWORD	Get_CMSG(CAddress Addr, UINT *pCount);
		DWORD	Get_DCO(CAddress Addr, UINT *pCount);
		DWORD	Get_DIO(CAddress Addr, UINT *pCount);
		DWORD	Get_GRCD(CAddress Addr, UINT *pCount);
		DWORD	Get_GTRD(CAddress Addr, UINT *pCount);
		DWORD	Get_HUM(CAddress Addr, UINT *pCount);
		DWORD	Get_INSG(CAddress Addr, UINT *pCount);
		DWORD	Get_INSS(CAddress Addr, UINT *pCount);
		DWORD	Get_LGC2(CAddress Addr, UINT *pCount);
		DWORD	Get_LGC8(CAddress Addr, UINT *pCount);
		DWORD	Get_LDG1(CAddress Addr, UINT *pCount);
		DWORD	Get_LDG2(CAddress Addr, UINT *pCount);
		DWORD	Get_LOP1(CAddress Addr, UINT *pCount);
		DWORD	Get_LOP2(CAddress Addr, UINT *pCount);
		DWORD	Get_LPD1(CAddress Addr, UINT *pCount);
		DWORD	Get_LPD2(CAddress Addr, UINT *pCount);
		DWORD	Get_LSET(CAddress Addr, UINT *pCount);
		DWORD	Get_LSP1(CAddress Addr, UINT *pCount);
		DWORD	Get_LSP2(CAddress Addr, UINT *pCount);
		DWORD	Get_LTUN(CAddress Addr, UINT *pCount);
		DWORD	Get_MATH(CAddress Addr, UINT *pCount);
		DWORD	Get_MUX(CAddress Addr, UINT *pCount);
		DWORD	Get_NANO(CAddress Addr, UINT *pCount);
		DWORD	Get_NET(CAddress Addr, UINT *pCount);
		DWORD	Get_NETS(CAddress Addr, UINT *pCount);
		DWORD	Get_OR(CAddress Addr, UINT *pCount);
		DWORD	Get_STER(CAddress Addr, UINT *pCount);
		DWORD	Get_STRS(CAddress Addr, UINT *pCount);
		DWORD	Get_TIME(CAddress Addr, UINT *pCount);
		DWORD	Get_USRL(CAddress Addr, UINT *pCount);
		DWORD	Get_USRV(CAddress Addr, UINT *pCount);
		DWORD	Get_VAAS(CAddress Addr, UINT *pCount);
		DWORD	Get_VALA(CAddress Addr, UINT *pCount);
		DWORD	Get_VMT(CAddress Addr, UINT *pCount);
		DWORD	Get_ZIR(CAddress Addr, UINT *pCount);

		UINT	AdjustCount(UINT *pCount, UINT uOffset, UINT uQty);

		DWORD	OneBlock(CAddress Addr, UINT uAddr);

		// Helpers
		BOOL	IsOctetEnd(char c);
		BOOL	IsDigit(char c);
		BOOL	IsByte(UINT uNum);
		BOOL	IsStringItem(UINT uTable, UINT uOffset);
		UINT	MakeIndirect(UINT uOffset);

		// Port Access
		BOOL	SendFrame(void);
		BOOL	RecvFrame(void);

		// Socket Management
		BOOL	CheckSocket(void);
		BOOL	OpenSocket(void);
		void	CloseSocket(BOOL fAbort);
	};

// End of File
