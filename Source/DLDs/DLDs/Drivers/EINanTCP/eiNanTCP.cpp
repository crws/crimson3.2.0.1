
#include "intern.hpp"

#include "einanTCP.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Invensys TCP Driver
//

// Instantiator

INSTANTIATE(CEINanoDacTCPDriver);

// Constructor

CEINanoDacTCPDriver::CEINanoDacTCPDriver(void)
{
	m_Ident	= DRIVER_ID;
	
	m_pTx	= m_bTx;

	m_pRx	= m_bRx;

	m_uKeep	= 0;
	}

// Destructor

CEINanoDacTCPDriver::~CEINanoDacTCPDriver(void)
{
	}

// Configuration

void MCALL CEINanoDacTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CEINanoDacTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CEINanoDacTCPDriver::Open(void)
{
	}

// Master Flags
WORD MCALL CEINanoDacTCPDriver::GetMasterFlags(void)
{
	return MF_NO_SPANNING;
	}

// Device

CCODE MCALL CEINanoDacTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP1		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->uWriteErrCt	= 0;
			m_pCtx->m_fDirty	= FALSE;
			m_pCtx->m_fAuxIP	= FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEINanoDacTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

UINT MCALL CEINanoDacTCPDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) {	// new IP

		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));

		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		if( uFunc == 1  ) {

			pCtx->m_IP1 = MotorToHost(dwValue);
			}

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		}

	if( uFunc == 2 ) {	// New Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort  = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 3 )  {	// Set Drop Number

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bUnit  = BYTE(uValue);

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 4 ) {	// Get Current IP Address

		return MotorToHost(pCtx->m_IP1);
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL CEINanoDacTCPDriver::Ping(void)
{
//**/	AfxTrace1("\r\nPing Ping Ping Ping Ping %d\r\n", m_pCtx->m_bUnit);

	if( OpenSocket() ) {

		if( m_pCtx->m_bUnit ) {

			DWORD    Data[1];

			CAddress Addr;

			Addr.a.m_Table  = SP_ALSC;
		
			Addr.a.m_Offset = 0;
		
			Addr.a.m_Type   = addrWordAsWord;

			Addr.a.m_Extra  = 0;

			return Read(Addr, Data, 1);
			}

		return CMasterDriver::Ping();
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEINanoDacTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_bUnit ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

//**/	AfxTrace3("\r\n\nREAD O=%d Type=%x Ct=%d ", Addr.a.m_Offset, Addr.a.m_Type, uCount);
//**/	Sleep(100); // Slow down for debug

	if( OpenSocket() ) {

		CAddress AddrUsed;

		AddrUsed.m_Ref = Addr.m_Ref;	// Set default Table, Offset, Type, Extra

		AddrUsed.m_Ref = GetModbusStart(Addr, &uCount);

		return HandleRead(AddrUsed, pData, uCount);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEINanoDacTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenSocket() ) {

		CAddress AddrUsed;

		AddrUsed.m_Ref = Addr.m_Ref;	// Set default Table, Offset, Type, Extra

		AddrUsed.m_Ref = GetModbusStart(Addr, &uCount);

		return HandleWrite(AddrUsed, pData, uCount);
		}

	return CCODE_ERROR;
	}

// Frame Building

void CEINanoDacTCPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;
	
	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);
	}

void CEINanoDacTCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < XSIZE ) {
	
		m_pTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CEINanoDacTCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CEINanoDacTCPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CEINanoDacTCPDriver::Transact(BOOL fIgnore)
{
	if( SendFrame() && RecvFrame() ) {
	
		if( fIgnore ) return TRUE;

		if( m_pRx[0] == m_pTx[6] ) {	// correct device

			if( m_pRx[1] & 0x80 ) {	// modbus error check

				return FALSE;
				}

			return TRUE;
			}
		}

	CloseSocket(TRUE);
		
	return FALSE;
	}

// Read Handlers
CCODE CEINanoDacTCPDriver::HandleRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nHandle Read %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	switch( Addr.a.m_Type ) {

		case WW: return DoWordRead( Addr, pData, uCount );
		case LL: return DoLongRead( Addr, pData, uCount );
		case RR: return DoRealRead( Addr, pData, uCount );
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE CEINanoDacTCPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nDoWordRead %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	BOOL fStr = IsStringItem(Addr.a.m_Table, Addr.a.m_Offset);

	m_pRx[1] = 0;

	StartFrame(0x03);

	AddWord((WORD)Addr.a.m_Offset);

	AddWord((WORD)fStr ? uCount * 2 : uCount);

	if( Transact(FALSE) ) {

		if( fStr ) {

			PBYTE pChar = m_pRx + 3;

			for( UINT n = 0; n < uCount; n++, pChar += 4 ) {

				pData[n] = (pChar[1] << 8) + pChar[3];
				}
			}

		else {

			for( UINT n = 0; n < uCount; n++ ) {

				DWORD x  = PU2(m_pRx + 3)[n];
			
				pData[n] = MotorToHost(x);
				}
			}

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CEINanoDacTCPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nDoLongRead %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	m_pRx[1]  = 0;

	StartFrame(0x03);

	AddWord( (WORD)MakeIndirect(Addr.a.m_Offset) );

	AddWord( uCount * 2 );
	
	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_pRx + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CEINanoDacTCPDriver::DoRealRead(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nDoRealRead %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	m_pRx[1] = 0;

	StartFrame(0x03);

	AddWord((WORD)MakeIndirect(Addr.a.m_Offset));
	
	AddWord((WORD)uCount * 2);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_pRx + 3)[n];
			
			pData[n] = MotorToHost(x);
			}

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CEINanoDacTCPDriver::HandleWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n++++++ Write +++++ T=%d O=%x D=%8.8lx ", Addr.a.m_Table, Addr.a.m_Offset, pData[0]);

	switch( Addr.a.m_Type ) {

		case WW: return DoWordWrite( Addr, pData, uCount );
		case LL: return DoLongWrite( Addr, pData, uCount );
		case RR: return DoRealWrite( Addr, pData, uCount );
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE CEINanoDacTCPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("\r\nWrite Word %u %u %u ", Addr.a.m_Table, Addr.a.m_Offset, uCount); AfxTrace("Type=%x Data=%8.8lx ", Addr.a.m_Type, pData[0]);

	BOOL fStr = IsStringItem(Addr.a.m_Table, Addr.a.m_Offset);

	StartFrame(16);

	if( fStr ) {

//**/		AfxTrace("\r\nIS STRING "); for( UINT k = 0; k < uCount; k++ ) AfxTrace1("{%8.8lx}", pData[k]);

		AddWord(Addr.a.m_Offset);
		}

	else {
		AddWord(MakeIndirect(Addr.a.m_Offset));
		}

	if( fStr ) {

		AddWord((uCount * 2) + 1);

		AddByte((uCount * 4) + 2);
		}

	else {
		AddWord(uCount * 1);

		AddByte(uCount * 2);
		}

	for( UINT n = 0; n < uCount; n++ ) {

		if( fStr ) {

			AddByte(0);

			AddByte(HIBYTE(LOWORD(pData[n])));

			AddByte(0);

			AddByte(LOBYTE(LOWORD(pData[n])));
			}

		else {
			AddWord(LOWORD(pData[n]));
			}
		}

	if( fStr ) {

		AddWord(0);
		}

	if( Transact(FALSE) ) {

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}

	return ++m_pCtx->uWriteErrCt < 3 ? CCODE_ERROR : 1;
	}

CCODE CEINanoDacTCPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("Write Long %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	StartFrame(16);
		
	AddWord((WORD)MakeIndirect(Addr.a.m_Offset));
		
	AddWord((WORD)uCount * 2);
		
	AddByte((BYTE)uCount * 4);

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = pData[n];

		AddLong(x);
		}

	if( Transact(FALSE) ) {

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}

	return ++m_pCtx->uWriteErrCt < 3 ? CCODE_ERROR : 1;
	}

CCODE CEINanoDacTCPDriver::DoRealWrite(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace("Write Real %u %u %u\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);
	
	StartFrame(16);
		
	AddWord((WORD)MakeIndirect(Addr.a.m_Offset));
		
	AddWord((WORD)uCount * 2);
		
	AddByte((BYTE)uCount * 4);

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD x = pData[n];

		AddLong(x);
		}

	if( Transact(FALSE) ) {

		m_pCtx->uWriteErrCt = 0;

		return uCount;
		}

	return ++m_pCtx->uWriteErrCt < 3 ? CCODE_ERROR : 1;
	}

// NanoDac Handling

DWORD CEINanoDacTCPDriver::GetModbusStart(CAddress Addr, UINT *pCount)
{
	UINT uRtn = 0;

	UINT uCnt = *pCount;

	switch( Addr.a.m_Table ) {

		case SP_ALSC: return Get_ALSC(Addr, pCount);
		case SP_ALSG: return Get_ALSG(Addr, pCount);
		case SP_ALSS: return Get_ALSS(Addr, pCount);
		case SP_BCD:  return Get_BCD(Addr, pCount);
		case SP_CALA: return Get_CALA(Addr, pCount);
		case SP_CHAS: return Get_CHAS(Addr, pCount);
		case SP_CHMN: return Get_CHMN(Addr, pCount);
		case SP_CMSG: return Get_CMSG(Addr, pCount);
		case SP_DCO:  return Get_DCO(Addr, pCount);
		case SP_DIO:  return Get_DIO(Addr, pCount);
		case SP_GRCD: return Get_GRCD(Addr, pCount);
		case SP_GTRD: return Get_GTRD(Addr, pCount);
		case SP_HUM:  return Get_HUM(Addr, pCount);
		case SP_INSG: return Get_INSG(Addr, pCount);
		case SP_INSS: return Get_INSS(Addr, pCount);
		case SP_LGC2: return Get_LGC2(Addr, pCount);
		case SP_LGC8: return Get_LGC8(Addr, pCount);
		case SP_LDG1: return Get_LDG1(Addr, pCount);
		case SP_LDG2: return Get_LDG2(Addr, pCount);
		case SP_LOP1: return Get_LOP1(Addr, pCount);
		case SP_LOP2: return Get_LOP2(Addr, pCount);
		case SP_LPD1: return Get_LPD1(Addr, pCount);
		case SP_LPD2: return Get_LPD2(Addr, pCount);
		case SP_LSET: return Get_LSET(Addr, pCount);
		case SP_LSP1: return Get_LSP1(Addr, pCount);
		case SP_LSP2: return Get_LSP2(Addr, pCount);
		case SP_LTUN: return Get_LTUN(Addr, pCount);
		case SP_MATH: return Get_MATH(Addr, pCount);
		case SP_MUX:  return Get_MUX(Addr, pCount);
		case SP_NANO: return Get_NANO(Addr, pCount);
		case SP_NET:  return Get_NET(Addr, pCount);
		case SP_NETS: return Get_NETS(Addr, pCount);
		case SP_OR:   return Get_OR(Addr, pCount);
		case SP_STER: return Get_STER(Addr, pCount);
		case SP_STRS: return Get_STRS(Addr, pCount);
		case SP_TIME: return Get_TIME(Addr, pCount);
		case SP_USRL: return Get_USRL(Addr, pCount);
		case SP_USRV: return Get_USRV(Addr, pCount);
		case SP_VAAS: return Get_VAAS(Addr, pCount);
		case SP_VALA: return Get_VALA(Addr, pCount);
		case SP_VMT:  return Get_VMT(Addr, pCount);
		}

	return Get_ZIR(Addr, pCount);
	}

DWORD CEINanoDacTCPDriver::Get_ALSC(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 4496);
	}

DWORD CEINanoDacTCPDriver::Get_ALSG(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 416);
	}

DWORD CEINanoDacTCPDriver::Get_ALSS(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 4624);
	}

DWORD CEINanoDacTCPDriver::Get_BCD(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 11976);
	}

DWORD CEINanoDacTCPDriver::Get_CALA(CAddress Addr, UINT *pCount)
{
	UINT uOffset	= Addr.a.m_Offset;

	UINT uBlock	= (uOffset / 34) * 128;

	UINT uParam	= uOffset % 34;

	UINT uAlm2	= 0;

	if( uParam >= 17 ) {

		uAlm2 = 32;

		uParam -= 17;
		}

	Addr.a.m_Offset	= 6208 + uAlm2 + uBlock + uParam;

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_CHAS(CAddress Addr, UINT *pCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if( uOffset < 8 ) {

		uOffset = 258 + (uOffset / 2) * 4 + (uOffset % 2);
		}

	else {
		uOffset += 424;	// 432 + uOffset - 8
		}

	Addr.a.m_Offset = uOffset;

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_CHMN(CAddress Addr, UINT *pCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if( uOffset < 8 ) {

		UINT uBlock = (uOffset / 2) * 4;

		uOffset = 256 + uBlock + (uOffset % 2);
		}

	else {
		uOffset -= 8;

		UINT uBlock = (uOffset / 35) * 128;

		uOffset = 6144 + uBlock + (uOffset % 35);
		}

	Addr.a.m_Offset = uOffset;

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_CMSG(CAddress Addr, UINT *pCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if( uOffset <  10 ) {

		AdjustCount(pCount, uOffset, 10);

		m_fCustomMsgTrig = TRUE;

		return OneBlock(Addr, 10480);
		}

	m_fCustomMsgTrig = FALSE;

	if( uOffset < 111 ) return OneBlock(Addr, 24064 -  10);
	if( uOffset < 212 ) return OneBlock(Addr, 24165 - 111);
	if( uOffset < 313 ) return OneBlock(Addr, 24266 - 212);
	if( uOffset < 414 ) return OneBlock(Addr, 24367 - 313);
	if( uOffset < 515 ) return OneBlock(Addr, 24468 - 414);
	if( uOffset < 616 ) return OneBlock(Addr, 24569 - 515);
	if( uOffset < 717 ) return OneBlock(Addr, 24670 - 616);
	if( uOffset < 818 ) return OneBlock(Addr, 24771 - 717);
	if( uOffset < 919 ) return OneBlock(Addr, 24872 - 818);

	return OneBlock(Addr, 24973 - 919);
	}

DWORD CEINanoDacTCPDriver::Get_DCO(CAddress Addr, UINT *pCount)
{
	Addr.a.m_Offset = 5536 + ((Addr.a.m_Offset / 11) * 16) + AdjustCount(pCount, Addr.a.m_Offset, 11);

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_DIO(CAddress Addr, UINT *pCount)
{
	Addr.a.m_Offset = 5376 + ((Addr.a.m_Offset / 11) * 16) + AdjustCount(pCount, Addr.a.m_Offset, 11);

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_GRCD(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 4128);
	}

DWORD CEINanoDacTCPDriver::Get_GTRD(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 4098);
	}

DWORD CEINanoDacTCPDriver::Get_HUM(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 11896);
	}

DWORD CEINanoDacTCPDriver::Get_INSG(CAddress Addr, UINT *pCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if( !uOffset ) {

		Addr.a.m_Offset = 199;

		return Addr.m_Ref;
		}

	return OneBlock(Addr, 4224);	// 4225 - 1 + uOffset = Addr.a.m_Offset
	}

DWORD CEINanoDacTCPDriver::Get_INSS(CAddress Addr, UINT *pCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if(        uOffset < 74 )	uOffset += 17408;
	else if(   uOffset < 95 )	uOffset += 17482 - 74;
	else if(  uOffset < 116 )	uOffset += 17503 - 95;
	else if(  uOffset < 122 )	uOffset += 17524 - 116;
	else if(  uOffset < 140 )	uOffset += 17530 - 122;
	else if(  uOffset < 280 )	uOffset += 21760 - 140;
	else if(  uOffset < 420 )	uOffset += 21888 - 280;
	else if(  uOffset < 560 )	uOffset += 22016 - 420;
	else if(  uOffset < 700 )	uOffset += 22144 - 560;
	else if(  uOffset < 840 )	uOffset += 22272 - 700;
	else if(  uOffset < 980 )	uOffset += 22400 - 840;
	else if( uOffset < 1120 )	uOffset += 22528 - 980;
	else if( uOffset < 1260 )	uOffset += 22656 - 1120;
	else if( uOffset < 1400 )	uOffset += 22784 - 1260;
	else if( uOffset < 1540 )	uOffset += 22912 - 1400;
	else if( uOffset < 1700 )	uOffset += 23040 - 1540;
	else if( uOffset < 1721 )	uOffset += 25344 - 1700;
	else if( uOffset < 1742 )	uOffset += 25365 - 1721;
	else if( uOffset < 1763 )	uOffset += 25386 - 1742;
	else if( uOffset < 1784 )	uOffset += 25407 - 1763;
	else if( uOffset < 1805 )	uOffset += 25428 - 1784;
	else if( uOffset < 1826 )	uOffset += 25449 - 1805;
	else if( uOffset < 1847 )	uOffset += 25470 - 1826;
	else if( uOffset < 1868 )	uOffset += 25491 - 1847;
	else if( uOffset < 1889 )	uOffset += 25512 - 1868;
	else if( uOffset < 2000 )	uOffset += 25533 - 1889;
	else if( uOffset < 2050 )	uOffset += 25555 - 2000;
	else if( uOffset < 2100 )	uOffset += 25605 - 2050;
	else				uOffset += 25655 - 2100;

	Addr.a.m_Offset = uOffset;

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_LGC2(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 12024);
	}

DWORD CEINanoDacTCPDriver::Get_LGC8(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 12108);
	}

DWORD CEINanoDacTCPDriver::Get_LDG1(CAddress Addr, UINT *pCount)
{
	if( Addr.a.m_Offset < 22 ) {

		AdjustCount(pCount, Addr.a.m_Offset, 22);

		Addr.a.m_Offset += 512;
		}

	else {
		return OneBlock(Addr, 5755);	// (uOffset - 22) + 5777;
		}

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_LDG2(CAddress Addr, UINT *pCount)
{
	if( Addr.a.m_Offset < 22 ) {

		AdjustCount(pCount, Addr.a.m_Offset, 22);

		Addr.a.m_Offset += 640;
		}

	else {
		return OneBlock(Addr, 6011);	// (uOffset - 22) + 6033
		}

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_LOP1(CAddress Addr, UINT *pCount)
{
	if( Addr.a.m_Offset < 2 ) {

		AdjustCount(pCount, Addr.a.m_Offset, 2);

		Addr.a.m_Offset += 523;
		}

	else {
		return OneBlock(Addr, 5739);	// 5741 - 2 + uOffset
		}

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_LOP2(CAddress Addr, UINT *pCount)
{
	if( Addr.a.m_Offset < 2 ) {

		AdjustCount(pCount, Addr.a.m_Offset, 2);

		Addr.a.m_Offset += 651;
		}

	else {
		return OneBlock(Addr, 5995);	// 5997 - 2 + uOffset
		}

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_LPD1(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 5685);
	}

DWORD CEINanoDacTCPDriver::Get_LPD2(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 5941);
	}

DWORD CEINanoDacTCPDriver::Get_LSET(CAddress Addr, UINT *pCount)
{
	if( Addr.a.m_Offset <  6 ) {

		AdjustCount(pCount, Addr.a.m_Offset, 6);

		Addr.a.m_Offset += 5632;
		}

	else if( Addr.a.m_Offset <  8 ) {

		AdjustCount(pCount, Addr.a.m_Offset - 6, 2);

		Addr.a.m_Offset += 5793;	// 5799 + uOffset - 6;
		}

	else if( Addr.a.m_Offset < 14 ) {

		AdjustCount(pCount, Addr.a.m_Offset - 8, 6);

		Addr.a.m_Offset += 5880;	// 5888 + uOffset - 8;
		}

	else {
		Addr.a.m_Offset += 6041;		// 6055 + uOffset - 14;
		}

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_LSP1(CAddress Addr, UINT *pCount)
{
	if( !Addr.a.m_Offset )  {

		*pCount = 1;

		Addr.a.m_Offset = 522;
		}

	else {
		return OneBlock(Addr, 5720);	// 5721 - 1 + uOffset
		}

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_LSP2(CAddress Addr, UINT *pCount)
{
	if( !Addr.a.m_Offset ) {

		*pCount = 1;

		Addr.a.m_Offset = 650;
		}

	else {
		return OneBlock(Addr, 5976);	// 5977 - 1 + uOffset
		}

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_LTUN(CAddress Addr, UINT *pCount)
{
	UINT uOffset = Addr.a.m_Offset;

	switch( uOffset ) {

		case  0:		// uParam = 519;
		case  1:		// uParam = 520;
		case  2:		// uParam = 521;
			
			uOffset = 519 + AdjustCount(pCount, uOffset, 3);
			break;

		case  3:		// uParam = 647;
		case  4:		// uParam = 648;
		case  5:		// uParam = 649;
			uOffset = 647 + AdjustCount(pCount, uOffset - 3, 3);
			break;

		case  6:		// uParam = 5680;
		case  7:		// uParam = 5681;
		case  8:		// uParam = 5682;
		case  9:		// uParam = 5683;
		case 10:		// uParam = 5684;
			uOffset = 5680 + AdjustCount(pCount, uOffset - 6, 5);
			break;

		default:
			uOffset = 5936 + AdjustCount(pCount, uOffset - 11, 5);	// 5936 - 5940
			break;
		}

	Addr.a.m_Offset = uOffset;

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_MATH(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 12198);
	}

DWORD CEINanoDacTCPDriver::Get_MUX(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 12134);
	}

DWORD CEINanoDacTCPDriver::Get_NANO(CAddress Addr, UINT *pCount)
{
	*pCount = 1;

	Addr.a.m_Offset = 11264;

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_NET(CAddress Addr, UINT *pCount)
{
	if( Addr.a.m_Offset < 31 ) {

		AdjustCount(pCount, Addr.a.m_Offset, 31);

		Addr.a.m_Offset += 4354;
		}

	else {
		return OneBlock(Addr, 4385);	// 4416 - 31 + uOffset
		}

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_NETS(CAddress Addr, UINT *pCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if(      uOffset <  100 )	uOffset += 4435;
	else if( uOffset <  118 )	uOffset += 17664 - 100;
	else if( uOffset <  136 )	uOffset += 17682 - 118;
	else if( uOffset <  172 )	uOffset += 17700 - 136;
	else if( uOffset <  220 )	uOffset += 17736 - 172;
	else if( uOffset <  330 )	uOffset += 17775 - 220;
	else if( uOffset <  348 )	uOffset += 17876 - 330;
	else if( uOffset <  500 )	uOffset += 17894 - 348;
	else if( uOffset <  518 )	uOffset += 17976 - 500;
	else if( uOffset <  600 )	uOffset += 17994 - 518;
	else if( uOffset <  618 )	uOffset += 18076 - 600;
	else if( uOffset <  700 )	uOffset += 18094 - 618;
	else if( uOffset <  721 )	uOffset += 18176 - 700;
	else if( uOffset < 1000 )	uOffset += 18197 - 721;
	else if( uOffset < 1095 )	uOffset += 25705 - 1000;
	else if( uOffset < 1185 )	uOffset += 25795 - 1095;
	else				uOffset += 25885 - 1185;

	Addr.a.m_Offset = uOffset;

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_OR(CAddress Addr, UINT *pCount)
{
	Addr.a.m_Offset = 11520 + (( Addr.a.m_Offset / 9) * 16) + AdjustCount(pCount, Addr.a.m_Offset, 9);

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_STER(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 11776);
	}

DWORD CEINanoDacTCPDriver::Get_STRS(CAddress Addr, UINT *pCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if(      uOffset < 21   )	Addr.a.m_Offset += 18688;
	else if( uOffset < 27   )	Addr.a.m_Offset += 18709 - 21;
	else if( uOffset < 48   )	Addr.a.m_Offset += 18715 - 27;
	else if( uOffset < 54   )	Addr.a.m_Offset += 18736 - 48;
	else if( uOffset < 75   )	Addr.a.m_Offset += 18742 - 54;
	else if( uOffset < 81   )	Addr.a.m_Offset += 18763 - 75;
	else if( uOffset < 102  )	Addr.a.m_Offset += 18769 - 81;
	else if( uOffset < 130  )	Addr.a.m_Offset += 18790 - 102;
	else if( uOffset < 160  )	Addr.a.m_Offset += 19200 - 130;
	else if( uOffset < 190  )	Addr.a.m_Offset += 19221 - 160;
	else if( uOffset < 220  )	Addr.a.m_Offset += 19227 - 190;
	else if( uOffset < 250  )	Addr.a.m_Offset += 19248 - 220;
	else if( uOffset < 280  )	Addr.a.m_Offset += 19254 - 250;
	else if( uOffset < 310  )	Addr.a.m_Offset += 19275 - 280;
	else if( uOffset < 340  )	Addr.a.m_Offset += 19281 - 310;
	else if( uOffset < 370  )	Addr.a.m_Offset += 19302 - 340;
	else if( uOffset < 400  )	Addr.a.m_Offset += 19308 - 370;
	else if( uOffset < 430  )	Addr.a.m_Offset += 19329 - 400;
	else if( uOffset < 460  )	Addr.a.m_Offset += 19335 - 430;
	else if( uOffset < 490  )	Addr.a.m_Offset += 19356 - 460;
	else if( uOffset < 520  )	Addr.a.m_Offset += 19362 - 490;
	else if( uOffset < 550  )	Addr.a.m_Offset += 19383 - 520;
	else if( uOffset < 580  )	Addr.a.m_Offset += 19389 - 550;
	else if( uOffset < 610  )	Addr.a.m_Offset += 19410 - 580;
	else if( uOffset < 640  )	Addr.a.m_Offset += 19416 - 610;
	else if( uOffset < 670  )	Addr.a.m_Offset += 19437 - 640;
	else if( uOffset < 700  )	Addr.a.m_Offset += 19443 - 670;
	else if( uOffset < 730  )	Addr.a.m_Offset += 19464 - 700;
	else if( uOffset < 760  )	Addr.a.m_Offset += 19470 - 730;
	else if( uOffset < 790  )	Addr.a.m_Offset += 19491 - 760;
	else if( uOffset < 820  )	Addr.a.m_Offset += 19497 - 790;
	else if( uOffset < 850  )	Addr.a.m_Offset += 19518 - 820;
	else if( uOffset < 880  )	Addr.a.m_Offset += 19524 - 850;
	else if( uOffset < 910  )	Addr.a.m_Offset += 19545 - 880;
	else if( uOffset < 940  )	Addr.a.m_Offset += 19551 - 910;
	else if( uOffset < 1100 )	Addr.a.m_Offset += 19573 - 940;
	else if( uOffset < 1400 )	Addr.a.m_Offset += 23296 - 1100;
	else if( uOffset < 1424 )	Addr.a.m_Offset += 23808 - 1400;
	else if( uOffset < 1532 )	Addr.a.m_Offset += 23824 - 1424;
	else if( uOffset < 1538 )	Addr.a.m_Offset += 26948 - 1532;
	else if( uOffset < 1544 )	Addr.a.m_Offset += 26954 - 1538;
	else if( uOffset < 1550 )	Addr.a.m_Offset += 26960 - 1544;
	else if( uOffset < 1556 )	Addr.a.m_Offset += 26966 - 1550;
	else if( uOffset < 1562 )	Addr.a.m_Offset += 26972 - 1556;
	else if( uOffset < 1568 )	Addr.a.m_Offset += 26978 - 1562;
	else if( uOffset < 1574 )	Addr.a.m_Offset += 26984 - 1568;
	else if( uOffset < 1580 )	Addr.a.m_Offset += 26990 - 1574;
	else if( uOffset < 1586 )	Addr.a.m_Offset += 26996 - 1580;
	else if( uOffset < 1592 )	Addr.a.m_Offset += 27002 - 1586;
	else if( uOffset < 1598 )	Addr.a.m_Offset += 27008 - 1592;
	else if( uOffset < 1000 )	Addr.a.m_Offset += 27014 - 1598;
	else if( uOffset < 1450 )	Addr.a.m_Offset += 21504 - 1000;
	else if( uOffset < 1460 )	Addr.a.m_Offset += 26871 - 1450;
	else if( uOffset < 1466 )	Addr.a.m_Offset += 26876 - 1460;
	else if( uOffset < 1472 )	Addr.a.m_Offset += 26882 - 1466;
	else if( uOffset < 1478 )	Addr.a.m_Offset += 26888 - 1472;
	else if( uOffset < 1484 )	Addr.a.m_Offset += 26894 - 1478;
	else if( uOffset < 1490 )	Addr.a.m_Offset += 26900 - 1484;
	else if( uOffset < 1496 )	Addr.a.m_Offset += 26906 - 1490;
	else if( uOffset < 1502 )	Addr.a.m_Offset += 26912 - 1496;
	else if( uOffset < 1508 )	Addr.a.m_Offset += 26918 - 1502;
	else if( uOffset < 1514 )	Addr.a.m_Offset += 26924 - 1508;
	else if( uOffset < 1520 )	Addr.a.m_Offset += 26930 - 1514;
	else if( uOffset < 1526 )	Addr.a.m_Offset += 26936 - 1520;
	else				Addr.a.m_Offset += 26942 - 1526;

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_TIME(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 12000);
	}

DWORD CEINanoDacTCPDriver::Get_USRL(CAddress Addr, UINT *pCount)
{
	Addr.a.m_Offset = 10496 + ((Addr.a.m_Offset / 65) * 192) + AdjustCount(pCount, Addr.a.m_Offset, 65);

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_USRV(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 11916);
	}

DWORD CEINanoDacTCPDriver::Get_VAAS(CAddress Addr, UINT *pCount)
{
	if( Addr.a.m_Offset < 28 ) {

		Addr.a.m_Offset = 290 + ((Addr.a.m_Offset / 2) * 4) + AdjustCount(pCount, Addr.a.m_Offset, 2);
		}

	else {
		return OneBlock(Addr, 420);	// 448 + uOffset - 28
		}

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_VALA(CAddress Addr, UINT *pCount)
{
	UINT uOffset	= Addr.a.m_Offset;

	UINT uBlock	= (uOffset / 34) * 128;

	UINT uParam	= uOffset % 34;

	UINT uAlm2	= 0;

	if( uParam >= 17 ) {

		uAlm2	= 32;

		uParam	-= 17;
		}

	Addr.a.m_Offset = 7232 + uAlm2 + uBlock + uParam;

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_VMT(CAddress Addr, UINT *pCount)
{
	UINT uOffset = Addr.a.m_Offset;

	UINT uParam  = uOffset % 48;

	if( uOffset < 28 ) {

		uOffset = 288 + ((uOffset / 2) * 4) + AdjustCount(pCount, uOffset, 2);
		}

	else {
		UINT uNormal	= uOffset - 28;

		UINT uBlock	= (uNormal / 20) * 128;

		UINT uParam	= uNormal % 20;

		if( uParam < 15 ) {

			uOffset = 7168 + uBlock + uParam;
			}

		else {
			if( uParam == 15 ) {

				*pCount = 1;

				uOffset = 7185 + uBlock;
				}

			else {
				uOffset = 7200 + uBlock + uParam - 16;
				}
			}
		}

	Addr.a.m_Offset = uOffset;

	return Addr.m_Ref;
	}

DWORD CEINanoDacTCPDriver::Get_ZIR(CAddress Addr, UINT *pCount)
{
	return OneBlock(Addr, 10368);
	}

UINT CEINanoDacTCPDriver::AdjustCount(UINT *pCount, UINT uOffset, UINT uQty)
{
	UINT uParam = uOffset % uQty;

	if( uParam + *pCount > uQty ) {

		*pCount = uQty - uParam;
		}

	return	uParam;
	}

DWORD CEINanoDacTCPDriver::OneBlock(CAddress Addr, UINT uAddr)
{
	Addr.a.m_Offset += uAddr;

	return Addr.m_Ref;
	}

// Helpers

BOOL CEINanoDacTCPDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CEINanoDacTCPDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CEINanoDacTCPDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

BOOL CEINanoDacTCPDriver::IsStringItem(UINT uTable, UINT uOffset)
{
	switch( uTable ) {

		case SP_INSS:
		case SP_NETS:
		case SP_STRS:	return TRUE;

		case SP_CMSG:	return !m_fCustomMsgTrig;
		}

	return FALSE;
	}

UINT CEINanoDacTCPDriver::MakeIndirect(UINT uOffset)
{
	return 0x8000 | ( uOffset * 2);
	}

// Port Access
BOOL CEINanoDacTCPDriver::SendFrame(void)
{
	m_bTx[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

//**/		AfxTrace1("\r\nSend %d ", uSize); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEINanoDacTCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

//**/			AfxTrace1("--- Recv %d\r\n", uSize); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("<%2.2x>", m_bRx[k]);

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRx[5];

				if( uPtr >= uTotal ) {

					if( m_bRx[0] == m_bTx[0] ) {

						if( m_bRx[1] == m_bTx[1] ) {

							memcpy(m_bRx, m_bRx + 6, uPtr - 6);

							memset(m_bRx + uPtr - 6, 0, 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRx[n] = m_bRx[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

// Socket Management
BOOL CEINanoDacTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CEINanoDacTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		m_pCtx->m_fAuxIP = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   Port;

		IP   = (IPADDR const &) m_pCtx->m_IP1;

		Port = WORD(m_pCtx->m_uPort);

		if( m_pCtx->m_pSock->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		return FALSE;
		}

	return FALSE;
	}

void CEINanoDacTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
