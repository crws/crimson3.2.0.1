
#include "intern.hpp"

#include "catold.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link
//

// Instantiator

INSTANTIATE(CCatLinkDriver);

// Constructor

CCatLinkDriver::CCatLinkDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CCatLinkDriver::~CCatLinkDriver(void)
{
	}

// Configuration

void MCALL CCatLinkDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CCatLinkDriver::CheckConfig(CSerialConfig &Config)
{
	Config.m_uFlags    = flagFastRx | flagTimeout;

	Config.m_uPhysical = 2;
	}
	
// Management

void MCALL CCatLinkDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CCatLinkHandler(m_pHelper, this);

	pPort->Bind(m_pHandler);
	}

void MCALL CCatLinkDriver::Detach(void)
{
	m_pHandler->Stop();

	m_pHandler->Release();
	}

void MCALL CCatLinkDriver::Open(void)
{
	memset(m_fDrop, 0, sizeof(m_fDrop));

	memset(m_uPend, 0, sizeof(m_uPend));

	memset(m_Data,  0, sizeof(m_Data));

	m_uScan   = 0;

	m_uActive = 0;

	m_uHead   = 0;

	m_uTail   = 0;

	m_pHandler->Start();
	}

// Device

CCODE MCALL CCatLinkDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	return CCODE_SUCCESS;
	}

CCODE MCALL CCatLinkDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

void MCALL CCatLinkDriver::Service(void)
{
	UpdateItems();
	}

CCODE MCALL CCatLinkDriver::Ping(void)
{
	return CCODE_SUCCESS;
	}

CCODE MCALL CCatLinkDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CDataItem *pItem = NULL;

	UINT       uName = Addr.a.m_Offset;

	UINT       uType = Addr.a.m_Table;

	UINT       uFunc = 0;

	if( uType >= addrNamed ) {

		uType = (Addr.a.m_Table - addrNamed) % 3 + 1;

		uFunc = (Addr.a.m_Table - addrNamed) / 3 + 0;
		}

	switch( uType ) {

		case 1:	uName += 0x000000; break;
		case 2:	uName += 0xD00000; break;
		case 3:	uName += 0xD10000; break;
		}

	if( IsValidName(uName) ) {

		if( (pItem = GetItem(uName)) ) {

			if( uFunc == 0 ) {

				switch( pItem->m_uState ) {

					case stateIdle:	

						pItem->m_uState = stateMissing;

						SetTime(pItem, timeMissing);

						break;

					case stateFound:
					case statePolled:
					case statePresent:

						*pData = pItem->m_Data;

						return 1;
					}

				*pData = 0;

				return 1;
				}

			if( uFunc == 1 ) {

				switch( pItem->m_uState ) {

					case stateFound:
					case statePolled:
					case statePresent:

						*pData = MAKEWORD(pItem->m_bSrc, pItem->m_uState);

						return 1;
					}
				
				*pData = MAKEWORD(0, pItem->m_uState);

				return 1;
				}

			if( uFunc == 2 ) {

				*pData = !pItem->m_fSkip;

				return 1;
				}
			}
		}

	*pData = 0;

	return 1;
	}

CCODE MCALL CCatLinkDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CDataItem *pItem = NULL;

	UINT       uName = Addr.a.m_Offset;

	UINT       uType = (Addr.a.m_Table - addrNamed) % 3 + 1;

	UINT       uFunc = (Addr.a.m_Table - addrNamed) / 3 + 0;

	switch( uType ) {

		case 1:	uName += 0x000000; break;
		case 2:	uName += 0xD00000; break;
		case 3:	uName += 0xD10000; break;
		}

	if( IsValidName(uName) ) {

		if( (pItem = GetItem(uName)) ) {

			if( uFunc == 2 ) {

				pItem->m_fSkip = !*pData;

				return 1;
				}
			}
		}

	return 1;
	}

// State Machine

void CCatLinkDriver::UpdateItems(void)
{
	for( UINT n = 0; n < elements(m_Data); n++ ) {

		UINT uName = GetName(n);

		if( IsValidName(uName) ) {

			CDataItem *pItem = m_Data + n;

			if( pItem->m_uState ) {

				UpdateItem(uName, pItem);
				}
			}
		}
	}

void CCatLinkDriver::UpdateItem(UINT uName, CDataItem *pItem)
{
	switch( pItem->m_uState ) {

		case stateMissing:

			if( pItem->m_bTx ) {

				OnPresentData(pItem);

				break;
				}

			if( TimeOut(pItem) ) {

				if( !pItem->m_fSkip ) {

					if( !m_uActive ) {

						SetTime(pItem, timeRetry);
						}
					else {
						FindInitTarget(pItem);

						if( QueuePoll(pItem) ) {

							pItem->m_uState = stateFind;
							}
						else {
							SetTime(pItem, timeRetry);

							pItem->m_uState = stateTooBusy;
							}
						}

					break;
					}

				SetTime(pItem, timeMissing);

				break;
				}

			break;

		case stateTooBusy:

			if( TimeOut(pItem) ) {

				if( QueuePoll(pItem) ) {

					pItem->m_uState = stateFind;
					}
				else
					SetTime(pItem, timeRetry);

				break;
				}
			break;

		case stateDefer:

			if( TimeOut(pItem) ) {

				if( QueuePoll(pItem) ) {

					pItem->m_uState = stateFind;
					}
				else {
					SetTime(pItem, timeRetry);

					pItem->m_uState = stateTooBusy;
					}

				break;
				}
			break;

		case stateFind:

			if( pItem->m_bTx ) {

				OnPolledData(pItem);

				break;
				}

			if( TimeOut(pItem) ) {
					
				SubPoll(pItem);

				if( !pItem->m_fSkip ) {

					FindNextTarget(pItem);

					if( pItem->m_uPoll < 2 * m_uActive ) {

						if( QueuePoll(pItem) ) {

							pItem->m_uState = stateFind;
							}
						else {
							SetTime(pItem, timeRetry);

							pItem->m_uState = stateTooBusy;
							}
						}
					else {
						SetTime(pItem, timeDefer);

						pItem->m_uState = stateDefer;
						}

					break;
					}

				pItem->m_uState = stateIdle;

				break;
				}

			break;

		case stateFound:

			if( pItem->m_bTx ) {

				if( FALSE ) {

					// NOTE -- Enable this to allow us to switch back
					// to listen mode if we see an item while we're
					// waiting to poll it. This isn't too stable as the
					// data item might not be around for long.

					OnPresentData(pItem);

					break;
					}

				pItem->m_bTx = 0;
				}

			if( TimeOut(pItem) ) {

				if( !pItem->m_fSkip ) {

					if( QueuePoll(pItem) ) {

						pItem->m_uState = statePolled;
						}
					else
						SetTime(pItem, timeRetry);

					break;
					}

				pItem->m_uState = stateIdle;

				break;
				}

			break;

		case statePolled:

			if( pItem->m_bTx ) {

				OnPolledData(pItem);

				break;
				}

			if( TimeOut(pItem) ) {

				SubPoll(pItem);

				pItem->m_uState = stateMissing;

				SetTime(pItem, timeImmediate);

				break;
				}

			break;

		case statePresent:

			if( pItem->m_bTx ) {

				OnPresentData(pItem);

				break;
				}

			if( TimeOut(pItem) ) {

				pItem->m_uState = stateMissing;

				SetTime(pItem, timeImmediate);

				break;
				}

			break;
		}
	}

BOOL CCatLinkDriver::OnPresentData(CDataItem *pItem)
{
	if( !IsSelf(pItem->m_bRx) ) {

		pItem->m_bSrc   = pItem->m_bTx;

		pItem->m_uState = statePresent;

		SetTime(pItem, timePresent);

		return TRUE;
		}

	pItem->m_bTx = 0;

	return FALSE;
	}

BOOL CCatLinkDriver::OnPolledData(CDataItem *pItem)
{
	if( IsSelf(pItem->m_bRx) ) {

		if( pItem->m_bTx == pItem->m_bSrc ) {

			SubPoll(pItem);

			pItem->m_uState = stateFound;

			pItem->m_uPoll  = 0;

			SetTime(pItem, timePoll);

			return TRUE;
			}
		}

	pItem->m_bTx = 0;

	return FALSE;
	}

void CCatLinkDriver::SetTime(CDataItem *pItem, UINT uTime)
{
	pItem->m_bTx   = 0;

	pItem->m_bRx   = 0;

	pItem->m_uTime = GetTickCount() + ToTicks(uTime);
	}

BOOL CCatLinkDriver::TimeOut(CDataItem *pItem)
{
	return int(GetTickCount() - pItem->m_uTime) >= 0;
	}

void CCatLinkDriver::FindInitTarget(CDataItem *pItem)
{
	for(;;) {

		if( m_fDrop[pItem->m_bSrc] ) {

			if( pItem->m_uPoll < 120 ) {

				pItem->m_uPoll++;
				}

			break;
			}

		pItem->m_bSrc++;
		}
	}

void CCatLinkDriver::FindNextTarget(CDataItem *pItem)
{
	for(;;) {

		pItem->m_bSrc++;

		if( m_fDrop[pItem->m_bSrc] ) {

			if( pItem->m_uPoll < 120 ) {

				pItem->m_uPoll++;
				}

			break;
			}
		}
	}

void CCatLinkDriver::AddPoll(CDataItem *pItem)
{
	m_uPend[pItem->m_bSrc]++;
	}

void CCatLinkDriver::SubPoll(CDataItem *pItem)
{
	m_uPend[pItem->m_bSrc]--;
	}

BOOL CCatLinkDriver::QueuePoll(CDataItem *pItem)
{
	UINT uActive = m_uPend[pItem->m_bSrc];

	UINT uLimit  = pollLimit;

	if( uActive < uLimit ) {

		UINT uNext = (m_uTail + 1) % elements(m_pPoll);

		if( uNext != m_uHead ) {

			AddPoll(pItem);

			m_pPoll[m_uTail] = pItem;

			m_uTail          = uNext;

			return TRUE;
			}
		}

	return FALSE;
	}

// Handler Calls

BOOL CCatLinkDriver::SeeDrop(BYTE bDrop)
{
	if( bDrop >= 0x10 ) {

		if( !m_fDrop[bDrop] ) {

			m_fDrop[bDrop] = TRUE;

			m_uActive     += 1;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCatLinkDriver::SetData(BYTE bSrc, BYTE bDest, UINT uName, UINT uData)
{
	CDataItem *pItem = GetItem(uName);

	if( pItem ) {

		if( !pItem->m_bTx ) {

			if( SignExtendByte(uName) ) {

				typedef signed char SCHAR;

				uData = UINT(LONG(SHORT(SCHAR(uData))));
				}

			if( SignExtendWord(uName) ) {

				uData = UINT(LONG(SHORT(uData)));
				}

			pItem->m_Data = uData;

			pItem->m_bTx  = bSrc;

			pItem->m_bRx  = bDest;

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CCatLinkDriver::GetSend(PBYTE pData)
{
	if( m_uHead != m_uTail ) {

		CDataItem *pItem = m_pPoll[m_uHead];

		UINT       uName = GetName(pItem - m_Data);

		BYTE       bDrop = pItem->m_bSrc;

		UINT       uSize = 0;

		pData[0] = selfDrop;

		pData[1] = bDrop;

		if( !uSize && !(uName & 0xFFFFFF00) ) {

			pData[2] = 0x20;

			pData[3] = LOBYTE(LOWORD(uName));

			uSize    = 4;
			}

		if( !uSize && !(uName & 0xFFFF0000) ) {

			pData[2] = 0x60;

			pData[3] = HIBYTE(LOWORD(uName));

			pData[4] = LOBYTE(LOWORD(uName));

			uSize    = 5;
			}

		if( !uSize && !(uName & 0xFF000000) ) {

			pData[2] = 0xCF;

			pData[3] = 0x00;

			pData[4] = LOBYTE(HIWORD(uName));

			pData[5] = HIBYTE(LOWORD(uName));

			pData[6] = LOBYTE(LOWORD(uName));

			uSize    = 7;
			}

		if( uSize ) {

			BYTE bSum = 0;

			for( UINT n = 0; n < uSize; n++ ) {

				bSum += pData[n];
				}

			pData[n] = BYTE(256 - bSum);

			uSize    = uSize + 1;
			}

		m_uHead = (m_uHead + 1) % elements(m_pPoll);

		SetTime(pItem, timeReply);

		return uSize;
		}

	return 0;
	}

// Data Item Access

CCatLinkDriver::CDataItem * CCatLinkDriver::GetItem(UINT uName)
{
	if( uName >= 0x000000 && uName <= 0x00007F ) {

		return m_Data + 0x0000 + (uName - 0x000000);
		}

	if( uName >= 0xD00000 && uName <= 0xD00FFF ) {

		return m_Data + 0x0080 + (uName - 0xD00000);
		}
	
	if( uName >= 0xD10000 && uName <= 0xD10FFF ) {

		return m_Data + 0x1080 + (uName - 0xD10000);
		}

	if( uName >= 0x00F000 && uName <= 0x00FFFF ) {

		return m_Data + 0x2080 + (uName - 0x00F000);
		}

	return NULL;
	}

// Item Name Helpers

BOOL CCatLinkDriver::IsValidName(UINT uName)
{
	switch( uName ) {

		case 0x0001:
		case 0x0002:
		case 0x0003:
		case 0x0007:
		case 0x0008:
		case 0x000C:
		case 0x000D:
		case 0x0011:
		case 0x0015:
		case 0x0016:
		case 0x0040:
		case 0x0041:
		case 0x0042:
		case 0x0044:
		case 0x0046:
		case 0x0047:
		case 0x004B:
		case 0x004D:
		case 0x004E:
		case 0x0053:
		case 0x0054:
		case 0x0055:
		case 0x0058:
		case 0x005A:
		case 0x005B:
		case 0x005C:
		case 0x005D:
		case 0x005E:
		case 0x005F:
		case 0x0080:
		case 0x0082:
		case 0x0083:
		case 0x0084:
		case 0x0086:
		case 0x00C8:
		case 0xF013:
		case 0xF014:
		case 0xF016:
		case 0xF01B:
		case 0xF02A:
		case 0xF02C:
		case 0xF08F:
		case 0xF09C:
		case 0xF0A6:
		case 0xF0A8:
		case 0xF0A9:
		case 0xF0AA:
		case 0xF0AC:
		case 0xF0B0:
		case 0xF0B1:
		case 0xF0B2:
		case 0xF0B3:
		case 0xF0B4:
		case 0xF0B5:
		case 0xF0B6:
		case 0xF0C1:
		case 0xF0C2:
		case 0xF0E8:
		case 0xF0F2:
		case 0xF0FD:
		case 0xF108:
		case 0xF109:
		case 0xF10A:
		case 0xF10B:
		case 0xF10C:
		case 0xF10D:
		case 0xF111:
		case 0xF112:
		case 0xF113:
		case 0xF115:
		case 0xF116:
		case 0xF117:
		case 0xF118:
		case 0xF119:
		case 0xF11B:
		case 0xF11C:
		case 0xF11D:
		case 0xF121:
		case 0xF122:
		case 0xF123:
		case 0xF124:
		case 0xF125:
		case 0xF129:
		case 0xF14F:
		case 0xF189:
		case 0xF192:
		case 0xF1B3:
		case 0xF1B4:
		case 0xF1D0:
		case 0xF1D3:
		case 0xF1D4:
		case 0xF1D5:
		case 0xF1D6:
		case 0xF213:
		case 0xF24D:
		case 0xF24F:
		case 0xF28A:
		case 0xF2CB:
		case 0xF2CC:
		case 0xF2D6:
		case 0xF2D7:
		case 0xF40E:
		case 0xF410:
		case 0xF411:
		case 0xF412:
		case 0xF415:
		case 0xF417:
		case 0xF419:
		case 0xF41C:
		case 0xF41F:
		case 0xF420:
		case 0xF430:
		case 0xF431:
		case 0xF432:
		case 0xF433:
		case 0xF434:
		case 0xF435:
		case 0xF436:
		case 0xF437:
		case 0xF438:
		case 0xF439:
		case 0xF43A:
		case 0xF43B:
		case 0xF43C:
		case 0xF43D:
		case 0xF43E:
		case 0xF43F:
		case 0xF440:
		case 0xF441:
		case 0xF442:
		case 0xF443:
		case 0xF444:
		case 0xF445:
		case 0xF446:
		case 0xF447:
		case 0xF448:
		case 0xF449:
		case 0xF44A:
		case 0xF44B:
		case 0xF44C:
		case 0xF44D:
		case 0xF44E:
		case 0xF44F:
		case 0xF45B:
		case 0xF460:
		case 0xF461:
		case 0xF462:
		case 0xF463:
		case 0xF464:
		case 0xF465:
		case 0xF466:
		case 0xF467:
		case 0xF468:
		case 0xF469:
		case 0xF46A:
		case 0xF46B:
		case 0xF46C:
		case 0xF46D:
		case 0xF48D:
		case 0xF48F:
		case 0xF4A0:
		case 0xF4A2:
		case 0xF4C3:
		case 0xF4C4:
		case 0xF4C7:
		case 0xF4C8:
		case 0xF4C9:
		case 0xF4CA:
		case 0xF4CB:
		case 0xF4CF:
		case 0xF4D0:
		case 0xF4D1:
		case 0xF4D2:
		case 0xF4EA:
		case 0xF508:
		case 0xF509:
		case 0xF50A:
		case 0xF50B:
		case 0xF50C:
		case 0xF50D:
		case 0xF50E:
		case 0xF50F:
		case 0xF510:
		case 0xF511:
		case 0xF512:
		case 0xF513:
		case 0xF515:
		case 0xF516:
		case 0xF517:
		case 0xF518:
		case 0xF519:
		case 0xF51A:
		case 0xF51B:
		case 0xF51C:
		case 0xF51D:
		case 0xF51E:
		case 0xF51F:
		case 0xF520:
		case 0xF524:
		case 0xF525:
		case 0xF53E:
		case 0xF557:
		case 0xF55A:
		case 0xF55B:
		case 0xF55C:
		case 0xF55D:
		case 0xF55E:
		case 0xF55F:
		case 0xF560:
		case 0xF561:
		case 0xF562:
		case 0xF563:
		case 0xF564:
		case 0xF565:
		case 0xF566:
		case 0xF567:
		case 0xF568:
		case 0xF569:
		case 0xF56A:
		case 0xF56B:
		case 0xF56C:
		case 0xF56D:
		case 0xF56E:
		case 0xF56F:
		case 0xF570:
		case 0xF571:
		case 0xF572:
		case 0xF573:
		case 0xF574:
		case 0xF575:
		case 0xF57B:
		case 0xF57C:
		case 0xF57E:
		case 0xF57F:
		case 0xF58E:
		case 0xF593:
		case 0xF594:
		case 0xF595:
		case 0xF596:
		case 0xF597:
		case 0xF598:
		case 0xF599:
		case 0xF59A:
		case 0xF59B:
		case 0xF59C:
		case 0xF59D:
		case 0xF59E:
		case 0xF59F:
		case 0xF5A0:
		case 0xF5A1:
		case 0xF5A2:
		case 0xF5A3:
		case 0xF5A4:
		case 0xF5A5:
		case 0xF5A6:
		case 0xF5A7:
		case 0xF5A8:
		case 0xF5A9:
		case 0xF5AA:
		case 0xF5AB:
		case 0xF5B1:
		case 0xF5BA:
		case 0xF5C9:
		case 0xF5E0:
		case 0xF61E:
		case 0xF62B:
		case 0xF701:
		case 0xF702:
		case 0xF703:
		case 0xF704:
		case 0xF705:
		case 0xF706:
		case 0xF707:
		case 0xF708:
		case 0xF709:
		case 0xF70A:
		case 0xF70B:
		case 0xF70C:
		case 0xF70D:
		case 0xF70E:
		case 0xF70F:
		case 0xF710:
		case 0xF711:
		case 0xF806:
		case 0xF811:
		case 0xF814:
		case 0xF81A:
		case 0xF81C:
		case 0xFC07:
		case 0xFC08:
		case 0xFC09:
		case 0xFC0D:
		case 0xFC0F:
		case 0xFC10:
		case 0xFC11:
		case 0xFC12:
		case 0xFC13:
		case 0xFC14:
		case 0xFC15:
		case 0xFC16:
		case 0xFC17:
		case 0xFC18:
		case 0xFC19:
		case 0xFC1A:
		case 0xFC1B:
		case 0xFC1C:
		case 0xFC1D:
		case 0xFC1E:
		case 0xFC1F:
		case 0xFC27:
		case 0xFC28:
		case 0xFC2D:
		case 0xD00020:
		case 0xD00021:
		case 0xD00022:
		case 0xD00023:
		case 0xD00024:
		case 0xD00025:
		case 0xD00026:
		case 0xD00027:
		case 0xD00028:
		case 0xD00029:
		case 0xD0002A:
		case 0xD0002B:
		case 0xD0002C:
		case 0xD0002D:
		case 0xD0002E:
		case 0xD0002F:
		case 0xD00030:
		case 0xD00031:
		case 0xD00032:
		case 0xD00033:
		case 0xD00040:
		case 0xD00041:
		case 0xD00042:
		case 0xD00043:
		case 0xD00044:
		case 0xD00045:
		case 0xD00046:
		case 0xD00047:
		case 0xD00048:
		case 0xD00049:
		case 0xD0004A:
		case 0xD0004B:
		case 0xD0004C:
		case 0xD0004D:
		case 0xD0004E:
		case 0xD0004F:
		case 0xD00050:
		case 0xD00051:
		case 0xD00052:
		case 0xD00053:
		case 0xD000EB:
		case 0xD000EC:
		case 0xD000ED:
		case 0xD000EE:
		case 0xD000EF:
		case 0xD000F0:
		case 0xD000F1:
		case 0xD000F2:
		case 0xD000F3:
		case 0xD000F4:
		case 0xD000F5:
		case 0xD000F6:
		case 0xD000F7:
		case 0xD000F8:
		case 0xD000F9:
		case 0xD000FA:
		case 0xD000FB:
		case 0xD000FC:
		case 0xD000FD:
		case 0xD000FE:
		case 0xD00109:
		case 0xD0010A:
		case 0xD0012F:
		case 0xD00130:
		case 0xD00131:
		case 0xD0027D:
		case 0xD0027E:
		case 0xD0027F:
		case 0xD00281:
		case 0xD002AE:
		case 0xD00375:
		case 0xD00377:
		case 0xD00378:
		case 0xD00379:
		case 0xD00418:
		case 0xD00419:
		case 0xD0041A:
		case 0xD00453:
		case 0xD00478:
		case 0xD00479:
		case 0xD0047A:
		case 0xD0047B:
		case 0xD0047C:
		case 0xD0047D:
		case 0xD0047E:
		case 0xD0047F:
		case 0xD00480:
		case 0xD00481:
		case 0xD00482:
		case 0xD00483:
		case 0xD004DB:
		case 0xD004DC:
		case 0xD10066:
		case 0xD100A0:
		case 0xD10104:
		case 0xD10120:
		case 0xD1013A:
		case 0xD10167:
		case 0xD10293:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::SignExtendByte(UINT uName)
{
	switch( uName ) {

		case 0x000D:
		case 0xF108:
		case 0xF109:
		case 0xF10A:
		case 0xF10B:
		case 0xF10C:
		case 0xF10D:
		case 0xF11C:
		case 0xF11D:
		case 0xF1D0:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::SignExtendWord(UINT uName)
{
	switch( uName ) {

		case 0x0044:
		case 0x004D:
		case 0xF420:
		case 0xF430:
		case 0xF431:
		case 0xF432:
		case 0xF433:
		case 0xF434:
		case 0xF435:
		case 0xF436:
		case 0xF437:
		case 0xF438:
		case 0xF439:
		case 0xF43A:
		case 0xF43B:
		case 0xF43C:
		case 0xF43D:
		case 0xF43E:
		case 0xF43F:
		case 0xF440:
		case 0xF441:
		case 0xF4A0:
		case 0xF4A2:
		case 0xF4C7:
		case 0xF4C8:
		case 0xF4C9:
		case 0xF4CA:
		case 0xF4CB:
		case 0xF509:
		case 0xF511:
		case 0xF51D:
		case 0xF53E:
		case 0xF557:
		case 0xF55C:
		case 0xF55D:
		case 0xF593:
		case 0xF594:
		case 0xF595:
		case 0xF596:
		case 0xF5BA:
		case 0xFC0F:
		case 0xFC11:
		case 0xFC12:
		case 0xFC13:
		case 0xFC14:
		case 0xFC15:
		case 0xFC16:
		case 0xFC17:
		case 0xFC18:
		case 0xFC19:
		case 0xFC1A:
		case 0xFC1B:
		case 0xD00040:
		case 0xD00041:
		case 0xD00042:
		case 0xD00043:
		case 0xD00044:
		case 0xD00045:
		case 0xD00046:
		case 0xD00047:
		case 0xD00048:
		case 0xD00049:
		case 0xD0004A:
		case 0xD0004B:
		case 0xD0004C:
		case 0xD0004D:
		case 0xD0004E:
		case 0xD0004F:

			return TRUE;
		}

	return FALSE;
	}

UINT CCatLinkDriver::GetName(UINT uIndex)
{
	if( uIndex >= 0x0000 && uIndex <= 0x007F ) {

		return uIndex - 0x0000 + 0x000000;
		}

	if( uIndex >= 0x0080 && uIndex <= 0x107F ) {

		return uIndex - 0x0080 + 0xD00000;
		}

	if( uIndex >= 0x1080 && uIndex <= 0x207F ) {

		return uIndex - 0x1080 + 0xD10000;
		}

	if( uIndex >= 0x2080 && uIndex <= 0x307F ) {

		return uIndex - 0x2080 + 0x00F000;
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Custom Port Handler
//

// Constructor

CCatLinkHandler::CCatLinkHandler(IHelper *pHelper, CCatLinkDriver *pDriver)
{
	StdSetRef();

	m_pHelper = pHelper;

	m_pDriver = pDriver;

	m_fRun    = FALSE;

	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

// Destructor

CCatLinkHandler::~CCatLinkHandler(void)
{
	m_pExtra->Release();
	}

// Operations

void CCatLinkHandler::Start(void)
{
	m_fSync   = FALSE;

	m_uRxPtr  = 0;

	m_uTxPtr  = 0;

	m_uTxSize = 0;

	m_fRun    = TRUE;

	KickCatMode(TRUE);
	}

void CCatLinkHandler::Stop(void)
{
	m_fRun = FALSE;
	}

// IUnknown

HRESULT CCatLinkHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG CCatLinkHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG CCatLinkHandler::Release(void)
{
	StdRelease();
	}

// IPortHandler

void CCatLinkHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
	}

void CCatLinkHandler::OnRxData(BYTE bData)
{
	if( m_fRun && m_fSync ) {

		if( m_uRxPtr < elements(m_bRxData) ) {

			m_bRxData[m_uRxPtr++] = bData;
			}
		}
	}

void CCatLinkHandler::OnRxDone(void)
{
	if( m_fRun ) {

		KickCatMode(FALSE);

		if( m_fSync ) {

			if( m_uRxPtr ) {

				if( m_uRxPtr == elements(m_bRxData) ) {

					m_fSync = FALSE;

					return;
					}
				else {
					BYTE bSum = 0;

					for( UINT n = 0; n < m_uRxPtr; n++ ) {

						bSum += m_bRxData[n];
						}

					if( !bSum ) {

						BYTE  bSrc   = m_bRxData[0];

						BYTE  bDest  = m_bRxData[1];

						PBYTE pFrame = m_bRxData + 2;

						UINT  uSize  = m_uRxPtr  - 3;

						if( !IsSelf(bSrc) ) {

							if( !IsSelf(bDest) ) {

								m_pDriver->SeeDrop(bDest);
								}

							m_pDriver->SeeDrop(bSrc);

							Process(bSrc, bDest, pFrame, uSize);
							}
						}
					}
				}
			}

		m_fSync = TRUE;

		m_uRxPtr  = 0;
		}
	}

BOOL CCatLinkHandler::OnTxData(BYTE &bData)
{
	if( m_uTxPtr < m_uTxSize ) {

		bData = m_bTxData[m_uTxPtr++];

		return TRUE;
		}

	return FALSE;
	}

void CCatLinkHandler::OnTxDone(void)
{
	}

void CCatLinkHandler::OnOpen(CSerialConfig const &Config)
{
	}

void CCatLinkHandler::OnClose(void)
{
	}

void CCatLinkHandler::OnTimer(void)
{
	if( m_fRun ) {

		if( (m_uTxSize = m_pDriver->GetSend(m_bTxData)) ) {

			m_pPort->SetOutput(0, 0);
			
			m_pPort->SetOutput(0, 1);

			m_uTxPtr = 1;

			m_pPort->Send(m_bTxData[0]);
			}
		else
			KickCatMode(TRUE);
		}
	}

// Implementation

void CCatLinkHandler::Process(BYTE bSrc, BYTE bDest, PBYTE pData, UINT uCount)
{
	if( !IsUnknown(pData) ) {

		if( pData[0] == 0x20 ) {

			if( uCount == 2 ) {

				// Poll with 1-byte PID

				return;
				}

			return;
			}

		if( pData[0] == 0x60 ) {

			if( uCount == 3 ) {

				// Poll with 2-byte PID

				return;
				}

			return;
			}

		if( pData[0] == 0xCF && pData[1] == 0x00 ) {

			if( uCount == 5 ) {

				// Poll with 3-byte PID

				return;
				}

			return;
			}

		for( UINT uPtr = 0; uPtr < uCount; ) {

			BYTE bCode = pData[uPtr];

			if( bCode == 0xBE ) {

				// Unknown sequence at end of frame

				UINT uName = GetNameLen(pData[uPtr+2]);

				uPtr += 2;
				
				uPtr += uName;

				uPtr += 1;

				continue;
				}

			UINT uNameLen = GetNameLen(bCode);

			UINT uDataLen = GetDataLen(bCode);

			if( uNameLen && uDataLen ) {

				// Data value extraction

				UINT uName = GetName(pData + uPtr, uNameLen);

				uPtr += uNameLen;

				UINT uData = GetData(pData + uPtr, uDataLen);

				uPtr += uDataLen;

				m_pDriver->SetData(bSrc, bDest, uName, uData);

				continue;
				}

			break;
			}
		}
	}

UINT CCatLinkHandler::GetNameLen(BYTE bData)
{
	if( bData >= 0x01 && bData <= 0x1F ) {

		// Data with 1-byte PID and 1-byte data field

		return 1;
		}

	if( bData >= 0x21 && bData <= 0x5F ) {

		// Data with 1-byte PID and 2-byte data field

		return 1;
		}

	if( bData >= 0xF0 && bData <= 0xF3 ) {

		// Data with 2-byte PID and 1-byte data field

		return 2;
		}

	if( bData >= 0xF4 && bData <= 0xF7 ) {

		// Data with 2-byte PID and 2-byte data field

		return 2;
		}

	if( bData >= 0xFC /* && bData <= 0xFF */ ) {

		// Data with 2-byte PID and 4-byte data field

		return 2;
		}

	if( bData >= 0xD0 && bData <= 0xD1 ) {

		// Data with 3-byte PID and 2-byte data field

		return 3;
		}

	return 0;
	}

UINT CCatLinkHandler::GetDataLen(BYTE bData)
{
	if( bData >= 0x01 && bData <= 0x1F ) {

		// Data with 1-byte PID and 1-byte data field

		return 1;
		}

	if( bData >= 0x21 && bData <= 0x5F ) {

		// Data with 1-byte PID and 2-byte data field

		return 2;
		}

	if( bData >= 0xF0 && bData <= 0xF3 ) {

		// Data with 2-byte PID and 1-byte data field

		return 1;
		}

	if( bData >= 0xF4 && bData <= 0xF7 ) {

		// Data with 2-byte PID and 2-byte data field

		return 2;
		}

	if( bData >= 0xFC /* && bData <= 0xFF */ ) {

		// Data with 2-byte PID and 4-byte data field

		return 4;
		}

	if( bData >= 0xD0 && bData <= 0xD1 ) {

		// Data with 3-byte PID and 2-byte data field

		return 2;
		}

	return 0;
	}

UINT CCatLinkHandler::GetName(PBYTE pName, UINT uLen)
{
	if( uLen == 1 ) {

		return pName[0];
		}

	if( uLen == 2 ) {

		return MAKEWORD( pName[1],
				 pName[0]
				 );
		}
	
	if( uLen == 3 ) {

		return MAKELONG( MAKEWORD(pName[2], pName[1]),
				 MAKEWORD(pName[0], 0)
				 );
		}

	return NOTHING;
	}

UINT CCatLinkHandler::GetData(PBYTE pData, UINT uLen)
{
	if( uLen == 1 ) {

		return pData[0];
		}

	if( uLen == 2 ) {

		return MAKEWORD( pData[0],
				 pData[1]
				 );
		}
	
	if( uLen == 4 ) {

		return MAKELONG( MAKEWORD(pData[0], pData[1]),
				 MAKEWORD(pData[2], pData[3])
				 );
		}

	return 999999;
	}

BOOL CCatLinkHandler::IsUnknown(PBYTE pData)
{
	if( pData[0] == 0x82 ) {

		return TRUE;
		}

	if( pData[0] == 0xA2 ) {

		return TRUE;
		}

	if( pData[0] == 0xBC ) {

		return TRUE;
		}

	if( pData[0] == 0xCF ) {

		return TRUE;
		}

	if( pData[0] == 0xE0 ) {

		return TRUE;
		}

	if( pData[0] == 0xCF && pData[1] == 0x80 ) {

		return TRUE;
		}

	if( pData[0] >= 0xF8 && pData[0] <= 0xFB ) {

		return TRUE;
		}

	return FALSE;
	}

void CCatLinkHandler::KickCatMode(BOOL fSlow)
{
	m_pExtra->ExpCatLinkMode(fSlow);
	}

// End of File
