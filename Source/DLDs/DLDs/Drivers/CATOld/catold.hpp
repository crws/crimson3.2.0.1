
//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CATOLD_HPP
	
#define	INCLUDE_CATOLD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCatLinkDriver;
class CCatLinkHandler;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	selfDrop	0xAA

#define	pollLimit	15

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	IsSelf(x)	((x) == selfDrop)

//////////////////////////////////////////////////////////////////////////
//
// Timeous
//

#define	timeMissing	4000	// Time between read and attempt to find PID
#define	timeRetry	100	// Time to wait if too many polls outstanding
#define	timeImmediate	10	// Time to force an immediate response
#define	timePoll	2000	// Time to between two polls for same PID
#define	timePresent	2000	// Time limit between broadcasts to avoid poll
#define	timeReply	400	// Time to wait for reply to poll message
#define	timeDefer	60000	// Time to wait between scans for missing PIDs

//////////////////////////////////////////////////////////////////////////
//
// States
//

enum
{
	stateIdle	= 0,
	stateMissing	= 1,
	stateTooBusy    = 2,
	stateDefer	= 3,
	stateFind	= 4,
	stateFound	= 5,
	statePolled	= 6,
	statePresent	= 7,
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Driver
//

class CCatLinkDriver : public CMasterDriver
{
	public:
		// Constructor
		CCatLinkDriver(void);

		// Destructor
		~CCatLinkDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Ping   (void);
		DEFMETH(CCODE) Read   (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write  (AREF Addr, PDWORD pData, UINT uCount);

		// Handler Calls
		BOOL SeeDrop(BYTE bSrc);
		BOOL SetData(BYTE bSrc, BYTE bDest, UINT uName, UINT uData);
		UINT GetSend(PBYTE pData);

	protected:
		// Data Item
		struct CDataItem
		{
			UINT	m_uState;
			BYTE	m_bSrc;
			BYTE	m_bTx;
			BYTE	m_bRx;
			BYTE	m_fSkip:1;
			BYTE	m_uPoll:7;
			UINT	m_uTime;
			DWORD	m_Data;
			};

		// Data Members
		CCatLinkHandler * m_pHandler;
		CDataItem	  m_Data [0x3080];
		BOOL		  m_fDrop[256];
		UINT		  m_uPend[256];
		CDataItem *	  m_pPoll[128];
		UINT		  m_uActive;
		UINT		  m_uScan;
		UINT		  m_uHead;
		UINT		  m_uTail;

		// State Machine
		void UpdateItems(void);
		void UpdateItem(UINT uName, CDataItem *pItem);
		BOOL OnPresentData(CDataItem *pItem);
		BOOL OnPolledData(CDataItem *pItem);
		void SetTime(CDataItem *pItem, UINT uTime);
		BOOL TimeOut(CDataItem *pItem);
		void FindInitTarget(CDataItem *pItem);
		void FindNextTarget(CDataItem *pItem);
		void AddPoll(CDataItem *pItem);
		void SubPoll(CDataItem *pItem);
		BOOL QueuePoll(CDataItem *pItem);

		// Data Item Access
		CDataItem * GetItem(UINT uName);

		// Item Name Helpers
		BOOL IsValidName   (UINT uName);
		BOOL SignExtendByte(UINT uName);
		BOOL SignExtendWord(UINT uName);
		UINT GetName       (UINT uIndex);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Handler
//

class CCatLinkHandler : public IPortHandler
{	
	public:
		// Constructor
		CCatLinkHandler(IHelper *pHelper, CCatLinkDriver *pDriver);

		// Destructor
		~CCatLinkHandler(void);

		// Operations
		void Start(void);
		void Stop(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);

	protected:
		// Data Members
		ULONG            m_uRefs;
		IHelper        * m_pHelper;
		IExtraHelper   * m_pExtra;
		CCatLinkDriver * m_pDriver;
		IPortObject    * m_pPort;
		BYTE		 m_bRxData[128];
		BYTE		 m_bTxData[128];
		UINT		 m_uRxPtr;
		UINT		 m_uTxPtr;
		UINT		 m_uTxSize;
		BOOL		 m_fRun;
		BOOL		 m_fSync;

		// Implementation
		void Process(BYTE bSrc, BYTE bDest, PBYTE pData, UINT uCount);
		UINT GetNameLen(BYTE bData);
		UINT GetDataLen(BYTE bData);
		UINT GetName(PBYTE pName, UINT uLen);
		UINT GetData(PBYTE pData, UINT uLen);
		BOOL IsUnknown(PBYTE pData);
		void KickCatMode(BOOL fSlow);
	};

// End of File

#endif
