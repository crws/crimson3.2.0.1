
#include "intern.hpp"

#include "s5tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Master
//

// Instantiator

INSTANTIATE(CS5AS511TCPMaster);

// Constructor

CS5AS511TCPMaster::CS5AS511TCPMaster(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CS5AS511TCPMaster::~CS5AS511TCPMaster(void)
{
	}

// Management

void MCALL CS5AS511TCPMaster::Attach(IPortObject *pPort)
{
	}

void MCALL CS5AS511TCPMaster::Open(void)
{
	}

// Device

CCODE MCALL CS5AS511TCPMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			InitBase(m_pCtx);
			
			m_pCtx->m_IP	  = HostToMotor(GetLong(pData));
			m_pCtx->m_uPort	  = GetWord(pData);
			m_pCtx->m_fCache  = GetByte(pData);
			m_pCtx->m_fKeep	  = GetByte(pData);
			m_pCtx->m_fPing	  = GetByte(pData);
			m_pCtx->m_uTime1  = GetWord(pData);
			m_pCtx->m_uTime2  = GetWord(pData);
			m_pCtx->m_fAddr32 = GetByte(pData);
			m_pCtx->m_pSock   = NULL;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	SetBase(m_pCtx);

	return CCODE_SUCCESS;
	}

CCODE MCALL CS5AS511TCPMaster::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		FreeBase();

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CS5AS511TCPMaster::Ping(void)
{
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, 1000) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}

		return CS5AS511Base::Ping();
		}

	return CCODE_ERROR; 
	}

// Transport

BOOL CS5AS511TCPMaster::Send(PCBYTE pData, UINT uLen)
{
	Dump(pData, uLen, TRUE);
	
	UINT uSize = uLen;

	if( m_pCtx->m_pSock->Send((PBYTE)pData, uSize) == S_OK ) {
		
		return uSize == uLen;
		}

	return FALSE;
	}

BOOL CS5AS511TCPMaster::Recv(PBYTE pData, UINT &uLen, BOOL fTerm)
{
	SetTimer(TIMEOUT);

	UINT uPtr = 0;
	
	BOOL fDLE = FALSE;

	BOOL fETX = FALSE;

	while( GetTimer() ) {

		UINT uSize = min(1, uLen - uPtr);

		if( uSize ) {

			m_pCtx->m_pSock->Recv(pData + uPtr, uSize);
			
			if( uSize ) {

				if( !fDLE ) {

					if( pData[uPtr] == DLE ) {

						fDLE = TRUE;
						}
					}
				else {
					fDLE = FALSE;

					switch( pData[uPtr] ) {

						case DLE:
							continue;

						case ETX:
							fETX = TRUE;
							break;

						case NAK:
							SetBusy();

							break;
						}
					}

				uPtr ++;
				
				continue;
				}
			}

		if( uPtr && (!fTerm || fETX) ) {
	
			uLen = fETX ? uPtr - 2 : uPtr;
			
			Dump(pData, uPtr, FALSE);

			return TRUE;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

// Link Management

BOOL CS5AS511TCPMaster::CheckLink(void)
{
	return OpenSocket();
	}

void CS5AS511TCPMaster::AbortLink(void)
{
	CloseSocket(TRUE);
	}

// Socket Management

BOOL CS5AS511TCPMaster::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CS5AS511TCPMaster::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime2);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CS5AS511TCPMaster::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
