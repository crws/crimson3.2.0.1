
//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 690 Data Spaces
//

#define	SPACE_BOOL	1
#define	SPACE_WORD	2
#define	SPACE_REAL	3
#define	SPACE_ERROR	4
#define	SPACE_II	5
#define	SPACE_V0	6
#define	SPACE_V1	7
#define	SPACE_CW	8
#define	SPACE_SR	9
#define	SPACE_CS	10
#define	SPACE_SS	11

#define	ISHEX		TRUE
#define	NOTHEX		FALSE
#define	ISWRITE		TRUE
#define	NOTWRITE	FALSE

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 690 Driver
//

class CEurotherm690Driver : public CMasterDriver
{
	public:
		// Constructor
		CEurotherm690Driver(void);

		// Destructor
		~CEurotherm690Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE m_bDrop[2];
			};

		CContext	* m_pCtx;

		LPCTXT		m_pHex;
		UINT		m_uPtr;
		BYTE		m_bCheck;
		BYTE		m_bTx[32];
		BYTE		m_bRx[32];
		
		// Implementation

		// Port Access
		UINT RxByte(UINT uTime);
		
		// Frame Building
		void StartFrame(void);
		void EndFrame(BOOL fIsWrite);
		void AddByte(BYTE bData);
		void PutHexInteger( DWORD dData, DWORD dFactor );
		void PutDecInteger( DWORD dData, DWORD dFactor );
		
		// Transport Layer
		BOOL AsciiTx(void);
		UINT AsciiRx(BOOL fIsWrite);
		BOOL Transact(BOOL fIsWrite);

		// Frame Formatting
		void  PutIdentifier(UINT uOffset, UINT uTable, BOOL fIsWrite);

		// Read Handlers
		CCODE DoRealRead(UINT Addr, PDWORD pData);
		CCODE DoWordRead(UINT Addr, UINT uTable, PDWORD pData);
		CCODE DoByteRead(UINT Addr, PDWORD pData);
		CCODE DoSpecialRead(UINT uTable, PDWORD pData);

		// Write Handlers
		CCODE DoRealWrite(UINT Addr, DWORD dData);
		CCODE DoWordWrite(UINT Addr, UINT uTable, DWORD dData);
		CCODE DoByteWrite(UINT Addr, UINT uTable, DWORD dData);
		CCODE DoSpecialWrite(UINT uTable, DWORD dData);

		// Helpers
		BOOL IsADigit( BYTE b, BOOL fIsHex );
		void GetIDs( UINT uOffset, UINT uTable, PBYTE pHi, PBYTE pLo );
	};

// End of File
