#include "intern.hpp"

#include "t2udpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 UDP Master Driver
//

// Instantiator

INSTANTIATE(CToshT2MasterUDPDriver);

// Constructor

CToshT2MasterUDPDriver::CToshT2MasterUDPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CToshT2MasterUDPDriver::~CToshT2MasterUDPDriver(void)
{
	}

// Configuration

void MCALL CToshT2MasterUDPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CToshT2MasterUDPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CToshT2MasterUDPDriver::Open(void)
{
	}

// Device

CCODE MCALL CToshT2MasterUDPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtxE = new CContextE;

			m_pCtx = m_pCtxE;

			m_pCtxE->m_IP     = GetAddr(pData);
			m_pCtxE->m_uPort  = GetWord(pData);
			m_pCtxE->m_fKeep  = GetByte(pData);
			m_pCtxE->m_fPing  = GetByte(pData);
			m_pCtxE->m_uTime1 = GetWord(pData);
			m_pCtxE->m_uTime2 = GetWord(pData);
			m_pCtxE->m_uTime3 = GetWord(pData);
			m_pCtxE->m_wTrans = 0;
			m_pCtxE->m_pSock  = NULL;
			m_pCtxE->m_uLast  = GetTickCount();

			m_pCtx->m_bDrop   = 1;
			m_pCtx->m_bSeries = TRUE;
			m_pCtx->m_fCheck  = FALSE;
				
			pDevice->SetContext(m_pCtxE);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx = m_pCtxE;

	return CCODE_SUCCESS;
	}

CCODE MCALL CToshT2MasterUDPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtxE->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtxE;

		m_pCtxE = NULL;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);

	}

// Entry Points

CCODE MCALL CToshT2MasterUDPDriver::Ping(void)
{
	if( m_pCtxE->m_fPing ) {

		if( CheckIP(m_pCtxE->m_IP, m_pCtxE->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}
		}

	return CToshibaBaseMasterDriver::Ping();	
	}

// Socket Management

BOOL CToshT2MasterUDPDriver::CheckSocket(void)
{
	if( m_pCtxE->m_pSock ) {

		UINT Phase;

		m_pCtxE->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CToshT2MasterUDPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtxE->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtxE->m_uLast;

		UINT tt = ToTicks(m_pCtxE->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtxE->m_pSock = CreateSocket(IP_UDP)) ) {
	
		IPADDR const &IP   = (IPADDR const &) m_pCtxE->m_IP;

		UINT         uPort = m_pCtxE->m_uPort;

		m_pCtxE->m_pSock->SetOption(OPT_RECV_QUEUE, sizeof(m_bRxBuff));

		if( m_pCtxE->m_pSock->Connect(IP, uPort, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtxE->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtxE->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}
				if( Phase == PHASE_CLOSING ) {

					m_pCtxE->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}
				
			CloseSocket(TRUE); 

			return FALSE;
			}
		}

	return FALSE;
	}

void CToshT2MasterUDPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtxE->m_pSock ) {

		if( fAbort )
			m_pCtxE->m_pSock->Abort();
		else
			m_pCtxE->m_pSock->Close();

		m_pCtxE->m_pSock->Release();

		m_pCtxE->m_pSock = NULL;

		m_pCtxE->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CToshT2MasterUDPDriver::Send(void)
{
	UINT uSize = m_uPtr;

	if( m_pCtxE->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CToshT2MasterUDPDriver::RecvFrame(void)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	BOOL fEnd = FALSE;

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	SetTimer(m_pCtxE->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtxE->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {
			
			for( UINT u = uPtr; u < uSize + uPtr; u++ ) {

				if( m_bRxBuff[u] == ')' ) {

					fEnd = TRUE;
					}
				}

			uPtr += uSize;

			if( uPtr > 4 && fEnd ) {

				return TRUE;
				}

			continue;
			}

		if( !CheckSocket() ) {
			
			return FALSE;
			}

		Sleep(10);
		} 
	
	return FALSE;
	}

BOOL CToshT2MasterUDPDriver::Transact(void)
{
	if( Send() && RecvFrame() ) {
		
		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE; 
	}

BOOL CToshT2MasterUDPDriver::CheckFrame(void)
{
	if( m_bRxBuff[4] == 'E' ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CToshT2MasterUDPDriver::Start(UINT &uCount, UINT uType)
{
	if( !OpenSocket() ) {

		return FALSE;
		}

	return CToshibaBaseMasterDriver::Start(uCount, uType);
	}

// End of File
