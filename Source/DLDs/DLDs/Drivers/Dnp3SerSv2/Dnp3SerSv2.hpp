#ifndef	INCLUDE_DNP3SERSv2_HPP
	
#define	INCLUDE_DNP3SERSv2_HPP

#include "Dnp3SerS.hpp"

#include "Dnp3S.hpp"

#include "Dnp3Base.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  DNP3 Serial Slave Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

class CDnp3SerialSlaveV2 : public CDnp3SerialSlave
{
	public:
		// Constructor
		CDnp3SerialSlaveV2(void);
	};

#endif

// End of File

