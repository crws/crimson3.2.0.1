
#include "Dnp3SerSv2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Serial Slave v2 Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CDnp3SerialSlaveV2);

// Constructor

CDnp3SerialSlaveV2::CDnp3SerialSlaveV2(void)
{
	m_Ident   = DRIVER_ID;
	}

// End of File

