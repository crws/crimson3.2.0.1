
//////////////////////////////////////////////////////////////////////////
//
// COPS Real-Time Executive
//
// Copyright (c) 1993-2001 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Time Support
//

// Constants

#define	DAYS	(60L * 60L * 24L)

#define	QUAD	(3 * 365 + 366)

#define	WEEKS	(DAYS * 7)

// Prototypes

global	DWORD	Time(UINT h, UINT m, UINT s);
global	DWORD	Date(UINT y, UINT m, UINT d);
global	void	GetWholeDate(DWORD t, UINT *p);
global	void	GetWholeTime(DWORD t, UINT *p);
global	UINT	GetYear(DWORD t);
global	UINT	GetMonth(DWORD t);
global	UINT	GetDate(DWORD t);
global	UINT	GetDays(DWORD t);
global	UINT	GetWeeks(DWORD t);
global	UINT	GetDay(DWORD t);
global	UINT	GetWeek(DWORD t);
global	UINT	GetWeekYear(DWORD t);
global	UINT	GetHours(DWORD t);
global	UINT	GetHour(DWORD t);
global	UINT	GetMin(DWORD t);
global	UINT	GetSec(DWORD t);
global	UINT	GetMonthDays(UINT y, UINT m);
static	UINT	GetCummDays(UINT y, UINT m);
static	UINT	GetFromDays(UINT y, UINT d);

// Code

global	DWORD	Time(UINT h, UINT m, UINT s)
{
	return s + 60L * (m + (60L * h));
	}

global	DWORD	Date(UINT y, UINT m, UINT d)
{
	d = d - 1;

	m = GetCummDays(y, m);

	y = y % 100;

	UINT o = 97;

	y = (y < o) ? (y + 100 - o) : (y - o);

	y = y * 365 + y / 4;
	
	return (m + d + y) * DAYS;
	}

global	void	GetWholeDate(DWORD t, UINT *p)
{
	UINT d = GetDays(t);

	UINT r = d % QUAD / 365;

	UINT q = d / QUAD;

	if( r > 3 ) r = 3;

	UINT o = 97;
	
	p[2] = o + r + 4 * q;

	d = d - 365 * r - QUAD * q;
	
	p[1] = GetFromDays(p[2], d);

	d = d - GetCummDays(p[2], p[1]);

	p[0] = 1 + d;
	}

global	void	GetWholeTime(DWORD t, UINT *p)
{
	p[0] = GetSec(t);
	
	p[1] = GetMin(t);
	
	p[2] = GetHour(t);
	}

global	UINT	GetYear(DWORD t)
{
	UINT d = GetDays(t);

	UINT r = d % QUAD / 365;

	UINT q = d / QUAD;

	if( r > 3 ) r = 3;

	UINT o = 97;

	return o + r + 4 * q;
	}

global	UINT	GetMonth(DWORD t)
{
	UINT y = GetYear(t);
	
	UINT d = (t - Date(y, 1, 1)) / DAYS;
	
	return GetFromDays(y, d);
	}

global	UINT	GetDate(DWORD t)
{
	UINT y = GetYear(t);
	
	UINT m = GetMonth(t);
	
	UINT d = (t - Date(y, m, 1)) / DAYS;
	
	return 1 + d;
	}

global	UINT	GetDays(DWORD t)
{
	return t / DAYS;
	}

global	UINT	GetWeeks(DWORD t)
{
	return t / WEEKS;
	}

global	UINT	GetWeek(DWORD t)
{
	UINT d = GetDays(t);
	
	UINT n = 1 + ((3 + d) / 7 % 52);
	
	return n;
	}

global	UINT	GetWeekYear(DWORD t)
{
	if( GetWeek(t) == 1 ) {
	
		if( GetMonth(t) == 12 ) {
		
			UINT y = GetYear(t);
			
			return y + 1;
			}
		}
	 
	return GetYear(t);
	}

global	UINT	GetDay(DWORD t)
{
	UINT d = GetDays(t);
	
	UINT n = (3 + d) % 7;
	
	return n;
	}

global	UINT	GetHours(DWORD t)
{
	return (UINT) (t / 3600);
	}

global	UINT	GetHour(DWORD t)
{
	return (UINT) (t / 3600 % 24);
	}

global	UINT	GetMin(DWORD t)
{
	return (UINT) (t / 60 % 60);
	}

global	UINT	GetSec(DWORD t)
{
	return (UINT) (t % 60);
	}

global	UINT	GetMonthDays(UINT y, UINT m)
{
	switch( m ) {
		
		case 2:
			return (y % 4) ? 28: 29;
			
		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
		}
		
	return 31;
	}

static	UINT	GetCummDays(UINT y, UINT m)
{
	if( y % 4 ) {
	
		switch( m ) {
		
			case  1: return 0;
			case  2: return 31;
			case  3: return 31+28;
			case  4: return 31+28+31;
			case  5: return 31+28+31+30;
			case  6: return 31+28+31+30+31;
			case  7: return 31+28+31+30+31+30;
			case  8: return 31+28+31+30+31+30+31;
			case  9: return 31+28+31+30+31+30+31+31;
			case 10: return 31+28+31+30+31+30+31+31+30;
			case 11: return 31+28+31+30+31+30+31+31+30+31;
			case 12: return 31+28+31+30+31+30+31+31+30+31+30;
			
			}
			
		return 365;
		}
	else {
		switch( m ) {
		
			case  1: return 0;
			case  2: return 31;
			case  3: return 31+29;
			case  4: return 31+29+31;
			case  5: return 31+29+31+30;
			case  6: return 31+29+31+30+31;
			case  7: return 31+29+31+30+31+30;
			case  8: return 31+29+31+30+31+30+31;
			case  9: return 31+29+31+30+31+30+31+31;
			case 10: return 31+29+31+30+31+30+31+31+30;
			case 11: return 31+29+31+30+31+30+31+31+30+31;
			case 12: return 31+29+31+30+31+30+31+31+30+31+30;
		
			}
			
		return 366;
		}
		
	return 0;
	}

static	UINT	GetFromDays(UINT y, UINT d)
{
	for( UINT m = 1; m < 12; m++ ) {
	
		if( d >= GetCummDays(y, 1 + m) ) {

			continue;
			}
		
		break;
		}
		
	return m;
	}

// End of File
