
#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC 3 Communications Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// CEmersonRocMasterDriver 3 Enumerations
//

enum Index {

	indPointType	= 0,
	indLogical	= 1,
	indParam	= 2,
	indType		= 3,
	};

enum Opcode {

	ocTable		= 0,
	ocLocation	= 1,
	ocType		= 2,
	};

enum Types {

	typUINT8	= 0,
	typUINT16	= 1,
	typUINT32	= 2,
	typReal		= 3,
	typDouble	= 4,
	typStr10	= 5,
	typStr12	= 6,
	typStr20	= 7,
	typStr30	= 8,
	typStr40	= 9,
	typTLP		= 10,
	typFlag		= 11,
	typTime		= 12,
	typHourMin	= 13,
	typCurTime	= 14,
	};

enum Tables {

	tabPTMax	= 236,
	tabDouble	= 237,
	tabStr		= 238,
	tabOpcode	= 239,
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Master Serial Driver Number 3 - Slot Definition
//

struct CSlot {

	DWORD	m_Slot;
	DWORD	m_Target;
	WORD	m_Extent;
	UINT	m_Time;
	DWORD	m_Data[10];
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Master Base Driver Number 3 - Enhanced
//

class CEmRoc3MasterBaseDriver : public CMasterDriver
{
	public:
		// Constructor
		CEmRoc3MasterBaseDriver(void);

		// Destructor
		~CEmRoc3MasterBaseDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
	protected:

		// Device Context
		struct CBaseCtx 
		{
			BYTE	m_bGroup;
			BYTE	m_bUnit;
			BYTE	m_bLast;
			UINT	m_uLast;
			UINT	m_uTime;
			UINT	m_uPoll;			
			PTXT	m_OpID;
			UINT	m_Pass;
			BYTE	m_Acc;
			BOOL	m_Logon;
			BOOL	m_Use;
			BYTE	m_Inc;
			DWORD   m_Ping;
			UINT    m_uSlots;
			CSlot * m_pSlots;
			};

		// Data Members
		CBaseCtx * m_pBase;

		BYTE	m_bGroup;
		BYTE	m_bUnit;
		BYTE	m_bTx[2000];
		BYTE	m_bRx[2000];
		UINT	m_TxPtr;
		UINT	m_RxPtr;
		CRC16	m_CRC;
		
		// Handlers
		CCODE	DoPing(void);
		CCODE	LogOn(void);
		CCODE   DoOpcode167(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   DoOpcode180(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode7  (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode10 (AREF Addr, PDWORD pData, UINT uCount);
		CCODE   DoOpcode166(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   DoOpcode181(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode8  (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoOpcode11 (AREF Addr, PDWORD pData, UINT uCount);
		BOOL	CheckOpcode167(void);
		BOOL	CheckOpcode180(void);
		BOOL	CheckOpcode7  (void);
		BOOL	CheckOpcode10 (void);
		BOOL	CheckOpcode166(void);
		BOOL	CheckOpcode181(void);
		BOOL	CheckOpcode8  (void);
		BOOL	CheckOpcode11 (void);

		// Transaction
		BOOL	Transact(void);
		virtual BOOL Send(void) = 0;
		virtual BOOL Recv(void) = 0;
		
		// Implementation
		virtual BOOL CheckFrame(void);
		void	Begin(void);
		void	End(void);
		void	AddDest(void);
		void	AddHost(void);
		void	AddData(AREF Addr, PDWORD pData, UINT uCount);
		void	AddByte(BYTE bByte);
		void	AddWord(WORD wWord);
		void	AddLong(DWORD dwWord);
		void	AddDouble(PDWORD pData);
		void	AddTLP(DWORD dwWord);
		void	AddText(PTXT pText, UINT uCount);
		void	AddString(PDWORD pData, UINT uCount, UINT uLen);
		void	AddCheck(BYTE bByte);
		BOOL	CheckCRC(void);
		UINT	GetBytes(PDWORD pData, UINT uCount, UINT uSize, UINT uInc);
		UINT	GetWords(PDWORD pData, UINT uCount, UINT uSize, UINT uInc);
		UINT	GetLongs(PDWORD pData, UINT uCount, UINT uSize, UINT uInc);
		UINT	GetDoubles(PDWORD pData, UINT uCount, UINT uSize, UINT uInc);
		UINT	GetTLPs (PDWORD pData, UINT uCount, UINT uSize, UINT uInc);
		UINT	GetTime (PDWORD pData, UINT uCount, UINT uSize, UINT uInc);
		UINT	GetStrings(BYTE bType, PDWORD pData, UINT uCount, UINT uSize, UINT uInc);
		CCODE	WalkRecvData(AREF Addr, PDWORD pData, UINT uCount, UINT uSize, UINT uInc);
		CCODE	WalkRecvData(AREF Addr, PDWORD pData, UINT uCount, UINT uInc);
		BYTE	GetPointType(AREF Addr);
		BYTE	GetLogical(AREF Addr);
		BYTE	GetParameter(AREF Addr);
		DWORD	GetExtent(AREF Addr);
		DWORD   FindTarget(AREF Addr);
		CCODE	GetSlotData(CSlot * pSlot, PDWORD pData, UINT uCount);
		void    IncrementAddr(CAddress &Addr, CSlot * pSlot);
		BYTE	FindLongSize(BYTE bType);
		BYTE    FindByteSize(BYTE bType);
		BYTE	GetParamCount(AREF Addr, UINT uCount);
		BYTE	GetOpTableCount(AREF Addr, UINT uCount);

		// Slot Support
		CSlot * FindSlot(AREF Addr);
		CSlot * FindSlotByTarget(AREF Addr, UINT uMask = NOTHING);
		CSlot * FindNext(CSlot * pSlot);
		
		// Opcode Table Support
		CCODE	WalkRecvTable(AREF Addr, PDWORD pData, UINT uCouunt);
		UINT    GetWalkByte  (PBYTE &pRx, PDWORD pData);
		UINT	GetWalkWord  (PBYTE &pRx, PDWORD pData);
		UINT	GetWalkLong  (PBYTE &pRx, PDWORD pData);
		UINT	GetWalkDouble(PBYTE &pRx, PDWORD pData, UINT uCount);
		UINT	GetWalkTLP   (PBYTE &pRx, PDWORD pData);
		UINT	GetWalkTime  (PBYTE &pRx, PDWORD pData);
		UINT	GetWalkString(BYTE bType, PBYTE &pRx, PDWORD pData, UINT uCount);

		// Helpers
		BOOL	IsSlot(AREF Addr);
		BOOL	IsDouble(BYTE bType);
		BOOL	IsString(BYTE bType);
		BOOL    IsStringText(UINT uChar);
		BOOL	IsTime(BYTE bType);
		BOOL    IsOpcodeTable(BYTE bTable);
		BOOL	IsCurTime(BYTE bType);
		BOOL	IsReadOnly(BYTE bType);
		BOOL	IsTimedOut(CSlot * pSlot);
		void	GetSafeMax(BYTE &bParams, UINT &uCount, BYTE bType);
		DWORD	FromRocTime(DWORD dwTime);
		void	ShowMap(void);				
	};

// End of file
