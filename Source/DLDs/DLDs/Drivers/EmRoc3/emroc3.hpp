
#include "emroc3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC 3 Communications Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Master Serial Driver Number 3 - Enhanced
//

class CEmRoc3MasterSerialDriver : public CEmRoc3MasterBaseDriver
{
	public:
		// Constructor
		CEmRoc3MasterSerialDriver(void);

		// Destructor
		~CEmRoc3MasterSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
	protected:

		// Device Context
		struct CContext : public CBaseCtx
		{			
			WORD	m_Timeout;
			};

		// Data Members
		CContext * m_pCtx;

		// Transport Layer
		BOOL	Send(void);
		BOOL	Recv(void);
	};

// End of file
