
#include "intern.hpp"

#include "emroc3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC 3 Communications Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

/////////////////////////////////////////////////////////////////////////
//
// Emerson ROC 3 Master Base Driver
//

// Constructor

CEmRoc3MasterBaseDriver::CEmRoc3MasterBaseDriver(void)
{
	m_Ident         = DRIVER_ID; 

	m_bGroup	= 2;

	m_bUnit		= 1;

	m_TxPtr		= 0;

	m_RxPtr		= 0;

	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx));
	}

// Destructor

CEmRoc3MasterBaseDriver::~CEmRoc3MasterBaseDriver(void)
{
	}

// Entry Points

CCODE MCALL CEmRoc3MasterBaseDriver::Ping(void)
{
	if( COMMS_SUCCESS(DoPing()) ) {

		return CCODE_SUCCESS;
		} 

	if( !m_pBase->m_Logon ) {

		if( COMMS_SUCCESS(LogOn()) ) {

			return DoPing();
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEmRoc3MasterBaseDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	//AfxTrace("\nROC3 Read %4.4x %2.2x %2.2x %u %8.8x %8.8x", Addr.a.m_Offset, Addr.a.m_Extra, Addr.a.m_Table, uCount, Addr.m_Ref, GetTickCount());

	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	// Detect string configuration problems that will break databases!

	if( IsString(Addr.a.m_Extra) ) {

		if( uCount < FindLongSize(Addr.a.m_Extra) ) {

			return uCount;
			}
		}

	if ( IsSlot(Address) ) {

		Address.m_Ref = FindTarget(Addr);

		if( IsOpcodeTable(Address.a.m_Table) ) {

			return DoOpcode10(Address, pData, uCount);
			}

		if( Address.m_Ref == NOTHING ) {

			if( IsString(Addr.a.m_Extra) ) {

				return 1;
				}

			return CCODE_ERROR | CCODE_HARD;
			}
		}

	if( IsCurTime(Addr.a.m_Extra) ) {

		return DoOpcode7(Address, pData, uCount);
		}

	if( m_pBase->m_Inc ) {

		return DoOpcode180(Address, pData, uCount);
		}

	return DoOpcode167(Address, pData, uCount);
	}

CCODE MCALL CEmRoc3MasterBaseDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	//AfxTrace("\nROC3 Write %4.4x %2.2x %2.2x %u %8.8x", Addr.a.m_Offset, Addr.a.m_Extra, Addr.a.m_Table, uCount, Addr.m_Ref);

	if( IsReadOnly(Addr.a.m_Extra) ) {

		return uCount;
		}
		
	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	if ( IsSlot(Address) ) {

		Address.m_Ref = FindTarget(Addr);

		if( IsOpcodeTable(Address.a.m_Table) && !IsCurTime(Addr.a.m_Extra) ) {

			return DoOpcode11(Address, pData, uCount);
			}

		if( Address.m_Ref == NOTHING ) {

			if( IsString(Addr.a.m_Extra) ) {

				return 1;
				}

			return CCODE_ERROR | CCODE_HARD;
			}
		}

	if( IsCurTime(Addr.a.m_Extra) ) {

		return DoOpcode8(Address, pData, uCount);
		}

	if( m_pBase->m_Inc ) {

		return DoOpcode181(Address, pData, uCount);
		}

	return DoOpcode166(Address, pData, uCount);
	}

// Device

UINT MCALL CEmRoc3MasterBaseDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CBaseCtx * pCtx = (CBaseCtx *) pContext;

	if( uFunc == 1 || uFunc == 2 )  {  // Set Group or Unit

		UINT uValue = ATOI(Value) & 0xFF;

		if( uValue != 240 ) {

			switch( uFunc ) {

				case 1:	pCtx->m_bGroup	= uValue;	return 1;
				case 2: pCtx->m_bUnit	= uValue;	return 1;	
				}
			}
		}

	if( uFunc == 3 || uFunc == 4 )  {  // Read Group or Unit

		switch( uFunc ) {

			case 3:	return pCtx->m_bGroup;
			case 4: return pCtx->m_bUnit;	
			}
		}
	
	return 0;
	}

// Handlers

CCODE CEmRoc3MasterBaseDriver::DoPing(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Offset = m_pBase->m_Ping & 0xFFFF;

	Addr.a.m_Extra  = 0;

	Addr.a.m_Table  = (m_pBase->m_Ping >> 16) & 0xFF;

	Addr.a.m_Type   = addrByteAsByte;

	return Read(Addr, Data, 1);
	}

CCODE CEmRoc3MasterBaseDriver::LogOn(void)
{
	Begin();

	AddByte(17);

	AddByte(m_pBase->m_Use ? 6 : 5);

	AddText(m_pBase->m_OpID, 3);

	AddWord(m_pBase->m_Pass);

	if( m_pBase->m_Use ) {

		AddByte(m_pBase->m_Acc);
		}

	End();

	if( Transact() ) {

		m_pBase->m_Logon = TRUE;

		return CCODE_SUCCESS;
		}
	
	return CCODE_ERROR;
	}

CCODE CEmRoc3MasterBaseDriver::DoOpcode167(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType      = Addr.a.m_Extra;

	BYTE bPointType	= GetPointType(Addr);

	BYTE bLogical	= GetLogical(Addr);

	BYTE bParameter	= GetParameter(Addr);

	BYTE bParams    = GetParamCount(Addr, uCount);

	GetSafeMax(bParams, uCount, bType);

	Begin();			// 4 bytes

	AddByte(167);			// Opcode

	AddByte(4);			// Data Length

	AddByte(bPointType);		// Point Type

	AddByte(bLogical);		// Logical

	AddByte(bParams);		// Number of Params

	AddByte(bParameter);		// Starting Parameter

	End();

	if( Transact() ) {

		if( CheckOpcode167() ) {

			return WalkRecvData(Addr, pData, uCount, 0, 0);
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc3MasterBaseDriver::DoOpcode180(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType      = Addr.a.m_Extra;

	BYTE bPointType	= GetPointType(Addr);

	BYTE bLogical	= GetLogical(Addr);

	BYTE bParameter	= GetParameter(Addr);

	BYTE bParams    = GetParamCount(Addr, uCount);

	GetSafeMax(bParams, uCount, bType);

	Begin();			// 4 bytes

	AddByte(180);			// Opcode

	AddByte(1 + 3 * bParams);	// Data Length

	AddByte(bParams);		// Number of Parameters

	for( BYTE b = 0; b < bParams; b++, bLogical++ ) {

		AddByte(bPointType);	// Point Type

		AddByte(bLogical);	// Logical
		
		AddByte(bParameter);	// Parameter
		}

	End();

	if( Transact() ) {

		if( CheckOpcode180() ) {

			return WalkRecvData(Addr, pData, uCount, 3);
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc3MasterBaseDriver::DoOpcode7(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 32);

	Begin();					// Read Current Time

	AddByte(7);					// OpCode

	AddByte(0);					// Data Length

	End();

	if( Transact() ) {

		if( CheckOpcode7() ) {

			pData[0] = 0;

			pData[0] += Time(m_bRx[8], m_bRx[7], m_bRx[6]);

			BOOL fRoc800 = TRUE;

			if( fRoc800 ) {

				WORD Year = MAKEWORD(m_bRx[11], m_bRx[12]);

				pData[0] += Date(Year - 2000, m_bRx[10], m_bRx[9]);
				}
			else {
				pData[0] += Date(m_bRx[11], m_bRx[10], m_bRx[9]);
				}
				
			return 1;
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc3MasterBaseDriver::DoOpcode10 (AREF Addr, PDWORD pData, UINT uCount)
{
	Begin();					// Read Opcode Table Locations

	AddByte(10);					// Opcode

	AddByte(3);					// Data Length

	AddByte(BYTE(Addr.a.m_Offset >> 12));		// Table Number

	AddByte(BYTE(Addr.a.m_Offset & 0xFF));		// Starting Location

	AddByte(GetOpTableCount(Addr, uCount));		// Number of Locations

	End();

	if( Transact() ) {

		if( CheckOpcode10() ) {

			return WalkRecvTable(Addr, pData, uCount);
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc3MasterBaseDriver::DoOpcode166(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType      = Addr.a.m_Extra;

	BYTE bPointType	= GetPointType(Addr);

	BYTE bLogical	= GetLogical(Addr);

	BYTE bParameter	= GetParameter(Addr);

	BYTE bParams    = GetParamCount(Addr, uCount);

	BYTE bBytes     = FindByteSize(bType);

	GetSafeMax(bParams, uCount, bType);

	Begin();			// 4 bytes

	AddByte(166);			// Opcode

	AddByte(4 + bBytes * bParams);	// Data Length

	AddByte(bPointType);		// Point Type

	AddByte(bLogical);		// Logical

	AddByte(bParams);		// Number of Params

	AddByte(bParameter);		// Starting Parameter

	AddData(Addr, pData, uCount);

	End();

	if( Transact() ) {

		if( CheckOpcode166() ) {

			if( !IsString(bType) ) {

				MakeMin(uCount, m_bRx[6]);
				}

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc3MasterBaseDriver::DoOpcode181(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType	= Addr.a.m_Extra;

	BYTE bPointType	= GetPointType(Addr);

	BYTE bLogical	= GetLogical(Addr);
	
	BYTE bParameter	= GetParameter(Addr);

	BYTE bParams    = GetParamCount(Addr, uCount);

	BYTE bBytes     = FindByteSize(bType) + 3;

	UINT uLong      = FindLongSize(bType);

	DWORD dwExtent  = GetExtent(Addr);

	if( dwExtent < NOTHING ) {

		MakeMin(bParams, dwExtent);
		}

	GetSafeMax(bParams, uCount, bType);

	MakeMin(uLong, uCount);

	Begin();				// 4 bytes

	AddByte(181);				// Opcode

	AddByte(1 + bBytes * bParams);		// Data Length

	AddByte(bParams);			// Number of Parameters

	UINT uParams = bParams * uLong;

	for( UINT b = 0; b < uParams; b += uLong, bLogical++ ) {

		AddByte(bPointType);		// Point Type

		AddByte(bLogical);		// Logical
		
		AddByte(bParameter);		// Parameter

		AddData(Addr, &pData[b], uLong);
		}

	End();

	if( Transact() ) {

		if( CheckOpcode181() ) {

			if( !IsString(bType) ) {

				MakeMin(uCount, m_bRx[6]);
				}

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc3MasterBaseDriver::DoOpcode8(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 32);

	PDWORD pRead = PDWORD(alloca(uCount));
	
	if( COMMS_SUCCESS(DoOpcode7(Addr, pRead, uCount)) ) {

		BOOL fRoc800 = TRUE;

		Begin();				// Write Current Time

		AddByte(8);				// OpCode

		fRoc800 ? AddByte(7) : AddByte(8);	// Data Len

		UINT uTime = pData[0];

		BYTE bLeap = m_bRx[12];

		BYTE bCurr = m_bRx[13];

		AddByte(GetSec  (uTime));
	
		AddByte(GetMin  (uTime));
	
		AddByte(GetHour (uTime));
	
		AddByte(GetDate (uTime));
	
		AddByte(GetMonth(uTime));

		if( fRoc800 ) {

			AddWord(GetYear(uTime) % 100 + 2000);
			}
		else {
	
			AddByte(GetYear (uTime) % 100);

			AddByte(bLeap);

			AddByte(bCurr);
			}
		End();

		if( Transact() ) {

			if( CheckOpcode8() ) {

				return 1;
				}
			}
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc3MasterBaseDriver::DoOpcode11 (AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType = Addr.a.m_Extra;

	Begin();					// Read Opcode Table Locations

	AddByte(11);					// Opcode

	AddByte(3 + FindByteSize(bType));		// Data Length

	AddByte(BYTE(Addr.a.m_Offset >> 12));		// Table Number

	AddByte(BYTE(Addr.a.m_Offset & 0xFF));		// Starting Location

	AddByte(1);					// Number of Locations

	UINT uSize = FindLongSize(bType);

	MakeMin(uCount, uSize);

	AddData(Addr, pData, uCount);			
	
	End();

	if( Transact() ) {

		if( CheckOpcode11() ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

BOOL CEmRoc3MasterBaseDriver::CheckOpcode167(void)
{
	return m_bRx[4] == 167 && !memcmp(m_bTx + 6, m_bRx + 6, 4);
	}

BOOL CEmRoc3MasterBaseDriver::CheckOpcode180(void)
{
	return m_bRx[4] == 180 && !memcmp(m_bTx + 6, m_bRx + 6, 4);
	}

BOOL CEmRoc3MasterBaseDriver::CheckOpcode7(void)
{
	return m_bRx[4] == 7;
	}

BOOL CEmRoc3MasterBaseDriver::CheckOpcode10(void)
{
	return m_bRx[4] == 10 && !memcmp(m_bTx + 6, m_bRx + 6, 3);
	}

BOOL CEmRoc3MasterBaseDriver::CheckOpcode166(void)
{
	return m_bRx[4] == 166;
	}

BOOL CEmRoc3MasterBaseDriver::CheckOpcode181(void)
{
	return m_bRx[4] == 181;
	}

BOOL CEmRoc3MasterBaseDriver::CheckOpcode8(void)
{
	return m_bRx[4] == 8;
	}

BOOL CEmRoc3MasterBaseDriver::CheckOpcode11(void)
{
	return m_bRx[4] == 11;
	}

// Implementation

void CEmRoc3MasterBaseDriver::Begin(void)
{
	m_TxPtr = 0;

	m_CRC.Clear();

	AddDest();

	AddHost();
	}

void CEmRoc3MasterBaseDriver::End(void)
{
	AddWord(m_CRC.GetValue());
	}

void CEmRoc3MasterBaseDriver::AddDest(void)
{
	AddByte(m_pBase->m_bUnit);

	AddByte(m_pBase->m_bGroup);
	}

void CEmRoc3MasterBaseDriver::AddHost(void)
{
	AddByte(m_bUnit);

	AddByte(m_bGroup);
	}

void CEmRoc3MasterBaseDriver::AddData(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType = Addr.a.m_Extra;

	for( UINT u = 0; u < uCount; u++ ) {

		if( bType == typUINT8 || bType == typFlag ) {

			AddByte(pData[u] & 0xFF);

			continue;
			}
		
		if( bType == typUINT16 || bType == typHourMin ) {

			AddWord(pData[u] & 0xFFFF);

			continue;
			}

		if( bType == typUINT32 || bType == typReal ) {

			AddLong(pData[u]);

			continue;
			}

		if( IsString(bType) ) {

			AddString(&pData[u], uCount, FindByteSize(bType));

			u += FindLongSize(bType) - 1;

			continue;
			}

		if( bType == typTLP ) {

			AddTLP(pData[u]);

			continue;
			}

		if( bType == typDouble  ) {

			AddDouble(&pData[u]);

			u++;
			}
		}
	}

void CEmRoc3MasterBaseDriver::AddByte(BYTE bByte)
{
	m_bTx[m_TxPtr] = bByte;

	AddCheck(bByte);

	m_TxPtr++;
       	}

void CEmRoc3MasterBaseDriver::AddWord(WORD wWord)
{
	AddByte(LOBYTE(wWord));

	AddByte(HIBYTE(wWord));
	}

void CEmRoc3MasterBaseDriver::AddLong(DWORD dwWord)
{
	AddWord(LOWORD(dwWord));

	AddWord(HIWORD(dwWord));
	}

void CEmRoc3MasterBaseDriver::AddDouble(PDWORD pData)
{
	AddLong(pData[1]);

	AddLong(pData[0]);
	}

void CEmRoc3MasterBaseDriver::AddTLP(DWORD dwWord)
{
	AddByte(LOBYTE(HIWORD(dwWord)));

	AddByte(HIBYTE(LOWORD(dwWord)));

	AddByte(LOBYTE(LOWORD(dwWord)));
	}

void CEmRoc3MasterBaseDriver::AddText(PTXT pText, UINT uCount)
{
	for( UINT u = 0; u < uCount;  u++ ) {

		AddByte(pText[u]);
		}
	}

void CEmRoc3MasterBaseDriver::AddString(PDWORD pData, UINT uCount, UINT uLen)
{	
	BYTE bShift = 0;

	BYTE bByte  = 0;

	BOOL fDone  = FALSE;

	uCount     *= sizeof(DWORD);

	MakeMin(uCount, uLen);

	for( UINT u = 0, i = 0; u < uLen; u++) {

		switch( u % 4 ) {

			case 0:	bShift = 24;	break;	
			case 1: bShift = 16;	break;
			case 2: bShift = 8;	break;
			case 3: bShift = 0;	break;
			}

		bByte = ((pData[i] >> bShift) & 0xFF);

		if( !fDone && u < uCount && IsStringText(bByte) ) {

			AddByte(bByte);
			}
		else {
			if( !fDone ) {

				fDone = bByte == 0;
				}

			AddByte(fDone ? 0x00 : 0x20);
			}

		if( !bShift ) {

			i++;
			}
		}
	}

void CEmRoc3MasterBaseDriver::AddCheck(BYTE bByte)
{
	m_CRC.Add(bByte);
	}


BOOL CEmRoc3MasterBaseDriver::Transact(void)
{
	if( Send() ) {

		if( Recv() ){

			if( CheckFrame() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CEmRoc3MasterBaseDriver::CheckFrame(void)
{
	if( m_bRx[0] != m_bUnit	) {

		return FALSE;
		}

	if( m_bRx[1] != m_bGroup ) {

		return FALSE;
		}

	if( m_bRx[2] != m_pBase->m_bUnit ) {

		return FALSE;
		}

	if( m_bRx[3] != m_pBase->m_bGroup ) {

		return FALSE;
		}

	if( m_bRx[4] == 0xFF ) {

		// ERROR

		switch( m_bRx[6] ) {

			case 20:
			case 21:
			case 63:

				m_pBase->m_Logon = FALSE;
				break;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CEmRoc3MasterBaseDriver::CheckCRC(void)
{
	return !m_CRC.GetValue();
	}

UINT CEmRoc3MasterBaseDriver::GetBytes(PDWORD pData, UINT uCount, UINT uSize, UINT uInc)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = PBYTE(m_bRx + 10 + u * sizeof(BYTE) + uInc * u)[0];
		}

	return uCount;
	}

UINT CEmRoc3MasterBaseDriver::GetWords(PDWORD pData, UINT uCount, UINT uSize, UINT uInc)
{
	for( UINT u = 0; u < uCount; u++ ) {

		WORD x = PU2(m_bRx + 10 + u * sizeof(WORD) + uInc * u)[0];

		pData[u] = LONG(SHORT(IntelToHost(x)));
		}

	return uCount;
	}


UINT CEmRoc3MasterBaseDriver::GetLongs(PDWORD pData, UINT uCount, UINT uSize, UINT uInc)
{
       for( UINT u = 0; u < uCount; u++ ) {

	       	DWORD x = PU4(m_bRx + 10 + u * sizeof(DWORD) + uInc * u)[0];
		
		pData[u] = IntelToHost(x);
		}

	return uCount;
	}

UINT CEmRoc3MasterBaseDriver::GetDoubles(PDWORD pData, UINT uCount, UINT uSize, UINT uInc)
{
	for( UINT u = 0, i = 0; u < uCount / 2; u++, i += 2 ) {

		DWORD x = PU4(m_bRx + 10 + u * sizeof(DWORD) * 2 + uInc * u + 4)[0];
		
		pData[i] = IntelToHost(x);

		x = PU4(m_bRx + 10 + u * sizeof(DWORD) * 2 + uInc * u + 0)[0];
		
		pData[i + 1] = IntelToHost(x);
		}

	return uCount;
	}

UINT CEmRoc3MasterBaseDriver::GetTLPs(PDWORD pData, UINT uCount, UINT uSize, UINT uInc)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = 0;

		pData[u] |= ((m_bRx[10 +  0 + u * 3 + uInc * u]) << 16);

		pData[u] |= ((m_bRx[10 +  1 + u * 3 + uInc * u]) <<  8);

		pData[u] |= ((m_bRx[10 +  2 + u * 3 + uInc * u]) <<  0);
		}

	return uCount;
	}

UINT CEmRoc3MasterBaseDriver::GetTime(PDWORD pData, UINT uCount, UINT uSize, UINT uInc)
{
	DWORD Data[1];

	if( GetLongs(Data, 1, uSize, uInc) ) {

		pData[0] = FromRocTime(Data[0]);

		return 1;
		}

	pData[0] = 0;
				
	return 1;
	}

UINT CEmRoc3MasterBaseDriver::GetStrings(BYTE bType, PDWORD pData, UINT uCount, UINT uSize, UINT uInc)
{
	UINT uChars = 0;

	switch( bType ) {

		case typStr10:	uChars = 10;	break;
		case typStr12:	uChars = 12;	break;
		case typStr20:	uChars = 20;	break;
		case typStr30:  uChars = 30;	break;
		case typStr40:	uChars = 40;	break;	
		}

	if( uChars ) {

		UINT uBytes = m_bRx[5] - 4;

		UINT uIndex = 0;

		for( UINT u = 0, c = 0; u < uBytes; u++ ) {

			BYTE bRx = m_bRx[10 + u];

			if( !IsStringText(bRx) ) {

				if( bRx != 0 ) {

					bRx = 0x20;
					}
				}

			switch( c % 4 ) {

				case 0:		pData[uIndex]  = 0;	
						pData[uIndex] |= (bRx << 24);
						break;

				case 1:		pData[uIndex] |= (bRx << 16);
						break;

				case 2:		pData[uIndex] |= (bRx <<  8);
						break;

				case 3:		pData[uIndex] |= (bRx <<  0);

						if( uIndex >= uCount ) {

							return uIndex;
							}

						uIndex++;

						break;
				}

			c++;

			if( c == uChars ) {

				if( uChars % 4 != 0 ) {

					uIndex++;

					if( uIndex >= uCount ) {

						return uIndex;
						}

					pData[uIndex]  = 0;
					}

				c = 0;

				u += uInc;
				}
			}

		return uIndex;
		}

	return CCODE_ERROR;
	}

CCODE CEmRoc3MasterBaseDriver::WalkRecvData(AREF Addr, PDWORD pData, UINT uCount, UINT uSize, UINT uInc)
{
	BYTE bType = Addr.a.m_Extra;

	if( bType == typUINT8 || bType == typFlag ) {

		return GetBytes(pData, uCount, uSize, uInc);
		}

	if( bType == typUINT16 || bType == typHourMin ) {

		return GetWords(pData, uCount, uSize, uInc);
		}

	if( bType == typUINT32 || bType == typReal ) {

		return GetLongs(pData, uCount, uSize, uInc);
		}

	if( IsDouble(bType) ) {

		return GetDoubles(pData, uCount, uSize, uInc);
		}

	if( IsString(bType) ) {

		return GetStrings(bType, pData, uCount, uSize, uInc);
		}

	if( bType == typTLP ) {

		return GetTLPs(pData, uCount, uSize, uInc);
		}

	if( bType == typTime ) {

		return GetTime(pData, uCount, uSize, uInc);
		}

	return 0;
	}

CCODE CEmRoc3MasterBaseDriver::WalkRecvData(AREF Addr, PDWORD pData, UINT uCount, UINT uInc)
{
	BYTE bType   = Addr.a.m_Extra;

	BYTE bSize   = FindByteSize(bType);

	UINT uSize   = FindLongSize(bType);

	return WalkRecvData(Addr, pData, uCount, uSize, 3);
	}

BYTE CEmRoc3MasterBaseDriver::GetPointType(AREF Addr)
{
	return Addr.a.m_Table;
	}

BYTE CEmRoc3MasterBaseDriver::GetLogical(AREF Addr)
{
	if( m_pBase->m_Inc )  {

		return Addr.a.m_Offset & 0xFF;
		}

	return BYTE(Addr.a.m_Offset >> 8);
	}

BYTE CEmRoc3MasterBaseDriver::GetParameter(AREF Addr)
{
	if( m_pBase->m_Inc )  {

		return BYTE(Addr.a.m_Offset >> 8);
		}

	return Addr.a.m_Offset & 0xFF;
	}

DWORD CEmRoc3MasterBaseDriver::GetExtent(AREF Addr)
{
	if( IsSlot(Addr) ) {

		BYTE bType = Addr.a.m_Extra;

		BYTE bSize = FindLongSize(bType);

		for( UINT s = 0; s < m_pBase->m_uSlots; s++ ) {

			if( m_pBase->m_pSlots[s].m_Target == Addr.m_Ref ) {

				return m_pBase->m_pSlots[s].m_Extent / bSize;
				}
			}

		// This must be an element of an array !!!!

		for( UINT u = 0; u < m_pBase->m_uSlots; u++ ) {

			DWORD dwSlot = m_pBase->m_pSlots[u].m_Slot;
			
			DWORD dwExt  = m_pBase->m_pSlots[u].m_Extent;

			if( dwSlot <= Addr.m_Ref && ((dwSlot + dwExt) >= Addr.m_Ref)  ) {

				return m_pBase->m_pSlots[u].m_Extent - ((Addr.m_Ref - dwSlot) / bSize);
				}
			}
		}

	return NOTHING;
	}

DWORD CEmRoc3MasterBaseDriver::FindTarget(AREF Addr)
{
	if( IsSlot(Addr) ) {

		UINT uLong = FindLongSize(Addr.a.m_Extra);

		for( UINT s = 0; s < m_pBase->m_uSlots; s++ ) {

			if( m_pBase->m_pSlots[s].m_Slot == Addr.m_Ref ) {

				return m_pBase->m_pSlots[s].m_Target;
				}

			// Test as an element of an array !!!!

			WORD wExtent = m_pBase->m_pSlots[s].m_Extent;

			for( UINT w = uLong; w < wExtent; w += uLong ) {

				if( m_pBase->m_pSlots[s].m_Slot + w == Addr.m_Ref ) {

					return m_pBase->m_pSlots[s].m_Target + w / uLong;
					}
				}
			}
		}

	return NOTHING;
	}

CCODE CEmRoc3MasterBaseDriver::GetSlotData(CSlot * pSlot, PDWORD pData, UINT uCount)
{
	CAddress Addr;

	Addr.m_Ref = pSlot->m_Target;

	BYTE bType = Addr.a.m_Extra;

	UINT uSize = FindLongSize(bType);

	memcpy(pData, pSlot->m_Data, uSize * sizeof(DWORD));

	return uSize;
	}

void CEmRoc3MasterBaseDriver::IncrementAddr(CAddress &Addr, CSlot * pSlot)
{
	CAddress Target;

	Target.m_Ref = pSlot->m_Target;

	BYTE bType = Target.a.m_Extra;

	Addr.a.m_Offset += FindLongSize(bType);
	}

BYTE CEmRoc3MasterBaseDriver::FindLongSize(BYTE bType)
{
	switch( bType ) {

		case typDouble:	return 2;

		case typStr10:	return 3;
		case typStr12:	return 3;
		case typStr20:	return 5;
		case typStr30:	return 8;
		case typStr40:	return 10;
		}

	return 1;
	}

BYTE CEmRoc3MasterBaseDriver::FindByteSize(BYTE bType)
{
	switch( bType ) {

		case typUINT16:	return 2;
		case typUINT32:	
		case typReal:	return 4;
		case typDouble: return 8;
		case typStr10:	return 10;
		case typStr12:	return 12;
		case typStr20:	return 20;
		case typStr30:	return 30;
		case typStr40:	return 40;
		case typTLP:	return 3;
		case typTime:	return 4;
		case typHourMin:return 2;
		case typCurTime:return 2;
		}
	
	return 1;
	}

BYTE CEmRoc3MasterBaseDriver::GetParamCount(AREF Addr, UINT uCount)
{
	if( IsSlot(Addr) ) {

		CAddress Address;

		Address.m_Ref = Addr.m_Ref;

		for( UINT u = 0, c = 0; u < m_pBase->m_uSlots; u++ ) {

			CSlot * pSlot = &m_pBase->m_pSlots[u];

			if( pSlot ) {

				if( Address.m_Ref == pSlot->m_Target ) {

					BYTE Count = 0;

					UINT uMask = 0xF0FFFFFF;

					UINT uSize = FindLongSize(AREF(pSlot->m_Target).a.m_Extra);

					while( pSlot && ((Address.m_Ref & uMask) == (pSlot->m_Target & uMask)) ) {

						UINT uExt  = GetExtent(AREF(pSlot->m_Target));

						Count += uExt;

						c += uExt * uSize;

						u++;

						if( u < m_pBase->m_uSlots && c < uCount ) {

							Address.m_Ref += m_pBase->m_Inc ? 1 : uExt;

							pSlot = &m_pBase->m_pSlots[u];

							continue;
							}

						break;
						}

					MakeMin(Count, uCount / uSize);

					return Count;
					}
				}
			}

		return BYTE(uCount / FindLongSize(Addr.a.m_Extra));
		}

	return BYTE(uCount);
	}

BYTE CEmRoc3MasterBaseDriver::GetOpTableCount(AREF Addr, UINT uCount)
{
	CAddress Target;

	Target.m_Ref  = Addr.m_Ref;

	UINT Count    = 0;

	CSlot * pSlot = FindSlotByTarget(Target);

	while( pSlot ) {

		UINT uLong = FindLongSize(Addr.a.m_Extra);

		if( pSlot->m_Extent ) {

			Count += pSlot->m_Extent / uLong;

			uLong  = pSlot->m_Extent;
			}

		if( uCount > uLong ) {

			uCount -= uLong;

			Target.a.m_Offset++;

			pSlot   = FindSlotByTarget(Target);

			continue;
			}

		break;
		}

	return Count;
	}

// Slot Support

CSlot * CEmRoc3MasterBaseDriver::FindSlot(AREF Addr)
{
	for( UINT u = 0; u < m_pBase->m_uSlots; u++ ) {

		CSlot * pSlot = &m_pBase->m_pSlots[u];

		if( pSlot ) {

			if( Addr.m_Ref == pSlot->m_Slot ) {

				return &m_pBase->m_pSlots[u];
				}
			}
		}
	
	return NULL;
	}

CSlot * CEmRoc3MasterBaseDriver::FindSlotByTarget(AREF Addr, UINT uMask)
{
	for( UINT u = 0; u < m_pBase->m_uSlots; u++ ) {

		CSlot * pSlot = &m_pBase->m_pSlots[u];

		if( pSlot ) {

			if( ((Addr.m_Ref & uMask) == (pSlot->m_Target & uMask)) ) {

				return &m_pBase->m_pSlots[u];
				}
			}
		}
	
	return NULL;
	}

CSlot * CEmRoc3MasterBaseDriver::FindNext(CSlot * pSlot)
{
	CAddress Addr;
	
	Addr.m_Ref = pSlot->m_Target + 1;

	CSlot * pFind = FindSlotByTarget(Addr, 0xF0FFFFFF);

	if( pFind && pFind->m_Slot > pSlot->m_Slot ) {

		return pFind;
		}

	return NULL;
	}

// Opcode Table Support 

CCODE CEmRoc3MasterBaseDriver::WalkRecvTable(AREF Addr, PDWORD pData, UINT uCount)
{
	CSlot * pSlot = FindSlotByTarget(Addr);

	PBYTE pRx = m_bRx;

	pRx += 13;

	UINT uRecv = 0;

	UINT uExtent = pSlot ? pSlot->m_Extent : 0;

	for( UINT u = 0; u < uCount; ) {

		if( u && !uExtent ) {

			pSlot = FindNext(pSlot);

			uExtent = pSlot ? pSlot->m_Extent : 0;
			}

		if( !pSlot ) {

			break;
			}

		AREF Addr  = AREF(pSlot->m_Target);

		BYTE bType = Addr.a.m_Extra;

		if( bType == typUINT8 || bType == typFlag ) {

			uRecv = GetWalkByte(pRx, &pData[u]);

			u += uRecv;

			uExtent = uExtent >= uRecv ? uExtent - uRecv : 0;

			continue;
			}

		if( bType == typUINT16 || bType == typHourMin ) {

			uRecv = GetWalkWord(pRx, &pData[u]);

			u += uRecv;

			uExtent = uExtent >= uRecv ? uExtent - uRecv : 0;

			continue;
			}

		if( bType == typUINT32 || bType == typReal ) {

			uRecv = GetWalkLong(pRx, &pData[u]);

			u += uRecv;

			uExtent = uExtent >= uRecv ? uExtent - uRecv : 0;

			continue;
			}

		if( IsDouble(bType) ) {

			uRecv = GetWalkDouble(pRx, &pData[u], uCount - u);

			u += uRecv;

			uExtent = uExtent >= uRecv ? uExtent - uRecv : 0;
			
			continue;
			}

		if( IsString(bType) ) {

			uRecv = GetWalkString(bType, pRx, &pData[u], uCount - u);

			u += uRecv;

			uExtent = uExtent >= uRecv ? uExtent - uRecv : 0;

			continue;
			}

		if( bType == typTLP ) {

			uRecv = GetWalkTLP(pRx, &pData[u]);

			u += uRecv;

			uExtent = uExtent >= uRecv ? uExtent - uRecv : 0;

			continue;
			}

		if( bType == typTime || bType == typCurTime ) {

			uRecv = GetWalkTime(pRx, &pData[u]);

			u += uRecv;

			uExtent = uExtent >= uRecv ? uExtent - uRecv : 0;
			
			continue;
			}

		break;
		}

	return u;
	}

UINT CEmRoc3MasterBaseDriver::GetWalkByte(PBYTE &pRx, PDWORD pData)
{
	*pData = *pRx;

	pRx++;

	return 1;
	}

UINT CEmRoc3MasterBaseDriver::GetWalkWord(PBYTE &pRx, PDWORD pData)
{
	WORD x = PU2(pRx)[0];

	*pData = LONG(SHORT(IntelToHost(x)));

	pRx += 2;

	return 1;
	}

UINT CEmRoc3MasterBaseDriver::GetWalkLong(PBYTE &pRx, PDWORD pData)
{
	DWORD x = PU4(pRx)[0];
	
	*pData  = IntelToHost(x);

	pRx += 4;

	return 1;
	}

UINT CEmRoc3MasterBaseDriver::GetWalkDouble(PBYTE &pRx, PDWORD pData, UINT uCount)
{
	UINT uReturn = 0;

	if( uCount > 1 ) {
	
		uReturn += GetWalkLong(pRx, &pData[1]);
		}

	uReturn += GetWalkLong(pRx, &pData[0]);

	return uReturn;
	}

UINT CEmRoc3MasterBaseDriver::GetWalkTLP(PBYTE &pRx, PDWORD pData)
{
	*pData = 0;

	*pData |= (*pRx << 16);

	pRx++;

	*pData |= (*pRx <<  8);

	pRx++;
	
	*pData |= (*pRx <<  0);

	pRx++;

	return 1;
	}

UINT CEmRoc3MasterBaseDriver::GetWalkTime(PBYTE &pRx, PDWORD pData)
{
	GetWalkLong(pRx, pData);

	pData[0] = FromRocTime(pData[0]);

	return 1;
	}

UINT CEmRoc3MasterBaseDriver::GetWalkString(BYTE bType, PBYTE &pRx, PDWORD pData, UINT uCount)
{
	UINT uBytes = FindByteSize(bType);

	UINT uLongs = FindLongSize(bType);

	for( UINT u = 0, i = 0; u < uBytes; u++, pRx++ ) {

		UINT uMod = u % 4;

		if( uMod == 0 ) {

			pData[i] = 0;
			}

		UINT uShift = 24 - uMod * 8;

		pData[i] |= *pRx << uShift;

		if( uShift == 0 ) {

			i++;
			}
		}

	return uLongs;
	}

// Helpers

BOOL CEmRoc3MasterBaseDriver::IsSlot(AREF Addr)
{
	return IsDouble(Addr.a.m_Extra) || IsString(Addr.a.m_Extra) || IsOpcodeTable(Addr.a.m_Table);
	}

BOOL CEmRoc3MasterBaseDriver::IsDouble(BYTE bType)
{
	return bType == typDouble;
	}

BOOL CEmRoc3MasterBaseDriver::IsString(BYTE bType)
{
	switch( bType ) {

		case typStr10:
		case typStr12:
		case typStr20:
		case typStr30:
		case typStr40:

			return TRUE;
		}

	return FALSE;
	}

BOOL CEmRoc3MasterBaseDriver::IsStringText(UINT uChar)
{
	return (isalpha(uChar) || isdigit(uChar) || ispunct(uChar) || isspace(uChar));
	}

BOOL CEmRoc3MasterBaseDriver::IsTime(BYTE bType)
{
	return ((bType == typTime) || (bType == typCurTime));
	}

BOOL CEmRoc3MasterBaseDriver::IsOpcodeTable(BYTE bTable)
{
	return bTable == tabOpcode;
	}

BOOL CEmRoc3MasterBaseDriver::IsCurTime(BYTE bType)
{
	return bType == typCurTime;
	}

BOOL CEmRoc3MasterBaseDriver::IsReadOnly(BYTE bType)
	{
	switch( bType ) {

		case typTime:

			return TRUE;
		}

	return FALSE;
	}

BOOL CEmRoc3MasterBaseDriver::IsTimedOut(CSlot * pSlot)
{
	return int(GetTickCount() - pSlot->m_Time - m_pBase->m_uPoll) >= 0;
	}

void CEmRoc3MasterBaseDriver::GetSafeMax(BYTE &bParams, UINT &uCount, BYTE bType)
{
	UINT uBytes = 32 * sizeof(DWORD) + 12;

	MakeMax(bParams, 1);

	MakeMin(bParams, min((uBytes - 12) / FindByteSize(bType), 255));

	MakeMin(uCount, bParams * FindLongSize(bType));
	}

DWORD CEmRoc3MasterBaseDriver::FromRocTime(DWORD dwRoc)
{
	return dwRoc - 852076800;
	}

void CEmRoc3MasterBaseDriver::ShowMap(void)
{
	AfxTrace("\nShowing MAP %u:", m_pBase->m_uSlots);

	for( UINT u = 0; u < m_pBase->m_uSlots; u++ ) {

		AREF Targ = AREF(m_pBase->m_pSlots[u].m_Target);

		AfxTrace("\n%8.8x %8.8x table %2.2x extra %2.2x type %2.2x ext %u", m_pBase->m_pSlots[u].m_Slot,
										    Targ.m_Ref,
										    Targ.a.m_Table,
										    Targ.a.m_Extra,
										    Targ.a.m_Type,
										    m_pBase->m_pSlots[u].m_Extent);
		}
	}

// End of file
