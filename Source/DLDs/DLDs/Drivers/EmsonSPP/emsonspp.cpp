
#include "intern.hpp"

#include "emsonspp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson SPP Driver
//

// Instantiator

INSTANTIATE(CEmersonSPPDriver);

// Constructor

CEmersonSPPDriver::CEmersonSPPDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_pTx       = NULL;

	m_pRx       = NULL;

	m_uTimeout  = 1000;
	}

// Destructor

CEmersonSPPDriver::~CEmersonSPPDriver(void)
{
	FreeBuffers();
	}

// Configuration

void MCALL CEmersonSPPDriver::Load(LPCBYTE pData)
{
	AllocBuffers();
	}
	
void MCALL CEmersonSPPDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}
	
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CEmersonSPPDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEmersonSPPDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmersonSPPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_fDisableCheck = TRUE;
			m_pCtx->m_fIgnoreReadEx = TRUE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmersonSPPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmersonSPPDriver::Ping(void)
{
	if( !m_pCtx->m_bDrop ) {

		return CCODE_SUCCESS;
		}

	DWORD  Data[1];

	return DoWordRead(PING_REGISTER, Data, 1);
	}

CCODE MCALL CEmersonSPPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_bDrop ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	switch( Addr.a.m_Type ) {

		case addrWordAsWord:
			return DoWordRead(Addr.a.m_Offset, pData, 1);

		case addrLongAsLong:
			return DoLongRead(Addr.a.m_Offset, pData, 1);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CEmersonSPPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n** WRITE** Tab=%d Off=%d Ct=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	switch( Addr.a.m_Type ) {

		case addrWordAsWord:
			return DoWordWrite(Addr.a.m_Offset, pData, 1);

		case addrLongAsLong:
			return DoLongWrite(Addr.a.m_Offset, pData, 1);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

void CEmersonSPPDriver::AllocBuffers(void)
{
	m_uTxSize = 16;
	
	m_uRxSize = 16;
	
	m_pTx = new BYTE [ m_uTxSize ];

	m_pRx = new BYTE [ m_uRxSize ];
	}

void CEmersonSPPDriver::FreeBuffers(void)
{
	if( m_pTx ) {

		delete [] m_pTx;

		m_pTx = NULL;
		}

	if( m_pRx ) {

		delete [] m_pRx;

		m_pRx = NULL;
		}
	}

BOOL CEmersonSPPDriver::IgnoreException(void)
{
	return (m_pCtx->m_fIgnoreReadEx && (m_pRx[1] & 0x80));
	}

// Frame Building

void CEmersonSPPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr   = 0;

	m_pRx[1] = 0;
	
	AddByte(m_pCtx->m_bDrop);
	
	AddByte(bOpcode);
	}

void CEmersonSPPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {
	
		m_pTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CEmersonSPPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CEmersonSPPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Port Access

UINT CEmersonSPPDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}
		
// Transport Layer

BOOL CEmersonSPPDriver::PutFrame(void)
{
	m_pData->ClearRx();
	
	return BinaryTx();
	}

BOOL CEmersonSPPDriver::GetFrame(BOOL fWrite)
{
	return BinaryRx(fWrite);
	}

BOOL CEmersonSPPDriver::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_pTx[k]);

	m_pData->Write(m_pTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CEmersonSPPDriver::BinaryRx(BOOL fWrite)
{
	UINT uPtr = 0;

	UINT uGap = 0;

	SetTimer(m_uTimeout);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		UINT uByte;
		
		if( (uByte = RxByte(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

//**/			AfxTrace1("<%2.2x>", uByte);

			if( uPtr <= m_uRxSize ) {
			
				uGap = 0;
				
				continue;
				}
				
			return FALSE;
			}

		if( ++uGap >= 5 ) {
			
			if( uPtr >= 4 ) {

				if( m_pCtx->m_fDisableCheck ) {
					
					if( uPtr >= ( fWrite ? 8 : UINT(m_pRx[2] + 5) ) ) {

						return TRUE;
						}
					}
				else {
				
					m_CRC.Preset();
				
					PBYTE p = m_pRx;
				
					UINT  n = uPtr - 2;
			
					for( UINT i = 0; i < n; i++ ) {

						m_CRC.Add(*(p++));
						}

					WORD c1 = IntelToHost(PWORD(p)[0]);
				
					WORD c2 = m_CRC.GetValue();
					
					if( c1 == c2 ) {
						 
						return TRUE;
						}
					}
				}
				
			uPtr = 0;
			
			uGap = 0;
			}
		}

	return FALSE;
	}

BOOL CEmersonSPPDriver::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {
	
		if( !m_pCtx->m_bDrop ) { 

			while( m_pData->Read(0) < NOTHING );

			return TRUE;
			}
			
		if( GetFrame(fIgnore) ) {

			return CheckReply(fIgnore);
			}
		}

	return FALSE;
	}

BOOL CEmersonSPPDriver::CheckReply(BOOL fIgnore)
{
	if( m_pRx[0] == m_pTx[0] ) {

		if( fIgnore ) {

			return TRUE;
			}
	
		if( !(m_pRx[1] & 0x80) ) {
		
			return TRUE;
			}
		}
		
	return FALSE;
	}

// Read Handlers

CCODE CEmersonSPPDriver::DoWordRead(UINT uOff, PDWORD pData, UINT uCount)
{
	StartFrame(3);
		
	AddWord( uOff - WORD_OFFSET );
	
	AddWord(1);
	
	if( Transact(FALSE) ) {

		pData[0] = LONG(SHORT(MotorToHost(*PWORD(m_pRx + 3))));

		return 1;
		}

	if( (m_pRx[1] & 0x80) && IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE CEmersonSPPDriver::DoLongRead(UINT uOff, PDWORD pData, UINT uCount)
{
	StartFrame(3);
		
	AddWord( uOff - LONG_OFFSET );
	
	AddWord(2); // 2 16 bit words
	
	if( Transact(FALSE) ) {

		pData[0] = MotorToHost(*PDWORD(DWORD(m_pRx[3])));

		return 1;
		}
		
	if( (m_pRx[1] & 0x80) && IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return 1;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CEmersonSPPDriver::DoWordWrite(UINT uOff, PDWORD pData, UINT uCount)
{
	StartFrame(6);
		
	AddWord( uOff - WORD_OFFSET );
			
	AddWord(LOWORD(pData[0]));

	return Transact(TRUE) ? 1 : CCODE_ERROR;
	}

CCODE CEmersonSPPDriver::DoLongWrite(UINT uOff, PDWORD pData, UINT uCount)
{
	StartFrame(16);

	AddWord( uOff - LONG_OFFSET );

	AddWord(2);

	AddByte(4);

	AddLong(pData[0]);

	return Transact(TRUE) ? 1 : CCODE_ERROR;
	}

// Helpers

// End of File
