
#include "intern.hpp"

#include "matfp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Driver
//

// Instantiator

INSTANTIATE(CMATFPDriver);

// Constructor

CMATFPDriver::CMATFPDriver(void)
{
	m_Ident	      = DRIVER_ID;

	CTEXT Hex[]   = "0123456789ABCDEF";

	m_pHex	      = Hex;
	}

// Destructor

CMATFPDriver::~CMATFPDriver(void)
{
	}

// Configuration

void MCALL CMATFPDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CMATFPDriver::CheckConfig(CSerialConfig &Config)
{
	if ( Config.m_uPhysical == 3 ) Make485(Config, TRUE);
	}
	
// Management

void MCALL CMATFPDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMATFPDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CMATFPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS; 
	}

CCODE MCALL CMATFPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMATFPDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);  
	}

CCODE MCALL CMATFPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( ValidMemory(Addr.a.m_Table) ) {

		BOOL fRelayWord = Addr.a.m_Table >= INTERNAL_RELAY_WORD && Addr.a.m_Table <= EXT_OUTP_RELAY_WORD;

		BOOL fLong      = IsLong(Addr.a.m_Type);

		UINT uCt        = fLong ? uCount*2 : (Addr.a.m_Type == addrBitAsBit || fRelayWord ? 1 : uCount);

		UINT uSize      = Addr.a.m_Type == addrBitAsBit ? 1 : ( fLong ? 8 : 4 );

		MakeMin( uCt, 16 );

		PutRead(Addr, uCt);

		if( Transact() ) {

			if( fRelayWord ) {

				pData[0] = GetRelayWord(Addr.a.m_Offset);

				return 1;
				}

			if ( CheckSize(Addr.a.m_Type, uCt) ) {

				for( UINT uScan = 0; uScan < uCount; uScan++ ) {

					PCSTR pText = PCSTR(m_bRxBuff + 5 + uSize * uScan);

					switch( uSize ) {

						case 1:
							pData[uScan] = xtoin(pText, 1);

							break;

						case 4:
							pData[uScan] = SwapBytes(xtoin(pText, 4));

							break;

						case 8:
							pData[uScan] = SwapLong(xtoin(pText, 8));

							break;
						}
					}

				return fLong ? uCt/2 : uCt;
				}
			}
		}
			
	return CCODE_ERROR;
	}

CCODE MCALL CMATFPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if(ValidWrite(Addr.a.m_Table) && ValidMemory(Addr.a.m_Table)) {

		UINT i;

		DWORD dData[16];

		BOOL fRelayWord = Addr.a.m_Table >= INTERNAL_RELAY_WORD && Addr.a.m_Table <= EXT_OUTP_RELAY_WORD;

		if( fRelayWord ) {

			CAddress A;

			A.a.m_Table  = Addr.a.m_Table - 5;
			A.a.m_Type   = addrBitAsBit;

			for( i = 0; i < 16; i++ ) {

				A.a.m_Offset = Addr.a.m_Offset + i;

				dData[0] = (pData[0] >> i) & 1;

				PutWrite( A, 1, &(dData[0]) );

				Transact();
				}

			return 1;
			}

		else {
			BOOL fLong = IsLong(Addr.a.m_Type);

			if( fLong ) {

				MakeMin( uCount, 8 );
				}

			else {
				if( IsSingleBit(Addr.a.m_Table) ) uCount = 1;

				else MakeMin( uCount, 16 );
				}

			UINT j = 0;

			for( i = 0; i < uCount; i++ ) {

				dData[j++] = LOWORD(pData[i]);

				if( fLong ) {

					dData[j++] = HIWORD(pData[i]);
					}
				}

			PutWrite(Addr, fLong ? uCount*2 : uCount, &(dData[0]));
			}

		Transact();
		}

	return IsSingleBit(Addr.a.m_Table) ? 1 : uCount;
	}

void CMATFPDriver::PutRead(AREF Addr, UINT uCount)
{
	StartFrame();

	PutReadCommand( Addr);

	PutMemoryCode( Addr.a.m_Table );

	PutAddress(Addr, uCount);
	}

void CMATFPDriver::PutWrite(AREF Addr, UINT uCount, PDWORD pData)
{
	StartFrame();

	PutWriteCommand(Addr);

	PutMemoryCode( Addr.a.m_Table );
	
	PutAddress(Addr, uCount);

	for( UINT uScan = 0; uScan < uCount; uScan++ ) {

		if( Addr.a.m_Type == addrBitAsBit ) {

			(pData[uScan] > 0) ? AddByte( '1' ) : AddByte( '0' );
			}

		else {
			PutGeneric(SwapBytes(pData[uScan]), 16, 4096);
			}
		}
	}

// Application Layer

void CMATFPDriver::PutReadCommand(AREF Addr)
{
	AddByte( 'R' );

	switch(Addr.a.m_Table)
	{
		case EXT_INP_RELAY:
		case EXT_OUTP_RELAY:
		case INTERNAL_RELAY:
		case SPC_INTL_RELAY:
		case LINK_RELAY:
		case EXT_INP_RELAY_WORD:
		case EXT_OUTP_RELAY_WORD:
		case INTERNAL_RELAY_WORD:
		case SPC_INTL_RELAY_WORD:
		case LINK_RELAY_WORD:
					AddByte( 'C' );
					
					PutUnitCode( Addr.a.m_Type );
					
					break;

		case DATA_REGS:
		case SPEC_DATA_REGS:
		case LINK_DATA_REGS:;
		case FILE_REGS:
		case X_INDEX:
		case Y_INDEX:		
					AddByte( 'D' );	
					
					break;

		case TIMER_CONTACT:			
		case COUNTER_CONTACT:	
					AddByte( 'C' );
					
					PutUnitCode( Addr.a.m_Type );
					
					break;

		case SET_VALUE:		
					AddByte( 'S' );	
					
					break;

		case ELAPSED_VALUE:	
					AddByte( 'K' );	
					
					break;
		}
	
	}

void CMATFPDriver::PutWriteCommand(AREF Addr)
{
	AddByte( 'W' );

	switch(Addr.a.m_Table)
	{
		case EXT_INP_RELAY:		break;
		case EXT_OUTP_RELAY:
		case INTERNAL_RELAY:
		case LINK_RELAY:
					AddByte( 'C' );
					
					PutUnitCode( Addr.a.m_Type );

					break;
		
		case DATA_REGS:
		case LINK_DATA_REGS:
		case FILE_REGS:
		case X_INDEX:
		case Y_INDEX:		
					AddByte( 'D' );	
					
					break;

		case SET_VALUE:		
					AddByte( 'S' );	
					
					break;
		
		case ELAPSED_VALUE:	
					AddByte( 'K' );	
					
					break;
		}
	
	}

void CMATFPDriver::PutUnitCode(WORD wDataType)
{
	wDataType == addrBitAsBit ? AddByte( 'S' ) : AddByte( 'C' );		
	}

void CMATFPDriver::PutMemoryCode(WORD wType)
{
	switch(wType)
	{
		case EXT_INP_RELAY:		AddByte( 'X' );	break;
		case EXT_OUTP_RELAY:		AddByte( 'Y' );	break;
		case INTERNAL_RELAY:		AddByte( 'R' );	break;
		case SPC_INTL_RELAY:		AddByte( 'R' );	break;
		case LINK_RELAY:		AddByte( 'L' );	break;
		case EXT_INP_RELAY_WORD:	AddByte( 'X' );	break;
		case EXT_OUTP_RELAY_WORD:	AddByte( 'Y' );	break;
		case INTERNAL_RELAY_WORD:	AddByte( 'R' );	break;
		case SPC_INTL_RELAY_WORD:	AddByte( 'R' );	break;
		case LINK_RELAY_WORD:		AddByte( 'L' );	break;
		case DATA_REGS:			AddByte( 'D' );	break;
		case SPEC_DATA_REGS:		AddByte( 'D' );	break;
		case LINK_DATA_REGS:		AddByte( 'L' );	break;
		case FILE_REGS:			AddByte( 'F' );	break;
		case X_INDEX:			AddByte( 'I' );
						AddByte( 'X' );	break;
		case Y_INDEX:			AddByte( 'I' );
						AddByte( 'Y' ); break;
		case TIMER_CONTACT:		AddByte( 'T' );	break;
		case COUNTER_CONTACT:		AddByte( 'C' );	break;
		case SET_VALUE:					break;
		case ELAPSED_VALUE:				break;
		}
	}

void CMATFPDriver::PutAddress(AREF Addr, UINT uCount)
{
	switch(Addr.a.m_Table)
	{
		case EXT_INP_RELAY:
		case EXT_OUTP_RELAY:
		case INTERNAL_RELAY:
		case SPC_INTL_RELAY:
		case LINK_RELAY:

			PutRelayAddress(Addr.a.m_Offset, 100);

			break;

		case EXT_INP_RELAY_WORD: // Offset is start address.
		case EXT_OUTP_RELAY_WORD:
		case INTERNAL_RELAY_WORD:
		case SPC_INTL_RELAY_WORD:
		case LINK_RELAY_WORD:

			PutGeneric(Addr.a.m_Offset/16, 16, 0x1000);

			PutGeneric( (Addr.a.m_Offset + 15) / 16, 16, 0x1000);

			break;

		case DATA_REGS:	
		case SPEC_DATA_REGS:
		case LINK_DATA_REGS:
		case FILE_REGS:
			
			PutGeneric(Addr.a.m_Offset, 10, 10000);

			PutGeneric(Addr.a.m_Offset + uCount - 1, 10, 10000);

			break;

		case X_INDEX:
		case Y_INDEX:
			
			PutGeneric(Addr.a.m_Offset, 10, 1000);

			PutGeneric(Addr.a.m_Offset, 10, 10000);

			break;

		case TIMER_CONTACT:
		case COUNTER_CONTACT:
			
			PutGeneric(Addr.a.m_Offset, 10, 1000);

			break;

		case SET_VALUE:
		case ELAPSED_VALUE:
			
			PutGeneric(Addr.a.m_Offset, 10, 1000);

			PutGeneric(Addr.a.m_Offset + uCount - 1, 10, 1000);

			break;

		default:	break;
		}
	}

void CMATFPDriver::PutRelayAddress(WORD wAddr, UINT uFactor)
{
	BYTE bHi = HIBYTE(wAddr);

	BYTE bLo = LOBYTE(wAddr);

	AddByte(m_pHex[(bHi / 16)]);

	AddByte(m_pHex[(bHi % 16)]);

	AddByte(m_pHex[(bLo / 16)]);

	AddByte(m_pHex[(bLo % 16)]);

	}

BOOL CMATFPDriver::ValidMemory(WORD wType)
{
	switch(wType)
	{
		case EXT_INP_RELAY:		break;
		case EXT_OUTP_RELAY:		break;
		case INTERNAL_RELAY:		break;
		case SPC_INTL_RELAY:		break;
		case LINK_RELAY:		break;
		case EXT_INP_RELAY_WORD:	break;
		case EXT_OUTP_RELAY_WORD:	break;
		case INTERNAL_RELAY_WORD:	break;
		case SPC_INTL_RELAY_WORD:	break;
		case LINK_RELAY_WORD:		break;
		case DATA_REGS:			break;
		case SPEC_DATA_REGS:		break;
		case LINK_DATA_REGS:		break;
		case FILE_REGS:			break;
		case X_INDEX:			break;
		case Y_INDEX:			break;
		case TIMER_CONTACT:		break;
		case COUNTER_CONTACT:		break;
		case SET_VALUE:			break;
		case ELAPSED_VALUE:		break;
		default:
	    		return FALSE;
		}

	return TRUE;
	}

BOOL CMATFPDriver::ValidWrite(WORD wType)
{
	switch(wType)
	{
		case EXT_INP_RELAY:		return FALSE;
		case EXT_OUTP_RELAY:		break;
		case INTERNAL_RELAY:		break;
		case SPC_INTL_RELAY:		return FALSE;
		case LINK_RELAY:		break;
		case EXT_INP_RELAY_WORD:	return FALSE;
		case EXT_OUTP_RELAY_WORD:	break;
		case INTERNAL_RELAY_WORD:	break;
		case SPC_INTL_RELAY_WORD:	return FALSE;
		case LINK_RELAY_WORD:		break;
		case DATA_REGS:			break;
		case SPEC_DATA_REGS:		return FALSE;
		case LINK_DATA_REGS:		break;
		case FILE_REGS:			break;
		case X_INDEX:			break;
		case Y_INDEX:			break;
		case TIMER_CONTACT:		return FALSE;
		case COUNTER_CONTACT:		return FALSE;
		case SET_VALUE:			break;
		case ELAPSED_VALUE:		break;
		default:
	    		return FALSE;
		}

	return TRUE;
	}

BOOL CMATFPDriver::IsSingleBit(WORD wType)
{
	switch(wType)
	{
		case EXT_INP_RELAY:
		case EXT_OUTP_RELAY:
		case INTERNAL_RELAY:
		case SPC_INTL_RELAY:
		case LINK_RELAY:		return TRUE;
		case DATA_REGS:			break;
		case SPEC_DATA_REGS:		break;
		case LINK_DATA_REGS:		break;
		case FILE_REGS:			break;
		case X_INDEX:			break;
		case Y_INDEX:			break;
		case TIMER_CONTACT:
		case COUNTER_CONTACT:		return TRUE;
		case SET_VALUE:			break;
		case ELAPSED_VALUE:		break;
		default:
	    		return FALSE;
		}

	return FALSE;
	}

// Data Link Layer Code

void CMATFPDriver::StartFrame(void)
{
	m_wPtr = 0;

	m_bCheck = 0;
		
	AddByte( '%' );
		
	AddByte( m_pHex[m_pCtx->m_bDrop / 16] );
	AddByte( m_pHex[m_pCtx->m_bDrop % 16] );

	AddByte( '#' );
	}
	
void CMATFPDriver::SendFrame(void)
{
	m_pData->ClearRx();

	m_bTxBuff[m_wPtr++] = m_pHex[m_bCheck / 16];
	m_bTxBuff[m_wPtr++] = m_pHex[m_bCheck % 16];
	m_bTxBuff[m_wPtr++] = CR;

	m_pData->Write( PCBYTE(m_bTxBuff), m_wPtr, FOREVER);

//	TxByte( m_pHex[m_bCheck / 16] );
//	TxByte( m_pHex[m_bCheck % 16] );
//	TxByte(CR);
	}

BOOL CMATFPDriver::GetFrame(void)
{
	UINT uState = 0;

	UINT uByte = 0;

	UINT uTimer = 0;
	
	m_uCount   = 0;	

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uByte = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == '%' )
					uState = 1;
				break;
				
			case 1:
				if( uByte == CR ) {
				
					if( CheckReply(m_uCount) ) {
						
						return TRUE;
						}
					
					return FALSE;
					}
				
				m_bRxBuff[m_uCount++] = uByte;
				
				if( m_uCount == sizeof(m_bRxBuff) ) {
					
					return FALSE;
					}
					
				break;
			}
		}
		
	return FALSE;
	}
	

BOOL CMATFPDriver::CheckReply(UINT uCount)
{
	BYTE bCheck = '%';
	
	for( UINT uScan = 0; uScan < uCount - 2; uScan++ )  {

		bCheck ^= m_bRxBuff[uScan];
		}

	if( xtoin(PSTR(m_bRxBuff + uScan), 2) == bCheck ) {

		if( m_bRxBuff[2] == '!' )
			return FALSE;

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CMATFPDriver::CheckSize(WORD wDataType, UINT uCount)
{
	if( wDataType == addrBitAsBit ) {

		UINT uTarget = 5 + uCount + 2;
	
		return (uTarget == m_uCount);				
		}
	else {
		UINT uTarget = 5 + uCount * 4 + 2;
	
		return (uTarget == m_uCount);				
		}
	}
	
BOOL CMATFPDriver::Transact(void)
{
	m_pData->ClearRx();

	SendFrame();
				
	return GetFrame();
	}

DWORD CMATFPDriver::GetRelayWord(UINT uOffset)
{
	DWORD dData[2] = {0};

	UINT uStart = uOffset % 16;

	PCSTR pText;

	for( UINT i = 0; i <= (uStart+15)/16; i++ ) {

		pText = PCSTR(m_bRxBuff + 5 + 4 * i);

		dData[i] = (SwapBytes(xtoin(pText,4)));
		}

	return ( (dData[0] >> uStart) | (dData[1] << (16 - uStart)) ) & 0xFFFF;
	}

// Port Access

void CMATFPDriver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CMATFPDriver::RxByte( void )
{
	UINT bData;

	bData = m_pData->Read(100);

	return bData;
	}

// Helpers

void CMATFPDriver::AddByte(BYTE bData)
{
	m_bTxBuff[m_wPtr++] = bData;

	m_bCheck ^= bData;
	}

DWORD CMATFPDriver::xtoin(PCTXT pText, UINT uCount)
{
	DWORD dData = 0;
	
	while( uCount-- ) {
	
		char cData = *(pText++);
		
		if( cData >= '0' && cData <= '9' )
			dData = 16 * dData + cData - '0';

		else if( cData >= 'A' && cData <= 'F' )
			dData = 16 * dData + cData - 'A' + 10;

		else if( cData >= 'a' && cData <= 'f' )
			dData = 16 * dData + cData - 'a' + 10;

		else break;
		}

	return dData;
	}

DWORD CMATFPDriver::SwapBytes(WORD wData)
{
	DWORD dData;

	dData = (wData << 8) & 0xFF00;

	dData |= ( (wData & 0xFF00) >> 8 );
		
	return dData;
	}

void CMATFPDriver::PutGeneric(DWORD dData, UINT uRadix, UINT uFactor)
{
	while( uFactor ) {
	
		AddByte(m_pHex[(dData / uFactor) % uRadix]);

		uFactor /= uRadix;
		}
	}

DWORD CMATFPDriver::SwapLong(DWORD dData)
{
	return SwapBytes(HIWORD(dData)) + (SwapBytes(LOWORD(dData)) << 16);
	}

BOOL CMATFPDriver::IsLong(UINT uType)
{
	return uType == addrWordAsLong || uType == addrWordAsReal;
	}

// End of File
