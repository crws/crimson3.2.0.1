
//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP
//

// Data Spaces

#define DATA_REGS		1
#define SPEC_DATA_REGS		2
#define LINK_DATA_REGS		3
#define FILE_REGS		4
#define X_INDEX			5
#define Y_INDEX			6
#define SET_VALUE		7
#define ELAPSED_VALUE		8
#define COUNTER_CONTACT		9
#define TIMER_CONTACT		10
#define INTERNAL_RELAY		11
#define SPC_INTL_RELAY		12
#define LINK_RELAY		13
#define EXT_INP_RELAY		14
#define EXT_OUTP_RELAY		15
#define INTERNAL_RELAY_WORD	16
#define SPC_INTL_RELAY_WORD	17	
#define LINK_RELAY_WORD		18
#define EXT_INP_RELAY_WORD	19
#define EXT_OUTP_RELAY_WORD	20

class CMATFPDriver : public CMasterDriver {

	public:
		// Constructor
		CMATFPDriver(void);

		// Destructor
		~CMATFPDriver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
			
		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);
		CCODE MCALL Read(AREF Addr, PDWORD pData, UINT uCount);
		CCODE MCALL Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			};
		CContext *	m_pCtx;

		// Data Members
		LPCTXT	m_pHex;

		BYTE	m_bTxBuff[256];
		BYTE	m_bRxBuff[256];
		UINT	m_wPtr;
		UINT	m_uCount;
		BYTE	m_bCheck;
		
		// Implementation

		void PutRead(AREF Addr,UINT uCount);
		void PutWrite(AREF Addr, UINT uCount, PDWORD pData);

		void PutReadCommand(AREF Addr);
		void PutWriteCommand(AREF Addr);

		void PutUnitCode(WORD wDataType);
		void PutMemoryCode(WORD wType);
		void PutAddress(AREF Addr, UINT uCount);
		void PutRelayAddress(WORD wAddr, UINT uFactor);

		BOOL ValidMemory(WORD wType);
		BOOL ValidWrite(WORD wType);
		BOOL IsSingleBit(WORD wType);

		void StartFrame( void );
		void SendFrame( void );
		BOOL GetFrame( void );
		BOOL CheckReply(UINT uCount);
		BOOL CheckSize(WORD wDataType, UINT uCount);
		BOOL Transact( void );
		DWORD GetRelayWord(UINT uOffset);

		// Data Link Layer
		void TxByte(BYTE bData);
		UINT RxByte(void);

		// Helpers
		void AddByte( BYTE bData );
		DWORD xtoin(PCTXT pText, UINT uCount);
		DWORD SwapBytes(WORD wData);
		void PutGeneric(DWORD dData, UINT uRadix, UINT uFactor);
		DWORD SwapLong(DWORD dData);
		BOOL IsLong(UINT uType);
	};

// End of File
