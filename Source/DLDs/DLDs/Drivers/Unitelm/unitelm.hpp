
#include "unitbase.hpp"

#define REPEAT_DONE 3

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Master Driver

class CUnitelwaymDriver : public CUnitelwayBaseDriver
{
	public:
		// Constructor
		CUnitelwaymDriver(void);

		// Destructor
		~CUnitelwaymDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Driver Data
		BYTE	m_ThisDrop;
		BYTE	m_Category;

		// Device Data
		struct CContext
		{
			UINT m_Drop;
			UINT m_OldCategory; // Dummy for old database support
			};
		CContext *	m_pCtx;

		BOOL	m_fPollOnly;
		BOOL	m_fActive[32];
		UINT	m_uThisActive;
		UINT	m_uThisInactive;
		UINT	m_uTrackInactive;
		UINT	m_uMaxPoll;
		UINT	m_uPoll;
		UINT	m_uActiveRepeat;
		BOOL	m_fPing;

		// Poll Handling
		void	PollNext(void);
		void	DoPollNext(UINT uPoll);
		void	PollThis(BOOL fActive);
	
		// Opcode Handlers
		BOOL BitRead   (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL ByteRead  (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL TCByteRead(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL WordRead  (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL LongRead  (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL BitWrite  (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL ByteWrite (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL WordWrite (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL LongWrite (AREF Addr, PDWORD pData, UINT * pCount);

		// Transport Layer
		BOOL Transact(BOOL fWait);
		BOOL Send(BOOL fWait);
		void SendPoll(BYTE bStation);

		// Helpers
		UINT Receive(UINT uType);
	};

// End of File
