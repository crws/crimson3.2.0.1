
#include "intern.hpp"

#include "unitelm.hpp"
	
//////////////////////////////////////////////////////////////////////////
//
// Unitelwaym Driver
//

// Instantiator

INSTANTIATE(CUnitelwaymDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CUnitelwaymDriver::CUnitelwaymDriver(void)
{
	m_Ident          = DRIVER_ID;

	m_pOpFrame       = &m_OpFrame;

	m_fPollOnly      = FALSE;

	m_uPoll          = 1;

	m_uMaxPoll       = 8;

	m_uThisActive    = 0;

	m_uThisInactive  = 0;

	m_uTrackInactive = 0;

	m_uActiveRepeat  = 0;

	memset( m_fActive, 0, sizeof(m_fActive) );

	m_fPing          = TRUE;
	}

// Destructor

CUnitelwaymDriver::~CUnitelwaymDriver(void)
{
	}

// Configuration

void MCALL CUnitelwaymDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_ThisDrop = GetWord(pData);

		m_Category = GetWord(pData) == 0 ? 7 : 6;
		} 
	}
	
void MCALL CUnitelwaymDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CUnitelwaymDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CUnitelwaymDriver::Open(void)
{

	}

// Device

CCODE MCALL CUnitelwaymDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_Drop = GetWord(pData);

			m_pCtx->m_OldCategory = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CUnitelwaymDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CUnitelwaymDriver::Ping(void)
{
	CAddress Addr;

	DWORD Data;

	Addr.a.m_Table  = MWORD;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	m_fPollOnly = FALSE;
	m_uPoll     = 1;
	m_fPing     = TRUE;

	if( Read(Addr, &Data, 1 ) != 1 ) {

		return CCODE_ERROR;
		}

	m_fPing = FALSE;

	m_fActive[0] = 0;

	m_fActive[m_ThisDrop] = 1;

	m_fActive[m_pCtx->m_Drop] = 1;

	m_uMaxPoll = max( m_uMaxPoll, 1 );

	m_uMaxPoll = min( m_uMaxPoll, 31 );

	for( UINT i = 1; i <= m_uMaxPoll; i++ ) {

		if( m_ThisDrop != i && m_pCtx->m_Drop != i ) {

			m_uPoll = i;

			DoPollNext(i);
			}
		}

	return 1;
	}

CCODE MCALL CUnitelwaymDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == POLL_ONLY ) {

		*pData = (DWORD)(m_uMaxPoll);

		PollNext();

		return 1;
		}

	BOOL fSuccess = FALSE;

	m_pOpFrame->bThatDrop = m_pCtx->m_Drop;

	m_pOpFrame->bThisDrop = m_ThisDrop;

	m_pOpFrame->uCategory = m_Category;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			fSuccess = BitRead( Addr, pData, &uCount );

			break;

		case addrByteAsByte:

			if( Addr.a.m_Table <= TPRS ) {

				fSuccess = ByteRead(Addr, pData, &uCount );
				}

			else {

				fSuccess = TCByteRead(Addr, pData, &uCount );
				}
			break;

		case addrWordAsWord:

			fSuccess = WordRead( Addr, pData, &uCount );

			break;

		case addrLongAsLong:
		case addrRealAsReal:

			fSuccess = LongRead( Addr, pData, &uCount );

			break;
		}

	if( !m_fPing ) {

		PollNext();

		}

	return fSuccess ? uCount : CCODE_ERROR;
	}

CCODE MCALL CUnitelwaymDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace0("\r\n** WRITE **");

	if( Addr.a.m_Table == POLL_ONLY ) {

		if( (*pData >= 1L) && (*pData < 32L) ) {

			m_uMaxPoll = LOWORD(*pData);
			}

		PollNext();

		return 1;
		}

	m_pOpFrame->bThatDrop = m_pCtx->m_Drop;

	m_pOpFrame->bThisDrop = m_ThisDrop;

	m_pOpFrame->uCategory = m_Category;

	if( Addr.a.m_Table == CWORD || Addr.a.m_Table == CLONG || Addr.a.m_Table == CREAL ) {

		PollNext();

		return uCount;
		}

	BOOL fSuccess = FALSE;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			fSuccess = BitWrite( Addr, pData, &uCount );

			break;

		case addrByteAsByte:

			fSuccess = ByteWrite( Addr, pData, &uCount );

			break;

		case addrWordAsWord:

			fSuccess = WordWrite( Addr, pData, &uCount );

			if( !fSuccess && m_pOpFrame->bSeg == TCSEG && m_bRx[7] ) {

				fSuccess = TRUE;
				}

			break;

		case addrLongAsLong:
		case addrRealAsReal:

			fSuccess = LongWrite( Addr, pData, &uCount );

			break;
		}

	PollNext();

	return fSuccess ? uCount : CCODE_ERROR;
	}

void CUnitelwaymDriver::PollNext(void)
{
	m_uTrackInactive = (m_uTrackInactive + 1) % 10;

	PollThis(m_uTrackInactive != 0);
	}

void CUnitelwaymDriver::DoPollNext(UINT uPoll)
{
	UINT uSave = m_uPoll;

	m_uPoll = uPoll;

	m_pOpFrame->bThatDrop = uPoll;

	m_pOpFrame->bThisDrop = m_ThisDrop;

	m_pOpFrame->uCategory = m_Category;

	m_fPollOnly = TRUE;

	Send(FALSE);

	m_uPoll = uSave;

	m_fPollOnly = FALSE;
	}

void CUnitelwaymDriver::PollThis(BOOL fActive)
{
	UINT uPoll = min( (fActive ? m_uThisActive : m_uThisInactive), m_uMaxPoll );

	UINT uEnd  = uPoll;

	BOOL fDone = FALSE;

	while( !fDone ) {

		uPoll = (uPoll + 1) % (m_uMaxPoll + 1);

		if( uPoll != m_ThisDrop /*&& uPoll != m_pCtx->m_Drop*/ ) {

			if( m_fActive[uPoll] == fActive ) {

				DoPollNext(uPoll);

				fDone = TRUE;
				}
			}

		if( uPoll == uEnd ) {

			fDone = TRUE;
			}
		}

	if( fActive ) m_uThisActive = uPoll;

	else m_uThisInactive = uPoll;
	}

// Opcode Handlers

BOOL CUnitelwaymDriver::BitRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uCount = FormBitRead(Addr, *pCount);

	UINT uStart = Addr.a.m_Offset % 8;

	if( Transact(FALSE) ) {

		if( GetBitsResponse(pData, &uCount, uStart) ) {

			*pCount = uCount;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUnitelwaymDriver::ByteRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	m_pOpFrame->bOp = Addr.a.m_Table == MBYTE ? OPRDI : OPRDS;

	UINT uCount = *pCount;

	MakeMin( uCount, 8 );

	for( UINT i = 0; i < uCount; i++ ) {

		PutShortHeader( (Addr.a.m_Offset + i) * 8 );

		if( !Transact(FALSE) || m_bRx[0] != ( 0x30 + m_pOpFrame->bOp ) ) {

			return FALSE;
			}

		pData[i] = m_bRx[1];
		}

	*pCount = uCount;
		
	return TRUE;
	}

BOOL CUnitelwaymDriver::TCByteRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	FormTCByteRead(Addr);

	if( !Transact(FALSE) || m_bRx[0] != ( 0x30 + m_pOpFrame->bOp ) ) {

		return FALSE;
		}

	if( !m_bRx[7] ) {

		*pData = m_bRx[8];

		*pCount = 1;
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CUnitelwaymDriver::WordRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uCount = FormWordRead(Addr, *pCount);

	if( Transact(FALSE) ) {

		if( GetWordResponse( pData, &uCount ) ) {

			*pCount = uCount;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUnitelwaymDriver::LongRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uCount = FormLongRead(Addr, *pCount);

	if( Transact(Addr.a.m_Table == MREAL || Addr.a.m_Table == CREAL) ) {

		if(GetLongResponse(pData, &uCount) ) {

			*pCount = uCount;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUnitelwaymDriver::BitWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	FormBitWrite(Addr);

	AddByte(pData[0] & 1 ? 1 : 0 );
	
	if( Transact(FALSE) ) {
	
		if( m_bRx[0] == 0xFE ) {

			*pCount = 1;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUnitelwaymDriver::ByteWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	if( Addr.a.m_Table >= TPRS ) { // TC Byte information is Read Only

		return TRUE;
		}

	m_pOpFrame->bOp = Addr.a.m_Table == MBYTE ? OPWRI : OPWRS;

	for( UINT i = 0; i < 8; i++ ) {

		PutShortHeader( (Addr.a.m_Offset * 8) + i );

		AddByte( pData[0] & ( 1 << i ) ? 1 : 0 );
	
		if( !Transact(FALSE) || m_bRx[0] != 0xFE ) {

			return FALSE;
			}
		}

	*pCount = 1;

	return TRUE;
	}

BOOL CUnitelwaymDriver::WordWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	if( Addr.a.m_Table == TVAL ) return TRUE; // T Value is Read Only

	UINT uCount = FormWordWrite(Addr, *pCount);
	
	for( UINT i = 0; i < uCount; i++ ) {

		AddData(LOWORD(pData[i]) );
		}
	
	if( Transact(FALSE) ) {

		*pCount = uCount;

		if( m_pOpFrame->bSeg == TCSEG ) {

			return (!m_bRx[7]) && ( m_bRx[0] == (0x30 + m_pOpFrame->bOp) );
			}
	
		else return m_bRx[0] == 0xFE;
		}
		
	return FALSE;
	}

BOOL CUnitelwaymDriver::LongWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uCount = FormLongWrite(Addr, *pCount);
	
	for( UINT i = 0; i < uCount; i++ ) {

		AddData(LOWORD(pData[i]) );
		AddData(HIWORD(pData[i]) );
		}
	
	if( Transact(FALSE) ) {
	
		if( m_bRx[0] == 0xFE ) {

			*pCount = uCount;

			return TRUE;
			}
		}
		
	return FALSE;
	}

// Transport Layer

BOOL CUnitelwaymDriver::Transact(BOOL fWait)
{
	return Send(fWait);
	}

BOOL CUnitelwaymDriver::Send(BOOL fWait)
{
	m_pData->ClearRx();	// clear Rx buffer anew

	UINT uState = m_fPollOnly ? 3 : 0;

	UINT uCount = 0;

	BYTE bDrop  = 0;

	for(;;) {
		
		switch( uState ) {

			case 0:
				SendPoll(m_pOpFrame->bThatDrop);

				switch( Receive(TYPE_POLL1) ) {
				
					case DLL_EOT:

						uState = 1;
						uCount = 0;
						break;
						
					case DLL_FRAME:

						if( uCount++ < RETRIES ) {

							Put(ACK);

							return TRUE;
							}

						else {
							return FALSE;
							}

						break;
						
					case DLL_ERROR:

						if( uCount++ < RETRIES ) {

							Put(NAK);
							}

						else {
							return FALSE;
							}

						break;

					case DLL_NULL:

						m_fActive[m_pOpFrame->bThatDrop] = FALSE;

						return FALSE;

					case DLL_RSP_ECHOED: // PLC echoed response, retry

						Put(ACK);

						Sleep(5);

						SendPoll(m_pOpFrame->bThatDrop);

						Receive(TYPE_POLL1);

						uState = 0;

						break;

					default:

						return FALSE;
					}

				break;
				
			case 1:
				SendFrame();

				switch( Receive(TYPE_REPLY) ) {

					case DLL_NAK:

						if( uCount++ >= RETRIES ) {

							Put(EOT);

							return FALSE;
							}
						break;
						
					case DLL_ACK:
						uState = 2;
						uCount = 0;
						break;

					case DLL_NULL:
					default:
						m_fActive[m_pOpFrame->bThatDrop] = FALSE;

						return FALSE;
					}
				break;
				
			case 2:
				SendPoll(m_pOpFrame->bThatDrop);

				switch( Receive(TYPE_POLL2) ) {
				
					case DLL_EOT:

						if( uCount++ < RETRIES ) {

							Sleep(NET_POLL_GAP);
							}

						else {
							return FALSE;
							}

						break;
					
					case DLL_ERROR:

						if( uCount++ < RETRIES ) {

							Put(NAK);
							}

						else {
							return FALSE;
							}
						break;

					case DLL_FRAME:

						Put(ACK);

						SendPoll(m_pOpFrame->bThatDrop);

						if( Receive(TYPE_POLL1) != DLL_EOT ) { // ACK not always seen

							Put(ACK);

							Sleep(5);

							SendPoll(m_pOpFrame->bThatDrop);

							Receive(TYPE_POLL1);
							}

						return TRUE;
						
					default:
						return FALSE;
					}
				break;

			case 3:
				bDrop = m_pOpFrame->bThatDrop;

				SendPoll(bDrop);

				switch( Receive(TYPE_POLL1) ) {

					case DLL_EOT: // slave sending nothing to PLC

						m_fActive[bDrop] = TRUE;

						break;

					case DLL_FRAME: // slave sent frame to PLC, PLC Acked, Slave EOT'd
					case DLL_ACK:
					case DLL_NAK:
					case DLL_ERROR:

						m_fActive[bDrop] = TRUE;

						m_uActiveRepeat = REPEAT_DONE;

						SendPoll(m_pOpFrame->bThatDrop); // drop updated in Receive

						Receive(TYPE_REPLY);

						break;

					default:
						m_fActive[bDrop] = FALSE;

						break;
					}

				return TRUE;						
			}
		}
	}

void CUnitelwaymDriver::SendPoll(BYTE bStation)
{
//**/	AfxTrace0("\r\n");

	Put(DLE);

	Put(ENQ);

	Put(bStation);
	}

// Helpers

UINT CUnitelwaymDriver::Receive(UINT uType)
{
	BOOL fTimeout = TRUE;

	UINT uLength = 0;
	UINT uCheck = 0;
	UINT uPtr = 0;

	UINT uPause = 0;

	UINT uState = 0;

	BYTE bHead[8] = {0};

	switch( uType ) {
	
		case TYPE_REPLY:
			SetTimer( DLL_REPLY_TIMEOUT );
			break;

		case TYPE_POLL1:
		case TYPE_POLL2:
			SetTimer(DLL_GAP_TIMEOUT*2);
			break;
			
		default:
			fTimeout = FALSE;
			break;
		}

//**/	AfxTrace0("\r\n");

	while( !fTimeout || GetTimer() ) {

		WORD wByte = m_pData->Read( 5 );
	
		if( wByte == LOWORD(NOTHING) ) {

			Sleep(DLL_PAUSE);
			 
			if( ++uPause == DLL_GAP_TIMEOUT ) {
				uPause = 0;
				uState = 0;
				}

			continue;
			}
		else {
			uPause = 0;
			}

		BYTE bByte = LOBYTE(wByte);

//**/		AfxTrace1("<%2.2x>", bByte );

		switch( uState ) {
		
			case 0:
				switch( bByte ) {

					case DLE:
						if( m_fPollOnly ) {

							SetTimer(DLL_REPLY_TIMEOUT);
							}

						uCheck = 0;
						uState = 1;
						break;
						
					case ACK:
						if( m_fPollOnly ) {

							if( uType == TYPE_POLL1 ) {

								uState = 0;
								}

							else {
								return DLL_EOT;
								}
							}

						else {
							if( uType == TYPE_REPLY ) {

								return DLL_ACK;
								}
							}
						break;
						
					case NAK:
						if( uType == TYPE_REPLY || m_fPollOnly ) {

							return DLL_NAK;
							}

						break;
						
					case EOT:
						if( uType != TYPE_REPLY || m_fPollOnly ) {

							return DLL_EOT;
							}
						break;
					}
				break;
				
			case 1:
				switch( bByte ) {
				
					case ENQ:
						uState = 2;
						break;
						
					case STX:
						uState = 3;
						break;
						
					default:
						uState = 0;
						break;
					}
				break;
			
			case 2:
				if( uType == TYPE_POLL1 || uType == TYPE_POLL2 ) {

					uState = 0;
					}

				else {
					return DLL_ENQ;
					}
				break;
				
			case 3:
				uState = 4;

				uPtr   = 0;

				if( m_fPollOnly ) {

					m_pOpFrame->bThatDrop = bByte;
					}
				break;
				
			case 4:
				if( bByte == DLE ) {

					uState = 5;
					}

				else {

					uLength = bByte;
					uState  = 6;
					}
				break;
			
			case 5:
			 	if( bByte == DLE ) {

					uLength = bByte;
					uState  = 6;
					}

				else {
					uState = 0;
					}
				break;
				
			case 6:
				if( bByte == DLE ) {

					uState = 7;
					}

				else {
					if( uPtr < 8 ) {

						bHead[uPtr++] = bByte;
						}

					else {
						m_bRx[uPtr-8] = bByte;

						uPtr++;
						}
					
					if( uPtr == uLength ) {

						uState = 8;
						}
					}
				break;
				
			case 7:
				if( bByte == DLE ) {

					if( uPtr < 8 ) {

						bHead[uPtr++] = bByte;
						}

					else {
						m_bRx[uPtr-8] = bByte;

						uPtr++;
						}

					uState = uPtr == uLength ? 8 : 6;
					}

				else {
					uState = 0;
					}

				break;
				
			case 8:
				if( m_fPollOnly ) {

					uState = 10;
					}

				else {
					switch( uType ) {

						case TYPE_POLL1:

							if( bHead[1] == m_pOpFrame->bThisDrop &&
							    bHead[3] == m_pOpFrame->bThatDrop
							    ) {
								return DLL_RSP_ECHOED;
								}

							uState = 10;

							break;

						case TYPE_POLL2:

							if(	bHead[1] == m_pOpFrame->bThisDrop &&
								bHead[3] == m_pOpFrame->bThatDrop &&
								bHead[6] == 0xF0 &&
								bHead[7] == m_pOpFrame->Transaction					    
								) {

								return bByte == LOBYTE(uCheck) ? DLL_FRAME : DLL_ERROR;
								}

							uState = 10;

							break;

						case TYPE_REPLY:
							uState = 0;
							break;
						}
					}

				break;

			case 9:
				return DLL_FRAME;

			case 10:

				return DLL_ACK; // should be ACK from Slave to PLC.

			}

		uCheck = uCheck + bByte;
		}
		
	return DLL_NULL;
	}

// End of File
