
#include "intern.hpp"

#include "unitbase.hpp"
	
//////////////////////////////////////////////////////////////////////////
//
// Unitelway Base Driver

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CUnitelwayBaseDriver::CUnitelwayBaseDriver(void)
{
	}

// Destructor

CUnitelwayBaseDriver::~CUnitelwayBaseDriver(void)
{
	}

// Frame Building

void CUnitelwayBaseDriver::PutShortHeader(UINT uOffset)
{
	Preamble();

	AddByte(m_pOpFrame->bOp);
	AddByte(m_pOpFrame->uCategory);

	AddData( uOffset );
	}

void CUnitelwayBaseDriver::PutWordsHeader(UINT uOffset, UINT uCount)
{
	Preamble();

	AddByte(m_pOpFrame->bOp);
	AddByte(m_pOpFrame->uCategory);
	AddByte(m_pOpFrame->bSeg);
	AddByte(m_pOpFrame->bType);

	AddData( uOffset );

	AddData( uCount );
	}

void CUnitelwayBaseDriver::PutTCHeader(UINT uOffset)
{
	Preamble();

	AddByte(m_pOpFrame->bOp);
	AddByte(m_pOpFrame->uCategory);
	AddByte(m_pOpFrame->bSeg);
	AddByte(m_pOpFrame->bType);

	AddData( uOffset );

	AddByte( 2 );
	AddByte(m_pOpFrame->bItem);
	}

UINT CUnitelwayBaseDriver::FormBitRead(AREF Addr, UINT uCount)
{
	m_pOpFrame->bOp = Addr.a.m_Table == MBIT ? OPRDI : OPRDS;

	PutShortHeader( Addr.a.m_Offset );

	MakeMin( uCount, UINT(8 - Addr.a.m_Offset % 8) );

	return uCount;
	}

void CUnitelwayBaseDriver::FormTCByteRead(AREF Addr)
{
	m_pOpFrame->bOp  = OPTCR;

	m_pOpFrame->bSeg = TCSEG;

	FillOpFrame( Addr.a.m_Table );

	PutTCHeader( Addr.a.m_Offset );
	}

UINT CUnitelwayBaseDriver::FormWordRead(AREF Addr, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if( Addr.a.m_Table < TPRS ) {

		m_pOpFrame->bOp = OPREAD;

		MakeMin( uCount, 16 );
		}

	else {
		m_pOpFrame->bOp = OPTCR;

		uCount = 1;
		}

	FillOpFrame(Addr.a.m_Table);

	if( m_pOpFrame->bSeg == TCSEG ) {

		PutTCHeader( uOffset );
		}

	else PutWordsHeader( uOffset, uCount );

	return uCount;
	}

UINT CUnitelwayBaseDriver::FormLongRead(AREF Addr, UINT uCount)
{
	m_pOpFrame->bOp   = OPREAD;

	MakeMin( uCount, 8 );

	UINT uSendCount = uCount;

	UINT uOffset = Addr.a.m_Offset;

	FillOpFrame(Addr.a.m_Table);

	if( m_pOpFrame->bSeg == SYSSEG ) uSendCount = uCount*2;

	PutWordsHeader( uOffset, uSendCount );

	return uCount;
	}

void CUnitelwayBaseDriver::FormBitWrite(AREF Addr)
{
	m_pOpFrame->bOp = Addr.a.m_Table == MBIT ? OPWRI : OPWRS;

	PutShortHeader( Addr.a.m_Offset );
	}

UINT CUnitelwayBaseDriver::FormWordWrite(AREF Addr, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	if( Addr.a.m_Table < TPRS ) {

		m_pOpFrame->bOp = OPWRITE;

		MakeMin( uCount, 4 );
		}

	else {

		m_pOpFrame->bOp = OPTCW;

		uCount = 1;
		}

	FillOpFrame(Addr.a.m_Table);

	m_pOpFrame->bSeg != TCSEG ? PutWordsHeader( uOffset, uCount ) : PutTCHeader( uOffset );

	return uCount;
	}

UINT CUnitelwayBaseDriver::FormLongWrite(AREF Addr, UINT uCount)
{
	m_pOpFrame->bOp = OPWRITE;

	MakeMin( uCount, 1 );

	UINT uSendCount = uCount;

	UINT uOffset = Addr.a.m_Offset;

	FillOpFrame(Addr.a.m_Table);

	if( m_pOpFrame->bSeg == SYSSEG ) uSendCount = uCount*2;

	PutWordsHeader( uOffset, uSendCount );

	return uCount;
	}

void CUnitelwayBaseDriver::AddByte(BYTE bData)
{
	if( bData == DLE )
		m_bTx[m_uPtr++] = DLE;

	m_bTx[m_uPtr++] = bData;

	m_bTx[3]++;
	}

void CUnitelwayBaseDriver::AddData(UINT uData)
{
	AddByte( LOBYTE(uData) );
	AddByte( HIBYTE(uData) );
	}

// Response Data

BOOL CUnitelwayBaseDriver::GetBitsResponse(PDWORD pData, UINT * pCount, UINT uStart)
{
	if( m_bRx[0] == 0x30 + m_pOpFrame->bOp ) {

		for( UINT i = 0; i < *pCount; i++ ) {

			pData[i] = ( m_bRx[1] & (1 << (uStart+i)) ) ? 1 : 0;
			}
			
		return TRUE;
		}

	return FALSE;
	}

BOOL CUnitelwayBaseDriver::GetWordResponse(PDWORD pData, UINT * pCount)
{
	DWORD dResult = 0;
	UINT uFirstData = 2;

	if( m_pOpFrame->bSeg == TCSEG ) {

		if( m_bRx[7] ) return FALSE; // operation failed

		else uFirstData = 8;
		}
	
	if( m_bRx[0] == 0x30 + m_pOpFrame->bOp ) {

		for( UINT i = 0; i < *pCount; i++, uFirstData+=2 ) {

			dResult = GetData( uFirstData, 2 );

			if( dResult & 0x8000 ) {
					
				dResult |= 0xFFFF0000;
				}

			pData[i] = dResult;
			}
			
		return TRUE;
		}

	return FALSE;
	}

BOOL CUnitelwayBaseDriver::GetLongResponse(PDWORD pData, UINT * pCount)
{
	DWORD dResult = 0;
	UINT i = 0;
	UINT j = 2;

	if( m_bRx[0] == 0x30 + m_pOpFrame->bOp ) {

		for( i = 0; i < *pCount; i++, j+=2 ) {

			dResult = GetData( j, 4 );

			pData[i] = dResult;
			}
			
		return TRUE;
		}

	return FALSE;
	}

// Transport Layer

void CUnitelwayBaseDriver::SendFrame(void)
{
	UINT uScan;
	UINT uCheck = 0;

	for( uScan = 0; uScan < m_uPtr; uScan++ ) {

		if( uScan == 3 && m_bTx[3] == DLE ) {
		
			uCheck += DLE;
			
			Put(DLE);
			}
				
		uCheck += m_bTx[uScan];
		
		Put(m_bTx[uScan]);
		}

	Put(LOBYTE(uCheck));
	}

// Port Access

void CUnitelwayBaseDriver::Put(BYTE b)
{
	m_pData->Write(b, FOREVER);

//**/	if( b == DLE ) AfxTrace0("\r\n"); AfxTrace1("[%2.2x]", b );
	}

UINT CUnitelwayBaseDriver::Get(void)
{
	return m_pData->Read( TIMEOUT );
	}

// Helpers

void CUnitelwayBaseDriver::FillOpFrame(UINT uTable)
{
	switch( uTable ) {

		case TRUN:
			m_pOpFrame->bType = PL7TYPE;
			m_pOpFrame->bItem = ITRUN;
			break;

		case TDONE:
			m_pOpFrame->bType = PL7TYPE;
			m_pOpFrame->bItem = ITDONE;
			break;

		case TMDONE:
			m_pOpFrame->bType = IECTYPE;
			m_pOpFrame->bItem = ITMQ;
			break;

		case CTRE:
			m_pOpFrame->bType = CTRTYPE;
			m_pOpFrame->bItem = ICE;
			break;

		case CTRD:
			m_pOpFrame->bType = CTRTYPE;
			m_pOpFrame->bItem = ICD;
			break;

		case CTRF:
			m_pOpFrame->bType = CTRTYPE;
			m_pOpFrame->bItem = ICF;
			break;

		case MWORD:
			m_pOpFrame->bSeg  = MEMSEG;
			m_pOpFrame->bType = MEMWORD;
			break;

		case SWORD:
			m_pOpFrame->bSeg  = SYSSEG;
			m_pOpFrame->bType = SYSWORD;
			break;

		case CWORD:
			m_pOpFrame->bSeg  = CONSEG;
			m_pOpFrame->bType = CONWORD;
			break;

		case TPRS:
			m_pOpFrame->bSeg  = TCSEG;
			m_pOpFrame->bType = PL7TYPE;
			m_pOpFrame->bItem = ITPRS;
			break;

		case TVAL:
			m_pOpFrame->bSeg  = TCSEG;
			m_pOpFrame->bType = PL7TYPE;
			m_pOpFrame->bItem = ITVAL;
			break;

		case TMPRS:
			m_pOpFrame->bSeg  = TCSEG;
			m_pOpFrame->bType = IECTYPE;
			m_pOpFrame->bItem = ITMPRS;
			break;

		case TMVAL:
			m_pOpFrame->bSeg  = TCSEG;
			m_pOpFrame->bType = IECTYPE;
			m_pOpFrame->bItem = ITMVAL;
			break;

		case CTRP:
			m_pOpFrame->bSeg  = TCSEG;
			m_pOpFrame->bType = CTRTYPE;
			m_pOpFrame->bItem = ICPRS;
			break;

		case CTRV:
			m_pOpFrame->bSeg  = TCSEG;
			m_pOpFrame->bType = CTRTYPE;
			m_pOpFrame->bItem = ICVAL;
			break;

		case MLONG:
		case MREAL:
			m_pOpFrame->bSeg  = MEMSEG;
			m_pOpFrame->bType = MEMLONG;
			break;

		case SLONG:
			m_pOpFrame->bSeg  = SYSSEG;
			m_pOpFrame->bType = SYSWORD; // Longs did not work with test PLC
			break;

		case CLONG:
		case CREAL:
			m_pOpFrame->bSeg  = CONSEG;
			m_pOpFrame->bType = CONLONG;
			break;

		}
	}

void CUnitelwayBaseDriver::Preamble(void)
{
	m_bTx[0] = DLE;
	m_bTx[1] = STX;
	m_bTx[2] = m_pOpFrame->bThatDrop ? m_pOpFrame->bThatDrop : m_pOpFrame->bThisDrop;
	m_bTx[3] = 0;
	
	m_uPtr = 4;

	AddByte(0x20);

	if( m_pOpFrame->bThatDrop == 0 ) { // talk to master

		AddByte(0x00);
		AddByte(0xFE);
		AddByte(0x00);
		AddByte(0xFE);
		AddByte(0x00);
		}

	else { // talk to slave
		AddByte(m_pOpFrame->bThisDrop);
		AddByte(0);
		AddByte(m_pOpFrame->bThatDrop);
		AddByte(0);
		AddByte(8);
		}

	AddByte(0xF9);

	AddByte(++m_pOpFrame->Transaction);
	}

DWORD CUnitelwayBaseDriver::GetData( UINT uPos, UINT uByteCount )
{
	DWORD dResult = 0;

	switch( uByteCount ) {

		case 4:
			dResult += ( (DWORD)(m_bRx[uPos+3]) << 24 );
		case 3:
			dResult += ( (DWORD)(m_bRx[uPos+2]) << 16 );
		case 2:
			dResult += ( (DWORD)(m_bRx[uPos+1]) << 8 );
		case 1:
		default:
			dResult += (DWORD)(m_bRx[uPos]);

			break;
		}

	return dResult;
	}

// End of File
