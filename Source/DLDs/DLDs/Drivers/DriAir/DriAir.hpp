
//////////////////////////////////////////////////////////////////////////
//
// Dri-Air Driver
//

enum {
	CSTAT	=   1,
	CSTOP	=   2,
	CSTRT	=   3,
	CCEQ	=   4,
	CDEW	=   5,
	CION	=   6,
	CIOFF	=   7,
	CLRN	=   8,
	CLOFF	=   9,
	CLON	=  10,
	CMTR	=  11,
	CVLV	=  12,
	CPD1	=  13,
	CPD2	=  14,
	CPT1	=  15,
	CPT2	=  16,
	CRST	=  17,
	CLDON	=  18,
	CLDOFF	=  19,
	CLDT	=  20,
	CLDD	=  21,
	CS1OFF  =  22,
	CS1ON   =  23,
	CS2OFF  =  24,
	CS2ON   =  25,
	CS1D    =  26,
	CS2D    =  27,
	CS1A    =  28,
	CS2A    =  29,
	CS1I    =  30,
	CS2I    =  31,
	CADDR	=  32,

	COLDS	= 235,
	CSCMD	= 236,
	CUCMD	= 237,
	CURSP	= 238,
	};

// Custom User Commands
#define	USRALLOW 16478 // (0x405E)
#define	USRFILL    100 // fill user response when req > 100

// Buffer sizes
#define	SZTX	 32
#define	SZUCMD	 80
#define	SZURSP	128
#define	URSPEND	127
#define	SZUCDW	SZUCMD / 4
#define	SZURDW	SZURSP / 4

// String Quantities
#define	SZREAD	 60

// Max string sizes
#define	SZMAX	 80

// Find Error
#define	FINDERR	1024 // char not found

// Receive States
#define	STNOTSL	1
#define	STWAITE	2
#define	STSTART	5
#define	STSTRT1	STSTART+1
#define	STSTRT2	STSTART+2
#define	STLEARN	8
#define	STLERN1	STLEARN+1
#define	STUSERS	10

// Busy Timeouts
#define	TO5K	 5000
#define	TO15K	15000
#define	TOMAX	0x7FFF

// Status Timeout
#define	TOSTATA	0 // 10000 for periodic read

#define	BIGBOOL	0xFFFFFFFF

// Command Definitions
struct FAR DriAirCmdDef {

	UINT	uID;		// Space number
	char	cOP[17];	// Command String
	};

//////////////////////////////////////////////////////////////////////////
//
// Dri-Air Serial Driver
//

class CDriAirDriver : public CMasterDriver
{
	public:
		// Constructor
		CDriAirDriver(void);

		// Destructor
		~CDriAirDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			UINT	m_uDrop;
			BYTE	Drop[2];
			BOOL	m_fUser;

			// User command storage
			char	m_cUCMD[SZUCMD];
			char	m_cURSP[SZURSP];

			// cached command data
			BOOL	m_fStartStatus;
			BOOL	m_fStopStatus;
			};

		CContext *	m_pCtx;

		// Data Members
		char	m_cTx[SZTX];

		IExtraHelper  * m_pExtra;

		LPCTXT	m_pHex;
		LPCTXT	m_pNet;
		LPCTXT	m_pOn;
		LPCTXT	m_pOff;
		LPCTXT	m_pAct;
		LPCTXT	m_pInh;
		LPCTXT	m_pDis;
		LPCTXT	m_pEnb;
		LPCTXT	m_pDone;
		LPCTXT	m_pStop;
		LPCTXT	m_pCur;
		LPCTXT	m_pLba;
		LPCTXT	m_pMot;
		LPCTXT	m_pVlv;
		LPCTXT	m_pH1S;
		LPCTXT	m_pH2S;
		LPCTXT	m_p1H;
		LPCTXT	m_p2H;

		UINT	m_uPtr;
		UINT	m_uWErrCt;
		UINT	m_uRcvEnd;
		UINT	m_uLearnState;
		UINT	m_uStartState;
		UINT	m_uBusyStart;
		UINT	m_uBusyEnd;
		UINT	m_uStatusStart;
		UINT	m_uStatusEnd;
		UINT	m_uStatusPtr;
		UINT	m_uStatusInterval;
		UINT	m_uUserLine;

		UINT	m_uDelay;

		char *	m_pRx[SZREAD];
		char	m_cRx0[80];

		BOOL	m_fInPing;
		BOOL	m_fBusy;

		// Command Definition Table
		static DriAirCmdDef CODE_SEG CL[];
		DriAirCmdDef FAR * m_pCL;
		DriAirCmdDef FAR * m_pDAItem;

		// Execute Read/Write
		CCODE	DoRead( AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWrite(AREF Addr, PDWORD pData, UINT uCount);
		void	GetOldStatus(void);

		// Frame Building
		void	StartFrame(void);
		void	AddCmdString(void);
		void	EndFrame(void);
		void	AddByte(char cData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dData);
		void	AddText(PCTXT pText);
		void	AddWriteData(PDWORD pData);
		BOOL	WantWriteData(void);

		// Transport
		BOOL	Transact(void);
		BOOL	Send(void);

		// Reply Handling
		BOOL	GetReply(void);
		char *	NextArray(UINT *pItem);
		void	DeallocDA(void);

		// Response Handling
		BOOL	GetReadResponse(PDWORD pData, UINT uOffset, UINT uCount);
		BOOL	GetStatusResponse(PDWORD pData, UINT uOffset, UINT uCount);
		BOOL	GetDewResponse(PDWORD pData, UINT uOffset, UINT uCount);
		BOOL	GetTxtResponse(PDWORD pData, char * pStrT, char *pStrF);
		BOOL	GetProcResponse(PDWORD pData, UINT uOffset, UINT uCount);
		BOOL	GetNumResponse(char * pStr, PDWORD pData, UINT uStart);
		BOOL	GetWriteResponse(void);
		void	GetOldStatusResponse(void);

		// Helpers

		// Command defintion pointer
		BOOL	SetItemPtr(UINT uTable);
		// Internal values
		BOOL	NoReadTransmit( AREF Addr, PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(AREF Addr, PDWORD pData, UINT uCount);

		// Old Command Handling
		BOOL	IsOldStatusRead(void);
		void	ConfigOldStatusRead(BOOL fForce);

		// Response Parsing
		UINT	FindData ( char * pStr, UINT * pPos);
		UINT	FindComma( char * pStr, UINT * pPos);
		UINT	FindSNum(  char * pStr, UINT uStart);
		UINT	FindDAChar(char * pStr, UINT uStart, char cFind);
		// Numeric handling
		BOOL	IsNum(char c);
		BOOL	IsSNum(char c);
		BOOL	IsAlp(char c);
		BOOL	IsAlpOrSNum(char c);
		BOOL	IsPrintable(char c);
		DWORD	GetNumFromTx(UINT uPos);
		UINT	GetStrLen(char * pStr);
		// Other Helpers
		BOOL	MatchString(char * pSrc, char * pRcv);
		BOOL	MatchStringC(const char * pSrc, char * pRcv);
		void	MakeUpper(char * pStr);
		UINT	GetLineCount(void);
		BOOL	IsAnyStringItem(void);
		void	GetStringResponse(PDWORD pData, UINT uCount);
		void	SaveUserText(PDWORD pData, UINT uCount);
		UINT	SetLongResponse(void);
		UINT	IsLongResponse(void);
		void	InitBusy(UINT uTime);
		BOOL	BusyTimeout(void);
		void	InitStatus(void);
		BOOL	StatusTimeout(void);
		BOOL	IsTimeout(UINT uStart, UINT uEnd);

	};

// End of File
