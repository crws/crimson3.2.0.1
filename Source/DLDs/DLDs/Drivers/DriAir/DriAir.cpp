#include "intern.hpp"

#include "DriAir.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Dri-Air Driver
//

// Command Definitions

//struct FAR DriAirCmdDef {
//	UINT	uID;		// Space number
//	char	uCOP[10];	// Command String
//	};

DriAirCmdDef CODE_SEG CDriAirDriver::CL[] = {

	{ CSTAT,	"STAT",			},	// "Show process & Regen Temps"
	{ CSTOP,	"STOP",			},	// "Stop System"
	{ CSTRT,	"START",		},	// "Start system"
	{ CCEQ,		"CLREQ",		},	// "Erases event queue"
	{ CDEW,		"DEW",			},	// "Displays dewpoint"
	{ CION,		"ITESTON",		},	// "Turns on Current testing"
	{ CIOFF,	"ITESTOFF",		},	// "Turns off Current testing"
	{ CLRN,		"LEARN",		},	// "Learn Currents Process"
	{ CLOFF,	"LBOFF",		},	// "Turns Loop Break Alarm off"
	{ CLON,		"LBON",			},	// "Turns Loop Break Alarm on"
	{ CMTR,		"MOTOR",		},	// "Motor select"
	{ CVLV,		"VALVE",		},	// "Air Valve select"
	{ CPD1,		"PD2OFF",		},	// "Selects 1 Hopper system"
	{ CPD2,		"PD2ON",		},	// "Selects 2 Hopper system"
	{ CPT1,		"PT1",			},	// "Process Temp Hopper 1"
	{ CPT2,		"PT2",			},	// "Process Temp Hopper 2"
	{ CRST,		"RESET",		},	// "Resets System"
	{ CLDOFF,	"LOADEROFF",		},	// "Loader Off"
	{ CLDON,	"LOADERON",		},	// "Loader On"
	{ CLDT,		"LOADERTIME",		},	// "Loader On Time"
	{ CLDD,		"LOADERDWELL",		},	// "Loader Dwell Time",
	{ CS1OFF,	"SETBACK1OFF",		},	// "Hopper 1 Setback Off"
	{ CS1ON,	"SETBACK1ON",		},	// "Hopper 1 Setback On"
	{ CS2OFF,	"SETBACK2OFF",		},	// "Hopper 2 Setback Off"
	{ CS2ON,	"SETBACK2ON",		},	// "Hopper 2 Setback On"
	{ CS1D,		"SETBACK1DELTA",	},	// "Hopper 1 Setback Delta"
	{ CS2D,		"SETBACK2DELTA",	},	// "Hopper 2 Setback Delta"
	{ CS1A,		"SETBACK1ACTIVATE",	},	// "Hopper 1 Setback Activate Delta T"
	{ CS2A,		"SETBACK2ACTIVATE",	},	// "Hopper 2 Setback Activate Delta T"
	{ CS1I,		"SETBACK1INHIBIT",	},	// "Hopper 1 Setback Inhibit Time"
	{ CS2I,		"SETBACK2INHIBIT",	},	// "Hopper 2 Setback Inhibit Time"
	{ CADDR,	"ADDR",			},	// "Select Network Address"
	{ CSCMD,	"Send User",		},	// "Send UCMD"
	{ CUCMD,	"User Cmd",		},	// "User Custom Command"
	{ CURSP,	"User Resp",		},	// "Response to User Command"
	{ COLDS,	"STATUS",		},	// "Old Status command"
	};

// Instantiator

INSTANTIATE(CDriAirDriver);

// Constructor

CDriAirDriver::CDriAirDriver(void)
{
	m_Ident		= DRIVER_ID;

	m_uWErrCt	= 0;

	m_uStartState	= 0;
	m_uLearnState	= 0;

	m_fBusy		= FALSE;

	memset(m_pRx, 0, sizeof(m_pRx));

	m_pRx[0]	= m_cRx0;

	CTEXT Hex[]	= "0123456789ABCDEF";
	CTEXT NET[]	= "NET";
	CTEXT SETON[]	= " ON";
	CTEXT SETOFF[]	= " OFF";
	CTEXT ACTIV[]	= "ACTIVATE";
	CTEXT INHIB[]	= "INHIBIT";
	CTEXT DISAB[]	= "DISABLE";
	CTEXT ENABL[]	= "ENABLE";
	CTEXT LDONE[]	= "LEARN CURRENTS DONE";
	CTEXT STOPS[]	= "STOP SYSTEM";
	CTEXT CURRT[]	= "CURRENT";
	CTEXT LOOPB[]	= "LOOP BREAK ALARM";
	CTEXT MOTOR[]	= "MOTOR";
	CTEXT VALVE[]	= "VALVE";
	CTEXT H1SB[]	= "HOPPER 1 SETBACK";
	CTEXT H2SB[]	= "HOPPER 2 SETBACK";
	CTEXT ONEH[]	= "ONE HOPPER SYSTEM";
	CTEXT TWOH[]	= "TWO HOPPER SYSTEM";

	m_pHex	= Hex;
	m_pNet	= NET;
	m_pOn	= SETON;
	m_pOff	= SETOFF;
	m_pAct	= ACTIV;
	m_pInh	= INHIB;
	m_pDis	= DISAB;
	m_pEnb	= ENABL;
	m_pDone	= LDONE;
	m_pStop = STOPS;
	m_pCur	= CURRT;
	m_pLba	= LOOPB;
	m_pMot	= MOTOR;
	m_pVlv	= VALVE;
	m_pH1S	= H1SB;
	m_pH2S	= H2SB;
	m_p1H	= ONEH;
	m_p2H	= TWOH;
	}

// Destructor

CDriAirDriver::~CDriAirDriver(void)
{
	DeallocDA();
	}

// Configuration

void MCALL CDriAirDriver::Load(LPCBYTE pData)
{
	MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

void MCALL CDriAirDriver::CheckConfig(CSerialConfig &Config)
{
	m_uDelay = 2 * Config.m_uBaudRate / 3;
	}

// Management

void MCALL CDriAirDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CDriAirDriver::Open(void)
{
	m_pCL = (DriAirCmdDef FAR *) CL;
	}

// Device

CCODE MCALL CDriAirDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			memset(m_pCtx, 0, sizeof(*m_pCtx));

			m_pCtx->m_uDrop = GetByte(pData);

			UINT uUser      = GetWord(pData);

			m_pCtx->Drop[0] = ((m_pCtx->m_uDrop / 10) % 10) + '0';
			m_pCtx->Drop[1] =  (m_pCtx->m_uDrop % 10) + '0';

			m_pCtx->m_fUser = uUser == m_Ident;

			m_pCtx->m_fStartStatus	= FALSE;
			m_pCtx->m_fStopStatus	= FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CDriAirDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		DeallocDA();

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CDriAirDriver::Ping(void)
{
	m_fInPing = TRUE;

//**/	AfxTrace0("\r\nPING ");

	DWORD Data[1];

	CAddress Addr;

	Addr.a.m_Table  = CSTAT;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

//	ConfigOldStatusRead(FALSE); // set disable status read

	if( Read(Addr, Data, 1) == 1 ) {

//		m_uStatusPtr      = 0;

		m_uStatusInterval = TOSTATA; // set periodic auto-reads

//		ConfigOldStatusRead(TRUE); // set force status read

//		GetOldStatus();

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CDriAirDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !SetItemPtr(Addr.a.m_Table) ) return uCount;

	if( NoReadTransmit(Addr, pData, uCount) ) return uCount;

//**/	AfxTrace3("\r\nRead T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	return DoRead(Addr, pData, uCount); 
	}

CCODE MCALL CDriAirDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !SetItemPtr(Addr.a.m_Table) ) return uCount;

	if( NoWriteTransmit(Addr, pData, uCount) ) return uCount;

//**/	AfxTrace3("\r\n\n*** WRITE *** T=%d O=%d C=%d\r\n\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	return DoWrite(Addr, pData, uCount);
	}

// Execute Read/Write

CCODE CDriAirDriver::DoRead(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_fBusy ) {

		GetReply();

		return CCODE_ERROR | (BusyTimeout() ? 0 : CCODE_BUSY);
		}

	StartFrame();

	BOOL fOk = FALSE;

	if( Transact() ) {

		fOk = GetReadResponse(pData, Addr.a.m_Offset, uCount);
		}

	DeallocDA();

	return fOk ? uCount : CCODE_ERROR;
	}

CCODE CDriAirDriver::DoWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_fBusy ) {

		m_uWErrCt = 0;

		while( m_uWErrCt++ < 10 ) {

			if( GetReply() ) {

				m_uWErrCt = 0;

				break;
				}
			}

		return uCount;
		}

	StartFrame();

	AddWriteData(pData);

	BOOL fOk = FALSE;

	if( Transact() ) fOk = GetWriteResponse();

	m_uWErrCt = fOk || m_uWErrCt >= 2 ? 0 : m_uWErrCt + 1;

	DeallocDA();

	if( m_pDAItem->uID == CSCMD ) return uCount;

	return fOk || !m_uWErrCt ? uCount : CCODE_ERROR;
	}

void CDriAirDriver::GetOldStatus(void)
{
	if( m_fBusy || !StatusTimeout() ) return;

	if( !SetItemPtr(COLDS) ) return;

	m_uPtr       = 0;

	m_uStatusPtr = 0;

	AddText((PCTXT)m_pDAItem->cOP);

	if( Transact() ) {

		GetOldStatusResponse();
		}

	DeallocDA();

	InitStatus();
	}

// Frame Building

void CDriAirDriver::StartFrame(void)
{
	m_uPtr = 0;

	if( m_pDAItem->uID == CSCMD ) {

		UINT uSize = min(GetStrLen(m_pCtx->m_cUCMD), SZUCMD);

		for( UINT i = 0; i < uSize; i++ ) {

			BYTE b = m_pCtx->m_cUCMD[i];

			if( b ) AddByte(b);

			else return;
			}

		return;
		}

	AddCmdString();

	AddByte(m_pCtx->Drop[0]);
	AddByte(m_pCtx->Drop[1]);
	}

void CDriAirDriver::AddCmdString(void)
{
	AddText(m_pNet);

	AddText((PCTXT)m_pDAItem->cOP);
	}

void CDriAirDriver::EndFrame(void)
{
	AddByte( CR );
	}

void CDriAirDriver::AddByte(char cData)
{
	if( m_uPtr < sizeof(m_cTx) ) m_cTx[m_uPtr++] = cData;
	}

void CDriAirDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));
	AddByte(LOBYTE(wData));
	}

void CDriAirDriver::AddLong(DWORD dData)
{
	AddWord(HIWORD(dData));
	AddWord(LOWORD(dData));
	}

void CDriAirDriver::AddText(PCTXT pText)
{
	for( UINT i = 0; i < strlen(pText); i++ ) AddByte(pText[i]);
	}

void CDriAirDriver::AddWriteData(PDWORD pData)
{
	if( !WantWriteData() ) return;

	AddByte('=');

	char c[12];

	SPrintf(c, "%d", *pData);

	AddText(c);
	}

BOOL CDriAirDriver::WantWriteData(void)
{
	switch( m_pDAItem->uID ) {

		case CPT1:
		case CPT2:
		case CLDT:
		case CLDD:
		case CS1D:
		case CS2D:
		case CS1A:
		case CS2A:
		case CS1I:
		case CS2I:
		case CADDR:
			return TRUE;
		}

	return FALSE;
	}

// Transport

BOOL CDriAirDriver::Transact(void)
{
	EndFrame();

	memset(m_cRx0,  0, SZMAX);

	if( Send() ) return GetReply();

	return FALSE;
	}

BOOL CDriAirDriver::Send(void)
{
	m_pData->ClearRx();

//**/	AfxTrace0("\r\nSend = "); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("%c", IsPrintable(m_cTx[k]) ? m_cTx[k] : '#');

	PCBYTE pC = PCBYTE(m_cTx);

	UINT i    = 0;
	WORD rr   = 0;
	BYTE ss   = 0;

	BOOL fU   = m_pDAItem->uID == CSCMD;

//**/	AfxTrace0("\r\nEcho = ");

	while( i < m_uPtr ) {

		ss = pC[i];

		rr = 0;

		m_pData->Write(&pC[i], 1, FOREVER); // device cannot receive consistantly @38400 without inter-char delay

		SetTimer(1000);

		while( GetTimer() ) { // wait for echoed character

			if( (rr = m_pData->Read(40)) == LOWORD(NOTHING) ) continue;

//**/			if( rr >= ' ' ) AfxTrace1("%c", rr); else AfxTrace1("<%2.2x>", rr);

			if( LOBYTE(rr) == ss || fU) {

				i++;

				break;
				}
			}

		if( rr != ss ) return FALSE;
		}

	if( rr == CR && ss == CR ) {

		rr = m_pData->Read(40); // get final LF

//**/		AfxTrace1("<%2.2x>", rr);

		return rr == LF;
		}

	if( fU ) return TRUE;

	m_pData->Write(CR, FOREVER);

	return FALSE;
	}

// Reply Handling

BOOL CDriAirDriver::GetReply(void)
{
	BOOL fUSR   = m_pDAItem->uID == CSCMD;

	BOOL fEOT   = FALSE;

	BOOL fUDone = FALSE;

	BOOL fUFill = m_uUserLine > USRFILL;

	BYTE bAdd   = 1;

	UINT uItem  = 0;

	UINT uTime  = 1000;

	UINT uPtr   = 0;

	UINT uState = fUSR ? 10 : SetLongResponse();

	if( m_fBusy ) {

		uState = m_uStartState ? STSTART+2 : STLEARN+1;
		}

	UINT uMax   = fUSR ? URSPEND : SZMAX;

	char * pArr = fUSR ? m_pCtx->m_cURSP : m_cRx0;

	UINT uLC    = 1;
	UINT uUTarg = m_uUserLine % USRFILL;

//**/	AfxTrace0("\r\nR=");

	SetTimer(uTime);

	while( GetTimer() ) {

		WORD wData;

		if( (wData = m_pData->Read(uTime)) == LOWORD(NOTHING) ) {

			continue;
			}

		char cData = (char)LOBYTE(wData);

//**/		if( cData >= ' ' ) AfxTrace1("%c", cData); else AfxTrace1("<%2.2x>", BYTE(cData));

		if( !fUSR ) pArr[uPtr++] = cData;

		if( cData == EOT ) fEOT = TRUE;

		switch( uState ) {

			case STNOTSL:
//**/
				m_fBusy = FALSE;

				switch( cData ) {

					case ETX:
					case EOT:
						uState = STWAITE;
						break;

					case  LF:
//**/						AfxTrace1("\r\nI=%d NA ", uItem);

						pArr = NextArray(&uItem);

						uPtr = 0;

						if( m_pDAItem->uID == CCEQ ) return TRUE;
						break;
					}
				break;

			case STWAITE:

				if( cData == LF ) return TRUE;
				break;

			case STSTART: // Start Command wait for "Stop" or "Start" in line

				InitBusy(TO5K);

				m_fBusy = TRUE;

				uState  = STSTRT1;

				break;

			case STSTRT1:

				if( cData == LF ) { // presumably "Dryer Start Initiated"

					uPtr = 0;

					uState = STSTRT2;

					m_uStartState = 2;
					}
				break;

			case STSTRT2:

				if( cData == LF ) {

					if( MatchStringC(m_pStop, m_cRx0) || MatchString(m_pDAItem->cOP, m_cRx0) ) {

						m_uStartState = 3;
						}

					InitBusy(m_uStartState == 3 ? TO5K : TOMAX);

					return FALSE; // wait for next line, if any
					}

				if( fEOT ) { // EOT added in subsequent version.

					uState        = STWAITE; // return TRUE on EOT
					m_fBusy       = FALSE;
					m_uStartState = 0;
					}

				break;

			case STLEARN: // Learn Response Entry

				InitBusy(TO15K);

				m_fBusy       = TRUE;

				uState        = STLEARN + 1;

				m_uLearnState = 2;

				break;

			case STLERN1: // Learn Command, wait for "Done"

				if( fEOT || cData == LF ) {

					uPtr = 0;

					if( fEOT || MatchStringC(m_pDone, m_cRx0) ) {

						m_fBusy       = FALSE;
						m_uLearnState = 0;
						return TRUE;
						}

					InitBusy(TOMAX);

					return FALSE;
					}
				break;

			case STUSERS: // receive User response

				if( cData == EOT || cData == ETX ) {

					pArr[uPtr] = 0;

					uState = STWAITE;
					}

				else {
					if( !fUDone ) {

						if( uLC >= uUTarg ) {

							if( cData < ' ' ) cData = fUFill ? '_' : 0;

							if( !cData ) fUDone = TRUE;

							else if( !(cData == ' ' && pArr[uPtr-1] == ' ') ) pArr[uPtr++] = cData;
							}

						else {
							if( cData == LF ) uLC++;
							}
						}
					}
				break;
			}

		if( uPtr >= uMax ) {

			if( !fUSR ) {

				if( uState ) return FALSE;

				uPtr = 0;
				}

			else {
				pArr[URSPEND] = 0;
				break;
				}
			}
		}

//**/	AfxTrace0("\r\nTIMEOUT");

	if( IsLongResponse() ) {

		if( m_uStartState == 1 ) {

			m_uStartState = 0;

			m_fBusy = FALSE;

			return TRUE;
			}

		if( m_uLearnState == 1 ) {

			m_uLearnState = 0;

			m_fBusy = FALSE;

			return TRUE;
			}

		return FALSE;
		}

	if( fUSR ) {

		pArr[min(uPtr, URSPEND)] = 0;
		return TRUE;
		}

	return IsOldStatusRead() ? m_uStatusPtr > 15 : FALSE;
	}

char * CDriAirDriver::NextArray(UINT *pItem)
{
	BOOL fOldStat = IsOldStatusRead();

	UINT uItem    = fOldStat ? m_uStatusPtr : *pItem;

	UINT uNew     = min(uItem + 1, SZREAD - 1);

	UINT uSize    = fOldStat ? GetStrLen(m_cRx0) : SZMAX;

	if( m_pRx[uNew] ) {

		delete m_pRx[uNew];
		m_pRx[uNew] = NULL;
		}

	m_pRx[uNew] = new char[uSize + 1];

	if( fOldStat ) {
		
		memcpy(m_pRx[uNew], m_cRx0, uSize);
		m_pRx[uNew][uSize-1] = 0;
		uItem = 0;
		m_uStatusPtr = uNew;
		}

	else uItem = uNew;

	*pItem = uItem;

	return m_pRx[uItem];
	}

void CDriAirDriver::DeallocDA(void)
{
	for( UINT i = 1; i < SZREAD; i++ ) {

		if( m_pRx[i] ) {

			delete m_pRx[i];

			m_pRx[i] = NULL;
			}
		}
	}

// Response Handling

BOOL CDriAirDriver::GetReadResponse(PDWORD pData, UINT uOffset, UINT uCount)
{
	if( m_fInPing ) {

		m_fInPing = FALSE;

		return (BOOL)m_cRx0[0];
		}

	switch( m_pDAItem->uID ) {

		case CSTAT:
			return GetStatusResponse(pData, uOffset, uCount);

		case CDEW:
			return GetDewResponse(pData, uOffset, uCount);

		case CS1ON:
		case CS2ON:
			return GetTxtResponse(pData, (char *)m_pOn, (char *)m_pOff );

		case CS1OFF:
		case CS2OFF:
			return GetTxtResponse(pData, (char *)m_pOff, (char *)m_pOn);

		case CPT1:
		case CPT2:
			return GetProcResponse(pData, uOffset, uCount);

		default:
			return GetNumResponse(m_cRx0, pData, 0);
		}

	return TRUE;
	}

BOOL CDriAirDriver::GetStatusResponse(PDWORD pData, UINT uOffset, UINT uCount)
{
	char * pSrc = m_cRx0;

	UINT i = 0;

	while( i < 3 ) {

		if( m_cRx0[i] == ' ' ) i++;

		else {
			pSrc = &m_cRx0[i];
			break;
			}
		}

	if( !IsSNum(pSrc[0]) ) {

		pSrc = m_pRx[1];

		if( !pSrc ) return FALSE;

		if( !IsSNum(pSrc[0]) ) return FALSE;
		}

	UINT uCnt   = uCount;

	UINT uFirst = 0;
	UINT uNext  = 0;

	i = 0;

	while( i < uOffset ) {

		FindComma(pSrc, &uNext);

		i++;
		}

	uNext = uFirst;

	char c[10];

	char * p = c;

	for( i = 0; i < uCnt; i++ ) {

		uFirst   = FindComma(pSrc, &uNext);

		if( uNext ) {

			memset(p, 0, 10);

			memcpy(p, &pSrc[uFirst], uNext - uFirst - 1);

			pData[i] = MotorToHost(DWORD(ATOI((const char *)p)));
			}

		else {
			UINT u = uCnt - 1;

			if( i < u ) memset(&pData[i], 0, 4 * (u - i));

			return BOOL(i);
			}
		}

	uNext  = GetStrLen(m_cRx0);

	if( m_cRx0[uNext-3] == ETX ) { // should be ETX,CR,LF at end of string

		BOOL f = BOOL(ATOI(&m_cRx0[uNext - 6]));

		m_pCtx->m_fStartStatus = f;
		m_pCtx->m_fStopStatus  = !f;
		}

	return TRUE;
	}

BOOL CDriAirDriver::GetDewResponse(PDWORD pData, UINT uOffset, UINT uCount)
{
	DWORD d[4] = {0};

	char * p   = (char *)m_pEnb;

	if( MatchString(p, m_cRx0) ) d[0] = 1;

	UINT uPos = 0;
	UINT uDig = 0;

	p = m_pRx[1];

	if( p && p[0] ) {

		uPos = FindDAChar(p, 0, ':');

		uDig = 0;

		if( uPos != FINDERR ) {

			uDig = FindSNum(p, uPos);

			if( uDig < FINDERR ) d[1] = ATOI(&p[uDig]);
			}

		uPos = FindDAChar(p, uDig, ':');

		if( uPos != FINDERR ) {

			uDig = FindSNum(p, uPos);

			if( uDig < FINDERR ) d[2] = ATOI(&p[uDig]);
			}
		}

	p = m_pRx[2];

	if( p && p[0] ) {

		uPos = 0;

		uPos = FindData(p, &uPos);

		if( uPos ) d[3] = ATOI(&p[uPos]);
		}

	UINT uCnt = min(uCount, 4);

	for( UINT i = 0; i < uCnt; i++ ) {

		pData[i] = MotorToHost(d[uOffset + i]);
		}

	return TRUE;
	}

BOOL CDriAirDriver::GetTxtResponse(PDWORD pData, char * pStrT, char *pStrF)
{
	if( MatchString(pStrT, m_cRx0) )	*pData = DWORD(TRUE);

	else if( MatchString(pStrF, m_cRx0) )	*pData = DWORD(FALSE);

	else return FALSE;

	return TRUE;
	}

BOOL CDriAirDriver::GetProcResponse(PDWORD pData, UINT uOffset, UINT uCount)
{
	DWORD d[2] = {0};

	UINT uPos  = 0;

	if( GetNumResponse(m_cRx0, d, 0) ) {

		uPos = FindDAChar(m_cRx0, 0, '=');

		if( GetNumResponse(m_cRx0, &d[1], uPos + 1) ) {

			UINT uCnt = min(uCount, 2);

			for( UINT i = 0; i < uCnt; i++ ) {

				pData[i] = MotorToHost(d[uOffset + i]);
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CDriAirDriver::GetNumResponse(char * pStr, PDWORD pData, UINT uStart)
{
	UINT uPos  = uStart;

	UINT uFind = FindData(pStr, &uPos);

	if( uFind ) {

		*pData = ATOI(&m_cRx0[uFind]);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDriAirDriver::GetWriteResponse(void)
{
	return TRUE;
	}

void CDriAirDriver::GetOldStatusResponse(void)
{
	BYTE bOneH  = 0;
	BYTE bTwoH  = 0;
	BYTE bMot   = 0;
	BYTE bVlv   = 0;
	BYTE bS1    = 0;
	BYTE bS2    = 0;
	BYTE bLB    = 0;
	BYTE bCur   = 0;

	BYTE bBeg   = 0;
	BYTE bDone  = 0;

	if( !m_pRx[1] ) return;

	char * pStr = m_pRx[1];

	for( UINT i = 1; !bDone && (i < 4); i++ ) {

		if( !m_pRx[i] ) return;

		char * pStr = m_pRx[i];

		if( !bOneH ) {
			if( MatchStringC(m_p1H, pStr) ) {

				bOneH = 1;
				bTwoH = 2;
				}
			}

		if( !bTwoH ) {

			if( MatchStringC(m_p2H, pStr) ) {

				bOneH = 2;
				bTwoH = 1;
				}
			}

		if( bOneH ) bBeg = i;
		}

	if( !(bOneH && bTwoH) ) return;

	bDone  = bOneH & 1 ? 5 : 6;

	for( i = bOneH; bDone && (i < 15); i++ ) {

		if( !m_pRx[i] ) return;

		pStr = m_pRx[i];

		if( !bMot ) {
			if( MatchStringC(m_pMot, pStr) ) {

				bMot = 1;
				bVlv = 2;
				bDone--;
				}
			}

		if( !bVlv ) {
			if( MatchStringC(m_pVlv, pStr) ) {

				bMot = 2;
				bVlv = 1;
				bDone--;
				}
			}

		if( !bS1 ) {
			if( MatchStringC(m_pH1S, pStr) ) {

				bDone--;
				bS1 = MatchStringC(m_pEnb, pStr) ? 1 : 2;
				}
			}

		if( !bLB ) {
			if( MatchStringC(m_pLba, pStr) ) {

				bDone--;
				bLB = MatchStringC(m_pEnb, pStr) ? 1 : 2;
				}
			}

		if( !bCur ) {
			if( MatchStringC(m_pCur, pStr) ) {

				bDone--;
				bCur = MatchStringC(m_pEnb, pStr) ? 1 : 2;
				}
			}

		if( !bS2 && (bTwoH & 1) ) { // Two Hopper System

			if( MatchStringC(m_pH2S, pStr) ) {

				bDone--;
				bS2 = MatchStringC(m_pEnb, pStr) ? 1 : 2;
				}
			}
		}
	}

// Helpers

// Command defintion pointer
BOOL CDriAirDriver::SetItemPtr(UINT uTable)
{
	DriAirCmdDef * p = m_pCL;

	for( UINT i = 0; i < elements(CL); i++, p++ ) {

		if( uTable == p->uID ) {

			m_pDAItem = p;

			return TRUE;
			}
		}

	return FALSE;
	}

// Internal Values
BOOL CDriAirDriver::NoReadTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	BOOL fResult = FALSE;

	if( IsAnyStringItem() ) {

		GetStringResponse(pData, uCount);

		fResult = TRUE;
		}

	else {
		switch( m_pDAItem->uID ) {

			case CSTRT:	*pData = m_pCtx->m_fStartStatus  ? BIGBOOL : 0;	fResult = TRUE; break;
			case CSTOP:	*pData = m_pCtx->m_fStopStatus   ? BIGBOOL : 0;	fResult = TRUE; break;

			case CCEQ:
			case CION:
			case CIOFF:
			case CLRN:
			case CLOFF:
			case CLON:
			case CMTR:
			case CVLV:
			case CLDON:
			case CLDOFF:
			case CPD1:
			case CPD2:
			case CS1OFF:
			case CS1ON:
			case CS2OFF:
			case CS2ON:
			case CRST:
			case CSCMD:
				*pData  = 0;
				fResult = TRUE;
				break;

			case CADDR:
				*pData  = DWORD(m_pCtx->m_uDrop);
				fResult = TRUE;
				break;

			case COLDS:
				ConfigOldStatusRead(TRUE); // set force old status read
				m_uStatusInterval = 0;     // disable periodic auto-reads
				break;
			}
		}

	if( m_uStatusInterval ) GetOldStatus();

	return fResult;
	}

BOOL CDriAirDriver::NoWriteTransmit(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD dData = *pData;

	UINT uT     = Addr.a.m_Table;

	switch( uT ) {

		case CURSP:
			memset(m_pCtx->m_cURSP, 0, SZURSP);
			return TRUE;

		case CUCMD:
			if( m_pCtx->m_fUser && (HIBYTE(HIWORD(pData[0]))) ) {

				SaveUserText(pData, uCount);
				}

			return TRUE;

		case CSCMD:
			if( m_pCtx->m_fUser && dData < 200 && (dData % USRFILL) ) {

				m_uUserLine = dData;

				return FALSE;
				}

			return TRUE;

		case CSTRT:
		case CSTOP:
		case CION:
		case CIOFF:
		case CLOFF:
		case CLON:
		case CMTR:
		case CVLV:
		case CLRN:
			return !dData;

		case CPT1:
		case CPT2:
			return BOOL(Addr.a.m_Offset); // only 0 offset is writeable
		}

	return FALSE;
	}

// Old Command Handling
BOOL CDriAirDriver::IsOldStatusRead(void)
{
	return m_pDAItem->uID == COLDS;
	}

void CDriAirDriver::ConfigOldStatusRead(BOOL fForce)
{
	UINT u = GetTickCount();
	UINT v = u + 100;

	fForce &= (v > u); // Tick count rollover protection

	m_uStatusStart = fForce ? v : u;
	m_uStatusEnd   = fForce ? u : v;
	}

// Response Parsing
UINT CDriAirDriver::FindData(char * pStr, UINT *pPos)
{
	UINT uPos   = *pPos;

	UINT uState = 0;

	UINT uLen   = GetStrLen(pStr);

	for( UINT i = uPos; i < uLen; i++ ) {

		char c = pStr[i];

		switch( uState ) {

			case 0:
				if( c == '=' ) uState = 1;

				break;

			case 1:
				uState = c == ' ' ? 2 : 0;

				if( IsSNum(c) ) {

					*pPos = i;

					return i;
					}
				break;

			case 2:
				if( IsSNum(c) ) {

					*pPos = i;

					return i;
					}

				uState = 0;

				break;
			}

		if( !IsPrintable(c) ) {

			return 0;
			}
		}

	*pPos = 0;

	return 0;
	}

UINT CDriAirDriver::FindComma(char * pStr, UINT *pPos)
{
	UINT uPos = *pPos;

	UINT uP1  = uPos;

	UINT uLen = GetStrLen(pStr);

	BOOL f1   = FALSE;

	for( UINT i = uPos; i < uLen; i++ ) {

		char c = pStr[i];

		if( IsSNum(c) ) {

			if( !f1 ) {

				uP1 = i; // first digit found
				f1  = TRUE;
				}
			}

		if( c == ',' ) {

			if( f1 ) { // previous digit found

				*pPos = i + 1; // next position

				return uP1;    // start of number
				}
			}

		if( c < ' ' ) {

			if( f1 ) {

				*pPos = i + 1;

				return uP1;
				}
			}
		}

	*pPos = 0;

	return 0;
	}

UINT CDriAirDriver::FindSNum(char * pStr, UINT uStart)
{
	UINT uLen = GetStrLen(pStr);

	for( UINT i = uStart; i < uLen; i++ ) {

		if( IsSNum(pStr[i]) ) return i;
		}

	return FINDERR;
	}

UINT CDriAirDriver::FindDAChar(char * pStr, UINT uStart, char cFind)
{
	UINT uLen = GetStrLen(pStr);

	for( UINT i = uStart; i < uLen; i++ ) {

		if( pStr[i] == cFind ) return i;

		if( pStr[i] < ' ' ) return FINDERR;
		}

	return FINDERR;
	}

// Numeric handling

BOOL CDriAirDriver::IsNum(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CDriAirDriver::IsSNum(char c)
{
	return IsNum(c) || c == '+' || c == '-';
	}

BOOL CDriAirDriver::IsAlp(char c)
{
	c &= 0xDF;

	return c >= 'A' && c <= 'Z';
	}

BOOL CDriAirDriver::IsAlpOrSNum(char c)
{
	return IsAlp(c) || IsSNum(c);
	}

BOOL CDriAirDriver::IsPrintable(char c)
{
	BYTE b = BYTE(c);

	return b >= ' ' && b <= 0x7F;
	}

DWORD CDriAirDriver::GetNumFromTx(UINT uPos)
{
	return DWORD(ATOI((const char *)&m_cTx[uPos]));
	}

UINT CDriAirDriver::GetStrLen(char * pStr)
{
	return strlen((const char *)pStr);
	}

// Other Helpers
BOOL CDriAirDriver::MatchString(char * pSrc, char * pRcv)
{
	MakeUpper(pRcv);

	UINT uLenS     = GetStrLen(pSrc);
	UINT uLenR     = GetStrLen(pRcv);

	for( UINT i = 0; i < uLenR; i++ ) {

		if( !strncmp(&pRcv[i], pSrc, uLenS) ) return TRUE;
		}

	return FALSE;
	}

BOOL CDriAirDriver::MatchStringC(const char * pStr, char * pRcv)
{
	return MatchString((char *)pStr, pRcv);
	}

void CDriAirDriver::MakeUpper(char * pStr)
{
	UINT uEnd = GetStrLen(pStr);

	for( UINT i = 0; i < uEnd; i++ ) {

		char b = pStr[i];

		if( b >= 'a' && b <= 'z' ) {

			pStr[i] &= 0x5F;
			}
		}
	}

UINT CDriAirDriver::GetLineCount(void)
{
// expected count over and above echo line

	switch( m_pDAItem->uID ) {

		case CSTRT:
			return 2;

		case CDEW:
			return 4;

		case CLRN:
			return 10;
		}

	return 1;
	}

BOOL CDriAirDriver::IsAnyStringItem(void)
{
	return m_pDAItem->uID == CUCMD || m_pDAItem->uID == CURSP;
	}

void CDriAirDriver::GetStringResponse(PDWORD pData, UINT uCount)
{
	BOOL f      = m_pDAItem->uID == CUCMD;

	char * pSrc = f ? m_pCtx->m_cUCMD : m_pCtx->m_cURSP;

	UINT uSz    = f ? SZUCDW : SZURDW;

	UINT uEnd   = min(uCount, uSz);

	for( UINT i = 0; i < uEnd; i++ ) {

		DWORD x  = PU4(pSrc)[i];

		pData[i] = MotorToHost(x);
		}
	}

void CDriAirDriver::SaveUserText(PDWORD pData, UINT uCount)
{
	PDWORD pD = (PDWORD)m_pCtx->m_cUCMD;

	for( UINT i = 0; i < uCount; i++ ) {

		pD[i] = HostToMotor(pData[i]);
		}
	}

UINT CDriAirDriver::SetLongResponse(void)
{
	m_uStartState = 0;
	m_uLearnState = 0;

	switch( IsLongResponse() ) {

		case 1:	if( !m_fBusy ) m_uStartState = 1;
			return STSTART;

		case 2:	if( !m_fBusy ) m_uLearnState = 1;
			return STLEARN;
		}

	return STNOTSL;
	}

UINT CDriAirDriver::IsLongResponse(void)
{
	switch( m_pDAItem->uID ) {

		case CSTRT:	return 1;
		case CLRN:	return 2;
		}

	return 0;
	}

void CDriAirDriver::InitBusy(UINT uTime)
{
	m_uBusyStart = GetTickCount();
	m_uBusyEnd   = m_uBusyStart + ToTicks(uTime);
	}

BOOL CDriAirDriver::BusyTimeout(void)
{
	if( IsTimeout(m_uBusyStart, m_uBusyEnd) ) {

		m_fBusy = FALSE;

		return TRUE;
		}

	return FALSE;
	}

void CDriAirDriver::InitStatus(void)
{
	m_uStatusStart = GetTickCount();
	m_uStatusEnd   = m_uStatusStart + ToTicks(TOSTATA);
	}

BOOL CDriAirDriver::StatusTimeout(void)
{
	return FALSE; //IsTimeout(m_uStatusStart, m_uStatusEnd);
	}

BOOL CDriAirDriver::IsTimeout(UINT uStart, UINT uEnd)
{
	UINT u = GetTickCount();

	if( uEnd > uStart ) return u >= uEnd || u < uStart;

	return u < uStart && u >= uEnd;
	}

// End of File
