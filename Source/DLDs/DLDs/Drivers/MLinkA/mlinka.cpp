
#include "intern.hpp"

#include "mlinka.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SEW Movilink A Serial Driver
//

// Instantiator

INSTANTIATE(CSEWMovilinkA);

// Constructor

CSEWMovilinkA::CSEWMovilinkA(void)
{
	m_Ident = DRIVER_ID;

	}

// Destructor

CSEWMovilinkA::~CSEWMovilinkA(void)
{
	}

// Entry Points

CCODE MCALL CSEWMovilinkA::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = 0x207E;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

// Configuration

void MCALL CSEWMovilinkA::Load(LPCBYTE pData)
{
	}
	
void MCALL CSEWMovilinkA::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CSEWMovilinkA::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CSEWMovilinkA::Open(void)
{	
	
	}

// Device

CCODE MCALL CSEWMovilinkA::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop		= GetByte(pData);

			m_pCtx->m_uDevice	= GetByte(pData);

			BOOL fBroadcast		= GetByte(pData);

			if( fBroadcast ) {

				m_pCtx->m_bDrop = CMD_BROADCAST;
				}

			m_pCtx->m_dwLatestError = 0;

			memset(m_pCtx->m_uPI, 0, sizeof(m_pCtx->m_uPI));   
	
			memset(m_pCtx->m_uPO, 0, sizeof(m_pCtx->m_uPO));

			m_pCtx->m_fHandshake = TRUE;
						
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	
	}

CCODE MCALL CSEWMovilinkA::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CSEWMovilinkA::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	if ( m_pCtx->m_bDrop == CMD_BROADCAST ) {

		return uCount;
		}

	UINT uAddr = GetOffset(Addr.a.m_Offset);

	UINT uTable = Addr.a.m_Table;
	
	if ( uTable == PDPA ) { 
	
		*pData = m_pCtx->m_uPO[uAddr - 1];
	
		return uCount;
		}
		
	if ( uTable == SPPA ) {
	
		*pData = 0;
	
		return uCount;
		} 
		
	UINT uFnc = Addr.a.m_Extra;
	
	if ( uAddr == CMD_EXCEPTION ) { 
		
		*pData = LONG(IntelToHost(m_pCtx->m_dwLatestError));
		
		return uCount;
		} 
		
	if ( PutRead(uAddr, uFnc) && GetReply(EORX) ) {
	
		if ( CheckReply(EORX) ) { 
		
			if( uAddr == ID_STATUS ) {
			
				DecodeStatusWord1(pData, Addr.a.m_Extra);

				return uCount;
				}
			
			GetGeneric(pData);  
						
			return uCount;
			}
			
		if ( IsError( m_bRx[MANAGE] ) ) {
		
			*pData = 0;
		
			return uCount; 
			}
		} 
		
	return CCODE_ERROR;

	}

CCODE MCALL CSEWMovilinkA::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	UINT uAddr = GetOffset(Addr.a.m_Offset);

	UINT uTable = Addr.a.m_Table;

	if ( uTable == SPPA ) { 
	
		if ( *pData == 0 ) {
		
			return uCount;
			}
	
		return DoProcessData(uAddr, Addr.a.m_Extra);
		} 
		
	if ( uTable == PDPA ) {
	
		return SaveProcessData(uAddr - (Addr.a.m_Extra & 0x03), pData);
		}

	if ( !(Addr.a.m_Extra & 0x03) ) {

		return uCount;
		}
	
	if ( PutWrite(uAddr, Addr.a.m_Extra, pData) ) { 

		if ( m_pCtx->m_bDrop == CMD_BROADCAST ) {

			while( m_pData->Read(0) < NOTHING );
		
			Sleep(25);
			 
			return uCount;
			}

		if ( GetReply(EORX) && CheckReply(EORX) ) {
		
			return uCount;
			}
			
		if ( IsError( m_bRx[MANAGE] ) ) { 
		
			return uCount;
			}
		}
		
	return CCODE_ERROR;
	}

// Implementation

BOOL CSEWMovilinkA::PutRead(UINT uOffset, UINT uOp)
{
	Start();
	
	PutPDU(uOffset, uOp, FALSE); 
	
	for( UINT i = 0; i <= LEN; i++ ) {
	
		AddByte(0x00);
		}
		
	End();
	
	return TRUE;
	}
	
BOOL CSEWMovilinkA::PutPDU(UINT uOffset, UINT uOp, BOOL fWrite)
{  
	PutManagement( uOffset, uOp, fWrite );
	
	AddByte(RES); 
	
	AddWord(uOffset); 
	
	return TRUE;
        }
        
BOOL CSEWMovilinkA::PutProcessDataPDU(UINT uAddr, UINT uOptions)
{
        BOOL bCyclical = (uOptions & 0x04) >> 2;
	
	UINT uPDU = uOptions & 0x03;

	BYTE bTYP = 0;
	
	bTYP |= ( bCyclical ? 0 : 1 ) << 7;
	
	bTYP |= ( uPDU + ( uPDU % 10 - 1) ); 
	
	AddByte(bTYP);

	for( UINT u = 1; u <= uPDU; u++ ) {
	
		switch ( u ) {
		
			case 1:
			case 2:
			case 3:
			        AddWord(m_pCtx->m_uPO[u - 1]); 
			        break;
			        
			case 4: 
				AddWord(m_pCtx->m_uPO[u - 1]);
				AddWord(m_pCtx->m_uPO[u]);
				AddWord(m_pCtx->m_uPO[u + 1]); 
				break;
				
			case 5:
				AddWord(m_pCtx->m_uPO[u + 1]);
				AddWord(m_pCtx->m_uPO[u + 2]);
				AddWord(m_pCtx->m_uPO[u + 3]);
				AddWord(m_pCtx->m_uPO[u + 4]); 
				break; 
			}
		}
		
        return TRUE;
	}
        
BOOL CSEWMovilinkA::PutManagement(UINT uOffset, UINT uOp, BOOL fWrite)
{       
	BYTE bManage = 0; 
	
	bManage |= GetServiceID(uOp, fWrite);
        
        bManage |= GetDataLength( );
        
        bManage |= GetHandshake( );
        
        bManage |= GetStatus( );
        
        AddByte(bManage);  
        
        return TRUE;
       	}
       	
BYTE CSEWMovilinkA::GetServiceID( UINT uOp, BOOL fWrite )
{
	AddByte(TYP);
	       	
	if( !fWrite ) {

		return 1;
		}
      	
	return (BYTE) ((uOp & 0x03) + 1);
	}
	
BYTE CSEWMovilinkA::GetDataLength(void)
{
        return (BYTE) LEN << 4;
	}
	
BYTE CSEWMovilinkA::GetHandshake(void)
{       
	m_pCtx->m_fHandshake = !m_pCtx->m_fHandshake;
	
	return (BYTE) m_pCtx->m_fHandshake << 6;
        }
	
BYTE CSEWMovilinkA::GetStatus(void)
{
        return (BYTE) 0;
	}
       	
CCODE CSEWMovilinkA::DoProcessData(UINT uOffset, UINT uOp) 
{
	UINT uPDU = uOffset;
	
	if ( PutProcessData(uOffset, ( uOp << 2 ) + uOffset) ) {
	        
	        if ( m_pCtx->m_bDrop == CMD_BROADCAST ) {
		
			Sleep(25);
			 
			return 1;
			}
			
		BYTE bLength = 4;
		
		for( UINT u = 0; u < uPDU; u++ ) { 
		
			switch(u) {
		
				case 0:
				case 1:
				case 2:
					bLength += 2;
					break;
				case 3:
					bLength += 6;
					break;
				case 4:
					bLength += 8;
					break;
				}
			}
				
		if ( GetReply(bLength) && CheckReply(bLength) ) { 
		
			for( UINT i = 3, n = 0; n <= uPDU; i+=2, n++ ) {
			        
				m_pCtx->m_uPI[n] = MAKEWORD(m_bRx[i+1], m_bRx[i]); 
				
				}
						
			return 1; 
			}
		}
	
	return CCODE_ERROR;        
	}
	
BOOL CSEWMovilinkA::PutWrite(UINT uOffset, UINT uOp, PDWORD pData)
{
	Start();
	
	PutPDU(uOffset, uOp, TRUE); 
	
	PutData(pData); 
	
	End();
	
	return TRUE;
	}
	
BOOL CSEWMovilinkA::PutProcessData(UINT uOffset, UINT uOp)
{
	Start(); 
	
	PutProcessDataPDU(uOffset, uOp);
	
	End( ); 
	
	return TRUE;
	}
	
CCODE CSEWMovilinkA::SaveProcessData(UINT uAddr, PDWORD pData) 
{
      	m_pCtx->m_uPO[uAddr - 1] = LOWORD(*pData);

	return 1;
	}
	
BOOL CSEWMovilinkA::PutData(PDWORD pData)
{
	DWORD x = (pData)[0];
		
	AddWord(HIWORD(x));
       		
       	AddWord(LOWORD(x));
               
	return TRUE;
	}

UINT CSEWMovilinkA::GetOffset(UINT uAddr)
{
	if( m_pCtx->m_uDevice == MOVITRAC ) {

		switch( uAddr ) {

			case 0x2116:

				return 0x2267;

			case 0x2117:

				return 0x2268;

			case 0x2126:

				return 0x2269;
			}
		}
	
	return uAddr;
	}

BOOL CSEWMovilinkA::GetReply(BYTE bLength)
{
	UINT uCount = 0;
	
	UINT uTimer = 0;

	UINT uData = 0;

	BOOL fFirst = FALSE;
       	
	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uData = m_pData->Read(uTimer) ) == NOTHING ) {

			if( !fFirst && uTimer > WAIT ) {

				return FALSE;
				}

			continue;
			}

		if( m_bRx[uCount] == SD2 ) {

			fFirst = TRUE;
			}

		m_bRx[uCount++] = uData; 

		if ( uCount == bLength ) {   
			
			return TRUE;
			}

		Sleep(IDLE);
		}

	return FALSE;
	}

BOOL CSEWMovilinkA::CheckReply(BYTE bLength )
{
	UINT i;
	
	if ( m_bRx[0] != SD2 ) 
	
		return FALSE;  
		
	BYTE bCheck = m_bRx[0]; 
	
	for( i = 1; i < m_uCount; i++ ) {
	
		bCheck = m_bRx[i] ^ bCheck;
		}
		
	if ( bCheck != m_bRx[bLength] )
	
		return FALSE;
		
	if ( IsError( m_bRx[MANAGE] ) ) {

		m_pCtx->m_dwLatestError = 0;  
		
		m_pCtx->m_dwLatestError |= MAKEWORD(m_bRx[5], m_bRx[6]); 
		
		if ( ( m_bRx[9] == 0 ) && ( m_bRx[10] == 0 ) ) {
		
			m_pCtx->m_dwLatestError |= MAKEWORD(m_bRx[7], m_bRx[8]) << 16; 
			}
			
		else {
			m_pCtx->m_dwLatestError |= MAKEWORD(m_bRx[9], m_bRx[10]) << 16;
			}
		
		return FALSE;
		} 
		
	for( i++; i < HEAD + 2; i++) {
		
		if ( m_bRx[i] != m_bTx[i] )
	
			return FALSE; 
		}
		
	return TRUE;
	} 
	
BOOL CSEWMovilinkA::IsError(BYTE Management) {

        if ( Management & 0x80 )
        
        	return TRUE;
       
        return FALSE;
	}

BOOL CSEWMovilinkA::Start(void)
{
	m_pData->ClearTx();  
	
	m_uCount = 0;
	
	m_uPtr = 0; 
	
	AddByte(SD1);
	
	m_bCheck = 0;
	
	AddByte( m_pCtx->m_bDrop );
	
	return TRUE;
	}
	
void CSEWMovilinkA::AddByte(BYTE bData)
{
	m_bTx[m_uPtr] = bData;
		
	m_uPtr++;
	
	m_uCount++;
	}
	
void CSEWMovilinkA::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	} 
	
void CSEWMovilinkA::End(void)
{
	BYTE bCheck = m_bTx[0]; 
	
	for( UINT i = 1; i < m_uCount; i++ ) {
	
		bCheck = m_bTx[i] ^ bCheck;
		}
		
	AddByte(bCheck); 
	
	Send(); 
	
	return;
	}
	
void CSEWMovilinkA::Send(void)
{
	for( UINT i = 0; i < sizeof(m_bRx); i++ ) {

		m_bRx[i] = 0;
		}
		
	m_pData->Write(m_bTx, m_uCount, FOREVER);
	}  
	
void CSEWMovilinkA::GetGeneric(PDWORD pData)
{
	*pData = 0;
	
	for( UINT i = HEAD + 2, shift = 24; i < EORX - 1; i++, shift -= 8 ) {
	
		UINT uQuarter = m_bRx[i];
		
		uQuarter = uQuarter << shift;
	
		*pData |= uQuarter;
		
		} 
	} 
	
void CSEWMovilinkA::GetBit(PDWORD pData, BYTE bBit)
{
     	*pData = 0;
     	
     	BYTE bResult = bBit; 
     	
     	for( UINT b = bBit; b > 0; b-- ) {
	
		bResult *= 2;
		}
		
	if ( !bResult )	bResult++; 
	
	*pData = m_bRx[10] & bResult; 
	} 
	
void CSEWMovilinkA::DecodeStatusWord1(PDWORD pData, BYTE bBit) 
{
       *pData = 0; 
       
       BOOL fWarning = m_bRx[10] & 32;
       
       switch( bBit ) {
        
        	case 0:     				// Inverter Status
			if ( !fWarning ) {
        		       
        			*pData = m_bRx[9]; 
				}
        		break;
        		
        		
        	case 1:           			// Operational Status
        		*pData = ( m_bRx[10] & 2 ) >> 1;
        		
        		break;
        		
               	case 2:     				// Error Status
			if ( fWarning ) {
        		
        			*pData = m_bRx[9];
				}
        		break;
        	        	
        	case 3: 				// Active Parameter Set
                        *pData = ( ( m_bRx[10] & 16 ) >> 4 ) + 1; 
                        
                        break;
                }
    	}

// End of File
