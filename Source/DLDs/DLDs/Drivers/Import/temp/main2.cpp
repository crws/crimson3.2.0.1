
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "yastcpm.hpp"

//#define YDEBUG FALSE
	
YaskawaTCPCmdDef CODE_SEG CYaskawaTCPDriver::CL[] = {
{"AB", /* "Abort Motion and Program",*/			CM_COM0,	1},
{"AB", /* "Abort Motion Only",*/			CM_COM1,	2},
{"AC", /* "Acceleration",*/				CM_BOTH2,	3},
{"AD", /* "After Distance",*/				CM_WRITE1,	4},
{"AF", /* "Enable Digital Feedback",*/			CM_BOTH0,	5},
{"AF", /* "Enable Analog Feedback",*/			CM_BOTH1,	6},
{"AI", /* "After Input",*/				CM_WRITE1,	7},
{"AL", /* "Arm Latch",*/				CM_COM3,	8},
{"AM", /* "After Motion",*/				CM_COM3,	9},
{"AO", /* "Analog Output",*/				CM_WRITE1,	10},
{"AP", /* "After Absolute Postion",*/			CM_WRITE1,	11},
{"AR", /* "After Relative Distance",*/			CM_WRITE1,	12},
{"AS", /* "At Speed",*/					CM_COM3,	13},
{"AT", /* "At Time",*/					CM_WRITE1,	14},
{"BG", /* "Begin Motion",*/				CM_COM3,	15},
{"BL", /* "Reverse Software Limit",*/			CM_BOTH2,	16},
{"BN", /* "Burn",*/					CM_COM4,	17},
{"BP", /* "Burn Program",*/				CM_COM4,	18},
{"BV", /* "Burn Variables",*/				CM_COM4,	19},
{"CB", /* "Clear Bit",*/				CM_WRITE1,	20},
{"CD", /* "Contour Data",*/				CM_WRITE1,	21},
{"CE", /* "Configure Encoder",*/			CM_WRITE1,	22},
{"CM", /* "Contour Mode",*/				CM_COM4,	23},
{"DC", /* "Deceleration",*/				CM_BOTH2,	24},
{"DE", /* "Dual (Auxiliary) Encoder Position",*/	CM_BOTH2,	25},
{"DP", /* "Define Position",*/				CM_BOTH2,	26},
{"DT", /* "Delta Time",*/				CM_BOTH2,	27},
{"DV", /* "Dual Velocity Enable (Dual Loop)",*/		CM_BOTH2,	28},
{"EA", /* "ECAM Master Axis",*/				CM_WRITE2,	29},
{"EB", /* "Enable ECAM Mode Enable",*/			CM_BOTH2,	30},
{"EC", /* "ECAM Counter",*/				CM_BOTH2,	31},
{"EG", /* "ECAM Engage",*/				CM_BOTH2,	32},
{"EM", /* "ECAM Cycle",*/				CM_WRITE1,	33},
{"EQ", /* "ECAM Quit (Disengage)",*/			CM_BOTH2,	34},
{"ER", /* "Error Limit",*/				CM_BOTH2,	35},
{"FA", /* "Acceleration Feed Forward",*/		CM_BOTH2,	36},
{"FE", /* "Find Edge",*/				CM_COM3,	37},
{"FI", /* "Find Index",*/				CM_COM3,	38},
{"FL", /* "Forward Software Limit",*/			CM_BOTH2,	39},
{"FV", /* "Velocity Feed Forward",*/			CM_BOTH2,	40},
{"GA", /* "Master Axis for Gearing",*/			CM_COM3,	41},
{"GR", /* "Gear Ratio",*/				CM_BOTH2,	42},
{"HM", /* "Home",*/					CM_COM4,	43},
{"HX", /* "Halt Task Execution",*/			CM_SPEC,	44},
{"IL", /* "Integrator Limit",*/				CM_BOTH2,	45},
{"IP", /* "Increment Position",*/			CM_BOTH2,	46},
{"IT", /* "Independent Time Constant",*/		CM_BOTH2,	47},
{"JG", /* "Jog",*/					CM_BOTH2,	48},
{"KD", /* "Derivative Constant",*/			CM_BOTH2,	49},
{"KI", /* "Integrator",*/				CM_BOTH2,	50},
{"KP", /* "Proportional Constant",*/			CM_BOTH2,	51},
{"MC", /* "Motion Complete (In Position)",*/		CM_COM3,	52},
{"MF", /* "Forward Motion to Position",*/		CM_WRITE1,	53},
{"MM", /* "Master Modulus",*/				CM_WRITE1,	54},
{"MO", /* "Motor Off",*/				CM_BOTH4,	55},
{"MR", /* "Reverse Motion to Position",*/		CM_WRITE1,	56},
{"MT", /* "Motor Type",*/				CM_BOTH2,	57},
{"NA", /* "Number of Axes",*/				CM_BOTH2,	58},
{"OB", /* "Output Bit",*/				CM_WRITE1,	59},
{"OE", /* "Off on Error - Enable/Disable",*/		CM_BOTH2,	60},
{"OF", /* "Offset",*/					CM_BOTH2,	61},
{"OP", /* "Output Port",*/				CM_WRITE1,	62},
{"PA", /* "Position Absolute",*/			CM_BOTH2,	63},
{"PR", /* "Position Relative",*/			CM_BOTH2,	64},
{"RL", /* "Report Latched Position",*/			CM_READ2,	65},
{"RS", /* "Reset",*/					CM_WRITE1,	66},
{"SB", /* "Set Bit",*/					CM_WRITE1,	67},
{"SH", /* "Servo Here",*/				CM_COM4,	68},
{"SP", /* "Speed",*/					CM_BOTH2,	69},
{"ST", /* "Stop",*/					CM_COM3,	70},
{"TB", /* "Tell Status Byte",*/				CM_READ1,	71},
{"TC", /* "Tell Error Code",*/				CM_READ1,	72},
{"TD", /* "Tell Dual Encoder",*/			CM_READ2,	73},
{"TE", /* "Tell Error",*/				CM_READ2,	74},
{"TI", /* "Tell Inputs",*/				CM_READ1,	75},
{"TL", /* "Torque Limit",*/				CM_BOTH2,	76},
{"TM", /* "Time Command",*/				CM_BOTH2,	77},
{"TP", /* "Tell Position",*/				CM_READ2,	78},
{"TS", /* "Tell Switches",*/				CM_READ2,	79},
{"TT", /* "Tell Torque",*/				CM_READ1,	80},
{"TV", /* "Tell Velocity",*/				CM_READ1,	81},
{"TW", /* "Timeout for In Position (MC)",*/		CM_BOTH2,	82},
{"VA", /* "Vector Acceleration",*/			CM_BOTH2,	83},
{"VD", /* "Vector Deceleration",*/			CM_BOTH2,	84},
{"VE", /* "Vector Sequence End",*/			CM_READ1,	85},
{"VR", /* "Vector Speed Ratio",*/			CM_BOTH2,	86},
{"VS", /* "Vector Speed",*/				CM_BOTH2,	87},
{"VT", /* "Vector Time Constant",*/			CM_BOTH2,	88},
{"WC", /* "Wait for Contour Data",*/			CM_COM4,	89},
{"WT", /* "Wait",*/					CM_WRITE1,	90},
{"PF", /* "Position Format - See App. Note"*/		CM_BOTH2,	POSITIONFORMAT},
{"VF", /* "Variable Format - See App. Note"*/		CM_BOTH2,	VARIABLEFORMAT},
{"YP", /* "User Variable YP",*/				CM_BOTH2,	FIRSTUSERVAR},
{"YQ", /* "User Variable YQ",*/				CM_BOTH2,	FIRSTUSERVAR+1},
{"YR", /* "User Variable YR",*/				CM_BOTH2,	FIRSTUSERVAR+2},
{"YS", /* "User Variable YS",*/				CM_BOTH2,	FIRSTUSERVAR+3},
{"YT", /* "User Variable YT",*/				CM_BOTH2,	FIRSTUSERVAR+4},
{"YU", /* "User Variable YU",*/				CM_BOTH2,	FIRSTUSERVAR+5},
{"YV", /* "User Variable YV",*/				CM_BOTH2,	FIRSTUSERVAR+6},
{"YW", /* "User Variable YW",*/				CM_BOTH2,	FIRSTUSERVAR+7},
{"YX", /* "User Variable YX",*/				CM_BOTH2,	FIRSTUSERVAR+8},
{"YY", /* "User Variable YY",*/				CM_BOTH2,	FIRSTUSERVAR+9},
{"IH", /* "Open Internet Handle (WO),*/			CM_SPEC,	CIH},
{"IH", /* "Internet Handle - Handle",*/			CM_SPEC,	CIHA},
{"IH", /* "Internet Handle - IP",*/			CM_SPEC,	CIHB},
{"IH", /* "Internet Handle - Port",*/			CM_SPEC,	CIHC},
{"IH", /* "Internet Handle - Protocol",*/		CM_SPEC,	CIHD},
{"IH", /* "Internet Handle - Terminate (WO)*/		CM_SPEC,	CIHE},
{"CH", /* "Connect Handle (WO)",*/			CM_SPEC,	CCH},
{"CH", /* "Connect Handle - Axis",*/			CM_SPEC,	CCHA},
{"CH", /* "Connect Handle - Send",*/			CM_SPEC,	CCHB},
{"CH", /* "Connect Handle - Receive",*/			CM_SPEC,	CCHC},
{"XQ", /* "Execute Program (WO),*/			CM_SPEC,	CXQ},
	};

//////////////////////////////////////////////////////////////////////////
//
// YaskawaLegend Driver
//

// Instantiator

INSTANTIATE(CYaskawaTCPDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CYaskawaTCPDriver::CYaskawaTCPDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_PositionFormat = 64;

	m_VariableFormat = 64;

	m_uKeep = 0;
	}

// Destructor

CYaskawaTCPDriver::~CYaskawaTCPDriver(void)
{
	}

// Configuration

void MCALL CYaskawaTCPDriver::Load(LPCBYTE pData)
{ 
	}
	
// Management

void MCALL CYaskawaTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CYaskawaTCPDriver::Open(void)
{
	m_pCL = (YaskawaTCPCmdDef FAR * )CL;
	}

// Device

CCODE MCALL CYaskawaTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP     = GetLong(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskawaTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep >= 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskawaTCPDriver::Ping(void)
{
	if( !OpenSocket() ) {

//		if ( YDEBUG ) AfxTrace0("\r\nNO SOCKET ");

		return CCODE_ERROR;
		}

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = CAC;
	Addr.a.m_Type   = addrLongAsLong;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CYaskawaTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Table == addrNamed ? Addr.a.m_Offset : Addr.a.m_Table;
	
	SetpItem( uOffset );

	if( !m_pItem ) return CCODE_ERROR | CCODE_HARD;

	if( ReadNoTransmit(pData) ) return 1;

	if ( !OpenSocket() ) return CCODE_ERROR;

	if ( Read( Addr ) ) {

		if ( Transact(TRUE) ) {

			GetResponse( pData, Addr.a.m_Type );

			UpdateFormats( (char *)m_bRx);

			return 1;
			}
		}

	m_Error = (uOffset << 16) + LOWORD(m_Error)+1;

	CloseSocket(FALSE);

	return CCODE_ERROR;
	}

CCODE MCALL CYaskawaTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uWriteResult;

	UINT uOffset = Addr.a.m_Table == addrNamed ? Addr.a.m_Offset : Addr.a.m_Table;

	SetpItem( uOffset );

	if( !m_pItem ) return CCODE_ERROR | CCODE_HARD;

	if( WriteNoTransmit(pData) ) return 1;

	if ( !OpenSocket() ) return CCODE_ERROR;

	uWriteResult = Write(Addr, pData);

	switch ( uWriteResult ) {

		case 1:
			Transact(FALSE);
			break;

		case 2:
			Transact(TRUE);
			break;

		default:
			break;
		}
	
	return 1;
	}

// PRIVATE METHODS

// Opcode Handlers

BOOL CYaskawaTCPDriver::Read(AREF Addr)
{
	StartFrame();

	BOOL fNamed = Addr.a.m_Table == addrNamed ? TRUE : FALSE;

	UINT uAd = fNamed ? Addr.a.m_Offset : Addr.a.m_Table;

	UINT uAx = fNamed ? 0 : Addr.a.m_Offset;

	if ( IsUserVariable() ) AddVarRead( uAd );

	else {
		AddCommand();

		switch( m_pItem->Type ) {

			case CM_READ1:
				if ( m_pItem->uID == CVE ) AddByte('?');
				break;

			case CM_READ2:
				SetAxisLetter( uAx );
				break;

			case CM_BOTH0:
			case CM_BOTH1:
			case CM_BOTH2:
			case CM_BOTH3:
			case CM_BOTH4:
				AddByte('?');
				break;

			case CM_SPEC:
				return FALSE;
			default:
				return FALSE;
			}
		}

	return TRUE;
	}

void CYaskawaTCPDriver::AddVarRead( UINT uAd )
{
	AddByte('Y');
	AddByte('P'+ uAd - FIRSTUSERVAR );
	AddByte('=');
	}

UINT CYaskawaTCPDriver::Write(AREF Addr, PDWORD pData)
{
	DWORD dCheck = 0;

	char cStart[16] = {0};

	char cReturn[16] = {0};
 
	StartFrame();

	BOOL fNamed = Addr.a.m_Table == addrNamed ? TRUE : FALSE;

	UINT uAxisPattern = fNamed ? Addr.a.m_Offset : 0;

	UINT uReply = 1;

	switch( m_pItem->Type ) {

		case CM_BOTH0:
		case CM_COM0:
			AddCommand();
			if ( uAxisPattern == 0 ) AddByte('0');
			else SetAxisData( uAxisPattern, 0L, 10 );
			break;

		case CM_BOTH1:
		case CM_COM1:
			AddCommand();
			if ( uAxisPattern == 0 ) AddByte('1');
			else SetAxisData( uAxisPattern, 1L, 10 );
			break;

		case CM_BOTH2:
		case CM_WRITE1:
		case CM_COM2:
			if ( Addr.a.m_Type == addrLongAsLong ) {
				if ( m_pItem->uID != COB )
					SPrintf( cReturn, "%8.8ld", *pData );
				else SPrintf( cReturn, "%1d,%1d",uAxisPattern,
					*pData ? 1 : 0
					);
				}

			else if ( Addr.a.m_Type == addrRealAsReal ) {

				if ( *pData != 0 ) { // protect against invalid Integers
					dCheck = (*pData) & 0x7F800000;
					dCheck >>= 23;
					if ( dCheck < 100 ) // > 8 decimal places
						*pData = 0;
					if ( dCheck > 160 ) // > 10 digits
						*pData = 0x501502F8 | (*pData & 0x80000000); // 9999999999
					}

				SPrintf( cStart, "%f", PassFloat(*pData) );

				SetPrec(m_pItem->uID);
				AdjustNumber( cStart, cReturn );
				}

			if ( IsUserVariable() ) { 
				AddVarRead(fNamed ? Addr.a.m_Offset : Addr.a.m_Table);
				uReply = 2;
				}
			else {
				AddCommand();

				if ( m_pItem->uID == POSITIONFORMAT ) {
					m_PositionFormat = *pData;
					}

				else if ( m_pItem->uID == VARIABLEFORMAT ) {
					m_VariableFormat = *pData;
					}
				}

			AddParam( cReturn );
			break;

		case CM_BOTH3:
		case CM_WRITE2:
		case CM_COM3:
			AddCommand();
			SetAxisLetter( uAxisPattern );
			break;

		case CM_BOTH4:
		case CM_COM4:
			AddCommand();
			break;

		case CM_SPEC:
			switch( m_pItem->uID ) {
				case CIH:
					IPToString(m_IHIP);
					SPrintf( cReturn, "IH%c=%s <%d> %d",
						SetHandleLetter(m_IHHandle, 6),
						cFormat,
						m_IHPort,
						(m_IHProtocol > 2) ? 0 : m_IHProtocol
						);
					AddParam( cReturn );
					break;

				case CIHE:
					SPrintf( cReturn, "IH%c= -%c",
						SetHandleLetter(m_IHHandle, 6),
						(*pData & 1) ? '1' : '2'
						);
					AddParam(cReturn);
					break;

				case CCH:
					SPrintf( cReturn, "CH%c= %c,%c",
						SetHandleLetter(m_CHAxis, 8),
						SetHandleLetter(m_CHSend, 6),
						SetHandleLetter(m_CHReceive, 6)
						);
					AddParam(cReturn);
					break;

				case CXQ:
				case CHX:
					if ( LOWORD(*pData) > 0x7FFF ) return 0;

					SPrintf( cReturn, "%s %d", m_pItem->sName, (UINT)*pData );
					AddParam(cReturn);
					break;
				default:
					break;
				}
			break;
		}

	return uReply;
	}

// Socket Management

BOOL CYaskawaTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

//			if ( YDEBUG ) AfxTrace0("\r\nPHASE ERROR ");

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

//			if ( YDEBUG ) AfxTrace0("\r\nPHASE CLOSING ");

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

//		if ( YDEBUG ) AfxTrace0("\r\nNot Keep ");

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

//					if ( YDEBUG ) AfxTrace0("\r\nCreate: PHASE CLOSING ");

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

//					if ( YDEBUG ) AfxTrace0("\r\nCreate: PHASE ERROR ");

					break;
					}

				Sleep(10);
				}

//			if ( YDEBUG ) AfxTrace0("\r\nCreate: TIMEOUT ");

			CloseSocket(TRUE);

			return FALSE;
			}

//		if ( YDEBUG ) AfxTrace0("\r\nNOT CONNECT ");
		}

	return FALSE;
	}

void CYaskawaTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Header Building

void CYaskawaTCPDriver::StartFrame(void)
{
	m_uPtr = 0;
	}

void CYaskawaTCPDriver::EndFrame(void)
{
	AddByte( CR );
	}

void CYaskawaTCPDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CYaskawaTCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CYaskawaTCPDriver::AddData(PDWORD pData, WORD wCount)
{
	for ( UINT i = 0; i < wCount; i++ ) {

		m_bTx[m_uPtr++] = LOBYTE(LOWORD(*(pData+i)));

		m_bTx[m_uPtr++] = HIBYTE(LOWORD(*(pData+i)));
		}
	}

void CYaskawaTCPDriver::AddCommand( void )
{
	PutText( m_pItem->sName );

	AddByte( ' ' );

	return;
	}

void CYaskawaTCPDriver::AddParam( char * cParam )
{
	for ( UINT i = 0; i < strlen(cParam); i++ ) AddByte(cParam[i]);
	}
	
void CYaskawaTCPDriver::PutText(LPCTXT pCmd)
{
	for( UINT i=0; pCmd[i]; i++ ) AddByte( BYTE(pCmd[i]) );
	}

// Transport Layer

BOOL CYaskawaTCPDriver::Transact(BOOL fWantReply)
{
	if ( Send() ) {

		if ( !fWantReply ) return TRUE;

		return GetReply();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CYaskawaTCPDriver::Send(void)
{
	EndFrame();

	UINT uSize = sizeof(m_bRx);

	m_pCtx->m_pSock->Recv(m_bRx, uSize);

	uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaTCPDriver::GetReply(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - m_uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( m_bRx[uPtr-1] == ':' ) return TRUE;

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}	

	return FALSE;
	}

BOOL CYaskawaTCPDriver::GetResponse( PDWORD pData, UINT uType )
{
	char * s;
	char s1[31];

	switch( m_bRx[0] ) {
		case '?':
		case ':':
		case CR :
			*pData = m_bRx[0];
			return TRUE;
		}

	if ( uType == addrRealAsReal ) {

		s = (char *)(&m_bRx[0]);

		*pData = ATOF(s); // CHECK??? 9 June - *pData OK

		UpdateFormats( s );
		}

	else *pData = GetNumber();

	return TRUE;
	}

// Helpers

void CYaskawaTCPDriver::SetpItem(UINT uID)
{
	m_pItem = m_pCL;

	for ( UINT i = 0; i < elements(CL); i++ ) {

		if ( uID == m_pItem->uID ) return;

		m_pItem++;
		}

	m_pItem = m_pCL + FIRSTUSERVAR+9;
	}

BOOL CYaskawaTCPDriver::ReadNoTransmit(PDWORD pData)
{
	if ( m_pItem->Type == CM_SPEC ) {

		switch ( m_pItem->uID ) {
			case CIH:
			case CIHE:
			case CCH:
				*pData = 0;
				return TRUE;
			case CXQ:
			case CHX:
				*pData = 0xFFFFFFFF;
				return TRUE;
			case CIHA:
				*pData = m_IHHandle;
				return TRUE;
			case CIHB:
				*pData = m_IHIP;
				return TRUE;
			case CIHC:
				*pData = m_IHPort;
				return TRUE;
			case CIHD:
				*pData = m_IHProtocol;
				return TRUE;
			case CCHA:
				*pData = m_CHAxis;
				return TRUE;
			case CCHB:
				*pData = m_CHSend;
				return TRUE;
			case CCHC:
				*pData = m_CHReceive;
				return TRUE;
			default:
				break;
			}
		}

	else if ( m_pItem->Type > CM_BOTH4 ) {

		*pData = 0L;

		return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaTCPDriver::WriteNoTransmit(PDWORD pData)
{
	if ( m_pItem->Type < CM_BOTH0 ) return TRUE;

	else if ( m_pItem->Type == CM_SPEC ) {

		switch ( m_pItem->uID ) {
			case CIHA:
				m_IHHandle = LOWORD(*pData);
				return TRUE;
			case CIHB:
				m_IHIP = *pData;
				return TRUE;
			case CIHC:
				m_IHPort = LOWORD(*pData);
				return TRUE;
			case CIHD:
				m_IHProtocol = LOWORD(*pData);
				return TRUE;
			case CCHA:
				m_CHAxis = LOWORD(*pData);
				return TRUE;
			case CCHB:
				m_CHSend = LOWORD(*pData);
				return TRUE;
			case CCHC:
				m_CHReceive = LOWORD(*pData);
				return TRUE;
			default:
				break;
			}
		}

	return FALSE;
	}

void CYaskawaTCPDriver::SetAxisLetter( UINT uAxes )
{
	if( uAxes && uAxes <= 8 ) {

		AddByte(' ');

		if ( uAxes == 1 ) AddByte('X');
		if ( uAxes == 2 ) AddByte('Y');
		if ( uAxes == 3 ) AddByte('Z');
		if ( uAxes == 4 ) AddByte('W');
		}
	}

void CYaskawaTCPDriver::SetAxisData( UINT uAxes, DWORD dData, DWORD uMaxFactor )
{
	AddByte(' ');
	switch (uAxes) {
		case 4: AddByte(',');
		case 3: AddByte(',');
		case 2: AddByte(',');
		case 1:
			m_dPrec = 0;
			PutNumber( dData);
			break;
		}
	}

void CYaskawaTCPDriver::SetPrec( UINT uID )
{
	if ( IsUserVariable() ) {
		m_dPrec = m_VariableFormat%10;
		return;
		}

	switch (uID) {
		case CAO: // AO
		case CGR: // GR
		case CIL: // IL
		case CIT: // IT
		case COF: // OF
		case CTL: // TL
		case CTT: // TT
		case CVR: // VR
		case CVT: // VT
			m_dPrec = 4; break; // 4 decimal places

		case CKD: // KD
		case CKI: // KI
		case CKP: // KP
			m_dPrec = 3; break; // 3 decimal places

		case CFA: // FA
			m_dPrec = 2; break; // 2 decimal places

		case CBL: // BL
		case CDE: // DE
		case CDP: // DP
		case CER: // ER
		case CFL: // FL
		case CIP: // IP
		case CJG: // JG
		case CPA: // PA
		case CPR: // PR
		case CRL: // RL
		case CSP: // SP
		case CTD: // TD
		case CTE: // TE
		case CTP: // TP
			m_dPrec = m_PositionFormat % 10;
			return;

		case POSITIONFORMAT: // PF
		case VARIABLEFORMAT: // VF
			m_dPrec = 1;
			break;

		default:
			m_dPrec = 0;
			break;
		}
	return;
	}

void CYaskawaTCPDriver::PutNumber(DWORD wNum)
{
	char c[16] = {0};

	if ( wNum & 0x80000000 ) {

		AddByte('-');

		wNum = (wNum^0xFFFFFFFF)+1;
		}

	SPrintf( c, "%ld", wNum );

	for ( UINT i = 0; i < strlen(c); i++ ) AddByte( c[i] );
	}

DWORD CYaskawaTCPDriver::GetNumber( void )
{
	UINT i = 0;
	UINT uRad = 10;
	char s1[32];
	char *s2 = s1;

	if ( m_bRx[0] == ' ' ) i++;

	if ( m_bRx[i] == '$' ) {

		i++;

		uRad = 16;
		}

	return ( strtol( (char *)(&m_bRx[i]), &s2, uRad ) );
	}

BOOL CYaskawaTCPDriver::IsUserVariable( void )
{
	return (m_pItem->uID >= FIRSTUSERVAR && m_pItem->uID <= FIRSTUSERVAR+9);
	}

DWORD CYaskawaTCPDriver::DWordToFloat( DWORD dDWord, DWORD dDP )
{
	return FloatToDWord( (float( dDWord )) / dDP );
	}

DWORD CYaskawaTCPDriver::FloatToDWord( float fFloat )
{
	return DWORD( *((DWORD*) &fFloat));
	}

void CYaskawaTCPDriver::AdjustNumber( char * cStart, char * cReturn )
{
	UINT uCt = 0;
	UINT iCt = 0;
	BOOL fDP = FALSE;
	DWORD dFrac = 0;
	DWORD dWanted;
	char sInt[16] = {0};
	char sFrame[16] = {0};

	while ( iCt < strlen( cStart ) ) {

		if ( cStart[iCt] >= '0' && cStart[iCt] <= '9' ) {

			if ( fDP ) {

				uCt++;

				dFrac = ( dFrac*10 ) + cStart[iCt] - '0';
				}
			}

		else if ( cStart[iCt] == '.' ) {

			fDP = TRUE;

			for ( UINT j = 0; j <= iCt; j++ )
				sInt[j] = cStart[j];
			}

		iCt++;
		}

	dWanted = m_dPrec;

	iCt = uCt;

	if ( uCt > dWanted ) {
		while ( uCt-- > dWanted ) dFrac = (dFrac+5)/10;
		}
	else if ( uCt < dWanted ) {
		while ( uCt++ < dWanted ) dFrac *= 10;
		}

	SPrintf( sFrame, "%%%1ld.%1ldld", m_dPrec, m_dPrec );

	SPrintf( sFrame, sFrame, dFrac );

	SPrintf( cReturn, "%s%s", sInt, sFrame );
	}

void CYaskawaTCPDriver::UpdateFormats( char * s )
{
	PDWORD p;

	switch ( m_pItem->uID ) {

		case POSITIONFORMAT:
			p = &m_PositionFormat;
			break;

		case VARIABLEFORMAT:
			p = &m_VariableFormat;
			break;

		default:
			return;
		}

	UINT uPos = 0;
	DWORD d = 0L;

	while ( s[uPos] != '.' ) uPos++;

	d = ATOI( s );

	d = (d*10) + s[uPos+1] - '0';

	*p = d;

	return;
	}

void	CYaskawaTCPDriver::IPToString( DWORD dIP )
{
	memset( cFormat, 0, 32 );

	SPrintf( cFormat, "%3d,%3d,%3d,%3d",
		UINT((dIP>>24)&0xFF),
		UINT((dIP>>16)&0xFF),
		UINT((dIP>>8)&0xFF),
		UINT(dIP&0xFF)
		);
	}

BOOL	CYaskawaTCPDriver::GetHexDigit( UINT * p )
{
	if ( *p >= '0' && *p <= '9' ) {
		*p -= '0';
		return TRUE;
		}

	if ( *p >= 'a' && *p <= 'f' ) {
		*p -= 'W';
		return TRUE;
		}

	if ( *p >= 'A' && *p <= 'F' ) {
		*p -= '7';
		return TRUE;
		}

	else return FALSE;
	}

UINT	CYaskawaTCPDriver::SetHandleLetter( UINT uH, UINT uMax )
{
	if ( uH > 0 && uH <= uMax ) return '@' + uH;

	if ( uH >= '1' && uH <= '8' )
		return (uH < '7' || uMax==8) ? uH + 0x10 : 'A'; // '1'-'8'->'A'-'H'

	if ( uH >= 'A' && uH <= 'H' )
		return (uH < 'G' || uMax==8) ? uH : 'A';

	if ( uH >= 'a' && uH <= 'h' )
		return (uH < 'g' || uMax==8) ? uH & 0x5F : 'A';

	return 'A';
	}

// End of File
