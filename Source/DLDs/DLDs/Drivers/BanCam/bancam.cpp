
#include "intern.hpp"

#include "bancam.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Banner Camera Driver
//

// Timeouts

static UINT const timeWaitTimeout  = 5000;

static UINT const timeWaitDelay    = 10;

static UINT const timeSendTimeout  = 5000;

static UINT const timeSendDelay	   = 10;

static UINT const timeRecvTimeout  = 20000;

static UINT const timeRecvDelay	   = 10;

static UINT const timeBuffDelay    = 100;

// Instantiator

INSTANTIATE(CBannerCamera);

// Constructor

CBannerCamera::CBannerCamera(void)
{
	m_Ident = DRIVER_ID;

	m_pCtx  = NULL;

	m_uKeep = 0;

	m_pHead = NULL;

	m_pTail = NULL;
	}

// Destructor

CBannerCamera::~CBannerCamera(void)
{
	}

// Configuration

void MCALL CBannerCamera::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uType = GetByte(pData);		
		}
	}
	
// Management

void MCALL CBannerCamera::Attach(IPortObject *pPort)
{

	}

void MCALL CBannerCamera::Open(void)
{
	}

// Device

CCODE MCALL CBannerCamera::DeviceOpen(IDevice *pDevice)
{
	CCameraDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_uDevice = GetWord(pData);
			m_pCtx->m_IP	  = GetAddr(pData);
			m_pCtx->m_uPort	  = GetWord(pData);
			m_pCtx->m_fKeep   = TRUE;
			m_pCtx->m_fPing   = TRUE;
			m_pCtx->m_uTime1  = 2000;
			m_pCtx->m_uTime2  = 1000;
			m_pCtx->m_uTime3  =  500;
			m_pCtx->m_uTime4  = GetWord(pData);
			m_pCtx->m_uLast3  = GetTickCount();
			m_pCtx->m_uLast4  = GetTickCount();
			m_pCtx->m_pSock	  = NULL;
			m_pCtx->m_pData   = NULL;

			memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CBannerCamera::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		CloseSocket(FALSE);

		delete m_pCtx->m_pData;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CCameraDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CBannerCamera::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CBannerCamera::ReadImage(PBYTE &pData)
{
	if( OpenSocket() ) {

		if( ReadPresenceImage(pData) || ReadiVuImage(pData) || ReadVeImage(pData) ) {

			m_pCtx->m_uLast4 = GetTickCount();

			return CCODE_SUCCESS;
			}

		if( Ping() == CCODE_SUCCESS ) {

			UINT dt = GetTickCount() - m_pCtx->m_uLast4;

			UINT tt = 1000 * ToTicks(m_pCtx->m_uTime4);

			if( !tt || dt < tt ) {

				pData = NULL;

				return CCODE_SUCCESS;
				}
			}
		}

	CloseSocket(TRUE);

	return CCODE_ERROR;
	}

void MCALL CBannerCamera::SwapImage(PBYTE pData)
{
	delete m_pCtx->m_pData;

	m_pCtx->m_uInfo[0] = GetTimeStamp();

	m_pCtx->m_uInfo[1] = ((BITMAP_RLC *) pData)->Frame;

	m_pCtx->m_pData    = pData;
	}

void MCALL CBannerCamera::KillImage(void)
{
	if( m_pCtx->m_pData ) {
		
		memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

		delete m_pCtx->m_pData;

		m_pCtx->m_pData = NULL;
		}
	}

PCBYTE MCALL CBannerCamera::GetData(UINT uDevice)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			return pCtx->m_pData;
			}
		}

	return NULL;
	}

UINT MCALL CBannerCamera::GetInfo(UINT uDevice, UINT uParam)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

		if( pCtx->m_uDevice == uDevice ) {

			if( uParam < elements(pCtx->m_uInfo) ) {

				return pCtx->m_uInfo[uParam];
				}
			}
		}

	return 0;
	}

BOOL MCALL CBannerCamera::SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	CContext *pCtx = (CContext *) pContext;

	return UploadInspection(pCtx->m_IP, uIndex, pFile);
	}

BOOL MCALL CBannerCamera::LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	CContext *pCtx = (CContext *) pContext;

	return DownloadInspection(pCtx->m_IP, uIndex, pFile);
	}

BOOL MCALL CBannerCamera::UseSetup(PVOID pContext, UINT uIndex)
{
	CContext *pCtx = (CContext *) pContext;

	return ChangeInspection(pCtx->m_IP, uIndex);
	}

// Socket Management

BOOL CBannerCamera::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CBannerCamera::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast3;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {
			
					m_pCtx->m_uLast4 = GetTickCount();

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CBannerCamera::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock  = NULL;

		m_pCtx->m_uLast3 = GetTickCount();

		m_uKeep--;
		}
	}

// Implementation

BOOL CBannerCamera::ReadHeader(HEADER_PPV &Header)
{
	if( Read(PBYTE(&Header), sizeof(Header)) ) {

		Header.Size  = MotorToHost(Header.Size);

		Header.Frame = MotorToHost(Header.Frame);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBannerCamera::ReadPresenceImage(PBYTE &pImage)
{
	if( m_uType == 0 ) {

		HEADER_PPV Header;

		if( ReadHeader(Header) ) {

			if( strncmp(Header.ID, "PPVS", 4) ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			PBYTE pData = new BYTE [ Header.Size ];

			if( Read(pData, Header.Size) ) {

				if( !strncmp(PTXT(pData), "BM", 2) ) {

					UINT uSkip = IntelToHost(PDWORD(pData+0x0A)[0]);

					int     cx = IntelToHost(PDWORD(pData+0x12)[0]);

					int     cy = IntelToHost(PDWORD(pData+0x12)[1]);

					int planes = IntelToHost( PWORD(pData+0x1A)[0]);

					int   bits = IntelToHost( PWORD(pData+0x1C)[0]);

					PBYTE pSrc = pData + uSkip;

					if( bits == 8 ) {
						
						UINT  uSize = cx * cy;

						PBYTE pWork = new BYTE [ sizeof(BITMAP_RLC) + uSize ];
					
						BITMAP_RLC *pBitmap = (BITMAP_RLC *) pWork;

						pBitmap->Frame  = Header.Frame;

						pBitmap->Format = image8bitGreyScale;

						pBitmap->Width  = cx;

						pBitmap->Height = cy;

						pBitmap->Stride = cx;

						memcpy(pBitmap->Data, pSrc, uSize);
						
						delete pData;

						pImage = pWork;
						
						return TRUE;
						}

					if( bits == 24 ) {						
						
						UINT  uSize = cx * cy * 3;

						PBYTE pWork = new BYTE [ sizeof(BITMAP_RLC) + uSize ];
					
						BITMAP_RLC *pBitmap = (BITMAP_RLC *) pWork;

						pBitmap->Frame  = Header.Frame;

						pBitmap->Format = image24bitColor;

						pBitmap->Width  = cx;

						pBitmap->Height = cy;

						pBitmap->Stride = cx * 3;

						memcpy(pBitmap->Data, pSrc, uSize);
						
						delete pData;

						pImage = pWork;

						return TRUE;
						}
					}
				}

			delete pData;
			}
		}

	return FALSE;
	}

BOOL CBannerCamera::ReadHeader(HEADER_IVU &Header)
{
	if( Read(PBYTE(&Header), sizeof(Header)) ) {

		Header.Size   = IntelToHost(Header.Size);

		Header.Frame  = IntelToHost(Header.Frame);

		Header.Width  = IntelToHost(Header.Width);

		Header.Height = IntelToHost(Header.Height);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBannerCamera::ReadiVuImage(PBYTE &pImage)
{
	if( m_uType == 1 ) {
		
		HEADER_IVU Header;

		if( ReadHeader(Header) ) {

			if( strncmp(Header.ID, "IVU PLUS IMAGE", 16) ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			PBYTE pData = new BYTE [ Header.Size ];

			if( Read(pData, Header.Size) ) {

				if( !strncmp(PTXT(pData), "BM", 2) ) {

					UINT uSkip = IntelToHost(PDWORD(pData+0x0A)[0]);

					int     cx = IntelToHost(PDWORD(pData+0x12)[0]);

					int     cy = IntelToHost(PDWORD(pData+0x12)[1]);

					int planes = IntelToHost( PWORD(pData+0x1A)[0]);

					int   bits = IntelToHost( PWORD(pData+0x1C)[0]);

					if( bits == 8 ) {
						
						UINT  uSize = cx * cy;

						PBYTE pWork = new BYTE [ sizeof(BITMAP_RLC) + uSize ];
					
						PBYTE pSrc  = pData + uSkip;

						BITMAP_RLC *pBitmap = (BITMAP_RLC *) pWork;

						pBitmap->Frame  = Header.Frame;

						pBitmap->Format = image8bitGreyScale;

						pBitmap->Width  = cx;

						pBitmap->Height = cy;

						pBitmap->Stride = cx;

						memcpy(pBitmap->Data, pSrc, uSize);
						
						delete pData;

						pImage = pWork;
						
						return TRUE;
						}

					if( bits == 24 ) {						
						
						UINT  uSize = cx * cy * 3;

						PBYTE pWork = new BYTE [ sizeof(BITMAP_RLC) + uSize ];
					
						PBYTE pSrc  = pData + uSkip;

						BITMAP_RLC *pBitmap = (BITMAP_RLC *) pWork;

						pBitmap->Frame  = Header.Frame;

						pBitmap->Format = image24bitColor;

						pBitmap->Width  = cx;

						pBitmap->Height = cy;

						pBitmap->Stride = cx * 3;

						memcpy(pBitmap->Data, pSrc, uSize);
						
						delete pData;

						pImage = pWork;

						return TRUE;
						}
					}
				}

			delete pData;
			}		
		}

	return FALSE;
	}

BOOL CBannerCamera::ReadHeader(HEADER_VE &Header)
{
	if( Read(PBYTE(&Header), sizeof(Header)) ) {

		Header.Size   = IntelToHost(Header.Size);

		Header.Frame  = IntelToHost(Header.Frame);

		Header.Width  = IntelToHost(Header.Width);

		Header.Height = IntelToHost(Header.Height);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBannerCamera::ReadVeImage(PBYTE &pImage)
{
	if( m_uType == 2 ) {
		
		HEADER_VE Header;

		if( ReadHeader(Header) ) {

			if( strncmp(Header.ID, "VE IMAGE", 16) ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			PBYTE pData = new BYTE [ Header.Size ];

			if( Read(pData, Header.Size) ) {

				if( !strncmp(PTXT(pData), "BM", 2) ) {
					
					UINT uSkip = IntelToHost(PDWORD(pData+0x0A)[0]);

					int     cx = IntelToHost(PDWORD(pData+0x12)[0]);

					int     cy = IntelToHost(PDWORD(pData+0x12)[1]);

					int planes = IntelToHost( PWORD(pData+0x1A)[0]);

					int   bits = IntelToHost( PWORD(pData+0x1C)[0]);

					bool fFlip = false;
					
					if( cy < 0 ) {

						cy = -cy;

						fFlip = true;
						}

					if( bits == 8 ) {
						
						UINT  uSize = cx * cy;

						PBYTE pWork = new BYTE [ sizeof(BITMAP_RLC) + uSize ];
					
						PBYTE pSrc  = pData + uSkip;

						BITMAP_RLC *pBitmap = (BITMAP_RLC *) pWork;

						pBitmap->Frame  = Header.Frame;

						pBitmap->Format = image8bitGreyScale;
						
						pBitmap->Width  = cx;

						pBitmap->Height = cy;

						pBitmap->Stride = cx;
						
						if( fFlip ) {

							for( UINT x = 0; x < cx; x++ ) {

								for( UINT y = 0; y < cy; y++ ) {

									pBitmap->Data[x + (cy - y - 1) * cx] = pSrc[x + cx * y];
									}
								}
							}
						else {
							memcpy(pBitmap->Data, pSrc, uSize);
							}
						
						delete [] pData;

						pImage = pWork;
						
						return TRUE;
						}

					if( bits == 24 ) {						
						
						UINT  uSize = cx * cy * 3;

						PBYTE pWork = new BYTE [ sizeof(BITMAP_RLC) + uSize ];
					
						PBYTE pSrc  = pData + uSkip;

						BITMAP_RLC *pBitmap = (BITMAP_RLC *) pWork;

						pBitmap->Frame  = Header.Frame;

						pBitmap->Format = image24bitColor;

						pBitmap->Width  = cx;

						pBitmap->Height = cy;

						pBitmap->Stride = cx * 3;

						if( fFlip ) {

							UINT sx = cx * 3;

							for( UINT x = 0; x < sx; x++ ) {

								for( UINT y = 0; y < cy; y++ ) {

									pBitmap->Data[x + (cy - y - 1) * sx] = pSrc[x + sx * y];
									}
								}
							}
						else {
							memcpy(pBitmap->Data, pSrc, uSize);
							}
						
						delete [] pData;

						pImage = pWork;

						return TRUE;
						}
					}
				}

			delete [] pData;
			}		
		}

	return FALSE;
	}

void CBannerCamera::ShowData(PBYTE pData, UINT uSize, BOOL fPrint)
{
	for( UINT n = 0; n < uSize; n ++ ) {

		if( n && !(n % 16) ) {
			
			AfxTrace("\n");
			}

		BYTE b = *pData++;

		if( fPrint && /*isprint(b)*/isalnum(b) ) {

			AfxTrace(" %c ", b);
			}
		else 
			AfxTrace("%2.2X ", b);			
		}

	AfxTrace("\n");
	}

BOOL CBannerCamera::Read(PBYTE pData, UINT uCount)
{
	UINT uTotal = 0;

	SetTimer(m_pCtx->m_uTime1);
	
	while( GetTimer() ) {

		if( uTotal == uCount ) {

			return TRUE;
			}

		UINT uAvail = uCount - uTotal;

		if( m_pCtx->m_pSock->Recv(pData + uTotal, uAvail) == S_OK ) {

			SetTimer(m_pCtx->m_uTime1);

			uTotal += uAvail;
			}
		else {
			UINT Phase = 0;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				Sleep(5);
				}
			else
				break;
			}
		}

	return FALSE;
	}

// Inspection Transfer

BOOL CBannerCamera::UploadInspection(DWORD IP, UINT uIndex, FILE *pFile)
{
	ISocket *pSock = CreateSocket(IP_TCP);

	if( pSock ) {

		pSock->SetOption(OPT_RECV_QUEUE, 255);

		if( pSock->Connect((IPADDR &) IP, 80) == S_OK ) {

			if( WaitOpen(pSock) ) {

				if( UploadSendRequest(pSock, uIndex) ) {

					if( UploadRecvReply(pSock, pFile) ) {

						pSock->Release();

						return TRUE;
						}
					}
				}

			pSock->Abort();
			}

		pSock->Release();
		}

	return FALSE;
	}

BOOL CBannerCamera::DownloadInspection(DWORD IP, UINT uIndex, FILE *pFile)
{
	ISocket *pSock = CreateSocket(IP_TCP);

	if( pSock ) {

		pSock->SetOption(OPT_SEND_QUEUE, 255);

		if( pSock->Connect((IPADDR &) IP, 80) == S_OK ) {

			if( WaitOpen(pSock) ) {

				if( DownloadSendRequest(pSock, uIndex, pFile) ) {

					if( DownloadRecvReply(pSock) ) {

						pSock->Release();

						return TRUE;
						}
					}
				}

			pSock->Abort();
			}

		pSock->Release();
		}

	return FALSE;
	}

BOOL CBannerCamera::ChangeInspection(DWORD IP, UINT uIndex)
{
	ISocket *pSock = CreateSocket(IP_TCP);

	if( pSock ) {

		if( pSock->Connect((IPADDR &) IP, 80) == S_OK ) {

			if( WaitOpen(pSock) ) {

				if( ChangeSendRequest(pSock, uIndex) ) {

					if( ChangeRecvReply(pSock) ) {

						pSock->Release();

						return TRUE;
						}
					}
				}

			pSock->Abort();
			}

		pSock->Release();
		}

	return FALSE;
	}

BOOL CBannerCamera::UploadSendRequest(ISocket *pSock, UINT uIndex)
{
	static char const sHead[] = {

		"POST /form/GetIns HTTP/1.1\r\n"
		"Content-Type: application/x-www-form-urlencoded\r\n"
		"Content-Length: %u\r\n"
		"\r\n"
		"location=%u\r\n"

		};

	if( pSock->SetOption(OPT_LINGER, 1) == S_OK ) {

		UINT uLength = 12;

		if( uIndex >= 10  ) uLength += 1;

		if( uIndex >= 100 ) uLength += 1;

		if( Form(pSock, sHead, uLength, uIndex) ) {

			pSock->Close();

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBannerCamera::UploadRecvReply(ISocket *pSock, FILE *pFile)
{
	SetTimer(timeRecvTimeout);

	UINT uState = 0;

	UINT uSize  = 0;

	UINT uCount = 0;

	UINT uHead  = 0;

	char sHead[512];

	while( GetTimer() ) {

		UINT Phase;

		pSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {
		
			CBuffer *pBuff = NULL;

			if( pSock->Recv(pBuff) == S_OK ) {

				if( uState == 0 ) {

					UINT n = pBuff->GetSize();

					PTXT p = PTXT(pBuff->GetData());

					for( UINT i = 0; i < n; i++ ) {

						sHead[uHead++] = p[i];

						if( p[i] == '\n' ) {

							PCTXT p2 = "\n\n";

							PCTXT p4 = "\r\n\r\n";

							if( !strncmp(sHead + uHead - 2, p2, 2) ) {

								break;
								}

							if( !strncmp(sHead + uHead - 4, p4, 4) ) {

								break;
								}
							}
						}

					if( i < n ) {

						sHead[uHead++] = 0;

						if( !UploadScanReply(sHead, uSize) ) {

							pBuff->Release();

							pSock->Abort();

							return FALSE;
							}

						pBuff->StripHead(i+1);

						uState = 1;
						}
					}

				if( uState == 1 ) {

					UINT  n = pBuff->GetSize();

					PBYTE p = pBuff->GetData();

					fwrite(p, 1, n, pFile);

					uCount += n;

					if( uCount >= uSize ) {

						pBuff->Release();

						return TRUE;
						}
					}

				pBuff->Release();

				SetTimer(timeRecvTimeout / 2);

				continue;
				}
			}

		if( Phase == PHASE_CLOSING ) {

			break;
			}

		if( Phase == PHASE_ERROR ) {
			
			break;
			}

		Sleep(timeRecvDelay);
		}

	return FALSE;
	}

BOOL CBannerCamera::UploadScanReply(PTXT pHead, UINT &uSize)
{
	if( ATOI(pHead+9) == 200 ) {

		for(;;) {

			PTXT pFind = strchr(pHead, '\n');

			if( pFind ) {

				*pFind = 0;

				if( pFind[-1] == '\r' ) {

					pFind[-1] = 0;
					}

				if( *pHead ) {

					PTXT pSep = strchr(pHead, ':');

					if( pSep ) {

						*pSep++ = 0;

						if( !stricmp(pHead, "Content-Length") ) {

							uSize = ATOI(pSep);
							}
						}

					pHead = pFind + 1;

					continue;
					}

				return uSize ? TRUE : FALSE;
				}

			break;
			}
		}

	return FALSE;
	}

BOOL CBannerCamera::DownloadSendRequest(ISocket *pSock, UINT uIndex, FILE *pFile)
{
	static char const sHead[] = {

		"POST /form/SaveIns HTTP/1.1\r\n"
		"Content-Type: multipart/form-data; boundary=----snickerdoodle\r\n"
		"Content-Length: %u\r\n"
		"\r\n"

		};

	static char const sFile[] = {

		"------snickerdoodle\r\n"
		"Content-Disposition: form-data; name=\"filedata\"; filename=\"name\"\r\n"
		"Content-Type: application/octet-stream\r\n"
		"\r\n"

		};

	static char const sTail[] = {

		"\r\n"
		"------snickerdoodle\r\n"
		"Content-Disposition: form-data; name=\"location\"\r\n"
		"\r\n"
		"%u\r\n"
		"------snickerdoodle--\r\n"

		};

	if( pSock->SetOption(OPT_LINGER, 1) == S_OK ) {

		fseek(pFile, 0, SEEK_END);

		DWORD dwFile  = ftell(pFile);

		fseek(pFile, 0, SEEK_SET);

		UINT  uLength = strlen(sFile) + strlen(sTail) + dwFile;

		if( uIndex <  10  ) uLength -= 1;

		if( uIndex >= 100 ) uLength += 1;

		if( Form(pSock, sHead, uLength) ) {
			
			if( Form(pSock, sFile) ) {

				if( SendFile(pSock, pFile) ) {

					if( Form(pSock, sTail, uIndex) ) {

						pSock->Close();

						return TRUE;
						}
					}
				}
			}
		}
	
	return FALSE;
	}

BOOL CBannerCamera::DownloadRecvReply(ISocket *pSock)
{
	SetTimer(timeRecvTimeout);

	UINT uState = 0;

	UINT uHead  = 0;

	char sHead[512];

	while( GetTimer() ) {

		UINT Phase;

		pSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {
		
			CBuffer *pBuff = NULL;

			if( pSock->Recv(pBuff) == S_OK ) {

				if( uState == 0 ) {

					UINT n = pBuff->GetSize();

					PTXT p = PTXT(pBuff->GetData());

					for( UINT i = 0; i < n; i++ ) {

						sHead[uHead++] = p[i];

						if( p[i] == '\n' ) {

							PCTXT p2 = "\n\n";

							PCTXT p4 = "\r\n\r\n";

							if( !strncmp(sHead + uHead - 2, p2, 2) ) {

								break;
								}

							if( !strncmp(sHead + uHead - 4, p4, 4) ) {

								break;
								}
							}
						}

					if( i < n ) {

						sHead[uHead++] = 0;

						if( !DownloadScanReply(sHead) ) {

							pBuff->Release();

							pSock->Abort();

							return FALSE;
							}

						pBuff->StripHead(i+1);

						uState = 1;
						}
					}

				pBuff->Release();

				SetTimer(timeRecvTimeout / 2);

				continue;
				}
			}

		if( Phase == PHASE_CLOSING ) {

			return TRUE;
			}

		if( Phase == PHASE_ERROR ) {
			
			break;
			}

		Sleep(timeRecvDelay);
		}

	return FALSE;
	}

BOOL CBannerCamera::DownloadScanReply(PTXT pHead)
{
	if( ATOI(pHead+9) == 200 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CBannerCamera::ChangeSendRequest(ISocket *pSock, UINT uIndex)
{
	static char const sHead[] = {

		"POST /form/ProdChange HTTP/1.1\r\n"
		"Content-Type: application/x-www-form-urlencoded\r\n"
		"Content-Length: %u\r\n"
		"\r\n"
		"location=%u\r\n"

		};

	if( pSock->SetOption(OPT_LINGER, 1) == S_OK ) {

		UINT uLength = 12;

		if( uIndex >= 10  ) uLength += 1;

		if( uIndex >= 100 ) uLength += 1;

		if( Form(pSock, sHead, uLength, uIndex) ) {

			pSock->Close();

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBannerCamera::ChangeRecvReply(ISocket *pSock)
{
	SetTimer(timeRecvTimeout);

	UINT uState = 0;

	UINT uHead  = 0;

	char sHead[512];

	while( GetTimer() ) {

		UINT Phase;

		pSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {
		
			CBuffer *pBuff = NULL;

			if( pSock->Recv(pBuff) == S_OK ) {

				if( uState == 0 ) {

					UINT n = pBuff->GetSize();

					PTXT p = PTXT(pBuff->GetData());

					for( UINT i = 0; i < n; i++ ) {

						sHead[uHead++] = p[i];

						if( p[i] == '\n' ) {

							PCTXT p2 = "\n\n";

							PCTXT p4 = "\r\n\r\n";

							if( !strncmp(sHead + uHead - 2, p2, 2) ) {

								break;
								}

							if( !strncmp(sHead + uHead - 4, p4, 4) ) {

								break;
								}
							}
						}

					if( i < n ) {

						sHead[uHead++] = 0;

						if( !ChangeScanReply(sHead) ) {

							pBuff->Release();

							pSock->Abort();

							return FALSE;
							}

						pBuff->StripHead(i+1);

						uState = 1;
						}
					}

				pBuff->Release();

				SetTimer(timeRecvTimeout / 2);

				continue;
				}
			}

		if( Phase == PHASE_CLOSING ) {

			return TRUE;
			}

		if( Phase == PHASE_ERROR ) {
			
			break;
			}

		Sleep(timeRecvDelay);
		}

	return FALSE;
	}

BOOL CBannerCamera::ChangeScanReply(PTXT pHead)
{
	if( ATOI(pHead+9) == 200 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CBannerCamera::Form(ISocket *pSock, PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	BOOL fResult = Send(pSock, pText, pArgs);

	va_end(pArgs);

	return fResult;
	}

BOOL CBannerCamera::Send(ISocket *pSock, PCTXT pText, va_list pArgs)
{
	UINT uSize = strlen(pText);

	PTXT pWork = new char [ uSize + 256 ];

	VSPrintf(pWork, pText, pArgs);

	if( Send(pSock, PCBYTE(pWork), strlen(pWork)) ) {

		delete [] pWork;

		return TRUE;
		}

	delete [] pWork;

	return FALSE;
	}

BOOL CBannerCamera::Send(ISocket *pSock, PCBYTE pText, UINT uSize)
{
	UINT uLimit = 1024;

	while( uSize ) {

		CBuffer *pBuff = CreateBuffer(uLimit, TRUE);

		if( pBuff ) {

			PBYTE pData = pBuff->GetData();

			UINT  uCopy = min(uLimit, uSize);

			memcpy(pData, pText, uCopy);

			pBuff->AddTail(uCopy);

			SetTimer(timeSendTimeout);

			while( pSock->Send(pBuff) == E_FAIL ) {

				UINT Phase;

				pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN && GetTimer() ) {

					Sleep(timeSendDelay);

					continue;
					}

				BuffRelease(pBuff);

				return FALSE;
				}

			pText += uCopy;

			uSize -= uCopy;
			}
		else
			Sleep(timeBuffDelay);
		}

	return TRUE;
	}

BOOL CBannerCamera::SendFile(ISocket *pSock, FILE *pFile)
{
	UINT uLimit = 1280;

	UINT uChunk = uLimit;

	for(;;) {

		CBuffer *pBuff = CreateBuffer(uLimit, TRUE);

		if( pBuff ) {

			PBYTE pData = pBuff->GetData();

			UINT  uSize = fread(pData, 1, uChunk, pFile);

			if( uSize ) {

				pBuff->AddTail(uSize);

				SetTimer(timeSendTimeout);

				while( pSock->Send(pBuff) == E_FAIL ) {

					UINT Phase;

					pSock->GetPhase(Phase);

					if( Phase == PHASE_OPEN ) {
						
						if( GetTimer() ) {

							Sleep(timeSendDelay);

							continue;
							}
						}

					BuffRelease(pBuff);

					return FALSE;
					}
				}
			else {
				BuffRelease(pBuff);

				return TRUE;
				}

			continue;
			}

		Sleep(timeBuffDelay);
		}
	}

BOOL CBannerCamera::WaitOpen(ISocket *pSock)
{
	SetTimer(timeWaitTimeout);

	while( GetTimer() ) {

		UINT Phase;

		pSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {

			return TRUE;
			}

		if( Phase == PHASE_CLOSING ) {

			pSock->Close();
			}

		if( Phase == PHASE_ERROR ) {

			break;
			}

		Sleep(timeWaitDelay);
		}

	return FALSE;
	}

// End of File
