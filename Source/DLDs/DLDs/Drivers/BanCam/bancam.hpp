
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CBannerCamera;

//////////////////////////////////////////////////////////////////////////
//
// PPV Header
//

struct HEADER_PPV
{
	DWORD	Size;
	DWORD	Frame;
	char	ID[4];
	DWORD	xx[5];
	};

//////////////////////////////////////////////////////////////////////////
//
// IVU Header
//

#pragma pack(2)

struct HEADER_IVU
{
	char	ID[16];
	DWORD   Version;
	DWORD   Size;
	DWORD	Frame;
	WORD	Width;
	WORD    Height;
	DWORD	xx[8];
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// VE Header
//

#pragma pack(2)

struct HEADER_VE
{
	char	ID[16];
	DWORD   Version;
	DWORD   Size;
	DWORD	Frame;
	WORD	Width;
	WORD    Height;
	DWORD	xx[8];
	};

#pragma pack()

/////////////////////////////////////////////////////////////////////////
//
// Banner Camera Driver
//

class CBannerCamera : public CCameraDriver
{
	public:
		// Constructor
		CBannerCamera(void);

		// Destructor
		~CBannerCamera(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE ) Ping     (void);
		DEFMETH(CCODE ) ReadImage(PBYTE &pData);
		DEFMETH(void  ) SwapImage(PBYTE pData);
		DEFMETH(void  ) KillImage(void);
		
		DEFMETH(PCBYTE) GetData  (UINT uDevice);
		DEFMETH(UINT  ) GetInfo  (UINT uDevice, UINT uParam);
		
		DEFMETH(BOOL  ) SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile);
		DEFMETH(BOOL  ) LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile);
		DEFMETH(BOOL  ) UseSetup (PVOID pContext, UINT uIndex);

	protected:
		// Device Data
		struct CContext
		{
			CContext * m_pNext;
			CContext * m_pPrev;
			UINT	   m_uDevice;
			DWORD	   m_IP;
			UINT	   m_uPort;
			BOOL	   m_fKeep;
			BOOL	   m_fPing;
			UINT	   m_uTime1;
			UINT	   m_uTime2;
			UINT	   m_uTime3;
			UINT	   m_uTime4;
			UINT	   m_uLast3;
			UINT	   m_uLast4;
			PBYTE	   m_pData;
			UINT	   m_uInfo[4];
			ISocket  * m_pSock;
			};

		// Data Members
		CContext * m_pCtx;
		CContext * m_pHead;
		CContext * m_pTail;
		UINT	   m_uKeep;
		UINT	   m_uType;
		
		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Implementation
		BOOL ReadHeader(HEADER_PPV &Header);
		BOOL ReadPresenceImage(PBYTE &pImage);
		BOOL ReadHeader(HEADER_IVU &Header);
		BOOL ReadHeader(HEADER_VE &Header);
		BOOL ReadiVuImage(PBYTE &pData);
		BOOL ReadVeImage(PBYTE &pData);
		BOOL Read(PBYTE pData, UINT uCount);
		void ShowData(PBYTE pData, UINT uSize, BOOL fPrint);		

		// Inspection Transfer
		BOOL SLOW UploadInspection(DWORD IP, UINT uIndex, FILE *pFile);
		BOOL SLOW DownloadInspection(DWORD IP, UINT uIndex, FILE *pFile);
		BOOL SLOW ChangeInspection(DWORD IP, UINT uIndex);
		BOOL SLOW UploadSendRequest(ISocket *pSock, UINT uIndex);
		BOOL SLOW UploadRecvReply(ISocket *pSock, FILE *pFile);
		BOOL SLOW UploadScanReply(PTXT pHead, UINT &uSize);
		BOOL SLOW DownloadSendRequest(ISocket *pSock, UINT uIndex, FILE *pFile);
		BOOL SLOW DownloadRecvReply(ISocket *pSock);
		BOOL SLOW DownloadScanReply(PTXT pHead);
		BOOL SLOW ChangeSendRequest(ISocket *pSock, UINT uIndex);
		BOOL SLOW ChangeRecvReply(ISocket *pSock);
		BOOL SLOW ChangeScanReply(PTXT pHead);
		BOOL SLOW Form(ISocket *pSock, PCTXT pText, ...);
		BOOL SLOW Send(ISocket *pSock, PCTXT pText, va_list pArgs);
		BOOL SLOW Send(ISocket *pSock, PCBYTE pText, UINT uSize);
		BOOL SLOW SendFile(ISocket *pSock, FILE *pFile);
		BOOL SLOW WaitOpen(ISocket *pSock);
	};

// End of File
