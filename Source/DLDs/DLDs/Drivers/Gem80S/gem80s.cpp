
#include "intern.hpp"

#include "gem80s.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Serial Slave Driver
//

// Instantiator

INSTANTIATE(CGem80SlaveSerialDriver);

// Constructor

CGem80SlaveSerialDriver::CGem80SlaveSerialDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Config

void MCALL CGem80SlaveSerialDriver::Load(LPCBYTE pData)
{
	if( GetWord( pData ) == 0x1234 ) {

		m_bDrop = GetByte(pData);

		return;
		}
	}

void MCALL CGem80SlaveSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, FALSE);
	}
	
void MCALL CGem80SlaveSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

// Entry Points

void MCALL CGem80SlaveSerialDriver::Service(void)
{
	for(;;) {
	
		if( !RxSlave() ) {

			continue;
			}
			
		if( m_bRxBuff[0] != 'K' || m_bRxBuff[1] != 'J' ) {

			continue;
			}

		if( !SlaveWrite('J', PWORD(m_bRxBuff + 2)) ) {

			continue;
			}

		if( !SlaveRead('K', PWORD(m_bTxBuff)) ) {

			continue;
			}
			
		TxPacket(0x03);
		}
	}

// Implementation

void CGem80SlaveSerialDriver::TxPacket(BYTE bMode)
{
	Send(STX);
	
	m_CRC.Clear();

	Send((m_bDrop << 4) | bMode);
	
	for( UINT uScan = 0; uScan < m_uPtr; uScan++ ) {

		switch( m_bTxBuff[uScan] ) {
		
			case ENQ:
			case STX:
			case ETX:
			case ETB:
			case EOT:
			case DLE:
				Send(DLE);

			default:
				Send(m_bTxBuff[uScan]);
			}
		}
	
	Send(m_bTerm);

	UINT uTxCRC = m_CRC.GetValue();
	
	Send(uTxCRC % 256);

	Send(uTxCRC / 256);
	}

BOOL CGem80SlaveSerialDriver::RxSlave(void)
{
	UINT uTarget = 0;

	UINT uRxCRC  = 0;

	BOOL fDLE    = FALSE;

	UINT uState  = 0;

	UINT uByte   = 0;

	UINT uRetry  = 3;

	for(;;) {

		if( (uByte = m_pData->Read(FOREVER)) == NOTHING ) {

			continue;
			}

		m_CRC.Add(uByte);
			
		if( !fDLE && uByte == DLE ) {

			if( uState == 0 || uState == 2 ) {

				fDLE = TRUE;

				continue;
				}
			}
			
		switch( uState ) {
		
			case 0:
				switch( uByte ) {
				
					case STX:
						
						m_CRC.Clear();

						uState = 1;

						break;

					case NAK:
						
						return FALSE;
						
					case ACK:

						uState = 5;

						break;
					}
				break;
				
			case 1:
				if( UINT(uByte & 0xF0) == UINT(m_bDrop << 4) ) { 
				
					m_uPtr = 0;

					uState = 2;
					}
				else {
					if( uRetry-- ) {

						m_pData->Write(ENQ, FOREVER);

						uState = 0;

						fDLE = FALSE;

						break;
						}

					return FALSE;
					}
				break;
				
			case 2:
				if( !fDLE ) {

					if( uByte == ETX || uByte == ETB ) {
					
						m_bTerm = uByte;
						
						uTarget = m_CRC.GetValue();

						uState = 3;
						
						break;
						}
					}

				if( m_uPtr < m_uRxSize ) {

					m_bRxBuff[m_uPtr++] = uByte;

					break;
					}

				return FALSE;
				
			case 3:
				uRxCRC = uByte;

				uState = 4;

				break;
				
			case 4:
				if( uTarget == MAKEWORD(uRxCRC, uByte) ) {
					
					return TRUE;
					}

				if( !uRetry-- ) {

					return FALSE;
					}

				m_pData->Write(ENQ, FOREVER);
					
				uState = 0;

				fDLE   = FALSE;

				break;
				
			case 5:
				if( uByte == m_bTerm ) {

					m_pData->Write(ACK, FOREVER);

					return FALSE;
					}

				return FALSE;
			}
		
		fDLE = FALSE;
		}
		
	return FALSE;
	}

BOOL CGem80SlaveSerialDriver::SlaveWrite(UINT uTable, PWORD pData)
{
	DWORD pBuff[128];

	for( UINT u = 0; u < m_uPtr / 2; u++ ) {

		WORD x = PWORD(pData)[u];

		pBuff[u] = LONG(SHORT(HostToIntel(x)));
		}

	CAddress Addr;

	Addr.a.m_Table = uTable;

	Addr.a.m_Offset = 0; 

	Addr.a.m_Type = addrWordAsWord;

	Addr.a.m_Extra = 0;

	if( CSlaveDriver::Write(Addr, pBuff, m_uPtr / 2) == S_OK ) {

		return TRUE;
		}

	return FALSE;

	}

BOOL CGem80SlaveSerialDriver::SlaveRead(UINT uTable, PWORD pData)
{
	DWORD pBuff[128];

	memset(pBuff, 0, 128);

	CAddress Addr;

	Addr.a.m_Table = uTable;

	Addr.a.m_Offset = 0;

	Addr.a.m_Type = addrWordAsWord;

	Addr.a.m_Extra = 0;

	if ( CSlaveDriver::Read(Addr, pBuff, m_uPtr / 2) == S_OK ) {

		for( UINT u = 0; u < m_uPtr / 2; u++ ) {

			WORD x = pBuff[u];

			pData[u] = LONG(SHORT(HostToIntel(x)));
			}
	
		return TRUE;
		}

	return FALSE;
	}

void CGem80SlaveSerialDriver::Send(BYTE bData)
{
	m_pData->Write(bData, FOREVER);

	m_CRC.Add(bData);
	}

// End of File
