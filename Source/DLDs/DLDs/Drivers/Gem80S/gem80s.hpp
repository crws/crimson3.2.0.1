
#include "gem80.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Serial Slave Driver
//

class CGem80SlaveSerialDriver : public CGem80SlaveDriver
{
	public:
		// Constructor
		CGem80SlaveSerialDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Entry Points
		DEFMETH(void) Service(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);

	protected:
		// Data Members
		CRC16 m_CRC;
				
		// Implementation
		void Send(BYTE bData);
		void TxPacket(BYTE bMode);
		BOOL RxSlave(void);
		BOOL SlaveWrite(UINT uTable, PWORD pData);
		BOOL SlaveRead(UINT uTable, PWORD pData);
	};

// End of File
