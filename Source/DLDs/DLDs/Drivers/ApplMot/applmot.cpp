
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "applmot.hpp"

//struct FAR AppliedMotionCmdDef {
//	UINT	uID;
//	char	cOP[4];
//	UINT	Type;
//	};

AppliedMotionCmdDef CODE_SEG CAppliedMotionDriver::APPMOTCL[] = {
// Motion Commands
	{ CAC,  "AC", /*Acceleration*/				RW},
	{ CAM,  "AM", /*Max Acceleration*/			RW},
	{ CCJ,  "CJ", /*Commence Jogging*/			WO},
	{ CDC,  "DC", /*Change Distance*/			RW},
	{ CDE,  "DE", /*Deceleration*/				RW},
	{ CDI,  "DI", /*Distance/Position*/			RW},
	{ CEG,  "EG", /*Electronic Gearing*/			RW},
	{ CEP,  "EP", /*Encoder Position*/			RW},
	{ CFC,  "FC", /*Feed to Length, Spd Change*/		WO},
	{ CFD,  "FD", /*Feed/Double (1=FD, 2=FDX)*/		WO},
	{ CFDA, "FD", /*FD(X) Input 1*/				RW},
	{ CFDB, "FD", /*FD(X) Condition 1*/			RW},
	{ CFDC, "FD", /*FD(X) Input 2*/				RW},
	{ CFDD, "FD", /*FD(X) Condition 2*/			RW},
	{ CFE,  "FE", /*Follow Encoder (INFO1)*/		WO},
	{ CFEX, "FEX",/*Follow Encoder, X*/			WO},
	{ CFLC, "FL", /*Feed/Length Command*/			WO},
	{ CFLV, "FL", /*Feed/Length with Value*/		WO},
	{ CFM,  "FM", /*Feed/Sensor, Mask Dist. (INFO1)*/	WO},
	{ CFMX, "FMX",/*Feed/Sensor, Mask Dist. X*/		WO},
	{ CFO,  "FO", /*Feed/Length + Set Out.*/		WO},
	{ CFOY, "FOY",/*Feed/Length + Set Out. Y*/		WO},
	{ CFPC, "FP", /*Feed/Position Command*/			WO},
	{ CFPV, "FP", /*Feed/Position with Value*/		WO},
	{ CFS,  "FS", /*Feed/Sensor (INFO1)*/			WO},
	{ CFSX, "FSX",/*Feed/Sensor, X*/			WO},
	{ CFY,  "FY", /*Feed/Sensor, Safety Dist. (INFO1)*/	WO},
	{ CFYX, "FYX",/*Feed/Sensor, Safety Dist.X*/		WO},
	{ CHW,  "HW", /*Hand Wheel (INFO1)*/			WO},
	{ CHWX, "HWX",/*Hand Wheel X*/				WO},
	{ CJA,  "JA", /*Jog Acceleration*/			RW},
	{ CJC,  "JC", /*Velocity mode second speed*/		RW},
	{ CJD,  "JD", /*Jog Disable*/				WO},
	{ CJE,  "JE", /*Jog Enable*/				WO},
	{ CJL,  "JL", /*Jog Deceleration*/			RW},
	{ CJM,  "JM", /*Jog Mode*/				RW},
	{ CJS,  "JS", /*Jog Speed*/				RW},
	{ CSH,  "SH", /*Seek Home (INFO1)*/			WO},
	{ CSHX, "SHX",/*Seek Home, X*/				WO},
	{ CSJ,  "SJ", /*Stop Jogging*/				WO},
	{ CSM,  "SM", /*Stop Move (1=SMD, 2=SMM)*/		WO},
	{ CSP,  "SP", /*Set Position*/				RW},
	{ CST,  "ST", /*Stop (1=ST, 2=STD)*/			WO},
	{ CVC,  "VC", /*Velocity Change*/			RW},
	{ CVE,  "VE", /*Velocity*/				RW},
	{ CVM,  "VM", /*Maximum Velocity*/			RW},
// Configuration Commands
	{ CBD,  "BD", /*Brake Disengage Delay*/			RW},
	{ CBE,  "BE", /*Brake Engage Delay*/			RW},
	{ CCC,  "CC", /*Change Current*/			RW},
	{ CCD,  "CD", /*Idle Current Delay Time*/		RW},
	{ CCF,  "CF", /*Anti-resonance Filter Freq.*/		RW},
	{ CCG,  "CG", /*Anti-resonance Filter Gain*/		RW},
	{ CCI,  "CI", /*Change Idle Current*/			RW},
	{ CCM,  "CM", /*Control Mode (Command Mode)*/		RW},
	{ CCP,  "CP", /*Change Peak Current*/			RW},
	{ CDA,  "DA", /*Define Address*/			RW},
	{ CDL,  "DL", /*Define Limits (X6, X7)*/		RW},
	{ CDR,  "DR", /*Data Register (Capture)*/		WO},
	{ CER,  "ER", /*Encoder Resolution*/			RW},
	{ CHG,  "HG", /*4th Harmonic Filter Gain*/		RW},
	{ CHP,  "HP", /*4th Harmonic Filter Phase*/		RW},
	{ CKC,  "KC", /*Overall Servo Filter*/			RW},
	{ CKD,  "KD", /*Differential Constant*/			RW},
	{ CKE,  "KE", /*Differential Filter*/			RW},
	{ CKF,  "KF", /*Velocity FeedFwd Constant*/		RW},
	{ CKI,  "KI", /*Integrator Constant*/			RW},
	{ CKK,  "KK", /*Inertia FeedFwd Constant*/		RW},
	{ CKP,  "KP", /*Proportional Constant*/			RW},
	{ CKV,  "KV", /*Velocity Feedback Constant*/		RW},
	{ CKW,  "KW", /*Velocity Filter*/			RW},
	{ CMO,  "MO", /*Motion Output (Y2)*/			RW},
	{ CPC,  "PC", /*Power-up Current*/			RW},
	{ CPF,  "PF", /*Position Fault/Torque %*/		RW},
	{ CPI,  "PI", /*Power-up Idle Current*/			RW},
	{ CPL,  "PL", /*Position Limit*/			RW},
	{ CPM,  "PM", /*Power-up Mode*/				RW},
	{ CPP,  "PP", /*Power-up Peak current*/			RW},
	{ CSA,  "SA", /*Save Parameters*/			WO},
	{ CSF,  "SF", /*Step Filter Frequency*/			RW},
	{ CSI,  "SI", /*Enable Input Usage*/			RW},
	{ CVI,  "VI", /*Velocity Integrator Constant*/		RW},
	{ CVP,  "VP", /*Velocity Mode Prop. Constant*/		RW},
	{ CZC,  "ZC", /*Regen Resistor Cont. Wattage*/		RW},
	{ CZR,  "ZR", /*Regen Resistor Value*/			RW},
	{ CZT,  "ZT", /*Regen Resistor Peak Time*/		RW},
// I/O Commands
	{ CAD,  "AD", /*Analog DeadBand*/			RW},
	{ CAF,  "AF", /*Analog Filter*/				RW},
	{ CAG,  "AG", /*Analog Velocity Gain*/			RW},
	{ CAI,  "AI", /*Alarm Input Usage*/			RW},
	{ CAO,  "AO", /*Alarm Output (Y3)*/			RW},
	{ CAP,  "AP", /*Analog Position Gain*/			RW},
	{ CAS,  "AS", /*Analog Scaling*/			RW},
	{ CAT,  "AT", /*Analog Threshold*/			RW},
	{ CAV,  "AV", /*Analog Offset Value*/			RW},
	{ CAZ,  "AZ", /*Analog Zero*/				WO},
	{ CBO,  "BO", /*Brake Output (Y1)*/			RW},
	{ CFI,  "FI", /*Filter Input*/				WO},
	{ CFX,  "FX", /*Filter select inputs*/			RW},
	{ CIH,  "IH", /*Immediate High Output*/			WO},
	{ CIHY, "IHY",/*Immediate High Output, Y*/		WO},
	{ CIL,  "IL", /*Immediate Low Output*/			WO},
	{ CILY, "ILY",/*Immediate Low Output, Y*/		WO},
	{ CIS,  "IS", /*Input Status*/				RO},
	{ CISX, "ISX",/*Input Status, X*/			RO},
	{ CSO,  "SO", /*Set Output*/				WO},
	{ CSOY, "SOY",/*Set Output, Y*/				WO},
// Communication Commands
	{ CTD,  "TD", /*Transmit Delay*/			RW},
// Q Program Commands
	{ CCT,  "CT", /*Continue*/				WO},
	{ CMT,  "MT", /*Multi-Tasking*/				RW},
	{ CPS,  "PS", /*Pause*/					WO},
	{ CQC,  "QC", /*Queue Call (QC)*/			WO},
	{ CQCN, "QC", /*Queue Call, (QC<segment #>)*/		WO},
	{ CQD,  "QD", /*Queue Delete*/				WO},
	{ CQE,  "QE", /*Queue Execute*/				WO},
	{ CQK,  "QK", /*Queue Kill (1=QKD,2=QKM)*/		WO},
	{ CQL,  "QL", /*Queue Load*/				WO},
	{ CQS,  "QS", /*Queue Save*/				WO},
	{ CQX,  "QX", /*Queue Load & Execute*/			WO},
	{ CWD,  "WD", /*Wait Delay*/				WO},
	{ CWT,  "WT", /*Wait Time*/				WO},
// Drive Commands
	{ CAL,  "AL", /*Alarm Code*/				RO},
	{ CAX,  "AX", /*Alarm Reset Buffered*/			WO},
	{ CMD,  "MD", /*Motor Disable*/				WO},
	{ CME,  "ME", /*Motor Enable*/				WO},
	{ CMN,  "MN", /*Model Number*/				RO},
	{ CSC,  "SC", /*Status Code*/				RO},
// Register Commands
	{ CMRC, "RC", /*Reg. Counter (see INFO1)*/		WO},
	{ CRCX, "RCX",/*Reg. Counter, X*/			WO},
	{ CRD,  "RD", /*Reg. Decrement*/			WO},
	{ CRI,  "RI", /*Reg. Increment*/			WO},
	{ CRL,  "RL", /*Reg. Load Immediate*/			RW},
	{ CRM,  "RM", /*Reg. Move*/				WO},
	{ CRR,  "RR", /*Reg. Read*/				WO},
	{ CRW,  "RW", /*Reg. Write*/				WO},
	{ CRX,  "RX", /*Reg. Load Buffered*/			RW},
	{ CTS,  "TS", /*Time Stamp*/				WO},
// Immediate Commands
	{ CAR,  "AR", /*Alarm Reset immediate*/			WO},
	{ CBS,  "BS", /*Buffer Status*/				RO},
	{ CCS,  "CS", /*Change Speed*/				RW},
	{ CGC,  "GC", /*Current Command*/			RW},
	{ CIA,  "IA", /*Immediate Analog*/			RO},
	{ CIC,  "IC", /*Immediate Current (Commanded)*/		RO},
	{ CID,  "ID", /*Immediate Distance*/			RO},
	{ CIE,  "IE", /*Immediate Encoder*/			RO},
	{ CIO,  "IO", /*Output Status*/				RW},
	{ CIOY, "IOY",/*Output Status, Y*/			RW},
	{ CIP,  "IP", /*Immediate Position*/			RO},
	{ CIQ,  "IQ", /*Immediate Current (Actual)*/		RO},
	{ CIT,  "IT", /*Immediate Temperature*/			RO},
	{ CIU,  "IU", /*Immediate Voltage*/			RO},
	{ CIVA, "IV", /*Immediate Actual Velocity*/		RO},
	{ CIVT, "IV", /*Immediate Target Velocity*/		RO},
	{ CIX,  "IX", /*Immediate Position Error*/		RO},
	{ CRE,  "RE", /*Restart or Reset*/			WO},
	{ CRS,  "RS", /*Request Status*/			RO},
	{ CRV,  "RV", /*Revision Level*/			RO},
	{ CSK,  "SK", /*Stop & Kill (1=SK, 2=SKD)*/		WO},
	{ CPR,  "PR", /*Protocol Type, not user selectable*/	RW},
	{ CIF,  "IF", /*Immediate Format, not user selectable*/	RW},
	{ CERR,"ERR", /*Error Code Received*/			RW},
	{ CP2D, "HR", /*PC to Drive (Enable=1, Disable=0)*/	RW},
	{ CG2D, "HR", /*Restart Comms after PC Disable*/	RW},
	{ CSGL,	"XX", /*Send user defined string*/		RW},
	{ CQRSP,"XX", /*SGL Error*/				RW}
	};

//////////////////////////////////////////////////////////////////////////
//
// Applied Motion Driver
//

// Instantiator

INSTANTIATE(CAppliedMotionDriver);

// Constructor

CAppliedMotionDriver::CAppliedMotionDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;
	}

// Destructor

CAppliedMotionDriver::~CAppliedMotionDriver(void)
{
	}

// Configuration

void MCALL CAppliedMotionDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CAppliedMotionDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CAppliedMotionDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CAppliedMotionDriver::Open(void)
{
	m_pCL = (AppliedMotionCmdDef FAR *)APPMOTCL;
	}

// Device

CCODE MCALL CAppliedMotionDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx           = new CContext;

			m_pCtx->m_bDrop  = GetByte(pData);

			m_pCtx->m_fPM2Disable = GetByte(pData); //TRUE; // for eric rice 090819 GetByte(pData);

			m_pCtx->m_dFD[0] = '1';
			m_pCtx->m_dFD[2] = '1';

			m_pCtx->m_dFD[1] = 'F';
			m_pCtx->m_dFD[3] = 'F';

			m_pCtx->m_dErrRcvd	= MSGZERO;
			m_pCtx->m_dPrevErr	= MSGZERO;
			m_pCtx->m_dQLData	= 0;
			m_pCtx->m_SGLError	= 0;

			m_pCtx->m_uDisableComms	= 1234;
			m_pCtx->m_uNoAddress	= INIT_INIT;
			m_pCtx->m_uBufferCount	= 0;

			m_pCtx->m_fQLActive	= FALSE;
			m_pCtx->m_fBusy		= FALSE;
			m_pCtx->m_fLikelyLegacy	= FALSE;
			m_pCtx->m_fLoadFlag	= TRUE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CAppliedMotionDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CAppliedMotionDriver::Ping(void)
{
//**/	AfxTrace0("\r\nPING PING PING\r\n\n");
/* Theory of Operation - Applied Motion SW cannot run simultaneously with driver.
1st Ping returns 1 to allow PC2D to get set "On Startup".  This will allow the Applied
Motion SW to continue communicating.  If the first opcode encountered in Read() or Write()
is not PC2D, CCODE_ERROR is returned to permit normal pinging.

With m_pCtx->m_uDisableComms != 1, Ping will then try to establish comms, with SetPR() checking
the current protocol settings, when possible, and modifying as desired for the driver
to continue.

If m_pCtx->m_uDisableComms is set to 1, no external comms occur.  Once that is set to 0, the
HR string is sent to the drive to put it into SCL comms mode.  HR can also be forced by
using the G32D command.
*/
	m_fInPing = TRUE;

	if( m_pCtx->m_fLoadFlag ) {

		m_pCtx->m_fLoadFlag = FALSE;

		return 1; // give "On Startup" a chance to disable comms
		}

	if( m_pCtx->m_uDisableComms == 1 ) return 1; // disable comms command executed

	FirstPass(); // wait for power up code to be received

	m_pCtx->m_uDisableComms	= 0;

	if( InitComms() ) {

		m_fInPing = FALSE;

		TryStatus(); // set Legacy indicator

		return 1;
		}

	m_pCtx->m_fLoadFlag     = TRUE;

	m_pCtx->m_uDisableComms = 1234;

	return CCODE_ERROR;
	}

CCODE MCALL CAppliedMotionDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nT=%d O=%x C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( m_fInPing ) { // comms not established

		Ping();
		}

	m_fBufferedCommand = FALSE;

	UINT uID           = GetUID(Addr);

	// re-ping if On Startup is not attempting to disable comms
	if( m_pCtx->m_uDisableComms == 1234 && !IsInternal(uID) ) {

		Sleep(50);

		return CCODE_ERROR;
		}

	m_pItem = SetCommandPtr(uID);

	if( m_pItem ) {

		if( ReadNoTransmit(pData) ) {

			return 1;
			}

		if( m_pCtx->m_uDisableComms && !IsInternal(uID) ) return CMDNOEX;

		if( !AllowCommand(uID, FALSE) ) return CMDNOEX;

		CCODE cc = uCount;

		switch( Addr.a.m_Type ) {

			case LL:
				cc = DoLongRead(Addr, pData, uCount);
				break;

			case RR:
				cc = DoRealRead(Addr, pData);
				break;
			}

		if( !HIWORD(cc) ) return cc;

		if( cc & CCODE_ERROR ) return CCODE_ERROR;

		return m_fBufferedCommand || m_pCtx->m_fQLActive ? CMDNOEX : CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CAppliedMotionDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\nWRITE*** T=%d O=%x D=%8.8lx ", Addr.a.m_Table, Addr.a.m_Offset, *pData);
//**/	AfxTrace1("C = %d ", uCount);

	m_fBufferedCommand = FALSE;

	UINT uID           = GetUID(Addr);

	// re-ping if On Startup is not attempting to disable comms
	if( m_pCtx->m_uDisableComms == 1234 && !IsInternal(uID) ) return CCODE_ERROR;

	m_pItem = SetCommandPtr(uID);

	if( m_pItem ) {

		if( m_pItem->uID == CSGL ) {

			return DoSGL(pData, uCount);
			}

		if( m_pCtx->m_uDisableComms && !IsInternal(uID) ) return 1;

		if( WriteNoTransmit(Addr.a.m_Offset, *pData) ) return 1;

		if( m_pCtx->m_uDisableComms == 5678 ) return CCODE_ERROR; // re-ping

		if( !AllowCommand(uID, TRUE) ) return 1;

		switch( Addr.a.m_Type ) {

			case LL:
				DoLongWrite(Addr, *pData);
				break;

			case RR:
				DoRealWrite(Addr, *pData);
				break;
			}

		return 1;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CAppliedMotionDriver::PingRW(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsWrite)
{
	UINT uID = GetUID(Addr);

	if( !m_pCtx->m_uDisableComms ) {

		m_pItem = SetCommandPtr(uID);

		if( fIsWrite ) {

			DoLongWrite(Addr, *pData);

			return 1;
			}

		return DoLongRead(Addr, pData, uCount);
		}

	return CCODE_ERROR;
	}
	
// Opcode Handlers
CCODE CAppliedMotionDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame();

	UINT uT = Addr.a.m_Table;

	switch( uT ) {

		case CIVA:	AddByte('0');	break;
		case CIVT:	AddByte('1');	break;
		case CRL:
		case CRX:	AddByte(LOBYTE(Addr.a.m_Offset));	break;
		}

	if( Transact(FALSE) ) {

		DWORD x;

		if( Addr.a.m_Table == CIF ) return 1;

		else if( IsCRSResponse(pData, uCount) )	return uCount;

		else if( m_pItem->uID == CMN ) {

			x = DWORD(m_bRx[0]);
			return 1;
			}

		else if( IsHexCommand() ) {

			x = ReadHexResponse();
			}

		else x = ATOI( (const char * )m_bDt );

		*pData = x;

		return 1;
		}

	if( !m_pCtx->m_fBusy ) return CCODE_ERROR;

	return CCODE_NO_RETRY | CCODE_NO_DATA;	
	}

BOOL  CAppliedMotionDriver::IsCRSResponse(PDWORD pData, UINT uCount)
{
	if( m_pItem->uID == CRS ) {

		DWORD d    = 0;

		PBYTE p    = m_bDt;

		BOOL fC    = FALSE;

		BOOL fBusy = FALSE;

		for( UINT i = 0; i < uCount; i++ ) {

			d = 0;

			for( UINT j = 0; j < 4; j++ ) {

				if( *p == CR ) fC = TRUE;

				if( !fC ) fBusy |= IsMotionLetter(*p);

				d <<= 8;

				d += fC ? 0 : *p;

				p++;
				}

			pData[i] = d;
			}

		m_pCtx->m_fBusy    = fBusy;

		m_pCtx->m_dErrRcvd = fBusy ? MSGBUSY : m_pCtx->m_dPrevErr;

		return TRUE;
		}

	return FALSE;
	}

CCODE CAppliedMotionDriver::DoRealRead(AREF Addr, PDWORD pData)
{
	StartFrame();

	if( Addr.a.m_Table == CIA ) AddByte(LOBYTE(Addr.a.m_Offset));

	if( Transact(FALSE) ) {

		*pData = ATOF( (const char * )m_bDt );

		return 1;
		}

	if( !m_pCtx->m_fBusy ) return CCODE_ERROR;

	return CCODE_NO_RETRY | CCODE_NO_DATA;
	}

void  CAppliedMotionDriver::DoLongWrite(AREF Addr, DWORD dData)
{
	BOOL fTransact = TRUE;

	StartFrame();

	BYTE bData = LOBYTE(dData);
	BYTE bA0   = LOBYTE(Addr.a.m_Offset);

	char c[3] = {0};

	switch(m_pItem->uID) {

		case CDR:
		case CRD:
		case CRI:
		case CDA:
			AddByte(bData);
			break;

		case CFE:
		case CFEX:
		case CFM:
		case CFMX:
		case CFS:
		case CFSX:
		case CFY:
		case CFYX:
		case CHW:
		case CHWX:
		case CMRC:
		case CRCX:
		case CSH:
		case CSHX:
			AddByte(m_pHex[bData & 0xF]);
			AddByte(bA0);
			break;

		case CSO:
		case CSOY:
		case CFO:
		case CFOY:
			AddByte(m_pHex[bA0 & 0xF]);
			AddByte(dData == 2 ? 'H' : 'L');
			break;

		case CFI:
			AddByte(m_pHex[bA0 & 0xF]);
			AddData(dData, FALSE);
			break;

		case CRL:
		case CRR:
		case CRW:
		case CRX:
			AddByte(bA0);
			AddData(dData, FALSE);
			break;

		case CRM:
			AddByte(bA0);
			AddByte(bData);
			break;

		case CWD:
		case CFLV:
		case CFPV:
			AddData(dData, FALSE);
			break;

		case CQCN:
		case CQD:
		case CQL:
		case CQS:
		case CQX:
		case CIH:
		case CIHY:
		case CIL:
		case CILY:
			AddByte(m_pHex[bData & 0xF]);
			break;

		case CIO:
		case CIOY:
			if( bData > 99 ) fTransact = FALSE;

			else if( bData > 9 ) AddByte(m_pHex[bData/10]);

			SPrintf( c, "%1.1d", bData % 10 );

			AddText(LPCTXT(c));
			break;

		case CQK:
		case CSM:
			if( dData == 1 || dData == 2 ) AddByte(dData == 1 ? 'D' : 'M');
			else fTransact = FALSE;
			break;

		case CSK:
		case CST:
			if( dData == 2 ) AddByte('D');
			else fTransact = dData == 1;
			break;

		case CFD:
			if( dData == 2 ) AddByte('X');

			for( dData = 0; dData < 4; dData++ ) AddByte( LOBYTE(m_pCtx->m_dFD[dData]) );

			break;

		default:
			if( m_pItem->Type == RW ) AddData(dData, FALSE);

			else if( m_pItem->Type == RO ) fTransact = FALSE;
			break;
			
		}

	if( fTransact ) Transact(TRUE);
	}

void  CAppliedMotionDriver::DoRealWrite(AREF Addr, DWORD dData)
{
	StartFrame();

	AddData(dData, TRUE);

	Transact(TRUE);
	}

// Frame Building
void  CAppliedMotionDriver::StartFrame(void)
{
	m_uPtr = 0;

	if( m_pCtx->m_uNoAddress < 2 ) AddByte(m_pCtx->m_bDrop);

	AddText(m_pItem->cOP);
	}

void  CAppliedMotionDriver::EndFrame(void)
{
	AddByte(CR);

	m_pData->ClearRx();
	}

void  CAppliedMotionDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr++] = bData;
		}
	}

void  CAppliedMotionDriver::AddData(DWORD dData, BOOL fReal)
{
	if( !(dData & 0x7FFFFFFF) ) {

		AddByte('0');
		return;
		}

	char c[12] = {0};

	if( fReal ) {

		if( dData == 0x80000000 ) dData = 0;

		SPrintf( c, "%f", PassFloat(dData) );
		}

	else SPrintf( c, "%d", dData );

	AddText(PCTXT(c));
	}

void  CAppliedMotionDriver::AddText( LPCTXT pText )
{
	UINT l = strlen(pText);

	if( m_uPtr + l < sizeof(m_bTx) ) {

		for( UINT i = 0; i < l; i++ ) AddByte(pText[i]);
		}
	}

// Transport Layer
BOOL  CAppliedMotionDriver::Transact(BOOL fIsWrite)
{
	EndFrame();

	if( m_pCtx->m_fLikelyLegacy ) Sleep(50);

	Send();

	return GetReply(fIsWrite);
	}

void  CAppliedMotionDriver::Send(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

BOOL  CAppliedMotionDriver::GetReply(BOOL fIsWrite)
{
	WORD wData;

	UINT uPtr  = 0;

	UINT uTime = 500;

	SetTimer(uTime);

//**/	AfxTrace0("\r\n.");

	while( GetTimer() ) {

		if( (wData = Get(uTime)) < LOWORD(NOTHING) ) {

			BYTE bData = LOBYTE(wData);

//**/			AfxTrace1("<%2.2x>", bData);

			m_bRx[uPtr++] = bData;

			if( bData == CR ) {

				switch( CheckReply(fIsWrite) ) {

					case 0: return FALSE;
					case 1: return TRUE;
					}
 
				uPtr  = 0; // Not this reply, wait
				uTime = 500;
				SetTimer(uTime);
				}

			if( m_pItem->uID == CMN ) return TRUE;

			if( uPtr >= sizeof(m_bRx) ) return FALSE;
			}
		}

	return FALSE;
	}

UINT  CAppliedMotionDriver::CheckReply(BOOL fIsWrite)
{
	if( m_fInPing ) {

		return HandlePingReply(fIsWrite);
		}

	PBYTE pT = m_bTx;
	PBYTE pR = m_bRx;

	if( m_pItem->uID == CSGL ) {

		if( m_bRx[1] == '%' || m_bRx[1] == '*' ) m_pCtx->m_SGLError = 0;

		else {
			UINT i      = 1;
			UINT uStart = 1;
			BYTE b;

			while( (b = m_bRx[i]) != CR ) {

				if( b == '=' ) { // read command sent

					uStart = i+1;
					break;
					}

				i++;
				}

			m_pCtx->m_SGLError = 0;

			for( i = uStart; i < uStart+4; i++ ) {

				if( (b = m_bRx[i]) != CR ) {

					m_pCtx->m_SGLError <<= 8;

					m_pCtx->m_SGLError += b;
					}

				else return 1;
				}
			}

		return 1;
		}

	m_pCtx->m_fBusy  = FALSE;

	BOOL fNoA = m_pCtx->m_uNoAddress == 2;

	UINT uPtr = fNoA ? 0 : 1;

	switch( pR[uPtr] ) {

		case  CR: return 0;

		case '%': return fIsWrite ? 1 : 0;

		case '*':
			if( fIsWrite && m_pCtx->m_fQLActive ) {

				m_pCtx->m_fBusy = FALSE;

				return 1;
				}

			m_pCtx->m_fBusy = !fIsWrite;

			return fIsWrite ? 1 : 0;

		case '?':
			m_pCtx->m_dErrRcvd = 0;

			m_pCtx->m_dErrRcvd += DWORD(m_pItem->cOP[0]) << 24;
			m_pCtx->m_dErrRcvd += DWORD(m_pItem->cOP[1]) << 16;

				if( pR[uPtr+1] != CR ) {

				m_pCtx->m_dErrRcvd += DWORD(pR[uPtr+1]) << 8;

				if( pR[uPtr+2] != CR ) m_pCtx->m_dErrRcvd += DWORD(pR[uPtr+2]);
				}

			else m_pCtx->m_dErrRcvd += 0x2020;

			m_pCtx->m_dPrevErr = m_pCtx->m_dErrRcvd;

			return fIsWrite ? 1 : 0;

		default:
			if( (pR[0] != pT[0]) ||
			    (pR[1] != pT[1]) ||
			    (!fNoA && (pR[2] != pT[2]))
			    )
			    return 2;

			if( fIsWrite ) return 1;
			break;
		}

	uPtr = fNoA ? 2 : 3;

	switch( m_pItem->uID ) {

		case CRL:
		case CRX:
			if( pR[uPtr] != m_bTx[uPtr] ) return 0;
			uPtr++;
			break;
		}

	if( pR[uPtr++] != '=' ) return 0;

	UINT uDPtr = 0;

	memset(m_bDt, 0, sizeof(m_bDt));

	while( pR[uPtr] != CR ) {

		m_bDt[uDPtr++] = pR[uPtr++];

		if( uDPtr >= sizeof(m_bDt) ) return 0;
		}

	m_bDt[uDPtr] = 0;

	return 1;
	}

UINT  CAppliedMotionDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Initializing

void  CAppliedMotionDriver::FirstPass(void)
{
	UINT u;

	UINT uTime = 500;

	SetTimer(uTime);

	while( GetTimer() ) {

		if( (u = Get(uTime)) == NOTHING ) {

			continue;
			}

		uTime = 50;

		SetTimer(uTime); // start-up string coming in from device
		}
	}

BOOL  CAppliedMotionDriver::InitComms(void)
{
	UINT *pNoA  = &m_pCtx->m_uNoAddress;

	if( *pNoA ) return ReadPM(*pNoA);	// comms and Legacy previously established 

	if( !m_pCtx->m_fPM2Disable ) {

		WritePM();
		}

	if( ReadPM(INIT_ADDY) ) {		// try this address

		*pNoA = INIT_ADDY;		// Drive uses address

		return TRUE;
		}

	if( ReadPM(INIT_ADDN) ) {		// try read with no address

		*pNoA = INIT_ADDN;		// disable sending address

		return TRUE;
		}

	return FALSE;				// no response to ReadPM
	}

void  CAppliedMotionDriver::WritePM(void)
{
 	CAddress Addr;

	Addr.a.m_Table	= AN;
	Addr.a.m_Offset	= CPM;
	Addr.a.m_Type	= LL;

	DWORD Data[1];

	Data[0] = MODE_SCLE;

	m_pCtx->m_uNoAddress = INIT_ADDN;

	PingRW(Addr, Data, 1, TRUE);

	m_pCtx->m_uNoAddress = 0;
	}

BOOL  CAppliedMotionDriver::ReadPM(UINT uAdd)
{
	CAddress Addr;

	Addr.a.m_Table	= AN;
	Addr.a.m_Offset	= CPM;
	Addr.a.m_Type	= LL;

	DWORD Data[1];

	UINT uSave = m_pCtx->m_uNoAddress;

	m_pCtx->m_uNoAddress = uAdd;

	memset(m_bRx, 0, 5);

	if( PingRW(Addr, Data, 1, FALSE) == 1 ) {

		switch( Data[0] ) {

			case MODE_SCLE:
			case MODE_SCLD:
			case MODE_QPRG:
				return TRUE;

			default:
				return FALSE;
			}
		}

	m_pCtx->m_uNoAddress = uSave;

	return FALSE;
	}

UINT  CAppliedMotionDriver::HandlePingReply(BOOL fIsWrite)
{
	if( !fIsWrite ) {

		m_bDt[1] = 0;

		if( m_bRx[0] ) {

			for( UINT k = 2; k < 4; k++ ) {	// PM=2 or nPM=2 are only options other than none

				if( m_bRx[k] == '=' ) {

					m_bDt[0] = m_bRx[k+1];

					return 1;
					}
				}
			}

		return 0;
		}

	return 1;
	}

void  CAppliedMotionDriver::TryStatus(void)
{
	DWORD Data[1];

	if( GetSC(Data) ) { // legacy units will not respond

		SetImmediateDecMode(); // unit may not respond

		if( !(*Data & IMMMASK) ) {

			SetPR(); // attempt to set protocol as desired
			}

		m_pCtx->m_fLikelyLegacy = FALSE;

		return;
		}

	m_pCtx->m_fLikelyLegacy = TRUE;
	}

CCODE CAppliedMotionDriver::SetPR(void)
{
	CAddress Addr;
	DWORD    Data[1];

	Addr.a.m_Table  = AN;
	Addr.a.m_Offset = CPR;
	Addr.a.m_Type   = LL;

	if( Read(Addr, Data, 1) == 1 ) {

		Data[0] &= (BUND | B485);
		Data[0] |=  BADD | BACK;

		Write(Addr, Data, 1); // if no reply, we'll find out later
		}

	return 1;
	}

BOOL  CAppliedMotionDriver::SetImmediateDecMode(void) // Decimal responses to Immediate Commands
{
	m_uPtr = 0;

	m_pItem = m_pCL;

	AddByte(m_pCtx->m_bDrop);
	AddByte('I');
	AddByte('F');
	AddByte('D');

	return Transact(TRUE) || m_bRx[0] == '%';
	}

// Immediate Only Checking

BOOL  CAppliedMotionDriver::IsImmediateCommand(UINT uID)
{
	if( uID & IMMCMD ) return TRUE;

	switch( uID ) {

		case CIA:
		case CRL:
		case CRS:
		case CSC:
			return TRUE; // immediate commands in other lists
		}

	return FALSE;
	}

BOOL  CAppliedMotionDriver::AllowCommand(UINT uID, BOOL fIsWrite)
{
	if( IsImmediateCommand(uID) ) {

		if( !m_pCtx->m_fQLActive || fIsWrite ) return TRUE;

		return uID == CQS || uID == CQE || uID == CSK || uID == CQK;
		}

	if( m_pCtx->m_fQLActive ) {

		if( m_pCtx->m_uBufferCount++ < SEGSIZE ) return TRUE;

		DWORD Data[1];

		m_pCtx->m_fQLActive = !SendSK(Data);

		return FALSE;
		}

	if( fIsWrite ) return TRUE;

	m_fBufferedCommand = AllowBufferedCommand(uID);

	return m_fBufferedCommand;
	}

BOOL  CAppliedMotionDriver::AllowBufferedCommand(UINT uID)
{
	DWORD Data[1];

	if( m_pCtx->m_fLikelyLegacy ) {

		return GetBS(Data) && (Data[0] > 20);
		}

	return GetSC(Data) && !(Data[0] & IMMMASK);
	}

BOOL  CAppliedMotionDriver::GetSC(PDWORD pData)
{
	return DoSpecial(CSC, pData, TRUE);
	}

BOOL  CAppliedMotionDriver::GetBS(PDWORD pData)
{
	return DoSpecial(CBS, pData, TRUE);
	}

BOOL  CAppliedMotionDriver::SendQS(PDWORD pData)
{
	return DoSpecial(CQS, pData, FALSE);
	}

BOOL  CAppliedMotionDriver::SendSK(PDWORD pData)
{
	return DoSpecial(CSK, pData, FALSE);
	}

BOOL  CAppliedMotionDriver::SendQL(PDWORD pData)
{
	return DoSpecial(CQL, pData, FALSE);
	}

BOOL  CAppliedMotionDriver::SendQLnQE(DWORD dData)
{
	if( DoSpecial(CQL, &dData, FALSE) ) {

		Sleep(10);

		return DoSpecial(CQE, &dData, FALSE);
		}

	return TRUE;
	}

BOOL  CAppliedMotionDriver::DoSpecial(UINT uOffset, PDWORD pData, BOOL fIsRead)
{
	AppliedMotionCmdDef *p = m_pItem;

	BOOL f = FALSE;

	if( fIsRead ) {

		CAddress A;

		A.a.m_Table  = AN;
		A.a.m_Offset = uOffset;
		A.a.m_Type   = LL;

		f = Read(A, pData, 1) == 1;
		}

	else {
		m_pItem    = SetCommandPtr(uOffset);

		BYTE bData = m_pHex[LOBYTE(*pData)];

		StartFrame();

		switch( uOffset ) {

			case CQS:
				AddByte(bData);
				break;

			case CQL:
				if( IsValidSegment(*pData) ) AddByte(bData);

				else {
					m_pCtx->m_uBufferCount = 0;

					if( !bData ) return FALSE;
					}

				break;
			}

		f = Transact(TRUE);
		}

	m_pItem = p;

	return f;
	}

// Command Identification

AppliedMotionCmdDef * CAppliedMotionDriver::SetCommandPtr(UINT uID)
{
	AppliedMotionCmdDef *pItem = m_pCL;

	for( UINT i = 0; i < elements(APPMOTCL); i++ ) {

		if( uID == pItem->uID ) return pItem;

		pItem++;
		}

	return NULL;
	}

UINT  CAppliedMotionDriver::GetUID(AREF Addr)
{
	return Addr.a.m_Table == AN ? Addr.a.m_Offset : Addr.a.m_Table;
	}

BOOL  CAppliedMotionDriver::IsMotionLetter(BYTE b)
{
	switch( b ) {

		case 'H': // drive in motion indicators
		case 'J':
		case 'M':
		case 'S':
		case 'T':
		case 'W':
			return TRUE;
		}

	return FALSE;
	}

BOOL  CAppliedMotionDriver::IsHexCommand(void)
{
	switch( m_pItem->uID ) {

		case CAL:
		case CIO:
		case CIS:
		case CSC:
			return TRUE;
		}

	return FALSE;
	}

BOOL  CAppliedMotionDriver::IsInternal(UINT uID)
{
	return uID == CP2D || uID == CG2D || uID == CERR;
	}

CCODE CAppliedMotionDriver::DoSGL(PDWORD pData, UINT uCount)
{
	m_uPtr     = 0;

	BOOL fDone = FALSE;

	AddByte(m_pCtx->m_bDrop);

	for( UINT i = 0; !fDone && (i < uCount); i++ ) {

		UINT n     = 32;

		DWORD d    = pData[i];

		while( n && !fDone ) {

			n     -= 8;

			BYTE b = d >> n;

			if( b ) AddByte(b);

			else fDone = TRUE;
			}
		}

	Transact(TRUE);

	return uCount;
	}

// Helpers

BOOL  CAppliedMotionDriver::ReadNoTransmit(PDWORD pData)
{
	UINT u = m_pItem->uID;

	Sleep(20);

	switch( u ) {

		case CERR:
			*pData = m_pCtx->m_dErrRcvd;
			return TRUE;

		case CQRSP:
			*pData = m_pCtx->m_SGLError;
			return TRUE;

		case CFD:
		case CG2D:
			*pData = 0;
			return TRUE;

		case CFDA:
		case CFDC:
			*pData = m_pCtx->m_dFD[u - CFDA] - '0';
			return TRUE;

		case CFDB:
		case CFDD:
			*pData = m_pCtx->m_dFD[u - CFDA];
			return TRUE;

		case CRR:
		case CRW:
			*pData = 0;
			return TRUE;

		case CQCN:
			*pData = RTNZERO;
			return TRUE;

		case CP2D:
			if( m_pCtx->m_uDisableComms > 1 ) *pData = m_pCtx->m_uDisableComms;
			else *pData = m_pCtx->m_uDisableComms ? 1 : 0;
			return TRUE;

		case CQS:
		case CQE:
		case CSO:
		case CSOY:
		case CFO:
		case CFOY:
			*pData = 0;
			break;

		case CQL:
			*pData = (m_pCtx->m_dQLData % 100) + m_pCtx->m_fQLActive ? 100 : 0;
			break;

		case CSGL:
			*pData = 0x78780000; // "xx";
			return TRUE;
		}

	return m_pItem->Type == WO;
	}

BOOL  CAppliedMotionDriver::WriteNoTransmit(UINT uOffset, DWORD dData)
{
	UINT u = m_pItem->uID;

	switch( u ) {

		case CERR:
			m_pCtx->m_dErrRcvd = MSGZERO;
			return TRUE;

		case CQRSP:
			m_pCtx->m_SGLError = 0;
			return TRUE;

		case CFDA:
		case CFDC:
			m_pCtx->m_dFD[u - CFDA] = m_pHex[dData & 0xF];
			return TRUE;

		case CFDB:
		case CFDD:
			m_pCtx->m_dFD[u - CFDA] = dData;
			return TRUE;

		case CWD:
		case CRD:
		case CRI:
		case CRM:
		case CDR:
			return dData > 255;

		case CRL:
		case CRX:
			return uOffset > 255;

		case CDA:
			return dData < '!' || dData > '@' || dData == '%' || dData == '*' || dData == '?';

		case CFD:
		case CQK:
		case CSK:
		case CSM:
		case CST:
		case CSO:
		case CSOY:
		case CFO:
		case CFOY:
			return dData < 1 || dData > 2;

		case CP2D:
			switch( m_pCtx->m_uDisableComms ) {

				case 0:
					break;

				case 1234:
					m_pCtx->m_uDisableComms = 5678;
					return TRUE;

				case 5678:
					return BOOL(dData);

				default:
					if( !dData ) SendHR();
					break;
				}

			m_pCtx->m_uDisableComms = dData ? 1 : 0;

			return TRUE;

		case CG2D:
			if( SendHR() ) m_pCtx->m_uDisableComms = 0;
			else if( SendHR() ) m_pCtx->m_uDisableComms = 0;
			return TRUE;

		case CQCN:
		case CQD:
		case CQX:
			return !IsValidSegment(dData);

		case CQL:
			if( m_pCtx->m_fQLActive ) return TRUE;

			DWORD d;

			if( dData > SEGMAX ) { // send QL by itself

				if( GetSC(&d) && (d & IMMMASK) ) return TRUE; // disallow when motor running

				m_pCtx->m_fQLActive = SendQL(&dData);

				m_pCtx->m_dQLData   = 0;

				return TRUE;
				}

			m_pCtx->m_dQLData = IsValidSegment(dData) ? dData : 0;

			return TRUE;

		case CQS:
			if( IsValidSegment(dData) && m_pCtx->m_fQLActive ) {

				m_pCtx->m_fQLActive = !SendQS(&dData);

				if( !m_pCtx->m_fQLActive ) {

					GetBS(&dData);
					}
				}

			return TRUE;

		case CQE:
			if( dData ) {

				dData = m_pCtx->m_dQLData % 100;

				if( IsValidSegment(dData) ) SendQLnQE(dData);
				}

			return TRUE;
		}

	return m_pItem->Type == RO;
	}

BOOL  CAppliedMotionDriver::IsValidSegment(DWORD dData)
{
	return dData >= SEGMIN && dData <= SEGMAX;
	}

DWORD CAppliedMotionDriver::ReadHexResponse(void)
{
	DWORD dResult = 0;
	PBYTE pD      = m_bDt;

	if( m_pItem->uID == CAL || m_pItem->uID == CSC ) {

		dResult  = (pD[0] - '0') << 12;
		dResult += (pD[1] - '0') <<  8;
		dResult += (pD[2] - '0') <<  4;
		dResult += (pD[3] - '0');

		return dResult;
		}

	while( *pD == '1' || *pD == '0' ) {

		dResult <<= 1;

		dResult |= *pD - '0';

		pD++;
		}

	return dResult;
	}

BOOL  CAppliedMotionDriver::SendHR(void)
{
	m_bRx[0] = 0;

	m_uPtr = 0;

	AddByte(m_pCtx->m_bDrop);
	AddByte('H');
	AddByte('R');

	Transact(FALSE);

	return BOOL(m_bRx[0]);
	}

// End of File
