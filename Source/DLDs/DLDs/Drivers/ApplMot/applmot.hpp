//////////////////////////////////////////////////////////////////////////
//
// Applied Motion Driver (3399)
//

#define	AN	addrNamed
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	RO	1
#define	RW	2
#define	WO	3

// Protocol bit definitions
#define	BUND	0x01
#define	BADD	0x02
#define	BACK	0x04
#define	BCHK	0x08
#define	B485	0x10
#define	BRLC	0x20

// 5 decimal 0's or 4 hex 0's
#define	RTNZERO	409600000

// Buffered command unavailable
#define CMDNOEX	CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA

// Immediate Command Indicator (OK to send when in motion)
#define	IMMCMD	0x8000

// Segment Values
#define	SEGMIN	 1
#define	SEGMAX	12
#define	SEGSIZE	62

// Error messages
#define	MSGBUSY	0x42555359
#define	MSGZERO 0x20202030

/* Allow Immediate Commands Only
From Applied Motion:
SC bit 14 takes precedence over all other bits in that if it's set then
buffered commands will not be allowed at all, regardless of what else the
drive might be doing.

You are correct that bit 4 will be set whenever bit 5, 6, or 10 is set.

Buffered commands may also not execute immediately when bit 1, 2, 7, 8, 9,
11, 12, 13, or 15 is set.

That only leaves bits 0 and 3 as the really "safe" bits for buffered
commands.
*/
#define	IMMMASK	0xFDF2 // any of these bits set = Immediate only

#define	MODE_SCLE	2	// PM - SCL comms, drive enabled
#define MODE_SCLD	5	// PM - SCL comms, drive disabled
#define	MODE_QPRG	7	// PM - Q program

// Address Status
#define	INIT_INIT	0
#define	INIT_ADDY	1
#define	INIT_ADDN	2

struct FAR AppliedMotionCmdDef {
	UINT	uID;
	char	cOP[4];
	UINT	Type;
	};

class CAppliedMotionDriver : public CMasterDriver
{
	public:
		// Constructor
		CAppliedMotionDriver(void);

		// Destructor
		~CAppliedMotionDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BOOL	m_fPM2Disable;

			DWORD	m_dFD[4];
			DWORD	m_dErrRcvd;
			DWORD	m_dPrevErr;
			DWORD	m_dQLData;
 			DWORD	m_SGLError;

			UINT	m_uDisableComms;
			UINT	m_uNoAddress;
			UINT	m_uBufferCount;

			BOOL	m_fQLActive;
			BOOL	m_fBusy;
			BOOL	m_fLikelyLegacy;
			BOOL	m_fLoadFlag;
			};

		CContext * m_pCtx;

		// Data Members
		BYTE	m_bTx[32];
		BYTE	m_bRx[32];
		BYTE	m_bDt[16];

		LPCTXT	m_pHex;

		UINT	m_uPtr;

		BOOL	m_fInPing;
		BOOL	m_fBufferedCommand;

		// Command Table
		static	AppliedMotionCmdDef CODE_SEG APPMOTCL[];
		AppliedMotionCmdDef FAR * m_pCL;
		AppliedMotionCmdDef FAR * m_pItem;

		// Ping Handling
		CCODE	PingRW(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsWrite);
	
		// Opcode Handlers
		CCODE	DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	IsCRSResponse(PDWORD pData, UINT uCount);
		CCODE	DoRealRead(AREF Addr, PDWORD pData);
		void	DoLongWrite(AREF Addr, DWORD dData);
		void	DoRealWrite(AREF Addr, DWORD dData);
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, BOOL fReal);
		void	AddText( LPCTXT pText );
		
		// Transport Layer
		BOOL	Transact(BOOL fIsWrite);
		void	Send(void);
		BOOL	GetReply(BOOL fIsWrite);
		UINT	CheckReply(BOOL fIsWrite);
		UINT	Get(UINT uTime);

		// Initializing
		void	FirstPass(void);
		BOOL	InitComms(void);
		void	WritePM(void);
		BOOL	ReadPM(UINT uAdd);
		UINT	HandlePingReply(BOOL fIsWrite);
		void	TryStatus(void);
		CCODE	SetPR(void);
		BOOL	SetImmediateDecMode(void);

		// Immediate Only Checking
		BOOL	IsImmediateCommand(UINT uID);
		BOOL	AllowCommand(UINT uID, BOOL fIsWrite);
		BOOL	AllowBufferedCommand(UINT uID);
		BOOL	GetSC(PDWORD pData);
		BOOL	GetBS(PDWORD pData);
		BOOL	SendQS(PDWORD pData);
		BOOL	SendSK(PDWORD pData);
		BOOL	SendQL(PDWORD pData);
		BOOL	SendQLnQE(DWORD dData);
		BOOL	DoSpecial(UINT uOffset, PDWORD pData, BOOL fIsRead);

		// Command Identification
		AppliedMotionCmdDef * SetCommandPtr(UINT uID);
		UINT	GetUID(AREF Addr);
		BOOL	IsMotionLetter(BYTE b);
		BOOL	IsHexCommand(void);
		BOOL	IsInternal(UINT uID);

		// SGL Handling
		CCODE	DoSGL(PDWORD pData, UINT uCount);

		// Helpers
		BOOL	ReadNoTransmit(PDWORD pData);
		BOOL	WriteNoTransmit(UINT uOffset, DWORD dData);
		BOOL	IsValidSegment(DWORD dData);
		DWORD	ReadHexResponse(void);
		BOOL	SendHR(void);
	};

// Table Items
//#define	CRD	1 // now 0x4001
//#define	CRI	2 // now 0x4002
#define	CRL	3 
#define	CRM	4 
#define	CRR	5 
#define	CRW	6 
#define	CRX	7 
#define	CFE	8
#define	CFEX	9
#define	CFM	10
#define	CFMX	11
#define	CFS	12
#define	CFSX	13
#define	CFY	14
#define	CFYX	15
#define	CHW	16
#define	CHWX	17
#define	CMRC	18
#define	CRCX	19
#define	CSH	20
#define	CSHX	21
#define	CFI	22
#define	CIA	23
#define	CSO	24
#define	CSOY	25
#define	CFO	26
#define	CFOY	27
#define	CRS	28
#define	CFDA	29
#define	CFDB	30
#define	CFDC	31
#define	CFDD	32
#define	CSGL	33

// Named Items
#define	CPR	0xF0
#define	CIF	0xF1
#define	CAC	0x0101
#define	CAM	0x0102
#define	CCJ	0x0103
#define	CDC	0x0104
#define	CDE	0x0105
#define	CDI	0x0106
#define	CEG	0x0107
#define	CEP	0x2108
#define	CFC	0x0109
#define	CFD	0x019A
#define	CFLC	0x010C
#define	CFLV	0x010D
#define	CFPC	0x0112
#define	CFPV	0x0113
#define	CJA	0x011A
#define	CJC	0x011B
#define	CJD	0x011C
#define	CJE	0x011D
#define	CJL	0x011E
#define	CJM	0x011F
#define	CJS	0x0120
#define	CSJ	0x8123
#define	CSM	0x1124
#define	CSP	0x0125
#define	CST	0x8126
#define	CVC	0x0127
#define	CVE	0x0128
#define	CVM	0x0129
#define	CBD	0x062A
#define	CBE	0x062B
#define	CCC	0x022C
#define	CCD	0x022D
#define	CCF	0x022E
#define	CCG	0x022F
#define	CCI	0x0230
#define	CCM	0x0231
#define	CCP	0x0232
#define	CDA	0x0233
#define	CDL	0x0634
#define	CDR	0x4235
#define	CER	0x0236
#define	CHG	0x0237
#define	CHP	0x0238
#define	CKC	0x0239
#define	CKD	0x023A
#define	CKE	0x023B
#define	CKF	0x023C
#define	CKI	0x023D
#define	CKK	0x023E
#define	CKP	0x023F
#define	CKV	0x0240
#define	CKW	0x0241
#define	CMO	0x0642
#define	CPC	0x0243
#define	CPF	0x0244
#define	CPI	0x0245
#define	CPL	0x0246
#define	CPM	0x0247
#define	CPP	0x0248
#define	CSA	0x2249
#define	CSF	0x024A
#define	CSI	0x064B
#define	CVI	0x024C
#define	CVP	0x024D
#define	CZC	0x024E
#define	CZR	0x024F
#define	CZT	0x0250
#define	CAD	0x0451
#define	CAF	0x0452
#define	CAG	0x0453
#define	CAI	0x0454
#define	CAO	0x0455
#define	CAP	0x0456
#define	CAS	0x0457
#define	CAT	0x0458
#define	CAV	0x0459
#define	CAZ	0x045A
#define	CBO	0x045B
#define	CFX	0x045D
#define	CIH	0x845E
#define	CIHY	0x845F
#define	CIL	0x8460
#define	CILY	0x8461
#define	CIS	0x8462
#define	CISX	0x8463
#define	CTD	0x0866
#define	CQRSP	0x08FC
#define	CG2D	0x08FD
#define	CP2D	0x08FE
#define	CERR	0x08FF
#define	CCT	0x9067
#define	CMT	0x1068
#define	CPS	0x1069
#define	CQC	0x106A
#define	CQCN	0x106B
#define	CQD	0x106C
#define	CQE	0x906D
#define	CQK	0x106E
#define	CQL	0x906F
#define	CQS	0x9070
#define	CQX	0x1071
#define	CWD	0x1072
#define	CWT	0x1073
#define	CAL	0xA074
#define	CAX	0x2075
#define	CMD	0x2076
#define	CME	0x2077
#define	CMN	0xA078
#define	CSC	0x2079
#define	CTS	0x4083
#define	CRD	0x4001
#define	CRI	0x4002
#define	CAR	0x8084
#define	CBS	0x8085
#define	CCS	0x8086
#define	CGC	0x8087
#define	CIC	0x8089
#define	CID	0x808A
#define	CIE	0x808B
#define	CIO	0x808C
#define	CIOY	0x808D
#define	CIP	0x808E
#define	CIQ	0x808F
#define	CIT	0x8090
#define	CIU	0x8091
#define	CIVA	0x8092
#define	CIVT	0x8093
#define	CIX	0x8094
#define	CRE	0x8095
#define	CRV	0x8098
#define	CSK	0x8099
