
//////////////////////////////////////////////////////////////////////////
//
// Squared Driver

#define	SPACE_INTG	1
#define	SPACE_LONG	2
#define	SPACE_REAL	3

class CSquareDDriver : public CMasterDriver
{
	public:
		// Constructor
		CSquareDDriver(void);

		// Destructor
		~CSquareDDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			DWORD	dDrops;
			UINT	bDrop1;
			UINT	bDrop2;
			BOOL	m_fHasRoute;
			BOOL	m_fHas3;

			char	cDst1[3];
			char	cDst2[3];
			};

		CContext *	m_pCtx;

		// Hex Lookup
		LPCTXT	m_pHex;

		// Source Device Number
		BYTE	m_bSource;
		char	cSrce[3];
		
		// Comms Data
		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		BYTE	m_bReq[64];
		BYTE	m_bLastResponse;
		BYTE	m_bSeq;
		BYTE	m_bCheck;
		UINT	m_uPtr;

		BOOL	m_fTxOdd;
		BOOL	m_fDataPad;

		UINT	m_uDataState;

		WORD	m_wCount;

		// CONTROL CHARACTERS

		#define	ODD	0x11
		#define	EVN	0x12

		// SYMBOL STRUCTURE

		struct	SYM {
			BOOL	fControl;
			BYTE	bValue;
			BYTE	bCheck;
			};
	
		typedef	SYM	*PSYM;

		// PROTOCOL CONSTANTS

		#define	REPLY_TIMEOUT	150
		#define	FRAME_TIMEOUT	1000

		#define	SEND_COUNT	4
		#define	ENQ_COUNT	8
		#define	NAK_COUNT	2

		#define	BUSY		2

		// APPLICATION STRUCTURES

		struct READREQ {
			BYTE	bOpcode;
			BYTE	bSeq;
			WORD	wStart;
			WORD	wCount;
			};
	
		typedef READREQ *PREADREQ;

		struct READREP {
			BYTE	bOpcode;
			BYTE	bSeq;
			WORD	wStart;
			WORD	wData[1];
			};
	
		typedef READREP *PREADREP;

		struct WRITEREQ {
			BYTE	bOpcode;
			BYTE	bSeq;
			WORD	wStart;
			WORD	wData[1];
			};
	
		typedef WRITEREQ *PWRITEREQ;

		struct WRITEREP {
			BYTE	bOpcode;
			BYTE	bSeq;
			};
	
		typedef WRITEREP *PWRITEREP;

		// Implementation

		// Device Access
		BOOL	TxNetworkPacket(PBYTE pData, UINT uCount);
		BOOL	RxNetworkPacket(void);
		void	TxNak(void);
		void	TxAck(BOOL fOdd);
		void	TxDataLinkPacket(PBYTE pData, UINT uCount);
		BOOL	RxDataLinkSymbol(PSYM pSymbol);

		void	StartFrame(void);
		void	PutDataTypeCode(WORD wType, WORD wDataType);//new
		void	PutOperand(WORD wType, WORD wAddr);
		void	PutRelayAddress(WORD wAddr, WORD uFactor);
		void	PutDataLength(WORD wType, WORD wCount, WORD wDataType);//new

		void	AddByte(BYTE bData);
		void	AddText(PCTXT Text);
		void	AddDLEplusByte(BYTE bData);
		void	AddValue(WORD wData, WORD wBase, UINT uFactor);

		// Transport
		void	Send(void);

		// Port Access
		void	Put(BYTE b);
		WORD	Get(UINT uTime);

		//Helpers
	};

// End of File
