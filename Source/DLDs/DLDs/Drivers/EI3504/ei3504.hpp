//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Invensys 350x Data Spaces
//

#define	SPACE_HOLD	0x04
// The following map to Modbus Holding Registers
#define	SP_COMT	99	// Indirect Register Access
#define	SP_ACC	10	// Access
#define	SP_ALA	11	// Alarm.1 - Alarm.4
#define	SP_ALB	12	// Alarm.5 - Alarm.8
#define	SP_ALS	13	// Alarm Summary
#define	SP_BCD	14	// BCDInput
#define	SP_COM	15	// Comms
#define	SP_DAL	16	// DigAlarm
#define	SP_HUM	17	// Humidity
#define	SP_INS	18	// Instrument
#define	SP_IPM	19	// IPMonitor
#define	SP_LG2	20	// Lgc2
#define	SP_LG8	21	// Lgc8
#define	SP_LGI	22	// LgcIO
#define	SP_LIN	23	// Lin16
#define	SP_LDG	24	// Loop Diag
#define	SP_LMN	25	// Loop Main
#define	SP_LOP	26	// Loop OP
#define	SP_PID	27	// Loop PID
#define	SP_SET	28	// Loop Setup
#define	SP_SP	29	// Loop SP
#define	SP_TUN	30	// Loop Tune
#define	SP_MAT	31	// Math2
#define	SP_MOD	32	// Mod
#define	SP_MID	33	// ModIDs
#define	SP_MUL	34	// MultiOper
#define	SP_PGM	35	// Programmer
#define	SP_PV	36	// PV
#define	SP_REC	37	// Recipe
#define	SP_RLY	38	// RelayAA
#define	SP_SWO	39	// SwitchOver
#define	SP_TIM	40	// Timer
#define	SP_TXD	41	// Txdr
#define	SP_USE	42	// UsrVal
#define	SP_ZIR	43	// Zirconia

// Added May 2011
#define	SP_PGS	48	// Prgr Segments
// End Space definitions

#define	MAXREAD	20
#define	RDONE	0x0FFF

#define	RXGOOD	0
#define	RXRERR	1	// register not found
#define	RXBAD	2	// no response or unintelligible

#define	PGMBASE	5184
#define	PGSBASE	5376

#define	WW	addrWordAsWord
#define	LL	addrLongAsLong

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Invensys Serial Driver
//

class CEI3504SerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CEI3504SerialDriver(void);

		// Destructor
		~CEI3504SerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;

			UINT	uArrCount;
			UINT	uWriteErrCt;
			};

		// Data Members
		CContext * m_pCtx;
		UINT	   m_uTxSize;
		UINT	   m_uRxSize;
		BYTE     * m_pTx;
		BYTE     * m_pRx;
		UINT	   m_uPtr;
		CRC16	   m_CRC;
		UINT	   m_uTimeout;
		UINT	   m_uTickCount;

		// Modbus Address Arrays
		UINT	*m_pAddrArr;
		UINT	*m_pPosnArr;
		UINT	*m_pSortDataArr;
		UINT	*m_pSortPosnArr;
		
		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		
		// Transport Layer
		BOOL	Transact(BOOL fIgnore);
		BOOL	PutFrame(void);
		BOOL	GetFrame(void);
		BOOL	BinaryTx(void);
		BOOL	BinaryRx(void);
		
		// Read Handlers
		CCODE	HandleRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	HandleModbusRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	HandleWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Modbus Address Normalization
		DWORD	FixMAddr(AREF Addr);

		// 3504 handling
		CCODE	Handle3504 (AREF Addr, PDWORD pData, UINT uCount, BOOL fIsWrite);
		CCODE	Do3504Read (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	Do3504Write(AREF Addr, PDWORD pData, UINT uCount);
		UINT	Check4Error(UINT ccode);
		CCODE	CheckDoneCt(UINT uDone, UINT uCount);

		// Helpers
		void	SetArrCount(UINT uTable);
		void	SetArrAddr(UINT uTable);
		UINT	GetALASpec(UINT uSel, UINT uBlk);
		void	ClearArr(void);
		void	MakeArr (UINT uTable);
		void	MakeAddr(void);
		UINT	IsSCADATable(CAddress Addr);
		UINT	MakeBlock(UINT uStart, UINT uCount);
		void	DoAddrSort(UINT uCount, UINT * pA, UINT * pP);
		void	SwapPositions(UINT *pAdd, UINT *pPos, UINT ui, UINT uj);
		void	SwapItem(UINT *p, UINT ui, UINT uj);
				
		// Implementation
		void	AllocBuffers(void);
		void	FreeBuffers(void);

		// Port Access
		void	TxByte(BYTE bData);
		UINT	RxByte(UINT uTime);

		// Transport Helpers
		UINT	FindReplySize(void);
		UINT	FindEndTime(void);
	};

// End of File
