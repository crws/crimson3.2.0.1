
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CImpactData;

/////////////////////////////////////////////////////////////////////////
//
// Impact Data Structures
//

#pragma pack(1)

struct ImpAddr 
{
	BYTE    m_Type;
	DWORD	m_Refs;
	char	m_Text[124];
	};

#pragma pack()

/////////////////////////////////////////////////////////////////////////
//
// Impact Data Constants
//

#define HEAD	0x3F786D6C

#define TAIL	0x2F505054

#define MARK    0x3C505054

#define FAIL	0x4661696C

/////////////////////////////////////////////////////////////////////////
//
// Impact Data Enumerations
//

enum {
	tObject   = 0,
	tTask     = 1,
	tRun	  = 2,
	tTrig     = 3,
	tUser     = 4,
	tGen	  = 5,
	tSyn	  = 6,
	tData	  = 7,
	tOnline   = 8,
	tOffline  = 9,
	tIsOnline = 10,
	tAbort	  = 11,
	};

/////////////////////////////////////////////////////////////////////////
//
// Impact Data Driver
//

class CImpactData : public CMasterDriver
{
	public:
		// Constructor
		CImpactData(void);

		// Srcructor
		~CImpactData(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping     (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
				
		

	protected:
		// Device Data
		struct CContext
		{
			DWORD	   m_IP;
			UINT	   m_uPort;
			BOOL	   m_fKeep;
			BOOL	   m_fPing;
			UINT	   m_uTime1;
			UINT	   m_uTime2;
			UINT	   m_uTime3;
			UINT	   m_uLast;
			ISocket  * m_pSock;
			UINT	   m_uNotes;
			ImpAddr  * m_pNoteBook;
			char	   m_Http[32 * 68 + 130];
			char	   m_GenError[128];
			char	   m_SynError[128];
			char	   m_DataError[128];
			};

		// Data Members
		CContext * m_pCtx;
		UINT	   m_uKeep;
		BYTE	   m_bRxBuff[200 * 32];
		UINT	   m_uRx;
						
		// Extra Help
		IExtraHelper   * m_pExtra;
						
		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Transport Layer
		BOOL RecvFrame(BOOL fXML = TRUE);
		BOOL CheckRecv(DWORD dwCheck, DWORD dwGot); 
		UINT Find(DWORD dwFind, PBYTE pData, UINT uBytes);
		BOOL Transact(BOOL fXML = TRUE);
		BOOL Send(ISocket *pSock, PCTXT pText, PTXT pArgs);
		BOOL Send(ISocket *pSock, PCBYTE pText, UINT uSize);
		
		// Implementation
		BOOL HTTPRead(AREF Addr, UINT uCount);
		BOOL IsCameraOnline(AREF Addr, UINT uCount); 
		BOOL HTTPWrite(AREF Addr, UINT& uCount, PDWORD pData);
		BOOL RunTaskOrTool(AREF Addr, UINT& uCount, PDWORD pData);
		BOOL TriggerCamera(AREF Addr, UINT& uCount, PDWORD pData);
		BOOL SetCameraOnline(AREF Addr, UINT& uCount, PDWORD pData);
		BOOL SetCameraOffline(AREF Addr, UINT& uCount, PDWORD pData);
		BOOL Abort(AREF Addr, UINT& uCount, PDWORD pData); 
		BOOL FindText(DWORD dwAddr, UINT uCount, UINT uTable);
		PCTXT FindNoteText(DWORD dwAddr);
		UINT FindNoteType(DWORD dwAddr);
		UINT FindNoteIndex(DWORD dwAddr);
		UINT FindValueField(UINT uPos);
		UINT FindBooleanField(PCTXT Text);
		UINT FindStringField(PCTXT Text);
		BOOL RecvReal(UINT n);
		BOOL Decode(PCTXT pText, PTXT pField);
		BOOL Encode(PCTXT pText, PTXT pEncode);
		BOOL CanRead(AREF Addr);
		BOOL ReadError(AREF Addr, PDWORD pData, UINT uCount);
		BOOL CheckStrEnd(PDWORD pData);
		BOOL IsBoolean(UINT uType);
		BOOL IsString(UINT uTable);
		BOOL IsLong(UINT uType, UINT uTable);
		BOOL HandleBoolean(PCTXT Text, DWORD& Data);
		BOOL HandleString(PCTXT Text, PDWORD pData, UINT& Index, UINT uCount);
		BOOL HandleLong(UINT uType, PCTXT Text, DWORD& Data);
		PTXT FindField(PCTXT Text);

		CCODE GetReadData(AREF Addr, PDWORD pData, UINT uCount);
		CCODE GetReadCmd(AREF Addr, PDWORD pData, UINT uCount);
		CCODE WriteSuccess(UINT uCount, DWORD dwAddr);
		CCODE RunSuccess(UINT uCount, DWORD dwAddr);
		CCODE TriggerSuccess(UINT uCount, DWORD dwAddr);
		CCODE OnlineSuccess(UINT uCount, DWORD dwAddr);
		CCODE OfflineSuccess(UINT uCount, DWORD dwAddr);
		CCODE AbortSuccess(UINT uCount, DWORD dwAddr);

		// Error Reporting
		BOOL ReportGeneralError(DWORD dwAddr);
		BOOL ReportSyntaxError(DWORD dwAddr);
		BOOL ReportDataError(DWORD dwAddr);
				
		// Helpers 
		void ShowNotes(void);
	};

// End of File
