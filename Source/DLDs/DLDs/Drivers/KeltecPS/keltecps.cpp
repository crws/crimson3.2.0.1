
#include "intern.hpp"

#include "keltecps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Keltec Power Supply Driver
//

// Instantiator

INSTANTIATE(CKeltecPowerSupplyDriver);

// Constructor

CKeltecPowerSupplyDriver::CKeltecPowerSupplyDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_dErrCode	= 0;

	m_dWriteErr	= 0;
	}

// Destructor

CKeltecPowerSupplyDriver::~CKeltecPowerSupplyDriver(void)
{
	}

// Configuration

void MCALL CKeltecPowerSupplyDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CKeltecPowerSupplyDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CKeltecPowerSupplyDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CKeltecPowerSupplyDriver::Open(void)
{
	}

// Device

CCODE MCALL CKeltecPowerSupplyDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx          = new CContext;

			m_pCtx->m_bDrop = GetByte(pData) + '0';

			memset(m_pCtx->m_bUSRC, 0, USRSZ);
			memset(m_pCtx->m_bUSRR, 0, USRSZ);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKeltecPowerSupplyDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CKeltecPowerSupplyDriver::Ping(void)
{
	DWORD    Data[1];

	memset(m_pCtx->m_bUSRC, 0, USRSZ);

	m_pCtx->m_bUSRC[0] = '0';

	CAddress Addr;

	Addr.a.m_Table  = USR;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrBitAsBit;
	Addr.a.m_Extra  = 0;

	MsgNoResponse();

	m_pCtx->m_fPingFail = FALSE;

	m_pCtx->m_fPingFail = Write(Addr, Data, 1) != 1;

	return 1;
	}

CCODE MCALL CKeltecPowerSupplyDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	SetKeltecID(Addr.a.m_Table, Addr.a.m_Offset);

	if( NoReadTransmit(pData, uCount) ) {

		return uCount;
		}

	return !m_pCtx->m_fPingFail ? DoRead(Addr, pData, uCount) : CCODE_ERROR;
	}

CCODE MCALL CKeltecPowerSupplyDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	SetKeltecID(Addr.a.m_Table, Addr.a.m_Offset);

	if( NoWriteTransmit(pData, uCount) ) { 

		return uCount;
		}
 
	return !m_pCtx->m_fPingFail ? DoWrite(Addr, pData, uCount) : CCODE_ERROR;
	}

// Implementation

void CKeltecPowerSupplyDriver::SetKeltecID(UINT uTable, UINT uOffset)
{
	m_uID = uTable == addrNamed ? uOffset : uTable;
	}

CCODE CKeltecPowerSupplyDriver::DoRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(FALSE);

	AddOffset(Addr.a.m_Table, Addr.a.m_Offset);

	return (Transact() && GetResponse(pData, uCount)) ? uCount : CCODE_ERROR;
	}

CCODE CKeltecPowerSupplyDriver::DoWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(TRUE);

	switch( m_uID ) {

		case NVWD:
		case NVBY:
			AddDec(Addr.a.m_Offset, 100, TRUE);
			AddByte(',');
			break;

		case FTMO:
			AddByte(',');
			break;

		case USR:
			return DoUserRead(pData, uCount);
		}

	AddWriteData(*pData);

	if( Transact() ) {

		if( m_bRx[0] == NAK ) {

			DWORD dErr = (m_bTx[2] == '@') || (m_bTx[2] == '?') ? m_bTx[3] : m_bTx[2];

			m_dErrCode = (dErr << 16) + m_bRx[3];

			if( ++m_dWriteErr < 3 ) {

				return CCODE_ERROR;
				}
			}

		m_dWriteErr = 0;

		return uCount;
		}

	if( !m_fGotAResp && m_uID == ARUN ) {

		return 1; // LVPS in Fault or Maintenance Mode
		}

	if( m_bRx[0] == ACK ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CKeltecPowerSupplyDriver::DoUserRead(PDWORD pData, UINT uCount)
{
	BYTE b;

	m_uPtr   = 0;

	AddByte(STX);

	AddByte(m_pCtx->m_bDrop);

	for( UINT i = 0; (b = m_pCtx->m_bUSRC[i]) && i < USRSZ; i++ ) {

		AddByte(b);
		}

	MsgNoResponse();

	if( Transact() ) {

		memset(m_pCtx->m_bUSRR, 0, USRSZ);

		UINT uState = 0;

		for( i = 0; i < USRSZ - 1; i++ ) {

			b = m_bRx[i];

			m_pCtx->m_bUSRR[i] = b;

			if( !b ) return uCount;

			switch( uState ) {

				case 0:
					if( b == 3 ) uState = 1;
					break;

				case 1:
					return uCount;
				}
			}
		}

	return uCount;
	}

void CKeltecPowerSupplyDriver::MsgNoResponse(void)
{
	memset(m_pCtx->m_bUSRR, 0, USRSZ);

	m_pCtx->m_bUSRR[ 0] = 'N';
	m_pCtx->m_bUSRR[ 1] = 'o';
	m_pCtx->m_bUSRR[ 2] = ' ';
	m_pCtx->m_bUSRR[ 3] = 'R';
	m_pCtx->m_bUSRR[ 4] = 'e';
	m_pCtx->m_bUSRR[ 5] = 's';
	m_pCtx->m_bUSRR[ 6] = 'p';
	m_pCtx->m_bUSRR[ 7] = 'o';
	m_pCtx->m_bUSRR[ 8] = 'n';
	m_pCtx->m_bUSRR[ 9] = 's';
	m_pCtx->m_bUSRR[10] = 'e';
	}

// Frame Building

void CKeltecPowerSupplyDriver::StartFrame(BOOL fIsWrite)
{
	m_uPtr = 0;

	AddByte(STX);

	AddByte(m_pCtx->m_bDrop);

	AddByte(FindCommandID(fIsWrite));
	}

void CKeltecPowerSupplyDriver::EndFrame(void)
{	
	m_bTx[m_uPtr++] = ETX;

	AddByte(MakeChecksum(TRUE, m_uPtr));
	}

void CKeltecPowerSupplyDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr++] = bData;
		}
	}

void CKeltecPowerSupplyDriver::AddDec(UINT uData, UINT uFactor, BOOL fKeepLeading0s)
{
	BOOL fFirst = fKeepLeading0s;

	while( uFactor ) {

		BYTE b = (uData / uFactor) % 10;

		if( fFirst || b ) {

			AddByte(m_pHex[b]);

			fFirst = TRUE;
			}

		uFactor /= 10;
		}
	}

// Add data to command 

void CKeltecPowerSupplyDriver::AddOffset(UINT uTable, UINT uOffset)
{
	switch( uTable ) {

		case NVWD:
		case NVBY:
			AddDec(LOBYTE(uOffset), 100, TRUE);
			return;

		case ADQ:
			AddByte(m_pHex[uOffset & 0xF]);
			return;
		}
	}

void CKeltecPowerSupplyDriver::AddWriteData(DWORD dData)
{
	switch( m_uID ) {

		case NVBY:
			AddDec(LOBYTE(LOWORD(dData)), 10, TRUE);
			return;

		case FTMO:
			AddDec(LOBYTE(LOWORD(dData)), 100, TRUE);
			return;

		case NVWD:
		case SRI:
			AddDec(LOWORD(dData), 1000, m_uID == NVWD);
			return;
		}
	}

BYTE CKeltecPowerSupplyDriver::FindCommandID(BOOL fIsWrite)
{
	BYTE bID = 0;

	switch( m_uID ) {

//		Queries - Address Named
		case  STQ:	bID = '1';	break;
		case  OCQ:	bID = '6';	break;
		case  OVQ:	bID = '7';	break;
		case  OPQ:	bID = '8';	break;
		case  TMQ:	bID = '=';	break;
		case  CSQ:	bID = 'C';	break;

//		Commands
		case SLFT:	bID = '2';	break;
		case ARDY:	bID = '4';	break;
		case ARUN:	bID = '5';	break;
		case FRST:	bID = '9';	break;

//		Read Only Table
		case  ADQ:	AddByte('?');	bID = 'A';	break;

//		Read/Write value named
		case  SRI:	bID = 'I';	break;

//		String Response
		case IDQ:	bID = '0';	break;
		}

	if( bID ) {

		m_bID = bID;
		return m_bID;
		}

//	Maintenance commands

	AddByte(fIsWrite ? '@' : '?');

	switch( m_uID ) {

//		Maintenance mode commands
		case REBT:	m_bID = '!';	break;
		case SETC:	m_bID = '*';	break;
		case MTMD:	m_bID = 'M';	break;
		case RELD:	m_bID = 'P';	break;

//		Read/Write value named
		case FTMO:	AddByte('X');	m_bID = 'X'; return '4';

//		Read/Write table
		case NVWD:	m_bID = 'Y';	break;

		case NVBY:	m_bID = 'X';	break;

		default:	m_bID =  0;	break;
		}

	return m_bID;
	}

// Transport Layer

BOOL CKeltecPowerSupplyDriver::Transact(void)
{
	Send();

	return GetReply();
	}

void CKeltecPowerSupplyDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CKeltecPowerSupplyDriver::GetReply(void)
{
	UINT uData;

	BYTE bData;

	UINT uCount = 0;

	UINT uTimer = 500;

	UINT uState = 0;

	BYTE bCheck = 0;

	BOOL fNak   = FALSE;

	m_fGotAResp = FALSE;

	SetTimer(uTimer);

//**/	AfxTrace0("\r\n");

	while( (uTimer = GetTimer() ) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		bData           = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);

		m_bRx[uCount++] = bData;

		m_fGotAResp     = TRUE;

		switch( uState ) {

			case 0:
				fNak = bData == NAK;

				if( bData == ACK || bData == STX || fNak ) {

					uState = 1;
					}
				break;

			case 1:
				uState = bData == m_bTx[1] ? 2 : 0;
				break;

			case 2:
				uState = bData == m_bTx[2] ? 3 : 0;
				break;

			case 3:
				if( bData == ETX ) {

					bCheck            = MakeChecksum(FALSE, uCount);

					m_bRx[uCount - 1] = 0; // Terminate receive string

					uState = 4;
					}
				break;

			case 4:
				return !fNak && (bCheck == bData);

			}

		if( uCount > sizeof(m_bRx) ) return FALSE;
		}	

	return FALSE;
	}

BOOL CKeltecPowerSupplyDriver::GetResponse( PDWORD pData, UINT uCount )
{
	UINT uCt;

	PU4  pRx;

	if( m_fHasData && m_bRx[0] == ACK ) {

		switch( m_uID ) {

			case OCQ:
			case OVQ:
			case OPQ:
			case TMQ:
				*pData = GetReadData(TRUE);
				return TRUE;

			case IDQ:
				pRx = PU4(&m_bRx[3]);

				for( uCt = 0; uCt < uCount; uCt++, pRx++ ) {

					pData[uCt] = MotorToHost(*pRx);
					}

				return TRUE;

			case ADQ:
				*pData = U4(m_bRx[3]);
				return TRUE;

			case STQ:
				*pData = 0;

				for( uCt = 3; uCt < 6; uCt++ ) {

					*pData <<= 4;

					*pData  += m_bRx[uCt] & 0x3F;
					}

				return TRUE;

			case CSQ:
			case SRI:
			case FTMO:
			case NVWD:
			case NVBY:
				*pData = GetReadData(FALSE);
				return TRUE;

			default:
				return FALSE;
			}
		}

	else {
		if( m_bRx[0] == ACK ) {

			return TRUE;
			}
		}

	m_dErrCode = GetReadData(FALSE);

	return FALSE;
	}

DWORD CKeltecPowerSupplyDriver::GetReadData(BOOL fIsReal)
{
	BYTE b[7] = {0};

	memcpy(b, &m_bRx[3], 6);

	return fIsReal ? ATOF((const char *)b) : ATOI((const char *)b);
	}

// Port Access

void CKeltecPowerSupplyDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );
	}

UINT CKeltecPowerSupplyDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

BOOL CKeltecPowerSupplyDriver::NoReadTransmit(PDWORD pData, UINT uCount)
{
	UINT i;

	PU4  p;

	m_fHasData = FALSE;

	switch( m_uID ) {

		case ARDY:
		case ARUN:
		case SLFT:
		case FRST:
		case REBT:
		case SETC:
		case MTMD:
		case RELD:
		case USR:
			*pData = 0;
			return TRUE;

		case STQ:
		case OCQ:
		case OVQ:
		case OPQ:
		case TMQ:
		case CSQ:
		case ADQ:
		case SRI:
		case FTMO:
		case NVWD:
		case NVBY:
		case IDQ:
			m_fHasData = TRUE;
			return FALSE;

		case USRC:
		case USRR:
			p = m_uID == USRC ? PU4(m_pCtx->m_bUSRC) : PU4(m_pCtx->m_bUSRR);

			for( i = 0; i < USRSZ / 4; i++ ) {

				if( i < uCount ) {

					pData[i] = p[i];
					}

				else {
					pData[i] = 0;
					}
				}

			return TRUE;

		case ERRC:
			*pData = m_dErrCode;
			return TRUE;
		}

	return FALSE;
	}
BOOL CKeltecPowerSupplyDriver::NoWriteTransmit(PDWORD pData, UINT uCount)
{
	UINT uCt;

	switch( m_uID ) {

		case STQ:
		case OCQ:
		case OVQ:
		case OPQ:
		case TMQ:
		case CSQ:
		case ADQ:
		case IDQ:
			return TRUE;

		case ARDY:
		case ARUN:
		case SLFT:
		case FRST:
		case REBT:
		case SETC:
		case MTMD:
		case RELD:
		case USR:
			return !(BOOL(*pData));

		case USRC:
			if( pData[0] & 0xFF000000 ) {

				memset(m_pCtx->m_bUSRC, 0, USRSZ);
				memcpy(m_pCtx->m_bUSRC, PBYTE(pData), min(uCount*4, USRSZ - 1));
				}

			return TRUE;			

		case USRR:
			memset(m_pCtx->m_bUSRR, 0, USRSZ);
			return TRUE;

		case ERRC:
			m_dErrCode = *pData;
			return TRUE;
		}

	return FALSE;
	}

BYTE CKeltecPowerSupplyDriver::MakeChecksum(BOOL fIsTx, UINT uCount)
{
	PBYTE     p = fIsTx ? m_bTx : m_bRx;

	BYTE bCheck = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		bCheck ^= p[i];
		}

	return bCheck;
	}

// End of File
