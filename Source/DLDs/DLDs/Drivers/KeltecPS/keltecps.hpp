
//////////////////////////////////////////////////////////////////////////
//
// Keltec Power Supply Driver
//

// Space Definitions
enum {
// Queries - Address Named
	STQ  = 10,	// Status Query (1)
	OCQ  = 11,	// Output Current Query (6)
	OVQ  = 12,	// Output Voltage Query (7)
	OPQ  = 13,	// Output Power Query (8)
	TMQ  = 14,	// Temperature Query (=)
	CSQ  = 15,	// Current State Query (C)
// Commands
	ARDY = 20,	// Activate Ready Mode Command (4)
	ARUN = 21,	// Activate Run Mode Command (5)
	SLFT = 22,	// Self Test Command (2)
	FRST = 23,	// Fault Reset Command (9)
	REBT = 24,	// Reboot (!)
	SETC = 25,	// Compute and set checksum (*)
	MTMD = 26,	// Maintenance Mode (M)
	RELD = 27,	// Reload (P)
// Read Only Table
	ADQ  =  1,	// ADC Query (A)
// R/W values named
	SRI  =  2,	// Set/Read Current (I)
	FTMO =  3,	// Set/Read Fault Timeout (X)
// R/W Table
	NVWD =  4,	// Set/Read 16 bit NV Parameters (Y)
	NVBY =  5,	// Set/Read  8 bit NV Parameters (Y)
// String Response
	IDQ  =  6,	// ID String (0)
// User Commands
	USR  = 17,	// Send USRC
	USRC = 18,	// User Command String
	USRR = 19,	// USRC Response String
// Latest Error Code
	ERRC = 30,	// Latest Error Code (Internal)
	};

#define USRSZ	20	// size of user command caches

class CKeltecPowerSupplyDriver : public CMasterDriver
{
	public:
		// Constructor
		CKeltecPowerSupplyDriver(void);

		// Destructor
		~CKeltecPowerSupplyDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_bUSRC[USRSZ];
			BYTE	m_bUSRR[USRSZ];
			BOOL	m_fPingFail;
			};
		CContext *	m_pCtx;

		// Data Members
		BOOL	m_fGotAResp;
		BOOL	m_fHasData;
		BYTE	m_bTx[24];
		BYTE	m_bRx[24];
		BYTE	m_bID;
		UINT	m_uPtr;
		UINT	m_uID;
		DWORD	m_dErrCode;
		DWORD	m_dWriteErr;

		// Hex Lookup
		LPCTXT	m_pHex;

		// Implementation
		void	SetKeltecID(UINT uTable, UINT uOffset);
		CCODE	DoRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoUserRead(PDWORD pData, UINT uCount);
		void	MsgNoResponse(void);
		
		// Frame Building
		void	StartFrame(BOOL fIsWrite);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddDec(UINT uData, UINT uFactor, BOOL fKeepLeading0s);

		// Add Data to command
		void	AddOffset(UINT uTable, UINT uOffset);
		void	AddWriteData(DWORD dData);

		BYTE	FindCommandID(BOOL fIsWrite);
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	GetResponse(PDWORD pData, UINT uCount);
		DWORD	GetReadData(BOOL fIsReal);
		void	GetUserResponse(PDWORD pData);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		BOOL	NoReadTransmit (PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(PDWORD pData, UINT uCount);
		BYTE	MakeChecksum(BOOL fIsTx, UINT uCount);
	};

// End of File
