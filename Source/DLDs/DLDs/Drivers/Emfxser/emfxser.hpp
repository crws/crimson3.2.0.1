
//////////////////////////////////////////////////////////////////////////
//
// Emerson FX Serial Driver
//

#define EMFXSER_ID 0x4067

//////////////////////////////////////////////////////////////////////////
//
// Emfxser Driver
//

#define	PARBIT	0x0400

#define	MAXPAR	0xFF

// string response holders
#define	BEGSTRR	 1
#define	CAIR	 1
#define	CCBR	 2
#define	CDUR	 3
#define	CEQR	 4
#define	CIDR	 5
#define	CPIR	 6
#define	CQCR	 7
#define	CTMR	 8
#define	CTWR	 9
#define	CVUR	10
// Error Response String
#define	CERR	11
#define	ENDSTRR	11

// string item id's
#define	IDAI	('A' << 8) + 'I'
#define	IDCB	('C' << 8) + 'B'
#define	IDDU	('D' << 8) + 'U'
#define	IDEQ	('E' << 8) + 'Q'
#define	IDID	('I' << 8) + 'D'
#define	IDPI	('P' << 8) + 'I'
#define	IDQC	('Q' << 8) + 'C'
#define	IDTM	('T' << 8) + 'M'
#define	IDTW	('T' << 8) + 'W'
#define	IDVU	('V' << 8) + 'U'

// executable item id's
#define	IDCL	('C' << 8) + 'L'
#define	IDHM	('H' << 8) + 'M'
#define	IDRR	('R' << 8) + 'R'
#define	IDRS	('R' << 8) + 'S'
#define	IDSJ	('S' << 8) + 'J'
#define	IDST	('S' << 8) + 'T'
#define	IDZR	('Z' << 8) + 'R'

// special case response id's
#define	IDQU	('Q' << 8) + 'U'

#define	SZTX	 32
#define	SZRX	128
#define	SZSTR	 81

#define	ISSNOT	  0
#define	ISSCMD	  1
#define	ISSRSP	  2

#define	TYPMASK	0x0300	// data type mask
#define	DATADEC	0x0000	// data transferred in decimal
#define	DATAHEX	0x0200	// data transferred in hex
#define	DATABIN	0x0100	// data transferred in binary
#define	DATATXT	0x0300	// data transferred in ascii

#define	DOQUERY	TRUE
#define	DOWRITE	FALSE
#define	SKIPSYM	TRUE
#define	ADDSYM	FALSE

class CEmfxserDriver : public CMasterDriver
{
	public:
		// Constructor
		CEmfxserDriver(void);

		// Destructor
		~CEmfxserDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Open(void);

		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_Drop;
			UINT	m_uWriteErrCt;
			BYTE	m_bTerminator;
			BYTE	m_pbAIR[SZSTR];
			BYTE	m_pbCBR[SZSTR];
			BYTE	m_pbDUR[SZSTR];
			BYTE	m_pbEQR[SZSTR];
			BYTE	m_pbIDR[SZSTR];
			BYTE	m_pbPIR[SZSTR];
			BYTE	m_pbQCR[SZSTR];
			BYTE	m_pbQUR[SZSTR];
			BYTE	m_pbTMR[SZSTR];
			BYTE	m_pbTWR[SZSTR];
			BYTE	m_pbVUR[SZSTR];
			BYTE	m_pbERR[SZSTR];

			PBYTE	m_pStrSrc[ENDSTRR - BEGSTRR + 2];
			};

		CContext *m_pCtx;

		// Data Members
		BYTE	m_bTx[SZTX];
		BYTE	m_bRx[SZRX];

		UINT	m_uTxPtr;
		UINT	m_uRxPtr;
		UINT	m_uID;
		UINT	m_uEchoTO;

		BOOL	m_fHasEcho;

		// Frame Building
		BOOL	DoStringItem(AREF Addr, PDWORD pData);
		void	StartFrame(AREF Addr, BOOL fIsQuery, BOOL fSkipOp);
		void	AddByte(BYTE bData);
		void	AddText(char * sText);
		void	AddCommand(UINT uTable, UINT uOffset);
		void	AddParameter(UINT uOffset);
		void	AddWriteData(PDWORD pData, UINT uType);
		void	EndFrame(void);

		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	GetResponse(PDWORD pData, UINT uType);
		void	StoreStringResponse(void);
		CCODE	CheckResponse(BOOL fIsWrite);

		// Port Access
		void	PingPut(void);
		void	Put(void);
		UINT	Get(UINT uTimer);

		// Internal access
		BOOL	NoReadTransmit (AREF Addr, PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
		BYTE	GetModifier(UINT uOffset);
		UINT	IsStringItem(UINT uTable, UINT uOffset);
		BOOL	IsExeCmd(void);
		UINT	FormID(UINT uTable, UINT uOffset);
		BOOL	GetHex(PBYTE pb);
		void	SetERRString(void);
	};

// End of File
