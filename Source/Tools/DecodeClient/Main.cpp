
#include "Intern.hpp"

// Prototypes

static void Error(char const *text, ...);

// Code

int main(int nArg, char const *pArg[])
{
	if( nArg == 4 ) {

		// Arguments are:
		//
		//	pArg[1] Serial Number
		//	pArg[2] UI Password
		//	pArg[3] Server IP Address

		vector<char> tx;

		tx.insert(tx.end(), pArg[1], pArg[1] + strlen(pArg[1]));

		tx.insert(tx.end(), '\n');

		tx.insert(tx.end(), pArg[2], pArg[2] + strlen(pArg[2]));

		tx.insert(tx.end(), '\n');

		WSADATA Data;

		WSAStartup(MAKEWORD(2, 2), &Data);

		SOCKET s = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);

		if( s > 0 ) {

			sockaddr_in addr = { 0 };

			addr.sin_family = AF_INET;

			addr.sin_port   = htons(7778);

			addr.sin_addr.S_un.S_addr = inet_addr(pArg[3]);

			if( connect(s, (sockaddr *) &addr, sizeof(addr)) >= 0 ) {

				if( send(s, tx.data(), tx.size(), 0) == tx.size() ) {

					shutdown(s, SD_SEND);

					vector<char> rx(16384, 0);

					int n = recv(s, rx.data(), rx.size(), MSG_WAITALL);

					if( n > 0 ) {

						rx.push_back(0);

						printf("%s", rx.data());

						return 0;
					}
				}
			}

			Error("unable to communicate with server");
		}

		Error("unable to create socket");
	}

	Error("invalid command line");

	return 0;
}

static void Error(char const *text, ...)
{
	va_list args;

	va_start(args, text);

	printf("DecodeClient: ");

	vprintf(text, args);

	printf(" (%u)\n", GetLastError());

	va_end(args);

	exit(1);
}

// End of File
