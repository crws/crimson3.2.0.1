
#include "Intern.hpp"

// Externals

extern void rc4(PBYTE pCode, PCBYTE pData, UINT uData, PCBYTE pKey, UINT uKey);

extern void rc4(PBYTE pData, UINT uData, PCBYTE pKey, UINT uKey);

// Prototypes

static	void	Error(char const *text, ...);
static	void	AfxDump(void const *pData, size_t uCount);
static	string	Decrypt(string const &text, string const &pass);

// Code

int main(int nArg, char const *pArg[])
{
	if( nArg == 3 ) {

		string d = Decrypt(pArg[1], pArg[2]);

		if( d.size() ) {

			vector<size_t> p;

			for( size_t n = 0; d[n]; n++ ) {

				if( d[n] == '|' ) {

					p.push_back(n);
				}
			}

			if( p.size() == 2 ) {

				printf("root    : %s\n", d.substr(0, p[0]).c_str());

				printf("rlcuser : %s\n", d.substr(p[0] + 1, p[1] - p[0] - 1).c_str());

				printf("spare   : %s\n", d.substr(p[1] + 1).c_str());

				return 0;
			}
		}

		Error("could not decode passwords");
	}

	Error("incorrect command line");

	return 1;
}

static void Error(char const *text, ...)
{
	va_list args;

	va_start(args, text);

	printf("PassDecode: ");

	vprintf(text, args);

	printf("\n");

	va_end(args);

	exit(1);
}

static void AfxDump(void const *pData, size_t uCount)
{
	if( pData ) {

		BYTE const *p = (BYTE const *) pData;

		UINT        s = 0;

		for( UINT n = 0; n < uCount; n++ ) {

			if( n % 0x10 == 0x0 ) {

				printf("%8.8X : %4.4X : ", DWORD(p + n), n);

				s = n;
			}

			if( true ) {

				printf("%2.2X ", p[n]);
			}

			if( n % 0x10 == 0xF || n == uCount - 1 ) {

				printf(" ");

				for( UINT j = n; j % 0x10 < 0xF; j++ ) {

					printf("   ");
				}

				for( UINT i = 0; i <= n - s; i++ ) {

					BYTE b = p[s+i];

					if( b >= 32 && b < 127 ) {

						printf("%c", b);
					}
					else
						printf(".");
				}

				printf("\n");
			}
		}
	}
}

static string Decrypt(string const &text, string const &pass)
{
	bytes data;

	for( size_t s = 0; s < text.size(); s += 2 ) {

		char h[3] = { text[s+0], text[s+1], 0 };

		data.push_back(BYTE(strtoul(h, NULL, 16)));
	}

	rc4(data.data(), data.size(), PCBYTE(pass.data()), pass.size());

	return string(PCSTR(data.data()), data.size());
}

// End of File
