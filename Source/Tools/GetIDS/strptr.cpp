
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Simple String Pointer
//

// Constructors

CStrPtr::CStrPtr(CStrPtr const &That)
{
	That.AssertValid();
	
	m_pText = That.m_pText;
	}
	
CStrPtr::CStrPtr(CString const &That)
{
	AfxAssert(FALSE);
	}

CStrPtr::CStrPtr(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	m_pText = (PTXT) pText;
	}

// Conversion

CStrPtr::operator PCTXT (void) const
{
	return m_pText;
	}

// Attributes

BOOL CStrPtr::IsEmpty(void) const
{
	return !*m_pText;
	}
	
BOOL CStrPtr::operator ! (void) const
{
	return !*m_pText;
	}

UINT CStrPtr::GetLength(void) const
{
	return strlen(m_pText);
	}

UINT CStrPtr::GetHashValue(void) const
{
	UINT uHash = 0;
	
	for( UINT n = 0; m_pText[n]; n++ ) {
	
		uHash = (uHash << 5) + uHash;
		
		uHash = uHash + m_pText[n];
		}
		
	return uHash;
	}

// Read-Only Access

char CStrPtr::GetAt(UINT uIndex) const
{
	return (uIndex < GetLength()) ? m_pText[uIndex] : char(0);
	}

// Comparison Functions

int CStrPtr::CompareC(PCTXT pText) const
{
	AfxValidateStringPtr(pText);
	
	return strcmp(m_pText, pText);
	}

int CStrPtr::CompareN(PCTXT pText) const
{
	AfxValidateStringPtr(pText);
	
	return _stricmp(m_pText, pText);
	}

int CStrPtr::CompareC(PCTXT pText, UINT uCount) const
{
	AfxValidateStringPtr(pText);
	
	return strncmp(m_pText, pText, uCount);
	}

int CStrPtr::CompareN(PCTXT pText, UINT uCount) const
{
	AfxValidateStringPtr(pText);
	
	return _strnicmp(m_pText, pText, uCount);
	}

int CStrPtr::GetScore(PCTXT pText) const
{
	if( _stricmp(m_pText, pText) ) {
	
		UINT n = strlen(pText);
		
		if( _strnicmp(m_pText, pText, n) )
			return 0;
			
		return n;
		}
		
	return 10000;
	}

// Comparison Helper

int AfxCompare(CStrPtr const &a, CStrPtr const &b)
{
	return _stricmp(a.m_pText, b.m_pText);
	}

// Comparison Operators

BOOL CStrPtr::operator == (PCTXT pText) const
{
	return CompareN(pText) == 0;
	}

BOOL CStrPtr::operator != (PCTXT pText) const
{
	return CompareN(pText) != 0;
	}

BOOL CStrPtr::operator < (PCTXT pText) const
{
	return CompareN(pText) < 0;
	}

BOOL CStrPtr::operator > (PCTXT pText) const
{
	return CompareN(pText) > 0;
	}

BOOL CStrPtr::operator <= (PCTXT pText) const
{
	return CompareN(pText) <= 0;
	}

BOOL CStrPtr::operator >= (PCTXT pText) const
{
	return CompareN(pText) >= 0;
	}

// Substring Extraction

CString CStrPtr::Left(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);

	return CString(m_pText, uCount);
	}

CString CStrPtr::Right(UINT uCount) const
{
	UINT uLen = GetLength();

	MakeMin(uCount, uLen);

	PCTXT pSrc = m_pText + uLen - uCount;

	return CString(pSrc, uCount);
	}

CString CStrPtr::Mid(UINT uPos, UINT uCount) const
{
	UINT uLen = GetLength();

	MakeMin(uPos, uLen);

	MakeMin(uCount, uLen - uPos);

	PCTXT pSrc = m_pText + uPos;

	return CString(pSrc, uCount);
	}

CString CStrPtr::Mid(UINT uPos) const
{
	UINT uLen = GetLength();

	MakeMin(uPos, uLen);

	PCTXT pSrc = m_pText + uPos;

	return CString(pSrc, uLen - uPos);
	}

// Charset Conversion

CString CStrPtr::ToOem(void) const
{
	CString Result(*this);

	Result.MakeOem();

	return Result;
	}

CString CStrPtr::ToAnsi(void) const
{
	CString Result(*this);

	Result.MakeAnsi();

	return Result;
	}

// Case Conversion

CString CStrPtr::ToUpper(void) const
{
	CString Result(*this);

	Result.MakeUpper();

	return Result;
	}

CString CStrPtr::ToLower(void) const
{
	CString Result(*this);

	Result.MakeLower();

	return Result;
	}

// Searching

UINT CStrPtr::Find(char cChar) const
{
	AfxAssert(cChar);
	
	PTXT pFind = strchr(m_pText, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}

UINT CStrPtr::Find(char cChar, UINT uPos) const
{
	AfxAssert(cChar);

	AfxAssert(uPos <= GetLength());
	
	PTXT pFind = strchr(m_pText + uPos, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CStrPtr::FindRev(char cChar) const
{
	AfxAssert(cChar);

	PTXT pFind = strrchr(m_pText, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CStrPtr::FindRev(char cChar, UINT uPos) const
{
	AfxAssert(cChar);

	AfxAssert(uPos <= GetLength());

	PTXT pFind = strrchr(m_pText + uPos, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CStrPtr::Find(PCTXT pText) const
{
        AfxValidateStringPtr(pText);
			
	PTXT pFind = strstr(m_pText, pText);
		
	return pFind ? pFind - PTXT(m_pText) : NOTHING;
	}
	
UINT CStrPtr::Find(PCTXT pText, UINT uPos) const
{
        AfxValidateStringPtr(pText);
			
	AfxAssert(uPos <= GetLength());

	PTXT pFind = strstr(m_pText + uPos, pText);
		
	return pFind ? pFind - PTXT(m_pText) : NOTHING;
	}
	
UINT CStrPtr::FindNot(PCTXT pList) const
{
	AfxValidateStringPtr(pList);

	UINT uFind = strspn(m_pText, pList);
	
	return uFind;
	}

UINT CStrPtr::FindNot(PCTXT pList, UINT uPos) const
{
	AfxValidateStringPtr(pList);
			
	AfxAssert(uPos <= GetLength());

	UINT uFind = strspn(m_pText + uPos, pList);
	
	return uFind ? uFind + uPos : 0;
	}

UINT CStrPtr::FindOne(PCTXT pList) const
{
	AfxValidateStringPtr(pList);

	UINT uFind = strcspn(m_pText, pList);
	
	return uFind;
	}

UINT CStrPtr::FindOne(PCTXT pList, UINT uPos) const
{
	AfxValidateStringPtr(pList);
			
	AfxAssert(uPos <= GetLength());

	UINT uFind = strcspn(m_pText + uPos, pList);
	
	return uFind ? uFind + uPos : 0;
	}

// Counting

UINT CStrPtr::Count(char cChar) const
{
	UINT c = 0;
	
	for( UINT n = 0; m_pText[n]; n++ ) {

		if( m_pText[n] == cChar )
			c++;
		}
		
	return c;
	}

// Diagnostics

void CStrPtr::AssertValid(void) const
{
	AfxValidateReadPtr(this, sizeof(*this));
	
	AfxValidateStringPtr(m_pText);
	}

// End of File
