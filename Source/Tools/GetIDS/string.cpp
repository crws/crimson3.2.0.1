
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Dynamic String
//

// Null Data

CStringData CString::m_Empty = { 0, 0, 0, 0, { 0 } };

// Data Access

#define	GetData(x) (((CStringData *) ((x)->m_pText)) - 1)

// Constructors

CString::CString(void)
{
	m_pText = m_Empty.m_cData;
	}

CString::CString(CString const &That)
{
	That.AssertValid();
	
	if( !That.IsEmpty() ) {
		
		m_pText = That.m_pText;
		
		GetData(this)->m_nRefs++;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CString::CString(PCTXT pText, UINT uCount)
{
	AfxValidateStringPtr(pText, uCount);
	
	if( *pText && uCount ) {
		
		Alloc(uCount);
		
		strncpy(m_pText, pText, uCount);
		
		m_pText[uCount] = 0;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CString::CString(char cData, UINT uCount)
{
	AfxAssert(cData);
	
	if( uCount ) {
		
		Alloc(uCount);
		
		memset(m_pText, cData, uCount);
		
		m_pText[uCount] = 0;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CString::CString(LPOLESTR pText)
{
	m_pText = m_Empty.m_cData;

	InitFrom(pText);
	}

CString::CString(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	if( *pText ) {
		
		Alloc(strlen(pText));
		
		strcpy(m_pText, pText);

		return;
		}

	m_pText = m_Empty.m_cData;
	}

// Destructor

CString::~CString(void)
{
	Release();
	}

// Assignment Operators

CString const & CString::operator = (CString const &That)
{
	if( this != &That ) {
		
		That.AssertValid();
		
		Release();
		
		if( !That.IsEmpty() ) {
			
			m_pText = That.m_pText;
			
			GetData(this)->m_nRefs++;
			}
		}
	
	return *this;
	}

CString const & CString::operator = (LPOLESTR pText)
{
	Release();

	InitFrom(pText);

	return *this;
	}

CString const & CString::operator = (PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	if( *pText ) {
		
		if( pText >= m_pText && pText <= m_pText + GetLength() ) {
			
			CString Work = pText;
			
			Release();
			
			Alloc(Work.GetLength());
			
			strcpy(m_pText, Work.m_pText);
			}
		else {
			Release();
			
			Alloc(strlen(pText));
			
			strcpy(m_pText, pText);
			}
		}
	else
		Release();
	
	return *this;
	}

// Attributes

UINT CString::GetLength(void) const
{
	return GetData(this)->m_uLen;
	}

// Read-Only Access

char CString::GetAt(UINT uIndex) const
{
	return (uIndex < GetLength()) ? m_pText[uIndex] : char(0);
	}

// Substring Extraction

CString CString::Left(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);
	
	return CString(m_pText, uCount);
	}

CString CString::Right(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);
	
	PCTXT pSrc = m_pText + uLen - uCount;
	
	return CString(pSrc, uCount);
	}

CString CString::Mid(UINT uPos, UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uPos, uLen);
	
	MakeMin(uCount, uLen - uPos);
	
	PCTXT pSrc = m_pText + uPos;
	
	return CString(pSrc, uCount);
	}

CString CString::Mid(UINT uPos) const
{
	UINT uLen = GetLength();
	
	MakeMin(uPos, uLen);
	
	PCTXT pSrc = m_pText + uPos;
	
	return CString(pSrc, uLen - uPos);
	}

// Buffer Managment

void CString::Empty(void)
{
	Release();
	}

void CString::Expand(UINT uAlloc)
{
	CopyOnWrite();
	
	CStringData *pData = GetData(this);
	
	if( pData->m_uAlloc < uAlloc ) {
		
		PCTXT pText = m_pText;
		
		Alloc(pData->m_uLen, uAlloc);
		
		strcpy(m_pText, pText);
		
		delete pData;
		}
	}

void CString::Compress(void)
{
	CopyOnWrite();
	
	CStringData *pData = GetData(this);
	
	if( pData->m_uAlloc > pData->m_uLen + 1 ) {
		
		PCTXT pText = m_pText;
		
		Alloc(pData->m_uLen);
		
		strcpy(m_pText, pText);
		
		delete pData;
		}
	}

void CString::CopyOnWrite(void)
{
	CStringData *pData = GetData(this);
	
	if( pData->m_nRefs ) {
		
		if( pData->m_nRefs > 1 ) {
			
			PCTXT pText = m_pText;
			
			pData->m_nRefs--;
			
			Alloc(pData->m_uLen);
			
			strcpy(m_pText, pText);
			}
		}
	else {
		Alloc(0);
		
		GetData(this)->m_uLen = 0;
		
		*m_pText = 0;
		}
	}

void CString::FixLength(void)
{
	GetData(this)->m_uLen = strlen(m_pText);
	}

// Concatenation In-Place

CString const & CString::operator += (CString const &That)
{
	That.AssertValid();
	
	if( !That.IsEmpty() ) {
		
		UINT uLen = GetData(this)->m_uLen;
		
		CString Copy = That;
		
		GrowString(uLen + Copy.GetLength());
		
		strcat(m_pText, Copy.m_pText);
		}
	
	return *this;
	}

CString const & CString::operator += (PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	if( *pText ) {
		
		UINT uLen = GetData(this)->m_uLen;
		
		if( pText >= m_pText && pText <= m_pText + uLen ) {
			
			CString Copy = pText;
			
			GrowString(uLen + Copy.GetLength());
			
			strcat(m_pText, Copy.m_pText);
			}
		else {
			GrowString(uLen + strlen(pText));
			
			strcat(m_pText, pText);
			}
		}
	
	return *this;
	}

CString const & CString::operator += (char cData)
{
	AfxAssert(cData);
	
	UINT uLen = GetData(this)->m_uLen;
	
	GrowString(uLen + 1);
	
	m_pText[uLen + 0] = cData;
	
	m_pText[uLen + 1] = 0;
	
	return *this;
	}

// Concatenation via Friends

CString operator + (CString const &A, CString const &B)
{
	A.AssertValid();
	
	B.AssertValid();
	
	return CString(A.m_pText, A.GetLength(), B.m_pText, B.GetLength());
	}

CString operator + (CString const &A, PCTXT pText)
{
	A.AssertValid();
	
	AfxValidateStringPtr(pText);
	
	return CString(A.m_pText, A.GetLength(), pText, strlen(pText));
	}

CString operator + (CString const &A, char cData)
{
	A.AssertValid();
	
	AfxAssert(cData);
	
	return CString(A.m_pText, A.GetLength(), &cData, 1);
	}

CString operator + (PCTXT pText, CString const &B)
{
	AfxValidateStringPtr(pText);
	
	B.AssertValid();
	
	return CString(pText, strlen(pText), B.m_pText, B.GetLength());
	}

CString operator + (char cData, CString const &B)
{
	AfxAssert(cData);
	
	B.AssertValid();
	
	return CString(&cData, 1, B.m_pText, B.GetLength());
	}

// Write Data Access

void CString::SetAt(UINT uIndex, char cData)
{
	AfxAssert(uIndex < GetLength());
	
	AfxAssert(cData);
	
	CopyOnWrite();
	
	m_pText[uIndex] = cData;
	}

// Comparison Helper

int AfxCompare(CString const &a, CString const &b)
{
	return strcmp(a.m_pText, b.m_pText);
	}

// Insert and Remove

void CString::Insert(UINT uIndex, PCTXT pText)
{
	UINT uLength = GetLength();

	UINT uCount  = strlen(pText);

	MakeMin(uIndex, uLength);

	GrowString(uLength + uCount);

	memmove( m_pText + uIndex + uCount,
		 m_pText + uIndex,
		 uLength - uIndex + 1
		 );

	memcpy(  m_pText + uIndex,
		 pText,
		 uCount
		 );
	}

void CString::Insert(UINT uIndex, CString const &That)
{
	UINT uLength = GetLength();

	UINT uCount  = That.GetLength();

	MakeMin(uIndex, uLength);

	GrowString(uLength + uCount);

	memmove( m_pText + uIndex + uCount,
		 m_pText + uIndex,
		 uLength - uIndex + 1
		 );

	memcpy(  m_pText + uIndex,
		 That.m_pText,
		 uCount
		 );
	}

void CString::Insert(UINT uIndex, char cData)
{
	UINT uLength = GetLength();

	MakeMin(uIndex, uLength);

	GrowString(uLength + 1);

	memmove( m_pText + uIndex + 1,
		 m_pText + uIndex,
		 uLength - uIndex + 1
		 );

	m_pText[uIndex] = cData;
	}

void CString::Delete(UINT uIndex, UINT uCount)
{
	UINT uLength = GetLength();

	if( uIndex < uLength ) {
		
		MakeMin(uCount, uLength - uIndex);

		CopyOnWrite();

		memcpy( m_pText + uIndex,
			m_pText + uIndex + uCount,
			uLength - uIndex - uCount + 1
			);

		GetData(this)->m_uLen -= uCount;
		}
	}

// Whitespace Trimming

void CString::TrimLeft(void)
{
	UINT c = GetLength();

	UINT n = 0;

	while( m_pText[n] ) {

		if( !isspace(m_pText[n]) )
			break;

		n++;
		}

	if( n ) {

		if( !m_pText[n] ) {

			Empty();

			return;
			}

		CopyOnWrite();

		memcpy(m_pText, m_pText + n, c - n + 1);

		GetData(this)->m_uLen -= n;
		}
	}

void CString::TrimRight(void)
{
	UINT c = GetLength();

	UINT n = c;

	while( n ) {

		if( !isspace(m_pText[n - 1]) )
			break;

		n--;
		}

	if( n < c ) {

		if( !n ) {

			Empty();

			return;
			}

		CopyOnWrite();

		m_pText[n] = 0;

		GetData(this)->m_uLen = n;
		}
	}

void CString::TrimBoth(void)
{
	TrimRight();

	TrimLeft();
	}

void CString::StripAll(void)
{
	UINT s = 0;

	UINT e = 0;

	for(;;) { 
	
		while( m_pText[s] ) {

			if( isspace(m_pText[s]) )
				break;

			s++;
			}

		e = s;	
		
		while( m_pText[e] ) {

			if( !isspace(m_pText[e]) )
				break;

			e++;
			}
	
		UINT cb = e - s;
		
		if( cb ) {

			if( cb == GetLength() ) {
			
				Empty();

				return;
				}
			
			CopyOnWrite();

			if( e == GetLength() ) {

				m_pText[s] = 0;

				GetData(this)->m_uLen = s;
				
				return;
				}

			memcpy( m_pText + s,
				m_pText + e,
				GetLength() - s - cb + 1
				);

			GetData(this)->m_uLen -= cb;

			e = s;
			}
		else
			break;
		}
	}

// Charset Switching

void CString::MakeOem(void)
{
	CopyOnWrite();
	
	AnsiToOem(m_pText, m_pText);
	}

void CString::MakeAnsi(void)
{
	CopyOnWrite();
	
	OemToAnsi(m_pText, m_pText);
	}

// Case Switching

void CString::MakeUpper(void)
{
	CopyOnWrite();
	
	_strupr(m_pText);
	}

void CString::MakeLower(void)
{
	CopyOnWrite();
	
	_strlwr(m_pText);
	}

// Replacement

UINT CString::Replace(char cFind, char cNew)
{
	UINT uCount = 0;

	for( UINT n = 0; m_pText[n]; n++ ) {

		if( m_pText[n] == cFind ) {

			if( !uCount )
				CopyOnWrite();

			m_pText[n] = cNew;

			uCount++;
			}
		}

	return uCount;
	}

UINT CString::Replace(PCTXT pFind, PCTXT pNew)
{
	UINT uCount = 0;

	UINT uStart = 0;

	for(;;) {

		UINT uPos = Find(pFind, uStart);

		if( uPos < NOTHING ) {
			
			Delete(uPos, strlen(pFind));

			Insert(uPos, pNew);

			uStart = uPos + strlen(pNew);

			uCount ++;
			}
		else
			break;
		}

	return uCount;
	}

// Removal

void CString::Remove(char cFind)
{
	for(;;) {

		UINT uPos = Find(cFind);

		if( uPos == NOTHING ) {

			return;
			}

		Delete(uPos, 1);
		}
	}

CString CString::Without(char cFind)
{
	CString Work = ThisObject;

	Work.Remove(cFind);

	return Work;
	}

// Parsing

UINT CString::Tokenize(CStringArray &Array, char  cFind) const
{
	return Tokenize(Array, NOTHING, cFind);
	}

UINT CString::Tokenize(CStringArray &Array, PCTXT pFind) const
{
	return Tokenize(Array, NOTHING, pFind);
	}

UINT CString::Tokenize(CStringArray &Array, UINT uLimit, char  cFind) const
{
	UINT    uCount = 0;

	CString Text   = ThisObject;

	while( !Text.IsEmpty() ) {

		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(cFind));

		uCount++;
		}

	return uCount;
	}

UINT CString::Tokenize(CStringArray &Array, UINT uLimit, PCTXT pFind) const
{
	UINT    uCount = 0;

	CString Text   = ThisObject;

	while( !Text.IsEmpty() ) {
		
		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(pFind));

		uCount++;
		}

	return uCount;
	}

CString CString::StripToken(char  cFind)
{
	if( !IsEmpty() ) {

		UINT    uPos = Find(cFind);

		CString Text = Left(uPos);

		if( uPos == NOTHING )
			Empty();
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return "";
	}

CString CString::StripToken(PCTXT pFind)
{
	if( !IsEmpty() ) {

		UINT    uPos = FindOne(pFind);

		CString Text = Left(uPos);

		if( uPos == NOTHING )
			Empty();
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return "";
	}

// Printf Formatting

void CString::Printf(PCTXT pFormat, ...)
{
	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntPrintf(pFormat, (PTXT) (&pFormat + 1));
	
	GrowString(strlen(pResult));
	
	strcpy(m_pText, pResult);
	
	delete pResult;
	}

void CString::VPrintf(PCTXT pFormat, PTXT pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntPrintf(pFormat, pArgs);
	
	GrowString(strlen(pResult));
	
	strcpy(m_pText, pResult);
	
	delete pResult;
	}

// Windows Formatting

void CString::Format(PCTXT pFormat, ...)
{
	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntFormat(pFormat, (PTXT) (&pFormat + 1));
	
	GrowString(strlen(pResult));
	
	strcpy(m_pText, pResult);
	
	delete pResult;
	}

void CString::VFormat(PCTXT pFormat, PTXT pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntFormat(pFormat, pArgs);
	
	GrowString(strlen(pResult));
	
	strcpy(m_pText, pResult);
	
	delete pResult;
	}

// Resource Name Encoding

void CString::SetResourceName(PCTXT pName)
{
	if( HIWORD(pName) ) {
		
		Release();
		
		Alloc(strlen(pName));
		
		strcpy(m_pText, pName);
		}
	else {
		CTEXT sHex[] = "0123456789ABCDEF";
		
		Release();
		
		Alloc(5);
		
		m_pText[0] = '#';
		
		m_pText[1] = sHex[(LOWORD(pName) >> 12) & 15];
		m_pText[2] = sHex[(LOWORD(pName) >>  8) & 15];
		m_pText[3] = sHex[(LOWORD(pName) >>  4) & 15];
		m_pText[4] = sHex[(LOWORD(pName)      ) & 15];
		
		m_pText[5] = 0;
		}
	}

// Diagnostics

void CString::AssertValid(void) const
{
	AfxValidateReadPtr(this, sizeof(*this));
	
	CStringData *pData = GetData(this);
	
	if( pData->m_nRefs == 0 ) {
		
		AfxAssert(m_pText == m_Empty.m_cData);
		
		AfxAssert(pData->m_uLen == 0);
		}
	else
		AfxAssert(pData->m_uAlloc > pData->m_uLen);
	
	AfxValidateStringPtr(m_pText);

	AfxAssert(m_Empty.m_nRefs == 0);
	}

// Protected Constructor

CString::CString(PCTXT p1, UINT u1, PCTXT p2, UINT u2)
{
	Alloc(u1 + u2);
	
	strncpy(m_pText, p1, u1);
	
	strncpy(m_pText + u1, p2, u2);
	
	m_pText[u1 + u2] = 0;
	}

// Initialisation

void CString::InitFrom(LPOLESTR pText)
{
	if( pText ) {

		AfxValidateReadPtr(pText);

		UINT uLen;

		for( uLen = 0; pText[uLen]; uLen++ );

		if( uLen ) {

			Alloc(uLen);

			for( UINT n = 0; m_pText[n] = char(pText[n]); n++ );
			}
		}
	}

// Implementation

void CString::Alloc(UINT uLen, UINT uAlloc)
{
	AfxAssert(uAlloc > uLen);
	
	uAlloc = AdjustSize(uAlloc);
	
	UINT uSize = uAlloc + sizeof(CStringData);
	
	CStringData *pData = (CStringData *) New BYTE [uSize];
	
	pData->m_uLen   = uLen;
	
	pData->m_uAlloc = uAlloc;
	
	pData->m_nRefs  = 1;
	
	m_pText = pData->m_cData;
	}

void CString::Alloc(UINT uLen)
{
	Alloc(uLen, uLen + 1);
	}

void CString::GrowString(UINT uLen)
{
	CopyOnWrite();
	
	CStringData *pData = GetData(this);
	
	if( uLen >= pData->m_uAlloc ) {
		
		PCTXT pText = m_pText;
		
		Alloc(uLen);
		
		strcpy(m_pText, pText);
		
		delete pData;

		return;
		}

	pData->m_uLen = uLen;
	}

void CString::Release(void)
{
	CStringData *pData = GetData(this);
	
	if( pData->m_nRefs ) {
		
		if( !--(pData->m_nRefs) )
			delete pData;
		
		m_pText = m_Empty.m_cData;
		}
	}

UINT CString::AdjustSize(UINT uAlloc)
{
	return ((uAlloc + 3) & ~3);
	}

PTXT CString::IntPrintf(PCTXT pFormat, PTXT pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	UINT uSize = 256;
	
	for(;;) {
		
		PTXT pWork = New char [ uSize ];
		
		pWork[uSize - 1] = 0;
		
		if( _vsnprintf(pWork, uSize - 1, pFormat, pArgs) < 0 ) {
			
			delete pWork;
			
			uSize <<= 1;
			
			AfxAssert(uSize);
			
			continue;
			}
		
		return pWork;
		}
	}

PTXT CString::IntFormat(PCTXT pFormat, PTXT pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	UINT uSize = 256;
	
	for(;;) {
		
		PTXT pWork = New char [ uSize ];

		PTXT pCopy = pArgs;
		
		DWORD dwFlags = FORMAT_MESSAGE_FROM_STRING;

		if( !FormatMessage(dwFlags, pFormat, 0, 0, pWork, uSize, &pCopy) ) {
			
			delete pWork;
			
			uSize <<= 1;
			
			AfxAssert(uSize);
			
			continue;
			}
		
		return pWork;
		}
	}

// End of File
