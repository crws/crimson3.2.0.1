
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SCALAR_HPP
	
#define	INCLUDE_SCALAR_HPP

/////////////////////////////////////////////////////////////////////////
//
// Template File Name
//

#undef	TP_FILE

#define	TP_FILE __FILE__

/////////////////////////////////////////////////////////////////////////
//
// Auto Intialized Value
//

template <typename CData> class CZeroed
{
	public:
		// Constructors
		CZeroed(void);
		CZeroed(CZeroed const &That);
		CZeroed(CData Data);

		// Assignment
		CData const & operator = (CZeroed const &That);
		CData const & operator = (CData Data);
		
		// Conversion
		operator CData (void) const;

	protected:
		// Data Members
		CData m_Data;
	};

/////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <typename CData>

#define	TP2 CZeroed <CData>

/////////////////////////////////////////////////////////////////////////
//
// Intialized Scalar Value
//

// Constructors

TP1 TP2::CZeroed(void)
{
	m_Data = 0;
	}

TP1 TP2::CZeroed(CZeroed const &That)
{
	m_Data = That.m_Data;
	}

TP1 TP2::CZeroed(CData Data)
{
	m_Data = Data;
	}

// Assignment

TP1 CData const & TP2::operator = (CZeroed const &That)
{
	return m_Data = That.m_Data;
	}

TP1 CData const & TP2::operator = (CData Data)
{
	return m_Data = Data;
	}

// Conversion

TP1 TP2::operator CData (void) const
{
	return m_Data;
	}

// End of File

#endif
