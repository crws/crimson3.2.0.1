
#include "intern.hpp"

// Contants

#define	EMPTY "[!!!]"

// Entry Structure

struct CRecord
{
	UINT	m_uID;
	CString	m_Name;
	CString	m_Text;
	BOOL	m_fUsed;
	BOOL    m_fOld;
	};

// Type Definitions

typedef CArray <CRecord *>     CDatabase;

typedef CMap   <CString, UINT> CIndexString;

typedef CMap   <UINT, UINT>    CIndexID;

// Static Data

static	CString		m_Path;

static	CString		m_Proj;

static	UINT		m_uBase;

static	CDatabase	m_Dbase;

static	CIndexString	m_ByName;

static	CIndexString	m_ByText;
	 
static	CIndexID	m_ByID;

// Prototypes

global	void	main(int nArg, char *pArgs[]);
static	void	LoadStrings(void);
static	void	LoadIdents(void);
static	BOOL	ScanSource(BOOL fWrite);
static	void	ScanFile(FILE *pFile);
static	void	MakeName(CString &Name, CString Text);
static	void	AllocIdents(void);
static	BOOL	WriteIdents(void);
static	BOOL	WriteIdentHead(FILE *pTemp);
static	BOOL	WriteIdentTail(FILE *pTemp);
static	BOOL	WriteIdentData(FILE *pTemp);
static	BOOL	WriteStrings(void);
static	BOOL	WriteStringHead(FILE *pTemp);
static	BOOL	WriteStringTail(FILE *pTemp);
static	BOOL	WriteStringData(FILE *pTemp);
static	BOOL	WriteFile(FILE *pRead, FILE *pTemp);
static	BOOL	GetLine(FILE *pFile, CString &Line);
static	FILE *	CreateTemp(void);
static	BOOL	Replace(CString Name, FILE *pTemp);

// Code

global	void	main(int nArg, char *pArgs[])
{
	if( nArg == 2 ) {

		char cSep = '\\';

		m_Path = pArgs[1];

		if( m_Path.Right(1)[0] == cSep ) {

			UINT uLen = m_Path.GetLength();

			m_Path.Delete(uLen - 1, 1);
			}

		m_Proj  = m_Path.Mid(m_Path.FindRev(cSep) + 1);

		m_Path  = m_Path + cSep;

		m_uBase = 0x4000;

		LoadStrings();

		LoadIdents();

		ScanSource(FALSE);

		AllocIdents();

		if( WriteIdents() && WriteStrings() ) {

			if( ScanSource(TRUE) ) {

				printf("getids: done\n");

				exit(0);
				}
			}

		exit(1);
		}

	printf("usage: getids <project-path>\n");

	exit(2);
	}

static	void	LoadStrings(void)
{
	CString Name  = m_Path + m_Proj + ".rcc";

	FILE *  pFile = fopen(Name, "rt");

	if( pFile ) {

		UINT    uState = 0;

		CString Line   = "";

		while( GetLine(pFile, Line) ) {

			Line.TrimBoth();

			if( uState == 0 ) {

				if( Line == "STRINGTABLE" ) {

					uState = 1;
					}

				continue;
				}

			if( uState == 1 ) {

				if( Line == "BEGIN" ) {

					uState = 2;
					}

				continue;
				}

			if( uState == 2 ) {

				if( Line == "END" ) {

					uState = 0;
					}
				else {
					CString Name = Line.StripToken(" \t");

					Line.TrimBoth();

					CRecord *pRec = New CRecord;

					pRec->m_uID   = 0;

					pRec->m_Name  = Name;

					pRec->m_Text  = Line.Mid(1, Line.FindRev('"') - 1);

					pRec->m_fUsed = FALSE;

					pRec->m_fOld  = FALSE;

					UINT uPos = m_Dbase.Append(pRec);

					m_ByName.Insert(pRec->m_Name, uPos);

					m_ByText.Insert(pRec->m_Text, uPos);
					}
				}
			}

		fclose(pFile);
		}
	}

static	void	LoadIdents(void)
{
	CString Name  = m_Path + "intern.hxx";

	FILE *  pFile = fopen(Name, "rt");

	if( pFile ) {

		CString Line = "";

		while( GetLine(pFile, Line) ) {

			Line.TrimBoth();

			CString Init = Line.StripToken(" \t");

			Line.TrimBoth();

			if( Init == "#define" ) {

				CString Name = Line.StripToken(" \t");

				Line.TrimBoth();

				if( Name.Left(4) == "IDS_" ) {

					CString Data = Line.StripToken(" \t");

					if( Data[0] == '0' && Data[1] == 'x' ) {

						UINT uID = strtol(PCTXT(Data)+2, NULL, 16);
						
						if( Name == "IDS_BASE" ) {

							m_uBase = uID;
							}
						else {
							INDEX Index = m_ByName.FindName(Name);

							if( m_ByName.Failed(Index) ) {

								CRecord *pRec = New CRecord;

								pRec->m_uID   = uID;

								pRec->m_Name  = Name;

								pRec->m_Text  = EMPTY;

								pRec->m_fUsed = FALSE;

								pRec->m_fOld  = TRUE;

								UINT uPos = m_Dbase.Append(pRec);

								m_ByName.Insert(pRec->m_Name, uPos);

								m_ByID  .Insert(pRec->m_uID,  uPos);
								}
							else {
								UINT     uPos = m_ByName.GetData(Index);

								CRecord *pRec = m_Dbase[uPos];

								pRec->m_uID   = uID;

								pRec->m_fOld  = TRUE;

								m_ByID.Insert(pRec->m_uID, uPos);
								}
							}
						}
					}
				}
			}

		fclose(pFile);
		}
	}

static	BOOL	ScanSource(BOOL fWrite)
{
	WIN32_FIND_DATA Data;

	HANDLE hFind = FindFirstFile(m_Path + "\\*.cpp", &Data);

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			if( !(Data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ) {

				CString Name  = m_Path + Data.cFileName;

				FILE *  pFile = fopen(Name, "rt");

				if( pFile ) {

					if( fWrite ) {

						FILE *pTemp = CreateTemp();

						if( WriteFile(pFile, pTemp) ) {
							
							fclose(pFile);

							if( !Replace(Name, pTemp) ) {

								fclose(pTemp);

								return FALSE;
								}

							fclose(pTemp);
							}
						else
							fclose(pTemp);
						}
					else {
						ScanFile(pFile);

						fclose(pFile);
						}
					}
				}

			} while( FindNextFile(hFind, &Data) );

		FindClose(hFind);
		}

	return TRUE;
	}

static	void	ScanFile(FILE *pFile)
{
	CString Line = "";

	while( GetLine(pFile, Line) ) {

		PTXT    pScan = PTXT(PCTXT(Line));

		CString Copy  = Line;
	
		for(;;) {

			char *pText = strstr(pScan, "IDS(\"");

			if( pText ) {

				char *pEnd = strstr(pText += 5, "\")");

				if( pEnd ) {

					*pEnd = 0;

					INDEX Index = m_ByText.FindName(pText);

					if( m_ByText.Failed(Index) ) {

						CRecord *pRec = New CRecord;

						pRec->m_uID   = 0;

						pRec->m_Text  = pText;

						pRec->m_fUsed = TRUE;

						MakeName(pRec->m_Name, pRec->m_Text);

						UINT uPos = m_Dbase.Append(pRec);

						m_ByName.Insert(pRec->m_Name, uPos);

						m_ByText.Insert(pRec->m_Text, uPos);
						}
					else {
						UINT     uPos = m_ByText.GetData(Index);

						CRecord *pRec = m_Dbase[uPos];

						pRec->m_fUsed = TRUE;
						}

					pScan = pEnd + 1;

					continue;
					}
				}

			break;
			}

		for(;;) {

			char *pText = strstr(pScan, "IDS_");

			if( pText ) {

				char *pEnd = pText;

				for(;;) {
					
					if( isalnum(*pEnd) || *pEnd == '_' ) {

						pEnd++;
						}
					else {
						*pEnd = 0;

						INDEX Index = m_ByName.FindName(pText);

						if( !m_ByName.Failed(Index) ) {

							UINT     uPos = m_ByName.GetData(Index);

							CRecord *pRec = m_Dbase[uPos];

							pRec->m_fUsed = TRUE;
							}

						pScan = pEnd + 1;

						break;
						}
					}

				continue;
				}

			break;
			}
		}
	}

static	void	MakeName(CString &Name, CString Text)
{
	for( UINT uChar = 0; Text[uChar]; uChar++ ) {

		char cData = Text[uChar];

		if( !isalpha(cData) && !isspace(cData) && cData != '%' ) {

			Text.Remove(Text[uChar]);

			uChar--;
			}
		}

	CString Base = "IDS";

	CStringArray Words;

	Text.Tokenize(Words, ' ');

	for( UINT uWord = 0; uWord < Words.GetCount(); uWord++ ) {

		CString Word = Words[uWord];

		Word.TrimBoth();

		if( Word == "THE" || Word == "A" || Word == "AN" ) {

			continue;
			}

		if( Word[0] == '%' ) {

			if( Base.Right(4) == "_FMT" ) {

				continue;
				}

			Word = "FMT";
			}

		if( Word.Find('%') < NOTHING ) {

			continue;
			}

		if( Base.GetLength() + Word.GetLength() > 20 ) {

			break;
			}

		if( Word.GetLength() ) {

			Base += '_';
			
			Base += Word.ToUpper();
			}
		}

	if( Base == "IDS" ) {

		Base = "IDS_FORMAT";
		}

	for( UINT uDiff = 0;; uDiff++ ) {

		if( !uDiff ) {

			Name = Base;
			}
		else
			Name.Printf("%s_%u", Base, 1+uDiff);

		if( TRUE ) {

			INDEX Index = m_ByName.FindName(Name);

			if( !m_ByName.Failed(Index) ) {

				if( !uDiff ) {

					UINT     uPos = m_ByName.GetData(Index);

					CRecord *pRec = m_Dbase[uPos];

					if( !pRec->m_fOld ) {
		
						if( pRec->m_Name.Right(2) != "_1" ) {

							pRec->m_Name += "_1";
							}
						}
					}

				continue;
				}
			}
			
		break;
		}
	}

static	void	AllocIdents(void)
{
	UINT  uID   = m_uBase;

	INDEX Index = m_ByName.GetHead();

	while( !m_ByName.Failed(Index) ) {

		UINT     uPos = m_ByName.GetData(Index);

		CRecord *pRec = m_Dbase[uPos];
		
		if( pRec->m_Name.Left(4) == "IDS_" ) {

			pRec->m_uID = uID++;
			}

		m_ByName.GetNext(Index);
		}
	}

static	BOOL	WriteIdents(void)
{
	CString Name  = m_Path + "intern.hxx";

	FILE *  pRead = fopen(Name, "rt");

	FILE *  pTemp = CreateTemp();

	if( pRead && pTemp ) {

		UINT    uState = 0;

		CString Line   = "";

		while( GetLine(pRead, Line) ) {

			CString Work  = Line;

			Work.TrimBoth();

			if( uState == 0 ) {

				CString Init = Work.StripToken(" \t");

				Work.TrimBoth();

				if( Init == "#define" ) {

					CString Name = Work.StripToken(" \t");

					Work.TrimBoth();

					if( Name.Left(4) == "IDS_" ) {

						uState = 1;

						continue;
						}
					}

				if( Init == "//" ) {

					if( Work.Left(4) == "End " ) {

						if( m_Dbase.GetCount() ) {

							WriteIdentHead(pTemp);

							WriteIdentData(pTemp);

							WriteIdentTail(pTemp);
							}

						fprintf(pTemp, "\n%s\n", Line);

						uState = 2;

						continue;
						}
					}
				}

			if( uState == 1 ) {

				CString Init = Work.StripToken(" \t");

				Work.TrimBoth();

				if( Init == "#define" ) {

					CString Name = Work.StripToken(" \t");

					Work.TrimBoth();

					if( Name.Left(4) == "IDS_" ) {

						continue;
						}
					}

				WriteIdentData(pTemp);

				uState = 2;

				fprintf(pTemp, "%s\n", Line);

				continue;
				}

			fprintf(pTemp, "%s\n", Line);
			}

		fclose(pRead);

		if( uState < 2 ) {

			printf("getids: unable write identifiers\n");

			fclose(pTemp);

			return FALSE;
			}

		if( Replace(Name, pTemp) ) {

			fclose(pTemp);

			return TRUE;
			}

		fclose(pTemp);
		}

	return FALSE;
	}

static	BOOL	WriteIdentHead(FILE *pTemp)
{
	fprintf(pTemp, "//////////////////////////////////////////////////////////////////////////\n");
	fprintf(pTemp, "//\n");
	fprintf(pTemp, "// String Identifiers\n");
	fprintf(pTemp, "//\n");
	fprintf(pTemp, "//\n");
	fprintf(pTemp, "\n");

	return TRUE;
	}

static	BOOL	WriteIdentTail(FILE *pTemp)
{
	return TRUE;
	}

static	BOOL	WriteIdentData(FILE *pTemp)
{
	fprintf( pTemp,
		 "#define %-23.23s 0x%4.4X\n",
		 "IDS_BASE",
		 m_uBase
		 );

	INDEX n = m_ByName.GetHead();
	
	while( !m_ByName.Failed(n) ) {

		UINT     uPos = m_ByName.GetData(n);

		CRecord *pRec = m_Dbase[uPos];

		if( pRec->m_uID ) {

			if( pRec->m_Name != "IDS_BASE" ) {

				fprintf( pTemp,
					 "#define %-23.23s 0x%4.4X",
					 pRec->m_Name,
					 pRec->m_uID
					 );

				if( !pRec->m_fUsed ) {

					fprintf( pTemp,
						 " /* NOT USED */"
						 );
					}

				fprintf( pTemp,
					 "\n"
					 );
				}
			}

		m_ByName.GetNext(n);
		}

	return TRUE;
	}

static	BOOL	WriteStrings(void)
{
	CString Name  = m_Path + m_Proj + ".rcc";

	FILE *  pRead = fopen(Name, "rt");

	FILE *  pTemp = CreateTemp();

	if( pRead && pTemp ) {

		UINT    uState = 0;

		CString Line   = "";

		while( GetLine(pRead, Line) ) {

			CString Work = Line;

			Work.TrimBoth();

			if( uState == 0 ) {

				if( Work.Left(10) == "// String " ) {

					fprintf(pTemp, "%s\n", Line);

					uState = 1;

					continue;
					}

				if( Work.Left(7) == "// End " ) {

					if( m_Dbase.GetCount() ) {

						WriteStringHead(pTemp);

						WriteStringData(pTemp);

						WriteStringTail(pTemp);
						}

					fprintf(pTemp, "\n%s\n", Line);

					uState = 4;

					continue;
					}
				}

			if( uState == 1 ) {

				if( Work == "STRINGTABLE" ) {

					fprintf(pTemp, "%s\n", Line);

					uState = 2;

					continue;
					}
				}

			if( uState == 2 ) {

				if( Work == "BEGIN" ) {

					fprintf(pTemp, "%s\n", Line);

					uState = 3;

					continue;
					}
				}

			if( uState == 3 ) {

				if( Work == "END" ) {

					WriteStringData(pTemp);

					fprintf(pTemp, "%s\n", Line);

					uState = 4;

					continue;
					}

				continue;
				}

			fprintf(pTemp, "%s\n", Line);
			}

		fclose(pRead);

		if( uState < 4 ) {

			printf("getids: unable write string table\n");

			fclose(pTemp);

			return FALSE;
			}

		if( Replace(Name, pTemp) ) {

			fclose(pTemp);

			return TRUE;
			}

		fclose(pTemp);
		}

	return FALSE;
	}

static	BOOL	WriteStringHead(FILE *pTemp)
{
	fprintf(pTemp, "//////////////////////////////////////////////////////////////////////////\n");
	fprintf(pTemp, "//\n");
	fprintf(pTemp, "// String Table\n");
	fprintf(pTemp, "//\n");
	fprintf(pTemp, "//\n");
	fprintf(pTemp, "\n");
	fprintf(pTemp, "STRINGTABLE\n");
	fprintf(pTemp, "BEGIN\n");

	return TRUE;
	}

static	BOOL	WriteStringTail(FILE *pTemp)
{
	fprintf(pTemp, "END\n");

	return TRUE;
	}

static	BOOL	WriteStringData(FILE *pTemp)
{
	INDEX n = m_ByName.GetHead();
	
	while( !m_ByName.Failed(n) ) {

		UINT     uPos = m_ByName.GetData(n);

		CRecord *pRec = m_Dbase[uPos];

		if( pRec->m_uID ) {

			if( pRec->m_Text != EMPTY ) {

				fprintf( pTemp,
					 "\t%-23.23s \"%s\"",
					 pRec->m_Name,
					 pRec->m_Text
					 );
				
				if( !pRec->m_fUsed ) {

					fprintf( pTemp,
						 " /* NOT USED */"
						 );
					}

				fprintf( pTemp,
					 "\n"
					 );
				}
			}

		m_ByName.GetNext(n);
		}

	return TRUE;
	}

static	BOOL	WriteFile(FILE *pRead, FILE *pTemp)
{
	BOOL    fHit = FALSE;

	CString Line = "";

	while( GetLine(pRead, Line) ) {

		if( Line.Find("IDS(\"") < NOTHING ) {

			UINT c = m_Dbase.GetCount();

			for( UINT n = 0; n < c; n++ ) {

				CRecord *pRec = m_Dbase[n];

				CString  Find = "IDS(\"" + pRec->m_Text + "\")";

				CString  Repl = "CString(" + pRec->m_Name + ")";

				Line.Replace(Find, Repl);
				}

			fHit = TRUE;
			}
		
		fprintf(pTemp, "%s\n", Line);
		}

	return fHit;
	}

static	BOOL	GetLine(FILE *pFile, CString &Line)
{
	if( !feof(pFile) ) {

		char sLine[1024] = { 0 };

		fgets(sLine, sizeof(sLine), pFile);

		if( sLine[0] ) {

			UINT n = strlen(sLine);

			Line   = CString(sLine, n-1);

			return TRUE;
			}
		}

	return FALSE;
	}

static	FILE *	CreateTemp(void)
{
	return fopen("c:\\temp\\getids", "w+t");
	}

static	BOOL	Replace(CString Name, FILE *pTemp)
{
	FILE *pFile = fopen(Name, "wt");

	if( pFile ) {
	
		fseek(pTemp, 0, SEEK_SET);

		UINT  uSize = 65536;

		PBYTE pData = new BYTE [ uSize ];

		for(;;) {

			UINT n = fread(pData, 1, uSize, pTemp);

			fwrite(pData, 1, n, pFile);

			if( n < uSize ) {

				break;
				}
			}

		delete pData;

		fclose(pFile);

		return TRUE;
		}

	printf("getids: unable to replace %s\n", Name);

	return FALSE;
	}

// End of File
