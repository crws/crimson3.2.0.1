
#include "Intern.hpp"

// Externals

extern void rc4(PBYTE pCode, PCBYTE pData, UINT uData, PCBYTE pKey, UINT uKey);

extern void rc4(PBYTE pData, UINT uData, PCBYTE pKey, UINT uKey);

// Prototypes

static	void	Error(char const *text, ...);
static	void	AfxDump(void const *pData, size_t uCount);
static	size_t	Tokenize(vector<string> &list, string text, char sep);
static	string	Decrypt(string const &text, string const &pass);
static	string	GetPeerName(sockaddr_in const &peer);
static	bool	FindPasswords(vector<char> &tx, string const &file, string const &serial, string const &pass);
static	void	Append(vector<char> &tx, string const &text);
static	void	Remove(string &text, char c);

// Code

int main(int nArg, char const *pArg[])
{
	if( nArg == 2 ) {

		WSADATA Data;

		WSAStartup(MAKEWORD(2, 2), &Data);

		SOCKET s1 = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);

		if( s1 > 0 ) {

			sockaddr_in addr = { 0 };

			addr.sin_family = AF_INET;

			addr.sin_port   = htons(7778);

			if( bind(s1, (sockaddr *) &addr, sizeof(addr)) >= 0 ) {

				if( listen(s1, 10) >= 0 ) {

					for( ;;) {

						int s2 = accept(s1, NULL, 0);

						if( s2 > 0 ) {

							vector<char> rx(16384, 0);

							int n = recv(s2, rx.data(), rx.size(), MSG_WAITALL);

							if( n > 0 ) {

								rx[n] = 0;

								sockaddr_in peer = { 0 };

								int         size = sizeof(peer);

								getpeername(s2, (sockaddr *) &peer, &size);

								shutdown(s2, SD_RECEIVE);

								vector<string> list;

								bool   pass = false;

								string text(rx.data());

								Remove(text, '\r');

								if( Tokenize(list, text, '\n') == 2 ) {

									vector<char> tx;

									if( FindPasswords(tx, pArg[1], list[0], list[1]) ) {

										send(s2, tx.data(), tx.size(), 0);

										shutdown(s2, SD_SEND);
									}
								}
							}

							closesocket(s2);
						}
					}
				}
			}
		}

		Error("unable to create socket");
	}

	Error("no log file specified");
}

static void Error(char const *text, ...)
{
	va_list args;

	va_start(args, text);

	printf("DecodeServer: ");

	vprintf(text, args);

	printf("\n");

	va_end(args);

	exit(1);
}

static void AfxDump(void const *pData, size_t uCount)
{
	if( pData ) {

		BYTE const *p = (BYTE const *) pData;

		UINT        s = 0;

		for( UINT n = 0; n < uCount; n++ ) {

			if( n % 0x10 == 0x0 ) {

				printf("%8.8X : %4.4X : ", DWORD(p + n), n);

				s = n;
			}

			if( true ) {

				printf("%2.2X ", p[n]);
			}

			if( n % 0x10 == 0xF || n == uCount - 1 ) {

				printf(" ");

				for( UINT j = n; j % 0x10 < 0xF; j++ ) {

					printf("   ");
				}

				for( UINT i = 0; i <= n - s; i++ ) {

					BYTE b = p[s+i];

					if( b >= 32 && b < 127 ) {

						printf("%c", b);
					}
					else
						printf(".");
				}

				printf("\n");
			}
		}
	}
}

static size_t Tokenize(vector<string> &list, string text, char sep)
{
	size_t p;

	while( !text.empty() ) {

		if( (p = text.find(sep)) == string::npos ) {

			list.push_back(text);

			break;
		}

		list.push_back(text.substr(0, p));

		text.erase(0, p+1);
	}

	return list.size();
}

static string Decrypt(string const &text, string const &pass)
{
	bytes data;

	for( size_t s = 0; s < text.size(); s += 2 ) {

		char h[3] = { text[s+0], text[s+1], 0 };

		data.push_back(BYTE(strtoul(h, NULL, 16)));
	}

	rc4(data.data(), data.size(), PCBYTE(pass.data()), pass.size());

	return string(PCSTR(data.data()), data.size());
}

static string GetPeerName(sockaddr_in const &peer)
{
	char p[32];

	sprintf(p,
		"%u.%u.%u.%u",
		peer.sin_addr.S_un.S_un_b.s_b1,
		peer.sin_addr.S_un.S_un_b.s_b2,
		peer.sin_addr.S_un.S_un_b.s_b3,
		peer.sin_addr.S_un.S_un_b.s_b4
	);

	return p;
}

static bool FindPasswords(vector<char> &tx, string const &file, string const &serial, string const &pass)
{
	HANDLE hRead = CreateFile(file.c_str(),
				  GENERIC_READ,
				  FILE_SHARE_READ | FILE_SHARE_WRITE,
				  NULL,
				  OPEN_EXISTING,
				  0,
				  NULL
	);

	if( hRead != INVALID_HANDLE_VALUE ) {

		UINT  size = GetFileSize(hRead, NULL);

		DWORD read = 0;

		vector<char> log(size, 0);

		ReadFile(hRead, log.data(), log.size(), &read, NULL);

		CloseHandle(hRead);

		if( read == size ) {

			log.push_back(0);

			string match = "," + serial + ",";

			char const *find = log.data();

			for( ;;) {

				if( (find = strstr(find, match.c_str())) ) {
					
					find += match.size();

					string code(find, strchr(find, '\r'));

					for( size_t p = 0; (p = code.find(',')) != code.npos; ) {

						code.erase(0, 1+p);
					}

					string text(Decrypt(code, pass));

					if( text.size() ) {

						vector<size_t> p;

						for( size_t n = 0; text[n]; n++ ) {

							if( text[n] == '|' ) {

								p.push_back(n);
							}
						}

						if( p.size() == 2 ) {

							Append(tx, "root    : ");

							Append(tx, text.substr(0, p[0]).c_str());

							Append(tx, "\r\n");

							Append(tx, "rlcuser : ");

							Append(tx, text.substr(p[0] + 1, p[1] - p[0] - 1).c_str());

							Append(tx, "\r\n");

							Append(tx, "spare   : ");

							Append(tx, text.substr(p[1] + 1).c_str());

							Append(tx, "\r\n");

							return true;
						}
					}

					continue;
				}

				break;
			}
		}
	}

	return false;
}

static void Append(vector<char> &tx, string const &text)
{
	tx.insert(tx.end(), text.data(), text.data() + text.size());
}

static void Remove(string &text, char c)
{
	text.erase(remove(text.begin(), text.end(), c), text.end());
}

// End of File
