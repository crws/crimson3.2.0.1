
#define  _WINSOCK_DEPRECATED_NO_WARNINGS

#define  _CRT_SECURE_NO_WARNINGS

#include <winsock2.h>

#include <Windows.h>

#include <time.h>

#include <map>

#include <string>

#include <vector>

using namespace std;

typedef vector<BYTE> bytes;

typedef BYTE const * PCBYTE;
