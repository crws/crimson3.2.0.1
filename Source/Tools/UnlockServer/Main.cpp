
#include "Intern.hpp"

// Static Data

static BCRYPT_ALG_HANDLE m_hShaAlg = NULL;

static BCRYPT_ALG_HANDLE m_hSigAlg = NULL;

static BCRYPT_KEY_HANDLE m_hSigKey = NULL;

// Externals

extern void rc4(PBYTE pCode, PCBYTE pData, UINT uData, PCBYTE pKey, UINT uKey);

extern void rc4(PBYTE pData, UINT uData, PCBYTE pKey, UINT uKey);

// Prototypes

static	void	Error(char const *text, ...);
static	void	AfxDump(void const *pData, size_t uCount);
static	size_t	Tokenize(vector<string> &list, string text, char sep);
static	bool	LogInfo(char const *name, vector<string> const &list, sockaddr_in const &peer, bool pass);
static	bool	OpenAlgorithms(void);
static	bool	ReadSigningKeyFromFile(string const &file);
static	bool	LoadSigningKey(string const &text);
static	bool	StringToBinary(bytes &bin, char const *text, size_t size);
static	bool	BinaryToString(string &text, bytes const &bin);
static	bool	DecodeObject(bytes &obj, bytes const &bin, DWORD enc, PCSTR type);
static	bool	EncodeObject(bytes &bin, bytes const &obj, DWORD enc, PCSTR type);
static	bool	MakeHash(BCRYPT_ALG_HANDLE hAlg, bytes &hash, vector<char> const &data);
static	bool	SignHash(BCRYPT_ALG_HANDLE hKey, string &text, bytes const &hash);
static	bool	MakeKeyFile(vector<char> &tx, vector<string> const &list);
static	void	Append(vector<char> &tx, string const &s);
static	void	Append(vector<char> &tx, char const *p);
static	void	Append(vector<char> &tx, char c);
static	void	Append(bytes &data, BYTE const *p, size_t s);
static	void	Reverse(PBYTE p, size_t n);
static	string	Encrypt(string const &text, string const &pass);
static	string	Decrypt(string const &text, string const &pass);
static	string	GetPeerName(sockaddr_in const &peer);
static	string	GetTimeStamp(void);
static	void	Remove(string &text, char c);

// Code

int main(int nArg, char const *pArg[])
{
	if( nArg == 3 ) {

		if( OpenAlgorithms() ) {

			if( ReadSigningKeyFromFile(pArg[1]) ) {

				WSADATA Data;

				WSAStartup(MAKEWORD(2, 2), &Data);

				SOCKET s1 = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);

				if( s1 > 0 ) {

					sockaddr_in addr = { 0 };

					addr.sin_family = AF_INET;

					addr.sin_port   = htons(7777);

					if( bind(s1, (sockaddr *) &addr, sizeof(addr)) >= 0 ) {

						if( listen(s1, 10) >= 0 ) {

							for( ;;) {

								int s2 = accept(s1, NULL, 0);

								if( s2 > 0 ) {

									vector<char> rx(16384, 0);

									int n = recv(s2, rx.data(), rx.size(), MSG_WAITALL);

									if( n > 0 ) {

										rx[n] = 0;

										sockaddr_in peer = { 0 };

										int         size = sizeof(peer);

										getpeername(s2, (sockaddr *) &peer, &size);

										shutdown(s2, SD_RECEIVE);

										vector<string> list;

										bool   pass = false;

										string text(rx.data());

										Remove(text, '\r');

										switch( Tokenize(list, text, '\n') ) {

											case 9:
											{
												pass = true;
											}

											case 5:
											{
												LogInfo(pArg[2], list, peer, pass);

												vector<char> tx;

												if( MakeKeyFile(tx, list) ) {

													send(s2, tx.data(), tx.size(), 0);

													shutdown(s2, SD_SEND);
												}
											}
										}

										closesocket(s2);
									}
								}
							}
						}
					}
				}

				Error("unable to create socket");
			}

			Error("unable to load signing key");
		}

		Error("unable to open crypto algorithms");
	}

	Error("no signing key file specified");
}

static void Error(char const *text, ...)
{
	va_list args;

	va_start(args, text);

	printf("UnlockServer: ");

	vprintf(text, args);

	printf("\n");

	va_end(args);

	exit(1);
}

static void AfxDump(void const *pData, size_t uCount)
{
	if( pData ) {

		BYTE const *p = (BYTE const *) pData;

		UINT        s = 0;

		for( UINT n = 0; n < uCount; n++ ) {

			if( n % 0x10 == 0x0 ) {

				printf("%8.8X : %4.4X : ", DWORD(p + n), n);

				s = n;
			}

			if( true ) {

				printf("%2.2X ", p[n]);
			}

			if( n % 0x10 == 0xF || n == uCount - 1 ) {

				printf(" ");

				for( UINT j = n; j % 0x10 < 0xF; j++ ) {

					printf("   ");
				}

				for( UINT i = 0; i <= n - s; i++ ) {

					BYTE b = p[s+i];

					if( b >= 32 && b < 127 ) {

						printf("%c", b);
					}
					else
						printf(".");
				}

				printf("\n");
			}
		}
	}
}

static size_t Tokenize(vector<string> &list, string text, char sep)
{
	size_t p;

	while( !text.empty() ) {

		if( (p = text.find(sep)) == string::npos ) {

			list.push_back(text);

			break;
		}

		list.push_back(text.substr(0, p));

		text.erase(0, p+1);
	}

	return list.size();
}

static bool LogInfo(char const *name, vector<string> const &list, sockaddr_in const &peer, bool pass)
{
	string line;

	line += list[2];

	line += ',';

	line += list[0];

	line += ',';

	line += GetPeerName(peer);

	line += ',';

	line += GetTimeStamp();

	if( pass ) {

		string save;

		save += list[6] + '|';
		save += list[7] + '|';
		save += list[8];

		line += ',';

		line += Encrypt(save, list[5]);
	}

	line += "\r\n";

	HANDLE hLog = CreateFile(name,
				 GENERIC_READ | GENERIC_WRITE,
				 0,
				 NULL,
				 OPEN_ALWAYS,
				 0,
				 NULL
	);

	if( hLog != INVALID_HANDLE_VALUE ) {

		DWORD uDone = 0;

		SetFilePointer(hLog, 0, NULL, FILE_END);

		WriteFile(hLog, line.data(), line.size(), &uDone, NULL);

		CloseHandle(hLog);

		if( uDone == line.size() ) {

			return true;
		}
	}

	return false;
}

static bool OpenAlgorithms(void)
{
	if( !BCryptOpenAlgorithmProvider(&m_hShaAlg, BCRYPT_SHA256_ALGORITHM, NULL, 0) ) {

		if( !BCryptOpenAlgorithmProvider(&m_hSigAlg, BCRYPT_ECDSA_P256_ALGORITHM, NULL, 0) ) {

			return true;
		}
	}

	return false;
}

static bool ReadSigningKeyFromFile(string const &file)
{
	HANDLE hRead = CreateFile(file.c_str(),
				  GENERIC_READ,
				  0,
				  NULL,
				  OPEN_EXISTING,
				  0,
				  NULL
	);

	if( hRead != INVALID_HANDLE_VALUE ) {

		UINT   size = GetFileSize(hRead, NULL);

		DWORD  read = 0;

		string text(size, 0);

		ReadFile(hRead, PSTR(text.data()), text.size(), &read, NULL);

		CloseHandle(hRead);

		if( read == size ) {

			return LoadSigningKey(text);
		}
	}

	Error("unable to open file");

	return false;
}

static bool LoadSigningKey(string const &text)
{
	bytes key;

	if( StringToBinary(key, text.data(), text.size()) ) {

		DWORD enc  = PKCS_7_ASN_ENCODING;

		PCSTR type = X509_ECC_PRIVATE_KEY;

		bytes obj;

		if( DecodeObject(obj, key, enc, type) ) {

			CRYPT_ECC_PRIVATE_KEY_INFO *e = (CRYPT_ECC_PRIVATE_KEY_INFO *) obj.data();

			bytes key(sizeof(BCRYPT_ECCKEY_BLOB), 0);

			BCRYPT_ECCKEY_BLOB *k = (BCRYPT_ECCKEY_BLOB *) key.data();

			k->dwMagic = BCRYPT_ECDSA_PRIVATE_P256_MAGIC;

			k->cbKey   = e->PrivateKey.cbData;

			Append(key, e->PublicKey.pbData + 0x01, 0x20);

			Append(key, e->PublicKey.pbData + 0x21, 0x20);

			Append(key, e->PrivateKey.pbData, 0x20);

			if( BCryptImportKeyPair(m_hSigAlg,
						NULL,
						BCRYPT_ECCPRIVATE_BLOB,
						&m_hSigKey,
						key.data(),
						key.size(),
						0) == 0 ) {

				return true;
			}

			Error("unable import keys");

		}

		Error("unable to decode");
	}

	Error("unable to string to bin");

	return false;
}

static bool StringToBinary(bytes &bin, char const *text, size_t size)
{
	DWORD out = 0;

	if( CryptStringToBinary(text,
				DWORD(size),
				0,
				NULL,
				&out,
				NULL,
				NULL
	) ) {
		if( out ) {

			bin.resize(out);

			if( CryptStringToBinary(text,
						size,
						0,
						bin.data(),
						&out,
						NULL,
						NULL
			) ) {
				return true;
			}
		}
	}

	return false;
}

static bool BinaryToString(string &text, bytes const &bin)
{
	DWORD out = 0;

	if( CryptBinaryToString(bin.data(),
				bin.size(),
				CRYPT_STRING_BASE64,
				NULL,
				&out
	) ) {
		if( out ) {

			text.resize(out-1);

			if( CryptBinaryToString(bin.data(),
						bin.size(),
						CRYPT_STRING_BASE64,
						PSTR(text.data()),
						&out
			) ) {
				size_t n;

				while( (n = text.find('\r')) != string::npos ) {

					text.erase(n, 1);
				}

				while( (n = text.find('\n')) != string::npos ) {

					text.erase(n, 1);
				}

				return true;
			}
		}
	}

	return false;
}

static bool DecodeObject(bytes &obj, bytes const &bin, DWORD enc, PCSTR type)
{
	DWORD out = 0;

	if( CryptDecodeObject(enc,
			      type,
			      bin.data(),
			      bin.size(),
			      0,
			      NULL,
			      &out
	) ) {
		if( out ) {

			obj.resize(out);

			if( CryptDecodeObject(enc,
					      type,
					      bin.data(),
					      bin.size(),
					      0,
					      obj.data(),
					      &out
			) ) {
				return true;
			}
		}
	}

	printf("Error %8.8X\n", GetLastError());

	return false;
}

static bool EncodeObject(bytes &bin, bytes const &obj, DWORD enc, PCSTR type)
{
	DWORD out = 0;

	if( CryptEncodeObject(enc,
			      type,
			      obj.data(),
			      NULL,
			      &out
	) ) {
		if( out ) {

			bin.resize(out);

			if( CryptEncodeObject(enc,
					      type,
					      obj.data(),
					      bin.data(),
					      &out
			) ) {
				return true;
			}
		}
	}

	return false;
}

static bool MakeHash(BCRYPT_ALG_HANDLE hAlg, bytes &hash, vector<char> const &data)
{
	ULONG uDone = 0;

	UINT  uWork = 0;

	UINT  uHash = 0;

	BCryptGetProperty(hAlg, BCRYPT_OBJECT_LENGTH, PBYTE(&uWork), 4, &uDone, 0);

	BCryptGetProperty(hAlg, BCRYPT_HASH_LENGTH, PBYTE(&uHash), 4, &uDone, 0);

	if( uWork && uHash ) {

		bytes work;

		work.resize(uWork);

		hash.resize(uHash);

		BCRYPT_HASH_HANDLE hHash = NULL;

		if( !BCryptCreateHash(hAlg, &hHash, work.data(), work.size(), NULL, 0, 0) ) {

			if( !BCryptHashData(hHash, PUCHAR(data.data()), data.size(), 0) ) {

				if( !BCryptFinishHash(hHash, hash.data(), hash.size(), 0) ) {

					if( !BCryptDestroyHash(hHash) ) {

						return true;
					}
				}
			}
		}
	}

	return false;
}

static bool SignHash(BCRYPT_ALG_HANDLE hKey, string &text, bytes const &hash)
{
	ULONG uSize = 0;

	if( !BCryptSignHash(hKey,
			    NULL,
			    PUCHAR(hash.data()),
			    hash.size(),
			    NULL,
			    0,
			    &uSize,
			    0
	) ) {
		bytes sign(uSize, 0);

		if( !BCryptSignHash(hKey,
				    NULL,
				    PUCHAR(hash.data()),
				    hash.size(),
				    sign.data(),
				    sign.size(),
				    &uSize,
				    0
		) ) {
			bytes bin;

			bytes info(sizeof(CERT_ECC_SIGNATURE), 0);

			CERT_ECC_SIGNATURE *i = (CERT_ECC_SIGNATURE *) info.data();

			i->r.cbData = 0x20;
			i->r.pbData = sign.data() + 0x00;
			i->s.cbData = 0x20;
			i->s.pbData = sign.data() + 0x20;

			Reverse(i->r.pbData, 0x20);

			Reverse(i->s.pbData, 0x20);

			DWORD enc  = X509_ASN_ENCODING;

			PCSTR type = X509_ECC_SIGNATURE;

			if( EncodeObject(bin, info, enc, type) ) {

				BinaryToString(text, bin);

				return true;
			}
		}
	}

	return false;
}

static bool MakeKeyFile(vector<char> &tx, vector<string> const &list)
{
	Append(tx, "K00\n");

	Append(tx, list[1]);

	Append(tx, "\n00000000\n0000000");

	switch( atoi(list[0].c_str()) ) {

		case 1:
			Append(tx, '0');
			break;

		case 2:
			Append(tx, '1');
			break;

		case 3:
			Append(tx, '4');
			break;

		case 4:
			Append(tx, '5');
			break;

		default:
			return false;
	}

	Append(tx, '\n');

	bytes hash;

	if( MakeHash(m_hShaAlg, hash, tx) ) {

		string text;

		if( SignHash(m_hSigKey, text, hash) ) {

			Append(tx, text);

			Append(tx, '\n');

			return true;
		}
	}

	return false;
}

static void Append(vector<char> &tx, string const &s)
{
	tx.insert(tx.end(), s.begin(), s.end());
}

static void Append(vector<char> &tx, char const *p)
{
	tx.insert(tx.end(), p, p + strlen(p));
}

static void Append(vector<char> &tx, char c)
{
	tx.push_back(c);
}

static void Append(bytes &data, BYTE const *p, size_t s)
{
	data.insert(data.end(), p, p + s);
}

static void Reverse(PBYTE p, size_t n)
{
	size_t i = 0;

	size_t j = n - 1;

	while( i < j ) {

		BYTE t = p[i];

		p[i] = p[j];

		p[j] = t;

		i++;
		j--;
	}
}

static string Encrypt(string const &text, string const &pass)
{
	bytes data(text.size(), 0);

	rc4(data.data(), PCBYTE(text.data()), text.size(), PCBYTE(pass.data()), pass.size());

	string code;

	for( BYTE b : data ) {

		static char hex[] = "0123456789ABCDEF";

		code += hex[b / 16];
		code += hex[b % 16];
	}

	return code;
}

static string Decrypt(string const &text, string const &pass)
{
	bytes data;

	for( size_t s = 0; s < text.size(); s += 2 ) {

		char h[3] = { text[s+0], text[s+1], 0 };

		data.push_back(BYTE(strtoul(h, NULL, 16)));
	}

	rc4(data.data(), data.size(), PCBYTE(pass.data()), pass.size());

	return string(PCSTR(data.data()), data.size());
}

static string GetPeerName(sockaddr_in const &peer)
{
	char p[32];

	sprintf(p,
		"%u.%u.%u.%u",
		peer.sin_addr.S_un.S_un_b.s_b1,
		peer.sin_addr.S_un.S_un_b.s_b2,
		peer.sin_addr.S_un.S_un_b.s_b3,
		peer.sin_addr.S_un.S_un_b.s_b4
	);

	return p;
}

static string GetTimeStamp(void)
{
	char t[32];

	sprintf(t, "%u", UINT(time(NULL)));

	return t;
}

static void Remove(string &text, char c)
{
	text.erase(remove(text.begin(), text.end(), c), text.end());
}

// End of File
