
#include "intern.hpp"

#include "gdiplus.hpp"

// Static Data

static	ULONG	gdipToken;

// Prototypes

global	void	main(int nArg, char *pArg[]);
static	void	InitGDIP(void);
static	void	TermGDIP(void);
static	int	GetFileCount(void);
static	void	SaveBitmap(HDC hDC, HBITMAP hBitmap, int cx, int cy, PCSTR pName);
static	void	DrawFlags(HDC hDC);
static	void	DrawFlag(HDC hDC, int xPos, int yPos, PCSTR pFile);

// Code

global	void	main(int nArg, char *pArg[])
{
	InitGDIP();

	int     nCount  = GetFileCount();

	int     cx      = 16 * nCount;

	int     cy      = 48;

	HDC     hRefDC  = GetDC(NULL);

	HDC     hDC     = CreateCompatibleDC(hRefDC);

	HBITMAP hBitmap = CreateCompatibleBitmap(hRefDC, cx, cy);

	HBITMAP hPrev   = SelectObject(hDC, hBitmap);

	HBRUSH  hBrush  = CreateSolidBrush(RGB(255,0,255));

	RECT    Rect    = { 0, 0, cx, cy };

	FillRect(hDC, &Rect, hBrush);

	DrawFlags(hDC);

	SaveBitmap(hDC, hBitmap, cx, cy, "flags.bmp");

	SelectObject(hDC, hPrev);

	DeleteObject(hBrush);

	DeleteDC(hDC);

	ReleaseDC(NULL, hRefDC);

	TermGDIP();
	}

static	void	InitGDIP(void)
{
	GdiplusStartupOutput Output;

	GdiplusStartupInput  Input;

	memset(&Input, 0, sizeof(Input));

	Input.GdiplusVersion = 1;

	GdiplusStartup(&gdipToken, &Input, &Output);
	}

static	void	TermGDIP(void)
{
	GdiplusShutdown(gdipToken);
	}

static	int	GetFileCount(void)
{
	WIN32_FIND_DATA Find;

	int     nFile = 0;

	HANDLE hFind = FindFirstFile("PNGs\\*.png", &Find);

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			nFile++;
			
			} while( FindNextFile(hFind, &Find) );
		}

	FindClose(hFind);

	return nFile;
	}

static	void	SaveBitmap(HDC hDC, HBITMAP hBitmap, int cx, int cy, PCSTR pName)
{
	UINT             uSize  = cx * cy * 4;

	BYTE             *pData = new BYTE [ uSize ];

	BITMAPINFO       *pInfo = new BITMAPINFO;

	BITMAPFILEHEADER *pHead = new BITMAPFILEHEADER;

	////////

	memset(pHead, 0, sizeof(BITMAPFILEHEADER));

	pHead->bfType    = 'MB';
	pHead->bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO);
	pHead->bfSize    = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO) + uSize;

	////////

	memset(pInfo, 0, sizeof(BITMAPINFO));

	pInfo->bmiHeader.biSize		 = sizeof(BITMAPINFO);
	pInfo->bmiHeader.biWidth	 = cx;
	pInfo->bmiHeader.biHeight	 = cy;
	pInfo->bmiHeader.biPlanes	 = 1;
	pInfo->bmiHeader.biBitCount	 = 32;
	pInfo->bmiHeader.biCompression	 = BI_RGB;
	pInfo->bmiHeader.biSizeImage	 = 0;
	pInfo->bmiHeader.biXPelsPerMeter = 0;
	pInfo->bmiHeader.biYPelsPerMeter = 0;
	pInfo->bmiHeader.biClrUsed	 = 0;
	pInfo->bmiHeader.biClrImportant	 = 0;

	////////

	GetDIBits( hDC,
		   hBitmap,
		   0,
		   cy,
		   pData,
		   pInfo,
		   DIB_RGB_COLORS
		   );

	////////

	FILE *pWrite = NULL;

	if( !fopen_s(&pWrite, pName, "wb") ) {

		fwrite(pHead, sizeof(*pHead), 1, pWrite);

		fwrite(pInfo, sizeof(*pInfo), 1, pWrite);

		fwrite(pData, uSize, 1, pWrite);

		fclose(pWrite);

		////////

		delete pData;

		delete pHead;

		delete pInfo;
		
		return;
		}

	printf("makefl: can't open %s\n", pName);

	exit(4);
	}

static	void	DrawFlags(HDC hDC)
{
	WIN32_FIND_DATA Find;

	int     nFile = 0;

	HANDLE hFind = FindFirstFile("PNGs\\*.png", &Find);

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			char sFile[128];

			strcpy_s(sFile, sizeof(sFile), "PNGs\\");

			strcat_s(sFile, sizeof(sFile), Find.cFileName);

			printf("%3u = %s\n", nFile, Find.cFileName);

			DrawFlag(hDC, 16 * nFile, 2, sFile);

			nFile++;
			
			} while( FindNextFile(hFind, &Find) );
		}

	FindClose(hFind);
	}

static	void	DrawFlag(HDC hDC, int xPos, int yPos, PCSTR pFile)
{
	HANDLE hFile = CreateFile( pFile,
				   GENERIC_READ,
				   FILE_SHARE_READ,
				   NULL,
				   OPEN_EXISTING,
				   FILE_FLAG_SEQUENTIAL_SCAN,
				   NULL
				   );

	if( hFile != INVALID_HANDLE_VALUE ) {

		UINT   uData = GetFileSize(hFile, NULL);

		HANDLE hData = GlobalAlloc(GHND, uData);

		if( hData ) {

			PBYTE  pData = PBYTE(GlobalLock(hData));

			DWORD  uRead = 0;

			ReadFile   (hFile, pData, uData, &uRead, NULL);

			CloseHandle(hFile);

			if( uRead == uData ) {

				GlobalUnlock(hData);

				IStream *pStream = NULL;

				GpImage *pImage  = NULL;

				CreateStreamOnHGlobal(hData, TRUE, &pStream);

				GdipLoadImageFromStream(pStream, &pImage);

				GpGraphics *pGraph = NULL;

				GdipCreateFromHDC(hDC, &pGraph);

				int ix, iy;

				GdipGetImageWidth(pImage,  (UINT *) &ix);

				GdipGetImageHeight(pImage, (UINT *) &iy);

				if( ix < 16 ) {

					xPos += (16 - ix) / 2;
					}

				for( int n = 0; n < 3; n++ ) {

					GdipDrawImageRectRectI( pGraph,
								pImage,
								xPos,
								yPos,
								16,
								12,
								0,
								0,
								16,
								12,
								UnitPixel,
								NULL,
								NULL,
								NULL
								);

					yPos += 16;
					}

				GdipDeleteGraphics(pGraph);

				GdipDisposeImage(pImage);
				}
			}
		}
	}

// End of File
