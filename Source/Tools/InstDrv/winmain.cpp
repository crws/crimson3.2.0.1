
#include "intern.hpp"

// Prototypes

static	BOOL	Install(WCHAR const *pInf, WCHAR const *pID);

// Code

global int WINAPI WinMain(HINSTANCE hThis, HINSTANCE hPrev, PSTR pCmdLine, int nCmdShow)
{
	WCHAR const  *  pLine  = GetCommandLine();

	INT             nCount = 0;

	WCHAR       * * pList  = CommandLineToArgvW(pLine, &nCount);

	if( nCount == 3 ) {

		if( Install(pList[1], pList[2]) ) {

			return 0;
			}

		return 1;
		}

	return 2;
	}

static BOOL Install(WCHAR const *pInf, WCHAR const *pID)
{
	GUID  guid;

	WCHAR name[32] = {0};

	SetupDiGetINFClass( pInf, 
			    &guid,
			    name,
			    sizeof(name),
			    0
			    );

	if( name[0] ) {

		HDEVINFO hInfo = SetupDiCreateDeviceInfoList(&guid, 0);

		if( hInfo != INVALID_HANDLE_VALUE ) {

			SP_DEVINFO_DATA	Data;

			Data.cbSize = sizeof(Data);

			SetupDiCreateDeviceInfo( hInfo, 
						 name, 
						 &guid,
						 NULL,
						 0,
						 DICD_GENERATE_ID,
						 &Data
						 );

			if( Data.DevInst ) {

				SetupDiSetDeviceRegistryPropertyW( hInfo, 
								   &Data, 
								   SPDRP_HARDWAREID, 
								   PBYTE(pID),
								   lstrlen(pID) * 2
								   );

				BOOL fInstall = SetupDiCallClassInstaller( DIF_REGISTERDEVICE,
									   hInfo,
									   &Data
									   );

				if( fInstall ) {

					BOOL fUpdate = UpdateDriverForPlugAndPlayDevices( 0,
											  pID,
											  pInf,
											  INSTALLFLAG_FORCE,
											  NULL
											  );

					if( fUpdate ) {

						return TRUE;
						}
					}

				DWORD e = GetLastError();
				
				SetupDiCallClassInstaller( DIF_REMOVE,
							   hInfo,
							   &Data
							   );

				SetLastError(e);
				}

			DWORD e = GetLastError();
			
			SetupDiDestroyDeviceInfoList(hInfo);

			SetLastError(e);
			}
		}
	
	return FALSE;
	}

// End of File
