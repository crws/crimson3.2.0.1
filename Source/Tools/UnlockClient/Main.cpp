
#include "Intern.hpp"

// Prototypes

static void Error(char const *text, ...);

// Code

int main(int nArg, char const *pArg[])
{
	if( nArg == 5 ) {

		// Arguments are:
		//
		//	pArg[1] Device Key File
		//	pArg[2] Required Group
		//	pArg[3] Server IP Address
		//	pArg[4] Unlock File
		//
		// Device Key File may be augmented with the four passwords
		// generated during the provisioning process, in which case
		// some of the passwords will be saved in the server log.

		HANDLE hRead = CreateFile(pArg[1],
					  GENERIC_READ,
					  FILE_SHARE_READ | FILE_SHARE_WRITE,
					  NULL,
					  OPEN_EXISTING,
					  0,
					  NULL
		);

		if( hRead != INVALID_HANDLE_VALUE ) {

			UINT  size = GetFileSize(hRead, NULL);

			DWORD read = 0;

			vector<char> tx(size, 0);

			ReadFile(hRead, tx.data(), tx.size(), &read, NULL);

			CloseHandle(hRead);

			if( read == size ) {

				tx.insert(tx.begin(), '\n');

				tx.insert(tx.begin(), '0' + atoi(pArg[2]));

				WSADATA Data;

				WSAStartup(MAKEWORD(2, 2), &Data);

				SOCKET s = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);

				if( s > 0 ) {

					sockaddr_in addr = { 0 };

					addr.sin_family = AF_INET;

					addr.sin_port   = htons(7777);

					addr.sin_addr.S_un.S_addr = inet_addr(pArg[3]);

					if( connect(s, (sockaddr *) &addr, sizeof(addr)) >= 0 ) {

						if( send(s, tx.data(), tx.size(), 0) == tx.size() ) {

							shutdown(s, SD_SEND);

							vector<char> rx(16384, 0);

							int n = recv(s, rx.data(), rx.size(), MSG_WAITALL);

							if( n > 0 ) {

								HANDLE hWrite = CreateFile(pArg[4],
											   GENERIC_WRITE,
											   0,
											   NULL,
											   CREATE_ALWAYS,
											   0,
											   NULL
								);

								if( hWrite != INVALID_HANDLE_VALUE ) {

									DWORD wrote = 0;

									WriteFile(hWrite, rx.data(), n, &wrote, NULL);

									CloseHandle(hWrite);

									if( wrote == n ) {

										return 0;
									}

									Error("unable to write to file");
								}

								Error("unable to create open file");
							}
						}
					}

					Error("unable to communicate with server");
				}

				Error("unable to create socket");
			}

			Error("unable to read from file");
		}

		Error("unable to open file");
	}

	Error("invalid command line");

	return 0;
}

static void Error(char const *text, ...)
{
	va_list args;

	va_start(args, text);

	printf("UnlockClient: ");

	vprintf(text, args);

	printf(" (%u)\n", GetLastError());

	va_end(args);

	exit(1);
}

// End of File
