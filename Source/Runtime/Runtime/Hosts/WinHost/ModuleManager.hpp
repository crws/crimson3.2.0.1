
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ModuleManager_HPP

#define INCLUDE_ModuleManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Types
//

using win32::HINSTANCE;

//////////////////////////////////////////////////////////////////////////
//
// Module Manager
//

class CModuleManager : public IModuleManager
{
	public:
		// Constructor
		CModuleManager(void);

		// Destructor
		~CModuleManager(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IModuleManager
		bool METHOD LoadClientModule(PCTXT pName);
		bool METHOD LoadClientModule(UINT &hModule, PCTXT pName);
		bool METHOD LoadClientModule(UINT &hModule, PCTXT pName, PCBYTE pData, UINT uSize);
		bool METHOD FreeClientModule(UINT hModule);
		bool METHOD FreeAllModules(void);

	protected:
		// Modules Calls
		typedef void (*MAINPTR)(IObjectBroker *);
		typedef void (*EXITPTR)(void);

		// Module Structure
		struct CModule
		{
			CString   m_Name;
			HINSTANCE m_hLib;
			MAINPTR   m_pfnMain;
			EXITPTR   m_pfnExit;
			};

		// Module List
		typedef CList <CModule *> CModuleList;

		// Data Members
		ULONG	    m_uRefs;
		CString     m_Path;
		CString     m_Temp;
		CModuleList m_List;

		// Implementation
		void FindHostPath(void);
		void FindTempPath(void);
	};

// End of File

#endif
