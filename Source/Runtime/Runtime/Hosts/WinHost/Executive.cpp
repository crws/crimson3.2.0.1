
#include "Intern.hpp"

#include "Executive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Object
//

// Instantiator

IUnknown * Create_Executive(void)
{
	return (IExecutive *) New CExecutive;
	}

// IUnknown

HRESULT CExecutive::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IWaitMultiple);

	return CMosExecutive::QueryInterface(riid, ppObject);
	}

ULONG CExecutive::AddRef(void)
{
	return CMosExecutive::AddRef();
	}

ULONG CExecutive::Release(void)
{
	return CMosExecutive::Release();
	}

// IExecutive

IThread * CExecutive::GetCurrentThread(void)
{
	return CExecThread::m_pThread;
	}

UINT CExecutive::GetCurrentIndex(void)
{
	return CExecThread::m_pThread->GetIndex();
	}

void CExecutive::Sleep(UINT uTime)
{
	CheckThreadCancellation();

	DWORD t1 = GetTickCount();

	for(;;) {

		DWORD rc = win32::SleepEx(uTime, TRUE);

		DWORD t2 = GetTickCount();

		DWORD dt = t2 - t1;

		AddToSlept(dt);

		if( rc == WAIT_IO_COMPLETION ) {

			CheckThreadCancellation();

			if( uTime == FOREVER ) {

				continue;
				}

			if( uTime > dt ) {

				uTime -= dt;

				continue;
				}
			}

		return;
		}
	}

BOOL CExecutive::ForceSleep(UINT uTime)
{
	UINT uSlept = CExecThread::m_pThread->GetSlept();

	if( uSlept < uTime ) {

		Sleep(uTime - uSlept);

		CExecThread::m_pThread->GetSlept();

		return TRUE;
		}

	return FALSE;
	}

UINT CExecutive::GetTickCount(void)
{
	return win32::GetTickCount();
	}

// IWaitMultiple

UINT CExecutive::WaitMultiple(IWaitable **pList, UINT uCount, UINT uTime)
{
	HANDLE *pHand = New HANDLE [ uCount ];

	for( UINT n = 0; n < uCount; n++ ) {

		pHand[n] = HANDLE(pList[n]->GetWaitable());

		AfxAssert(pHand[n]);
		}

	UINT uCode = win32::WaitForMultipleObjectsEx( uCount,
						      pHand,
						      FALSE,
						      uTime,
						      TRUE
						      );

	if( uCode >= WAIT_OBJECT_0 && uCode < WAIT_OBJECT_0 + uCount ) {

		uCode -= WAIT_OBJECT_0;

		uCode += 1;

		delete [] pHand;

		return uCode;
		}

	delete [] pHand;

	return 0;
	}

UINT CExecutive::WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, UINT uTime)
{
	IWaitable *pWait[] = { pWait1, pWait2 };

	return WaitMultiple(pWait, elements(pWait), uTime);
	}

UINT CExecutive::WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, IWaitable *pWait3, UINT uTime)
{
	IWaitable *pWait[] = { pWait1, pWait2, pWait3 };

	return WaitMultiple(pWait, elements(pWait), uTime);
	}

// Overridables

void CExecutive::AllocThreadObject(CMosExecThread * &pThread)
{
	pThread = New CExecThread(this);
	}

// End of File
