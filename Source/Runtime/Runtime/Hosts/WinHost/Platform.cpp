
#include "Intern.hpp"

#include "Platform.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Filing System Options
//

#define USE_RLOS 0

//////////////////////////////////////////////////////////////////////////
//
// Network API
//

AfxNamespaceBegin(win32);

#include <ws2def.h> 

#include <ws2ipdef.h>

#include <Iphlpapi.h>

AfxNamespaceEnd(win32);

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

extern void Register_HxFiling(void);

extern void Register_HxTcpIp(void);

extern void Revoke_HxFiling(void);

extern void Revoke_HxTcpIp(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extensions Libraries
//

#if defined(AEON_COMP_MSVC)

#pragma comment(lib, "hxtcpip.lib")

#pragma comment(lib, "hxfiling.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice * Create_InputQueue(void);
extern IDevice * Create_Beeper(void);
extern IDevice * Create_Display(void);
extern IDevice * Create_Entropy(void);
extern IDevice * Create_Serial(UINT);
extern IDevice * Create_Pipe(UINT);
extern IDevice * Create_PCapNic(UINT);
extern IDevice * Create_NandMemory(void);
extern IDevice * Create_SerialMemory(void);
extern IDevice * Create_SdHost(void);
extern IDevice * Create_TimeZone(void);

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern void Register_NativeTcpSocket(void);
extern void Register_NativeRawSocket(void);
extern void Register_NativeDnsResolver(void);
extern void Register_PosixFileCopier(void);

extern void Revoke_NativeTcpSocket(void);
extern void Revoke_NativeRawSocket(void);
extern void Revoke_NativeDnsResolver(void);
extern void Revoke_PosixFileCopier(void);

//////////////////////////////////////////////////////////////////////////
//
// File Support Hook
//

// Externals

extern IUnknown * Create_WinFileSupport(void);

extern IUnknown * Create_RlosFileSupport(void);

// Instantiator

IUnknown * Create_FileSupport(void)
{
	#if USE_RLOS

	return Create_RlosFileSupport();

	#else

	return Create_WinFileSupport();

	#endif
}

//////////////////////////////////////////////////////////////////////////
//
// Platform Object
//

// Instantiator

IDevice * Create_Platform(void)
{
	return New CPlatform;
}

// Constructor

CPlatform::CPlatform(void)
{
	m_pDiskMan = NULL;

	m_pBlkDev  = NULL;

	m_pSdHost  = NULL;

	m_iDriveC  = NOTHING;
}

// Destructor

CPlatform::~CPlatform(void)
{
	SetWebAddr(NULL);

	Revoke_PosixFileCopier();

	#if USE_RLOS

	FreeFilingSystem();

	Revoke_HxFiling();

	#endif

	if( g_Config.m_fEmulate ) {

		Revoke_HxTcpIp();
	}
	else {
		Revoke_NativeTcpSocket();

//		Revoke_NativeUdpSocket();

		Revoke_NativeRawSocket();
	}

	Revoke_NativeDnsResolver();

	piob->RevokeGroup("dev.");
}

// IDevice

BOOL CPlatform::Open(void)
{
	if( TRUE ) {

		MakeDevice("dev.input-d", 1, Create_InputQueue);

		MakeDevice("dev.entropy", 1, Create_Entropy);

		MakeDevice("dev.nand", 1, Create_NandMemory);

		MakeDevice("dev.fram", 1, Create_SerialMemory);

		MakeDevice("dev.tz", 1, Create_TimeZone);

		#if USE_RLOS

		MakeDevice("dev.sdhost", 1, Create_SdHost);

		#endif

		RegisterBaseCommon();

		MakeDeviceType("dev.uart", 1, Create_Serial);

		MakeDeviceType("dev.pipe", 1, Create_Pipe);
	}

	if( !g_Config.m_fBlind ) {

		MakeDevice("dev.beeper", 1, Create_Beeper);

		MakeDisplay();
	}

	if( g_Config.m_fEmulate ) {

		MakeDeviceType("dev.nic", 1, Create_PCapNic);

		Register_HxTcpIp();
	}
	else {
		Register_NativeTcpSocket();

//		Register_NativeUdpSocket();

		Register_NativeRawSocket();

		Register_NativeDnsResolver();
	}

	#if USE_RLOS

	Register_HxFiling();

	BindFilingSystem();

	#endif

	Register_PosixFileCopier();

	FindSerial();

	return TRUE;
}

// IPlatform

PCTXT CPlatform::GetFamily(void)
{
	return g_Config.m_pModel->m_pFamily;
}

PCTXT CPlatform::GetModel(void)
{
	return g_Config.m_pModel->m_pModel;
}

PCTXT CPlatform::GetVariant(void)
{
	return g_Config.m_pModel->m_pVariant;
}

UINT CPlatform::GetFeatures(void)
{
	UINT uFeatures = g_Config.m_pModel->m_uFeatures;

	if( g_Config.m_fBlind ) {

		uFeatures &= ~rfDisplay;
	}

	return uFeatures;
}

BOOL CPlatform::IsService(void)
{
	return g_Config.m_fService;
}

BOOL CPlatform::IsEmulated(void)
{
	return g_Config.m_fEmulate;
}

BOOL CPlatform::HasMappings(void)
{
	return !g_Config.m_Map.IsEmpty();
}

void CPlatform::ResetSystem(void)
{
	win32::ExitProcess(666);
}

void CPlatform::SetWebAddr(PCTXT pAddr)
{
	char path[MAX_PATH];

	win32::GetTempPathA(MAX_PATH, path);

	strcat(path, "c32-ip");

	if( !g_Config.m_Ident.IsEmpty() ) {

		strcat(path, "-");

		strcat(path, g_Config.m_Ident);
	}

	if( pAddr ) {

		win32::HANDLE h = win32::CreateFileA(path, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);

		DWORD uDone = 0;

		win32::WriteFile(h, pAddr, strlen(pAddr), &uDone, NULL);

		win32::CloseHandle(h);

		return;
	}

	win32::DeleteFileA(path);
}

// Implementation

void CPlatform::MakeDisplay(void)
{
	IDevice *pDisplay = Create_Display();

	pDisplay->Open();

	pDisplay->AddRef();

	pDisplay->AddRef();

	piob->RegisterSingleton("dev.display", 0, pDisplay);

	piob->RegisterSingleton("dev.touch", 0, pDisplay);

	piob->RegisterSingleton("dev.leds", 0, pDisplay);
}

void CPlatform::FindSerial(void)
{
	CMacAddr Mac;

	AfxGetAutoObject(pNic, "dev.nic", 0, INic);

	if( pNic ) {

		pNic->ReadMac(Mac);
	}
	else {
		win32::PIP_ADAPTER_ADDRESSES pData = 0;

		win32::DWORD                 uData = 0;

		win32::GetAdaptersAddresses(AF_INET, 0, 0, pData, &uData);

		pData = win32::PIP_ADAPTER_ADDRESSES(win32::HeapAlloc(win32::GetProcessHeap(), 0, uData));

		win32::GetAdaptersAddresses(AF_INET, 0, 0, pData, &uData);

		win32::PIP_ADAPTER_ADDRESSES pWalk = pData;

		while( pWalk ) {

			if( pWalk->FirstUnicastAddress ) {

				PSTR pAddr = pWalk->FirstUnicastAddress->Address.lpSockaddr->sa_data;

				CIpAddr Ip = (DWORD &) pAddr[sizeof(WORD)];

				if( Ip != IP_LOOP ) {

					if( pWalk->PhysicalAddressLength == 6 ) {

						Mac = (MACADDR &) pWalk->PhysicalAddress[0];

						break;
					}
				}
			}

			pWalk = pWalk->Next;
		}

		win32::HeapFree(win32::GetProcessHeap(), 0, pData);
	}

	if( Mac.IsEmpty() ) {

		Mac.m_Addr[3] = BYTE(rand());
		Mac.m_Addr[4] = BYTE(rand());
		Mac.m_Addr[5] = BYTE(rand());
	}

	CPlatformBase::FindSerial(Mac);
}

bool CPlatform::BindFilingSystem(void)
{
	#if USE_RLOS

	AfxGetObject("diskman", 0, IDiskManager, m_pDiskMan);

	if( m_pDiskMan ) {

		AfxNewObject("sddrive", IBlockDevice, m_pBlkDev);

		if( m_pBlkDev ) {

			AfxGetObject("sdhost", 0, ISdHost, m_pSdHost);

			m_pBlkDev->Open(m_pSdHost);

			m_iDriveC = m_pDiskMan->RegisterDisk(m_pBlkDev);

			m_iHostC  = m_pDiskMan->RegisterHost(m_iDriveC);
		}

		return true;
	}

	#endif

	return false;
	}

void CPlatform::FreeFilingSystem(void)
{
	#if USE_RLOS

	if( m_pDiskMan ) {

		if( m_pBlkDev ) {

			m_pDiskMan->UnregisterHost(m_iDriveC, m_iHostC);
		}

		m_pDiskMan->Release();
	}

	#endif
	}

// End of File
