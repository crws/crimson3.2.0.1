
#include "Intern.hpp"

#include "TimeZone.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Time Zone Storage
//

// Instantiators

IDevice * Create_TimeZone(void)
{
	return New CTimeZone;
}

// Constructor

CTimeZone::CTimeZone(void)
{
	m_next   = 0;

	m_dst    = FALSE;

	m_offset = 0;

	StdSetRef();
}

// Destructor

CTimeZone::~CTimeZone(void)
{
}

// IUnknown

HRESULT CTimeZone::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ITimeZone);

	StdQueryInterface(ITimeZone);

	return E_NOINTERFACE;
}

ULONG CTimeZone::AddRef(void)
{
	StdAddRef();
}

ULONG CTimeZone::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CTimeZone::Open(void)
{
	win32::TIME_ZONE_INFORMATION tz = { 0 };

	if( win32::GetTimeZoneInformation(&tz) == TIME_ZONE_ID_DAYLIGHT ) {

		m_dst = true;
	}

	m_offset = -(tz.Bias - tz.StandardBias);

	return TRUE;
}

// ITimeZone

BOOL CTimeZone::GetDst(void)
{
	return m_dst;
}

INT CTimeZone::GetOffset(void)
{
	return m_offset;
}

time_t CTimeZone::GetNext(void)
{
	return m_next;
}

BOOL CTimeZone::Lock(DWORD magic)
{
	return TRUE;
}

void CTimeZone::SetDst(BOOL dst)
{
}

void CTimeZone::SetOffset(INT offset)
{
}

void CTimeZone::SetNext(time_t next)
{
}

// End of File
