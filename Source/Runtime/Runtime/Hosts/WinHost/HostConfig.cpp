
#include "Intern.hpp"

#include "HostConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "HostModel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Model Data
//

extern CHostModel modelG07;
extern CHostModel modelG09;
extern CHostModel modelG10;
extern CHostModel modelG10r;
extern CHostModel modelG12;
extern CHostModel modelG15;
extern CHostModel modelGC_0320_240;
extern CHostModel modelGC_0640_480;
extern CHostModel modelGC_0800_480;
extern CHostModel modelGC_1280_800;
extern CHostModel modelCA04;
extern CHostModel modelCA07;
extern CHostModel modelCA07eq;
extern CHostModel modelCA10;
extern CHostModel modelCA10ev;
extern CHostModel modelCA12;
extern CHostModel modelCA15;
extern CHostModel modelCO04;
extern CHostModel modelCO07;
extern CHostModel modelCO07eq;
extern CHostModel modelCO10;
extern CHostModel modelCO10ev;

//////////////////////////////////////////////////////////////////////////
//
// Host Configuration
//

// Instance

global CHostConfig g_Config;

// Constructor

CHostConfig::CHostConfig(void)
{
	m_fConsole = true;

	m_fBlind   = false;

	m_fEmulate = false;

	m_fFrame   = true;

	m_fService = false;

	m_Caption  = "Aeon Host";

	m_Service  = "AeonService";

	m_pModel   = NULL;
}

// Operations

BOOL CHostConfig::BindModel(void)
{
	if( m_fService ) {

		m_fBlind = TRUE;
	}

	if( m_Model.IsEmpty() ) {

		m_pModel = &modelG09;

		return TRUE;
	}

	static CHostModel * List[] = {

		&modelG07,
		&modelG09,
		&modelG10,
		&modelG10r,
		&modelG12,
		&modelG15,
		&modelGC_0320_240,
		&modelGC_0640_480,
		&modelGC_0800_480,
		&modelGC_1280_800,
		&modelCA04,
		&modelCA07,
		&modelCA07eq,
		&modelCA10,
		&modelCA10ev,
		&modelCA12,
		&modelCA15,
		&modelCO04,
		&modelCO07,
		&modelCO07eq,
		&modelCO10,

	};

	for( UINT p = 0; p < 2; p++ ) {

		CString Model(m_Model);

		if( m_Model.StartsWith("da50") || m_Model.StartsWith("da70") ) {

			Model = "gc" + Model.Mid(Model.Find('-'));
		}

		for( UINT n = 0; n < elements(List); n++ ) {

			BOOL h = FALSE;

			if( p == 0 ) {

				if( Model == List[n]->m_pName ) {

					h = TRUE;
				}
			}

			if( p == 1 ) {

				if( Model == CPrintf("gc-%4.4u-%3.3u", List[n]->m_cxImage, List[n]->m_cyImage) ) {

					h = TRUE;
				}
			}

			if( h ) {

				m_pModel = List[n];

				if( m_Model.StartsWith("da50") || m_Model.StartsWith("da70") ) {

					static char sName[128];

					static char sModel[128];

					strcpy(sName, m_Model.ToUpper());

					strcpy(sModel, m_Model.Left(m_Model.Find('-')).ToLower());

					List[n]->m_pFamily  = "FlexEdge(TM)";

					List[n]->m_pName    = sName;

					List[n]->m_pModel   = sModel;

					List[n]->m_pVariant = sModel;
				}

				return TRUE;
			}
		}
	}

	return FALSE;
}

// End of File
