
#include "Intern.hpp"

#include "NativeDnsResolver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Libraries
//

#pragma comment(lib, "Ws2_32.lib")

//////////////////////////////////////////////////////////////////////////
//
// DNS Resolver
//

// Registration

global void Register_NativeDnsResolver(void)
{
	piob->RegisterSingleton("net.dns", 0, New CNativeDnsResolver);
	}

global void Revoke_NativeDnsResolver(void)
{
	piob->RevokeSingleton("net.dns", 0);
	}

// Constructor

CNativeDnsResolver::CNativeDnsResolver(void)
{
	StdSetRef();
	}

// Destructor

CNativeDnsResolver::~CNativeDnsResolver(void)
{
	}

// IUnknown

HRESULT CNativeDnsResolver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDnsResolver);

	StdQueryInterface(IDnsResolver);

	return E_NOINTERFACE;
	}

ULONG CNativeDnsResolver::AddRef(void)
{
	StdAddRef();
	}

ULONG CNativeDnsResolver::Release(void)
{
	StdRelease();
	}

// IDnsResolver

CIpAddr CNativeDnsResolver::Resolve(PCTXT pName)
{
	CArray <CIpAddr> List;

	if( Resolve(List, pName) ) {

		return List[rand() % List.GetCount()];
		}

	return IP_EMPTY;
	}

BOOL CNativeDnsResolver::Resolve(CArray <CIpAddr> &List, PCTXT pName)
{
	if( pName && *pName ) {

		CIpAddr Ip = pName;

		if( Ip.IsEmpty() ) {

			win32::addrinfo *pData = NULL;

			if( !win32::GetAddrInfoA(pName, "http", NULL, &pData) ) {

				win32::addrinfo *pWalk = pData;

				while( pWalk ) {

					if( pWalk->ai_family == AF_INET ) {

						CIpAddr const *pAddr = (CIpAddr const *) (pWalk->ai_addr->sa_data + 2);

						List.Append(*pAddr);
						}

					pWalk = pWalk->ai_next;
					}

				win32::FreeAddrInfoA(pData);

				return !List.IsEmpty();
				}

			return FALSE;
			}

		List.Append(Ip);

		return TRUE;
		}

	return FALSE;
	}

// End of File
