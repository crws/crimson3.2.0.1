
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LibC Implementation
//

clink void abort(void)
{
	// TODO -- What should this do?

	for(;;);
	}

clink int raise(int)
{	
	// TODO -- What should this do?

	return 0;
	}

clink struct passwd * getpwuid(uid_t uid)
{
	return NULL;
	}

clink uid_t getuid(void)
{
	return 0;
	}

clink struct passwd * getpwnam(char const *name)
{
	return NULL;
	}

// End of File
