
#include "Intern.hpp"

#include "ManualEvent.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Manual Event Object
//

// Instantiator

static IUnknown * Create_ManualEvent(PCTXT pName)
{
	return (IEvent *) New CManualEvent;
	}

// Registration

global void Register_ManualEvent(void)
{
	piob->RegisterInstantiator("exec.event-m", Create_ManualEvent);
	}

// Constructor

CManualEvent::CManualEvent(void)
{
	m_hSync = win32::CreateEvent(NULL, TRUE, FALSE, NULL);
	}

// IUnknown

HRESULT CManualEvent::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IEvent);

	return CWaitable::QueryInterface(riid, ppObject);
	}

ULONG CManualEvent::AddRef(void)
{
	return CWaitable::AddRef();
	}

ULONG CManualEvent::Release(void)
{
	return CWaitable::Release();
	}

// IWaitable

PVOID CManualEvent::GetWaitable(void)
{
	return CWaitable::GetWaitable();
	}

BOOL CManualEvent::Wait(UINT uWait)
{
	return CWaitable::Wait(uWait, TRUE);
	}

BOOL CManualEvent::HasRequest(void)
{
	return CWaitable::HasRequest();
	}

// IEvent

void CManualEvent::Set(void)
{
	win32::SetEvent(m_hSync);
	}

void CManualEvent::Clear(void)
{
	win32::ResetEvent(m_hSync);
	}

// End of File
