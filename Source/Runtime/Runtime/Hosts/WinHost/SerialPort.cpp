
#include "Intern.hpp"

#include "SerialPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Serial Port
//

// Instantiator

IDevice * Create_Serial(UINT uInst)
{
	if( uInst < 3 ) {

		CPrintf Port   = CPrintf("com%u", uInst);

		CString Target = g_Config.m_Map[Port];

		CString Name   = CPrintf("COM%u", atoi(Target));

		return New CSerialPort(Name);
	}

	return NULL;
}

// Constructor

CSerialPort::CSerialPort(CString Name) : CSerialBase(Name)
{
}

// Handlers

BOOL CSerialPort::OnError(void)
{
	win32::COMSTAT cs;

	for(;;) {

		DWORD dwFlags = 0;

		win32::ClearCommError(m_hPort, &dwFlags, &cs);

		if( !dwFlags ) {

			break;
		}
	}

	return TRUE;
}

// Implementation

BOOL CSerialPort::PortOpen(void)
{
	win32::WCHAR sName[64] = { 0 };

	wstrcat(sName, "\\\\.\\");

	wstrcat(sName, m_Name);

	m_hPort = win32::CreateFile( sName,
				     GENERIC_READ | GENERIC_WRITE,
				     0,
				     NULL,
				     OPEN_EXISTING,
				     FILE_FLAG_OVERLAPPED,
				     NULL
				     );

	if( m_hPort != INVALID_HANDLE_VALUE ) {

		if( PortSetFormat() ) {

			PortSetTimeouts();

			PortSetBuffers();

			return TRUE;
		}

		PortClose();

		return FALSE;
	}

	return FALSE;
}

BOOL CSerialPort::PortSetFormat(void)
{
	win32::DCB Ctrl;

	memset(&Ctrl, 0, sizeof(Ctrl));

	Ctrl.DCBlength = sizeof(win32::DCB);

	GetCommState(m_hPort, &Ctrl);

	Ctrl.BaudRate        = PortFindBaud();

	Ctrl.ByteSize	     = BYTE(m_Config.m_uDataBits);

	Ctrl.StopBits	     = PortFindStop();

	Ctrl.fParity         = (m_Config.m_uParity == parityNone) ? FALSE : TRUE;

	Ctrl.Parity          = PortFindParity();

	Ctrl.fBinary	     = TRUE;

	Ctrl.fOutxCtsFlow    = (m_Config.m_uFlags & flagHonorCTS) ? TRUE : FALSE;

	Ctrl.fOutxDsrFlow    = FALSE;

	Ctrl.fDtrControl     = DTR_CONTROL_ENABLE;

	Ctrl.fRtsControl     = RTS_CONTROL_TOGGLE;

	Ctrl.fDsrSensitivity = FALSE;

	Ctrl.fOutX	     = FALSE;

	Ctrl.fInX	     = FALSE;

	Ctrl.fNull	     = FALSE;

	return SetCommState(m_hPort, &Ctrl);
}

UINT CSerialPort::PortFindBaud(void)
{
	switch( m_Config.m_uBaudRate ) {

		case    110: return    CBR_110;
		case    300: return    CBR_300;
		case    600: return    CBR_600;
		case   1200: return   CBR_1200;
		case   2400: return   CBR_2400;
		case   4800: return   CBR_4800;
		case   9600: return   CBR_9600;
		case  14400: return  CBR_14400;
		case  19200: return  CBR_19200;
		case  38400: return  CBR_38400;
		case  56000: return  CBR_56000;
		case  57600: return  CBR_57600;
		case 115200: return CBR_115200;
		case 128000: return CBR_128000;
		case 256000: return CBR_256000;
	}

	return m_Config.m_uBaudRate;
}

BYTE CSerialPort::PortFindParity(void)
{
	switch( m_Config.m_uParity ) {

		case parityNone: return NOPARITY;
		case parityEven: return EVENPARITY;
		case parityOdd : return ODDPARITY;
	}

	return 0;
}

BYTE CSerialPort::PortFindStop(void)
{
	switch( m_Config.m_uStopBits ) {

		case 1: return ONESTOPBIT;
		case 2: return TWOSTOPBITS;
	}

	return 0;
}

void CSerialPort::PortSetTimeouts(void)
{
	win32::COMMTIMEOUTS TimeOut;

	TimeOut.ReadIntervalTimeout         = 1;

	TimeOut.ReadTotalTimeoutMultiplier  = 0;

	TimeOut.ReadTotalTimeoutConstant    = 0;

	TimeOut.WriteTotalTimeoutMultiplier = 0;

	TimeOut.WriteTotalTimeoutConstant   = 0;

	win32::SetCommTimeouts(m_hPort, &TimeOut);
}

void CSerialPort::PortSetBuffers(void)
{
	win32::SetupComm(m_hPort, 8192, 8192);
}

// End of File
