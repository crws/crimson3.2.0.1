
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PCapNic_HPP

#define INCLUDE_PCapNic_HPP

//////////////////////////////////////////////////////////////////////////
//
// Window APIs
//

AfxNamespaceBegin(win32);

#include <ws2tcpip.h>

#include <iphlpapi.h>

AfxNamespaceEnd(win32);

//////////////////////////////////////////////////////////////////////////
//
// PCAP API
//

#include "pcap/include/pcap.h"

//////////////////////////////////////////////////////////////////////////
//
// PCAP-Based NIC Driver
//

class CPCapNic : public INic, public IClientProcess, public IDiagProvider
{
	public:
		// Constructor
		CPCapNic(UINT uInst, UINT uFace);

		// Destructor
		~CPCapNic(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// INic
		bool METHOD Open(bool fFast, bool fFull);
		bool METHOD Close(void);
		bool METHOD InitMac(MACADDR const &Addr);
		void METHOD ReadMac(MACADDR &Addr);
		UINT METHOD GetCapabilities(void);
		void METHOD SetFlags(UINT uFlags);
		bool METHOD SetMulticast(MACADDR const *pList, UINT uList);
		bool METHOD IsLinkActive(void);
		bool METHOD WaitLink(UINT uTime);
		bool METHOD SendData(CBuffer *  pBuff, UINT uTime);
		bool METHOD ReadData(CBuffer * &pBuff, UINT uTime);
		void METHOD GetCounters(NICDIAG &Diag);
		void METHOD ResetCounters(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Interface Location
		static UINT FindInterfaces(void);

	protected:
		// Interface Data
		struct CFace
		{
			CString	         m_Adapter;
			CMacAddr	 m_MacAddr;
			CArray <CIpAddr> m_IpAddrs;
			};

		// Interface List
		static CArray <CFace *> m_Faces;

		// Diagnostic Data
		IDiagManager * m_pDiag;
		UINT           m_uProv;

		// Data Members
		ULONG        m_uRefs;
		UINT	     m_uInst;
		UINT         m_uFace;
		BOOL	     m_fOnline;
		CFace      * m_pFace;
		IMutex     * m_pLock;
		ISemaphore * m_pFlag;
		CMacAddr     m_Mac;
		pcap_t     * m_hPort;
		HTHREAD      m_hThread;
		char	     m_sError[256];
		UINT         m_uRxLimit;
		UINT         m_uRxCount;
		CBuffer    * m_pRxHead;
		CBuffer    * m_pRxTail;
		UINT         m_uMulti;
		CMacAddr   * m_pMulti;

		// Implementation
		bool LoadFilter(pcap_t *hPort, BOOL fMulti);
		void OnPacket(PCBYTE pData, UINT uData);
		bool FindFace(void);
		void FlipMac(void);

		// Diagnostics
		BOOL DiagRegister(void);
		BOOL DiagRevoke(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagConnect(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagDisconnect(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Packet Handler
		static void PacketProc(PBYTE pParam, pcap_pkthdr const *pInfo, PCBYTE pData);
	};

// End of File

#endif
