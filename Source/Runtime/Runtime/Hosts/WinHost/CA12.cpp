
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Canyon CA12 Model Data
//

static BYTE imageCA12[] = {

	#include "ca12-x1.png.dat"
	0
	};

global CHostModel modelCA12 = {

	"",
	"CA12",
	"CR3000-12",
	"CR3000-12000",
	rfCanyon,
	1,
	1432,
	896,
	76,
	88,
	1280,
	720,
	0,
	NULL,
	sizeof(imageCA12)-1,
	imageCA12
	};

// End of File
