
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite Edge Controller 800x480 Model Data
//

static BYTE imageGC_0800_480[] = {

	#include "g09-x1.png.dat"
	0
};

static CHostKey keysGC_0800_480[] = {

	{ 344, 634, 404, 694, 128 },
	{ 464, 634, 524, 694, 129 },
	{ 584, 634, 644, 694, 162 }

};

global CHostModel modelGC_0800_480 = {

	"Graphite(R)",
	"GC-0800-480",
	"gc",
	"gc",
	rfGraphite,
	1,
	988,
	758,
	94,
	124,
	800,
	480,
	elements(keysGC_0800_480),
	keysGC_0800_480,
	sizeof(imageGC_0800_480)-1,
	imageGC_0800_480
};

// End of File
