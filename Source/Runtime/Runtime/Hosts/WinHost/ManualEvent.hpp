
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ManualEvent_HPP

#define INCLUDE_ManualEvent_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Waitable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Manual Event Object
//

class CManualEvent : public CWaitable, public IEvent
{
	public:
		// Constructor
		CManualEvent(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IWaitable
		PVOID METHOD GetWaitable(void);
		BOOL  METHOD Wait(UINT uTime);
		BOOL  METHOD HasRequest(void);

		// IEvent
		void METHOD Set(void);
		void METHOD Clear(void);
	};

// End of File

#endif
