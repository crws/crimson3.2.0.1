
#include "Intern.hpp"

#include "GdiPlus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// GDI+ Token
//

static ULONG gdipToken;

//////////////////////////////////////////////////////////////////////////
//
// Init and Term Hooks
//

global void GdiPlusInit(void)
{
	win32::GdiplusStartupOutput Output;

	win32::GdiplusStartupInput  Input;

	memset(&Input, 0, sizeof(Input));

	Input.GdiplusVersion = 1;

	win32::GdiplusStartup(&gdipToken, &Input, &Output);
	}

global void GdiPlusTerm(void)
{
	win32::GdiplusShutdown(gdipToken);
	}

// End of File
