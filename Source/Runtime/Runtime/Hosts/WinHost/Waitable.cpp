
#include "Intern.hpp"

#include "Waitable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic Waitable Object
//

// Constructor

CWaitable::CWaitable(void)
{
	StdSetRef();

	m_uWait = 0;
	}

// Destructor

CWaitable::~CWaitable(void)
{
//	AfxAssert(!m_uWait);

	win32::CloseHandle(m_hSync);
	}

// IUnknown

HRESULT CWaitable::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IWaitable);

	StdQueryInterface(IWaitable);

	return E_NOINTERFACE;
	}

ULONG CWaitable::AddRef(void)
{
	StdAddRef();
	}

ULONG CWaitable::Release(void)
{
	StdRelease();
	}

// IWaitable

PVOID CWaitable::GetWaitable(void)
{
	return m_hSync;
	}

BOOL CWaitable::Wait(UINT uWait, BOOL fAlert)
{
	if( fAlert ) {

		CheckThreadCancellation();
		}

	for(;;) {

		AtomicIncrement(&m_uWait);
	
		DWORD t1 = GetTickCount();

		DWORD rc = win32::WaitForSingleObjectEx(m_hSync, uWait, fAlert);

		AtomicDecrement(&m_uWait);

		DWORD t2 = GetTickCount();

		DWORD dt = t2 - t1;

		AddToSlept(dt);

		if( rc == WAIT_IO_COMPLETION ) {

			if( fAlert ) {

				CheckThreadCancellation();
				}

			if( uWait == FOREVER ) {

				continue;
				}

			if( uWait > dt ) {

				uWait -= dt;

				continue;
				}
			}

		return rc == WAIT_OBJECT_0;
		}
	}

BOOL CWaitable::HasRequest(void)
{
	return m_uWait > 0;
	}

// End of File
