
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Mutex_HPP

#define INCLUDE_Mutex_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Waitable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CExecThread;

//////////////////////////////////////////////////////////////////////////
//
// Mutex Object
//

class CMutex : public CWaitable, public IMutex
{
	public:
		// Constructor
		CMutex(void);

		// Destructor
		~CMutex(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IWaitable
		PVOID METHOD GetWaitable(void);
		BOOL  METHOD Wait(UINT uTime);
		BOOL  METHOD HasRequest(void);

		// IMutex
		void METHOD Free(void);

		// Task Teardown
		static void FreeMutexList(CExecThread *pThread);

	protected:
		// Data Members
		UINT	      m_uCount;
		CExecThread * m_pOwner;
		CMutex      * m_pNext;
		CMutex      * m_pPrev;

		// Implmentation
		void ListAppend(void);
		void ListRemove(void);
	};

// End of File

#endif
