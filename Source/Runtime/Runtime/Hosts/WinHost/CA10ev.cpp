
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Canyon CA10ev Model Data
//

static BYTE imageCA10ev[] = {

	#include "ca10ev-x1.png.dat"
	0
	};

global CHostModel modelCA10ev = {

	"",
	"CA10EV",
	"CR3000-10",
	"CR3000-10000",
	rfCanyon,
	1,
	792,
	704,
	76,
	100,
	640,
	480,
	0,
	NULL,
	sizeof(imageCA10ev)-1,
	imageCA10ev
	};

// End of File
