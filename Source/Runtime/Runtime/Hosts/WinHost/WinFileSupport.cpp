]
#include "Intern.hpp"

#include "WinFileSupport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File System Support
//

// Instantiator

IUnknown * Create_WinFileSupport(void)
{
	return (IFileSupport *) New CWinFileSupport;
}

// Static Data

win32::FILETIME CWinFileSupport::btime = { 0 };

// Constructor

CWinFileSupport::CWinFileSupport(void)
{
	m_HostSep  = '\\';

	m_HostBase = FindPath() + CString(g_Config.m_pModel->m_pName).ToLower() + m_HostSep;

	m_HostRoot = m_HostBase + "FS\\";

	MkDir("C:\\");
}

// IUnknown

HRESULT CWinFileSupport::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IFileUtilities);

	return CBaseFileSupport::QueryInterface(riid, ppObject);
}

ULONG CWinFileSupport::AddRef(void)
{
	return CBaseFileSupport::AddRef();
}

ULONG CWinFileSupport::Release(void)
{
	return CBaseFileSupport::Release();
}

// IFileUtilities

BOOL CWinFileSupport::IsDiskMounted(char cDrive)
{
	return TRUE;
}

UINT CWinFileSupport::GetDiskStatus(char cDrive)
{
	return 5;
}

DWORD CWinFileSupport::GetDiskIdent(char cDrive)
{
	return MAKELONG(MAKEWORD(cDrive, cDrive), MAKEWORD(cDrive, cDrive));
}

UINT64 CWinFileSupport::GetDiskSize(char cDrive)
{
	return 1024 * 1024 * 1024;
}

UINT64 CWinFileSupport::GetDiskFree(char cDrive)
{
	return 512 * 1024 * 1024;
}

BOOL CWinFileSupport::FormatDisk(char cDrive)
{
	if( isalpha(cDrive) ) {

		EmptyDirectory(m_HostRoot + char(toupper(cDrive)) + '\\');

		return TRUE;
	}

	return FALSE;
}

BOOL CWinFileSupport::EjectDisk(char cDrive)
{
	return TRUE;
}

// Host Hooks

bool CWinFileSupport::HostRename(CString const &From, CString const &To)
{
	CString Full(From.Left(From.FindRev(m_HostSep) + 1) + To);

	return win32::MoveFileA(From, Full) ? true : false;
}

bool CWinFileSupport::HostUnlink(CString const &Path)
{
	return win32::DeleteFileA(Path) ? true : false;
}

bool CWinFileSupport::HostStat(CString const &Path, struct stat *buffer)
{
	for( UINT p = 0; p < 2; p++ ) {

		HANDLE hd = win32::CreateFileA(Path,
					       GENERIC_READ,
					       FILE_SHARE_READ,
					       NULL,
					       OPEN_EXISTING,
					       p ? FILE_FLAG_BACKUP_SEMANTICS : 0,
					       NULL
		);

		if( hd != INVALID_HANDLE_VALUE ) {

			HostGetInfo(hd, buffer);

			win32::CloseHandle(hd);

			return true;
		}
	}

	return false;
}

bool CWinFileSupport::HostUTime(CString const &Path, time_t time)
{
	HANDLE h  = win32::CreateFileA(Path,
				       GENERIC_READ | GENERIC_WRITE,
				       FILE_SHARE_READ,
				       NULL,
				       OPEN_EXISTING,
				       0,
				       NULL
	);

	if( h != INVALID_HANDLE_VALUE ) {

		INT64 tb = btime.dwLowDateTime + (INT64(btime.dwHighDateTime) << 32);

		INT64 tt = tb + time * 10000000LL;

		win32::FILETIME ft;

		ft.dwHighDateTime = DWORD((tt >> 32) & 0xFFFFFFFF);

		ft.dwLowDateTime  = DWORD((tt >>  0) & 0xFFFFFFFF);

		if( win32::SetFileTime(h, NULL, &ft, &ft) ) {

			win32::CloseHandle(h);

			return true;
		}

		win32::CloseHandle(h);
	}

	return false;
}

bool CWinFileSupport::HostChMod(CString const &Path, mode_t mode)
{
	return true;
}

PVOID CWinFileSupport::HostOpen(CString const &Path, int oflag, int pmode)
{
	// TODO -- Are we getting all the modes here? !!!

	int cm = (oflag & O_CREAT) ? (oflag & O_TRUNC) ? CREATE_ALWAYS : OPEN_ALWAYS : OPEN_EXISTING;

	int wm = (oflag & (O_RDWR | O_WRONLY)) ? GENERIC_WRITE : 0;

	HANDLE h  = win32::CreateFileA(Path,
				       GENERIC_READ | wm,
				       FILE_SHARE_READ,
				       NULL,
				       cm,
				       0,
				       NULL
	);

	if( h != INVALID_HANDLE_VALUE ) {

		if( oflag & O_APPEND ) {

			win32::SetFilePointer(h, 0, NULL, FILE_END);
		}

		return PVOID(h);
	}

	return NULL;
}

void CWinFileSupport::HostClose(PVOID hFile)
{
	win32::CloseHandle(HANDLE(hFile));
}

bool CWinFileSupport::HostStat(PVOID hFile, struct stat *buffer)
{
	HostGetInfo(HANDLE(hFile), buffer);

	return true;
}

int CWinFileSupport::HostRead(PVOID hFile, void *buffer, unsigned int count)
{
	DWORD dwDone = 0;

	if( win32::ReadFile(HANDLE(hFile), buffer, count, &dwDone, NULL) ) {

		return dwDone;
	}

	return -1;
}

int CWinFileSupport::HostWrite(PVOID hFile, void const *buffer, unsigned int count)
{
	DWORD dwDone = 0;

	if( win32::WriteFile(HANDLE(hFile), buffer, count, &dwDone, NULL) ) {

		return dwDone;
	}

	return -1;
}

int CWinFileSupport::HostSeek(PVOID hFile, long offset, int origin)
{
	DWORD dwPos = win32::SetFilePointer(HANDLE(hFile), offset, 0, origin);

	if( dwPos != INVALID_SET_FILE_POINTER ) {

		return dwPos;
	}

	return -1;
}

bool CWinFileSupport::HostTruncate(PVOID hFile, DWORD size)
{
	DWORD dwPos = win32::SetFilePointer(HANDLE(hFile), 0, NULL, FILE_CURRENT);

	if( dwPos != INVALID_SET_FILE_POINTER ) {

		MakeMin(dwPos, size);

		if( win32::SetFilePointer(HANDLE(hFile), size, 0, 0) == size ) {

			if( win32::SetEndOfFile(HANDLE(hFile)) ) {

				if( win32::SetFilePointer(HANDLE(hFile), dwPos, 0, 0) == dwPos ) {

					return true;
				}
			}
		}
	}

	return false;

}

int CWinFileSupport::HostIoCtl(PVOID hFile, int func, void *data)
{
	return -1;
}

bool CWinFileSupport::HostIsValidDir(CString const &Path)
{
	return win32::SetCurrentDirectoryA(Path) ? true : false;
}

bool CWinFileSupport::HostRmDir(CString const &Path)
{
	return win32::RemoveDirectoryA(Path) ? true : false;
}

bool CWinFileSupport::HostMkDir(CString const &Path)
{
	if( !win32::CreateDirectoryA(Path, NULL) ) {

		return false;
	}

	return true;
}

int CWinFileSupport::HostScanDir(CString const &Path, struct dirent ***list, int (*selector)(struct dirent const *))
{
	int c = 0;

	for( int p = 0; p < 2; p++ ) {

		win32::WIN32_FIND_DATAA Data;

		win32::HANDLE hFind = win32::FindFirstFileA(Path + '*', &Data);

		if( hFind == INVALID_HANDLE_VALUE ) {

			return -1;
		}
		else {
			for( int n = 0;; ) {

				dirent d = { 0 };

				strcpy(d.d_name, Data.cFileName);

				if( Data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) {

					d.d_type = DT_DIR;
				}
				else
					d.d_type = DT_REG;

				if( !selector || selector(&d) ) {

					if( strcmp(d.d_name, "..") || !IsRoot(Path) ) {

						if( p == 1 && n < c ) {

							(*list)[n] = (dirent *) malloc(sizeof(dirent));

							memcpy((*list)[n], &d, sizeof(d));
						}

						n++;
					}
				}

				if( !win32::FindNextFileA(hFind, &Data) ) {

					c = n;

					win32::FindClose(hFind);

					break;
				}
			}

			if( p == 0 ) {

				if( !c )
					break;

				*list = (dirent **) calloc(c, sizeof(dirent *));
			}
		}
	}

	return c;
}

bool CWinFileSupport::HostSync(void)
{
	return true;
}

// Implementation

void CWinFileSupport::EmptyDirectory(CString const &Path)
{
	struct dirent **list;

	int    c;

	if( (c = HostScanDir(Path, &list, NULL)) > 0 ) {

		for( int p = 0; p < 2; p++ ) {

			for( int n = 0; n < c; n++ ) {

				struct dirent *file = list[n];

				if( file->d_type == DT_DIR ) {

					if( p == 1 ) {

						if( !strcmp(file->d_name, ".") || !strcmp(file->d_name, "..") ) {

							continue;
						}

						EmptyDirectory(Path + file->d_name + m_HostSep);

						HostRmDir(Path);
					}
				}
				else {
					if( p == 0 ) {

						HostUnlink(Path + file->d_name + m_HostSep);
					}
				}

				if( p == 1 ) {

					free(file);
				}
			}
		}

		free(list);
	}
}

void CWinFileSupport::HostGetInfo(HANDLE h, struct stat *buffer)
{
	InitTimeRef();

	win32::FILETIME ctime, mtime;

	win32::GetFileTime(h, &ctime, NULL, &mtime);

	INT64 tb = btime.dwLowDateTime + (INT64(btime.dwHighDateTime) << 32);
	INT64 tc = ctime.dwLowDateTime + (INT64(ctime.dwHighDateTime) << 32);
	INT64 tm = mtime.dwLowDateTime + (INT64(mtime.dwHighDateTime) << 32);

	DWORD dwSize = win32::GetFileSize(h, NULL);

	buffer->st_atime = 0;
	buffer->st_ctime = time_t((tc - tb) / 10000000LL);
	buffer->st_mtime = time_t((tm - tb) / 10000000LL);
	buffer->st_dev   = 0;
	buffer->st_rdev  = 0;
	buffer->st_nlink = 1;
	buffer->st_size  = dwSize;
	buffer->st_mode = _S_IREAD | _S_IWRITE;
}

bool CWinFileSupport::InitTimeRef(void)
{
	if( !btime.dwHighDateTime ) {

		// Find the base of the Unix epoch so we can
		// convert Windows FILETIMEs into Unix time.

		win32::SYSTEMTIME time = { 0 };

		time.wYear  = 1970;
		time.wMonth = 1;
		time.wDay   = 1;

		win32::SystemTimeToFileTime(&time, &btime);

		return true;
	}

	return false;
}

CString CWinFileSupport::FindPath(void)
{
	char Path[MAX_PATH] = { 0 };

	strcpy(Path, g_Config.m_EmData);

	Path[0] || win32::GetEnvironmentVariableA("AeonTemp", Path, sizeof(Path));

	Path[0] || win32::GetEnvironmentVariableA("Tmp", Path, sizeof(Path));

	Path[0] || win32::GetEnvironmentVariableA("Temp", Path, sizeof(Path));

	Path[0] || strcpy(Path, ".");

	UINT c = strlen(Path);

	if( Path[c-1] != '\\' ) {

		strcat(Path, "\\");
	}

	return Path;
}

// End of File
