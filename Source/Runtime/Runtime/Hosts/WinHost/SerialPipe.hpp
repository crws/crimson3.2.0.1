
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_SerialPipe_HPP

#define INCLUDE_SerialPipe_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SerialBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Serial Pipe
//

class CSerialPipe : public CSerialBase
{
public:
	// Constructor
	CSerialPipe(CString Name);

protected:
	// Data Members
	UINT m_uPipe;

	// Handlers
	BOOL OnConn(void);
	BOOL OnTest(void);
	void OnDone(void);
	void OnBreak(void);
	BOOL OnError(void);

	// Port
	BOOL PortOpen(void);
};

// End of File

#endif
