
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Malloc Implementation
//

// Imported Types

using win32::HANDLE;
using win32::HINSTANCE;
using win32::PSTR;

// Imported APIs

using win32::HeapCreate;
using win32::HeapDestroy;
using win32::HeapAlloc;
using win32::HeapReAlloc;
using win32::HeapFree;
using win32::HeapLock;
using win32::HeapUnlock;

// Static Data

static HANDLE m_hHeap = NULL;

// Code

global void Init_Malloc(void)
{
	m_hHeap = HeapCreate(0, 0, 0);
	}

global void Term_Malloc(void)
{
	HeapDestroy(m_hHeap);
	}

clink void * __malloc_r(struct _reent *ptr, UINT uSize)
{
	return HeapAlloc(m_hHeap, 0, uSize);
	}

clink void * __memalign_r(struct _reent *ptr, UINT uAlign, UINT uSize)
{
	// Should not be needed on Windows, as alignment is really
	// only an issue for low-level code. If this proves to be an
	// issue, please feel free to implement something!

	return __malloc_r(ptr, uSize);
	}

clink void * __calloc_r(struct _reent *ptr, UINT uSize, UINT uCount)
{
	return HeapAlloc(m_hHeap, HEAP_ZERO_MEMORY, uSize * uCount);
	}

clink void * __realloc_r(struct _reent *ptr, void *pData, UINT uSize)
{
	return pData ? HeapReAlloc(m_hHeap, 0, pData, uSize) : HeapAlloc(m_hHeap, 0, uSize);
	}

clink void __free_r(struct _reent *ptr, void *pData)
{
	HeapFree(m_hHeap, 0, pData);
	}

clink void __malloc_lock(void *)
{
	HeapLock(m_hHeap);
	}

clink void __malloc_unlock(void *)
{
	HeapUnlock(m_hHeap);
	}

// End of File
