
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite Edge Controller VGA Model Data
//

static BYTE imageGC_0320_240[] = {

	#include "g04-x2.png.dat"
	0
	};

static CHostKey keysGC_0320_240[] = {

	{ 264, 648, 328, 712, 128 },
	{ 392, 648, 456, 712, 129 },
	{ 520, 648, 584, 712, 162 }

	};

global CHostModel modelGC_0320_240 = {

	"Graphite(R)",
	"GC-0320-240",
	"gc",
	"gc",
	rfGraphite,
	2,
	848,
	784,
	104,
	136,
	320,
	240,
	elements(keysGC_0320_240),
	keysGC_0320_240,
	sizeof(imageGC_0320_240)-1,
	imageGC_0320_240
	};

// End of File
