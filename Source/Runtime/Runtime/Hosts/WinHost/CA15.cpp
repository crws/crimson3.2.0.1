
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Canyon CA15 Model Data
//

static BYTE imageCA15[] = {

	#include "ca15-x1.png.dat"
	0
	};

global CHostModel modelCA15 = {

	"",
	"CA15",
	"CR3000-15",
	"CR3000-15000",
	rfCanyon,
	1,
	1176,
	992,
	76,
	100,
	1024,
	768,
	0,
	NULL,
	sizeof(imageCA15)-1,
	imageCA15
	};

// End of File
