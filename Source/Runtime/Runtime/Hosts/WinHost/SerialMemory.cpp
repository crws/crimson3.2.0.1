
#include "Intern.hpp"

#include "SerialMemory.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Windows Emulated Nand Flash Manager
//

// Instantiator

IDevice * Create_SerialMemory(void)
{
	return (ISerialMemory *) New CSerialMemory;
	}

// Constructor

CSerialMemory::CSerialMemory(void)
{
	StdSetRef();

	m_uSize = 32768;

	m_Name  = "fram.data";
	}

// Destructor

CSerialMemory::~CSerialMemory(void)
{
	}

// IUnknown

HRESULT CSerialMemory::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ISerialMemory);

	return E_NOINTERFACE;
	}

ULONG CSerialMemory::AddRef(void)
{
	StdAddRef();
	}

ULONG CSerialMemory::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL CSerialMemory::Open(void)
{
	AllocData();

	return TRUE;
	}

// ISerialMemory

UINT CSerialMemory::GetSize(void)
{
	return m_uSize;
	}

BOOL CSerialMemory::IsFast(void)
{
	return TRUE;
	}

BOOL CSerialMemory::GetData(UINT uAddr, PBYTE  pData, UINT uCount)
{
	memcpy(pData, m_pData + uAddr, uCount);

	return TRUE;
	}

BOOL CSerialMemory::PutData(UINT uAddr, PCBYTE pData, UINT uCount)
{
	memcpy(m_pData + uAddr, pData, uCount);

	return TRUE;
	}

// End of File
