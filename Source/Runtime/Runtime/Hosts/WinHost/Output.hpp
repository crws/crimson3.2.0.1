
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Output_HPP

#define INCLUDE_Output_HPP

//////////////////////////////////////////////////////////////////////////
//
// Use Entire Win32 API
//

using namespace win32;

//////////////////////////////////////////////////////////////////////////
//
// Output Window Object
//

class COutput
{
	public:
		// Constructor
		COutput(void);

		// Destructor
		~COutput(void);

		// Operations
		void    Bind(HWND hWnd);
		void    SetFont(HFONT hFont);
		void    AddLine(CString Text, BOOL fSend);
		void    Clear(void);
		void    Sync(void);
		LRESULT WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam);

	protected:
		// Data Members
		HWND         m_hWnd;
		HANDLE	     m_hSema;
		CStringArray m_Text;
		int	     m_nWheel;
		int	     m_nTop;
		int          m_cx;
		int          m_cy;
		HFONT        m_hFont;
		int          m_xFont;
		int          m_yFont;
		int	     m_nPage;
		BOOL	     m_fSelect;
		BOOL	     m_fCapture;
		POINT	     m_Mouse;
		POINT	     m_Anchor;
		POINT	     m_Select;
		POINT	     m_Start;
		POINT	     m_End;

		// Implementation
		void SetMapping(HDC hDC, RECT &Clip);
		void AdjustRect(RECT &Clip);
		void Draw(HDC hDC, RECT &Clip);
		void Draw(HDC hDC, POINT &p, SIZE &s, PCTXT pText, int nFrom, int nTo, BOOL fSelect);
		BOOL Scroll(int nTop);
		void UpdateScrollBar(void);
		void InvalidateLine(int nLine);
		BOOL KeepInView(int nLine);
		void ClearSelection(void);
		void FindSelection(void);
		void SelectWholeWord(void);
		void FindMousePos(POINTS p);
		void CopySelection(void);
		void SelectAll(void);
	};

// End of File

#endif
