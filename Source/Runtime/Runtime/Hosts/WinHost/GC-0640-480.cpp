
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite Edge Controller 640x480 Model Data
//

static BYTE imageGC_0640_480[] = {

	#include "g10-x1.png.dat"
	0
	};

static CHostKey keysGC_0640_480[] = {

	{ 276, 604, 324, 652, 128 },
	{ 372, 604, 420, 652, 129 },
	{ 468, 604, 516, 652, 162 }

	};

global CHostModel modelGC_0640_480 = {

	"Graphite(R)",
	"GC-0640-480",
	"gc",
	"gc",
	rfGraphite,
	1,
	792,
	704,
	76,
	100,
	640,
	480,
	elements(keysGC_0640_480),
	keysGC_0640_480,
	sizeof(imageGC_0640_480)-1,
	imageGC_0640_480
	};

// End of File
