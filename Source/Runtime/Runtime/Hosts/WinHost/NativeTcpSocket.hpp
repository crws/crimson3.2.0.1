
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NativeTcpSocket_HPP

#define INCLUDE_NativeTcpSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Types
//

using win32::SOCKET;

using win32::HANDLE;

using win32::sockaddr_in;

using win32::sockaddr;

//////////////////////////////////////////////////////////////////////////
//
// Native TCP Socket Object
//

class CNativeTcpSocket : public ISocket
{
	public:
		// Constructor
		CNativeTcpSocket(void);

		// Destructor
		~CNativeTcpSocket(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ISocket
		HRM Listen(WORD Loc);
		HRM Listen(IPADDR const &IP, WORD Loc);
		HRM Connect(IPADDR const &IP, WORD Rem);
		HRM Connect(IPADDR const &IP, WORD Rem, WORD Loc);
		HRM Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc);
		HRM Recv(PBYTE pData, UINT &uSize, UINT uTime);
		HRM Recv(PBYTE pData, UINT &uSize);
		HRM Send(PBYTE pData, UINT &uSize);
		HRM Recv(CBuffer * &pBuff, UINT uTime);
		HRM Recv(CBuffer * &pBuff);
		HRM Send(CBuffer   *pBuff);
		HRM GetLocal (IPADDR &IP);
		HRM GetRemote(IPADDR &IP);
		HRM GetLocal (IPADDR &IP, WORD &Port);
		HRM GetRemote(IPADDR &IP, WORD &Port);
		HRM GetPhase(UINT &Phase);
		HRM SetOption(UINT uOption, UINT uValue);
		HRM Abort(void);
		HRM Close(void);

	protected:
		// States
		enum
		{
			stateInit,
			stateListen,
			stateConnect,
			stateOpen,
			stateClosing,
			stateError
			};

		// Data Members
		ULONG  m_uRefs;
		SOCKET m_hSocket;
		HANDLE m_hEvent;
		BOOL   m_fDebug;
		UINT   m_uState;

		// Implementation
		void SetNonBlocking(void);
		void AllowDupAddress(void);
		BOOL LoadAddress(sockaddr_in &addr, IPADDR const &IP, WORD Port);
		BOOL LoadAddress(sockaddr_in &addr, WORD Port);
		BOOL CheckRemoteClose(void);
		void SetAbortMode(void);
		void CloseHandle(void);
		void Debug(PCTXT pType, PCBYTE pData, UINT uSize);
	};

// End of File

#endif
