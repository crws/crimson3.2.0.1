
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite G15 Model Data
//

static BYTE imageG15[] = {

	#include "g15-x1.png.dat"
	0
	};

static CHostKey keysG15[] = {

	{ 456, 922, 516, 982, 128 },
	{ 576, 922, 636, 982, 129 },
	{ 696, 922, 756, 982, 162 }

	};

global CHostModel modelG15 = {

	"Graphite(R)",
	"G15",
	"g15",
	"g15",
	rfGraphite,
	1,
	1212,
	1046,
	94,
	124,
	1024,
	768,
	elements(keysG15),
	keysG15,
	sizeof(imageG15)-1,
	imageG15
	};

// End of File
