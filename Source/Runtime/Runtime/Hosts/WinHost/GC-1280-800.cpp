
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite Edge Controller 1280x800 Model Data
//

static BYTE imageGC_1280_800[] = {

	#include "g12-x1.png.dat"
	0
	};

static CHostKey keysGC_1280_800[] = {

	{ 596, 924, 644, 972, 128 },
	{ 692, 924, 740, 972, 129 },
	{ 788, 924, 836, 972, 162 }

	};

global CHostModel modelGC_1280_800 = {

	"Graphite(R)",
	"GC-1280-800",
	"gc",
	"gc",
	rfGraphite,
	1,
	1432,
	1024,
	76,
	100,
	1280,
	800,
	elements(keysGC_1280_800),
	keysGC_1280_800,
	sizeof(imageGC_1280_800)-1,
	imageGC_1280_800
	};

// End of File
