
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_HostConfig_HPP

#define INCLUDE_HostConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

struct CHostModel;

//////////////////////////////////////////////////////////////////////////
//
// Host Configuration
//

class CHostConfig
{
	public:
		// Constructor
		CHostConfig(void);

		// Operations
		BOOL BindModel(void);

		// Data Members
		bool	           m_fConsole;
		bool	           m_fBlind;
		bool		   m_fEmulate;
		bool		   m_fFrame;
		bool	           m_fService;
		CString            m_Ident;
		CString            m_Model;
		CString            m_Caption;
		CString            m_EmData;
		CString		   m_Service;
		CStringMap         m_Map;
		CHostModel const * m_pModel;
	};

// End of File

#endif
