
#include "Intern.hpp"

#include "Qutex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Quick Mutex Object
//

// Instantiator

static IUnknown * Create_Qutex(PCTXT pName)
{
	return New CQutex;
	}

// Registration

global void Register_Qutex(void)
{
	piob->RegisterInstantiator("exec.qutex", Create_Qutex);

	piob->RegisterInstantiator("exec.rutex", Create_Qutex);
	}

// Constructor

CQutex::CQutex(void)
{
	StdSetRef();

	win32::InitializeCriticalSection(&m_cs);
	}

// Destructor

CQutex::~CQutex(void)
{
	win32::DeleteCriticalSection(&m_cs);
	}

// IUnknown

HRESULT CQutex::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(IMutex);

	return E_NOINTERFACE;
	}

ULONG CQutex::AddRef(void)
{
	StdAddRef();
	}

ULONG CQutex::Release(void)
{
	StdRelease();
	}

// IWaitable

PVOID CQutex::GetWaitable(void)
{
	AfxAssert(FALSE);

	return NULL;
	}

BOOL CQutex::Wait(UINT uWait)
{
	if( uWait == FOREVER ) {

		win32::EnterCriticalSection(&m_cs);

		if( CExecThread::m_pThread ) {

			CExecThread::m_pThread->Guard(TRUE);
			}

		return TRUE;
		}

	if( uWait == 0 ) {

		if( win32::TryEnterCriticalSection(&m_cs) ) {

			if( CExecThread::m_pThread ) {

				CExecThread::m_pThread->Guard(TRUE);
				}

			return TRUE;
			}

		return FALSE;
		}

	AfxAssert(FALSE);

	return TRUE;
	}

BOOL CQutex::HasRequest(void)
{
	AfxAssert(FALSE);

	return FALSE;
	}

// IQutex

void CQutex::Free(void)
{
	if( CExecThread::m_pThread ) {

		CExecThread::m_pThread->Guard(FALSE);
		}

	win32::LeaveCriticalSection(&m_cs);
	}

// End of File
