
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Beeper_HPP

#define INCLUDE_Beeper_HPP

//////////////////////////////////////////////////////////////////////////
//
// Windows Multimedia
//

AfxNamespaceBegin(win32);

#include <MMSystem.h>

AfxNamespaceEnd(win32);

//////////////////////////////////////////////////////////////////////////
//
// Imported Types
//

using win32::HANDLE;

using win32::HWAVEOUT;

//////////////////////////////////////////////////////////////////////////
//
// Beeper Object
//

class CBeeper : public IBeeper, public IClientProcess
{
	public:
		// Constructor
		CBeeper(void);

		// Destructor
		~CBeeper(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IBeeper
		void METHOD Beep(UINT uBeep);
		void METHOD Beep(UINT uFreq, UINT uTime);
		void METHOD BeepOff(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

	protected:
		// Data Members
		ULONG    m_uRefs;
		HTHREAD  m_hThread;
		HANDLE   m_hBuff;
		HWAVEOUT m_hOut;
		HANDLE   m_hPlay;
		HANDLE   m_hStop;
		HANDLE   m_hExit;
		HANDLE   m_hDone;
		UINT     m_uSamp;
		UINT     m_uFreq;
		UINT     m_uTime;
	};

// End of File

#endif
