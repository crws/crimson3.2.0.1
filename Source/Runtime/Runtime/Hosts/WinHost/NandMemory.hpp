
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandMemory_HPP
	
#define	INCLUDE_NandMemory_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Memory.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Windows Emulated Nand Flash Manager
//

class CNandMemory : public CMemory, public INandMemory
{	
	public:
		// Constructor
		CNandMemory(void);

		// Destructor
		~CNandMemory(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// INandMemory
		BOOL METHOD GetGeometry(CNandGeometry &Geom);
		BOOL METHOD GetUniqueId(UINT uChip, PBYTE pData);
		BOOL METHOD IsBlockBad(UINT uChip, UINT uBlock);
		BOOL METHOD MarkBlockBad(UINT uChip, UINT uBlock);
		BOOL METHOD EraseBlock(UINT uChip, UINT uBlock);
		BOOL METHOD WriteSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PCBYTE pData);
		BOOL METHOD ReadSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PBYTE pData);
		BOOL METHOD WritePage(UINT uChip, UINT uBlock, UINT uPage, PCBYTE pData);
		BOOL METHOD ReadPage(UINT uChip, UINT uBlock, UINT uPage, PBYTE pData);

	protected:
		// Data Members
		ULONG	      m_uRefs;
		CNandGeometry m_Geom;
		UINT	      m_uSizeSector;
		UINT	      m_uSizePage;
		UINT	      m_uSizeBlock;
		UINT	      m_uSizeChip;
		UINT	      m_uSizeTotal;
	};

// End of File

#endif
