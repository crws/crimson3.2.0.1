
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Qutex_HPP

#define INCLUDE_Qutex_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Types
//

using win32::CRITICAL_SECTION;

//////////////////////////////////////////////////////////////////////////
//
// Quick Mutex Object
//

class CQutex : public IMutex
{
	public:
		// Constructor
		CQutex(void);

		// Destructor
		~CQutex(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IWaitable
		PVOID METHOD GetWaitable(void);
		BOOL  METHOD Wait(UINT uTime);
		BOOL  METHOD HasRequest(void);

		// IMutex
		void METHOD Free(void);

	protected:
		// Data Members
		ULONG		 m_uRefs;
		CRITICAL_SECTION m_cs;
	};

// End of File

#endif
