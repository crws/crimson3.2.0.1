
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Display.hpp"

#include "Output.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Use Entire Win32 API
//

using namespace win32;

//////////////////////////////////////////////////////////////////////////
//
// Console Window
//

// Constants

#define MAX_TEXT	200

// Static Data

static HANDLE		m_hFile = INVALID_HANDLE_VALUE;

static HINSTANCE	m_hThis    = NULL;
static BOOL		m_fConsole = FALSE;
static HFONT		m_hFont    = NULL;
static HWND		m_hConsole = NULL;
static HWND		m_hOutput  = NULL;
static HWND		m_hCommand = NULL;
static HWND		m_hDisplay = NULL;
static WNDPROC		m_pfnSuper = NULL;
static UINT		m_uScroll  = NOTHING;
static COutput  *	m_pOutput  = NULL;
static CDisplay *	m_pDisplay = NULL;
static DWORD		m_dwPump   = 0;
static HTHREAD	        m_hThread  = NULL;
static IEvent	*       m_pCmdReq  = NULL;
static IEvent	*       m_pCmdAck  = NULL;

static CString		m_Buffer;
static CString		m_Console;
static CString		m_Input;
static CString		m_CmdText;
static CStringArray	m_History;

// Prototypes

global	BOOL		InitConsole(void);
global	BOOL		ExecConsole(HTHREAD hThread);
global	void		TermConsole(void);
global	LRESULT WINAPI	ConsoleWndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam);
global	LRESULT WINAPI	OutputWndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam);
global	LRESULT WINAPI	CommandWndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam);
global	LRESULT WINAPI	DisplayWndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam);
global	void		LogToConsole(CString Text);
static  int		ThreadProc(HTHREAD hThread, void *pParam, UINT uParam);
static	BOOL		CreateConsole(void);
static	BOOL		CreateDisplay(void);
static	BOOL		CreateLogFile(void);
static	void		FindWindowPos(RECT &Rect, PCTXT pName, DWORD dwStyle, int cx, int cy, int nMon);
static	void		SaveWindowPos(HWND hWnd, PCTXT pName);
static	BOOL		SetRegValue(HKEY hKey, PCTXT pName, LONG  nData);
static	BOOL		GetRegValue(HKEY hKey, PCTXT pName, LONG &nData);
static	CString		GetRegKeyName(PCTXT pName);

// Code

global BOOL InitConsole(void)
{
	if( !g_Config.m_fService ) {

		m_hThis = GetModuleHandle(NULL);

		if( CreateConsole() && CreateDisplay() ) {

			if( m_hConsole ) {

				m_pCmdReq = Create_AutoEvent();

				m_pCmdAck = Create_AutoEvent();

				m_hThread = CreateThread(ThreadProc, 0, 0, 0);
				}

			m_fConsole = TRUE;

			return TRUE;
			}

		return FALSE;
		}

	AfxTrace("winhost: service mode selected\n");

	CreateLogFile();

	return TRUE;
	}

global BOOL ExecConsole(HTHREAD hThread)
{
	if( m_fConsole ) {

		if( !g_Config.m_fService ) {

			m_dwPump = GetCurrentThreadId();

			MSG Msg;

			while( GetMessage(&Msg, NULL, 0, 0) ) {
				
				if( GetThreadExecState(hThread) >= 4 ) {

					return TRUE;
					}

				TranslateMessage(&Msg);

				DispatchMessage(&Msg);
				}

			return FALSE;
			}
		}

	return TRUE;
	}

global void TermConsole(void)
{
	if( m_fConsole ) {

		if( !g_Config.m_fService ) {

			if( m_hConsole ) {

				DestroyWindow(m_hConsole);

				DestroyThread(m_hThread);

				m_pCmdReq->Release();

				m_pCmdAck->Release();
				}

			if( m_hDisplay ) {

				DestroyWindow(m_hDisplay);

				DeleteObject (m_hFont);
				}
			}

		m_fConsole = FALSE;
		}
	}

global LRESULT WINAPI ConsoleWndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( uMessage == WM_CREATE ) {

		m_hFont    = CreateFont( 16, 0,
					 0, 0,
					 FW_NORMAL,
					 0, 0, 0, 0, 0, 0, 0, 0,
					 L"Consolas"
					 );

		m_hOutput  = CreateWindow( L"AeonOutputClass",
					   L"",
					   WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_VSCROLL,
					   0, 0, 0, 0,
					   hWnd,
					   HMENU(100),
					   m_hThis,
					   NULL
					   );

		m_hCommand = CreateWindow( L"edit",
					   L"",
					   WS_CHILD | WS_VISIBLE,
					   0, 0, 0, 0,
					   hWnd,
					   HMENU(101),
					   m_hThis,
					   NULL
					   );

		m_pfnSuper = (WNDPROC) SetWindowLong(m_hCommand, GWL_WNDPROC, LONG(&CommandWndProc));

		SendMessage   (m_hCommand, WM_SETFONT, WPARAM(m_hFont), 0);

		SetWindowTextA(m_hCommand, "diag.help");

		SendMessage   (m_hCommand, EM_SETSEL, 0, 32767);

		SendMessage   (m_hCommand, EM_SETLIMITTEXT, MAX_TEXT - 1, 0);

		AfxDoNotTrack(TRUE);

		m_History.Append("diag.help");

		AfxDoNotTrack(FALSE);
		}

	if( uMessage == WM_DESTROY ) {

		SaveWindowPos(hWnd, "Console");
		}

	if( uMessage == WM_SIZE ) {

		if( m_hOutput ) {

			RECT r1, r2;

			GetClientRect(hWnd, &r1);

			InflateRect(&r1, -6, -6);

			r2 = r1;

			r1.bottom -= 26;

			r2.top     = r1.bottom + 8;

			MoveWindow( m_hOutput,
				    r1.left,
				    r1.top,
				    r1.right - r1.left,
				    r1.bottom - r1.top,
				    TRUE
				    );

			MoveWindow( m_hCommand,
				    r2.left,
				    r2.top,
				    r2.right - r2.left,
				    r2.bottom - r2.top,
				    TRUE
				    );

			InvalidateRect(hWnd, NULL, TRUE);
			}
		}

	if( uMessage == WM_CTLCOLORSTATIC ) {

		if( lParam == LPARAM(m_hOutput) ) {

			HDC hDC = HDC(wParam);

			SetBkColor(hDC, RGB(255,255,255));

			return LRESULT(GetStockObject(WHITE_BRUSH));
			}
		}

	if( uMessage == WM_MOUSEWHEEL ) {

		if( m_pOutput ) {

			POINTS s = MAKEPOINTS(lParam);

			POINT  p = { s.x, s.y };

			RECT   r;

			GetWindowRect(m_hOutput, &r);

			if( PtInRect(&r, p) ) {

				m_pOutput->WndProc(uMessage, wParam, lParam);

				return 0;
				}
			}
		}

	if( uMessage == WM_PAINT ) {

		PAINTSTRUCT ps;

		HDC hDC = BeginPaint(hWnd, &ps);

		RECT r1, r2;

		GetWindowRect(m_hOutput, &r1);
		GetWindowRect(m_hCommand,   &r2);

		ScreenToClient(hWnd, PPOINT(&r1)+0);
		ScreenToClient(hWnd, PPOINT(&r1)+1);
		ScreenToClient(hWnd, PPOINT(&r2)+0);
		ScreenToClient(hWnd, PPOINT(&r2)+1);

		for( UINT n = 0; n < 2; n++ ) {

			InflateRect(&r1, 1, 1);
			InflateRect(&r2, 1, 1);

			FrameRect(hDC, &r1, GetStockObject(WHITE_BRUSH));
			FrameRect(hDC, &r2, GetStockObject(WHITE_BRUSH));
			}

		EndPaint(hWnd, &ps);

		return 0;
		}

	if( uMessage == WM_SETFOCUS ) {

		SetFocus(m_hCommand);
		}

	if( uMessage == WM_COMMAND ) {

		if( wParam == 102 ) {

			if( m_Input == "cls" ) {

				m_pOutput->Clear();
				}
			else {
				LogToConsole("\n> " + m_Input + "\n");

				if( m_Input == "sync" ) {

					m_pOutput->Sync();

					return 0;
					}

				if( m_Input.GetLength() ) {

					LogToConsole("\n");

					if( m_Input == "exit" ) {

						PostQuitMessage(0);

						return 0;
						}

					if( m_Input == "help" ) {

						m_Input = "diag.help";
						}

					m_pCmdAck->Wait(FOREVER);

					AfxDoNotTrack(TRUE);

					m_CmdText = m_Input;

					m_CmdText.CopyOnWrite();

					AfxDoNotTrack(FALSE);

					m_pCmdReq->Set();

					return 0;
					}
				}
			}
		}

	if( uMessage == WM_CLOSE ) {

		PostQuitMessage(0);

		return 0;
		}

	return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}

global LRESULT WINAPI OutputWndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( uMessage == WM_CREATE ) {

		m_pOutput = New COutput;

		m_pOutput->Bind(hWnd);

		m_pOutput->SetFont(m_hFont);
		}

	if( uMessage == WM_DESTROY ) {

		COutput *pOutput;
		
		pOutput   = m_pOutput;

		m_pOutput = NULL;

		pOutput->WndProc(uMessage, wParam, lParam);

		delete pOutput;
		}

	if( m_pOutput ) {

		return m_pOutput->WndProc(uMessage, wParam, lParam);
		}

	return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}

global LRESULT WINAPI CommandWndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( uMessage == WM_CHAR ) {

		if( wParam == VK_RETURN ) {

			char sText[MAX_TEXT];

			GetWindowTextA(hWnd, sText, elements(sText));

			m_Input = sText;

			if( m_History.IsEmpty() || m_History.GetLastElement() != m_Input ) {

				AfxDoNotTrack(TRUE);

				CString Copy = m_Input;

				Copy.CopyOnWrite();

				m_History.Append(Copy);

				AfxDoNotTrack(FALSE);
				}

			SetWindowTextA(hWnd, "");

			m_uScroll = NOTHING;

			SendMessage(GetParent(hWnd), WM_COMMAND, 102, 0);

			return 0;
			}

		if( wParam == VK_ESCAPE ) {

			SetWindowTextA(hWnd, "");

			m_uScroll = NOTHING;

			return 0;
			}
		}

	if( uMessage == WM_KEYDOWN ) {

		if( wParam == VK_UP ) {

			if( !m_History.IsEmpty() ) {

				if( m_uScroll == NOTHING ) {

					m_uScroll = m_History.GetCount();
					}

				if( m_uScroll > 0 ) {

					SetWindowTextA(hWnd, m_History[--m_uScroll]);

					UINT n = GetWindowTextLength(hWnd);

					SendMessage(hWnd, EM_SETSEL, n, n);
					}
				}

			return 0;
			}

		if( wParam == VK_DOWN ) {

			if( m_uScroll < NOTHING ) {

				if( m_uScroll < m_History.GetLast() ) {

					SetWindowTextA(hWnd, m_History[++m_uScroll]);

					UINT n = GetWindowTextLength(hWnd);

					SendMessage(hWnd, EM_SETSEL, n, n);
					}
				}

			return 0;
			}

		if( wParam == VK_RIGHT ) {

			if( !m_History.IsEmpty() ) {

				LRESULT sel = SendMessage(hWnd, EM_GETSEL, 0, 0);

				int     s1  = LOWORD(sel);

				int     s2  = HIWORD(sel);

				int     tl  = int(SendMessage(hWnd, WM_GETTEXTLENGTH, 0, 0));

				if( (!s1 || s1 == s2) && s2 == tl ) {

					char sText[MAX_TEXT];

					GetWindowTextA(hWnd, sText, elements(sText));

					CString History = m_History[m_History.GetLast()];

					if( History.StartsWith(sText) ) {

						sText[tl  ] = History[tl];

						sText[++tl] = 0;

						SetWindowTextA(hWnd, sText);

						SendMessage(hWnd, EM_SETSEL, tl, tl);

						return 0;
						}
					}
				}
			}
		}

	return (*m_pfnSuper)(hWnd, uMessage, wParam, lParam);
	}

global LRESULT WINAPI DisplayWndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( uMessage == WM_CREATE ) {

		m_pDisplay = CDisplay::m_pThis;

		m_pDisplay->Bind(hWnd);
		}

	if( uMessage == WM_DESTROY ) {

		SaveWindowPos(hWnd, "Display");

		m_pDisplay->WndProc(uMessage, wParam, lParam);

		m_pDisplay = NULL;
		}

	if( m_pDisplay ) {

		return m_pDisplay->WndProc(uMessage, wParam, lParam);
		}

	return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}

global void LogToConsole(CString Text)
{
	if( g_Config.m_fService ) {

		if( m_hFile != INVALID_HANDLE_VALUE ) {

			UINT  uLines = Text.Replace("\n", "\r\n");
		
			DWORD dwSize = 0;

			WriteFile(m_hFile, PCTXT(Text), Text.GetLength(), &dwSize, NULL);

			if( uLines ) {

				FlushFileBuffers(m_hFile);
				}
			}
		}
	else {
		if( m_pOutput ) {

			AfxDoNotTrack(TRUE);

			m_Buffer += Text;

			for(;;) {

				UINT uFind = m_Buffer.Find('\n');

				if( uFind < NOTHING ) {

					CString Text = m_Buffer.Left(uFind);

					m_Buffer.Delete(0, uFind + 1);

					AfxDoNotTrack(FALSE);

					if( GetCurrentThreadId() == m_dwPump ) {

						m_pOutput->AddLine(Text, TRUE);
						}
					else
						m_pOutput->AddLine(Text, FALSE);

					AfxDoNotTrack(TRUE);

					continue;
					}

				break;
				}

			AfxDoNotTrack(FALSE);
			}
		}
	}

static int ThreadProc(HTHREAD hThread, void *pParam, UINT uParam)
{
	for(;;) { 

		m_pCmdAck->Set();

		m_pCmdReq->Wait(FOREVER);

		IDiagManager *pDiag = NULL;

		AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

		pDiag->RunCommand(NULL, m_CmdText);

		pDiag->Release();
		}
	}

static BOOL CreateConsole(void)
{
	if( g_Config.m_fConsole ) {

		WNDCLASS Class1, Class2;

		Class1.style		= 0;
		Class1.lpfnWndProc	= ConsoleWndProc;
		Class1.cbClsExtra	= 0;
		Class1.cbWndExtra	= 0;
		Class1.hInstance	= m_hThis;
		Class1.hCursor		= LoadCursor(NULL, IDC_ARROW);
		Class1.hIcon		= LoadIcon  (m_hThis, MAKEINTRESOURCE(101));
		Class1.hbrBackground	= GetStockObject(GRAY_BRUSH);
		Class1.lpszMenuName	= NULL;
		Class1.lpszClassName	= L"AeonConsoleClass";

		Class2.style		= CS_DBLCLKS;
		Class2.lpfnWndProc	= OutputWndProc;
		Class2.cbClsExtra	= 0;
		Class2.cbWndExtra	= 0;
		Class2.hInstance	= m_hThis;
		Class2.hCursor		= LoadCursor(NULL, IDC_IBEAM);
		Class2.hIcon		= NULL;
		Class2.hbrBackground	= GetStockObject(WHITE_BRUSH);
		Class2.lpszMenuName	= NULL;
		Class2.lpszClassName	= L"AeonOutputClass";

		if( RegisterClass(&Class1) && RegisterClass(&Class2) ) {

			DWORD dwStyle = WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN;
			
			RECT  Rect;

			FindWindowPos(Rect, "Console", dwStyle, 1200, 900, 0);

			m_hConsole = CreateWindow( L"AeonConsoleClass",
						   L"Aeon Host",
						   dwStyle,
						   Rect.left,
						   Rect.top,
						   Rect.right  - Rect.left,
						   Rect.bottom - Rect.top,
						   NULL,
						   NULL,
						   m_hThis,
						   NULL
						   );

			ShowWindow(m_hConsole, g_Config.m_fConsole ? SW_SHOW : SW_SHOWMINNOACTIVE);

			UpdateWindow(m_hConsole);

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

static BOOL CreateDisplay(void)
{
	if( !g_Config.m_fBlind ) {

		win32::WCHAR ClassName[64] = L"AeonWindow";

		win32::WCHAR Caption  [64] = {0};

		wstrcat(ClassName, g_Config.m_Ident);

		wstrcat(Caption,   g_Config.m_Caption);

		WNDCLASS Class;

		Class.style		= 0;
		Class.lpfnWndProc	= DisplayWndProc;
		Class.cbClsExtra	= 0;
		Class.cbWndExtra	= 0;
		Class.hInstance		= m_hThis;
		Class.hCursor		= LoadCursor(NULL, IDC_ARROW);
		Class.hIcon		= LoadIcon  (m_hThis, MAKEINTRESOURCE(101));
		Class.hbrBackground	= NULL;
		Class.lpszMenuName	= NULL;
		Class.lpszClassName	= ClassName;

		if( RegisterClass(&Class) ) {

			int   xMon    = GetSystemMetrics(SM_XVIRTUALSCREEN);

			int   nMon    = g_Config.m_fConsole ? (xMon < 0) ? -1 : + 1 : 0;

			int   cx      = g_Config.m_fFrame ? g_Config.m_pModel->m_cxFrame : g_Config.m_pModel->m_cxImage * g_Config.m_pModel->m_nScale;

			int   cy      = g_Config.m_fFrame ? g_Config.m_pModel->m_cyFrame : g_Config.m_pModel->m_cyImage * g_Config.m_pModel->m_nScale;

			DWORD dwStyle = WS_POPUP | WS_MINIMIZEBOX | WS_SYSMENU;

			RECT  Rect;

			if( !g_Config.m_fFrame ) {

				cx += 4;

				cy += 4;

				dwStyle |= WS_CAPTION;
				}

			FindWindowPos(Rect, "Display", dwStyle, cx, cy, nMon);

			m_hDisplay = CreateWindow( ClassName,
						   Caption,
						   dwStyle,
						   Rect.left,
						   Rect.top,
						   Rect.right  - Rect.left,
						   Rect.bottom - Rect.top,
						   NULL,
						   NULL,
						   m_hThis,
						   NULL
						   );

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

static BOOL CreateLogFile(void)
{
	// TODO -- Put this file somewhere more appropriate.

	m_hFile = CreateFile(	L"C:\\Temp\\Aeon.txt",
				GENERIC_WRITE,
				FILE_SHARE_READ,
				NULL,
				CREATE_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL
				);

	return m_hFile != INVALID_HANDLE_VALUE;
	}

static void FindWindowPos(RECT &Rect, PCTXT pName, DWORD dwStyle, int cx, int cy, int nMon)
{
	SetRect(&Rect, 0, 0, cx, cy);

	AdjustWindowRect(&Rect, dwStyle, FALSE);

	cx = Rect.right  - Rect.left;

	cy = Rect.bottom - Rect.top;

	if( pName ) {

		HKEY hKey = NULL;

		RegOpenKeyExA( HKEY_CURRENT_USER,
			       GetRegKeyName(pName),
			       0,
			       KEY_ALL_ACCESS,
			       &hKey
			       );

		if( hKey ) {

			RECT  Read;

			GetRegValue(hKey, "left",   Read.left  );
			GetRegValue(hKey, "top",    Read.top   );
			GetRegValue(hKey, "right",  Read.right );
			GetRegValue(hKey, "bottom", Read.bottom);

			if( Read.right > Read.left && Rect.bottom > Rect.top ) {

				int dx = cx - (Read.right - Read.left);

				int dy = cy - (Read.bottom - Read.top);

				int sx = dx + ((dx < 0) ? -1 : +1);

				int sy = dy + ((dy < 0) ? -1 : +1);

				Rect.left   = Read.left   - dx/2;

				Rect.right  = Read.right  + sx/2;

				Rect.top    = Read.top    - dy/2;

				Rect.bottom = Read.bottom + sy/2;

				return;
				}
			}
		}

	Rect.left   = (GetSystemMetrics(SM_CXSCREEN) - cx) / 2 + nMon * GetSystemMetrics(SM_CXSCREEN);

	Rect.top    = (GetSystemMetrics(SM_CYSCREEN) - cy) / 2;

	Rect.right  = Rect.left + cx;

	Rect.bottom = Rect.top  + cy;
	}

static void SaveWindowPos(HWND hWnd, PCTXT pName)
{
	if( pName ) {

		HKEY hKey = NULL;

		RegCreateKeyExA( HKEY_CURRENT_USER,
				 GetRegKeyName(pName),
				 0,
				 NULL,
				 0,
				 KEY_ALL_ACCESS,
				 NULL,
				 &hKey,
				 NULL
				 );

		if( hKey ) {

			if( !IsIconic(hWnd) ) {

				RECT Rect;

				GetWindowRect(hWnd, &Rect);

				SetRegValue(hKey, "left",   Rect.left  );
				SetRegValue(hKey, "top",    Rect.top   );
				SetRegValue(hKey, "right",  Rect.right );
				SetRegValue(hKey, "bottom", Rect.bottom);
				}

			RegCloseKey(hKey);
			}
		}
	}

static BOOL SetRegValue(HKEY hKey, PCTXT pName, LONG nData)
{
	RegSetValueExA(hKey, pName, 0, REG_DWORD, PCBYTE(&nData), 4);

	return TRUE;
	}

static BOOL GetRegValue(HKEY hKey, PCTXT pName, LONG &nData)
{
	DWORD dwType = 0;

	DWORD dwSize = 4;

	RegQueryValueExA(hKey, pName, 0, &dwType, PBYTE(&nData), &dwSize);

	if( dwType == REG_DWORD && dwSize == 4 ) {

		return TRUE;
		}

	nData = 0;

	return FALSE;
	}

static CString GetRegKeyName(PCTXT pName)
{
	return CPrintf("Software\\Red Lion Controls\\Aeon\\1.0\\WinHost\\Windows\\%s", pName);
	}

// End of File
