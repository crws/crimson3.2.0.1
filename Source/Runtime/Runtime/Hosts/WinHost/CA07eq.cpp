
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Canyon CA07eq Model Data
//

static BYTE imageCA07eq[] = {

	#include "ca07eq-x2.png.dat"
	0
	};

global CHostModel modelCA07eq = {

	"",
	"CA07EQ",
	"CR3000-07",
	"CR3000-07000",
	rfCanyon,
	2,
	944,
	928,
	152,
	200,
	320,
	240,
	0,
	NULL,
	sizeof(imageCA07eq)-1,
	imageCA07eq
	};

// End of File
