
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite G10r Model Data
//

static BYTE imageG10r[] = {

	#include "g10r-x1.png.dat"
	0
	};

static CHostKey keysG10r[] = {

	{ 344, 754, 404, 814, 128 },
	{ 464, 754, 524, 814, 129 },
	{ 584, 754, 644, 814, 162 }

	};

global CHostModel modelG10r = {

	"Graphite(R)",
	"G10r",
	"g10r",
	"g10r",
	rfGraphite,
	1,
	988,
	878,
	94,
	124,
	800,
	600,
	elements(keysG10r),
	keysG10r,
	sizeof(imageG10r)-1,
	imageG10r
	};

// End of File
