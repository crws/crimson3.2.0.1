
#include "Intern.hpp"

#include "ModuleManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Imported Types
//

using win32::HANDLE;

//////////////////////////////////////////////////////////////////////////
//
// Imported APIs
//

using win32::CloseHandle;
using win32::CreateFileA;
using win32::DeleteFileA;
using win32::LoadLibraryA;
using win32::FreeLibrary;
using win32::GetProcAddress;
using win32::GetModuleFileNameA;
using win32::GetTempPathA;
using win32::WriteFile;

//////////////////////////////////////////////////////////////////////////
//
// Module Manager
//

// Constructor

CModuleManager::CModuleManager(void)
{
	StdSetRef();

	FindHostPath();

	FindTempPath();

	piob->RegisterSingleton("host.modman", 0, this);
	}

// Destructor

CModuleManager::~CModuleManager(void)
{
	// The module manager is deleted via a C++ delete call
	// so we don't want the Release from the revocation to
	// kick off another call to the destructor!

	AddRef();

	piob->RevokeSingleton("host.modman", 0);

	FreeAllModules();
	}

// IUnknown

HRESULT CModuleManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IModuleManager);

	StdQueryInterface(IModuleManager);

	return E_NOINTERFACE;
	}

ULONG CModuleManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CModuleManager::Release(void)
{
	StdRelease();
	}

// Operations

bool CModuleManager::LoadClientModule(PCTXT pName)
{
	UINT hModule;

	return LoadClientModule(hModule, pName);
	}

bool CModuleManager::LoadClientModule(UINT &hModule, PCTXT pName)
{
	if( pName && *pName ) {

		CModule *pModule = New CModule;

		if( pName[0] == '!' ) {

			CString Path = m_Path.Left(m_Path.FindRev('\\') + 1);

			pModule->m_Name = Path + (pName + 1) + ".dll";
			}
		else {
			CString Name = pName;

			CString Path = Name.Count('\\')      ? "" : m_Path.Left(m_Path.FindRev('\\') + 1);

			CString Type = Name.EndsWith(".dll") ? "" : ".dll";

			pModule->m_Name = Path + Name + Type;
			}

		if( (pModule->m_hLib = LoadLibraryA(pModule->m_Name)) ) {

			pModule->m_pfnMain = MAINPTR(GetProcAddress(pModule->m_hLib, "AeonCslMain"));

			pModule->m_pfnExit = EXITPTR(GetProcAddress(pModule->m_hLib, "AeonCslExit"));

			if( pModule->m_pfnMain && pModule->m_pfnExit ) {

				(pModule->m_pfnMain)(piob);

				hModule = UINT(m_List.Append(pModule));

				return true;
				}

			AfxTrace("ERROR: Cannot find entry points\n");

			delete pModule;

			return false;
			}

		AfxTrace("ERROR: Cannot load client module %s\n", PCTXT(pModule->m_Name));

		delete pModule;
		}

	return false;
	}

bool CModuleManager::LoadClientModule(UINT &hModule, PCTXT pName, PCBYTE pData, UINT uSize)
{
	CString Name  = pName;

	CString Path  = m_Temp + Name + ".dll";
	
	HANDLE	hFile = CreateFileA( Path,
				     GENERIC_READ | GENERIC_WRITE,
				     FILE_SHARE_READ,
				     NULL,
				     OPEN_ALWAYS,
				     0,
				     NULL
				     );

	if( hFile != INVALID_HANDLE_VALUE ) {

		DWORD uDone;

		WriteFile  (hFile, pData, uSize, &uDone, NULL);

		CloseHandle(hFile);

		if( uDone == uSize ) {

			return LoadClientModule(hModule, Path);
			}

		DeleteFileA(Path);
		}

	return false;
	}

bool CModuleManager::FreeClientModule(UINT hModule)
{
	if( hModule ) {

		CModule *pModule = m_List[INDEX(hModule)];

		(pModule->m_pfnExit)();

		FreeLibrary(pModule->m_hLib);

		if( pModule->m_Name.StartsWith(m_Temp) ) {
			
			DeleteFileA(pModule->m_Name);
			}

		delete pModule;

		m_List.Remove(INDEX(hModule));

		return true;
		}

	return false;
	}

bool CModuleManager::FreeAllModules(void)
{
	INDEX i;

	while( !m_List.Failed(i = m_List.GetTail()) ) {

		FreeClientModule(UINT(i));
		}

	return true;
	}

// Implementation

void CModuleManager::FindHostPath(void)
{
	char path[MAX_PATH];

	GetModuleFileNameA(NULL, path, sizeof(path));

	m_Path = path;
	}

void CModuleManager::FindTempPath(void)
{
	char path[MAX_PATH];

	GetTempPathA(MAX_PATH, path);

	m_Temp = path;
	}

// End of File
