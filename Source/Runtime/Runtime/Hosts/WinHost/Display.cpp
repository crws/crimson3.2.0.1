
#include "Intern.hpp"

#include "Display.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display Object
//

// Constants

#define timeInitial 500

#define timeRepeat  100

#define cellSize    4

// Instance Pointer

CDisplay * CDisplay::m_pThis = NULL;

// Instantiator

IDevice * Create_Display(void)
{
	return (IDisplay *) New CDisplay;
	}

// Constructor

CDisplay::CDisplay(void)
{
	StdSetRef();

	m_fShow   = false;

	m_pLock   = Create_Mutex();

	m_pThis   = this;

	m_pMap    = NULL;

	m_uTimer  = 0;

	m_fBeep   = TRUE;

	m_fLed[0] = FALSE;

	m_fLed[1] = FALSE;

	m_fLed[2] = FALSE;

	m_hLed[0] = CreateSolidBrush(RGB(255,0,0));

	m_hLed[1] = CreateSolidBrush(RGB(0,0,255));

	m_hLed[2] = CreateSolidBrush(RGB(0,255,0));
	}

// Destructor

CDisplay::~CDisplay(void)
{
	DeleteObject(m_hLed[0]);
	
	DeleteObject(m_hLed[1]);
	
	DeleteObject(m_hLed[2]);

	m_pLock->Release();

	m_pThis = NULL;
	}

// Operations

void CDisplay::Bind(HWND hWnd)
{
	m_hWnd = hWnd;
	}

LRESULT CDisplay::WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( uMessage == WM_CREATE ) {

		GdiPlusInit();

		RECT Rect;

		GetClientRect(m_hWnd, &Rect);

		SetWindowLong(m_hWnd, GWL_EXSTYLE, GetWindowLong(m_hWnd, GWL_EXSTYLE) | WS_EX_LAYERED);

		SetLayeredWindowAttributes(m_hWnd, RGB(0xFE, 0x01, 0xFE), 255, LWA_COLORKEY);

		m_cx = g_Config.m_pModel->m_cxImage;

		m_cy = g_Config.m_pModel->m_cyImage;

		m_xCells = (m_cx + cellSize - 1) / cellSize;

		m_yCells = (m_cy + cellSize - 1) / cellSize;

		BITMAPINFO bmi;

		ZeroMemory(&bmi, sizeof(bmi));

		bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
		bmi.bmiHeader.biWidth       = +m_cx;
		bmi.bmiHeader.biHeight      = -m_cy;
		bmi.bmiHeader.biPlanes      = 1;
		bmi.bmiHeader.biBitCount    = 32;
		bmi.bmiHeader.biCompression = BI_RGB;
		bmi.bmiHeader.biSizeImage   = m_cx * m_cy * 4;

		m_hDraw = CreateCompatibleDC(NULL);

		m_pData = NULL;

		m_hBits = CreateDIBSection(m_hDraw, &bmi, DIB_RGB_COLORS, (void **) &m_pData, NULL, 0);

		m_hOld  = SelectObject(m_hDraw, m_hBits);

		memset(m_pData, 0, m_cx * m_cy * 4);

		m_hEvent = CreateEvent(NULL, FALSE, TRUE, NULL);

		AfxGetObject("beeper",  0, IBeeper,     m_pBeeper);

		AfxGetObject("input-d", 0, IInputQueue, m_pInput);

		#ifdef NDEBUG

		if( !g_Config.m_Map.IsEmpty() ) {

			m_uTimer = 15 * 60;

			SetTimer(m_hWnd, 200, 1000, NULL);
			}

		#endif

		m_fPress = FALSE;
		}

	if( uMessage == WM_DESTROY ) {

		Claim();

		SelectObject(m_hDraw, m_hOld);

		DeleteObject(m_hBits);

		DeleteDC(m_hDraw);

		CloseHandle(m_hEvent);

		AfxRelease(m_pBeeper);

		AfxRelease(m_pInput);

		m_pData = NULL;

		Free();

		GdiPlusTerm();
		}

	if( uMessage == WM_NCHITTEST ) {

		if( g_Config.m_fFrame ) {

			POINT p;

			p.x = short(LOWORD(lParam));
		
			p.y = short(HIWORD(lParam));

			ScreenToClient(m_hWnd, &p);

			if( p.x >= g_Config.m_pModel->m_xpImage && p.x < g_Config.m_pModel->m_xpImage + g_Config.m_pModel->m_cxImage ) {

				if( p.y >= g_Config.m_pModel->m_ypImage && p.y < g_Config.m_pModel->m_ypImage + g_Config.m_pModel->m_cyImage ) {

					return HTCLIENT;
					}
				}
		
			for( int k = 0; k < g_Config.m_pModel->m_nKey; k++ ) {

				CHostKey const &key = g_Config.m_pModel->m_pKey[k];

				if( p.x >= key.m_x1 && p.x < key.m_x2 ) {

					if( p.y >= key.m_y1 && p.y < key.m_y2 ) {

						return HTCLIENT;
						}
					}
				}

			return HTCAPTION;
			}
		}

	if( uMessage == WM_PAINT ) {

		if( g_Config.m_fFrame ) {

			PAINTSTRUCT ps;

			HDC         hDC    = BeginPaint(m_hWnd, &ps);

			HDC         hWork  = CreateCompatibleDC(hDC);

			HBITMAP     hBits  = CreateCompatibleBitmap(hDC, g_Config.m_pModel->m_cxFrame, g_Config.m_pModel->m_cyFrame);

			HBITMAP     hPrev  = SelectObject(hWork, hBits);

			RECT        Rect   = { 0, 0, g_Config.m_pModel->m_cxFrame, g_Config.m_pModel->m_cyFrame };

			HBRUSH      hFill  = CreateSolidBrush(RGB(0xFE, 0x01, 0xFE));

			GpImage    *pImage = CreateImage();

			GpGraphics *pGraph = NULL;

			GdipCreateFromHDC(hWork, &pGraph);

			FillRect(hWork, &Rect, hFill);

			GdipDrawImageRectI(pGraph, pImage, 0, 0, g_Config.m_pModel->m_cxFrame, g_Config.m_pModel->m_cyFrame);

			GdipDeleteGraphics(pGraph);

			GdipDisposeImage(pImage);

			if( g_Config.m_pModel->m_nScale > 1 ) {

				StretchBlt( hWork,
					    g_Config.m_pModel->m_xpImage,
					    g_Config.m_pModel->m_ypImage,
					    g_Config.m_pModel->m_cxImage * g_Config.m_pModel->m_nScale,
					    g_Config.m_pModel->m_cyImage * g_Config.m_pModel->m_nScale,
					    m_hDraw,
					    0,
					    0,
					    g_Config.m_pModel->m_cxImage,
					    g_Config.m_pModel->m_cyImage,
					    SRCCOPY
					    );
				}
			else {
				BitBlt( hWork,
					g_Config.m_pModel->m_xpImage,
					g_Config.m_pModel->m_ypImage,
					g_Config.m_pModel->m_cxImage,
					g_Config.m_pModel->m_cyImage,
					m_hDraw,
					0,
					0,
					SRCCOPY
					);
				}


			for( UINT n = 0; n < 3; n++ ) {

				RECT r = { 32 + n * 32, 32, 52 + n * 32, 52 };

				FrameRect(hWork, &r, GetStockObject(DKGRAY_BRUSH));

				InflateRect(&r, -2, -2);

				FillRect(hWork, &r, m_fLed[n] ? m_hLed[n] : GetStockObject(BLACK_BRUSH));
				}

			BitBlt( hDC,
				0,
				0,
				g_Config.m_pModel->m_cxFrame,
				g_Config.m_pModel->m_cyFrame,
				hWork,
				0,
				0,
				SRCCOPY
				);

			DeleteObject(hFill);

			SelectObject(hWork, hPrev);

			DeleteObject(hBits);

			DeleteDC(hWork);

			EndPaint(m_hWnd, &ps);
			}
		else {
			PAINTSTRUCT ps;

			HDC         hDC = BeginPaint(m_hWnd, &ps);

			RECT        Rect;

			GetClientRect(m_hWnd, &Rect);

			FrameRect(hDC, &Rect, GetStockObject(BLACK_BRUSH));

			InflateRect(&Rect, -1, -1);

			FrameRect(hDC, &Rect, GetStockObject(BLACK_BRUSH));

			if( g_Config.m_pModel->m_nScale > 1 ) {

				StretchBlt( hDC,
					    2,
					    2,
					    g_Config.m_pModel->m_cxImage * g_Config.m_pModel->m_nScale,
					    g_Config.m_pModel->m_cyImage * g_Config.m_pModel->m_nScale,
					    m_hDraw,
					    0,
					    0,
					    g_Config.m_pModel->m_cxImage,
					    g_Config.m_pModel->m_cyImage,
					    SRCCOPY
					    );
				}
			else {
				BitBlt( hDC,
					2,
					2,
					g_Config.m_pModel->m_cxImage,
					g_Config.m_pModel->m_cyImage,
					m_hDraw,
					0,
					0,
					SRCCOPY
					);
				}

			EndPaint(m_hWnd, &ps);
			}
		}

	if( uMessage == WM_COMMAND ) {

		if( wParam == 100 ) {

			if( !m_fShow ) {

				HANDLE hEvent = OpenEventA( GENERIC_READ | GENERIC_WRITE,
							    FALSE,
							    "AeonEvent" + g_Config.m_Ident
							    );

				if( hEvent != INVALID_HANDLE_VALUE ) {

					SetEvent(hEvent);

					CloseHandle(hEvent);
					}

				ShowWindow(m_hWnd, SW_SHOWNA);

				m_fShow = true;
				}
			else {
				InvalidateRect(m_hWnd, NULL, FALSE);
				
				UpdateWindow(m_hWnd);
				}

			SetEvent(m_hEvent);
			}

		if( wParam == 150 ) {

			InvalidateRect(m_hWnd, NULL, FALSE);
				
			UpdateWindow(m_hWnd);
			}

		if( wParam == 200 ) {

			PostQuitMessage(0);
			}
		}

	if( uMessage == WM_LBUTTONDOWN ) {

		int px = short(LOWORD(lParam));
		
		int py = short(HIWORD(lParam));

		int tx = px - (g_Config.m_fFrame ? g_Config.m_pModel->m_xpImage : 2);

		int ty = py - (g_Config.m_fFrame ? g_Config.m_pModel->m_ypImage : 2);

		if( tx >= 0 && tx < g_Config.m_pModel->m_cxImage ) {

			if( ty >= 0 && ty < g_Config.m_pModel->m_cyImage ) {

				m_xVal   = tx;
		
				m_yVal   = ty;

				m_xPress = m_xVal / cellSize;

				m_yPress = m_yVal / cellSize;

				if( IsActive() ) {

					m_fPress = TRUE;

					StoreEvent(stateDown);

					SetTimer(m_hWnd, 100, timeInitial, NULL);
					}
				}
			}

		// TODO -- Keys!
		}

	if( uMessage == WM_LBUTTONUP ) {

		if( m_fPress ) {

			m_fPress = FALSE;

			StoreEvent(stateUp);

			KillTimer(m_hWnd, 100);
			}
		}

	if( uMessage == WM_TIMER ) {

		if( wParam == 100 ) {

			if( m_fPress ) {

				StoreEvent(stateRepeat);

				SetTimer(m_hWnd, 100, timeRepeat, NULL);
				}
			else
				KillTimer(m_hWnd, 100);
			}

		if( wParam == 200 ) {

			if( m_pBeeper ) {

				if( m_uTimer && m_uTimer <= 10 ) {

					m_pBeeper->Beep(beepPress);
					}
				}

			if( m_uTimer && !--m_uTimer ) {

				PostQuitMessage(0);
				}

			SetTimer(m_hWnd, 200, 1000, NULL);
			}
		}

	if( uMessage == WM_CLOSE ) {

		if( !g_Config.m_fConsole ) {
				
			PostQuitMessage(0);
			}

		return 0;
		}

	return DefWindowProc(m_hWnd, uMessage, wParam, lParam);
	}

// IUnknown

HRESULT CDisplay::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDisplay);

	StdQueryInterface(IDisplay);
	StdQueryInterface(ITouchScreen);
	StdQueryInterface(ILeds);

	return E_NOINTERFACE;
	}

ULONG CDisplay::AddRef(void)
{
	StdAddRef();
	}

ULONG CDisplay::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL CDisplay::Open(void)
{
	return TRUE;
	}

// IDisplay

void CDisplay::Claim(void)
{
	m_pLock->Wait(FOREVER);
	}

void CDisplay::Free(void)
{
	m_pLock->Free();
	}

void CDisplay::GetSize(int &cx, int &cy)
{
	cx = m_cx;

	cy = m_cy;
	}

void CDisplay::Update(PCVOID pData)
{
	if( m_pData ) {

		GuardThread(TRUE);

		WaitForSingleObjectEx(m_hEvent, INFINITE, TRUE);

		if( m_pData ) {

			PCDWORD ps = PCDWORD(pData);

			PDWORD  pd = PDWORD (m_pData);

			int     np = m_cx * m_cy;

			memcpy(pd, ps, np * sizeof(DWORD));

			PostMessage(m_hWnd, WM_COMMAND, 100, 0);
			}

		GuardThread(FALSE);
		}
	}

BOOL CDisplay::SetBacklight(UINT pc)
{
//	AfxTrace("display: backlight set to %u\n", pc);

	return FALSE;
	}

UINT CDisplay::GetBacklight(void)
{
	return 100;
	}

BOOL CDisplay::EnableBacklight(BOOL fOn)
{
	return FALSE;
	}

BOOL CDisplay::IsBacklightEnabled(void)
{
	return TRUE;
	}

// ITouchScreen

void CDisplay::GetCellSize(int &xCell, int &yCell)
{
	xCell = cellSize;

	yCell = cellSize;
	}

void CDisplay::GetDispCells(int &xDisp, int &yDisp)
{
	xDisp = m_xCells;

	yDisp = m_yCells;
	}

void CDisplay::SetBeepMode(bool fBeep)
{
	m_fBeep = fBeep;
	}

void CDisplay::SetDragMode(bool fDrag)
{
	}

PCBYTE CDisplay::GetMap(void)
{
	return m_pMap;
	}

void CDisplay::SetMap(PCBYTE pMap)
{
	m_pMap = pMap;
	}

bool CDisplay::GetRaw(int &xRaw, int &yRaw)
{
	xRaw = 0;

	yRaw = 0;

	return false;
	}

// ILeds

void CDisplay::SetLed(UINT uLed, UINT uState)
{
	if( uLed < elements(m_fLed) ) {

		BOOL fState = uState ? TRUE : FALSE;

		if( m_fLed[uLed] != fState ) {

			m_fLed[uLed] = fState;

			PostMessage(m_hWnd, WM_COMMAND, 150, 0);
			}
		}
	}

void CDisplay::SetLedLevel(UINT uPercent, bool fPersist)
{
	}

UINT CDisplay::GetLedLevel(void)
{
	return 100;
	}

// Image Loading

GpImage * CDisplay::CreateImage(void)
{
	HGLOBAL hData = GlobalAlloc(GHND, g_Config.m_pModel->m_nFile);

	PBYTE   pData = PBYTE(GlobalLock(hData));

	memcpy(pData, g_Config.m_pModel->m_pFile, g_Config.m_pModel->m_nFile);

	GlobalUnlock(hData);

	IStream *pStream = NULL;

	CreateStreamOnHGlobal(hData, FALSE, &pStream);

	if( pStream ) {

		GpImage *pImage = NULL;

		GdipLoadImageFromStream(pStream, &pImage);

		pStream->Release();

		GlobalFree(hData);

		return pImage;
		}

	GlobalFree(hData);

	return NULL;
	}

// Implementation

bool CDisplay::StoreEvent(UINT uType)
{
	if( m_pInput ) {

		CInput Input;

		Input.x.i.m_Type   = typeTouch;

		Input.x.i.m_State  = uType;

		Input.x.i.m_Local  = TRUE;

		Input.x.d.t.m_XPos = m_xPress;
	
		Input.x.d.t.m_YPos = m_yPress;
	
		bool fOkay = m_pInput->Store(Input.m_Ref);

		if( uType == stateDown ) {

			if( m_fBeep && m_pBeeper ) {

				UINT uBeep = fOkay ? beepPress : beepFull;

				m_pBeeper->Beep(uBeep);
				}
			}

		return fOkay;
		}

	return false;
	}

bool CDisplay::IsActive(void)
{
	// cppcheck-suppress knownConditionTrueFalse

	if( !IsBacklightEnabled() ) {

		return true;
		}

	if( m_pMap ) {

		UINT n = m_xPress + m_xCells * m_yPress;

		BYTE b = (0x80 >> (n%8));

		return (m_pMap[n/8] & b) ? true : false;
		}

	return false;
	}

// End of File
