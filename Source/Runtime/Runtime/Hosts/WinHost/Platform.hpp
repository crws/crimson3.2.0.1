
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Platform_HPP

#define INCLUDE_Platform_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/PlatformBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Platform Object
//

class CPlatform : public CPlatformBase
{
	public:
		// Constructor
		CPlatform(void);

		// Destructor
		~CPlatform(void);

		// IDevice
		BOOL METHOD Open(void);

		// IPlatform
		PCTXT METHOD GetFamily  (void);
		PCTXT METHOD GetModel   (void);
		PCTXT METHOD GetVariant (void);
		UINT  METHOD GetFeatures(void);
		BOOL  METHOD IsService  (void);
		BOOL  METHOD IsEmulated (void);
		BOOL  METHOD HasMappings(void);
		void  METHOD ResetSystem(void);
		void  METHOD SetWebAddr (PCTXT pAddr);

	protected:
		// Data Members
		IDiskManager   * m_pDiskMan;
		ISdHost        * m_pSdHost;
		IBlockDevice   * m_pBlkDev;
		UINT             m_iDriveC;
		UINT		 m_iHostC;

		// Implementation
		void MakeDisplay(void);
		void FindSerial(void);
		bool BindFilingSystem(void);
		void FreeFilingSystem(void);
	};

// End of File

#endif
