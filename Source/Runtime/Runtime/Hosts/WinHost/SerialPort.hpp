
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_SerialPort_HPP

#define INCLUDE_SerialPort_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SerialBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Serial Port
//

class CSerialPort : public CSerialBase
{
public:
	// Constructor
	CSerialPort(CString Name);

protected:
	// Handlers
	BOOL OnError(void);

	// Port
	BOOL PortOpen(void);
	BOOL PortSetFormat(void);
	UINT PortFindBaud(void);
	BYTE PortFindParity(void);
	BYTE PortFindStop(void);
	void PortSetTimeouts(void);
	void PortSetBuffers(void);
};

// End of File

#endif
