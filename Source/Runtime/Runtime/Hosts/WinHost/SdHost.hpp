
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SdHost_HPP
	
#define	INCLUDE_SdHost_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Memory.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Windows Emulated SD Host
//

class CSdHost : public CMemory, public ISdHost
{	
	public:
		// Constructor
		CSdHost(void);

		// Destructor
		~CSdHost(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ISdHost
		BOOL METHOD IsCardReady(void);
		UINT METHOD GetSectorCount(void);
		UINT METHOD GetSerialNumber(void);
		void METHOD AttachCardEvent(IEvent *pEvent);
		BOOL METHOD WriteSector(UINT uSect, PCBYTE pData);
		BOOL METHOD ReadSector(UINT uSect, PBYTE pData);

	protected:
		// Data Members
		ULONG m_uRefs;
		UINT  m_uSects;
	};

// End of File

#endif
