
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Pointer Validation Implementation
//

// Asserts

#if defined(_DEBUG)

global void AfxAssertReadPtr(PCVOID pData, UINT uSize)
{
	if( !AfxCheckReadPtr(pData, uSize) ) {

		AfxTrace("ERROR: Bad read pointer %8.8X\n\n", pData);

		HostBreak();
		}
	}

global void AfxAssertWritePtr(PVOID pData, UINT uSize)
{
	if( !AfxCheckWritePtr(pData, uSize) ) {

		AfxTrace("ERROR: Bad write pointer %8.8X\n\n", pData);

		HostBreak();
		}
	}

global void AfxAssertStringPtr(PCTXT pText, UINT uSize)
{
	AfxAssertReadPtr(pText, 1);
	}

global void AfxAssertStringPtr(PCUTF pText, UINT uSize)
{
	AfxAssertReadPtr(pText, 2);
	}

#endif

// Checks

global BOOL AfxCheckReadPtr(PCVOID pData, UINT uSize)
{
	return !win32::IsBadReadPtr(pData, uSize);
	}

global BOOL AfxCheckWritePtr(PVOID pData, UINT uSize)
{
	return !win32::IsBadWritePtr(pData, uSize);
	}

global BOOL AfxCheckStringPtr(PCTXT pText, UINT uSize)
{
	return AfxCheckReadPtr(pText, 1);
	}

global BOOL AfxCheckStringPtr(PCUTF pText, UINT uSize)
{
	return AfxCheckReadPtr(pText, 2);
	}

// End of File
