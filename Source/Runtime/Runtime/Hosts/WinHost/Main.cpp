
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ModuleManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Constructors Hook
//

extern BOOL CallCtors(void);

//////////////////////////////////////////////////////////////////////////
//
// System Object Management
//

extern void Init_Malloc(void);

extern void Term_Malloc(void);

extern void Init_RtlSupport(void);

extern void Term_RtlSupport(void);

//////////////////////////////////////////////////////////////////////////
//
// Entry Points
//

// RTL Hooks

clink void __security_init_cookie(void);

// Static Data

static CModuleManager * m_pModMan = NULL;

static bool             m_fTest   = false;

static CStringArray     m_Modules;

// Prototypes

clink	int	Start(void);
global	BOOL	LoadModules(void);
global	void	FreeModules(void);
static	int	HostInit(void);
static  BOOL	GetCommandLine(CStringArray &List);

// Code

clink int Start(void)
{
	__security_init_cookie();

	Init_Malloc();

	Init_RtlSupport();

	CallCtors();

	int r = HostInit();

	Term_RtlSupport();

	Term_Malloc();

	// TODO -- Why is this needed? If we don't use it
	// and we're doing multimedia, we do not kill our
	// pool of worker threads and things hang...

	win32::ExitProcess(r);

	return r;
	}

global BOOL LoadModules(void)
{
	AfxTrace("winhost: loading modules\n");

	m_pModMan = New CModuleManager;

	for( UINT n = 0; n < m_Modules.GetCount(); n++ ) {

		m_pModMan->LoadClientModule(m_Modules[n]);
		}

	AfxTrace("winhost: modules loaded\n");

	return TRUE;
	}

global void FreeModules(void)
{
	delete m_pModMan;

	m_Modules.Empty();
	}

static int HostInit(void)
{
	struct COpt
	{
		PCTXT     pName;
		bool    * pFlag;
		CString * pData;
		};

	static COpt Opts[] = {

		{  "test",     &m_fTest,              NULL		   },
		{  "emulate",  &g_Config.m_fEmulate,  NULL		   },
		{  "blind",    &g_Config.m_fBlind,    NULL		   },
		{  "console",  &g_Config.m_fConsole,  NULL		   },
		{  "frame",    &g_Config.m_fFrame,    NULL		   },
		{  "service",  &g_Config.m_fService,  &g_Config.m_Service  },
		{  "id",       NULL,		      &g_Config.m_Ident    },
		{  "model",    NULL,		      &g_Config.m_Model    },
		{  "caption",  NULL,		      &g_Config.m_Caption  },
		{  "emdata",   NULL,		      &g_Config.m_EmData   },

		};

	CStringArray List;

	if( GetCommandLine(List) ) {

		for( UINT n = 1; n < List.GetCount(); n++ ) {

			CString const &Arg = List[n];

			if( Arg[0] == '-' || Arg[0] == '/' ) {

				CString Data = Arg.Mid(1);

				CString Name = Data.StripToken('=');

				if( !Data.IsEmpty() ) {

					UINT uLen = Data.GetLength();

					if( Data[0] == '"' && Data[uLen-1] == '"' ) {

						Data.Delete(0, 1);

						Data.Delete(uLen-2, 1);
						}
					}

				if( Name == "map" ) {

					if( Data.IsEmpty() ) {

						AfxTrace("winhost: missing data for switch -map\n", Name);

						return -1;
						}

					CStringArray List;

					Data.Tokenize(List, ',');

					for( UINT i = 0; i < List.GetCount(); i++ ) {

						CString Targ = List[i];

						CString Port = Targ.StripToken(':');

						g_Config.m_Map.Insert(Port, Targ);
						}

					continue;
					}

				for( UINT i = 0; i < elements(Opts); i++ ) {

					COpt const &Opt = Opts[i];

					if( Name == Opt.pName || (Name.StartsWith("no") && Name.Mid(2) == Opt.pName) ) {

						if( Opt.pFlag ) {

							*Opt.pFlag = !Name.StartsWith("no");
							}

						if( Opt.pData ) {

							if( Data.IsEmpty() ) {

								if( !Opt.pFlag ) {

									AfxTrace("winhost: missing data for switch -%s\n", Name);

									return -1;
									}
								}
							else
								*Opt.pData = Data;
							}

						break;
						}
					}

				continue;
				}

			m_Modules.Append(Arg);
			}
		}

	if( m_Modules.IsEmpty() ) {

		m_Modules.Append(m_fTest ? "CMTest" : "CM4");
		}

	if( !g_Config.BindModel() ) {

		AfxTrace("winhost: unable to support model %s\n", g_Config.m_Model);

		return -1;
		}

	return AeonHostInit(m_fTest);
	}

static BOOL GetCommandLine(CStringArray &List)
{
	CString Line = win32::GetCommandLineA();

	Line.Tokenize(List, ' ', '"');

	return TRUE;
	}

// End of File
