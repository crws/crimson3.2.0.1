
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite G07 Model Data
//

static BYTE imageG07[] = {

	#include "g07-x1.png.dat"
	0
	};

static CHostKey keysG07[] = {

	{ 344, 634, 404, 694, 128 },
	{ 464, 634, 524, 694, 129 },
	{ 584, 634, 644, 694, 162 }

	};

global CHostModel modelG07 = {

	"Graphite(R)",
	"G07",
	"g07",
	"g07",
	rfGraphite,
	1,
	988,
	758,
	94,
	124,
	800,
	480,
	elements(keysG07),
	keysG07,
	sizeof(imageG07)-1,
	imageG07
	};

// End of File
