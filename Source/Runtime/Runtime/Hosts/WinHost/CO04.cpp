
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Colorado CO04 Model Data
//

static BYTE imageCO04[] = {

	#include "co04-x1.png.dat"
	0
	};

global CHostModel modelCO04 = {

	"",
	"CO04",
	"CR1000-04",
	"CR1000-04000",
	rfColorado,
	1,
	584,
	424,
	52,
	68,
	480,
	272,
	0,
	NULL,
	sizeof(imageCO04)-1,
	imageCO04
	};

// End of File
