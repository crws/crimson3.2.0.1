
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Constructor Calls for Win32
//

// Sections

#pragma section(".CRT$XCA", read)

#pragma section(".CRT$XCZ", read)

// Markers

global DWORD __declspec(allocate(".CRT$XCA")) _xc_a = 0;

global DWORD __declspec(allocate(".CRT$XCZ")) _xc_z = 0;

// Code

BOOL CallCtors(void)
{
	PDWORD p1 = &_xc_a + 1;

	PDWORD p2 = &_xc_z;

	while( p1 < p2 ) {

		void (*pfnFunc)(void) = (void (*)(void)) *p1++;

		(*pfnFunc)();
		}

	return TRUE;
	}

// End of File
