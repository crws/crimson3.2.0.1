
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Colorado CO10ev Model Data
//

static BYTE imageCO10ev[] = {

	#include "co10ev-x1.png.dat"
	0
	};

global CHostModel modelCO10ev = {

	"",
	"CO10ev",
	"CR1000-10",
	"CR1000-10000",
	rfColorado,
	1,
	792,
	704,
	76,
	100,
	640,
	480,
	0,
	NULL,
	sizeof(imageCO10ev)-1,
	imageCO10ev
	};

// End of File
