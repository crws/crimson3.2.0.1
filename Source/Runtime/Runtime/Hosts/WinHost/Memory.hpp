
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Memory_HPP
	
#define	INCLUDE_Memory_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Types
//

using win32::HANDLE;

//////////////////////////////////////////////////////////////////////////
//
// Windows Emulated Memory Device
//

class CMemory
{	
	public:
		// Constructor
		CMemory(void);

		// Destructor
		~CMemory(void);

	protected:
		// Data Members
		CString m_Name;
		UINT    m_uSize;
		BOOL    m_fMap;
		PBYTE	m_pData;
		HANDLE  m_hFile;
		HANDLE  m_hMap;

		// Implementation
		void    AllocData(void);
		void    FreeData(void);
		CString FindPath(void);
	};

// End of File

#endif
