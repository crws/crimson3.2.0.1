
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GDIPLUS_HPP

#define	INCLUDE_GDIPLUS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

struct IDirectDrawSurface7;

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef signed short   INT16;

typedef unsigned short UINT16;

//////////////////////////////////////////////////////////////////////////
//
// Library Files
//

namespace win32
{
	namespace DllExports
	{
		#include "GdiplusMem.h"
		};

	#include "GdiplusBase.h"
	#include "GdiplusEnums.h"
	#include "GdiplusTypes.h"
	#include "GdiplusInit.h"
	#include "GdiplusPixelFormats.h"
	#include "GdiplusColor.h"
	#include "GdiplusMetaHeader.h"
	#include "GdiplusImaging.h"
	#include "GdiplusColorMatrix.h"
	#include "GdiplusGpStubs.h"
	#include "GdiplusHeaders.h"

	namespace DllExports
	{
		#include "GdiplusFlat.h"
		};
	};

//////////////////////////////////////////////////////////////////////////
//
// Namespace Selection
//

using namespace win32::DllExports;

//////////////////////////////////////////////////////////////////////////
//
// Library Files
//

#pragma	comment(lib, "gdiplus.lib")

//////////////////////////////////////////////////////////////////////////
//
// Initialization
//

extern void GdiPlusInit(void);

extern void GdiPlusTerm(void);

// End of File

#endif
