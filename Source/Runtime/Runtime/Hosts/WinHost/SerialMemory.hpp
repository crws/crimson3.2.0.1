
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SerialMemory_HPP
	
#define	INCLUDE_SerialMemory_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Memory.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Windows Emulated Serial Memory
//

class CSerialMemory : public CMemory, public ISerialMemory
{	
	public:
		// Constructor
		CSerialMemory(void);

		// Destructor
		~CSerialMemory(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ISerialMemory
		UINT METHOD GetSize(void);
		BOOL METHOD IsFast(void);
		BOOL METHOD GetData(UINT uAddr, PBYTE  pData, UINT uCount);
		BOOL METHOD PutData(UINT uAddr, PCBYTE pData, UINT uCount);

	protected:
		// Data Members
		ULONG m_uRefs;
	};

// End of File

#endif
