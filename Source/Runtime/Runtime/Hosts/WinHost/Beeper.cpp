
#include "Intern.hpp"

#include "Beeper.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Windows API
//

// Libraries

#pragma comment(lib, "winmm.lib")

// Imported Types

using win32::WAVEFORMATEX;
using win32::WAVEHDR;

// Imported APIs

using win32::CreateEvent;
using win32::SetEvent;
using win32::WaitForSingleObjectEx;
using win32::WaitForMultipleObjectsEx;
using win32::CloseHandle;
using win32::waveOutOpen;
using win32::waveOutPrepareHeader;
using win32::waveOutWrite;
using win32::waveOutReset;
using win32::waveOutUnprepareHeader;
using win32::waveOutClose;

//////////////////////////////////////////////////////////////////////////
//
// Beeper Object
//

// Instantiator

IDevice * Create_Beeper(void)
{
	return (IBeeper *) New CBeeper;
	}

// Constructor

CBeeper::CBeeper(void)
{
	StdSetRef();

	m_hThread = NULL;

	m_uSamp   = 44100;

	m_hOut    = NULL;
	}

// Destructor

CBeeper::~CBeeper(void)
{
	if( m_hThread ) {

		SetEvent(m_hExit);

		WaitThread(m_hThread, INFINITE);

		CloseThread(m_hThread);

		CloseHandle(m_hPlay);
		CloseHandle(m_hStop);
		CloseHandle(m_hExit);
		CloseHandle(m_hBuff);
		CloseHandle(m_hDone);

		waveOutClose(m_hOut);
		}
	}

// IUnknown

HRESULT CBeeper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IBeeper);

	return E_NOINTERFACE;
	}

ULONG CBeeper::AddRef(void)
{
	StdAddRef();
	}

ULONG CBeeper::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL CBeeper::Open(void)
{
	m_hBuff = CreateEvent(NULL, FALSE, FALSE, NULL);

	WAVEFORMATEX Format = {0};

	Format.wFormatTag      = WAVE_FORMAT_PCM;
	Format.nChannels       = 1;
	Format.nSamplesPerSec  = m_uSamp;
	Format.wBitsPerSample  = 16;
	Format.nBlockAlign     = Format.nChannels   * (Format.wBitsPerSample >> 3);
	Format.nAvgBytesPerSec = Format.nBlockAlign * Format.nSamplesPerSec;

	waveOutOpen(&m_hOut, WAVE_MAPPER, &Format, DWORD(m_hBuff), NULL, CALLBACK_EVENT);

	if( m_hOut ) {

		m_hPlay   = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_hStop   = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_hExit   = CreateEvent(NULL, FALSE, FALSE, NULL);
		m_hDone   = CreateEvent(NULL, FALSE, FALSE, NULL);

		m_hThread = CreateClientThread(this, 1, THREAD_REALTIME);

		return TRUE;
		}

	CloseHandle(m_hBuff);

	return FALSE;
	}

// IBeeper

void CBeeper::Beep(UINT uBeep)
{
	switch( uBeep ) {

		case beepStart:

			Beep(72, 200);

			return;

		case beepPress:

			Beep(72, 50);

			return;
		
		case beepFull:

			Beep(48, 500);

			return;
		}

	BeepOff();
	}

void CBeeper::Beep(UINT uNote, UINT uTime)
{
	if( uTime ) {

		// TODO -- Tune this to match Crimson!

		static UINT uTable[] = { 14080,
					 14917,
					 15804,
					 16744,
					 17740,
					 18795,
					 19912,
					 21096,
					 22351,
					 23680,
					 25088,
					 26580
					 };

		BeepOff();

		WaitForSingleObjectEx(m_hDone, INFINITE, TRUE);

		m_uFreq = uTable[uNote % 12] >> (10 - (uNote / 12));

		m_uTime = uTime;

		SetEvent(m_hPlay);
		}
	}

void CBeeper::BeepOff(void)
{
	SetEvent(m_hStop);
	}

// IClientProcess

BOOL CBeeper::TaskInit(UINT uTask)
{
	return TRUE;
	}

INT CBeeper::TaskExec(UINT uTask)
{
	for(;;) {

		SetEvent(m_hDone);

		HANDLE h[] = { m_hPlay, m_hStop, m_hExit };

		UINT uWait = WaitForMultipleObjectsEx(elements(h), h, FALSE, INFINITE, TRUE);

		if( uWait == WAIT_OBJECT_0 + 0 ) {

			UINT   uSize = m_uSamp * m_uTime / 1000;

			UINT   uBuff = ((uSize + 4095) >> 12) << 12;

			PSHORT pData = New short [ uBuff ];

			double pi    = acos(-1);

			for( UINT n = 0; n < uBuff; n++ ) {

				if( n < uSize ) {

					double t = (n * 2 * pi / m_uSamp * m_uFreq);

					pData[n] = short(4096 * sin(t));

					continue;
					}

				pData[n] = 0;
				}

			WAVEHDR Header = {0};

			Header.dwBufferLength = uBuff;

			Header.lpData         = (char *) pData;

			waveOutPrepareHeader(m_hOut, &Header, sizeof(WAVEHDR));

			waveOutWrite(m_hOut, &Header, sizeof(WAVEHDR));

			for(;;) {

				HANDLE h[] = { m_hBuff, m_hStop };

				UINT uWait = WaitForMultipleObjectsEx(elements(h), h, FALSE, INFINITE, TRUE);

				if( uWait == WAIT_OBJECT_0 + 0 ) {

					if( Header.dwFlags & WHDR_DONE ) {

						break;
						}
					}

				if( uWait == WAIT_OBJECT_0 + 1 ) {

					waveOutReset(m_hOut);

					break;
					}
				}

			waveOutUnprepareHeader(m_hOut, &Header, sizeof(WAVEHDR));

			delete [] pData;
			}
	
		if( uWait == WAIT_OBJECT_0 + 2 ) {

			break;
			}
		}

	return 0;
	}

void CBeeper::TaskStop(UINT uTask)
{
	}

void CBeeper::TaskTerm(UINT uTask)
{
	}

// End of File
