
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NativeRawSocket_HPP

#define INCLUDE_NativeRawSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Window APIs
//

AfxNamespaceBegin(win32);

#include <ws2tcpip.h>

#include <iphlpapi.h>

AfxNamespaceEnd(win32);

//////////////////////////////////////////////////////////////////////////
//
// PCAP API
//

#include "pcap/include/pcap.h"

//////////////////////////////////////////////////////////////////////////
//
// Raw Socket Object
//

class CNativeRawSocket : public ISocket, public IClientProcess
{
	public:
		// Constructor
		CNativeRawSocket(void);

		// Destructor
		~CNativeRawSocket(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ISocket
		HRM Listen(WORD Loc);
		HRM Listen(IPADDR const &IP, WORD Loc);
		HRM Connect(IPADDR const &IP, WORD Rem);
		HRM Connect(IPADDR const &IP, WORD Rem, WORD Loc);
		HRM Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc);
		HRM Recv(PBYTE pData, UINT &uSize, UINT uTime);
		HRM Recv(PBYTE pData, UINT &uSize);
		HRM Send(PBYTE pData, UINT &uSize);
		HRM Recv(CBuffer * &pBuff, UINT uTime);
		HRM Recv(CBuffer * &pBuff);
		HRM Send(CBuffer   *pBuff);
		HRM GetLocal (IPADDR &IP);
		HRM GetRemote(IPADDR &IP);
		HRM GetLocal (IPADDR &IP, WORD &Port);
		HRM GetRemote(IPADDR &IP, WORD &Port);
		HRM GetPhase(UINT &Phase);
		HRM SetOption(UINT uOption, UINT uValue);
		HRM Abort(void);
		HRM Close(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// Interface Location
		static void FindInterfaces(void);

	protected:
		// Interface Data
		struct CFace
		{
			CString	         m_Adapter;
			CMacAddr	 m_MacAddr;
			CArray <CIpAddr> m_IpAddrs;
			};

		// Interface List
		static CArray <CFace *> m_Faces;

		// Data Members
		ULONG	          m_uRefs;
		IMutex          * m_pLock;
		char	          m_sError[256];
		CArray <HTHREAD	> m_Threads;
		CArray <pcap_t *> m_Ports;
		UINT		  m_uAddress;
		UINT		  m_uRxLimit;
		UINT              m_uRxCount;
		CBuffer         * m_pRxHead;
		CBuffer         * m_pRxTail;

		// Implementation
		void OnPacket(PCBYTE pData, UINT uData);
		void ResetParams(void);

		// Packet Handler
		static void PacketProc(PBYTE pParam, pcap_pkthdr const *pInfo, PCBYTE pData);
	};

// End of File

#endif
