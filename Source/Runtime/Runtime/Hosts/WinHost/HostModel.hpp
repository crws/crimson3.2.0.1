
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_HostModel_HPP

#define INCLUDE_HostModel_HPP

//////////////////////////////////////////////////////////////////////////
//
// Host Key Definition
//

struct CHostKey
{
	int	m_x1;
	int	m_y1;
	int	m_x2;
	int	m_y2;
	int	m_vk;
	};

//////////////////////////////////////////////////////////////////////////
//
// Model Definition
//

struct CHostModel
{
	char	 const * m_pFamily;
	char     const * m_pName;
	char     const * m_pModel;
	char     const * m_pVariant;
	UINT             m_uFeatures;
	int		 m_nScale;
	int		 m_cxFrame;
	int		 m_cyFrame;
	int		 m_xpImage;
	int		 m_ypImage;
	int		 m_cxImage;
	int		 m_cyImage;
	int		 m_nKey;
	CHostKey const * m_pKey;
	int		 m_nFile;
	BYTE     const * m_pFile;
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphite Hardware Features
//

#define	rfGraphite    ( 1 * rfGraphiteModules |\
			0 * rfSerialModules   |\
			1 * rfDisplay         )

//////////////////////////////////////////////////////////////////////////
//
// Canyon Hardware Features
//

#define	rfCanyon      ( 1 * rfGraphiteModules |\
			0 * rfSerialModules   |\
			1 * rfDisplay         )

//////////////////////////////////////////////////////////////////////////
//
// Colorado Hardware Features
//

#define	rfColorado    ( 0 * rfGraphiteModules |\
			0 * rfSerialModules   |\
			1 * rfDisplay         )

// End of File

#endif
