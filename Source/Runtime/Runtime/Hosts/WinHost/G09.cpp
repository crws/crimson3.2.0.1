
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite G09 Model Data
//

static BYTE imageG09[] = {

	#include "g09-x1.png.dat"
	0
	};

static CHostKey keysG09[] = {

	{ 344, 634, 404, 694, 128 },
	{ 464, 634, 524, 694, 129 },
	{ 584, 634, 644, 694, 162 }

	};

global CHostModel modelG09 = {

	"Graphite(R)",
	"G09",
	"g09",
	"g09",
	rfGraphite,
	1,
	988,
	758,
	94,
	124,
	800,
	480,
	elements(keysG09),
	keysG09,
	sizeof(imageG09)-1,
	imageG09
	};

// End of File
