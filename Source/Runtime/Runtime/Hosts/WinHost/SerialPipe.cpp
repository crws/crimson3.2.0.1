
#include "Intern.hpp"

#include "SerialPipe.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Serial Pipe
//

// Instantiator

IDevice * Create_Pipe(UINT uInst)
{
	if( uInst == 0 ) {

		CString Name;

		Name = "pipe\\c32-program";

		if( !g_Config.m_Ident.IsEmpty() ) {

			Name += '-';

			Name += g_Config.m_Ident;
		}

		return New CSerialPipe(Name);
	}

	return NULL;
}

// Constructor

CSerialPipe::CSerialPipe(CString Name) : CSerialBase(Name)
{
}

// Handlers

BOOL CSerialPipe::OnConn(void)
{
	if( !win32::ConnectNamedPipe(m_hPort, &m_oConn) ) {

		if( win32::GetLastError() == ERROR_PIPE_CONNECTED ) {

			m_uPipe = ToTicks(10000);

			return TRUE;
		}

		if( win32::GetLastError() == ERROR_IO_PENDING ) {

			for(;;) {

				HANDLE hObjects[3] = { m_hTimer, m_hConn, m_hTerm };

				UINT uDelay  = INFINITE;

				UINT uObject = win32::WaitForMultipleObjectsEx( elements(hObjects),
									        hObjects,
									        FALSE,
									        uDelay,
									        TRUE
										);

				EnterGuarded();

				switch( uObject ) {

					case WAIT_OBJECT_0 + 0:

						OnTimer();

						break;

					case WAIT_OBJECT_0 + 1:

						m_uPipe = ToTicks(10000);

						LeaveGuarded();

						return TRUE;

					case WAIT_OBJECT_0 + 2:

						// TODO -- Not a clean way to signal this...

						OnTerm();

						LeaveGuarded();

						return TRUE;
				}

				LeaveGuarded();
			}
		}
	}

	Sleep(1000);

	return FALSE;
}

BOOL CSerialPipe::OnTest(void)
{
	if( !--m_uPipe ) {

		return FALSE;
	}

	return TRUE;
}

void CSerialPipe::OnDone(void)
{
	m_uPipe = ToTime(10000);
}

void CSerialPipe::OnBreak(void)
{
	win32::DisconnectNamedPipe(m_hPort);
}

BOOL CSerialPipe::OnError(void)
{
	if( win32::GetLastError() == ERROR_BROKEN_PIPE ) {

		// TODO -- This is kindof naff...

		m_uPipe = 1;

		return TRUE;
	}

	return FALSE;
}

// Implementation

BOOL CSerialPipe::PortOpen(void)
{
	win32::WCHAR sName[64] = { 0 };

	wstrcat(sName, "\\\\.\\");

	wstrcat(sName, m_Name);

	m_hPort = win32::CreateNamedPipe( sName,
					  PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED,
					  PIPE_TYPE_BYTE | PIPE_READMODE_BYTE,
					  10,
					  65536,
					  65536,
					  100,
					  NULL
					  );

	if( m_hPort != INVALID_HANDLE_VALUE ) {

		return TRUE;
	}

	return FALSE;
}

// End of File
