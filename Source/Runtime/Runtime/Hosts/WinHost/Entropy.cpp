
#include "Intern.hpp"

#include "Entropy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Windows Cryptography
//

AfxNamespaceBegin(win32);

#include <bcrypt.h>

#pragma  comment(lib, "bcrypt.lib")

AfxNamespaceEnd(win32);

//////////////////////////////////////////////////////////////////////////
//
// Entropy Source
//

// Instantiator

IDevice * Create_Entropy(void)
{
	return New CEntropy;
	}

// Constructor

CEntropy::CEntropy(void)
{
	StdSetRef();
	}

// Destructor

CEntropy::~CEntropy(void)
{
	}

// IUnknown

HRESULT CEntropy::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IEntropy);

	return E_NOINTERFACE;
	}

ULONG CEntropy::AddRef(void)
{
	StdAddRef();
	}

ULONG CEntropy::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL CEntropy::Open(void)
{
	return TRUE;
	}

// IEntropy

BOOL CEntropy::GetEntropy(PBYTE pData, UINT uData)
{
	win32::BCryptGenRandom(NULL, pData, uData, BCRYPT_USE_SYSTEM_PREFERRED_RNG);

	return TRUE;
	}

// End of File
