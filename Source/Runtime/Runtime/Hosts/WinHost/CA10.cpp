
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Canyon CA10 Model Data
//

static BYTE imageCA10[] = {

	#include "ca10-x1.png.dat"
	0
	};

global CHostModel modelCA10 = {

	"",
	"CA10",
	"CR3000-10",
	"CR3000-10000",
	rfCanyon,
	1,
	988,
	878,
	94,
	124,
	800,
	600,
	0,
	NULL,
	sizeof(imageCA10)-1,
	imageCA10
	};

// End of File
