
#include "Intern.hpp"

#include "PCapNic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Libraries
//

#pragma comment(lib, "Ws2_32.lib")

#pragma comment(lib, "wpcap.lib")

#pragma comment(lib, "Iphlpapi.lib")

//////////////////////////////////////////////////////////////////////////
//
// PCAP-Based NIC Driver
//

// Static Data

CArray <CPCapNic::CFace *> CPCapNic::m_Faces;

// Instantiator

IDevice * Create_PCapNic(UINT uInst)
{
	if( uInst < 2 ) {

		CPrintf Port   = CPrintf("nic%u", uInst);

		CString Target = g_Config.m_Map[Port];

		UINT    uFace  = Target.IsEmpty() ? NOTHING : atoi(Target) - 1;

		return New CPCapNic(uInst, uFace);
		}

	return NULL;
	}

// Constructor

CPCapNic::CPCapNic(UINT uInst, UINT uFace)
{
	StdSetRef();
	
	m_uInst    = uInst;

	m_uFace    = uFace;

	m_fOnline  = TRUE;

	m_pLock    = Create_Qutex();

	m_pFlag    = Create_Semaphore();
		   
	m_hPort    = NULL;
		   
	m_hThread  = NULL;
		   
	m_uRxLimit = 32;
		   
	m_uRxCount = 0;
		   
	m_pRxHead  = NULL;
		   
	m_pRxTail  = NULL;

	m_uMulti   = 0;

	m_pMulti   = NULL;

	FindFace();

	DiagRegister();
	}

// Destructor

CPCapNic::~CPCapNic(void)
{
	DiagRevoke();

	delete [] m_pMulti;

	AfxRelease(m_pFlag);

	AfxRelease(m_pLock);
	}

// IUnknown

HRESULT CPCapNic::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);
	StdQueryInterface(INic);
	StdQueryInterface(IClientProcess);
	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CPCapNic::AddRef(void)
{
	StdAddRef();
	}

ULONG CPCapNic::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL CPCapNic::Open(void)
{
	return TRUE;
	}

// INic

bool CPCapNic::Open(bool fFast, bool fFull)
{
	if( m_pFace ) {

		pcap_t *hPort = pcap_open_live(m_pFace->m_Adapter, 4096, true, 0, m_sError);

		if( hPort >= 0 ) {

			pcap_setnonblock(hPort, 1, m_sError);

			if( LoadFilter(hPort, m_uMulti ? TRUE : FALSE) ) {

				m_hPort   = hPort;

				m_hThread = CreateClientThread(this, 0, 81000);

			//	AfxTrace("pcapnic: opened %s as interface nic%u\n", m_pFace->m_Adapter, m_uInst);

				return true;
				}

			pcap_close(hPort);
			}
		}

	return false;
	}

bool CPCapNic::Close(void)
{
	if( m_hThread ) {

		DestroyThread(m_hThread);

		while( m_pRxHead ) {

			m_pRxHead->Release();

			AfxListRemove(m_pRxHead, m_pRxTail, m_pRxHead, m_pNext, m_pPrev);
			}
		}

	return true;
	}

bool CPCapNic::InitMac(MACADDR const &Addr)
{
	return true;
	}

void CPCapNic::ReadMac(MACADDR &Addr)
{
	Addr = m_Mac;
	}

UINT CPCapNic::GetCapabilities(void)
{
	return 0;
	}

void CPCapNic::SetFlags(UINT uFlags)
{
	}

bool CPCapNic::SetMulticast(MACADDR const *pList, UINT uList)
{
	if( m_pFace ) {

		m_pLock->Wait(FOREVER);

		if( pList && uList) {

			if( !m_uMulti ) {

				LoadFilter(m_hPort, TRUE);
				}

			if( uList != m_uMulti ) {

				delete [] m_pMulti;

				m_pMulti = New CMacAddr [ uList ];
				}

			memcpy(m_pMulti, pList, sizeof(MACADDR) * uList);

			m_uMulti = uList;
			}
		else {
			if( m_uMulti ) {

				LoadFilter(m_hPort, FALSE);

				delete [] m_pMulti;

				m_pMulti = NULL;

				m_uMulti = 0;
				}
			}

		m_pLock->Free();

		return true;
		}

	return false;
	}

bool CPCapNic::IsLinkActive(void)
{
	return m_fOnline ? true : false;
	}

bool CPCapNic::WaitLink(UINT uTime)
{
	if( uTime < NOTHING ) {

		SetTimer(uTime);
		}

	while( uTime == NOTHING || GetTimer() ) {

		if( m_fOnline ) {

			return true;
			}

		Sleep(10);
		}

	return false;
	}

bool CPCapNic::SendData(CBuffer *pBuff, UINT uTime)
{
	PBYTE pData = pBuff->GetData();

	UINT  uData = pBuff->GetSize();

	(MACADDR &) pData[6] = m_Mac;

	return pcap_sendpacket(m_hPort, pData, uData) == 0;
	}

bool CPCapNic::ReadData(CBuffer * &pBuff, UINT uTime)
{
	if( m_pFlag->Wait(uTime) ) {

		if( m_fOnline ) {

			m_pLock->Wait(FOREVER);

			if( m_pRxHead ) {

				pBuff = m_pRxHead;

				AfxListRemove(m_pRxHead, m_pRxTail, m_pRxHead, m_pNext, m_pPrev);

				m_uRxCount--;

				m_pLock->Free();

				return true;
				}

			AfxAssert(FALSE);

			m_pLock->Free();
			}
		}

	return false;
	}

void CPCapNic::GetCounters(NICDIAG &Diag)
{
	memset(&Diag, 0, sizeof(Diag));
	}

void CPCapNic::ResetCounters(void)
{
	}

// IClientProcess

BOOL CPCapNic::TaskInit(UINT uTask)
{
	return TRUE;
	}

INT CPCapNic::TaskExec(UINT uTask)
{
	for(;;) {

		if( !pcap_dispatch(m_hPort, 1, PacketProc, PBYTE(this)) ) {
			
			Sleep(10);
			}
		}

	return 0;
	}

void CPCapNic::TaskStop(UINT uTask)
{
	}

void CPCapNic::TaskTerm(UINT uTask)
{
	pcap_close(m_hPort);
	}

// IDiagProvider

UINT CPCapNic::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1: return DiagStatus    (pOut, pCmd);
		case 2: return DiagConnect   (pOut, pCmd);
		case 3: return DiagDisconnect(pOut, pCmd);
		}

	return 0;
	}

// Interface Location

UINT CPCapNic::FindInterfaces(void)
{
	if( m_Faces.IsEmpty() ) {

		DWORD		       dwSize = 65536;

		win32::IP_ADAPTER_INFO *pData = (win32::IP_ADAPTER_INFO *) malloc(dwSize);

		if( win32::GetAdaptersInfo(pData, &dwSize) == ERROR_SUCCESS ) {

			win32::IP_ADAPTER_INFO *pWalk = pData;

			while( pWalk ) {

				if( pWalk->Type == MIB_IF_TYPE_ETHERNET ) {

					if( pWalk->AddressLength == 6 ) {

						// Build a list of all Ethernet ports fitted
						// to the system, including their addresses.

						CFace * pFace    = New CFace;

						pFace->m_Adapter = CString("\\Device\\NPF_") + pWalk->AdapterName;

						pFace->m_MacAddr = pWalk->Address;

						for( win32::IP_ADDR_STRING *p = &pWalk->IpAddressList; p; p = p->Next ) {

							pFace->m_IpAddrs.Append(CIpAddr(p->IpAddress.String));
							}

						m_Faces.Append(pFace);
						}
					else
						m_Faces.Append(NULL);
					}
				else
					m_Faces.Append(NULL);

				pWalk = pWalk->Next;
				}
			}

		free(pData);
		}

	return m_Faces.GetCount();
	}

// Implementation

bool CPCapNic::LoadFilter(pcap_t *hPort, BOOL fMulti)
{
	bpf_program Program;

	CString     Filter;

	Filter.Printf( "(%sether dst %s or ether dst FF:FF:FF:FF:FF:FF) and !(ether src %s)",
			fMulti ? "ether multicast or " : "",
			PCTXT(m_Mac.GetAsText()),
			PCTXT(m_Mac.GetAsText())
			);

	if( pcap_compile(hPort, &Program, PTXT(PCTXT(Filter)), 1, 0) >= 0 ) {

		pcap_setfilter  (hPort, &Program);

		pcap_freecode   (&Program);

		return true;
		}

	return false;
	}

void CPCapNic::OnPacket(PCBYTE pData, UINT uData)
{
	m_pLock->Wait(FOREVER);

	if( m_uRxCount < m_uRxLimit ) {

		CMacAddr const &Dest = (CMacAddr const &) pData[0];

		CMacAddr const &From = (CMacAddr const &) pData[6];

		bool           fDrop = true;

		if( Dest.IsMulticast() ) {

			for( UINT n = 0; n < m_uMulti; n++ ) {

				if( Dest == m_pMulti[n] ) {

					fDrop = false;

					break;
					}
				}
			}
		else
			fDrop = false;

		if( !fDrop ) {

			CBuffer *pBuff = BuffAllocate(0);

			memcpy(pBuff->AddTail(uData), pData, uData);

			if( From == m_pFace->m_MacAddr ) {

				// The packet came from the local MAC address, so
				// it is quite likely to have an incomplete checksum
				// as the Windows NIC driver will be off-loading the
				// checksum generation to the hardware.

				pBuff->SetFlag(packetSumsValid);
				}

			AfxListAppend(m_pRxHead, m_pRxTail, pBuff, m_pNext, m_pPrev);

			m_uRxCount++;

			m_pFlag->Signal(1);
			}
		}

	m_pLock->Free();
	}

bool CPCapNic::FindFace(void)
{
	if( m_uFace < CPCapNic::FindInterfaces() ) {

		m_pFace = m_Faces[m_uFace];
		   
		m_Mac   = m_pFace->m_MacAddr;

		FlipMac();

		return true;
		}

	m_pFace = NULL;

	return false;
	}

void CPCapNic::FlipMac(void)
{
	// We have to generate a unique MAC address that we
	// can use with this port. This isn't the best way of
	// doing it, but it will work for now...

	for( UINT n = 1; n < elements(m_Mac.m_Addr); n++ ) {

		m_Mac.m_Addr[n] ^= 0xFF;
		}
	}

// Diagnostics

BOOL CPCapNic::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, CPrintf("nic%u", m_uInst));

		pDiag->RegisterCommand(m_uProv, 1, "status");
		
		pDiag->RegisterCommand(m_uProv, 2, "connect");
		
		pDiag->RegisterCommand(m_uProv, 3, "disconnect");

		pDiag->Release();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPCapNic::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return TRUE;
		}

	return FALSE;
	}

UINT CPCapNic::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->Print("nic%u: %s\n", m_uInst, m_fOnline ? "connected" : "disconnected");

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CPCapNic::DiagConnect(IDiagOutput *pOut, IDiagCommand *pCmd)
{	
	if( !pCmd->GetArgCount() ) {

		m_fOnline = TRUE;

		pOut->Print("nic%u: connected\n", m_uInst);

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CPCapNic::DiagDisconnect(IDiagOutput *pOut, IDiagCommand *pCmd)
{	
	if( !pCmd->GetArgCount() ) {

		m_fOnline = FALSE;

		pOut->Print("nic%u: disconnected\n", m_uInst);

		m_pFlag->Signal(1);

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

// Packet Handler

void CPCapNic::PacketProc(PBYTE pParam, pcap_pkthdr const *pInfo, PCBYTE pData)
{
	((CPCapNic *) pParam)->OnPacket(pData, pInfo->len);
	}

// End of File
