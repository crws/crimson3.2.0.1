
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite G10 Model Data
//

static BYTE imageG10[] = {

	#include "g10-x1.png.dat"
	0
	};

static CHostKey keysG10[] = {

	{ 276, 604, 324, 652, 128 },
	{ 372, 604, 420, 652, 129 },
	{ 468, 604, 516, 652, 162 }

	};

global CHostModel modelG10 = {

	"Graphite(R)",
	"G10",
	"g10",
	"g10",
	rfGraphite,
	1,
	792,
	704,
	76,
	100,
	640,
	480,
	elements(keysG10),
	keysG10,
	sizeof(imageG10)-1,
	imageG10
	};

// End of File
