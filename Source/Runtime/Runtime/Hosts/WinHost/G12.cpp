
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite G12 Model Data
//

static BYTE imageG12[] = {

	#include "g12-x1.png.dat"
	0
	};

static CHostKey keysG12[] = {

	{ 596, 924, 644, 972, 128 },
	{ 692, 924, 740, 972, 129 },
	{ 788, 924, 836, 972, 162 }

	};

global CHostModel modelG12 = {

	"Graphite(R)",
	"G12",
	"g12",
	"g12",
	rfGraphite,
	1,
	1432,
	1024,
	76,
	100,
	1280,
	800,
	elements(keysG12),
	keysG12,
	sizeof(imageG12)-1,
	imageG12
	};

// End of File
