
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NativeDnsResolver_HPP

#define INCLUDE_NativeDnsResolver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Window APIs
//

AfxNamespaceBegin(win32);

#include <ws2tcpip.h>

AfxNamespaceEnd(win32);

//////////////////////////////////////////////////////////////////////////
//
// DNS Resolver
//

class CNativeDnsResolver : public IDnsResolver
{
	public:
		// Constructor
		CNativeDnsResolver(void);

		// Destructor
		~CNativeDnsResolver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDnsResolver
		CIpAddr METHOD Resolve(PCTXT pName);
		BOOL    METHOD Resolve(CArray <CIpAddr> &List, PCTXT pName);

	protected:
		// Data Members
		ULONG m_uRefs;
	};

// End of File

#endif
