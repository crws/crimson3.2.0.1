
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// System Call Implementation
//

// Data

static INT64 m_offset = 0;

// Code

static INT64 GetMicroSeconds(void)
{
	static win32::FILETIME base = { 0 };

	if( !base.dwHighDateTime ) {

		// Find the base of the Unix epoch so we can
		// convert Windows FILETIMEs into Unix time.

		win32::SYSTEMTIME time = { 0 };

		time.wYear  = 1970;
		time.wMonth = 1;
		time.wDay   = 1;

		win32::SystemTimeToFileTime(&time, &base);
		}

	win32::FILETIME time;

	win32::GetSystemTimeAsFileTime(&time);

	INT64 tn = time.dwLowDateTime + (INT64(time.dwHighDateTime) << 32);

	INT64 te = base.dwLowDateTime + (INT64(base.dwHighDateTime) << 32);

	return (tn - te) / 10LL;
	}

clink int gettimeofday(struct timeval *pt, void const *tz)
{
	INT64 tu = GetMicroSeconds() + m_offset;

	pt->tv_sec  = DWORD(tu / 1000000LL);

	pt->tv_usec = DWORD(tu % 1000000LL);

	return 0;
	}

clink int settimeofday(struct timeval const *pt, void const *tz)
{
	INT64 tu = GetMicroSeconds();

	INT64 ts = 0;
	
	ts += pt->tv_sec * 1000000LL;

	ts += pt->tv_usec;

	m_offset = ts - tu;

	return 0;
	}

clink time_t getmonosecs(void)
{
	return time_t(win32::GetTickCount64() / 1000);
}

// End of File
