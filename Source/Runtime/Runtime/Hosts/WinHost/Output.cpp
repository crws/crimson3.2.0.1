
#include "Intern.hpp"

#include "Output.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Output Window Object
//

// Constructor

COutput::COutput(void)
{
	m_hWnd     = NULL;

	m_hSema    = CreateSemaphore(NULL, 250, 250, NULL);

	m_nWheel   = 0;

	m_nTop     = 0;

	m_fCapture = FALSE;

	m_fSelect  = FALSE;
	}

// Destructor

COutput::~COutput(void)
{
	CloseHandle(m_hSema);
	}

// Operations

void COutput::Bind(HWND hWnd)
{
	m_hWnd = hWnd;
	}

void COutput::SetFont(HFONT hFont)
{
	m_hFont = hFont;

	HDC   hDC  = GetDC(m_hWnd);

	HFONT hOld = SelectObject(hDC, m_hFont);

	SIZE s;

	GetTextExtentPoint(hDC, L"X", 1, &s);

	m_xFont = s.cx + 0;

	m_yFont = s.cy + 2;

	SelectObject(hDC, hOld);

	ReleaseDC(m_hWnd, hDC);
	}

void COutput::Clear(void)
{
	m_fSelect = FALSE;

	m_nWheel  = 0;

	m_nTop    = 0;

	m_Text.Empty();

	InvalidateRect(m_hWnd, NULL, FALSE);

	UpdateScrollBar();
	}

void COutput::Sync(void)
{
	int nLast  = int(m_Text.GetCount() - 1);

	KeepInView(nLast);

	m_nWheel = 0;
	}

void COutput::AddLine(CString Text, BOOL fSend)
{
	if( IsWindow(m_hWnd) ) {

		PTXT pText = strdup(Text);

		if( fSend ) {

			SendMessage(m_hWnd, WM_COMMAND, 100, LPARAM(pText));
			}
		else {
			WaitForSingleObject(m_hSema, INFINITE);

			PostMessage(m_hWnd, WM_COMMAND, 101, LPARAM(pText));
			}

		return;
		}

	AfxDoNotTrack(TRUE);

	m_Text.Append(Text);

	AfxDoNotTrack(FALSE);
	}

LRESULT COutput::WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	// TODO -- Take focus and repsond to keys?

	if( uMessage == WM_CREATE ) {

		SCROLLINFO si;

		si.cbSize = sizeof(si);
		si.fMask  = SIF_DISABLENOSCROLL | SIF_RANGE;
		si.nMin   = 0;
		si.nMax   = 0;

		SetScrollInfo(m_hWnd, SB_VERT, &si, TRUE);
		}

	if( uMessage == WM_CREATE || uMessage == WM_SIZE ) {

		RECT Rect;

		GetClientRect(m_hWnd, &Rect);

		m_cx    = Rect.right - Rect.left;

		m_cy    = Rect.bottom - Rect.top;

		m_nPage = m_cy / m_yFont;
		}

	if( uMessage == WM_SIZE ) {

		int nLine = m_Text.GetCount() - 1;

		KeepInView(nLine);

		m_nWheel = 0;
		}

	if( uMessage == WM_COMMAND ) {
		
		if( wParam == 100 || wParam == 101 ) {

			AfxDoNotTrack(TRUE);

			PTXT pText = PTXT(lParam);

			int  nLine = m_Text.Append(pText);

			AfxDoNotTrack(FALSE);

			if( *pText != '>' && nLine > m_nTop + m_nPage ) {

				UpdateScrollBar();
				}
			else {
				if( !KeepInView(nLine) ) {

					InvalidateLine(nLine);
					}
				}

			free(pText);

			m_nWheel = 0;
			}

		if( wParam == 100 || wParam == 101 ) {

			UpdateWindow(m_hWnd);
			}

		if( wParam == 101 ) {

			ReleaseSemaphore(m_hSema, 1, NULL);
			}

		if( wParam == 200 ) {

			CopySelection();
			}

		if( wParam == 201 ) {

			SelectAll();
			}
		}

	if( uMessage == WM_MOUSEWHEEL ) {

		if( (m_nWheel += SHORT(HIWORD(wParam))) ) {

			int const nCount = 2;

			int const nLimit = WHEEL_DELTA / nCount;

			while( m_nWheel > +nLimit ) {

				Scroll(m_nTop - 1);

				m_nWheel -= nLimit;
				}

			while( m_nWheel < -nLimit ) {

				Scroll(m_nTop + 1);

				m_nWheel += nLimit;
				}
			}

		return 0;
		}

	if( uMessage == WM_VSCROLL ) {

		switch( LOWORD(wParam) ) {
			
			case SB_THUMBPOSITION:
			case SB_THUMBTRACK:

				Scroll(int(HIWORD(wParam)));

				break;

			case SB_LINEUP:

				Scroll(m_nTop - 1);

				break;

			case SB_LINEDOWN:

				Scroll(m_nTop + 1);

				break;

			case SB_PAGEUP:

				Scroll(m_nTop - m_nPage);

				break;

			case SB_PAGEDOWN:

				Scroll(m_nTop + m_nPage);

				break;
			}

		m_nWheel = 0;
		}

	if( uMessage == WM_PAINT ) {

		PAINTSTRUCT ps;

		HDC hDC = BeginPaint(m_hWnd, &ps);

		SetMapping(hDC, ps.rcPaint);

		Draw(hDC, ps.rcPaint);

		EndPaint(m_hWnd, &ps);
		}

	if( uMessage == WM_LBUTTONDBLCLK ) {

		ClearSelection();

		FindMousePos(MAKEPOINTS(lParam));

		SelectWholeWord();
		}

	if( uMessage == WM_LBUTTONDOWN ) {

		if( !m_fCapture ) {

			ClearSelection();

			FindMousePos(MAKEPOINTS(lParam));

			m_Anchor   = m_Mouse;

			m_Select   = m_Mouse;

			m_fCapture = TRUE;

			SetCapture(m_hWnd);
			}
		}

	if( uMessage == WM_MOUSEMOVE ) {

		if( m_fCapture ) {

			FindMousePos(MAKEPOINTS(lParam));

			if( m_Select.x != m_Mouse.x || m_Select.y != m_Mouse.y ) {

				m_fSelect = TRUE;

				m_Select  = m_Mouse;

				FindSelection();

				KeepInView(m_Mouse.y);

				InvalidateRect(m_hWnd, NULL, FALSE);
				}
			}
		}

	if( uMessage == WM_LBUTTONUP ) {

		if( m_fCapture ) {

			m_fCapture = FALSE;

			SetCapture(NULL);
			}
		}

	if( uMessage == WM_RBUTTONDOWN ) {

		FindMousePos(MAKEPOINTS(lParam));

		BOOL fSelect = FALSE;

		if( m_fSelect ) {

			if( m_Mouse.y >= m_Start.y && m_Mouse.y <= m_End.y ) {

				if( m_Mouse.y > m_Start.y || m_Mouse.x >= m_Start.x ) {

					if( m_Mouse.y <= m_End.y || m_Mouse.x <= m_End.x ) {

						fSelect = TRUE;
						}
					}
				}
			}

		POINTS p     = MAKEPOINTS(lParam);

		POINT  Pos   = { p.x, p.y };

		HMENU  hMenu = CreatePopupMenu();

		if( fSelect ) {

			AppendMenu(hMenu, 0, 200, L"Copy");

			AppendMenu(hMenu, MF_SEPARATOR, 0, L"");
			}

		AppendMenu(hMenu, 0, 201, L"Select All");

		ClientToScreen(m_hWnd, &Pos);

		TrackPopupMenu(hMenu, 0, Pos.x, Pos.y, 0, m_hWnd, NULL);

		DestroyMenu(hMenu);
		}

	return DefWindowProc(m_hWnd, uMessage, wParam, lParam);
	}

// Implementation

void COutput::SetMapping(HDC hDC, RECT &Clip)
{
	int yTop = m_nTop * m_yFont;

	SetWindowOrgEx(hDC, 0, yTop, NULL);

	Clip.top    += yTop;
	
	Clip.bottom += yTop;
	}

void COutput::AdjustRect(RECT &Clip)
{
	int yTop = m_nTop * m_yFont;

	Clip.top    -= yTop;
	
	Clip.bottom -= yTop;
	}

void COutput::Draw(HDC hDC, RECT &Clip)
{
	FindSelection();

	HFONT hOld  = win32::SelectObject(hDC, m_hFont);

	int   nLine = Clip.top / m_yFont;

	int   yPos  = nLine    * m_yFont;

	while( yPos < Clip.bottom ) {

		if( nLine >= int(m_Text.GetCount()) ) {

			RECT r;

			r.left   = Clip.left;
			r.right  = Clip.right;
			r.top    = yPos;
			r.bottom = Clip.bottom;

			FillRect(hDC, &r, GetStockObject(WHITE_BRUSH));

			break;
			}

		PCTXT pText = m_Text[nLine];

		int   nText = m_Text[nLine].GetLength();

		POINT p     = { 0, yPos };

		SIZE  s     = { 0, 0 };

		if( m_fSelect ) {

			if( nLine < m_Start.y || nLine > m_End.y ) {

				Draw(hDC, p, s, pText, 0, nText, FALSE);
				}

			if( nLine > m_Start.y && nLine < m_End.y ) {

				Draw(hDC, p, s, pText, 0, nText, TRUE);
				}

			if( nLine == m_Start.y && nLine == m_End.y ) {
				
				Draw(hDC, p, s, pText, 0, m_Start.x, FALSE);

				Draw(hDC, p, s, pText, m_Start.x, m_End.x, TRUE);

				Draw(hDC, p, s, pText, m_End.x, nText, FALSE);
				}

			if( nLine == m_Start.y && nLine < m_End.y ) {
				
				Draw(hDC, p, s, pText, 0, m_Start.x, FALSE);

				Draw(hDC, p, s, pText, m_Start.x, nText, TRUE);
				}

			if( nLine > m_Start.y && nLine == m_End.y ) {
				
				Draw(hDC, p, s, pText, 0, m_End.x, TRUE);

				Draw(hDC, p, s, pText, m_End.x, nText, FALSE);
				}
			}
		else
			Draw(hDC, p, s, pText, 0, nText, FALSE);

		if( s.cx < Clip.right ) {

			RECT r;

			r.left   = s.cx;
			r.right  = Clip.right;
			r.top    = yPos;
			r.bottom = yPos + m_yFont;

			FillRect(hDC, &r, GetStockObject(WHITE_BRUSH));
			}

		if( s.cy < m_yFont ) {

			RECT r;

			r.left   = Clip.left;
			r.right  = Clip.right;
			r.top    = yPos + s.cy;
			r.bottom = yPos + m_yFont;

			FillRect(hDC, &r, GetStockObject(WHITE_BRUSH));
			}

		yPos  += m_yFont;

		nLine += 1;
		}

	win32::SelectObject(hDC, hOld);
	}

void COutput::Draw(HDC hDC, POINT &p, SIZE &s, PCTXT pText, int nFrom, int nTo, BOOL fSelect)
{
	if( nTo > nFrom ) {

		SIZE t;

		if( fSelect ) {

			SetTextColor(hDC, GetSysColor(COLOR_HIGHLIGHTTEXT));

			SetBkColor  (hDC, GetSysColor(COLOR_HIGHLIGHT));
			}
		else {
			SetTextColor(hDC, GetSysColor(COLOR_WINDOWTEXT));

			SetBkColor  (hDC, GetSysColor(COLOR_WINDOW));
			}

		GetTextExtentPointA(hDC, pText + nFrom, nTo - nFrom, &t);

		TextOutA(hDC, p.x, p.y, pText + nFrom, nTo - nFrom);

		p.x  += t.cx;

		s.cx += t.cx;

		s.cy  = t.cy;
		}
	}

BOOL COutput::Scroll(int nTop)
{
	int nLast  = int(m_Text.GetCount() - 1);

	int nLimit = max(0, nLast - m_nPage + 1);

	MakeMin(nTop, nLimit);

	MakeMax(nTop, 0);

	if( nTop != m_nTop ) {

		int nDelta = (m_nTop - nTop) * m_yFont;

		m_nTop     = nTop;

		if( abs(nDelta) < m_cy / 2 ) {

			RECT Rect, Rest;

			GetClientRect(m_hWnd, &Rect);

			GetClientRect(m_hWnd, &Rest);

			Rest.top = Rect.bottom = m_yFont * (Rect.bottom / m_yFont);

			ScrollWindow(m_hWnd, 0, nDelta, &Rect, &Rect);

			InvalidateRect(m_hWnd, &Rest, FALSE);
			}
		else
			InvalidateRect(m_hWnd, NULL, FALSE);

		UpdateScrollBar();

		return TRUE;
		}

	return FALSE;
	}

void COutput::UpdateScrollBar(void)
{
	int nLast = int(m_Text.GetCount() - 1);

	SCROLLINFO si;

	si.cbSize = sizeof(si);
	si.fMask  = SIF_DISABLENOSCROLL | SIF_PAGE | SIF_POS | SIF_RANGE;
	si.nMin   = 0;
	si.nMax   = nLast;
	si.nPage  = m_nPage;
	si.nPos   = m_nTop;

	SetScrollInfo(m_hWnd, SB_VERT, &si, TRUE);
	}

void COutput::InvalidateLine(int nLine)
{
	RECT r;

	r.left   = 0;
	r.right  = m_cx;
	r.top    = m_yFont * (nLine + 0);
	r.bottom = m_yFont * (nLine + 1);

	AdjustRect(r);

	InvalidateRect(m_hWnd, &r, FALSE);
	}

BOOL COutput::KeepInView(int nLine)
{
	if( nLine < m_nTop ) {

		return Scroll(nLine);
		}

	if( nLine > m_nTop + m_nPage - 1 ) {

		return Scroll(nLine - m_nPage + 1);
		}

	return FALSE;
	}

void COutput::ClearSelection(void)
{
	if( m_fSelect ) {

		m_fSelect = FALSE;

		InvalidateRect(m_hWnd, NULL, FALSE);
		}
	}

void COutput::SelectWholeWord(void)
{
	m_Anchor = m_Mouse;

	m_Select = m_Mouse;

	CString const &Text = m_Text[m_Mouse.y];
	
	int nMin = 0;

	int nMax = Text.GetLength();

	while( m_Anchor.x > nMin && !isspace(Text[m_Anchor.x - 1]) ) {

		m_Anchor.x--;
		}

	while( m_Select.x < nMax && !isspace(Text[m_Select.x + 0]) ) {

		m_Select.x++;
		}

	m_fSelect = TRUE;

	FindSelection();

	InvalidateRect(m_hWnd, NULL, FALSE);
	}

void COutput::FindSelection(void)
{
	if( m_fSelect ) {

		m_Start = m_Anchor;

		m_End   = m_Select;

		if( m_Start.y == m_End.y ) {

			if( m_Start.x == m_End.x ) {

				m_fSelect = FALSE;

				return;
				}

			if( m_Start.x > m_End.x ) {

				m_Start = m_Select;

				m_End   = m_Anchor;

				return;
				}
			}

		if( m_Start.y > m_End.y ) {

			m_Start = m_Select;

			m_End   = m_Anchor;

			return;
			}
		}
	}

void COutput::FindMousePos(POINTS p)
{
	int nLine = m_nTop + p.y / m_yFont;

	if( nLine < 0 ) {

		m_Mouse.x = 0;

		m_Mouse.y = 0;
		}
	else {
		int nLast = m_Text.GetCount() - 1;

		if( nLine > nLast ) {

			m_Mouse.x = m_Text[nLast].GetLength();

			m_Mouse.y = nLast;
			}
		else {
			int nChar = (p.x + m_xFont / 2) / m_xFont;

			MakeMax(nChar, 0);

			MakeMin(nChar, int(m_Text[nLine].GetLength()));

			m_Mouse.x = nChar;

			m_Mouse.y = nLine;
			}
		}
	}

void COutput::CopySelection(void)
{
	if( OpenClipboard(m_hWnd) ) {

		EmptyClipboard();

		CString Text;

		for( int nLine = m_Start.y; nLine <= m_End.y; nLine++ ) {

			int nMin  = 0;

			int nMax  = m_Text[nLine].GetLength();

			int nFrom = (nLine == m_Start.y) ? m_Start.x : nMin;

			int nTo   = (nLine == m_End.y  ) ? m_End.x   : nMax;

			Text += m_Text[nLine].Mid(nFrom, nTo - nFrom);

			Text += "\r\n";
			}

		HGLOBAL hMem = GlobalAlloc(GHND, Text.GetLength() + 1);

		PTXT    pMem = PTXT(GlobalLock(hMem));

		strcpy(pMem, Text);

		GlobalUnlock(hMem);

		SetClipboardData(CF_TEXT, hMem);

		CloseClipboard();
		}
	}

void COutput::SelectAll(void)
{
	int nLast  = int(m_Text.GetCount() - 1);

	m_Anchor.x = 0;

	m_Anchor.y = 0;

	m_Select.x = m_Text[nLast].GetLength();

	m_Select.y = nLast;

	m_fSelect  = TRUE;

	FindSelection();

	InvalidateRect(m_hWnd, NULL, FALSE);
	}

// End of File
