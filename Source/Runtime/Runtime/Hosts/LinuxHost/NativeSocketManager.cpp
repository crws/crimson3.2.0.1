
#include "Intern.hpp"

#include "NativeSocketManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "BaseSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Native Socket Manager
//

// Instance Pointer

CNativeSocketManager * CNativeSocketManager::m_pThis = NULL;

// Instantiators

global void Register_NativeSocketManager(void)
{
	New CNativeSocketManager;
}

global void Revoke_NativeSocketManager(void)
{
	delete CNativeSocketManager::m_pThis;
}

// Constructor

CNativeSocketManager::CNativeSocketManager(void)
{
	StdSetRef();

	m_pThis = this;
}

// Destructor

CNativeSocketManager::~CNativeSocketManager(void)
{
	m_pThis = NULL;
}

// Operations

void CNativeSocketManager::AddSocket(CBaseSocket *pSock)
{
	CListRoot *pRoot = (CListRoot *) AddThreadNotify(this);

	AfxListAppend(pRoot->m_pHead, pRoot->m_pTail, pSock, m_pNext, m_pPrev);

	pSock->m_pRoot = pRoot;
}

void CNativeSocketManager::FreeSocket(CBaseSocket *pSock)
{
	CListRoot *pRoot = (CListRoot *) pSock->m_pRoot;

	AfxListRemove(pRoot->m_pHead, pRoot->m_pTail, pSock, m_pNext, m_pPrev);
}

// IUnknown

HRESULT CNativeSocketManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IThreadNotify);

	StdQueryInterface(IThreadNotify);

	return E_NOINTERFACE;
}

ULONG CNativeSocketManager::AddRef(void)
{
	StdAddRef();
}

ULONG CNativeSocketManager::Release(void)
{
	StdRelease();
}

// IThreadNotify

UINT CNativeSocketManager::OnThreadCreate(IThread *pThread, UINT uIndex)
{
	CListRoot *pRoot = New CListRoot;

	pRoot->m_pHead = NULL;

	pRoot->m_pTail = NULL;

	return UINT(pRoot);
}

void CNativeSocketManager::OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam)
{
	CListRoot *pRoot = (CListRoot *) uParam;

	if( pRoot->m_pHead ) {

		AfxTrace("thread %u exiting with open sockets\n", pThread->GetIdent());

		while( pRoot->m_pHead ) {

			CBaseSocket *pSock = pRoot->m_pHead;

			pSock->Release();

			AfxAssert(pRoot->m_pHead != pSock);
		}
	}

	delete pRoot;
}

// End of File
