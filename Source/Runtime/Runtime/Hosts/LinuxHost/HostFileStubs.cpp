
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Stubs for Filing System Support
//

// Code

global void Bind_FileSupport(void)
{
}

global void Free_FileSupport(void)
{
}

clink int isatty(int fd)
{
	return _isatty(fd);
}

clink int rename(char const *from, char const *to)
{
	return _rename(from, to);
}

clink int unlink(char const *name)
{
	return _unlink(name);
}

clink int stat(char const *name, struct stat *buffer)
{
	int fd = _open(name, O_RDONLY, 0);

	if( fd >= 0 ) {

		if( _fstat(fd, buffer) ) {

			_close(fd);

			return 0;
		}

		_close(fd);
	}

	return -1;
}

clink int utime(char const *name, time_t time)
{
	return _utime(name, time, time);
}

clink int chmod(char const *name, mode_t mode)
{
	return _chmod(name, mode);
}

clink int open(char const *name, int oflag, ...)
{
	va_list pArgs;

	va_start(pArgs, oflag);

	int mode = va_arg(pArgs, int);

	int fd   = _open(name, oflag, mode);

	va_end(pArgs);

	return fd;
}

clink int close(int fd)
{
	return _close(fd);
}

clink int fstat(int fd, struct stat *buffer)
{
	return _fstat(fd, buffer);
}

clink int read(int fd, void *buffer, unsigned int count)
{
	return _read(fd, buffer, count);
}

clink int write(int fd, void const *buffer, unsigned int count)
{
	return _write(fd, buffer, count);
}

clink off_t lseek(int fd, off_t offset, int origin)
{
	return _lseek(fd, offset, origin);
}

clink int ftruncate(int fd, off_t size)
{
	return _ftruncate(fd, size);
}

clink int ioctl(int fd, int func, void *data)
{
	return _ioctl(fd, func, data);
}

clink char * getcwd(char *buff, size_t size)
{
	AfxAssert(FALSE);

	return NULL;
}

clink int chdir(char const *name)
{
	return _chdir(name);
}

clink int rmdir(char const *name)
{
	return _rmdir(name);
}

clink int mkdir(char const *name, mode_t mode)
{
	return _mkdir(name, mode);
}

clink int scandir(char const *name, struct dirent ***list, int (*selector)(struct dirent const *), int (*compare)(struct dirent const **, struct dirent const **))
{
	int c = 0;

	struct linux_dirent64
	{
		INT64		d_ino;
		INT64		d_off;
		unsigned short	d_reclen;
		unsigned char	d_type;
		char		d_name[];
	};

	for( int p = 0; p < 2; p++ ) {

		int fd = _open(name, O_RDONLY, 0);

		if( fd < 0 ) {

			return -1;
		}
		else {
			BYTE buff[1024];

			int  used = 0;

			int  pos  = 0;

			for( int n = 0;; ) {

				if( pos >= used ) {

					if( (used = _getdents64(fd, buff, sizeof(buff))) <= 0 ) {

						c = n;

						_close(fd);

						break;
					}

					pos = 0;
				}

				linux_dirent64 *read = (linux_dirent64 *) (buff + pos);

				dirent d = { 0 };

				d.d_type = read->d_type;

				strcpy(d.d_name, read->d_name);

				if( !selector || selector(&d) ) {

					if( p == 1 && n < c ) {

						(*list)[n] = (dirent *) malloc(sizeof(dirent));

						memcpy((*list)[n], &d, sizeof(d));
					}

					n++;
				}

				pos += read->d_reclen;
			}

			if( p == 0 ) {

				if( !c )
					break;

				*list = (dirent **) calloc(c, sizeof(dirent *));
			}
		}
	}

	if( compare ) {

		if( c > 1 ) {

			qsort(*list, c, sizeof(dirent *), (__compar_fn_t) compare);
		}
	}

	return c;
}

clink int sync(void)
{
	return _sync();
}

clink int mapname(char *name, int add)
{
	AfxGetAutoObject(pSupport, "aeon.filesupport", 0, IFileSupport);

	if( pSupport ) {

		return pSupport->MapName(name, add);
	}

	return -1;
}

// End of File
