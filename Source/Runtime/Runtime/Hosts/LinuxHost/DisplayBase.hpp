
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DisplayBase_HPP

#define INCLUDE_DisplayBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Display Object Base Class
//

class CDisplayBase : public IDisplay
{
public:
	// Constructor
	CDisplayBase(void);

	// Destructor
	virtual ~CDisplayBase(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IDisplay
	void METHOD Claim(void);
	void METHOD Free(void);
	void METHOD GetSize(int &cx, int &cy);
	BOOL METHOD SetBacklight(UINT pc);
	UINT METHOD GetBacklight(void);
	BOOL METHOD EnableBacklight(BOOL fOn);
	BOOL METHOD IsBacklightEnabled(void);

protected:
	// Data Members
	ULONG    m_uRefs;
	IMutex * m_pLock;
	PCSTR    m_pBackRaw;
	PCSTR    m_pBackMax;
	UINT     m_uBackRaw;
	UINT     m_uBackMax;
	UINT     m_uBackVal;
	BOOL     m_fBackOn;
	int      m_cx;
	int      m_cy;

	// Implementation
	void ReadBacklight(void);
	BOOL WriteFile(PCSTR pFile, UINT uData);
	UINT ReadFile(PCSTR pFile, UINT uData);
};

// End of File

#endif
