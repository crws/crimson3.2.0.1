
#include "Intern.hpp"

#include "EdgePort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// System Headers
//

#include <sys/ioctl.h>

#include <linux/hidraw.h>

//////////////////////////////////////////////////////////////////////////
//
// Edge Port
//

// Instantiators

IDevice * Create_EdgePort(UINT uSled)
{
	CPrintf Name("/dev/xredge_%u", uSled);

	return New CEdgePort(Name, uSled);
}

// Constructor

CEdgePort::CEdgePort(CString Name, UINT uSled)
{
	StdSetRef();

	m_pGuard = Create_Qutex();
	
	m_Name   = Name;

	m_uUnit  = uSled;

	m_fd     = -1;
}

// Destructor

CEdgePort::~CEdgePort(void)
{
	_close(m_fd);

	AfxRelease(m_pGuard);
}

// IUnknown

HRESULT CEdgePort::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IGpio);

	StdQueryInterface(IGpio);

	return E_NOINTERFACE;
}

ULONG CEdgePort::AddRef(void)
{
	StdAddRef();
}

ULONG CEdgePort::Release(void)
{
	StdRelease();
}

// IDevice

BOOL METHOD CEdgePort::Open(void)
{
	CAutoLock Lock(m_pGuard);
	
	if( m_fd < 0 ) {

		int fd = _open(m_Name, O_RDWR, 0);

		if( fd >= 0 ) {

			struct hidraw_devinfo info;

			if( _ioctl(fd, HIDIOCGRAWINFO, &info) >= 0 ) {

				if( info.vendor == 0x1037 && info.product == 0x1214 ) {

					m_fd  = fd;

					return TRUE;
				}

				if( info.vendor == 0x04E2 && info.product == 0x1200 ) {

					AfxTrace("supporting legacy sled\n");

					m_fd  = fd;

					return TRUE;
				}
			}

			_close(fd);
		}

		return FALSE;
	}

	return TRUE;
}

// IGpio

void METHOD CEdgePort::SetDirection(UINT n, BOOL fOutput)
{
	CAutoLock Lock(m_pGuard);

	WORD wAddr = n < 16 ? 0x03C1 : 0x03CD;

	WORD wMask = Bit(n % 16);

	WORD wData = GetRegister(wAddr);

	if( fOutput ) {

		wData |= wMask;
	}
	else {
		wData &= ~wMask;
	}

	SetRegister(wAddr, wData);
}

void METHOD CEdgePort::SetState(UINT n, BOOL fState)
{
	WORD wMask = Bit(n % 16);

	if( fState ) {

		WORD wAddr = n < 16 ? 0x03C2 : 0x03CE;

		SetRegister(wAddr, wMask);
	}
	else {
		WORD wAddr = n < 16 ? 0x03C3 : 0x03CF;

		SetRegister(wAddr, wMask);
	}
}

BOOL METHOD CEdgePort::GetState(UINT n)
{
	WORD wAddr = n < 16 ? 0x03C4 : 0x03D0;

	WORD wMask = Bit(n % 16);

	return (GetRegister(wAddr) & wMask) ? TRUE : FALSE;
}

void METHOD CEdgePort::EnableEvents(void)
{
}

void METHOD CEdgePort::SetIntHandler(UINT n, UINT uType, IEventSink *pSink, UINT uParam)
{
}

void METHOD CEdgePort::SetIntEnable(UINT n, BOOL fEnable)
{
}

// Registers

BOOL CEdgePort::SetRegister(WORD wAddr, WORD wData)
{
	CAutoLock Lock(m_pGuard);

	BYTE bCmd[5];

	bCmd[0] = 0x3C;
	bCmd[1] = LOBYTE(wAddr);
	bCmd[2] = HIBYTE(wAddr);
	bCmd[3] = LOBYTE(wData);
	bCmd[4] = HIBYTE(wData);

	return IoCtl(HIDIOCSFEATURE(sizeof(bCmd)), bCmd);
}

WORD CEdgePort::GetRegister(WORD wAddr)
{
	CAutoLock Lock(m_pGuard);

	BYTE bCmd[3];

	bCmd[0] = 0x4B;
	bCmd[1] = LOBYTE(wAddr);
	bCmd[2] = HIBYTE(wAddr);

	if( IoCtl(HIDIOCSFEATURE(sizeof(bCmd)), bCmd) ) {

		bCmd[0] = 0x5A;
		bCmd[1] = 0x00;
		bCmd[2] = 0x00;

		if( IoCtl(HIDIOCGFEATURE(sizeof(bCmd)), bCmd) ) {

			WORD wData = MAKEWORD(bCmd[1], bCmd[2]);

			return wData;
		}
	}

	return 0;
}

BOOL CEdgePort::IoCtl(int n, void *d)
{
	for( ;;) {

		int r = _ioctl(m_fd, n, d);

		if( r < 0 ) {

			if( errno == EAGAIN || errno == EINTR ) {

				continue;
			}

			if( errno == ENODEV ) {

				for( Close(); !Open(); sleep(200) );

				continue;
			}

			return FALSE;
		}

		break;
	}

	return TRUE;
}

void CEdgePort::Close(void)
{
	_close(m_fd);

	m_fd = -1;
}

// End of File
