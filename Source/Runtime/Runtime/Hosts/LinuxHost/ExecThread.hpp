
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ExecTask_HPP

#define INCLUDE_ExecTask_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/MosExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Task Object
//

class CExecThread : public CMosExecThread
{
public:
	// Constructor
	CExecThread(CMosExecutive *pExec);

	// Destructor
	~CExecThread(void);

	// IWaitable
	PVOID METHOD GetWaitable(void);
	BOOL  METHOD Wait(UINT uTime);
	BOOL  METHOD HasRequest(void);

	// IThread
	void METHOD Guard(BOOL fGuard);
	void METHOD Guard(PGUARD pProc, PVOID pData);
	void METHOD Exit(int nCode);
	BOOL METHOD Destroy(void);

	// Stack Pool
	static DWORD m_PoolBase;
	static DWORD m_PoolSize;

protected:
	// Platform Data
	UINT  m_uStackSize;
	PBYTE m_pStackBase;
	PBYTE m_pStackInit;
	PVOID m_pTlsData;
	UINT  m_uTlsSize;
	PINT  m_pWid;

	// Overridables
	BOOL OnCreate(void);
	void OnChange(int state);

	// Implementation
	void AllocateStack(void);
	void FreeStack(void);
	int  ExecuteThread(void);
	void InstallTls(void);
	void InstallImpure(void);
	void ApplyPriority(void);

	// Entry Point
	static int ThreadProc(void *);
};

// End of File

#endif
