
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Creation Helper
//

bool CreateStaticObject(PCTXT pName, REFIID riid, void **ppObject);

//////////////////////////////////////////////////////////////////////////
//
// Debug Trace Implementation
//

// Data

static IMutex       * m_pMutex = NULL;

static IDiagManager * m_pDiag  = NULL;

// Code

global void AfxDebug(char cData)
{
	_write(1, &cData, 1);
}

global void AfxDebug(PCTXT pText)
{
	_write(1, pText, strlen(pText));
}

global void AfxPrint(PCTXT pText)
{
	if( CreateStaticObject("exec.qutex", AfxAeonIID(IMutex), (void **) &m_pMutex) ) {

		if( m_pMutex->Wait(FOREVER) ) {

			if( !m_pDiag ) {

				AfxGetObject("aeon.diagmanager", 0, IDiagManager, m_pDiag);
			}

			if( m_pDiag ) {

				m_pDiag->WriteToConsoles(pText);
			}

			AfxDebug(pText);

			m_pMutex->Free();
		}

		return;
	}

	AfxDebug(pText);
}

// End of File
