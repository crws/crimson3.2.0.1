
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Intern_HPP

#define INCLUDE_Intern_HPP

//////////////////////////////////////////////////////////////////////////
//
// Environment Selection
//

#define AEON_NEED_LINUX

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include "../../HSL/AeonHsl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// System Call Types
//

#if defined(AEON_PROC_X86)

typedef __socklen_t socklen_t;

#endif

#if defined(AEON_PROC_ARM)

typedef unsigned int socklen_t;

#endif

//////////////////////////////////////////////////////////////////////////
//
// System Call Wrappers
//

clink void   _exit(int code);
clink void   _exit_group(int code);
clink int    _fork(void);
clink int    _nice(int inc);
clink int    _sched_getscheduler(int pid);
clink int    _sched_setscheduler(int pid, int policy, int const *priority);
clink int    _sched_get_priority_min(int policy);
clink int    _sched_get_priority_max(int policy);
clink int    _rename(const char *from, const char *to);
clink int    _unlink(char const *name);
clink int    _execve(char const *filename, char const * const argv[], char const * const envp[]);
clink int    _utime(char const *name, time_t atime, time_t mtime);
clink int    _chmod(char const *name, mode_t mode);
clink int    _dup2(int oldfd, int newfd);
clink int    _open(char const *name, int oflag, int pmode);
clink int    _close(int fd);
clink int    _close_on_exec(void);
clink int    _waitpid(int pid, int *status);
clink int    _wait_thread(int pid);
clink int    _wait_thread_raw(int pid);
clink int    _isatty(int fd);
clink int    _fstat(int fd, struct stat *buffer);
clink int    _stat(char const *path, struct stat *buffer);
clink int    _read(int fd, void *buffer, size_t count);
clink int    _read_wait(int fd, void *buffer, size_t count, UINT ms);
clink int    _write(int fd, void const *buffer, size_t count);
clink int    _getdents64(int fd, void *p, size_t size);
clink off_t  _lseek(int fd, off_t offset, int origin);
clink int    _ftruncate(int fd, off_t size);
clink int    _chdir(char const *path);
clink int    _mkdir(char const *path, int mode);
clink int    _rmdir(char const *path);
clink int    _sync(void);
clink int    _fsync(int fd);
clink int    _getpid(void);
clink int    _gettid(void);
clink int    __gettid(void);
clink int    _ioctl(int fd, int n, void *p);
clink int    _getdrivesize(char const *path, unsigned long long *size);
clink int    _getdrivefree(char const *path, unsigned long long *free);
clink int    _kill(int pid, int code);
clink int    _tkill(int tid, int code);
clink int    _brk(unsigned long req);
clink int    _fcntl(int fd, int cmd, int data);
clink int    _sigaction(int n, struct sigaction const *act, struct sigaction *old);
clink int    _sighandler(int n, void (*pfnHandler)(int));
clink int    gettimeofday(timeval *pt, void const *tz);
clink int    settimeofday(timeval const *pt, void const *tz);
clink time_t getmonosecs(void);
clink INT64  getmonomilli(void);
clink INT64  getmonomicro(void);
clink int    _readlink(char const *link, char *buff, int size);
clink void * _mmap(unsigned long addr, unsigned long len, int prot, int flags, int fd, long long offset);
clink void * _mmap_private(unsigned long len);
clink int    _munmap(void *base, unsigned long len);
clink int    _munmap_and_exit(void *base, unsigned long len, int code);
clink int    _create_thread(int (*pfnProc)(void*), void *pParam, void *pStack, int *pPid);
clink int    _sched_yield(void);
clink int    _sleep(int ms);
clink int    _usleep(int us);
clink int    _futex(int *p, int op, int val, void *t);
clink int    _futex_base(int *p, int op, int val, void *t);
clink int    _futex_wait(int *p, int val, UINT ms);
clink int    _futex_wake(int *p, int n);
clink int    _clock_gettime_ms(int n);
clink int    _select(int nfds, fd_set *read, fd_set *write, fd_set *except, UINT ms);
clink int    _select_read(int fd, UINT ms);
clink int    _select_write(int fd, UINT ms);
clink int    _select_except(int fd, UINT ms);
clink int    _poll(struct pollfd *fds, int nfds, int ms);
clink int    _timerfd_create(int clockid, int flags);
clink int    _timerfd_settime(int fd, int flags, UINT ms, bool periodic);
clink int    _timerfd_gettime(int fd, PUINT *pms);
clink int    _eventfd(int init);
clink int    _pipe(int &fi, int &fo);
clink int    _socket(int domain, int type, int protocol);
clink int    _bind(int fd, const struct sockaddr *addr, socklen_t addrlen);
clink int    _connect(int fd, const struct sockaddr *addr, socklen_t addrlen);
clink int    _listen(int fd, int backlog);
clink int    _accept(int fd, const struct sockaddr *addr, socklen_t *addrlen);
clink int    _shutdown(int fd, int how);
clink int    _send(int fd, const void *buf, size_t len, int flags);
clink int    _recv(int fd, void *buf, size_t len, int flags);
clink int    _recvfrom(int fd, void *buf, size_t len, int flags, sockaddr *from, int *fromlen);
clink int    _recvmsg(int fd, struct msghdr *msg, int flags);
clink int    _getsockopt(int fd, int level, int optname, void *optval, socklen_t *optlen);
clink int    _setsockopt(int fd, int level, int optname, const void *optval, socklen_t optlen);
clink int    _getsockname(int fd, struct sockaddr *addr, socklen_t *addrlen);
clink int    _getpeername(int fd, struct sockaddr *addr, socklen_t *addrlen);

//////////////////////////////////////////////////////////////////////////
//
// Process Invocation
//

global int CallProcess(char const *pCmd, char const * const *pArgs, char const *pStdOut, char const *pStdErr);
global int CallProcess(char const *pCmd, char const *pArgs, char const *pStdOut, char const *pStdErr);
global int CallProcess(char const * const *pArgs, char const *pStdOut, char const *pStdErr);
global int InitProcess(char const *pCmd, char const * const *pArgs);
global int InitProcess(char const *pCmd, char const *pArgs);
global int InitProcess(char const * const *pArgs);
global int TermProcess(int pid);
global int KillProcess(int pid);

//////////////////////////////////////////////////////////////////////////
//
// Wait Accumulation
//

global void AddToSlept(UINT uTime);

// End of File

#endif
