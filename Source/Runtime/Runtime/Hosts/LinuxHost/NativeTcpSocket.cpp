
#include "Intern.hpp"

#include "NativeTcpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "Socket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP Socket Object
//

// Access Lock

IMutex * CNativeTcpSocket::m_pLock = NULL;

// Listen Map

CMap<CNativeTcpSocket::CBind, CNativeTcpSocket::CListen *> CNativeTcpSocket::m_Listen;

// Instantiator

static IUnknown * Create_NativeTcpSocket(PCTXT pName)
{
	return (IUnknown *) New CNativeTcpSocket;
}

// Registration

global void Register_NativeTcpSocket(void)
{
	CNativeTcpSocket::Init();

	piob->RegisterInstantiator("net.sock-tcp", Create_NativeTcpSocket);
}

global void Revoke_NativeTcpSocket(void)
{
	piob->RevokeInstantiator("net.sock-tcp");

	CNativeTcpSocket::Term();
}

// Constructor

CNativeTcpSocket::CNativeTcpSocket(void)
{
}

// Destructor

CNativeTcpSocket::~CNativeTcpSocket(void)
{
	Abort();
}

// ISocket

HRESULT CNativeTcpSocket::Listen(IPADDR const &IP, WORD Loc)
{
	if( m_uState == stateInit ) {

		CAutoLock Lock(m_pLock);

		CBind     Bind(IP, Loc);

		INDEX     Find = m_Listen.FindName(Bind);

		if( m_Listen.Failed(Find) ) {

			if( (m_hSocket = _socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) > 0 ) {

				SetNonBlocking();

				AllowDupAddress();

				sockaddr_in loc;

				LoadAddress(loc, IP, Loc);

				if( _bind(m_hSocket, (sockaddr *) &loc, sizeof(loc)) == 0 ) {

					m_pListen = New CListen(Bind, m_hSocket);

					m_Listen.Insert(Bind, m_pListen);

					m_uState  = stateListen;

					ListenAdjust();

					return S_OK;
				}
			}

			Close();
		}
		else {
			m_pListen = m_Listen.GetData(Find);

			m_hSocket = m_pListen->m_hSocket;

			m_uState  = stateListen;

			m_pListen->m_uCount++;

			ListenAdjust();

			return S_OK;
		}
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc)
{
	if( m_uState == stateInit ) {

		if( (m_hSocket = _socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) > 0 ) {

			sockaddr_in rem;

			sockaddr_in loc;

			LoadAddress(rem, IP, Rem);

			if( !LoadAddress(loc, IF, Loc) || _bind(m_hSocket, (sockaddr *) &loc, sizeof(loc)) == 0 ) {

				SetNonBlocking();

				int nCode = _connect(m_hSocket, (sockaddr *) &rem, sizeof(rem));

				if( nCode == 0 ) {

					m_uState = stateOpen;

					return S_OK;
				}

				if( errno == EINPROGRESS ) {

					m_uState = stateConnect;

					return S_OK;
				}
			}

			Close();
		}
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::GetPhase(UINT &Phase)
{
	if( m_uState == stateInit ) {

		Phase = PHASE_IDLE;

		return S_OK;
	}

	if( m_uState == stateListen ) {

		SOCKET hSocket;

		if( (hSocket = _accept(m_hSocket, NULL, 0)) > 0 ) {

			CAutoLock Lock(m_pLock);

			if( !--m_pListen->m_uCount ) {

				ListenDelete();

				_close(m_hSocket);
			}
			else {
				ListenAdjust();
			}

			m_hSocket = hSocket;

			SetNonBlocking();

			m_uState = stateOpen;

			Phase    = PHASE_OPEN;

			return S_OK;
		}

		Phase = PHASE_IDLE;

		return S_OK;
	}

	if( m_uState == stateConnect ) {

		if( _select_write(m_hSocket, 0) > 0 ) {

			if( HasError() ) {

				Abort();

				Phase = PHASE_ERROR;

				return S_OK;
			}

			m_uState = stateOpen;

			Phase    = PHASE_OPEN;

			return S_OK;
		}

		Phase = PHASE_OPENING;

		return S_OK;
	}

	if( m_uState == stateOpen ) {

		if( CheckRemoteClose(FALSE) ) {

			Phase = PHASE_CLOSING;

			return S_OK;
		}

		Phase = PHASE_OPEN;

		return S_OK;
	}

	if( m_uState == stateClosed ) {

		if( CheckRemoteClose(TRUE) ) {

			Phase = PHASE_ERROR;

			return S_OK;
		}

		Phase = PHASE_OPEN;

		return S_OK;
	}

	if( m_uState == stateError ) {

		Phase = PHASE_ERROR;

		return S_OK;
	}

	AfxAssert(FALSE);

	return E_FAIL;
}

HRESULT CNativeTcpSocket::SetOption(UINT uOption, UINT uValue)
{
	if( uOption == OPT_LINGER ) {

		ULONG uEnable = uValue ? TRUE : FALSE;

		_setsockopt(m_hSocket, SOL_SOCKET, SO_LINGER, PCTXT(&uEnable), sizeof(uEnable));

		return S_OK;
	}

	if( uOption == OPT_KEEP_ALIVE ) {

		ULONG uEnable = uValue ? TRUE : FALSE;

		_setsockopt(m_hSocket, SOL_SOCKET, SO_KEEPALIVE, PCTXT(&uEnable), sizeof(uEnable));

		return S_OK;
	}

	if( uOption == OPT_NAGLE ) {

		ULONG uEnable = uValue ? FALSE : TRUE;

		_setsockopt(m_hSocket, IPPROTO_TCP, TCP_NODELAY, PCTXT(&uEnable), sizeof(uEnable));

		return S_OK;
	}

	return CBaseSocket::SetOption(uOption, uValue);
}

HRESULT CNativeTcpSocket::Abort(void)
{
	if( m_uState < stateError ) {

		if( ListenAbort() ) {

			return S_OK;
		}

		if( m_uState >= stateOpen ) {

			if( CheckRemoteClose(TRUE) ) {

				return S_OK;
			}
		}

		if( m_uState == stateOpen ) {

			SetAbortMode();
		}

		CloseHandle();

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Close(void)
{
	if( m_uState <= stateConnect ) {

		return Abort();
	}

	if( m_uState <= stateOpen ) {

		_shutdown(m_hSocket, SHUT_WR);

		m_uState = stateClosed;

		CheckRemoteClose(TRUE);

		return S_OK;
	}

	return E_FAIL;
}

// Initialization

void CNativeTcpSocket::Init(void)
{
	m_pLock = Create_Mutex();
}

void CNativeTcpSocket::Term(void)
{
	while( !m_Listen.IsEmpty() ) {

		INDEX    nListen = m_Listen.GetHead();

		CListen *pListen = m_Listen[nListen];

		_close(pListen->m_hSocket);

		delete pListen;

		m_Listen.Remove(nListen);
	}

	AfxRelease(CNativeTcpSocket::m_pLock);
}

// Implementation

BOOL CNativeTcpSocket::CheckRemoteClose(BOOL fDone)
{
	if( _select_read(m_hSocket, 0) > 0 ) {

		ULONG uCount = 0;

		_ioctl(m_hSocket, FIONREAD, &uCount);

		if( !uCount ) {

			if( fDone ) {

				if( m_uState != stateClosed ) {

					if( HasError() ) {

						SetAbortMode();
					}
				}

				CloseHandle();
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CNativeTcpSocket::HasError(void)
{
	ULONG     uError = 0;

	socklen_t uSize  = sizeof(uError);

	if( _getsockopt(m_hSocket, SOL_SOCKET, SO_ERROR, &uError, &uSize) == 0 ) {

		if( !uError ) {

			return FALSE;
		}
	}

	return TRUE;
}

void CNativeTcpSocket::SetAbortMode(void)
{
	struct linger linger;

	linger.l_onoff  = 1;

	linger.l_linger = 0;

	_setsockopt(m_hSocket, SOL_SOCKET, SO_LINGER, PCTXT(&linger), sizeof(linger));
}

BOOL CNativeTcpSocket::ListenAdjust(void)
{
	#if defined(_DEBUG)

	if( m_fDebug ) {

		AfxTrace("init listen on %d for %s:%u %d times\n",
			 m_hSocket,
			 m_pListen->m_Bind.m_Addr.GetAsText().data(),
			 m_pListen->m_Bind.m_Port, m_pListen->m_uCount
		);
	}

	#endif

	_listen(m_hSocket, m_pListen->m_uCount);

	return TRUE;
}

BOOL CNativeTcpSocket::ListenDelete(void)
{
	#if defined(_DEBUG)

	if( m_fDebug ) {

		AfxTrace("stop listen on %d for %s:%u\n",
			 m_hSocket,
			 m_pListen->m_Bind.m_Addr.GetAsText().data(),
			 m_pListen->m_Bind.m_Port
		);
	}

	#endif

	m_Listen.Remove(m_pListen->m_Bind);

	delete m_pListen;

	return TRUE;
}

BOOL CNativeTcpSocket::ListenAbort(void)
{
	if( m_uState == stateListen ) {

		CAutoLock Lock(m_pLock);

		if( !--m_pListen->m_uCount ) {
			
			ListenDelete();

			CloseHandle();
		}
		else {
			ListenAdjust();

			m_hSocket = 0;

			m_uState = stateError;
		}

		return TRUE;
	}

	return FALSE;
}

// End of File
