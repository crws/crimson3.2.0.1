
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ModuleManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Constructors Hook
//

extern void CallCtors(void);

//////////////////////////////////////////////////////////////////////////
//
// Environment Setup
//

extern void InitEnviron(void);

//////////////////////////////////////////////////////////////////////////
//
// System Object Management
//

extern void Init_RtlSupport(void);

extern void Term_RtlSupport(void);

//////////////////////////////////////////////////////////////////////////
//
// Entry Points
//

// Global Data

global BOOL		g_fService = FALSE;

// Static Data

static CModuleManager * m_pModMan  = NULL;

static BOOL             m_fTest    = NULL;

static CStringArray     m_Modules;

// Prototypes

clink	int	main(void);
global	BOOL	LoadModules(void);
global	void	FreeModules(void);
static	int	HostInit(void);
static  BOOL	GetCommandLine(CStringArray &List);
global	BOOL	ServiceHook(int &nCode);

// Code

clink int main(void)
{
	Init_RtlSupport();

	InitEnviron();

	CallCtors();

	int r = LOWORD(HostInit());

	Term_RtlSupport();

	_exit_group(r);

	return r;
}

global BOOL LoadModules(void)
{
	m_pModMan = New CModuleManager;

	for( UINT n = 0; n < m_Modules.GetCount(); n++ ) {

		m_pModMan->LoadClientModule(m_Modules[n]);
	}

	return TRUE;
}

global void FreeModules(void)
{
	delete m_pModMan;

	m_Modules.Empty();
}

static int HostInit(void)
{
	CStringArray List;

	if( GetCommandLine(List) ) {

		for( UINT n = 1; n < List.GetCount(); n++ ) {

			CString const &Arg = List[n];

			if( Arg[0] == '-' || Arg[0] == '/' ) {

				if( Arg.Mid(1) == "test" ) {

					m_fTest = TRUE;

					continue;
				}

				if( Arg.Mid(1) == "service" ) {

					g_fService = TRUE;

					continue;
				}

				AfxTrace("host: bad switch '%s'\n", PCTXT(Arg)+1);

				return -1;
			}

			m_Modules.Append(Arg);
		}
	}

	if( m_Modules.IsEmpty() ) {

		m_Modules.Append(m_fTest ? "CMTest" : "CM1");
	}

	return AeonHostInit(m_fTest);
}

static BOOL GetCommandLine(CStringArray &List)
{
	int pid  = _getpid();

	int file = _open(CPrintf("/proc/%u/cmdline", pid), O_RDONLY, 0);

	if( file > 0 ) {

		char line[1024];

		memset(line, 0, sizeof(line));

		int len = _read(file, line, sizeof(line));

		int ptr = 0;

		while( ptr < len ) {

			int arg = strlen(line + ptr);

			List.Append(line + ptr);

			ptr += arg + 1;
		}

		_close(file);

		return TRUE;
	}

	return FALSE;
}

global BOOL ServiceHook(int &nCode)
{
	return FALSE;
}

// End of File
