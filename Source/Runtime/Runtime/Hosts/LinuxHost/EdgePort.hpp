
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_EdgePort_HPP

#define INCLUDE_EdgePort_HPP

//////////////////////////////////////////////////////////////////////////
//
// HID Edge Port
//

class CEdgePort : public IGpio
{
public:
	// Constructor
	CEdgePort(CString Name, UINT uSled);

	// Destructor
	~CEdgePort(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IGpio
	void METHOD SetDirection(UINT n, BOOL fOutput);
	void METHOD SetState(UINT n, BOOL fState);
	BOOL METHOD GetState(UINT n);
	void METHOD EnableEvents(void);
	void METHOD SetIntHandler(UINT n, UINT uType, IEventSink *pSink, UINT uParam);
	void METHOD SetIntEnable(UINT n, BOOL fEnable);

protected:
	// Data Members
	ULONG    m_uRefs;
	CString	 m_Name;
	UINT	 m_uUnit;
	int	 m_fd;
	IMutex * m_pGuard;

	// Registers
	BOOL SetRegister(WORD wAddr, WORD wData);
	WORD GetRegister(WORD wAddr);
	BOOL IoCtl(int n, void *d);
	void Close(void);
};

// End of File

#endif
