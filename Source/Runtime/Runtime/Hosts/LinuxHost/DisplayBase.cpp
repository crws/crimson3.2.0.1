
#include "Intern.hpp"

#include "DisplayBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display Object Base Class
//

// Constructor

CDisplayBase::CDisplayBase(void)
{
	StdSetRef();

	m_pLock    = Create_Mutex();

	m_pBackRaw = "/sys/class/backlight/backlight/brightness";

	m_pBackMax = "/sys/class/backlight/backlight/max_brightness";

	m_uBackRaw = 0;

	m_uBackMax = 0;

	m_uBackVal = 0;

	m_fBackOn  = TRUE;

	m_cx       = 0;

	m_cy       = 0;
}

// Destructor

CDisplayBase::~CDisplayBase(void)
{
	m_pLock->Release();
}

// IUnknown

HRESULT CDisplayBase::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IDisplay);

	return E_NOINTERFACE;
}

ULONG CDisplayBase::AddRef(void)
{
	StdAddRef();
}

ULONG CDisplayBase::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CDisplayBase::Open(void)
{
	ReadBacklight();

	return TRUE;
}

// IDisplay

void CDisplayBase::Claim(void)
{
	m_pLock->Wait(FOREVER);
}

void CDisplayBase::Free(void)
{
	m_pLock->Free();
}

void CDisplayBase::GetSize(int &cx, int &cy)
{
	cx = m_cx;

	cy = m_cy;
}

BOOL CDisplayBase::SetBacklight(UINT pc)
{
	if( m_uBackVal != pc ) {

		if( pc <= 100 ) {

			UINT rv = (m_uBackMax * pc + 50) / 100;

			if( m_uBackRaw != rv ) {

				if( !m_fBackOn || WriteFile(m_pBackRaw, rv) ) {

					m_uBackVal = pc;

					m_uBackRaw = rv;

					return TRUE;
				}

				return FALSE;
			}

			m_uBackVal = pc;

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

UINT CDisplayBase::GetBacklight(void)
{
	return m_uBackVal;
}

BOOL CDisplayBase::EnableBacklight(BOOL fOn)
{
	if( m_fBackOn != fOn ) {

		if( WriteFile(m_pBackRaw, fOn ? m_uBackRaw : 0) ) {

			m_fBackOn = fOn;

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CDisplayBase::IsBacklightEnabled(void)
{
	return m_fBackOn;
}

// Implementation

void CDisplayBase::ReadBacklight(void)
{
	m_uBackMax = ReadFile(m_pBackMax, 100);

	m_uBackRaw = ReadFile(m_pBackRaw, 100);

	m_uBackVal = 100 * m_uBackRaw / m_uBackMax;

	m_fBackOn  = TRUE;
}

BOOL CDisplayBase::WriteFile(PCSTR pFile, UINT uData)
{
	int f;

	if( (f = _open(pFile, O_RDWR, 0)) > 0 ) {

		char sText[8];

		int  n = sprintf(sText, "%u\n", uData);

		if( _write(f, sText, n) == n ) {

			_close(f);

			return TRUE;
		}

		_close(f);
	}

	return FALSE;
}

UINT CDisplayBase::ReadFile(PCSTR pFile, UINT uData)
{
	int f, n;

	if( (f = _open(pFile, O_RDONLY, 0)) > 0 ) {

		char sText[8];

		if( (n = _read(f, sText, sizeof(sText))) < sizeof(sText) ) {

			sText[n] = 0;

			uData    = atoi(sText);
		}

		_close(f);
	}

	return uData;
}

// End of File
