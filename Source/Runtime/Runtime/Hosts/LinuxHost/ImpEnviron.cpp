
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Environment Implementation
//

// Data

static char *  none[] = { NULL };

global char ** environ = none;

// Code

void InitEnviron(void)
{
	int pid  = _getpid();

	int file = _open(CPrintf("/proc/%u/environ", pid), O_RDONLY, 0);

	if( file > 0 ) {

		int   size = 256;

		char *data = (char *) malloc(size);

		int   done = 0;

		for( ;;) {

			done += _read(file, data + done, size - done);

			if( done < size ) {

				break;
			}

			data = (char *) realloc(data, size <<= 1);
		}

		_close(file);

		int n = 0;

		for( int i = 0; i < done; i++ ) {

			if( !data[i] ) {

				n++;
			}
		}

		environ = New PTXT[n];

		char *p = data;

		for( int i = 0; i < n - 1; i++ ) {

			environ[i] = p;

			p += strlen(p);

			p += 1;
		}

		environ[n-1] = NULL;
	}
}

// End of File
