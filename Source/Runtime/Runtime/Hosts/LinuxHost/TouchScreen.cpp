
#include "Intern.hpp"

#include "TouchScreen.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <linux/input.h>

//////////////////////////////////////////////////////////////////////////
//
// Touch Screen Device Driver
//

// Constants

#define timeDelay	500

#define	timeRepeat	100

#define	timeHide	2500

#define touchValid	45	// TODO -- Why?

#define touchCount	20	// TODO -- Why?

#define cellSize	4

// Instantiator

IDevice * Create_TouchScreen(void)
{
	return New CTouchScreen;
}

// Constructor

CTouchScreen::CTouchScreen(void)
{
	StdSetRef();

	m_hThread = NULL;
	m_fTimer  = false;
	m_fd      = 0;
	m_td      = 0;
	m_ed      = 0;
}

// Destructor

CTouchScreen::~CTouchScreen(void)
{
	Close();
}

// IUnknown

HRESULT CTouchScreen::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ITouchScreen);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CTouchScreen::AddRef(void)
{
	StdAddRef();
}

ULONG CTouchScreen::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CTouchScreen::Open(void)
{
	// TODO -- Should we auto detect these pathnames?

	PCTXT pName[] = { "/dev/input/by-id/usb-VirtualBox_USB_Tablet-event-mouse",
			  "/dev/input/by-path/platform-TI-am335x-tsc-event"
	};

	for( int n = 0; n < elements(pName); n++ ) {

		if( (m_fd = _open(pName[n], O_RDONLY, 0)) > 0 ) {

			m_pMap    = NULL;

			m_fBeep   = false;

			m_fDrag   = false;

			m_hThread = CreateClientThread(this, 1, 800);

			return true;
		}
	}

	return false;
}

// ITouchScreen

void CTouchScreen::GetCellSize(int &xCell, int &yCell)
{
	xCell = cellSize;

	yCell = cellSize;
}

void CTouchScreen::GetDispCells(int &xDisp, int &yDisp)
{
	xDisp = m_xCells;

	yDisp = m_yCells;
}

void CTouchScreen::SetBeepMode(bool fBeep)
{
	m_fBeep = fBeep;
}

void CTouchScreen::SetDragMode(bool fDrag)
{
	// TODO -- Implement...

	m_fDrag = fDrag;
}

PCBYTE CTouchScreen::GetMap(void)
{
	return m_pMap;
}

void CTouchScreen::SetMap(PCBYTE pMap)
{
	m_pMap = pMap;
}

bool CTouchScreen::GetRaw(int &xRaw, int &yRaw)
{
	xRaw = m_xRaw;

	yRaw = m_yRaw;

	return true;
}

// IClientProcess

BOOL CTouchScreen::TaskInit(UINT uTask)
{
	m_td = _timerfd_create(1, 0);

	m_ed = _eventfd(0);

	AfxGetObject("input-d", 0, IInputQueue, m_pInput);

	AfxGetObject("display", 0, IDisplay, m_pDisplay);

	AfxGetObject("beeper", 0, IBeeper, m_pBeeper);

	m_pDisplay->GetSize(m_xDisp, m_yDisp);

	m_xCells = (m_xDisp + cellSize - 1) / cellSize;

	m_yCells = (m_yDisp + cellSize - 1) / cellSize;

	input_absinfo xabs = { 0 };
	input_absinfo yabs = { 0 };
	input_absinfo pabs = { 0 };

	_ioctl(m_fd, EVIOCGABS(ABS_X), &xabs);
	_ioctl(m_fd, EVIOCGABS(ABS_Y), &yabs);
	_ioctl(m_fd, EVIOCGABS(ABS_PRESSURE), &pabs);

	m_xMin = xabs.minimum;
	m_xMax = xabs.maximum;
	m_yMin = yabs.minimum;
	m_yMax = yabs.maximum;
	m_pMin = pabs.minimum;
	m_pMax = pabs.maximum;

	if( !m_pMax ) {

		m_pDisplay->QueryInterface(AfxAeonIID(IPointer), (void **) &m_pPointer);
	}
	else
		m_pPointer = NULL;

	m_fPress = false;

	m_nPress = 0;

	return true;
}

INT CTouchScreen::TaskExec(UINT uTask)
{
	for( ;;) {

		fd_set set;

		if( WaitForEvent(set) ) {

			if( FD_ISSET(m_ed, &set) ) {

				break;
			}

			if( FD_ISSET(m_fd, &set) ) {

				input_event ie[64];

				int b = _read(m_fd, ie, sizeof(ie));

				int c = b / sizeof(*ie);

				for( int n = 0; n < c; n++ ) {

					input_event &ev = ie[n];

					if( ev.type == EV_ABS ) {

						if( ev.code == ABS_X ) {

							m_xRaw = ev.value;

							m_xTmp = Decode(m_xRaw, m_xDisp, m_xMin, m_xMax);
						}

						if( ev.code == ABS_Y ) {

							m_yRaw = ev.value;

							m_yTmp = Decode(m_yRaw, m_yDisp, m_yMin, m_yMax);
						}

						if( ev.code == ABS_PRESSURE ) {

							m_pVal = Decode(ev.value, 1000, m_pMin, m_pMax);
						}
					}

					if( ev.type == EV_SYN ) {

						if( !m_pMax || m_pVal > touchValid ) {

							// TODO -- Do we need averaging?

							// TODO -- Apply the calibration data.

							m_xVal = m_xTmp;

							m_yVal = m_yTmp;
						}

						if( m_pMax ) {

							if( m_pVal ) {

								if( !m_fPress ) {

									if( m_pVal > touchValid ) {

										if( ++m_nPress >= touchCount ) {

											ev.type  = EV_KEY;
											ev.code  = BTN_MOUSE;
											ev.value = 1;
										}
									}
									else
										m_nPress = 0;
								}
							}
							else {
								if( m_fPress ) {

									ev.type  = EV_KEY;
									ev.code  = BTN_MOUSE;
									ev.value = 0;
								}

								m_nPress = 0;
							}
						}

						if( m_fDrag ) {

							// TODO -- How would we average dragging?

							if( m_fPress && ev.type == EV_SYN ) {

								if( m_xPress != m_xVal / cellSize || m_yPress != m_yVal / cellSize ) {

									m_xPress = m_xVal / cellSize;

									m_yPress = m_yVal / cellSize;

									StoreEvent(stateDrag);
								}
							}
						}

						if( m_pPointer ) {

							if( !m_fPress ) {

								SetWaitTimer(set, timeHide);
							}

							m_pPointer->SetPointerPos(m_xVal, m_yVal);

							m_pPointer->ShowPointer(true);
						}
					}

					if( ev.type == EV_KEY ) {

						if( ev.code == BTN_MOUSE ) {

							if( (m_fPress = ev.value) ) {

								m_xPress = m_xVal / cellSize;

								m_yPress = m_yVal / cellSize;

								if( (m_fValid = IsActive()) ) {

									SetWaitTimer(set, timeDelay);

									StoreEvent(stateDown);
								}
								else
									SetWaitTimer(set, 0);
							}
							else {
								if( m_pPointer ) {

									SetWaitTimer(set, timeHide);
								}
								else
									SetWaitTimer(set, 0);

								if( m_fValid ) {

									StoreEvent(stateUp);
								}
							}
						}
					}
				}
			}

			if( FD_ISSET(m_td, &set) ) {

				if( m_fPress ) {

					SetWaitTimer(set, timeRepeat);

					StoreEvent(stateRepeat);
				}
				else {
					if( m_pPointer ) {

						m_pPointer->ShowPointer(false);
					}

					SetWaitTimer(set, 0);
				}
			}

			continue;
		}

		AfxTrace("touchscreen: wait failed\n");
	}

	return 0;
}

void CTouchScreen::TaskStop(UINT uTask)
{
}

void CTouchScreen::TaskTerm(UINT uTask)
{
	AfxRelease(m_pDisplay);

	AfxRelease(m_pPointer);

	AfxRelease(m_pBeeper);

	_close(m_ed);

	_close(m_td);

	_close(m_fd);

	m_fd = 0;
}

// Implementation

void CTouchScreen::SetWaitTimer(fd_set &set, UINT ms)
{
	FD_CLR(m_td, &set);

	if( ms ) {

		_timerfd_settime(m_td, 0, ms, false);

		m_fTimer = true;

		return;
	}

	m_fTimer = false;
}

bool CTouchScreen::WaitForEvent(fd_set &set)
{
	int nfds = 0;

	FD_ZERO(&set);

	if( true ) {

		FD_SET(m_fd, &set);

		MakeMax(nfds, m_fd);
	}

	if( m_ed > 0 ) {

		FD_SET(m_ed, &set);

		MakeMax(nfds, m_ed);
	}

	if( m_fTimer ) {

		FD_SET(m_td, &set);

		MakeMax(nfds, m_td);
	}

	return _select(nfds+1, &set, NULL, NULL, FOREVER) >= 0;
}

bool CTouchScreen::Close(void)
{
	if( m_fd > 0 ) {

		INT64 n = 1;

		_write(m_ed, &n, sizeof(n));

		WaitThread(m_hThread, FOREVER);

		CloseThread(m_hThread);

		return true;
	}

	return false;
}

bool CTouchScreen::StoreEvent(UINT uType)
{
	if( m_pInput ) {

		CInput Input;

		Input.x.i.m_Type   = typeTouch;

		Input.x.i.m_State  = uType;

		Input.x.i.m_Local  = true;

		Input.x.d.t.m_XPos = m_xPress;

		Input.x.d.t.m_YPos = m_yPress;

		bool fOkay = m_pInput->Store(Input.m_Ref);

		if( uType == stateDown ) {

			if( m_fBeep && m_pBeeper ) {

				UINT uBeep = fOkay ? beepPress : beepFull;

				m_pBeeper->Beep(uBeep);
			}
		}

		return fOkay;
	}

	return false;
}

int CTouchScreen::Decode(int nRaw, int nDisp, int nMin, int nMax)
{
	return ((nRaw - nMin) * (nDisp - 1) + (nMax - nMin) / 2) / (nMax - nMin);
}

bool CTouchScreen::IsActive(void)
{
	if( !m_pDisplay->IsBacklightEnabled() ) {

		return true;
	}

	if( m_pMap ) {

		UINT n = m_xPress + m_xCells * m_yPress;

		BYTE b = (0x80 >> (n%8));

		return (m_pMap[n/8] & b) ? true : false;
	}

	return true;
}

// End of File
