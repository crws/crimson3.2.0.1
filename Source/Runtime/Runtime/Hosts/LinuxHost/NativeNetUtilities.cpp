
#include "Intern.hpp"

#include "NativeNetUtilities.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "Socket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Network Information
//

// Registration

global void Register_NativeNetUtilities(void)
{
	piob->RegisterSingleton("ip", 0, New CNativeNetUtilities);
}

global void Revoke_NativeNetUtilities(void)
{
	piob->RevokeSingleton("ip", 0);
}

// Constructor

CNativeNetUtilities::CNativeNetUtilities(void)
{
	m_fd       = _socket(AF_INET, SOCK_DGRAM, 0);

	m_pLock    = Create_Mutex();

	m_fForce   = false;

	m_wPingSeq = 0;

	StdSetRef();
}

// Destructor

CNativeNetUtilities::~CNativeNetUtilities(void)
{
	AfxRelease(m_pLock);

	_close(m_fd);
}

// IUnknown

HRESULT CNativeNetUtilities::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(INetUtilities);

	StdQueryInterface(INetUtilities);

	return E_NOINTERFACE;
}

ULONG CNativeNetUtilities::AddRef(void)
{
	StdAddRef();
}

ULONG CNativeNetUtilities::Release(void)
{
	StdRelease();
}

// INetUtilities

BOOL CNativeNetUtilities::SetInterfaceNames(CStringArray const &Names)
{
	CAutoLock Lock(m_pLock);

	m_fForce = !(m_Names = Names).IsEmpty();

	return TRUE;
}

BOOL CNativeNetUtilities::SetInterfaceDescs(CStringArray const &Descs)
{
	CAutoLock Lock(m_pLock);

	if( m_fForce ) {

		m_Descs = Descs;

		return TRUE;
	}

	return FALSE;
}

BOOL CNativeNetUtilities::SetInterfacePaths(CStringArray const &Paths)
{
	CAutoLock Lock(m_pLock);

	if( m_fForce ) {

		m_Paths = Paths;

		return TRUE;
	}

	return FALSE;
}

BOOL CNativeNetUtilities::SetInterfaceIds(CUIntArray const &Ids)
{
	CAutoLock Lock(m_pLock);

	if( m_fForce ) {

		m_Ids = Ids;

		return TRUE;
	}

	return FALSE;
}

UINT CNativeNetUtilities::GetInterfaceCount(void)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		return m_Names.GetCount();
	}

	return 0;
}

UINT CNativeNetUtilities::FindInterface(CString const &Name)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		for( UINT n = 0; n < m_Names.GetCount(); n++ ) {

			if( m_Names[n] == Name ) {

				return n;
			}
		}
	}

	return NOTHING;
}

UINT CNativeNetUtilities::FindInterface(UINT Id)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		for( UINT n = 0; n < m_Ids.GetCount(); n++ ) {

			if( m_Ids[n] == Id ) {

				return n;
			}
		}
	}

	return NOTHING;
}

BOOL CNativeNetUtilities::HasInterfaceType(UINT uType)
{
	// TODO -- Store the type and a type mask!!!

	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		for( UINT n = 0; n < m_Names.GetCount(); n++ ) {

			CString const &Name = m_Names[n];

			if( TypeFromName(Name) == uType ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

UINT CNativeNetUtilities::GetInterfaceType(UINT uFace)
{
	// TODO -- Store the type and a type mask!!!

	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		if( uFace < m_Names.GetCount() ) {

			CString const &Name = m_Names[uFace];

			return TypeFromName(Name);
		}
	}

	return IT_UNKNOWN;
}

UINT CNativeNetUtilities::GetInterfaceOrdinal(UINT uFace)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		if( uFace < m_Names.GetCount() ) {

			// LATER -- This isn't very pretty, but it works...

			UINT t = TypeFromName(m_Names[uFace]);

			UINT o = 0;

			for( UINT n = 0; n < uFace; n++ ) {

				if( TypeFromName(m_Names[n]) == t ) {

					o++;
				}
			}

			return o;
		}
	}

	return NOTHING;
}

BOOL CNativeNetUtilities::GetInterfaceName(UINT uFace, CString &Name)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		if( uFace < m_Names.GetCount() ) {

			Name = m_Names[uFace];

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CNativeNetUtilities::GetInterfaceDesc(UINT uFace, CString &Desc)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		if( uFace < m_Names.GetCount() ) {

			Desc = m_Descs[uFace];

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CNativeNetUtilities::GetInterfacePath(UINT uFace, CString &Path)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		if( uFace < m_Names.GetCount() ) {

			Path = m_Paths[uFace];

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CNativeNetUtilities::GetInterfaceMac(UINT uFace, MACADDR &Mac)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		if( uFace < m_Names.GetCount() ) {

			struct ifreq ifr = { 0 };

			strcpy(ifr.ifr_ifrn.ifrn_name, m_Names[uFace]);

			if( !_ioctl(m_fd, SIOCGIFHWADDR, &ifr) ) {

				if( ifr.ifr_ifru.ifru_hwaddr.sa_family == ARPHRD_ETHER ) {

					(CMacAddr &) Mac = PCBYTE(ifr.ifr_ifru.ifru_hwaddr.sa_data);

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CNativeNetUtilities::GetInterfaceAddr(UINT uFace, IPADDR &Addr)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		if( uFace < m_Names.GetCount() ) {

			struct ifreq ifr = { 0 };

			strcpy(ifr.ifr_ifrn.ifrn_name, m_Names[uFace]);

			if( !_ioctl(m_fd, SIOCGIFADDR, &ifr) ) {

				if( ifr.ifr_ifru.ifru_addr.sa_family == AF_INET ) {

					(CIpAddr &) Addr = (DWORD &) ifr.ifr_ifru.ifru_addr.sa_data[2];

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CNativeNetUtilities::GetInterfaceMask(UINT uFace, IPADDR &Mask)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		if( uFace < m_Names.GetCount() ) {

			struct ifreq ifr = { 0 };

			strcpy(ifr.ifr_ifrn.ifrn_name, m_Names[uFace]);

			if( !_ioctl(m_fd, SIOCGIFNETMASK, &ifr) ) {

				if( ifr.ifr_ifru.ifru_netmask.sa_family == AF_INET ) {

					(CIpAddr &) Mask = (DWORD &) ifr.ifr_ifru.ifru_netmask.sa_data[2];

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CNativeNetUtilities::GetInterfaceGate(UINT uFace, IPADDR &Gate)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		if( uFace < m_Names.GetCount() ) {

			CAutoFile File("/proc/net/route", "r");

			if( File ) {

				File.GetLine();

				CString Line;

				while( !(Line = File.GetLine()).IsEmpty() ) {

					CStringArray Face;

					Line.Replace('\n', '\t');

					Line.Remove(' ');

					Line.Tokenize(Face, '\t');

					if( Face.GetCount() > 3 ) {

						if( m_Names[uFace] == Face[0] ) {

							if( strtoul(PCTXT(Face[3]), NULL, 0x10) & 0x2 ) {

								Gate.m_dw = strtoul(PCTXT(Face[2]), NULL, 0x10);

								return TRUE;
							}
						}
					}
				}
			}
		}
	}

	return FALSE;
}

BOOL CNativeNetUtilities::GetInterfaceStatus(UINT uFace, CString &Status)
{
	CAutoLock Lock(m_pLock);

	if( IsInterfaceUp(uFace) ) {

		Status = "UP";

		return TRUE;
	}

	Status = "DOWN";

	return TRUE;
}

BOOL CNativeNetUtilities::IsInterfaceUp(UINT uFace)
{
	CAutoLock Lock(m_pLock);

	if( FindFaces() ) {

		if( uFace < m_Names.GetCount() ) {

			struct ifreq ifr = { 0 };

			strcpy(ifr.ifr_ifrn.ifrn_name, m_Names[uFace]);

			if( !_ioctl(m_fd, SIOCGIFFLAGS, &ifr) ) {

				if( ifr.ifr_ifru.ifru_flags & IFF_RUNNING ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

DWORD CNativeNetUtilities::GetOption(BYTE bType)
{
	CAutoLock Lock(m_pLock);

	// TODO -- Implement!!!

	return 0;
}

DWORD CNativeNetUtilities::GetOption(BYTE bType, UINT uFace)
{
	CAutoLock Lock(m_pLock);

	// TODO -- Implement!!!

	return 0;
}

BOOL CNativeNetUtilities::IsBroadcast(IPADDR const &IP)
{
	CAutoLock Lock(m_pLock);

	UINT uFace = FindRoute(IP);

	if( uFace < NOTHING ) {

		CIpAddr const &Ref = (CIpAddr const &) IP;

		CIpAddr Mask;

		if( GetInterfaceMask(uFace, Mask) ) {

			return Ref.IsBroadcast(Mask);
		}
	}

	return FALSE;
}

BOOL CNativeNetUtilities::GetBroadcast(IPADDR const &IP, IPADDR &Broad)
{
	CAutoLock Lock(m_pLock);

	UINT uFace = FindRoute(IP);

	if( uFace < NOTHING ) {

		CIpAddr &Ref = (CIpAddr &) IP;

		CIpAddr Addr;

		CIpAddr Mask;

		if( GetInterfaceAddr(uFace, Addr) && GetInterfaceMask(uFace, Mask) ) {

			Ref = Addr;

			Ref.MakeBroadcast(Mask);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CNativeNetUtilities::EnumBroadcast(UINT &uFace, IPADDR &Broad)
{
	CAutoLock Lock(m_pLock);

	CIpAddr &Ref = (CIpAddr &) Broad;

	for( UINT n = 0; n < m_Names.GetCount(); n++ ) {

		uFace = (uFace + 1) % m_Names.GetCount();

		CIpAddr Addr;

		CIpAddr Mask;

		if( GetInterfaceAddr(uFace, Addr) && GetInterfaceMask(uFace, Mask) ) {

			if( !Addr.IsLoopback() && !Addr.IsExperimental() ) {

				Ref = Addr;

				Ref.MakeBroadcast(Mask);

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CNativeNetUtilities::IsFastAddress(IPADDR const &IP)
{
	CAutoLock Lock(m_pLock);

	return TRUE;
}

BOOL CNativeNetUtilities::IsSafeAddress(IPADDR const &IP)
{
	CAutoLock Lock(m_pLock);

	return TRUE;
}

BOOL CNativeNetUtilities::GetDefaultGateway(IPADDR const &IP, IPADDR &Gate)
{
	CAutoLock Lock(m_pLock);

	return GetInterfaceGate(FindRoute(IP), Gate);
}

UINT CNativeNetUtilities::Ping(IPADDR const &IP, UINT uTimeout)
{
	CAutoLock Lock(m_pLock);

	int fd = _socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);

	if( fd >= 0 ) {

		BYTE bData[60] = { 0 };

		BYTE bRecv[80] = { 0 };

		bData[0] = 8;

		UINT uSize = sizeof(bData);

		for( UINT n = 8; n < uSize; n++ ) {

			bData[n] = 'A' + (n-8) % 26;
		}

		PWORD pData = PWORD(bData);

		pData[2] = WORD(DWORD(this));

		pData[3] = m_wPingSeq++;

		DWORD dwCheck = 0;

		for( UINT k = 0; k < uSize / 2; k++ ) {

			dwCheck += pData[k];
		}

		pData[1] = ~(LOWORD(dwCheck) + HIWORD(dwCheck));

		sockaddr_in addr = { 0 };

		addr.sin_family      = AF_INET;

		addr.sin_addr.s_addr = IP.m_dw;

		if( _connect(fd, (sockaddr *) &addr, sizeof(addr)) == 0 ) {

			if( _send(fd, bData, sizeof(bData), 0) > 0 ) {

				SetTimer(uTimeout);

				if( _select_read(fd, uTimeout) > 0 ) {

					int iRecv = _recv(fd, bRecv, sizeof(bRecv), 0);

					if( iRecv > 0 ) {

						UINT uLeft = GetTimer();

						_close(fd);

						if( iRecv == sizeof(bRecv) ) {

							if( !memcmp(bData + 8, bRecv + 28, uSize - 8) ) {

								return uTimeout - uLeft;
							}
						}

						return NOTHING;
					}
				}
			}
		}

		_close(fd);
	}

	return NOTHING;
}

// Implementation

BOOL CNativeNetUtilities::FindFaces(void)
{
	if( !m_fForce ) {

		struct ifconf conf;

		conf.ifc_len           = 0;

		conf.ifc_ifcu.ifcu_buf = NULL;

		if( !_ioctl(m_fd, SIOCGIFCONF, &conf) ) {

			UINT nf = conf.ifc_len / sizeof(struct ifreq);

			if( m_Names.GetCount() == nf ) {

				return TRUE;
			}
			else {
				conf.ifc_ifcu.ifcu_buf = malloc(conf.ifc_len);

				if( !_ioctl(m_fd, SIOCGIFCONF, &conf) ) {

					m_Names.Empty();

					for( UINT n = 0; n < nf; n++ ) {

						struct ifreq *ifr = conf.ifc_ifcu.ifcu_req + n;

						if( ifr->ifr_ifru.ifru_addr.sa_family == AF_INET ) {

							m_Names.Append(ifr->ifr_ifrn.ifrn_name);

							m_Descs.Append(ifr->ifr_ifrn.ifrn_name);
						}
					}

					free(conf.ifc_ifcu.ifcu_buf);

					return TRUE;
				}

				free(conf.ifc_ifcu.ifcu_buf);
			}
		}

		return FALSE;
	}

	return TRUE;
}

UINT CNativeNetUtilities::FindRoute(IPADDR const &IP)
{
	for( UINT n = 0; n < m_Names.GetCount(); n++ ) {

		CIpAddr Addr;

		CIpAddr Mask;

		if( GetInterfaceAddr(n, Addr) && GetInterfaceMask(n, Mask) ) {

			if( ((CIpAddr &) IP).OnSubnet(Addr, Mask) ) {

				return n;
			}
		}
	}

	return NOTHING;
}

UINT CNativeNetUtilities::TypeFromName(CString const &Name)
{
	if( Name == "lo" ) {

		return IT_LOOPBACK;
	}

	if( Name.StartsWith("eth") || Name.StartsWith("br") ) {

		return IT_ETHERNET;
	}

	if( Name.StartsWith("wwan") ) {

		return IT_CELLULAR;
	}

	if( Name.StartsWith("wlan") ) {

		return IT_WIFI;
	}

	if( Name.StartsWith("gre") || Name.StartsWith("tun") ) {

		return IT_TUNNEL;
	}

	if( Name.StartsWith("ipsec") ) {

		return IT_NETKEY;
	}

	return IT_UNKNOWN;
}

// End of File
