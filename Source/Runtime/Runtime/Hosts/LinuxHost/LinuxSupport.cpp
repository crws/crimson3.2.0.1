
#include "Intern.hpp"

#include "LinuxSupport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Linux Specific APIs
//

// Registration

void Register_LinuxSupport(void)
{
	piob->RegisterSingleton("os.linux", 0, (ILinuxSupport *) New CLinuxSupport);
}

void Revoke_LinuxSupport(void)
{
	piob->RevokeSingleton("os.linux", 0);
}

// Constructor

CLinuxSupport::CLinuxSupport(void)
{
	StdSetRef();
}

// IUnknown

HRESULT CLinuxSupport::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ILinuxSupport);

	StdQueryInterface(ILinuxSupport);

	return E_NOINTERFACE;
}

ULONG CLinuxSupport::AddRef(void)
{
	StdAddRef();
}

ULONG CLinuxSupport::Release(void)
{
	StdRelease();
}

// ILinuxSupport

int CLinuxSupport::CallProcess(char const *pCmd, char const * const *pArgs, char const *pStdOut, char const *pStdErr)
{
	return ::CallProcess(pCmd, pArgs, pStdOut, pStdErr);
}

int CLinuxSupport::CallProcess(char const *pCmd, char const *pArgs, char const *pStdOut, char const *pStdErr)
{
	return ::CallProcess(pCmd, pArgs, pStdOut, pStdErr);
}

int CLinuxSupport::CallProcess(char const * const *pArgs, char const *pStdOut, char const *pStdErr)
{
	return ::CallProcess(pArgs, pStdOut, pStdErr);
}

int CLinuxSupport::InitProcess(char const *pCmd, char const * const *pArgs)
{
	return ::InitProcess(pCmd, pArgs);
}

int CLinuxSupport::InitProcess(char const *pCmd, char const *pArgs)
{
	return ::InitProcess(pCmd, pArgs);
}

int CLinuxSupport::InitProcess(char const * const *pArgs)
{
	return ::InitProcess(pArgs);
}

int CLinuxSupport::TermProcess(int pid)
{
	return ::TermProcess(pid);
}

int CLinuxSupport::KillProcess(int pid)
{
	return ::KillProcess(pid);
}

// End of File
