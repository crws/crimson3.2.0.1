
#include "Intern.hpp"

#include "Semaphore.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Semaphore Object
//

// Instantiator

static IUnknown * Create_Semaphore(PCTXT pName)
{
	return New CSemaphore;
}

// Registration

global void Register_Semaphore(void)
{
	piob->RegisterInstantiator("exec.semaphore", Create_Semaphore);
}

// Constructor

CSemaphore::CSemaphore(void)
{
	StdSetRef();
}

// IUnknown

HRESULT CSemaphore::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(ISemaphore);

	return E_NOINTERFACE;
}

ULONG CSemaphore::AddRef(void)
{
	StdAddRef();
}

ULONG CSemaphore::Release(void)
{
	StdRelease();
}

// IWaitable

PVOID CSemaphore::GetWaitable(void)
{
	return NULL;
}

BOOL CSemaphore::Wait(UINT uWait)
{
	int c;

	while( (c = AtomicFetchAndSub(&m_state, 1)) <= 0 ) {

		AtomicFetchAndAdd(&m_state, 1);

		if( !WaitForChange(c, uWait, TRUE) ) {

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CSemaphore::HasRequest(void)
{
	return m_swait > 0;
}

// ISemaphore

void CSemaphore::Signal(UINT uCount)
{
	while( uCount-- ) {

		AtomicFetchAndAdd(&m_state, 1);

		WakeThreads(1);
	}
}

// End of File
