
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DisplayNone_HPP

#define INCLUDE_DisplayNone_HPP

//////////////////////////////////////////////////////////////////////////
//
// Dummy Display Object
//

class CDisplayNone : public IDisplay
{
public:
	// Constructor
	CDisplayNone(void);

	// Destructor
	~CDisplayNone(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IDisplay
	void METHOD Claim(void);
	void METHOD Free(void);
	void METHOD GetSize(int &cx, int &cy);
	BOOL METHOD SetBacklight(UINT pc);
	UINT METHOD GetBacklight(void);
	BOOL METHOD EnableBacklight(BOOL fOn);
	BOOL METHOD IsBacklightEnabled(void);
	void METHOD Update(PCVOID pData);

protected:
	// Data Members
	ULONG    m_uRefs;
	IMutex * m_pLock;
	int      m_cx;
	int      m_cy;
};

// End of File

#endif
