
#include "Intern.hpp"

#include "Executive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Object
//

// Instantiator

IUnknown * Create_Executive(void)
{
	return (IExecutive *) New CExecutive;
}

// Constructor

CExecutive::CExecutive(void)
{
	_sighandler(SIGUSR1, SigHandler);

	_sighandler(SIGPIPE, SigHandler);
}

// IExecutive

IThread * CExecutive::GetCurrentThread(void)
{
	return (IThread *) (_tls_valid ? _tls_get_data()->ithread : NULL);
}

UINT CExecutive::GetCurrentIndex(void)
{
	return GetCurrentThread()->GetIndex();
}

void CExecutive::Sleep(UINT uTime)
{
	DWORD t1 = GetTickCount();

	_sleep(uTime);

	DWORD t2 = GetTickCount();

	DWORD dt = t2 - t1;

	AddToSlept(dt);
}

BOOL CExecutive::ForceSleep(UINT uTime)
{
	CExecThread *pThread = (CExecThread *) _tls_get_data()->cthread;

	UINT         uSlept  = pThread->GetSlept();

	if( uSlept < uTime ) {

		Sleep(uTime - uSlept);

		pThread->GetSlept();

		return TRUE;
	}

	return FALSE;
}

UINT CExecutive::GetTickCount(void)
{
	return _clock_gettime_ms(1);
}

// Overridables

void CExecutive::AllocThreadObject(CMosExecThread * &pThread)
{
	pThread = New CExecThread(this);
}

// Signal Handler

void CExecutive::SigHandler(int n)
{
	if( n == SIGUSR1 ) {

		_tls_data *tls = _tls_get_data();

		if( tls ) {

			CExecThread *pThread = (CExecThread *) tls->cthread;

			// There is a race here if we try to kill a thread
			// before its TLS has even been initialize, so we
			// check to make sure this isn't invalid...

			if( DWORD(pThread) != 0x55555555 ) {

				if( pThread ) {

					pThread->SetCancelFlag();
				}
			}
		}
	}
}

// End of File
