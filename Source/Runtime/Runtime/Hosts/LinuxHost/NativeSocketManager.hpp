
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NativeSocketManager_HPP

#define INCLUDE_NativeSocketManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CBaseSocket;

//////////////////////////////////////////////////////////////////////////
//
// Native Socket Manager
//

class CNativeSocketManager : public IThreadNotify
{
public:
	// Constructor
	CNativeSocketManager(void);

	// Destructor
	~CNativeSocketManager(void);

	// Operations
	void AddSocket(CBaseSocket *pSock);
	void FreeSocket(CBaseSocket *pSock);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IThreadNotify
	UINT OnThreadCreate(IThread *pThread, UINT uIndex);
	void OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam);

	// Instance Pointer
	static CNativeSocketManager * m_pThis;

protected:
	// List Root
	struct CListRoot
	{
		CBaseSocket * m_pHead;
		CBaseSocket * m_pTail;
	};

	// Data Members
	ULONG m_uRefs;
};

// End of File

#endif
