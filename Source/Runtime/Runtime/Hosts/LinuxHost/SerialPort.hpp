
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_SerialPort_HPP

#define INCLUDE_SerialPort_HPP

//////////////////////////////////////////////////////////////////////////
//
// Serial Port
//

class CSerialPort : public IPortObject, public IClientProcess
{
public:
	// Constructor
	CSerialPort(CString Name, UINT uSled, UINT uPort);

	// Destructor
	~CSerialPort(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IPortObject
	void  METHOD Bind(IPortHandler *pHandler);
	void  METHOD Bind(IPortSwitch  *pSwitch);
	UINT  METHOD GetPhysicalMask(void);
	BOOL  METHOD Open(CSerialConfig const &Config);
	void  METHOD Close(void);
	void  METHOD Send(BYTE bData);
	void  METHOD SetBreak(BOOL fBreak);
	void  METHOD EnableInterrupts(BOOL fEnable);
	void  METHOD SetOutput(UINT uOutput, BOOL fOn);
	BOOL  METHOD GetInput(UINT uInput);
	DWORD METHOD GetHandle(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Data Members
	ULONG            m_uRefs;
	CString		 m_Name;
	UINT		 m_uSled;
	UINT		 m_uPort;
	UINT		 m_uUnit;
	UINT		 m_uTick;
	int		 m_fd;
	int		 m_timer;
	int		 m_term;
	int		 m_send;
	int		 m_done;
	IMutex         * m_pGuard;
	IThread        * m_pThread;
	IPortSwitch    * m_pSwitch;
	IPortHandler   * m_pHandler;
	BOOL             m_fOpen;
	CSerialConfig    m_Config;
	BYTE		 m_bSend;
	INT64		 m_uRecv;
	UINT		 m_uWait;

	// Handlers
	bool WaitForEvent(UINT &uMask);
	void OnTerm(void);
	BOOL OnTimer(void);
	BOOL OnRecv(void);
	void OnSend(void);

	// Comms
	void StartSend(BYTE bData);
	void WaitDone(void);

	// Objects
	void InitObjects(void);
	void StartTimer(void);
	void TermObjects(void);
	void InitThread(void);
	void TermThread(void);
	void SetEvent(int fd);
	void ClearEvent(int fd);
	BOOL ReadEvent(int fd);

	// Interrupts
	void EnterGuarded(void);
	void LeaveGuarded(void);

	// Port
	BOOL PortRecycle(void);
	BOOL PortOpen(void);
	void PortClose(void);
	BOOL PortSetFormat(void);
	BOOL PortSetPhysical(void);
};

// End of File

#endif
