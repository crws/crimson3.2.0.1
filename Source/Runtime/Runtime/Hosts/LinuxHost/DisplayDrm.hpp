
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DisplayDrm_HPP

#define INCLUDE_DisplayDrm_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DisplaySoftPoint.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <sys/ioctl.h>

#include <drm/drm.h>

#include <drm/drm_fourcc.h>

//////////////////////////////////////////////////////////////////////////
//
// Display Object using Direct Rendering Manager
//

class CDisplayDrm : public CDisplaySoftPoint
{
public:
	// Constructor
	CDisplayDrm(void);

	// Destructor
	~CDisplayDrm(void);

	// IDevice
	BOOL METHOD Open(void);

	// IDisplay
	void METHOD Update(PCVOID pData);

protected:
	// Update Modes
	enum
	{
		modeSimple      = 0,
		modeFlipSetCrtc = 1,
		modeFlipCommand = 2,
		modeFlipSync    = 3
	};

// Data Members
	PCSTR             m_pName;
	UINT              m_uMode;
	int               m_cb;
	int               m_mb;
	int               m_fd;
	UINT              m_crtc;
	UINT              m_page;
	UINT              m_cid;
	UINT              m_fb0;
	UINT              m_fb1;
	PVOID             m_pb0;
	PVOID             m_pb1;
	drm_mode_modeinfo m_mode;

	// Implementation
	BOOL OpenDisplay(void);
	void CloseDisplay(void);
	BOOL IoCtl(int n, void *p);
	BOOL SetMaster(void);
	BOOL DropMaster(void);
	BOOL CreateBuffer(UINT &id, PVOID &pb);
	BOOL GetEncoderCrtc(UINT &crtc, UINT enc);
	BOOL SetCrtc(UINT crtc, UINT fb, UINT64 conn, struct drm_mode_modeinfo &mode);
};

// End of File

#endif
