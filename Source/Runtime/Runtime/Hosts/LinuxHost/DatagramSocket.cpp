
#include "Intern.hpp"

#include "DatagramSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Datagram Socket Object
//

// Constructor

CDatagramSocket::CDatagramSocket(void)
{
}

// ISocket

HRESULT CDatagramSocket::GetPhase(UINT &Phase)
{
	if( m_uState == stateInit ) {

		Phase = PHASE_IDLE;

		return S_OK;
	}

	if( m_uState == stateOpen ) {

		Phase = PHASE_OPEN;

		return S_OK;
	}

	if( m_uState == stateError ) {

		Phase = PHASE_ERROR;

		return S_OK;
	}

	AfxAssert(FALSE);

	return E_FAIL;
}

HRESULT CDatagramSocket::Abort(void)
{
	return Close();
}

HRESULT CDatagramSocket::Close(void)
{
	if( m_uState < stateError ) {

		CloseHandle();

		return S_OK;
	}

	return E_FAIL;
}

// End of File
