
#include "Intern.hpp"

#include "DeviceIo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Device I/O
//

// Instantiator

IDevice * Create_DeviceIo(void)
{
	return New CDeviceIo;
}

// Constructor

CDeviceIo::CDeviceIo(void)
{
	m_pLock = Create_Mutex();

	StdSetRef();
}

// Destructor

CDeviceIo::~CDeviceIo(void)
{
	AfxRelease(m_pLock);
}

// IUnknown

HRESULT CDeviceIo::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IDeviceIo);

	return E_NOINTERFACE;
}

ULONG CDeviceIo::AddRef(void)
{
	StdAddRef();
}

ULONG CDeviceIo::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CDeviceIo::Open(void)
{
	if( Write("/sys/class/gpio/gpio170/direction", "high") ) {

		return TRUE;
	}

	return FALSE;
}

// IDeviceIo

bool CDeviceIo::GetDigitalIn(void)
{
	CAutoLock Lock(m_pLock);
	
	return atoi(Read("/sys/class/gpio/gpio165/value")) == 0;
}

UINT CDeviceIo::GetAnalogIn(void)
{
	CAutoLock Lock(m_pLock);

	return Scale(atoi(Read("/sys/bus/iio/devices/iio:device0/in_voltage2_raw")));
}

UINT CDeviceIo::GetPsuVoltage(void)
{
	CAutoLock Lock(m_pLock);

	return Scale(atoi(Read("/sys/bus/iio/devices/iio:device0/in_voltage0_raw")));
}

void CDeviceIo::SetDigitalOut(bool fState)
{
	CAutoLock Lock(m_pLock);

	Write("/sys/class/gpio/gpio170/value", fState ? "0" : "1");
}

bool CDeviceIo::GetDigitalOut(void)
{
	CAutoLock Lock(m_pLock);

	return atoi(Read("/sys/class/gpio/gpio170/value")) == 0;
}

// Implementation

bool CDeviceIo::Write(PCSTR pFile, PCSTR pData)
{
	int fd = _open(pFile, O_WRONLY, 0);

	if( fd >= 0 ) {

		_write(fd, pData, strlen(pData));

		_close(fd);

		return TRUE;
	}

	return FALSE;
}

CString CDeviceIo::Read(PCSTR pFile)
{
	int fd = _open(pFile, O_RDONLY, 0);

	if( fd >= 0 ) {

		char cData[64];

		int  nRead = _read(fd, cData, sizeof(cData));

		_close(fd);

		if( nRead > 0 ) {

			cData[nRead] = 0;

			return cData;
		}
	}

	return "";
}

UINT CDeviceIo::Scale(UINT counts)
{
	UINT64 ivolt = counts * 1800000LL / 4095LL;	// Microvolts at pin

	UINT64 rvolt = ivolt  * 26110LL   / 1210LL;	// Microvolts at input

	return UINT(rvolt / 1000LL);			// Millivolts at input
}

// End of File
