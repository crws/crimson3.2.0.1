
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Console
//

// Data

extern BOOL g_fService;

static BOOL m_fConsole = FALSE;

// Code

static void SigHandler(int n)
{
}

global BOOL InitConsole(void)
{
	// TODO -- Can we find a way to use the CAnsiDebugConsole
	// class here? We'd have to turn off local echo and ideally
	// work in character rather than line mode...

	m_fConsole = TRUE;

	_sighandler(SIGTTIN, SigHandler);

	return TRUE;
}

global BOOL ExecConsole(HTHREAD hThread)
{
	if( g_fService ) {

		AfxTrace("host: service mode\n");

		WaitThread(hThread, FOREVER);

		return TRUE;
	}

	if( m_fConsole ) {

		for(;;) {

			char sLine[1024] = { 0 };

			UINT uPtr = 0;

			for( ;;) {

				if( !_select_read(0, 100) ) {

					if( GetThreadExecState(hThread) >= 4 ) {

						return TRUE;
					}
				}
				else {
					int n = _read(0, sLine + uPtr, sizeof(sLine) - uPtr - 1);

					if( n > 0 ) {

						uPtr += n;

						if( sLine[uPtr - 1] == '\n' ) {

							if( uPtr > 1 ) {

								sLine[uPtr - 1] = 0;

								if( !strcasecmp(sLine, "exit") ) {

									return FALSE;
								}

								if( !strcasecmp(sLine, "help") ) {

									strcpy(sLine, "diag.help");
								}

								AfxPrint("\n");

								IDiagManager *pDiag = NULL;

								AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

								pDiag->RunCommand(NULL, sLine);

								pDiag->Release();

								AfxPrint("\n");
							}

							uPtr = 0;
						}
					}
					else {
						if( errno == EINTR || errno == EIO ) {

							Sleep(250);
						}

						if( errno == EEXIST ) {

							// We're detached from stdin as we're running
							// as a process from startup. We'll just sleep
							// and spin, but we need a better way to kill
							// Crimson when it's in this mode...

							Sleep(1000);
						}
					}
				}
			}
		}
	}

	return FALSE;
}

global void TermConsole(void)
{
	m_fConsole = FALSE;
}

// End of File
