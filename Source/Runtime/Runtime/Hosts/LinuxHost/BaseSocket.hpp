
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_BaseSocket_HPP

#define INCLUDE_BaseSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

struct sockaddr_in;

typedef int SOCKET;

//////////////////////////////////////////////////////////////////////////
//
// Base Socket Object
//

class CBaseSocket : public ISocket
{
public:
	// Constructor
	CBaseSocket(void);

	// Destructor
	virtual ~CBaseSocket(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ISocket
	HRM Listen(WORD Loc);
	HRM Connect(IPADDR const &IP, WORD Rem);
	HRM Connect(IPADDR const &IP, WORD Rem, WORD Loc);
	HRM Recv(PBYTE pData, UINT &uSize, UINT uTime);
	HRM Recv(PBYTE pData, UINT &uSize);
	HRM Send(PBYTE pData, UINT &uSize);
	HRM Recv(CBuffer * &pBuff, UINT uTime);
	HRM Recv(CBuffer * &pBuff);
	HRM Send(CBuffer   *pBuff);
	HRM GetLocal(IPADDR &IP);
	HRM GetRemote(IPADDR &IP);
	HRM GetLocal(IPADDR &IP, WORD &Port);
	HRM GetRemote(IPADDR &IP, WORD &Port);
	HRM SetOption(UINT uOption, UINT uValue);

	// Linked List
	PVOID	      m_pRoot;
	CBaseSocket * m_pNext;
	CBaseSocket * m_pPrev;

protected:
	enum
	{
		stateInit,
		stateListen,
		stateConnect,
		stateOpen,
		stateClosed,
		stateError
	};

	// Data Members
	ULONG  m_uRefs;
	SOCKET m_hSocket;
	UINT   m_uState;
	BOOL   m_fDebug;

	// Implementation
	void SetNonBlocking(void);
	void AllowDupAddress(void);
	BOOL LoadAddress(sockaddr_in &addr, IPADDR const &IP, WORD Port);
	BOOL LoadAddress(sockaddr_in &addr, WORD Port);
	void CloseHandle(void);
	void Debug(PCTXT pType, PCBYTE pData, UINT uSize);
};

// End of File

#endif
