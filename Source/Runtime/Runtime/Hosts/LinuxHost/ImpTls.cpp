
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// TLS Helper
//

clink void * __tls_get_addr(void *p)
{
	// This code implements the thunk-based TLS scheme using Aeon's
	// own TLS mechanism to store the data. We only have two slots
	// and we use the passed address as an opaque identifier when
	// allocating them. This method is never actually called as our
	// code relies on Aeon's own TLS mechanism.

	// See also https://redlion.atlassian.net/wiki/x/igBPEQ

	_tls_data *tls = _tls_get_data();

	if( tls->magic1 == _tls_magic && tls->magic2 == _tls_magic ) {

		if( !tls->addr1 || tls->addr1 == DWORD(p) ) {

			tls->addr1 = DWORD(p);

			return &tls->data1;
		}

		if( !tls->addr2 || tls->addr2 == DWORD(p) ) {

			tls->addr2 = DWORD(p);

			return &tls->data2;
		}
	}

	AfxAssert(FALSE);

	return NULL;
}

// End of File
