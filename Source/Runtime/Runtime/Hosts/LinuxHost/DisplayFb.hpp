
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DisplayFb_HPP

#define INCLUDE_DisplayFb_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DisplaySoftPoint.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Display Object using Frame Buffer
//

class CDisplayFb : public CDisplaySoftPoint
{
public:
	// Constructor
	CDisplayFb(void);

	// Destructor
	~CDisplayFb(void);

	// IDevice
	BOOL Open(void);

	// IDisplay
	void METHOD Update(PCVOID pData);

protected:
	// Data Members
	PCSTR m_pName;
	int   m_cb;
	int   m_mb;
	int   m_fb;
	PVOID m_pb;

	// Implementation
	BOOL OpenDisplay(void);
	void DisableCursor(void);
	void CloseDisplay(void);
	void FlipPointer(void);
};

// End of File

#endif
