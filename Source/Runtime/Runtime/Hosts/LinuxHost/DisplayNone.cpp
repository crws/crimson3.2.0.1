
#include "Intern.hpp"

#include "DisplayNone.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display Object Base Class
//

// Instantiator

IDevice * Create_DisplayNone(void)
{
	return (IDisplay *) New CDisplayNone;
}

// Constructor

CDisplayNone::CDisplayNone(void)
{
	StdSetRef();

	m_pLock = Create_Mutex();

	m_cx    = 800;

	m_cy    = 480;
}

// Destructor

CDisplayNone::~CDisplayNone(void)
{
	m_pLock->Release();
}

// IUnknown

HRESULT CDisplayNone::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IDisplay);

	return E_NOINTERFACE;
}

ULONG CDisplayNone::AddRef(void)
{
	StdAddRef();
}

ULONG CDisplayNone::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CDisplayNone::Open(void)
{
	return TRUE;
}

// IDisplay

void CDisplayNone::Claim(void)
{
	m_pLock->Wait(FOREVER);
}

void CDisplayNone::Free(void)
{
	m_pLock->Free();
}

void CDisplayNone::GetSize(int &cx, int &cy)
{
	cx = m_cx;

	cy = m_cy;
}

BOOL CDisplayNone::SetBacklight(UINT pc)
{
	return TRUE;
}

UINT CDisplayNone::GetBacklight(void)
{
	return 100;
}

BOOL CDisplayNone::EnableBacklight(BOOL fOn)
{
	return TRUE;
}

BOOL CDisplayNone::IsBacklightEnabled(void)
{
	return TRUE;
}

void CDisplayNone::Update(PCVOID pData)
{
}

// End of File
