
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_TouchNone_HPP

#define INCLUDE_TouchNone_HPP

//////////////////////////////////////////////////////////////////////////
//
// Dummy Touch Screen Driver
//

class CTouchNone : public ITouchScreen
{
public:
	// Constructor
	CTouchNone(void);

	// Destructor
	~CTouchNone(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// ITouchScreen
	void   METHOD GetCellSize(int &xCell, int &yCell);
	void   METHOD GetDispCells(int &xDisp, int &yDisp);
	void   METHOD SetBeepMode(bool fBeep);
	void   METHOD SetDragMode(bool fDrag);
	PCBYTE METHOD GetMap(void);
	void   METHOD SetMap(PCBYTE pMap);
	bool   METHOD GetRaw(int &xRaw, int &yRaw);

protected:
	// Data Members
	ULONG      m_uRefs;
	IDisplay * m_pDisplay;
	int        m_xDisp;
	int        m_yDisp;
	int	   m_xCells;
	int	   m_yCells;
};

// End of File

#endif
