
#include "Intern.hpp"

#include "DisplayFb.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <linux/fb.h>

//////////////////////////////////////////////////////////////////////////
//
// Display Object using Frame Buffer
//

// Instantiator

IDevice * Create_DisplayFb(void)
{
	return (IDisplay *) New CDisplayFb;
}

// Constructor

CDisplayFb::CDisplayFb(void)
{
	StdSetRef();

	m_pName = "/dev/fb0";

	m_pb    = 0;
}

// Destructor

CDisplayFb::~CDisplayFb(void)
{
	CloseDisplay();
}

// IDevice

BOOL CDisplayFb::Open(void)
{
	if( OpenDisplay() ) {

		m_pBuff = PDWORD(m_pb);

		return CDisplaySoftPoint::Open();
	}

	return FALSE;
}

// IDisplay

void CDisplayFb::Update(PCVOID pData)
{
	if( m_pb ) {

		LockPointer(true);

		SavePointer(PDWORD(pData));

		DrawPointer(PDWORD(pData));

		PCDWORD ps = PCDWORD(pData);

		PDWORD  pd = PDWORD(m_pb);

		int     np = m_cx * m_cy;

		while( np-- ) {

			*pd++ = HostToMotor(*ps++) >> 8;
		}

		LoadPointer(PDWORD(pData));

		FlipPointer();

		LockPointer(false);
	}
}

// Implementation

BOOL CDisplayFb::OpenDisplay(void)
{
	if( (m_fb = _open(m_pName, O_RDWR, 0)) > 0 ) {

		struct fb_var_screeninfo vinfo;

		_ioctl(m_fb, FBIOGET_VSCREENINFO, &vinfo);

		if( vinfo.bits_per_pixel == 32 ) {

			m_cx = vinfo.xres;

			m_cy = vinfo.yres;

			m_cb = m_cx * m_cy * 4;

			m_mb = ((m_cb + ((1<<12)-1)) >> 12) << 12;

			if( (m_pb = _mmap(0, m_mb, PROT_READ | PROT_WRITE, MAP_SHARED, m_fb, 0)) ) {

				memset(m_pb, 0, m_cb);

				DisableCursor();

				return TRUE;
			}
		}

		_close(m_fb);

		m_fb = 0;
	}

	AfxTrace("display: failed to open fb device\n");

	return FALSE;
}

void CDisplayFb::DisableCursor(void)
{
	// TODO -- Implement!!!
}

void CDisplayFb::CloseDisplay(void)
{
	if( m_fb > 0 ) {

		if( m_pb > 0 ) {

			_munmap(m_pb, m_cb);
		}

		_close(m_fb);
	}
}

void CDisplayFb::FlipPointer(void)
{
	for( UINT n = 0; n < m_uSave; n++ ) {

		DWORD flip = HostToMotor(m_pSave[n]) >> 8;

		m_pSave[n] = flip;
	}
}

// End of File
