
#include "Intern.hpp"

#include "Waitable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic Waitable Object
//

// Constructor

CWaitable::CWaitable(void)
{
	m_swait = 0;

	m_state = 0;
}

// Implementation

BOOL CWaitable::WaitForChange(int nState, BOOL fAlert)
{
	UINT uWait = FOREVER;

	return WaitForChange(nState, fAlert);
}

BOOL CWaitable::WaitForChange(int nState, UINT &uWait, BOOL fAlert)
{
	if( uWait ) {

		if( fAlert ) {

			CheckThreadCancellation();
		}

		AtomicFetchAndAdd(&m_swait, 1);

		DWORD t1 = GetTickCount();

		int   c;

		if( !(c = _futex_wait(&m_state, nState, uWait)) || c == -EAGAIN || c == -EINTR ) {

			AtomicFetchAndSub(&m_swait, 1);

			DWORD t2 = GetTickCount();

			DWORD dt = t2 - t1;

			AddToSlept(dt);

			if( fAlert ) {

				CheckThreadCancellation();
			}

			if( c == -EINTR ) {

				if( uWait < FOREVER ) {

					if( dt >= uWait ) {

						return FALSE;
					}

					uWait -= dt;
				}
			}

			return TRUE;
		}

		AtomicFetchAndSub(&m_swait, 1);
	}

	return FALSE;
}

BOOL CWaitable::WakeThreads(int nThread)
{
	if( m_swait && _futex_wake(&m_state, nThread) ) {

		_sched_yield();

		return TRUE;
	}

	return FALSE;
}

// End of File
