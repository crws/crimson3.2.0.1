
#include "Intern.hpp"

#include "SdHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Windows Emulated SD Host
//

// Instantiator

IDevice * Create_SdHost(void)
{
	return (ISdHost *) New CSdHost;
}

// Constructor

CSdHost::CSdHost(void)
{
	StdSetRef();

	m_uSize  = 4 * 1024 * 1024;

	m_fMap   = FALSE;

	m_uSects = m_uSize / 512;

	m_Name   = "sd";
}

// Destructor

CSdHost::~CSdHost(void)
{
}

// IUnknown

HRESULT CSdHost::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ISdHost);

	return E_NOINTERFACE;
}

ULONG CSdHost::AddRef(void)
{
	StdAddRef();
}

ULONG CSdHost::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CSdHost::Open(void)
{
	AllocData();

	return TRUE;
}

// ISdHost

BOOL CSdHost::IsCardReady(void)
{
	return TRUE;
}

UINT CSdHost::GetSectorCount(void)
{
	return m_uSects;
}

UINT CSdHost::GetSerialNumber(void)
{
	return 0x1234568;
}

void CSdHost::AttachCardEvent(IEvent *pEvent)
{
	pEvent->Set();
}

BOOL CSdHost::WriteSector(UINT uSect, PCBYTE pData)
{
	lseek(m_fd, uSect * 512, SEEK_SET);

	write(m_fd, pData, 512);

	return TRUE;
}

BOOL CSdHost::ReadSector(UINT uSect, PBYTE pData)
{
	lseek(m_fd, uSect * 512, SEEK_SET);

	read(m_fd, pData, 512);

	return TRUE;
}

// End of File
