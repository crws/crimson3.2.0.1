
#include "Intern.hpp"

#include "Platform.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Memory Serialization
//

clink void __malloc_serialize(bool);

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice * Create_InputQueue(void);
extern IDevice * Create_DisplayFb(void);
extern IDevice * Create_DisplayDrm(void);
extern IDevice * Create_DisplayNone(void);
extern IDevice * Create_TouchScreen(void);
extern IDevice * Create_TouchNone(void);
extern IDevice * Create_Entropy(void);
extern IDevice * Create_Security(void);
extern IDevice * Create_BaseSerial(UINT);
extern IDevice * Create_PipeSerial(UINT);
extern IDevice * Create_SledSerial(UINT, UINT);
extern IDevice * Create_NandMemory(void);
extern IDevice * Create_SerialMemory(void);
extern IDevice * Create_SdHost(void);
extern IDevice * Create_EdgePort(UINT);
extern IDevice * Create_TimeZone(void);
extern IDevice * Create_DeviceIo(void);

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern void Register_NativeSocketManager(void);
extern void Register_NativeTcpSocket(void);
extern void Register_NativeUdpSocket(void);
extern void Register_NativeRawSocket(void);
extern void Register_NativeDnsResolver(void);
extern void Register_NativeNetUtilities(void);
extern void Register_PosixFileCopier(void);
extern void Register_LinuxSupport(void);

extern void Revoke_NativeSocketManager(void);
extern void Revoke_NativeTcpSocket(void);
extern void Revoke_NativeUdpSocket(void);
extern void Revoke_NativeRawSocket(void);
extern void Revoke_NativeDnsResolver(void);
extern void Revoke_NativeNetUtilities(void);
extern void Revoke_PosixFileCopier(void);
extern void Revoke_LinuxSupport(void);

//////////////////////////////////////////////////////////////////////////
//
// Platform Object
//

// Instantiator

IDevice * Create_Platform(void)
{
	__malloc_serialize(true);

	return New CPlatform;
}

// Constructor

CPlatform::CPlatform(void)
{
	m_uSleds = 0;
}

// Destructor

CPlatform::~CPlatform(void)
{
	Revoke_PosixFileCopier();

	Revoke_NativeTcpSocket();

	Revoke_NativeUdpSocket();

	Revoke_NativeRawSocket();

	Revoke_NativeDnsResolver();

	Revoke_NativeNetUtilities();

	Revoke_NativeSocketManager();

	Revoke_LinuxSupport();

	piob->RevokeGroup("dev.");

	__malloc_serialize(false);
}

// IUnknown

HRESULT CPlatform::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IPortSwitch);

	return CPlatformBase::QueryInterface(riid, ppObject);
}

ULONG CPlatform::AddRef(void)
{
	return CPlatformBase::AddRef();
}

ULONG CPlatform::Release(void)
{
	return CPlatformBase::Release();
}

// IDevice

BOOL CPlatform::Open(void)
{
	MakeDevice("dev.input-d", 1, Create_InputQueue);

	MakeDevice("dev.display", 2, Create_DisplayDrm, Create_DisplayFb);

	MakeDevice("dev.touch", 1, Create_TouchScreen);

	MakeDeviceType("dev.uart", 1, Create_BaseSerial);

	MakeDeviceType("dev.pipe", 1, Create_PipeSerial);

	MakeDevice("dev.entropy", 1, Create_Entropy);

	MakeDevice("dev.nand", 1, Create_NandMemory);

	MakeDevice("dev.fram", 1, Create_SerialMemory);

	MakeDevice("dev.tz", 1, Create_TimeZone);

	Register_LinuxSupport();

	RegisterBaseCommon();

	UINT n = 0;

	for( UINT s = 1; s <= 3; s++ ) {

		IDevice *pDevice = Create_EdgePort(s);

		piob->RegisterSingleton("dev.sled-edge", s-1, pDevice);

		for( UINT p = 0; p < 2; p++ ) {

			IDevice *pDevice = Create_SledSerial(s, p);

			piob->RegisterSingleton("dev.sled-uart", n++, pDevice);
		}
	}

	if( MakeDevice("dev.security", 1, Create_Security) ) {

		Register_NativeSocketManager();

		Register_NativeTcpSocket();

		Register_NativeUdpSocket();

		Register_NativeRawSocket();

		Register_NativeDnsResolver();

		Register_NativeNetUtilities();

		Register_PosixFileCopier();

		FindDeviceInfo();

		if( m_Model == "DA50" ) {

			MakeDevice("dev.devio", 1, Create_DeviceIo);
		}

		m_DefName.Printf("%s-%2.2x%2.2x%2.2x",
				 PCSTR(m_Model.Left(4)),
				 m_Info.m_bMac0[3],
				 m_Info.m_bMac0[4],
				 m_Info.m_bMac0[5]
		);

		return TRUE;
	}

	return FALSE;
}

// IPlatform

PCTXT CPlatform::GetFamily(void)
{
	return "FlexEdge(TM)";
}

PCTXT CPlatform::GetModel(void)
{
	return m_Model;
}

PCTXT CPlatform::GetVariant(void)
{
	return m_Variant;
}

UINT CPlatform::GetFeatures(void)
{
	return	0 * rfGraphiteModules |
		1 * rfSerialModules   |
		1 * rfDisplay;
}

BOOL CPlatform::IsService(void)
{
	return FALSE;
}

BOOL CPlatform::IsEmulated(void)
{
	return FALSE;
}

BOOL CPlatform::HasMappings(void)
{
	return FALSE;
}

void CPlatform::ResetSystem(void)
{
	_sync();

	_exit_group(66);
}

void CPlatform::SetWebAddr(PCTXT pAddr)
{
}

// IPortSwitch

UINT CPlatform::GetCount(UINT uUnit)
{
	return 1;
}

UINT CPlatform::GetMask(UINT uUnit)
{
	if( uUnit == 0 ) {

		return Bit(physicalRS232);
	}

	if( uUnit == 1 ) {

		if( m_Model == "DA50" || m_Variant == "0G" ) {

			return Bit(physicalRS485) | Bit(physicalRS422Master) | Bit(physicalRS422Slave);
		}

		return Bit(physicalRS232);
	}

	if( uUnit == 2 ) {

		return Bit(physicalRS485) | Bit(physicalRS422Master) | Bit(physicalRS422Slave);
	}

	if( uUnit >= 10 && uUnit < 40 ) {

		UINT uSled = uUnit / 10;

		UINT uPort = uUnit % 10;

		if( uPort < 2 ) {

			UINT uIdent = GetEdge(uSled-1, 17, 0x03);

			switch( uIdent ) {

				case 0:
					return Bit(physicalRS232);

				case 1:
					return Bit(physicalRS485) | Bit(physicalRS422Master) | Bit(physicalRS422Slave);

				case 2:
					return uPort ? Bit(physicalRS232) : Bit(physicalRS485) | Bit(physicalRS422Master) | Bit(physicalRS422Slave);
			}
		}

		return 0;
	}

	return 0;
}

UINT CPlatform::GetType(UINT uUnit, UINT uLog)
{
	if( uUnit == 0 ) {

		return physicalRS232;
	}

	if( uUnit == 1 ) {

		if( m_Model == "DA50" || m_Variant == "0G" ) {

			return physicalRS485;
		}

		return physicalRS232;
	}

	if( uUnit == 2 ) {

		return physicalRS485;
	}

	if( uUnit >= 10 && uUnit < 40 ) {

		UINT uSled = uUnit / 10;

		UINT uPort = uUnit % 10;

		if( uPort < 2 ) {

			UINT uIdent = GetEdge(uSled-1, 17, 0x03);

			switch( uIdent ) {

				case 0:
					return physicalRS232;

				case 1:
					return physicalRS485;

				case 2:
					return uPort ? physicalRS232 : physicalRS485;
			}
		}

		return physicalNone;
	}

	return 0;
}

BOOL CPlatform::EnablePort(UINT uUnit, BOOL fEnable)
{
	switch( uUnit ) {

		case 0:
			return SetGpio(3, 25, fEnable);

		case 1:
			return SetGpio(3, 17, fEnable);

		case 2:
			return SetGpio(3, 16, fEnable);

		case 3:
			return SetGpio(5, 25, fEnable);

		case 10:
			return SetEdge(0, 31, fEnable);

		case 11:
			return SetEdge(0, 30, fEnable);

		case 20:
			return SetEdge(1, 31, fEnable);

		case 21:
			return SetEdge(1, 30, fEnable);

		case 30:
			return SetEdge(2, 31, fEnable);

		case 31:
			return SetEdge(2, 30, fEnable);
	}

	return FALSE;
}

BOOL CPlatform::SetPhysical(UINT uUnit, BOOL fRS485)
{
	if( uUnit == 1 ) {

		if( m_Model == "DA50" || m_Variant == "0G" ) {

			return fRS485;
		}
	}

	if( uUnit == 2 ) {

		return fRS485;
	}

	if( uUnit >= 10 && uUnit < 40 ) {

		UINT uSled = uUnit / 10;

		UINT uPort = uUnit % 10;

		if( uPort < 2 ) {

			UINT uIdent = GetEdge(uSled-1, 17, 0x03);

			if( uIdent == 1 || (uIdent == 2  && uPort == 0) ) {

				return fRS485;
			}
		}
	}

	return FALSE;
}

BOOL CPlatform::SetFull(UINT uUnit, BOOL fFull)
{
	if( uUnit == 1 ) {

		if( m_Model == "DA50" || m_Variant == "0G" ) {

			return SetGpio(0, 12, !fFull);
		}
	}

	if( uUnit == 2 ) {

		return SetGpio(0, 13, !fFull);
	}

	if( uUnit == 10 ) {

		return SetEdge(0, 28, !fFull);
	}

	if( uUnit == 11 ) {

		return SetEdge(0, 29, !fFull);
	}

	if( uUnit == 20 ) {

		return SetEdge(1, 28, !fFull);
	}

	if( uUnit == 21 ) {

		return SetEdge(1, 29, !fFull);
	}

	if( uUnit == 30 ) {

		return SetEdge(2, 28, !fFull);
	}

	if( uUnit == 31 ) {

		return SetEdge(2, 29, !fFull);
	}

	return FALSE;
}

BOOL CPlatform::SetMode(UINT uUnit, BOOL fAuto)
{
	return FALSE;
}

// Implementation

BOOL CPlatform::FindDeviceInfo(void)
{
	AfxGetAutoObject(pSec, "dev.security", 0, IFeatures);

	if( pSec ) {

		if( pSec->GetDeviceInfo(m_Info) ) {

			CString Full(PCSTR(m_Info.m_bModel), 6);

			m_Model   = Full.Left(4);

			m_Variant = Full.Mid(4);

			m_Serial  = CString(PCSTR(m_Info.m_bSerial), 14);

			m_Model.MakeLower();

			if( m_Model == "da70" ) {

				m_uSleds = 3;
			}

			if( m_Model == "da50" ) {

				m_uSleds = 1;
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CPlatform::SetGpio(UINT uPort, UINT uGpio, UINT uState)
{
	CPrintf Name("/sys/class/gpio/gpio%d/value", 32 * uPort + uGpio);

	int fd = _open(Name, O_WRONLY, 0);

	if( fd >= 0 ) {

		char cData = uState ? '1' : '0';

		_write(fd, &cData, sizeof(cData));

		_close(fd);

		return TRUE;
	}

	return FALSE;
}

BOOL CPlatform::SetEdge(UINT uDev, UINT uGpio, UINT uState)
{
	AfxGetAutoObject(pEdge, "dev.sled-edge", uDev, IGpio);

	if( pEdge ) {

		for( ; !pEdge->Open(); Sleep(100) );

		pEdge->SetDirection(uGpio, TRUE);

		pEdge->SetState(uGpio, uState);

		return TRUE;
	}

	return FALSE;
}

BOOL CPlatform::GetEdge(UINT uDev, UINT uGpio)
{
	AfxGetAutoObject(pEdge, "dev.sled-edge", uDev, IGpio);

	if( pEdge ) {

		for( ; !pEdge->Open(); Sleep(100) );

		pEdge->SetDirection(uGpio, FALSE);

		return pEdge->GetState(uGpio);
	}

	return FALSE;
}

UINT CPlatform::GetEdge(UINT uDev, UINT uGpio, UINT uMask)
{
	AfxGetAutoObject(pEdge, "dev.sled-edge", uDev, IGpio);

	if( pEdge ) {

		for( ; !pEdge->Open(); Sleep(100) );

		UINT uData = 0;

		UINT uTest = 1;

		while( uMask ) {

			if( uMask & 1 ) {

				pEdge->SetDirection(uGpio, FALSE);

				if( pEdge->GetState(uGpio) ) {

					uData |= uTest;
				}
			}

			uMask >>= 1;

			uTest <<= 1;

			uGpio++;
		}

		return uData;
	}

	return 0;
}

// End of File
