
#include "Intern.hpp"

#include "Memory.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Linux Emulated Memory Device
//

// Constructor

CMemory::CMemory(void)
{
	m_uSize = 0;

	m_fMap  = TRUE;

	m_pData = NULL;

	m_fd    = 0;
}

// Destructor

CMemory::~CMemory(void)
{
	FreeData();
}

// Implementation

void CMemory::AllocData(void)
{
	BOOL i  = FALSE;

	m_Name  = FindPath() + m_Name + ".data";

	m_fd    = _open(m_Name, O_RDWR, 0);

	if( m_fd > 0 ) {

		struct stat stat;

		_fstat(m_fd, &stat);

		if( stat.st_size != m_uSize ) {

			_close(m_fd);

			_unlink(m_Name);

			m_fd = -1;
		}
	}

	if( m_fd < 0 ) {

		m_fd = _open(m_Name, O_RDWR | O_EXCL | O_CREAT, 0644);

		i    = TRUE;
	}

	if( m_fd > 0 ) {

		if( i ) {

			AfxTrace("host: initializing %s\n", PCTXT(m_Name));

			UINT  uData = 65536;

			PBYTE pData = New BYTE[uData];

			memset(pData, 0xFF, uData);

			UINT uFill = m_uSize;

			while( uFill ) {

				UINT  uLump = min(uFill, uData);

				DWORD uDone = 0;

				_write(m_fd, pData, uLump);

				uFill -= uLump;
			}

			_lseek(m_fd, 0, SEEK_SET);

			delete[] pData;
		}

		if( !m_fMap ) {

			return;
		}

		if( (m_pData = PBYTE(_mmap(NULL, m_uSize, PROT_READ | PROT_WRITE, MAP_SHARED, m_fd, 0))) ) {

			return;
		}

		close(m_fd);

		m_fd = 0;
	}

	if( m_fMap ) {

		if( (m_pData = New BYTE[m_uSize]) ) {

			memset(m_pData, 0xFF, m_uSize);

			return;
		}
	}

	AfxAssert(FALSE);
}

void CMemory::FreeData(void)
{
	if( m_pData || !m_fMap ) {

		if( m_fd ) {

			if( m_pData ) {

				_munmap(m_pData, m_uSize);
			}

			_close(m_fd);

			return;
		}

		delete[] m_pData;
	}
}

CString CMemory::FindPath(void)
{
	char Path[MAX_PATH] = { 0 };

	strcpy(Path, "/vap/opt/crimson/memory/");

	_mkdir("/vap", 0755);
	_mkdir("/vap/opt", 0755);
	_mkdir("/vap/opt/crimson", 0755);
	_mkdir("/vap/opt/crimson/memory", 0755);

	return Path;
}

void CMemory::Commit(void)
{
	_fsync(m_fd);
}

// End of File
