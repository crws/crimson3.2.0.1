
#include "Intern.hpp"

#include <sys/wait.h>

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Process Invocation
//

static void KillProc(IThread *pThread, void *pArg)
{
	_kill(int(pArg), SIGKILL);
}

global int CallProcess(char const *pCmd, char const * const *pArgs, char const *pStdOut, char const *pStdErr)
{
	UINT n;

	for( n = 0; pArgs && pArgs[n]; n++ );

	CAutoArray<char const *> List(2+n);

	UINT c = 0;

	List[c++] = pCmd;

	for( UINT a = 0; a < n; List[c++] = PCTXT(pArgs[a++]) );

	List[c++] = NULL;

	return CallProcess(List, pStdOut, pStdErr);
}

global int CallProcess(char const *pCmd, char const *pArgs, char const *pStdOut, char const *pStdErr)
{
	CStringArray Args;

	CString(pArgs).Tokenize(Args, ' ', '\"');

	UINT n = Args.GetCount();

	CAutoArray<char const *> List(2+n);

	UINT c = 0;

	List[c++] = pCmd;

	for( UINT a = 0; a < n; List[c++] = PCTXT(Args[a++]) );

	List[c++] = NULL;

	return CallProcess(List, pStdOut, pStdErr);
}

global int CallProcess(char const * const *pArgs, char const *pStdOut, char const *pStdErr)
{
	int pid = _fork();

	if( pid >= 0 ) {

		if( !pid ) {

			_chdir("/");

			if( !pStdOut || strcmp(pStdOut, ".") ) {

				if( !pStdOut ) {

					_dup2(_open("/./dev/null", O_WRONLY | O_CREAT, 0644), 1);
				}
				else {
					_unlink(pStdOut);

					_dup2(_open(pStdOut, O_WRONLY | O_CREAT, 0644), 1);
				}
			}

			if( !pStdErr || strcmp(pStdErr, ".") ) {

				if( !pStdErr ) {

					_dup2(_open("/./dev/null", O_WRONLY | O_CREAT, 0644), 2);
				}
				else {
					if( !strcmp(pStdOut, pStdErr) ) {

						_dup2(1, 2);
					}
					else {
						_unlink(pStdErr);

						_dup2(_open(pStdErr, O_WRONLY | O_CREAT, 0644), 2);
					}
				}
			}

			_close_on_exec();

			char * const pEnv[] = { NULL };

			_execve(pArgs[0], pArgs, pEnv);

			_exit(255);
		}
		else {
			int status;

			GuardThread(KillProc, (void *) pid);

			if( _waitpid(pid, &status) == pid ) {

				if( WIFEXITED(status) ) {

					int code = WEXITSTATUS(status);

					if( code < 255 ) {

						GuardThread(FALSE);

						return code;
					}
				}
			}

			GuardThread(FALSE);
		}

	}

	return -1;
}

global int InitProcess(char const *pCmd, char const * const *pArgs)
{
	UINT n;

	for( n = 0; pArgs && pArgs[n]; n++ );

	CAutoArray<char const *> List(2+n);

	UINT c = 0;

	List[c++] = pCmd;

	for( UINT a = 0; a < n; List[c++] = PCTXT(pArgs[a++]) );

	List[c++] = NULL;

	return InitProcess(List);
}

global int InitProcess(char const *pCmd, char const *pArgs)
{
	CStringArray Args;

	CString(pArgs).Tokenize(Args, ' ', '\"');

	UINT n = Args.GetCount();

	CAutoArray<char const *> List(2+n);

	UINT c = 0;

	List[c++] = pCmd;

	for( UINT a = 0; a < n; List[c++] = PCTXT(Args[a++]) );

	List[c++] = NULL;

	return InitProcess(List);
}

global int InitProcess(char const * const *pArgs)
{
	int pid = _fork();

	if( pid >= 0 ) {

		if( !pid ) {

			_chdir("/");

			_dup2(_open("/./dev/null", O_WRONLY | O_CREAT, 0644), 1);
		
			_dup2(_open("/./dev/null", O_WRONLY | O_CREAT, 0644), 2);

			_close_on_exec();

			char * const pEnv[] = { NULL };

			_execve(pArgs[0], pArgs, pEnv);

			_exit(255);
		}

		return pid;
	}

	return -1;
}

global int TermProcess(int pid)
{
	// TODO -- This should have a timeout!!!

	int status;

	GuardThread(KillProc, (void *) pid);

	_kill(pid, SIGTERM);

	if( _waitpid(pid, &status) == pid ) {

		if( WIFEXITED(status) ) {

			int code = WEXITSTATUS(status);

			if( code < 255 ) {

				GuardThread(FALSE);

				return code;
			}
		}
	}

	GuardThread(FALSE);

	return -1;
}

global int KillProcess(int pid)
{
	int status;

	GuardThread(KillProc, (void *) pid);

	_kill(pid, SIGKILL);

	if( _waitpid(pid, &status) == pid ) {

		if( WIFEXITED(status) ) {

			int code = WEXITSTATUS(status);

			if( code < 255 ) {

				GuardThread(FALSE);

				return code;
			}
		}
	}

	GuardThread(FALSE);

	return -1;
}

// End of File
