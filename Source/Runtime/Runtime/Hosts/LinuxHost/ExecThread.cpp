
#include "Intern.hpp"

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Executive.hpp"

#include "Mutex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Close-Coupled APIs
//

HTHREAD GetCurrentThread(void)
{
	return _tls_valid ? ((IThread *) _tls_get_data()->ithread) : NULL;
}

global UINT GetThreadIndex(void)
{
	return ((CExecThread *) _tls_get_data()->cthread)->GetIndex();
}

global void CheckThreadCancellation(void)
{
	((CExecThread *) _tls_get_data()->cthread)->CheckCancellation();
}

global void SetTimer(UINT uTime)
{
	((CExecThread *) _tls_get_data()->cthread)->SetTimer(uTime);
}

global UINT GetTimer(void)
{
	return ((CExecThread *) _tls_get_data()->cthread)->GetTimer();
}

global void AddToSlept(UINT uTime)
{
	((CExecThread *) _tls_get_data()->cthread)->AddToSlept(uTime);
}

//////////////////////////////////////////////////////////////////////////
//
// Executive Thread Object
//

// Stack Pool

DWORD CExecThread::m_PoolBase = 0;

DWORD CExecThread::m_PoolSize = 0;

// Constructor

CExecThread::CExecThread(CMosExecutive *pExec) : CMosExecThread(pExec)
{
	// We allocate this as a distinct memory block, as we
	// can't risk it being released with this object. This
	// would create write to free memory if the thread exits
	// and releases itself in its own context. This is a
	// minor memory leak that we should fix at some point.

	m_pWid = new INT;
}

// Destructor

CExecThread::~CExecThread(void)
{
	Info(0, "deleted\n");
}

// IWaitable

PVOID CExecThread::GetWaitable(void)
{
	AfxAssert(FALSE);

	return NULL;
}

BOOL CExecThread::Wait(UINT uWait)
{
	// Only the parent is allowed to wait on the thread. This is a
	// restriction compare to the Win32 implementation, but we need
	// to call _waitpid to ensure the thread cleans up correctly
	// and there is no way to do that except from the parent. THIS
	// IS NOT TRUE ANYMORE SO WE CAN RELAX THIS EVENTUALLY...

	if( _gettid() == m_parent ) {

		while( m_state == 1 ) {

			DWORD t1 = (uWait == FOREVER) ? 0 : GetTickCount();

			int   c;

			if( !(c = _futex_wait(&m_state, 1, uWait)) || c == -EAGAIN || c == -EINTR ) {

				if( uWait < FOREVER ) {

					DWORD t2 = GetTickCount();

					DWORD dt = t2 - t1;

					if( dt >= uWait ) {

						return FALSE;
					}

					uWait -= dt;
				}

				continue;
			}

			return FALSE;
		}

		if( m_pWid ) {

			// The thread has now exited our substantive code, but might be
			// running clean-up code so if we return straight away and the
			// parent then exits, we risk SIGHUP events or freeing the object
			// before its done. We use the wait id value created by clone.

			_futex_base(m_pWid, 0, m_pid, 0);

			// We can now free our wait id, since it won't be used again.

			delete m_pWid;

			m_pWid = NULL;
		}

		return TRUE;
	}

	AfxAssert(FALSE);

	return FALSE;
}

BOOL CExecThread::HasRequest(void)
{
	AfxAssert(FALSE);

	return FALSE;
}

// IThread

void CExecThread::Guard(BOOL fGuard)
{
	if( _gettid() == m_pid ) {

		if( fGuard ) {

			m_guard++;
		}
		else {
			AfxAssert(m_guard);

			if( !--m_guard ) {

				m_pGuardProc = NULL;

				m_pGuardData = NULL;
			}
		}

		return;
	}

	AfxAssert(FALSE);
}

void CExecThread::Guard(PGUARD pProc, PVOID pData)
{
	if( !m_guard ) {

		AfxAssert(!m_pGuardProc);

		m_pGuardProc = pProc;

		m_pGuardData = pData;
	}

	Guard(TRUE);
}

void CExecThread::Exit(int nCode)
{
	if( _gettid() == m_pid ) {

		if( AtomicCompAndSwap(&m_state, 1, 2) == 1 ) {

			Info(1, "exiting\n");

			CExecCancel c;

			c.r = nCode;

			throw c;
		}

		// The thread is already being destroyed, so
		// just wait and allow the process to proceed.

		for( ;;) Sleep(FOREVER);
	}

	AfxAssert(FALSE);
}

BOOL CExecThread::Destroy(void)
{
	if( _gettid() == m_parent ) {

		if( m_state == 1 ) {

			CAutoGuard Guard(SafeToGuard());

			if( AtomicCompAndSwap(&m_state, 1, 2) == 1 ) {

				if( m_state >= 3 ) {

					// The thread returned or threw an exception while
					// we were waiting for any guards to be released, so
					// we don't need to kill the cancellation request.

					Info(1, "returned already\n");
				}
				else {
					// Call any user cancellation procedure.

					if( m_pGuardProc ) {

						(*m_pGuardProc)(this, m_pGuardData);

						m_pGuardProc = NULL;

						m_pGuardData = NULL;
					}

					while( m_state == 2 ) {

						if( m_cancel < 2 ) {

							// This set the cancellation and then interrupts
							// any waits so that we're able to respond.

							_tkill(m_pid, SIGUSR1);
						}

						// Make sure that the state changed to avoid
						// any issues with race conditions on signals.

						_futex_wait(&m_state, 2, 50);
					}
				}

				// If we're using thread state management, we do
				// not wait for an exit at this point as the thread
				// will just move to the next state.

				if( !m_pTms ) {

					if( m_pWid ) {

						// Wait for the Linux thread to exit.

						_futex_base(m_pWid, 0, m_pid, 0);

						// We can now free our wait id, since it won't be used again.

						delete m_pWid;

						m_pWid = NULL;
					}

					// And it's now safe to release the thread. This ought
					// to delete it as the thread's own reference will have
					// been removed during its shutdown process.

					Release();
				}

				return TRUE;
			}
		}

		Release();

		return FALSE;
	}

	AfxAssert(FALSE);

	return FALSE;
}

// Overridables

BOOL CExecThread::OnCreate(void)
{
	if( CMosExecThread::OnCreate() ) {

		m_state  = 1;

		m_parent = _gettid();

		AllocateStack();

		int c = _create_thread(ThreadProc, this, m_pStackInit, m_pWid);

		if( c > 0 ) {

			m_pid = c;

			return TRUE;
		}
	}

	m_state = 0;

	AfxTrace("exec: thread creation failed\n");

	return FALSE;
}

void CExecThread::OnChange(int state)
{
	m_state = state;

	_futex_wake(&m_state, 1);
}

// Implementation

void CExecThread::AllocateStack(void)
{
	if( !m_PoolSize ) {

		// We allocate a block of 16 standard-sized stacks using mmap,
		// and then mess about with the start and end of the block to
		// ensure it's aligned correctly. We most likely end up with one
		// fewer stacks in our pool. Note that all this code is guarded
		// by the locking mechanism in the Executive, so we don't have
		// to worry about serializing access to our static data.

		m_PoolSize = _tls_stack_size * 16;

		m_PoolBase = DWORD(_mmap_private(m_PoolSize));

		UINT uSkip = (_tls_stack_size - (m_PoolBase % _tls_stack_size) % _tls_stack_size);

		_munmap(PVOID(m_PoolBase), uSkip);

		m_PoolBase = m_PoolBase + uSkip;

		m_PoolSize = m_PoolSize - uSkip;

		UINT uTrim = m_PoolSize % _tls_stack_size;

		m_PoolSize = m_PoolSize - uTrim;

		_munmap(PVOID(m_PoolBase + m_PoolSize), uTrim);
	}

	m_uStackSize = _tls_stack_size;

	m_pStackBase = PBYTE(m_PoolBase);

	m_pStackInit = m_pStackBase + m_uStackSize;

	// Default pattern lets us look at stack usage.

	memset(m_pStackBase, 0x55, m_uStackSize);

	m_PoolBase = m_PoolBase + m_uStackSize;

	m_PoolSize = m_PoolSize - m_uStackSize;
}

void CExecThread::FreeStack(void)
{
	_munmap(PVOID(m_pStackBase), m_uStackSize);
}

int CExecThread::ExecuteThread(void)
{
	m_pid = __gettid();

	InstallTls();

	CreateImpure();

	InstallImpure();

	ApplyPriority();

	CommonExecute();

	CMutex::FreeMutexList(this);

	// Grab a copy of the exit state and stack data in case
	// the Release call frees this object out from under us.

	int   nExitCode  = m_exit;

	PBYTE pStackBase = m_pStackBase;

	UINT  uStackSize = m_uStackSize;

	// We can't use this pointer any more as when we are
	// freed, our vtable information will be corrupted.

	_tls_get_data()->ithread = NULL;

	// Only one release here as the parent does its own.

	Release();

	// Reduce the TLS validity count.

	AtomicDecrement(&_tls_valid);

	// Unmap the stack and exit in a single operation.

	_munmap_and_exit(PVOID(pStackBase), uStackSize, nExitCode);

	return 0;
}

void CExecThread::InstallTls(void)
{
	_tls_data *tls = _tls_get_data();

	tls->tid     = m_pid;
	tls->impure  = 0;
	tls->ithread = (IThread     *) this;
	tls->cthread = (CExecThread *) this;
	tls->exec    = (IExecutive  *) m_pExec;
	tls->eh[0]   = NULL;
	tls->eh[1]   = NULL;
	tls->eh[2]   = NULL;
	tls->eh[3]   = NULL;
	tls->addr1   = 0;
	tls->data1   = 0;
	tls->addr2   = 0;
	tls->data2   = 0;
	tls->magic1  = _tls_magic;
	tls->magic2  = _tls_magic;

	AtomicIncrement(&_tls_valid);
}

void CExecThread::InstallImpure(void)
{
	_tls_data *tls = _tls_get_data();

	tls->impure = m_impure;
}

void CExecThread::ApplyPriority(void)
{
	if( m_uLevel == NOTHING ) {

		// This level is used for the timer thread, to which we assign
		// the FIFO scheduling policy and run at a high priority. We
		// leave a little bit of headroom for serial port pump threads.

		int policy   = 1;

		int priority = 90;

		_sched_setscheduler(0, policy, &priority);
	}
	else {
		// For other priority levels we are going to stick with the standard
		// scheduling policy and use the nice value to tweak behavior. The
		// levels above 10000 are PXE threads, so we'll give them all -16
		// and then distribute the rest between 0 and -15.

		#if 0

		if( m_uLevel >= 10000 ) {

			_nice(-16);
		}
		else {
			int inc = int(15 * m_uLevel + 5000) / 10000;

			_nice(-inc);
		}

		#endif
	}
}

// Entry Point

int CExecThread::ThreadProc(void *pParam)
{
	CExecThread *pThread = (CExecThread *) pParam;

	return pThread->ExecuteThread();
}

// End of File
