
#include "Intern.hpp"

#include "ModuleManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <dlfcn.h>

//////////////////////////////////////////////////////////////////////////
//
// Module Manager
//

// Constructor

CModuleManager::CModuleManager(void)
{
	StdSetRef();

	AfxVerify(FindHostPath());

	AfxVerify(FindTempPath());

	piob->RegisterSingleton("host.modman", 0, this);
}

// Destructor

CModuleManager::~CModuleManager(void)
{
	// The module manager is deleted via a C++ delete call
	// so we don't want the Release from the revocation to
	// kick off another call to the destructor!

	AddRef();

	piob->RevokeSingleton("host.modman", 0);

	FreeAllModules();
}

// IUnknown

HRESULT CModuleManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IModuleManager);

	StdQueryInterface(IModuleManager);

	return E_NOINTERFACE;
}

ULONG CModuleManager::AddRef(void)
{
	StdAddRef();
}

ULONG CModuleManager::Release(void)
{
	StdRelease();
}

// Operations

bool CModuleManager::LoadClientModule(PCTXT pName)
{
	UINT hModule;

	return LoadClientModule(hModule, pName);
}

bool CModuleManager::LoadClientModule(UINT &hModule, PCTXT pName)
{
	if( pName && *pName ) {

		CModule *pModule = New CModule;

		if( pName[0] == '!' ) {

			CString Path = m_Path.Left(m_Path.FindRev('/') + 1);

			pModule->m_Name = Path + (pName + 1) + ".so";
		}
		else {
			CString  Name   = pName;

			CString  Path   = Name.Count('/') ? "" : m_Path.Left(m_Path.FindRev('/') + 1);

			CString  Type   = Name.EndsWith(".so") ? "" : ".so";

			pModule->m_Name = Path + Name + Type;
		}

		if( &dlopen ) {

			AfxTrace("modman: loading %s\n", PCTXT(pModule->m_Name));

			if( (pModule->m_hLib = dlopen(pModule->m_Name, RTLD_LAZY)) ) {

				pModule->m_pfnMain = MAINPTR(dlsym(pModule->m_hLib, "AeonCslMain"));

				pModule->m_pfnExit = EXITPTR(dlsym(pModule->m_hLib, "AeonCslExit"));

				if( pModule->m_pfnMain && pModule->m_pfnExit ) {

					(pModule->m_pfnMain)(piob);

					hModule = UINT(m_List.Append(pModule));

					AfxTrace("modman: loaded\n");

					return true;
				}

				AfxTrace("modman: cannot find entry points\n");

				AfxTrace("modman: %s\n", dlerror());

				delete pModule;

				return false;
			}

			AfxTrace("modman: cannot load client module\n");

			AfxTrace("modman: %s\n", dlerror());

			delete pModule;
		}
		else
			AfxTrace("modman: not linked to libdl.so\n");
	}

	return false;
}

bool CModuleManager::LoadClientModule(UINT &hModule, PCTXT pName, PCBYTE pData, UINT uSize)
{
	CString Name  = pName;

	CString Path  = m_Temp + Name + ".so";

	int     fd    = _open(Path, O_RDWR | O_CREAT | O_TRUNC, 0755);

	if( fd ) {

		UINT uDone = _write(fd, pData, uSize);

		_close(fd);

		_chmod(Path, 0755);

		if( uDone == uSize ) {

			return LoadClientModule(hModule, Path);
		}

		_unlink(Path);
	}

	return false;
}

bool CModuleManager::FreeClientModule(UINT hModule)
{
	if( hModule ) {

		CModule *pModule = m_List[INDEX(hModule)];

		(pModule->m_pfnExit)();

		#if !defined(AEON_PROC_Arm926)

		// This crashes on Arm926 as a result of the rather
		// tenuous support for our executable format. I am
		// not sure it's really needed anyway as when we die
		// the modules are all freed in any case.

		dlclose(pModule->m_hLib);

		#endif

		if( pModule->m_Name.StartsWith(m_Temp) ) {

			_unlink(pModule->m_Name);
		}

		delete pModule;

		m_List.Remove(INDEX(hModule));

		return true;
	}

	return false;
}

bool CModuleManager::FreeAllModules(void)
{
	INDEX i;

	while( !m_List.Failed(i = m_List.GetTail()) ) {

		FreeClientModule(UINT(i));
	}

	return true;
}

// Implementation

bool CModuleManager::FindHostPath(void)
{
	char path[MAX_PATH];

	int  pid = _getpid();

	int  len = _readlink(CPrintf("/proc/%u/exe", pid), path, sizeof(path) - 1);

	if( len > 0 && len < sizeof(path) - 1 ) {

		path[len] = 0;

		m_Path = path;

		return true;
	}

	return false;
}

bool CModuleManager::FindTempPath(void)
{
	m_Temp = "/tmp/crimson/modules/";

	_mkdir(m_Temp, 0755);

	return true;
}

// End of File
