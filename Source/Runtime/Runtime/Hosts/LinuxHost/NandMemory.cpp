
#include "Intern.hpp"

#include "NandMemory.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Windows Emulated Nand Flash Manager
//

// Instantiator

IDevice * Create_NandMemory(void)
{
	return (INandMemory *) New CNandMemory;
}

// Constructor

CNandMemory::CNandMemory(void)
{
	StdSetRef();

	m_Geom.m_uChips   = 1;
	m_Geom.m_uBlocks  = 256;
	m_Geom.m_uPages   = 64;
	m_Geom.m_uSectors = 4;
	m_Geom.m_uBytes   = 512;

	m_uSizeSector = m_Geom.m_uBytes;
	m_uSizePage   = m_Geom.m_uSectors * m_uSizeSector;
	m_uSizeBlock  = m_Geom.m_uPages   * m_uSizePage;
	m_uSizeChip   = m_Geom.m_uBlocks  * m_uSizeBlock;
	m_uSizeTotal  = m_Geom.m_uChips   * m_uSizeChip;

	m_uSize = m_uSizeTotal;

	m_Name  = "nand";
}

// Destructor

CNandMemory::~CNandMemory(void)
{
}

// IUnknown

HRESULT CNandMemory::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(INandMemory);

	return E_NOINTERFACE;
}

ULONG CNandMemory::AddRef(void)
{
	StdAddRef();
}

ULONG CNandMemory::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CNandMemory::Open(void)
{
	AllocData();

	return TRUE;
}

// INandMemory

BOOL METHOD CNandMemory::GetGeometry(CNandGeometry &Geom)
{
	Geom = m_Geom;

	return TRUE;
}

BOOL METHOD CNandMemory::GetUniqueId(UINT uChip, PBYTE pData)
{
	memset(pData, 0xAA, 16);

	return TRUE;
}

BOOL METHOD CNandMemory::IsBlockBad(UINT uChip, UINT uBlock)
{
	return FALSE;
}

BOOL METHOD CNandMemory::MarkBlockBad(UINT uChip, UINT uBlock)
{
	return FALSE;
}

BOOL METHOD CNandMemory::EraseBlock(UINT uChip, UINT uBlock)
{
	UINT uAddr = 0;

	uAddr += uChip  * m_uSizeChip;

	uAddr += uBlock * m_uSizeBlock;

	memset(m_pData + uAddr, 0xFF, m_uSizeBlock);

	return TRUE;
}

BOOL METHOD CNandMemory::WriteSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PCBYTE pData)
{
	UINT uAddr = 0;

	uAddr += uChip  * m_uSizeChip;

	uAddr += uBlock * m_uSizeBlock;

	uAddr += uPage  * m_uSizePage;

	uAddr += uSect  * m_uSizeSector;

	memcpy(m_pData + uAddr, pData, m_uSizeSector);

	return TRUE;
}

BOOL METHOD CNandMemory::ReadSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PBYTE pData)
{
	UINT uAddr = 0;

	uAddr += uChip  * m_uSizeChip;

	uAddr += uBlock * m_uSizeBlock;

	uAddr += uPage  * m_uSizePage;

	uAddr += uSect  * m_uSizeSector;

	memcpy(pData, m_pData + uAddr, m_uSizeSector);

	return TRUE;
}

BOOL METHOD CNandMemory::WritePage(UINT uChip, UINT uBlock, UINT uPage, PCBYTE pData)
{
	UINT uAddr = 0;

	uAddr += uChip  * m_uSizeChip;

	uAddr += uBlock * m_uSizeBlock;

	uAddr += uPage  * m_uSizePage;

	memcpy(m_pData + uAddr, pData, m_uSizePage);

	return TRUE;
}

BOOL METHOD CNandMemory::ReadPage(UINT uChip, UINT uBlock, UINT uPage, PBYTE pData)
{
	UINT uAddr = 0;

	uAddr += uChip  * m_uSizeChip;

	uAddr += uBlock * m_uSizeBlock;

	uAddr += uPage  * m_uSizePage;

	memcpy(pData, m_pData + uAddr, m_uSizePage);

	return TRUE;
}

// End of File
