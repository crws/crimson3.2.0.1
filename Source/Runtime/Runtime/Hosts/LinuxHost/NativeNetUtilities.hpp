
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NativeNetUtilities_HPP

#define INCLUDE_NativeNetUtilities_HPP

//////////////////////////////////////////////////////////////////////////
//
// Network Information 
//

class CNativeNetUtilities : public INetUtilities
{
public:
	// Constructor
	CNativeNetUtilities(void);

	// Destructor
	~CNativeNetUtilities(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// INetUtilities
	BOOL  METHOD SetInterfaceNames(CStringArray const &Names);
	BOOL  METHOD SetInterfaceDescs(CStringArray const &Descs);
	BOOL  METHOD SetInterfacePaths(CStringArray const &Paths);
	BOOL  METHOD SetInterfaceIds(CUIntArray const &Ids);
	UINT  METHOD GetInterfaceCount(void);
	UINT  METHOD FindInterface(CString const &Name);
	UINT  METHOD FindInterface(UINT Id);
	BOOL  METHOD HasInterfaceType(UINT uType);
	UINT  METHOD GetInterfaceType(UINT uFace);
	UINT  METHOD GetInterfaceOrdinal(UINT uFace);
	BOOL  METHOD GetInterfaceName(UINT uFace, CString &Name);
	BOOL  METHOD GetInterfaceDesc(UINT uFace, CString &Desc);
	BOOL  METHOD GetInterfacePath(UINT uFace, CString &Path);
	BOOL  METHOD GetInterfaceMac(UINT uFace, MACADDR &Mac);
	BOOL  METHOD GetInterfaceAddr(UINT uFace, IPADDR &Addr);
	BOOL  METHOD GetInterfaceMask(UINT uFace, IPADDR &Mask);
	BOOL  METHOD GetInterfaceGate(UINT uFace, IPADDR &Gate);
	BOOL  METHOD GetInterfaceStatus(UINT uFace, CString &Status);
	BOOL  METHOD IsInterfaceUp(UINT uFace);
	DWORD METHOD GetOption(BYTE bType);
	DWORD METHOD GetOption(BYTE bType, UINT uFace);
	BOOL  METHOD IsBroadcast(IPADDR const &IP);
	BOOL  METHOD GetBroadcast(IPADDR const &IP, IPADDR &Broad);
	BOOL  METHOD EnumBroadcast(UINT &uFace, IPADDR &Broad);
	BOOL  METHOD IsFastAddress(IPADDR const &IP);
	BOOL  METHOD IsSafeAddress(IPADDR const &IP);
	BOOL  METHOD GetDefaultGateway(IPADDR const &IP, IPADDR &Gate);
	UINT  METHOD Ping(IPADDR const &IP, UINT uTimeout);

protected:
	// Data Members
	ULONG        m_uRefs;
	int	     m_fd;
	IMutex *     m_pLock;
	CStringArray m_Names;
	CStringArray m_Descs;
	CStringArray m_Paths;
	CUIntArray   m_Ids;
	bool	     m_fForce;
	WORD         m_wPingSeq;

	// Implementation
	BOOL FindFaces(void);
	UINT FindRoute(IPADDR const &IP);
	UINT TypeFromName(CString const &Name);
};

// End of File

#endif
