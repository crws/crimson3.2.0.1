
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LibC Implementation
//

// Data

static IMutex * m_pMutex = NULL;

// Code

clink void __libc_csu_init(void)
{
}

clink void __libc_csu_fini(void)
{
}

clink void abort(void)
{
	AfxAssert(FALSE);

	for( ;;);
}

clink int raise(int)
{
	AfxAssert(FALSE);

	return 0;
}

clink void __malloc_serialize(bool enable)
{
	if( enable ) {

		if( !m_pMutex ) {

			m_pMutex = Create_Qutex();
		}
	}
	else {
		if( m_pMutex ) {

			IMutex *pMutex = m_pMutex;

			m_pMutex = NULL;

			pMutex->Release();
		}
	}
}

clink void __malloc_lock(void *)
{
	if( m_pMutex ) {

		m_pMutex->Wait(FOREVER);
	}
}

clink void __malloc_unlock(void *)
{
	if( m_pMutex ) {

		m_pMutex->Free();
	}
}

clink struct passwd * getpwuid(uid_t uid)
{
	return NULL;
}

clink uid_t getuid(void)
{
	return 0;
}

clink struct passwd * getpwnam(char const *name)
{
	return NULL;
}

// End of File
