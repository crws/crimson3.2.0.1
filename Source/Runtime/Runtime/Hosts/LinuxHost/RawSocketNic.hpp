
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RawSocketNic_HPP

#define INCLUDE_RawSocketNic_HPP

//////////////////////////////////////////////////////////////////////////
//
// Raw Socket Based NIC Driver
//

class CRawSocketNic : public INic, public IClientProcess
{
public:
	// Constructor
	CRawSocketNic(UINT uInst, UINT uFace);

	// Destructor
	~CRawSocketNic(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// INic
	bool METHOD Open(bool fFast, bool fFull);
	bool METHOD Close(void);
	bool METHOD InitMac(MACADDR const &Addr);
	void METHOD ReadMac(MACADDR &Addr);
	UINT METHOD GetCapabilities(void);
	void METHOD SetFlags(UINT uFlags);
	bool METHOD SetMulticast(MACADDR const *pList, UINT uList);
	bool METHOD IsLinkActive(void);
	bool METHOD WaitLink(UINT uTime);
	bool METHOD SendData(CBuffer *  pBuff, UINT uTime);
	bool METHOD ReadData(CBuffer * &pBuff, UINT uTime);
	void METHOD GetCounters(NICDIAG &Diag);
	void METHOD ResetCounters(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

	// Interface Location
	static UINT FindInterfaces(void);

protected:
	// Interface Data
	struct CFace
	{
		UINT		 m_uIndex;
		CString	         m_Adapter;
		CMacAddr	 m_MacAddr;
		CArray <CIpAddr> m_IpAddrs;
	};

// Interface List
	static CArray <CFace *> m_Faces;

	// Data Members
	ULONG        m_uRefs;
	UINT	     m_uInst;
	UINT         m_uFace;
	CFace      * m_pFace;
	IMutex     * m_pLock;
	ISemaphore * m_pFlag;
	CMacAddr     m_Mac;
	int          m_hSocket;
	HTHREAD      m_hThread;
	char	     m_sError[256];
	UINT         m_uRxLimit;
	UINT         m_uRxCount;
	CBuffer    * m_pRxHead;
	CBuffer    * m_pRxTail;
	UINT         m_uMulti;
	CMacAddr   * m_pMulti;

	// Implementation
	bool FindFace(void);
	void FlipMac(void);
	void SetAdapterAndProtocol(UINT uIndex, WORD Protocol);
	void SetPromiscuous(UINT uIndex, PCTXT pName);
};

// End of File

#endif
