
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DatagramSocket_HPP

#define INCLUDE_DatagramSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BaseSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Datagram Socket Object
//

class CDatagramSocket : public CBaseSocket
{
public:
	// Constructor
	CDatagramSocket(void);

	// ISocket
	HRM GetPhase(UINT &Phase);
	HRM Abort(void);
	HRM Close(void);
};

// End of File

#endif
