

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NativeDnsResolver_HPP

#define INCLUDE_NativeDnsResolver_HPP

//////////////////////////////////////////////////////////////////////////
//
// DNS Resolver
//

class CNativeDnsResolver : public IDnsResolver
{
public:
	// Constructor
	CNativeDnsResolver(void);

	// Destructor
	~CNativeDnsResolver(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDnsResolver
	CIpAddr METHOD Resolve(PCTXT pName);
	BOOL    METHOD Resolve(CArray <CIpAddr> &List, PCTXT pName);

protected:
	// Data Members
	ULONG    m_uRefs;
	IMutex * m_pLock;
	BOOL     m_fUtil;
	CString  m_Temp;

	// Implementation
	bool FindTempPath(void);
};

// End of File

#endif
