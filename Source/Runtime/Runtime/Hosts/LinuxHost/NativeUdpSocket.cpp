
#include "Intern.hpp"

#include "NativeUdpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "Socket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// UDP Socket Object
//

// Instantiator

static IUnknown * Create_NativeUdpSocket(PCTXT pName)
{
	return (IUnknown *) New CNativeUdpSocket;
}

// Registration

global void Register_NativeUdpSocket(void)
{
	piob->RegisterInstantiator("net.sock-udp", Create_NativeUdpSocket);
}

global void Revoke_NativeUdpSocket(void)
{
	piob->RevokeInstantiator("net.sock-udp");
}

// Constructor

CNativeUdpSocket::CNativeUdpSocket(void)
{
	m_uAddress = 0;

	m_Loc      = 0;
}

// Destructor

CNativeUdpSocket::~CNativeUdpSocket(void)
{
	Abort();
}

// ISocket

HRESULT CNativeUdpSocket::Listen(IPADDR const &IP, WORD Loc)
{
	if( m_uState == stateInit || m_uState == stateError ) {

		if( (m_hSocket = _socket(AF_INET, SOCK_DGRAM, IPPROTO_IP)) > 0 ) {

			SetMulticast(IP);

			SetNonBlocking();

			SetPacketInfo();

			AllowDupAddress();

			sockaddr_in loc;

			LoadAddress(loc, IP, Loc);

			if( _bind(m_hSocket, (sockaddr *) &loc, sizeof(loc)) == 0 ) {

				m_Loc    = Loc;

				m_uState = stateOpen;

				return S_OK;
			}

			int n = errno;

			Close();
		}
	}

	return E_FAIL;
}

HRESULT CNativeUdpSocket::Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc)
{
	if( m_uState == stateInit || m_uState == stateError ) {

		if( (m_hSocket = _socket(AF_INET, SOCK_DGRAM, IPPROTO_IP)) > 0 ) {

			SetMulticast(IP);

			SetNonBlocking();

			SetPacketInfo();

			AllowDupAddress();

			sockaddr_in rem;

			sockaddr_in loc;

			LoadAddress(rem, IP, Rem);

			if( !LoadAddress(loc, IF, Loc) || _bind(m_hSocket, (sockaddr *) &loc, sizeof(loc)) == 0 ) {

				if( _connect(m_hSocket, (sockaddr *) &rem, sizeof(rem)) == 0 ) {

					m_Loc    = Loc;

					m_uState = stateOpen;

					return S_OK;
				}
			}

			int n = errno;

			Close();
		}
	}

	return E_FAIL;
}

HRESULT CNativeUdpSocket::Recv(PBYTE pData, UINT &uSize)
{
	AfxAssert(!m_uAddress);

	return CBaseSocket::Recv(pData, uSize);
}

HRESULT CNativeUdpSocket::Send(PBYTE pData, UINT &uSize)
{
	AfxAssert(!m_uAddress);

	return CBaseSocket::Send(pData, uSize);
}

HRESULT CNativeUdpSocket::Recv(CBuffer * &pBuff)
{
	if( m_uAddress ) {

		CAutoBuffer Buff(NOTHING);

		struct msghdr mh = { 0 };

		struct iovec  io = { 0 };

		struct sockaddr_in peer;

		BYTE cm[256] = { 0 };

		io.iov_base = Buff->GetData() + Buff->GetSize();;

		io.iov_len  = Buff->GetTailSpace();

		mh.msg_name       = &peer;
		mh.msg_namelen    = sizeof(peer);
		mh.msg_iov        = &io;
		mh.msg_iovlen     = 1;
		mh.msg_control    = cm;
		mh.msg_controllen = sizeof(cm);

		int nRead = _recvmsg(m_hSocket, &mh, 0);

		if( nRead >= 0 ) {

			Buff->AddTail(nRead);

			for( struct cmsghdr *cmsg = CMSG_FIRSTHDR(&mh); cmsg != NULL; cmsg = CMSG_NXTHDR(&mh, cmsg) ) {

				if( cmsg->cmsg_level == IPPROTO_IP && cmsg->cmsg_type == IP_PKTINFO ) {

					struct in_pktinfo *pi = (struct in_pktinfo *) CMSG_DATA(cmsg);

					if( m_uAddress >= 3 ) {

						*BuffAddHead(Buff, IPADDR) = CIpAddr(pi->ipi_spec_dst.s_addr);
					}

					if( m_uAddress >= 2 ) {

						*BuffAddHead(Buff, WORD)   = HostToNet(m_Loc);

						*BuffAddHead(Buff, IPADDR) = CIpAddr(pi->ipi_addr.s_addr);
					}

					if( m_uAddress >= 1 ) {

						*BuffAddHead(Buff, WORD)   = peer.sin_port;

						*BuffAddHead(Buff, IPADDR) = CIpAddr(peer.sin_addr.s_addr);
					}

					pBuff = Buff.TakeOver();

					return S_OK;
				}
			}
		}

		pBuff = NULL;

		return E_FAIL;
	}

	return CBaseSocket::Recv(pBuff);
}

HRESULT CNativeUdpSocket::Send(CBuffer *pBuff)
{
	if( m_uAddress ) {

		IPADDR *pSrc  = BuffStripHead(pBuff, IPADDR);

		WORD   *pPort = BuffStripHead(pBuff, WORD);

		sockaddr_in rem;

		LoadAddress(rem, *pSrc, NetToHost(*pPort));

		if( !_connect(m_hSocket, (sockaddr *) &rem, sizeof(rem)) == 0 ) {

			return E_FAIL;
		}
	}
			       
	return CBaseSocket::Send(pBuff);
}

HRESULT CNativeUdpSocket::SetOption(UINT uOption, UINT uValue)
{
	if( uOption == OPT_ADDRESS ) {

		m_uAddress = uValue;

		return S_OK;
	}

	return CBaseSocket::SetOption(uOption, uValue);
}

// Implementation

void CNativeUdpSocket::SetPacketInfo(void)
{
	ULONG uEnable = m_uAddress ? 1 : 0;

	_setsockopt(m_hSocket, IPPROTO_IP, IP_PKTINFO, &uEnable, sizeof(uEnable));
}

bool CNativeUdpSocket::SetMulticast(IPADDR const &IP)
{
	// TODO -- Can we skip this for modems and ports that don't want multicast?

	if( IPREF(IP).IsMulticast() ) {

		int fd = _socket(AF_INET, SOCK_DGRAM, 0);

		struct ifconf conf;

		conf.ifc_len           = 0;

		conf.ifc_ifcu.ifcu_buf = NULL;

		_ioctl(fd, SIOCGIFCONF, &conf);

		if( conf.ifc_len ) {

			conf.ifc_ifcu.ifcu_buf = malloc(conf.ifc_len);

			_ioctl(fd, SIOCGIFCONF, &conf);

			UINT c = conf.ifc_len / sizeof(struct ifreq);

			for( UINT n = 0; n < c; n++ ) {

				struct ifreq *ifr = conf.ifc_ifcu.ifcu_req + n;

				if( ifr->ifr_ifru.ifru_addr.sa_family == AF_INET ) {

					CIpAddr Ip = (DWORD &) ifr->ifr_ifru.ifru_addr.sa_data[2];

					if( !Ip.IsLoopback() ) {

						_ioctl(fd, SIOCGIFINDEX, ifr);

						struct ip_mreqn mreq = { 0 };

						mreq.imr_multiaddr.s_addr = IP.m_dw;

						mreq.imr_ifindex          = ifr->ifr_ifru.ifru_ivalue;

						_setsockopt(m_hSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
					}
				}
			}

			free(conf.ifc_ifcu.ifcu_buf);
		}

		_close(fd);

		return true;
	}

	return false;
}

// End of File
