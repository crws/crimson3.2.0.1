
#include "Intern.hpp"

#include "Qutex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Quick Mutex Object
//

// Instantiator

static IUnknown * Create_Qutex(PCTXT pName)
{
	return New CQutex;
}

// Registration

global void Register_Qutex(void)
{
	piob->RegisterInstantiator("exec.qutex", Create_Qutex);

	piob->RegisterInstantiator("exec.rutex", Create_Qutex);
}

// Constructor

CQutex::CQutex(void)
{
	StdSetRef();

	m_count = 0;
}

// IUnknown

HRESULT CQutex::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(IMutex);

	return E_NOINTERFACE;
}

ULONG CQutex::AddRef(void)
{
	StdAddRef();
}

ULONG CQutex::Release(void)
{
	StdRelease();
}

// IWaitable

PVOID CQutex::GetWaitable(void)
{
	return NULL;
}

BOOL CQutex::Wait(UINT uWait)
{
	int p = _gettid();

	int c;

	if( m_state == p ) {

		m_count++;

		return TRUE;
	}

	while( (c = AtomicCompAndSwap(&m_state, 0, p)) ) {

		if( !WaitForChange(c, uWait, FALSE) ) {

			return FALSE;
		}
	}

	if( (m_guard = _tls_valid) ) {

		IThread *i = (IThread *) _tls_get_data()->ithread;

		if( likely(i) ) {

			i->Guard(TRUE);
		}
		else {
			// We're in the process of deleting ourselves
			// so we have to be careful not to guard...

			m_guard = 0;
		}
	}

	m_count = 1;

	return TRUE;
}

BOOL CQutex::HasRequest(void)
{
	AfxAssert(FALSE);

	return FALSE;
}

// IMutex

void CQutex::Free(void)
{
	if( m_state == _gettid() ) {

		if( !--m_count ) {

			if( m_guard ) {

				((IThread *) _tls_get_data()->ithread)->Guard(FALSE);
			}

			m_state = 0;

			WakeThreads(1);
		}

		return;
	}

	AfxAssert(FALSE);
}

// End of File
