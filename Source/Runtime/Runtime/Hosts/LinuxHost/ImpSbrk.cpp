
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SBrk Implementation
//

// Code

static unsigned long align(unsigned long n, unsigned long p)
{
	return p * ((n + p - 1) / p);
}

clink void * sbrk(ptrdiff_t increment)
{
	static unsigned long our_brk = 0;

	static unsigned long sys_brk = 0;

	if( !our_brk ) {

		our_brk = _brk(0);

		sys_brk = our_brk;
	}

	if( our_brk + increment <= sys_brk ) {

		unsigned long old_brk = our_brk;

		our_brk = our_brk + increment;

		return (void *) old_brk;
	}
	else {
		unsigned long old_brk = our_brk;

		unsigned long new_brk = align(our_brk + increment, 4096);

		unsigned long ret_brk = _brk(new_brk);

		if( ret_brk > sys_brk ) {

			sys_brk = ret_brk;

			our_brk = our_brk + increment;

			return (void *) old_brk;
		}

		return NULL;
	}
}

// End of File
