
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RawSocket_HPP

#define INCLUDE_RawSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DatagramSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw Socket Object
//

class CNativeRawSocket : public CDatagramSocket
{
public:
	// Constructor
	CNativeRawSocket(void);

	// Destructor
	~CNativeRawSocket(void);

	// ISocket
	HRM Listen(IPADDR const &IP, WORD Loc);
	HRM Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc);
	HRM Recv(PBYTE pData, UINT &uSize);
	HRM Send(PBYTE pData, UINT &uSize);
	HRM Recv(CBuffer * &pBuff);
	HRM Send(CBuffer   *pBuff);
	HRM GetLocal(IPADDR &IP, WORD &Port);
	HRM GetRemote(IPADDR &IP, WORD &Port);
	HRM SetOption(UINT uOption, UINT uValue);
	HRM Close(void);

protected:
	// Interface Data
	struct CFace
	{
		UINT		 m_uIndex;
		CString	         m_Adapter;
		CMacAddr	 m_MacAddr;
		CArray <CIpAddr> m_IpAddrs;
	};

	// Data Members
	CArray <CFace *>	m_Faces;
	CArray <SOCKET>		m_Socks;
	CArray <CMacAddr>	m_Addrs;
	CMap   <CMacAddr, UINT> m_Seen;
	UINT			m_uAddress;

	// Implementation
	void FindInterfaces(void);
	void SetAdapterAndProtocol(UINT uIndex, WORD Protocol);
	void SetPromiscuous(UINT uIndex, PCTXT pName);
	void SetMulticast(UINT uIndex, CIpAddr const &Ip, PCTXT pName);
	void AddSeenMac(PCBYTE pData, UINT uSize, UINT uFace);
	UINT GetSendMask(PCBYTE pData);
};

// End of File

#endif
