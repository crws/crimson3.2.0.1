
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinuxSupport_HPP

#define INCLUDE_LinuxSupport_HPP

//////////////////////////////////////////////////////////////////////////
//
// File System Support
//

class CLinuxSupport : public ILinuxSupport
{
public:
	// Constructor
	CLinuxSupport(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ILinuxSupport
	int METHOD CallProcess(char const *pCmd, char const * const *pArgs, char const *pStdOut, char const *pStdErr);
	int METHOD CallProcess(char const *pCmd, char const *pArgs, char const *pStdOut, char const *pStdErr);
	int METHOD CallProcess(char const * const *pArgs, char const *pStdOut, char const *pStdErr);
	int METHOD InitProcess(char const *pCmd, char const * const *pArgs);
	int METHOD InitProcess(char const *pCmd, char const *pArgs);
	int METHOD InitProcess(char const * const *pArgs);
	int METHOD TermProcess(int pid);
	int METHOD KillProcess(int pid);

protected:
	// Data Members
	ULONG m_uRefs;
};

// End of File

#endif
