
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Socket_HPP

#define INCLUDE_Socket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Linux Definitions
//

typedef void * __caddr_t;

typedef INT64  __off64_t;

#define _BSD_SOURCE

#define __aligned_u64 __u64 __attribute__((aligned(8)))

#define __wur __attribute__((__warn_unused_result__))

#define _SIGSET_H_types

//////////////////////////////////////////////////////////////////////////
//
// Socket Headers
//

#include <linux/net.h>

#include <linux/if_packet.h>

#include <linux/in.h>

#include <linux/tcp.h>

#include <net/if.h>

#include <net/if_arp.h>

#include <net/ethernet.h>

#include <sys/socket.h>

#include <sys/ioctl.h>

// End of File

#endif
