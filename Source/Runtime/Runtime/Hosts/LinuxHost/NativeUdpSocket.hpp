
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NativeUdpSocket_HPP

#define INCLUDE_NativeUdpSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DatagramSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// UDP Socket Object
//

class CNativeUdpSocket : public CDatagramSocket
{
public:
	// Constructor
	CNativeUdpSocket(void);

	// Destructor
	~CNativeUdpSocket(void);

	// ISocket
	HRM Listen(IPADDR const &IP, WORD Loc);
	HRM Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc);
	HRM Recv(PBYTE pData, UINT &uSize);
	HRM Send(PBYTE pData, UINT &uSize);
	HRM Recv(CBuffer * &pBuff);
	HRM Send(CBuffer   *pBuff);
	HRM SetOption(UINT uOption, UINT uValue);

protected:
	// Data Members
	UINT m_uAddress;
	WORD m_Loc;

	// Implementation
	void SetPacketInfo(void);
	bool SetMulticast(IPADDR const &IP);
};

// End of File

#endif
