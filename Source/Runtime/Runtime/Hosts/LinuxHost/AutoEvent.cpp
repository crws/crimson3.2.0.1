
#include "Intern.hpp"

#include "AutoEvent.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Auto Event Object
//

// Instantiator

static IUnknown * Create_AutoEvent(PCTXT pName)
{
	return New CAutoEvent;
}

// Registration

global void Register_AutoEvent(void)
{
	piob->RegisterInstantiator("exec.event-a", Create_AutoEvent);
}

// Constructor

CAutoEvent::CAutoEvent(void)
{
	StdSetRef();
}

// IUnknown

HRESULT CAutoEvent::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(IEvent);

	return E_NOINTERFACE;
}

ULONG CAutoEvent::AddRef(void)
{
	StdAddRef();
}

ULONG CAutoEvent::Release(void)
{
	StdRelease();
}

// IWaitable

PVOID CAutoEvent::GetWaitable(void)
{
	return NULL;
}

BOOL CAutoEvent::Wait(UINT uWait)
{
	int c;

	while( !AtomicCompAndSwap(&m_state, 1, 0) ) {

		if( !WaitForChange(0, uWait, TRUE) ) {

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CAutoEvent::HasRequest(void)
{
	return m_swait > 0;
}

// IEvent

void CAutoEvent::Set(void)
{
	if( !AtomicCompAndSwap(&m_state, 0, 1) ) {

		WakeThreads(1);
	}
}

void CAutoEvent::Clear(void)
{
	m_state = 0;
}

// End of File
