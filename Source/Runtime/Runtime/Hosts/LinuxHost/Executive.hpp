
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Executive_HPP

#define INCLUDE_Executive_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/MosExecutive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Object
//

class CExecutive : public CMosExecutive
{
public:
	// Constructor
	CExecutive(void);

	// IExecutive
	IThread * METHOD GetCurrentThread(void);
	UINT      METHOD GetCurrentIndex(void);
	void      METHOD Sleep(UINT uTime);
	BOOL	  METHOD ForceSleep(UINT uTime);
	UINT      METHOD GetTickCount(void);

protected:
	// Overridables
	void AllocThreadObject(CMosExecThread * &pThread);

	// Signal Handler
	static void SigHandler(int n);
};

// End of File

#endif
