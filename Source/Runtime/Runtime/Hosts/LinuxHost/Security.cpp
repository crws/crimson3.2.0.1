
#include "Intern.hpp"

#include "Security.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Library Headers
//

#include <cryptoauthlib.h>

#include <atcacert/atcacert_client.h>

#include <atcacert/atcacert_host_hw.h>

//////////////////////////////////////////////////////////////////////////
//
// Certificate Templates
//

extern "C" const atcacert_def_t SIGNER_CERTIFICATE_DEFINITION;

extern "C" const atcacert_def_t DEVICE_CERTIFICATE_DEFINITION;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define MAGIC_DEV_INFO	0x23094820

#define MAGIC_DEV_KEY	0x98325408

//////////////////////////////////////////////////////////////////////////
//
// Hardware Security Module
//

// Instantiator

IDevice * Create_Security(void)
{
	return New CSecurity;
}

// Constructor

CSecurity::CSecurity(void)
{
	m_uGroup  = SW_GROUP_1;

	m_fImport = false;

	StdSetRef();
}

// Destructor

CSecurity::~CSecurity(void)
{
}

// IUnknown

HRESULT CSecurity::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IFeatures);

	return E_NOINTERFACE;
}

ULONG CSecurity::AddRef(void)
{
	StdAddRef();
}

ULONG CSecurity::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CSecurity::Open(void)
{
	if( FALSE ) {

		m_uGroup = SW_GROUP_4;

		ImportDeviceInfo();

		return TRUE;
	}

	if( OpenLibrary() ) {

		if( FindIssuerKey() && ReadSignerCert() && ReadDeviceCert() ) {

			if( Challenge() ) {

				AfxTrace("validated security module\n");

				ReadDeviceInfo();

				if( ReadDeviceKey() ) {

					if( m_fImport ) {

						// This will happen only once during device characterization
						// just before shipping. That's the first time we'll see an
						// unlock file. At that point, we can commit the imported device
						// info, and clear all the databases to force the default admin
						// user to be recreated with a new password.

						SaveDeviceInfo();

						unlink("/vap/opt/crimson/appbase/valid");

						unlink("/vap/opt/crimson/sysbase/valid");
					}
				}

				return TRUE;
			}
		}
	}

	return FALSE;
}

// IFeatures

UINT CSecurity::GetEnabledGroup(void)
{
	return m_uGroup;
}

BOOL CSecurity::GetDeviceInfo(CSecureDeviceInfo &Info)
{
	Info = m_Info;

	return TRUE;
}

UINT CSecurity::GetDeviceCode(PBYTE pData, UINT uData)
{
	UINT uRead = min(uData, sizeof(m_DeviceNum));

	memcpy(pData, m_DeviceNum, uRead);

	return uRead;
}

BOOL CSecurity::InstallUnlock(PCBYTE pData, UINT uData)
{
	CString Text(PCSTR(pData), uData);

	CStringArray List;

	Text.Tokenize(List, '\n');

	if( VerifyKeyFile(List) ) {

		DWORD dwMagic = MAGIC_DEV_KEY;

		DWORD dwSize  = uData;

		if( !atcab_write_bytes_zone(ATCA_ZONE_DATA, 8, 0, PBYTE(&dwMagic), sizeof(dwMagic)) ) {

			if( !atcab_write_bytes_zone(ATCA_ZONE_DATA, 8, 4, PBYTE(&dwSize), sizeof(dwSize)) ) {

				UINT uWork = 4 * ((uData + 3) / 4);

				if( !atcab_write_bytes_zone(ATCA_ZONE_DATA, 8, 8, pData, uWork) ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

// HSM Support

bool CSecurity::OpenLibrary(void)
{
	static ATCAIfaceCfg dc;

	dc.iface_type            = ATCA_I2C_IFACE;
	dc.devtype               = ATECC608A;
	dc.atcai2c.baud          = 1000000;
	dc.atcai2c.bus           = 0;
	dc.atcai2c.slave_address = 0xC0;
	dc.wake_delay	         = 800;
	dc.rx_retries            = 3;
	dc.cfg_data              = NULL;

	return !atcab_init(&dc);
}

bool CSecurity::FindIssuerKey(void)
{
	MakeKey(m_IssuerKey,
		"61afe5953e6cc23bcc959f436a5d3e3e3a5e3952f7e8159de67dbb6cc704bd68",
		"f983604f24b0ab67019d80518d82ed26a583c0835bd73a31eebe2bde25344e34"
	);

	return true;
}

bool CSecurity::ReadSignerCert(void)
{
	atcacert_def_t const *pDef = &SIGNER_CERTIFICATE_DEFINITION;

	m_SignerSize = sizeof(m_SignerCert);

	if( !atcacert_read_cert(pDef, m_IssuerKey, m_SignerCert, &m_SignerSize) ) {

		if( Verify(pDef, m_SignerCert, m_SignerSize, m_IssuerKey) ) {

			if( !atcacert_get_subj_public_key(pDef, m_SignerCert, m_SignerSize, m_SignerKey) ) {

				return true;
			}
		}
	}

	return false;
}

bool CSecurity::ReadDeviceCert(void)
{
	atcacert_def_t const *pDef = &DEVICE_CERTIFICATE_DEFINITION;

	m_DeviceSize = sizeof(m_DeviceCert);

	if( !atcacert_read_cert(pDef, m_SignerKey, m_DeviceCert, &m_DeviceSize) ) {

		if( Verify(pDef, m_DeviceCert, m_DeviceSize, m_SignerKey) ) {

			if( !atcacert_get_subj_public_key(pDef, m_DeviceCert, m_DeviceSize, m_DeviceKey) ) {

				size_t size = sizeof(m_DeviceNum);

				if( !atcacert_get_cert_sn(pDef, m_DeviceCert, m_DeviceSize, m_DeviceNum, &size) ) {

					return true;
				}
			}
		}
	}

	return false;
}

bool CSecurity::Challenge(void)
{
	AfxGetAutoObject(pEntropy, "entropy", 0, IEntropy);

	BYTE msg[32];

	BYTE sig[64];

	pEntropy->GetEntropy(msg, sizeof(msg));

	if( !atcab_sign(DEVICE_CERTIFICATE_DEFINITION.private_key_slot, msg, sig) ) {

		if( Verify(m_DeviceKey, msg, sig) ) {

			return true;
		}
	}

	return false;
}

bool CSecurity::Verify(atcacert_def_t const *pDef, PCBYTE pCert, UINT uCert, PCBYTE key)
{
	BYTE msg[32] = { 0 };

	BYTE sig[64] = { 0 };

	atcacert_get_tbs_digest(pDef, pCert, uCert, msg);

	atcacert_get_signature(pDef, pCert, uCert, sig);

	return Verify(key, msg, sig);
}

bool CSecurity::Verify(PCBYTE key, PCBYTE msg, PCBYTE sig)
{
	AfxNewAutoObject(pVerify, "crypto.verify-ec-p256", ICryptoVerify);

	pVerify->SetHash("crypto.hash-null");

	pVerify->Initialize(key, 64);

	pVerify->Update(msg, 32);

	pVerify->Finalize();

	return pVerify->Verify(sig, 64, 0);
}

bool CSecurity::MakeKey(PBYTE key, PCSTR x, PCSTR y)
{
	int  p = 0;

	char c[3];

	for( int i = 0; x[i]; i+=2 ) {

		c[0] = x[i+0];
		c[1] = x[i+1];
		c[2] = 0;

		key[p++] = BYTE(strtoul(c, NULL, 16));
	}

	for( int i = 0; y[i]; i+=2 ) {

		c[0] = y[i+0];
		c[1] = y[i+1];
		c[2] = 0;

		key[p++] = BYTE(strtoul(c, NULL, 16));
	}

	return true;
}

// Device Info

bool CSecurity::ReadDeviceInfo(void)
{
	DWORD dwMagic = 0;

	if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 13, 0, PBYTE(&dwMagic), sizeof(dwMagic)) ) {

		if( dwMagic == MAGIC_DEV_INFO ) {

			DWORD dwSize = 0;

			if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 13, 4, PBYTE(&dwSize), sizeof(dwSize)) ) {

				if( dwSize == sizeof(CSecureDeviceInfo) ) {

					if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 13, 8, PBYTE(&m_Info), sizeof(m_Info)) ) {

						CString DefPass = ReadBoot("defpass1", "");

						if( DefPass.IsEmpty() || !strncmp(PCSTR(m_Info.m_bDefPass), DefPass, 22) ) {

							AfxTrace("using secure device info\n");

							return true;
						}

						AfxTrace("password info changed\n");
					}
				}
			}
		}
	}

	if( ImportDeviceInfo() ) {

		AfxTrace("using imported device info\n");

		m_fImport = true;

		return true;
	}

	AfxTrace("failed to import device info\n");

	return false;
}

bool CSecurity::SaveDeviceInfo(void)
{
	DWORD dwMagic = MAGIC_DEV_INFO;

	DWORD dwSize  = sizeof(CSecureDeviceInfo);

	if( !atcab_write_bytes_zone(ATCA_ZONE_DATA, 13, 0, PBYTE(&dwMagic), sizeof(dwMagic)) ) {

		if( !atcab_write_bytes_zone(ATCA_ZONE_DATA, 13, 4, PBYTE(&dwSize), sizeof(dwSize)) ) {

			if( !atcab_write_bytes_zone(ATCA_ZONE_DATA, 13, 8, PBYTE(&m_Info), sizeof(m_Info)) ) {

				AfxTrace("written secure device info\n");

				return true;
			}
		}
	}

	AfxTrace("failed to write device info\n");

	return false;
}

bool CSecurity::ImportDeviceInfo(void)
{
	memset(&m_Info, 0, sizeof(m_Info));

	CString Model   = ReadSkvs("FC_MODELNO", "");

	CString Serial  = ReadSkvs("FC_SERIALNO", "");

	CString Mac0    = ReadBoot("ethaddr", "");

	CString Mac1    = ReadBoot("eth1addr", "");

	CString DefPass = ReadBoot("defpass1", "quotable-chevy-9898");

	Model = Model.Left(4) + Model.Mid(6, 2);

	memcpy(m_Info.m_bModel, Model.data(), Model.size());

	memcpy(m_Info.m_bSerial, Serial.data(), Serial.size());

	memcpy(m_Info.m_bDefPass, DefPass.data(), DefPass.size());

	memcpy(m_Info.m_bModel, Model.data(), Model.size());

	memcpy(m_Info.m_bMac0, CMacAddr(Mac0).m_Addr, 6);

	memcpy(m_Info.m_bMac1, CMacAddr(Mac1).m_Addr, 6);

	m_Info.m_Revision = 0;

	m_Info.m_Options  = 0;

	return true;
}

bool CSecurity::VerifyKeyFile(CStringArray const &List)
{
	if( List.GetCount() == 5 ) {

		CString Test;

		for( UINT n = 0; n < elements(m_DeviceNum); n++ ) {

			Test.AppendPrintf("%2.2X", m_DeviceNum[n]);
		}

		if( List[0] == "K00" && List[1] == Test ) {

			CString Body;

			Body += List[0]; Body += '\n';
			Body += List[1]; Body += '\n';
			Body += List[2]; Body += '\n';
			Body += List[3]; Body += '\n';

			AfxNewAutoObject(pVerify, "crypto.verify-ec-p256", ICryptoVerify);

			if( pVerify ) {

				BYTE key[64];

				MakeKey(key,
					"116112fcb18cdcbbd1096a5913b5c148991ec471e95896b49756ad96cdc522dd",
					"1a77fb7bbc8834e09706744f0660f2753d6e009ee03fe3840883c23736d51e5d"
				);

				pVerify->SetHash("crypto.hash-sha256");

				pVerify->Initialize(key, sizeof(key));

				pVerify->Update(PCBYTE(Body.data()), Body.size());

				pVerify->Finalize();

				CByteArray sig;

				CBase64::ToBytes(sig, List[4]);

				if( pVerify->Verify(sig.data(), sig.size(), 1) ) {

					return true;
				}
			}
		}
	}

	return false;
}

bool CSecurity::ReadDeviceKey(void)
{
	DWORD dwMagic = 0;

	if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 8, 0, PBYTE(&dwMagic), sizeof(dwMagic)) ) {

		if( dwMagic == MAGIC_DEV_KEY ) {

			DWORD dwSize = 0;

			if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 8, 4, PBYTE(&dwSize), sizeof(dwSize)) ) {

				UINT uWork = 4 * ((dwSize / 4) + 3);

				if( uWork >= 100 && uWork <= 300 ) {

					CAutoArray<BYTE> Data(uWork);

					if( !atcab_read_bytes_zone(ATCA_ZONE_DATA, 8, 8, Data, uWork) ) {

						CString      Text(PCSTR(PCBYTE(Data)), dwSize);

						CStringArray List;

						Text.Tokenize(List, '\n');

						if( VerifyKeyFile(List) ) {

							DWORD c1 = strtoul(List[2], NULL, 16);

							DWORD c2 = strtoul(List[3], NULL, 16);

							m_uGroup = LOWORD(c2);

							return true;
						}
					}
				}
			}
		}
	}

	return false;
}

// Implementation

CString CSecurity::ReadFile(PCTXT pName, PCTXT pDef)
{
	int fd = _open(pName, O_RDONLY, 0);

	if( fd >= 0 ) {

		char s[128];

		int r = _read(fd, s, sizeof(s));

		if( r < sizeof(s) ) {

			s[r] = 0;

			return s;
		}

		_close(fd);
	}

	return pDef;
}

CString CSecurity::ReadSkvs(PCTXT pKey, PCTXT pDef)
{
	return ReadFile(CPrintf("/tmp/skvs.d/%s", pKey), pDef);
}

CString CSecurity::ReadBoot(PCTXT pKey, PCTXT pDef)
{
	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		CPrintf Temp("/./tmp/%8.8X%8.8X", rand(), rand());

		if( pLinux->CallProcess("/sbin/fw_printenv", pKey, Temp, Temp) == 0 ) {

			CString Line = ReadFile(Temp, "");

			UINT    uPos = Line.Find('=');

			unlink(Temp);

			if( uPos < NOTHING ) {

				Line.TrimRight();

				return Line.Mid(uPos+1);
			}
		}
	}

	return pDef;
}

// End of File
