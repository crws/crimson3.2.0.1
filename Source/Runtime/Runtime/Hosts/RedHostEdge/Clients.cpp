
#include "../../HSL/AeonHsl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Client Macros
//

#define ClientData(x)	clink BYTE _binary_##x##_cbf_start[];		\
			clink BYTE _binary_##x##_cbf_end  []

#define ClientInfo(x)	{ #x,						\
			_binary_##x##_cbf_start,			\
			_binary_##x##_cbf_end - _binary_##x##_cbf_start	\
			}

//////////////////////////////////////////////////////////////////////////
//
// Client Info Structure
//

struct CClientInfo
{
	PCTXT  pName;
	PCBYTE pData;
	INT    uData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Client Data
//

ClientData(CrimsonPxe);

ClientData(CrimsonApp);

//////////////////////////////////////////////////////////////////////////
//
// Client Directory
//

static CClientInfo const Clients[] = {

	ClientInfo(CrimsonPxe),

	ClientInfo(CrimsonApp)
	};

//////////////////////////////////////////////////////////////////////////
//
// Client API
//

global bool GetCommandLine(CStringArray &List)
{
	List.Append("CrimsonPxe");

	List.Append("CrimsonApp");

	return true;
	}

global bool FindClientImage(PCTXT pName, PCBYTE &pData, UINT &uData)
{
	for( UINT n = 0; n < elements(Clients); n++ ) {

		if( !strcasecmp(Clients[n].pName, pName) ) {

			pData = Clients[n].pData;

			uData = Clients[n].uData;

			return true;
			}
		}

	return false;
	}

// End of File
