
#include "../../HSL/AeonHsl.hpp"

#include "../../RLOS/RedMX51/Models.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Model Detection
//

// Externals

clink BYTE _base[];

// Top of Memory

global unsigned long _top = 0x30000000;

// Code

UINT AeonFindModel(void)
{
	WORD id = *PWORD(_base + 0x12);

	switch( id ) {

		case 1: return MODEL_EDGE;
		case 2: return MODEL_CORE;
		}

	return MODEL_EDGE;
	}

// End of File
