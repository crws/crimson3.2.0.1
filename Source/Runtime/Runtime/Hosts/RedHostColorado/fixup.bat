
REM *** Macro definitions:
REM
REM	%1 = $(ToolDir)
REM	%2 = $(TargetDir)
REM	%3 = $(TargetName)
REM	%4 = $(TargetPath)

REM *** Generate RedHostColorado.cbf for testing

"%~1fixup.exe" -m 2 -b 0x20000800 -c -o "%~2%~3.cbf" %4

REM *** Generate model-specific files for deployment

"%~1fixup.exe" -m 1 -b 0x20000800 -c -o "%~2co04.bin"   %4
"%~1fixup.exe" -m 2 -b 0x20000800 -c -o "%~2co07.bin"   %4
"%~1fixup.exe" -m 3 -b 0x20000800 -c -o "%~2co07eq.bin" %4
"%~1fixup.exe" -m 4 -b 0x20000800 -c -o "%~2co10.bin"   %4
"%~1fixup.exe" -m 5 -b 0x20000800 -c -o "%~2co10ev.bin" %4

REM *** Done
