
#include "../../HSL/AeonHsl.hpp"

#include "../../RLOS/RedSitara/Models.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Model Detection
//

// Externals

clink BYTE _base[];

// Top of Memory

global unsigned long _top = 0x40000000;

// Code

UINT AeonFindModel(void)
{
	WORD id = *PWORD(_base + 0x12);

	switch( id ) {

		case 1: return MODEL_MANTICORE_10;
		case 2: return MODEL_MANTICORE_30;
		case 3: return MODEL_MANTICORE_50;
		case 4: return MODEL_MANTICORE_70;
		}

	return MODEL_MANTICORE_10;
	}

// End of File
