
#include "../../HSL/AeonHsl.hpp"

#include "../../RLOS/RedSitara/Models.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Model Detection
//

// Externals

clink BYTE _base[];

// Top of Memory

global unsigned long _top = 0x40000000;

// Code

UINT AeonFindModel(void)
{
	WORD id = *PWORD(_base + 0x12);

	switch( id ) {

		case 1: return MODEL_CANYON_04;
		case 2: return MODEL_CANYON_07;
		case 3: return MODEL_CANYON_07EQ;
		case 4: return MODEL_CANYON_10;
		case 5: return MODEL_CANYON_10EV;
		case 6: return MODEL_CANYON_15;
		}

	return MODEL_CANYON_07;
	}

// End of File
