
REM *** Macro definitions:
REM
REM	%1 = $(ToolDir)
REM	%2 = $(TargetDir)
REM	%3 = $(TargetName)
REM	%4 = $(TargetPath)

REM *** Generate RedHostGraphite.cbf for testing

"%~1fixup.exe" -m 2 -b 0x20000C00 -c -o "%~2%~3.cbf" %4

REM *** Generate model-specific files for deployment

"%~1fixup.exe" -m 1 -b 0x20000C00 -g -c -o "%~2g07.bin"  %4
"%~1fixup.exe" -m 2 -b 0x20000C00 -g -c -o "%~2g09.bin"  %4
"%~1fixup.exe" -m 3 -b 0x20000C00 -g -c -o "%~2g10.bin"  %4
"%~1fixup.exe" -m 4 -b 0x20000C00 -g -c -o "%~2g10r.bin" %4
"%~1fixup.exe" -m 5 -b 0x20000C00 -g -c -o "%~2g12.bin"  %4
"%~1fixup.exe" -m 6 -b 0x20000C00 -g -c -o "%~2g15.bin"  %4

REM *** Done
