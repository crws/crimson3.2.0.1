
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IMessaging_HPP

#define INCLUDE_IMessaging_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 25 -- Messaging
//

interface IMsgTransport;
interface IMsgBroker;
interface IMsgSendNotify;
interface IMsgRecvNotify;

//////////////////////////////////////////////////////////////////////////
//
// Messaging Payload
//

struct CMsgPayload
{
	CString	   m_OrigName;
	CString	   m_OrigAddr;
	CString	   m_RcptName;
	CString    m_RcptAddr;
	CString    m_Subject;
	CString    m_Message;
	CFilename  m_AttachFile;
	CByteArray m_AttachData;
	UINT	   m_SendNotify;
	UINT	   m_Attempts;
	UINT	   m_Param[4];
};

//////////////////////////////////////////////////////////////////////////
//
// Messaging Transport
//

interface IMsgTransport : public IUnknown
{
	// https://

	AfxDeclareIID(24, 1);

	virtual BOOL METHOD CanAcceptAddress(CString const &Addr)                     = 0;
	virtual UINT METHOD RegisterSendNotify(UINT uNotify, IMsgSendNotify *pNotify) = 0;
	virtual BOOL METHOD RevokeSendNotify(UINT uNotify)                            = 0;
	virtual BOOL METHOD QueueMessage(CMsgPayload *pMessage)                       = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Messaging Broker
//

interface IMsgBroker : public IUnknown
{
	// https://

	AfxDeclareIID(24, 2);

	virtual UINT METHOD RegisterRecvNotify(IMsgRecvNotify *pNotify)       = 0;
	virtual BOOL METHOD RevokeRecvNotify(UINT uNotify)                    = 0;
	virtual BOOL METHOD SubmitMessage(PCTXT pName, CMsgPayload *pMessage) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Messaging Send Notification
//

interface IMsgSendNotify : public IUnknown
{
	// https://

	AfxDeclareIID(24, 3);

	virtual void METHOD OnMsgSent(CMsgPayload const *pMessage, BOOL fOkay) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Messaging Recieve Notification
//

interface IMsgRecvNotify : public IUnknown
{
	// https://

	AfxDeclareIID(24, 4);

	virtual BOOL METHOD OnMsgRecv(PCTXT pName, CMsgPayload const *pMessage) = 0;
};

// End of File

#endif
