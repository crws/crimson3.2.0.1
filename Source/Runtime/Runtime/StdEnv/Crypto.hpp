
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_Crypto_HPP

#define INCLUDE_Crypto_HPP

//////////////////////////////////////////////////////////////////////////
//
// Simple Crypto Support
//

extern DWORD crc(PCBYTE pData, UINT uData);

extern DWORD crc(DWORD in, PCBYTE pData, UINT uData);

extern void  md5(PCBYTE pData, UINT uData, PBYTE pHash);

extern void  rc4(PBYTE pCode, PCBYTE pData, UINT uData, PCBYTE pKey, UINT uKey);

extern void  rc4(PBYTE pData, UINT uData, PCBYTE pKey, UINT uKey);

// End of File

#endif
