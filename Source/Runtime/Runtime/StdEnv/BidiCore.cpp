
#include "Intern.hpp"

#include "Bidi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define IsOdd(n)	((n) & 1)

#define IsEven(n)	(!IsOdd(n))

//////////////////////////////////////////////////////////////////////////
//
// Unicode Order Conversion
//

CUnicode UniVisual(CUnicode s)
{
	CBidi Bidi(s);

	CUnicode v;
	
	v = Bidi.GetDisplayOrder();

	return v;
	}

void UniVisual(CUnicode s, CUIntArray &Map)
{
	CBidi Bidi(s);

	Bidi.GetDisplayOrder(Map);
	}

void UniVisual(CUnicode s, CUIntArray &MemToVis, CUIntArray &VisToMem, CUIntArray &MemLevel)
{
	CBidi Bidi(s);

	Bidi.GetDisplayOrder(MemToVis, MemLevel);

	VisToMem.SetCount(MemToVis.GetCount());

	for( UINT n = 0; n < MemToVis.GetCount(); n ++ ) {
		
		VisToMem.SetAt(MemToVis[n], n);
		}
	}

BOOL UniIsComplex(CUnicode s)
{
	CBidi Bidi(s);

	return Bidi.IsComplex();
	}

BOOL UniIsComplex(TCHAR c)
{
	CBidi Bidi(CUnicode(c, 1));

	return Bidi.IsComplex();
	}

//////////////////////////////////////////////////////////////////////////
//
// Unicode Bidirectional Algorithm
//

// Constructor

CBidi::CBidi(CUnicode Text)
{
	m_Text     = Text;

	m_fComplex = FALSE;
	
	FindBaseType(Text);

	FindBaseLevel();

	AnalyzeText(Text);	
	}

// Destructor

CBidi::~CBidi(void)
{
	}

// Attributes

BOOL CBidi::IsComplex(void)
{
	return m_fComplex;
	}

// Operations

CUnicode CBidi::GetDisplayOrder(void)
{
	if( m_fComplex ) {

		ResolveAll();

		CUnicode Text;

		for( UINT n = 0; n < m_Chars.GetCount(); n++ ) {

			CCharInfo const &Char = m_Chars[n];

			Text +=	m_Chars[Char.m_uPos].m_cChar;
			}	
		
		return Text;
		}
	
	return m_Text;
	}

void CBidi::GetDisplayOrder(CUIntArray &Order)
{
	if( m_fComplex ) {

		ResolveAll();

		for( UINT n = 0; n < m_Chars.GetCount(); n++ ) {

			CCharInfo const &Char = m_Chars[n];

			Order.Append(Char.m_uPos);
			}	
		}
	else {
		for( UINT n = 0; n < m_Chars.GetCount(); n++ ) {		

			Order.Append(n);
			}
		}
	}

void CBidi::GetDisplayOrder(CUIntArray &Order, CUIntArray &Level)
{
	if( m_fComplex ) {

		ResolveAll();

		for( UINT n = 0; n < m_Chars.GetCount(); n++ ) {

			CCharInfo const &Char = m_Chars[n];

			Order.Append(Char.m_uPos);
			
			Level.Append(Char.m_uLevel);
			}	
		}
	else {
		for( UINT n = 0; n < m_Chars.GetCount(); n++ ) {		

			Order.Append(n);

			Level.Append(0);
			}
		}
	}

// Implementation

void CBidi::FindBaseType(CUnicode Text)
{
	for( UINT n = 0; n < Text.GetLength(); n++ ) {

		WCHAR    cChar = Text[n];

		CCharType Type = GetType(cChar);
		
		switch( Type ) {
			
			case typeL:
			case typeR:
			case typeAL:

				m_BaseType = Type;
				
				return;

			default:
				break;
			}
		}

	m_BaseType = typeL;
	}

void CBidi::FindBaseLevel(void)
{
	UINT uLevel;

	switch( m_BaseType ) {
		
		case typeR:
		case typeAL:
			uLevel = 1;
			break;

		default:
			uLevel = 0;
			break;
		}

	m_uBaseLevel = uLevel;
	}

void CBidi::AnalyzeText(CUnicode Text)
{
	m_SOR = m_BaseType;

	for( UINT n = 0; n < Text.GetLength(); n++ ) {

		WCHAR   cChar = Text[n];

		CCharInfo Char;

		Char.m_cChar  = cChar;

		Char.m_Type   = GetType(cChar);

		Char.m_uLevel = m_uBaseLevel;

		Char.m_uPos   = n;

		switch( Char.m_Type ) {
			
			case typeR:
			case typeAL:
				m_fComplex = TRUE;
				break;

			default:
				break;
			}

		m_Chars.Append(Char);
		}

	m_EOR = m_BaseType;
	}

void CBidi::ResolveAll(void)
{
	ResolveWeakTypes();

	ResolveNeutralTypes();

	ResolveImplicitLevels();

	ReorderResolvedLevels();
	}

void CBidi::ResolveWeakTypes(void)
{
	for( UINT uRule = 1; uRule <= 7; uRule++ ) {
		
		ResolveWeakTypes(uRule);
		}
	}

void CBidi::ResolveWeakTypes(UINT uRule)
{
	UINT c = m_Chars.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CCharInfo &Char = (CCharInfo &) m_Chars[n];

		if( uRule == 1 ) {

			if( Char.m_Type == typeNSM ) {

				CCharType Prev = n ? m_Chars[n-1].m_Type : m_SOR;

				Char.m_Type    = Prev;

				Rule('W', uRule, n);
				}
			
			continue;
			}

		if( uRule == 2 ) {

			if( Char.m_Type == typeEN ) {

				CCharType Type = typeNA;

				if( n ) {

					UINT p = n;

					while( --p ) {

						Type = m_Chars[p].m_Type;
						
						if( IsStrongType(Type) || Type == m_SOR ) {
							
							break;
							}
						}
					}
				else {
					Type = m_SOR;						
					}

				if( Type == typeAL ) {
					
					Char.m_Type = typeAL;
					
					Rule('W', uRule, n);
					}
				}
			
			continue;
			}

		if( uRule == 3 ) {

			if( Char.m_Type == typeAL ) {

				Char.m_Type = typeR;
					
				Rule('W', uRule, n);
				}
			
			continue;
			}

		if( uRule == 4 ) {

			CCharType Prev = n > 0 ? m_Chars[n-1].m_Type : m_SOR;

			CCharType Next = n < c ? m_Chars[n+1].m_Type : m_EOR;

			if( Prev == Next ) {

				if( Char.m_Type == typeES ) {

					switch( Prev ) {
						
						case typeEN:

							Char.m_Type = typeEN;
								
							Rule('W', uRule, n);
							
							break;

						default:
							break;
						}						
					}

				if( Char.m_Type == typeCS ) {

					switch( Prev ) {

						case typeEN:
						case typeAN:

							Char.m_Type = Prev;

							Rule('W', uRule, n);								
							
							break;

						default:
							break;
						}						
					}
				}
			
			continue;
			}

		if( uRule == 5 ) {

			if( Char.m_Type == typeET ) {

				UINT p = n;
				
				while( ++n < c ) {
					
					if( m_Chars[n].m_Type != typeET ) {
						
						break;
						}
					}

				CCharType Prev = p > 0 ? m_Chars[p-1].m_Type : m_SOR;

				CCharType Next = n < c ? m_Chars[n+0].m_Type : m_EOR;
				
				if( Prev == typeEN || Next == typeEN ) {

					for( ; p < n; p++ ) {

						CCharInfo &Char = (CCharInfo &) m_Chars[p];

						Char.m_Type = typeEN;

						Rule('W', uRule, p);
						}
					}
				}

			continue;
			}

		if( uRule == 6 ) {

			switch( Char.m_Type ) {

				case typeET:
				case typeES:
				case typeCS:

					Char.m_Type = typeON;

					Rule('W', uRule, n);

					break;

				default:
					break;
				}
			
			continue;
			}

		if( uRule == 7 ) {
			
			if( Char.m_Type == typeEN ) {

				CCharType Type = typeNA;

				if( n ) {

					UINT p = n;

					while( --p ) {

						Type = m_Chars[p].m_Type;
						
						if( IsStrongType(Type) || Type == m_SOR ) {
							
							break;
							}
						}

					}
				else {
					Type = m_SOR;						
					}

				if( Type == typeL ) {
					
					Char.m_Type = typeL;
					
					Rule('W', uRule, n);
					}
				}

			continue;
			}
		}
	}

void CBidi::ResolveNeutralTypes(void)
{
	for( UINT uRule = 1; uRule <= 2 ; uRule++ ) {
		
		ResolveNeutralTypes(uRule);
		}
	}

void CBidi::ResolveNeutralTypes(UINT uRule)
{
	UINT c = m_Chars.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CCharInfo &Char = (CCharInfo &) m_Chars[n];

		if( uRule == 1 ) {

			if( IsNeutralType(Char.m_Type) ) {
				
				UINT p = n;
				
				while( ++n < c ) {
					
					if( !IsNeutralType(m_Chars[n].m_Type) ) {
						
						break;
						}
					}

				CCharType Prev = p > 0 ? FindStrongDir(m_Chars[p-1].m_Type) : m_SOR;

				CCharType Next = n < c ? FindStrongDir(m_Chars[n+0].m_Type) : m_EOR;

				if( Prev == Next ) {

					for( ; p < n; p++ ) {

						CCharInfo &Char = (CCharInfo &) m_Chars[p];

						Char.m_Type = Prev;

						Rule('N', uRule, p);
						}
					}
				}
			
			continue;
			}

		if( uRule == 2 ) {

			if( IsNeutralType(Char.m_Type) ) {

				Char.m_Type = m_BaseType;

				Rule('N', uRule, n);
				}
			
			continue;
			}
		}
	}

void CBidi::ResolveImplicitLevels(void)
{
	UINT c = m_Chars.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CCharInfo &Char = (CCharInfo &) m_Chars[n];

		if( IsEven(Char.m_uLevel) ) {
			
			switch( Char.m_Type ) {
				
				case typeL:
					break;
				
				case typeR:

					Char.m_uLevel += 1;
					
					break;
				
				case typeEN:

					Char.m_uLevel += 2;
					
					break;
				
				case typeAN:

					Char.m_uLevel += 2;
					
					break;

				default:
					break;
				}

			continue;
			}

		if( IsOdd(Char.m_uLevel) ) {
			
			switch( Char.m_Type ) {
				
				case typeL:

					Char.m_uLevel += 1;
					
					break;
				
				case typeR:
					break;
				
				case typeEN:

					Char.m_uLevel += 1;
					
					break;
				
				case typeAN:
					
					Char.m_uLevel += 1;
					
					break;

				default:
					break;
				}
			
			continue;
			}
		}
	}

void CBidi::ReorderResolvedLevels(void)
{
	for( UINT uRule = 1; uRule <= 4; uRule++ ) {
		
		ReorderResolvedLevels(uRule);
		}
	}

void CBidi::ReorderResolvedLevels(UINT uRule)
{
	if( uRule == 1 ) {

		if( TRUE ) {

			UINT      c = m_Chars.GetCount();

			for( UINT n = c; n; n-- ) {

				CCharInfo &Char = (CCharInfo &) m_Chars[n-1];

				CCharType  Type = GetType(Char.m_cChar);
				
				if( Type == typeWS ) {

					Char.m_uLevel = m_uBaseLevel;
					}
				else
					break;
				}
			}

		if( TRUE ) {

			UINT      c = m_Chars.GetCount();

			for( UINT n = c; n; n-- ) {

				CCharInfo &Char = (CCharInfo &) m_Chars[n-1];

				CCharType  Type = GetType(Char.m_cChar);
				
				if( Type == typeS || Type == typeB ) {

					Char.m_uLevel = m_uBaseLevel;

					while( --n ) {

						CCharInfo &Char = (CCharInfo &) m_Chars[n-1];
						
						CCharType  Type = GetType(Char.m_cChar);

						if( Type == typeWS ) {

							Char.m_uLevel = m_uBaseLevel;
							}
						else
							break;
						}
					}
				}
			}

		return;
		}

	if( uRule == 2 ) {

		UINT uHigh = FindHighestLevel();

		UINT  uLow = FindLowestOddLevel();

		for( UINT uLevel = uHigh; uLevel >= uLow; uLevel-- ) {

			UINT uRun = 0;

			UINT uPos = 0;

			UINT c = m_Chars.GetCount();

			for( UINT n = 0; n <= c; n++ ) {

				if( n < c ) {

					CCharInfo &Char = (CCharInfo &) m_Chars[n];
					
					if( Char.m_uLevel >= uLevel ) {

						if( !uRun ) {

							uPos = n;						
							}
						
						uRun++;

						continue;
						}
					}

				if( uRun ) {

					Reverse(uPos, uRun);
					
					uRun = 0;
					}
				}
			}

		return;
		}

	if( uRule == 3 ) {

		return;
		}

	if( uRule == 4 ) {

		return;
		}
	}

UINT CBidi::FindHighestLevel(void)
{
	UINT uLevel = 0;

	UINT      c = m_Chars.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CCharInfo const &Char = (CCharInfo &) m_Chars[n];

		MakeMax(uLevel, Char.m_uLevel);
		}

	return uLevel;
	}

UINT CBidi::FindLowestOddLevel(void)
{
	UINT uLevel = 1;

	UINT      c = m_Chars.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CCharInfo const &Char = (CCharInfo &) m_Chars[n];

		if( IsOdd(Char.m_uLevel) ) {
			
			MakeMin(uLevel, Char.m_uLevel);
			}
		}
	
	return uLevel;
	}

void CBidi::Reverse(UINT uPos, UINT uLen)
{
	for( UINT n = 0; n < uLen / 2; n ++ ) {

		CCharInfo &Char1 = (CCharInfo &) m_Chars[uPos + n];

		CCharInfo &Char2 = (CCharInfo &) m_Chars[uPos - n + uLen - 1];

		UINT       uTemp = Char1.m_uPos;

		Char1.m_uPos     = Char2.m_uPos;

		Char2.m_uPos     = uTemp;
		}
	}

// Rule Reporting

void CBidi::Rule(WCHAR t, UINT r, UINT n)
{
	#if 0

	CCharInfo const &Char = (CCharInfo const &) m_Chars[n];

	m_Rule.Append(CPrintf(L"%c%d->%s, %d", t, r, EnumTypes(Char.m_Type), n));

	#endif
	}

void CBidi::ShowRules(void)
{
	#if 0

	for( UINT n = 0; n < m_Rule.GetCount(); n ++ ) {

		AfxTrace(L"%s\n", m_Rule[n]);
		}

	#endif
	}

PCUTF CBidi::EnumTypes(CCharType Type)
{
	#if 0

	switch( Type ) {

		default:	return L"!!";

		case typeL:	return L"L";
		case typeLRE:	return L"LRE";
		case typeLRO:	return L"LRO";
		case typeR:	return L"R";
		case typeAL:	return L"AL";
		case typeRLE:	return L"RLE";
		case typeRLO:	return L"RLO";
		
		case typePDF:	return L"PDF";
		case typeEN:	return L"EN";
		case typeES:	return L"ES";
		case typeET:	return L"ET";
		case typeAN:	return L"AN";
		case typeCS:	return L"CS";
		case typeNSM:	return L"NSM";

		case typeBN:	return L"BN";
		case typeB:	return L"B";
		case typeS:	return L"S";
		case typeWS:	return L"WS";
		case typeON:	return L"ON";
		}

	#endif

	return NULL;
	}

CBidi::CCharType CBidi::FindStrongDir(CCharType Type)
{
	switch( Type ) {

		case typeR:
		case typeEN:
		case typeAN:
			return typeR;

		case typeL:
			return typeL;

		default:
			return Type;
		}
	}

// Type Classification

BOOL CBidi::IsStrongType(CCharType Type)
{
	switch( Type ) {

		case typeL:
		case typeLRE:
		case typeLRO:
		case typeR:
		case typeAL:
		case typeRLE:
		case typeRLO:
			return TRUE;
		
		default:
			return FALSE;
		}
	}

BOOL CBidi::IsWeakType(CCharType Type)
{
	switch( Type ) {

		case typePDF:
		case typeEN:
		case typeES:
		case typeET:
		case typeAN:
		case typeCS:
		case typeNSM:
		case typeBN:
			return TRUE;
		
		default:
			return FALSE;
		}
	}

BOOL CBidi::IsNeutralType(CCharType Type)
{
	switch( Type ) {

		case typeB:
		case typeS:
		case typeWS:
		case typeON:
			return TRUE;
		
		default:
			return FALSE;
		}
	}

// Character Type

CBidi::CCharType CBidi::GetType(WCHAR c)
{
	if( IsType(c, m_LTR) )			return typeL;

	if( IsType(c, m_RTL) )			return typeR;

	if( IsType(c, m_LRM) )			return typeL;

	if( IsType(c, m_RLM) )			return typeR;

	if( IsType(c, m_PDF) )			return typePDF;

	if( IsType(c, m_EuropeNumber) )		return typeEN;

	if( IsType(c, m_EuropeSeparator) )	return typeES;

	if( IsType(c, m_EuropeTerminator) )	return typeET;

	if( IsType(c, m_ArabicNumber) )		return typeAN;

	if( IsType(c, m_CommonSeparator) )	return typeCS;
	
	if( IsType(c, m_NonSpacingMark) )	return typeNSM;

	if( IsType(c, m_BlockSeparator) )	return typeB;

	if( IsType(c, m_SegmentSeparator) )	return typeS;

	if( IsType(c, m_WhiteSpace) )		return typeWS;

	if( IsType(c, m_OtherNeutral) )		return typeON;

	if( IsType(c, m_ArabicLetter) )		return typeAL;

//	AfxAssert(FALSE);

	return typeNA;
	}

// Type Table Access

BOOL CBidi::IsType(WCHAR c, CSpanDef const *pTable)
{
	UINT n = 0;

	CSpanDef Span;

	do {
		Span = pTable[n ++];

		if( c >= Span.cFrom && c < Span.cTo ) {
			
			return TRUE;
			}

		} while( Span.cFrom != Span.cTo );

	return FALSE;
	}

// Debug

void CBidi::ShowChars(UINT Prop)
{
	#if 0

	for( UINT n = 0; n < m_Chars.GetCount(); n ++ ) {

		CCharInfo const &Char = m_Chars[n];

		switch( Prop ) {
			
			case 0:
				AfxTrace(L"<%3c>", Char.m_cChar);
				break;
			
			case 1:
				AfxTrace(L"<%3s>", EnumTypes(Char.m_Type));
				break;
			
			case 2:
				AfxTrace(L"<%3s>", EnumTypes(GetType(Char.m_cChar)));
				break;
			
			case 3:
				AfxTrace(L"%1d", Char.m_uLevel);
				break;
			
			case 4:
				AfxTrace(L"%1c", Char.m_cChar);
				break;
			
			case 5:
				AfxTrace(L"<%2d>", Char.m_uPos);
				break;
			
			case 6:
				AfxTrace(L"%1d", n % 10);
				break;

			default:
				break;
			}
		}

	AfxTrace(L"\n");

	#endif
	}

// End of File
