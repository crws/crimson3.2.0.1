
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AutoLock_HPP

#define	INCLUDE_AutoLock_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Automatic Mutex Lock
//

class DLLAPI CAutoLock
{
public:
	// Constructors

	STRONG_INLINE CAutoLock(IMutex *pMutex)
	{
		m_fLock  = TRUE;

		pMutex->Wait(FOREVER);

		m_pMutex = pMutex;
	}

	STRONG_INLINE CAutoLock(IMutex *pMutex, BOOL fLock)
	{
		if( (m_fLock = fLock) ) {

			pMutex->Wait(FOREVER);
		}

		m_pMutex = pMutex;
	}

	// Destructor

	STRONG_INLINE ~CAutoLock(void)
	{
		if( m_fLock ) {

			m_pMutex->Free();
		}
	}

	// Operations

	STRONG_INLINE void Lock(BOOL fLock)
	{
		if( m_fLock != fLock ) {

			if( (m_fLock = fLock) ) {

				m_pMutex->Wait(FOREVER);
			}
			else
				m_pMutex->Free();
		}
	}

	STRONG_INLINE void Lock(void)
	{
		Lock(TRUE);
	}

	STRONG_INLINE void Free(void)
	{
		Lock(FALSE);
	}

protected:
	// Data Members
	IMutex * m_pMutex;
	BOOL     m_fLock;

private:
	// No Assign or Copy

	void operator = (CAutoLock const &That) const {};

	CAutoLock(CAutoLock const &That) {};
};

// End of File

#endif
