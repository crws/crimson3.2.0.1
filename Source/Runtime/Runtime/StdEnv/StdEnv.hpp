
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_StdEnv_HPP

#define INCLUDE_StdEnv_HPP

//////////////////////////////////////////////////////////////////////////
//
// Aeon Environment Flag
//

#define AEON_ENVIRONMENT 1

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "StdHeader.hpp"

#include "StdInterfaces.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tracking Control
//

#if defined(_DEBUG)

#define _TRACKING

#endif

//////////////////////////////////////////////////////////////////////////
//
// Tracking Allocators
//

#if defined(_TRACKING)

#define	 New    new(__FILE__, __LINE__)

#define _New(a) new((a), 0)

extern void * operator new    (size_t, char const *, int);

extern void * operator new [] (size_t, char const *, int);

global void AfxDumpBlock(IDiagOutput *pOut, void *pData);
global BOOL AfxCheckMemory(IDiagOutput *pOut);
global BOOL AfxCheckBlock(IDiagOutput *pOut, void *pData);
global BOOL AfxCheckBlock(void *pData);

#else

#define	 New    new

#define _New(a) new

#endif

global UINT AfxMarkMemory(void);
global void AfxDumpMemory(IDiagOutput *pOut, UINT uMark);
global void AfxDoNotTrack(BOOL fStart);

//////////////////////////////////////////////////////////////////////////
//
// Debugging
//

extern bool AfxHasDebug(void);
extern void AfxTraceArgs(PCTXT pText, va_list pArgs);
extern void AfxTrace(PCTXT pText, ...);
extern void AfxPrint(PCTXT pText);
extern void AfxDump(PCVOID pData, UINT uSize);
extern void AfxAssertFailed(PCTXT pFile, UINT uLine);
extern void AfxStackTrace(void);

//////////////////////////////////////////////////////////////////////////
//
// Pointer Validation
//

#if defined(_DEBUG)

extern void AfxAssertReadPtr(PCVOID pData, UINT uSize = 1);
extern void AfxAssertWritePtr(PVOID pData, UINT uSize = 1);
extern void AfxAssertStringPtr(PCTXT pText, UINT uSize = 0);
extern void AfxAssertStringPtr(PCUTF pText, UINT uSize = 0);

#else

inline void AfxAssertReadPtr(PCVOID pData, UINT uSize = 1) { }
inline void AfxAssertWritePtr(PVOID pData, UINT uSize = 1) { }
inline void AfxAssertStringPtr(PCTXT pText, UINT uSize = 0) { }
inline void AfxAssertStringPtr(PCUTF pText, UINT uSize = 0) { }

#endif

extern BOOL AfxCheckReadPtr(PCVOID pData, UINT uSize = 1);
extern BOOL AfxCheckWritePtr(PVOID pData, UINT uSize = 1);
extern BOOL AfxCheckStringPtr(PCTXT pText, UINT uSize = 0);
extern BOOL AfxCheckStringPtr(PCUTF pText, UINT uSize = 0);

//////////////////////////////////////////////////////////////////////////
//
// Formatting
//

extern UINT SPrintf(PTXT pBuffer, PCTXT pFormat, ...);
extern UINT SNPrintf(PTXT pBuffer, UINT uLimit, PCTXT pFormat, ...);
extern UINT VSPrintf(PTXT pBuffer, PCTXT pFormat, va_list pArgs);
extern UINT VSNPrintf(PTXT pBuffer, UINT uLimit, PCTXT pFormat, va_list pArgs);

//////////////////////////////////////////////////////////////////////////
//
// Assertion Macros
//

#if defined(_DEBUG)

#define AfxAssert(x) BEGIN { if( !(x) ) AfxAssertFailed(__FILE__, __LINE__); } END

#define AfxVerify(x) BEGIN { if( !(x) ) AfxAssertFailed(__FILE__, __LINE__); } END

#endif

//////////////////////////////////////////////////////////////////////////
//
// General Index Pointer
//

typedef struct tagINDEX { } const * INDEX;

//////////////////////////////////////////////////////////////////////////
//
// Missing String Prototypes
//

clink int strcasecmp(char const *, char const *);

clink int strncasecmp(char const *, char const *, unsigned int);

//////////////////////////////////////////////////////////////////////////
//
// Monotonic Clock
//

clink time_t getmonosecs(void);

//////////////////////////////////////////////////////////////////////////
//
// Wide String Mappings
//

#define	watof(x)  wcstof((x), NULL)
#define	watoi(x)  wcstol((x), NULL, 10)
#define	wisupper  iswupper
#define	wislower  iswlower
#define	wisalpha  iswalpha
#define	wisalnum  iswalnum
#define	wisspace  iswspace
#define	wisdigit  iswdigit
#define	wtoupper  towupper
#define	wtolower  towlower
#define	wstrlen   wcslen
#define	wstrcat   wcscat
#define	wstrcpy   wcscpy
#define	wstrncpy  wcsncpy
#define	wstrcmp   wcscmp
#define	wstricmp  wcscasecmp
#define	wstrncmp  wcsncmp
#define	wstrnicmp wcsncasecmp
#define	wstrchr   wcschr
#define	wstrrchr  wcsrchr
#define	wstrstr   wcsstr
#define	wstristr  wcsstr
#define	wstrspn   wcsspn
#define	wstrcspn  wcscspn
#define	wstrdup   wcsdup
#define	wstrdpi   wcsdpi
#define	wstrupr   wcsupr
#define	wstrlwr   wcslwr

//////////////////////////////////////////////////////////////////////////
//
// Converting Functions
//

extern wchar_t * wstrdup(char const *);

extern wchar_t * wstrdpi(wchar_t const *);

extern void      wstrcat(wchar_t *, char const *);

//////////////////////////////////////////////////////////////////////////
//
// Text Macro
//

#define	T(x) x

//////////////////////////////////////////////////////////////////////////
//
// Comparison Functions
//

inline int AfxCompare(PCTXT const &a, PCTXT const &b)
{
	return strcasecmp(a, b);
	}

inline int AfxCompare(PCUTF const &a, PCUTF const &b)
{
	return wcscasecmp(a, b);
	}

template <typename t> inline int AfxCompare(t const &a, t const &b)
{
	return (a > b) ? +1 : ((b > a ) ? -1 : 0);
	}

//////////////////////////////////////////////////////////////////////////
//
// Swap Function
//

template <typename type> inline void Swap(type &a, type &b)
{
	type c = a; a = b; b = c;
	}

//////////////////////////////////////////////////////////////////////////
//
// Zeroing Function
//

template <typename t> inline void AfxSetZero(t &a)
{
	a = 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// This Pointer Check
//

STRONG_INLINE bool ThisNotNull(void const *p)
{
	DWORD volatile &a = (DWORD volatile &) p;

	return a ? true : false;
	}

STRONG_INLINE bool ThisIsNull(void const *p)
{
	DWORD volatile &a = (DWORD volatile &) p;

	return a ? false : true;
	}

//////////////////////////////////////////////////////////////////////////
//
// Collection Templates
//

#include "Array.hpp"

#include "Linked.hpp"

#include "Tree.hpp"

#include "Map.hpp"

#include "ZeroMap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CString;

class CUnicode;

//////////////////////////////////////////////////////////////////////////
//
// Standard Collections
//

typedef CArray	 <BYTE>			CByteArray;

typedef CArray	 <WORD>			CWordArray;

typedef CArray	 <UINT>			CUIntArray;

typedef CArray	 <DWORD>		CLongArray;

typedef CArray	 <CString>		CStringArray;

typedef CZeroMap <CString, CString>	CStringMap;

typedef CArray	 <CUnicode>		CUnicodeArray;

typedef CZeroMap <CUnicode, CUnicode>	CUnicodeMap;

typedef CByteArray const &		CBytes;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

#if !defined(PROJECT_STDENV)

#include "String.hpp"

#include "Unicode.hpp"

#endif

//////////////////////////////////////////////////////////////////////////
//
// Template Instantiations
//

#if !defined(PROJECT_STDENV)

extern template class CArray   <BYTE>;

extern template class CArray   <WORD>;

extern template class CArray   <DWORD>;

extern template class CArray   <CString>;

extern template class CZeroMap <CString, CString>;

extern template class CArray   <CUnicode>;

extern template class CZeroMap <CUnicode, CUnicode>;

#endif

//////////////////////////////////////////////////////////////////////////
//
// Path Helpers
//

extern bool PathAppendSlash(char *path);

extern void PathMakeAbsolute(char *path, char const *name);

extern void PathJoin(char *path, char const *p1, char const *p2);

extern void PathAppend(char *path, char const *name);

extern void PathFromFile(char *path, char const *name);

//////////////////////////////////////////////////////////////////////////
//
// Format Conversion
//

extern void     DecodeUtf(CString &Text);

extern void     EncodeUtf(CString &Text);

extern CString  UtfConvert(PCUTF p);

extern CUnicode UtfConvert(PCTXT p);

extern CString  UniConvert(PCUTF p);

extern CUnicode UniConvert(PCTXT p);

//////////////////////////////////////////////////////////////////////////
//
// Order Conversion
//

extern CUnicode UniVisual(CUnicode s);

extern void     UniVisual(CUnicode s, CUIntArray &Map);

extern void     UniVisual(CUnicode s, CUIntArray &MemToVis, CUIntArray &VisToMem, CUIntArray &MemLevel);

extern BOOL     UniIsComplex(CUnicode s);

extern BOOL     UniIsComplex(TCHAR c);

//////////////////////////////////////////////////////////////////////////
//
// Core Crypto Support
//

#include "Crypto.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Thread Handle
//

typedef IThread * HTHREAD;

//////////////////////////////////////////////////////////////////////////
//
// Executive Instantiators
//

extern IMutex     * Create_Mutex(void);
extern IMutex     * Create_Qutex(void);
extern IMutex     * Create_Rutex(void);
extern ISemaphore * Create_Semaphore(void);
extern ISemaphore * Create_Semaphore(UINT uCount);
extern IEvent     * Create_ManualEvent(void);
extern IEvent     * Create_AutoEvent(void);

//////////////////////////////////////////////////////////////////////////
//
// Executive API
//

extern HTHREAD  CreateThread(PENTRY pfnProc, UINT uLevel, void *pParam, UINT uParam);
extern HTHREAD  CreateClientThread(IClientProcess *pProc, UINT uIndex, UINT uLevel);
extern HTHREAD  CreateClientThread(IClientProcess *pProc, UINT uIndex, UINT uLevel, ISemaphore *pTms);
extern BOOL     WaitThread(HTHREAD hThread, UINT uTime);
extern void     GuardThread(BOOL fGuard);
extern void     GuardThread(PGUARD pProc, PVOID pData);
extern BOOL     DestroyThread(HTHREAD hThread);
extern void     CheckThreadCancellation(void);
extern BOOL     AdvanceThread(HTHREAD hThread);
extern void     CloseThread(HTHREAD hThread);
extern void     ExitThread(int nCode);
extern void     Sleep(UINT uTime);
extern BOOL     ForceSleep(UINT uTime);
extern UINT     GetTickCount(void);
extern UINT     ToTicks(UINT uTime);
extern UINT     ToTime(UINT uTicks);
extern void     SetTimer(UINT uTime);
extern UINT     GetTimer(void);
extern void     SetThreadName(PCTXT pName);
extern PCTXT    GetThreadName(HTHREAD hThread);
extern HTHREAD  GetCurrentThread(void);
extern UINT     GetThreadIndex(void);
extern PVOID    GetThreadPtrParam(void);
extern UINT     GetThreadIntParam(void);
extern UINT     GetThreadExecState(HTHREAD hThread);
extern INT      GetThreadExitCode(HTHREAD hThread);
extern DWORD    GetThreadFlags(void);
extern void	SetThreadFlags(DWORD dwMask, DWORD dwData);
extern HDATA    GetThreadData(UINT uID);
extern BOOL     SetThreadData(HDATA hData);
extern BOOL     FreeThreadData(UINT uID);
extern UINT     AddExecThreadNotify(IThreadNotify *pNotify);
extern BOOL     RemoveExecThreadNotify(UINT uNotify);
extern UINT     AddThreadNotify(IThreadNotify *pNotify);
extern ITimer * CreateTimer(void);
extern void     SetThreadLibPointer(PVOID pEh);
extern PVOID    GetThreadLibPointer(void);
extern UINT     WaitMultiple(IWaitable **pList, UINT uCount, UINT uTime);
extern UINT     WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, UINT uTime);
extern UINT     WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, IWaitable *pWait3, UINT uTime);

// End of File

#endif
