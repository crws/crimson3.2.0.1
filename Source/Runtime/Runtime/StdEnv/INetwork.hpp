
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_INetwork_HPP

#define INCLUDE_INetwork_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 7 -- TCP/IP Networking Interfaces
//

interface ISocket;
interface IDnsResolver;
interface IPacketCapture;
interface IEthernetStatus;
interface ICellStatus;
interface IWifiStatus;
interface ILocationSource;
interface ITimeSource;
interface INetworkInfo;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "IpAddr.hpp"

#include "MacAddr.hpp"

#include "String.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Interface Status Info
//

struct CInterfaceStatusInfo
{
	CInterfaceStatusInfo(void)
	{
		m_dwSize = sizeof(*this);
	}

	DWORD	m_dwSize;
	BOOL	m_fValid;
	CString m_Device;
	DWORD   m_MxTime;
	INT64   m_RxBytes;
	INT64   m_TxBytes;
	BOOL	m_fOnline;
	BOOL	m_fReqOff;
};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Status Info
//

struct CEthernetStatusInfo : public CInterfaceStatusInfo
{
	CEthernetStatusInfo(void)
	{
		m_dwSize = sizeof(*this);
	}

	BOOL	m_fCarrier;
	BOOL	m_fFullDuplex;
	UINT	m_uLinkSpeed;
};

//////////////////////////////////////////////////////////////////////////
//
// Wireless Status Info
//

struct CWirelessStatusInfo : public CInterfaceStatusInfo
{
	CWirelessStatusInfo(void)
	{
		m_dwSize = sizeof(*this);
	}

	CString m_State;
	CString	m_Network;
	CString m_Model;
	CString m_Version;
	UINT	m_uSignal;
	DWORD   m_CTime;
};

//////////////////////////////////////////////////////////////////////////
//
// Cell Status Info
//

struct CCellStatusInfo : public CWirelessStatusInfo
{
	CCellStatusInfo(void)
	{
		m_dwSize = sizeof(*this);
	}

	BOOL	m_fRegister;
	BOOL	m_fRoam;
	UINT	m_uSlot;
	CString m_Service;
	CString	m_Carrier;
	CString m_Iccid;
	CString m_Imsi;
	CIpAddr m_Addr;
	CString m_Imei;
};

//////////////////////////////////////////////////////////////////////////
//
// WiFi Status Info
//

struct CWiFiStatusInfo : public CWirelessStatusInfo
{
	CWiFiStatusInfo(void)
	{
		m_dwSize = sizeof(*this);
	}

	UINT	 m_fApMode;
	UINT	 m_uChannel;
	CMacAddr m_PeerMac;
};

//////////////////////////////////////////////////////////////////////////
//
// WiFi Network Info
//

struct CWiFiNetworkInfo
{
	CWiFiNetworkInfo(void)
	{
		m_dwSize = sizeof(*this);
	}

	DWORD	m_dwSize;
	CString	m_Network;
	UINT	m_uSignal;
};

//////////////////////////////////////////////////////////////////////////
//
// Location Source Info
//

struct CLocationSourceInfo
{
	CLocationSourceInfo(void)
	{
		m_dwSize = sizeof(*this);
	}

	DWORD	m_dwSize;
	UINT	m_uFix;
	UINT	m_uSeq;
	double	m_Lat;
	double	m_Long;
	double	m_Alt;
};

//////////////////////////////////////////////////////////////////////////
//
// Time Source Info
//

struct CTimeSourceInfo
{
	CTimeSourceInfo(void)
	{
		m_dwSize = sizeof(*this);
	}

	DWORD	m_dwSize;
	BOOL	m_fValid;
	INT64	m_Remote;
	INT64	m_Local;
	int	m_Zone;
	int	m_Dst;
};

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class     CBuffer;

struct    MACADDR;

//////////////////////////////////////////////////////////////////////////
//
// Wrapper References
//

typedef class CMacAddr const & MACREF;

//////////////////////////////////////////////////////////////////////////
//
// Socket Phases
//

#define	PHASE_IDLE	0
#define	PHASE_OPENING	1
#define	PHASE_OPEN	2
#define	PHASE_CLOSING	3
#define	PHASE_ERROR	4

//////////////////////////////////////////////////////////////////////////
//
// Socket Options
//

#define	OPT_SEND_MSS	3
#define	OPT_RECV_MSS	4
#define	OPT_SEND_QUEUE	5
#define	OPT_RECV_QUEUE	6
#define	OPT_LINGER	7
#define	OPT_ADDRESS	8
#define	OPT_LOCAL	9
#define	OPT_KEEP_ALIVE	10
#define	OPT_NAGLE	11
#define OPT_DEBUG	12
#define OPT_GET_CHILD	13
#define OPT_SET_CHILD	14

//////////////////////////////////////////////////////////////////////////
//
// IP Protocol Codes
//

#define	IP_STEALTH	0x8000

#define	IP_ICMP		0x0001
#define	IP_UDP		0x0011
#define	IP_TCP		0x0006
#define	IP_RAW		0x00FE
#define	IP_QUEUE	0x00FF

//////////////////////////////////////////////////////////////////////////
//
// Interface Types
//

#define IT_LOOPBACK	0
#define IT_UNKNOWN	1
#define IT_ETHERNET	2
#define IT_MODEM	3
#define IT_CELLULAR	4
#define IT_WIFI		5
#define IT_TUNNEL	6
#define IT_NETKEY	7
#define IT_COUNT	8

//////////////////////////////////////////////////////////////////////////
//
// Socket Interface
//

interface ISocket : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 1);

	virtual HRM Listen(WORD Loc)				                    = 0;
	virtual HRM Listen(IPADDR const &IP, WORD Loc)		                    = 0;
	virtual HRM Connect(IPADDR const &IP, WORD Rem)	                            = 0;
	virtual HRM Connect(IPADDR const &IP, WORD Rem, WORD Loc)                   = 0;
	virtual HRM Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc) = 0;
	virtual HRM Recv(PBYTE pData, UINT &uSize, UINT uTime)	                    = 0;
	virtual HRM Recv(PBYTE pData, UINT &uSize)		                    = 0;
	virtual HRM Send(PBYTE pData, UINT &uSize)		                    = 0;
	virtual HRM Recv(CBuffer * &pBuff, UINT uTime)		                    = 0;
	virtual HRM Recv(CBuffer * &pBuff)			                    = 0;
	virtual HRM Send(CBuffer   *pBuff)			                    = 0;
	virtual HRM GetLocal(IPADDR &IP)			                    = 0;
	virtual HRM GetRemote(IPADDR &IP)			                    = 0;
	virtual HRM GetLocal(IPADDR &IP, WORD &Port)		                    = 0;
	virtual HRM GetRemote(IPADDR &IP, WORD &Port)		                    = 0;
	virtual HRM GetPhase(UINT &Phase)			                    = 0;
	virtual HRM SetOption(UINT uOption, UINT uValue)	                    = 0;
	virtual HRM Abort(void)				                            = 0;
	virtual HRM Close(void)				                            = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// DNS Resolver Interface
//

interface IDnsResolver : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 9);

	virtual CIpAddr METHOD Resolve(PCTXT pName)			    = 0;
	virtual BOOL    METHOD Resolve(CArray <CIpAddr> &List, PCTXT pName) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Packet Capture Interface
//

interface IPacketCapture : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 10);

	virtual BOOL  METHOD IsCaptureRunning(void)               = 0;
	virtual PCSTR METHOD GetCaptureFilter(void)               = 0;
	virtual UINT  METHOD GetCaptureSize(void)                 = 0;
	virtual BOOL  METHOD CopyCapture(PBYTE pData, UINT uSize) = 0;
	virtual BOOL  METHOD StartCapture(PCSTR pFilter)          = 0;
	virtual void  METHOD StopCapture(void)		          = 0;
	virtual void  METHOD KillCapture(void)		          = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Generic Status Interface
//

interface IInterfaceStatus : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 17);

	virtual BOOL METHOD GetInterfaceStatus(CInterfaceStatusInfo &Info) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Status Interface
//

interface IEthernetStatus : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 15);

	virtual BOOL METHOD GetEthernetStatus(CEthernetStatusInfo &Info) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Cellular Status Interface
//

interface ICellStatus : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 11);

	virtual BOOL METHOD GetCellStatus(CCellStatusInfo &Info) = 0;
	virtual BOOL METHOD SendCommand(PCTXT pCmd)              = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// WiFi Status Interface
//

interface IWiFiStatus : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 16);

	virtual BOOL METHOD GetWiFiStatus(CWiFiStatusInfo &Info)         = 0;
	virtual BOOL METHOD ScanNetworks(CArray<CWiFiNetworkInfo> &List) = 0;
	virtual BOOL METHOD SendCommand(PCTXT pCmd)                      = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Time Source Interface
//

interface ITimeSource : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 13);

	virtual BOOL METHOD GetTimeData(CTimeSourceInfo &Info) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Location Source Interface
//

interface ILocationSource : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 14);

	virtual BOOL METHOD GetLocationData(CLocationSourceInfo &Info) = 0;
	virtual BOOL METHOD GetLocationTime(CTimeSourceInfo &Info)     = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Network Utilities Interface
//

interface INetUtilities : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 12);

	virtual BOOL  METHOD SetInterfaceNames(CStringArray const &Names)      = 0;
	virtual BOOL  METHOD SetInterfaceDescs(CStringArray const &Descs)      = 0;
	virtual BOOL  METHOD SetInterfacePaths(CStringArray const &Paths)      = 0;
	virtual BOOL  METHOD SetInterfaceIds(CUIntArray const &Ids)	       = 0;
	virtual UINT  METHOD GetInterfaceCount(void)	                       = 0;
	virtual UINT  METHOD FindInterface(CString const &Name)		       = 0;
	virtual UINT  METHOD FindInterface(UINT Id)			       = 0;
	virtual BOOL  METHOD HasInterfaceType(UINT uType)                      = 0;
	virtual UINT  METHOD GetInterfaceType(UINT uFace)                      = 0;
	virtual UINT  METHOD GetInterfaceOrdinal(UINT uFace)                   = 0;
	virtual BOOL  METHOD GetInterfaceName(UINT uFace, CString &Name)       = 0;
	virtual BOOL  METHOD GetInterfaceDesc(UINT uFace, CString &Desc)       = 0;
	virtual BOOL  METHOD GetInterfacePath(UINT uFace, CString &Path)       = 0;
	virtual BOOL  METHOD GetInterfaceMac(UINT uFace, MACADDR &Mac)         = 0;
	virtual BOOL  METHOD GetInterfaceAddr(UINT uFace, IPADDR &Addr)        = 0;
	virtual BOOL  METHOD GetInterfaceMask(UINT uFace, IPADDR &Mask)        = 0;
	virtual BOOL  METHOD GetInterfaceGate(UINT uFace, IPADDR &Gate)        = 0;
	virtual BOOL  METHOD GetInterfaceStatus(UINT uFace, CString &Status)   = 0;
	virtual BOOL  METHOD IsInterfaceUp(UINT uFace)                         = 0;
	virtual DWORD METHOD GetOption(BYTE bType)                             = 0;
	virtual DWORD METHOD GetOption(BYTE bType, UINT uFace)                 = 0;
	virtual BOOL  METHOD IsBroadcast(IPADDR const &IP)                     = 0;
	virtual BOOL  METHOD GetBroadcast(IPADDR const &IP, IPADDR &Broad)     = 0;
	virtual BOOL  METHOD EnumBroadcast(UINT &uFace, IPADDR &Broad)         = 0;
	virtual BOOL  METHOD IsFastAddress(IPADDR const &IP)                   = 0;
	virtual BOOL  METHOD IsSafeAddress(IPADDR const &IP)                   = 0;
	virtual BOOL  METHOD GetDefaultGateway(IPADDR const &IP, IPADDR &Gate) = 0;
	virtual UINT  METHOD Ping(IPADDR const &IP, UINT uTimeout)             = 0;
};

// End of File

#endif
