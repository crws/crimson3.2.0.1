
#pragma  once

#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////////////
//	
// GcCore Class Library
//
// Copyright (c) 2018 Granby Consulting LLC
//
// All Rights Reserved
//

#ifndef INCLUDE_Hex_HPP

#define INCLUDE_Hex_HPP

////////////////////////////////////////////////////////////////////////////////
//	
// Hex Encoder
//

class DLLAPI CHex
{
public:
	// Encoding Options
	enum Encoding
	{
		encLower = 0,
		encUpper = 1,
	};

	// Operations
	static CString    ToHex(PCBYTE p, size_t n, int enc = encLower);
	static CString    ToHex(CByteArray const &d, int enc = encLower);
	static CString    ToHex(CString const &s, int enc = encLower);
	static CByteArray ToBytes(CString const &s);
	static CString    ToAnsi(CString const &s);

protected:
	// Decode Helper
	template<typename dtype> static bool Decode(dtype &d, CString const &s, bool z);

	// Size Estimation
	static size_t GetEncodeSize(size_t s);
	static size_t GetDecodeSize(size_t s);

	// Encoding List
	static char const * GetList(int enc);
};

// End of File

#endif
