
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AutoFile_HPP

#define	INCLUDE_AutoFile_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Instantiated Classes
//

#include "AutoArray.hpp"

#include "String.hpp"

//////////////////////////////////////////////////////////////////////////
//								
// Automatic Standard I/O File
//

class DLLAPI CAutoFile
{
public:
	// Constructors

	STRONG_INLINE CAutoFile(void)
	{
		m_pFile = NULL;
	}

	STRONG_INLINE CAutoFile(FILE *pFile)
	{
		m_pFile = pFile;
	}

	STRONG_INLINE CAutoFile(char const *pName, char const *pMode)
	{
		m_pFile = fopen(pName, pMode);
	}

	STRONG_INLINE CAutoFile(char const *pName, char const *pMode1, char const *pMode2)
	{
		(m_pFile = fopen(pName, pMode1)) || (m_pFile = fopen(pName, pMode2));
	}

// Destructor

	STRONG_INLINE ~CAutoFile(void)
	{
		if( m_pFile ) {

			fclose(m_pFile);
		}
	}

// Attributes

	STRONG_INLINE bool End(void) const
	{
		return feof(m_pFile);
	}

	STRONG_INLINE UINT Tell(void) const
	{
		return ftell(m_pFile);
	}

	STRONG_INLINE UINT GetSize(void) const
	{
		UINT uTell = ftell(m_pFile);

		fseek(m_pFile, 0, SEEK_END);

		UINT uSize = ftell(m_pFile);

		fseek(m_pFile, uTell, SEEK_SET);

		return uSize;
	}

	STRONG_INLINE UINT GetPos(void) const
	{
		return ftell(m_pFile);
	}

	STRONG_INLINE time_t GetUnix(void) const
	{
		struct stat s;

		fstat(fileno(m_pFile), &s);

		return s.st_mtime;
	}

// Operations

	STRONG_INLINE FILE * TakeOver(void)
	{
		AfxAssert(m_pFile);

		FILE *pFile = m_pFile;

		m_pFile     = NULL;

		return pFile;
	}

	STRONG_INLINE bool Open(char const *pName, char const *pMode)
	{
		Close();

		if( !(m_pFile = fopen(pName, pMode)) ) {

			return false;
		}

		return true;
	}

	STRONG_INLINE bool Open(char const *pName, char const *pMode1, char const *pMode2)
	{
		Close();

		if( !(m_pFile = fopen(pName, pMode1)) ) {

			if( !(m_pFile = fopen(pName, pMode2)) ) {

				return false;
			}
		}

		return true;
	}

	STRONG_INLINE void Close(void)
	{
		if( m_pFile ) {

			fclose(m_pFile);

			m_pFile = NULL;
		}
	}

	STRONG_INLINE void Seek(UINT uPos)
	{
		fseek(m_pFile, uPos, SEEK_SET);
	}

	STRONG_INLINE void SeekFromHere(INT nPos)
	{
		fseek(m_pFile, nPos, SEEK_CUR);
	}

	STRONG_INLINE void SeekEnd(INT nPos = 0)
	{
		fseek(m_pFile, nPos, SEEK_END);
	}

	STRONG_INLINE void Truncate(void)
	{
		fflush(m_pFile);

		ftruncate(fileno(m_pFile), GetPos());
	}

	template <typename tname> STRONG_INLINE UINT Read(tname *pData, UINT uCount)
	{
		return fread(pData, sizeof(tname), uCount, m_pFile);
	}

	template <typename tname> STRONG_INLINE UINT Read(CAutoArray<tname> &Data, UINT uCount)
	{
		return fread((tname *) Data, sizeof(tname), uCount, m_pFile);
	}

	template <typename tname> STRONG_INLINE UINT Write(tname const *pData, UINT uCount)
	{
		return fwrite(pData, sizeof(tname), uCount, m_pFile);
	}

	template <typename tname> STRONG_INLINE UINT Write(CAutoArray<tname> const &Data, UINT uCount)
	{
		return fwrite((tname const *) Data, sizeof(tname), uCount, m_pFile);
	}

	template <typename tname> STRONG_INLINE UINT Write(CArray<tname> const &Data)
	{
		return fwrite(Data.data(), sizeof(tname), Data.size(), m_pFile);
	}

	STRONG_INLINE bool Write(CString const &Text)
	{
		return fwrite(Text.data(), 1, Text.size(), m_pFile) == Text.size();
	}

	STRONG_INLINE UINT ReadVoid(void *pData, UINT uSize)
	{
		return fread(pData, 1, uSize, m_pFile);
	}

	STRONG_INLINE UINT WriteVoid(void const *pData, UINT uSize)
	{
		return fwrite(pData, 1, uSize, m_pFile);
	}

	UINT PutLine(char const *pLine)
	{
		UINT n = strlen(pLine);

		if( fwrite(pLine, 1, n, m_pFile) == n ) {

			#if defined(AEON_PLAT_LINUX)

			char c[] = { '\n' };

			#else

			char c[] = { '\r', '\n' };

			#endif

			if( fwrite(&c, 1, sizeof(c), m_pFile) == sizeof(c) ) {

				return n + sizeof(c);
			}
		}

		return 0;
	}

	CString GetLine(void)
	{
		if( m_pFile ) {

			char sLine[512] = { 0 };

			fgets(sLine, sizeof(sLine), m_pFile);

			if( sLine[0] ) {

				UINT n = strlen(sLine);

				while( n && (sLine[n-1] == '\n' || sLine[n-1] == '\r') ) {

					n--;
				}

				return CString(sLine, n);
			}
		}

		return "";
	}

	// Conversion

	STRONG_INLINE operator bool(void) const
	{
		return m_pFile ? true : false;
	}

	STRONG_INLINE operator FILE * & (void)
	{
		return m_pFile;
	}

// Operators

	STRONG_INLINE bool operator ! (void) const
	{
		return m_pFile ? false : true;
	}

protected:
	// Data Members
	FILE * m_pFile;

private:
	// No Assign or Copy

	void operator = (CAutoFile const &That) const {};

	CAutoFile(CAutoFile const &That) {};
};

// End of File

#endif
