
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI <--- Shouldn't be needed as explicitly included!!!

#ifndef INCLUDE_ProcX86_HPP

#define INCLUDE_ProcX86_HPP

//////////////////////////////////////////////////////////////////////////
//
// Processor Flag
//

#define AEON_PROC_X86

#define AEON_LITTLE_ENDIAN

#define _E_LITTLE

//////////////////////////////////////////////////////////////////////////
//
// Breakpoint Macro
//

#if !defined(__INTELLISENSE__)

#if defined(AEON_COMP_MSVC)

#define ProcNop()	__asm nop

#define ProcTrap(n)	__asm int 3

#define ProcBreak()	__asm int 3

#endif

#if defined(AEON_COMP_GCC)

#define ProcNop()	__asm ("nop")

#define ProcTrap(n)	__asm ("int $3")

#define ProcBreak()	__asm ("int $3")

#endif

#else

#define ProcNop()   ((void) 0)

#define ProcTrap(n) ((void) n)

#define ProcBreak() ((void) 0)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Naked Functions
//

#if !defined(__INTELLISENSE__) && defined(AEON_COMP_MSVC)

#define NAKED __declspec(naked)

#else

#define NAKED

#endif

//////////////////////////////////////////////////////////////////////////
//
// Byte Ordering Helpers
//

#define	MotorToHost(x)	HostToMotor(x)

#define	IntelToHost(x)	HostToIntel(x)

//////////////////////////////////////////////////////////////////////////
//
// Motorola Byte Ordering
//

STRONG_INLINE WORD HostToMotor(WORD Data)
{
	#if !defined(__INTELLISENSE__)

	#if defined(AEON_COMP_MSVC)

	ASM
	{
		mov	ax, Data;
		ror	ax, 8;
		}

	#elif defined(AEON_COMP_GCC)

	ASM(	"ror $8, %0\n\t"
		: "+r"(Data)
		);

	return Data;

	#endif

	#else

	BYTE lo = LOBYTE(Data);
	BYTE hi = HIBYTE(Data);

	return MAKEWORD(hi,lo);

	#endif
	}

STRONG_INLINE DWORD HostToMotor(DWORD Data)
{
	#if !defined(__INTELLISENSE__)

	#if defined(AEON_COMP_MSVC)

	ASM
	{
		mov	eax, Data;
		bswap	eax; 
		}

	#elif defined(AEON_COMP_GCC)

	ASM(	"bswap %0\n\t"
		: "+r"(Data)
		);

	return Data;

	#endif

	#else

	WORD lo = HostToMotor(LOWORD(Data));
	WORD hi = HostToMotor(HIWORD(Data));

	return MAKELONG(hi,lo);

	#endif
	}

STRONG_INLINE LONG HostToMotor(LONG Data)
{
	#if !defined(__INTELLISENSE__)

	#if defined(AEON_COMP_MSVC)

	ASM
	{
		mov	eax, Data;
		bswap	eax; 
		}

	#elif defined(AEON_COMP_GCC)

	ASM(	"bswap %0\n\t"
		: "+r"(Data)
		);

	return Data;

	#endif

	#else

	WORD lo = HostToMotor(LOWORD(Data));
	WORD hi = HostToMotor(HIWORD(Data));

	return MAKELONG(hi,lo);

	#endif
	}

STRONG_INLINE INT64 HostToMotor(INT64 Data)
{
	#if !defined(__INTELLISENSE__)

	#if defined(AEON_COMP_MSVC)

	DWORD d0 = PDWORD(&Data)[0];
	DWORD d1 = PDWORD(&Data)[1];

	ASM
	{
		mov	eax, d1;
		bswap	eax;
		mov	edx, d0;
		bswap	edx;
		}

	#elif defined(AEON_COMP_GCC)

	DWORD lo = DWORD((Data >>  0) & 0xFFFFFFFF);
	DWORD hi = DWORD((Data >> 32) & 0xFFFFFFFF);

	return INT64(MotorToHost(hi)) | (INT64(MotorToHost(lo)) << 32);

	#endif

	#else

	DWORD lo = DWORD((Data >>  0) & 0xFFFFFFFF);
	DWORD hi = DWORD((Data >> 32) & 0xFFFFFFFF);

	return INT64(MotorToHost(hi)) | (INT64(MotorToHost(lo)) << 32);

	#endif
	}

//////////////////////////////////////////////////////////////////////////
//
// Intel Byte Ordering
//

STRONG_INLINE WORD HostToIntel(WORD Data)
{
	return Data;
	}

STRONG_INLINE DWORD HostToIntel(DWORD Data)
{
	return Data;
	}

STRONG_INLINE LONG HostToIntel(LONG Data)
{
	return Data;
	}

STRONG_INLINE INT64 HostToIntel(INT64 Data)
{
	return Data;
	}

///////////////////////////////////////////////////////////////////////
//								
// Network Byte Ordering
//

STRONG_INLINE WORD HostToNet(WORD const Data)
{
	return HostToMotor(Data);
	}

STRONG_INLINE DWORD HostToNet(DWORD const Data)
{
	return HostToMotor(Data);
	}

STRONG_INLINE WORD NetToHost(WORD const Data)
{
	return MotorToHost(Data);
	}

STRONG_INLINE DWORD NetToHost(DWORD const Data)
{
	return MotorToHost(Data);
	}

// End of File

#endif
