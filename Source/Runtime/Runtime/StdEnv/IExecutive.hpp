
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IExecutive_HPP

#define INCLUDE_IExecutive_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 4 -- Executive Objects
//

// https://redlion.atlassian.net/wiki/x/iQFQEQ

interface IExecutive;
interface IThread;
interface IWaitMultiple;
interface ITimer;
interface IThreadNotify;
interface IEventSink;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "IWaitable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cancellation Exception
//

struct CExecCancel { int r; };

//////////////////////////////////////////////////////////////////////////
//
// Task Entry Point
//

typedef int (*PENTRY)(IThread *, void *, UINT);

//////////////////////////////////////////////////////////////////////////
//
// Guard Callback
//

typedef void (*PGUARD)(IThread *, void *);

//////////////////////////////////////////////////////////////////////////
//
// Thread Data Base Class
//

class CThreadData
{
	public:
		// Constructor
		CThreadData(UINT uID) : m_uID(uID) { }

		// Destructor
		virtual ~CThreadData(void) { };
		
		// Data Members
		UINT	      m_uID;
		CThreadData * m_pPrev;
		CThreadData * m_pNext;
	};

typedef class CThreadData * HDATA;

//////////////////////////////////////////////////////////////////////////
//
// MOS Priorities
//

#define THREAD_REALTIME	UINT(-1)

//////////////////////////////////////////////////////////////////////////
//
// Executive Core Object
//

interface IExecutive : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/7gFPEQ

	AfxDeclareIID(4, 1);

	virtual void      METHOD Open(void)									 = 0;
	virtual void	  METHOD Close(void)									 = 0;
	virtual IThread * METHOD CreateThread(PENTRY pfnProc, UINT uLevel, void *pParam, UINT uParam)            = 0;
	virtual IThread * METHOD CreateThread(IClientProcess *pProc, UINT uIndex, UINT uLevel)                   = 0;
	virtual IThread * METHOD CreateThread(IClientProcess *pProc, UINT uIndex, UINT uLevel, ISemaphore *pTms) = 0;
	virtual BOOL      METHOD DestroyThread(IThread *pTask)			                                 = 0;
	virtual IThread * METHOD GetCurrentThread(void)                                                          = 0;
	virtual UINT      METHOD GetCurrentIndex(void)                                                           = 0;
	virtual void      METHOD Sleep(UINT uTime)				                                 = 0;
	virtual BOOL	  METHOD ForceSleep(UINT uTime)				                                 = 0;
	virtual UINT      METHOD GetTickCount(void)					                         = 0;
	virtual UINT      METHOD ToTicks(UINT uTime)					                         = 0;
	virtual UINT      METHOD ToTime(UINT uTicks)					                         = 0;
	virtual UINT      METHOD AddNotify(IThreadNotify *pNotify)						 = 0;
	virtual BOOL	  METHOD RemoveNotify(UINT uNotify)							 = 0;
	virtual ITimer *  METHOD CreateTimer(void)								 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Executive Thread Object
//

interface IThread : public IWaitable
{
	// https://redlion.atlassian.net/wiki/x/ioFOEQ

	AfxDeclareIID(4, 2);

	enum
	{
		stateInitial,
		stateCreated,
		stateWaitInit,
		stateRunInit,
		stateWaitExec,
		stateRunExec,
		stateExecDone,
		stateRunStop,
		stateWaitTerm,
		stateRunTerm,
		stateTermDone,
		stateDestroyed
		};

	enum
	{
		exitOkay   =  0,
		exitCancel = -1,
		exitExcept = -2,
		exitNoInit = -3,
		};

	virtual PVOID METHOD GetObject(void)		          = 0;
	virtual void  METHOD SetName(PCTXT pName)	          = 0;
	virtual PCTXT METHOD GetName(void)		          = 0;
	virtual UINT  METHOD GetIdent(void)		          = 0;
	virtual UINT  METHOD GetIndex(void)		          = 0;
	virtual PVOID METHOD GetPtrParam(void)		          = 0;
	virtual UINT  METHOD GetIntParam(void)		          = 0;
	virtual UINT  METHOD GetExecState(void)		          = 0;
	virtual INT   METHOD GetExitCode(void)		          = 0;
	virtual void  METHOD SetCancelFlag(void)	          = 0;
	virtual void  METHOD CheckCancellation(void)	          = 0;
	virtual UINT  METHOD GetTimer(void)		          = 0;
	virtual void  METHOD SetTimer(UINT uTime)	          = 0;
	virtual DWORD METHOD GetFlags(void)		          = 0;
	virtual void  METHOD SetFlags(DWORD dwMask, DWORD dwData) = 0;
	virtual HDATA METHOD GetData(UINT uID)		          = 0;
	virtual BOOL  METHOD SetData(HDATA hData)	          = 0;
	virtual BOOL  METHOD FreeData(UINT uID)		          = 0;
	virtual void  METHOD Guard(BOOL fGuard)		          = 0;
	virtual void  METHOD Guard(PGUARD pProc, void *pData)     = 0;
	virtual void  METHOD Exit(int nCode)		          = 0;
	virtual BOOL  METHOD Advance(void)		          = 0;
	virtual BOOL  METHOD Destroy(void)		          = 0;
	virtual UINT  METHOD AddNotify(IThreadNotify *pNotify)    = 0;
	virtual void  METHOD SetLibPointer(PVOID pLibData)        = 0;
	virtual PVOID METHOD GetLibPointer(void)	          = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
//  Wait Multiple
//

interface IWaitMultiple : public IUnknown
{
	// https://

	AfxDeclareIID(4, 3);

	virtual UINT METHOD WaitMultiple(IWaitable **pList, UINT uCount, UINT uTime)			      = 0;
	virtual UINT METHOD WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, UINT uTime)		      = 0;
	virtual UINT METHOD WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, IWaitable *pWait3, UINT uTime) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Timer Interface
//

interface ITimer : public IUnknown
{
	// https://

	AfxDeclareIID(4, 4);

	virtual void METHOD Enable(bool fEnable)                    = 0;
	virtual void METHOD SetPeriod(UINT uPeriod)                 = 0;
	virtual bool METHOD SetHook(IEventSink *pSink, UINT uParam) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Thread Notification
//

interface IThreadNotify : public IUnknown
{
	// https://

	AfxDeclareIID(4, 5);

	virtual UINT METHOD OnThreadCreate(IThread *pThread, UINT uIndex)	       = 0;
	virtual void METHOD OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Sink
//

interface IEventSink
{
	// Methods
	virtual void OnEvent(UINT uLine, UINT uParam) = 0;
	};

// End of File

#endif
