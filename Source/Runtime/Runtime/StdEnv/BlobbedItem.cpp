
#include "Intern.hpp"

#include "BlobbedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// Blob-Persisted Item
//

// Destructor

CBlobbedItem::~CBlobbedItem(void)
{
	}

// Persistance

void CBlobbedItem::Load(PCBYTE &pData)
{
	}

// End of File
