
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AutoObject_HPP

#define	INCLUDE_AutoObject_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Object Broker Reference
//

extern IObjectBroker * piob;

//////////////////////////////////////////////////////////////////////////
//								
// Automatic Object Macros
//

#define AfxGetAutoObject(v, n, x, i) CAutoObject<i> v(n, x, AfxAeonIID(i))

#define AfxNewAutoObject(v, n, i)    CAutoObject<i> v(n, AfxAeonIID(i))

//////////////////////////////////////////////////////////////////////////
//								
// Automatic Object Pointer
//

template <typename CObject> class DLLAPI CAutoObject
{
public:
	// Constructors

	STRONG_INLINE CAutoObject(CObject *pObject)
	{
		m_pObject = pObject;
	}

	STRONG_INLINE CAutoObject(PCSTR pName, UINT uInst, REFIID riid)
	{
		piob->GetObject(pName, uInst, riid, (void **) &m_pObject);
	}

	STRONG_INLINE CAutoObject(PCSTR pName, REFIID riid)
	{
		piob->NewObject(pName, riid, (void **) &m_pObject);
	}

	// Destructor

	STRONG_INLINE ~CAutoObject(void)
	{
		AfxRelease(m_pObject);
	}

	// Operations

	STRONG_INLINE CObject * TakeOver(void)
	{
		AfxAssert(m_pObject);

		CObject *pObject = m_pObject;

		m_pObject        = NULL;

		return pObject;
	}

	// Conversion

	STRONG_INLINE operator bool(void) const
	{
		return m_pObject ? true : false;
	}

	STRONG_INLINE operator CObject * (void) const
	{
		return m_pObject;
	}

	// Operators

	STRONG_INLINE bool operator ! (void) const
	{
		return m_pObject ? false : true;
	}

	STRONG_INLINE CObject * operator -> (void) const
	{
		return m_pObject;
	}

protected:
	// Data Members
	CObject * m_pObject;

private:
	// No Assign or Copy

	void operator = (CAutoObject const &That) const {};

	CAutoObject(CAutoObject const &That) {};
};

// End of File

#endif
