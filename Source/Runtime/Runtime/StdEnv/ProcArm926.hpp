
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_ProcArm926_HPP

#define INCLUDE_ProcArm926_HPP

//////////////////////////////////////////////////////////////////////////
//
// Processor Flags
//

#define AEON_PROC_Arm926

// End of File

#endif
