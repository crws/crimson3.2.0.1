
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_StdInterfaces_HPP

#define INCLUDE_StdInterfaces_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/XwGkE

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "ICore.hpp"

#include "IDiagnostic.hpp"

#include "IWaitable.hpp"

#include "IExecutive.hpp"

#include "IDisplayUx.hpp"

#include "IPort.hpp"

#include "IComms.hpp"

#include "IPeripheral.hpp"

#include "IMemory.hpp"

#include "IIdentity.hpp"

#include "IGpio.hpp"

#include "IUsb.hpp"

#include "IExpansion.hpp"

// End of File

#endif
