
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef	INCLUDE_ZeroMap_HPP
	
#define	INCLUDE_ZeroMap_HPP

/////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/6QSkE

/////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Map.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Template File Name
//

#undef	TP_FILE

#define	TP_FILE TEXT(__FILE__)

/////////////////////////////////////////////////////////////////////////
//
// Zeroed Map Collection
//

template <typename CName, typename CData> class CZeroMap : public CMap <CName, CData>
{
	public:
		// Constructors
		CZeroMap(void);
		CZeroMap(CZeroMap <CName, CData> const &That);

		// Assignment
		CZeroMap const & operator = (CZeroMap const &That);

		// Lookup Operator
		CData const & operator [] (CName const &Name) const;

	protected:
		// Data Members
		CData m_Null;
	};

/////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#undef	TP1

#undef	TP2

#define	TP1 template <typename CName, typename CData>

#define	TP2 CZeroMap <CName, CData>

/////////////////////////////////////////////////////////////////////////
//
// Map Collection
//

// Constructors

TP1 TP2::CZeroMap(void)
{
	AfxSetZero(m_Null);
	}

TP1 TP2::CZeroMap(CZeroMap <CName, CData> const &That) : CMap <CName, CData> (That)
{
	AfxSetZero(m_Null);
	}

// Assignment

// cppcheck-suppress operatorEqVarError

TP1 TP2 const & TP2::operator = (CZeroMap <CName, CData> const &That)
{
	CMap <CName, CData>::m_Tree = That.m_Tree;

	return ThisObject;
	}

// Lookup Operator

TP1 CData const & TP2::operator [] (CName const &Name) const
{
	CTree < CNamedPair <CName, CData> > const &Tree = CMap <CName, CData>::m_Tree;

	INDEX Index = Tree.Find(CNamedPair <CName, CData> (Name));

	return Tree.Failed(Index) ? m_Null : Tree[Index].GetData();
	}

// End of File

#endif
