
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IComms_HPP

#define INCLUDE_IComms_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 13 -- Comms Driver Objects
//

// https://

// TODO -- List actual interfaces, and then forward declarations.

interface IMetaData;
interface ICommsDevice;
interface ICommsHelper;
interface ISlaveHelper;
interface IImplicit;
interface IExplicit;
interface IDnpChannelConfig;
interface IDnpChannel;
interface IDnpHelper;
interface IDnpSession;
struct    IPADDR;

//////////////////////////////////////////////////////////////////////////
//
// Driver Categories
//

#define	DC_TYPE			0xFF00
#define	DC_SUBTYPE		0x00FF
#define	DC_NULL			0x0000
#define	DC_LINK			0x0100
#define	DC_LINK_MODEM		0x0101
#define	DC_COMMS		0x0200
#define	DC_COMMS_SLAVE		0x0201
#define	DC_COMMS_MASTER		0x0202
#define	DC_COMMS_KEYBOARD	0x0203
#define	DC_COMMS_PRINTER	0x0204
#define DC_COMMS_DISPLAY	0x0205
#define	DC_COMMS_PROGTHRU	0x0206
#define	DC_COMMS_RAWPORT	0x0207
#define	DC_COMMS_CAMERA		0x0208
#define	DC_COMMS_STREAMER	0x0209
#define DC_COMMS_PROCON		0x020A

//////////////////////////////////////////////////////////////////////////
//
// Driver Flags
//

#define DF_ANY_MODEL		0x0001
#define	DF_FORCE_SERVICE	0x0002
#define DF_REMOTED		0x0004
#define DF_NO_SERVICE		0x0008
#define DF_CALL_CLOSE		0x0010

//////////////////////////////////////////////////////////////////////////
//
// Master Flags
//

#define	MF_ATOMIC_WRITE		0x0001
#define	MF_NO_SPANNING		0x0002
#define	MF_SERVICE_TASK		0x0004
#define MF_TASK_NOTIFY		0x0008

//////////////////////////////////////////////////////////////////////////
//
// Completion Codes Type
//

typedef LONG	CCODE;

//////////////////////////////////////////////////////////////////////////
//
// Completion Codes Flags
//

// https://redlion.atlassian.net/wiki/x/MYALGg

#define CCODE_ERROR		0x80000000L
#define CCODE_HARD		0x40000000L
#define CCODE_BUSY		0x20000000L
#define CCODE_SILENT		0x10000000L
#define CCODE_NO_RETRY		0x08000000L
#define CCODE_NO_DATA		0x04000000L
#define CCODE_NO_CONFIG		0x02000000L
#define CCODE_SUCCESS		0x00000000L
#define CCODE_FLAGS		0xFE000000L

//////////////////////////////////////////////////////////////////////////
//
// Completion Codes Macros
//

#define COMMS_SUCCESS(x)	(CCODE(x) >= 0)

#define COMMS_ERROR(x)		(CCODE(x) <  0)

//////////////////////////////////////////////////////////////////////////
//								
// Address Item
//

struct DLLAPI CAddress
{
	union {
		struct {

			#if defined(AEON_PROC_X86) || defined(AEON_PROC_ARM)

			DWORD	m_Offset : 16;
			DWORD	m_Table : 8;
			DWORD	m_Extra : 4;
			DWORD	m_Type : 4;

			#else

			DWORD	m_Type : 4;
			DWORD	m_Extra : 4;
			DWORD	m_Table : 8;
			DWORD	m_Offset : 16;

			#endif

		} a;

		DWORD m_Ref;
	};
};

typedef CAddress const & AREF;

//////////////////////////////////////////////////////////////////////////
//								
// Address Table Codes
//

enum AddrTable
{
	addrEmpty	= 0x00,
	addrNamed	= 0xF0,
};

//////////////////////////////////////////////////////////////////////////
//								
// Address Type Codes
//

enum AddrType
{
	addrBitAsBit	= 0x00,
	addrBitAsByte	= 0x01,
	addrBitAsWord	= 0x02,
	addrBitAsLong	= 0x03,
	addrBitAsReal	= 0x04,
	addrByteAsByte	= 0x05,
	addrByteAsWord	= 0x06,
	addrByteAsLong	= 0x07,
	addrByteAsReal	= 0x08,
	addrWordAsWord	= 0x09,
	addrWordAsLong	= 0x0A,
	addrWordAsReal	= 0x0B,
	addrLongAsLong	= 0x0C,
	addrLongAsReal	= 0x0D,
	addrRealAsReal	= 0x0E,
	addrReserved	= 0x0F,
};

//////////////////////////////////////////////////////////////////////////
//
// Camera Format
//

enum
{
	image8bitGreyScale = 1,
	image24bitColor	   = 2,
	imagePNG	   = 4,
};

//////////////////////////////////////////////////////////////////////////
//
// RLC Bitmap
//

struct CBitmapRLC
{
	DWORD	Frame;
	DWORD	Format;
	DWORD	Width;
	DWORD	Height;
	DWORD	Stride;
	BYTE	Data[0];
};

//////////////////////////////////////////////////////////////////////////
//
// More Help Codes
//

#define IDH_ENETIP	1
#define IDH_DISPLAY	2
#define	IDH_SYNC	3
#define IDH_DEVNET	4
#define	IDH_EXTRA	5
#define IDH_MPI		6
#define	IDH_TRACE	7
#define IDH_DNP		8

//////////////////////////////////////////////////////////////////////////
//
// Driver Interface
//

interface IDriver : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/JIBgFg

	AfxDeclareIID(13, 1);

	virtual WORD METHOD GetIdentifier(void)		     = 0;
	virtual WORD METHOD GetCategory(void)		     = 0;
	virtual WORD METHOD GetFlags(void)		     = 0;
	virtual void METHOD SetHelper(ICommsHelper *pHelper) = 0;
	virtual void METHOD SetIdentifier(WORD wIdent)	     = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Generic Comms Driver Interface
//

interface ICommsDriver : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/OABhFg

	AfxDeclareIID(13, 2);

	virtual void  METHOD Load(LPCBYTE pData)			   = 0;
	virtual void  METHOD Load(LPCBYTE pData, UINT uSize)		   = 0;
	virtual void  METHOD CheckConfig(CSerialConfig &Config)		   = 0;
	virtual void  METHOD Attach(IPortObject *pPort)			   = 0;
	virtual void  METHOD Detach(void)				   = 0;
	virtual void  METHOD Open(void)					   = 0;
	virtual void  METHOD Close(void)				   = 0;
	virtual CCODE METHOD DeviceOpen(ICommsDevice *pDevice)		   = 0;
	virtual CCODE METHOD DeviceClose(BOOL fPersistConnection)	   = 0;
	virtual void  METHOD Service(void)				   = 0;
	virtual UINT  METHOD DrvCtrl(UINT uFunc, PCTXT pValue)		   = 0;
	virtual UINT  METHOD DevCtrl(void *pCtx, UINT uFunc, PCTXT pValue) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Slave Driver Interface
//

interface ICommsSlave : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/BwCRFg

	AfxDeclareIID(13, 3);

	virtual void METHOD SetHelper(ISlaveHelper *pHelper) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Master Driver Interface
//

interface ICommsMaster : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/BICSFg

	AfxDeclareIID(13, 4);

	virtual WORD  METHOD GetMasterFlags(void)			   = 0;
	virtual CCODE METHOD Ping(void)					   = 0;
	virtual CCODE METHOD Read(AREF Addr, PDWORD pData, UINT  uCount)   = 0;
	virtual CCODE METHOD Write(AREF Addr, PDWORD pData, UINT  uCount)  = 0;
	virtual CCODE METHOD Atomic(AREF Addr, DWORD  Data, DWORD dwMask) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Keyboard Driver Interface
//

interface ICommsKeyboard : public IUnknown
{
	// https://

	AfxDeclareIID(13, 5);
};

//////////////////////////////////////////////////////////////////////////
//
// Printer Driver Interface
//

interface ICommsPrinter : public IUnknown
{
	// https://

	AfxDeclareIID(13, 6);

	virtual void METHOD OutChar(char cData)	   = 0;
	virtual void METHOD OutString(PCTXT pData) = 0;
	virtual void METHOD OutNewLine(void)	   = 0;
	virtual void METHOD OutFormFeed(void)	   = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Display Driver Interface
//

interface ICommsDisplay : public IUnknown
{
	// https://

	AfxDeclareIID(13, 7);

	virtual UINT METHOD GetSize(int cx, int cy)									  = 0;
	virtual void METHOD Capture(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait) = 0;
	virtual void METHOD Service(UINT uDrop, PBYTE pLast, PBYTE pData, UINT uSize)					  = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Raw Port Driver Interface
//

interface ICommsRawPort : public IUnknown
{
	// https://

	AfxDeclareIID(13, 8);

	virtual void METHOD Connect(void)				 = 0;
	virtual void METHOD Disconnect(void)				 = 0;
	virtual UINT METHOD Read(UINT uTime)				 = 0;
	virtual UINT METHOD Write(BYTE bData, UINT uTime)		 = 0;
	virtual UINT METHOD Write(PCBYTE pData, UINT uCount, UINT uTime) = 0;
	virtual void METHOD SetRTS(BOOL fState)				 = 0;
	virtual BOOL METHOD GetCTS(void)				 = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Camera Driver Interface
//

interface ICommsCamera : public IUnknown
{
	// https://

	AfxDeclareIID(13, 9);

	virtual CCODE  METHOD Ping(void)					  = 0;
	virtual CCODE  METHOD ReadImage(PBYTE &pData)				  = 0;
	virtual void   METHOD SwapImage(PBYTE pData)				  = 0;
	virtual void   METHOD KillImage(void)					  = 0;
	virtual PCBYTE METHOD GetData(UINT uDevice)				  = 0;
	virtual UINT   METHOD GetInfo(UINT uDevice, UINT uIndex)		  = 0;
	virtual BOOL   METHOD SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile) = 0;
	virtual BOOL   METHOD LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile) = 0;
	virtual BOOL   METHOD UseSetup(PVOID pContext, UINT uIndex)		  = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Streamer Driver Interface
//

interface ICommsStreamer : public IUnknown
{
	// https://

	AfxDeclareIID(13, 10);

	virtual DWORD  METHOD GetNextTime(DWORD dwTime) = 0;
	virtual PCTXT  METHOD GetFilename(DWORD dwTime) = 0;
	virtual UINT   METHOD GetDataCount(void)	= 0;
	virtual PCBYTE METHOD GetDataPointer(void)	= 0;
	virtual DWORD  METHOD GetValue(UINT uIndex)     = 0;
	virtual void   METHOD FreeData(void)	        = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Raw CAN Comms Driver
//

interface ICommsRawCAN : public IUnknown
{
	// https://

	AfxDeclareIID(13, 11);

	virtual BOOL METHOD MakePDU(UINT uID, UINT DLC, BOOL fExt, BOOL fRx)		  = 0;
	virtual BOOL METHOD RxPDU(UINT uID, PDWORD pData)				  = 0;
	virtual BOOL METHOD TxPDU(UINT uID, PDWORD pData)				  = 0;
	virtual BOOL METHOD MakeRxMailBox(UINT uBox, UINT uMask, UINT uFilter, UINT uDLC) = 0;
	virtual BOOL METHOD MakeTxMailBox(UINT uBox, UINT uId, UINT uDLC)		  = 0;
	virtual BOOL METHOD RxMail(PTXT Mail)						  = 0;
	virtual BOOL METHOD TxMailBox(UINT uBox, PDWORD pData)				  = 0;
	virtual UINT METHOD GetDLC(UINT uID, BOOL fRx)					  = 0;
	virtual UINT METHOD GetMailDLC(UINT uBox)					  = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Device Interface
//

interface ICommsDevice : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/GYCPFg

	AfxDeclareIID(13, 20);

	virtual LPCBYTE METHOD GetConfig(void)		= 0;
	virtual UINT    METHOD GetConfigSize(void)	= 0;
	virtual void    METHOD SetContext(PVOID pData)	= 0;
	virtual PVOID   METHOD GetContext(void)		= 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Helper Interface
//

interface ICommsHelper : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/FgCSFg

	AfxDeclareIID(13, 30);

	virtual void METHOD WontReturn(void)                = 0;
	virtual void METHOD StopSystem(void)                = 0;
	virtual BOOL METHOD MoreHelp(WORD ID, void **pHelp) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Slave Helper Interface
//

interface ISlaveHelper : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/IYCRFg

	AfxDeclareIID(13, 31);

	virtual CCODE METHOD Read(AREF Addr, PDWORD pData, UINT uCount)     = 0;
	virtual CCODE METHOD Read(AREF Addr, PDWORD pData, IMetaData **ppm) = 0;
	virtual CCODE METHOD Write(AREF Addr, PDWORD pData, UINT uCount)     = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Extra Helper Interface
//

interface IExtraHelper : public IUnknown
{
	// https://

	AfxDeclareIID(13, 32);

	virtual BOOL  METHOD GetRawPort(UINT uPort, ICommsRawPort **pPort)	= 0;
	virtual UINT  METHOD GetTimeStamp(UINT &Fraction)			= 0;
	virtual UINT  METHOD FindTagIndex(PCTXT pLabel)				= 0;
	virtual PTXT  METHOD GetTagLabel(UINT uIndex)				= 0;
	virtual PTXT  METHOD FormatTagData(UINT uIndex)				= 0;
	virtual BOOL  METHOD ValidateLogon(PCTXT u, PCTXT p, DWORD r)		= 0;
	virtual DWORD METHOD GetNow(void)					= 0;
	virtual BOOL  METHOD SetNow(DWORD dwTime)				= 0;
	virtual void  METHOD ExpCatLinkMode(BOOL fSlow)				= 0;
	virtual PCTXT METHOD WhoGetName(BOOL fGeneric)				= 0;
	virtual void  METHOD SetGPSTime(DWORD Time)				= 0;
	virtual PBYTE METHOD Png2RLCBmp(PBYTE pData, UINT uBytes)		= 0;
	virtual PBYTE METHOD Jpeg2RLCBmp(PBYTE pData, UINT uBytes, UINT uScale)	= 0;
	virtual PBYTE METHOD WinBmp2RLCBmp(PBYTE pData, UINT uBytes)		= 0;
	virtual BOOL  METHOD WriteFile(PCTXT pName, PBYTE pData, UINT  uCount)	= 0;
	virtual BOOL  METHOD ReadFile(PCTXT pName, PBYTE pData, UINT &uCount)	= 0;
	virtual void  METHOD GetCodedItem(PCBYTE &pData, PVOID &pItem)		= 0;
	virtual DWORD METHOD ExecuteCoded(PVOID pItem, UINT uType)		= 0;
	virtual DWORD METHOD GetDnsAddress(PCTXT pName, DWORD Default)		= 0;
	virtual BOOL  METHOD GetMacId(UINT uPort, PBYTE pMac)			= 0;
	virtual BOOL  METHOD IsBroadcast(DWORD Ip)				= 0;
	virtual BOOL  METHOD GetBroadcast(DWORD Ip, DWORD &Broadcast)		= 0;
	virtual BOOL  METHOD EnumBroadcast(UINT &uFace, DWORD &Broadcast)	= 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Keyboard Helper Interface
//

interface IKeyboardHelper : public IUnknown
{
	// https://

	AfxDeclareIID(13, 33);

	virtual void METHOD PostKey(WORD wCode)	= 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet IP Helper Interface
//

interface IEthernetIPHelper : public IUnknown
{
	// https://

	AfxDeclareIID(13, 34);

	virtual WORD        METHOD GetVendorId(void)	= 0;
	virtual BOOL        METHOD Open(void)		= 0;
	virtual void        METHOD Close(void)		= 0;
	virtual IImplicit * METHOD GetImplicit(void)	= 0;
	virtual IExplicit * METHOD GetExplicit(void)	= 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Display Helper Interface
//

interface IDisplayHelper : public IUnknown
{
	// https://

	AfxDeclareIID(13, 35);

	virtual UINT METHOD GetType(void) = 0;
	virtual INT  METHOD GetCx(void)	  = 0;
	virtual INT  METHOD GetCy(void)	  = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Sync Object Helper Interface
//

interface ISyncHelper : public IUnknown
{
	// https://

	AfxDeclareIID(13, 36);

	virtual IEvent     * METHOD CreateAutoEvent(void)	 = 0;
	virtual IEvent     * METHOD CreateManualEvent(void)	 = 0;
	virtual ISemaphore * METHOD CreateSemaphore(UINT uCount) = 0;
	virtual IMutex     * METHOD CreateMutex(void)		 = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Helper Interface
//

interface IDeviceNetHelper : public IUnknown
{
	// https://

	AfxDeclareIID(13, 37);

	virtual void METHOD Attach(IPortObject *pPort)			    = 0;
	virtual void METHOD Detach(void)				    = 0;
	virtual BOOL METHOD Open(void)					    = 0;
	virtual void METHOD Close(void)					    = 0;
	virtual void METHOD SetMac(BYTE bMac)				    = 0;
	virtual BOOL METHOD EnableBitStrobe(WORD wRecvSize, WORD wSendSize) = 0;
	virtual BOOL METHOD EnablePolled(WORD wRecvSize, WORD wSendSize)    = 0;
	virtual BOOL METHOD EnableData(WORD wRecvSize, WORD wSendSize)	    = 0;
	virtual BOOL METHOD IsOnLine(void)				    = 0;
	virtual UINT METHOD Read(UINT uId, PBYTE pData, UINT uSize)	    = 0;
	virtual UINT METHOD Write(UINT uId, PCBYTE pData, UINT uSize)	    = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// DNP Helper Interface
//

interface IDnpHelper : public IUnknown
{
	// https://

	AfxDeclareIID(13, 38);

	virtual IDnpChannelConfig * METHOD GetConfig(IPADDR IP, IPADDR IP2, WORD wTcp, WORD wUdp, WORD wTO)		       = 0;
	virtual IDnpChannel	  * METHOD OpenChannel(IDataHandler *pHandler, WORD wSrc, BOOL fMaster)	                       = 0;
	virtual IDnpChannel       * METHOD OpenChannel(IDnpChannelConfig *pConfig, WORD wSrc, BOOL fMaster)	               = 0;
	virtual BOOL		    METHOD CloseChannel(void *pVoid, WORD wSrc, BOOL fMaster)	                               = 0;
	virtual IDnpSession       * METHOD OpenSession(IDnpChannel *pChannel, WORD wDest, WORD wTO, DWORD dwLink, void *pCfg)  = 0;
	virtual BOOL		    METHOD CloseSession(IDnpSession *pSession)					               = 0;
	virtual BOOL		    METHOD CloseSessions(IDnpChannel *pChannel)					               = 0;
	virtual void		    METHOD Service(void)								       = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// MPI Helper Interface
//

interface IMPIHelper : public IUnknown
{
	// https://

	AfxDeclareIID(13, 39);

};

//////////////////////////////////////////////////////////////////////////
//
// Trace Interface
//

interface ITraceHelper : public IUnknown
{
	// https://

	AfxDeclareIID(13, 40);

};

//////////////////////////////////////////////////////////////////////////
//
// Metadata Access
//

interface IMetaData : public IUnknown
{
	// https://

	AfxDeclareIID(13, 41);

	virtual DWORD METHOD GetMetaData(WORD ID) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Comms Tester
//

interface ICommsTester : public IUnknown
{
	// https://

	AfxDeclareIID(13, 50);

	virtual BOOL  METHOD GetSerialConfig(CSerialConfig &Config)                    = 0;
	virtual BOOL  METHOD GetDriverConfig(PBYTE &pData)                             = 0;
	virtual BOOL  METHOD GetDeviceConfig(PBYTE &pData)                             = 0;
	virtual BOOL  METHOD ExecDriverCtrl(ICommsDriver *pDriver)                     = 0;
	virtual BOOL  METHOD ExecDeviceCtrl(ICommsDriver *pDriver)                     = 0;
	virtual BOOL  METHOD ExecTest(CCODE &Code, ICommsMaster *pDriver, UINT uIndex) = 0;
	virtual CCODE METHOD SlaveRead(AREF Addr, PDWORD pData, UINT uCount)          = 0;
	virtual CCODE METHOD SlaveRead(AREF Addr, PDWORD pData, IMetaData **ppm)      = 0;
	virtual CCODE METHOD SlaveWrite(AREF Addr, PDWORD pData, UINT uCount)          = 0;
};

// End of File

#endif
