
#include "Intern.hpp"

#include "Printf.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/JASgE

//////////////////////////////////////////////////////////////////////////
//
// Printf Formatted String
//

// Constructors

CPrintf::CPrintf(PCTXT pFormat, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	AfxAssertStringPtr(pFormat);

	UINT uResult = 0;

	PTXT pResult = IntPrintf(uResult, pFormat, pArgs);

	Alloc(uResult);
	
	strcpy(m_pText, pResult);
	
	delete pResult;

	va_end(pArgs);
	}

// End of File
