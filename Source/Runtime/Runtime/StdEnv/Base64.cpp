
#include "Intern.hpp"

#include "Base64.hpp"

////////////////////////////////////////////////////////////////////////////////
//	
// GcCore Class Library
//
// Copyright (c) 2018 Granby Consulting LLC
//
// Placed in the Public Domain.
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////////////
//	
// Employed Classes
//

#include "String.hpp"

#include "Array.hpp"

////////////////////////////////////////////////////////////////////////////////
//	
// Base64 Encoder
//

CString CBase64::ToBase64(PCBYTE p, size_t n, int enc)
{
	CString r;

	r.reserve(GetEncodeSize(n, enc));

	PCSTR x = GetList(enc);

	UINT  c = 0;

	for( size_t q = 0; (q = Min(n, size_t(3))); n -= q ) {

		if( q == 1 ) {

			BYTE const b0 = *p++;

			r += x[((b0 & 0xFC) >> 2)];

			r += x[((b0 & 0x03) << 4)];

			c += 2;

			if( !(enc == encUrl) ) {

				r += "==";

				c += 2;
			}
		}

		if( q == 2 ) {

			BYTE const b0 = *p++;

			BYTE const b1 = *p++;

			r += x[((b0 & 0xFC) >> 2)];

			r += x[((b0 & 0x03) << 4) | ((b1 & 0xF0) >> 4)];

			r += x[((b1 & 0x0F) << 2)];

			c += 3;

			if( !(enc == encUrl) ) {

				r += '=';

				c += 1;
			}
		}

		if( q == 3 ) {

			BYTE const b0 = *p++;

			BYTE const b1 = *p++;

			BYTE const b2 = *p++;

			r += x[((b0 & 0xFC) >> 2)];

			r += x[((b0 & 0x03) << 4) | ((b1 & 0xF0) >> 4)];

			r += x[((b1 & 0x0F) << 2) | ((b2 & 0xC0) >> 6)];

			r += x[((b2 & 0x3F) << 0)];

			c += 4;
		}

		if( enc == encBasic ) {

			if( c >= 76 ) {

				r += "\r\n";

				c  = 0;
			}
		}
	}

	if( enc == encBasic ) {

		if( c ) {

			r += "\r\n";
		}
	}

	return r;
}

CString CBase64::ToBase64(CByteArray const &d, int enc)
{
	PCBYTE const p = PCBYTE(d.data());

	size_t const n = d.size();

	return ToBase64(p, n, enc);
}

CString CBase64::ToBase64(CString const &s, int enc)
{
	PCBYTE const p = PCBYTE(s.data());

	size_t const n = s.size();

	return ToBase64(p, n, enc);
}

bool CBase64::ToBytes(CByteArray &d, CString const &s)
{
	return Decode(d, s, true);
}

bool CBase64::ToAnsi(CString &d, CString const &s)
{
	return Decode(d, s, false);
}

CByteArray CBase64::ToBytes(CString const &s)
{
	CByteArray d;

	Decode(d, s, true);

	return d;
}

CString CBase64::ToAnsi(CString const &s)
{
	CString r;

	Decode(r, s, false);

	return r;
}

// Decode Helper

template<typename dtype> bool CBase64::Decode(dtype &d, CString const &s, bool z)
{
	d.clear();

	d.reserve(GetDecodeSize(s.size()));

	char const *p = s;

	while( *p ) {

		BYTE b[4];

		UINT q = 0;

		while( *p && q < 4 ) {

			int const x = Decode(*p);

			if( x >= 0 ) {

				b[q++] = BYTE(x);
			}

			p++;
		}

		if( q >= 2 ) {

			BYTE const d0 = ((b[0] & 0x3F) << 2) | ((b[1] & 0x30) >> 4);

			if( d0 || z ) {

				d += (typename dtype::value_type)(d0);
			}
		}

		if( q >= 3 ) {

			BYTE const d1 = ((b[1] & 0x0F) << 4) | ((b[2] & 0x3C) >> 2);

			if( d1 || z ) {

				d += (typename dtype::value_type)(d1);
			}
		}

		if( q >= 4 ) {

			BYTE const d2 = ((b[2] & 0x03) << 6) | ((b[3] & 0x3F) >> 0);

			if( d2 || z ) {

				d += (typename dtype::value_type)(d2);
			}
		}
	}

	return true;
}

// Size Estimation

STRONG_INLINE size_t CBase64::GetEncodeSize(size_t s, int enc)
{
	size_t n = 4 * ((s + 2) / 3);

	if( enc == encBasic ) {

		n += 2 * ((n + 75) / 76);
	}

	return n;
}

STRONG_INLINE size_t CBase64::GetDecodeSize(size_t s)
{
	return 3 * ((s + 3) / 4);
}

// Encoding List

STRONG_INLINE char const * CBase64::GetList(int enc)
{
	switch( enc ) {

		case encBasic:
		case encNoBreak:

			return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

		case encUrl:

			return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
	}

	return nullptr;
}

// Character Decode

STRONG_INLINE int CBase64::Decode(char c)
{
	if( c >= 'A' && c <= 'Z' ) {

		return c - 'A' + 0;
	}

	if( c >= 'a' && c <= 'z' ) {

		return c - 'a' + 26;
	}

	if( c >= '0' && c <= '9' ) {

		return c - '0' + 52;
	}

	if( c == '+' || c == '-' ) {

		return 62;
	}

	if( c == '/' || c == '_' ) {

		return 63;
	}

	return -1;
}

// End of File
