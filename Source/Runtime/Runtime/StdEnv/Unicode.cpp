
#include "Intern.hpp"

#include "Unicode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/ywDUE

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Guid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Dynamic String
//

// Null Data

DWORD	      CUnicode::m_Null[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

CUnicodeData & CUnicode::m_Empty   = (CUnicodeData &) m_Null[0];

// Constructors

CUnicode::CUnicode(PCUTF pText, UINT uCount)
{
	AfxAssertStringPtr(pText, uCount);
	
	if( *pText && uCount ) {
		
		Alloc(uCount);
		
		wcsncpy(m_pText, pText, uCount);
		
		m_pText[uCount] = 0;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CUnicode::CUnicode(WCHAR cData, UINT uCount)
{
	AfxAssert(cData);
	
	if( uCount ) {
		
		Alloc(uCount);
		
		wmemset(m_pText, cData, uCount);
		
		m_pText[uCount] = 0;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CUnicode::CUnicode(GUID const &Guid)
{
	m_pText = m_Empty.m_cData;

	InitFrom(Guid);
	}

// Explicit Constructors

CUnicode::CUnicode(PCTXT pText)
{
	AfxAssertStringPtr(pText);

	if( *pText ) {

		Alloc(strlen(pText));

		for( UINT n = 0; (m_pText[n] = pText[n]); n++ );

		return;
		}

	m_pText = m_Empty.m_cData;
	}

// Assignment Operators

CUnicode const & CUnicode::operator = (CUnicode const &That)
{
	if( this != &That ) {
		
		That.AssertValid();
		
		Release();
		
		if( !That.IsEmpty() ) {
			
			m_pText = That.m_pText;
			
			GetStringData(this)->m_nRefs++;
			}
		}
	
	return ThisObject;
	}

CUnicode const & CUnicode::operator = (GUID const &Guid)
{
	Release();

	InitFrom(Guid);

	return ThisObject;
	}

CUnicode const & CUnicode::operator = (PCUTF pText)
{
	AfxAssertStringPtr(pText);

	if( *pText ) {
		
		if( pText >= m_pText && pText <= m_pText + GetLength() ) {
			
			CUnicode Work = pText;
			
			Release();
			
			Alloc(Work.GetLength());
			
			wcscpy(m_pText, Work.m_pText);
			}
		else {
			Release();
			
			Alloc(wcslen(pText));
			
			wcscpy(m_pText, pText);
			}
		}
	else
		Release();
	
	return ThisObject;
	}

CUnicode const & CUnicode::operator = (PCTXT pText)
{
	AfxAssertStringPtr(pText);

	if( *pText ) {

		Release();

		Alloc(strlen(pText));

		for( UINT n = 0; (m_pText[n] = pText[n]); n++ );
		}
	else
		Release();

	return ThisObject;
	}

// Quick Init

void CUnicode::QuickInit(PCUTF pText, UINT uSize)
{
	CopyOnWrite();

	CUnicodeData *pData = GetStringData(this);

	UINT         uAlloc = FindAlloc(uSize);
	
	if( pData->m_uAlloc >= uAlloc ) {
		
		pData->m_uLen = uSize;
		}
	else {
		delete pData;
		
		Alloc(uSize, uAlloc);
		}
		
	wmemcpy(m_pText, pText, uSize);

	m_pText[uSize] = 0;
	}

// Subwcsing Extraction

CUnicode CUnicode::Left(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);
	
	return CUnicode(m_pText, uCount);
	}

CUnicode CUnicode::Right(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);
	
	PCUTF pSrc = m_pText + uLen - uCount;
	
	return CUnicode(pSrc, uCount);
	}

CUnicode CUnicode::Mid(UINT uPos, UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uPos, uLen);
	
	MakeMin(uCount, uLen - uPos);
	
	PCUTF pSrc = m_pText + uPos;
	
	return CUnicode(pSrc, uCount);
	}

CUnicode CUnicode::Mid(UINT uPos) const
{
	UINT uLen = GetLength();
	
	MakeMin(uPos, uLen);
	
	PCUTF pSrc = m_pText + uPos;
	
	return CUnicode(pSrc, uLen - uPos);
	}

// Buffer Managment

void CUnicode::Empty(void)
{
	Release();
	}

void CUnicode::Expand(UINT uAlloc)
{
	CopyOnWrite();
	
	CUnicodeData *pData = GetStringData(this);
	
	if( pData->m_uAlloc < uAlloc ) {
		
		PCUTF pText = m_pText;
		
		Alloc(pData->m_uLen, uAlloc);
		
		wcscpy(m_pText, pText);
		
		delete pData;
		}
	}

void CUnicode::Compress(void)
{
	CopyOnWrite();
	
	CUnicodeData *pData = GetStringData(this);
	
	if( pData->m_uAlloc > FindAlloc(pData->m_uLen) ) {
		
		PCUTF pText = m_pText;
		
		Alloc(pData->m_uLen);
		
		wcscpy(m_pText, pText);
		
		delete pData;
		}
	}

void CUnicode::CopyOnWrite(void)
{
	CUnicodeData *pData = GetStringData(this);
	
	if( pData->m_nRefs ) {
		
		if( pData->m_nRefs > 1 ) {
			
			PCUTF pText = m_pText;
			
			pData->m_nRefs--;
			
			Alloc(pData->m_uLen);
			
			wcscpy(m_pText, pText);
			}
		}
	else {
		Alloc(0);
		
		GetStringData(this)->m_uLen = 0;
		
		*m_pText = 0;
		}
	}

void CUnicode::FixLength(UINT uLen)
{
	CUnicodeData *pData   = GetStringData(this);

	pData->m_cData[uLen] = 0;

	pData->m_uLen        = uLen;
	}

void CUnicode::FixLength(void)
{
	GetStringData(this)->m_uLen = wcslen(m_pText);
	}

// Concatenation In-Place

CUnicode const & CUnicode::operator += (CUnicode const &That)
{
	That.AssertValid();
	
	if( !That.IsEmpty() ) {
		
		UINT uLen = GetStringData(this)->m_uLen;
		
		CUnicode Copy = That;
		
		GrowString(uLen + Copy.GetLength());
		
		wcscpy(m_pText + uLen, Copy.m_pText);
		}
	
	return ThisObject;
	}

CUnicode const & CUnicode::operator += (PCUTF pText)
{
	AfxAssertStringPtr(pText);
	
	if( *pText ) {
		
		UINT uLen = GetStringData(this)->m_uLen;
		
		if( pText >= m_pText && pText <= m_pText + uLen ) {
			
			CUnicode Copy = pText;
			
			GrowString(uLen + Copy.GetLength());
		
			wcscpy(m_pText + uLen, pText);
			}
		else {
			GrowString(uLen + wcslen(pText));
		
			wcscpy(m_pText + uLen, pText);
			}
		}
	
	return ThisObject;
	}

CUnicode const & CUnicode::operator += (PCTXT pText)
{
	if( *pText ) {
		
		UINT uLen = GetStringData(this)->m_uLen;
		
		GrowString(uLen + strlen(pText));
		
		wstrcat(m_pText, pText);
		}
	
	return *this;
	}

CUnicode const & CUnicode::operator += (WCHAR cData)
{
	AfxAssert(cData);
	
	UINT uLen = GetStringData(this)->m_uLen;
	
	GrowString(uLen + 1);
	
	m_pText[uLen + 0] = cData;
	
	m_pText[uLen + 1] = 0;
	
	return ThisObject;
	}

// Concatenation via Friends

CUnicode operator + (CUnicode const &A, CUnicode const &B)
{
	A.AssertValid();
	
	B.AssertValid();
	
	return CUnicode(A.m_pText, A.GetLength(), B.m_pText, B.GetLength());
	}

CUnicode operator + (CUnicode const &A, PCUTF pText)
{
	A.AssertValid();
	
	AfxAssertStringPtr(pText);
	
	return CUnicode(A.m_pText, A.GetLength(), pText, wcslen(pText));
	}

CUnicode operator + (CUnicode const &A, WCHAR cData)
{
	A.AssertValid();
	
	AfxAssert(cData);
	
	return CUnicode(A.m_pText, A.GetLength(), &cData, 1);
	}

CUnicode operator + (PCUTF pText, CUnicode const &B)
{
	AfxAssertStringPtr(pText);
	
	B.AssertValid();
	
	return CUnicode(pText, wcslen(pText), B.m_pText, B.GetLength());
	}

CUnicode operator + (WCHAR cData, CUnicode const &B)
{
	AfxAssert(cData);
	
	B.AssertValid();
	
	return CUnicode(&cData, 1, B.m_pText, B.GetLength());
	}

// Concatenation Functions

BOOL CUnicode::Append(CUnicode const &That)
{
	if( !That.IsEmpty() ) {
		
		UINT    uLen = GetStringData(this)->m_uLen;
		
		CUnicode Copy = That;
		
		GrowString(uLen + Copy.GetLength());
		
		wcscpy(m_pText + uLen, Copy.m_pText);

		return TRUE;
		}

	return FALSE;
	}

BOOL CUnicode::Append(PCUTF pText)
{
	if( *pText ) {
		
		UINT uLen = GetStringData(this)->m_uLen;
		
		if( pText >= m_pText && pText <= m_pText + uLen ) {
			
			CUnicode Copy(pText);
			
			GrowString(uLen + Copy.GetLength());
			
			wcscpy(m_pText + uLen, Copy.m_pText);
			}
		else {
			GrowString(uLen + wcslen(pText));
			
			wcscpy(m_pText + uLen, pText);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CUnicode::Append(PCUTF pText, UINT uText)
{
	if( *pText && uText ) {
		
		UINT uLen = GetStringData(this)->m_uLen;
		
		if( pText >= m_pText && pText <= m_pText + uLen ) {
			
			CUnicode Copy(pText, uText);
			
			GrowString(uLen + Copy.GetLength());
			
			wcscpy(m_pText + uLen, Copy.m_pText);
			}
		else {
			MakeMin(uText, wcslen(pText));

			GrowString(uLen + uText);
			
			wmemcpy(m_pText + uLen, pText, uText);

			m_pText[uLen + uText] = 0;
			}

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CUnicode::Append(char cData)
{
	if( cData ) {

		UINT uLen = GetStringData(this)->m_uLen;
	
		GrowString(uLen + 1);
	
		m_pText[uLen + 0] = cData;
	
		m_pText[uLen + 1] = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUnicode::AppendPrintf(PCUTF pFormat, ...)
{
	va_list pArgs;

	va_start(pArgs, pFormat);

	AppendVPrintf(pFormat, pArgs);

	va_end(pArgs);

	return TRUE;
	}

BOOL CUnicode::AppendVPrintf(PCUTF pFormat, va_list pArgs)
{
	AfxAssert(FALSE);

	return FALSE;
	}

// Write Data Access

void CUnicode::SetAt(UINT uIndex, WCHAR cData)
{
	AfxAssert(uIndex < GetLength());
	
	AfxAssert(cData);
	
	CopyOnWrite();
	
	m_pText[uIndex] = cData;
	}

// Insert and Remove

void CUnicode::Insert(UINT uIndex, PCUTF pText)
{
	UINT uLength = GetLength();

	UINT uCount  = wcslen(pText);

	MakeMin(uIndex, uLength);

	GrowString(uLength + uCount);

	wmemmove( m_pText + uIndex + uCount,
		  m_pText + uIndex,
		  uLength - uIndex + 1
		  );

	wmemcpy(  m_pText + uIndex,
		  pText,
		  uCount
		  );
	}

void CUnicode::Insert(UINT uIndex, CUnicode const &That)
{
	UINT uLength = GetLength();

	UINT uCount  = That.GetLength();

	MakeMin(uIndex, uLength);

	GrowString(uLength + uCount);

	wmemmove( m_pText + uIndex + uCount,
		  m_pText + uIndex,
		  uLength - uIndex + 1
		  );

	wmemcpy(  m_pText + uIndex,
		  That.m_pText,
		  uCount
		  );
	}

void CUnicode::Insert(UINT uIndex, WCHAR cData)
{
	UINT uLength = GetLength();

	MakeMin(uIndex, uLength);

	GrowString(uLength + 1);

	wmemmove( m_pText + uIndex + 1,
		  m_pText + uIndex,
		  uLength - uIndex + 1
		  );

	m_pText[uIndex] = cData;
	}

void CUnicode::Delete(UINT uIndex, UINT uCount)
{
	UINT uLength = GetLength();

	if( uIndex < uLength ) {
		
		MakeMin(uCount, uLength - uIndex);

		CopyOnWrite();

		wmemcpy( m_pText + uIndex,
			 m_pText + uIndex + uCount,
			 uLength - uIndex - uCount + 1
			 );

		GetStringData(this)->m_uLen -= uCount;
		}
	}

// Whitespace Trimming

void CUnicode::TrimLeft(void)
{
	UINT c = GetLength();

	UINT n = 0;

	while( m_pText[n] ) {

		if( !wisspace(m_pText[n]) ) {

			break;
			}

		n++;
		}

	if( n ) {

		if( !m_pText[n] ) {

			Empty();

			return;
			}

		CopyOnWrite();

		wmemcpy(m_pText, m_pText + n, c - n + 1);

		GetStringData(this)->m_uLen -= n;
		}
	}

void CUnicode::TrimRight(void)
{
	UINT c = GetLength();

	UINT n = c;

	while( n ) {

		if( !wisspace(m_pText[n - 1]) ) {

			break;
			}

		n--;
		}

	if( n < c ) {

		if( !n ) {

			Empty();

			return;
			}

		CopyOnWrite();

		m_pText[n] = 0;

		GetStringData(this)->m_uLen = n;
		}
	}

void CUnicode::TrimBoth(void)
{
	TrimRight();

	TrimLeft();
	}

void CUnicode::StripAll(void)
{
	UINT s = 0;

	UINT e = 0;

	for(;;) { 
	
		while( m_pText[s] ) {

			if( wisspace(m_pText[s]) ) {

				break;
				}

			s++;
			}

		e = s;	
		
		while( m_pText[e] ) {

			if( !wisspace(m_pText[e]) ) {

				break;
				}

			e++;
			}
	
		UINT cb = e - s;
		
		if( cb ) {

			if( cb == GetLength() ) {
			
				Empty();

				return;
				}
			
			CopyOnWrite();

			if( e == GetLength() ) {

				m_pText[s] = 0;

				GetStringData(this)->m_uLen = s;
				
				return;
				}

			wmemcpy( m_pText + s,
				 m_pText + e,
				 GetLength() - s - cb + 1
				 );

			GetStringData(this)->m_uLen -= cb;

			e = s;
			}
		else
			break;
		}
	}

// Case Switching

void CUnicode::MakeUpper(void)
{
	CopyOnWrite();
	
//	wcsupr(m_pText);
	}

void CUnicode::MakeLower(void)
{
	CopyOnWrite();
	
//	wcslwr(m_pText);
	}

// Replacement

UINT CUnicode::Replace(WCHAR cFind, WCHAR cNew)
{
	UINT uCount = 0;

	for( UINT n = 0; m_pText[n]; n++ ) {

		if( m_pText[n] == cFind ) {

			if( !uCount ) {

				CopyOnWrite();
				}

			m_pText[n] = cNew;

			uCount++;
			}
		}

	return uCount;
	}

UINT CUnicode::Replace(PCUTF pFind, PCUTF pNew)
{
	UINT uCount = 0;

	UINT uStart = 0;

	for(;;) {

		UINT uPos = Find(pFind, uStart);

		if( uPos < NOTHING ) {
			
			Delete(uPos, wcslen(pFind));

			Insert(uPos, pNew);

			uStart = uPos + wcslen(pNew);

			uCount ++;
			}
		else
			break;
		}

	return uCount;
	}

// Removal

void CUnicode::Remove(WCHAR cFind)
{
	for(;;) {

		UINT uPos = Find(cFind);

		if( uPos == NOTHING ) {

			return;
			}

		Delete(uPos, 1);
		}
	}

CUnicode CUnicode::Without(WCHAR cFind)
{
	CUnicode Work = ThisObject;

	Work.Remove(cFind);

	return Work;
	}

// Parsing

UINT CUnicode::Tokenize(CUnicodeArray &Array, WCHAR cFind) const
{
	return Tokenize(Array, NOTHING, cFind);
	}

UINT CUnicode::Tokenize(CUnicodeArray &Array, WCHAR cFind, WCHAR cQuote) const
{
	return Tokenize(Array, NOTHING, cFind, cQuote);
	}

UINT CUnicode::Tokenize(CUnicodeArray &Array, WCHAR cFind, WCHAR cOpen, WCHAR cClose) const
{
	return Tokenize(Array, NOTHING, cFind, cOpen, cClose);
	}

UINT CUnicode::Tokenize(CUnicodeArray &Array, PCUTF pFind) const
{
	return Tokenize(Array, NOTHING, pFind);
	}

UINT CUnicode::Tokenize(CUnicodeArray &Array, UINT uLimit, WCHAR cFind) const
{
	UINT    uCount = 0;

	CUnicode Text   = ThisObject;

	while( !Text.IsEmpty() ) {

		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(cFind));

		uCount++;
		}

	return uCount;
	}

UINT CUnicode::Tokenize(CUnicodeArray &Array, UINT uLimit, WCHAR cFind, WCHAR cQuote) const
{
	UINT    uCount = 0;

	CUnicode Text   = ThisObject;

	while( !Text.IsEmpty() ) {

		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(cFind, cQuote));

		uCount++;
		}

	return uCount;
	}

UINT CUnicode::Tokenize(CUnicodeArray &Array, UINT uLimit, WCHAR cFind, WCHAR cOpen, WCHAR cClose) const
{
	UINT    uCount = 0;

	CUnicode Text   = ThisObject;

	while( !Text.IsEmpty() ) {

		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(cFind, cOpen, cClose));

		uCount++;
		}

	return uCount;
	}

UINT CUnicode::Tokenize(CUnicodeArray &Array, UINT uLimit, PCUTF pFind) const
{
	UINT    uCount = 0;

	CUnicode Text   = ThisObject;

	while( !Text.IsEmpty() ) {
		
		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(pFind));

		uCount++;
		}

	return uCount;
	}

CUnicode CUnicode::TokenLeft(WCHAR cFind) const
{
	UINT uPos = Find(cFind);

	return Left(uPos);
	}

CUnicode CUnicode::TokenFrom(WCHAR cFind) const
{
	UINT uPos = Find(cFind);

	if( uPos < NOTHING ) {

		return Mid(uPos+1);
		}

	return L"";
	}

CUnicode CUnicode::TokenLast(WCHAR cFind) const
{
	UINT uPos = FindRev(cFind);

	return Mid(uPos+1);
	}

CUnicode CUnicode::StripToken(WCHAR cFind)
{
	if( !IsEmpty() ) {

		UINT    uPos = Find(cFind);

		CUnicode Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return L"";
	}

CUnicode CUnicode::StripToken(WCHAR cFind, WCHAR cQuote)
{
	if( !IsEmpty() ) {

		UINT    uPos = Find(cFind, cQuote);

		CUnicode Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return L"";
	}

CUnicode CUnicode::StripToken(WCHAR cFind, WCHAR cOpen, WCHAR cClose)
{
	if( !IsEmpty() ) {

		UINT    uPos = Find(cFind, cOpen, cClose);

		CUnicode Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return L"";
	}

CUnicode CUnicode::StripToken(PCUTF pFind)
{
	if( !IsEmpty() ) {

		UINT    uPos = FindOne(pFind);

		CUnicode Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + wcslen(pFind));

		return Text;
		}

	return L"";
	}

// Building

void CUnicode::Build(CUnicodeArray &Array, WCHAR cSep)
{
	Empty();

	UINT c = Array.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( n ) {

			operator += (cSep);
			}

		operator += (Array[n]);
		}
	}

void CUnicode::Build(CUnicodeArray &Array, PCUTF pSep)
{
	Empty();

	UINT c = Array.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( n ) {

			operator += (pSep);
			}

		operator += (Array[n]);
		}
	}

// Display Ordering

void CUnicode::MakeDisplayOrder(void)
{
	}

// Printf Formatting

void CUnicode::Printf(PCUTF pFormat, ...)
{
	AfxAssert(FALSE);
	}

void CUnicode::VPrintf(PCUTF pFormat, va_list pArgs)
{
	AfxAssert(FALSE);
	}

// Diagnostics

#ifdef _DEBUG

void CUnicode::AssertValid(void) const
{
	AfxAssertReadPtr(this, sizeof(ThisObject));
	
	CUnicodeData *pData = GetStringData(this);
	
	if( pData->m_nRefs == 0 ) {
		
		AfxAssert(*m_pText == 0);
		
		AfxAssert(pData->m_uLen == 0);
		}
	else
		AfxAssert(pData->m_uAlloc > pData->m_uLen);
	
	AfxAssertStringPtr(m_pText);

	AfxAssert(m_Empty.m_nRefs == 0);
	}

#endif

// Protected Conwcsuctor

CUnicode::CUnicode(PCUTF p1, UINT u1, PCUTF p2, UINT u2)
{
	Alloc(u1 + u2);
	
	wcsncpy(m_pText, p1, u1);
	
	wcsncpy(m_pText + u1, p2, u2);
	
	m_pText[u1 + u2] = 0;
	}

// Initialisation

void CUnicode::InitFrom(GUID const &Guid)
{
	AfxAssert(FALSE);
	}

// Internal Helpers

PUTF CUnicode::IntPrintf(UINT &uSize, PCUTF pFormat, va_list pArgs)
{
	AfxAssert(FALSE);

	return NULL;
	}

// Implementation

BOOL CUnicode::GrowString(UINT uLen)
{
	CopyOnWrite();
	
	CUnicodeData *pData = GetStringData(this);
	
	if( FindAlloc(uLen) > pData->m_uAlloc ) {
		
		PCUTF pText = m_pText;
		
		Alloc(uLen);
		
		wcscpy(m_pText, pText);
		
		delete pData;

		return TRUE;
		}

	pData->m_uLen = uLen;

	return FALSE;
	}

// End of File
