
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Converting Functions
//

wchar_t * wstrdup(char const *p)
{
	int      n = 1 + strlen(p);

	wchar_t *d = (wchar_t *) malloc(sizeof(wchar_t) * n);

	for( int i = 0; (d[i] = p[i]); i++ );

	return d;
	}

wchar_t * wstrdpi(wchar_t const *p)
{
	int      n = wstrlen(p) + 1;

	wchar_t *c = PUTF(malloc(n * sizeof(WCHAR)));

	wstrcpy(c, p);

	while( n-- ) {

		BYTE hi = HIBYTE(c[n]);
		BYTE lo = LOBYTE(c[n]);

		c[n] = MAKEWORD(hi,lo);
		}

	return c;
	}

void wstrcat(wchar_t *d, char const *s)
{
	wchar_t *p = d + wstrlen(d);

	for( int i = 0; (p[i] = s[i]); i++ );
	}

// End of File
