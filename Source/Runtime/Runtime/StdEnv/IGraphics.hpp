
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IGraphics_HPP

#define INCLUDE_IGraphics_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 10 -- Crimson Graphics
//

// TODO -- We should add a font manager!

interface IRegion;
interface IGdiFont;
interface IGdi;
interface ITouchMap;

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

interface ITouchScreen;

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

// TODO -- Don't like this!

typedef char	      TCHAR;

typedef wchar_t	      WCHAR;

typedef WCHAR *	      PUTF;

typedef WCHAR const * PCUTF;

//////////////////////////////////////////////////////////////////////////
//
// Unicode Characters
//

#define spaceNormal  0x0020
#define spaceNoBreak 0x00A0
#define spaceFigure  0x2007
#define spaceHair    0x200A
#define spaceNarrow  0x202F
#define digitFixed   0xFF10
#define digitSimple  0x0030
#define letterFixed  0xFF21
#define letterSimple 0x0041
#define symbolFixed  0xFF01

//////////////////////////////////////////////////////////////////////////
//
// Unicode Explicit Codes
//

#define uniLRE 0x202A
#define uniRLE 0x202B
#define uniPDF 0x202C
#define uniLRO 0x202D
#define uniRLO 0x202E
#define uniLRM 0x200E
#define uniRLM 0x200F

//////////////////////////////////////////////////////////////////////////
//
// Color Layout
//

typedef	unsigned short	COLOR;

#define	COL_FORM_BITS	5

#define	COL_FORM_RED	(0*COL_FORM_BITS)

#define	COL_FORM_GREEN	(1*COL_FORM_BITS)

#define	COL_FORM_BLUE	(2*COL_FORM_BITS)

#define	COL_FORM_MASK	COLOR((1<<COL_FORM_BITS)-1)

#define	COL_MIN		COLOR(0)

#define	COL_MAX		COLOR(COL_FORM_MASK)

#define	COL_INVALID	COLOR(0xFFFF)

//////////////////////////////////////////////////////////////////////////
//
// Color Extraction
//

#define	GetRED(c)	BYTE(((c) >> COL_FORM_RED  ) & COL_FORM_MASK)

#define	GetGREEN(c)	BYTE(((c) >> COL_FORM_GREEN) & COL_FORM_MASK)

#define	GetBLUE(c)	BYTE(((c) >> COL_FORM_BLUE ) & COL_FORM_MASK)

//////////////////////////////////////////////////////////////////////////
//
// Color Combination
//

#define	GetRGB(r,g,b)	COLOR(						\
			((((r) & COL_FORM_MASK) << COL_FORM_RED  )	\
			|(((g) & COL_FORM_MASK) << COL_FORM_GREEN)	\
			|(((b) & COL_FORM_MASK) << COL_FORM_BLUE )	\
			))						\

//////////////////////////////////////////////////////////////////////////
//
// Shader Function
//

typedef BOOL (*PSHADER)(IGdi *pGdi, int p, int c);

//////////////////////////////////////////////////////////////////////////
//
// Raster Operations
//

enum
{
	ropBLACKNESS  = 0x0000,
	ropDSTINVERT  = 0x0001,
	ropNOTSRCAND  = 0x0002,
	ropNOTSRCCOPY = 0x0003,
	ropSRCAND     = 0x0004,
	ropSRCCOPY    = 0x0005,
	ropSRCINVERT  = 0x0006,
	ropSRCPAINT   = 0x0007,
	ropWHITENESS  = 0x0008,
	ropMask       = 0x000F,
	ropTrans      = 0x8000,
	ropInvert     = 0x4000,
	ropRLE        = 0x2000,
	ropDisable    = 0x1000,
	ropSrc24      = 0x0800,
	ropZoom4X     = 0x0400,
	ropZoom3X     = 0x0200,
	ropZoom2X     = 0x0100,
	ropBlend      = 0x0080,
	};

//////////////////////////////////////////////////////////////////////////
//
// Color Formats
//

enum
{
	colorRGB888	= 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// Background Modes
//

enum
{
	modeOpaque	= 0,
	modeTransparent = 1,
	};

//////////////////////////////////////////////////////////////////////////
//
// Brush Styles
//

enum
{
	brushNull	= 0,
	brushBack	= 1,
	brushFore	= 2,
	brushBlack	= 1,
	brushWhite	= 2,
	brushGray25	= 3,
	brushGray50	= 4,
	brushGray75	= 5,
	brushHatchF	= 6,
	brushHatchB	= 7,
	brushHatchH	= 8,
	brushHatchV	= 9,
	brushHatchX	= 10,
	brushHatchD	= 11,
	brushWave	= 12,
	brushHatchFB	= 13,
	brushHatchBB	= 14,
	brushBitmap	= 128,
	};

//////////////////////////////////////////////////////////////////////////
//
// Pen Styles
//

enum
{
	penNull		= 0,
	penBack		= 1,
	penFore		= 2,
	penBlack	= 1,
	penWhite	= 2,
	penGray		= 3,
	penDotted	= 4,
	penDashed	= 5,
	penDotDash	= 6,
	};

//////////////////////////////////////////////////////////////////////////
//
// Pen Endcaps
//

enum
{
	capsNone	= 0,
	capsStart	= 1,
	capsEnd		= 2,
	capsBoth	= 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Entity Types
//

enum
{
	etWhole = 0,
	etQuad1 = 1,
	etQuad2 = 2,
	etQuad3	= 3,
	etQuad4 = 4,
	etHalf1 = 5,
	etHalf2 = 6,
	etHalf3 = 7,
	etHalf4 = 8
	};

//////////////////////////////////////////////////////////////////////////
//
// Standard Fonts
//

enum
{
	fontDefault   = 0,
	fontSwiss0406 = 1,
	fontSwiss0708 = 2,
	fontSwiss0512 = 3,
	fontSwiss0712 = 4,
	fontSwiss1216 = 5,
	fontTimes0808 = 6,
	fontTimes0812 = 7,
	fontTimes1612 = 8,
	fontHei16     = 9,
	fontHei16Bold = 10,
	fontHei10     = 11,
	fontHei10Bold = 12,
	fontHei12     = 13,
	fontHei12Bold = 14,
	fontHei14     = 15,
	fontHei14Bold = 16,
	fontHei18     = 17,
	fontHei18Bold = 18,
	fontHei20     = 19,
	fontHei20Bold = 20,
	fontHei24     = 21,
	fontHei24Bold = 22,
	fontBig24     = 23,
	fontBig32     = 24,
	fontBig64     = 25,
	fontBig96     = 26,
	fontCount     = 27,
	};

//////////////////////////////////////////////////////////////////////////
//
// Logical Brush
//

struct CLogBrush
{
	UINT	m_Style;
	UINT	m_Trans;
	COLOR	m_Fore;
	COLOR	m_Back;
	PCVOID  m_Bits;
	int     m_bcx;
	int     m_bcy;
	DWORD	m_Rich;
	};

//////////////////////////////////////////////////////////////////////////
//
// Logical Pen
//

struct CLogPen
{
	UINT	m_Style;
	UINT	m_Trans;
	UINT	m_Width;
	UINT	m_Caps;
	COLOR	m_Fore;
	COLOR	m_Back;
	};

//////////////////////////////////////////////////////////////////////////
//
// Logical Font
//

struct CLogFont
{
	IGdiFont * m_pFont;
	UINT	   m_Trans;
	UINT	   m_Smooth;
	COLOR	   m_Fore;
	COLOR	   m_Back;
	};

//////////////////////////////////////////////////////////////////////////
//
// Point
//

struct P2
{
	int	x;
	int	y;
	};

//////////////////////////////////////////////////////////////////////////
//
// Size
//

struct S2
{
	int	cx;
	int	cy;
	};

//////////////////////////////////////////////////////////////////////////
//
// Rectangle
//

struct R2
{
	union {
		int	x1;
		int	left;
		};
	union {
		int	y1;
		int	top;
		};
	union {
		int	x2;
		int	right;
		};
	union {
		int	y2;
		int	bottom;
		};
	};

//////////////////////////////////////////////////////////////////////////
//
// Matrix
//

struct M3
{
	int e[3][3];
	};

//////////////////////////////////////////////////////////////////////////
//
// Passing Macros
//

#define PassPoint(p)	p.x, p.y

#define PassRect(r)	r.x1, r.y1, r.x2, r.y2

//////////////////////////////////////////////////////////////////////////
//
// Rectangle Functions
//

extern DLLAPI BOOL IsRectEmpty(R2 const &Rect);
extern DLLAPI void SetRectEmpty(R2 &Rect);
extern DLLAPI void CombineRects(R2 &Result, R2 const &Rect1, R2 const &Rect2);
extern DLLAPI void IntersectRects(R2 &Result, R2 const &Rect1, R2 const &Rect2);
extern DLLAPI BOOL RectsEqual(R2 const &Rect1, R2 const &Rect2);
extern DLLAPI BOOL RectsIntersect(R2 const &Rect1, R2 const &Rect2);
extern DLLAPI BOOL RectInRect(R2 const &Rect1, R2 const &Rect2);
extern DLLAPI BOOL PtInRect(R2 const &Rect, P2 const &Point);
extern DLLAPI void DeflateRect(R2 &Rect, int cx, int cy);
extern DLLAPI void InflateRect(R2 &Rect, int cx, int cy);
extern DLLAPI void DeflateRect(R2 &Rect, R2 const &Init, int cx, int cy);
extern DLLAPI void InflateRect(R2 &Rect, R2 const &Init, int cx, int cy);

//////////////////////////////////////////////////////////////////////////
//
// Region Interface
//

interface IRegion : public IUnknown
{
	// Identifier
	AfxDeclareIID(10, 1);

	// Attributes
	virtual BOOL IsEmpty(void)     = 0;
	virtual BOOL GetRect(R2 &Rect) = 0;

	// Operations
	virtual void Clear     (void)           = 0;
	virtual void AddRect   (R2 const &Rect) = 0;
	virtual void AddEllipse(R2 const &Rect) = 0;

	// Hit Testing
	virtual BOOL HitTest(P2 const &Point) = 0;
	virtual BOOL HitTest(R2 const &Rect ) = 0;

	// Filling
	virtual void Fill(IGdi *pGdi) = 0;

	// Diagnostics
	virtual void Show(IGdi *pGdi) = 0;
	virtual void Dump(void)       = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Font Interface
//

interface IGdiFont : public IUnknown
{
	// Identifier
	AfxDeclareIID(10, 2);

	// Attributes
	virtual BOOL IsProportional(void  ) = 0;
	virtual int  GetBaseLine   (void  ) = 0;
	virtual int  GetGlyphWidth (WORD c) = 0;
	virtual int  GetGlyphHeight(WORD c) = 0;

	// Operations
	virtual void InitBurst(IGdi *pGdi, CLogFont const &Font)   = 0;
	virtual void DrawGlyph(IGdi *pGdi, int &x, int &y, WORD c) = 0;
	virtual void BurstDone(IGdi *pGdi, CLogFont const &Font)   = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Gdi Interface
//

interface IGdi : public IUnknown
{
	// Identifier
	AfxDeclareIID(10, 3);

	// Initialization
	virtual void Create(int cx, int cy, PVOID pData) = 0;

	// Buffer Access
	virtual PVOID GetBuffer(void) = 0;

	// Buffer Rendering
	virtual UINT Render(PBYTE pData, UINT uBits) = 0;

	// Palette Access
	virtual DWORD GetPaletteEntry(UINT uEntry) = 0;

	// Serialization
	virtual void BufferClaim(void) = 0;
	virtual void BufferFree (void) = 0;

	// Color Format
	virtual UINT GetColorFormat(void) = 0;

	// Attributes
	virtual int GetCx(void) = 0;
	virtual int GetCy(void) = 0;

	// Transformation
	virtual void SetIdentity (void)			 = 0;
	virtual void SetTransform(M3 const &M)		 = 0;
	virtual void AddTransform(M3 const &M)		 = 0;
	virtual void AddRotation (int nDegrees)		 = 0;
	virtual void AddMovement (int dx, int dy)	 = 0;
	virtual void AddMirror   (BOOL x, BOOL y)	 = 0;
	virtual void MakeIdentity(M3 &M)		 = 0;
	virtual void MakeRotation(M3 &M, int nDegrees)	 = 0;
	virtual void MakeMovement(M3 &M, int dx, int dy) = 0;
	virtual void MakeMirror  (M3 &M, BOOL x, BOOL y) = 0;

	// Combined Attributes
	virtual void ResetAll 	 (void)		= 0;
	virtual void PushAll 	 (void)		= 0;
	virtual void PullAll 	 (void)		= 0;
	virtual void ResetBoth	 (void)		= 0;
	virtual void PushBoth	 (void)		= 0;
	virtual void PullBoth	 (void)		= 0;
	virtual void SetBackMode (UINT  Trans)	= 0;
	virtual void SetForeColor(COLOR Fore )	= 0;
	virtual void SetBackColor(COLOR Back )	= 0;

	// Text Attributes
	virtual void ResetFont    (void)		 = 0;
	virtual void PushFont     (void)		 = 0;
	virtual void PullFont     (void)		 = 0;
	virtual void SelectFont   (CLogFont const &Font) = 0;
	virtual void SelectFont   (UINT           uFont) = 0;
	virtual void SelectFont   (IGdiFont *     pFont) = 0;
	virtual void GetFont      (CLogFont       &Font) = 0;
	virtual void SetTextTrans (UINT  Trans)		 = 0;
	virtual void SetTextSmooth(UINT  Smooth)	 = 0;
	virtual void SetTextFore  (COLOR Fore )		 = 0;
	virtual void SetTextBack  (COLOR Back )		 = 0;

	// Brush Attributes
	virtual void ResetBrush   (void)			= 0;
	virtual void PushBrush    (void)			= 0;
	virtual void PullBrush    (void)			= 0;
	virtual void SelectBrush  (CLogBrush const &Brush)	= 0;
	virtual void GetBrush     (CLogBrush       &Brush)	= 0;
	virtual void SelectBrush  (UINT  Style)			= 0;
	virtual void SetBrushStyle(UINT  Style)			= 0;
	virtual void SetBrushTrans(UINT  Trans)			= 0;
	virtual void SetBrushFore (COLOR Fore)			= 0;
	virtual void SetBrushFore (DWORD Rich)			= 0;
	virtual void SetBrushBack (COLOR Back)			= 0;
	virtual void SetBrushBits (PCVOID pd, int cx, int cy)   = 0;

	// Pen Attributes
	virtual void ResetPen   (void)			= 0;
	virtual void PushPen    (void)			= 0;
	virtual void PullPen    (void)			= 0;
	virtual void SelectPen  (CLogPen const &Pen)	= 0;
	virtual void GetPen     (CLogPen       &Pen)	= 0;
	virtual void SelectPen  (UINT  Style)		= 0;
	virtual void SetPenStyle(UINT  Style)		= 0;
	virtual void SetPenTrans(UINT  Trans)		= 0;
	virtual void SetPenWidth(UINT  Width)		= 0;
	virtual void SetPenCaps (UINT  Caps)		= 0;
	virtual void SetPenFore (COLOR Fore)		= 0;
	virtual void SetPenBack (COLOR Back)		= 0;

	// Clearing
	virtual void ClearScreen(COLOR Color) = 0;

	// Pixel Access
	virtual void SetPixel(int x1, int y1, COLOR Color) = 0;

	// Text Metrics
	virtual int GetTextHeight(PCTXT p) = 0;
	virtual int GetTextWidth (PCTXT p) = 0;
	virtual int GetTextHeight(TCHAR c) = 0;
	virtual int GetTextWidth (TCHAR c) = 0;

	// Text Output
	virtual void TextOut(int x, int y, PCTXT p)         = 0;
	virtual void TextOut(int x, int y, PCTXT p, UINT n) = 0;

	// Text Metrics
	virtual int GetTextHeight(PCUTF p) = 0;
	virtual int GetTextWidth (PCUTF p) = 0;
	virtual int GetTextHeight(WCHAR c) = 0;
	virtual int GetTextWidth (WCHAR c) = 0;

	// Text Output
	virtual void TextOut(int x, int y, PCUTF p)         = 0;
	virtual void TextOut(int x, int y, PCUTF p, UINT n) = 0;

	// Text Helpers
	virtual void CharBlt(int x, int y, int cx, int cy, int s, PCBYTE p)		        = 0;
	virtual void CharBlt(int x, int y, int cx, int cy, int s, PCBYTE p, COLOR c1, COLOR c2) = 0;
	virtual void CharBlt(int x, int y, int cx, int cy, int s, PCBYTE p, COLOR c1)           = 0;

	// Bitmaps
	virtual void BitBlt(int x, int y, int cx, int cy, int s, PCBYTE p, UINT rop) = 0;

	// Line Drawing
	virtual void MoveTo  (int x1, int y1)			= 0;
	virtual void LineTo  (int x1, int y1)			= 0;
	virtual void DrawLine(int x1, int y1, int x2, int y2)	= 0;

	// Polygon Drawing
	virtual void DrawPolygon(P2 const *pList, UINT uCount, DWORD dwRound) = 0;
	virtual void FillPolygon(P2 const *pList, UINT uCount, DWORD dwRound) = 0;

	// Shaded Polygon
	virtual void ShadePolygon(P2 const *pList1, UINT uCount1,                                 DWORD dwRound, PSHADER pShade) = 0;
	virtual void ShadePolygon(P2 const *pList1, UINT uCount1, P2 const *pList2, UINT uCount2, DWORD dwRound, PSHADER pShade) = 0;

	// Smooth Polygon
	virtual void SmoothPolygon(P2 const *pList1, UINT uCount1, P2 const *pList2, UINT uCount2, DWORD dwRound, PSHADER pShade, int nAlpha) = 0;

	// Shaded Figures
	virtual void ShadeEllipse(int x1, int y1, int x2, int y2, UINT uType, PSHADER pShade) = 0;
	virtual void ShadeRect   (int x1, int y1, int x2, int y2,	      PSHADER pShade) = 0;
	virtual void ShadeRounded(int x1, int y1, int x2, int y2, int  r,     PSHADER pShade) = 0;
	virtual void ShadeWedge  (int x1, int y1, int x2, int y2, UINT uType, PSHADER pShade) = 0;

	// Rectangle Drawing
	virtual void DrawRect(int x1, int y1, int x2, int y2) = 0;
	virtual void FillRect(int x1, int y1, int x2, int y2) = 0;

	// Rounded Rectangles
	virtual void DrawRounded(int x1, int y1, int x2, int y2, int r) = 0;
	virtual void FillRounded(int x1, int y1, int x2, int y2, int r) = 0;

	// Ellipses
	virtual void DrawEllipse(int x1, int y1, int x2, int y2, UINT uType) = 0;
	virtual void FillEllipse(int x1, int y1, int x2, int y2, UINT uType) = 0;

	// Wedges
	virtual void DrawWedge(int x1, int y1, int x2, int y2, UINT uType) = 0;
	virtual void FillWedge(int x1, int y1, int x2, int y2, UINT uType) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Touch Map Interface
//

interface ITouchMap : public IUnknown
{
	// Identifier
	AfxDeclareIID(10, 4);

	// Initialization
	virtual void Create(ITouchScreen *pTouch)				     = 0;
	virtual void Create(IGdi *pGdi)						     = 0;
	virtual void Create(int xSize, int ySize, int xCell, int yCell, PBYTE pData) = 0;

	// Buffer Access
	virtual PBYTE GetBuffer(void) = 0;

	// Offset Control
	virtual void SetOffset(int xOffset, int yOffset) = 0;

	// Color Selection
	virtual void SetColor(BOOL fSet) = 0;
	virtual void ClearMap(void)      = 0;
	
	// Drawing Operations
	virtual	void SetCell(int x, int y)				     = 0;
	virtual	void FillRect(int x1, int y1, int x2, int y2)		     = 0;
	virtual	void FillEllipse(int x1, int y1, int x2, int y2, UINT uType) = 0;
	virtual	void FillWedge(int x1, int y1, int x2, int y2, UINT uType)   = 0;

	// Diagnostics
	virtual void Show(IGdi *pGdi) = 0;
	};

// End of File

#endif
