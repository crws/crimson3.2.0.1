
#include "Intern.hpp"

#include "MacAddr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// LINK

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "String.hpp"

#include "Printf.hpp"

////////////////////////////////////////////////////////////////////////
//
// MAC Address Class
//

// Static Data

MACADDR const CMacAddr::m_Broad = { { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF } };

MACADDR const CMacAddr::m_Empty = { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };

MACADDR const CMacAddr::m_Multi = { { 0x01, 0x00, 0x5E, 0x00, 0x00, 0x00 } };

// Assignment

CMacAddr const & CMacAddr::operator = (PCTXT pText)
{
	CString Text(pText);

	if( Text.Count(':') == 5 ) {

		for( UINT n = 0; n < 6; n++ ) {

			m_Addr[n] = BYTE(strtoul(Text.StripToken(':'), NULL, 16));
			}

		return ThisObject;
		}

	if( Text.Count('-') == 5 ) {

		for( UINT n = 0; n < 6; n++ ) {

			m_Addr[n] = BYTE(strtoul(Text.StripToken('-'), NULL, 16));
		}

		return ThisObject;
	}

	if( Text.GetLength() == sizeof(MACADDR) * 2 ) {

		for( UINT n = 0; n < 6; n++ ) {

			m_Addr[n] = BYTE(strtoul(Text.Mid(n * 2, 2), NULL, 16));
			}

		return ThisObject;
		}

	memcpy(this, &m_Empty, sizeof(MACADDR));

	return ThisObject;
	}

// Conversion

CString CMacAddr::GetAsText(void) const
{
	return CPrintf( "%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X",
			m_Addr[0], m_Addr[1], m_Addr[2],
			m_Addr[3], m_Addr[4], m_Addr[5]
			);
	}

// Attributes

BOOL CMacAddr::IsBroadcast(void) const
{
	return !memcmp(this, &m_Broad, sizeof(MACADDR));
	}

BOOL CMacAddr::IsMulticast(void) const
{
	return IsGroup() && !IsBroadcast();
	}

BOOL CMacAddr::IsGroup(void) const
{
	return (m_Addr[0] & 1) ? TRUE : FALSE;
	}

BOOL CMacAddr::IsEmpty(void) const
{
	return !memcmp(this, &m_Empty, sizeof(MACADDR));
	}

// Operations

void CMacAddr::MakeBroadcast(void)
{
	memcpy(this, &m_Broad, sizeof(MACADDR));
	}

void CMacAddr::MakeMulticast(void)
{
	memcpy(this, &m_Multi, sizeof(MACADDR));
	}

BOOL CMacAddr::MakeMulticast(IPREF IP)
{
	memcpy(this, &m_Multi, sizeof(MACADDR));

	if( IP.IsMulticast() ) {

		m_Addr[3] = (m_Multi.m_Addr[3] & 0x80) | (IP.m_b2 & 0x7F);
		
		m_Addr[4] = IP.m_b3;
		
		m_Addr[5] = IP.m_b4;

		return TRUE;
		}

	return FALSE;
	}

void CMacAddr::MakeEmpty(void)
{
	memcpy(this, &m_Empty, sizeof(MACADDR));
	}

// End of File
