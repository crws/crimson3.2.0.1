
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IEtherNetIp_HPP

#define INCLUDE_IEtherNetIp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 23 -- EtherNetIP
//

interface IEtherNetIp;

//////////////////////////////////////////////////////////////////////////
//
// CIP Address
//

struct CIPADDR
{
	IPADDR	m_ip;
	WORD	m_wPort;
	WORD	m_wSlot;
	PCTXT   m_pPath;
	BYTE	m_bService;
	WORD	m_wClass;
	WORD	m_wInst;
	WORD	m_wAttr;
	WORD	m_wMember;
	PBYTE   m_bTagData;
	UINT    m_uTagSize;
	};

//////////////////////////////////////////////////////////////////////////
//
// CIP Address Constants
//

#define EIP_EMPTY	0xFFFF

//////////////////////////////////////////////////////////////////////////
//
// Producer Interface
//

interface IProducer : public IUnknown 
{
	AfxDeclareIID(23, 1);

	virtual DWORD METHOD GetIdent(void)			= 0;
	virtual UINT  METHOD GetSize(void)			= 0;
	virtual BOOL  METHOD IsActive(void)			= 0;
	virtual UINT  METHOD Send(PBYTE pData, UINT uSize)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Consumer Interface
//

interface IConsumer : public IUnknown
{
	AfxDeclareIID(23, 2);

	virtual DWORD METHOD GetIdent(void)			= 0;
	virtual UINT  METHOD GetSize(void)			= 0;
	virtual BOOL  METHOD IsActive(void)			= 0;
	virtual BOOL  METHOD HasNewData(void)			= 0;
	virtual void  METHOD ClearNewData(void)			= 0;
	virtual UINT  METHOD Recv(PBYTE pData, UINT uSize)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// IImplicit Messagin Interface
//

interface IImplicit : public IUnknown
{
	AfxDeclareIID(23, 3);

	virtual BOOL METHOD GetFirstConsumer(IConsumer * &pConsumer)	= 0;
	virtual BOOL METHOD GetNextConsumer (IConsumer * &pConsumer)	= 0;
	virtual BOOL METHOD GetFirstProducer(IProducer * &pProducer)	= 0;
	virtual BOOL METHOD GetNextProducer (IProducer * &pProducer)	= 0;
	virtual UINT METHOD Send(DWORD dwInst, PBYTE pData, UINT uSize)	= 0;
	virtual UINT METHOD Recv(DWORD dwInst, PBYTE pData, UINT uSize)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// IExplicit Messaging Interface
//

interface IExplicit : public IUnknown
{
	AfxDeclareIID(23, 4);

	virtual void METHOD SetTimeout(UINT uTimeout)			 = 0;
	virtual BOOL METHOD Send(CIPADDR &Addr)				 = 0;
	virtual BOOL METHOD Send(CIPADDR &Addr, PBYTE pData, UINT uSize) = 0;
	virtual BOOL METHOD Send(CIPADDR &Addr, CBuffer *pMsg)		 = 0;
	virtual BOOL METHOD Recv(PBYTE pData, UINT &uSize)		 = 0;
	virtual BOOL METHOD Recv(CBuffer * &pMsg)			 = 0;
	virtual BOOL METHOD Abort(IPADDR const &Addr)			 = 0;
	virtual BYTE METHOD GetLastError(void)				 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// IEtherNetIp Interface
//

interface IEtherNetIp : public IUnknown
{
	AfxDeclareIID(23, 5);

	virtual WORD        METHOD GetVendorId(void)		= 0;
	virtual BOOL        METHOD Open(void)			= 0;
	virtual BOOL        METHOD Close(void)			= 0;
	virtual BOOL        METHOD Connect(void)		= 0;
	virtual IImplicit * METHOD GetImplicit(void)		= 0;
	virtual IExplicit * METHOD GetExplicit(void)		= 0;
	virtual void	    METHOD FreeImplicit(IImplicit *p)	= 0;
	virtual void	    METHOD FreeExplicit(IExplicit *p)	= 0;
	};

// End of File

#endif
