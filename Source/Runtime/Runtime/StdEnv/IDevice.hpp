
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IDevice_HPP

#define INCLUDE_IDevice_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 5 -- Device Driver Control
//

interface IDevice;
interface IDeviceEx;
interface IPlatform;
interface IFeatures;

//////////////////////////////////////////////////////////////////////////
//
// Software Groups
//

enum
{
	SW_GROUP_1  = 0,
	SW_GROUP_2  = 1,
	SW_GROUP_3A = 2,
	SW_GROUP_3B = 3,
	SW_GROUP_3C = 4,
	SW_GROUP_4  = 5
};

//////////////////////////////////////////////////////////////////////////
//
// Platform Features
//

enum
{
	// Hardware
	rfMaskHardware	  = 0xFF,
	rfGraphiteModules = 0x01,
	rfSerialModules   = 0x02,
	rfDisplay         = 0x04
};

//////////////////////////////////////////////////////////////////////////
//
// Secure Device Information
//

#pragma pack(1)

struct CSecureDeviceInfo
{
	BYTE	m_bModel  [8];
	BYTE	m_bSerial [16];
	BYTE	m_bDefPass[22];
	BYTE	m_bMac0[6];
	BYTE	m_bMac1[6];
	DWORD	m_Options;
	WORD	m_Revision;
};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Device Driver Base Class
//

interface IDevice : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/7ICCFQ

	AfxDeclareIID(5, 1);

	virtual BOOL METHOD Open(void) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Device Driver Base Class
//

interface IDeviceEx : public IUnknown
{
	// LINK

	AfxDeclareIID(5, 2);

	virtual BOOL METHOD Open(IUnknown *pUnk) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Platform Object
//

interface IPlatform : public IUnknown
{
	// LINK

	AfxDeclareIID(5, 3);

	virtual PCTXT METHOD GetFamily(void)	     = 0;
	virtual PCTXT METHOD GetModel(void)          = 0;
	virtual PCTXT METHOD GetVariant(void)        = 0;
	virtual PCTXT METHOD GetSerial(void)         = 0;
	virtual PCTXT METHOD GetDefName(void)        = 0;
	virtual UINT  METHOD GetFeatures(void)       = 0;
	virtual BOOL  METHOD IsService(void)         = 0;
	virtual BOOL  METHOD IsEmulated(void)        = 0;
	virtual BOOL  METHOD HasMappings(void)       = 0;
	virtual void  METHOD ResetSystem(void)       = 0;
	virtual void  METHOD SetWebAddr(PCTXT pAddr) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Features Object
//

interface IFeatures : public IUnknown
{
	// LINK

	AfxDeclareIID(5, 4);

	virtual UINT METHOD GetEnabledGroup(void)                   = 0;
	virtual BOOL METHOD GetDeviceInfo(CSecureDeviceInfo &Info)  = 0;
	virtual UINT METHOD GetDeviceCode(PBYTE pData, UINT uData)  = 0;
	virtual BOOL METHOD InstallUnlock(PCBYTE pData, UINT uData) = 0;
};

// End of File

#endif
