
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_ICypto_HPP

#define INCLUDE_ICypto_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "IpAddr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Family 8 -- Cryptography
//

interface IEntropy;
interface ICryptoHash;
interface ICryptoHmac;
interface ICryptoSym;
interface ICryptoAsym;
interface ICryptoSign;
interface ICryptoVerify;
interface ICertGen;
interface IPemParser;

//////////////////////////////////////////////////////////////////////////
//
// Hash Formats
//

enum
{
	hashHex    = 0,
	hashBase64 = 1,
	hashUrlEnc = 2,
	hashUrl64  = 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Asymetric Options
//

enum
{
	asymKeyPublic  = 0x0001,
	asymKeyPrivate = 0x0002,
	asymKeyMask    = 0x000F
	};

//////////////////////////////////////////////////////////////////////////
//
// RSA Options
//

enum
{
	rsaFromBlob   = 0x0100,
	rsaFromDER    = 0x0200,
	rsaFromPEM    = 0x0300,
	rsaFromCert   = 0x0400,
	rsaFromMask   = 0x0F00,
	rsaPadNone    = 0x1000,
	rsaPadPkcs15  = 0x2000,
	rsaPadOaep    = 0x3000,
	rsaPadOaep256 = 0x4000,
	rsaPadMask    = 0xF000
	};

//////////////////////////////////////////////////////////////////////////
//
// Entropy Source
//

interface IEntropy : public IDevice
{
	// LINK

	AfxDeclareIID(8, 1);

	virtual BOOL METHOD GetEntropy(PBYTE pData, UINT uData) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Hash Provider
//

interface ICryptoHash : public IUnknown
{
	// LINK

	AfxDeclareIID(8, 2);

	virtual CString GetName(void)                    = 0;
	virtual void    GetHashOid(CByteArray &oid)	 = 0;
	virtual UINT    GetHashSize(void)                = 0;
	virtual PCBYTE  GetHashData(void)                = 0;
	virtual BOOL    GetHashData(CByteArray &Data)    = 0;
	virtual CString GetHashString(UINT Code)         = 0;
	virtual void    Initialize(void)		 = 0;
	virtual void    Update(PCBYTE pData, UINT uData) = 0;
	virtual void    Update(CByteArray const &Data)   = 0;
	virtual void    Update(CString const &Data)      = 0;
	virtual void    Finalize(void)                   = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// HMAC Provider
//

interface ICryptoHmac : public ICryptoHash
{
	// LINK

	AfxDeclareIID(8, 3);

	virtual void Initialize(PCBYTE pPass, UINT uPass) = 0;
	virtual void Initialize(CByteArray const &Pass)   = 0;
	virtual void Initialize(CString const &Pass)	  = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Symmetric Encryption Provider
//

interface ICryptoSym : public IUnknown
{
	// LINK

	AfxDeclareIID(8, 2);

	virtual CString GetName(void)                                  = 0;
	virtual void    Initialize(PCBYTE pPass, UINT uPass)           = 0;
	virtual void    Initialize(CByteArray const &Pass)             = 0;
	virtual void    Initialize(CString const &Pass)	               = 0;
	virtual void    Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)    = 0;
	virtual void    Encrypt(PBYTE pData, UINT uSize)	       = 0;
	virtual void    Encrypt(CByteArray &Out, CByteArray const &In) = 0;
	virtual void    Encrypt(CByteArray &Data)		       = 0;
	virtual void    Decrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)    = 0;
	virtual void    Decrypt(PBYTE pData, UINT uSize)	       = 0;
	virtual void    Decrypt(CByteArray &Out, CByteArray const &In) = 0;
	virtual void    Decrypt(CByteArray &Data)		       = 0;
	virtual void    Finalize(void)                                 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Asymmetric Encryption Provider
//

interface ICryptoAsym : public IUnknown
{
	// LINK

	AfxDeclareIID(8, 3);

	virtual CString GetName(void)                                                        = 0;
	virtual UINT    GetKeyBytes(void)                                                    = 0;
	virtual BOOL	Initialize(PCBYTE pKey, UINT uKey, CString const &Pass, UINT uFlags) = 0;
	virtual BOOL    Initialize(CByteArray const &Key, CString const &Pass, UINT uFlags)  = 0;
	virtual BOOL	Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)                          = 0;
	virtual BOOL	Encrypt(CByteArray &Out, CByteArray const &In)                       = 0;
	virtual BOOL	Decrypt(CByteArray &Out, PCBYTE pIn)				     = 0;
	virtual void    Finalize(void)                                                       = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Digital Signature Provider
//

interface ICryptoSign : public IUnknown
{
	// LINK

	AfxDeclareIID(8, 4);

	virtual CString GetName(void)                                           = 0;
	virtual UINT    GetSigSize(void)                                        = 0;
	virtual PCBYTE  GetSigData(void)                                        = 0;
	virtual BOOL    GetSigData(CByteArray &Data)                            = 0;
	virtual CString GetSigString(UINT Code)                                 = 0;
	virtual BOOL	Initialize(PCBYTE pKey, UINT uKey, CString const &Pass) = 0;
	virtual BOOL    Initialize(CByteArray const &Key, CString const &Pass)  = 0;
	virtual void    SetHash(ICryptoHash *pHash)				= 0;
	virtual BOOL    SetHash(PCSTR pName)					= 0;
	virtual void    Update(PCBYTE pData, UINT uData)                        = 0;
	virtual void    Update(CByteArray const &Data)                          = 0;
	virtual void    Update(CString const &Data)                             = 0;
	virtual void    Finalize(void)                                          = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Digital Signature Verifier
//

interface ICryptoVerify : public IUnknown
{
	// LINK

	AfxDeclareIID(8, 6);

	virtual CString GetName(void)                                = 0;
	virtual BOOL	Verify(PCBYTE pSig, UINT uSig, UINT uFormat) = 0;
	virtual BOOL	Initialize(PCBYTE pKey, UINT uKey)           = 0;
	virtual BOOL    Initialize(CByteArray const &Key)            = 0;
	virtual void    SetHash(ICryptoHash *pHash)	             = 0;
	virtual BOOL    SetHash(PCSTR pName)		             = 0;
	virtual void    Update(PCBYTE pData, UINT uData)             = 0;
	virtual void    Update(CByteArray const &Data)               = 0;
	virtual void    Update(CString const &Data)                  = 0;
	virtual void    Finalize(void)                               = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Certificate Creation
//

interface ICertGen : public IUnknown
{
	// LINK

	AfxDeclareIID(8, 5);

	virtual void   AddName(PCTXT pName)                  = 0;
	virtual void   AddAddr(IPREF Addr )                  = 0;
	virtual void   SetRootCert(PCBYTE pCert, UINT uCert) = 0;
	virtual void   SetRootPriv(PCBYTE pPriv, UINT uPriv) = 0;
	virtual void   SetRootPass(PCTXT pPass)              = 0;
	virtual bool   Generate(void)                        = 0;
	virtual PCBYTE GetRootData(void)                     = 0;
	virtual UINT   GetRootSize(void)                     = 0;
	virtual PCBYTE GetCertData(void)                     = 0;
	virtual UINT   GetCertSize(void)                     = 0;
	virtual PCBYTE GetPrivData(void)                     = 0;
	virtual UINT   GetPrivSize(void)                     = 0;
	virtual PCTXT  GetPrivPass(void)                     = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// PEM Parser
//

interface IPemParser : public IUnknown
{
	// LINK

	AfxDeclareIID(8, 6);

	virtual BOOL Decode(CByteArray &Data, PCSTR pPass, PCSTR  pText)             = 0;
	virtual BOOL Decode(CByteArray &Data, PCSTR pPass, PCBYTE pText, UINT uSize) = 0;
};

// End of File

#endif
