
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IPort_HPP

#define INCLUDE_IPort_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 12 -- Port Objects
//

interface IPortObject;
interface IPortSwitch;
interface IPortHandler;
interface IDataHandler;

//////////////////////////////////////////////////////////////////////////
//
// Parity Codes
//

enum
{
	parityNone,
	parityOdd,
	parityEven,
	};

//////////////////////////////////////////////////////////////////////////
//
// Physical Layer Codes
//

enum
{
	physicalNone,
	physicalRS232,
	physicalRS422Master,
	physicalRS422Slave,
	physicalRS485,
	physical20mA,
	physicalVirtual,
	physicalCAN,
	physicalProfibus,
	physicalFireWire,
	physicalDeviceNet,
	physicalCatLink,
	physicalMPI,
	physicalJ1939

	};

//////////////////////////////////////////////////////////////////////////
//
// Serial Port Flags
//

enum
{
	flagNone	  = 0x0000,
	flagFastRx	  = 0x0001,
	flagNoCheckParity = 0x0002,
	flagHonorCTS	  = 0x0004,
	flagNoBatchSend	  = 0x0008,
	flagPrivate	  = 0x0010,
	flagTimeout	  = 0x0020,
	flagExtID	  = 0x0040,
	flagNoCheckError  = 0x0080,
	flagHonorRTS	  = 0x0100,
	flagEncapMode	  = 0x0200,
	flagExtPass	  = 0x0400,
	};

//////////////////////////////////////////////////////////////////////////
//
// Serial Port Configuration
//

struct CSerialConfig
{
	UINT  m_uPhysical;
	UINT  m_uPhysMode;
	UINT  m_uBaudRate;
	UINT  m_uDataBits;
	UINT  m_uStopBits;
	UINT  m_uParity;
	UINT  m_uFlags;
	BYTE  m_bDrop;
	};

//////////////////////////////////////////////////////////////////////////
//
// Device Port Object
//

interface IPortObject : public IDevice
{
	// https://redlion.atlassian.net/wiki/x/hwFwFQ

	AfxDeclareIID(12, 1);

	virtual void  METHOD Bind(IPortHandler *pHandler)	= 0;
	virtual void  METHOD Bind(IPortSwitch  *pSwitch)	= 0;
	virtual UINT  METHOD GetPhysicalMask(void)		= 0;
	virtual BOOL  METHOD Open(CSerialConfig const &Config)	= 0;
	virtual void  METHOD Close(void)			= 0;
	virtual void  METHOD Send(BYTE bData)			= 0;
	virtual void  METHOD SetBreak(BOOL fBreak)		= 0;
	virtual void  METHOD EnableInterrupts(BOOL fEnable)	= 0;
	virtual void  METHOD SetOutput(UINT uOutput, BOOL fOn)	= 0;
	virtual BOOL  METHOD GetInput(UINT uInput)		= 0;
	virtual DWORD METHOD GetHandle(void)			= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Serial Port Switch
//

interface IPortSwitch : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/jQFwFQ

	AfxDeclareIID(12, 2);

	virtual UINT METHOD GetCount(UINT uUnit)		 = 0;
	virtual UINT METHOD GetMask(UINT uUnit)			 = 0;
	virtual UINT METHOD GetType(UINT uUnit, UINT uLog)	 = 0;
	virtual BOOL METHOD EnablePort(UINT uUnit, BOOL fEnable) = 0;
	virtual BOOL METHOD SetPhysical(UINT uUnit, BOOL fRS485) = 0;
	virtual BOOL METHOD SetFull(UINT uUnit, BOOL fFull)      = 0;
	virtual BOOL METHOD SetMode(UINT uUnit, BOOL fAuto)	 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Device Port Handler
//

interface IPortHandler : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/oYCAFQ

	AfxDeclareIID(12, 3);

	virtual void METHOD Bind(IPortObject *pPort)		= 0;
	virtual void METHOD OnRxData(BYTE bData)		= 0;
	virtual void METHOD OnRxDone(void)			= 0;
	virtual BOOL METHOD OnTxData(BYTE &bData)		= 0;
	virtual void METHOD OnTxDone(void)			= 0;
	virtual void METHOD OnOpen(CSerialConfig const &Config)	= 0;
	virtual void METHOD OnClose(void)			= 0;
	virtual void METHOD OnTimer(void)			= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Device Port Data Handler
//

interface IDataHandler : public IPortHandler
{
	// https://redlion.atlassian.net/wiki/x/8ICCFQ

	AfxDeclareIID(12, 4);

	virtual void METHOD SetTxSize(UINT uSize)			 = 0;
	virtual void METHOD SetRxSize(UINT uSize)			 = 0;
	virtual UINT METHOD Read(UINT uTime)				 = 0;
	virtual BOOL METHOD Write(BYTE bData, UINT uTime)		 = 0;
	virtual UINT METHOD Read(PBYTE pData, UINT uCount, UINT uTime)   = 0;
	virtual UINT METHOD Write(PCBYTE pData, UINT uCount, UINT uTime) = 0;
	virtual void METHOD SetBreak(BOOL fBreak)			 = 0;
	virtual void METHOD ClearRx(void)				 = 0;
	virtual void METHOD ClearTx(void)				 = 0;
	};

// End of File

#endif
