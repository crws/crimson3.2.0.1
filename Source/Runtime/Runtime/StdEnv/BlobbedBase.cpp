
#include "Intern.hpp"

#include "BlobbedBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "String.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Blob-Persisted Base Class
//

// Init Check

void CBlobbedBase::ValidateLoad(PCTXT pName,PCBYTE &pData)
{
	AfxVerify(GetWord(pData) == 0x1234);
	}

// Init Helpers

PCUTF CBlobbedBase::GetCryp(PCBYTE &pData)
{
	UINT   s = GetWord(pData);

	PCBYTE p = PCBYTE(pData);

	PBYTE  w = PBYTE(alloca(s));

	pData   += s;

	////////

	PCSTR pPass = "PineappleHead";

	memcpy(w, p, s);

	rc4(w, s, PBYTE(pPass), strlen(pPass));

	////////

	PCBYTE t = w;

	return GetWide(t);
	}

// Code Helper

void CBlobbedBase::GetCoded(PCBYTE &pData, CString &Text)
{
	switch( GetByte(pData) ) {

		case 0:
		{
			Text.Empty();
		}
		break;

		case 1:
		{
			// TODO -- This needs to be hacked once Kathy's
			// alignment and other mods are merged across!!!

			CByteArray Data;

			WORD wMark = GetWord(pData);

			Data.Append(HIBYTE(wMark));
			Data.Append(LOBYTE(wMark));

			UINT uRefs = GetWord(pData);

			Data.Append(HIBYTE(uRefs));
			Data.Append(LOBYTE(uRefs));

			while( uRefs-- ) {

				DWORD r = GetLong(pData);

				Data.Append(HIBYTE(HIWORD(r)));
				Data.Append(LOBYTE(HIWORD(r)));
				Data.Append(HIBYTE(LOWORD(r)));
				Data.Append(LOBYTE(LOWORD(r)));
			}

			UINT uCode = GetWord(pData);

			Data.Append(HIBYTE(uCode));
			Data.Append(LOBYTE(uCode));

			Data.Append(pData, uCode);

			pData += uCode;

			Text = CString(' ', Data.size());

			memcpy(PBYTE(PCSTR(Text)), Data.data(), Data.size());
		}
		break;

		default:
		{
			AfxAssert(FALSE);
		}
		break;
	}
}

// End of File
