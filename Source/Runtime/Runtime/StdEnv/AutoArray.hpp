
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AutoArray_HPP

#define	INCLUDE_AutoArray_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Automatic Array
//

template <typename CData> class DLLAPI CAutoArray
{
public:
	// Constructors

	STRONG_INLINE CAutoArray(UINT uSize)
	{
		m_pData = new CData[uSize];
	}

	STRONG_INLINE CAutoArray(CData *pData)
	{
		m_pData = pData;
	}

	STRONG_INLINE CAutoArray(CData const *pData, UINT uSize)
	{
		m_pData = new CData[uSize];

		memcpy(m_pData, pData, uSize);
	}

	STRONG_INLINE CAutoArray(void)
	{
		m_pData = NULL;
	}
	
	// Destructor

	STRONG_INLINE ~CAutoArray(void)
	{
		delete[] m_pData;
	}

	// Operations

	STRONG_INLINE void Alloc(UINT uSize)
	{
		AfxAssert(!m_pData);

		m_pData = new CData[uSize];
	}

	STRONG_INLINE CData * TakeOver(void)
	{
		AfxAssert(m_pData);

		CData *pData = m_pData;

		m_pData      = NULL;

		return pData;
	}

	// Conversion

	STRONG_INLINE operator CData * (void) const
	{
		return m_pData;
	}

protected:
	// Data Members
	CData * m_pData;

private:
	// No Assign or Copy

	void operator = (CAutoArray const &That) const {};

	CAutoArray(CAutoArray const &That) {};
};

// End of File

#endif
