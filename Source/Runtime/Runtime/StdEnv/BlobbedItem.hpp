
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_BlobbedItem_HPP

#define INCLUDE_BlobbedItem_HPP

/////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BlobbedBase.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Blob-Persisted Item
//

class DLLAPI CBlobbedItem : public CBlobbedBase
{
	public:
		// Destructor
		virtual ~CBlobbedItem(void);

		// Persistance
		virtual void Load(PCBYTE &pData);
	};

// End of File

#endif
