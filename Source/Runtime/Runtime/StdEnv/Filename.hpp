
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_Filename_HPP

#define	INCLUDE_Filename_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// File Name Parser
//

class DLLAPI CFilename
{
	public:
		// Constructors
		CFilename(void);
		CFilename(CFilename const &That);
		CFilename(CFilename const &That, UINT uLen);
		CFilename(CString const &Text);
		CFilename(PCTXT pText);

		// Destructor
		~CFilename(void);
		
		// Assignment Operator
		CFilename const & operator = (CFilename const &That);
		CFilename const & operator = (CString const &Text);
		CFilename const & operator = (PCTXT pText);
		
		// Concatenation In-Place
		CFilename const & operator += (CFilename const &That);
		CFilename const & operator += (CString const &Text);
		CFilename const & operator += (PCTXT pText);

		// Concatenation via Friends
		friend CFilename operator + (CFilename const &A, CFilename const &B);
		friend CFilename operator + (CFilename const &A, CString const &Text);
		friend CFilename operator + (CFilename const &A, PCTXT pText);

		// Attributes
		BOOL  IsEmpty(void) const;
		UINT  GetLength(void) const;
		PBYTE GetData(void) const;
		PCTXT GetRest(void) const;
		BOOL  HasDrive(void) const;
		BOOL  HasType(void) const;
		BOOL  HasDirectory(void) const;
		CHAR  GetDrive(void) const;
		PCTXT GetType(void) const;
		PCTXT GetPath(void) const;
		
		// Operations
		void MakeUpper(void);
		void MakeLower(void);
		void Empty(void);
		BOOL ChangeType(PCTXT pExt);

		// Element Parsing
		BOOL  MoveTop(PTXT pName) const;
		BOOL  MoveDown(PTXT pName) const;
		BOOL  GetDirectory(CFilename &Dir) const;
		PCTXT GetName(void) const;
		BOOL  GetBareName(PTXT pName) const;

		// Comparison
		BOOL operator == (CFilename const &That) const;
		BOOL operator == (PCTXT pText) const;

		// Conversion
		operator PCTXT (void) const;

		// Access
		CHAR operator [] (UINT iIndex) const;

	protected:
		// Data Members
		PTXT m_pData;
		UINT m_uLen;
		UINT m_uAlloc;

		// Mutable Data
		mutable UINT m_uInit;

		// Initialisation
		void Init(void);
		void InitFrom(PCTXT pName, UINT uLen);
		
		// Implemenation
		void Free(void);
		void Alloc(UINT uSize);

		// Type Helpers
		void RemoveType(void);
		BOOL AppendType(PCTXT pExt);
	};

// End of File

#endif
