
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Path Helpers
//

bool PathAppendSlash(char *path)
{
	UINT n = strlen(path);

	if( !n || path[n-1] != '\\' ) {

		path[n++] = '\\';

		path[n++] = 0;

		return true;
		}

	return false;
	}

void PathMakeAbsolute(char *path, char const *name)
{
	if( name[0] == '\\' ) {

		getcwd(path, MAX_PATH);

		strcpy(path+2, name);

		return;
		}

	if( isalpha(name[0]) && name[1] == ':' ) {

		strcpy(path, name);

		return;
		}

	getcwd(path, MAX_PATH);

	PathAppendSlash(path);

	strcat(path, name);
	}

void PathJoin(char *path, char const *p1, char const *p2)
{
	strcpy(path, p1);

	PathAppendSlash(path);

	strcat(path, p2);
	}

void PathAppend(char *path, char const *name)
{
	PathAppendSlash(path);

	strcat(path, name);
	}

void PathFromFile(char *path, char const *name)
{
	char *find = strrchr(name, '\\');

	if( find ) {

		char work[MAX_PATH];

		strcpy(work, name);

		work[find - name] = 0;

		PathMakeAbsolute(path, work);

		return;
		}

	PathMakeAbsolute(path, ".");
	}

// End of File
