
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_ITimeZone_HPP

#define INCLUDE_ITimeZone_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 26 -- Time Zone
//

interface ITimeZone;

//////////////////////////////////////////////////////////////////////////
//
// Time Zone Storage
//

interface ITimeZone : public IDevice
{
	// https://

	AfxDeclareIID(26, 1);

	virtual BOOL   METHOD GetDst(void)          = 0;
	virtual INT    METHOD GetOffset(void)       = 0;
	virtual time_t METHOD GetNext(void)         = 0;
	virtual BOOL   METHOD Lock(DWORD magic)     = 0;
	virtual void   METHOD SetDst(BOOL dst)      = 0;
	virtual void   METHOD SetOffset(INT offset) = 0;
	virtual void   METHOD SetNext(time_t next)  = 0;
};

// End of File

#endif
