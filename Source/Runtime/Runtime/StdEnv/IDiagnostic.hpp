
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IDiagnostic_HPP

#define INCLUDE_IDiagnostic_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 2 -- Diagnostic Interfaces
//

// https://redlion.atlassian.net/wiki/x/GgAIEQ

interface IDiagManager;
interface IDiagProvider;
interface IDiagConsole;
interface IDiagOutput;
interface IDiagCommand;

//////////////////////////////////////////////////////////////////////////
//
// Diagnostic Manager
//

interface IDiagManager : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/SIAHEQ

	AfxDeclareIID(2, 1);

	virtual UINT METHOD RegisterProvider(IDiagProvider *pProv, PCTXT pName)  = 0;
	virtual void METHOD RevokeProvider(UINT pProv)				 = 0;
	virtual void METHOD RegisterCommand(UINT uProv, UINT uCode, PCTXT pName) = 0;
	virtual UINT METHOD RunCommand(IDiagConsole *pConsole, PCTXT pLine)      = 0;
	virtual UINT METHOD RunCommand(char **ppBuff, UINT uEnc, PCTXT pLine)    = 0;
	virtual BOOL METHOD CompleteCommand(char *pText, UINT uSize)		 = 0;
	virtual UINT METHOD RegisterConsole(IDiagConsole *pConsole)		 = 0;
	virtual void METHOD RevokeConsole(UINT uConsole)			 = 0;
	virtual void METHOD WriteToConsoles(char const *pText)			 = 0;
	virtual BOOL METHOD HasConsoles(void)					 = 0;
	virtual BOOL METHOD SerializeAccess(BOOL fLock)				 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Diagnostic Provider
//

interface IDiagProvider : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/pIAIEQ

	AfxDeclareIID(2, 2);

	virtual UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Diagnostic Console
//

interface IDiagConsole : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/QgAJEQ

	AfxDeclareIID(2, 3);

	virtual void METHOD Write(PCTXT pText) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Diagnostic Output
//

interface IDiagOutput : public IUnknown
{
	AfxDeclareIID(2, 4);

	virtual void METHOD Error(PCTXT pText, ...)			   = 0;
	virtual void METHOD Print(PCTXT pText, ...)			   = 0;
	virtual void METHOD VPrint(PCTXT pText, va_list pArgs)		   = 0;
	virtual void METHOD Dump(PCVOID pData, UINT uCount)		   = 0;
	virtual void METHOD Dump(PCVOID pData, UINT uCount, UINT uBase)	   = 0;
	virtual void METHOD AddTable(UINT uCols)			   = 0;
	virtual void METHOD EndTable(void)				   = 0;
	virtual void METHOD SetColumn(UINT uCol, PCTXT pName, PCTXT pForm) = 0;
	virtual void METHOD AddHead(void)				   = 0;
	virtual void METHOD AddRule(char cData)				   = 0;
	virtual void METHOD AddRow(void)				   = 0;
	virtual void METHOD EndRow(void)				   = 0;
	virtual void METHOD SetData(UINT uCol, ...)			   = 0;
	virtual void METHOD AddPropList(void)				   = 0;
	virtual void METHOD EndPropList(void)				   = 0;
	virtual void METHOD AddProp(PCTXT pName, PCTXT pForm, ...)	   = 0;
	virtual void METHOD Finished(void)				   = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Diagnostic Command
//

interface IDiagCommand : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/PgAJEQ

	AfxDeclareIID(2, 5);

	virtual UINT  METHOD GetCode    (void)	    = 0;
	virtual PCTXT METHOD GetCmdLine (void)	    = 0;
	virtual UINT  METHOD GetArgCount(void)	    = 0;
	virtual PCTXT METHOD GetArg     (UINT uArg) = 0;
	virtual BOOL  METHOD FromConsole(void)      = 0;
	};

// End of File

#endif
