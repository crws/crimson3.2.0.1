
#include "Intern.hpp"

#include "AutoDirentList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Automatic Directory Entry List
//

// Operations

bool CAutoDirentList::AddFileTimes(char const *name)
{
	if( m_uSize ) {

		char path[MAX_PATH];

		PathMakeAbsolute(path, name);

		PathAppendSlash(path);

		for( UINT n = 0; n < m_uSize; n++ ) {

			char full[MAX_PATH];

			PathJoin(full, path, m_pList[n]->d_name);

			struct stat s = { 0 };

			::stat(full, &s);

			m_pList[n]->d_off = s.st_mtime;
			}

		return true;
		}

	return false;
	}

bool CAutoDirentList::SortByTime(char const *name, int (*compare)(struct dirent const **, struct dirent const **))
{
	if( AddFileTimes(name) ) {

		qsort(	m_pList,
			m_uSize,
			sizeof(struct dirent *),
			(__compar_fn_t) compare
			);

		return true;
		}

	return false;
	}

int CAutoDirentList::stat(char const *name, UINT n, struct stat *s)
{
	if( n < m_uSize ) {

		char path[MAX_PATH];

		PathMakeAbsolute(path, name);

		PathAppendSlash(path);

		char full[MAX_PATH];

		PathJoin(full, path, m_pList[n]->d_name);

		return ::stat(full, s);
		}

	return -1;
	}

// Sorting

int CAutoDirentList::StdSort(struct dirent const **p1, struct dirent const **p2)
{
	struct dirent const &d1 = **p1;

	struct dirent const &d2 = **p2;

	if( d1.d_type < d2.d_type ) {

		return -1;
		}

	if( d1.d_type > d2.d_type ) {

		return +1;
		}

	return strcmp(d1.d_name, d2.d_name);
	}

int CAutoDirentList::RevSort(struct dirent const **p1, struct dirent const **p2)
{
	return StdSort(p2, p1);
}

int CAutoDirentList::FwdTimeSort(struct dirent const **p1, struct dirent const **p2)
{
	struct dirent const &d1 = **p1;

	struct dirent const &d2 = **p2;

	if( d1.d_off < d2.d_off ) {

		return -1;
		}

	if( d1.d_off > d2.d_off ) {

		return +1;
		}

	return 0;
	}

int CAutoDirentList::RevTimeSort(struct dirent const **p1, struct dirent const **p2)
{
	return FwdTimeSort(p2, p1);
	}

// Selection

int CAutoDirentList::SelectFiles(struct dirent const *p)
{
	return p->d_type == DT_REG;
	}

int CAutoDirentList::SelectDirs(struct dirent const *p)
{
	return p->d_type == DT_DIR && strcmp(p->d_name, ".") && strcmp(p->d_name, "..");
	}

// End of File
