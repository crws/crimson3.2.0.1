
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_Range_HPP

#define	INCLUDE_Range_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Linear Range
//

class DLLAPI CRange
{
	public:
		// Constructors
		CRange(void);
		CRange(INT nFromTo);
		CRange(INT nFrom, INT nTo);

		// Attributes
		UINT GetCount(void) const;
		BOOL IsEmpty(void) const;

		// Operations
		void Empty(void);
		
		// Containment
		BOOL Contains(INT nData) const;
		BOOL ContainsAll(INT nFrom, INT nTo) const;
		BOOL ContainsAny(INT nFrom, INT nTo) const;
		BOOL ContainsAll(CRange const &That) const;
		BOOL ContainsAny(CRange const &That) const;

		// Truncation
		CRange & Truncate(INT nFrom, INT nTo);
		CRange & Truncate(CRange const &Exclude);

		// Conversion
		operator BOOL (void) const;

		// Comparison
	        BOOL operator == (CRange const &That) const;
	        BOOL operator != (CRange const &That) const;

		// In-Place Operators
		CRange const & operator &= (CRange const &That);
		CRange const & operator |= (CRange const &That);

		// Friend Operators
		friend CRange operator & (CRange const &A, CRange const &B);
		friend CRange operator | (CRange const &A, CRange const &B);

		// Data
		INT m_nFrom;
		INT m_nTo;
	};

// End of File

#endif
