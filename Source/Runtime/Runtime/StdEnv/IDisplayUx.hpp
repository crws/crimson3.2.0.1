
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IDisplayUx_HPP

#define INCLUDE_IDisplayUx_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 6 -- Display and User Interface
//

interface IInputQueue;
interface IDisplay;
interface IPointer;
interface ITouchScreen;
interface IKeyboard;
interface IBeeper;
interface ILeds;
interface IInputSwitch;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "IDevice.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Input Wrapper Type
//

#if defined(_M_IX86)

typedef INT64	INPUT;

#else

typedef DWORD	INPUT;

#endif

//////////////////////////////////////////////////////////////////////////
//
// Virtual Key Codes
//

#if !defined(AEON_NEED_WIN32)

#define	VK_SOFT1	0x80
#define	VK_SOFT2	0x81
#define	VK_SOFT3	0x82
#define	VK_SOFT4	0x83
#define	VK_SOFT5	0x84
#define	VK_SOFT6	0x85
#define	VK_SOFT7	0x86
#define	VK_SOFT8	0x87
#define	VK_SOFT9	0x88

#define	VK_F1		0x90
#define	VK_F2		0x91
#define	VK_F3		0x92
#define	VK_F4		0x93
#define	VK_F5		0x94
#define	VK_F6		0x95
#define	VK_F7		0x96
#define	VK_F8		0x97
#define	VK_F9		0x98
#define	VK_F10		0x99
#define	VK_F11		0x9A
#define	VK_F12		0x9B
#define	VK_F13		0x9C
#define	VK_F14		0x9D
#define	VK_F15		0x9E
#define	VK_F16		0x9F

#define	VK_ALARMS	0xA0
#define	VK_MUTE		0xA1
#define	VK_EXIT		0x1B
#define	VK_MENU		0xA2

#define	VK_RAISE	0xA3
#define	VK_LOWER	0xA4
#define	VK_NEXT		0x09
#define	VK_PREV		0x08
#define	VK_ENTER	0x0D

#define	VK_SECURE	0xF0

#endif

//////////////////////////////////////////////////////////////////////////
//
// Input Source Codes
//

enum InputSource
{
	typeKey	  = 0,
	typeTouch = 1
	};

//////////////////////////////////////////////////////////////////////////
//
// Input Transition Codes
//

enum InputTransition
{
	stateDown   = 0,
	stateUp	    = 1,
	stateRepeat = 2,
	stateDrag   = 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Input Information
//

#pragma pack(1)

struct CInputInfo
{
	DWORD	m_Type	: 1;
	DWORD	m_State	: 3;
	DWORD	m_Local : 1;
	DWORD	m_Spare : 3;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Input Data Union
//

#pragma pack(1)

struct CInputData
{
	union
	{
		struct /*CInputKey*/
		{
			DWORD	m_Code	: 16;

			} k;

		struct /*CInputTouch*/
		{
			DWORD	m_XPos	: 12;
			DWORD	m_YPos	: 12;

			} t;
		};
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Input Queue Entry
//

#pragma pack(1)

struct CInput
{
	union
	{
		struct
		{
			CInputInfo	i;
			CInputData	d;

			} x;

		INPUT m_Ref;
		};
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Input Queue
//

interface IInputQueue : public IDevice
{
	// LINK

	AfxDeclareIID(6, 1);

	// Methods
	virtual INPUT METHOD Read (UINT uTime)  = 0;
	virtual bool  METHOD Store(INPUT Input) = 0;
	virtual void  METHOD Clear(void)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Display Interface
//

interface IDisplay : public IDevice
{
	// LINK

	AfxDeclareIID(6, 2);

	virtual void METHOD Claim(void)               = 0;
	virtual void METHOD Free(void)                = 0;
	virtual void METHOD GetSize(int &cx, int &cy) = 0;
	virtual void METHOD Update(PCVOID pData)      = 0;
	virtual BOOL METHOD SetBacklight(UINT pc)     = 0;
	virtual UINT METHOD GetBacklight(void)	      = 0;
	virtual BOOL METHOD EnableBacklight(BOOL fOn) = 0;
	virtual BOOL METHOD IsBacklightEnabled(void)  = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Display Pointer
//

interface IPointer : public IDevice
{
	// LINK

	AfxDeclareIID(6, 3);

	virtual void METHOD LockPointer(bool fLock)           = 0;
	virtual void METHOD SetPointerPos(int xPos, int yPos) = 0;
	virtual void METHOD ShowPointer(bool fShow)           = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Touch Screen Interface
//

interface ITouchScreen : public IDevice
{
	// LINK

	AfxDeclareIID(6, 5);

	virtual void   METHOD GetCellSize(int &xCell, int &yCell)  = 0;
	virtual void   METHOD GetDispCells(int &xDisp, int &yDisp) = 0;
	virtual void   METHOD SetBeepMode(bool fBeep)		   = 0;
	virtual void   METHOD SetDragMode(bool fDrag)		   = 0;
	virtual PCBYTE METHOD GetMap(void)			   = 0;
	virtual void   METHOD SetMap(PCBYTE pMap)		   = 0;
	virtual bool   METHOD GetRaw(int &xRaw, int &yRaw)	   = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Calibrating Touch Screen Interface
//

interface ITouchScreenCalib : public ITouchScreen
{
	// LINK

	AfxDeclareIID(6, 8);

	virtual bool METHOD SetCalib(int xMin, int yMin, int xMax, int yMax) = 0;
	virtual void METHOD ClearCalib(bool fPersist)			     = 0;
	virtual void METHOD LoadCalib(void)				     = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Keyboard Interface
//

interface IKeyboard : public IDevice
{
	// LINK

	AfxDeclareIID(6, 6);

	virtual void METHOD SetBeepMode(bool fBeep) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Beeper Tones
//

enum BeeperTone
{
	beepStart = 1000,
	beepPress = 1001,
	beepFull  = 1002,
	};

//////////////////////////////////////////////////////////////////////////
//
// Beeper
//

interface IBeeper : public IDevice
{
	// LINK

	AfxDeclareIID(6, 7);

	virtual void METHOD Beep(UINT uBeep)	         = 0;
	virtual void METHOD Beep(UINT uFreq, UINT uTime) = 0;
	virtual void METHOD BeepOff(void)                = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// LED State
//

enum LedState
{
	stateOff   = 0,
	stateGreen = Bit(0),
	stateRed   = Bit(1),
	stateBlue  = Bit(2),
	stateOn	   = 255
	};

//////////////////////////////////////////////////////////////////////////
//
// LEDs
//

interface ILeds : public IDevice
{
	// LINK

	AfxDeclareIID(6, 8);

	virtual void METHOD SetLed(UINT uLed, UINT uState)            = 0;
	virtual void METHOD SetLedLevel(UINT uPercent, bool fPersist) = 0;
	virtual UINT METHOD GetLedLevel(void)			      = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Swithes 
//

interface IInputSwitch : public IDevice
{
	// LINK

	AfxDeclareIID(6, 9);

	virtual UINT METHOD GetSwitches(void)       = 0;
	virtual UINT METHOD GetSwitch(UINT uSwitch) = 0;
	};

// End of File

#endif
