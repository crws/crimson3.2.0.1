
#include "Intern.hpp"

#include "Range.hpp"

#include <limits.h>

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Linear Range
//

// Constructors

CRange::CRange(void)
{
	Empty();
	}

CRange::CRange(INT nFromTo)
{
	m_nFrom = nFromTo;

	m_nTo   = nFromTo;
	}

CRange::CRange(INT nFrom, INT nTo)
{
	m_nFrom = nFrom;

	m_nTo   = nTo;
	}

// Attributes

UINT CRange::GetCount(void) const
{
	return IsEmpty() ? 0 : (m_nTo - m_nFrom + 1);
	}

BOOL CRange::IsEmpty(void) const
{
	return m_nTo < m_nFrom;
	}

// Operations

void CRange::Empty(void)
{
	m_nFrom = INT_MAX;
	
	m_nTo   = INT_MIN;
	}

// Containment

BOOL CRange::Contains(INT nData) const
{
	return nData >= m_nFrom && nData <= m_nTo;
	}

BOOL CRange::ContainsAll(INT nFrom, INT nTo) const
{
	return ContainsAll(CRange(nFrom, nTo));
	}

BOOL CRange::ContainsAny(INT nFrom, INT nTo) const
{
	return ContainsAny(CRange(nFrom, nTo));
	}

BOOL CRange::ContainsAll(CRange const &That) const
{
	if( IsEmpty() ) {

		return FALSE;
		}

	if( That.IsEmpty() ) {

		return FALSE;
		}

	return Contains(That.m_nFrom) && Contains(That.m_nTo);
	}

BOOL CRange::ContainsAny(CRange const &That) const
{
	if( IsEmpty() ) {

		return FALSE;
		}

	if( That.IsEmpty() ) {

		return FALSE;
		}

	return Contains(That.m_nFrom) || Contains(That.m_nTo);
	}

// Truncation

CRange & CRange::Truncate(INT nFrom, INT nTo)
{
	return Truncate(CRange(nFrom, nTo));
	}

CRange & CRange::Truncate(CRange const &Exclude)
{
	if( !IsEmpty() && !Exclude.IsEmpty() ) {

		if( Exclude.ContainsAll(ThisObject) ) {

			Empty();

			return ThisObject;
			}
		
		if( ContainsAll(Exclude) ) {
			
			if( m_nTo == Exclude.m_nTo ) {

				m_nTo = Exclude.m_nFrom - 1;				
				}
			
			if( m_nFrom == Exclude.m_nFrom ) {

				m_nFrom = Exclude.m_nTo + 1;				
				}

			return ThisObject;
			}
		
		if( ContainsAny(Exclude) ) {

			if( Contains(Exclude.m_nFrom) ) {

				m_nTo = Exclude.m_nFrom - 1;
				}
			else {
				m_nFrom = Exclude.m_nTo + 1;
				}

			return ThisObject;
			}
		}
	
	return ThisObject;
	}

// Conversion

CRange::operator BOOL (void) const
{
	return IsEmpty() ? FALSE : TRUE;
	}

// Comparison

BOOL CRange::operator == (CRange const &That) const
{
	return m_nFrom == That.m_nFrom && m_nTo == That.m_nTo;
	}

BOOL CRange::operator != (CRange const &That) const
{
	return m_nFrom != That.m_nFrom || m_nTo != That.m_nTo;
	}

//  In-Place Operators

CRange const & CRange::operator &= (CRange const &That)
{
	if( ContainsAny(That) ) {
	
		m_nFrom = Max(m_nFrom, That.m_nFrom);

		m_nTo   = Min(m_nTo, That.m_nTo);

		return ThisObject;
		}

	Empty();

	return ThisObject;
	}

CRange const & CRange::operator |= (CRange const &That)
{
	m_nFrom = Min(m_nFrom, That.m_nFrom);

	m_nTo   = Max(m_nTo, That.m_nTo);

	return ThisObject;
	}

//  Friend Operators

CRange operator & (CRange const &A, CRange const &B)
{
	if( A.ContainsAny(B) ) {

		INT nFrom = Max(A.m_nFrom, B.m_nFrom);

		INT nTo   = Min(A.m_nTo, B.m_nTo);

		return CRange(nFrom, nTo);
		}

	return CRange();
	}

CRange operator | (CRange const &A, CRange const &B)
{
	INT nFrom = Min(A.m_nFrom, B.m_nFrom);

	INT nTo   = Max(A.m_nTo, B.m_nTo);

	return CRange(nFrom, nTo);
	}

// End of File
