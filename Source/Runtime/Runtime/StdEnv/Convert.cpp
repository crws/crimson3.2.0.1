
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "String.hpp"

#include "Unicode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Format Conversion
//

void DecodeUtf(CString &Text)
{
	PCTXT p = Text;

	for( UINT i = 0; p[i]; i++ ) {

		if( p[i] & 0x80 ) {

			CString t = Text.Left(i);

			for( UINT j = i; p[j]; j++ ) {

				if( p[j] & 0x80 ) {

					if( (p[j] & 0xE0) == 0xC0 ) {

						WORD c = ((p[j+1] & 0x3F) << 0) |
							 ((p[j+0] & 0x1F) << 6) ;

						if( c < 0x0100 ) {

							t += char(c);
							}
						else
							t.AppendPrintf("\\u%4.4X", c);

						j += 1;
						}

					if( (p[j] & 0xF0) == 0xE0 ) {

						WORD c = ((p[j+2] & 0x3F) <<  0) |
							 ((p[j+1] & 0x3F) <<  6) |
							 ((p[j+0] & 0x0F) << 12) ;

						t.AppendPrintf("\\u%4.4X", c);

						j += 2;
						}

					if( (p[j] & 0xF8) == 0xF0 ) {

						j += 3;
						}
					}
				else
					t += p[j];
				}

			Text = t;

			break;
			}
		}
	}

void EncodeUtf(CString &Text)
{
	PCTXT p = Text;

	for( UINT i = 0; p[i]; i++ ) {

		if( p[i] & 0x80 ) {

			CString t = Text.Left(i);

			for( UINT j = i; p[j]; j++ ) {

				if( p[j] & 0x80 ) {

					t += (0xC0 | ((p[j] & 0xC0) >> 6));
					t += (0x80 | ((p[j] & 0x3F) >> 0));
					}
				else
					t += p[j];
				}

			Text = t;

			break;
			}
		}
	}

CString UtfConvert(PCUTF p)
{
	CString r;

	while( *p ) {

		WCHAR c = *p++;

		if( c < 0x80 ) {
			
			r += char(c);
			}
		else {
			WCHAR c0 = LOBYTE(c);
	
			WCHAR c1 = HIBYTE(c);

			if( c < 0x800 ) {

				r += (0xC0 | ((c1 & 0x07) << 2) | ((c0 & 0xC0) >> 6));
				r += (0x80 | ((c0 & 0x3F) >> 0));
				}
			else {
				r += (0xE0 | ((c1 & 0xF0) >> 4));
				r += (0x80 | ((c1 & 0x0F) << 2) | ((c0 & 0xC0) >> 6));
				r += (0x80 | ((c0 & 0x3F) >> 0));
				}
			}
		}

	return r;
	}

CUnicode UtfConvert(PCTXT p)
{
	CUnicode r;

	while( *p ) {

		if( p[0] & 0x80 ) {

			if( (p[0] & 0xE0) == 0xC0 ) {

				WCHAR c = ((p[1] & 0x3F) << 0) |
					  ((p[0] & 0x1F) << 6) ;

				r += c;

				p += 2;
				}

			if( (p[0] & 0xF0) == 0xE0 ) {

				WCHAR c = ((p[2] & 0x3F) <<  0) |
					  ((p[1] & 0x3F) <<  6) |
					  ((p[0] & 0x0F) << 12) ;

				r += c;

				p += 3;
				}

			if( (p[0] & 0xF8) == 0xF0 ) {

				p += 4;
				}
			}
		else {
			r += WCHAR(p[0]);

			p += 1;
			}
		}

	return r;
	}

CString UniConvert(PCUTF p)
{
	CString r;

	while( *p ) {

		WCHAR c = *p++;

		if( HIBYTE(c) ) {

			r += '&';
			
			r += '#';

			r += '0' + (c / 10000) % 10;
			r += '0' + (c / 1000 ) % 10;
			r += '0' + (c / 100  ) % 10;
			r += '0' + (c / 10   ) % 10;
			r += '0' + (c / 1    ) % 10;

			r += ';';

			continue;
			}

		r += char(c);
		}

	return r;
	}

CUnicode UniConvert(PCTXT p)
{
	CUnicode r;

	while( *p ) {

		BYTE c = BYTE(*p++);

		if( c == '&' ) {
			
			if( p[0] == '#' && p[6] == ';' ) {

				r += WCHAR(atoi(p + 1));

				p += 7;

				continue;
				}
			}

		r += c;
		}

	return r;
	}

// End of File
