
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////////////
//	
// GcCore Class Library
//
// Copyright (c) 2018 Granby Consulting LLC
//
// All Rights Reserved
//

#include "Hex.hpp"

////////////////////////////////////////////////////////////////////////////////
//	
// Employed Classes
//

#include "String.hpp"

#include "Array.hpp"

////////////////////////////////////////////////////////////////////////////////
//	
// Hex Encoder
//

CString CHex::ToHex(PCBYTE p, size_t n, int enc)
{
	CString r;

	r.reserve(GetEncodeSize(n));

	PCSTR h = GetList(enc);

	while( n-- ) {

		r += h[(*p >> 4) & 15];

		r += h[(*p >> 0) & 15];

		p++;
	}

	return r;
}

CString CHex::ToHex(CByteArray const &d, int enc)
{
	PCBYTE const p = PCBYTE(d.data());

	size_t const n = d.size();

	return ToHex(p, n, enc);
}

CString CHex::ToHex(CString const &s, int enc)
{
	PCBYTE const p = PCBYTE(s.data());

	size_t const n = s.size();

	return ToHex(p, n, enc);
}

CByteArray CHex::ToBytes(CString const &s)
{
	CByteArray d;

	Decode(d, s, true);

	return d;
}

CString CHex::ToAnsi(CString const &s)
{
	CString r;

	Decode(r, s, false);

	return r;
}

// Decode Helper

template<typename dtype> bool CHex::Decode(dtype &d, CString const &s, bool z)
{
	d.clear();

	d.reserve(GetDecodeSize(s.size()));

	char const *p = s;

	while( p[0] && p[1] ) {

		BYTE b = 0;

		if( p[0] >= '0' && p[0] <= '9' ) {

			b |= ((p[0] - '0') +  0) << 4;
		}

		if( p[0] >= 'a' && p[0] <= 'f' ) {

			b |= ((p[0] - 'a') + 10) << 4;
		}

		if( p[0] >= 'A' && p[0] <= 'F' ) {

			b |= ((p[0] - 'A') + 10) << 4;
		}

		if( p[1] >= '0' && p[1] <= '9' ) {

			b |= ((p[1] - '0') +  0) << 0;
		}

		if( p[1] >= 'a' && p[1] <= 'f' ) {

			b |= ((p[1] - 'a') + 10) << 0;
		}

		if( p[1] >= 'A' && p[1] <= 'F' ) {

			b |= ((p[1] - 'A') + 10) << 0;
		}

		if( b || z ) {

			d += (typename dtype::value_type)(b);
		}

		p += 2;
	}

	return true;
}

// Size Estimation

STRONG_INLINE size_t CHex::GetEncodeSize(size_t s)
{
	return 2 * s;
}

STRONG_INLINE size_t CHex::GetDecodeSize(size_t s)
{
	return s / 2;
}

// Encoding List

char const * CHex::GetList(int enc)
{
	if( enc == encLower ) {

		return "0123456789abcdef";
	}

	if( enc == encUpper ) {

		return "0123456789ABCDEF";
	}

	return nullptr;
}

// End of File
