
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IExpansion_HPP

#define INCLUDE_IExpansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 22 -- Expansion Interfaces
//

// https://

interface IExpansionEvents;
interface IExpansionInterface;
interface IExpansionFirmware;

//////////////////////////////////////////////////////////////////////////
//
// Referenced Types 
//

union UsbTreePath;

//////////////////////////////////////////////////////////////////////////
//
// Referenced Interfaces 
//

interface IUsbDevice;
interface IUsbHostFuncDriver;
interface IUsbSystemPower;

//////////////////////////////////////////////////////////////////////////
//
// Module Classes
//

enum
{
	rackClassNone		= 0,
	rackClassRack		= 1,
	rackClassComms		= 2,
	rackClassNetwork	= 3,
	rackClassLicense	= 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// Port Types
//

enum
{
	rackNone		= 0,
	rackSerial		= 1,
	rackNetwork		= 2,
	rackCan			= 4,
	rackProfibus		= 5,
	rackFireWire		= 6,
	rackDevNet		= 7,
	rackCatLink		= 8,
	rackMpi			= 9,
	rackRack		= 10,
	rackJ1939		= 11,
	rackTest		= 12,
	};

//////////////////////////////////////////////////////////////////////////
//
// System Interface
//

interface IExpansionSystem : public IUnknown
{
	AfxDeclareIID(22, 1);

	virtual void		      METHOD Attach(IExpansionEvents *pSink)	  = 0;
	virtual void		      METHOD Attach(IExpansionFirmware *pFirm)	  = 0;
	virtual void		      METHOD Attach(IUsbSystemPower *pPower)	  = 0;
	virtual UINT                  METHOD GetSlotCount(void)			  = 0;
	virtual UINT		      METHOD FindSlot(UsbTreePath const &Path)	  = 0;
	virtual BOOL 		      METHOD WaitSlot(UINT iSlot, UINT uTimeout)  = 0;
	virtual IUsbDevice          * METHOD GetDevice(UINT iSlot)		  = 0;
	virtual IExpansionInterface * METHOD GetInterface(UINT iSlot, UINT iIdx)  = 0;
	virtual IUsbHostFuncDriver  * METHOD GetFuncDriver(UINT iSlot, UINT iIdx) = 0; 
	virtual PVOID                 METHOD GetAppObject(UINT iSlot, UINT iIdx)  = 0;
	virtual void		      METHOD FreeAppObject(UINT iSlot, UINT iIdx) = 0;
	virtual void		      METHOD FreeAppObjects(void)		  = 0;
	virtual PVOID                 METHOD GetSysObject(UINT iSlot, UINT iIdx)  = 0;
	virtual void		      METHOD FreeSysObject(UINT iSlot, UINT iIdx) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// System Events
//

interface IExpansionEvents : public IUnknown
{
	AfxDeclareIID(22, 2);

	virtual void METHOD OnDeviceArrival(UINT iSlot, UINT iInterface) = 0;
	virtual void METHOD OnDeviceRemoval(UINT iSlot, UINT iInterface) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Expansion Plug n Play 
//

interface IExpansionPnp : public IUnknown
{
	AfxDeclareIID(22, 3);

	virtual void METHOD OnDeviceArrival(IUsbHostFuncDriver *pDriver) = 0;
	virtual void METHOD OnDeviceRemoval(IUsbHostFuncDriver *pDriver) = 0;
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Expansion Interface
//

interface IExpansionInterface : public IUnknown
{
	AfxDeclareIID(22, 4);

	virtual PCTXT		METHOD GetName(void)			    = 0;
	virtual UINT		METHOD GetIdent(void)			    = 0;
	virtual UINT		METHOD GetClass(void)			    = 0;
	virtual UINT		METHOD GetIndex(void)			    = 0;
	virtual	UINT		METHOD GetPower(void)			    = 0;
	virtual BOOL		METHOD HasBootLoader(void)		    = 0;
	virtual IDevice       * METHOD MakeObject(IUsbHostFuncDriver *pDrv) = 0;
	virtual IExpansionPnp * METHOD QueryPnpInterface(IDevice *pObj)	    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Expansion Serial Interface
//

interface IExpansionSerial : public IExpansionInterface
{
	AfxDeclareIID(22, 5);

	virtual	UINT  METHOD GetPortCount(void)	     = 0;
	virtual DWORD METHOD GetPortMask(void)	     = 0;
	virtual UINT  METHOD GetPortType(UINT iPort) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Expansion Rack Interface
//

interface IExpansionRack : public IExpansionInterface
{
	AfxDeclareIID(22, 6);

	virtual void METHOD RegisterPower(IExpansionInterface *pInterface)   = 0;
	virtual void METHOD UnregisterPower(IExpansionInterface *pInterface) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Firmware Interface
//

interface IExpansionFirmware : public IUnknown
{
	AfxDeclareIID(22, 7);

	virtual BOOL   METHOD SetFirmwareLock(BOOL fSet)  = 0;
	virtual PCBYTE METHOD GetFirmwareGuid(UINT uFirm) = 0;
	virtual UINT   METHOD GetFirmwareSize(UINT uFirm) = 0;
	virtual PCBYTE METHOD GetFirmwareData(UINT uFirm) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Port Object Interface
//

interface IDeviceNetPort : public IPortObject
{
	AfxDeclareIID(22, 8);

	virtual void METHOD SetMac(BYTE bMac)				    = 0;
	virtual BOOL METHOD EnableBitStrobe(WORD wRecvSize, WORD wSendSize) = 0;
	virtual BOOL METHOD EnablePolled(WORD wRecvSize, WORD wSendSize)    = 0;
	virtual BOOL METHOD EnableData(WORD wRecvSize, WORD wSendSize)	    = 0;
	virtual BOOL METHOD IsOnLine(void)				    = 0;
	virtual UINT METHOD Read(UINT uId, PBYTE pData, UINT uSize)	    = 0;
	virtual UINT METHOD Write(UINT uId, PCBYTE pData, UINT uSize)	    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Profibus Port Object
//

interface IProfibusPort : public IPortObject
{
	AfxDeclareIID(22, 9);

	virtual	void  METHOD Poll(void)				    = 0;
	virtual BOOL  METHOD IsOnline(void)			    = 0;
	virtual UINT  METHOD GetOutputSize(void)		    = 0;
	virtual UINT  METHOD GetInputSize(void)			    = 0;
	virtual PBYTE METHOD GetOutputBuffer(void)		    = 0;
	virtual BOOL  METHOD PutInputData(PCBYTE pData, UINT uSize) = 0;
	};

// End of File

#endif
