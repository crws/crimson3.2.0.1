
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_PlatWin32_HPP

#define INCLUDE_PlatWin32_HPP

//////////////////////////////////////////////////////////////////////////
//
// Platform Flag
//

#define AEON_PLAT_WIN32

//////////////////////////////////////////////////////////////////////////
//
// Environment Check
//

#if !defined(_MSC_VER) || !defined(_M_IX86)

#error "Win32 platform expects MSVC and X86."

#endif

//////////////////////////////////////////////////////////////////////////
//
// Linux API Access
//

#if defined(AEON_NEED_LINUX)

#error "Linux API access not available from Windows."

#endif

//////////////////////////////////////////////////////////////////////////
//
// Source Annotation Avoidance
//

#if defined(AEON_NEED_WIN32)

#define  _M_CEE_SAFE

#include <CodeAnalysis\sourceannotations.h>

#undef   _M_CEE_SAFE

#undef   __volatile

#endif

//////////////////////////////////////////////////////////////////////////
//
// Host Macros
//

#define HostBreak()	ProcBreak()

#define	HostTrap(x)	ProcTrap(x)

#define HostTrace()	ProcBreak()

//////////////////////////////////////////////////////////////////////////
//
// Useful Macros
//

#if 1 || !defined(AEON_NEED_WIN32)

#define MAX_PATH	260

#endif

//////////////////////////////////////////////////////////////////////////
//
// Windows API Access
//

#if defined(AEON_NEED_WIN32)

#define	_WIN32_WINNT		_WIN32_WINNT_WIN7

#define	WINVER			_WIN32_WINNT_WIN7

#define WIN32_LEAN_AND_MEAN	TRUE

#define	NO_STRICT		TRUE

AfxNamespaceBegin(win32);

#undef   timerclear

#undef   timercmp

#include <WinSock2.h>

#include <Windows.h>

#include <WinIoctl.h>

#include <Unknwn.h>

#undef   GetObject

AfxNamespaceEnd(win32);

#endif

//////////////////////////////////////////////////////////////////////////
//
// Windows API Libraries
//

#if defined(AEON_NEED_WIN32)

#pragma comment(lib, "kernel32")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Typedef Helpers
//

#if defined(AEON_NEED_WIN32)

// Windows defines TYPE, PTYPE and LPTYPE.

#define	AfxImportTypeBoth(name, type)		\
						\
	using   win32::name;			\
	using   win32::P##name;			\
	using   win32::LP##name;		\
	typedef type const    * PC##name;	\
	typedef type volatile * PV##name;	\
	typedef type const    * LPC##name;	\
	typedef type volatile * LPV##name;	\

// Windows defines TYPE and PTYPE.

#define	AfxImportTypeNear(name, type)		\
						\
	using   win32::name;			\
	using   win32::P##name;			\
	typedef type const    * PC##name;	\
	typedef type volatile * PV##name;	\
	typedef type          * LP##name;	\
	typedef type const    * LPC##name;	\
	typedef type volatile * LPV##name;	\

// Windows defines LPTYPE and LPCTYPE.

#define	AfxImportTypeLong(name, type)		\
						\
	using   win32::LP##name;		\
	using   win32::LPC##name;		\
	typedef type            name;		\
	typedef type          * P##name;	\
	typedef type const    * PC##name;	\
	typedef type volatile * PV##name;	\
	typedef type volatile * LPV##name;	\

// Windows defines LPTYPE and LPCTYPE.

#define	AfxImportPtrsLong(name, type)		\
						\
	using   win32::LP##name;		\
	using   win32::LPC##name;		\
	typedef type          * P##name;	\
	typedef type const    * PC##name;	\
	typedef type volatile * PV##name;	\
	typedef type volatile * LPV##name;	\

#endif

//////////////////////////////////////////////////////////////////////////
//
// Imported Type Definitions
//

#if defined(AEON_NEED_WIN32)

#undef VOID

AfxImportTypeNear(CHAR,  CHAR);
AfxImportTypeNear(SHORT, SHORT);
AfxImportTypeNear(ULONG, ULONG);
AfxImportTypeBoth(INT,   INT);
AfxImportTypeBoth(LONG,  LONG);
AfxImportTypeBoth(BYTE,  BYTE);
AfxImportTypeBoth(UINT,  UINT);
AfxImportTypeBoth(WORD,  WORD);
AfxImportTypeBoth(DWORD, DWORD);
AfxImportTypeBoth(BOOL,  BOOL);
AfxImportTypeLong(VOID,  void);
AfxImportPtrsLong(STR,   char);

using win32::DWORD_PTR;

using win32::LONG_PTR;

#endif

//////////////////////////////////////////////////////////////////////////
//
// Imported COM Support
//

#if defined(AEON_NEED_WIN32)

#define AEON_COM_DEFINED

using   win32::IUnknown;

using   win32::GUID;

using   win32::IID;

using   win32::HRESULT;

#endif

// End of File

#endif
