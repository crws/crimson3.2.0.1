
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IFileCopier_HPP

#define INCLUDE_IFileCopier_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 25 -- File Copier
//

// https://

interface IFileCopier;

//////////////////////////////////////////////////////////////////////////
//
// File Copier Flags
//

enum FileCopy
{
	copyNew			= Bit(0),
	copyDiff		= Bit(1),
	copyAll			= Bit(2),
	copyRecursive		= Bit(3),
	copyOverwrite		= Bit(4),
	copyReserved		= Bit(31),
	};

////////////////////////////////////////////////////////////////////////
//
// File Copier Interface
//

interface IFileCopier : public IUnknown
{
	AfxDeclareIID(25, 1);

	virtual BOOL CopyFile(PCTXT pSrc, PCTXT pDir, UINT uFlags)	= 0;
	virtual BOOL MoveFile(PCTXT pSrc, PCTXT pDir, UINT uFlags)	= 0;
	virtual BOOL CopyDir (PCTXT pSrc, PCTXT pDir, UINT uFlags)	= 0;
	virtual BOOL MoveDir (PCTXT pSrc, PCTXT pDir, UINT uFlags)	= 0;
	};

// End of File

#endif
