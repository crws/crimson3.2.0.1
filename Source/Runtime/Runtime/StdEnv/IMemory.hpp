
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IMemory_HPP

#define INCLUDE_IMemory_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 15 -- Memory Devices
//

interface INandMemory;
interface ISerialMemory;
interface ISdHost;

//////////////////////////////////////////////////////////////////////////
//
// Geometry
//

struct CNandGeometry
{
	UINT m_uChips;
	UINT m_uBlocks;
	UINT m_uPages;
	UINT m_uSectors;
	UINT m_uBytes;
	};

//////////////////////////////////////////////////////////////////////////
//
// Nand Memory Interface
//

interface INandMemory : public IDevice
{
	// https://

	AfxDeclareIID(15, 1);

	virtual BOOL METHOD GetGeometry(CNandGeometry &Geom)						= 0;
	virtual BOOL METHOD GetUniqueId(UINT uChip, PBYTE pData)					= 0;
	virtual BOOL METHOD IsBlockBad(UINT uChip, UINT uBlock)						= 0;
	virtual BOOL METHOD MarkBlockBad(UINT uChip, UINT uBlock)					= 0;
	virtual BOOL METHOD EraseBlock(UINT uChip, UINT uBlock)						= 0;
	virtual BOOL METHOD WriteSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PCBYTE pData)	= 0;
	virtual BOOL METHOD ReadSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PBYTE pData)	= 0;
	virtual BOOL METHOD WritePage(UINT uChip, UINT uBlock, UINT uPage, PCBYTE pData)		= 0;
	virtual BOOL METHOD ReadPage(UINT uChip, UINT uBlock, UINT uPage, PBYTE pData)			= 0;
	};

/////////////////////////////////////////////////////////////////////////
//
// SD Host Memory Interface
//

interface ISdHost : public IDevice
{
	// https://

	AfxDeclareIID(15, 2);

	virtual BOOL METHOD IsCardReady(void)			  = 0;
	virtual UINT METHOD GetSectorCount(void)		  = 0;
	virtual UINT METHOD GetSerialNumber(void)		  = 0;
	virtual void METHOD AttachCardEvent(IEvent *pEvent)	  = 0;
	virtual BOOL METHOD WriteSector(UINT uSect, PCBYTE pData) = 0;
	virtual BOOL METHOD ReadSector(UINT uSect, PBYTE pData)   = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Serial Memory Interface
//

interface ISerialMemory : public IDevice
{
	// https:

	AfxDeclareIID(15, 3);

	virtual UINT METHOD GetSize(void) = 0;
	virtual BOOL METHOD IsFast(void)  = 0;
	virtual BOOL METHOD GetData(UINT uAddr, PBYTE  pData, UINT uCount) = 0;
	virtual BOOL METHOD PutData(UINT uAddr, PCBYTE pData, UINT uCount) = 0;
	};

// End of File

#endif
