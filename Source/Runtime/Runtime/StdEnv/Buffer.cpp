
#include "Intern.hpp"

#include "Buffer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Dynamic Buffer
//

// Constructor

CBuffer::CBuffer(IBufferManager *pManager, UINT uAlloc)
{
	m_uInit    = 256;

	m_uSize    = 0;

	m_uAlloc   = uAlloc;

	m_uFlags   = 0;

	m_pManager = pManager;

	m_pData    = New BYTE [ m_uAlloc ];

	m_pNext    = NULL;

	m_pPrev    = NULL;

	m_nRefs    = 0;
	}

// End of File
