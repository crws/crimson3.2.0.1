
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IZLib_HPP

#define INCLUDE_IZLib_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 24 -- ZLib Compression
//

interface IZLib;

//////////////////////////////////////////////////////////////////////////
//
// ZLib Support
//

interface IZLib : public IUnknown
{
	// https://

	AfxDeclareIID(24, 1);
	
	virtual UINT METHOD Expand  (PCBYTE pIn, UINT uIn, PBYTE pOut, UINT uOut) = 0;
	virtual UINT METHOD Compress(PCBYTE pIn, UINT uIn, PBYTE pOut, UINT uOut) = 0;
	};

// End of File

#endif
