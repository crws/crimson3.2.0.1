
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Buffer_HPP

#define INCLUDE_Buffer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CBuffer;

//////////////////////////////////////////////////////////////////////////
//
// Buffer APIs
//

extern CBuffer * BuffAllocate(UINT uSize);

//////////////////////////////////////////////////////////////////////////
//
// Buffer Macros
//

#define	BuffRelease(p)		(p)->Release()

#define	BuffStripHead(p, n)	((n *) (p)->StripHead(sizeof(n)))

#define	BuffStripTail(p, n)	((n *) (p)->StripTail(sizeof(n)))

#define	BuffAddHead(p, n)	((n *) (p)->AddHead(sizeof(n)))

#define	BuffAddTail(p, n)	((n *) (p)->AddTail(sizeof(n)))

#define BuffGetData(p, n)	((n *) (p)->GetData())

#define BuffCheckSize(p, n)	((p)->GetSize() >= sizeof(n))

////////////////////////////////////////////////////////////////////////
//
// Dynamic Buffer
//

class DLLAPI CBuffer
{
	public:
		// Constructor
		CBuffer(IBufferManager *pManager, UINT uAlloc);

		// Attributes
		STRONG_INLINE UINT  GetInit(void) const;
		STRONG_INLINE UINT  GetSize(void) const;
		STRONG_INLINE UINT  GetHeadSpace(void) const;
		STRONG_INLINE UINT  GetTailSpace(void) const;
		STRONG_INLINE bool  TestFlag(UINT uMask) const;
		STRONG_INLINE UINT  GetFlags(void) const;
		STRONG_INLINE PBYTE GetData(void);

		// Operations
		STRONG_INLINE void  AddRef(void);
		STRONG_INLINE void  Release(void);
		STRONG_INLINE void  Clear(void);
		STRONG_INLINE void  SetInit(UINT uInit);
		STRONG_INLINE void  SetFlags(UINT uFlags);
		STRONG_INLINE void  SetFlag(UINT uMask, bool fState);
		STRONG_INLINE void  SetFlag(UINT uMask);
		STRONG_INLINE void  ClearFlag(UINT uMask);
		STRONG_INLINE PBYTE StripHead(UINT uSize);
		STRONG_INLINE PBYTE StripTail(UINT uSize);
		STRONG_INLINE PBYTE AddHead(UINT uSize);
		STRONG_INLINE PBYTE AddTail(UINT uSize);

		// Duplication
		STRONG_INLINE CBuffer * MakeCopy(void);

	protected:
		// Static Data
		static char const m_ErrHead[];
		static char const m_ErrTail[];

		// Primary Data
		PBYTE		 m_pData;
		UINT		 m_uInit;
		UINT		 m_uSize;
		UINT		 m_uAlloc;
		UINT		 m_uFlags;
		IBufferManager * m_pManager;

	public:
		// Linked List
		CBuffer	* m_pNext;
		CBuffer	* m_pPrev;
		INT	  m_nRefs;
	};

////////////////////////////////////////////////////////////////////////
//
// Dynamic Buffer
//

// Attributes

STRONG_INLINE UINT CBuffer::GetInit(void) const
{
	return m_uInit;
	}

STRONG_INLINE UINT CBuffer::GetSize(void) const
{
	return m_uSize;
	}

STRONG_INLINE UINT CBuffer::GetHeadSpace(void) const
{
	return m_uInit;
	}

STRONG_INLINE UINT CBuffer::GetTailSpace(void) const
{
	return m_uAlloc - m_uInit - m_uSize;
	}

STRONG_INLINE bool CBuffer::TestFlag(UINT uMask) const
{
	return (m_uFlags & uMask) ? true : false;
	}

STRONG_INLINE UINT CBuffer::GetFlags(void) const
{
	return m_uFlags;
	}

STRONG_INLINE PBYTE CBuffer::GetData(void)
{
	return m_pData + m_uInit;
	}

// Operations

STRONG_INLINE void CBuffer::AddRef(void)
{
	AtomicIncrement(&m_nRefs);
	}

STRONG_INLINE void CBuffer::Release(void)
{
	m_pManager->MarkFree(this);
	}

STRONG_INLINE void CBuffer::Clear(void)
{
	m_uInit  = 256;

	m_uSize  = 0;

	m_uFlags = 0;

	m_nRefs  = 1;
	}

STRONG_INLINE void CBuffer::SetInit(UINT uInit)
{
	m_uSize = m_uSize + m_uInit - uInit;

	m_uInit = uInit;
	}

STRONG_INLINE void CBuffer::SetFlags(UINT uFlags)
{
	m_uFlags = uFlags;
	}

STRONG_INLINE void CBuffer::SetFlag(UINT uMask, bool fState)
{
	fState ? SetFlag(uMask) : ClearFlag(uMask);
	}

STRONG_INLINE void CBuffer::SetFlag(UINT uMask)
{
	m_uFlags |= uMask;
	}

STRONG_INLINE void CBuffer::ClearFlag(UINT uMask)
{
	m_uFlags &= ~uMask;
	}

STRONG_INLINE PBYTE CBuffer::StripHead(UINT uSize)
{
	PBYTE pData = GetData();

	m_uInit += uSize;

	m_uSize -= uSize;

	return pData;
	}

STRONG_INLINE PBYTE CBuffer::StripTail(UINT uSize)
{
	m_uSize -= uSize;

	return GetData();
	}

STRONG_INLINE PBYTE CBuffer::AddHead(UINT uSize)
{
	# if 0

	if( GetHeadSpace() >= uSize ) {

		m_uInit -= uSize;

		m_uSize += uSize;

		return GetData();
		}

	m_pManager->ShowError(this, m_ErrHead);

	return m_pData;

	#else
	
	m_uInit -= uSize;

	m_uSize += uSize;

	return GetData();

	#endif
	}

STRONG_INLINE PBYTE CBuffer::AddTail(UINT uSize)
{
	#if 0

	if( GetTailSpace() >= uSize ) {

		PBYTE pData = GetData() + m_uSize;

		m_uSize += uSize;

		return pData;
		}

	m_pManager->ShowError(this, m_ErrTail);

	return m_pData;

	#else

	PBYTE pData = GetData() + m_uSize;

	m_uSize += uSize;

	return pData;

	#endif
	}

// Duplication

STRONG_INLINE CBuffer * CBuffer::MakeCopy(void)
{
	CBuffer *pCopy = BuffAllocate(m_uSize);

	if( pCopy ) {

		memcpy(pCopy->AddTail(m_uSize), m_pData + m_uInit, m_uSize);

		return pCopy;
		}

	return NULL;
	}

// End of File

#endif
