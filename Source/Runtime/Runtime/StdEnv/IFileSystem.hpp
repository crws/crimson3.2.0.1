
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_IFileSystem_HPP

#define INCLUDE_IFileSystem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 18 -- File System Interfaces
//

// https://

interface IFileManager;
interface IDiskManager;
interface IVolumeManager;
interface IFilingSystem;
interface IFile;
interface IBlockCache;
interface IBlockDevice;
class	  CFilename;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define FILE_NAME_MAX	256

#define FILE_PATH_MAX	1024

//////////////////////////////////////////////////////////////////////////
//
// File Mode
//

enum FileMode
{
	fileNone		= 0,
	fileRead		= Bit(0),
	fileWrite		= Bit(1),
	fileCreate		= Bit(2),
	fileShareRead		= Bit(3),
	fileShareWrite		= Bit(4),
	};

//////////////////////////////////////////////////////////////////////////
//
// Disk Lock
//

enum DiskLock
{
	diskLockNone		= 0,
	diskLockRead		= Bit(0),
	diskLockWrite		= Bit(1),
	};

//////////////////////////////////////////////////////////////////////////
//
// Disk Status
//

enum DiskStatus
{
	diskEmpty		= 0,
	diskInvalid		= 1,
	diskCheck		= 2,
	diskFormat		= 3,
	diskLocked		= 4,
	diskReady		= 5,
	};

//////////////////////////////////////////////////////////////////////////
//
// Disk Status Flags
//

enum DiskStatusFlags
{
	flagEmpty		= Bit(0),
	};

//////////////////////////////////////////////////////////////////////////
//
// Utility Flags
//

enum ScanFlags
{
	scanOrder		= Bit(0),
	scanReverse		= Bit(1),
	scanAll			= Bit(2),
	scanFile		= Bit(3),
	scanDir			= Bit(4),
	};

//////////////////////////////////////////////////////////////////////////
//
// Cache Policy
//

enum CachePolicy
{
	cacheWriteBack,
	cacheWriteThrough,
	};

//////////////////////////////////////////////////////////////////////////
//
// File Posititioning 
//

enum FileSeek
{
	seekBeg,
	seekCur,
	seekEnd,
	};

//////////////////////////////////////////////////////////////////////////
//
// File System Index
//

struct FSIndex
{
	UINT m_iCluster;
	UINT m_iIndex;
	};

//////////////////////////////////////////////////////////////////////////
//
// File Handle
//

typedef void *	HFILE;

////////////////////////////////////////////////////////////////////////
//
// File Management
//

interface IFileManager : public IUnknown
{
	AfxDeclareIID(18, 1);

	virtual IFilingSystem * METHOD FindFileSystem(void)					= 0;
	virtual IFilingSystem * METHOD FindFileSystem(CHAR cDrive)				= 0;
	virtual IFilingSystem * METHOD FindFileSystem(UINT iVolume)				= 0;
	virtual IFilingSystem * METHOD LockFileSystem(void)					= 0;
	virtual IFilingSystem * METHOD LockFileSystem(CHAR cDrive)				= 0;
	virtual IFilingSystem * METHOD LockFileSystem(UINT iVolume)				= 0;
	virtual BOOL            METHOD LockFileSystem(IFilingSystem *pFileSys)			= 0;
	virtual void		METHOD FreeFileSystem(IFilingSystem *pFileSys, BOOL fLocked)	= 0;
	virtual BOOL		METHOD RegisterTask(IFilingSystem *pFileSys)			= 0;
	virtual BOOL		METHOD UnregisterTask(IFilingSystem *pFileSys)			= 0;
	virtual void		METHOD Purge(UINT uTask)					= 0;
	};

////////////////////////////////////////////////////////////////////////
//
// Disk Management
//

interface IDiskManager : public IUnknown
{
	AfxDeclareIID(18, 2);

	virtual UINT           METHOD RegisterHost(UINT iDisk)					    = 0;
	virtual void           METHOD UnregisterHost(UINT iDisk, UINT iHost)			    = 0;
	virtual UINT	       METHOD RegisterDisk(IBlockDevice *pDevice)			    = 0;
	virtual UINT	       METHOD FindDisk(CHAR cDrive)					    = 0;
	virtual UINT	       METHOD FindVolume(CHAR cDrive)					    = 0;
	virtual bool	       METHOD CheckDisk(UINT iDisk)					    = 0;
	virtual bool	       METHOD EjectDisk(UINT iDisk)					    = 0;
	virtual bool	       METHOD FormatDisk(UINT iDisk)					    = 0;
	virtual UINT           METHOD GetDiskCount(void)					    = 0;
	virtual IBlockDevice * METHOD GetDisk(UINT iDisk)					    = 0;
	virtual IBlockCache  * METHOD GetDiskCache(UINT iDisk)					    = 0;
	virtual bool	       METHOD GetDiskReady(UINT iDisk)					    = 0;
	virtual UINT	       METHOD GetDiskStatus(UINT iDisk)					    = 0;
	virtual UINT           METHOD GetVolumeCount(UINT iDisk)				    = 0;
	virtual UINT	       METHOD GetVolumeIndex(UINT iDisk, UINT n)			    = 0;
	virtual bool           METHOD WaitDiskLock(UINT iHost, UINT iDisk, UINT uType, UINT uWait)  = 0;
	virtual bool           METHOD FreeDiskLock(UINT iHost, UINT iDisk, UINT uType)		    = 0;
	};

////////////////////////////////////////////////////////////////////////
//
// Volume Management
//

interface IVolumeManager : public IUnknown
{
	AfxDeclareIID(18, 3);

	virtual UINT		METHOD FormatVolume(UINT iDisk, CHAR cDrive, UINT uFirst, UINT uSize)	= 0;
	virtual UINT		METHOD MountVolume(UINT iDisk, CHAR cDrive, UINT uFirst)		= 0;
	virtual bool		METHOD UnMountVolume(UINT iVolume)					= 0;
	virtual bool 		METHOD UnMountVolumes(void)						= 0;
	virtual bool		METHOD IsVolumeMounted(UINT iVolume)					= 0;
	virtual CHAR		METHOD GetDriveLetter(UINT iVolume)					= 0;
	virtual IFilingSystem * METHOD GetFileSystem(void)						= 0;
	virtual IFilingSystem * METHOD GetFileSystem(UINT iVolume)					= 0;
	virtual IFilingSystem * METHOD GetTaskVolume(void)						= 0;
	virtual bool		METHOD SetTaskVolume(IFilingSystem *pFileSystem)			= 0;
	};

////////////////////////////////////////////////////////////////////////
//
// Filing System
//

interface IFilingSystem : public IUnknown
{
	AfxDeclareIID(18, 4);

	// Attributes
	virtual UINT  GetIdent(void)							    = 0;
	virtual UINT  GetIndex(void)							    = 0;
	virtual UINT  GetDisk(void)							    = 0;
	virtual CHAR  GetDrive(void)							    = 0;
	virtual INT64 GetSize(void)							    = 0;
	virtual INT64 GetFree(void)							    = 0;
	virtual INT64 GetUsed(void)							    = 0;
	
	// Methods
	virtual BOOL  Init(void)							    = 0;
	virtual BOOL  Start(void)							    = 0;
	virtual BOOL  Stop(void)							    = 0;
	virtual BOOL  IsMounted(void)							    = 0;
	virtual BOOL  WaitLock(UINT uWait)						    = 0;
	virtual BOOL  FreeLock(void)							    = 0;

	// Enumeration
	virtual BOOL   GetRoot(FSIndex &Index)						    = 0;
	virtual BOOL   GetFirst(FSIndex &Index)						    = 0;
	virtual BOOL   GetNext(FSIndex &Index)						    = 0;
	virtual BOOL   GetName(FSIndex const &Index, CFilename &Name)			    = 0;
	virtual BOOL   GetPath(FSIndex const &Index, CFilename &Name)			    = 0;
	virtual time_t GetUnixTime(FSIndex const &Index)				    = 0;
	virtual UINT   GetSize(FSIndex const &Index)					    = 0;
	virtual BOOL   IsRoot(FSIndex const &Index)					    = 0;
	virtual BOOL   IsValid(FSIndex const &Index)					    = 0;
	virtual BOOL   IsDirectory(FSIndex const &Index)				    = 0;
	virtual BOOL   IsFile(FSIndex const &Index)					    = 0;
	virtual BOOL   IsVolume(FSIndex const &Index)					    = 0;
	virtual BOOL   IsSpecial(FSIndex const &Index)					    = 0;
	virtual BOOL   GetArchive(FSIndex const &Index)					    = 0;
	
	// Directory
	virtual BOOL LocateDir(CFilename const &Name, FSIndex &Index)			    = 0;
	virtual BOOL LocateDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)   = 0;
	virtual BOOL CreateDir(CFilename const &Name)					    = 0;
	virtual BOOL CreateDir(CFilename const &Name, FSIndex const &Dir)		    = 0;
	virtual BOOL CreateDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)   = 0;
	virtual BOOL ChangeDir(CFilename const &Name)					    = 0;
	virtual BOOL ChangeDir(CFilename const &Name, FSIndex const &Dir)		    = 0;
	virtual BOOL ChangeDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)   = 0;
	virtual BOOL ChangeDir(FSIndex const &Dir, FSIndex &Index)			    = 0;
	virtual BOOL MoveDir(CFilename const &Name, FSIndex &Index)			    = 0;
	virtual BOOL MoveDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)     = 0;
	virtual BOOL MoveDir(FSIndex const &Dir, FSIndex &Index)			    = 0;
	virtual UINT ScanDir(FSIndex *&pList, UINT uFlags)				    = 0;
	virtual UINT ScanDir(CFilename const &Name, FSIndex *&pList, UINT uFlags)	    = 0;
	virtual UINT ScanDir(FSIndex const &Dir, FSIndex *&pList, UINT uFlags)		    = 0;
	virtual	BOOL RenameDir(CFilename const &From, CFilename const &To)		    = 0;
	virtual	BOOL RenameDir(CFilename const &Name, FSIndex &Index)			    = 0; 
	virtual BOOL EmptyDir(CFilename const &Name)					    = 0;
	virtual BOOL EmptyDir(CFilename const &Name, FSIndex const &Dir)		    = 0;
	virtual BOOL EmptyDir(FSIndex const &Index)					    = 0;
	virtual BOOL RemoveDir(CFilename const &Name)					    = 0;
	virtual BOOL RemoveDir(FSIndex const &Index)					    = 0;
	virtual UINT ParentDir(FSIndex const &Index)					    = 0;
	virtual BOOL CurrentDir(FSIndex &Index)						    = 0;

	// File
	virtual BOOL   LocateFile(CFilename const &Name, FSIndex &Index)	             = 0;
	virtual BOOL   LocateFile(CFilename const &Name, FSIndex const &Dir, FSIndex &Index) = 0;
	virtual HFILE  CreateFile(CFilename const &Name)			             = 0;
	virtual HFILE  CreateFile(CFilename const &Name, FSIndex const &Dir)		     = 0;
	virtual HFILE  OpenFile(CFilename const &Name, UINT Mode)			     = 0;
	virtual HFILE  OpenFile(CFilename const &Name, FSIndex const &Dir, UINT Mode)	     = 0;
	virtual HFILE  OpenFile(FSIndex const &Index, UINT Mode)		             = 0;
	virtual BOOL   CloseFile(HFILE hFile)						     = 0;
	virtual BOOL   DeleteFile(CFilename const &Name)			             = 0;
	virtual BOOL   DeleteFile(CFilename const &Name, FSIndex const &Dir)		     = 0;
	virtual BOOL   DeleteFile(FSIndex const &Index)					     = 0;
	virtual BOOL   DeleteFile(HFILE hFile)						     = 0;
	virtual BOOL   RenameFile(CFilename const &From, CFilename const &To)		     = 0;
	virtual BOOL   RenameFile(HFILE hFile, CFilename const &Name)			     = 0;
	virtual BOOL   FileSetLength(HFILE hFile, UINT uSize)				     = 0;
	virtual UINT   FileGetLength(HFILE hFile)					     = 0;
	virtual BOOL   FileSetUnixTime(HFILE hFile, time_t time)			     = 0;
	virtual time_t FileGetUnixTime(HFILE hFile)				             = 0;
	virtual BOOL   FileGetIndex(HFILE hFile, FSIndex &Index)		             = 0;
	virtual BOOL   FileAllocData(HFILE hFile, UINT uSize)				     = 0;
	virtual UINT   FileWriteData(HFILE hFile, PCBYTE pData, UINT uOffset, UINT uCount)   = 0;
	virtual UINT   FileReadData(HFILE hFile, PBYTE pData, UINT uOffset, UINT uCount)     = 0;

	// Synchronisation
	virtual BOOL  WaitReadLock(FSIndex const &Index)				    = 0;
	virtual BOOL  WaitWriteLock(FSIndex const &Index)				    = 0;
	virtual BOOL  FreeReadLock(FSIndex const &Index)				    = 0;
	virtual BOOL  FreeWriteLock(FSIndex const &Index)				    = 0;
	};										  

////////////////////////////////////////////////////////////////////////
//
// File Interface
//

interface IFile : public IUnknown
{
	AfxDeclareIID(18, 5);

	// Initialization
	virtual BOOL Attach(IFilingSystem *pFileSystem)					= 0;

	// Attributes
	virtual HFILE GetHandle(void)							= 0;
	virtual UINT  GetLength(void)							= 0;

	// Management
	virtual BOOL Create(CFilename const &Name)					= 0;
	virtual BOOL Create(CFilename const &Name, FSIndex const &Dir)			= 0;
	virtual BOOL Open(CFilename const &Name, UINT Mode)				= 0;
	virtual BOOL Open(CFilename const &Name, FSIndex const &Dir, UINT Mode)		= 0;
	virtual BOOL Open(FSIndex const &Index, UINT Mode)				= 0;
	virtual BOOL Close(void)							= 0;
	virtual BOOL Delete(void)							= 0;
	virtual BOOL SetLength(UINT uLength)						= 0;
	virtual BOOL Rename(CFilename const &Name)					= 0;

	// I/O
	virtual UINT   Read(PBYTE pData, UINT uCount)					= 0;
	virtual UINT   Write(PCBYTE pData, UINT uCount)					= 0;
	virtual BOOL   SetPos(INT nOffset, UINT Mode)					= 0;
	virtual BOOL   SetBeg(void)							= 0;
	virtual BOOL   SetEnd(void)							= 0;
	virtual UINT   GetPos(void)							= 0;
	virtual BOOL   SetUnixTime(time_t time)					        = 0;
	virtual time_t GetUnixTime(void)                                                = 0;
	};

////////////////////////////////////////////////////////////////////////
//
// Cached Block Device
//

interface IBlockCache : public IUnknown
{
	AfxDeclareIID(18, 7);

	virtual UINT  GetSectorCount(void)				= 0;
	virtual UINT  GetSectorSize(void)				= 0;
	virtual BOOL  Flush(void)					= 0;
	virtual void  Invalidate(void)					= 0;
	virtual PBYTE LockSector(UINT uSector, BOOL fSkipRead)		= 0;
	virtual BOOL  CommitSector(UINT uSector)			= 0;
	virtual BOOL  UnlockSector(UINT uSector, BOOL fDirty)		= 0;
	};

////////////////////////////////////////////////////////////////////////
//
// Block Device
//

interface IBlockDevice : public IDeviceEx
{
	AfxDeclareIID(18, 8);

	virtual BOOL IsReady(void)					= 0;
	virtual void Attach(IEvent *pEvent)				= 0;
	virtual void Arrival(PVOID pDevice)				= 0;
	virtual void Removal(void)					= 0;
	virtual UINT GetSectorCount(void)				= 0;
	virtual UINT GetSectorSize(void)				= 0;
	virtual UINT GetCylinderCount(void)				= 0;
	virtual UINT GetHeadCount(void)					= 0;
	virtual UINT GetSectorsPerHead(void)				= 0;
	virtual BOOL WriteSector(UINT uSector, PCBYTE pData)		= 0;
	virtual BOOL ReadSector(UINT uSector, PBYTE pData)		= 0;
	virtual void ClearCounters(void)				= 0;
	virtual UINT GetReadCount(void)					= 0;
	virtual UINT GetWriteCount(void)				= 0;
	};

// End of File

#endif
