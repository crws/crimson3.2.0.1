
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_ProcAM437_HPP

#define INCLUDE_ProcAM437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Processor Flags
//

#define AEON_PROC_AM437

#define AEON_PROC_Cortex

// End of File

#endif
