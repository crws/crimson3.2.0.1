
#include "Intern.hpp"

#include "IGraphics.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Rectangle Functions
//

BOOL IsRectEmpty(R2 const &Rect)
{
	return Rect.x1 >= Rect.x2 || Rect.y1 >= Rect.y2;
	}

void SetRectEmpty(R2 &Rect)
{
	Rect.x1 = 0;
	Rect.y1 = 0;
	Rect.x2 = 0;
	Rect.y2 = 0;
	}

void CombineRects(R2 &Result, R2 const &Rect1, R2 const &Rect2)
{
	if( IsRectEmpty(Rect1) ) {

		Result = Rect2;

		return;
		}

	if( IsRectEmpty(Rect2) ) {

		Result = Rect1;

		return;
		}

	Result.x1 = min(Rect1.x1, Rect2.x1);
	Result.x2 = max(Rect1.x2, Rect2.x2);
	Result.y1 = min(Rect1.y1, Rect2.y1);
	Result.y2 = max(Rect1.y2, Rect2.y2);
	}

void IntersectRects(R2 &Result, R2 const &Rect1, R2 const &Rect2)
{
	if( RectsIntersect(Rect1, Rect2) ) {

		Result.x1 = max(Rect1.x1, Rect2.x1);
		Result.x2 = min(Rect1.x2, Rect2.x2);
		Result.y1 = max(Rect1.y1, Rect2.y1);
		Result.y2 = min(Rect1.y2, Rect2.y2);

		return;
		}

	SetRectEmpty(Result);
	}

BOOL RectsEqual(R2 const &Rect1, R2 const &Rect2)
{
	return Rect1.x1 == Rect2.x1 &&
	       Rect1.y1 == Rect2.y1 &&
	       Rect1.x2 == Rect2.x2 &&
	       Rect1.y2 == Rect2.y2 ;
	}

BOOL RectsIntersect(R2 const &Rect1, R2 const &Rect2)
{
	return !( Rect2.x1 > Rect1.x2 ||
		  Rect2.x2 < Rect1.x1 ||
		  Rect2.y1 > Rect1.y2 ||
		  Rect2.y2 < Rect1.y1 );
        }

BOOL RectInRect(R2 const &Rect1, R2 const &Rect2)
{
	R2 Result;

	IntersectRects(Result, Rect1, Rect2);

	return RectsEqual(Result, Rect2);
	}

BOOL PtInRect(R2 const &Rect, P2 const &Point)
{
	if( Point.x < Rect.x1 || Point.x >= Rect.x2 ) {

		return FALSE;
		}

	if( Point.y < Rect.y1 || Point.y >= Rect.y2 ) {

		return FALSE;
		}

	return TRUE;
	}

void DeflateRect(R2 &Rect, int cx, int cy)
{
	Rect.x1 += cx;
	Rect.y1 += cy;
	Rect.x2 -= cx;
	Rect.y2 -= cy;
	}

void InflateRect(R2 &Rect, int cx, int cy)
{
	Rect.x1 -= cx;
	Rect.y1 -= cy;
	Rect.x2 += cx;
	Rect.y2 += cy;
	}

// End of File
