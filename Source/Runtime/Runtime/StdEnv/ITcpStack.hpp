
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ITcpStack_HPP

#define INCLUDE_ITcpStack_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 7 -- TCP/IP Stack Interfaces
//

interface IPacketSink;
interface IPacketDriver;
interface IProtocol;
interface IQueue;
interface IRouter;
interface IRecvSMS;
interface ISendSMS;

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

interface ISocket;

//////////////////////////////////////////////////////////////////////////
//
// Wrapper References
//

typedef class CPsHeader const & PSREF;

//////////////////////////////////////////////////////////////////////////
//
// PPP Connection Modes
//

enum
{
	pppClient     = 0,
	pppSafeClient = 1,
	pppServer     = 2,
};

//////////////////////////////////////////////////////////////////////////
//
// PPP Connection Types
//

enum
{
	connectDirect	 = 1,
	connectLandModem = 2,
	connectCellModem = 3,
	connectCellGPRS	 = 4,
	connectCellHSPA	 = 5,
	connectCellHSPA2 = 6,
};

//////////////////////////////////////////////////////////////////////////
//								
// Ethernet Driver Configuration
//

#pragma pack(1)

struct CConfigEth
{
	CString m_Display;
	CString m_DevName;
	UINT    m_DevInst;
	BOOL	m_fFast;
	BOOL	m_fFull;
	CIpAddr	m_Ip;
	CIpAddr	m_Mask;
	CIpAddr	m_Gate;
	WORD	m_SendMSS;
	WORD	m_RecvMSS;
};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//								
// PPP Driver Configuration
//

#pragma pack(1)

struct CConfigPpp
{
	CString		m_Display;
	CString		m_DevName;
	UINT		m_DevInst;
	UINT		m_uMode;
	UINT		m_uConnect;
	UINT		m_uDevice;
	UINT		m_uBaudRate;
	UINT		m_uTimeout;
	char		m_sInit[48];
	char		m_sDial[32];
	BOOL		m_fCHAP;
	char		m_sUser[32];
	char		m_sPass[16];
	BOOL		m_fGate;
	CIpAddr		m_RemAddr;
	CIpAddr		m_RemMask;
	CIpAddr		m_LocAddr;
	IRecvSMS *	m_pSMS;
	DWORD		m_InitMask;
	BOOL		m_fLogFile;
	UINT		m_uDynDNS;
	char		m_sDynUser[32];
	char		m_sDynPass[16];
	char		m_sDynHost[24];
	BYTE		m_bLocation;
	BYTE		m_bMore[43];
};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Packet Sink Interface
//

interface IPacketSink : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 4);

	virtual void OnLinkActive(UINT uFace)		      = 0;
	virtual void OnLinkDown(UINT uFace)		      = 0;
	virtual void OnPacket(UINT uFace, CBuffer *pBuff) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Packet Driver Interface
//

interface IPacketDriver : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 5);

	virtual bool    Bind(IRouter *pRouter)			 = 0;
	virtual bool    Bind(IPacketSink *pSink, UINT uFace)	 = 0;
	virtual CString GetDeviceName(void)			 = 0;
	virtual CString GetStatus(void)				 = 0;
	virtual bool    SetIpAddress(IPREF Ip, IPREF Mask)	 = 0;
	virtual bool	SetIpGateway(IPREF Gate)		 = 0;
	virtual bool    GetMacAddress(MACADDR &Addr)		 = 0;
	virtual bool    GetDhcpOption(BYTE bType, UINT &uValue)	 = 0;
	virtual bool    GetSockOption(UINT uCode, UINT &uValue)	 = 0;
	virtual bool    SetMulticast(IPADDR *pList, UINT uCount) = 0;
	virtual bool    Open(void)				 = 0;
	virtual bool    Close(void)				 = 0;
	virtual void    Poll(void)				 = 0;
	virtual bool    Send(IPREF IP, CBuffer *pBuff)		 = 0;
};

/////////////////////////////////////////////////////////////////////////////
//
// IP Protocol Interface
//

interface IProtocol : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 6);

	virtual BOOL Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask)	= 0;
	virtual BOOL CreateSocket(ISocket * &pSocket)				= 0;
	virtual BOOL Recv(UINT uFace, IPREF Face, PSREF PS, CBuffer *pBuff)	= 0;
	virtual BOOL Send(void)							= 0;
	virtual BOOL Poll(void)							= 0;
	virtual BOOL NetStat(IDiagOutput *pOut)					= 0;
	virtual BOOL SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff)		= 0;
};

////////////////////////////////////////////////////////////////////////////
//
// Queue Protocol Interface
//

interface IQueue : public IProtocol
{
	// LINK

	AfxDeclareIID(7, 7);

	virtual BOOL Queue(UINT uFace, IPREF Gate, CBuffer *pBuff) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// IP Router Interface
//

interface IRouter : public IUnknown
{
	// LINK

	AfxDeclareIID(7, 8);

	// Configuration
	virtual BOOL METHOD AddEthernet(CConfigEth const &Config)				           = 0;
	virtual BOOL METHOD AddPpp(CConfigPpp const &Config)						   = 0;
	virtual UINT METHOD AddInterface(IPacketDriver *pPacket, IPREF IP, IPREF Mask, BOOL fSafe)	   = 0;
	virtual UINT METHOD AddNullInterface(void)                                                         = 0;
	virtual BOOL METHOD AddLoopback(void)								   = 0;
	virtual void METHOD EnableRouting(BOOL fFarSide)						   = 0;

	// User APIs
	virtual BOOL	  METHOD Open(void)								   = 0;
	virtual BOOL	  METHOD Close(void)								   = 0;
	virtual ISocket * METHOD CreateSocket(WORD ID)							   = 0;

	// Protocol Calls
	virtual DWORD METHOD GetPseudoSum(CIpAddr Dest, CIpAddr From, WORD Prot, CBuffer *pBuff)	   = 0;
	virtual BOOL  METHOD Send(CIpAddr Dest, CIpAddr From, WORD Prot, CBuffer *pBuff, IProtocol *pProt) = 0;
	virtual BOOL  METHOD Send(UINT uFace, IPREF Gate, CBuffer *pBuff)				   = 0;
	virtual BOOL  METHOD Queue(CIpAddr Dest, CIpAddr From, WORD Prot, CBuffer *pBuff)		   = 0;
	virtual BOOL  METHOD Queue(UINT uFace, IPREF Gate, CBuffer *pBuff)				   = 0;
	virtual void  METHOD SendReq(UINT uMask, BOOL fState)						   = 0;
	virtual void  METHOD CopyOptions(ISocket *pSock, IPREF IP)					   = 0;
	virtual void  METHOD EnableMulticast(IPREF IP, BOOL fEnable)					   = 0;

	// Interface Calls
	virtual BOOL METHOD SetConfig(UINT uFace, IPREF IP, IPREF Mask)					   = 0;
	virtual BOOL METHOD GetConfig(UINT uFace, CIpAddr &IP, CIpAddr &Mask, CMacAddr &MAC)		   = 0;

	// Route Management
	virtual void METHOD SetGateway(UINT uFace, IPREF IP)						   = 0;
	virtual BOOL METHOD GetGateway(CIpAddr &IP)							   = 0;
	virtual void METHOD AddRoute(UINT uFace, IPREF IP, IPREF Mask, IPREF Gate)			   = 0;
	virtual void METHOD RemoveRoute(UINT uFace, IPREF IP, IPREF Mask)				   = 0;
	virtual void METHOD AddRoutes(UINT uFace)							   = 0;
	virtual void METHOD RemoveRoutes(UINT uFace)							   = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// SMS Reciever Interface
//

interface IRecvSMS
{
	// LINK

	AfxDeclareIID(7, 9);

	virtual BOOL RecvSMS(PCTXT pNumber, PCTXT pMessage) = 0;
	virtual void SetSend(ISendSMS *pSend)               = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// SMS Sender Interface
//

interface ISendSMS
{
	// LINK

	AfxDeclareIID(7, 10);

	virtual BOOL SendSMS(PCTXT pNumber, PCTXT pMessage) = 0;
};

// End of File

#endif
