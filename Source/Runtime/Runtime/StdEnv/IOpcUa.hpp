
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IOpcUa_HPP

#define INCLUDE_IOpcUa_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 11 -- OPC-UA
//

interface IOpcUaServer;
interface IOpcUaServerHost;
interface IOpcUaDataModel;
interface IOpcUaClient;

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server Configuration
//

struct COpcUaServerConfig
{
	UINT	   m_uSize;
	UINT	   m_uDebug;
	UINT	   m_uSample;
	UINT	   m_uQuota;
	UINT	   m_uTime;
	PCBYTE	   m_pGuid;
	CByteArray m_Cert;
	CByteArray m_Priv;
	CString    m_Pass;
};

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server Interface
//

interface IOpcUaServer : public IUnknown
{
	// LINK

	AfxDeclareIID(11, 1);

	// Methods
	virtual void OpcLoad(COpcUaServerConfig const &Config)		= 0;
	virtual bool OpcInit(IOpcUaServerHost *pHost, CString Endpoint) = 0;
	virtual bool OpcExec(void)					= 0;
	virtual bool OpcStop(void)					= 0;
	virtual bool OpcTerm(void)					= 0;
};

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server Host Interface
//

interface IOpcUaServerHost : public IUnknown
{
	// LINK

	AfxDeclareIID(11, 2);

	// Methods
	virtual bool    LoadModel(IOpcUaDataModel *pModel)                             = 0;
	virtual bool    OpenSession(UINT si, CString const &User, CString const &Pass) = 0;
	virtual void    CloseSession(UINT si)                                          = 0;
	virtual bool    SkipValue(UINT si, UINT ns, UINT id)                           = 0;
	virtual void    SetValueDouble(UINT si, UINT ns, UINT id, UINT n, double d)    = 0;
	virtual void    SetValueInt(UINT si, UINT ns, UINT id, UINT n, int d)          = 0;
	virtual void    SetValueInt64(UINT si, UINT ns, UINT id, UINT n, INT64 d)      = 0;
	virtual void    SetValueString(UINT si, UINT ns, UINT id, UINT n, PCTXT d)     = 0;
	virtual double  GetValueDouble(UINT si, UINT ns, UINT id, UINT n)              = 0;
	virtual UINT    GetValueInt(UINT si, UINT ns, UINT id, UINT n)                 = 0;
	virtual UINT64  GetValueInt64(UINT si, UINT ns, UINT id, UINT n)               = 0;
	virtual CString GetValueString(UINT si, UINT ns, UINT id, UINT n)              = 0;
	virtual timeval GetValueTime(UINT si, UINT ns, UINT id, UINT n)		       = 0;
	virtual bool    GetDesc(CString &Desc, UINT ns, UINT id)		       = 0;
	virtual bool    GetSourceTimeStamp(timeval &t, UINT ns, UINT id)	       = 0;
	virtual bool    HasEvents(CArray <UINT> &List, UINT ns, UINT id)	       = 0;
	virtual bool    HasCustomHistory(UINT ns, UINT id)			       = 0;
	virtual bool    InitHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory)     = 0;
	virtual bool	HasChanged(UINT ns, UINT id, UINT uType, DWORD *pHistory)      = 0;
	virtual void	KillHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory)     = 0;
	virtual UINT	GetWireType(UINT uType)                                        = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Data Model
//

interface IOpcUaDataModel : public IUnknown
{
	// LINK

	AfxDeclareIID(11, 3);

	// Methods
	virtual void AddObject(UINT nsNode, UINT idNode, UINT nsType, UINT idType, CString Name)                                                                              = 0;
	virtual void AddVariable(UINT nsNode, UINT idNode, UINT nsType, UINT idType, CString Name, UINT nsData, UINT idData, INT Rank, UINT Dim1, UINT Access, bool fHistory) = 0;
	virtual void AddObjectType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract)                                                          = 0;
	virtual void AddVariableType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, UINT nsData, UINT idData, INT Rank, bool fAbstract)                    = 0;
	virtual void AddDataType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract)                                                            = 0;
	virtual void AddReferenceType(UINT nsNode, UINT idNode, UINT nsBase, UINT idBase, CString Name, bool fAbstract, bool fSymmetric)                                      = 0;
	virtual void AddReference(UINT nsNode, UINT idNode, UINT nsRef, UINT idRef, UINT nsTarget, UINT idTarget)                                                             = 0;
	virtual void AddOrganizes(UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget)                                                                                     = 0;
	virtual void AddProperty(UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget)                                                                                     = 0;
	virtual void AddComponent(UINT nsNode, UINT idNode, UINT nsTarget, UINT idTarget)                                                                                     = 0;
	virtual void AddMandatoryRule(UINT nsNode, UINT idNode)                                                                                                               = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client Configuration
//

struct COpcUaClientConfig
{
	UINT	m_uSize;
	UINT	m_uDebug;
};

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client Interface
//

interface IOpcUaClient : public IUnknown
{
	// LINK

	AfxDeclareIID(11, 4);

	// Methods
	virtual void OpcLoad(COpcUaClientConfig const &Config)                                                    = 0;
	virtual bool OpcInit(void)							                          = 0;
	virtual bool OpcStop(void)							                          = 0;
	virtual bool OpcTerm(void)							                          = 0;
	virtual UINT OpcOpenDevice(PCTXT pHost, PCTXT pUser, PCTXT pPass, CStringArray const &Nodes)              = 0;
	virtual bool OpcCloseDevice(UINT uDevice)						                  = 0;
	virtual bool OpcPingDevice(UINT uDevice)						                  = 0;
	virtual bool OpcBrowseDevice(UINT uDevice, CStringArray &List)				                  = 0;
	virtual bool OpcReadData(UINT uDevice, PCUINT pList, UINT uList, PCUINT pType, PDWORD pData)              = 0;
	virtual bool OpcReadArray(UINT uDevice, UINT uNode, UINT uType, UINT uCount, PDWORD pData)                = 0;
	virtual bool OpcWriteData(UINT uDevice, PCUINT pList, UINT uList, PCUINT pType, PCDWORD pData)            = 0;
	virtual bool OpcWriteArray(UINT uDevice, UINT uNode, UINT uType, UINT uStart, UINT uCount, PCDWORD pData) = 0;
};

// End of File

#endif
