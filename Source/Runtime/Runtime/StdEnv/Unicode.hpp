
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Unicode_HPP
	
#define	INCLUDE_Unicode_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/ywDUE

/////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UniPtr.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CModule;

//////////////////////////////////////////////////////////////////////////
//
// String Information
//

struct DLLAPI CUnicodeData
{
	UINT	m_uAlloc;
	UINT	m_uLen;
	int	m_nRefs;
	UINT	m_uDummy;
	WCHAR	m_cData[];
	};

//////////////////////////////////////////////////////////////////////////
//
// String Searching
//

#ifndef DEFINE_SearchOptions

#define  DEFINE_SearchOptions

enum SearchOptions
{
	searchContains	 = 0x0001,
	searchIsEqualTo  = 0x0002,
	searchStartsWith = 0x0003,
	searchMatchCase  = 0x0100,
	searchWholeWords = 0x0200,
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// Dynamic String
//

class DLLAPI CUnicode : public CUniPtr
{
	public:
		// Constructors
		CUnicode(void);
		CUnicode(CUnicode const &That);
		CUnicode(PCUTF pText, UINT uCount);
		CUnicode(WCHAR cData, UINT uCount);
		CUnicode(GUID const &Guid);
		CUnicode(PCUTF pText);

		// Explicit Constructors
		explicit CUnicode(PCTXT pText);

		// Destructor
		~CUnicode(void);

		// Assignment Operators
		CUnicode const & operator = (CUnicode const &That);
		CUnicode const & operator = (GUID const &Guid);
		CUnicode const & operator = (PCUTF pText);
		CUnicode const & operator = (PCTXT pText);

		// Quick Init
		void QuickInit(PCUTF pText, UINT uSize);

		// Attributes
		UINT GetLength(void) const;

		// Read-Only Access
		WCHAR GetAt(UINT uIndex) const;

		// Subwcsing Extraction
		CUnicode Left(UINT uCount) const;
		CUnicode Right(UINT uCount) const;
		CUnicode Mid(UINT uPos, UINT uCount) const;
		CUnicode Mid(UINT uPos) const;

		// Buffer Managment
		void Empty(void);
		void Expand(UINT uAlloc);
		void Compress(void);
		void CopyOnWrite(void);
		void FixLength(UINT uLen);
		void FixLength(void);

		// Concatenation In-Place
		CUnicode const & operator += (CUnicode const &That);
		CUnicode const & operator += (PCUTF pText);
		CUnicode const & operator += (PCTXT pText);
		CUnicode const & operator += (WCHAR cData);

		// Concatenation via Friends
		friend DLLAPI CUnicode operator + (CUnicode const &A, CUnicode const &B);
		friend DLLAPI CUnicode operator + (CUnicode const &A, PCUTF pText);
		friend DLLAPI CUnicode operator + (CUnicode const &A, WCHAR cData);
		friend DLLAPI CUnicode operator + (PCUTF pText, CUnicode const &B);
		friend DLLAPI CUnicode operator + (WCHAR cData, CUnicode const &B);

		// Concatenation Functions
		BOOL Append(CUnicode const &That);
		BOOL Append(PCUTF pText);
		BOOL Append(PCUTF pText, UINT uText);
		BOOL Append(char cData);
		BOOL AppendPrintf(PCUTF pFormat, ...);
		BOOL AppendVPrintf(PCUTF pFormat, va_list pArgs);

		// Write Data Access
		void SetAt(UINT uIndex, WCHAR cData);

		// Comparison Helper
		friend DLLAPI int AfxCompare(CUnicode const &a, CUnicode const &b);

		// Insert and Remove
		void Insert(UINT uIndex, PCUTF pText);
		void Insert(UINT uIndex, CUnicode const &That);
		void Insert(UINT uIndex, WCHAR cData);
		void Delete(UINT uIndex, UINT uCount);

		// Whitespace Trimming
		void TrimLeft(void);
		void TrimRight(void);
		void TrimBoth(void);
		void StripAll(void);

		// Case Switching
		void MakeUpper(void);
		void MakeLower(void);

		// Replacement
		UINT Replace(WCHAR cFind, WCHAR cNew);
		UINT Replace(PCUTF pFind, PCUTF pNew);

		// Removal
		void    Remove (WCHAR cFind);
		CUnicode Without(WCHAR cFind);

		// Parsing
		UINT    Tokenize(CUnicodeArray &Array, WCHAR cFind) const;
		UINT    Tokenize(CUnicodeArray &Array, WCHAR cFind, WCHAR cQuote) const;
		UINT    Tokenize(CUnicodeArray &Array, WCHAR cFind, WCHAR cOpen, WCHAR cClose) const;
		UINT    Tokenize(CUnicodeArray &Array, PCUTF pFind) const;
		UINT    Tokenize(CUnicodeArray &Array, UINT uLimit, WCHAR cFind) const;
		UINT    Tokenize(CUnicodeArray &Array, UINT uLimit, WCHAR pFind, WCHAR cQuote) const;
		UINT    Tokenize(CUnicodeArray &Array, UINT uLimit, WCHAR cFind, WCHAR cOpen, WCHAR cClose) const;
		UINT    Tokenize(CUnicodeArray &Array, UINT uLimit, PCUTF pFind) const;
		CUnicode TokenLeft(WCHAR cFind) const;
		CUnicode TokenFrom(WCHAR cFind) const;
		CUnicode TokenLast(WCHAR cFind) const;
		CUnicode StripToken(WCHAR cFind);
		CUnicode StripToken(WCHAR cFind, WCHAR cQuote);
		CUnicode StripToken(WCHAR cFind, WCHAR cOpen, WCHAR cClose);
		CUnicode StripToken(PCUTF pFind);

		// Building
		void Build(CUnicodeArray &Array, WCHAR cSep);
		void Build(CUnicodeArray &Array, PCUTF pSep);

		// Display Ordering
		void MakeDisplayOrder(void);

		// Printf Formatting
		void Printf(PCUTF pFormat, ...);
		void VPrintf(PCUTF pFormat, va_list pArgs);

		// Diagnostics
		void AssertValid(void) const;

	protected:
		// Static Data
		static DWORD          m_Null[];
		static CUnicodeData & m_Empty;

		// Protected Constructor
		CUnicode(PCUTF p1, UINT u1, PCUTF p2, UINT u2);

		// Initialisation
		void InitFrom(GUID const &Guid);

		// Interal Helpers
		PUTF IntPrintf(UINT &uLen, PCUTF pFormat, va_list pArgs);

		// Clearing
		friend void DLLAPI AfxSetZero(CUnicode &Text);

		// Implementation
		void Alloc(UINT uLen, UINT uAlloc);
		void Alloc(UINT uLen);
		BOOL GrowString(UINT uLen);
		void Release(void);
		UINT AdjustSize(UINT uAlloc);
		UINT FindAlloc(UINT uLen);

		// Friends
		friend CUnicodeData * GetStringData(CUnicode const *p);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Data Access

INLINE CUnicodeData * GetStringData(CUnicode const *p)
{
	return (((CUnicodeData *) p->m_pText) - 1);
	}

// Constructors

INLINE CUnicode::CUnicode(void)
{
	m_pText = m_Empty.m_cData;
	}

INLINE CUnicode::CUnicode(CUnicode const &That)
{
	That.AssertValid();

	if( !That.IsEmpty() ) {

		m_pText = That.m_pText;

		GetStringData(this)->m_nRefs++;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

INLINE CUnicode::CUnicode(PCUTF pText)
{
	AfxAssertStringPtr(pText);

	if( *pText ) {

		Alloc(wcslen(pText));

		wcscpy(m_pText, pText);

		return;
		}

	m_pText = m_Empty.m_cData;
	}

// Destructor

INLINE CUnicode::~CUnicode(void)
{
	Release();
	}

// Attributes

INLINE UINT CUnicode::GetLength(void) const
{
	return GetStringData(this)->m_uLen;
	}

// Read-Only Access

INLINE WCHAR CUnicode::GetAt(UINT uIndex) const
{
	return (uIndex < GetLength()) ? m_pText[uIndex] : char(0);
	}

// Comparison Helper

INLINE int AfxCompare(CUnicode const &a, CUnicode const &b)
{
	return wcscasecmp(a.m_pText, b.m_pText);
	}

// Diagnostics

#ifndef _DEBUG

INLINE void CUnicode::AssertValid(void) const
{
	}

#endif

// Clearing

INLINE void AfxSetZero(CUnicode &Text)
{
	Text.Empty();
	}

// Implementation

INLINE void CUnicode::Alloc(UINT uLen, UINT uAlloc)
{
	uAlloc     = AdjustSize(uAlloc);
	
	UINT uSize = uAlloc + sizeof(CUnicodeData);
	
	CUnicodeData *pData = (CUnicodeData *) _New("(Unicode)") BYTE [ uSize ];
	
	pData->m_uLen   = uLen;
	
	pData->m_uAlloc = uAlloc;
	
	pData->m_nRefs  = 1;

	// cppcheck-suppress uninitStructMember -- Lint doesn't like this as we're
	// referencing an unitialized variable, even though that's perfectly okay.

	m_pText = pData->m_cData;

	// cppcheck-suppress memleak -- Lint thinks we are going to leak memory
	// here as we've allocated pData but not stored that pointer anywhere.
	}

INLINE void CUnicode::Alloc(UINT uLen)
{
	Alloc(uLen, FindAlloc(uLen));
	}

INLINE void CUnicode::Release(void)
{
	CUnicodeData *pData = GetStringData(this);
	
	if( pData->m_nRefs ) {
		
		if( !--(pData->m_nRefs) ) {

			delete pData;
			}
		
		m_pText = m_Empty.m_cData;
		}
	}

INLINE UINT CUnicode::AdjustSize(UINT uAlloc)
{
	return ((uAlloc + 15) & ~15);
	}

INLINE UINT CUnicode::FindAlloc(UINT uLen)
{
	return sizeof(WCHAR) * (1 + uLen);
	}

// End of File

#endif
