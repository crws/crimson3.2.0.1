
#pragma  once

#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////////////
//	
// GcCore Class Library
//
// Copyright (c) 2018 Granby Consulting LLC
//
// Placed in the Public Domain.
//

#ifndef INCLUDE_Base64_HPP

#define INCLUDE_Base64_HPP

////////////////////////////////////////////////////////////////////////////////
//	
// Base64 Encoder
//

class DLLAPI CBase64
{
public:
	// Encoding Options
	enum Encoding
	{
		encBasic   = 0,
		encNoBreak = 1,
		encUrl     = 2,
	};

	// Operations
	static CString    ToBase64(PCBYTE p, size_t n, int enc = encNoBreak);
	static CString    ToBase64(CByteArray const &d, int enc = encNoBreak);
	static CString    ToBase64(CString const &s, int enc = encNoBreak);
	static bool       ToBytes(CByteArray &d, CString const &s);
	static bool	  ToAnsi(CString &d, CString const &s);
	static CByteArray ToBytes(CString const &s);
	static CString    ToAnsi(CString const &s);

protected:
	// Decode Helper
	template<typename dtype> static bool Decode(dtype &d, CString const &s, bool z);

	// Size Estimation
	static size_t GetEncodeSize(size_t s, int enc);
	static size_t GetDecodeSize(size_t s);

	// Encoding List
	static char const * GetList(int enc);

	// Character Decode
	static int Decode(char c);
};

// End of File

#endif
