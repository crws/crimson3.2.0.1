
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MACAddr_HPP
	
#define	INCLUDE_MACAddr_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// 

/////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "IPAddr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Wrapper References
//

typedef class CMacAddr const & MACREF;

//////////////////////////////////////////////////////////////////////////
//								
// Standard MAC Addresses
//

#define	MAC_EMPTY ((MACREF) CMacAddr::m_Empty)
#define	MAC_BROAD ((MACREF) CMacAddr::m_Broad)

//////////////////////////////////////////////////////////////////////////
//								
// MAC Address Record
//

struct MACADDR
{
	BYTE m_Addr[6];
	};

//////////////////////////////////////////////////////////////////////////////
//
// MAC Address Wrapper
//

class DLLAPI CMacAddr : public MACADDR
{
	public:
		// Constructors
		CMacAddr(void);
		CMacAddr(MACADDR const &Addr);
		CMacAddr(PCBYTE pAddr);
		CMacAddr(PCTXT pText);

		// Assignment
		CMacAddr const & operator = (CMacAddr const &That);
		CMacAddr const & operator = (MACADDR  const &Addr);
		CMacAddr const & operator = (PCBYTE pAddr);
		CMacAddr const & operator = (PCTXT pText);

		// Comparison Operators
		BOOL operator == (MACADDR const &That) const;
		BOOL operator != (MACADDR const &That) const;

		// Conversion
		CString GetAsText(void) const;

		// Attributes
		BOOL IsBroadcast(void) const;
		BOOL IsMulticast(void) const;
		BOOL IsGroup(void) const;
		BOOL IsEmpty(void) const;

		// Operations
		void MakeBroadcast(void);
		void MakeMulticast(void);
		BOOL MakeMulticast(IPREF IP);
		void MakeEmpty(void);

		// Static Data
		static MACADDR const m_Broad;
		static MACADDR const m_Empty;
		static MACADDR const m_Multi;
	};

////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Comparison

STRONG_INLINE int AfxCompare(CMacAddr const &a, CMacAddr const &b)
{
	return memcmp(a.m_Addr, b.m_Addr, 6);
	}

// Constructors

STRONG_INLINE CMacAddr::CMacAddr(void)
{
	memcpy(this, &m_Empty, sizeof(MACADDR));
	}

STRONG_INLINE CMacAddr::CMacAddr(MACADDR const &Addr)
{
	memcpy(this, &Addr, sizeof(MACADDR));
	}

STRONG_INLINE CMacAddr::CMacAddr(PCBYTE pAddr)
{
	memcpy(this, pAddr, sizeof(MACADDR));
	}

STRONG_INLINE CMacAddr::CMacAddr(PCTXT pText)
{
	operator = (pText);
	}

// Assignment

STRONG_INLINE CMacAddr const & CMacAddr::operator = (CMacAddr const &That)
{
	memcpy(this, &That, sizeof(MACADDR));
	
	return *this;
	}

STRONG_INLINE CMacAddr const & CMacAddr::operator = (MACADDR const &Addr)
{
	memcpy(this, &Addr, sizeof(MACADDR));

	return *this;
	}

STRONG_INLINE CMacAddr const & CMacAddr::operator = (PCBYTE pAddr)
{
	memcpy(this, pAddr, sizeof(MACADDR));

	return *this;
	}

// Comparison Operators

STRONG_INLINE BOOL CMacAddr::operator == (MACADDR const &That) const
{
	return memcmp(this, &That, sizeof(MACADDR)) == 0;
	}

STRONG_INLINE BOOL CMacAddr::operator != (MACADDR const &That) const
{
	return memcmp(this, &That, sizeof(MACADDR)) != 0;
	}

// End of File

#endif
