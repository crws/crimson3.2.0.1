
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IWaitable_HPP

#define INCLUDE_IWaitable_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 3 -- Waitable Objects
//

// https://redlion.atlassian.net/wiki/x/K4AlEQ

interface IWaitable;
interface IMutex;
interface ISemaphore;
interface IEvent;

//////////////////////////////////////////////////////////////////////////
//
// Waitable Object
//

interface IWaitable : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/FoAiEQ

	AfxDeclareIID(3, 1);

	virtual PVOID METHOD GetWaitable(void)  = 0;
	virtual BOOL  METHOD Wait(UINT uTime)   = 0;
	virtual BOOL  METHOD HasRequest(void)   = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Mutex Object
//

interface IMutex : public IWaitable
{
	// https://redlion.atlassian.net/wiki/x/IQAhEQ

	AfxDeclareIID(3, 2);

	virtual void METHOD Free(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Semaphore Object
//

interface ISemaphore : public IWaitable
{
	// https://redlion.atlassian.net/wiki/x/MYAdEQ

	AfxDeclareIID(3, 3);

	virtual void METHOD Signal(UINT uCount) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Object
//

interface IEvent : public IWaitable
{
	// https://redlion.atlassian.net/wiki/x/OIAhEQ

	AfxDeclareIID(3, 4);

	virtual void METHOD Set  (void) = 0;
	virtual void METHOD Clear(void) = 0;
	};

// End of File

#endif
