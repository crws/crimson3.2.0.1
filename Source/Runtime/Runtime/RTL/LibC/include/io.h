
#include <sys/types.h>

#if defined(__cplusplus)
extern "C" {
#endif

extern int   isatty(int fd);
extern int   unlink(char const *name);
extern int   stat(char const *name, struct stat *buffer);
extern int   utime(char const *name, time_t time);
extern int   open(char const *name, int oflag, ...);
extern int   close(int fd);
extern int   fstat(int fd, struct stat *buffer);
extern int   read(int fd, void *buffer, size_t count);
extern int   write(int fd, void const *buffer, size_t count);
extern off_t lseek(int fd, off_t offset, int origin);

#if defined(__cplusplus)
}
#endif
