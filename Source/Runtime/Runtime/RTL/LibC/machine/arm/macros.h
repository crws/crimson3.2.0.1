
// Function prolog and epilog macros

	.macro def_fn f p2align=0
	.text
	.p2align \p2align
1:	.ascii	"asm_\f\000"
	.align	2
2:	.word	0xFF000000 + 2b - 1b
	.global \f
	.type \f, %function
\f:
	mov	ip, sp
	push	{fp, ip, lr, pc}
	sub	fp, ip, #4
	.endm

	.macro return
	ldmfd	sp, {fp, sp, pc}
	.endm
