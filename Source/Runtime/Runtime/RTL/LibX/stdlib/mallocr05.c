
#if !defined(WIN32)

#define BUILD_ME

#undef  DEFINE_MALLOC
#undef  DEFINE_FREE
#undef  DEFINE_REALLOC
#undef  DEFINE_CALLOC
#define DEFINE_CFREE
#undef  DEFINE_MEMALIGN
#undef  DEFINE_VALLOC
#undef  DEFINE_PVALLOC
#undef  DEFINE_MALLINFO
#undef  DEFINE_MALLOC_STATS
#undef  DEFINE_MALLOC_USABLE_SIZE
#undef  DEFINE_MALLOPT

#include "mallocr.c"

#endif
