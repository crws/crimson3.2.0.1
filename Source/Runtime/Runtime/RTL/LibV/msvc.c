
int _purecall(void)
{
	// TODO -- This is actually a fatal exit for this
	// thread and maybe more as the stack is left in
	// a horrible state... !!!

	__asm int 3

	return 0;
	}

void __declspec(naked) _chkstk(void)
{
	__asm pop  edx
	__asm sub  esp, eax
	__asm push edx
	__asm ret
	}

void __declspec(naked) _alloca_probe(void)
{
	__asm pop  edx
	__asm sub  esp, eax
	__asm push edx
	__asm ret
	}

void __declspec(naked) _alloca_probe_8(void)
{
	__asm pop  edx
	__asm sub  esp, eax
	__asm and  esp, ~07h
	__asm push edx
	__asm ret
	}

void __declspec(naked) _alloca_probe_16(void)
{
	__asm pop  edx
	__asm sub  esp, eax
	__asm and  esp, ~0Fh
	__asm push edx
	__asm ret
	}

int _fltused = 0;
