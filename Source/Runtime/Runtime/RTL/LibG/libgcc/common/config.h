
#define count_leading_zeros(n, v)	(n) = __builtin_clz (v)

#define LIBGCC2_UNITS_PER_WORD		4

#define BITS_PER_UNIT			8

#define MIN_UNITS_PER_WORD		LIBGCC2_UNITS_PER_WORD
