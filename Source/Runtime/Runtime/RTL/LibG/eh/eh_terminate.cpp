// -*- C++ -*- std::terminate, std::unexpected and friends.
// Copyright (C) 1994-2017 Free Software Foundation, Inc.
//
// This file is part of GCC.
//
// GCC is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3, or (at your option)
// any later version.
//
// GCC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Under Section 7 of GPL version 3, you are granted additional
// permissions described in the GCC Runtime Library Exception, version
// 3.1, as published by the Free Software Foundation.

// You should have received a copy of the GNU General Public License and
// a copy of the GCC Runtime Library Exception along with this program;
// see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
// <http://www.gnu.org/licenses/>.

#include "max_align.h"

#include "typeinfo"

#include "exception"

#include "unwind-cxx.h"

using namespace __cxxabiv1;

namespace std
{
extern void abort(void);
#if defined(__INTELLISENSE__)
extern std::unexpected_handler set_unexpected(std::unexpected_handler) throw();
extern std::unexpected_handler get_unexpected(void);
extern std::terminate_handler  set_terminate(std::terminate_handler) throw();
extern std::terminate_handler  get_terminate(void);
#endif
}

void __cxxabiv1::__terminate (std::terminate_handler handler) throw ()
{
  try 
    {
      handler ();
      std::abort ();
    } 
  catch(...) 
    { 
      std::abort ();
    }
  for(;;);
}

void
__cxxabiv1::__unexpected (std::unexpected_handler handler)
{
  handler();
  std::terminate ();
}

void std::terminate () throw()
{
  __terminate (get_terminate ());
}

void
std::unexpected ()
{
  __unexpected (get_unexpected ());
}

#if ATOMIC_POINTER_LOCK_FREE > 1
std::terminate_handler std::set_terminate (std::terminate_handler func) throw()
{
  std::terminate_handler old;
#if ATOMIC_POINTER_LOCK_FREE > 1
  __atomic_exchange (&__terminate_handler, &func, &old, __ATOMIC_ACQ_REL);
#else
  // This isn't technically atomic on Arm926 but
  // we never really use this so we don't care,
  // but nonetheless we don't compile it...
  old = __terminate_handler;
  __terminate_handler = func;
#endif
  return old;
}
#endif

std::terminate_handler std::get_terminate ()
{
  std::terminate_handler func;
#if ATOMIC_POINTER_LOCK_FREE > 1
  __atomic_load (&__terminate_handler, &func, __ATOMIC_ACQUIRE);
#else
  // This is atomic anyway...
  func = __terminate_handler;
#endif
  return func;
}

#if ATOMIC_POINTER_LOCK_FREE > 1
std::unexpected_handler std::set_unexpected (std::unexpected_handler func) throw()
{
  std::unexpected_handler old;
#if ATOMIC_POINTER_LOCK_FREE > 1
  __atomic_exchange (&__unexpected_handler, &func, &old, __ATOMIC_ACQ_REL);
#else
  // This isn't technically atomic on Arm926 but
  // we never really use this so we don't care,
  // but nonetheless we don't compile it...
  old = __unexpected_handler;
  __unexpected_handler = func;
#endif
  return old;
}
#endif

std::unexpected_handler std::get_unexpected ()
{
  std::unexpected_handler func;
#if ATOMIC_POINTER_LOCK_FREE > 1
  __atomic_load (&__unexpected_handler, &func, __ATOMIC_ACQUIRE);
#else
  // This is atomic anyway...
  func = __unexpected_handler;
#endif
  return func;
}
