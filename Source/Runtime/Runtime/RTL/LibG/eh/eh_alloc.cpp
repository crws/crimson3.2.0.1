
// Headers

#include "../rtti/tinfo.h"

#include "unwind-cxx.h"

// Global Data

int __eh_extra = sizeof(__cxxabiv1::__cxa_refcounted_exception);

// End of File
