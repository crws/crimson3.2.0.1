
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_BaseExecutive_HPP

#define INCLUDE_BaseExecutive_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CBaseExecThread;

class CExecTimer;

//////////////////////////////////////////////////////////////////////////
//
// Base Executive Object
//

class CBaseExecutive : public IExecutive
{
	public:
		// Constructor
		CBaseExecutive(void);

		// Destructor
		virtual ~CBaseExecutive(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IExecutive
		ITimer * METHOD CreateTimer(void);

		// Timer Support
		virtual void CreateTimer(CExecTimer *pTimer) = 0;
		virtual void DeleteTimer(CExecTimer *pTimer) = 0;

		// Operations
		void GetEntropy(PBYTE pData, UINT uData);

	protected:
		// Static Data
		static UINT m_uAlloc;

		// Notify List
		typedef CList <IThreadNotify *> CNotifyList;

		// Data Members
		ULONG	     m_uRefs;
		CNotifyList  m_NotifyList;
		CExecTimer * m_pHeadTimer;
		CExecTimer * m_pTailTimer;
		IEntropy   * m_pEntropy;

		// Implementation
		void ServiceTimers(UINT uTicks);
	};

// End of File

#endif
