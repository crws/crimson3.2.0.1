
#include "Intern.hpp"

#include "InputQueue.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Input Queue
//

// Instantiator

IDevice * Create_InputQueue(void)
{
	return New CInputQueue(TRUE);
	}

// Instantiator

static IUnknown * Create_InputQueue(PCTXT pName)
{
	return New CInputQueue(FALSE);
	}

// Registration

global void Register_InputQueue(void)
{
	piob->RegisterInstantiator("util.input-x", Create_InputQueue);
	}

global void Revoke_InputQueue(void)
{
	piob->RevokeInstantiator("util.input-x");
	}

// Constructor

CInputQueue::CInputQueue(BOOL fDisp)
{
	StdSetRef();

	m_fDisp = fDisp;

	m_pLock = Create_Rutex();

	m_pFlag = Create_Semaphore();

	m_uHead = 0;

	m_uTail = 0;
	}

// IDevice

BOOL CInputQueue::Open(void)
{
	return TRUE;
	}

// IUnknown

HRESULT CInputQueue::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IInputQueue);

	return E_NOINTERFACE;
	}

ULONG CInputQueue::AddRef(void)
{
	StdAddRef();
	}

ULONG CInputQueue::Release(void)
{
	StdRelease();
	}

// IInputQueue

INPUT CInputQueue::Read(UINT uTime)
{
	if( m_pFlag->Wait(uTime) ) {

		INPUT Input = m_Data[m_uHead];

		m_uHead = (m_uHead + 1) % elements(m_Data);

		return Input;
		}

	return 0;
	}

bool CInputQueue::Store(INPUT Input)
{
	CAutoLock Lock(m_pLock);

	UINT uNext = (m_uTail + 1) % elements(m_Data);

	if( m_uHead != uNext ) {

		m_Data[m_uTail] = Input;

		m_uTail = uNext;

		m_pFlag->Signal(1);

		return true;
		}

	Lock.Lock(FALSE);

	AfxTrace("input: queue is full\n");

	return false;
	}

void CInputQueue::Clear(void)
{
	while( Read(0) );
	}

// End of File
