
#include "Intern.hpp"

#include "Debug.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Debug Support Object
//

// Instantiator

IUnknown * Create_Debug(void)
{
	return New CDebug;
	}

// Constructor

CDebug::CDebug(void)
{
	StdSetRef();
	}

// Destructor

CDebug::~CDebug(void)
{
	}

// IUnknown

HRESULT CDebug::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDebug);

	StdQueryInterface(IDebug);

	return E_NOINTERFACE;
	}

ULONG CDebug::AddRef(void)
{
	StdAddRef();
	}

ULONG CDebug::Release(void)
{
	StdRelease();
	}

// IDebug

void CDebug::AfxTraceArgs(PCTXT pText, va_list pArgs)
{
	::AfxTraceArgs(pText, pArgs);
	}

void CDebug::AfxPrint(PCTXT pText)
{
	::AfxPrint(pText);
	}

void CDebug::AfxDump(PCVOID pData, UINT uCount)
{
	::AfxDump(pData, uCount);
	}

void CDebug::AfxAssertFailed(PCTXT pFile, UINT uLine)
{
	::AfxAssertFailed(pFile, uLine);
	}

void CDebug::AfxAssertReadPtr(PCVOID pData, UINT uSize)
{
	::AfxAssertReadPtr(pData, uSize);
	}

void CDebug::AfxAssertWritePtr(PVOID pData, UINT uSize)
{
	::AfxAssertWritePtr(pData, uSize);
	}

void CDebug::AfxAssertStringPtr(PCTXT pText, UINT uSize)
{
	::AfxAssertStringPtr(pText, uSize);
	}

void CDebug::AfxAssertStringPtr(PCUTF pText, UINT uSize)
{
	::AfxAssertStringPtr(pText, uSize);
	}

BOOL CDebug::AfxCheckReadPtr(PCVOID pData, UINT uSize)
{
	return ::AfxCheckReadPtr(pData, uSize);
	}

BOOL CDebug::AfxCheckWritePtr(PVOID pData, UINT uSize)
{
	return ::AfxCheckWritePtr(pData, uSize);
	}

BOOL CDebug::AfxCheckStringPtr(PCTXT pText, UINT uSize)
{
	return ::AfxCheckStringPtr(pText, uSize);
	}

BOOL CDebug::AfxCheckStringPtr(PCUTF pText, UINT uSize)
{
	return ::AfxCheckStringPtr(pText, uSize);
	}

void CDebug::AfxStackTrace(void)
{
	HostTrace();
	}

// End of File
