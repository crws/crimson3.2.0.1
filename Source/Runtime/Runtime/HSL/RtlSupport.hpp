
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RtlSupport_HPP

#define INCLUDE_RtlSupport_HPP

//////////////////////////////////////////////////////////////////////////
//
// RTL Support Object
//

class CRtlSupport : public IRtlSupport
{
	public:
		// Constructor
		CRtlSupport(void);

		// Destructor
		~CRtlSupport(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IRtlSupport
		void   METHOD GetThreadImpurePtrThunk(struct _reent *(**pfnFunc)(void));
		void   METHOD GetGlobalImpurePtrThunk(struct _reent *(**pfnFunc)(void));
		UINT   METHOD VSNPrintf(PTXT pBuff, UINT uLimit, PCTXT pText, va_list pArgs);
		int    METHOD VFPrintf(FILE *pFile, PCTXT pText, va_list pArgs);
		int    METHOD SVFPrintf(FILE *pFile, PCTXT pText, va_list pArgs);
		int    METHOD VFIPrintf(FILE *pFile, PCTXT pText, va_list pArgs);
		int    METHOD SVFIPrintf(FILE *pFile, PCTXT pText, va_list pArgs);
		int    METHOD SSVFScanf(FILE *pFile, PCTXT pText, va_list pArgs);
		int    METHOD GetTimeOfDay(struct timeval *pt, void const *tz);
		int    METHOD SetTimeOfDay(struct timeval const *pt, void const *tz);
		time_t METHOD GetMonoSecs(void);

	protected:
		// Data Members
		ULONG m_uRefs;
	};

// End of File

#endif
