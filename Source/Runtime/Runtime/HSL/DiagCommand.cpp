
#include "Intern.hpp"

#include "DiagCommand.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Diagnostics Command
//

// Constructor

CDiagCommand::CDiagCommand(PCTXT pLine, BOOL fConsole)
{
	StdSetRef();

	m_pLine    = pLine;

	m_fConsole = fConsole;

	m_uCode    = NOTHING;

	Tokenize();
	}

// Destructor

CDiagCommand::~CDiagCommand(void)
{
	for( UINT n = 0; n < m_uTok; n++ ) {

		free(m_pTok[n]);
		}

	free(m_pTok);
	}

// IUnknown

HRESULT CDiagCommand::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagCommand);

	StdQueryInterface(IDiagCommand);

	return E_NOINTERFACE;
	}

ULONG CDiagCommand::AddRef(void)
{
	StdAddRef();
	}

ULONG CDiagCommand::Release(void)
{
	StdRelease();
	}

// IDiagCommand

UINT CDiagCommand::GetCode(void)
{
	return m_uCode;
	}

PCTXT CDiagCommand::GetCmdLine(void)
{
	return m_pLine;
	}

UINT CDiagCommand::GetArgCount(void)
{
	return m_uTok - 1;
	}

PCTXT CDiagCommand::GetArg(UINT uArg)
{
	return (uArg + 1 < m_uTok) ? m_pTok[uArg + 1] : "";
	}

BOOL CDiagCommand::FromConsole(void)
{
	return m_fConsole;
	}

// Attributes

PCTXT CDiagCommand::GetCommand(void)
{
	return m_uTok ? m_pTok[0] : "";
	}

// Operations

void CDiagCommand::SetCode(UINT uCode)
{
	m_uCode = uCode;
	}

// Implementation

void CDiagCommand::Tokenize(void)
{
	m_uTok = 0;

	for( UINT p = 0; p < 2; p++ ) {

		bool f = false;

		UINT l = strlen(m_pLine);

		UINT t = 0;

		UINT c = 0;

		for( UINT n = 0; n <= l; n++ ) {

			bool s = (n == l) || isspace(m_pLine[n]);

			if( !s && f ) {

				c = n;
				}

			if( s && !f ) {

				if( p == 1 ) {

					UINT r = n - c;

					PTXT a = PTXT(malloc(r+1));

					a[r]   = 0;

					memcpy(a, m_pLine + c, r);

					m_pTok[t] = a;
					}

				t++;
				}

			f = s;
			}

		if( p == 0 ) {

			if( (m_uTok = t) ) {

				m_pTok = New PTXT [ t ];

				continue;
				}

			break;
			}
		}
	}

// End of File
