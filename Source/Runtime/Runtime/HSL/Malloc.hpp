
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Malloc_HPP

#define INCLUDE_Malloc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Memory Allocator
//

class CMalloc : public IMalloc, public IDiagProvider
{
	public:
		// Constructor
		CMalloc(void);

		// Destructor
		~CMalloc(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IMalloc
		void * METHOD Alloc(UINT uSize);
		void * METHOD Alloc(UINT uSize, PCSTR pFile, INT nLine);
		void * METHOD Align(UINT uAlign, UINT uSize);
		void * METHOD CAlloc(UINT uSize, UINT uCount);
		void * METHOD ReAlloc(PVOID pData, UINT uSize);
		void   METHOD Free(PVOID pData);
		UINT   METHOD MarkMemory(void);
		void   METHOD DumpMemory(IDiagOutput *pOut, UINT uMark);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Data Members
		ULONG m_uRefs;
		UINT  m_uMark;

		// Diagnostics
		BOOL DiagRegister(void);
		UINT DiagStats(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagDump(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagMark(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagSince(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagCheck(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
