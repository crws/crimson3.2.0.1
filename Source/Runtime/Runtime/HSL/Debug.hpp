
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Debug_HPP

#define INCLUDE_Debug_HPP

//////////////////////////////////////////////////////////////////////////
//
// Debug Support Object
//

class CDebug : public IDebug
{
	public:
		// Constructor
		CDebug(void);

		// Destructor
		~CDebug(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDebug
		void METHOD AfxTraceArgs(PCTXT pText, va_list pArgs);
		void METHOD AfxPrint(PCTXT pText);
		void METHOD AfxDump(PCVOID pData, UINT uCount);
		void METHOD AfxAssertFailed(PCTXT pFile, UINT uLine);
		void METHOD AfxAssertReadPtr(PCVOID pData, UINT uSize);
		void METHOD AfxAssertWritePtr(PVOID pData, UINT uSize);
		void METHOD AfxAssertStringPtr(PCTXT pText, UINT uSize);
		void METHOD AfxAssertStringPtr(PCUTF pText, UINT uSize);
		BOOL METHOD AfxCheckReadPtr(PCVOID pData, UINT uSize);
		BOOL METHOD AfxCheckWritePtr(PVOID pData, UINT uSize);
		BOOL METHOD AfxCheckStringPtr(PCTXT pText, UINT uSize);
		BOOL METHOD AfxCheckStringPtr(PCUTF pText, UINT uSize);
		void METHOD AfxStackTrace(void);

	protected:
		// Data Members
		ULONG m_uRefs;
	};

// End of File

#endif
