
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MosExecThread_HPP

#define INCLUDE_MosExecThread_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BaseExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMosExecutive;

class CMutex;

//////////////////////////////////////////////////////////////////////////
//
// Modern OS Executive Thread Object
//

class CMosExecThread : public CBaseExecThread
{
	public:
		// Constructor
		CMosExecThread(CMosExecutive *pExec);

		// Destructor
		~CMosExecThread(void);

		// IThread
		UINT METHOD GetTimer(void);
		void METHOD SetTimer(UINT uTime);

		// Attributes
		UINT GetSlept(void);

		// Operations
		void AddToSlept(UINT uTime);

		// Linked Lists
		CMosExecThread * m_pNextInExec;
		CMosExecThread * m_pPrevInExec;
		CMutex	       * m_pOwnedHead;
		CMutex	       * m_pOwnedTail;

	protected:
		// Data Members
		CMosExecutive * m_pExec;
		UINT            m_uTimer;
		UINT		m_uSlept;

		// Overridables
		BOOL OnCreate(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

STRONG_INLINE UINT CMosExecThread::GetSlept(void)
{
	UINT val = m_uSlept;

	m_uSlept = 0;

	return val;
	}

// Operations

STRONG_INLINE void CMosExecThread::AddToSlept(UINT uTime)
{
	m_uSlept += uTime;
	}

// End of File

#endif
