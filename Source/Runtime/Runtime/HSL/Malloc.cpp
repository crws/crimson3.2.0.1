
#include "Intern.hpp"

#include "Malloc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DiagOutput.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tracking Hooks
//

#if defined(_TRACKING)

clink void * _malloc_t(struct _reent *ptr, size_t size, char const *file, int line);

#endif

//////////////////////////////////////////////////////////////////////////
//
// Memory Allocator
//

// Instantiator

IUnknown * Create_Malloc(void)
{
	return (IMalloc *) New CMalloc;
	}

// Constructor

CMalloc::CMalloc(void)
{
	StdSetRef();

	DiagRegister();
	}

// Destructor

CMalloc::~CMalloc(void)
{
	}

// IUnknown

HRESULT CMalloc::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IMalloc);

	StdQueryInterface(IMalloc);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CMalloc::AddRef(void)
{
	StdAddRef();
	}

ULONG CMalloc::Release(void)
{
	StdRelease();
	}

// IMalloc

void * CMalloc::Alloc(UINT uSize)
{
	return malloc(uSize);
	}

void * CMalloc::Alloc(UINT uSize, PCSTR pFile, INT nLine)
{
	#if defined(_TRACKING)
		
	return _malloc_t(_REENT, uSize, pFile, nLine);

	#else

	return malloc(uSize);

	#endif
	}

void * CMalloc::Align(UINT uAlign, UINT uSize)
{
	return memalign(uAlign, uSize);
	}

void * CMalloc::CAlloc(UINT uSize, UINT uCount)
{
	return calloc(uSize, uCount);
	}

void * CMalloc::ReAlloc(PVOID pData, UINT uSize)
{
	return realloc(pData, uSize);
	}

void CMalloc::Free(PVOID pData)
{
	free(pData);
	}

UINT CMalloc::MarkMemory(void)
{
	#if defined(_TRACKING)
		
	return AfxMarkMemory();

	#else

	return 0;

	#endif
	}

void CMalloc::DumpMemory(IDiagOutput *pOut, UINT uMark)
{
	#if defined(_TRACKING)
		
	if( !pOut ) {

		CDiagOutput Out(NULL, "dump");

		AfxDumpMemory(&Out, uMark);

		return;
		}

	AfxDumpMemory(pOut, uMark);

	#endif
	}

// IDiagProvider

UINT CMalloc::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStats(pOut, pCmd);

		case 2:
			return DiagDump(pOut, pCmd);

		case 3:
			return DiagMark(pOut, pCmd);

		case 4:
			return DiagSince(pOut, pCmd);

		case 5:
			return DiagCheck(pOut, pCmd);
		}

	return 0;
	}

// Diagnostics

BOOL CMalloc::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		UINT uProv = pDiag->RegisterProvider(this, "mem");

		pDiag->RegisterCommand(uProv, 1, "stats");
	
		pDiag->RegisterCommand(uProv, 2, "hd");

		pDiag->RegisterCommand(uProv, 3, "mark");

		pDiag->RegisterCommand(uProv, 4, "since");

		pDiag->RegisterCommand(uProv, 5, "check");

		pDiag->Release();

		return TRUE;
		}

	return FALSE;
	}

UINT CMalloc::DiagStats(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->Error("not implemented");

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CMalloc::DiagDump(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 || pCmd->GetArgCount() == 2 ) {

		DWORD dwAddr = 0;

		UINT  uSize  = 256;

		if( true ) {

			PCTXT pArg1 = pCmd->GetArg(0);

			if( pArg1[0] == '0' && tolower(pArg1[1]) == 'x' ) {

				dwAddr = strtoul(pArg1 + 2, NULL, 16);
				}
			else
				dwAddr = strtoul(pArg1 + 0, NULL, 16);
			}

		if( pCmd->GetArgCount() == 2 ) {

			PCTXT pArg2 = pCmd->GetArg(1);

			if( pArg2[0] == '0' && tolower(pArg2[1]) == 'x' ) {

				uSize = strtoul(pArg2 + 2, NULL, 16);
				}
			else
				uSize = strtoul(pArg2 + 0, NULL, 10);
			}

		pOut->Dump(PCVOID(dwAddr), uSize);

		return 0;
		}

	pOut->Error("params are <hex-addr> [<count>]");

	return 1;
	}

UINT CMalloc::DiagMark(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		#if defined(_TRACKING)

		m_uMark = AfxMarkMemory();

		pOut->Print("Mark is %u\n", m_uMark);

		#else

		pOut->Error("memory tracking not enabled");

		#endif

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CMalloc::DiagSince(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 0 || pCmd->GetArgCount() == 1 ) {

		UINT uMark = 0;

		if( pCmd->GetArgCount() == 1 ) {

			PCTXT pArg2 = pCmd->GetArg(0);

			uMark = strtoul(pArg2, NULL, 10);
			}

		#if defined(_TRACKING)

		AfxDumpMemory(pOut, uMark);

		#else

		pOut->Error("memory tracking not enabled");

		#endif

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CMalloc::DiagCheck(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		#if defined(_TRACKING)

		DWORD dwAddr = 0;

		if( true ) {

			PCTXT pArg1 = pCmd->GetArg(0);

			if( pArg1[0] == '0' && tolower(pArg1[1]) == 'x' ) {

				dwAddr = strtoul(pArg1 + 2, NULL, 16);
				}
			else
				dwAddr = strtoul(pArg1 + 0, NULL, 16);
			}

		if( AfxCheckBlock(pOut, PVOID(dwAddr)) ) {

			pOut->Print("Block is valid\n\n");

			AfxDumpBlock(pOut, PVOID(dwAddr));
			}

		#else

		pOut->Error("memory tracking not enabled");

		#endif

		return 0;
		}

	pOut->Error("params are <hex-addr>");

	return 1;
	}

// End of File
