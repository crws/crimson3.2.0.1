
#include "Intern.hpp"

#include "BaseExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "BaseExecutive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Thread Base Class
//

// Externals

clink void __sinit(struct _reent *);

// Constructor

CBaseExecThread::CBaseExecThread(CBaseExecutive *pExec)
{
	StdSetRef();

	m_pExec      = pExec;
	m_pid        = 0;
	m_parent     = 0;
	m_guard      = 0;
	m_pGuardProc = NULL;
	m_pGuardData = 0;
	m_pTms       = NULL;
	m_pTas       = NULL;
	m_dwFlags    = 0;
	m_pHeadData  = NULL;
	m_pTailData  = NULL;
	m_cancel     = 0;
	m_tms        = stateInitial;
	m_state      = 0;
	m_exit       = 0;
	m_impure     = NULL;
}

// Destructor

CBaseExecThread::~CBaseExecThread(void)
{
	for( UINT n = 0; n < m_NotifyList.GetCount(); n++ ) {

		CNotify const &Notify = m_NotifyList[n];

		Notify.m_pNotify->OnThreadDelete(this, m_uIndex, Notify.m_uData);
	}

	for( CThreadData *pData = m_pHeadData; pData; ) {

		CThreadData *pNext = pData->m_pNext;

		delete pData;

		pData = pNext;
	}

	AfxRelease(m_pTas);
}

// IUnknown

HRESULT CBaseExecThread::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(IThread);

	return E_NOINTERFACE;
}

ULONG CBaseExecThread::AddRef(void)
{
	StdAddRef();
}

ULONG CBaseExecThread::Release(void)
{
	StdRelease();
}

// IThread

PVOID CBaseExecThread::GetObject(void)
{
	return this;
}

void CBaseExecThread::SetName(PCTXT pName)
{
	Info(1, "is %s\n", pName);

	m_Name = pName;
}

PCTXT CBaseExecThread::GetName(void)
{
	return m_Name;
}

UINT CBaseExecThread::GetIndex(void)
{
	return m_uIndex;
}

UINT CBaseExecThread::GetIdent(void)
{
	return m_pid;
}

PVOID CBaseExecThread::GetPtrParam(void)
{
	return m_pParam;
}

UINT CBaseExecThread::GetIntParam(void)
{
	return m_uParam;
}

UINT CBaseExecThread::GetExecState(void)
{
	return m_state;
}

INT CBaseExecThread::GetExitCode(void)
{
	return m_exit;
}

void CBaseExecThread::SetCancelFlag(void)
{
	AtomicCompAndSwap(&m_cancel, 0, 1);
}

void CBaseExecThread::CheckCancellation(void)
{
	if( !m_guard ) {

		if( AtomicCompAndSwap(&m_cancel, 1, 2) == 1 ) {

			// TODO -- Why does this have to print on Windows? If
			// we do not, we get a crash during system shutdown. !!!

			Info(0, "cancelling\n");

			CExecCancel c;

			c.r = exitCancel;

			throw c;
		}
	}
}

DWORD CBaseExecThread::GetFlags(void)
{
	return m_dwFlags;
}

void CBaseExecThread::SetFlags(DWORD dwMask, DWORD dwData)
{
	m_dwFlags = ((m_dwFlags & ~dwMask) | (dwData & dwMask));
}

HDATA CBaseExecThread::GetData(UINT uID)
{
	for( CThreadData *pData = m_pHeadData; pData; pData = pData->m_pNext ) {

		if( pData->m_uID == uID ) {

			return pData;
		}
	}

	return NULL;
}

BOOL CBaseExecThread::SetData(HDATA hData)
{
	if( !GetData(hData->m_uID) ) {

		AfxListAppend(m_pHeadData, m_pTailData, hData, m_pNext, m_pPrev);

		return TRUE;
	}

	return FALSE;
}

BOOL CBaseExecThread::FreeData(UINT uID)
{
	HDATA hData = GetData(uID);

	if( hData ) {

		AfxListRemove(m_pHeadData, m_pTailData, hData, m_pNext, m_pPrev);

		delete hData;

		return TRUE;
	}

	return FALSE;
}

BOOL CBaseExecThread::Advance(void)
{
	if( m_pTms && m_pTas ) {

		m_pTas->Signal(1);

		return TRUE;
	}

	return FALSE;
}

UINT CBaseExecThread::AddNotify(IThreadNotify *pNotify)
{
	for( UINT n = 0; n < m_NotifyList.GetCount(); n++ ) {

		CNotify const &Notify = m_NotifyList[n];

		if( Notify.m_pNotify == pNotify ) {

			return Notify.m_uData;
		}
	}

	CNotify Notify;

	Notify.m_pNotify = pNotify;

	Notify.m_uData   = pNotify->OnThreadCreate(this, m_uIndex);

	m_NotifyList.Append(Notify);

	return Notify.m_uData;
}

void CBaseExecThread::SetLibPointer(PVOID pLibData)
{
	AfxAssert(FALSE);
}

PVOID CBaseExecThread::GetLibPointer(void)
{
	AfxAssert(FALSE);

	return NULL;
}

// Operations

BOOL CBaseExecThread::Create(UINT uIndex, PENTRY pfnProc, IClientProcess *pProc, UINT uLevel, void *pParam, UINT uParam, ISemaphore *pTms)
{
	m_uIndex  = uIndex;

	m_pfnProc = pfnProc;

	m_pProc   = pProc;

	m_uLevel  = uLevel;

	m_pParam  = pParam;

	m_uParam  = uParam;

	m_pTms    = pTms;

	m_pTas    = pTms ? Create_Semaphore() : NULL;

	m_dwFlags = GetThreadFlags();

	return OnCreate();
}

int CBaseExecThread::ClientProc(void *pParam, UINT uParam)
{
	IClientProcess *pProc = (IClientProcess *) pParam;

	SignalNextState(stateWaitInit);

	WaitForNextState(stateRunInit);

	GuardThread(TRUE);

	try {
		if( pProc->TaskInit(uParam) ) {

			Info(0, "initialized\n");

			int r = 0;

			try {
				GuardThread(FALSE);

				SignalNextState(stateWaitExec);

				WaitForNextState(stateRunExec);

				SignalNextState(stateRunExec);

				r = pProc->TaskExec(uParam);

				Info(0, "returned with %d\n", r);

				UnwindGuards();

				BlockOnTms(stateExecDone);
			}

			catch( CExecCancel const &c )
			{
				OnChange(3);

				r = c.r;

				Info(0, "cancelled with %d\n", r);
			}

			catch( ... ) {

				r = exitExcept;

				Info(1, "uncaught exception in exec\n");
			}

			try {
				SetState(stateRunStop);

				pProc->TaskStop(uParam);

				SignalNextState(stateWaitTerm);

				WaitForNextState(stateRunTerm);
			}

			catch( ... ) {

				r = exitExcept;

				Info(1, "uncaught exception in stop\n");
			}

			try {
				pProc->TaskTerm(uParam);

				SignalNextState(stateTermDone);
			}

			catch( ... ) {

				r = exitExcept;

				Info(1, "uncaught exception in term\n");
			}

			Info(0, "completed\n");

			return r;
		}
	}

	catch( ... ) {

		Info(1, "uncaught exception in init\n");

		return exitExcept;
	}

	Info(1, "failed to initialize\n");

	return exitNoInit;
}

// Overridables

void CBaseExecThread::OnChange(int state)
{
	m_state = state;
}

// Implementation

void CBaseExecThread::CommonExecute(void)
{
	SetDefaultName();

	Info(0, "started\n");

	try {
		m_exit = (*m_pfnProc)(this, m_pParam, m_uParam);

		Info(0, "returned\n");

		UnwindGuards();
	}

	catch( CExecCancel const &c ) {

		m_exit  = c.r;

		Info(0, "cancelled\n");
	}

	catch( ... ) {

		m_exit  = exitExcept;

		Info(1, "uncaught exception\n");
	}

	OnChange(4);

	Info(0, "ended with %d\n", m_exit);
}

void CBaseExecThread::CreateImpure(void)
{
	struct _reent *impure = New struct _reent;

	memset(impure, 0, sizeof(struct _reent));

	_REENT_INIT_PTR_ZEROED(impure);

	__sinit(impure);

	m_pExec->GetEntropy(PBYTE(&impure->_new._reent._rand_next), sizeof(&impure->_new._reent._rand_next));

	_thread_impure_ptr = impure;

	m_impure           = impure;
}

void CBaseExecThread::DeleteImpure(void)
{
	_reent *r = (_reent *) m_impure;

	_reclaim_reent(r);

	delete r;
}

void CBaseExecThread::SetDefaultName(void)
{
	m_Name.Printf("%u", m_pid);
}

BOOL CBaseExecThread::SafeToGuard(void)
{
	// For functions that can be called from the initial process
	// before we have created an Aeon thread or called during
	// a thread's destruction, indicate that we should not guard.

	if( !GetCurrentThread() || GetThreadExecState(GetCurrentThread()) > 1 ) {

		return FALSE;
	}

	return TRUE;
}

BOOL CBaseExecThread::UnwindGuards(void)
{
	while( m_guard ) {

		Guard(FALSE);
	}

	return TRUE;
}

void CBaseExecThread::SetState(UINT state)
{
	m_tms = state;
}

void CBaseExecThread::WaitForNextState(UINT state)
{
	if( m_pTas ) {

		m_pTas->Wait(FOREVER);
	}

	m_tms = state;
}

void CBaseExecThread::SignalNextState(UINT state)
{
	m_tms = state;

	if( m_pTms ) {

		m_pTms->Signal(1);
	}
}

void CBaseExecThread::BlockOnTms(UINT state)
{
	m_tms = state;

	if( m_pTms ) {

		Sleep(FOREVER);
	}
}

void CBaseExecThread::Info(UINT uLevel, PCSTR pText, ...)
{
	#if 1

	if( uLevel ) {

		char s[128] = { 0 };

		va_list pArgs;

		va_start(pArgs, pText);

		siprintf(s + strlen(s), "exec: thread %d ", m_pid);

		vsiprintf(s + strlen(s), pText, pArgs);

		va_end(pArgs);

		AfxPrint(s);
	}

	#endif
}

// End of File
