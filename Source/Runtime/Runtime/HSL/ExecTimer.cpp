
#include "Intern.hpp"

#include "ExecTimer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Implementation
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "BaseExecutive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Timer Implementation
//

// Constructor

CExecTimer::CExecTimer(CBaseExecutive *pExec)
{
	StdSetRef();

	m_pExec   = pExec;
		  
	m_fEnable = FALSE;

	m_uPeriod = 0;

	m_pSink   = NULL;

	m_uParam  = 0;

	m_uTimer  = 0;

	m_pExec->CreateTimer(this);
	}

// Destructor

CExecTimer::~CExecTimer(void)
{
	m_pExec->DeleteTimer(this);
	}

// IUnknown

HRESULT CExecTimer::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ITimer);

	StdQueryInterface(ITimer);

	return E_NOINTERFACE;
	}

ULONG CExecTimer::AddRef(void)
{
	StdAddRef();
	}

ULONG CExecTimer::Release(void)
{
	StdRelease();
	}

// ITimer

void METHOD CExecTimer::Enable(bool fEnable)
{
	if( fEnable ) {

		m_uTimer = m_uPeriod;
		}

	m_fEnable = fEnable;
	}

void METHOD CExecTimer::SetPeriod(UINT uPeriod)
{
	m_uPeriod = ToTicks(uPeriod);
	}

bool METHOD CExecTimer::SetHook(IEventSink *pSink, UINT uParam)
{
	if( !m_fEnable ) {

		m_pSink  = pSink;

		m_uParam = uParam;
		
		return true;
		}

	return false;
	}

// Entry

void CExecTimer::OnTick(UINT uTicks)
{
	if( m_fEnable ) {
		
		if( m_pSink ) {

			while( m_uTimer < uTicks ) {

				m_pSink->OnEvent(NOTHING, m_uParam);

				m_uTimer += m_uPeriod;
				}

			m_uTimer -= uTicks;
			}
		}
	}

// End of File
