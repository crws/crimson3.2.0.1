
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Printf Variants
//

// Printf Control

extern int _fp_printf;

// Implementation

global UINT VSPrintf(PTXT pBuff, PCTXT pText, va_list pArgs)
{
	return _fp_printf ? vsprintf(pBuff, pText, pArgs) : vsiprintf(pBuff, pText, pArgs);
	}

global UINT VSNPrintf(PTXT pBuff, UINT uLimit, PCTXT pText, va_list pArgs)
{
	// TODO -- We need the uLimit variant!!!

	return _fp_printf ? vsprintf(pBuff, pText, pArgs) : vsiprintf(pBuff, pText, pArgs);
	}

global UINT SPrintf(PTXT pBuff, PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	UINT n = _fp_printf ? vsprintf(pBuff, pText, pArgs) : vsiprintf(pBuff, pText, pArgs);

	va_end(pArgs);

	return n;
	}

global UINT SNPrintf(PTXT pBuff, UINT uLimit, PCTXT pText, ...)
{
	// TODO -- We need the uLimit variant!!!

	va_list pArgs;

	va_start(pArgs, pText);

	UINT n = _fp_printf ? vsprintf(pBuff, pText, pArgs) : vsiprintf(pBuff, pText, pArgs);

	va_end(pArgs);

	return n;
	}

clink int _snprintf(char *pBuffer, UINT uLimit, char const *pFormat, ...)
{
	va_list pArgs;

	va_start(pArgs, pFormat);

	return VSNPrintf(pBuffer, uLimit, pFormat, pArgs);
	}

// End of File
