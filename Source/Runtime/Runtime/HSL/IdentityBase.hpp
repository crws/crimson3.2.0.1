
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_IdentityBase_HPP

#define INCLUDE_IdentityBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Identity Base Class
//

class CIdentityBase : public IIdentity
{
	public:
		// Constructor
		CIdentityBase(UINT uAddr);

		// Destructor
		virtual ~CIdentityBase(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IIdentity
		BOOL METHOD Save(void);
		BOOL METHOD Load(void);
		BOOL METHOD Import(PCBYTE pData, UINT uSize);
		BOOL METHOD Export(PBYTE  pData, UINT uSize);
		BYTE METHOD GetPropByte(UINT uProp);
		BOOL METHOD SetPropByte(UINT uProp, BYTE Data);
		WORD METHOD GetPropWord(UINT uProp);
		BOOL METHOD SetPropWord(UINT uProp, WORD Data);
		LONG METHOD GetPropLong(UINT uProp);
		BOOL METHOD SetPropLong(UINT uProp, DWORD Data);
		BOOL METHOD GetPropData(UINT uProp, PBYTE pData, UINT uSize);
		BOOL METHOD SetPropData(UINT uProp, PCBYTE pData, UINT uSize);

	protected:
		// Constants
		static const UINT constMagic = 0x49444554;

		// Layout
		enum 
		{
			propMagic   = 0x0000,
			propSize    = 0x0004,
			propVersion = 0x0006,
			};

		// Data Members
		ULONG           m_uRefs;
		ISerialMemory * m_pMem;
		UINT            m_uAddr;
		WORD            m_wSize;

		// Overrideables
		virtual void OnInit(void);
		virtual BOOL OnLoad(void);
		virtual BOOL OnSave(void);

		// Implementation
		BOOL METHOD GetData(UINT uAddr, PBYTE  pData, UINT uCount);
		BOOL METHOD PutData(UINT uAddr, PCBYTE pData, UINT uCount);
	};

// End of File

#endif
