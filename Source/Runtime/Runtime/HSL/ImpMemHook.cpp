
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Low-Level Allocator
//

clink void * __malloc_r(struct _reent *ptr, UINT uSize);

clink void * __memalign_r(struct _reent *ptr, UINT uAlign, UINT uSize);

clink void * __calloc_r(struct _reent *ptr, UINT uCount, UINT uSize);

clink void * __realloc_r(struct _reent *ptr, void *pData, UINT uSize);

clink void   __free_r(struct _reent *ptr, void *pData);

clink void   __malloc_lock(void *);

clink void   __malloc_unlock(void *);

//////////////////////////////////////////////////////////////////////////
//
// Memory Debug Constants
//

static UINT  const memSentinel    = MAKELONG(MAKEWORD('M','S'),MAKEWORD('M','S'));

static UINT  const memMagic       = MAKELONG(MAKEWORD('M','B'),MAKEWORD('M','B'));

static UINT  const memRelease     = MAKELONG(MAKEWORD('M','F'),MAKEWORD('M','F'));

static DWORD const memFillEmpty   = 0xEB;

static BYTE  const memFillRelease = 0xFB;

static UINT  const memHeadSize    = 4;

static UINT  const memTailSize    = 4;

//////////////////////////////////////////////////////////////////////////
//
// Memory Debug Structures
//

struct CHeader
{
	UINT	  m_uPad1;
	UINT	  m_uPad2;
	UINT	  m_uMagic;
	CHeader * m_pNext;
	CHeader * m_pPrev;
	UINT	  m_uSize;
	UINT	  m_uSkip;
	UINT	  m_uMark;
	UINT	  m_uSeq;
	PCSTR	  m_pFile;
	INT	  m_nLine;
	UINT	  m_uPad3;
	UINT	  m_uSent[memHeadSize];
	BYTE	  m_bData[];
	};
	
struct CTailer
{
	UINT	  m_uSent[memTailSize];
	};

//////////////////////////////////////////////////////////////////////////
//
// Memory Debug Data
//

static	BOOL	  m_fLock   = FALSE;
static	CHeader * m_pHead   = NULL;
static	CHeader * m_pTail   = NULL;
static	CHeader * m_pWalk   = NULL;
static	UINT	  m_uWalk   = 0;
static	UINT	  m_uDeep   = 0;
static	DWORD	  m_dwTotal = 0;
static	DWORD	  m_dwCount = 0;
static	UINT	  m_uMark   = 1;
static	UINT	  m_uSeq    = 1;
static	BOOL	  m_fDNT    = FALSE;

//////////////////////////////////////////////////////////////////////////
//
// Memory Diagnostic Helpers
//

#if defined(_TRACKING)

static void Print(IDiagOutput *pOut, PCSTR pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	pOut ? pOut->VPrint(pText, pArgs) : AfxTraceArgs(pText, pArgs);

	va_end(pArgs);
	}

static void Dump(IDiagOutput *pOut, PCBYTE pData, UINT uSize, UINT uBase)
{
	pOut ? pOut->Dump(pData, uSize, uBase) : AfxDump(pData, uSize);
	}

static void LoadSentinel(UINT *pData, UINT uCount)
{
	while( uCount-- ) {

		*pData++ = memSentinel;
		}
	}

static BOOL TestSentinel(UINT *pData, UINT uCount)
{
	while( uCount-- ) {

		if( *pData++ != memSentinel ) {

			return FALSE;
			}
		}

	return TRUE;
	}

static BOOL IsFourBytes(void * &pData, BYTE bCheck)
{
	for( UINT n = 0; n < sizeof(pData); n++ ) {

		if( ((BYTE *) &pData)[n] == bCheck ) {

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

static BOOL IsValidPointer(IDiagOutput *pOut, void *pData)
{
	if( pData == NULL ) {

		Print(pOut, "Attempting to use NULL memory pointer\n\n");

		return FALSE;
		}

	if( DWORD(pData) & 3 ) {

		Print(pOut, "Attempting to use misaligned memory pointer\n\n");

		return FALSE;
		}

	if( pData == PBYTE(0xFFFFFFFF) ) {

		Print(pOut, "Attempting to use memory pointer of -1\n\n");

		return FALSE;
		}

	if( !AfxCheckReadPtr(pData) ) {

		Print(pOut, "Attempting to use invalid memory pointer\n\n");

		return FALSE;
		}

	if( IsFourBytes(pData, memFillEmpty) ) {

		Print(pOut, "Attempting to use pointer from empty block\n\n");

		return FALSE;
		}

	if( IsFourBytes(pData, memFillRelease) ) {

		Print(pOut, "Attempting to use pointer from released block\n\n");

		return FALSE;
		}

	return TRUE;
	}

static void WalkMemory(void)
{
	// We typically keep this turned off as it's really
	// expensive for performance as it kills our ability
	// to keep the data cache relevant.

	#if 1

	if( !m_uDeep++ ) {

		// If we auto walk too early, things break...

		if( m_uWalk >= 8192 ) {

			if( !m_pWalk ) {

				m_pWalk = m_pHead;
				}

			for( UINT n = 0; n < 16; n++ ) {

				if( m_pWalk ) {

					if( !AfxCheckBlock(NULL, m_pWalk->m_bData) ) {

						AfxAssert(FALSE);
						}

					m_pWalk = m_pWalk->m_pNext;

					continue;
					}

				break;
				}

			m_uDeep--;

			return;
			}

		m_uDeep--;

		m_uWalk++;
		}

	#endif
	}

static bool IsValidName(char const *p)
{
	// We have to be careful as we might have a filename that came from
	// a module that we've already unloaded. If we just assume it's valid,
	// we'll get into all sorts of trouble. We thus check for valid paths
	// or valid tags starting and ending with round brackets.

	if( p[0] == '(' ) {

		for( int n = 0; n < 32; n++ ) {

			if( p[n+0] == ')' && p[n+1] == 0 ) {

				return true;
				}
			}
		}

	if( isalpha(p[0]) ) {

		if( p[1] == ':' && (p[2] == '\\' || p[2] == '/') ) {

			for( int n = 0; n < 256; n++ ) {

				if( p[n+0] == 0 ) {

					return true;
					}
				}
			}
		}

	return false;
	}

#endif

//////////////////////////////////////////////////////////////////////////
//
// Memory Diagnostic Functions
//

#if defined(_TRACKING)

global UINT AfxMarkMemory(void)
{
	AfxCheckMemory(NULL);

	return ++m_uMark;
	}

global void AfxDumpMemory(IDiagOutput *pOut, UINT uMark)
{
	UINT uFind = uMark ? uMark : m_uMark;

	if( uFind == m_uMark ) {

		m_uMark++;
		}
		
	pOut->Print("Blocks allocated with marker %u:\n\n", uFind);

	pOut->AddTable(6);

	pOut->SetColumn(0, "Seq",     "%u");
	pOut->SetColumn(1, "Address", "%8.8X");
	pOut->SetColumn(2, "Size",    "%u");
	pOut->SetColumn(3, "File",    "%s");
	pOut->SetColumn(4, "Line",    "%d");
	pOut->SetColumn(5, "State",   "%s");

	pOut->AddHead();

	pOut->AddRule('-');

	for( CHeader *pHead = m_pHead; pHead; pHead = pHead->m_pNext ) {

		if( pHead->m_uMark == uFind ) {

			pOut->AddRow();

			CString File("(Unknown)");

			if( !AfxCheckStringPtr(pHead->m_pFile) || !IsValidName(pHead->m_pFile) ) {

				File = "(UNLOADED)";
				}
			else {
				PCSTR p1 = NULL;
				
				p1 = p1 ? p1 : strstr(pHead->m_pFile, "Source\\Runtime\\Runtime\\");

				p1 = p1 ? p1 : strstr(pHead->m_pFile, "source\\runtime\\runtime\\");

				if( p1 )
					File = p1 + 7;
				else {
					PCSTR p2 = NULL;
					
					p2 = p2 ? p2 : strstr(pHead->m_pFile, "include/");

					p2 = p2 ? p2 : strstr(pHead->m_pFile, "include\\");

					if( p2 )
						File = p2 + 8;
					else
						File = pHead->m_pFile;
					}

				File.Replace('\\', '/');
				}

			pOut->SetData(0, pHead->m_uSeq);
			pOut->SetData(1, pHead->m_bData);
			pOut->SetData(2, pHead->m_uSize);
			pOut->SetData(3, PCTXT(File));
			pOut->SetData(4, pHead->m_nLine);
			pOut->SetData(5, AfxCheckBlock(pHead->m_bData) ? "Valid" : "INVALID");

			pOut->EndRow();
			}
		}

	pOut->AddRule('-');

	pOut->AddRow();

	pOut->SetData(0, m_uSeq);

	pOut->EndRow();

	pOut->EndTable();

	AfxCheckMemory(NULL);
	}

global void AfxDoNotTrack(BOOL fStart)
{
	if( fStart ) {

		__malloc_lock(_REENT);

		m_fDNT = TRUE;
		}
	else {
		m_fDNT = FALSE;

		__malloc_unlock(_REENT);
		}
	}

global void AfxDumpBlock(IDiagOutput *pOut, void *pData)
{
	CHeader *pHead = (CHeader *) pData - 1;

	if( IsValidPointer(pOut, pData) ) {
		
		if( pHead->m_uMagic == memMagic ) {

			CTailer *pTail = (CTailer *) (pHead->m_bData + pHead->m_uSize);

			Print(pOut, "Span : %8.8X - %8.8X\n", pHead->m_bData, pTail);

			Print(pOut, "Size : %8.8X = %u\n", pHead->m_uSize, pHead->m_uSize);

			Print(pOut, "File : %s\n", pHead->m_pFile ? pHead->m_pFile : "(Unknown)");

			Print(pOut, "Line : %d\n", pHead->m_nLine);

			Print(pOut, "\n");

			PBYTE p1 = PBYTE(pHead)   - 32;

			PBYTE p2 = PBYTE(pTail+1) + 32;

			if( p2 - p1 > 384 ) {

				Dump(pOut, p1, 192, UINT(p1 - pHead->m_bData));

				Print(pOut, ".\n");
				Print(pOut, ".\n");
				Print(pOut, ".\n");

				PBYTE p3 = p2 - 192;

				Dump(pOut, p3, 192, UINT(p3 - pHead->m_bData));
				}
			else
				Dump(pOut, p1, p2 - p1, UINT(p1 - pHead->m_bData));
			}
		else {
			Print(pOut, "Not a dumpable block\n\n");

			PBYTE pShow = PBYTE(pData) - 64;

			UINT  uShow = 192;

			Dump(pOut, pShow, uShow, UINT(-64));
			}
		}
	else {
		Print(pOut, "Block cannot be displayed.\n");
		}
	}

global BOOL AfxCheckMemory(IDiagOutput *pOut)
{
	__malloc_lock(_REENT);

	BOOL fOkay = TRUE;

	for( CHeader *pHead = m_pHead; pHead; pHead = pHead->m_pNext ) {

		if( !AfxCheckBlock(pOut, pHead->m_bData) ) {

			fOkay = FALSE;

			break;
			}
		}

	__malloc_unlock(_REENT);

	return fOkay;
	}

global BOOL AfxCheckBlock(IDiagOutput *pOut, void *pData)
{
	if( IsValidPointer(pOut, pData) ) {

		CHeader *pHead = (CHeader *) pData - 1;

		if( pHead->m_uMagic == memMagic ) {

			if( TestSentinel(pHead->m_uSent, memHeadSize) ) {
			
				CTailer *pTail = (CTailer *) (pHead->m_bData + pHead->m_uSize);
				
				if( TestSentinel(pTail->m_uSent, memTailSize) ) {

					return TRUE;
					}

				Print(pOut, "\n*** BLOCK at %8.8X has invalid tail sentinel\n\n", pData);

				AfxDumpBlock(pOut, pData);

				return FALSE;
				}

			Print(pOut, "\n*** BLOCK at %8.8X has invalid head sentinel\n\n", pData);

			AfxDumpBlock(pOut, pData);

			return FALSE;
			}

		if( pHead->m_uMagic == memRelease ) {

			Print(pOut, "\n*** BLOCK at %8.8X has already been released\n\n", pData);

			AfxDumpBlock(pOut, pData);

			return FALSE;
			}

		Print(pOut, "\n*** BLOCK at %8.8X has invalid magic number\n\n", pData);

		AfxDumpBlock(pOut, pData);

		return FALSE;
		}

	Print(pOut, "\n*** BLOCK at %8.8X has invalid address\n", pData);

	return FALSE;
	}

global BOOL AfxCheckBlock(void *pData)
{
	CHeader *pHead = (CHeader *) pData - 1;

	if( pHead->m_uMagic == memMagic ) {

		if( TestSentinel(pHead->m_uSent, memHeadSize) ) {
			
			CTailer *pTail = (CTailer *) (pHead->m_bData + pHead->m_uSize);
				
			if( TestSentinel(pTail->m_uSent, memTailSize) ) {

				return TRUE;
				}
			}
		}
		
	return FALSE;
	}

#else

global UINT AfxMarkMemory(void)
{
	return 0;
	}

global void AfxDumpMemory(IDiagOutput *pOut, UINT uMark)
{
	}

global void AfxDoNotTrack(BOOL fStart)
{
	}

#endif

//////////////////////////////////////////////////////////////////////////
//
// Tracking Allocator
//

clink void * _malloc_t(struct _reent *ptr, UINT uSize, PCSTR pFile, INT nLine)
{
	#if defined(_TRACKING)

	UINT     uExtra = sizeof(CHeader) + sizeof(CTailer);
	
	CHeader *pHead  = (CHeader *) __malloc_r(ptr, uSize + uExtra);

	if( pHead ) {

		BOOL fDNT = (m_fDNT || (GetThreadFlags() & 1));

		pHead->m_uPad1  = 0;
		pHead->m_uPad2  = 0;
		pHead->m_uPad3  = 0;
		pHead->m_uMagic = NOTHING;
		pHead->m_uSize  = uSize;
		pHead->m_uSkip  = 0;
		pHead->m_pFile  = fDNT ? 0 : pFile;
		pHead->m_nLine  = fDNT ? 0 : nLine;
		pHead->m_uMark  = fDNT ? 0 : m_uMark;

		LoadSentinel(pHead->m_uSent, memHeadSize);

		memset(pHead->m_bData, memFillEmpty, uSize);

		CTailer *pTail = (CTailer *) ((BYTE *) (pHead + 1) + uSize);

		LoadSentinel(pTail->m_uSent, memTailSize);

		__malloc_lock(ptr);

		pHead->m_uSeq = m_uSeq++;
			
		m_dwTotal += pHead->m_uSize;
			
		m_dwCount += 1;

		AfxListAppend(m_pHead, m_pTail, pHead, m_pNext, m_pPrev);

		pHead->m_uMagic = memMagic;

		WalkMemory();

		__malloc_unlock(ptr);

		return pHead->m_bData;
		}

	return NULL;

	#else

	return __malloc_r(ptr, uSize);

	#endif
	}

clink void * _malloc_r(struct _reent *ptr, UINT uSize)
{
	#if defined(_TRACKING)

	return _malloc_t(ptr, uSize, "(Malloc)", 0);

	#else

	return __malloc_r(ptr, uSize);

	#endif
	}

clink void * _memalign_r(struct _reent *ptr, UINT uAlign, UINT uSize)
{
	#if defined(_TRACKING)

	UINT  uExtra = sizeof(CHeader) + sizeof(CTailer);

	PBYTE pAlloc = PBYTE(__malloc_r(ptr, uSize + uExtra + uAlign));

	if( pAlloc ) {

		UINT uMiss = DWORD(pAlloc + sizeof(CHeader)) % uAlign;

		UINT uSkip = uMiss ? (uAlign - uMiss) : 0;

		CHeader *pHead  = (CHeader *) (pAlloc + uSkip);

		pHead->m_uPad1  = 0;
		pHead->m_uPad2  = 0;
		pHead->m_uPad3  = 0;
		pHead->m_uMagic = NOTHING;
		pHead->m_uSize  = uSize;
		pHead->m_uSkip  = uSkip;
		pHead->m_pFile  = "(Aligned)";
		pHead->m_nLine  = 0;
		pHead->m_uMark  = m_uMark;

		LoadSentinel(pHead->m_uSent, memHeadSize);

		memset(pHead->m_bData, memFillEmpty, uSize);

		CTailer *pTail = (CTailer *) ((BYTE *) (pHead + 1) + uSize);

		LoadSentinel(pTail->m_uSent, memTailSize);

		__malloc_lock(ptr);
			
		pHead->m_uSeq = m_uSeq++;

		m_dwTotal += pHead->m_uSize;
			
		m_dwCount += 1;

		AfxListAppend(m_pHead, m_pTail, pHead, m_pNext, m_pPrev);

		pHead->m_uMagic = memMagic;

		WalkMemory();

		__malloc_unlock(ptr);

		return pHead->m_bData;
		}
		
	return NULL;

	#else

	return __memalign_r(ptr, uAlign, uSize);

	#endif
	}

clink void * _calloc_r(struct _reent *ptr, UINT uCount, UINT uSize)
{
	#if defined(_TRACKING)

	void *p = _malloc_t(ptr, uCount * uSize, "(Calloc)", 0);

	memset(p, 0, uCount * uSize);

	return p;

	#else

	return __calloc_r(ptr, uCount, uSize);

	#endif
	}

clink void * _realloc_r(struct _reent *ptr, void *pData, UINT uSize)
{
	#if defined(_TRACKING)

	if( pData ) {

		void    * pCopy = _malloc_t(ptr, uSize, "(Realloc)", 0);

		CHeader * pHead = (CHeader *) pData - 1;

		UINT      uCopy = min(uSize, pHead->m_uSize);

		memcpy(pCopy, pData, uCopy);

		_free_r(ptr, pData);

		return pCopy;
		}

	return _malloc_t(ptr, uSize, "(Realloc)", 0);
	
	#else

	return __realloc_r(ptr, pData, uSize);

	#endif
	}

clink void _free_r(struct _reent *ptr, void *pData)
{
	#if defined(_TRACKING)

	if( pData ) {

		if( AfxCheckBlock(NULL, pData) ) {

			CHeader *pHead = (CHeader *) pData - 1;

			if( TRUE ) {

				__malloc_lock(ptr);

				pHead->m_uMagic = memRelease;

				if( m_pWalk == pHead ) {

					m_pWalk = m_pWalk->m_pNext;
					}

				AfxListRemove(m_pHead, m_pTail, pHead, m_pNext, m_pPrev);
				
				m_dwCount -= 1;
				
				m_dwTotal -= pHead->m_uSize;

				WalkMemory();

				__malloc_unlock(ptr);
				}

			memset(pHead->m_bData, memFillRelease, pHead->m_uSize);

			__free_r(ptr, PBYTE(pHead) - pHead->m_uSkip);

			return;
			}

		HostBreak();
		}
	#else

	__free_r(ptr, pData);

	#endif
	}

// End of File
