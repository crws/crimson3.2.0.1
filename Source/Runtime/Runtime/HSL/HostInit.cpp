
#include "Intern.hpp"

#include <wctype.h>

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Broker Creation
//

extern IObjectBroker * Create_ObjectBroker(void);

//////////////////////////////////////////////////////////////////////////
//
// Object Broker Pointer
//

global IObjectBroker * piob = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Stub Binding
//

extern void Bind_Executive(void);
extern void Free_Executive(void);

extern void Bind_BuffMan(void);
extern void Free_BuffMan(void);

extern void Bind_FileSupport(void);
extern void Free_FileSupport(void);

//////////////////////////////////////////////////////////////////////////
//
// Core Object Creation
//

extern IUnknown * Create_DiagManager(void);
extern IUnknown * Create_Malloc(void);
extern IUnknown * Create_RtlSupport(void);
extern IUnknown * Create_FileSupport(void);
extern IUnknown * Create_Debug(void);
extern IUnknown * Create_Executive(void);
extern IUnknown * Create_BufferManager(void);
extern IUnknown * Create_ExceptionIndex(void);

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

global int AeonHostInit(BOOL fTest)
{
	// Create the object broker.

	Create_ObjectBroker();

	// Create and register the Aeon core objects.

	piob->RegisterSingleton("aeon.diagmanager", 0, Create_DiagManager());

	piob->RegisterSingleton("aeon.malloc",      0, Create_Malloc());

	piob->RegisterSingleton("aeon.rtlsupport",  0, Create_RtlSupport());

	piob->RegisterSingleton("aeon.debug",       0, Create_Debug());

	piob->RegisterSingleton("aeon.exec",        0, Create_Executive());

	piob->RegisterSingleton("aeon.buffman",     0, Create_BufferManager());

	#if !defined(AEON_PLAT_WIN32)

	piob->RegisterSingleton("aeon.exidx",       0, Create_ExceptionIndex());

	#endif

	// Register the object broker diagnostics.

	piob->RegisterDiagnostics();

	// Prevent a spurious linkage into GLIBC.

	iswspace(' ');

	// Bind the host's executive and buffer manager stubs.

	Bind_Executive();

	Bind_BuffMan();

	// Create and bind the filing system support.

	piob->RegisterSingleton("aeon.filesupport", 0, Create_FileSupport());

	Bind_FileSupport();

	// Start the main thread and wait for it to complete.

	IThread *pMain = CreateThread(AeonHostMain, 98000, NULL, fTest);

	// On RLOS, we never get this far as the Executive takes over.

	WaitThread(pMain, FOREVER);

	// If you see crashes after here on Linux, you probably haven't
	// killed all your threads so _tls_valid is still set but we may
	// not have valid data for the runtime library!

	int nCode = pMain->GetExitCode();

	pMain->Release();

	// Free the host's various stubs.

	Free_FileSupport();

	Free_BuffMan();

	Free_Executive();

	// Revoke and thereby delete the Aeon core objects.

	piob->RevokeGroup("aeon.");

	// Delete the object broker.

	piob->Release();

	// And exit.

	return nCode;
	}

// End of File
