
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Debug Support Implementation
//

// Printf Control

extern int _fp_printf;

// Common Code

global bool AfxHasDebug(void)
{
	// TODO -- Forward somewhere... !!!

	return true;
	}

clink void _AfxTrace(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	AfxTraceArgs(pText, pArgs);	

	va_end(pArgs);
	}

global void AfxTrace(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	AfxTraceArgs(pText, pArgs);	

	va_end(pArgs);
	}

global void AfxTraceArgs(PCTXT pText, va_list pArgs)
{
	// TODO -- Potential buffer overflow!!!

	char sWork[1024];

	// TODO -- Restore ability to use FP here? !!!

	(0 && _fp_printf) ? vsprintf(sWork, pText, pArgs) : vsiprintf(sWork, pText, pArgs);

	AfxPrint(sWork);
	}

global void AfxDump(PCVOID pData, UINT uCount)
{
	if( pData ) {

		PCBYTE p = PCBYTE(pData);

		UINT   s = 0;

		for( UINT n = 0; n < uCount; n++ ) {

			if( n % 0x10 == 0x0 ) {

				AfxTrace("%8.8X : %4.4X : ", p + n, n);

				s = n;
				}

			if( true ) {

				AfxTrace("%2.2X ", p[n]);
				}

			if( n % 0x10 == 0xF || n == uCount - 1 ) {

				AfxPrint(" ");

				for( UINT j = n; j % 0x10 < 0xF; j++ ) {

					AfxPrint("   ");
					}

				for( UINT i = 0; i <= n - s; i++ ) {

					BYTE b = p[s+i];

					if( b >= 32 && b < 127 ) {

						AfxTrace("%c", b);
						}
					else
						AfxPrint(".");
					}

				AfxPrint("\n");
				}
			}
		}
	}

global void AfxAssertFailed(PCTXT pFile, UINT uLine)
{
	AfxTrace("ERROR: Assertion Failed at line %u of %s\n", uLine, pFile);

	HostBreak();
	}

// End of File
