
#include "Intern.hpp"

#include "DiagConsoleString.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// String-Base Diagnostics Console
//

// Constructor

CDiagConsoleString::CDiagConsoleString(char **ppBuff)
{
	*ppBuff  = NULL;

	m_ppBuff = ppBuff;

	m_uAlloc = 0;

	m_uPtr   = 0;

	StdSetRef();
	}

// Destructor

CDiagConsoleString::~CDiagConsoleString(void)
{
	}

// IUnknown

HRESULT CDiagConsoleString::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagConsole);

	StdQueryInterface(IDiagConsole);

	return E_NOINTERFACE;
	}

ULONG CDiagConsoleString::AddRef(void)
{
	StdAddRef();
	}

ULONG CDiagConsoleString::Release(void)
{
	StdRelease();
	}

// IDiagConsole

void CDiagConsoleString::Write(PCTXT pText)
{
	UINT n = strlen(pText);

	while( m_uPtr + n + 1 > m_uAlloc ) {

		m_uAlloc  = m_uAlloc ? (m_uAlloc << 1) : 1024;

		*m_ppBuff = (char *) realloc(*m_ppBuff, m_uAlloc);
		}

	strcpy(*m_ppBuff + m_uPtr, pText);

	m_uPtr += n;
	}

// End of File
