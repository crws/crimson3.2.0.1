
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ExceptionIndex_HPP

#define INCLUDE_ExceptionIndex_HPP

//////////////////////////////////////////////////////////////////////////
//
// Exception Index
//

class CExceptionIndex : public IExceptionIndex, public IDiagProvider
{
	public:
		// Constructor
		CExceptionIndex(void);

		// Destructor
		~CExceptionIndex(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IExceptionIndex
		PVOID METHOD ExAlloc(int nSize);
		void  METHOD ExFree(PVOID pExcept);
		PVOID METHOD ArmRegister(PBYTE pc1, PBYTE pc2, PINT64 pi1, PINT64 pi2);
		void  METHOD ArmDeregister(PVOID p);
		UINT  METHOD ArmLookup(UINT Addr, PINT pSize);
		PVOID METHOD X86Register(PCVOID begin, struct object *obj);
		void  METHOD X86Deregister(PVOID p);
		void  METHOD X86Lookup(struct dwarf_fde const **ppfde, void *pc, struct dwarf_eh_bases *bases);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Alloc
		struct CAlloc
		{
			int  m_fLock;
			BYTE m_bData[256];
			};

		// Record
		struct CRecord
		{
			PBYTE  m_pc1;
			PBYTE  m_pc2;
			PINT64 m_pi1;
			PINT64 m_pi2;
			};

		// Data Members
		IMutex           * m_pLock;
		ULONG              m_uRefs;
		CArray <CRecord *> m_List;
		CAlloc             m_Pool[16];
		int		   m_nLast;

		// Diagnostics
		BOOL DiagRegister(void);
		UINT DiagList(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
