
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ComProxy_HPP

#define INCLUDE_ComProxy_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/ZoCgE

//////////////////////////////////////////////////////////////////////////
//
// COM Proxy Object
//

class CComProxy
{
	public:
		// Constructor
		CComProxy(PCSTR pName, IUnknown *pObj, IUnknown *pUnk, REFIID iid);

		// IUnknown
		virtual HRM QueryInterface(REFIID riid, void **ppObject);
		virtual ULM AddRef(void);
		virtual ULM Release(void);

		// Proxy Methods
		virtual HRM Method03(void);
		virtual HRM Method04(void);
		virtual HRM Method05(void);
		virtual HRM Method06(void);
		virtual HRM Method07(void);
		virtual HRM Method08(void);
		virtual HRM Method09(void);
		virtual HRM Method10(void);
		virtual HRM Method11(void);
		virtual HRM Method12(void);
		virtual HRM Method13(void);
		virtual HRM Method14(void);
		virtual HRM Method15(void);
		virtual HRM Method16(void);
		virtual HRM Method17(void);
		virtual HRM Method18(void);
		virtual HRM Method19(void);
		virtual HRM Method20(void);
		virtual HRM Method21(void);
		virtual HRM Method22(void);
		virtual HRM Method23(void);
		virtual HRM Method24(void);
		virtual HRM Method25(void);
		virtual HRM Method26(void);
		virtual HRM Method27(void);
		virtual HRM Method28(void);
		virtual HRM Method29(void);
		virtual HRM Method30(void);
		virtual HRM Method31(void);
		virtual HRM Method32(void);
		virtual HRM Method33(void);
		virtual HRM Method34(void);
		virtual HRM Method35(void);
		virtual HRM Method36(void);
		virtual HRM Method37(void);
		virtual HRM Method38(void);
		virtual HRM Method39(void);
		virtual HRM Method40(void);
		virtual HRM Method41(void);
		virtual HRM Method42(void);
		virtual HRM Method43(void);
		virtual HRM Method44(void);
		virtual HRM Method45(void);
		virtual HRM Method46(void);
		virtual HRM Method47(void);
		virtual HRM Method48(void);
		virtual HRM Method49(void);
		virtual HRM Method50(void);
		virtual HRM Method51(void);
		virtual HRM Method52(void);
		virtual HRM Method53(void);
		virtual HRM Method54(void);
		virtual HRM Method55(void);
		virtual HRM Method56(void);
		virtual HRM Method57(void);
		virtual HRM Method58(void);
		virtual HRM Method59(void);
		virtual HRM Method60(void);
		virtual HRM Method61(void);
		virtual HRM Method62(void);
		virtual HRM Method63(void);

	protected:
		// Data Members
		ULONG      m_uRefs;
		IUnknown * m_pObj;
		IUnknown * m_pUnk;
		CGuid      m_iid;
		char       m_sName[64];
	};

// End of File

#endif
