
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DoubleDataHandler_HPP

#define INCLUDE_DoubleDataHandler_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

//////////////////////////////////////////////////////////////////////////
//
// Double Data Handler
//

class CDoubleDataHandler : public IDataHandler
{
	public:
		// Constructor
		CDoubleDataHandler(void);

		// Destructor
		~CDoubleDataHandler(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);
		
		// IDataHandler
		void METHOD SetTxSize(UINT uSize);
		void METHOD SetRxSize(UINT uSize);
		UINT METHOD Read(UINT uTime);
		BOOL METHOD Write(BYTE bData, UINT uTime);
		UINT METHOD Read(PBYTE pData, UINT uCount, UINT uTime);
		UINT METHOD Write(PCBYTE pData, UINT uCount, UINT uTime);
		void METHOD SetBreak(BOOL fBreak);
		void METHOD ClearRx(void);
		void METHOD ClearTx(void);

	protected:
		// Data Members
		ULONG         m_uRefs;
		IPortObject * m_pPort;
		ISemaphore  * m_pRxFlag;
		PBYTE	      m_pRxData;
		UINT	      m_uRxSize;
		UINT	      m_uRxCount;
		UINT	      m_uRxHead;
		UINT	      m_uRxTail;
		ISemaphore  * m_pTxFlag;
		IEvent      * m_pTxDone;
		PBYTE	      m_pTxData;
		UINT          m_uTxSize;
		UINT	      m_uTxCount;
		UINT          m_uTxHead;
		UINT          m_uTxTail;
		BOOL          m_fTxBusy;
		BOOL          m_fBatch;

		// Implementation
		void AllocBuffers(void);
		void KillBuffers(void);
		void CreateObjects(void);
		void DeleteObjects(void);

		// Transmit Helpers
		void TxStore(BYTE bData);
		void TxFirst(BYTE bData);
		BOOL TxStart(UINT uLimit);
		BOOL TxClaim(UINT uCount);

		// Interrupts
		void EnableInterrupts(BOOL fEnable);
	};

// End of File

#endif
