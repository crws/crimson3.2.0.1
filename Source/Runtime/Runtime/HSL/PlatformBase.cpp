
#include "Intern.hpp"

#include "PlatformBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern void Register_InputQueue(void);
extern void Register_DoubleDataHandler(void);
extern void Register_SingleDataHandler(void);

extern void Revoke_InputQueue(void);
extern void Revoke_DoubleDataHandler(void);
extern void Revoke_SingleDataHandler(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

extern void Register_HxGraph(void);
extern void Register_HxMatrix(void);
extern void Register_HxOpcUa(void);
extern void Register_HxZLib(void);
extern void Register_HxEnetIp(void);
extern void Register_HxDnp(void);

extern void Revoke_HxGraph(void);
extern void Revoke_HxMatrix(void);
extern void Revoke_HxOpcUa(void);
extern void Revoke_HxZLib(void);
extern void Revoke_HxEnetIp(void);
extern void Revoke_HxDnp(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extensions Libraries
//

#if defined(AEON_COMP_MSVC)

#pragma comment(lib, "hxgraph.lib")
#pragma comment(lib, "hxmatrix.lib")
#pragma comment(lib, "hxopcua.lib")
#pragma comment(lib, "hxzlib.lib")
#pragma comment(lib, "hxenetip.lib")
#pragma comment(lib, "hxdnp.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Platform Object Base Class
//

// Constructor

CPlatformBase::CPlatformBase(void)
{
	StdSetRef();

	piob->RegisterSingleton("dev.platform", 0, (IPlatform *) this);
	}

// Destructor

CPlatformBase::~CPlatformBase(void)
{
	RevokeBaseCommon();
	}

// IUnknown

HRESULT CPlatformBase::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IPlatform);

	return E_NOINTERFACE;
	}

ULONG CPlatformBase::AddRef(void)
{
	StdAddRef();
	}

ULONG CPlatformBase::Release(void)
{
	StdRelease();
	}

// IPlatform

PCTXT CPlatformBase::GetSerial(void)
{
	return m_Serial;
	}

PCTXT CPlatformBase::GetDefName(void)
{
	return m_DefName;
	}

// Implementation

BOOL CPlatformBase::MakeDevice(PCTXT pName, UINT n, ...)
{
	va_list list;

	va_start(list, n);

	while( n-- ) {

		typedef IDevice * (*PFUNC)(void);

		PFUNC pFunc = va_arg(list, PFUNC);

		if( pFunc ) {

			IDevice *pDevice = (*pFunc)();

			if( pDevice->Open() ) {

				piob->RegisterSingleton(pName, 0, pDevice);

				return TRUE;
				}
			
			pDevice->Release();

			continue;
			}

		break;
		}

	AfxTrace("platform: no %s support detected\n", pName);

	return FALSE;
	}

BOOL CPlatformBase::MakeDeviceType(PCTXT pName, UINT n, ...)
{
	va_list list;

	va_start(list, n);

	while( n-- ) {

		typedef IDevice * (*PFUNC)(UINT);

		PFUNC pFunc = va_arg(list, PFUNC);

		if( pFunc ) {

			for( UINT i = 0;; i++ ) {

				IDevice *pDevice = (*pFunc)(i);

				if( pDevice ) {

					if( pDevice->Open() ) {

						piob->RegisterSingleton(pName, i, pDevice);

						continue;
						}

					pDevice->Release();
					}

				if( i ) {

					return TRUE;
					}

				break;
				}

			continue;
			}

		break;
		}

	AfxTrace("platform: no %s support detected\n", pName);

	return FALSE;
	}

void CPlatformBase::RegisterBaseCommon(void)
{
	Register_InputQueue();

	Register_DoubleDataHandler();

	Register_SingleDataHandler();

	Register_HxOpcUa();

	Register_HxGraph();

	Register_HxMatrix();
	
	Register_HxZLib();

	Register_HxEnetIp();

	Register_HxDnp();
	}

void CPlatformBase::RevokeBaseCommon(void)
{
	Revoke_HxDnp();

	Revoke_HxEnetIp();

	Revoke_HxZLib();

	Revoke_HxMatrix();

	Revoke_HxGraph();

	Revoke_HxOpcUa();

	Revoke_SingleDataHandler();

	Revoke_DoubleDataHandler();

	Revoke_InputQueue();
	}

void CPlatformBase::FindSerial(CMacAddr const &Mac)
{
	m_Serial.Printf("%2.2x-%2.2x-%2.2x",
			Mac.m_Addr[3],
			Mac.m_Addr[4],
			Mac.m_Addr[5]
			);

	m_DefName = "red-" + m_Serial;
	}

// End of File
