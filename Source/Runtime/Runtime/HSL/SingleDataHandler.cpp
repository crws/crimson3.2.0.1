
#include "Intern.hpp"

#include "SingleDataHandler.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Single Data Handler
//

// Instantiator

static IUnknown * Create_SingletDataHandler(PCTXT pName)
{
	return New CSingleDataHandler;
	}

// Registration

global void Register_SingleDataHandler(void)
{
	piob->RegisterInstantiator("util.data-s", Create_SingletDataHandler);
	}

global void Revoke_SingleDataHandler(void)
{
	piob->RevokeInstantiator("util.data-s");
	}

// Constructor

CSingleDataHandler::CSingleDataHandler(void)
{
	StdSetRef();

	m_pPort    = NULL;

	m_pTxFlag  = NULL;

	m_pRxFlag  = NULL;

	m_pRxData  = NULL;
	
	m_pTxData  = NULL;

	m_uRxCount = 0;
	
	m_uTxCount = 0;

	m_uRxSize  = 256;
	}

// Destructor

CSingleDataHandler::~CSingleDataHandler(void)
{
	DeleteObjects();

	KillBuffers();
	}

// IUnknown

HRESULT CSingleDataHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDataHandler);

	StdQueryInterface(IDataHandler);

	return E_NOINTERFACE;
	}

ULONG CSingleDataHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG CSingleDataHandler::Release(void)
{
	StdRelease();
	}

// IPortHandler

void METHOD CSingleDataHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
	}

void METHOD CSingleDataHandler::OnRxData(BYTE bData)
{
	UINT uNext = (m_uRxTail + 1) % m_uRxSize;

	if( uNext != m_uRxHead ) {

		m_pRxData[m_uRxTail] = bData;

		m_uRxTail = uNext;

		m_uRxCount++;
		}
	}

void METHOD CSingleDataHandler::OnRxDone(void)
{
	m_pRxFlag->Signal(m_uRxCount);

	m_uRxCount = 0;
	}

BOOL METHOD CSingleDataHandler::OnTxData(BYTE &bData)
{
	if( m_uTxCount ) {

		m_uTxCount--;

		bData = *m_pTxData++;

		return TRUE;
		}
		
	m_pTxFlag->Set();

	return FALSE;
	}

void METHOD CSingleDataHandler::OnTxDone(void)
{
	}

void METHOD CSingleDataHandler::OnOpen(CSerialConfig const &Config)
{
	AllocBuffers();

	CreateObjects();

	m_uRxCount = 0;
	
	m_uRxHead  = 0;

	m_uRxTail  = 0;

	m_pTxData  = NULL;
	
	m_uTxCount = 0;

	m_pTxFlag->Set();
	}

void METHOD CSingleDataHandler::OnClose(void)
{
	m_pTxFlag->Wait(1000);

	DeleteObjects();

	KillBuffers();
	}

void METHOD CSingleDataHandler::OnTimer(void)
{
	}
		
// IDataHandler

void METHOD CSingleDataHandler::SetTxSize(UINT uSize)
{
	}

void METHOD CSingleDataHandler::SetRxSize(UINT uSize)
{
	if( !m_pRxData ) {
	
		m_uRxSize = uSize;
		}
	}

UINT METHOD CSingleDataHandler::Read(UINT uTime)
{
	if( m_pRxFlag->Wait(uTime) ) {

		BYTE bGet = m_pRxData[m_uRxHead];

		m_uRxHead = (m_uRxHead + 1) % m_uRxSize;

		return bGet;
		}

	return NOTHING;
	}

BOOL METHOD CSingleDataHandler::Write(BYTE bData, UINT uTime)
{
	return Write(PCBYTE(&bData), sizeof(bData), uTime) ? TRUE : FALSE;
	}

UINT METHOD CSingleDataHandler::Read(PBYTE pData, UINT uCount, UINT uTime)
{
	for( UINT n = 0; n < uCount; n ++ ) {

		if( m_pRxFlag->Wait(uTime) ) {

			pData[n] = m_pRxData[m_uRxHead];

			m_uRxHead = (m_uRxHead + 1) % m_uRxSize;

			continue;
			}

		return n ? n : NOTHING;
		}

	return uCount;
	}

UINT METHOD CSingleDataHandler::Write(PCBYTE pData, UINT uCount, UINT uTime)
{
	if( m_pTxFlag->Wait(uTime) ) {

		m_pTxData  = pData;

		m_uTxCount = uCount;

		if( m_uTxCount ) {

			m_pTxFlag->Clear();

			m_uTxCount--;

			m_pPort->Send(*m_pTxData++);

			return uCount;
			}

		return 1;
		}

	return 0;
	}

void METHOD CSingleDataHandler::SetBreak(BOOL fBreak)
{
	if( m_pPort ) {
		
		m_pPort->SetBreak(fBreak);
		}
	}

void METHOD CSingleDataHandler::ClearRx(void)
{
	EnableInterrupts(FALSE);

	m_uRxHead  = 0;

	m_uRxTail  = 0;

	m_uRxCount = 0;

	while( m_pRxFlag->Wait(0) );

	EnableInterrupts(TRUE);
	}

void METHOD CSingleDataHandler::ClearTx(void)
{
	}

// Implementation

void CSingleDataHandler::AllocBuffers(void)
{
	m_pRxData = New BYTE [ m_uRxSize ];
	}

void CSingleDataHandler::KillBuffers(void)
{
	if( m_pRxData ) {

		delete [] m_pRxData;

		m_pRxData = NULL;
		}
	}

void CSingleDataHandler::CreateObjects(void)
{
	m_pRxFlag = Create_Semaphore();

	m_pTxFlag = Create_ManualEvent();
	}

void CSingleDataHandler::DeleteObjects(void)
{
	AfxRelease(m_pTxFlag);

	AfxRelease(m_pRxFlag);

	m_pTxFlag = NULL;

	m_pRxFlag = NULL;
	}

// Interrupts

void CSingleDataHandler::EnableInterrupts(BOOL fEnable)
{
	if( m_pPort ) {

		m_pPort->EnableInterrupts(fEnable);
		}
	}

// End of File
