
#include "Intern.hpp"

#include "BaseExecutive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ExecTimer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Base Executive Object
//

// Static Data

UINT CBaseExecutive::m_uAlloc = 1;

// Constructor

CBaseExecutive::CBaseExecutive(void)
{
	StdSetRef();

	m_pHeadTimer = NULL;

	m_pTailTimer = NULL;

	m_pEntropy   = NULL;
	}

// Destructor

CBaseExecutive::~CBaseExecutive(void)
{
	AfxRelease(m_pEntropy);
	}

// IUnknown

HRESULT CBaseExecutive::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IExecutive);

	StdQueryInterface(IExecutive);

	return E_NOINTERFACE;
	}

ULONG CBaseExecutive::AddRef(void)
{
	StdAddRef();
	}

ULONG CBaseExecutive::Release(void)
{
	StdRelease();
	}

// IExecutive

ITimer * CBaseExecutive::CreateTimer(void)
{
	return New CExecTimer(this);
	}

// Operations

void CBaseExecutive::GetEntropy(PBYTE pData, UINT uData)
{
	if( !m_pEntropy ) {

		AfxGetObject("dev.entropy", 0, IEntropy, m_pEntropy);
		}

	if( !m_pEntropy ) {

		memset(pData, BYTE(GetTickCount()), uData);

		return;
		}

	m_pEntropy->GetEntropy(pData, uData);
	}

// Implementation

void CBaseExecutive::ServiceTimers(UINT uTicks)
{
	for( CExecTimer *pTimer = m_pHeadTimer; pTimer; pTimer = pTimer->m_pNext ) {

		pTimer->OnTick(uTicks);
		}
	}

// End of File
