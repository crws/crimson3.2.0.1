
#include "Intern.hpp"

#include "IdentityBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Identity Object Base Class
//

// Constructor

CIdentityBase::CIdentityBase(UINT uAddr)
{
	StdSetRef();

	m_pMem  = NULL;

	m_uAddr = uAddr;

	m_wSize = 8;

	AfxGetObject("fram", 0, ISerialMemory, m_pMem);
	}

// Destructor

CIdentityBase::~CIdentityBase(void)
{
	AfxRelease(m_pMem);
	}

// IUnknown

HRESULT CIdentityBase::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IIdentity);

	StdQueryInterface(IIdentity);

	return E_NOINTERFACE;
	}

ULONG CIdentityBase::AddRef(void)
{
	StdAddRef();
	}

ULONG CIdentityBase::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CIdentityBase::Open(void)
{
	if( GetPropLong(propMagic) != constMagic ) {

		OnInit();

		return Save();
		}

	return TRUE;
	}

// IIdentity

BOOL METHOD CIdentityBase::Save(void)
{
	if( OnSave() ) {

		SetPropLong(propMagic, constMagic);

		SetPropWord(propSize,  m_wSize);

		return TRUE;
		}

	return FALSE;
	}

BOOL METHOD CIdentityBase::Load(void)
{
	if( GetPropLong(propMagic) == constMagic ) {

		return OnLoad();
		}

	return FALSE;
	}

BOOL METHOD CIdentityBase::Import(PCBYTE pData, UINT uSize)
{
	if( *PCDWORD(pData) == constMagic ) {

		if( uSize >= m_wSize ) {

			return PutData(propMagic, pData, uSize);
			}
		}

	return FALSE;
	}

BOOL METHOD CIdentityBase::Export(PBYTE pData, UINT uSize)
{
	if( GetPropLong(propMagic) == constMagic ) {

		return GetPropData(propMagic, pData, uSize);
		}

	return 0;
	}

BYTE METHOD CIdentityBase::GetPropByte(UINT uProp)
{
	BYTE Data = 0;

	GetData(uProp, PBYTE(&Data), sizeof(Data));

	return Data;
	}

BOOL METHOD CIdentityBase::SetPropByte(UINT uProp, BYTE Data)
{
	return PutData(uProp, PBYTE(&Data), sizeof(Data));
	}

WORD METHOD CIdentityBase::GetPropWord(UINT uProp)
{
	WORD Data = 0;

	GetData(uProp, PBYTE(&Data), sizeof(Data));

	return Data;
	}

BOOL METHOD CIdentityBase::SetPropWord(UINT uProp, WORD Data)
{
	return PutData(uProp, PBYTE(&Data), sizeof(Data));
	}

LONG METHOD CIdentityBase::GetPropLong(UINT uProp)
{
	DWORD Data = 0;

	GetData(uProp, PBYTE(&Data), sizeof(Data));

	return Data;
	}

BOOL METHOD CIdentityBase::SetPropLong(UINT uProp, DWORD Data)
{
	return PutData(uProp, PBYTE(&Data), sizeof(Data));
	}

BOOL METHOD CIdentityBase::GetPropData(UINT uProp, PBYTE pData, UINT uSize)
{
	return GetData(uProp, pData, uSize);
	}

BOOL METHOD CIdentityBase::SetPropData(UINT uProp, PCBYTE pData, UINT uSize)
{
	return PutData(uProp, pData, uSize);
	}

// Overrideables

void CIdentityBase::OnInit(void)
{
	SetPropWord(propVersion, 1);
	}

BOOL CIdentityBase::OnLoad(void)
{
	return TRUE;
	}

BOOL CIdentityBase::OnSave(void)
{
	return TRUE;
	}

// Implementation

BOOL METHOD CIdentityBase::GetData(UINT uAddr, PBYTE pData, UINT uCount)
{
	if( m_pMem ) {

		return m_pMem->GetData(m_uAddr + uAddr, pData, uCount);
		}

	return FALSE;
	}

BOOL METHOD CIdentityBase::PutData(UINT uAddr, PCBYTE pData, UINT uCount)
{
	if( m_pMem ) {

		return m_pMem->PutData(m_uAddr + uAddr, pData, uCount);
		}

	return FALSE;
	}

// End of File
