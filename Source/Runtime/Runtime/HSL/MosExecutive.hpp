
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MosExecutive_HPP

#define INCLUDE_MosExecutive_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BaseExecutive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMosExecThread;

//////////////////////////////////////////////////////////////////////////
//
// Modern OS Executive Object
//

class CMosExecutive : public CBaseExecutive, public IClientProcess
{
	public:
		// Constructor
		CMosExecutive(void);

		// Destructor
		~CMosExecutive(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IExecutive
		void      METHOD Open(void);
		void      METHOD Close(void);
		IThread * METHOD CreateThread(PENTRY pfnProc, UINT uLevel, void *pParam, UINT uParam);
		IThread * METHOD CreateThread(IClientProcess *pProc, UINT uIndex, UINT uLevel);
		IThread * METHOD CreateThread(IClientProcess *pProc, UINT uIndex, UINT uLevel, ISemaphore *pTms);
		BOOL      METHOD DestroyThread(IThread *pThread);
		UINT      METHOD ToTicks(UINT uTime);
		UINT      METHOD ToTime(UINT uTicks);
		UINT      METHOD AddNotify(IThreadNotify *pNotify);
		BOOL	  METHOD RemoveNotify(UINT uNotify);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// Timer Support
		void CreateTimer(CExecTimer *pTimer);
		void DeleteTimer(CExecTimer *pTimer);

		// Operations
		void RemoveThread(CMosExecThread *pThread);

	protected:
		// Data Members
		IMutex         * m_pMutex;
		CMosExecThread * m_pHeadThread;
		CMosExecThread * m_pTailThread;
		HTHREAD		 m_hTimer;

		// Overridables
		virtual void AllocThreadObject(CMosExecThread * &pThread) = 0;

		// Creation Herlper
		IThread * CreateThread(PENTRY pfnProc, IClientProcess *pProc, UINT uLevel, void *pParam, UINT uParam, ISemaphore *pTms);

		// Implementation
		void LockData(void);
		void FreeData(void);

		// Entry Point
		static int ClientProc(IThread *pTask, void *pParam, UINT uParam);
	};

// End of File

#endif
