
#include "Intern.hpp"

#include "BufferManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Buffer Manager
//

// Instantiator

IUnknown * Create_BufferManager(void)
{
	return (IBufferManager *) New CBufferManager();
	}

// Constructor

CBufferManager::CBufferManager(void)
{
	StdSetRef();

	m_pLock     = Create_Rutex();

	m_pHeadFree = NULL;

	m_pTailFree = NULL;

	m_pHeadUsed = NULL;

	m_pTailUsed = NULL;

	CreatePool();

	AddDiagCmds();
	}

// Destructor

CBufferManager::~CBufferManager(void)
{
	m_pLock->Release();
	}

// IUnknown

HRESULT CBufferManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IBufferManager);

	StdQueryInterface(IBufferManager);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CBufferManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CBufferManager::Release(void)
{
	StdRelease();
	}

// IBufferManager

CBuffer * CBufferManager::Allocate(UINT uSize)
{
	if( m_pTailFree ) {

		CAutoLock Lock(m_pLock);

		CBuff *p = m_pTailFree;

		if( p ) {

			AfxListRemove(m_pHeadFree, m_pTailFree, p, m_pNextBuff, m_pPrevBuff);

			AfxListAppend(m_pHeadUsed, m_pTailUsed, p, m_pNextBuff, m_pPrevBuff);

			Lock.Lock(FALSE);

			p->Clear();

			return p;
			}
		}

	AfxTrace("\n*** NO BUFFER\n");

	return NULL;
	}

bool CBufferManager::MarkFree(CBuffer *pBuff)
{
	if( !AtomicDecrement(&pBuff->m_nRefs) ) {

		CBuff *p = (CBuff *) pBuff;

		CAutoLock Lock(m_pLock);

		AfxListRemove(m_pHeadUsed, m_pTailUsed, p, m_pNextBuff, m_pPrevBuff);

		AfxListAppend(m_pHeadFree, m_pTailFree, p, m_pNextBuff, m_pPrevBuff);

		return true;
		}

	return false;
	}

void CBufferManager::ShowError(CBuffer *pBuff, PCTXT pText)
{
	AfxTrace("\n*** BUFFER ERROR - %s\n", pText);

	HostBreak();
	}

// IDiagProvider

UINT CBufferManager::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);
		}

	return 0;
	}

// Implementation

void CBufferManager::CreatePool(void)
{
	for( UINT n = 0; n < 1024; n++ ) {

		CBuff *p = New CBuff(this, 2048);

		AfxListAppend( m_pHeadFree,
			       m_pTailFree,
			       p,
			       m_pNextBuff,
			       m_pPrevBuff
			       );
		}
	}

// Diagnostics

bool CBufferManager::AddDiagCmds(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		UINT uProv = pDiag->RegisterProvider(this, "buff");

		pDiag->RegisterCommand(uProv, 1, "status");

		pDiag->Release();

		return true;
		}

	return false;
	}

UINT CBufferManager::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		UINT uFree = 0;

		UINT uUsed = 0;

		if( TRUE ) {

			CAutoLock Lock(m_pLock);

			for( CBuff *pFree = m_pHeadFree; pFree; pFree = pFree->m_pNextBuff ) uFree++;

			for( CBuff *pUsed = m_pHeadUsed; pUsed; pUsed = pUsed->m_pNextBuff ) uUsed++;
			}

		pOut->AddPropList();

		pOut->AddProp("Free",  "%u", uFree);

		pOut->AddProp("Used",  "%u", uUsed);

		pOut->AddProp("Total", "%u", uUsed + uFree);

		pOut->EndPropList();

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

//////////////////////////////////////////////////////////////////////////
//
// Buffer Object
//

CBufferManager::CBuff::CBuff(CBufferManager *pManager, UINT uAlloc) : CBuffer(pManager, uAlloc)
{
	}

// End of File
