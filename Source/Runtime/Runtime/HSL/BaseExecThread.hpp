
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_BaseExecThread_HPP

#define INCLUDE_BaseExecThread_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CBaseExecutive;

//////////////////////////////////////////////////////////////////////////
//
// Executive Thread Base Class
//

class CBaseExecThread : public IThread
{
	public:
		// Constructor
		CBaseExecThread(CBaseExecutive *pExec);

		// Destructor
		virtual ~CBaseExecThread(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IThread
		PVOID METHOD GetObject(void);
		void  METHOD SetName(PCTXT pName);
		PCTXT METHOD GetName(void);
		UINT  METHOD GetIdent(void);
		UINT  METHOD GetIndex(void);
		PVOID METHOD GetPtrParam(void);
		UINT  METHOD GetIntParam(void);
		UINT  METHOD GetExecState(void);
		INT   METHOD GetExitCode(void);
		void  METHOD SetCancelFlag(void);
		void  METHOD CheckCancellation(void);
		DWORD METHOD GetFlags(void);
		void  METHOD SetFlags(DWORD dwMask, DWORD dwData);
		HDATA METHOD GetData(UINT uID);
		BOOL  METHOD SetData(HDATA hData);
		BOOL  METHOD FreeData(UINT uID);
		BOOL  METHOD Advance(void);
		UINT  METHOD AddNotify(IThreadNotify *pNotify);
		void  METHOD SetLibPointer(PVOID pLibData);
		PVOID METHOD GetLibPointer(void);

		// Operations
		BOOL Create(UINT uIndex, PENTRY pfnProc, IClientProcess *pProc, UINT uLevel, void *pParam, UINT uParam, ISemaphore *pTms);
		int  ClientProc(void *pParam, UINT uParam);

	protected:
		// Notify Item
		struct CNotify
		{
			IThreadNotify * m_pNotify;
			UINT		m_uData;
			};

		// Notify List
		typedef CArray <CNotify> CNotifyList;

		// General Data
		ULONG		 m_uRefs;
		CBaseExecutive * m_pExec;
		int		 m_pid;
		int		 m_parent;
		int		 m_guard;
		PGUARD		 m_pGuardProc;
		PVOID		 m_pGuardData;
		UINT		 m_uIndex;
		PENTRY		 m_pfnProc;
		IClientProcess * m_pProc;
		UINT		 m_uLevel;
		PVOID		 m_pParam;
		UINT		 m_uParam;
		ISemaphore     * m_pTms;
		ISemaphore     * m_pTas;
		DWORD		 m_dwFlags;
		CThreadData    * m_pHeadData;
		CThreadData    * m_pTailData;
		int		 m_cancel;
		int		 m_tms;
		int		 m_state;
		int		 m_exit;
		CString		 m_Name;
		void	       * m_impure;
		CNotifyList	 m_NotifyList;

		// Overridables
		virtual BOOL OnCreate(void) = 0;
		virtual void OnChange(int state);

		// Implementation
		void CommonExecute(void);
		void CreateImpure(void);
		void DeleteImpure(void);
		void SetDefaultName(void);
		BOOL SafeToGuard(void);
		BOOL UnwindGuards(void);
		void SetState(UINT state);
		void WaitForNextState(UINT state);
		void SignalNextState(UINT state);
		void BlockOnTms(UINT state);
		void Info(UINT uLevel, PCSTR pText, ...);
	};

// End of File

#endif
