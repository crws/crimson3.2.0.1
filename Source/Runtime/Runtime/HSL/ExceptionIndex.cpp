
#include "Intern.hpp"

#include "ExceptionIndex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Exception Index
//

#if defined(AEON_PLAT_LINUX) || defined(AEON_PLAT_RLOS)

// Locking

#define LOCKED_LOOKUP 0

// Unwind Trace

#define UNWIND_TRACE  0

// Externals

extern int __eh_extra;

// X86 Externals

#if defined(AEON_PROC_X86)

typedef struct dwarf_fde fde;

clink  void        ___register_frame_info(const void *begin, struct object *obj);

clink  void *      ___deregister_frame_info(const void *begin);

clink  const fde * __Unwind_Find_FDE(void *pc, struct dwarf_eh_bases *bases);

#endif

// Instantiator

IUnknown * Create_ExceptionIndex(void)
{
	return (IExceptionIndex *) New CExceptionIndex;
	}

// Constructor

CExceptionIndex::CExceptionIndex(void)
{
	StdSetRef();

	m_pLock = Create_Rutex();

	memset(m_Pool, 0, sizeof(m_Pool));

	m_nLast = 0;

	DiagRegister();
	}

// Destructor

CExceptionIndex::~CExceptionIndex(void)
{
	AfxRelease(m_pLock);
	}

// IUnknown

HRESULT CExceptionIndex::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IExceptionIndex);

	StdQueryInterface(IExceptionIndex);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CExceptionIndex::AddRef(void)
{
	StdAddRef();
	}

ULONG CExceptionIndex::Release(void)
{
	StdRelease();
	}

// IExceptionIndex

PVOID CExceptionIndex::ExAlloc(int nSize)
{
	if( UNWIND_TRACE ) {

		AfxTrace("Exception:\n");
		}

	int nTotal = nSize + __eh_extra;

	if( nTotal <= sizeof(m_Pool[0].m_bData) ) {

		int i = m_nLast;

		for( int n = 0; n < elements(m_Pool); n++ ) {

			CAlloc &Alloc = m_Pool[i];

			if( !AtomicCompAndSwap(&Alloc.m_fLock, 0, 1) ) {

				m_nLast = (i + 1) % elements(m_Pool);

				PBYTE p = Alloc.m_bData;

				memset(p, 0, nTotal);

				return p + __eh_extra;
				}

			i = (i + 1) % elements(m_Pool);
			}
		}

	PBYTE p = PBYTE(malloc(nTotal));

	memset(p, 0, nTotal);

	return p + __eh_extra;
	}

void CExceptionIndex::ExFree(PVOID pExcept)
{
	if( UNWIND_TRACE ) {

		AfxTrace("  Done\n");
		}

	PBYTE pBase = PBYTE(pExcept) - __eh_extra;

	PBYTE pPool = PBYTE(m_Pool);

	if( pBase >= pPool ) {

		CAlloc *pAlloc = (CAlloc *) (pBase - offset(CAlloc, m_bData));

		int n = pAlloc - m_Pool;

		if( n < elements(m_Pool) ) {

			pAlloc->m_fLock = 0;

			m_nLast         = n;

			return;
			}
		}

	free(pBase);
	}

PVOID CExceptionIndex::ArmRegister(PBYTE pc1, PBYTE pc2, PINT64 pi1, PINT64 pi2)
{
	#if defined(AEON_PROC_ARM)

	CRecord *pRec = New CRecord;

	pRec->m_pc1 = pc1;
	pRec->m_pc2 = pc2;
	pRec->m_pi1 = pi1;
	pRec->m_pi2 = pi2;

	CAutoLock Lock(m_pLock);

	m_List.Append(pRec);

	return pRec;

	#endif

	return NULL;
	}

void CExceptionIndex::ArmDeregister(PVOID p)
{
	#if defined(AEON_PROC_ARM)

	CAutoLock Lock(m_pLock);

	CRecord *pRec = (CRecord *) p;

	m_List.Remove(m_List.Find(pRec));

	delete pRec;

	#endif
	}

UINT CExceptionIndex::ArmLookup(UINT Addr, PINT pSize)
{
	#if defined(AEON_PROC_ARM)

	CAutoLock Lock(m_pLock, LOCKED_LOOKUP);

	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CRecord const *pRec = m_List[n];

		if( Addr >= UINT(pRec->m_pc1) && Addr <= UINT(pRec->m_pc2) ) {

			if( UNWIND_TRACE ) {

				extern  bool ArmGetFuncName(char *, UINT, DWORD, DWORD, UINT);

				char sName[128];

				ArmGetFuncName(sName, sizeof(sName), Addr - 2, 0, 3);

				AfxTrace("  Unwind %s\n", sName);
				}

			*pSize = pRec->m_pi2 - pRec->m_pi1;

			return UINT(pRec->m_pi1);
			}
		}

	if( UNWIND_TRACE ) {

		AfxTrace("  Unwind %8.8X\n", Addr);
		}

	#endif

	return 0;
	}

PVOID METHOD CExceptionIndex::X86Register(PCVOID begin, struct object *obj)
{
	#if defined(AEON_PROC_X86)

	CAutoLock Lock(m_pLock);

	___register_frame_info(begin, obj);

	#endif

	return PVOID(begin);
	}

void METHOD CExceptionIndex::X86Deregister(PVOID p)
{
	#if defined(AEON_PROC_X86)

	CAutoLock Lock(m_pLock);

	___deregister_frame_info(p);

	#endif
	}

void METHOD CExceptionIndex::X86Lookup(struct dwarf_fde const **ppfde, void *pc, struct dwarf_eh_bases *bases)
{
	#if defined(AEON_PROC_X86)

	CAutoLock Lock(m_pLock, LOCKED_LOOKUP);

	*ppfde = __Unwind_Find_FDE(pc, bases);

	#endif
	}

// IDiagProvider

UINT CExceptionIndex::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(AEON_PROC_ARM)

	switch( pCmd->GetCode() ) {

		case 1:
			return DiagList(pOut, pCmd);
		}

	#endif

	return 0;
	}

// Diagnostics

BOOL CExceptionIndex::DiagRegister(void)
{
	#if defined(AEON_PROC_ARM)

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		UINT uProv = pDiag->RegisterProvider(this, "exidx");

		pDiag->RegisterCommand(uProv, 1, "list");

		pDiag->Release();

		return TRUE;
		}

	#endif

	return FALSE;
	}

UINT CExceptionIndex::DiagList(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(AEON_PROC_ARM)

	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(4);

		pOut->SetColumn(0, "CStart",  "%8.8X");
		pOut->SetColumn(1, "CEnd",    "%8.8X");
		pOut->SetColumn(2, "IStart",  "%8.8X");
		pOut->SetColumn(3, "IEnd",    "%8.8X");

		pOut->AddHead();

		pOut->AddRule('-');

		for( UINT n = 0; n < m_List.GetCount(); n++ ) {

			CRecord const *pRec = m_List[n];

			pOut->AddRow();

			pOut->SetData(0, pRec->m_pc1);
			pOut->SetData(1, pRec->m_pc2);
			pOut->SetData(2, pRec->m_pi1);
			pOut->SetData(3, pRec->m_pi2);

			pOut->EndRow();
			}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
		}

	pOut->Error(NULL);

	#endif

	return 1;
	}

// End of File

#endif
