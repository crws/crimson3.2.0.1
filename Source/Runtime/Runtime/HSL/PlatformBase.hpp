
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PlatformBase_HPP

#define INCLUDE_PlatformBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Filing System Interfaces
//

#include "../../../StdEnv/IFileSystem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Platform Object Base Class
//

class CPlatformBase : public IDevice, public IPlatform
{
	public:
		// Constructor
		CPlatformBase(void);

		// Destructor
		virtual ~CPlatformBase(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPlatform
		PCTXT METHOD GetSerial (void);
		PCTXT METHOD GetDefName(void);

	protected:
		// Data Members
		ULONG   m_uRefs;
		CString m_Serial;
		CString m_DefName;

		// Implementation
		BOOL MakeDevice(PCTXT pName, UINT n, ...);
		BOOL MakeDeviceType(PCTXT pName, UINT n, ...);
		void RegisterBaseCommon(void);
		void RevokeBaseCommon(void);
		void FindSerial(CMacAddr const &Mac);
	};

// End of File

#endif
