
#include "Intern.hpp"

#include "ObjectBroker.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/1gCkE

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ComProxy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Broker
//

// Instantiator

IObjectBroker * Create_ObjectBroker(void)
{
	AfxInstantiate(IObjectBroker, CObjectBroker);
	}

// Constructor

CObjectBroker::CObjectBroker(void)
{
	piob    = this;

	m_fLock = FALSE;

	m_pLock = NULL;

	StdSetRef();
	}

// Destructor

CObjectBroker::~CObjectBroker(void)
{
	AfxAssert(!m_fLock);

	FreeList();
	}

// IUnknown

HRESULT CObjectBroker::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IObjectBroker);

	StdQueryInterface(IObjectBroker);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CObjectBroker::AddRef(void)
{
	StdAddRef();
	}

ULONG CObjectBroker::Release(void)
{
	StdRelease();
	}

// IObjectBroker

HRESULT CObjectBroker::RegisterDiagnostics(void)
{
	DiagRegister();

	return S_OK;
	}

HRESULT CObjectBroker::RegisterSingleton(PCSTR pName, UINT uInst, IUnknown *punkObject)
{
	CPrintf   Full("%s.%u", pName, uInst);
	
	CAutoLock Lock(m_pLock, m_fLock);

	if( m_List.Failed(m_Dict[Full]) ) {

		CObject *pObject = New CObject;

		pObject->m_Name            = pName;
		pObject->m_Inst            = uInst;
		pObject->m_punkObject      = punkObject;
		pObject->m_pfnInstantiator = NULL;

		INDEX Index = m_List.Append(pObject);

		m_Dict.Insert(Full, Index);

		for( PCTXT pFind = pName; (pFind = strchr(pFind, '.')); pFind++ ) {

			CPrintf Name("%s.%u", pFind+1, uInst);

			m_Dict.Insert(Name, Index);
			}

		return S_OK;
		}

	AfxAssert(FALSE);

	return E_FAIL;
	}

HRESULT CObjectBroker::RegisterInstantiator(PCSTR pName, INSTANTIATOR pfnInstantiator)
{
	CPrintf   Full("%s.%u", pName, 0);

	CAutoLock Lock(m_pLock, m_fLock);

	if( m_List.Failed(m_Dict[Full]) ) {

		CObject *pObject = New CObject;

		pObject->m_Name            = pName;
		pObject->m_Inst            = 0;
		pObject->m_punkObject      = NULL;
		pObject->m_pfnInstantiator = pfnInstantiator;

		INDEX Index = m_List.Append(pObject);

		m_Dict.Insert(Full, Index);

		for( PCTXT pFind = pName; (pFind = strchr(pFind, '.')); pFind++ ) {

			CPrintf Name("%s.%u", pFind+1, 0);

			m_Dict.Insert(Name, Index);
			}

		return S_OK;
		}

	AfxAssert(FALSE);

	return E_FAIL;
	}

HRESULT CObjectBroker::GetObject(PCSTR pName, UINT uInst, REFIID iid, void **ppObject)
{
	AfxAssert(pName);

	AfxAssert(ppObject);

	CPrintf   Full("%s.%u", pName, uInst);

	CAutoLock Lock(m_pLock, m_fLock);

	INDEX     Index;

	if( !m_List.Failed(Index = m_Dict[Full]) ) {

		CObject const *pObject = m_List[Index];

		if( pObject->m_punkObject ) {

			IUnknown *pUnk = pObject->m_punkObject;

			IUnknown *pObj = NULL;

			HRESULT   hr   = pUnk->QueryInterface(iid, (void **) &pObj);

			if( hr == S_OK ) {

				if( !((CGuid &) iid).IsUnknown() ) {

					*ppObject = New CComProxy(Full, pObj, pUnk, iid);

					pObj->Release();
					}
				else
					*ppObject = pObj;

				return S_OK;
				}

			*ppObject = NULL;

			return hr;
			}

		AfxAssert(FALSE);
		}

	*ppObject = NULL;

	return E_FAIL;
	}

HRESULT CObjectBroker::NewObject(PCSTR pName, REFIID iid, void **ppObject)
{
	AfxAssert(pName);

	AfxAssert(ppObject);

	CPrintf   Full("%s.%u", pName, 0);

	CAutoLock Lock(m_pLock, m_fLock);

	INDEX     Index;

	if( !m_List.Failed(Index = m_Dict[Full]) ) {

		CObject const *pObject = m_List[Index];

		if( !pObject->m_punkObject ) {

			IUnknown *pUnk = (pObject->m_pfnInstantiator)(Full);

			if( pUnk ) {

				IUnknown *pObj = NULL;

				HRESULT   hr   = pUnk->QueryInterface(iid, (void **) &pObj);

				if( hr == S_OK ) {

					*ppObject = pObj;

					pObj->Release();

					return S_OK;
					}

				pUnk->Release();

				*ppObject = NULL;

				return hr;
				}
			}

		AfxAssert(FALSE);
		}

	*ppObject = NULL;

	return E_FAIL;
	}

HRESULT CObjectBroker::RevokeSingleton(PCSTR pName, UINT uInst)
{
	CPrintf   Full("%s.%u", pName, uInst);

	CAutoLock Lock(m_pLock, m_fLock);

	INDEX     Index;

	if( !m_List.Failed(Index = m_Dict[Full]) ) {

		CObject *pObject = m_List[Index];

		if( pObject->m_punkObject ) {

			Revoke(Index, pObject, Full);

			return S_OK;
			}

		AfxAssert(FALSE);
		}

	return E_FAIL;
	}

HRESULT CObjectBroker::RevokeInstantiator(PCSTR pName)
{
	CPrintf   Full("%s.%u", pName, 0);

	CAutoLock Lock(m_pLock, m_fLock);
	
	INDEX     Index;

	if( !m_List.Failed(Index = m_Dict[Full]) ) {

		CObject *pObject = m_List[Index];

		if( !pObject->m_punkObject ) {

			Revoke(Index, pObject, Full);

			return S_OK;
			}

		AfxAssert(FALSE);
		}

	return E_FAIL;
	}

HRESULT CObjectBroker::RevokeGroup(PCSTR pName)
{
	CAutoLock Lock(m_pLock, m_fLock);

	for( INDEX Index = m_List.GetTail(); !m_List.Failed(Index); ) {

		CObject *pObject = m_List[Index];

		if( pObject->m_Name.StartsWith(pName) ) {

			// Revoke will move Index to the correct
			// previous entry in the list, accounting
			// for any ripple effects...

			Revoke(Index, pObject, "");
			}
		else
			m_List.GetPrev(Index);
		}

	return E_FAIL;
	}

HRESULT CObjectBroker::SerializeAccess(BOOL fLock)
{
	if( m_fLock != fLock ) {

		if( fLock ) {

			NewObject("exec.qutex", AfxAeonIID(IMutex), (void **) &m_pLock);

			m_fLock = TRUE;
			}
		else {
			m_fLock = FALSE;

			m_pLock->Release();

			m_pLock = NULL;
			}

		return S_OK;
		}

	AfxAssert(FALSE);

	return E_FAIL;
	}

// IDiagProvider

UINT CObjectBroker::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagList(pOut, pCmd);

		case 2:
			return DiagQuery(pOut, pCmd);
		}

	return 0;
	}

// Diagnostics

BOOL CObjectBroker::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		UINT uProv = pDiag->RegisterProvider(this, "ob");

		pDiag->RegisterCommand(uProv, 1, "list");

		pDiag->RegisterCommand(uProv, 2, "query");

		pDiag->Release();

		return TRUE;
		}

	return FALSE;
	}

UINT CObjectBroker::DiagList(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() <= 1 ) {

		PCTXT pName = pCmd->GetArg(0);

		pOut->AddTable(4);

		pOut->SetColumn(0, "Name",     "%s" );
		pOut->SetColumn(1, "Inst",     "%-u");
		pOut->SetColumn(2, "Type",     "%s" );
		pOut->SetColumn(3, "IUnknown", "%s" );

		pOut->AddHead();

		pOut->AddRule('-');

		CAutoLock Lock(m_pLock, m_fLock);

		for( INDEX n = m_Dict.GetHead(); !m_Dict.Failed(n); m_Dict.GetNext(n) ) {

			INDEX          Index   = m_Dict.GetData(n);

			CObject const *pObject = m_List[Index];

			if( pObject->m_Name.StartsWith(pName) ) {

				if( m_Dict.GetName(n).StartsWith(pObject->m_Name) ) {

					pOut->AddRow();

					pOut->SetData(0, PCTXT(pObject->m_Name));
		
					pOut->SetData(1, pObject->m_Inst);

					if( pObject->m_pfnInstantiator ) {

						pOut->SetData(2, "dynamic");

						pOut->SetData(3, "n/a");
						}
					else {
						pOut->SetData(2, "static");

						pOut->SetData(3, PCTXT(CPrintf("%8.8X", pObject->m_punkObject)));
						}

					pOut->EndRow();
					}
				}
			}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CObjectBroker::DiagQuery(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 || pCmd->GetArgCount() == 2 ) {

		PCTXT     pName = pCmd->GetArg(0);

		UINT      uInst = (pCmd->GetArgCount() == 2) ? atoi(pCmd->GetArg(1)) : 0;

		CPrintf   Full  = CPrintf(strchr(pName, '.') ? "%s" : "%s.%u", pName, uInst);

		CAutoLock Lock(m_pLock, m_fLock);

		INDEX     Index;

		if( !m_List.Failed(Index = m_Dict[Full]) ) {

			CObject const *pObject = m_List[Index];

			IUnknown      *pUnk    = NULL;

			if( !pObject->m_punkObject ) {

				pUnk = (pObject->m_pfnInstantiator)(Full);
				}
			else
				pUnk = pObject->m_punkObject;

			bool t = false;

			for( UINT f = 1; f < 10; f++ ) {

				for( UINT n = 1; n < 20; n++ ) {

					IUnknown *pTest = NULL;

					pUnk->QueryInterface(CGuid(f, n), (void **) &pTest);

					if( pTest ) {

						if( !t ) {

							pOut->AddTable(2);

							pOut->SetColumn(0, "SIID", "%2u.%-u");

							pOut->SetColumn(1, "Full", "%s");

							pOut->AddHead();

							pOut->AddRule('-');

							pOut->AddRow();

							pOut->SetData(0, 0, 0);

							pOut->SetData(1, "{00000000-0000-0000-C000-000000000046}");

							pOut->EndRow();

							t = true;
							}

						pOut->AddRow();

						pOut->SetData(0, f, n);

						pOut->SetData(1, PCTXT(CGuid(f, n).GetAsText()));

						pOut->EndRow();

						pTest->Release();
						}
					}
				}

			if( !pObject->m_punkObject ) {

				pUnk->Release();
				}

			if( t ) {

				pOut->AddRule('-');

				pOut->EndTable();

				return 0;
				}

			pOut->Print("No interfaces supported\n");

			return 0;
			}

		pOut->Error("no such object %s", PCTXT(Full));

		return 1;
		}

	pOut->Error("params are <name> [<instance>]");

	return 1;
	}

// Implementation

void CObjectBroker::Revoke(INDEX &Index, CObject *pObject, CString Full)
{
	if( Full.IsEmpty() ) {

		Full.Printf("%s.%u", PCTXT(pObject->m_Name), pObject->m_Inst);
		}

	for( PCTXT pFind = pObject->m_Name; (pFind = strchr(pFind, '.')); pFind++ ) {

		CPrintf Name("%s.%u", pFind+1, pObject->m_Inst);

		m_Dict.Remove(Name);
		}

	if( pObject->m_punkObject ) {

		pObject->m_punkObject->Release();
		}

	// This is important. We need to update the Index to
	// the previous item in the list to enable some callers
	// to continue enumerating the list. But we have to do
	// it after any singleton is deleted as that operation
	// might remove other items from our list!

	m_Dict.Remove(Full);

	INDEX Work = Index;

	m_List.GetPrev(Index);

	m_List.Remove (Work);

	delete pObject;
	}

void CObjectBroker::FreeList(void)
{
	for( INDEX n = m_List.GetHead(); !m_List.Failed(n); m_List.GetNext(n) ) {

		CObject const *pObject = m_List[n];

		// These should not happen as everyone should already
		// have revoked their own objects. They're not fatal,
		// but they indicate poor structure elsewhere.

		HostBreak();

		if( pObject->m_punkObject ) {

			pObject->m_punkObject->Release();
			}

		delete pObject;
		}

	// This will fire after any warning message above.

	AfxAssert(!m_List.GetCount());

	m_List.Empty();

	m_Dict.Empty();
	}

// End of File
