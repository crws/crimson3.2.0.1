
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_SingleDataHandler_HPP

#define INCLUDE_SingleDataHandler_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

//////////////////////////////////////////////////////////////////////////
//
// Single Data Handler
//

class CSingleDataHandler : public IDataHandler
{
	public:
		// Constructor
		CSingleDataHandler(void);

		// Destructor
		~CSingleDataHandler(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);
		
		// IDataHandler
		void METHOD SetTxSize(UINT uSize);
		void METHOD SetRxSize(UINT uSize);
		UINT METHOD Read(UINT uTime);
		BOOL METHOD Write(BYTE bData, UINT uTime);
		UINT METHOD Read(PBYTE pData, UINT uCount, UINT uTime);
		UINT METHOD Write(PCBYTE pData, UINT uCount, UINT uTime);
		void METHOD SetBreak(BOOL fBreak);
		void METHOD ClearRx(void);
		void METHOD ClearTx(void);

	protected:
		// Data Members
		ULONG         m_uRefs;
		IPortObject * m_pPort;
		ISemaphore  * m_pRxFlag;
		PBYTE	      m_pRxData;
		UINT	      m_uRxSize;
		UINT	      m_uRxCount;
		UINT	      m_uRxHead;
		UINT	      m_uRxTail;
		IEvent      * m_pTxFlag;
		PCBYTE	      m_pTxData;
		UINT	      m_uTxCount;

		// Implementation
		void AllocBuffers(void);
		void KillBuffers(void);
		void CreateObjects(void);
		void DeleteObjects(void);

		// Interrupts
		void EnableInterrupts(BOOL fEnable);
	};

// End of File

#endif
