
#include "Intern.hpp"

#include "DoubleDataHandler.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Double Data Handler
//

// Instantiator

static IUnknown * Create_DoubleDataHandler(PCTXT pName)
{
	return New CDoubleDataHandler;
	}

// Reigistration

global void Register_DoubleDataHandler(void)
{
	piob->RegisterInstantiator("util.data-d", Create_DoubleDataHandler);
	}

global void Revoke_DoubleDataHandler(void)
{
	piob->RevokeInstantiator("util.data-d");
	}

// Constructor

CDoubleDataHandler::CDoubleDataHandler(void)
{
	StdSetRef();

	m_pPort    = NULL;

	m_pTxFlag  = NULL;

	m_pRxFlag  = NULL;

	m_pRxData  = NULL;
	
	m_pTxData  = NULL;

	m_pTxDone  = NULL;

	m_uRxCount = 0;
	
	m_uTxCount = 0;

	m_uRxSize  = 256;

	m_uTxSize  = 256;
	}

// Destructor

CDoubleDataHandler::~CDoubleDataHandler(void)
{
	DeleteObjects();

	KillBuffers();
	}

// IUnknown

HRESULT CDoubleDataHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDataHandler);

	StdQueryInterface(IDataHandler);

	return E_NOINTERFACE;
	}

ULONG CDoubleDataHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG CDoubleDataHandler::Release(void)
{
	StdRelease();
	}

// IPortHandler

void METHOD CDoubleDataHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
	}

void METHOD CDoubleDataHandler::OnRxData(BYTE bData)
{
	UINT uNext = (m_uRxTail + 1) % m_uRxSize;

	if( uNext != m_uRxHead ) {

		m_pRxData[m_uRxTail] = bData;

		m_uRxTail = uNext;

		m_uRxCount++;
		}
	}

void METHOD CDoubleDataHandler::OnRxDone(void)
{
	m_pRxFlag->Signal(m_uRxCount);

	m_uRxCount = 0;
	}

BOOL METHOD CDoubleDataHandler::OnTxData(BYTE &bData)
{
	if( m_uTxHead == m_uTxTail ) {

		if( m_fTxBusy ) {

			m_pTxDone->Set();

			m_fTxBusy = FALSE;
			}

		return FALSE;
		}
	else {
		bData     = m_pTxData[m_uTxHead];

		m_uTxHead = (m_uTxHead + 1) % m_uTxSize;

		m_uTxCount++;

		return TRUE;
		}
	}

void METHOD CDoubleDataHandler::OnTxDone(void)
{
	m_pTxFlag->Signal(m_uTxCount);

	m_uTxCount = 0;
	}

void METHOD CDoubleDataHandler::OnOpen(CSerialConfig const &Config)
{
	AllocBuffers();

	CreateObjects();

	m_fBatch   = FALSE && !(Config.m_uFlags & flagNoBatchSend);

	m_uRxCount = 0;
	
	m_uRxHead  = 0;
	
	m_uRxTail  = 0;

	m_uTxCount = 0;

	m_uTxHead  = 0;
	
	m_uTxTail  = 0;
	
	m_fTxBusy  = FALSE;

	m_pTxDone->Set();
	}

void METHOD CDoubleDataHandler::OnClose(void)
{
	m_pTxFlag->Wait(1000);

	DeleteObjects();

	KillBuffers();
	}

void METHOD CDoubleDataHandler::OnTimer(void)
{
	}
		
// IDataHandler

void METHOD CDoubleDataHandler::SetTxSize(UINT uSize)
{
	if( !m_pTxData ) {
	
		m_uTxSize = uSize;
		}
	}

void METHOD CDoubleDataHandler::SetRxSize(UINT uSize)
{
	if( !m_pRxData ) {
	
		m_uRxSize = uSize;
		}
	}

UINT METHOD CDoubleDataHandler::Read(UINT uTime)
{
	if( !m_fTxBusy ) {

		TxStart(1);
		}

	if( m_pRxFlag->Wait(uTime) ) {

		BYTE bGet = m_pRxData[m_uRxHead];

		m_uRxHead = (m_uRxHead + 1) % m_uRxSize;

		return bGet;
		}

	return NOTHING;
	}

BOOL METHOD CDoubleDataHandler::Write(BYTE bData, UINT uTime)
{
	if( m_pTxFlag->Wait(uTime) ) {

		EnableInterrupts(FALSE);

		if( m_fTxBusy ) {

			TxStore(bData);

			EnableInterrupts(TRUE);
			}
		else {
			EnableInterrupts(TRUE);

			if( m_fBatch ) {
				
				TxStore(bData);

				TxStart(4);
				}
			else {
				TxFirst(bData);
				}
			}

		return TRUE;
		}

	return FALSE;
	}

UINT METHOD CDoubleDataHandler::Read(PBYTE pData, UINT uCount, UINT uTime)
{
	for( UINT n = 0; n < uCount; n ++ ) {

		if( m_pRxFlag->Wait(uTime) ) {

			pData[n]  = m_pRxData[m_uRxHead];

			m_uRxHead = (m_uRxHead + 1) % m_uRxSize;

			continue;
			}

		return n;
		}

	return uCount;
	}

UINT METHOD CDoubleDataHandler::Write(PCBYTE pData, UINT uCount, UINT uTime)
{
	UINT uSent = 0;

	if( !uCount ) {

		m_pTxDone->Wait(uTime);

		return 1;
		}

	if( !m_fTxBusy && uCount > 1 ) {

		UINT uBulk = Min(uCount - 1, m_uTxSize - 1);

		memcpy(m_pTxData, pData + 1, uBulk);

		m_uTxHead = 0;

		m_uTxTail = uBulk;

		TxClaim(uBulk);

		m_pTxDone->Clear();

		m_fTxBusy = TRUE;

		m_pPort->Send(*pData);

		uSent  += uBulk + 1;

		pData  += uBulk + 1;

		uCount -= uBulk + 1;
		}

	while( uCount-- ) {

		if( !Write(*pData++, uTime) ) {

			break;
			}

		uSent++;
		}

	return uSent;
	}

void METHOD CDoubleDataHandler::SetBreak(BOOL fBreak)
{
	if( m_pPort ) {
		
		m_pPort->SetBreak(fBreak);
		}
	}

void METHOD CDoubleDataHandler::ClearRx(void)
{
	EnableInterrupts(FALSE);

	m_uRxHead  = 0;

	m_uRxTail  = 0;

	m_uRxCount = 0;

	while( m_pRxFlag->Wait(0) );

	EnableInterrupts(TRUE);
	}

void METHOD CDoubleDataHandler::ClearTx(void)
{
	}

// Implementation

void CDoubleDataHandler::AllocBuffers(void)
{
	m_pRxData = New BYTE [ m_uRxSize ];

	m_pTxData = New BYTE [ m_uTxSize ];
	}

void CDoubleDataHandler::KillBuffers(void)
{
	if( m_pRxData ) {

		delete [] m_pRxData;

		m_pRxData = NULL;
		}

	if( m_pTxData ) {

		delete [] m_pTxData;

		m_pTxData = NULL;
		}
	}

void CDoubleDataHandler::CreateObjects(void)
{
	m_pRxFlag = Create_Semaphore();

	m_pTxFlag = Create_Semaphore();

	m_pTxDone = Create_ManualEvent();

	m_pTxFlag->Signal(m_uTxSize - 16);
	}

void CDoubleDataHandler::DeleteObjects(void)
{
	AfxRelease(m_pTxDone);

	AfxRelease(m_pTxFlag);

	AfxRelease(m_pRxFlag);

	m_pTxDone = NULL;

	m_pTxFlag = NULL;

	m_pRxFlag = NULL;
	}

// Transmit Helpers

void CDoubleDataHandler::TxStore(BYTE bData)
{
	m_pTxData[m_uTxTail] = bData;

	m_uTxTail            = (m_uTxTail + 1) % m_uTxSize;
	}

void CDoubleDataHandler::TxFirst(BYTE bData)
{
	m_pTxFlag->Signal(1);

	m_pTxDone->Clear();

	m_fTxBusy = true;

	m_pPort->Send(bData);
	}

BOOL CDoubleDataHandler::TxStart(UINT uLimit)
{
	UINT uCount = (m_uTxTail + m_uTxSize - m_uTxHead) % m_uTxSize;

	if( uCount >= uLimit ) {

		BYTE bData = m_pTxData[m_uTxHead];

		m_uTxHead  = (m_uTxHead + 1) % m_uTxSize;

		TxFirst(bData);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDoubleDataHandler::TxClaim(UINT uCount)
{
	while( uCount-- ) {
		
		if( !m_pTxFlag->Wait(FOREVER) ) {

			return FALSE;
			}
		}

	return TRUE;
	}

// Interrupts

void CDoubleDataHandler::EnableInterrupts(BOOL fEnable)
{
	if( m_pPort ) {

		m_pPort->EnableInterrupts(fEnable);
		}
	}

// End of File
