
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_InputQueue_HPP

#define INCLUDE_InputQueue_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// LINK

//////////////////////////////////////////////////////////////////////////
//
// Input Queue
//

class CInputQueue : public IInputQueue
{
	public:
		// Constructor
		CInputQueue(BOOL fDisp);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IInputQueue
		INPUT METHOD Read(UINT uTime);
		bool  METHOD Store(INPUT Input);
		void  METHOD Clear(void);

	protected:
		// Data Members
		ULONG	     m_uRefs;
		BOOL	     m_fDisp;
		IMutex     * m_pLock;
		ISemaphore * m_pFlag;
		INPUT        m_Data[128];
		UINT	     m_uHead;
		UINT	     m_uTail;
	};

// End of File

#endif
