
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_BaseFileSupport_HPP

#define INCLUDE_BaseFileSupport_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <sys/reent.h>

#include <sys/stat.h>

#include <fcntl.h>

//////////////////////////////////////////////////////////////////////////
//
// Base File System Support
//

class CBaseFileSupport : public IFileSupport, public IThreadNotify
{
	public:
		// Constructor
		CBaseFileSupport(void);

		// Destructor
		~CBaseFileSupport(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IFileSupport
		int    METHOD IsConsole(int fd);
		int    METHOD Rename(char const *from, char const *to);
		int    METHOD Unlink(char const *name);
		int    METHOD Stat(char const *name, struct stat *buffer);
		int    METHOD UTime(char const *name, time_t time);
		int    METHOD ChMod(char const *name, mode_t mode);
		int    METHOD Open(char const *name, int oflag, int pmode);
		int    METHOD Close(int fd);
		int    METHOD FStat(int fd, struct stat *buffer);
		int    METHOD Read(int fd, void *buffer, unsigned int count);
		int    METHOD Write(int fd, void const *buffer, unsigned int count);
		int    METHOD LSeek(int fd, long offset, int origin);
		int    METHOD FTruncate(int fd, long size);
		int    METHOD IoCtl(int fd, int func, void *data);
		char * METHOD GetCwd(char *buff, size_t size);
		int    METHOD ChDir(char const *name);
		int    METHOD RmDir(char const *name);
		int    METHOD MkDir(char const *name);
		int    METHOD ScanDir(CScanDirArgs const &Args);
		int    METHOD Sync(void);
		int    METHOD MapName(char *name, int add);

		// IThreadNotify
		UINT METHOD OnThreadCreate(IThread *pThread, UINT uIndex);
		void METHOD OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam);

	protected:
		// Forware Declaration
		struct CThreadInfo;

		// Temp Record
		struct CTempRecord
		{
			bool	m_fUsed;
			char	m_sName[32];
			DWORD	m_dwSize;
			DWORD	m_dwAlloc;
			PBYTE	m_pData;
			UINT    m_uRefs;
			};

		// Temp Handle
		struct CTempHandle
		{
			bool	      m_fUsed;
			bool          m_fKill;
			UINT	      m_uRecord;
			DWORD	      m_dwPtr;
			CThreadInfo * m_pInfo;
			CTempHandle * m_pNext;
			CTempHandle * m_pPrev;
			};

		// Host Handle
		struct CHostHandle
		{
			bool	      m_fUsed;
			PVOID	      m_hFile;
			CThreadInfo * m_pInfo;
			CHostHandle * m_pNext;
			CHostHandle * m_pPrev;
			};

		// Thread Info
		struct CThreadInfo
		{
			CString	      m_Cwd;
			CTempHandle * m_pHeadTemp;
			CTempHandle * m_pTailTemp;
			CHostHandle * m_pHeadHost;
			CHostHandle * m_pTailHost;
			};

		// Data Members
		ULONG       m_uRefs;
		IMutex    * m_pLock;
		char        m_HostSep;
		CString	    m_HostBase;
		CString     m_HostRoot;
		CTempRecord m_TempRecord[32];
		CTempHandle m_TempHandle[32];
		CHostHandle m_HostHandle[64];
		UINT	    m_uTempBase;
		UINT	    m_uHostBase;

		// Host Hooks
		virtual bool  HostRename(CString const &From, CString const &To)                                              = 0;
		virtual bool  HostUnlink(CString const &Path)								      = 0;
		virtual bool  HostStat(CString const &Path, struct stat *buffer)                                              = 0;
		virtual bool  HostUTime(CString const &Path, time_t time)						      = 0;
		virtual bool  HostChMod(CString const &Path, mode_t mode)						      = 0;
		virtual PVOID HostOpen(CString const &Path, int oflag, int pmode)                                             = 0;
		virtual void  HostClose(PVOID hFile)                                                                          = 0;
		virtual bool  HostStat(PVOID hFile, struct stat *buffer)                                                      = 0;
		virtual int   HostRead(PVOID hFile, void *buffer, unsigned int count)                                         = 0;
		virtual int   HostWrite(PVOID hFile, void const *buffer, unsigned int count)                                  = 0;
		virtual int   HostSeek(PVOID hFile, long offset, int origin)                                                  = 0;
		virtual bool  HostTruncate(PVOID hFile, DWORD size)                                                           = 0;
		virtual int   HostIoCtl(PVOID hFile, int func, void *data)						      = 0;
		virtual bool  HostIsValidDir(CString const &Path)                                                             = 0;
		virtual bool  HostRmDir(CString const &Path)                                                                  = 0;
		virtual bool  HostMkDir(CString const &Path)                                                                  = 0;
		virtual int   HostScanDir(CString const &Path, struct dirent ***list, int (*selector)(struct dirent const *)) = 0;
		virtual bool  HostSync(void)                                                                                  = 0;

		// Implementation
		bool    IsRoot(CString const &Path);
		bool	IsHost(int fd);
		bool	IsTemp(int fd);
		bool	IsTemp(char const *name);
		void    ExtendTempFile(CTempRecord &Record, DWORD dwSize);
		CString MakeHost(CString Path, bool fAdd);
		CString MakeAeon(CString Path);
		CString GetCwd(void);
		void    EmptyDirectory(CString const &Path);
	};

// End of File

#endif
