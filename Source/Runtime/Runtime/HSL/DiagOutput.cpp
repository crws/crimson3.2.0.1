
#include "Intern.hpp"

#include "DiagOutput.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Diagnostics Output
//

// Constructor

CDiagOutput::CDiagOutput(IDiagConsole *pConsole, PCTXT pName)
{
	// TODO -- Track states to ensure correct
	// ordering of calls to the table APIs.

	StdSetRef();

	m_pConsole = pConsole;

	m_pName    = pName;

	m_fPrint   = false;
	
	m_fTable   = false;

	m_State    = stateNormal;
	}

// IUnknown

#if defined(AEON_ENVIRONMENT)

HRESULT CDiagOutput::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagOutput);

	StdQueryInterface(IDiagOutput);

	return E_NOINTERFACE;
	}

ULONG CDiagOutput::AddRef(void)
{
	StdAddRef();
	}

ULONG CDiagOutput::Release(void)
{
	StdRelease();
	}

#endif

// IDiagOutput

void CDiagOutput::Error(PCTXT pText, ...)
{
	m_fTable = false;

	Write(m_pName);

	Write(": ");

	if( pText ) {

		char sText[256];

		va_list pArgs;

		va_start(pArgs, pText);

		vsprintf(sText, pText, pArgs);

		va_end(pArgs);

		Write(sText);

		Write("\n");

		return;
		}

	Write("syntax error\n");
	}

void CDiagOutput::Print(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	VPrint(pText, pArgs);

	va_end(pArgs);
	}

void CDiagOutput::VPrint(PCTXT pText, va_list pArgs)
{
	m_fTable = false;

	char sText[256];

	vsprintf(sText, pText, pArgs);

	Write(sText);
	}

void CDiagOutput::Dump(PCVOID pData, UINT uCount)
{
	Dump(pData, uCount, NOTHING);
	}

void CDiagOutput::Dump(PCVOID pData, UINT uCount, UINT uBase)
{
	if( pData ) {

		m_fTable = false;

		PCBYTE p = PCBYTE(pData);

		UINT   s = 0;

		for( UINT n = 0; n < uCount; n++ ) {

			if( n % 0x10 == 0x0 ) {

				if( uBase == NOTHING ) {

					Print("%8.8X : %4.4X : ", p + n, n);
					}
				else {
					if( HIWORD(uBase) != 0x0000 && HIWORD(uBase) != 0xFFFF ) {

						Print("%8.8X : ", uBase + n);
						}
					else
						Print("%8.8X : %4.4X : ", p + n, LOWORD(uBase + n));
					}

				s = n;
				}

			if( true ) {

				Print("%2.2X ", p[n]);
				}

			if( n % 0x10 == 0xF || n == uCount - 1 ) {

				Print(" ");

				for( UINT j = n; j % 0x10 < 0xF; j++ ) {

					Print("   ");
					}

				for( UINT i = 0; i <= n - s; i++ ) {

					BYTE b = p[s+i];

					if( b >= 32 && b < 127 ) {

						Print("%c", b);
						}
					else
						Print(".");
					}

				Print("\n");
				}
			}
		}
	}

void CDiagOutput::AddTable(UINT uCols)
{
	m_uCols = uCols;

	m_uRows = 0;

	m_uSize = 80;

	m_pCol  = (CCol *) calloc(m_uCols, sizeof(CCol));

	m_pRow  = (CRow *) calloc(m_uSize, sizeof(CRow));
	}

void CDiagOutput::EndTable(void)
{
	if( m_fTable ) {

		Write("\n");
		}
	else
		m_fTable = true;

	for( UINT r = 0; r < m_uRows; r++ ) {

		CRow &Row = m_pRow[r];

		if( Row.m_uType == 1 ) {

			for( UINT c = 0; c < m_uCols; c++ ) {

				CCol &Col = m_pCol[c];

				Pad(Col.m_pHead, Col.m_uWide, Col.m_fRight);

				Write("  ");
				}

			Write("\n");
			}

		if( Row.m_uType == 2 ) {

			for( UINT c = 0; c < m_uCols; c++ ) {

				CCol &Col = m_pCol[c];

				for( UINT p = 0; p < Col.m_uWide; p++ ) {

					char s[2] = { Row.m_cRule, 0 };

					Write(s);
					}

				Write("  ");
				}

			Write("\n");
			}

		if( Row.m_uType == 3 ) {

			for( UINT c = 0; c < m_uCols; c++ ) {

				CCol &Col = m_pCol[c];

				PTXT pOut = Row.m_pData[c] ? Row.m_pData[c] : PTXT("");

				Pad(pOut, Col.m_uWide, Col.m_fRight);

				Write("  ");

				free(Row.m_pData[c]);
				}

			free(Row.m_pData);

			Write("\n");
			}
		}

	for( UINT c = 0; c < m_uCols; c++ ) {

		CCol &Col = m_pCol[c];

		free(Col.m_pHead);
		
		free(Col.m_pForm);
		}

	free(m_pCol);

	free(m_pRow);
	}

void CDiagOutput::SetColumn(UINT uCol, PCTXT pName, PCTXT pForm)
{
	CCol & Col   = m_pCol[uCol];

	Col.m_pHead  = strdup(pName);

	Col.m_pForm  = strdup(pForm);

	Col.m_fRight = IsRight(pForm);

	MakeMax(Col.m_uWide, strlen(pName));
	}

void CDiagOutput::AddHead(void)
{
	CheckRows();

	CRow & Row  = m_pRow[m_uRows];

	Row.m_uType = 1;

	m_uRows++;
	}

void CDiagOutput::AddRule(char cData)
{
	CheckRows();

	CRow & Row  = m_pRow[m_uRows];

	Row.m_uType = 2;

	Row.m_cRule = cData;

	m_uRows++;
	}

void CDiagOutput::AddRow(void)
{
	CheckRows();

	CRow & Row  = m_pRow[m_uRows];

	Row.m_uType = 3;

	Row.m_pData = (PTXT *) calloc(m_uCols, sizeof(PTXT *));
	}

void CDiagOutput::EndRow(void)
{
	m_uRows++;
	}

void CDiagOutput::SetData(UINT uCol, ...)
{
	CCol &Col = m_pCol[uCol];

	CRow &Row = m_pRow[m_uRows];

	char sText[256];

	va_list pArgs;

	va_start(pArgs, uCol);

	vsprintf(sText, Col.m_pForm, pArgs);

	va_end(pArgs);

	Row.m_pData[uCol] = strdup(sText);

	MakeMax(Col.m_uWide, strlen(sText));
	}

void CDiagOutput::AddPropList(void)
{
	AddTable(2);

	SetColumn(0, "Name",  "%s");
	SetColumn(1, "Value", "%s");

	AddHead();

	AddRule('-');
	}

void CDiagOutput::EndPropList(void)
{
	AddRule('-');

	EndTable();
	}

void CDiagOutput::AddProp(PCTXT pName, PCTXT pForm, ...)
{
	char sText[256];

	va_list pArgs;

	va_start(pArgs, pForm);

	vsprintf(sText, pForm, pArgs);

	va_end(pArgs);

	AddRow();

	SetData(0, pName);

	SetData(1, sText);

	EndRow();
	}

void CDiagOutput::Finished(void)
{
	if( !m_fPrint ) {

		Write(m_pName);

		Write(": (no output)\n");
		}
	}

// Implementation

void CDiagOutput::Write(PCTXT pText)
{
	extern void AfxDebug(PCTXT);

	m_fPrint = true;

	if( m_pConsole ) {

		m_pConsole->Write(pText);

		return;
		}

	AfxDebug(pText);
	}

void CDiagOutput::Pad(PCTXT pText, UINT uWide, bool fRight)
{
	if( !fRight ) {

		Write(pText);
		}

	for( UINT p = uWide - strlen(pText); p; p-- ) {

		Write(" ");
		}

	if( fRight ) {

		Write(pText);
		}
	}

bool CDiagOutput::IsRight(PCTXT pForm)
{
	while( *pForm ) {

		if( *pForm++ == '%' ) {

			if( *pForm == '%' ) {

				pForm++;

				continue;
				}

			if( *pForm == '-' ) {

				return false;
				}

			if( *pForm == '+' ) {

				return true;
				}

			while( *pForm ) {

				if( isalpha(*pForm) ) {

					if( tolower(*pForm) == 's' || tolower(*pForm) == 'x' ) {

						return false;
						}

					return true;
					}

				pForm++;
				}
			}
		}

	return false;
	}

void CDiagOutput::CheckRows(void)
{
	if( m_uRows == m_uSize ) {

		UINT n = 2 * m_uSize;

		m_pRow = (CRow *) realloc(m_pRow, sizeof(CRow) * n);

		memset(m_pRow + m_uSize, 0, sizeof(CRow) * (n - m_uSize));

		m_uSize = n;
		}
	}

// End of File
