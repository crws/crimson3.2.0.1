
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Buffer Messages
//

char const CBuffer::m_ErrHead[] = { "No Space at Head" };

char const CBuffer::m_ErrTail[] = { "No Space at Tail" };

//////////////////////////////////////////////////////////////////////////
//
// Stubs for Buffer Support
//

// Static Data

static IBufferManager * m_pBuffMan = NULL;

// Common Code

global void Bind_BuffMan(void)
{
	AfxGetObject("buffman", 0, IBufferManager, m_pBuffMan);
	}

global void Free_BuffMan(void)
{
	m_pBuffMan->Release();

	m_pBuffMan = NULL;
	}

global CBuffer * BuffAllocate(UINT uSize)
{
	return m_pBuffMan->Allocate(uSize);
	}

// End of File
