
#include "Intern.hpp"

#include "ComProxy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/ZoCgE

//////////////////////////////////////////////////////////////////////////
//
// COM Proxy Forwarding Code
//

#if defined(__INTELLISENSE__)

#define StdForward(n) ((void) n)

#else

#if defined(AEON_COMP_MSVC)

#define	StdForward(n)	__asm mov eax, [esp+4]		\
			__asm mov eax, [eax+8]		\
			__asm mov ecx, [eax+0]		\
			__asm mov ecx, [ecx+4*n]	\
			__asm mov [esp+4], eax		\
			__asm jmp ecx			\

#endif

#if defined(AEON_COMP_GCC) && defined(AEON_PROC_X86)

#define	StdForward(n)	ASM(				\
			"pop %%ebp\n\t"			\
			"mov 4(%%esp), %%eax\n\t"	\
			"mov 8(%%eax), %%eax\n\t"	\
			"mov 0(%%eax), %%ecx\n\t"	\
			"add %0,       %%ecx\n\t"	\
			"mov 0(%%ecx), %%ecx\n\t"	\
			"mov %%eax,    4(%%esp)\n\t"	\
			"jmp *%%ecx\n\t"		\
			:				\
			: "e"(n*4)			\
			);				\
			return 0			\

#endif

#if defined(AEON_COMP_GCC) && defined(AEON_PROC_ARM)

#define	StdForward(n)	ASM(				\
			"sub  sp, #8\n\t"		\
			"str  r4, [sp, #0]\n\t"		\
			"ldr  r0, [r0, #8]\n\t"		\
			"ldr  r4, [r0]\n\t"		\
			"add  r4, %0\n\t"		\
			"ldr  r4, [r4]\n\t"		\
			"str  r4, [sp, #4]\n\t"		\
			"pop  {r4}\n\t"			\
			"pop  {pc}\n\t"			\
			:				\
			: "I"(n*4)			\
			)				\

#endif

#endif

//////////////////////////////////////////////////////////////////////////
//
// COM Proxy Object
//

// Constructor

CComProxy::CComProxy(PCSTR pName, IUnknown *pObj, IUnknown *pUnk, REFIID iid)
{
	StdSetRef();

	m_pObj = pObj;
	       
	m_pUnk = pUnk;
	       
	m_iid  = iid;

	memset(m_sName, '*', sizeof(m_sName));

	strcpy(m_sName, pName);
	}

// IUnknown

HRM CComProxy::QueryInterface(REFIID riid, void **ppObject)
{
	if( AfxIsUnknown(riid) || m_iid == riid ) {

		AddRef();

		*ppObject = this;

		return S_OK;
		}
	else {
		IUnknown *pObj = NULL;

		HRESULT   hr   = m_pUnk->QueryInterface(riid, (void **) &pObj);

		if( hr == S_OK ) {

			*ppObject = New CComProxy("QI", pObj, m_pUnk, riid);

			pObj->Release();

			return S_OK;
			}

		*ppObject = NULL;

		return hr;
		}
	}

ULM CComProxy::AddRef(void)
{
	StdAddRef();
	}

ULM CComProxy::Release(void)
{
	StdRelease();
	}

// Proxy Methods

NAKED HRM CComProxy::Method03(void)
{
	StdForward(3);
	}

NAKED HRM CComProxy::Method04(void)
{
	StdForward(4);
	}

NAKED HRM CComProxy::Method05(void)
{
	StdForward(5);
	}

NAKED HRM CComProxy::Method06(void)
{
	StdForward(6);
	}

NAKED HRM CComProxy::Method07(void)
{
	StdForward(7);
	}

NAKED HRM CComProxy::Method08(void)
{
	StdForward(8);
	}

NAKED HRM CComProxy::Method09(void)
{
	StdForward(9);
	}

NAKED HRM CComProxy::Method10(void)
{
	StdForward(10);
	}

NAKED HRM CComProxy::Method11(void)
{
	StdForward(11);
	}

NAKED HRM CComProxy::Method12(void)
{
	StdForward(12);
	}

NAKED HRM CComProxy::Method13(void)
{
	StdForward(13);
	}

NAKED HRM CComProxy::Method14(void)
{
	StdForward(14);
	}

NAKED HRM CComProxy::Method15(void)
{
	StdForward(15);
	}

NAKED HRM CComProxy::Method16(void)
{
	StdForward(16);
	}

NAKED HRM CComProxy::Method17(void)
{
	StdForward(17);
	}

NAKED HRM CComProxy::Method18(void)
{
	StdForward(18);
	}

NAKED HRM CComProxy::Method19(void)
{
	StdForward(19);
	}

NAKED HRM CComProxy::Method20(void)
{
	StdForward(20);
	}

NAKED HRM CComProxy::Method21(void)
{
	StdForward(21);
	}

NAKED HRM CComProxy::Method22(void)
{
	StdForward(22);
	}

NAKED HRM CComProxy::Method23(void)
{
	StdForward(23);
	}

NAKED HRM CComProxy::Method24(void)
{
	StdForward(24);
	}

NAKED HRM CComProxy::Method25(void)
{
	StdForward(25);
	}

NAKED HRM CComProxy::Method26(void)
{
	StdForward(26);
	}

NAKED HRM CComProxy::Method27(void)
{
	StdForward(27);
	}

NAKED HRM CComProxy::Method28(void)
{
	StdForward(28);
	}

NAKED HRM CComProxy::Method29(void)
{
	StdForward(29);
	}

NAKED HRM CComProxy::Method30(void)
{
	StdForward(30);
	}

NAKED HRM CComProxy::Method31(void)
{
	StdForward(31);
	}

NAKED HRM CComProxy::Method32(void)
{
	StdForward(32);
	}

NAKED HRM CComProxy::Method33(void)
{
	StdForward(33);
	}

NAKED HRM CComProxy::Method34(void)
{
	StdForward(34);
	}

NAKED HRM CComProxy::Method35(void)
{
	StdForward(35);
	}

NAKED HRM CComProxy::Method36(void)
{
	StdForward(36);
	}

NAKED HRM CComProxy::Method37(void)
{
	StdForward(37);
	}

NAKED HRM CComProxy::Method38(void)
{
	StdForward(38);
	}

NAKED HRM CComProxy::Method39(void)
{
	StdForward(39);
	}

NAKED HRM CComProxy::Method40(void)
{
	StdForward(40);
	}

NAKED HRM CComProxy::Method41(void)
{
	StdForward(41);
	}

NAKED HRM CComProxy::Method42(void)
{
	StdForward(42);
	}

NAKED HRM CComProxy::Method43(void)
{
	StdForward(43);
	}

NAKED HRM CComProxy::Method44(void)
{
	StdForward(44);
	}

NAKED HRM CComProxy::Method45(void)
{
	StdForward(45);
	}

NAKED HRM CComProxy::Method46(void)
{
	StdForward(46);
	}

NAKED HRM CComProxy::Method47(void)
{
	StdForward(47);
	}

NAKED HRM CComProxy::Method48(void)
{
	StdForward(48);
	}

NAKED HRM CComProxy::Method49(void)
{
	StdForward(49);
	}

NAKED HRM CComProxy::Method50(void)
{
	StdForward(50);
	}

NAKED HRM CComProxy::Method51(void)
{
	StdForward(51);
	}

NAKED HRM CComProxy::Method52(void)
{
	StdForward(52);
	}

NAKED HRM CComProxy::Method53(void)
{
	StdForward(53);
	}

NAKED HRM CComProxy::Method54(void)
{
	StdForward(54);
	}

NAKED HRM CComProxy::Method55(void)
{
	StdForward(55);
	}

NAKED HRM CComProxy::Method56(void)
{
	StdForward(56);
	}

NAKED HRM CComProxy::Method57(void)
{
	StdForward(57);
	}

NAKED HRM CComProxy::Method58(void)
{
	StdForward(58);
	}

NAKED HRM CComProxy::Method59(void)
{
	StdForward(59);
	}

NAKED HRM CComProxy::Method60(void)
{
	StdForward(60);
	}

NAKED HRM CComProxy::Method61(void)
{
	StdForward(61);
	}

NAKED HRM CComProxy::Method62(void)
{
	StdForward(62);
	}

NAKED HRM CComProxy::Method63(void)
{
	StdForward(63);
	}

// End of File
