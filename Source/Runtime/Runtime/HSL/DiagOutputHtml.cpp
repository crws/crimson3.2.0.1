
#include "Intern.hpp"

#include "DiagOutputHtml.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTML Diagnostics Output
//

// Constructor

CDiagOutputHtml::CDiagOutputHtml(IDiagConsole *pConsole, PCTXT pName)
{
	// TODO -- Track states to ensure correct
	// ordering of calls to the table APIs.

	StdSetRef();

	m_pConsole = pConsole;

	m_pName    = pName;

	m_fPrint   = false;

	m_State    = stateNormal;

	Write("<pre>");
	}

// Destructor

CDiagOutputHtml::~CDiagOutputHtml(void)
{
	Write("</pre>");
	}

// IUnknown

#if defined(AEON_ENVIRONMENT)

HRESULT CDiagOutputHtml::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagOutput);

	StdQueryInterface(IDiagOutput);

	return E_NOINTERFACE;
	}

ULONG CDiagOutputHtml::AddRef(void)
{
	StdAddRef();
	}

ULONG CDiagOutputHtml::Release(void)
{
	StdRelease();
	}

#endif

// IDiagOutput

void CDiagOutputHtml::Error(PCTXT pText, ...)
{
	Write("<p>");

	Write(m_pName);

	Write(": ");

	if( pText ) {

		va_list pArgs;

		va_start(pArgs, pText);

		IntVPrint(pText, pArgs);

		Write("<p>");

		va_end(pArgs);

		return;
		}

	Write("syntax error");

	Write("<p>");
	}

void CDiagOutputHtml::Print(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	Write("<p>");

	IntVPrint(pText, pArgs);

	Write("</p>");

	va_end(pArgs);
	}

void CDiagOutputHtml::VPrint(PCTXT pText, va_list pArgs)
{
	Write("<p>");

	IntVPrint(pText, pArgs);

	Write("</p>");
	}

void CDiagOutputHtml::Dump(PCVOID pData, UINT uCount)
{
	Dump(pData, uCount, NOTHING);
	}

void CDiagOutputHtml::Dump(PCVOID pData, UINT uCount, UINT uBase)
{
	if( pData ) {

		PCBYTE p = PCBYTE(pData);

		UINT   s = 0;

		for( UINT n = 0; n < uCount; n++ ) {

			if( n % 0x10 == 0x0 ) {

				if( uBase == NOTHING ) {

					IntPrint("<p>%8.8X : %4.4X : ", p + n, n);
					}
				else
					IntPrint("<p>%8.8X : ", uBase + n);

				s = n;
				}

			if( true ) {

				IntPrint("%2.2X ", p[n]);
				}

			if( n % 0x10 == 0xF || n == uCount - 1 ) {

				Print(" ");

				for( UINT j = n; j % 0x10 < 0xF; j++ ) {

					Print("   ");
					}

				for( UINT i = 0; i <= n - s; i++ ) {

					BYTE b = p[s+i];

					if( b >= 32 && b < 127 ) {

						IntPrint("%c", b);
						}
					else
						IntPrint(".");
					}

				IntPrint("</p>");
				}
			}
		}
	}

void CDiagOutputHtml::AddTable(UINT uCols)
{
	m_uCols = uCols;

	m_uRows = 0;

	m_pCol  = (CCol *) calloc(m_uCols, sizeof(CCol));

	m_pRow  = (CRow *) calloc(80,      sizeof(CRow));
	}

void CDiagOutputHtml::EndTable(void)
{
	Write("<table class='table' style='width: auto'>");

	bool fData = false;

	for( UINT r = 0; r < m_uRows; r++ ) {

		CRow &Row = m_pRow[r];

		if( Row.m_uType == 1 ) {

			Write("<thead>\n<tr>");

			for( UINT c = 0; c < m_uCols; c++ ) {

				CCol &Col = m_pCol[c];

				if( Col.m_fRight ) {

					IntPrint("<th style='text-align: right'>%s</th>", Col.m_pHead);
					}
				else
					IntPrint("<th>%s</th>", Col.m_pHead);
				}

			Write("</tr>\n</thead>\n");
			}

		if( Row.m_uType == 3 ) {

			if( !fData ) {

				Write("<tbody>\n");

				fData = true;
				}

			Write("<tr>");

			for( UINT c = 0; c < m_uCols; c++ ) {

				CCol &Col = m_pCol[c];

				PTXT pOut = Row.m_pData[c] ? Row.m_pData[c] : PTXT("");

				if( Col.m_fRight ) {

					IntPrint("<td style='text-align: right'>%s</td>", pOut);
					}
				else
					IntPrint("<td>%s</td>", pOut);

				free(Row.m_pData[c]);
				}

			Write("</tr>\n");

			free(Row.m_pData);
			}
		}

	if( fData ) {

		Write("</tbody>\n");
		}

	Write("</table>\n");

	for( UINT c = 0; c < m_uCols; c++ ) {

		CCol &Col = m_pCol[c];

		free(Col.m_pHead);
		
		free(Col.m_pForm);
		}

	free(m_pCol);

	free(m_pRow);
	}

void CDiagOutputHtml::SetColumn(UINT uCol, PCTXT pName, PCTXT pForm)
{
	CCol & Col   = m_pCol[uCol];

	Col.m_pHead  = strdup(pName);

	Col.m_pForm  = strdup(pForm);

	Col.m_fRight = IsRight(pForm);

	MakeMax(Col.m_uWide, strlen(pName));
	}

void CDiagOutputHtml::AddHead(void)
{
	CRow & Row  = m_pRow[m_uRows];

	Row.m_uType = 1;

	m_uRows++;
	}

void CDiagOutputHtml::AddRule(char cData)
{
	CRow & Row  = m_pRow[m_uRows];

	Row.m_uType = 2;

	Row.m_cRule = cData;

	m_uRows++;
	}

void CDiagOutputHtml::AddRow(void)
{
	CRow & Row  = m_pRow[m_uRows];

	Row.m_uType = 3;

	Row.m_pData = (PTXT *) calloc(m_uCols, sizeof(PTXT *));
	}

void CDiagOutputHtml::EndRow(void)
{
	m_uRows++;
	}

void CDiagOutputHtml::SetData(UINT uCol, ...)
{
	CCol &Col = m_pCol[uCol];

	CRow &Row = m_pRow[m_uRows];

	char sText[256];

	va_list pArgs;

	va_start(pArgs, uCol);

	vsprintf(sText, Col.m_pForm, pArgs);

	va_end(pArgs);

	Row.m_pData[uCol] = strdup(sText);

	MakeMax(Col.m_uWide, strlen(sText));
	}

void CDiagOutputHtml::AddPropList(void)
{
	AddTable(2);

	SetColumn(0, "Name",  "%s");
	SetColumn(1, "Value", "%s");

	AddHead();

	AddRule('-');
	}

void CDiagOutputHtml::EndPropList(void)
{
	AddRule('-');

	EndTable();
	}

void CDiagOutputHtml::AddProp(PCTXT pName, PCTXT pForm, ...)
{
	char sText[256];

	va_list pArgs;

	va_start(pArgs, pForm);

	vsprintf(sText, pForm, pArgs);

	va_end(pArgs);

	AddRow();

	SetData(0, pName);

	SetData(1, sText);

	EndRow();
	}

void CDiagOutputHtml::Finished(void)
{
	if( !m_fPrint ) {

		Write("<p>");

		Write(m_pName);

		Write(": (no output)</p>");
		}
	}

// Implementation

void CDiagOutputHtml::IntPrint(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	IntVPrint(pText, pArgs);

	va_end(pArgs);
	}

void CDiagOutputHtml::IntVPrint(PCTXT pText, va_list pArgs)
{
	// TODO -- Dynanic buffer!!!

	char sText[256];

	vsprintf(sText, pText, pArgs);

	Write(sText);
	}

void CDiagOutputHtml::Write(PCTXT pText)
{
	m_fPrint = true;

	if( m_pConsole ) {

		m_pConsole->Write(pText);

		return;
		}

	AfxPrint(pText);
	}

void CDiagOutputHtml::Pad(PCTXT pText, UINT uWide, bool fRight)
{
	if( !fRight ) {

		Write(pText);
		}

	for( UINT p = uWide - strlen(pText); p; p-- ) {

		Write(" ");
		}

	if( fRight ) {

		Write(pText);
		}
	}

bool CDiagOutputHtml::IsRight(PCTXT pForm)
{
	while( *pForm ) {

		if( *pForm++ == '%' ) {

			if( *pForm == '%' ) {

				pForm++;

				continue;
				}

			if( *pForm == '-' ) {

				return false;
				}

			if( *pForm == '+' ) {

				return true;
				}

			while( *pForm ) {

				if( isalpha(*pForm) ) {

					if( tolower(*pForm) == 's' || tolower(*pForm) == 'x' ) {

						return false;
						}

					return true;
					}

				pForm++;
				}
			}
		}

	return false;
	}

// End of File
