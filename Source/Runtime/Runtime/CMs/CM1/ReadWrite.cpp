
#include "Intern.hpp"

#include "ReadWrite.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Read-Write Lock
//

// Constructors

CReadWriteLock::CReadWriteLock(void)
{
	m_pSerial  = Create_Qutex();

	m_pLocked  = Create_Semaphore(1);

	m_nPromote = 0;
	}

// Destructor

CReadWriteLock::~CReadWriteLock(void)
{
	}

// Operations

BOOL CReadWriteLock::LockRead(UINT uWait)
{
	m_pSerial->Wait(FOREVER);

	HTHREAD hThread = GetCurrentThread();

	INDEX iFind = m_Readers.FindName(hThread);

	if( iFind == m_Readers.Failed() ) {

		m_Readers.Insert(hThread, 1);

		return TRUE;
		}
	else {
		UINT uCount = m_Readers.GetData(iFind) + 1;

		m_Readers.SetData(iFind, uCount);
		}














	if( !m_Readers.IsEmpty() ) {

		AddReader(hThread);
		}
	else {
		m_pSerial->Free();

		m_pLocked->Wait(FOREVER);

		m_pSerial->Wait(FOREVER);

		m_Readers.Insert(hThread, 1);
		}

	m_pSerial->Free();

	return TRUE;
	}

BOOL CReadWriteLock::FreeRead(void)
{
	m_pSerial->Wait(FOREVER);

	HTHREAD hThread = GetCurrentThread();

	if( RemoveReader(hThread) ) {

		if( m_Readers.IsEmpty() ) {

			m_pLocked->Signal(1);
			}
		}

	m_pSerial->Free();

	return TRUE;
	}

BOOL CReadWriteLock::LockWrite(UINT uWait)
{
	m_pSerial->Wait(FOREVER);

	HTHREAD hThread = GetCurrentThread();

	if( m_Readers.GetCount() == 1 ) {

		if( m_Readers.GetName(m_Readers.GetHead()) == GetCurrentThread() ) {

			m_nPromote++;

			m_pSerial->Free();

			return TRUE;
			}
		}

	m_pSerial->Free();

	m_pLocked->Wait(FOREVER);

	m_pSerial->Wait(FOREVER);

	AddReader(hThread);

	m_pSerial->Free();

	return TRUE;
	}

BOOL CReadWriteLock::FreeWrite(void)
{
	m_pSerial->Wait(FOREVER);

	HTHREAD hThread = GetCurrentThread();

	if( m_nPromote ) {
		
		m_nPromote--;

		m_pSerial->Free();

		return TRUE;
		}

	m_pLocked->Signal(1);

	RemoveReader(hThread);

	m_pSerial->Free();

	return TRUE;
	}


// Implementation

BOOL CReadWriteLock::AddReader(HTHREAD hThread)
{
	INDEX iFind = m_Readers.FindName(hThread);

	if( iFind == m_Readers.Failed() ) {

		m_Readers.Insert(hThread, 1);

		return TRUE;
		}

	UINT uCount = m_Readers.GetData(iFind) + 1;

	m_Readers.SetData(iFind, uCount);

	return FALSE;
	}

BOOL CReadWriteLock::RemoveReader(HTHREAD hThread)
{
	INDEX   iFind   = m_Readers.FindName(hThread);

	UINT    uCount  = m_Readers.GetData(iFind) - 1;

	if( uCount ) {

		m_Readers.SetData(iFind, uCount);

		return FALSE;
		}

	m_Readers.Remove(iFind);

	return TRUE;
	}

// End of File
