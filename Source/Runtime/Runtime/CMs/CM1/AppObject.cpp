
#include "Intern.hpp"

#include "AppObject.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ThreadObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Instantiator

clink IUnknown * Create_AppObject(PCSTR pName)
{
	return (IClientProcess *) New CAppObject;
	}

// Constructor

CAppObject::CAppObject(void)
{
	StdSetRef();

	for( UINT n = 0; n < elements(m_pThread); n++ ) {

		m_pThread[n] = New CThreadObject(n);
		}

	m_pTms = Create_Semaphore(0);
	}

// Destructor

CAppObject::~CAppObject(void)
{
	for( UINT n = 0; n < elements(m_pThread); n++ ) {

		m_pThread[n]->Release();
		}

	m_pTms->Release();
	}

// IUnknown

HRESULT CAppObject::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IThreadNotify);

	return E_NOINTERFACE;
	}

ULONG CAppObject::AddRef(void)
{
	StdAddRef();
	}

ULONG CAppObject::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CAppObject::TaskInit(UINT uTask)
{
	SetThreadName("App");

	m_uNotify = AddExecThreadNotify(this);

	CreateThreads();

	InitThreads();

	StartThreads();

	return TRUE;
	}

INT CAppObject::TaskExec(UINT uTask)
{
	for(;;) Sleep(40000);

	return 0;
	}

void CAppObject::TaskStop(UINT uTask)
{
	StopThreads();
	}

void CAppObject::TaskTerm(UINT uTask)
{
	TermThreads();

	WaitThreads();

	CloseThreads();

	RemoveExecThreadNotify(m_uNotify);
	}

// IThreadNotify

UINT CAppObject::OnThreadCreate(IThread *pThread, UINT uIndex)
{
	AfxTrace("OTC(%8.8X,%u)\n", pThread, uIndex);

	return 0;
	}

void CAppObject::OnThreadDelete(IThread *pThread, UINT uIndex, UINT uData)
{
	AfxTrace("OTD(%8.8X,%u)\n", pThread, uIndex);
	}

// Implementation

void CAppObject::CreateThreads(void)
{
	AfxTrace("=== CreateThreads\n");

	for( UINT n = 0; n < elements(m_hThread); n++ ) {

		m_hThread[n] = CreateClientThread( m_pThread[n],
						   n,
						   9999+elements(m_hThread)-n,
						   m_pTms
						   );
		}

	WaitForTransition();
	}

void CAppObject::InitThreads(void)
{
	AfxTrace("=== InitThreads\n");

	for( UINT n = 0; n < elements(m_hThread); n++ ) {

		AdvanceThread(m_hThread[n]);
		}

	WaitForTransition();
	}

void CAppObject::StartThreads(void)
{
	AfxTrace("=== StartThreads\n");

	for( UINT n = 0; n < elements(m_hThread); n++ ) {

		AdvanceThread(m_hThread[n]);
		}

	WaitForTransition();
	}

void CAppObject::StopThreads(void)
{
	AfxTrace("=== StopThreads\n");

	for( UINT n = 0; n < elements(m_hThread); n++ ) {

		DestroyThread(m_hThread[n]);
		}

	WaitForTransition();
	}

void CAppObject::TermThreads(void)
{
	AfxTrace("=== TermThreads\n");

	for( UINT n = 0; n < elements(m_hThread); n++ ) {

		AdvanceThread(m_hThread[n]);
		}

	WaitForTransition();
	}

void CAppObject::WaitThreads(void)
{
	AfxTrace("=== WaitThreads\n");

	for( UINT n = 0; n < elements(m_hThread); n++ ) {

		WaitThread(m_hThread[n], FOREVER);
		}

	AfxTrace("=== Transition Complete\n");
	}

void CAppObject::CloseThreads(void)
{
	AfxTrace("=== CloseThreads\n");

	for( UINT n = 0; n < elements(m_hThread); n++ ) {

		CloseThread(m_hThread[n]);
		}

	AfxTrace("=== Transition Complete\n");
	}

BOOL CAppObject::WaitForTransition(void)
{
	for( UINT n = 0; n < elements(m_hThread); n++ ) {

		if( m_pTms->Wait(5000) ) {

			continue;
			}

		AfxTrace("=== Transition Timeout!\n");

		return FALSE;
		}

	AfxTrace("=== Transition Complete\n");

	return TRUE;
	}

// End of File
