
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ReadWrite_HPP

#define INCLUDE_ReadWrite_HPP

//////////////////////////////////////////////////////////////////////////
//
// Read-Write Lock
//

class CReadWriteLock
{
	public:
		// Constructors
		CReadWriteLock(void);

		// Destructor
		~CReadWriteLock(void);

		// Operations
		BOOL LockRead(UINT uWait);
		BOOL FreeRead(void);
		BOOL LockWrite(UINT uWait);
		BOOL FreeWrite(void);

	protected:
		// Data Members
		IMutex           * m_pSerial;
		ISemaphore       * m_pLocked;
		CMap<HTHREAD,UINT> m_Readers;
		INT		   m_nPromote;

		// Implementation
		BOOL AddReader(HTHREAD hThread);
		BOOL RemoveReader(HTHREAD hThread);
	};

// End of File

#endif
