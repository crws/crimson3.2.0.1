
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ThreadObject_HPP

#define INCLUDE_ThreadObject_HPP

//////////////////////////////////////////////////////////////////////////
//
// Thread Object
//

class CThreadObject : public IClientProcess
{
	public:
		// Constructor
		CThreadObject(UINT n);

		// Destructor
		~CThreadObject(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

	protected:
		// Data Members
		ULONG m_uRefs;
		UINT  m_n;

		// Implementation
		void FloatStuff(UINT uTask, DWORD &d);
	};

// End of File

#endif
