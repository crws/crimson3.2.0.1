
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

clink void AeonCmMain(void)
{
	extern IUnknown * Create_AppObject(PCSTR pName);

	piob->RegisterInstantiator("app", Create_AppObject);
	}

// End of File
