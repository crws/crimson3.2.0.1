
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AppObjectClient_HPP

#define INCLUDE_AppObjectClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

class CAppObjectClient : public IClientProcess
{
	public:
		// Constructor
		CAppObjectClient(void);

		// Destructor
		~CAppObjectClient(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

	protected:
		// Data Members
		ULONG          m_uRefs;
		IOpcUaClient * m_pClient;
	};

// End of File

#endif
