
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AppObjectServer_HPP

#define INCLUDE_AppObjectServer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

class CAppObjectServer : public IClientProcess, public IOpcUaServerHost
{
	public:
		// Constructor
		CAppObjectServer(void);

		// Destructor
		~CAppObjectServer(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// IOpcUaServerHost
		bool    LoadModel(IOpcUaDataModel *pModel);
		bool    CheckUser(CString const &User, CString const &Pass);
		bool    SkipValue(UINT ns, UINT id);
		void    SetValueDouble(UINT ns, UINT id, UINT n, double d);
		void    SetValueInt(UINT ns, UINT id, UINT n, int d);
		void    SetValueInt64(UINT ns, UINT id, UINT n, INT64 d);
		void    SetValueString(UINT ns, UINT id, UINT n, PCTXT d);
		double  GetValueDouble(UINT ns, UINT id, UINT n);
		UINT    GetValueInt(UINT ns, UINT id, UINT n);
		UINT64  GetValueInt64(UINT ns, UINT id, UINT n);
		CString GetValueString(UINT ns, UINT id, UINT n);
		timeval GetValueTime(UINT ns, UINT id, UINT n);
		bool    GetDesc(CString &Desc, UINT ns, UINT id);
		bool    GetSourceTimeStamp(timeval &t, UINT ns, UINT id);
		bool    HasEvents(CArray <UINT> &List, UINT ns, UINT id);
		bool    HasCustomHistory(UINT ns, UINT id);
		bool    InitHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory);
		bool	HasChanged(UINT ns, UINT id, UINT uType, DWORD *pHistory);
		void	KillHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory);
		UINT	GetWireType(UINT uType);

	protected:
		// Data Members
		ULONG          m_uRefs;
		IOpcUaServer * m_pServer;

		// Emulation Support
		void InitEmulation(void);
		void KillEmulation(void);
		void InitEmulatedIp(void);
		void KillEmulatedIp(void);
		void InitEmulatedFs(void);
	};

// End of File

#endif
