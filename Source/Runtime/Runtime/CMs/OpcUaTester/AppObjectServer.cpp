
#include "Intern.hpp"

#include "AppObjectServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "OpcIds.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Instantiator

clink IUnknown * Create_AppObjectServer(PCSTR pName)
{
	return (IClientProcess *) New CAppObjectServer;
	}

// Constructor

CAppObjectServer::CAppObjectServer(void)
{
	StdSetRef();
	}

// Destructor

CAppObjectServer::~CAppObjectServer(void)
{
	}

// IUnknown

HRESULT CAppObjectServer::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IOpcUaServerHost);

	return E_NOINTERFACE;
	}

ULONG CAppObjectServer::AddRef(void)
{
	StdAddRef();
	}

ULONG CAppObjectServer::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CAppObjectServer::TaskInit(UINT uTask)
{
	InitEmulation();

	SetThreadName("OpcUaServer");

	AfxNewObject("opcua-server", IOpcUaServer, m_pServer);

	static BYTE bGuid[16];

	memset(bGuid, 0x55, 16);

	COpcUaServerConfig Config;

	Config.m_uSize   = sizeof(Config);
	Config.m_uDebug  = 2;
	Config.m_uSample = 1;
	Config.m_uQuota  = 50;
	Config.m_pGuid   = bGuid;

	m_pServer->OpcLoad(Config);

	CString Endpoint = "opc.tcp://127.0.0.1:4444";

	return m_pServer->OpcInit(this, Endpoint);
	}

INT CAppObjectServer::TaskExec(UINT uTask)
{
	if( m_pServer->OpcExec() ) {

		for(;;) Sleep(FOREVER);
		}

	return 0;
	}

void CAppObjectServer::TaskStop(UINT uTask)
{
	m_pServer->OpcStop();
	}

void CAppObjectServer::TaskTerm(UINT uTask)
{
	m_pServer->OpcTerm();

	m_pServer->Release();

	KillEmulation();
	}

// IOpcUaServerHost

bool CAppObjectServer::LoadModel(IOpcUaDataModel *pModel)
{
	pModel->AddObject   (1, 1, 0, OpcUaId_BaseObjectType, "Aeon");

	pModel->AddObject   (1, 2, 0, OpcUaId_BaseObjectType, "Tags");

	pModel->AddObject   (1, 3, 0, OpcUaId_BaseObjectType, "Tag1");

	pModel->AddObject   (1, 4, 0, OpcUaId_BaseObjectType, "Tag2");

	pModel->AddOrganizes(0, OpcUaId_ObjectsFolder, 1, 1);

	pModel->AddComponent(1, 1,                     1, 2);

	pModel->AddComponent(1, 2,                     1, 3);

	pModel->AddComponent(1, 2,                     1, 4);

	pModel->AddVariable (2, 3, 0, OpcUaId_BaseDataVariableType, "Value", 0, OpcUaId_Int32, 0, 0, OpcUa_AccessLevels_CurrentRead | OpcUa_AccessLevels_HistoryRead, true);

	pModel->AddVariable (2, 4, 0, OpcUaId_BaseDataVariableType, "Value", 0, OpcUaId_Int32, 0, 0, OpcUa_AccessLevels_CurrentRead | OpcUa_AccessLevels_HistoryRead, true);

	pModel->AddProperty (1, 3, 2, 3);

	pModel->AddProperty (1, 4, 2, 4);

	pModel->AddObject   (3, 3, 0, OpcUaId_HistoricalDataConfigurationType, "HA Configuration");

	pModel->AddObject   (3, 4, 0, OpcUaId_HistoricalDataConfigurationType, "HA Configuration");

	pModel->AddReference(2, 3, 0, OpcUaId_HasHistoricalConfiguration, 3, 3);

	pModel->AddReference(2, 4, 0, OpcUaId_HasHistoricalConfiguration, 3, 4);

	pModel->AddVariable (4, 3, 0, OpcUaId_BaseDataVariableType, "Stepped", 0, OpcUaId_Boolean, 0, 0, OpcUa_AccessLevels_CurrentRead, false);

	pModel->AddVariable (4, 4, 0, OpcUaId_BaseDataVariableType, "Stepped", 0, OpcUaId_Boolean, 0, 0, OpcUa_AccessLevels_CurrentRead, false);

	pModel->AddProperty (3, 3, 4, 3);

	pModel->AddProperty (3, 4, 4, 4);

	return true;
	}

bool CAppObjectServer::CheckUser(CString const &User, CString const &Pass)
{
	return true;
	}

bool CAppObjectServer::SkipValue(UINT ns, UINT id)
{
	return false;
	}

void CAppObjectServer::SetValueDouble(UINT ns, UINT id, UINT n, double d)
{
	}

void CAppObjectServer::SetValueInt(UINT ns, UINT id, UINT n, int d)
{
	}

void CAppObjectServer::SetValueInt64(UINT ns, UINT id, UINT n, INT64 d)
{
	}

void CAppObjectServer::SetValueString(UINT ns, UINT id, UINT n, PCTXT d)
{
	}

double CAppObjectServer::GetValueDouble(UINT ns, UINT id, UINT n)
{
	return 0;
	}

UINT CAppObjectServer::GetValueInt(UINT ns, UINT id, UINT n)
{
	if( ns == 2 ) {
		
		timeval t;

		gettimeofday(&t, NULL);

		return (10 * id + t.tv_sec) % 60;
		}

	return 0;
	}

UINT64 CAppObjectServer::GetValueInt64(UINT ns, UINT id, UINT n)
{
	return 0;
	}

CString CAppObjectServer::GetValueString(UINT ns, UINT id, UINT n)
{
	if( ns == 0 ) {

		switch( id ) {

			case OpcUaId_Server_ServerArray:

				return "AeonServer";
					
			case OpcUaId_Server_ServerStatus_BuildInfo_ManufacturerName:
	
				return "Red Lion Controls";

			case OpcUaId_Server_ServerStatus_BuildInfo_ProductName:

				return "Aeon-Based Device";

			case OpcUaId_Server_ServerStatus_BuildInfo_ProductUri:

				return "http://www.redlion.net";

			case OpcUaId_Server_ServerStatus_BuildInfo_ApplicationUri:

				return "http://www.redlion.net/crimson";

			case OpcUaId_Server_ServerStatus_BuildInfo_SoftwareVersion:

				return "Aeon 1.000";
			}
		}

	return "";
	}

timeval CAppObjectServer::GetValueTime(UINT ns, UINT id, UINT n)
{
	timeval t = { 0 };

	return t;
	}

bool CAppObjectServer::GetDesc(CString &Desc, UINT ns, UINT id)
{
	return false;
	}

bool CAppObjectServer::GetSourceTimeStamp(timeval &t, UINT ns, UINT id)
{
	return false;
	}

bool CAppObjectServer::HasEvents(CArray <UINT> &List, UINT ns, UINT id)
{
	return false;
	}

bool CAppObjectServer::HasCustomHistory(UINT ns, UINT id)
{
	return false;
	}

bool CAppObjectServer::InitHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	return true;
	}

bool CAppObjectServer::HasChanged(UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	return false;
	}

void CAppObjectServer::KillHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	}

UINT CAppObjectServer::GetWireType(UINT uType)
{
	return uType;
	}

// Emulator Support

void CAppObjectServer::InitEmulation(void)
{
	#if 1

	InitEmulatedIp();

	InitEmulatedFs();

	#endif
	}

void CAppObjectServer::KillEmulation(void)
{
	#if 1

	KillEmulatedIp();

	#endif
	}

void CAppObjectServer::InitEmulatedIp(void)
{
	IRouter *pRouter;

	AfxGetObject("ip", 0, IRouter, pRouter);

	pRouter->AddLoopback();

	CConfigEth Config;

	memset(&Config, 0, sizeof(Config));

	pRouter->AddEthernet(0, Config);

	pRouter->Open();
	}

void CAppObjectServer::KillEmulatedIp(void)
{
	IRouter *pRouter;

	AfxGetObject("ip", 0, IRouter, pRouter);

	pRouter->Close();
	}

void CAppObjectServer::InitEmulatedFs(void)
{
	IDiskManager *pDiskManager;

	ISdHost      *pSdHost;

	IBlockDevice *pBlockDevice;

	AfxGetObject("diskman", 0, IDiskManager, pDiskManager);

	AfxGetObject("sdhost",  0, ISdHost,      pSdHost);

	AfxNewObject("sddrive",    IBlockDevice, pBlockDevice);

	pBlockDevice->Open(pSdHost);

	pDiskManager->RegisterDisk(pBlockDevice);

	pDiskManager->FormatDisk(0);

	while( !pDiskManager->GetDiskReady(0) ) Sleep(100);

	AfxTrace("Disk Ready\n");
	}

// End of File
