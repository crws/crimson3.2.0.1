
#include "Intern.hpp"

#include "AppObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Instantiator

clink IUnknown * Create_AppObject(PCSTR pName)
{
	return (IClientProcess *) New CAppObject;
	}

// Constructor

CAppObject::CAppObject(void)
{
	StdSetRef();
	}

// Destructor

CAppObject::~CAppObject(void)
{
	}

// IUnknown

HRESULT CAppObject::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CAppObject::AddRef(void)
{
	StdAddRef();
	}

ULONG CAppObject::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CAppObject::TaskInit(UINT uTask)
{
	return TRUE;
	}

INT CAppObject::TaskExec(UINT uTask)
{
	globalParams = New GlobalParams(NULL);

	PDFDoc *pDoc = New PDFDoc(New GString("C:\\Temp\\out.pdf"));

	if( pDoc->isOk() ) {

		SplashColor paperColor;

		paperColor[0] = 255;
		paperColor[1] = 255;
		paperColor[2] = 255;

		SplashOutputDev * pSplash = New SplashOutputDev( splashModeBGR8, 
								 1, 
								 gFalse, 
								 paperColor
								 );

		pSplash->startDoc(pDoc->getXRef());

		pDoc->displayPage( pSplash, 
				   1, 
				   72, 
				   72, 
				   0, 
				   gFalse, 
				   gTrue, 
				   gFalse
				   );

		int   dx = pSplash->getBitmap()->getWidth();

		int   dy = pSplash->getBitmap()->getHeight();

		PBYTE pd = pSplash->getBitmap()->getDataPtr();

		int   nb = pSplash->getBitmap()->getRowSize();

		////////

		IDisplay *pDisp;

		AfxGetObject("display", 0, IDisplay, pDisp);

		int cx, cy;

		pDisp->GetSize(cx, cy);

		PDWORD pb = New DWORD [ cx * cy ];

		memset(pb, 0, 4 * cx * cy);

		int xp = (dx < cx) ? (cx - dx) / 2 : 0;

		for( int y = 0; y < min(cy, dy); y++ ) {

			PDWORD d = pb + y * cx + xp;

			PBYTE  s = pd + y * nb;

			for( int x = 0; x < min(cx, dx); x++ ) {

				BYTE r = *s++;
				BYTE g = *s++;
				BYTE b = *s++;

				*d++ = r | (g<<8) | (b<<16);
				}
			}

		pDisp->Update(pb);

		pDisp->Release();

		delete pSplash;
		}

	delete pDoc;

	return 0;
	}

void CAppObject::TaskStop(UINT uTask)
{
	}

void CAppObject::TaskTerm(UINT uTask)
{
	}

// Implementation

void CAppObject::TestFT2(void)
{
	FT_Library lib  = 0;

	FT_Face    face = 0;

	FT_Init_FreeType(&lib);

	FT_New_Face(lib, "c:\\windows\\fonts\\ARIALUNI.TTF", 0, &face);

	FT_Set_Pixel_Sizes(face, 256, 256);

	FT_Load_Char(face, '?', FT_LOAD_DEFAULT);

	FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);

	ShowGlyph(&face->glyph->bitmap);

	FT_Done_Face(face);

	FT_Done_FreeType(lib);
	}

void CAppObject::ShowGlyph(FT_Bitmap *bitmap)
{
	IDisplay *pDisp;

	AfxGetObject("display", 0, IDisplay, pDisp);

	int cx, cy;

	pDisp->GetSize(cx, cy);

	PDWORD p = New DWORD [ cx * cy ];

	memset(p, 0, 4 * cx * cy);

	PCBYTE s = bitmap->buffer;

	for( UINT y = 0; y < bitmap->rows; y++ ) {

		PDWORD d = p + 16 + cx * (16 + y);

		for( UINT x = 0; x < bitmap->width; x++ ) {

			*d = s[x] | (s[x]<<8) | (s[x]<<16);

			d++;
			}

		s += bitmap->pitch;
		}

	pDisp->Update(p);

	pDisp->Release();
	}

// End of File
