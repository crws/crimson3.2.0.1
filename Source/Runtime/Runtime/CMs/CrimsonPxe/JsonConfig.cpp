
#include "Intern.hpp"

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// JSON Configuration Object
//

// Constructors

CJsonConfig::CJsonConfig(CJsonData *pJson, IConfigStorage *pConfig)
{
	m_pJson   = pJson;

	m_pKeys   = NULL;

	m_fDelete = true;

	ReadPersonality(pConfig);
}

CJsonConfig::CJsonConfig(CJsonData *pJson, CStringMap *pKeys)
{
	m_pJson   = pJson;

	m_pKeys   = pKeys;

	m_fDelete = false;
}

// Destructor

CJsonConfig::~CJsonConfig(void)
{
	if( m_fDelete ) {

		delete m_pJson;

		delete m_pKeys;
	}
}

// Attributes

CString CJsonConfig::GetValue(CString Name) const
{
	return GetValue(Name, "");
}

CString CJsonConfig::GetValue(UINT uIndex) const
{
	return GetValue(uIndex, "");
}

CString CJsonConfig::GetValue(CString Name, CString Default) const
{
	CString Data = m_pJson->GetValue(Name, Default);

	if( Data[0] == '=' ) {

		if( !ReadFromPersonality(Data) ) {

			return Default;
		}
	}

	return Data;
}

CString CJsonConfig::GetValue(UINT uIndex, CString Default) const
{
	CString Data = m_pJson->GetValue(uIndex, Default);

	if( Data[0] == '=' ) {

		if( !ReadFromPersonality(Data) ) {

			return Default;
		}
	}

	return Data;
}

BOOL CJsonConfig::GetValueAsBlob(CString Name, CByteArray &Data) const
{
	CString Text = GetValue(Name, "");

	if( !Text.IsEmpty() ) {

		if( Text.Find('|') < NOTHING ) {

			Text.StripToken('|');

			if( Text.StartsWith("data:") ) {

				Text.StripToken(';');

				if( Text.StartsWith("base64,") ) {

					Text.StripToken(',');

					CBase64::ToBytes(Data, Text);

					return TRUE;
				}
			}
		}
	}

	Data.Empty();

	return FALSE;
}

UINT CJsonConfig::GetValueAsUInt(CString Name, UINT uDef, UINT uMin, UINT uMax) const
{
	CString Data = GetValue(Name, "");

	if( !Data.IsEmpty() ) {

		PSTR pDone = NULL;

		UINT uData = strtoul(Data, &pDone, 10);

		if( pDone && !*pDone ) {

			MakeMin(uData, uMax);

			MakeMax(uData, uMin);

			return uData;
		}
	}

	return uDef;
}

UINT CJsonConfig::GetValueAsUInt(UINT uIndex, UINT uDef, UINT uMin, UINT uMax) const
{
	CString Data = GetValue(uIndex, "");

	if( !Data.IsEmpty() ) {

		PSTR pDone = NULL;

		UINT uData = strtoul(Data, &pDone, 10);

		if( pDone && !*pDone ) {

			MakeMin(uData, uMax);

			MakeMax(uData, uMin);

			return uData;
		}
	}

	return uDef;
}

INT CJsonConfig::GetValueAsInt(CString Name, INT nDef, INT nMin, INT nMax) const
{
	CString Data = GetValue(Name, "");

	if( !Data.IsEmpty() ) {

		PSTR pDone = NULL;

		INT  nData = strtol(Data, &pDone, 10);

		if( pDone && !*pDone ) {

			MakeMin(nData, nMax);

			MakeMax(nData, nMin);

			return nData;
		}
	}

	return nDef;
}

INT CJsonConfig::GetValueAsInt(UINT uIndex, INT nDef, INT nMin, INT nMax) const
{
	CString Data = GetValue(uIndex, "");

	if( !Data.IsEmpty() ) {

		PSTR pDone = NULL;

		INT  nData = strtol(Data, &pDone, 10);

		if( pDone && !*pDone ) {

			MakeMin(nData, nMax);

			MakeMax(nData, nMin);

			return nData;
		}
	}

	return nDef;
}

double CJsonConfig::GetValueAsDouble(CString Name, double nDef) const
{
	CString Data = GetValue(Name, "");

	if( !Data.IsEmpty() ) {

		PSTR   pDone = NULL;

		double nData = strtod(Data, &pDone);

		if( pDone && !*pDone ) {

			return nData;
		}
	}

	return nDef;
}

double CJsonConfig::GetValueAsDouble(UINT uIndex, double nDef) const
{
	CString Data = GetValue(uIndex, "");

	if( !Data.IsEmpty() ) {

		PSTR   pDone = NULL;

		double nData = strtod(Data, &pDone);

		if( pDone && !*pDone ) {

			return nData;
		}
	}

	return nDef;
}

BOOL CJsonConfig::GetValueAsBool(CString Name, BOOL fDef) const
{
	CString Data = GetValue(Name, "");

	if( !Data.IsEmpty() ) {

		if( Data == "true" || Data == "1" ) {

			return TRUE;
		}

		if( Data == "false" || Data == "0" ) {

			return FALSE;
		}
	}

	return fDef;
}

BOOL CJsonConfig::GetValueAsBool(UINT uIndex, BOOL fDef) const
{
	CString Data = GetValue(uIndex, "");

	if( !Data.IsEmpty() ) {

		if( Data == "true" || Data == "1" ) {

			return TRUE;
		}

		if( Data == "false" || Data == "0" ) {

			return FALSE;
		}
	}

	return fDef;
}

CIpAddr CJsonConfig::GetValueAsIp(CString Name, CIpAddr Def) const
{
	CString Data = GetValue(Name, "");

	if( !Data.IsEmpty() ) {

		return CIpAddr(PCTXT(Data));
	}

	return Def;
}

CIpAddr CJsonConfig::GetValueAsIp(UINT uIndex, CIpAddr Def) const
{
	CString Data = GetValue(uIndex, "");

	if( !Data.IsEmpty() ) {

		return CIpAddr(PCTXT(Data));
	}

	return Def;
}

CMacAddr CJsonConfig::GetValueAsMac(CString Name, CMacAddr Def) const
{
	CString Data = GetValue(Name, "");

	if( !Data.IsEmpty() ) {

		return CMacAddr(PCTXT(Data));
	}

	return Def;
}

CMacAddr CJsonConfig::GetValueAsMac(UINT uIndex, CMacAddr Def) const
{
	CString Data = GetValue(uIndex, "");

	if( !Data.IsEmpty() ) {

		return CMacAddr(PCTXT(Data));
	}

	return Def;
}

CJsonConfig * CJsonConfig::GetChild(CString Name) const
{
	CJsonData *pChild = m_pJson->GetChild(Name);

	if( pChild ) {

		return New CJsonConfig(pChild, m_pKeys);
	}

	return NULL;
}

CJsonConfig * CJsonConfig::GetChild(UINT uIndex) const
{
	CJsonData *pChild = m_pJson->GetChild(uIndex);

	if( pChild ) {

		return New CJsonConfig(pChild, m_pKeys);
	}

	return NULL;
}

CJsonConfig * CJsonConfig::GetChild(INDEX Index) const
{
	CJsonData *pChild = m_pJson->GetChild(Index);

	if( pChild ) {

		return New CJsonConfig(pChild, m_pKeys);
	}

	return NULL;
}

CJsonData * CJsonConfig::GetJsonData(void)
{
	return m_pJson;
}

UINT CJsonConfig::GetCount(void) const
{
	return m_pJson->GetCount();
}

DWORD CJsonConfig::GetCRC(void) const
{
	CString Data = "DATA";

	ReadString(Data, m_pJson);

	DWORD c = crc(PCBYTE(PCTXT(Data)), Data.GetLength());

	return (c < 2) ? 2 : c;
}

// Implementation

bool CJsonConfig::ReadPersonality(IConfigStorage *pConfig)
{
	if( pConfig ) {

		CString Text;

		if( pConfig->GetConfig('p', Text) ) {

			CAutoPointer<CJsonData> pJson(new CJsonData);

			if( pJson->Parse(Text) ) {

				CJsonData *pKeys;

				if( (pKeys = pJson->GetChild("set")) ) {

					if( (pKeys = pKeys->GetChild("keys")) ) {

						if( pKeys->GetCount() ) {

							m_pKeys = new CStringMap;

							for( INDEX i = pKeys->GetHead(); !pKeys->Failed(i); pKeys->GetNext(i) ) {

								CJsonData *pKey = pKeys->GetChild(i);

								m_pKeys->Insert(pKey->GetValue("name", ""), pKey->GetValue("value", ""));
							}

							return true;
						}
					}
				}
			}
		}
	}

	return false;
}

bool CJsonConfig::ReadFromPersonality(CString &Data) const
{
	if( m_pKeys ) {

		INDEX i = m_pKeys->FindName(Data.Mid(1));

		if( !m_pKeys->Failed(i) ) {

			Data = m_pKeys->GetData(i);

			return true;
		}
	}

	return true;
}

bool CJsonConfig::ReadString(CString &Data, CJsonData *pJson) const
{
	for( INDEX n = pJson->GetHead(); !pJson->Failed(n); pJson->GetNext(n) ) {

		if( pJson->GetType(n) == jsonObject ) {

			Data += ",{";

			ReadString(Data, pJson->GetChild(n));

			Data += ",}";
		}
		else {
			Data += ",";

			Data += pJson->GetName(n);

			Data += "=";

			CString Read = pJson->GetValue(n);

			ReadFromPersonality(Read);

			Data += Read;
		}
	}

	return true;
}

// End of File
