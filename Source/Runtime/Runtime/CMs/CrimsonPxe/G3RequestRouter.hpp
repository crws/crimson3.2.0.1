
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3RequestRouter_HPP

#define	INCLUDE_G3RequestRouter_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Request Router
//

class CG3RequestRouter : public ILinkService
{
public:
	// Constructor
	CG3RequestRouter(ICrimsonPxe *pPxe);

	// Destructor
	~CG3RequestRouter(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ILinkService
	void Timeout(void);
	UINT Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans);
	void EndLink(CG3LinkFrame &Req);

protected:
	// Data Members
	ULONG	       m_uRefs;
	ILinkService * m_p[16];
};

// End of File

#endif
