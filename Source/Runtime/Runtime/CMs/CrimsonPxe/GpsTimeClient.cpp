
#include "intern.hpp"

#include "GpsTimeClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// GPS Time Client
//

// Constructor

CGpsTimeClient::CGpsTimeClient(CJsonConfig *pJson) : CBaseTimeClient(pJson)
{
	m_Name = "GpsSync";

	m_Diag = "gps";
}

// Overridables

BOOL CGpsTimeClient::PerformSync(void)
{
	// TODO -- Find correct numbered item!!!

	AfxGetAutoObject(pTime, "net.location", 0, ILocationSource);

	if( pTime ) {

		CTimeSourceInfo Info;

		if( pTime->GetLocationTime(Info) ) {

			return SyncTime(Info);
		}
	}

	return FALSE;
}

// End of File
