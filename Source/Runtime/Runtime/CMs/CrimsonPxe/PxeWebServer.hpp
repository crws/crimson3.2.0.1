
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PxeWebServer_HPP

#define	INCLUDE_PxeWebServer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class  CPxeHttpServer;

class  CPxeHttpServerSession;

class  CWebFiles;

struct CWebReqContext;

//////////////////////////////////////////////////////////////////////////
//
// PXE Web Server
//

class CPxeWebServer : public IClientProcess
{
public:
	// Constructor
	CPxeWebServer(ICrimsonPxe *pPxe);

	// Destructor
	~CPxeWebServer(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

	// Redirection
	void EnableRedirect(BOOL fEnable);

	// Operations
	BOOL ExecuteAjax(CWebReqContext const &Ctx, CString Name);
	BOOL ReplyWithPxeFile(CWebReqContext const &Ctx, CString Name);
	BOOL ReplyWithDevFile(CWebReqContext const &Ctx, CString Name, BOOL fExpire);

protected:
	// System Command
	struct CSysCmd
	{
		UINT   uName;
		UINT   uDesc;
		bool   fLinux;
		char * pCmd;
		bool   fEnable;
	};

	// System Commands
	static CSysCmd m_Cmds[];

	// Data Members
	ULONG		       m_uRefs;
	BOOL		       m_fRedirect;
	ICrimsonPxe	     * m_pPxe;
	CHttpServerOptions   * m_pOpts;
	CHttpServerManager   * m_pManager;
	CPxeHttpServer       * m_pServer;
	CWebFiles	     * m_pFiles;
	IPlatform	     * m_pPlatform;
	IFeatures	     * m_pFeatures;
	IConfigStorage	     * m_pConfig;
	ISchemaGenerator     * m_pSchema;
	INetUtilities	     * m_pNetUtils;

	// Server Management
	BOOL OpenServer(void);
	BOOL CloseServer(void);

	// Element Expansion
	BOOL    ExpandElements(CWebReqContext const &Ctx, CString &Text, BOOL &fCache);
	CString GetElementData(CWebReqContext const &Ctx, CString Name, BOOL &fCache);

	// Page Handlers
	CString ExpandGlobal(CWebReqContext const &Ctx, CString Name);
	CString ExpandSystem(CWebReqContext const &Ctx, CString Name);
	CString ExpandConfig(CWebReqContext const &Ctx, CString Name);

	// Ajax Handlers
	BOOL ExecuteSysCommand(CWebReqContext const &Ctx);
	BOOL ExecuteSysDebugUpdate(CWebReqContext const &Ctx);
	BOOL ExecuteSysPcapStatus(CWebReqContext const &Ctx);
	BOOL ExecuteSysPcapControl(CWebReqContext const &Ctx);
	BOOL ExecuteSysPcapRead(CWebReqContext const &Ctx);
	BOOL ExecuteJumpConfig(CWebReqContext const &Ctx);
	BOOL ExecuteJumpRuntime(CWebReqContext const &Ctx);
	BOOL ExecuteReadPipe(CWebReqContext const &Ctx);
	BOOL ExecuteMainStatus(CWebReqContext const &Ctx);
	BOOL ExecuteDeviceStatus(CWebReqContext const &Ctx);
	BOOL ExecutePxeCommand(CWebReqContext const &Ctx);
	BOOL ExecuteFaceStatus(CWebReqContext const &Ctx);
	BOOL ExecuteCellStatus(CWebReqContext const &Ctx);
	BOOL ExecuteWiFiStatus(CWebReqContext const &Ctx);
	BOOL ExecuteCellCommand(CWebReqContext const &Ctx);
	BOOL ExecuteWiFiCommand(CWebReqContext const &Ctx);
	BOOL ExecuteWiFiScan(CWebReqContext const &Ctx);
	BOOL ExecuteGetStatus(CWebReqContext const &Ctx);
	BOOL ExecuteSysPing(CWebReqContext const &Ctx);
	BOOL ExecuteSysInfo(CWebReqContext const &Ctx);
	BOOL ExecuteSysRead(CWebReqContext const &Ctx);
	BOOL ExecuteLogRead(CWebReqContext const &Ctx);
	BOOL ExecuteMakePass(CWebReqContext const &Ctx);
	BOOL ExecuteMakeCert(CWebReqContext const &Ctx);
	BOOL ExecuteDecrypt(CWebReqContext const &Ctx);
	BOOL ExecuteSetPass(CWebReqContext const &Ctx);
	BOOL ExecuteSysWrite(CWebReqContext const &Ctx);
	BOOL ExecuteSysReset(CWebReqContext const &Ctx);
	BOOL ExecuteGetDump(CWebReqContext const &Ctx);
	BOOL ExecuteClearDumps(CWebReqContext const &Ctx);
	BOOL ExecuteSysToolStart(CWebReqContext const &Ctx);
	BOOL ExecuteSysToolStop(CWebReqContext const &Ctx);
	BOOL ExecuteDevRead(CWebReqContext const &Ctx);
	BOOL ExecuteKeyWrite(CWebReqContext const &Ctx);
	BOOL ExecuteRestart(CWebReqContext const &Ctx);
	BOOL ExecuteToolAutoSled(CWebReqContext const &Ctx);
	BOOL ExecuteToolAutoPort(CWebReqContext const &Ctx);
	BOOL ExecutePutImage(CWebReqContext const &Ctx);

	// Ajax Helpers
	BOOL GetCellPipeName(CString &Name, UINT uIndex);
	BOOL GetWiFiPipeName(CString &Name, UINT uIndex);
	BOOL AppendPxeStatus(CString &Data, BOOL fFull);
	BOOL AppendGpsStatus(CString &Data, BOOL fFull);
	BOOL AppendDeviceInfo(CString &Data, BOOL fFull);
	BOOL AppendCoreDumps(CString &Data);
	BOOL AppendFaceStatus(CString &Face, CString const &Find);
	BOOL AppendFaceStatus(CString &Face, UINT uFace);
	BOOL AppendEthernetStatus(CString &Face, UINT uIndex, BOOL fFull);
	BOOL AppendCellStatus(CString &Face, UINT uIndex, BOOL fFull);
	BOOL AppendWiFiStatus(CString &Face, UINT uIndex, BOOL fFull);
	BOOL AppendEthernetStatus(CString &Face, CEthernetStatusInfo const &Info, BOOL fFull);
	BOOL AppendCellStatus(CString &Face, CCellStatusInfo const &Info, BOOL fFull);
	BOOL AppendWiFiStatus(CString &Face, CWiFiStatusInfo const &Info, BOOL fFull);
	void AppendJson(CString &Json, PCTXT pName, PCTXT pData);
	void AppendJson(CString &Json, PCTXT pName, bool fData);
	void AppendJson(CString &Json, PCTXT pName, UINT uData);
	void AppendJson(CString &Json, PCTXT pName, double nData);
	void AppendOkayAndClose(CString &Json, bool fOkay);
	UINT GetConnectTime(UINT uTime);
	UINT ToBars(UINT uSignal);

	// File Reader
	CString ReadFile(CString const &Name);

	// Implementation
	void AddListItem(CString &d, CHttpServerRequest *pReq, CString Url, CString Text);
	void AddSubMenu(CString &d, CString Text);
	void EndSubMenu(CString &d);
	BOOL RunCommand(CString &t, UINT n);
	void MakeOptionTable(CString &t, PCTXT p, CString d);
	void CheckCmds(void);
	BOOL CheckType(CString const &Text, char cTag);
};

// End of File

#endif
