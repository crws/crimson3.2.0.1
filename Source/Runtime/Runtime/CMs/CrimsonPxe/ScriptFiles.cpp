
#include "Intern.hpp"

#include "ScriptFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Helper
//

#define Entry(f, p) { f, data_##p, size_##p }

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

#include "c3-add-dns.sh.dat"
#include "c3-add-host.sh.dat"
#include "c3-cycle.sh.dat"
#include "c3-def-cert.sh.dat"
#include "c3-dep.sh.dat"
#include "c3-dhcp-event.sh.dat"
#include "c3-drop-dns.sh.dat"
#include "c3-drop-host.sh.dat"
#include "c3-get-certs.sh.dat"
#include "c3-ifbreak.sh.dat"
#include "c3-ifdn.sh.dat"
#include "c3-ifmake.sh.dat"
#include "c3-ifstart.sh.dat"
#include "c3-ifstop.sh.dat"
#include "c3-ifup.sh.dat"
#include "c3-init.sh.dat"
#include "c3-ipsec.sh.dat"
#include "c3-ipsec-event.sh.dat"
#include "c3-ipt-event.sh.dat"
#include "c3-make-cert.sh.dat"
#include "c3-prep-rules.sh.dat"
#include "c3-rc-crimson.sh.dat"
#include "c3-rc-sysinit.sh.dat"
#include "c3-rc-syslog.sh.dat"
#include "c3-rc-udev2.sh.dat"
#include "c3-show-dns.sh.dat"
#include "c3-show-filters.sh.dat"
#include "c3-show-ipsec.sh.dat"
#include "c3-show-leases.sh.dat"
#include "c3-show-rules.sh.dat"
#include "c3-term.sh.dat"
#include "c3-try.sh.dat"
#include "c3-update-wifi.sh.dat"

//////////////////////////////////////////////////////////////////////////
//
// File Table
//

CScriptFileData const CScriptFiles::m_Files[] = {

	Entry("c3-add-dns",		c3_add_dns_sh),
	Entry("c3-add-host",		c3_add_host_sh),
	Entry("c3-cycle",		c3_cycle_sh),
	Entry("c3-def-cert",		c3_def_cert_sh),
	Entry("c3-dep",			c3_dep_sh),
	Entry("c3-dhcp-event",		c3_dhcp_event_sh),
	Entry("c3-drop-dns",		c3_drop_dns_sh),
	Entry("c3-drop-host",		c3_drop_host_sh),
	Entry("c3-get-certs",		c3_get_certs_sh),
	Entry("c3-ifbreak",		c3_ifbreak_sh),
	Entry("c3-ifdn",		c3_ifdn_sh),
	Entry("c3-ifmake",		c3_ifmake_sh),
	Entry("c3-ifstart",		c3_ifstart_sh),
	Entry("c3-ifstop",		c3_ifstop_sh),
	Entry("c3-ifup",		c3_ifup_sh),
	Entry("c3-init",		c3_init_sh),
	Entry("c3-ipsec",		c3_ipsec_sh),
	Entry("c3-ipsec-event",		c3_ipsec_event_sh),
	Entry("c3-ipt-event",		c3_ipt_event_sh),
	Entry("c3-make-cert",		c3_make_cert_sh),
	Entry("c3-prep-rules",		c3_prep_rules_sh),
	Entry("c3-rc-crimson",		c3_rc_crimson_sh),
	Entry("c3-rc-sysinit",		c3_rc_sysinit_sh),
	Entry("c3-rc-syslog",		c3_rc_syslog_sh),
	Entry("c3-rc-udev2",		c3_rc_udev2_sh),
	Entry("c3-show-dns",		c3_show_dns_sh),
	Entry("c3-show-filters",	c3_show_filters_sh),
	Entry("c3-show-ipsec",		c3_show_ipsec_sh),
	Entry("c3-show-leases",		c3_show_leases_sh),
	Entry("c3-show-rules",		c3_show_rules_sh),
	Entry("c3-term",		c3_term_sh),
	Entry("c3-try",			c3_try_sh),
	Entry("c3-update-wifi",		c3_update_wifi_sh)
};

//////////////////////////////////////////////////////////////////////////
//
// Script Files
//

// Constructors

CScriptFiles::CScriptFiles(void)
{
	MakeIndex();
}

// File Lookup

BOOL CScriptFiles::FindFile(CScriptFileData const * &pFile, UINT uPos)
{
	if( uPos < elements(m_Files) ) {

		pFile = m_Files + uPos;

		return TRUE;
	}

	return FALSE;
}

BOOL CScriptFiles::FindFile(CScriptFileData const * &pFile, PCTXT pName)
{
	INDEX n = m_Index.FindName(pName);

	if( !m_Index.Failed(n) ) {

		pFile = m_Files + m_Index.GetData(n);

		return TRUE;
	}

	return FALSE;
}

// Implementation

BOOL CScriptFiles::MakeIndex(void)
{
	for( UINT n = 0; n < elements(m_Files); n++ ) {

		m_Index.Insert(m_Files[n].m_pName, n);
	}

	return TRUE;
}

// End of File
