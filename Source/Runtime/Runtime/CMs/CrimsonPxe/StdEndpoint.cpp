
#include "Intern.hpp"

#include "StdEndpoint.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Timing Constants
//

static UINT const timeTimeout      = (5*60*1000);

static UINT const timeDataTimeout  = (2*60*1000);

static UINT const timeSendTimeout  = (3*60*1000);

static UINT const timeSendDelay	   = 5;

static UINT const timeRecvTimeout  = (1*60*1000);

static UINT const timeRecvDelay	   = 5;

static UINT const timeBuffDelay    = 100;

//////////////////////////////////////////////////////////////////////////
//
// Standard RFC Endpoint
//

// Constructor

CStdEndpoint::CStdEndpoint(void)
{
	m_pCmdSock = NULL;

	m_fDebug   = TRUE;

	m_wPort    = 0;
}

// Destructor

CStdEndpoint::~CStdEndpoint(void)
{
	CloseCmdSocket();
}

// Operations

BOOL CStdEndpoint::Send(PCTXT pText)
{
	return Send(m_pCmdSock, pText);
}

BOOL CStdEndpoint::SendFile(FILE *pFile, BOOL fCode)
{
	return SendFile(m_pCmdSock, pFile, fCode);
}

// Transport

BOOL CStdEndpoint::Send(ISocket *pSock, PCTXT pText)
{
	UINT uSize  = strlen(pText);

	UINT uLimit = 1280;

	if( uSize ) {
		
		if( strchr(pText, '\n') == pText + uSize - 1 ) {

			if( uSize > 40 ) {

				AfxTrace(">>> %s...\n", PCTXT(CString(pText, 40)));
			}
			else {
				AfxTrace(">>> %s\n", PCTXT(CString(pText, uSize-2)));
			}
		}
	}

	while( uSize ) {

		CAutoBuffer pBuff(uLimit);

		if( pBuff ) {

			UINT uCopy = min(uLimit, uSize);

			memcpy(pBuff->AddTail(uCopy), pText, uCopy);

			SetTimer(timeSendTimeout);

			while( pSock->Send(pBuff) == E_FAIL ) {

				UINT Phase;

				pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					if( GetTimer() ) {

						Sleep(timeSendDelay);

						continue;
					}

					return FALSE;
				}
			}

			pText += uCopy;

			uSize -= uCopy;

			pBuff.TakeOver();

			continue;
		}

		Sleep(timeBuffDelay);
	}

	return TRUE;
}

// File Transmission

BOOL CStdEndpoint::SendFile(ISocket *pSock, FILE *pFile)
{
	return SendFile(pSock, pFile, FALSE);
}

BOOL CStdEndpoint::SendFile(ISocket *pSock, FILE *pFile, BOOL fCode)
{
	UINT uLimit = fCode ? 912 : 1280;

	for( ;;) {

		CAutoBuffer pBuff(uLimit);

		if( pBuff ) {

			PBYTE pData = pBuff->GetData();

			UINT  uData = fread(pData, 1, uLimit, pFile);

			if( uData ) {

				if( fCode ) {

					CString Code = CBase64::ToBase64(pData, uData, CBase64::encBasic);

					memcpy(pData, Code.data(), Code.size());

					uData = Code.size();
				}

				pBuff->AddTail(uData);

				SetTimer(timeSendTimeout);

				while( pSock->Send(pBuff) == E_FAIL ) {

					UINT Phase;

					pSock->GetPhase(Phase);

					if( Phase == PHASE_OPEN ) {

						if( GetTimer() ) {

							Sleep(timeSendDelay);

							continue;
						}
					}

					return FALSE;
				}

				pBuff.TakeOver();

				continue;
			}

			return TRUE;

		}

		Sleep(timeBuffDelay);
	}
}

BOOL CStdEndpoint::RecvFile(ISocket *pSock, FILE *pFile)
{
	SetTimer(timeRecvTimeout);

	for( ;;) {

		CAutoBuffer pBuff;

		if( pSock->Recv(pBuff) == E_FAIL ) {

			UINT Phase;

			pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				if( GetTimer() ) {

					Sleep(timeRecvDelay);

					continue;
				}
			}

			if( Phase == PHASE_CLOSING ) {

				pSock->Close();

				return TRUE;
			}

			return FALSE;
		}

		fwrite(pBuff->GetData(), 1, pBuff->GetSize(), pFile);

		SetTimer(timeRecvTimeout);
	}
}

// Socket Management

BOOL CStdEndpoint::CheckCmdSocket(void)
{
	if( m_pCmdSock ) {

		UINT Phase;

		m_pCmdSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			AbortCmdSocket();

			return FALSE;
		}

		if( Phase == PHASE_CLOSING ) {

			CloseCmdSocket();

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

void CStdEndpoint::CloseCmdSocket(void)
{
	if( m_pCmdSock ) {

		m_pCmdSock->Close();

		m_pCmdSock->Release();

		m_pCmdSock = NULL;
	}
}

void CStdEndpoint::AbortCmdSocket(void)
{
	if( m_pCmdSock ) {

		m_pCmdSock->Abort();

		m_pCmdSock->Release();

		m_pCmdSock = NULL;
	}
}

// End of File
