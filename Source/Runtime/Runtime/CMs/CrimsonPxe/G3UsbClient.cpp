
#include "Intern.hpp"

#include "G3UsbClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

////////////////////////////////////////////////////////////////////////
//
// USB Client Transport
//

// Static Data

static CG3UsbClient * m_pThis = NULL;

// Launcher

global BOOL Create_XPUsb(CJsonConfig *pJson, UINT uLevel, ILinkService *pService)
{
	if( pJson && pJson->GetValueAsBool("enable", FALSE) ) {

		CG3UsbClient *pClient = New CG3UsbClient(uLevel, pService);

		if( pClient->Open(pJson) ) {

			m_pThis = pClient;

			return TRUE;
		}

		delete pClient;
	}

	return FALSE;
}

global BOOL Delete_XPUsb(void)
{
	if( m_pThis ) {

		m_pThis->Close();

		delete m_pThis;

		m_pThis = NULL;

		return TRUE;
	}

	return FALSE;
}

// Instantiator

global IUsbFuncEvents * Create_UsbLink(UINT uLevel, ILinkService *pService)
{
	return New CG3UsbClient(uLevel, pService);
}

// Constructor

CG3UsbClient::CG3UsbClient(UINT uLevel, ILinkService *pService)
{
	StdSetRef();

	m_pEvent    = Create_ManualEvent();

	m_pDriver   = NULL;

	m_uLevel    = uLevel;

	m_pService  = pService;

	m_pThread   = NULL;
}

// Destructor

CG3UsbClient::~CG3UsbClient(void)
{
	AfxRelease(m_pDriver);

	m_pEvent->Release();
}

// Config Constructor

CG3UsbClient::CConfig::CConfig(void)
{
}

// Config Operations

BOOL CG3UsbClient::CConfig::Parse(CJsonConfig *pJson)
{
	return TRUE;
}

// Operations

BOOL CG3UsbClient::Open(CJsonConfig *pJson)
{
	if( m_Config.Parse(pJson) ) {

		AfxGetAutoObject(pFunc, "usb.func", 0, IUsbFuncStack);

		AfxGetAutoObject(pCtrl, "usbctrl-d", 0, IUsbDriver);

		if( pFunc && pCtrl ) {

			pFunc->Bind((IUsbFuncHardwareDriver *) (IUsbDriver *) pCtrl);

			pFunc->Bind(this);

			pFunc->SetProduct(GetOemDriverBase() + 0x0201, "HMI");

			pFunc->Start();

			return TRUE;
		}
	}

	return FALSE;
}

void CG3UsbClient::Close(void)
{
	AfxGetAutoObject(pFunc, "usb.func", 0, IUsbFuncStack);

	pFunc->Stop();
}

// IUnknown

HRESULT CG3UsbClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IUsbFuncEvents);

	StdQueryInterface(IUsbFuncEvents);

	StdQueryInterface(IUsbEvents);

	StdQueryInterface(ILinkTransport);

	return E_NOINTERFACE;
}

ULONG CG3UsbClient::AddRef(void)
{
	StdAddRef();
}

ULONG CG3UsbClient::Release(void)
{
	StdRelease();
}

// IUsbEvents

BOOL CG3UsbClient::GetDriverInterface(IUsbDriver *&pDriver)
{
	return FALSE;
}

void CG3UsbClient::OnBind(IUsbDriver *pDriver)
{
	pDriver->QueryInterface(AfxAeonIID(IUsbFuncCompDriver), (void **) &m_pDriver);

	m_iInterface = m_pDriver->FindInterface(this);
}

void CG3UsbClient::OnInit(void)
{
	m_iEndptRecv = m_pDriver->GetFirsEndptAddr(m_iInterface) + 0;

	m_iEndptSend = m_pDriver->GetFirsEndptAddr(m_iInterface) + 1;
}

void CG3UsbClient::OnStart(void)
{
	m_pThread = CreateThread(ThreadUsbClient, m_uLevel, this, 0);
}

void CG3UsbClient::OnStop(void)
{
	if( m_pThread ) {

		m_pThread->Destroy();

		m_pThread = NULL;
	}
}

// IUsbFuncEvents

void CG3UsbClient::OnState(UINT uState)
{
	if( uState == devConfigured ) {

		m_pEvent->Set();
	}
	else {
		m_pEvent->Clear();
	}
}

BOOL CG3UsbClient::OnSetupClass(UsbDeviceReq &Req)
{
	return FALSE;
}

BOOL CG3UsbClient::OnSetupVendor(UsbDeviceReq &Req)
{
	return FALSE;
}

BOOL CG3UsbClient::OnSetupOther(UsbDeviceReq &Req)
{
	return FALSE;
}

BOOL CG3UsbClient::OnGetDevice(UsbDesc &Desc)
{
	return FALSE;
}

BOOL CG3UsbClient::OnGetQualifier(UsbDesc &Desc)
{
	return FALSE;
}

BOOL CG3UsbClient::OnGetConfig(UsbDesc &Desc)
{
	return FALSE;
}

BOOL CG3UsbClient::OnGetInterface(UsbDesc &Desc)
{
	UsbInterfaceDesc &Int = (UsbInterfaceDesc &) Desc;

	Int.m_bEndpoints = 2;

	Int.m_bClass	 = devVendor;

	Int.m_bSubClass	 = devVendor;

	Int.m_bProtocol	 = devVendor;

	return TRUE;
}

BOOL CG3UsbClient::OnGetEndpoint(UsbDesc &Desc)
{
	UsbEndpointDesc &Ep = (UsbEndpointDesc &) Desc;

	if( Ep.m_bAddr == 1 ) {

		Ep.m_bDirIn    = FALSE;

		Ep.m_bTransfer = eptBulk;

		return TRUE;
	}

	if( Ep.m_bAddr == 2 ) {

		Ep.m_bDirIn    = TRUE;

		Ep.m_bTransfer = eptBulk;

		return TRUE;
	}

	return FALSE;
}

BOOL CG3UsbClient::OnGetString(UsbDesc *&pDesc, UINT i)
{
	return FALSE;
}

// Thread Entry

void CG3UsbClient::ThreadEntry(void)
{
	WaitHost();

	for( ;;) {

		WaitRunning();

		if( GetRequest() ) {

			UINT p = Process();

			if( p == procError ) {

				m_Rep.StartFrame(m_Req.GetService(), opNak);
			}

			SendReply();

			if( p == procEndLink ) {

				Sleep(50);

				Shutdown();

				Sleep(50);

				EndLink(m_Req);

				ExitThread(0);
			}
		}
	}
}

// Implementation

void CG3UsbClient::WaitHost(void)
{
	m_fInit = true;

	SetTimer(g_uTimeout);

	while( GetTimer() ) {

		if( m_pDriver->GetState() >= devDefault && m_pDriver->GetState() <= devConfigured ) {

			return;
		}

		Sleep(20);
	}

	Timeout();

	m_fInit = false;
}

void CG3UsbClient::WaitRunning(void)
{
	m_pEvent->Wait(FOREVER);
}

void CG3UsbClient::Shutdown(void)
{
	m_pThread->Release();

	m_pThread = NULL;

	if( m_pDriver ) {

		m_pDriver->Stop();
	}
}

// ILinkTransport

BOOL CG3UsbClient::IsUsb(void)
{
	return TRUE;
}

BOOL CG3UsbClient::IsP2P(void)
{
	return TRUE;
}

void CG3UsbClient::SetAuth(void)
{
}

// Frame Handlers

BOOL CG3UsbClient::GetRequest(void)
{
	for( ;;) {

		UINT uTimeout = m_fInit ? g_uTimeout : FOREVER;

		if( Recv(PBYTE(&m_CB), sizeof(m_CB), uTimeout) == sizeof(m_CB) ) {

			if( m_CB.m_dwSig == SIG_G3CB ) {

				m_wTag = m_CB.m_wTag;

				m_Req.StartFrame(m_CB.m_bService, m_CB.m_bOpcode);

				m_Req.SetFlags(m_CB.m_bFlags);

				m_Req.AddData(m_CB.m_bData, m_CB.m_bDataSize);

				if( m_CB.m_wBulkSize ) {

					INT nCount = MotorToHost(m_CB.m_wBulkSize);

					while( nCount > 0 ) {

						UINT uSize = Recv(m_bRxData, Min(sizeof(m_bRxData), nCount), 5000);

						if( uSize != NOTHING ) {

							m_Req.AddData(m_bRxData, uSize);

							nCount -= uSize;

							continue;
						}

						return FALSE;
					}
				}

				return TRUE;
			}
		}
		else {
			if( m_fInit ) {

				Timeout();

				m_fInit = FALSE;
			}

			return FALSE;
		}
	}
}

BOOL CG3UsbClient::SendReply(void)
{
	m_SB.m_dwSig   = SIG_G3SB;

	m_SB.m_wTag    = m_wTag;

	m_SB.m_bOpcode = m_Rep.GetOpcode();

	UINT  uCount = m_Rep.GetDataSize();

	PBYTE pData  = m_Rep.GetData();

	if( m_Rep.GetOpcode() == opReply ) {

		if( uCount == 1 ) {

			if( *pData == 0 ) {

				m_SB.m_bOpcode = opReplyFalse;

				uCount = 0;
			}

			if( *pData == 1 ) {

				m_SB.m_bOpcode = opReplyTrue;

				uCount = 0;
			}
		}
	}

	if( uCount ) {

		m_SB.m_bReadBulk = 1;

		m_SB.m_wBulkSize = HostToMotor(WORD(uCount));

		if( Send(PBYTE(&m_SB), sizeof(m_SB), FALSE) ) {

			while( uCount ) {

				UINT uSize = min(uCount, 1024);

				if( Send(pData, uSize, FALSE) ) {

					uCount -= uSize;

					pData  += uSize;
				}
			}

			m_SB.m_dwSig     = SIG_G3SB;

			m_SB.m_wTag      = m_wTag;

			m_SB.m_bOpcode   = m_Rep.GetOpcode();

			m_SB.m_bReadBulk = 0;

			m_SB.m_wBulkSize = WORD(uCount);

			return Send(PBYTE(&m_SB), sizeof(m_SB), TRUE);
		}

		return FALSE;
	}

	m_SB.m_bReadBulk = 0;

	m_SB.m_wBulkSize = 0;

	return Send(PBYTE(&m_SB), sizeof(m_SB), TRUE);
}

UINT CG3UsbClient::Process(void)
{
	if( m_pService ) {

		return m_pService->Process(m_Req, m_Rep, this);
	}

	return procError;
}

void CG3UsbClient::Timeout(void)
{
	if( m_pService ) {

		m_pService->Timeout();
	}
}

void CG3UsbClient::EndLink(CG3LinkFrame &Req)
{
	if( m_pService ) {

		m_pService->EndLink(Req);
	}
}

// Transport Layer

BOOL CG3UsbClient::Send(PBYTE pData, UINT uLen, BOOL fLast)
{
	if( m_pDriver ) {

		if( m_pDriver->SendBulk(m_iEndptSend, pData, uLen, FOREVER) == uLen ) {

			if( fLast && !(uLen % m_pDriver->GetMaxPacket(m_iEndptSend)) ) {

				return m_pDriver->SendBulk(m_iEndptSend, NULL, 0, FOREVER) == 0;
			}

			return TRUE;
		}
	}

	return FALSE;
}

UINT CG3UsbClient::Recv(PBYTE pData, UINT uLen, UINT uTimeout)
{
	if( m_pDriver ) {

		return m_pDriver->RecvBulk(m_iEndptRecv, pData, uLen, uTimeout);
	}

	return NOTHING;
}

// Thread Entry

int CG3UsbClient::ThreadUsbClient(IThread *pThread, void *pParam, UINT uParam)
{
	pThread->SetName("XpUsb");

	((CG3UsbClient *) pParam)->ThreadEntry();

	return 0;
}

// End of File
