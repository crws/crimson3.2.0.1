
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandEventStorage_HPP

#define	INCLUDE_NandEventStorage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include <CxNand.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Event Storage
//

class CNandEventStorage : public CNandClient, public IEventStorage
{
public:
	// Constructor
	CNandEventStorage(CNandBlock const &Start, CNandBlock const &End);

	// Destructor
	~CNandEventStorage(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IEventStorage
	void METHOD Init(void);
	void METHOD BindManager(IEventManager *pManager);
	BOOL METHOD ReadMemory(IEventLogger *pLogger);
	BOOL METHOD WriteMemory(CEventInfo const &Info);
	BOOL METHOD ClearMemory(void);

protected:
	// Constants
	static DWORD const magicSlot  = 0x45564E54;
	static DWORD const magicEmpty = 0xFFFFFFFF;

	// Sector Header
	struct CSectorHeader
	{
		DWORD		Magic;
		GUID		Guid;
		DWORD	        Mark;
	};

	// Event Header
	struct CSlot
	{
		CSectorHeader m_Header;
		DWORD	      m_Time;
		DWORD	      m_Type : 2;
		DWORD         m_Source : 3;
		DWORD         m_HasText : 1;
		DWORD         m_Code : 26;
		WCHAR	      m_Text[0];
	};

	// Type Definitions
	typedef	CSectorHeader	      * PSECTOR;
	typedef	CSectorHeader   const * PCSECTOR;
	typedef	CSlot	              * PSLOT;
	typedef	CSlot           const * PCSLOT;

	// Data Members
	ULONG		m_uRefs;
	IEventManager * m_pManager;
	IDatabase     * m_pDatabase;
	CNandSector	m_SectorHead;
	CNandSector	m_SectorTail;
	CNandSector	m_SectorCurrent;
	UINT            m_Next;
	BOOL		m_fFoundEmpty;

	// Write with Relocation
	BOOL WriteWithReloc(CNandSector &Sector);
	BOOL CopySectors(CNandSector &Dest, CNandSector &From, CNandSector const &Stop);

	// Queue
	BOOL IsEmpty(void);
	void FindQueue(void);
	BOOL FindNextBlock(CNandBlock  &Next);
	BOOL FindNextSector(CNandSector &Sector);
	void MarkSector(PSECTOR pSector);

	// Slot Helpers
	void SlotToInfo(CEventInfo &Info, PCSLOT pSlot);
	void InfoToSlot(PSLOT pSlot, CEventInfo const &Info);

	// Debug
	void DebugShowQueue(void);
};

// End of File

#endif
