
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosHardwareApplicator_HPP

#define INCLUDE_RlosHardwareApplicator_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

class CModuleFirmware;

//////////////////////////////////////////////////////////////////////////
//
// RLOS Hardware Applicator
//

class CRlosHardwareApplicator :
	public IHardwareApplicator,
	public IUsbSystem
{
public:
	// Constructor
	CRlosHardwareApplicator(void);

	// Destructor
	~CRlosHardwareApplicator(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IHardwareApplicator
	bool METHOD ApplySettings(CJsonConfig *pJson);

	// IUsbSystem
	void METHOD OnDeviceArrival(IUsbHostFuncDriver *pDriver);
	void METHOD OnDeviceRemoval(IUsbHostFuncDriver *pDriver);

protected:
	// Slot Identifier
	union CSlot
	{
		CSlot(DWORD t, DWORD u, DWORD s)
		{
			dw   = 0;
			type = t;
			unit = u;
			slot = s;
		}

		CSlot(DWORD d)
		{
			dw = d;
		}

		struct
		{
			DWORD type : 4;
			DWORD unit : 4;
			DWORD slot : 4;
		};

		DWORD dw;
	};

	// Data Members
	ULONG		    m_uRefs;
	IPxeModel	  * m_pModel;
	BOOL		    m_fReboot;
	DWORD		    m_crcConfig;
	IPlatform         * m_pPlatform;
	IUsbHostStack     * m_pStack;
	IExpansionSystem  * m_pExpand;
	CModuleFirmware   * m_pFirmware;
	CMap<DWORD, UINT>   m_Modules;
	CMap<DWORD, DWORD>  m_SlotToPath;
	CMap<DWORD, DWORD>  m_PathToSlot;
	CMap<UINT, DWORD>   m_ModToIds;
	CMap<DWORD, UINT>   m_IdsToMod;
	CMap<DWORD, UINT>   m_NicSlots;
	CMap<DWORD, UINT>   m_ModemSlots;
	CMap<DWORD, UINT>   m_ComSlots;
	CMap<DWORD, UINT>   m_IoSlots;
	BOOL		    m_fExtModem;
	UINT		    m_uExtModem;
	UINT		    m_uNicMask;

	// Implementation
	void RegisterNics(void);
	void RegisterPorts(void);
	BOOL ApplyExpansion(CJsonConfig *pJson);
	void LoadModuleMaps(void);
	void LoadSlotMaps(UINT tcount, UINT tport, UINT fcount);
	BOOL ApplyRackModules(UINT uCount, UINT uType, CJsonConfig *pJson);
	BOOL ApplyModules(UINT uType, UINT uUnit, CJsonConfig *pJson);
	BOOL ApplyExtModem(CJsonConfig *pJson);
	BOOL CheckCRC(DWORD &crc, CJsonConfig * &pJson);

	// Model Specific
	void LoadBaseSlotMap(UINT uCount);
	void LoadTetheredSlotMap(UINT uCount, UINT uPort);
	void LoadFixedSlotMap(UINT uCount);
};

// End of File

#endif
