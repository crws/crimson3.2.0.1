
#include "Intern.hpp"

#include "G3SerialClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "G3Slave.hpp"

#include "JsonConfig.hpp"

#include "PortGrabber.hpp"

////////////////////////////////////////////////////////////////////////
//
// Serial Client Transport
//

// Static Data

static CG3SerialClient * m_pThis = NULL;

// Launcher

global BOOL Create_XPSerial(CJsonConfig *pJson, UINT uLevel, UINT uPort, ILinkService *pService)
{
	if( pJson && pJson->GetValueAsBool("enable", FALSE) ) {

		CG3SerialClient *pClient = New CG3SerialClient(uLevel, uPort, pService);

		if( pClient->Open(pJson) ) {

			m_pThis = pClient;

			return TRUE;
		}

		delete pClient;
	}

	return FALSE;
}

global BOOL Delete_XPSerial(void)
{
	if( m_pThis ) {

		m_pThis->Close();

		delete m_pThis;

		m_pThis = NULL;

		return TRUE;
	}

	return FALSE;
}

// Constructor

CG3SerialClient::CG3SerialClient(UINT uLevel, UINT uPort, ILinkService *pService) : CG3SerialBase(uLevel, pService)
{
	m_Name      = "XpSerial";

	m_uPort     = uPort;

	m_pGrabber  = New CPortGrabber;

	piob->RegisterSingleton("c3.grab", m_uPort, m_pGrabber);
}

// Destructor

CG3SerialClient::~CG3SerialClient(void)
{
	piob->RevokeSingleton("c3.grab", m_uPort);
}

// Config Constructor

CG3SerialClient::CConfig::CConfig(void)
{
}

// Config Operations

BOOL CG3SerialClient::CConfig::Parse(CJsonConfig *pJson)
{
	return TRUE;
}

// Operations

BOOL CG3SerialClient::Open(CJsonConfig *pJson)
{
	if( m_Config.Parse(pJson) ) {

		return CG3SerialBase::Open();
	}

	return FALSE;
}

// Frame Processing

void CG3SerialClient::EndLink(CG3LinkFrame &Req)
{
	CG3SerialBase::EndLink(Req);
}

// Transport Layer

BOOL CG3SerialClient::OpenPort(void)
{
	if( m_pGrabber->Claim() ) {

		GuardThread(TRUE);

		AfxGetObject("dev.uart", m_uPort, IPortObject, m_pPort);

		if( m_pPort ) {

			AfxNewObject("util.data-s", IDataHandler, m_pData);

			if( m_pData ) {

				m_pPort->Bind(m_pData);

				CSerialConfig Config;

				Config.m_uPhysical = physicalRS232;
				Config.m_uBaudRate = 115200;
				Config.m_uDataBits = 8;
				Config.m_uStopBits = 1;
				Config.m_uParity   = 0;
				Config.m_uFlags    = 0;

				m_pData->SetRxSize(2560);

				if( m_pPort->Open(Config) ) {

					GuardThread(FALSE);

					return TRUE;
				}

				ClosePort();
			}
		}

		GuardThread(FALSE);

		m_pGrabber->Free();
	}

	return FALSE;
}

BOOL CG3SerialClient::ClosePort(void)
{
	CAutoGuard Guard;

	if( CG3SerialBase::ClosePort() ) {

		m_pGrabber->Free();

		return TRUE;
	}

	return FALSE;
}

BOOL CG3SerialClient::HasPortRequest(void)
{
	return m_pGrabber->HasRequest();
}

void CG3SerialClient::ServicePortRequest(void)
{
	ClosePort();

	OpenPort();
}

// End of File
