
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NandFirmwareProgram_HPP

#define INCLUDE_NandFirmwareProgram_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "NandFirmwareProps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Firmware Programming Object
//

class CNandFirmwareProgram : public CNandFirmwareProps, public IFirmwareProgram
{
public:
	// Constructor
	CNandFirmwareProgram(UINT uStart, UINT uEnd);

	// Destructor
	~CNandFirmwareProgram(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IFirmwareProps
	bool   METHOD IsCodeValid(void);
	PCBYTE METHOD GetCodeVersion(void);
	UINT   METHOD GetCodeSize(void);
	PCBYTE METHOD GetCodeData(void);

	// IFirmwareProgram
	bool METHOD ClearProgram(UINT uBlocks);
	bool METHOD WriteProgram(PCBYTE pData, UINT uCount);
	bool METHOD WriteVersion(PCBYTE pData);
	bool METHOD StartProgram(UINT uTimeout);

protected:
	// Data Members
	UINT m_uPtr;
	bool m_fWrite;

	// Implementation
	bool FlushCode(void);
};

// End of File

#endif
