
#include "Intern.hpp"

#include "G3BootService.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Database Format
//

#include "../../../../../Version/dbver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Boot Loader Service
//

// Instantiator

global ILinkService * Create_BootService(ICrimsonPxe *pPxe)
{
	return New CG3BootService(pPxe);
}

// Constructor

CG3BootService::CG3BootService(ICrimsonPxe *pPxe)
{
	m_pPxe       = pPxe;

	m_pPlatform  = NULL;

	m_pFirmPend  = NULL;

	m_pFirmProps = NULL;

	m_fPend      = FALSE;

	AfxGetObject("platform", 0, IPlatform, m_pPlatform);

	AfxGetObject("c3.firmpend", 0, IFirmwareProgram, m_pFirmPend);

	AfxGetObject("c3.firmprops", 0, IFirmwareProps, m_pFirmProps);

	StdSetRef();
}

// Destructor

CG3BootService::~CG3BootService(void)
{
	AfxRelease(m_pPlatform);

	AfxRelease(m_pFirmPend);

	AfxRelease(m_pFirmProps);
}

// IUnknown

HRESULT CG3BootService::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ILinkService);

	StdQueryInterface(ILinkService);

	return E_NOINTERFACE;
}

ULONG CG3BootService::AddRef(void)
{
	StdAddRef();
}

ULONG CG3BootService::Release(void)
{
	StdRelease();
}

// ILinkService

void CG3BootService::Timeout(void)
{
}

UINT CG3BootService::Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans)
{
	if( Req.GetService() == servBoot ) {

		switch( Req.GetOpcode() ) {

			case bootReadModel:
				return ReadModel(Req, Rep);

			case bootReadOem:
				return ReadOem(Req, Rep);

			case bootCheckVersion:
				return CheckVersion(Req, Rep);

			case bootForceReset:
				return ForceReset(Req, Rep, pTrans);

			case bootCheckHardware:
				return CheckHardware(Req, Rep);

			case bootStartProgram:
				return StartProgram(Req, Rep);

			case bootReadRevision:
				return ReadRevision(Req, Rep);

			case bootWriteMAC:
				return WriteMAC(Req, Rep);

			case bootWriteSerial:
				return WriteSerialNumber(Req, Rep);

			case bootCheckLevel:
				return CheckLevel(Req, Rep);

			case bootClearProgram:
				return ClearProgram(Req, Rep);

			case bootWriteProgram:
				return WriteProgram(Req, Rep);

			case bootWriteVersion:
				return WriteVersion(Req, Rep);

			case bootAutoDetect:
				return AutoDetect(Req, Rep);
		}

		return procError;
	}

	return procError;
}

void CG3BootService::EndLink(CG3LinkFrame &Req)
{
	if( Req.GetService() == servBoot ) {

		switch( Req.GetOpcode() ) {

			case bootForceReset:
			{
				UINT  uPtr      = 0;

				DWORD dwTimeout = Req.ReadLong(uPtr);

				m_pPxe->RestartSystem(dwTimeout, 55);

				for(;;) Sleep(FOREVER);
			}

			break;
		}
	}
}

// Implementation

UINT CG3BootService::ReadModel(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	PCTXT pName = m_pPlatform->GetModel();

	Rep.StartFrame(servBoot, opReply);

	Rep.AddData(PBYTE(pName), strlen(pName) + 1);

	return procOkay;
}

UINT CG3BootService::ReadOem(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	PCTXT pName = "Red Lion Controls"; /*!!!*/

	Rep.StartFrame(servBoot, opReply);

	Rep.AddData(PBYTE(pName), strlen(pName) + 1);

	return procOkay;
}

UINT CG3BootService::CheckVersion(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	#if defined(AEON_PLAT_WIN32)

	Rep.StartFrame(servBoot, opReply);

	Rep.AddByte(TRUE);

	#else

	#if defined(AEON_PLAT_LINUX)

	if( CAutoFile("\\.\\tmp\\debug", "r") ) {

		Rep.StartFrame(servBoot, opReply);

		Rep.AddByte(TRUE);

		Rep.AddByte(TRUE);

		return procOkay;
	}

	#endif

	UINT   uPtr  = 0;

	PCBYTE pData = Req.ReadData(uPtr, 16);

	DWORD  Dbase = uPtr < Req.GetDataSize() ? Req.ReadLong(uPtr) : 0;

	PCBYTE pComp = m_pFirmProps->GetCodeVersion();

	UINT   n;

	for( n = 0; n < 16; n++ ) {

		if( pComp[n] != pData[n] ) {

			break;
		}
	}

	Rep.StartFrame(servBoot, opReply);

	Rep.AddByte(n == 16);

	Rep.AddByte(Dbase == DBASE_FORMAT ? 1 : 0);

	#endif

	return procOkay;
}

UINT CG3BootService::ForceReset(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans)
{
	#if defined(AEON_PLAT_WIN32)

	return procOkay;

	#else

	if( m_pFirmPend ) {

		m_fPend = TRUE;

		Rep.StartFrame(servBoot, opReplyTrue);

		return procOkay;
	}

	Rep.StartFrame(servBoot, opAck);

	return procEndLink;

	#endif
}

UINT CG3BootService::CheckHardware(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servBoot, opReply);

	BYTE b1[] = {
		0xFF, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xFF
	};

	BYTE b2[] = {
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00
	};

	Rep.AddData(b1, 16);

	Rep.AddWord(1);

	Rep.AddWord(0);

	Rep.AddData(b2, 16);

	return procOkay;
}

UINT CG3BootService::StartProgram(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	if( m_pFirmPend->StartProgram(0) ) {

		if( m_fPend ) {

			ImageDisable('a');

			Req.StartFrame(servBoot, bootForceReset);

			Req.AddLong(0);

			return procEndLink;
		}
	}

	Rep.StartFrame(servBoot, opNak);

	return procOkay;
}

UINT CG3BootService::ReadRevision(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	// TODO -- Move this somewhere more suitable!

	Rep.StartFrame(servBoot, opReply);

	CAutoFile File("/./bin/osrev", "r");

	if( File ) {

		CString Line = File.GetLine();

		Rep.AddLong(atoi(Line));

		return procOkay;
	}

	Rep.AddLong(1);

	return procOkay;
}

UINT CG3BootService::WriteMAC(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
/*	static const MACADDR Rlc = { { 0x00, 0x05, 0xE4, 0x00, 0x00, 0x00 } };

	UINT   uPtr = 0;

	for( UINT n = 0; n < 2; n ++ ) {

		PCBYTE pMac = Req.ReadData(uPtr, 6);

		if( !memcmp(pMac, &Rlc, 3) ) {

			WORD wAddr = (n == 0) ? Mem(MacId0) : Mem(MacId1);

			FRAMPutData(wAddr, PBYTE(pMac), 6);
			}
		}

*/	Rep.StartFrame(servBoot, opAck);

return procOkay;
}

UINT CG3BootService::WriteSerialNumber(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
/*	UINT  uPtr   = 0;

	UINT  uCount = Req.ReadWord(uPtr);

	PCTXT pData  = PCTXT(Req.ReadData(uPtr, uCount));

	if( g_pIdentity->SetSerial(pData, uCount) ) {

		Rep.StartFrame(servBoot, opAck);

		return procOkay;
		}

*/	return procError;
}

UINT CG3BootService::CheckLevel(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servBoot, opReply);

	Rep.AddByte(1);

	return procOkay;
}

UINT CG3BootService::ClearProgram(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	if( m_pFirmPend ) {

		if( m_fPend ) {

			UINT uPtr   = 0;

			BYTE bCount = Req.ReadByte(uPtr);

			if( m_pFirmPend->ClearProgram(bCount) ) {

				Rep.StartFrame(servBoot, opAck);

				return procOkay;
			}
		}
	}

	Rep.StartFrame(servBoot, opNak);

	return procOkay;
}

UINT CG3BootService::WriteProgram(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	if( m_pFirmPend ) {

		if( m_fPend ) {

			UINT  uPtr   = 0;

			DWORD dwAddr = Req.ReadLong(uPtr);

			WORD  wCount = Req.ReadWord(uPtr);

			PBYTE pData  = PBYTE(Req.ReadData(uPtr, wCount));

			AfxTouch(dwAddr);

			if( m_pFirmPend->WriteProgram(pData, wCount) ) {

				Rep.StartFrame(servBoot, opAck);

				return procOkay;
			}
		}
	}

	Rep.StartFrame(servBoot, opNak);

	return procOkay;
}

UINT CG3BootService::WriteVersion(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	if( m_pFirmPend ) {

		if( m_fPend ) {

			UINT  uPtr   = 0;

			PBYTE pData  = PBYTE(Req.ReadData(uPtr, 16));

			if( m_pFirmPend->WriteVersion(pData) ) {

				Rep.StartFrame(servBoot, opAck);

				return procOkay;
			}
		}
	}

	Rep.StartFrame(servBoot, opNak);

	return procOkay;
}

UINT CG3BootService::AutoDetect(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	AfxGetAutoObject(pFeatures, "dev.security", 0, IFeatures);

	PCTXT pModel   = m_pPlatform->GetModel();

	PCTXT pVariant = m_pPlatform->GetVariant();

	UINT  uGroup   = pFeatures->GetEnabledGroup();

	CPrintf Model("%s|%s|%ux%u|G=%uS0=101", pModel, pVariant, 640, 480, uGroup);

	for( UINT s = 0; s < 3; s++ ) {

		CString   Type;

		CPrintf   Name("/./tmp/skvs.d/SYSTEM_SLED_PORT%u_TYPE", 1+s);

		CAutoFile File(Name, "r");

		if( File ) {

			CString Line = File.GetLine();

			if( Line == "SERIAL" ) {

				CPrintf   Name("/./tmp/skvs.d/SYSTEM_SLED_PORT%u_XR_ID", 1+s);

				CAutoFile File(Name, "r");

				if( File ) {

					CString Text = File.GetLine();

					if( Text == "000" ) {

						Type = "103";
					}

					if( Text == "001" ) {

						Type = "104";
					}

					if( Text == "010" ) {

						Type = "105";
					}
				}
			}

			if( Line == "CELL" ) {

				Type = "100";
			}

			if( Line == "ETH" ) {

				Type = "101";
			}

			if( Line == "WIFIBT" ) {

				Type = "102";
			}

			if( !Type.IsEmpty() ) {

				Model.AppendPrintf(",S%u=%s", s, PCSTR(Type));
			}
		}
	}

	Rep.StartFrame(servBoot, opReply);

	Rep.AddData(PBYTE(PCSTR(Model)), Model.GetLength() + 1);

	return procOkay;
}

// End of File
