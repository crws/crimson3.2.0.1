
#include "intern.hpp"

#include "Identifier.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Identifier Client
//

// Constants

#define ET_REDLION	0x8900

#define ET_SUBTYPE	0x8001

#define ET_VERSION	0x0001

// Constructor

CIdentifier::CIdentifier(CJsonConfig *pJson)
{
	m_fFlash  = TRUE;

	m_fLimit  = TRUE;

	ApplyConfig(pJson);

	m_pPlatform = NULL;

	m_pNetApp   = NULL;

	m_pNetUtils = NULL;

	m_pPxe      = NULL;

	m_Time1     = 0;

	m_Time2     = 0;

	AfxGetObject("platform", 0, IPlatform, m_pPlatform);

	AfxGetObject("c3.net-applicator", 0, INetApplicator, m_pNetApp);

	AfxGetObject("ip", 0, INetUtilities, m_pNetUtils);

	AfxGetObject("pxe", 0, ICrimsonPxe, m_pPxe);

	StdSetRef();
}

// Destructor

CIdentifier::~CIdentifier(void)
{
	AfxRelease(m_pPlatform);

	AfxRelease(m_pNetApp);

	AfxRelease(m_pNetUtils);

	AfxRelease(m_pPxe);
}

// IUnknown

HRESULT CIdentifier::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IDeviceIdentifier);

	return E_NOINTERFACE;
}

ULONG CIdentifier::AddRef(void)
{
	StdAddRef();
}

ULONG CIdentifier::Release(void)
{
	StdRelease();
}

// IDeviceIdentifier

BOOL CIdentifier::RunOperation(BYTE bCode, BOOL fUsb, CBuffer *pBuff)
{
	if( bCode == 1 ) {

		UINT t2 = GetTickCount();

		if( fUsb || !m_fLimit || !m_Time1 || t2 >= m_Time1 + 400 ) {

			PBYTE pSize = pBuff->AddTail(1);

			PBYTE pCode = pBuff->AddTail(1);

			PBYTE pInit = pBuff->AddTail(0);

			if( m_pPlatform ) {

				PCTXT pSerial = m_pPlatform->GetSerial();

				UINT  uSerial = strlen(pSerial);

				*pBuff->AddTail(1) = 1;

				*pBuff->AddTail(1) = BYTE(uSerial);

				memcpy(pBuff->AddTail(uSerial), pSerial, uSerial);
			}

			if( m_pPlatform ) {

				PCTXT pModel = m_pPlatform->GetModel();

				UINT  uModel = strlen(pModel);

				*pBuff->AddTail(1) = 4;

				*pBuff->AddTail(1) = BYTE(uModel);

				memcpy(pBuff->AddTail(uModel), pModel, uModel);
			}

			if( m_pNetUtils ) {

				UINT c = m_pNetUtils->GetInterfaceCount();

				for( UINT n = 0; n < c; n++ ) {

					CIpAddr Addr;

					if( m_pNetUtils->GetInterfaceAddr(n, Addr) ) {

						if( !Addr.IsEmpty() && !Addr.IsLoopback() ) {

							*pBuff->AddTail(1) = 2;

							*pBuff->AddTail(1) = 4;

							memcpy(pBuff->AddTail(4), Addr.m_b, 4);
						}
					}
				}
			}

			if( m_pNetApp ) {

				CStringArray List;

				m_pNetApp->GetmDnsNames(List);

				for( UINT n = 0; n < List.GetCount(); n++ ) {

					PCTXT pName = List[n];

					UINT  uName = strlen(pName);

					*pBuff->AddTail(1) = 3;

					*pBuff->AddTail(1) = BYTE(uName);

					memcpy(pBuff->AddTail(uName), pName, uName);
				}
			}

			if( m_pPxe ) {

				WORD wPort = m_pPxe->GetG3LinkPort();

				*pBuff->AddTail(1) = 6;

				*pBuff->AddTail(1) = 2;

				*pBuff->AddTail(1) = LOBYTE(wPort);

				*pBuff->AddTail(1) = HIBYTE(wPort);
			}

			if( TRUE ) {

				#if defined(AEON_PLAT_WIN32)

				*pBuff->AddTail(1) = 5;

				*pBuff->AddTail(1) = 0;

				#endif
			}

			*pSize = BYTE(pBuff->AddTail(0) - pInit);

			*pCode = 2;

			if( !fUsb ) {

				m_Time1 = t2;
			}

			return TRUE;
		}
	}

	if( bCode == 3 ) {

		UINT t2 = GetTickCount();

		if( fUsb || !m_fLimit || !m_Time2 || t2 >= m_Time2 + 10000 ) {

			if( m_fFlash ) {

				m_pPxe->IdentifyUnit();
			}

			if( !fUsb ) {

				m_Time2 = t2;
			}
		}
	}

	return FALSE;
}

// IClientProcess

BOOL CIdentifier::TaskInit(UINT uTask)
{
	SetThreadName("Identifier");

	piob->RegisterSingleton("identifier", 0, (IClientProcess *) this);

	AddRef();

	return TRUE;
}

INT CIdentifier::TaskExec(UINT uTask)
{
	for( ;;) {

		AfxNewAutoObject(pSockRaw, "sock-raw", ISocket);

		AfxNewAutoObject(pSockUdp, "sock-udp", ISocket);

		if( pSockRaw ) {
			
			CIpAddr Multi(224, 13, 17, 19);

			pSockUdp->Listen(Multi, 55555);

			pSockRaw->Listen(Multi, ET_REDLION);

			UINT t1 = GetTickCount();

			for( ;;) {

				CAutoBuffer pRecv;

				if( pSockRaw->Recv(pRecv) == S_OK ) {

					if( pRecv->GetSize() >= 20 ) {

						PCBYTE pInit = pRecv->GetData();

						pRecv->StripHead(14);

						BYTE bSig[] = { HIBYTE(ET_SUBTYPE), LOBYTE(ET_SUBTYPE),
								HIBYTE(ET_VERSION), LOBYTE(ET_VERSION),
						};

						if( !memcmp(pRecv->GetData(), bSig, sizeof(bSig)) ) {

							pRecv->StripHead(sizeof(bSig));

							BYTE bSize = *BuffStripHead(pRecv, BYTE);

							BYTE bCode = *BuffStripHead(pRecv, BYTE);

							CAutoBuffer pSend(512);

							memcpy(pSend->AddTail(6), pInit + 6, 6);

							memset(pSend->AddTail(6), 0, 6);

							*pSend->AddTail(1) = HIBYTE(ET_REDLION);

							*pSend->AddTail(1) = LOBYTE(ET_REDLION);

							memcpy(pSend->AddTail(sizeof(bSig)), bSig, sizeof(bSig));

							if( RunOperation(bCode, FALSE, pSend) ) {

								if( pSockRaw->Send(pSend) == S_OK ) {

									pSend.TakeOver();
								}
							}

							AfxTouch(bSize);
						}
					}

					continue;
				}

				if( GetTickCount() - t1 >= 10000 ) {

					// If the interface list has changed under us, we
					// might end up having to register to be active on
					// all of the available interfaces...

					pSockRaw->Close();

					break;
				}

				Sleep(20);
			}
		}

		Sleep(250);
	}

	return 0;
}

void CIdentifier::TaskStop(UINT uTask)
{
}

void CIdentifier::TaskTerm(UINT uTask)
{
	piob->RevokeSingleton("identifier", 0);

	Release();
}

// Implementation

void CIdentifier::ApplyConfig(CJsonConfig *pJson)
{
	if( pJson ) {

		m_fFlash = pJson->GetValueAsBool("flash", m_fFlash);

		m_fLimit = pJson->GetValueAsBool("limit", m_fLimit);
	}
}

// End of File
