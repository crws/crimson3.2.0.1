
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MsgTransport_HPP

#define	INCLUDE_MsgTransport_HPP

//////////////////////////////////////////////////////////////////////////
//
// Message Transport Base Class
//

class CMsgTransport : public IMsgTransport
{
public:
	// Constructor
	CMsgTransport(void);

	// Destructor
	~CMsgTransport(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IMsgTransport
	UINT METHOD RegisterSendNotify(UINT uNotify, IMsgSendNotify *pNotify);
	BOOL METHOD RevokeSendNotify(UINT uNotify);
	BOOL METHOD QueueMessage(CMsgPayload *pMessage);

protected:
	// Static Data
	static UINT m_uSeq;

	// Data Members
	ULONG			     m_uRefs;
	IMutex			   * m_pLock;
	ISemaphore		   * m_pWait;
	BOOL			     m_fEnable;
	CList<CMsgPayload *>	     m_Queue;
	CMap<UINT, IMsgSendNotify *> m_Notify;

	// Implementation
	void SendNotify(CMsgPayload const *pMessage, BOOL fOkay);
};

// End of File

#endif
