
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_BaseConfigStorage_HPP

#define INCLUDE_BaseConfigStorage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Config Storage Object
//

class CBaseConfigStorage : public IConfigStorage
{
public:
	// Constructor
	CBaseConfigStorage(IDatabase *pDbase);

	// Destructor
	~CBaseConfigStorage(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IConfigStorage
	bool METHOD AddUpdateSink(IConfigUpdate *pUpdate);
	bool METHOD SetConfig(char cTag, CString const &Text, bool fEdit);
	bool METHOD SetEdited(char cTag, bool fEdit);
	bool METHOD GetConfig(char cTag, CString &Text);
	bool METHOD IsEdited(char cTag);
	bool METHOD ClearData(void);

protected:
	// Update List
	typedef CArray<IConfigUpdate *> CUpdateList;

	// Data Members
	ULONG		   m_uRefs;
	IDatabase	 * m_pDbase;
	CUpdateList        m_Updates;
	CString		   m_Config[4];
	bool		   m_Edited[4];
	ISchemaGenerator * m_pSchema;

	// Implementation
	bool ReadStoredData(void);
	bool WriteStoredData(void);
	bool WriteStoredData(UINT uItem, CString const &Text, bool fEdit, bool fTidy);
	char GetTag(UINT uItem);
	UINT GetItem(char cTag);
};

// End of File

#endif
