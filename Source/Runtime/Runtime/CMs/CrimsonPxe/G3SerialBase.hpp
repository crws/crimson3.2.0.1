
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3SerialBase_HPP

#define	INCLUDE_G3SerialBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "G3LinkFrame.hpp"

#include "G3Slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

interface ILinkService;

////////////////////////////////////////////////////////////////////////
//
// Serial Base Transport
//

class CG3SerialBase : public IClientProcess, public ILinkTransport
{
public:
	// Constructor
	CG3SerialBase(UINT uLevel, ILinkService *pService);

	// Destructor
	~CG3SerialBase(void);

	// Operations
	BOOL Open(void);
	void Close(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

	// ILinkTransport
	BOOL IsUsb(void);
	BOOL IsP2P(void);
	void SetAuth(void);

protected:
	// Configuration
	struct CConfig
	{
		// Constructor
		CConfig(void);

		// Operations
		BOOL Parse(CJsonConfig *pJson);
	};

	// Data
	ULONG		m_uRefs;
	UINT		m_uLevel;
	ILinkService *  m_pService;
	HTHREAD		m_hThread;
	BOOL		m_fInit;
	UINT		m_uTimeout;
	IPortObject  *  m_pPort;
	IDataHandler *  m_pData;
	CG3LinkFrame	m_Req;
	CG3LinkFrame	m_Rep;
	CString         m_Name;
	BYTE		m_bRxBuff[1280];
	BYTE		m_bTxBuff[2560];
	WORD		m_wCheck;
	UINT		m_uSize;
	UINT		m_uPtr;

	// Frame Processing
	UINT Process(void);
	void Timeout(void);
	void EndLink(CG3LinkFrame &Req);

	// Frame Buidling
	void NewFrame(void);
	void AddCtrl(BYTE bCtrl);
	void AddData(PCBYTE pData, UINT uCount);
	void AddByte(BYTE bData);

	// Transport Layer
	BOOL GetFrame(void);
	BOOL PutFrame(void);
	BOOL GetAck(void);
	void SendByte(BYTE b);

	// Port
	virtual BOOL OpenPort(void);
	virtual BOOL ClosePort(void);
	virtual BOOL HasPortRequest(void);
	virtual void ServicePortRequest(void);

	// Session Timeout
	void ResetTimeout(void);
	void CheckTimeout(void);
};

// End of File

#endif
