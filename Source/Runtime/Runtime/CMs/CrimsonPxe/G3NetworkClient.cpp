
#include "Intern.hpp"

#include "G3NetworkClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

////////////////////////////////////////////////////////////////////////
//
// Network Client Transport
//

// Static Data

static CG3NetworkClient * m_pThis = NULL;

// Launcher

global BOOL Create_XPNetwork(CJsonConfig *pJson, UINT uLevel, ILinkService *pService)
{
	if( pJson && pJson->GetValueAsBool("enable", FALSE) ) {

		CG3NetworkClient *pClient = New CG3NetworkClient(uLevel, pService);

		if( pClient->Open(pJson) ) {

			m_pThis = pClient;

			return TRUE;
		}

		delete pClient;
	}

	return FALSE;
}

global BOOL Delete_XPNetwork(void)
{
	if( m_pThis ) {

		m_pThis->Close();

		delete m_pThis;

		m_pThis = NULL;

		return TRUE;
	}

	return FALSE;
}

// Constructor

CG3NetworkClient::CG3NetworkClient(UINT uLevel, ILinkService *pService)
{
	StdSetRef();

	m_uLevel   = uLevel;

	m_pService = pService;

	m_hThread  = NULL;

	m_pSocket  = NULL;

	m_fAuth    = FALSE;
}

// Destructor

CG3NetworkClient::~CG3NetworkClient(void)
{
}

// Config Constructor

CG3NetworkClient::CConfig::CConfig(void)
{
	m_uPort    = 789;

	m_uTimeout = 30000;
}

// Config Operations

BOOL CG3NetworkClient::CConfig::Parse(CJsonConfig *pJson)
{
	if( pJson ) {

		m_uPort    = pJson->GetValueAsUInt("port", 789, 1, 65535);

		m_uTimeout = pJson->GetValueAsUInt("timeout", 30, 5, 3600) * 1000;

		m_ReqAddr  = pJson->GetValueAsIp("reqaddr", IP_EMPTY);

		m_ReqMask  = pJson->GetValueAsIp("reqmask", IP_EMPTY);
	}

	return TRUE;
}

// Operations

BOOL CG3NetworkClient::Open(CJsonConfig *pJson)
{
	if( m_Config.Parse(pJson) ) {

		m_hThread = CreateClientThread(this, 0, m_uLevel);

		return TRUE;
	}

	return FALSE;
}

void CG3NetworkClient::Close(void)
{
	DestroyThread(m_hThread);
}

// IUnknown

HRESULT CG3NetworkClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(ILinkTransport);

	return E_NOINTERFACE;
}

ULONG CG3NetworkClient::AddRef(void)
{
	StdAddRef();
}

ULONG CG3NetworkClient::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CG3NetworkClient::TaskInit(UINT uTask)
{
	SetThreadName("XpNetwork");

	return TRUE;
}

INT CG3NetworkClient::TaskExec(UINT uTask)
{
	BOOL fCheck   = TRUE;

	UINT uTimeout = 0;

	for( ;;) {

		if( !m_pSocket ) {

			CAutoGuard Guard;

			AfxNewObject("net.sock-tcp", ISocket, m_pSocket);

			if( m_pSocket ) {

				m_pSocket->Listen(WORD(m_Config.m_uPort));

				m_pSocket->SetOption(OPT_RECV_QUEUE, 255);

				m_fAuth = FALSE;

				fCheck  = TRUE;
			}
		}

		if( m_pSocket ) {

			UINT uPhase = 0;

			m_pSocket->GetPhase(uPhase);

			switch( uPhase ) {

				case PHASE_OPEN:
				{
					if( fCheck ) {

						if( !CheckAccess() ) {

							m_pSocket->Abort();

							continue;
						}

						fCheck = FALSE;
					}

					if( GetFrame() ) {

						UINT p = Process();

						if( p == procError ) {

							m_Rep.StartFrame(m_Req.GetService(), opNak);
						}

						PutFrame();

						if( p == procEndLink ) {

							m_pSocket->Close();

							m_pSocket->Release();

							m_pSocket = NULL;

							Sleep(50);

							EndLink(m_Req);
						}

						uTimeout = 0;

						continue;
					}

					if( (uTimeout += 5) >= m_Config.m_uTimeout ) {

						m_pSocket->Abort();
					}
				}
				break;

				case PHASE_CLOSING:
				{
					m_pSocket->Close();
				}
				break;

				case PHASE_ERROR:
				{
					CAutoGuard Guard;

					m_pSocket->Release();

					m_pSocket = NULL;
				}
				break;

				default:
				{
					Sleep(20);
				}
				break;
			}

			continue;
		}

		Sleep(5);
	}

	return 0;
}

void CG3NetworkClient::TaskStop(UINT uTask)
{
}

void CG3NetworkClient::TaskTerm(UINT uTask)
{
	if( m_pSocket ) {

		m_pSocket->Abort();

		m_pSocket->Release();

		m_pSocket = NULL;
	}
}

// ILinkTransport

BOOL CG3NetworkClient::IsUsb(void)
{
	return FALSE;
}

BOOL CG3NetworkClient::IsP2P(void)
{
	return FALSE;
}

void CG3NetworkClient::SetAuth(void)
{
	m_fAuth = TRUE;
}

// Frame Processing

UINT CG3NetworkClient::Process(void)
{
	if( m_pService ) {

		if( !(m_fAuth || m_Req.GetService() == servAuth) ) {

			m_Rep.StartFrame(m_Req.GetService(), opNotAuth);

			return procOkay;
		}

		return m_pService->Process(m_Req, m_Rep, this);
	}

	return procError;
}

void CG3NetworkClient::Timeout(void)
{
	if( m_pService ) {

		m_pService->Timeout();
	}
}

void CG3NetworkClient::EndLink(CG3LinkFrame &Req)
{
	if( m_pService ) {

		m_pService->EndLink(Req);
	}
}

// Transport Layer

BOOL CG3NetworkClient::GetFrame(void)
{
	WORD wFrame = 0;

	UINT uSize  = sizeof(wFrame);

	if( m_pSocket->Recv(PBYTE(&wFrame), uSize, 5) == S_OK ) {

		if( uSize == sizeof(wFrame) ) {

			wFrame = MotorToHost(wFrame);

			CAutoArray<BYTE> pData(wFrame);

			UINT uPtr = 0;

			SetTimer(m_Config.m_uTimeout);

			while( CheckSocket() ) {

				UINT uSize = wFrame - uPtr;

				if( m_pSocket->Recv(pData + uPtr, uSize, 5) == S_OK ) {

					if( (uPtr += uSize) == wFrame ) {

						m_bSeq = pData[1];

						m_Req.StartFrame(pData[0], pData[2]);

						m_Req.SetFlags(pData[3]);

						m_Req.AddData(pData + 4, uPtr- 4);

						return TRUE;
					}

					SetTimer(m_Config.m_uTimeout);

					continue;
				}

				if( !GetTimer() ) {

					m_pSocket->Abort();

					break;
				}
			}
		}
	}

	return FALSE;
}

BOOL CG3NetworkClient::PutFrame(void)
{
	CBuffer *pBuff = BuffAllocate(1280);

	UINT     uBuff = 1274;

	if( pBuff ) {

		UINT  uSize = m_Rep.GetDataSize();

		PBYTE pFrom = m_Rep.GetData();

		PBYTE pHead = pBuff->AddTail(6);

		pHead[0] = HIBYTE(uSize + 4);

		pHead[1] = LOBYTE(uSize + 4);

		pHead[2] = m_Rep.GetService();

		pHead[3] = m_bSeq;

		pHead[4] = m_Rep.GetOpcode();

		pHead[5] = 0;

		for( ;;) {

			UINT  uLump = min(uSize, uBuff);

			PBYTE pData = pBuff->AddTail(uLump);

			memcpy(pData, pFrom, uLump);

			pFrom += uLump;

			uSize -= uLump;

			for( ;;) {

				try {
					if( !CheckSocket() ) {

						pBuff->Release();

						return FALSE;
					}

					if( m_pSocket->Send(pBuff) == S_OK ) {

						break;
					}
				}

				catch( CExecCancel const & )
				{
					pBuff->Release();

					throw;
				}

				Sleep(5);
			}

			if( uSize ) {

				if( (pBuff = BuffAllocate(1280)) ) {

					uBuff = 1280;

					continue;
				}

				return FALSE;
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CG3NetworkClient::CheckSocket(void)
{
	if( m_pSocket ) {

		UINT uPhase = 0;

		m_pSocket->GetPhase(uPhase);

		return uPhase == PHASE_OPEN;
	}

	return FALSE;
}

BOOL CG3NetworkClient::CheckAccess(void)
{
	CIpAddr RemAddr;

	if( m_pSocket->GetRemote(RemAddr) == S_OK ) {

		if( (RemAddr & m_Config.m_ReqMask) == (m_Config.m_ReqAddr & m_Config.m_ReqMask) ) {

			return TRUE;
		}
	}

	return FALSE;
}

// End of File
