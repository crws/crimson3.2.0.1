
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_FtpServer_HPP

#define	INCLUDE_FtpServer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class  CJsonConfig;

class  CFtpSession;

struct CFtpConfig;

//////////////////////////////////////////////////////////////////////////
//
// FTP Server
//

class CFtpServer : public IClientProcess
{
public:
	// Constructor
	CFtpServer(CJsonConfig *pJson);

	// Destructor
	~CFtpServer(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Data Members
	ULONG	      m_uRefs;
	BOOL	      m_fEnable;
	CFtpSession * m_pSession[4];
	IThread     * m_pThread [4];
	CFtpConfig  * m_pConfig;

	// Implementation
	void ApplyConfig(CJsonConfig *pJson);
};

// End of File

#endif
