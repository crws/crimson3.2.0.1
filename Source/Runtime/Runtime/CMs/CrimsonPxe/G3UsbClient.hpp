
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3UsbClient_HPP

#define	INCLUDE_G3UsbClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "G3LinkFrame.hpp"

#include "G3Slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

interface ILinkService;

class     CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// G3 Link USB Signatures
//

#define	SIG_G3CB	HostToMotor(DWORD(0x47334342))

#define SIG_G3SB	HostToMotor(DWORD(0x47335342))

//////////////////////////////////////////////////////////////////////////
//
// Structures
//

struct G3CB;
struct G3SB;

//////////////////////////////////////////////////////////////////////////
//
// G3 Link USB Command Block
//

#pragma pack(1)

struct G3CB
{
	DWORD	m_dwSig;
	WORD    m_wTag;
	BYTE	m_bService;
	BYTE	m_bOpcode;
	BYTE    m_bFlags;
	BYTE	m_bDataSize;
	WORD    m_wBulkSize;
	BYTE	m_bData[19];

};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// G3 Link USB Status Block
//

#pragma pack(1)

struct G3SB
{
	DWORD	m_dwSig;
	WORD	m_wTag;
	BYTE	m_bOpcode;
	BYTE	m_bReadBulk;
	WORD	m_wBulkSize;

};

#pragma pack()

////////////////////////////////////////////////////////////////////////
//
// USB Client Transport
//

class CG3UsbClient : public IUsbFuncEvents, public ILinkTransport
{
public:
	// Constructor
	CG3UsbClient(UINT uLevel, ILinkService *pService);

	// Destructor
	~CG3UsbClient(void);

	// Operations
	BOOL Open(CJsonConfig *pJson);
	void Close(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IUsbEvents
	void METHOD OnBind(IUsbDriver *pDriver);
	BOOL METHOD GetDriverInterface(IUsbDriver *&pDriver);
	void METHOD OnInit(void);
	void METHOD OnStart(void);
	void METHOD OnStop(void);

	// IUsbFuncEvents
	void METHOD OnState(UINT uState);
	BOOL METHOD OnSetupClass(UsbDeviceReq &r);
	BOOL METHOD OnSetupVendor(UsbDeviceReq &r);
	BOOL METHOD OnSetupOther(UsbDeviceReq &r);
	BOOL METHOD OnGetDevice(UsbDesc &d);
	BOOL METHOD OnGetQualifier(UsbDesc &d);
	BOOL METHOD OnGetConfig(UsbDesc &d);
	BOOL METHOD OnGetInterface(UsbDesc &d);
	BOOL METHOD OnGetEndpoint(UsbDesc &d);
	BOOL METHOD OnGetString(UsbDesc *&p, UINT iIndex);

	// ILinkTransport
	BOOL IsUsb(void);
	BOOL IsP2P(void);
	void SetAuth(void);

protected:
	// Configuration
	struct CConfig
	{
		// Constructor
		CConfig(void);

		// Operations
		BOOL Parse(CJsonConfig *pJson);
	};

// Data Members
	ULONG                m_uRefs;
	CConfig		     m_Config;
	UINT		     m_uLevel;
	ILinkService       * m_pService;
	UINT                 m_iInterface;
	IUsbFuncCompDriver * m_pDriver;
	IEvent             * m_pEvent;
	IThread            * m_pThread;
	UINT                 m_iEndptSend;
	UINT                 m_iEndptRecv;
	BOOL		     m_fInit;
	WORD		     m_wTag;
	CG3LinkFrame	     m_Req;
	CG3LinkFrame	     m_Rep;
	G3CB		     m_CB;
	BYTE		     m_bPad;
	G3SB		     m_SB;
	BYTE		     m_bRxData[2048];

	// Thread Entry
	void ThreadEntry(void);

	// Implementation
	void WaitHost(void);
	void WaitRunning(void);
	void Shutdown(void);

	// Frame Handlers
	BOOL GetRequest(void);
	BOOL SendReply(void);
	UINT Process(void);
	void Timeout(void);
	void EndLink(CG3LinkFrame &Req);

	// Transport Layer
	BOOL Send(PBYTE pData, UINT uLen, BOOL fLast);
	UINT Recv(PBYTE pData, UINT uLen, UINT uTimeout);

	// Thread Entry
	static int ThreadUsbClient(IThread *pThread, void *pParam, UINT uParam);
};

// End of File

#endif
