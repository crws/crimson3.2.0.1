
#include "Intern.hpp"

#include "PortGrabber.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//								
// Port Grabber
//

// Constructor

CPortGrabber::CPortGrabber(void)
{
	m_fRequest = FALSE;
	
	m_pMutex1  = Create_Mutex();
	
	m_pMutex2  = Create_Mutex();
	}

// Destructor

CPortGrabber::~CPortGrabber(void)
{
	m_pMutex1->Release();

	m_pMutex2->Release();
	}

// IUnknown

HRESULT CPortGrabber::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPortGrabber);

	StdQueryInterface(IPortGrabber);

	return E_NOINTERFACE;
}

ULONG CPortGrabber::AddRef(void)
{
	StdAddRef();
}

ULONG CPortGrabber::Release(void)
{
	StdRelease();
}

// IGrabber

BOOL CPortGrabber::HasRequest(void)
{
	return m_fRequest;
	}

BOOL CPortGrabber::Claim(void)
{
	m_pMutex1->Wait(FOREVER);

	m_fRequest = TRUE;

	m_pMutex2->Wait(FOREVER);

	m_fRequest = FALSE;

	m_pMutex1->Free();
	
	return TRUE;
	}

void CPortGrabber::Free(void)
{
	m_pMutex2->Free();
	}

// End of File
