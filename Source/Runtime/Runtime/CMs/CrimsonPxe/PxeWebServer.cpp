
#include "Intern.hpp"

#include "PxeWebServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "StringTable.hpp"

#include "PxeHttpServer.hpp"

#include "PxeHttpServerSession.hpp"

#include "WebFiles.hpp"

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PXE Web Server
//

// System Commands

CPxeWebServer::CSysCmd CPxeWebServer::m_Cmds[] = {

	{ IDS_SYS_INFO,		0,			true,		""						},
	{ IDS_MEM_SPACE,	IDS_MEM_DSPACE,		true,		"/usr/bin/free"					},
	{ IDS_DSK_SPACE,	IDS_DSK_DSPACE,		true,		"/bin/df"					},
	{ IDS_CPU_USAGE,	IDS_CPU_DUSAGE,		true,		"/usr/bin/top -b -n 1"				},
	{ IDS_NET_INFO,		0,			false,		""						},
	{ IDS_NET_CFG,		IDS_NET_DCFG,		false,		"ip.config"					},
	{ IDS_NET_STS,		IDS_NET_DSTS,		false,		"ip.status"					},
	{ IDS_NET_RTS,		IDS_NET_DRTS,		false,		"ip.routes"					},
	{ IDS_NET_STS1,		IDS_NET_DSTS1,		false,		"eth0.all"					},
	{ IDS_NET_STS2,		IDS_NET_DSTS2,		false,		"eth1.all"					},
	{ IDS_NET_CFG,		IDS_NET_DCFG,		true,		"/sbin/ifconfig"				},
	{ IDS_NET_STS,		IDS_NET_DSTS,		true,		"/sbin/netstat -t -u -a -n"			},
	{ IDS_NET_RTS,		IDS_NET_DRTS,		true,		"/sbin/route -n"				},
	{ IDS_NET_FIRE1,	IDS_NET_DFIRE1,		true,		"/sbin/iptables -t filter --list -v -n -w"	},
	{ IDS_NET_FIRE2,	IDS_NET_DFIRE2,		true,		"/sbin/iptables -t nat --list -v -n -w"		},
	{ IDS_NET_FIRE3,	IDS_NET_DFIRE3,		true,		"/sbin/iptables -t mangle --list -v -n -w"	},
	{ IDS_NET_MACFILT,	IDS_NET_DMACFILT,	true,		"/sbin/arptables --list -v"			},
	{ IDS_NET_ARP,		IDS_NET_DARP,		true,		"/sbin/arp -n"					},
	{ IDS_NET_DHCPD,	IDS_NET_DDHCPD,		true,		"/opt/crimson/scripts/c3-show-leases all"	},
};

// Constructor

CPxeWebServer::CPxeWebServer(ICrimsonPxe *pPxe)
{
	m_pPxe      = pPxe;

	m_pOpts     = New CHttpServerOptions;

	m_pManager  = NULL;

	m_pServer   = NULL;

	m_pFiles    = NULL;

	m_fRedirect = TRUE;

	CheckCmds();

	m_pOpts->m_fTls          = TRUE;

	m_pOpts->m_uPort         = 4443;

	m_pOpts->m_uHttpRedirect = 8080;

	m_pOpts->m_uSockCount    = 20;

	m_pOpts->m_uMaxKeep      = 5;

	AfxGetObject("platform", 0, IPlatform, m_pPlatform);

	AfxGetObject("dev.security", 0, IFeatures, m_pFeatures);

	AfxGetObject("c3.schema-generator", 0, ISchemaGenerator, m_pSchema);

	AfxGetObject("c3.config-storage", 0, IConfigStorage, m_pConfig);

	AfxGetObject("ip", 0, INetUtilities, m_pNetUtils);

	StdSetRef();
}

// Destructor

CPxeWebServer::~CPxeWebServer(void)
{
	delete m_pOpts;

	AfxRelease(m_pSchema);

	AfxRelease(m_pConfig);

	AfxRelease(m_pNetUtils);
}

// IUnknown

HRESULT CPxeWebServer::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IConfigUpdate);

	return E_NOINTERFACE;
}

ULONG CPxeWebServer::AddRef(void)
{
	StdAddRef();
}

ULONG CPxeWebServer::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CPxeWebServer::TaskInit(UINT uTask)
{
	SetThreadName("PxeWeb");

	SetThreadFlags(1, 1);

	m_pPxe->SetSysWebLink(CPrintf(":%u/ajax/jump-config.htm", m_pOpts->m_uPort));

	m_pFiles = New CWebFiles;

	return TRUE;
}

INT CPxeWebServer::TaskExec(UINT uTask)
{
	for( ;;) {

		BOOL fSave = m_fRedirect;

		BOOL fOpen = OpenServer();

		UINT uCert = m_pPxe->GetDefCertStep();

		for( ;;) {

			if( m_pPxe->GetDefCertStep() != uCert ) {

				if( m_pServer->IsIdle() ) {

					break;
				}
			}

			if( fSave != m_fRedirect ) {

				m_pServer->Disconnect(m_pServer->GetSockCount() - 2);

				m_pServer->Disconnect(m_pServer->GetSockCount() - 1);

				m_pServer->EnableRedirect(m_fRedirect);

				fSave = m_fRedirect;
			}

			if( fOpen && !m_pServer->Service() ) {

				Sleep(10);
			}
			else {
				ForceSleep(10);
			}
		}

		CloseServer();
	}

	return 0;
}

void CPxeWebServer::TaskStop(UINT uTask)
{
}

void CPxeWebServer::TaskTerm(UINT uTask)
{
	CloseServer();

	delete m_pFiles;
}

// Redirection

void CPxeWebServer::EnableRedirect(BOOL fEnable)
{
	m_fRedirect = fEnable;
}

// Operations

BOOL CPxeWebServer::ExecuteAjax(CWebReqContext const &Ctx, CString Name)
{
	typedef BOOL(CPxeWebServer::*PFNDISP)(CWebReqContext const &Ctx);

	struct CDispatch
	{
		CString	m_Name;
		PFNDISP m_pfnDisp;
	};

	if( Ctx.pReq->GetVerb() == "GET" ) {

		// TODO -- Add an index and use binary search!!!

		static CDispatch Table[] = {

			{ "jump-config.htm",		&CPxeWebServer::ExecuteJumpConfig	},
			{ "jump-runtime.htm",		&CPxeWebServer::ExecuteJumpRuntime	},
			{ "read-pipe.ajax",		&CPxeWebServer::ExecuteReadPipe		},
			{ "main-status.ajax",		&CPxeWebServer::ExecuteMainStatus	},
			{ "device-status.ajax",		&CPxeWebServer::ExecuteDeviceStatus	},
			{ "pxe-command.ajax",		&CPxeWebServer::ExecutePxeCommand	},
			{ "face-status.ajax",		&CPxeWebServer::ExecuteFaceStatus	},
			{ "cell-status.ajax",		&CPxeWebServer::ExecuteCellStatus	},
			{ "wifi-status.ajax",		&CPxeWebServer::ExecuteWiFiStatus	},
			{ "cell-command.ajax",		&CPxeWebServer::ExecuteCellCommand	},
			{ "wifi-command.ajax",		&CPxeWebServer::ExecuteWiFiCommand	},
			{ "wifi-scan.ajax",		&CPxeWebServer::ExecuteWiFiScan		},
			{ "get-status.ajax",		&CPxeWebServer::ExecuteGetStatus	},
			{ "syscmd-execute.ajax",	&CPxeWebServer::ExecuteSysCommand	},
			{ "sysdebug-update.ajax",	&CPxeWebServer::ExecuteSysDebugUpdate	},
			{ "syspcap-status.ajax",	&CPxeWebServer::ExecuteSysPcapStatus	},
			{ "syspcap-control.ajax",	&CPxeWebServer::ExecuteSysPcapControl	},
			{ "syspcap-read.pcap",		&CPxeWebServer::ExecuteSysPcapRead	},
			{ "sysping.ajax",		&CPxeWebServer::ExecuteSysPing		},
			{ "sysinfo.ajax",		&CPxeWebServer::ExecuteSysInfo		},
			{ "sysread.ajax",		&CPxeWebServer::ExecuteSysRead		},
			{ "logread.ajax",		&CPxeWebServer::ExecuteLogRead		},
			{ "mkpass.ajax",		&CPxeWebServer::ExecuteMakePass		},
			{ "mkcert.ajax",		&CPxeWebServer::ExecuteMakeCert		},
			{ "setpass.htm",		&CPxeWebServer::ExecuteSetPass		},
			{ "get-dump.ajax",		&CPxeWebServer::ExecuteGetDump		},
			{ "clear-dumps.ajax",		&CPxeWebServer::ExecuteClearDumps	},
			{ "systool-start.ajax",		&CPxeWebServer::ExecuteSysToolStart	},
			{ "systool-stop.ajax",		&CPxeWebServer::ExecuteSysToolStop	},
			{ "devread.ajax",		&CPxeWebServer::ExecuteDevRead		},
			{ "restart.ajax",		&CPxeWebServer::ExecuteRestart		},
			{ "tool-autosled.ajax",		&CPxeWebServer::ExecuteToolAutoSled     },
			{ "tool-autoport.ajax",		&CPxeWebServer::ExecuteToolAutoPort     },
		};

		for( UINT n = 0; n < elements(Table); n++ ) {

			if( Name == Table[n].m_Name ) {

				return (this->*Table[n].m_pfnDisp)(Ctx);
			}
		}
	}

	if( Ctx.pReq->GetVerb() == "POST" ) {

		// TODO -- Add an index and use binary search!!!

		static CDispatch Table[] = {

			{ "syswrite.ajax",	&CPxeWebServer::ExecuteSysWrite	},
			{ "sysreset.ajax",	&CPxeWebServer::ExecuteSysReset	},
			{ "decrypt.ajax",	&CPxeWebServer::ExecuteDecrypt  },
			{ "keywrite.ajax",	&CPxeWebServer::ExecuteKeyWrite	},
			{ "putimage.ajax",	&CPxeWebServer::ExecutePutImage },

		};

		for( UINT n = 0; n < elements(Table); n++ ) {

			if( Name == Table[n].m_Name ) {

				return (this->*Table[n].m_pfnDisp)(Ctx);
			}
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::ReplyWithPxeFile(CWebReqContext const &Ctx, CString Name)
{
	CWebFileInfo Info;

	if( m_pFiles->FindFile(Info, Name) ) {

		// TODO -- Handle cache management better...

		PCTXT   pType     = strrchr(Info.p->m_pName, '.') + 1;

		BOOL	fHtml     = !strcasecmp(pType, "htm");

		BOOL	fJson     = !strcasecmp(pType, "json");

		UINT    uAccess   = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

		CString User      = Ctx.pSess->GetReal() + CPrintf(":%u", uAccess);

		DWORD   dwUser    = crc(PCBYTE(PCTXT(User)), User.GetLength());

		CPrintf CacheTag  = CPrintf("SFile:%8.8x:%8.8x:%8.8x", Info.p, Info.t, dwUser);

		time_t  timeCreated = Info.t;

		time_t  timeExpires = time(NULL) + 10 * 60 * 60;

		BOOL	fCache      = TRUE;

		if( fHtml || fJson ) {

			CacheTag.AppendPrintf(":%8.8x", 12345678/*!!!*/);

			timeCreated = Info.t;

			timeExpires = 0;
		}

		if( !Ctx.pReq->IsUnchanged(timeCreated, CacheTag) ) {

			Ctx.pReq->SetContentType(m_pManager->GetMimeType(pType));

			if( fHtml ) {

				CString Text(PCTXT(Info.p->m_pData), Info.p->m_uSize);

				ExpandElements(Ctx, Text, fCache);

				Ctx.pReq->AddReplyHeader("X-Frame-Options", "SAMEORIGIN");

				Ctx.pReq->SetReplyBody(Text);
			}
			else
				Ctx.pReq->SetReplyBody(Info.p->m_pData, Info.p->m_uSize);

			if( fCache ) {

				Ctx.pReq->AddCacheInfo(timeCreated, timeExpires, CacheTag);
			}

			Ctx.pReq->SetStatus(200);

			return TRUE;
		}

		Ctx.pReq->AddCacheInfo(timeCreated, timeExpires, CacheTag);

		Ctx.pReq->SetStatus(304);

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::ReplyWithDevFile(CWebReqContext const &Ctx, CString Name, BOOL fExpire)
{
	CAutoFile File(Name, "r");

	if( File ) {

		time_t timeCreated = File.GetUnix();

		time_t timeExpires = fExpire ? time(NULL) + 8*60*60 : 0;

		DWORD  dwUser      = crc(PCBYTE(PCTXT(Name)), Name.GetLength());

		CPrintf CacheTag("CFile:%8.8x:%8.8x", dwUser, timeCreated);

		if( !Ctx.pReq->IsUnchanged(timeCreated, CacheTag) ) {

			// We use a CString object to hold the buffer
			// even when the data is binary as it avoids
			// our having to copy it an extra time.

			UINT    uSize = File.GetSize();

			CString Text;

			Text.Expand(uSize+1);

			PBYTE  pData = PBYTE(PCTXT(Text));

			if( File.Read(pData, uSize) == uSize ) {

				File.Close();

				BOOL    fCache = TRUE;

				CString Type   = CFilename(Name).GetType();

				CString Mime   = m_pManager->GetMimeType(Type);

				Ctx.pReq->SetReplyBody(pData, uSize);

				if( fCache ) {

					Ctx.pReq->AddCacheInfo(timeCreated, timeExpires, CacheTag);
				}

				Ctx.pReq->SetContentType(Mime);

				Ctx.pReq->SetStatus(200);

				return TRUE;
			}
		}
		else {
			File.Close();

			Ctx.pReq->AddCacheInfo(timeCreated, timeExpires, CacheTag);

			Ctx.pReq->SetStatus(304);

			return TRUE;
		}
	}

	return FALSE;
}

// Server Management

BOOL CPxeWebServer::OpenServer(void)
{
	m_pManager = New CHttpServerManager;

	m_pManager->AddExtendedTypes();

	if( m_pManager->Open() ) {

		if( m_pOpts->m_fTls ) {

			if( m_pPxe->GetDefCertStep() ) {

				CByteArray Data[2];

				CString    Pass;

				if( m_pPxe->GetDefCertData(Data, Pass) ) {

					m_pManager->LoadServerCert(Data[0].data(),
								   Data[0].size(),
								   Data[1].data(),
								   Data[1].size(),
								   Pass
					);
				}
			}
		}

		m_pServer = New CPxeHttpServer(this, m_pManager, *m_pOpts);

		m_pServer->EnableRedirect(m_fRedirect);

		return TRUE;
	}

	delete m_pManager;

	m_pManager = NULL;

	return FALSE;
}

BOOL CPxeWebServer::CloseServer(void)
{
	if( m_pManager ) {

		m_pServer->Term();

		delete m_pServer;

		delete m_pManager;

		return TRUE;
	}

	return FALSE;
}

// Element Expansion

BOOL CPxeWebServer::ExpandElements(CWebReqContext const &Ctx, CString &Text, BOOL &fCache)
{
	UINT p0 = 0;

	for( ;;) {

		UINT p1 = Text.Find("<%", p0);

		if( p1 < NOTHING ) {

			UINT p2 = Text.Find("%>", p1);

			if( p2 < NOTHING ) {

				CString Name = Text.Mid(p1 + 2, p2 - p1 - 2);

				CString Data = GetElementData(Ctx, Name, fCache);

				Text = Text.Left(p1) + Data + Text.Mid(p2+2);

				p0   = p1 + Data.GetLength();
			}

			continue;
		}

		break;
	}

	return TRUE;
}

CString CPxeWebServer::GetElementData(CWebReqContext const &Ctx, CString Name, BOOL &fCache)
{
	if( Name[0] == '=' ) {

		CString Type = Name.StripToken('.');

		if( Type == "=String" ) {

			return GetWebString(atoi(Name));
		}

		if( Type == "=Global" ) {

			return ExpandGlobal(Ctx, Name);
		}

		if( Type == "=Config" ) {

			return ExpandConfig(Ctx, Name);
		}

		if( Type == "=System" ) {

			fCache = FALSE;

			return ExpandSystem(Ctx, Name);
		}

		return CPrintf("!!Unknown Variable %s.%s!!",
			       PCTXT(Type)+1,
			       PCTXT(Name)
		);
	}
	else {
		CString Cmd = Name.StripToken(' ');

		if( Cmd == "Include" ) {

			CWebFileInfo Info;

			if( m_pFiles->FindFile(Info, Name) ) {

				CString Data(PCSTR(Info.p->m_pData), Info.p->m_uSize);

				ExpandElements(Ctx, Data, fCache);

				return Data;
			}

			return CPrintf("!!Cannot Include %s!!",
				       PCTXT(Name)
			);
		}

		if( Cmd == "NoCache" ) {

			fCache = FALSE;

			return "";
		}

		return CPrintf("!!Unknown Command %s!!",
			       PCTXT(Cmd)
		);
	}
}

// Page Handlers

CString CPxeWebServer::ExpandGlobal(CWebReqContext const &Ctx, CString Name)
{
	CString d;

	if( Name == "Home" ) {

		return "<img src='/assets/images/logo.png' style='max-height: 22px; height: auto; width: auto'/>";
	}

	if( Name == "Title" ) {

		return GetWebString(IDS_ENH_WEB_SERVER);
	}

	if( Name == "PageHeader" ) {

		return GetWebString(IDS_WELCOME);
	}

	if( Name == "MainNavBar" ) {

		d +=	"<ul class='nav navbar-nav'>"
			"<span id='c3-navbar-head'></span>";

		AddListItem(d, Ctx.pReq, "/default.htm", GetWebString(IDS_HOME));

		d += "<span id='c3-navbar-home'></span>";

		if( true ) {

			if( Ctx.pReq->GetPath() == "/config.htm" ) {

				d += "<li class='dropdown active'>";
			}
			else
				d += "<li class='dropdown'>";

			d += "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>";

			d += "Configuration";

			d += "<span class='caret'></span></a><ul class='dropdown-menu'>";

			d += ExpandConfig(Ctx, "Dropdown");

			d += "</ul></li>";
		}

		if( true ) {

			if( Ctx.pReq->GetPath() == "/system.htm" ) {

				d += "<li class='dropdown active'>";
			}
			else
				d += "<li class='dropdown'>";

			d += "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>";

			d += "Diagnostics";

			d += "<span class='caret'></span></a><ul class='dropdown-menu'>";

			d += ExpandSystem(Ctx, "Dropdown");

			d += "</ul></li>";
		}

		d += "<span id='c3-navbar-tail'></span>";

		d += "</ul><ul class='nav navbar-nav navbar-right'>";

		if( true ) {

			d += "<li id='wifi-item'><p class='navbar-text'>";

			d += "<canvas id='wifi-canvas' width='24px' height='16px' style='background: transparent'></canvas>";

			d += "</p></li>";
		}

		if( true ) {

			d += "<li id='bars-item'><p class='navbar-text'>";

			d += "<canvas id='bars-canvas' width='24px' height='16px' style='background: transparent'></canvas>";

			d += "</p></li>";
		}

		if( true ) {

			d += "<li id='map-item'><p class='navbar-text'>";

			d += "<span id='map-pin' class='glyphicon glyphicon-map-marker pin-disable'></span>";

			d += "</p></li>";
		}

		if( true ) {

			d += "<li><a id='sys-info' href='devinfo.htm'>";

			d += CString(m_pPlatform->GetModel()).ToUpper();

			#if defined(AEON_PLAT_WIN32)

			d += "&nbsp;Emulator";

			#else

			d += "&nbsp;";

			d += CString(m_pPlatform->GetSerial()).ToUpper();

			#endif

			d += "</a></li>";
		}

		if( true ) {

			UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

			BOOL fLocal  = ((CPxeHttpServerSession *) Ctx.pSess)->IsLocalUser();

			d +=	"<li class='dropdown'>"
				"<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>";

			PCSTR pIcon = fLocal ? "fa-user" : "fa-user-circle";

			DWORD dwCol = 0;

			switch( uAccess ) {

				case 0: dwCol = 0xD00000; break;
				case 1: dwCol = 0xD0B000; break;
				case 2: dwCol = 0xD0D000; break;
				case 3: dwCol = 0x00C000; break;
			}

			d.AppendPrintf("<span id='access-level' style='display: none'>%u</span>", uAccess);

			d.AppendPrintf("<span class='fa %s' style='color: #%6.6X'></span>", pIcon, dwCol);

			d += "&nbsp;&nbsp;";

			d += Ctx.pSess->GetReal();

			d += "<span class='caret'></span></a><ul class='dropdown-menu'>";

			d += "<li><a href='/logoff.htm'>";

			d += GetWebString(IDS_LOG_OFF);

			d += "</a></li>";

			if( Ctx.pSess->IsLocalUser() ) {

				d += "<li><a href='/setpass.htm'>";

				d += "Change Password";

				d += "</a></li>";
			}

			if( uAccess <= 0 ) {

				d += "<li><a href='/editor.htm?m=u'>";

				d += "Edit Security";

				d += "</a></li>";
			}

			d += "</ul></li>";
		}

		if( !m_pFeatures || m_pFeatures->GetEnabledGroup() >= SW_GROUP_3A ) {

			d += "<li>";

			d += "<a href='/ajax/jump-runtime.htm'>";

			d += "<span class='glyphicon glyphicon-play'></span>&nbsp;";

			d += "Runtime";

			d += "</a></li>";
		}

		d += "</ul>";

		return d;
	}

	AfxTrace("Unknown Element Global.%s\n", PCTXT(Name));

	return Name;
}

CString CPxeWebServer::ExpandSystem(CWebReqContext const &Ctx, CString Name)
{
	CString d;

	if( Name == "Dropdown" ) {

		UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

		if( uAccess <= 3 ) {

			UINT uMode = (uAccess == 0) ? 2 : 1;

			AddListItem(d, Ctx.pReq, CPrintf("/sysdebug.htm?m=%u", uMode), GetWebString(IDS_DBG_CONSOLE));
		}

		if( uAccess <= 1 ) {

			AddListItem(d, Ctx.pReq, "/syslog.htm", GetWebString(IDS_SYSTEM_LOG));
		}

		if( uAccess <= 0 ) {

			AddListItem(d, Ctx.pReq, "/syspcap.htm", GetWebString(IDS_PCAP_NAME));
		}

		if( true ) {

			d += "<li class='divider'></li>";
		}

		if( true ) {

			BOOL fFirst = TRUE;

			for( int n = 0; n < elements(m_Cmds); n++ ) {

				CSysCmd const &Cmd = m_Cmds[n];

				if( Cmd.fEnable ) {

					if( !Cmd.uDesc ) {

						if( !fFirst ) {

							EndSubMenu(d);
						}

						AddSubMenu(d, GetWebString(Cmd.uName));

						fFirst = FALSE;

						continue;
					}

					AddListItem(d, Ctx.pReq, CPrintf("/syscmd.htm?cmd=%u", n), GetWebString(Cmd.uName));
				}
			}

			EndSubMenu(d);
		}

		if( true ) {

			AddSubMenu(d, "Network Tools");

			AddListItem(d, Ctx.pReq, "/systool.htm?cmd=ping", "ICMP Ping");

			AddListItem(d, Ctx.pReq, "/systool.htm?cmd=arping", "ARP Ping");

			AddListItem(d, Ctx.pReq, "/systool.htm?cmd=tracert", "Trace Route");

			AddListItem(d, Ctx.pReq, "/systool.htm?cmd=getroute", "Get Route");

			AddListItem(d, Ctx.pReq, "/systool.htm?cmd=tcptest", "Socket Test");

			AddListItem(d, Ctx.pReq, "/systool.htm?cmd=nslookup", "Name Lookup");

			EndSubMenu(d);
		}

		if( uAccess <= 3 ) {

			AddSubMenu(d, "Sled Status");

			AddListItem(d, Ctx.pReq, "/cellinfo.htm", "Cell Modem Status");

			AddListItem(d, Ctx.pReq, "/wifiinfo.htm", "Wi-Fi Sled Status");

			EndSubMenu(d);
		}

		return d;
	}

	if( Name == "Menu" ) {

		UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

		if( uAccess <= 3 ) {

			UINT uMode = (uAccess == 0) ? 2 : 1;

			d.AppendPrintf("<tr><td><a href='/sysdebug.htm?m=%u'>%s</a></td><td>%s<td></tr>\n",
				       uMode,
				       GetWebString(IDS_DBG_CONSOLE),
				       GetWebString(IDS_DBG_INTERACT)
			);
		}

		if( uAccess <= 1 ) {

			d.AppendPrintf("<tr><td><a href='/syslog.htm'>%s</a></td><td>%s<td></tr>\n",
				       GetWebString(IDS_SYSTEM_LOG),
				       "Show and track the contents of the Linux system log."
			);
		}

		if( uAccess <= 0 ) {

			d.AppendPrintf("<tr><td><a href='/syspcap.htm'>%s</a></td><td>%s<td></tr>\n",
				       GetWebString(IDS_PCAP_NAME),
				       GetWebString(IDS_PCAP_DESC)
			);
		}

		if( uAccess <= 3 ) {

			d.AppendPrintf("<tr><td><a href='/cellinfo.htm'>%s</a></td><td>%s<td></tr>\n",
				       "Cell Modem Status",
				       "Show the status of the cellular modem."
			);

			d.AppendPrintf("<tr><td><a href='/wifiinfo.htm'>%s</a></td><td>%s<td></tr>\n",
				       "Wi-Fi Sled Status",
				       "Show the status of the Wi-Fi sled."
			);
		}

		for( int n = 0; n < elements(m_Cmds); n++ ) {

			CSysCmd const &Cmd = m_Cmds[n];

			if( Cmd.fEnable && Cmd.uDesc ) {

				d.AppendPrintf("<tr><td><a href='/syscmd.htm?cmd=%u'>%s</a></td><td>%s<td></tr>\n",
					       n,
					       GetWebString(Cmd.uName),
					       GetWebString(Cmd.uDesc)
				);
			}
		}

		CString t;

		MakeOptionTable(t, NULL, d);

		return t;
	}

	if( Name == "CmdHead" ) {

		UINT n = Ctx.pReq->GetParamDecimal("Cmd", NOTHING);

		if( n < elements(m_Cmds) ) {

			return GetWebString(m_Cmds[n].uName);
		}

		return "Invalid Command";
	}

	if( Name == "CaptureList" ) {

		CString t;

		UINT    c = m_pNetUtils->GetInterfaceCount();

		for( UINT n = 1; n < c; n++ ) {

			CString Desc;

			if( m_pNetUtils->GetInterfaceDesc(n, Desc) ) {

				t.AppendPrintf("<option value='%u'>%s</option>", n, PCTXT(Desc));
			}
		}

		return t;
	}

	AfxTrace("Unknown Element System.%s\n", PCTXT(Name));

	return Name;
}

CString CPxeWebServer::ExpandConfig(CWebReqContext const &Ctx, CString Name)
{
	CString d;

	if( Name == "Dropdown" ) {

		UINT    uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

		CString LockCfg = (uAccess >= 2) ? "&r=1" : "";

		CString LockPer = (uAccess >= 3) ? "&r=1" : "";

		AddListItem(d, Ctx.pReq, "/editor.htm?m=h" + LockCfg, "Hardware");

		AddListItem(d, Ctx.pReq, "/editor.htm?m=s" + LockCfg, "System");

		AddListItem(d, Ctx.pReq, "/editor.htm?m=p" + LockPer, "Personality");

		AddListItem(d, Ctx.pReq, "/cutils.htm", "Utilities");

		return d;
	}

	if( Name == "Menu" ) {

		UINT    uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

		CString LockCfg = (uAccess >= 2) ? "&r=1" : "";

		CString LockPer = (uAccess >= 3) ? "&r=1" : "";

		d.AppendPrintf("<tr><td><a href='/editor.htm?m=h%s'>%s</a></td><td>%s<td></tr>\n",
			       PCTXT(LockCfg),
			       "Hardware Configuration",
			       "View or edit the hardware options associated with this device."
		);

		d.AppendPrintf("<tr><td><a href='/editor.htm?m=s%s'>%s</a></td><td>%s<td></tr>\n",
			       PCTXT(LockCfg),
			       "System Configuration",
			       "View or edit the network and system settings associated with this device."
		);

		d.AppendPrintf("<tr><td><a href='/editor.htm?m=p%s'>%s</a></td><td>%s<td></tr>\n",
			       PCTXT(LockPer),
			       "Device Personality",
			       "View or edit the personality settings used to customize this device."
		);

		d.AppendPrintf("<tr><td><a href='/cutils.htm'>%s</a></td><td>%s<td></tr>\n",
			       "Configuration Utilities",
			       "Export, import or reset the configuration of this device."
		);

		CString t;

		MakeOptionTable(t, NULL, d);

		return t;
	}

	AfxTrace("Unknown Element System.%s\n", PCTXT(Name));

	return Name;
}

// Ajax Handlers

BOOL CPxeWebServer::ExecuteSysCommand(CWebReqContext const &Ctx)
{
	UINT    uCmd = Ctx.pReq->GetParamDecimal("Cmd", NOTHING);

	CPrintf Poll = CPrintf("cmd-%u", uCmd);

	CString Data;

	if( RunCommand(Data, uCmd) ) {

		if( Ctx.pSess->SlowPoll(Poll, Data, Ctx.pReq) ) {

			Ctx.pReq->SetStatus(999);

			return TRUE;
		}

		Ctx.pReq->SetReplyBody(Data);

		Ctx.pReq->SetStatus(200);

		Ctx.pReq->SetContentType("text/html");

		return TRUE;
	}

	Ctx.pReq->SetStatus(404);

	return TRUE;
}

BOOL CPxeWebServer::ExecuteSysDebugUpdate(CWebReqContext const &Ctx)
{
	UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

	if( uAccess <= 2 ) {

		BOOL    fInit = Ctx.pReq->GetParamDecimal("init", 0);

		CString Text;

		if( fInit ) {

			Ctx.pSess->m_Console.Enable(TRUE);

			Ctx.pSess->m_Console.Read(Text);

			Ctx.pSess->m_Console.Exec("auto-hello");
		}
		else {
			if( uAccess == 0 ) {

				CString Cmd = Ctx.pReq->GetParamString("cmd", "");

				if( !Cmd.IsEmpty() ) {

					if( Cmd == "help" ) {

						Cmd = "diag.help";
					}

					Ctx.pSess->m_Console.Exec(Cmd);
				}
			}
		}

		Ctx.pSess->m_Console.Read(Text);

		Ctx.pReq->SetReplyBody(Text);

		Ctx.pReq->SetStatus(200);

		Ctx.pReq->SetContentType("text/html");

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteSysPcapStatus(CWebReqContext const &Ctx)
{
	UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

	if( uAccess <= 0 ) {

		UINT uCap = Ctx.pReq->GetParamDecimal("src", 0);

		AfxGetAutoObject(pCap, "net.pcap", uCap, IPacketCapture);

		if( pCap ) {

			CPrintf Text("1,%s,%u,%u",
				     pCap->GetCaptureFilter(),
				     pCap->IsCaptureRunning(),
				     pCap->GetCaptureSize()
			);

			Ctx.pReq->SetReplyBody(Text);
		}
		else
			Ctx.pReq->SetReplyBody("0");

		Ctx.pReq->SetStatus(200);

		Ctx.pReq->SetContentType("text/html");

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteSysPcapControl(CWebReqContext const &Ctx)
{
	UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

	if( uAccess <= 0 ) {

		UINT uCap = Ctx.pReq->GetParamDecimal("src", 0);

		AfxGetAutoObject(pCap, "net.pcap", uCap, IPacketCapture);

		if( pCap ) {

			CString Cmd = Ctx.pReq->GetParamString("action", "none");

			if( Cmd == "start" ) {

				CString Filter = Ctx.pReq->GetParamString("filter", "none");

				AfxTrace("Filter is [%s]\n", PCTXT(Filter));

				pCap->StartCapture(Filter);
			}

			if( Cmd == "stop" ) {

				pCap->StopCapture();
			}

			CPrintf Text("1,%s,%u,%u",
				     pCap->GetCaptureFilter(),
				     pCap->IsCaptureRunning(),
				     pCap->GetCaptureSize()
			);

			Ctx.pReq->SetReplyBody(Text);
		}
		else
			Ctx.pReq->SetReplyBody("0");

		Ctx.pReq->SetStatus(200);

		Ctx.pReq->SetContentType("text/html");

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteSysPcapRead(CWebReqContext const &Ctx)
{
	UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

	if( uAccess <= 0 ) {

		UINT uCap = Ctx.pReq->GetParamDecimal("src", 0);

		AfxGetAutoObject(pCap, "net.pcap", uCap, IPacketCapture);

		if( pCap ) {

			UINT uSize = pCap->GetCaptureSize();

			if( uSize ) {

				CByteArray Data;

				Data.SetCount(uSize);

				pCap->CopyCapture(PBYTE(Data.GetPointer()), uSize);

				Ctx.pReq->SetReplyBody(Data);

				Ctx.pReq->SetStatus(200);

				Ctx.pReq->SetContentType("application/vnd.tcpdump.pcap");

				return TRUE;
			}
		}

		Ctx.pReq->SetStatus(404);

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteJumpConfig(CWebReqContext const &Ctx)
{
	Ctx.pCon->ClearKeepAlive();

	Ctx.pReq->AddReplyHeader("Location", "/default.htm");

	Ctx.pReq->SetStatus(302);

	return TRUE;
}

BOOL CPxeWebServer::ExecuteJumpRuntime(CWebReqContext const &Ctx)
{
	Ctx.pCon->ClearKeepAlive();

	CString Link;

	if( m_pPxe->GetAppWebLink(Link) ) {

		CString From = Ctx.pReq->GetRequestHeader("Referrer");

		CString Full = Ctx.pReq->GetRequestHeader("Host");

		CString Host = Full.Left(Full.FindRev(':'));

		UINT    uPos = Link.Find(':');

		CString Prot = Link.Left(uPos);

		Ctx.pReq->AddReplyHeader("Location", Prot + "://" + Host + Link.Mid(uPos));

		Ctx.pReq->SetStatus(302);

		return TRUE;
	}

	Ctx.pReq->AddReplyHeader("Location", "/nojump.htm");

	Ctx.pReq->SetStatus(302);

	return TRUE;
}

BOOL CPxeWebServer::ExecuteReadPipe(CWebReqContext const &Ctx)
{
	CString Name;

	CString Type = Ctx.pReq->GetParamString("type", "");

	if( Type == "cell" ) {

		GetCellPipeName(Name, Ctx.pReq->GetParamDecimal("id", 0));
	}

	if( Type == "wifi" ) {

		GetWiFiPipeName(Name, Ctx.pReq->GetParamDecimal("id", 0));
	}

	if( Type == "syslog" ) {

		Name = "/./tmp/crimson/syslog/syslog.pipe";
	}

	if( Type == "tool" ) {

		CString t = Ctx.pReq->GetParamString("tab", "tab");

		Name.Printf("/./tmp/crimson/init/tool-%s.pipe", PCSTR(t));
	}

	if( !Name.IsEmpty() ) {

		CString Data;

		if( Ctx.pSess->ReadPipe(Data, Name, Ctx.pReq) ) {

			Ctx.pReq->SetStatus(999);

			return TRUE;
		}

		if( Data == "\x01A" ) {

			Ctx.pReq->SetStatus(404);

			return TRUE;
		}

		Ctx.pReq->SetReplyBody(Data);

		Ctx.pReq->SetStatus(200);

		Ctx.pReq->SetContentType("plain/text");

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteMainStatus(CWebReqContext const &Ctx)
{
	CString Data = "{";

	CString Poll = "main-status";

	AppendFaceStatus(Data, "all");

	AppendDeviceInfo(Data, FALSE);

	Data.Append("\"pxe\":{");

	AppendOkayAndClose(Data, !!AppendPxeStatus(Data, TRUE));

	Data.Append(',');

	AppendJson(Data, "defer", true);

	AppendOkayAndClose(Data, true);

	if( Ctx.pSess->SlowPoll(Poll, Data, Ctx.pReq) ) {

		Ctx.pReq->SetStatus(999);

		return TRUE;
	}

	Ctx.pReq->SetReplyBody(Data);

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("application/json");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteDeviceStatus(CWebReqContext const &Ctx)
{
	CString Data = "{";

	CString Poll = "device-status";

	AppendDeviceInfo(Data, TRUE);

	AppendCoreDumps(Data);

	AppendJson(Data, "defer", true);

	AppendOkayAndClose(Data, true);

	if( Ctx.pSess->SlowPoll(Poll, Data, Ctx.pReq) ) {

		Ctx.pReq->SetStatus(999);

		return TRUE;
	}

	Ctx.pReq->SetReplyBody(Data);

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("application/json");

	return TRUE;
}

BOOL CPxeWebServer::ExecutePxeCommand(CWebReqContext const &Ctx)
{
	bool okay    = false;

	UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

	if( uAccess <= 1 ) {

		CString Cmd = Ctx.pReq->GetParamString("command", "nop");

		if( Cmd == "start" ) {

			m_pPxe->SystemStart();
		}

		if( Cmd == "stop" ) {

			m_pPxe->SystemStop();
		}
	}

	Ctx.pReq->SetReplyBody(okay ? "OK" : "FAIL");

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("text/plain");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteFaceStatus(CWebReqContext const &Ctx)
{
	CString Data = "{";

	CString Find = Ctx.pReq->GetParamString("face", "all");

	CString Poll = "face-status-" + Find;

	AppendFaceStatus(Data, Find);

	AppendJson(Data, "defer", true);

	AppendOkayAndClose(Data, true);

	if( Ctx.pSess->SlowPoll(Poll, Data, Ctx.pReq) ) {

		Ctx.pReq->SetStatus(999);

		return TRUE;
	}

	Ctx.pReq->SetReplyBody(Data);

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("application/json");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteCellStatus(CWebReqContext const &Ctx)
{
	CString Data = "{";

	UINT    cell = Ctx.pReq->GetParamDecimal("id", 0);

	CPrintf Poll = CPrintf("cell-status-%u", cell);

	bool    okay = !!AppendCellStatus(Data, cell, TRUE);

	AppendJson(Data, "defer", true);

	AppendOkayAndClose(Data, okay);

	if( Ctx.pSess->SlowPoll(Poll, Data, Ctx.pReq) ) {

		Ctx.pReq->SetStatus(999);

		return TRUE;
	}

	Ctx.pReq->SetReplyBody(Data);

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("application/json");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteWiFiStatus(CWebReqContext const &Ctx)
{
	CString Data = "{";

	UINT    wifi = Ctx.pReq->GetParamDecimal("id", 0);

	CPrintf Poll = CPrintf("wifi-status-%u", wifi);

	bool	okay = !!AppendWiFiStatus(Data, wifi, TRUE);

	AppendJson(Data, "defer", true);

	AppendOkayAndClose(Data, okay);

	if( Ctx.pSess->SlowPoll(Poll, Data, Ctx.pReq) ) {

		Ctx.pReq->SetStatus(999);

		return TRUE;
	}

	Ctx.pReq->SetReplyBody(Data);

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("application/json");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteCellCommand(CWebReqContext const &Ctx)
{
	bool okay    = false;

	UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

	if( uAccess <= 1 ) {

		AfxGetAutoObject(pCell, "net.cell", Ctx.pReq->GetParamDecimal("id", 0), ICellStatus);

		if( pCell ) {

			if( pCell->SendCommand(Ctx.pReq->GetParamString("command", "nop")) ) {

				okay = true;
			}
		}
	}

	Ctx.pReq->SetReplyBody(okay ? "OK" : "FAIL");

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("text/plain");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteWiFiCommand(CWebReqContext const &Ctx)
{
	bool okay    = false;

	UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

	if( uAccess <= 1 ) {

		AfxGetAutoObject(pWiFi, "net.wifi", Ctx.pReq->GetParamDecimal("id", 0), IWiFiStatus);

		if( pWiFi ) {

			if( pWiFi->SendCommand(Ctx.pReq->GetParamString("command", "nop")) ) {

				okay = true;
			}
		}
	}

	Ctx.pReq->SetReplyBody(okay ? "OK" : "FAIL");

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("text/plain");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteWiFiScan(CWebReqContext const &Ctx)
{
	CString Data = "{";

	UINT    wifi = Ctx.pReq->GetParamDecimal("id", 0);

	bool    okay = false;

	AfxGetAutoObject(pWiFi, "net.wifi", wifi, IWiFiStatus);

	if( pWiFi ) {

		CArray<CWiFiNetworkInfo> List;

		if( pWiFi->ScanNetworks(List) ) {

			Data += "\"scan\":[";

			for( UINT n = 0; n < List.GetCount(); n++ ) {

				Data += n ? ",{" : "{";

				AppendJson(Data, "ssid", List[n].m_Network);

				AppendJson(Data, "bars", ToBars(List[n].m_uSignal));

				AppendOkayAndClose(Data, true);
			}

			Data += "],";

			okay = true;
		}
	}

	AppendOkayAndClose(Data, okay);

	Ctx.pReq->SetReplyBody(Data);

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("application/json");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteGetStatus(CWebReqContext const &Ctx)
{
	CString Poll = "get-status";

	CString Data = "{";

	Data.Append("\"pxe\":{");

	AppendOkayAndClose(Data, !!AppendPxeStatus(Data, FALSE));

	Data.Append(",\"gps\":{");

	AppendOkayAndClose(Data, !!AppendGpsStatus(Data, FALSE));

	Data.Append(",\"cell\":{");

	AppendOkayAndClose(Data, !!AppendCellStatus(Data, 0, FALSE));

	Data.Append(",\"wifi\":{");

	AppendOkayAndClose(Data, !!AppendWiFiStatus(Data, 0, FALSE));

	Data.Append(",");

	AppendJson(Data, "defer", true);

	AppendOkayAndClose(Data, true);

	if( Ctx.pSess->SlowPoll(Poll, Data, Ctx.pReq) ) {

		Ctx.pReq->SetStatus(999);

		return TRUE;
	}

	Ctx.pReq->SetReplyBody(Data);

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("application/json");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteSysPing(CWebReqContext const &Ctx)
{
	Ctx.pReq->SetReplyBody("OK");

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("text/plain");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteSysInfo(CWebReqContext const &Ctx)
{
	CString Text;

	CString t = Ctx.pReq->GetParamString("t", "");

	AfxGetAutoObject(pApp, "c3.net-applicator", 0, INetApplicator);

	if( pApp ) {

		if( pApp->GetInfo(Text, t) ) {

			Ctx.pReq->SetReplyBody(Text);

			Ctx.pReq->SetStatus(200);

			Ctx.pReq->SetContentType("text/plain");

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteSysRead(CWebReqContext const &Ctx)
{
	CString Text;

	CString f = Ctx.pReq->GetParamString("f", "");

	CString t = Ctx.pReq->GetParamString("t", "");

	if( f == "schema" ) {

		m_pSchema->GetSchema(t[0], Text);
	}

	if( f == "config" ) {

		CString File;

		UINT    uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

		if( t[0] == 'd' ) {

			if( uAccess <= 3 ) {

				CString i = Ctx.pReq->GetParamString("i", "");

				PCTXT tag = "hspu";

				for( UINT n = 0; n < 4; n++ ) {

					char cTag = tag[n];

					if( i.IsEmpty() || i.Find(cTag) < NOTHING ) {

						if( tag[n] != 'u' || uAccess == 0 ) {

							CString Part;

							m_pConfig->GetConfig(tag[n], Part);

							Text += Text.IsEmpty() ? "{" : ",";

							Text.AppendPrintf("\"%cconfig\":", cTag);

							Text += Part;
						}
					}
				}
			}

			Text += "}";

			File = "devcon";
		}
		else {
			if( (uAccess == 0 || t[0] != 'u') && uAccess <= 3 ) {

				m_pConfig->GetConfig(t[0], Text);

				m_pSchema->GetNaming(t[0], File);
			}
		}

		if( Text ) {

			CString Unit;

			m_pPxe->GetUnitName(Unit);

			CPrintf Full("%s-%s.json", PCTXT(Unit), PCTXT(File));

			Full.MakeLower();

			Ctx.pReq->AddReplyHeader("Content-Disposition", "attachment; filename=\"" + Full + "\"");
		}
	}

	if( Text ) {

		Ctx.pReq->SetReplyBody(Text);

		Ctx.pReq->SetStatus(200);

		Ctx.pReq->SetContentType("application/json");

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteLogRead(CWebReqContext const &Ctx)
{
	CAutoFile File("/./var/log/messages", "r");

	if( File ) {

		CString Text;

		UINT    uSize = File.GetSize();

		Text.Expand(uSize+1);

		PBYTE  pData = PBYTE(PCTXT(Text));

		if( File.Read(pData, uSize) == uSize ) {

			File.Close();

			Text.FixLength(uSize);

			Ctx.pReq->SetReplyBody(Text);

			Ctx.pReq->AddReplyHeader("Content-Disposition", "attachment; filename=\"syslog.txt\"");

			Ctx.pReq->SetContentType("text/plain");

			Ctx.pReq->SetStatus(200);

			return TRUE;
		}
	}

	Ctx.pReq->SetStatus(404);

	return TRUE;
}

BOOL CPxeWebServer::ExecuteMakePass(CWebReqContext const &Ctx)
{
	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		if( !pLinux->CallProcess("/opt/crimson/bin/MakePassword", "", "/tmp/passwd", NULL) ) {

			CAutoFile File("/./tmp/passwd", "r");

			Ctx.pReq->SetReplyBody(File.GetLine());

			Ctx.pReq->SetStatus(200);

			Ctx.pReq->SetContentType("text/plain");

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteMakeCert(CWebReqContext const &Ctx)
{
	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		CString Unit;

		m_pPxe->GetUnitName(Unit);

		UINT    uSeq = UINT((time(NULL) / 10) % 1000000);

		CString Type(Ctx.pReq->GetParamString("type", "x509"));

		CString Pref(Type.StartsWith("x509") ? "Cert" : "Secret");

		CPrintf Name("%s-%s-%6.6u", PCTXT(Pref), PCTXT(Unit), uSeq);

		CPrintf Args("-c \"/opt/crimson/scripts/c3-make-cert %s %s", PCTXT(Type), PCTXT(Unit));

		CString Pass;

		if( Type == "x509" ) {

			Pass.Printf("%4.4X-%4.4X-%4.4X", WORD(rand()), WORD(rand()), WORD(rand()));

			Args += " ";

			Args += Pass;
		}

		if( !pLinux->CallProcess("/bin/sh", Args + "\"", NULL, NULL) ) {

			CString Data = "{\n";

			Data += "\"name\":\"" + Name;

			Data += "\",\n";

			if( Type.StartsWith("x509") ) {

				CString Cert = ReadFile("/./tmp/crimson/mkcert/output.crt");

				Data += "\"cert\":\"";

				Data += "data:application/x-x509-ca-cert;base64," + CBase64::ToBase64(Cert);

				Data += "\",\n";
			}

			if( true ) {

				CString Priv = ReadFile("/./tmp/crimson/mkcert/output.key");

				Data += "\"priv\":\"";

				Data += "data:application/octet-stream;base64," + CBase64::ToBase64(Priv);

				Data += "\",\n";
			}

			if( !Pass.IsEmpty() ) {

				Data += "\"pass\":\"" + Pass;

				Data += "\",\n";
			}

			AppendOkayAndClose(Data, true);

			Ctx.pReq->SetReplyBody(Data);

			Ctx.pReq->SetStatus(200);

			Ctx.pReq->SetContentType("application/json");

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteDecrypt(CWebReqContext const &Ctx)
{
	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		CString   Data  = "{";

		bool      fOkay = false;

		CString   ReqText(PCSTR(Ctx.pReq->GetRequestBody()), Ctx.pReq->GetRequestSize());

		CJsonData ReqJson;

		if( ReqJson.Parse(ReqText) ) {

			CString Key  = ReqJson.GetValue("key", "");

			CString Pass = ReqJson.GetValue("pass", "");

			if( !Key.IsEmpty() && !Pass.IsEmpty() ) {

				mkdir("/./tmp/crimson/decrypt", 0755);

				Key.StripToken(',');

				CByteArray KeyData;

				CBase64::ToBytes(KeyData, Key);

				CString   PathIn  = "/tmp/crimson/decrypt/input.key";

				CString   PathOut = "/tmp/crimson/decrypt/output.key";

				CAutoFile FileIn("/." + PathIn, "w");

				if( FileIn ) {

					FileIn.Write(KeyData);

					FileIn.Close();

					CString Prog("/usr/bin/openssl");

					CPrintf Args("rsa -in %s -out %s -passin pass:%s",
						     PCTXT(PathIn),
						     PCTXT(PathOut),
						     PCTXT(Pass)
					);

					if( !pLinux->CallProcess(Prog, Args, NULL, NULL) ) {

						CAutoFile FileOut("/." + PathOut, "r");

						if( FileOut ) {

							KeyData.SetCount(FileOut.GetSize());

							FileOut.Read(KeyData.data(), KeyData.size());

							Data += "\"key\":\"";

							Data += "data:application/octet-stream;base64," + CBase64::ToBase64(KeyData);

							Data += "\",\n";

							fOkay = true;
						}
					}
				}

				unlink("/." + PathIn);

				unlink("/." + PathOut);
			}
		}

		AppendOkayAndClose(Data, fOkay);

		Ctx.pReq->SetReplyBody(Data);

		Ctx.pReq->SetStatus(200);

		Ctx.pReq->SetContentType("application/json");

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteSetPass(CWebReqContext const &Ctx)
{
	if( Ctx.pSess->IsLocalUser() ) {

		CString p1 = Ctx.pReq->GetParamString("p1", "");

		CString p2 = Ctx.pReq->GetParamString("p2", "");

		if( m_pPxe->SetUserPass(Ctx.pSess->GetUser(), p1, p2) ) {

			Ctx.pReq->SetReplyBody("OK");

			Ctx.pReq->SetStatus(200);

			Ctx.pReq->SetContentType("text/plain");

			return TRUE;
		}

		Ctx.pReq->SetReplyBody("The old password is incorrect.");

		Ctx.pReq->SetStatus(200);

		Ctx.pReq->SetContentType("text/plain");

		return TRUE;
	}

	Ctx.pReq->SetReplyBody("Cannot edit password of non-local user.");

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("text/plain");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteSysWrite(CWebReqContext const &Ctx)
{
	CString f = Ctx.pReq->GetParamString("f", "");

	CString t = Ctx.pReq->GetParamString("t", "");

	if( f == "config" ) {

		UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

		if( t[0] == 'd' ) {

			if( uAccess <= 2 ) {

				if( CString(Ctx.pReq->GetRequestHeader("Content-Type")) == "application/json" ) {

					PCBYTE  pData = Ctx.pReq->GetRequestBody();

					UINT    uData = Ctx.pReq->GetRequestSize();

					CString Value = CString(PCTXT(pData), uData);

					CJsonData Json;

					if( Json.Parse(Value) ) {

						CString i = Ctx.pReq->GetParamString("i", "");

						PCTXT tag = "hspu";

						for( UINT n = 0; n < 4; n++ ) {

							char cTag = tag[n];

							if( i.IsEmpty() || i.Find(cTag) < NOTHING ) {

								UINT uTarget = 1;

								if( cTag == 'u' ) {

									uTarget = 0;
								}

								if( cTag == 'p' ) {

									uTarget = 2;
								}

								if( uAccess <= uTarget ) {

									CJsonData *pPart = Json.GetChild(CPrintf("%cconfig", cTag));

									if( pPart ) {

										CString Text = pPart->GetAsText(FALSE);

										m_pConfig->SetConfig(cTag, Text, true);
									}
								}
							}
						}
					}
				}
			}

			Ctx.pReq->SetReplyBody("OK");

			Ctx.pReq->SetStatus(200);

			Ctx.pReq->SetContentType("text/plain");

			return TRUE;
		}
		else {
			if( (uAccess == 0 || t[0] != 'u') && uAccess <= (t[0] == 'p' ? 2u : 1u) ) {

				if( CString(Ctx.pReq->GetRequestHeader("Content-Type")) == "application/json" ) {

					PCBYTE  pData = Ctx.pReq->GetRequestBody();

					UINT    uData = Ctx.pReq->GetRequestSize();

					CString Value = CString(PCTXT(pData), uData);

					if( CheckType(Value, t[0]) ) {

						m_pConfig->SetConfig(t[0], Value, true);

						m_pConfig->GetConfig(t[0], Value);
					}
					else {
						Value = "{}";
					}

					Ctx.pReq->SetReplyBody(Value);

					Ctx.pReq->SetStatus(200);

					Ctx.pReq->SetContentType("application/json");

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteSysReset(CWebReqContext const &Ctx)
{
	CString f = Ctx.pReq->GetParamString("f", "");

	CString t = Ctx.pReq->GetParamString("t", "");

	if( f == "config" ) {

		UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

		if( t[0] == 'd' ) {

			t = Ctx.pReq->GetParamString("i", "hspu");
		}

		PCTXT tag = "hspu";

		for( UINT n = 0; n < 4; n++ ) {

			char cTag = tag[n];

			if( t.Find(cTag) < NOTHING ) {

				UINT uTarget = 1;

				if( cTag == 'u' ) {

					uTarget = 0;
				}

				if( cTag== 'p' ) {

					uTarget = 2;
				}

				if( uAccess <= uTarget ) {

					CString Text, Name;

					if( m_pSchema->GetDefault(cTag, Text) ) {

						m_pSchema->GetNaming(cTag, Name);

						m_pConfig->SetConfig(cTag, Text, true);
					}
				}
			}
		}

		Ctx.pReq->SetReplyBody("OK");

		Ctx.pReq->SetStatus(200);

		Ctx.pReq->SetContentType("text/plain");

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteGetDump(CWebReqContext const &Ctx)
{
	CString Name = Ctx.pReq->GetParamString("n", "");

	if( !Name.IsEmpty() ) {

		if( Name.Find('/') == NOTHING ) {

			Ctx.pReq->AddReplyHeader("Content-Disposition", "attachment; filename=\"" + Name + "\"");

			ReplyWithDevFile(Ctx, "/./vap/opt/crimson/logs/" + Name, FALSE);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteClearDumps(CWebReqContext const &Ctx)
{
	if( ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess() == 0 ) {

		AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

		if( pLinux ) {

			CString         Path = "/./vap/opt/crimson/logs";

			CAutoDirentList List;

			if( List.ScanFiles(Path, List.RevSort) ) {

				for( UINT n = 0; n < List.GetCount(); n++ ) {

					CString File = List[n]->d_name;

					unlink(Path + '/' + File);
				}
			}
		}
	}

	Ctx.pReq->SetReplyBody("OK");

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("text/plain");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteSysToolStart(CWebReqContext const &Ctx)
{
	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		// TODO -- All these new data cleansing!!!

		CString Tab  = Ctx.pReq->GetParamString("tab", "tab");

		CString Cmd  = Ctx.pReq->GetParamString("cmd", "");

		CString Host = Ctx.pReq->GetParamString("host", "");

		CString Dest = Ctx.pReq->GetParamString("dest", "");

		UINT    face = Ctx.pReq->GetParamDecimal("face", 99);

		UINT    port = Ctx.pReq->GetParamDecimal("port", 80);

		if( !Host.IsEmpty() || !Dest.IsEmpty() ) {

			CString Tail;

			if( Cmd == "ping" || Cmd == "arping" ) {

				Tail = Cmd + " -c 8 -w 2 ";

				if( Cmd == "ping" ) {

					Tail += "-i 0.25 ";
				}

				if( face < 99 ) {

					CString Name;

					if( m_pNetUtils->GetInterfaceName(face, Name) ) {

						Tail.AppendPrintf("-I %s ", PCSTR(Name));
					}
				}

				Tail += Host;
			}

			if( Cmd == "tracert" ) {

				Tail = "traceroute ";

				if( face < 99 ) {

					CString Name;

					if( m_pNetUtils->GetInterfaceName(face, Name) ) {

						Tail.AppendPrintf("-i %s ", PCSTR(Name));
					}
				}

				Tail += Host;
			}

			if( Cmd == "tcptest" ) {

				Tail = "TcpTest ";

				Tail += Host;

				Tail.AppendPrintf(" %u", port);
			}

			if( Cmd == "nslookup" ) {

				Tail = "nslookup ";

				Tail += Host;

				Tail += " 127.0.0.1";
			}

			if( Cmd == "getroute" ) {

				Tail = "ip route get ";

				Tail += Dest;

				if( face < 99 ) {

					CIpAddr Addr;

					if( m_pNetUtils->GetInterfaceAddr(face, Addr) ) {

						Tail += " from ";

						Tail += Addr.GetAsText();
					}
				}
			}

			if( !Tail.IsEmpty() ) {

				AfxTrace("Running [%s]\n", PCSTR(Tail));

				CString Init = "/opt/crimson/bin/InitC32";

				CPrintf Args = CPrintf("run tool-%s %s", PCSTR(Tab), PCSTR(Tail));

				pLinux->CallProcess(Init, Args, NULL, NULL);
			}
		}
	}

	Ctx.pReq->SetReplyBody("OK");

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("text/plain");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteSysToolStop(CWebReqContext const &Ctx)
{
	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		CString Tab  = Ctx.pReq->GetParamString("tab", "tab");

		CString Init = "/opt/crimson/bin/InitC32";

		CPrintf Args = CPrintf("stop tool-%s", PCSTR(Tab));

		pLinux->CallProcess(Init, Args, NULL, NULL);
	}

	Ctx.pReq->SetReplyBody("OK");

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("text/plain");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteDevRead(CWebReqContext const &Ctx)
{
	UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

	if( uAccess <= 0 ) {

		AfxGetAutoObject(pSec, "dev.security", 0, IFeatures);

		if( pSec ) {

			BYTE key[16];

			if( pSec->GetDeviceCode(key, sizeof(key)) == sizeof(key) ) {

				CString Model  = CString(m_pPlatform->GetModel()).ToLower();

				CString Serial = m_pPlatform->GetSerial();

				CString Text;

				for( UINT n = 0; n < sizeof(key); n++ ) {

					Text.AppendPrintf("%2.2X", key[n]);
				}

				Model.MakeUpper();

				Serial.MakeUpper();

				Text.AppendPrintf("\n%s,%s\n%8.8X\n%8.8X\n",
						  PCSTR(Model),
						  PCSTR(Serial),
						  0,
						  pSec->GetEnabledGroup()

				);

				Model.MakeLower();

				Serial.MakeLower();

				CPrintf File("%s-%s-devkey.txt", PCSTR(Model), PCSTR(Serial));

				Ctx.pReq->SetReplyBody(Text);

				Ctx.pReq->AddReplyHeader("Content-Disposition", CPrintf("attachment; filename=\"%s\"", PCSTR(File)));

				Ctx.pReq->SetContentType("text/plain");

				Ctx.pReq->SetStatus(200);

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteKeyWrite(CWebReqContext const &Ctx)
{
	UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

	if( uAccess <= 0 ) {

		AfxGetAutoObject(pSec, "dev.security", 0, IFeatures);

		if( pSec ) {

			UINT   uData = Ctx.pReq->GetRemoteSize();

			PCBYTE pData = Ctx.pReq->GetRemoteBody();

			if( pSec->InstallUnlock(pData, uData) ) {

				Ctx.pReq->SetReplyBody("OK");

				Ctx.pReq->SetStatus(200);

				return TRUE;
			}

			Ctx.pReq->SetReplyBody("FAIL");

			Ctx.pReq->SetStatus(200);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteRestart(CWebReqContext const &Ctx)
{
	UINT uAccess = ((CPxeHttpServerSession *) Ctx.pSess)->GetAccess();

	if( uAccess <= 0 ) {

		AfxGetAutoObject(pDbase, "c3.database", 0, IDatabase);

		UINT t = Ctx.pReq->GetParamDecimal("t", 0);

		switch( t ) {

			case 0:
			{
				m_pPxe->RestartSystem(0, 55);
			}
			break;

			case 1:
			{
				m_pPxe->RestartSystem(0, 44);
			}
			break;

			case 2:
			{
				m_pPxe->SystemStop();

				pDbase->Clear();

				m_pPxe->RestartSystem(0, 55);
			}
			break;

			case 3:
			{
				m_pConfig->ClearData();

				pDbase->Clear();

				m_pPxe->RestartSystem(0, 66);
			}
			break;
		}

		Ctx.pReq->SetReplyBody("OK");

		Ctx.pReq->SetContentType("text/plain");

		Ctx.pReq->SetStatus(200);

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::ExecuteToolAutoSled(CWebReqContext const &Ctx)
{
	CString Data = "{";

	for( UINT s = 0; s < 3; s++ ) {

		CPrintf   Name("/./tmp/skvs.d/SYSTEM_SLED_PORT%u_TYPE", 1+s);

		CAutoFile File(Name, "r");

		if( File ) {

			CString Line = File.GetLine();

			if( Line == "SERIAL" ) {

				CPrintf   Name("/./tmp/skvs.d/SYSTEM_SLED_PORT%u_XR_ID", 1+s);

				CAutoFile File(Name, "r");

				if( File ) {

					Line += "-";

					Line += File.GetLine();
				}
			}

			AppendJson(Data, CPrintf("slot%u", s), Line);
		}
	}

	AppendOkayAndClose(Data, true);

	Ctx.pReq->SetReplyBody(Data);

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("application/json");

	return TRUE;
}

BOOL CPxeWebServer::ExecuteToolAutoPort(CWebReqContext const &Ctx)
{
	CString Data = "{";

	bool    okay = false;

	AfxGetAutoObject(pModel, "c3.model", 0, IPxeModel);

	if( pModel ) {

		UINT c = pModel->GetObjCount('s');

		for( UINT p = 0; p < c; p++ ) {

			AppendJson(Data, CPrintf("port%u", 1+p), CPrintf("%u", pModel->GetPortType(p)));
		}

		okay = true;
	}

	AppendOkayAndClose(Data, okay);

	Ctx.pReq->SetReplyBody(Data);

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("application/json");

	return TRUE;
}

BOOL CPxeWebServer::ExecutePutImage(CWebReqContext const &Ctx)
{
	CString Reply("OK");

	IHttpStreamWrite *pStream = Ctx.pCon->GetStream();

	if( pStream ) {

		IHttpStreamHash *pHash = NULL;

		pStream->QueryInterface(AfxAeonIID(IHttpStreamHash), (void **) &pHash);

		if( pHash ) {

			CString Hash;

			pHash->GetHash(Hash);

			Reply += ':';

			Reply += Hash;
		}
	}

	rename("/update/image.tmp", "image.ci3");

	unlink("/update/image.ci3.d");

	Ctx.pReq->SetReplyBody(Reply);

	Ctx.pReq->SetStatus(200);

	Ctx.pReq->SetContentType("text/plain");

	m_pPxe->RestartSystem(0, 55);

	return TRUE;
}

// Ajax Helpers

BOOL CPxeWebServer::GetCellPipeName(CString &Name, UINT uIndex)
{
	AfxGetAutoObject(pCell, "net.cell", uIndex, ICellStatus);

	if( pCell ) {

		CCellStatusInfo Info;

		if( pCell->GetCellStatus(Info) ) {

			Name = "/./tmp/crimson/face/" + Info.m_Device + "/monitor.pipe";

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::GetWiFiPipeName(CString &Name, UINT uIndex)
{
	AfxGetAutoObject(pWiFi, "net.wifi", uIndex, IWiFiStatus);

	if( pWiFi ) {

		CWiFiStatusInfo Info;

		if( pWiFi->GetWiFiStatus(Info) ) {

			Name = "/./tmp/crimson/face/" + Info.m_Device + "/monitor.pipe";

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::AppendPxeStatus(CString &Data, BOOL fFull)
{
	if( fFull ) {

		if( !m_pFeatures || m_pFeatures->GetEnabledGroup() >= SW_GROUP_2 ) {

			CString Status;

			switch( m_pPxe->GetStatus() ) {

				case 5:
				{
					Status = "Starting";
				}
				break;

				case 7:
				{
					Status = "Running";

					AppendJson(Data, "stop", true);
				}
				break;

				case 9:
				{
					Status = "Stopped";

					AppendJson(Data, "start", true);

				}
				break;

				case 10:
				{
					Status = "Invalid Database";
				}
				break;

				case 11:
				case 12:
				{
					Status = "Shutting Down";
				}
				break;

				default:
				{
					Status = "Starting Up";
				}

				break;
			}

			AppendJson(Data, "status", Status);

			return TRUE;
		}

		AppendJson(Data, "status", "Not Supported");

		return TRUE;
	}

	AppendJson(Data, "status", m_pPxe->GetStatus());

	return TRUE;
}

BOOL CPxeWebServer::AppendGpsStatus(CString &Data, BOOL fFull)
{
	AfxGetAutoObject(pLoc, "c3.location", 0, ILocationSource);

	if( pLoc ) {

		CLocationSourceInfo Info;

		if( pLoc->GetLocationData(Info) ) {

			if( Info.m_uFix >= 2 ) {

				AppendJson(Data, "lat", Info.m_Lat);

				AppendJson(Data, "long", Info.m_Long);

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::AppendDeviceInfo(CString &Data, BOOL fFull)
{
	CString Work;

	CString Model  = m_pPlatform->GetModel();

	CString Family = m_pPlatform->GetFamily();

	Model.MakeUpper();

	if( !Family.IsEmpty() ) {

		if( Family.EndsWith("(R)") ) {

			Family.Delete(Family.GetLength()-3, 3);

			Family += "&#174;";
		}

		if( Family.EndsWith("(TM)") ) {

			Family.Delete(Family.GetLength()-4, 4);

			Family += "&#153;";
		}

		Model = Family + " " + Model;
	}

	#if defined(AEON_PLAT_WIN32)

	Model += " Emulator";

	#endif

	AppendJson(Work, "model", Model);

	AppendJson(Work, "serial", CString(m_pPlatform->GetSerial()).ToUpper());

	AppendJson(Work, "version", C3_VERSION);

	switch( m_pFeatures ? m_pFeatures->GetEnabledGroup() : SW_GROUP_4 ) {

		case SW_GROUP_1:
			AppendJson(Work, "group", "Group 1 (Networking Only)");
			break;

		case SW_GROUP_2:
			AppendJson(Work, "group", "Group 2 (Crimson Protocol Conversion)");
			break;

		case SW_GROUP_3A:
		case SW_GROUP_3B:
		case SW_GROUP_3C:
			AppendJson(Work, "group", "Group 3 (Full Crimson Functionality)");
			break;

		case SW_GROUP_4:
			AppendJson(Work, "group", "Group 4 (Crimson plus Crimson Control)");
			break;
	}

	if( fFull ) {

		AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

		if( pLinux ) {

			CString Up = CAutoFile("/./proc/uptime", "r").GetLine();

			AppendJson(Work, "duptime", UINT(60 * (atoi(Up) / 60)));

			AppendJson(Work, "sled1", CAutoFile("/./tmp/skvs.d/SYSTEM_SLED_PORT1_TYPE", "r").GetLine());

			AppendJson(Work, "sled2", CAutoFile("/./tmp/skvs.d/SYSTEM_SLED_PORT2_TYPE", "r").GetLine());

			AppendJson(Work, "sled3", CAutoFile("/./tmp/skvs.d/SYSTEM_SLED_PORT3_TYPE", "r").GetLine());

			CString Os  = CAutoFile("/./proc/version", "r").GetLine();

			CString Rev = CAutoFile("/./bin/osrev", "r").GetLine();

			Os = Os.Left(Os.Find('(') - 1);

			Os.Replace("version", "Version");

			if( !Rev.IsEmpty() ) {

				Os += " Rev ";

				Os += Rev;
			}

			AppendJson(Work, "os", Os);
		}

		time_t InitTime;

		m_pPxe->GetInitTime(InitTime);

		AppendJson(Work, "suptime", GetConnectTime(InitTime));
	}

	AppendOkayAndClose(Work, true);

	Data.Append("\"device\":{" + Work + ",");

	return TRUE;
}

BOOL CPxeWebServer::AppendCoreDumps(CString &Data)
{
	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		CString         Path = "/./vap/opt/crimson/logs";

		CAutoDirentList List;

		if( List.ScanFiles(Path, List.RevSort) ) {

			Data += "\"dumps\":[";

			UINT c = List.GetCount();

			for( UINT n = 0; n < Min(c, 8u); n++ ) {

				CString File = List[n]->d_name;

				if( File.EndsWith(".dump.gz") ) {

					if( n ) {

						Data += ',';
					}

					Data += "{";

					AppendJson(Data, "file", List[n]->d_name);

					AppendOkayAndClose(Data, true);
				}
			}

			Data += "],";
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::AppendFaceStatus(CString &Data, CString const &Find)
{
	CString Work;

	UINT c = m_pNetUtils->GetInterfaceCount();

	for( UINT n = 1; n < c; n++ ) {

		CString Name;

		bool    only = false;

		m_pNetUtils->GetInterfaceName(n, Name);

		if( Find == "all" || (only = ((Find == Name) ? true : false)) ) {

			CString Face;

			if( AppendFaceStatus(Face, n) ) {

				AppendJson(Face, "name", Name);

				if( !only ) {

					AppendOkayAndClose(Face, true);

					Work.Splice(',', "{");

					Work.Append(Face);
				}
				else {
					Data.Append(Face);

					return TRUE;
				}
			}
		}
	}

	if( Find == "all" ) {

		Data.Append("\"faces\":[" + Work + "],");
	}

	return TRUE;
}

BOOL CPxeWebServer::AppendFaceStatus(CString &Face, UINT uFace)
{
	CString  Desc, Path;

	CIpAddr  Addr, Mask;

	m_pNetUtils->GetInterfaceDesc(uFace, Desc);
	m_pNetUtils->GetInterfacePath(uFace, Path);
	m_pNetUtils->GetInterfaceAddr(uFace, Addr);
	m_pNetUtils->GetInterfaceMask(uFace, Mask);

	AppendJson(Face, "desc", Desc);
	AppendJson(Face, "path", Path);
	AppendJson(Face, "addr", Addr.IsEmpty() ? "" : Addr.GetAsText());
	AppendJson(Face, "mask", Mask.IsEmpty() ? "" : Mask.GetAsText());

	switch( m_pNetUtils->GetInterfaceType(uFace) ) {

		case IT_ETHERNET:
		{
			UINT uIndex = m_pNetUtils->GetInterfaceOrdinal(uFace);

			if( AppendEthernetStatus(Face, uIndex, FALSE) ) {

				CMacAddr Mac;

				m_pNetUtils->GetInterfaceMac(uFace, Mac);

				AppendJson(Face, "mac", Mac.GetAsText());

				AppendJson(Face, "type", "eth");

				AppendJson(Face, "index", uIndex);
			}
		}
		break;

		case IT_CELLULAR:
		{
			UINT uIndex = m_pNetUtils->GetInterfaceOrdinal(uFace);

			if( AppendCellStatus(Face, uIndex, FALSE) ) {

				AppendJson(Face, "type", "cell");

				AppendJson(Face, "index", uIndex);
			}
		}
		break;

		case IT_WIFI:
		{
			UINT uIndex = m_pNetUtils->GetInterfaceOrdinal(uFace);

			if( AppendWiFiStatus(Face, uIndex, FALSE) ) {

				CMacAddr Mac;

				m_pNetUtils->GetInterfaceMac(uFace, Mac);

				AppendJson(Face, "mac", Mac.GetAsText());

				AppendJson(Face, "type", "wifi");

				AppendJson(Face, "index", uIndex);
			}
		}
		break;

		default:
		{
			AppendJson(Face, "up", m_pNetUtils->IsInterfaceUp(uFace) ? true : false);
		}
		break;
	}

	return TRUE;
}

BOOL CPxeWebServer::AppendEthernetStatus(CString &Face, UINT uIndex, BOOL fFull)
{
	AfxGetAutoObject(pEthernet, "net.ethernet", uIndex, IEthernetStatus);

	if( pEthernet ) {

		CEthernetStatusInfo Info;

		if( pEthernet->GetEthernetStatus(Info) ) {

			return AppendEthernetStatus(Face, Info, fFull);
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::AppendCellStatus(CString &Face, UINT uIndex, BOOL fFull)
{
	AfxGetAutoObject(pCell, "net.cell", uIndex, ICellStatus);

	if( pCell ) {

		CCellStatusInfo Info;

		if( pCell->GetCellStatus(Info) ) {

			return AppendCellStatus(Face, Info, fFull);
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::AppendWiFiStatus(CString &Face, UINT uIndex, BOOL fFull)
{
	AfxGetAutoObject(pWiFi, "net.wifi", uIndex, IWiFiStatus);

	if( pWiFi ) {

		CWiFiStatusInfo Info;

		if( pWiFi->GetWiFiStatus(Info) ) {

			return AppendWiFiStatus(Face, Info, fFull);
		}
	}

	return FALSE;
}

BOOL CPxeWebServer::AppendEthernetStatus(CString &Face, CEthernetStatusInfo const &Info, BOOL fFull)
{
	if( Info.m_fValid ) {

		AppendJson(Face, "full", !!Info.m_fFullDuplex);

		AppendJson(Face, "speed", Info.m_uLinkSpeed);

		if( !fFull ) {

			AppendJson(Face, "up", !!Info.m_fOnline);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::AppendCellStatus(CString &Face, CCellStatusInfo const &Info, BOOL fFull)
{
	if( Info.m_fValid ) {

		AppendJson(Face, "network", Info.m_Network);

		if( fFull ) {

			AppendJson(Face, "signal", Info.m_uSignal);

			AppendJson(Face, "online", !!Info.m_fOnline);

			AppendJson(Face, "register", !!Info.m_fRegister);

			AppendJson(Face, "roam", !!Info.m_fRoam);

			AppendJson(Face, "service", Info.m_Service);

			AppendJson(Face, "slot", Info.m_uSlot);

			AppendJson(Face, "model", Info.m_Model);

			AppendJson(Face, "version", Info.m_Version);

			AppendJson(Face, "carrier", Info.m_Carrier);

			AppendJson(Face, "iccid", Info.m_Iccid);

			AppendJson(Face, "imsi", Info.m_Imsi);

			AppendJson(Face, "imei", Info.m_Imei);

			AppendJson(Face, "state", Info.m_State);

			AppendJson(Face, "addr", Info.m_Addr.GetAsText());

			AppendJson(Face, "ctime", GetConnectTime(Info.m_CTime));
		}
		else {
			AppendJson(Face, "up", !!Info.m_fOnline);

			AppendJson(Face, "register", !!Info.m_fRegister);

			AppendJson(Face, "bars", ToBars(Info.m_uSignal));
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeWebServer::AppendWiFiStatus(CString &Face, CWiFiStatusInfo const &Info, BOOL fFull)
{
	if( Info.m_fValid ) {

		AppendJson(Face, "apmode", !!Info.m_fApMode);

		AppendJson(Face, "network", Info.m_Network);

		AppendJson(Face, "model", Info.m_Model);

		AppendJson(Face, "version", Info.m_Version);

		if( fFull ) {

			AppendJson(Face, "signal", Info.m_uSignal);

			AppendJson(Face, "online", !!Info.m_fOnline);

			AppendJson(Face, "peermac", Info.m_PeerMac.GetAsText());

			AppendJson(Face, "state", Info.m_State);

			AppendJson(Face, "ctime", GetConnectTime(Info.m_CTime));
		}
		else {
			AppendJson(Face, "up", !!Info.m_fOnline);

			AppendJson(Face, "register", !!Info.m_fOnline);

			AppendJson(Face, "bars", ToBars(Info.m_uSignal));
		}

		return TRUE;
	}

	return FALSE;
}

void CPxeWebServer::AppendJson(CString &Json, PCTXT pName, PCTXT pData)
{
	Json.AppendPrintf("\"%s\":\"%s\",", pName, pData);
}

void CPxeWebServer::AppendJson(CString &Json, PCTXT pName, bool fData)
{
	Json.AppendPrintf("\"%s\":%s,", pName, fData ? "true" : "false");
}

void CPxeWebServer::AppendJson(CString &Json, PCTXT pName, UINT uData)
{
	Json.AppendPrintf("\"%s\":%u,", pName, uData);
}

void CPxeWebServer::AppendJson(CString &Json, PCTXT pName, double nData)
{
	Json.AppendPrintf("\"%s\":%.8f,", pName, nData);
}

void CPxeWebServer::AppendOkayAndClose(CString &Json, bool fOkay)
{
	Json.AppendPrintf("\"okay\":%s}", fOkay ? "true" : "false");
}

UINT CPxeWebServer::GetConnectTime(UINT uTime)
{
	if( uTime ) {

		uTime = getmonosecs() - uTime;

		return uTime - uTime % 60;
	}

	return 0;
}

UINT CPxeWebServer::ToBars(UINT uSignal)
{
	if( uSignal < 99 ) {

		if( uSignal >= 20 ) return 4;
		if( uSignal >= 15 ) return 3;
		if( uSignal >= 10 ) return 2;
		if( uSignal >=  2 ) return 1;
	}

	if( uSignal == 100 ) {

		return 100;
	}

	return 0;
}

// File Reader

CString CPxeWebServer::ReadFile(CString const &Name)
{
	CAutoFile File(Name, "r");

	if( File ) {

		CString Text;

		UINT    uSize = File.GetSize();

		Text.Expand(uSize+1);

		File.Read(PSTR(PCSTR(Text)), uSize);

		Text.FixLength(uSize);

		File.Close();

		return Text;
	}

	return "";
}

// Implementation

void CPxeWebServer::AddListItem(CString &d, CHttpServerRequest *pReq, CString Link, CString Text)
{
	d.AppendPrintf("<li><a href='%s'>%s</a></li>",
		       PCTXT(Link),
		       PCTXT(Text)
	);
}

void CPxeWebServer::AddSubMenu(CString &d, CString Text)
{
	d += "<li class='dropdown-submenu'><a href='#'>";

	d += Text;

	d += "</a><ul class='dropdown-menu'>";
}

void CPxeWebServer::EndSubMenu(CString &d)
{
	d += "</ul></li>";
}

BOOL CPxeWebServer::RunCommand(CString &t, UINT n)
{
	if( n < elements(m_Cmds) ) {

		CSysCmd const &Cmd = m_Cmds[n];

		if( Cmd.fEnable ) {

			if( Cmd.fLinux ) {

				AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

				if( pLinux ) {

					CPrintf Temp("/./tmp/%8.8X%8.8X", rand(), rand());

					CString Head(Cmd.pCmd);

					CString Args;

					UINT    uPos;

					if( Head.StartsWith("/opt/crimson/scripts/") ) {

						Head = "/bin/sh -c \"" + Head + "\"";
					}

					if( (uPos = Head.Find(' ')) < NOTHING ) {

						Args = Head.Mid(uPos+1);

						Head = Head.Left(uPos);
					}

					if( pLinux->CallProcess(Head, Args, Temp, Temp) == 0 ) {

						CString Reply = ReadFile(Temp);

						if( !Reply.IsEmpty() ) {

							CStringArray Lines;

							t.Expand(5 * Reply.GetLength() / 4);

							Reply.Tokenize(Lines, '\n');

							t += "<pre>";

							for( UINT n = 0; n < Lines.GetCount(); n++ ) {

								t += "<p>";

								t += Lines[n];

								t += "</p>";
							}

							t += "</pre>";
						}
						else {
							t += "<pre>";

							t += "<p>(no output)</p>";

							t += "</pre>";
						}

						unlink(Temp);

						return TRUE;
					}

					unlink(Temp);
				}
			}
			else {
				AfxGetAutoObject(pDiag, "diagmanager", 0, IDiagManager);

				if( pDiag ) {

					char *pText = NULL;

					pDiag->RunCommand(&pText, 1, Cmd.pCmd);

					if( pText ) {

						t = pText;

						free(pText);
					}

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

void CPxeWebServer::MakeOptionTable(CString &t, PCTXT p, CString d)
{
	t += "<table class='table'><thead><tr><th>";

	t += GetWebString(IDS_OPTION);

	t += "</th><th>";

	t += GetWebString(IDS_DESCRIPTION);

	t += "</th></tr></thead><tbody>";

	if( p ) {

		t.AppendPrintf("<tr id='%s-head'></tr>", p);

		t += d;

		t.AppendPrintf("<tr id='%s-tail'></tr>", p);
	}
	else
		t += d;

	t += "</tbody></table>";
}

void CPxeWebServer::CheckCmds(void)
{
	AfxGetAutoObject(pDiag, "diagmanager", 0, IDiagManager);

	if( pDiag ) {

		AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

		for( int n = 0; n < elements(m_Cmds); n++ ) {

			CSysCmd &Cmd = m_Cmds[n];

			if( Cmd.fLinux ) {

				Cmd.fEnable = pLinux ? true : false;
			}
			else {
				Cmd.fEnable = (pDiag->RunCommand(NULL, 0, Cmd.pCmd) == 0);
			}
		}
	}
}

BOOL CPxeWebServer::CheckType(CString const &Text, char cTag)
{
	CString key = "\"atype\":";

	UINT    f1  = Text.Find(key);

	if( f1 < NOTHING ) {

		UINT f2 = Text.Find('"', f1 + key.GetLength());

		if( f2 < NOTHING ) {

			if( Text[f2+1] == cTag && Text[f2+2] == '"' ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

// End of File
