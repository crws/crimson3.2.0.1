
#include "Intern.hpp"

#include "FileConfigStorage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IDatabase * Create_FileDatabase(CString const &Root, UINT uBase, UINT uPool);

//////////////////////////////////////////////////////////////////////////
//
// Filing System Config Storage Object
//

// Instantiator

IConfigStorage * Create_FileConfigStorage(CString const &Root)
{
	return new CFileConfigStorage(Root);
}

// Constructor

CFileConfigStorage::CFileConfigStorage(CString const &Root) : CBaseConfigStorage(Create_FileDatabase(Root, 0, 1024))
{
}

// End of File
