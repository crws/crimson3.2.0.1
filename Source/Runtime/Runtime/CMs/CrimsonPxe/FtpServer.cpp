
#include "intern.hpp"

#include "FtpServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

#include "FtpSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// FTP Server
//

// Time Constants

static UINT const timeScanDelay = 20;

// Constructor

CFtpServer::CFtpServer(CJsonConfig *pJson)
{
	memset(m_pThread, 0, sizeof(m_pThread));

	m_fEnable = FALSE;

	m_pConfig = New CFtpConfig;

	m_pConfig->m_uAnon     = 2;

	m_pConfig->m_uTlsMode  = 1;

	m_pConfig->m_uCmdPort  = 21;

	ApplyConfig(pJson);

	StdSetRef();
}

// Destructor

CFtpServer::~CFtpServer(void)
{
	delete m_pConfig;
}

// IUnknown

HRESULT CFtpServer::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CFtpServer::AddRef(void)
{
	StdAddRef();
}

ULONG CFtpServer::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CFtpServer::TaskInit(UINT uTask)
{
	if( uTask == 0 ) {

		SetThreadName("FtpServer");

		if( m_fEnable ) {

			return TRUE;
		}

		Release();

		return FALSE;
	}
	else {
		UINT s = uTask - 1;

		SetThreadName("FtpSession");

		m_pSession[s] = NULL;

		return TRUE;
	}
}

INT CFtpServer::TaskExec(UINT uTask)
{
	if( uTask == 0 ) {

		UINT p = 24045;

		for( UINT s = 0; s < elements(m_pThread); s++ ) {

			m_pThread[s] = CreateClientThread(this, 1+s, p);
		}

		Sleep(FOREVER);
	}
	else {
		UINT s = uTask - 1;

		m_pSession[s] = New CFtpSession;

		m_pSession[s]->SetConfig(m_pConfig);

		for( ;;) {

			if( !m_pSession[s]->Poll() ) {

				Sleep(timeScanDelay);
			}
		}

		Sleep(FOREVER);
	}

	return 0;
}

void CFtpServer::TaskStop(UINT uTask)
{
	if( uTask == 0 ) {

		for( UINT s = 0; s < elements(m_pThread); s++ ) {

			m_pThread[s]->Destroy();
		}
	}
}

void CFtpServer::TaskTerm(UINT uTask)
{
	if( uTask == 0 ) {

		Release();
	}
	else {
		UINT s = uTask - 1;

		if( m_pSession[s] ) {

			delete m_pSession[s];

			m_pSession[s] = NULL;
		}
	}
}

// Implementation

void CFtpServer::ApplyConfig(CJsonConfig *pJson)
{
	if( pJson ) {

		m_fEnable = pJson->GetValueAsBool("mode", FALSE);

		m_pConfig->m_uAnon    = pJson->GetValueAsUInt("anon", 0, 0, 2);

		m_pConfig->m_uTlsMode = pJson->GetValueAsUInt("tls", 0, 0, 2);

		m_pConfig->m_uCmdPort = pJson->GetValueAsUInt("port", 21, 1, 65535);
	}
}

// End of File
