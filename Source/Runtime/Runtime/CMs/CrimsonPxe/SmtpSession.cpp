
#include "intern.hpp"

#include "SmtpSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SmtpDefs.hpp"

#include "SmtpEncodeBase.hpp"

#include "SmtpEncodeMime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Timing Constants
//

static UINT const timeTimeout      = (5*60*1000);

static UINT const timeDataTimeout  = (2*60*1000);

static UINT const timeSendTimeout  = (3*60*1000);

static UINT const timeEODTimeout   = (10*60*1000);

static UINT const timeSendDelay	   = 10;

static UINT const timeBuffDelay    = 100;

//////////////////////////////////////////////////////////////////////////
//
// SMTP Client
//

// States

enum
{
	smtpIdle,
	smtpConn,
	smtpHelo,
	smtpEhlo,
	smtpQuit,
	smtpOrig,
	smtpRcpt,
	smtpData1,
	smtpData2,
	smtpEOD,
	smtpAuth,
	smtpTls,
};

// Constructor

CSmtpSession::CSmtpSession(CSmtpConfig const *pConfig)
{
	m_pConfig = pConfig;

	m_uState  = smtpConn;

	m_fTls    = FALSE;

	m_uOption = 69;
}

// Destructor

CSmtpSession::~CSmtpSession(void)
{
}

// Operations

BOOL CSmtpSession::SendMail(CMsgPayload const *pMessage, BOOL fKeep)
{
	BOOL fTls = (m_pConfig->m_uTls == 1);

	if( OpenCmdSocket(m_pConfig->m_Server, WORD(m_pConfig->m_uPort), TRUE, fTls) ) {

		m_pMessage = pMessage;

		m_fGone    = FALSE;

		while( m_uState != smtpIdle ) {

			switch( m_uState ) {

				case smtpIdle:				break;
				case smtpConn:	HandleConn();		break;
				case smtpHelo:	HandleHelo();		break;
				case smtpEhlo:	HandleEhlo();		break;
				case smtpOrig:	HandleOrig();		break;
				case smtpRcpt:	HandleRcpt();		break;
				case smtpData1:	HandleData1();		break;
				case smtpData2:	HandleData2();		break;
				case smtpEOD:	HandleEOD(fKeep);	break;
				case smtpQuit:	HandleQuit();		break;
				case smtpAuth:	HandleAuth();		break;
				case smtpTls:	HandleTls();		break;

				default:	m_uState = smtpIdle;	break;
			}
		}

		if( m_fGone && fKeep ) {

			m_uState = smtpOrig;

			return TRUE;
		}
		else {
			CloseCmdSocket();

			return m_fGone;
		}
	}

	return FALSE;
}

// State Handlers

void CSmtpSession::HandleConn(void)
{
	if( RecvReply() ) {

		switch( m_uReplyCode ) {

			case 220:
				m_uState = smtpEhlo;
				break;

			case 421:
				m_uState = smtpIdle;
				break;

			default:
				m_uState = smtpIdle;
				break;
		}

		return;
	}

	m_uState = smtpIdle;
}

void CSmtpSession::HandleHelo(void)
{
	if( SendHelo(m_pConfig->m_OrigDomain) && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 250:
				m_uState = smtpOrig;
				break;

			case 500:
			case 501:
			case 504:
			case 421:
				m_uState = smtpQuit;
				break;

			default:
				m_uState = smtpQuit;
				break;
		}

		return;
	}

	m_uState = smtpIdle;
}

void CSmtpSession::HandleEhlo(void)
{
	CString Domain;

	if( !m_pConfig->m_OrigDomain.IsEmpty() ) {

		Domain = m_pConfig->m_OrigDomain;
	}
	else {
		CIpAddr Addr;

		m_pCmdSock->GetLocal(Addr);

		Domain = Addr.GetAsText();
	}

	if( SendEhlo(Domain) && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 250:
				if( TestEhloRep() ) {

					if( m_uState == smtpAuth ) {

						m_uAuthState = 0;

						m_NonceCount = 0;
					}
					break;
				}

				m_uState = smtpOrig;
				break;

			case 500:
				m_uState = smtpHelo;
				break;

			case 501:
			case 504:
			case 421:
				m_uState = smtpQuit;
				break;

			default:
				m_uState = smtpQuit;
				break;
		}

		return;
	}

	m_uState = smtpIdle;
}

void CSmtpSession::HandleQuit(void)
{
	if( SendQuit() && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 221:
				m_uState = smtpIdle;
				break;

			case 500:
				m_uState = smtpIdle;
				break;

			default:
				m_uState = smtpIdle;
				break;
		}

		return;
	}

	m_uState = smtpIdle;
}

void CSmtpSession::HandleRcpt(void)
{
	if( SendRcptTo(m_pMessage->m_RcptAddr) && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 250:
			case 251:
				break;

			case 500:
			case 501:
			case 503:
			case 550:
			case 551:
			case 552:
			case 553:
			case 555:
				m_fGone  = TRUE;
				m_uState = smtpQuit;
				break;

			case 421:
			case 450:
			case 451:
			case 452:
				m_uState = smtpQuit;
				break;

			default:
				m_uState = smtpQuit;
				break;
		}

		if( m_uState == smtpQuit ) {

			return;
		}

		m_uState = smtpData1;

		return;
	}

	m_uState = smtpIdle;
}

void CSmtpSession::HandleOrig(void)
{
	if( SendMailFrom(m_pConfig->m_OrigEmail) && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 250:
				m_uState = smtpRcpt;
				break;

			case 451:
			case 452:
				m_uState = smtpQuit;
				break;

			case 500:
			case 501:
			case 552:
				m_fGone  = TRUE;
				m_uState = smtpQuit;
				break;

			case 421:
				m_uState = smtpQuit;
				break;

			default:
				m_uState = smtpQuit;
				break;
		}

		return;
	}

	m_uState = smtpIdle;
}

void CSmtpSession::HandleData1(void)
{
	if( SendData() && RecvReply(timeDataTimeout) ) {

		switch( m_uReplyCode ) {

			case 354:
				m_uState = smtpData2;
				break;

			case 451:
			case 503:
			case 554:
				m_uState = smtpQuit;
				break;

			default:
				m_uState = smtpQuit;
				break;
		}

		return;
	}

	m_uState = smtpIdle;
}

void CSmtpSession::HandleData2(void)
{
	BOOL fOkay = FALSE;

	if( !m_pMessage->m_AttachFile.IsEmpty() || !m_pMessage->m_AttachData.IsEmpty() ) {

		CAutoPointer<CSmtpEncodeBase> pEncode(new CSmtpEncodeMime(this));

		fOkay = pEncode->Encode(m_pMessage);
	}
	else {
		CAutoPointer<CSmtpEncodeBase> pEncode(new CSmtpEncodeBase(this));

		fOkay = pEncode->Encode(m_pMessage);
	}

	m_uState = fOkay ? smtpEOD : smtpIdle;
}

void CSmtpSession::HandleEOD(BOOL fKeep)
{
	if( SendEOD() && RecvReply(timeEODTimeout) ) {

		switch( m_uReplyCode ) {

			case 250:
			case 251:
				m_fGone  = TRUE;
				m_uState = fKeep ? smtpIdle : smtpQuit;
				break;

			case 550:
			case 551:
			case 552:
			case 553:
				m_fGone  = TRUE;
				m_uState = fKeep ? smtpIdle : smtpQuit;
				break;

			default:
				m_uState = smtpQuit;
				break;
		}

		return;
	}

	m_uState = smtpIdle;
}

void CSmtpSession::HandleAuth(void)
{
	switch( m_uAuthMethod ) {

		case 1:
			HandleAuthLogin();
			break;

		case 2:
			HandleAuthDigest();
			break;
	}
}

void CSmtpSession::HandleTls(void)
{
	if( SendStartTls() && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 220:

				if( SwitchCmdSocket() ) {

					m_fTls   = TRUE;

					m_uState = smtpEhlo;
				}
				else {
					m_uState = smtpIdle;
				}

				break;

			case 454:
			case 501:
				m_uState = smtpQuit;
				break;

			default:
				m_uState = smtpQuit;
				break;
		}

		return;
	}

	m_uState = smtpIdle;
}

// Commands

BOOL CSmtpSession::SendHelo(PCTXT pDomain)
{
	char s[64];

	SPrintf(s, "HELO %s\r\n", pDomain);

	return Send(s);
}

BOOL CSmtpSession::SendEhlo(PCTXT pDomain)
{
	char s[64];

	SPrintf(s, "EHLO %s\r\n", pDomain);

	return Send(s);
}

BOOL CSmtpSession::SendStartTls(void)
{
	return Send("STARTTLS\r\n");
}

BOOL CSmtpSession::SendQuit(void)
{
	return Send("QUIT\r\n");
}

BOOL CSmtpSession::SendNoOp(void)
{
	return Send("NOOP\r\n");
}

BOOL CSmtpSession::SendMailFrom(PCTXT pPath)
{
	CString s;

	s.Printf("MAIL FROM:<%s>\r\n", pPath);

	return Send(s);
}

BOOL CSmtpSession::SendRcptTo(PCTXT pPath)
{
	CString s;

	s.Printf("RCPT TO:<%s>\r\n", pPath);

	return Send(s);
}

BOOL CSmtpSession::SendData(void)
{
	return Send("DATA\r\n");
}

BOOL CSmtpSession::SendEOD(void)
{
	return Send(".\r\n");
}

BOOL CSmtpSession::SendRset(void)
{
	return Send("RSET\r\n");
}

BOOL CSmtpSession::SendAuth(PCTXT pMethod)
{
	CString s;

	s.Printf("AUTH %s\r\n", pMethod);

	return Send(s);
}

BOOL CSmtpSession::SendCmnd(PCTXT pCommand)
{
	CString s;

	s.Printf("%s\r\n", pCommand);

	return Send(s);
}

// Authentication

BOOL CSmtpSession::TestEhloRep(void)
{
	m_ReplyExtra.Append(m_ReplyText);

	UINT uFrom = 1;

	UINT uPos;

	if( !m_fTls ) {

		if( m_pConfig->m_uTls == 2 ) {

			uFrom = 0;
		}
	}

	for( UINT p = uFrom; p < 2; p++ ) {

		for( UINT n = 0; n < m_ReplyExtra.GetCount(); n++ ) {

			CString const &Line = m_ReplyExtra[n];

			if( p == 0 ) {

				if( (uPos = Line.Find("STARTTLS")) < NOTHING ) {

					m_uState = smtpTls;

					return TRUE;
				}
			}

			if( p == 1 ) {

				if( (uPos = Line.Find("AUTH ")) < NOTHING ) {

					CString Reply = Line.Mid(uPos + 5);

					CString Auth  = Reply.StripToken("\r\n");

					Auth.TrimLeft();

					BOOL fDigest = FALSE;

					BOOL fBasic  = FALSE;

					while( !Auth.IsEmpty() ) {

						CString Data = Auth.StripToken(' ');

						Data.TrimBoth();

						if( Data == "DIGEST-MD5" ) {

							fDigest = TRUE;
						}

						if( Data == "LOGIN" ) {

							fBasic = TRUE;
						}
					}

					if( fDigest ) {

						if( m_pConfig->m_uAuth <= 2 ) {

							m_uAuthMethod = 2;

							m_uState      = smtpAuth;

							return TRUE;
						}
					}

					if( fBasic ) {

						if( m_pConfig->m_uAuth <= 1 ) {

							m_uAuthMethod = 1;

							m_uState      = smtpAuth;

							return TRUE;
						}
					}
				}
			}
		}
	}

	m_uAuthMethod = 0;

	return FALSE;
}

void CSmtpSession::HandleAuthLogin(void)
{
	CString Req = MakeLoginReq();

	if( SendCmnd(Req) && RecvReply() ) {

		if( TestLoginRep(m_ReplyText) ) {

			m_uState = smtpOrig;
		}
	}
}

void CSmtpSession::HandleAuthDigest(void)
{
	CString Req = MakeDigestReq();

	if( SendCmnd(Req) && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 235:
				m_uState = smtpOrig;
				break;

			case 334:
				if( TestDigestRep(CBase64::ToAnsi(m_ReplyText)) ) {

					switch( m_uAuthState ) {

						case 0:
							m_uAuthState = 1;
							break;

						case 1:
							m_uAuthState = 2;
							break;
					}

				}
				else {
					m_fGone  = TRUE;
					m_uState = smtpQuit;
				}
				break;

			default:
				m_fGone  = TRUE;
				m_uState = smtpQuit;
				break;
		}
	}
}

// Digest Authentication Method

CString CSmtpSession::MakeDigestReq(void)
{
	if( m_uAuthState == 0 ) {

		m_NonceCount++;

		m_Realm.Empty();

		m_Nonce.Empty();

		CString Req = "AUTH DIGEST-MD5";

		return Req;
	}

	if( m_uAuthState == 1 ) {

		m_NonceC = MakeNonce();

		CString Response = MakeResponse("AUTHENTICATE", m_NonceC);

		CString Req;

		if( m_fUseUTF8 ) {

			Req += "charset=utf-8,";
		}

		Req += CPrintf("username=\"%s\",", PCTXT(m_pConfig->m_User));

		Req += CPrintf("realm=\"%s\",", PCTXT(m_Realm));

		Req += CPrintf("nonce=\"%s\",", PCTXT(m_Nonce));

		Req += CPrintf("nc=%.8X,", m_NonceCount);

		Req += CPrintf("cnonce=\"%s\",", PCTXT(m_NonceC));

		Req += CPrintf("digest-uri=\"smtp/%s\",", PCTXT(m_Realm));

		Req += "serv-type=smtp,";

		Req += CPrintf("response=%s,", PCTXT(Response));

		Req += "maxbuf=1280,";

		Req += "qop=auth";

		return CBase64::ToBase64(Req);
	}

	if( m_uAuthState == 2 ) {

		CString Req;

		return Req;
	}

	return "";
}

BOOL CSmtpSession::TestDigestRep(CString Reply)
{
	CString qop;
	CString	Stale;
	CString Maxbuf;
	CString Charset;
	CString Algorithm;
	CString Cipher;
	CString Auth;

	while( !Reply.IsEmpty() ) {

		CString Name = Reply.StripToken('=');

		Name.Remove(',');

		Name.TrimBoth();

		CString Data;

		UINT uPos;

		if( (uPos = Reply.Find('"')) < NOTHING ) {

			Reply = Reply.Mid(uPos + 1);

			Data  = Reply.StripToken('"');
		}
		else
			Data  = Reply.StripToken(',');

		Data.TrimBoth();

		if( Name == "realm" ) m_Realm = Data;
		if( Name == "nonce" ) m_Nonce = Data;
		if( Name == "qop" ) qop = Data;
		if( Name == "stale" ) Stale = Data;
		if( Name == "maxbuf" ) Maxbuf = Data;
		if( Name == "charset" ) Charset = Data;
		if( Name == "algorithm" ) Algorithm = Data;
		if( Name == "cipher" ) Cipher = Data;
		if( Name == "rspauth" ) Auth = Data;
	}

	if( !Auth.IsEmpty() ) {

		CString Resp = MakeResponse("", m_NonceC);

		return Auth == Resp;
	}

	if( m_Nonce.IsEmpty() ) {

		return FALSE;
	}

	if( Algorithm.IsEmpty() ) {

		return FALSE;
	}

	if( Algorithm != "md5-sess" ) {

		return FALSE;
	}

	if( Charset == "utf-8" ) {

		m_fUseUTF8 = TRUE;
	}

	BOOL fAuth = TRUE;

	if( !qop.IsEmpty() ) {

		fAuth = FALSE;
	}

	while( !qop.IsEmpty() ) {

		CString Opt = qop.StripToken(',');

		Opt.TrimBoth();

		if( Opt == "auth" ) {

			fAuth = TRUE;

			break;
		}
	}

	return fAuth;
}

CString CSmtpSession::MakeNonce(void)
{
	UINT  uTime   = GetTickCount();

	DWORD Data[6] = { 0 };

	m_pCmdSock->GetRemote((IPADDR &) Data[0]);

	m_pCmdSock->GetLocal((IPADDR &) Data[1]);

	Data[2] = uTime;

	Data[3] = GetTickCount();

	Data[4] = m_NonceCount;

	Data[5] = 0;

	return MD5(Data, sizeof(Data));
}

CString CSmtpSession::MakeResponse(PCTXT pAuth, CString NonceC)
{
	CString S1 = CPrintf("%s:%s:%s",
			     PCTXT(m_pConfig->m_User),
			     PCTXT(m_Realm),
			     PCTXT(m_pConfig->m_Pass));

	CString S2 = CPrintf(":%s:%s",
			     PCTXT(m_Nonce),
			     PCTXT(NonceC)
	);

	char bA1[256] = { 0 };

	MD5(S1, PBYTE(bA1));

	strcpy(bA1 + 16, S2);

	CString H1 = MD5(bA1, 16 + S2.GetLength());

	CString A2 = CPrintf("%s:smtp/%s",
			     pAuth,
			     PCTXT(m_Realm)
	);

	CString H2 = MD5(A2);

	CString D1 = CPrintf("%s:%s:%.8X:%s:auth:%s",
			     PCTXT(H1),
			     PCTXT(m_Nonce),
			     m_NonceCount,
			     PCTXT(NonceC),
			     PCTXT(H2)
	);

	return MD5(D1);
}

// Login Authentication Method

CString CSmtpSession::MakeLoginReq(void)
{
	if( m_uAuthState == 0 ) {

		CString Init = CBase64::ToBase64(m_pConfig->m_User);

		CString  Req = CPrintf("AUTH LOGIN %s", PCTXT(Init));

		return Req;
	}

	if( m_uAuthState == 1 ) {

		CString  Req = CBase64::ToBase64(m_pConfig->m_Pass);

		return Req;
	}

	return "";
}

BOOL CSmtpSession::TestLoginRep(CString Reply)
{
	switch( m_uReplyCode ) {

		case 334:
			if( CBase64::ToAnsi(Reply) == "Password:" ) {

				m_uAuthState = 1;

				break;
			}

			m_fGone  = TRUE;
			m_uState = smtpQuit;
			break;

		case 235:
			return TRUE;

		default:
			m_fGone  = TRUE;
			m_uState = smtpQuit;
			break;
	}

	return FALSE;
}

// MD5 Helpers

CString CSmtpSession::MD5(CString Data)
{
	return MD5(PCTXT(Data), Data.GetLength());
}

CString CSmtpSession::MD5(PCVOID pData, UINT uSize)
{
	BYTE bHash[16];

	char sHash[33];

	MD5(PBYTE(pData), uSize, bHash);

	PCTXT pHex = "0123456789abcdef";

	UINT  n, c;

	for( n = 0, c = 0; n < 16; n++ ) {

		sHash[c++] = pHex[bHash[n]/16];

		sHash[c++] = pHex[bHash[n]%16];
	}

	sHash[c++] = 0;

	return sHash;
}

void CSmtpSession::MD5(CString Data, PBYTE pHash)
{
	MD5(PCTXT(Data), Data.GetLength(), pHash);
}

void CSmtpSession::MD5(PCVOID pData, UINT uSize, PBYTE pHash)
{
	::md5(PBYTE(pData), uSize, pHash);
}

// End of File
