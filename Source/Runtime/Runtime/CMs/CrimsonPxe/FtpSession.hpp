
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_FtpSession_HPP

#define	INCLUDE_FtpSession_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "StdServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// FTP Config
//

struct CFtpConfig
{
	UINT m_uAnon;
	UINT m_uTlsMode;
	UINT m_uCmdPort;
};

//////////////////////////////////////////////////////////////////////////
//
// FTP Session
//

class CFtpSession : public CStdServer
{
public:
	// Constructor
	CFtpSession(void);

	// Destructor
	~CFtpSession(void);

	// Operations
	void SetConfig(CFtpConfig const *pConfig);
	BOOL Poll(void);

protected:
	// Session States
	enum
	{
		stateWaitRouter,
		stateMakeSocket,
		stateWaitSocket,
		stateLogonUser,
		stateLogonPass,
		stateActive
	};

	// Data Members
	CFtpConfig const * m_pConfig;
	CString		   m_Prefix;
	CString		   m_Local;
	UINT	           m_uState;
	UINT	           m_uTime;
	BOOL		   m_fTlsData;
	BOOL	           m_fASCII;
	BOOL	           m_fRecord;
	BOOL	           m_fAnon;
	BOOL	           m_fWrite;
	UINT		   m_uRestart;
	UINT	           m_uPort;
	ISocket		 * m_pDataSock;
	char	           m_sUser[64];
	char	           m_sPass[64];
	CString		   m_Rename;

	// Socket Management
	BOOL OpenDataSocket(UINT uPort, BOOL fSend);
	BOOL WaitDataSocket(void);
	void CloseDataSocket(void);
	void AbortDataSocket(void);

	// Session Control
	void InitSession(void);
	void ClearSession(void);
	void KillSession(void);

	// Command Processing
	void OnCommand(void);
	void OnAuth(PTXT pLine);
	void OnPBSZ(PTXT pLine);
	void OnProt(PTXT pLine);
	void OnUser(PTXT pLine);
	void OnPass(PTXT pLine);
	void OnQuit(PTXT pLine);
	void OnRein(PTXT pLine);
	void OnSyst(PTXT pLine);
	void OnFeat(PTXT pLine);
	void OnPort(PTXT pLine);
	void OnPasv(PTXT pLine);
	void OnType(PTXT pLine);
	void OnMode(PTXT pLine);
	void OnStru(PTXT pLine);
	void OnRest(PTXT pLine);
	void OnRetr(PTXT pLine);
	void OnStor(PTXT pLine);
	void OnSize(PTXT pLine);
	void OnMDTM(PTXT pLine);
	void OnDele(PTXT pLine);
	void OnNoop(PTXT pLine);
	void OnMKD(PTXT pLine);
	void OnRMD(PTXT pLine);
	void OnPWD(PTXT pLine);
	void OnCWD(PTXT pLine);
	void OnCDUP(PTXT pLine);
	void OnList(PTXT pLine);
	void OnRnFr(PTXT pLine);
	void OnRnTo(PTXT pLine);

	// Implementation
	void    FindLocalPrefix(void);
	CString FindLocalPrefix(CString &Prefix);
	BOOL    FileToLocal(PTXT pPath);
	void    StripOptions(PTXT &pLine);
	BOOL    SendDirectory(PTXT pPath);
	PCTXT   GetMonthName(UINT uMonth);
	void    NotifyConnection(void);
	void    NotifyComplete(void);
};

// End of File

#endif
