
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PxeHttpServer_HPP

#define	INCLUDE_PxeHttpServer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPxeHttpServerSession;

class CPxeWebServer;

//////////////////////////////////////////////////////////////////////////
//
// Web Request Context
//

struct CWebReqContext
{
	CPxeHttpServerSession * pSess;
	CHttpServerConnection * pCon;
	CHttpServerRequest    * pReq;
};

//////////////////////////////////////////////////////////////////////////
//
// Web Server Object
//

class CPxeHttpServer : public CHttpServer
{
public:
	// Constructor
	CPxeHttpServer(CPxeWebServer *pServer, CHttpServerManager *pManager, CHttpServerOptions const &Opts);

	// Destructor
	~CPxeHttpServer(void);

	// Operations
	void EnableRedirect(BOOL fEnable);

protected:
	// Data Members
	CPxeWebServer * m_pServer;
	BOOL		m_fRedirect;
	time_t		m_timeBoot;
	time_t		m_timeComp;
	UINT		m_uBackoff;

	// Overridables
	BOOL SkipAuth(CHttpServerRequest *pReq);
	void ServePage(CHttpServerSession *pSess, CConnectionInfo &Info, CHttpServerRequest *pReq);
	BOOL CatchPost(IHttpStreamWrite * &pStm, CHttpServerConnection *pCon, CHttpServerRequest *pReq, PCTXT pPath);
	void MakeSession(CHttpServerSession * &pSess);
	void KillSession(CHttpServerSession *pSess);
	BOOL UseForRedirect(UINT n, UINT &uPort, BOOL &fTls);

	// Implementation
	CString GetPath(CHttpServerRequest *pReq);
};

// End of File

#endif
