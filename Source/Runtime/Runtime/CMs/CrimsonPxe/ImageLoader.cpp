
#include "Intern.hpp"

#include "ImageLoader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PXE Image Loader
//

// Disable Hook

bool ImageDisable(char cTag)
{
	if( cTag == '*' ) {

		char tag[] = "dhspua";

		for( UINT n = 0; tag[n]; n++ ) {

			ImageDisable(tag[n]);
		}
	}
	else {
		CImageLoader Image;

		while( Image.Check(cTag) ) {

			Image.Disable();
		}
	}

	return true;
}

// Constructor

CImageLoader::CImageLoader(void)
{
	AfxGetAutoObject(pPlatform, "platform", 0, IPlatform);

	m_pSchema = NULL;

	m_pConfig = NULL;

	AfxGetObject("c3.schema-generator", 0, ISchemaGenerator, m_pSchema);

	AfxGetObject("c3.config-storage", 0, IConfigStorage, m_pConfig);

	m_Model = pPlatform->GetModel();

	m_Serial = pPlatform->GetSerial();

	m_Model.MakeUpper();

	m_Serial.MakeUpper();

	m_pIndex = NULL;

	m_pTargs = NULL;

	m_pFiles = NULL;

	m_cTag = 0;
}

// Destructor

CImageLoader::~CImageLoader(void)
{
	delete[] m_pTargs;

	delete[] m_pFiles;

	AfxRelease(m_pSchema);

	AfxRelease(m_pConfig);
}

// Operations

bool CImageLoader::Check(char cTag)
{
	if( !SkipCheck() ) {

		CString Path;

		for( UINT n = 0; FindPath(Path, cTag, n); n++ ) {

			CAutoFile File(Path, "r");

			if( File ) {

				AfxTrace("image file found at %s\n", PCSTR(Path));

				if( !CAutoFile(Path + ".d", "r") ) {

					bool hit = false;

					switch( cTag ) {

						case 'a':
						{
							hit = CheckApplication(File);
						}
						break;

						case 'd':
						{
							hit = CheckDeviceConfig(File);
						}
						break;

						case 'h':
						case 'p':
						case 's':
						case 'u':
						{
							hit = CheckConfigPart(File, cTag);
						}
						break;
					}

					if( hit ) {

						AfxTrace("image file will be applied\n");

						m_cTag = cTag;

						m_Path = Path;

						return true;
					}
					else {
						if( Path.StartsWith("C:\\") ) {

							CAutoFile File(Path + ".d", "w");
						}

						AfxTrace("image file skipped as it is unchanged\n");
					}
				}
				else {
					AfxTrace("image file skipped as disable file present\n");
				}
			}
		}
	}

	return false;
}

bool CImageLoader::Disable(void)
{
	AfxTrace("image file disabled for %s\n", PCSTR(m_Path));

	CAutoFile File(m_Path + ".d", "w");

	return true;
}

bool CImageLoader::Import(void)
{
	AfxTrace("image file import started\n");

	bool boot = false;

	bool kill = false;

	switch( m_cTag ) {

		case 'a':
			boot = LoadApplication(kill);
			break;

		case 'd':
			boot = LoadDeviceConfig();
			kill = true;
			break;

		case 'h':
		case 'p':
		case 's':
		case 'u':
			boot = LoadConfigPart();
			kill = true;
			break;
	}

	if( kill ) {

		#if !defined(AEON_PLAT_RLOS)

		if( m_Path.StartsWith("C:\\") ) {

			// We can disable files on internal drives because
			// they're not going to be reused, and we can avoid
			// having to run the check-for-change each time.

			Disable();
		}

		#endif
	}

	return boot;
}

// Implementation

bool CImageLoader::FindPath(CString &Path, char cTag, UINT n)
{
	switch( n ) {

		case 0:
		{
			Path.Printf("C:\\update\\%s-%s\\", PCSTR(m_Model), PCSTR(m_Serial));
		}
		break;

		case 1:
		{
			Path.Printf("D:\\update\\%s-%s\\", PCSTR(m_Model), PCSTR(m_Serial));
		}
		break;

		case 2:
		{
			Path.Printf("E:\\update\\%s-%s\\", PCSTR(m_Model), PCSTR(m_Serial));
		}
		break;

		case 3:
		{
			Path = "C:\\update\\";
		}
		break;

		case 4:
		{
			Path = "D:\\update\\";
		}
		break;

		case 5:
		{
			Path = "E:\\update\\";
		}
		break;

		default:
		{
			return false;
		}
	}

	switch( cTag ) {

		case 'a':
		{
			Path += "image.ci3";
		}
		return true;

		case 'd':
		{

			Path += "devcon.json";
		}
		return true;

		case 'h':
		case 'p':
		case 's':
		case 'u':
		{
			CString Name;

			m_pSchema->GetNaming(cTag, Name);

			Path += Name + ".json";
		}
		return true;
	}

	return false;
}

bool CImageLoader::CheckApplication(CAutoFile &File)
{
	if( File.Read(&m_Header, 1) ) {

		if( ValidateHeader() ) {

			UINT nt = m_Header.m_wTargCount;

			UINT nf = m_Header.m_wFileCount;

			m_pTargs = new CImageTargRecord[nt];

			m_pFiles = new CImageFileRecord[nf];

			if( File.Read(m_pTargs, nt) == nt ) {

				if( File.Read(m_pFiles, nf) == nf ) {

					for( UINT t = 0; t < nt; t++ ) {

						CImageTargRecord *pTarg = m_pTargs + t;

						if( m_Model == pTarg->m_sName ) {

							if( ValidateIndices(pTarg, nf) ) {

								if( ValidateCrc(File) ) {

									m_pIndex = pTarg->m_wFile;

									if( CheckBoot(m_pFiles + m_pIndex[1]) ) {

										return true;
									}

									if( CheckFirm(m_pFiles + m_pIndex[0]) ) {

										return true;
									}

									if( CheckDatabase(m_pFiles + m_Header.m_wDbase) ) {

										return true;
									}

									return false;
								}
							}
						}
					}
				}
			}

			delete m_pTargs;

			delete m_pFiles;

			m_pTargs = NULL;

			m_pFiles = NULL;
		}
	}

	AfxTrace("image file did not validate\n");

	return false;
}

bool CImageLoader::CheckDeviceConfig(CAutoFile &File)
{
	UINT uSize = File.GetSize();

	if( uSize < 256 * 1024 ) {

		m_Text = CString(' ', uSize);

		if( File.Read(PSTR(PCSTR(m_Text)), uSize) == uSize ) {

			if( m_Json.Parse(m_Text) ) {

				PCTXT tag = "hspu";

				bool  hit = false;

				for( UINT n = 0; n < 4; n++ ) {

					char       cTag = tag[n];

					CJsonData *pPart = m_Json.GetChild(CPrintf("%cconfig", cTag));

					if( pPart ) {

						CString Text = pPart->GetAsText(FALSE);

						CString Test;

						m_pConfig->GetConfig(cTag, Test);

						if( Text.CompareC(Test) ) {

							hit = true;
						}
					}
				}

				m_Text.Empty();

				return hit;
			}
		}

		m_Text.Empty();
	}

	return false;
}

bool CImageLoader::CheckConfigPart(CAutoFile &File, char cTag)
{
	UINT uSize = File.GetSize();

	if( uSize < 256 * 1024 ) {

		m_Text = CString(' ', uSize);

		if( File.Read(PSTR(PCSTR(m_Text)), uSize) == uSize ) {

			if( CheckType(m_Text, cTag) ) {

				CJsonData Json;

				if( Json.Parse(m_Text) ) {

					CString Test;

					if( m_pConfig->GetConfig(cTag, Test) ) {

						if( Json.GetAsText(FALSE).CompareC(Test) ) {

							return true;
						}
					}
				}
			}
		}

		m_Text.Empty();
	}

	return false;
}

bool CImageLoader::LoadApplication(bool &kill)
{
	bool fBoot = ApplyBoot(m_pFiles + m_pIndex[1]);

	bool fFirm = ApplyFirm(m_pFiles + m_pIndex[0]);

	if( !fBoot && !fFirm ) {

		ApplyDatabase(m_pFiles + m_Header.m_wDbase);

		kill = true;

		return false;
	}

	return true;
}

bool CImageLoader::LoadDeviceConfig(void)
{
	PCTXT tag = "hspu";

	for( UINT n = 0; n < 4; n++ ) {

		char       cTag = tag[n];

		CJsonData *pPart = m_Json.GetChild(CPrintf("%cconfig", cTag));

		if( pPart ) {

			CString Text = pPart->GetAsText(FALSE);

			m_pConfig->SetConfig(cTag, Text, true);
		}
	}

	return true;
}

bool CImageLoader::LoadConfigPart(void)
{
	m_pConfig->SetConfig(m_cTag, m_Text, true);

	return true;
}

bool CImageLoader::ValidateHeader(void)
{
	if( m_Header.m_dwMagic == 0x334943 && m_Header.m_wVersion == 0x000000 ) {

		if( !(m_Header.m_wFlags & 0x0002) ) {

			if( m_Header.m_wTargCount > 8 ) {

				return false;
			}

			if( m_Header.m_wFileCount > 32 ) {

				return false;
			}

			if( strcmp(m_Header.m_sOEM, "Red Lion Controls") ) {

				return false;
			}

			return true;
		}
	}

	return false;
}

bool CImageLoader::ValidateIndices(CImageTargRecord *pTarg, UINT nf)
{
	if( m_Header.m_wDbase < nf ) {

		for( UINT f = 0; f < elements(pTarg->m_wFile); f++ ) {

			if( pTarg->m_wFile[f] == 0xFFFF ) {

				continue;
			}

			if( pTarg->m_wFile[f] < nf ) {

				continue;
			}

			return false;
		}

		return true;
	}

	return false;
}

bool CImageLoader::ValidateCrc(CAutoFile &File)
{
	UINT  uSize = File.GetSize() - 4;

	int   fd    = fileno(File);

	UINT  uBuff = 1024 * 1024;

	DWORD calc = 0;

	DWORD test = 0;

	CAutoArray<BYTE> Data(uBuff);

	lseek(fd, 0, 0);

	while( uSize ) {

		UINT uLump = min(uSize, uBuff);

		UINT uRead = read(fd, PBYTE(Data), uLump);

		if( uRead == uLump ) {

			calc = crc(calc, Data, uRead);

			uSize = uSize - uRead;

			continue;
		}

		AfxTrace("failed during crc data read\n");

		return false;
	}

	if( read(fd, &test, sizeof(test)) == sizeof(test) ) {

		if( calc == test ) {

			return true;
		}

		AfxTrace("failed to match image file crc\n");

		return false;
	}

	AfxTrace("failed during crc test read\n");

	return false;
}

UINT CImageLoader::FindBootRevision(void)
{
	CAutoFile File("/./bin/osrev", "r");

	if( File ) {

		CString Line = File.GetLine();

		return atoi(Line);
	}

	return 1;
}

bool CImageLoader::CheckBoot(CImageFileRecord *pBoot)
{
	UINT uInstalled = FindBootRevision();

	UINT uAvailable = ((PDWORD) pBoot->m_bGUID)[0];

	if( uAvailable > uInstalled ) {

		return true;
	}

	return false;
}

bool CImageLoader::CheckFirm(CImageFileRecord *pFirm)
{
	AfxGetAutoObject(pProps, "c3.firmprops", 0, IFirmwareProps);

	if( memcmp(pProps->GetCodeVersion(), pFirm->m_bGUID, 16) ) {

		return true;
	}

	return false;
}

bool CImageLoader::CheckDatabase(CImageFileRecord *pData)
{
	AfxGetAutoObject(pDbase, "c3.database", 0, IDatabase);

	if( pDbase ) {

		CAutoFile File(m_Path, "r");

		if( File ) {

			BYTE  bGuid[16];

			DWORD dwTime = DWORD(File.GetUnix());

			DWORD dwLast = pDbase->GetRevision();

			pDbase->GetVersion(bGuid);

			if( memcmp(bGuid, pData->m_bGUID, 16) || dwTime > dwLast ) {

				return true;
			}
		}
	}

	return false;
}

bool CImageLoader::ApplyBoot(CImageFileRecord *pBoot)
{
	AfxTrace("checking bootloader\n");

	UINT uInstalled = FindBootRevision();

	UINT uAvailable = ((PDWORD) pBoot->m_bGUID)[0];

	if( uAvailable > uInstalled ) {

		AfxTrace("installing boot loader\n");

		AfxGetAutoObject(pPend, "c3.bootpend", 0, IFirmwareProgram);

		if( pPend ) {

			CAutoFile File(m_Path, "r");

			if( File ) {

				pPend->ClearProgram(0);

				File.Seek(pBoot->m_dwPos);

				UINT uSize = pBoot->m_dwSize;

				UINT uBuff = 32768;

				CAutoArray<BYTE> Data(uBuff);

				while( uSize ) {

					UINT uLump = min(uSize, uBuff);

					File.Read(Data, uLump);

					if( !pPend->WriteProgram(Data, uLump) ) {

						AfxTrace("boot loader write failed\n");

						return false;
					}

					uSize -= uLump;
				}

				if( pPend->WriteVersion(NULL) ) {

					if( pPend->StartProgram(0) ) {

						AfxTrace("boot loader installed\n");

						return true;
					}
				}
			}
		}

		AfxTrace("boot loader failed\n");
	}

	AfxTrace("no change to boot loader\n");

	return false;
}

bool CImageLoader::ApplyFirm(CImageFileRecord *pFirm)
{
	AfxTrace("checking firmware\n");

	AfxGetAutoObject(pProps, "c3.firmprops", 0, IFirmwareProps);

	if( memcmp(pProps->GetCodeVersion(), pFirm->m_bGUID, 16) ) {

		AfxTrace("installing firmware\n");

		AfxGetAutoObject(pPend, "c3.firmpend", 0, IFirmwareProgram);

		if( pPend ) {

			CAutoFile File(m_Path, "r");

			if( File ) {

				pPend->ClearProgram(0);

				File.Seek(pFirm->m_dwPos);

				UINT uSize = pFirm->m_dwSize;

				UINT uBuff = 32768;

				CAutoArray<BYTE> Data(uBuff);

				while( uSize ) {

					UINT uLump = min(uSize, uBuff);

					File.Read(Data, uLump);

					if( !pPend->WriteProgram(Data, uLump) ) {

						AfxTrace("firmware write failed\n");

						return false;
					}

					uSize -= uLump;
				}

				if( pPend->WriteVersion(pFirm->m_bGUID) ) {

					if( pPend->StartProgram(0) ) {

						AfxTrace("firmware installed\n");

						return true;
					}
				}
			}
		}

		AfxTrace("firmware failed\n");
	}

	AfxTrace("no change to firmware\n");

	return false;
}

bool CImageLoader::ApplyDatabase(CImageFileRecord *pData)
{
	AfxTrace("checking database\n");

	AfxGetAutoObject(pDbase, "c3.database", 0, IDatabase);

	if( pDbase ) {

		CAutoFile File(m_Path, "r");

		if( File ) {

			BYTE  bGuid[16];

			DWORD dwTime = DWORD(File.GetUnix());

			DWORD dwLast = pDbase->GetRevision();

			pDbase->GetVersion(bGuid);

			if( memcmp(bGuid, pData->m_bGUID, 16) || dwTime > dwLast ) {

				AfxTrace("clearing database\n");

				pDbase->SetValid(FALSE);

				pDbase->Clear();

				File.Seek(pData->m_dwPos);

				bool fInit = true;

				for( ;;) {

					CItemInfo Info = { 0 };

					File.Read(&Info.m_uItem, 1);

					if( !Info.m_uItem ) {

						if( !fInit ) {

							pDbase->SetVersion(pData->m_bGUID);

							pDbase->SetRevision(dwTime);

							pDbase->SetValid(TRUE);

							AfxTrace("database installed\n");

							return true;
						}

						fInit = false;
					}

					File.Read(&Info.m_uClass, 3);

					if( Info.m_uSize ) {

						CAutoArray<BYTE> Item(Info.m_uComp);

						File.Read(Item, Info.m_uComp);

						pDbase->WriteItem(Info, Item);
					}
					else {
						pDbase->WriteItem(Info, NULL);
					}
				}
			}
		}
	}

	AfxTrace("no change to database\n");

	return false;
}

bool CImageLoader::CheckType(CString const &Text, char cTag)
{
	CString key = "\"atype\":";

	UINT    f1 = Text.Find(key);

	if( f1 < NOTHING ) {

		UINT f2 = Text.Find('"', f1 + key.GetLength());

		if( f2 < NOTHING ) {

			if( Text[f2+1] == cTag && Text[f2+2] == '"' ) {

				return true;
			}
		}
	}

	return false;
}

bool CImageLoader::SkipCheck(void)
{
	#if defined(AEON_PLAT_LINUX)

	if( CAutoFile("\\.\\tmp\\debug", "r") ) {

		return true;
	}

	#endif

	return false;
}

// End of File
