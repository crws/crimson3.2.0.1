
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NandFirmwareProps_HPP

#define INCLUDE_NandFirmwareProps_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include <CxNand.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Nand Firmware Properties Object
//

class CNandFirmwareProps : public CNandClient, public IFirmwareProps
{
public:
	// Constructor
	CNandFirmwareProps(UINT uStart, UINT uEnd);

	// Destructor
	~CNandFirmwareProps(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IFirmwareProps
	bool   METHOD IsCodeValid(void);
	PCBYTE METHOD GetCodeVersion(void);
	UINT   METHOD GetCodeSize(void);
	PCBYTE METHOD GetCodeData(void);

protected:
	// Image Header
	struct CHead
	{
		BYTE m_bMagic[16];
		BYTE m_bGuid[16];
		UINT m_uImage;
	};

	// Data Members
	ULONG	  m_uRefs;
	bool	  m_fValid;
	CHead	  m_Head;
	CNandPage m_PageStart;
	UINT      m_uAlloc;
	PBYTE	  m_pImage;
	UINT	  m_uImage;

	// Magic Number
	static BYTE m_bMagic[16];

	// Implementation
	bool AllocImage(void);
	void CheckCode(void);
	bool ReadCode(void);
};

// End of File

#endif
