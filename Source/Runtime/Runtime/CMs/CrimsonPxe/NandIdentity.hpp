
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandIdentity_HPP

#define	INCLUDE_NandIdentity_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include <CxNand.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Identity Manager
//

class CNandIdentity :
	public CNandClient,
	public ICrimsonIdentity
{
public:
	// Constructor
	CNandIdentity(CNandBlock const &Start, CNandBlock const &End);

	// Destructor
	~CNandIdentity(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ICrimsonIdentity
	void METHOD Init(void);
	int  METHOD GetOemName(PTXT   pName, UINT uSize);
	int  METHOD GetOemApp(PTXT   pName, UINT uSize);
	bool METHOD GetKeyData(PBYTE  pData, UINT uSize);
	bool METHOD GetSerial(PTXT   pName, UINT uSize);
	bool METHOD SetOemName(PCTXT  pName, UINT uSize);
	bool METHOD SetOemApp(PCTXT  pName, UINT uSize);
	bool METHOD SetKeyData(PCBYTE pData, UINT uSize);
	bool METHOD SetSerial(PCTXT  pName, UINT uSize);

protected:
	// Constants
	static DWORD const magicOem   = 0x004F454D;
	static DWORD const magicEmpty = 0xFFFFFFFF;

	// Page Header
	struct CPageHeader
	{
		DWORD		Magic;
		DWORD		Generation;
	};

	// Property Page
	struct CPropsPage
	{
		CPageHeader	Page;
		char		App[64];
		char		Name[64];
		BYTE		Key[128];
		char            Serial[128];
	};

	// Type Definitions
	typedef	CPageHeader	    * PPAGE;
	typedef	CPageHeader   const * PCPAGE;
	typedef	CPropsPage          * PPROPS;
	typedef	CPropsPage    const * PCPROPS;

	// Data
	ULONG	  m_uRefs;
	CNandPage m_PageProps;
	UINT	  m_uPropsGen;
	char	  m_App[64];
	char	  m_Name[64];
	char      m_Serial[32];
	BYTE	  m_Key[128];

	// Implementation
	bool FindItems(void);
	bool CommitPropPage(void);

	// Overridables
	bool OnPageReloc(PCBYTE pData, CNandPage const &Dest);
};

// End of File

#endif
