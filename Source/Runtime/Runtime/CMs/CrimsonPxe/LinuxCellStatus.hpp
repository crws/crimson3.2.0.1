
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinuxCellStatus_HPP

#define INCLUDE_LinuxCellStatus_HPP

//////////////////////////////////////////////////////////////////////////
//
// Linux Cell Status
//

class CLinuxCellStatus :
	public IInterfaceStatus,
	public ICellStatus,
	public ILocationSource,
	public ITimeSource
{
public:
	// Constructor
	CLinuxCellStatus(PCTXT pName, UINT uInst);

	// Destructor
	~CLinuxCellStatus(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IInterfaceStatus
	BOOL METHOD GetInterfaceStatus(CInterfaceStatusInfo &Info);

	// ICellStatus
	BOOL METHOD GetCellStatus(CCellStatusInfo &Info);
	BOOL METHOD SendCommand(PCTXT pCmd);

	// ILocationSource
	BOOL METHOD GetLocationData(CLocationSourceInfo &Info);
	BOOL METHOD GetLocationTime(CTimeSourceInfo &Info);

	// ITimeSource
	BOOL METHOD GetTimeData(CTimeSourceInfo &Info);

protected:
	// Data Members
	ULONG               m_uRefs;
	IMutex		  * m_pLock;
	CString             m_Name;
	CString		    m_Face;
	UINT                m_uInst;
	DWORD	            m_timeStatus;
	DWORD	            m_timeLocation;
	DWORD	            m_timeTime;
	CCellStatusInfo	    m_CellInfo;
	CLocationSourceInfo m_LocationData;
	CTimeSourceInfo     m_LocationTime;
	CTimeSourceInfo     m_TimeData;

	// Implementation
	bool ReadJson(CJsonData &Json, PCTXT pType);
	void ReadStatus(void);
	void ReadLocation(void);
	void ReadTime(void);
};

// End of File

#endif
