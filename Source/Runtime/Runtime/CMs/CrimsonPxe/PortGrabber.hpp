
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PortGrabber_HPP

#define	INCLUDE_PortGrabber_HPP

//////////////////////////////////////////////////////////////////////////
//
// Port Grabber
//

class CPortGrabber : public IPortGrabber
{
	public:
		// Constructor
		CPortGrabber(void);

		// Destructor
		~CPortGrabber(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortGrabber
		BOOL METHOD HasRequest(void);
		BOOL METHOD Claim(void);
		void METHOD Free(void);

	protected:
		// Data
		ULONG	 m_uRefs;
		BOOL     m_fRequest;
		IMutex * m_pMutex1;
		IMutex * m_pMutex2;
	};

// End of File

#endif
