
#include "Intern.hpp"

#include "LinuxHardwareApplicator.hpp"

#include <fcntl.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Linux Hardware Applicator
//

// Instantiator

IHardwareApplicator * Create_LinuxHardwareApplicator(void)
{
	return new CLinuxHardwareApplicator;
}

// Constructor

CLinuxHardwareApplicator::CLinuxHardwareApplicator(void)
{
	m_pModel    = NULL;

	m_crcConfig = 0;

	AfxGetObject("c3.model", 0, IPxeModel, m_pModel);

	StdSetRef();
}

// Destructor

CLinuxHardwareApplicator::~CLinuxHardwareApplicator(void)
{
	AfxRelease(m_pModel);
}

// IUnknown

HRESULT CLinuxHardwareApplicator::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IHardwareApplicator);

	StdQueryInterface(IHardwareApplicator);

	return E_NOINTERFACE;
}

ULONG CLinuxHardwareApplicator::AddRef(void)
{
	StdAddRef();
}

ULONG CLinuxHardwareApplicator::Release(void)
{
	StdRelease();
}

// IHardwareApplicator

bool CLinuxHardwareApplicator::ApplySettings(CJsonConfig *pJson)
{
	if( CheckCRC(m_crcConfig, pJson) ) {

		/*if( m_fReboot ) {

			return false;
		}*/

		piob->RevokeGroup("pnp.");
	}

	if( pJson ) {

		ApplyPowerScheme(pJson);

		RegisterPorts();

		RegisterSleds(pJson);

		RegisterNics();
	}

	return true;
}

// Implementation

void CLinuxHardwareApplicator::ApplyPowerScheme(CJsonConfig *pJson)
{
	CJsonConfig *pBase = pJson->GetChild("base");

	if( pBase ) {

		ApplyCoreScheme(pBase->GetValueAsUInt("power", 0, 0, NOTHING));

		ApplyHaloScheme(pBase->GetValueAsUInt("leds", 0, 0, NOTHING));
	}
}

void CLinuxHardwareApplicator::ApplyCoreScheme(UINT uMode)
{
	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		static PCSTR List[] = {

			"performance",
			"powersave",
			"ondemand",
			"conservative"
		};

		if( uMode < elements(List) ) {

			PCSTR pPower = List[uMode];

			CPrintf Args("cpufreq_%s", pPower);

			if( pLinux->CallProcess("/sbin/modprobe", Args, NULL, NULL) == 0 ) {

				CPrintf Name("/./sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");

				int fd = open(Name, O_WRONLY | O_TRUNC);

				if( fd >= 0 ) {

					write(fd, pPower, strlen(pPower));

					close(fd);
				}
			}
		}
	}
}

void CLinuxHardwareApplicator::ApplyHaloScheme(UINT uMode)
{
	int fd = open("/./tmp/crimson/leds/LedManager.pipe", O_WRONLY);

	if( fd >= 0 ) {

		CPrintf Cmd("POWERSAVE-%u\n", uMode);

		write(fd, PCTXT(Cmd), Cmd.GetLength());

		close(fd);
	}
}

void CLinuxHardwareApplicator::RegisterPorts(void)
{
	UINT np = m_pModel->GetObjCount('s');

	UINT pp = 0;

	for( UINT n = 0; n <= np; n++ ) {

		/*if( m_fExtModem && m_uExtModem == n ) {

			continue;
		}*/

		AfxGetAutoObject(pPort, "uart", n, IPortObject);

		if( pPort ) {

			UINT uPort = (n < np) ? pp++ : 100;

			if( piob->RegisterSingleton("pnp.pnp-uart", uPort, pPort) == S_OK ) {

				pPort.TakeOver();
			}
		}
	}
}

void CLinuxHardwareApplicator::RegisterSleds(CJsonConfig *pJson)
{
	UINT np = m_pModel->GetObjCount('s');

	UINT ns = m_pModel->GetObjCount('x');

	if( ns ) {

		CJsonConfig *pSleds = pJson->GetChild("sleds.slist");

		if( pSleds ) {

			for( UINT s = 0; s < ns; s++ ) {

				CJsonConfig *pSled = pSleds->GetChild(s);

				if( pSled ) {

					UINT uType = pSled->GetValueAsUInt("type", 0, 0, NOTHING);

					if( uType == 103 || uType == 104 || uType == 105 ) {

						for( UINT p = 0; p < 2; p++ ) {

							IPortObject *pPort = NULL;

							AfxGetObject("dev.sled-uart", 2 * s + p, IPortObject, pPort);

							if( pPort ) {

								piob->RegisterSingleton("pnp.pnp-uart", np++, pPort);
							}
						}
					}
				}
			}
		}
	}
}

void CLinuxHardwareApplicator::RegisterNics(void)
{
	#if defined(AEON_PLAT_WIN32)

	// NOTE -- Only used when emulating Linux on Windows.

	UINT ne = m_pModel->GetObjCount('e');

	for( UINT n = 0; n < ne; n++ ) {

		AfxGetAutoObject(pNic, "nic", n, INic);

		if( pNic ) {

			if( piob->RegisterSingleton("pnp.pnp-nic", n, pNic) == S_OK ) {

				pNic.TakeOver();
			}
		}
	}

	#endif
}

BOOL CLinuxHardwareApplicator::CheckCRC(DWORD &crc, CJsonConfig * &pJson)
{
	DWORD c = pJson ? pJson->GetCRC() : 1;

	if( crc ) {

		if( crc == c ) {

			pJson = NULL;

			return FALSE;
		}

		crc = c;

		return TRUE;
	}

	crc = c;

	return FALSE;
}

// End of File
