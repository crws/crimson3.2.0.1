#!/bin/sh

# c3-show-filters <interface>
#
# List the MAC filter rules associated with this interface.

cat /vap/opt/crimson/config/macfilt/init | grep "\b$1\b"
