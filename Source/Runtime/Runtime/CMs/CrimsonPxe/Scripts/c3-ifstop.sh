#!/bin/sh

# c3-ifstop <interface>
#
# This script is called when an interface has stopped, which is to
# say that the physical link has gone down. The script runs the script
# created by the net applicator, which may remove allocated addresses
# and perhaps terminate the DHCP client.

test=/tmp/crimson/up/$1.start

if [ -f $test ]
then
	logger -t c3-net "$1 stopping"

	script=/vap/opt/crimson/config/net/$1.stop
	
	if [ -f $script ]
	then
		$script
	fi

	rm $test

	logger -t c3-net "$1 stopped"
fi
