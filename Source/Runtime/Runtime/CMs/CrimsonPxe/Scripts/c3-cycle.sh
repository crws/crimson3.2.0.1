#!/bin/sh

# c3-cycle
#
# Apply the configuration in /tmp/crimson/config and indicate
# whether any changes were present. The process is performed
# using the Flipper utility to minimize disk writes.

# LATER -- It would be nice to limit the reconfiguration
# to only those interfaces that have changed, but the logic
# might get a little complicated...

main()
{
	ptemp="/tmp/crimson/config"
	
	pconf="/vap/opt/crimson/config"
	
	if [ -d /tmp/crimson/config ]
	then
		if changed
		then
			if changed /net
			then
				log "full reconfig start"

				term_all

				swap

				/opt/crimson/scripts/c3-get-certs

				init_all
				
				log "full reconfig done"
			else
				log "partial reconfig start"
				
				check_term snmp
				check_term tlsc
				check_term tlss
				check_term routes
				check_term firewall
				check_term macfilt
				check_term dns
				check_term syslog

				swap

				check_init syslog
				check_init dns
				check_init macfilt
				check_init firewall
				check_init routes
				check_init tlss
				check_init tlsc
				check_init snmp

				log "partial reconfig done"
			fi

			exit 0
		else
			log "config is unchanged"
		
			rm -rf $ptemp
			
			exit 1
		fi
	else
		log "no config found"
		
		exit 2
	fi
}

check_init()
{
	if [ -d /tmp/crimson/config/$1 ]
	then
		name="run_$1"

		if [ "${!name}" == "yes" ]
		then
			init_one $1
		fi
	fi
}

check_term()
{
	if [ -d /tmp/crimson/config/$1 ]
	then
		if changed /$1
		then
			name="run_$1"

			printf -v "$name" '%s' 'yes'

			term_one $1
		fi
	fi
}

changed()
{
	log "check $ptemp$1 vs. $pconf$1"
	
	sum1=`getsum $ptemp$1`
	
	sum2=`getsum $pconf$1`
		
	if [ "$sum1" != "$sum2" ]
	then
		log "changed"

		return 0
	fi
		
	log "no change"

	return 1
}

getsum()
{
	(find $1 -type f -exec sha256sum {} \; | awk '{print $1}' | sort) | sha256sum
}

init_all()
{
	init_one syslog
	init_net 1
	init_one firewall
	init_one macfilt
	init_net 2
	init_one dns
	init_one routes
	init_one tlss
	init_one tlsc
	init_one snmp
}

init_net()
{
	log "init net $1"
	
	if [ "$1" == "1" ]
	then
		$pconf/net/init1
	else
		init_dep fast

		init_dep slow

		if [ -f $pconf/net/init2 ]
		then
			$pconf/net/init2
		fi
	fi
}

init_dep()
{
	/opt/crimson/scripts/c3-dep ifup $1
}

init_one()
{
	if [ -e $pconf/$1/init ]
	then
		log "init $1"
		
		$pconf/$1/init
	else
		log "init $1 skipped"
	fi
}

term_all()
{
	term_one snmp
	term_one tlsc
	term_one tlss
	term_one routes
	term_one dns
	term_net 1
	term_one macfilt
	term_one firewall
	term_net 2
	term_one syslog
}

term_net()
{
	if [ -e $pconf/net/deps ]
	then
		log "term net $1"
	
		if [ "$1" == "1" ]
		then
			$pconf/net/term1

			term_dep slow

			term_dep fast
		else
			if [ -f $pconf/net/term2 ]
			then
				$pconf/net/term2
			fi
		fi
	fi
}

term_dep()
{
	/opt/crimson/scripts/c3-dep ifdn $1
}

term_one()
{
	if [ -e $pconf/net/deps ]
	then
		if [ -e $pconf/$1/term ]
		then
			log "term $1"
	
			$pconf/$1/term
		else
			log "term $1 skipped"
		fi
	fi
}

swap()
{
	mkdir -p $pconf

	/opt/crimson/bin/Flipper $pconf $ptemp
}

log()
{
	logger -t c3-net "$*"
}

main $*
