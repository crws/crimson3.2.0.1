#!/bin/sh

# c3-ipt-event
#
# Process binding events for cellular interfaces taking part in IPT. We
# build the customized start and stop scripts for the bound interface so
# that it adopts the assigned gateway address, and we create the DHCPD
# configuration to hand out the assigned IP with a minimal netmask.

runBind()
{
	base="$interface"

	ip addr add $ip/$subnet dev $interface

	ping -c 1 -w 1 -I $interface $router

	ip addr flush dev $interface

	ip addr add $secaddr/$secmask peer $secgate dev $interface

	mkdir -p /tmp/crimson/face/$ipt

	file_start=/tmp/crimson/face/$ipt/start
	file_stop=/tmp/crimson/face/$ipt/stop
	file_dhcpd=/tmp/crimson/face/$ipt/dhcpd

	rm -f $file_start
	rm -f $file_stop
	rm -f $file_dhcpd

	# Start

	echo "ip addr add $router/$common dev $ipt" > $file_start

	echo "ip rule add prio $rule from $ip table c3-$ipt-in" >> $file_start
	
	echo "rm -f /vap/opt/crimson/dhcp/leases-$ipt" >> $file_start

	echo "/opt/crimson/bin/InitC32 start $ipt-dhcpd udhcpd -f -S -I $router $file_dhcpd" >> $file_start

	echo "/opt/crimson/scripts/c3-add-host $interface-client $ip" >> $file_start

	echo "/opt/crimson/scripts/c3-ifmake $ipt $router" >> $file_start

	# Stop

	echo "/opt/crimson/bin/InitC32 stop $ipt-dhcpd" > $file_stop

	echo "/opt/crimson/scripts/c3-ifbreak $ipt" >> $file_stop

	echo "/opt/crimson/scripts/c3-drop-host $interface-client" >> $file_stop

	echo "ip rule delete prio $rule" >> $file_stop

	# DCHP

	echo "interface $ipt" > $file_dhcpd

	echo "lease_file /vap/opt/crimson/dhcp/leases-$ipt" >> $file_dhcpd

	echo "start $ip" >> $file_dhcpd

	echo "end $ip" >> $file_dhcpd

	echo "max_leases 1" >> $file_dhcpd

	echo "opt subnet $common" >> $file_dhcpd

	echo "opt router $router" >> $file_dhcpd

	if [ -n "$dns" ]
	then
		echo "opt dns $dns" >> $file_dhcpd
	else
		echo "opt dns $router" >> $file_dhcpd
	fi

	echo "auto_time 60" >> $file_dhcpd

	echo "remaining no" >> $file_dhcpd

	echo "min_lease 240" >> $file_dhcpd

	echo "opt lease 240" >> $file_dhcpd

	# Done

	ip route flush table c3-$ipt-in

	ip route add default via $secgate dev $base metric $metric

	ip route add default via $secgate dev $base table c3-$ipt-in

	ip route add default via $secgate dev $base table c3-$base-out

	if [ -n "$dns" ]
	then
		/opt/crimson/scripts/c3-add-dns $base "$dns"
	fi

	export priaddr="$ip"

	/opt/crimson/scripts/c3-ifmake $base $secaddr $ip
}

main()
{
	if [ -z "$interface" ]
	then
		echo no interface
		exit 1
	fi

	if [ "$1" == "bound" ]
	then
		runBind $*
	fi

	exit 0
}

main $*
