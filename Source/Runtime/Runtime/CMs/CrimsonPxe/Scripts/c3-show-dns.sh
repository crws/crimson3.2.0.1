#!/bin/sh

# c3-show-dns
#
# List the DNS servers current used by the DNS Masquerade service.

cd /

static=`cat /vap/opt/crimson/config/dns/dns.conf | grep "server=" | wc -l`

if [ $static -eq 0 ]
then
	echo "<b>DHCP Name Servers</b><br/>"
	cat /tmp/crimson/dns/dhcp.conf
else
	echo "<b>Static Name Servers</b><br/>"
	cat /vap/opt/crimson/config/dns/dns.conf | grep "server="
fi
