#!/bin/sh

# c3-show-leases <interface>
#
# List the leases allocated by the interface's DHCP server.

if [ "$1" == "all" ]
then
	for file in /vap/opt/crimson/dhcp/leases-*
	do
		face="${file:29}"

		echo "<b>Leases for $face</b>"
		echo "<br/>"

		$0 $face

		if [ $? -eq 0 ]
		then
			echo "<br/>"
		fi
	done

	exit 0
else
	if [ -e /vap/opt/crimson/dhcp/leases-$1 ]
	then
		/opt/crimson/bin/InitC32 signal $1-dhcpd

		dumpleases -f /vap/opt/crimson/dhcp/leases-$1

		exit 0
	fi

	exit 2
fi
