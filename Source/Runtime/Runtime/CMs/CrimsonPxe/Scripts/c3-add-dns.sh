#!/bin/sh

# c3-add-dns <interface> <dns-servers>
#
# Add the DNS servers to the DNS Masquerade configuration file, tagging
# then with the indicated interface so that we can remove them later. We
# first remove any old entries for this interface, or that were placed
# there as default in the absence of any interface-supplied servers.

conf="/tmp/crimson/dns/dhcp.conf"

temp="/tmp/crimson/dns/dhcp.temp"

face="$1"

list="$2"

(
	flock -x 9
	
	cat $conf | grep -v "int_$face" | grep -v "int_default" > $temp

	for s in $list
	do
		echo "nameserver $s # int_$face" >> $temp
	done
		
	cp $temp $conf
	
	killall -q -SIGHUP dnsmasq

) 9> /var/lock/c3-dns
