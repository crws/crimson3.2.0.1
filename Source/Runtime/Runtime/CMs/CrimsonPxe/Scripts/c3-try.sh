#!/bin/bash

# c3-try <command>
#
# Try running a command up to five times. This is used for things
# like Dynamic DNS where we want to give the client at least a good
# chance of sending its data.

for i in {1..5}
do
	sleep 2

	$*

	if [ $? -eq 0 ]
	then
		exit 0
	fi

	sleep 2
done

exit 1
