#!/bin/bash
#
# chkconfig: - 15 90
# description: udev2
#
# processname: udev
# config: /etc/udev/udev2.conf

# source function library
. /etc/rc.d/init.d/functions

#inlucde FCs
. /etc/iog/model_features &>/dev/null

RETVAL=0

#DEBUG="--verbose"  #v110
#DEBUG="--debug"   #v138

PROG="udevd"
CONF_FILE="/etc/udev/udev.conf"

load_usb_drivers()
{
    #from usb.rc in hotplug
    modprobe -q usbcore >/dev/null 2>&1
    if [ -d /proc/bus/usb ]; then
        # if it's not mounted, try to mount it
        if [ ! -f /proc/bus/usb/devices ]; then
            if grep -q -E "^[^#][^[:space:]]+[[:space:]]+/proc/bus/usb/?[[:space:]]" /etc/fstab; then
                mount /proc/bus/usb
            else
                if grep -q usbfs /proc/filesystems; then
                    mount -t usbfs usbfs /proc/bus/usb
                else
                    mount -t usbdevfs usbdevfs /proc/bus/usb
                fi
            fi
        fi
    fi

    modprobe -q ehci-hcd >/dev/null 2>&1
    modprobe -q ohci-hcd >/dev/null 2>&1
    #usb-storage and airprime have "eject" sequences in them
    modprobe -q usb-storage >/dev/null 2>&1
    modprobe -q airprime >/dev/null 2>&1
    modprobe -q sierra >/dev/null 2>&1
    modprobe -q option >/dev/null 2>&1
    modprobe -q ftdi_sio >/dev/null 2>&1
    modprobe -q GobiSerial>/dev/null 2>&1
    #load GobiNet in the background because sometimes it stalls trying to load non-existant wan1
    modprobe -q GobiNet>/dev/null 2>&1 &
    #Telit modules use cdc_xxx
    modprobe -q cdc_ether &>/dev/null
    modprobe -q cdc_ncm &>/dev/null
    modprobe -q smsc95xx &>/dev/null
	modprobe -q cdc_mbim &>/dev/null
}

function load_wifi_drivers()
{
	if [ "0" == "$FC_WIFI" ]; then
		return
	fi
	local DRIVERS="ath9k_htc mac80211 cfg80211"
	for d in $DRIVERS; do
		modprobe -q $d &>/dev/null
	done
}

#
# Setup specific rules for the different model families.
#
function setup_udev_rules()
{
	rm -f /etc/udev/rules.d/*
        cd /lib/udev/rules.d
        ln -sf 14-sled.rules.$FC_MODEL_FAMILY 14-sled.rules
        ln -sf 15-eth.rules.$FC_MODEL_FAMILY 15-eth.rules
        ln -sf 21-xr.rules.$FC_MODEL_FAMILY 21-xr.rules
        ln -sf 21-ttyUSB_FW.rules.$FC_MODEL_FAMILY 21-ttyUSB_FW.rules
}

#use udev, not hotplug, turn it off in proc
sysctl  -w kernel.hotplug="/bin/true" >/dev/null 2>&1

RESTART=0

case "$1" in
  start)
	load_usb_drivers
        setup_udev_rules
	echo -n $"Starting $PROG: "
	pidof $PROG &> /dev/null
	RETVAL=$?
	if [ $RETVAL != 0 ]; then
            initlog -c "$PROG --daemon $DEBUG" && success || failure
	    RETVAL=$?
	    [ $RETVAL -eq 0 ] && touch /var/lock/subsys/$PROG

		#If restarting, don't retrigger everything
		if [ $RESTART -eq 0 ]; then
			#now tell udev to get USB events now that it missed
			#Maybe we can just leave off the  --subsystem-match= in the future to match "ALL" subsystems
			#Does not hurt if that system is not there
			#This also creates missing devices which might have been removed from a powerdown
			/sbin/udevadm trigger --action=add --subsystem-match=spidev
			/sbin/udevadm trigger --action=add --subsystem-match=block
			/sbin/udevadm trigger --action=add --subsystem-match=GobiQMI
			/sbin/udevadm trigger --action=add --subsystem-match=usb-serial
			/sbin/udevadm trigger --action=add --subsystem-match=stbus
			/sbin/udevadm trigger --action=add --subsystem-match=net
			/sbin/udevadm trigger --action=add --subsystem-match=usb
			#this one takes the longest
			/sbin/udevadm trigger --action=add --subsystem-match=tty
			#now power on the sled slots if needed
			( sledctl scan on &>/dev/null ) &
		fi

		#
		# start the sled watcher, exits if already running
		#
	        (sled_watcher &>/dev/null) &

	else
		failure
	fi

	echo

	#now load wifi drivers so the firmware can load
	load_wifi_drivers

	;;
  stop)
	echo -n $"Shutting down $PROG: "
	killproc $PROG
	RETVAL=$?
	[ $RETVAL -eq 0 ] && rm -f /var/lock/subsys/$PROG
	#
	killall -9 sled_watcher &>/dev/null
	rm -rf /var/lock/sled_watcher* &>/dev/null
	echo
	;;
  restart|reload)
	RESTART=1
	$0 stop
	$0 start
	RETVAL=$?
        ;;
  condrestart)
        if [ -f /var/lock/subsys/$PROG ]; then
                $0 stop
		$0 start
        fi
	RETVAL=$?
        ;;
  status)
        status $PROG
	RETVAL=$?
        ;;
  *)
	echo $"Usage: $0 {start|stop|restart|reload|condrestart|status}"
	exit 1
esac

exit $RETVAL
