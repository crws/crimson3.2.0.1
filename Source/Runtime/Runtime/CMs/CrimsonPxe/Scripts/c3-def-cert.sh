#!/bin/sh

# c3-def-cert
#
# Called when a new version of the default certificate
# is placed in /tmp/crimson/defcert. Right now, we just
# signal any stunnel instances to reload their config.

pkill -SIGHUP stunnel
