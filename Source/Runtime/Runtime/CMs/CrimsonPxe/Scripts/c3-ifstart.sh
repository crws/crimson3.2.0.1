#!/bin/sh

# c3-ifstart <interface>
#
# This script is called when an interface has started, which is to
# say that the physical link is up but that no IP address has yet
# been allocated. The script runs the script created by the net
# applicator, which may assign a static IP and run the c3-if-make
# script, or may start the DHCP client to obtain an address.

test=/tmp/crimson/up/$1.start

if [ ! -f $test ]
then
	logger -t c3-net "$1 starting"
	
	touch $test

	script=/vap/opt/crimson/config/net/$1.start
	
	if [ -f $script ]
	then
		$script $2
	fi

	logger -t c3-net "$1 started"
fi
