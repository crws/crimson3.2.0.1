#!/bin/sh

# c3-ifmake <interface> <address> [<real-ip>]
#
# This script is called when an interface obtain its IP address,
# typically because the DHCP client has received a lease or because
# a link with a static IP has started. The script adds the interface
# to the hosts files, run the optional interface-specific script
# created by the net applicator, and then starts any interfaces that
# are dependent on this one. The real-ip argument is optionally used
# in IP Transparency to pass the actual cellular IP, rather than the
# fake one assigned to this interface, as used by IPT Dynamic DNS.

test=/tmp/crimson/up/$1.make

if [ ! -f $test ]
then
	if [ "$3" == "" ]
	then
		logger -t c3-net "$1 making with ip of $2"
	else
		logger -t c3-net "$1 making with ip of $2 / $3"
	fi
	
	touch $test

	/opt/crimson/scripts/c3-add-host $1 $2

	script=/vap/opt/crimson/config/net/$1.make
	
	if [ -f $script ]
	then
		$script $2 $3
	fi

	ip route flush cache

	if [ -f /vap/opt/crimson/config/routes/init ]
	then
		/vap/opt/crimson/config/routes/init
	fi

	/opt/crimson/scripts/c3-dep ifup $1 $2

	logger -t c3-net "$1 made"
fi
