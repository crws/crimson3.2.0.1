#!/bin/sh

# c3-show-ipsec <interface>
#
# Show the IPSec status of the specified interface.

ipsec whack --status | grep \"$1 | cut -f 2- -d : | sed "s/^[ ]//"
