#!/bin/sh

# c3-ifdn <interface>
#
# This script is called to take down the specified interface
# by running the script created by the net applicator. This
# script may configure the interface and indicate that it
# is started, or it may start a Watcher to manage the link.

test=/tmp/crimson/up/$1.up

if [ -f $test ]
then
	logger -t c3-net "taking down $1"

	script=/vap/opt/crimson/config/net/$1.dn

	if [ -f $script ]
	then
		$script $1
	fi

	rm $test
fi
