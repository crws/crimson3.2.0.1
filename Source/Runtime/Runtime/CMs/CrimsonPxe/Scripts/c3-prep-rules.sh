#!/bin/sh

# c3-prep-rules <face-list>
#
# Remove any existing ip rules that we have created, and
# then update the routing table list to include a table
# for each of the interfaces in the list. This allows us
# to add routing rules to each if we need to.

clear_rules()
{
        IFS=$'\n'

        for line in `ip rule`
        do
                prio=${line%%:*}

                if [ $prio -ge 1000 ] && [ $prio -lt 3999 ]
                then
                        ip rule delete prio $prio
                fi
        done

        IFS=$' '

        ip route flush cache
}

make_tables()
{
        table=/etc/iproute2/rt_tables

        echo "255 local"    > $table
        echo "254 main"    >> $table
        echo "253 default" >> $table
        echo "0 unspec"    >> $table

        tid=100

        for face in $1
        do
               echo "$tid c3-$face-in" >> $table

               tid=$((tid+1))

               echo "$tid c3-$face-out" >> $table

               tid=$((tid+1))
        done

        ip route flush cache

        for face in $1
        do
                ip route flush table c3-$face-in

                ip route flush table c3-$face-out
        done

        ip route flush cache
}

main()
{
        clear_rules
        
        make_tables "$1"
}

main "$1"
