#!/bin/sh

# c3-get-certs
#
# Install the certificates from the configuration into the NSS
# database used by IPSec, converting the local cert to PKCS12
# and setting the appropriate trust settings.

main()
{
	fold=/vap/opt/crimson/config/ipsec

	work=/tmp/crimson/certs

	rm    -rf $work
	
	mkdir -p  $work

	rm -rf /etc/ipsec.d/*.db

	ipsec initnss

	if ls $fold/*-loc.crt 1> /dev/null 2>&1
	then
		for file in $fold/*-loc.crt
		do
			base=`basename $file`
	
			face=${base%%-*}
	
			cat $fold/$face-loc.key $fold/$face-loc.crt > $work/$face-loc.txt
	
			ipass=`cat $fold/$face-loc.pwd`
	
			if [ "$ipass" == "" ]
			then
				opass="password"
			else
				opass="$ipass"
			fi
	
			openssl pkcs12	-export				\
					-descert			\
					-in $work/$face-loc.txt		\
					-out $work/$face-loc.p12	\
					-passin pass:$ipass		\
					-passout pass:$opass		\
					-name $face-loc
	 
			pk12util -i $work/$face-loc.p12 -d sql:/etc/ipsec.d -W $opass

			certutil -M -d sql:/etc/ipsec.d -n $face-loc -t 'u,u,u'

			certutil -A -a -i $fold/$face-rem.crt -d sql:/etc/ipsec.d -n $face-rem -t 'CT,,'

			log "imported certs for $face"
		done
	fi
}

log()
{
	logger -t c3-net "$*"
}

main $*
