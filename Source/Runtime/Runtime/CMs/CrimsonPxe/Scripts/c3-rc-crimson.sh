#!/bin/bash
#
# chkconfig: - 10 80
# description: crimson
#

# c3-rc
#
# Crimson startup script that gets copied into /etc/init.d by the
# PXE to ensure that we've always got the right file installed.

. /etc/rc.d/init.d/functions

RETVAL=0

case "$1" in
  
	start)
  		echo -n "Starting crimson:"

		modprobe -r g_ether

		modprobe    g_serial

		mkdir -p /tmp/core

		echo "/tmp/core/%p.dump" > /proc/sys/kernel/core_pattern

		ulimit -S -c unlimited

		ulimit -c unlimited

		rm -f /etc/localtime
		
		ln -s /usr/share/zoneinfo/UTC /etc/localtime

		mkdir -p /vap/opt/crimson/disk/C

		if [ ! -L /vap/opt/crimson/disk/D ]
		then
			ln -s /media/usb-storage /vap/opt/crimson/disk/D
		fi

		if [ ! -L /vap/opt/crimson/disk/E ]
		then
			ln -s /media/sdcard /vap/opt/crimson/disk/E
		fi

		/opt/crimson/bin/InitC32 start-service

		/opt/crimson/bin/InitC32 start led LedManager

		/opt/crimson/bin/InitC32 start log PipeLog

		/opt/crimson/bin/InitC32 start c32 LinuxHost -service CrimsonPxe

  		if [ -e /opt/crimson/scripts/c3-init ]
		then
			/opt/crimson/scripts/c3-init
		fi

  		success
  		RETVAL=0
		cd 
  		;;
  
	stop)
		echo -n "Stopping crimson:"

  		if [ -e /opt/crimson/scripts/c3-term ]
		then
			/opt/crimson/scripts/c3-term
		fi

		/opt/crimson/bin/InitC32 stop-service

		success
  		RETVAL=0
		;;
	
  	restart|reload)
		RESTART=1
		$0 stop
		$0 start
		RETVAL=$?
    	;;
    
	*)
		echo $"Usage: $0 {start|stop|restart}"
		exit 1

esac

exit $RETVAL
