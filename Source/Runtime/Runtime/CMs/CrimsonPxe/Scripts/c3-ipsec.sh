#!/bin/sh

# c3-ipsec <start|stop>
#
# Bring up or take down the IPSec intrastructure.

pid=/var/run/pluto/pluto.pid
ctl=/var/run/pluto/pluto.ctl

if [ "$1" == "start" ]
then
	$0 stop

	mkdir -p /var/run/pluto

	if [ ! -e /etc/ipsec.d/key4.db ]
	then
		ipsec --initnss
	fi

	ipsec pluto --config /vap/opt/crimson/config/ipsec/config --secretsfile /vap/opt/crimson/config/ipsec/secrets

	if [ $? -eq 0 ]
	then
		while true
		do
			if [ -e $ctl ]
			then
				break
			fi
		done

		sleep 1
	fi
fi

if [ "$1" == "stop" ]
then
	if [ -e $pid ]
	then
		ipsec whack --shutdown

		if [ $? -eq 0 ]
		then
			for i in {1..25}
			do
				if [ ! -e $ctl ]
				then
					exit 0
				fi
			done

			sleep 0.2
		fi

		kill `cat $pid`

		sleep 2

		rm -f $pid
		rm -f $ctl
	fi
fi
