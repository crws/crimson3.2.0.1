#!/bin/sh

# c3-dhcp-event
#
# Process DHCP events for the specified interface. This script is called
# both from the DHCP client daemon, but also from the cell modem manager
# to apply the IP address found during the link process. The script sets
# the interface's address, and also adds the interface and optionally the
# default routes. We do these manually to allow the metric to be set.

checkAlias()
{
	base="$interface"
		
	config="/vap/opt/crimson/config/net/$interface-conf"

	if [ -e $config ]
	then
		other=`cat $config | grep "base=" | cut -d= -f2`

		if [ "$other" != "" ]
		then
			base="$other"

			config="/vap/opt/crimson/config/net/$base-conf"
		fi
	fi
}

runAction()
{
	case "$1" in

		deconfig)

			checkAlias

			ip addr flush dev $base

			if [ -e $config ]
			then
				rule=`cat $config | grep "rule=" | cut -d= -f2`

				if [ "$rule" != "" ]
				then
					ip rule delete prio $rule
				fi
			fi
		
			/opt/crimson/scripts/c3-ifbreak $base

			;;

		renew|bound)

			checkAlias

			metric="1"

			if [ -e $config ]
			then
				metric=`cat $config | grep "metric=" | cut -d= -f2`

				rule=`cat $config | grep "rule=" | cut -d= -f2`
			fi

			if [ "$1" == "bound" ]
			then
				IFS=. read -r i1 i2 i3 i4 <<< "$ip"

				IFS=. read -r m1 m2 m3 m4 <<< "$subnet"

				net="$((i1 & m1)).$((i2 & m2)).$((i3 & m3)).$((i4 & m4))"

				ip addr flush dev $interface

				ip addr add $ip/$subnet dev $interface noprefixroute

				ip route add $net/$subnet dev $interface metric $metric

				if [ "$rule" != "" ]
				then
					logger "ip rule add to $net/$subnet lookup main prio $rule"

					ip rule add to $net/$subnet lookup main prio $rule
				fi
			fi
		
			if [ "$1" == "bound" ]
			then
				while ip route show | grep default | grep $interface
				do
					ip route del default dev $interface
				done

				if [ -n "$router" ]
				then
					if [ "$defaultroute" != "no" ]
					then
						sym=0

						tab="table main"

						if [ -e $config ]
						then
							sym=`cat $config | grep "sym=" | cut -d= -f2`
						fi

						for i in $router
						do
							while ip route show metric $metric | grep default
							do
								ip route del default metric $metric
							done

							ip route add default via $i metric $((metric++)) dev $interface
						done
					
						if [ $sym -ne 0 ]
						then
							tab="table c3-$interface-out"

							metric=0

							for i in $router
							do
								while ip route show metric $metric $tab | grep default
								do
									ip route del default metric $metric $tab
								done

								ip route add default via $i metric $((metric++)) dev $interface $tab
							done
						fi
					fi
				fi
			fi
		
			if [ -n "$dns" ]
			then
				/opt/crimson/scripts/c3-add-dns $base "$dns"
			fi

			if [ "$1" == "bound" ]
			then
				/opt/crimson/scripts/c3-ifmake $base $ip
			fi
		
			;;
	esac
}

main()
{
	if [ -z "$interface" ]
	then
		echo no interface
		exit 1
	fi
	
	runAction $*

	exit 0
}

main $*
