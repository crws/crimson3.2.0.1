#!/bin/sh

# c3-make-cert <type> <cn> <password>
#
# Create a self-signed certificate, optionally with a
# password, or a shared secret to be used with OpenVPN.

main()
{
	work=/tmp/crimson/mkcert

	rm    -rf $work
	mkdir -p  $work

	if [ "${1:0:4}" == "x509" ]
	then
		conf=$work/config

		echo "[req]"							 > $conf
		echo "prompt=no"						>> $conf
		echo "distinguished_name=dn"					>> $conf
		echo ""								>> $conf
		echo "[dn]"							>> $conf
		echo "commonName=$2"						>> $conf
		echo ""								>> $conf
		echo "[ext]"							>> $conf
		echo "subjectKeyIdentifier=hash"				>> $conf
		echo "authorityKeyIdentifier=keyid:always,issuer"		>> $conf
		echo "keyUsage=digitalSignature,keyEncipherment,keyCertSign"	>> $conf
		echo "extendedKeyUsage=serverAuth,clientAuth"			>> $conf
		echo "basicConstraints=CA:TRUE"					>> $conf
	fi

	if [ "$1" == "x509" ]
	then
		tnow=`date +%s`

		days=`expr \( 2147483647 - $tnow \) / 86400`

		openssl req -x509 -config $conf -extensions ext -newkey rsa:1024 -keyout $work/output.key -out $work/output.crt -days $days -passout pass:$3
	fi

	if [ "$1" == "x509np" ]
	then
		tnow=`date +%s`

		days=`expr \( 2147483647 - $tnow \) / 86400`

		openssl req -x509 -config $conf -extensions ext -newkey rsa:1024 -keyout $work/output.key -out $work/output.crt -days $days -nodes 
	fi

	if [ "$1" == "ovs" ]
	then
		openvpn --genkey --secret $work/output.key
	fi
}

main $*
