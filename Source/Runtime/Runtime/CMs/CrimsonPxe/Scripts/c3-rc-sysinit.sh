#!/bin/bash

# (C)2018-2019 Red Lion Controls, Inc. All rights reserved. Red Lion, the Red Lion
# logo and Sixnet are registered trademarks of Red Lion Controls, Inc. All
# other company and product names are trademarks of their respective owners

# Warning: Making changes to this file
# can cause serious and unexpected results
# if improperly done.  This includes system
# failure to boot.

echo
date
echo

# Mount core system directories.
mount -t proc proc /proc
mount /
mount -t sysfs sys /sys

ldconfig
depmod -a

# "mount --all" equivalent - but purges directories first
for point in $(grep -vP '^\s*(#|$)' /etc/fstab | awk '{print $2}'); do
	if [ "$point" != "/" -a "$point" != "/proc" -a "$point" != "/sys" ]; then
		rm -rf "$point"
		mkdir -p "$point"
	fi
	mount "$point"
done

# This is our slice of the larger non-root partition for factory settings and
# reflash persistence.
FACTORY_DIR="/opt/.factory"
mkdir --parents "$FACTORY_DIR"

# Create old /storage, /vault, etc. directories as symlinks so we can find and
# migrate our utilities to the new factory dir.
make_old_root_dirs() {
    rm -rf /storage /vault /datalog /images
    for dir in storage vault datalog images; do
        mkdir --parents "$FACTORY_DIR/oldroot/$dir"
        ln -s "$FACTORY_DIR/oldroot/$dir" "/$dir"
    done
    # For older devices, these partitions could still exist, and we will want
    # their contents available.
    local ubiopts="-t ubifs -o noatime,defaults,sync"
    if [ -e /dev/ubi2_1 ]; then
        mount $ubiopts /dev/ubi2_1 "$FACTORY_DIR/oldroot/vault"
        # /opt replaces the /storage volume at ubi2_0. If any extra partitions
        # exist, /opt probably contains files we expect in /storage.
        rm /storage
        ln -s /opt /storage
    fi
    if [ -e /dev/ubi2_2 ]; then
        mount $ubiopts /dev/ubi2_2 "$FACTORY_DIR/oldroot/datalog"
    fi
    if [ -e /dev/ubi2_3 ]; then
        mount $ubiopts /dev/ubi2_3 "$FACTORY_DIR/oldroot/images"
    fi
}
make_old_root_dirs

PRINTENV_FILE=/var/log/bt_printenv
PRINTENV_EXE=/sbin/fw_printenv
SETENV_EXE=/sbin/fw_setenv
function Update_fwvars
{
	$PRINTENV_EXE > $PRINTENV_FILE 2> /dev/null
}

#fix mtab
if [ ! -h "/etc/mtab" ]; then
	rm -f /etc/mtab
	ln -sf /proc/mounts /etc/mtab
fi

# This was suspected of causing a corrupt file system, but it has not caused any
# problems and the documentation states that it is not destructive
# https://www.kernel.org/doc/Documentation/sysctl/vm.txt
sync; echo 3 > /proc/sys/vm/drop_caches 2> /dev/null

#Fix alignment issues
echo 2 > /proc/cpu/alignment 2> /dev/null

#check busybox's permissions
# the "su" applet requires sticky bit set on owner
if [ -e "/bin/busybox" ]; then
	ls -al /bin/busybox | /bin/grep "\-rwsr\-xr\-x" &> /dev/null
	if [ $? != "0" ]; then
		echo "Fixing busybox permissions to 4755"
		/bin/chmod 4755  /bin/busybox
	fi
fi

/bin/mkdir -p /var/run /var/log /var/lock /var/cron /var/empty /var/ftp
/bin/mkdir -p /var/lib/dhcp /var/run/netreport /var/run/profile /var/lib/pcmcia /var/spool /var/spool/uucp
/bin/touch /var/log/lastlog
/bin/touch /var/log/wtmp
/bin/touch /var/run/utmp
touch /var/lib/dhcp/dhcpd.leases
touch /var/lib/dhcp/dhcpd.leases~
ln -sf /tmp/ /var/tmp
ln -sf /dev/null /var/lib/dhcp/dhclient.leases
ln -sf /dev/null /var/lib/dhcp/dhclient.leases~
[ -d "/var/lost+found" ] && /bin/rm -rf /var/lost+found

#make dir after mounting
mkdir -p /var/lock/subsys

#Change permissions on datalog after mounting fstab
addgroup datalog &> /dev/null
chown root:datalog /datalog &> /dev/null
chmod 0775 /datalog &> /dev/null

Update_fwvars
# Populate skvs database for anything run before prestart (requires fwvars)
/etc/prestart/01_skvs_model.pl

BTMODEL=`/usr/iog/bin/skvs get MODEL_NUMBER`
BTSERIAL=`cat $PRINTENV_FILE | grep btserial | LANG=C awk 'BEGIN { FS = "=" } ; { print $2 }'`

#link the gwlnx logfile for easy access
if ! [ -L /home/jbmgatew/logfile ]; then
	ln -s -f /tmp/logfile /home/jbmgatew/logfile > /dev/null 2>&1
fi


#setup DIRS for cron on the flash in the /root
#dir so they are saved
if ! [ -d /root/cron ]; then
	mkdir /root/cron
	mkdir /root/cron/tabs
fi
if ! [ -d /root/cron/tabs ]; then
	mkdir /root/cron/tabs
fi
#link the tabs DIR to the one in the /root DIR
if ! [ -L /var/cron/tabs ]; then
	ln -s -f /root/cron/tabs /var/cron/tabs
fi
#link /var/cron/log -> /dev/null
# so cron logs go into the bit bucket
if ! [ -L /var/cron/log ]; then
	ln -s -f /dev/null /var/cron/log
fi

# Create dpkg database infrastructure
# Package database should be preserved across reboot, but not firmware upgrade
local_dpkg="/usr/lib/dpkg"
if [ ! -d "$local_dpkg" ]; then
	mkdir -p "$local_dpkg/info"
	touch "$local_dpkg/status"
fi
mkdir -p /var/lib/dpkg
mount --bind "$local_dpkg" /var/lib/dpkg

#factoryinit="$(awk -F= '/^factoryinit=/{print $2}' "$PRINTENV_FILE")"
#if [ "$factoryinit" ]; then
#	echo "Factorytest mode detected. Switching to factoryinit"
#	exec /etc/rc.d/rc.factoryinit "$factoryinit"
#fi

#
#rc.sysinit stuff below
#
# /etc/rc.sysinit - run once at boot time
#
# Taken in part from Miquel van Smoorenburg's bcheckrc.
#

# Parse model (again) after everything has been mounted.
/etc/prestart/01_skvs_model.pl

# Set the path
. /etc/iog.system/env

BUILD_VERSION=$(/bin/build_version 2>&1)
if [ $? = 0 ]; then
	echo "Build version $BUILD_VERSION"
fi


# Read in config data.
if [ -f /etc/sysconfig/network ]; then
	. /etc/sysconfig/network
else
	NETWORKING=no
fi

if [ -f "/etc/hostname" ]; then
	HOSTNAME="$(cat /etc/hostname)"
fi

echo

if [ -z "$HOSTNAME" -o "$HOSTNAME" = "(none)" ]; then
	HOSTNAME=SNgateway
	echo "HOSTNAME set to $HOSTNAME"
else
	echo "HOSTNAME set to $HOSTNAME"
fi

#Now actually SET the hostname
hostname $HOSTNAME


# Source functions
. /etc/rc.d/init.d/functions

echo -en $"\n\tPress 'I' now to enter Interactive startup.\n"

#getkey has been known to 'hang', kill it after 5 seconds
{
sleep 5
killall -TERM getkey >/dev/null 2>&1
if [ 0 = $? ]; then
	echo "*** Killed stale getkey #1 ***"
fi
sleep 5
}&

/sbin/getkey -c 1 i &> /dev/null && touch /var/run/confirm
echo

# Fix console loglevel
/bin/dmesg -n $LOGLEVEL

# The root filesystem is now read-write, so we can now log via syslog() directly..
if [ -n "$IN_INITLOG" ]; then
	IN_INITLOG=
fi

#turn it on anyways
USEMODULES=y

#embedded box, so don't write dep files every boot
#to keep wear leveling down on the ROMFS
#kernelversion=`uname -r`
#if ! [ -f /lib/modules/$kernelversion/modules.dep ]; then
#    INITLOG_ARGS= action $"Finding module dependencies: " depmod -A
#fi
#changed 2/24/10 - go ahead and run it every bootup
INITLOG_ARGS= action $"Finding module dependencies: " depmod -A


#load the Feature Code /proc Driver
echo -n "Loading the FC driver ... "
for i in 10 9 8 7 6 5 4 3 2 1; do
	modprobe jbm_feature modelno="$BTMODEL" serialno="$BTSERIAL" &> /dev/null
	if [ 0 = $? ]; then
		break
	fi
	sleep 1
done

if [ ! -d /proc/feature_codes/ ]; then
	echo "ERROR!"
	echo
	echo "********************************************* "
	echo "*** Unable to load the Feature Driver     *** "
	echo "***  Please contact Red Lion Support      *** "
	echo "***        - REBOOTING UNIT -             *** "
	echo "********************************************* "
	echo
	sync; reboot
else
	echo "done."
fi

#populate Feature Codes
/usr/iog/bin/getfc &>/dev/null
. /etc/iog/model_features &>/dev/null

# Load modules (for backward compatibility with VARs)
if [ -f /etc/rc.d/rc.modules ]; then
	sh /etc/rc.d/rc.modules
fi

# TODO - Test that IOBUS hardware is supported on platform
if [ -f /etc/init.d/iobus ]; then
    service iobus start
fi

#Default to backward compatibility
ROOT_VAULT_DIR="/storage/vault/"

#Setup some vault dirs
mkdir -p $ROOT_VAULT_DIR/system/logs/

if [ -e "/etc/rc.d/rc.jbm" ]; then
		sh /etc/rc.d/rc.jbm &> /dev/null &
fi

#Take initial copies of passwords
if [ ! -e /etc/factory/shadow ]
then
	mkdir -p /etc/factory
	cp -a /etc/passwd /etc/factory/
	cp -a /etc/shadow /etc/factory/
fi

#look for bxfactoryset=1 in /proc/cmdline
grep -q "bxfactoryset=1" /proc/cmdline &> /dev/null
if [ $? = 0 ]; then
	cp -a /etc/factory/passwd /etc/
	cp -a /etc/factory/shadow /etc/
	touch /etc/reset.me
	$SETENV_EXE bxfactoryset &> /dev/null
fi

#try to fix the time before firstboot
#turn off syslog and start it here, so firstboot and /etc/prestart scripts can syslog
chkconfig syslog off &> /dev/null
service syslog start

#Start clock after syslog so time/date setting from restore file can be logged
/etc/rc.d/init.d/clock start &> /dev/null

#work around for broken packages that hang. hit recovery and firstboot doesnt run
#if [ -e "/etc/.firstboot" ]; then
#	if [ -x "/etc/jbm/recovery/firstboot/firstboot" ]; then
#		/etc/jbm/recovery/firstboot/firstboot
#	else
#		echo "***"
#		echo "*** ERROR: Firstboot script missing, unable to setup environment"
#		echo "***"
#	fi
#fi

#Turn on the watchdog program incase it was disabled
chkconfig watchdog on &>/dev/null


# tweak isapnp settings if needed.
sysctl -w kernel.modprobe="/sbin/modprobe" >/dev/null 2>&1
sysctl -w kernel.hotplug="/bin/true" >/dev/null 2>&1


# Adjust symlinks as necessary in /boot to keep system services from
# spewing messages about mismatched System maps and so on.
if [ -L /boot/System.map -a -r /boot/System.map-`uname -r` -a \
	! /boot/System.map -ef /boot/System.map-`uname -r` ] ; then
	ln -s -f System.map-`uname -r` /boot/System.map
fi
if [ ! -e /boot/System.map -a -r /boot/System.map-`uname -r` ] ; then
	ln -s -f System.map-`uname -r` /boot/System.map
fi

# Now that we have all of our basic modules loaded and the kernel going,
# let's dump the syslog ring somewhere so we can find it later
dmesg -s 32768 > /var/log/dmesg

#syslog starts before running scripts
if [ -d "/etc/prestart" ]; then
	echo "Running prestart scripts"
	for SCRIPT in /etc/prestart/*; do
		[ -d $SCRIPT -o ! -x $SCRIPT ] && continue;
		logger -p security.notice -t "$0" "running script $SCRIPT"
		( $SCRIPT &> /dev/null ) &
		#run it in the background an wait a few seconds
		sleep 1
	done
fi

# BIG NOTE
# Anything run from this script gets killed after the runlevel changes, so use daemon() or
# make sure it is a short run/exit script/program

echo

# Warning: Making changes to this file
# can cause serious and unexpected results
# if improperly done.  This includes system
# failure to boot.
#
#
#               responsible for the very first setup of basic
#               things, such as setting the hostname.
#
# Original Author:
#               Miquel van Smoorenburg, <miquels@drinkel.nl.mugnet.org>
#

#set to anything to show time info per script
TIME=
#Change to yes for testing
BOOT_FLAG=yes

# Source function library.
. /etc/init.d/functions

#runlevel is always 3 on embedded boxes
runlevel=3


if [ -n "$TIME" ]; then
	TIME=time
fi

if [ -x "/etc/rc.d/init.d/sshd" ]; then
        /sbin/service sshd firstboot &> /dev/null
fi

# See if we want to be in user confirmation mode
if [ -f /var/run/confirm ]; then
   rm -f /var/run/confirm
   CONFIRM=yes
   export CONFIRM
   echo $"Entering interactive startup"
else
   echo $"Entering non-interactive startup"
fi


# Tell linuxconf what runlevel we are in
[ -d /var/run ] && echo "/etc/rc$runlevel.d" > /var/run/runlevel.dir

# Is there an rc directory for this new runlevel?
if [ -d /etc/rc.d/rc$runlevel.d ]; then
	# First, run the KILL scripts.
	#for i in /etc/rc$runlevel.d/K*; do
	#	# Check if the script is there.
	#	[ ! -f $i ] && continue
	#
	#	# Don't run [KS]??foo.{rpmsave,rpmorig} scripts
	#	[ "${i%.rpmsave}" != "${i}" ] && continue
	#	[ "${i%.rpmorig}" != "${i}" ] && continue
	#	[ "${i%.rpmnew}" != "${i}" ] && continue
	#
	#	# Check if the subsystem is already up.
	#	subsys=${i#/etc/rc$runlevel.d/K??}
	#	[ ! -f /var/lock/subsys/$subsys ] && \
	#	    [ ! -f /var/lock/subsys/${subsys}.init ] && continue
	#
	#	# Bring the subsystem down.
	#	if egrep -q "(killproc |action )" $i ; then
	#		$i stop
	#	else
	#		action $"Stopping $subsys: " $i stop
	#	fi
	#done

	# Now run the START scripts.
	for i in /etc/rc.d/rc$runlevel.d/S*; do
		# Check if the script is there.
		[ ! -f $i ] && continue

		# Don't run [KS]??foo.{rpmsave,rpmorig} scripts
		[ "${i%.rpmsave}" != "${i}" ] && continue
		[ "${i%.rpmorig}" != "${i}" ] && continue
		[ "${i%.rpmnew}" != "${i}" ] && continue

		# Check if the subsystem is already up.
		subsys=${i#/etc/rc.d/rc$runlevel.d/S??}
		[ -f /var/lock/subsys/$subsys ] || \
		    [ -f /var/lock/subsys/${subsys}.init ] && continue

		# If we're in confirmation mode, get user confirmation
		[ -n "$CONFIRM" ]  &&
		  {
		    confirm $subsys
		    case $? in
		      0)
		        :
		      ;;
		      2)
		        CONFIRM=
		      ;;
		      *)
		        continue
		      ;;
		    esac
		  }

		#signal gwlnx to play nice for a bit
		killall -q -USR2 gwlnx

		#allow other scripts to know this is a fresh boot
		export BOOTTIME="$BOOT_FLAG"

		# Bring the subsystem up.
		if egrep -q "(daemon |action )" $i ; then
			if [ "$subsys" = "halt" -o "$subsys" = "reboot" ]; then
				unset LANG
				unset LC_ALL
				unset TEXTDOMAIN
				unset TEXTDOMAINDIR
				exec $i start
			else
				$TIME $i start
			fi
		else
			if [ "$subsys" = "halt" -o "$subsys" = "reboot" -o "$subsys" = "single" -o "$subsys" = "local" ]; then
			    if [ "$subsys" = "halt" -o "$subsys" = "reboot" ]; then
				unset LANG
				unset LC_ALL
				unset TEXTDOMAIN
				unset TEXTDOMAINDIR
				exec $i start
			    fi
			    $i start
			else
			    action $"Starting $subsys: " $TIME $i start
			fi
		fi
	done
fi



#run user local apps
if [ -f /etc/rc.d/rc.local ]; then
        /bin/sh /etc/rc.d/rc.local &
fi
