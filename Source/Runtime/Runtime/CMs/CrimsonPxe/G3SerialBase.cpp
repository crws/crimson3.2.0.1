
#include "Intern.hpp"

#include "G3SerialBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ASCII Codes
//

#define	STX 0x02
#define	ETX 0x03
#define	ENQ 0x05
#define	ACK 0x06
#define	DLE 0x10
#define	NAK 0x15
#define	SYN 0x16

////////////////////////////////////////////////////////////////////////
//
// Serial Base Transport
//

// Constructor

CG3SerialBase::CG3SerialBase(UINT uLevel, ILinkService *pService)
{
	StdSetRef();

	m_uLevel    = uLevel;

	m_pService  = pService;

	m_hThread   = NULL;

	m_fInit	    = TRUE;
}

// Denstructor

CG3SerialBase::~CG3SerialBase(void)
{
}

// Operations

BOOL CG3SerialBase::Open(void)
{
	m_hThread = CreateClientThread(this, 0, m_uLevel);

	return TRUE;
}

void CG3SerialBase::Close(void)
{
	DestroyThread(m_hThread);
}

// IUnknown

HRESULT CG3SerialBase::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(ILinkTransport);

	return E_NOINTERFACE;
}

ULONG CG3SerialBase::AddRef(void)
{
	StdAddRef();
}

ULONG CG3SerialBase::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CG3SerialBase::TaskInit(UINT uTask)
{
	SetThreadName(m_Name);

	return OpenPort();
}

INT CG3SerialBase::TaskExec(UINT uTask)
{
	for( ;;) {

		if( GetFrame() ) {

			m_Req.StartFrame(m_bRxBuff[2], m_bRxBuff[4]);

			m_Req.SetFlags(m_bRxBuff[5]);

			m_Req.AddData(m_bRxBuff + 6, m_uSize - 6);

			UINT p = Process();

			if( p == procError ) {

				m_Rep.StartFrame(m_Req.GetService(), opNak);
			}

			PutFrame();

			if( p == procEndLink ) {

				Sleep(50);

				ClosePort();

				EndLink(m_Req);
			}
		}
		else {
			if( HasPortRequest() ) {

				ServicePortRequest();
			}
		}
	}

	return 0;
}

void CG3SerialBase::TaskStop(UINT uTask)
{
}

void CG3SerialBase::TaskTerm(UINT uTask)
{
	ClosePort();
}

// ILinkTransport

BOOL CG3SerialBase::IsUsb(void)
{
	return FALSE;
}

BOOL CG3SerialBase::IsP2P(void)
{
	return TRUE;
}

void CG3SerialBase::SetAuth(void)
{
}

// Frame Processing

UINT CG3SerialBase::Process(void)
{
	if( m_pService ) {

		return m_pService->Process(m_Req, m_Rep, this);
	}

	return procError;
}

void CG3SerialBase::Timeout(void)
{
	if( m_pService ) {

		m_pService->Timeout();
	}
}

void CG3SerialBase::EndLink(CG3LinkFrame &Req)
{
	if( m_pService ) {

		m_pService->EndLink(Req);
	}
}

// Frame Buidling

void CG3SerialBase::NewFrame(void)
{
	m_uPtr = 0;
}

void CG3SerialBase::AddCtrl(BYTE bCtrl)
{
	m_bTxBuff[m_uPtr++] = DLE;

	m_bTxBuff[m_uPtr++] = bCtrl;
}

void CG3SerialBase::AddData(PCBYTE pData, UINT uCount)
{
	while( uCount-- ) {

		AddByte(*pData++);
	}
}

void CG3SerialBase::AddByte(BYTE bData)
{
	if( bData == DLE ) {

		m_bTxBuff[m_uPtr++] = bData;

		m_bTxBuff[m_uPtr++] = bData;
	}
	else
		m_bTxBuff[m_uPtr++] = bData;

	m_wCheck += bData;
}

// Transport Layer

BOOL CG3SerialBase::GetFrame(void)
{
	UINT uState   = 0;
	UINT uCount   = 0;
	UINT uDelay   = 0;
	WORD wCheck   = 0;
	BOOL fDLE     = FALSE;

	ResetTimeout();

	for( ;;) {

		UINT w = m_pData->Read(50);

		if( w == NOTHING ) {

			if( HasPortRequest() ) {

				return FALSE;
			}

			if( ++uDelay >= 10 ) {

				uDelay = 0;

				uState = 0;
			}

			CheckTimeout();

			continue;
		}
		else {
			CheckTimeout();

			uDelay = 0;
		}

		if( w == DLE ) {

			if( !fDLE ) {

				fDLE = TRUE;

				continue;
			}
			else
				fDLE = FALSE;
		}

		switch( uState ) {

			case 0:
				if( fDLE ) {

					if( w == STX ) {

						wCheck = 0x5678;

						uCount = 0;

						uState = 1;

						break;
					}
				}

				if( HasPortRequest() ) {

					return FALSE;
				}

				break;

			case 1:
				if( fDLE ) {

					if( w == ETX ) {

						uState = 2;
					}
					else
						uState = 0;
				}
				else {
					m_bRxBuff[uCount] = BYTE(w);

					if( ++uCount == sizeof(m_bRxBuff) ) {

						uState = 0;
					}

					wCheck += WORD(w);
				}

				break;

			case 2:
				if( fDLE ) {

					uState = 0;
				}
				else {
					wCheck = WORD(wCheck - 0x001 * w);

					uState = 3;
				}

				break;

			case 3:
				if( fDLE ) {

					uState = 0;
				}
				else {
					wCheck = WORD(wCheck - 0x100 * w);

					if( wCheck ) {

						SendByte(NAK);

						uState = 0;
					}
					else {
						SendByte(ACK);

						ResetTimeout();

						m_uSize = uCount;

						return TRUE;
					}
				}
				break;
		}

		fDLE = FALSE;
	}

	return FALSE;
}

BOOL CG3SerialBase::PutFrame(void)
{
	NewFrame();

	AddCtrl(STX);

	m_wCheck = 0x5678;

	AddByte(0x00);

	AddByte(0x00);

	AddByte(m_bRxBuff[2]);

	AddByte(m_bRxBuff[3]);

	AddByte(m_Rep.GetOpcode());

	AddByte(0x00);

	AddData(m_Rep.GetData(), m_Rep.GetDataSize());

	AddCtrl(ETX);

	WORD wCheck = m_wCheck;

	AddByte(LOBYTE(wCheck));

	AddByte(HIBYTE(wCheck));

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return GetAck();
}

BOOL CG3SerialBase::GetAck(void)
{
	SetTimer(200);

	for( ;;) {

		UINT w = m_pData->Read(50);

		if( w == NOTHING ) {

			if( !GetTimer() ) {

				return FALSE;
			}

			continue;
		}

		if( w == ACK ) {

			return TRUE;
		}

		if( w == NAK ) {

			return FALSE;
		}
	}

	return FALSE;
}

void CG3SerialBase::SendByte(BYTE b)
{
	m_pData->Write(b, FOREVER);
}

// Port

BOOL CG3SerialBase::OpenPort(void)
{
	return FALSE;
}

BOOL CG3SerialBase::ClosePort(void)
{
	CAutoGuard Guard;

	if( m_pPort ) {

		m_pPort->Close();

		AfxRelease(m_pPort);

		AfxRelease(m_pData);

		m_pPort = NULL;

		m_pData = NULL;

		return TRUE;
	}

	return FALSE;
}

BOOL CG3SerialBase::HasPortRequest(void)
{
	return FALSE;
}

void CG3SerialBase::ServicePortRequest(void)
{
}

// Timeouts

void CG3SerialBase::ResetTimeout(void)
{
	if( m_fInit ) {

		m_uTimeout = GetTickCount() + ToTicks(g_uTimeout);
	}
}

void CG3SerialBase::CheckTimeout(void)
{
	if( m_fInit ) {

		if( GetTickCount() > m_uTimeout ) {

			Timeout();

			m_fInit = FALSE;
		}
	}
}

// End of File
