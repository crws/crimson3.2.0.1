
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_JsonConfig_HPP

#define INCLUDE_JsonConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// JSON Configuration Object
//

class CJsonConfig
{
public:
	// Constructor
	CJsonConfig(CJsonData *pJson, IConfigStorage *pConfig);
	CJsonConfig(CJsonData *pJson, CStringMap *pKeys);

	// Destructor
	~CJsonConfig(void);

	// Attributes
	CString       GetValue(CString Name) const;
	CString       GetValue(UINT uIndex) const;
	CString       GetValue(CString Name, CString Default) const;
	CString       GetValue(UINT uIndex, CString Default) const;
	BOOL	      GetValueAsBlob(CString Name, CByteArray &Data) const;
	UINT	      GetValueAsUInt(CString Name, UINT uDef, UINT uMin, UINT uMax) const;
	UINT	      GetValueAsUInt(UINT uIndex, UINT uDef, UINT uMin, UINT uMax) const;
	INT	      GetValueAsInt(CString Name, INT nDef, INT nMin, INT nMax) const;
	INT	      GetValueAsInt(UINT uIndex, INT nDef, INT nMin, INT nMax) const;
	double	      GetValueAsDouble(CString Name, double nDef) const;
	double        GetValueAsDouble(UINT uIndex, double nDef) const;
	BOOL	      GetValueAsBool(CString Name, BOOL fDef) const;
	BOOL	      GetValueAsBool(UINT uIndex, BOOL fDef) const;
	CIpAddr       GetValueAsIp(CString Name, CIpAddr Def) const;
	CIpAddr       GetValueAsIp(UINT uIndex, CIpAddr Def) const;
	CMacAddr      GetValueAsMac(CString Name, CMacAddr Def) const;
	CMacAddr      GetValueAsMac(UINT uIndex, CMacAddr Def) const;
	CJsonConfig * GetChild(CString Name) const;
	CJsonConfig * GetChild(UINT uIndex) const;
	CJsonConfig * GetChild(INDEX Index) const;
	UINT	      GetCount(void) const;
	DWORD	      GetCRC(void) const;
	CJsonData   * GetJsonData(void);

protected:
	// Data Members
	CJsonData  * m_pJson;
	CStringMap * m_pKeys;
	BOOL         m_fDelete;

	// Implementation
	bool ReadPersonality(IConfigStorage *pConfig);
	bool ReadFromPersonality(CString &Data) const;
	bool ReadString(CString &Data, CJsonData *pJson) const;
};

// End of File

#endif
