
#include "intern.hpp"

#include "MsgBroker.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Message Broker
//

// Static Data

UINT CMsgBroker::m_uSeq = 1000;

// Constructor

CMsgBroker::CMsgBroker(void)
{
	m_pLock = Create_Mutex();

	StdSetRef();

	piob->RegisterSingleton("msg.broker", 0, this);
}

// Destructor

CMsgBroker::~CMsgBroker(void)
{
	AddRef();

	piob->RevokeSingleton("msg.broker", 0);
}

// IUnknown

HRESULT CMsgBroker::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IMsgBroker);

	StdQueryInterface(IMsgBroker);

	return E_NOINTERFACE;
}

ULONG CMsgBroker::AddRef(void)
{
	StdAddRef();
}

ULONG CMsgBroker::Release(void)
{
	StdRelease();
}

// IMsgBroker

UINT CMsgBroker::RegisterRecvNotify(IMsgRecvNotify *pNotify)
{
	CAutoLock Lock(m_pLock);

	m_Notify.Insert(m_uSeq, pNotify);

	return m_uSeq++;
}

BOOL CMsgBroker::RevokeRecvNotify(UINT uNotify)
{
	CAutoLock Lock(m_pLock);

	return m_Notify.Remove(uNotify);
}

BOOL CMsgBroker::SubmitMessage(PCTXT pName, CMsgPayload *pMessage)
{
	CAutoLock Lock(m_pLock);

	for( INDEX i = m_Notify.GetHead(); !m_Notify.Failed(i); m_Notify.GetNext(i) ) {

		if( m_Notify[i]->OnMsgRecv(pName, pMessage) ) {

			return TRUE;
		}
	}

	return FALSE;
}

// End of File
