
#include "intern.hpp"

#include "MsgTransport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Message Transport Base Class
//

// Static Data

UINT CMsgTransport::m_uSeq = 1000;

// Constructor

CMsgTransport::CMsgTransport(void)
{
	m_pLock   = Create_Mutex();

	m_pWait   = Create_Semaphore();

	m_fEnable = FALSE;

	StdSetRef();
}

// Destructor

CMsgTransport::~CMsgTransport(void)
{
}

// IUnknown

HRESULT CMsgTransport::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IMsgTransport);

	StdQueryInterface(IMsgTransport);

	return E_NOINTERFACE;
}

ULONG CMsgTransport::AddRef(void)
{
	StdAddRef();
}

ULONG CMsgTransport::Release(void)
{
	StdRelease();
}

// IMsgTransport

UINT CMsgTransport::RegisterSendNotify(UINT uNotify, IMsgSendNotify *pNotify)
{
	CAutoLock Lock(m_pLock);

	INDEX Index = m_Notify.FindName(uNotify);

	if( m_Notify.Failed(Index) ) {

		m_Notify.Insert(m_uSeq, pNotify);

		return m_uSeq++;
	}

	AfxAssert(m_Notify[Index] == pNotify);

	return uNotify;
}

BOOL CMsgTransport::RevokeSendNotify(UINT uNotify)
{
	CAutoLock Lock(m_pLock);

	return m_Notify.Remove(uNotify);
}

BOOL CMsgTransport::QueueMessage(CMsgPayload *pMessage)
{
	if( m_fEnable ) {

		if( m_Queue.GetCount() < 50 ) {

			pMessage->m_Attempts = 0;

			CAutoLock Lock(m_pLock);

			m_Queue.Append(pMessage);

			Lock.Free();

			m_pWait->Signal(1);

			return TRUE;
		}
	}

	return FALSE;
}

// Implementation

void CMsgTransport::SendNotify(CMsgPayload const *pMessage, BOOL fOkay)
{
	CAutoLock Lock(m_pLock);

	if( pMessage->m_SendNotify ) {

		INDEX Index = m_Notify.FindName(pMessage->m_SendNotify);

		if( !m_Notify.Failed(Index) ) {

			IMsgSendNotify *pNotify = m_Notify[Index];

			pNotify->OnMsgSent(pMessage, fOkay);
		}
	}

	Lock.Free();
}

// End of File
