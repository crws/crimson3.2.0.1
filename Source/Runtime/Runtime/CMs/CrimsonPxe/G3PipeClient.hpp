
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3PipeClient_HPP

#define	INCLUDE_G3PipeClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "G3SerialBase.hpp"

////////////////////////////////////////////////////////////////////////
//
// Serial Pipe Client Transport
//

class CG3PipeClient : public CG3SerialBase
{
public:
	// Constructor
	CG3PipeClient(UINT uLevel, ILinkService *pService);

protected:
	// Port
	BOOL OpenPort(void);
};

// End of File

#endif
