
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_StdDebugCmds_HPP

#define INCLUDE_StdDebugCmds_HPP

////////////////////////////////////////////////////////////////////////
//
// Standard Debug Commands
//

class CStdDebugCmds : public IDiagProvider
{
	public:
		// Constructor
		CStdDebugCmds(void);

		// Destructor
		~CStdDebugCmds(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Data Members
		ULONG	       m_uRefs;
		time_t	       m_timeStart;
		IDiagManager * m_pDiag;
		UINT           m_uProv;

		// Registration
		void AddDiagCmds(void);

		// Command Codes
		enum
		{
			cmdNone,
			cmdChangeDir,	
			cmdClearGMC,	
			cmdCrash,	
			cmdCrashDump,
			cmdDelete,	
			cmdDir,		
			cmdFormat,	
			cmdFramDump,	
			cmdHexDump,
			cmdMakeDir,
			cmdMemCheck,	
			cmdMemStat,	
			cmdMemTest,	
			cmdMount,
			cmdPing,	
			cmdPwd,
			cmdRemoveDir,
			cmdReset,
			cmdShowARP,	
			cmdTime,	
			cmdTraps,
			cmdType,
			cmdUnmount,
			cmdUpTime
			};

		// Command Handlers
		UINT CmdChangeDir(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdChkDsk(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdClearGMC(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdCrash(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdCrashDump(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdDelete(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdDir(IDiagOutput *pOut, IDiagCommand *pCmd);	
		UINT CmdDisk(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdFormat(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdFramDump(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdHello(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdHexDump(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdMakeDir(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdMemCheck(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdMemStat(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdMemTest(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdMount(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdPing(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdPwd(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdRemoveDir(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdReset(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdTime(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdTraps(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdType(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdUnmount(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdUpTime(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Implementation
		BOOL ParseRange(IDiagCommand *pCmd, UINT &uInit, UINT &uSize, UINT uBase, UINT uDefault);
		UINT ParseNumber(CString const &Text);
		void ShowTime(IDiagOutput *pOut);
	};

// End of File

#endif
