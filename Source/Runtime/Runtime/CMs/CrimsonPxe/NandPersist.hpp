
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandDatabase_HPP

#define	INCLUDE_NandDatabase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include "CxNand.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Persistence Manager
//

class CNandPersist : public CNandClient, public IPersist
{
public:
	// Constructor
	CNandPersist(CNandBlock const &Start, CNandBlock const &End);

	// Destructor
	~CNandPersist(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IPersist
	void METHOD Init(void);
	void METHOD Term(void);
	void METHOD ByeBye(void);
	BOOL METHOD IsDirty(void);
	void METHOD Commit(BOOL fReset);
	BYTE METHOD GetByte(DWORD dwAddr);
	WORD METHOD GetWord(DWORD dwAddr);
	LONG METHOD GetLong(DWORD dwAddr);
	void METHOD GetData(PBYTE pData, DWORD dwAddr, UINT uCount);
	void METHOD PutByte(DWORD dwAddr, BYTE bData);
	void METHOD PutWord(DWORD dwAddr, WORD wData);
	void METHOD PutLong(DWORD dwAddr, LONG lData);
	void METHOD PutData(PBYTE pData, DWORD dwAddr, UINT uCount);

protected:
	// Constants
	static DWORD const magicBank     = 0x62616E6B;
	static DWORD const magicHead	 = 0x68656164;
	static DWORD const magicBody     = 0x626F6479;
	static DWORD const magicTail     = 0x7461696C;
	static UINT  const C64K          = 65536;

	// Bank State
	struct CBank
	{
		PBYTE	   m_pData;
		BOOL	   m_fDirty;
		CNandBlock m_Block;
	};

	// Page Header
	struct CPageHeader
	{
		DWORD Magic;
		DWORD Param;
		DWORD Pad1;
		DWORD Pad2;
		DWORD Pad3;
	};

	// Type Definitions
	typedef	CPageHeader       * PPAGE;
	typedef	CPageHeader const * PCPAGE;

	// Data Members
	UINT	    m_uRefs;
	IMutex    * m_pLock;
	IDatabase * m_pDatabase;
	CNandBlock  m_BlockScan;
	CBank	    m_Bank[4];

	// Implementation
	bool InitBanks(void);
	bool FreeBanks(void);
	bool LoadBanks(void);
	bool FindFreeBlock(CNandBlock &Block);
	bool FormatBlock(CNandBlock &Block);
};

// End of File

#endif
