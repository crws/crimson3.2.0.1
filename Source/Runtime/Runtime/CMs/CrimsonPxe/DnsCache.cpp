
#include "intern.hpp"

#include "DnsCache.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-20010 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DNS Cache
//

// Constructor

CDnsCache::CDnsCache(void)
{
	m_pMutex = Create_Mutex();
}

// Destructor

CDnsCache::~CDnsCache(void)
{
	m_pMutex->Release();
}

// Attributes

IMutex * CDnsCache::GetLock(void) const
{
	return m_pMutex;
}

// Operations

BOOL CDnsCache::Find(PCTXT pHost, CIpList * &pList)
{
	INDEX n = m_Map.FindName(pHost);

	if( !m_Map.Failed(n) ) {

		CDnsEntry *pEntry = m_Map.GetData(n);

		pList = &pEntry->m_List;

		return TRUE;
	}

	return FALSE;
}

void CDnsCache::Add(PCTXT pHost, CIpList const &List, DWORD dwTTL)
{
	CAutoLock Lock(m_pMutex);

	INDEX n = m_Map.FindName(pHost);

	if( !m_Map.Failed(n) ) {

		CDnsEntry *pEntry = m_Map.GetData(n);

		pEntry->m_List = List;

		pEntry->m_Time = (dwTTL == NOTHING) ? -1 : (time(NULL) + dwTTL);
	}
	else {
		CDnsEntry *pEntry = New CDnsEntry;

		pEntry->m_Name = pHost;

		pEntry->m_List = List;

		pEntry->m_Time = (dwTTL == NOTHING) ? -1 : (time(NULL) + dwTTL);

		m_Map.Insert(pHost, pEntry);
	}
}

void CDnsCache::Poll(void)
{
	CAutoLock Lock(m_pMutex);

	time_t t = time(NULL);

	for( INDEX n, i = m_Map.GetHead(); !m_Map.Failed((n = i)); i = n ) {

		m_Map.GetNext(n);

		CDnsEntry *pEntry = m_Map.GetData(i);

		if( t >= pEntry->m_Time ) {

			m_Map.Remove(i);

			delete pEntry;
		}
	}
}

void CDnsCache::Show(void)
{
/*	#if defined(_DEBUG)

	m_pMutex->Wait(FOREVER);

	BOOL fHead = FALSE;

	UINT uTime = GetTickCount();

	for( UINT n = 0; n < elements(m_List); n++ ) {

		CEntry &Entry = m_List[n];

		if( Entry.m_fUsed ) {

			if( !fHead ) {

				AfxTrace("IP Address              TTL  Name\n");

				AfxTrace("---------------  ----------  ------------------------------\n");

				fHead = TRUE;
				}

			// !!C3!!

			CString s = Entry.m_List[0].GetAsText();

			AfxTrace("%-15s  %10.10u  %s\n", PCTXT(s), Entry.m_dwTTL, Entry.m_sName);
			}
		}

	if( fHead ) {

		AfxTrace("---------------  ----------  ------------------------------\n");
		}
	else
		AfxTrace("No Entries\n");

	m_pMutex->Free();

	#endif
*/
}

// End of File
