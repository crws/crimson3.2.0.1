
#include "Intern.hpp"

#include "WebFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Helper
//

#define Entry(f, p) { f, data_##p, size_##p }

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

#include "cellinfo.htm.dat"
#include "config.htm.dat"
#include "cutils.htm.dat"
#include "default.htm.dat"
#include "devinfo.htm.dat"
#include "editor.htm.dat"
#include "logoff.htm.dat"
#include "logon.htm.dat"
#include "nojump.htm.dat"
#include "setpass.htm.dat"
#include "stdhead.htm.dat"
#include "stdnavbar.htm.dat"
#include "systool.htm.dat"
#include "syscmd.htm.dat"
#include "sysdebug.htm.dat"
#include "syslog.htm.dat"
#include "syspcap.htm.dat"
#include "system.htm.dat"
#include "wifiinfo.htm.dat"

//////////////////////////////////////////////////////////////////////////
//
// File Table
//

CWebFileData const CWebFiles::m_Files3[] = {

	Entry("/cellinfo.htm",	cellinfo_htm),
	Entry("/config.htm",	config_htm),
	Entry("/cutils.htm",	cutils_htm),
	Entry("/default.htm",	default_htm),
	Entry("/devinfo.htm",	devinfo_htm),
	Entry("/editor.htm",	editor_htm),
	Entry("/logoff.htm",	logoff_htm),
	Entry("/logon.htm",	logon_htm),
	Entry("/nojump.htm",	nojump_htm),
	Entry("/setpass.htm",	setpass_htm),
	Entry("/stdhead.htm",	stdhead_htm),
	Entry("/stdnavbar.htm",	stdnavbar_htm),
	Entry("/system.htm",	system_htm),
	Entry("/syscmd.htm",	syscmd_htm),
	Entry("/sysdebug.htm",	sysdebug_htm),
	Entry("/syslog.htm",	syslog_htm),
	Entry("/syspcap.htm",	syspcap_htm),
	Entry("/systool.htm",	systool_htm),
	Entry("/wifiinfo.htm",	wifiinfo_htm),
};

UINT const CWebFiles::m_uCount3 = elements(m_Files3);

// End of File
