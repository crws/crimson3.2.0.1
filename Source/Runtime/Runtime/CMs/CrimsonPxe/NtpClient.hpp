
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NtpClient_HPP

#define	INCLUDE_NtpClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PxeIpClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// NTP Client
//

class CNtpClient : public CPxeIpClient, public IClientProcess
{
public:
	// Constructor
	CNtpClient(CJsonConfig *pJson, UINT uFaces);

	// Destructor
	~CNtpClient(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Data Members
	ULONG m_uRefs;
	UINT  m_uPeriod;

	// Implementation
	void   ApplyConfig(CJsonConfig *pJson);
	BOOL   PerformSync(void);
	BOOL   SetTime64(INT64 t64, IPREF ip);
	INT64  GetTime64(void);
	void   StoreTime(PBYTE pData, UINT n, INT64 t64);
	INT64  FetchTime(PBYTE pData, UINT n);
};

// End of File

#endif
