
#include "intern.hpp"

#include "MultiDns.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Othre Headers
//

#include "DnsHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multicast DNS Server
//

// Constructor

CMultiDns::CMultiDns(UINT Protocol, CString Name1, CString Name2)
{
	m_Protocol  = Protocol;

	m_pSendSock = NULL;

	m_pRecvSock = NULL;

	if( Protocol == protocolMDNS ) {

		m_Name1 = Name1.GetLength() ? (Name1 + ".local") : "";

		m_Name2 = Name2.GetLength() ? (Name2 + ".local") : "";
	}

	if( Protocol == protocolLLMNR ) {

		m_Name1 = Name1;

		m_Name2 = Name2;
	}

	StdSetRef();
}

// Destructor

CMultiDns::~CMultiDns(void)
{
}

// IUnknown

HRESULT CMultiDns::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CMultiDns::AddRef(void)
{
	StdAddRef();
}

ULONG CMultiDns::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CMultiDns::TaskInit(UINT uTask)
{
	SetThreadName("MultiDns");

	return TRUE;
}

INT CMultiDns::TaskExec(UINT uTask)
{
	for(;;) {

		AfxNewObject("sock-udp", ISocket, m_pSendSock);

		AfxNewObject("sock-udp", ISocket, m_pRecvSock);

		DWORD t1 = GetTickCount();

		if( OpenSocket() ) {

			for( ;;) {

				CBuffer *pRecv;

				if( m_pRecvSock->Recv(pRecv) == S_OK ) {

					Process(pRecv);

					pRecv->Release();

					t1 = GetTickCount();

					continue;
				}

				if( GetTickCount() - t1 >= 10000 ) {

					// If the interface list has changed under us, we
					// might end up having to register to be active on
					// all of the available interfaces...

					break;
				}

				Sleep(20);
			}

			KillSockets();
		}
		else {
			KillSockets();

			break;
		}
	}

	return 0;
}

void CMultiDns::TaskStop(UINT uTask)
{
}

void CMultiDns::TaskTerm(UINT uTask)
{
	KillSockets();

	Release();
}

// Implementation

bool CMultiDns::OpenSocket(void)
{
	CIpAddr Ip;

	if( GetMulticast(Ip) ) {

		m_pRecvSock->SetOption(OPT_ADDRESS, 3);

		m_pRecvSock->SetOption(OPT_RECV_QUEUE, 4);

		m_pRecvSock->Listen(Ip, GetPort());

		return true;
	}

	return false;
}

void CMultiDns::KillSockets(void)
{
	AfxRelease(m_pSendSock);

	AfxRelease(m_pRecvSock);

	m_pSendSock = NULL;

	m_pRecvSock = NULL;
}

bool CMultiDns::GetMulticast(CIpAddr &Ip)
{
	if( m_Protocol == protocolMDNS ) {

		Ip.m_b1 = 224;
		Ip.m_b2 = 0;
		Ip.m_b3 = 0;
		Ip.m_b4 = 251;

		return true;
	}

	if( m_Protocol == protocolLLMNR ) {

		Ip.m_b1 = 224;
		Ip.m_b2 = 0;
		Ip.m_b3 = 0;
		Ip.m_b4 = 252;

		return true;
	}

	return false;
}

WORD CMultiDns::GetPort(void)
{
	if( m_Protocol == protocolMDNS ) {

		return 5353;
	}

	if( m_Protocol == protocolLLMNR ) {

		return 5355;
	}

	return 0;
}

void CMultiDns::AddText(CBuffer *pBuff, CString Name)
{
	for( ;;) {

		CString Label = Name.StripToken('.');

		UINT    uLen  = Label.GetLength();

		*BuffAddTail(pBuff, BYTE) = BYTE(uLen);

		if( uLen ) {

			memcpy(pBuff->AddTail(uLen), PCTXT(Label), uLen);

			continue;
		}

		break;
	}
}

void CMultiDns::AddByte(CBuffer *pBuff, BYTE Data)
{
	*BuffAddTail(pBuff, BYTE) = Data;
}

void CMultiDns::AddWord(CBuffer *pBuff, WORD Data)
{
	*BuffAddTail(pBuff, WORD) = HostToNet(Data);
}

void CMultiDns::AddLong(CBuffer *pBuff, DWORD Data)
{
	*BuffAddTail(pBuff, DWORD) = HostToNet(Data);
}

void CMultiDns::Process(CBuffer *pRecv)
{
	////////

	if( FALSE ) {

		AfxTrace("\nREQUEST:\n");

		AfxDump(pRecv->GetData(), pRecv->GetSize());
	}

	////////

	CIpAddr LocIp, RemIp, UseIp;

	WORD    LocPn, RemPn;

	RemIp = BuffStripHead(pRecv, CIpAddr)[0];

	RemPn = MotorToHost(*BuffStripHead(pRecv, WORD));

	LocIp = BuffStripHead(pRecv, CIpAddr)[0];

	LocPn = MotorToHost(*BuffStripHead(pRecv, WORD));

	UseIp = BuffStripHead(pRecv, CIpAddr)[0];

	////////

	CDnsHeader *pReqHeader = BuffStripHead(pRecv, CDnsHeader);

	pReqHeader->NetToHost();

	if( pReqHeader->m_Opcode == 0 ) {

		if( pReqHeader->m_QR == 0 ) {

			if( pReqHeader->m_QDCount >= 1 ) {

				CDnsHeader *pRepHeader = NULL;

				CBuffer    *pSendQD    = NULL;

				CBuffer    *pSendAN    = NULL;

				CString     Match;

				while( pReqHeader->m_QDCount-- ) {

					CDnsQuestion *pReqQuestion = (CDnsQuestion *) pRecv->GetData();

					CDnsQFooter  *pQuestFooter = pReqQuestion->GetFoot();

					pReqQuestion->NetToHost();

					if( (pQuestFooter->m_Class & 0x7FFF) == classIN ) {

						CDnsName        *pReqName  = pReqQuestion->GetName();

						UINT             uIndex    = 0;

						CDnsLabel const *pReqLabel = pReqName->GetHead(uIndex);

						CString          Name      = "";

						if( pReqLabel ) {

							if( pReqLabel->IsPointer() ) {

								UINT uOffset = pReqLabel->GetPointer();

								pReqName  = (CDnsName *) (PBYTE(pReqHeader) + uOffset);

								pReqLabel = pReqName->GetHead(uIndex);
							}

							while( pReqLabel && pReqLabel->GetLength() ) {

								if( Name.GetLength() ) {

									Name += '.';
								}

								Name += CString(pReqLabel->GetText(),
										pReqLabel->GetLength()
								);

								pReqLabel = pReqName->GetNext(uIndex);
							}
						}

						if( (m_Name1.GetLength() && m_Name1 == Name) || (m_Name2.GetLength() && m_Name2 == Name) ) {

							if( !pSendQD ) {

								Match      = (Name == m_Name1) ? m_Name1 : m_Name2;

								pSendQD    = BuffAllocate(1024);

								pSendAN    = BuffAllocate(1024);

								pRepHeader = BuffAddTail(pSendQD, CDnsHeader);

								pRepHeader->Init();

								pRepHeader->m_ID = pReqHeader->m_ID;

								pRepHeader->m_QR = 1;

								if( m_Protocol == protocolMDNS ) {

									pRepHeader->m_AA = 1;
								}
							}

							if( TRUE ) {

								AddText(pSendQD, Match);

								AddWord(pSendQD, pQuestFooter->m_Type);

								AddWord(pSendQD, pQuestFooter->m_Class);

								pRepHeader->m_QDCount++;
							}

							if( pQuestFooter->m_Type == typeA ) {

								AddText(pSendAN, Match);

								AddWord(pSendAN, typeA);

								if( m_Protocol == protocolMDNS ) {

									AddWord(pSendAN, classIN | 0x8000);
								}

								if( m_Protocol == protocolLLMNR ) {

									AddWord(pSendAN, classIN);
								}

								AddLong(pSendAN, 30);

								AddWord(pSendAN, 4);

								AddByte(pSendAN, UseIp.m_b1);
								AddByte(pSendAN, UseIp.m_b2);
								AddByte(pSendAN, UseIp.m_b3);
								AddByte(pSendAN, UseIp.m_b4);

								pRepHeader->m_ANCount++;
							}
						}
					}

					pRecv->StripHead(pReqQuestion->GetSize());
				}

				if( pSendQD ) {

					if( m_Protocol == protocolMDNS || pRepHeader->m_ANCount == 0 ) {

						AddText(pSendAN, Match);

						AddWord(pSendAN, typeSOA);

						AddWord(pSendAN, classIN);

						AddLong(pSendAN, 30);

						PWORD pSize = BuffAddTail(pSendAN, WORD);

						AddText(pSendAN, Match);

						AddText(pSendAN, Match);

						AddLong(pSendAN, 1000);

						AddLong(pSendAN, 30);
						AddLong(pSendAN, 30);
						AddLong(pSendAN, 30);
						AddLong(pSendAN, 30);

						UINT n = pSendAN->GetData() + pSendAN->GetSize() - PBYTE(pSize) - 2;

						*pSize = HostToNet(WORD(n));

						pRepHeader->m_NSCount++;
					}

					if( m_Protocol == protocolMDNS && pRepHeader->m_ANCount == 0 ) {

						AddText(pSendAN, Match);

						AddWord(pSendAN, 47);

						AddWord(pSendAN, classIN);

						AddLong(pSendAN, 30);

						PWORD pSize = BuffAddTail(pSendAN, WORD);

						AddText(pSendAN, Match);

						AddByte(pSendAN, 0);
						AddByte(pSendAN, 6);
						AddLong(pSendAN, 0x40000000);
						AddWord(pSendAN, 0x0001);

						UINT n = pSendAN->GetData() + pSendAN->GetSize() - PBYTE(pSize) - 2;

						*pSize = HostToNet(WORD(n));

						pRepHeader->m_ARCount++;
					}

					pRepHeader->HostToNet();

					memcpy(pSendQD->AddTail(pSendAN->GetSize()),
					       pSendAN->GetData(),
					       pSendAN->GetSize()
					);

					pSendAN->Release();

					if( FALSE ) {

						AfxTrace("REPLY:\n");

						AfxDump(pSendQD->GetData(), pSendQD->GetSize());
					}

					if( m_Protocol == protocolMDNS ) {

						m_pSendSock->Connect(LocIp, RemPn, LocPn);
					}

					if( m_Protocol == protocolLLMNR ) {

						m_pSendSock->Connect(RemIp, RemPn, LocPn);
					}

					m_pSendSock->Send(pSendQD);

					m_pSendSock->Close();
				}
			}
		}
	}
}

// End of File
