
#include "Intern.hpp"

#include "WebFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Helper
//

#define Entry(f, p) { f, data_##p, size_##p }

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

#include "favicon.ico.dat"
#include "logo.png.dat"

//////////////////////////////////////////////////////////////////////////
//
// File Table
//

CWebFileData const CWebFiles::m_Files5[] = {

	Entry("/assets/icons/favicon.ico",	favicon_ico),
	Entry("/assets/images/logo.png",	logo_png),

};

UINT const CWebFiles::m_uCount5 = elements(m_Files5);

// End of File
