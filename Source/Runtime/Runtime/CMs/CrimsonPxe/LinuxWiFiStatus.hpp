
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinuxWiFiStatus_HPP

#define INCLUDE_LinuxWiFiStatus_HPP

//////////////////////////////////////////////////////////////////////////
//
// Linux Wi-Fi Status
//

class CLinuxWiFiStatus :
	public IInterfaceStatus,
	public IWiFiStatus
{
public:
	// Constructor
	CLinuxWiFiStatus(PCTXT pName, UINT uInst);

	// Destructor
	~CLinuxWiFiStatus(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IInterfaceStatus
	BOOL METHOD GetInterfaceStatus(CInterfaceStatusInfo &Info);

	// IWiFiStatus
	BOOL METHOD GetWiFiStatus(CWiFiStatusInfo &Info);
	BOOL METHOD ScanNetworks(CArray<CWiFiNetworkInfo> &List);
	BOOL METHOD SendCommand(PCTXT pCmd);

protected:
	// Data Members
	ULONG               m_uRefs;
	CString             m_Name;
	CString		    m_Face;
	UINT                m_uInst;
	IMutex		  * m_pLock;
	DWORD	            m_timeStatus;
	DWORD		    m_timeScan;
	CWiFiStatusInfo	    m_WiFiInfo;

	// Implementation
	bool ReadJson(CJsonData &Json, PCTXT pType);
	void ReadStatus(void);
};

// End of File

#endif
