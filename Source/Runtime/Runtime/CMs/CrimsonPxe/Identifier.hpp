
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Identifier_HPP

#define	INCLUDE_Identifier_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// Network Identifier Client
//

class CIdentifier : public IClientProcess, public IDeviceIdentifier
{
public:
	// Constructor
	CIdentifier(CJsonConfig *pJson);

	// Destructor
	~CIdentifier(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDeviceIdentifier
	BOOL METHOD RunOperation(BYTE bCode, BOOL fUsb, CBuffer *pBuff);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Data Members
	ULONG		 m_uRefs;
	IPlatform      * m_pPlatform;
	INetApplicator * m_pNetApp;
	INetUtilities  * m_pNetUtils;
	ICrimsonPxe    * m_pPxe;
	BOOL		 m_fFlash;
	BOOL		 m_fLimit;
	UINT		 m_Time1;
	UINT		 m_Time2;

	// Implementation
	void ApplyConfig(CJsonConfig *pJson);
};

// End of File

#endif
