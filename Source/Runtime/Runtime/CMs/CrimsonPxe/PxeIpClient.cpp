
#include "intern.hpp"

#include "PxeIpClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Multiple IP Client
//

// Constructor

CPxeIpClient::CPxeIpClient(UINT uFaces)
{
	// TODO -- This needs to use net info object!!!

	m_uFaces  = uFaces;

	m_uMode   = 1;

	m_bOption = 0;

	m_pUtils  = NULL;

	AfxGetObject("ip", 0, INetUtilities, m_pUtils);
}

// Destructor

CPxeIpClient::~CPxeIpClient(void)
{
	AfxRelease(m_pUtils);
}

// Implementation

BOOL CPxeIpClient::FindServerList(void)
{
	if( m_uMode == 1 ) {

		if( m_pUtils ) {

			if( m_Faces.IsEmpty() ) {

				for( UINT n = 0; n < m_uFaces; n++ ) {

					CIpAddr Server;

					if( m_pUtils->IsInterfaceUp(n) ) {

						Server = m_pUtils->GetOption(m_bOption, n);
					}

					m_Faces.Append(Server);
				}
			}
			else {
				for( UINT n = 0; n < m_uFaces; n++ ) {

					CIpAddr Server;

					if( m_pUtils->IsInterfaceUp(n) ) {

						Server = m_pUtils->GetOption(m_bOption, n);
					}

					if( m_Faces[n] != Server ) {

						m_Faces.SetAt(n, Server);

						m_ServerIps.Empty();
					}
				}
			}
		}
	}

	if( m_ServerIps.IsEmpty() ) {

		if( m_uMode == 2 ) {

			for( UINT n = 0; n < m_ConfigIps.GetCount(); n++ ) {

				if( !m_ConfigIps[n].IsEmpty() ) {

					m_ServerIps.Append(m_ConfigIps[n]);
				}
			}
		}
		else {
			for( UINT n = 0; n < m_uFaces; n++ ) {

				if( !m_Faces[n].IsEmpty() ) {

					m_ServerIps.Append(m_Faces[n]);
				}
			}
		}

		if( m_ServerIps.IsEmpty() ) {

			m_ServerIps = m_DefaultIps;
		}

		m_uActive = rand() % m_ServerIps.GetCount();

		return TRUE;
	}

	return FALSE;
}

// End of File
