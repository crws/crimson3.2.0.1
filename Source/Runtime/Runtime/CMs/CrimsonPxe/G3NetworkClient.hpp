
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3NetworkClient_HPP

#define	INCLUDE_G3NetworkClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "G3LinkFrame.hpp"

#include "G3Slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

interface ILinkService;

class     CJsonConfig;

////////////////////////////////////////////////////////////////////////
//
// Network Client Transport
//

class CG3NetworkClient : public IClientProcess, public ILinkTransport
{
public:
	// Constructor
	CG3NetworkClient(UINT uLevel, ILinkService *pService);

	// Destructor
	~CG3NetworkClient(void);

	// Operations
	BOOL Open(CJsonConfig *pJson);
	void Close(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

	// ILinkTransport
	BOOL IsUsb(void);
	BOOL IsP2P(void);
	void SetAuth(void);

protected:
	// Configuration
	struct CConfig
	{
		// Constructor
		CConfig(void);

		// Operations
		BOOL Parse(CJsonConfig *pJson);

		// Data Members
		UINT	m_uPort;
		UINT	m_uTimeout;
		CIpAddr m_ReqAddr;
		CIpAddr m_ReqMask;
	};

	// Data Members
	ULONG		   m_uRefs;
	CConfig		   m_Config;
	UINT		   m_uLevel;
	ILinkService     * m_pService;
	HTHREAD		   m_hThread;
	ISocket	         * m_pSocket;
	BYTE		   m_bSeq;
	CG3LinkFrame	   m_Req;
	CG3LinkFrame	   m_Rep;
	BOOL		   m_fAuth;
	CIpAddr		   m_LastPeer;
	UINT		   m_uLastTime;
	BOOL		   m_fLastAuth;

	// Frame Processing
	UINT Process(void);
	void Timeout(void);
	void EndLink(CG3LinkFrame &Req);

	// Transport Layer
	BOOL GetFrame(void);
	BOOL PutFrame(void);
	BOOL CheckSocket(void);
	BOOL CheckAccess(void);
};

// End of File

#endif
