
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Session Address Rewrite and Cellular Bars
//

// Data

var m_status_init;
var m_status_data;
var m_status_busy;
var m_status_wait;
var m_status_func;
var m_access;
var m_tab_id;

// Code

function doSession(onLoaded) {

	m_status_busy = false;

	m_status_wait = false;

	m_access = parseInt($("#access-level").html());

	initStatus();

	if (typeof doCustom === "function") {

		doCustom();
	}

	if (typeof onLoaded === "function") {

		onLoaded();
	}
}

function doReturn() {

	setTimeout(doJump, 2000);
}

function doJump() {

	location.href = getParam("back");
}

function jumpTo(url) {

	location.href = url;
}

function makeAjax(url) {

	return url;
}

function getParam(name) {

	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

	var u = location.href;

	var s = "[\\?&]" + name + "=([^&#]*)";

	var r = new RegExp(s);

	var k = r.exec(u);

	return k == null ? null : k[1];
}

function Queue() {

	var a = [], b = 0;

	this.getLength = function () { return a.length - b; };

	this.isEmpty = function () { return a.length == 0; };

	this.enqueue = function (b) { a.push(b); };

	this.dequeue = function () { if (a.length > 0) { var c = a[b]; 2 * ++b >= a.length && (a = a.slice(b), b = 0); return c; } };

	this.peek = function () { return a.length > 0 ? a[b] : void 0; }
}

function check401(code) {

	if (code == 401) {

		if (m_status_wait) {

			window.location = "/logon.htm";
		}
		else {

			var uri = window.location + "";

			var len = 2;

			len += window.location.protocol.length;

			len += window.location.host.length;

			uri = uri.substring(len, uri.length);

			window.location = "/logon.htm?uri=" + uri;
		}

		return true;
	}

	return false;
}

function setUrlArg(name, value) {

	if (location.protocol !== undefined) {

		var args = location.search.substr(1).split('&');

		var rest = "?";

		var init = true;

		var done = false;

		for (var n = 0; n < args.length; n++) {

			if (args[n].length) {

				var bits = args[n].split('=');

				rest += (init ? "" : "&");

				if (bits[0] == name) {

					rest += name + "=" + value;

					done = true;
				}
				else {

					rest += args[n];
				}

				init = false;
			}
		}

		if (!done) {

			rest += (init ? "" : "&") + name + "=" + value;
		}

		if (rest != location.search) {

			var base = location.protocol + '//' + location.host + location.pathname;

			window.history.replaceState("", "", base + rest);
		}
	}
}

function initStatus() {

	if (document.getElementById("bars-canvas")) {

		if ('tabid' in sessionStorage) {

			m_tab_id = sessionStorage.tabid;
		}
		else {

			m_tab_id = Math.floor(Math.random() * 1000000).toString();

			sessionStorage.setItem("tabid", m_tab_id);
		}

		if ('status' in sessionStorage) {

			m_status_data = JSON.parse(sessionStorage.status);

			m_status_init = false;

			parseStatus();

			setTimeout(sendStatusRequest, 1000);
		}
		else {

			m_status_init = true;

			sendStatusRequest();
		}
	}
	else {

		sessionStorage.removeItem('status');
	}
}

function sendStatusRequest() {

	var url = "/ajax/get-status.ajax";

	url += "?tab=" + m_tab_id;

	url += "&init=" + (m_status_init ? 1 : 0);

	$.get({
		url: makeAjax(url),
		success: onStatusOkay,
		error: onStatusFailed,
		timeout: m_status_init ? 15000 : 0,
		xhrFields: { withCredentials: true },
	});
}

function onStatusOkay(reply) {

	m_status_data = reply;

	sessionStorage.setItem('status', JSON.stringify(m_status_data));

	parseStatus();

	if (!("defer" in reply) || !reply.defer) {

		setTimeout(sendStatusRequest, 1000);
	}
	else {

		m_status_init = false;

		setTimeout(sendStatusRequest, 100);
	}
}

function onStatusFailed(error) {

	if (!check401(error.status)) {

		m_status_init = true;

		setTimeout(sendStatusRequest, 5000);
	}
}

function waitRestart() {

	$("#sys-info").css("color", "#D00000");

	m_status_busy = true;

	m_status_wait = true;
}

function parseStatus() {

	if (!m_status_wait) {

		if ('pxe' in m_status_data) {

			if ('status' in m_status_data.pxe) {

				if (m_status_data.pxe.status == 8) {

					if (!m_status_busy) {

						$("#sys-info").css("color", "#D00000");

						m_status_busy = true;
					}
				}
				else {

					if (m_status_busy) {

						if (m_status_func) {

							m_status_func();
						}

						$("#sys-info").css("color", "");

						m_status_busy = false;
					}
				}
			}
		}

		if ('wifi' in m_status_data) {

			showSignal($("#wifi-canvas"), $("#wifi-item"), m_status_data.wifi, drawWiFi, showWiFi);
		}

		if ('cell' in m_status_data) {

			showSignal($("#bars-canvas"), $("#bars-item"), m_status_data.cell, drawBars, showCell);
		}

		if ('gps' in m_status_data) {

			var pin = $("#map-pin");

			if (m_status_data.gps.okay) {

				pin.removeClass('pin-disable').addClass('pin-enable').off('click').on('click', mapMe);
			}
			else {

				pin.removeClass('pin-enable').addClass('pin-disable').off('click');
			}
		}
	}
}

function showSignal(canvas, item, info, draw, show) {

	if (info.okay) {

		if (info.register) {

			draw(canvas[0], info.bars);

			if (!hasTooltip(canvas)) {

				canvas.attr('data-toggle', 'tooltip').attr('title', info.network);

				canvas.attr('data-container', 'body');

				canvas.tooltip({ trigger: 'hover', placement: 'bottom' });
			}
			else {

				canvas.attr('data-original-title', info.network);

				canvas.tooltip('fixTitle');

				canvas.tooltip('enable');
			}
		}
		else {

			if (hasTooltip(canvas)) {

				canvas.tooltip('disable');
			}

			draw(canvas[0], 0);
		}

		canvas.addClass('hand-enable').off('click').on('click', function () { show(0); });

		item.css('display', '');
	}
	else {

		hideBars(canvas[0]);

		if (hasTooltip(canvas)) {

			canvas.tooltip('disable');
		}

		canvas.removeClass('hand-enable').off('click');

		item.css('display', 'none');
	}
}

function hasTooltip(field) {

	return field.attr('data-original-title') ? true : false;
}

function drawBars(cvs, bars) {

	var ctx = cvs.getContext("2d");

	for (var n = 0; n < 4; n++) {

		ctx.fillStyle = (bars == 100) ? '#6060E0' : (n < bars) ? '#00C000' : '#808080';

		ctx.fillRect(6 * n, 4 * (3 - n), 4, 20 - 4 * (3 - n));
	}
}

function drawWiFi(cvs, bars) {

	var ctx = cvs.getContext("2d");

	ctx.clearRect(0, 0, 24, 16);

	ctx.lineWidth = 2.2;

	for (var n = 0; n < 4; n++) {

		var color = (bars == 100) ? '#6060E0' : (n < bars) ? '#00C000' : '#808080';

		if (n == 0) {

			ctx.fillStyle = color;

			ctx.beginPath();

			ctx.arc(12, 14, 1.8, rad(-180), rad(+180));

			ctx.fill();
		}
		else {

			ctx.strokeStyle = color;

			ctx.beginPath();

			ctx.arc(12, 14, 1 + 4 * n, rad(-50), rad(+50));

			ctx.stroke();
		}
	}
}

function hideBars(cvs) {

	var ctx = cvs.getContext("2d");

	ctx.clearRect(0, 0, 24, 16);
}

function rad(v) {

	return (v / 180 - 0.5) * Math.PI;
}

function mapMe() {

	if ('lat' in m_status_data.gps && 'long' in m_status_data.gps) {

		var url = "https://maps.google.com/?q=";

		url += m_status_data.gps.lat.toString();

		url += ",";

		url += m_status_data.gps.long.toString();

		window.open(url);
	}
}

function showWiFi(id) {

	jumpTo("/wifiinfo.htm?id=" + ((id == null) ? "0" : id));
}

function showCell(id) {

	jumpTo("/cellinfo.htm?id=" + ((id == null) ? "0" : id));
}

function getString(name) {

	return $("#str-" + name).html();
}

function upTime(x) {

	if (x) {

		var d = Math.floor(x / (24 * 60 * 60));

		var h = Math.floor(x / (60 * 60)) % 24;

		var m = Math.floor(x / 60) % 60;

		if (d == 0) {

			return pad(h.toString()) + ":" + pad(m.toString());
		}

		if (d == 1) {

			return d.toString() + " day " + pad(h.toString()) + ":" + pad(m.toString());
		}

		return d.toString() + " days " + pad(h.toString()) + ":" + pad(m.toString());
	}

	return "00:00";
}

function pad(t) {

	t = "0" + t;

	return t.substr(t.length - 2);
}

// End of File
