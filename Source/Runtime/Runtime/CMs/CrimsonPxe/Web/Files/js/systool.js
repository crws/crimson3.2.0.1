
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// System Tool Support
//

// Data

var m_page_busy;

var m_page_cmd;

// Code

function pageMain() {

	initLogArea("/ajax/read-pipe.ajax?type=tool", false);

	m_page_busy = false;

	m_page_cmd = getParam("cmd");

	m_log_once = function () {

		m_page_busy = false;

		doEnables();
	};

	m_log_save = m_page_cmd + ".txt";

	if (m_page_cmd == "ping") {

		$("#tool-name").html("ICMP Ping");

		$("#show-tool-face").css("display", "");
	}

	if (m_page_cmd == "arping") {

		$("#tool-name").html("ARP Ping");

		$("#show-tool-face").css("display", "");
	}

	if (m_page_cmd == "tracert") {

		$("#tool-name").html("Trace Route");

		$("#show-tool-face").css("display", "");
	}

	if (m_page_cmd == "nslookup") {

		$("#tool-name").html("Name Lookup");
	}

	if (m_page_cmd == "tcptest") {

		$("#tool-name").html("Socket Test");

		$("#show-tool-port").css("display", "");
	}

	if (m_page_cmd == "getroute") {

		$("#tool-name").html("Get Route");

		$("#show-tool-host").css("display", "none");

		$("#show-tool-dest").css("display", "");

		$("#show-tool-face").css("display", "");
	}

	$("#tool-start").click(onStart);

	$("#tool-stop").click(onStop);

	$("#tool-host").on('input', doEnables);

	$("#tool-dest").on('input', doEnables);

	$("#tool-port").on('input', doEnables);

	$("#tool-face").change(doEnables);

	doEnables();

	$("#tool-main").css("display", "");
}

function onStart() {

	onLogReset();

	var toolStarted = function () {

		onLogShow(true);
	};

	var toolFailed = function () {

		m_page_busy = false;

		doEnables();
	};

	var url = "/ajax/systool-start.ajax";

	url += "?tab=" + m_tab_id;

	url += "&cmd=" + m_page_cmd;

	url += "&host=" + $("#tool-host").val();

	url += "&dest=" + $("#tool-dest").val();

	url += "&port=" + $("#tool-port").val();

	url += "&face=" + $("#tool-face").val();

	$.get({
		url: makeAjax(url),
		success: toolStarted,
		error: toolFailed,
		xhrFields: { withCredentials: true }
	});

	m_page_busy = true;

	doEnables();
}

function onStop() {

	var url = "/ajax/systool-stop.ajax";

	url += "?cmd=ping&tab=" + m_tab_id;

	$.get({
		url: makeAjax(url),
		xhrFields: { withCredentials: true }
	});
}

function doEnables() {

	var valid = true;

	if (m_page_cmd == "getroute") {

		if ($("#tool-dest").val() == "") {

			valid = false;
		}
	}
	else {

		if ($("#tool-host").val() == "") {

			valid = false;
		}
	}

	if (m_page_cmd == "tcptest") {

		if ($("#tool-port").val() == "") {

			valid = false;
		}
	}

	$("#tool-host").prop("disabled", m_page_busy);

	$("#tool-dest").prop("disabled", m_page_busy);

	$("#tool-port").prop("disabled", m_page_busy);

	$("#tool-face").prop("disabled", m_page_busy);

	$("#tool-start").prop("disabled", !valid || m_page_busy);

	$("#tool-stop").prop("disabled", !m_page_busy);
}

// End of File
