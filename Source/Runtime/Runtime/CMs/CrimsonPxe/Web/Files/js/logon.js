
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Log On Support
//

// Data

var path;
var hxr;
var busy;
var timeout;

// Code

function pageMain() {

	$("#inputPassword").on("keypress", function (e) { if (e.which == 13) submitLogon(); });

	busy = false;

	xhr = new XMLHttpRequest();
}

function submitLogon() {

	if (!busy) {

		var user = $("#inputUserName").val();

		var pass = $("#inputPassword").val();

		if (user.length && pass.length) {

			path = getPath("uri");

			if (path == null) {

				path = "/";
			}

			$(".btn-lg").attr("class", "btn btn-lg btn-primary btn-block disabled");

			xhr.open("GET", "/logon.sub?user=" + user + "&pass=" + pass, true);

			xhr.onload = logonComplete;

			xhr.onerror = logonFailed;

			alertHide();

			busy = true;

			xhr.send(null);
		}
	}
}

function logonComplete() {

	if (xhr.status == 200) {

		window.location = path;
	}
	else {

		if (xhr.status == 401) {

			alertShow("alert-danger", "Error", xhr.response);
		}

		$(".btn-lg").attr("class", "btn btn-lg btn-primary btn-block");

		busy = false;
	}
}

function logonFailed() {

	alertShow("alert-danger", "Error", "No Response From Server.");

	$(".btn-lg").attr("class", "btn btn-lg btn-primary btn-block");

	busy = false;
}

function alertHide() {

	if (timeout) {

		clearTimeout(timeout);

		$("#alertdiv").remove();

		timeout = null;
	}
}

function alertShow(coloring, tagword, message) {

	$("#alert-placeholder").html("<div id='alertdiv' class='alert " + coloring + " alert-dismissable'>" +
		"<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" +
		"<span><strong>" + tagword + "!</strong> " + message + "</span></div>"
	);

	timeout = setTimeout(alertHide, 2500);
}

function getPath(name) {

	var u = location.href;

	var s = "[\\?&]" + name + "=\(.*)";

	var r = new RegExp(s);

	var k = r.exec(u);

	return k == null ? null : k[1];
}

// End of File
