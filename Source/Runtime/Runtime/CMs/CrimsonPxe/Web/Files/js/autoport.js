
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Automatic Port Detection Tool
//

function autoDetectPorts(prop, done) {

	var run = function () {

		var url = "/ajax/tool-autoport.ajax";

		var okay = function (data) {

			if ("okay" in data) {

				if (data.okay) {

					var edit = false;

					for (var p = 1; p <= 3; p++) {

						var name = "port" + p.toString();

						if (name in prop && name in data) {

							if (prop[name] != data[name]) {

								prop[name] = data[name];

								edit = true;
							}
						}
					}

					hideWait();

					done(edit);

					return;
				}
			}

			done(false);
		};

		var fail = function () {

			display("AUTO-DETECT", "No valid reply from device.");

			hideWait();

			done(false);
		};

		showWait();

		$.get({
			url: makeAjax(url),
			success: okay,
			error: fail,
			xhrFields: { withCredentials: true },
		});
	};

	confirm("AUTO-DETECT", "Do you really want to set the port types to match this device?", run);
}

// End of File
