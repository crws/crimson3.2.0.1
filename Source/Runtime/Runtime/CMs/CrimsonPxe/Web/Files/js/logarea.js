
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Log Area Management
//

// Data

var m_log_from;
var m_log_init;
var m_log_show;
var m_log_lock;
var m_log_init;
var m_log_func;
var m_log_once;
var m_log_filt;
var m_log_high;
var m_log_pend;
var m_log_save;

// Code

function initLogArea(url, show, func) {

	m_log_from = url;

	m_log_init = true;

	m_log_show = false;

	m_log_lock = false;

	m_log_init = true;

	m_log_once = null;

	m_log_filt = null;

	m_log_high = false;

	m_log_pend = "";

	m_log_save = "log.txt";

	var lock = $("#log-lock");

	var wipe = $("#log-wipe");

	var save = $("#log-save");

	var filt = $("#log-filter");

	var high = $("#log-high");

	if (lock.length) {

		lock.change(function () { onLogLock($(this).prop("checked")); });
	}

	if (wipe.length) {

		wipe.click(function () { onLogWipe(); });
	}

	if (save.length) {

		save.click(function () { onLogSave(); });
	}

	if (filt.length) {

		filt.on("input", function () { onLogFilter(); });
	}

	if (high.length) {

		high.change(function () { onLogHighlight($(this).prop("checked")); });
	}

	if (func) {

		m_log_func = func;
	}

	if (show) {

		onLogShow(true);
	}
}

function sendLogRequest() {

	if (m_log_init) {

		$("#log-text").html("<p style='color: #00FF00'>Loading...</p>");
	}

	var url = m_log_from;

	url += "&tab=" + m_tab_id;

	url += "&init=" + (m_log_init ? 1 : 0);

	$.get({
		url: makeAjax(url),
		success: onLogOkay,
		error: onLogFailed,
		timeout: 0,
		xhrFields: { withCredentials: true },
	});
}

function onLogOkay(data) {

	if (m_log_show) {

		var jdiv = $("#log-text");

		var ddiv = jdiv[0];

		if (m_log_init) {

			jdiv.html("");

			m_log_init = false;
		}

		if (data != "") {

			var list = (m_log_pend + data).split('\n');

			for (var n = 0; n < list.length - 1; n++) {

				while (ddiv.childElementCount >= 5000) {

					ddiv.removeChild(ddiv.firstChild);
				}

				if (list[n] == "") {

					jdiv.append("<p>&nbsp;</p>\n");
				}
				else {

					var text = m_log_func ? m_log_func(list[n]) : list[n];

					if (!m_log_lock && m_log_filt) {

						var filt = filterLine(text);

						if (filt.length) {

							var show = $("#log-filt");

							show.append("<p>" + filt + "</p>\n");

							show[0].scrollTop = show[0].scrollHeight;
						}
					}

					jdiv.append("<p>" + text + "</p>\n");
				}
			}

			m_log_pend = list[list.length - 1];

			ddiv.scrollTop = ddiv.scrollHeight;
		}

		setTimeout(sendLogRequest, 250);
	}

	m_log_init = false;
}

function onLogFailed(error) {

	if (m_log_show) {

		if (!check401(error.status)) {

			if (m_log_once) {

				var jdiv = $("#log-text");

				if (m_log_init) {

					jdiv.html("");
				}

				jdiv.append("<p>&nbsp;</p><p style='color: #00FF00'>Done.</p>\n");

				m_log_once();
			}
			else {

				onLogWipe();

				$("#log-text").html("<p style='color: #FF0000'>Failed to Load</p>");

				setTimeout(sendLogRequest, 1000);
			}
		}
	}
}

function onLogReset() {

	if (m_log_show) {

		onLogWipe();

		m_log_show = false;
	}
}

function onLogShow(show) {

	if (m_log_show != show) {

		if ((m_log_show = show)) {

			m_log_init = true;

			m_log_lock = false;

			m_log_filt = null;

			m_log_pend = "";

			$("#log-filter").val("");

			$("#log-text").css("display", "");

			$("#log-snap").css("display", "none");

			$("#log-filt").css("display", "none");

			$("#log-area").css("display", "");

			sendLogRequest();
		}
		else {

			$("#log-area").css("display", "none");
		}
	}
}

function onLogLock(lock) {

	m_log_lock = lock;

	var snap = $("#log-snap");

	var text = $("#log-text");

	if (m_log_lock) {

		snap.html(text.html());

		showElement(snap, true);

		snap[0].scrollTop = text[0].scrollTop;

		snap[0].scrollLeft = text[0].scrollLeft;

		showElement(text, false);
	}
	else {

		showElement(text, true);

		text[0].scrollTop = text[0].scrollHeight;

		text[0].scrollLeft = 0;

		showElement(snap, false);

		snap.html("");
	}
}

function onLogWipe() {

	$("#log-text").html("");

	if (m_log_lock) {

		$("#log-lock").prop("checked", false);

		onLogLock(false);
	}

	if (m_log_filt) {

		$("#log-filter").val("");

		onLogFilter();
	}
}

function onLogSave() {

	var txt = (m_log_lock ? $("#log-snap") : $("#log-text")).html();

	txt = txt.replace(/<p[^>]*>/g, "");

	txt = txt.replace(/<\/p>/g, "\r");

	txt = txt.replace(/&nbsp;/g, "");

	var hid = $("#hidden");

	hid.empty();

	var url = "data:text/plain," + encodeURIComponent(txt);

	var act = $("<a/>").appendTo(hid);

	act.attr("href", url).attr("download", m_log_save);

	act[0].click();
}

function onLogHighlight(high) {

	m_log_high = high;

	$("#log-filter").attr("placeholder", high ? "Enter Search Text" : "Enter Filter Text");

	onLogFilter();
}

function onLogFilter() {

	var txt = $("#log-filter").val();

	var dst = $("#log-filt");

	var src = $(m_log_lock ? "#log-snap" : "#log-text");

	if (txt == "") {

		m_log_filt = null;

		dst.css("display", "none");

		src.css("display", "");

		return false;
	}
	else {

		if (m_log_lock) {

			dst.removeClass("debug-text").addClass("debug-snap");
		}
		else {

			dst.addClass("debug-text").removeClass("debug-snap");
		}

		m_log_filt = txt.toLowerCase();

		copyWithFilter(dst, src);

		src.css("display", "none");

		dst.css("display", "");

		dst[0].scrollTop = dst[0].scrollHeight;

		return true;
	}
}

function copyWithFilter(dst, src) {

	var list = src.html().split("\n");

	var done = "";

	for (var n = 0; n < list.length; n++) {

		var line = list[n];

		var filt = filterLine(line);

		if (filt.length) {

			done += filt;

			done += "\n";
		}
	}

	dst.html(done);
}

function filterLine(line) {

	var find = line.toLowerCase().indexOf(m_log_filt);

	if (find >= 0) {

		if (m_log_high) {

			if (line.substr(0, 3) == "<p>") {

				line = line.replace("<p>", "");

				line = line.replace("</p>", "");

				return "<p><span style='background: #505080'>" + line + "</span></p>";
			}

			return "<span style='background: #505080'>" + line + "</span>";
		}
		else {

			var done = "";

			done += line.substr(0, find);

			done += "<span style='background: #505080'>";

			done += line.substr(find, m_log_filt.length);

			done += "</span>";

			done += line.substr(find + m_log_filt.length);

			return done;
		}
	}

	return m_log_high ? line : "";
}

function showElement(div, show) {

	div.css("display", show ? "" : "none");

	if (!show) {

		onLogFilter();
	}
}

// End of File
