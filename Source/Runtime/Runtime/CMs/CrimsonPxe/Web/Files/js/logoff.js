
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Log Off Support
//

// Code

function pageMain() {

	var xhr = new XMLHttpRequest();

	xhr.open("GET", "/logoff.sub?nc=" + (new Date()).getTime(), true);

	xhr.send(null);
}

// End of File
