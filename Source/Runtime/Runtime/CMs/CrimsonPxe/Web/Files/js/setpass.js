
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Set Password
//

// Code

function pageMain() {

	$("#p1").on("input", onChange);
	$("#p2").on("input", onChange);
	$("#p3").on("input", onChange);

	onChange();
}

function onChange() {

	var p1 = $("#p1").val();
	var p2 = $("#p2").val();
	var p3 = $("#p3").val();

	$("#b").attr("disabled", !p1.length || !p2.length || !p3.length || p2 != p3);

	$("#m").html((p2 == p3) ? "" : "<i>The new passwords do not match.</i>");
}

function createPassword() {

	var url = "/ajax/mkpass.ajax";

	$.get({
		url: makeAjax(url),
		success: makeComplete,
		error: subFailed,
		xhrFields: { withCredentials: true }
	});
}

function makeComplete(data) {

	$("#p2").val("");

	$("#p3").val("");

	onChange();

	navigator.clipboard.writeText(data);

	alertShow("alert-success", "Success", "The generated password has been placed on the clipboard.");
}

function submitSetPass() {

	var p1 = $("#p1").val();

	var p2 = $("#p2").val();

	if (p1.length && p2.length) {

		$("#b").attr("disabled", true);

		var url = "/ajax/setpass.htm?p1=" + p1 + "&p2=" + p2;

		$.get({
			url: makeAjax(url),
			success: subComplete,
			error: subFailed,
			xhrFields: { withCredentials: true }
		});
	}
}

function subComplete(data) {

	if (data == "OK") {

		var f = function () { jumpTo("/default.htm"); }

		alertShow("alert-success", "Success", "The password has been sucessfully changed.", f);
	}
	else {

		alertShow("alert-danger", "Error", data);
	}
}

function subFailed(error) {

	if (!check401(error.status)) {

		alertShow("alert-danger", "Error", "The server did not respond correctly.");

		$("#b").attr("disabled", false);
	}
}

function alertShow(coloring, tagword, message, func) {

	$("#alert-placeholder").html("<div id='alertdiv' class='col-md-6 alert " + coloring + " alert-dismissable'>" +
		"<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" +
		"<span><strong>" + tagword + "!</strong> " + message + "</span></div>"
	);

	var hide = function () {

		if (func) func();

		$("#alertdiv").remove();
	};

	setTimeout(hide, 2500);
}

// End of File
