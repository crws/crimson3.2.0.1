
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Home Page
//

// Data

var m_page_init;

var m_device_upgrade;

// Code

function pageMain() {

	m_page_init = true;

	m_device_upgrade = false;

	sendInfoRequest();
}

function sendInfoRequest() {

	var url = "/ajax/device-status.ajax";

	url += "?tab=" + m_tab_id;

	url += "&init=" + (m_page_init ? 1 : 0);

	$.get({
		url: makeAjax(url),
		success: onInfoOkay,
		error: onInfoFailed,
		timeout: m_page_init ? 15000 : 0,
		xhrFields: { withCredentials: true },
	});
}

function onInfoOkay(data) {

	if (data.okay) {

		showSection(data, "device", $("#device-table"), null, null, showDevice);

		showSection(data, "dumps", $("#core-table"), $("#core-show"), $("#core-head"), showDumps);
	}

	if (!m_device_upgrade) {

		showDeviceUpgrade();

		m_device_upgrade = true;
	}

	if (!("defer" in data) || !data.defer) {

		setTimeout(sendInfoRequest, 1000);
	}
	else {

		m_page_init = false;

		setTimeout(sendInfoRequest, 100);
	}

	$("#page-area").css("display", "");
}

function showSection(data, name, table, show, hide, func) {

	if (!show) {

		show = table;
	}

	if (name in data) {

		func(data[name]);

		if (hide) {

			hide.css("display", "none");
		}

		show.css("display", "");
	}
	else {

		if (hide) {

			hide.css("display", "");
		}

		show.css("display", "none");
	}
}

function showDevice(device) {

	var body = $("#device-body");

	body.empty();

	addProp(body, device, "model", "Device Model");

	addProp(body, device, "group", "Enabled Group");

	addProp(body, device, "serial", "Serial Number");

	addProp(body, device, "version", "Software Version");

	addProp(body, device, "os", "Operating System");

	addProp(body, device, "sled1", "Detected Sled 1", sledType);

	addProp(body, device, "sled2", "Detected Sled 2", sledType);

	addProp(body, device, "sled3", "Detected Sled 3", sledType);

	addProp(body, device, "suptime", "Crimson Up Time", upTime);

	addProp(body, device, "duptime", "Device Up Time", upTime);
}

function showDumps(dumps) {

	var body = $("#core-body");

	var tail = $("#core-tail");

	body.empty();

	tail.empty();

	for (var n = 0; n < dumps.length; n++) {

		if ("file" in dumps[n]) {

			var file = dumps[n].file;

			var dots = file.split('.');

			var list = dots[0].split('-');

			var name = list[3];

			var date = list[1] + ' ' + list[2];

			var link = "<a href='/ajax/get-dump.ajax?n=" + file + "'>" + name + "</a>";

			var tr = $("<tr/>").appendTo(body);

			$("<td/>").html(link).appendTo(tr);

			$("<td/>").html(date).appendTo(tr);
		}
	}

	if (m_access == 0) {

		var bd = $("<div/>").appendTo(tail);

		var b1 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-danger").html("Clear Core Dumps").appendTo(bd);

		b1.on('click', function () { clearDumps(); });
	}
}

function clearDumps() {

	var url = "/ajax/clear-dumps.ajax";

	$.get({
		url: makeAjax(url),
		xhrFields: { withCredentials: true },
	});
}

function onInfoFailed(error) {

	if (!check401(error.status)) {

		$("#page-area").css("display", "none");

		m_page_init = true;

		setTimeout(sendInfoRequest, 5000);
	}
}

function addProp(tb, data, name, label, func) {

	if (name in data) {

		var dv = func ? func(data[name]) : data[name];

		if (dv != null) {

			var tr = $("<tr/>").appendTo(tb);

			$("<td/>").html("<b>" + label + "</b>").css("width", "40%").appendTo(tr);

			var td = $("<td/>").css("width", "60%").appendTo(tr);

			td.html(dv.toString());
		}
	}
}

function sledType(name) {

	if (name == "ETH") {

		return "Dual Ethernet";
	}

	if (name == "WIFIBT") {

		return "Wi-Fi Interface";
	}

	if (name == "CELL") {

		return "Cellular Modem";
	}

	if (name == "SERIAL") {

		return "Dual Serial";
	}

	return null;
}

function uploadKeyFile() {

	var install = function () {

		var hid = $("#hidden");

		hid.empty();

		var sel = $("<input/>").appendTo(hid);

		sel.attr("type", "file").attr("accept", "text/plain");

		sel.on("input", function (e) {

			var file = this.files[0];

			var read = new FileReader();

			read.onerror = function (e) {

				display("READ FAILED", "Unable to read from file.");
			};

			read.onload = function (e) {

				var text = this.result;

				var url = "/ajax/keywrite.ajax";

				var fail = function () {

					display("SEND FAILED", "No response from server.");
				};

				var okay = function (text) {

					var reset = function () {

						$.get({
							url: makeAjax("/ajax/restart.ajax"),
							success: waitRestart,
							xhrFields: { withCredentials: true }
						});
					};

					if (text == "OK") {

						display("COMMIT COMPLETE", "<p>The new key file has been committed and Crimson will now restart.</p>", reset);
					}
					else {

						display("FAILED TO COMMIT", "The key file is not valid for this device.");
					}
				};

				$.post({
					url: makeAjax(url),
					data: text,
					success: okay,
					error: fail,
					dataType: 'text',
					contentType: 'text/plain',
					xhrFields: { withCredentials: true }
				});
			};

			read.readAsText(file);
		});

		sel.click();
	};

	confirm("INSTALL UNLOCK FILE", "<p>This operation will require a system restart.</p><p>Do you really want to continue?</p>", install);
}

function restartSystem() {

	var reset = function () {

		$.get({
			url: makeAjax("/ajax/restart.ajax?t=0"),
			xhrFields: { withCredentials: true }
		});
	};

	confirm("RESTART SYSTEM", "Do you really want to restart the system?", reset);
}

function rebootSystem() {

	var reset = function () {

		$.get({
			success: waitRestart,
			url: makeAjax("/ajax/restart.ajax?t=1"),
			xhrFields: { withCredentials: true }
		});
	};

	confirm("REBOOT SYSTEM", "Do you really want to reboot the system without an orderly shutdown?", reset);
}

function factoryReset() {

	var reset = function () {

		$.get({
			success: waitRestart,
			url: makeAjax("/ajax/restart.ajax?t=3"),
			xhrFields: { withCredentials: true }
		});
	};

	var again = function () {

		confirm("FACTORY RESET", "Are you really sure?", reset);
	};

	confirm("FACTORY RESET", "Do you really want to reset the device to its factory defaults?", again);
}

function showDeviceUpgrade() {

	var div = $("#dev-upgrade");

	if (m_access == 0) {

		$("<h3/>").html("Device Operations").appendTo(div);

		var d1 = $("<div/>").appendTo(div);

		var b1 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-primary").html("Get Device Key").appendTo(d1);

		var b2 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-warning").html("Install Unlock File").appendTo(d1);

		var b3 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-warning").html("Restart System").appendTo(d1);

		var b4 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-danger").html("Force Reboot").appendTo(d1);

		var b5 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-danger").html("Factory Reset").appendTo(d1);

		b1.click(function () { window.open("/ajax/devread.ajax"); });

		b2.click(uploadKeyFile);

		b3.click(restartSystem);

		b4.click(rebootSystem);

		b5.click(factoryReset);
	}
}

// End of File
