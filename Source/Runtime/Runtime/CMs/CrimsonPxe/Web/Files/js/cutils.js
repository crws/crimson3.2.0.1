

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Configuration Utilities
//

// Code

function pageMain() {

	addDevCon();

	addAppCon();
}

function addDevCon() {

	var body = $("#devcon-body");

	for (var n = 0; n < 5; n++) {

		var name, rtag, mode;

		switch (n) {

			case 0:
				name = "Hardware Configuration";
				rtag = "h";
				mode = (m_access <= 1) ? 2 : 1;
				break;

			case 1:
				name = "System Configuration";
				rtag = "s";
				mode = (m_access <= 1) ? 2 : 1;
				break;

			case 2:
				name = "Device Personality";
				rtag = "p";
				mode = (m_access <= 3) ? 2 : 1;
				break;

			case 3:
				name = "User Configuration";
				rtag = "u";
				mode = (m_access == 0) ? 2 : 0;
				break;

			case 4:
				name = "All Checked Items";
				rtag = "d";
				mode = 2;
				break;
		}

		if (mode) {

			var row = $("<tr/>").appendTo(body);

			var td1 = $("<td/>").appendTo(row);

			var td2 = $("<td/>").appendTo(row);

			var td3 = $("<td/>").appendTo(row);

			if (n < 4) {

				var check = $("<input/>").appendTo(td1);

				check.addClass("custom-control-input").attr("type", "checkbox").attr("id", "include-" + rtag);

				if (n < 3) {

					check.prop("checked", true);
				}

			}

			td2.html(name);

			td3.css("white-space", "nowrap");

			var bd = $("<div/>").appendTo(td3);

			addIcon(bd, name, rtag, "fa-arrow-up", "Upload", "#808CC8", mode == 2, doUpload);

			addIcon(bd, name, rtag, "fa-arrow-down", "Download", "#808CC8", mode >= 1, doDownload);

			addIcon(bd, name, rtag, "fa-times", "Clear", "#A00000", mode == 2, doReset);
		}
	}

	$("[data-toggle='tooltip']").tooltip({ trigger: 'hover' });

	$("#page-area").css("display", "");
}

function addIcon(div, name, rtag, icon, tip, color, enable, action) {

	var span = $("<span/>").appendTo(div);

	span.addClass("tb-icon fa").addClass(icon);

	span.attr("data-toggle", "tooltip").attr("title", tip);

	span.css("color", enable ? color : "#C0C0C0");

	span.data("name", name);

	span.data("rtag", rtag);

	if (action && enable) {

		span.on("click", action);
	}
}

function doUpload(event) {

	var rtag = $(event.target).data("rtag");

	var name = $(event.target).data("name");

	var url = "/ajax/syswrite.ajax?f=config&t=" + rtag;

	if (rtag == "d") {

		var filter = getFilter();

		if (filter == "") {

			display("UPLOAD " + name.toUpperCase(), "<p>You do not have any items checked.</p>");

			return;
		}

		url += "&i=" + filter;
	}

	var hid = $("#hidden");

	hid.empty();

	var sel = $("<input/>").appendTo(hid);

	sel.attr("type", "file").attr("accept", "application/json");

	sel.on("input", function (e) {

		var file = this.files[0];

		var read = new FileReader();

		read.onerror = function (e) {

			display("READ FAILED", "Unable to read from file.");
		};

		read.onload = function (e) {

			var load = this;

			var install = function () {

				showWait();

				var text = load.result;

				var fail = function () {

					hideWait();

					display("SEND FAILED", "No valid response from server.");
				};

				var okay = function (text) {

					hideWait();

					if (rtag == "d") {

						display("UPLOAD COMPLETE", "<p>All checked items have been uploaded.</p>");
					}
					else {

						display("UPLOAD COMPLETE", "<p>The " + name + " has been uploaded.</p>");
					}
				};

				$.post({
					url: makeAjax(url),
					data: text,
					success: okay,
					error: fail,
					dataType: 'text',
					contentType: 'application/json',
					xhrFields: { withCredentials: true }
				});
			}

			var text = "";

			text += "<p>This operation will overwrite the existing configuration.</p>";

			text += "<p>Do you want to continue?</p>"

			confirm("UPLOAD " + name.toUpperCase(), text, install);
		};

		read.readAsText(file);
	});

	sel.click();
}

function doDownload(event) {

	var rtag = $(event.target).data("rtag");

	var name = $(event.target).data("name");

	var url = "/ajax/sysread.ajax?f=config&t=" + rtag;

	if (rtag == "d") {

		var filter = getFilter();

		if (filter == "") {

			display("DOWNLOAD " + name.toUpperCase(), "<p>You do not have any items checked.</p>");

			return;
		}

		url += "&i=" + filter;
	}

	window.open(url);
}

function doReset(event) {

	var reset = function () {

		showWait();

		var fail = function () {

			hideWait();

			display("SEND FAILED", "No response from server.");
		};

		var okay = function (text) {

			hideWait();

			if (rtag == "d") {

				display("CLEAR COMPLETE", "<p>All checked items have been cleared.</p>");
			}
			else {

				display("CLEAR COMPLETE", "<p>The " + name + " has been cleared.</p>");
			}
		};

		$.post({
			url: makeAjax(url),
			data: "",
			success: okay,
			error: fail,
			dataType: 'text',
			contentType: 'application/json',
			xhrFields: { withCredentials: true }
		});
	};

	var rtag = $(event.target).data("rtag");

	var name = $(event.target).data("name");

	var url = "/ajax/sysreset.ajax?f=config&t=" + rtag;

	if (rtag == "d") {

		var filter = getFilter();

		if (filter == "") {

			display("CLEAR " + name.toUpperCase(), "<p>You do not have any items checked.</p>");

			return;
		}

		url += "&i=" + filter;
	}

	var again = function () {

		confirm("CLEAR " + name.toUpperCase(), "Are you really sure?", reset);
	};

	var text = "";

	text += "<p>This operation will clear the existing configuration.</p>";

	text += "<p>Do you want to continue?</p>"

	confirm("CLEAR " + name.toUpperCase(), text, again);
}

function getFilter() {

	var filter = "";

	for (var n = 0; n < 4; n++) {

		var rtag;

		switch (n) {

			case 0:
				rtag = "h";
				break;

			case 1:
				rtag = "s";
				break;

			case 2:
				rtag = "p";
				break;

			case 3:
				rtag = "u";
				break;
		}

		var check = $("#include-" + rtag);

		if (check.length && check.prop("checked")) {

			filter += rtag;
		}
	}

	return filter;
}

function addAppCon() {

	if (m_access == 0) {

		var di = $("#appcon");

		var bd = $("<div/>").appendTo(di);

		var b1 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-warning").html("Upload Image").appendTo(bd);

		var b2 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-danger").html("Clear Config").appendTo(bd);

		b1.off("click").on("click", function () { uploadCrimson(); });

		b2.off("click").on("click", function () { clearCrimson(); });

		di.css("display", "");
	}
}

function uploadCrimson() {

	var url = "/ajax/putimage.ajax";

	var hid = $("#hidden");

	hid.empty();

	var sel = $("<input/>").appendTo(hid);

	sel.attr("type", "file").attr("accept", ".ci3");

	sel.on("input", function (e) {

		var file = this.files[0];

		var read = new FileReader();

		read.onerror = function (e) {

			display("READ FAILED", "Unable to read from file.");
		};

		read.onload = function (e) {

			var load = this;

			var install = function () {

				var post;

				var quit = false;

				var data = load.result;

				var fail = function () {

					hideProgress();

					if (!quit) {

						display("SEND FAILED", "No valid response from server.");
					}
				};

				var okay = function (text) {

					hideProgress();

					display("UPLOAD COMPLETE", "<p>The image file has been uploaded and the device will now restart.</p>", waitRestart);
				};

				var init = function () {

					var xhr = $.ajaxSettings.xhr();

					xhr.upload.onprogress = function (e) {

						setProgress(Math.floor(e.loaded / e.total * 100));
					};

					xhr.withCredentials = true;

					return xhr;
				};

				var stop = function () {

					quit = true;

					post.abort();
				};

				showProgress(stop);

				post = $.post({

					url: makeAjax(url),
					data: data,
					xhr: init,
					success: okay,
					error: fail,
					dataType: 'text',
					processData: false,
					contentType: 'application/octet-stream'
				});
			};

			var again = function () {

				confirm("UPLOAD IMAGE", "Are you really sure?", install);
			};

			var text = "";

			text += "<p>This operation will overwrite the existing configuration and firmware.</p>";

			text += "<p>The device will be restarted to apply the changes.</p>";

			text += "<p>Do you want to continue?</p>";

			confirm("UPLOAD IMAGE", text, again);
		};

		read.readAsArrayBuffer(file);
	});

	sel.click();
}

function clearCrimson() {

	var reset = function () {

		$.get({
			success: waitRestart,
			url: makeAjax("/ajax/restart.ajax?t=2"),
			xhrFields: { withCredentials: true }
		});
	};

	var again = function () {

		confirm("CLEAR CRIMSON CONFIGURATION", "Are you really sure?", reset);
	};

	var text = "";

	text += "Do you want to clear the Crimson configuration and restart the system?";

	confirm("CLEAR CRIMSON CONFIGURATION", text, again);
}

// End of File
