
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RadiusFrame_HPP

#define	INCLUDE_RadiusFrame_HPP

//////////////////////////////////////////////////////////////////////////
//
// Radius Frame Structure
//

#pragma pack(1)

struct RADIUS_FRAME
{
	BYTE	m_bCode;
	BYTE	m_bIdent;
	WORD	m_wLength;
	BYTE	m_bAuth[16];
	BYTE	m_bAttr[4];
};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Radius Frame Wrapper
//

class CRadiusFrame : public RADIUS_FRAME
{
public:
	// Operation Codes
	enum
	{
		codeAccessRequest = 1,
		codeAccessAccept  = 2
	};

	// Attribute Identifiers
	enum
	{
		attrUserName       = 1,
		attrUserPassword   = 2,
		attrChapPassword   = 3,
		attrServiceType    = 6,
		attrFramedProtocol = 7
	};

	// Byte Order Conversion
	void NetToHost(void);
	void HostToNet(void);

	// Operations
	void Create(BYTE bCode, BYTE bIdent);
	bool Validate(PCSTR pSecret, CRadiusFrame const &Send);
	void AddAttribute(BYTE bType, PCBYTE pData, UINT uData);
	void AddAttribute(BYTE bType, PCSTR pText);
	void AddPassword(PCSTR pSecret, PCSTR pPassword);
	void AddChapPassword(PCSTR pPassword);
	void AddChap(CByteArray const &Challenge, CByteArray const &Response);
	void AddPppAttributes(void);

	// Attribute Access
	bool HasAttribute(UINT uPos);
	bool GetAttribute(UINT &uPos, BYTE &bType, UINT &uSize, PCBYTE &pData);

private:
	// Private Constructor
	CRadiusFrame(void) {}
};

// End of File

#endif
