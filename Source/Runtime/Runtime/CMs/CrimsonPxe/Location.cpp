
#include "intern.hpp"

#include "Location.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Location Service
//

// Constructor

CLocation::CLocation(CJsonConfig *pJson)
{
	m_pLock = Create_Mutex();

	m_Info.m_uFix = 0;

	m_Info.m_uSeq = 0;

	ApplyConfig(pJson);

	StdSetRef();

	piob->RegisterSingleton("c3.location", 0, this);
}

// Destructor

CLocation::~CLocation(void)
{
	piob->RevokeSingleton("c3.location", 0);

	AfxRelease(m_pLock);
}

// IUnknown

HRESULT CLocation::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ILocationSource);

	StdQueryInterface(ILocationSource);

	return E_NOINTERFACE;
}

ULONG CLocation::AddRef(void)
{
	StdAddRef();
}

ULONG CLocation::Release(void)
{
	StdRelease();
}

// ILocationSource

BOOL CLocation::GetLocationData(CLocationSourceInfo &Info)
{
	CAutoLock Lock(m_pLock);

	if( m_uSource >= 2 ) {

		AfxGetAutoObject(pLocation, "net.location", 0, ILocationSource);

		if( pLocation ) {

			CLocationSourceInfo Read;

			pLocation->GetLocationData(Read);

			if( Read.m_uFix != m_Info.m_uFix || GetDistance(m_Info, Read) >= m_Delta ) {

				Read.m_uSeq = m_Info.m_uSeq + 1;

				m_Info      = Read;
			}
		}
		else {
			m_Info.m_uFix = 0;

			m_Info.m_uSeq = 0;
		}
	}

	if( m_Info.m_uFix >= 2 ) {

		Info = m_Info;

		return TRUE;
	}

	Info.m_uFix = 0;

	Info.m_uSeq = 0;

	return TRUE;
}

BOOL CLocation::GetLocationTime(CTimeSourceInfo &Info)
{
	return FALSE;
}

// Implementation

void CLocation::ApplyConfig(CJsonConfig *pJson)
{
	if( pJson ) {

		if( (m_uSource = pJson->GetValueAsUInt("source", 0, 0, 100)) ) {

			if( m_uSource == 1 ) {

				m_Info.m_Lat  = pJson->GetValueAsDouble("lat", 0);

				m_Info.m_Long = pJson->GetValueAsDouble("long", 0);

				m_Info.m_uSeq = 1;

				m_Info.m_uFix = 2;
			}
		}

		m_Delta = pJson->GetValueAsInt("thresh", 200, 0, 100000);
	}
}

double CLocation::GetDistance(CLocationSourceInfo const &a, CLocationSourceInfo const &b)
{
	double base = 20903520;

	double dLat = ToRad(b.m_Lat  - a.m_Lat);
	
	double dLon = ToRad(b.m_Long - a.m_Long);

	double Lat1 = ToRad(a.m_Lat);
	
	double Lat2 = ToRad(b.m_Lat);

	double k1 = sin(dLat/2) * sin(dLat/2) + sin(dLon/2) * sin(dLon/2) * cos(Lat1) * cos(Lat2);

	double ds = base * 2 * atan2(sqrt(k1), sqrt(1-k1));

	if( a.m_uFix == 3 ) {

		double da = a.m_Alt - b.m_Alt;

		ds = sqrt((ds*ds)+(da*da));
	}

	return ds;
}

double CLocation::ToRad(double deg)
{
	return deg / 180.0 * 3.14159265;
}

// End of File
