
#include "Intern.hpp"

#include "StdServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Timing Constants
//

static UINT const timeInitTimeout = (5*60*1000);

//////////////////////////////////////////////////////////////////////////
//
// Standard RFC Server
//

// Constructor

CStdServer::CStdServer(void)
{
	m_pTls = NULL;
}

// Destructor

CStdServer::~CStdServer(void)
{
	if( m_pTls ) {

		m_pTls->Release();
	}
}

// Transport

BOOL CStdServer::SendReply(UINT uCode, PCTXT pText)
{
	char sBuff[128];

	SPrintf(sBuff, "%3.3u %s\r\n", uCode, pText);

	return Send(sBuff);
}

BOOL CStdServer::RecvCommand(void)
{
	for( ;;) {

		CAutoBuffer pBuff;

		if( m_pCmdSock->Recv(pBuff) == S_OK ) {

			PCBYTE pData = pBuff->GetData();

			UINT   uSize = pBuff->GetSize();

			while( uSize-- ) {

				OnCommand(char(*pData++));
			}

			if( m_uCmdState == 2 ) {

				// Extra null needed for parsing.

				m_sCmdData[m_uCmdPtr++] = 0;

				m_sCmdData[m_uCmdPtr++] = 0;

				m_uCmdState = 0;

				m_uCmdPtr   = 0;

				AfxTrace("<<< %s\n", m_sCmdData);

				return TRUE;
			}
		}
		else {
			UINT uPhase = 0;

			m_pCmdSock->GetPhase(uPhase);

			if( uPhase == PHASE_OPEN ) {

				return FALSE;
			}

			AbortCmdSocket();

			return FALSE;
		}
	}

	return FALSE;
}

void CStdServer::OnCommand(char cData)
{
	if( m_uCmdState == 0 ) {

		if( cData == '\n' ) {

			m_uCmdState = 2;

			return;
		}

		if( isprint(cData) ) {

			m_sCmdData[m_uCmdPtr++] = cData;

			if( m_uCmdPtr == sizeof(m_sCmdData) - 1 ) {

				m_uCmdState = 1;
			}

			return;
		}
	}

	if( m_uCmdState == 1 ) {

		if( cData == '\n' ) {

			m_uCmdState = 2;

			return;
		}
	}
}

// Socket Management

BOOL CStdServer::OpenCmdSocket(WORD wPort, BOOL fLarge)
{
	if( !m_pCmdSock ) {

		AfxNewObject("sock-tcp", ISocket, m_pCmdSock);

		if( m_pCmdSock ) {

			if( fLarge ) {

				UINT uOption = OPT_SEND_QUEUE;

				UINT uValue  = 255;

				m_pCmdSock->SetOption(uOption, uValue);
			}

			if( m_pCmdSock->Listen(wPort) == S_OK ) {

				m_wPort     = wPort;

				m_uCmdState = 0;

				m_uCmdPtr   = 0;

				return TRUE;
			}

			AbortCmdSocket();
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CStdServer::WaitCmdSocket(void)
{
	if( m_pCmdSock ) {

		UINT Phase;

		m_pCmdSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {

			return TRUE;
		}

		if( Phase == PHASE_ERROR ) {

			return TRUE;
		}

		if( Phase == PHASE_CLOSING ) {

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CStdServer::SwitchCmdSocket(void)
{
	if( CreateTlsContext() ) {

		m_pCmdSock = m_pTls->CreateSocket(m_pCmdSock);

		if( m_pCmdSock ) {

			if( m_pCmdSock->Listen(m_wPort) == S_OK ) {

				SetTimer(timeInitTimeout);

				while( GetTimer() ) {

					UINT Phase;

					m_pCmdSock->GetPhase(Phase);

					if( Phase == PHASE_OPENING ) {

						Sleep(10);

						continue;
					}

					if( Phase == PHASE_OPEN ) {

						return TRUE;
					}

					break;
				}
			}
		}
	}

	return FALSE;
}

BOOL CStdServer::CreateTlsContext(void)
{
	AfxGetAutoObject(pMatrix, "tls", 0, ITlsLibrary);

	if( pMatrix ) {

		if( !m_pTls ) {

			pMatrix->CreateServerContext(m_pTls);

			if( m_pTls ) {

				AfxGetAutoObject(pPxe, "pxe", 0, ICrimsonPxe);

				if( pPxe->GetDefCertStep() ) {

					CByteArray Data[2];

					CString    Pass;

					if( pPxe->GetDefCertData(Data, Pass) ) {

						if( m_pTls->LoadServerCert(Data[0].data(),
									   Data[0].size(),
									   Data[1].data(),
									   Data[1].size(),
									   Pass
						) ) {
							return TRUE;
						}
					}
				}

				m_pTls->Release();

				m_pTls = NULL;
			}

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

// End of File
