
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandDatabase_HPP

#define	INCLUDE_NandDatabase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include <CxNand.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

struct CItemInfo;

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Database
//

class CNandDatabase : public CNandClient, public IDatabase
{
public:
	// Constructor
	CNandDatabase(CNandBlock const &Start, CNandBlock const &End, UINT uFram, UINT uBase, UINT uPool);

	// Destructor
	~CNandDatabase(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDatabase
	void   METHOD Init(void);
	void   METHOD Clear(void);
	BOOL   METHOD IsValid(void);
	void   METHOD SetValid(BOOL fValid);
	void   METHOD SetRunning(BOOL fRun);
	BOOL   METHOD GarbageCollect(void);
	BOOL   METHOD GetVersion(PBYTE pGuid);
	DWORD  METHOD GetRevision(void);
	BOOL   METHOD SetVersion(PCBYTE pData);
	BOOL   METHOD SetRevision(DWORD dwRes);
	BOOL   METHOD CheckSpace(UINT uSize);
	BOOL   METHOD WriteItem(CItemInfo const &Info, PCVOID pData);
	BOOL   METHOD GetItemInfo(UINT uItem, CItemInfo &Info);
	PCVOID METHOD LockItem(UINT uItem, CItemInfo &Info);
	PCVOID METHOD LockItem(UINT uItem);
	void   METHOD PendItem(UINT uItem, BOOL fPend);
	void   METHOD LockPendingItems(BOOL fLock);
	void   METHOD FreeItem(UINT uItem);

protected:
	// Constants
	static UINT  const dataItemCount = 4096;
	static DWORD const magicProps	 = 0x58444E49;
	static DWORD const magicItem     = 0x4D455449;
	static DWORD const magicBody     = 0x59444F42;
	static DWORD const magicEmpty	 = 0xFFFFFFFF;
	static DWORD const magicFRAM     = 0x12190846;
	static DWORD const clearFRAM     = 0x22190846;

	// Page Header
	struct CPageHeader
	{
		DWORD		Magic;
		DWORD		Generation;
	};

	// Property Page
	struct CPropsPage
	{
		CPageHeader	Page;
		GUID		Guid;
		DWORD		Revision;
	};

	// Item Data Page
	struct CItemDataPage
	{
		CPageHeader	Page;
		DWORD		Index;
		DWORD		Pad[1];
	};

	// Item Header Page
	struct CItemHeadPage
	{
		CItemDataPage	Header;
		DWORD		Class;
		DWORD		CRC;
		DWORD		Size;
		DWORD		Comp;
	};

	// Item State
	struct CState
	{
		CNandPage	m_Page;
		DWORD		m_Class;
		DWORD		m_CRC;
		DWORD		m_Size;
		DWORD		m_Comp;
		BOOL		m_fValid;
		UINT		m_uGeneration;
		UINT		m_uLock;
		PBYTE		m_pData;
		CState *	m_pNext;
		CState *	m_pPrev;
	};

	// Type Definitions
	typedef	CPageHeader	    * PPAGE;
	typedef	CPageHeader   const * PCPAGE;
	typedef	CPropsPage          * PPROPS;
	typedef	CPropsPage    const * PCPROPS;
	typedef	CItemDataPage       * PDATA;
	typedef	CItemDataPage const * PCDATA;
	typedef	CItemHeadPage       * PHEAD;
	typedef	CItemHeadPage const * PCHEAD;

	// Data
	ULONG		m_uRefs;
	ISerialMemory * m_pFram;
	UINT		m_uFram;
	UINT		m_uBase;
	IMutex	      * m_pMutex;
	UINT		m_uFreePages;
	UINT		m_uPoolSize;
	UINT		m_uPoolUsed;
	CNandPage	m_PageProps;
	UINT		m_uPropsGen;
	CState	      * m_pState;
	CState	      * m_pHead;
	CState	      * m_pTail;
	DWORD		m_Revision;
	GUID		m_Guid;

	// Item State Array
	bool MountItems(void);
	bool FindItems(void);
	void CheckPoolUsage(void);

	// Property Page Support
	bool CommitPropsPage(void);

	// Obsolete Page Support
	bool EraseObsoleteBlocks(void);
	bool IsBlockObsolete(CNandBlock Block);
	bool IsPageObsolete(CNandPage Page);
	bool IsPageEmpty(CNandPage Page);
	bool FindEmptyBlock(CNandBlock &Block);
	bool FindValidBlock(CNandBlock &Block);
	bool CopyBlock(CNandBlock Dest, CNandBlock From);

	// Overridables
	bool OnPageReloc(PCBYTE pData, CNandPage const &Dest);

	// Implementation
	BOOL IsClear(void);
	void SetClear(void);
};

// End of File

#endif
