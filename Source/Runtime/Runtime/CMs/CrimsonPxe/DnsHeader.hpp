
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DnsHeader_HPP

#define	INCLUDE_DnsHeader_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DnsFrames.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNS Header Wrapper
//

class CDnsHeader : public DNS_HEADER
{
public:
	// Conversion
	void NetToHost(void);
	void HostToNet(void);

	// Operations
	void Init(void);

private:
	// Constructor
	CDnsHeader(void);
};

//////////////////////////////////////////////////////////////////////////
//
// DNS Label Wrapper
//

class CDnsLabel : public DNS_LABEL
{
public:
	// Conversion
	void NetToHost(void);
	void HostToNet(void);

	// Attributes
	BOOL IsNull(void) const;
	BOOL IsLabel(void) const;
	BOOL IsPointer(void) const;
	UINT GetFree(void) const;
	UINT GetLength(void) const;
	UINT GetSize(void) const;
	UINT GetPointer(void) const;

	// Operations
	void  Init(void);
	PCTXT GetText(void) const;
	BOOL  SetText(PCTXT pText);
	BOOL  SetText(PCTXT pText, UINT uLen);
	BOOL  SetPointer(WORD wPtr);
	void  SetNull(void);

	// Operators
	operator PCBYTE (void) const;

protected:
	// Constructors
	CDnsLabel(void);
};

//////////////////////////////////////////////////////////////////////////
//
// DNS Name Wrapper
//

class CDnsName : public DNS_NAME
{
public:
	// Conversion
	void NetToHost(void);
	void HostToNet(void);

	// Attributes
	UINT GetCount(void) const;
	UINT GetSize(void) const;
	UINT GetFree(void) const;
	BOOL HasPointers(void) const;

	// Operations
	void Init(void);
	void Empty(void);
	BOOL Append(PCTXT pText);
	UINT FindSize(PCTXT pText) const;

	// Enum
	CDnsLabel const * GetHead(UINT &uIndex) const;
	CDnsLabel const * GetNext(UINT &uIndex) const;
	CDnsLabel const * GetTail(UINT &uIndex) const;

	// Operators
	operator PCBYTE (void) const;

protected:
	// Constructors
	CDnsName(void);
};

//////////////////////////////////////////////////////////////////////////
//
// DNS Question Footer Wrapper
//

class CDnsQFooter : public DNS_QFOOT
{
public:
	// Conversion
	void NetToHost(void);
	void HostToNet(void);

protected:
	// Constructors
	CDnsQFooter(void);
};

//////////////////////////////////////////////////////////////////////////
//
// DNS Question Wrapper
//

class CDnsQuestion : public DNS_NAME
{
public:
	// Conversion
	void NetToHost(void);
	void HostToNet(void);

	// Attributes
	CDnsName    * GetName(void) const;
	CDnsQFooter * GetFoot(void) const;
	UINT          GetSize(void) const;

	// Operations
	UINT FindSize(PCTXT pText) const;
	void Init(PCTXT pText, WORD wType, WORD wClass);

protected:
	// Constructors
	CDnsQuestion(void);
};

//////////////////////////////////////////////////////////////////////////
//
// DNS Resource  Footer Wrapper
//

class CDnsRRFooter : public DNS_RRFOOT
{
public:
	// Conversion
	void NetToHost(void);
	void HostToNet(void);

protected:
	// Constructors
	CDnsRRFooter(void);
};

//////////////////////////////////////////////////////////////////////////
//
// DNS Resource Record Wrapper
//

class CDnsRecord : public DNS_NAME
{
public:
	// Conversion
	void NetToHost(void);
	void HostToNet(void);

	// Operations
	UINT           GetSize(void) const;
	WORD           GetType(void) const;
	WORD           GetClass(void) const;
	DWORD          GetTTL(void) const;
	CDnsName     * GetName(void) const;
	CDnsRRFooter * GetFoot(void) const;
	DWORD          GetAddr(void) const;

protected:
	// Constructors
	CDnsRecord(void);
};

// End of File

#endif
