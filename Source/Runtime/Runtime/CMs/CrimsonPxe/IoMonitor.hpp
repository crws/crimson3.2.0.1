
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IoMonitor_HPP

#define	INCLUDE_IoMonitor_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// I/O Monitor
//

class CIoMonitor : public IClientProcess
{
public:
	// Constructor
	CIoMonitor(CJsonConfig *pJson);

	// Destructor
	~CIoMonitor(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Digital Input
	struct CDigital
	{
		CDigital(void)
		{
			m_fInit = true;
		}

		UINT	m_uModeRise;
		UINT	m_uModeFall;
		bool	m_fInit;
		bool	m_fState;
	};

	// Analog Input
	struct CAnalog
	{
		CAnalog(void)
		{
			m_fInit = true;
		}

		UINT	m_uModeLow;
		UINT	m_uModeHigh;
		UINT	m_uTripLow;
		UINT	m_uTripHigh;
		bool	m_fInit;
		bool	m_fState;
	};

	// Data Members
	ULONG       m_uRefs;
	IDeviceIo * m_pIo;
	UINT	    m_uDoMode;
	CDigital    m_Di;
	CAnalog	    m_Ai;
	CAnalog	    m_Vi;
	CString	    m_DevName;
	CString	    m_Recip1;
	CString	    m_Recip2;

	// Implementation
	void ApplyConfig(CJsonConfig *pJson);
	void RunDigital(CDigital &Di, PCSTR pName, bool fState);
	void RunAnalog(CAnalog &Ai, PCSTR pName, UINT uLevel);
	void RunAction(UINT uAction, PCSTR pMessage);
	void SendMessage(PCSTR pRecip, PCSTR pMessage);
};

// End of File

#endif
