
#include "Intern.hpp"

#include "WebFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Helper
//

#define Entry(f, p) { f, data_##p, size_##p }

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

#include "autoport.js.dat"
#include "autosled.js.dat"
#include "bootstrap.min.js.dat"
#include "bootstrap-multiselect.js.dat"
#include "bootstrap-treeview.min.js.dat"
#include "cellinfo.js.dat"
#include "cutils.js.dat"
#include "default.js.dat"
#include "devinfo.js.dat"
#include "editor.js.dat"
#include "html5shiv.min.js.dat"
#include "ie10-viewport-bug-workaround.js.dat"
#include "jquery.min.js.dat"
#include "logarea.js.dat"
#include "logoff.js.dat"
#include "logon.js.dat"
#include "modal.js.dat"
#include "respond.min.js.dat"
#include "session.js.dat"
#include "setpass.js.dat"
#include "syscmd.js.dat"
#include "sysdebug.js.dat"
#include "syslog.js.dat"
#include "syspcap.js.dat"
#include "systool.js.dat"
#include "wifiinfo.js.dat"

//////////////////////////////////////////////////////////////////////////
//
// File Table
//

CWebFileData const CWebFiles::m_Files4[] = {

	Entry("/assets/js/autoport.js",					autoport_js),
	Entry("/assets/js/autosled.js",					autosled_js),
	Entry("/assets/js/bootstrap-multiselect.js",			bootstrap_multiselect_js),
	Entry("/assets/js/bootstrap-treeview.min.js",			bootstrap_treeview_min_js),
	Entry("/assets/js/bootstrap.min.js",				bootstrap_min_js),
	Entry("/assets/js/cellinfo.js",					cellinfo_js),
	Entry("/assets/js/cutils.js",					cutils_js),
	Entry("/assets/js/default.js",					default_js),
	Entry("/assets/js/devinfo.js",					devinfo_js),
	Entry("/assets/js/editor.js",					editor_js),
	Entry("/assets/js/html5shiv.min.js",				html5shiv_min_js),
	Entry("/assets/js/ie10-viewport-bug-workaround.js",		ie10_viewport_bug_workaround_js),
	Entry("/assets/js/jquery.min.js",				jquery_min_js),
	Entry("/assets/js/logarea.js",					logarea_js),
	Entry("/assets/js/logoff.js",					logoff_js),
	Entry("/assets/js/logon.js",					logon_js),
	Entry("/assets/js/modal.js",					modal_js),
	Entry("/assets/js/respond.min.js",				respond_min_js),
	Entry("/assets/js/session.js",					session_js),
	Entry("/assets/js/setpass.js",					setpass_js),
	Entry("/assets/js/syscmd.js",					syscmd_js),
	Entry("/assets/js/sysdebug.js",					sysdebug_js),
	Entry("/assets/js/syslog.js",					syslog_js),
	Entry("/assets/js/syspcap.js",					syspcap_js),
	Entry("/assets/js/systool.js",					systool_js),
	Entry("/assets/js/wifiinfo.js",					wifiinfo_js),

};

UINT const CWebFiles::m_uCount4 = elements(m_Files4);

// End of File
