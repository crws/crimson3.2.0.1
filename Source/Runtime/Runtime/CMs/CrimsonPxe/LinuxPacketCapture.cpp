
#include "Intern.hpp"

#include "LinuxPacketCapture.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Linux Packet Capture
//

// Constructor

CLinuxPacketCapture::CLinuxPacketCapture(PCTXT pName, UINT uInst, UINT uMode)
{
	m_Name   = pName;

	m_uInst  = uInst;

	m_uMode  = uMode;

	m_File   = "/./tmp/crimson/pcap/" + m_Name + ".pcap";

	m_pid    = 0;

	m_pLinux = NULL;

	piob->RegisterSingleton("net.pcap", m_uInst, (IPacketCapture *) this);

	AfxGetObject("os.linux", 0, ILinuxSupport, m_pLinux);

	StdSetRef();

	unlink(m_File);
}

// Destructor

CLinuxPacketCapture::~CLinuxPacketCapture(void)
{
	if( m_pid ) {

		m_pLinux->KillProcess(m_pid);
	}

	unlink(m_File);

	AfxRelease(m_pLinux);

	piob->RevokeSingleton("net.pcap", m_uInst);
}

// IUnknown

HRESULT CLinuxPacketCapture::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPacketCapture);

	StdQueryInterface(IPacketCapture);

	return E_NOINTERFACE;
}

ULONG CLinuxPacketCapture::AddRef(void)
{
	StdAddRef();
}

ULONG CLinuxPacketCapture::Release(void)
{
	StdRelease();
}

// IPacketCapture

BOOL CLinuxPacketCapture::IsCaptureRunning(void)
{
	return m_pid ? TRUE : FALSE;
}

PCSTR CLinuxPacketCapture::GetCaptureFilter(void)
{
	return m_Filter;
}

UINT CLinuxPacketCapture::GetCaptureSize(void)
{
	CAutoFile File(m_File, "r");

	if( File ) {

		return File.GetSize();
	}

	return 0;
}

BOOL CLinuxPacketCapture::CopyCapture(PBYTE pData, UINT uSize)
{
	CAutoFile File(m_File, "r");

	if( File ) {

		File.Read(pData, uSize);

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxPacketCapture::StartCapture(PCSTR pFilter)
{
	m_Filter = pFilter;

	mkdir("/./tmp/crimson/pcap", 0755);

	unlink(m_File);

	CString Args;

	CString Filt(m_Filter);

	Filt.Replace(';', ',');

	Args.AppendPrintf("-i %s ", PCTXT(m_Name));

	Args.AppendPrintf("-m %u ", m_uMode);

	Args.AppendPrintf("-f %s", PCTXT(Filt));

	int pid  = m_pLinux->InitProcess("/opt/crimson/bin/Capture", Args);

	if( pid > 0 ) {

		m_pid = pid;

		return TRUE;
	}

	return FALSE;
}

void CLinuxPacketCapture::StopCapture(void)
{
	if( m_pid ) {

		m_pLinux->TermProcess(m_pid);

		m_pid = 0;
	}
}

void CLinuxPacketCapture::KillCapture(void)
{
	StopCapture();

	unlink(m_File);
}

// End of File
