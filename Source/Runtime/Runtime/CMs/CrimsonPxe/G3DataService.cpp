
#include "Intern.hpp"

#include "G3DataService.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Access Service
//

// Instantiator

global ILinkService * Create_DataService(ICrimsonPxe *pPxe)
{
	return New CG3DataService(pPxe);
}

// Constructor

CG3DataService::CG3DataService(ICrimsonPxe *pPxe)
{
	m_pPxe = pPxe;

	StdSetRef();
}

// Destructor

CG3DataService::~CG3DataService(void)
{
}

// IUnknown

HRESULT CG3DataService::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ILinkService);

	StdQueryInterface(ILinkService);

	return E_NOINTERFACE;
}

ULONG CG3DataService::AddRef(void)
{
	StdAddRef();
}

ULONG CG3DataService::Release(void)
{
	StdRelease();
}

// ILinkService

void CG3DataService::Timeout(void)
{
}

UINT CG3DataService::Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans)
{
	if( Req.GetService() == servData ) {

		if( Req.GetOpcode() == dataListRead ) {

			ICrimsonApp *pApp;

			if( m_pPxe->LockApp(pApp) ) {

				UINT uPtr   = 0;

				UINT uCount = Req.ReadWord(uPtr);

				Rep.StartFrame(servData, dataListRead);

				Rep.AddWord(WORD(uCount));

				for( UINT n = 0; n < uCount; n++ ) {

					DWORD Ref  = Req.ReadLong(uPtr);

					DWORD Data = 0;

					if( pApp->GetData(Data, Ref) ) {

						Rep.AddByte(1);

						Rep.AddLong(Data);
					}
					else
						Rep.AddByte(0);
				}

				m_pPxe->FreeApp();
			}
			else {
				UINT uPtr   = 0;

				UINT uCount = Req.ReadWord(uPtr);

				Rep.StartFrame(servData, dataListRead);

				Rep.AddWord(WORD(uCount));

				for( UINT n = 0; n < uCount; n++ ) {

					Rep.AddByte(0);
				}
			}

			Sleep(50);

			return procOkay;
		}
	}

	return procError;
}

void CG3DataService::EndLink(CG3LinkFrame &Req)
{
}

// End of File
