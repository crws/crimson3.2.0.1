
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3SerialClient_HPP

#define	INCLUDE_G3SerialClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "G3SerialBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

////////////////////////////////////////////////////////////////////////
//
// Serial Client Transport
//

class CG3SerialClient : public CG3SerialBase
{
public:
	// Constructor
	CG3SerialClient(UINT uLevel, UINT uPort, ILinkService *pService);

	// Destructor
	~CG3SerialClient(void);

	// Operations
	BOOL Open(CJsonConfig *pJson);

protected:
	// Configuration
	struct CConfig
	{
		// Constructor
		CConfig(void);

		// Operations
		BOOL Parse(CJsonConfig *pJson);
	};

	// Data
	CConfig        m_Config;
	UINT	       m_uPort;
	IPortGrabber * m_pGrabber;

	// Frame Processing
	void EndLink(CG3LinkFrame &Req);

	// Port
	BOOL OpenPort(void);
	BOOL ClosePort(void);
	BOOL HasPortRequest(void);
	void ServicePortRequest(void);
};

// End of File

#endif
