
#include "Intern.hpp"

#include "WebFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Helper
//

#define Entry(f, p) { f, data_##p, size_##p }

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

#include "font-awesome.min.css.dat"
#include "fontawesome-webfont.eot.dat"
#include "fontawesome-webfont.svg.dat"
#include "fontawesome-webfont.ttf.dat"
#include "fontawesome-webfont.woff.dat"
#include "fontawesome-webfont.woff2.dat"
#include "glyphicons-halflings-regular.eot.dat"
#include "glyphicons-halflings-regular.svg.dat"
#include "glyphicons-halflings-regular.ttf.dat"
#include "glyphicons-halflings-regular.woff.dat"
#include "glyphicons-halflings-regular.woff2.dat"

//////////////////////////////////////////////////////////////////////////
//
// File Table
//

CWebFileData const CWebFiles::m_Files2[] = {

	Entry("/assets/fonts/fontawesome-webfont.eot",			fontawesome_webfont_eot),
	Entry("/assets/fonts/fontawesome-webfont.svg",			fontawesome_webfont_svg),
	Entry("/assets/fonts/fontawesome-webfont.ttf",			fontawesome_webfont_ttf),
	Entry("/assets/fonts/fontawesome-webfont.woff",			fontawesome_webfont_woff),
	Entry("/assets/fonts/fontawesome-webfont.woff2",		fontawesome_webfont_woff2),
	Entry("/assets/fonts/glyphicons-halflings-regular.eot",		glyphicons_halflings_regular_eot),
	Entry("/assets/fonts/glyphicons-halflings-regular.svg",		glyphicons_halflings_regular_svg),
	Entry("/assets/fonts/glyphicons-halflings-regular.ttf",		glyphicons_halflings_regular_ttf),
	Entry("/assets/fonts/glyphicons-halflings-regular.woff",	glyphicons_halflings_regular_woff),
	Entry("/assets/fonts/glyphicons-halflings-regular.woff2",	glyphicons_halflings_regular_woff2),
};

UINT const CWebFiles::m_uCount2 = elements(m_Files2);

// End of File
