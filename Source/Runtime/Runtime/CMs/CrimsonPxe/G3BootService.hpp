
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3BootService_HPP

#define	INCLUDE_G3BootService_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Boot Loader Service
//

class CG3BootService : public ILinkService
{
public:
	// Constructor
	CG3BootService(ICrimsonPxe *pPxe);

	// Destructor
	~CG3BootService(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ILinkService
	void Timeout(void);
	UINT Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans);
	void EndLink(CG3LinkFrame &Req);

protected:
	// Data Members
	ULONG	           m_uRefs;
	ICrimsonPxe      * m_pPxe;
	IPlatform        * m_pPlatform;
	IFirmwareProgram * m_pFirmPend;
	IFirmwareProps   * m_pFirmProps;
	BOOL		   m_fPend;

	// Implementation
	UINT ReadModel(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT ReadOem(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT CheckVersion(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT ForceReset(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans);
	UINT CheckHardware(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT StartProgram(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT ReadRevision(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT WriteMAC(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT WriteSerialNumber(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT CheckLevel(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT ClearProgram(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT WriteProgram(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT WriteVersion(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT AutoDetect(CG3LinkFrame &Req, CG3LinkFrame &Rep);
};

// End of File

#endif
