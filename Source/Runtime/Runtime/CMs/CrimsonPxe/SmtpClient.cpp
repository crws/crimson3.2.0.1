
#include "intern.hpp"

#include "SmtpClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

#include "SmtpDefs.hpp"

#include "SmtpSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SMTP Client Service
//

CSmtpClient::CSmtpClient(CJsonConfig *pJson)
{
	m_pSmtpConfig = New CSmtpConfig;

	m_pLinkConfig = New CStdClientConfig;

	ApplyConfig(pJson);

	StdSetRef();
}

// Destructor

CSmtpClient::~CSmtpClient(void)
{
	delete m_pSmtpConfig;

	delete m_pLinkConfig;
}

// IUnknown

HRESULT CSmtpClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IClientProcess);

	return CMsgTransport::QueryInterface(riid, ppObject);
}

ULONG CSmtpClient::AddRef(void)
{
	return CMsgTransport::AddRef();
}

ULONG CSmtpClient::Release(void)
{
	return CMsgTransport::Release();
}

// IClientProcess

BOOL CSmtpClient::TaskInit(UINT uTask)
{
	SetThreadName("SmtpClient");

	if( m_pSmtpConfig->m_uMode ) {

		m_fEnable = TRUE;

		piob->RegisterSingleton("msg.smtp", 0, (IMsgTransport *) this);

		AddRef();

		return TRUE;
	}

	Release();

	return FALSE;
}

INT CSmtpClient::TaskExec(UINT uTask)
{
	for( ;;) {

		m_pWait->Wait(FOREVER);

		CAutoPointer<CSmtpSession> pSession(New CSmtpSession(m_pSmtpConfig));

		pSession->SetConfig(m_pLinkConfig);

		for( ;;) {

			CAutoLock    Lock(m_pLock);

			INDEX        Index    = m_Queue.GetHead();

			CMsgPayload *pMessage = m_Queue[Index];

			BOOL         fMore    = (m_Queue.GetCount() > 1);

			BOOL	     fOkay    = TRUE;

			m_Queue.Remove(Index);

			Lock.Free();

			pMessage->m_OrigName = m_pSmtpConfig->m_OrigName;

			pMessage->m_OrigAddr = m_pSmtpConfig->m_OrigEmail;

			if( !pSession->SendMail(pMessage, fMore) ) {

				if( ++pMessage->m_Attempts < 4 ) {

					Lock.Lock();

					m_Queue.Append(pMessage);

					if( !fMore ) {

						m_pWait->Signal(1);

						break;
					}

					continue;
				}

				fOkay = FALSE;
			}

			SendNotify(pMessage, fOkay);

			delete pMessage;

			if( fMore ) {

				m_pWait->Wait(0);

				continue;
			}

			break;
		}
	}

	return 0;
}

void CSmtpClient::TaskStop(UINT uTask)
{
}

void CSmtpClient::TaskTerm(UINT uTask)
{
	while( !m_Queue.IsEmpty() ) {

		CMsgPayload *pMessage = m_Queue[m_Queue.GetHead()];

		SendNotify(pMessage, FALSE);

		delete pMessage;
	}

	piob->RevokeSingleton("msg.smtp", 0);

	Release();
}

// IMsgTransport

BOOL CSmtpClient::CanAcceptAddress(CString const &Addr)
{
	if( Addr.Find(':') == NOTHING ) {

		if( Addr.Count('@') == 1 ) {

			return TRUE;
		}
	}

	return FALSE;
}

// Implementation

void CSmtpClient::ApplyConfig(CJsonConfig *pJson)
{
	if( pJson ) {

		AfxGetAutoObject(pPxe, "pxe", 0, ICrimsonPxe);

		CString UnitName;
		
		pPxe->GetUnitName(UnitName);

		m_pSmtpConfig->m_uMode      = pJson->GetValueAsUInt("mode", 0, 0, 1);
		m_pSmtpConfig->m_Server     = pJson->GetValue("server", "");
		m_pSmtpConfig->m_uPort      = pJson->GetValueAsUInt("port", 24, 1, 65535);
		m_pSmtpConfig->m_uTls       = pJson->GetValueAsUInt("cmode", 0, 0, 2);
		m_pSmtpConfig->m_uAuth      = pJson->GetValueAsUInt("logon", 0, 0, 2);
		m_pSmtpConfig->m_User       = pJson->GetValue("user", "");
		m_pSmtpConfig->m_Pass       = pJson->GetValue("pass", "");
		m_pSmtpConfig->m_OrigDomain = pJson->GetValue("origDomain", "");
		m_pSmtpConfig->m_OrigName   = pJson->GetValue("origName", "");
		m_pSmtpConfig->m_OrigEmail  = pJson->GetValue("origEmail", "noreply@example.com");
		m_pLinkConfig->m_uCa        = pJson->GetValueAsUInt("casrc", 0, 0, 65535);
		m_pLinkConfig->m_uCheck     = pJson->GetValueAsUInt("cacheck", 0, 0, 2);
		
		pJson->GetValueAsBlob("cacert", m_pLinkConfig->m_CaCert);
	}
}

// End of File
