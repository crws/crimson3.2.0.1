
#include "Intern.hpp"

#include "PxeObject.hpp"

#include <io.h>

#include <fcntl.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Headers
//

#include "JsonConfig.hpp"

#include "G3Slave.hpp"

#include "PxeWebServer.hpp"

#include "MsgBroker.hpp"

#include "Splash.hpp"

#include "ImageLoader.hpp"

#include "UserManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IPxeModel * Create_RlosModelData(void);

extern IPxeModel * Create_LinuxModelData(void);

//////////////////////////////////////////////////////////////////////////
//
// Debug Commands
//

extern void Register_StdDebugCmds(void);

//////////////////////////////////////////////////////////////////////////
//
// PXE Application Object
//

// Static Data

UINT	   CPxeObject::m_DefCertStep = 0;

CByteArray CPxeObject::m_DefRootData;

CByteArray CPxeObject::m_DefRootPriv;

CString    CPxeObject::m_DefRootPass;

CByteArray CPxeObject::m_DefCertData;

CByteArray CPxeObject::m_DefCertPriv;

CString    CPxeObject::m_DefCertPass;

// Instantiator

clink IUnknown * Create_PxeObject(PCSTR pName)
{
	return (IClientProcess *) New CPxeObject;
}

// Constructor

CPxeObject::CPxeObject(void)
{
	Register_StdDebugCmds();

	m_fDebug     = FALSE;
	m_InitTime   = getmonosecs();
	m_uState     = stateNull;
	m_uSaved     = 0;
	m_uTimer     = 0;
	m_uEvent     = eventNull;
	m_uDefTimer  = 0;
	m_uDefEvent  = 0;
	m_pAppLock   = Create_Mutex();
	m_pEvent     = Create_AutoEvent();
	m_pMakeCert  = Create_AutoEvent();
	m_hMakeCert  = NULL;
	m_uProv      = 0;
	m_pSplash    = New CSplash;
	m_pReqRouter = NULL;
	m_pModel     = NULL;
	m_pSchema    = NULL;
	m_pConfig    = NULL;
	m_pNetApp    = NULL;
	m_pHwApp     = NULL;
	m_pHcon      = NULL;
	m_pScon      = NULL;
	m_pPconLock  = Create_Mutex();
	m_fPconValid = FALSE;
	m_crcNet     = 0;
	m_crcIdent   = 0;
	m_crcLink    = 0;
	m_pWeb       = NULL;
	m_pMsgBroker = New CMsgBroker;
	m_pApp       = NULL;
	m_pPlatform  = NULL;
	m_pNetUtils  = NULL;
	m_pFileUtils = NULL;
	m_pDisp      = NULL;
	m_pBeep      = NULL;
	m_pLeds      = NULL;
	m_uLedType   = 0;
	m_uLedMode   = 0;
	m_uBright    = 0;
	m_pTimer     = CreateTimer();
	m_uTickCount = 0;
	m_uIdentify  = 0;
	m_uAlarm     = 0;
	m_fSiren     = FALSE;
	m_uSiren     = NOTHING;
	m_uTimeout1  = 0;
	m_uTimeout2  = 0;
	m_uLoadTime  = 0;
	m_uHalo      = NOTHING;
	m_uExitCode  = 0;
	m_hUpdate    = NULL;
	m_fDebug     = FALSE;
	m_wTcpPort   = 0;

	CheckDebug();

	m_pSplash->SetEventSink(this);

	AfxGetObject("platform", 0, IPlatform, m_pPlatform);

	AfxGetObject("ip", 0, INetUtilities, m_pNetUtils);

	AfxGetObject("aeon.filesupport", 0, IFileUtilities, m_pFileUtils);

	AfxGetObject("display", 0, IDisplay, m_pDisp);

	AfxGetObject("beeper", 0, IBeeper, m_pBeep);

	AfxGetObject("leds", 0, ILeds, m_pLeds);

	m_DefName = m_pPlatform->GetDefName();

	CreateModel();

	StdSetRef();

	piob->RegisterSingleton("pxe", 0, (ICrimsonPxe *) this);

	DiagRegister();
}

// Destructor

CPxeObject::~CPxeObject(void)
{
	DiagRevoke();

	delete m_pMsgBroker;

	piob->RevokeSingleton("pxe", 0);

	AfxRelease(m_pLeds);
	AfxRelease(m_pBeep);
	AfxRelease(m_pDisp);
	AfxRelease(m_pFileUtils);
	AfxRelease(m_pNetUtils);
	AfxRelease(m_pPlatform);
	AfxRelease(m_pTimer);
	AfxRelease(m_pMakeCert);
	AfxRelease(m_pEvent);
	AfxRelease(m_pAppLock);
	AfxRelease(m_pModel);
	AfxRelease(m_pPconLock);

	delete m_pSplash;
}

// IUnknown

HRESULT CPxeObject::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ICrimsonPxe);

	StdQueryInterface(ICrimsonPxe);
	StdQueryInterface(IConfigUpdate);
	StdQueryInterface(IClientProcess);
	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
}

ULONG CPxeObject::AddRef(void)
{
	StdAddRef();
}

ULONG CPxeObject::Release(void)
{
	StdRelease();
}

// ICrimsonPxe

BOOL CPxeObject::LockApp(ICrimsonApp * &pApp)
{
	if( m_pAppLock->Wait(100) ) {

		if( m_uState == stateRunning ) {

			pApp = m_pApp;

			return TRUE;
		}

		m_pAppLock->Free();
	}

	return FALSE;
}

void CPxeObject::FreeApp(void)
{
	m_pAppLock->Free();
}

void CPxeObject::GetInitTime(time_t &time)
{
	time = m_InitTime;
}

void CPxeObject::SetSysWebLink(CString const &Link)
{
	m_SysLink = Link;
}

BOOL CPxeObject::GetSysWebLink(CString &Link)
{
	Link = m_SysLink;

	return !m_SysLink.IsEmpty();
}

void CPxeObject::SetAppWebLink(CString const &Link)
{
	m_pWeb->EnableRedirect((m_AppLink = Link).IsEmpty());
}

BOOL CPxeObject::GetAppWebLink(CString &Link)
{
	Link = m_AppLink;

	return !m_AppLink.IsEmpty();
}

void CPxeObject::SetAlarm(UINT uAlarm)
{
	m_uAlarm = uAlarm;
}

UINT CPxeObject::GetAlarm(void)
{
	return m_uAlarm;
}

void CPxeObject::SetSiren(BOOL fSiren)
{
	if( m_fSiren != fSiren ) {

		if( (m_fSiren = fSiren) ) {

			if( m_pDisp ) {

				m_uBright = m_pDisp->GetBacklight();
			}
		}
	}
}

BOOL CPxeObject::GetSiren(void)
{
	return m_fSiren;
}

UINT CPxeObject::GetLedMode(void)
{
	return m_uLedMode;
}

void CPxeObject::SetLedMode(UINT uMode)
{
	m_uLedMode = uMode;
}

BOOL CPxeObject::SystemStart(void)
{
	while( m_uState == stateInitial ) {

		Sleep(100);
	}

	switch( m_uState ) {

		case stateRunning:

			return TRUE;

		case stateMultiple:
		case stateDelay:
		case statePaused:
		case stateStopped:
		case stateInvalid:

			SetStateEvent(eventStart, TRUE);

		case stateStarting:

			// TODO -- Timeout here? !!!

			for( SetTimer(10000); GetTimer(); Sleep(50) ) {

				switch( m_uState ) {

					case stateRunning:
					case stateWaiting:

						return TRUE;

					case stateInvalid:

						return FALSE;
				}
			}

			break;
	}

	return FALSE;
}

BOOL CPxeObject::SystemStop(void)
{
	while( m_uState == stateInitial ) {

		Sleep(100);
	}

	switch( m_uState ) {

		case stateStopped:

			return TRUE;

		case stateMultiple:
		case stateDelay:
		case statePaused:
		case stateStarting:
		case stateRunning:
		case stateInvalid:

			SetStateEvent(eventStop, FALSE);

			// TODO -- Timeout here? !!!

			for( SetTimer(10000); GetTimer(); Sleep(50) ) {

				switch( m_uState ) {

					case stateStopped:

						return TRUE;
				}
			}

			break;
	}

	return FALSE;
}

void CPxeObject::SystemUpdate(UINT hItem, PBYTE pData, UINT uSize)
{
	m_Update.Empty();

	m_Update.Append(pData, uSize);

	m_hUpdate = hItem;

	SetStateEvent(eventStop, FALSE);
}

void CPxeObject::RestartSystem(UINT uTimeout, UINT uExitCode)
{
	m_uExitCode = MAKELONG(uExitCode, uTimeout);

	SetStateEvent(eventExit, FALSE);
}

BOOL CPxeObject::ImageSave(PCTXT pName)
{
	return TRUE;
}

void CPxeObject::KickTimeout(UINT uIndex)
{
	switch( uIndex ) {

		case 1:
			m_uTimeout1 = ToTimer(1000) * 90;
			break;

		case 2:
			m_uTimeout2 = ToTimer(1000) * 90;
			break;
	}
}

UINT CPxeObject::GetDefCertStep(void)
{
	return m_DefCertStep;
}

BOOL CPxeObject::GetDefCertData(CByteArray *pData, CString &Pass)
{
	pData[0] = CPxeObject::m_DefCertData;

	pData[1] = CPxeObject::m_DefCertPriv;

	Pass = CPxeObject::m_DefCertPass;

	return TRUE;
}

BOOL CPxeObject::GetHostName(CString &Name)
{
	CStringArray List;

	m_pNetApp->GetmDnsNames(List);

	Name = List[0] + ".local";

	return TRUE;
}

BOOL CPxeObject::GetUnitName(CString &Name)
{
	m_pNetApp->GetUnitName(Name);

	return TRUE;
}

UINT CPxeObject::GetStatus(void)
{
	return m_fApply ? stateChange : m_uState;
}

BOOL CPxeObject::GetPersonality(CString const &Name, CString &Data)
{
	CAutoGuard Guard;

	CAutoLock  Lock(m_pPconLock);

	if( LoadPersonality() ) {

		Data = m_PKeys[Name];

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeObject::SetPersonality(CString const &Name, CString const &Data)
{
	CAutoGuard Guard;

	CAutoLock  Lock(m_pPconLock);

	if( LoadPersonality() ) {

		m_PKeys.Remove(Name);

		m_PKeys.Insert(Name, Data);

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeObject::CommitPersonality(BOOL fRestart)
{
	CAutoGuard Guard;

	CAutoLock  Lock(m_pPconLock);

	CString    Text;

	if( TRUE ) {

		CAutoPointer<CJsonData> pJson(new CJsonData);

		CJsonData *pKeys = NULL;

		pJson->AddValue("atype", "p");

		pJson->AddList("set.keys", pKeys);

		for( INDEX i = m_PKeys.GetHead(); !m_PKeys.Failed(i); m_PKeys.GetNext(i) ) {

			CJsonData *pKey = NULL;

			pKeys->AddChild(FALSE, pKey);

			pKey->AddValue("name", m_PKeys.GetName(i), jsonString);

			pKey->AddValue("value", m_PKeys.GetData(i), jsonString);
		}

		Text = pJson->GetAsText(FALSE);
	}

	if( m_pConfig->SetConfig('p', Text, true) ) {

		if( fRestart ) {

			SetDeferEvent(eventHChange);
		}
	}

	return TRUE;
}

void CPxeObject::IdentifyUnit(void)
{
	if( !m_uIdentify ) {

		m_uIdentify = 120;
	}
}

BOOL CPxeObject::GetUserInfo(CString const &User, CAuthInfo const &Auth, CUserInfo &Info)
{
	return m_pUsers->GetUserInfo(User, Auth, Info);
}

BOOL CPxeObject::SetUserPass(CString const &User, CString const &From, CString const &To)
{
	return m_pUsers->SetUserPass(User, From, To);
}

WORD CPxeObject::GetG3LinkPort(void)
{
	return m_wTcpPort;
}

// IConfigUpdate

void CPxeObject::OnConfigUpdate(char cTag)
{
	if( m_uState > stateNull ) {

		if( cTag == 'p' || cTag == 's' ) {

			SetDeferEvent(eventSChange);
		}

		if( cTag == 'h' ) {

			SetDeferEvent(eventHChange);
		}

		ImageDisable(cTag);
	}
}

// IClientProcess

BOOL CPxeObject::TaskInit(UINT uTask)
{
	if( uTask == 0 ) {

		SetThreadName("PxeMain");

		SetThreadFlags(1, 1);

		FindLedType();

		m_hMakeCert = CreateClientThread(this, 1, 7990, NULL);

		m_pSplash->ShowStatus("Initializing...");

		m_pTimer->SetHook(this, 0);

		m_pTimer->SetPeriod(TIMER_RATE);

		m_pTimer->Enable(true);

		CreatePxeObjects();

		CreateAppObjects();

		m_pUsers = New CUserManager;
	}

	if( uTask == 1 ) {

		SetThreadName("PxeCert");
	}

	return TRUE;
}

INT CPxeObject::TaskExec(UINT uTask)
{
	if( uTask == 0 ) {

		UINT uDrive = GetActiveDrives();

		if( CheckImageFiles(TRUE) ) {

			return 55;
		}

		if( LoadHardwareConfig() && LoadSystemConfig() ) {

			while( m_uState != stateExit ) {

				try {
					if( getmonosecs() < m_InitTime + 30 ) {

						UINT uCheck = GetActiveDrives();

						if( uCheck & ~uDrive ) {

							if( CheckImageFiles(FALSE) ) {

								return 55;
							}

							uDrive = uCheck;
						}
					}

					if( m_uEvent ) {

						for( UINT uEvent = 1; uEvent; uEvent <<= 1 ) {

							if( m_uEvent & uEvent ) {

								if( OnStateEvent(m_uState, uEvent) ) {

									if( !(m_uEvent &= ~uEvent) ) {

										break;
									}
								}
							}
						}

						continue;
					}

					if( m_uDefTimer && GetTickCount() >= m_uDefTimer ) {

						m_uDefTimer = 0;

						SetStateEvent(m_uDefEvent, FALSE);

						m_uDefEvent = 0;
					}

					if( m_uTimer && GetTickCount() >= m_uTimer ) {

						m_uTimer = 0;

						OnStateTimer(m_uState);
					}

					switch( m_uState ) {

						case stateInitial:
						case stateMultiple:
						case stateDelay:
						case statePaused:
						case stateStarting:
						case stateRunning:
						case stateStopped:
						case stateInvalid:

							if( HasIpListChanged() ) {

								if( m_uState != stateRunning ) {

									ShowIpList();
								}

								m_pMakeCert->Set();

								IpListChangeDone();
							}
							break;
					}

					m_pEvent->Wait(500);
				}

				catch( CExecCancel const & )
				{
					SetStateEvent(eventExit, FALSE);
				}
			}
		}
	}

	if( uTask == 1 ) {

		for( ;;) {

			m_pMakeCert->Wait(FOREVER);

			if( m_uState != stateChange ) {

				MakeCertificate();
			}
		}
	}

	return m_uExitCode;
}

void CPxeObject::TaskStop(UINT uTask)
{
	if( uTask == 0 ) {

		DestroyThread(m_hMakeCert);

		if( m_uState > stateNull ) {

			SwitchState(stateExit);
		}
	}
}

void CPxeObject::TaskTerm(UINT uTask)
{
	if( uTask == 0 ) {

		m_pTimer->Enable(false);

		SetHalo(haloExit);

		delete m_pUsers;

		AfxRelease(m_pNetApp);
		AfxRelease(m_pHwApp);
		AfxRelease(m_pConfig);
		AfxRelease(m_pSchema);
		AfxRelease(m_pApp);
		AfxRelease(m_pReqRouter);

		piob->RevokeGroup("c3.");
	}
}

// State Machine

void CPxeObject::SwitchState(UINT uState)
{
	if( m_uState != uState ) {

		CAutoLock Lock(m_pAppLock, m_uState == stateRunning);

		OnLeaveState(m_uState);

		m_uState = uState;

		m_uTimer = 0;

		OnEnterState(m_uState);
	}
}

void CPxeObject::SetStateTimer(UINT uDelta)
{
	m_uTimer = GetTickCount() + ToTicks(uDelta);
}

void CPxeObject::SetStateEvent(UINT uEvent, BOOL fWait)
{
	m_uEvent = m_uEvent | uEvent;

	m_pEvent->Set();

	if( fWait ) {

		while( m_uEvent & uEvent ) {

			Sleep(50);
		}
	}
}

void CPxeObject::SetDeferEvent(UINT uEvent)
{
	m_uDefEvent = m_uDefEvent | uEvent;

	m_uDefTimer = GetTickCount() + ToTicks(1000);
}

void CPxeObject::OnEnterState(UINT uState)
{
	switch( uState ) {

		case stateInitial:

			m_pSplash->EnableUI(1);

			m_pSplash->ShowStatus("Initializing...");

			ApplyHardwareConfig(m_pHcon);

			ApplySystemConfig(m_pScon, TRUE);

			ShowIpList();

			MakeCertificate();

			IpListChangeDone();

			m_pWeb = New CPxeWebServer(this);

			m_hWeb = CreateClientThread(m_pWeb, 0, 10000, NULL);

			if( m_LoadCount >= 4 ) {

				SwitchState(stateMultiple);
			}
			else {
				if( m_pDbase->IsValid() ) {

					SwitchState(stateDelay);
				}
				else {
					m_Error = "Invalid Database";

					SwitchState(stateInvalid);
				}
			}
			break;

		case stateMultiple:

			m_pSplash->EnableUI(1);

			m_pSplash->ShowStatus("Multiple Load Failures");

			SetStateTimer(5000);

			break;

		case stateDelay:

			m_pSplash->EnableUI(3);

			m_pSplash->ShowStatus("Initializing...");

			SetStateTimer(2000);

			break;

		case statePaused:

			m_pSplash->EnableUI(3);

			m_pSplash->ShowStatus("Startup Paused...");

			SetStateTimer(10000);

			break;

		case stateStarting:

			m_pSplash->EnableUI(1);

			m_pSplash->ShowStatus("Loading Database...");

			SetThreadFlags(1, 0);

			m_LoadCount += 1;

			m_uLoadTime  = LOAD_TIMEOUT;

			m_Error      = "Failed to Load Database";

			if( m_pDbase->IsValid() ) {

				if( ApplyDeviceConfig() ) {

					m_pDbase->SetRunning(FALSE);

					SwitchState(stateWaiting);

					break;
				}
				else {
					m_pSplash->EnableUI(0);

					if( m_pApp->LoadConfig(m_Error) ) {

						m_pApp->InitApp();

						SwitchState(stateRunning);

						break;
					}

					m_pDbase->SetRunning(FALSE);
				}
			}

			SwitchState(stateInvalid);

			break;

		case stateWaiting:

			m_pSplash->EnableUI(1);

			m_pSplash->ShowStatus("Updating Device Config...");

			break;

		case stateChange:

			m_pSplash->EnableUI(1);

			m_pSplash->ShowStatus("Updating Hardware Config...");

			ApplySystemConfig(NULL, FALSE);

			LoadHardwareConfig();

			if( !ApplyHardwareConfig(m_pHcon) ) {

				SwitchState(stateExit);
			}
			else {
				LoadSystemConfig();

				ApplySystemConfig(m_pScon, FALSE);

				MakeCertificate();

				SwitchState(m_uSaved);
			}

			break;

		case stateStopped:

			if( m_hUpdate ) {

				CItemInfo Info;

				Info.m_uItem  = m_hUpdate;
				Info.m_uClass = 0xFFFF;
				Info.m_uSize  = m_Update.size();
				Info.m_uComp  = m_Update.size();

				m_pDbase->WriteItem(Info, m_Update.data());

				m_hUpdate = NULL;

				SetStateEvent(eventStart, FALSE);
			}

			m_pSplash->EnableUI(1);

			m_pSplash->ShowStatus("Updating Database...");

			break;

		case stateInvalid:

			m_pSplash->EnableUI(1);

			m_pSplash->ShowStatus(m_Error);

			break;

		case stateExit:

			m_pSplash->EnableUI(1);

			DestroyThread(m_hWeb);

			ApplySystemConfig(NULL, TRUE);

			ApplyHardwareConfig(NULL);

			break;
	}
}

BOOL CPxeObject::OnStateEvent(UINT uState, UINT uEvent)
{
	if( uEvent == eventStart ) {

		switch( m_uState ) {

			case stateMultiple:
			case stateDelay:
			case statePaused:
			case stateStopped:
			case stateInvalid:

				SwitchState(stateStarting);

				return TRUE;
		}

		return FALSE;
	}

	if( uEvent == eventStop ) {

		switch( m_uState ) {

			case stateMultiple:
			case stateDelay:
			case statePaused:
			case stateInvalid:

				SwitchState(stateStopped);

				return TRUE;

			case stateRunning:

				SwitchState(stateStopped);

				return TRUE;
		}

		return FALSE;
	}

	if( uEvent == eventExit ) {

		if( LOWORD(m_uExitCode) == 44 ) {

			Sleep(500);

			m_pPlatform->ResetSystem();
		}

		SwitchState(stateExit);

		return TRUE;
	}

	if( uEvent == eventHChange ) {

		switch( m_uState ) {

			case stateDelay:
			case statePaused:

				m_uSaved = m_uState;

				SwitchState(stateChange);

				return TRUE;

			case stateMultiple:
			case stateStopped:
			case stateInvalid:

				m_uSaved = stateStopped;

				SwitchState(stateChange);

				return TRUE;

			case stateRunning:
			case stateWaiting:

				m_uSaved = stateStarting;

				SwitchState(stateChange);

				return TRUE;
		}
	}

	if( uEvent == eventSChange ) {

		switch( m_uState ) {

			case stateInitial:
			case stateMultiple:
			case stateDelay:
			case statePaused:
			case stateRunning:
			case stateStopped:
			case stateInvalid:

				LoadSystemConfig();

				ApplySystemConfig(m_pScon, FALSE);

				MakeCertificate();

				return TRUE;

			case stateWaiting:

				LoadSystemConfig();

				ApplySystemConfig(m_pScon, FALSE);

				MakeCertificate();

				SwitchState(stateStarting);

				return TRUE;
		}
	}

	switch( uState ) {

		case stateNull:

			if( uEvent & eventNull ) {

				SwitchState(stateInitial);
			}
			break;

		case stateDelay:

			if( uEvent & eventPause ) {

				SwitchState(statePaused);
			}
			break;

		case statePaused:

			if( uEvent & eventPlay ) {

				SwitchState(stateDelay);
			}
			break;
	}

	return TRUE;
}

void CPxeObject::OnStateTimer(UINT uState)
{
	switch( uState ) {

		case stateMultiple:

			m_pDbase->SetValid(FALSE);

			ImageDisable('*');

			m_LoadCount.Reset();

			m_Error = "Invalid Database";

			SwitchState(stateInvalid);

			break;

		case stateDelay:

			SwitchState(stateStarting);

			break;

		case statePaused:

			m_pSplash->ClearPause();

			SwitchState(stateDelay);

			break;
	}
}

void CPxeObject::OnLeaveState(UINT uState)
{
	switch( uState ) {

		case stateStarting:

			m_LoadCount = 0;

			m_LoadCount.SetFlag(FALSE);

			m_uLoadTime = 0;

			SetThreadFlags(1, 1);

			break;

		case stateRunning:

			m_pApp->HaltApp();

			SetAppWebLink("");

			m_pSplash->EnableUI(1);

			m_pSplash->ShowStatus("Stopping System...");

			m_uTimeout1 = 0;

			m_uTimeout2 = 0;

			SetAlarm(0);

			SetSiren(FALSE);

			m_pApp->TermApp();

			FreePersonality();

			break;
	}
}

// IEventSink

void CPxeObject::OnEvent(UINT uLine, UINT uParam)
{
	if( uLine == 0 ) {

		if( uParam == 1 ) {

			SetStateEvent(eventPause, FALSE);
		}

		if( uParam == 2 ) {

			SetStateEvent(eventPlay, FALSE);
		}
	}

	if( uLine == NOTHING ) {

		PollSiren();

		if( m_uState == stateRunning ) {

			if( !m_fDebug ) {

				if( m_uTimeout1 && !--m_uTimeout1 ) {

					AfxTrace("\n*** TIMEOUT 1\n\n");

					HostTrap(15);
				}

				if( m_uTimeout2 && !--m_uTimeout2 ) {

					AfxTrace("\n*** TIMEOUT 2\n\n");

					HostTrap(14);
				}
			}
		}

		if( m_uIdentify ) {

			BOOL fState = ((m_uIdentify / 2) % 2) && ((m_uIdentify / 4) % 4);

			if( m_pLeds ) {

				m_pLeds->SetLed(0, fState);
				m_pLeds->SetLed(1, fState);
				m_pLeds->SetLed(2, fState);
			}
			else {
				SetHalo(haloIdentify);
			}

			m_uIdentify--;
		}
		else {
			if( m_uState == stateRunning ) {

				PollRunning();
			}
			else {
				PollStopped();
			}
		}

		if( TRUE ) {

			#if !defined(_DEBUG)

			if( m_uLoadTime && !--m_uLoadTime ) {

				m_LoadCount.SetFlag(TRUE);

				if( m_pBeep ) {

					m_pBeep->BeepOff();
				}

				AfxTrace("\n*** LOAD TIMEOUT\n");

				m_pPlatform->ResetSystem();
			}

			#endif
		}

		m_uTickCount++;
	}
}

// IDiagProvider

UINT CPxeObject::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagHello(pOut, pCmd);

		case 2:
			return DiagClearConfig(pOut, pCmd);

		case 3:
			return ((int *) 0)[0];
	}

	return 0;
}

// Timer Helpers

void CPxeObject::FindLedType(void)
{
	CString Model = m_pPlatform->GetModel();

	if( Model.StartsWith("GC") ) {

		m_uLedType = ledEdge;

		return;
	}

	if( Model.StartsWith("DA50") || Model.StartsWith("DA70") ) {

		SetHalo(haloStartup);

		m_uLedType = ledDAX;

		return;
	}

	m_uLedType = ledStd;
}

void CPxeObject::PollSiren(void)
{
	if( m_fSiren ) {

		if( m_uSiren == NOTHING ) {

			m_uSiren = 0;
		}

		if( m_uSiren % FLASH_RATE == 0 ) {

			UINT n = m_uSiren / FLASH_RATE;

			if( m_pBeep ) {

				if( n < 6 ) {

					switch( n % 3 ) {

						case 0:
							m_pBeep->Beep(72, 150);

							break;

						case 1:
							m_pBeep->Beep(84, 150);

							break;

						case 2:
							m_pBeep->Beep(96, 150);

							break;
					}
				}
			}

			if( m_pDisp ) {

				UINT d = (n % 2) ? 1 : 2;

				m_pDisp->SetBacklight(m_uBright / d);
			}
		}

		m_uSiren = (m_uSiren + 1) % (8 * FLASH_RATE);

		return;
	}

	if( m_uSiren < NOTHING ) {

		if( m_pDisp ) {

			m_pDisp->SetBacklight(m_uBright);
		}

		m_uSiren = NOTHING;
	}
}

void CPxeObject::PollRunning(void)
{
	BOOL fEnable = TRUE;

	if( m_pDisp ) {

		fEnable = m_pDisp->IsBacklightEnabled();
	}

	if( m_uLedType == ledDAX ) {

		switch( m_uAlarm ) {

			case 0:
				SetHalo(haloRunning);
				break;

			case 1:
				SetHalo(haloAccepted);
				break;

			case 2:
				SetHalo(haloActive);
				break;
		}

		return;
	}

	if( m_uLedType == ledEdge ) {

		m_pLeds->SetLed(1, fEnable);

		PollDriveStatus(2, fEnable);

		return;
	}

	if( m_pLeds ) {

		if( !(m_uLedMode & (1 << 0)) ) {

			BOOL fAlarm = FALSE;

			if( fEnable ) {

				switch( m_uAlarm ) {

					case 2:
						fAlarm = (m_uTickCount / FLASH_RATE) % 2;
						break;

					case 1:
						fAlarm = TRUE;
						break;
				}
			}

			m_pLeds->SetLed(0, fEnable ? (fAlarm ? stateOn : stateOff) : stateOff);
		}

		if( !(m_uLedMode & (1 << 1)) ) {

			PollDriveStatus(1, fEnable);
		}

		if( !(m_uLedMode & (1 << 2)) ) {

			m_pLeds->SetLed(2, fEnable);
		}
	}
}

void CPxeObject::PollStopped(void)
{
	if( m_uLedType == ledDAX ) {

		SetHalo(haloStopped);

		return;
	}

	if( m_uLedType == ledEdge ) {

		m_pLeds->SetLed(1, TRUE);

		PollDriveStatus(2, FALSE);

		return;
	}

	if( m_pLeds ) {

		m_pLeds->SetLed(0, stateOff);

		PollDriveStatus(1, stateOff);

		m_pLeds->SetLed(2, stateOn);
	}
}

void CPxeObject::PollDriveStatus(UINT uLED, BOOL fState)
{
	#if 0 // !!!

	if( m_pDiskMan ) {

		switch( m_pDiskMan->GetDiskStatus(m_iDriveC) ) {

			case diskEmpty:

				m_pLeds->SetLed(uLED, stateOff);

				break;

			case diskInvalid:

				m_pLeds->SetLed(uLED, (m_uTickCount & 0x10) ? stateRed : stateOff);

				break;

			case diskCheck:
			case diskFormat:

				m_pLeds->SetLed(uLED, (m_uTickCount & 0x04) ? stateGreen : stateOff);

				break;

			case diskLocked:

				m_pLeds->SetLed(uLED, (m_uTickCount & 0x01) ? stateGreen : stateOff);

				break;

			case diskReady:

				m_pLeds->SetLed(uLED, fState ? stateGreen : stateOff);

				break;
		}
	}

	#endif
}

// Diagnostics

BOOL CPxeObject::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "");

		pDiag->RegisterCommand(m_uProv, 1, "hello");

		pDiag->RegisterCommand(m_uProv, 2, "wipe");

		pDiag->RegisterCommand(m_uProv, 3, "die");

		pDiag->Release();

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeObject::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return TRUE;
	}

	return FALSE;
}

UINT CPxeObject::DiagHello(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 0 ) {

		pOut->Print("Crimson 3.2 Debug Console\n\n");

		pOut->Print("Time is ");

		ShowTime(pOut);

		pOut->Print("\n");

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CPxeObject::DiagClearConfig(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		if( !pCmd->FromConsole() ) {

			pOut->Error("must be run from console");

			return 2;
		}

		CString List = pCmd->GetArg(0);

		bool    fApp = false;

		for( UINT n = 0; List[n]; n++ ) {

			char cTag = char(tolower(List[n]));

			if( cTag == 'a' ) {

				SystemStop();

				m_pDbase->SetValid(FALSE);

				fApp = true;
			}
			else {
				if( strchr("hpsu", cTag) ) {

					CString Text, Name;

					if( m_pSchema->GetDefault(cTag, Text) ) {

						m_pSchema->GetNaming(cTag, Name);

						m_pConfig->SetConfig(cTag, Text, true);

						pOut->Print("%s configuration cleared\n", PCTXT(Name));
					}
				}
			}
		}

		if( fApp ) {

			SystemStart();

			pOut->Print("application config cleared\n");
		}

		return 0;
	}

	pOut->Error("specify string of one or more of h, p, s, u or a");

	return 1;
}

void CPxeObject::ShowTime(IDiagOutput *pOut)
{
	time_t t = time(NULL);

	struct tm *tm = gmtime(&t);

	pOut->Print("%2.2u/%2.2u/%2.2u %2.2u:%2.2u:%2.2u\n",
		    tm->tm_mon  + 1,
		    tm->tm_mday + 0,
		    tm->tm_year + 1900,
		    tm->tm_hour,
		    tm->tm_min,
		    tm->tm_sec
	);
}

// Implementation

void CPxeObject::CreateModel(void)
{
	#if defined(AEON_PLAT_WIN32)

	CString Model = m_pPlatform->GetModel();

	if( Model.StartsWith("DA50") || Model.StartsWith("DA70") ) {

		piob->RegisterSingleton("c3.model", 0, Create_LinuxModelData());
	}
	else {
		piob->RegisterSingleton("c3.model", 0, Create_RlosModelData());
	}

	#else

	#if defined(AEON_PLAT_LINUX)

	piob->RegisterSingleton("c3.model", 0, Create_LinuxModelData());

	#else

	piob->RegisterSingleton("c3.model", 0, Create_RlosModelData());

	#endif

	#endif

	AfxGetObject("c3.model", 0, IPxeModel, m_pModel);
}

void CPxeObject::CreatePxeObjects(void)
{
	m_pModel->MakePxeObjects();

	AfxGetObject("c3.schema-generator", 0, ISchemaGenerator, m_pSchema);

	AfxGetObject("c3.config-storage", 0, IConfigStorage, m_pConfig);

	AfxGetObject("c3.hardware-applicator", 0, IHardwareApplicator, m_pHwApp);

	AfxGetObject("c3.net-applicator", 0, INetApplicator, m_pNetApp);

	m_pSchema->Open();

	m_pConfig->AddUpdateSink(this);
}

BOOL CPxeObject::CheckImageFiles(BOOL fLoad)
{
	char tag[] = "dhspua";

	bool hit   = false;

	for( UINT n = 0; tag[n]; n++ ) {

		CImageLoader Image;

		if( Image.Check(tag[n]) ) {

			if( fLoad ) {

				m_pSplash->ShowStatus("Loading Image...");

				if( Image.Import() ) {

					hit = true;
				}
			}
			else {
				return TRUE;
			}
		}

		if( hit ) {

			if( tag[n] == 'd' || tag[n] == 'u' || tag[n] == 'a' ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CPxeObject::LoadHardwareConfig(void)
{
	return LoadConfig('h', FALSE, m_pHcon);
}

BOOL CPxeObject::LoadSystemConfig(void)
{
	return LoadConfig('s', TRUE, m_pScon);
}

BOOL CPxeObject::LoadPersonality(void)
{
	if( !m_fPconValid ) {

		for( UINT p = 0; p < 2; p++ ) {

			CString Text;

			if( m_pConfig->GetConfig('p', Text) ) {

				CAutoPointer<CJsonData> pJson(new CJsonData);

				if( pJson->Parse(Text) ) {

					CJsonData *pKeys;

					if( (pKeys = pJson->GetChild("set.keys")) ) {

						if( pKeys->GetCount() ) {

							for( INDEX i = pKeys->GetHead(); !pKeys->Failed(i); pKeys->GetNext(i) ) {

								CJsonData *pKey = pKeys->GetChild(i);

								m_PKeys.Insert(pKey->GetValue("name", ""), pKey->GetValue("value", ""));
							}
						}
					}

					m_fPconValid = TRUE;

					return TRUE;
				}
			}

			m_pSchema->GetDefault('p', Text);

			m_pConfig->SetConfig('p', Text, true);
		}

		AfxAssert(FALSE);

		return FALSE;
	}

	return TRUE;
}

BOOL CPxeObject::FreePersonality(void)
{
	CAutoGuard Guard;

	CAutoLock  Lock(m_pPconLock);

	m_fPconValid = FALSE;

	m_PKeys.Empty();

	return TRUE;
}

BOOL CPxeObject::LoadConfig(char cTag, BOOL fPerson, CJsonConfig * &pCon)
{
	for( UINT p = 0; p < 2; p++ ) {

		CString Text;

		if( m_pConfig->GetConfig(cTag, Text) ) {

			CAutoPointer<CJsonData> pJson(new CJsonData);

			if( pJson->Parse(Text) ) {

				delete pCon;

				IConfigStorage *pPerson = fPerson ? m_pConfig : (IConfigStorage *) NULL;

				pCon = new CJsonConfig(pJson.TakeOver(), pPerson);

				return TRUE;
			}
		}

		m_pSchema->GetDefault(cTag, Text);

		m_pConfig->SetConfig(cTag, Text, true);
	}

	AfxAssert(FALSE);

	return FALSE;
}

BOOL CPxeObject::ApplyHardwareConfig(CJsonConfig *pHcon)
{
	if( pHcon ) {

		AfxTrace("applying hardware configuration\n");
	}

	if( m_pHwApp->ApplySettings(pHcon) ) {

		if( pHcon ) {

			AfxTrace("done\n");
		}

		return TRUE;
	}

	if( pHcon ) {

		AfxTrace("reset needed\n");
	}

	return FALSE;
}

BOOL CPxeObject::ApplySystemConfig(CJsonConfig *pScon, BOOL fInit)
{
	m_fApply = TRUE;

	if( pScon ) {

		AfxTrace("applying system configuration\n");

		ApplyNetConfig(pScon);

		ApplyCertConfig(pScon->GetChild("net.ident"));

		ApplyLinkConfig(pScon->GetChild("g3link"), fInit);

		AfxTrace("done\n");
	}
	else {
		ApplyLinkConfig(NULL, fInit);

		ApplyCertConfig(NULL);

		ApplyNetConfig(NULL);
	}

	m_fApply = FALSE;

	return TRUE;
}

BOOL CPxeObject::ApplyNetConfig(CJsonConfig *pNet)
{
	if( CheckCRC(m_crcNet, pNet) ) {

		if( !pNet ) {

			m_pNetApp->ApplySettings(NULL);
		}
	}

	if( pNet ) {

		m_pNetApp->ApplySettings(pNet);

		CStringArray List;

		m_pNetApp->GetmDnsNames(List);

		m_pSplash->SetmDnsNames(List);
	}

	return TRUE;
}

BOOL CPxeObject::ApplyLinkConfig(CJsonConfig *pLink, BOOL fInit)
{
	BOOL fSerial = TRUE;

	if( CAutoFile("/./vap/opt/crimson/debug/noser", "r") ) {

		fSerial = FALSE;
	}

	if( CheckCRC(m_crcLink, pLink) ) {

		if( fInit ) {

			Delete_XPUsb();
		}

		if( fSerial ) {

			Delete_XPSerial();
		}

		Delete_XPPipe();

		Delete_XPNetwork();
	}

	if( pLink ) {

		if( fInit ) {

			Create_XPUsb(pLink->GetChild("usb"), 32500, m_pReqRouter);
		}

		if( fSerial ) {

			Create_XPSerial(pLink->GetChild("serial"), 21000, 0, m_pReqRouter);
		}

		if( true ) {

			CJsonConfig *pJson = pLink->GetChild("pipe");

			Create_XPPipe(pJson, 20000, m_pReqRouter);
		}

		if( true ) {

			CJsonConfig *pJson = pLink->GetChild("net");

			if( Create_XPNetwork(pJson, 25000, m_pReqRouter) ) {

				m_wTcpPort = WORD(pJson->GetValueAsUInt("port", 789, 1, 65535));
			}
			else {
				m_wTcpPort = 0;
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CPxeObject::ApplyCertConfig(CJsonConfig *pIdent)
{
	if( CheckCRC(m_crcIdent, pIdent) ) {

	}

	if( pIdent ) {

		UINT uMode = pIdent->GetValueAsUInt("autocert", 0, 0, 65535);

		if( uMode >= 100 ) {

			AfxGetAutoObject(pCertMan, "c3.certman", 0, ICertManager);

			if( pCertMan ) {

				if( pCertMan->GetIdentityCert(uMode, m_DefCertData, m_DefCertPriv, m_DefCertPass) ) {

					m_fAutoCert = FALSE;

					m_CertPrior.Empty();

					m_DefCertStep++;

					SaveCertificate();

					return TRUE;
				}
			}
		}
		else {
			if( uMode == 0 || uMode == 2 ) {

				pIdent->GetValueAsBlob("certdata", m_DefCertData);

				pIdent->GetValueAsBlob("certpriv", m_DefCertPriv);

				if( !m_DefCertData.IsEmpty() && !m_DefCertPriv.IsEmpty() ) {

					m_DefCertPass = pIdent->GetValue("certpass", "");

					if( uMode == 0 ) {

						m_fAutoCert = FALSE;

						m_CertPrior.Empty();

						m_DefCertStep++;

						SaveCertificate();

						return TRUE;
					}
				}
			}

			m_DefRootData.Empty();

			m_DefRootPriv.Empty();

			m_DefRootPass.Empty();
		}

		CString Name1 = pIdent->GetValue("xname1", "");

		CString Name2 = pIdent->GetValue("name", "");

		CString Name3 = m_DefName;

		if( !Name2.IsEmpty() && !Name2.Count('.') ) {

			Name2 += ".local";
		}

		if( !Name3.IsEmpty() && !Name3.Count('.') ) {

			Name3 += ".local";
		}

		Join(m_CertNames, Name1, Name2);

		Join(m_CertNames, Name3);

		m_CertAddrs = pIdent->GetValue("xip1", "");

		m_fAutoIps  = pIdent->GetValueAsBool("autoips", FALSE) ? FALSE : TRUE;

		m_fAutoCert = TRUE;

		m_CertPrior.Empty();
	}

	return TRUE;
}

BOOL CPxeObject::MakeCertificate(void)
{
	if( m_fAutoCert ) {

		BOOL fCustom = !m_DefRootData.IsEmpty();

		CString Addrs, Check;

		if( m_fAutoIps ) {

			UINT c = m_pNetUtils->GetInterfaceCount();

			for( UINT n = 0; n < c; n++ ) {

				CIpAddr Addr;

				if( m_pNetUtils->GetInterfaceAddr(n, Addr) ) {

					if( !Addr.IsEmpty() && !Addr.IsLoopback() ) {

						if( fCustom || Addr.IsPrivate() ) {

							Join(Addrs, Addr.GetAsText());
						}
					}
				}
			}
		}

		Join(Addrs, m_CertAddrs);

		Join(Check, Addrs, m_CertNames);

		if( m_CertPrior.CompareC(Check) ) {

			AfxNewAutoObject(pGen, "certgen", ICertGen);

			if( pGen ) {

				CStringArray AddrList, NameList;

				Addrs.Tokenize(AddrList, '|');

				m_CertNames.Tokenize(NameList, '|');

				// The stuff below using 's' isn't brilliant, but it provides the
				// Emulator platform and thus the configuration software with our
				// address so it can launch a browser to access the web server.

				BOOL s = TRUE;

				for( UINT n = 0; n < NameList.GetCount(); n++ ) {

					CString Name = NameList[n];

					if( !Name.IsEmpty() ) {

						BOOL fLocal = Name.EndsWith(".local");

						if( fCustom || fLocal ) {

							pGen->AddName(Name);
						}

						if( s && fLocal ) {

							m_pPlatform->SetWebAddr(Name);

							s = FALSE;
						}
					}
				}

				for( UINT a = 0; a < AddrList.GetCount(); a++ ) {

					CIpAddr Addr(AddrList[a]);

					if( !Addr.IsEmpty() ) {

						if( fCustom || Addr.IsPrivate() ) {

							pGen->AddAddr(Addr);
						}

						if( s ) {

							m_pPlatform->SetWebAddr(AddrList[a]);

							s = FALSE;
						}
					}
				}

				if( s ) {

					m_pPlatform->SetWebAddr(NULL);
				}

				if( fCustom ) {

					pGen->SetRootCert(m_DefRootData.GetPointer(), m_DefRootData.GetCount());

					pGen->SetRootPriv(m_DefRootPriv.GetPointer(), m_DefRootPriv.GetCount());

					pGen->SetRootPass(m_DefRootPass);
				}

				if( pGen->Generate() ) {

					m_DefCertData = CByteArray(pGen->GetCertData(), pGen->GetCertSize());

					m_DefCertPriv = CByteArray(pGen->GetPrivData(), pGen->GetPrivSize());

					m_DefCertPass = pGen->GetPrivPass();

					m_DefCertStep++;

					AfxTrace("made cert for [%s]\n", PCTXT(Check));

					SaveCertificate();
				}
			}

			m_CertPrior = Check;

			return TRUE;
		}
	}

	return TRUE;
}

BOOL CPxeObject::SaveCertificate(void)
{
	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		mkdir("/./tmp/crimson/defcert", 0755);

		CAutoFile("/./tmp/crimson/defcert/cert.crt.tmp", "w").Write(m_DefCertData);

		CAutoFile("/./tmp/crimson/defcert/cert.key.tmp", "w").Write(m_DefCertPriv);

		CAutoFile("/./tmp/crimson/defcert/password.tmp", "w").Write(m_DefCertPass);

		CString Path("/tmp/crimson/defcert/cert.key.tmp");

		CString Pass("/tmp/crimson/defcert/password.tmp");

		CPrintf Args("rsa -in %s -out %s -passin file:%s", PCSTR(Path), PCSTR(Path), PCSTR(Pass));

		if( pLinux->CallProcess("/usr/bin/openssl", Args, NULL, NULL) == 0 ) {

			AfxTrace("exported certificate to linux services\n");

			chmod("/./tmp/crimson/defcert/cert.crt.tmp", 0600);

			chmod("/./tmp/crimson/defcert/cert.key.tmp", 0600);

			unlink("/./tmp/crimson/defcert/password.tmp");

			unlink("/./tmp/crimson/defcert/cert.crt");

			unlink("/./tmp/crimson/defcert/cert.key");

			rename("/./tmp/crimson/defcert/cert.crt.tmp", "cert.crt");

			rename("/./tmp/crimson/defcert/cert.key.tmp", "cert.key");

			pLinux->CallProcess("/bin/sh", "/opt/crimson/scripts/c3-def-cert", NULL, NULL);

			return TRUE;
		}

		unlink("/./tmp/crimson/defcert/password.tmp");
	}

	return FALSE;
}

void CPxeObject::Join(CString &x, CString const &a, CString const &b)
{
	x = a;

	if( !b.IsEmpty() ) {

		if( !x.IsEmpty() ) {

			x += '|';
		}

		x += b;
	}
}

void CPxeObject::Join(CString &x, CString const &a)
{
	if( !a.IsEmpty() ) {

		if( !x.IsEmpty() ) {

			x += '|';
		}

		x += a;
	}
}

BOOL CPxeObject::CheckCRC(DWORD &crc, CJsonConfig * &pJson)
{
	DWORD c = pJson ? pJson->GetCRC() : 1;

	if( crc ) {

		if( crc == c ) {

			pJson = NULL;

			return FALSE;
		}

		crc = c;

		return TRUE;
	}

	crc = c;

	return FALSE;
}

void CPxeObject::CreateAppObjects(void)
{
	m_pModel->MakeAppObjects();

	AfxGetObject("c3.database", 0, IDatabase, m_pDbase);

	m_pDbase->Init();

	m_pReqRouter = Create_RequestRouter(this);

	for( UINT n = 0; n < 2; n++ ) {

		AfxNewObject("c3-app", ICrimsonApp, m_pApp);

		if( !n && !m_pApp ) {

			AfxGetAutoObject(pManager, "modman", 0, IModuleManager);

			pManager->LoadClientModule("CrimsonApp");

			continue;
		}

		break;
	}

	m_pApp->BindPxe(this);
}

void CPxeObject::ShowIpList(void)
{
	CStringArray List;

	UINT c = m_pNetUtils->GetInterfaceCount();

	for( UINT n = 0; n < c; n++ ) {

		CIpAddr Addr;

		if( m_pNetUtils->GetInterfaceAddr(n, Addr) ) {

			if( !Addr.IsLoopback() ) {

				CString Desc;

				m_pNetUtils->GetInterfaceDesc(n, Desc);

				List.Append(Desc);

				List.Append(Addr.GetAsText());
			}
		}
	}

	m_pSplash->SetIpList(List);
}

BOOL CPxeObject::ApplyDeviceConfig(void)
{
	PCSTR pTags = "hpsu";

	UINT  uItem = UINT(-1);

	BOOL  fSave = FALSE;

	for( UINT n = 0; pTags[n]; n++ ) {

		if( !m_pConfig->IsEdited(pTags[n]) ) {

			CItemInfo Info;

			if( m_pDbase->GetItemInfo(uItem, Info) ) {

				PCWORD pItem = PCWORD(m_pDbase->LockItem(uItem));

				if( pItem ) {

					if( HostToMotor(pItem[0]) == 0x1234 ) {

						if( HostToMotor(pItem[1]) == 0x0001 ) {

							PCSTR   pText = PCSTR(pItem+2);

							CString Prev;

							m_pConfig->GetConfig(pTags[n], Prev);

							if( strcmp(pText, Prev) ) {

								AfxTrace("applying change to %c\n", pTags[n]);

								m_pConfig->SetConfig(pTags[n], pText, false);

								fSave = TRUE;
							}
							else {
								AfxTrace("no change to %c\n", pTags[n]);
							}
						}
					}

					m_pDbase->FreeItem(uItem);
				}
			}
		}
		else {
			AfxTrace("leaving local edits for %c\n", pTags[n]);
		}

		uItem--;
	}

	return fSave;
}

BOOL CPxeObject::HasIpListChanged(void)
{
	m_IpList.Empty();

	UINT c = m_pNetUtils->GetInterfaceCount();

	for( UINT n = 0; n < c; n++ ) {

		CIpAddr Addr;

		if( m_pNetUtils->GetInterfaceAddr(n, Addr) ) {

			if( !m_IpList.IsEmpty() ) {

				m_IpList += '|';
			}

			m_IpList += Addr.GetAsText();
		}
	}

	return m_IpList != m_IpLast;
}

void CPxeObject::IpListChangeDone(void)
{
	m_IpLast = m_IpList;
}

void CPxeObject::SetHalo(UINT uHalo)
{
	if( m_uLedType == ledDAX ) {

		if( m_uHalo != uHalo ) {

			PCSTR p = NULL;

			switch( (m_uHalo = uHalo) ) {

				case haloStartup:
				{
					p = "202020-1000-1000,E0E0E0-400-0\n";
				}
				break;

				case haloStopped:
				{
					p = "202020-200\n";
				}
				break;

				case haloRunning:
				{
					p = "208020-200\n";
				}
				break;

				case haloAccepted:
				{
					p = "902020-200\n";
				}
				break;

				case haloActive:
				{
					p = "FF0000-400-0,202020-800-800\n";
				}
				break;

				case haloIdentify:
				{
					p = "FF0000-150-150,000000-150-150,00FF00-150-150,000000-150-150\n";
				}
				break;

				case haloExit:
				{
					p = "202020-1000-1000,E0E0E0-400-0\n";
				}
				break;
			}

			int fd = open("/./tmp/crimson/leds/LedManager.pipe", O_WRONLY);

			if( fd >= 0 ) {

				write(fd, p, strlen(p));

				close(fd);
			}
		}
	}
}

void CPxeObject::CheckDebug(void)
{
	if( CAutoFile("/./tmp/debug", "r") ) {

		m_fDebug = true;
	}
}

UINT CPxeObject::GetActiveDrives(void)
{
	UINT uMask = 0;

	uMask |= (m_pFileUtils->IsDiskMounted('C') ? 1 : 0) << 0;
	uMask |= (m_pFileUtils->IsDiskMounted('D') ? 1 : 0) << 1;
	uMask |= (m_pFileUtils->IsDiskMounted('E') ? 1 : 0) << 2;

	return uMask;
}

// End of File
