
#include "Intern.hpp"

#include "G3PromService.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Database Format
//

#include "../../../../../Version/dbver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Prom Loader Service
//

// Instantiator

global ILinkService * Create_PromService(ICrimsonPxe *pPxe)
{
	return New CG3PromService(pPxe);
}

// Constructor

CG3PromService::CG3PromService(ICrimsonPxe *pPxe)
{
	m_pPxe       = pPxe;

	m_pPlatform  = NULL;

	m_pBootPend  = NULL;

	m_fPend      = FALSE;

	AfxGetObject("platform", 0, IPlatform, m_pPlatform);

	AfxGetObject("c3.bootpend", 0, IFirmwareProgram, m_pBootPend);

	StdSetRef();
}

// Destructor

CG3PromService::~CG3PromService(void)
{
	AfxRelease(m_pPlatform);

	AfxRelease(m_pBootPend);
}

// IUnknown

HRESULT CG3PromService::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ILinkService);

	StdQueryInterface(ILinkService);

	return E_NOINTERFACE;
}

ULONG CG3PromService::AddRef(void)
{
	StdAddRef();
}

ULONG CG3PromService::Release(void)
{
	StdRelease();
}

// ILinkService

void CG3PromService::Timeout(void)
{
}

UINT CG3PromService::Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans)
{
	if( Req.GetService() == servProm ) {

		switch( Req.GetOpcode() ) {

			case promWriteProgram:
				return WriteProgram(Req, Rep);

			case promUpdateProgram:
				return UpdateProgram(Req, Rep);

			case promCheckLoader:
				return CheckLoader(Req, Rep);
		}

		return procError;
	}

	return procError;
}

void CG3PromService::EndLink(CG3LinkFrame &Req)
{
	if( Req.GetService() == servProm ) {

		switch( Req.GetOpcode() ) {

			case bootForceReset:
			{
				UINT  uPtr      = 0;

				DWORD dwTimeout = Req.ReadLong(uPtr);

				m_pPxe->RestartSystem(dwTimeout, 55);

				for(;;) Sleep(FOREVER);
			}

			break;
		}
	}
}

// Implementation

UINT CG3PromService::WriteProgram(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	UINT  uPtr   = 0;

	DWORD dwAddr = Req.ReadLong(uPtr);

	UINT  uCount = Req.ReadWord(uPtr);

	PBYTE pData  = PBYTE(Req.ReadData(uPtr, uCount));

	m_pBootPend->WriteProgram(pData, uCount);

	Rep.StartFrame(servProm, opAck);

	AfxTouch(dwAddr);

	return procOkay;
}

UINT CG3PromService::UpdateProgram(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	m_pBootPend->WriteVersion(NULL);

	m_pBootPend->StartProgram(0);

	Rep.StartFrame(servProm, opAck);

	return procOkay;
}

UINT CG3PromService::CheckLoader(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	m_pBootPend->ClearProgram(0);

	Rep.StartFrame(servProm, opAck);

	return procOkay;
}

// End of File
