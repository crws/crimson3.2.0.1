
#include "intern.hpp"

#include "CellTimeClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cell Time Client
//

// Constructor

CCellTimeClient::CCellTimeClient(CJsonConfig *pJson) : CBaseTimeClient(pJson)
{
	m_Name = "CellSync";

	m_Diag = "cell";
}

// Overridables

BOOL CCellTimeClient::PerformSync(void)
{
	// TODO -- Find correct numbered item!!!

	AfxGetAutoObject(pTime, "net.time", 0, ITimeSource);

	if( pTime ) {

		CTimeSourceInfo Info;

		if( pTime->GetTimeData(Info) ) {

			return SyncTime(Info);
		}
	}

	return FALSE;
}

// End of File
