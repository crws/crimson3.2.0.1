
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3Slave_HPP

#define	INCLUDE_G3Slave_HPP

////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Link.hpp"

#include "G3LinkFrame.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Dummy Configuration
//

#define g_uTimeout		10000

#define GetOemDriverBase()	0

////////////////////////////////////////////////////////////////////////
//
// Processing Results
//

enum
{
	procError,
	procOkay,
	procEndLink
};

////////////////////////////////////////////////////////////////////////
//
// G3 Link Transport
//

interface ILinkTransport : public IUnknown
{
	AfxDeclareIID(201, 2);

	virtual BOOL IsUsb(void)   = 0;
	virtual BOOL IsP2P(void)   = 0;
	virtual void SetAuth(void) = 0;
};

////////////////////////////////////////////////////////////////////////
//
// G3 Link Client Service
//

interface ILinkService : public IUnknown
{
	AfxDeclareIID(201, 1);

	virtual void Timeout(void)			                                   = 0;
	virtual UINT Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans) = 0;
	virtual void EndLink(CG3LinkFrame &Req)		                                   = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Programming Transports
//

extern BOOL Create_XPNetwork(CJsonConfig *pJson, UINT uLevel, ILinkService *pService);

extern BOOL Create_XPUsb(CJsonConfig *pJson, UINT uLevel, ILinkService *pService);

extern BOOL Create_XPSerial(CJsonConfig *pJson, UINT uLevel, UINT uPort, ILinkService *pService);

extern BOOL Create_XPPipe(CJsonConfig *pJson, UINT uLevel, ILinkService *pService);

extern BOOL Delete_XPNetwork(void);

extern BOOL Delete_XPUsb(void);

extern BOOL Delete_XPSerial(void);

extern BOOL Delete_XPPipe(void);

//////////////////////////////////////////////////////////////////////////
//
// Request Router
//

extern ILinkService * Create_RequestRouter(ICrimsonPxe *pPxe);

// End of File

#endif
