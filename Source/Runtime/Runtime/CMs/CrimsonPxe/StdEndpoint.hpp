
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_StdEndpoint_HPP

#define	INCLUDE_StdEndpoint_HPP

//////////////////////////////////////////////////////////////////////////
//
// Standard RFC Endpoint
//

class CStdEndpoint
{
public:
	// Constructor
	CStdEndpoint(void);

	// Destructor
	~CStdEndpoint(void);

	// Operations
	BOOL Send(PCTXT pText);
	BOOL SendFile(FILE *pFile, BOOL fCode);

protected:
	// Data Members
	CString	    m_LogFile;
	BOOL	    m_fDebug;
	ISocket   * m_pCmdSock;
	WORD	    m_wPort;

	// Transport
	BOOL Send(ISocket *pSock, PCTXT pText);

	// File Transmission
	BOOL SendFile(ISocket *pSock, FILE *pFile);
	BOOL SendFile(ISocket *pSock, FILE *pFile, BOOL fCode);
	BOOL RecvFile(ISocket *pSock, FILE *pFile);

	// Socket Management
	BOOL CheckCmdSocket(void);
	void CloseCmdSocket(void);
	void AbortCmdSocket(void);
};

// End of File

#endif
