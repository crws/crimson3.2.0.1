
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosNetApplicator_HPP

#define INCLUDE_RlosNetApplicator_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

class CMultiDns;

class CLocation;

//////////////////////////////////////////////////////////////////////////
//
// Network Interfaces
//

#include "../../StdEnv/ITcpStack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RLOS Net Applicator
//

class CRlosNetApplicator : public INetApplicator
{
public:
	// Constructor
	CRlosNetApplicator(void);

	// Destructor
	~CRlosNetApplicator(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// INetApplicator
	bool METHOD ApplySettings(CJsonConfig *pJson);
	bool METHOD GetUnitName(CString &Name);
	bool METHOD GetmDnsNames(CStringArray &List);
	bool METHOD GetInfo(CString &Status, PCSTR pName);

protected:
	// Data Members
	ULONG                 m_uRefs;
	INetUtilities	    * m_pUtils;
	IRouter		    * m_pRouter;
	CString		      m_DefName;
	DWORD		      m_crcFaces;
	DWORD		      m_crcRouting;
	DWORD		      m_crcResolver;
	DWORD		      m_crcNtpSync;
	DWORD		      m_crcSmtpClient;
	DWORD		      m_crcLocation;
	DWORD		      m_crcIdent;
	DWORD		      m_crcFtpServer;
	IThread		    * m_pMulti1;
	IThread		    * m_pMulti2;
	IThread		    * m_pIdentifier;
	IDnsResolver	    * m_pResolver;
	IThread		    * m_pNtpSync;
	IThread		    * m_pSmtpClient;
	CLocation	    * m_pLocation;
	IThread		    * m_pFtpServer;
	CString		      m_UnitName;
	CStringArray	      m_mDnsNames;
	CArray<CString>       m_FaceNames;
	CArray<CString>       m_FaceDescs;

	// Implementation
	BOOL ApplyFaces(CJsonConfig *pFaces);
	BOOL ApplyEthernet(UINT uInst, CJsonConfig *pFace);
	BOOL ApplyPpp(UINT uInst, CJsonConfig *pFace);
	BOOL ApplyRouting(CJsonConfig *pRouting);
	BOOL ApplyResolver(CJsonConfig *pResolver);
	BOOL ApplyServices(CJsonConfig *pResolver);
	BOOL ApplyNtpSync(CJsonConfig *pNtpSync);
	BOOL ApplySmtpClient(CJsonConfig *pNtpSync);
	BOOL ApplyLocation(CJsonConfig *pLocation);
	BOOL ApplyFtpServer(CJsonConfig *pFtpServer);
	BOOL ApplyIdent(CJsonConfig *pIdent);
	BOOL CheckCRC(DWORD &crc, CJsonConfig * &pJson);
};

// End of File

#endif
