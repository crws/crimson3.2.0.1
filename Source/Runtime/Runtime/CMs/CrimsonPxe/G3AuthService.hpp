
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3AuthService_HPP

#define	INCLUDE_G3AuthService_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Authentication Service
//

class CG3AuthService : public ILinkService
{
public:
	// Constructor
	CG3AuthService(ICrimsonPxe *pPxe);

	// Destructor
	~CG3AuthService(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ILinkService
	void Timeout(void);
	UINT Process(CG3LinkFrame &Req, CG3LinkFrame &Re, ILinkTransport *pTrans);
	void EndLink(CG3LinkFrame &Req);

protected:
	// Data Members
	ULONG	         m_uRefs;
	ICrimsonPxe    * m_pPxe;
};

// End of File

#endif
