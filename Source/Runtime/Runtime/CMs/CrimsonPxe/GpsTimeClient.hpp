
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GpsTimeClient_HPP

#define	INCLUDE_GpsTimeClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BaseTimeClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// GPS Time Client
//

class CGpsTimeClient : public CBaseTimeClient
{
public:
	// Constructor
	CGpsTimeClient(CJsonConfig *pJson);

protected:
	// Overridables
	BOOL PerformSync(void);
};

// End of File

#endif
