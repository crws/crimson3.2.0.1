
#include "Intern.hpp"

#include "G3PipeClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Serial Pipe Client Transport
//

// Static Data

static CG3PipeClient * m_pThis = NULL;

// Launcher

global BOOL Create_XPPipe(CJsonConfig *pJson, UINT uLevel, ILinkService *pService)
{
	CG3PipeClient *pClient = New CG3PipeClient(uLevel, pService);

	if( pClient->Open() ) {

		m_pThis = pClient;

		return TRUE;
	}

	delete pClient;

	return FALSE;
}

global BOOL Delete_XPPipe(void)
{
	if( m_pThis ) {

		m_pThis->Close();

		delete m_pThis;

		m_pThis = NULL;

		return TRUE;
	}

	return FALSE;
}

// Constructor

CG3PipeClient::CG3PipeClient(UINT uLevel, ILinkService *pService) : CG3SerialBase(uLevel, pService)
{
	m_Name = "XpPipe";
}

// Transport Layer

BOOL CG3PipeClient::OpenPort(void)
{
	GuardThread(TRUE);

	AfxGetObject("dev.pipe", 0, IPortObject, m_pPort);

	if( m_pPort ) {

		AfxNewObject("util.data-s", IDataHandler, m_pData);

		if( m_pData ) {

			m_pPort->Bind(m_pData);

			m_pData->SetRxSize(2560);

			CSerialConfig Config = { 0 };

			Config.m_uBaudRate = 115200;

			Config.m_uDataBits = 8;

			Config.m_uStopBits = 1;

			if( m_pPort->Open(Config) ) {

				GuardThread(FALSE);

				return TRUE;
			}

			ClosePort();
		}
	}

	GuardThread(FALSE);

	return FALSE;
}

// End of File
