
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DnsResolver_HPP

#define	INCLUDE_DnsResolver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PxeIpClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDnsCache;

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// DNS Client Configuration
//

class CDnsResolver : public CPxeIpClient, public IDnsResolver
{
public:
	// Constructor
	CDnsResolver(CJsonConfig *pJson, UINT uFaces);

	// Destructor
	~CDnsResolver(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDnsResolver
	CIpAddr METHOD Resolve(PCTXT pName);
	BOOL    METHOD Resolve(CArray <CIpAddr> &List, PCTXT pName);

protected:
	// Data Members
	ULONG	    m_uRefs;
	CString	    m_Suffix;
	CDnsCache * m_pCache;
	IMutex    * m_pMutex;
	WORD        m_wID;

	// Implementation
	void ApplyConfig(CJsonConfig *pJson);
	BOOL DoResolve(PCTXT pHost, CIpList &List);
	BOOL SendQuery(ISocket *pSock, PCTXT pHost);
	BOOL RecvReply(ISocket *pSock, CIpList &List, DWORD &dwTTL, UINT uTime);
	BOOL IsDotted(PCTXT pText);
};

// End of File

#endif
