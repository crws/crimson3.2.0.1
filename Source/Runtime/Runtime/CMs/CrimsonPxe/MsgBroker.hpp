
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MsgBroker_HPP

#define	INCLUDE_MsgBroker_HPP

//////////////////////////////////////////////////////////////////////////
//
// Message Broker
//

class CMsgBroker : public IMsgBroker
{
public:
	// Constructor
	CMsgBroker(void);

	// Destructor
	~CMsgBroker(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IMsgBroker
	UINT METHOD RegisterRecvNotify(IMsgRecvNotify *pNotify);
	BOOL METHOD RevokeRecvNotify(UINT uNotify);
	BOOL METHOD SubmitMessage(PCTXT pName, CMsgPayload *pMessage);

protected:
	// Static Data
	static UINT m_uSeq;

	// Data Members
	ULONG			     m_uRefs;
	IMutex			   * m_pLock;
	CMap<UINT, IMsgRecvNotify *> m_Notify;
};

// End of File

#endif
