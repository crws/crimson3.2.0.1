
#include "intern.hpp"

#include "RadiusClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

#include "RadiusFrame.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Radius Client Service
//

// Constructor

CRadiusClient::CRadiusClient(CJsonConfig *pJson)
{
	m_pJson = pJson;
}

// Destructor

CRadiusClient::~CRadiusClient(void)
{
}

// Operations

UINT CRadiusClient::GetUserInfo(CString const &User, CAuthInfo const &Auth, CUserInfo &Info)
{
	for( UINT n = 1; n <= 2; n++ ) {

		UINT c;

		switch( (c = CheckUser(n, User, Auth, Info)) ) {

			case accessAllowed:
			case accessDenied:

				return c;
		}
	}

	return accessFailed;
}

// Implementation

UINT CRadiusClient::CheckUser(UINT n, CString const &User, CAuthInfo const &Auth, CUserInfo &Info)
{
	CIpAddr Addr = m_pJson->GetValueAsIp(CPrintf("server%u", n), IP_EMPTY);

	if( !Addr.IsEmpty() ) {

		CString Secret = m_pJson->GetValue(CPrintf("secret%u", n), "");

		if( !Secret.IsEmpty() ) {

			UINT Port = m_pJson->GetValueAsUInt(CPrintf("port%u", n), 1812, 1, 65535);

			AfxNewAutoObject(pSock, "sock-udp", ISocket);

			if( pSock ) {

				pSock->Connect(Addr, WORD(Port));

				BYTE bSend[256] = { 0 };

				BYTE bRecv[256] = { 0 };

				CRadiusFrame &Send = (CRadiusFrame &) bSend[0];

				CRadiusFrame &Recv = (CRadiusFrame &) bRecv[0];

				for( UINT n = 0; n < 2; n++ ) {

					Send.Create(Send.codeAccessRequest, BYTE(rand()));

					Send.AddAttribute(Send.attrUserName, User);

					if( Auth.m_Challenge.GetCount() ) {

						Send.AddChap(Auth.m_Challenge, Auth.m_Response);
					}
					else {
						if( m_pJson->GetValueAsUInt(CPrintf("method%u", n), 0, 0, 1) ) {

							Send.AddChapPassword(Auth.m_Password);
						}
						else {
							Send.AddPassword(Secret, Auth.m_Password);
						}
					}

					Send.AddPppAttributes();

					UINT uSend = Send.m_wLength;

					Send.HostToNet();

					if( pSock->Send(bSend, uSend) == S_OK ) {

						SetTimer(500);

						while( GetTimer() ) {

							UINT uRecv = sizeof(bRecv);

							if( pSock->Recv(bRecv, uRecv) == S_OK ) {

								if( Recv.m_bIdent == Send.m_bIdent ) {

									if( Recv.Validate(Secret, Send) ) {

										if( Recv.m_bCode == Recv.codeAccessAccept ) {

											Info.m_User       = User;

											Info.m_Real       = Info.m_User;

											Info.m_fLocal     = FALSE;

											Info.m_uWebAccess = m_pJson->GetValueAsUInt("access", 0, 0, 3);

											for( UINT uPos = 0; Recv.HasAttribute(uPos); ) {

												BYTE   bType;

												UINT   uSize;

												PCBYTE pData;

												Recv.GetAttribute(uPos, bType, uSize, pData);

												if( bType == 26 ) {

													if( MotorToHost(PDWORD(pData)[0]) == 9 ) {

														if( pData[4] == 1 ) {

															CString tag(PCSTR(pData+6), pData[5]-2);

															DecodeAccessTag(tag, Info);

															break;
														}
													}
												}
											}

											Info.m_uFtpAccess = (Info.m_uWebAccess == 3) ? 1 : 2;

											return accessAllowed;
										}

										return accessDenied;
									}

									break;
								}
							}
						}
					}
				}
			}
		}
	}

	return accessFailed;
}

bool CRadiusClient::DecodeAccessTag(CString const &tag, CUserInfo &Info)
{
	if( tag == "admin" ) {

		Info.m_uWebAccess = 0;

		return true;
	}

	if( tag == "editor" ) {

		Info.m_uWebAccess = 1;

		return true;
	}

	if( tag == "peditor" ) {

		Info.m_uWebAccess = 2;

		return true;
	}

	if( tag == "viewer" ) {

		Info.m_uWebAccess = 3;

		return true;
	}

	return false;
}

// End of File
