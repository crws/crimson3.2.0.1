
#include "Intern.hpp"

#include "WebFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Web Files
//

// Constructors

CWebFiles::CWebFiles(void)
{
	MakeIndex();

	FindTimes();
}

// File Lookup

BOOL CWebFiles::FindFile(CWebFileInfo &Info, PCTXT pName)
{
	INDEX n = m_Index.FindName(pName);

	if( !m_Index.Failed(n) ) {

		Info.p = m_Index.GetData(n);

		Info.t = m_timeComp;

		return TRUE;
	}

	return FALSE;
}

// Implementation

BOOL CWebFiles::MakeIndex(void)
{
	for( UINT n = 0; n < m_uCount1; n++ ) {

		m_Index.Insert(m_Files1[n].m_pName, &m_Files1[n]);
	}

	for( UINT n = 0; n < m_uCount2; n++ ) {

		m_Index.Insert(m_Files2[n].m_pName, &m_Files2[n]);
	}

	for( UINT n = 0; n < m_uCount3; n++ ) {

		m_Index.Insert(m_Files3[n].m_pName, &m_Files3[n]);
	}

	for( UINT n = 0; n < m_uCount4; n++ ) {

		m_Index.Insert(m_Files4[n].m_pName, &m_Files4[n]);
	}

	for( UINT n = 0; n < m_uCount5; n++ ) {

		m_Index.Insert(m_Files5[n].m_pName, &m_Files5[n]);
	}

	return TRUE;
}

void CWebFiles::FindTimes(void)
{
	m_timeBoot = time(NULL);

	m_timeComp = CHttpTime::Parse("Day, " __DATE__ " " __TIME__ " EST");
}

// End of File
