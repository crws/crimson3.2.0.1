
#include "Intern.hpp"

#include "LinuxCellStatus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Linux Cell Status
//

// Constructor

CLinuxCellStatus::CLinuxCellStatus(PCTXT pName, UINT uInst)
{
	m_Name  = pName;

	m_uInst = uInst;

	m_pLock = Create_Mutex();

	m_Face.Printf("iface.%s", pName);

	piob->RegisterSingleton("net.cell", m_uInst, (ICellStatus *) this);

	piob->RegisterSingleton("net.location", m_uInst, (ILocationSource *) this);

	piob->RegisterSingleton("net.time", m_uInst, (ITimeSource *) this);

	piob->RegisterSingleton(m_Face, 0, (IInterfaceStatus *) this);

	m_timeStatus   = 0;

	m_timeLocation = 0;

	m_timeTime     = 0;

	StdSetRef();
}

// Destructor

CLinuxCellStatus::~CLinuxCellStatus(void)
{
	piob->RevokeSingleton("net.cell", m_uInst);

	piob->RevokeSingleton("net.location", m_uInst);

	piob->RevokeSingleton("net.time", m_uInst);

	piob->RevokeSingleton(m_Face, 0);

	AfxRelease(m_pLock);
}

// IUnknown

HRESULT CLinuxCellStatus::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ICellStatus);

	StdQueryInterface(IInterfaceStatus);

	StdQueryInterface(ICellStatus);

	StdQueryInterface(ILocationSource);

	StdQueryInterface(ITimeSource);

	return E_NOINTERFACE;
}

ULONG CLinuxCellStatus::AddRef(void)
{
	StdAddRef();
}

ULONG CLinuxCellStatus::Release(void)
{
	StdRelease();
}

// IInterfaceStatus

BOOL CLinuxCellStatus::GetInterfaceStatus(CInterfaceStatusInfo &Info)
{
	CAutoLock Lock(m_pLock);

	ReadStatus();

	Info = m_CellInfo;

	return TRUE;
}

// ICellStatus

BOOL CLinuxCellStatus::GetCellStatus(CCellStatusInfo &Info)
{
	CAutoLock Lock(m_pLock);

	ReadStatus();

	Info = m_CellInfo;

	return TRUE;
}

BOOL CLinuxCellStatus::SendCommand(PCTXT pCmd)
{
	CAutoLock Lock(m_pLock);

	CPrintf   Name("/./tmp/crimson/face/%s/command", PCSTR(m_Name));

	CAutoFile File(Name, "w+");

	if( File ) {

		File.PutLine(pCmd);

		return TRUE;
	}

	return FALSE;
}

// ILocationSource

BOOL CLinuxCellStatus::GetLocationData(CLocationSourceInfo &Info)
{
	CAutoLock Lock(m_pLock);

	ReadLocation();

	Info = m_LocationData;

	return TRUE;
}

BOOL CLinuxCellStatus::GetLocationTime(CTimeSourceInfo &Info)
{
	CAutoLock Lock(m_pLock);

	ReadLocation();

	Info = m_LocationTime;

	return TRUE;
}

// ITimeSource

BOOL CLinuxCellStatus::GetTimeData(CTimeSourceInfo &Info)
{
	ReadTime();

	CAutoLock Lock(m_pLock);

	Info = m_TimeData;

	return TRUE;
}

// Implementation

bool CLinuxCellStatus::ReadJson(CJsonData &Json, PCTXT pType)
{
	CPrintf   Name("/./tmp/crimson/face/%s/%s", PCSTR(m_Name), pType);

	CAutoFile File(Name, "r");

	if( File ) {

		UINT uSize = File.GetSize();

		CAutoArray<char> Data(uSize+1);

		if( File.Read(Data, uSize) == uSize ) {

			Data[uSize] = 0;

			if( Json.Parse(Data) ) {

				return true;
			}
		}
	}

	return false;
}

void CLinuxCellStatus::ReadStatus(void)
{
	DWORD time = GetTickCount();

	bool  good = false;

	if( time - m_timeStatus >= 1000 ) {

		for( int t = 0; t < 2; t++ ) {

			CJsonData Json;

			if( ReadJson(Json, "status") ) {

				m_CellInfo.m_fValid    = TRUE;

				m_CellInfo.m_Device    = m_Name;

				m_CellInfo.m_fRegister = (Json.GetValue("register", "") == "true");

				m_CellInfo.m_fRoam     = (Json.GetValue("roam", "") == "true");

				m_CellInfo.m_fOnline   = (Json.GetValue("online", "") == "true");

				m_CellInfo.m_fReqOff   = (Json.GetValue("reqoff", "") == "true");

				m_CellInfo.m_Network   = Json.GetValue("network", "Unknown");

				m_CellInfo.m_Service   = Json.GetValue("service", "Unknown");

				m_CellInfo.m_uSignal   = atoi(Json.GetValue("signal", "99"));

				m_CellInfo.m_uSlot     = atoi(Json.GetValue("slot", "0"));

				m_CellInfo.m_Model     = Json.GetValue("model", "Unknown");

				m_CellInfo.m_Version   = Json.GetValue("version", "Unknown");

				m_CellInfo.m_Carrier   = Json.GetValue("carrier", "Unknown");

				m_CellInfo.m_Iccid     = Json.GetValue("iccid", "Unknown");

				m_CellInfo.m_Imsi      = Json.GetValue("imsi", "Unknown");

				m_CellInfo.m_Imei      = Json.GetValue("imei", "Unknown");

				m_CellInfo.m_State     = Json.GetValue("state", "Undefined");

				m_CellInfo.m_Addr      = Json.GetValue("addr", "0.0.0.0");

				m_CellInfo.m_CTime     = atoi(Json.GetValue("ctime", "0"));

				m_CellInfo.m_MxTime    = atoi(Json.GetValue("mxtime", "0"));

				m_CellInfo.m_RxBytes   = strtoull(Json.GetValue("rxbytes", "0"), NULL, 10);

				m_CellInfo.m_TxBytes   = strtoull(Json.GetValue("txbytes", "0"), NULL, 10);

				good = true;

				break;
			}

			Sleep(5);
		}

		if( !good ) {

			m_CellInfo.m_fValid  = FALSE;
		}

		m_timeStatus = time;
	}
}

void CLinuxCellStatus::ReadLocation(void)
{
	DWORD time = GetTickCount();

	bool  good = false;

	if( time - m_timeLocation >= 1000 ) {

		for( int t = 0; t < 2; t++ ) {

			CJsonData Json;

			if( ReadJson(Json, "location") ) {

				m_LocationData.m_uFix   = atoi(Json.GetValue("fix", "0"));

				m_LocationData.m_Lat    = strtod(Json.GetValue("lat", "0"), NULL);

				m_LocationData.m_Long   = strtod(Json.GetValue("long", "0"), NULL);

				m_LocationData.m_Alt    = strtod(Json.GetValue("alt", "0"), NULL);

				m_LocationTime.m_fValid = TRUE;

				m_LocationTime.m_Remote = (INT64(1000) * atoi(Json.GetValue("ts", "0"))) + atoi(Json.GetValue("tm", "0"));

				m_LocationTime.m_Local  = (INT64(1000) * atoi(Json.GetValue("rs", "0"))) + atoi(Json.GetValue("rm", "0"));

				m_LocationTime.m_Zone   = 9999;

				m_LocationTime.m_Dst    = 9999;

				good = true;

				break;
			}

			Sleep(5);
		}

		if( !good ) {

			m_LocationData.m_uFix   = 0;

			m_LocationTime.m_fValid = FALSE;
		}

		m_timeLocation = time;
	}
}

void CLinuxCellStatus::ReadTime(void)
{
	DWORD time = GetTickCount();

	bool  good = false;

	if( time - m_timeTime >= 500 ) {

		for( int t = 0; t < 2; t++ ) {

			CJsonData Json;

			if( ReadJson(Json, "time") ) {

				m_TimeData.m_fValid = TRUE;

				m_TimeData.m_Remote = (INT64(1000) * atoi(Json.GetValue("ts", "0"))) + atoi(Json.GetValue("tm", "0"));

				m_TimeData.m_Local  = (INT64(1000) * atoi(Json.GetValue("rs", "0"))) + atoi(Json.GetValue("rm", "0"));

				m_TimeData.m_Zone   = atoi(Json.GetValue("tz", "9999"));

				m_TimeData.m_Dst    = atoi(Json.GetValue("ds", "9999"));

				good = true;

				break;
			}

			Sleep(5);
		}

		if( !good ) {

			m_TimeData.m_fValid = FALSE;
		}

		m_timeTime = time;
	}
}

// End of File
