
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_StdServer_HPP

#define	INCLUDE_StdServer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "StdEndpoint.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Standard RFC Server
//

class CStdServer : public CStdEndpoint
{
public:
	// Constructor
	CStdServer(void);

	// Destructor
	~CStdServer(void);

protected:
	// Data Members
	char m_sCmdData[1024];
	UINT m_uCmdState;
	UINT m_uCmdPtr;

	// TLS Support
	ITlsServerContext * m_pTls;

	// Transport
	BOOL SendReply(UINT uCode, PCTXT pText);
	BOOL RecvCommand(void);
	void OnCommand(char cData);

	// Socket Management
	BOOL OpenCmdSocket(WORD wPort, BOOL fLarge);
	BOOL WaitCmdSocket(void);
	BOOL SwitchCmdSocket(void);
	BOOL CreateTlsContext(void);
};

// End of File

#endif
