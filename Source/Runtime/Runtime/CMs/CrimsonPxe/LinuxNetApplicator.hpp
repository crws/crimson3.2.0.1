

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinuxNetApplicator_HPP

#define INCLUDE_LinuxNetApplicator_HPP

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

class CLinuxCellStatus;

class CLocation;

class CCertManager;

//////////////////////////////////////////////////////////////////////////
//
// Linux Net Applicator
//

class CLinuxNetApplicator : public INetApplicator
{
public:
	// Constructor
	CLinuxNetApplicator(void);

	// Destructor
	~CLinuxNetApplicator(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// INetApplicator
	bool METHOD ApplySettings(CJsonConfig *pJson);
	bool METHOD GetUnitName(CString &Name);
	bool METHOD GetmDnsNames(CStringArray &List);
	bool METHOD GetInfo(CString &Status, PCSTR pName);

protected:
	// Interface Info
	struct CFaceInfo
	{
		CJsonConfig   *	m_pFace;
		UINT		m_uIndex;
		UINT		m_uMajor;
		UINT		m_uMinor;
		PCSTR		m_pName;
		PCSTR		m_pBase;
		CString		m_Name;
		CString		m_Base;
		CString		m_Desc;
		CString		m_Path;

		CFaceInfo(void)
		{
			m_uIndex = 0;

			m_uMajor = 0;

			m_uMinor = 0;
		}

		void Load(CJsonConfig *pFace, CString const &Name, PCTXT pPath, UINT &uPath)
		{
			m_pFace = pFace;

			m_Name  = Name;

			m_Base  = Name;

			m_pName = PCSTR(m_Name);

			m_pBase = PCSTR(m_Base);

			m_Desc.Empty();

			m_Path.Printf(pPath, uPath++);
		}

		void SetBoth(CString const &Name)
		{
			m_Name  = Name;

			m_Base  = Name;

			m_pName = PCSTR(m_Name);

			m_pBase = PCSTR(m_Base);
		}

		void SetName(CString const &Name)
		{
			m_Name  = Name;

			m_pName = PCSTR(m_Name);
		}

		void StepMajor(BOOL fIndex)
		{
			if( fIndex ) {

				StepIndex();
			}

			m_uMajor++;

			m_uMinor = 0;
		}

		void StepMinor(void)
		{
			m_uMinor++;
		}

		void StepIndex(void)
		{
			m_uIndex++;
		}

		CString GetValue(PCSTR pName, PCSTR pDef)
		{
			return m_pFace->GetValue(pName, pDef);
		}

		UINT GetUInt(PCSTR pName, UINT uDef, UINT uMin, UINT uMax)
		{
			return m_pFace->GetValueAsUInt(pName, uDef, uMin, uMax);
		}

		BOOL GetBool(PCSTR pName, BOOL fDef)
		{
			return m_pFace->GetValueAsBool(pName, fDef);
		}

		CIpAddr GetIp(PCSTR pName, CIpAddr Def)
		{
			return m_pFace->GetValueAsIp(pName, Def);
		}

		BOOL GetBlob(CString Name, CByteArray &Data)
		{
			return m_pFace->GetValueAsBlob(Name, Data);
		}
	};

	// Scripts
	enum
	{
		scriptUp    = 0,
		scriptDn    = 1,
		scriptStart = 2,
		scriptStop  = 3,
		scriptMake  = 4,
		scriptBreak = 5,
		configFile  = 6,
		scriptCount = 7,
	};

	// Data Members
	ULONG                  m_uRefs;
	INetUtilities	     * m_pUtils;
	CString		       m_DefName;
	DWORD		       m_crcCertManager;
	DWORD		       m_crcIdent;
	DWORD		       m_crcZoneSync;
	DWORD		       m_crcNtpSync;
	DWORD		       m_crcCellSync;
	DWORD		       m_crcGpsSync;
	DWORD		       m_crcSmtpClient;
	DWORD		       m_crcSmsClient;
	DWORD		       m_crcLocation;
	DWORD		       m_crcFtpServer;
	DWORD		       m_crcSvmClient;
	DWORD		       m_crcIoMonitor;
	CCertManager         * m_pCertManager;
	IThread		     * m_pMulti1;
	IThread		     * m_pMulti2;
	IThread		     * m_pIdentifier;
	IThread		     * m_pZoneSync;
	IThread		     * m_pNtpSync;
	IThread		     * m_pCellSync;
	IThread		     * m_pGpsSync;
	IThread		     * m_pSmtpClient;
	IThread		     * m_pSmsClient;
	CLocation	     * m_pLocation;
	IThread		     * m_pFtpServer;
	IThread		     * m_pSvmClient;
	IThread		     * m_pIoMonitor;
	CMap<UINT, UINT>       m_BridgeMap;
	CArray<UINT>	       m_BridgeRev;
	CArray<UINT>	       m_BridgeMtu;
	CStringArray	       m_Sharers;
	CStringArray	       m_FiltInit;
	CStringArray	       m_FiltTerm;
	CStringArray	       m_FireInit;
	CStringArray	       m_FireHead;
	CStringArray	       m_FireDrop;
	CStringArray	       m_FireLast;
	CStringArray	       m_FireTerm;
	CMap<CString, CString> m_Dependencies;
	CStringArray	       m_IpSecConfig;
	CStringArray	       m_IpSecSecrets;
	CMap<UINT, CString>    m_WhiteLists;
	CStringArray	       m_UntrustedRules;
	CUIntArray	       m_UntrustedFaces;
	CMap<UINT, CString>    m_AutoIps;
	BOOL		       m_fRoutingRules;
	CJsonConfig          * m_pRoutesStatic;
	CJsonConfig          * m_pRoutesOutbound;
	CJsonConfig          * m_pRoutesInbound;
	CString		       m_DhcpRelay;
	CStringArray	       m_DhcpServer;
	CStringArray	       m_DhcpClients;
	BOOL		       m_fDnsProxy;
	BOOL		       m_fNtpProxy;
	CString		       m_UnitName;
	CStringArray	       m_mDnsNames;
	CStringArray	       m_Init1;
	CStringArray	       m_Init2;
	CStringArray	       m_Term1;
	CStringArray	       m_Term2;
	CArray<CString>        m_FaceNames;
	CArray<CString>        m_FaceDescs;
	CArray<CString>        m_FacePaths;
	CArray<UINT>	       m_FaceIds;
	CArray<IUnknown *>     m_FaceStatus;
	CArray<UINT>	       m_RuleFaces;
	CString		       m_IptFace;

	// Interface Naming
	UINT    GetFaceId(CString const &Face);
	CString GetFaceName(UINT id);

	// Physical Interfaces
	BOOL ApplyNetwork(CJsonConfig *pJson);
	BOOL ApplyFaces(CJsonConfig *pFaces, CJsonConfig *pTuns);
	BOOL ApplyBaseEthernet(CFaceInfo &Info);
	BOOL ApplySledEthernet(CFaceInfo &Info);
	BOOL ApplySledWifi(CFaceInfo &Info);
	BOOL ApplySledCell(CFaceInfo &Info);
	BOOL ApplyWifi(CStringArray *s, CFaceInfo &Info);
	BOOL ApplyCell(CStringArray *s, CFaceInfo &Info, BOOL ipt);
	BOOL ApplyCellSim(CStringArray *s, CFaceInfo &Info, UINT uSim);
	BOOL ApplyEthernet(CStringArray *s, CFaceInfo &Info, UINT face, UINT defm, UINT mode);
	BOOL ApplyVLans(CStringArray *s, CFaceInfo &Root);
	void ApplyStaticIp(CStringArray *s, CFaceInfo &Info, UINT defm, UINT asym, UINT prio);
	void ApplyStaticIp(CStringArray &init, CStringArray &term, CFaceInfo &Info, UINT defm, UINT asym, UINT prio);
	void ApplyDhcpClient(CStringArray *s, CFaceInfo &Info, PCSTR pHost, BOOL fGate);
	void ApplyDhcpClient(CStringArray &init, CStringArray &term, CFaceInfo &Info, PCSTR pHost, BOOL fGate);
	UINT ApplyPhysical(CStringArray *s, CFaceInfo &Info, UINT mtu);
	UINT ApplyPhysical(CStringArray *s, CFaceInfo &Info);
	BOOL ApplyAliases(CStringArray *s, CFaceInfo &Info, CIpAddr const &IntAddr, CIpAddr const &IntMask, UINT defm);
	BOOL ApplyDhcpServer(CStringArray *s, CFaceInfo &Info);
	BOOL ApplyDynDns(CStringArray *s, CFaceInfo &Info, BOOL fTrans);
	BOOL ApplySharing(CStringArray *s, CFaceInfo &Info);
	BOOL ApplyMonitor(CStringArray *s, CFaceInfo &Info);
	BOOL ApplyVrrp(CStringArray *s, CFaceInfo &Info);
	BOOL AddFace(CStringArray *s, CFaceInfo &Info, IUnknown *pStatus, UINT uCapture);
	BOOL ScanFacesForBridges(CJsonConfig *pFaces);
	BOOL ScanTunnelsForBridges(CJsonConfig *pFaces);
	UINT FindBridge(UINT uFace);
	BOOL CreateBridge(CStringArray *s, CFaceInfo &Info, UINT defm, UINT mtu);
	BOOL BindToBridge(CStringArray *s, CFaceInfo &Info, BOOL primary);
	BOOL IsTrusted(CJsonConfig *pFace);
	BOOL GetMetric(CJsonConfig *pFace, UINT uDefault);
	BOOL AppendWatcher(CStringArray *s, PCSTR pType, PCSTR pName);
	BOOL AppendProgram(CStringArray *s, UINT uBase, PCSTR pProgram, PCSTR pSuffix, PCSTR pName);
	BOOL AppendProgram(CStringArray &init, CStringArray &term, PCSTR pProgram, PCSTR pTag);

	// Tunnel Interfaces
	BOOL    ApplyTunnelGre(CFaceInfo &Info);
	BOOL    ApplyTunnelIpsec(CFaceInfo &Info);
	BOOL    ApplyTunnelOpenVpn(CFaceInfo &Info, bool tap);
	BOOL    ApplyIpsecDeadPeer(CStringArray &s, CFaceInfo &Info);
	BOOL    ApplyIpsecPhase1(CStringArray &s, CFaceInfo &Info);
	BOOL    ApplyIpsecPhase2(CStringArray &s, CFaceInfo &Info);
	CString ApplyIpsecFace(CStringArray &s, CFaceInfo &Info);
	BOOL	ApplyIpsecAuth(CStringArray &s, DWORD &crc, CFaceInfo &Info, PCSTR pLoc, PCSTR pRem);
	BOOL	ApplyIpsecAuthPsk(CStringArray &s, CFaceInfo &Info, PCSTR pLoc, PCSTR pRem);
	BOOL	ApplyIpsecAuthCert(CStringArray &s, DWORD &crc, CFaceInfo &Info, PCSTR pLoc, PCSTR pRem);
	BOOL	ApplyIpsecOption(CStringArray &s, IPREF Addr, PCSTR pName);
	BOOL	ApplyIpsecOption(CStringArray &s, PCSTR pData, PCSTR pName);
	BOOL	ApplyFinalIpSec(void);
	BOOL	ApplyOpenPorts(CStringArray &s, CFaceInfo &Info);
	BOOL	ApplyOpenFace(CStringArray &s, CFaceInfo &Info);
	BOOL	ApplyOpenMode(CStringArray &s, DWORD &crc, CFaceInfo &Info, UINT mode, UINT cmode, UINT proto, bool tap);
	BOOL	ApplyOpenAuth(CStringArray &s, DWORD &crc, CFaceInfo &Info, UINT mode, UINT cmode);
	BOOL	ApplyOpenKeepAlive(CStringArray &s, CFaceInfo &Info);
	BOOL	ApplyOpenCipher(CStringArray &s, CFaceInfo &Info, UINT mode);
	BOOL	ApplyOpenMisc(CStringArray &s, CFaceInfo &Info);
	BOOL	ApplyOpenCredentials(CStringArray &s, DWORD &crc, CFaceInfo &Info);
	BOOL	ApplyOpenCustom(CStringArray &s, CJsonConfig *pCustom);
	BOOL	ApplyOpenClients(CStringArray &s, DWORD &crc, CFaceInfo &Info, CIpAddr const &LocMask);
	BOOL	ApplyOpenRoutes(CStringArray &s, CJsonConfig *pRoutes, bool fPush);
	BOOL    ApplyPeerCheck(CStringArray *s, CFaceInfo &Info, PCSTR pPeer, bool fEnable);
	BOOL    IsValidGreKey(PCSTR pKey);

	// IpSec Configuration
	CString GetPhase1(UINT e, UINT a, BOOL p, UINT d);
	CString GetPhase1Enc(UINT e);
	CString GetPhase1Auth(UINT a);
	CString GetPhase1Dh(UINT d);
	CString GetEsp(UINT e, UINT a);
	CString GetEspEnc(UINT e);
	CString GetEspAuth(UINT a);
	CString GetAh(UINT a);

	// Certificate Helpers
	BOOL SaveTrustedRoots(DWORD &crc, PCSTR pSave, PCSTR pName, PCSTR pCert, UINT uCert);
	BOOL SaveIdentityCert(DWORD &crc, PCSTR pSave, PCSTR pName, PCSTR pCert, PCSTR pPriv, PCSTR pPass, UINT uCert);
	BOOL SaveTrustedCert(DWORD &crc, PCSTR pSave, PCSTR pName, PCSTR pCert, UINT uCert);
	void SaveCertFile(DWORD &crc, PCSTR pSave, PCSTR pName, PCSTR pType, CByteArray const &Data);
	void SavePassFile(DWORD &crc, PCSTR pSave, PCSTR pName, PCSTR pType, CString const &Data);
	BOOL DecryptKeyFile(PCSTR pSave, PCSTR pName, PCSTR pType, PCSTR pPass);

	// MAC Filter Control
	BOOL ApplyMacFilter(void);
	BOOL ApplyMacFilter(CStringArray *s, CFaceInfo &Info, bool arp);

	// Firewall Control
	BOOL ApplyFirewall(CJsonConfig *pFirewall);
	BOOL ApplyFirewallLists(CJsonConfig *pLists);
	BOOL ApplyFirewallWhiteLists(CJsonConfig *pWhite);
	BOOL ApplyFirewallBlackList(CJsonConfig *pBlack);
	BOOL ApplyFirewallFilter(CJsonConfig *pFilter);
	BOOL ApplyFirewallCustom(CJsonConfig *pCustom);
	BOOL ApplyFirewallInbound(CJsonConfig *pIn);
	BOOL ApplyFirewallInboundDefault(void);
	BOOL ApplyFirewallInboundPreDefined(CJsonConfig *pPre);
	BOOL ApplyFirewallInboundCustom(CJsonConfig *pCustom);
	BOOL ApplyFirewallForward(CJsonConfig *pForward);
	BOOL ApplyFirewallNat(CJsonConfig *pNat);
	BOOL ApplyFirewallNatHost(CJsonConfig *pHost);
	BOOL ApplyFirewallNatDmz(CJsonConfig *pDmz);
	BOOL ApplyIptRules(CStringArray *s, CFaceInfo &Info, CString const &Bind, UINT mark, BOOL gate);
	BOOL ApplyUntrustedRules(CStringArray *s, CFaceInfo &Info);
	BOOL ApplyTrafficRules(CStringArray *s, CFaceInfo &Info);
	BOOL ApplyFinalRules(CStringArray *s, CFaceInfo &Info);
	BOOL ApplyFinalRules(void);
	BOOL AddSubTable(CStringArray *s, PCSTR pTable, PCSTR pChain, PCSTR pFace);
	UINT AddRule(CStringArray &List, CString Rule, PCSTR pFace, UINT uList);
	BOOL AddAccept(CStringArray &List, CString Rule, UINT uList);
	BOOL CanAcceptInput(CJsonConfig *pFace);
	BOOL CanForwardData(CJsonConfig *pFace);
	BOOL FindSharingProviders(CJsonConfig *pFaces);

	// Rule Generation
	CString GetRule(CString Key);

	// Script Helpers
	BOOL SaveFaceScripts(PCSTR pName, CStringArray const *s);
	BOOL SaveScript(PCSTR pCat, PCSTR pName, CStringArray const &script);
	BOOL SaveConfig(PCSTR pCat, PCSTR pName, CStringArray const &script);
	BOOL SaveFile(PCSTR pCat, PCSTR pName, BOOL fExec, CStringArray const &script);
	void AppendConfig(CStringArray &s, PCSTR pText, ...);
	void AppendConfig(CStringArray *s, PCSTR pText, ...);
	void AppendScript(CStringArray *s, UINT uFile, PCSTR pText, ...);

	// Other Services
	BOOL ApplyServices(CJsonConfig *pResolver);
	BOOL ApplyServiceDhcpRelay(CJsonConfig *pDhcp);
	BOOL ApplyServiceDhcpServer(CJsonConfig *pDhcp);
	BOOL SetPrivateRoutes(UINT uFace);
	UINT HasPrivateRoutes(UINT uFace);
	BOOL ApplyPrivateRoutes(CStringArray *s, CFaceInfo &Info, UINT face, BOOL asym);
	BOOL ApplyRouting(CJsonConfig *pRouting);
	BOOL ApplyRoutes(CStringArray &s, UINT uFace, UINT uMetric);
	BOOL ApplyRoutes(CStringArray &s, CJsonConfig *pTable, UINT uFace, UINT uMetric);
	BOOL ApplyRoute(CStringArray &s, CJsonConfig *pRoute, PCSTR pName, UINT uMetric, PCSTR pTable);
	BOOL ApplyResolver(CJsonConfig *pResolver);
	BOOL ApplyCertManager(CJsonConfig *pConfig);
	BOOL ApplyIdent(CJsonConfig *pIdent);
	BOOL ApplyZoneSync(CJsonConfig *pZoneSync);
	BOOL ApplyNtpSync(CJsonConfig *pNtpSync);
	BOOL ApplyCellSync(CJsonConfig *pCellSync);
	BOOL ApplyGpsSync(CJsonConfig *pGpsSync);
	BOOL ApplySmtpClient(CJsonConfig *pSmtpClient);
	BOOL ApplySmsClient(CJsonConfig *pSmsClient);
	BOOL ApplyLocation(CJsonConfig *pLocation);
	BOOL ApplyFtpServer(CJsonConfig *pFtpServer);
	BOOL ApplySvmClient(CJsonConfig *pSvmClient);
	BOOL ApplyIoMonitor(CJsonConfig *pIoMonitor);
	BOOL ApplyTlsRelay(CJsonConfig *pConfig, char cMode);
	BOOL ApplyTlsServer(CJsonConfig *pServer);
	BOOL ApplyTlsClient(CJsonConfig *pClient);
	BOOL ApplySysLog(CJsonConfig *pClient);
	BOOL ApplySnmp(CJsonConfig *pAgent);

	// Status Building
	BOOL AppendFile(CString &Info, PCSTR pHead, PCSTR pName);
	BOOL AppendCommand(CString &Info, PCSTR pHead, PCSTR pCmd, PCSTR pArgs);
	int  RunCommand(PCSTR pCmd, PCSTR pArgs, PCSTR pFile);

	// Address Formatting
	CString GetFullAddr(CIpAddr const &Addr, CIpAddr const &Mask);
	CString GetFullNet(CIpAddr const &Addr, CIpAddr const &Mask);

	// Implementation
	BOOL  ClearConfigData(void);
	BOOL  AddDependency(PCSTR pParent, PCSTR pChild);
	BOOL  WriteDependencies(void);
	BOOL  CheckCRC(DWORD &in, CJsonConfig * &pJson);
	DWORD GetCRC(CStringArray const &s, DWORD crc);
	void  WaitForStartup(void);
};

// End of File

#endif
