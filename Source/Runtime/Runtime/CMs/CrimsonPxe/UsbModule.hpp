
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_UsbModule_HPP

#define INCLUDE_UsbModule_HPP

//////////////////////////////////////////////////////////////////////////
//
// USB Module Information Object
//

class CUsbModule : public IUsbModule
{
public:
	// Constructor
	CUsbModule(DWORD dwSlot);

	// Destructor
	~CUsbModule(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IUsbModule
	DWORD METHOD GetSlot(void);

protected:
	// Data Members
	ULONG m_uRefs;
	DWORD m_dwSlot;
};

// End of File

#endif
