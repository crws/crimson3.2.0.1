
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SvmClient_HPP

#define	INCLUDE_SvmClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// SixView Manager Client
//

class CSvmClient : public IClientProcess
{
public:
	// Constructor
	CSvmClient(CJsonConfig *pJson);

	// Destructor
	~CSvmClient(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Data Members
	ULONG		     m_uRefs;
	UINT		     m_uMode;
	CString		     m_Server1;
	CString		     m_Server2;
	UINT		     m_uPeriod;
	UINT		     m_uConfigMode;
	CString		     m_ConfigServer1;
	CString		     m_ConfigServer2;
	UINT		     m_uConfigPeriod;
	CString		     m_Device;
	UINT		     m_uRetry;
	UINT		     m_uPort;
	UINT		     m_uFace;
	UINT		     m_uTls;
	UINT		     m_uCa;
	CByteArray	     m_CaCert;
	UINT		     m_uCheck;
	CIpAddr		     m_Sip1;
	CIpAddr		     m_Sip2;
	CIpAddr		     m_From;
	bool		     m_fOkay;
	IPlatform          * m_pPlatform;
	INetApplicator     * m_pNetApp;
	INetUtilities      * m_pNetUtils;
	ICrimsonPxe        * m_pPxe;
	IDnsResolver       * m_pResolver;
	ISocket            * m_pSock;
	CLocationSourceInfo  m_Loc;
	DWORD		     m_CellMx[2];
	UINT64		     m_CellRx[2];
	UINT64		     m_CellTx[2];
	UINT64		     m_PendRx[2];
	UINT64		     m_PendTx[2];

	// Implementation
	void ApplyConfig(CJsonConfig *pJson);
	bool ReadOverride(void);
	bool SaveOverride(void);
	void WaitForUpdate(void);
	bool LoadCerts(CHttpClientManager *pManager);
	bool LoadOptions(CHttpClientConnectionOptions &Opts);
	bool PerformCheckin(CHttpClientConnection *pConnect, UINT uServer, bool fInit);
	bool ProcessOption(CHttpClientConnection *pConnect, CString Name, CString Data);
	bool DownloadConfig(CHttpClientConnection *pConnect, CString Line);
	bool SendFileAck(CHttpClientConnection *pConnect, CString Id, bool fOkay);
	bool FindNames(CString &Temp, CString &Name, CString const &File);
	bool FindJsonName(CString &Name, CString const &Temp);
	bool CheckType(CString const &Text, char cTag);

	// Check-In Strings
	bool AddSerialNumber(CString &Body);
	bool AddBaseInfo(CString &Body);
	bool AddNicStatus(CString &Body, UINT uNic);
	bool AddCellStatus(CString &Body, UINT uServer);
	bool AddGpsLocation(CString &Body);
	bool AddHeartbeat(CString &Body, bool fInit);
	UINT ToBars(UINT uSignal);
	UINT ToUptime(DWORD ctime);

	// Transaction Handler
	bool Transact(CHttpClientConnection *pConnect, CHttpClientRequest &Req, CString Body);
};

// End of File

#endif
