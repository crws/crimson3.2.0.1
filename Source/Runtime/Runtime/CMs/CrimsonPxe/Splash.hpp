
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Splash_HPP

#define INCLUDE_Splash_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <c3.2/g3ctrl.hpp>

//////////////////////////////////////////////////////////////////////////
//
// PXE Splash Screen
//

class CSplash : public IClientProcess, public INotify, public IStyle
{
public:
	// Constructor
	CSplash(void);

	// Destructor
	~CSplash(void);

	// Operations
	void SetEventSink(IEventSink *pSink);
	void ShowStatus(PCTXT pText);
	void SetIpList(CStringArray const &IpList);
	void SetmDnsNames(CStringArray const &Names);
	void EnableUI(UINT uLevel);
	void ClearPause(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

	// INotify
	void OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData);

	// IStyle
	void GetColor(UINT uStyle, UINT uCode, COLOR      & Color);
	void GetMetric(UINT uStyle, UINT uCode, int        & nSize);
	void GetFont(UINT uStyle, UINT uCode, IGdiFont * & pFont);

protected:
	// Data Members
	ULONG	       m_uRefs;
	UINT	       m_uLevel;
	IEventSink   * m_pSink;
	IMutex	     * m_pLock;
	IDisplay     * m_pDisp;
	IGdi         * m_pGdi;
	ITouchScreen * m_pTouch;
	IInputQueue  * m_pInput;
	ITouchMap    * m_pMap;
	UINT	       m_uFont;
	int	       m_cx;
	int	       m_cy;
	int	       m_tx;
	int	       m_ty;
	int	       m_hy;
	CString	       m_Text;
	CStringArray   m_IpList;
	CStringArray   m_Names;
	IButton      * m_pButton;
	BOOL	       m_fPause;
	HTHREAD        m_hThread;

	// Implementation
	void Redraw(void);
};

// End of File

#endif
