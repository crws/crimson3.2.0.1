
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_FileConfigStorage_HPP

#define INCLUDE_FileConfigStorage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BaseConfigStorage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Filing System Config Storage Object
//

class CFileConfigStorage : public CBaseConfigStorage
{
public:
	// Constructor
	CFileConfigStorage(CString const &Root);
};

// End of File

#endif

