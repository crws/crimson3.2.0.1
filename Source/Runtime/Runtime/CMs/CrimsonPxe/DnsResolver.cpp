
#include "intern.hpp"

#include "DnsResolver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

#include "DnsHeader.hpp"

#include "DnsCache.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNS Resolver
//

// Constructor

CDnsResolver::CDnsResolver(CJsonConfig *pJson, UINT uFaces) : CPxeIpClient(uFaces)
{
	m_pCache  = New CDnsCache;

	m_pMutex  = Create_Mutex();

	m_wID     = WORD(rand());

	m_bOption = 6;

	ApplyConfig(pJson);

	FindServerList();
}

// Destructor

CDnsResolver::~CDnsResolver(void)
{
	delete m_pCache;

	m_pMutex->Release();
}

// IUnknown

HRESULT CDnsResolver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDnsResolver);

	StdQueryInterface(IDnsResolver);

	return E_NOINTERFACE;
}

ULONG CDnsResolver::AddRef(void)
{
	StdAddRef();
}

ULONG CDnsResolver::Release(void)
{
	StdRelease();
}

// IDnsResolver

CIpAddr CDnsResolver::Resolve(PCTXT pName)
{
	if( IsDotted(pName) ) {

		return CIpAddr(pName);
	}
	else {
		if( m_uMode ) {

			CAutoLock Lock(m_pCache->GetLock());

			m_pCache->Poll();

			CIpList *pList = NULL;

			if( !m_pCache->Find(pName, pList) ) {

				Lock.Free();

				CIpList List;

				if( DoResolve(pName, List) ) {

					UINT n = rand() % List.GetCount();

					return List[n];
				}

				return CIpAddr::m_Empty;
			}

			UINT n = rand() % pList->GetCount();

			return pList->GetAt(n);
		}

		return CIpAddr::m_Empty;
	}
}

BOOL CDnsResolver::Resolve(CArray <CIpAddr> &List, PCTXT pName)
{
	if( IsDotted(pName) ) {

		List.Empty();

		List.Append(CIpAddr(pName));

		return TRUE;
	}
	else {
		if( m_uMode ) {

			CAutoLock Lock(m_pCache->GetLock());

			m_pCache->Poll();

			CIpList *pList = NULL;

			if( !m_pCache->Find(pName, pList) ) {

				Lock.Free();

				if( DoResolve(pName, List) ) {

					return TRUE;
				}

				return FALSE;
			}

			List = *pList;

			return TRUE;
		}

		return FALSE;
	}
}

// Implementation

void CDnsResolver::ApplyConfig(CJsonConfig *pJson)
{
	if( pJson ) {

		m_uMode = pJson->GetValueAsUInt("mode", 1, 0, 2);

		m_ConfigIps.Append(pJson->GetValueAsIp("dns1", CIpAddr::m_Empty));

		m_ConfigIps.Append(pJson->GetValueAsIp("dns2", CIpAddr::m_Empty));

		m_Suffix = pJson->GetValue("suffix", "");

		CJsonConfig *pHosts = pJson->GetChild("hosts");

		if( pHosts ) {

			for( UINT n = 0;; n++ ) {

				CJsonConfig *pHost = pHosts->GetChild(n);

				if( pHost ) {

					CString Name = pHost->GetValue("name", "");

					CIpAddr Addr = pHost->GetValueAsIp("ip", CIpAddr::m_Empty);

					if( !Name.IsEmpty() && !Addr.IsEmpty() ) {

						CIpList List(Addr);

						m_pCache->Add(Name, List, NOTHING);
					}

					continue;
				}

				break;
			}
		}
	}

	m_DefaultIps.Append(CIpAddr(8, 8, 8, 8));

	m_DefaultIps.Append(CIpAddr(8, 8, 4, 4));
}

BOOL CDnsResolver::DoResolve(PCTXT pHost, CIpList &List)
{
	CAutoLock Lock(m_pMutex);

	FindServerList();

	UINT nq = m_Suffix.IsEmpty() ? 1 : 2;

	for( UINT t = 0; t < 2; t++ ) {

		for( UINT s = 0; s < m_ServerIps.GetCount(); s++ ) {

			AfxNewAutoObject(pSock, "sock-udp", ISocket);

			if( pSock ) {

				IPREF Ip = m_ServerIps[m_uActive];

				pSock->Connect(Ip, 53);

				for( UINT q = 0; q < nq; q++ ) {

					CString Name(pHost);

					if( q ) {

						if( m_Suffix[0] != '.' ) {

							Name += '.';
						}

						Name += m_Suffix;
					}

					if( SendQuery(pSock, Name) ) {

						UINT  uTime = 0;

						DWORD dwTTL = NOTHING;

						if( !m_pUtils || m_pUtils->IsFastAddress(Ip) ) {

							uTime = 2000;
						}
						else
							uTime = 10000;

						if( RecvReply(pSock, List, dwTTL, uTime) ) {

							if( dwTTL > 0 && dwTTL < NOTHING ) {

								m_pCache->Add(pHost, List, dwTTL);
							}

							return TRUE;
						}
					}
				}

				m_uActive = (m_uActive + 1) % m_ServerIps.GetCount();
			}
		}

		if( m_pUtils ) {

			// TODO -- This ought to force the modem to connect, but
			// we don't know enough about the IP routes that will in
			// fact make this happen, so we just take a guess...

			CIpAddr Force(1, 1, 1, 1);

			m_pUtils->Ping(Force, 100);
		}
	}

	return FALSE;
}

BOOL CDnsResolver::SendQuery(ISocket *pSock, PCTXT pHost)
{
	CAutoBuffer pBuff(DNS_MAX_MSG);

	if( pBuff ) {

		CDnsHeader *pH = BuffAddTail(pBuff, CDnsHeader);

		pH->Init();

		pH->m_ID      = ++m_wID;
		pH->m_QR      = FALSE;
		pH->m_Opcode  = opSTD;
		pH->m_TC      = FALSE;
		pH->m_RD      = TRUE;
		pH->m_QDCount = 1;

		pH->HostToNet();

		CDnsQuestion *pQ = (CDnsQuestion *) pBuff->AddTail(0);

		if( pQ->FindSize(pHost) < pBuff->GetTailSpace() ) {

			pQ->Init(pHost, typeA, classIN);

			pQ->HostToNet();

			pBuff->AddTail(pQ->GetSize());

			if( FALSE ) {

				AfxTrace("DNS Send:\n");

				AfxDump(pBuff->GetData(), pBuff->GetSize());
			}

			if( pSock->Send(pBuff) == S_OK ) {

				pBuff.TakeOver();

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CDnsResolver::RecvReply(ISocket *pSock, CIpList &List, DWORD &dwTTL, UINT uTime)
{
	SetTimer(uTime);

	while( GetTimer() ) {

		CAutoBuffer pBuff;

		if( pSock->Recv(pBuff) == S_OK ) {

			if( FALSE ) {

				AfxTrace("DNS Recv:\n");

				AfxDump(pBuff->GetData(), pBuff->GetSize());
			}

			if( pBuff->GetSize() >= sizeof(CDnsHeader) ) {

				CDnsHeader *pHead = BuffStripHead(pBuff, CDnsHeader);

				pHead->NetToHost();

				if( pHead->m_ID == m_wID ) {

					if( pHead->m_QR && pHead->m_Opcode == rcOK ) {

						if( pHead->m_QDCount ) {

							CDnsQuestion *pQuery = (CDnsQuestion *) pBuff->GetData();

							pQuery->NetToHost();

							pBuff->StripHead(pQuery->GetSize());
						}

						for( UINT n = 0; n < pHead->m_ANCount; n++ ) {

							CDnsRecord *pRecord = (CDnsRecord *) pBuff->GetData();

							pRecord->NetToHost();

							if( pRecord->GetType() == typeA ) {

								if( pRecord->GetClass() == classIN ) {

									List.Append(HostToMotor(pRecord->GetAddr()));

									MakeMin(dwTTL, pRecord->GetTTL());
								}
							}

							pBuff->StripHead(pRecord->GetSize());
						}

						if( List.GetCount() ) {

							return TRUE;
						}
					}

					return FALSE;
				}
			}
		}

		Sleep(20);
	}

	return FALSE;
}

BOOL CDnsResolver::IsDotted(PCTXT pText)
{
	UINT d = 0;

	for( UINT n = 0; pText[n]; n++ ) {

		if( pText[n] == '.' ) {

			d++;
		}
		else {
			if( !isdigit(pText[n]) ) {

				return FALSE;
			}
		}
	}

	return d == 3;
}

// End of File
