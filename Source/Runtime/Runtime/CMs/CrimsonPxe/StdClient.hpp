
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_StdClient_HPP

#define	INCLUDE_StdClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "StdEndpoint.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Standard RFC Client Configuration
//

struct CStdClientConfig
{
	UINT		m_uCa;
	CByteArray	m_CaCert;
	UINT		m_uCheck;
};

/////////////////////////////////////////////////////////////////////////
//
// Standard RFC Client
//

class CStdClient : public CStdEndpoint
{
	public:
		// Constructor
		CStdClient(void);

		// Destructor
		~CStdClient(void);

		// Operations
		void SetConfig(CStdClientConfig const *pConfig);

	protected:
		// Data Members
		CStdClientConfig const * m_pConfig;
		UINT			 m_uCheck;
		CString		         m_Host;
		IDnsResolver           * m_pResolver;
		INetUtilities          * m_pUtils;
		UINT	                 m_uOption;
		UINT                     m_uReplyCode;
		CString                  m_ReplyText;
		CStringArray             m_ReplyExtra;
		
		// TLS Support
		ITlsClientContext * m_pTls;

		// Transport
		BOOL RecvReply(UINT uTimeout);
		BOOL RecvReply(void);

		// Socket Management
		BOOL OpenCmdSocket(CString const &Host, WORD wPort, BOOL fLarge, BOOL fTls);
		BOOL SwitchCmdSocket(void);
		BOOL CreateTlsContext(void);
	};

// End of File

#endif
