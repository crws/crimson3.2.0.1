
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CertManager_HPP

#define	INCLUDE_CertManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// Certificate Manager
//

class CCertManager : public ICertManager
{
public:
	// Constructor
	CCertManager(CJsonConfig *pJson);

	// Destructor
	~CCertManager(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ICertManager
	BOOL METHOD GetTrustedRoots(CByteArray &Certs);
	BOOL METHOD GetTrustedCert(UINT uSlot, CByteArray &Cert);
	BOOL METHOD GetIdentityCert(UINT uSlot, CByteArray &Cert, CByteArray &Priv, CString &Pass);

protected:
	// Cert Structure
	struct CCert
	{
		CByteArray m_Cert;
		CByteArray m_Priv;
		CString    m_Pass;
	};

	// Data Members
	ULONG	         m_uRefs;
	CArray<CCert>    m_Certs;
	CMap<UINT, UINT> m_Roots;
	CMap<UINT, UINT> m_Server;
	CMap<UINT, UINT> m_Client;

	// Implementation
	void ApplyConfig(CJsonConfig *pJson);
	void ApplyConfig(CJsonConfig *pJson, CMap<UINT, UINT> &Map);
};

// End of File

#endif
