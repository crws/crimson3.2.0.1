
#include "intern.hpp"

#include "SvmClient.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SixView Manager Client
//

// URL Encoding

#define UrlEncode(x) PCTXT(CHttpUrlEncoding::Encode(x))

// Version

#define	SVM_VERSION "1.62"

// Constructor

CSvmClient::CSvmClient(CJsonConfig *pJson)
{
	m_pPlatform  = NULL;

	m_pNetApp    = NULL;

	m_pNetUtils  = NULL;

	m_pPxe       = NULL;

	m_pResolver  = NULL;

	m_pSock      = NULL;

	m_fOkay      = true;

	m_Loc.m_uFix = 0;

	m_Loc.m_uSeq = 0;

	m_CellMx[0]  = 0;

	m_CellMx[1]  = 0;

	AfxGetObject("platform", 0, IPlatform, m_pPlatform);

	AfxGetObject("c3.net-applicator", 0, INetApplicator, m_pNetApp);

	AfxGetObject("ip", 0, INetUtilities, m_pNetUtils);

	AfxGetObject("pxe", 0, ICrimsonPxe, m_pPxe);

	AfxGetObject("net.dns", 0, IDnsResolver, m_pResolver);

	ApplyConfig(pJson);

	StdSetRef();
}

// Destructor

CSvmClient::~CSvmClient(void)
{
	AfxRelease(m_pPlatform);

	AfxRelease(m_pNetApp);

	AfxRelease(m_pNetUtils);

	AfxRelease(m_pPxe);

	AfxRelease(m_pResolver);
}

// IUnknown

HRESULT CSvmClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CSvmClient::AddRef(void)
{
	StdAddRef();
}

ULONG CSvmClient::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CSvmClient::TaskInit(UINT uTask)
{
	SetThreadName("SvmClient");

	if( m_uMode ) {

		return TRUE;
	}

	Release();

	return FALSE;
}

INT CSvmClient::TaskExec(UINT uTask)
{
	ReadOverride();

	CAutoPointer<CHttpClientManager> pManager(New CHttpClientManager);

	if( pManager->Open() ) {

		if( LoadCerts(pManager) ) {

			AfxGetAutoObject(pDns, "net.dns", 0, IDnsResolver);

			bool fInit = true;

			for( ;;) {

				CHttpClientConnectionOptions Opts;

				if( LoadOptions(Opts) ) {

					if( true ) {

						m_fOkay = true;

						CAutoPointer<CHttpClientConnection> pConnect1(pManager->CreateConnection(Opts));

						CAutoPointer<CHttpClientConnection> pConnect2(pManager->CreateConnection(Opts));

						if( pDns ) {

							m_Sip1 = (m_uMode != 2) ? pDns->Resolve(m_Server1) : IP_EMPTY;

							m_Sip2 = (m_uMode != 1) ? pDns->Resolve(m_Server2) : IP_EMPTY;
						}

						if( m_uMode != 2 ) {

							AfxTrace("svm: checking in with %s\n", PCSTR(m_Server1));

							pConnect1->SetServer(m_Server1, "", "");

							if( !PerformCheckin(pConnect1, 0, fInit) ) {

								AfxTrace("svm: failed\n");

								m_fOkay = false;
							}
							else {

								AfxTrace("svm: okay\n");
							}
						}

						if( m_uMode != 1 && !(m_uMode == 3 && m_fOkay) ) {

							AfxTrace("svm: checking in with %s\n", PCSTR(m_Server2));

							pConnect2->SetServer(m_Server2, "", "");

							if( !PerformCheckin(pConnect2, 1, fInit) ) {

								AfxTrace("svm: failed\n");

								m_fOkay = false;
							}
							else {
								AfxTrace("svm: okay\n");
							}
						}

						if( m_fOkay ) {

							fInit = false;
						}
					}

					WaitForUpdate();
				}
				else {
					Sleep(1000);
				}
			}
		}
	}

	return 0;
}

void CSvmClient::TaskStop(UINT uTask)
{
}

void CSvmClient::TaskTerm(UINT uTask)
{
	AfxRelease(m_pSock);

	Release();
}

// Implementation

void CSvmClient::ApplyConfig(CJsonConfig *pJson)
{
	CString Name;

	m_pNetApp->GetUnitName(Name);

	m_uMode   = pJson->GetValueAsUInt("mode", 0, 0, 4);
	m_Server1 = pJson->GetValue("server1", "");
	m_Server2 = pJson->GetValue("server2", "");
	m_Device  = pJson->GetValue("device", Name);
	m_uPeriod = pJson->GetValueAsUInt("update", 200, 1, 86400);
	m_uRetry  = pJson->GetValueAsUInt("retry", 30, 1, 86400);
	m_uPort   = pJson->GetValueAsUInt("port", 18081, 1, 65535);
	m_uFace   = pJson->GetValueAsUInt("source", 0, 0, 65535);
	m_uTls    = pJson->GetValueAsUInt("cmode", 1, 0, 1);
	m_uCa     = pJson->GetValueAsUInt("casrc", 0, 0, 65535);
	m_uCheck  = pJson->GetValueAsUInt("cacheck", 0, 0, 2);

	m_uConfigMode   = m_uMode;
	m_ConfigServer1 = m_Server1;
	m_ConfigServer2 = m_Server2;
	m_uConfigPeriod = m_uPeriod;

	pJson->GetValueAsBlob("cacert", m_CaCert);
}

bool CSvmClient::ReadOverride(void)
{
	CAutoFile File("/svm/config.json", "r");

	if( File ) {

		UINT uSize = File.GetSize();

		if( uSize < 4 * 1024 ) {

			CString Text(' ', uSize);

			if( File.Read(PSTR(PCSTR(Text)), uSize) == uSize ) {

				CJsonData Json;

				if( Json.Parse(Text) ) {

					m_uMode   = atoi(Json.GetValue("mode", CPrintf("%u", m_uMode)));

					m_uPeriod = atoi(Json.GetValue("update", CPrintf("%u", m_uPeriod)));

					m_Server1 = Json.GetValue("server1", m_Server1);

					m_Server2 = Json.GetValue("server2", m_Server1);

					return true;
				}
			}
		}
	}

	return false;
}

bool CSvmClient::SaveOverride(void)
{
	mkdir("/svm", 0755);

	CAutoFile File("/svm/config.json", "w");

	if( File ) {

		CString Text = "{\n";

		if( m_uMode != m_uConfigMode ) {

			Text.AppendPrintf("\"mode\"=%u,\n", m_uMode);
		}

		if( m_uPeriod != m_uConfigPeriod ) {

			Text.AppendPrintf("\"update\"=%u,\n", m_uPeriod);
		}

		if( m_Server1 != m_ConfigServer1 ) {

			Text.AppendPrintf("\"server1\"=\"%s\",\n", PCSTR(m_Server1));
		}

		if( m_Server2 != m_ConfigServer2 ) {

			Text.AppendPrintf("\"server2\"=\"%s\",\n", PCSTR(m_Server2));
		}

		Text += "\"okay\"=true\n}\n";

		File.Write(Text);

		return true;
	}

	return false;
}

void CSvmClient::WaitForUpdate(void)
{
	UINT uWait = m_fOkay ? m_uPeriod : m_uRetry;

	for( UINT uTime = uWait; uTime; uTime-- ) {

		for( SetTimer(60000); GetTimer(); ) {

			if( !m_pSock ) {

				AfxNewObject("sock-tcp", ISocket, m_pSock);

				m_pSock->Listen(m_From, 7785);
			}
			else {
				UINT Phase;

				m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					char b[256];

					UINT s = sizeof(b) - 1;

					if( m_pSock->Recv(PBYTE(b), s) == S_OK ) {

						b[s] = 0;

						if( !strcmp(b, m_pPlatform->GetSerial()) ) {

							AfxTrace("svm: force command received\n");

							strcpy(b, "MKay\r\n");

							s = strlen(b);

							m_pSock->Send(PBYTE(b), s);

							SetTimer(0);

							uTime = 1;
						}

						m_pSock->Close();
					}
				}

				if( Phase == PHASE_CLOSING || Phase == PHASE_ERROR ) {

					m_pSock->Release();

					m_pSock = NULL;
				}
			}

			Sleep(25);
		}

		if( true ) {

			AfxGetAutoObject(pLoc, "c3.location", 0, ILocationSource);

			if( pLoc ) {

				UINT uSeq = m_Loc.m_uSeq;

				if( pLoc->GetLocationData(m_Loc) ) {

					if( m_Loc.m_uSeq > uSeq ) {

						AfxTrace("svm: gps movement detected\n");

						uTime = 1;
					}
				}
			}
		}
	}
}

bool CSvmClient::LoadCerts(CHttpClientManager *pManager)
{
	if( m_uCa ) {

		if( m_uCa >= 2 ) {

			m_CaCert.Empty();

			AfxGetAutoObject(pCertMan, "c3.certman", 0, ICertManager);

			if( m_uCa >= 100 ) {

				if( !pCertMan->GetTrustedCert(m_uCa, m_CaCert) ) {

					pCertMan->GetTrustedRoots(m_CaCert);
				}
			}
			else {
				pCertMan->GetTrustedRoots(m_CaCert);
			}
		}

		if( !m_CaCert.IsEmpty() ) {

			pManager->LoadTrustedRoots(m_CaCert.data(), m_CaCert.size());
		}

		return true;
	}

	return true;
}

bool CSvmClient::LoadOptions(CHttpClientConnectionOptions &Opts)
{
	Opts.m_fTls   = m_uTls ? TRUE : FALSE;

	Opts.m_uPort  = m_uPort;

	Opts.m_uCheck = tlsCheckNone;

	if( m_uCa >= 1 ) {

		Opts.m_uCheck = 1 + m_uCheck;
	}

	if( m_uFace ) {

		AfxGetAutoObject(pUtils, "ip", 0, INetUtilities);

		if( pUtils ) {

			UINT i = pUtils->FindInterface(m_uFace);

			if( i < NOTHING ) {

				if( pUtils->IsInterfaceUp(i) ) {

					pUtils->GetInterfaceAddr(i, m_From);

					if( !m_From.IsEmpty() ) {

						Opts.m_Source = m_From;

						return true;
					}
				}
			}
		}

		return false;
	}

	return true;
}

bool CSvmClient::PerformCheckin(CHttpClientConnection *pConnect, UINT uServer, bool fInit)
{
	CHttpClientRequest Req;

	CString Body;

	if( AddBaseInfo(Body) ) {

		AddNicStatus(Body, 0);

		AddNicStatus(Body, 1);

		AddCellStatus(Body, uServer);

		AddGpsLocation(Body);

		AddHeartbeat(Body, fInit);

		if( Transact(pConnect, Req, Body) ) {

			bool fSave = false;

			bool fOkay = true;

			Body = PCTXT(Req.GetRemoteBody());

			while( !Body.IsEmpty() ) {

				CString Line = Body.StripToken('\n');

				CString Verb = Line.StripToken(':');

				if( false ) {

					AfxTrace("svm: verb=[%s] line=[%s]\n", PCSTR(Verb), PCSTR(Line));
				}

				if( Verb == "ERROR" ) {

					fOkay = false;

					Line.TrimLeft();

					break;
				}

				if( Verb == "OPTION" ) {

					Line.TrimLeft();

					CString Name = Line.StripToken('=');

					if( ProcessOption(pConnect, Name, Line) ) {

						fSave = true;
					}
				}

				if( Verb == "DLFILE" ) {

					Line.TrimLeft();

					if( DownloadConfig(pConnect, Line) ) {

						// Delay to allow traffic counters to settle

						Sleep(2000);

						// Once last send to record traffic information

						m_CellRx[uServer] = m_PendRx[uServer];

						m_CellTx[uServer] = m_PendTx[uServer];

						PerformCheckin(pConnect, uServer, false);

						m_pPxe->RestartSystem(0, 55);
					}
				}
			}

			if( fOkay ) {

				m_CellRx[uServer] = m_PendRx[uServer];

				m_CellTx[uServer] = m_PendTx[uServer];
			}

			if( fSave ) {

				SaveOverride();
			}

			return true;
		}
	}

	return false;
}

bool CSvmClient::ProcessOption(CHttpClientConnection *pConnect, CString Name, CString Data)
{
	if( Name == "interval" ) {

		UINT data = atoi(PCTXT(Data));

		m_uPeriod = (data >= 1 && data <= 86400) ? data : m_uConfigPeriod;

		return true;
	}

	if( Name == "dualmode" ) {

		UINT mode = 0;

		if( Data == "none" ) {

			mode = 1;
		}

		if( Data == "secondaryonly" ) {

			mode = 2;
		}

		if( Data == "error" ) {

			mode = 3;
		}

		if( Data == "both" ) {

			mode = 4;
		}

		m_uMode = mode ? mode : m_uConfigMode;

		return true;
	}

	if( Name.StartsWith("sip") ) {

		Data.TrimBoth();

		if( Data == "0.0.0.0" ) {

			Data.Empty();
		}
	}

	if( Name == "sip" ) {

		m_Server1 = Data.IsEmpty() ? m_ConfigServer1 : Data;

		return true;
	}

	if( Name == "sip2" ) {

		m_Server2 = Data.IsEmpty() ? m_ConfigServer2 : Data;

		return true;
	}

	return false;
}

bool CSvmClient::DownloadConfig(CHttpClientConnection *pConnect, CString Line)
{
	CHttpClientRequest Req;

	Req.SetPath(Line);

	CString File  = Line.StripToken(':');
	CString Size  = Line.StripToken(':');
	CString Type  = Line.StripToken(':');
	CString Hash  = Line.StripToken(':');
	CString Id    = Line.StripToken(':');

	UINT uSize = atoi(PCTXT(Size));

	if( uSize > 0 && uSize < 128 * 1024 * 1024 ) {

		mkdir("/update", 0755);

		CString Temp, Name;

		if( FindNames(Temp, Name, File) ) {

			AfxTrace("svm: downloading via %s\n", PCSTR(Temp));

			if( !Name.IsEmpty() ) {

				if( CAutoFile(Name, "r") ) {

					unlink(Temp);

					rename(Name, Temp.TokenLast('/'));
				}
			}

			CAutoFile File(Temp, "r+", "w+");

			if( File ) {

				CHttpStreamWriteFileMd5 Stream(File.TakeOver());

				Req.SetReplyStream(&Stream);

				if( Req.Transact(pConnect, "GET", 200) || Req.Transact(pConnect, "GET", 200) ) {

					Req.SetReplyStream(NULL);

					CString Test;

					if( Stream.GetHash(Test) ) {

						if( Test == Hash ) {

							if( !Name.IsEmpty() || FindJsonName(Name, Temp) ) {

								unlink(Name);

								unlink(Name + ".d");

								rename(Temp, Name.TokenLast('/'));

								AfxTrace("svm: download completed to %s\n", PCSTR(Name));

								SendFileAck(pConnect, Id, true);

								return true;
							}
							else {
								AfxTrace("svm: file was not recognized\n");
							}
						}
						else {
							AfxTrace("svm: hash did not match\n");
						}
					}

					SendFileAck(pConnect, Id, false);

					return false;
				}

				Req.SetReplyStream(NULL);
			}
		}
	}

	// NOTE -- We can't report a failure on the update until we have
	// actually downloaded it so for some failures, we have to use a
	// null stream to grab and dump the file.

	CHttpStreamWriteNull Stream;

	Req.SetReplyStream(&Stream);

	Req.Transact(pConnect, "GET", 200);

	Req.SetReplyStream(NULL);

	SendFileAck(pConnect, Id, false);

	return false;
}

bool CSvmClient::SendFileAck(CHttpClientConnection *pConnect, CString Id, bool fOkay)
{
	CHttpClientRequest Req;

	CString Body;

	if( AddSerialNumber(Body) ) {

		Body.AppendPrintf("&file_id=%s", PCTXT(Id));

		Body.AppendPrintf("&file_status=%s", fOkay ? "SUCCESS" : "FAILED");

		return Transact(pConnect, Req, Body);
	}

	return false;
}

bool CSvmClient::FindNames(CString &Temp, CString &Name, CString const &File)
{
	CString Type = File.TokenLast('.');

	if( Type == "ci3" ) {

		Temp = "/update/image.tmp";

		Name = "/update/image.ci3";

		return true;
	}

	if( Type == "json" || Type == "zip" ) {

		Temp = "/update/svm-json.tmp";

		return true;
	}

	return false;
}

bool CSvmClient::FindJsonName(CString &Name, CString const &Temp)
{
	CAutoFile File(Temp, "r");

	if( File ) {

		UINT uSize = File.GetSize();

		if( uSize < 256 * 1024 ) {

			CString Text(' ', uSize);

			if( File.Read(PSTR(PCSTR(Text)), uSize) == uSize ) {

				if( CheckType(Text, 'h') ) {

					Name = "/update/hardware.json";

					return true;
				}

				if( CheckType(Text, 's') ) {

					Name = "/update/system.json";

					return true;
				}

				if( CheckType(Text, 'p') ) {

					Name = "/update/personality.json";

					return true;
				}

				if( CheckType(Text, 'u') ) {

					Name = "/update/users.json";

					return true;
				}
			}

			Name = "/update/devcon.json";

			return true;
		}
	}

	return false;
}

bool CSvmClient::CheckType(CString const &Text, char cTag)
{
	CString key = "\"atype\":";

	UINT    f1  = Text.Find(key);

	if( f1 < NOTHING ) {

		UINT f2 = Text.Find('"', f1 + key.GetLength());

		if( f2 < NOTHING ) {

			if( Text[f2+1] == cTag && Text[f2+2] == '"' ) {

				return true;
			}
		}
	}

	return false;
}

// Check-In Strings

bool CSvmClient::AddSerialNumber(CString &Body)
{
	// IMPLEMENTED:
	//
	// sn,serial number,"Unit's serial number, used to uniquely identify this unit in SVM when combined with it's platform"

	Body.Printf("sn=%s", m_pPlatform->GetSerial());

	return true;
}

bool CSvmClient::AddBaseInfo(CString &Body)
{
	AddSerialNumber(Body);

	// MISSING:
	//
	// bvers,build version,"If applicable, a build version for the firmware"
	// notes,notes,Free-form notes to be included in the checkin
	// smfc,smartmodem_fc,
	// techHdrColorCode,CELLMODEM_TECH_HDR_COLOR_CODE,
	// techHdrColorCode,CELLMODEM_TECH_HDR_COLOR_CODE,
	// techHdrDrcCover,CELLMODEM_TECH_HDR_DRC_COVER,
	// techHdrDrcCover,CELLMODEM_TECH_HDR_DRC_COVER,
	// techHdrDrcValue,CELLMODEM_TECH_HDR_DRC_VALUE,
	// techHdrDrcValue,CELLMODEM_TECH_HDR_DRC_VALUE,
	// techHdrPnOffset,CELLMODEM_TECH_HDR_PN_OFFSET,
	// techHdrPnOffset,CELLMODEM_TECH_HDR_PN_OFFSET,
	// techHdrSecId,CELLMODEM_TECH_HDR_SECTOR_ID,
	// techHdrSecId,CELLMODEM_TECH_HDR_SECTOR_ID,
	// techHdrSubnetMask,CELLMODEM_TECH_HDR_SUBNET_MASK,
	// techHdrSubnetMask,CELLMODEM_TECH_HDR_SUBNET_MASK,
	// uname,unit name,Human-readable name for the unit
	//
	// IMPLEMENTED:
	//
	// ver,version,"Version of the unit's firwmare"
	// ut,uptime,"Time since the unit last powered on"
	// mn,model number,"Model number of the unit"
	// cv,SVM client version,Version number for the SVM client doing the check-in
	// cpi,checkin interval,Minutes between SVM checkins
	// sip,SVM primary IP,IP address for the primary SVM server
	// sip2,SVM secondary IP,IP address for the secondary SVM server
	// hn,hostname,Hostname for the unit
	// plat,platform,
	// dualmode,dual server mode,"One of 'secondaryonly', 'none', 'both', or 'error' describing how to deal with primary/secondary SVM servers"

	CString Dual = "none";

	switch( m_uMode ) {

		case 2:
			Dual = "secondaryonly";
			break;

		case 3:
			Dual = "error";
			break;

		case 4:
			Dual = "both";
			break;
	}

	time_t ctime;

	m_pPxe->GetInitTime(ctime);

	AfxGetAutoObject(pPlatform, "platform", 0, IPlatform);

	CString Model = pPlatform->GetModel();

	Model.MakeUpper();

	Body.AppendPrintf("&ver=3.2.%4.4u.%u", C3_BUILD, C3_HOTFIX);	// Firmware Version
	Body.AppendPrintf("&ut=%u", ToUptime(ctime));			// Uptime
	Body.AppendPrintf("&mn=%s", UrlEncode(Model));			// Model Name
	Body.AppendPrintf("&cv=%s", SVM_VERSION);			// Client Version
	Body.AppendPrintf("&cpi=%u", m_uPeriod);			// Checkin Interval
	Body.AppendPrintf("&sip=%s", UrlEncode(m_Sip1.GetAsText()));	// Server IP 1
	Body.AppendPrintf("&sip2=%s", UrlEncode(m_Sip2.GetAsText()));	// Server IP 2
	Body.AppendPrintf("&hn=%s", UrlEncode(m_Device));		// Host Name
	Body.AppendPrintf("&plat=%s", UrlEncode("CI3"));		// Platform
	Body.AppendPrintf("&dualmode=%s", UrlEncode(Dual));		// Server Mode

	return true;
}

bool CSvmClient::AddNicStatus(CString &Body, UINT uNic)
{
	AfxGetAutoObject(pUtils, "ip", 0, INetUtilities);

	if( pUtils ) {

		UINT uFace = pUtils->FindInterface(CPrintf("eth%u", uNic));

		if( uFace < NOTHING ) {

			// MISSING:
			//
			// intInfo,Interfaces info,Lots of info from ifconfig
			//
			// IMPLEMENTED:
			//
			// intE0,eth0 status,NA, UP, or DOWN for the status of the eth0 interface
			// intE1,eth1 status,NA, UP, or DOWN for the status of the eth1 interface
			// ipE0,eth0 IP,IP address for the eth0 interface
			// ipE1,eth1 IP,IP address for the eth1 interface

			CString Status = "NA";

			CIpAddr Addr;

			pUtils->GetInterfaceStatus(uFace, Status);

			pUtils->GetInterfaceAddr(uFace, Addr);

			if( Addr.IsEmpty() ) {

				if( Status == "UP" ) {

					Status = "DOWN";
				}
			}

			Body.AppendPrintf("&intE%u=%s", uNic, PCSTR(Status));

			Body.AppendPrintf("&ipE%u=%s", uNic, PCSTR(Addr.GetAsText()));

			return true;
		}
	}

	return false;
}

bool CSvmClient::AddCellStatus(CString &Body, UINT uServer)
{
	AfxGetAutoObject(pInfo, "net.cell", 0, ICellStatus);

	if( pInfo ) {

		CCellStatusInfo Info;

		if( pInfo->GetCellStatus(Info) ) {

			// MISSING:
			//
			// chan,Current cell modem channel,Channel used by the unit's cellular modem
			// curr_cm_apn,current apn,
			// ecio,Current cell modem ECIO,ECIO used by the unit's cellular modem
			// mdn,Cell modem MDN,Unit's cell modem's MDN
			// plmn,Cell modem carrier PLMN,
			// prl,CELLMODEM_PRL_VERSION,
			// rsrp,CELLMODEM_LTE_RSRP,
			// rsrq,CELLMODEM_LTE_RSRQ,
			// wrx,wireless RX bytes,
			// wtx,wireless TX bytes,
			//
			// IMPLEMENTED:
			//
			// imsi,CELLMODEM_SIM_IMSI,
			// cmn,cellmodem name,
			// simid,CELLMODEM_SIM_ID,
			// esn,esn,
			// rssi,lightbar rssi,
			// wss, lightbar signal,
			// wut,wireless uptime,
			// stype,cell modem service type,Service type for the cellular modem

			CString Service = Info.m_Carrier + " " + Info.m_Service;

			m_PendRx[uServer] = Info.m_RxBytes;

			m_PendTx[uServer] = Info.m_TxBytes;

			if( m_CellMx[uServer] != Info.m_MxTime ) {

				m_CellMx[uServer] = Info.m_MxTime;

				m_CellRx[uServer] = m_PendRx[uServer];

				m_CellTx[uServer] = m_PendTx[uServer];
			}

			Body.AppendPrintf("&imsi=%s", UrlEncode(Info.m_Imei));
			Body.AppendPrintf("&cmn=%s", UrlEncode(Info.m_Model));
			Body.AppendPrintf("&simid=%s", UrlEncode(Info.m_Iccid));
			Body.AppendPrintf("&esn=%s", UrlEncode(Info.m_Imei));
			Body.AppendPrintf("&rssi=%u", Info.m_uSignal);
			Body.AppendPrintf("&wss=%u", ToBars(Info.m_uSignal));
			Body.AppendPrintf("&wut=%u", ToUptime(Info.m_CTime));
			Body.AppendPrintf("&stype=%s", UrlEncode(Service));
			Body.AppendPrintf("&wrx=%llu", Info.m_RxBytes - m_CellRx[uServer]);
			Body.AppendPrintf("&wtx=%llu", Info.m_TxBytes - m_CellTx[uServer]);

			return true;
		}
	}

	return false;
}

bool CSvmClient::AddGpsLocation(CString &Body)
{
	if( m_Loc.m_uFix >= 2 ) {

		Body.AppendPrintf("&gps_type=%u", 1);
		Body.AppendPrintf("&gpsType=%u", 1);
		Body.AppendPrintf("&gps_lat=%.8f", m_Loc.m_Lat);
		Body.AppendPrintf("&gps_long=%.8f", m_Loc.m_Long);

		if( m_Loc.m_uFix >= 3 ) {

			Body.AppendPrintf("&gps_alt=%.0f", m_Loc.m_Alt);
		}

		return true;
	}

	return false;
}

bool CSvmClient::AddHeartbeat(CString &Body, bool fInit)
{
	// alert_lvl,alert level,"A number representing the severity of the alert (lower numbers are high severity)"

	// alert_msg,alert message,"Short message describing the type of checkin"

	Body.AppendPrintf("&alert_lvl=%u", fInit ? 5 : 6);
	Body.AppendPrintf("&alert_msg=%s", "Heartbeat");

	return true;
}

UINT CSvmClient::ToBars(UINT uSignal)
{
	if( uSignal < 99 ) {

		if( uSignal >= 20 ) return 4;
		if( uSignal >= 15 ) return 3;
		if( uSignal >= 10 ) return 2;
		if( uSignal >=  2 ) return 1;
	}

	return 0;
}

UINT CSvmClient::ToUptime(DWORD ctime)
{
	if( ctime ) {

		time_t now  = getmonosecs();

		time_t then = time_t(ctime);

		return (now > then) ? (now - then) : 0;
	}

	return 0;
}

// Transaction Handler

bool CSvmClient::Transact(CHttpClientConnection *pConnect, CHttpClientRequest &Req, CString Body)
{
	CString Path = "/jbm_mgmt/update_status.php";

	if( Body.GetLength() < 1024 ) {

		Req.SetPath(Path + "?" + Body);

		Req.SetVerb("GET");
	}
	else {
		Req.SetPath(Path);

		Req.SetRequestBody(Body);

		Req.AddRequestHeader("Content-Type", "application/x-www-form-urlencoded");

		Req.SetVerb("POST");
	}

	if( Req.Transact(pConnect, 200) ) {

		return true;
	}

	return false;
}

// End of File
