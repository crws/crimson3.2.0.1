
#include "Intern.hpp"

#include "Splash.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PXE Splash Screen
//

// Constructor

CSplash::CSplash(void)
{
	m_pDisp  = NULL;

	m_pTouch = NULL;

	m_pGdi   = NULL;

	m_pInput = NULL;

	AfxGetObject("display", 0, IDisplay, m_pDisp);

	if( m_pDisp ) {

		AfxGetObject("touch", 0, ITouchScreen, m_pTouch);

		AfxNewObject("gdi", IGdi, m_pGdi);

		AfxGetObject("input-d", 0, IInputQueue, m_pInput);

		m_uLevel  = 0;

		m_pSink   = NULL;

		m_pLock   = Create_Qutex();

		m_pMap    = NULL;

		m_pButton = NULL;

		m_hThread = NULL;

		m_fPause  = FALSE;

		m_uFont   = fontHei16;

		m_pDisp->GetSize(m_cx, m_cy);

		m_pGdi->Create(m_cx, m_cy, NULL);

		switch( MAKELONG(m_cx, m_cy) ) {

			case MAKELONG(320, 240):
			{
				m_uFont = fontHei10;
			}
			break;
		}

		m_pGdi->SelectFont(m_uFont);

		m_tx = m_pGdi->GetTextWidth ('X');

		m_ty = m_pGdi->GetTextHeight('X');

		m_hy = m_ty / 2;

		Redraw();
	}

	StdSetRef();
}

// Destructor

CSplash::~CSplash(void)
{
	AfxRelease(m_pInput);
	AfxRelease(m_pGdi);
	AfxRelease(m_pTouch);
	AfxRelease(m_pDisp);
}

// Operations

void CSplash::SetEventSink(IEventSink *pSink)
{
	m_pSink = pSink;
}

void CSplash::ShowStatus(PCTXT pText)
{
	m_Text = pText;

	Redraw();
}

void CSplash::SetIpList(CStringArray const &IpList)
{
	m_IpList = IpList;

	Redraw();
}

void CSplash::SetmDnsNames(CStringArray const &Names)
{
	m_Names = Names;

	Redraw();
}

void CSplash::EnableUI(UINT uLevel)
{
	if( m_pDisp ) {

		CAutoLock Lock(m_pLock);

		if( m_uLevel != uLevel ) {

			if( (m_uLevel = uLevel) >= 3 ) {

				if( !m_hThread ) {

					m_pButton = Create_PushButton();

					R2 Rect;

					Rect.right  = m_cx - m_ty;
					Rect.bottom = m_cy - m_ty;
					Rect.left   = Rect.right  - 5 * m_ty;
					Rect.top    = Rect.bottom - 5 * m_hy;

					m_pButton->Create(m_pGdi, this, Rect, 100, bsLatch, 0, this);

					m_pButton->SetText(L"Pause");

					AfxNewObject("touchmap", ITouchMap, m_pMap);

					m_pMap->Create(m_pTouch);

					m_pMap->FillRect(PassRect(Rect));

					m_pTouch->SetMap(m_pMap->GetBuffer());

					m_fPause  = FALSE;

					m_hThread = CreateClientThread(this, 0, 20000, NULL);
				}
			}
			else {
				if( m_hThread ) {

					DestroyThread(m_hThread);

					m_hThread = 0;
				}
			}

			Redraw();
		}
	}
}

void CSplash::ClearPause(void)
{
	if( m_pDisp ) {

		CAutoLock Lock(m_pLock);

		if( m_uLevel >= 3 && m_fPause ) {

			m_pButton->SetState(0);

			m_fPause = FALSE;

			Redraw();
		}
	}
}

// IUnknown

HRESULT CSplash::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CSplash::AddRef(void)
{
	StdAddRef();
}

ULONG CSplash::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CSplash::TaskInit(UINT uTask)
{
	SetThreadName("PxeSplash");

	return m_pDisp ? TRUE : FALSE;
}

INT CSplash::TaskExec(UINT uTask)
{
	for( ;;) {

		INPUT dwRead = m_pInput->Read(NOTHING);

		BOOL  fMatch = FALSE;

		if( dwRead ) {

			CInput &i    = (CInput &) dwRead;

			UINT uType  = i.x.i.m_Type;

			UINT uCode  = i.x.d.k.m_Code;

			UINT uTrans = i.x.i.m_State;

			BOOL fLocal = i.x.i.m_Local;

			if( uType == typeKey ) {

				if( m_pButton ) {

					if( uTrans == stateDown || uTrans == stateRepeat ) {

						fMatch = m_pButton->OnMessage(MAKELONG(msgKeyDown, fLocal), uCode);
					}

					if( uTrans == stateUp ) {

						fMatch = m_pButton->OnMessage(MAKELONG(msgKeyUp, fLocal), uCode);
					}
				}
			}

			if( uType == typeTouch ) {

				P2 Pos;

				Pos.x = i.x.d.t.m_XPos * 4;

				Pos.y = i.x.d.t.m_YPos * 4;

				UINT uMsg   = 0;

				UINT uParam = MAKELONG(Pos.x, Pos.y);

				switch( uTrans ) {

					case stateDown:
						uMsg = msgTouchDown;
						break;

					case stateRepeat:
						uMsg = msgTouchRepeat;
						break;

					case stateUp:
						uMsg = msgTouchUp;
						break;
				}

				if( m_pButton ) {

					fMatch = m_pButton->OnMessage(MAKELONG(uMsg, fLocal), uParam);
				}
			}

			if( fMatch ) {

				CAutoLock Lock(m_pLock);

				AfxNewAutoObject(pDirty, "region", IRegion);

				m_pButton->DrawPrep(m_pGdi, pDirty);

				m_pButton->DrawExec(m_pGdi, pDirty);

				m_pDisp->Update(m_pGdi->GetBuffer());
			}
		}
	}
}

void CSplash::TaskStop(UINT uTask)
{
}

void CSplash::TaskTerm(UINT uTask)
{
	m_pTouch->SetMap(NULL);

	AfxRelease(m_pMap);

	AfxRelease(m_pButton);

	m_pMap    = NULL;

	m_pButton = NULL;
}

// INotify

void CSplash::OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData)
{
	m_fPause = (m_pButton->GetState() == 1);

	m_pSink->OnEvent(0, m_fPause ? 1 : 2);
}

// IStyle

void CSplash::GetColor(UINT uStyle, UINT uCode, COLOR &Color)
{
	if( uCode == colButtonBorder ) {

		Color = GetRGB(20, 20, 20);
	}
}

void CSplash::GetMetric(UINT uStyle, UINT uCode, int &nSize)
{
}

void CSplash::GetFont(UINT uStyle, UINT uCode, IGdiFont * &pFont)
{
	pFont = (IGdiFont *) MAKELONG(m_uFont, 0);
}

// Implementation

void CSplash::Redraw(void)
{
	if( m_pDisp ) {

		CAutoLock Lock(m_pLock);

		if( m_uLevel ) {

			m_pGdi->ClearScreen(0);

			m_pGdi->ResetAll();

			m_pGdi->SelectFont(m_uFont);

			m_pGdi->DrawRect(2, 2, m_cx-2, m_cy-2);

			if( TRUE ) {

				m_pGdi->SetTextFore(GetRGB(31, 31, 31));

				m_pGdi->TextOut(m_ty, 2 * m_hy, "CRIMSON 3.2 RUNTIME");

				m_pGdi->SetTextFore(GetRGB(20, 20, 31));

				m_pGdi->TextOut(m_ty, 7 * m_hy, m_Text);
			}

			if( m_IpList.GetCount() ) {

				int x1 = m_ty;

				int x2 = x1 + m_cx / 4 - 16;

				m_pGdi->SetTextFore(GetRGB(31, 31, 31));

				m_pGdi->TextOut(x1, 12 * m_hy, "Interface");

				m_pGdi->TextOut(x2, 12 * m_hy, "IP Address");

				m_pGdi->SetTextFore(GetRGB(20, 20, 20));

				for( UINT r = 1, n = 0; n < m_IpList.GetCount(); r += 1, n += 2 ) {

					m_pGdi->TextOut(x1, (12 + 3 * r) * m_hy, m_IpList[n + 0]);

					m_pGdi->TextOut(x2, (12 + 3 * r) * m_hy, m_IpList[n + 1]);
				}
			}

			if( m_Names.GetCount() ) {

				int x3 = m_cx / 2 + 8;

				m_pGdi->SetTextFore(GetRGB(31, 31, 31));

				m_pGdi->TextOut(x3, 12 * m_hy, "mDNS Names");

				m_pGdi->SetTextFore(GetRGB(20, 20, 20));

				for( UINT r = 1, n = 0; n < m_Names.GetCount(); r += 1, n += 1 ) {

					m_pGdi->TextOut(x3, (12 + 3 * r) * m_hy, m_Names[n]);
				}
			}

			if( TRUE ) {

				m_pGdi->SetTextFore(GetRGB(20, 20, 20));

				m_pGdi->TextOut(m_ty, m_cy - 2 * m_ty, "Copyright \x0A9 1993-2020 Red Lion Controls Inc.");
			}

			if( m_pButton ) {

				AfxNewAutoObject(pDirty, "region", IRegion);

				R2 Rect = { 0, 0, m_cx, m_cy };

				pDirty->AddRect(Rect);

				m_pButton->DrawExec(m_pGdi, pDirty);
			}

			m_pDisp->Update(m_pGdi->GetBuffer());
		}
	}
}

// End of File
