
#include "Intern.hpp"

#include "G3AuthService.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Authentication Service
//

// Instantiator

global ILinkService * Create_AuthService(ICrimsonPxe *pPxe)
{
	return New CG3AuthService(pPxe);
}

// Constructor

CG3AuthService::CG3AuthService(ICrimsonPxe *pPxe)
{
	m_pPxe = pPxe;

	StdSetRef();
}

// Destructor

CG3AuthService::~CG3AuthService(void)
{
}

// IUnknown

HRESULT CG3AuthService::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ILinkService);

	StdQueryInterface(ILinkService);

	return E_NOINTERFACE;
}

ULONG CG3AuthService::AddRef(void)
{
	StdAddRef();
}

ULONG CG3AuthService::Release(void)
{
	StdRelease();
}

// ILinkService

void CG3AuthService::Timeout(void)
{
}

UINT CG3AuthService::Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans)
{
	if( Req.GetService() == servAuth ) {

		if( Req.GetOpcode() == authGetChallenge ) {

			if( !pTrans->IsP2P() ) {

				Rep.StartFrame(servAuth, opReply);

				CString Device;

				BYTE    bChallenge[16];

				m_pPxe->GetUnitName(Device);

				AfxGetAutoObject(pEntropy, "entropy", 0, IEntropy);

				pEntropy->GetEntropy(bChallenge, sizeof(bChallenge));

				Rep.AddByte(BYTE(Device.GetLength()));

				Rep.AddByte(sizeof(bChallenge));

				Rep.AddData(PCBYTE(PCSTR(Device)), Device.GetLength());

				Rep.AddData(bChallenge, sizeof(bChallenge));

				return procOkay;
			}

			Rep.StartFrame(servAuth, opReply);

			Rep.AddByte(BYTE(0));

			Rep.AddByte(BYTE(0));

			return procOkay;
		}

		if( Req.GetOpcode() == authSendResponse ) {

			if( !pTrans->IsP2P() ) {

				UINT    uPtr  = 0;

				UINT    nu    = Req.ReadByte(uPtr);

				UINT    nr    = Req.ReadByte(uPtr);

				CString User  = CString(PCSTR(Req.ReadData(uPtr, nu)), nu);

				PCBYTE  pData = Req.ReadData(uPtr, nr);

				CAuthInfo Auth;

				CUserInfo Info;

				Auth.m_Challenge.Append(pData, nr / 2);

				Auth.m_Response.Append(pData + nr / 2, nr - nr / 2);

				Rep.StartFrame(servAuth, opReply);

				if( m_pPxe->GetUserInfo(User, Auth, Info) ) {

					if( Info.m_uWebAccess <= 1 ) {

						pTrans->SetAuth();

						Rep.AddByte(1);

						return procOkay;
					}
				}

				Rep.AddByte(0);

				return procOkay;
			}

			Rep.AddByte(1);

			return procOkay;
		}
	}

	return procError;
}

void CG3AuthService::EndLink(CG3LinkFrame &Req)
{
}

// End of File
