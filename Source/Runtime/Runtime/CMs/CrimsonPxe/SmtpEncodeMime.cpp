
#include "intern.hpp"

#include "SmtpEncodeMime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SmtpDefs.hpp"

#include "StdEndpoint.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MIME Mail Enoder
//

// Constructor

CSmtpEncodeMime::CSmtpEncodeMime(CStdEndpoint *pEndpoint) : CSmtpEncodeBase(pEndpoint)
{
}

// Operations

BOOL CSmtpEncodeMime::Encode(CMsgPayload const *pMessage)
{
	MakeBoundary();

	return	SendHead(pMessage) &&
		SendVersion() &&
		SendContentType(2, pMessage) &&
		SendBoundary() &&
		SendContentType(0, pMessage) &&
		SendContentTransferEncoding(0) &&
		SendBody(pMessage->m_Message) &&
		SendBoundary() &&
		SendContentType(1, pMessage) &&
		SendContentTransferEncoding(1) &&
		SendAttachment(pMessage->m_AttachFile) &&
		SendBoundary(TRUE);
}

// Implementation

BOOL CSmtpEncodeMime::SendVersion(void)
{
	CString s;

	s.Printf("MIME-Version: 1.0\r\n");

	return Send(s);
}

BOOL CSmtpEncodeMime::SendContentTransferEncoding(UINT uType)
{
	if( uType == 0 ) {

		CString s;

		s.Printf("Content-Transfer-Encoding: 7bit\r\n");

		return Send(s);
	}

	if( uType == 1 ) {

		CString s;

		s.Printf("Content-Transfer-Encoding: base64\r\n");

		return Send(s);
	}

	return FALSE;
}

BOOL CSmtpEncodeMime::SendContentType(UINT uType, CMsgPayload const *pMessage)
{
	CString s;

	if( uType == 0 ) {

		s.Printf("Content-Type: text/plain; charset=us-ascii\r\n");
	}

	if( uType == 1 ) {

		CString Name(pMessage->m_AttachFile);

		Name.MakeLower();

		s.Printf("Content-Type: application/octet-stream; name=%s\r\n", PCTXT(Name));
	}

	if( uType == 2 ) {

		s.Printf("Content-Type: multipart/mixed; boundary=\"%s\"\r\n", m_Boundary);
	}

	return Send(s);
}

BOOL CSmtpEncodeMime::SendBoundary(BOOL fEnd)
{
	CString s;

	s += "\r\n--";

	s += m_Boundary;

	s += fEnd ? "--\r\n" : "\r\n";

	return Send(s);
}

BOOL CSmtpEncodeMime::SendAttachment(CFilename const &Name)
{
	CAutoFile File(Name, "r");

	if( File ) {

		Send("\r\n");

		if( m_pEndpoint->SendFile(File, TRUE) ) {

			return TRUE;
		}
	}

	return FALSE;
}

void CSmtpEncodeMime::MakeBoundary(void)
{
	char cRand = "ABCDEFGHIJ"[rand()%10];

	int  nRand = rand();

	int  nTime = GetTickCount();

	SPrintf(m_Boundary, "%c_%8.8X.%10.10d", cRand, nRand, nTime);
}

// End of File
