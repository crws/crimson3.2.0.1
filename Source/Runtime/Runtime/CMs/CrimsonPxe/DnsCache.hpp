
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DnsCache_HPP

#define	INCLUDE_DnsCache_HPP

//////////////////////////////////////////////////////////////////////////
//
// IP Address List
//

typedef CArray <CIpAddr> CIpList;

//////////////////////////////////////////////////////////////////////////
//
// DNS Cache
//

class CDnsCache
{
public:
	// Constructor
	CDnsCache(void);

	// Destructor
	~CDnsCache(void);

	// Attributes
	IMutex * GetLock(void) const;

	// Operations
	BOOL Find(PCTXT pHost, CIpList * &pList);
	void Add(PCTXT pHost, CIpList const &List, DWORD dwTTL);
	void Poll(void);
	void Show(void);

protected:
	// Cache Entry
	struct CDnsEntry
	{
		CString m_Name;
		CIpList m_List;
		time_t  m_Time;
	};

	// DNS Map
	typedef CMap<PCTXT, CDnsEntry *> CDnsMap;

	// Data Members
	IMutex * m_pMutex;
	CDnsMap  m_Map;
};

// End of File

#endif
