
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ImageLoader_HPP

#define INCLUDE_ImageLoader_HPP

//////////////////////////////////////////////////////////////////////////
//
// PXE Image Loader
//

class CImageLoader
{
public:
	// Constructor
	CImageLoader(void);

	// Destructor
	~CImageLoader(void);

	// Operations
	bool Check(char cTag);
	bool Disable(void);
	bool Import(void);

protected:
	// File Header 
	struct CImageHeader
	{
		DWORD	m_dwMagic;
		WORD	m_wVersion;
		WORD	m_wFlags;
		WORD	m_wTargCount;
		WORD	m_wFileCount;
		WORD	m_wDbase;
		BYTE	m_bPad[18];
		char	m_sOEM[32];
	};

	// Target Record
	struct CImageTargRecord
	{
		char	m_sName[16];
		WORD	m_wFile[8];
	};

	// File Record
	struct CImageFileRecord
	{
		char	m_sName[8];
		DWORD	m_dwPos;
		DWORD	m_dwSize;
		BYTE	m_bGUID[16];
	};

	// Data Members
	ISchemaGenerator * m_pSchema;
	IConfigStorage   * m_pConfig;
	char		   m_cTag;
	CString		   m_Path;
	CString		   m_Model;
	CString		   m_Serial;
	PWORD		   m_pIndex;
	CImageHeader       m_Header;
	CImageTargRecord * m_pTargs;
	CImageFileRecord * m_pFiles;
	CString		   m_Text;
	CJsonData          m_Json;

	// Implementation
	bool FindPath(CString &Path, char cTag, UINT n);
	bool CheckApplication(CAutoFile &File);
	bool CheckDeviceConfig(CAutoFile &File);
	bool CheckConfigPart(CAutoFile &File, char cTag);
	bool LoadApplication(bool &kill);
	bool LoadDeviceConfig(void);
	bool LoadConfigPart(void);
	bool ValidateHeader(void);
	bool ValidateIndices(CImageTargRecord *pTarg, UINT nf);
	bool ValidateCrc(CAutoFile &File);
	UINT FindBootRevision(void);
	bool CheckBoot(CImageFileRecord *pBoot);
	bool CheckFirm(CImageFileRecord *pFirm);
	bool CheckDatabase(CImageFileRecord *pData);
	bool ApplyBoot(CImageFileRecord *pBoot);
	bool ApplyFirm(CImageFileRecord *pFirm);
	bool ApplyDatabase(CImageFileRecord *pData);
	bool CheckType(CString const &Text, char cTag);
	bool SkipCheck(void);
};

// End of File

#endif
