
#include "intern.hpp"

#include "RadiusFrame.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Radius Frame
//

// Byte Order Conversion

void CRadiusFrame::NetToHost(void)
{
	m_wLength = ::NetToHost(m_wLength);
}

void CRadiusFrame::HostToNet(void)
{
	m_wLength = ::HostToNet(m_wLength);
}

// Attributes Access

bool CRadiusFrame::HasAttribute(UINT uPos)
{
	return uPos < m_wLength - sizeof(RADIUS_FRAME) + sizeof(m_bAttr);
}

bool CRadiusFrame::GetAttribute(UINT &uPos, BYTE &bType, UINT &uSize, PCBYTE &pData)
{
	if( uPos < m_wLength - sizeof(RADIUS_FRAME) + sizeof(m_bAttr) ) {

		bType = m_bAttr[uPos+0];

		uSize = m_bAttr[uPos+1];

		pData = m_bAttr + uPos + 2;

		uPos += uSize;

		return TRUE;
	}

	return FALSE;
}

// Operations

void CRadiusFrame::Create(BYTE bCode, BYTE bIdent)
{
	m_bCode   = bCode;

	m_bIdent  = bIdent;

	m_wLength = WORD(sizeof(RADIUS_FRAME) - sizeof(m_bAttr));

	AfxGetAutoObject(pEntropy, "entropy", 0, IEntropy);

	pEntropy->GetEntropy(m_bAuth, sizeof(m_bAuth));
}

bool CRadiusFrame::Validate(PCSTR pSecret, CRadiusFrame const &Send)
{
	if( m_bIdent == Send.m_bIdent ) {

		BYTE test[16], hash[16];

		memcpy(test, m_bAuth, 16);

		memcpy(m_bAuth, Send.m_bAuth, 16);

		UINT uThis   = ::NetToHost(m_wLength);

		UINT uSecret = strlen(pSecret);

		UINT uData   = uThis + uSecret;

		CAutoArray<BYTE> pData(uData);

		memcpy(pData, this, uThis);

		memcpy(pData + uThis, pSecret, uSecret);

		md5(pData, uData, hash);

		if( !memcmp(hash, test, 16) ) {

			NetToHost();

			return true;
		}
	}

	return false;
}

void CRadiusFrame::AddAttribute(BYTE bType, PCBYTE pData, UINT uData)
{
	UINT uPos = m_wLength - sizeof(RADIUS_FRAME) + sizeof(m_bAttr);

	m_bAttr[uPos++] = bType;

	m_bAttr[uPos++] = BYTE(uData + 2);

	if( uData ) {

		memcpy(m_bAttr + uPos, pData, uData);

		uPos += uData;
	}

	m_wLength = WORD(uPos + sizeof(RADIUS_FRAME) - sizeof(m_bAttr));
}

void CRadiusFrame::AddAttribute(BYTE bType, PCSTR pText)
{
	AddAttribute(bType, PCBYTE(pText), strlen(pText));
}

void CRadiusFrame::AddPassword(PCSTR pSecret, PCSTR pPassword)
{
	UINT uSecret   = strlen(pSecret);

	UINT uPassword = strlen(pPassword);

	BYTE cn[16];

	BYTE pw[256];

	UINT up = 0;

	memcpy(cn, m_bAuth, 16);

	for( UINT n = 0; n < uPassword; n += 16 ) {

		BYTE bn[256];

		UINT un = uSecret + 16;

		memcpy(bn, pSecret, uSecret);

		memcpy(bn + uSecret, cn, 16);

		md5(bn, un, cn);

		for( UINT i = 0; i < 16; i++ ) {

			if( !pPassword[n+i] ) {

				break;
			}

			cn[i] ^= BYTE(pPassword[n+i]);
		}

		memcpy(pw + up, cn, 16);

		up += 16;
	}

	AddAttribute(attrUserPassword, pw, up);
}

void CRadiusFrame::AddChapPassword(PCSTR pPassword)
{
	UINT uPassword = strlen(pPassword);

	UINT uData     = 1 + uPassword + sizeof(m_bAuth);

	CAutoArray<BYTE> pData(uData);

	pData[0] = BYTE(rand());

	memcpy(pData + 1, pPassword, uPassword);

	memcpy(pData + 1 + uPassword, m_bAuth, sizeof(m_bAuth));

	BYTE pr[17];

	pr[0] = pData[0];

	md5(pData, uData, pr+1);

	AddAttribute(attrChapPassword, pr, sizeof(pr));
}

void CRadiusFrame::AddChap(CByteArray const &Challenge, CByteArray const &Response)
{
	memcpy(m_bAuth, Challenge.data(), Challenge.size());

	AddAttribute(attrChapPassword, Response.data(), Response.size());
}

void CRadiusFrame::AddPppAttributes(void)
{
	DWORD st = HostToMotor(DWORD(2));

	DWORD fp = HostToMotor(DWORD(1));

	AddAttribute(attrServiceType, PCBYTE(&st), sizeof(st));

	AddAttribute(attrFramedProtocol, PCBYTE(&fp), sizeof(fp));
}

// End of File
