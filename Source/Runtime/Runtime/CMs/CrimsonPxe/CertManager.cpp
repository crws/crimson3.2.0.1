
#include "intern.hpp"

#include "CertManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cetificate Manager
//

// Constructor

CCertManager::CCertManager(CJsonConfig *pJson)
{
	ApplyConfig(pJson);

	StdSetRef();

	piob->RegisterSingleton("c3.certman", 0, this);
}

// Destructor

CCertManager::~CCertManager(void)
{
	piob->RevokeSingleton("c3.certman", 0);
}

// IUnknown

HRESULT CCertManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ICertManager);

	StdQueryInterface(ICertManager);

	return E_NOINTERFACE;
}

ULONG CCertManager::AddRef(void)
{
	StdAddRef();
}

ULONG CCertManager::Release(void)
{
	StdRelease();
}

// ICertManager

BOOL CCertManager::GetTrustedRoots(CByteArray &Certs)
{
	for( INDEX i = m_Roots.GetHead(); !m_Roots.Failed(i); m_Roots.GetNext(i) ) {

		CCert const &c = m_Certs[m_Roots.GetData(i)];

		Certs.Append(c.m_Cert);
	}

	return !Certs.IsEmpty();
}

BOOL CCertManager::GetIdentityCert(UINT uSlot, CByteArray &Cert, CByteArray &Priv, CString &Pass)
{
	INDEX i = m_Client.FindName(uSlot);

	if( !m_Client.Failed(i) ) {

		CCert const &c = m_Certs[m_Client.GetData(i)];

		Cert = c.m_Cert;
		
		Priv = c.m_Priv;
		
		Pass = c.m_Pass;

		return TRUE;
	}

	return FALSE;
}

BOOL CCertManager::GetTrustedCert(UINT uSlot, CByteArray &Cert)
{
	INDEX i = m_Server.FindName(uSlot);

	if( !m_Server.Failed(i) ) {

		CCert const &c = m_Certs[m_Server.GetData(i)];

		Cert = c.m_Cert;

		return TRUE;
	}

	return FALSE;
}

// Implementation

void CCertManager::ApplyConfig(CJsonConfig *pJson)
{
	if( pJson ) {

		ApplyConfig(pJson->GetChild("roots"), m_Roots);

		ApplyConfig(pJson->GetChild("server"), m_Server);

		ApplyConfig(pJson->GetChild("client"), m_Client);
	}
}

void CCertManager::ApplyConfig(CJsonConfig *pJson, CMap<UINT, UINT> &Map)
{
	if( pJson ) {

		CJsonConfig *pList = pJson->GetChild("certs");

		if( pList ) {

			for( UINT n = 0; n < pList->GetCount(); n++ ) {

				CJsonConfig *pCert = pList->GetChild(n);

				UINT         uKey  = pCert->GetValueAsUInt("key", n, 0, 65535);

				CCert   Cert;

				if( !pCert->GetValueAsBlob("data", Cert.m_Cert) ) {

					pCert->GetValueAsBlob("cert", Cert.m_Cert);

					pCert->GetValueAsBlob("priv", Cert.m_Priv);

					Cert.m_Pass = pCert->GetValue("pass", "");
				}

				Map.Insert(uKey, m_Certs.Append(Cert));
			}
		}
	}
}

// End of File
