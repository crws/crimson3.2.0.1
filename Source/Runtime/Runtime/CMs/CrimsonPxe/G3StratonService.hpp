
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3StratonService_HPP

#define	INCLUDE_G3StratonService_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Straton Programming Service
//

class CG3StratonService : public ILinkService
{
public:
	// Constructor
	CG3StratonService(ICrimsonPxe *pPxe);

	// Destructor
	~CG3StratonService(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ILinkService
	void Timeout(void);
	UINT Process(CG3LinkFrame &Req, CG3LinkFrame &Re, ILinkTransport *pTrans);
	void EndLink(CG3LinkFrame &Req);

protected:
	// Data Members
	ULONG	         m_uRefs;
	ICrimsonPxe    * m_pPxe;
	BYTE		 m_bData[1024];
};

// End of File

#endif
