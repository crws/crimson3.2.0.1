
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SmtpEncodeMime_HPP

#define	INCLUDE_SmtpEncodeMime_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SmtpEncodeBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Basic Mail Enoder
//

class CSmtpEncodeMime : public CSmtpEncodeBase
{
public:
	// Constructor
	CSmtpEncodeMime(CStdEndpoint *pEndpoint);

	// Operations
	BOOL Encode(CMsgPayload const *pMessage);

protected:
	// Data
	char m_Boundary[32];

	// Implementation
	BOOL SendVersion(void);
	BOOL SendContentTransferEncoding(UINT uType);
	BOOL SendContentType(UINT uType, CMsgPayload const *pMessage);
	BOOL SendBoundary(BOOL fEnd = FALSE);
	BOOL SendAttachment(CFilename const &Name);
	void MakeBoundary(void);
};

// End of File

#endif
