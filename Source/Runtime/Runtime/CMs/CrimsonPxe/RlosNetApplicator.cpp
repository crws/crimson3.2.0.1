
#include "Intern.hpp"

#include "RlosNetApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

#include "MultiDns.hpp"

#include "DnsResolver.hpp"

#include "NtpClient.hpp"

#include "SmtpClient.hpp"

#include "Location.hpp"

#include "FtpServer.hpp"

#include "Identifier.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RLOS Net Applicator
//

// Instantiator

INetApplicator * Create_RlosNetApplicator(void)
{
	return new CRlosNetApplicator;
}

// Constructor

CRlosNetApplicator::CRlosNetApplicator(void)
{
	m_pRouter       = NULL;
	m_pUtils        = NULL;
	m_crcFaces      = 0;
	m_crcRouting    = 0;
	m_crcResolver   = 0;
	m_crcNtpSync    = 0;
	m_crcSmtpClient = 0;
	m_crcLocation   = 0;
	m_crcIdent      = 0;
	m_crcFtpServer  = 0;
	m_pMulti1       = NULL;
	m_pMulti2       = NULL;
	m_pNtpSync      = NULL;
	m_pSmtpClient   = NULL;
	m_pLocation     = NULL;
	m_pIdentifier   = NULL;
	m_pFtpServer    = NULL;
	m_pResolver     = NULL;

	AfxGetObject("ip", 0, IRouter, m_pRouter);

	AfxGetObject("ip", 0, INetUtilities, m_pUtils);

	AfxGetAutoObject(pPlatform, "platform", 0, IPlatform);

	m_DefName = pPlatform->GetDefName();

	StdSetRef();
}

// Destructor

CRlosNetApplicator::~CRlosNetApplicator(void)
{
	AfxRelease(m_pRouter);

	AfxRelease(m_pUtils);
}

// IUnknown

HRESULT CRlosNetApplicator::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(INetApplicator);

	StdQueryInterface(INetApplicator);

	return E_NOINTERFACE;
}

ULONG CRlosNetApplicator::AddRef(void)
{
	StdAddRef();
}

ULONG CRlosNetApplicator::Release(void)
{
	StdRelease();
}

// INetApplicator

bool CRlosNetApplicator::ApplySettings(CJsonConfig *pJson)
{
	if( pJson ) {

		ApplyFaces(pJson->GetChild("net.faces"));

		ApplyResolver(pJson->GetChild("net.resolver"));

		ApplyIdent(pJson->GetChild("net.ident"));

		ApplyRouting(pJson->GetChild("net.routing"));

		ApplyServices(pJson->GetChild("services"));

		return true;
	}
	else {
		ApplyServices(NULL);

		ApplyIdent(NULL);

		ApplyResolver(NULL);

		ApplyFaces(NULL);

		return true;
	}

	return false;
}

bool CRlosNetApplicator::GetUnitName(CString &Name)
{
	Name = m_UnitName;

	return true;
}

bool CRlosNetApplicator::GetmDnsNames(CStringArray &List)
{
	List = m_mDnsNames;

	return true;
}

bool CRlosNetApplicator::GetInfo(CString &Status, PCSTR pName)
{
	return false;
}

// Implementation

BOOL CRlosNetApplicator::ApplyFaces(CJsonConfig *pFaces)
{
	if( CheckCRC(m_crcFaces, pFaces) ) {

		ApplyRouting(NULL);

		m_pRouter->Close();

		m_FaceNames.Empty();

		m_FaceDescs.Empty();
	}

	if( pFaces ) {

		m_FaceNames.Append("lo");

		m_FaceDescs.Append("Loopback");

		for( UINT n = 0;; n++ ) {

			CJsonConfig *pFace = pFaces->GetChild(CPrintf("eth%u", n));

			if( pFace ) {

				if( !ApplyEthernet(n, pFace) ) {

					m_pRouter->AddNullInterface();

					m_FaceNames.Append("null");

					m_FaceDescs.Append("Disabled Interface");
				}

				continue;
			}

			break;
		}

		for( UINT n = 0;; n++ ) {

			CJsonConfig *pFace = pFaces->GetChild(CPrintf("ppp%u", n));

			if( pFace ) {

				if( !ApplyPpp(n, pFace) ) {

					m_pRouter->AddNullInterface();

					m_FaceNames.Append("null");

					m_FaceDescs.Append("Disabled Interface");
				}

				continue;
			}

			break;
		}

		m_pUtils->SetInterfaceNames(m_FaceNames);

		m_pUtils->SetInterfaceDescs(m_FaceDescs);

		m_pRouter->Open();
	}

	return TRUE;
}

BOOL CRlosNetApplicator::ApplyEthernet(UINT uInst, CJsonConfig *pFace)
{
	UINT uMode;

	if( (uMode = pFace->GetValueAsUInt("mode", 0, 0, 3)) ) {

		CConfigEth Config = { "", "", 0 };

		if( uMode == 2 ) {

			BYTE b3 = BYTE(1 + uInst);

			Config.m_Ip   = pFace->GetValueAsIp("address", CIpAddr(192, 168, b3, 10));

			Config.m_Mask = pFace->GetValueAsIp("netmask", "255.255.255.0");

			Config.m_Gate = pFace->GetValueAsIp("gateway", "0.0.0.0");
		}

		if( uMode == 3 ) {

			Config.m_Ip   = IP_EXPER;

			Config.m_Mask = IP_EXPER;

			Config.m_Gate = IP_EXPER;
		}

		Config.m_RecvMSS = WORD(pFace->GetValueAsUInt("recvmss", 1280, 768, 2000));

		Config.m_SendMSS = WORD(pFace->GetValueAsUInt("sendmss", 1280, 768, 2000));

		Config.m_fFast   = pFace->GetValueAsBool("fast", TRUE);

		Config.m_fFull   = pFace->GetValueAsBool("full", TRUE);

		Config.m_Display = CPrintf("eth%u", uInst);

		Config.m_DevName = "pnp.pnp-nic";

		Config.m_DevInst = uInst;

		m_pRouter->AddEthernet(Config);

		m_FaceNames.Append(Config.m_Display);

		m_FaceDescs.Append(CPrintf("Ethernet %u", 1 + uInst));

		return TRUE;
	}

	return FALSE;
}

BOOL CRlosNetApplicator::ApplyPpp(UINT uInst, CJsonConfig *pFace)
{
	UINT uMode;

	if( (uMode = pFace->GetValueAsUInt("mode", 0, 0, 1)) ) {

		CString Type = pFace->GetValue("type", "");

		if( Type == "hspa" ) {

			CConfigPpp Config = { "", "", 0 };

			Config.m_uMode    = pppClient;

			Config.m_uConnect = connectCellHSPA2;

			strcpy(Config.m_sDial, pFace->GetValue("apn", ""));

			strcpy(Config.m_sUser, pFace->GetValue("username", ""));

			strcpy(Config.m_sPass, pFace->GetValue("password", ""));

			if( pFace->GetValueAsBool("onDemand", FALSE) ) {

				Config.m_uTimeout = pFace->GetValueAsUInt("timeout", 30, 30, 3600);
			}
			else {
				Config.m_uTimeout = 0;
			}

			if( pFace->GetValueAsBool("routeMode", FALSE) ) {

				BYTE b3 = BYTE(100+uInst);

				Config.m_fGate   = FALSE;

				Config.m_RemAddr = pFace->GetValueAsIp("routeAddr", CIpAddr(192, 168, b3, 0));

				Config.m_RemMask = pFace->GetValueAsIp("routeMask", CIpAddr(255, 255, 255, 0));
			}
			else {
				Config.m_fGate = TRUE;
			}

			Config.m_Display = CPrintf("ppp%u", uInst);

			Config.m_DevName = "pnp.pnp-modem";

			Config.m_DevInst = uInst;

			m_pRouter->AddPpp(Config);

			m_FaceNames.Append(Config.m_Display);

			m_FaceDescs.Append(CPrintf("Modem %u", 1 + uInst));

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CRlosNetApplicator::ApplyRouting(CJsonConfig *pRouting)
{
	if( CheckCRC(m_crcRouting, pRouting) ) {

		m_pRouter->RemoveRoutes(NOTHING);
	}

	if( pRouting ) {

		CJsonConfig *pRoutes = pRouting->GetChild("routes");

		if( pRoutes ) {

			for( UINT n = 0;; n++ ) {

				CJsonConfig *pRoute = pRoutes->GetChild(n);

				if( pRoute ) {

					CIpAddr Dest = pRoute->GetValueAsIp("ip", CIpAddr::m_Empty);

					CIpAddr Mask = pRoute->GetValueAsIp("mask", CIpAddr::m_Broad);

					CIpAddr Gate = pRoute->GetValueAsIp("gateway", CIpAddr::m_Empty);

					if( !Gate.IsEmpty() ) {

						m_pRouter->AddRoute(NOTHING, Dest, Mask, Gate);
					}

					continue;
				}

				break;
			}
		}

		m_pRouter->EnableRouting(pRouting->GetValueAsBool("enable", FALSE));
	}

	return TRUE;
}

BOOL CRlosNetApplicator::ApplyResolver(CJsonConfig *pResolver)
{
	if( CheckCRC(m_crcResolver, pResolver) ) {

		piob->RevokeSingleton("net.dns", 0);
	}

	if( pResolver ) {

		m_pResolver = New CDnsResolver(pResolver, 0);

		piob->RegisterSingleton("net.dns", 0, m_pResolver);
	}

	return TRUE;
}

BOOL CRlosNetApplicator::ApplyServices(CJsonConfig *pServices)
{
	if( pServices ) {

		ApplyNtpSync(pServices->GetChild("time.ntp"));

		ApplyLocation(pServices->GetChild("location"));

		ApplyFtpServer(pServices->GetChild("ftps"));

		ApplySmtpClient(pServices->GetChild("smtp"));
	}
	else {
		ApplyNtpSync(NULL);

		ApplyLocation(NULL);

		ApplyFtpServer(NULL);

		ApplySmtpClient(NULL);
	}

	return TRUE;
}

BOOL CRlosNetApplicator::ApplyNtpSync(CJsonConfig *pNtpSync)
{
	if( CheckCRC(m_crcNtpSync, pNtpSync) ) {

		if( m_pNtpSync ) {

			m_pNtpSync->Destroy();

			m_pNtpSync = NULL;
		}
	}

	if( pNtpSync ) {

		CNtpClient *pClient = New CNtpClient(pNtpSync, 0);

		m_pNtpSync = CreateClientThread(pClient, 0, 24000, NULL);
	}

	return TRUE;
}

BOOL CRlosNetApplicator::ApplySmtpClient(CJsonConfig *pSmtpClient)
{
	if( CheckCRC(m_crcSmtpClient, pSmtpClient) ) {

		if( m_pSmtpClient ) {

			m_pSmtpClient->Destroy();

			m_pSmtpClient = NULL;
		}
	}

	if( pSmtpClient ) {

		CSmtpClient *pClient = New CSmtpClient(pSmtpClient);

		m_pSmtpClient = CreateClientThread(pClient, 0, 24000, NULL);
	}

	return TRUE;
}

BOOL CRlosNetApplicator::ApplyLocation(CJsonConfig *pLocation)
{
	if( CheckCRC(m_crcLocation, pLocation) ) {

		if( m_pLocation ) {

			m_pLocation->Release();

			m_pLocation = NULL;
		}
	}

	if( pLocation ) {

		m_pLocation = New CLocation(pLocation);
	}

	return TRUE;
}

BOOL CRlosNetApplicator::ApplyFtpServer(CJsonConfig *pFtpServer)
{
	if( CheckCRC(m_crcFtpServer, pFtpServer) ) {

		if( m_pFtpServer ) {

			m_pFtpServer->Destroy();

			m_pFtpServer= NULL;
		}
	}

	if( pFtpServer ) {

		CFtpServer *pServer = New CFtpServer(pFtpServer);

		m_pFtpServer = CreateClientThread(pServer, 0, 24040, NULL);
	}

	return TRUE;
}

BOOL CRlosNetApplicator::ApplyIdent(CJsonConfig *pIdent)
{
	if( CheckCRC(m_crcIdent, pIdent) ) {

		if( m_pMulti1 ) {

			m_pMulti1->Destroy();

			m_pMulti1 = NULL;
		}

		if( m_pMulti2 ) {

			m_pMulti2->Destroy();

			m_pMulti2 = NULL;
		}

		if( m_pIdentifier ) {

			m_pIdentifier->Destroy();

			m_pIdentifier = NULL;
		}

		m_mDnsNames.Empty();
	}

	if( pIdent ) {

		bool    fUsed = false;

		CString Name1 = m_DefName;

		CString Name2 = pIdent->GetValue("name", "");

		if( pIdent->GetValueAsBool("llmnr", TRUE) ) {

			CMultiDns *pDns1 = New CMultiDns(CMultiDns::protocolLLMNR, Name1, Name2);

			m_pMulti1 = CreateClientThread(pDns1, 0, 24010, NULL);

			fUsed = true;
		}

		if( pIdent->GetValueAsBool("mdns", TRUE) ) {

			CMultiDns *pDns2 = New CMultiDns(CMultiDns::protocolMDNS, Name1, Name2);

			m_pMulti2 = CreateClientThread(pDns2, 0, 24020, NULL);

			fUsed = true;
		}

		if( pIdent->GetValueAsBool("identifier", TRUE) ) {

			CIdentifier *pClient = New CIdentifier(pIdent);

			m_pIdentifier = CreateClientThread(pClient, 0, 24030, NULL);
		}

		if( fUsed ) {

			if( !Name2.IsEmpty() ) {

				m_mDnsNames.Append(Name2);
			}

			if( !Name1.IsEmpty() ) {

				m_mDnsNames.Append(Name1);
			}
		}

		m_UnitName = Name2.IsEmpty() ? Name1 : Name2;
	}

	return TRUE;
}

BOOL CRlosNetApplicator::CheckCRC(DWORD &crc, CJsonConfig * &pJson)
{
	DWORD c = pJson ? pJson->GetCRC() : 1;

	if( crc ) {

		if( crc == c ) {

			pJson = NULL;

			return FALSE;
		}

		crc = c;

		return TRUE;
	}

	crc = c;

	return FALSE;
}

// End of File
