
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Entry Points
//

clink void AeonCmMain(void)
{
	extern IUnknown * Create_PxeObject(PCSTR pName);

	piob->RegisterInstantiator("app", Create_PxeObject);
}

clink void AeonCmExit(void)
{
	piob->RevokeInstantiator("app");
}

// End of File
