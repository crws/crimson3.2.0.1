
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SmtpSession_HPP

#define	INCLUDE_SmtpSession_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "StdClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

struct CSmtpConfig;

struct CSmtpInfo;

//////////////////////////////////////////////////////////////////////////
//
// SMTP Client Session
//

class CSmtpSession : public CStdClient
{
public:
	// Constructor
	CSmtpSession(CSmtpConfig const *pConfig);

	// Destructor
	~CSmtpSession(void);

	// Implementation
	BOOL SendMail(CMsgPayload const *pMessage, BOOL fKeep);

protected:
	// Data Members
	CSmtpConfig const * m_pConfig;
	CMsgPayload const * m_pMessage;
	UINT		    m_uState;
	BOOL		    m_fGone;
	UINT		    m_uAuthMethod;
	UINT		    m_uAuthState;
	CString		    m_Realm;
	CString		    m_Nonce;
	UINT		    m_NonceCount;
	CString             m_NonceC;
	BOOL		    m_fUseUTF8;
	BOOL		    m_fTls;

	// State Handlers
	void HandleConn(void);
	void HandleHelo(void);
	void HandleEhlo(void);
	void HandleQuit(void);
	void HandleOrig(void);
	void HandleRcpt(void);
	void HandleData1(void);
	void HandleData2(void);
	void HandleEOD(BOOL fKeep);
	void HandleAuth(void);

	// Commands
	BOOL SendHelo(PCTXT pDomain);
	BOOL SendEhlo(PCTXT pDomain);
	BOOL SendQuit(void);
	BOOL SendNoOp(void);
	BOOL SendMailFrom(PCTXT pPath);
	BOOL SendRcptTo(PCTXT pPath);
	BOOL SendData(void);
	BOOL SendEOD(void);
	BOOL SendRset(void);
	BOOL SendAuth(PCTXT pMethod);
	BOOL SendCmnd(PCTXT pCommand);
	BOOL SendStartTls(void);

	// Authentication
	BOOL TestEhloRep(void);
	void HandleAuthLogin(void);
	void HandleAuthDigest(void);
	void HandleTls(void);

	// Digest Authentication Method
	CString MakeDigestReq(void);
	BOOL    TestDigestRep(CString Reply);
	CString MakeNonce(void);
	CString MakeResponse(PCTXT pAuth, CString NonceC);

	// Login Authentication Method
	CString MakeLoginReq(void);
	BOOL    TestLoginRep(CString Reply);

	// MD5 Helpers
	CString MD5(CString Data);
	CString MD5(PCVOID pData, UINT uSize);
	void    MD5(CString Data, PBYTE pHash);
	void    MD5(PCVOID pData, UINT uSize, PBYTE pHash);
};

// End of File

#endif
