
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ModuleFirmware_HPP

#define INCLUDE_ModuleFirmware_HPP

//////////////////////////////////////////////////////////////////////////
//
// Module Firmware
//

class CModuleFirmware : public IExpansionFirmware
{
public:
	// Constructor
	CModuleFirmware(void);

	// Destructor
	~CModuleFirmware(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IExpansionFirmware
	BOOL   METHOD SetFirmwareLock(BOOL fSet);
	PCBYTE METHOD GetFirmwareGuid(UINT uFirm);
	UINT   METHOD GetFirmwareSize(UINT uFirm);
	PCBYTE METHOD GetFirmwareData(UINT uFirm);

protected:
	// Data Members
	ULONG m_uRefs;

	// Implementation
	BOOL FindData(PCBYTE &pData, UINT &uSize, UINT uFirm);
};

// End of File

#endif
