
#include "intern.hpp"

#include "NandFirmwareProps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Nand Firmware Properties Object
//

// Instantiator

global IFirmwareProps *	Create_NandFirmwareProps(UINT uStart, UINT uEnd)
{
	return New CNandFirmwareProps(uStart, uEnd);
}

// Magic Number

BYTE CNandFirmwareProps::m_bMagic[16] = { 0xFE, 0xF7, 0xC0, 0x95,
					  0xA1, 0x2B, 0x45, 0x08,
					  0xB2, 0x85, 0x12, 0x32,
					  0x9A, 0x2F, 0x45, 0xF2
};

// Constructor

CNandFirmwareProps::CNandFirmwareProps(UINT uStart, UINT uEnd)
{
	m_BlockStart = CNandBlock(0, uStart);

	m_BlockEnd   = CNandBlock(0, uEnd);

	CNandClient::Init();

	m_uAlloc = m_Geometry.m_uPages * m_uPageSize * (uEnd - uStart);

	m_pImage = NULL;

	m_uImage = 0;

	m_fValid = false;

	CheckCode();

	StdSetRef();
}

// Destructor

CNandFirmwareProps::~CNandFirmwareProps(void)
{
	delete[] m_pImage;
}

// IUnknown

HRESULT CNandFirmwareProps::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IFirmwareProps);

	StdQueryInterface(IFirmwareProps);

	return E_NOINTERFACE;
}

ULONG CNandFirmwareProps::AddRef(void)
{
	StdAddRef();
}

ULONG CNandFirmwareProps::Release(void)
{
	StdRelease();
}

// IFirmwareProps

bool CNandFirmwareProps::IsCodeValid(void)
{
	return m_fValid;
}

PCBYTE CNandFirmwareProps::GetCodeVersion(void)
{
	static BYTE bNull[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

	return m_fValid ? m_Head.m_bGuid : bNull;
}

UINT CNandFirmwareProps::GetCodeSize(void)
{
	if( m_fValid ) {

		if( ReadCode() ) {

			return m_uImage;
		}
	}

	return 0;
}

PCBYTE CNandFirmwareProps::GetCodeData(void)
{
	if( m_fValid ) {

		if( ReadCode() ) {

			return m_pImage;
		}
	}

	return 0;
}

// Implementation

bool CNandFirmwareProps::AllocImage(void)
{
	if( !m_pImage ) {

		m_pImage = New BYTE[m_uAlloc];

		if( m_pImage ) {

			return true;
		}
	}

	return false;
}

bool CNandFirmwareProps::ReadCode(void)
{
	if( !m_uImage ) {

		AllocImage();

		while( m_uImage < m_Head.m_uImage ) {

			if( ReadPage(m_PageCurrent) ) {

				memcpy(m_pImage + m_uImage, m_pPageData, m_uPageSize);

				m_uImage += m_uPageSize;

				GetNextGoodPage(m_PageCurrent);

				continue;
			}

			return false;
		}

		m_uImage = m_Head.m_uImage;

		return true;
	}

	return true;
}

void CNandFirmwareProps::CheckCode(void)
{
	CNandBlock Block = m_BlockStart;

	SkipBadBlocks(Block);

	m_PageCurrent = Block;

	if( ReadPage(m_PageCurrent) ) {

		CHead *pHead = (CHead *) m_pPageData;

		if( !memcmp(pHead->m_bMagic, m_bMagic, 16) ) {

			m_Head   = *pHead;

			m_fValid = true;

			m_PageCurrent.m_uPage++;

			return;
		}

		m_fValid = false;

		return;
	}

	m_fValid = false;

	EraseFirst(true);
}

// End of File
