
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinuxPacketCapture_HPP

#define INCLUDE_LinuxPacketCapture_HPP

//////////////////////////////////////////////////////////////////////////
//
// Linux Packet Capture
//

class CLinuxPacketCapture :
	public IPacketCapture
{
public:
	// Constructor
	CLinuxPacketCapture(PCTXT pName, UINT uInst, UINT uMode);

	// Destructor
	~CLinuxPacketCapture(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IPacketCapture
	BOOL  METHOD IsCaptureRunning(void);
	PCSTR METHOD GetCaptureFilter(void);
	UINT  METHOD GetCaptureSize(void);
	BOOL  METHOD CopyCapture(PBYTE pData, UINT uSize);
	BOOL  METHOD StartCapture(PCSTR pFilter);
	void  METHOD StopCapture(void);
	void  METHOD KillCapture(void);

protected:
	// Data Members
	ULONG		m_uRefs;
	CString		m_Name;
	UINT		m_uInst;
	UINT		m_uMode;
	ILinuxSupport *	m_pLinux;
	CString		m_File;
	CString		m_Filter;
	int		m_pid;
};

// End of File

#endif
