
#include "Intern.hpp"

#include "RlosHardwareApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// USB Identifiers
//

#include "..\..\..\HXLs\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

#include "ModuleFirmware.hpp"

#include "UsbModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RLOS Hardware Applicator
//

// Instantiator

IHardwareApplicator * Create_RlosHardwareApplicator(void)
{
	return new CRlosHardwareApplicator;
}

// Constructor

CRlosHardwareApplicator::CRlosHardwareApplicator(void)
{
	m_pModel    = NULL;

	m_fReboot   = FALSE;

	m_crcConfig = 0;

	m_pPlatform = NULL;

	m_pStack    = NULL;

	m_pExpand   = NULL;

	m_pFirmware = New CModuleFirmware;

	m_fExtModem = FALSE;

	m_uExtModem = 0;

	AfxGetObject("c3.model", 0, IPxeModel, m_pModel);

	AfxGetObject("platform", 0, IPlatform, m_pPlatform);

	LoadModuleMaps();

	AfxGetObject("usb.host", 0, IUsbHostStack, m_pStack);

	AfxGetObject("usb.rack", 0, IExpansionSystem, m_pExpand);

	if( m_pStack ) {

		m_pStack->Init();

		m_pStack->Attach(this);
	}

	if( m_pExpand ) {

		m_pExpand->Attach(m_pFirmware);
	}

	StdSetRef();
}

// Destructor

CRlosHardwareApplicator::~CRlosHardwareApplicator(void)
{
	AfxRelease(m_pStack);

	AfxRelease(m_pExpand);

	AfxRelease(m_pPlatform);

	AfxRelease(m_pModel);

	delete m_pFirmware;
}

// IUnknown

HRESULT CRlosHardwareApplicator::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IHardwareApplicator);

	StdQueryInterface(IHardwareApplicator);

	StdQueryInterface(IUsbSystem);

	return E_NOINTERFACE;
}

ULONG CRlosHardwareApplicator::AddRef(void)
{
	StdAddRef();
}

ULONG CRlosHardwareApplicator::Release(void)
{
	StdRelease();
}

// IHardwareApplicator

bool CRlosHardwareApplicator::ApplySettings(CJsonConfig *pJson)
{
	if( CheckCRC(m_crcConfig, pJson) ) {

		if( m_fReboot ) {

			return false;
		}

		piob->RevokeGroup("pnp.");

		m_NicSlots.Empty();

		m_ModemSlots.Empty();

		m_ComSlots.Empty();

		m_IoSlots.Empty();
	}

	if( pJson ) {

		m_uNicMask = 0;

		RegisterNics();

		ApplyExtModem(pJson->GetChild("modem"));

		RegisterPorts();

		ApplyExpansion(pJson->GetChild("exp"));

		if( m_pStack ) {

			if( 1 || m_Modules.GetCount() ) {

				m_fReboot = TRUE;
			}

			m_pStack->Start();
		}
	}

	return true;
}

// IUsbSystem

void CRlosHardwareApplicator::OnDeviceArrival(IUsbHostFuncDriver *pDriver)
{
	IUsbDevice *pDevice;

	pDriver->GetDevice(pDevice);

	if( pDevice ) {

		DWORD tree = pDevice->GetTreePath().dw;

		INDEX find = m_PathToSlot.FindName(tree);

		UINT  susb = m_pExpand->FindSlot(pDevice->GetTreePath());

		if( find != m_PathToSlot.Failed() ) {

			CSlot slot = m_PathToSlot.GetData(find);

			DWORD ids  = MAKELONG(pDriver->GetVendor(), pDriver->GetProduct());

			UINT  mod  = m_IdsToMod[ids];

			INDEX find = m_Modules.FindName(slot.dw);

			if( !m_Modules.Failed(find) ) {

				UINT exp = m_Modules.GetData(find);

				if( mod == exp + 500 ) {

					AfxTrace("Correct loader %u in %8.8X (%u)\n", mod, slot.dw, susb);

					return;
				}

				if( mod == exp ) {

					AfxTrace("Correct module %u in %8.8X (%u)\n", mod, slot.dw, susb);

					switch( mod ) {

						case 100:
						case 101:
						case 102:
						case 103:
						case 104:
						{
							PCTXT pName = (mod == 100) ? "pnp.pnp-modem" : "pnp.pnp-uart";
							
							UINT  uInst = (mod == 100) ? m_ModemSlots[slot.dw] : m_ComSlots[slot.dw];

							AfxGetAutoObject(pFind, pName, uInst, IPortObject);

							if( !pFind ) {

								IUnknown *pUnk = (IUnknown *) m_pExpand->GetSysObject(susb, 0);

								if( pUnk ) {

									IPortObject *pPort = NULL;

									if( pUnk->QueryInterface(AfxAeonIID(IPortObject), (void **) &pPort) == S_OK ) {

										if( piob->RegisterSingleton(pName, uInst, pPort) == S_OK ) {

											pPort->AddRef();
										}

										AfxRelease(pPort);
									}
								}
							}
						}
						break;
						
						case 200:
						case 201:
						case 202:
						case 203:
						case 204:
						case 205:
						case 206:
						case 207:
						case 208:
						case 209:
						{
							UINT uInst = m_IoSlots[slot.dw];

							AfxGetAutoObject(pFind, "pnp.pnp-io", uInst, IUsbModule);

							if( !pFind ) {

								CUsbModule *pModule = New CUsbModule(tree);

								if( piob->RegisterSingleton("pnp.pnp-io", uInst, pModule) == S_OK ) {

									pModule->AddRef();
								}

								AfxRelease(pModule);
							}
						}
						break;

						default:
						{
						}
						break;
					}

					return;
				}

				AfxTrace("Wrong module %u vs. %u in %8.8X (%u)\n", mod, exp, slot.dw, susb);

				return;
			}

			AfxTrace("Unknown module in %8.8X (%u)\n", slot.dw, susb);

			return;
		}

		if( pDriver->GetVendor() == vidSmc && pDriver->GetProduct() == pidSmcLan9512 ) {

			RegisterNics();
		}

		if( FALSE ) {

			UsbTreePath const &t = pDevice->GetTreePath();

			AfxTrace("Other USB device in %u { %u, %u, %u, %u, %u, %u, %u, %u, %u }\n",
				 susb,
				 t.a.dwCtrl,
				 t.a.dwHost,
				 t.a.dwTier,
				 t.a.dwPort1,
				 t.a.dwPort2,
				 t.a.dwPort3,
				 t.a.dwPort4,
				 t.a.dwPort5,
				 t.a.dwPort6
			);
		}
	}
}

void CRlosHardwareApplicator::OnDeviceRemoval(IUsbHostFuncDriver *pDriver)
{
}

// Implementation

void CRlosHardwareApplicator::RegisterNics(void)
{
	UINT ne = m_pModel->GetObjCount('e');

	for( UINT n = 0; n < ne; n++ ) {

		UINT uMask = (1 << n);

		if( !(m_uNicMask & uMask) ) {

			AfxGetAutoObject(pNic, "nic", n, INic);

			if( pNic ) {

				if( piob->RegisterSingleton("pnp.pnp-nic", n, pNic) == S_OK ) {

					pNic.TakeOver();
				}

				m_uNicMask |= uMask;
			}
		}
	}
}

void CRlosHardwareApplicator::RegisterPorts(void)
{
	UINT np = m_pModel->GetObjCount('p');

	for( UINT n = 0; n < np; n++ ) {

		if( m_fExtModem && m_uExtModem == n ) {

			continue;
		}

		AfxGetAutoObject(pPort, "uart", n, IPortObject);

		if( pPort ) {

			if( piob->RegisterSingleton("pnp.pnp-uart", n, pPort) == S_OK ) {

				pPort.TakeOver();
			}
		}

		CSlot slot(3, 0, n);

		m_ComSlots.Insert(slot.dw, n);
	}
}

void CRlosHardwareApplicator::LoadModuleMaps(void)
{
	static UINT d[][3] = {

		{	100,	vidTelit,	pidTelitHspa	},
		{	101,	vidRedLion,	pidCan		},
		{	601,	vidRedLion,	pidCanBoot	},
		{	102,	vidRedLion,	pidJ1939	},
		{	602,	vidRedLion,	pidJ1939Boot	},
		{	103,	vidRedLion,	pidDevNet	},
		{	603,	vidRedLion,	pidDevNetBoot	},
		{	104,	vidRedLion,	pidProfibus	},
		{	604,	vidRedLion,	pidProfibusBoot	},
		{	105,	vidRedLion,	pidDnp3Dongle	},
		{	200,	vidRedLion,	pidDio14	},
		{	201,	vidRedLion,	pidIni8		},
		{	202,	vidRedLion,	pidInv8		},
		{	203,	vidRedLion,	pidOut4		},
		{	204,	vidRedLion,	pidPid1		},
		{	205,	vidRedLion,	pidPid2		},
		{	206,	vidRedLion,	pidRtd6		},
		{	207,	vidRedLion,	pidTc8		},
		{	208,	vidRedLion,	pidUin4		},
		{	209,	vidRedLion,	pidSg1		},

	};

	for( UINT n = 0; n < elements(d); n++ ) {

		UINT  mod = d[n][0];

		DWORD ids = MAKELONG(d[n][1], d[n][2]);

		m_ModToIds.Insert(mod, ids);

		m_IdsToMod.Insert(ids, mod);
	}
}

void CRlosHardwareApplicator::LoadSlotMaps(UINT tcount, UINT tport, UINT fcount)
{
	UINT nb = m_pModel->GetObjCount('b');

	LoadBaseSlotMap(nb);

	LoadTetheredSlotMap(tcount, tport);

	LoadFixedSlotMap(fcount);
}

BOOL CRlosHardwareApplicator::ApplyExpansion(CJsonConfig *pJson)
{
	m_SlotToPath.Empty();

	m_PathToSlot.Empty();

	m_Modules.Empty();

	if( pJson ) {

		UINT nt = m_pModel->GetObjCount('t');

		UINT nf = m_pModel->GetObjCount('f');

		UINT tcount = pJson->GetValueAsUInt("tcount", 0, 0, nt);

		UINT tport  = pJson->GetValueAsUInt("tport", 0, 0, 1);

		UINT fcount = pJson->GetValueAsUInt("fcount", 0, 0, nf);

		LoadSlotMaps(tcount, tport, fcount);

		ApplyModules(0, 0, pJson->GetChild("base"));

		ApplyRackModules(tcount, 1, pJson->GetChild("tethered"));

		ApplyRackModules(fcount, 2, pJson->GetChild("fixed"));

		return TRUE;
	}

	return FALSE;
}

BOOL CRlosHardwareApplicator::ApplyRackModules(UINT uCount, UINT uType, CJsonConfig *pJson)
{
	if( pJson ) {

		for( UINT uRack = 0; uRack < uCount; uRack++ ) {

			CJsonConfig *pRack = pJson->GetChild(CPrintf("rack%u", 1+uRack));

			ApplyModules(uType, uRack, pRack);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CRlosHardwareApplicator::ApplyModules(UINT uType, UINT uUnit, CJsonConfig *pJson)
{
	if( pJson ) {

		CJsonConfig *pModules = pJson->GetChild("modules");

		for( UINT uSlot = 0; uSlot < pModules->GetCount(); uSlot++ ) {

			CJsonConfig *pModule = pModules->GetChild(uSlot);

			if( pModule ) {

				UINT mod = pModule->GetValueAsUInt("type", 0, 0, 300);

				if( mod ) {

					CSlot slot = CSlot(uType, uUnit, uSlot);

					switch( mod ) {

						case 100:
						{
							m_ModemSlots.Insert(slot.dw, m_ModemSlots.GetCount());
						}
						break;

						case 101:
						case 102:
						case 103:
						case 104:
						{
							m_ComSlots.Insert(slot.dw, m_ComSlots.GetCount());
						}
						break;

						case 200:
						case 201:
						case 202:
						case 203:
						case 204:
						case 205:
						case 206:
						case 207:
						case 208:
						case 209:
						{
							m_IoSlots.Insert(slot.dw, m_IoSlots.GetCount());
						}
						break;

					}

					m_Modules.Insert(slot.dw, mod);
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CRlosHardwareApplicator::ApplyExtModem(CJsonConfig *pJson)
{
	if( pJson ) {

		if( pJson->GetValueAsBool("enable", FALSE) ) {

			m_fExtModem = TRUE;

			m_uExtModem = pJson->GetValueAsUInt("port", 0, 0, 2);

			AfxGetAutoObject(pPort, "uart", m_uExtModem, IPortObject);

			if( pPort ) {

				UINT uInst = m_ModemSlots.GetCount();

				if( piob->RegisterSingleton("pnp.pnp-modem", uInst, pPort) == S_OK ) {

					pPort.TakeOver();

					CSlot slot = CSlot(3, 0, 0);

					m_ModemSlots.Insert(slot.dw, uInst);
				}
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CRlosHardwareApplicator::CheckCRC(DWORD &crc, CJsonConfig * &pJson)
{
	DWORD c = pJson ? pJson->GetCRC() : 1;

	if( crc ) {

		if( crc == c ) {

			pJson = NULL;

			return FALSE;
		}

		crc = c;

		return TRUE;
	}

	crc = c;

	return FALSE;
}

// Model Specific

void CRlosHardwareApplicator::LoadBaseSlotMap(UINT uCount)
{
	PCDWORD p = m_pModel->GetUsbPaths('b');

	for( UINT n = 0; n < uCount; n++ ) {

		UsbTreePath path;

		if( (path.dw = p[n]) ) {

			CSlot s(0, 0, n);

			m_SlotToPath.Insert(s.dw, path.dw);

			m_PathToSlot.Insert(path.dw, s.dw);

			continue;
		}

		break;
	}
}

void CRlosHardwareApplicator::LoadTetheredSlotMap(UINT uCount, UINT uPort)
{
	PCDWORD p = m_pModel->GetUsbPaths('t');

	UINT    i = 0;

	for( UINT r = 0; r < uCount; r++ ) {

		if( p[i] ) {

			for( UINT n = 0;; n++ ) {

				UsbTreePath path;

				if( (path.dw = p[i++]) ) {

					// TODO -- Is this true for all models???!!!

					path.a.dwPort2 = 2 + uPort;

					CSlot s(1, r, n);

					m_SlotToPath.Insert(s.dw, path.dw);

					m_PathToSlot.Insert(path.dw, s.dw);

					continue;
				}

				break;
			}

			continue;
		}

		break;
	}
}

void CRlosHardwareApplicator::LoadFixedSlotMap(UINT uCount)
{
	PCDWORD p = m_pModel->GetUsbPaths('f');

	UINT    i = 0;

	for( UINT r = 0; r < uCount; r++ ) {

		if( p[i] ) {

			for( UINT n = 0;; n++ ) {

				UsbTreePath path;

				if( (path.dw = p[i++]) ) {

					CSlot s(2, r, n);

					m_SlotToPath.Insert(s.dw, path.dw);

					m_PathToSlot.Insert(path.dw, s.dw);

					continue;
				}

				break;
			}

			continue;
		}

		break;
	}
}

// End of File
