
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LoadCounter_HPP

#define INCLUDE_LoadCounter_HPP

//////////////////////////////////////////////////////////////////////////
//
// Loader Counter Object
//

class CLoadCounter
{
public:
	// Constructor
	CLoadCounter(void);

	// Destructor
	~CLoadCounter(void);

	// Assignment Operator
	CLoadCounter const & operator = (BYTE b);

	// Arithmetic Operators
	CLoadCounter const & operator += (BYTE b);
	CLoadCounter const & operator -= (BYTE b);

	// Comparison Operator
	BOOL operator >= (BYTE) const;

	// Operations
	void Reset(void);
	void SetFlag(BOOL fState);

	// Attributes
	BYTE GetData(void) const;
	BOOL GetFlag(void);

protected:
	// Data Members
	ISerialMemory * m_pFram;
	BYTE	        m_bData[2];

	// Implementation
	void Load(void);
	void Save(void);
};

// End of File

#endif
