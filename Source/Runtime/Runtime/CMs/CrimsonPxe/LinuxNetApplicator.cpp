
#include "Intern.hpp"

#include "LinuxNetApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"
#include "LinuxCellStatus.hpp"
#include "LinuxWiFiStatus.hpp"
#include "LinuxEthernetStatus.hpp"
#include "LinuxPacketCapture.hpp"
#include "CertManager.hpp"
#include "MultiDns.hpp"
#include "DnsResolver.hpp"
#include "ZoneSync.hpp"
#include "NtpClient.hpp"
#include "CellTimeClient.hpp"
#include "GpsTimeClient.hpp"
#include "SmtpClient.hpp"
#include "LinuxSmsClient.hpp"
#include "Location.hpp"
#include "Identifier.hpp"
#include "FtpServer.hpp"
#include "SvmClient.hpp"
#include "IoMonitor.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Linux Net Applicator
//

// Instantiator

INetApplicator * Create_LinuxNetApplicator(void)
{
	return new CLinuxNetApplicator;
}

// Constructor

CLinuxNetApplicator::CLinuxNetApplicator(void)
{
	m_pUtils          = NULL;
	m_crcCertManager  = 0;
	m_crcIdent        = 0;
	m_crcZoneSync     = 0;
	m_crcNtpSync      = 0;
	m_crcCellSync     = 0;
	m_crcGpsSync      = 0;
	m_crcSmtpClient   = 0;
	m_crcSmsClient    = 0;
	m_crcLocation     = 0;
	m_crcFtpServer    = 0;
	m_crcSvmClient    = 0;
	m_crcIoMonitor    = 0;
	m_fDnsProxy       = TRUE;
	m_fNtpProxy       = TRUE;
	m_pRoutesStatic   = NULL;
	m_pRoutesOutbound = NULL;
	m_pRoutesInbound  = NULL;
	m_pCertManager    = NULL;
	m_pMulti1         = NULL;
	m_pMulti2         = NULL;
	m_pIdentifier     = NULL;
	m_pZoneSync       = NULL;
	m_pNtpSync        = NULL;
	m_pCellSync       = NULL;
	m_pGpsSync        = NULL;
	m_pSmtpClient     = NULL;
	m_pSmsClient      = NULL;
	m_pLocation       = NULL;
	m_pFtpServer      = NULL;
	m_pSvmClient	  = NULL;
	m_pIoMonitor      = NULL;

	AfxGetObject("ip", 0, INetUtilities, m_pUtils);

	AfxGetAutoObject(pPlatform, "platform", 0, IPlatform);

	m_DefName = pPlatform->GetDefName();

	StdSetRef();
}

// Destructor

CLinuxNetApplicator::~CLinuxNetApplicator(void)
{
	for( UINT n = 0; n < m_FaceStatus.GetCount(); n++ ) {

		AfxRelease(m_FaceStatus[n]);
	}

	AfxRelease(m_pUtils);
}

// IUnknown

HRESULT CLinuxNetApplicator::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(INetApplicator);

	StdQueryInterface(INetApplicator);

	return E_NOINTERFACE;
}

ULONG CLinuxNetApplicator::AddRef(void)
{
	StdAddRef();
}

ULONG CLinuxNetApplicator::Release(void)
{
	StdRelease();
}

// INetApplicator

bool CLinuxNetApplicator::ApplySettings(CJsonConfig *pJson)
{
	if( pJson ) {

		WaitForStartup();

		ClearConfigData();

		ApplyCertManager(pJson->GetChild("security"));

		ApplyNetwork(pJson);

		ApplyIdent(pJson->GetChild("net.ident"));

		ApplyServices(pJson->GetChild("services"));

		return true;
	}
	else {
		ApplyIdent(NULL);

		ApplyCertManager(NULL);

		ApplyServices(NULL);

		return true;
	}

	return false;
}

bool CLinuxNetApplicator::GetUnitName(CString &Name)
{
	Name = m_UnitName;

	return true;
}

bool CLinuxNetApplicator::GetmDnsNames(CStringArray &List)
{
	List = m_mDnsNames;

	return true;
}

bool CLinuxNetApplicator::GetInfo(CString &Info, PCSTR pName)
{
	CString Name(pName);

	if( Name.StartsWith("network-") ) {

		if( Name.Mid(8, 1) == "s" ) {

			AppendCommand(Info, NULL, "/sbin/ifconfig", "");
		}

		if( Name.Mid(8, 1) == "c" ) {

			CString Path("/./vap/opt/crimson/config/net/");

			AppendFile(Info, "Startup Script 1", Path + "init1");

			AppendFile(Info, "Startup Script 2", Path + "init2");

			AppendFile(Info, "Shutdown Script 1", Path + "term1");

			AppendFile(Info, "Shutdown Script 2", Path + "term2");
		}
	}

	if( Name.StartsWith("firewall-") ) {

		if( Name.Mid(9, 1) == "s" ) {

			AppendCommand(Info, "Filter Chains", "/sbin/iptables", "-t filter --list -v -n");

			AppendCommand(Info, "NAT Chains", "/sbin/iptables", "-t nat --list -v -n");

			AppendCommand(Info, "Mangle Chains", "/sbin/iptables", "-t mangle --list -v -n");
		}

		if( Name.Mid(9, 1) == "c" ) {

			CString Path("/./vap/opt/crimson/config/firewall/");

			AppendFile(Info, "Startup Script", Path + "init");

			AppendFile(Info, "Shutdown Script", Path + "term");
		}
	}

	if( Name.StartsWith("tlss-") || Name.StartsWith("tlsc-") ) {

		if( Name.Mid(5, 1) == "c" ) {

			CPrintf Path("/./vap/opt/crimson/config/%s/", PCSTR(Name.Left(4)));

			AppendFile(Info, "Startup Script", Path + "init");

			AppendFile(Info, "Shutdown Script", Path + "term");

			AppendFile(Info, "Configuration", Path + "tls.conf");
		}
	}

	if( Name.StartsWith("routes-") ) {

		if( Name.Mid(7, 1) == "s" ) {

			AppendCommand(Info, NULL, "/sbin/route", "-n");
		}

		if( Name.Mid(7, 1) == "c" ) {

			CString Path("/./vap/opt/crimson/config/routes/");

			AppendFile(Info, "Startup Script", Path + "init");

			AppendFile(Info, "Shutdown Script", Path + "term");

			if( !Info.IsEmpty() ) {

				Info += "</br><i>(Other routes may be added by interface scripts.)</i>";
			}
		}
	}

	if( Name.StartsWith("dns-") ) {

		if( Name.Mid(4, 1) == "s" ) {

			AppendCommand(Info, NULL, "/bin/sh", "/opt/crimson/scripts/c3-show-dns");

			AppendFile(Info, "Current Hosts", "/./etc/hosts");
		}

		if( Name.Mid(4, 1) == "c" ) {

			CString Path("/./vap/opt/crimson/config/dns/");

			AppendFile(Info, "DNS Configuration", Path + "dns.conf");

			AppendFile(Info, "Local Hosts", Path + "hosts-local");

			AppendFile(Info, "Global Hosts", Path + "hosts-global");
		}
	}

	if( Name.StartsWith("face-") ) {

		if( Name.Mid(5, 2) == "s-" ) {

			CString Face(Name.Mid(7));

			CString Used(Face);

			AppendCommand(Info, "Status", "/sbin/ifconfig", Face);

			if( Face.StartsWith("ipsec") ) {

				AppendCommand(Info, "IPsec Status", "/bin/sh", "-c /opt/crimson/scripts/c3-show-ipsec " + Face);
			}

			if( Face.StartsWith("wlan") ) {

				AppendFile(Info, "Wi-Fi Status", CPrintf("/./tmp/crimson/face/%s/status", PCSTR(Face)));
			}

			if( Face.StartsWith("tun") ) {

				AppendFile(Info, "Tunnel Status", CPrintf("/./tmp/crimson/face/%s/status", PCSTR(Face)));
			}

			if( Face.StartsWith("tap") ) {

				AppendFile(Info, "Tunnel Status", CPrintf("/./tmp/crimson/face/%s/status", PCSTR(Face)));
			}

			if( Face.StartsWith("wwan") ) {

				AppendFile(Info, "Cellular", CPrintf("/./tmp/crimson/face/%s/status", PCSTR(Face)));

				AppendFile(Info, "Location", CPrintf("/./tmp/crimson/face/%s/location", PCSTR(Face)));

				AppendFile(Info, "Time", CPrintf("/./tmp/crimson/face/%s/time", PCSTR(Face)));
			}

			UINT face = GetFaceId(Face);

			UINT br   = FindBridge(face);

			UINT priv = HasPrivateRoutes(face);

			if( br ) {

				Used.Printf("br%u", br);

				AppendCommand(Info, NULL, "/sbin/ifconfig", Used);
			}

			if( priv ) {

				AppendCommand(Info, "Inbound Routes", "/sbin/ip", "route show table c3-" + Face + "-in");

				AppendCommand(Info, "Outbound Routes", "/sbin/ip", "route show table c3-" + Face + "-out");
			}

			AppendCommand(Info, "Global Routes", "/sbin/ip", "route show dev " + Used);

			AppendCommand(Info, "DHCP Leases", "/bin/sh", "/opt/crimson/scripts/c3-show-leases " + Face);
		}

		if( Name.Mid(5, 2) == "c-" ) {

			CString Face(Name.Mid(7));

			CString Used(Face);

			UINT    br = FindBridge(GetFaceId(Face));

			if( br ) {

				Used.Printf("br%u", br);
			}

			CString Path("/./vap/opt/crimson/config/net/");

			AppendFile(Info, "Bring-Up Script", Path + Face + ".up");

			AppendFile(Info, "Take-Down Script", Path + Face + ".dn");

			AppendFile(Info, "On Start Script", Path + Face + ".start");

			AppendFile(Info, "On Stop Script", Path + Face + ".stop");

			AppendFile(Info, "On Up Script", Path + Face + ".tunup");

			AppendFile(Info, "On Down Script", Path + Face + ".tundn");

			AppendFile(Info, "On Make Script", Path + Face + ".make");

			AppendFile(Info, "On Break Script", Path + Face + ".break");

			AppendFile(Info, "Configuration", Path + Face + "-conf");

			if( !Face.StartsWith("ipsec") ) {

				AppendCommand(Info, "Firewall Rules", "/bin/sh", "/opt/crimson/scripts/c3-show-rules " + Used);

				AppendCommand(Info, "MAC Filtering", "/bin/sh", "/opt/crimson/scripts/c3-show-filters " + Used);

				AppendFile(Info, "DHCP Server", Path + Face + "-dhcp-conf");
			}
		}
	}

	return !Info.IsEmpty();
}

// Interface Naming

UINT CLinuxNetApplicator::GetFaceId(CString const &Face)
{
	if( Face.StartsWith("br") ) {

		if( Face.GetLength() == 3 ) {

			// brX

			UINT n = Face[2] - '0';

			if( n >= 0 && n < m_BridgeRev.GetCount() ) {

				return m_BridgeRev[n];
			}
		}
	}

	if( Face.StartsWith("eth") ) {

		if( Face.GetLength() == 4 ) {

			// ethX

			UINT n = Face[3] - '0';

			if( n >= 0 && n <= 1 ) {

				return 1000 + n * 100;
			}
		}

		if( Face.GetLength() == 5 ) {

			// ethXv

			UINT n = Face[3] - '0';

			UINT v = tolower(Face[4]) - 'a' + 1;

			if( n >= 0 && n <= 1 ) {

				if( v >= 1 && v <= 26 ) {

					return 1000 + n * 100 + v;
				}
			}
		}

		if( Face.GetLength() == 6 ) {

			// ethXsY

			UINT p = Face[3] - '0';

			UINT s = Face[5] - '0';

			if( p >= 0 && p <= 1 ) {

				if( s >= 1 && s <= 3 ) {

					int n = p + 2 * (s-1);

					return 2000 + n * 100;
				}
			}
		}

		if( Face.GetLength() == 7 ) {

			// ethXsYv

			UINT p = Face[3] - '0';

			UINT s = Face[5] - '0';

			UINT v = tolower(Face[6]) - 'a' + 1;

			if( p >= 0 && p <= 1 ) {

				if( s >= 1 && s <= 3 ) {

					if( v >= 1 && v <= 26 ) {

						int n = p + 2 * (s-1);

						return 2000 + n * 100 + v;
					}
				}
			}
		}
	}

	if( Face.StartsWith("wwan") ) {

		if( Face.GetLength() == 5 ) {

			UINT n = Face[4] - '0';

			if( n >= 0 && n <= 2 ) {

				return 3000 + n;
			}
		}
	}

	if( Face.StartsWith("wlan0s") ) {

		if( Face.GetLength() == 7 ) {

			UINT s = Face[6] - '0';

			if( s >= 1 && s <= 3 ) {

				int n = (s-1);

				return 4000 + n;
			}
		}
	}


	if( Face.StartsWith("grex") ) {

		if( Face.GetLength() == 5 ) {

			UINT n = Face[4] - '0';

			if( n >= 0 && n <= 9 ) {

				return 5000 + n;
			}
		}
	}

	if( Face.StartsWith("tun") ) {

		if( Face.GetLength() == 4 ) {

			UINT n = Face[3] - '0';

			if( n >= 0 && n <= 9 ) {

				return 5300 + n;
			}
		}
	}

	if( Face.StartsWith("tap") ) {

		if( Face.GetLength() == 4 ) {

			UINT n = Face[3] - '0';

			if( n >= 0 && n <= 9 ) {

				return 5400 + n;
			}
		}
	}

	return NOTHING;
}

CString CLinuxNetApplicator::GetFaceName(UINT id)
{
	INDEX i = m_BridgeMap.FindName(id);

	if( !m_BridgeMap.Failed(i) ) {

		UINT br = m_BridgeMap.GetData(i);

		return CPrintf("br%u", br);
	}

	switch( id / 1000 ) {

		case 1:
		{
			// ethX and VLANs

			UINT n = (id % 1000) / 100;

			UINT v = (id % 1000) % 100;

			if( n < 2 && v < 26 ) {

				return CPrintf(v ? "eth%u%c" : "eth%u", n, 'a'+v-1);
			}
		}

		case 2:
		{
			// ethXsY and VLANs

			UINT n = (id % 1000) / 100;

			UINT p = n % 2;

			UINT s = n / 2;

			UINT v = (id % 1000) % 100;

			if( s < 3 && v < 26 ) {

				return CPrintf(v ? "eth%us%u%c" : "eth%us%u", p, s+1, 'a'+v-1);
			}
		}

		case 3:
		{
			// wwanX

			UINT s = id % 1000;

			if( s < 3 ) {

				return CPrintf("wwan%u", s);
			}
		}

		case 4:
		{
			// wlan0sX

			UINT s = id % 1000;

			if( s < 3 ) {

				return CPrintf("wlan0s%u", s+1);
			}
		}

		case 5:
		{
			// Tunnels

			switch( (id % 1000) / 100 ) {

				case 0:
				{
					// grexX

					UINT n = id % 100;

					if( n < 10 ) {

						return CPrintf("grex%u", n);
					}
				}
				break;

				case 3:
				{
					// tunX

					UINT n = id % 100;

					if( n < 10 ) {

						return CPrintf("tun%u", n);
					}
				}
				break;

				case 4:
				{
					// tapX

					UINT n = id % 100;

					if( n < 10 ) {

						return CPrintf("tap%u", n);
					}
				}
				break;
			}
		}
	}

	return "";
}

// Physical Interfaces

BOOL CLinuxNetApplicator::ApplyNetwork(CJsonConfig *pJson)
{
	if( pJson ) {

		if( false ) {

			AfxGetAutoObject(pProps, "c3.firmprops", 0, IFirmwareProps);

			if( pProps ) {

				CGuid guid;

				memcpy(&guid, pProps->GetCodeVersion(), sizeof(guid));

				AppendConfig(m_Init1, "#crc %s", PCSTR(guid.GetAsText()));
			}
		}

		m_Init1.Append("ip link set dev lo up");

		m_Init1.Append("rm -rf /tmp/crimson/up");

		m_Init1.Append("mkdir -p /tmp/crimson/up");

		m_Init1.Append("mkdir -p /tmp/crimson/dns");

		m_Init1.Append("echo -n > /tmp/crimson/dns/dhcp.conf");

		m_Init1.Append("echo -n > /tmp/crimson/dns/hosts");

		CJsonConfig *pFaces = pJson->GetChild("net.faces");

		CJsonConfig *pTuns  = pJson->GetChild("net.tunnels");

		ScanFacesForBridges(pFaces);

		ScanTunnelsForBridges(pTuns);

		ApplyMacFilter();

		ApplyFirewall(pJson->GetChild("net.firewall"));

		ApplyRouting(pJson->GetChild("net.routing"));

		ApplyServiceDhcpRelay(pJson->GetChild("services.dhcpRelay"));

		ApplyServiceDhcpServer(pJson->GetChild("services.dhcpServer"));

		ApplyFaces(pFaces, pTuns);

		m_FireHead.Append(m_FireDrop);

		m_FireHead.Append(m_FireInit);

		m_FireHead.Append(m_FireLast);

		SaveScript("firewall", "init", m_FireHead);

		SaveScript("firewall", "term", m_FireTerm);

		SaveScript("macfilt", "init", m_FiltInit);

		SaveScript("macfilt", "term", m_FiltTerm);

		ApplyResolver(pJson->GetChild("net.resolver"));

		ApplyTlsServer(pJson->GetChild("services.tlsServer"));

		ApplyTlsClient(pJson->GetChild("services.tlsClient"));

		ApplySysLog(pJson->GetChild("services.syslog"));

		ApplySnmp(pJson->GetChild("services.snmp"));

		RunCommand("/bin/sh", "/opt/crimson/scripts/c3-cycle", NULL);

		CUIntArray Ids;

		m_pUtils->SetInterfaceNames(m_FaceNames);

		m_pUtils->SetInterfaceDescs(m_FaceDescs);

		m_pUtils->SetInterfacePaths(m_FacePaths);

		for( UINT n = 0; n < m_FaceNames.GetCount(); n++ ) {

			UINT id = GetFaceId(m_FaceNames[n]);

			Ids.Append(id);
		}

		m_pUtils->SetInterfaceIds(Ids);

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyFaces(CJsonConfig *pFaces, CJsonConfig *pTuns)
{
	m_DhcpClients.Empty();

	m_Sharers.Empty();

	if( pFaces ) {

		FindSharingProviders(pFaces);

		CJsonData *pData = pFaces->GetJsonData();

		PCSTR      pPath = "0.0.1.%u";

		UINT       uPath = 0;

		CFaceInfo  InfoEth, InfoCell, InfoWiFi;

		for( UINT n = 0; n < 2; n++ ) {

			CPrintf Name("eth%u", n);

			InfoEth.Load(pFaces->GetChild(Name), Name, pPath, uPath);

			InfoEth.StepMajor(ApplyBaseEthernet(InfoEth));
		}

		for( UINT p = 0; p < 3; p++ ) {

			for( INDEX i = pData->GetHead(); !pData->Failed(i); pData->GetNext(i) ) {

				CString Name = pData->GetName(i);

				if( p == 0 && Name.StartsWith("eth") && Name.GetLength() > 4 ) {

					InfoEth.Load(pFaces->GetChild(i), Name, pPath, uPath);

					InfoEth.StepMajor(ApplySledEthernet(InfoEth));
				}

				if( p == 1 && Name.StartsWith("wwan") ) {

					InfoCell.Load(pFaces->GetChild(i), Name, pPath, uPath);

					InfoCell.StepMajor(ApplySledCell(InfoCell));
				}

				if( p == 2 && Name.StartsWith("wlan0s") ) {

					InfoWiFi.Load(pFaces->GetChild(i), Name, pPath, uPath);

					InfoWiFi.StepMajor(ApplySledWifi(InfoWiFi));
				}
			}
		}
	}

	if( pTuns ) {

		CJsonData *pData = pTuns->GetJsonData();

		PCSTR      pPath    = "0.0.2.%u";

		UINT       uPath    = 0;

		CFaceInfo  InfoGre, InfoIpSec, InfoOpen, InfoOtap;

		for( UINT p = 0; p < 4; p++ ) {

			for( INDEX i = pData->GetHead(); !pData->Failed(i); pData->GetNext(i) ) {

				CString Name = pData->GetName(i);

				if( p == 0 && Name.StartsWith("grex") ) {

					InfoGre.Load(pFaces->GetChild(i), Name, pPath, uPath);

					InfoGre.StepMajor(ApplyTunnelGre(InfoGre));
				}

				if( p == 1 && Name.StartsWith("ipsec") ) {

					InfoIpSec.Load(pFaces->GetChild(i), Name, pPath, uPath);

					InfoIpSec.StepMajor(ApplyTunnelIpsec(InfoIpSec));
				}

				if( p == 2 && Name.StartsWith("tun") ) {

					InfoOpen.Load(pFaces->GetChild(i), Name, pPath, uPath);

					InfoOpen.StepMajor(ApplyTunnelOpenVpn(InfoOpen, false));
				}

				if( p == 3 && Name.StartsWith("tap") ) {

					InfoOtap.Load(pFaces->GetChild(i), Name, pPath, uPath);

					InfoOtap.StepMajor(ApplyTunnelOpenVpn(InfoOtap, true));
				}
			}
		}

		if( InfoIpSec.m_uIndex ) {

			m_Init1.Append("modprobe ip_vti");
		}

		if( InfoOpen.m_uIndex || InfoOtap.m_uIndex ) {

			m_Init1.Append("modprobe tun");
		}
	}

	ApplyFinalIpSec();

	ApplyFinalRules();

	CString Prep = "/opt/crimson/scripts/c3-prep-rules \"";

	for( UINT n = 0; n < m_RuleFaces.GetCount(); n++ ) {

		if( n ) {

			Prep += ' ';
		}

		Prep += GetFaceName(m_RuleFaces[n]);
	}

	Prep += "\"";

	m_Init1.Append(Prep);

	m_Init1.Append("/opt/crimson/scripts/c3-drop-dns none");

	SaveScript("net", "init1", m_Init1);
	SaveScript("net", "init2", m_Init2);
	SaveScript("net", "term1", m_Term1);
	SaveScript("net", "term2", m_Term2);

	WriteDependencies();

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyBaseEthernet(CFaceInfo &Info)
{
	if( Info.m_pFace ) {

		UINT face = GetFaceId(Info.m_Name);

		UINT defm = 200 + 10 * Info.m_uMajor;

		UINT mode = Info.GetUInt("mode", 0, 0, 9999);

		UINT br   = FindBridge(face);

		if( mode ) {

			CStringArray s[scriptCount];

			Info.m_Desc.Printf("Ethernet %u", 1 + Info.m_uMajor);

			UINT mtu = ApplyPhysical(s, Info);

			if( br ) {

				Info.SetName(CPrintf("br%u", br));

				m_BridgeMtu.SetAt(br-1, mtu);

				CreateBridge(s, Info, defm, mtu);

				BindToBridge(s, Info, true);

				AppendConfig(s, "watch=%s", Info.m_pName);
			}

			if( true ) {

				IEthernetStatus *pStatus  = new CLinuxEthernetStatus(Info.m_pBase, Info.m_uIndex);

				AddFace(s, Info, pStatus, 1);
			}

			if( mode != 4 ) {

				AddDependency("fast", Info.m_pBase);
			}

			ApplyEthernet(s, Info, face, defm, mode);

			ApplyMacFilter(s, Info, true);

			AppendWatcher(s, "Ethernet", Info.m_pBase);

			SaveFaceScripts(Info.m_pBase, s);

			ApplyVLans(s, Info);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplySledEthernet(CFaceInfo &Info)
{
	if( Info.m_pFace ) {

		UINT face = GetFaceId(Info.m_Name);

		UINT defm = 200 + 10 * Info.m_uMajor;

		UINT mode = Info.GetUInt("mode", 0, 0, 9999);

		if( mode ) {

			CStringArray s[scriptCount];

			Info.m_Desc.Printf("Ethernet %u", 1 + Info.m_uMajor);

			UINT br = FindBridge(mode);

			if( br ) {

				if( false ) {

					// We could to this to show the PHY status but right now
					// it shows the individual MAC rather than the bridge MAC,
					// and that's misleading for the user...

					IEthernetStatus *pStatus = new CLinuxEthernetStatus(Info.m_pBase, Info.m_uIndex);

					AddFace(s, Info, pStatus, 1);
				}

				UINT mtu = m_BridgeMtu[br-1];

				Info.SetName(CPrintf("br%u", br));

				ApplyPhysical(s, Info, mtu);

				BindToBridge(s, Info, false);

				ApplyMacFilter(s, Info, false);
			}
			else {
				IEthernetStatus *pStatus = new CLinuxEthernetStatus(Info.m_pBase, Info.m_uIndex);

				AddFace(s, Info, pStatus, 1);

				ApplyPhysical(s, Info);

				ApplyEthernet(s, Info, face, defm, mode);

				ApplyMacFilter(s, Info, true);
			}

			if( mode != 4 ) {

				AddDependency("fast", Info.m_pBase);
			}

			AppendWatcher(s, "Ethernet", Info.m_pBase);

			SaveFaceScripts(Info.m_pBase, s);

			ApplyVLans(s, Info);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplySledWifi(CFaceInfo &Info)
{
	if( Info.m_pFace ) {

		UINT face = GetFaceId(Info.m_Name);

		UINT defm = 500 + 10 * Info.m_uMajor;

		UINT mode = Info.GetUInt("mode", 0, 0, 9999);

		if( mode ) {

			CStringArray s[scriptCount];

			Info.m_Desc.Printf("Wi-Fi %u", 1 + Info.m_uMajor);

			IWiFiStatus *pStatus = new CLinuxWiFiStatus(Info.m_pBase, Info.m_uIndex);

			AddFace(s, Info, pStatus, 1);

			ApplyWifi(s, Info);

			ApplyEthernet(s, Info, face, defm, mode);

			AddDependency("slow", Info.m_pBase);

			AppendWatcher(s, "EspWiFi", Info.m_pBase);

			SaveFaceScripts(Info.m_pBase, s);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplySledCell(CFaceInfo &Info)
{
	if( Info.m_pFace ) {

		UINT face = GetFaceId(Info.m_Name);

		UINT defm = 600 + 10 * Info.m_uMajor;

		UINT mode = Info.GetUInt("mode", 0, 0, 5);

		if( mode ) {

			CStringArray s[scriptCount];

			Info.m_Desc.Printf("Modem %u", 1 + Info.m_uMajor);

			UINT actm = GetMetric(Info.m_pFace, defm);

			UINT mtu  = Info.GetUInt("mtu", 1500, 64, 2048);

			UINT prio = 2000 + (m_FaceNames.GetCount() - 1);

			BOOL asym = Info.GetBool("allowAsymm", false);

			UINT priv = Info.GetUInt("allowPrivate", 0, 0, 2);

			ICellStatus *pStatus = new CLinuxCellStatus(Info.m_pBase, Info.m_uIndex);

			AddFace(s, Info, pStatus, 1);

			AppendScript(s, scriptStart, "ip addr flush dev %s", Info.m_pName);

			ApplyCell(s, Info, mode == 5);

			AppendScript(s, scriptStop, "/opt/crimson/scripts/c3-ifbreak %s", Info.m_pName);

			AppendScript(s, scriptStart, "ip link set dev %s mtu %u", Info.m_pName, mtu);

			if( priv == 0 ) {

				priv = IsTrusted(Info.m_pFace) ? 2 : 1;
			}

			if( true ) {

				AppendConfig(m_FireDrop, "iptables -A OUTPUT -p igmp -o %s -j DROP", Info.m_pName);
			}

			if( priv == 1 ) {
				AppendConfig(m_FireDrop, "iptables -A OUTPUT -o %s -d 192.168.0.0/16 -j DROP", Info.m_pName);

				AppendConfig(m_FireDrop, "iptables -A OUTPUT -o %s -d 10.0.0.0/8 -j DROP", Info.m_pName);

				AppendConfig(m_FireDrop, "iptables -A OUTPUT -o %s -d 172.16.0.0/12 -j DROP", Info.m_pName);

				AppendConfig(m_FireDrop, "iptables -A OUTPUT -o %s -d 169.254.0.0/16 -j DROP", Info.m_pName);

				AppendConfig(m_FireDrop, "iptables -A FORWARD -o %s -d 192.168.0.0/16 -j DROP", Info.m_pName);

				AppendConfig(m_FireDrop, "iptables -A FORWARD -o %s -d 10.0.0.0/8 -j DROP", Info.m_pName);

				AppendConfig(m_FireDrop, "iptables -A FORWARD -o %s -d 172.16.0.0/12 -j DROP", Info.m_pName);

				AppendConfig(m_FireDrop, "iptables -A FORWARD -o %s -d 169.254.0.0/16 -j DROP", Info.m_pName);

				if( Info.GetUInt("internet", 0, 0, 2) != 2 ) {

					AppendConfig(m_FireDrop, "iptables -A FORWARD -o %s -s 192.168.0.0/16 -j DROP", Info.m_pName);

					AppendConfig(m_FireDrop, "iptables -A FORWARD -o %s -s 10.0.0.0/8 -j DROP", Info.m_pName);

					AppendConfig(m_FireDrop, "iptables -A FORWARD -o %s -s 172.16.0.0/12 -j DROP", Info.m_pName);

					AppendConfig(m_FireDrop, "iptables -A FORWARD -o %s -s 169.254.0.0/16 -j DROP", Info.m_pName);
				}
			}

			if( mode == 5 ) {

				UINT    mark = 2000 + (m_FaceNames.GetCount() - 1);

				UINT    bind = Info.GetUInt("face", 0, 0, 65535);

				CString Bind = bind ? GetFaceName(bind) : m_IptFace;

				BOOL    gate = Info.GetUInt("defaultRoute", 0, 0, 1);

				ApplyPrivateRoutes(s, Info, face, false);

				AppendScript(s, scriptStart, "ip rule add fwmark %u lookup c3-%s-in prio %u", mark, PCSTR(Bind), prio-1000);

				AppendScript(s, scriptStop, "ip rule delete prio %u", prio-1000);

				AppendConfig(s, "ipt=%s", PCSTR(Bind));

				AppendConfig(s, "metric=%u", gate ? actm : 3000);

				ApplyIptRules(s, Info, Bind, mark, gate);

				ApplyTrafficRules(s, Info);

				ApplyDynDns(s, Info, TRUE);

				AddDependency(Info.m_pBase, Bind);
			}
			else {
				ApplyPrivateRoutes(s, Info, face, asym);

				AppendConfig(s, "sym=%u", asym ? 0 : 1);

				AppendConfig(s, "metric=%u", actm);

				m_DhcpClients.Append(Info.m_pName);

				ApplyDynDns(s, Info, FALSE);

				ApplyRoutes(s[scriptMake], face, actm);

				ApplyMonitor(s, Info);

				ApplySharing(s, Info);

				ApplyUntrustedRules(s, Info);

				ApplyFinalRules(s, Info);
			}

			AppendScript(s, scriptStop, "ip addr flush dev %s", Info.m_pName);

			// Allocate these anyway as they're used by either IPT mode or
			// by modems like the 910C2 that need an internal IP address. We
			// could later check to see if they clash with any other ports,
			// but for now we'll assume they are safe addresses.

			BYTE b = BYTE(240 + 2 * Info.m_uMajor);

			AppendConfig(s, "secaddr=192.168.255.%u", b++);

			AppendConfig(s, "secgate=192.168.255.%u", b++);

			AppendConfig(s, "secmask=255.255.255.254");

			AppendConfig(s, "rule=%u", prio);

			AddDependency("slow", Info.m_pName);

			AppendWatcher(s, "Modem", Info.m_pBase);

			SaveFaceScripts(Info.m_pName, s);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyEthernet(CStringArray *s, CFaceInfo &Info, UINT face, UINT defm, UINT mode)
{
	AppendScript(s, scriptStart, "ip addr flush dev %s", Info.m_pName);

	if( mode == 4 ) {

		if( m_IptFace.IsEmpty() ) {

			m_IptFace = Info.m_pBase;
		}

		AppendScript(s, scriptStart, "source /tmp/crimson/face/%s/start", Info.m_pBase);

		AppendScript(s, scriptStop, "source /tmp/crimson/face/%s/stop", Info.m_pBase);

		SetPrivateRoutes(face);

		ApplyTrafficRules(s, Info);
	}

	if( mode == 1 || mode == 2 ) {

		defm = GetMetric(Info.m_pFace, defm);

		BOOL asym = Info.GetBool("allowAsymm", false);

		UINT prio = 2000 + (m_FaceNames.GetCount() - 2);

		ApplyPrivateRoutes(s, Info, face, asym);

		if( mode == 1 ) {

			ApplyDhcpClient(s, Info, m_DefName, Info.GetBool("dhcpGate", true));

			if( !CanAcceptInput(Info.m_pFace) ) {

				AddRule(m_FireInit, GetRule("dhcpc"), Info.m_pName, 0);
			}

			m_DhcpClients.Append(Info.m_pName);

			ApplyDynDns(s, Info, FALSE);

			AppendConfig(s, "rule=%u", prio);

			AppendConfig(s, "sym=%u", asym ? 0 : 1);
		}

		if( mode == 2 ) {

			CIpAddr Addr = Info.GetIp("address", IP_EMPTY);

			CIpAddr Mask = Info.GetIp("netmask", IP_EMPTY);

			ApplyStaticIp(s, Info, defm, asym, prio);

			ApplyAliases(s, Info, Addr, Mask, defm);

			ApplyDhcpServer(s, Info);

			ApplyDynDns(s, Info, FALSE);

			AppendScript(s, scriptStart, "/opt/crimson/scripts/c3-ifmake %s %s", Info.m_pBase, PCSTR(Addr.GetAsText()));
		}

		ApplyRoutes(s[scriptMake], GetFaceId(Info.m_pBase), defm);

		ApplyMonitor(s, Info);

		ApplySharing(s, Info);

		ApplyVrrp(s, Info);

		ApplyUntrustedRules(s, Info);

		ApplyFinalRules(s, Info);

		AppendScript(s, scriptStop, "/opt/crimson/scripts/c3-ifbreak %s", Info.m_pBase);

		AppendConfig(s, "metric=%u", defm);
	}

	AppendScript(s, scriptStop, "ip addr flush dev %s", Info.m_pName);

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyVLans(CStringArray *s, CFaceInfo &Root)
{
	CFaceInfo Info = Root;

	for( UINT v = 1; v <= 26; v++ ) {

		char alpha = char('a' + v - 1);

		Info.SetBoth(CPrintf("%s%c", Root.m_pBase, alpha));

		Info.m_Path.Printf("%s.%u", PCSTR(Root.m_Path), v-1);

		Info.m_pFace = Root.m_pFace->GetChild(Info.m_Name);

		Info.StepMinor();

		if( Info.m_pFace ) {

			UINT face = 1000 + 100 * Info.m_uMajor + v;

			UINT defm = 200  + 10  * Info.m_uMajor + 5;

			UINT mode = Info.GetUInt("mode", 0, 0, 9999);

			UINT br   = FindBridge(face);

			if( mode ) {

				Info.StepIndex();

				CStringArray s[scriptCount];

				Info.m_Desc.Printf("VLAN %u%c", 1 + Info.m_uMajor, alpha);

				UINT vn  = Info.GetUInt("vlan", 0, 1, 4094);

				UINT mtu = ApplyPhysical(s, Info);

				AppendScript(s, scriptUp, "ip link add link %s name %s type vlan id %u", Root.m_pName, Info.m_pName, vn);

				AppendScript(s, scriptDn, "ip link delete name %s", Info.m_pName);

				if( br ) {

					Info.SetName(CPrintf("br%u", br));

					m_BridgeMtu.SetAt(br-1, mtu);

					CreateBridge(s, Info, defm, mtu);

					BindToBridge(s, Info, true);

					AppendConfig(s, "watch=%s", Info.m_pName);
				}

				IEthernetStatus *pStatus = new CLinuxEthernetStatus(Info.m_pBase, Info.m_uIndex);

				AddFace(s, Info, pStatus, 1);

				AppendConfig(s, "virtual=true");

				AddDependency("fast", Info.m_pBase);

				ApplyEthernet(s, Info, face, defm, mode);

				AppendWatcher(s, "Ethernet", Info.m_pBase);

				SaveFaceScripts(Info.m_pBase, s);
			}

			continue;
		}

		break;
	}

	Root.m_uIndex = Info.m_uIndex;

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyWifi(CStringArray *s, CFaceInfo &Info)
{
	CString ssid = Info.GetValue("ssid", "");

	CString psk  = Info.GetValue("psk", "");

	if( Info.GetUInt("wmode", 0, 0, 1) == 0 ) {

		AppendConfig(s, "mode=%u", 0);

		AppendConfig(s, "ssid=%s", PCSTR(ssid));

		AppendConfig(s, "#skip psk=%s", "****");

		AppendConfig(s, "psk=%s", PCSTR(psk));
	}
	else {
		AppendConfig(s, "mode=%u", 1);

		AppendConfig(s, "ssid=%s", PCSTR(ssid));

		int chan = Info.GetUInt("channel", 7, 1, 11);

		int cast = Info.GetUInt("broadcast", 0, 0, 1) ? 0 : 1;

		int code = 0;

		switch( Info.GetUInt("encrpyt", 2, 0, 4) ) {

			case 0:
				psk  = "";
				break;

			case 1:
				code = 2;
				break;

			case 2:
				code = 3;
				break;

			case 3:
				code = 4;
				break;

			case 4:
				code = 5;
				break;
		}

		AppendConfig(s, "channel=%u", chan);

		AppendConfig(s, "broadcast=%u", cast);

		AppendConfig(s, "encrypt=%u", code);

		AppendConfig(s, "#skip psk=%s", "****");

		AppendConfig(s, "psk=%s", PCSTR(psk));

		ApplyMacFilter(s, Info, false);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyCell(CStringArray *s, CFaceInfo &Info, BOOL ipt)
{
	AppendConfig(s, "slot=%u", Info.GetUInt("simSlot", 0, 0, 1));

	AppendConfig(s, "switch=%u", Info.GetUInt("simSwitch", 0, 0, 2));

	AppendConfig(s, "fallback=%u", Info.GetUInt("simFallback", 30, 5, 2880));

	AppendConfig(s, "router=%u", ipt ? 1 : Info.GetUInt("defaultRoute", 0, 0, 1));

	AppendConfig(s, "dns=%u", Info.GetUInt("peerDNS", 0, 0, 1));

	AppendConfig(s, "sms=%u", Info.GetUInt("smsMode", 1, 0, 2));

	AppendConfig(s, "time=%u", Info.GetUInt("timeSync", 1, 0, 1));

	AppendConfig(s, "gps=%u", Info.GetUInt("enableGPS", 1, 0, 1));

	UINT power = Info.GetUInt("power", 0, 0, 1);

	if( power ) {

		AppendConfig(s, "power=%u", power);

		AppendConfig(s, "pcycle=%u", Info.GetUInt("pcycle", 0, 0, 1440));

		AppendConfig(s, "pconnect=%u", Info.GetUInt("pconnect", 0, 0, 1440));

		AppendConfig(s, "pmin=%u", Info.GetUInt("pmin", 0, 0, 1440));

		AppendConfig(s, "pmax=%u", Info.GetUInt("pmax", 0, 0, 1440));
	}

	ApplyCellSim(s, Info, 1);

	ApplyCellSim(s, Info, 2);

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyCellSim(CStringArray *s, CFaceInfo &Info, UINT uSim)
{
	CString apn = Info.GetValue(CPrintf("apn%u", uSim), "");

	if( !apn.IsEmpty() ) {

		AppendConfig(s, "apn%u=%s", uSim, PCSTR(apn));

		AppendConfig(s, "roam%u=%u", uSim, 1 - Info.GetUInt(CPrintf("roaming%u", uSim), 0, 0, 1));

		AppendConfig(s, "speed%u=%u", uSim, Info.GetUInt(CPrintf("speed%u", uSim), 0, 0, 3));

		UINT band = Info.GetUInt(CPrintf("band%u", uSim), 0, 0, 1);

		UINT auth = Info.GetUInt(CPrintf("authMode%u", uSim), 0, 0, 2);

		AppendConfig(s, "band%u=%u", uSim, band);

		AppendConfig(s, "auth%u=%u", uSim, auth);

		if( band ) {

			AppendConfig(s, "band3g%u=%u", uSim, Info.GetUInt(CPrintf("band3g%u", uSim), 1, 0, 7));

			AppendConfig(s, "band4g%u=%u", uSim, Info.GetUInt(CPrintf("band4g%u", uSim), 65535, 0, 65535));
		}

		if( auth ) {

			AppendConfig(s, "user%u=%s", uSim, PCSTR(Info.GetValue(CPrintf("authName%u", uSim), "")));

			AppendConfig(s, "#skip pass%u=%s", uSim, "****");

			AppendConfig(s, "pass%u=%s", uSim, PCSTR(Info.GetValue(CPrintf("authPass%u", uSim), "")));
		}

		AppendConfig(s, "#skip pin%u=%s", uSim, "****");

		AppendConfig(s, "pin%u=%s", uSim, PCSTR(Info.GetValue(CPrintf("simCode%u", uSim), "")));

		return TRUE;
	}

	return FALSE;
}

void CLinuxNetApplicator::ApplyStaticIp(CStringArray *s, CFaceInfo &Info, UINT defm, UINT asym, UINT prio)
{
	ApplyStaticIp(s[scriptStart], s[scriptStop], Info, defm, asym, prio);
}

void CLinuxNetApplicator::ApplyStaticIp(CStringArray &init, CStringArray &term, CFaceInfo &Info, UINT defm, UINT asym, UINT prio)
{
	CIpAddr Addr = Info.GetIp("address", IP_EMPTY);

	CIpAddr Mask = Info.GetIp("netmask", IP_EMPTY);

	CIpAddr Gate = Info.GetIp("gateway", IP_EMPTY);

	AppendConfig(init, "ip addr add %s dev %s noprefixroute", PCSTR(GetFullAddr(Addr, Mask)), Info.m_pName);

	AppendConfig(init, "ip route add %s dev %s metric %u", PCSTR(GetFullNet(Addr, Mask)), Info.m_pName, defm);

	AppendConfig(init, "ip rule add to %s lookup main prio %u", PCSTR(GetFullNet(Addr, Mask)), prio);

	AppendConfig(term, "ip rule delete prio %u", prio);

	if( !Gate.IsEmpty() ) {

		CString gate = Gate.GetAsText();

		if( !asym ) {

			AppendConfig(init, "ip route add default via %s dev %s table c3-%s-out", PCSTR(gate), Info.m_pName, Info.m_pName);

			AppendConfig(term, "ip route del default dev %s table c3-%s-out", Info.m_pName, Info.m_pName);
		}

		AppendConfig(init, "ip route add default via %s dev %s metric %u", PCSTR(gate), Info.m_pName, defm);

		AppendConfig(term, "ip route del default dev %s", Info.m_pName);
	}
}

void CLinuxNetApplicator::ApplyDhcpClient(CStringArray *s, CFaceInfo &Info, PCSTR pHost, BOOL fGate)
{
	ApplyDhcpClient(s[scriptStart], s[scriptStop], Info, pHost, fGate);
}

void CLinuxNetApplicator::ApplyDhcpClient(CStringArray &init, CStringArray &term, CFaceInfo &Info, PCSTR pHost, BOOL fGate)
{
	CString opts("-f -S");

	opts.AppendPrintf(fGate ? "" : " -G");

	opts.AppendPrintf(" -i %s", Info.m_pName);

	opts.AppendPrintf(" -x hostname:%s", PCSTR(m_DefName));

	opts.AppendPrintf(" -s /opt/crimson/scripts/c3-dhcp-event");

	AppendConfig(init, "/opt/crimson/bin/InitC32 start %s-dhcpc udhcpc %s", Info.m_pBase, PCSTR(opts));

	AppendConfig(term, "/opt/crimson/bin/InitC32 stop %s-dhcpc", Info.m_pBase);
}

UINT CLinuxNetApplicator::ApplyPhysical(CStringArray *s, CFaceInfo &Info, UINT mtu)
{
	UINT uFast = Info.GetUInt("fast", 0, 0, 2);

	UINT uFull = Info.GetUInt("full", 0, 0, 2);

	if( !uFast && !uFull ) {

		AppendConfig(s, "auto=%u", 1);
	}
	else {
		AppendConfig(s, "auto=%u", 0);

		AppendConfig(s, "fast=%u", (uFast == 1) ? 0 : 1);

		AppendConfig(s, "full=%u", (uFull == 1) ? 0 : 1);
	}

	AppendConfig(s, "mtu=%u", mtu);

	return mtu;
}

UINT CLinuxNetApplicator::ApplyPhysical(CStringArray *s, CFaceInfo &Info)
{
	return ApplyPhysical(s, Info, Info.GetUInt("mtu", 1500, 64, 2048));
}

BOOL CLinuxNetApplicator::ApplyAliases(CStringArray *s, CFaceInfo &Info, CIpAddr const &IntAddr, CIpAddr const &IntMask, UINT defm)
{
	CTree<DWORD> Done;

	CJsonConfig *pAliases = Info.m_pFace->GetChild("aliases");

	if( pAliases ) {

		UINT c = pAliases->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pAlias = pAliases->GetChild(n);

			if( pAlias ) {

				CIpAddr Addr = pAlias->GetValueAsIp("address", IP_EMPTY);

				CIpAddr Mask = pAlias->GetValueAsIp("netmask", IP_EMPTY);

				AppendScript(s, scriptStart, "ip addr add %s dev %s noprefixroute", PCSTR(GetFullAddr(Addr, Mask)), Info.m_pName);

				if( !Addr.OnSubnet(IntAddr, IntMask) ) {

					AppendScript(s, scriptStart, "ip route add %s dev %s metric %u", PCSTR(GetFullNet(Addr, Mask)), Info.m_pName, defm);
				}

				Done.Insert(Addr.m_dw);
			}
		}
	}

	UINT  Face = GetFaceId(Info.m_pName);

	INDEX Find = m_AutoIps.FindName(Face);

	if( !m_AutoIps.Failed(Find) ) {

		CStringArray List;

		m_AutoIps.GetData(Find).Tokenize(List, '|');

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CIpAddr Addr(List[n]);

			if( Addr.OnSubnet(IntAddr, IntMask) ) {

				if( Done.Failed(Done.Find(Addr.m_dw)) ) {

					AppendScript(s, scriptStart, "ip addr add %s dev %s noprefixroute", PCSTR(GetFullAddr(Addr, IntMask)), Info.m_pName);

					Done.Insert(Addr.m_dw);
				}
			}
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyDhcpServer(CStringArray *s, CFaceInfo &Info)
{
	UINT mode = Info.GetUInt("dhcpServerMode", 0, 0, 2);

	if( mode == 1 ) {

		mkdir("/./vap/opt/crimson/dhcp", 0);

		CPrintf Conf("%s-dhcp-conf", Info.m_pBase);

		CPrintf Data("/vap/opt/crimson/dhcp/leases-%s", Info.m_pBase);

		CIpAddr Start = Info.GetIp("dhcpServerStart", IP_EMPTY);

		CIpAddr End   = Info.GetIp("dhcpServerEnd", IP_EMPTY);

		CIpAddr Addr  = Info.GetIp("address", IP_EMPTY);

		CIpAddr Mask  = Info.GetIp("netmask", IP_EMPTY);

		CStringArray c;

		AppendConfig(c, "interface %s", Info.m_pName);

		AppendConfig(c, "start %s", PCSTR(Start.GetAsText()));

		AppendConfig(c, "end %s", PCSTR(End.GetAsText()));

		AppendConfig(c, "max_leases %u", 1 + (End - Start));

		AppendConfig(c, "lease_file %s", PCSTR(Data));

		AppendConfig(c, "opt subnet %s", PCSTR(Mask.GetAsText()));

		if( Info.GetBool("dhcpServerGate", TRUE) ) {

			AppendConfig(c, "opt router %s", PCSTR(Addr.GetAsText()));
		}

		if( m_fDnsProxy ) {

			if( !CanAcceptInput(Info.m_pFace) ) {

				AddRule(m_FireInit, GetRule("dns"), Info.m_pName, 0);
			}

			AppendConfig(c, "opt dns %s", PCSTR(Addr.GetAsText()));
		}

		if( m_fNtpProxy ) {

			if( !CanAcceptInput(Info.m_pFace) ) {

				AddRule(m_FireInit, GetRule("ntp"), Info.m_pName, 0);
			}

			AppendConfig(c, "opt ntpsrv %s", PCSTR(Addr.GetAsText()));
		}

		if( !CanAcceptInput(Info.m_pFace) ) {

			AddRule(m_FireInit, GetRule("dhcpd"), Info.m_pName, 0);
		}

		c.Append(m_DhcpServer);

		SaveConfig("net", Conf, c);

		CPrintf Prog("udhcpd -f -S -I %s /vap/opt/crimson/config/net/%s", PCSTR(Addr.GetAsText()), PCSTR(Conf));

		AppendProgram(s, scriptStart, Prog, "dhcpd", Info.m_pBase);

		return TRUE;
	}

	if( mode == 2 ) {

		if( !m_DhcpRelay.IsEmpty() ) {

			if( !CanAcceptInput(Info.m_pFace) ) {

				AddRule(m_FireInit, GetRule("dhcpd"), Info.m_pName, 0);
			}

			CPrintf Prog("dhcrelay -d -i %s %s", Info.m_pName, PCSTR(m_DhcpRelay));

			AppendProgram(s, scriptStart, Prog, "dhcpr", Info.m_pBase);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyDynDns(CStringArray *s, CFaceInfo &Info, BOOL fTrans)
{
	UINT dyn = Info.GetUInt("dynDnsService", 0, 0, NOTHING);

	if( dyn ) {

		CString User(Info.GetValue("dynDnsUsername", ""));

		CString Pass(Info.GetValue("dynDnsPassword", ""));

		CString Host(Info.GetValue("dynDnsHostname", ""));

		UINT    uArg = fTrans ? 2 : 1;

		CString Cmd;

		switch( dyn ) {

			case 1:
			{
				Cmd = "ez-ipupdate -S dhs -u $user:$pass -h $host -a $%u";
			}
			break;

			case 2:
			{
				CString Server(Info.GetValue("dynDnsServer", ""));

				CString Request(Info.GetValue("dynDnsRequest", ""));

				if( Server.IsEmpty() ) {

					Cmd = "ez-ipupdate -S dyndns -u $user:$pass -h $host -a $%u";
				}
				else {
					AppendScript(s, scriptMake, "server=%s", PCSTR(Server));

					AppendScript(s, scriptMake, "request=%s", PCSTR(Request));

					Cmd = "ez-ipupdate -S dyndns-custom -s $server -g $request -u $user:$pass -h $host -a $%u";
				}
			}
			break;

			case 3:
			{
				Cmd = "ez-ipupdate -S dyns -u $user:$pass -h $host -a $%u";
			}
			break;

			case 4:
			{
				Cmd = "curl -k \"https://www.duckdns.org/update?domains=$host&token=$pass&ip=$%u\"";
			}
			break;

			case 5:
			{
				CString Partner(Info.GetValue("dynDnsPartner", ""));

				if( Partner.IsEmpty() ) {

					Cmd = "ez-ipupdate -S easydns -u $user:$pass -h $host -a $%u";
				}
				else {
					AppendScript(s, scriptMake, "partner=%s", PCSTR(Partner));

					Cmd = "ez-ipupdate -S easydns-partner -z $partner -u $user:$pass -h $host -a $%u";
				}
			}
			break;

			case 6:
			{
				Cmd = "ez-ipupdate -S zoneedit -u $user:$pass -h $host -a $%u";
			}
			break;
		}

		if( !Cmd.IsEmpty() ) {

			Cmd = "/opt/crimson/scripts/c3-try " + Cmd + " &";

			AppendScript(s, scriptMake, "user=%s", PCSTR(User));

			AppendScript(s, scriptMake, "#skip pass=****");

			AppendScript(s, scriptMake, "pass=%s", PCSTR(Pass));

			AppendScript(s, scriptMake, "host=%s", PCSTR(Host));

			AppendScript(s, scriptMake, Cmd, uArg);
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::AddFace(CStringArray *s, CFaceInfo &Info, IUnknown *pStatus, UINT uCapture)
{
	if( pStatus ) {

		m_FaceStatus.Append(pStatus);
	}

	if( uCapture == 1 || uCapture == 2 ) {

		m_FaceStatus.Append(new CLinuxPacketCapture(Info.m_pName, m_FaceNames.GetCount(), uCapture));
	}

	m_FaceNames.Append(Info.m_Name);

	m_FaceDescs.Append(Info.m_Desc);

	m_FacePaths.Append(Info.m_Path);

	return TRUE;
}

BOOL CLinuxNetApplicator::ScanFacesForBridges(CJsonConfig *pFaces)
{
	CJsonData *pData = pFaces->GetJsonData();

	for( INDEX i = pData->GetHead(); !pData->Failed(i); pData->GetNext(i) ) {

		CJsonConfig *pFace = pFaces->GetChild(i);

		UINT         mode  = pFace->GetValueAsUInt("mode", 0, 0, 9999);

		if( mode > 3 ) {

			UINT br = 1 + m_BridgeRev.GetCount();

			m_BridgeMap.Insert(mode, br);

			m_BridgeRev.Append(mode);

			m_BridgeMtu.Append(1500);
		}
	}

	return !m_BridgeMap.IsEmpty();
}

BOOL CLinuxNetApplicator::ScanTunnelsForBridges(CJsonConfig *pTuns)
{
	CJsonData *pData = pTuns->GetJsonData();

	for( INDEX i = pData->GetHead(); !pData->Failed(i); pData->GetNext(i) ) {

		CJsonConfig *pTun = pTuns->GetChild(i);

		UINT         mode = pTun->GetValueAsUInt("imode", 0, 0, 9999);

		if( mode > 3 ) {

			UINT br = 1 + m_BridgeRev.GetCount();

			m_BridgeMap.Insert(mode, br);

			m_BridgeRev.Append(mode);

			m_BridgeMtu.Append(1500);
		}
	}

	return !m_BridgeMap.IsEmpty();
}

UINT CLinuxNetApplicator::FindBridge(UINT uFace)
{
	if( uFace >= 1000 ) {

		INDEX i = m_BridgeMap.FindName(uFace);

		if( !m_BridgeMap.Failed(i) ) {

			return m_BridgeMap.GetData(i);
		}
	}

	return 0;
}

BOOL CLinuxNetApplicator::CreateBridge(CStringArray *s, CFaceInfo &Info, UINT defm, UINT mtu)
{
	CStringArray Conf;

	AppendConfig(Conf, "base=%s", Info.m_pBase);

	SaveConfig("net", Info.m_Name + "-conf", Conf);

	AppendConfig(m_Init1, "ip link add name %s type bridge", Info.m_pName);

	AppendConfig(m_Init1, "echo 0 > /sys/class/net/%s/bridge/default_pvid", Info.m_pName);

	AppendScript(s, scriptUp, "ip link set dev %s up", Info.m_pName);

	AppendScript(s, scriptUp, "ip link set dev %s mtu %u", Info.m_pName, mtu);

	AppendScript(s, scriptDn, "ip link set dev %s down", Info.m_pName);

	AppendConfig(m_Term1, "ip link delete name %s", Info.m_pName);

	return TRUE;
}

BOOL CLinuxNetApplicator::BindToBridge(CStringArray *s, CFaceInfo &Info, BOOL primary)
{
	if( primary ) {

		AppendScript(s, scriptUp, "ip link set dev %s master %s", Info.m_pBase, Info.m_pName);

		AppendScript(s, scriptUp, "mac=`cat /sys/class/net/%s/address`", Info.m_pBase);

		AppendScript(s, scriptUp, "ip link set %s address $mac", Info.m_pName);
	}
	else {
		// Non-primary might be PNP and therefore not present on UP request.

		AppendScript(s, scriptStart, "ip link set dev %s master %s", Info.m_pBase, Info.m_pName);
	}

	AppendScript(s, scriptStart, "ip addr flush dev %s", Info.m_pBase);

	AppendScript(s, scriptStop, "ip addr flush dev %s", Info.m_pBase);

	AppendScript(s, scriptDn, "ip link set dev %s nomaster", Info.m_pBase);

	return TRUE;
}

BOOL CLinuxNetApplicator::IsTrusted(CJsonConfig *pFace)
{
	return pFace->GetValueAsUInt("trust", 0, 0, 1) == 1;
}

BOOL CLinuxNetApplicator::GetMetric(CJsonConfig *pFace, UINT uDefault)
{
	UINT uMetric = pFace->GetValueAsUInt("metric", 0, 0, 4095);

	if( pFace->GetValueAsUInt("checkMode", 0, 0, 3) ) {

		return 2000 + (uMetric ? uMetric : uDefault);
	}

	return uMetric ? uMetric : uDefault;
}

BOOL CLinuxNetApplicator::AppendWatcher(CStringArray *s, PCSTR pType, PCSTR pName)
{
	return AppendProgram(s, scriptUp, CPrintf("Watch%s -i %s", pType, pName), "watch", pName);
}

BOOL CLinuxNetApplicator::AppendProgram(CStringArray *s, UINT uBase, PCSTR pProgram, PCSTR pSuffix, PCSTR pName)
{
	return AppendProgram(s[uBase+0], s[uBase+1], pProgram, CPrintf("%s-%s", pName, pSuffix));
}

BOOL CLinuxNetApplicator::AppendProgram(CStringArray &init, CStringArray &term, PCSTR pProgram, PCSTR pTag)
{
	CPrintf Dn("/opt/crimson/bin/InitC32 stop %s", pTag);

	CPrintf Up("/opt/crimson/bin/InitC32 start %s %s", pTag, pProgram);

	init.Append(Up);

	term.Insert(0, Dn);

	return TRUE;
}

// Tunnel Interfaces

BOOL CLinuxNetApplicator::ApplyTunnelGre(CFaceInfo &Info)
{
	if( Info.m_pFace ) {

		UINT face = 5000 + 1  * Info.m_uMajor;

		UINT defm = 300  + 10 * Info.m_uMajor;

		UINT mode = Info.GetUInt("mode", 0, 0, 4);

		if( mode ) {

			CStringArray s[scriptCount];

			Info.m_Desc.Printf("GRE %u", 1 + Info.m_uMajor);

			AddFace(s, Info, NULL, 1);

			UINT    uFace   = Info.GetUInt("source", 0, 0, NOTHING);

			UINT    uLive   = Info.GetUInt("ttl", 64, 1, 200);

			BOOL    fArp    = Info.GetBool("arp", true);

			BOOL    fCast   = Info.GetBool("multicast", true);

			CIpAddr Peer    = Info.GetIp("peer", IP_EMPTY);

			CIpAddr Loc     = Info.GetIp("locaddr", IP_EMPTY);

			CIpAddr Rem     = Info.GetIp("remaddr", IP_EMPTY);

			CIpAddr Mask    = Info.GetIp("netmask", IP_EMPTY);

			UINT    uMetric = GetMetric(Info.m_pFace, defm);

			CString IKey    = Info.GetValue("ikey", "");

			CString OKey    = Info.GetValue("okey", "");

			CString Opts;

			if( IsValidGreKey(IKey) ) {

				AppendScript(s, scriptUp, "#skip ikey=****");

				AppendScript(s, scriptUp, "ikey=%s", PCSTR(IKey));

				Opts += " ikey $ikey";
			}

			if( IsValidGreKey(OKey) ) {

				AppendScript(s, scriptUp, "#skip okey=****");

				AppendScript(s, scriptUp, "okey=%s", PCSTR(OKey));

				Opts += " okey $okey";
			}

			if( !uFace ) {

				AddDependency("slow", Info.m_pName);
			}
			else {
				AddDependency(GetFaceName(uFace), Info.m_pName);

				Opts += " local $2";
			}

			AppendScript(s, scriptUp, "ip tunnel add %s mode gre%s remote %s ttl %u", Info.m_pName, PCSTR(Opts), PCSTR(Peer.GetAsText()), uLive);

			AppendScript(s, scriptStart, "ip link set %s up", Info.m_pName);

			AppendScript(s, scriptStart, "ip link set %s arp %s", Info.m_pName, fArp ? "on" : "off");

			AppendScript(s, scriptStart, "ip link set %s multicast %s", Info.m_pName, fCast ? "on" : "off");

			AppendScript(s, scriptStart, "ip addr flush dev %s", Info.m_pName);

			AppendScript(s, scriptStart, "ip addr add %s dev %s noprefixroute", PCSTR(GetFullAddr(Loc, Mask)), Info.m_pName);

			AppendScript(s, scriptStart, "ip route add %s dev %s metric %u", PCSTR(GetFullNet(Loc, Mask)), Info.m_pName, uMetric);

			AppendScript(s, scriptStart, "/opt/crimson/scripts/c3-ifmake %s %s", Info.m_pName, PCSTR(Loc.GetAsText()));

			AppendScript(s, scriptStop, "/opt/crimson/scripts/c3-ifbreak %s", Info.m_pName);

			AppendScript(s, scriptStop, "ip link set %s down", Info.m_pName);

			AppendScript(s, scriptStop, "ip addr flush dev %s", Info.m_pName);

			ApplyRoutes(s[scriptMake], face, uMetric);

			ApplyUntrustedRules(s, Info);

			ApplyFinalRules(s, Info);

			AppendScript(s, scriptDn, "ip tunnel delete %s", Info.m_pName);

			ApplyPeerCheck(s, Info, PCSTR(Peer.GetAsText()), true);

			SaveFaceScripts(Info.m_pName, s);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyTunnelIpsec(CFaceInfo &Info)
{
	if( Info.m_pFace ) {

		UINT mode = Info.GetUInt("mode", 0, 0, 3);

		if( mode ) {

			Info.m_Desc.Printf("IPsec %u", 1 + Info.m_uMajor);

			UINT  defm = 300  + 10 * Info.m_uMajor;

			UINT  mark = 3000 + Info.m_uMajor;

			UINT  actm = GetMetric(Info.m_pFace, defm);

			bool  cert = (Info.GetUInt("authmode", 0, 0, 1) == 1);

			DWORD crc  = 0;

			CStringArray s[scriptCount];

			CStringArray &c = s[configFile];

			AppendConfig(s, "conn %s", Info.m_pName);

			AppendConfig(s, " auto=add");

			AppendConfig(s, " type=tunnel");

			AppendConfig(s, " vti-interface=%s", Info.m_pName);

			AppendConfig(s, " vti-routing=no");

			AppendConfig(s, " mark=%u/0xFFFFFFFF", mark);

			AppendConfig(s, " ikev2=permit");

			AppendConfig(s, " pfs=%s", Info.GetBool("pfs", TRUE) ? "yes" : "no");

			AppendConfig(s, " aggrmode=%s", Info.GetBool("p1neg", TRUE) ? "yes" : "no");

			AppendConfig(s, " ikelifetime=%um", Info.GetUInt("p1rekey", 480, 1, 1440));

			ApplyIpsecDeadPeer(c, Info);

			ApplyIpsecPhase1(c, Info);

			ApplyIpsecPhase2(c, Info);

			CString LocIp  = ApplyIpsecFace(s[configFile], Info);

			CString RemIp  = Info.GetValue("peer", "");

			CString LocId  = Info.GetValue("locpeer", cert ? "%fromcert" : "");

			CString RemId  = Info.GetValue("rempeer", cert ? "%fromcert" : "");

			CString RemRef = !RemId.IsEmpty() ? RemId : RemIp;

			CString LocRef = !LocId.IsEmpty() ? LocId : "%any";

			ApplyIpsecAuth(c, crc, Info, LocRef, RemRef);

			ApplyIpsecOption(c, LocId, "leftid");

			ApplyIpsecOption(c, RemId, "rightid");

			ApplyIpsecOption(c, LocIp, "left");

			ApplyIpsecOption(c, RemIp, "right");

			ApplyIpsecOption(c, "0.0.0.0/0", "leftsubnet");

			ApplyIpsecOption(c, "0.0.0.0/0", "rightsubnet");

			AppendConfig(s, " leftupdown=/opt/crimson/scripts/c3-ipsec-event");

			AppendConfig(m_IpSecConfig, "include /vap/opt/crimson/config/net/%s-conf", Info.m_pName);

			AppendScript(s, scriptStart, "#crc %8.8X", GetCRC(c, crc));

			AppendScript(s, scriptMake, "ip link set dev %s up", Info.m_pName);

			ApplyRoutes(s[scriptMake], GetFaceId(Info.m_pBase), defm);

			ApplyUntrustedRules(s, Info);

			ApplyFinalRules(s, Info);

			CJsonConfig *pList = Info.m_pFace->GetChild("remnets");

			if( pList ) {

				UINT c = pList->GetCount();

				for( UINT n = 0; n < c; n++ ) {

					CJsonConfig *pItem = pList->GetChild(n);

					CIpAddr Addr = pItem->GetValueAsIp("ip", IP_EMPTY);

					CIpAddr Mask = pItem->GetValueAsIp("mask", IP_EMPTY);

					CPrintf Spec("%s/%s dev %s", PCSTR(Addr.GetAsText()), PCSTR(Mask.GetAsText()), Info.m_pName);

					AppendScript(s, scriptMake, "ip route add %s metric %u", PCSTR(Spec), actm);
				}
			}

			CString Verb = "--initiate --async";

			CString Kill = "--terminate";

			switch( mode ) {

				case 2:
				{
					Verb = "--route";

					Kill = "--unroute";
				}
				break;
			}

			AppendScript(s, scriptBreak, "ip link set dev %s down", Info.m_pName);

			AppendScript(s, scriptStart, "loc=`/opt/crimson/bin/Resolve %s`", PCSTR(LocIp));

			AppendScript(s, scriptStart, "rem=`/opt/crimson/bin/Resolve %s`", PCSTR(RemIp));

			AppendScript(s, scriptStart, "ip tunnel add %s local $loc remote $rem mode vti key %u", Info.m_pName, mark);

			AppendScript(s, scriptStart, "ipsec whack %s --name %s", PCSTR(Verb), Info.m_pName);

			AppendScript(s, scriptStop, "ipsec whack %s --name %s", PCSTR(Kill), Info.m_pName);

			AppendScript(s, scriptStop, "ip tunnel delete %s", Info.m_pName);

			AddFace(s, Info, NULL, 2);

			ApplyPeerCheck(s, Info, PCSTR(RemIp), true);

			SaveFaceScripts(Info.m_pName, s);
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyTunnelOpenVpn(CFaceInfo &Info, bool tap)
{
	if( Info.m_pFace ) {

		UINT face = 5000 + 1  * Info.m_uMajor;

		UINT defm = 300  + 10 * Info.m_uMajor;

		UINT mode = Info.GetUInt("mode", 0, 0, 4);

		if( mode ) {

			UINT    cmode = Info.GetUInt("cmode", 0, 0, 1);

			UINT    proto = Info.GetUInt("prot", 0, 0, 2);

			DWORD   crc   = 0;

			CString peer;

			CStringArray s[scriptCount];

			CStringArray &c = s[configFile];

			Info.m_Desc.Printf("%s %u", tap ? "TAP" : "VPN", 1 + Info.m_uMajor);

			AppendConfig(s, "syslog openvpn[%s]", Info.m_pBase);

			AppendConfig(s, "status /tmp/crimson/face/%s/status 5", Info.m_pBase);

			AppendConfig(s, "dev %s", Info.m_pBase);

			// TODO -- Can we add some sort of OpenVPN link status object?

			if( tap ) {

				CStringArray TunUp, TunDn;

				UINT imode = Info.GetUInt("imode", 0, 0, 9999);

				UINT br    = FindBridge(imode);

				if( br ) {

					// TODO -- Do we need to do something re MTUs?

					AddFace(s, Info, NULL, 1);

					UINT mtu = m_BridgeMtu[br-1];

					Info.SetName(CPrintf("br%u", br));

					AppendConfig(TunUp, "ip link set dev %s master %s", Info.m_pBase, Info.m_pName);

					AppendConfig(TunUp, "ip link set dev %s up", Info.m_pBase);

					AppendConfig(TunUp, "ip addr flush dev %s", Info.m_pBase);

					AfxTouch(mtu);
				}
				else {
					CString Mac = Info.GetValue("forceMac", "");

					if( Mac.IsEmpty() ) {

						CMacAddr mac;

						AfxGetAutoObject(pEntropy, "entropy", 0, IEntropy);

						pEntropy->GetEntropy(mac.m_Addr, sizeof(mac.m_Addr));

						mac.m_Addr[0] = BYTE(((mac.m_Addr[0] & 3) + 1) << 1);

						Mac = mac.GetAsText();
					}

					AppendConfig(TunUp, "ip link set dev %s address %s", Info.m_pBase, PCSTR(Mac));

					AppendConfig(TunUp, "ip link set dev %s up", Info.m_pBase);

					AppendConfig(TunUp, "ip addr flush dev %s", Info.m_pBase);

					UINT actm = GetMetric(Info.m_pFace, defm);

					BOOL asym = false;

					UINT prio = 2000 + (m_FaceNames.GetCount() - 2);

					if( imode == 1 ) {

						s[configFile].Insert(0, CPrintf("#rule=%u", prio));

						s[configFile].Insert(0, CPrintf("#sym=%u", asym ? 0 : 1));

						s[configFile].Insert(0, CPrintf("#metric=%u", actm));

						ApplyDhcpClient(TunUp, TunDn, Info, m_DefName, Info.GetBool("dhcpGate", true));
					}
					else {
						ApplyStaticIp(TunUp, TunDn, Info, actm, asym, prio);
					}

					AddFace(s, Info, NULL, 1);
				}

				if( SaveFile("net", CPrintf("%s.tunup", Info.m_pBase), true, TunUp) ) {

					AppendConfig(s, "up \"/vap/opt/crimson/config/net/%s.tunup\"", Info.m_pBase);
				}

				if( SaveFile("net", CPrintf("%s.tundn", Info.m_pBase), true, TunDn) ) {

					AppendConfig(s, "down \"/vap/opt/crimson/config/net/%s.tundn\"", Info.m_pBase);
				}

				AppendConfig(s, "script-security 2");
			}
			else {
				AddFace(s, Info, NULL, 2);
			}

			if( mode == 4 ) {

				CByteArray Config;

				Info.GetBlob("config", Config);

				if( !Config.IsEmpty() ) {

					CStringArray Lines;

					CString Text(PCSTR(Config.data()), Config.size());

					Text.Remove('\r');

					Text.Tokenize(Lines, '\n');

					for( UINT n = 0; n < Lines.GetCount(); n++ ) {

						CString const &Line = Lines[n];

						if( Line.StartsWith("syslog ") ) {

							continue;
						}

						if( Line.StartsWith("status ") ) {

							continue;
						}

						if( Line.StartsWith("dev ") ) {

							continue;
						}

						if( Line.StartsWith("local ") ) {

							continue;
						}

						if( Line.StartsWith("remote ") ) {

							CString Work(Line);

							Work.StripToken(' ');

							peer = Work.StripToken(' ');
						}

						AppendConfig(s, "%s", PCSTR(Line));
					}
				}

				ApplyOpenFace(c, Info);
			}
			else {
				AppendConfig(s, "auth-nocache");

				ApplyOpenPorts(c, Info);

				ApplyOpenFace(c, Info);

				ApplyOpenMode(c, crc, Info, mode, cmode, proto, tap);

				ApplyOpenAuth(c, crc, Info, mode, cmode);

				ApplyOpenKeepAlive(c, Info);

				ApplyOpenCipher(c, Info, mode);

				ApplyOpenMisc(c, Info);

				peer = Info.GetValue("peer", "");
			}

			if( !tap ) {

				AppendConfig(s, "route-metric %u", GetMetric(Info.m_pFace, defm));
			}

			ApplyOpenCustom(c, Info.m_pFace->GetChild("custom"));

			AppendScript(s, scriptStart, "#crc %8.8X", GetCRC(c, crc));

			CPrintf Prog("openvpn --config /vap/opt/crimson/config/net/%s-conf", Info.m_pBase);

			AppendProgram(s, scriptStart, Prog, "open", Info.m_pBase);

			ApplyPeerCheck(s, Info, peer, mode != 3 && proto != 2);

			SaveFaceScripts(Info.m_pBase, s);
		}

		AfxTouch(face);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyOpenPorts(CStringArray &s, CFaceInfo &Info)
{
	UINT lport = Info.GetUInt("lport", 0, 0, 65535);

	UINT rport = Info.GetUInt("rport", 0, 0, 65535);

	if( lport == rport ) {

		AppendConfig(s, "port %u", lport ? lport : 1194);
	}
	else {
		AppendConfig(s, "lport %u", lport ? lport : 1194);

		AppendConfig(s, "rport %u", rport ? rport : 1194);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyOpenFace(CStringArray &s, CFaceInfo &Info)
{
	UINT uFace = Info.GetUInt("source", 0, 0, NOTHING);

	if( uFace ) {

		CString Base = GetFaceName(uFace);

		AddDependency(Base, Info.m_pBase);

		AppendConfig(s, "local c3-%s-addr", PCSTR(Base));

		return TRUE;
	}

	AddDependency("slow", Info.m_pName);

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyOpenMode(CStringArray &s, DWORD &crc, CFaceInfo &Info, UINT mode, UINT cmode, UINT proto, bool tap)
{
	switch( proto ) {

		case 0:
			AppendConfig(s, "proto udp4");
			break;

		case 1:
			AppendConfig(s, "proto tcp-client");
			break;

		case 2:
			AppendConfig(s, "proto tcp-server");
			break;
	}

	if( !tap ) {

		if( mode == 1 || cmode == 0 ) {

			AppendConfig(s, "topology p2p");
		}
		else {
			if( mode == 3 ) {

				AppendConfig(s, "topology subnet");
			}
		}
	}

	if( mode == 1 || ((mode == 2 || mode == 3) && cmode == 0) ) {

		ApplyOpenRoutes(s, Info.m_pFace->GetChild("remnets"), false);
	}

	if( (mode == 2 || mode == 3) && cmode == 1 ) {

		if( mode == 3 ) {

			CIpAddr LocAddr = Info.GetIp("locaddr", IP_EMPTY);

			CIpAddr LocMask = Info.GetIp("locmask", IP_EMPTY);

			LocAddr = (LocAddr & LocMask);

			ApplyOpenRoutes(s, Info.m_pFace->GetChild("locnets"), true);

			ApplyOpenClients(s, crc, Info, LocMask);

			AppendConfig(s, "server %s %s", PCSTR(LocAddr.GetAsText()), PCSTR(LocMask.GetAsText()));

			AppendConfig(s, "ifconfig-pool-persist /vap/opt/crimson/openvpn/%s-ipp.txt 5", Info.m_pBase);

			mkdir("/./vap/opt/crimson/openvpn", 0755);
		}
		else {
			AppendConfig(s, "client");
		}
	}
	else {
		if( !tap ) {

			CIpAddr LocAddr = Info.GetIp("locaddr", IP_EMPTY);

			CIpAddr RemAddr = Info.GetIp("remaddr", IP_EMPTY);

			AppendConfig(s, "ifconfig %s %s", PCSTR(LocAddr.GetAsText()), PCSTR(RemAddr.GetAsText()));
		}
	}

	if( mode == 1 || mode == 2 ) {

		if( proto < 2 ) {

			CString RemPub = Info.GetValue("peer", "");

			AppendConfig(s, "remote %s", PCSTR(RemPub));
		}
	}

	if( mode == 2 && cmode == 1 ) {

		ApplyOpenCredentials(s, crc, Info);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyOpenAuth(CStringArray &s, DWORD &crc, CFaceInfo &Info, UINT mode, UINT cmode)
{
	UINT auth, kdir;

	if( mode == 1 ) {

		CByteArray Secret;

		Info.GetBlob("secret", Secret);

		if( !Secret.IsEmpty() ) {

			SaveCertFile(crc, "openvpn", Info.m_pBase, "sec.key", Secret);
		}

		if( (kdir = Info.GetUInt("keydir", 0, 0, 2)) ) {

			AppendConfig(s, "key-direction %u", kdir - 1);
		}

		AppendConfig(s, "secret /vap/opt/crimson/config/openvpn/%s-sec.key", Info.m_pBase);
	}
	else {
		UINT LocSrc = Info.GetUInt("locsrc", 0, 0, 65535);

		UINT RemSrc = Info.GetUInt("remsrc", 0, 0, 65535);

		if( LocSrc >= 100 ) {

			SaveIdentityCert(crc, "openvpn", Info.m_pBase, "loc.crt", "loc.key", NULL, LocSrc);
		}
		else {
			CByteArray LocCert, LocPriv;

			Info.GetBlob("loccert", LocCert);

			Info.GetBlob("locpriv", LocPriv);

			SaveCertFile(crc, "openvpn", Info.m_pBase, "loc.crt", LocCert);

			SaveCertFile(crc, "openvpn", Info.m_pBase, "loc.key", LocPriv);

			CString LocPass = Info.GetValue("locpass", "");

			crc = ::crc(crc, PCBYTE(LocPass.data()), LocPass.size());

			DecryptKeyFile("openvpn", Info.m_pBase, "loc.key", LocPass);
		}

		if( RemSrc >= 100 ) {

			SaveTrustedCert(crc, "openvpn", Info.m_pBase, "rem.crt", RemSrc);
		}
		else {
			CByteArray RemCert;

			Info.GetBlob("remcert", RemCert);

			SaveCertFile(crc, "openvpn", Info.m_pBase, "rem.crt", RemCert);
		}

		if( mode == 3 ) {

			if( cmode == 0 ) {

				AppendConfig(s, "tls-server");
			}

			if( Info.GetUInt("tlscheck", 0, 0, 1) ) {

				AppendConfig(s, "remote-cert-tls client");
			}

			CString DH =
				"-----BEGIN DH PARAMETERS-----\n"
				"MIIBCAKCAQEA2K32b9DizquL2g/LmxRBn/uIi3eDoclTvP/iAT3poNLVNR3NsQRe\n"
				"viO3590kR0h/rufQKY/JTVSAhwV5MPWx/0cm74/RdLATS2ho1JAQTC5cEFO5IsuV\n"
				"8A+gk4j3v0YKEibpuZpDSMB9af/Y0S4ZBpKjwoWFJVCCeekb04WmnNsHJ+FCxcl1\n"
				"FJmc47G1znGZ4/INPyTUtvVqknSbtXCZvUiOlbuDp+uSqsB/nF5ZzhahlsBEhrMm\n"
				"XkGM+4DOL0Ua2Irtbi6X7I1U5kmUPHOAA1/JXIL43shuVmlzg4qopEsx51R2KPMa\n"
				"Lj4nCsZtpQ+xDO/0cdg4vs4yiWi7qvmNEwIBAg==\n"
				"-----END DH PARAMETERS-----\n";

			SavePassFile(crc, "openvpn", Info.m_pBase, "dh.dh", DH);

			AppendConfig(s, "dh /vap/opt/crimson/config/openvpn/%s-dh.dh", Info.m_pBase);
		}
		else {
			if( cmode == 0 ) {

				AppendConfig(s, "tls-client");
			}

			if( Info.GetUInt("tlscheck", 0, 0, 1) ) {

				AppendConfig(s, "remote-cert-tls server");
			}
		}

		if( (auth = Info.GetUInt("tlsauth", 0, 0, 2)) ) {

			CByteArray Secret;

			Info.GetBlob("tlskey", Secret);

			if( !Secret.IsEmpty() ) {

				SaveCertFile(crc, "openvpn", Info.m_pBase, "tls.key", Secret);
			}

			if( (kdir = Info.GetUInt("keydir", 0, 0, 2)) ) {

				AppendConfig(s, "key-direction %u", kdir - 1);
			}

			PCSTR pVerb = (auth == 1) ? "auth" : "crypt";

			AppendConfig(s, "tls-%s /vap/opt/crimson/config/openvpn/%s-tls.key", pVerb, Info.m_pBase);
		}

		AppendConfig(s, "ca /vap/opt/crimson/config/openvpn/%s-rem.crt", Info.m_pBase);

		AppendConfig(s, "cert /vap/opt/crimson/config/openvpn/%s-loc.crt", Info.m_pBase);

		AppendConfig(s, "key /vap/opt/crimson/config/openvpn/%s-loc.key", Info.m_pBase);
	}

	return TRUE;
}


BOOL CLinuxNetApplicator::ApplyOpenKeepAlive(CStringArray &s, CFaceInfo &Info)
{
	if( Info.GetUInt("ping", 1, 0, 1) ) {

		UINT t = Info.GetUInt("ptime", 30, 0, 6000);

		AppendConfig(s, "keepalive %u %u", t, 4 * t);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyOpenCipher(CStringArray &s, CFaceInfo &Info, UINT mode)
{
	CString Name;

	switch( Info.GetUInt("cipher", 0, 0, 6) ) {

		case 0: Name = "AES-128"; break;
		case 1: Name = "AES-192"; break;
		case 2: Name = "AES-256"; break;
		case 3: Name = "Camellia-128"; break;
		case 4: Name = "Camellia-192"; break;
		case 5: Name = "Camellia-256"; break;
		case 6: Name = "SEED"; break;
	}

	if( mode == 1 ) {

		AppendConfig(s, "cipher %s-CBC", PCSTR(Name));

		AppendConfig(s, "ncp-disable");
	}
	else {
		CString Mode;

		switch( Info.GetUInt("ciphmode", 0, 0, 4) ) {

			case 0: Mode = "CBC"; break;
			case 1: Mode = "CFB1"; break;
			case 2: Mode = "CFB8"; break;
			case 3: Mode = "GCM"; break;
			case 4: Mode = "OFB"; break;
		}

		AppendConfig(s, "cipher %s-%s", PCSTR(Name), PCSTR(Mode));
	}

	switch( Info.GetUInt("auth", 0, 0, 3) ) {

		case 1: AppendConfig(s, "auth none"); break;
		case 2: AppendConfig(s, "auth SHA1"); break;
		case 3: AppendConfig(s, "auth SHA256"); break;
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyOpenMisc(CStringArray &s, CFaceInfo &Info)
{
	UINT comp = Info.GetUInt("compress", 0, 0, 2);

	if( comp >= 1 ) {

		AppendConfig(s, "comp-lzo");

		if( comp >= 2 ) {

			AppendConfig(s, "comp-noadapt");
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyOpenCredentials(CStringArray &s, DWORD &crc, CFaceInfo &Info)
{
	CString user = Info.GetValue("user", "");

	CString pass = Info.GetValue("pass", "");

	if( !user.IsEmpty() && !pass.IsEmpty() ) {

		CStringArray c;

		c.Append(user);

		c.Append(pass);

		SaveConfig("openvpn", CPrintf("%s-cred", Info.m_pBase), c);

		AppendConfig(s, "auth-user-pass /vap/opt/crimson/config/openvpn/%s-cred", Info.m_pBase);

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyOpenCustom(CStringArray &s, CJsonConfig *pCustom)
{
	if( pCustom ) {

		UINT p = s.GetCount();

		UINT c = pCustom->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRow = pCustom->GetChild(n);

			CString      Name = pRow->GetValue("name", "");

			CString      Data = pRow->GetValue("value", "");

			if( !Name.IsEmpty() ) {

				if( !Data.IsEmpty() ) {

					Name += " ";
				}

				if( pRow->GetValueAsUInt("force", 0, 0, 1) ) {

					for( UINT r = 0; r < p; r++ ) {

						if( s[r].StartsWith(Name) ) {

							s.Remove(r);

							r--;
							p--;
						}
					}
				}

				if( !Data.IsEmpty() ) {

					Name += Data;
				}

				AppendConfig(s, "%s", PCSTR(Name));
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyOpenClients(CStringArray &s, DWORD &crc, CFaceInfo &Info, CIpAddr const &LocMask)
{
	CJsonConfig *pClients  = Info.m_pFace->GetChild("clients");

	CJsonConfig *pNetworks = Info.m_pFace->GetChild("clinets");

	CZeroMap<CString, CStringArray *> Map;

	BOOL c2c = Info.GetUInt("c2c", 0, 0, 1);

	if( pClients ) {

		UINT c = pClients->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRow = pClients->GetChild(n);

			CString      Name = pRow->GetValue("name", "");

			CIpAddr      Addr = pRow->GetValueAsIp("ip", IP_EMPTY);

			if( !Name.IsEmpty() && !Addr.IsEmpty() ) {

				CStringArray *pFile = Map[Name];

				if( !pFile ) {

					pFile = New CStringArray;

					Map.Insert(Name, pFile);
				}

				CStringArray &client = *pFile;

				AppendConfig(client, "ifconfig-push %s %s", PCSTR(Addr.GetAsText()), PCSTR(LocMask.GetAsText()));
			}
		}
	}

	if( pNetworks ) {

		UINT c = pNetworks->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRow = pNetworks->GetChild(n);

			CString      Name = pRow->GetValue("name", "");

			CIpAddr      Addr = pRow->GetValueAsIp("addr", IP_EMPTY);

			CIpAddr      Mask = pRow->GetValueAsIp("mask", IP_EMPTY);

			if( !Name.IsEmpty() && !Addr.IsEmpty() && !Mask.IsEmpty() ) {

				CStringArray *pFile = Map[Name];

				if( !pFile ) {

					pFile = New CStringArray;

					Map.Insert(Name, pFile);
				}

				CStringArray &client = *pFile;

				AppendConfig(client, "iroute %s %s", PCSTR(Addr.GetAsText()), PCSTR(Mask.GetAsText()));

				if( c2c ) {

					AppendConfig(s, "push \"route %s %s\"", PCSTR(Addr.GetAsText()), PCSTR(Mask.GetAsText()));
				}

				AppendConfig(s, "route %s %s", PCSTR(Addr.GetAsText()), PCSTR(Mask.GetAsText()));
			}
		}
	}

	if( Map.GetCount() ) {

		for( INDEX i = Map.GetHead(); !Map.Failed(i); Map.GetNext(i) ) {

			CString const &Name  =  Map.GetName(i);

			CStringArray &client = *Map.GetData(i);

			SaveConfig(CPrintf("openvpn/%s-client", Info.m_pBase), Name, client);

			client.Append(Name);

			crc = GetCRC(client, crc);
		}

		if( c2c ) {

			AppendConfig(s, "client-to-client");
		}

		AppendConfig(s, "client-config-dir /vap/opt/crimson/config/openvpn/%s-client", Info.m_pBase);

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyOpenRoutes(CStringArray &s, CJsonConfig *pRoutes, bool fPush)
{
	if( pRoutes ) {

		UINT c = pRoutes->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRow = pRoutes->GetChild(n);

			CIpAddr Addr = pRow->GetValueAsIp("ip", IP_EMPTY);

			CIpAddr Mask = pRow->GetValueAsIp("mask", IP_EMPTY);

			if( !Addr.IsEmpty() ) {

				CPrintf r("route %s %s", PCSTR(Addr.GetAsText()), PCSTR(Mask.GetAsText()));

				if( fPush ) {

					AppendConfig(s, "push \"%s\"", PCSTR(r));
				}
				else {
					AppendConfig(s, "%s", PCSTR(r));
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyIpsecDeadPeer(CStringArray &s, CFaceInfo &Info)
{
	// TODO -- This needs to be configurable!!!

	if( Info.GetBool("deadpeer", TRUE) ) {

		s.Append(" dpddelay=30");

		s.Append(" dpdtimeout=120");

		s.Append(" dpdaction=restart");

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyIpsecPhase1(CStringArray &s, CFaceInfo &Info)
{
	UINT e = Info.GetUInt("p1enc", 0, 0, NOTHING);

	UINT a = Info.GetUInt("p1enc", 0, 0, NOTHING);

	BOOL p = Info.GetBool("pfs", TRUE);

	UINT d = Info.GetUInt("p1dh", 0, 0, NOTHING);

	AppendConfig(s, " ike=%s", PCSTR(GetPhase1(e, a, p, d)));

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyIpsecPhase2(CStringArray &s, CFaceInfo &Info)
{
	if( Info.GetUInt("p2type", 0, 0, 1) == 0 ) {

		UINT e = Info.GetUInt("p2enc", 0, 0, NOTHING);

		UINT a = Info.GetUInt("p2auth", 0, 0, NOTHING);

		AppendConfig(s, " esp=%s", PCSTR(GetEsp(e, a)));

		AppendConfig(s, " keylife=%um", Info.GetUInt("p2life", 60, 1, 1440));
	}
	else {
		UINT a = Info.GetUInt("p2ah", 0, 0, NOTHING);

		AppendConfig(s, " ah=%s", PCSTR(GetAh(a)));
	}

	return TRUE;
}

CString CLinuxNetApplicator::ApplyIpsecFace(CStringArray &s, CFaceInfo &Info)
{
	UINT uFace = Info.GetUInt("source", 0, 0, NOTHING);

	if( !uFace ) {

		AddDependency("eth0", Info.m_pName);

		return "c3-eth0-addr";
	}

	CString Base = GetFaceName(uFace);

	AddDependency(Base, Info.m_pName);

	return CPrintf("c3-%s-addr", PCSTR(Base));
}

BOOL CLinuxNetApplicator::ApplyIpsecAuth(CStringArray &s, DWORD &crc, CFaceInfo &Info, PCSTR pLoc, PCSTR pRem)
{
	if( Info.GetUInt("authmode", 0, 0, 1) == 0 ) {

		return ApplyIpsecAuthPsk(s, Info, pLoc, pRem);
	}

	return ApplyIpsecAuthCert(s, crc, Info, pLoc, pRem);
}

BOOL CLinuxNetApplicator::ApplyIpsecAuthPsk(CStringArray &s, CFaceInfo &Info, PCSTR pLoc, PCSTR pRem)
{
	CString Key = Info.GetValue("psk", "");

	if( !Key.IsEmpty() ) {

		CStringArray secret;

		s.Append(" authby=secret");

		AppendConfig(secret, "%s %s : PSK \"%s\"", pLoc, pRem, PCSTR(Key));

		CPrintf File("%s.secrets", Info.m_pName);

		SaveConfig("ipsec", File, secret);

		AppendConfig(m_IpSecSecrets, "include /vap/opt/crimson/config/ipsec/%s", PCSTR(File));

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyIpsecAuthCert(CStringArray &s, DWORD &crc, CFaceInfo &Info, PCSTR pLoc, PCSTR pRem)
{
	UINT LocSrc = Info.GetUInt("locsrc", 0, 0, 65535);

	UINT RemSrc = Info.GetUInt("remsrc", 0, 0, 65535);

	UINT uValid = 0;

	if( LocSrc >= 100 ) {

		if( SaveIdentityCert(crc, "ipsec", Info.m_pName, "loc.crt", "loc.key", "loc.pwd", LocSrc) ) {

			uValid++;
		}
	}
	else {
		CByteArray LocCert, LocPriv;

		Info.GetBlob("loccert", LocCert);

		Info.GetBlob("locpriv", LocPriv);

		if( !LocCert.IsEmpty() && !LocPriv.IsEmpty() ) {

			CString LocPass = Info.GetValue("locpass", "");

			SaveCertFile(crc, "ipsec", Info.m_pName, "loc.crt", LocCert);

			SaveCertFile(crc, "ipsec", Info.m_pName, "loc.key", LocPriv);

			SavePassFile(crc, "ipsec", Info.m_pName, "loc.pwd", LocPass);

			uValid++;
		}
	}

	if( RemSrc >= 100 ) {

		if( SaveTrustedCert(crc, "ipsec", Info.m_pName, "rem.crt", RemSrc) ) {

			uValid++;
		}
	}
	else {
		CByteArray RemCert;

		Info.GetBlob("remcert", RemCert);

		if( !RemCert.IsEmpty() ) {

			SaveCertFile(crc, "ipsec", Info.m_pName, "rem.crt", RemCert);

			uValid++;
		}
	}

	if( uValid == 2 ) {

		AppendConfig(s, " leftcert=%s-loc", Info.m_pName);

		AppendConfig(s, " rightcert=%s-rem", Info.m_pName);

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyIpsecOption(CStringArray &s, IPREF Addr, PCSTR pName)
{
	if( !Addr.IsEmpty() ) {

		AppendConfig(s, " %s=%s", pName, PCSTR(Addr.GetAsText()));

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyIpsecOption(CStringArray &s, PCSTR pData, PCSTR pName)
{
	if( *pData ) {

		AppendConfig(s, " %s=%s", pName, pData);

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyPeerCheck(CStringArray *s, CFaceInfo &Info, PCSTR pPeer, bool fEnable)
{
	CStringArray p;

	if( fEnable && pPeer && *pPeer ) {

		AppendConfig(p, "mode=%u", Info.GetUInt("check", 0, 0, 3));

		AppendConfig(p, "peer=%s", pPeer);
	}
	else {
		AppendConfig(p, "mode=0");
	}

	SaveConfig("net", CPrintf("%s-peer-conf", Info.m_pBase), p);

	AppendWatcher(s, "Peer", Info.m_pBase);

	return TRUE;
}

// IpSec Configuration

BOOL CLinuxNetApplicator::ApplyFinalIpSec(void)
{
	if( !m_IpSecConfig.IsEmpty() ) {

		CStringArray init, term;

		AppendConfig(init, "/opt/crimson/scripts/c3-ipsec start");

		AppendConfig(term, "/opt/crimson/scripts/c3-ipsec stop");

		SaveConfig("ipsec", "config", m_IpSecConfig);

		SaveConfig("ipsec", "secrets", m_IpSecSecrets);

		SaveScript("ipsec", "init", init);

		SaveScript("ipsec", "term", term);

		return TRUE;
	}

	return FALSE;
}
CString CLinuxNetApplicator::GetPhase1(UINT e, UINT a, BOOL p, UINT d)
{
	CString c = GetPhase1Enc(e) + "-" + GetPhase1Auth(a);

	if( p ) {

		c += "-" + GetPhase1Dh(d);
	}

	return c;
}

CString CLinuxNetApplicator::GetPhase1Enc(UINT n)
{
	switch( n ) {

		case 1: return "aes";
		case 2: return "aes128";
		case 4: return "aes256";
		case 8: return "3des";
	}

	return "aes";
}

CString CLinuxNetApplicator::GetPhase1Auth(UINT n)
{
	switch( n ) {

		case 1: return "md5";
		case 2: return "sha1";
		case 4: return "sha256";
		case 8: return "sha512";
	}

	return "sha1";
}

CString CLinuxNetApplicator::GetPhase1Dh(UINT n)
{
	switch( n ) {

		case   1: return "modp768";
		case   2: return "modp1024";
		case   4: return "modp1536";
		case   8: return "modp2048";
		case  16: return "modp3072";
		case  32: return "modp4096";
		case  64: return "modp6144";
		case 128: return "modp8192";
	}

	return "modp2048";
}

CString CLinuxNetApplicator::GetEsp(UINT e, UINT a)
{
	return GetEspEnc(e) + "-" + GetEspAuth(a);
}

CString CLinuxNetApplicator::GetEspEnc(UINT n)
{
	switch( n ) {

		case 1: return "aes128";
		case 2: return "aes128";
		case 4: return "aes256";
		case 8: return "3des";
	}

	return "aes";
}

CString CLinuxNetApplicator::GetEspAuth(UINT n)
{
	switch( n ) {

		case 1: return "md5";
		case 2: return "sha1";
		case 4: return "sha256";
	}

	return "sha1";
}

CString CLinuxNetApplicator::GetAh(UINT n)
{
	switch( n ) {

		case 1: return "hmac-md5-96";
		case 2: return "hmac-sha1-96";
	}

	return "hmac-sha1-96";
}

// Certificate Helpers

BOOL CLinuxNetApplicator::SaveTrustedRoots(DWORD &crc, PCSTR pSave, PCSTR pName, PCSTR pCert, UINT uCert)
{
	return FALSE;
}

BOOL CLinuxNetApplicator::SaveIdentityCert(DWORD &crc, PCSTR pSave, PCSTR pName, PCSTR pCert, PCSTR pPriv, PCSTR pPass, UINT uCert)
{
	if( m_pCertManager ) {

		CByteArray Cert, Priv;

		CString    Pass;

		if( m_pCertManager->GetIdentityCert(uCert, Cert, Priv, Pass) ) {

			CPrintf Save("/./tmp/crimson/config/%s", pSave);

			mkdir(Save, 0755);

			CPrintf FullCert("%s/%s-%s", PCSTR(Save), pName, pCert);

			CPrintf FullPriv("%s/%s-%s", PCSTR(Save), pName, pPriv);

			if( TRUE ) {

				CAutoFile FileCert(FullCert, "w");

				CAutoFile FilePriv(FullPriv, "w");

				FileCert.Write(Cert.data(), Cert.size());

				FilePriv.Write(Priv.data(), Priv.size());

				::crc(crc, Cert.data(), Cert.size());

				::crc(crc, Priv.data(), Priv.size());

				FileCert.Close();

				FilePriv.Close();

				chmod(FullCert, 0600);

				chmod(FullPriv, 0600);
			}

			if( pPass ) {

				CPrintf   FullPass("%s/%s-%s", PCSTR(Save), pName, pPass);

				CAutoFile FilePass(FullPass, "w");

				FilePass.Write(Pass.data(), Pass.size());

				FilePass.Close();

				chmod(FullPass, 0600);
			}
			else {
				if( Pass.GetLength() ) {

					::crc(crc, PCBYTE(Pass.data()), Pass.size());

					DecryptKeyFile(pSave, pName, pPriv, Pass);
				}
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::SaveTrustedCert(DWORD &crc, PCSTR pSave, PCSTR pName, PCSTR pCert, UINT uCert)
{
	return FALSE;
}

void CLinuxNetApplicator::SaveCertFile(DWORD &crc, PCSTR pSave, PCSTR pName, PCSTR pType, CByteArray const &Data)
{
	CPrintf Save("/./tmp/crimson/config/%s", pSave);

	CPrintf Full("%s/%s-%s", PCSTR(Save), pName, pType);

	mkdir(Save, 0755);

	CAutoFile File(Full, "w");

	File.Write(Data.data(), Data.size());

	File.Close();

	chmod(Full, 0600);
}

void CLinuxNetApplicator::SavePassFile(DWORD &crc, PCSTR pSave, PCSTR pName, PCSTR pType, CString const &Data)
{
	CPrintf Save("/./tmp/crimson/config/%s", pSave);

	CPrintf Full("%s/%s-%s", PCSTR(Save), pName, pType);

	mkdir(Save, 0755);

	CAutoFile File(Full, "w");

	File.Write(Data.data(), Data.size());
}

BOOL CLinuxNetApplicator::DecryptKeyFile(PCSTR pSave, PCSTR pName, PCSTR pType, PCSTR pPass)
{
	if( pPass && *pPass ) {

		AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

		if( pLinux ) {

			CPrintf Save("/./tmp/crimson/config/%s", pSave);

			CPrintf Full("%s/%s-%s", PCSTR(Save), pName, pType);

			CPrintf Args("rsa -in %s -out %s -passin pass:%s", PCSTR(Full), PCSTR(Full), pPass);

			if( pLinux->CallProcess("/usr/bin/openssl", Args, NULL, NULL) == 0 ) {

				return TRUE;
			}

			return FALSE;
		}
	}

	return TRUE;
}

// MAC Filter Control

BOOL CLinuxNetApplicator::ApplyMacFilter(void)
{
	m_FiltInit.Append("arptables -t filter --flush");

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyMacFilter(CStringArray *s, CFaceInfo &Info, bool arp)
{
	if( arp ) {

		UINT filter = (1 + Info.GetUInt("arpFilter", 0, 0, 2)) % 3;

		UINT proxy  = Info.GetUInt("arpProxy", 0, 0, 1);

		m_Init1.Append(CPrintf("sysctl -w net.ipv4.conf.%s.arp_ignore=%u", Info.m_pName, filter));

		m_Init1.Append(CPrintf("sysctl -w net.ipv4.conf.%s.proxy_arp=%u", Info.m_pName, proxy));
	}

	if( Info.GetUInt("macMode", 0, 0, 1) ) {

		CJsonConfig *pTable = Info.m_pFace->GetChild("macFilter");

		if( pTable ) {

			UINT  c = pTable->GetCount();

			UINT  d = Info.GetUInt("macDefault", 0, 0, 1);

			PCSTR a = d ? "ACCEPT" : "DROP";

			UINT  h = FALSE;

			for( UINT n = 0; n < c; n++ ) {

				CJsonConfig *pRow = pTable->GetChild(n);

				CString Mac = pRow->GetValue("mac", "");

				if( !Mac.IsEmpty() ) {

					CString Rule;

					Rule.Printf("arptables -A INPUT -i %s --src-mac %s -j %s", Info.m_pName, PCSTR(Mac), a);

					m_FiltInit.Append(Rule);

					h = TRUE;
				}
			}

			if( d == 1 && h ) {

				CString Rule;

				Rule.Printf("arptables -A INPUT -i %s -j DROP", Info.m_pName);

				m_FiltInit.Append(Rule);
			}
		}

		return TRUE;
	}

	return FALSE;
}

// Firewall Control

BOOL CLinuxNetApplicator::ApplyFirewall(CJsonConfig *pFirewall)
{
	m_FireHead.Append("iptables -t filter --flush");

	m_FireHead.Append("iptables -t filter -X");

	m_FireHead.Append("iptables -t nat --flush");

	m_FireHead.Append("iptables -t nat -X");

	m_FireHead.Append("iptables -t mangle --flush");

	m_FireHead.Append("iptables -t mangle -X");

	m_FireHead.Append("iptables -A INPUT -m state --state INVALID -j DROP");

	m_FireHead.Append("iptables -A FORWARD -m state --state INVALID -j DROP");

	m_FireHead.Append("iptables -A OUTPUT -m state --state INVALID -j DROP");

	m_UntrustedRules.Empty();

	m_UntrustedFaces.Empty();

	m_WhiteLists.Empty();

	m_AutoIps.Empty();

	m_fRoutingRules = FALSE;

	if( pFirewall ) {

		ApplyFirewallLists(pFirewall->GetChild("lists"));

		ApplyFirewallFilter(pFirewall->GetChild("filter"));

		ApplyFirewallForward(pFirewall->GetChild("forward"));

		ApplyFirewallNat(pFirewall->GetChild("nat"));

		ApplyFirewallInbound(pFirewall->GetChild("inbound"));

		ApplyFirewallCustom(pFirewall->GetChild("custom"));
	}
	else {
		ApplyFirewallInboundDefault();
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallLists(CJsonConfig *pLists)
{
	if( pLists ) {

		ApplyFirewallWhiteLists(pLists->GetChild("wlist"));

		ApplyFirewallBlackList(pLists->GetChild("blist"));
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallWhiteLists(CJsonConfig *pWhite)
{
	if( pWhite ) {

		CMap<CString, UINT> Used;

		UINT c = pWhite->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRule = pWhite->GetChild(n);

			UINT    uKey = pRule->GetValueAsUInt("key", 0, 0, NOTHING);

			CString Name = pRule->GetValue("name", "");

			CIpAddr Addr = pRule->GetValueAsIp("ip", IP_EMPTY);

			CIpAddr Mask = pRule->GetValueAsIp("mask", IP_EMPTY);

			CString Full = GetFullAddr(Addr, Mask);

			INDEX   Find = Used.FindName(Name);

			if( !Used.Failed(Find) ) {

				UINT  m = Used.GetData(Find);

				INDEX j = m_WhiteLists.FindName(m);

				m_WhiteLists.SetData(j, m_WhiteLists.GetData(j) + "," + Full);

				m_WhiteLists.Insert(uKey, CPrintf("=%u", m));
			}
			else {
				m_WhiteLists.Insert(uKey, Full);

				Used.Insert(Name, uKey);
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyFirewallBlackList(CJsonConfig *pBlack)
{
	if( pBlack ) {

		UINT c = pBlack->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRule = pBlack->GetChild(n);

			CIpAddr Addr = pRule->GetValueAsIp("ip", IP_EMPTY);

			CIpAddr Mask = pRule->GetValueAsIp("mask", IP_EMPTY);

			if( !Mask.IsEmpty() ) {

				CPrintf r1("iptables -A FORWARD -s %s -j DROP", PCSTR(GetFullAddr(Addr, Mask)));

				CPrintf r2("iptables -A INPUT -s %s -j DROP", PCSTR(GetFullAddr(Addr, Mask)));

				m_FireInit.Append(r1);

				m_FireInit.Append(r2);
			}

		}

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyFirewallFilter(CJsonConfig *pFilter)
{
	if( pFilter ) {

		CJsonConfig *pList = pFilter->GetChild("list");

		if( pList ) {

			UINT c = pList->GetCount();

			for( UINT n = 0; n < c; n++ ) {

				CJsonConfig *pRule = pList->GetChild(n);

				UINT    uDstFace = pRule->GetValueAsUInt("dface", 0, 0, NOTHING);

				UINT    uSrcFace = pRule->GetValueAsUInt("sface", 0, 0, NOTHING);

				CIpAddr DstAddr  = pRule->GetValueAsIp("dip", IP_EMPTY);

				CIpAddr DstMask  = pRule->GetValueAsIp("dmask", IP_EMPTY);

				UINT    uList    = pRule->GetValueAsUInt("wlist", 0, 0, NOTHING);

				CString DstName(GetFaceName(uDstFace));

				CString SrcName(GetFaceName(uSrcFace));

				CPrintf Rule("iptables -A FORWARD -i %s -o %s", PCSTR(SrcName), PCSTR(DstName));

				if( !DstMask.IsEmpty() ) {

					Rule.AppendPrintf(" -d %s", PCSTR(GetFullAddr(DstAddr, DstMask)));
				}

				AddAccept(m_FireInit, Rule, uList);

				m_fRoutingRules = TRUE;
			}
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallCustom(CJsonConfig *pCustom)
{
	if( pCustom ) {

		CJsonConfig *pList = pCustom->GetChild("list");

		if( pList ) {

			UINT c = pList->GetCount();

			for( UINT n = 0; n < c; n++ ) {

				CJsonConfig *pRule = pList->GetChild(n);

				CString Table;

				CString Chain;

				switch( pRule->GetValueAsUInt("table", 0, 0, 2) ) {

					case 0: Table = "filter"; break;
					case 1: Table = "nat";    break;
					case 2: Table = "mangle"; break;
				}

				switch( pRule->GetValueAsUInt("chain", 0, 0, 4) ) {

					case 0: Chain = "INPUT";       break;
					case 1: Chain = "FORWARD";     break;
					case 2: Chain = "OUTPUT";      break;
					case 3: Chain = "PREROUTING";  break;
					case 4: Chain = "POSTROUTING"; break;
				}

				if( !Table.IsEmpty() && !Chain.IsEmpty() ) {

					CString Rule(pRule->GetValue("rule", ""));

					if( !Rule.IsEmpty() ) {

						AppendConfig(m_FireInit, "iptables -t %s -A %s %s", PCSTR(Table), PCSTR(Chain), PCSTR(Rule));
					}
				}
			}
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallInbound(CJsonConfig *pIn)
{
	if( pIn ) {

		ApplyFirewallInboundPreDefined(pIn->GetChild("predef"));

		ApplyFirewallInboundCustom(pIn->GetChild("custom"));
	}
	else {
		ApplyFirewallInboundDefault();
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallInboundDefault(void)
{
	// TODO -- Defaults need better handling, as this assumes
	// knowledge of how the default JSON will be configured.

	PCSTR def[] = { "ipsec", "nat", "open" };

	for( UINT n = 0; n < elements(def); n++ ) {

		UINT uDone = AddRule(m_UntrustedRules, GetRule(def[n]), "%s", 0);

		while( uDone-- ) {

			m_UntrustedFaces.Append(0);
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallInboundPreDefined(CJsonConfig *pPre)
{
	if( pPre ) {

		UINT c = pPre->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRule = pPre->GetChild(n);

			if( pRule->GetValueAsBool("enable", TRUE) ) {

				CString Rule = GetRule(pRule->GetValue("key", ""));

				if( !Rule.IsEmpty() ) {

					UINT uList = pRule->GetValueAsUInt("wlist", 0, 0, NOTHING);

					UINT uDone = AddRule(m_UntrustedRules, Rule, "%s", uList);

					while( uDone-- ) {

						m_UntrustedFaces.Append(0);
					}
				}
			}
		}
	}
	else {
		ApplyFirewallInboundDefault();
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallInboundCustom(CJsonConfig *pCustom)
{
	if( pCustom ) {

		UINT c = pCustom->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRule = pCustom->GetChild(n);

			UINT uFace = pRule->GetValueAsUInt("face", 0, 0, NOTHING);

			UINT uProt = pRule->GetValueAsUInt("prot", 0, 0, 3);

			UINT uFrom = pRule->GetValueAsUInt("sport", 0, 0, NOTHING);

			UINT uLast = pRule->GetValueAsUInt("eport", uFrom, uFrom, NOTHING);

			CPrintf Range((uFrom == uLast) ? "%u" : "%u:%u", uFrom, uLast);

			for( UINT p = 0; p < 3; p++ ) {

				CString Rule;

				if( p == 0 && ((uProt+1) & 1) ) {

					Rule.Printf("iptables -A INPUT -i %%s -p tcp --dport %s -m state --state NEW", PCSTR(Range));
				}

				if( p == 1 && ((uProt+1) & 2) ) {

					Rule.Printf("iptables -A INPUT -i %%s -p udp --dport %s", PCSTR(Range));
				}

				if( p == 2 && ((uProt+1) & 4) ) {

					Rule.Printf("iptables -A INPUT -i %%s -p icmp");
				}

				if( !Rule.IsEmpty() ) {

					UINT uList = pRule->GetValueAsUInt("wlist", 0, 0, NOTHING);

					AddAccept(m_UntrustedRules, Rule, uList);

					m_UntrustedFaces.Append(uFace);
				}
			}
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallForward(CJsonConfig *pForward)
{
	if( pForward ) {

		CJsonConfig *pList = pForward->GetChild("list");

		if( pList ) {

			UINT c = pList->GetCount();

			for( UINT n = 0; n < c; n++ ) {

				CJsonConfig *pRule = pList->GetChild(n);

				UINT    uInFace = pRule->GetValueAsUInt("face", 0, 0, NOTHING);

				CIpAddr InAddr  = pRule->GetValueAsIp("iip", IP_EMPTY);

				UINT    uInPort = pRule->GetValueAsUInt("iport", 0, 0, NOTHING);

				UINT    uProt   = pRule->GetValueAsUInt("prot", 0, 0, 2);

				UINT    uToPort = pRule->GetValueAsUInt("dport", uInPort, 0, NOTHING);

				CIpAddr ToAddr  = pRule->GetValueAsIp("dip", IP_EMPTY);

				UINT    uList   = pRule->GetValueAsUInt("wlist", 0, 0, NOTHING);

				CString AddrI, FaceI, FaceO;

				if( !InAddr.IsEmpty() ) {

					AddrI.AppendPrintf(" -d %s", PCTXT(InAddr.GetAsText()));
				}

				if( uInFace ) {

					FaceI.AppendPrintf(" -i %s", PCTXT(GetFaceName(uInFace)));

					FaceO.AppendPrintf(" -o %s", PCTXT(GetFaceName(uInFace)));
				}

				for( UINT p = 0; p < 2; p++ ) {

					CString Prot;

					if( p == 0 && ((uProt+1) & 1) ) {

						Prot = "tcp";
					}

					if( p == 1 && ((uProt+1) & 2) ) {

						Prot = "udp";
					}

					if( !Prot.IsEmpty() ) {

						CString t1("iptables -t nat -A PREROUTING%s%s -p %s --dport %u -j DNAT --to %s:%u");

						CString t2("iptables -t nat -A POSTROUTING -p %s -d %s --dport %u -j MASQUERADE");

						CString t3("iptables -A FORWARD%s -p %s -d %s --dport %u");

						CString t4("iptables -A FORWARD%s -p %s -s %s --sport %u -m state --state RELATED,ESTABLISHED");

						CPrintf r1(t1, PCSTR(FaceI), PCSTR(AddrI), PCSTR(Prot), uInPort, PCSTR(ToAddr.GetAsText()), uToPort);

						CPrintf r2(t2, PCSTR(Prot), PCSTR(ToAddr.GetAsText()), uToPort);

						CPrintf r3(t3, PCSTR(FaceI), PCSTR(Prot), PCSTR(ToAddr.GetAsText()), uToPort);

						CPrintf r4(t4, PCSTR(FaceO), PCSTR(Prot), PCSTR(ToAddr.GetAsText()), uToPort);

						m_FireInit.Append(r1);

						m_FireInit.Append(r2);

						AddAccept(m_FireInit, r3, uList);

						AddAccept(m_FireInit, r4, uList);
					}
				}
			}
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallNat(CJsonConfig *pForward)
{
	if( pForward ) {

		ApplyFirewallNatHost(pForward->GetChild("host"));

		ApplyFirewallNatDmz(pForward->GetChild("dmz"));
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallNatHost(CJsonConfig *pHost)
{
	if( pHost ) {

		UINT c = pHost->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRule = pHost->GetChild(n);

			UINT    uFace = pRule->GetValueAsUInt("oface", 0, 0, NOTHING);

			UINT    uProt = pRule->GetValueAsUInt("prot", 0, 0, 3);

			CIpAddr From  = pRule->GetValueAsIp("oip", IP_EMPTY);

			CIpAddr Dest  = pRule->GetValueAsIp("nip", IP_EMPTY);

			UINT    uList = pRule->GetValueAsUInt("wlist", 0, 0, NOTHING);

			BOOL    fBidi = pRule->GetValueAsBool("dir", FALSE);

			BOOL    fMasq = pRule->GetValueAsBool("masq", FALSE);

			UINT    uSpan = pRule->GetValueAsUInt("count", 1, 1, 65536);

			if( uFace && uSpan <= 32 ) {

				INDEX   Find = m_AutoIps.FindName(uFace);

				CString Text;

				for( UINT s = 0; s < uSpan; s++ ) {

					if( s ) {

						Text += '|';
					}

					Text += (From + s).GetAsText();
				}

				if( !m_AutoIps.Failed(Find) ) {

					Text += '|';

					Text += m_AutoIps.GetData(Find);

					m_AutoIps.SetData(Find, Text);
				}
				else {
					m_AutoIps.Insert(uFace, Text);
				}
			}

			for( UINT s = 0; s < uSpan; s++ ) {

				CString ff = (From + s).GetAsText();

				CString dd = (Dest + s).GetAsText();

				PCSTR   pf = PCSTR(ff);

				PCSTR   pd = PCSTR(dd);

				for( UINT p = 0; p < 3; p++ ) {

					CString Prot;

					if( p == 0 && uProt == 2 ) {

						Prot = "all";
					}
					else {
						if( p == 0 && ((uProt+1) & 1) ) {

							Prot = "tcp";
						}

						if( p == 1 && ((uProt+1) & 2) ) {

							Prot = "udp";
						}

						if( p == 2 ) {

							Prot = "icmp";
						}
					}

					if( !Prot.IsEmpty() ) {

						CString t1("iptables -t nat -A PREROUTING -p %s -d %s -j DNAT --to %s");

						CString t2("iptables -A FORWARD -p %s -d %s");

						CPrintf r1(t1, PCSTR(Prot), pf, pd);

						CPrintf r2(t2, PCSTR(Prot), pd);

						m_FireInit.Append(r1);

						AddAccept(m_FireInit, r2, uList);

						if( uList ) {

							CString t1("iptables -A FORWARD -p %s -d %s -j DROP");

							CPrintf r1(t1, PCSTR(Prot), pd);

							m_FireInit.Append(r1);
						}

						if( fMasq ) {

							CString t1("iptables -t nat -A POSTROUTING -p %s -d %s -j MASQUERADE");

							CPrintf r1(t1, PCSTR(Prot), pd);

							m_FireInit.Append(r1);
						}

						if( fBidi ) {

							CString t1("iptables -t nat -A POSTROUTING -p %s -s %s -j SNAT --to %s");

							CString t2("iptables -A FORWARD -p %s -s %s");

							CPrintf r1(t1, PCSTR(Prot), pd, pf);

							CPrintf r2(t2, PCSTR(Prot), pd);

							m_FireInit.Append(r1);

							AddAccept(m_FireInit, r2, 0);
						}
						else {
							CString t1("iptables -A FORWARD -p %s -s %s -m state --state RELATED,ESTABLISHED");

							CString t2("iptables -A FORWARD -p %s -s %s -j DROP");

							CPrintf r1(t1, PCSTR(Prot), pd);

							CPrintf r2(t2, PCSTR(Prot), pd);

							AddAccept(m_FireInit, r1, 0);

							m_FireInit.Append(r2);
						}
					}

					if( uProt == 2 ) {

						break;
					}
				}
			}
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFirewallNatDmz(CJsonConfig *pDmz)
{
	if( pDmz ) {

		UINT c = pDmz->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRule = pDmz->GetChild(n);

			UINT    uFace = pRule->GetValueAsUInt("face", 0, 0, NOTHING);

			CIpAddr Dest  = pRule->GetValueAsIp("nip", IP_EMPTY);

			UINT    uList = pRule->GetValueAsUInt("wlist", 0, 0, NOTHING);

			CString t1("iptables -t nat -A PREROUTING -i %s -j DNAT --to %s");

			CString t2("iptables -A FORWARD -i %s");

			CString t3("iptables -A FORWARD -o %s");

			CPrintf r1(t1, PCSTR(GetFaceName(uFace)), PCSTR(Dest.GetAsText()));

			CPrintf r2(t2, PCSTR(GetFaceName(uFace)));

			CPrintf r3(t3, PCSTR(GetFaceName(uFace)));

			m_FireInit.Append(r1);

			AddAccept(m_FireInit, r2, uList);

			AddAccept(m_FireInit, r3, uList);
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplySharing(CStringArray *s, CFaceInfo &Info)
{
	if( !m_Sharers.IsEmpty() ) {

		if( Info.GetUInt("internet", 0, 0, 2) == 1 ) {

			AppendConfig(m_FireInit, "iptables -t mangle -A FORWARD -i %s -j MARK --set-mark 1000", Info.m_pName);

			for( UINT n = 0; n < m_Sharers.GetCount(); n++ ) {

				PCSTR pFrom = m_Sharers[n];

				AppendConfig(m_FireInit, "iptables -A FORWARD -i %s -o %s -j ACCEPT", Info.m_pName, pFrom);

				AppendConfig(m_FireInit, "iptables -A FORWARD -i %s -o %s -m state --state RELATED,ESTABLISHED -j ACCEPT", pFrom, Info.m_pName);
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyMonitor(CStringArray *s, CFaceInfo &Info)
{
	UINT mode;

	if( (mode = Info.GetUInt("checkMode", 0, 0, 3)) ) {

		CPrintf Conf("%s-fade-conf", Info.m_pBase);

		CStringArray c;

		UINT manual = Info.GetUInt("checkManual", 0, 0, 1);

		AppendConfig(c, "mode=%u", mode);

		AppendConfig(c, "manual=%u", manual);

		AppendConfig(c, "watch=%s", Info.m_pName);

		AppendConfig(c, "action=%u", Info.GetUInt("checkAction", 0, 0, 1));

		AppendConfig(c, "idle=%u", Info.GetUInt("checkIdle", 0, 0, 1));

		if( manual ) {

			AppendConfig(c, "name=%s", PCSTR(Info.GetValue("checkName", "")));

			AppendConfig(c, "period=%u", Info.GetUInt("checkPeriod", 0, 0, 600));

			AppendConfig(c, "delay=%u", Info.GetUInt("checkDelay", 0, 0, 600));

			AppendConfig(c, "burst=%u", Info.GetUInt("checkBurst", 0, 0, 20));

			AppendConfig(c, "success=%u", Info.GetUInt("checkSuccess", 0, 0, 20));

			AppendConfig(c, "failure=%u", Info.GetUInt("checkFailure", 0, 0, 20));
		}

		AppendProgram(s, scriptMake, CPrintf("LinkFader -i %s", Info.m_pBase), "fade", Info.m_pBase);

		SaveConfig("net", Conf, c);

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyVrrp(CStringArray *s, CFaceInfo &Info)
{
	if( Info.GetUInt("vrrpEnable", 0, 0, 1) ) {

		CIpAddr addr = Info.GetIp("vrrpAddress", IP_EMPTY);

		if( !addr.IsEmpty() ) {

			UINT group    = Info.GetUInt("vrrpGroup", 1, 1, 255);

			UINT priority = Info.GetUInt("vrrpPriority", 100, 0, 65535);

			UINT notify   = Info.GetUInt("vrrpNotify", 1, 1, 600);

			CPrintf Program("vrrpd -i %s -v %u -p %u -d %u -n 1 %s",
					Info.m_pBase,
					group,
					priority,
					notify,
					PCSTR(addr.GetAsText())
			);

			AppendProgram(s, scriptMake, Program, "vrrp", Info.m_pBase);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyIptRules(CStringArray *s, CFaceInfo &Info, CString const &Bind, UINT mark, BOOL gate)
{
	CJsonConfig *pRules = Info.m_pFace->GetChild("iptfwd");

	UINT         icmp   = Info.GetUInt("iptIcmpMode", 1, 0, 2);

	if( pRules || icmp ) {

		AddSubTable(s, "filter", "INPUT", Info.m_pBase);

		AddSubTable(s, "filter", "FORWARD", Info.m_pBase);

		AddSubTable(s, "nat", "PREROUTING", Info.m_pBase);

		AddSubTable(s, "mangle", "OUTPUT", Info.m_pBase);

		CString t1("iptables -t mangle -A OUT_%s -m conntrack --ctstate DNAT --ctorigdst $priaddr -j MARK --set-mark %u");

		CPrintf r1(t1, Info.m_pBase, mark);
	}

	if( gate ) {

		AddSubTable(s, "nat", "POSTROUTING", Info.m_pBase);

		if( Info.GetUInt("internet", 0, 0, 2) == 2 ) {

			CString t1("iptables -t nat -A POS_%s -m mark --mark 1000 -j SNAT --to $priaddr");

			CPrintf r1(t1, Info.m_pBase);

			AppendScript(s, scriptMake, r1);
		}

		CString t1("iptables -t nat -A POS_%s -s $secaddr -j SNAT --to $priaddr");

		CPrintf r1(t1, Info.m_pBase);

		CString t2("iptables -t nat -A POS_%s -s $priaddr -j SNAT --to $priaddr");

		CPrintf r2(t2, Info.m_pBase);

		AppendScript(s, scriptMake, r1);

		AppendScript(s, scriptMake, r2);
	}
	else {
		AddSubTable(s, "filter", "OUTPUT", Info.m_pBase);

		CString t1("iptables -t filter -A OUT_%s -s $secaddr -m mark --mark %u -j ACCEPT");

		CPrintf r1(t1, Info.m_pBase, mark);

		CString t2("iptables -t filter -A OUT_%s -s $secaddr -j DROP");

		CPrintf r2(t2, Info.m_pBase);

		AppendScript(s, scriptMake, r1);

		AppendScript(s, scriptMake, r2);
	}

	if( pRules ) {

		for( UINT n = 0; n < pRules->GetCount(); n++ ) {

			CJsonConfig *pRule = pRules->GetChild(n);

			UINT uInPort = pRule->GetValueAsUInt("iport", 0, 0, NOTHING);

			UINT uProt   = pRule->GetValueAsUInt("prot", 0, 0, 2);

			UINT uToPort = pRule->GetValueAsUInt("dport", uInPort, 0, NOTHING);

			BOOL fRouter = pRule->GetValueAsBool("target", FALSE);

			UINT uList   = pRule->GetValueAsUInt("wlist", 0, 0, NOTHING);

			for( UINT p = 0; p < 2; p++ ) {

				CString Prot;

				if( p == 0 && ((uProt+1) & 1) ) {

					Prot = "tcp";
				}

				if( p == 1 && ((uProt+1) & 2) ) {

					Prot = "udp";
				}

				if( !Prot.IsEmpty() ) {

					if( fRouter ) {

						CString t1("iptables -t nat -A PRE_%s -i %s -p %s -d $priaddr --dport %u -j DNAT --to $secaddr:%u");

						CString t2("iptables -A INP_%s -i %s -p %s --dport %u");

						CPrintf r1(t1, Info.m_pBase, Info.m_pBase, PCSTR(Prot), uInPort, uToPort);

						CPrintf r2(t2, Info.m_pBase, Info.m_pBase, PCSTR(Prot), uToPort);

						AppendScript(s, scriptMake, r1);

						AddAccept(s[scriptMake], r2, uList);
					}
					else {
						if( uInPort != uToPort ) {

							CString t1("iptables -t nat -A PRE_%s -i %s -p %s -d $priaddr --dport %u -j DNAT --to $priaddr:%u");

							CPrintf r1(t1, Info.m_pBase, Info.m_pBase, PCSTR(Prot), uInPort, uToPort);

							AppendScript(s, scriptMake, r1);
						}

						CString t2("iptables -A FOR_%s -i %s -o %s -p %s --dport %u");

						CPrintf r2(t2, Info.m_pBase, Info.m_pBase, PCSTR(Bind), PCSTR(Prot), uToPort);

						AddAccept(s[scriptMake], r2, uList);
					}
				}
			}
		}
	}

	if( icmp == 2 ) {

		UINT uList = Info.GetUInt("iptIcmpList", 0, 0, NOTHING);

		CString t1("iptables -t nat -A PRE_%s -i %s -p icmp -d $priaddr -j DNAT --to $secaddr");

		CString t2("iptables -A INP_%s -i %s -p icmp");

		CPrintf r1(t1, Info.m_pBase, Info.m_pBase);

		CPrintf r2(t2, Info.m_pBase, Info.m_pBase);

		AppendScript(s, scriptMake, r1);

		AddAccept(s[scriptMake], r2, uList);
	}

	if( !Info.GetUInt("peerDNS", 0, 0, 1) ) {

		AddRule(m_FireInit, GetRule("dns"), PCSTR(Bind), 0);
	}

	AddRule(m_FireInit, GetRule("dhcpd"), PCSTR(Bind), 0);

	AppendConfig(m_FireInit, "iptables -A INPUT -i %s -m state --state RELATED,ESTABLISHED -j ACCEPT", Info.m_pName);

	AppendConfig(m_FireInit, "iptables -A INPUT -i %s -m state --state RELATED,ESTABLISHED -j ACCEPT", PCSTR(Bind));

	AppendConfig(m_FireInit, "iptables -A INPUT -i %s -j DROP", Info.m_pName);

	AppendConfig(m_FireInit, "iptables -A INPUT -i %s -j DROP", PCSTR(Bind));

	if( IsTrusted(Info.m_pFace) ) {

		if( icmp == 0 ) {

			AppendConfig(m_FireInit, "iptables -A FORWARD -i %s -o %s -p icmp -j DROP", Info.m_pName, PCSTR(Bind));
		}

		AppendConfig(m_FireInit, "iptables -A FORWARD -i %s -o %s -j ACCEPT", Info.m_pName, PCSTR(Bind));

		AppendConfig(m_FireInit, "iptables -A FORWARD -i %s -o %s -j ACCEPT", PCSTR(Bind), Info.m_pName);
	}
	else {
		if( icmp == 1 ) {

			AppendConfig(m_FireInit, "iptables -A FORWARD -i %s -o %s -p icmp -j ACCEPT", Info.m_pName, PCSTR(Bind));
		}

		AppendConfig(m_FireInit, "iptables -A FORWARD -i %s -o %s -m state --state RELATED,ESTABLISHED -j ACCEPT", Info.m_pName, PCSTR(Bind));

		AppendConfig(m_FireInit, "iptables -A FORWARD -i %s -o %s -j ACCEPT", PCSTR(Bind), Info.m_pName);
	}

	AppendConfig(m_FireInit, "iptables -A FORWARD -i %s -j DROP", Info.m_pName);

	AppendConfig(m_FireInit, "iptables -A FORWARD -i %s -j DROP", PCSTR(Bind));

	AppendConfig(m_FireInit, "iptables -A FORWARD -o %s -j DROP", Info.m_pName);

	AppendConfig(m_FireInit, "iptables -A FORWARD -o %s -j DROP", PCSTR(Bind));

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyUntrustedRules(CStringArray *s, CFaceInfo &Info)
{
	if( !IsTrusted(Info.m_pFace) ) {

		UINT face = GetFaceId(Info.m_Name);

		for( UINT n = 0; n < m_UntrustedRules.GetCount(); n++ ) {

			if( !m_UntrustedFaces[n] || m_UntrustedFaces[n] == face ) {

				AppendConfig(m_FireInit, PCSTR(m_UntrustedRules[n]), Info.m_pName);
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyTrafficRules(CStringArray *s, CFaceInfo &Info)
{
	AppendConfig(m_FireInit, "iptables -t mangle -A PREROUTING -i %s -j ACCEPT", Info.m_pName);

	AppendConfig(m_FireInit, "iptables -t mangle -A POSTROUTING -o %s -j ACCEPT", Info.m_pName);

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFinalRules(CStringArray *s, CFaceInfo &Info)
{
	if( !CanAcceptInput(Info.m_pFace) ) {

		AppendConfig(m_FireInit, "iptables -A INPUT -i %s -m state --state RELATED,ESTABLISHED -j ACCEPT", Info.m_pName);

		AppendConfig(m_FireInit, "iptables -A INPUT -i %s -j DROP", Info.m_pName);
	}

	if( !CanForwardData(Info.m_pFace) ) {

		AppendConfig(m_FireLast, "iptables -A FORWARD -i %s -j DROP", Info.m_pName);
	}

	ApplyTrafficRules(s, Info);

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFinalRules(void)
{
	return TRUE;
}

BOOL CLinuxNetApplicator::AddSubTable(CStringArray *s, PCSTR pTable, PCSTR pChain, PCSTR pFace)
{
	AppendConfig(m_FireInit, "iptables -t %s -N %3.3s_%s", pTable, pChain, pFace);

	if( !strcmp(pChain, "INPUT") || !strcmp(pChain, "FORWARD") || !strcmp(pChain, "PREROUTING") ) {

		AppendConfig(m_FireInit, "iptables -t %s -A %s -i %s -j %3.3s_%s", pTable, pChain, pFace, pChain, pFace);
	}
	else {
		if( !strcmp(pChain, "POSTROUTING") ) {

			AppendConfig(m_FireInit, "iptables -t %s -A %s -o %s -j %3.3s_%s", pTable, pChain, pFace, pChain, pFace);
		}
		else {
			AppendConfig(m_FireInit, "iptables -t %s -A %s -j %3.3s_%s", pTable, pChain, pChain, pFace);
		}
	}

	AppendScript(s, scriptBreak, "iptables -t %s --flush %3.3s_%s", pTable, pChain, pFace);

	return TRUE;
}

UINT CLinuxNetApplicator::AddRule(CStringArray &List, CString Rule, PCSTR pFace, UINT uList)
{
	UINT uCount = 0;

	CStringArray Subs;

	Rule.Tokenize(Subs, ';');

	for( UINT s = 0; s < Subs.GetCount(); s++ ) {

		CString Rule;

		CString Sub  = Subs[s];

		CString Prot = Sub.StripToken('=');

		if( Prot == "udp" || Prot == "tcp" ) {

			CStringArray Ports;

			Sub.Tokenize(Ports, ',');

			for( UINT p = 0; p < Ports.GetCount(); p++ ) {

				CString Temp;

				if( Prot == "tcp" ) {

					Temp = "iptables -A INPUT -i %s -p tcp --dport %s -m state --state NEW";
				}

				if( Prot == "udp" ) {

					Temp = "iptables -A INPUT -i %s -p udp --dport %s";
				}

				Rule.Printf(Temp, pFace, PCSTR(Ports[p]));

				AddAccept(List, Rule, uList);

				uCount++;
			}
		}

		if( Prot == "icmp" ) {

			Rule.Printf("iptables -A INPUT -i %s -p icmp", pFace);

			AddAccept(List, Rule, uList);

			uCount++;
		}
	}

	return uCount;
}

BOOL CLinuxNetApplicator::AddAccept(CStringArray &List, CString Rule, UINT uList)
{
	if( !Rule.IsEmpty() ) {

		if( uList ) {

			INDEX i = m_WhiteLists.FindName(uList);

			if( !m_WhiteLists.Failed(i) ) {

				CString Full = m_WhiteLists.GetData(i);

				if( Full[0] == '=' ) {

					return AddAccept(List, Rule, atoi(PCSTR(Full)+1));
				}

				Rule += " -s ";

				Rule += Full;
			}
		}

		List.Append(Rule + " -j ACCEPT");

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::CanAcceptInput(CJsonConfig *pFace)
{
	return IsTrusted(pFace);
}

BOOL CLinuxNetApplicator::CanForwardData(CJsonConfig *pFace)
{
	return IsTrusted(pFace) && !m_fRoutingRules;
}

BOOL CLinuxNetApplicator::FindSharingProviders(CJsonConfig *pFaces)
{
	CJsonData *pData = pFaces->GetJsonData();

	for( INDEX i = pData->GetHead(); !pData->Failed(i); pData->GetNext(i) ) {

		CJsonConfig *pFace = pFaces->GetChild(i);

		if( pFace->GetValueAsUInt("internet", 0, 0, 2) == 2 ) {

			UINT mode = pFace->GetValueAsUInt("mode", 1, 0, 65535);

			if( mode == 1 || mode == 2 ) {

				CString Name = pData->GetName(i);

				AppendConfig(m_FireInit, "iptables -t nat -A POSTROUTING -m mark --mark 1000 -o %s -j MASQUERADE", PCSTR(Name));

				m_Sharers.Append(Name);
			}

			if( mode == 5 ) {

				CString Name = pData->GetName(i);

				m_Sharers.Append(Name);
			}
		}
	}

	return !m_Sharers.IsEmpty();
}

// Rule Generation

CString CLinuxNetApplicator::GetRule(CString Key)
{
	CString Rule;

	if( Key == "ping" )   Rule = "icmp";
	if( Key == "ssh" )    Rule = "tcp=22";
	if( Key == "telnet" ) Rule = "tcp=23";
	if( Key == "web" )    Rule = "tcp=80,443,8080,4443";
	if( Key == "c3" )     Rule = "tcp=789";
	if( Key == "dhcpd" )  Rule = "udp=67";
	if( Key == "dhcpc" )  Rule = "udp=68";
	if( Key == "mdns" )   Rule = "udp=5353";
	if( Key == "llmnr" )  Rule = "udp=5355";
	if( Key == "ntp" )    Rule = "udp=123";
	if( Key == "dns" )    Rule = "udp=53";
	if( Key == "snmp" )   Rule = "udp=160,161";
	if( Key == "ipsec" )  Rule = "udp=500";
	if( Key == "nat" )    Rule = "udp=4500";
	if( Key == "open" )   Rule = "udp=1194;tcp=1194";
	if( Key == "svm" )    Rule = "tcp=7785";

	return Rule;
}

// Script Helpers

BOOL CLinuxNetApplicator::SaveFaceScripts(PCSTR pName, CStringArray const *s)
{
	SaveScript("net", CString(pName) + ".up", s[scriptUp]);

	SaveScript("net", CString(pName) + ".dn", s[scriptDn]);

	SaveScript("net", CString(pName) + ".start", s[scriptStart]);

	SaveScript("net", CString(pName) + ".stop", s[scriptStop]);

	SaveScript("net", CString(pName) + ".make", s[scriptMake]);

	SaveScript("net", CString(pName) + ".break", s[scriptBreak]);

	SaveConfig("net", CString(pName) + "-conf", s[configFile]);

	return TRUE;
}

BOOL CLinuxNetApplicator::SaveScript(PCSTR pCat, PCSTR pName, CStringArray const &script)
{
	return SaveFile(pCat, pName, TRUE, script);
}

BOOL CLinuxNetApplicator::SaveConfig(PCSTR pCat, PCSTR pName, CStringArray const &script)
{
	return SaveFile(pCat, pName, FALSE, script);
}

BOOL CLinuxNetApplicator::SaveFile(PCSTR pCat, PCSTR pName, BOOL fExec, CStringArray const &script)
{
	CPrintf Path("/./tmp/crimson/config/%s", PCSTR(pCat));

	CPrintf Name("%s/%s", PCSTR(Path), pName);

	mkdir(Path, 0);

	if( script.IsEmpty() ) {

		if( fExec ) {

			unlink(Name);
		}
		else {
			CAutoFile File(Name, "w+");

			chmod(Name, 0644);
		}

		return FALSE;
	}
	else {
		CAutoFile File(Name, "w+");

		if( File ) {

			if( fExec ) {

				File.PutLine("#!/bin/bash");
			}

			for( UINT n = 0; n < script.GetCount(); n++ ) {

				File.PutLine(script[n]);
			}

			chmod(Name, fExec ? 0755 : 0644);

			return TRUE;
		}
	}

	return FALSE;
}

void CLinuxNetApplicator::AppendConfig(CStringArray &s, PCSTR pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	CString p;

	p.VPrintf(pText, pArgs);

	s.Append(p);

	va_end(pArgs);
}

void CLinuxNetApplicator::AppendConfig(CStringArray *s, PCSTR pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	CString p;

	p.VPrintf(pText, pArgs);

	s[configFile].Append(p);

	va_end(pArgs);
}

void CLinuxNetApplicator::AppendScript(CStringArray *s, UINT uFile, PCSTR pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	CString p;

	p.VPrintf(pText, pArgs);

	s[uFile].Append(p);

	va_end(pArgs);
}

// Other Services

BOOL CLinuxNetApplicator::ApplyServices(CJsonConfig *pServices)
{
	if( pServices ) {

		ApplyIoMonitor(pServices->GetChild("io"));

		ApplyZoneSync(pServices->GetChild("time.tz"));

		ApplyNtpSync(pServices->GetChild("time.ntp"));

		ApplyCellSync(pServices->GetChild("time.cell"));

		ApplyGpsSync(pServices->GetChild("time.gps"));

		ApplyLocation(pServices->GetChild("location"));

		ApplyFtpServer(pServices->GetChild("ftps"));

		ApplySvmClient(pServices->GetChild("svm"));

		ApplySmtpClient(pServices->GetChild("smtp"));

		ApplySmsClient(pServices->GetChild("sms"));
	}
	else {
		ApplyIoMonitor(NULL);

		ApplyZoneSync(NULL);

		ApplyNtpSync(NULL);

		ApplyCellSync(NULL);

		ApplyGpsSync(NULL);

		ApplyLocation(NULL);

		ApplyFtpServer(NULL);

		ApplySvmClient(NULL);

		ApplySmtpClient(NULL);

		ApplySmsClient(NULL);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyServiceDhcpRelay(CJsonConfig *pDhcp)
{
	m_DhcpRelay.Empty();

	if( pDhcp ) {

		CJsonConfig *pServers = pDhcp->GetChild("servers");

		if( pServers ) {

			UINT c = pServers->GetCount();

			for( UINT n = 0; n < c; n++ ) {

				CJsonConfig *pServer = pServers->GetChild(n);

				CIpAddr      Addr    = pServer->GetValueAsIp("ip", IP_EMPTY);

				if( !Addr.IsEmpty() ) {

					if( !m_DhcpRelay.IsEmpty() ) {

						m_DhcpRelay += " ";
					}

					m_DhcpRelay += Addr.GetAsText();
				}
			}
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyServiceDhcpServer(CJsonConfig *pDhcp)
{
	if( pDhcp ) {

		m_DhcpServer.Empty();

		CJsonConfig *pResList;

		CString      Domain;

		AppendConfig(m_DhcpServer, "auto_time %u", 60);

		AppendConfig(m_DhcpServer, "remaining %s", "no");

		AppendConfig(m_DhcpServer, "min_lease %u", 60 * pDhcp->GetValueAsUInt("minLeaseMin", 60, 1, 14400));

		AppendConfig(m_DhcpServer, "opt lease %u", 60 * pDhcp->GetValueAsUInt("defLeaseMNin", 240, 1, 14400));

		if( !(m_fDnsProxy = !pDhcp->GetValueAsBool("dnsMode", FALSE)) ) {

			CIpAddr Dns1 = pDhcp->GetValueAsIp("dns1", "8.8.8.8");

			CIpAddr Dns2 = pDhcp->GetValueAsIp("dns2", "8.8.4.4");

			AppendConfig(m_DhcpServer, "opt dns %s", PCSTR(Dns1.GetAsText()));

			AppendConfig(m_DhcpServer, "opt dns %s", PCSTR(Dns2.GetAsText()));
		}

		if( !(m_fNtpProxy = !pDhcp->GetValueAsBool("ntpMode", FALSE)) ) {

			CIpAddr Ntp1 = pDhcp->GetValueAsIp("ntp1", "129.6.15.28");

			AppendConfig(m_DhcpServer, "opt ntp %s", PCSTR(Ntp1.GetAsText()));
		}

		if( !(Domain = pDhcp->GetValue("domain", "")).IsEmpty() ) {

			AppendConfig(m_DhcpServer, "opt domain %s", PCSTR(Domain));
		}

		if( (pResList = pDhcp->GetChild("res")) ) {

			UINT c = pResList->GetCount();

			for( UINT n = 0; n < c; n++ ) {

				CJsonConfig *pRes = pResList->GetChild(n);

				CMacAddr Mac = pRes->GetValueAsMac("mac", MAC_EMPTY);

				CIpAddr  Ip  = pRes->GetValueAsIp("ip", IP_EMPTY);

				if( !Mac.IsEmpty() && !Ip.IsEmpty() ) {

					CPrintf Lease("static_lease %s %s", PCSTR(Mac.GetAsText()), PCSTR(Ip.GetAsText()));

					m_DhcpServer.Append(Lease);
				}
			}
		}
	}
	else {
		m_DhcpServer.Empty();

		AppendConfig(m_DhcpServer, "auto_time %u", 60);

		AppendConfig(m_DhcpServer, "remaining %s", "no");

		AppendConfig(m_DhcpServer, "min_lease %u", 3600);

		AppendConfig(m_DhcpServer, "opt lease %u", 14400);

		m_fDnsProxy = TRUE;

		m_fNtpProxy = TRUE;
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::SetPrivateRoutes(UINT uFace)
{
	if( uFace ) {

		if( !HasPrivateRoutes(uFace) ) {

			m_RuleFaces.Append(uFace);

			return TRUE;
		}
	}

	return FALSE;
}

UINT CLinuxNetApplicator::HasPrivateRoutes(UINT uFace)
{
	for( UINT n = 0; n < m_RuleFaces.GetCount(); n++ ) {

		if( m_RuleFaces[n] == uFace ) {

			return 1+n;
		}
	}

	return 0;
}

BOOL CLinuxNetApplicator::ApplyPrivateRoutes(CStringArray *s, CFaceInfo &Info, UINT face, BOOL asym)
{
	UINT priv = HasPrivateRoutes(face);

	if( !asym ) {

		if( !priv ) {

			SetPrivateRoutes(face);

			priv = HasPrivateRoutes(face);
		}
	}

	if( priv ) {

		UINT prio = 3000 + 2 * (m_FaceNames.GetCount() - 2);

		AppendScript(s, scriptMake, "ip rule add from $1 prio %u table c3-%s-out", prio+0, Info.m_pName);

		AppendScript(s, scriptMake, "ip rule add iif %s prio %u table c3-%s-in", Info.m_pName, prio+1, Info.m_pName);

		AppendScript(s, scriptBreak, "ip rule delete prio %u", prio+0);

		AppendScript(s, scriptBreak, "ip rule delete prio %u", prio+1);

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyRouting(CJsonConfig *pRouting)
{
	CStringArray init, term;

	UINT en = 0;

	AppendConfig(m_Init1, "sysctl -w net.ipv4.conf.default.rp_filter=0");

	AppendConfig(m_Init1, "sysctl -w net.ipv4.conf.all.rp_filter=0");

	if( pRouting ) {

		en = pRouting->GetValueAsUInt("enable", 0, 0, 1);

		m_pRoutesStatic   = pRouting->GetChild("routes");

		m_pRoutesOutbound = pRouting->GetChild("outbound");

		m_pRoutesInbound  = pRouting->GetChild("inbound");

		ApplyRoutes(init, 0, 100);
	}

	AppendConfig(m_Init1, "sysctl -w net.ipv4.ip_forward=%u", en);

	SaveScript("routes", "init", init);

	SaveScript("routes", "term", term);

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyRoutes(CStringArray &s, UINT uFace, UINT uMetric)
{
	ApplyRoutes(s, m_pRoutesStatic, uFace, uMetric);

	ApplyRoutes(s, m_pRoutesOutbound, uFace, uMetric);

	ApplyRoutes(s, m_pRoutesInbound, uFace, uMetric);

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyRoutes(CStringArray &s, CJsonConfig *pTable, UINT uFace, UINT uMetric)
{
	if( pTable ) {

		UINT c = pTable->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonConfig *pRoute = pTable->GetChild(n);

			UINT out  = pRoute->GetValueAsUInt("outbound", 0, 0, NOTHING);

			UINT in   = pRoute->GetValueAsUInt("inbound", 0, 0, NOTHING);

			UINT face = pRoute->GetValueAsUInt("face", out, 0, NOTHING);

			if( face == uFace ) {

				PCSTR   pName = NULL;

				CString Where;

				if( face ) {

					Where = GetFaceName(face);

					pName = Where;
				}

				if( !in && !out ) {

					ApplyRoute(s, pRoute, pName, uMetric, NULL);
				}
				else {
					if( out ) {

						SetPrivateRoutes(out);

						CPrintf Table("c3-%s-out", PCSTR(GetFaceName(out)));

						ApplyRoute(s, pRoute, pName, 0, Table);
					}

					if( in ) {

						SetPrivateRoutes(in);

						CPrintf Table("c3-%s-in", PCSTR(GetFaceName(in)));

						ApplyRoute(s, pRoute, pName, 0, Table);
					}
				}
			}
			else {
				if( !uFace ) {

					SetPrivateRoutes(out);

					SetPrivateRoutes(in);
				}
			}
		}
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyRoute(CStringArray &s, CJsonConfig *pRoute, PCSTR pName, UINT uMetric, PCSTR pTable)
{
	CIpAddr Dest = pRoute->GetValueAsIp("ip", IP_EMPTY);

	CIpAddr Mask = pRoute->GetValueAsIp("mask", IP_EMPTY);

	CIpAddr Gate = pRoute->GetValueAsIp("gateway", IP_EMPTY);

	UINT    Dist = pRoute->GetValueAsUInt("metric", 0, 0, 2000);

	if( pName || !Gate.IsEmpty() ) {

		CPrintf Line("ip route add");

		Line.AppendPrintf(" %s", Mask.IsEmpty() ? "default" : PCSTR(GetFullAddr(Dest, Mask)));

		if( !Gate.IsEmpty() ) {

			Line.AppendPrintf(" via %s", PCSTR(Gate.GetAsText()));
		}

		if( !Dist ) {

			Dist = uMetric;
		}
		else {
			if( uMetric > 2000 ) {

				Dist += 2000;
			}
		}

		if( Dist ) {

			Line.AppendPrintf(" metric %u", Dist);
		}

		if( pName ) {

			Line.AppendPrintf(" dev %s", pName);
		}

		if( pTable ) {

			Line.AppendPrintf(" table %s", pTable);
		}

		s.Append(Line);

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::ApplyResolver(CJsonConfig *pResolver)
{
	CStringArray init, term, conf;

	CStringArray loc, glob, res;

	if( pResolver ) {

		CJsonConfig *pHosts;

		CString      Suffix;

		UINT	     uMode;

		AppendConfig(init, "cat /vap/opt/crimson/config/dns/hosts-local /tmp/crimson/dns/hosts > /etc/hosts");

		AppendConfig(init, "cp /vap/opt/crimson/config/dns/resolv.conf /etc");

		AppendConfig(conf, "domain-needed");

		AppendConfig(conf, "bogus-priv");

		AppendConfig(conf, "no-poll");

		AppendConfig(conf, "cache-size=150");

		AppendConfig(conf, "ttladd=10000");

		if( FALSE ) {

			for( UINT n = 0; n < m_FaceNames.GetCount(); n++ ) {

				AppendConfig(conf, "interface=%s", PCSTR(m_FaceNames[n]));
			}
		}

		AppendConfig(conf, "no-hosts");

		AppendConfig(conf, "addn-hosts=/vap/opt/crimson/config/dns/hosts-global");

		if( (uMode = pResolver->GetValueAsUInt("mode", 1, 0, 2)) ) {

			if( uMode == 1 ) {

				if( !m_DhcpClients.IsEmpty() ) {

					AppendConfig(conf, "resolv-file=/tmp/crimson/dns/dhcp.conf");
				}
				else {
					AppendConfig(conf, "resolv-file=");

					AppendConfig(conf, "server=8.8.8.8");

					AppendConfig(conf, "server=8.8.4.4");
				}
			}

			if( uMode == 2 ) {

				AppendConfig(conf, "resolv-file=");

				AppendConfig(conf, "server=%s", PCSTR(pResolver->GetValue("dns1", "8.8.8.8")));

				AppendConfig(conf, "server=%s", PCSTR(pResolver->GetValue("dns2", "8.8.4.4")));
			}

			if( !(Suffix = pResolver->GetValue("suffix", "")).IsEmpty() ) {

				res.Append("server " + Suffix);
			}

			res.Append("nameserver 127.0.0.1");
		}
		else {
			AppendConfig(conf, "resolv-file=");

			res.Append("nameserver 127.0.0.1");
		}

		if( true ) {

			loc.Append("127.0.0.1 localhost");

			loc.Append("127.0.0.1 " + m_DefName);
		}

		if( (pHosts = pResolver->GetChild("hosts")) ) {

			UINT c = pHosts->GetCount();

			for( UINT n = 0; n < c; n++ ) {

				CJsonConfig *pHost = pHosts->GetChild(n);

				CIpAddr Addr = pHost->GetValueAsIp("ip", IP_EMPTY);

				CString Name = pHost->GetValue("name", "");

				UINT    Glob = pHost->GetValueAsUInt("scope", 0, 0, 1);

				if( !Addr.IsEmpty() && !Name.IsEmpty() ) {

					if( Glob ) {

						AppendConfig(glob, "%s %s", PCSTR(Addr.GetAsText()), PCSTR(Name));
					}

					AppendConfig(loc, "%s %s", PCSTR(Addr.GetAsText()), PCSTR(Name));
				}
			}
		}

		if( false ) {

			loc.Append(":1 ip6-localhost ip6-loopback");
			loc.Append("fe00::0 ip6-localnet");
			loc.Append("ff00::0 ip6-mcastprefix");
			loc.Append("ff02::1 ip6-allnodes");
			loc.Append("ff02::2 ip6-allrouters");
		}

		CPrintf Prog("dnsmasq -k -C /vap/opt/crimson/config/dns/dns.conf");

		AppendProgram(init, term, Prog, "dnsmasq");
	}

	SaveScript("dns", "init", init);

	SaveConfig("dns", "dns.conf", conf);

	SaveConfig("dns", "hosts-local", loc);

	SaveConfig("dns", "hosts-global", glob);

	SaveConfig("dns", "resolv.conf", res);

	SaveScript("dns", "term", term);

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyCertManager(CJsonConfig *pConfig)
{
	if( CheckCRC(m_crcCertManager, pConfig) ) {

		if( m_pCertManager ) {

			m_pCertManager->Release();

			m_pCertManager = NULL;
		}
	}

	if( pConfig ) {

		m_pCertManager = New CCertManager(pConfig);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyIdent(CJsonConfig *pIdent)
{
	if( CheckCRC(m_crcIdent, pIdent) ) {

		if( m_pMulti1 ) {

			m_pMulti1->Destroy();

			m_pMulti1 = NULL;
		}

		if( m_pIdentifier ) {

			m_pIdentifier->Destroy();

			m_pIdentifier = NULL;
		}

		if( m_pMulti2 ) {

			m_pMulti2->Destroy();

			m_pMulti2 = NULL;
		}

		m_mDnsNames.Empty();
	}

	if( pIdent ) {

		bool    fUsed = false;

		CString Name1 = m_DefName;

		CString Name2 = pIdent->GetValue("name", "");

		if( pIdent->GetValueAsBool("llmnr", TRUE) ) {

			CMultiDns *pDns1 = New CMultiDns(CMultiDns::protocolLLMNR, Name1, Name2);

			m_pMulti1 = CreateClientThread(pDns1, 0, 24010, NULL);

			fUsed = true;
		}

		if( pIdent->GetValueAsBool("mdns", TRUE) ) {

			CMultiDns *pDns2 = New CMultiDns(CMultiDns::protocolMDNS, Name1, Name2);

			m_pMulti2 = CreateClientThread(pDns2, 0, 24020, NULL);

			fUsed = true;
		}

		if( pIdent->GetValueAsBool("identifier", TRUE) ) {

			CIdentifier *pClient = New CIdentifier(pIdent);

			m_pIdentifier = CreateClientThread(pClient, 0, 24030, NULL);
		}

		if( fUsed ) {

			if( !Name2.IsEmpty() ) {

				m_mDnsNames.Append(Name2);
			}

			if( !Name1.IsEmpty() ) {

				m_mDnsNames.Append(Name1);
			}
		}

		m_UnitName = Name2.IsEmpty() ? Name1 : Name2;
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyZoneSync(CJsonConfig *pZoneSync)
{
	if( CheckCRC(m_crcZoneSync, pZoneSync) ) {

		if( m_pZoneSync ) {

			m_pZoneSync->Destroy();

			m_pZoneSync= NULL;
		}
	}

	if( pZoneSync ) {

		CZoneSync *pSync = New CZoneSync(pZoneSync, 0);

		m_pZoneSync = CreateClientThread(pSync, 0, 24000, NULL);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyNtpSync(CJsonConfig *pNtpSync)
{
	if( CheckCRC(m_crcNtpSync, pNtpSync) ) {

		if( m_pNtpSync ) {

			m_pNtpSync->Destroy();

			m_pNtpSync = NULL;
		}
	}

	if( pNtpSync ) {

		// TODO -- How to find DHCP options? !!!

		// TODO -- Do we need the interface count? !!!

		CNtpClient *pClient = New CNtpClient(pNtpSync, 0);

		m_pNtpSync = CreateClientThread(pClient, 0, 24000, NULL);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyCellSync(CJsonConfig *pCellSync)
{
	if( CheckCRC(m_crcCellSync, pCellSync) ) {

		if( m_pCellSync ) {

			m_pCellSync->Destroy();

			m_pCellSync = NULL;
		}
	}

	if( pCellSync ) {

		CCellTimeClient *pClient = New CCellTimeClient(pCellSync);

		m_pCellSync = CreateClientThread(pClient, 0, 24000, NULL);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyGpsSync(CJsonConfig *pGpsSync)
{
	if( CheckCRC(m_crcGpsSync, pGpsSync) ) {

		if( m_pGpsSync ) {

			m_pGpsSync->Destroy();

			m_pGpsSync = NULL;
		}
	}

	if( pGpsSync ) {

		CGpsTimeClient *pClient = New CGpsTimeClient(pGpsSync);

		m_pGpsSync = CreateClientThread(pClient, 0, 24000, NULL);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplySmtpClient(CJsonConfig *pSmtpClient)
{
	if( CheckCRC(m_crcSmtpClient, pSmtpClient) ) {

		if( m_pSmtpClient ) {

			m_pSmtpClient->Destroy();

			m_pSmtpClient = NULL;
		}
	}

	if( pSmtpClient ) {

		CSmtpClient *pClient = New CSmtpClient(pSmtpClient);

		m_pSmtpClient = CreateClientThread(pClient, 0, 24000, NULL);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplySmsClient(CJsonConfig *pSmsClient)
{
	if( CheckCRC(m_crcSmsClient, pSmsClient) ) {

		if( m_pSmsClient ) {

			m_pSmsClient->Destroy();

			m_pSmsClient = NULL;
		}
	}

	if( pSmsClient ) {

		CLinuxSmsClient *pClient = New CLinuxSmsClient(pSmsClient);

		m_pSmsClient = CreateClientThread(pClient, 0, 24000, NULL);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyLocation(CJsonConfig *pLocation)
{
	if( CheckCRC(m_crcLocation, pLocation) ) {

		if( m_pLocation ) {

			m_pLocation->Release();

			m_pLocation = NULL;
		}
	}

	if( pLocation ) {

		m_pLocation = New CLocation(pLocation);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyFtpServer(CJsonConfig *pFtpServer)
{
	if( CheckCRC(m_crcFtpServer, pFtpServer) ) {

		if( m_pFtpServer ) {

			m_pFtpServer->Destroy();

			m_pFtpServer = NULL;
		}
	}

	if( pFtpServer ) {

		CFtpServer *pServer = New CFtpServer(pFtpServer);

		m_pFtpServer = CreateClientThread(pServer, 0, 24040, NULL);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplySvmClient(CJsonConfig *pSvmClient)
{
	if( CheckCRC(m_crcSvmClient, pSvmClient) ) {

		if( m_pSvmClient ) {

			m_pSvmClient->Destroy();

			m_pSvmClient = NULL;
		}
	}

	if( pSvmClient ) {

		CSvmClient *pClient = New CSvmClient(pSvmClient);

		m_pSvmClient = CreateClientThread(pClient, 0, 24050, NULL);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyIoMonitor(CJsonConfig *pIoMonitor)
{
	if( CheckCRC(m_crcIoMonitor, pIoMonitor) ) {

		if( m_pIoMonitor ) {

			m_pIoMonitor->Destroy();

			m_pIoMonitor = NULL;
		}
	}

	if( pIoMonitor ) {

		CIoMonitor *pMonitor = New CIoMonitor(pIoMonitor);

		m_pIoMonitor = CreateClientThread(pMonitor, 0, 24050, NULL);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyTlsRelay(CJsonConfig *pConfig, char cMode)
{
	CPrintf Name("tls%c", cMode);

	CStringArray init, term, conf;

	if( pConfig ) {

		CJsonConfig *pCons = pConfig->GetChild("cons");

		UINT         uUsed = 0;

		DWORD        crc   = 0;

		if( pCons && pCons->GetCount() ) {

			CByteArray Cert, Priv;

			UINT KeepAlive = pConfig->GetValueAsUInt("keepalive", 0, 0, 1);

			UINT UseCert   = pConfig->GetValueAsUInt("usecert", 0, 0, 65535);

			if( UseCert ) {

				if( UseCert >= 100 ) {

					if( !SaveIdentityCert(crc, Name, "tls", "cert.crt", "priv.key", NULL, UseCert) ) {

						UseCert = 0;
					}
				}
				else {
					pConfig->GetValueAsBlob("cert", Cert);

					pConfig->GetValueAsBlob("priv", Priv);

					if( !Cert.IsEmpty() && !Priv.IsEmpty() ) {

						SaveCertFile(crc, Name, "tls", "cert.crt", Cert);

						SaveCertFile(crc, Name, "tls", "priv.key", Priv);

						CString Pass = pConfig->GetValue("pass", "");

						crc = ::crc(crc, PCBYTE(Pass.data()), Pass.size());

						DecryptKeyFile(Name, "tls", "priv.key", Pass);
					}
					else {
						UseCert = 0;
					}
				}
			}

			UINT TlsFace = pConfig->GetValueAsUInt("facetls", 0, 0, NOTHING);

			UINT TcpFace = pConfig->GetValueAsUInt("facetcp", 0, 0, NOTHING);

			AppendConfig(conf, "client=%s", (cMode == 'c') ? "yes" : "no");

			AppendConfig(conf, "foreground=yes");

			AppendConfig(conf, "debug=5");

			AppendConfig(conf, "delay=yes");

			AppendConfig(conf, "TIMEOUTconnect=%u", pConfig->GetValueAsUInt("twait", 20, 5, 300));

			AppendConfig(conf, "TIMEOUTidle=%u", 60 * pConfig->GetValueAsUInt("tidle", 5, 1, 1440));

			if( KeepAlive ) {

				AppendConfig(conf, "socket=l:SO_KEEPALIVE=%u", 30);

				AppendConfig(conf, "socket=r:SO_KEEPALIVE=%u", 30);
			}

			if( UseCert ) {

				AppendConfig(conf, "cert=/vap/opt/crimson/config/tls%c/tls-cert.crt", cMode);

				AppendConfig(conf, "key=/vap/opt/crimson/config/tls%c/tls-priv.key", cMode);
			}
			else {
				AppendConfig(conf, "cert=/tmp/crimson/defcert/cert.crt");

				AppendConfig(conf, "key=/tmp/crimson/defcert/cert.key");
			}

			AppendConfig(conf, "");

			for( UINT n = 0; n < pCons->GetCount(); n++ ) {

				CJsonConfig *pCon = pCons->GetChild(n);

				CIpAddr LocIp   = pCon->GetValueAsIp("locip", IP_EMPTY);

				UINT    LocPort = pCon->GetValueAsUInt("locport", 0, 0, 65535);

				CString RemHost = pCon->GetValue("remhost", "");

				UINT    RemPort = pCon->GetValueAsUInt("remport", 0, 0, 65535);

				UINT    Service = pCon->GetValueAsUInt("protocol", 0, 0, 7);

				if( !RemHost.IsEmpty() ) {

					AppendConfig(conf, "[%c%u]", cMode, 1+n);

					AppendConfig(conf, "accept=%s:%u", PCSTR(LocIp.GetAsText()), LocPort);

					AppendConfig(conf, "connect=%s:%u", PCSTR(RemHost), RemPort);

					uUsed++;
				}

				if( Service ) {

					PCSTR List[] = { "", "CIFS", "IMAP", "PGSQL", "POP3", "Proxy", "SMTP", "SOCKS", "CONNECT", "NNTP" };

					if( Service < elements(List) ) {

						AppendConfig(conf, "protocol=%s", List[Service]);
					}
				}

				if( TlsFace ) {

					char sock = (cMode == 'c') ? 'l' : 'a';

					AppendConfig(conf, "socket=%c:SO_BINDTODEVICE=%s", sock, PCSTR(GetFaceName(TlsFace)));
				}

				if( TcpFace ) {

					char sock = (cMode == 'c') ? 'a' : 'l';

					AppendConfig(conf, "socket=%c:SO_BINDTODEVICE=%s", sock, PCSTR(GetFaceName(TcpFace)));
				}

			}

			if( uUsed ) {

				CPrintf Prog("stunnel /vap/opt/crimson/config/tls%c/tls.conf", cMode);

				AppendProgram(init, term, Prog, Name);
			}
		}

		SaveScript(Name, "init", init);

		SaveConfig(Name, "tls.conf", conf);

		SaveScript(Name, "term", term);
	}

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplyTlsServer(CJsonConfig *pServer)
{
	return ApplyTlsRelay(pServer, 's');
}

BOOL CLinuxNetApplicator::ApplyTlsClient(CJsonConfig *pClient)
{
	return ApplyTlsRelay(pClient, 'c');
}

BOOL CLinuxNetApplicator::ApplySysLog(CJsonConfig *pSysLog)
{
	CStringArray init, term, vars, conf;

	if( pSysLog ) {

		if( pSysLog->GetValueAsUInt("mode", 0, 0, 1) ) {

			vars.Append("opts=\"-R 192.168.1.35:514\"");

			init.Append("service syslog restart");

			CJsonConfig *pWhite = pSysLog->GetChild("wlist");

			CJsonConfig *pBlack = pSysLog->GetChild("blist");

			if( pWhite ) {

				for( UINT n = 0; n < pWhite->GetCount(); n++ ) {

					CJsonConfig *pRule = pWhite->GetChild(n);

					conf.Append("whitelist=" + pRule->GetValue("text", ""));
				}
			}

			if( pBlack ) {

				for( UINT n = 0; n < pBlack->GetCount(); n++ ) {

					CJsonConfig *pRule = pBlack->GetChild(n);

					conf.Append("blacklist=" + pRule->GetValue("text", ""));
				}
			}

			vars.Append("cp /vap/opt/crimson/config/syslog/syslog.conf /etc/sysconfig/rsyslog_filter.conf");
		}
	}

	if( init.IsEmpty() ) {

		init.Append("service syslog restart");
	}

	SaveScript("syslog", "init", init);

	SaveScript("syslog", "vars", vars);

	SaveConfig("syslog", "syslog.conf", conf);

	SaveScript("syslog", "term", term);

	return TRUE;
}

BOOL CLinuxNetApplicator::ApplySnmp(CJsonConfig *pAgent)
{
	CStringArray init, term, conf;

	if( pAgent ) {

		if( pAgent->GetValueAsUInt("mode", 0, 0, 1) ) {

			bool ver3 = pAgent->GetValueAsUInt("version", 0, 0, 1) ? true : false;

			AppendConfig(conf, "syslocation %s", PCSTR(pAgent->GetValue("location", "Not Defined")));

			AppendConfig(conf, "syscontact %s", PCSTR(pAgent->GetValue("contact", "Not Defined")));

			if( !ver3 ) {

				AppendConfig(conf, "rocommunity %s", PCSTR(pAgent->GetValue("community", "public")));
			}
			else {
				CJsonConfig *pUsers = pAgent->GetChild("users");

				if( pUsers ) {

					for( UINT n = 0; n < pUsers->GetCount(); n++ ) {

						CJsonConfig *pUser = pUsers->GetChild(n);

						CString User(pUser->GetValue("user", "None"));

						CPrintf Line1("createUser %s", PCSTR(User));

						CPrintf Line2("rouser %s", PCSTR(User));

						UINT auth, priv;

						if( (auth = pUser->GetValueAsUInt("auth", 0, 0, 2)) ) {

							PCSTR alg[] = { NULL, "MD5", "SHA" };

							Line1.AppendPrintf(" %s %s", alg[auth], PCSTR(pUser->GetValue("apass", "None")));

							if( (priv = pUser->GetValueAsUInt("privacy", 0, 0, 2)) ) {

								PCSTR alg[] = { NULL, "DES", "AES" };

								Line1.AppendPrintf(" %s %s", alg[priv], PCSTR(pUser->GetValue("ppass", "None")));

								Line2.Append(" priv");
							}
							else {
								Line2.Append(" auth");
							}
						}
						else {
							Line2.Append(" noauth");
						}

						conf.Append(Line1);

						conf.Append(Line2);
					}
				}
			}

			CPrintf Prog("snmpd -f -a -Lsd -c /vap/opt/crimson/config/snmp/snmp.conf");

			AppendProgram(init, term, Prog, "snmp");
		}

		SaveScript("snmp", "init", init);

		SaveConfig("snmp", "snmp.conf", conf);

		SaveScript("snmp", "term", term);
	}

	return TRUE;
}

// Status Building

BOOL CLinuxNetApplicator::AppendFile(CString &Info, PCSTR pHead, PCSTR pName)
{
	CAutoFile File(pName, "r");

	if( File ) {

		if( File.GetSize() ) {

			if( !Info.IsEmpty() ) {

				Info += "</br>";
			}

			if( pHead ) {

				Info += "<b>";

				Info += pHead;

				Info += "</b></br></br>";
			}

			bool fEmpty = false;

			while( !File.End() ) {

				CString Line = File.GetLine();

				if( !Line.IsEmpty() ) {

					if( fEmpty ) {

						Info += "</br>";
					}

					if( Line.StartsWith("#!") ) {

						continue;
					}

					if( Line.StartsWith("#crc ") ) {

						continue;
					}

					if( Line.StartsWith("#skip ") ) {

						Info += Line.Mid(6) + "</br>";

						File.GetLine();
					}
					else {
						Line.Replace("<", "&lt;");

						Line.Replace(">", "&gt;");

						Info += Line + "</br>";
					}

					fEmpty = false;
				}
				else {
					fEmpty = true;
				}
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::AppendCommand(CString &Info, PCSTR pHead, PCSTR pCmd, PCSTR pArgs)
{
	CPrintf Temp("/./tmp/%8.8X%8.8X", rand(), rand());

	if( RunCommand(pCmd, pArgs, Temp) == 0 ) {

		if( AppendFile(Info, pHead, Temp) ) {

			unlink(Temp);

			return TRUE;
		}
	}

	unlink(Temp);

	return FALSE;
}

int CLinuxNetApplicator::RunCommand(PCSTR pCmd, PCSTR pArgs, PCSTR pFile)
{
	AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

	if( pLinux ) {

		return pLinux->CallProcess(pCmd, pArgs, pFile, pFile);
	}

	return -1;
}

// Address Formatting

CString CLinuxNetApplicator::GetFullAddr(CIpAddr const &Addr, CIpAddr const &Mask)
{
	if( Mask == IP_BROAD ) {

		return Addr.GetAsText();
	}

	return CPrintf("%s/%u", PCSTR(Addr.GetAsText()), Mask.GetBitCount());
}

CString CLinuxNetApplicator::GetFullNet(CIpAddr const &Addr, CIpAddr const &Mask)
{
	return GetFullAddr(Addr & Mask, Mask);
}

// Implementation

BOOL CLinuxNetApplicator::ClearConfigData(void)
{
	for( UINT n = 0; n < m_FaceStatus.GetCount(); n++ ) {

		AfxRelease(m_FaceStatus[n]);
	}

	m_FaceStatus.Empty();

	m_Dependencies.Empty();

	m_IptFace.Empty();

	m_BridgeMap.Empty();
	m_BridgeRev.Empty();
	m_BridgeMtu.Empty();

	m_FaceNames.Empty();
	m_FaceDescs.Empty();
	m_FacePaths.Empty();
	m_RuleFaces.Empty();

	m_FaceNames.Append("lo");
	m_FaceDescs.Append("Loopback");
	m_FacePaths.Append("");

	m_FiltInit.Empty();
	m_FiltTerm.Empty();
	m_FireHead.Empty();
	m_FireInit.Empty();
	m_FireDrop.Empty();
	m_FireLast.Empty();
	m_FireTerm.Empty();

	m_IpSecConfig.Empty();
	m_IpSecSecrets.Empty();

	m_Init1.Empty();
	m_Init2.Empty();
	m_Term1.Empty();
	m_Term2.Empty();

	RunCommand("/bin/rm", "-rf /tmp/crimson/config", NULL);

	return TRUE;
}

BOOL CLinuxNetApplicator::AddDependency(PCSTR pParent, PCSTR pChild)
{
	INDEX i = m_Dependencies.FindName(pParent);

	if( !m_Dependencies.Failed(i) ) {

		CString List = m_Dependencies.GetData(i);

		m_Dependencies.SetData(i, List + " " + pChild);

		return TRUE;
	}

	m_Dependencies.Insert(pParent, pChild);

	return FALSE;
}

BOOL CLinuxNetApplicator::WriteDependencies(void)
{
	CStringArray List;

	for( INDEX i = m_Dependencies.GetHead(); !m_Dependencies.Failed(i); m_Dependencies.GetNext(i) ) {

		CString Line;

		Line += "x" + m_Dependencies.GetName(i) + "x ";

		Line += m_Dependencies.GetData(i);

		List.Append(Line);
	}

	SaveConfig("net", "deps", List);

	return TRUE;
}

BOOL CLinuxNetApplicator::IsValidGreKey(PCSTR pKey)
{
	if( *pKey ) {

		PTXT pLast;

		strtoul(pKey, &pLast, 10);

		if( *pLast ) {

			if( CIpAddr(pKey).IsEmpty() ) {

				return FALSE;
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxNetApplicator::CheckCRC(DWORD &crc, CJsonConfig * &pJson)
{
	DWORD c = pJson ? pJson->GetCRC() : 1;

	if( crc ) {

		if( crc == c ) {

			pJson = NULL;

			return FALSE;
		}

		crc = c;

		return TRUE;
	}

	crc = c;

	return FALSE;
}

DWORD CLinuxNetApplicator::GetCRC(CStringArray const &s, DWORD in)
{
	CArray<DWORD> d;

	for( UINT n = 0; n < s.GetCount(); n++ ) {

		CString const &i = s[n];

		d.Append(crc(in, PCBYTE(PCSTR(i)), i.GetLength()));
	}

	return crc(PCBYTE(d.GetPointer()), 4 * d.GetCount());
}

void CLinuxNetApplicator::WaitForStartup(void)
{
	bool w = false;

	for( ;;) {

		if( CAutoFile("/./var/run/poststart", "r") ) {

			if( w ) {

				Sleep(2500);

				AfxTrace("Startup complete\n");
			}

			break;
		}

		if( !w ) {

			AfxTrace("Wait for startup\n");

			w = true;
		}

		Sleep(500);
	}
}

// End of File
