
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_WebFiles_HPP

#define INCLUDE_WebFiles_HPP

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

struct CWebFileData
{
	PCTXT  m_pName;
	PCBYTE m_pData;
	UINT   m_uSize;
};

//////////////////////////////////////////////////////////////////////////
//
// File Info
//

struct CWebFileInfo
{
	CWebFileData const *p;
	time_t              t;
};

//////////////////////////////////////////////////////////////////////////
//
// Web Files
//

class CWebFiles
{
public:
	// Constructors
	CWebFiles(void);

	// File Lookup
	BOOL FindFile(CWebFileInfo &Info, PCTXT pName);

protected:
	// File Tables
	static UINT const m_uCount1;
	static UINT const m_uCount2;
	static UINT const m_uCount3;
	static UINT const m_uCount4;
	static UINT const m_uCount5;
	static CWebFileData const m_Files1[];
	static CWebFileData const m_Files2[];
	static CWebFileData const m_Files3[];
	static CWebFileData const m_Files4[];
	static CWebFileData const m_Files5[];

	// File Index
	CMap<PCTXT, CWebFileData const *> m_Index;

	// File Times
	time_t m_timeBoot;
	time_t m_timeComp;

	// Implementation
	BOOL MakeIndex(void);
	void FindTimes(void);
};

// End of File

#endif
