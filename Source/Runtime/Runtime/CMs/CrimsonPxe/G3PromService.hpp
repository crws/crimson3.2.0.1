
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3PromService_HPP

#define	INCLUDE_G3PromService_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Prom Loader Service
//

class CG3PromService : public ILinkService
{
public:
	// Constructor
	CG3PromService(ICrimsonPxe *pPxe);

	// Destructor
	~CG3PromService(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ILinkService
	void Timeout(void);
	UINT Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans);
	void EndLink(CG3LinkFrame &Req);

protected:
	// Data Members
	ULONG	           m_uRefs;
	ICrimsonPxe      * m_pPxe;
	IPlatform        * m_pPlatform;
	IFirmwareProgram * m_pBootPend;
	BOOL		   m_fPend;

	// Implementation
	UINT WriteProgram(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT UpdateProgram(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT CheckLoader(CG3LinkFrame &Req, CG3LinkFrame &Rep);
};

// End of File

#endif
