

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PxeObject_HPP

#define INCLUDE_PxeObject_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "LoadCounter.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;
class CSplash;
class CPxeWebServer;
class CMsgBroker;
class CUserManager;

//////////////////////////////////////////////////////////////////////////
//
// Referenced Interfaces
//

interface ILinkService;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define TIMER_RATE	50

#define	ToTimer(t)	((t) / TIMER_RATE)

#define	FLASH_RATE	ToTimer(200)

#define LOAD_TIMEOUT	ToTimer(15 * 1000)

//////////////////////////////////////////////////////////////////////////
//
// PXE Application Object
//

class CPxeObject :
	public ICrimsonPxe,
	public IConfigUpdate,
	public IClientProcess,
	public IDiagProvider,
	public IEventSink
{
public:
	// Constructor
	CPxeObject(void);

	// Destructor
	~CPxeObject(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ICrimsonPxe
	BOOL METHOD LockApp(ICrimsonApp * &pApp);
	void METHOD FreeApp(void);
	void METHOD GetInitTime(time_t &time);
	void METHOD SetSysWebLink(CString const &Link);
	BOOL METHOD GetSysWebLink(CString &Link);
	void METHOD SetAppWebLink(CString const &Link);
	BOOL METHOD GetAppWebLink(CString &Link);
	void METHOD SetAlarm(UINT uAlarm);
	UINT METHOD GetAlarm(void);
	void METHOD SetSiren(BOOL fSiren);
	BOOL METHOD GetSiren(void);
	UINT METHOD GetLedMode(void);
	void METHOD SetLedMode(UINT uMode);
	BOOL METHOD SystemStart(void);
	BOOL METHOD SystemStop(void);
	void METHOD SystemUpdate(UINT hItem, PBYTE pData, UINT uSize);
	void METHOD RestartSystem(UINT uTimeout, UINT uExitCode);
	BOOL METHOD ImageSave(PCTXT pName);
	void METHOD KickTimeout(UINT uIndex);
	UINT METHOD GetDefCertStep(void);
	BOOL METHOD GetDefCertData(CByteArray *pData, CString &Data);
	BOOL METHOD GetHostName(CString &Name);
	BOOL METHOD GetUnitName(CString &Name);
	UINT METHOD GetStatus(void);
	BOOL METHOD GetPersonality(CString const &Name, CString &Data);
	BOOL METHOD SetPersonality(CString const &Name, CString const &Data);
	BOOL METHOD CommitPersonality(BOOL fRestart);
	void METHOD IdentifyUnit(void);
	BOOL METHOD GetUserInfo(CString const &User, CAuthInfo const &Auth, CUserInfo &Info);
	BOOL METHOD SetUserPass(CString const &User, CString const &From, CString const &To);
	WORD METHOD GetG3LinkPort(void);

	// IConfigUpdate
	void METHOD OnConfigUpdate(char cTag);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

	// IEventSink
	void OnEvent(UINT uLine, UINT uParam);

	// IDiagProvider
	UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

protected:
	// System States
	enum
	{
		stateNull,
		stateInitial,
		stateMultiple,
		stateDelay,
		statePaused,
		stateStarting,
		stateWaiting,
		stateRunning,
		stateChange,
		stateStopped,
		stateInvalid,
		stateExit,
		stateDone
	};

	// System Events
	enum
	{
		eventNull    = 1,
		eventPause   = 2,
		eventPlay    = 4,
		eventStop    = 8,
		eventStart   = 16,
		eventHChange = 32,
		eventSChange = 64,
		eventExit    = 128,
	};

	// LED Types
	enum
	{
		ledStd,
		ledEdge,
		ledDAX
	};

	// Halo Modes
	enum
	{
		haloStartup,
		haloStopped,
		haloRunning,
		haloAccepted,
		haloActive,
		haloIdentify,
		haloExit
	};

	// Static Data
	static UINT	  m_DefCertStep;
	static CByteArray m_DefRootData;
	static CByteArray m_DefRootPriv;
	static CString    m_DefRootPass;
	static CByteArray m_DefCertData;
	static CByteArray m_DefCertPriv;
	static CString    m_DefCertPass;

	// Data Members
	ULONG	              m_uRefs;
	time_t		      m_InitTime;
	BOOL		      m_fDebug;
	BOOL		      m_fApply;
	UINT		      m_uState;
	UINT		      m_uSaved;
	UINT		      m_uTimer;
	UINT volatile	      m_uEvent;
	UINT		      m_uDefEvent;
	UINT		      m_uDefTimer;
	IMutex		    * m_pAppLock;
	IEvent		    * m_pEvent;
	CString		      m_Error;
	IEvent		    * m_pMakeCert;
	HTHREAD		      m_hMakeCert;
	CMsgBroker	    * m_pMsgBroker;
	CPxeWebServer       * m_pWeb;
	HTHREAD		      m_hWeb;
	UINT		      m_uProv;
	CSplash		    * m_pSplash;
	CString		      m_DefName;
	CString		      m_SysLink;
	CString		      m_AppLink;
	IPxeModel	    * m_pModel;
	ISchemaGenerator    * m_pSchema;
	IConfigStorage      * m_pConfig;
	INetApplicator      * m_pNetApp;
	IHardwareApplicator * m_pHwApp;
	CUserManager        * m_pUsers;
	CJsonConfig         * m_pHcon;
	CJsonConfig         * m_pScon;
	IMutex		    * m_pPconLock;
	BOOL		      m_fPconValid;
	CStringMap	      m_PKeys;
	DWORD		      m_crcNet;
	DWORD		      m_crcIdent;
	DWORD		      m_crcLink;
	BOOL		      m_fAutoCert;
	BOOL		      m_fAutoIps;
	CString		      m_CertNames;
	CString		      m_CertAddrs;
	CString		      m_CertPrior;
	ICrimsonApp	    * m_pApp;
	ILinkService        * m_pReqRouter;
	IPlatform	    * m_pPlatform;
	INetUtilities	    * m_pNetUtils;
	IFileUtilities	    * m_pFileUtils;
	IDatabase           * m_pDbase;
	IDisplay	    * m_pDisp;
	IBeeper		    * m_pBeep;
	ILeds		    * m_pLeds;
	UINT		      m_uLedType;
	UINT		      m_uLedMode;
	UINT		      m_uBright;
	ITimer	            * m_pTimer;
	UINT		      m_uTickCount;
	UINT		      m_uIdentify;
	UINT		      m_uAlarm;
	BOOL		      m_fSiren;
	UINT		      m_uSiren;
	UINT		      m_uTimeout1;
	UINT		      m_uTimeout2;
	UINT		      m_uLoadTime;
	CLoadCounter	      m_LoadCount;
	CString		      m_IpList;
	CString		      m_IpLast;
	UINT		      m_uHalo;
	UINT		      m_uExitCode;
	UINT		      m_hUpdate;
	CByteArray	      m_Update;
	WORD		      m_wTcpPort;

	// State Machine
	void SwitchState(UINT uState);
	void SetStateTimer(UINT uDelta);
	void SetStateEvent(UINT uEvent, BOOL fWait);
	void SetDeferEvent(UINT uEvent);
	void OnEnterState(UINT uState);
	BOOL OnStateEvent(UINT uState, UINT uEvent);
	void OnStateTimer(UINT uState);
	void OnLeaveState(UINT uState);

	// Timer Helpers
	void FindLedType(void);
	void PollSiren(void);
	void PollRunning(void);
	void PollStopped(void);
	void PollDriveStatus(UINT uLED, BOOL fState);

	// Diagnostics
	BOOL DiagRegister(void);
	BOOL DiagRevoke(void);
	UINT DiagHello(IDiagOutput *pOut, IDiagCommand *pCmd);
	UINT DiagClearConfig(IDiagOutput *pOut, IDiagCommand *pCmd);
	void ShowTime(IDiagOutput *pOut);

	// Implementation
	void CreateModel(void);
	void CreatePxeObjects(void);
	BOOL CheckImageFiles(BOOL fLoad);
	BOOL LoadHardwareConfig(void);
	BOOL LoadSystemConfig(void);
	BOOL LoadPersonality(void);
	BOOL FreePersonality(void);
	BOOL LoadConfig(char cTag, BOOL fPerson, CJsonConfig * &pCon);
	BOOL ApplyHardwareConfig(CJsonConfig *pHcon);
	BOOL ApplySystemConfig(CJsonConfig *pScon, BOOL fInit);
	BOOL ApplyNetConfig(CJsonConfig *pNet);
	BOOL ApplyCertConfig(CJsonConfig *pIdent);
	BOOL ApplyLinkConfig(CJsonConfig *pLink, BOOL fInit);
	BOOL MakeCertificate(void);
	BOOL SaveCertificate(void);
	void Join(CString &x, CString const &a, CString const &b);
	void Join(CString &x, CString const &a);
	BOOL CheckCRC(DWORD &crc, CJsonConfig * &pJson);
	void CreateAppObjects(void);
	void ShowIpList(void);
	BOOL ApplyDeviceConfig(void);
	BOOL HasIpListChanged(void);
	void IpListChangeDone(void);
	void SetHalo(UINT uHalo);
	void CheckDebug(void);
	UINT GetActiveDrives(void);
};

// End of File

#endif
