
#include "Intern.hpp"

#include "PxeHttpServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PxeHttpServerSession.hpp"

#include "PxeWebServer.hpp"

#include "StringTable.hpp"

#include "WebFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Web Server
//

// Constructor

CPxeHttpServer::CPxeHttpServer(CPxeWebServer *pServer, CHttpServerManager *pManager, CHttpServerOptions const &Opts) : CHttpServer(pManager, Opts)
{
	m_pServer    = pServer;

	m_fRedirect  = FALSE;

	m_timeBoot   = time(NULL);

	m_timeComp   = CHttpTime::Parse("Day, " __DATE__ " " __TIME__ " EST"); // TODO -- Avoid image changes on build!!!

	m_SessionKey = "C32-Pxe-Session";

	m_uBackoff   = 0;
}

// Destructor

CPxeHttpServer::~CPxeHttpServer(void)
{
}

// Operations

void CPxeHttpServer::EnableRedirect(BOOL fEnable)
{
	m_fRedirect = fEnable;
}

// Overridables

void CPxeHttpServer::ServePage(CHttpServerSession *pSession, CConnectionInfo &Info, CHttpServerRequest *pReq)
{
	// Setup the request context we'll be using.

	CWebReqContext Ctx;

	Ctx.pSess = (CPxeHttpServerSession *) pSession;

	Ctx.pCon  = Info.m_pCon;

	Ctx.pReq  = pReq;

	// Parse the URI into the initial
	// path element and the remainder.

	CString Full = GetPath(Ctx.pReq);

	UINT    uPos = Full.FindRev('/');

	CString Name = Full.Mid(uPos+1);

	CString Path = Full.Left(uPos+1);

	// Are we handling a GET request?

	if( Ctx.pReq->GetVerb() == "GET" ) {

		// Requests for the favorite icon are special-cased
		// as browsers ping this URL a lot and we don't want
		// to trigger any security sessions etc.

		if( Full == "/favicon.ico" ) {

			if( m_pServer->ReplyWithPxeFile(Ctx, "/assets/icons/favicon.ico") ) {

				return;
			}

			Ctx.pReq->SetStatus(404);

			return;
		}

		// Check for re-entry from the runtime server and
		// allow it without a user check at this point.

		if( Full == "/ajax/jump-config.htm" ) {

			if( m_pServer->ExecuteAjax(Ctx, Name) ) {

				return;
			}
		}

		// Requests in the /assets folder are for the standard
		// scripts, stylesheets etc. that support the website.

		if( Path.Left(8) == "/assets/" ) {

			if( m_pServer->ReplyWithPxeFile(Ctx, Full) ) {

				return;
			}
		}

		// This is a good time to check the language.

		SetWebLanguage(pReq->GetRequestHeader("Accept-Language"));

		// If we've got this far, we need to start thinking
		// about form-based authentication if it is enabled.

		if( true ) {

			// Is this a file in the root directory?

			if( Path == "/" ) {

				// Root cert?

				if( Name == "c32root.crt" ) {

					AfxNewAutoObject(pGen, "certgen", ICertGen);

					if( pGen ) {

						Ctx.pReq->SetReplyBody(CByteArray(pGen->GetRootData(), pGen->GetRootSize()));

						Ctx.pReq->AddReplyHeader("Content-Disposition", "attachment; filename=\"c32root.crt\"");

						Ctx.pReq->SetContentType("application/x-x509-ca-cert");

						Ctx.pReq->SetStatus(200);
					}
					else
						Ctx.pReq->SetStatus(404);

					return;
				}

				// Logon page?

				if( Name == "logon.htm" ) {

					if( m_pServer->ReplyWithPxeFile(Ctx, Full) ) {

						return;
					}

					Ctx.pReq->SetStatus(404);

					return;
				}

				// Logoff page?

				if( Name == "logoff.htm" ) {

					// There isn't a real logoff page for form-based
					// authentication. Instead we clear the sesion
					// information and then redirect back to the logon
					// page so a new user can enter their details.

					((CPxeHttpServerSession *) Ctx.pSess)->ClearUserInfo();

					CString Body;

					Body += "<!DOCTYPE html>\r\n";
					Body += "<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n";
					Body += "<script type='text/javascript'>window.location='/logon.htm?uri=/default.htm'</script>";
					Body += "</html>\r\n";

					Ctx.pReq->SetReplyBody(Body);

					Ctx.pReq->SetStatus(200);

					Ctx.pCon->ClearKeepAlive();

					return;
				}

				// Logon script?

				if( Name == "logon.sub" ) {

					// This page is used to submit logon information from the
					// form on the logon page. The credentials are sent in the
					// clear in the URI, but that's okay as this method should
					// only be used over HTTPS.

					CString User = Ctx.pReq->GetParamString("user", "");

					CString Pass = Ctx.pReq->GetParamString("pass", "");

					// If the password is empty or invalid, reply with a suitable
					// error message that will be picked up by the logon script
					// and relayed to the user. Close the connection and pause a
					// little to prevent repeat attacks.

					AfxGetAutoObject(pPxe, "pxe", 0, ICrimsonPxe);

					CAuthInfo Auth;

					CUserInfo Info;

					Auth.m_Password = Pass;

					if( Pass.IsEmpty() || !pPxe->GetUserInfo(User, Auth, Info) ) {

						Ctx.pReq->SetReplyBody(GetWebString(IDS_ACCESS_DENIED));

						Ctx.pReq->SetStatus(401);

						Ctx.pCon->ClearKeepAlive();

						if( m_uBackoff < 60 ) {

							m_uBackoff++;
						}

						Sleep(500 * m_uBackoff);
					}
					else {
						// Load the info into the context object.

						CPxeHttpServerSession *pSess = (CPxeHttpServerSession *) Ctx.pSess;
						
						pSess->SetUserInfo(Info);

						// We're going to use the first eight of the session
						// opaque value as our URL-stored session identifier.

						Ctx.pReq->SetReplyBody(Ctx.pSess->GetOpaque().Left(8));

						Ctx.pReq->SetStatus(200);

						m_uBackoff = 0;
					}

					return;
				}
			}

			// Do we have a valid user right now?

			if( !Ctx.pSess->HasUser() ) {

				// Are we accessing an ajax resource or a debug map?

				if( Name.EndsWith(".map") || (Path == "/ajax/" && !Name.StartsWith("jump-")) ) {

					// If so, we're probably on a remote view or data
					// page, so send a reply that our scripts will pickup
					// to redirect the user to the logon page.

					Ctx.pReq->SetReplyBody("Unauthorized.");

					Ctx.pReq->SetStatus(401);
				}
				else {
					// Otherwise, unauthorized things are afoot so we need
					// to bounce the request and redirect the user to the logon
					// page. We pass the current URI in the logon path so that
					// the script can redirect after upon sucess, but we are
					// careful to first remove any session information.

					CString Body;

					CString Path = Ctx.pReq->GetFullPath();

					// Simple HTML to redirect us to the logon page.

					Body += "<!DOCTYPE html>\r\n";
					Body += "<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n";
					Body += "<script type='text/javascript'>window.location='/logon.htm?uri=";
					Body += Path;
					Body += "';</script>\r\n";
					Body += "</html>\r\n";

					Ctx.pReq->SetReplyBody(Body);

					Ctx.pReq->SetStatus(200);
				}

				return;
			}
		}

		// Now we can start serving real pages. Start
		// by considering those in the root directory.

		if( Path == "/" ) {

			if( m_pServer->ReplyWithPxeFile(Ctx, Full) ) {

				return;
			}
		}

		// And then check for any ajax-based resources.

		if( Path == "/ajax/" ) {

			if( m_pServer->ExecuteAjax(Ctx, Name) ) {

				return;
			}
		}

		// No joy so return with a 404 reply code.

		Ctx.pReq->SetStatus(404);

		return;
	}

	// Are we handling a POST request?

	if( Ctx.pReq->GetVerb() == "POST" ) {

		// Are we accessing an ajax resource?

		if( Path == "/ajax/" ) {

			// Do we have a valid user right now?

			if( !Ctx.pSess->HasUser() ) {

				Ctx.pReq->SetReplyBody("Unauthorized.");

				Ctx.pReq->SetStatus(401);
			}

			// Handle any ajax-based resources.

			if( m_pServer->ExecuteAjax(Ctx, Name) ) {

				return;
			}
		}

		// No joy so return with a 404 reply code.

		Ctx.pReq->SetStatus(404);

		return;
	}

	// Unsuppoted verb.

	Ctx.pReq->SetStatus(405);
}

BOOL CPxeHttpServer::CatchPost(IHttpStreamWrite * &pStm, CHttpServerConnection *pCon, CHttpServerRequest *pReq, PCTXT pPath)
{
	CHttpServerSession *pSess = NULL;
	
	FindSession(pSess, pCon, pReq);

	if( pSess ) {

		if( pSess->HasUser() ) {

			CPxeHttpServerSession *pOurs = (CPxeHttpServerSession *) pSess;

			if( pOurs->m_Info.m_uWebAccess == 0 ) {

				if( !strcmp(pPath, "/ajax/putimage.ajax") ) {

					mkdir("/update", 0755);

					if( CAutoFile("/update/image.ci3", "r") ) {

						unlink("/update/image.tmp");

						rename("/update/image.ci3", "image.tmp");
					}

					CAutoFile File("/update/image.tmp", "r+", "w+");

					if( File ) {

						// You can use MD5 here if you want the hash, but
						// right now, the JavaScript doesn't verify it...

						pStm = new CHttpStreamWriteFile(File.TakeOver());

						return TRUE;
					}
				}
			}
		}
	}

	return FALSE;
}

BOOL CPxeHttpServer::SkipAuth(CHttpServerRequest *pReq)
{
	// We don't apply HTTP security to the favorite
	// icon as many browsers ping it incessantly.

	if( pReq->GetPath() == "/favicon.ico" ) {

		return TRUE;
	}

	return CHttpServer::SkipAuth(pReq);
}

void CPxeHttpServer::MakeSession(CHttpServerSession * &pSess)
{
	pSess = New CPxeHttpServerSession(this);
}

void CPxeHttpServer::KillSession(CHttpServerSession *pSess)
{
	delete pSess;
}

BOOL CPxeHttpServer::UseForRedirect(UINT n, UINT &uPort, BOOL &fTls)
{
	if( n == 0 ) {

		uPort = 8080;

		fTls  = FALSE;

		return TRUE;
	}

	if( m_fRedirect ) {

		if( n == m_Opts.m_uSockCount - 2 ) {

			uPort = 80;

			fTls  = FALSE;

			return TRUE;
		}

		if( n == m_Opts.m_uSockCount - 1 ) {

			uPort = 443;

			fTls  = TRUE;

			return TRUE;
		}
	}

	return FALSE;
}

// Implementation

CString CPxeHttpServer::GetPath(CHttpServerRequest *pReq)
{
	CString Full = pReq->GetPath();

	if( Full == "/" ) {

		Full = "/default.htm";
	}

	return Full;
}

// End of File
