
#include "Intern.hpp"

#include "StdDebugCmds.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Standard Debug Commands
//

// Registration

void Register_StdDebugCmds(void)
{
	New CStdDebugCmds;
}

// Constructor

CStdDebugCmds::CStdDebugCmds(void)
{
	StdSetRef();

	m_pDiag     = NULL;

	m_timeStart = time(NULL);

	AddDiagCmds();
}

// Destructor

CStdDebugCmds::~CStdDebugCmds(void)
{
	if( m_pDiag ) {

		m_pDiag->RevokeProvider(m_uProv);

		m_pDiag->Release();

		m_pDiag = NULL;
	}
}

// IUnknown

HRESULT CStdDebugCmds::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDiagProvider);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
}

ULONG CStdDebugCmds::AddRef(void)
{
	StdAddRef();
}

ULONG CStdDebugCmds::Release(void)
{
	StdRelease();
}

// IDiagProvider

UINT CStdDebugCmds::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		// TODO -- Add ability to delete with wildcards.

		case cmdChangeDir:	return CmdChangeDir(pOut, pCmd);
		case cmdClearGMC:	return CmdClearGMC(pOut, pCmd);
		case cmdCrash:		return CmdCrash(pOut, pCmd);
		case cmdCrashDump:	return CmdCrashDump(pOut, pCmd);
		case cmdDelete:		return CmdDelete(pOut, pCmd);
		case cmdDir:		return CmdDir(pOut, pCmd);
		case cmdFormat:		return CmdFormat(pOut, pCmd);
		case cmdFramDump:	return CmdFramDump(pOut, pCmd);
		case cmdHexDump:	return CmdHexDump(pOut, pCmd);
		case cmdMakeDir:	return CmdMakeDir(pOut, pCmd);
		case cmdMemCheck:	return CmdMemCheck(pOut, pCmd);
		case cmdMemStat:	return CmdMemStat(pOut, pCmd);
		case cmdMemTest:	return CmdMemTest(pOut, pCmd);
		case cmdMount:		return CmdMount(pOut, pCmd);
		case cmdPing:		return CmdPing(pOut, pCmd);
		case cmdPwd:		return CmdPwd(pOut, pCmd);
		case cmdRemoveDir:	return CmdRemoveDir(pOut, pCmd);
		case cmdReset:		return CmdReset(pOut, pCmd);
		case cmdTime:		return CmdTime(pOut, pCmd);
		case cmdTraps:		return CmdTraps(pOut, pCmd);
		case cmdType:		return CmdType(pOut, pCmd);
		case cmdUnmount:	return CmdUnmount(pOut, pCmd);
		case cmdUpTime:		return CmdUpTime(pOut, pCmd);
	}

	return 0;
}

// Registration

void CStdDebugCmds::AddDiagCmds(void)
{
	AfxGetObject("aeon.diagmanager", 0, IDiagManager, m_pDiag);

	if( m_pDiag ) {

		m_uProv = m_pDiag->RegisterProvider(this, "");

		m_pDiag->RegisterCommand(m_uProv, cmdType, "cat");
		m_pDiag->RegisterCommand(m_uProv, cmdChangeDir, "cd");
		m_pDiag->RegisterCommand(m_uProv, cmdClearGMC, "cleargmc");
		m_pDiag->RegisterCommand(m_uProv, cmdCrash, "crash");
		m_pDiag->RegisterCommand(m_uProv, cmdCrashDump, "crashdump");
		m_pDiag->RegisterCommand(m_uProv, cmdDelete, "delete");
		m_pDiag->RegisterCommand(m_uProv, cmdDir, "dir");
		m_pDiag->RegisterCommand(m_uProv, cmdFormat, "format");
		m_pDiag->RegisterCommand(m_uProv, cmdFramDump, "fram");
		m_pDiag->RegisterCommand(m_uProv, cmdHexDump, "hd");
		m_pDiag->RegisterCommand(m_uProv, cmdDir, "ls");
		m_pDiag->RegisterCommand(m_uProv, cmdMakeDir, "mkdir");
	//	m_pDiag->RegisterCommand(m_uProv, cmdMemCheck, "memcheck");
	//	m_pDiag->RegisterCommand(m_uProv, cmdMemStat, "memstat");
		m_pDiag->RegisterCommand(m_uProv, cmdMemTest, "memtest");
	//	m_pDiag->RegisterCommand(m_uProv, cmdMount, "mount");
		m_pDiag->RegisterCommand(m_uProv, cmdPing, "ping");
		m_pDiag->RegisterCommand(m_uProv, cmdPwd, "pwd");
		m_pDiag->RegisterCommand(m_uProv, cmdRemoveDir, "rmdir");
	//	m_pDiag->RegisterCommand(m_uProv, cmdReset, "reset");
		m_pDiag->RegisterCommand(m_uProv, cmdTime, "time");
		m_pDiag->RegisterCommand(m_uProv, cmdTraps, "traps");
		m_pDiag->RegisterCommand(m_uProv, cmdType, "type");
	//	m_pDiag->RegisterCommand(m_uProv, cmdUnmount, "unmount");
		m_pDiag->RegisterCommand(m_uProv, cmdUpTime, "uptime");
	}
}

// Command Handlers

UINT CStdDebugCmds::CmdChangeDir(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		if( !chdir(pCmd->GetArg(0)) ) {

			char path[MAX_PATH];

			getcwd(path, sizeof(path));

			pOut->Print("cd: directory is %s\n", path);

			return 0;
		}

		pOut->Error("can't find directory");

		return 2;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdChkDsk(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	/* !!! ADD ME !!! */

	return 0;
}

UINT CStdDebugCmds::CmdClearGMC(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

//		TrapAcknowledge();

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdCrash(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		Sleep(100);

		HostTrap(1);

		PVBYTE p1 = PVBYTE(0);

		*p1++;

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdCrashDump(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		// TODO -- This is not provided...

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdDelete(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		if( !unlink(pCmd->GetArg(0)) ) {

			return 0;
		}

		pOut->Error("can't open file");

		return 2;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdDir(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		char path[MAX_PATH];

		getcwd(path, sizeof(path));

		pOut->Print("Directory of %s\n\n", path);

		CAutoDirentList List;

		if( List.Scan(".") ) {

			for( UINT n = 0; n < List.GetCount(); n++ ) {

				dirent *pFile = List[n];

				char full[MAX_PATH];

				PathMakeAbsolute(full, pFile->d_name);

				struct stat s;

				stat(full, &s);

				struct tm *tm = gmtime(&s.st_mtime);

				pOut->Print("  %2.2d/%2.2d/%4.4d ", 1+tm->tm_mon, tm->tm_mday, 1900+tm->tm_year);

				pOut->Print("%2.2d:%2.2d:%2.2d  ", tm->tm_hour, tm->tm_min, tm->tm_sec);

				if( pFile->d_type == DT_DIR ) {

					pOut->Print("%-12s ", "<DIR>");
				}
				else
					pOut->Print("%-12u ", DWORD(s.st_size));

				pOut->Print("%s\n", pFile->d_name);
			}
		}

		if( TRUE ) {

			AfxGetAutoObject(pUtils, "aeon.filesupport", 0, IFileUtilities);

			if( pUtils ) {

				UINT64 s = pUtils->GetDiskSize(path[0]);

				UINT64 f = pUtils->GetDiskFree(path[0]);

				if( List.GetCount() ) {

					pOut->Print("\n");
				}

				pOut->Print("  %14llu bytes total\n", s);

				pOut->Print("  %14llu bytes free\n", f);
			}
		}

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdFormat(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		AfxGetAutoObject(pUtils, "aeon.filesupport", 0, IFileUtilities);

		if( pUtils ) {

			PCTXT p = pCmd->GetArg(0);

			if( strlen(p) == 1 ) {

				if( isalpha(p[0]) ) {

					if( pUtils->FormatDisk(p[0]) ) {

						pOut->Print("format operation in progress\n");

						return 0;
					}

					pOut->Print("operation failed");

					return 2;
				}
			}
		}

		pOut->Error("operation not available");

		return 2;
	}

	pOut->Error("missing drive letter");

	return 1;
}

UINT CStdDebugCmds::CmdFramDump(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() >= 0 && pCmd->GetArgCount() <= 2 ) {

		AfxGetAutoObject(pFram, "fram", 0, ISerialMemory);

		if( pFram ) {

			UINT uInit = 0;

			UINT uSize = pFram->GetSize();

			if( ParseRange(pCmd, uInit, uSize, 0, 256) ) {

				CAutoArray<BYTE> pData(uSize);

				pFram->GetData(uInit, pData, uSize);

				pOut->Dump(pData, uSize, uInit);

				return 0;
			}
		}

		pOut->Error("fram not available");

		return 1;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdHello(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	pOut->Print("Crimson 3.2 Debug Console\n");

	pOut->Print("Time is ");

	ShowTime(pOut);

	pOut->Print("\n");

	return 0;
}

UINT CStdDebugCmds::CmdHexDump(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() >= 0 && pCmd->GetArgCount() <= 3 ) {

		CAutoFile File(pCmd->GetArg(pCmd->GetArgCount() - 1), "rb");

		if( File ) {

			UINT uInit = 0;

			UINT uSize = File.GetSize();

			if( ParseRange(pCmd, uInit, uSize, 0, 256) ) {

				CAutoArray<BYTE> Data(uSize);

				File.Seek(uInit);

				if( File.Read(Data, uSize) == uSize ) {

					File.Close();

					pOut->Dump(Data, uSize, uInit);

					return 0;
				}

				pOut->Error("unable to read file");

				return 3;
			}

			pOut->Error(NULL);

			return 1;
		}

		pOut->Error("unable to open file");

		return 2;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdMakeDir(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		if( !mkdir(pCmd->GetArg(0), 0) ) {

			pOut->Print("mkdir: directory created\n");

			return 0;
		}

		pOut->Error("can't make directory");

		return 2;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdMemCheck(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if 0

	if( !pCmd->GetArgCount() ) {

		// TODO -- Should use pOut

		extern void AfxCheckMemory(void);

		AfxCheckMemory();

		return 0;
	}

	#endif

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdMemStat(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if 0

	if( !pCmd->GetArgCount() ) {

		// TODO -- Is this right?

		extern void AfxCheckMemory(void);

		AfxCheckMemory();

		return 0;
	}

	#endif

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdMemTest(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		// TODO -- This doesn't do much these days...

		CmdMemStat(pOut, pCmd);

		UINT  uCount = 1;

		PBYTE *pData = New PBYTE[uCount];

		for( UINT a = 0; a < uCount; a++ ) {

			pData[a] = New BYTE[1024];

			memset(pData[a], BYTE(a), 1024);
		}

		CmdMemStat(pOut, pCmd);

		for( UINT b = 0; b < uCount; b++ ) {

			for( UINT i = 0; i < 1024; i++ ) {

				if( pData[b][i] != BYTE(b) ) {

					pOut->Print("Error at %p\n", pData[b] + i);

					break;
				}
			}

			memset(pData[b], 0, 1024);

			delete[] pData[b];
		}

		delete[] pData;

		CmdMemStat(pOut, pCmd);

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdMount(IDiagOutput *pOut, IDiagCommand *pCmd)
{
/*	if( !pCmd->GetArgCount() ) {

		m_pDiskMan->CheckDisk(m_pDiskMan->FindDisk('C'));

		m_pDiskMan->CheckDisk(m_pDiskMan->FindDisk('D'));

		return 0;
		}

*/	pOut->Error(NULL);

return 1;
}

UINT CStdDebugCmds::CmdPing(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		AfxGetAutoObject(pDns, "dns", 0, IDnsResolver);

		CString Arg  = pCmd->GetArg(0);

		CIpAddr Addr = pDns ? pDns->Resolve(Arg) : CIpAddr(Arg);

		if( !Addr.IsEmpty() ) {

			AfxGetAutoObject(pUtils, "ip", 0, INetUtilities);

			if( pUtils ) {

				CString Name = Addr.GetAsText();

				UINT uTotal = 0;

				UINT uCount = 0;

				UINT n;

				for( n = 0; n < 4; n++ ) {

					UINT uTime = pUtils->Ping(Addr, 5000);

					if( uTime == NOTHING ) {

						if( !uCount ) {

							pOut->Print("ping: no reply from %s\n", PCTXT(Name));

							break;
						}

						pOut->Print("Reply from %s : LOST\n", PCTXT(Name));
					}
					else {
						pOut->Print("Reply from %s : %5ums\n", PCTXT(Name), uTime);

						uTotal += uTime;

						uCount += 1;
					}

					Sleep(50);
				}

				if( n == 4 ) {

					pOut->Print("\nAverage time is %ums\n", uTotal / uCount);
				}

				return 0;
			}

			pOut->Error("ip not enabled\n");

			return 3;
		}

		pOut->Error("unable to resolve hostname\n");

		return 2;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdPwd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		char path[MAX_PATH];

		getcwd(path, sizeof(path));

		pOut->Print("pwd: directory is %s\n", path);

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdRemoveDir(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		if( !rmdir(pCmd->GetArg(0)) ) {

			pOut->Print("rmdir: directory removed\n");

			return 0;
		}

		pOut->Error("can't remove directory");

		return 2;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdReset(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if 0

	if( !pCmd->GetArgCount() ) {

		if( pCmd->FromConsole() ) {

			Critical(TRUE);

			NICTerm();

			DriveTerm();

			for( ;;) HostMaxIPL();

			return 0;
		}

		pOut->Error("only supported from system console");

		return 2;
	}

	#endif

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdTime(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->Print("Time is ");

		ShowTime(pOut);

		// TODO -- !!!

		// pOut->Print("\n\nBattery is %s\n", IsBatteryOK() ? "OKAY" : "BAD");

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdTraps(IDiagOutput *pOut, IDiagCommand *pCmd)
{
/*	if( !pCmd->GetArgCount() ) {

		// TODO -- Is this obsolete?

		pOut->Print("Last seven resets:\n\n");

		for( UINT n = 1; n < 8; n++ ) {

			DWORD dwData[4];

			pOut->Print("  %u)  ", n);

			if( TrapGetData(n, dwData) ) {

				DWORD Time = dwData[1];

				pOut->Print( "%2.2u/%2.2u/%2.2u %2.2u:%2.2u:%2.2u  ",
					  GetMonth(Time),
					  GetDate (Time),
					  GetYear (Time) % 100,
					  GetHour (Time),
					  GetMin  (Time),
					  GetSec  (Time)
					  );

				if( !dwData[3] ) {

					pOut->Print("Software Reset");
					}
				else {
					UINT  uType  = ((dwData[2] >> 18) & 0xFF);

					DWORD dwAddr = dwData[3];

					pOut->Print("Exception 0x%2.2X at %8.8X", uType, dwAddr);
					}
				}
			else {
				pOut->Print("00/00/00 00:00:00  ");

				pOut->Print("Hardware Reset");
				}

			pOut->Print("\n");
			}

		return 0;
		}

*/	pOut->Error(NULL);

return 1;
}

UINT CStdDebugCmds::CmdType(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() >= 0 && pCmd->GetArgCount() <= 3 ) {

		CAutoFile File(pCmd->GetArg(pCmd->GetArgCount() - 1), "rb");

		if( File ) {

			UINT uData = File.GetSize();

			if( uData < 512 * 1024 ) {

				CAutoArray<char> Data(uData+2);

				if( File.Read(Data, uData) == uData ) {

					File.Close();

					Data[uData+0] = '\n';

					Data[uData+1] = 0;

					CArray <char *> Lines;

					for( char *pScan = Data; pScan < Data + uData; ) {

						char *pFind = strchr(pScan, '\n');

						if( pFind - pScan > 200 ) {

							pOut->Error("lines too long or file not ASCII");

							return 2;
						}

						if( pFind > pScan && pFind[-1] == '\r' ) {

							pFind[-1] = 0;
						}
						else
							*pFind = 0;

						Lines.Append(pScan);

						pScan = pFind + 1;
					}

					UINT uInit = 0;

					UINT uRead = Lines.GetCount();

					if( ParseRange(pCmd, uInit, uRead, 1, 20) ) {

						UINT d = 0;

						for( UINT x = uInit+uRead; x; x /= 10 ) {

							d++;
						}

						if( uInit ) {

							pOut->Print("(earlier data skipped)\n");
						}

						for( UINT n = uInit; n < uInit+uRead; n++ ) {

							pOut->Print("%*.*u: %s\n", d, d, n+1, Lines[n]);
						}

						if( uInit+uRead < Lines.GetCount() ) {

							pOut->Print("(further data skipped)\n");
						}

						return 0;
					}

					pOut->Error(NULL);

					return 1;
				}
			}

			pOut->Error("file is too long");

			return 4;
		}

		pOut->Error("unable to open file");

		return 2;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CStdDebugCmds::CmdUnmount(IDiagOutput *pOut, IDiagCommand *pCmd)
{
/*	if( !pCmd->GetArgCount() ) {

		if( FindFileSystem() ) {

			m_pDiskMan->EjectDisk(m_pFileSys->GetDisk());
			}

		FreeFileSystem();

		return 0;
		}

*/	pOut->Error(NULL);

return 1;
}

UINT CStdDebugCmds::CmdUpTime(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		time_t up = time(NULL) - m_timeStart;

		int d = up / (24 * 60 * 60);
		int h = up / (60 * 60) % 24;
		int m = up / (60) % 60;
		int s = up / (1) % 60;

		PCTXT e = (d == 1) ? "" : "s";

		pOut->Print("Time is ");

		ShowTime(pOut);

		pOut->Print("\nUp for %u day%s, %2.2u:%2.2u:%2.2u\n", d, e, h, m, s);

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

// Implementation

BOOL CStdDebugCmds::ParseRange(IDiagCommand *pCmd, UINT &uInit, UINT &uSize, UINT uBase, UINT uDefault)
{
	// Possible syntaxes...
	//
	// b		Display default number of entries from b
	// +n		Display first n entries
	// -n		Display last n entries
	// b n		Display n entries from b
	// b+n		Display n entries from b
	// b-n		Display entries from b to n inclusive

	UINT uLimit = uSize;

	UINT uArgs  = pCmd->GetArgCount();

	if( uArgs == 1 ) {

		uInit = uBase;

		uSize = uDefault;
	}
	else {
		CString a0 = (uArgs >= 2) ? pCmd->GetArg(0) : "";

		CString a1 = (uArgs >= 3) ? pCmd->GetArg(1) : "";

		if( uArgs == 2 ) {

			char const *s = "-+";

			for( int n = 0; s[n]; n++ ) {

				UINT f = a0.Find(s[n]);

				if( f && f < NOTHING ) {

					a1 = a0.Mid(f);

					a0 = a0.Left(f);

					uArgs++;

					break;
				}
			}
		}

		if( uArgs == 2 ) {

			if( a0.StartsWith("+") ) {

				uInit = uBase;

				uSize = ParseNumber(a0.Mid(1));
			}
			else {
				if( a0.StartsWith("-") ) {

					uSize = ParseNumber(a0.Mid(1));

					MakeMin(uSize, uLimit);

					uInit = uLimit - uSize;

					return TRUE;
				}

				uInit = ParseNumber(a0);

				uSize = uDefault;

				MakeMax(uInit, uBase);

				MakeMin(uInit, uBase + uLimit - 1);
			}
		}

		if( uArgs == 3 ) {

			uInit = ParseNumber(a0);

			MakeMax(uInit, uBase);

			MakeMin(uInit, uBase + uLimit - 1);

			if( a1.StartsWith("-") ) {

				uSize = 1 + ParseNumber(a1.Mid(1));

				if( uSize <= uInit ) {

					return FALSE;
				}

				uSize -= uInit;
			}
			else {
				if( a1.StartsWith("+") ) {

					uSize = ParseNumber(a1.Mid(1));
				}
				else
					uSize = ParseNumber(a1);
			}
		}
	}

	uInit -= uBase;

	MakeMin(uSize, uLimit - uInit);

	return TRUE;
}

UINT CStdDebugCmds::ParseNumber(CString const &Text)
{
	if( Text.StartsWith("0x") ) {

		return strtoul(PCSTR(Text)+2, NULL, 16);
	}

	return strtoul(Text, NULL, 10);
}

void CStdDebugCmds::ShowTime(IDiagOutput *pOut)
{
	time_t t = time(NULL);

	struct tm *tm = gmtime(&t);

	pOut->Print("%2.2u/%2.2u/%2.2u %2.2u:%2.2u:%2.2u\n",
		    tm->tm_mon  + 1,
		    tm->tm_mday + 0,
		    tm->tm_year + 1900,
		    tm->tm_hour,
		    tm->tm_min,
		    tm->tm_sec
	);
}

// End of File
