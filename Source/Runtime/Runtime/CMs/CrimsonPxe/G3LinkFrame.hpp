
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3LinkFrame_HPP

#define	INCLUDE_G3LinkFrame_HPP

////////////////////////////////////////////////////////////////////////
//
// G3 Link Command Flags
//

enum LinkFlags
{
	flagsAsync = 0x02,
};

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Frame
//

class CG3LinkFrame
{
public:
	// Constructor
	CG3LinkFrame(void);

	// Destructor
	~CG3LinkFrame(void);

	// Frame Creation
	void StartFrame(BYTE bService, BYTE bOpcode);

	// Operations
	void MarkAsync(void);
	void MarkVerySlow(void);
	void SetFlags(BYTE bFlags);

	// Simple Data
	void AddByte(BYTE bData);
	void AddWord(WORD wData);
	void AddLong(LONG lData);
	void AddGuid(GUID Guid);

	// Larger Data
	void AddData(PCBYTE pData, UINT uCount);
	void AddBulk(PCBYTE pData, UINT uCount);

	// Data Reads
	PCBYTE ReadData(UINT &uPtr, UINT uCount) const;
	GUID & ReadGuid(UINT &uPtr) const;
	BYTE   ReadByte(UINT &uPtr) const;
	WORD   ReadWord(UINT &uPtr) const;
	DWORD  ReadLong(UINT &uPtr) const;

	// Attributes
	BYTE  GetService(void) const;
	BYTE  GetOpcode(void) const;
	BYTE  GetFlags(void) const;
	BOOL  IsAsync(void) const;
	BOOL  IsVerySlow(void) const;
	UINT  GetDataSize(void) const;
	UINT  GetBulkSize(void) const;
	PBYTE GetData(void) const;
	PBYTE GetBulk(void) const;
	BYTE  GetAt(UINT uPos);

protected:
	// Data Members	  
	BYTE  m_bService;
	BYTE  m_bOpcode;
	BYTE  m_bFlags;
	UINT  m_uDataCount;
	UINT  m_uBulkCount;
	UINT  m_uDataAlloc;
	UINT  m_uBulkAlloc;
	PBYTE m_pData;
	PBYTE m_pBulk;
	BOOL  m_fSlow;

	// Implementation
	void FreeData(void);
	void FreeBulk(void);
	void ExpandData(UINT uExtra);
	void ExpandBulk(UINT uExtra);
};

// End of File

#endif
