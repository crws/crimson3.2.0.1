
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinuxHardwareApplicator_HPP

#define INCLUDE_LinuxHardwareApplicator_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// Linux Hardware Applicator
//

class CLinuxHardwareApplicator : public IHardwareApplicator
{
public:
	// Constructor
	CLinuxHardwareApplicator(void);

	// Destructor
	~CLinuxHardwareApplicator(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IHardwareApplicator
	bool METHOD ApplySettings(CJsonConfig *pJson);

protected:
	// Data Members
	ULONG	    m_uRefs;
	IPxeModel * m_pModel;
	DWORD	    m_crcConfig;

	// Implementation
	void ApplyPowerScheme(CJsonConfig *pJson);
	void ApplyCoreScheme(UINT uMode);
	void ApplyHaloScheme(UINT uMode);
	void RegisterPorts(void);
	void RegisterSleds(CJsonConfig *pJson);
	void RegisterNics(void);
	BOOL CheckCRC(DWORD &crc, CJsonConfig * &pJson);
};

// End of File

#endif
