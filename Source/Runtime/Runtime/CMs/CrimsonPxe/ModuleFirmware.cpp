
#include "Intern.hpp"

#include "ModuleFirmware.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// USB Identifiers
//

#include "..\..\..\HXLs\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Module Images
//

#include "gmcdl.bin.dat"
#include "gmcn.bin.dat"
#include "gmdn.bin.dat"
#include "gmdnp3.bin.dat"
#include "gmj1939.bin.dat"
#include "gmpb.bin.dat"
#include "gmrc.bin.dat"

//////////////////////////////////////////////////////////////////////////
//
// Module Firmware
//

// Constructor

CModuleFirmware::CModuleFirmware(void)
{
	StdSetRef();
}

// Destructor

CModuleFirmware::~CModuleFirmware(void)
{
}

// IUnknown

HRESULT CModuleFirmware::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IExpansionFirmware);

	StdQueryInterface(IExpansionFirmware);

	return E_NOINTERFACE;
}

ULONG CModuleFirmware::AddRef(void)
{
	StdAddRef();
}

ULONG CModuleFirmware::Release(void)
{
	StdRelease();
}

// IExpansionFirmware

BOOL CModuleFirmware::SetFirmwareLock(BOOL fSet)
{
	// TODO -- Is this needed in C3.2?!!!

	return TRUE;
}

PCBYTE CModuleFirmware::GetFirmwareGuid(UINT uFirm)
{
	PCBYTE pData;

	UINT   uSize;

	if( FindData(pData, uSize, uFirm) ) {

		pData += uSize;

		pData -= 24;

		if( !strcmp(PCSTR(pData), "GUID-->") ) {

			return pData + 8;
		}
	}

	return NULL;
}

UINT CModuleFirmware::GetFirmwareSize(UINT uFirm)
{
	PCBYTE pData;

	UINT   uSize;

	if( FindData(pData, uSize, uFirm) ) {

		return uSize;
	}

	return 0;
}

PCBYTE CModuleFirmware::GetFirmwareData(UINT uFirm)
{
	PCBYTE pData;

	UINT   uSize;

	if( FindData(pData, uSize, uFirm) ) {

		return pData;
	}

	return NULL;
}

// Implementation

BOOL CModuleFirmware::FindData(PCBYTE &pData, UINT &uSize, UINT uFirm)
{
	switch( uFirm ) {

		case pidDevNetBoot:
		{
			pData = data_gmdn_bin;
			uSize = size_gmdn_bin;
			return TRUE;
		}

		case pidCanBoot:
		{
			pData = data_gmcn_bin;
			uSize = size_gmcn_bin;
			return TRUE;
		}

		case pidProfibusBoot:
		{
			pData = data_gmpb_bin;
			uSize = size_gmpb_bin;
			return TRUE;
		}

		case pidJ1939Boot:
		{
			pData = data_gmj1939_bin;
			uSize = size_gmj1939_bin;
			return TRUE;
		}

		case pidSerialBoot:
		{
			pData = data_gmrc_bin;
			uSize = size_gmrc_bin;
			return TRUE;
		}

		case pidDnp3Boot:
		{
			pData = data_gmdnp3_bin;
			uSize = size_gmdnp3_bin;
			return TRUE;
		}

		case pidCanCdlBoot:
		{
			pData = data_gmcdl_bin;
			uSize = size_gmcdl_bin;
			return TRUE;
		}
	}

	return FALSE;
}

// End of File
