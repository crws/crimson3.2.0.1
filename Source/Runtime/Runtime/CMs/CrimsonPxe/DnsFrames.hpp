
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DnsFrames_HPP

#define	INCLUDE_DnsFrames_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDnsResolver;
class CDnsCache;
class CDnsHeader;

//////////////////////////////////////////////////////////////////////////
//
// DNS Types
//

enum DNS_Type
{
	typeA		= 1,
	typeNS		= 2,
	typeMD		= 3,
	typeMF		= 4,
	typeCNAME	= 5,
	typeSOA		= 6,
	typeMB		= 7,
	typeMG		= 8,
	typeMR		= 9,
	typeNULL	= 10,
	typeWKS		= 11,
	typePTR		= 12,
	typeHINFO	= 13,
	typeMINFO	= 14,
	typeMX		= 15,
	typeTXT		= 16,
	typeAAAA	= 28,
	typeAXFR	= 252,
	typeMAILB	= 253,
	typeMAILA	= 254,
	typeALL		= 255,

};

//////////////////////////////////////////////////////////////////////////
//
// DNS Class Values
//

enum DNS_Class
{
	classIN		= 1,
	classCS		= 2,
	classCH		= 3,
	classHS		= 4,
	classANY	= 255,

};

//////////////////////////////////////////////////////////////////////////
//
// DNS Flags
//

enum DNS_Flag
{
	flagRA		= 0x0001,
	flagRD		= 0x0002,
	flagTC		= 0x0004,
	flagAA		= 0x0008,

};

//////////////////////////////////////////////////////////////////////////
//
// DNS OpCodes
//

enum DNS_Opcode
{
	opSTD		= 0,
	opINV		= 1,
	opSTATUS	= 2,

};

//////////////////////////////////////////////////////////////////////////
//
// DNS Response Code
//

enum DNS_RCode
{
	rcOK		= 0,
	rcFMT		= 1,
	rcSVR		= 2,
	rcERR		= 3,
	rcNIMP		= 4,
	rcRFD		= 5,

};

//////////////////////////////////////////////////////////////////////////
//
// DNS Constansts
//

#define DNS_MAX_LABEL	64

#define DNS_MAX_NAME	255

#define DNS_MAX_MSG	512

//////////////////////////////////////////////////////////////////////////
//
// DNS Header
//

#pragma pack(1)

#if defined(_E_LITTLE)

struct DNS_HEADER
{
	WORD	m_ID;
	WORD	m_RD : 1;
	WORD	m_TC : 1;
	WORD	m_AA : 1;
	WORD	m_Opcode : 4;
	WORD	m_QR : 1;
	WORD	m_RCode : 4;
	WORD	m_Z : 3;
	WORD	m_RA : 1;
	WORD	m_QDCount;
	WORD	m_ANCount;
	WORD	m_NSCount;
	WORD	m_ARCount;
};

#else

struct DNS_HEADER
{
	WORD	m_ID;
	WORD	m_QR : 1;
	WORD	m_Opcode : 4;
	WORD	m_AA : 1;
	WORD	m_TC : 1;
	WORD	m_RD : 1;
	WORD	m_RA : 1;
	WORD	m_Z : 3;
	WORD	m_RCode : 4;
	WORD	m_QDCount;
	WORD	m_ANCount;
	WORD	m_NSCount;
	WORD	m_ARCount;
};

#endif

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// DNS Label
//

#pragma pack(1)

struct DNS_LABEL
{
	BYTE	m_Data[DNS_MAX_LABEL];
};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// DNS Name
//

#pragma pack(1)

struct DNS_NAME
{
	BYTE	m_Data[DNS_MAX_NAME];
};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// DNS Question Footer
//

#pragma pack(1)

struct DNS_QFOOT
{
	WORD	m_Type;
	WORD	m_Class;

};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// DNS Resource Footer
//

#pragma pack(1)

struct DNS_RRFOOT
{
	WORD	m_Type;
	WORD	m_Class;
	DWORD	m_TTL;
	WORD	m_RDLen;
	BYTE	m_RData[4];

};

#pragma pack()

// End of File

#endif
