
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MultiDns_HPP

#define INCLUDE_MultiDns_HPP

//////////////////////////////////////////////////////////////////////////
//
// Multicast DNS Server
//

class CMultiDns : public IClientProcess
{
public:
	// Protocols
	enum
	{
		protocolMDNS  = 1,
		protocolLLMNR = 2
	};

// Constructor
	CMultiDns(UINT Protocol, CString Name1, CString Name2);

	// Destructor
	~CMultiDns(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Data Members
	ULONG	  m_uRefs;
	UINT      m_Protocol;
	CString	  m_Name1;
	CString	  m_Name2;
	ISocket * m_pRecvSock;
	ISocket * m_pSendSock;

	// Implementation
	bool OpenSocket(void);
	void KillSockets(void);
	bool GetMulticast(CIpAddr &Ip);
	WORD GetPort(void);
	void AddText(CBuffer *pBuff, CString Name);
	void AddByte(CBuffer *pBuff, BYTE Data);
	void AddWord(CBuffer *pBuff, WORD Data);
	void AddLong(CBuffer *pBuff, DWORD Data);
	void Process(CBuffer *pBuff);
};

// End of File

#endif
