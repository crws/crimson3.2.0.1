
#include "Intern.hpp"

#include "UsbModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// USB Module Information Object
//

// Constructor

CUsbModule::CUsbModule(DWORD dwSlot)
{
	m_dwSlot = dwSlot;

	StdSetRef();
}

// Destructor

CUsbModule::~CUsbModule(void)
{
}

// IUnknown

HRESULT CUsbModule::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IUsbModule);

	StdQueryInterface(IUsbModule);

	return E_NOINTERFACE;
}

ULONG CUsbModule::AddRef(void)
{
	StdAddRef();
}

ULONG CUsbModule::Release(void)
{
	StdRelease();
}

// IUsbModule

DWORD CUsbModule::GetSlot(void)
{
	return m_dwSlot;
}

// End of File
