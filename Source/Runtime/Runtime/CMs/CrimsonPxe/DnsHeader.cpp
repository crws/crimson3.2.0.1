
#include "Intern.hpp"

#include "DnsHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DNS Header Wrapper
//

// Conversion

void CDnsHeader::NetToHost(void)
{
	#if defined(_E_LITTLE)

	m_ID      = ::NetToHost(m_ID);
	m_QDCount = ::NetToHost(m_QDCount);
	m_ANCount = ::NetToHost(m_ANCount);
	m_NSCount = ::NetToHost(m_NSCount);
	m_ARCount = ::NetToHost(m_ARCount);

	#endif
}

void CDnsHeader::HostToNet(void)
{
	#if defined(_E_LITTLE)

	m_ID      = ::HostToNet(m_ID);
	m_QDCount = ::HostToNet(m_QDCount);
	m_ANCount = ::HostToNet(m_ANCount);
	m_NSCount = ::HostToNet(m_NSCount);
	m_ARCount = ::HostToNet(m_ARCount);

	#endif
}

// Operations

void CDnsHeader::Init(void)
{
	memset(this, 0, sizeof(*this));

	m_ID = WORD(rand());
}

//////////////////////////////////////////////////////////////////////////
//
// DNS Label Wrapper
//

// Conversion

void CDnsLabel::NetToHost(void)
{
	#if defined(_E_LITTLE)


	#endif
}

void CDnsLabel::HostToNet(void)
{
	#if defined(_E_LITTLE)


	#endif
}

// Attributes

BOOL CDnsLabel::IsNull(void) const
{
	return m_Data[0] == 0;
}

BOOL CDnsLabel::IsPointer(void) const
{
	return (m_Data[0] & 0xC0) == 0xC0;
}

BOOL CDnsLabel::IsLabel(void) const
{
	return (m_Data[0] & 0xC0) == 0x00;
}

UINT CDnsLabel::GetLength(void) const
{
	return IsLabel() ? m_Data[0] : 0;
}

UINT CDnsLabel::GetFree(void) const
{
	return DNS_MAX_LABEL - GetSize();
}

UINT CDnsLabel::GetSize(void) const
{
	return IsLabel() ? m_Data[0] + 1 : sizeof(WORD);
}

UINT CDnsLabel::GetPointer(void) const
{
	return IsPointer() ? (MotorToHost((WORD &) m_Data[0])) & 0x3FFF : 0;
}

// Operations 

void CDnsLabel::Init(void)
{
	SetNull();
}

PCTXT CDnsLabel::GetText(void) const
{
	return IsLabel() ? PCTXT(m_Data) + 1 : "<PTR>";
}

BOOL CDnsLabel::SetText(PCTXT pText)
{
	return SetText(pText, strlen(pText));
}

BOOL CDnsLabel::SetText(PCTXT pText, UINT uLen)
{
	if( uLen < DNS_MAX_LABEL ) {

		m_Data[0] = uLen & 0x3F;

		memcpy(&m_Data[1], pText, uLen);

		return TRUE;
	}

	return FALSE;
}

BOOL CDnsLabel::SetPointer(WORD wPtr)
{
	if( wPtr <= 0x3FFF ) {

		((WORD &) m_Data[0]) = wPtr | 0xC000;

		return TRUE;
	}

	return FALSE;
}

void CDnsLabel::SetNull(void)
{
	m_Data[0] = 0;
}

// Operators

CDnsLabel::operator PCBYTE (void) const
{
	return m_Data;
}

//////////////////////////////////////////////////////////////////////////
//
// DNS Name Wrapper
//

// Conversion

void CDnsName::NetToHost(void)
{
	#if defined(_E_LITTLE)

	UINT uIndex;

	for( CDnsLabel const *p = GetHead(uIndex); p; p = GetNext(uIndex) ) {

		((CDnsLabel *) p)->NetToHost();
	}

	#endif
}

void CDnsName::HostToNet(void)
{
	#if defined(_E_LITTLE)

	UINT uIndex;

	for( CDnsLabel const *p = GetHead(uIndex); p; p = GetNext(uIndex) ) {

		((CDnsLabel *) p)->HostToNet();
	}

	#endif
}

// Attributes

UINT CDnsName::GetCount(void) const
{
	UINT uCount = 0;

	UINT uIndex;

	for( CDnsLabel const *p = GetHead(uIndex); p; p = GetNext(uIndex) ) {

		uCount++;
	}

	return uCount;
}

UINT CDnsName::GetSize(void) const
{
	UINT uIndex;

	UINT uSize = 0;

	for( CDnsLabel const *p = GetHead(uIndex); p; p = GetNext(uIndex) ) {

		uSize += p->GetSize();
	}

	return uSize;
}

UINT CDnsName::GetFree(void) const
{
	UINT uSize = GetSize();

	return uSize < DNS_MAX_NAME ? DNS_MAX_NAME - uSize : 0;
}

BOOL CDnsName::HasPointers(void) const
{
	UINT uIndex;

	for( CDnsLabel const *p = GetHead(uIndex); p; p = GetNext(uIndex) ) {

		if( p->IsPointer() ) {

			return TRUE;
		}
	}

	return FALSE;
}

// Operations

void CDnsName::Init(void)
{
	((CDnsLabel &) m_Data[0]).SetNull();
}

void CDnsName::Empty(void)
{
	Init();
}

BOOL CDnsName::Append(PCTXT pLabel)
{
	// TODO -- Compression

	UINT uSize  = GetSize();

	UINT uIndex = uSize - 1;

	UINT uFree  = DNS_MAX_NAME - uSize - 1;

	UINT uLen   = strlen(pLabel);

	if( uLen < uFree ) {

		while( pLabel ) {

			PCTXT pText = pLabel;

			UINT  uThis = uLen;

			PCTXT pFind = strchr(pLabel, '.');

			if( pFind ) {

				uThis  = pFind - pLabel;

				pLabel = pFind + 1;

				uLen  -= (uThis + 1);
			}
			else {
				pLabel = NULL;
			}

			CDnsLabel &Label = (CDnsLabel &) m_Data[uIndex];

			Label.SetText(pText, uThis);

			uIndex += Label.GetSize();
		}

		((CDnsLabel &) m_Data[uIndex]).SetNull();

		return TRUE;
	}

	return FALSE;
}

UINT CDnsName::FindSize(PCTXT pLabel) const
{
	UINT uSize = 1;

	if( pLabel ) {

		uSize += strlen(pLabel);

		uSize += 1;
	}

	return uSize;
}

// Enum

CDnsLabel const * CDnsName::GetHead(UINT &uIndex) const
{
	uIndex = 0;

	return &(CDnsLabel &) m_Data[uIndex];
}

CDnsLabel const * CDnsName::GetNext(UINT &uIndex) const
{
	CDnsLabel &Label = (CDnsLabel &) m_Data[uIndex];

	if( !Label.IsNull() && !Label.IsPointer() ) {

		uIndex += Label.GetSize();

		return &(CDnsLabel &) m_Data[uIndex];
	}

	return NULL;
}

CDnsLabel const * CDnsName::GetTail(UINT &uIndex) const
{
	CDnsLabel const *p = GetHead(uIndex);

	while( !p->IsNull() && !p->IsPointer() ) {

		p = GetNext(uIndex);
	}

	return p;
}

// Operators

CDnsName::operator PCBYTE (void) const
{
	return m_Data;
}

//////////////////////////////////////////////////////////////////////////
//
// DNS Question Footer Wrapper
//

void CDnsQFooter::NetToHost(void)
{
	#if defined(_E_LITTLE)

	m_Type  = ::NetToHost(m_Type);
	m_Class = ::NetToHost(m_Class);

	#endif
}

void CDnsQFooter::HostToNet(void)
{
	#if defined(_E_LITTLE)

	m_Type  = ::HostToNet(m_Type);
	m_Class = ::HostToNet(m_Class);

	#endif
}

//////////////////////////////////////////////////////////////////////////
//
// DNS Question Wrapper
//

// Conversion

void CDnsQuestion::NetToHost(void)
{
	#if defined(_E_LITTLE)

	GetName()->NetToHost();

	GetFoot()->NetToHost();

	#endif
}

void CDnsQuestion::HostToNet(void)
{
	#if defined(_E_LITTLE)

	GetName()->HostToNet();

	GetFoot()->HostToNet();

	#endif
}

// Attributes

CDnsName * CDnsQuestion::GetName(void) const
{
	return (CDnsName *) m_Data;
}

CDnsQFooter * CDnsQuestion::GetFoot(void) const
{
	return (CDnsQFooter *) &m_Data[GetName()->GetSize()];
}

UINT CDnsQuestion::GetSize(void) const
{
	return GetName()->GetSize() + sizeof(DNS_QFOOT);
}

// Operations

UINT CDnsQuestion::FindSize(PCTXT pText) const
{
	return GetName()->FindSize(pText) + sizeof(DNS_QFOOT);
}

void CDnsQuestion::Init(PCTXT pText, WORD wType, WORD wClass)
{
	CDnsName *pName = GetName();

	pName->Init();

	pName->Append(pText);

	CDnsQFooter *pFoot = GetFoot();

	pFoot->m_Type  = typeA;

	pFoot->m_Class = classIN;
}

//////////////////////////////////////////////////////////////////////////
//
// DNS Resource Record Footer Wrapper
//

void CDnsRRFooter::NetToHost(void)
{
	#if defined(_E_LITTLE)

	m_Type  = ::NetToHost(m_Type);
	m_Class = ::NetToHost(m_Class);
	m_TTL   = ::NetToHost(m_TTL);
	m_RDLen = ::NetToHost(m_RDLen);

	if( m_Type == typeA && m_Class == classIN ) {

		DWORD &Addr = (DWORD &) m_RData[0];

		Addr = ::NetToHost(Addr);
	}

	#endif
}

void CDnsRRFooter::HostToNet(void)
{
	#if defined(_E_LITTLE)

	m_Type  = ::HostToNet(m_Type);
	m_Class = ::HostToNet(m_Class);
	m_TTL   = ::HostToNet(m_TTL);
	m_RDLen = ::HostToNet(m_RDLen);

	if( m_Type == typeA && m_Class == classIN ) {

		DWORD &Addr = (DWORD &) m_RData[0];

		Addr = ::HostToNet(Addr);
	}

	#endif
}

//////////////////////////////////////////////////////////////////////////
//
// DNS Resource Record Wrapper
//

// Conversion

void CDnsRecord::NetToHost(void)
{
	#if defined(_E_LITTLE)

	GetName()->NetToHost();

	GetFoot()->NetToHost();

	#endif
}

void CDnsRecord::HostToNet(void)
{
	#if defined(_E_LITTLE)

	GetName()->HostToNet();

	GetFoot()->HostToNet();

	#endif
}

// Operations

UINT CDnsRecord::GetSize(void) const
{
	UINT uSize = 0;

	uSize += GetName()->GetSize();

	uSize += sizeof(DNS_RRFOOT) - 4;

	uSize += GetFoot()->m_RDLen;

	return uSize;
}

WORD CDnsRecord::GetType(void) const
{
	return GetFoot()->m_Type;
}

WORD CDnsRecord::GetClass(void) const
{
	return GetFoot()->m_Class;
}

DWORD CDnsRecord::GetTTL(void) const
{
	return GetFoot()->m_TTL;
}

CDnsName * CDnsRecord::GetName(void) const
{
	return (CDnsName *) m_Data;
}

CDnsRRFooter * CDnsRecord::GetFoot(void) const
{
	return (CDnsRRFooter *) &m_Data[GetName()->GetSize()];
}

DWORD CDnsRecord::GetAddr(void)	const
{
	return (DWORD &) GetFoot()->m_RData[0];
}

// End of File
