
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Entry Points
//

clink void AeonCmMain(void)
{
	extern IUnknown * Create_AppObject(PCSTR pName);

	piob->RegisterInstantiator("c3-app", Create_AppObject);
	}

clink void AeonCmExit(void)
{
	piob->RevokeInstantiator("c3-app");
	}

// End of File
