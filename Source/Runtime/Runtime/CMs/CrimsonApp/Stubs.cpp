
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Acceptable Stubs
//

void RackSetError(UINT uError)
{
}

void PersistGet(BYTE bSlot, PBYTE pData, UINT uSize)
{
	UINT uLump = Mem(Module1) - Mem(Module0);

	UINT uAddr = Mem(Module0) + bSlot * uLump;

	FRAMGetData(WORD(uAddr), pData, uSize);
}

void PersistPut(BYTE bSlot, PBYTE pData, UINT uSize)
{
	UINT uLump = Mem(Module1) - Mem(Module0);

	UINT uAddr = Mem(Module0) + bSlot * uLump;

	FRAMPutData(WORD(uAddr), pData, uSize);
}

//////////////////////////////////////////////////////////////////////////
//
// Unacceptable Stubs !!!
//

// Other Hooks

void SystemTimeChange(void)
{
	}

UINT AddrGetCount(BOOL fSample)
{
	return 0;
	}

unsigned int AppGetBootVersion(void)
{
	return 10;
	}

// End of File
