
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AppObject_HPP

#define INCLUDE_AppObject_HPP

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

class CAppObject : public ICrimsonApp,
		   public IClientProcess,
		   public IDiagProvider
{
	public:
		// Constructor
		CAppObject(void);

		// Destructor
		~CAppObject(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICrimsonApp
		void METHOD BindPxe(ICrimsonPxe *pPxe);
		void METHOD SetWebLink(CString const &Link);
		BOOL METHOD GetWebLink(CString &Link);
		BOOL METHOD LoadConfig(CString &Error);
		BOOL METHOD InitApp(void);
		BOOL METHOD HaltApp(void);
		BOOL METHOD TermApp(void);
		BOOL METHOD IsRunningControl(void);
		BOOL METHOD GetData(DWORD &Data, DWORD Ref);
		BOOL METHOD StratonCall(PCBYTE pIn, PBYTE pOut);
		BOOL METHOD RackTunnel(BYTE bDrop, PBYTE pData);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Data Members
		ULONG		 m_uRefs;
		CString		 m_Link;
		UINT		 m_uProv;
		ISemaphore     * m_pTms;
		CTaskList        m_List;
		CArray <HTASK>   m_Task;

		// Diagnostics
		BOOL DiagRegister(void);
		BOOL DiagRevoke(void);
		UINT DiagStop(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagStart(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagCycle(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagRestart(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Task Management
		void GetTaskList(CTaskList &List);
		void CreateTasks(void);
		void InitTasks(void);
		void StartTasks(void);
		void StopTasks(void);
		void TermTasks(void);
		void DeleteTasks(void);
		BOOL WaitTransition(void);

		// Implementation
		void FindObjects(void);
		void FreeObjects(void);
	};

// End of File

#endif
