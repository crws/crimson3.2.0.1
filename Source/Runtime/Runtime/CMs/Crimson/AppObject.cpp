
#include "Intern.hpp"

#include "AppObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Instantiator

clink IUnknown * Create_AppObject(PCSTR pName)
{
	return (IClientProcess *) New CAppObject;
	}

// Constructor

CAppObject::CAppObject(void)
{
	StdSetRef();

	DiagRegister();
	}

// Destructor

CAppObject::~CAppObject(void)
{
	DiagRevoke();
	}

// IUnknown

HRESULT CAppObject::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CAppObject::AddRef(void)
{
	StdAddRef();
	}

ULONG CAppObject::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CAppObject::TaskInit(UINT uTask)
{
	ShimInit();

	return ControlInit();
	}

INT CAppObject::TaskExec(UINT uTask)
{
	return ControlExec();
	}

void CAppObject::TaskStop(UINT uTask)
{
	ControlStop();
	}

void CAppObject::TaskTerm(UINT uTask)
{
	ControlTerm();
	}

// IDiagProvider

UINT CAppObject::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagTest(pOut, pCmd);
		}

	return 0;
	}

// Diagnostics

BOOL CAppObject::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "app");

		pDiag->RegisterCommand(m_uProv, 1, "test");

		pDiag->Release();

		return TRUE;
		}

	return FALSE;
	}

BOOL CAppObject::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return TRUE;
		}

	return FALSE;
	}

UINT CAppObject::DiagTest(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->Print("Welcome to the app!\n");

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

// End of File
