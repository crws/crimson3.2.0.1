
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Platform Objects
//

// Object Pointers

global IDatabase        * g_pDbase       = NULL;

global IEvStg	        * g_pEvStg       = NULL;

global IFirmwareProgram * g_pBootProgram = NULL;

global IFirmwareProps   * g_pFirmProps   = NULL;

global IFirmwareProgram * g_pFirmPend    = NULL;

global ICrimsonIdentity * g_pIdentity    = NULL;

global IPersist         * g_pPersist     = NULL;

global IProm            * g_pProm        = NULL;

global ILicense         * g_pLicense     = NULL;

// Object Creation

void CreateTargetObjects(void)
{
	#if !defined(AEON_PLAT_LINUX)

	g_pBootProgram = Create_NandBootProgram       (  0,   8);

	g_pFirmProps   = Create_NandFirmwareProps     (  8, 128);

	g_pFirmPend    = Create_NandFirmwareProgram   (128, 248);

	g_pIdentity    = Create_NandFlashIdentity     (248, 256);

	g_pEvStg       = Create_NandFlashEventStorage (256, 384);

	g_pPersist     = Create_NandFlashPersist      (384, 512);

	g_pDbase       = Create_NandFlashDatabase     (512, 2048, 20480);

	#else

	g_pBootProgram = Create_NandBootProgram       ( 0,  0 + 8);

	g_pFirmProps   = Create_NandFirmwareProps     ( 8,  8 + 8);

	g_pFirmPend    = Create_NandFirmwareProgram   (16, 16 + 8);

	g_pIdentity    = Create_NandFlashIdentity     (24, 24 + 8);

	g_pEvStg       = Create_NandFlashEventStorage (32, 32 + 32);

	g_pPersist     = Create_NandFlashPersist      (64, 64 + 32);

	g_pDbase       = Create_NandFlashDatabase     (96, 512, 20480);

	#endif

	g_pLicense     = Create_License               ();

	g_pIdentity->Init();
	}

// End of File
