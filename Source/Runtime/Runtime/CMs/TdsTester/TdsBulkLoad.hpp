//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TDS_TdsBulkLoad_HPP

#define INCLUDE_TDS_TdsBulkLoad_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TdsPacket.hpp"


//////////////////////////////////////////////////////////////////////////
//
// TDS Bulk Load Packet
//

class CTdsBulkLoad : public CTdsPacket
{
	public:
		// Constructors
		CTdsBulkLoad(void);
		CTdsBulkLoad(UINT uBufferSize);
		CTdsBulkLoad(UINT uBufferSize, bool fAddHeader);
		~CTdsBulkLoad(void);

		// Operations
		void AddColumnCount(USHORT uColumnCount);
		void AddColumn(PCSTR pColumnName, TdsDataType eColumnType, USHORT uColumnSize, BOOL fNullable = TRUE);
		void StartRow();
		void AddFieldInt1(BYTE bValue);
		void AddFieldInt2(SHORT sValue);
		void AddFieldInt4(LONG lValue);
		void AddFieldInt8(LONGLONG lValue);
		void AddFieldFloat4(FLOAT gValue);
		void AddFieldFloat8(DOUBLE dValue);
		void AddFieldDateTime(ULONGLONG lValue);
		void AddFieldVarchar(PCSTR pValue);
		void AddFieldVarchar(PCSTR pValue, UINT uSize);
		void EndPacket(bool fEnd);
	};

// End of File

#endif
