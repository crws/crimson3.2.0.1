
#include "Intern.hpp"

#include "TdsPacket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// TDS Packet
//

// Packet ID Count

UINT  CTdsPacket::m_uPacketID = 0;

// Constructors

CTdsPacket::CTdsPacket(void) : m_Data(1024)
{
	m_Type = typeNull;

	m_Spid = 0;

	m_Data.AddULong(0);

	m_Data.AddULong(0);
	}

CTdsPacket::CTdsPacket(TdsType Type) : m_Data(1024)
{
	m_Type = Type;

	m_Spid = 0;

	m_Data.AddULong(0);

	m_Data.AddULong(0);
	}

CTdsPacket::CTdsPacket(TdsType Type, UINT BufferSize) : m_Data(BufferSize)
{
	m_Type = Type;

	m_Spid = 0;

	m_Data.AddULong(0);

	m_Data.AddULong(0);
	}

CTdsPacket::CTdsPacket(TdsType Type, UINT BufferSize, bool fAddHeader) : m_Data(BufferSize)
{
	m_Type = Type;

	m_Spid = 0;

	if(fAddHeader) {

		m_Data.AddULong(0);

		m_Data.AddULong(0);
		}
	}


// Attributes

TdsType CTdsPacket::GetType(void) const
{
	return m_Type;
	}

PCBYTE CTdsPacket::GetData(void) const
{
	return m_Data.GetData();
	}

PBYTE CTdsPacket::GetDataMod(void) const
{
	return m_Data.GetData();
	}

UINT CTdsPacket::GetSize(void) const
{
	return m_Data.GetSize();
	}

// Get Data

BYTE CTdsPacket::GetByte(UINT &uPos)
{
	return m_Data.GetByte(uPos);
	}

FLOAT CTdsPacket::GetFloat(UINT &uPos)
{
	return m_Data.GetFloat(uPos);
	}

DOUBLE CTdsPacket::GetDouble(UINT &uPos)
{
	return m_Data.GetDouble(uPos);
	}

USHORT CTdsPacket::GetUShortReverse(UINT &uPos)
{
	return m_Data.GetUShortReverse(uPos);
	}

ULONG CTdsPacket::GetULongReverse(UINT &uPos)
{
	return m_Data.GetULongReverse(uPos);
	}

ULONGLONG CTdsPacket::GetULongLongReverse(UINT &uPos)
{
	return m_Data.GetULongLongReverse(uPos);
	}

DOUBLE CTdsPacket::GetDoubleReverse(UINT &uPos)
{
	return m_Data.GetDoubleReverse(uPos);
	}

bool CTdsPacket::GetByteCharacterData(CHAR* pOutputString, USHORT uLength, UINT &uPos)
{
	return m_Data.GetByteCharacterData(pOutputString, uLength, uPos);

	}

bool CTdsPacket::GetReverseCharacterData(CHAR* pOutputString, USHORT uLength, UINT &uPos)
{
	return m_Data.GetReverseCharacterData(pOutputString, uLength, uPos);
	}

// Parsing

int CTdsPacket::Parse(PCBYTE pData, UINT uSize)
{
	if( m_Data.GetSize() == 8 ) {

		m_Data.Clear();
		}

	if( uSize > 0 ) {

		m_Data.AddData(pData, uSize);

		UINT p2    = 2;

		UINT uNeed = m_Data.GetUShort(p2);

		if( m_Data.GetSize() >= uNeed ) {

			if( m_Data.GetSize() == uNeed ) {

				UINT p0 = 0;

				UINT p4 = 4;

				m_Type = TdsType(m_Data.GetByte(p0));

				m_Spid = m_Data.GetUShort(p4);

				// Success.

				return +1;
				}

			// Failure.

			return -1;
			}
		}

	// More data needed.

	return 0;
	}

// Operations

void CTdsPacket::SetSpid(WORD Spid)
{
	m_Spid = Spid;
	}

void CTdsPacket::EndPacket(void)
{
	PBYTE pData = m_Data.GetData();

	UINT  uSize = m_Data.GetSize();

	pData[0] = BYTE(m_Type);
	pData[1] = BYTE(statusEnd);
	pData[2] = HIBYTE(uSize);
	pData[3] = LOBYTE(uSize);
	pData[4] = HIBYTE(m_Spid);
	pData[5] = LOBYTE(m_Spid);
	pData[6] = 1;
	pData[7] = 0;
	}

void CTdsPacket::Dump(void)
{
	m_Data.Dump();
	}

void CTdsPacket::DumpASCII(void)
{
	m_Data.DumpASCII();
	}

UINT CTdsPacket::GetPacketID(void)
{
	m_uPacketID = (m_uPacketID + 1) % 256;

	return m_uPacketID;
	}

// Packet Building

void CTdsPacket::AddByte(BYTE bData)
{
	m_Data.AddByte(bData);
	}

void CTdsPacket::AddUShort(USHORT uData)
{
	m_Data.AddUShort(uData);
	}

void CTdsPacket::AddULong(ULONG uData)
{
	m_Data.AddULong(uData);
	}

void CTdsPacket::AddULongLong(ULONGLONG uData)
{
	m_Data.AddULongLong(uData);
	}

void CTdsPacket::AddUShortReverse(USHORT uData)
{
	m_Data.AddUShortReverse(uData);
	}

void CTdsPacket::AddULongReverse(ULONG uData)
{
	m_Data.AddULongReverse(uData);
	}

void CTdsPacket::AddULongLongReverse(ULONGLONG uData)
{
	m_Data.AddULongLongReverse(uData);
	}

void CTdsPacket::AddFloat(FLOAT uData)
{
	m_Data.AddData((PCBYTE)&uData, sizeof(FLOAT));
	}

void CTdsPacket::AddFloatReverse(FLOAT uData)
{
	m_Data.AddFloatReverse(uData);
	}

void CTdsPacket::AddDouble(DOUBLE uData)
{
	m_Data.AddData((PCBYTE)&uData, sizeof(DOUBLE));
	}

void CTdsPacket::AddDoubleReverse(DOUBLE uData)
{
	m_Data.AddDoubleReverse(uData);
	}

void CTdsPacket::AddText(PCSTR pText)
{
	m_Data.AddText (pText);
	}

void CTdsPacket::AddReverseCharText(PCSTR pText)
{
	m_Data.AddReverseCharText(pText);
	}

void CTdsPacket::AddTextNoTerminator(PCSTR pText)
{
	m_Data.AddData((PCBYTE)pText, strlen(pText));
	}

void CTdsPacket::AddTextNoTerminator(PCSTR pText, UINT uSize)
{
	m_Data.AddData((PCBYTE)pText, uSize);
	}

void CTdsPacket::RemoveHead(UINT uSize)
{
	m_Data.RemoveHead(uSize);
	}


// End of File
