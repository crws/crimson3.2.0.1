//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "Intern.hpp"

#include "TdsLogin.hpp"


//////////////////////////////////////////////////////////////////////////
//
// TDS Login Packet
//

// Constructors

CTdsLogin::CTdsLogin(void) : CTdsPacket(typeLogin7)
{
	}


CTdsLogin::~CTdsLogin(void)
{
	}


// Operations

void CTdsLogin::Create(PCSTR pHostName, PCSTR pUserName, PCSTR pPassword, PCSTR pClientAppName, PCSTR pDatabaseName, ULONG uPacketSize)
{
	BYTE bNextStringStart;

	AddLength(0x5E + (strlen(pHostName) + strlen(pUserName) + strlen(pPassword) + strlen(pClientAppName) + strlen(pDatabaseName)) * 2);

	AddVersion(0x02,0x00,0x09,0x72);

	AddPacketSize(uPacketSize);

	AddClientProgVer(7);

	AddClientPID(1);

	AddConnectionID(0);

	AddOptionFlags1(224);
	
	AddOptionFlags2(3);

	AddTypeFlags(0);

	AddOptionFlags3(0);

	AddClientTimeZone(480);

	AddClientLCID(1033);

	bNextStringStart = 0x5E;
	AddIbHostName(bNextStringStart);
	AddCchHostName(USHORT(strlen(pHostName)));
	bNextStringStart += BYTE(strlen(pHostName) * 2);

	AddIbUserName(bNextStringStart);
	AddCchUserName(USHORT(strlen(pUserName)));
	bNextStringStart += BYTE(strlen(pUserName) * 2);

	AddIbPassword(bNextStringStart);
	AddCchPassword(USHORT(strlen(pPassword)));
	bNextStringStart += BYTE(strlen(pPassword) * 2);

	AddIbAppName(bNextStringStart);
	AddCchAppName(USHORT(strlen(pClientAppName)));
	bNextStringStart += BYTE(strlen(pClientAppName) * 2);

	AddIbServerName(bNextStringStart);
	AddCchServerName(0);

	AddIbUnused(bNextStringStart);
	AddCchUnused(0);

	AddIbCltIntName(bNextStringStart);
	AddCchCltInitName(0);

	AddIbLanguage(bNextStringStart);
	AddCchLanguage(0);

	AddIbDatabase(bNextStringStart);
	AddCchDatabase(USHORT(strlen(pDatabaseName)));
	bNextStringStart += BYTE(strlen(pDatabaseName) * 2);

	CTdsBytes TestBytes(16);
	TestBytes.AddByte(0x00);
	TestBytes.AddByte(0x50);
	TestBytes.AddByte(0x8B);
	TestBytes.AddByte(0xE2);
	TestBytes.AddByte(0xB7);
	TestBytes.AddByte(0x8F);
	AddClientID(&TestBytes);
		
	AddIbSSPI(bNextStringStart);
	AddCchSSPI(0);

	AddIbAttachDBFile(bNextStringStart);
	AddCchAttachDBFile(0);

	AddIbChangePassword(bNextStringStart);
	AddCchChangePassword(0);

	AddCbSSPILong(0);

	EndPacket(pHostName, pUserName, pPassword, pClientAppName, pDatabaseName);

	}


void CTdsLogin::AddLength(ULONG uLength)
{
	AddULongReverse(uLength);

	}

void CTdsLogin::AddVersion(BYTE bVersion1, BYTE bVersion2, BYTE bVersion3, BYTE bVersion4)
{
	AddByte(bVersion1);
	AddByte(bVersion2);
	AddByte(bVersion3);
	AddByte(bVersion4);

	}

void CTdsLogin::AddPacketSize(ULONG uPacketSize)
{
	AddULongReverse(uPacketSize);

	}

void CTdsLogin::AddClientProgVer(BYTE bClientProgVer)
{
	AddByte(0);
	AddByte(0);
	AddByte(0);
	AddByte((BYTE)bClientProgVer);

	}

void CTdsLogin::AddClientPID(ULONG uClientPID)
{
	AddULongReverse(uClientPID);
	}

void CTdsLogin::AddConnectionID(ULONG uConnectionID)
{
	AddULongReverse(uConnectionID);
	}

void CTdsLogin::AddOptionFlags1(BYTE bOptionFlags1)
{
	AddByte(bOptionFlags1);
	}

void CTdsLogin::AddOptionFlags2(BYTE bOptionFlags2)
{
	AddByte(bOptionFlags2);
	}

void CTdsLogin::AddOptionFlags3(BYTE bOptionFlags3)
{
	AddByte(bOptionFlags3);
	}

void CTdsLogin::AddTypeFlags(BYTE bTypeFlags)
{
	AddByte(bTypeFlags);
	}

void CTdsLogin::AddClientTimeZone(ULONG uClientTimeZone)
{
	AddULongReverse(uClientTimeZone);
	}

void CTdsLogin::AddClientLCID(ULONG uClientLCID)
{
	AddULongReverse(uClientLCID);
	}

void CTdsLogin::AddIbHostName(USHORT uIbHostName)
{
	AddUShortReverse(uIbHostName);
	}

void CTdsLogin::AddCchHostName(USHORT uCchHostName)
{
	AddUShortReverse(uCchHostName);
	}
	
void CTdsLogin::AddIbUserName(USHORT uIbUserName)
{
	AddUShortReverse(uIbUserName);
	}

void CTdsLogin::AddCchUserName(USHORT uCchUserName)
{
	AddUShortReverse(uCchUserName);
	}

void CTdsLogin::AddIbPassword(USHORT uIbPassword)
{
	AddUShortReverse(uIbPassword);
	}

void CTdsLogin::AddCchPassword(USHORT uCchPassword)
{
	AddUShortReverse(uCchPassword);
	}

void CTdsLogin::AddIbAppName(USHORT uIbAppName)
{
	AddUShortReverse(uIbAppName);
	}

void CTdsLogin::AddCchAppName(USHORT uCchAppName)
{
	AddUShortReverse(uCchAppName);
	}

void CTdsLogin::AddIbServerName(USHORT uIbServerName)
{
	AddUShortReverse(uIbServerName);
	}

void CTdsLogin::AddCchServerName(USHORT uCchServerName)
{
	AddUShortReverse(uCchServerName);
	}

void CTdsLogin::AddIbUnused(USHORT uIbUnused)
{
	AddUShortReverse(uIbUnused);
	}

void CTdsLogin::AddCchUnused(USHORT uCchUnused)
{
	AddUShortReverse(uCchUnused);
	}

void CTdsLogin::AddIbCltIntName(USHORT uIbCltIntName)
{
	AddUShortReverse(uIbCltIntName);
	}

void CTdsLogin::AddCchCltInitName(USHORT uCchCltIntName)
{
	AddUShortReverse(uCchCltIntName);
	}

void CTdsLogin::AddIbLanguage(USHORT uIbLanguage)
{
	AddUShortReverse(uIbLanguage);
	}

void CTdsLogin::AddCchLanguage(USHORT uCchLanguage)
{
	AddUShortReverse(uCchLanguage);
	}

void CTdsLogin::AddIbDatabase(USHORT uIbDatabase)
{
	AddUShortReverse(uIbDatabase);
	}

void CTdsLogin::AddCchDatabase(USHORT uCchDatabase)
{
	AddUShortReverse(uCchDatabase);
	}

void CTdsLogin::AddClientID(CTdsBytes* ClientID)
{
	m_Data.AddData(ClientID->GetData(), ClientID->GetSize());
	}

void CTdsLogin::AddIbSSPI(USHORT uIbSSPI)
{
	AddUShortReverse(uIbSSPI);
	}

void CTdsLogin::AddCchSSPI(USHORT uCchSSPI)
{
	AddUShortReverse(uCchSSPI);
	}

void CTdsLogin::AddIbAttachDBFile(USHORT uIbAttachDBFile)
{
	AddUShortReverse(uIbAttachDBFile);
	}

void CTdsLogin::AddCchAttachDBFile(USHORT uCchAttachDBFile)
{
	AddUShortReverse(uCchAttachDBFile);
	}

void CTdsLogin::AddIbChangePassword(USHORT uIbChangePassword)
{
	AddUShortReverse(uIbChangePassword);
	}

void CTdsLogin::AddCchChangePassword(USHORT uCchChangePassword)
{
	AddUShortReverse(uCchChangePassword);
	}


void CTdsLogin::AddCbSSPILong(LONG CbSSPILong)
{
	m_Data.AddData((PCBYTE)&CbSSPILong, sizeof(LONG));
	}


void CTdsLogin::AddString(PCSTR pString)
{
	for (ULONG uIndex = 0; uIndex < strlen(pString); uIndex++)
	{
		m_Data.AddByte(pString[uIndex]);
		m_Data.AddByte(0);
		}
	}


void CTdsLogin::AddPasswordString(PCSTR pString)
{
	// Each byte of the password string must swap the 4 low-order bits with the
	//	4 high-order bits and then XOR with 0xA5 (according to the spec document).
	for (ULONG uIndex = 0; uIndex < strlen(pString); uIndex++)
	{
		BYTE bNextChar = (pString[uIndex] << 4) + (pString[uIndex] >> 4);
		bNextChar = bNextChar ^ 0xA5;
		m_Data.AddByte(bNextChar);

		m_Data.AddByte(0 ^ 0xA5);
		}
	}


void CTdsLogin::EndPacket(PCSTR pHostName, PCSTR pUserName, PCSTR pPassword, PCSTR pClientAppName, PCSTR pDatabaseName)
{
	AddString(pHostName);
	AddString(pUserName);
	AddPasswordString(pPassword);
	AddString(pClientAppName);
	AddString(pDatabaseName);

	CTdsPacket::EndPacket();
	}


// End of File
