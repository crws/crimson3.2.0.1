
#include "Intern.hpp"

#include "AppObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "FakeSocket.hpp"

#include "TdsPacket.hpp"

#include "TdsPreLogin.hpp"

#include "TdsLogin.hpp"

#include "TdsResponse.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Instantiator

clink IUnknown * Create_AppObject(PCSTR pName)
{
	return (IClientProcess *) New CAppObject;
	}

// Constructor

CAppObject::CAppObject(void)
{
	StdSetRef();
	}

// Destructor

CAppObject::~CAppObject(void)
{
	}

// IUnknown

HRESULT CAppObject::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CAppObject::AddRef(void)
{
	StdAddRef();
	}

ULONG CAppObject::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CAppObject::TaskInit(UINT uTask)
{
	return TRUE;
	}

INT CAppObject::TaskExec(UINT uTask)
{
	AfxGetObject("tls", 0, ITlsLibrary, m_pTls);

	if( m_pTls ) {

		if( m_pTls->CreateClientContext(m_pClient) ) {

			if( OpenCmdSocket() ) {

				if( ConnectToSql() ) {

					AfxTrace("Connected\n");
					}

				m_pCmdSock->Release();
				}

			m_pClient->Release();
			}

		m_pTls->Release();
		}

	return 0;
	}

void CAppObject::TaskStop(UINT uTask)
{
	}

void CAppObject::TaskTerm(UINT uTask)
{
	}

// Implementation

bool CAppObject::OpenCmdSocket(void)
{
	AfxNewObject("sock-tcp", ISocket, m_pCmdSock);

	m_pCmdSock->Connect(CIpAddr("192.168.1.5"), 1433);

	SetTimer(5000);

	while( GetTimer() ) {

		UINT Phase;

		m_pCmdSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {

			return true;
			}

		if( Phase == PHASE_CLOSING ) {

			m_pCmdSock->Close();
			}

		if( Phase == PHASE_ERROR ) {

			break;
			}

		Sleep(10);
		}

	m_pCmdSock->Release();

	m_pCmdSock = NULL;

	return false;
	}

bool CAppObject::SendCommand(CTdsPacket &clsSentPacket)
{
	UINT  uSize = clsSentPacket.GetSize();

	PBYTE pData = clsSentPacket.GetDataMod();

	while( uSize ) {

		UINT uCopy    = min(1280, uSize);

		UINT uRetries = 4;

		while( !(m_pCmdSock->Send(pData, uCopy) == S_OK) ) {

			if( !--uRetries ) {

				return false;
				}

			Sleep(100);

			uCopy = min(1280, uSize);
			}

		uSize -= uCopy;

		pData += uCopy;
		}

	return true;
	}

bool CAppObject::SendCommandAndGetReply(CTdsPacket &clsSentPacket, CTdsPacket &clsReceivedPacket)
{
	if( SendCommand(clsSentPacket) ) {

		CBuffer *pBuffer = NULL;

		for( UINT i = 0; i < (5000 / 100); i++ ) {

			if(m_pCmdSock->Recv(pBuffer) == S_OK ) {

				int n = clsReceivedPacket.Parse(pBuffer->GetData(), pBuffer->GetSize());

				if( n > 0 ) {

					pBuffer->Release();

					return true;
					}

				if( n < 0 ) {

					pBuffer->Release();

					return false;
					}

				continue;
				}

			Sleep(100);
			}

		return false;
		}

	return false;
	}

bool CAppObject::ConnectToSql(void)
{
	CTdsPreLogin SendMsg, RecvMsg;

	SendMsg.Create("", 0x12345678, TRUE);

	if( SendCommandAndGetReply(SendMsg, RecvMsg) ) {

		BYTE bEncrypt = RecvMsg.GetEncryption();

		if( bEncrypt == 1 || bEncrypt == 3 ) {

			if( !SwitchToTls() ) {

				return false;
				}

			AfxTrace("Switched to TLS\n");
			}

		CTdsLogin    SendMsg;

		CTdsResponse RecvMsg;

		SendMsg.Create(	"Crimson3Host", 
				"sa",
				"password",
				"Crimson3",
				"DevTest",
				8196
				);

		if( SendCommandAndGetReply(SendMsg, RecvMsg) ) {

			if( !RecvMsg.HaveErrorFromServer() ) {
			
				return true;
				}
			}
		}

	return false;
	}

bool CAppObject::SwitchToTls(void)
{
	// TDS is a little strange in that the initial TLS negotiation is
	// carried out by embedding the handshake messages in TDS prelogin
	// packets. To handle this, we create a fake child socket that will
	// capture the TLS transmissions and let us feed respones back. Once
	// the TLS socket is happy, we switch its to use our TDS socket as
	// its child socket and let everything run in TLS from there.

	CFakeSocket *pFake = New CFakeSocket;

	ISocket	    *pSock = m_pClient->CreateSocket(pFake, NULL, 0);

	pSock->Connect(CIpAddr(), 0);

	for(;;) {

		UINT Phase = 0;

		for( UINT n = 0; n < 10; n++ ) {

			pSock->GetPhase(Phase);
			}

		if( Phase == PHASE_OPEN ) {

			pSock->SetOption(OPT_SET_CHILD, UINT(m_pCmdSock));

			pFake->Release();

			m_pCmdSock = pSock;

			return true;
			}

		if( Phase == PHASE_OPENING ) {

			PCBYTE pData;

			UINT   uData;

			// We should have data to send, so grab it.

			if( pFake->GetTxData(pData, uData) ) {

				AfxTrace("SEND:\n");

				AfxDump(pData, uData);

				AfxTrace("\n");

				// Create a prelogin packet with a payload.

				CTdsPreLogin SendMsg, RecvMsg;

				SendMsg.AddPayload(pData, uData);

				SendMsg.EndPacket();

				// Exchange packets with the server.

				if( SendCommandAndGetReply(SendMsg, RecvMsg) ) {

					// Find the reply payload contents.

					pData = RecvMsg.GetPayloadData();

					uData = RecvMsg.GetPayloadSize();

					if( uData ) {

						// And pass them on to the TLS socket.

						AfxTrace("RECV:\n");

						AfxDump(pData, uData);

						AfxTrace("\n");

						pFake->SetRxData(pData, uData);

						continue;
						}
					}
				}
			}

		break;
		}

	pSock->Release();

	return false;
	}

// End of File
