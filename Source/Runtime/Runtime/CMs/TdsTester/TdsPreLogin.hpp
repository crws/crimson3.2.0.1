
//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TDS_TdsPreLogin_HPP

#define INCLUDE_TDS_TdsPreLogin_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TdsPacket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TDS Pre-Login Packet
//

class CTdsPreLogin : public CTdsPacket
{
	public:
		// Constructors
		CTdsPreLogin(void);

		// Attributes
		USHORT GetVersion(void) const;
		USHORT GetBuild(void) const;
		USHORT GetSubBuild(void) const;
		BYTE   GetEncryption(void) const;
		PCSTR  GetInstance(void) const;
		ULONG  GetTheadId(void) const;
		BYTE   GetMars(void) const;
		PCBYTE GetPayloadData(void) const;
		UINT   GetPayloadSize(void) const;

		// Parsing
		int Parse(PCBYTE pData, UINT uSize);

		// Operations
		void Empty(void);
		void Create(PCSTR pName, ULONG uThread, BOOL fEncrypt);
		void AddVersion(USHORT uVersion, USHORT uBuild, USHORT uSubBuild);
		void AddEncryption(BYTE bMode);
		void AddInstance(PCSTR pName);
		void AddThreadId(ULONG uThread);
		void AddMars(bool fEnable);
		void AddPayload(PCBYTE pData, UINT uSize);
		void EndPacket(void);

	protected:
		// Option
		struct COption
		{
			BYTE      m_bToken;
			CTdsBytes m_Data;
			};

		// Data Members
		COption m_Opt[32];
		UINT    m_uCount;
		PCBYTE  m_pPayload;
		UINT    m_uPayload;

		// Implementation
		COption const * FindOption(BYTE bToken) const;
	};

// End of File

#endif
