//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TDS_TdsSQLBatch_HPP

#define INCLUDE_TDS_TdsSQLBatch_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TdsPacket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TDS SQLBatch Packet
//

class CTdsSqlBatch : public CTdsPacket
{
	public:
		// Constructors
		CTdsSqlBatch(void);
		CTdsSqlBatch(UINT uMaxPacketSize);
		~CTdsSqlBatch(void);

		// Operations
		UINT Create(CTdsBytes& bCommand, UINT uMaxPacketSize);
		UINT Append(CTdsBytes& bCommand, UINT uDataStart, UINT uMaxPacketSize);
		void AddTotalLength(ULONG uLength);
		void AddHeaderLength(ULONG uLength);
		void AddHeaderType(USHORT bType);
		void AddTransactionDescriptor(BYTE bDescriptor);
		void AddOutstandingRequestCount(ULONG uCount);
		void AddSQLStatementText(CTdsBytes& bCommand, UINT uDataStart, UINT uMaxPacketSize);
		void EndPacket(void);

		// Data Members
		UINT m_uIndex;
	};


// End of File

#endif
