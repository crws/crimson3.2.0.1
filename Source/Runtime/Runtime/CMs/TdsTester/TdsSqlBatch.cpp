//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "Intern.hpp"

#include "TdsSQLBatch.hpp"

#define	MAX_TDS_PACKET_SIZE	8196

//////////////////////////////////////////////////////////////////////////
//
// TDS SQLBatch Packet
//

// Constructors

CTdsSqlBatch::CTdsSqlBatch(void) : CTdsPacket(typeSqlBatch, MAX_TDS_PACKET_SIZE)
{
	}

CTdsSqlBatch::CTdsSqlBatch(UINT uMaxPacketSize) : CTdsPacket(typeSqlBatch, uMaxPacketSize)
{
	}

CTdsSqlBatch::~CTdsSqlBatch(void)
{
	}


// Operations

UINT CTdsSqlBatch::Create(CTdsBytes& bCommand, UINT uMaxPacketSize)
{
	AddTotalLength(0x16);
	AddHeaderLength(0x12);
	AddHeaderType(0x02);
	AddTransactionDescriptor(0x00);
	AddOutstandingRequestCount(0x01);

	AddSQLStatementText(bCommand, 0, uMaxPacketSize);

	EndPacket();

	return m_uIndex;
	}

UINT CTdsSqlBatch::Append(CTdsBytes& bCommand, UINT uDataStart, UINT uMaxPacketSize)
{
	if(uDataStart == 0) {
		return Create(bCommand, uMaxPacketSize);
		}
	else {
		AddSQLStatementText(bCommand, uDataStart, uMaxPacketSize);

		EndPacket();

		return m_uIndex;
		}
	}

void CTdsSqlBatch::AddTotalLength(ULONG uLength)
{
	AddULongReverse(uLength);
	}

void CTdsSqlBatch::AddHeaderLength(ULONG uLength)
{
	AddULongReverse(uLength);
	}

void CTdsSqlBatch::AddHeaderType(USHORT uType)
{
	AddUShortReverse(uType);
	}

void CTdsSqlBatch::AddTransactionDescriptor(BYTE bDescriptor)
{
	AddByte(0);
	AddByte(0);
	AddByte(0);
	AddByte(0);
	AddByte(0);
	AddByte(0);
	AddByte(0);
	AddByte(bDescriptor);
	}

void CTdsSqlBatch::AddOutstandingRequestCount(ULONG uCount)
{
	AddULongReverse(uCount);
	}

void CTdsSqlBatch::AddSQLStatementText(CTdsBytes& bCommand, UINT uDataStart, UINT uMaxPacketSize)
{
	m_uIndex     = uDataStart;

	UINT uLength = bCommand.GetSize();

	while(m_uIndex < uLength) {

		if(m_Data.GetSize()+1 > uMaxPacketSize) {

			return;
			}
		AddByte(bCommand.GetByte(m_uIndex));
		}
	m_uIndex = NOTHING;
	}

void CTdsSqlBatch::EndPacket()
{
	PBYTE pData = m_Data.GetData();
	UINT  uSize = m_Data.GetSize();
	pData[0] = BYTE(m_Type);
	pData[1] = BYTE(m_uIndex == NOTHING);
	pData[2] = HIBYTE(uSize);
	pData[3] = LOBYTE(uSize);
	pData[4] = HIBYTE(m_Spid);
	pData[5] = LOBYTE(m_Spid);
	pData[6] = BYTE(GetPacketID());
	pData[7] = 0;
	}

// End of File
