//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TDS_TdsDataSet_HPP

#define INCLUDE_TDS_TdsDataSet_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TdsPacket.hpp"


//////////////////////////////////////////////////////////////////////////
//
// Column Information Structure
//

struct CColumnInfo {
	BYTE	ColumnType;
	USHORT	ColumnTypeSize;
	PSTR	ColumnName;
	PBYTE	RowData;
	};


//////////////////////////////////////////////////////////////////////////
//
// TDS DataSet (COLMETADATA) Packet
//

class CTdsDataSet : public CTdsPacket
{
	public:
		// Constructors
		CTdsDataSet(void);
		CTdsDataSet(UINT BufferSize);
		~CTdsDataSet(void);

		// Attributes
		UINT    GetNumColumns(void) const;
		BYTE    GetColumnType(UINT &uIndex) const;
		USHORT	GetColumnTypeSize(UINT &uIndex) const;
		PCSTR	GetColumnName(UINT &uIndex) const;
		UINT	GetNumRows(void) const;

		// Parsing
		int Parse(PCBYTE pData, UINT uSize);

		// Operations
		BYTE GetByteField(UINT uRowIndex, UINT uColIndex);
		SHORT GetShortField(UINT uRowIndex, UINT uColIndex);
		LONG GetLongField(UINT uRowIndex, UINT uColIndex);
		LONGLONG GetLongLongField(UINT uRowIndex, UINT uColIndex);
		FLOAT GetFloatField(UINT uRowIndex, UINT uColIndex);
		DOUBLE GetDoubleField(UINT uRowIndex, UINT uColIndex);
		PCSTR GetVarcharField(UINT uRowIndex, UINT uColIndex);

		// Type Helpers
		bool IsFixedSizeType(UINT uType);
		bool IsString(UINT uType);
		bool IsDateTime(UINT uType);
		bool IsWideString(UINT uType);
		bool HasCollationInfo(UINT uType);
		UINT GetFixedSize(UINT uType);
		UINT GetVarWidth(UINT uType);
		UINT GetVarTypeSize(UINT uType, UINT &uPos);
		UINT ParseDateTime(UINT uType, UINT uSize, UINT &uPos);


	protected:
		// Data Members
		USHORT		m_uNumColumns;
		CColumnInfo*	m_pColumns;
		USHORT		m_uNumRows;

	private:
		// Helper operations
		void FreeColumnInfo(void);
		void CreateColumnInfo(void);
		bool ReadColumnInfo(UINT &uPos);
		void CountRows(UINT &uPos);
		void CreateRowStorage(void);
		void ParseRows(UINT &uPos);
		void ParseOrdering(UINT &uPos);
	};

// End of File

#endif
