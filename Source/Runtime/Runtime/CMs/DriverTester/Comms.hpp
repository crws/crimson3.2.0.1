
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Comms_HPP

#define INCLUDE_Comms_HPP

//////////////////////////////////////////////////////////////////////////
//
// Data Headers
//

#include "Ascii.hpp"

#include "Crc16.hpp"

#include "DataPacker.hpp"

#include "Align.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Drivers
//

#include "Driver.hpp"

#include "CommsDriver.hpp"

#include "MasterDriver.hpp"

#include "SlaveDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Helpers
//

#include "Helper.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tester
//

#include "CommsTester.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Type Modifiers
//

#define	DEFMETH(t)		virtual t METHOD

#define MCALL			/**/

#define CODE_SEG		const

#define	CTEXT			static char CODE_SEG

////////////////////////////////////////////////////////////////////////
//
// Standard Macros
//

#define ATOI(s)			atoi(s)

#define ATOF(s)			atof(s) // !!!

#define RAND(s)			rand()

////////////////////////////////////////////////////////////////////////
//
// Trace Macros
//

#define	AfxTrace0(s)		AfxTrace(s)		

#define	AfxTrace1(s,a)		AfxTrace(s, a)

#define	AfxTrace2(s,a,b)	AfxTrace(s,a,b)

#define	AfxTrace3(s,a,b,c)	AfxTrace(s,a,b,c)

//////////////////////////////////////////////////////////////////////////
//
// Instantiator 
//

#define	Driver_Instantiate(Name)					\
									\
clink IUnknown * Create_##Name(PCSTR pName)				\
{									\
	return (IUnknown *) new Name;					\
	}								\

//////////////////////////////////////////////////////////////////////////
//
// Registration 
//

#define Driver_Register(Name)						\
									\
	BEGIN {								\
									\
	extern IUnknown * Create_##Name(PCSTR pName);			\
									\
	piob->RegisterInstantiator("driver", Create_##Name);		\
									\
	} END								\

#define Tester_Register(Name)						\
									\
	BEGIN {								\
									\
	extern IUnknown * Create_##Name(PCSTR pName);			\
									\
	piob->RegisterInstantiator("tester", Create_##Name);		\
									\
	} END								\

//////////////////////////////////////////////////////////////////////////
//
// Legacy 
//

#define INSTANTIATE(Name) Driver_Instantiate(Name)

//////////////////////////////////////////////////////////////////////////
//
// Visual C++ 2010 Warning Control
//

#if _MSC_VER == 1600

#pragma warning(disable: 4244)	// conversion possible loss of data

#endif

// End of File

#endif
