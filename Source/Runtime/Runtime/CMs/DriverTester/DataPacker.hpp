
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DataPacker_HPP

#define INCLUDE_DataPacker_HPP

//////////////////////////////////////////////////////////////////////////
//
// Motorola Data Packer
//

class DLLAPI CMotorDataPacker 
{
	public:
		// Constructor
		CMotorDataPacker(PDWORD pData, UINT uCount, BOOL fSign);

		// Pack
		void Pack(PBYTE  pData) const;
		void Pack(PWORD  pData) const;
		void Pack(PDWORD pData) const;

		// Unpack
		void Unpack(PBYTE  pData);
		void Unpack(PWORD  pData);
		void Unpack(PDWORD pData);

	protected:
		// Data
		PDWORD m_pData;
		UINT   m_uCount;
		BOOL   m_fSign;
	};

//////////////////////////////////////////////////////////////////////////
//
// Intel Data Packer
//

class DLLAPI CIntelDataPacker
{
	public:
		// Constructor
		CIntelDataPacker(PDWORD pData, UINT uCount, BOOL fSign);

		// Pack
		void Pack(PBYTE  pData) const;
		void Pack(PWORD  pData) const;
		void Pack(PDWORD pData) const;

		// Unpack
		void Unpack(PBYTE  pData);
		void Unpack(PWORD  pData);
		void Unpack(PDWORD pData);

	protected:
		// Data
		PDWORD m_pData;
		UINT   m_uCount;
		BOOL   m_fSign;
	};

// End of File

#endif
