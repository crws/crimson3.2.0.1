
#include "Intern.hpp"

#include "mbtcpsTester.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Modbus TCP/IP Slave Driver Tester
//

// Instantiator

INSTANTIATE(CModbusTCPSlaveTester);

// Constructor

CModbusTCPSlaveTester::CModbusTCPSlaveTester(void)
{
	memset(m_Data, 0, sizeof(m_Data));

	m_uTrans = 0;
	}

// Destructor

CModbusTCPSlaveTester::~CModbusTCPSlaveTester(void)
{
	}

// IDriverTester

BOOL CModbusTCPSlaveTester::GetSerialConfig(CSerialConfig &Config)
{
	return FALSE;
	}

BOOL CModbusTCPSlaveTester::GetDriverConfig(PBYTE &pData)
{
	AddWord(pData, 502);	// m_Socket
	AddWord(pData, 4);	// m_Count
	AddByte(pData, 0);	// m_Restrict
	AddLong(pData, 0);	// m_SecAddr
	AddLong(pData, 0);	// m_SecMask
	AddByte(pData, 0);	// m_FlipLong
	AddByte(pData, 0);	// m_FlipReal
	AddLong(pData, 10000);	// m_Timeout

	return TRUE;
	}

BOOL CModbusTCPSlaveTester::GetDeviceConfig(PBYTE &pData)
{
	return TRUE;
	}

CCODE CModbusTCPSlaveTester::SlaveRead(AREF Addr, PDWORD pData, UINT uCount)
{
	CCODE Code = CCODE_ERROR;

	if( Addr.a.m_Table == 1 ) {

		if( Addr.a.m_Type == addrWordAsWord ) {

			UINT o = Addr.a.m_Offset;

			for( UINT n = 0; n < uCount; n++ ) {

				if( o >= 1 && o <= elements(m_Data) ) {

					if( pData ) {

						pData[n] = m_Data[o-1]++;
						}

					Code = uCount;
					}

				o++;
				}
			}
		}

	if( pData ) {

		ShowRead(Code, m_uTrans, Addr, pData, uCount, TRUE);

		m_uTrans = (m_uTrans + 1) % 100;
		}

	return Code;
	}

CCODE CModbusTCPSlaveTester::SlaveRead(AREF Addr, PDWORD pData, IMetaData **ppm)
{
	*ppm = NULL;

	return 0;
	}

CCODE CModbusTCPSlaveTester::SlaveWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	CCODE Code = CCODE_ERROR;

	if( Addr.a.m_Table == 1 ) {

		if( Addr.a.m_Type == addrWordAsWord ) {

			UINT o = Addr.a.m_Offset;

			for( UINT n = 0; n < uCount; n++ ) {

				if( o >= 1 && o <= elements(m_Data) ) {

					if( pData ) {

						m_Data[o-1] = pData[n];
						}

					Code = uCount;
					}

				o++;
				}
			}
		}

	if( pData ) {

		ShowWrite(Code, m_uTrans, Addr, pData, uCount, TRUE);

		m_uTrans = (m_uTrans + 1) % 100;
		}

	return Code;
	}

// End of File
