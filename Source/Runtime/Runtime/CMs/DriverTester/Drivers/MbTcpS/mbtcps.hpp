
//////////////////////////////////////////////////////////////////////////
//
// Modbus Exception Codes
//

#define	ILLEGAL_FUNCTION	0x01

#define ILLEGAL_ADDRESS		0x02

#define	ILLEGAL_DATA		0x03

//////////////////////////////////////////////////////////////////////////
//
// Modbus TCP Slave
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

class CModbusTCPSlave : public CSlaveDriver
{
	public:
		// Constructor
		CModbusTCPSlave(void);

		// Destructor
		~CModbusTCPSlave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Entry Points
		DEFMETH(void) Service(void);

		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);
		
	protected:
		// Socket Context
		struct SOCKET
		{
			ISocket	* m_pSocket;
			UINT	  m_uPtr;
			PBYTE	  m_pData;
			BOOL	  m_fBusy;
			UINT	  m_uTime;
			};

		// Data Members
		UINT    m_uPort;
		UINT    m_uCount;
		UINT	m_uRestrict;
		DWORD	m_SecData;
		DWORD	m_SecMask;
		BOOL	m_fFlipLong;
		BOOL	m_fFlipReal;
		SOCKET *m_pSock;
		UINT    m_uTimeout;
		BOOL	m_fEnable;

		// Implementation
		void OpenSocket(UINT n, SOCKET &Sock);
		void CloseSocket(SOCKET &Sock);
		void ReadData(SOCKET &Sock);
		BOOL CheckAccess(SOCKET &Sock, BOOL fWrite);
		BOOL DoFrame(SOCKET &Sock, PBYTE pData, UINT uSize);

		// Opcode Handlers
		BOOL DoStdRead (SOCKET &Sock, PBYTE pData, UINT uSize, BYTE bCode);
		BOOL DoStdWrite(SOCKET &Sock, PBYTE pData, UINT uSize, BYTE bCode);
		BOOL DoBitRead (SOCKET &Sock, PBYTE pData, UINT uSize, BYTE bCode);
		BOOL DoBitWrite(SOCKET &Sock, PBYTE pData, UINT uSize, BYTE bCode);
		BOOL DoFuncXX  (SOCKET &Sock, PBYTE pData, UINT uSize, BYTE bCode);

		// Helpers
		void Make16BitSigned(DWORD &dwData);
		BOOL IsLongReg(UINT uType);
		BOOL IsReal(UINT uType);
	};

// End of File
