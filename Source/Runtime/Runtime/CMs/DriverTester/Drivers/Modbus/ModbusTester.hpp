
#include "../../Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ModbusTester_HPP

#define INCLUDE_ModbusTester_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../Comms.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver Tester
//

class CModbusDriverTester : public CCommsTester
{
	public:
		// Constructor
		CModbusDriverTester(void);

		// Destructor
		~CModbusDriverTester(void);

		// IDriverTester
		BOOL METHOD GetSerialConfig(CSerialConfig &Config);
		BOOL METHOD GetDriverConfig(PBYTE &pData);
		BOOL METHOD GetDeviceConfig(PBYTE &pData);
		BOOL METHOD ExecDriverCtrl(ICommsDriver *pDriver);
		BOOL METHOD ExecDeviceCtrl(ICommsDriver *pDriver);
		BOOL METHOD ExecTest(CCODE &Code, ICommsMaster *pDriver, UINT uIndex);
	};

// End of File

#endif
