
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Helper_HPP

#define INCLUDE_Helper_HPP

//////////////////////////////////////////////////////////////////////////
//
// Helper Object
//

class DLLAPI CHelper : public IHelper
{
	public:
		// Constructor
		CHelper(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IHelper
		BOOL METHOD MoreHelp(WORD ID, void **pHelp); 

	protected:
		// Data Members
		ULONG m_uRefs;
	};

// End of File

#endif
