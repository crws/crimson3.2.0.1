
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Ascii_HPP

#define INCLUDE_Ascii_HPP

//////////////////////////////////////////////////////////////////////////
//
// Codes
//

#define	NUL	0x00
#define	SOH	0x01
#define	STX	0x02
#define	ETX	0x03
#define	EOT	0x04
#define	ENQ	0x05
#define	ACK	0x06
#define	LF	0x0A
#define	CR	0x0D
#define	DLE	0x10
#define DC2	0x12
#define	NAK	0x15
#define	SYN	0x16
#define	ETB	0x17
#define	ESC	0x1B
#define	PAD	0xFE

// End of File

#endif
