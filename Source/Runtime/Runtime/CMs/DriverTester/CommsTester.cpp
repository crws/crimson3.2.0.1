
#include "Intern.hpp"

#include "CommsTester.hpp"

#include "Align.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Driver Tester Object
//

// Constructor

CCommsTester::CCommsTester(void)
{
	StdSetRef();
	}

// Destructor

CCommsTester::~CCommsTester(void)
{
	}

// IUnknown

HRESULT CCommsTester::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ICommsTester);

	StdQueryInterface(ICommsTester);

	return E_NOINTERFACE;
	}

ULONG CCommsTester::AddRef(void)
{
	StdAddRef();
	}

ULONG CCommsTester::Release(void)
{
	StdRelease();
	}

// ICommsTester

BOOL CCommsTester::GetSerialConfig(CSerialConfig &Config)
{
	Config.m_uPhysical = physicalRS232;
	Config.m_uPhysMode = 0;
	Config.m_uBaudRate = 9600;
	Config.m_uDataBits = 8;
	Config.m_uStopBits = 1;
	Config.m_uParity   = parityNone;
	Config.m_uFlags    = flagNone;
	Config.m_bDrop     = 0;	

	return TRUE;
	}

BOOL CCommsTester::GetDriverConfig(PBYTE &pData)
{
	return TRUE;
	}

BOOL CCommsTester::GetDeviceConfig(PBYTE &pData)
{
	return TRUE;
	}

BOOL CCommsTester::ExecDriverCtrl(ICommsDriver *pDriver)
{
	return TRUE;
	}

BOOL CCommsTester::ExecDeviceCtrl(ICommsDriver *pDriver)
{
	return TRUE;
	}

BOOL CCommsTester::ExecTest(CCODE &Code, ICommsMaster *pDriver, UINT uIndex)
{
	return FALSE;
	}

CCODE CCommsTester::SlaveRead(AREF Addr, PDWORD pData, UINT uCount)
{
	CCODE Code = CCODE_ERROR;

	ShowRead(Code, 0, Addr, pData, uCount, FALSE);

	return Code;
	}

CCODE CCommsTester::SlaveRead(AREF Addr, PDWORD pData, IMetaData **ppm)
{
	*ppm = NULL;

	return 0;
	}

CCODE CCommsTester::SlaveWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	CCODE Code = CCODE_ERROR;

	ShowWrite(Code, 0, Addr, pData, uCount, TRUE);

	return Code;
	}

// Init Helpers

void CCommsTester::AddByte(PBYTE &pData, BYTE Data)
{
	*pData++ = Data;
	}

void CCommsTester::AddWord(PBYTE &pData, WORD Data)
{
	Item_Align2(pData);

	*PWORD(pData) = HostToMotor(Data);

	pData += sizeof(Data);
	}

void CCommsTester::AddLong(PBYTE &pData, LONG Data)
{
	Item_Align4(pData);

	*PDWORD(pData) = HostToMotor(Data);

	pData += sizeof(Data);
	}

void CCommsTester::AddAddr(PBYTE &pData, IPREF Data)
{
	Item_Align4(pData);

	*PDWORD(pData) = Data.m_dw;

	pData += sizeof(Data);
	}

void CCommsTester::AddText(PBYTE &pData, PCTXT Data)
{
	UINT uLen = strlen(Data);

	AddWord(pData, WORD(uLen));

	for( UINT n = 0; n < uLen; n++ ) {

		AddWord(pData, Data[n]);
		}
	}

void CCommsTester::AddData(PBYTE &pData, PCBYTE Data, UINT uSize)
{
	memcpy(pData, Data, uSize);

	pData += uSize;
	}

// Diagnostics

void CCommsTester::ShowRead(CCODE Code, UINT uIndex, AREF Addr, PDWORD pData, UINT uCount, BOOL fData)
{
	CString Text;

	DecodeAddress(Text, Addr);

	AfxTrace("%2.2u) Read %s for %u\n\n", uIndex, PCTXT(Text), uCount);

	if( COMMS_SUCCESS(Code) ) {

		if( Code & CCODE_NO_DATA ) {

			AfxTrace("    Read no data\n\n");
			}
		else {
			UINT c = LOWORD(Code);

			AfxTrace("    Read %u register%s\n\n", c, c == 1 ? "" : "s");

			if( fData ) {

				ShowData(Code, Addr, pData, c);
				}
			}

		return;
		}

	DecodeFailure(Text, Code);

	AfxTrace("    %s\n\n", PCTXT(Text));
	}

void CCommsTester::ShowWrite(CCODE Code, UINT uIndex, AREF Addr, PDWORD pData, UINT uCount, BOOL fData)
{
	CString Text;

	DecodeAddress(Text, Addr);

	AfxTrace("%2.2u) Write %s for %u\n\n", uIndex, PCTXT(Text), uCount);

	if( COMMS_SUCCESS(Code) ) {

		UINT c = LOWORD(Code);

		AfxTrace("    Wrote %u register%s\n\n", c, c == 1 ? "" : "s");

		if( fData ) {

			ShowData(Code, Addr, pData, c);
			}

		return;
		}

	DecodeFailure(Text, Code);

	AfxTrace("    %s\n\n", PCTXT(Text));
	}

void CCommsTester::ShowData(CCODE Code, AREF Addr, PDWORD pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {

		AfxTrace("    %2.2u) 0x%8.8X = ", n, pData[n]);

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:

				AfxTrace(pData[n] ? "ON" : "OFF");

				break;
					
			case addrBitAsByte:
			case addrByteAsByte:

				AfxTrace("0x%2.2X %u", BYTE(pData[n]), BYTE(pData[n]));

				break;

			case addrBitAsWord:
			case addrByteAsWord:
			case addrWordAsWord:

				AfxTrace("0x%4.4X %5u %6d", WORD(pData[n]), WORD(pData[n]), SHORT(pData[n]));

				break;

			case addrBitAsLong:
			case addrByteAsLong:
			case addrWordAsLong:
			case addrLongAsLong:

				AfxTrace("0x%8.8X %10u %11d", DWORD(pData[n]), DWORD(pData[n]), LONG(pData[n]));

				break;

			case addrBitAsReal:
			case addrByteAsReal:
			case addrWordAsReal:
			case addrLongAsReal:
			case addrRealAsReal:

				AfxTrace("%f", (double) (float &) pData[n]);

				break;
					
			case addrReserved:

				AfxTrace("Reserved");

				break;
			}

		AfxTrace("\n");
		}

	AfxTrace("\n");
	}

void CCommsTester::DecodeAddress(CString &Text, AREF Addr)
{
	Text.Printf("0x%8.8X (", Addr.m_Ref);

	Text.AppendPrintf("t=%u,o=%u,x=%u,", Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Extra);

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:   Text += "BitAsBit";   break;
		case addrBitAsByte:  Text += "BitAsByte";  break;
		case addrBitAsWord:  Text += "BitAsWord";  break;
		case addrBitAsLong:  Text += "BitAsLong";  break;
		case addrBitAsReal:  Text += "BitAsReal";  break;
		case addrByteAsByte: Text += "ByteAsByte"; break;
		case addrByteAsWord: Text += "ByteAsWord"; break;
		case addrByteAsLong: Text += "ByteAsLong"; break;
		case addrByteAsReal: Text += "ByteAsReal"; break;
		case addrWordAsWord: Text += "WordAsWord"; break;
		case addrWordAsLong: Text += "WordAsLong"; break;
		case addrWordAsReal: Text += "WordAsReal"; break;
		case addrLongAsLong: Text += "LongAsLong"; break;
		case addrLongAsReal: Text += "LongAsReal"; break;
		case addrRealAsReal: Text += "RealAsReal"; break;
		case addrReserved:   Text += "Reserved";   break;
		}

	Text += ")";
	}

void CCommsTester::DecodeFailure(CString &Text, CCODE Code)
{
	Text.Printf("ERROR 0x%8.8X", Code);

	static DWORD Mask[] = { CCODE_HARD, CCODE_BUSY, CCODE_SILENT, CCODE_NO_RETRY, CCODE_NO_CONFIG };

	static PCTXT Name[] = { "hard", "busy", "slient", "no-retry", "no-config" };

	UINT c = 0;

	for( UINT n = 0; n < elements(Mask); n++ ) {

		if( Code & Mask[n] ) {
			
			Text += c++ ? "," : " (";

			Text += Name[n];
			}
		}

	if( c ) {

		Text += ")";
		}
	}

// End of File
