
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Crc16_HPP

#define INCLUDE_Crc16_HPP

//////////////////////////////////////////////////////////////////////////
//
// 16-Bit CRC Generator 
//

class DLLAPI CRC16
{
	public:
		// Constructor
		CRC16(void);
			
		// Attributes
		WORD GetValue(void) const;
			
		// Operations
		void Clear(void);
		void Preset(void);
		void Add(BYTE b);
			
	protected:
		// Lookup Data
		static WORD m_Table[];

		// Data Members
		WORD m_CRC;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructor

INLINE CRC16::CRC16(void)
{
	m_CRC = 0;
	}
	
// Attributes

INLINE WORD CRC16::GetValue(void) const
{
	return m_CRC;
	}
	
// Operations

INLINE void CRC16::Clear(void)
{
	m_CRC = 0x0000;
	}
	
INLINE void CRC16::Preset(void)
{
	m_CRC = 0xFFFF;
	}

INLINE void CRC16::Add(BYTE b)
{
	m_CRC = WORD((m_CRC >> 8) ^ m_Table[b ^ (m_CRC & 0xFF)]);
	}

// End of File

#endif
