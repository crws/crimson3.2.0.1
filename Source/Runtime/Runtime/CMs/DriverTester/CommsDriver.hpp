
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsDriver_HPP

#define INCLUDE_CommsDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Driver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Comms Driver Object
//

class DLLAPI CCommsDriver : public CDriver
{
	public:
		// Constructor
		CCommsDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);

		// ICommsDriver
		virtual void  METHOD Load(LPCBYTE pData);
		virtual void  METHOD Load(LPCBYTE pData, UINT uSize);
		virtual void  METHOD CheckConfig(CSerialConfig &Config);
		virtual void  METHOD Attach(IPortObject *pPort);
		virtual void  METHOD Detach(void);
		virtual void  METHOD Open(void);
		virtual void  METHOD Close(void);
		virtual CCODE METHOD DeviceOpen(ICommsDevice *pDevice);
		virtual CCODE METHOD DeviceClose(BOOL fPersistConnection);
		virtual void  METHOD Service(void);
		virtual UINT  METHOD DrvCtrl(UINT uFunc, PCTXT pValue);
		virtual UINT  METHOD DevCtrl(void *pContext, UINT uFunc, PCTXT pValue);
		
	protected:
		// Data Members
		ICommsDevice * m_pDevice;
		IDataHandler * m_pData;
		BOOL	       m_fOpen;

		// Implementation
		BOOL Make485(CSerialConfig &Config, BOOL fMaster);
		BOOL Make422(CSerialConfig &Config, BOOL fMaster);
	};

// End of File

#endif
