
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NViewListener_HPP

#define INCLUDE_NViewListener_HPP

//////////////////////////////////////////////////////////////////////////
//
// N-View Types
//

typedef BYTE	NT_U8;
typedef WORD	NT_U16;
typedef DWORD	NT_U32;
typedef INT64   NT_U64;
typedef	UINT	NT_STATUS;
typedef	VOID	NT_VOID;
typedef UINT	port_NumberType;

//////////////////////////////////////////////////////////////////////////
//
// N-View Header
//

#pragma  pack(1)

#define  NTOS_PACKED

#include "autocast.h"

#pragma  pack()

//////////////////////////////////////////////////////////////////////////
//
// N-View Listener
//

class CNViewListener : public IClientProcess
{
	public:
		// Constructor
		CNViewListener(void);

		// Destructor
		~CNViewListener(void);

		// Attributes
		UINT GetSwitchCount(void) const;

		// Switch Attributes
		CString  GetSwitchName(UINT s) const;
		CString  GetSwitchModel(UINT s) const;
		UINT     GetSwitchPorts(UINT s) const;
		bool     IsSwitchActive(UINT s) const;
		CIpAddr  GetSwitchIp(UINT s) const;
		CMacAddr GetSwitchMac(UINT s) const;
		CMacAddr GetSwitchRxMac(UINT s) const;
		UINT     GetSwitchVersion(UINT s) const;
		timeval  GetSwitchUpdate(UINT s) const;
		bool     IsRingManager(UINT s) const;
		bool     IsRingActive(UINT s) const;
		UINT     GetRingState(UINT s) const;

		// Port Attributes
		CString GetPortName(UINT s, UINT p) const;
		UINT    GetPortChipId(UINT s, UINT p) const;
		UINT    GetPortPortId(UINT s, UINT p) const;
		bool    GetPortStatus(UINT s, UINT p) const;
		bool    GetPortEnabled(UINT s, UINT p) const;
		bool    GetPortDuplex(UINT s, UINT p) const;
		bool    GetPortPause(UINT s, UINT p) const;
		UINT    GetPortSpeed(UINT s, UINT p) const;

		// Counter Attributes
		UINT    GetCounterCount(void) const;
		CString GetCounterName(UINT n) const;
		bool    IsCounter64(UINT n) const;
		UINT    GetCounter32(UINT s, UINT p, UINT n) const;
		UINT64  GetCounter64(UINT s, UINT p, UINT n) const;

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

	protected:
		// Packing
		#pragma pack(1)

		// N-View Frames
		struct CRawHeader
		{
			CMacAddr	  m_Dest;
			CMacAddr	  m_Source;
			WORD		  m_Type;

			void Translate(void);
			};

		struct CAcPortFrame
		{
			AcPortType	  m_Port;
			AcMibType	  m_Mib;

			void Translate(void);
			};

		struct CAcFrame
		{
			NT_U16		  m_nVersion;
			NT_U32		  m_IpAddress;
			AcRingData	  m_Ring;
			NT_U8		  m_acModel[NT_AC_MODEL_MAX_SIZE];

			void Translate(void);
			};

		struct CAcFrameVersion1 : CAcFrame
		{
			CAcPortFrame	  m_Port;

			void Translate(void);
			};

		struct CAcFrameVersion3 : CAcFrame
		{		
			AcPortExtDataType m_ExtData;
			CAcPortFrame	  m_Port;

			void Translate(void);
			};

		// Packing
		#pragma pack()

		// Port Record
		struct CPort
		{
			UINT         m_uSpeed;
			CAcPortFrame m_Frame;
			};

		// Switch Record
		struct CSwitch
		{
			CString	     m_Name;
			UINT         m_uPorts;
			UINT         m_uGrouping;
			UINT	     m_uPerChip;
			UINT         m_uMatch;
			CMacAddr     m_Mac;
			CMacAddr     m_RxMac;
			CIpAddr      m_Ip;
			bool         m_fActive;
			timeval      m_Update;
			UINT         m_uVersion;
			CString      m_Model;
			AcRingData   m_Ring;
			CPort      * m_pPorts;
			};

		// Data Members
		ULONG     m_uRefs;
		ISocket * m_pSock;
		UINT      m_uSwitch;
		CSwitch * m_pSwitch;

		// Implementation
		void ReadConfig(void);
		void ParseConfig(CString Text);
	};

// End of File

#endif
