
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AppObject_HPP

#define INCLUDE_AppObject_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CNViewListener;

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

class CAppObject : public IClientProcess, public IOpcUaServerHost
{
	public:
		// Constructor
		CAppObject(void);

		// Destructor
		~CAppObject(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// IOpcUaServerHost
		bool    LoadModel(IOpcUaDataModel *pModel);
		bool    CheckUser(CString const &User, CString const &Pass);
		bool    SkipValue(UINT ns, UINT id);
		void    SetValueDouble(UINT ns, UINT id, UINT n, double d);
		void    SetValueInt(UINT ns, UINT id, UINT n, int d);
		void    SetValueInt64(UINT ns, UINT id, UINT n, INT64 d);
		void    SetValueString(UINT ns, UINT id, UINT n, PCTXT d);
		double  GetValueDouble(UINT ns, UINT id, UINT n);
		UINT    GetValueInt(UINT ns, UINT id, UINT n);
		UINT64  GetValueInt64(UINT ns, UINT id, UINT n);
		CString GetValueString(UINT ns, UINT id, UINT n);
		timeval GetValueTime(UINT ns, UINT id, UINT n);
		bool    GetDesc(CString &Desc, UINT ns, UINT id);
		bool    GetSourceTimeStamp(timeval &t, UINT ns, UINT id);
		bool    HasEvents(CArray <UINT> &List, UINT ns, UINT id);
		bool    HasCustomHistory(UINT ns, UINT id);
		bool    InitHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory);
		bool	HasChanged(UINT ns, UINT id, UINT uType, DWORD *pHistory);
		void	KillHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory);
		UINT	GetWireType(UINT uType);

	protected:
		// Data Members
		ULONG	         m_uRefs;
		IOpcUaServer   * m_pServer;
		INT	         m_nData;
		CNViewListener * m_pNView;
		IThread        * m_pThread;

		// Implementation
		void AddSwitch(IOpcUaDataModel *pModel, UINT s);
		void AddSwitchVars(IOpcUaDataModel *pModel, UINT nsSwitch, UINT idSwitch, UINT nsVars, UINT idVars);
		void AddPort(IOpcUaDataModel *pModel, UINT nsSwitch, UINT idSwitch, UINT idPorts, UINT s, UINT p);
		void AddPortVars(IOpcUaDataModel *pModel, UINT nsPort, UINT idPort, UINT nsVars, UINT idVars);
		void AddMibVars(IOpcUaDataModel *pModel, UINT nsMib, UINT idMib, UINT nsVars, UINT idVars);
	};

// End of File

#endif
