
#include "Intern.hpp"

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Coded Host
//

// Loading Helpers

void CCodedHost::GetCoded(PCBYTE &pData, CCodedItem * &pItem)
{
	BYTE bType = GetByte(pData);

	if( bType ) {

		pItem = New CCodedItem;

		if( bType == 1 || bType == 2 ) {

			pItem->m_Data = GetLong(pData);

			return;
			}

		if( bType == 3 ) {

			UINT    c = GetWord(pData);

			CString t = CString(PCTXT(pData), c);

			pItem->m_Data = DWORD(wstrdup(UniConvert(t)));

			pData += 1 * c;

			return;
			}

		if( bType == 4 ) {

			UINT     c = GetWord(pData);

			CUnicode t = CUnicode(PCUTF(pData), c);

			pItem->m_Data = DWORD(wstrdup(t));

			pData += 2 * c;

			return;
			}

		AfxAssert(FALSE);
		}
	}

// Scan Control

void CCodedHost::SetItemScan(CCodedItem *pItem, UINT uCode)
{
	}

// Data Access

C3INT CCodedHost::GetItemData(CCodedItem *pItem, C3INT Default)
{
	return pItem ? C3INT(pItem->m_Data) : Default;
	}

C3REAL CCodedHost::GetItemData(CCodedItem *pItem, C3REAL Default)
{
	return pItem ? I2R(pItem->m_Data) : Default;
	}

CUnicode CCodedHost::GetItemData(CCodedItem *pItem, PCUTF Default)
{
	return CUnicode(pItem ? PCUTF(pItem->m_Data) : Default);
	}

CString CCodedHost::GetItemData(CCodedItem *pItem, PCTXT Default)
{
	return pItem ? UniConvert(PCUTF(pItem->m_Data)) : CString(Default);
	}

// Data Free

BOOL CCodedHost::FreeData(DWORD Data, UINT Type)
{
	if( Type == typeString ) {

		free(PTXT(Data));

		return TRUE;
		}

	return FALSE;
	}

// Null Data

DWORD CCodedHost::GetNull(UINT Type)
{
	switch( Type ) {
		
		case typeInteger:

			return 0;

		case typeReal:

			return R2I(0);

		case typeString:

			return DWORD(wstrdup(L""));
		}

	return 0;
	}

// End of File
