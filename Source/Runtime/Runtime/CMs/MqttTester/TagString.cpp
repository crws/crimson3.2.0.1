
#include "Intern.hpp"

#include "TagString.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Crimson Real Tag
//

// Constructor

CTagString::CTagString(CString Name, CUnicode Initial, UINT msUpdate)
{
	InitData();

	m_Name     = Name;

	m_Desc     = L"The tag named " + m_Name;

	m_pData[0] = Initial;

	m_Initial  = Initial;

	m_msUpdate = msUpdate;

	m_msLast   = ToTime(GetTickCount());
	}

// Destructor

CTagString::~CTagString(void)
{
	delete [] m_pData;
	}

// Attributes

UINT CTagString::GetDataType(void) const
{
	return typeString;
	}

// Evaluation

BOOL CTagString::IsAvail(CDataRef const &Ref, UINT Flags)
{
	if( LocalData() ) {

		return TRUE;
		}

	return CDataTag::IsAvail(Ref, 0); 
	}

BOOL CTagString::SetScan(CDataRef const &Ref, UINT Code)
{
	if( LocalData() ) {

		return TRUE;
		}

	return CDataTag::SetScan(Ref, Code);
	}

DWORD CTagString::GetData(CDataRef const &Ref, UINT Type, UINT Flags)
{
	if( m_msUpdate ) {

		UINT msNow = ToTime(GetTickCount());

		while( msNow >= m_msLast + m_msUpdate ) {

			for( UINT n = 0; n < m_uSize; n++ ) {

				m_pData[n].Append('*');

				if( m_pData[n].GetLength() > m_Initial.GetLength() + 10 ) {

					m_pData[n] = m_Initial;
					}
				}

			m_msLast += m_msUpdate;
			}
		}

	if( Flags & getNoString ) {

		return 0;
		}

	if( IsString(Type) ) {

		UINT n = Ref.x.m_Array;

		if( n < m_uSize ) {

			if( LocalData() ) {

				return DWORD(wstrdup(m_pData[n]));
				}

			return CDataTag::GetData(Ref, Type, Flags);
			}
		}

	return GetNull(Type);
	}

BOOL CTagString::SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	if( IsString(Type) ) {

		UINT n = Ref.x.m_Array;

		if( n < m_uSize ) {

			if( !(Flags & setForce) ) {

				DWORD Read = GetData(Ref, Type, Flags);

				if( !wstrcmp(PUTF(Data), PUTF(Read)) ) {

					free(PUTF(Data));

					free(PUTF(Read));

					return TRUE;
					}

				free(PUTF(Read));
				}

			if( LocalData() ) {

				m_pData[n] = PCUTF(Data);

				free(PUTF(Data));

				return TRUE;
				}
			}
		}

	return CDataTag::SetData(Ref, Data, Type, Flags);
	}

DWORD CTagString::GetProp(CDataRef const &Ref, WORD ID, UINT Type)
{
	return CDataTag::GetProp(Ref, ID, Type);
	}

// Deadband

void CTagString::InitPrevious(DWORD &Prev)
{
	Prev = 0;
	}

BOOL CTagString::HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev)
{
	if( Prev == 0 ) {

		Data = GetData(Ref, typeString, getNone);

		return TRUE;
		}
	else {
		PUTF pRead = PUTF(GetData(Ref, typeString, getNone));

		PUTF pLast = PUTF(Prev);

		if( wstrcmp(pRead, pLast) ) {

			free(PUTF(Data));

			Data = DWORD(pRead);

			return TRUE;
			}

		free(pRead);

		return FALSE;
		}
	}

void CTagString::KillPrevious(DWORD &Prev)
{
	free(PTXT(Prev));

	Prev = 0;
	}

// Implementation

BOOL CTagString::InitData(void)
{
	if( LocalData() ) {

		m_uSize = max(m_Extent, 1);

		m_pData = New CUnicode [ m_uSize ];

		return TRUE;
		}

	return FALSE;
	}

// End of File
