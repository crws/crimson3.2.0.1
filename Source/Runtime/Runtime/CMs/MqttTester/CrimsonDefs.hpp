
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CrimsonDefs_HPP

#define INCLUDE_CrimsonDefs_HPP

//////////////////////////////////////////////////////////////////////////
//
// Format Flags
//

enum FmtFlag
{
	fmtStd	 = 0x00,
	fmtPad	 = 0x01,
	fmtBare  = 0x02,
	fmtUnits = 0x04,
	fmtTime  = 0x08,
	fmtDate  = 0x10,
	fmtANSI	 = 0x20,
	fmtShow  = 0x40,
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Types
//

enum DataType
{
	typeVoid	= 0x0000,
	typeInteger	= 0x0001,
	typeReal	= 0x0002,
	typeString	= 0x0003,
	typeLogical	= 0x0004,
	typeLValue	= 0x0005,
	typeNumeric	= 0x0006,
	typeObject	= 0x0080,
	typeMask	= 0x00FF
	};

//////////////////////////////////////////////////////////////////////////
//
// Type Flags
//

enum TypeFlag
{
	flagConstant  = 0x0001,
	flagWritable  = 0x0002,
	flagArray     = 0x0004,
	flagExtended  = 0x0008,
	flagElement   = 0x0010,
	flagBitRef    = 0x0020,
	flagLocal     = 0x0040,
	flagActive    = 0x0080,
	flagSoftWrite = 0x0100,
	flagInherent  = 0x0200,
	flagCommsRef  = 0x0400,
	flagCommsTab  = 0x0800,
	flagTagRef    = 0x1000,
	};

//////////////////////////////////////////////////////////////////////////
//
// GetData Flags
//

enum GetDataFlag
{
	getNone		= 0x0000,
	getMaskSource	= 0x000F,
	getScaled	= 0x0000,
	getManipulated	= 0x0001,
	getCommsData	= 0x0002,
	getNoString     = 0x0010,
	};

//////////////////////////////////////////////////////////////////////////
//
// SetData Flags
//

enum SetDataFlag
{
	setNone		= 0x0000,
	setMaskSource	= 0x000F,
	setScaled	= 0x0000,
	setManipulated	= 0x0001,
	setCommsData	= 0x0002,
	setForce	= 0x0010,
	setDirect	= 0x0020,
	setFlush	= 0x0040,
	};

//////////////////////////////////////////////////////////////////////////
//
// Scan Codes
//

enum ScanCode
{
	scanFalse   = 0,
	scanTrue    = 1,
	scanUser    = 2,
	scanOnce    = 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag Properties
//

enum
{
	tpAsText	= 1,
	tpLabel		= 2,
	tpDescription	= 3,
	tpPrefix	= 4,
	tpUnits		= 5,
	tpSetPoint	= 6,
	tpMinimum	= 7,
	tpMaximum	= 8,
	tpForeColor	= 9,
	tpBackColor	= 10,
	tpName		= 11,
	tpIndex		= 12,
	tpAlarms	= 13,
	tpTextOff	= 14,
	tpTextOn	= 15,
	tpStateCount	= 16,
	tpDeadband	= 17,
	tpStateText	= 1000,
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Reference
//

#if defined(_E_BIG)

struct CDataRef
{
	union {
		struct {
			UINT	m_HasBit : 1;
			UINT    m_BitRef : 5;
			UINT	m_Block  : 10;
			UINT	m_IsTag  : 1;
			INT	m_Offset : 15;
			} b;

		struct {
			UINT    m_HasBit : 1;
			UINT	m_BitRef : 5;
			UINT	m_Array  : 10;
			UINT	m_IsTag  : 1;
			UINT	m_Index  : 15;
			} t;

		struct {
			UINT    m_HasBit : 1;
			UINT	m_Array  : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Index  : 15;
			} x;
		
		DWORD m_Ref;
		};
	};

#else

struct CDataRef
{
	union {
		struct {
			INT	m_Offset : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Block  : 10;
			UINT    m_BitRef : 5;
			UINT	m_HasBit : 1;
			} b;

		struct {
			UINT	m_Index  : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Array  : 10;
			UINT	m_BitRef : 5;
			UINT    m_HasBit : 1;
			} t;

		struct {
			UINT	m_Index  : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Array  : 15;
			UINT    m_HasBit : 1;
			} x;

		DWORD m_Ref;
		};
	};
#endif

//////////////////////////////////////////////////////////////////////////
//
// Crimson Type Definitions
//

typedef signed int	C3INT;

typedef float		C3REAL;

//////////////////////////////////////////////////////////////////////////
//
// Crimson Type Conversion
//

STRONG_INLINE C3REAL I2R(DWORD d)
{
	return *((C3REAL *) &d);
	}

STRONG_INLINE DWORD R2I(C3REAL r)
{
	return *((DWORD *) &r);
	}

//////////////////////////////////////////////////////////////////////////
//
// Crimson Task Entry Interface
//

interface ITaskEntry
{
	virtual void TaskInit(UINT uID) = 0;
	virtual void TaskExec(UINT uID) = 0;
	virtual void TaskStop(UINT uID) = 0;
	virtual void TaskTerm(UINT uID) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Crimson Task Definition
//

struct CTaskDef
{
	CString      m_Name;
	ITaskEntry * m_pEntry;
	UINT	     m_uID;
	UINT	     m_uCount;
	UINT	     m_uLevel;
	UINT	     m_uStack;
	};

//////////////////////////////////////////////////////////////////////////
//
// Crimson Task List
//

typedef CArray <CTaskDef> CTaskList;

//////////////////////////////////////////////////////////////////////////
//
// Crimson Service Interface
//

interface IService
{
	virtual void LoadService(PCBYTE &pData)   = 0;
	virtual UINT GetID	(void)		  = 0;
	virtual void GetTaskList(CTaskList &List) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Crimson Time Access
//

STRONG_INLINE static INT GetZuluOffset(void)
{
	return -4 * 60 * 60;
	}

STRONG_INLINE static DWORD GetNow(void)
{
	timeval t;

	gettimeofday(&t, NULL);

	return t.tv_sec - 852076800 + 60 * GetZuluOffset();
	}

// End of File

#endif
