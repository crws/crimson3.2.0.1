
#include "Intern.hpp"

#include "ConfigBlob.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Configuration Blob
//

// Constructor

CConfigBlob::CConfigBlob(void)
{
	}

// Attributes

PCBYTE CConfigBlob::GetData(void) const
{
	return m_Data.GetPointer();
	}

// Operations

void CConfigBlob::AddByte(BYTE Data)
{
	m_Data.Append(Data);
	}

void CConfigBlob::AddWord(WORD Data)
{
	Align2();

	AddByte(HIBYTE(Data));
	AddByte(LOBYTE(Data));
	}

void CConfigBlob::AddLong(LONG Data)
{
	Align4();

	AddWord(HIWORD(Data));
	AddWord(LOWORD(Data));
	}

void CConfigBlob::AddAddr(IPREF Data)
{
	Align4();

	m_Data.Append(Data.m_b, sizeof(Data.m_b));
	}

void CConfigBlob::AddText(PCTXT pData)
{
	UINT uLen = strlen(pData);

	AddWord(WORD(uLen));

	for( UINT n = 0; n < uLen; n++ ) {

		AddWord(pData[n]);
		}
	}

void CConfigBlob::AddText(PCUTF pData)
{
	UINT uLen = wstrlen(pData);

	AddWord(WORD(uLen));

	for( UINT n = 0; n < uLen; n++ ) {

		AddWord(pData[n]);
		}
	}

void CConfigBlob::AddData(PCBYTE pData, UINT uSize)
{
	m_Data.Append(pData, uSize);
	}

void CConfigBlob::AddFile(PCBYTE pData, UINT uSize)
{
	AddWord(WORD(uSize));

	AddData(pData, uSize);
	}

void CConfigBlob::AddCode(C3INT Data)
{
	AddByte(1);

	AddLong(Data);
	}

void CConfigBlob::AddCode(C3REAL Data)
{
	AddByte(2);

	AddLong(R2I(Data));
	}

void CConfigBlob::AddCode(PCTXT Data)
{
	AddByte(3);

	AddWord(WORD(strlen(Data)));

	AddData(PCBYTE(Data), 1 * strlen(Data));
	}

void CConfigBlob::AddCode(PCUTF Data)
{
	AddByte(4);

	AddWord(WORD(wstrlen(Data)));

	AddData(PCBYTE(Data), 2 * wstrlen(Data));
	}

void CConfigBlob::AddNull(void)
{
	AddByte(0);
	}

// Implementation

void CConfigBlob::Align2(void)
{
	#if 1 || defined(_M_ARM)

	while( m_Data.GetCount() % 2 ) m_Data.Append(0);

	#endif
	}

void CConfigBlob::Align4(void)
{
	#if 1 || defined(_M_ARM)

	while( m_Data.GetCount() % 4 ) m_Data.Append(0);

	#endif
	}

// End of File
