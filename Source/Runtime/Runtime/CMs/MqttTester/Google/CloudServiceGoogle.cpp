
#include "Intern.hpp"

#include "Service.hpp"

#include "CloudServiceGoogle.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MqttClientGoogle.hpp"

#include "MqttClientOptionsGoogle.hpp"

#include "CloudDeviceDataSet.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Google MQTT Service
//

// Instantiator

IService * Create_CloudServiceGoogle(void)
{
	return New CCloudServiceGoogle;
	}

// Constructor

CCloudServiceGoogle::CCloudServiceGoogle(void)
{
	m_Name = "GOOGLE";
	}

// Initialization

void CCloudServiceGoogle::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudServiceGoogle", pData);

	CCloudServiceCrimson::Load(pData);

	for( UINT n = 0; n < elements(m_pSet); n++ ) {

		if( m_pSet[n]->m_Array == 0 ) {

			m_pSet[n]->AddRewrite('[', '-');

			m_pSet[n]->AddRewrite(']', 0);
			}

		if( m_pSet[n]->m_Tree == 0 ) {

			m_pSet[n]->AddRewrite('.', '-');
			}

		m_pSet[n]->m_Array |= 2;
		}

	CMqttClientOptionsGoogle *pOpts = New CMqttClientOptionsGoogle;

	pOpts->Load(pData);

	m_pOpts   = pOpts;

	CheckHistory();

	m_pClient = New CMqttClientGoogle(this, *pOpts);

	m_pOpts->m_DiskPath = "C:\\MQTT\\Google";
	}

// Service ID

UINT CCloudServiceGoogle::GetID(void)
{
	return 14;
	}

// End of File
