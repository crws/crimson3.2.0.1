
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqqtClientOptionsGoogle_HPP

#define	INCLUDE_MqqtClientOptionsGoogle_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientOptionsJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Google MQTT Client Options
//

class CMqttClientOptionsGoogle : public CMqttClientOptionsJson
{
	public:
		// Constructor
		CMqttClientOptionsGoogle(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		void MakeCredentials(void);

		// Data Members
		CString m_Project;
		CString m_Region;
		CString m_Registry;
		CString m_Device;
		PBYTE   m_pPrivData;
		UINT    m_uPrivData;
		CString m_PubTopic;
		CString m_SubTopic;

	protected:
		// Implementation
		BOOL LoadFile(PCBYTE &pData, PBYTE &pFile, UINT &uFile);
		void MakeFile(char *pName, PCBYTE pData, UINT uSize);
	};

// End of File

#endif
