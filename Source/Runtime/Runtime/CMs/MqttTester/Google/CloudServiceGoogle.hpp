
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudServiceGoogle_HPP

#define	INCLUDE_CloudServiceGoogle_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CloudServiceCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMqttClientOptionsGoogle;

class CMqttClientGoogle;

//////////////////////////////////////////////////////////////////////////
//
// Google MQTT Service
//

class CCloudServiceGoogle : public CCloudServiceCrimson
{
	public:
		// Constructor
		CCloudServiceGoogle(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Service ID
		UINT GetID(void);
	};

// End of File

#endif
