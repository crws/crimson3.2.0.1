
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CodedHost_HPP

#define INCLUDE_CodedHost_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCodedItem;

//////////////////////////////////////////////////////////////////////////
//
// Crimson Coded Host
//

class CCodedHost : public CBlobbedItem
{
	public:
		// Loading Helpers
		static void GetCoded(PCBYTE &pData, CCodedItem * &pItem);

	protected:
		// Scan Control
		static void SetItemScan(CCodedItem *pItem, UINT uCode);

		// Data Access
		static C3INT    GetItemData(CCodedItem *pItem, C3INT  Default);
		static C3REAL   GetItemData(CCodedItem *pItem, C3REAL Default);
		static CString  GetItemData(CCodedItem *pItem, PCTXT  Default);
		static CUnicode GetItemData(CCodedItem *pItem, PCUTF  Default);

		// Data Free
		static BOOL FreeData(DWORD Data, UINT Type);

		// Null Data
		static DWORD GetNull(UINT Type);
	};

#endif
