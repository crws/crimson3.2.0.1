
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqqtClientOptionsAws_HPP

#define	INCLUDE_MqqtClientOptionsAws_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientOptionsJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for AWS Options
//

class CMqttClientOptionsAws : public CMqttClientOptionsJson
{
	public:
		// Constructor
		CMqttClientOptionsAws(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		PBYTE m_pCertData;
		UINT  m_uCertData;
		PBYTE m_pPrivData;
		UINT  m_uPrivData;
		PBYTE m_pAuthData;
		UINT  m_uAuthData;
	
	protected:
		// Implementation
		BOOL LoadFile(PCBYTE &pData, PBYTE &pFile, UINT &uFile);
	};

// End of File

#endif
