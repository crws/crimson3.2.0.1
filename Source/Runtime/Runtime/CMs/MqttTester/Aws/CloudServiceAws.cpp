
#include "Intern.hpp"

#include "Service.hpp"

#include "CloudServiceAws.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MqttClientAws.hpp"

#include "MqttClientOptionsAws.hpp"

#include "CloudDeviceDataSet.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AWS Cloud Service
//

// Instantiator

IService * Create_CloudServiceAws(void)
{
	return New CCloudServiceAws;
	}

// Constructor

CCloudServiceAws::CCloudServiceAws(void)
{
	m_Name = "AWS";
	}

// Initialization

void CCloudServiceAws::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudServiceAws", pData);

	CCloudServiceCrimson::Load(pData);

	CMqttClientOptionsAws *pOpts = New CMqttClientOptionsAws;

	pOpts->Load(pData);

	m_pOpts   = pOpts;

	CheckHistory();

	m_pClient = New CMqttClientAws(this, *pOpts);
	}

// Service ID

UINT CCloudServiceAws::GetID(void)
{
	return 9;
	}

// End of File
