
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsAws.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for AWS Options
//

// Constructor

CMqttClientOptionsAws::CMqttClientOptionsAws(void)
{
	m_pCertData  = NULL;
	m_uCertData  = 0;
	m_pPrivData  = NULL;
	m_uPrivData  = 0;
	m_pAuthData  = NULL;
	m_uAuthData  = 0;
	}

// Initialization

void CMqttClientOptionsAws::Load(PCBYTE &pData)
{
	CMqttClientOptionsJson::Load(pData);

	LoadFile(pData, m_pCertData, m_uCertData);

	LoadFile(pData, m_pPrivData, m_uPrivData);

	LoadFile(pData, m_pAuthData, m_uAuthData);
	}

// Implementation

BOOL CMqttClientOptionsAws::LoadFile(PCBYTE &pData, PBYTE &pFile, UINT &uFile)
{
	if( (uFile = GetWord(pData)) ) {

		pFile = New BYTE [ uFile ];

		memcpy(pFile, pData, uFile);

		pData += uFile;

		return TRUE;
		}

	return FALSE;
	}

// End of File
