
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_APPOBJECT_Generic_HPP

#define INCLUDE_APPOBJECT_Generic_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "AppObject.hpp"

#include "ConfigBlob.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Service
//

extern IService * Create_CloudServiceGeneric(void);

//////////////////////////////////////////////////////////////////////////
//
// Service Creation
//

void CAppObject::CreateService(void)
{
	m_pService = Create_CloudServiceGeneric();
	}

void CAppObject::CreateServiceBlob(CConfigBlob &Blob)
{
	// CloudServiceCrimson

	Blob.AddWord(0x1234);		// Marker
	Blob.AddCode(C3INT(1));		// Enable
	Blob.AddByte(0);		// Service
	Blob.AddCode(0);		// Status
	Blob.AddCode(L"");		// Ident
	}

void CAppObject::CreateOptionsBlob(CConfigBlob &Blob)
{
	// MqqtClientOptions

	CString Peer1 = "192.168.1.217";

	CString Peer2 = "";

	CString Name  = "device-01";

	CString User  = "user";

	CString Pass  = "password";

	Blob.AddWord(0x1234);	// Marker
	Blob.AddByte(1);	// fDebug
	Blob.AddByte(0);	// fTls
	Blob.AddByte(0);	// uCheck
	Blob.AddWord(5883);	// uPort
	Blob.AddByte(1);	// PubQos
	Blob.AddByte(1);	// SubQos
	Blob.AddText(Peer1);	// PeerName[0]
	Blob.AddText(Peer2);	// PeerName[1]
	Blob.AddText(Name);	// ClientId
	Blob.AddText(User);	// UserName
	Blob.AddText(Pass);	// Password
	Blob.AddWord(30);	// ConnTimeout
	Blob.AddWord(15);	// SendTimeout
	Blob.AddWord(15);	// RecvTimeout
	Blob.AddWord(5);	// BackOffTime
	Blob.AddWord(30);	// BackOffMax
	Blob.AddWord(600);	// KeepAlive

	// MqttQueuedClientOptions

	Blob.AddByte(0);	// Buffer

	// MqttClientOptionsCrimson

	Blob.AddByte(0);	// Mode
	Blob.AddByte(0);	// Reconn

	// MqttClientOptionsJson

	Blob.AddByte(0);	// Root
	Blob.AddByte(0);	// Code

	// MqqtClientOptionsGeneric

	Blob.AddText(L"");	// PubTopic
	Blob.AddText(L"");	// SubTopic
	Blob.AddByte(1);	// NoDollar
	}

// End of File

#endif
