
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClientOptionsCrimson_HPP

#define	INCLUDE_MqttClientOptionsCrimson_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client Options
//

class CMqttClientOptionsCrimson : public CMqttQueuedClientOptions
{
	public:
		// Constructor
		CMqttClientOptionsCrimson(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		UINT m_Mode;
		UINT m_Reconn;

	protected:
		// Implementation
		void SetDefaults(void);
	};

// End of File

#endif
