
#include "Intern.hpp"

#include "TagReal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson Real Tag
//

// Constructor

CTagReal::CTagReal(CString Name, C3REAL Initial, C3REAL Deadband, UINT msUpdate) : CTagNumeric(typeReal)
{
	m_Name     = Name;

	m_Desc     = L"The tag named " + m_Name;

	m_pData[0] = R2I(Initial);

	m_Dead     = R2I(Deadband);

	m_msUpdate = msUpdate;

	m_msLast   = ToTime(GetTickCount());
	}

// Evaluation

DWORD CTagReal::GetData(CDataRef const &Ref, UINT Type, UINT Flags)
{
	if( m_msUpdate ) {

		UINT msNow = ToTime(GetTickCount());

		while( msNow >= m_msLast + m_msUpdate ) {

			for( UINT n = 0; n < m_uSize; n++ ) {

				((C3REAL &) m_pData[n]) += C3REAL(0.1);
				}

			m_msLast += m_msUpdate;
			}
		}

	return CTagNumeric::GetData(Ref, Type, Flags);
	}

// End of File
