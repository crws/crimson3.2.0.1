
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AppObject_HPP

#define INCLUDE_AppObject_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TagList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCloudServiceAws;

class CConfigBlob;

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

class CAppObject : public IClientProcess
{
	public:
		// Constructor
		CAppObject(void);

		// Destructor
		~CAppObject(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

	protected:
		// Data Members
		ULONG            m_uRefs;
		IService       * m_pService;
		CTaskList        m_TaskList;
		CArray <HTHREAD> m_Thread;
		
		// Implementation
		void InitEmulation(void);
		void KillEmulation(void);
		void InitEmulatedIp(void);
		void KillEmulatedIp(void);
		void InitEmulatedFs(void);
		void CreateService(void);
		void CreateTags(void);
		void CreateServiceBlob(CConfigBlob &Blob);
		void CreateOptionsBlob(CConfigBlob &Blob);
		BOOL AddDeviceData(CConfigBlob &Blob);
		BOOL AddTagList1(CConfigBlob &Blob);
		BOOL AddTagList2(CConfigBlob &Blob);
		BOOL AddTagList3(CConfigBlob &Blob);
		BOOL AddTagList4(CConfigBlob &Blob);
	};

// End of File

#endif
