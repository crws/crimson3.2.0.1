
#include "Intern.hpp"

#include "Service.hpp"

#include "CloudServiceCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudDeviceDataSet.hpp"

#include "CloudTagSet.hpp"

#include "MqttClientCrimson.hpp"

#include "MqttClientOptionsCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Service
//

// Constructor

CCloudServiceCrimson::CCloudServiceCrimson(void)
{
	m_pEnable  = NULL;

	m_uService = 0;

	m_pIdent   = NULL;

	m_pStatus  = NULL;

	m_pDev     = New CCloudDeviceDataSet;

	for( UINT n = 0; n < elements(m_pSet); n++ ) {

		m_pSet[n] = New CCloudTagSet(n);
		}

	m_pClient = NULL;

	m_pOpts   = NULL;

	m_Name    = "ANON";
	}

// Destructor

CCloudServiceCrimson::~CCloudServiceCrimson(void)
{
	delete m_pDev;

	for( UINT n = 0; n < elements(m_pSet); n++ ) {

		delete m_pSet[n];
		}

	delete m_pIdent;

	delete m_pStatus;

	delete m_pEnable;

	delete m_pClient;

	delete m_pOpts;
	}

// Initialization

void CCloudServiceCrimson::Load(PCBYTE &pData)
{
	GetCoded(pData, m_pEnable);

	m_uService = GetByte(pData);

	GetCoded(pData, m_pStatus);

	GetCoded(pData, m_pIdent);

	for( UINT n = 0; n < elements(m_pSet); n++ ) {

		m_pSet[n]->Load(pData);
		}

	m_pDev->Load(pData);
	}

// Task List

void CCloudServiceCrimson::GetTaskList(CTaskList &List)
{
	if( GetItemData(m_pEnable, C3INT(0)) ) {

		CTaskDef Task;

		Task.m_Name    = "MQTT-" + m_Name;
		Task.m_pEntry  = this;
		Task.m_uID     = 0;
		Task.m_uCount  = 3;
		Task.m_uLevel  = 2100;
		Task.m_uStack  = 4096;

		Task.m_uLevel += 3 * (GetID() - 8);

		List.Append(Task);
		}
	}

// Attributes

CCloudDataSet * CCloudServiceCrimson::GetDataSet(UINT n) const
{
	switch( n ) {

		case 0: return m_pSet[0];
		case 1: return m_pSet[1];
		case 2: return m_pSet[2];
		case 3: return m_pSet[3];
		case 4: return m_pDev;
		}

	return NULL;
	}

CString CCloudServiceCrimson::GetSiteIdent(void) const
{
	return GetItemData(m_pIdent, "");
	}

// Operations

void CCloudServiceCrimson::SetStatus(UINT uCode)
{
	if( m_pStatus ) {

		m_pStatus->SetValue(uCode, typeInteger, flagNone);
		}
	}

// Task Entries

void CCloudServiceCrimson::TaskInit(UINT uID)
{
	if( uID == 1 ) {

		for( UINT n = 0; n < elements(m_pSet); n++ ) {

			m_pSet[n]->Init();
			}

		m_pDev->Init();
		
		m_fExec = FALSE;
		}
	}

void CCloudServiceCrimson::TaskExec(UINT uID)
{
	if( uID == 1 ) {

		// MQTT comms is middle priority.

		if( m_pIdent ) {

			m_pIdent->SetScan(scanTrue);
			}

		if( m_pStatus ) {

			m_pStatus->SetScan(scanTrue);
			}

		if( m_pClient->Open() ) {

			for(;;) {

				if( !m_pClient->Poll(0) ) {

					Sleep(10);
					}

				if( !m_fExec ) {

					m_fExec = TRUE;
					}
				}
			}
		}
	else {
		while( !m_fExec ) {

			Sleep(10);
			}

		for(;;) {

			// Logger is highest priority.

			if( uID == 2 && !m_pClient->Poll(1) ) {

				Sleep(50);
				}

			// Saver is lowest priority.

			if( uID == 0 && !m_pClient->Poll(2) ) {

				Sleep(50);
				}
			}
		}
	}

void CCloudServiceCrimson::TaskStop(UINT uID)
{
	}

void CCloudServiceCrimson::TaskTerm(UINT uID)
{
	}

// Implementation

BOOL CCloudServiceCrimson::CheckHistory(void)
{
	if( m_pOpts->m_uBuffer ) {

		UINT n = 0;

		for( UINT s = 0; s < 5; s++ ) {

			CCloudDataSet *pSet = GetDataSet(s);

			if( pSet->m_History ) {

				n++;
				}
			}

		if( n == 0 ) {

			m_pOpts->m_uBuffer = 0;

			return FALSE;
			}

		return TRUE;
		}

	for( UINT s = 0; s < 5; s++ ) {

		CCloudDataSet *pSet = GetDataSet(s);

		pSet->m_History = 0;
		}

	return FALSE;
	}

// End of File
