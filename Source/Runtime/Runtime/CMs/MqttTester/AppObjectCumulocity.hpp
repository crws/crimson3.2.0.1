
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_APPOBJECT_CUMULOCITY_HPP

#define INCLUDE_APPOBJECT_CUMULOCITY_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "AppObject.hpp"

#include "ConfigBlob.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cumulocity Service
//

extern IService * Create_CloudServiceCumulocity(void);

//////////////////////////////////////////////////////////////////////////
//
// Service Creation
//

void CAppObject::CreateService(void)
{
	m_pService = Create_CloudServiceCumulocity();
	}

void CAppObject::CreateBlob(CConfigBlob &Blob)
{
	// CloudServiceCumulocity

	Blob.AddWord(0x1234);				// Marker
	Blob.AddCode(C3INT(1));				// Enable
	Blob.AddByte(0);				// Service
	Blob.AddText("mikegranby.cumulocity.com");	// Host
	Blob.AddText("CrimsonDevice3");			// Device
	Blob.AddText("Node3");				// Node
	Blob.AddByte(0);				// AutoCred
	Blob.AddText("mikeg@mikeg.net");		// User
	Blob.AddText("kd8kGdgW1w");			// Pass
	Blob.AddWord(10);				// Scan
	Blob.AddWord(1);				// Batch
	Blob.AddByte(1);				// Close

	// HttpClientOptions

	Blob.AddWord(0x1234);		// Marker
	Blob.AddByte(1);		// fTls
	Blob.AddWord(443);		// uPort
	Blob.AddWord(30);		// SendTimeout
	Blob.AddWord(30);		// RecvTimeout
	Blob.AddLong(0);		// ClientCert
	Blob.AddByte(0);		// Check
	Blob.AddWord(30);		// ConnTimeout
	Blob.AddByte(0);		// CompRequest
	Blob.AddByte(0);		// CompReply
	Blob.AddByte(11);		// Ver
	}

// End of File

#endif
