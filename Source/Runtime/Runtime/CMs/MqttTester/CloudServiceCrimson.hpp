
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudServiceCrimson_HPP

#define	INCLUDE_CloudServiceCrimson_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCloudDataSet;

class CCloudDeviceDataSet;

class CCloudTagSet;

class CMqttClientCrimson;

class CMqttClientOptionsCrimson;

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Service
//

class CCloudServiceCrimson : public CServiceItem, public ITaskEntry
{
	public:
		// Constructor
		CCloudServiceCrimson(void);

		// Destructor
		~CCloudServiceCrimson(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Task List
		void GetTaskList(CTaskList &List);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Attributes
		CCloudDataSet * GetDataSet(UINT n) const;
		CString         GetSiteIdent(void) const;

		// Operations
		void SetStatus(UINT uCode);

		// Item Properties
		CCodedItem          * m_pEnable;
		UINT		      m_uService;
		CCodedItem          * m_pIdent;
		CCodedItem          * m_pStatus;
		CCloudDeviceDataSet * m_pDev;
		CCloudTagSet        * m_pSet[4];

	protected:
		// Data Members
		CString			    m_Name;
		BOOL		            m_fExec;
		CMqttClientCrimson        * m_pClient;
		CMqttClientOptionsCrimson * m_pOpts;

		// Implementation
		BOOL CheckHistory(void);
	};

// End of File

#endif
