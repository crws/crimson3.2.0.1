
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_TagInteger_HPP

#define INCLUDE_TagInteger_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TagNumeric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Integer Tag
//

class CTagInteger : public CTagNumeric
{
	public:
		// Constructor
		CTagInteger(CString Name, C3INT Initial, C3INT Deadband, UINT msUpdate);

		// Evaluation
		DWORD GetData(CDataRef const &Ref, UINT Type, UINT Flags);

	protected:
		// Data Members
		UINT m_msUpdate;
		UINT m_msLast;
	};

// End of File

#endif
