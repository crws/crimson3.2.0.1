
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_TagReal_HPP

#define INCLUDE_TagReal_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TagNumeric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Real Tag
//

class CTagReal : public CTagNumeric
{
	public:
		// Constructor
		CTagReal(CString Name, C3REAL Initial, C3REAL Deadband, UINT msUpdate);

		// Evaluation
		DWORD GetData(CDataRef const &Ref, UINT Type, UINT Flags);

	protected:
		// Data Members
		UINT m_msUpdate;
		UINT m_msLast;
	};

// End of File

#endif
