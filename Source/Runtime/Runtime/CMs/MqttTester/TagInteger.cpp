
#include "Intern.hpp"

#include "TagInteger.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson Integer Tag
//

// Constructor

CTagInteger::CTagInteger(CString Name, C3INT Initial, C3INT Deadband, UINT msUpdate) : CTagNumeric(typeInteger)
{
	m_Name     = Name;

	m_Desc     = L"The tag named " + m_Name;

	m_pData[0] = DWORD(Initial);

	m_Dead     = DWORD(Deadband);

	m_msUpdate = msUpdate;

	m_msLast   = ToTime(GetTickCount());
	}

// Evaluation

DWORD CTagInteger::GetData(CDataRef const &Ref, UINT Type, UINT Flags)
{
	if( m_msUpdate ) {

		UINT msNow = ToTime(GetTickCount());

		while( msNow >= m_msLast + m_msUpdate ) {

			for( UINT n = 0; n < m_uSize; n++ ) {

				((C3INT &) m_pData[n]) += C3INT(1);
				}

			m_msLast += m_msUpdate;
			}
		}

	return CTagNumeric::GetData(Ref, Type, Flags);
	}

// End of File
