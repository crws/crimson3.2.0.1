
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClientJson_HPP

#define	INCLUDE_MqttClientJson_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCloudServiceCrimson;

class CMqttClientOptionsJson;

class CCloudDataSet;

//////////////////////////////////////////////////////////////////////////
//
// Crimson JSON MQTT Client
//

class CMqttClientJson : public CMqttClientCrimson
{
	public:
		// Constructor
		CMqttClientJson(CCloudServiceCrimson *pService, CMqttClientOptionsJson &Opts);

	protected:
		// Data Members
		CMqttClientOptionsJson & m_Opts;

		// Layout
		CString m_ObjTop;
		CString m_ObjRep;
		CString m_ObjDes;

		// Client Hooks
		void OnClientPhaseChange(void);
		BOOL OnClientDataSent(CMqttMessage const *pMsg);

		// Publish Hook
		BOOL GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg);

		// Read Helper
		BOOL OnRead(CCloudDataSet *pSet, UINT64 uTime, BOOL fTemp, UINT uModem, CJsonData *pRep, CJsonData *pDes);

		// Write Helper
		BOOL OnWrite(CJsonData const &Json, CString Child);

		// Implementation
		CString GetWillData(void);
		void    ShowSentData(CMqttMessage const *pMsg);
	};

// End of File

#endif
