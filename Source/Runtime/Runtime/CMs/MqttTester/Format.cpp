
#include "Intern.hpp"

#include "Format.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Display Format
//

// NAN Tests

#define IsNAN(r) isnanf(r)

#define IsINF(r) isinff(r)

// General Formatting

CUnicode CDispFormat::GeneralFormat(DWORD Data, UINT Type, UINT Flags)
{
	if( !(Flags & fmtUnits) ) {

		if( Type ) {

			CUnicode Text;

			if( Type == typeInteger ) {

				char s[32];

				SPrintf(s, "%d", C3INT(Data));

				Text = s;
				}

			if( Type == typeReal ) {

				C3REAL r = I2R(Data);

				if( IsNAN(r) ) {

					return L"NAN";
					}

				if( IsINF(r) ) {

					if( r > 0 ) {

						return L"+INF";
						}

					return L"-INF";
					}

				char s[32];

				PTXT w = s;

				if( r < 0 ) {

					// NOTE : gcvt is not reliable between platforms as
					// to how it treats negative numbers, so we do the
					// conversion ourselves to maintain the same ouptut.

					r    = -r;

					*w++ = '-';
					}

				gcvt(r, 5, w);

				char *p = strchr(w, 'e');

				if( p ) {

					p[0] = 'E';

					if( p[2] == '0' ) {

						if( p[3] == '0' ) {

							p[2] = p[4];
							p[3] = 0;
							}
						else {
							p[2] = p[3];
							p[3] = p[4];
							p[4] = 0;
							}
						}

					if( *--p == '.' ) {

						memmove(p, p+1, strlen(p));
						}
					}
				else {
					int n = strlen(s);

					if( n ) {
						
						if( s[n-1] == '.' ) {

							s[n-1] = 0;
							}
						}
					}

				Text = s;
				}

			if( Type == typeString ) {

				Text = Data ? PCUTF(Data) : L"";

				return Text;
				}

			if( Flags & fmtPad ) {

				if( !(Flags & fmtANSI) ) {

					MakeDigitsFixed(Text);
					}
				}

			return Text;
			}

		return L"---";
		}

	return L"";
	}

// General Parsing

BOOL CDispFormat::GeneralParse(DWORD &Data, CString Text, UINT Type)
{
	if( Type == typeInteger ) {

		Data = strtol(Text, NULL, 10);

		return TRUE;
		}

	if( Type == typeReal ) {

		Data = R2I(C3REAL(strtod(Text, NULL)));

		return TRUE;
		}

	if( Type == typeString ) {

		Data = DWORD(wstrdup(UniConvert(Text)));

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CDispFormat::MakeDigitsFixed(CUnicode &Text)
{
	MakeDigitsFixed(PUTF(PCUTF(Text)));
	}

void CDispFormat::MakeLettersFixed(CUnicode &Text)
{
	MakeLettersFixed(PUTF(PCUTF(Text)));
	}

void CDispFormat::MakeDigitsFixed(PUTF pText)
{
	while( *pText ) {

		if( *pText >= digitSimple && *pText < digitSimple + 10 ) {

			*pText -= digitSimple;

			*pText += digitFixed;
			}

		pText++;
		}
	}

void CDispFormat::MakeLettersFixed(PUTF pText)
{
	while( *pText ) {

		if( *pText >= letterSimple && *pText < letterSimple + 6 ) {

			*pText -= letterSimple;

			*pText += letterFixed;
			}

		pText++;
		}
	}

// End of File
