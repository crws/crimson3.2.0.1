
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_TagList_HPP

#define INCLUDE_TagList_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTag;

//////////////////////////////////////////////////////////////////////////
//
// Crimson Tag List
//

class CTagList
{
	public:
		// Constructor
		CTagList(void);

		// Destructor
		~CTagList(void);

		// Attributes
		CTag * GetItem(UINT uTag) const;

		// Data Members
		CArray <CTag *> m_List;
	};

// End of File

#endif
