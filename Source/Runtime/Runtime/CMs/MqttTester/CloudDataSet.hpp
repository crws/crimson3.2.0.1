
#include "Intern.hpp"

#include "service.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudDataSet_HPP

#define	INCLUDE_CloudDataSet_HPP

//////////////////////////////////////////////////////////////////////////
//
// Cloud Data Set
//

class CCloudDataSet : public CCodedHost
{
	public:
		// Constructor
		CCloudDataSet(void);

		// Destructor
		~CCloudDataSet(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsEnabled(void) const;
		BOOL UseHistory(void) const;
		UINT GetPeriod(void) const;
		BOOL IsTriggered(void);

		// Operations
		void Tick(void);

		// Json Formatting
		virtual BOOL GetJson(CJsonData *pRep, CJsonData *pDes, CString Ident, BOOL fRoot, UINT uMode) = 0;

		// List Formatting
		virtual UINT GetCount(void) = 0;
		virtual UINT GetProps(void) = 0;
		virtual BOOL GetData (UINT uIndex, CString &Name, DWORD &Data, UINT &Type, BOOL &Free, BOOL fLive) = 0;
		virtual BOOL GetProp (UINT uIndex, UINT uProp, CString &Name, DWORD &Data, UINT &Type) = 0;
		virtual BOOL SetData (UINT uIndex, DWORD Data, UINT Type) = 0;

		// Operations
		virtual void Init(void);
		virtual void ResetHistory(void);
		virtual void DataWasSent(void);

		// Data Members
		UINT		m_Mode;
		UINT		m_History;
		UINT		m_Scan;
		UINT		m_Force;
		CCodedItem    * m_pReq;
		CCodedItem    * m_pAck;

		// Modes
		enum
		{
			modeDelta = 0,
			modeIfAny = 1,
			modeForce = 2,
			};

	protected:
		// Data Members
		BOOL m_fAck;
		UINT m_Timer;

		// Implementation
		void SetAck(BOOL fAck);
	};

// End of File

#endif
