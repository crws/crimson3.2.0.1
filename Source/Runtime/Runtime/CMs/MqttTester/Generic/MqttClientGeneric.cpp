
#include "Intern.hpp"

#include "Service.hpp"

#include "Matrix.hpp"

#include "MqttClientGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudServiceGeneric.hpp"

#include "MqttClientOptionsGeneric.hpp"

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client
//

// Constructor

CMqttClientGeneric::CMqttClientGeneric(CCloudServiceGeneric *pService, CMqttClientOptionsGeneric &Opts) : CMqttClientJson(pService, Opts), m_Opts(Opts)
{
	m_Will.SetTopic(m_Opts.m_PubTopic);

	m_Will.SetData (GetWillData());
	}

// Client Hooks

void CMqttClientGeneric::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseInitial ) {

		if( HasWrites() ) {

			AddToSubList(subData, m_Opts.m_SubTopic);
			}
		}

	CMqttClientJson::OnClientPhaseChange();
	}

BOOL CMqttClientGeneric::OnClientNewData(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode == subData ) {

		CJsonData Json;

		if( pMsg->GetJson(Json) ) {

			OnWrite(Json, "");
			}

		return TRUE;
		}

	return TRUE;
	}

// Publish Hook

BOOL CMqttClientGeneric::GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg)
{
	if( FALSE ) {

		uMode = CCloudDataSet::modeForce;
		}

	if( CMqttClientJson::GetPubMessage(uTopic, uTime, fTemp, uMode, pMsg) ) {

		pMsg->SetTopic(m_Opts.m_PubTopic);

		return TRUE;
		}

	return FALSE;
	}

// End of File
