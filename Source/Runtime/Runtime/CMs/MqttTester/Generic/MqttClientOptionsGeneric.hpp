
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqqtClientOptionsGeneric_HPP

#define	INCLUDE_MqqtClientOptionsGeneric_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientOptionsJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client Options
//

class CMqttClientOptionsGeneric : public CMqttClientOptionsJson
{
	public:
		// Constructor
		CMqttClientOptionsGeneric(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		CString m_PubTopic;
		CString m_SubTopic;
		UINT    m_NoDollar;

	protected:
		// Implementation
		void SetDefaults(void);
	};

// End of File

#endif
