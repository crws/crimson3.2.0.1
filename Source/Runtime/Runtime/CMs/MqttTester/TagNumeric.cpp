
#include "Intern.hpp"

#include "TagNumeric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson Numeric Tag
//

// Constructor

CTagNumeric::CTagNumeric(UINT Type)
{
	m_uType  = Type;

	m_uSize  = 0;

	m_pData  = NULL;

	InitData();
	}

// Destructor

CTagNumeric::~CTagNumeric(void)
{
	delete [] m_pData;
	}

// Attributes

UINT CTagNumeric::GetDataType(void) const
{
	return m_uType;
	}

// Evaluation

BOOL CTagNumeric::IsAvail(CDataRef const &Ref, UINT Flags)
{
	if( LocalData() ) {

		return TRUE;
		}

	return CDataTag::IsAvail(Ref, 0);
	}

BOOL CTagNumeric::SetScan(CDataRef const &Ref, UINT Code)
{
	if( LocalData() ) {

		return TRUE;
		}

	return CDataTag::SetScan(Ref, Code);
	}

DWORD CTagNumeric::GetData(CDataRef const &Ref, UINT Type, UINT Flags)
{
	if( Type ) {

		if( Type == typeReal && IsInteger(m_uType) ) {

			DWORD Data = GetData(Ref, typeInteger, Flags);

			return R2I(C3REAL(C3INT(Data)));
			}

		if( m_uType == typeReal && IsInteger(Type) ) {

			DWORD Data = GetData(Ref, typeReal, Flags);

			return C3INT(I2R(Data));
			}
		}

	if( Type == m_uType || Type == typeVoid ) {

		UINT n = Ref.x.m_Array;

		if( n < m_uSize ) {

			if( LocalData() ) {

				DWORD Data = m_pData[n];

				TransToDisp(Data, Flags, n);

				return Data;
				}

			DWORD Data = CDataTag::GetData(Ref, Type, Flags);

			TransToDisp(Data, Flags, n);

			return Data;
			}
		}

	return GetNull(Type);
	}

BOOL CTagNumeric::IsValid(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	if( Type == m_uType ) {

		if( (Flags & getMaskSource) <= getScaled ) {

			DWORD Min = FindMin(Ref, Type);

			DWORD Max = FindMax(Ref, Type);

			if( Type == typeInteger ) {

				if( C3INT(Data) < C3INT(Min) ) {

					return FALSE;
					}

				if( C3INT(Data) > C3INT(Max) ) {

					return FALSE;
					}

				return TRUE;
				}

			if( Type == typeReal ) {

				if( I2R(Data) < I2R(Min) ) {

					return FALSE;
					}

				if( I2R(Data) > I2R(Max) ) {

					return FALSE;
					}

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagNumeric::SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	if( Type ) {

		if( m_uType == typeReal && IsInteger(Type) ) {

			Data = R2I(C3REAL(C3INT(Data)));

			Type = m_uType;
			}

		if( Type == typeReal && IsInteger(m_uType) ) {

			Data = C3INT(I2R(Data));

			Type = m_uType;
			}
		}

	if( Type == m_uType || Type == typeVoid ) {

		UINT n = Ref.x.m_Array;

		if( n < m_uSize ) {

			if( !(Flags & setForce) ) {

				DWORD Read = GetData(Ref, Type, Flags);

				if( Read == Data ) {

					return TRUE;
					}
				}

			if( !AllowWrite(Ref, Data, Type) ) {

				return FALSE;
				}

			if( TRUE ) {

				TransToData(Data, Flags, n);
				}

			if( LocalData() ) {

				m_pData[n] = Data;

				return TRUE;
				}

			return FALSE;
			}
		}

	return CDataTag::SetData(Ref, Data, Flags, Type);
	}

DWORD CTagNumeric::GetProp(CDataRef const &Ref, WORD ID, UINT Type)
{
	switch( ID ) {

		case tpPrefix:
			return FindPrefix(Ref);

		case tpUnits:
			return FindUnits(Ref);

		case tpSetPoint:
			return FindSP (Ref, Type ? Type : GetDataType());

		case tpMinimum:
			return FindMin(Ref, Type ? Type : GetDataType());

		case tpMaximum:
			return FindMax(Ref, Type ? Type : GetDataType());

		case tpAlarms:
			return FindEventStatusMask(Ref);
		}

	if( ID == tpStateCount ) {

		return FindMax(Ref, typeInteger) + 1;
		}

	if( ID >= tpStateText && ID < tpStateText + 1000 ) {

		return FindAsText(Ref, typeInteger, ID - tpStateText);
		}

	return CDataTag::GetProp(Ref, ID, Type);
	}

// Deadband

BOOL CTagNumeric::HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev)
{
	if( Prev == 0x7FFFFFFF ) {

		Data = GetData(Ref, m_uType, getNone);

		return TRUE;
		}
	else {
		if( m_uType == typeReal ) {

			C3REAL Read = I2R(GetData(Ref, typeReal, getNone));

			C3REAL Last = I2R(Prev);

			C3REAL Dead = I2R(FindDeadband(Ref, typeReal));

			if( Dead ? (fabs(Read - Last) >= Dead) : (Read != Last) ) {

				Data = R2I(Read);

				return TRUE;
				}

			return FALSE;
			}

		if( m_uType == typeInteger ) {

			C3INT Read = GetData(Ref, typeInteger, getNone);

			C3INT Last = C3INT(Prev);

			C3INT Dead = FindDeadband(Ref, typeInteger);

			if( Dead ? (abs(Read - Last) >= Dead) : (Read != Last) ) {

				Data = Read;

				return TRUE;
				}

			return FALSE;
			}
		}

	return CTag::HasChanged(Ref, Data, Prev);
	}

// Property Access

DWORD CTagNumeric::FindPrefix(CDataRef const &Ref)
{
	return DWORD(wstrdup(L""));
	}

DWORD CTagNumeric::FindUnits(CDataRef const &Ref)
{
	return DWORD(wstrdup(L"units"));
	}

DWORD CTagNumeric::FindSP(CDataRef const &Ref, UINT Type)
{
	return GetNull(Type);
	}

DWORD CTagNumeric::FindMin(CDataRef const &Ref, UINT Type)
{
	if( Type == typeInteger ) {

		return 0;
		}

	return R2I(0);
	}

DWORD CTagNumeric::FindMax(CDataRef const &Ref, UINT Type)
{
	if( Type == typeInteger ) {

		return 999999;
		}

	return R2I(999999);
	}

DWORD CTagNumeric::FindDeadband(CDataRef const &Ref, UINT Type)
{
	return m_Dead;
	}

DWORD CTagNumeric::FindEventStatusMask(CDataRef const &Ref)
{
	return 0;
	}

// Implementation

BOOL CTagNumeric::InitData(void)
{
	if( LocalData() ) {

		m_uSize = max(m_Extent, 1);

		m_pData = New DWORD [ m_uSize ];

		if( GetDataType() == typeInteger ) {

			for( UINT n = 0; n < m_uSize; n++ ) {

				m_pData[n] = 0;
				}

			m_Dead = 0;

			return TRUE;
			}

		if( GetDataType() == typeReal ) {

			for( UINT n = 0; n < m_uSize; n++ ) {

				m_pData[n] = R2I(0.0);
				}

			m_Dead = R2I(0);

			return TRUE;
			}

		AfxAssert(FALSE);

		return FALSE;
		}

	m_uSize = max(m_Extent, 1);

	return FALSE;
	}

void CTagNumeric::TransToDisp(DWORD &Data, UINT Flags, UINT uPos)
{
	if( (Flags & getMaskSource) <= getManipulated ) {

		Manipulate(Data, TRUE);
		}

	if( (Flags & getMaskSource) <= getScaled ) {

		ScaleToDisp(Data, uPos);
		}
	}
						
void CTagNumeric::TransToData(DWORD &Data, UINT Flags, UINT uPos)
{
	if( (Flags & setMaskSource) <= setScaled ) {

		ScaleToData(Data, uPos);
		}

	if( (Flags & setMaskSource) <= setManipulated ) {

		Manipulate(Data, FALSE);
		}
	}

void CTagNumeric::Manipulate(DWORD &Data, BOOL fGet)
{
	}

void CTagNumeric::ScaleToDisp(DWORD &Data, UINT uPos)
{
	}

void CTagNumeric::ScaleToData(DWORD &Data, UINT uPos)
{
	}

// End of File
