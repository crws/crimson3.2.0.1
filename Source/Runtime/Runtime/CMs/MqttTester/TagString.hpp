
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_TagString_HPP

#define INCLUDE_TagString_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DataTag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson String Tag
//

class CTagString : public CDataTag
{
	public:
		// Constructor
		CTagString(CString Name, CUnicode Initial, UINT msUpdate);

		// Destructor
		~CTagString(void);

		// Attributes
		UINT GetDataType(void) const;

		// Evaluation
		BOOL  IsAvail(CDataRef const &Ref, UINT Flags);
		BOOL  SetScan(CDataRef const &Ref, UINT Code);
		DWORD GetData(CDataRef const &Ref, UINT Type, UINT Flags);
		BOOL  SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		DWORD GetProp(CDataRef const &Ref, WORD ID, UINT Type);

		// Deadband
		void InitPrevious(DWORD &Prev);
		BOOL HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev);
		void KillPrevious(DWORD &Prev);

	protected:
		// Data Members
		UINT       m_uSize;
		CUnicode * m_pData;
		CUnicode   m_Initial;
		UINT       m_msUpdate;
		UINT       m_msLast;

		// Implementation
		BOOL InitData(void);
	};

// End of File

#endif
