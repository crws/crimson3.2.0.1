
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_TagNumeric_HPP

#define INCLUDE_TagNumeric_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DataTag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Numeric Tag
//

class CTagNumeric : public CDataTag
{
	public:
		// Constructor
		CTagNumeric(UINT Type);

		// Destructor
		~CTagNumeric(void);

		// Attributes
		UINT GetDataType(void) const;

		// Evaluation
		BOOL  IsAvail(CDataRef const &Ref, UINT Flags);
		BOOL  SetScan(CDataRef const &Ref, UINT Code);
		DWORD GetData(CDataRef const &Ref, UINT Type, UINT Flags);
		BOOL  IsValid(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		BOOL  SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		DWORD GetProp(CDataRef const &Ref, WORD ID, UINT Type);

		// Deadband
		BOOL HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev);

	protected:
		// Data Members
		UINT    m_uType;
		UINT    m_uSize;
		DWORD * m_pData;
		DWORD   m_Dead;

		// Property Location
		DWORD FindPrefix(CDataRef const &Ref);
		DWORD FindUnits(CDataRef const &Ref);
		DWORD FindSP(CDataRef const &Ref, UINT Type);
		DWORD FindMin(CDataRef const &Ref, UINT Type);
		DWORD FindMax(CDataRef const &Ref, UINT Type);
		DWORD FindDeadband(CDataRef const &Ref, UINT Type);
		DWORD FindEventStatusMask(CDataRef const &Ref);

		// Implementation
		BOOL InitData(void);
		void TransToDisp(DWORD &Data, UINT Flags, UINT uPos);
		void TransToData(DWORD &Data, UINT Flags, UINT uPos);
		void Manipulate (DWORD &Data, BOOL fGet);
		void ScaleToDisp(DWORD &Data, UINT uPos);
		void ScaleToData(DWORD &Data, UINT uPos);
	};

// End of File

#endif
