
#include "Intern.hpp"

#include "CloudDeviceDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cloud Data Set
//

// Constructor

CCloudDeviceDataSet::CCloudDeviceDataSet(void)
{
	m_Gps  = 1;

	m_Cell = 1;
	}

// Destructor

CCloudDeviceDataSet::~CCloudDeviceDataSet(void)
{
	}

// Initialization

void CCloudDeviceDataSet::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudDeviceDataSet", pData);

	CCloudDataSet::Load(pData);

	m_Gps  = GetByte(pData);

	m_Cell = GetByte(pData);
	}

// Json Formatting

BOOL CCloudDeviceDataSet::GetJson(CJsonData *pRep, CJsonData *pDes, CString Ident, BOOL fRoot, UINT uMode)
{
	if( !fRoot ) {

		CJsonData *pDevice;

		pRep->AddChild("device", FALSE, pDevice);

		pDevice->AddValue("status", "okay");

		if( Ident.GetLength() ) {

			pDevice->AddValue("cid", Ident);
			}

		if( m_Gps ) {

			CJsonData *pGps;

			pDevice->AddChild("location", FALSE, pGps);

			pGps->AddValue("valid", AddGpsData(pGps) ? "true" : "false");
			}

		if( m_Cell ) {

			CJsonData *pCell;

			pDevice->AddChild("cellular", FALSE, pCell);

			pCell->AddValue("valid", AddCellData(pCell) ? "true" : "false");
			}
		}

	return TRUE;
	}

// List Formatting

UINT CCloudDeviceDataSet::GetCount(void)
{
	UINT uCount = 1;

	if( m_Gps  ) uCount += 1;

	if( m_Cell ) uCount += 1;

	return uCount;
	}

UINT CCloudDeviceDataSet::GetProps(void)
{
	return 0;
	}

BOOL CCloudDeviceDataSet::GetData(UINT uIndex, CString &Name, DWORD &Data, UINT &Type, BOOL &Free, BOOL fLive)
{
	if( uIndex < 1 ) {

		if( uIndex == 0 ) {

			Name = "Status";

			Data = DWORD(wstrdup(L"okay"));

			Type = typeString;

			Free = TRUE;

			return TRUE;
			}
		}
	else
		uIndex -= 1;

	if( m_Gps ) {

		if( uIndex < 1 ) {

			if( uIndex == 0 ) {

				Name = "Location/Valid";

				Data = DWORD(wstrdup(L"false"));

				Type = typeString;

				Free = TRUE;

				return TRUE;
				}
			}
		else
			uIndex -= 1;
		}

	if( m_Cell ) {

		if( uIndex < 1 ) {

			if( uIndex == 0 ) {

				Name = "Cellular/Valid";

				Data = DWORD(wstrdup(L"false"));

				Type = typeString;

				Free = TRUE;

				return TRUE;
				}
			}
		else
			uIndex -= 1;
		}

	return FALSE;
	}

BOOL CCloudDeviceDataSet::GetProp(UINT uIndex, UINT uProp, CString &Name, DWORD &Data, UINT &Type)
{
	return FALSE;
	}

BOOL CCloudDeviceDataSet::SetData(UINT uIndex, DWORD Data, UINT Type)
{
	return TRUE;
	}

// Implementation

BOOL CCloudDeviceDataSet::AddGpsData(CJsonData *pGps)
{
	return FALSE;
	}

BOOL CCloudDeviceDataSet::AddCellData(CJsonData *pCell)
{
	return FALSE;
	}

// End of File
