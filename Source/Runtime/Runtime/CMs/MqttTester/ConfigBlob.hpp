
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ConfigBlob_HPP

#define INCLUDE_ConfigBlob_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Configuration Blob
//

class CConfigBlob
{
	public:
		// Constructor
		CConfigBlob(void);

		// Attributes
		PCBYTE GetData(void) const;

		// Operations
		void AddByte(BYTE   Data);
		void AddWord(WORD   Data);
		void AddLong(LONG   Data);
		void AddAddr(IPREF  Data);
		void AddText(PCTXT  pData);
		void AddText(PCUTF  pData);
		void AddData(PCBYTE pData, UINT uSize);
		void AddFile(PCBYTE pData, UINT uSize);
		void AddCode(C3INT  Data);
		void AddCode(C3REAL Data);
		void AddCode(PCTXT  pData);
		void AddCode(PCUTF  pData);
		void AddNull(void);

	protected:
		// Data Members
		CByteArray m_Data;

		// Implementation
		void Align2(void);
		void Align4(void);
	};

// End of File

#endif
