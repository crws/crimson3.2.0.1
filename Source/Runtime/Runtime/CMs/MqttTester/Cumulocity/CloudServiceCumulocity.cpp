  
#include "Intern.hpp"

#include "Service.hpp"

#include "CloudServiceCumulocity.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Aeon Stubs
//

#if defined(AEON_ENVIRONMENT)

PCTXT WhoGetName(BOOL)
{
	return "AeonHost";
	}

CString AfxFindMac(void)
{
	return "11-22-33-44-55-66";
	}

void AfxTagCommit(void)
{
	}

#endif

//////////////////////////////////////////////////////////////////////////
//
// Crimson Stubs
//

#if defined(INCLUDE_C3CORE_HPP)

CString AfxFindMac(void)
{
	char s[32] = {0};

	CMACAddr Mac;

	NICGetDriver(0)->ReadMAC(Mac);

	Mac.AppendAsText(s);

	return s;
	}

void AfxTagCommit(void)
{
	CCommsSystem::m_pThis->m_pTags->Commit();
	}

#endif

//////////////////////////////////////////////////////////////////////////
//
// Cloud Manager
//

// Instantiator

IService * Create_CloudServiceCumulocity(void)
{
	return New CCloudServiceCumulocity;
	}

// Constructor

CCloudServiceCumulocity::CCloudServiceCumulocity(void)
{
	m_pEnable    = NULL;
	
	m_uTags      = 0;
	
	m_pTags      = NULL;
	
	m_pType      = NULL;

	m_pSrc       = afxTagList;

	m_pCredTag   = NULL;

	m_pConnect   = NULL;

	m_pSave      = Create_ManualEvent();
	
	m_pData	     = NULL;

	m_pTime	     = NULL;
	}

// Destructor

CCloudServiceCumulocity::~CCloudServiceCumulocity(void)
{
	m_pSave->Release();

	delete m_pCredTag;

	delete m_pEnable;

	delete [] m_pData;

	delete [] m_pTime;

	delete [] m_pTags;

	delete [] m_pType;
	}

// Initialization

void CCloudServiceCumulocity::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudServiceCumulocity", pData);

	GetCoded(pData, m_pEnable);

	m_Cfg.m_uService = GetByte(pData);
	
	m_Cfg.m_Host     = UniConvert(GetWide(pData));

	m_Cfg.m_Device   = UniConvert(GetWide(pData));
	
	m_Cfg.m_Node     = UniConvert(GetWide(pData));
	
	if( !GetByte(pData) ) {

		m_Cfg.m_User = UniConvert(GetWide(pData));
	
		m_Cfg.m_Pass = UniConvert(GetWide(pData));
		}
	else
		GetCoded(pData, m_pCredTag);

	m_Cfg.m_uScan  = GetWord(pData);

	m_Cfg.m_uBatch = GetWord(pData);
	
	m_Cfg.m_fClose = GetByte(pData);

	m_Cfg.m_Opts.Load(pData);

	LoadTagList(pData);

	AllocDataBuffer();
	}

// Service ID

UINT CCloudServiceCumulocity::GetID(void)
{
	return 8;
	}

// Task List

void CCloudServiceCumulocity::GetTaskList(CTaskList &List)
{
	m_Cfg.m_fEnable = GetItemData(m_pEnable, C3INT(0));

	if( m_Cfg.m_fEnable ) {

		CTaskDef Task;

		Task.m_Name    = "CUMULOCITY";
		Task.m_pEntry  = this;
		Task.m_uID     = 0;
		Task.m_uCount  = 2;
		Task.m_uLevel  = 2100;
		Task.m_uStack  = 4096;

		Task.m_uLevel += 2 * (GetID() - 8);

		List.Append(Task);
		}
	}

// Task Entries

void CCloudServiceCumulocity::TaskInit(UINT uID)
{
	if( uID == 0 ) {

		FindMac();

		m_Make   = "RedLion";

		m_Model  = WhoGetName(TRUE);

		m_Model.MakeUpper();

		m_Serial = CPrintf("1001-%s-0001", PCTXT(m_Mac.Mid(6)));

		m_Frag   = CPrintf("%s.%s.Tags",   PCTXT(m_Make), PCTXT(m_Cfg.m_Node));

		m_Name   = m_Cfg.m_Device.IsEmpty() ? m_Make + " " + m_Model + " " + m_Serial : m_Cfg.m_Device;

		InitTagList();
		}
	}

void CCloudServiceCumulocity::TaskExec(UINT uID)
{
	if( uID == 0 ) {

		CHttpClientManager *pManager = New CHttpClientManager;

		if( pManager->Open(NULL, 0, NULL) ) {

			UINT uWait = 1;

			m_pConnect = pManager->CreateConnection(m_Cfg.m_Opts);

			for(;;) {

				if( !GetCredentials() ) {

					Sleep(1000 * uWait);

					uWait = uWait << ((uWait < 64) ? 1 : 0);

					continue;
					}

				uWait = 1;

				break;
				}

			if( m_pConnect->SetServer(m_Cfg.m_Host, m_Cfg.m_User, m_Cfg.m_Pass) ) {

				for(;;) {

					if( EnsureRegistered() ) {

						uWait = 1;

						if( !m_pSave->Wait(0) && m_Cfg.m_fClose ) {

							m_pConnect->Close();
							}

						for(;;) {

							if( m_pSave->Wait(250) ) {

								m_pSave->Clear();

								// We only sent up until the current tail, which keeps
								// the batches aligned to the clock. We could send everything
								// we have, which will perform better but mean that we'll
								// send additional samples on each batch.

								if( SendMeasurements(m_uTail) ) {

									if( !m_pSave->Wait(0) && m_Cfg.m_fClose ) {
	
										m_pConnect->Close();
										}

									uWait = 1;

									continue;
									}

								m_pSave->Set();

								break;
								}

							m_pConnect->Idle();
							}
						}

					m_pConnect->Close();

					Sleep(1000 * uWait);

					uWait = uWait << ((uWait < 64) ? 1 : 0);
					}
				}
			}

		for(;;) Sleep(FOREVER);
		}

	if( uID == 1 ) {

		DWORD dwLast = 0;

		for(;;) {
			
			DWORD dwTime = GetNow();
					
			DWORD dwSlot = dwTime / m_Cfg.m_uScan;

			if( !dwLast ) {

				if( dwTime % m_Cfg.m_uScan ) {

					Sleep(100);

					continue;
					}
				}

			if( dwLast != dwSlot ) {

				TakeSample(dwTime);

				if( (dwLast = dwSlot) % m_Cfg.m_uBatch == 0 ) {

					m_pSave->Set();
					}
				}

			Sleep(500);
			}
		}
	}

void CCloudServiceCumulocity::TaskStop(UINT uID)
{
	}

void CCloudServiceCumulocity::TaskTerm(UINT uID)
{
	}

// Transactions

BOOL CCloudServiceCumulocity::GetCredentials(void)
{
	if( m_Cfg.m_User.IsEmpty() ) {

		if( ReadCredTag() ) {

			return TRUE;
			}

		if( m_pConnect->SetServer( "management.cumulocity.com",
					   "management/devicebootstrap",
					   "Fhdt1bb1f") ) {

			CRestCall Rest( "POST",
					"/devicecontrol/deviceCredentials",
					"application/vnd.com.nsn.cumulocity.deviceCredentials+json",
					""
					);

			CJsonData *pJson = Rest.GetJsonReq();

			pJson->AddValue("id", "RedLion-" + m_Mac, jsonString);

			if( Rest.Transact(m_pConnect, 201) ) {

				CJsonData *pJson = Rest.GetJsonRep();

				m_Cfg.m_User = pJson->GetValue("username");

				m_Cfg.m_Pass = pJson->GetValue("password");

				WriteCredTag();
			
				return TRUE;
				}
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CCloudServiceCumulocity::EnsureRegistered(void)
{
	BOOL fReply;

	if( CheckRegistered(fReply) ) {

		if( fReply ) {

			return TRUE;
			}

		if( CreateMainDevice() && RegisterDeviceMac() && RegisterDeviceSerial() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCloudServiceCumulocity::CheckRegistered(BOOL &fReply)
{
	CRestCall Rest( "GET",
			"/identity/externalIds/c8y_MAC/" + m_Mac,
			"application/vnd.com.nsn.cumulocity.externalId+json",
			"application/vnd.com.nsn.cumulocity.externalId+json"
			);

	if( Rest.Transact(m_pConnect) ) {

		if( Rest.GetStatus() == 200 ) {

			CJsonData *pJson   = Rest.GetJsonRep();

			CJsonData *pObject = pJson->GetChild("managedObject");

			if( pObject ) {

				m_MainId = pObject->GetValue("id");

				fReply   = TRUE;
			
				return TRUE;
				}
			}

		if( Rest.GetStatus() == 404 ) {

			fReply = FALSE;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCloudServiceCumulocity::CheckMainDevice(BOOL &fReply)
{
	// We get the childDevices collection as it's a quicker
	// way of checking if our main device object still exists
	// without having to read all its very many properties.

	CRestCall Rest( "GET",
			"/inventory/managedObjects/" + m_MainId + "/childDevices",
			"application/vnd.com.nsn.cumulocity.managedObject+json",
			""
			);

	if( Rest.Transact(m_pConnect) ) {

		if( Rest.GetStatus() == 200 ) {

			fReply = TRUE;

			return TRUE;
			}

		if( Rest.GetStatus() == 404 ) {

			fReply = FALSE;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCloudServiceCumulocity::CreateMainDevice(void)
{
	CRestCall Rest( "POST",
			"/inventory/managedObjects",
			"application/vnd.com.nsn.cumulocity.managedObject+json",
			"application/vnd.com.nsn.cumulocity.managedObject+json"
			);

	CJsonData *pJson = Rest.GetJsonReq();
	
	CJsonData *pSupp = NULL;

	CJsonData *pHard = NULL;

	CJsonData *pMeas = NULL;

	pJson->AddValue ("name", m_Name,       jsonString);
	
	pJson->AddValue ("type", "RedLion.C3", jsonString);
	
	pJson->AddObject("c8y_IsDevice");
	
	pJson->AddObject("com_cumulocity_model_Agent");
	
	pJson->AddList  ("c8y_SupportedOperations",   pSupp);
	
	pJson->AddObject("c8y_Hardware",              pHard);

	pJson->AddList  ("c8y_SupportedMeasurements", pMeas);

	pSupp->AddMember("c8y_Restart");

	pHard->AddValue ("model",       m_Make + " " + m_Model, jsonString);
	
	pHard->AddValue ("revision",    "0001",                 jsonString);
	
	pHard->AddValue ("serialNumber", m_Serial,              jsonString);
	
	pMeas->AddMember(m_Frag);

	if( Rest.Transact(m_pConnect, 201) ) {

		CJsonData *pJson = Rest.GetJsonRep();
			
		m_MainId = pJson->GetValue("id");
	
		return TRUE;
		}

	return FALSE;
	}

BOOL CCloudServiceCumulocity::RegisterDeviceSerial(void)
{
	CRestCall Rest( "POST",
			"/identity/globalIds/" + m_MainId + "/externalIds",
			"application/vnd.com.nsn.cumulocity.externalId+json",
			""
			);

	CJsonData *pJson = Rest.GetJsonReq();

	pJson->AddValue("type",       "c8y_Serial", jsonString);

	pJson->AddValue("externalId", m_Serial,     jsonString);

	if( Rest.Transact(m_pConnect, 201) ) {

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCloudServiceCumulocity::RegisterDeviceMac(void)
{
	CRestCall Rest( "POST",
			"/identity/globalIds/" + m_MainId + "/externalIds",
			"application/vnd.com.nsn.cumulocity.externalId+json",
			""
			);

	CJsonData *pJson = Rest.GetJsonReq();

	pJson->AddValue("type",       "c8y_MAC", jsonString);

	pJson->AddValue("externalId", m_Mac,     jsonString);

	if( Rest.Transact(m_pConnect, 201) ) {

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCloudServiceCumulocity::SendMeasurements(UINT uTail)
{
	BOOL fReply;
	
	if( CheckMainDevice(fReply) ) {

		if( fReply ) {

			while( m_uHead != uTail ) {

				if( !SendMeasurement(m_uHead) ) {

					return FALSE;
					}

				FreeHeadStrings();

				m_uHead = (m_uHead + 1) % m_uSize;
				}

			if( m_Cfg.m_fClose ) {

				m_pConnect->Close();
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCloudServiceCumulocity::SendMeasurement(UINT uSlot)
{
	CRestCall Rest( "POST",
			"/measurement/measurements",
			"application/vnd.com.nsn.cumulocity.measurement+json",
			""
			);

	CJsonData *pJson = Rest.GetJsonReq();

	CJsonData *pFrom;

	CJsonData *pTags;

	pJson->AddValue ("type",   "Tags",		    jsonString);

	pJson->AddValue ("time",   GetTime(m_pTime[uSlot]), jsonString);

	pJson->AddObject("source", pFrom);

	pFrom->AddValue ("id",     m_MainId,		    jsonString);

	pJson->AddObject(m_Frag,   pTags);

	UINT s = uSlot * m_uTags;

	for( UINT n = 0; n < m_uTags; n++ ) {

		CDataRef &Ref = (CDataRef &) m_pTags[n];

		UINT     uTag = Ref.t.m_Index;

		UINT     uPos = Ref.t.m_Array;

		CTag   * pTag = m_pSrc->GetItem(uTag);

		CString  Name = UniConvert(pTag->m_Name);

		CString  Data = UniConvert(pTag->GetAsText(uPos, m_pData[s], m_pType[n], fmtBare));

		if( m_pType[n] == typeInteger || m_pType[n] == typeReal ) {

			CString Unit = UniConvert(pTag->GetAsText(uPos, fmtUnits));

			Unit.TrimBoth();

			CJsonData *pData;

			pTags->AddObject(Name, pData);

			pData->AddValue("unit",  Unit, jsonString);

			pData->AddValue("value", Data, jsonNumber);
			}

		if( FALSE && m_pType[n] == typeString ) {

			CJsonData *pData;

			pTags->AddObject(Name, pData);

			pData->AddValue("value", Data, jsonString);
			}

		s++;
		}

	if( Rest.Transact(m_pConnect, 201) ) {

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CCloudServiceCumulocity::FindMac(void)
{
	m_Mac = AfxFindMac();

	m_Mac.Remove('-');
	}

BOOL CCloudServiceCumulocity::LoadTagList(PCBYTE &pData)
{
	if( (m_uTags = GetWord(pData)) ) {

		m_pTags = New DWORD [ m_uTags ];

		m_pType = New BYTE  [ m_uTags ];

		for( UINT n = 0; n < m_uTags; n++ ) {

			m_pTags[n]  = GetLong(pData);
			}

		return TRUE;
		}

	return FALSE;
	}

void CCloudServiceCumulocity::AllocDataBuffer(void)
{
	m_uSize = max(240, 8 * m_Cfg.m_uBatch);

	m_pTime = New DWORD [ m_uSize ];

	m_pData = New DWORD [ m_uSize * m_uTags ];

	m_uHead = 0;

	m_uTail = 0;
	}

void CCloudServiceCumulocity::InitTagList(void)
{
	for( UINT n = 0; n < m_uTags; n++ ) {
		
		DWORD          Data = m_pTags[n];

		CDataRef const &Ref = (CDataRef &) Data;

		UINT           uTag = Ref.t.m_Index;
			
		CTag         * pTag = m_pSrc->GetItem(uTag);

		if( pTag ) {

			UINT uType  = pTag->GetDataType();

			m_pType[n]  = BYTE(uType);

			pTag->SetScan(Ref, scanTrue);
				
			continue;
			}

		m_pType[n] = typeVoid;

		m_pTags[n] = 0xFFFF;
		}
	}

CString CCloudServiceCumulocity::GetTime(DWORD dwTime)
{
	PCTXT Sign = "+";

	INT   Zone = GetZuluOffset() / 60;

	if( Zone < 0 ) {

		Sign = "-";

		Zone = -Zone;
		}

	return CPrintf( "20%2.2u-%2.2u-%2.2uT%2.2u:%2.2u:%2.2u.%3.3u%s%2.2d:%2.2d",
			GetYear (dwTime) % 100,
			GetMonth(dwTime),
			GetDate (dwTime),
			GetHour (dwTime),
			GetMin  (dwTime),
			GetSec  (dwTime),
			0,
			Sign,
			Zone/60,
			Zone%60
			);
	}

BOOL CCloudServiceCumulocity::TakeSample(DWORD dwTime)
{
	UINT uNext = (m_uTail + 1 ) % m_uSize;

	// Serialization...

	if( uNext == m_uHead ) {

		FreeHeadStrings();

		m_uHead = (m_uHead + 1 ) % m_uSize;
		}

	UINT s = m_uTail * m_uTags;

	for( UINT n = 0; n < m_uTags; n++ ) {

		CDataRef &Ref = (CDataRef &) m_pTags[n];

		UINT     uTag = Ref.t.m_Index;

		CTag   * pTag = m_pSrc->GetItem(uTag);

		if( pTag->IsAvail(Ref, 0) ) {

			m_pData[s] = pTag->GetData(Ref, m_pType[n], 0);
			}
		else
			m_pData[s] = 0;

		m_pTime[m_uTail] = dwTime;

		m_uTail          = uNext;

		s++;
		}

	return TRUE;
	}

void CCloudServiceCumulocity::FreeHeadStrings(void)
{
	UINT s = m_uHead * m_uTags;

	for( UINT n = 0; n < m_uTags; n++ ) {

		if( m_pType[n] == typeString ) {

			PUTF p = PUTF(m_pData[s]);

			free(PUTF(p));
			}

		s++;
		}
	}

BOOL CCloudServiceCumulocity::ReadCredTag(void)
{
	if( m_pCredTag ) {

		CString Cred = GetItemData(m_pCredTag, "");

		if( Cred.StripToken(':') == "Cred" ) {

			m_Cfg.m_User = Cred.StripToken(':');

			m_Cfg.m_Pass = Cred.StripToken(':');

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCloudServiceCumulocity::WriteCredTag(void)
{
	if( m_pCredTag ) {

		CUnicode Cred;

		Cred += "Cred:";

		Cred += UniConvert(m_Cfg.m_User);
		
		Cred += ":";

		Cred += UniConvert(m_Cfg.m_Pass);

		m_pCredTag->SetValue(DWORD(wstrdup(Cred)), typeString, 0);

		AfxTagCommit();

		return TRUE;
		}

	return FALSE;
	}

// End of File
