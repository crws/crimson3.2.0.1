
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsSparkplug.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for Sparkplug Options
//

// Constructor

CMqttClientOptionsSparkplug::CMqttClientOptionsSparkplug(void)
{
	m_pAuthData  = NULL;

	m_uAuthData  = 0;

	m_GroupId    = "Aeon Devices";
		    
	m_NodeId     = "AeonNode1";
	}

// Initialization

void CMqttClientOptionsSparkplug::Load(PCBYTE &pData)
{
	CMqttClientOptionsCrimson::Load(pData);

	LoadFile(pData, m_pAuthData, m_uAuthData);

	m_GroupId    = UniConvert(GetWide(pData));

	m_NodeId     = UniConvert(GetWide(pData));

	m_PriAppId   = UniConvert(GetWide(pData));

	m_Mode       = 0;

	m_Reconn     = 1;

	m_SubQos     = 0;

	MakeClientId();
	}

// Implementation

BOOL CMqttClientOptionsSparkplug::LoadFile(PCBYTE &pData, PBYTE &pFile, UINT &uFile)
{
	if( (uFile = GetWord(pData)) ) {

		pFile = New BYTE [ uFile ];

		memcpy(pFile, pData, uFile);

		pData += uFile;

		return TRUE;
		}

	return FALSE;
	}

BOOL CMqttClientOptionsSparkplug::MakeClientId(void)
{
	if( m_ClientId.IsEmpty() ) {

		#if defined(AEON_ENVIRONMENT)

		BYTE p[] = { 0x00, 0x00, 0x00, 0x11, 0x22, 0x33 };

		#else

		PCBYTE p = NICGetMac();

		#endif

		m_ClientId.Printf( "redlion-client-%2.2x-%2.2x-%2.2x",
				    p[3], p[4], p[5]
				    );

		return TRUE;
		}

	return FALSE;
	}

// End of File
