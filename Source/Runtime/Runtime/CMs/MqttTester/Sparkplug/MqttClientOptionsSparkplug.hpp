
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqqtClientOptionsSparkplug_HPP

#define	INCLUDE_MqqtClientOptionsSparkplug_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientOptionsCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for Sparkplug Options
//

class CMqttClientOptionsSparkplug : public CMqttClientOptionsCrimson
{
	public:
		// Constructor
		CMqttClientOptionsSparkplug(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		PBYTE   m_pAuthData;
		UINT    m_uAuthData;
		CString m_GroupId;
		CString m_NodeId;
		CString m_PriAppId;
	
	protected:
		// Implementation
		BOOL LoadFile(PCBYTE &pData, PBYTE &pFile, UINT &uFile);
		BOOL MakeClientId(void);
	};

// End of File

#endif
