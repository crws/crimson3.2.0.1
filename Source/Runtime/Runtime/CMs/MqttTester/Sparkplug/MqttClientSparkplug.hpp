
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClientSparkplug_HPP

#define	INCLUDE_MqttClientSparkplug_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCloudServiceSparkplug;

class CMqttClientOptionsSparkplug;

class CGpbEncoder;

class CCloudDataSet;

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for Sparkplug
//

class CMqttClientSparkplug : public CMqttClientCrimson
{
	public:
		// Constructor
		CMqttClientSparkplug(CCloudServiceSparkplug *pService, CMqttClientOptionsSparkplug &Opts);

		// Operations
		BOOL Open(void);
		BOOL Poll(UINT uID);

	protected:
		// Pub Codes
		enum
		{
			pubNBirth = setCount + 0,
			};

		// Sub Codes
		enum
		{
			subNCmd  = 0,
			subState = 1,
			};

		// Sparkplug Data Type
		enum
		{
			sptInt8     = 1,
			sptInt16    = 2,
			sptInt32    = 3,
			sptInt64    = 4,
			sptUInt8    = 5,
			sptUInt16   = 6,
			sptUInt32   = 7,
			sptUInt64   = 8,
			sptFloat    = 9,
			sptDouble   = 10,
			sptBoolean  = 11,
			sptString   = 12,
			sptDateTime = 13,
			sptText     = 14,
			};

		// Sparkplug Message Properties
		enum
		{
			propMsgTimestamp = 1,
			propMsgMetrics   = 2,
			propMsgSeq       = 3,
			propMsgUuid      = 4,
			propMsgBody      = 5,
			};

		// Sparkplug Metric Properties
		enum
		{
			propMetricName         = 1,
			propMetricAlias        = 2,
			propMetricTimestamp    = 3,
			propMetricDataType     = 4,
			propMetricIsHistorical = 5,
			propMetricIsTransient  = 6,
			propMetricIsNull       = 7,
			propMetricMetadata     = 8,
			propMetricPropertySet  = 9,
			propMetricIntValue     = 10,
			propMetricLongValue    = 11,
			propMetricFloatValue   = 12,
			propMetricDoubleValue  = 13,
			propMetricBooleanValue = 14,
			propMetricStringValue  = 15,
			};

		// Sparkplug Property Set Properties
		enum
		{
			propPropSetKey   = 1,
			propPropSetValue = 2,
			};

		// Sparkplug Property Value Properties
		enum
		{
			propPropValueType         = 1,
			propPropValueIsNull       = 2,
			propPropValueIntValue     = 3,
			propPropValueLongValue    = 4,
			propPropValueFloatValue   = 5,
			propPropValueDoubleValue  = 6,
			propPropValueBooleanValue = 7,
			propPropValueStringValue  = 8,
			};

		// Options
		CMqttClientOptionsSparkplug &m_Opts;

		// Data Members
		UINT32  m_BdSeq;
		UINT64  m_BdTime;
		BYTE    m_bSeq;
		UINT    m_uOnline;
		BOOL    m_fBorn;
		UINT	m_uLast;

		// Client Hooks
		void OnClientPhaseChange(void);
		void OnClientPublish(CByteArray const &Blob);
		BOOL OnClientNewData(CMqttMessage const *pMsg);
		BOOL OnClientGetData(CMqttMessage * &pMsg);
		BOOL OnClientDataSent(CMqttMessage const *pMsg);

		// Publish Hook
		BOOL GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg);

		// Message Hook
		BOOL OnMakeHistoric(CMqttMessage *pMsg);

		// State Handler
		BOOL OnAppState(CString Topic, CByteArray const &Blob);

		// Command Handlers
		BOOL OnNodeCommand(CByteArray const &Blob);

		// Message Building
		BOOL MakeBirthCert(CByteArray &Blob);
		BOOL MakeDeathCert(CByteArray &Blob);
		BOOL AddBdSeq(CGpbEncoder &msg);
		BOOL AddProps(CGpbEncoder &msg);
		BOOL AddProp(CGpbEncoder &msg, CString Name, CString Value);
		BOOL AddData(CGpbEncoder &msg, UINT uTopic, UINT64 uTime, BOOL fBirth);
		BOOL AddData(CGpbEncoder &msg, CCloudDataSet *pSet, CString Root, UINT uAlias, UINT64 uTime, BOOL fBirth);
		BOOL AddProp(CGpbEncoder &set, CCloudDataSet *pSet, UINT uIndex, UINT uProp, CString Prop);

		// Implementation
		void GoOnline(void);
		void ShowSentData(CMqttMessage const *pMsg);
	};

// End of File

#endif
