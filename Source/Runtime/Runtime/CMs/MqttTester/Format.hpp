
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormat_HPP

#define INCLUDE_DispFormat_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Display Format
//

class CDispFormat : public CCodedHost
{
	public:
		// General Formatting
		static CUnicode GeneralFormat(DWORD Data, UINT Type, UINT Flags);

		// General Parsing
		static BOOL GeneralParse(DWORD &Data, CString Text, UINT Type);

	protected:
		// Implementation
		static void MakeDigitsFixed(CUnicode &Text);
		static void MakeLettersFixed(CUnicode &Text);
		static void MakeDigitsFixed(PUTF pText);
		static void MakeLettersFixed(PUTF pText);
	};

// End of File

#endif
