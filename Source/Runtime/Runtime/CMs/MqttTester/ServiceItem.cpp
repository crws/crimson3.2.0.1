
#include "Intern.hpp"

#include "ServiceItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Crimson Service Base Class
//

// Constructor

CServiceItem::CServiceItem(void)
{
	}

// IService

void CServiceItem::LoadService(PCBYTE &pData)
{
	Load(pData);
	}

UINT CServiceItem::GetID(void)
{
	return 0;
	}

void CServiceItem::GetTaskList(CTaskList &List)
{
	}

// End of File
