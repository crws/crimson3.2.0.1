
#include "Intern.hpp"

#include "AppObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <CxHttp.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ConfigBlob.hpp"

#include "TagInteger.hpp"

#include "TagReal.hpp"

#include "TagString.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Test Configuration
//

#include "AppObjectGoogle.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Tag List
//

global CTagList * afxTagList = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Instantiator

clink IUnknown * Create_AppObject(PCSTR pName)
{
	return (IClientProcess *) New CAppObject;
	}

// Constructor

CAppObject::CAppObject(void)
{
	StdSetRef();

	InitEmulation();

	CreateTags();

	CreateService();

	CConfigBlob Blob;

	CreateServiceBlob(Blob);

	AddTagList1(Blob);

	AddTagList2(Blob);

	AddTagList3(Blob);

	AddTagList4(Blob);

	AddDeviceData(Blob);

	CreateOptionsBlob(Blob);

	PCBYTE pData = Blob.GetData();

	m_pService->LoadService(pData);

	m_pService->GetTaskList(m_TaskList);

	AfxAssert(m_TaskList.GetCount()  == 1);
	}

// Destructor

CAppObject::~CAppObject(void)
{
	KillEmulation();

	delete afxTagList;
	}

// IUnknown

HRESULT CAppObject::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CAppObject::AddRef(void)
{
	StdAddRef();
	}

ULONG CAppObject::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CAppObject::TaskInit(UINT uTask)
{
	CTaskDef const &Task = m_TaskList[0];

	if( uTask == 0 ) {

		for( UINT n = 0; n < Task.m_uCount; n++ ) {

			Task.m_pEntry->TaskInit(Task.m_uID + n);
			}

		for( UINT n = 0; n < Task.m_uCount; n++ ) {

			m_Thread.Append(CreateClientThread(this, 1+n, 0));
			}
		}

	return TRUE;
	}

INT CAppObject::TaskExec(UINT uTask)
{
	CTaskDef const &Task = m_TaskList[0];

	if( uTask == 0 ) {

		for(;;) Sleep(FOREVER);
		}

	if( uTask >= 1 ) {

		Task.m_pEntry->TaskExec(Task.m_uID + uTask - 1);
		}

	return 0;
	}

void CAppObject::TaskStop(UINT uTask)
{
	CTaskDef const &Task = m_TaskList[0];

	if( uTask == 0 ) {

		for( UINT n = 0; n < Task.m_uCount; n++ ) {

			Task.m_pEntry->TaskStop(Task.m_uID + n);
			}
		}
	}

void CAppObject::TaskTerm(UINT uTask)
{
	CTaskDef const &Task = m_TaskList[0];

	if( uTask == 0 ) {

		for( UINT n = 0; n < Task.m_uCount; n++ ) {

			DestroyThread(m_Thread[n]);
			}

		for( UINT n = 0; n < Task.m_uCount; n++ ) {

			Task.m_pEntry->TaskTerm(Task.m_uID + n);
			}
		}
	}

// Implementation

void CAppObject::InitEmulation(void)
{
	#if 1

	InitEmulatedIp();

	InitEmulatedFs();

	#endif
	}

void CAppObject::KillEmulation(void)
{
	#if 1

	KillEmulatedIp();

	#endif
	}

void CAppObject::InitEmulatedIp(void)
{
	IRouter *pRouter;

	AfxGetObject("ip", 0, IRouter, pRouter);

	pRouter->AddLoopback();

	CConfigEth Config;

	memset(&Config, 0, sizeof(Config));

	pRouter->AddEthernet(0, Config);

	pRouter->Open();
	}

void CAppObject::KillEmulatedIp(void)
{
	IRouter *pRouter;

	AfxGetObject("ip", 0, IRouter, pRouter);

	pRouter->Close();
	}

void CAppObject::InitEmulatedFs(void)
{
	IDiskManager *pDiskManager;

	ISdHost      *pSdHost;

	IBlockDevice *pBlockDevice;

	AfxGetObject("diskman", 0, IDiskManager, pDiskManager);

	AfxGetObject("sdhost",  0, ISdHost,      pSdHost);

	AfxNewObject("sddrive",    IBlockDevice, pBlockDevice);

	pBlockDevice->Open(pSdHost);

	pDiskManager->RegisterDisk(pBlockDevice);

//	pDiskManager->FormatDisk(0);

	while( !pDiskManager->GetDiskReady(0) ) Sleep(100);

	AfxTrace("Disk Ready\n");
	}

void CAppObject::CreateTags(void)
{
	afxTagList = New CTagList;

	// Integers

	afxTagList->m_List.Append(New CTagInteger(/*"Top1.Group1.*/"Tag1", 1000, 10,  0));

	afxTagList->m_List.Append(New CTagInteger(/*"Top1.Group1.*/"Tag2", 2000, 20,  1));

	afxTagList->m_List.Append(New CTagInteger(/*"Top1.Group1.*/"Tag3", 3000,  0,  0));

	afxTagList->m_List.Append(New CTagInteger(/*"Top1.Group1.*/"Tag4", 4000,  0,  0));

	afxTagList->m_List.Append(New CTagInteger(/*"Top1.Group1.*/"Tag5", 5000,  0,  0));

	// Reals

	afxTagList->m_List.Append(New CTagReal   (/*"Top1.Group2.*/"Tag6", 6000,  2, 250));

	// Strings

	afxTagList->m_List.Append(New CTagString (/*"Group3.*/"Tag7", L"Seven", 2000));

	afxTagList->m_List.Append(New CTagString (/*"Group3.*/"Tag8", L"Eight",    0));
	}

BOOL CAppObject::AddDeviceData(CConfigBlob &Blob)
{
	Blob.AddWord(0x1234);	// Marker

	Blob.AddByte(1);	// Mode
	Blob.AddByte(0);	// History
	Blob.AddWord(600);	// Scan
	Blob.AddWord(0);	// Force
	Blob.AddByte(0);	// Req
	Blob.AddByte(0);	// Ack
	Blob.AddByte(1);	// Gps
	Blob.AddByte(1);	// Cell

	return TRUE;
	}

BOOL CAppObject::AddTagList1(CConfigBlob &Blob)
{
	Blob.AddWord(0x1234);	// Marker

	Blob.AddByte(0);	// Mode
	Blob.AddByte(0);	// History
	Blob.AddWord(10);	// Scan
	Blob.AddWord(0);	// Force
	Blob.AddByte(0);	// Req
	Blob.AddByte(0);	// Ack
	Blob.AddByte(0);	// Label
	Blob.AddByte(0);	// Tree
	Blob.AddByte(0);	// Array
	Blob.AddByte(0);	// Props
	Blob.AddByte(0);	// Write
	
	Blob.AddWord(0);

	return TRUE;
	}

BOOL CAppObject::AddTagList2(CConfigBlob &Blob)
{
	Blob.AddWord(0x1234);	// Marker

	Blob.AddByte(1);	// Mode
	Blob.AddByte(1);	// History
	Blob.AddWord(1);	// Scan
	Blob.AddWord(0);	// Force
	Blob.AddByte(0);	// Req
	Blob.AddByte(0);	// Ack
	Blob.AddByte(0);	// Label
	Blob.AddByte(0);	// Tree
	Blob.AddByte(0);	// Array
	Blob.AddByte(0);	// Props
	Blob.AddByte(1);	// Write

	Blob.AddWord(6);

	Blob.AddLong(0);
	Blob.AddLong(1);
	Blob.AddLong(2);
	Blob.AddLong(3);
	Blob.AddLong(5);
	Blob.AddLong(6);

	return TRUE;
	}

BOOL CAppObject::AddTagList3(CConfigBlob &Blob)
{
	Blob.AddWord(0x1234);	// Marker

	Blob.AddByte(3);	// Mode
	Blob.AddByte(0);	// History
	Blob.AddWord(10);	// Scan
	Blob.AddWord(0);	// Force
	Blob.AddByte(0);	// Req
	Blob.AddByte(0);	// Ack
	Blob.AddByte(0);	// Label
	Blob.AddByte(0);	// Tree
	Blob.AddByte(1);	// Array
	Blob.AddByte(0);	// Props
	Blob.AddByte(1);	// Write

	Blob.AddWord(1);

	Blob.AddLong(4);

	return TRUE;
	}

BOOL CAppObject::AddTagList4(CConfigBlob &Blob)
{
	Blob.AddWord(0x1234);	// Marker

	Blob.AddByte(0);	// Mode
	Blob.AddByte(0);	// History
	Blob.AddWord(10);	// Scan
	Blob.AddWord(0);	// Force
	Blob.AddByte(0);	// Req
	Blob.AddByte(0);	// Ack
	Blob.AddByte(0);	// Label
	Blob.AddByte(0);	// Tree
	Blob.AddByte(0);	// Array
	Blob.AddByte(0);	// Props
	Blob.AddByte(0);	// Write

	Blob.AddWord(2);

	Blob.AddLong(3);
	Blob.AddLong(7);

	return TRUE;
	}

// End of File
