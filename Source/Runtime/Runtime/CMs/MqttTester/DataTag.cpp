
#include "Intern.hpp"

#include "DataTag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Crimson Data Tag
//

// Constructors

CDataTag::CDataTag(void)
{
	m_Extent = 0;
	}

// Attributes

BOOL CDataTag::IsArray(void) const
{
	return m_Extent > 0;
	}

UINT CDataTag::GetExtent(void) const
{
	return m_Extent;
	}

BOOL CDataTag::CanWrite(void) const
{
	return TRUE;
	}

// Operations

void CDataTag::Force(void)
{
	}

// Implementation

BOOL CDataTag::AllowWrite(CDataRef const &Ref, DWORD Data, UINT Type)
{
	return TRUE;
	}

BOOL CDataTag::LocalData(void)
{
	return TRUE;
	}

BOOL CDataTag::IsNumeric(UINT Type)
{
	return Type == typeReal || IsInteger(Type);
	}

BOOL CDataTag::IsInteger(UINT Type)
{
	return Type == typeVoid || Type == typeInteger || Type == typeLogical;
	}

BOOL CDataTag::IsString(UINT Type)
{
	return Type == typeString;
	}

// End of File
