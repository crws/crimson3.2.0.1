
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client Options
//

// Constructor

CMqttClientOptionsCrimson::CMqttClientOptionsCrimson(void)
{
	m_Reconn = 0;
	}

// Initialization

void CMqttClientOptionsCrimson::Load(PCBYTE &pData)
{
	CMqttQueuedClientOptions::Load(pData);

	m_Mode   = GetByte(pData);

	m_Reconn = GetByte(pData);
	}

// End of File
