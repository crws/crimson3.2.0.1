
#include "Intern.hpp"

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Crimson Coded Item
//

// Operations

void CCodedItem::SetScan(UINT uScan)
{
	}

void CCodedItem::SetValue(DWORD Data, UINT Type, UINT uFlags)
{
	if( Type == typeString ) {

		free(PTXT(m_Data));

		m_Data = DWORD(wstrdup(PCUTF(Data)));

		return;
		}

	m_Data = Data;
	}

// End of File
