
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqqtClientOptionsAzure_HPP

#define	INCLUDE_MqqtClientOptionsAzure_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientOptionsJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for Azure Options
//

class CMqttClientOptionsAzure : public CMqttClientOptionsJson
{
	public:
		// Constructor
		CMqttClientOptionsAzure(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		void MakeCredentials(UINT uMins);

		// Data Members
		UINT    m_Twin;
		CString m_Pub;
		CString m_Sub;
		CString m_Key;

	protected:
		// Implementation
		CString MakeSas(CString Time);
		CString GetHmac(PCBYTE pData, UINT uKey, CString const &Data);
	};

// End of File

#endif
