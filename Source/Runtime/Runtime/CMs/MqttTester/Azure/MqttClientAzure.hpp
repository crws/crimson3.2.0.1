
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClientAzure_HPP

#define	INCLUDE_MqttClientAzure_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCloudServiceAzure;

class CMqttClientOptionsAzure;

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for Azure
//

class CMqttClientAzure : public CMqttClientJson
{
	public:
		// Constructor
		CMqttClientAzure(CCloudServiceAzure *pService, CMqttClientOptionsAzure &Opts);

		// Operations
		BOOL Poll(UINT uID);

	protected:
		// Pub Topics
		enum
		{
			pubGetTwin = setCount + 0,
			pubCount   = setCount + 1,
			};

		// Sub Topics
		enum
		{
			subResp   = 0,
			subDevice = 1,
			subWrite  = 2,
			};

		// Options
		CMqttClientOptionsAzure &m_Opts;

		// Data Members
		CString m_PubTopic;
		BOOL    m_fRunning;
		BOOL    m_fSent[pubCount];
		UINT    m_uTime[pubCount];
		UINT	m_uLast;

		// Client Hooks
		void OnClientPhaseChange(void);
		BOOL OnClientNewData(CMqttMessage const *pMsg);
		BOOL OnClientGetData(CMqttMessage * &pMsg);
		BOOL OnClientDataSent(CMqttMessage const *pMsg);

		// Publish Hook
		BOOL GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg);
	};

// End of File

#endif
