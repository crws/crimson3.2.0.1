
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_APPOBJECT_AWS_HPP

#define INCLUDE_APPOBJECT_AWS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "AppObject.hpp"

#include "ConfigBlob.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AWS Test Keys
//

static BYTE m_bThingCert[] = {
	#include "9947c042d4-certificate.pem.crt.dat"
	0
	};

static BYTE m_bThingPriv[] = {
	#include "9947c042d4-private.pem.key.dat"
	0
	};

static BYTE m_bThingAuth[] = {
	#include "Certification-Authority.pem.dat"
	0
	};

//////////////////////////////////////////////////////////////////////////
//
// AWS Service
//

extern IService * Create_CloudServiceAws(void);

//////////////////////////////////////////////////////////////////////////
//
// Service Creation
//

void CAppObject::CreateService(void)
{
	m_pService = Create_CloudServiceAws();
	}

void CAppObject::CreateServiceBlob(CConfigBlob &Blob)
{
	// CloudServiceCrimson

	Blob.AddWord(0x1234);		// Marker
	Blob.AddCode(C3INT(1));		// Enable
	Blob.AddByte(0);		// Service
	Blob.AddCode(0);		// Status
	Blob.AddCode(L"");		// Ident
	}

void CAppObject::CreateOptionsBlob(CConfigBlob &Blob)
{
	// MqqtClientOptions

	CString Peer1 = "a1hbbi4z1w5rtq.iot.us-west-2.amazonaws.com";

	CString Peer2 = "";
	
	CString Name  = "AeonThing01";

	Blob.AddWord(0x1234);	// Marker
	Blob.AddByte(1);	// fDebug
	Blob.AddByte(1);	// fTls
	Blob.AddByte(0);	// uCheck
	Blob.AddWord(8883);	// uPort
	Blob.AddByte(1);	// PubQos
	Blob.AddByte(1);	// SubQos
	Blob.AddText(Peer1);	// PeerName[0]
	Blob.AddText(Peer2);	// PeerName[1]
	Blob.AddText(Name);	// ClientId
	Blob.AddText("");	// UserName
	Blob.AddText("");	// Password
	Blob.AddWord(30);	// ConnTimeout
	Blob.AddWord(15);	// SendTimeout
	Blob.AddWord(15);	// RecvTimeout
	Blob.AddWord(5);	// BackOffTime
	Blob.AddWord(30);	// BackOffMax
	Blob.AddWord(600);	// KeepAlive

	// MqttQueuedClientOptions

	Blob.AddByte(0);	// Buffer

	// MqttClientOptionsCrimson

	Blob.AddByte(0);	// Mode
	Blob.AddByte(0);	// Reconn

	// MqttClientOptionsJson

	Blob.AddByte(0);	// Root
	Blob.AddByte(0);	// Code

	// MqqtClientOptionsAws

	Blob.AddFile(m_bThingCert, sizeof(m_bThingCert)-1);
	Blob.AddFile(m_bThingPriv, sizeof(m_bThingPriv)-1);
	Blob.AddFile(m_bThingAuth, sizeof(m_bThingAuth)-1);
	}

// End of File

#endif
