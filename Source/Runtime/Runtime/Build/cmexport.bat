@echo off

REM %1 = $(GnuToolFolder)
REM %2 = $(OutDir)
REM %3 = $(TargetFileName)
REM %4 = $(TargetName)

cd /D %2

lzcompress "%~2%3" "%~2%4.cbf"

REM Convert the compressed image to linkable binary object,
REM which is basically an OBJ file but renamed so that it will
REM not confuse the build system. We can link this into hosts
REM so that the Module Manager can load it.

"%~1arm-eabi\bin\objcopy.exe" --rename-section .data=.rodata.%4,alloc,load,readonly,data,contents -I binary -O elf32-littlearm -B arm "%4.cbf" "%4.blo"

erase "%~2%4.cbf"
