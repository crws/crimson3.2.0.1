
#include "Intern.hpp"

#include "Pca9536.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// NXP I/O Expander
//

// Instantiator

IGpioExpander * Create_Pca9536(BYTE bAddr, INT nLimitLo, INT nLimitHi)
{
	CPca9536 *pDevice = New CPca9536;

	pDevice->Open();

	return pDevice;
	}

// Constructor

CPca9536::CPca9536()
{
	StdSetRef();

	m_bChip = 0x82;

	AfxGetObject("dev.i2c", 0, II2c, m_pI2c);
	}

// Destructor

CPca9536::~CPca9536(void)
{
	m_pI2c->Release();
	}

// IUnknown

HRESULT CPca9536::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IGpioExpander);

	return E_NOINTERFACE;
	}

ULONG CPca9536::AddRef(void)
{
	StdAddRef();
	}

ULONG CPca9536::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CPca9536::Open(void)
{
	return TRUE;
	}

// IGpioExpander

void METHOD CPca9536::SetBit(UINT uIndex, bool fState)
{
	BYTE bData;

	if( GetData(regOutput, &bData, sizeof(bData)) ) {

		BYTE bMask = 1 << uIndex;

		if( !fState ) {
			
			bData |= bMask;
			}
		else {
			bData &= ~bMask;
			}
		
		PutData(regOutput, &bData, sizeof(bData));
		}
	}

BOOL METHOD CPca9536::GetBit(UINT uIndex)
{
	return false;
	}

// Implementation

void CPca9536::InitChip(void)
{	
	InitConfig();
	}

void CPca9536::InitConfig(void)
{
	BYTE bData = 0x00;

	PutData(regConfig, &bData, sizeof(bData));
	}

bool CPca9536::PutData(BYTE bAddr, PCBYTE pData, UINT uCount)
{
	if( m_pI2c->Lock(FOREVER) ) {

		if( m_pI2c->Send(m_bChip, &bAddr, 1, pData, uCount) ) {

			m_pI2c->Free();
			
			return true;
			}

		m_pI2c->Free();

		return false;
		}

	return false;
	}

bool CPca9536::GetData(BYTE bAddr, PBYTE pData, UINT uCount)
{
	if( m_pI2c->Lock(FOREVER) ) {

		bAddr |= 1;

		if( m_pI2c->Recv(m_bChip, &bAddr, 1, pData, uCount) ) {

			m_pI2c->Free();
			
			return true;
			}

		m_pI2c->Free();

		return false;
		}

	return false;
	}

// End of File
