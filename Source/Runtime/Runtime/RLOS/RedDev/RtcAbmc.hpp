
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RtcAbmc_HPP
	
#define	INCLUDE_RtcAbmc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RtcGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Abracon Real Time Clock
//

class CRtcAbmc : public CRtcGeneric
{
	public:
		// Constructor
		CRtcAbmc(void);

		// Destructor
		~CRtcAbmc(void);

	protected:
		// Data Members
		II2c * m_pI2c;
		BYTE   m_bChip;

		// Overridables
		bool InitChip(void);
		bool GetTimeFromChip(void);
		bool GetSecsFromChip(UINT &uSecs);
		bool WriteTimeToChip(void);

		// Implementation
		bool GetData(BYTE bAddr, PBYTE  pData, UINT uCount);
		bool PutData(BYTE bAddr, PCBYTE pData, UINT uCount);
		UINT ToBin(BYTE bData, UINT uWidth);
		BYTE ToBcd(UINT uData, UINT uWidth);
	};

// End of File

#endif
