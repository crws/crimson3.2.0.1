
#include "Intern.hpp"

#include "BatteryMonitor.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic Battery Wrapper Object
//

// Instantiator

IDevice * Create_BatterMonitor(IBatteryMonitor *pBattMon)
{
	IDevice *pDevice = New CBatteryMonitor(pBattMon);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CBatteryMonitor::CBatteryMonitor(IBatteryMonitor *pBattMon)
{
	StdSetRef();

	m_pBattMon = pBattMon;
	}

// IUnknown

HRESULT CBatteryMonitor::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IBatteryMonitor);

	StdQueryInterface(IBatteryMonitor);

	return E_NOINTERFACE;
	}

ULONG CBatteryMonitor::AddRef(void)
{
	StdAddRef();
	}

ULONG CBatteryMonitor::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CBatteryMonitor::Open(void)
{
	return TRUE;
	}

// IBatteryMonitor

bool METHOD CBatteryMonitor::IsBatteryGood(void)
{
	return m_pBattMon->IsBatteryGood();
	}

bool METHOD CBatteryMonitor::IsBatteryBad(void)
{
	return m_pBattMon->IsBatteryBad();
	}

// End of File
