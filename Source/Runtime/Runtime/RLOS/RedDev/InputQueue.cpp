
#include "Intern.hpp"

#include "InputQueue.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Input Queue
//

// Instantiator

IDevice * Create_InputQueueRLOS(void)
{
	return New CInputQueueRLOS(TRUE);
	}

// Constructor

CInputQueueRLOS::CInputQueueRLOS(BOOL fDisp)
{
	StdSetRef();

	m_fDisp = fDisp;

	m_pFlag = Create_Semaphore();

	m_uHead = 0;

	m_uTail = 0;
	}

// IDevice

BOOL CInputQueueRLOS::Open(void)
{
	return TRUE;
	}

// IUnknown

HRESULT CInputQueueRLOS::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IInputQueue);

	return E_NOINTERFACE;
	}

ULONG CInputQueueRLOS::AddRef(void)
{
	StdAddRef();
	}

ULONG CInputQueueRLOS::Release(void)
{
	StdRelease();
	}

// IInputQueue

INPUT CInputQueueRLOS::Read(UINT uTime)
{
	if( m_pFlag->Wait(uTime) ) {

		INPUT Input = m_Data[m_uHead];

		m_uHead = (m_uHead + 1) % elements(m_Data);

		return Input;
		}

	return 0;
	}

bool CInputQueueRLOS::Store(INPUT Input)
{
	UINT ipr;

	HostRaiseIpr(ipr);

	UINT uNext = (m_uTail + 1) % elements(m_Data);

	if( m_uHead != uNext ) {

		m_Data[m_uTail] = Input;

		m_uTail = uNext;

		HostLowerIpr(ipr);

		m_pFlag->Signal(1);

		return true;
		}

	HostLowerIpr(ipr);

	AfxTrace("input: queue is full\n");

	return false;
	}

void CInputQueueRLOS::Clear(void)
{
	while( Read(0) );
	}

// End of File
