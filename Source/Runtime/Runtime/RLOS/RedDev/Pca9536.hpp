
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Pca9536_HPP
	
#define	INCLUDE_Pca9536_HPP

//////////////////////////////////////////////////////////////////////////
//
// NXP I/O Expander
//

class CPca9536 : public IGpioExpander
{
	public:
		// Constructor
		CPca9536(void);

		// Destructor
		~CPca9536(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IGpioExpander
		void METHOD SetBit(UINT uIndex, bool fState);
		BOOL METHOD GetBit(UINT uIndex);

	protected:
		// Registers
		enum {
			regInput	= 0,
			regOutput	= 1,
			regPolarity	= 2,
			regConfig	= 3,
			};


		// Data Members
		ULONG	m_uRefs;
		BYTE	m_bChip;
		II2c  * m_pI2c;

		// Implementation
		void InitChip(void);
		void InitConfig(void);
		bool PutData(BYTE bAddr, PCBYTE pData, UINT uCount);
		bool GetData(BYTE bAddr, PBYTE pData, UINT uCount);
	};

// End of File

#endif
