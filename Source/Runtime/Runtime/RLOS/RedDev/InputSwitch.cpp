
#include "Intern.hpp"

#include "InputSwitch.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Input Switch Generic Wrapper Object
//

// Instantiator

IDevice * Create_InputSwitch(IInputSwitch *pInput)
{
	IDevice *pDevice = New CInputSwitch(pInput);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CInputSwitch::CInputSwitch(IInputSwitch *pInput)
{
	StdSetRef();

	m_pInput = pInput;
	}

// IUnknown

HRESULT CInputSwitch::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IInputSwitch);

	StdQueryInterface(IInputSwitch);

	return E_NOINTERFACE;
	}

ULONG CInputSwitch::AddRef(void)
{
	StdAddRef();
	}

ULONG CInputSwitch::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CInputSwitch::Open(void)
{
	return TRUE;
	}

// IInputSwitch

UINT METHOD CInputSwitch::GetSwitches(void)
{
	return m_pInput->GetSwitches();
	}

UINT METHOD CInputSwitch::GetSwitch(UINT uSwitch)
{
	return m_pInput->GetSwitch(uSwitch);
	}

// End of File
