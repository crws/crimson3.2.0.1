
#include "Intern.hpp"

#include "Mr25Hxx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Serial SPI MRAM 
//

// Instantiator

IDevice * Create_Mr25Hxx(UINT uChan)
{
	IDevice *pDevice = (IDevice *) New CMr25Hxx(uChan);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CMr25Hxx::CMr25Hxx(UINT uChan)
{
	StdSetRef();

	m_uChan = uChan;

	AfxGetObject("spi", 0, ISpi, m_pSpi); 
	}

// Destructor

CMr25Hxx::~CMr25Hxx(void)
{
	m_pSpi->Release();
	}

// IUnknown

HRESULT CMr25Hxx::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ISerialMemory);

	return E_NOINTERFACE;
	}

ULONG CMr25Hxx::AddRef(void)
{
	StdAddRef();
	}

ULONG CMr25Hxx::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CMr25Hxx::Open(void)
{
	CSpiConfig Config;

	Config.m_uFreq      = 20000000;
	Config.m_fClockIdle = false;
	Config.m_fDataIdle  = false;
	Config.m_uSelect    = 0;
	Config.m_uClockPol  = 0;
	Config.m_uClockPha  = 0;
	Config.m_fBurst	    = false;
	
	m_pSpi->Init(m_uChan, Config);

	WriteEnable(true);

	return TRUE;
	}

// ISerialMemory

UINT METHOD CMr25Hxx::GetSize(void)
{
	return 32768;
	}

BOOL METHOD CMr25Hxx::IsFast(void)
{
	return TRUE;
	}

BOOL METHOD CMr25Hxx::GetData(UINT uAddr, PBYTE pData, UINT uCount)
{
	while( uCount ) {

		UINT uThis = uCount;

		MakeMin(uThis, 128);

		BYTE bCmd[3];

		bCmd[0] = 0x03;

		bCmd[1] = HIBYTE(WORD(uAddr));

		bCmd[2] = LOBYTE(WORD(uAddr));

		if( m_pSpi->Recv(m_uChan, bCmd, sizeof(bCmd), pData, uThis) ) {

			uCount -= uThis;

			uAddr  += uThis;

			pData  += uThis;

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL METHOD CMr25Hxx::PutData(UINT uAddr, PCBYTE pData, UINT uCount)
{
	while( uCount ) {

		UINT uThis = uCount;

		MakeMin(uThis, 128);
		
		BYTE bCmd[3];

		bCmd[0] = 0x02;

		bCmd[1] = HIBYTE(WORD(uAddr));

		bCmd[2] = LOBYTE(WORD(uAddr));

		if( m_pSpi->Send(m_uChan, bCmd, sizeof(bCmd), pData, uThis) ) {

			uCount -= uThis;

			uAddr  += uThis;

			pData  += uThis;

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

// Implementation

bool CMr25Hxx::WriteEnable(bool fEnable)
{
	BYTE bCmd = fEnable ? 0x06 : 0x04; 

	return m_pSpi->Send(m_uChan, &bCmd, sizeof(bCmd), NULL, 0);
	}

// End of File
