
#include "Intern.hpp"

#include "UsbHub251x.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub 251x Serial EEPROM
//

// Instantiator

IDevice * Create_UsbHub251x(void)
{
	IDevice *pDevice = (IDevice *) New CUsbHub251x();

	pDevice->Open();

	return pDevice;
	}

// Constructor

CUsbHub251x::CUsbHub251x(void)
{
	StdSetRef();
	
	m_bChip = 0x58;

	AfxGetObject("i2c", 0, II2c, m_pI2c);
	}

// Destructor

CUsbHub251x::~CUsbHub251x(void)
{
	m_pI2c->Release();
	}

// IUnknown

HRESULT CUsbHub251x::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ISerialMemory);

	return E_NOINTERFACE;
	}

ULONG CUsbHub251x::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbHub251x::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CUsbHub251x::Open(void)
{
	return TRUE;
	}

// ISerialMemory

UINT METHOD CUsbHub251x::GetSize(void)
{
	return 255;
	}

BOOL METHOD CUsbHub251x::IsFast(void)
{
	return FALSE;
	}

BOOL METHOD CUsbHub251x::GetData(UINT uAddr, PBYTE pData, UINT uCount)
{
	if( m_pI2c->Lock(FOREVER) ) {

		while( uCount ) {

			BYTE bCount = Min(BYTE(uCount), BYTE(32));

			BYTE bAddr  = BYTE(uAddr);

			m_bBuff[0]  = 0;

			if( m_pI2c->Recv(m_bChip, &bAddr, sizeof(bAddr), m_bBuff, bCount + 1) ) {

				bCount = Min(m_bBuff[0], bCount);

				if( bCount ) {

					ArmMemCpy(pData, m_bBuff + 1, bCount);
								
					uAddr  += bCount;

					uCount -= bCount;

					pData  += bCount;

					continue;
					}
				}
			
			m_pI2c->Free();

			return FALSE;
			}

		m_pI2c->Free();

		return TRUE;
		}

	return FALSE;
	}

BOOL METHOD CUsbHub251x::PutData(UINT uAddr, PCBYTE pData, UINT uCount)
{
	if( m_pI2c->Lock(FOREVER) ) {

		while( uCount ) {

			BYTE bCount = Min(BYTE(uCount), BYTE(32));

			BYTE bHdr[2] = { BYTE(uAddr), bCount };

			if( m_pI2c->Send(m_bChip, bHdr, sizeof(bHdr), pData, bCount) ) {

				uAddr  += bCount;

				uCount -= bCount;

				pData  += bCount;

				continue;
				}
			
			m_pI2c->Free();

			return FALSE;
			}

		m_pI2c->Free();

		return TRUE;
		}

	return FALSE;
	}

// End of File
