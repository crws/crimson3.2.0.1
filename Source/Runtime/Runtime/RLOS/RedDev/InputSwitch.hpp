
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_InputSwitch_HPP
	
#define	INCLUDE_InputSwitch_HPP

//////////////////////////////////////////////////////////////////////////
//
// Input Switch Proxy Object
//

class CInputSwitch : public IInputSwitch
{
	public:
		// Constructor
		CInputSwitch(IInputSwitch *pInput);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IInputSwitch
		UINT METHOD GetSwitches(void);
		UINT METHOD GetSwitch(UINT uSwitch);

	protected:
		// Data Members
		ULONG          m_uRefs;
		IInputSwitch * m_pInput;
	};

// End of File

#endif
