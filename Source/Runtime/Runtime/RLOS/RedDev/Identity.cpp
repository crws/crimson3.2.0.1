
#include "Intern.hpp"

#include "Identity.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Sitara Identity Object
//

// Instantiator

IDevice * Create_Identity(UINT uAddr)
{
	IDevice *pDevice = New CIdentity(uAddr);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CIdentity::CIdentity(UINT uAddr) : CIdentityBase(uAddr)
{
	m_wSize += 10;
	}

// Implementation

void CIdentity::OnInit(void)
{
	CIdentityBase::OnInit();

	SetPropWord(propBacklight, 100);
	}

// End of File
