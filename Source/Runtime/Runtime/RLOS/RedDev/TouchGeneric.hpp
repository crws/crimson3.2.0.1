
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_TouchGeneric_HPP
	
#define	INCLUDE_TouchGeneric_HPP

//////////////////////////////////////////////////////////////////////////
//
// Generic Touch Screen Controller
//

class CTouchGeneric : public ITouchScreenCalib
{
	public:
		// Constructor
		CTouchGeneric(void);

		// Destructor 
		virtual ~CTouchGeneric(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ITouchScreen
		void   METHOD GetCellSize(int &xCell, int &yCell);
		void   METHOD GetDispCells(int &xDisp, int &yDisp);
		void   METHOD SetBeepMode(bool fBeep);
		void   METHOD SetDragMode(bool fDrag);
		PCBYTE METHOD GetMap(void);
		void   METHOD SetMap(PCBYTE pMap);
		bool   METHOD GetRaw(int &xRaw, int &yRaw);

		// ITouchScreenCalib
		bool METHOD SetCalib(int xMin, int yMin, int xMax, int yMax);
		void METHOD ClearCalib(bool fPersist);
		void METHOD LoadCalib(void);

		// Virtual Key Codes
		enum {
			virtSoft1	= 0x80,
			virtSoft2	= 0x81,
			virtSoft3	= 0x82,
			virtMenu	= 0xA2,
			};

	protected:
		// Constants
		enum
		{
			constCellSize	= 4,
			};

		// Data Members
		ULONG		m_uRefs;
		ISerialMemory * m_pMem;
		UINT            m_uSlot;
		IDisplay      * m_pDisplay;
		IInputQueue   * m_pInput;
		IBeeper       * m_pBeep;
		BOOL		m_fBeep;
		PCBYTE		m_pMap;
		int		m_xRaw;
		int		m_yRaw;
		int		m_xPos;
		int		m_yPos;
		int		m_xDisp;
		int		m_yDisp;
		int		m_xCells;
		int		m_yCells;
		bool		m_fFlipX;
		bool		m_fFlipY;
		int		m_xMin;
		int		m_yMin;
		int		m_xMax;
		int		m_yMax;
		int		m_fIcon;
		UINT		m_uCode;

		// Scaling
		int  Scale(int nRaw, int nMin, int nMax, int nSize, bool fFlip);

		// Implementation
		void FindCalib(void);
		void SaveCalib(void);
		bool IsValidTouch(void);
		bool IsActive(void);
		void PostEvent(UINT uState);

		// Overridables
		virtual void DefaultCalib(void) = 0;
		virtual bool OnTouchValue(void);
	};

// End of File

#endif
