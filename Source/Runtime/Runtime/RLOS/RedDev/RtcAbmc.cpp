
#include "Intern.hpp"

#include "RtcAbmc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Abracon Real Time Clock
//

// Instantiator

IDevice * Create_RtcAbmc(void)
{
	IDevice *pDevice = New CRtcAbmc();

	pDevice->Open();

	return pDevice;
	}

// Constructor

CRtcAbmc::CRtcAbmc(void)
{
	m_bChip = 0xAC;

	AfxGetObject("i2c", 0, II2c, m_pI2c);
	}

// Destructor

CRtcAbmc::~CRtcAbmc(void)
{
	m_pI2c->Release();
	}

// Overridables

bool CRtcAbmc::InitChip(void)
{
	BYTE bData = 0x00;

	PutData(0x03, &bData, 1);

	return true;
	}

bool CRtcAbmc::GetTimeFromChip(void)
{
	if( m_pI2c->Lock(FOREVER) ) {

		BYTE bTime[7];

		if( GetData(0x08, bTime, sizeof(bTime)) ) {

			struct tm tm = { 0 };

			tm.tm_sec  = ToBin(bTime[0], 7);
			tm.tm_min  = ToBin(bTime[1], 7);
			tm.tm_hour = ToBin(bTime[2], 6);

			tm.tm_mday = ToBin(bTime[3], 6) + 0;
			tm.tm_mon  = ToBin(bTime[5], 5) - 1;
			tm.tm_year = ToBin(bTime[6], 7) + 100;

			/*AfxTrace("Got %2.2u/%2.2u/%4.4u %2.2u:%2.2u:%2.2u\n",
				    tm.tm_mon  + 1,
				    tm.tm_mday + 0,
				    tm.tm_year + 1900,
				    tm.tm_hour,
				    tm.tm_min,
				    tm.tm_sec
				    );

			AfxDump(bTime, sizeof(bTime));*/

			m_time = timegm(&tm);

			m_pI2c->Free();

			return true;
			}

		m_pI2c->Free();
		}

	return false;
	}

bool CRtcAbmc::GetSecsFromChip(UINT &uSecs)
{
	if( m_pI2c->Lock(0) ) {

		BYTE bSecs;

		if( GetData(0x08, &bSecs, 1) ) {

			uSecs = ToBin(bSecs, 7);

			m_pI2c->Free();

			return true;
			}

		m_pI2c->Free();
		}

	return false;
	}

bool CRtcAbmc::WriteTimeToChip(void)
{
	if( m_pI2c->Lock(FOREVER) ) {

		BYTE bData[7] = { 0, 0, 0, 0, 1, 0, 0 };

		struct tm *tm = gmtime(&m_time);

		/*AfxTrace("Put %2.2u/%2.2u/%4.4u %2.2u:%2.2u:%2.2u\n",
				tm->tm_mon  + 1,
				tm->tm_mday + 0,
				tm->tm_year + 1900,
				tm->tm_hour,
				tm->tm_min,
				tm->tm_sec
				);*/

		bData[0] = ToBcd(tm->tm_sec,  7);
		bData[1] = ToBcd(tm->tm_min,  7);
		bData[2] = ToBcd(tm->tm_hour, 6);

		bData[3] = ToBcd(tm->tm_mday - 0,   6);
		bData[5] = ToBcd(tm->tm_mon  + 1,   5);
		bData[6] = ToBcd(tm->tm_year - 100, 7);

		/*AfxDump(bData, sizeof(bData));*/

		PutData(0x08, bData, sizeof(bData));

		m_pI2c->Free();

		return true;
		}

	return false;
	}

// Implementation

bool CRtcAbmc::GetData(BYTE bAddr, PBYTE pData, UINT uCount)
{
	return m_pI2c->Recv(m_bChip, &bAddr, 1, pData, uCount);
	}

bool CRtcAbmc::PutData(BYTE bAddr, PCBYTE pData, UINT uCount)
{
	return m_pI2c->Send(m_bChip, &bAddr, 1, pData, uCount);
	}

UINT CRtcAbmc::ToBin(BYTE bData, UINT uWidth)
{
	BYTE bLoMask = ((BYTE(0xFF) >> (8 - uWidth)) >> 0) & 0x0F;

	BYTE bHiMask = ((BYTE(0xFF) >> (8 - uWidth)) >> 4) & 0x0F;

	return (((bData >> 4) & bHiMask) * 10) + ((bData >> 0) & bLoMask);
	}

BYTE CRtcAbmc::ToBcd(UINT uData, UINT uWidth)
{
	BYTE bLoMask = ((BYTE(0xFF) >> (8 - uWidth)) >> 0) & 0x0F;

	BYTE bHiMask = ((BYTE(0xFF) >> (8 - uWidth)) >> 4) & 0x0F;

	return (((uData / 10) & bHiMask) << 4) | (((uData % 10) & bLoMask) << 0);
	}

// End of File
