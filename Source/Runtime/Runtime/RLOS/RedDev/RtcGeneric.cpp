
#include "Intern.hpp"

#include "RtcGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Dead Reckoning RTC
//

// Constructor

CRtcGeneric::CRtcGeneric(void)
{
	StdSetRef();

	m_pTimer  = CreateTimer();

	m_uSecond = ToTicks(1000);

	m_uAdjust = ToTicks(250);

	m_uTarget = m_uSecond;

	m_uTicks  = 0;

	m_uSync   = 0;

	m_uMono   = 0;

	m_fInit   = false;
}

// Destructor

CRtcGeneric::~CRtcGeneric(void)
{
	m_pTimer->Release();
}

// IUnknown

HRESULT CRtcGeneric::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IRtc);

	return E_NOINTERFACE;
}

ULONG CRtcGeneric::AddRef(void)
{
	StdAddRef();
}

ULONG CRtcGeneric::Release(void)
{
	StdRelease();
}

// IDevice

BOOL METHOD CRtcGeneric::Open(void)
{
	if( InitChip() ) {

		m_fInit = true;

		ReadTime();
	}
	else {
		AfxTrace("\n*** Cannot enable RTC\n");

		m_time = 0;
	}

	EnableEvents();

	return TRUE;
}

// IRtc

void CRtcGeneric::GetTime(struct timeval *tv)
{
	UINT  irql  = Hal_RaiseIrql(IRQL_TIMER);

	tv->tv_sec  = m_time;

	tv->tv_usec = 1000000 * m_uTicks / m_uSecond;

	Hal_LowerIrql(irql);
}

bool CRtcGeneric::SetTime(struct timeval const *tv)
{
	CAutoGuard Guard;

	m_time = tv->tv_sec + ((tv->tv_usec & 0x80000000) ? 1 : 0);

	if( m_fInit ) {

		WriteTimeToChip();
	}

	return true;
}

UINT CRtcGeneric::GetCalibStatus(void)
{
	return rtcCalibNone;
}

INT CRtcGeneric::GetCalib(void)
{
	return 100;
}

bool CRtcGeneric::PutCalib(INT nCalib)
{
	return true;
}

UINT CRtcGeneric::GetMonotonic(void)
{
	return m_uMono;
}

// IEventSink

void CRtcGeneric::OnEvent(UINT uLine, UINT uParam)
{
	if( ++m_uTicks >= m_uTarget ) {

		m_uTarget = m_uSecond;

		m_time++;

		if( m_fInit ) {

			if( ++m_uSync >= 600 ) {

				UINT uSecs = m_time % 60;

				if( uSecs >= 15 && uSecs <= 45 ) {

					UINT uRead;

					if( GetSecsFromChip(uRead) ) {

						if( uRead < uSecs ) {

							m_uTarget += m_uAdjust;
						}

						if( uRead > uSecs ) {

							m_uTarget -= m_uAdjust;
						}

						m_uSync = 0;
					}
				}
			}
		}

		m_uTicks = 0;
	}

	m_uMono++;
}

// Implementation

bool CRtcGeneric::ReadTime(void)
{
	if( !GetTimeFromChip() || m_time < 1546300800 ) {

		m_time = 1546300800;

		WriteTimeToChip();

		return false;
	}

	return true;
}

void CRtcGeneric::EnableEvents(void)
{
	m_pTimer->SetPeriod(5);

	m_pTimer->SetHook(this, 0);

	m_pTimer->Enable(true);
}

// End of File
