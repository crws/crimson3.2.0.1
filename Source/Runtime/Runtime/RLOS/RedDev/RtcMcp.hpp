
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RtcMcp_HPP
	
#define	INCLUDE_RtcMcp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RtcGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MCP7941x Real Time Clock
//

class CRtcMcp : public CRtcGeneric
{
	public:
		// Constructor
		CRtcMcp(UINT uSlot);

		// Destructor
		~CRtcMcp(void);

		// IRtc
		UINT  METHOD GetCalibStatus(void);
		INT   METHOD GetCalib(void);
		bool  METHOD PutCalib(INT Calib);

	protected:
		// Constants
		static const DWORD constRtcMagic;

		// Data Members
		ISerialMemory  * m_pMem;
		II2c           * m_pI2c;
		BYTE		 m_bChip;
		UINT             m_uSlot;
		
		// Overrideables
		bool InitChip(void);
		bool GetTimeFromChip(void);
		bool GetSecsFromChip(UINT &uSecs);
		bool WriteTimeToChip(void);		

		// Implementation
		bool PutData(BYTE bAddr, PBYTE pData, UINT uCount);
		bool GetData(BYTE bAddr, PBYTE pData, UINT uCount);
		void Start(void);
		void Stop(void);
		INT  GetCalFromChip(void);
		bool WriteCalToChip(INT nCalib);
		void SaveCalib(INT nCalib);
		void CalibFromMem(void);
		UINT ToBin(BYTE bData, UINT uWidth);
		BYTE ToBcd(UINT uData, UINT uWidth);
	};

// End of File

#endif
