
#include "Intern.hpp"

#include "Temp112x.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// TMP112x Digital Temperature Sensor
//

// Instantiator

ISensor * Create_Temp112x(BYTE bAddr, INT nLimitLo, INT nLimitHi)
{
	CTemp112x *pDevice = New CTemp112x(bAddr, nLimitLo, nLimitHi);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CTemp112x::CTemp112x(BYTE bAddr, INT nLimitLo, INT nLimitHi)
{
	StdSetRef();

	m_bChip    = bAddr;

	m_nLimitLo = nLimitLo;

	m_nLimitHi = nLimitHi;

	m_fExtend  = false;

	m_fIntMode = false;

	AfxGetObject("dev.i2c", 0, II2c, m_pI2c);
	}

// Destructor

CTemp112x::~CTemp112x(void)
{
	m_pI2c->Release();
	}

// IUnknown

HRESULT CTemp112x::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ISensor);

	return E_NOINTERFACE;
	}

ULONG CTemp112x::AddRef(void)
{
	StdAddRef();
	}

ULONG CTemp112x::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CTemp112x::Open(void)
{
	WORD wConfig = 0;

	wConfig |= (configCR0 | configCR1);

	wConfig |= (m_fExtend ? configEM : 0);

	wConfig |= (m_fIntMode ? configTM : 0);

	PutData(addrConfig, wConfig);

	if( m_nLimitLo != INT(NOTHING) ) {

		PutData(addrLo, EncodeTemp(m_nLimitLo));
		}

	if( m_nLimitHi != INT(NOTHING) ) {

		PutData(addrHi, EncodeTemp(m_nLimitHi));
		}

	return TRUE;
	}

// ISensor

INT METHOD CTemp112x::GetData(void)
{
	return DecodeTemp(GetData(addrTemp));
	}

BOOL METHOD CTemp112x::GetAlarm(void)
{
	WORD wConfig   = GetData(addrConfig);

	BOOL fAlert    = !!(wConfig & configAL);

	BOOL fActiveHi = !!(wConfig & configPOL);

	return fActiveHi ? fAlert : !fAlert;
	}

// Encoding

WORD CTemp112x::EncodeTemp(INT nData) const
{
	WORD wData = WORD((LONG(nData) * 1000) / 625);

	wData <<= (m_fExtend ? 3 : 4);

	wData |=  (m_fExtend ? Bit(0) : 0);

	return wData;
	}

INT CTemp112x::DecodeTemp(WORD wData) const
{
	INT nData = INT(((LONG)((SHORT) wData) * 625) / 1000);

	BOOL fExt = wData & Bit(0);

	nData /= (fExt ? (1 << 3) : (1 << 4));

	return nData;
	}

// Register Access

WORD CTemp112x::GetData(BYTE bAddr)
{
	WORD wData = 0;

	if( GetData(bAddr, PBYTE(&wData), sizeof(wData)) ) {

		return MotorToHost(wData);
		}

	return 0;
	}

bool CTemp112x::PutData(BYTE bAddr, WORD wData)
{
	wData = HostToMotor(wData);

	return PutData(bAddr, PBYTE(&wData), sizeof(wData));
	}

bool CTemp112x::GetData(BYTE bAddr, PBYTE pData, UINT uCount)
{
	m_pI2c->Lock(FOREVER);

	if( m_pI2c->Recv(m_bChip, &bAddr, 1, pData, uCount) ) {

		m_pI2c->Free();

		return true;
		}

	m_pI2c->Free();

	return false;
	}

bool CTemp112x::PutData(BYTE bAddr, PCBYTE pData, UINT uCount)
{
	m_pI2c->Lock(FOREVER);

	if( m_pI2c->Send(m_bChip, &bAddr, 1, pData, uCount) ) {

		m_pI2c->Free();

		return true;
		}

	m_pI2c->Free();

	return false;
	}

// End of File
