
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Mr25Hxx_HPP
	
#define	INCLUDE_Mr25Hxx_HPP

//////////////////////////////////////////////////////////////////////////
//
// Serial SPI MRAM 
//

class CMr25Hxx : public ISerialMemory
{
	public:
		// Constructor
		CMr25Hxx(UINT uChan);

		// Destructor
		~CMr25Hxx(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ISerialMemory
		UINT METHOD GetSize(void);
		BOOL METHOD IsFast(void);
		BOOL METHOD GetData(UINT uAddr, PBYTE  pData, UINT uCount);
		BOOL METHOD PutData(UINT uAddr, PCBYTE pData, UINT uCount);

	protected:
		// Data Members
		ULONG   m_uRefs;
		ISpi  * m_pSpi;
		UINT    m_uChan;

		// Implementation
		bool WriteEnable(bool fEnable);
	};

// End of File

#endif
