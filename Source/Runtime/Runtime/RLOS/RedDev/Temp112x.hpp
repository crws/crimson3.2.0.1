
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Temp112x_HPP
	
#define	INCLUDE_Temp112x_HPP

//////////////////////////////////////////////////////////////////////////
//
// TMP112x Digital Temperature Sensor
//

class CTemp112x : public ISensor
{
	public:
		// Constructor
		CTemp112x(BYTE bAddr, INT nLimitLo, INT nLimitHi);

		// Destructor
		~CTemp112x(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ISensor
		INT  METHOD GetData(void);
		BOOL METHOD GetAlarm(void);

	protected:
		// Registers
		enum
		{
			addrTemp	= 0x00,
			addrConfig	= 0x01,
			addrLo		= 0x02,
			addrHi		= 0x03,
			};

		// Config
		enum
		{
			configOS	= Bit(15),
			configR1	= Bit(14),
			configR0	= Bit(13),
			configF1	= Bit(12),
			configF0	= Bit(11),
			configPOL	= Bit(10),
			configTM	= Bit( 9),
			configSD	= Bit( 8),
			configCR1	= Bit( 7),
			configCR0	= Bit( 6),
			configAL	= Bit( 5),
			configEM	= Bit( 4),
			};

		// Data Members
		ULONG	m_uRefs;
		II2c  * m_pI2c;
		BYTE	m_bChip;
		INT 	m_nLimitHi;
		INT	m_nLimitLo;
		bool    m_fExtend;
		bool	m_fIntMode;

		// Encoding
		WORD EncodeTemp(INT nData) const;
		INT  DecodeTemp(WORD wData) const;

		// Register Access
		WORD GetData(BYTE bAddr);
		bool PutData(BYTE bAddr, WORD wData);
		bool GetData(BYTE bAddr, PBYTE pData, UINT uCount);
		bool PutData(BYTE bAddr, PCBYTE pData, UINT uCount);
	};

// End of File

#endif
