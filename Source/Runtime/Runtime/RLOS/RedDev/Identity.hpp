#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Identity_HPP
	
#define	INCLUDE_Identity_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/IdentityBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Sitara Identity Object
//

class CIdentity : public CIdentityBase
{
	public:
		// Constructor
		CIdentity(UINT uAddr);

		// Layout
		enum 
		{
			propSeries	= 0x0008, // WORD 
			propModel	= 0x000A, // WORD
			propVariant	= 0x000C, // WORD
			propBacklight	= 0x000E, // WORD
			propSerial	= 0x0010, // WORD
			};

	protected:
		// Implementation
		void OnInit(void);
	};

// End of File

#endif
