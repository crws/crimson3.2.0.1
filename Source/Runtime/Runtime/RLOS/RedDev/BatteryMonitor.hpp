
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_BatteryMonitor_HPP
	
#define	INCLUDE_BatteryMonitor_HPP

//////////////////////////////////////////////////////////////////////////
//
// Interfaces
//

#include "../../StdEnv/IRtc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Battery Monitor Proxy Object
//

class CBatteryMonitor : public IBatteryMonitor
{
	public:
		// Constructor
		CBatteryMonitor(IBatteryMonitor *pBattMon);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IBatteryMonitor
		bool METHOD IsBatteryGood(void);
		bool METHOD IsBatteryBad(void);

	protected:
		// Data Members
		ULONG             m_uRefs;
		IBatteryMonitor * m_pBattMon;
	};

// End of File

#endif
