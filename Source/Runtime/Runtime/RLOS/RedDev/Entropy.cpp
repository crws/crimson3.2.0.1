
#include "Intern.hpp"

#include "Entropy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Entropy Source
//

// Instantiator

IDevice * Create_Entropy(void)
{
	return New CEntropy;
	}

// Static Data

BYTE CEntropy::m_bData[256];

UINT CEntropy::m_uHead = 0;

UINT CEntropy::m_uTail = 0;

// Accumulation

void AccumulateEntropy(BYTE b)
{
	CEntropy::m_bData[CEntropy::m_uTail] ^= b;

	CEntropy::m_uTail = (CEntropy::m_uTail+1) % elements(CEntropy::m_bData);
	}

void AccumulateEntropy(WORD w)
{
	BYTE b = ((w >>  8) & 255)
	       ^ ((w >>  0) & 255);

	AccumulateEntropy(b);
	}

void AccumulateEntropy(DWORD d)
{
	BYTE b = ((d >> 24) & 255)
	       ^ ((d >> 16) & 255)
	       ^ ((d >>  8) & 255)
	       ^ ((d >>  0) & 255);

	AccumulateEntropy(b);
	}

// Constructor

CEntropy::CEntropy(void)
{
	StdSetRef();
	}

// Destructor

CEntropy::~CEntropy(void)
{
	}

// IUnknown

HRESULT CEntropy::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IEntropy);

	return E_NOINTERFACE;
	}

ULONG CEntropy::AddRef(void)
{
	StdAddRef();
	}

ULONG CEntropy::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL CEntropy::Open(void)
{
	return TRUE;
	}

// IEntropy

BOOL CEntropy::GetEntropy(PBYTE pData, UINT uData)
{
	for( UINT n = 0; n < uData; n++ ) {

		pData[n] = m_bData[m_uHead];

		m_uHead  = (m_uHead+1) % elements(m_bData);
		}

	return TRUE;
	}

// End of File
