 
#include "Intern.hpp"

#include "SdHostGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic SD Host Controller
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CSdHostGeneric::CSdHostGeneric(void)
{
	StdSetRef();

	m_pBase      = NULL;

	m_pEventCard = NULL;

	m_Card       = cardNone;

	m_fBusy      = false;

	m_Rca	     = 0;

	m_fAllowFast = true;

	m_fAllowWide = true;
	}

// Destructor

CSdHostGeneric::~CSdHostGeneric(void)
{
	}

// IUnknown

HRESULT CSdHostGeneric::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ISdHost);

	return E_NOINTERFACE;
	}

ULONG CSdHostGeneric::AddRef(void)
{
	StdAddRef();
	}

ULONG CSdHostGeneric::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CSdHostGeneric::Open(void)
{
	return TRUE;
	}

// ISdHost

BOOL METHOD CSdHostGeneric::IsCardReady(void)
{
	if( m_Card == cardNone || m_Card == cardBounce || m_Card == cardInvalid ) {

		return FALSE;
		}

	if( m_Card == cardInserted ) {

		return InitCard();
		}

	return TRUE;
	}

UINT METHOD CSdHostGeneric::GetSectorCount(void)
{
	if( m_Card >= cardValid ) {

		return m_uCount;
		}

	return 0;
	}

UINT METHOD CSdHostGeneric::GetSerialNumber(void)
{
	if( m_Card >= cardValid ) {

		return m_Psn;
		}

	return 0;
	}

void METHOD CSdHostGeneric::AttachCardEvent(IEvent *pEvent)
{
	m_pEventCard = pEvent;

	if( IsCardReady() ) {

		FireCardEvent();
		}
	}

BOOL METHOD CSdHostGeneric::WriteSector(UINT uSect, PCBYTE pData)
{
	if( m_Card >= cardValid ) {

		if( uSect < m_uCount ) {

			if( m_Card == cardSD1 || m_Card == cardSD2 ) {

				uSect <<= 9;
				}

			if( CheckBusy() ) {

				SetLength(512);

				if( ExecCmd(24, uSect, respR1, dataWrite) ) {

					if( SendData(pData, 512) ) {

						m_fBusy = true;

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL METHOD CSdHostGeneric::ReadSector(UINT uSect, PBYTE pData)
{
	if( m_Card >= cardValid ) {

		if( uSect < m_uCount ) {

			if( m_Card == cardSD1 || m_Card == cardSD2 ) {

				uSect <<= 9;
				}

			if( CheckBusy() ) {

				SetLength(512);

				if( ExecCmd(17, uSect, respR1, dataRead) ) {

					if( ReadData(pData, 512) ) {

						m_fBusy = true;

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

// Card Management

bool CSdHostGeneric::InitCard(void)
{
	ResetCard();

	if( FindCardVersion() ) {

		DWORD Arg = Bit(28) | m_uVoltage;

		if( m_Card == cardSD2 ) {

			Arg |= Bit(30);
			}

		for( UINT n = 0; n < 100; n++ ) {

			if( ExecApp(41, Arg, respR3, dataNone) ) {

				if( Reg(CmdRsp0) & Bit(31) ) {

					if( Reg(CmdRsp0) & Bit(30) ) {

						m_Card = cardHC2;
						}

					if( GetCardIdentity() ) {

						SetClockSpeed(clock25MHz);
						
						if( GetCardStructure() ) {

							if( SelectCard() ) {

								if( GetCardConfig() ) {

									ShowCardData();

									return true;
									}
								}
							}
						}

					return false;
					}

				Sleep(10);

				continue;
				}

			MarkInvalid("No Response on Init Poll");

			return false;
			}

		MarkInvalid("Timeout on Init Poll");
		
		return false;
		}

	return false;
	}

void CSdHostGeneric::ShowCardData(void)
{
	#if defined(_XDEBUG)

	AfxTrace("\n");

	AfxTrace("### Card Type %u Initialized\n\n", m_Card);

	AfxTrace("  Mid  = %2.2X\n", m_Mid);
	AfxTrace("  Psn  = %8.8X\n", m_Psn);
	AfxTrace("  Pnm  = %s\n",    m_Pnm);
	AfxTrace("  Prv  = %s\n",    m_Prv);
	AfxTrace("  Rca  = %8.8X\n", m_Rca);
	AfxTrace("  Fast = %u\n",    m_fUsingFast);
	AfxTrace("  Wide = %u\n",    m_fUsingWide);

	AfxTrace("\n%u bytes available\n", 512 * m_uCount);

	#endif
	}

bool CSdHostGeneric::FindCardVersion(void)
{
	if( ExecCmd(8, 0x000001AA, respR6, dataNone) ) {

		if( (Reg(CmdRsp0) & 0x0FFF) == 0x01AA ) {

			m_Card = cardSD2;

			if( ExecApp(41, 0, respR3, dataNone) ) {

				if( Reg(CmdRsp0) & m_uVoltage ) {

					return true;
					}
	
				MarkInvalid("Unsupported Voltage");

				return false;
				}

			MarkInvalid("No Response to Voltage Check");

			return false;
			}

		MarkInvalid("Invalid Response to CMD8");

		return false;
		}

	m_Card = cardSD1;

	return true;
	}

bool CSdHostGeneric::GetCardIdentity(void)
{
	if( ExecCmd(2, 0, respR2, dataNone) ) {

		BYTE b[16];

		((PDWORD) b)[0] = Reg(CmdRsp0);
		((PDWORD) b)[1] = Reg(CmdRsp1);
		((PDWORD) b)[2] = Reg(CmdRsp2);
		((PDWORD) b)[3] = Reg(CmdRsp3);

		if( DecodeIdentity(b) ) {

			if( ExecCmd(3, 0, respR6, dataNone) ) {

				m_Rca = (Reg(CmdRsp0) & 0xFFFF0000);

				return true;
				}

			MarkInvalid("Failed to read RCA");

			return false;
			}

		MarkInvalid("Failed to decode CID");

		return false;
		}

	MarkInvalid("Failed to read CID");

	return false;
	}

bool CSdHostGeneric::GetCardStructure(void)
{
	if( ExecCmd(9, m_Rca, respR2, dataNone) ) {

		BYTE b[16];

		((PDWORD) b)[0] = Reg(CmdRsp0);
		((PDWORD) b)[1] = Reg(CmdRsp1);
		((PDWORD) b)[2] = Reg(CmdRsp2);
		((PDWORD) b)[3] = Reg(CmdRsp3);

		OffsetStructure(b);

		if( DecodeStructure(b) ) {

			return true;
			}

		MarkInvalid("Failed to decode CSD");

		return false;
		}

	MarkInvalid("Failed to read CSD");

	return false;
	}

bool CSdHostGeneric::SelectCard(void)
{
	if( ExecCmd(7, m_Rca, respR1b, dataNone) ) {

		if( ExecCmd(16, 512, respR1, dataNone) ) {

			return true;
			}

		MarkInvalid("Failed to Set Length");

		return false;
		}

	MarkInvalid("Failed to Select Card");

	return false;
	}

bool CSdHostGeneric::GetCardConfig(void)
{
	BYTE b[8];

	SetLength(sizeof(b));

	if( ExecApp(51, 0, respR1, dataRead) ) {

		if( ReadData(b, sizeof(b)) ) {

			if( DecodeConfig(b) ) {

				if( CheckWide() ) {

					if( CheckFast() ) {

						return true;
						}
					}

				return false;
				}

			MarkInvalid("Failed to decode SCR");

			return false;
			}
		}

	MarkInvalid("Failed to read SCR");

	return false;
	}

bool CSdHostGeneric::DecodeIdentity(PCBYTE b)
{
	m_Mid = b[14];

	m_Psn = MAKELONG(MAKEWORD(b[2],b[3]),MAKEWORD(b[4],b[5]));

	m_Pnm[0] = b[11];
	m_Pnm[1] = b[10];
	m_Pnm[2] = b[ 9];
	m_Pnm[3] = b[ 8];
	m_Pnm[4] = b[ 7];
	m_Pnm[5] = 0x00;

	m_Prv[0] = '0' + b[6] / 16;
	m_Prv[2] = '0' + b[6] % 16;

	m_Prv[1] = 0x2E;
	m_Prv[3] = 0x00;

	return true;
	}

bool CSdHostGeneric::DecodeStructure(PCBYTE b)
{
	UINT uStruct = DecodeField(b, 126, 2);
	
	if( uStruct == 0 ) {

		UINT rl  = 1 << DecodeField(b, 80, 4);

		UINT sv  = 4 << DecodeField(b, 47, 3);

		UINT cs  = DecodeField(b, 62, 12) + 1;

		m_uCount = (rl >> 9) * sv * cs;

		return true;
		}

	if( uStruct == 1 ) {

		UINT cs  = DecodeField(b, 48, 22);

		m_uCount = (cs + 1) * 1024;

		return true;
		}

	return false;
	}

bool CSdHostGeneric::DecodeConfig(PCBYTE b)
{
	if( m_fAllowWide ) {

		m_fUsingWide = (b[1] & 0x04) ? true : false;

		return true;
		}

	m_fUsingWide = false;

	return true;
	}

UINT CSdHostGeneric::DecodeField(PCBYTE b, UINT uFrom, UINT uBits)
{
	UINT p = uFrom / 8;

	UINT i = uFrom % 8;

	BYTE m = BYTE(Bit(i));

	UINT v = 0;

	UINT s = 1;

	while( uBits-- ) {

		if( b[p] & m ) {

			v |= s;
			}

		if( !(m <<= 1) ) {

			m = 1;

			p = p + 1;
			}

		s <<= 1;
		}

	return v;
	}

bool CSdHostGeneric::CheckWide(void)
{
	if( m_fUsingWide ) {

		if( ExecApp(6, 2, respR1, dataNone) ) {

			SetBusWidth(true);

			return true;
			}

		MarkInvalid("Failed to Switch to 4-Bit");

		return false;
		}

	m_fUsingWide = false;

	return true;
	}

bool CSdHostGeneric::CheckFast(void)
{
	if( m_fAllowFast ) {
		
		BYTE b[64];

		SetLength(sizeof(b));

		if( ExecCmd(6, 0x00000000, respR1, dataRead) ) {

			if( ReadData(b, sizeof(b)) ) {

				if( b[13] & 0x02 ) {

					if( ExecCmd(6, 0x80000001, respR1, dataRead) ) {

						if( ReadData(b, sizeof(b)) ) {
						
							SetClockSpeed(clock50MHz);

							m_fUsingFast = true;

							return true;
							}
						}

					MarkInvalid("Failed to Switch to 50MHz");

					return false;
					}
				}
			}
		}

	m_fUsingFast = false;

	return true;
	}

// Card Interface

void CSdHostGeneric::ResetCard(void)
{
	// TODO -- What is the exact delay needed here?

	SetClockSpeed(clockSlow);

	SetBusWidth  (false);

	ExecCmd(0, 0, respR0, dataNone);
	
	for( UINT i = 0; i < 4000; i ++ ) {

		#if !defined(__INTELLISENSE__)
		
		ASM("nop\r\n");
		
		#endif
		}

	m_fBusy = false;

	m_Rca	= 0;
	}

bool CSdHostGeneric::ExecApp(BYTE bCmd, DWORD Arg, EResp Resp, EData Data)
{
	if( ExecCmd(55, m_Rca, respR1, dataNone) ) {

		if( ExecCmd(bCmd, Arg, Resp, Data) ) {

			return true;
			}
		}

	return false;
	}

bool CSdHostGeneric::ExecCmd(BYTE bCmd, DWORD Arg, EResp Resp, EData Data)
{
	ClearEvents();

	BYTE RSPTYPE = 0;
	BYTE CICEN   = 0;
	BYTE CCCEN   = 0;
	BYTE DPSEL   = 0;
	BYTE DTDSEL  = 0;

	switch( Resp ) {

		case respR0:

			RSPTYPE = 0;
			CICEN   = 0;
			CCCEN   = 0;
			break;

		case respR2:

			RSPTYPE = 1;
			CICEN   = 0;
			CCCEN   = 1;
			break;

		case respR3:
		case respR4:

			RSPTYPE = 2;
			CICEN   = 0;
			CCCEN   = 0;
			break;

		case respR1:
		case respR5:
		case respR6:

			RSPTYPE = 2;
			CICEN   = 1;
			CCCEN   = 1;
			break;

		case respR1b:
		case respR5b:

			RSPTYPE = 3;
			CICEN   = 1;
			CCCEN   = 1;
			break;
		}

	switch( Data ) {

		case dataNone:

			DPSEL  = 0;
			DTDSEL = 0;
			break;

		case dataRead:

			DPSEL  = 1;
			DTDSEL = 1;
			break;

		case dataWrite:

			DPSEL  = 1;
			DTDSEL = 0;
			break;
		}

	Reg(IrqStat)  = 0xFFFFFFFF;

	Reg(CmdArg)   = Arg;

	Reg(XferType) = (bCmd    << 24) |
			(DPSEL   << 21) |
			(CICEN   << 20) |
			(CCCEN   << 19) |
			(RSPTYPE << 16) |
			(DTDSEL  <<  4) ;

	return WaitCmdComplete();
	}

void CSdHostGeneric::MarkInvalid(PCSTR pText)
{
	AfxTrace("*** %s\n", pText);

	m_Card = cardInvalid;
	}

// Blocking Calls

void CSdHostGeneric::ClearEvents(void)
{
	}

bool CSdHostGeneric::WaitCmdComplete(void)
{
	WaitForEvent(Bit(0));

	if( Reg(IrqStat) & Bit(16) ) {

		Reg(IrqStat) = Bit(16);
		
		return false;
		}

	return true;
	}

bool CSdHostGeneric::SendData(PCBYTE pData, UINT uCount)
{
	if( uCount % 4 == 0 ) {

		WaitForEvent(Bit(4));

		for( UINT n = 0; n < uCount / 4; n++ ) {

			Reg(DatPort) = ((PDWORD) pData)[n];
			}

		return true;
		}

	return false;
	}

bool CSdHostGeneric::ReadData(PBYTE pData, UINT uCount)
{
	if( uCount % 4 == 0 ) {

		WaitForEvent(Bit(5));

		for( UINT n = 0; n < uCount / 4; n++ ) {

			((PDWORD) pData)[n] = Reg(DatPort);
			}

		return true;
		}

	return false;
	}

bool CSdHostGeneric::CheckBusy(void)
{
	if( m_fBusy ) {

		WaitForEvent(Bit(1));

		m_fBusy	= false;

		return true;
		}

	return true;
	}

// Response Offsets

void CSdHostGeneric::OffsetStructure(PBYTE b)
{
	}

// Implementation

void CSdHostGeneric::WaitForEvent(UINT uMask)
{
	for(;;) {

		// TODO -- What if the card is pulled out or
		// some error occurs so that we never get this
		// interrupt. Or does it always fire anyway?

		if( Reg(IrqStat) & uMask ) {

			Reg(IrqStat) = uMask;

			return;
			}
		
		#if !defined(__INTELLISENSE__)
		
		ASM("nop\r\n");
		
		#endif
		}
	}

void CSdHostGeneric::FireCardEvent(void)
{
	if( m_pEventCard ) {

		m_pEventCard->Set();
		}
	}

// End of File
