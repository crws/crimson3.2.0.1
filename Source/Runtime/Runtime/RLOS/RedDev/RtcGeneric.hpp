
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RtcGeneric_HPP
	
#define	INCLUDE_RtcGeneric_HPP

//////////////////////////////////////////////////////////////////////////
//
// Interfaces
//

#include "../../StdEnv/IRtc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Dead Reckoning RTC
//

class CRtcGeneric : public IRtc, public IEventSink
{
	public:
		// Constructor
		CRtcGeneric(void);

		// Destructor
		virtual ~CRtcGeneric(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IRtc
		void METHOD GetTime(struct timeval *tv);
		bool METHOD SetTime(struct timeval const *tv);
		UINT METHOD GetCalibStatus(void);
		INT  METHOD GetCalib(void);
		bool METHOD PutCalib(INT Calib);
		UINT METHOD GetMonotonic(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Data Members
		ULONG        m_uRefs;
		ITimer     * m_pTimer;
		UINT	     m_uSecond;
		UINT	     m_uAdjust;
		UINT	     m_uTarget;
		UINT	     m_uTicks;
		UINT	     m_uSync;
		UINT	     m_uMono;
		bool	     m_fInit;
		time_t	     m_time;

		// Implementation
		bool ReadTime(void);
		void EnableEvents(void);

		// Overridables
		virtual bool InitChip(void)               = 0;
		virtual bool GetTimeFromChip(void)        = 0;
		virtual bool GetSecsFromChip(UINT &uSecs) = 0;
		virtual bool WriteTimeToChip(void)        = 0;
	};

// End of File

#endif
