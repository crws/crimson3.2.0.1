
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SdHostGeneric_HPP
	
#define	INCLUDE_SdHostGeneric_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Hardware Drivers
//

class CSdHostGeneric : public ISdHost
{
	public:
		// Constructor
		CSdHostGeneric(void);

		// Destructor
		virtual ~CSdHostGeneric(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ISdHost
		BOOL METHOD IsCardReady(void);
		UINT METHOD GetSectorCount(void);
		UINT METHOD GetSerialNumber(void);
		void METHOD AttachCardEvent(IEvent *pEvent);
		BOOL METHOD WriteSector(UINT uSect, PCBYTE pData);
		BOOL METHOD ReadSector(UINT uSect, PBYTE pData);

	protected:
		// Registers
		enum
		{
			regDmaAddr   = 0x0000 / sizeof(DWORD),
			regBlkAttr   = 0x0004 / sizeof(DWORD),
			regCmdArg    = 0x0008 / sizeof(DWORD),
			regXferType  = 0x000C / sizeof(DWORD),
			regCmdRsp0   = 0x0010 / sizeof(DWORD),
			regCmdRsp1   = 0x0014 / sizeof(DWORD),
			regCmdRsp2   = 0x0018 / sizeof(DWORD),
			regCmdRsp3   = 0x001C / sizeof(DWORD),
			regDatPort   = 0x0020 / sizeof(DWORD),
			regState     = 0x0024 / sizeof(DWORD),
			regProCtl    = 0x0028 / sizeof(DWORD),
			regSysCtl    = 0x002C / sizeof(DWORD),
			regIrqStat   = 0x0030 / sizeof(DWORD),
			regIrqStatEn = 0x0034 / sizeof(DWORD),
			regIrqLineEn = 0x0038 / sizeof(DWORD),
			regAutoErr   = 0x003C / sizeof(DWORD),
			regHostCaps  = 0x0040 / sizeof(DWORD),
			regWatermark = 0x0044 / sizeof(DWORD),
			regForceEvnt = 0x0050 / sizeof(DWORD),
			regControl2  = 0x0080 / sizeof(DWORD),
			regControl3  = 0x0084 / sizeof(DWORD),
			};

		// Responses
		enum EResp
		{
			respR0,
			respR1,
			respR1b,
			respR2,
			respR3,
			respR4,
			respR5,
			respR5b,
			respR6
			};

		// Directions
		enum EData
		{
			dataNone,
			dataRead,
			dataWrite
			};

		// Card Types
		enum ECard
		{
			cardNone,
			cardBounce,
			cardInserted,
			cardInvalid,
			cardValid,
			cardSD1,
			cardSD2,
			cardHC2,
			};

		// Clock Speeds
		enum EClock
		{
			clockSlow,
			clock25MHz,
			clock50MHz
			};

		// Data Members
		PVDWORD	     m_pBase;
		ULONG        m_uRefs;
		IEvent     * m_pEventCard;
		UINT	     m_uVoltage;
		ECard        m_Card;
		BYTE	     m_Mid;
		char	     m_Pnm[6];
		char	     m_Prv[4];
		DWORD	     m_Psn;
		DWORD	     m_Rca;
		bool	     m_fAllowFast;
		bool	     m_fAllowWide;
		bool	     m_fUsingWide;
		bool	     m_fUsingFast;
		UINT	     m_uCount;
		bool	     m_fBusy;

		// Card Management
		bool InitCard(void);
		void ShowCardData(void);
		bool FindCardVersion(void);
		bool GetCardIdentity(void);
		bool GetCardStructure(void);
		bool SelectCard(void);
		bool GetCardConfig(void);
		bool DecodeIdentity(PCBYTE b);
		bool DecodeStructure(PCBYTE b);
		bool DecodeConfig(PCBYTE b);
		UINT DecodeField(PCBYTE b, UINT uFrom, UINT uBits);
		bool CheckWide(void);
		bool CheckFast(void);

		// Card Interface
		void ResetCard(void);
		bool ExecApp(BYTE bCmd, DWORD Arg, EResp Resp, EData Data);
		bool ExecCmd(BYTE bCmd, DWORD Arg, EResp Resp, EData Data);
		void MarkInvalid(PCSTR pError);

		// Controller Interface
		virtual void SetClockSpeed(EClock Clock) = 0;
		virtual void SetBusWidth(bool fWide)     = 0;
		virtual void SetLength(UINT uBytes)      = 0;

		// Blocking Calls
		virtual void ClearEvents(void);
		virtual bool WaitCmdComplete(void);
		virtual bool SendData(PCBYTE pData, UINT uCount);
		virtual bool ReadData(PBYTE pData, UINT uCount);
		virtual bool CheckBusy(void);

		// Response Offsets
		virtual void OffsetStructure(PBYTE b);

		// Implementation
		void WaitForEvent(UINT uMask);
		void FireCardEvent(void);
	};

// End of File

#endif
