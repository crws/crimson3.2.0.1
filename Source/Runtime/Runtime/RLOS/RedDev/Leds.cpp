
#include "Intern.hpp"

#include "Leds.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LEDs Generic Wrapper Object
//

// Instantiator

IDevice * Create_Leds(ILeds *pLeds)
{
	IDevice *pDevice = New CLeds(pLeds);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CLeds::CLeds(ILeds *pLeds)
{
	StdSetRef();

	m_pLeds = pLeds;
	}

// IUnknown

HRESULT CLeds::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ILeds);

	StdQueryInterface(ILeds);

	return E_NOINTERFACE;
	}

ULONG CLeds::AddRef(void)
{
	StdAddRef();
	}

ULONG CLeds::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CLeds::Open(void)
{
	return TRUE;
	}

// ILeds

void METHOD CLeds::SetLed(UINT uLed, UINT uState)
{
	m_pLeds->SetLed(uLed, uState);
	}

void METHOD CLeds::SetLedLevel(UINT uPercent, bool fPersist)
{
	m_pLeds->SetLedLevel(uPercent, fPersist);
	}
	
UINT METHOD CLeds::GetLedLevel(void)
{
	return m_pLeds->GetLedLevel();
	}

// End of File
