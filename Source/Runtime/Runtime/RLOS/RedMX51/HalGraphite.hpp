
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_HalGraphite_HPP

#define INCLUDE_HalGraphite_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HalMX51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Graphite HMI
//

class CHalGraphite : public CHalMX51
{
	public:
		// Constructor
		CHalGraphite(void);
	};

// End of File

#endif
