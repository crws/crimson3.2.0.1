
#include "Intern.hpp"

#include "UsbDev51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define Reg(x)   (m_pBase[reg##x + m_uBase])

//////////////////////////////////////////////////////////////////////////
//
// iMX51 USB Device Controller
//

// Instantiator

IUsbDriver * Create_UsbFunc51(UINT iIndex)
{
	CUsbDev51 *p = New CUsbDev51(iIndex);

	return p;
	}

// Constructor

CUsbDev51::CUsbDev51(UINT iIndex) : CUsbBase51(iIndex)
{
	SetMode(modeDevice);	
	
	MakeQueueHeads();

	MakeTransfer(8);

	m_pLock = Create_Mutex();
	
	m_pFlag = Create_Semaphore();

	m_pFlag->Signal(8);

	m_fHiSpeed = true;

	m_pDriver  = NULL;
	}

// Destructor

CUsbDev51::~CUsbDev51(void)
{
	KillQueueHeads();

	KillTransfer();

	m_pFlag->Release();

	m_pLock->Release();
	
	AfxRelease(m_pDriver);
	}

// IUnknown

HRESULT CUsbDev51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbFuncHardwareDriver);

	StdQueryInterface(IUsbFuncHardwareDriver);

	StdQueryInterface(IUsbDriver);

	return E_NOINTERFACE;
	}

ULONG CUsbDev51::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbDev51::Release(void)
{
	StdRelease();
	}

// IUsbDriver

BOOL CUsbDev51::Bind(IUsbEvents *pDriver)
{
	if( pDriver ) {

		AfxRelease(m_pDriver);

		m_pDriver = NULL;

		pDriver->QueryInterface(AfxAeonIID(IUsbFuncHardwareEvents), (void **) &m_pDriver);

		if( m_pDriver ) {
		
			m_pDriver->OnBind(this);

			return true;
			}
		}

	return false;
	}

BOOL CUsbDev51::Bind(IUsbDriver *pDriver)
{
	if( pDriver ) {

		AfxRelease(m_pDriver);

		m_pDriver = NULL;

		pDriver->QueryInterface(AfxAeonIID(IUsbFuncHardwareEvents), (void **) &m_pDriver);

		if( m_pDriver ) {
		
			m_pDriver->OnBind(this);

			return true;
			}
		}

	return false;
	}

BOOL CUsbDev51::Init(void)
{
	if( m_pDriver ) {

		SetPhySuspend(false);

		ConfigurePort();

		InitQueueHeads(true);

		InitTransfer();
	
		InitInterrupts();

		m_pDriver->OnInit();

		return true;
		}

	return false;
	}

BOOL CUsbDev51::Start(void)
{
	if( m_pDriver ) {

		EnableEvents();

		EnableInterrupts(true);

		SetPhySuspend(false);

		SetRun();

		m_pDriver->OnStart();
		
		return true;
		}

	return false;
	}

BOOL CUsbDev51::Stop(void)
{
	if( m_pDriver ) {
		
		m_pDriver->OnStop();
		
		EnableInterrupts(false);

		StopEndpoints();

		SetStop();
		
		SetPhySuspend(true);

		return true;
		}

	return false;
	}

// IUsbFuncHardwareDriver

BOOL CUsbDev51::GetHiSpeedCapable(void)
{
	return m_fHiSpeed;
	}

BOOL CUsbDev51::GetHiSpeedActual(void)
{
	return Reg(PortSc1) & portHighSpeedStatus ? true : false;
	}

BOOL CUsbDev51::SetAddress(UINT uAddr)
{
	m_bAddr = 0x80 | uAddr;
	
	return true;
	}

BOOL CUsbDev51::SetConfig(UINT uConfig)
{
	return true;
	}

UINT CUsbDev51::GetAddress(void)
{
	return m_bAddr & ~0x7F;
	}

BOOL CUsbDev51::InitBulk(UINT iEndpt, BOOL fIn, UINT uMax)
{
	if( iEndpt > 0 && iEndpt < 16 ) {

		Reg(EndptCtrl0 + iEndpt)  = 0x00480048;
		
		CQueueHead &Qh = m_pQueueHead[ GetQueueHead(iEndpt, fIn) ];

		Qh.m_dwMaxPackLen = uMax;

		Qh.m_dwZlt        = true;

		Reg(EndptCtrl0 + iEndpt) |= (1 << (fIn ? 23 : 7));

		return true;
		}

	return false;
	}

BOOL CUsbDev51::KillBulk(UINT iEndpt)
{
	if( iEndpt > 0 && iEndpt < 16 ) {

		if( (Reg(EndptCtrl0 + iEndpt) & 0x000C000C) == 0x00080008 ) {

			Reg(EndptCtrl0 + iEndpt) &= 0x007F007F;

			FreeAllTransfer(iEndpt);

			return true;
			}
		}

	return false;
	}

BOOL CUsbDev51::Transfer(UsbIor &Req)
{
	if( Req.m_uCount <= 16384 ) {

		UINT i = AllocTransfer(Req.m_iEndpt);

		if( i < NOTHING ) {

			CTransfer &t = m_pTransfer[i];

			t.m_pIo                 = (Req.m_uFlags & Req.flagNotify) ? &Req : NULL;

			t.m_fDirIn              = (Req.m_uFlags & Req.flagIn)  ? true : false;

			t.m_pTd->m_dwTotalBytes = Req.m_uCount;

			if( t.m_fDirIn ) {

				ArmMemCpy(t.m_pBuff, PBYTE(Req.m_pData), Req.m_uCount);
				}
			
			return QueueTransfer(t);
			}
		}

	return false;
	}

BOOL CUsbDev51::Abort(UsbIor &Req)
{
	UINT iEndpt = Req.m_iEndpt;

	bool fDirIn = Req.m_uFlags & Req.flagIn;

	FlushEndpoint(iEndpt, fDirIn);

	EnableInterrupts(false);

	FreeAllTransfer(iEndpt);

	CQueueHead &Qh = m_pQueueHead[ GetQueueHead(iEndpt, fDirIn) ];

	Qh.m_pTransferHead = NULL;

	Qh.m_pTransferTail = NULL;

	EnableInterrupts(true);
	
	return true;
	}

BOOL CUsbDev51::SetStall(UINT iEndpoint, BOOL fSet)
{
	if( fSet ) {

		Reg(EndptCtrl0 + iEndpoint) |=  0x00010001;
		}
	else { 
		Reg(EndptCtrl0 + iEndpoint) &= ~0x00010001;
		}

	return true;
	}

BOOL CUsbDev51::GetStall(UINT iEndpoint)
{
	return Reg(EndptCtrl0 + iEndpoint) &= 0x00010001;
	}

BOOL CUsbDev51::ResetDataToggle(UINT iEndpoint)
{
	Reg(EndptCtrl0 + iEndpoint) |= 0x00400040;

	return true;
	}

// IEventSink

void CUsbDev51::OnEvent(UINT uLine, UINT uParam)
{
	for(;;) {

		DWORD dwEvent = Reg(UsbSts) & Reg(UsbIntr);

		if( dwEvent & intReset ) {

			OnReset();
			}

		if( dwEvent & intSuspend ) {

			OnSuspend();
			}
		
		if( dwEvent & intTransfer ) {

			OnTransfer();
			}
		
		if( dwEvent & intPortChange ) {

			OnPortChange();
			}
		
		if( dwEvent & intSysError ) {	

			OnSysError();
			}

		Reg(UsbSts) = dwEvent;
		
		if( !dwEvent ) break;
		}
	}

// Events

void CUsbDev51::OnReset(void)
{
	Reg(EndptSetupStat) = Reg(EndptSetupStat);

	Reg(EndptComplete)  = Reg(EndptComplete);

	while( Reg(EndptPrime) );			

	Reg(EndptFlush) = 0xFFFFFFFF;

	if( (Reg(PortSc1) & portReset) ) {

		m_pDriver->OnReset();

		TermAllTransfer();

		FreeAllTransfer();

		InitQueueHeads(false);

		m_bAddr = 0;
		}
	else {
		AfxTrace("[Reset Required]\n");
		}
	}

void CUsbDev51::OnSuspend(void)
{
	m_pDriver->OnSuspend();
	}

void CUsbDev51::OnTransfer(void)
{
	if( Reg(EndptSetupStat) & 1 ) {

		Reg(EndptSetupStat) = 1;

		for(;;) {

			Reg(UsbCmd) |= cmdSetupTripWire;

			ArmMemCpy(m_bSetup, PVOID(m_pQueueHead[0].m_bSetup), sizeof(m_bSetup));

			if( Reg(UsbCmd) & cmdSetupTripWire ) {

				break;
				}
			}

		Reg(UsbCmd) &= ~cmdSetupTripWire;

		m_pDriver->OnSetup(m_bSetup);
		}

	while( Reg(EndptComplete) ) {

		Reg(EndptComplete) = Reg(EndptComplete);

		for( UINT i = 0; i < m_uTdSize; i ++ ) {

			CTransfer     &t  =  m_pTransfer[i];

			CTransferDesc &td = *m_pTransfer[i].m_pTd; 

			if( t.m_fUsed && !td.m_dwActive ) {

				if( td.m_NextTDPtr == TD_PTR_END ) {

					CQueueHead &Qh = m_pQueueHead[ GetQueueHead(t.m_iEndpt, t.m_fDirIn) ];

					Qh.m_pTransferHead = NULL;

					Qh.m_pTransferTail = NULL;
					}
								
				if( t.m_iEndpt == 0 && m_bAddr & 0x80 ) {

					m_bAddr &= ~0x80;

					Reg(DeviceAddr) = DWORD(m_bAddr) << 25;
					}

				if( t.m_pIo ) {

					if( !td.m_dwActive && !td.m_dwHalted && !td.m_dwTransErr && !td.m_dwDataBufErr ) {

						t.m_pIo->m_uStatus = t.m_pIo->statPassed;

						t.m_pIo->m_uCount -= td.m_dwTotalBytes;

						if( !t.m_fDirIn ) {

							ArmMemCpy(PBYTE(t.m_pIo->m_pData), t.m_pBuff, t.m_pIo->m_uCount);
							}
						}
					else {
						t.m_pIo->m_uStatus = t.m_pIo->statFailed;
						}

					m_pDriver->OnTransfer(*t.m_pIo);
					}

				t.m_fUsed = false; 

				m_pFlag->Signal(1);
				}
			}
		}
	}

void CUsbDev51::OnPortChange(void)
{
	if( !(Reg(PortSc1) & portSuspend) ) {

		m_pDriver->OnResume();
		}
	}

void CUsbDev51::OnSysError(void)
{
	AfxTrace("[System Error]\n");
	}

// Transfers

BOOL CUsbDev51::QueueTransfer(CTransfer &Transfer)
{
	EnableInterrupts(false);

	CQueueHead &Qh = m_pQueueHead[ GetQueueHead(Transfer.m_iEndpt, Transfer.m_fDirIn) ];

	if( !Qh.m_pTransferHead ) {

		EnableInterrupts(true);
	
		Qh.m_pTransferHead        = &Transfer;

		Qh.m_pTransferTail        = &Transfer;
		
		Qh.m_Transfer.m_NextTDPtr = Transfer.m_dwTdPhy;

		Qh.m_Transfer.m_dwActive  = false;

		Qh.m_Transfer.m_dwHalted  = false;

		if( !PrimeEndpoint(Transfer.m_iEndpt, Transfer.m_fDirIn) ) {

			DumpQueueHead(GetQueueHead(Transfer.m_iEndpt, Transfer.m_fDirIn));
			
			return false;
			}

		return true;
		}
	else {
		Qh.m_pTransferTail->m_pTd->m_NextTDPtr = Transfer.m_dwTdPhy;

		Qh.m_pTransferTail                     = &Transfer;

		if( IsEndpointPrimed(Transfer.m_iEndpt, Transfer.m_fDirIn) ) {
		
			EnableInterrupts(true);

			return true;
			}
		
		for(;;) {

			Reg(UsbCmd) |= cmdAddTripWire;

			BOOL fEndptStat = Reg(EndptStat) & Bit(Transfer.m_iEndpt + (Transfer.m_fDirIn ? 16 : 0));
						
			if( Reg(UsbCmd) & cmdAddTripWire ) {

				Reg(UsbCmd) &= ~cmdAddTripWire;		
				
				if( fEndptStat ) {

					EnableInterrupts(true);

					return true;
					}
				else {
					Qh.m_Transfer.m_NextTDPtr = Transfer.m_dwTdPhy;

					Qh.m_Transfer.m_dwActive  = false;

					Qh.m_Transfer.m_dwHalted  = false;

					if( PrimeEndpoint(Transfer.m_iEndpt, Transfer.m_fDirIn) ) {

						EnableInterrupts(true);

						return true;
						}

					break;
					}
				}
			}

		EnableInterrupts(true);

		return false;
		}
	}

BOOL CUsbDev51::PrimeEndpoint(UINT iEndpt, BOOL fIn)
{
	DWORD dwMask = Bit(iEndpt + (fIn ? 16 : 0)); 

	Reg(EndptPrime) |= dwMask;

	while( Reg(EndptPrime) & dwMask );

	return true;
	}

BOOL CUsbDev51::FlushEndpoint(UINT iEndpt, BOOL fIn)
{
	DWORD dwMask = Bit(iEndpt + (fIn ? 16 : 0)); 

	for(;;) {
	
		Reg(EndptFlush) = dwMask;

		while( Reg(EndptFlush) & dwMask );

		if( !(Reg(EndptStat) & dwMask) ) {

			return true;
			}
		}
	}

BOOL CUsbDev51::IsEndpointPrimed(UINT iEndpt, BOOL fIn)
{
	DWORD dwMask = Bit(iEndpt + (fIn ? 16 : 0)); 

	return Reg(EndptPrime) & dwMask;
	}

// Hardware

void CUsbDev51::InitInterrupts(void)
{
	Reg(EndptNakEn) = 0x00000000;

	Reg(EndptNak)   = 0xFFFFFFFF;
	
	Reg(UsbSts)     = 0x030005FF;

	Reg(UsbIntr)    = 0x00000000;

	m_dwIntMask     = intTransfer | intPortChange | intSysError | intReset | intSuspend;
	}

void CUsbDev51::EnableInterrupts(BOOL fSet)
{
	if( fSet ) {

		Reg(UsbIntr) = m_dwIntMask;
		}
	else {
		Reg(UsbIntr) = 0x00000000;
		}
	}

void CUsbDev51::ConfigurePort(void)
{
	Reg(OtgSC)   = 0x007F0028;

	Reg(PortSc1) = 0x10000004 | (m_fHiSpeed ? 0 : portHighSpeedDisable);
	}

void CUsbDev51::SetPhySuspend(bool fSet)
{
	if( fSet ) {
		
		m_pBase[regPhyCtrl0 ] &= ~Bit(12);
		}
	else {
		m_pBase[regPhyCtrl0 ] |= Bit(12);
		}
	}

void CUsbDev51::StopEndpoints(void)
{
	for( UINT i = 0; i < 16; i ++ ) {

		if( IsEndpointPrimed(i, false) ) {

			FlushEndpoint(i, false);
			}

		if( IsEndpointPrimed(i, true) ) {

			FlushEndpoint(i, true);
			}
		}
	}

// Queue Helpers

void CUsbDev51::InitQueueHeads(bool fFull)
{
	if( fFull ) {

		memset(m_pQueueHead, 0, sizeof(CQueueHead) * 32);

		for( UINT i = 0; i < 32; i ++ ) {
		
			m_pQueueHead[i].m_dwMaxPackLen	       = 64;
		
			m_pQueueHead[i].m_dwIos		       = true;

			m_pQueueHead[i].m_dwZlt		       = false;

			m_pQueueHead[i].m_Transfer.m_NextTDPtr = TD_PTR_END;
			}

		Reg(EndptListAddr) = phal->VirtualToPhysical(DWORD(m_pQueueHead));
		}
	else {
		for( UINT i = 0; i < 32; i ++ ) {

			m_pQueueHead[i].m_pTransferHead = NULL;

			m_pQueueHead[i].m_pTransferTail = NULL;
			}
		}
	}

void CUsbDev51::MakeQueueHeads(void)
{
	m_pQueueHead = (CQueueHead *) AllocNonCached(32 * sizeof(CQueueHead), 2048);
	}

void CUsbDev51::KillQueueHeads(void)
{
	if( m_pQueueHead ) {

		FreeNonCached(m_pQueueHead);

		m_pQueueHead = NULL;
		}
	}

UINT CUsbDev51::GetQueueHead(UINT i, BOOL fIn) const
{
	return i < 16 ? i * 2 + (fIn ? 1 : 0) : NOTHING;
	}

// Transfers Descriptors

void CUsbDev51::InitTransfer(void)
{
	memset(m_pTransfer, 0, sizeof(CTransfer)     * m_uTdSize);

	memset(m_pTdList,   0, sizeof(CTransferDesc) * m_uTdSize);

	for( UINT i = 0; i < m_uTdSize; i ++ ) {

		CTransfer      &t = m_pTransfer[i];

		CTransferDesc &td = m_pTdList[i];

		t.m_pTd           = &td;

		t.m_pBuff         = m_pTdBuff + 16384 * i;

		t.m_dwTdPhy       = phal->VirtualToPhysical(DWORD(t.m_pTd));
		
		td.m_BufPtr[0]    = phal->VirtualToPhysical(DWORD(t.m_pBuff) + 0x0000);

		td.m_BufPtr[1]    = phal->VirtualToPhysical(DWORD(t.m_pBuff) + 0x1000);

		td.m_BufPtr[2]    = phal->VirtualToPhysical(DWORD(t.m_pBuff) + 0x2000);

		td.m_BufPtr[3]    = phal->VirtualToPhysical(DWORD(t.m_pBuff) + 0x3000);

		td.m_NextTDPtr    = TD_PTR_END;
		}
	}

void CUsbDev51::MakeTransfer(UINT uCount)
{
	m_pTransfer = New CTransfer [ uCount ];

	m_pTdList   = (CTransferDesc *) AllocNonCached(uCount * sizeof(CTransferDesc), sizeof(CTransferDesc));

	m_pTdBuff   = (PBYTE          ) AllocNonCached(uCount * 16384, 4096);

	m_uTdSize   = uCount;
	}

void CUsbDev51::KillTransfer(void)
{
	if( m_pTransfer ) {

		free(m_pTransfer);

		FreeNonCached(m_pTdList);

		FreeNonCached(m_pTdBuff);

		m_pTdList    = NULL;

		m_pTdBuff    = NULL;

		m_pTransfer  = NULL;

		m_uTdSize    = 0;
		}
	}

UINT CUsbDev51::AllocTransfer(UINT iEndpt)
{
	m_pFlag->Wait(FOREVER);

	for( UINT i = 0; i < m_uTdSize; i ++ ) {

		CTransfer &t = m_pTransfer[i];

		m_pLock->Wait(FOREVER);

		if( !t.m_fUsed ) {

			t.m_pTd->m_dwTotalBytes = 0;

			t.m_pTd->m_NextTDPtr    = TD_PTR_END;

			t.m_pTd->m_dwIoc        = true;

			t.m_pTd->m_dwActive     = true;

			t.m_pTd->m_BufPtr[0]   &= 0xFFFFF000;

			t.m_pTd->m_BufPtr[1]   &= 0xFFFFF000;

			t.m_iEndpt              = iEndpt;

			t.m_fUsed               = true;

			m_pLock->Free();
						
			return i;
			}
		
		m_pLock->Free();
		}

	return NOTHING;
	}

void CUsbDev51::FreeTransfer(UINT iIndex)
{
	if( iIndex < m_uTdSize ) {

		if( m_pTransfer[iIndex].m_fUsed ) {

			m_pTransfer[iIndex].m_fUsed = false;

			m_pFlag->Signal(1);
			}
		}
	}

void CUsbDev51::FreeAllTransfer(void)
{
	for( UINT i = 0; i < m_uTdSize; i ++ ) {

		if( m_pTransfer[i].m_fUsed ) {

			m_pTransfer[i].m_fUsed = false;

			m_pFlag->Signal(1);
			}
		}
	}

void CUsbDev51::FreeAllTransfer(UINT iEndpt)
{
	for( UINT i = 0; i < m_uTdSize; i ++ ) {

		if( m_pTransfer[i].m_fUsed && m_pTransfer[i].m_iEndpt == iEndpt ) {

			m_pTransfer[i].m_fUsed = false;

			m_pFlag->Signal(1);
			}
		}
	}

void CUsbDev51::TermAllTransfer(void)
{
	for( UINT i = 0; i < m_uTdSize; i ++ ) {

		if( m_pTransfer[i].m_fUsed ) {

			if( m_pTransfer[i].m_pIo ) {

				m_pTransfer[i].m_pIo->m_uStatus = UsbIor::statFailed;

				m_pDriver->OnTransfer(*m_pTransfer[i].m_pIo);
				}
			}
		}
	}

// Debug 

void CUsbDev51::Dump(void) const
{
	#if defined(_DEBUG)

	AfxTrace("DeviceAddr         = 0x%8.8X\n", Reg(DeviceAddr));
	AfxTrace("UsbStatus          = 0x%8.8X\n", Reg(UsbSts));
	AfxTrace("UsbIntr            = 0x%8.8X\n", Reg(UsbIntr));
	AfxTrace("EndptListAddr      = 0x%8.8X\n", Reg(EndptListAddr));
	AfxTrace("EndptSetupStat     = 0x%8.8X\n", Reg(EndptSetupStat));
	AfxTrace("EndptPrime         = 0x%8.8X\n", Reg(EndptPrime));
	AfxTrace("EndptFlush         = 0x%8.8X\n", Reg(EndptFlush));
	AfxTrace("EndptStat          = 0x%8.8X\n", Reg(EndptStat));
	AfxTrace("EndptComplete      = 0x%8.8X\n", Reg(EndptComplete));
	AfxTrace("EndptCtrl0         = 0x%8.8X\n", Reg(EndptCtrl0));
	
	#endif
	}

void CUsbDev51::DumpQueueHead(void) const
{
	#if defined(_DEBUG)

	AfxTrace("Queue Heads\n\n");
	
	for( UINT i = 0; i < 32; i ++ ) {

		DumpQueueHead(i);
		}

	#endif
	}

void CUsbDev51::DumpQueueHead(UINT i) const
{
	#if defined(_DEBUG)

	if( i < 32 ) {

		CQueueHead const &qh = m_pQueueHead[i];

		AfxTrace("Queue Head : %d {Virtual Address 0x%8.8X, Physical Address 0x%8.8X}\n", 
			  i,
			  &m_pQueueHead[i],
			  phal->VirtualToPhysical(DWORD(&m_pQueueHead[i]))
			  );

		AfxTrace("Mult                 = %d\n",      qh.m_dwMult);
		AfxTrace("Zlt                  = %d\n",      qh.m_dwZlt);
		AfxTrace("MaxPackLen           = %d\n",      qh.m_dwMaxPackLen);
		AfxTrace("Ios                  = %d\n",      qh.m_dwIos);
		AfxTrace("CurrentTDPtr         = 0x%8.8X\n", qh.m_CurrentTDPtr);
		AfxTrace("Transfer.NextTDPtr   = 0x%8.8X\n", qh.m_Transfer.m_NextTDPtr);
		AfxTrace("Transfer.Active      = %d\n",      qh.m_Transfer.m_dwActive);
		AfxTrace("Transfer.Halted      = %d\n",      qh.m_Transfer.m_dwHalted);
		AfxTrace("Transfer.DataBufErr  = %d\n",      qh.m_Transfer.m_dwDataBufErr);
		AfxTrace("Transfer.TransErr    = %d\n",      qh.m_Transfer.m_dwTransErr);
		AfxTrace("Transfer.MultO       = %d\n",      qh.m_Transfer.m_dwMultO);
		AfxTrace("Transfer.Ioc         = %d\n",      qh.m_Transfer.m_dwIoc);
		AfxTrace("Transfer.TotalBytes  = %d\n",      qh.m_Transfer.m_dwTotalBytes);
		AfxTrace("Transfer.BufPtr[0]   = %8.8X\n",   qh.m_Transfer.m_BufPtr[0]);
		AfxTrace("Transfer.BufPtr[1]   = %8.8X\n",   qh.m_Transfer.m_BufPtr[1]);
		AfxTrace("Transfer.BufPtr[2]   = %8.8X\n",   qh.m_Transfer.m_BufPtr[2]);
		AfxTrace("Transfer.BufPtr[3]   = %8.8X\n",   qh.m_Transfer.m_BufPtr[3]);
		AfxTrace("Transfer.BufPtr[4]   = %8.8X\n",   qh.m_Transfer.m_BufPtr[4]);
		AfxTrace("Setup Buffer[0]      = 0x%2.2X\n", qh.m_bSetup[0]);
		AfxTrace("Setup Buffer[1]      = 0x%2.2X\n", qh.m_bSetup[1]);
		AfxTrace("Setup Buffer[2]      = 0x%2.2X\n", qh.m_bSetup[2]);
		AfxTrace("Setup Buffer[3]      = 0x%2.2X\n", qh.m_bSetup[3]);
		AfxTrace("Setup Buffer[4]      = 0x%2.2X\n", qh.m_bSetup[4]);
		AfxTrace("Setup Buffer[5]      = 0x%2.2X\n", qh.m_bSetup[5]);
		AfxTrace("Setup Buffer[6]      = 0x%2.2X\n", qh.m_bSetup[6]);
		AfxTrace("Setup Buffer[7]      = 0x%2.2X\n", qh.m_bSetup[7]);
		AfxTrace("Transfer Head        = 0x%8.8X\n", qh.m_pTransferHead);
		AfxTrace("Transfer Tail        = 0x%8.8X\n", qh.m_pTransferTail);

		AfxTrace("\n");
		}
	#endif
	}

void CUsbDev51::DumpTransfer(void) const
{
	#if defined(_DEBUG)

	AfxTrace("Transfer List\n\n");

	for( UINT i = 0; i < m_uTdSize; i ++ ) {

		DumpTransfer(i);
		}

	#endif
	}

void CUsbDev51::DumpTransfer(UINT i) const
{
	#if defined(_DEBUG)

	if( i < m_uTdSize ) {

		CTransfer     const &t  = m_pTransfer[i];
		
		CTransferDesc const &td = m_pTdList[i];
		
		AfxTrace("Transfer Descriptor : %d {Virtual Address 0x%8.8X, Physical Address 0x%8.8X}\n", 
			  i, 
			  &m_pTdList[i],
			  phal->VirtualToPhysical(DWORD(&m_pTdList[i]))
			  );

		AfxTrace("m_NextTDPtr       = %8.8X\n", td.m_NextTDPtr);
		AfxTrace("m_dwTransErr      = %d\n",    td.m_dwTransErr);
		AfxTrace("m_dwDataBufErr    = %d\n",    td.m_dwDataBufErr);
		AfxTrace("m_dwHalted        = %d\n",    td.m_dwHalted);
		AfxTrace("m_dwActive        = %d\n",    td.m_dwActive);
		AfxTrace("m_dwReserved1     = %d\n",    td.m_dwReserved1);
		AfxTrace("m_dwMultO         = %d\n",    td.m_dwMultO);
		AfxTrace("m_dwReserved2     = %d\n",    td.m_dwReserved2);
		AfxTrace("m_dwIoc           = %d\n",    td.m_dwIoc);
		AfxTrace("m_dwTotalBytes    = %d\n",    td.m_dwTotalBytes);
		AfxTrace("m_dwReserved3     = %d\n",    td.m_dwReserved3);
		AfxTrace("m_BufPtr[0]       = %8.8X\n", td.m_BufPtr[0]);
		AfxTrace("m_BufPtr[1]       = %8.8X\n", td.m_BufPtr[1]);
		AfxTrace("m_BufPtr[2]       = %8.8X\n", td.m_BufPtr[2]);
		AfxTrace("m_BufPtr[3]       = %8.8X\n", td.m_BufPtr[3]);
		AfxTrace("m_BufPtr[4]       = %8.8X\n", td.m_BufPtr[4]);
		AfxTrace("m_pBuff           = %8.8X\n", t.m_pBuff);
		AfxTrace("m_pTd             = %8.8X\n", t.m_pTd);
		AfxTrace("m_dwTdPhy         = %8.8X\n", t.m_dwTdPhy);
		AfxTrace("m_dwUsed          = %d\n",    t.m_fUsed);
		AfxTrace("m_dwEndpt         = %d\n",    t.m_iEndpt);
		AfxTrace("Data[0...7]       = ");
		
		for( UINT n = 0; n < 8; n ++ ) {

			AfxTrace("%2.2X ", t.m_pBuff[n]);
			}

		AfxTrace("\n");
		}

	#endif
	}

// End of File
