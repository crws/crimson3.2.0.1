
#include "Intern.hpp"

#include "Beeper51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Pwm51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Beeper
//

// Instantiator

IDevice * Create_Beeper51(CPwm51 *pPwm)
{
	CBeeper51 *p = New CBeeper51(pPwm);

	p->Open();

	return p;
	}

// Static Data

UINT CBeeper51::m_Table[] = {

	33488,
	35479,
	37589,
	39824,
	42192,
	44701,
	47359,
	50175,
	53159,
	56320,
	59669,
	63217
	};

// Constructor

CBeeper51::CBeeper51(CPwm51 *pPwm)
{
	StdSetRef();

	m_pPwm   = pPwm;

	m_uTime  = 0;

	m_pTimer = CreateTimer();
	}

// Destructor

CBeeper51::~CBeeper51(void)
{
	m_pTimer->Release();
	}

// IUnknown

HRESULT CBeeper51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IBeeper);

	return E_NOINTERFACE;
	}

ULONG CBeeper51::AddRef(void)
{
	StdAddRef();
	}

ULONG CBeeper51::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CBeeper51::Open(void)
{
	m_pTimer->SetHook((IEventSink *) this, 0);

	m_pTimer->SetPeriod(5);

	m_pTimer->Enable(true);

	return TRUE;
	}

// IBeeper

void METHOD CBeeper51::Beep(UINT uBeep)
{
	switch( uBeep ) {

		case beepStart:

			Beep(72, 200);

			return;

		case beepPress:

			Beep(72, 50);

			return;
		
		case beepFull:

			Beep(48, 500);

			return;
		}

	BeepOff();
	}

void METHOD CBeeper51::Beep(UINT uNote, UINT uTime)
{
	UINT ipr;

	HostRaiseIpr(ipr);

	if( (m_uTime = ToTicks(uTime)) ) {
	
		UINT uFreq = m_Table[uNote % 12] >> (12-(uNote / 12));

		m_pPwm->SetFreq(uFreq);
		}
	else {
		BeepOff();
		}

	HostLowerIpr(ipr);
	}

void METHOD CBeeper51::BeepOff(void)
{
	m_pPwm->SetDuty(0);
	}

// IEventSink

void CBeeper51::OnEvent(UINT uLine, UINT uParam)
{
	if( m_uTime && !--m_uTime ) {

		BeepOff();
		}
	}

// End of File
