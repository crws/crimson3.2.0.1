
#include "Intern.hpp"

#include "Pwm51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ccm51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Pulse Width Modulator
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CPwm51::CPwm51(UINT iIndex, CCcm51 *pCcm)
{
	m_pBase = PVDWORD(iIndex == 0 ? ADDR_PWM1 : ADDR_PWM2);

	m_uFreq = pCcm->GetFreq(CCcm51::clkIpg);

	m_uPre  = 8;

	Init();
	}

// IPwm

void CPwm51::SetFreq(UINT uFreq)
{
	Reg(Control) &= ~Bit(0);

	m_uPeriod     = (m_uFreq / (m_uPre * uFreq)) - 2;

	Reg(Period)   = m_uPeriod;

	Reg(Sample)   = m_uPeriod / 2;

	Reg(Control) |= Bit(0);
	}

void CPwm51::SetDuty(UINT uDuty)
{
	Reg(Sample) = (m_uPeriod * uDuty) / 100;
	}

// Implementation

void CPwm51::Init(void)
{
	Reg(Control) = Bit(3);

	Reg(Control) = 0x00010000 | ((m_uPre - 1) << 4);

	Reg(Int)     = 0x00000000;
	}

// End of File
