
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PlatformGraphite_HPP

#define INCLUDE_PlatformGraphite_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpu51;
class CPwm51;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PlatformMX51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Graphite HMI
//

class CPlatformGraphite : public CPlatformMX51,
	                  public IPortSwitch,
			  public ILeds,
			  public IInputSwitch,
			  public IUsbSystem,
			  public IUsbSystemPortMapper,
			  public IUsbSystemPower
{
	public:
		// Constructor
		CPlatformGraphite(UINT uModel);

		// Destructor
		~CPlatformGraphite(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ILeds
		void METHOD SetLed(UINT uLed, UINT uState);
		void METHOD SetLedLevel(UINT uPercent, bool fPersist);
		UINT METHOD GetLedLevel(void);

		// IInputSwitch
		UINT METHOD GetSwitches(void);
		UINT METHOD GetSwitch(UINT uSwitch);

	protected:
		// Data Members
		CIpu51        * m_pIpu;
		CPwm51        * m_pPwm[2];
		UINT            m_uLedLevel;
		UINT            m_uRackLoad;
		UINT            m_uRackLimit;
		IUsbHubDriver * m_pHub;

		// IPortSwitch
		UINT METHOD GetCount(UINT uUnit);
		UINT METHOD GetMask(UINT uUnit);
		UINT METHOD GetType(UINT uUnit, UINT uLog);
		BOOL METHOD EnablePort(UINT uUnit, BOOL fEnable);
		BOOL METHOD SetPhysical(UINT uUnit, BOOL fRS485);
		BOOL METHOD SetFull(UINT uUnit, BOOL fFull);
		BOOL METHOD SetMode(UINT uUnit, BOOL fAuto);

		// IUsbSystem
		void METHOD OnDeviceArrival(IUsbHostFuncDriver *pDriver);
		void METHOD OnDeviceRemoval(IUsbHostFuncDriver *pDriver);
		
		// IUsbSystemPortMapper
		UINT METHOD GetPortCount(void);
		UINT METHOD GetPortType(UsbTreePath const &Path);
		UINT METHOD GetPortReset(UsbTreePath const &Path);
		
		// IUsbSystemPower
		void METHOD OnNewDevice(UsbTreePath const &Path, UINT uPower);
		void METHOD OnDelDevice(UsbTreePath const &Path, UINT uPower);

		// Initialisation
		void InitPriorities(void);
		void InitClocks(void);
		void InitRails(void);
		void InitMux(void);
		void InitGpio(void);
		void InitUarts(void);
		void InitUsb(void);
		void InitRack(void);
		void ProgramPic(void);
		BOOL ReadRfid(void);

		// Persistence
		void LoadLedLevel(void);
		void SaveLedLevel(void);
	};

// End of File

#endif
