
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IpuDmfc51_HPP
	
#define	INCLUDE_IpuDmfc51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpu51;

//////////////////////////////////////////////////////////////////////////
//
// Fifo Config 
//

struct CDmfcConfig
{
	UINT  m_uFrameH;
	UINT  m_uFrameW;
	UINT  m_uPixels;
	UINT  m_uBurstSize;
	UINT  m_uFifoSize;
	UINT  m_uStartAddr;
	UINT  m_uWmClrLev;
	UINT  m_uWmSetLev;
	bool  m_fWmEnable;
	};

//////////////////////////////////////////////////////////////////////////
//
// Fifo Size 
//

enum
{
	size512x128,
	size256x128,
	size64x128,
	size32x128,
	size16x128,
	size8x128,
	sizes4x128
	};

//////////////////////////////////////////////////////////////////////////
//
// Fifo Burst Sizes
//

enum
{
	burst32,
	burst16,
	burst8,
	burst4,
	};

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Image Processing Unit Multi Fifo Control Module
//

class CIpuDisplayFifo51
{	
	public:
		// Constructor
		CIpuDisplayFifo51(CIpu51 *pIpu);

		// Interface
		void Enable(bool fEnable);
		bool SetConfig(UINT uChan, UINT uWidth, UINT uHeight);
		bool SetConfig(UINT uChan, CDmfcConfig const &Config);
		bool SetConfigAlt(UINT uChan, CDmfcConfig const &Config);
		bool GetFifoFull(UINT uChan);
		bool GetFifoEmpty(UINT uChan);

	protected:
		// Regisgters
		enum
		{
			regRdChan	= 0x0000 / sizeof(DWORD),
			regWrChan	= 0x0004 / sizeof(DWORD),
			regWrChanDef	= 0x0008 / sizeof(DWORD),
			regDpChan	= 0x000C / sizeof(DWORD),
			regDpChanDef	= 0x0010 / sizeof(DWORD),
			regGen1		= 0x0014 / sizeof(DWORD),
			regGen2		= 0x0018 / sizeof(DWORD),
			regIntCtrl	= 0x001C / sizeof(DWORD),
			regWrChanAlt	= 0x0020 / sizeof(DWORD),
			regWrChanDefAlt	= 0x0024 / sizeof(DWORD),
			regDpChanAlt	= 0x0028 / sizeof(DWORD),
			regDpChanDefAlt	= 0x002C / sizeof(DWORD),
			regGen1Alt	= 0x0300 / sizeof(DWORD),
			regStat		= 0x0304 / sizeof(DWORD),
			};

		// Data
		PVDWORD   m_pBase;
		CIpu51  * m_pIpu;
		UINT      m_uUnit;
		
		// Implementation
		bool SetRead (CDmfcConfig const &Config, DWORD volatile &R1, DWORD volatile &R2);
		bool SetWrite(CDmfcConfig const &Config, DWORD volatile &R1, DWORD volatile &R2, UINT uShift);
		UINT FindScale(UINT uChan) const;
		UINT FindSize(UINT uWidth, UINT uScale) const;
	};

// End of File

#endif
