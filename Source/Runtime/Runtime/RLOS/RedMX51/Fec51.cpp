
#include "Intern.hpp"

#include "Fec51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Gpio51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Fast Ethernet Controller
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Instantiator

IDevice * Create_Nic51(void)
{
	IDevice *pDevice = New CFec51();

	pDevice->Open();

	return pDevice;
	}

// Constructor

CFec51::CFec51(void)
{
	StdSetRef();

	m_pBase         = PVDWORD(ADDR_FEC);

	m_uLine	        = INT_FEC;

	m_uPhyResetLine = 16;

	m_uPhyIntLine   = 15;

	m_uNicState     = nicInitial;

	m_uPhyState     = phyIdle;

	m_uMiiState     = miiIdle;

	m_uMulti	= 0;

	m_pMulti	= NULL;

	m_pMiiFlag      = Create_AutoEvent();
	
	m_pLinkOkay     = Create_ManualEvent();
	
	m_pLinkDown     = Create_ManualEvent();
	
	m_pTxFlag       = Create_Semaphore();
	
	m_pRxFlag       = Create_Semaphore();

	AfxGetObject("gpio", 3, IGpio, m_pGpioPhyReset);

	AfxGetObject("gpio", 1, IGpio, m_pGpioPhyInt);

	m_pGpioPhyReset->SetState(m_uPhyResetLine, true);

	AllocBuffers();

	DiagRegister();
	}

// Destructor

CFec51::~CFec51(void)
{
	DiagRevoke();

	m_pGpioPhyReset->Release();

	m_pGpioPhyInt->Release();

	m_pMiiFlag->Release();

	m_pLinkOkay->Release();

	m_pLinkDown->Release();

	m_pTxFlag->Release();

	m_pRxFlag->Release();
	}

// IUnknown

HRESULT CFec51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(INic);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CFec51::AddRef(void)
{
	StdAddRef();
	}

ULONG CFec51::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CFec51::Open(void)
{
	ResetPhy(true);

	ResetController();

	ResetCounters();
	
	return TRUE;
	}

// INic

bool CFec51::Open(bool fFast, bool fFull)
{
	CAutoGuard Guard;

	if( m_uNicState == nicClosed ) {

		m_fAllowFast = fFast;

		m_fAllowFull = fFull;

		m_pLinkOkay->Clear();

		m_pLinkDown->Set();

		ResetState();

		ResetCounters();

		ConfigController();

		EnableEvents();

		ResetPhy(false);

		ConfigPhy();

		return true;
		}

	return false;
	}

bool CFec51::Close(void)
{
	CAutoGuard Guard;

	if( m_uNicState >= nicOpen ) {

		DisableEvents();
	
		OnLinkDown();

		m_uNicState = nicClosed;

		ResetController();

		ResetPhy(true);

		ResetCounters();

		return true;
		}

	return false;
	}

bool CFec51::InitMac(MACADDR const &Addr)
{
	if( m_uNicState == nicInitial ) {

		m_MacAddr   = Addr;

		m_uNicState = nicClosed;

		return true;
		}

	return false;
	}

void CFec51::ReadMac(MACADDR &Addr)
{
	Addr = m_MacAddr;
	}

UINT CFec51::GetCapabilities(void)
{
	return nicFilterIp | nicCheckSums | nicAddSums;
	}

void CFec51::SetFlags(UINT uFlags)
{
	m_uFlags = uFlags;
	}

bool CFec51::SetMulticast(MACADDR const *pList, UINT uList)
{
	if( pList && uList ) {

		delete [] m_pMulti;

		m_pMulti = New MACADDR[ uList ];

		m_uMulti = uList;

		ArmMemCpy(m_pMulti, pList, sizeof(MACADDR) * m_uMulti);
		}
	else {
		delete [] m_pMulti;

		m_pMulti = NULL;

		m_uMulti = 0;
		}

	LoadMultiFilter();

	return true;
	}

bool CFec51::IsLinkActive(void)
{
	return m_uNicState == nicActive;
	}

bool CFec51::WaitLink(UINT uTime)
{
	return m_pLinkOkay->Wait(uTime);
	}

bool CFec51::SendData(CBuffer *pBuff, UINT uTime)
{
	if( m_uNicState == nicActive ) {

		UINT uWait = WaitMultiple(m_pTxFlag, m_pLinkDown, uTime);

		if( uWait == 1 ) {

			WORD  wSize = pBuff->GetSize();

			PBYTE pData = pBuff->GetData();

			Hal_Critical(true);	
				
			CBuffDesc *pDesc = m_pTxDesc + m_uTxTail;

			Reg(EIMR) &= ~intTxF;

			m_uTxTail = (m_uTxTail + 1) % constTxLimit;

			PBYTE pDest = PBYTE(phal->PhysicalToVirtual(DWORD(pDesc->m_pData), false));

			AddMac(pData);

			ArmMemCpy(pDest, pData, wSize);

			pDesc->m_wCount = wSize;

			StartTx(pDesc);

			Reg(EIMR) |= intTxF;

			Hal_Critical(false);

			return true;
			}
		}

	return false;
	}

bool CFec51::ReadData(CBuffer * &pBuff, UINT uTime)
{
	if( m_uNicState == nicActive ) {

		for(;;) {

			UINT uWait = WaitMultiple(m_pRxFlag, m_pLinkDown, uTime);

			if( uWait == 1 ) {

				Hal_Critical(true);

				CBuffDesc *pDesc = m_pRxDesc + m_uRxHead;

				m_uRxHead = (m_uRxHead + 1) % constRxLimit;

				Hal_Critical(false);
				
				if( !(pDesc->m_wCtrl & rxError) ) {

					PBYTE pData  = PBYTE(phal->PhysicalToVirtual(DWORD(pDesc->m_pData), false));

					if( TakeFrame(pData) ) {

						WORD  wCount = pDesc->m_wCount - ((pDesc->m_wCtrl & rxEnd) ? 4 : 0);

						BOOL  fIp    = MotorToHost((WORD &) pData[12]) == 0x0800;

						if( !(m_uFlags & nicOnlyIp) || fIp ) {

							CBuffer *pWork = BuffAllocate(wCount);

							if( pWork ) {

								PBYTE pDest = pWork->AddTail(wCount);

								ArmMemCpy(pDest, pData, wCount);

								if( fIp ) {

									pWork->SetFlag(packetTypeIp);
									}

								pWork->SetFlag(packetTypeValid);

								pWork->SetFlag(packetSumsValid);
				
								StartRx(pDesc);

								pBuff = pWork;

								return true;
								}
							}
						}
					}

				StartRx(pDesc);

				continue;
				}
			
			break;
			}
		}

	pBuff = NULL;

	return false;
	}

void METHOD CFec51::GetCounters(NICDIAG &Diag)
{
	UINT ipr;

	HostRaiseIpr(ipr);
	
	Diag = m_Diag;

	HostLowerIpr(ipr);
	}

void METHOD CFec51::ResetCounters(void)
{
	UINT ipr;

	HostRaiseIpr(ipr);

	memset(&m_Diag, 0, sizeof(m_Diag));

	HostLowerIpr(ipr);
	}

// IEventSink

void CFec51::OnEvent(UINT uLine, UINT uParam)
{
	if( uLine == m_uLine ) {

		for(;;) {
		
			DWORD Data = Reg(EIR) & Reg(EIMR);

			if( Data ) {
			
				if( Data & intMii ) {

					Reg(EIR) = intMii;

					OnMiiEvent();
					}

				if( Data & intRxF ) {

					Reg(EIR) = intRxF;

					OnRecvEvent();
					}

				if( Data & intTxF ) {

					Reg(EIR) = intTxF;

					OnSendEvent();
					}

				if( Data & (intUn | intRL | intLC) ) {

					Reg(EIR) = (intUn | intRL | intLC);

					m_Diag.m_TxDisc ++;
					}

				if( Data & intHbErr ) {

					Reg(EIR) = intHbErr;

					AfxTrace("NIC Heartbeat Error\n");
					}

				if( Data & (intBabT | intBabR) ) {

					Reg(EIR) = (intBabT | intBabR);

					AfxTrace("NIC Babbing Detected.\n");
					}

				if( Data & intEBErr ) {
				
					Reg(EIR) = intEBErr;
				
					AfxTrace("NIC Ethernet Bus Error\n");
					}
				
				continue;
				}

			break;
			}
		}
	else {
		m_uPhyState = phyGetIsr;

		IntGetMii(0x1D);
		}
	}

// IDiagProvider

UINT CFec51::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);
		}

	return 0;
	}

// Implementation

void CFec51::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);

	if( m_pGpioPhyInt ) {

		m_pGpioPhyInt->SetDirection(m_uPhyIntLine, false);

		m_pGpioPhyInt->SetIntHandler(m_uPhyIntLine, intLevelLo, this, 0);
		}
	}

void CFec51::DisableEvents(void)
{
	phal->EnableLine(m_uLine, false);

	PhyInterrupt(false);
	}

// Mac Management

void CFec51::ResetState(void)
{
	if( Hal_GetIrql() == IRQL_TASK ) {

		while( m_pRxFlag->Wait(5) );

		while( m_pTxFlag->Wait(5) );
		}

	m_uFlags = 0;

	ResetBuffers();

	m_pTxFlag->Signal(constTxLimit-1);
	}

void CFec51::ResetController(void)
{
	Reg(ECR) = Bit(0);

	while( Reg(ECR) & Bit(0) ) {

		Sleep(5);
		}
	}

void CFec51::ConfigController(void)
{
	Reg(EIMR)   = 0x00000000;

	Reg(EIR)    = 0xFFFFFFFF;

	Reg(MSCR)   = 0x1D << 1;

	Reg(RCR)    = (1518 << 16) | Bit(2) | Bit(1);

	Reg(TCR)    = 0x00;

	Reg(TFWR)   = 3;
	
	Reg(ERDSR)  = phal->VirtualToPhysical(DWORD(m_pRxDesc));

	Reg(ETDSR)  = phal->VirtualToPhysical(DWORD(m_pTxDesc));

	Reg(EMRBR)  = 0x7F << 4;

	Reg(PALR)   = (HostToMotor(PDWORD(&m_MacAddr)[0]) & 0xFFFFFFFF);
	
	Reg(PAUR)   = (HostToMotor(PDWORD(&m_MacAddr)[1]) & 0xFFFF0000);

	Reg(IAUR)   = 0;
	
	Reg(IALR)   = 0;

	Reg(GAUR)   = 0;
	
	Reg(GALR)   = 0;

	m_uNicState = nicOpen;

	Reg(EIMR)   = 0xEAF80000;
	}

void CFec51::LoadMultiFilter(void)
{
	DWORD Upper = 0;

	DWORD Lower = 0;

	for( UINT n = 0; n < m_uMulti; n++ ) {

		UINT uHash = GetMacHash(m_pMulti[n]);

		if( uHash < 32 ) {

			Lower |= (1 << uHash);
			}
		else {
			uHash -= 32;

			Upper |= (1 << uHash);
			}
		}

	Reg(GAUR) = Upper;

	Reg(GALR) = Lower;
	}

void CFec51::StartController(void)
{
	Reg(RCR)  = m_fUsingFull ? (1518 << 16) | Bit(2) : (1518 << 16) | Bit(2) | Bit(1);

	Reg(TCR)  = m_fUsingFull ? Bit(2)	         : 0x00;

	Reg(ECR)  = Bit(1);

	Reg(RDAR) = 1; 
	}

void CFec51::StopController(void)
{
	Reg(ECR) = 0x00;
	}

void CFec51::OnSendEvent(void)
{
	UINT uCount = 0;
  
	for(;;) {

		CBuffDesc *pDesc = m_pTxDesc + m_uTxHead;

		if( !(pDesc->m_wCtrl & txRdy) && (pDesc->m_wCtrl & txUsr1) ) {

			pDesc->m_wCtrl &= ~txUsr1;
			
			m_uTxHead = (m_uTxHead + 1) % constTxLimit;

			uCount    = uCount + 1;

			m_Diag.m_TxCount ++;

			if( m_uTxHead == m_uTxTail ) {

				break;
				}

			continue;
			}

		break;
		}

	m_pTxFlag->Signal(uCount);
	}

void CFec51::OnRecvEvent(void)
{
	UINT uCount = 0;

	for(;;) {
		
		CBuffDesc *pDesc = m_pRxDesc + m_uRxTail;

		if( !(pDesc->m_wCtrl & rxEmpty) ) {

			if( pDesc->m_wCtrl & rxUsrTouch ) {

				pDesc->m_wCtrl &= ~rxUsrTouch;

				m_uRxTail = (m_uRxTail + 1) % constRxLimit;

				uCount    = uCount + 1;

				m_Diag.m_RxCount++;

				if( m_uRxTail != m_uRxHead ) {

					continue;
					}
				}
			}

		break;
		}
	
	m_pRxFlag->Signal(uCount);
	}

void CFec51::AddMac(PBYTE pData)
{
	pData[ 6] = m_MacAddr.m_Addr[0];
	pData[ 7] = m_MacAddr.m_Addr[1];
	pData[ 8] = m_MacAddr.m_Addr[2];
	pData[ 9] = m_MacAddr.m_Addr[3];
	pData[10] = m_MacAddr.m_Addr[4];
	pData[11] = m_MacAddr.m_Addr[5];
	}

UINT CFec51::GetMacHash(MACADDR const &Mac)
{
	// Polynominal always contains X^32, and the bits in
	// the Poly mask indicate which other terms are used.
	// 
	// X^32 +			Implied
	// X^26 +			Encoded as 4
	// X^23 + X^22 +		Encoded as C
	// X^16 +			Encoded as 1
	// X^12 +			Encoded as 1
	// X^11 + X^10 + X^8 +		Encoded as D
	// X^7  + X^5  + X^4 +		Encoded as B
	// X^2  + X^1  + X^0		Encoded as 7
	//
	// See page 26-43 in MCF54455 Reference Manual.

	DWORD Poly = 0x04C11DB7;

	DWORD CRC  = 0xFFFFFFFF;

	for( UINT i = 0; i < 6; i++ ) {          
							
		BYTE add = Mac.m_Addr[i];
							
		BYTE bit;               
							
		for( UINT j = 0; j < 8; j++ ) {
							
			bit = CRC >> 31;
							
			CRC = CRC << 1;
							
			if( bit ^ (add & 1) ) {
							
				CRC ^= Poly;
				}

			add = add >> 1;
			}                        
		}

	return	((CRC & 0x01) << 5) |
		((CRC & 0x02) << 3) |
		((CRC & 0x04) << 1) |
		((CRC & 0x08) >> 1) |
		((CRC & 0x10) >> 3) |
		((CRC & 0x20) >> 5) ;
	}

bool CFec51::TakeFrame(PBYTE pData)
{
	if( m_pMulti ) {

		// NOTE -- If we have multicast enabled, if we have to filter for
		// addresses that get by the hash filter in the chip but which are
		// not included in our multicast list. Note that we explicitly
		// test for broadcast and allow it, as it won't be in the list but
		// it does meet the definition of the multicast address.

		MACADDR const *pAddr = (MACADDR const *) pData;

		if( pAddr->m_Addr[0] & 0x01 ) {

			static MACADDR Cast = { { 0xFF, 0xFF,
						  0xFF, 0xFF,
						  0xFF, 0xFF
						  } };

			if( memcmp(pAddr, &Cast, sizeof(MACADDR)) ) {

				UINT n;

				for( n = 0; n < m_uMulti; n++ ) {

					if( !memcmp(pAddr, m_pMulti + n, sizeof(MACADDR)) ) {

						break;
						}
					}

				if( n == m_uMulti ) {

					return false;
					}
				}
			}
		}

	return true;
	}

// Phy Management

void CFec51::PhyInterrupt(bool fEnable)
{
	if( m_pGpioPhyInt ) {
		
		m_pGpioPhyInt->SetIntEnable(m_uPhyIntLine, fEnable);
		}
	}

void CFec51::ResetPhy(bool fAssert)
{
	m_pGpioPhyReset->SetState(m_uPhyResetLine, fAssert ? false : true);
		
	Sleep(20);
	}

void CFec51::ConfigPhy(void)
{
	WORD caps = 0x0021;

	if( m_fAllowFast ) {
		
		caps |= 0x0080;
		}

	if( m_fAllowFull ) {
		
		caps |= ((caps & 0x00A0) << 1);
		}

	AppGetMii(0x1D);
		
	AppPutMii(0x1E, Bit(6) | Bit(4));

	AppPutMii(0x04, caps);

	AppPutMii(0x00, Bit(12));
	}

void CFec51::OnMiiEvent(void)
{
	PhyInterrupt(true);

	switch( m_uMiiState ) {

		case miiAppPut:
		case miiAppGet:

			m_uMiiState = miiIdle;

			m_pMiiFlag->Set();

			break;

		case miiIntPut:

			m_uMiiState = miiIdle;

			break;

		case miiIntGet:

			m_uMiiState = miiIdle;

			OnPhyEvent();

			break;
		}
	}

void CFec51::OnPhyEvent(void)
{
	if( m_uPhyState == phyGetIsr ) {

		DWORD Data = Reg(MMFR);

		if( Data & Bit(6) ) {

			m_uPhyState = phyGetLink;

			IntGetMii(0x1F);

			return;
			}

		if( Data & Bit(4) ) {

			m_uPhyState = phyIdle;
		
			OnLinkDown();

			IntGetMii(0x01);

			return;
			}

		return;
		}

	if( m_uPhyState == phyGetLink ) {

		DWORD Data = Reg(MMFR);

		m_fUsingFast = (Data & Bit(3)) ? true : false;

		m_fUsingFull = (Data & Bit(4)) ? true : false;

		m_uPhyState  = phyIdle;

		OnLinkUp();

		IntGetMii(0x01);

		return;
		}
	}

void CFec51::OnLinkUp(void)
{
	if( m_uNicState == nicOpen ) {
		
		ResetState();

		StartController();

		m_pLinkDown->Clear();

		m_pLinkOkay->Set();

		m_uNicState = nicActive;
		}
	}

void CFec51::OnLinkDown(void)
{
	if( m_uNicState == nicActive ) {

		m_pLinkOkay->Clear();

		m_pLinkDown->Set();

		StopController();

		m_uNicState = nicOpen;
		}
	}

// Buffer Management

void CFec51::AllocBuffers(void)
{
	m_pTxDesc = (CBuffDesc *) phal->GetNonCachedAlias(DWORD(memalign(16, sizeof(CBuffDesc) * constTxLimit)));

	m_pRxDesc = (CBuffDesc *) phal->GetNonCachedAlias(DWORD(memalign(16, sizeof(CBuffDesc) * constRxLimit)));

	m_pTxBuff = (PBYTE)       phal->GetNonCachedAlias(DWORD(memalign(16, constTxLimit * constBuffSize)));

	m_pRxBuff = (PBYTE)       phal->GetNonCachedAlias(DWORD(memalign(16, constRxLimit * constBuffSize)));

	for( UINT t = 0; t < constTxLimit; t++ ) {

		CBuffDesc &Buff = m_pTxDesc[t];

		Buff.m_wCtrl  = 0;
		
		Buff.m_wCount = 0;

		Buff.m_pData  = phal->VirtualToPhysical(DWORD(m_pTxBuff) + t * constBuffSize);
		}

	for( UINT r = 0; r < constRxLimit; r++ ) {

		CBuffDesc &Buff = m_pRxDesc[r];

		Buff.m_wCtrl  = 0;
		
		Buff.m_wCount = 0;

		Buff.m_pData  = phal->VirtualToPhysical(DWORD(m_pRxBuff) + r * constBuffSize);
		}
	}

void CFec51::ResetBuffers(void)
{
	for( UINT t = 0; t < constTxLimit; t++ ) {

		CBuffDesc &Buff = m_pTxDesc[t];

		Buff.m_wCount = 0;

		Buff.m_wCtrl  = txEnd;
		}

	for( UINT r = 0; r < constRxLimit; r++ ) {

		CBuffDesc &Buff = m_pRxDesc[r];
		
		Buff.m_wCount = constBuffSize;

		Buff.m_wCtrl  = rxEmpty | rxUsrTouch;
		}

	m_pTxDesc[constTxLimit-1].m_wCtrl |= txWrap;

	m_pRxDesc[constRxLimit-1].m_wCtrl |= rxWrap;

	m_uTxHead  = 0;

	m_uTxTail  = 0;

	m_uRxHead  = 0;
	
	m_uRxTail  = 0;
	}

void CFec51::FreeBuffers(void)
{
	free(m_pTxDesc);

	free(m_pRxDesc);

	free(m_pTxBuff);

	free(m_pRxBuff);
	}

void CFec51::StartRx(CBuffDesc *pBuff)
{
	pBuff->m_wCount = constBuffSize;

	pBuff->m_wCtrl &= rxWrap;

	pBuff->m_wCtrl |= (rxEmpty | rxUsrTouch);

	Reg(RDAR) = 1; 
	}

void CFec51::StartTx(CBuffDesc *pBuff)
{
	pBuff->m_wCtrl &= txWrap;

	pBuff->m_wCtrl |= txEnd | txCrc | txUsr1;

	pBuff->m_wCtrl |= txRdy;

	Reg(TDAR) = 1; 
	}

// MII Access

BOOL CFec51::AppPutMii(BYTE bAddr, WORD wData)
{
	PhyInterrupt(false);

	m_uMiiState = miiAppPut;

	BYTE bDelim   = 1;
	BYTE bCommand = 1;
	BYTE bPHY     = 2;
	BYTE bTurn    = 2;

	Reg(MMFR)     =	( (bDelim   << 30)
			| (bCommand << 28)
			| (bPHY     << 23)
			| (bAddr    << 18) 
			| (bTurn    << 16)
			| (wData    <<  0)
			);

	if( !m_pMiiFlag->Wait(1000) ) {

		AfxTrace("PHY Timeout on PutMII\n");

		PhyInterrupt(true);
		
		return FALSE;
		}

	Sleep(10);

	return TRUE;
	}

WORD CFec51::AppGetMii(BYTE bAddr)
{
	PhyInterrupt(false);

	m_uMiiState = miiAppGet;

	BYTE bDelim   = 1;
	BYTE bCommand = 2;
	BYTE bPHY     = 2;
	BYTE bTurn    = 2;

	Reg(MMFR)     =	( (bDelim   << 30)
			| (bCommand << 28)
			| (bPHY     << 23)
			| (bAddr    << 18) 
			| (bTurn    << 16)
			);

	if( !m_pMiiFlag->Wait(1000) ) {

		AfxTrace("PHY Timeout on GetMII\n");
		
		PhyInterrupt(true);

		return 0;
		}

	Sleep(10);

	return LOWORD(Reg(MMFR));
	}

void CFec51::IntPutMii(BYTE bAddr, WORD wData)
{
	m_uMiiState = miiIntPut;

	BYTE bDelim   = 1;
	BYTE bCommand = 1;
	BYTE bPHY     = 2;
	BYTE bTurn    = 2;

	Reg(MMFR)     =	( (bDelim   << 30)
			| (bCommand << 28)
			| (bPHY     << 23)
			| (bAddr    << 18) 
			| (bTurn    << 16)
			| (wData    <<  0)
			);
	}

void CFec51::IntGetMii(BYTE bAddr)
{
	PhyInterrupt(false);

	m_uMiiState = miiIntGet;

	BYTE bDelim   = 1;
	BYTE bCommand = 2;
	BYTE bPHY     = 2;
	BYTE bTurn    = 2;

	Reg(MMFR)     =	( (bDelim   << 30)
			| (bCommand << 28)
			| (bPHY     << 23)
			| (bAddr    << 18) 
			| (bTurn    << 16)
			);
	}

// Diagnostics

bool CFec51::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "nic0");

		pDiag->RegisterCommand(m_uProv, 1, "status");

		pDiag->Release();

		return true;
		}

	return false;
	}

bool CFec51::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return true;
		}

	return false;
	}

UINT CFec51::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		pOut->AddProp("State", "%u", m_uNicState);

		if( m_uNicState ) {

			pOut->AddProp("AllowFull", "%u", m_fAllowFull);
			pOut->AddProp("AllowFast", "%u", m_fAllowFast);
			pOut->AddProp("UsingFull", "%u", m_fUsingFull);
			pOut->AddProp("UsingFast", "%u", m_fUsingFast);
			pOut->AddProp("RxCount",   "%u", m_Diag.m_RxCount);
			pOut->AddProp("RxDisc",    "%u", m_Diag.m_RxDisc);
			pOut->AddProp("RxOver",    "%u", m_Diag.m_RxOver);
			pOut->AddProp("TxCount",   "%u", m_Diag.m_TxCount);
			pOut->AddProp("TxDisc",    "%u", m_Diag.m_TxDisc);
			pOut->AddProp("TxFail",    "%u", m_Diag.m_TxFail);
			}

		pOut->EndPropList();

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

// End of File
