
#include "Intern.hpp"

#include "IoMux51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// iMX51 I/O Multipexer
//

// Constructor

CIoMux51::CIoMux51(void)
{
	m_pBase = PVDWORD(ADDR_IOMUXC);
	}

// Operations

void CIoMux51::Set(PCDWORD pList, UINT uSize)
{
	for( UINT i = 0; i < uSize; i += 2 ) {

		Set(pList[i+0], pList[i+1]);
		}
	}

// End of File
