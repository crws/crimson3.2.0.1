
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Models_HPP

#define INCLUDE_Models_HPP

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Models
//

#define MODEL_EDGE		11
#define MODEL_CORE		12
#define MODEL_GRAPHITE_07	21
#define MODEL_GRAPHITE_09	22
#define MODEL_GRAPHITE_10S	23
#define MODEL_GRAPHITE_10V	24
#define MODEL_GRAPHITE_12	25
#define MODEL_GRAPHITE_15	26

// End of File

#endif
