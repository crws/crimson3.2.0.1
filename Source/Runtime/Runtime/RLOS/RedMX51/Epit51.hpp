
#include "Intern.hpp"

/////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Epit51_HPP
	
#define	INCLUDE_Epit51_HPP

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Enhanced Period Interval Timer
//

class CEpit51 : public IEventSink
{
	public:
		// Constructor
		CEpit51(IPic *pPic);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// Attributes
		UINT GetFraction(void);

		// Operations
		void SetEventHandler(IEventSink *pSink, UINT uParam);
		void SetPeriodic(UINT uPeriod);
		void EnableEvents(void);

	protected:
		// Registers
		enum
		{
			regControl = 0x0000 / sizeof(DWORD),
			regStatus  = 0x0004 / sizeof(DWORD),
			regLoad    = 0x0008 / sizeof(DWORD),
			regCompare = 0x000C / sizeof(DWORD),
			regCounter = 0x0010 / sizeof(DWORD),
			};

		// Data Members
		IPic	   * m_pPic;
		PVDWORD	     m_pBase;
		UINT	     m_uLine;
		UINT         m_uClock;
		IEventSink * m_pSink;
		UINT	     m_uParam;
		UINT         m_uPeriod;
		UINT         m_uLoad;
		UINT         m_uPrediv;

		// Implementation
		UINT FindLoadValue(UINT uPeriod);
	};

// End of File

#endif
