
#include "Intern.hpp"

#include "Uart51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ccm51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 UART
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Instantiator

IDevice * Create_Uart51(UINT iIndex, CCcm51 *pCcm)
{
	CUart51 *p = New CUart51(iIndex, pCcm);

	p->Open();

	return p;
	}

// Constructor

CUart51::CUart51(UINT iIndex, CCcm51 *pCcm)
{
	StdSetRef();

	m_pBase    = PVDWORD(FindBase(iIndex));

	m_uUnit    = iIndex;

	m_uLine    = FindLine(iIndex);

	m_uClock   = pCcm->GetFreq(CCcm51::clkUart);

	m_uState   = stateClosed;

	m_pHandler = NULL;

	m_pTimer   = CreateTimer();
	}

// Destructor

CUart51::~CUart51(void)
{
	m_pTimer->Release();
	}

// IUnknown

HRESULT CUart51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IPortObject);

	return E_NOINTERFACE;
	}

ULONG CUart51::AddRef(void)
{
	StdAddRef();
	}

ULONG CUart51::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CUart51::Open(void)
{
	m_pTimer->SetPeriod(5);

	m_pTimer->SetHook(this, 0);

	return TRUE;
	}

// IPortObject

void METHOD CUart51::Bind(IPortHandler *pHandler)
{
	m_pHandler = pHandler;

	m_pHandler->Bind(this);
	}

void METHOD CUart51::Bind(IPortSwitch *pSwitch)
{
	m_pSwitch = pSwitch; 
	}

UINT METHOD CUart51::GetPhysicalMask(void)
{
	return m_pSwitch->GetMask(m_uUnit);
}

BOOL METHOD CUart51::Open(CSerialConfig const &Config)
{
	if( m_uState == stateClosed ) {

		CAutoGuard Guard;

		m_Config = Config;

		if( InitUart() && InitBaudRate() && InitFlags() && InitFormat() ) {

			EnableEvents();

			m_uState = stateOpen;

			m_pHandler->OnOpen(m_Config);

			EnablePort();

			ConfigurePIC();

			EnableRecv();

			m_pTimer->Enable(true);

			return true;
			}
		}

	return false;
	}

void METHOD CUart51::Close(void)
{
	if( m_uState == stateOpen ) {

		CAutoGuard Guard;

		m_pTimer->Enable(false); 

		DisableEvents();

		WaitDone();

		DisablePort();

		m_pHandler->OnClose();

		m_uState = stateClosed;
		}
	}

void METHOD CUart51::Send(BYTE bData)
{
	if( m_uState == stateOpen ) {

		EnableInterrupts(false);

		EnableSend();

		SwitchTxOn(true);

		Reg(Send) = bData;

		EnableInterrupts(true);
		}
	}

void METHOD CUart51::SetBreak(BOOL fBreak)
{
	if( m_uState == stateOpen ) {

		SwitchTxOn(true);

		if( fBreak ) {

			AtomicBitSet(&Reg(Ctrl1), 4);
			}
		else {
			AtomicBitClr(&Reg(Ctrl1), 4);
			}

		EnableRecv();
		}
	}

void METHOD CUart51::EnableInterrupts(BOOL fEnable)
{
	phal->EnableLineViaIrql(m_uLine, m_uSave, fEnable);
	}

void METHOD CUart51::SetOutput(UINT uOutput, BOOL fOn)
{
	if( fOn ) {

		AtomicBitSet(&Reg(Ctrl2), 12);
		}
	else {
		AtomicBitClr(&Reg(Ctrl2), 12);
		}
	}

BOOL METHOD CUart51::GetInput(UINT uInput)
{
	return Reg(Stat1) & Bit(14);	
	}

DWORD METHOD CUart51::GetHandle(void)
{
	return NOTHING;
	}

// IEventSink

void CUart51::OnEvent(UINT uLine, UINT uParam)
{
	if( m_uState == stateOpen ) {

		if( uLine == m_uLine ) {

			if( Reg(Ctrl1) & bitTxFifo ) {

				bool fData = false;
				
				while( !(Reg(Test) & bitTxFull) ) {

					BYTE bData;

					if( m_pHandler->OnTxData(bData) ) {

						Reg(Send) = bData;

						fData     = true;

						continue;
						}

					if( fData ) {

						m_pHandler->OnTxDone();
						}
											
					EnableRecv();

					SwitchTxOn(false);

					break;
					}
				}

			if( Reg(Ctrl1) & bitRxFifo ) {

				bool fData = false;
				
				while( !(Reg(Test) & bitRxEmpty) ) {

					DWORD dwData = Reg(Recv);

					if( m_Config.m_uFlags & flagNoCheckParity ) {

						dwData &= ~(errError | errParity);
						}

					if( dwData & errReady ) {

						if( !(dwData & (errFrame | errBreak | errParity)) ) {

							m_pHandler->OnRxData(dwData & m_bMask);
						
							fData = true;
							}
						}
					}

				if( Reg(Stat1) & bitRxAged ) {

					Reg(Stat1) |= bitRxAged;
					}

				if( fData ) {

					m_pHandler->OnRxDone();
					}
				}
			}

		if( uLine == NOTHING ) {

			m_pHandler->OnTimer();
			}
		}
	}

// Implementation

bool CUart51::InitUart(void)
{
	Reset();

	Reg(Ctrl1) = 0x0000;

	Reg(Ctrl2) = 0x0007;

	Reg(Ctrl3) = 0x0004;

	Reg(Ctrl4) = 0x0010;
	
	Reg(Fifo)  = 0x4090;

	Reg(OneMs) = m_uClock / (5 * 1000);

	return true;
	}

bool CUart51::InitBaudRate(void)
{
	SetBaudRate(m_Config.m_uBaudRate);

	return true;
	}

bool CUart51::InitFlags(void)
{
	if( m_Config.m_uFlags & flagFastRx ) {
		
		Reg(Fifo) = 0x4081;
		}

	return true;
	}

bool CUart51::InitFormat(void)
{
	WORD wFmt = 0x0000;

	wFmt |= GetParityInit();

	wFmt |= GetStopBitsInit();

	wFmt |= GetDataBitsInit();

	wFmt |= GetPhysical();

	if( wFmt != 0xFFFF ) {

		m_bMask     = GetDataBitsMask();

		Reg(Ctrl2) |= wFmt;
		}

	return true;
	}

WORD CUart51::GetParityInit(void)
{
	switch( m_Config.m_uParity ) {

		case parityNone: return 0x0 << 7;
		case parityOdd : return 0x3 << 7;
		case parityEven: return 0x2 << 7;
		}

	return 0xFFFF;
	}

WORD CUart51::GetStopBitsInit(void)
{
	switch( m_Config.m_uStopBits ) {

		case 1: return 0x0 << 6;
		case 2: return 0x1 << 6;
		}

	return 0xFFFF;
	}

WORD CUart51::GetDataBitsInit(void)
{
	switch( m_Config.m_uDataBits ) {

		case 7:	return 0x0 << 5;
		case 8: return 0x1 << 5;
		}

	return 0xFFFF;
	}

BYTE CUart51::GetDataBitsMask(void)
{
	switch( m_Config.m_uDataBits ) {

		case 7:	return 0x7F;
		case 8: return 0xFF;
		}

	return 0x00;
	}

WORD CUart51::GetPhysical(void)
{
	switch( m_Config.m_uPhysical ) {
		
		case physicalRS232:

			m_pSwitch->SetPhysical(m_uUnit, false);

			m_pSwitch->SetFull(m_uUnit, true);

			SetOutput(0, true);

			if( m_Config.m_uFlags & flagHonorCTS ) {

				return 0;
				}

			return bitTxNoRTS;
		
		case physicalRS422Master:

			m_pSwitch->SetPhysical(m_uUnit, true);

			m_pSwitch->SetFull(m_uUnit, true);

			m_pSwitch->SetMode(m_uUnit, false);

			SetOutput(0, true);

			return bitTxNoRTS;

		case physicalRS422Slave:

			m_pSwitch->SetPhysical(m_uUnit, true);

			m_pSwitch->SetFull(m_uUnit, true);

			m_pSwitch->SetMode(m_uUnit, true);

			SetOutput(0, false);

			return bitTxNoRTS;

		case physicalRS485:

			m_pSwitch->SetPhysical(m_uUnit, true);

			m_pSwitch->SetFull(m_uUnit, false);

			m_pSwitch->SetMode(m_uUnit, true);

			SetOutput(0, false);

			return bitTxNoRTS;
		}

	return 0xFFFF;
	}

void CUart51::SwitchTxOn(bool fOn)
{
	switch( m_Config.m_uPhysical ) {

		case physicalRS422Slave:

		case physicalRS485:
		
			SetOutput(0, fOn);

			break;
		}
	}

void CUart51::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);
	}

void CUart51::DisableEvents(void)
{
	phal->EnableLine(m_uLine, false);
	}

void CUart51::EnablePort(void)
{
	Reg(Ctrl1) |= bitEnable;
	
	m_pSwitch->EnablePort(m_uUnit, true);
	}

void CUart51::DisablePort(void)
{
	Reg(Ctrl1) = 0x0000;

	m_pSwitch->EnablePort(m_uUnit, false);
	}

void CUart51::Reset(void)
{
	Reg(Ctrl2) = 0x0000;

	while( Reg(Test) & bitReset );
	}

void CUart51::EnableRecv(void)
{
	Reg(Stat1)  = bitRxAged;

	Reg(Ctrl1) |= bitRxFifo;

	Reg(Ctrl2) |= bitRxTimer;

	Reg(Ctrl1) &= ~bitTxFifo;
	}

void CUart51::EnableSend(void)
{
	Reg(Ctrl1) |= bitTxFifo;

	Reg(Ctrl1) &= ~bitRxFifo;

	Reg(Ctrl2) &= ~bitRxTimer;
	}

bool CUart51::WaitDone(void)
{
	UINT uBits, uTime;
		
	uBits  = m_Config.m_uDataBits + m_Config.m_uStopBits;

	uBits += (m_Config.m_uParity == parityNone) ? 1 : 2;

	uTime  = uBits * 1000 / m_Config.m_uBaudRate;	

	IThread *pThread = GetCurrentThread();

	if( pThread ) {

		pThread->SetTimer(100);

		while( pThread->GetTimer() ) {

			if( Reg(Stat2) & bitTxDone ) {

				return true;
				}

			Sleep(uTime);
			}
		
		return false;
		}
	else {
		UINT uTimeout = GetTickCount() + ToTicks(uTime);

		do {
			if( Reg(Stat2) & bitTxDone ) {

				return true;
				}
		
			} while( GetTickCount() < uTimeout );
		}

	return false;
	}

void CUart51::SetBaudRate(UINT uBaud)
{
	WORD wInc = 1024;

	UINT uMod = ((m_uClock / 5) * (wInc / 16)) / uBaud;

	while( uMod >= 65535 || (!(wInc & 1) && !(uMod & 1)) ) {

		wInc >>= 1;

		uMod >>= 1;
		}

	Reg(BaudInc) = wInc - 1;

	Reg(BaudMod) = uMod - 1;
	}

void CUart51::ConfigurePIC(void)
{
	switch( m_Config.m_uPhysical ) {

		case physicalRS422Slave:

		case physicalRS485:

			SwitchTxOn(false);

			Reg(Ctrl2) &= ~Bit(bitTxEnable);
			
			switch( m_Config.m_uParity ) {

				case parityNone: ;
				case parityOdd : ;
										
					Reg(Send) = 0x01;
					Reg(Send) = 0x00;

					break;

				case parityEven:
					
					Reg(Send) = 0x03;
					Reg(Send) = 0x00;

					break;
				}

			Reg(Ctrl2) |= Bit(bitTxEnable);

			while( !(Reg(Stat2) & bitTxDone) );
			
			break;
		}
	}

UINT CUart51::FindBase(UINT iIndex) const
{
	switch( iIndex ) {

		case 0 : return ADDR_UART1;
		case 1 : return ADDR_UART2;
		case 2 : return ADDR_UART3;
		}

	return NOTHING;
	}

UINT CUart51::FindLine(UINT iIndex) const
{
	switch( iIndex ) {

		case 0 : return INT_UART1;
		case 1 : return INT_UART2;
		case 2 : return INT_UART3;
		}

	return NOTHING;
	}

// End of File
