
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IpuCpmem51_HPP
	
#define	INCLUDE_IpuCpmem51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Channel Configuration
//

struct CCpmConfig
{
	bool	m_fInterleaved;
	BYTE	m_bBitsPerPixel;
	BYTE	m_bDecAddrSelect;
	BYTE	m_bAccessDimension;
	BYTE	m_bScanOrder;
	BYTE	m_bBandMode;
	BYTE	m_bBlockMode;
	BYTE	m_bRotation90;
	BYTE	m_bFlipHoriz;
	BYTE	m_bFlipVert;
	BYTE	m_bThresholdEnable;
	BYTE	m_bCondAccessEnable;
	BYTE	m_bCondAccessPolarity;
	BYTE	m_bPixelBurst;
	BYTE	m_bPixelFormat;
	BYTE	m_bAlphaUsed;
	BYTE	m_bAXI;
	BYTE	m_bThreshold;
	WORD	m_wHeight;
	WORD	m_wWidth;
	UINT	m_uLineStride;
	UINT	m_uLineStrideY;
	UINT	m_uLineStrideUV;
	BYTE	m_bComp0Witdh;
	BYTE	m_bComp1Witdh;
	BYTE	m_bComp2Witdh;
	BYTE	m_bComp3Witdh;
	BYTE	m_bComp0Offset;
	BYTE	m_bComp1Offset;
	BYTE	m_bComp2Offset;
	BYTE	m_bComp3Offset;
	};

struct CCpmBuffOffsets
{
	bool	m_fInterleaved;
	UINT	m_uOffsetU;
	UINT	m_uOffsetV;
	UINT	m_uInterlaceOffset;
	};

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Channel Parameter Memory
//

class CIpuChanParam51
{	
	public:
		// Constructor
		CIpuChanParam51(void);

		// Interface
		void ClearAll(void);
		bool SetConfig(UINT uChan, CCpmConfig const &Config); 
		bool SetConfig(UINT uChan, UINT uWidth, UINT uHeight, UINT uBpp, UINT uStride, bool fInt);
		bool SetBuffer(UINT uChan, UINT uNum,  DWORD dwAddr);
		bool SetOffset(UINT uChan, CCpmBuffOffsets const &Config);
		bool SetBandMode(UINT uChan, UINT uMode);
		bool SetXScroll(UINT uChan, UINT uScroll);

	protected:
		// Data
		PVDWORD m_pBase;

		// Implementation
		UINT FindAlphaChanMapping(UINT uChan) const;
		void BitSet(PVDWORD pReg, DWORD Data, UINT uPos, UINT uSize);
		UINT BitGet(PVDWORD pReg, UINT nPos, UINT uSize);
	};

// End of File

#endif
