
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Rtc51_HPP
	
#define	INCLUDE_Rtc51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../RedDev/RtcGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPmui51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 PMUI Real Time Clock
//

class CRtc51 : public CRtcGeneric
{
	public:
		// Constructor
		CRtc51(CPmui51 *pPmui);

		// Destructor
		~CRtc51(void);

		// IRtc
		UINT  METHOD GetCalibStatus(void);
		INT   METHOD GetCalib(void);
		bool  METHOD PutCalib(INT Calib);

	protected:
		// Constants
		static const DWORD constRtcMagic;

		// Data Members
		CPmui51		* m_pPmui;
		ISerialMemory	* m_pMem;

		// Overridables
		bool InitChip(void);
		bool GetTimeFromChip(void);
		bool GetSecsFromChip(UINT &uSecs);
		bool WriteTimeToChip(void);

		// Implementation
		INT  GetCalFromChip(void);
		bool WriteCalToChip(INT Calib);
		void CalibFromMram(void);
		void CalibToMram(INT nCalib);
	};

// End of File

#endif
