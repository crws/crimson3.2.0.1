
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_I2c51_HPP
	
#define	INCLUDE_I2c51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCcm51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 IC2 Controller
//

class CI2c51 : public II2c, public IEventSink
{
	public:
		// Constructor
		CI2c51(CCcm51 *pCcm);

		// Destructor
		~CI2c51(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// II2c
		BOOL METHOD Lock(UINT uTime);
		void METHOD Free(void);
		BOOL METHOD Send(BYTE bChip, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData);
		BOOL METHOD Recv(BYTE bChip, PCBYTE pHead, UINT uHead, PBYTE  pData, UINT uData);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Registers
		enum
		{
			regAddr	= 0x0000 / sizeof(WORD),
			regFreq	= 0x0004 / sizeof(WORD),
			regCtrl = 0x0008 / sizeof(WORD),
			regStat = 0x000C / sizeof(WORD),
			regData = 0x0010 / sizeof(WORD)
			};

		// Control Bits
		enum
		{
			ctrlEnable	= Bit(7),
			ctrlIntEnable	= Bit(6),
			ctrlMaster	= Bit(5),
			ctrlSend	= Bit(4),
			ctrlNoTxAck	= Bit(3),
			ctrlRepeat	= Bit(2)
			};

		// Status Bits
		enum
		{
			statDone	= Bit(7),
			statAddress	= Bit(6),
			statBusBusy	= Bit(5),
			statArbLost	= Bit(4),
			statSlaveTx	= Bit(2),
			statIntPend	= Bit(1),
			statRxNak	= Bit(0)
			};

		// Data Members
		PVWORD	     m_pBase;
		ULONG	     m_uRefs;
		UINT	     m_uLine;
		DWORD        m_uFreq;
		IMutex     * m_pLock;
		IEvent     * m_pDone;
		BYTE	     m_bAddr;
		PCBYTE	     m_pHead;
		UINT	     m_uHead;
		PBYTE	     m_pData;
		UINT	     m_uData;
		bool	     m_fSend;
		bool	     m_fWait;
		bool	     m_fOkay;
		bool	     m_fBusy;
		UINT	     m_uState;
		UINT	     m_uPtr;
		BYTE	     m_bCmd;

		// Implementation
		void InitController(void);
		void InitEvents(void);
		void StartSend(void);
		void StartRepeat(void);
		void StartRecv(void);
		void SetStop(void);
		void SetRecv(void);
		void SetSend(void);
		void SetAck(bool fAck);
		void ClearPending(void);
		void EndSequence(bool fOkay);
		bool CanWait(void);
		void TestWait(void);
		void WaitDone(void);
		UINT FindDivider(UINT uFreq) const;
	};

// End of File

#endif
