
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Pwm51_HPP
	
#define	INCLUDE_Pwm51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCcm51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Pulse Width Modulator
//

class CPwm51
{
	public:
		// Constructor
		CPwm51(UINT iIndex, CCcm51 *pCcm);

		// Operations
		void SetFreq(UINT uFreq);
		void SetDuty(UINT uDuty);

	protected:
		// Registers
		enum
		{
			regControl = 0x0000 / sizeof(DWORD),
			regStatus  = 0x0004 / sizeof(DWORD),
			regInt     = 0x0008 / sizeof(DWORD),
			regSample  = 0x000C / sizeof(DWORD),
			regPeriod  = 0x0010 / sizeof(DWORD),
			regCount   = 0x0014 / sizeof(DWORD),
			};

		// Data Members
		PVDWORD m_pBase;
		DWORD   m_uFreq;
		UINT    m_uPre;
		UINT    m_uPeriod;

		// Implementation
		void Init(void);
	};

// End of File

#endif
