
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PlatformMX51_HPP

#define INCLUDE_PlatformMX51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../RedCore/PlatformRed.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIoMux51;
class CDpll51;
class CCcm51;
class CPmui51;

/////////////////////////////////////////////////////////////////////////
//
// Platform Object for iMX51 Device
//

class CPlatformMX51 : public CPlatformRed,
		      public IDiagProvider,
		      public IEventSink
{
	public:
		// Constructor
		CPlatformMX51(void);

		// Destructor
		~CPlatformMX51(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IPlatform
		PCTXT GetFamily(void);
		PCTXT GetModel(void);
		PCTXT GetVariant(void);
		UINT  GetFeatures(void);
		UINT  GetGroup(void);
		BOOL  IsService(void);
		BOOL  IsEmulated(void);
		BOOL  HasMappings(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Data
		UINT	   m_uProv;
		UINT       m_uModel;
		CIoMux51 * m_pMux;
		CCcm51   * m_pCcm;
		CDpll51  * m_pDpll[3];
		IGpio    * m_pGpio[4];
		ITimer   * m_pTimer;
		CPmui51  * m_pPmui;
		UINT       m_uPmui;

		// IDiagProvider
		UINT RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);
		
		// Implementation
		void InitTimer(void);

		// Inititialisation
		virtual void InitPriorities(void);
		virtual void InitClocks(void);
		virtual void InitRails(void);
		virtual void InitMux(void);
		virtual void InitGpio(void);

		// Diagnostics
		bool DiagRegister(void);
		bool DiagRevoke(void);
		UINT DiagClocks(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagRails(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
