
#include "Intern.hpp"

#include "HalMX51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Watchdog51.hpp"

#include "Gpt51.hpp"

#include "Epit51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for iMX51
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CHalMX51::CHalMX51(void)
{
	m_pEntropy   = PVDWORD(ADDR_EPIT1 + 0x10);

	m_uDebugAddr = 0;

	m_uDebugLine = 0;
	
	m_pRead      = NULL;
	
	m_uHead      = 0;
	
	m_uTail      = 0;
	}

// Destructor

CHalMX51::~CHalMX51(void)
{
	}

// Initialization

void CHalMX51::Open(void)
{
	FindDebug();

	CArmHal::Open();

	CreateDevices();

	StartTimer();

	AfxTrace("HAL initialized\n");
	}

// IHal Timer Support

UINT CHalMX51::GetTickFraction(void)
{	
	return m_pEpit->GetFraction();
	}

void CHalMX51::SpinDelayFast(UINT uTime)
{
	m_pGpt->SpinDelay(uTime);
	}

// IHal Debug Support

void CHalMX51::DebugReset(void)
{
	// Not sure this is needed on Aeon...
	}

UINT CHalMX51::DebugRead(UINT uDelay)
{
	if( m_uDebugAddr ) {

		if( !m_pRead ) {

			if( AfxNewObject("semaphore", ISemaphore, m_pRead) == S_OK ) {

				m_pPic->SetLineHandler(m_uDebugLine, this, 2);

				Reg(Stat1)  = bitRxAged;

				Reg(Ctrl1) |= bitRxFifo;

				Reg(Ctrl2) |= bitRxTimer;

				m_pPic->EnableLine(m_uDebugLine, true);
				}
			}

		if( m_pRead ) {

			if( m_pRead->Wait(uDelay) ) {

				BYTE bData = m_bBuff[m_uHead];

				m_uHead = (m_uHead + 1) % elements(m_bBuff);

				return bData;
				}

			return NOTHING;
			}
		}

	Sleep(uDelay);

	return NOTHING;
	}

void CHalMX51::DebugOut(char cData)
{
	if( m_uDebugAddr ) {

		if( cData == '\n' ) {

			while( Reg(Test) & bitTxFull );

			Reg(Send) = '\r';
			}

		while( Reg(Test) & bitTxFull );

		Reg(Send) = cData;
		}
	}

void CHalMX51::DebugWait(void)
{
	if( m_uDebugAddr ) {

		while( !(Reg(Test)  & bitTxEmpty) );

		while( !(Reg(Stat2) & bitTxDone ) );
		}
	}

void CHalMX51::DebugStop(void)
{
	#if defined(_DEBUG)

	if( m_uDebugAddr ) {

		PCSTR p = "*** STOP ***\n";
	
		while( *p ) {
		
			DebugOut(*p++);
			}

		for( UINT e = 0; e < 2; ) {
		
			HostMaxIpr();

			while( !(Reg(Test) & bitRxEmpty) ) {

				if( BYTE(Reg(Recv)) == 0x1B ) {

					e++;
					}
				}
		
			m_pDog->Kick();
			}
		}

	#endif

	for(;;) HostMaxIpr();
	}

void CHalMX51::DebugKick(void)
{
	m_pDog->Kick();
	}

// IEventSink

void CHalMX51::OnEvent(UINT uLine, UINT uParam)
{
	if( uParam == 1 ) {

		CArmHal::OnEvent(uLine, uParam);

		m_pDog->Kick();
		}

	if( uParam == 2 ) {

		if( Reg(Ctrl1) & bitRxFifo ) {

			UINT uData = 0;
				
			while( !(Reg(Test) & bitRxEmpty) ) {

				DWORD dwData = Reg(Recv);

				if( m_Config.m_uFlags & flagNoCheckParity ) {

					dwData &= ~(errError | errParity);
					}

				if( dwData & errReady ) {

					if( !(dwData & (errFrame | errBreak | errParity)) ) {

						UINT uNext = (m_uTail + 1) % elements(m_bBuff);

						if( uNext != m_uHead ) {

							m_bBuff[m_uTail] = BYTE(dwData) & m_bMask;

							m_uTail          = uNext;

							uData++;
							}
						}
					}
				}

			if( Reg(Stat1) & bitRxAged ) {

				Reg(Stat1) |= bitRxAged;
				}

			if( uData ) {

				m_pRead->Signal(uData);
				}
			}
		}
	}

// Power Management Hooks

void CHalMX51::EnterLowPower(void)
{
	// This is a simple implementation that just lowers the
	// core clock frequency by a factor of 4. We should ideally
	// be lowering the voltage as well, but that will mean some
	// work with the GPC and the CCM. Consider for later!!!

	UINT ipr;

	HostRaiseIpr(ipr);

	*PVDWORD(0x73FD4010) = 0x03;

	m_fIdle = true;

	HostLowerIpr(ipr);
	}

void CHalMX51::LeaveLowPower(void)
{
	UINT ipr;

	HostRaiseIpr(ipr);

	*PVDWORD(0x73FD4010) = 0x00;

	m_fIdle = false;

	HostLowerIpr(ipr);
	}

// Implementation

void CHalMX51::FindDebug(void)
{
	m_uClock = 54000000;

	memset(&m_Config, 0, sizeof(m_Config));

	m_Config.m_uBaudRate = 115200;
	m_Config.m_uDataBits = 8;
	m_Config.m_uStopBits = 1;
	m_Config.m_uParity   = parityNone;

	m_pBase = PVDWORD(m_uDebugAddr);

	Reg(Ctrl2) = 0;

	while( Reg(Test) & bitReset );

	InitUart();

	InitBaudRate();

	InitFlags();

	InitFormat();

	Reg(Ctrl1) |= bitEnable;
	}

bool CHalMX51::InitUart(void)
{
	Reg(Ctrl1) = 0x0000;

	Reg(Ctrl2) = 0x0007;

	Reg(Ctrl3) = 0x0004;

	Reg(Ctrl4) = 0x0010;
	
	Reg(Fifo)  = 0x4090;

	Reg(OneMs) = m_uClock / (5 * 1000);

	return true;
	}

bool CHalMX51::InitBaudRate(void)
{
	SetBaudRate(m_Config.m_uBaudRate);

	return true;
	}

bool CHalMX51::InitFlags(void)
{
	if( m_Config.m_uFlags & flagFastRx ) {
		
		Reg(Fifo) = 0x4081;
		}

	return true;
	}

bool CHalMX51::InitFormat(void)
{
	WORD wFmt = 0x0000;

	wFmt |= GetParityInit();

	wFmt |= GetStopBitsInit();

	wFmt |= GetDataBitsInit();

	wFmt |= bitTxNoRTS;

	if( wFmt != 0xFFFF ) {

		m_bMask     = GetDataBitsMask();

		Reg(Ctrl2) |= wFmt;
		}

	return true;
	}

WORD CHalMX51::GetParityInit(void)
{
	switch( m_Config.m_uParity ) {

		case parityNone: return 0x0 << 7;
		case parityOdd : return 0x3 << 7;
		case parityEven: return 0x2 << 7;
		}

	return 0xFFFF;
	}

WORD CHalMX51::GetStopBitsInit(void)
{
	switch( m_Config.m_uStopBits ) {

		case 1: return 0x0 << 6;
		case 2: return 0x1 << 6;
		}

	return 0xFFFF;
	}

WORD CHalMX51::GetDataBitsInit(void)
{
	switch( m_Config.m_uDataBits ) {

		case 7:	return 0x0 << 5;
		case 8: return 0x1 << 5;
		}

	return 0xFFFF;
	}

BYTE CHalMX51::GetDataBitsMask(void)
{
	switch( m_Config.m_uDataBits ) {

		case 7:	return 0x7F;
		case 8: return 0xFF;
		}

	return 0x00;
	}

void CHalMX51::SetBaudRate(UINT uBaud)
{
	WORD wInc = 1024;

	UINT uMod = ((m_uClock / 5) * (wInc / 16)) / uBaud;

	while( uMod >= 65535 || (!(wInc & 1) && !(uMod & 1)) ) {

		wInc >>= 1;

		uMod >>= 1;
		}

	Reg(BaudInc) = wInc - 1;

	Reg(BaudMod) = uMod - 1;
	}

void CHalMX51::CreateDevices(void)
{
	m_pDog  = New CWatchdog51;

	m_pPic  = Create_Tzic51();

	m_pMmu  = Create_Mmu51(this);

	m_pGpt  = New CGpt51;

	m_pEpit = New CEpit51(m_pPic);

	m_pDog->Enable(1);

	m_pDog->Kick();
	}

void CHalMX51::StartTimer(void)
{
	m_pEpit->SetPeriodic(GetTimerResolution());
	
	m_pEpit->SetEventHandler(this, 1);
	
	m_pEpit->EnableEvents();
	}

// End of File
