
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Battery51_HPP
	
#define	INCLUDE_Battery51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Interfaces
//

#include "../../StdEnv/IRtc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPmui51;

//////////////////////////////////////////////////////////////////////////
//
// PMIC Battery Monitor
//

class CBattery51 : public IBatteryMonitor, public IEventSink
{
	public:
		// Constructor
		CBattery51(CPmui51 *pPmui);

		// Destructor
		virtual ~CBattery51(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IBatteryMonitor
		bool METHOD IsBatteryGood(void);
		bool METHOD IsBatteryBad(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Constants
		static UINT const constThreshold = 2400;
		static UINT const constPeriod	 = 3600000;

		// Data Members
		ULONG     m_uRefs;
		CPmui51 * m_pPmui;
		ITimer  * m_pTimer;
		UINT      m_uBatt;

		// Events

	};

// End of File

#endif
