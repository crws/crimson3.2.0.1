
#include "Intern.hpp"

#include "Gpt51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define Reg(x) (m_pBase[reg##x])

//////////////////////////////////////////////////////////////////////////
//
// iMX51 General Purpose Timer
//

// Constructor

CGpt51::CGpt51(void)
{
	m_pBase  = PVDWORD(ADDR_GPT);

	m_uClock = 66500000;
	}

// Operations

void CGpt51::SetPeriod(UINT uPeriod)
{
	Reset();

	Reg(Prescale) = m_uClock / (1000000 * (m_uPeriod = uPeriod)); 

	Reg(Control)  = Bit(9) | (1 << 6);

	Reg(Control) |= Bit(0);

	Sleep(10);
	}

void CGpt51::SpinDelay(UINT uDelay)
{
	DWORD dwNow = Reg(Counter);

	DWORD dwEnd = dwNow + uDelay + 1;

	if( dwEnd > dwNow ) {

		while( Reg(Counter) < dwEnd );
		}
	else {
		while( Reg(Counter) > dwEnd );

		while( Reg(Counter) < dwEnd );
		}
	}

// Implementation

void CGpt51::Reset(void)
{
	Reg(Control) = Bit(15);

	while( Reg(Control) & Bit(15) );
	}

// End of File
