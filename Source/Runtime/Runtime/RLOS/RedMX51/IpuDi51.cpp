
#include "Intern.hpp"

#include "Ipu51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IpuDi51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Image Processor Unit Display Interface Module
//

// Register Access

#define Reg(x)		(m_pBase[reg##x])

#define RegN(x, n)	(m_pBase[reg##x + n])

// Constructor

CIpuDisplayInterface51::CIpuDisplayInterface51(UINT iIndex, CIpu51 *pIpu, UINT uClock)
{
	m_pBase  = PVDWORD(ADDR_IPUEX) + (iIndex == 0 ? CIpu51::regDi0 : CIpu51::regDi1);

	m_uUnit  = iIndex == 0 ? CIpu51::modDi0 : CIpu51::modDi1;

	m_pIpu   = pIpu;

	m_uClock = uClock;
	}

// Interface

void CIpuDisplayInterface51::Enable(bool fEnable)
{
	m_pIpu->EnableModule(m_uUnit, fEnable);
	}

void CIpuDisplayInterface51::SetBaseClock(UINT uFreq)
{
	UINT uDn  = m_uClock / uFreq;

	UINT uDiv = INT64(m_uClock) * 16 / uFreq;

	MakeMax(uDiv, 16);

	Reg(BsClkGen0) = uDiv;

	Reg(BsClkGen1) = (uDn << 16);
	}

void CIpuDisplayInterface51::SetPolarity(bool fClock, bool fChip0, bool fChip1, BYTE bData)
{
	DWORD dwData = 0;

	dwData |= (fClock ? Bit(17) : 0);

	dwData |= (fChip0 ? Bit( 8) : 0);

	dwData |= (fChip1 ? Bit( 9) : 0);

	dwData |= bData;

	Reg(General) = dwData;
	}

void CIpuDisplayInterface51::SetActiveWindowH(UINT uStart, UINT uEnd, UINT uCounter, UINT uTrigger)
{
	Reg(Window0) = (uTrigger << 28) | (uEnd << 16) | (uCounter << 12) | uStart;
	}

void CIpuDisplayInterface51::SetActiveWindowV(UINT uStart, UINT uEnd, UINT uCounter)
{
	Reg(Window1) = (uEnd << 16) | (uCounter << 12) | uStart;
	}

void CIpuDisplayInterface51::SetScreenHeight(UINT uHeight)
{
	Reg(ScrConf) = uHeight - 1;
	}

void CIpuDisplayInterface51::SetSyncConfig(UINT uSel, CSyncConfig const &Config)
{
	if( uSel >= 1 && uSel <= 9 ) {

		uSel --;
		
		DWORD dwReg1 = 0;

		dwReg1 |= (Config.m_uRun		<< 19);
	
		dwReg1 |= (Config.m_uRunTrig		<< 16);
	
		dwReg1 |= (Config.m_uOffset		<<  3);
	
		dwReg1 |= (Config.m_uOffsetTrig		<<  0);

		DWORD dwReg2 = 0;
	
		dwReg2 |= (Config.m_uPolarityEnable	<< 29);
	
		dwReg2 |= (Config.m_uAutoReload ? Bit(28) : 0);
	
		dwReg2 |= (Config.m_uClearSel		<< 25);
	
		dwReg2 |= (Config.m_uPosDn 		<< 17);
	
		dwReg2 |= (Config.m_uToggleTrigSel	<< 12);
	
		dwReg2 |= (Config.m_uPolarityClearSel	<<  9);
	
		dwReg2 |= (Config.m_uPosUp		<<  1);
	
		DWORD dwMask = 0x0FFF;

		DWORD dwReg3 = Config.m_uStepRepeat;

		if( uSel & 1 ) {

			dwMask <<= 16;

			dwReg3 <<= 16;
			}

		RegN(SwGen0x, uSel) = dwReg1;
	
		RegN(SwGen1x, uSel) = dwReg2;

		RegN(StepRepx,  uSel/2) &= ~dwMask;

		RegN(StepRepx,  uSel/2) |= dwReg3;
		}
	}

void CIpuDisplayInterface51::SetLineCounter(UINT uSel)
{
	if( uSel >= 1 && uSel <= 8 ) {
		
		Reg(SyncAsGen) &= ~(0x7 << 13);

		Reg(SyncAsGen) |= ((uSel-1) << 13);
		}
	}

void CIpuDisplayInterface51::SetSyncStart(UINT uStart)
{
	Reg(SyncAsGen) &= ~0xFFF;

	Reg(SyncAsGen) |= (uStart & 0xFFF);
	}

void CIpuDisplayInterface51::SetWaveformConfig(UINT uPtr, CWaveformConfig const &Config)
{
	if( uPtr < 12 ) {

		DWORD dwAccessSize = (DWORD)((INT64) Config.m_uAccessSize * m_uClock / 1000000000 - 1);
		
		DWORD dwCompSize   = (DWORD)((INT64) Config.m_uCompSize * m_uClock / 1000000000 - 1);
		
		DWORD dwData = ((dwAccessSize      & 0xFF) << 24) |
			       ((dwCompSize        & 0xFF) << 16) |
			       ((Config.m_uChipSel & 0x03) << 14) |
			       ((Config.m_uPin17   & 0x03) << 12) |
			       ((Config.m_uPin16   & 0x03) << 10) |
			       ((Config.m_uPin15   & 0x03) <<  8) |
			       ((Config.m_uPin14   & 0x03) <<  6) |
			       ((Config.m_uPin13   & 0x03) <<  4) |
			       ((Config.m_uPin12   & 0x03) <<  2) |
			       ((Config.m_uPin11   & 0x03) <<  0) ;

		RegN(DwGenx, uPtr) = dwData;
		}
	}

void CIpuDisplayInterface51::SetDataWaveSet(UINT uPtr, UINT uSet, UINT uUp, UINT uDn)
{
	if( uPtr < 12 ) {

		uUp = (UINT)((INT64) uUp * m_uClock / 1000000000);
		
		uDn = (UINT)((INT64) uDn * m_uClock / 1000000000);

		DWORD dwData = MAKELONG(uUp, uDn);

		switch( uSet ) {

			case 0:
				RegN(DwSet0x, uPtr) = dwData;

				break;

			case 1:
				RegN(DwSet1x, uPtr) = dwData;

				break;

			case 2:
				RegN(DwSet2x, uPtr) = dwData;

				break;

			case 3:
				RegN(DwSet3x, uPtr) = dwData;

				break;
			}
		}
	}

// End of File
