
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Ipu51_HPP
	
#define	INCLUDE_Ipu51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Dispaly Config
//

struct CDisplayConfig
{
	UINT m_xSize;
	UINT m_ySize;
	UINT m_uHorzFront;
	UINT m_uHorzSync; 
	UINT m_uHorzBack;
	UINT m_uVertFront;
	UINT m_uVertSync;
	UINT m_uVertBack;
	UINT m_uFrequency;
	};

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpuDisplayControl51;
class CIpuDisplayInterface51;
class CIpuDisplayFifo51;
class CIpuImageDma51;
class CIpuChanParam51;
class CCcm51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Image Processing Unit
//

class CIpu51
{	
	public:
		// Constructor
		CIpu51(CCcm51 *pCcm);

		// Destructor
		~CIpu51(void);

		// Configuration
		void Configure(CDisplayConfig const &Config);
		void SetFrame(PBYTE pFrameBuf);
		void Update(void);

		// Channels
		enum
		{
			chanVdiInputPrev	=  8,
			chanVdiInputCurr	=  9,
			chanVdiInputNext	= 10,
			chanPpInputVideo	= 11,
			chanPrpInputVideo	= 12,
			chanVdiOuptut		= 13,
			chanPrpInputGraphic	= 14,
			chanPpInputGraphic	= 15,
			chanPrpTrans		= 17,
			chanPpTrans		= 18,
			chanPrpOutputEnc	= 20,
			chanPrpOutputVf		= 21,
			chanPpOutput		= 22,
			chanDpPrimaryMain	= 23,
			chanDpSecondaryMain	= 24,
			chanDpPrimaryAux	= 27,
			chanDcSyncAsync		= 28,
			chanDpSecondaryAux	= 29,
			chanDpPrimaryAuxTran	= 30,
			chanDpSecondaryAuxTran	= 33,
			chanDcRead		= 40,
			chanDcAsync		= 41,
			chanDcCmdStream1	= 42,
			chanDcCmdStream2	= 43,
			chanDcOutputMask	= 44,
			chanIrtInputPrpEnc	= 45,
			chanIrtInputPpVf	= 46,
			chanIrtInputPp		= 47,
			chanIrtOutputPrpEnc	= 48,
			chanIrtOutputPrpVf	= 49,
			chanIrtOutputPp		= 50,
			chanDpPrimaryMainTran	= 51,
			chanDpSecondaryMainTran	= 52,
			};

		// Sub Modules
		enum
		{
			addrMem		= 0x00000000,
			addrCommon	= 0x1E000000,
			addrIdmac	= 0x1E008000,
			addrIc		= 0x1E020000,
			addrIrt		= 0x1E028000,
			addrCsi0	= 0x1E030000,
			addrCsi1	= 0x1E038000,
			addrDi0		= 0x1E040000,
			addrDi1		= 0x1E048000,
			addrSmfc	= 0x1E050000,
			addrDc		= 0x1E058000,
			addrDmfc	= 0x1E060000,
			addrVdi		= 0x1E068000,
			addrCpmem	= 0x1F000000,
			addrLut		= 0x1F020000,
			addrSrm		= 0x1F040000,
			addrTrm		= 0x1F060000,
			addrDctpl	= 0x1F080000,
			};

		// Sub Modules
		enum
		{
			regMem		= addrMem    / sizeof(DWORD),
			regCommon	= addrCommon / sizeof(DWORD),
			regIdmac	= addrIdmac  / sizeof(DWORD),
			regIc		= addrIc     / sizeof(DWORD),
			regIrt		= addrIrt    / sizeof(DWORD),
			regCsi0		= addrCsi0   / sizeof(DWORD),
			regCsi1		= addrCsi1   / sizeof(DWORD),
			regDi0		= addrDi0    / sizeof(DWORD),
			regDi1		= addrDi1    / sizeof(DWORD),
			regSmfc		= addrSmfc   / sizeof(DWORD),
			regDc		= addrDc     / sizeof(DWORD),
			regDmfc		= addrDmfc   / sizeof(DWORD),
			regVdi		= addrVdi    / sizeof(DWORD),
			regCpmem	= addrCpmem  / sizeof(DWORD),
			regLut		= addrLut    / sizeof(DWORD),
			regSrm		= addrSrm    / sizeof(DWORD),
			regTrm		= addrTrm    / sizeof(DWORD),
			regDctpl	= addrDctpl  / sizeof(DWORD),
			};

		// Sub Modules
		enum
		{
			modCsi0		=  0,
			modCsi1		=  1,
			modIc		=  2, 
			modIrt		=  3,
			modDp		=  5,
			modDi0		=  6,
			modDi1		=  7,
			modSmfc		=  8,
			modDc		=  9,
			modDmfc		= 10,
			modSisg		= 11,
			modVdi		= 12,
			};

	protected:
		// Common Registers
		enum 
		{
			regConfig	= 0x1E000000 / sizeof(DWORD),
			regSisgCtrl0	= 0x1E000004 / sizeof(DWORD),
			regSisgCtrl1	= 0x1E000008 / sizeof(DWORD),
			regSisgSetx	= 0x1E00000C / sizeof(DWORD), 
			regSisgClrx	= 0x1E000024 / sizeof(DWORD), 
			regIntCtrlx	= 0x1E00003C / sizeof(DWORD), 
			regSdmaEvent	= 0x1E000078 / sizeof(DWORD),
			regSrmPri1	= 0x1E0000A0 / sizeof(DWORD),
			regSrmPri2	= 0x1E0000A4 / sizeof(DWORD),
			regFsuProcFlow1	= 0x1E0000A8 / sizeof(DWORD),
			regFsuProcFlow2	= 0x1E0000AC / sizeof(DWORD),
			regFsuProcFlow3	= 0x1E0000B0 / sizeof(DWORD),
			regFsuDispFlow1	= 0x1E0000B4 / sizeof(DWORD),
			regFsuDispFlow2	= 0x1E0000B8 / sizeof(DWORD),
			regSkip		= 0x1E0000BC / sizeof(DWORD),
			regDispAltConf	= 0x1E0000C0 / sizeof(DWORD),
			regDispGen	= 0x1E0000C4 / sizeof(DWORD),
			regDispAlt1	= 0x1E0000C8 / sizeof(DWORD),
			regDispAlt2	= 0x1E0000CC / sizeof(DWORD),
			regDispAlt3	= 0x1E0000D0 / sizeof(DWORD),
			regDispAlt4	= 0x1E0000D4 / sizeof(DWORD),
			regSnoop	= 0x1E0000D8 / sizeof(DWORD),
			regMemRst	= 0x1E0000DC / sizeof(DWORD),
			regPm		= 0x1E0000E0 / sizeof(DWORD),
			regGeneral	= 0x1E0000E4 / sizeof(DWORD),
			regChDbMode0	= 0x1E000150 / sizeof(DWORD),
			regChDbMode1	= 0x1E000154 / sizeof(DWORD),
			regAltChDbMode0	= 0x1E000168 / sizeof(DWORD),
			regAltChDbMode1	= 0x1E00016C / sizeof(DWORD),
			regChTrbMode0	= 0x1E000178 / sizeof(DWORD),
			regChTrbMode1	= 0x1E00017C / sizeof(DWORD),
			regIntStat1	= 0x1E000200 / sizeof(DWORD),
			regCurBuf0	= 0x1E00023C / sizeof(DWORD),
			regCurBuf1	= 0x1E000240 / sizeof(DWORD),
			regChBuf0Rdy0	= 0x1E000268 / sizeof(DWORD),
			regChBuf0Rdy1	= 0x1E00026C / sizeof(DWORD),
			regChBuf1Rdy0	= 0x1E000270 / sizeof(DWORD),
			regChBuf1Rdy1	= 0x1E000274 / sizeof(DWORD),
			};

		// Sync Pins
		enum 
		{
			syncPixel	 = 0,
			syncISync	 = 1,
			syncHSync	 = 2,
			syncVSync	 = 3,
			syncActiveLine	 = 4,
			syncActiveClock8 = 5,
			syncActiveClock  = 6,
			syncDE		 = 8,
			};

		// Waveform
		enum
		{
			waveformSync	 = 3,
			};

		// Async Pins
		enum
		{
			asyncNouse      = 3,
			asyncReady      = 0,
			};

		// Channels
		enum
		{
			chanSync	= 1,
			chanPrimary	= 5,
			};

		// Data
		PVDWORD			  m_pBase;
		DWORD			  m_dwFrame;
		UINT			  m_uLineS;
		UINT			  m_uLineE;
		UINT			  m_uClock;
		UINT                      m_uType;
		CDisplayConfig		  m_Display;
		CIpuDisplayControl51    * m_pDc;
		CIpuDisplayInterface51	* m_pDi[2];
		CIpuDisplayFifo51	* m_pDmfc;
		CIpuImageDma51		* m_pIdmac;
		CIpuChanParam51		* m_pCpmem;
		UINT                      m_uBpp;

		// Init
		void Init(void);
		void InitCommon(void);
		void InitCpmem(void);
		void InitDp(void);
		void InitDmfc(void);
		void InitIdmac(void);

		// Config
		void ConfigDc(void);
		void ConfigDi(void);
		void ConfigCpmem(void);
		void ConfigDmfc(void);
		void ConfigIdmac(void);
		void EnableFlow(void);

		// Common
		void ResetMemory(void);
		void DisableModules(void);
		void EnableModules(void);
		void EnableModule(UINT uMod, bool fEnable);
		void ReleaseDiCounters(void);
		void SetDBuffMode(UINT uChan, BOOL fEnable);
		
		// Display Interface
		void InitDiWaveforms(void);
		void InitDiISync(void);
		void InitDiHSync(void);
		void InitDiVSync(void);
		void InitDiActiveLine(void);
		void InitDiActiveClock(void);
		void InitDiActiveClock8(void);
		void InitDiDataEnable(void);
		void InitDiMisc(void);

		// Display Controller
		void InitDcChannels(void);
		void InitDcDisplays(void);
		void InitDcBusMapping(void);
		void InitDcMicrocode(void);
		void InitDcEvents(void);
		void InitDcMisc(void);

		// Objects
		void MakeObjects(void);
		void FreeObjects(void);

		// Friends
		friend class CIpuDisplayControl51;
		friend class CIpuDisplayInterface51;
		friend class CIpuDisplayFifo51;
	};

// End of File

#endif
