
#include "Intern.hpp"

#include "Ipu51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IpuDmfc51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Image Processing Unit Multi Fifo Control Module
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CIpuDisplayFifo51::CIpuDisplayFifo51(CIpu51 *pIpu)
{
	m_pBase  = PVDWORD(ADDR_IPUEX) + CIpu51::regDmfc;

	m_pIpu   = pIpu;

	m_uUnit  = CIpu51::modDmfc;
	}

// Interface

void CIpuDisplayFifo51::Enable(bool fEnable)
{
	m_pIpu->EnableModule(m_uUnit, fEnable);
	}

bool CIpuDisplayFifo51::SetConfig(UINT uChan, UINT uWidth, UINT uHeight)
{
	CDmfcConfig Config;

	Config.m_uFrameH    = uHeight;
	
	Config.m_uFrameW    = uWidth;
	
	Config.m_uBurstSize = burst4;
	
	Config.m_uFifoSize  = FindSize(uWidth, FindScale(uChan));
	
	Config.m_uPixels    = 0;

	Config.m_uStartAddr = (uChan == CIpu51::chanDcSyncAsync ? 2 : 0);

	Config.m_fWmEnable  = (uChan == CIpu51::chanDpPrimaryMain || uChan == CIpu51::chanDpPrimaryAux);

	Config.m_uWmClrLev  = 7;

	Config.m_uWmSetLev  = 5;

	return SetConfig(uChan, Config);
	}

bool CIpuDisplayFifo51::SetConfig(UINT uChan, CDmfcConfig const &Config)
{
	switch( uChan ) {

		case 40: 
			return SetRead(Config, Reg(RdChan), Reg(Gen2));

		case 23:
			return SetWrite(Config, Reg(DpChan), Reg(DpChanDef), 0);

		case 24:
			return SetWrite(Config, Reg(DpChan), Reg(DpChanDef), 16);

		case 27:
			return SetWrite(Config, Reg(DpChan), Reg(DpChanDef), 8);

		case 28:
			return SetWrite(Config, Reg(WrChan), Reg(WrChanDef), 0);

		case 29:
			return SetWrite(Config, Reg(DpChan), Reg(DpChanDef), 24);

		case 41:
			return SetWrite(Config, Reg(WrChan), Reg(WrChanDef), 8);

		case 42:
			return SetWrite(Config, Reg(WrChan), Reg(WrChanDef), 16);

		case 43:
			return SetWrite(Config, Reg(WrChan), Reg(WrChanDef), 24);

		case 44:
			Reg(Gen1) &= 0xFFFF0003; 

			Reg(Gen1) = ((Config.m_uWmClrLev  & 0x7) << 13) |
				    ((Config.m_uWmSetLev  & 0x7) << 10) |
				    ((Config.m_fWmEnable  & 0x1) <<  9) |
				    ((Config.m_uBurstSize & 0x3) <<  5) ;

			return true;
		}

	return false;
	}

bool CIpuDisplayFifo51::SetConfigAlt(UINT uChan, CDmfcConfig const &Config)
{
	switch( uChan ) {

		case 23:
			return SetWrite(Config, Reg(DpChanAlt), Reg(DpChanDefAlt), 8);

		case 24:
			return SetWrite(Config, Reg(DpChanAlt), Reg(DpChanDefAlt), 16);

		case 29:
			return SetWrite(Config, Reg(DpChanAlt), Reg(DpChanDefAlt), 24);

		case 41:
			return SetWrite(Config, Reg(WrChanAlt), Reg(WrChanDefAlt), 8);
		}

	return false;
	}

bool CIpuDisplayFifo51::GetFifoFull(UINT uChan)
{
	switch( uChan ) {

		case 40: return Reg(Stat) & Bit(0);
		case 28: return Reg(Stat) & Bit(1);
		case 41: return Reg(Stat) & Bit(2);
		case 42: return Reg(Stat) & Bit(3);
		case 43: return Reg(Stat) & Bit(4);
		case 23: return Reg(Stat) & Bit(5);
		case 27: return Reg(Stat) & Bit(6);
		case 24: return Reg(Stat) & Bit(7);
		case 29: return Reg(Stat) & Bit(8);
		case 44: return Reg(Stat) & Bit(9);
		}

	return false;
	}

bool CIpuDisplayFifo51::GetFifoEmpty(UINT uChan)
{
	switch( uChan ) {

		case 40: return Reg(Stat) & Bit(12);
		case 28: return Reg(Stat) & Bit(13);
		case 41: return Reg(Stat) & Bit(14);
		case 42: return Reg(Stat) & Bit(15);
		case 43: return Reg(Stat) & Bit(16);
		case 23: return Reg(Stat) & Bit(17);
		case 27: return Reg(Stat) & Bit(18);
		case 24: return Reg(Stat) & Bit(19);
		case 29: return Reg(Stat) & Bit(20);
		case 44: return Reg(Stat) & Bit(21);
		}

	return true;
	}

// Implementaton

bool CIpuDisplayFifo51::SetRead(CDmfcConfig const &Config, DWORD volatile &Reg1, DWORD volatile &Reg2)
{
	Reg1 = ((Config.m_uPixels    & 0x3) << 24) |
	       ((Config.m_uWmClrLev  & 0x7) << 21) |
	       ((Config.m_uWmSetLev  & 0x7) << 18) |
	       ((Config.m_fWmEnable  & 0x1) << 17) |
	       ((Config.m_uBurstSize & 0x3) <<  6) ;

	Reg2 = ((Config.m_uFrameH & 0x1FFF) << 16) |
	       ((Config.m_uFrameW & 0x1FFF) <<  0) ;

	return true;
	}

bool CIpuDisplayFifo51::SetWrite(CDmfcConfig const &Config, DWORD volatile &Reg1, DWORD volatile &Reg2, UINT uShift)
{
	DWORD Mask = 0xFF;

	DWORD Data1 = ((Config.m_uBurstSize & 0x3) << 6) | 
		      ((Config.m_uFifoSize  & 0x7) << 3) | 
		      ((Config.m_uStartAddr & 0x7) << 0) ;

	DWORD Data2 = ((Config.m_uWmClrLev  & 0x7) << 5) |
		      ((Config.m_uWmSetLev  & 0x7) << 2) |
		      ((Config.m_fWmEnable  & 0x1) << 1) ;
 
	Reg1 &= ~(Mask  << uShift);
	
	Reg1 |=  (Data1 << uShift);

	Reg2 &= ~(Mask  << uShift);

	Reg2 |=  (Data2 << uShift);

	return true;
	}

UINT CIpuDisplayFifo51::FindScale(UINT uChan) const
{
	switch( uChan ) {

		case CIpu51::chanDcSyncAsync:
		case CIpu51::chanDpPrimaryAux:

			return 2;
		
		case CIpu51::chanDpPrimaryMain:

			return 3;
		}

	return 1;
	}

UINT CIpuDisplayFifo51::FindSize(UINT uWidth, UINT uScale) const
{
	UINT uRequired = uWidth * 32 * uScale;

	for( int iSize = 7, uSize = 8 * 128; iSize > 2; iSize --, uSize <<= 1 ) {

		if( uRequired < uSize ) {

			return iSize;
			}
		}

	return uWidth > 1024 ? 1 : 2;
	}

// End of File
