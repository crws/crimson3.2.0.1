
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IpuDi51_HPP
	
#define	INCLUDE_IpuDi51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpu51;

//////////////////////////////////////////////////////////////////////////
//
// Sync Pulse Configuraton
//

struct CSyncConfig
{
	UINT  m_uRun;
	UINT  m_uRunTrig;
	UINT  m_uOffset;
	UINT  m_uOffsetTrig;
	bool  m_uAutoReload;
	UINT  m_uClearSel;
	UINT  m_uToggleTrigSel;
	UINT  m_uPolarityEnable;
	UINT  m_uPolarityClearSel;
	UINT  m_uPosUp;
	UINT  m_uPosDn;
	UINT  m_uStepRepeat;
	};

//////////////////////////////////////////////////////////////////////////
//
// Waveform Generator Configuraton
//

struct CWaveformConfig
{
	UINT  m_uAccessSize;
	UINT  m_uCompSize;
	UINT  m_uChipSel;
	UINT  m_uPin17;
	UINT  m_uPin16;
	UINT  m_uPin15;
	UINT  m_uPin14;
	UINT  m_uPin13;
	UINT  m_uPin12;
	UINT  m_uPin11;
	};

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Image Processing Unit Diplay Interface Module
//

class CIpuDisplayInterface51
{	
	public:
		// Constructor
		CIpuDisplayInterface51(UINT iIndex, CIpu51 *pIpu, UINT uClock);

		// Interface
		void Enable(bool fEnable);
		void SetBaseClock(UINT uFreq);
		void SetPolarity(bool fClock, bool fChip0, bool fChip1, BYTE bData);
		void SetActiveWindowH(UINT uStart, UINT uEnd, UINT uCounter, UINT uTrigger);
		void SetActiveWindowV(UINT uStart, UINT uEnd, UINT uCounter);
		void SetScreenHeight(UINT uHeight);
		void SetSyncConfig(UINT uSel, CSyncConfig const &Config);
		void SetWaveformConfig(UINT uPtr, CWaveformConfig const &Config);
		void SetDataWaveSet(UINT uPtr, UINT uSet, UINT uUp, UINT uDn);
		void SetLineCounter(UINT uSel);
		void SetSyncStart(UINT uStart);

	protected:
		// Regisgters
		enum
		{
			regGeneral	= 0x0000 / sizeof(DWORD),
			regBsClkGen0	= 0x0004 / sizeof(DWORD),
			regBsClkGen1	= 0x0008 / sizeof(DWORD),
			regSwGen0x	= 0x000C / sizeof(DWORD),
			regSwGen1x	= 0x0030 / sizeof(DWORD),
			regSyncAsGen	= 0x0054 / sizeof(DWORD),
			regDwGenx	= 0x0058 / sizeof(DWORD),
			regDwSet0x	= 0x0088 / sizeof(DWORD),
			regDwSet1x	= 0x00B8 / sizeof(DWORD),
			regDwSet2x	= 0x00E8 / sizeof(DWORD),
			regDwSet3x	= 0x0118 / sizeof(DWORD),
			regStepRepx	= 0x0148 / sizeof(DWORD),
			regSerConf	= 0x015C / sizeof(DWORD),
			regControl	= 0x0160 / sizeof(DWORD),
			regPolarity	= 0x0164 / sizeof(DWORD),
			regWindow0	= 0x0168 / sizeof(DWORD),
			regWindow1	= 0x016C / sizeof(DWORD),
			regScrConf	= 0x0170 / sizeof(DWORD),
			regStat		= 0x0174 / sizeof(DWORD),
			};

		// Data
		PVDWORD   m_pBase;
		CIpu51  * m_pIpu;
		UINT      m_uUnit;
		UINT	  m_uClock;
	};

// End of File

#endif
