
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Tzic51_HPP
	
#define	INCLUDE_Tzic51_HPP

//////////////////////////////////////////////////////////////////////////
//
// iMX51 TrustZone Interrupt Controller
//

class CTzic51 : public IPic, public IDiagProvider
{
	public:
		// Constructor
		CTzic51(void);

		// Attributes
		UINT GetLineCount(void);
		UINT GetPriorityCount(void);
		UINT GetPriorityMask(void);
		UINT GetLinePriority(UINT uLine);
		bool IsLineEnabled(UINT uLine);
		bool IsLinePending(UINT uLine);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDeviceDriver
		void DevInit(void);
		void DevTerm(void);

		// IPic
		void OnInterrupt(UINT uSource);
		bool SetLinePriority(UINT uLine, UINT uPriority);
		bool SetLineHandler(UINT uLine, IEventSink *pSink, UINT uParam);
		bool EnableLine(UINT uLine, bool fEnable);
		bool EnableLineViaIrql(UINT uLine, UINT &uSave, bool fEnable);
		void DebugRegister(void);
		void DebugRevoke(void);

		// IDiagProvider
		UINT RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Operations
		bool SetPriorityMask(UINT uPriority);

	protected:
		// Registers
		enum
		{
			regControl   = 0x0000 / sizeof(DWORD),
			regType      = 0x0004 / sizeof(DWORD),
			regPriMask   = 0x000C / sizeof(DWORD),
			regSync      = 0x0010 / sizeof(DWORD),
			regHoldoff   = 0x0014 / sizeof(DWORD),
			regSecure    = 0x0080 / sizeof(DWORD),
			regEnableSet = 0x0100 / sizeof(DWORD),
			regEnableClr = 0x0180 / sizeof(DWORD),
			regSourceSet = 0x0200 / sizeof(DWORD),
			regSourceClr = 0x0280 / sizeof(DWORD),
			regPriority  = 0x0400 / sizeof(DWORD),
			regPending   = 0x0D00 / sizeof(DWORD),
			regPriPend   = 0x0D80 / sizeof(DWORD),
			regWakeUp    = 0x0E00 / sizeof(DWORD),
			regTrigger   = 0x0F00 / sizeof(DWORD),
			};

		// Sink Record
		struct CSink
		{
			IEventSink * m_pSink;
			UINT	     m_uParam;
			UINT	     m_uCount;
			BOOL	     m_fUsed;
			};

		// Diagnostic Data
		ULONG	       m_uRefs;
		IDiagManager * m_pDiag;
		UINT           m_uProv;
		UINT	       m_uInit;

		// Data Members
		PVDWORD	     m_pBase;
		UINT	     m_uSets;
		UINT	     m_uUsed;
		UINT         m_uLines;
		CSink      * m_pSink;

		// Implementation
		bool DisableAll(void);
		bool Dispatch(void);
		bool EnableLine(UINT uLine);
		bool DisableLine(UINT uLine);
	};

// End of File

#endif
