
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_iMX51_UsbBase51_HPP
	
#define	INCLUDE_iMX51_UsbBase51_HPP

//////////////////////////////////////////////////////////////////////////
//
// iMX51 USB Controller Base Class
//

class CUsbBase51 : public IEventSink
{
	public:
		// Constructor
		CUsbBase51(UINT iIndex);

		// Destructor
		virtual ~CUsbBase51(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// Operations
		void EnableEvents(void);

	protected:
		// Identification Registers
		enum 
		{
			regId		     = 0x0000 / sizeof(DWORD),
			regHwGeneral	     = 0x0004 / sizeof(DWORD),
			regHwHost	     = 0x0008 / sizeof(DWORD),
			regHwDevice	     = 0x000C / sizeof(DWORD),
			regHwTxBuf	     = 0x0010 / sizeof(DWORD),
			regHwRxBuf	     = 0x0014 / sizeof(DWORD),
			regHwTtTxBuf	     = 0x0018 / sizeof(DWORD),
			regHwTtRxBuf	     = 0x001C / sizeof(DWORD),
			regGpTimer0Load	     = 0x0080 / sizeof(DWORD),
			regGpTimer0Ctrl	     = 0x0084 / sizeof(DWORD),
			regGpTimer1Load	     = 0x0088 / sizeof(DWORD),
			regGpTimer1Ctrl	     = 0x008C / sizeof(DWORD),
			};

		// Capability Registers
		enum
		{
			regCapLength	     = 0x0100 / sizeof(DWORD),
			regHcsParams	     = 0x0104 / sizeof(DWORD),
			regHccParams	     = 0x0108 / sizeof(DWORD),
			regDciVersion	     = 0x0120 / sizeof(DWORD),
			regDccParams	     = 0x0124 / sizeof(DWORD),
			};

		// Operational Registers
		enum
		{
			regUsbCmd	     = 0x0140 / sizeof(DWORD),
			regUsbSts	     = 0x0144 / sizeof(DWORD),
			regUsbIntr	     = 0x0148 / sizeof(DWORD),
			regFrIndex	     = 0x014C / sizeof(DWORD),
			regPeriodicListBase  = 0x0154 / sizeof(DWORD),
			regAsyncListAddr     = 0x0158 / sizeof(DWORD),
			regAsyncTtSts	     = 0x015C / sizeof(DWORD),
			regBurstSize	     = 0x0160 / sizeof(DWORD),
			regTxFillTuning	     = 0x0164 / sizeof(DWORD),
			regTxTtFillTuning    = 0x0168 / sizeof(DWORD),
			regIcUsb	     = 0x016C / sizeof(DWORD),
			regUlpiView	     = 0x0170 / sizeof(DWORD),
			regCfgFlag	     = 0x0180 / sizeof(DWORD),
			regPortSc1	     = 0x0184 / sizeof(DWORD),
			regPortSc2	     = 0x0188 / sizeof(DWORD),
			regPortSc3	     = 0x018C / sizeof(DWORD),
			regPortSc4	     = 0x0190 / sizeof(DWORD),
			regPortSc5	     = 0x0194 / sizeof(DWORD),
			regPortSc6	     = 0x0198 / sizeof(DWORD),
			regPortSc7	     = 0x019C / sizeof(DWORD),
			regPortSc8	     = 0x01A0 / sizeof(DWORD),
			regOtgSC	     = 0x01A4 / sizeof(DWORD),
			regUsbMode	     = 0x01A8 / sizeof(DWORD),
			};
		
		// Device Operational Registers
		enum
		{
			regDeviceAddr	     = 0x0154 / sizeof(DWORD),
			regEndptListAddr     = 0x0158 / sizeof(DWORD),
			regEndptNak	     = 0x0178 / sizeof(DWORD),
			regEndptNakEn	     = 0x017C / sizeof(DWORD),
			regEndptSetupStat    = 0x01AC / sizeof(DWORD),
			regEndptPrime	     = 0x01B0 / sizeof(DWORD),
			regEndptFlush	     = 0x01B4 / sizeof(DWORD),
			regEndptStat	     = 0x01B8 / sizeof(DWORD),
			regEndptComplete     = 0x01BC / sizeof(DWORD),
			regEndptCtrl0	     = 0x01C0 / sizeof(DWORD),
			regEndptCtrl1	     = 0x01C4 / sizeof(DWORD),
			regEndptCtrl2	     = 0x01C8 / sizeof(DWORD),
			regEndptCtrl3	     = 0x01CC / sizeof(DWORD),
			regEndptCtrl4	     = 0x01D0 / sizeof(DWORD),
			regEndptCtrl5	     = 0x01D4 / sizeof(DWORD),
			regEndptCtrl6	     = 0x01D8 / sizeof(DWORD),
			regEndptCtrl7	     = 0x01DC / sizeof(DWORD),
			regEndptCtrl8	     = 0x01E0 / sizeof(DWORD),
			regEndptCtrl9	     = 0x01E4 / sizeof(DWORD),
			regEndptCtrl10	     = 0x01E8 / sizeof(DWORD),
			regEndptCtrl11	     = 0x01EC / sizeof(DWORD),
			regEndptCtrl12	     = 0x01F0 / sizeof(DWORD),
			regEndptCtrl13	     = 0x01F4 / sizeof(DWORD),
			regEndptCtrl14	     = 0x01F8 / sizeof(DWORD),
			regEndptCtrl15	     = 0x01FC / sizeof(DWORD),
			};

		// Integration Registers
		enum
		{
			regCtrl		     = 0x800 / sizeof(DWORD),
			regPhyCtrl0	     = 0x808 / sizeof(DWORD),
			regPhyCtrl1	     = 0x80C / sizeof(DWORD),
			regCtrl1	     = 0x810 / sizeof(DWORD),
			regHost2Ctrl	     = 0x814 / sizeof(DWORD),
			regHost3Ctrl	     = 0x818 / sizeof(DWORD),
			};

		// Modes
		enum
		{
			modeIdle             = 0,	
			modeDevice	     = 2,
			modeHost	     = 3,
			};

		// Interrupts
		enum
		{
			intTransfer	     = Bit( 0),
			intError	     = Bit( 1),
			intPortChange	     = Bit( 2),
			intRollover 	     = Bit( 3),
			intSysError          = Bit( 4),
			intAsyncAdvance	     = Bit( 5),
			intReset	     = Bit( 6),
			intSof		     = Bit( 7),
			intSuspend	     = Bit( 8),
			intPhy   	     = Bit(10),
			intNak		     = Bit(16),
			intTimer0   	     = Bit(24),
			intTimer1   	     = Bit(25),
			};

		// Port Status and Control
		enum
		{
			portConnect	     = Bit( 0),
			portResume	     = Bit( 6),
			portSuspend	     = Bit( 7),
			portReset	     = Bit( 8),
			portHighSpeedDisable = Bit(24),
			portHighSpeedStatus  = Bit(27),
			};

		// Command Register Bits
		enum 
		{
			cmdRun		     = Bit( 0),
			cmdReset	     = Bit( 1),
			cmdFrameListSize     = Bit( 2),
			cmdPeriodicEnable    = Bit( 4),
			cmdAsyncEnable	     = Bit( 5),
			cmdDoorbell	     = Bit( 6),
			cmdAsyncParkMode     = Bit( 8),
			cmdAsyncParkEnable   = Bit(11),
			cmdAddTripWire	     = Bit(12),
			cmdSetupTripWire     = Bit(13),
			cmdIntThreshold	     = Bit(16),
			};
	
		// Data Members
		ULONG   m_uRefs;
		PVDWORD m_pBase;
		UINT	m_uBase;
		UINT	m_uLine;

		// Operations
		void SetMode(UINT uMode);
		void SetReset(void);
		void SetRun(void);
		void SetStop(void);
		void InitHardware(void);
		void SetIntLatency(UINT uFrames);
		void SetTimer(UINT i, DWORD dwPeriod);
		UINT GetTimer(UINT i);
		
		// Memory Helpers
		PVOID AllocNonCached(UINT uSize);
		PVOID AllocNonCached(UINT uSize, UINT uAlign);
		void  FreeNonCached(PVOID pMem);

		// Debug
		void Dump(void);
	};

// End of File

#endif
