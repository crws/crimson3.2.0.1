
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Ecspi51_HPP
	
#define	INCLUDE_Ecspi51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIoMux51;
class CCcm51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Enhanced Configurable Serial Peripheral Interface
//

class CEcspi51 : public ISpi, public IEventSink
{
	public:
		// Constructor
		CEcspi51(CIoMux51 *pMux, CCcm51 *pCcm);

		// Destructor
		~CEcspi51(void);
		
		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ISpi
		BOOL METHOD Init(UINT uChan, CSpiConfig const &Config);
		BOOL METHOD Send(UINT uChan, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData);
		BOOL METHOD Recv(UINT uChan, PCBYTE pHead, UINT uHead, PBYTE  pData, UINT uData);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Registers
		enum
		{
			regRxData	= 0x0000 / sizeof(DWORD),
			regTxData	= 0x0004 / sizeof(DWORD),
			regControl	= 0x0008 / sizeof(DWORD),
			regConfig	= 0x000C / sizeof(DWORD),
			regInt		= 0x0010 / sizeof(DWORD),
			regDma		= 0x0014 / sizeof(DWORD),
			regStat		= 0x0018 / sizeof(DWORD),
			regPeriod	= 0x001C / sizeof(DWORD),
			regTest		= 0x0020 / sizeof(DWORD),
			regMsgData	= 0x0040 / sizeof(DWORD),
			};

		// Interrupts
		enum
		{
			intTxEmpty	= Bit(0),
			intTxData 	= Bit(1),
			intTxFull 	= Bit(2),
			intRxReady	= Bit(3),
			intRxData 	= Bit(4),
			intRxFull 	= Bit(5),
			intRxOverflow	= Bit(6),
			intTransfer	= Bit(7),
			};
				
		// Configuration
		struct CConfig
		{
			UINT	m_uPre;
			UINT	m_uPod;
			};

		// Request
		struct CRequest
		{
			UINT	m_uChan;
			PCBYTE	m_pHead;
			UINT	m_uHead;
			PBYTE	m_pData;
			UINT	m_uData;
			bool	m_fSend;
			bool    m_fPoll;
			bool	m_fDone;
			};

		// Data Members
		PVDWORD		     m_pBase;
		IMutex		   * m_pMutex;
		IEvent             * m_pEvent;
		CConfig		     m_Config[4];
		CRequest           * m_pList[16];
		CRequest * volatile  m_pReq;
		UINT       volatile  m_uHead;
		UINT	   volatile  m_uTail;
		UINT		     m_uLine;
		UINT		     m_uClock;
		ULONG		     m_uRefs;

		// Erratum
		UINT                 m_uMux[2];
		CIoMux51           * m_pIoMux;
		UINT	             m_uMuxSel;
		UINT	             m_uMuxGpio;

		// Implementation
		void InitController(void);
		void InitEvents(void);
		bool Request(CRequest *pReq);
		void StartComms(void);
		void InitWait(CRequest *pReq);
		void TermWait(CRequest *pReq);
		void WaitDone(CRequest *pReq);
		void CommsDone(CRequest *pReq);
		void WriteFifo(PCBYTE pData1, UINT uData1, PCBYTE pData2, UINT uData2);
		void ReadFifo(PBYTE pData, UINT uData, UINT uSkip);
		void FlushFifo(void);
		bool FindDivider(UINT uChan, UINT uFreq);
		void EnableSelect(UINT uChan, bool fEnable);
	};

// End of File

#endif
