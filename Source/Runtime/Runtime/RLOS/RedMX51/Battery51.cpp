
#include "Intern.hpp"

#include "Battery51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Pmui51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Battery Wrapper Object
//

// Instantiator

IDevice * Create_BatteryMonitor51(CPmui51 *pPmui)
{
	IDevice *pDevice = New CBattery51(pPmui);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CBattery51::CBattery51(CPmui51 *pPmui)
{
	StdSetRef();

	m_pPmui  = pPmui;

	m_uBatt  = 0;

	m_pTimer = CreateTimer();
	}

// Destructor

CBattery51::~CBattery51(void)
{
	m_pTimer->Release();
	}

// IUnknown

HRESULT CBattery51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IBatteryMonitor);

	StdQueryInterface(IBatteryMonitor);

	return E_NOINTERFACE;
	}

ULONG CBattery51::AddRef(void)
{
	StdAddRef();
	}

ULONG CBattery51::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CBattery51::Open(void)
{
	m_pTimer->SetPeriod(constPeriod);

	m_pTimer->SetHook(this, 0);

	m_pTimer->Enable(true);

	m_pPmui->AttachTouch(this, 1);

	m_pPmui->ReqVBat();

	return TRUE;
	}

// IBatteryMonitor

bool METHOD CBattery51::IsBatteryGood(void)
{
	return m_uBatt > constThreshold;
	}

bool METHOD CBattery51::IsBatteryBad(void)
{
	return m_uBatt <= constThreshold;
	}

// IEventSink

void CBattery51::OnEvent(UINT uLine, UINT uParam)
{
	if( uParam == 0 ) {

		m_pPmui->ReqVBat();
		}
	else {
		m_uBatt = m_pPmui->GetVBat();
		}
	}

// End of File
