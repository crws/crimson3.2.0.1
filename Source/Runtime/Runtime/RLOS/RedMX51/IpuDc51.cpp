
#include "Intern.hpp"

#include "Ipu51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IpuDc51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Image Processor Unit Display Controller Module
//

// Register Access

#define Reg(x)		(m_pBase[reg##x])

#define RegN(x, n)	(m_pBase[reg##x + n])

// Constructor

CIpuDisplayControl51::CIpuDisplayControl51(CIpu51 *pIpu)
{
	m_pBase  = PVDWORD(ADDR_IPUEX) + CIpu51::regDc;

	m_uUnit  = CIpu51::modDc;

	m_pCode  = PVDWORD(ADDR_IPUEX) + CIpu51::regDctpl;

	m_pIpu   = pIpu;

	m_dwUsed = 0;

	Init();
	}

// Interface

void CIpuDisplayControl51::Enable(bool fEnable)
{
	m_pIpu->EnableModule(m_uUnit, fEnable);
	}

bool CIpuDisplayControl51::SetChanMode(UINT uChan, UINT uMode)
{
	DWORD Mask = ~(0x07 << 5);

	DWORD Data = (uMode & 0x7) << 5;

	switch( uChan ) {

		case 1:
			Reg(WrChConf1) &= Mask;

			Reg(WrChConf1) |= Data;

			return true;

		case 2:
			Reg(WrChConf2) &= Mask;

			Reg(WrChConf2) |= Data;

			return true;

		case 5:
			Reg(WrChConf5) &= Mask;

			Reg(WrChConf5) |= Data;

			return true;

		case 6:
			Reg(WrChConf6) &= Mask;

			Reg(WrChConf6) |= Data;

			return true;
		}
	
	return false;
	}

bool CIpuDisplayControl51::SetWriteChanConfig(UINT uChan, CDcWriteChanConfig const &Config)
{
	DWORD dwCfg = (Config.m_uStartTime << 16) |
		      (Config.m_uFieldMode <<  9) |
		      (Config.m_uEventMask <<  8) |
		      (Config.m_uMode      <<  5) |
		      (Config.m_uDisplay   <<  3) |
		      (Config.m_uDi        <<  2) |
		      ((Config.m_uWordSize >>  3) - 1);

	switch( uChan ) {

		case 1:
			Reg(WrChConf1) = dwCfg;

			Reg(WrChAddr1) = Config.m_uStartAddr;

			return true;

		case 2:
			Reg(WrChConf2) = dwCfg & ~Bit(9);

			Reg(WrChAddr2) = Config.m_uStartAddr;

			return true;

		case 5:
			Reg(WrChConf5) = dwCfg;

			Reg(WrChAddr5) = Config.m_uStartAddr;

			return true;

		case 6:
			Reg(WrChConf6) = dwCfg & ~Bit(9);

			Reg(WrChAddr6) = Config.m_uStartAddr;

			return true;

		case 8:
			Reg(WrCh8Conf1) = (dwCfg & 0x1B) | (Config.m_uEventMask << 2);

			Reg(WrCh8Conf2) = Config.m_uStartAddr;

			return true;

		case 9:
			Reg(WrCh9Conf1) = (dwCfg & 0x1B) | (Config.m_uEventMask << 2);

			Reg(WrCh9Conf2) = Config.m_uStartAddr;

			return true;
		}
	
	return false;
	}

bool CIpuDisplayControl51::SetDispConfig(UINT uDisp, CDcDisplayConfig const &Config)
{
	if( uDisp < 4 ) {

		RegN(DispConf1x, uDisp) = (Config.m_uRdValPtr   << 7) |
					  (Config.m_uMcuAccMask << 6) |
					  (Config.m_uAddrBe     << 4) |
					  (Config.m_uAddrInc    << 2) |
					  (Config.m_uType       << 0) ;

		RegN(DispConf2x, uDisp) = Config.m_uStride;

		return true;
		}

	return false;
	}

bool CIpuDisplayControl51::SetUserEvent(UINT uEvent, CDcUserEventConfig const &Config)
{
	if( uEvent < 4 ) {

		RegN(Ugde0x, uEvent) = (Config.m_uTrig              << 27) |
				       (Config.m_fAutoRestart       << 26) |
				       (Config.m_fOddMode           << 25) |
				       (Config.m_uCodeOddStartAddr  << 16) |
				       (Config.m_uCodeEvenStartAddr <<  8) |
				       (Config.m_uPriority          <<  3) |
				       (Config.m_uDcChan            <<  0) ;


		RegN(Ugde1x, uEvent) = Config.m_uCountCompare;

		RegN(Ugde2x, uEvent) = Config.m_uCountOffset;

		RegN(Ugde3x, uEvent) = Config.m_uEventCount;
		
		return true;
		}
	
	return false;
	}

bool CIpuDisplayControl51::DisableUserEvent(UINT uEvent)
{
	if( uEvent < 4 ) {

		RegN(Ugde0x, uEvent) = 0;

		return true;
		}
	
	return false;
	}

bool CIpuDisplayControl51::SetMicrocode(UINT uIdx, CDcMicrocodeConfig const &Config)
{
	if( uIdx < 256 ) {

		DWORD volatile &dwLo = m_pCode[uIdx * 2 + 0];

		DWORD volatile &dwHi = m_pCode[uIdx * 2 + 1];

		switch( Config.m_uOpcode ) {

			case opHlg:

				dwHi = (Config.m_fStop << 9) | (0x000 << 5) | ((Config.m_uOperand >> 27) & 0x1F);

				dwLo = (Config.m_uOperand << 5);

				return true;
				
			case opWrg:

				dwHi = (Config.m_fStop << 9) | (0x001 << 7) | ((Config.m_uOperand >> 17) & 0x7F);

				dwLo = (Config.m_uOperand   << 15) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opHloa:

				dwHi = (Config.m_fStop << 9) | (0x00A << 5) | ((Config.m_uOperand >> 12) & 0x1F);

				dwLo = (Config.m_uMapping << 15);

				return true;

			case opWroa:

				dwHi = (Config.m_fStop << 9) | (0x00E << 5) | ((Config.m_uOperand >> 12) & 0x1F);

				dwLo = (Config.m_uOperand   << 20) | 
				       (Config.m_uMapping   << 15) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opHlod:

				dwHi = (Config.m_fStop << 9) | (0x010 << 4) | ((Config.m_uOperand >> 12) & 0x0F);

				dwLo = (Config.m_uMapping << 15);

				return true;

			case opWrod:

				dwHi = (Config.m_fStop << 9) | (0x018 << 4) | ((Config.m_uOperand >> 12) & 0x0F);

				dwLo = (Config.m_uOperand   << 20) | 
				       (Config.m_uMapping   << 15) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opHloar:

				dwHi = (Config.m_fStop << 9) | (0x047 << 2);

				dwLo = (Config.m_uMapping << 15);

				return true;

			case opWroar:

				dwHi = (Config.m_fStop << 9) | (0x067 << 2);

				dwLo = (Config.m_uMapping   << 15) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opHlodr:

				dwHi = (Config.m_fStop << 9) | (0x046 << 2);

				dwLo = (Config.m_uMapping << 15);

				return true;

			case opWrodr:

				dwHi = (Config.m_fStop << 9) | (0x066 << 2) | (Config.m_uOperand >> 2);

				dwLo = (Config.m_uOperand   << 30) | 
				       (Config.m_uMapping   << 15) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opWrbc:

				dwHi = (Config.m_fStop << 9) | (0x19B << 0);

				dwLo = (Config.m_uMapping   << 15) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opWclk:

				dwHi = (Config.m_fStop << 9) | (0x0C9 << 1) | (Config.m_uOperand >> 12);

				dwLo = (Config.m_uOperand << 20);

				return true;

			case opWstsi1:

				dwHi = (0x089 << 1) | (Config.m_uOperand >> 12);

				dwLo = (Config.m_uOperand   << 20) | 
				       (Config.m_uMapping   << 15) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opWstsi2:

				dwHi = (0x08A << 1) | (Config.m_uOperand >> 12);

				dwLo = (Config.m_uOperand   << 20) | 
				       (Config.m_uMapping   << 15) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opWstsi3:

				dwHi = (Config.m_fStop << 9) | (0x08B << 1) | (Config.m_uOperand >> 12);

				dwLo = (Config.m_uOperand   << 20) | 
				       (Config.m_uMapping   << 15) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opRd:

				dwHi = (Config.m_fStop << 9) | (0x088 << 1) | (Config.m_uOperand >> 12);

				dwLo = (Config.m_uOperand   << 20) | 
				       (Config.m_uMapping   << 15) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opWack:

				dwHi = (Config.m_fStop << 9) | (0x11A << 0);

				dwLo = (Config.m_uOperand   << 19) | 
				       (Config.m_uWaveform  << 11) | 
				       (Config.m_uGlueLogic <<  4) | 
				       (Config.m_uSync      <<  0);

				return true;

			case opMsk:

				dwHi = (Config.m_fStop << 9) | (0x190 << 0);

				dwLo = (Config.m_uOperand << 15);

				return true;

			case opHma:

				dwHi = (Config.m_fStop << 9) | (0x040 << 0);

				dwLo = (Config.m_uOperand << 5);

				return true;

			case opHma1:

				dwHi = (Config.m_fStop << 9) | (0x020 << 0);

				dwLo = (Config.m_uOperand << 5);

				return true;

			case opBma:

				dwHi = (Config.m_fStop << 9) | (0x060 << 0);

				dwLo = (Config.m_uOperand << 5) | (Config.m_uSync << 0);

				return true;
			}
		}

	return false;
	}

bool CIpuDisplayControl51::SetMicrocodeEvent(UINT uChan, UINT uEvent, UINT uPriority, UINT uAddr)
{
	const DWORD dwReg[10] = { 
		
		regRl0Ch0, regRl0Ch1, regRl0Ch2, 0, 0, regRl0Ch5, regRl0Ch6, 0, regRl1Ch8, regRl1Ch9,
		};

	if( uChan < elements(dwReg) ) {
		
		if( dwReg[uChan] ) {

			DWORD volatile &Reg = m_pBase[ dwReg[uChan] + uEvent / 2 ];
		
			DWORD dwMask = 0x0000FF0F;

			DWORD dwData = (uAddr << 8 ) | uPriority;
			
			if( !(uEvent % 2) && uEvent != dcNewData ) {
				
				dwMask <<= 16;

				dwData <<= 16;
				}
				
			Reg &= ~dwMask;

			Reg |=  dwData;

			return true;
			}
		}

	return false;
	}

bool CIpuDisplayControl51::SetMappingPointer(UINT uPtr, CDcBusMappingData const &Config)
{
	if( uPtr < 30 ) {

		DWORD dwMapping = 0;

		for( UINT n = 0; n < 3; n ++ ) {

			UINT i = FindFreeMask();

			if( i != NOTHING ) {
			
				dwMapping |= ((i & 0x1F) << ((2-n) * 5));

				DWORD volatile &Reg = m_pBase[ regMapConfx + i / 2 + 15];

				DWORD dwData = (Config.m_uOffset[n] << 8) | Config.m_uMask[n];

				DWORD dwMask = 0x1FFF;

				if( i % 2 ) {

					dwData <<= 16;

					dwMask <<= 16;
					}

				Reg &= ~dwMask;

				Reg |=  dwData;

				continue;
				}

			return false;
			}

		DWORD volatile &Reg = m_pBase[ regMapConfx + uPtr / 2 ];

		DWORD dwMask = 0x7FFF;

		if( uPtr % 2 ) {

			dwMask    <<= 16;

			dwMapping <<= 16;
			}

		Reg &= ~dwMask;

		Reg |=  dwMapping;

		return true;
		}
	
	return false;
	}

// Implementation

void CIpuDisplayControl51::Init(void)
{
	Reg(Gen)	= 0x00000044;

	RegN(Ugde0x, 0)	= 0x00000000;

	RegN(Ugde0x, 1)	= 0x00000000;

	RegN(Ugde0x, 2)	= 0x00000000;

	RegN(Ugde0x, 3)	= 0x00000000;
	}

UINT CIpuDisplayControl51::FindFreeMask(void)
{
	for( UINT i = 0; i < 24; i ++ ) {

		if( !(m_dwUsed & Bit(i)) ) {

			m_dwUsed |= Bit(i);

			return i;
			}
		}

	return NOTHING;
	}

// End of File
