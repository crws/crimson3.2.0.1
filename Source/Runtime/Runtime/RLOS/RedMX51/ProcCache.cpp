
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Cache Management
//

global void ProcSyncCaches(void)
{
	ProcPurgeDataCache();

	ProcInvalidateInstructionCache();
	}

global void ProcInvalidateInstructionCache(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	mov    r1,  #0			\n\t"
		"	mcr    p15, #0, r1, c7, c5, #0	\n\t"
		"	dsb				\n\t"
		"	isb				\n\t"
		:
		:
		: "r1"
		);

	#endif
	}

global void ProcInvalidateDataCache(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r4-r11}			\n\t"
		"	dmb					\n\t"
		"	mrc	p15, #1, r0, c0, c0, #1		\n\t"
		"	ands	r3, r0, #0x7000000		\n\t"
		"	mov	r3, r3, lsr #23			\n\t"
		"	beq	0f				\n\t"
		"	mov	r10, #0				\n\t"
		"1:	add	r2, r10, r10, lsr #1		\n\t"
		"	mov	r1, r0, lsr r2			\n\t"
		"	and	r1, r1, #7			\n\t"
		"	cmp	r1, #2				\n\t"
		"	blt	4f				\n\t"
		"	mcr	p15, #2, r10, c0, c0, #0	\n\t"
		"	isb					\n\t"
		"	mrc	p15, #1, r1, c0, c0, #0		\n\t"
		"	and	r2, r1, #7			\n\t"
		"	add	r2, r2, #4			\n\t"
		"	ldr	r4, =0x3FF			\n\t"
		"	ands	r4, r4, r1, lsr #3		\n\t"
		"	clz	r5, r4				\n\t"
		"	ldr	r7, =0x00007FFF			\n\t"
		"	ands	r7, r7, r1, lsr #13		\n\t"
		"2:	mov	r9, r4				\n\t"
		"3:	orr	r11, r10, r9, lsl r5		\n\t"
		"	orr	r11, r11, r7, lsl r2		\n\t"
		"	mcr	p15, #0, r11, c7, c10, #2	\n\t"
		"	subs	r9, r9, #1			\n\t"
		"	bge	3b				\n\t"
		"	subs	r7, r7, #1			\n\t"
		"	bge	2b				\n\t"
		"4:	add	r10, r10, #2			\n\t"
		"	cmp	r3, r10				\n\t"
		"	bgt	1b				\n\t"
		"0:	dsb					\n\t"
		"	isb					\n\t"
		"	pop	{r4-r11}			\n\t"
		:
		:
		: "r0", "r1", "r2", "r3"
		);

	#endif	
	}

global void ProcPurgeDataCache(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r4-r11}			\n\t"
		"	dmb					\n\t"
		"	mrc	p15, #1, r0, c0, c0, #1		\n\t"
		"	ands	r3, r0, #0x7000000		\n\t"
		"	mov	r3, r3, lsr #23			\n\t"
		"	beq	0f				\n\t"
		"	mov	r10, #0				\n\t"
		"1:	add	r2, r10, r10, lsr #1		\n\t"
		"	mov	r1, r0, lsr r2			\n\t"
		"	and	r1, r1, #7			\n\t"
		"	cmp	r1, #2				\n\t"
		"	blt	4f				\n\t"
		"	mcr	p15, #2, r10, c0, c0, #0	\n\t"
		"	isb					\n\t"
		"	mrc	p15, #1, r1, c0, c0, #0		\n\t"
		"	and	r2, r1, #7			\n\t"
		"	add	r2, r2, #4			\n\t"
		"	ldr	r4, =0x3FF			\n\t"
		"	ands	r4, r4, r1, lsr #3		\n\t"
		"	clz	r5, r4				\n\t"
		"	ldr	r7, =0x00007FFF			\n\t"
		"	ands	r7, r7, r1, lsr #13		\n\t"
		"2:	mov	r9, r4				\n\t"
		"3:	orr	r11, r10, r9, lsl r5		\n\t"
		"	orr	r11, r11, r7, lsl r2		\n\t"
		"	mcr	p15, #0, r11, c7, c14, #2	\n\t"
		"	subs	r9, r9, #1			\n\t"
		"	bge	3b				\n\t"
		"	subs	r7, r7, #1			\n\t"
		"	bge	2b				\n\t"
		"4:	add	r10, r10, #2			\n\t"
		"	cmp	r3, r10				\n\t"
		"	bgt	1b				\n\t"
		"0:	dsb					\n\t"
		"	isb					\n\t"
		"	pop	{r4-r11}			\n\t"
		:
		:
		: "r0", "r1", "r2", "r3"
		);

	#endif	
	}

global void ProcPurgeDataCache(UINT uAddr, UINT uSize)
{
	#if 0

	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r14}				\n\t"
		"	add	r14, r0, r1               	\n\t"
		"	dmb					\n\t"
		"	mrc	p15, #0, r2, c0, c0, #1   	\n\t"
		"	ubfx	r2, r2, #16, #4           	\n\t"
		"	mov	r3, #2				\n\t"
		"	add	r3, r3, r2			\n\t"
		"	mov	r2, #1				\n\t"
		"	lsl	r2, r2, r3                	\n\t"
		"	sub	r3, r2, #1                	\n\t"
		"	bic	r0, r0, r3                	\n\t"
		"	tst	r3, r14				\n\t"
		"	bic	r14, r14, r3			\n\t"
		"	mcrne	p15, #0, r14, c7, c14, #1 	\n\t"
		"0:	mcr	p15, #0, r0 , c7, c14, #1 	\n\t"
		"	adds	r0, r0, r2                	\n\t"
		"	cmp	r0, r14	 			\n\t"
		"	blt	0b				\n\t"
		"	dsb					\n\t"
		"	pop	{r14}				\n\t"
		:
		:
		: "r0","r1","r2","r3"
		);

	#endif

	#endif
	}

// End of File
