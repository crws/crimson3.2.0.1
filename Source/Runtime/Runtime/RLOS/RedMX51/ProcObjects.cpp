
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern bool      Create_HalEdge(void);

extern bool      Create_HalGraphite(void);

extern IDevice * Create_PlatformEdge(UINT uModel);

extern IDevice * Create_PlatformGraphite(UINT uModel);

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Processor Objects
//

// Externals

extern UINT AeonFindModel(void);

// Data

static UINT m_uModel = 0;

// Code

global bool Create_Hal(void)
{
	m_uModel = AeonFindModel();

	switch( m_uModel ) {

		case MODEL_EDGE:
		case MODEL_CORE:

			return Create_HalEdge();

		case MODEL_GRAPHITE_07:
		case MODEL_GRAPHITE_09:
		case MODEL_GRAPHITE_10S:
		case MODEL_GRAPHITE_10V:
		case MODEL_GRAPHITE_12:
		case MODEL_GRAPHITE_15:

			return Create_HalGraphite();
		}

	return false;
	}

global IDevice * Create_Platform(void)
{
	AfxTrace("Model is %u\n", m_uModel);

	switch( m_uModel ) {

		case MODEL_EDGE:
		case MODEL_CORE:

			return Create_PlatformEdge(m_uModel);

		case MODEL_GRAPHITE_07:
		case MODEL_GRAPHITE_09:
		case MODEL_GRAPHITE_10S:
		case MODEL_GRAPHITE_10V:
		case MODEL_GRAPHITE_12:
		case MODEL_GRAPHITE_15:

			return Create_PlatformGraphite(m_uModel);
		}

	for(;;);
	}

// End of File
