
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_iMX51_IpuDc51_HPP
	
#define	INCLUDE_iMX51_IpuDc51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpu51;

//////////////////////////////////////////////////////////////////////////
//
// Write Channel Configuration
//

struct CDcWriteChanConfig
{
	UINT  m_uStartTime;
	UINT  m_uFieldMode;
	UINT  m_uEventMask;
	UINT  m_uMode;
	UINT  m_uDisplay;
	UINT  m_uDi;
	UINT  m_uWordSize;
	UINT  m_uStartAddr;
	};

//////////////////////////////////////////////////////////////////////////
//
// Display Configuration 
//

struct CDcDisplayConfig
{
	UINT  m_uRdValPtr;
	UINT  m_uMcuAccMask;
	UINT  m_uAddrBe;
	UINT  m_uAddrInc;
	UINT  m_uType;
	UINT  m_uStride;
	};

//////////////////////////////////////////////////////////////////////////
//
// User Event Configuration 
//

struct CDcUserEventConfig
{
	UINT  m_uTrig;
	bool  m_fAutoRestart;
	bool  m_fOddMode;
	UINT  m_uCodeOddStartAddr;
	UINT  m_uCodeEvenStartAddr;
	UINT  m_uPriority;
	UINT  m_uDcChan;
	UINT  m_uCountCompare;
	UINT  m_uCountOffset;
	UINT  m_uEventCount;
	};

//////////////////////////////////////////////////////////////////////////
//
// Bus Mapping Configuration
//

struct CDcBusMappingData
{
	UINT  m_uOffset[3];
	UINT  m_uMask  [3];
	};

//////////////////////////////////////////////////////////////////////////
//
// Microcode Configuration
//

struct CDcMicrocodeConfig
{
	UINT  m_uOpcode;
	UINT  m_uOperand;
	UINT  m_uMapping;
	UINT  m_uWaveform;
	UINT  m_uGlueLogic;
	UINT  m_uSync;
	bool  m_fStop;
	};

//////////////////////////////////////////////////////////////////////////
//
// Microcode Opcodes
//

enum 
{
	opHlg,
	opWrg,
	opHloa,
	opWroa,
	opHlod,
	opWrod,
	opHloar,
	opWroar,
	opHlodr,
	opWrodr,
	opWrbc,
	opWclk,
	opWstsi1,
	opWstsi2,
	opWstsi3,
	opRd,
	opWack,
	opMsk,
	opHma,
	opHma1,
	opBma,
	};

//////////////////////////////////////////////////////////////////////////
//
// Microcode Event Types
//

enum 
{
	dcNewLine,
	dcNewFrame,
	dcNewField,
	dcEndOfFrame,
	dcEndOfField,
	dcEndOfLine,
	dcNewChan,
	dcNewAddr,
	dcNewData,
	};

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Image Processing Unit Display Controller Module
//

class CIpuDisplayControl51
{	
	public:
		// Constructor
		CIpuDisplayControl51(CIpu51 *pIpu);

		// Interface
		void Enable(bool fEnable);
		bool SetChanMode(UINT uChan, UINT uMode);
		bool SetWriteChanConfig(UINT uChan, CDcWriteChanConfig const &Config);
		bool SetDispConfig(UINT uDisp, CDcDisplayConfig const &Config); 
		bool SetUserEvent(UINT uEvent, CDcUserEventConfig const &Config);
		bool DisableUserEvent(UINT uEvent);
		bool SetMicrocode(UINT uIdx, CDcMicrocodeConfig const &Config);
		bool SetMicrocodeEvent(UINT uChan, UINT uEvent, UINT uPriority, UINT uAddr);
		bool SetMappingPointer(UINT uPtr, CDcBusMappingData const &Config);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Regisgters
		enum
		{
			regReadChConf	= 0x0000 / sizeof(DWORD),
			regReadChAddr	= 0x0004 / sizeof(DWORD),
			regRl0Ch0	= 0x0008 / sizeof(DWORD),
			regRl1Ch0	= 0x000C / sizeof(DWORD),
			regRl2Ch0	= 0x0010 / sizeof(DWORD),
			regRl3Ch0	= 0x0014 / sizeof(DWORD),
			regRl4Ch0	= 0x0018 / sizeof(DWORD),
			regWrChConf1	= 0x001C / sizeof(DWORD),
			regWrChAddr1	= 0x0020 / sizeof(DWORD),
			regRl0Ch1	= 0x0024 / sizeof(DWORD),
			regRl1Ch1	= 0x0028 / sizeof(DWORD),
			regRl2Ch1	= 0x002C / sizeof(DWORD),
			regRl3Ch1	= 0x0030 / sizeof(DWORD),
			regRl4Ch1	= 0x0034 / sizeof(DWORD),
			regWrChConf2	= 0x0038 / sizeof(DWORD),
			regWrChAddr2	= 0x003C / sizeof(DWORD),
			regRl0Ch2	= 0x0040 / sizeof(DWORD),
			regRl1Ch2	= 0x0044 / sizeof(DWORD),
			regRl2Ch2	= 0x0048 / sizeof(DWORD),
			regRl3Ch2	= 0x004C / sizeof(DWORD),
			regRl4Ch2	= 0x0050 / sizeof(DWORD),
			regWrChConf3	= 0x0054 / sizeof(DWORD),
			regWrChConf4	= 0x0058 / sizeof(DWORD),
			regWrChConf5	= 0x005C / sizeof(DWORD),
			regWrChAddr5	= 0x0060 / sizeof(DWORD),
			regRl0Ch5	= 0x0064 / sizeof(DWORD),
			regRl1Ch5	= 0x0068 / sizeof(DWORD),
			regRl2Ch5	= 0x006C / sizeof(DWORD),
			regRl3Ch5	= 0x0070 / sizeof(DWORD),
			regRl4Ch5	= 0x0074 / sizeof(DWORD),
			regWrChConf6	= 0x0078 / sizeof(DWORD),
			regWrChAddr6	= 0x007C / sizeof(DWORD),
			regRl0Ch6	= 0x0080 / sizeof(DWORD),
			regRl1Ch6	= 0x0084 / sizeof(DWORD),
			regRl2Ch6	= 0x0088 / sizeof(DWORD),
			regRl3Ch6	= 0x008C / sizeof(DWORD),
			regRl4Ch6	= 0x0090 / sizeof(DWORD),
			regWrCh8Conf1	= 0x0094 / sizeof(DWORD),
			regWrCh8Conf2	= 0x0098 / sizeof(DWORD),
			regRl1Ch8	= 0x009C / sizeof(DWORD),
			regRl2Ch8	= 0x00A0 / sizeof(DWORD),
			regRl3Ch8	= 0x00A4 / sizeof(DWORD),
			regRl4Ch8	= 0x00A8 / sizeof(DWORD),
			regRl5Ch8	= 0x00AC / sizeof(DWORD),
			regRl6Ch8	= 0x00B0 / sizeof(DWORD),
			regWrCh9Conf1	= 0x00B4 / sizeof(DWORD),
			regWrCh9Conf2	= 0x00B8 / sizeof(DWORD),
			regRl1Ch9	= 0x00BC / sizeof(DWORD),
			regRl2Ch9	= 0x00C0 / sizeof(DWORD),
			regRl3Ch9	= 0x00C4 / sizeof(DWORD),
			regRl4Ch9	= 0x00C8 / sizeof(DWORD),
			regRl5Ch9	= 0x00CC / sizeof(DWORD),
			regRl6Ch9	= 0x00D0 / sizeof(DWORD),
			regGen		= 0x00D4 / sizeof(DWORD),
			regDispConf1x	= 0x00D8 / sizeof(DWORD),
			regDispConf2x	= 0x00E8 / sizeof(DWORD),
			regDi0Conf1	= 0x00F8 / sizeof(DWORD),
			regDi0Conf2	= 0x00FC / sizeof(DWORD),
			regDi1Conf1	= 0x0100 / sizeof(DWORD),
			regDi1Conf2	= 0x0104 / sizeof(DWORD),
			regMapConfx	= 0x0108 / sizeof(DWORD),
			regUgde0x	= 0x0174 / sizeof(DWORD),
			regUgde1x	= 0x0184 / sizeof(DWORD),
			regUgde2x	= 0x0194 / sizeof(DWORD),
			regUgde3x	= 0x01A4 / sizeof(DWORD),
			regLla0		= 0x01B4 / sizeof(DWORD),
			regLla1		= 0x01B8 / sizeof(DWORD),
			regRlla0	= 0x01BC / sizeof(DWORD),
			regRlla1	= 0x01C0 / sizeof(DWORD),
			regWrChAddr5Alt	= 0x01C4 / sizeof(DWORD),
			regStat		= 0x01C8 / sizeof(DWORD),
			};

		// Data
		PVDWORD   m_pBase;
		PVDWORD   m_pCode;
		CIpu51  * m_pIpu;
		UINT      m_uUnit;
		DWORD	  m_dwUsed;

		// Implementation
		void Init(void);
		UINT FindFreeMask(void);
	};

// End of File

#endif
