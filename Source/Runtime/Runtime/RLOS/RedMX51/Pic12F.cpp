
#include "Intern.hpp"

#include "Pic12F.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Gpio51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PIC12F In-Circuit Programmer
//

// Constructor

CPic12F::CPic12F(IGpio **pGpio, UINT uPinD, UINT uPinC, UINT uPinM)
{
	m_pGpio = pGpio;

	m_uBitM = uPinM;

	m_uBitC = uPinC;

	m_uBitD = uPinD;

	InitGpio();
	}

// Destructor

CPic12F::~CPic12F(void)
{
	}

// Operations

DWORD CPic12F::ReadVer(void)
{
	WORD wData[4];

	Read(0x8000, wData, elements(wData));

	return MAKELONG(MAKEWORD(wData[0], wData[1]), MAKEWORD(wData[2], wData[3]));
	}

void CPic12F::LoadVer(DWORD dwVer)
{
	WORD wData[4] = {
		
		LOBYTE(LOWORD(dwVer)),
		HIBYTE(LOWORD(dwVer)),
		LOBYTE(HIWORD(dwVer)),
		HIBYTE(HIWORD(dwVer)),
		};

	Load(0x8000, wData, elements(wData));
	}

bool CPic12F::Load(PCTXT pHex)
{
	enum 
	{
		stateCode,
		stateByte,
		stateAddr,
		stateType,
		stateData,
		stateCheck
		};

	WORD wData[8];

	UINT uState = 0;

	UINT uScan  = 0;

	UINT uData  = 0;

	UINT uCount = 0;

	UINT uAddr  = 0;

	UINT uType  = 0;

	UINT uCheck = 0;

	UINT uRead  = 0;

	while( *pHex ) {
		
		BYTE b = BYTE(*pHex++);

		if( b == ':' ) {

			uState = stateByte;

			uData  = 0;
			
			uScan  = 2;

			uCheck = 0;

			uRead  = 0;

			continue;
			}

		if( uScan ) {

			if( b >= 'A' && b <= 'F') {

				b -= ('A' - 0xA);
				}
			else {
				b -= '0';
				}

			uData <<= 4;

			uData  |= b;

			uScan  -= 1;
			}

		if( uState != stateCheck ) {

			if( !(uScan & 1) ) {

				uCheck += BYTE(uData);
				}
			}

		if( uScan ) {

			continue;
			}

		switch( uState ) {
	
			case stateByte:

				uCount = uData / 2;

				uState = stateAddr;

				uScan  = 4;

				break;

			case stateAddr:

				uAddr &= 0xFFFF0000;
				
				uAddr |= uData;

				uState = stateType;

				uScan  = 2;

				break;

			case stateType:

				uType  = uData;

				uState = stateData;

				uScan  = 4;

				break;

			case stateData:

				wData[ uRead ++ ] = ::HostToMotor(WORD(uData));

				uScan = 4;

				if( uRead == uCount ) {

					uState = stateCheck;

					uScan = 2;
					}

				break;

			case stateCheck:

				uState = stateCode;

				if( LOBYTE(uData) == LOBYTE(~uCheck+1) ) {

					switch( uType ) {

						case 0x00:

							Load(uAddr / 2, wData, uRead);
						
							break;

						case 0x04:

							uAddr = ::HostToMotor(wData[0]) << 16;

							break;
						}
					
					break;
					}
				
				return false;
			}
			
		uData = 0;
		}

	return true;
	}

void CPic12F::Unlock(bool fLock)
{
	if( fLock ) {

		EnterProgMode();
		}
	else {
		LeaveProgMode();
		}
	}

void CPic12F::Erase(void)
{
	LoadConfig(0);

	BulkEraseProg();

	BulkEraseData();
	}

void CPic12F::Read(UINT uAddr, PWORD pData, UINT uCount)
{
	SetAddr(uAddr);

	while( uCount-- ) {

		*pData++ = ReadProg();

		IncrementAddr();
		}
	}

void CPic12F::Load(UINT uAddr, PCWORD pData, UINT uCount)
{
	SetAddr(uAddr);

	while( uCount ) {

		UINT uLatch = uCount;

		MakeMin(uLatch, constLatches);

		MakeMin(uLatch, constLatches - (m_uAddr % constLatches));

		for( UINT n = 0; n < uLatch; n ++ ) {

			if( n ) {

				IncrementAddr();
				}

			LoadProg(*pData++);
			}

		BeginIntProgram();

		IncrementAddr();

		uCount -= uLatch;
		}
	}

void CPic12F::SetAddr(UINT uAddr)
{
	if( uAddr < 0x8000 ) {

		if( m_uAddr >= 0x8000 || uAddr < m_uAddr ) {

			ResetAddr();
			}
		}
	else {
		if( m_uAddr < 0x8000 || uAddr < m_uAddr ) {

			LoadConfig(0);
			}
		}

	while( m_uAddr < uAddr ) {

		IncrementAddr();
		}
	}

// Command Helpers

void CPic12F::EnterProgMode(void)
{
	m_uAddr = 0;

	DWORD Unlock = 0x4D434850;

	SetClockLo();
	
	SetMclrLo();

	for( UINT i = 0; i < 32; i ++ ) {

		PutBit(Unlock & 1);

		Unlock >>= 1;
		}

	SetClockHi();

	SetClockLo();
	}

void CPic12F::LeaveProgMode(void)
{
	SetMclrHi();
	}

void CPic12F::LoadConfig(WORD Data)
{
	m_uAddr = 0x8000;

	PutCmd(cmdLoadConfig);

	PutData(Data);
	}

void CPic12F::LoadProg(WORD Data)
{
	PutCmd(cmdLoadProg);

	PutData(Data);
	}

void CPic12F::LoadData(BYTE Data)
{
	PutCmd(cmdLoadData);

	PutData(Data);
	}

WORD CPic12F::ReadProg(void)
{
	PutCmd(cmdReadProg);

	return GetData();
	}

BYTE CPic12F::ReadData(void)
{
	PutCmd(cmdReadData);

	return LOBYTE(GetData());
	}

void CPic12F::IncrementAddr(void)
{
	m_uAddr ++;

	PutCmd(cmdIncAddr);
	}

void CPic12F::ResetAddr(void)
{
	m_uAddr = 0x0000;

	PutCmd(cmdResetAddr);
	}

void CPic12F::BeginIntProgram(void)
{
	PutCmd(cmdBegIntProg);

	Sleep(10);
	}

void CPic12F::BulkEraseProg(void)
{
	PutCmd(cmdBulkEraseProg);

	Sleep(10);
	}

void CPic12F::BulkEraseData(void)
{
	PutCmd(cmdBulkEraseData);

	Sleep(10);
	}

// Command Helpers

void CPic12F::PutCmd(UINT uCmd)
{
	for( UINT i = 0; i < 6; i ++ ) {

		PutBit( uCmd & 1 );

		uCmd >>= 1;
		}

	Delay(1);
	}

void CPic12F::PutData(WORD wData)
{
	SetClockHi();

	SetDataLo();

	SetClockLo();

	for( UINT i = 0; i < 14; i ++ ) {

		PutBit(wData & 1);

		wData >>= 1;
		}
	
	SetClockHi();

	SetDataLo();

	SetClockLo();
	}

WORD CPic12F::GetData(void)
{
	SetDataIn();

	SetClockHi();

	SetClockLo();

	WORD w = 0;

	WORD m = 1;

	for( UINT i = 0; i < 14; i ++ ) {

		if( GetBit() ) {

			w |= m;
			}

		m <<= 1;
		}
	
	SetClockHi();

	SetClockLo();

	SetDataOut();

	SetDataLo();

	return w;
	}

// Bit Helpers

void CPic12F::PutBit(bool fBit)
{
	SetData(fBit);

	SetClockHi();

	SetClockLo();
	}

bool CPic12F::GetBit(void)
{
	SetClockHi();

	bool fData = GetDataIn();

	SetClockLo();

	return fData;
	}

void CPic12F::SetMclrHi(void)
{
	SetGpioState(m_uBitM, true);	

	Delay(250);	
	}

void CPic12F::SetMclrLo(void)
{
	SetGpioState(m_uBitM, false);	

	Delay(250);	
	}

void CPic12F::SetClockHi(void)
{
	SetGpioState(m_uBitC, true);	

	Delay(1);
	}

void CPic12F::SetClockLo(void)
{
	SetGpioState(m_uBitC, false);	

	Delay(1);
	}

void CPic12F::SetDataHi(void)
{
	SetGpioState(m_uBitD, true);	
	}

void CPic12F::SetDataLo(void)
{
	SetGpioState(m_uBitD, false);	
	}

void CPic12F::SetDataIn(void)
{
	SetGpioDirection(m_uBitD, false);	
	}

void CPic12F::SetDataOut(void)
{
	SetGpioDirection(m_uBitD, true);	
	}

void CPic12F::SetData(bool fHi)
{
	SetGpioState(m_uBitD, fHi);	
	}

bool CPic12F::GetDataIn(void)
{
	return GetGpioState(m_uBitD);	
	}

void CPic12F::Delay(UINT uDelay)
{
	phal->SpinDelayFast(uDelay);	
	}

void CPic12F::SetGpioDirection(UINT uSel, bool fOutput)
{
	UINT uPort = uSel / 32;

	UINT iBit  = uSel % 32;

	m_pGpio[uPort]->SetDirection(iBit, fOutput);
	}

void CPic12F::SetGpioState(UINT uSel, bool fState)
{
	UINT uPort = uSel / 32;

	UINT iBit  = uSel % 32;

	m_pGpio[uPort]->SetState(iBit, fState);
	}

bool CPic12F::GetGpioState(UINT uSel)
{
	UINT uPort = uSel / 32;

	UINT iBit  = uSel % 32;

	return m_pGpio[uPort]->GetState(iBit);
	}

void CPic12F::InitGpio(void)
{
	SetGpioDirection(m_uBitD, true);	

	SetGpioDirection(m_uBitC, true);	

	SetGpioDirection(m_uBitM, true);

	SetGpioState(m_uBitM, true);	

	SetGpioState(m_uBitC, true);	
	}

// End of File
