
#include "Intern.hpp"

#include "Dpll51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Digital Phase Lock Loop Interface
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CDpll51::CDpll51(UINT iIndex)
{
	m_pBase    = PVDWORD(FindBase(iIndex));

	m_uFreq[0] = 24000000;

	m_uFreq[1] = 32768 * 1024;
	}

// Operations

void CDpll51::Set(UINT uMfn)
{
	Reg(MFN)    = uMfn;

	Reg(Config) = Bit(0);

	while( Reg(Config) & Bit(0) );
	}

void CDpll51::Set(UINT uRef, UINT uMfi, UINT uMfn, UINT uMfd, UINT uPdf, BOOL fDouble)
{
	Reg(Config)    = 0;

	Reg(Ctrl)      = (uRef << 8) | Bit(5) | Bit(2) | (uMfd < 8 ? 0 : Bit(1)) | (fDouble ? Bit(12) : 0);
	
	Reg(Operation) = (uMfi << 4) | (uPdf - 1);

	Reg(MFD)       = uMfd - 1;

	Reg(MFN)       = uMfn;

	Reg(Config)    = Bit(1);

	Reg(Ctrl)     |= Bit(4);

	WaitLock();
	}

DWORD CDpll51::Get(void) const
{
	DWORD  Ctrl = Reg(Ctrl);

	DWORD  Mfi  = (Reg(Operation) >> 4) & 0x0F;

	DWORD  Pdf  = (Reg(Operation) >> 0) & 0x0F;

	DWORD  Mfn  = Reg(MFN);

	DWORD  Mfd  = Reg(MFD);

	DWORD  Mul  = (Ctrl & Bit(10)) ? 2 : 4; 
	
	UINT64 Ref  = GetRefFreq((Ctrl >> 8) & 0x3) * Mul;
	
	UINT64 Freq = (Ref * Mfi + (Ref * Mfn) / (Mfd + 1)) / (Pdf + 1);

	if( !(Ctrl & Bit(12)) ) {

		Freq /= 2;
		}

	return DWORD(Freq);
	}

// Implementation

DWORD CDpll51::FindBase(UINT i) const
{
	switch( i ) {

		case 0 : return ADDR_DPLLIP1;
		case 1 : return ADDR_DPLLIP2;
		case 2 : return ADDR_DPLLIP3;
		}

	return 0;
	}

UINT CDpll51::GetRefFreq(UINT uSel) const
{
	switch( uSel ) {

		case refOsc: return m_uFreq[0];

		case refFpm: return m_uFreq[1];
		}

	return 0;
	}

// End of File
