
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Intern_HPP

#define INCLUDE_Intern_HPP

//////////////////////////////////////////////////////////////////////////
//
// Environment Selection
//

#define AEON_NEED_RLOS

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include "StdEnv.hpp"

#include "Models.hpp"

#include "MemoryMap.hpp"

#include "MemorySlot.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Broker
//

extern IObjectBroker * piob;

//////////////////////////////////////////////////////////////////////////
//
// Optimized Copy
//

global void ArmMemCpy(PVOID d, PCVOID s, UINT n);

//////////////////////////////////////////////////////////////////////////
//
// Panic Codes
//

#define PANIC_DEVICE_MMU	(PANIC_DEVICE + 0x01)
#define PANIC_DEVICE_CCM	(PANIC_DEVICE + 0x02)

// End of File

#endif
