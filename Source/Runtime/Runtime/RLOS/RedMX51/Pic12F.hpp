
//////////////////////////////////////////////////////////////////////////
//
// Shared Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Shared_Pic12F_HPP
	
#define	INCLUDE_Shared_Pic12F_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CGpio51;

//////////////////////////////////////////////////////////////////////////
//
// PIC12F In-Circuit Programmer
//

class CPic12F
{
	public:
		// Constructor
		CPic12F(IGpio **pGpio, UINT uPinD, UINT uPinC, UINT uPinM);

		// Destructor
		~CPic12F(void);

		// Operations
		void  Unlock(bool fLock);
		bool  Load(PCTXT pHex);
		DWORD ReadVer(void);
		void  LoadVer(DWORD dwVer);
		void  Read(UINT uAddr, PWORD  pData, UINT uCount);
		void  Load(UINT uAddr, PCWORD pData, UINT uCount);
		void  Erase(void);

	protected:
		// Commands
		enum
		{
			cmdLoadConfig	 = 0x00,
			cmdLoadProg	 = 0x02,
			cmdLoadData	 = 0x03,
			cmdReadProg	 = 0x04,
			cmdReadData	 = 0x05,
			cmdIncAddr	 = 0x06,
			cmdResetAddr	 = 0x16,
			cmdBegIntProg	 = 0x08,
			cmdBegExtProg	 = 0x18,
			cmdEndExtProg	 = 0x0A,
			cmdBulkEraseProg = 0x09,
			cmdBulkEraseData = 0x0B,
			cmdRowEraseProg	 = 0x11,
			};

		// Constants
		enum
		{
			constLatches = 16,
			};

		// Data Members
		IGpio   ** m_pGpio;
		UINT	   m_uBitM;
		UINT	   m_uBitC;
		UINT	   m_uBitD;
		UINT       m_uAddr;

		// Commands
		void SetAddr(UINT uAddr);
		void EnterProgMode(void);
		void LeaveProgMode(void);
		void LoadConfig(WORD Data);
		void LoadProg(WORD Data);
		void LoadData(BYTE Data);
		WORD ReadProg(void);
		BYTE ReadData(void);
		void IncrementAddr(void);
		void ResetAddr(void);
		void BeginIntProgram(void);
		void BulkEraseProg(void);
		void BulkEraseData(void);

		// Command Helpers
		void PutCmd(UINT uCmd);
		void PutData(WORD wData);
		WORD GetData(void);

		// Bit Helpers
		void PutBit(bool fBit);
		bool GetBit(void);
		void SetMclrHi(void);
		void SetMclrLo(void);
		void SetClockHi(void);
		void SetClockLo(void);
		void SetDataHi(void);
		void SetDataLo(void);
		void SetDataIn(void);
		void SetDataOut(void);
		void SetData(bool fData);
		void SetDataDir(bool fOut);
		bool GetDataIn(void);
		void Delay(UINT uDelay);
		void SetGpioDirection(UINT uSel, bool fOutput);
		void SetGpioState(UINT uSel, bool fSet);
		bool GetGpioState(UINT uSel);
		void InitGpio(void);
	};

// End of File

#endif
