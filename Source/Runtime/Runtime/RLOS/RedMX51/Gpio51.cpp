
#include "Intern.hpp"

#include "Gpio51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// iMX51 GPIO Unit
//

// Instantiator

global IDevice * Create_Gpio51(UINT iIndex)
{
	IDevice *pDevice = New CGpio51(iIndex);

	pDevice->Open();

	return pDevice;
	}

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CGpio51::CGpio51(UINT iIndex)
{
	StdSetRef();

	m_pBase  = PVDWORD(FindBase(iIndex));
	
	m_uLine  = FindLine(iIndex);

	Reg(IMR) = 0x00000000;

	memset(m_Sink, 0, sizeof(m_Sink));
	}

// IUnknown

HRESULT CGpio51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IGpio);

	return E_NOINTERFACE;
	}

ULONG CGpio51::AddRef(void)
{
	StdAddRef();
	}

ULONG CGpio51::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL CGpio51::Open(void)
{
	return TRUE;
	}

// IGpio

void CGpio51::SetDirection(UINT n, BOOL fOutput)
{
	if( fOutput ) {

		AtomicBitSet(&m_pBase[regGDIR], n);
		}
	else {
		AtomicBitClr(&m_pBase[regGDIR], n);
		}
	}

void CGpio51::SetState(UINT n, BOOL fState)
{
	if( fState ) {

		AtomicBitSet(&m_pBase[regDR], n);
		}
	else {
		AtomicBitClr(&m_pBase[regDR], n);
		}
	}

BOOL CGpio51::GetState(UINT n)
{
	return m_pBase[regPSR] & Bit(n);	
	}

void CGpio51::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine + 0, this, 0);
	
	phal->SetLineHandler(m_uLine + 1, this, 0);

	phal->EnableLine    (m_uLine + 0, true);

	phal->EnableLine    (m_uLine + 1, true);
	}

void CGpio51::SetIntHandler(UINT n, UINT uType, IEventSink *pSink, UINT uParam)
{
	EnableEvents();	

	if( n < elements(m_Sink) ) {

		DWORD dwMask = Bit(n);	

		Reg(IMR) &= ~dwMask;

		Reg(ISR)  = dwMask;

		if( uType < intEdgeAny ) {

			DWORD volatile &ICR = Reg(ICR1 + n / 16);

			ICR &= ~(DWORD(0x3)   << ((n % 16) * 2));

			ICR |=  (DWORD(uType) << ((n % 16) * 2));

			Reg(EDR) &= ~dwMask;
			}
		else {
			Reg(EDR) |= dwMask;
			}

		m_Sink[n].m_pSink  = pSink;

		m_Sink[n].m_uParam = uParam;
		}
	}

void CGpio51::SetIntEnable(UINT n, BOOL fEnable)
{
	if( n < elements(m_Sink) ) {

		if( fEnable ) {

			Reg(IMR) |= Bit(n);
			}
		else {
			Reg(IMR) &= ~Bit(n);
			}
		}
	}

// IEventSink

void CGpio51::OnEvent(UINT uLine, UINT uParam)
{
	for(;;) {

		DWORD dwStat = Reg(ISR) & Reg(IMR);
		
		if( dwStat ) {

			DWORD dwMask = 1;
			
			for( UINT i = 0; i < elements(m_Sink); i ++ ) {

				if( dwStat & dwMask ) {

					if( m_Sink[i].m_pSink ) {

						m_Sink[i].m_pSink->OnEvent(uLine, m_Sink[i].m_uParam);
						}
					}

				dwMask <<= 1;
				}
			
			Reg(ISR) = dwStat;
			
			continue;
			}

		break;
		}
	}

// Implementation

DWORD CGpio51::FindBase(UINT iIndex) const
{
	switch( iIndex ) {

		case 0: return ADDR_GPIO1;
		case 1: return ADDR_GPIO2;
		case 2: return ADDR_GPIO3;
		case 3: return ADDR_GPIO4;
		}

	return NOTHING;
	}

UINT CGpio51::FindLine(UINT iIndex) const
{
	switch( iIndex ) {

		case 0: return INT_GPIO1_L0;
		case 1: return INT_GPIO2_L0;
		case 2: return INT_GPIO3_L0;
		case 3: return INT_GPIO4_L0;
		}

	return NOTHING;
	}

// End of File
