
#include "Intern.hpp"

#include "Mmu51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Memory Management Unit
//

// Top of Memory

extern unsigned long _top;

// Constants

#define SECT_SHIFT	20

#define SECT_SIZE	(1<<SECT_SHIFT)

#define SECT_MASK	(~(SECT_SIZE-1))

// Instantiator

IMmu * Create_Mmu51(IHal *pHal)
{
	return New CMmu51;
	}

// Constructor

CMmu51::CMmu51(void)
{
	DWORD base = 0;

	#if !defined(__INTELLISENSE__)

	ASM(	"mrc p15, 0, %0, c2, c0, 0\n\t"
		: "=r"(base)
		);

	#endif

	m_pDesc = (DESC *) base;

	FindGranularity();

//	ShowTypes();

	BlockUnused();

	SetTypes();

	HostFlushTLBs();
	
//	ShowAll();
	}

// IMmu

bool CMmu51::IsVirtualValid(DWORD dwAddr, bool fWrite)
{
	if( dwAddr ) {

		DESC &d = (DESC &) m_pDesc[dwAddr >> SECT_SHIFT];

		if( d.AP && (!fWrite || !d.AP2) ) {

			switch( d.d & 0xF0000000 ) {

				case ADDR_DDR1:
				case ADDR_DDR2:

					// Only the DRAM is valid for this test.

					return true;
				}
			}
		}

	return false;
	}

DWORD CMmu51::PhysicalToVirtual(DWORD dwAddr, bool fCached)
{
	for( UINT n = 0; n < 4096; n += m_uStep ) {

		DESC &d = (DESC &) m_pDesc[n];

		if( IsSection(d) ) {

			if( (dwAddr & m_dwMask) == (d.d & m_dwMask) ) {

				if( IsCached(d) == fCached ) {

					return (n << SECT_SHIFT) | (dwAddr & ~m_dwMask);
					}
				}
			}
		}

	HostTrap(PANIC_DEVICE_MMU);

	return NOTHING;
	}

DWORD CMmu51::VirtualToPhysical(DWORD dwAddr)
{
	return (m_pDesc[dwAddr >> SECT_SHIFT].d & SECT_MASK) | (dwAddr & ~SECT_MASK);
	}

void CMmu51::DebugRegister(void)
{
	}

void CMmu51::DebugRevoke(void)
{
	}

// Implementation

bool CMmu51::FindGranularity(void)
{
	// This is a bit hairy but it looks at the MMU table and figures
	// out how often we have to check entries when converting from a
	// physical address to a virtual address. Without this, we might
	// have to look at 4096 entries, but using this trick we can get
	// the count down to 16 on a typical iMX51 system.

	UINT   uCount  = 4096;

	UINT64 uSpace  = 0x100000000LL;

	UINT   uEach   = UINT(uSpace / uCount);

	DWORD  dwLast  = NOTHING;

	UINT   uVirt   = 0;

	UINT   uPhys   = (m_pDesc[0].d & SECT_MASK);

	UINT   uBlock  = 1;

	UINT   uLimit  = 0x80000000;

	for( UINT n = 0; n < uCount; n++ ) {

		DESC &d = (DESC &) m_pDesc[n];

		if( n ) {

			// We basically look for runs of adjacent entries, and then
			// when the run ends, do some GCD math on the size and the base
			// addresses to figure out how many subdivisions we can use.

			if( (d.d & SECT_MASK) == ((dwLast & SECT_MASK) + SECT_SIZE) ) {

				uBlock++;

				if( n < uCount - 1 ) {

					dwLast = d.d;

					continue;
					}
				}

			UINT a = Gcd64(uSpace + uVirt, uSpace + uPhys) / uEach;

			uLimit = Gcd32(Gcd32(uLimit, a), uBlock);

			uBlock = 1;

			uVirt  = n << 20;

			uPhys  = (d.d & SECT_MASK);
			}

		dwLast = d.d;
		}

	// Now we've figured out the subdivision size, we can
	// convert this into the mask that we use for testing.

	m_uStep  = uLimit;

	m_dwMask = SECT_MASK;

	while( (uLimit >>= 1) ) {

		m_dwMask <<= 1;
		}

	AfxTrace("MMU mask is 0x%8.8X\n", m_dwMask);

	return true;
	}

UINT CMmu51::Gcd32(UINT a, UINT b)
{
	// Greatest Common Divisor by...
	//
	// https://en.wikipedia.org/wiki/Euclidean_algorithm

	return UINT(b ? Gcd32(b, a % b) : a);
	}

UINT CMmu51::Gcd64(UINT64 a, UINT64 b)
{
	// Greatest Common Divisor by...
	//
	// https://en.wikipedia.org/wiki/Euclidean_algorithm

	return UINT(b ? Gcd64(b, a % b) : a);
	}

void CMmu51::BlockUnused(void)
{
	if( _top == 0x30000000 ) {

		BlockRange(0x10000000, 0x20000000);
		BlockRange(0x30000000, 0x40000000);
		}

	BlockRange(0x40000000, 0x5DF00000);
	BlockRange(0x60000000, 0x70000000);
	BlockRange(0x74000000, 0x80000000);
	BlockRange(0x84000000, 0x90000000);
	BlockRange(0xB0000000, 0xCF000000);
	BlockRange(0xD0000000, 0xE0000000);
	BlockRange(0xE0100000, 0x00000000);
	}

void CMmu51::SetTypes(void)
{
	#if 0

	// TODO -- Figure out why we can't use WB  !!!

	// TODO -- Figure out why we can't use DEV !!!

	SetRangeType(0x00000000, 0x20000000, 4); // STD-NC
	SetRangeType(0x20000000, 0x40000000, 2); // STD-WT
	SetRangeType(0x40000000, 0x90000000, 1); // DEV-NC
	SetRangeType(0x90000000, 0xB0000000, 4); // STD-NC
	SetRangeType(0xB0000000, 0x00000000, 1); // DEV-NC

	#endif
	}

void CMmu51::BlockRange(DWORD a, DWORD b)
{
	while( a != b ) {

		DESC &d = (DESC &) m_pDesc[a >> SECT_SHIFT];

		d.AP2 = 0;
		d.AP  = 0;

		a += SECT_SIZE;
		}
	}

void CMmu51::SetRangeType(DWORD a, DWORD b, UINT t)
{
	while( a != b ) {

		DESC &d = (DESC &) m_pDesc[a >> SECT_SHIFT];

		d.TEX = ((t & 4) >> 2);
		d.C   = ((t & 2) >> 1);
		d.B   = ((t & 1) >> 0);
		
		a += SECT_SIZE;
		}
	}

void CMmu51::ShowAll(void)
{
	for( UINT n = 0; n < 4096; n += m_uStep ) {

		ShowAddress(n << 20);
		}
	}

void CMmu51::ShowAddress(DWORD a)
{
	DESC const &d = (DESC const &) m_pDesc[a >> SECT_SHIFT];

	DWORD SCTR, PRRR, NMRR;

	HostGetSCTR(SCTR);
	HostGetPRRR(PRRR);
	HostGetNMRR(NMRR);

	AfxTrace("Address %8.8X ", a);

	if( SCTR & Bit(28) ) {

		// TEX Extensions

		UINT ndx  = (d.B << 0) | (d.C << 1) | ((d.TEX & 1) << 2);

		UINT tr  = ((PRRR >> ( 0 + 2 * ndx)) & 3);

		UINT nos = ((PRRR >> (24 + 1 * ndx)) & 1);

		UINT ci  = ((NMRR >> ( 0 + 2 * ndx)) & 3);

		UINT co  = ((NMRR >> (16 + 2 * ndx)) & 3);

		static char const *n[] = { "N", "Y" };

		static char const *t[] = { "S/O", "DEV", "STD", "???" };

		static char const *c[] = { "NC", "WA", "WT", "WB" };

		AfxTrace("ndx=%u type=%s shared=%s inner=%s outer=%s\n", ndx, t[tr], n[nos], c[ci], c[co]);

		return;
		}

	AfxTrace("not decoded in non-TRE mode\n");
	}

void CMmu51::ShowTypes(void)
{
	DWORD SCTR, PRRR, NMRR;

	HostGetSCTR(SCTR);
	HostGetPRRR(PRRR);
	HostGetNMRR(NMRR);

	for( UINT ndx = 0; ndx < 8; ndx++ ) {

		UINT tr  = ((PRRR >> ( 0 + 2 * ndx)) & 3);

		UINT nos = ((PRRR >> (24 + 1 * ndx)) & 1);

		UINT ci  = ((NMRR >> ( 0 + 2 * ndx)) & 3);

		UINT co  = ((NMRR >> (16 + 2 * ndx)) & 3);

		static char const *n[] = { "N", "Y" };

		static char const *t[] = { "S/O", "DEV", "STD", "???" };

		static char const *c[] = { "NC", "WA", "WT", "WB" };

		AfxTrace("ndx=%u type=%s shared=%s inner=%s outer=%s\n", ndx, t[tr], n[nos], c[ci], c[co]);
		}
	}

// End of File
