
#include "Intern.hpp"

#include "UsbBase51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//


AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Macros  
//

#define RoundUp(x, n)	((x + (n-1)) & ~(n-1))

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define Reg(x)   (m_pBase[reg##x + m_uBase])

//////////////////////////////////////////////////////////////////////////
//
// iMX51 USB Controller Base Class
//

// Constructor

CUsbBase51::CUsbBase51(UINT iIndex)
{
	StdSetRef();

	m_pBase = PVDWORD(ADDR_USBOH3U);

	m_uBase = iIndex * 0x200 / sizeof(DWORD);

	m_uLine = iIndex == 0 ? INT_USBOHOTG : iIndex == 1 ? INT_USBOH1 : INT_USBOH2;

	SetStop();

	SetReset();

	SetIntLatency(0);

	InitHardware();
	}

// Destructor

CUsbBase51::~CUsbBase51(void)
{
	}

// IEventSink

void CUsbBase51::OnEvent(UINT uLine, UINT uParam)
{
	}

// Operations

void CUsbBase51::InitHardware(void)
{
	m_pBase[regCtrl	    ] = 0x00020112;

	m_pBase[regPhyCtrl0 ] = 0x80001400;

	m_pBase[regPhyCtrl1 ] = 0x00541401;

	m_pBase[regCtrl1    ] = 0x0E000000;

	m_pBase[regHost2Ctrl] = 0x00001002;

	m_pBase[regHost3Ctrl] = 0x00001002;
	}

void CUsbBase51::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);
	}

void CUsbBase51::SetMode(UINT uMode)
{
	Reg(UsbMode) = uMode | Bit(3);
	}

void CUsbBase51::SetReset(void)
{
	Reg(UsbCmd) |= cmdReset;

	while( Reg(UsbCmd) & cmdReset );
	}

void CUsbBase51::SetRun(void)
{
	Reg(UsbCmd) |= cmdRun;
	}

void CUsbBase51::SetStop(void)
{
	Reg(UsbCmd) &= ~cmdRun;
	}

void CUsbBase51::SetIntLatency(UINT uFrames)
{
	Reg(UsbCmd) &= ~(0xFF << 16);

	Reg(UsbCmd) |= (uFrames << 16);
	}

void CUsbBase51::SetTimer(UINT i, DWORD dwPeriod)
{
	if( i < 2 ) {

		Reg(GpTimer0Ctrl + 2 * i) = 0x00000000;

		Reg(GpTimer0Load + 2 * i) = dwPeriod;

		Reg(GpTimer0Ctrl + 2 * i) = 0xC0000000;
		}
	}

UINT CUsbBase51::GetTimer(UINT i)
{
	if( i < 2 ) {

		DWORD d = Reg(GpTimer0Ctrl + 2 * i);

		if( d & 0x80000000 ) {

			return d & 0x00FFFFFF;
			}
		}

	return 0;
	}

// Memory Helpers

PVOID CUsbBase51::AllocNonCached(UINT uAlloc)
{
	return AllocNonCached(uAlloc, 64);
	}

PVOID CUsbBase51::AllocNonCached(UINT uAlloc, UINT uAlign)
{
	uAlign = RoundUp(uAlign, 64);

	uAlloc = RoundUp(uAlloc, 64);

	PVOID pMem  = memalign(uAlign, uAlloc);

	DWORD Alias = phal->GetNonCachedAlias(DWORD(pMem));

	DWORD Phy   = phal->VirtualToPhysical(DWORD(pMem));

	ProcPurgeDataCache(Phy, uAlloc);

	return PVOID(Alias);
	}

void CUsbBase51::FreeNonCached(PVOID pMem)
{
	if( pMem ) {

		DWORD Alias = phal->GetCachedAlias(DWORD(pMem));

		free(PVOID(Alias));
		}
	}

// Debug

void CUsbBase51::Dump(void)
{
	#if defined(_XDEBUG)

	AfxTrace("regCtrl       = 0x%8.8X\n", m_pBase[regCtrl     ]);
	AfxTrace("regPhyCtrl0   = 0x%8.8X\n", m_pBase[regPhyCtrl0 ]);
	AfxTrace("regPhyCtrl1   = 0x%8.8X\n", m_pBase[regPhyCtrl1 ]);
	AfxTrace("regCtrl1      = 0x%8.8X\n", m_pBase[regCtrl1    ]);
	AfxTrace("regHost2Ctrl  = 0x%8.8X\n", m_pBase[regHost2Ctrl]);
	AfxTrace("regHost3Ctrl  = 0x%8.8X\n", m_pBase[regHost3Ctrl]);

	#endif
	}

// End of File
