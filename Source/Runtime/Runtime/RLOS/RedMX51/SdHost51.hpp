
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SdHost51_HPP
	
#define	INCLUDE_SdHost51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../RedDev/SdHostGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCcm51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 SD Host Controller
//

class CSdHost51 : public CSdHostGeneric, public IEventSink
{
	public:
		// Constructor
		CSdHost51(CCcm51 *pCcm);

		// Destructor
		~CSdHost51(void);

		// IDevice
		BOOL METHOD Open(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);
		
	protected:
		// Interrupt Masks
		enum
		{
			irqFailCmd  = Bit(16) | Bit(17) | Bit(18) | Bit(19),
			irqFailData = Bit(20) | Bit(21) | Bit(22),
			irqCommand  = Bit( 0),
			irqTransfer = Bit( 1),
			irqWrite    = Bit( 4),
			irqRead	    = Bit( 5),
			irqInsert   = Bit( 6),
			irqRemove   = Bit( 7),
			irqAll	    = irqFailCmd  | irqFailData | irqCommand | irqTransfer | irqWrite | irqRead
			};

		// Data Members
		IGpio         * m_pGpio;
		UINT	        m_uLine;
		UINT	        m_uClock;
		UINT	        m_uDetect;
		UINT            m_uProtect;
		UINT	        m_uBounce;
		ITimer        * m_pTimer;
		IEvent        * m_pCommand;
		IEvent	      * m_pTransfer;
		IEvent	      * m_pWrite;
		IEvent	      * m_pRead;
		IEvent	      * m_pFailCmd;
		IEvent	      * m_pFailData;

		// Controller Interface
		void InitController(void);
		void StopController(void);
		void SetClockSpeed(EClock Clock);
		void SetBusWidth(bool fWide);
		void SetLength(UINT uBytes);

		// Blocking Calls
		void ClearEvents(void);
		bool WaitCmdComplete(void);
		bool SendData(PCBYTE pData, UINT uCount);
		bool ReadData(PBYTE pData, UINT uCount);
		bool CheckBusy(void);

		// Response Offsets
		void OffsetStructure(PBYTE b);

		// Implementation
		void SendInitClocks(void);
		void WaitClockStable(void);
		void EnableEvents(void);
		void DisableEvents(void);
	};

// End of File

#endif
