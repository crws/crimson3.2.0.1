
#include "Intern.hpp"

#include "Nand51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ccm51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Nand Flash Controller
//

// Register Access

#define Reg(x)	(m_pBase[reg##x])

#define Axi(x)	(m_pBuff[axi##x])

// Instantiator

IDevice * Create_Nand51(CCcm51 *pCcm)
{
	CNand51 *p = New CNand51(pCcm);

	p->Open();

	return p;
	}

// Constructor

CNand51::CNand51(CCcm51 *pCcm)
{
	StdSetRef();

	m_pBase	 = PVDWORD(ADDR_EMI);

	m_pBuff	 = PVDWORD(ADDR_NAND_AXI);

	m_uLine	 = INT_EMI_NFC;

	m_uClock = pCcm->GetFreq(CCcm51::clkEnfc);

	m_Geom.m_uChips = 1;

	m_Geom.m_uSpare = NOTHING;

	m_pDone  = Create_AutoEvent();
	
	m_pLock  = Create_Qutex();
	}

// IUnknown

HRESULT CNand51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(INandMemory);

	return E_NOINTERFACE;
	}

ULONG CNand51::AddRef(void)
{
	StdAddRef();
	}

ULONG CNand51::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CNand51::Open(void)
{
	InitController();
	
	EnableEvents();

	Reset(0);

	return TRUE;
	}

// INandMemory

BOOL METHOD CNand51::GetGeometry(CNandGeometry &Geom)
{
	if( m_Geom.m_uSpare == NOTHING ) {

		Lock();

		AtomicReadId(0);

		PBYTE pInfo = PBYTE(m_pBuff);

		if( pInfo[0] == 0x2C ) {

			if( pInfo[1] == 0xAA ) {

				m_Geom.m_uPageSize   = 2048;
				
				m_Geom.m_uBlockSize  = 64;
				
				m_Geom.m_uPlaneSize  = 1024;
				
				m_Geom.m_uDeviceSize = 2;
				
				m_Geom.m_uSpare      = 64;
				}
			}

		Free();
		}

	Geom.m_uChips   = m_Geom.m_uChips;
				
	Geom.m_uBlocks  = m_Geom.m_uPlaneSize * m_Geom.m_uDeviceSize;

	Geom.m_uPages   = m_Geom.m_uBlockSize;

	Geom.m_uBytes   = 512;

	Geom.m_uSectors = m_Geom.m_uPageSize / Geom.m_uBytes;

	return TRUE;
	}

BOOL METHOD CNand51::GetUniqueId(UINT uChip, PBYTE pData)
{
	Lock();

	if( AtomicCmd(uChip, 0xED, 10) ) {
	
		AtomicAddr(uChip, 0);

		if( AtomicDataOut(uChip) ) {

			PCBYTE p = PCBYTE(m_pBuff);

			for( UINT i = 0; i < 16; i ++ ) {

				if( (p[i] ^ p[i + 16]) != 0xFF ) {

					Free();

					return FALSE;
					}
				}

			ArmMemCpy(pData, p, 16);

			Free();

			return TRUE;
			}
		}

	Free();
	
	return FALSE;
	}

BOOL METHOD CNand51::IsBlockBad(UINT uChip, UINT uBlock)
{
	Lock();

	if( AtomicReadPage(uChip, uBlock, 0) ) {

		if( PBYTE(m_pBuff)[0x1000] == 0x00 ) {

			Free();
			
			return TRUE;
			}

		Free();

		return FALSE;
		}

	Free();

	return TRUE;
	}

BOOL METHOD CNand51::MarkBlockBad(UINT uChip, UINT uBlock)
{
	Lock();

	m_pBuff[ 0x1000 / sizeof(DWORD) ] = 0x000000;
	
	if( AtomicProgPage(0, uBlock, 0) ) {
		
		Free();

		return TRUE;
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand51::EraseBlock(UINT uChip, UINT uBlock)
{
	Lock();

	if( AtomicEraseBlock(uChip, uBlock) ) {

		Free();
		
		return TRUE;
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand51::WriteSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PCBYTE pData)
{
	Lock();

	if( AtomicReadPage(uChip, uBlock, uPage) ) {

		ArmMemCpy(PBYTE(m_pBuff) + uSect * 512, pData, 512);

		if( AtomicProgPage(uChip, uBlock, uPage) ) {

			Free();
			
			return TRUE;
			}
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand51::ReadSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PBYTE pData)
{
	Lock();

	if( AtomicReadPage(uChip, uBlock, uPage) ) {

		ArmMemCpy(pData, PBYTE(m_pBuff) + uSect * 512, 512);

		Free();

		return TRUE;
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand51::WritePage(UINT uChip, UINT uBlock, UINT uPage, PCBYTE pData)
{
	Lock();

	ArmMemCpy(PBYTE(m_pBuff), pData, m_Geom.m_uPageSize);	

	if( AtomicProgPage(uChip, uBlock, uPage) ) {

		Free();
		
		return TRUE;
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand51::ReadPage(UINT uChip, UINT uBlock, UINT uPage, PBYTE pData)
{
	Lock();

	if( AtomicReadPage(uChip, uBlock, uPage) ) {

		ArmMemCpy(pData, PBYTE(m_pBuff), m_Geom.m_uPageSize);	

		Free();

		return TRUE;
		}

	Free();

	return FALSE;
	}

// IEventSink

void CNand51::OnEvent(UINT uLine, UINT uParam)
{
	if( (Reg(Control) & Bit(31)) ) { 

		Reg(Control) &= ~Bit(31);

		m_pDone->Set();
		}
	}

// Implementation

void CNand51::Lock(void)
{
	m_pLock->Wait(FOREVER);
	}

void CNand51::Free(void)
{
	m_pLock->Free();

	CheckThreadCancellation();
	}

void CNand51::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);
	}

void CNand51::DisableEvents(void)
{
	phal->EnableLine(m_uLine, false);
	}

void CNand51::InitController(void)
{
	LockRegisters(true);

	Reg(Config2)	= ( 0x70 << 24 )  |
			  ( 0x20 << 16 )  |
			  (    0 << 15 )  |
			  (    1 << 14 )  |
			  (    2 << 12 )  |
			  (    0 <<  9 )  |
			  (    1 <<  7 )  |
			  (    0 <<  6 )  |
			  (    1 <<  5 )  |
			  (    1 <<  4 )  |
			  (    1 <<  3 )  |
			  (    0 <<  2 )  |
			  (    1 <<  0 )  ;
	
	UINT uNum = m_Geom.m_uChips - 1;

	Reg(Config3)	= (    1 << 20 )  |
			  (    0 << 16 )  |
			  (    1 << 15 )  |
			  ( uNum << 12 )  |
			  (    0 << 11 )  |
			  (    6 <<  8 )  |
			  (    0 <<  4 )  |
			  (    1 <<  3 )  |
			  (    0 <<  2 )  |
			  (    0 <<  0 )  ;

	Reg(AddrUnlock) = 0xFFFF0000;
	
	Reg(WriteProt)	= ( 2    <<  6 )  |
			  ( 0    <<  3 )  |
			  ( 4    <<  0 )  ;

	LockRegisters(false);

	Axi(Config)     = Bit(2); 
	}

void CNand51::LockRegisters(bool fLock)
{
	if( fLock ) {

		Reg(Control) |= Bit(0);

		while( !(Reg(Control) & Bit(1)) );
		}
	else {
		Reg(Control) &= ~Bit(0);
		}
	}

// Commands

bool CNand51::Reset(UINT uChip)
{
	Axi(Config) |= Bit(2); 

	return AtomicCmd(uChip, 0xFF, 100);
	}

bool CNand51::AutoReadPage(UINT uBlock, UINT uPage)
{
	Axi(Command)  = MAKEWORD(0x00, 0x30);

	Axi(Address0) = (uBlock * m_Geom.m_uBlockSize + uPage) << 16;

	Axi(Address8) = (uBlock * m_Geom.m_uBlockSize + uPage) >> 16;

	Axi(Config)   = 0;

	Axi(Launch)   = cmdAutoRead;

	if( WaitDone(0, 10) ) {

		UINT uChip = 0;

		if( Axi(StatusSum) & Bit(uChip + 8) ) {

			Axi(StatusSum) &= ~Bit(uChip + 8);

			return false;			
			}		
			
		return true;
		}

	return false;
	}

bool CNand51::AutoProgPage(UINT uBlock, UINT uPage)
{
	Axi(Command)  = MAKEWORD(0x80, 0x10);

	Axi(Address0) = (uBlock * m_Geom.m_uBlockSize + uPage) << 16;

	Axi(Address8) = (uBlock * m_Geom.m_uBlockSize + uPage) >> 16;

	Axi(Config)   = 0;

	Axi(Launch)   = cmdAutoProg;

	return WaitDone(0, 100);
	}

bool CNand51::AutoEraseBlock(UINT uBlock)
{
	Axi(Command)  = MAKEWORD(0x60, 0xD0);

	Axi(Address0) = uBlock * m_Geom.m_uBlockSize;

	Axi(Launch)   = cmdAutoErase;

	return WaitDone(0, 100);
	}

bool CNand51::AtomicReadId(UINT uChip)
{
	if( AtomicCmd(uChip, 0x90, 10) ) {
	
		AtomicAddr(uChip, 0);
	
		if( AtomicDataId(uChip) ) {
		
			return true;
			}
		}

	return false;
	}

bool CNand51::AtomicReadPage(UINT uChip, UINT uBlock, UINT uPage)
{
	DWORD dwAddr = uBlock * m_Geom.m_uBlockSize + uPage;
	
	if( !AtomicCmd(uChip, 0x00, 10) ) {

		return false;
		}

	if( !AtomicAddr(uChip, 0) || !AtomicAddr(uChip, 0) ) {

		return false;
		}

	for( UINT r = 0; r < 3; r ++ ) {

		if( AtomicAddr(uChip, dwAddr & 0xFF) ) {

			dwAddr >>= 8;

			continue;
			}

		return false;
		}

	if( !AtomicCmd(uChip, 0x30, 10) ) {

		return false;
		}

	WaitReady();

	if( AtomicDataOut(uChip) ) {

		if( Axi(StatusSum) & Bit(uChip + 8) ) {

			Axi(StatusSum) &= ~Bit(uChip + 8);

			AfxTrace("nand: ECC fail (%d.%d)\n", uBlock, uPage);
			
			return false;
			}

		return TRUE;
		}

	return true;
	}

bool CNand51::AtomicProgPage(UINT uChip, UINT uBlock, UINT uPage)
{
	DWORD dwAddr = uBlock * m_Geom.m_uBlockSize + uPage;
	
	if( !AtomicCmd(uChip, 0x80, 10) ) {

		return false;
		}

	if( !AtomicAddr(uChip, 0) || !AtomicAddr(uChip, 0) ) {

		return false;
		}

	for( UINT r = 0; r < 3; r ++ ) {

		if( AtomicAddr(uChip, dwAddr & 0xFF) ) {

			dwAddr >>= 8;

			continue;
			}

		return false;
		}

	if( !AtomicDataIn(uChip) ) {

		return false;
		}
	
	if( !AtomicCmd(uChip, 0x10, 10) ) {

		return false;
		}

	WaitReady();

	if( AtomicReadStatus(uChip) ) {

		BYTE bStatus = (Axi(Config) >> 16) & 0xFF;
		
		return !(bStatus & Bit(0));
		}

	return false;
	}

bool CNand51::AtomicEraseBlock(UINT uChip, UINT uBlock)
{
	memset(PBYTE(m_pBuff), 0x00, m_Geom.m_uPageSize);	

	WaitReady();

	if( !AtomicCmd(uChip, 0x60, 10) ) {

		return false;
		}

	DWORD dwAddr = uBlock * m_Geom.m_uBlockSize;
	
	for( UINT n = 0; n < 3; n ++ ) {

		if( AtomicAddr(uChip, dwAddr & 0xFF) ) {

			dwAddr >>= 8;

			continue;
			}

		return false;
		}

	if( !AtomicCmd(uChip, 0xD0, 10) ) {

		return false;
		}

	WaitReady();

	if( AtomicReadStatus(uChip) ) {

		BYTE bStatus = (Axi(Config) >> 16) & 0xFF;

		return !(bStatus & Bit(0));
		}

	return false;
	}

bool CNand51::AtomicReadStatus(UINT uChip)
{
	if( AtomicCmd(uChip, 0x70, 10) ) {
		
		return AtomicDataStatus(uChip);
		}

	return false;
	}

void CNand51::WaitReady(void)
{
	while( !(Reg(Control) & Bit(28)) );
	}

bool CNand51::WaitDone(UINT uChip, UINT uWait)
{
	if( m_pDone->Wait(uWait) ) {

		return (Axi(StatusSum) & Bit(uChip)) == 0;
		}

	return false;
	}

// Atomic 

bool CNand51::AtomicCmd(UINT uChip, BYTE bCmd, UINT uWait)
{
	Axi(Command) = bCmd;

	Axi(Config)  = uChip << 12;

	Axi(Launch)  = cmdCmdIn;

	return WaitDone(uChip, uWait);
	}

bool CNand51::AtomicAddr(UINT uChip, DWORD dwAddr)
{
	Axi(Address0) = dwAddr;

	Axi(Config)   = uChip << 12;

	Axi(Launch)   = cmdAddrIn;

	return WaitDone(uChip, 10);
	}

bool CNand51::AtomicDataIn(UINT uChip)
{
	Axi(Config) = uChip << 12;

	Axi(Launch) = cmdDataIn; 

	return WaitDone(uChip, 100);
	}

bool CNand51::AtomicDataOut(UINT uChip)
{
	Axi(Config) = uChip << 12;

	Axi(Launch) = cmdDataOutPage; 

	return WaitDone(uChip, 100);
	}

bool CNand51::AtomicDataId(UINT uChip)
{
	Axi(Config) = uChip << 12;

	Axi(Launch) = cmdDataOutId; 

	return WaitDone(uChip, 10);
	}

bool CNand51::AtomicDataStatus(UINT uChip)
{
	Axi(Config) = uChip << 12;

	Axi(Launch) = cmdDataOutStat; 

	return WaitDone(uChip, 10);
	}

// End of File
