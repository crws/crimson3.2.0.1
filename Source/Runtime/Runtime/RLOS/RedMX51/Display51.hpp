
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Display51_HPP
	
#define	INCLUDE_Display51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

///////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DisplaySoftPoint.hpp"

///////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpu51;
class CPwm51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Display Driver
//

class CDisplay51 : public CDisplaySoftPoint
{	
	public:
		// Constructor
		CDisplay51(UINT uModel, CIpu51 *pIpu, CPwm51 *pPwm);

		// Destructor
		~CDisplay51(void);

		// IDevice
		BOOL METHOD Open(void);

		// IDisplay
		void METHOD Update(PCVOID pData);
		BOOL METHOD SetBacklight(UINT pc);
		UINT METHOD GetBacklight(void);
		BOOL METHOD EnableBacklight(BOOL fOn);
		BOOL METHOD IsBacklightEnabled(void);

	protected:
		// Data Members
		IIdentity      * m_pIdentity;
		CIpu51         * m_pIpu;
		IGpio          * m_pGpioBacklight;
		UINT             m_uLineBacklight;
		IGpio          * m_pGpioDisplayOn;
		UINT             m_uLineDisplayOn;
		CPwm51         * m_pPwmBacklight;
		bool	         m_fBacklight;
		int              m_nBacklight;
		UINT             m_uType;

		// Implementation
		void FindDisplay(void);
	};

// End of File

#endif
