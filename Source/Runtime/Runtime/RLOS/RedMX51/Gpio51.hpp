
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Gpio51_HPP
	
#define	INCLUDE_Gpio51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 GPIO Unit
//

class CGpio51 : public IGpio, public IEventSink
{
	public:
		// Constructor
		CGpio51(UINT iIndex);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IGpio
		void METHOD SetDirection(UINT n, BOOL fOutput);
		void METHOD SetState(UINT n, BOOL fState);
		BOOL METHOD GetState(UINT n);
		void METHOD EnableEvents(void);
		void METHOD SetIntHandler(UINT n, UINT uType, IEventSink *pSink, UINT uParam);
		void METHOD SetIntEnable(UINT n, BOOL fEnable);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Registers
		enum
		{
			regDR   = 0x0000 / sizeof(DWORD),
			regGDIR = 0x0004 / sizeof(DWORD),
			regPSR  = 0x0008 / sizeof(DWORD),
			regICR1 = 0x000C / sizeof(DWORD),
			regICR2 = 0x0010 / sizeof(DWORD),
			regIMR  = 0x0014 / sizeof(DWORD),
			regISR  = 0x0018 / sizeof(DWORD),
			regEDR  = 0x001C / sizeof(DWORD),
			};

		// Sink Record
		struct CSink
		{
			IEventSink * m_pSink;
			UINT	     m_uParam;
			};
		
		// Data Members
		ULONG     m_uRefs;
		PVDWORD   m_pBase;
		UINT      m_uLine;
		CSink     m_Sink[32];

		// Implementation
		DWORD FindBase(UINT iIndex) const;
		UINT  FindLine(UINT iIndex) const;
	};

// End of File

#endif
