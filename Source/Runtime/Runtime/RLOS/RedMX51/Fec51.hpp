
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2013 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_iMX51_Fec51_HPP
	
#define	INCLUDE_iMX51_Fec51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Fast Ethernet Controller
//

class CFec51 : public INic, public IEventSink, public IDiagProvider
{
	public:
		// Constructor
		CFec51(void);

		// Destructor
		~CFec51(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// INic
		bool METHOD Open(bool fFast, bool fFull);
		bool METHOD Close(void);
		bool METHOD InitMac(MACADDR const &Addr);
		void METHOD ReadMac(MACADDR &Addr);
		UINT METHOD GetCapabilities(void);
		void METHOD SetFlags(UINT uFlags);
		bool METHOD SetMulticast(MACADDR const *pList, UINT uList);
		bool METHOD IsLinkActive(void);
		bool METHOD WaitLink(UINT uTime);
		bool METHOD SendData(CBuffer *  pBuff, UINT uTime);
		bool METHOD ReadData(CBuffer * &pBuff, UINT uTime);
		void METHOD GetCounters(NICDIAG &Diag);
		void METHOD ResetCounters(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// IDiagProvider
		UINT RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Registers
		enum
		{
			regEIR		= 0x0004 / sizeof(DWORD),
			regEIMR		= 0x0008 / sizeof(DWORD),
			regRDAR		= 0x0010 / sizeof(DWORD),
			regTDAR		= 0x0014 / sizeof(DWORD),
			regECR		= 0x0024 / sizeof(DWORD),
			regMMFR		= 0x0040 / sizeof(DWORD),
			regMSCR		= 0x0044 / sizeof(DWORD),
			regMIBC		= 0x0064 / sizeof(DWORD),
			regRCR		= 0x0084 / sizeof(DWORD),
			regTCR		= 0x00C4 / sizeof(DWORD),
			regPALR		= 0x00E4 / sizeof(DWORD),
			regPAUR		= 0x00E8 / sizeof(DWORD),
			regOPD		= 0x00EC / sizeof(DWORD),
			regIAUR		= 0x0118 / sizeof(DWORD),
			regIALR		= 0x011C / sizeof(DWORD),
			regGAUR		= 0x0120 / sizeof(DWORD),
			regGALR		= 0x0124 / sizeof(DWORD),
			regTFWR		= 0x0144 / sizeof(DWORD),
			regFRBR		= 0x014C / sizeof(DWORD),
			regFRSR		= 0x0150 / sizeof(DWORD),
			regERDSR	= 0x0180 / sizeof(DWORD),
			regETDSR	= 0x0184 / sizeof(DWORD),
			regEMRBR	= 0x0188 / sizeof(DWORD),
			};

		// Interrupt Flags
		enum
		{
			intUn		= Bit(19),
			intRL		= Bit(20),
			intLC		= Bit(21),
			intEBErr	= Bit(22),
			intMii		= Bit(23),
			intRxB		= Bit(24),
			intRxF		= Bit(25),
			intTxB		= Bit(26),
			intTxF		= Bit(27),
			intGra		= Bit(28),
			intBabT		= Bit(29),
			intBabR		= Bit(30),
			intHbErr	= Bit(31),
			};

		// Rx Buffer Control Flags
		enum
		{
			rxTrunc		= Bit(0), 
			rxOverrun	= Bit(1),
			rxBadCrc	= Bit(2),
			rxBadAlign	= Bit(4),
			rxBadLen	= Bit(5),
			rxMulti		= Bit(6),
			rxBroad		= Bit(7),
			rxMiss		= Bit(8),
			rxEnd		= Bit(11),
			rxUsr2		= Bit(12),
			rxWrap		= Bit(13),
			rxUsrTouch	= Bit(14),
			rxEmpty		= Bit(15),
			rxError		= rxBadLen | rxBadAlign | rxBadCrc | rxOverrun | rxTrunc,
			};

		// Tx Buffer Control Flags
		enum 
		{
			txBad		= Bit(9),
			txCrc		= Bit(10),
			txEnd		= Bit(11),
			txUsr2		= Bit(12),
			txWrap		= Bit(13),
			txUsr1		= Bit(14),
			txRdy		= Bit(15),
			};

		// MII Management Frame Bits
		enum
		{
			frameTA		= 16,
			frameRA		= 18,
			framePA		= 23,
			frameOP		= 28,
			frameST		= 30,
			};

		// Nic States
		enum
		{
			nicInitial,
			nicClosed,
			nicOpen,
			nicNegotiate,
			nicActive,
			};

		// Phy States
		enum
		{
			phyIdle,
			phyGetIsr,
			phyGetLink,
			};

		// Mii States
		enum
		{
			miiIdle,
			miiAppPut,
			miiAppGet,
			miiIntPut,
			miiIntGet,
			};

		// Buffer Descriptor
		struct CBuffDesc
		{
			WORD  volatile m_wCount;
			WORD  volatile m_wCtrl;
			DWORD 	       m_pData;
			};

		// Buffer Layout
		enum
		{
			constTxLimit  = 16,
			constRxLimit  = 32,
			constBuffSize = 2048,
			};

		// Data Members
		PVDWORD	        m_pBase;
		ULONG           m_uRefs;
		UINT		m_uProv;
		UINT		m_uLine;
		IGpio         * m_pGpioPhyReset;
		UINT            m_uPhyResetLine;
		IGpio         * m_pGpioPhyInt;
		UINT            m_uPhyIntLine;
		MACADDR		m_MacAddr;
		MACADDR	      * m_pMulti;
		UINT		m_uMulti;
		NICDIAG		m_Diag;
		UINT            m_uFlags;
		IEvent        * m_pLinkOkay;
		IEvent        * m_pLinkDown;
		bool		m_fAllowFull;
		bool		m_fAllowFast;
		bool		m_fUsingFull;
		bool		m_fUsingFast;
		UINT		m_uNicState;
		UINT            m_uMiiState;
		UINT	        m_uPhyState;
		IEvent        * m_pMiiFlag;
		CBuffDesc     * m_pTxDesc;
		CBuffDesc     * m_pRxDesc;
		PBYTE	        m_pTxBuff;
		PBYTE	        m_pRxBuff;
		ISemaphore    * m_pTxFlag;
		ISemaphore    * m_pRxFlag;
		UINT            m_uTxHead;
		UINT            m_uTxTail;
		UINT            m_uRxHead;
		UINT            m_uRxTail;

		// Implementation
		void EnableEvents(void);
		void DisableEvents(void);

		// Mac Management
		void ResetState(void);
		void ResetController(void);
		void ConfigController(void);
		void LoadMultiFilter(void);
		void StartController(void);
		void StopController(void);
		void OnSendEvent(void);
		void OnRecvEvent(void);
		void AddMac(PBYTE pData);
		UINT GetMacHash(MACADDR const &Mac);
		bool TakeFrame(PBYTE pData);

		// Phy Management
		void PhyInterrupt(bool fEnable);
		void ResetPhy(bool fAssert);
		void ConfigPhy(void);
		void OnMiiEvent(void);
		void OnPhyEvent(void);
		void OnLinkUp(void);
		void OnLinkDown(void);

		// Buffer Management
		void AllocBuffers(void);
		void ResetBuffers(void);
		void FreeBuffers (void);
		void StartRx(CBuffDesc *pBuff);
		void StartTx(CBuffDesc *pBuff);

		// Mii Register Access
		BOOL AppPutMii(BYTE bAddr, WORD wData);
		WORD AppGetMii(BYTE bAddr);
		void IntPutMii(BYTE bAddr, WORD wData);
		void IntGetMii(BYTE bAddr);

		// Diagnostics
		bool DiagRegister(void);
		bool DiagRevoke(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
