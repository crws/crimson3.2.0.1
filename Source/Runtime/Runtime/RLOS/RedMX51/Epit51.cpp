
#include "Intern.hpp"

#include "Epit51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define Reg(x) (m_pBase[reg##x])

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Enhanced Periodic Interval Timer
//

// Constructor

CEpit51::CEpit51(IPic *pPic)
{
	m_pPic    = pPic;

	m_pBase   = PVDWORD(ADDR_EPIT1);

	m_uLine   = INT_EPIT1;

	m_uClock  = 66500000;

	m_uPrediv = 4;

	m_pSink   = NULL;
	}

// IEventSink

void CEpit51::OnEvent(UINT uLine, UINT uParam)
{
	Reg(Status) = 1;

	if( m_pSink ) {

		m_pSink->OnEvent(uLine, m_uParam);
		}
	}

// Attributes

UINT CEpit51::GetFraction(void)
{
	if( !(Reg(Status) & 1) ) {

		UINT uRead = Reg(Counter);

		UINT uTime = (m_uLoad - uRead) * (m_uPeriod * 1000) / m_uLoad;

		return uTime;
		}

	return m_uPeriod * 1000;
	}

// Operations

void CEpit51::SetEventHandler(IEventSink *pSink, UINT uParam)
{
	m_pSink  = pSink;

	m_uParam = uParam;
	}

void CEpit51::SetPeriodic(UINT uPeriod)
{
	m_uPeriod    = uPeriod;

	m_uLoad      = FindLoadValue(uPeriod);

	Reg(Control) = 0x00010000;

	Reg(Control) = 0x0100000E | ((m_uPrediv - 1) << 4);

	Reg(Load)    = m_uLoad;

	Reg(Compare) = 0;

	Reg(Control) = 0x0100000F | ((m_uPrediv - 1) << 4);
	}

void CEpit51::EnableEvents(void)
{
	m_pPic->SetLineHandler(m_uLine, this, 0);

	m_pPic->EnableLine    (m_uLine, true);
	}

// Implementation

UINT CEpit51::FindLoadValue(UINT uPeriod)
{
	INT64 uClock    = m_uClock;

	INT64 uPrescale = m_uPrediv;

	INT64 uLoad     = (uPeriod * uClock) / (1000 * uPrescale);

	return UINT(uLoad);
	}

// End of File
