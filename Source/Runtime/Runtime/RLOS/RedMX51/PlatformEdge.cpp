
#include "Intern.hpp"

#include "PlatformEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IoMux51.hpp"

#include "Dpll51.hpp"

#include "Ccm51.hpp"

#include "Pmui51.hpp"

#include "Pic12F.hpp"

#include "Pic/Pic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Identifiers
//

#include "..\..\HXLs\HxUsb\UsbIdentifiers.hpp"

#include "..\..\HXLs\HxUsb\Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice    * Create_Uart51(UINT i, CCcm51 *pCcm);
extern IDevice    * Create_Mr25Hxx(UINT uChan);
extern IDevice    * Create_Nand51(CCcm51 *pCcm);
extern IDevice    * Create_SdHost51(CCcm51 *pCcm);
extern IDevice    * Create_M24LR(void);
extern IDevice    * Create_RtcAbmc(void);
extern IDevice    * Create_Nic51(void);
extern IDevice    * Create_Identity(UINT uAddr);
extern IDevice    * Create_Leds(ILeds *pLeds);
extern IDevice    * Create_InputSwitch(IInputSwitch *pInput);
extern IDevice    * Create_BatteryMonitor51(CPmui51 *pPmui);
extern IUsbDriver * Create_UsbHost51(UINT iIndex);
extern IUsbDriver * Create_UsbFunc51(UINT iIndex);
extern IUsbDriver * Create_UsbHostEnhanced(void);
extern IUsbDriver * Create_UsbHostController(void);
extern IDevice	  * Create_UsbNetwork(IUsbHostFuncDriver *pDriver);

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Edge Controller
//

// Instantiator

IDevice * Create_PlatformEdge(UINT uModel)
{
	return (IDevice *) (CPlatformMX51 *) New CPlatformEdge(uModel);
}

// Constructor

CPlatformEdge::CPlatformEdge(UINT uModel)
{
	m_uModel     = uModel;

	m_nEnable    = 0;

	m_uRackLoad  = 43 + 54;

	m_uRackLimit = GetPortCount() * 43;

	m_pHub       = NULL;
}

// Destructor

CPlatformEdge::~CPlatformEdge(void)
{
	piob->RevokeGroup("dev.");
}

// IUnknown

HRESULT CPlatformEdge::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IPortSwitch);

	StdQueryInterface(IInputSwitch);

	StdQueryInterface(ILeds);

	StdQueryInterface(IUsbSystem);

	StdQueryInterface(IUsbSystemPortMapper);

	StdQueryInterface(IUsbSystemPower);

	return CPlatformMX51::QueryInterface(riid, ppObject);
}

ULONG CPlatformEdge::AddRef(void)
{
	return CPlatformMX51::AddRef();
}

ULONG CPlatformEdge::Release(void)
{
	return CPlatformMX51::Release();
}

// IDevice

BOOL CPlatformEdge::Open(void)
{
	if( CPlatformMX51::Open() ) {

		#if !defined(_DEBUG)

		piob->RegisterSingleton("dev.uart", 0, Create_Uart51(0, m_pCcm));

		#endif

		piob->RegisterSingleton("dev.uart", 1, Create_Uart51(1, m_pCcm));

		piob->RegisterSingleton("dev.uart", 2, Create_Uart51(2, m_pCcm));

		piob->RegisterSingleton("dev.nand", 0, Create_Nand51(m_pCcm));

		piob->RegisterSingleton("dev.fram", 0, Create_Mr25Hxx(1));

		piob->RegisterSingleton("dev.ident", 0, Create_Identity(addrIdentity));

		piob->RegisterSingleton("dev.sdhost", 0, Create_SdHost51(m_pCcm));

		piob->RegisterSingleton("dev.nic", 0, Create_Nic51());

		piob->RegisterSingleton("dev.leds", 0, Create_Leds(this));

		piob->RegisterSingleton("dev.input-s", 0, Create_InputSwitch(this));

		piob->RegisterSingleton("dev.rtc", 0, Create_RtcAbmc());

		piob->RegisterSingleton("dev.batt", 0, Create_BatteryMonitor51(m_pPmui));

		piob->RegisterSingleton("dev.usbctrl-d", 0, Create_UsbFunc51(0));

		piob->RegisterSingleton("dev.usbctrl-h", 0, Create_UsbHost51(1));

		piob->RegisterSingleton("dev.usbctrl-h", 1, Create_UsbHost51(2));

		piob->RegisterSingleton("dev.usbctrl-e", 0, Create_UsbHostEnhanced());

		piob->RegisterSingleton("dev.usbctrl-e", 1, Create_UsbHostEnhanced());

		piob->RegisterSingleton("dev.usbctrl-c", 0, Create_UsbHostController());

		piob->RegisterSingleton("dev.usbctrl-c", 1, Create_UsbHostController());

		InitUarts();

		InitUsb();

		InitNicMac(0);

		FindSerial();

		ProgramPic(0);

		ProgramPic(1);

		RegisterRedCommon();

		InitRack();

		return TRUE;
	}

	return FALSE;
}

// ILeds

void METHOD CPlatformEdge::SetLed(UINT uLed, UINT uState)
{
	UINT iSel = HIBYTE(uLed);

	UINT iLed = LOBYTE(uLed);

	if( iSel == 0 ) {

		switch( iLed ) {

			case 1:
				m_pPmui->SetLed(2, uState == stateOff ? 0 : 100);

				break;

			case 2:
				m_pPmui->SetLed(0, uState == stateOff ? 0 : 100);

				break;
		}
	}
}

void METHOD CPlatformEdge::SetLedLevel(UINT uPercent, bool fPersist)
{
}

UINT METHOD CPlatformEdge::GetLedLevel(void)
{
	return 100;
}

// IInputSwitch

UINT METHOD CPlatformEdge::GetSwitches(void)
{
	return 0xFFFFFFFE | GetSwitch(0);
}

UINT METHOD CPlatformEdge::GetSwitch(UINT uSwitch)
{
	if( uSwitch == 0 ) {

		return m_pGpio[0]->GetState(6) ? 1 : 0;
	}

	return 1;
}

// IPortSwitch

UINT METHOD CPlatformEdge::GetCount(UINT uUnit)
{
	switch( uUnit ) {

		case 0:	return 1;

		case 1:	return 1;

		case 2:	return 1;
	}

	return 0;
}

UINT METHOD CPlatformEdge::GetMask(UINT uUnit)
{
	switch( uUnit ) {

		case 0:	return
			(1 << physicalRS232);

		case 1:	return
			(1 << physicalRS485)       |
			(1 << physicalRS422Master) |
			(1 << physicalRS422Slave);

		case 2:	return
			(1 << physicalRS485)	   |
			(1 << physicalRS422Master) |
			(1 << physicalRS422Slave);
	}

	return physicalNone;
}

UINT METHOD CPlatformEdge::GetType(UINT uUnit, UINT uLog)
{
	switch( uUnit ) {

		case 0:
			return physicalRS232;

		case 1:
			return physicalRS485;

		case 2:
			return physicalRS485;
	}

	return physicalNone;
}

BOOL METHOD CPlatformEdge::EnablePort(UINT uUnit, BOOL fEnable)
{
	if( fEnable ) {

		if( AtomicIncrement(&m_nEnable) == 1 ) {

			m_pGpio[0]->SetState(24, true);
		}
	}
	else {
		if( AtomicDecrement(&m_nEnable) == 0 ) {

			m_pGpio[0]->SetState(24, false);
		}
	}

	if( uUnit == 1 ) {

		if( !fEnable ) {

			m_pGpio[3]->SetState(20, false);
		}

		return true;
	}

	if( uUnit == 2 ) {

		if( !fEnable ) {

			m_pGpio[0]->SetState(5, false);
		}

		return true;
	}

	return false;
}

BOOL METHOD CPlatformEdge::SetPhysical(UINT uUnit, BOOL fRS485)
{
	return FALSE;
}

BOOL METHOD CPlatformEdge::SetFull(UINT uUnit, BOOL fFull)
{
	if( uUnit == 1 ) {

		m_pGpio[2]->SetState(1, !fFull);

		return true;
	}

	if( uUnit == 2 ) {

		m_pGpio[0]->SetState(3, !fFull);

		return true;
	}

	return false;
}

BOOL METHOD CPlatformEdge::SetMode(UINT uUnit, BOOL fAuto)
{
	if( uUnit == 1 ) {

		m_pGpio[3]->SetState(20, fAuto);

		return true;
	}

	if( uUnit == 2 ) {

		m_pGpio[0]->SetState(5, fAuto);

		return true;
	}

	return false;
}

// IUsbSystem

void METHOD CPlatformEdge::OnDeviceArrival(IUsbHostFuncDriver *pDriver)
{
	IUsbDevice *pDev = NULL;

	pDriver->GetDevice(pDev);

	UsbTreePath const &Path = pDev->GetTreePath();

	if( GetPortType(Path) == typeSystem ) {

		if( pDriver->GetClass() == devHub ) {

			m_pHub = (IUsbHubDriver *) pDriver;

			UsbHubLedMap Map = {

				{ 0, MAKEWORD(0,1), MAKEWORD(2,1), 0, 0, 0, 0, 0 },
				{ 0, MAKEWORD(1,1), MAKEWORD(3,1), 0, 0, 0, 0, 0 },
			};

			m_pHub->SetLedMap(Map);

			return;
		}

		if( pDriver->GetVendor() == vidSmc && pDriver->GetProduct() == pidSmcLan9512 ) {

			piob->RegisterSingleton("dev.nic", 1, Create_UsbNetwork(pDriver));

			InitNicMac(1);

			return;
		}
	}

	if( GetPortType(Path) == typeOption ) {

		return;
	}

	if( GetPortType(Path) == typeExtern ) {

		return;
	}
}

void METHOD CPlatformEdge::OnDeviceRemoval(IUsbHostFuncDriver *pDriver)
{
}

// IUsbSystemPortMapper

UINT METHOD CPlatformEdge::GetPortCount(void)
{
	return 2;
}

UINT METHOD CPlatformEdge::GetPortType(UsbTreePath const &Path)
{
	if( Path.a.dwCtrl == 0 && Path.a.dwHost == 0 && Path.a.dwPort1 == 0 ) {

		return typeExtern;
	}

	if( Path.a.dwCtrl == 1 && Path.a.dwHost == 0 && Path.a.dwPort1 == 0 ) {

		if( Path.a.dwTier == 1 ) {

			return typeSystem;
		}

		if( Path.a.dwTier == 2 ) {

			switch( Path.a.dwPort2 ) {

				case 1:
					return typeSystem;

				case 2:
				case 3:
					return typeExtern;

				case 4:
				case 5:
					return typeOption;
			}
		}

		return typeUnknown;
	}

	return typeUnknown;
}

UINT METHOD CPlatformEdge::GetPortReset(UsbTreePath const &Path)
{
	return NOTHING;
}

// IUsbSystemPower

void METHOD CPlatformEdge::OnNewDevice(UsbTreePath const &Path, UINT uPower)
{
	if( GetPortType(Path) == typeOption ) {

		m_uRackLoad += uPower;

		if( m_uRackLoad > m_uRackLimit ) {

			m_pHub->ClrPortFeature(5, selPortPower);
		}
	}
}

void METHOD CPlatformEdge::OnDelDevice(UsbTreePath const &Path, UINT uPower)
{
	if( GetPortType(Path) == typeOption ) {

		if( m_uRackLoad > uPower ) {

			m_uRackLoad -= uPower;
		}
	}
}

// Initialisation

void CPlatformEdge::InitPriorities(void)
{
	CPlatformMX51::InitPriorities();

	phal->SetLinePriority(INT_UART1, 1);
	phal->SetLinePriority(INT_UART2, 1);
	phal->SetLinePriority(INT_UART3, 1);
	phal->SetLinePriority(INT_SDHC1, 1);
	phal->SetLinePriority(INT_FEC, 1);
	phal->SetLinePriority(INT_EMI_NFC, 1);
	phal->SetLinePriority(INT_USBOHOTG, 3);
	phal->SetLinePriority(INT_USBOH1, 4);
	phal->SetLinePriority(INT_USBOH2, 4);
}

void CPlatformEdge::InitClocks(void)
{
	m_pCcm->SetGating(true);

	m_pCcm->Set(CCcm51::clkUsbPhy, CCcm51::clkOsc, 1, 1);

	if( m_pCcm->GetFreq(CCcm51::clkPll1) != 600000000 ) {

		m_pCcm->Set(CCcm51::clkAxia, CCcm51::clkPll2, 4);
		m_pCcm->Set(CCcm51::clkAxib, CCcm51::clkPll2, 5);
		m_pCcm->Set(CCcm51::clkArmAxi, CCcm51::clkAxia);
		m_pCcm->Set(CCcm51::clkDdr, CCcm51::clkAxia, 4);

		m_pDpll[0]->Set(CDpll51::refOsc, 6, 45, 180, 1, true);

		for( UINT i = 0; i < 4000; i++ ) {

			ASM("nop\r\n");
		}

		m_pCcm->Set(CCcm51::clkArmRoot, CCcm51::clkPll1, 1);
		m_pCcm->Set(CCcm51::clkDdr, CCcm51::clkPll1, 3);
		m_pCcm->Set(CCcm51::clkSdhc1, CCcm51::clkPll1, 4, 3);
	}

	if( m_pCcm->GetFreq(CCcm51::clkPll2) != 665000000 ) {

		m_pCcm->Set(CCcm51::clkAxia, CCcm51::clkPll2, 5);
		m_pCcm->Set(CCcm51::clkAxib, CCcm51::clkPll2, 6);
		m_pCcm->Set(CCcm51::clkAhb, CCcm51::clkPll2, 6);
		m_pCcm->Set(CCcm51::clkAxia, CCcm51::clkPll1, 5);
		m_pCcm->Set(CCcm51::clkAxib, CCcm51::clkPll1, 6);
		m_pCcm->Set(CCcm51::clkAhb, CCcm51::clkPll1, 6);

		m_pDpll[1]->Set(CDpll51::refOsc, 6, 89, 96, 1, true);

		for( UINT i = 0; i < 4000; i++ ) {

			ASM("nop\r\n");
		}

		m_pCcm->Set(CCcm51::clkAxia, CCcm51::clkPll2, 4);
		m_pCcm->Set(CCcm51::clkAxib, CCcm51::clkPll2, 5);
		m_pCcm->Set(CCcm51::clkAhb, CCcm51::clkPll2, 5);
		m_pCcm->Set(CCcm51::clkEmi, CCcm51::clkPll2, 5);
		m_pCcm->Set(CCcm51::clkIpg, CCcm51::clkPll2, 2);
		m_pCcm->Set(CCcm51::clkPer, CCcm51::clkPll2, 2, 6);
		m_pCcm->Set(CCcm51::clkGpu2, CCcm51::clkAxib);
		m_pCcm->Set(CCcm51::clkArmAxi, CCcm51::clkAxia);
		m_pCcm->Set(CCcm51::clkIpuHsp, CCcm51::clkAxib);
		m_pCcm->Set(CCcm51::clkGpu, CCcm51::clkAxib);
		m_pCcm->Set(CCcm51::clkVpu, CCcm51::clkAxib);
		m_pCcm->Set(CCcm51::clkUsboh3, CCcm51::clkPll2, 5, 2);
		m_pCcm->Set(CCcm51::clkEcspi, CCcm51::clkPll2, 5, 2);
		m_pCcm->Set(CCcm51::clkEnfc, CCcm51::clkEmi, 5);
	}

	if( m_pCcm->GetFreq(CCcm51::clkPll3) != 216000000 ) {

		m_pCcm->Set(CCcm51::clkUart, CCcm51::clkPll2, 4, 3);

		m_pDpll[2]->Set(CDpll51::refOsc, 9, 0, 1, 2, false);

		for( UINT i = 0; i < 4000; i++ ) {

			ASM("nop\r\n");
		}

		m_pCcm->Set(CCcm51::clkUart, CCcm51::clkPll3, 4, 1);
	}

	m_pCcm->SetFpm(CCcm51::fpmOff);

	m_pCcm->EnableAmps(false);

	m_pCcm->SetGating(false);
}

void CPlatformEdge::InitRails(void)
{
	m_pPmui->SetSw1(1000);

	m_pPmui->SetVGen1(0);

	m_pPmui->SetVSd(0);

	m_pPmui->SetVGen2(3150);

	m_pPmui->SetVGen3(1800);

	m_pPmui->SetVVid(2775);
}

void CPlatformEdge::InitMux(void)
{
	CPlatformMX51::InitMux();

	DWORD const muxUart[] = {

		CIoMux51::MUX_UART1_TXD,	0,
		CIoMux51::MUX_UART1_RXD,	0,
		CIoMux51::MUX_UART1_RTS,	0,
		CIoMux51::MUX_UART1_CTS,	0,
		CIoMux51::MUX_UART2_TXD,	0,
		CIoMux51::MUX_UART2_RXD,	0,
		CIoMux51::MUX_EIM_D26,		4,
		CIoMux51::MUX_EIM_D25,		4,
		CIoMux51::MUX_UART3_TXD,	1,
		CIoMux51::MUX_UART3_RXD,	1,
		CIoMux51::MUX_EIM_D27,		3,
		CIoMux51::MUX_EIM_D24,		3,

		CIoMux51::SEL_UART1_RX,		0,
		CIoMux51::SEL_UART1_RTS,	0,
		CIoMux51::SEL_UART2_RX,		2,
		CIoMux51::SEL_UART2_RTS,	3,
		CIoMux51::SEL_UART3_RX,		4,
		CIoMux51::SEL_UART3_RTS,	3,

		CIoMux51::PAD_UART1_TXD,	CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART1_RXD,	CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART1_CTS,	CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART1_RTS,	CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART2_TXD,	CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART2_RXD,	CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_D25,		CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_D26,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART3_TXD,	CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART3_RXD,	CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_D24,		CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_D27,		CIoMux51::PAD_FAST,
	};

	DWORD const muxPic[] = {

		CIoMux51::MUX_AUD3_BB_RXD,	3,
		CIoMux51::MUX_AUD3_BB_CK,	3,
		CIoMux51::MUX_AUD3_BB_FS,	3,
		CIoMux51::MUX_GPIO1_2,		0,
		CIoMux51::MUX_GPIO1_5,		0,
		CIoMux51::MUX_DI1_PIN13,	4,
	};

	DWORD const muxSdHost[] = {

		CIoMux51::MUX_SD1_CMD,		16,
		CIoMux51::MUX_SD1_CLK,		0,
		CIoMux51::MUX_SD1_DATA0,	0,
		CIoMux51::MUX_SD1_DATA1,	0,
		CIoMux51::MUX_SD1_DATA2,	0,
		CIoMux51::MUX_SD1_DATA3,	0,
		CIoMux51::MUX_GPIO1_0,		1,
		CIoMux51::MUX_GPIO1_1,		1,

		CIoMux51::PAD_SD1_CMD,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD1_CLK,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD1_DATA0,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD1_DATA1,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD1_DATA2,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD1_DATA3,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_GPIO1_0,		CIoMux51::PAD_HYST_ON | CIoMux51::PAD_PULL_UP_100K,
		CIoMux51::PAD_GPIO1_1,		CIoMux51::PAD_HYST_ON | CIoMux51::PAD_PULL_UP_100K,
	};

	DWORD const muxNic[] = {

		CIoMux51::MUX_EIM_EB2,		3,
		CIoMux51::MUX_EIM_EB3,		3,
		CIoMux51::MUX_EIM_CS2,		3,
		CIoMux51::MUX_EIM_CS3,		3,
		CIoMux51::MUX_EIM_CS4,		3,
		CIoMux51::MUX_EIM_CS5,		3,
		CIoMux51::MUX_NANDF_RB2,	1,
		CIoMux51::MUX_NANDF_RB3,	1,
		CIoMux51::MUX_NANDF_CS2,	2,
		CIoMux51::MUX_NANDF_CS3,	2,
		CIoMux51::MUX_NANDF_CS4,	2,
		CIoMux51::MUX_NANDF_CS5,	2,
		CIoMux51::MUX_NANDF_CS6,	2,
		CIoMux51::MUX_NANDF_CS7,	1,
		CIoMux51::MUX_NANDF_RDY_INT,	1,
		CIoMux51::MUX_NANDF_D8,		2,
		CIoMux51::MUX_NANDF_D9,		2,
		CIoMux51::MUX_NANDF_D11,	2,
		CIoMux51::MUX_EIM_A21,		1,
		CIoMux51::MUX_I2C1_CLK,		3,

		CIoMux51::PAD_EIM_EB2,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_EIM_EB3,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_CS2,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_CS3,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_CS4,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_CS5,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_NANDF_RB2,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V,
		CIoMux51::PAD_NANDF_RB3,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V,
		CIoMux51::PAD_NANDF_CS2,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_CS3,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_CS4,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_CS5,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_CS6,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_CS7,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_RDY_INT,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V,
		CIoMux51::PAD_NANDF_D8,		CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_D9,		CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V,
		CIoMux51::PAD_NANDF_D11,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V,
		CIoMux51::PAD_EIM_A21,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_I2C1_CLK,		CIoMux51::PAD_OPEN_DRAIN,
	};

	DWORD const muxPort[] = {

		CIoMux51::MUX_DI1_PIN12,	4,
		CIoMux51::MUX_AUD3_BB_CK,	3,
		CIoMux51::MUX_OWIRE_LINE,	3,
		CIoMux51::MUX_GPIO1_3,		0,
		CIoMux51::MUX_GPIO1_5,		0,

		CIoMux51::PAD_DI1_PIN12,	CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_AUD3_BB_CK,	CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_OWIRE_LINE,	CIoMux51::PAD_SLOW,
		CIoMux51::PAD_GPIO1_3,		CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_GPIO1_5,		CIoMux51::PAD_DRIVE_HI,
	};

	DWORD const muxSwitch[] = {

		CIoMux51::PAD_GPIO1_6,		CIoMux51::PAD_PULL_DN_100K,
	};

	DWORD const muxUsb[] = {

		CIoMux51::MUX_USBH1_CLK,	0,
		CIoMux51::MUX_USBH1_DIR,	0,
		CIoMux51::MUX_USBH1_STP,	0,
		CIoMux51::MUX_USBH1_NXT,	0,
		CIoMux51::MUX_USBH1_DATA0,	0,
		CIoMux51::MUX_USBH1_DATA1,	0,
		CIoMux51::MUX_USBH1_DATA2,	0,
		CIoMux51::MUX_USBH1_DATA3,	0,
		CIoMux51::MUX_USBH1_DATA4,	0,
		CIoMux51::MUX_USBH1_DATA5,	0,
		CIoMux51::MUX_USBH1_DATA6,	0,
		CIoMux51::MUX_USBH1_DATA7,	0,
		CIoMux51::MUX_GPIO1_7,		0,
		CIoMux51::MUX_NANDF_D12,	3,

		CIoMux51::MUX_EIM_A24,		2,
		CIoMux51::MUX_EIM_A25,		2,
		CIoMux51::MUX_EIM_A26,		2,
		CIoMux51::MUX_EIM_A27,		2,
		CIoMux51::MUX_EIM_D16,		2,
		CIoMux51::MUX_EIM_D17,		2,
		CIoMux51::MUX_EIM_D18,		2,
		CIoMux51::MUX_EIM_D19,		2,
		CIoMux51::MUX_EIM_D20,		2,
		CIoMux51::MUX_EIM_D21,		2,
		CIoMux51::MUX_EIM_D22,		2,
		CIoMux51::MUX_EIM_D23,		2,
		CIoMux51::MUX_EIM_A23,		1,

		CIoMux51::PAD_USBH1_CLK,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DIR,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_STP,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_NXT,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA0,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA1,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA2,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA3,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA4,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA5,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA6,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA7,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_GPIO1_7,		CIoMux51::PAD_SLOW | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_NANDF_D12,	CIoMux51::PAD_SLOW | CIoMux51::PAD_LO_V ,

		CIoMux51::PAD_EIM_A24,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_A25,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_A26,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_A27,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D16,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D17,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D18,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D19,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D20,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D21,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D22,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D23,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_A23,		CIoMux51::PAD_SLOW | CIoMux51::PAD_DRIVE_HI,
	};

	m_pMux->Set(muxUart, elements(muxUart));

	m_pMux->Set(muxPic, elements(muxPic));

	m_pMux->Set(muxSdHost, elements(muxSdHost));

	m_pMux->Set(muxNic, elements(muxNic));

	m_pMux->Set(muxPort, elements(muxPort));

	m_pMux->Set(muxSwitch, elements(muxSwitch));

	m_pMux->Set(muxUsb, elements(muxUsb));
}

void CPlatformEdge::InitGpio(void)
{
	CPlatformMX51::InitGpio();

	m_pGpio[0]->SetDirection(0, false);

	m_pGpio[0]->SetDirection(1, false);

	m_pGpio[0]->SetDirection(3, true);

	m_pGpio[0]->SetDirection(5, true);

	m_pGpio[0]->SetDirection(6, false);

	m_pGpio[0]->SetDirection(7, true);

	m_pGpio[0]->SetDirection(24, true);

	m_pGpio[1]->SetDirection(15, false);

	m_pGpio[1]->SetDirection(17, true);

	m_pGpio[2]->SetDirection(1, true);

	m_pGpio[2]->SetDirection(28, true);

	m_pGpio[3]->SetDirection(16, true);

	m_pGpio[3]->SetDirection(20, true);
}

void CPlatformEdge::InitUarts(void)
{
	for( UINT i = 0; i < 3; i++ ) {

		IPortObject *pPort = NULL;

		AfxGetObject("uart", i, IPortObject, pPort);

		if( pPort ) {

			pPort->Bind(this);

			pPort->Release();
		}
	}
}

void CPlatformEdge::InitUsb(void)
{
	m_pGpio[0]->SetState(7, true);

	m_pGpio[1]->SetState(17, false);

	m_pGpio[2]->SetState(28, false);

	Sleep(5);

	for( UINT i = 0; i < 2; i++ ) {

		IUsbDriver *p0, *p1, *p2;

		piob->GetObject("usbctrl-h", i, AfxAeonIID(IUsbDriver), (void **) &p0);

		piob->GetObject("usbctrl-e", i, AfxAeonIID(IUsbDriver), (void **) &p1);

		piob->GetObject("usbctrl-c", i, AfxAeonIID(IUsbDriver), (void **) &p2);

		p0->Bind(p1);

		p1->Bind(p2);

		p0->Release();

		p1->Release();

		p2->Release();
	}

	m_pGpio[0]->SetState(7, false);

	m_pGpio[2]->SetState(28, true);

	m_pGpio[1]->SetState(17, true);

	Sleep(50);
}

void CPlatformEdge::InitRack(void)
{
	IUsbHostStack    *pStack;

	IExpansionSystem *pExpSys;

	IUsbSystem       *pExpUsb;

	AfxGetObject("usb.host", 0, IUsbHostStack, pStack);

	AfxGetObject("usb.rack", 0, IExpansionSystem, pExpSys);

	AfxGetObject("usb.rack", 0, IUsbSystem, pExpUsb);

	if( pStack ) {

		pStack->Init();

		pStack->Attach((IUsbSystem *) this);

		pStack->Attach(pExpUsb);

		pExpSys->Attach((IUsbSystemPower *) this);
	}

	AfxRelease(pExpSys);

	AfxRelease(pStack);

	AfxRelease(pExpUsb);
}

void CPlatformEdge::ProgramPic(UINT uPic)
{
	UINT uPinM = uPic ? 66 : 117;

	UINT uPinC = uPic ? 5 : 116;

	UINT uPinD = uPic ? 3 : 115;

	CPic12F Pic(m_pGpio, uPinD, uPinC, uPinM);

	Pic.Unlock(true);

	if( Pic.ReadVer() != PIC_VER ) {

		Pic.Erase();

		Pic.Load(PIC_CODE);

		Pic.LoadVer(PIC_VER);
	}

	Pic.Unlock(false);
}

// End of File
