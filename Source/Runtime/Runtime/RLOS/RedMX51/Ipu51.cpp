
#include "Intern.hpp"

#include "Ipu51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IpuDc51.hpp"

#include "IpuDi51.hpp"

#include "IpuDmfc51.hpp"

#include "IpuIdmac51.hpp"

#include "IpuChanParam51.hpp"

#include "Ccm51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Image Processor Unit
//

// Register Access

#define Reg(x)		(m_pBase[reg##x])

#define RegN(x, n)	(m_pBase[reg##x + n])

// Constructor

CIpu51::CIpu51(CCcm51 *pCcm)
{
	m_pBase  = PVDWORD(ADDR_IPUEX);

	m_uLineS = INT_IPUEX_S; 

	m_uLineE = INT_IPUEX_E; 

	m_uType  = 0;

	m_uClock = pCcm->GetFreq(CCcm51::clkIpuHsp);

	MakeObjects();
	}

// Destructor

CIpu51::~CIpu51(void)
{
	FreeObjects();
	}

// Interface

void CIpu51::Configure(CDisplayConfig const &Config)
{
	m_Display = Config;

	m_uBpp    = 32;

	Init();
	}

void CIpu51::SetFrame(PBYTE pFrameBuf)
{
	m_dwFrame = phal->VirtualToPhysical(phal->GetNonCachedAlias(DWORD(pFrameBuf)));
	}

void CIpu51::Update(void)
{
	}

// Init

void CIpu51::Init(void)
{
	DisableModules();

	ResetMemory();

	InitCommon();

	InitDmfc();

	InitIdmac();

	InitCpmem();

	InitDp();

	ConfigDc();

	ConfigDi();

	ConfigCpmem();

	ConfigDmfc();

	ConfigIdmac();

	EnableFlow();
	}

void CIpu51::InitCommon(void)
{
	Reg(DispGen) = 0x00080000;

	Reg(General) = 0x00000000;
	}

void CIpu51::InitCpmem(void)
{
	m_pCpmem->ClearAll();

	m_pCpmem->SetBuffer(chanDpPrimaryMain, 0, m_dwFrame);
	}

void CIpu51::InitDmfc(void)
{
	}

void CIpu51::InitIdmac(void)
{
	SetDBuffMode(chanDpPrimaryMain, false);
	}

void CIpu51::InitDp(void)
{
	memset(PVOID(m_pBase + regSrm), 0, 280);
	}

void CIpu51::ConfigDc(void)
{
	InitDcChannels();

	InitDcDisplays();

	InitDcBusMapping();

	InitDcEvents();

	InitDcMicrocode();
	}

void CIpu51::ConfigDi(void)
{
	InitDiWaveforms();

	InitDiISync();

	InitDiHSync();

	InitDiVSync();

	InitDiActiveLine();

	InitDiActiveClock();

	InitDiActiveClock8();

	InitDiDataEnable();

	InitDiMisc();
	}

void CIpu51::ConfigCpmem(void)
{
	UINT uStride = m_Display.m_xSize * m_uBpp / 8;

	m_pCpmem->SetConfig(chanDpPrimaryMain, m_Display.m_xSize, m_Display.m_ySize, m_uBpp, uStride, false);
	}

void CIpu51::ConfigDmfc(void)
{
	m_pDmfc->SetConfig(chanDpPrimaryMain, m_Display.m_xSize, m_Display.m_ySize);
	}

void CIpu51::ConfigIdmac(void)
{
	m_pIdmac->SetChanWatermark(chanDpPrimaryMain, true);

	m_pIdmac->SetChanPriority(chanDpPrimaryMain, true);
	}

void CIpu51::EnableFlow(void)
{
	EnableModule(modDi0, true);

	EnableModule(modDp, true);

	EnableModule(modDc, true);

	EnableModule(modDmfc, true);

	ReleaseDiCounters();

	m_pIdmac->SetChanEnable(chanDpPrimaryMain, true);
	}

// Common

void CIpu51::DisableModules(void)
{
	Reg(Config) = 0x00000000;
	}

void CIpu51::EnableModules(void)
{
	m_pDc->Enable(true);

	m_pDi[0]->Enable(true);

	m_pDmfc->Enable(true);
	}

void CIpu51::ResetMemory(void)
{
	Reg(MemRst) = 0x007FFFFF;

	Reg(MemRst) |= Bit(31);

	while( Reg(MemRst) & Bit(31) );
	}

void CIpu51::EnableModule(UINT uMod, bool fEnable)
{
	if( fEnable ) {

		AtomicBitSet(&Reg(Config), uMod);
		}
	else {
		AtomicBitClr(&Reg(Config), uMod);
		}
	}

void CIpu51::ReleaseDiCounters(void)
{
	Reg(DispGen) |= Bit(24); 
	}

void CIpu51::SetDBuffMode(UINT uChan, BOOL fEnable)
{
	if( fEnable ) {
	
		RegN(ChDbMode0, uChan / 32) |= Bit(uChan % 32);
		}
	else {
		RegN(ChDbMode0, uChan / 32) &= ~Bit(uChan % 32);
		}
	}

// Display Interface

void CIpu51::InitDiWaveforms(void)
{
	CWaveformConfig Config;

	Config.m_uAccessSize = ((INT64) 1000000000 / m_Display.m_uFrequency) + 1;
	Config.m_uCompSize   = ((INT64) 1000000000 / m_Display.m_uFrequency) + 1; 
	Config.m_uChipSel    = asyncNouse;
	Config.m_uPin17	     = asyncNouse;
	Config.m_uPin16	     = asyncNouse;
	Config.m_uPin15	     = asyncReady;
	Config.m_uPin14	     = asyncNouse;
	Config.m_uPin13	     = asyncNouse;
	Config.m_uPin12	     = asyncNouse;
	Config.m_uPin11	     = asyncNouse;
	
	m_pDi[0]->SetWaveformConfig(waveformSync, Config);

	m_pDi[0]->SetDataWaveSet(waveformSync, asyncReady, 0, ((INT64)1000000000 * 2 / m_Display.m_uFrequency) + 1);

	m_pDi[0]->SetDataWaveSet(waveformSync, asyncNouse, 0, 0);
	}

void CIpu51::InitDiISync(void)
{
	UINT uRunH = m_Display.m_xSize      + 
		     m_Display.m_uHorzFront + 
		     m_Display.m_uHorzSync  + 
		     m_Display.m_uHorzBack  ;

	CSyncConfig Sync;

	Sync.m_uRun		 = uRunH-1;
	Sync.m_uRunTrig		 = syncPixel + 1;
	Sync.m_uOffset		 = 0;
	Sync.m_uOffsetTrig	 = 0;
	Sync.m_uAutoReload	 = 1;
	Sync.m_uClearSel	 = 0;
	Sync.m_uToggleTrigSel	 = 0;
	Sync.m_uPolarityEnable	 = 0;
	Sync.m_uPolarityClearSel = 0;
	Sync.m_uPosUp		 = 0;
	Sync.m_uPosDn		 = 0;
	Sync.m_uStepRepeat       = 0;

	m_pDi[0]->SetSyncConfig(syncISync, Sync);
	}

void CIpu51::InitDiHSync(void)
{
	UINT uRunH = m_Display.m_xSize      + 
		     m_Display.m_uHorzFront + 
		     m_Display.m_uHorzSync  + 
		     m_Display.m_uHorzBack  ;

	CSyncConfig Sync;

	Sync.m_uRun		 = uRunH-1;
	Sync.m_uRunTrig		 = syncPixel + 1;
	Sync.m_uOffset		 = 0;
	Sync.m_uOffsetTrig	 = 0;
	Sync.m_uAutoReload	 = 1;
	Sync.m_uClearSel	 = 0;
	Sync.m_uToggleTrigSel	 = syncPixel + 1;
	Sync.m_uPolarityEnable	 = 1;
	Sync.m_uPolarityClearSel = 0;
	Sync.m_uPosUp		 = 0;
	Sync.m_uPosDn		 = m_Display.m_uHorzSync;
	Sync.m_uStepRepeat       = 0;

	m_pDi[0]->SetSyncConfig(syncHSync, Sync);
	}

void CIpu51::InitDiVSync(void)
{
	UINT uRunV = m_Display.m_ySize      + 
		     m_Display.m_uVertFront + 
		     m_Display.m_uVertSync  + 
		     m_Display.m_uVertBack  ;

	CSyncConfig Sync;

	Sync.m_uRun		 = uRunV-1;
	Sync.m_uRunTrig		 = syncHSync + 1;
	Sync.m_uOffset		 = 0;
	Sync.m_uOffsetTrig	 = 0;
	Sync.m_uAutoReload	 = 1;
	Sync.m_uClearSel	 = 0;
	Sync.m_uToggleTrigSel	 = syncHSync + 1;
	Sync.m_uPolarityEnable	 = 1;
	Sync.m_uPolarityClearSel = 0;
	Sync.m_uPosUp		 = 0;
	Sync.m_uPosDn		 = m_Display.m_uVertSync;
	Sync.m_uStepRepeat       = 0;

	m_pDi[0]->SetSyncConfig(syncVSync, Sync);
	}

void CIpu51::InitDiActiveLine(void)
{
	CSyncConfig Sync;

	Sync.m_uRun		 = 0;
	Sync.m_uRunTrig		 = syncHSync + 1;
	Sync.m_uOffset		 = m_Display.m_uVertFront + m_Display.m_uVertSync;
	Sync.m_uOffsetTrig	 = syncHSync + 1;
	Sync.m_uAutoReload	 = 0;
	Sync.m_uClearSel	 = syncVSync + 1;
	Sync.m_uToggleTrigSel	 = 0;
	Sync.m_uPolarityEnable	 = 0;
	Sync.m_uPolarityClearSel = 0;
	Sync.m_uPosUp		 = 0;
	Sync.m_uPosDn		 = 0;
	Sync.m_uStepRepeat       = m_Display.m_ySize;

	m_pDi[0]->SetSyncConfig(syncActiveLine, Sync);
	}

void CIpu51::InitDiActiveClock(void)
{
	CSyncConfig Sync;

	Sync.m_uRun		 = 0;
	Sync.m_uRunTrig		 = syncPixel + 1;
	Sync.m_uOffset		 = m_Display.m_uHorzFront + m_Display.m_uHorzSync;
	Sync.m_uOffsetTrig	 = syncPixel + 1;
	Sync.m_uAutoReload	 = 0;
	Sync.m_uClearSel	 = syncActiveLine + 1;
	Sync.m_uToggleTrigSel	 = 0;
	Sync.m_uPolarityEnable	 = 0;
	Sync.m_uPolarityClearSel = 0;
	Sync.m_uPosUp		 = 0;
	Sync.m_uPosDn		 = 0;
	Sync.m_uStepRepeat       = m_Display.m_xSize;

	m_pDi[0]->SetSyncConfig(syncActiveClock, Sync);
	}

void CIpu51::InitDiActiveClock8(void)
{
	CSyncConfig Sync;

	Sync.m_uRun		 = 8 - 1;
	Sync.m_uRunTrig		 = syncPixel + 1;
	Sync.m_uOffset		 = m_Display.m_uHorzFront + m_Display.m_uHorzSync;
	Sync.m_uOffsetTrig	 = syncPixel + 1;
	Sync.m_uAutoReload	 = 0;
	Sync.m_uClearSel	 = syncActiveLine + 1;
	Sync.m_uToggleTrigSel	 = syncPixel + 1;
	Sync.m_uPolarityEnable	 = 1;
	Sync.m_uPolarityClearSel = 0;
	Sync.m_uPosUp		 = 0;
	Sync.m_uPosDn		 = 8 / 2;
	Sync.m_uStepRepeat       = (m_Display.m_xSize / 8) + 2;

	m_pDi[0]->SetSyncConfig(syncActiveClock8, Sync);
	}

void CIpu51::InitDiDataEnable(void)
{
	CSyncConfig Sync;

	Sync.m_uRun		 = m_Display.m_xSize - 1;
	Sync.m_uRunTrig		 = syncPixel + 1;
	Sync.m_uOffset		 = m_Display.m_uHorzFront + m_Display.m_uHorzSync;
	Sync.m_uOffsetTrig	 = syncPixel + 1;
	Sync.m_uAutoReload	 = 1;
	Sync.m_uClearSel	 = syncActiveLine + 1;
	Sync.m_uToggleTrigSel	 = syncActiveClock8 + 1;
	Sync.m_uPolarityEnable	 = 1;
	Sync.m_uPolarityClearSel = 0;
	Sync.m_uPosUp		 = 0;
	Sync.m_uPosDn		 = m_Display.m_xSize / 8;
	Sync.m_uStepRepeat       = 0;

	m_pDi[0]->SetSyncConfig(syncDE, Sync);
	}

void CIpu51::InitDiMisc(void)
{
	UINT uRunV = m_Display.m_ySize      + 
		     m_Display.m_uVertFront + 
		     m_Display.m_uVertSync  + 
		     m_Display.m_uVertBack  ;

	m_pDi[0]->SetBaseClock(m_Display.m_uFrequency);
	
	m_pDi[0]->SetScreenHeight(uRunV);

	m_pDi[0]->SetSyncStart(2);

	m_pDi[0]->SetLineCounter(syncVSync);

	m_pDi[0]->SetPolarity(false, false, false, 0x80);

	Reg(Di0) |= Bit(31);
	}

// Display Controller

void CIpu51::InitDcChannels(void)
{
	CDcWriteChanConfig ChanConfig;

	ChanConfig.m_uStartTime	= 0;
	ChanConfig.m_uFieldMode	= 0;
	ChanConfig.m_uEventMask	= 0;
	ChanConfig.m_uMode	= 4;
	ChanConfig.m_uDisplay	= 0;
	ChanConfig.m_uDi	= 0;
	ChanConfig.m_uWordSize	= 24;
	ChanConfig.m_uStartAddr	= 0;

	m_pDc->SetWriteChanConfig(chanPrimary, ChanConfig);
	}

void CIpu51::InitDcDisplays(void)
{
	CDcDisplayConfig DispConfig;

	DispConfig.m_uRdValPtr	 = 0;
	DispConfig.m_uMcuAccMask = 0;
	DispConfig.m_uAddrBe	 = 0;
	DispConfig.m_uAddrInc	 = 0;
	DispConfig.m_uType	 = 2;
	DispConfig.m_uStride	 = m_Display.m_xSize;
	
	m_pDc->SetDispConfig(0, DispConfig);
	}

void CIpu51::InitDcBusMapping(void)
{
	CDcBusMappingData BusConfig;

	BusConfig.m_uOffset[0] = 23; 	
	BusConfig.m_uMask  [0] = 0xFF;
	BusConfig.m_uOffset[1] = 15;	
	BusConfig.m_uMask  [1] = 0xFF;
	BusConfig.m_uOffset[2] = 7;	
	BusConfig.m_uMask  [2] = 0xFF;	

	m_pDc->SetMappingPointer(0, BusConfig);

	// For both panels, we must configure HSC module (MIPI controller)
	// so that Display Data pins 0-5 come from IPU and not from HSC
	 
	PDWORD(0x83FDC000)[0] = 0x00000F00;
	}

void CIpu51::InitDcMicrocode(void)
{
	CDcMicrocodeConfig CodeConfig;

	UINT uCodePtr = 0;

	CodeConfig.m_uOpcode    = opWrod;
	CodeConfig.m_uOperand   = 0;
	CodeConfig.m_uMapping   = 0 + 1;
	CodeConfig.m_uWaveform  = waveformSync + 1;
	CodeConfig.m_uGlueLogic = 0x8;
	CodeConfig.m_uSync      = syncActiveClock;
	CodeConfig.m_fStop      = true;

	m_pDc->SetMicrocode(uCodePtr++, CodeConfig);

	CodeConfig.m_uGlueLogic = 0x4;

	m_pDc->SetMicrocode(uCodePtr++, CodeConfig);

	CodeConfig.m_uGlueLogic = 0x0;

	m_pDc->SetMicrocode(uCodePtr++, CodeConfig);
	}

void CIpu51::InitDcEvents(void)
{
	UINT uCodePtr = 0;

	m_pDc->SetMicrocodeEvent(chanPrimary, dcNewLine,    3, uCodePtr);
	m_pDc->SetMicrocodeEvent(chanPrimary, dcNewFrame,   0, 0);
	m_pDc->SetMicrocodeEvent(chanPrimary, dcNewField,   0, 0);
	m_pDc->SetMicrocodeEvent(chanPrimary, dcEndOfFrame, 0, 0);
	m_pDc->SetMicrocodeEvent(chanPrimary, dcEndOfField, 0, 0);
	m_pDc->SetMicrocodeEvent(chanPrimary, dcNewChan,    0, 0);
	m_pDc->SetMicrocodeEvent(chanPrimary, dcNewAddr,    0, 0);

	m_pDc->SetMicrocodeEvent(chanPrimary, dcEndOfLine,  2, ++uCodePtr);

	m_pDc->SetMicrocodeEvent(chanPrimary, dcNewData,    1, ++uCodePtr);
	}

// Objects

void CIpu51::MakeObjects(void)
{
	m_pIdmac = New CIpuImageDma51(this);

	m_pDmfc  = New CIpuDisplayFifo51(this);

	m_pDi[0] = New CIpuDisplayInterface51(0, this, m_uClock);

	m_pDi[1] = New CIpuDisplayInterface51(1, this, m_uClock);

	m_pDc    = New CIpuDisplayControl51(this);

	m_pCpmem = New CIpuChanParam51();
	}

void CIpu51::FreeObjects(void)
{
	delete m_pIdmac;

	delete m_pDmfc;

	delete m_pDi[0];

	delete m_pDi[1];

	delete m_pDc;

	delete m_pCpmem;
	}

// End of File
