;------------------------------------------------------------------------------
;
;	iMX51 Tacoma Hardware Drivers
;
;	Copyright (c) 1991-2012 Red Lion Controls
;
;	All Rights Reserved
;
;------------------------------------------------------------------------------

;------------------------------------------------------------------------------
;
; 	Processor
;
;------------------------------------------------------------------------------    

	list		p=12LF1822

;------------------------------------------------------------------------------
;
; 	Includes
;
;------------------------------------------------------------------------------    

	#include	<p12lf1822.inc>

;------------------------------------------------------------------------------
;
; 	Configuration Words
;
;------------------------------------------------------------------------------    

	__CONFIG _CONFIG1, _FOSC_INTOSC & _WDTE_ON & _PWRTE_OFF & _MCLRE_ON & _CP_OFF & _CPD_OFF & _BOREN_OFF & _CLKOUTEN_OFF & _IESO_ON & _FCMEN_ON
	__CONFIG _CONFIG2, _WRT_OFF & _PLLEN_OFF & _STVREN_ON & _BORV_19 & _LVP_ON

;------------------------------------------------------------------------------
;	
; 	Constants
;
;------------------------------------------------------------------------------

MAX_BIT_COUNT	EQU		0x1555

;------------------------------------------------------------------------------
;
; 	I/O Pin Bits
;
;------------------------------------------------------------------------------

BIT_PIC_SP		EQU		0		; In-Circuit Programming Data / GPIO_4_19
BIT_PIC_IN		EQU		1		; In-Circuit Programming Clock / GPIO_4_20
BIT_RTS_IN		EQU		2		; RTS Sample Input
BIT_RTS_EX		EQU		4 		; RTS Output 
BIT_SER_IN		EQU		5		; Uart TX Sample

;------------------------------------------------------------------------------
;
; 	I/O Pin Masks
;
;------------------------------------------------------------------------------

PIC_SP			EQU		0x01
PIC_IN			EQU		0x02
RTS_IN			EQU		0x04
RTS_EX			EQU		0x10
SER_IN			EQU		0x20

;------------------------------------------------------------------------------
;
; 	Working Registers
;
;------------------------------------------------------------------------------

BIT_TIME_LO		EQU		0x70
BIT_TIME_HI		EQU		0x71
BYTE_TIME_LO	EQU		0x72
BYTE_TIME_HI	EQU		0x73
LIMIT_LO		EQU		0x74
LIMIT_HI		EQU		0x75
PRESCALER		EQU		0x76
TEMP			EQU		0x77
TEMP_LO			EQU		0x77
TEMP_HI			EQU		0x78

;------------------------------------------------------------------------------
;
; 	Reset Vector
;
;------------------------------------------------------------------------------

    org     0x0000 
    goto    Start             

;------------------------------------------------------------------------------
;
; 	Interrupt Service Routine
;
;------------------------------------------------------------------------------

    org      0x0004
    retfie                    
    
;------------------------------------------------------------------------------
;
; 	Main Program
;
;------------------------------------------------------------------------------

Start

	call	Init
	call	InitIO
	call	InitOsc
	call	InitTimer

Loop

	call	WaitAuto
	call	TimeBitByte
	call	AutoLoop

	goto 	Loop

;------------------------------------------------------------------------------
;
; 	Init
;
;------------------------------------------------------------------------------

Init

	movlw	LOW MAX_BIT_COUNT 
	movwf	LIMIT_LO
	movlw	HIGH MAX_BIT_COUNT
	movwf	LIMIT_HI
	clrf	PRESCALER

	return

;------------------------------------------------------------------------------
;
; 	Init I/O
;
;------------------------------------------------------------------------------

InitIO

	banksel	PORTA
	clrf	PORTA
	banksel LATA	
	clrf 	LATA
	banksel ANSELA	
 	clrf 	ANSELA
	banksel TRISA 
	movlw	PIC_SP | PIC_IN | RTS_IN | SER_IN 
	movwf 	TRISA 
	banksel PORTA 
	bsf	  	PORTA, BIT_RTS_EX

	return

;------------------------------------------------------------------------------
;
; 	Init Oscillator
;
;------------------------------------------------------------------------------

InitOsc

	banksel	OSCCON
	movlw	0xF0 
	movwf	OSCCON
	btfss 	OSCSTAT, HFIOFR
	goto 	$-1

	return

;------------------------------------------------------------------------------
;
; 	Init Timer
;
;------------------------------------------------------------------------------

InitTimer

	banksel	T1CON
	movlw	0x40
	movwf	T1CON
	movlw	0x00
	movwf	T1GCON
	banksel	TMR1H
	clrf	TMR1H
	banksel	TMR1L
	clrf	TMR1L
	clrf	PRESCALER

	return

;------------------------------------------------------------------------------
;
; 	Wait Auto 
;
;------------------------------------------------------------------------------

WaitAuto

	; Kick the dog
	
	clrwdt

	; Forward RTS state 

	banksel	PORTA
	btfss	PORTA, BIT_RTS_IN
	bcf		PORTA, BIT_RTS_EX
	btfsc	PORTA, BIT_RTS_IN
	bsf		PORTA, BIT_RTS_EX

	; Check auto-enable detect

	btfss	PORTA, BIT_PIC_IN
	goto	WaitAuto

	return 

;------------------------------------------------------------------------------
;
; 	Auto Loop
;
;------------------------------------------------------------------------------

AutoLoop

	; Kick the dog

	clrwdt

	; Check auto-enable exit condition

	banksel	PORTA
	btfss	PORTA, BIT_PIC_IN
	return

	; Wait for a start bit

	btfsc	PORTA, BIT_SER_IN
	goto	AutoLoop

	; Assert RTS

	bcf		PORTA, BIT_RTS_EX

	; Wait for the full time to pass

AutoNewByte:

	call 	WaitByteTime

	; Clear the timer again

	banksel TMR1L
	clrf	TMR1L
	banksel TMR1H
	clrf	TMR1H

	; Give the dog a kick

	clrwdt

	; Wait for a new start bit

AutoWaitStart:	

	banksel PORTA
	btfss	PORTA, BIT_SER_IN
	goto	AutoNewByte
	banksel TMR1H
	movfw	TMR1H
	subwf	BIT_TIME_HI, W
	movwf	TEMP
	banksel TMR1L
	movfw	TMR1L
	subwf	BIT_TIME_LO, W
	btfss	STATUS, C
	decf	TEMP, F
	btfss	TEMP, 7
	goto	AutoWaitStart

	; Turn off the RTS signal

	banksel PORTA
	bsf		PORTA, BIT_RTS_EX

; Go around again

	goto	AutoLoop

;------------------------------------------------------------------------------
;
; 	Time Bit/Byte
;
;------------------------------------------------------------------------------

TimeBitByte

	; Clear timer

	call	InitTimer

	; Kick the dog

	clrwdt

	; Wait for the start bit
	
	banksel	PORTA
	btfsc	PORTA, BIT_SER_IN
	goto	$-1

	; Start Timer

	banksel	T1CON
	bsf		T1CON, TMR1ON

	; Time the length of the start bit

	banksel	PORTA
	btfss	PORTA, BIT_SER_IN
	goto	$-1

	; Record bit time

	banksel	T1CON
	bcf		T1CON, TMR1ON
	banksel	TMR1H
	movfw	TMR1H
	movwf	BIT_TIME_HI
	banksel	TMR1L
	movfw	TMR1L
	movwf	BIT_TIME_LO
	bsf		T1CON, TMR1ON

	; Check for we won't overflow

CheckOver

	movfw	BIT_TIME_HI
	subwf	LIMIT_HI, W
	movwf	TEMP
	movfw	BIT_TIME_LO
	subwf	LIMIT_LO, W
	btfss	STATUS, C
	decf	TEMP, F
	btfss	TEMP, 7
	goto	WaitFirstBit

	; Change the prescaler
	
	incf	PRESCALER, F
	swapf	PRESCALER, W
	banksel	T1CON
	movwf	T1CON
	bsf		T1CON, 6
	bsf		T1CON, TMR1ON

	; Half the bit time and timer.

	banksel T1CON
	bcf		T1CON, TMR1ON
	bcf		STATUS, C
	banksel	TMR1H
	rrf		TMR1H, F
	banksel	TMR1L
	rrf		TMR1L, F
	banksel T1CON
	bsf		T1CON, TMR1ON
	bcf		STATUS, C
	rrf		BIT_TIME_HI, F
	rrf		BIT_TIME_LO, F

	; Check the overflow again.

	goto	CheckOver	

	; Wait for end of first bit

WaitFirstBit

	banksel	PORTA
	btfsc	PORTA, BIT_SER_IN
	goto	$-1

	; Wait for end of byte

	btfss	PORTA, BIT_SER_IN
	goto	$-1

	; Wait for end of stop

	btfsc	PORTA, BIT_SER_IN
	goto	$-1

	; Record the overall byte time

	banksel	T1CON
	bcf		T1CON, TMR1ON
	banksel	TMR1H
	movfw	TMR1H
	movwf	BYTE_TIME_HI
	banksel	TMR1L
	movfw	TMR1L
	movwf	BYTE_TIME_LO
	banksel	T1CON
	bsf		T1CON, TMR1ON

	; Subtract half bit time

	bcf		STATUS, C
	rrf		BIT_TIME_HI, W
	movwf	TEMP_HI
	rrf		BIT_TIME_LO, W
	movwf	TEMP_LO
	movfw	TEMP_HI
	subwf	BYTE_TIME_HI, F
	movfw	TEMP_LO
	subwf	BYTE_TIME_LO, F
	btfss	STATUS, C
	decf	BYTE_TIME_HI, F

	; Wait for end of 2nd training byte

	btfss	PORTA, BIT_SER_IN
	goto	$-1

	return

;------------------------------------------------------------------------------
;
; 	Wait Byte Time
;
;------------------------------------------------------------------------------

WaitByteTime

	banksel TMR1L
	clrf	TMR1L
	banksel TMR1H
	clrf	TMR1H

WaitTimer:

	banksel TMR1H
	movfw	TMR1H
	subwf	BYTE_TIME_HI, W
	movwf	TEMP
	banksel TMR1L
	movfw	TMR1L
	subwf	BYTE_TIME_LO, W
	btfss	STATUS, C
	decf	TEMP, F
	btfss	TEMP, 7
	goto	WaitTimer

	return

;------------------------------------------------------------------------------
;
; 	END
;
;------------------------------------------------------------------------------

	end