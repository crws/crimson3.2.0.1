
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Touhc51_HPP
	
#define	INCLUDE_Touch51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Interfaces
//

#include "../../StdEnv/IRtc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../RedDev/TouchGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPmui51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Pmuic Touch Screen Controller
//

class CTouch51 : public CTouchGeneric,
		 public IBatteryMonitor,
		 public IEventSink
{
	public:
		// Constructor
		CTouch51(UINT uModel, CPmui51 *pPmui);

		// Destructor
		~CTouch51(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// BatterMonitor
		bool METHOD IsBatteryGood(void);
		bool METHOD IsBatteryBad(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Constants
		static UINT const timeReadBatt = 720000;
		static UINT const timeInitial  = 100;
		static UINT const timeRepeat   = 20;
		static UINT const timeRecover  = 2;

		// States
		enum
		{
			stateInit,
			stateReadBatt,
			stateWaitDown,
			stateConvert,
			stateWaitUp,
			};

		// Data Members
		UINT      m_uModel;
		CPmui51 * m_pPmui;
		ITimer  * m_pTimer;   
		UINT	  m_uState;
		UINT      m_uCount;
		UINT      m_uBat;
		UINT      m_uRead;
		UINT      m_x0[2], m_x1[2];
		UINT      m_y0[2], m_y1[2];
		UINT      m_z0[2], m_z1[2];

		// Events
		void OnTimer(void);
		void OnTouch(bool fTouch);

		// Implementation
		void InitEvents(void);
		void WaitReadBatt(void);
		void WaitStylusDown(void);
		void WaitStylusUp(void);
		void WaitPosition(void);
		void ReadTouch(void);
		bool TestTouch(void);
		int  FindAdjust(void);

		// Overridables
		void DefaultCalib(void);
		bool OnTouchValue(void);
	};

// End of File

#endif
