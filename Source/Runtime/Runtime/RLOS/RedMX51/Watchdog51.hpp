
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Watchdog51_HPP
	
#define	INCLUDE_Watchdog51_HPP

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Watchdog Timer
//

class CWatchdog51
{
	public:
		// Constructor
		CWatchdog51(void);

		// Operations
		void Enable(UINT uTime);
		void Kick(void);

	protected:
		// Registers
		enum
		{
			regControl   = 0x0000 / sizeof(WORD),
			regService   = 0x0002 / sizeof(WORD),
			regStatus    = 0x0004 / sizeof(WORD),
			regInterrupt = 0x0006 / sizeof(WORD),
			regMisc      = 0x0008 / sizeof(WORD),
			};

		// Data Members
		PVWORD m_pBase;
	};

// End of File

#endif
