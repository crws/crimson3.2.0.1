
#include "Intern.hpp"

#include "Ipu51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IpuChanParam51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Channel Parameter Memory
//

// Memory Access

#define Word(n, r)  PDWORD(m_pBase + (n * 16 + r * 8))

// Constructor

CIpuChanParam51::CIpuChanParam51(void)
{
	m_pBase = PVDWORD(ADDR_IPUEX) + CIpu51::regCpmem;
	}

// Interface

void CIpuChanParam51::ClearAll(void)
{
	memset(PVOID(m_pBase), 0, 80 * sizeof(DWORD) * 8 * 2);
	}

bool CIpuChanParam51::SetBuffer(UINT uChan, UINT uNum, DWORD dwAddr)
{
	if( uChan < 80 ) {

		PDWORD p1 = Word(uChan, 1);

		if( uNum == 0 ) {

			BitSet(p1, dwAddr >> 3,  0, 29);
			}
		else {
			BitSet(p1, dwAddr >> 3, 29, 29);
			}

		return true;
		}

	return false;
	}

bool CIpuChanParam51::SetOffset(UINT uChan, CCpmBuffOffsets const &Cfg)
{
	if( uChan < 80 ) {

		PDWORD p0  = Word(uChan, 0);

		PDWORD p1  = Word(uChan, 1);

		BitSet(p1, Cfg.m_uInterlaceOffset >> 3, 58, 20); 

		if( !Cfg.m_fInterleaved ) {

			BitSet(p0, Cfg.m_uOffsetU >> 3, 46, 22); 

			BitSet(p0, Cfg.m_uOffsetV >> 3, 68, 22); 
			}

		return true;
		}

	return false;
	}

bool CIpuChanParam51::SetConfig(UINT uChan, UINT uWidth, UINT uHeight, UINT uBpp, UINT uStride, bool fInterlaced)
{
	CCpmConfig      Cfg;

	CCpmBuffOffsets Off;

	Cfg.m_bAccessDimension	  = 0;
	Cfg.m_bScanOrder	  = fInterlaced ? 1 : 0;
	Cfg.m_bBandMode		  = 0;
	Cfg.m_bBlockMode	  = 0;
	Cfg.m_bThresholdEnable	  = 0;
	Cfg.m_bThreshold          = 0;
	Cfg.m_bCondAccessEnable   = 0;
	Cfg.m_bAXI		  = (uChan == CIpu51::chanDpPrimaryMain || uChan == CIpu51::chanDcSyncAsync) ? 0 : 1;
	Cfg.m_bDecAddrSelect	  = 0;
	Cfg.m_bCondAccessPolarity = 0;
	Cfg.m_bAlphaUsed	  = 0;
	Cfg.m_bRotation90	  = 0;
	Cfg.m_bFlipHoriz	  = 0;
	Cfg.m_bFlipVert		  = 0;
	Cfg.m_bPixelFormat	  = 7;
	Cfg.m_wHeight		  = uHeight - 1;
	Cfg.m_wWidth		  = uWidth  - 1;
	Cfg.m_uLineStride	  = (fInterlaced ? uStride * 2 : uStride) - 1;
	Cfg.m_fInterleaved        = true;
	Off.m_fInterleaved        = true;
	Off.m_uInterlaceOffset	  = fInterlaced ? uStride : 0;
	
	switch( uBpp ) {

		case 24:
			Cfg.m_bBitsPerPixel = 1;
			Cfg.m_bPixelBurst   = 15;
			Cfg.m_bComp0Witdh   = 8-1;
			Cfg.m_bComp1Witdh   = 8-1;
			Cfg.m_bComp2Witdh   = 8-1;
			Cfg.m_bComp3Witdh   = 0;
			Cfg.m_bComp0Offset  = 0;
			Cfg.m_bComp1Offset  = 8;
			Cfg.m_bComp2Offset  = 16;
			Cfg.m_bComp3Offset  = 0;
			
			break;

		case 32:
			Cfg.m_bBitsPerPixel = 0;
			Cfg.m_bPixelBurst   = 15;
			Cfg.m_bComp0Witdh   = 8-1;
			Cfg.m_bComp1Witdh   = 8-1;
			Cfg.m_bComp2Witdh   = 8-1;
			Cfg.m_bComp3Witdh   = 8-1;
			Cfg.m_bComp0Offset  = 8;
			Cfg.m_bComp1Offset  = 16;
			Cfg.m_bComp2Offset  = 24;
			Cfg.m_bComp3Offset  = 0;
		
			break;

		default:
			return false;
		}

	return SetOffset(uChan, Off) && SetConfig(uChan, Cfg);
	}

bool CIpuChanParam51::SetConfig(UINT uChan, CCpmConfig const &Cfg)
{
	if( uChan < 80 ) {

		PDWORD p0  = Word(uChan, 0);

		PDWORD p1  = Word(uChan, 1);

		BYTE   bCh = FindAlphaChanMapping(uChan);  

		if( !Cfg.m_fInterleaved ) { 

			BitSet(p0, 0,			        0, 10);
			BitSet(p0, 0,			       10,  9); 
			BitSet(p0, 0,			       19, 13);
			BitSet(p0, 0,			       32, 12); 
			BitSet(p0, 0,			       44,  1); 
			BitSet(p0, 0,			       45,  1); 
			BitSet(p0, 0,			       90,  4);
			BitSet(p0, Cfg.m_bScanOrder,	      113,  1);
			BitSet(p0, Cfg.m_bBandMode,	      114,  3); 
			BitSet(p0, Cfg.m_bBlockMode,	      117,  2);
			BitSet(p0, Cfg.m_bRotation90,	      119,  1);
			BitSet(p0, Cfg.m_bFlipHoriz,	      120,  1);
			BitSet(p0, Cfg.m_bFlipVert,	      121,  1);
			BitSet(p0, Cfg.m_bThresholdEnable,    122,  1);
			BitSet(p0, Cfg.m_bCondAccessEnable,   123,  1);
			BitSet(p0, Cfg.m_bCondAccessPolarity, 124,  1); 
			BitSet(p0, Cfg.m_wWidth,              125, 13);
			BitSet(p0, Cfg.m_wHeight,	      138, 12);
			
			BitSet(p1, Cfg.m_bPixelBurst,          78,  7);
			BitSet(p1, Cfg.m_bPixelFormat,         85,  4);
			BitSet(p1, Cfg.m_bAlphaUsed,           89,  1);
			BitSet(p1, bCh,		               90,  3);
			BitSet(p1, Cfg.m_bAXI,	               93,  2);
			BitSet(p1, Cfg.m_bThreshold,           95,  7);
			BitSet(p1, Cfg.m_uLineStrideY,        102, 14);
			BitSet(p1, Cfg.m_bComp3Witdh,         125,  3);
			BitSet(p1, Cfg.m_uLineStrideUV,       128, 14);
			}
		else {
			BitSet(p0, 0,			        0, 10);
			BitSet(p0, 0,			       10,  9); 
			BitSet(p0, 0,			       19, 13);
			BitSet(p0, 0,			       32, 12); 
			BitSet(p0, 0,			       44,  1); 
			BitSet(p0, 0,			       45,  1); 
			BitSet(p0, 0,			       46, 12);
			BitSet(p0, 0,		               58, 11);
			BitSet(p0, 0,		               69, 10);
			BitSet(p0, 0,			       79,  7);
			BitSet(p0, 0,			       86, 10);
			BitSet(p0, 0,		               96,  1);
			BitSet(p0, 0,		  	       97,  1);
			BitSet(p0, 0,		               98,  7);
			BitSet(p0, 0,			      105,  1);
			BitSet(p0, 0,			      106,  1);
			BitSet(p0, Cfg.m_bBitsPerPixel,       107,  3);
			BitSet(p0, Cfg.m_bDecAddrSelect,      110,  2);
			BitSet(p0, Cfg.m_bAccessDimension,    112,  1);
			BitSet(p0, Cfg.m_bScanOrder,	      113,  1);
			BitSet(p0, Cfg.m_bBandMode,	      114,  3); 
			BitSet(p0, Cfg.m_bBlockMode,	      117,  2);
			BitSet(p0, Cfg.m_bRotation90,	      119,  1);
			BitSet(p0, Cfg.m_bFlipHoriz,	      120,  1);
			BitSet(p0, Cfg.m_bFlipVert,	      121,  1);
			BitSet(p0, Cfg.m_bThresholdEnable,    122,  1);
			BitSet(p0, Cfg.m_bCondAccessEnable,   123,  1);
			BitSet(p0, Cfg.m_bCondAccessPolarity, 124,  1); 
			BitSet(p0, Cfg.m_wWidth,	      125, 13);
			BitSet(p0, Cfg.m_wHeight,	      138, 12);

			BitSet(p1, Cfg.m_bPixelBurst,          78,  7);
			BitSet(p1, Cfg.m_bPixelFormat,         85,  4);
			BitSet(p1, Cfg.m_bAlphaUsed,           89,  1);
			BitSet(p1, bCh,		               90,  3);
			BitSet(p1, Cfg.m_bAXI,	               93,  2);
			BitSet(p1, Cfg.m_bThreshold,           95,  7);
			BitSet(p1, Cfg.m_uLineStride,         102, 14);
			BitSet(p1, Cfg.m_bComp0Witdh,         116,  3);
			BitSet(p1, Cfg.m_bComp1Witdh,         119,  3);
			BitSet(p1, Cfg.m_bComp2Witdh,         122,  3);
			BitSet(p1, Cfg.m_bComp3Witdh,         125,  3);
			BitSet(p1, Cfg.m_bComp0Offset,        128,  5);
			BitSet(p1, Cfg.m_bComp1Offset,        133,  5);
			BitSet(p1, Cfg.m_bComp2Offset,        138,  5);
			BitSet(p1, Cfg.m_bComp3Offset,        143,  5);
			}
		
		return true;
		}

	return false;
	}

bool CIpuChanParam51::SetBandMode(UINT uChan, UINT uMode)
{
	if( uChan < 80 ) {

		PDWORD p0 = Word(uChan, 0);

		BitSet(p0, uMode, 114, 3);

		return true;
		}

	return false;
	}

bool CIpuChanParam51::SetXScroll(UINT uChan, UINT uScroll)
{
	if( uChan < 80 ) {

		PDWORD p0 = Word(uChan, 0);

		PDWORD p1 = Word(uChan, 1);

		UINT uPixelFmt = BitGet(p1, 85, 4);

		if( uPixelFmt < 5 ) {

			BitSet(p0, uScroll, 90,  4);
			}
		else {
			BitSet(p0, uScroll, 46, 12);
			}
		
		return true;
		}

	return false;
	}

// Implementation

UINT CIpuChanParam51::FindAlphaChanMapping(UINT uChan) const
{
	switch( uChan ) {

		case 14: 
			return 0;

		case 15:
			return 1;

		case 27:
			return 2;

		case 29:
			return 3;

		case 23:
			return 4;

		case 24:
			return 5;
		}

	return 0;
	}

void CIpuChanParam51::BitSet(PVDWORD pReg, DWORD Data, UINT nPos, UINT uSize)
{
	pReg += (nPos / 32);

	while( uSize ) {

		UINT  b = nPos % 32;

		UINT  n = (b + uSize) > 32 ? 32 - b : uSize;

		DWORD m = (1 << n) - 1;

		pReg[0] &= ~(m << b);

		pReg[0] |= ((Data & m) << b);

		uSize -= n;

		nPos  += n;

		Data >>= n;

		pReg ++;
		}
	}

UINT CIpuChanParam51::BitGet(PVDWORD pReg, UINT nPos, UINT uSize)
{
	if( uSize <= 32 ) {

		pReg += (nPos / 32);

		DWORD Data = 0;

		UINT  uBit = 0;

		while( uSize ) {

			UINT  b = nPos % 32;

			UINT  n = (b + uSize) > 32 ? 32 - b : uSize;

			DWORD m = (1 << n) - 1;

			DWORD d = (pReg[0] >> b) & m;

			Data  |= (d << uBit);
			
			uBit  += n;

			nPos  += n;

			uSize -= n;

			pReg ++;
			}
	
		return Data;
		}

	return 0;
	}

// End of File
