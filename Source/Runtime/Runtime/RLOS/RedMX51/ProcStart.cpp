
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Startup Code
//

// No Optimization

#pragma GCC optimize("O0")

// Externals

extern int  Main(void);

extern bool Create_Hal(void);

// Prototypes

clink	void	Start(void);
clink	void	SetSuperStack(void);
clink	void	SetOtherStacks(void);

// Code

clink void NAKED Start(void)
{
	// At this point we're executing in non-cached memory,
	// so our PC doesn't match our relocated address. That
	// is okay as long as we limit what we do, so we need
	// to set a few things up and then we can perform a
	// jump to the absolute location of the next routine,
	// setting the PC to the correct, cached address.

	DWORD p = DWORD(&SetSuperStack);

	#if !defined(__INTELLISENSE__)

	ASM(	//	Switch to supervisor
		"	ldr	r0, =0x01D3		\n\t"
		"	msr	cpsr_c, r0		\n\t"
		//	Flush the TLBs
		"	mov	r0,  #0			\n\t"
		"	mcr	p15, 0, r0, c8, c7, 0	\n\t"
		//	Flush the write buffers
		"	mov	r0,  #0			\n\t"
		//	Erratum #09830
		"	mcr	p15, 0, r0, c7, c10, 4	\n\t"
		"	mrc	p15, 0, r1, c10, c2, 0	\n\t"
		"	bic	r1,  #(3 << 16)		\n\t"
		"	mcr	p15, 0, r1, c10, c2, 0	\n\t"
		"	mrc	p15, 0, r1, c1, c0, 0	\n\t"
		"	orr	r1,  r1, #(1 << 28)	\n\t"
		"	mcr	p15, 0, r1, c1, c0, 0	\n\t"
		//	Invalidate the i-cache
		"	mov	r1,  #0			\n\t"
		"	mcr	p15, 0, r1, c7, c5, 0	\n\t"
		//	Invalidate the TLBs
		"	mov	r1,  #0			\n\t"
		"	mcr	p15, 0, r1, c8, c7, 0	\n\t"
		//	Jump to cached memory
		"	mov	lr,  #0			\n\t"
		"	mov     r0,  %0			\n\t"
		"	bx	r0			\n\t"
		:
		: "r"(p)
		: "r0", "r1"
		);

	#endif
	}

clink void NAKED SetSuperStack(void)
{
	// Now we're in cached memory but we don't have
	// a stack. So we configure a stack for supervisor
	// mode before we call the function to set up the
	// other stacks. We can't do this in this function
	// as our frame variables won't work properly.

	static DWORD stack[4096];

	DWORD  set = DWORD(stack + elements(stack));

	#if !defined(__INTELLISENSE__)

	ASM(	"mov sp, %0		\n\t"
		"mov r0, #0		\n\t"
		"str r0, [sp, #-4]!	\n\t"
		"str r0, [sp, #-4]!	\n\t"
		"str r0, [sp, #-4]!	\n\t"
		"str r0, [sp, #-4]!	\n\t"
		:
		: "r"(set)
		: "r0"
		);

	#endif

	// Before we go any further, make sure there's
	// no junk left in our data cache. This should
	// not be required, but it's worth doing.

	ProcInvalidateDataCache();

	// And jump to the next bit of the startup.

	SetOtherStacks();
	}
	
clink void SetOtherStacks(void)
{
	// We executing in supervisor mode on a valid stack,
	// so we're at the point where we can do all the usual
	// C++ stuff like use frame variables and so on. We
	// loop around and setup the stacks for the other modes,
	// being careful to go back to supervisor each time so
	// that our frame variables remain valid.

	//				UNDEF	ABORT	IRQ	FIQ	MON	SYS

	static DWORD const pmode[6] = { 0x01DB, 0x01D7, 0x01D2, 0x01D1, 0x01D6, 0x01DF };

	static DWORD       stack[6][4096];

	for( UINT n = 0; n < elements(pmode); n++ ) {

		DWORD cpsr = pmode[n];

		DWORD load = DWORD(stack[n] + elements(stack[n]));

		DWORD back = 0x01D3;

		#if !defined(__INTELLISENSE__)

		// We dirty registers r8 - r12 (except fp) to
		// make sure that the compiler doesn't use them
		// to pass values to this code. Since we're
		// switching mode, they won't come across!

		ASM(	"msr cpsr_c, %0		\n\t"
			"mov sp, %1		\n\t"
			"mov r8, #0		\n\t"
			"str r8, [sp, #-4]!	\n\t"
			"str r8, [sp, #-4]!	\n\t"
			"str r8, [sp, #-4]!	\n\t"
			"str r8, [sp, #-4]!	\n\t"
			"msr cpsr_c, %2		\n\t"
			:
			: "r"(cpsr), "r"(load), "r"(back)
			: "r8", "r9", "r10", "r12"
			);

		#endif
		}

	// We can now create the HAL. Up until this point,
	// there are no interrupts, no fault handling and
	// very little support for debug, so be careful!

	if( Create_Hal() ) {

		// Now all that is done, we can switch into system mode
		// and jump to our main entry point, clearing the link
		// register to terminate any stack traces.

		DWORD p = DWORD(&Main);

		#if !defined(__INTELLISENSE__)

		ASM(	// Switch to user mode
			"mov	r0, #0x001F		\n\t"
			"msr	cpsr_c, r0		\n\t"
			// Jump to entry point
			"mov    lr,  #0			\n\t"
			"mov    r0,  %0			\n\t"
			"bx	r0			\n\t"
			:
			: "r"(p)
			: "r0"
			);

		#endif
		}

	// Failed to create HAL so we'll just have to reboot.

	for(;;);
	}

// End of File
