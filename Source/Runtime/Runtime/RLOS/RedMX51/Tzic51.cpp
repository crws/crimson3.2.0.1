
#include "Intern.hpp"

#include "Tzic51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define Reg(x) (m_pBase[reg##x])

//////////////////////////////////////////////////////////////////////////
//
// iMX51 TrustZone Interrupt Controller
//

// Nesting

static bool const m_fNest = true;

// Instantiator

IPic * Create_Tzic51(void)
{
	CTzic51 *pPic = New CTzic51;

	pPic->DevInit();

	return pPic;
	}

// Constructor

CTzic51::CTzic51(void)
{
	StdSetRef();

	m_pBase  = PVDWORD(ADDR_TZIC);

	m_uSets  = (Reg(Type) & 31) + 1;

	m_uUsed  = 0;

	m_uLines = 32 * m_uSets;

	m_pDiag  = NULL;

	m_uInit  = phal->GetTickCount();

	m_pSink  = New CSink [ m_uLines ];

	memset(m_pSink, 0, sizeof(CSink) * m_uLines);

	DisableAll();

	SetPriorityMask(0);

	Reg(Control) = (Bit(31) | Bit(16) | Bit(0));

	AfxTrace("PIC has %u lines\n", m_uLines);
	}

// Attributes

UINT CTzic51::GetLineCount(void)
{
	return m_uLines;
	}

UINT CTzic51::GetPriorityCount(void)
{
	return 31;
	}

UINT CTzic51::GetPriorityMask(void)
{
	return 31 - (Reg(PriMask) >> 3);
	}

UINT CTzic51::GetLinePriority(UINT uLine)
{
	if( uLine < m_uLines ) {

		PVBYTE pPri = PVBYTE(&Reg(Priority));

		return 30 - (pPri[uLine] >> 3);
		}

	return 0;
	}

bool CTzic51::IsLineEnabled(UINT uLine)
{
	if( uLine < m_uLines ) {

		PVDWORD pTest = &Reg(EnableSet);

		if( pTest[uLine/32] & (1 << (uLine%32)) ) {

			return true;
			}
		}

	return false;
	}

bool CTzic51::IsLinePending(UINT uLine)
{
	if( uLine < m_uLines ) {

		PVDWORD pPend = &Reg(PriPend);

		if( pPend[uLine/32] & (1 << (uLine%32)) ) {

			return true;
			}
		}

	return false;
	}

// IUnknown

HRESULT CTzic51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagProvider);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CTzic51::AddRef(void)
{
	StdAddRef();
	}

ULONG CTzic51::Release(void)
{
	StdRelease();
	}

// IDeviceDriver

void CTzic51::DevInit(void)
{
	HostMinIpr();
	}

void CTzic51::DevTerm(void)
{
	DisableAll();

	Reg(Control) = Bit(31);
	}

// IPic

void CTzic51::OnInterrupt(UINT uSource)
{
	if( likely(m_fNest && uSource == 1) ) {

		UINT PriMask = Reg(PriMask);

		Reg(PriMask) = 0;

		Dispatch();

		Reg(PriMask) = PriMask;

		return;
		}

	while( Dispatch() );
	}

bool CTzic51::SetLinePriority(UINT uLine, UINT uPriority)
{
	if( uLine < m_uLines ) {

		if( uPriority < 31 ) {

			MakeMax(m_uUsed, (uLine + 32) / 32);

			PVBYTE pPri = PVBYTE(&Reg(Priority));

			pPri[uLine] = (30 - uPriority) << 3;

			return true;
			}
		}

	return false;
	}

bool CTzic51::SetLineHandler(UINT uLine, IEventSink *pSink, UINT uParam)
{
	if( uLine < m_uLines ) {

		MakeMax(m_uUsed, (uLine + 32) / 32);

		m_pSink[uLine].m_pSink  = pSink;

		m_pSink[uLine].m_uParam = uParam;

		return true;
		}

	return false;
	}

bool CTzic51::EnableLine(UINT uLine, bool fEnable)
{
	if( fEnable ) {

		return EnableLine(uLine);
		}

	return DisableLine(uLine);
	}

bool CTzic51::EnableLineViaIrql(UINT uLine, UINT &uSave, bool fEnable)
{
	if( uLine < m_uLines ) {

		if( !fEnable ) {

			UINT i = GetLinePriority(uLine) ? 2 : 1;

			uSave  = Hal_RaiseIrql(IRQL_HARDWARE + i);

			if( m_fNest ) {

				uSave |= (Reg(PriMask) << 8); 

				PVBYTE pPri = PVBYTE(&Reg(Priority));

				if( pPri[uLine] < Reg(PriMask) ) {

					Reg(PriMask) = pPri[uLine];
					}
				}

			return true;
			}

		if( m_fNest ) {

			Reg(PriMask) = (uSave >> 8);
			
			uSave &= 0xFF;
			}

		Hal_LowerIrql(uSave);

		return true;
		}

	return false;
	}

void CTzic51::DebugRegister(void)
{
	AfxGetObject("diagmanager", 0, IDiagManager, m_pDiag);

	if( m_pDiag ) {

		m_uProv = m_pDiag->RegisterProvider(this, "pic");

		m_pDiag->RegisterCommand(m_uProv, 1, "counters");
		}
	}

void CTzic51::DebugRevoke(void)
{
	if( m_pDiag ) {

		m_pDiag->RevokeProvider(m_uProv);

		m_pDiag->Release();

		m_pDiag = NULL;
		}
	}

// IDiagProvider

static UINT tf(UINT n, UINT d)
{
	return UINT((UINT64(n) * 1000 + UINT64(d) / 2) / UINT64(d));
	}

UINT CTzic51::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		UINT t1 = phal->GetTickCount();

		UINT dt = phal->TicksToTime(t1 - m_uInit);

		if( dt ) {

			pOut->AddTable(3);

			pOut->SetColumn(0, "Line",  "%u");

			pOut->SetColumn(1, "Count", "%u");

			pOut->SetColumn(2, "Rate",  "%u");

			pOut->AddHead();

			pOut->AddRule('-');

			for( UINT n = 0; n < m_uLines; n++ ) {

				if( m_pSink[n].m_fUsed ) {

					pOut->AddRow();

					pOut->SetData(0, n);

					pOut->SetData(1, m_pSink[n].m_uCount);

					pOut->SetData(2, tf(m_pSink[n].m_uCount, dt));

					pOut->EndRow();
					}
				}

			pOut->AddRule('-');
		
			pOut->EndTable();

			HostMaxIpr();

			for( UINT n = 0; n < m_uLines; n++ ) {

				m_pSink[n].m_uCount = 0;
				}

			m_uInit = t1;

			HostMinIpr();

			return 0;
			}

		pOut->Error("no time ellapsed");

		return 1;
		}

	pOut->Error(NULL);

	return 1;
	}

// Operations

bool CTzic51::SetPriorityMask(UINT uPriority)
{
	if( uPriority < 32 ) {

		Reg(PriMask) = (31 - uPriority) << 3;

		return true;
		}

	return false;
	}

// Implementation

bool CTzic51::DisableAll(void)
{
	PVDWORD pSec = &Reg(Secure);

	PVDWORD pClr = &Reg(EnableClr);

	PVDWORD pPri = &Reg(Priority);

	for( UINT uSet = 0; uSet < m_uSets; uSet++ ) {

		pSec[uSet] = (uSet == 1) ? 0x00000100 : 0x00000000;

		pClr[uSet] = 0xFFFFFFFF;

		pPri[uSet] = 0xF0F0F0F0;
		}

	return true;
	}

bool CTzic51::Dispatch(void)
{
	PVDWORD pPend = &Reg(PriPend);

	PVDWORD pClr  = &Reg(SourceClr);

	PVBYTE  pPri  = PVBYTE(&Reg(Priority));

	bool    fFind = false;

	UINT    uBase = 31;

	for( UINT uSet = 0; uSet < m_uUsed; uSet++ ) {

		DWORD dwPend = pPend[uSet];

		while( dwPend ) {

			UINT  uFind = BuiltInClz(dwPend);

			DWORD dwBit = 0x80000000 >> uFind;

			UINT  uLine = uBase - uFind;

			CSink &Sink = m_pSink[uLine];

			if( likely(m_fNest) ) {
						
				if( likely(!Reg(PriMask)) ) {

					Reg(PriMask) = pPri[uLine];

					HostSetIpr(128);
					}
				}

			if( likely(pPri[uLine] <= Reg(PriMask)) ) {

				Sink.m_fUsed = TRUE;

				Sink.m_uCount++;

				if( likely(Sink.m_pSink) ) {

					Sink.m_pSink->OnEvent(uLine, Sink.m_uParam);
					}

				pClr[uSet] = dwBit;
						
				fFind = true;
				}

			dwPend ^= dwBit;

			continue;
			}

		uBase += 32;
		}

	return fFind;
	}

bool CTzic51::EnableLine(UINT uLine)
{
	if( uLine < m_uLines ) {

		PVDWORD pSet   = &Reg(EnableSet);

		pSet[uLine/32] = 1 << (uLine%32);

		return true;
		}

	return false;
	}

bool CTzic51::DisableLine(UINT uLine)
{
	if( uLine < m_uLines ) {

		PVDWORD pClr   = &Reg(EnableClr);

		pClr[uLine/32] = 1 << (uLine%32);

		return true;
		}

	return false;
	}

// End of File
