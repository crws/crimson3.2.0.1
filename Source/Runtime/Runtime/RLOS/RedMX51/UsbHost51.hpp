
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_iMX51_UsbHost_HPP
	
#define	INCLUDE_iMX51_UsbHost_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbBase51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 USB Host Controller
//

class CUsbHost51 : public CUsbBase51, public IUsbHostHardwareDriver
{
	public:
		// Constructor
		CUsbHost51(UINT iIndex);

		// Destructor
		~CUsbHost51(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbDriver
		BOOL METHOD Bind(IUsbEvents *pEvents);
		BOOL METHOD Bind(IUsbDriver *pDriver);
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);
	
		// IUsbHostHardwareDriver
		DWORD METHOD GetBaseAddr(void);
		UINT  METHOD GetMemAlign(UINT uType);
		void  METHOD MemCpy(PVOID d, PCVOID s, UINT n);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Data
		IUsbHostHardwareEvents * m_pDriver;
	};

// End of File

#endif
