 
#include "Intern.hpp"

#include "SdHost51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Gpio51.hpp"

#include "Ccm51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 SD Host Controller
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Instantiator

IDevice * Create_SdHost51(CCcm51 *pCcm)
{
	CSdHost51 *pDevice = New CSdHost51(pCcm);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CSdHost51::CSdHost51(CCcm51 *pCcm)
{
	m_pBase    = PVDWORD(ADDR_ESDHC1);

	m_uLine    = INT_SDHC1;

	m_uClock   = pCcm->GetFreq(CCcm51::clkSdhc1);

	m_uDetect  = 0;

	m_uProtect = 1;

	m_uVoltage = Bit(19);

	AfxGetObject("gpio", 0, IGpio, m_pGpio);

	m_pTimer    = CreateTimer();

	m_pCommand  = Create_AutoEvent();

	m_pTransfer = Create_AutoEvent();

	m_pWrite    = Create_AutoEvent();

	m_pRead     = Create_AutoEvent();

	m_pFailCmd  = Create_AutoEvent();

	m_pFailData = Create_AutoEvent();
	}

// Destructor

CSdHost51::~CSdHost51(void)
{
	m_pGpio->Release();

	m_pTimer->Release();

	m_pCommand->Release();

	m_pTransfer->Release();

	m_pWrite->Release();

	m_pRead->Release();

	m_pFailCmd->Release();

	m_pFailData->Release();
	}

// IDevice

BOOL METHOD CSdHost51::Open(void)
{
	if( CSdHostGeneric::Open() ) {

		m_pTimer->SetPeriod(50);
		
		m_pTimer->SetHook(this, 1);

		m_pTimer->Enable(true);
		
		return TRUE;
		}

	return FALSE;
	}

// IEventSink

void CSdHost51::OnEvent(UINT uLine, UINT uParam)
{
	if( uParam == 0 ) {

		if( Reg(IrqStat) & irqFailCmd ) {

			m_pFailCmd->Set();

			Reg(IrqStat) = irqCommand;

			Reg(IrqStat) = irqFailCmd;
			}

		if( Reg(IrqStat) & irqFailData ) {

			m_pFailData->Set();

			Reg(IrqStat) = irqTransfer | irqWrite | irqRead;

			Reg(IrqStat) = irqFailData;
			}

		if( Reg(IrqStat) & irqCommand ) {
		
			m_pCommand->Set();

			Reg(IrqStat) = irqCommand;
			}

		if( Reg(IrqStat) & irqTransfer ) {

			m_pTransfer->Set();

			Reg(IrqStat) = irqTransfer;
			}

		if( Reg(IrqStat) & Reg(IrqLineEn) & irqWrite ) {
		
			Reg(IrqLineEn) &= ~irqWrite;

			Reg(IrqStat)    = irqWrite;

			m_pWrite->Set();
			}

		if( Reg(IrqStat) & Reg(IrqLineEn) & irqRead ) {
		
			Reg(IrqLineEn) &= ~irqRead;

			Reg(IrqStat)    = irqRead;

			m_pRead->Set();
			}
		}

	if( uParam == 1 ) {

		switch( m_Card ) {
			
			case cardNone:

				if( !m_pGpio->GetState(m_uDetect) ) {

					InitController();

					m_uBounce = 10;

					m_Card    = cardBounce;
					}

				break;

			case cardBounce:

				if( !m_pGpio->GetState(m_uDetect) ) {

					if( !--m_uBounce ) {

						SetClockSpeed(clockSlow);

						SendInitClocks();

						m_Card = cardInserted;

						FireCardEvent();
						}

					break;
					}

				m_Card = cardNone;

				break;

			default:
				if( m_pGpio->GetState(m_uDetect) ) {

					StopController();

					m_Card = cardNone;

					FireCardEvent();
					}
				
				break;
			}
		}
	}

// Controller Interface

void CSdHost51::InitController(void)
{
	Reg(IrqStatEn) = irqAll;

	Reg(IrqStat  ) = irqAll;

	Reg(IrqLineEn) = irqAll;

	Reg(ProCtl   ) = 0x00000020;

	Reg(SysCtl   ) = 0x000E0000;

	Reg(Watermark) = 0x10100101;

	ClearEvents();
	
	EnableEvents();
	}

void CSdHost51::StopController(void)
{
	DisableEvents();
	
	Reg(SysCtl) = Bit(24);
	}

void CSdHost51::SetClockSpeed(EClock Clock)
{
	DWORD dwReg = Reg(SysCtl);

	Reg(SysCtl) = dwReg & ~Bit(3);

	UINT  uFreq = 400000;

	bool  fFast = false;

	UINT  uFact = 0;

	switch( Clock ) {

		case clockSlow:

			uFreq = 400000;
			
			fFast = false;
			
			break;

		case clock25MHz:

			uFreq = 25000000;
			
			fFast = false;
			
			break;

		case clock50MHz:

			uFreq = 50000000;
			
			fFast = true;
			
			break;
		}

	for(;;) {

		UINT uNext = uFact + (uFact ? uFact : 1);

		if( m_uClock / uNext > uFreq ) {

			uFact = uNext;

			continue;
			}

		break;
		}

	dwReg &= 0xFFFF000F;

	dwReg |= uFact << 8;

	Reg(SysCtl) = dwReg;

	WaitClockStable();
	
	Reg(SysCtl) = dwReg | Bit(3);

	SetBusWidth(fFast);
	}

void CSdHost51::SetBusWidth(bool fWide)
{
	if( fWide ) {
		
		AtomicBitSet(&Reg(ProCtl), 1);
		}
	else {
		AtomicBitClr(&Reg(ProCtl), 1);
		}
	}

void CSdHost51::SetLength(UINT uBytes)
{
	Reg(BlkAttr) = uBytes;
	}

// Blocking Calls

void CSdHost51::ClearEvents(void)
{
	m_pCommand->Clear();

	m_pTransfer->Clear();

	m_pWrite->Set();

	m_pRead->Clear();

	m_pFailCmd->Clear();

	m_pFailData->Clear();
	
	m_fBusy = false;
	}

bool CSdHost51::WaitCmdComplete(void)
{
	return WaitMultiple(m_pCommand, m_pFailCmd, 50) == 1;
	}

bool CSdHost51::SendData(PCBYTE pData, UINT uCount)
{
	if( uCount % 4 == 0 ) {

		UINT nSend = 0;

		while( uCount ) {

			UINT uWait = WaitMultiple(m_pWrite, m_pFailData, 50);

			if( uWait == 1 ) {

				while( (Reg(State) & Bit(10) ) && uCount ) {

					Reg(DatPort) = ((PDWORD) pData)[nSend++];

					uCount -= 4;
					}
				
				Reg(IrqStat)    = irqWrite;
			
				Reg(IrqLineEn) |= irqWrite;

				continue;
				}

			return false;
			}

		return true;
		}

	return false;
	}

bool CSdHost51::ReadData(PBYTE pData, UINT uCount)
{
	if( uCount % 4 == 0 ) {

		UINT uRecv = 0;

		while( uCount ) {

			UINT uWait = WaitMultiple(m_pRead, m_pFailData, 50);

			if( uWait == 1 ) {

				while( (Reg(State) & Bit(11)) && uCount ) {

					((PDWORD) pData)[uRecv++] = Reg(DatPort);
					
					uCount -= 4;
					}
							
				Reg(IrqStat)    = irqRead;

				Reg(IrqLineEn) |= irqRead;
		
				continue;
				}

			return false;
			}

		return true;
		}
		
	return false;
	}

bool CSdHost51::CheckBusy(void)
{
	if( m_fBusy ) {

		UINT uWait = WaitMultiple(m_pTransfer, m_pFailData, 1000);

		m_fBusy	= false;

		while( Reg(State) & (Bit(0) | Bit(1)) ) {

			Sleep(5);
			}

		return uWait == 1;
		}

	return true;
	}

// Response Offsets

void CSdHost51::OffsetStructure(PBYTE b)
{
	memmove(b + 1, b, (128 - 8) / 8);

	b[0] = 0;
	}

// Implementation

void CSdHost51::SendInitClocks(void)
{
	Reg(SysCtl) |= Bit(27);

	while( Reg(SysCtl) & Bit(27) );
	}

void CSdHost51::WaitClockStable(void)
{
	while( !(Reg(State) & Bit(3)) );
	}

void CSdHost51::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);
	}

void CSdHost51::DisableEvents(void)
{
	phal->EnableLine(m_uLine, false);
	}

// End of File
