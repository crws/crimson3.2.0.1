
#include "Intern.hpp"

#include "Touch51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Pmui51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice * Create_BatterMonitor(IBatteryMonitor *pBattMon);

//////////////////////////////////////////////////////////////////////////
//
// iMX51 PMUI Touch Screen Controller
//

// Instantiator

IDevice * Create_TouchScreen51(UINT uModel, CPmui51 *pPmui)
{
	IDevice *p = (IDevice *)(CTouchGeneric *) New CTouch51(uModel, pPmui);

	p->Open();

	return p;
	}

// Constructor

CTouch51::CTouch51(UINT uModel, CPmui51 *pPmui)
{
	m_uModel = uModel;

	m_pPmui  = pPmui;
	
	m_fFlipX = false;

	m_fFlipY = false;

	m_uState = stateInit;

	m_uSlot  = addrTouch;

	m_pTimer = CreateTimer();

	piob->RegisterSingleton("dev.batt", 0, Create_BatterMonitor(this));
	}

// Destructor

CTouch51::~CTouch51(void)
{
	piob->RevokeSingleton("dev.batt", 0);

	m_pTimer->Release();
	}

// IUnknown

HRM CTouch51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IBatteryMonitor);

	return CTouchGeneric::QueryInterface(riid, ppObject);
	}

ULM CTouch51::AddRef(void)
{
	return CTouchGeneric::AddRef();
	}

ULM CTouch51::Release(void)
{
	return CTouchGeneric::Release();
	}

// IDevice

BOOL METHOD CTouch51::Open(void)
{
	if( CTouchGeneric::Open() ) {

		m_yDisp += FindAdjust();

		LoadCalib();

		InitEvents();

		WaitReadBatt();

		return TRUE;
		}

	return FALSE;
	}

// BatterMonitor

bool METHOD CTouch51::IsBatteryGood(void)
{
	return m_uBat > 2400;
	}

bool METHOD CTouch51::IsBatteryBad(void)
{
	return IsBatteryGood() ? false : true;
	}

// IEventSink

void CTouch51::OnEvent(UINT uLine, UINT uParam)
{
	if( uParam == 0 ) {

		OnTimer();
		}
	else {
		OnTouch(uParam == 1);
		}
	}

// Events

void CTouch51::OnTimer(void)
{
	switch( m_uState ) {

		case stateWaitDown:

			if( m_uCount && !--m_uCount ) {

				WaitReadBatt();
				}

			break;

		case stateWaitUp:

			if( m_uCount == timeInitial ) {

				PostEvent(stateDown);
				}

			if( m_uCount && !--m_uCount ) {

				m_uCount = timeRepeat;

				PostEvent(stateRepeat);
				}

			break;
		}
	}

void CTouch51::OnTouch(bool fTouch)
{
	switch( m_uState ) {

		case stateReadBatt:

			m_uBat = m_pPmui->GetVBat();

			WaitStylusDown();

			break;

		case stateWaitDown:

			if( fTouch ) {

				WaitPosition();
				}
		
			break;

		case stateConvert:

			if( !fTouch ) {

				ReadTouch();

				if( m_uRead == 0 ) {					

					m_uRead ++;

					WaitPosition();
					
					break;
					}

				m_uRead ++;

				if( TestTouch() ) {
					
					if( IsValidTouch() ) {

						WaitStylusUp();
					
						m_uCount = timeInitial;

						m_uState = stateWaitUp;
					
						break;
						}
					}
				}

			WaitStylusDown();

			break;

		case stateWaitUp:

			if( fTouch ) {

				if( m_uCount < timeInitial ) {

					PostEvent(stateUp);
					}

				WaitStylusDown();
				}

			break;
		}
	}

// Implementation

void CTouch51::InitEvents(void)
{
	m_pPmui->AttachTouch(this, 1);

	m_pTimer->SetHook(this, 0);

	m_pTimer->SetPeriod(5);

	m_pTimer->Enable(true);
	}

void CTouch51::WaitReadBatt(void)
{
	m_uState = stateReadBatt;

	m_pPmui->ReqVBat();
	}

void CTouch51::WaitStylusDown(void)
{
	m_uRead  = 0;

	m_uState = stateWaitDown;

	m_uCount = timeReadBatt;

	m_pPmui->ReqPress();
	}

void CTouch51::WaitStylusUp(void)
{
	m_pPmui->ReqPress();
	}

void CTouch51::WaitPosition(void)
{
	m_uState = stateConvert;

	m_pPmui->ReqTouch();
	}

void CTouch51::ReadTouch(void)
{
	UINT &x0 = m_x0[m_uRead];
	UINT &x1 = m_x1[m_uRead];
	UINT &y0 = m_y0[m_uRead];
	UINT &y1 = m_y1[m_uRead];
	UINT &z0 = m_z0[m_uRead];
	UINT &z1 = m_z1[m_uRead];

	m_pPmui->GetTouch(x0, x1, y0, y1, z0, z1);
	}

bool CTouch51::TestTouch(void)
{
	#define	LIMIT_DELTA	10

	UINT xRaw = NOTHING;
	
	UINT yRaw = NOTHING;

	for( UINT n = 0; n < 2; n ++ ) {

		UINT &x0 = m_x0[n];
		UINT &x1 = m_x1[n];
		UINT &y0 = m_y0[n];
		UINT &y1 = m_y1[n];
		UINT &z0 = m_z0[n];
		UINT &z1 = m_z1[n];

		if( z0 != 1022 && z1 != 1022 ) {

			if( (Max(x0, x1) - Min(x0, x1)) <= LIMIT_DELTA ) {

				xRaw = (x0 + x1) / 2;
				}
			
			if( (Max(y0, y1) - Min(y0, y1)) <= LIMIT_DELTA ) {

				yRaw = (y0 + y1) / 2;
				}
			}
		}

	if( xRaw != NOTHING && yRaw != NOTHING ) {
		
		m_xRaw = xRaw;

		m_yRaw = yRaw;

		return true;
		}

	return false;
	}

int CTouch51::FindAdjust(void)
{
	switch( m_uModel ) {

		case MODEL_GRAPHITE_07	: return 69;
		case MODEL_GRAPHITE_09	: return 49;
		case MODEL_GRAPHITE_10S	: return 50;
		case MODEL_GRAPHITE_10V	: return 63;
		case MODEL_GRAPHITE_12	: return 52;
		case MODEL_GRAPHITE_15	: return 53;
		}

	return 0;
	}

// Overridables

void CTouch51::DefaultCalib(void)
{
	m_xMin = 0;

	m_yMin = 0;

	m_xMax = 1022;

	m_yMax = 1022;
	}

bool CTouch51::OnTouchValue(void)
{
	int xCell, yCell;

	int xDisp, yDisp;

	GetCellSize(xCell, yCell);

	m_pDisplay->GetSize(xDisp, yDisp);

	xDisp /= xCell;

	yDisp /= yCell;

	if( m_yPos > yDisp ) {

		int xIcon = xCell * 3;

		int  xMid = xDisp / 2;

		int xPt[] = { 
			
			xMid - xIcon * 3, 
			xMid - xIcon * 1, 
			xMid + xIcon * 1,
			xMid + xIcon * 3,

			};

		static const UINT uCode[] = {

			virtSoft1,
			virtSoft2,
			virtMenu,
			
			};

		for( UINT n = 0; n < elements(uCode); n ++ ) {

			if( m_xPos >= xPt[n+0] && m_xPos < xPt[n+1]) {
				
				m_uCode = uCode[n];

				return true;
				}
			}

		m_uCode = NOTHING;

		return false;
		}

	m_uCode = NOTHING;

	return IsActive();
	}

// End of File
