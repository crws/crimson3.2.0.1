
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Gpt51_HPP
	
#define	INCLUDE_Gpt51_HPP

//////////////////////////////////////////////////////////////////////////
//
// iMX51 General Purpose Timer
//

class CGpt51  
{
	public:
		// Constructor
		CGpt51(void);

		// Operations
		void SetPeriod(UINT uPeriod);
		void SpinDelay(UINT uDelay);

	protected:
		// Registers
		enum
		{
			regControl   = 0x0000 / sizeof(DWORD),
			regPrescale  = 0x0004 / sizeof(DWORD),
			regStatus    = 0x0008 / sizeof(DWORD),
			regInterrupt = 0x000C / sizeof(DWORD),
			regCompare1  = 0x0010 / sizeof(DWORD),
			regCompare2  = 0x0014 / sizeof(DWORD),
			regCompare3  = 0x0018 / sizeof(DWORD),
			regInput1    = 0x001C / sizeof(DWORD),
			regInput2    = 0x0020 / sizeof(DWORD),
			regCounter   = 0x0024 / sizeof(DWORD),
			};

		// Data Members
		PVDWORD m_pBase;
		UINT    m_uClock;
		UINT    m_uPeriod;

		// Implementation 
		void Reset(void);
	};

// End of File

#endif
