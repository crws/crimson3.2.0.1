
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Ccm51_HPP
	
#define	INCLUDE_Ccm51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDpll51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Clock Controller Module
//

class CCcm51
{
	public:
		// Constructor
		CCcm51(CDpll51 **ppDpll);

		// Attributes
		UINT GetSrc (UINT uClk) const;
		UINT GetFreq(UINT uClk) const;

		// Operations
		void SetFpm(UINT uMode);
		void EnableAmps(BOOL fEnable);
		bool Set(UINT uClk, UINT uSrc);
		bool Set(UINT uClk, UINT uSrc, UINT uPod);
		bool Set(UINT uClk, UINT uSrc, UINT uPre, UINT uPod);
		void SetGating(bool fOn);
		void SetTestOutput(void);
		void SetArmPod(UINT uPod);

		// Clock Identifiers
		enum
		{
			clkPll1		=  1,
			clkPll2		=  2,
			clkPll3		=  3,
			clkLpArm	=  4,
			clkArmRoot	=  5,
			clkAxia		=  6,
			clkAxib		=  7,
			clkEmi		=  8,
			clkAhb		=  9,
			clkIpg		= 10,
			clkPer		= 11,
			clkSync		= 12,
			clkGpu2		= 13,
			clkArmAxi	= 14,
			clkIpuHsp	= 15,
			clkGpu		= 16,
			clkVpu		= 17,
			clkEnfc		= 18,
			clkUsboh3	= 21,
			clkSdhc1	= 22,
			clkSdhc3	= 23,
			clkSdhc4	= 24,
			clkSdhc2	= 25,
			clkUart		= 26,
			clkSsi1		= 27,
			clkSsi3		= 28,
			clkSsi2		= 29,
			clkSsiExt1	= 30,
			clkSsiExt2	= 31,
			clkUsbPhy	= 32,
			clkTv		= 33,
			clkDi1		= 34,
			clkVpuR		= 35,
			clkSpdif0	= 36,
			clkSpdif1	= 37,
			clkSim		= 39,
			clkFiri		= 40,
			clkHsI2c	= 41,
			clkSsi		= 42,
			clkDdr		= 45,
			clkMclk1	= 47,
			clkMclk2	= 48,
			clkEcspi	= 49,
			clkWrck		= 50,
			clkLpsr		= 51,
			clkPgc		= 52,
			clkOsc		= 99
			};

		// FPM Multiplier
		enum 
		{
			fpmOff,
			fpm512,
			fpm1024,
			};
		
	protected:
		// Registers
		enum
		{
			regCtrl		= 0x0000 / sizeof(DWORD),
			regCtrlDiv	= 0x0004 / sizeof(DWORD),
			regStatus	= 0x0008 / sizeof(DWORD),
			regClkSwitch	= 0x000C / sizeof(DWORD),
			regArmClkRoot	= 0x0010 / sizeof(DWORD),
			regBusClkDiv	= 0x0014 / sizeof(DWORD),
			regBusClkMux	= 0x0018 / sizeof(DWORD),
			regSerClkMux1	= 0x001C / sizeof(DWORD),
			regSerClkMux2	= 0x0020 / sizeof(DWORD),
			regSerClkDiv1	= 0x0024 / sizeof(DWORD),
			regSSI1Div	= 0x0028 / sizeof(DWORD),
			regSSI2Div	= 0x002C / sizeof(DWORD),
			regDIClkDiv	= 0x0030 / sizeof(DWORD),
			regSerClkDiv2	= 0x0038 / sizeof(DWORD),
			regSerClkDiv3	= 0x003C / sizeof(DWORD),
			regSerClkDiv4	= 0x0040 / sizeof(DWORD),
			regWakeup	= 0x0044 / sizeof(DWORD),
			regDivHandshake	= 0x0048 / sizeof(DWORD),
			regDVFSCtrl	= 0x004C / sizeof(DWORD),
			regTesting	= 0x0050 / sizeof(DWORD),
			regLowPwrCtrl	= 0x005C / sizeof(DWORD),
			regIntStat	= 0x0050 / sizeof(DWORD),
			regIntMask	= 0x0054 / sizeof(DWORD),
			regClkOpSrc	= 0x0060 / sizeof(DWORD),
			regGeneral	= 0x0064 / sizeof(DWORD),
			regClkGate0	= 0x0068 / sizeof(DWORD),
			regClkGate1	= 0x006C / sizeof(DWORD),
			regClkGate2	= 0x0070 / sizeof(DWORD),
			regClkGate3	= 0x0074 / sizeof(DWORD),
			regClkGate4	= 0x0078 / sizeof(DWORD),
			regClkGate5	= 0x007C / sizeof(DWORD),
			regClkGate6	= 0x0080 / sizeof(DWORD),
			regModEnableOvr	= 0x0084 / sizeof(DWORD),
			};

		// Data Members
		PVDWORD    m_pBase;
		CDpll51 ** m_ppDpll;

		// Sources
		UINT GetArmSrc (void) const;
		UINT GetAxxSrc (void) const;
		UINT GetEmiSrc (void) const;
		UINT GetPerSrc (void) const;
		UINT GetGpu2Src(void) const;
		UINT GetAxiSrc (void) const;
		UINT GetIpuSrc (void) const;
		UINT GetGpuSrc (void) const;
		UINT GetVpuSrc (void) const;
		UINT GetSdh1Src(void) const;
		UINT GetSdh2Src(void) const;
		UINT GetUartSrc(void) const;
		UINT GetUsbSrc (void) const;
		UINT GetPhySrc (void) const;
		UINT GetSpiSrc (void) const;
		UINT GetDdrSrc (void) const;
		
		void SetArmSrc (UINT uSrc);
		void SetAxxSrc (UINT uSrc);
		void SetEmiSrc (UINT uSrc);
		void SetPerSrc (UINT uSrc);
		void SetGpu2Src(UINT uSrc);
		void SetAxiSrc (UINT uSrc);
		void SetIpuSrc (UINT uSrc);
		void SetGpuSrc (UINT uSrc);
		void SetVpuSrc (UINT uSrc);
		void SetSdh1Src(UINT uSrc);
		void SetSdh2Src(UINT uSrc);
		void SetUartSrc(UINT uSrc);
		void SetUsbSrc (UINT uSrc);
		void SetPhySrc (UINT uSrc);
		void SetSpiSrc (UINT uSrc);
		void SetDdrSrc (UINT uSrc);

		// Dividers
		UINT GetArmDiv (void) const;
		UINT GetAxiaDiv(void) const;
		UINT GetAxibDiv(void) const;
		UINT GetAhbDiv (void) const;
		UINT GetEmiDiv (void) const;
		UINT GetIpgDiv (void) const;
		UINT GetPerDiv (void) const;
		UINT GetSdh1Div(void) const;
		UINT GetSdh2Div(void) const;
		UINT GetUartDiv(void) const;
		UINT GetUsbDiv (void) const;
		UINT GetPhyDiv (void) const;
		UINT GetSpiDiv (void) const;
		UINT GetDdrDiv (void) const;
		UINT GetEnfcDiv(void) const;

		void SetArmDiv (UINT uDiv);
		void SetAxiaDiv(UINT uDiv);
		void SetAxibDiv(UINT uDiv);
		void SetAhbDiv (UINT uDiv);
		void SetEmiDiv (UINT uDiv);
		void SetIpgDiv (UINT uDiv);
		void SetPerDiv (UINT uPre1, UINT uPre2, UINT uPod);
		void SetSdh1Div(UINT uPre, UINT uPod);
		void SetSdh2Div(UINT uPre, UINT uPod);
		void SetUartDiv(UINT uPre, UINT uPod);
		void SetUsbDiv (UINT uPre, UINT uPod);
		void SetPhyDiv (UINT uPre, UINT uPod);
		void SetSpiDiv (UINT uPre, UINT uPod);
		void SetDdrDiv (UINT uDiv);
		void SetEnfcDiv(UINT uDiv);

		// Implementation
		DWORD GetReg(UINT uReg, UINT nBit, UINT nBits) const;
		void  SetReg(UINT uReg, UINT nBit, UINT nBits, DWORD dwData);
		void  SetReg(UINT uReg, UINT nBit0, UINT nBits0, DWORD dwData0, UINT nBit1, UINT nBits1, DWORD dwData1);
	};

// End of File

#endif
