
#include "Intern.hpp"

#include "Pmui51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Gpio51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Power Management and User Interface Controller
//

// Constructor

CPmui51::CPmui51(void)
{
	m_uLine  = 8;

	m_uChan  = 0;

	m_pTouch = NULL;

	AfxGetObject("gpio", 0, IGpio, m_pGpio);

	AfxGetObject("spi", 0, ISpi, m_pSpi);
	
	Open();
	}

// Destructor

CPmui51::~CPmui51(void)
{
	m_pSpi->Release();

	m_pGpio->Release();
	}

// IDevice

BOOL METHOD CPmui51::Open(void)
{
	InitSpi();

	m_dwMask0 = int0Adc | int0Touch;

	m_dwMask1 = /*int1Rtc*/0;

	PutReg(regIntStat0,   0x00FFFFFF);
	
	PutReg(regIntStat1,   0x00FFFFFF);

	PutReg(regIntMask0,   ~m_dwMask0); 

	PutReg(regIntMask1,   ~m_dwMask1); 

	PutReg(regAcc0,       0x00000000);

	PutReg(regPwrCtrl0,   0x00000040);

	PutReg(regPwrCtrl1,   0x00000000);
	
	PutReg(regPwrCtrl2,   0x00000000);

	PutReg(regRtcAlarm,   0x0000FFFF);

	SetBit(regSwitchers4, 22);

	PutReg(regPwrMisc,    0x00018040);

	PutReg(regCharger0,   0x00480003);
	
	PutReg(regLed2,       0x00E00E00);

	PutReg(regLed3,       0x00001C00);

	AdcReset();
	
	AdcCalibrate();

	InitEvents();

	return TRUE;
	}

// IEventSink

void CPmui51::OnEvent(UINT uLine, UINT uParam)
{ 
	for(;;) {

		DWORD dwStatus = GetReg(regIntStat0);

		if( dwStatus ) {

			PutReg(regIntStat0, dwStatus);
			
			dwStatus &= m_dwMask0;

			if( dwStatus & int0Adc ) {

				OnAdc();
				}

			if( dwStatus & int0Touch ) {

				OnTouch();
				}

			continue;
			}

		dwStatus = GetReg(regIntStat1);
		
		if( dwStatus ) {

			PutReg(regIntStat1, dwStatus);
		
			dwStatus &= m_dwMask1;

			if( dwStatus & int1Rtc ) {

				OnRtc();
				}

			continue;
			}

		break;
		}
	}

// Attributes

DWORD CPmui51::GetIdent(void) 
{
	return GetReg(regId);
	}

// Voltage Regulators

UINT CPmui51::GetVAud(void) 
{
	UINT dwMode = GetReg(regRegMode1);

	if( dwMode & Bit(5) ) {
	
		DWORD dwCtrl = GetReg(regRegCtrl1);

		return EnumVAud((dwCtrl >> 4) & 0x3);
		}

	return 0;
	}

UINT CPmui51::GetVVid(void)
{
	UINT dwMode = GetReg(regRegMode1);

	if( dwMode & Bit(12) ) {
	
		DWORD dwCtrl = GetReg(regRegCtrl1);

		return EnumVVid((dwCtrl >> 2) & 0x3);
		}

	return 0;
	}

UINT CPmui51::GetVPll(void)
{
	UINT dwMode = GetReg(regRegMode0);

	if( dwMode & Bit(15) ) {
	
		DWORD dwCtrl = GetReg(regRegCtrl0);

		return EnumVPll((dwCtrl >> 9) & 0x3);
		}

	return 0;
	}

UINT CPmui51::GetVDig(void) 
{
	UINT dwMode = GetReg(regRegMode0);

	if( dwMode & Bit(9) ) {
	
		DWORD dwCtrl = GetReg(regRegCtrl0);

		return EnumVDig((dwCtrl >> 4) & 0x3);
		}

	return 0;
	}

UINT CPmui51::GetVCam(void)
{
	UINT dwMode = GetReg(regRegMode1);

	if( dwMode & Bit(6) ) {
	
		DWORD dwCtrl = GetReg(regRegCtrl0);

		return EnumVCam((dwCtrl >> 16) & 0x3);
		}

	return 0;
	}

UINT CPmui51::GetVSd(void)
{
	UINT dwMode = GetReg(regRegMode1);

	if( dwMode & Bit(18) ) {
	
		DWORD dwCtrl = GetReg(regRegCtrl1);

		return EnumVSd((dwCtrl >> 7) & 0x7);
		}

	return 0;
	}

UINT CPmui51::GetVGen1(void)
{
	UINT dwMode = GetReg(regRegMode0);

	if( dwMode & Bit(0) ) {
	
		DWORD dwCtrl = GetReg(regRegCtrl0);

		return EnumVGen(1, dwCtrl & 0x3);
		}

	return 0;
	}

UINT CPmui51::GetVGen2(void)
{
	DWORD dwMode = GetReg(regRegMode0);

	if( dwMode & Bit(12) ) {
	
		DWORD dwCtrl = GetReg(regRegCtrl0);

		return EnumVGen(2, (dwCtrl >> 6) & 0x7);
		}

	return 0;
	}

UINT CPmui51::GetVGen3(void)
{
	DWORD dwMode = GetReg(regRegMode1);

	if( dwMode & Bit(0) ) {
	
		DWORD dwCtrl = GetReg(regRegCtrl0);

		return EnumVGen(3, (dwCtrl >> 14) & 0x1);
		}
	
	return 0;
	}

UINT CPmui51::GetSw1(void)
{
	return GetSw(1);
	}

UINT CPmui51::GetSw2(void)
{
	return GetSw(2);
	}

UINT CPmui51::GetSw3(void)
{
	return GetSw(3);
	}

UINT CPmui51::GetSw4(void)
{
	return GetSw(4);
	}

UINT CPmui51::GetSw(UINT i)
{
	DWORD d = GetReg(regSwitchers0 + i - 1);

	return EnumSw(d & 0x1F, d & Bit(23));
	}

bool CPmui51::SetVAud(UINT uMv)
{
	if( uMv ) {

		for( UINT i = 0; EnumVAud(i); i ++ ) {

			if( uMv == EnumVAud(i) ) {

				SetReg(regRegCtrl1, i, 0x3, 4);
				
				SetBit(regRegMode1, 15);
				
				return true;
				}
			}

		return false;
		}
	else {
		ClrBit(regRegMode1, 15);

		return true;
		}
	}

bool CPmui51::SetVVid(UINT uMv)
{
	if( uMv ) {

		for( UINT i = 0; EnumVVid(i); i ++ ) {

			if( uMv == EnumVVid(i) ) {

				SetReg(regRegCtrl1, i, 0x3, 2);
				
				SetBit(regRegMode1, 12);
				
				return true;
				}
			}

		return false;
		}
	else {
		ClrBit(regRegMode1, 12);

		return true;
		}
	}

bool CPmui51::SetVPll(UINT uMv)
{
	if( uMv ) {

		for( UINT i = 0; EnumVPll(i); i ++ ) {

			if( uMv == EnumVPll(i) ) {

				SetReg(regRegCtrl0, i, 0x3, 9);
				
				SetBit(regRegMode0, 15);
				
				return true;
				}
			}

		return false;
		}
	else {
		ClrBit(regRegMode0, 15);

		return true;
		}
	}

bool CPmui51::SetVDig(UINT uMv)
{
	if( uMv ) {

		for( UINT i = 0; EnumVDig(i); i ++ ) {

			if( uMv == EnumVDig(i) ) {

				SetReg(regRegCtrl0, i, 0x3, 4);
				
				SetBit(regRegMode0, 9);
				
				return true;
				}
			}

		return false;
		}
	else {
		ClrBit(regRegMode0, 9);

		return true;
		}
	}

bool CPmui51::SetVCam(UINT uMv)
{
	if( uMv ) {

		for( UINT i = 0; EnumVCam(i); i ++ ) {

			if( uMv == EnumVCam(i) ) {

				SetReg(regRegCtrl0, i, 0x3, 16);
				
				SetReg(regRegMode1, 0x9, 0xF, 6);
				
				return true;
				}
			}

		return false;
		}
	else {
		ClrBit(regRegMode1, 6);

		return true;
		}
	}

bool CPmui51::SetVSd(UINT uMv)
{
	if( uMv ) {

		for( UINT i = 0; EnumVSd(i); i ++ ) {

			if( uMv == EnumVSd(i) ) {

				SetReg(regRegCtrl1, i, 0x7, 6);
				
				SetBit(regRegMode1, 18);
				
				return true;
				}
			}

		return false;
		}
	else {
		ClrBit(regRegMode1, 18);

		return true;
		}
	}

bool CPmui51::SetVGen1(UINT uMv)
{
	if( uMv ) {

		for( UINT i = 0; EnumVGen(1, i); i ++ ) {

			if( uMv == EnumVGen(1, i) ) {

				SetReg(regRegCtrl0, i, 0x3, 0);
				
				SetBit(regRegMode0, 0);
				
				return true;
				}
			}

		return false;
		}
	else {
		ClrBit(regRegMode0, 0);

		return true;
		}
	}

bool CPmui51::SetVGen2(UINT uMv)
{
	if( uMv ) {

		for( UINT i = 0; EnumVGen(2, i); i ++ ) {

			if( uMv == EnumVGen(2, i) ) {

				SetReg(regRegCtrl0, i, 0x7, 6);
				
				SetBit(regRegMode0, 12);
				
				return true;
				}
			}

		return false;
		}
	else {
		ClrBit(regRegMode0, 12);

		return true;
		}
	}

bool CPmui51::SetVGen3(UINT uMv)
{
	if( uMv ) {

		for( UINT i = 0; EnumVGen(3, i); i ++ ) {

			if( uMv == EnumVGen(3, i) ) {

				SetReg(regRegCtrl0, i, 0x1, 14);
				
				SetReg(regRegMode1, 0x9, 0xF, 0);
				
				return true;
				}
			}

		return false;
		}
	else {
		ClrBit(regRegMode1, 0);

		return true;
		}
	}

bool CPmui51::SetSw1(UINT uMv) 
{
	return SetSw(1, uMv);
	}

bool CPmui51::SetSw2(UINT uMv) 
{
	return SetSw(2, uMv);
	}

bool CPmui51::SetSw3(UINT uMv) 
{
	return SetSw(3, uMv);
	}

bool CPmui51::SetSw4(UINT uMv) 
{
	return SetSw(4, uMv);
	}

bool CPmui51::SetSw(UINT uSw, UINT uMv) 
{
	for( UINT n = 0; n < (uSw == 1 ? 1 : 2); n ++ ) {

		for( UINT i = 0; EnumSw(i, n); i ++ ) {

			if( uMv == EnumSw(i, n) ) {

				SetReg(regSwitchers0 + uSw - 1, 0, 0x1, 23);

				SetReg(regSwitchers0 + uSw - 1, i, 0x1F, 0);
				
				SetReg(regSwitchers0 + uSw - 1, n, 0x1, 23);

				return true;
				}
			}
		}

	return false;
	}

UINT CPmui51::EnumVAud(UINT n) const
{
	switch( n ) {
			
		case 0: return 2300;
		case 1: return 2500;
		case 2: return 2775;
		case 3: return 3000;
		}

	return 0;
	}

UINT CPmui51::EnumVVid(UINT n) const
{
	switch( n ) {
			
		case 0: return 2700;
		case 1: return 2775;
		case 2: return 2500;
		case 3: return 2600;
		}

	return 0;
	}

UINT CPmui51::EnumVPll(UINT n) const
{
	switch( n ) {
			
		case 0: return 1200;
		case 1: return 1250;
		case 2: return 1500;
		case 3: return 1800;
		}

	return 0;
	}

UINT CPmui51::EnumVDig(UINT n) const
{
	switch( n ) {
			
		case 0: return 1050;
		case 1: return 1250;
		case 2: return 1650;
		case 3: return 1800;
		}

	return 0;
	}

UINT CPmui51::EnumVCam(UINT n) const
{
	switch( n ) {
			
		case 0: return 2500;
		case 1: return 2600;
		case 2: return 2750;
		case 3: return 3000;
		}

	return 0;
	}

UINT CPmui51::EnumVSd(UINT n) const
{
	switch( n ) {
			
		case 0: return 1800;
		case 1: return 2000;
		case 2: return 2600;
		case 3: return 2700;
		case 4: return 2800;
		case 5: return 2900;
		case 6: return 3000;
		case 7: return 3150;
		}

	return 0;
	}

UINT CPmui51::EnumVGen(UINT i, UINT n) const
{
	switch( i ) {

		case 1:
			switch( n ) {
			
				case 0: return 1200;
				case 1: return 1500;
				case 2: return 2775;
				case 3: return 3150;
				}

			break;

		case 2:
			switch( n ) {

				case 0: return 1200;
				case 1: return 1500;
				case 2: return 1600;
				case 3: return 1800;
				case 4: return 2700;
				case 5: return 2800;
				case 6: return 3000;
				case 7: return 3150;
				}

			break;

		case 3:
			switch( n ) {
			
				case 0: return 1800;
				case 1: return 2900;
				}

			break;
		}

	return 0;
	}

UINT CPmui51::EnumSw(UINT n, bool fHi) const
{
	if( n < 32 ) {

		return (fHi ? 1100 : 600)  + n * 25;
		}

	return 0;
	}

// Real Time Clock

bool CPmui51::GetRtcValid(void)
{
	return GetReg(regIntStat1) & Bit(7) ? false : true;
	}

DWORD CPmui51::GetRtcTime(void)
{
	return GetReg(regRtcTime) & 0x1FFFF;
	}

DWORD CPmui51::GetRtcDays(void)
{
	return GetReg(regRtcDay) & 0x7FFF;
	}

DWORD CPmui51::GetRtcCal(void)
{
	return (GetReg(regRtcTime) >> 17) & 0x7F;
	}

bool CPmui51::SetRtcValid(void)
{
	return SetBit(regIntStat1, 7); 
	}

bool CPmui51::SetRtcTime(DWORD dwTime)
{
	return SetReg(regRtcTime, dwTime, 0x1FFFF, 0); 
	}

bool CPmui51::SetRtcDays(DWORD dwDay)
{
	return SetReg(regRtcDay, dwDay, 0x7FFF, 0); 
	}

bool CPmui51::SetRtcCal(DWORD dwCal)
{
	DWORD dwCalMode = 3 << 5;

	return SetReg(regRtcTime, (dwCalMode | dwCal), 0x7F, 17); 
	}

// Memory 

DWORD CPmui51::GetMemA(void)
{
	return GetReg(regMemoryA);
	}

DWORD CPmui51::GetMemB(void)
{
	return GetReg(regMemoryB);
	}

bool CPmui51::SetMemA(DWORD dwData)
{
	return PutReg(regMemoryA, dwData);
	}

bool CPmui51::SetMemB(DWORD dwData)
{
	return PutReg(regMemoryB, dwData);
	}

// Touch

void CPmui51::AttachTouch(IEventSink *pEvent, UINT uParam)
{
	m_pTouch = pEvent;

	m_uParam = uParam;
	}

void CPmui51::ReqPress(void)
{
	PutReg(regAdc1, adcNoTrigger);

	PutReg(regAdc0, adcTouchMode0 | adcChargeScale);
	}

void CPmui51::ReqTouch(void)
{
	PutReg(regAdc0, accTouchRefOn | adcTouchMode1 | adcChargeScale);

	PutReg(regAdc1, adcEnable | adcNoTrigger);

	PutReg(regAdc1, adcEnable | adcNoTrigger | adcSelectTouch | (50 << 11) | adcAtoxDelayOn | adcStart);
	}

void CPmui51::GetTouch(UINT &x0, UINT &x1, UINT &y0, UINT &y1, UINT &z0, UINT &z1)
{
	for( UINT n = 0; n < 3; n ++ ) {

		UINT uChan = n * 3;

		PutReg(regAdc1, ((n * 3 + 0) << 5) | ((n * 3 + 1) << 8));

		DWORD dwValue = GetReg(regAdc2);

		WORD w0 = (dwValue >>  2) & 0x3FF;

		WORD w1 = (dwValue >> 14) & 0x3FF;

		switch( n ) {

			case 0:	x0 = w0; x1 = w1; break;
			case 1:	y0 = w0; y1 = w1; break;
			case 2:	z0 = w0; z1 = w1; break;
			}
		}
	}

// ADC

UINT CPmui51::GetVBat(void)
{
	DWORD dw = (GetReg(regAdc2) >> 2) & 0x3FF;

	return (dw * 3600) / 1024;
	}

void CPmui51::ReqVBat(void)
{
	PutReg(regAdc0, adcLithium | adcChargeScale);

	PutReg(regAdc1, adcEnable | adcNoTrigger);

	PutReg(regAdc1, adcEnable | adcNoTrigger | (6 << 5) | (255 < 11) | adcStart);
	}

UINT CPmui51::GetVBatTherm(void)
{
	DWORD dw = (GetReg(regAdc2) >> 2) & 0x3FF;

	return (dw * 2400) / 1024;
	}

void CPmui51::ReqVBatTherm(void)
{
	PutReg(regAdc0, adcLithium | adcChargeScale);

	PutReg(regAdc1, adcEnable | adcNoTrigger);

	PutReg(regAdc1, adcEnable | adcNoTrigger | (5 << 5) | (255 < 11) | adcStart);
	}

// Charger

bool CPmui51::ChargerResetTimer(void)
{
	return SetBit(regCharger0, 19);
	}

// Signaling Leds

void CPmui51::SetLed(UINT uLed, int nPercent)
{
	DWORD Mask = 0x1F;

	UINT  Data = (nPercent * Mask) / 100;

	switch( uLed ) {

		case 0: 
			SetReg(regLed3, Data, Mask, 3);

			break;

		case 1: 
			SetReg(regLed2, Data, Mask, 3);

			break;

		case 2: 
			SetReg(regLed2, Data, Mask, 15);

			break;
		}
	}

// Init

void CPmui51::InitSpi(void)
{
	CSpiConfig Config;

	Config.m_uFreq      = 10000000;
	Config.m_fClockIdle = false;
	Config.m_fDataIdle  = false;
	Config.m_uSelect    = 1;
	Config.m_uClockPol  = 0;
	Config.m_uClockPha  = 0;
	Config.m_fBurst	    = false;
	
	m_pSpi->Init(m_uChan, Config);
	}

void CPmui51::InitEvents(void)
{
	m_pGpio->SetDirection(m_uLine, false);

	m_pGpio->SetIntHandler(m_uLine, intLevelHi, this, 0);

	m_pGpio->SetIntEnable(m_uLine, true);
	}

// Events

void CPmui51::OnAdc(void)
{
	if( m_pTouch ) {

		m_pTouch->OnEvent(NOTHING, m_uParam + 1);
		}
	}

void CPmui51::OnTouch(void)
{
	if( m_pTouch ) {

		m_pTouch->OnEvent(NOTHING, m_uParam + 0);
		}
	}

void CPmui51::OnRtc(void)
{
	AfxTrace("[On Tick]");
	}

// ADC

void CPmui51::AdcReset(void)
{
	PutReg(regAdc1, 0);

	PutReg(regAdc0, adcReset);

	while( GetReg(regAdc0) & adcReset ); 
	}

void CPmui51::AdcWait(void)
{
	while( !(GetReg(regIntStat0) & int0Adc) );

	PutReg(regIntStat0, int0Adc);
	}

void CPmui51::AdcCalibrate(void)
{
	// ADC calibration does not always complete/start. Recovery mechanism is to 
	// de-asserts ASC bit before re-asserting it; sometimes a dummy read is required.  
	// Occasionally an erroneous calibration affects the touch screen. Fix is to do a 
	// minimum of 5 calibrations, and then check the last against the median. 

	const UINT constPassFail   = 5;

	const UINT constDataReads  = 5;

	const UINT constMinSamples = 5;

	const UINT constMaxSamples = 20;

	const UINT constAdcTimeout = 20;

	UINT uData[constMaxSamples];

	UINT uSamp = 0;

	UINT nPass = 0;
	
	for( nPass = 0; nPass < constMaxSamples; nPass ++ ) {

		PutReg(regAdc1, adcEnable | adcNoTrigger | adcCalibrate | adcStart);

		UINT x;

		for( x = 0; x < constAdcTimeout; x ++ ) {

			if( !(GetReg(regAdc1) & adcCalibrate) ) {

				PutReg(regIntStat0, int0Adc);

				UINT uRead = 0;

				for( UINT s = 0; s < constDataReads; s ++ ) {
						
					ReqVBatTherm();

					AdcWait();

					uRead += GetVBatTherm();
					}

				uRead /= constDataReads;

				uData[uSamp++] = uRead;

				if( uSamp >= constMinSamples ) {

					qsort(uData, uSamp, sizeof(UINT), CompareAdc);

					UINT uMedian = uData[uSamp/2];

					/*AfxTrace("Last=%d, Min=%d, Max=%d, Median=%d, Samples=%d, Attempts=%d\n", uRead, uData[0], uData[uSamp-1], uMedian, uSamp, nPass+1);*/

					if( uRead >= (uMedian - constPassFail) && uRead <= (uMedian + constPassFail) ) {

						return;
						}
					}

				break;
				}
			}

		if( x == constAdcTimeout ) {

			PutReg(regAdc1, adcEnable | adcNoTrigger);
	
			PutReg(regIntStat0, int0Adc);

			ReqVBatTherm();

			AdcWait();
			}

		PutReg(regAdc1, adcEnable | adcNoTrigger);
		}

	AfxTrace("Adc Calibration Failed (Attempts=%d, Samples=%d).\n", nPass, uSamp);

	HostMaxIpr();

	for(;;);
	}

// Register Helpers

BOOL CPmui51::SetBit(UINT uReg, UINT nBit)
{
	return PutReg(uReg, GetReg(uReg) | Bit(nBit));
	}

BOOL CPmui51::ClrBit(UINT uReg, UINT nBit)
{
	return PutReg(uReg, GetReg(uReg) & ~Bit(nBit));
	}

BOOL CPmui51::SetReg(UINT uReg, DWORD dwValue, DWORD dwMask, UINT uBit)
{
	DWORD dwData = GetReg(uReg);

	dwValue &=  dwMask;

	dwData  &= ~(dwMask  << uBit);

	dwData  |=  (dwValue << uBit);

	return PutReg(uReg, dwData);
	}

BOOL CPmui51::PutReg(UINT uReg, DWORD dwData)
{
	BYTE  bCmd = Bit(7) | (uReg << 1);

	DWORD Data = HostToMotor(dwData);

	return m_pSpi->Send(m_uChan, &bCmd, 1, PBYTE(&Data) + 1, 3);
	}

DWORD CPmui51::GetReg(UINT uReg) 
{
	BYTE  bCmd = uReg << 1;

	DWORD Data = 0;

	if( m_pSpi->Recv(m_uChan, &bCmd, 1, PBYTE(&Data) + 1, 3) ) {

		return HostToMotor(Data);
		}

	return 0;
	}

// Friends

int CompareAdc(const void *arg1, const void *arg2)
{
	UINT n1 = *(UINT *) arg1;	

	UINT n2 = *(UINT *) arg2;	

	return n1 == n2 ? 0 : n1 < n2 ? -1 : +1;
	}

// End of File
