
#include "Intern.hpp"

#include "UsbHost51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define Reg(x)   (m_pBase[reg##x + m_uBase])

//////////////////////////////////////////////////////////////////////////
//
// iMX51 USB Host Controller
//

// Instantiator

IUsbDriver * Create_UsbHost51(UINT iIndex)
{
	CUsbHost51 *p = New CUsbHost51(iIndex);

	return p;
	}

// Constructor

CUsbHost51::CUsbHost51(UINT iIndex) : CUsbBase51(iIndex)
{
	SetMode(modeHost);

	m_pDriver = NULL;
	}

// Destructor

CUsbHost51::~CUsbHost51(void)
{
	AfxRelease(m_pDriver);
	}

// IUnknown

HRESULT CUsbHost51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbHostHardwareDriver);

	StdQueryInterface(IUsbHostHardwareDriver);

	StdQueryInterface(IUsbDriver);

	return E_NOINTERFACE;
	}

ULONG CUsbHost51::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbHost51::Release(void)
{
	StdRelease();
	}

// IUsbDriver

BOOL CUsbHost51::Bind(IUsbEvents *pDriver)
{
	if( !m_pDriver && pDriver ) {

		pDriver->QueryInterface(AfxAeonIID(IUsbHostHardwareEvents), (void **) &m_pDriver);

		if( m_pDriver ) {

			m_pDriver->OnBind(this);
			
			return true;
			}
		}

	return false;
	}

BOOL CUsbHost51::Bind(IUsbDriver *pDriver)
{
	if( !m_pDriver && pDriver ) {

		pDriver->QueryInterface(AfxAeonIID(IUsbHostHardwareEvents), (void **) &m_pDriver);

		if( m_pDriver ) {

			m_pDriver->OnBind(this);
			
			return true;
			}
		}

	return false;
	}

BOOL CUsbHost51::Init(void)
{
	if( m_pDriver ) {

		Reg(PortSc1) &= ~(0x3 << 30);
	
		Reg(PortSc1) |=  (0x2 << 30);

		if( false ) {

			// Force full speed

			Reg(PortSc1) |= Bit(24);
			}

		m_pDriver->OnInit();

		return true;
		}

	return false;
	}

BOOL CUsbHost51::Start(void)
{
	if( m_pDriver ) {	

		m_pDriver->OnStart();

		EnableEvents();

		return true;
		}

	return false;
	}

BOOL CUsbHost51::Stop(void)
{
	if( m_pDriver ) {

		m_pDriver->OnStop();

		return true;
		}
	
	return false;
	}

// IUsbHostEnhancedDriver

DWORD CUsbHost51::GetBaseAddr(void)
{
	return DWORD(m_pBase + m_uBase) + 0x100;
	}

UINT CUsbHost51::GetMemAlign(UINT uType)
{
	switch( uType ) {

		case usbMemTransferDesc:  
			
			return 32;
		
		case usbMemQueueHeadDesc: 
			
			return 64;

		case usbMemPeriodicList:

			return 4096;
		}

	return 1;
	}

void CUsbHost51::MemCpy(PVOID d, PCVOID s, UINT n)
{
	ArmMemCpy(d, s, n);
	}

// IEventSink

void CUsbHost51::OnEvent(UINT uLine, UINT uParam)
{
	if( m_pDriver ) {

		m_pDriver->OnEvent();
		}
	}

// End of File
