
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PlatformRed_HPP

#define INCLUDE_PlatformRed_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/PlatformBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for RLOS Host
//

class CPlatformRed : public CPlatformBase
{
	public:
		// Constructor
		CPlatformRed(BOOL fRack);

		// Destructor
		~CPlatformRed(void);

		// IPlatform
		void METHOD ResetSystem(void);
		void METHOD SetWebAddr(PCTXT pAddr);

	protected:
		// Data Members
		BOOL		 m_fRack;
		IDiskManager   * m_pDiskMan;
		ISdHost        * m_pSdHost;
		IBlockDevice   * m_pBlkDev;
		IBlockDevice   * m_pUsbDev;
		UINT             m_iDriveC;
		UINT		 m_iDriveD;
		UINT		 m_iHostC;
		UINT		 m_iHostD;

		// Implementation
		BOOL RegisterRedCommon(void);
		bool BindFilingSystem(void);
		void FreeFilingSystem(void);
		void InitNicMac(UINT uNic);
		void FindSerial(void);
	};

// End of File

#endif
