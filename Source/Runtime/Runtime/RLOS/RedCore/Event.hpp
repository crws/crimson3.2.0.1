
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Implementation
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Event_HPP
	
#define	INCLUDE_Event_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Waitable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Event Implementation
//

class CEvent : public CWaitable, public IEvent
{
	public:
		// Constructor
		CEvent(CExecutive *pExec, bool fAuto);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// CWaitable
		bool WaitInit(CExecThread *pThread, UINT uSlot, bool fWait);
		void WaitDone(CExecThread *pThread, UINT uSlot);

		// IWaitable
		PVOID METHOD GetWaitable(void);
		BOOL  METHOD Wait(UINT uTime);
		BOOL  METHOD HasRequest(void);

		// IEvent
		bool METHOD Test(void);
		void METHOD Set(void);
		void METHOD Clear(void);
		void METHOD Pulse(void);

	protected:
		// Data Members
		ULONG         m_uRefs;
		bool 	      m_fAuto;
		BOOL volatile m_fSignal;

		// Implementation
		bool GetState(void);
	};

// End of File

#endif
