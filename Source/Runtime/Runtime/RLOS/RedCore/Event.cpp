
#include "Intern.hpp"

#include "Event.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Implementation
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Executive.hpp"

#include "ExecThread.hpp"

#include "BaseHal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Event Implementation
//

// Instantiators

IUnknown * CExecutive::Create_ManualEvent(PCTXT pName)
{
	return New CEvent(m_pThis, false);
	}

IUnknown * CExecutive::Create_AutoEvent(PCTXT pName)
{
	return New CEvent(m_pThis, true);
	}

// Constructor

CEvent::CEvent(CExecutive *pExec, bool fAuto) : CWaitable(pExec)
{
	StdSetRef();

	AfxAssert(sizeof(m_fSignal) == sizeof(int));

	m_fAuto   = fAuto;

	m_fSignal = FALSE;
	}

// IUnknown

HRESULT CEvent::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(IEvent);

	return E_NOINTERFACE;
	}

ULONG CEvent::AddRef(void)
{
	StdAddRef();
	}

ULONG CEvent::Release(void)
{
	StdRelease();
	}

// CWaitable

bool CEvent::WaitInit(CExecThread *pThread, UINT uSlot, bool fWait)
{
	if( m_Wake || !m_fSignal ) {

		if( !fWait ) {
			
			return true;
			}

		if( !AppendThread(pThread, uSlot) ) {

			return true;
			}

		if( !m_fSignal ) {

			return true;
			}

		RemoveThread(pThread, uSlot);
		}

	if( fWait && pThread->IsWaker(this) ) {

		return false;
		}

	if( m_fAuto ) {
		
		m_fSignal = FALSE;
		}

	return false;
	}

void CEvent::WaitDone(CExecThread *pThread, UINT uSlot)
{
	RemoveThread(pThread, uSlot);
	}

// IWaitable

PVOID CEvent::GetWaitable(void)
{
	return (CWaitable *) this;
	}

BOOL CEvent::Wait(UINT uTime)
{
	Hal_CheckMaxIrql(uTime ? IRQL_TASK : NOTHING);

	// This provides a fast path for waits that do not need to
	// block, avoiding the need to enter the more complex wait
	// code or to raise the system IRQL.

	if( m_fAuto ) {

		if( AtomicCompAndSwap(&m_fSignal, TRUE, FALSE) == TRUE ) {

			return true;
			}
		}
	else {
		if( m_fSignal ) {

			return true;
			}
		}

	if( !uTime ) {

		return false;
		}

	return m_pExec->InitWait(this, uTime);
	}

BOOL CEvent::HasRequest(void)
{
	return m_Wake;
	}

// IEvent

bool CEvent::Test(void)
{
	return m_fSignal;
	}

void CEvent::Set(void)
{
	UINT ipr;

	HostRaiseIpr(ipr);

	if( likely(!m_Wake) ) {

		// This provides a fast path to signal the object when
		// no-one is waiting, avoiding the need to raise the IRQL.

		m_fSignal = TRUE;

		HostLowerIpr(ipr);
		}
	else {
		HostLowerIpr(ipr);

		UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

		if( m_fAuto ) {

			if( !WakeOne(this) ) {

				m_fSignal = TRUE;
				}
			}
		else {
			m_fSignal = TRUE;

			WakeAll(this);
			}

		Hal_LowerIrql(irql);
		}
	}

void CEvent::Clear(void)
{
	m_fSignal = false;
	}

void CEvent::Pulse(void)
{
	if( m_Wake ) {

		UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

		WakeOne(this);

		m_fSignal = FALSE;

		Hal_LowerIrql(irql);
		}
	}

// End of File
