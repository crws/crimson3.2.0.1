
#include "Intern.hpp"

#include "BaseHal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Base Hardware Abstraction Layer
//

// Instance Pointers

global IHal * phal = NULL;

// Data Members

IExecHook   * CBaseHal::m_pExec  = NULL;
IMmu        * CBaseHal::m_pMmu   = NULL;
bool          CBaseHal::m_fExec  = false;
bool	      CBaseHal::m_fIdle  = false;
UINT volatile CBaseHal::m_uIdle  = 0;
UINT          CBaseHal::m_IRQL   = IRQL_DISPATCH;
UINT          CBaseHal::m_uTicks = 0;
UINT          CBaseHal::m_uDelay = 0;
INT           CBaseHal::m_nCrit  = 0;
UINT          CBaseHal::m_uSave  = 0;

// Constructor

CBaseHal::CBaseHal(void)
{
	phal   = this;

	m_IRQL = IRQL_TASK;
	}

// Destructor

CBaseHal::~CBaseHal(void)
{
	m_IRQL = IRQL_DISPATCH;

	phal   = NULL;
	}

// Initialization

void CBaseHal::Open(void)
{
	PrintWelcome();
	}

// Executive Hooks

void CBaseHal::SetExecHook(IExecHook *pExec)
{
	m_pExec = pExec;
	}

void CBaseHal::SetExecRunning(void)
{
	m_fExec = true;
	}

// Friends

UINT Hal_GetIrql(void)
{
	return CBaseHal::m_IRQL;
	}

bool Hal_Critical(bool fEnter)
{
	if( CBaseHal::m_fExec ) {

		if( fEnter ) {

			UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

			if( ++CBaseHal::m_nCrit == 1 ) {

				CBaseHal::m_uSave = irql;

				return true;
				}

			return false;
			}
		else {
			if( --CBaseHal::m_nCrit == 0 ) {

				Hal_LowerIrql(CBaseHal::m_uSave);

				return true;
				}

			return false;
			}
		}

	return false;
	}

// Power Management

void CBaseHal::EnterIdleState(void)
{
	bool volatile &idle = m_fIdle;

	EnterLowPower();

	while( idle ) WaitForInterrupt();
	}

void CBaseHal::LeaveIdleState(void)
{
	if( m_fIdle ) {

		LeaveLowPower();
		}
	}

void CBaseHal::RestartSystem(void)
{
	DebugWait();

	// TODO -- Transition to boot loader? !!!

	for(;;) HostMaxIpr();
	}

void CBaseHal::ShutdownSystem(void)
{
	DebugWait();

	// TODO -- Transition to boot loader? !!!

	for(;;) HostMaxIpr();
	}

// Timer Support

UINT CBaseHal::GetTimerResolution(void)
{
	return 5;
	}

UINT CBaseHal::TimeToTicks(UINT uTime)
{
	return uTime ? ((uTime < 5) ? 1 : (uTime / 5)) : 0;
	}

UINT CBaseHal::TicksToTime(UINT uTicks)
{	
	return uTicks * 5;
	}

UINT CBaseHal::GetTickCount(void)
{	
	return m_uTicks;
	}

void CBaseHal::SpinDelay(UINT uTime)
{
	m_uDelay = TimeToTicks(uTime);

	UINT volatile &uTicks = m_uDelay;

	while( uTicks ) WaitForInterrupt();
	}

// Debug Support

void CBaseHal::DebugRegister(void)
{
	}

void CBaseHal::DebugRevoke(void)
{
	}

// Memory Management

bool CBaseHal::IsVirtualValid(DWORD dwAddr, bool fWrite)
{
	return m_pMmu->IsVirtualValid(dwAddr, fWrite);
	}

DWORD CBaseHal::PhysicalToVirtual(DWORD dwAddr, bool fCached)
{
	return m_pMmu->PhysicalToVirtual(dwAddr, fCached);
	}

DWORD CBaseHal::VirtualToPhysical(DWORD dwAddr)
{
	return m_pMmu->VirtualToPhysical(dwAddr);
	}

DWORD CBaseHal::GetNonCachedAlias(DWORD dwAddr)
{
	return PhysicalToVirtual(VirtualToPhysical(dwAddr), false);
	}

DWORD CBaseHal::GetCachedAlias(DWORD dwAddr)
{
	return PhysicalToVirtual(VirtualToPhysical(dwAddr), true);
	}

// IEventSink

void CBaseHal::OnEvent(UINT uLine, UINT uParam)
{
	if( m_uDelay ) {

		m_uDelay--;
		}

	if( true ) {

		m_uTicks++;
		}

	if( m_pExec ) {

		m_pExec->ExecTick();
		}
	}

// Power Management Hooks

void CBaseHal::EnterLowPower(void)
{
	m_fIdle = true;
	}

void CBaseHal::LeaveLowPower(void)
{
	m_fIdle = false;
	}

// Implementation

void CBaseHal::PrintWelcome(void)
{
	AfxTrace("\n\n\n\n");

	AfxTrace("==========================================\n");

	AfxTrace("Welcome to Aeon RLOS!\n");
	}

bool CBaseHal::IsExecValid(void)
{
	return m_fExec;
	}

PCTXT CBaseHal::GetThreadName(void)
{
	return ::GetThreadName(::GetCurrentThread());
	}

PCTXT CBaseHal::GetContext(void)
{
	if( IsExecValid() ) {

		if( m_IRQL >= IRQL_HARDWARE ) {

			return "ISP";
			}

		return GetThreadName();
		}

	return "INIT";
	}

bool CBaseHal::ShowPanicCode(UINT Code)
{
	bool fatal = false;

	AfxPrint("\n*** ");

	switch( Code ) {

		case PANIC_HAL_BAD_MODE:
		case PANIC_EXECUTIVE_REAPER_BUSY:
		case PANIC_AEON_OUT_OF_MEMORY:
			fatal = true;
			break;
		}

	switch( Code ) {

		case PANIC_AEON_BREAKPOINT:
			AfxPrint("BREAKPOINT");
			break;

		case PANIC_AEON_NULL_CALL:
			AfxPrint("NULL CALL");
			break;

		case PANIC_AEON_PURE_VIRTUAL:
			AfxPrint("PURE VIRTUAL");
			break;

		case PANIC_AEON_BAD_TYPEID:
			AfxPrint("BAD TYPEID");
			break;

		case PANIC_AEON_BAD_CAST:
			AfxPrint("BAD CAST");
			break;
		
		case PANIC_AEON_ABORT:
			AfxPrint("ABORT CALLED");
			break;
		
		case PANIC_AEON_RAISE:
			AfxPrint("RAISE CALLED");
			break;

		case PANIC_AEON_OUT_OF_MEMORY:
			AfxPrint("OUT OF MEMORY");
			break;

		case PANIC_FAULT_DATA_ABORT:
			AfxPrint("DATA ABORT");
			break;

		case PANIC_FAULT_INST_ABORT:
			AfxPrint("INST ABORT");
			break;

		case PANIC_FAULT_UNDEFINED:
			AfxPrint("UNDEFINED");
			break;

		case PANIC_HAL_BAD_MODE:
			AfxPrint("BAD MODE");
			break;

		case PANIC_HAL_BAD_IRQL:
			AfxPrint("BAD IRQL");
			break;

		case PANIC_EXECUTIVE_UNSUPPORTED:
			AfxPrint("EXEC UNSUPPORTED");
			break;

		case PANIC_EXECUTIVE_WRONG_THREAD:
			AfxPrint("WRONG THREAD");
			break;

		case PANIC_EXECUTIVE_GUARD_ERROR:
			AfxPrint("GUARD ERROR");
			break;

		case PANIC_EXECUTIVE_UNEXPECTED:
			AfxPrint("EXEC UNEXPECTED");
			break;

		case PANIC_EXECUTIVE_REAPER_BUSY:
			AfxPrint("REPEAR BUSY");
			break;

		case PANIC_EXECUTIVE_OWNED_MUTEX:
			AfxPrint("OWNED MUTEX");
			break;

		default:
			switch( HIBYTE(Code) ) {

				case HIBYTE(PANIC_EXECUTIVE):
					AfxTrace("EXECUTIVE %2.2X", LOBYTE(Code));
					break;

				case HIBYTE(PANIC_DEVICE):
					AfxTrace("DEVICE %2.2X", LOBYTE(Code));
					break;

				case HIBYTE(PANIC_APPLICATION):
					AfxTrace("APPLICATION %2.2X", LOBYTE(Code));
					break;

				default:
					AfxTrace("PANIC %4.4X", Code);
					break;
				}
			break;
		}

	return fatal;
	}

// End of File
