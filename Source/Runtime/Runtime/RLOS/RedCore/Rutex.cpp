
#include "Intern.hpp"

#include "Rutex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Executive.hpp"

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Quick Mutex Object
//

// Instantiator

static IUnknown * Create_Rutex(PCTXT pName)
{
	return New CRutex;
	}

// Registration

global void Register_Rutex(void)
{
	piob->RegisterInstantiator("exec.rutex", Create_Rutex);
	}

// Constructor

CRutex::CRutex(void)
{
	StdSetRef();
	}

// IUnknown

HRESULT CRutex::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(IMutex);

	return E_NOINTERFACE;
	}

ULONG CRutex::AddRef(void)
{
	StdAddRef();
	}

ULONG CRutex::Release(void)
{
	StdRelease();
	}

// IWaitable

PVOID CRutex::GetWaitable(void)
{
	HostTrap(PANIC_EXECUTIVE_UNSUPPORTED);

	return NULL;
	}

BOOL CRutex::Wait(UINT uWait)
{
	if( likely(uWait == FOREVER || uWait == 0) ) {

		Hal_Critical(true);

		return TRUE;
		}

	HostTrap(PANIC_EXECUTIVE_UNSUPPORTED);

	return TRUE;
	}

BOOL CRutex::HasRequest(void)
{
	HostTrap(PANIC_EXECUTIVE_UNSUPPORTED);

	return FALSE;
	}

// IMutex

void CRutex::Free(void)
{
	Hal_Critical(false);
	}

// End of File
