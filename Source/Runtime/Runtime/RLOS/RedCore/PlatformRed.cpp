
#include "Intern.hpp"

#include "PlatformRed.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

extern void Register_HxUsb(void);
extern void Register_HxRack(void);
extern void Register_HxFiling(void);
extern void Register_HxTcpIp(void);
extern void Register_PosixFileCopier(void);

extern void Revoke_HxUsb(void);
extern void Revoke_HxRack(void);
extern void Revoke_HxFiling(void);
extern void Revoke_HxTcpIp(void);
extern void Revoke_PosixFileCopier(void);

//////////////////////////////////////////////////////////////////////////
//
// File Support Hook
//

// Externals

extern IUnknown * Create_RlosFileSupport(void);

// Instantiator

IUnknown * Create_FileSupport(void)
{
	return Create_RlosFileSupport();
}

//////////////////////////////////////////////////////////////////////////
//
// Default MAC Addresses
//

static const MACADDR MacRlc =

{ { 0x00, 0x05, 0xE4, 0x00, 0x00, 0x00 } };

static const MACADDR MacDef[] =
{
	{ { 0x00, 0x05, 0xE4, 0x00, 0xAA, 0xAA } },
	{ { 0x00, 0x05, 0xE4, 0x00, 0xAA, 0xAB } },

};

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for RLOS Host
//

// Constructor

CPlatformRed::CPlatformRed(BOOL fRack)
{
	m_fRack	   = fRack;

	m_pDiskMan = NULL;

	m_pBlkDev  = NULL;

	m_pUsbDev  = NULL;

	m_pSdHost  = NULL;

	m_iDriveC  = NOTHING;

	m_iDriveD  = NOTHING;
}

// Destructor

CPlatformRed::~CPlatformRed(void)
{
	FreeFilingSystem();

	Revoke_PosixFileCopier();

	Revoke_HxFiling();

	if( m_fRack ) {

		Revoke_HxRack();
	}

	Revoke_HxUsb();

	Revoke_HxTcpIp();
}

// IPlatform

void CPlatformRed::ResetSystem(void)
{
	phal->RestartSystem();
}

void CPlatformRed::SetWebAddr(PCTXT pAddr)
{
}

// Implementation

BOOL CPlatformRed::RegisterRedCommon(void)
{
	Register_HxTcpIp();

	Register_HxUsb();

	if( m_fRack ) {

		Register_HxRack();
	}

	Register_HxFiling();

	Register_PosixFileCopier();

	BindFilingSystem();

	return TRUE;
}

bool CPlatformRed::BindFilingSystem(void)
{
	AfxGetObject("diskman", 0, IDiskManager, m_pDiskMan);

	if( m_pDiskMan ) {

		AfxNewObject("sddrive", IBlockDevice, m_pBlkDev);

		AfxNewObject("usbdrive", IBlockDevice, m_pUsbDev);

		if( m_pBlkDev ) {

			AfxGetObject("sdhost", 0, ISdHost, m_pSdHost);

			m_pBlkDev->Open(m_pSdHost);

			m_iDriveC = m_pDiskMan->RegisterDisk(m_pBlkDev);

			m_iHostC  = m_pDiskMan->RegisterHost(m_iDriveC);
		}

		if( m_pUsbDev ) {

			m_iDriveD = m_pDiskMan->RegisterDisk(m_pUsbDev);

			m_iHostD  = m_pDiskMan->RegisterHost(m_iDriveD);
		}

		return true;
	}

	return false;
}

void CPlatformRed::FreeFilingSystem(void)
{
	if( m_pDiskMan ) {

		if( m_pBlkDev ) {

			m_pDiskMan->UnregisterHost(m_iDriveC, m_iHostC);
		}

		if( m_pUsbDev ) {

			m_pDiskMan->UnregisterHost(m_iDriveD, m_iHostD);
		}

		m_pDiskMan->Release();
	}
}

void CPlatformRed::InitNicMac(UINT uNic)
{
	AfxGetAutoObject(pNic, "nic", uNic, INic);

	if( pNic ) {

		CMacAddr *pMac = (CMacAddr *) &MacDef[uNic];

		MACADDR   Mac;

		AfxGetAutoObject(pFram, "fram", 0, ISerialMemory);

		if( pFram ) {

			UINT uAddr = uNic ? 0x0032 : 0x002C;

			if( pFram->GetData(uAddr, Mac.m_Addr, sizeof(Mac)) ) {

				if( !memcmp(&Mac, &MacRlc, 3) ) {

					pMac = (CMacAddr *) &Mac;
				}
			}
		}

		pNic->InitMac(*pMac);
	}
}

void CPlatformRed::FindSerial(void)
{
	CMacAddr Mac;

	AfxGetAutoObject(pNic, "dev.nic", 0, INic);

	if( pNic ) {

		pNic->ReadMac(Mac);
	}

	if( Mac.IsEmpty() ) {

		Mac.m_Addr[3] = BYTE(rand());
		Mac.m_Addr[4] = BYTE(rand());
		Mac.m_Addr[5] = BYTE(rand());
	}

	CPlatformBase::FindSerial(Mac);
}

// End of File
