
#include "Intern.hpp"

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Executive.hpp"

#include "Mutex.hpp"

#include "BaseHal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Close-Coupled APIs
//

HTHREAD GetCurrentThread(void)
{
	return CExecutive::m_pThread;
	}

global UINT GetThreadIndex(void)
{
	return CExecutive::m_pThread->GetIndex();
	}

global void CheckThreadCancellation(void)
{
	CExecutive::m_pThread->CheckCancellation();
	}

global void SetTimer(UINT uTime)
{
	CExecutive::m_pThread->SetTimer(uTime);
	}

global UINT GetTimer(void)
{
	return CExecutive::m_pThread->GetTimer();
	}

//////////////////////////////////////////////////////////////////////////
//
// Executive Thread Object
//

// Externals

clink void __sinit(struct _reent *);

// Constructor

CExecThread::CExecThread(CExecutive *pExec) : CBaseExecThread(pExec)
{
	m_pExec      = pExec;
	m_pOwnedHead = NULL;
	m_pOwnedTail = NULL;
	m_pAlias     = NULL;
	m_fReady     = true;
	m_fSuspend   = false;
	m_pExit      = NULL;
	m_uTimer     = 0;
	m_uSleep     = 0;
	m_uSlept     = 0;
	m_Slice      = 1500 * phal->GetTimerResolution();
	m_RunTime    = 0;
	m_AccTime    = 0;
	m_pWaker     = NULL;
	m_pWait      = NULL;
	m_uWait      = 0;

	memset(m_Wait, 0, sizeof(m_Wait));
	}

// Destructor

CExecThread::~CExecThread(void)
{
//	Info(0, "deleted\n");

	AfxRelease(m_pExit);

	m_pExec->RemoveThread(this);
	}

// IWaitable

PVOID CExecThread::GetWaitable(void)
{
	HostTrap(PANIC_EXECUTIVE_UNSUPPORTED);

	return NULL;
	}

BOOL CExecThread::Wait(UINT uWait)
{
	// This is alertable in RLOS but not otherwise... !!!

	return m_pExit->Wait(uWait);
	}

BOOL CExecThread::HasRequest(void)
{
	HostTrap(PANIC_EXECUTIVE_UNSUPPORTED);

	return FALSE;
	}

// IThread

UINT CExecThread::GetTimer(void)
{
	return ToTime(m_uTimer);
	}

void CExecThread::SetTimer(UINT uTime)
{
	m_uTimer = ToTicks(uTime);
	}

void CExecThread::CheckCancellation(void)
{
	Hal_CheckMaxIrql(IRQL_DISPATCH);

	CBaseExecThread::CheckCancellation();
	}

void CExecThread::Guard(BOOL fGuard)
{
	// !!! HACK !!!

	if( Hal_GetIrql() >= IRQL_HARDWARE ) {

		return;
		}

	// !!! HACK !!!

	if( CExecutive::m_pThread == this ) {

		Hal_CheckMaxIrql(IRQL_DISPATCH);

		if( fGuard ) {

			m_guard++;
			}
		else {
			if( !m_guard ) {

				HostTrap(PANIC_EXECUTIVE_GUARD_ERROR);
				}

			if( !--m_guard ) {

				m_pGuardProc = NULL;

				m_pGuardData = NULL;
				}
			}

		return;
		}

	HostTrap(PANIC_EXECUTIVE_WRONG_THREAD);
	}

void CExecThread::Guard(PGUARD pProc, PVOID pData)
{
	Hal_CheckMaxIrql(IRQL_DISPATCH);

	if( !m_pGuardProc ) {
	
		m_pGuardProc = pProc;

		m_pGuardData = pData;

		Guard(TRUE);

		return;
		}

	HostTrap(PANIC_EXECUTIVE_GUARD_ERROR);
	}

void CExecThread::Exit(int nCode)
{
	if( CExecutive::m_pThread == this ) {

		Hal_CheckMaxIrql(IRQL_DISPATCH);

		if( AtomicCompAndSwap(&m_state, 1, 2) == 1 ) {

			Info(1, "exiting\n");

			CExecCancel c;

			c.r = nCode;

			throw c;
			}

		// The thread is already being destroyed, so
		// just wait and allow the process to proceed.

		for(;;) Sleep(FOREVER);
		}

	HostTrap(PANIC_EXECUTIVE_WRONG_THREAD);
	}

BOOL CExecThread::Destroy(void)
{
	if( CExecutive::m_pAlias->m_pid == m_parent ) {

		Hal_CheckMaxIrql(IRQL_DISPATCH);

		if( m_state == 1 ) {

			CAutoGuard Guard(SafeToGuard());

			if( AtomicCompAndSwap(&m_state, 1, 2) == 1 ) {

				if( m_state == 3 ) {

					// The thread did returned or threw an exception while
					// we were waiting for any guards to be released, so
					// we don't need to kill the cancellation request.

					Info(1, "returned already\n");
					}
				else {
					// Call any user cancellation procedure.

					if( m_pGuardProc ) {

						(*m_pGuardProc)(this, m_pGuardData);

						m_pGuardProc = NULL;

						m_pGuardData = NULL;
						}

					// This set the cancellation and interrupts
					// any waits so that we're able to respond.

					UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

					SetCancelFlag();

					if( !m_guard && !m_fReady ) {

						m_fReady = true;

						m_uSleep = 0;

						m_pExec->Schedule(this);
						}

					Hal_LowerIrql(irql);
					}

				// If we're using thread state management, we do
				// not wait for an exit at this point as the thread
				// will just move to the next state.

				if( !m_pTms ) {

					// Wait for the thread to exit.

					m_pExit->Wait(FOREVER);

					// And it's now safe to release the thread. This ought
					// to delete it as the thread's own reference will have
					// been removed during its shutdown process.

					Release();
					}

				return TRUE;
				}
			}
		
		Release();

		return FALSE;
		}

	HostTrap(PANIC_EXECUTIVE_WRONG_THREAD);

	return FALSE;
	}

// Attributes

void CExecThread::GetFlagsAsText(char *pText) const
{
	pText[0] = (m_fReady    ? 'R' : '.');
	
	pText[1] = (m_uWait     ? 'W' : '.');
	
	pText[2] = (m_uSleep    ? 'T' : '.');
	
	pText[3] = (m_fSuspend  ? 'S' : '.');
	
	pText[4] = (m_guard     ? 'G' : '.');

	pText[5] = phal->ContextUsesFp(m_pCtx) ? 'F' : '.';

	pText[6] = ('0' + m_state);

	pText[7] = 0;
	}

void CExecThread::GetFuncName(char *pText) const
{
	if( m_state < 5 ) {

		if( CExecutive::m_pThread == this ) {

			strcpy(pText, "CExecThread::GetFuncName");

			return;
			}

		phal->GetFuncName(pText, 256, m_pCtx);

		return;
		}

	strcpy(pText, "Reaped");
	}

UINT CExecThread::GetStackUsage(void) const
{
	if( m_state < 5 ) {

		for( UINT n = 0; n < m_uStack; n++ ) {

			if( m_pStack[n] == 0x55555555 ) {

				continue;
				}

			return 1000 - (1000 * n / m_uStack);
			}

		return 1000;
		}

	return 0;
	}

// Operations

void CExecThread::Suspend(void)
{
	if( !m_fSuspend ) {

		UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

		if( CExecutive::m_pThread == this ) {

			// If we are suspending ourselves, we need
			// to schedule whatever thread is avavailable.

			m_fSuspend = true;

			m_pExec->Schedule();
			}
		else {
			// Otherwise we have to check whether our
			// group's active pointer needs updating.

			m_fSuspend = true;

			m_pExec->CheckGroup(this);
			}

		Hal_LowerIrql(irql);
		}
	}

UINT CExecThread::WaitMultiple(IWaitable **pList, UINT uCount, UINT uTime)
{
	UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

	if( m_cancel == 1 && m_guard == 0 ) {

		Hal_LowerIrql(irql);

		CheckCancellation();

		HostTrap(PANIC_EXECUTIVE_UNEXPECTED);
		}
	else {
		m_uSleep  = 0;
		
		m_fReady  = false;

		m_pWaker  = NULL;

		m_pWait   = pList;

		m_uWait   = uCount;

		for( UINT n = 0; n < uCount; n++ ) {

			CWaitable *pWait = (CWaitable *) pList[n]->GetWaitable();

			if( !pWait->WaitInit(this, n, true) ) {

				for( UINT m = 0; m < n; m++ ) {

					CWaitable *pDone = (CWaitable *) pList[m]->GetWaitable();

					pDone->WaitDone(this, m);
					}

				m_fReady = true;

				m_uWait  = 0;

				Hal_LowerIrql(irql);

				return 1 + n;
				}
			}

		if( !m_fReady ) {

			LoadSleep(uTime);

			m_pExec->Schedule();

			Hal_LowerIrql(irql);

			CheckCancellation();
			}
		else {
			WaitDone();

			Hal_LowerIrql(irql);
			}

		if( m_pWaker ) {
		
			UINT n;

			for( n = 0; n < uCount - 1; n++ ) {

				if( m_pWaker == pList[n] ) {

					break;
					}
				}

			return 1 + n;
			}
		}

	return 0;
	}

bool CExecThread::WakeTask(IWaitable *pWait)
{
	UINT ipr;

	HostRaiseIpr(ipr);

	if( !m_pWaker ) {

		m_pWaker = pWait;

		m_fReady = true;

		HostLowerIpr(ipr);

		m_pExec->Schedule(this);

		return true;
		}

	HostLowerIpr(ipr);

	return false;
	}

void CExecThread::WaitDone(void)
{
	if( m_uWait ) {

		for( UINT n = 0; n < m_uWait; n++ ) {

			if( m_pWait[n] ) {

				CWaitable *pWait = (CWaitable *) m_pWait[n]->GetWaitable();

				pWait->WaitDone(this, n);
				}
			}

		m_uWait = 0;
		}
	}

void CExecThread::ReapThread(void)
{
	// This is called by the Reaper thread when the
	// thread has exited. We can't perform all the
	// shutdown in the thread's own context, so the
	// reaper does all the dirty work for us.

//	Info(0, "being reaped\n");

	m_state = 5;

	Suspend();

	if( m_pid == 3 ) {

		// The third thread is the primary one after
		// the base level and the reaper, so on RLOS
		// we should at this point reset the system.

		AfxTrace("exec: primary thread exited\n");

		phal->RestartSystem();
		}

	DeleteImpure();

	phal->DeleteContext(m_pCtx);

	free(m_pStack);

	m_pExit->Set();

	Release();
	}

void CExecThread::ResetRunTime(void)
{
	m_RunTime = 0;
}

// Overridables

BOOL CExecThread::OnCreate(void)
{
	m_pid     = m_uIndex;

	m_state   = 1;

	m_impure  = NULL;

	memset(m_eh, 0, sizeof(m_eh));

	if( CExecutive::m_pThread ) {

		m_parent = CExecutive::m_pThread->GetIdent();

		m_pExit  = Create_ManualEvent();
		}

	AllocateStack();

	DWORD Stack = DWORD(m_pStack + m_uStack);

	m_pCtx      = phal->CreateContext(Stack, DWORD(&ThreadProc), DWORD(this));

	return TRUE;
	}

// Implementation

void CExecThread::AllocateStack(void)
{
	m_uStack = 32768;

	m_pStack = PDWORD(memalign(16, sizeof(DWORD) * m_uStack));
	
	memset(m_pStack, 0x55, sizeof(DWORD) * m_uStack);
	}

int CExecThread::ExecuteThread(void)
{
	CreateImpure();

	CommonExecute();

	CMutex::FreeMutexList(this);

	m_pExec->ReapThread(this);

	return 0;
	}

// Entry Point

void CExecThread::ThreadProc(CExecThread *pThread)
{
	pThread->ExecuteThread();
	}
		
// End of File
