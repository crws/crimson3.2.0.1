
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Qutex_HPP

#define INCLUDE_Qutex_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Mutex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Quick Mutex Object
//

class CQutex : public CMutex
{
	public:
		// Constructor
		CQutex(CExecutive *pExec);

		// IWaitable
		BOOL METHOD Wait(UINT uTime);

		// IMutex
		void METHOD Free(void);
	};

// End of File

#endif
