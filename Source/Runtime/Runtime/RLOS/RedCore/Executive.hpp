
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Executive_HPP

#define INCLUDE_Executive_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/BaseExecutive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CExecThread;

class CTimer;

//////////////////////////////////////////////////////////////////////////
//
// Executive Thread Link
//

struct CExecThreadLink
{
	CExecThreadLink(void)
	{
		m_p = NULL;
		m_s = 0;
		}

	CExecThreadLink(CExecThread *p, UINT s)
	{
		m_p = p;
		m_s = s;
		}

	operator bool (void) const
	{
		return m_p ? true : false;
		}

	bool operator ! (void) const
	{
		return m_p ? false : true;
		}

	bool empty(void) const
	{
		return !m_p && !m_s;
		}

	void clear(void)
	{
		m_p = NULL;
		m_s = 0;
		}

	CExecThread * volatile m_p;
	UINT          volatile m_s;
	};

//////////////////////////////////////////////////////////////////////////
//
// Executive Link Pair
//

struct CExecLinkPair
{
	CExecThreadLink m_Next;
	CExecThreadLink m_Prev;
	};

//////////////////////////////////////////////////////////////////////////
//
// Executive Group
//

struct CExecGroup
{
	UINT           m_uLevel;
	CExecThread  * m_pActive;
	CExecThread  * m_pHeadThread;
	CExecThread  * m_pTailThread;
	CExecGroup   * m_pNextInExec;
	CExecGroup   * m_pPrevInExec;
	};

//////////////////////////////////////////////////////////////////////////
//
// Executive Object
//

class CExecutive : public CBaseExecutive,
		   public IExecHook,
		   public IWaitMultiple,
		   public IDiagProvider
{
	public:
		// Constructor
		CExecutive(void);

		// Destructor
		~CExecutive(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IExecutive
		void      METHOD Open(void);
		void      METHOD Close(void);
		IThread * METHOD CreateThread(PENTRY pfnProc, UINT uLevel, void *pParam, UINT uParam);
		IThread * METHOD CreateThread(IClientProcess *pProc, UINT uIndex, UINT uLevel);
		IThread * METHOD CreateThread(IClientProcess *pProc, UINT uIndex, UINT uLevel, ISemaphore *pTms);
		BOOL      METHOD DestroyThread(IThread *pThread);
		IThread * METHOD GetCurrentThread(void);
		UINT      METHOD GetCurrentIndex(void);
		UINT      METHOD ToTicks(UINT uTime);
		UINT      METHOD ToTime(UINT uTicks);
		void      METHOD Sleep(UINT uTime);
		BOOL	  METHOD ForceSleep(UINT uTime);
		UINT      METHOD GetTickCount(void);
		UINT      METHOD AddNotify(IThreadNotify *pNotify);
		BOOL	  METHOD RemoveNotify(UINT uNotify);

		// IWaitMultiple
		UINT METHOD WaitMultiple(IWaitable **pList, UINT uCount, UINT uTime);
		UINT METHOD WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, IWaitable *pWait3, UINT uTime);
		UINT METHOD WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, UINT uTime);

		// IExecHook
		void ExecTick(void);
		bool HasPendingDispatch(void);
		void DispatchThread(DWORD Frame);
		void AccumThreadTime(bool fTimer);
		void AccumInterruptTime(void);
		bool SuspendThread(void);

		// Impersonation
		CExecThread * Impersonate(CExecThread *pThread);

		// Operations
		void RemoveThread(CExecThread *pThread);
		bool Schedule(void);
		bool Schedule(CExecThread *pThread);
		void CheckGroup(CExecThread *pThread);
		bool InitWait(IWaitable *pWait, UINT uTime);
		void ReapThread(CExecThread *pThread);

		// Timer Support
		void CreateTimer(CExecTimer *pTimer);
		void DeleteTimer(CExecTimer *pTimer);

		// Current Thread
		static CExecThread * volatile m_pThread;
		static CExecThread * volatile m_pAlias;

	protected:
		// Instance Pointer
		static CExecutive * m_pThis;

		// Data Members
		UINT		       m_uProv;
		CExecThread	     * m_pHeadThread;
		CExecThread          * m_pTailThread;
		CExecGroup	     * m_pHeadGroup;
		CExecGroup	     * m_pTailGroup;
		CExecThread * volatile m_pPend;
		bool          volatile m_fPend;
		UINT		       m_uTick;
		INT		       m_nWhole;
		INT		       m_nFract;
		UINT64		       m_IntTime;
		UINT64		       m_RefTime;
		IEvent      *	       m_pReapFlag;
		IEvent      *	       m_pReapBusy;
		CExecThread * volatile m_pReapThread;

		// Creation Herlper
		IThread * CreateThread(PENTRY pfnProc, IClientProcess *pProc, UINT uLevel, void *pParam, UINT uParam, ISemaphore *pTms);

		// Implementation
		void ScheduleNew(CExecThread *pThread);
		void Dispatch(DWORD Frame);
		void SwitchTask(CExecThread *pThread, DWORD Frame);
		UINT GetTimeDelta(void);

		// IDiagProvider
		UINT RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Diagnostics
		BOOL    DiagRegister(void);
		BOOL    DiagRevoke(void);
		UINT    DiagTasks(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT    DiagClear(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT    DiagGroups(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT    DiagTrace(IDiagOutput *pOut, IDiagCommand *pCmd);
		BOOL    FindThread(IDiagOutput *pOut, CExecThread **ppThread, PCTXT pName);
		CString Format(UINT64 t, int w);

		// Entry Points
		static int BaseLevel (IThread *pTask, void *pParam, UINT uParam);
		static int ReaperProc(IThread *pTask, void *pParam, UINT uParam);
		static int ClientProc(IThread *pTask, void *pParam, UINT uParam);

		// Instantiators
		static IUnknown * Create_Mutex(PCTXT pName);
		static IUnknown * Create_Qutex(PCTXT pName);
		static IUnknown * Create_ManualEvent(PCTXT pName);
		static IUnknown * Create_AutoEvent(PCTXT pName);
		static IUnknown * Create_Semaphore(PCTXT pName);
	};

// End of File

#endif
