
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_BaseHal_HPP

#define INCLUDE_BaseHal_HPP

//////////////////////////////////////////////////////////////////////////
//
// Base Hardware Abstraction Layer
//

class CBaseHal : public IHal, public IEventSink
{
	public:
		// Constructor
		CBaseHal(void);

		// Destructor
		~CBaseHal(void);

		// Initialization
		void Open(void);

		// Executive Hook
		void SetExecHook(IExecHook *pExec);
		void SetExecRunning(void);

		// Power Management
		void EnterIdleState(void);
		void LeaveIdleState(void);
		void RestartSystem(void);
		void ShutdownSystem(void);

		// Timer Support
		UINT GetTimerResolution(void);
		UINT TimeToTicks(UINT uTime);
		UINT TicksToTime(UINT uTicks);
		UINT GetTickCount(void);
		void SpinDelay(UINT uTime);

		// Debug Support
		void DebugRegister(void);
		void DebugRevoke(void);

		// Memory Management
		bool  IsVirtualValid(DWORD dwAddr, bool fWrite);
		DWORD PhysicalToVirtual(DWORD dwAddr, bool fCached);
		DWORD VirtualToPhysical(DWORD dwAddr);
		DWORD GetNonCachedAlias(DWORD dwAddr);
		DWORD GetCachedAlias(DWORD dwAddr);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Data Members
		static IExecHook   * m_pExec;
		static IMmu        * m_pMmu;
		static bool          m_fExec;
		static bool	     m_fIdle;
		static UINT volatile m_uIdle;
		static UINT          m_IRQL;
		static UINT          m_uTicks;
		static UINT          m_uDelay;
		static INT           m_nCrit;
		static UINT          m_uSave;

		// Power Management Hooks
		virtual void EnterLowPower(void);
		virtual void LeaveLowPower(void);

		// Implementation
		static void  PrintWelcome(void);
		static bool  IsExecValid(void);
		static PCTXT GetThreadName(void);
		static PCTXT GetContext(void);
		static bool  ShowPanicCode(UINT Code);

		// Friends
		friend UINT Hal_GetIrql(void);
		friend bool Hal_Critical(bool fEnter);
		friend void Hal_CheckMinIrql(UINT uIrql);
		friend void Hal_CheckMaxIrql(UINT uIrql);
		friend void Hal_CheckIrql(UINT uIrql);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

STRONG_INLINE void Hal_CheckMinIrql(UINT uIrql)
{
	if( unlikely(CBaseHal::m_IRQL < uIrql) ) {

		AfxTrace("*** IRQL TOO LOW at 0x%2.2X versus 0x%2.2X\n", CBaseHal::m_IRQL, uIrql);

		HostTrap(PANIC_HAL_BAD_IRQL);
		}
	}

STRONG_INLINE void Hal_CheckMaxIrql(UINT uIrql)
{
	if( unlikely(CBaseHal::m_IRQL > uIrql) ) {

		AfxTrace("\n*** IRQL TOO HIGH at 0x%2.2X versus 0x%2.2X\n", CBaseHal::m_IRQL, uIrql);

		HostTrap(PANIC_HAL_BAD_IRQL);
		}
	}

STRONG_INLINE void Hal_CheckIrql(UINT uIrql)
{
	if( unlikely(CBaseHal::m_IRQL != uIrql) ) {

		AfxTrace("\n*** IRQL INCORRECT at 0x%2.2X versus 0x%2.2X\n", CBaseHal::m_IRQL, uIrql);

		HostTrap(PANIC_HAL_BAD_IRQL);
		}
	}

// End of File

#endif
