
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ExecThread_HPP

#define INCLUDE_ExecThread_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/BaseExecThread.hpp"

#include "Waitable.hpp"

#include "Executive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMutex;

//////////////////////////////////////////////////////////////////////////
//
// Executive Thread Object
//

class CExecThread : public CBaseExecThread
{
	public:
		// Constructor
		CExecThread(CExecutive *pExec);

		// Destructor
		~CExecThread(void);

		// IWaitable
		PVOID METHOD GetWaitable(void);
		BOOL  METHOD Wait(UINT uTime);
		BOOL  METHOD HasRequest(void);

		// IThread
		UINT  METHOD GetTimer(void);
		void  METHOD SetTimer(UINT uTime);
		void  METHOD CheckCancellation(void);
		void  METHOD Guard(BOOL fGuard);
		void  METHOD Guard(PGUARD pProc, PVOID pData);
		void  METHOD Exit(int nCode);
		BOOL  METHOD Destroy(void);

		// Impersonation
		CExecThread * GetAlias(void);

		// Link Access
		CExecLinkPair & GetPair(UINT uSlot);

		// Attributes
		PVOID  GetContext(void) const;
		PVOID  GetImpure(void) const;
		PVOID  GetEhData(void) const;
		UINT   GetPriority(void) const;
		bool   IsCancelPending(void) const;
		bool   IsReady(void) const;
		bool   IsSuspended(void) const;
		bool   IsBaseLevel(void) const;
		bool   IsSliceExpired(void) const;
		bool   IsOnlyThread(void) const;
		bool   IsWaiting(void) const;
		UINT64 GetRunTime(void) const;
		UINT64 GetSliceTime(void) const;
		bool   IsWaker(IWaitable *pWait) const;
		void   GetFlagsAsText(char *pText) const;
		void   GetFuncName(char *pText) const;
		UINT   GetStackUsage(void) const;
		UINT   GetSlept(void);

		// Operations
		void OnTick(UINT uTick);
		void Suspend(void);
		UINT WaitMultiple(IWaitable **pList, UINT uCount, UINT uTime);
		bool WakeTask(IWaitable *pWait);
		void WaitDone(void);
		void InitSleep(void);
		void InitSleep(UINT uTime);
		void SetAlias(CExecThread *pThread);
		void AddRunTime(UINT uTime);
		void ResetSlice(void);
		void ReapThread(void);
		void ResetRunTime(void);

		// Linked Lists
		CExecThread * m_pNextInExec;
		CExecThread * m_pPrevInExec;
		CExecThread * m_pNextInGroup;
		CExecThread * m_pPrevInGroup;
		CMutex      * m_pOwnedHead;
		CMutex      * m_pOwnedTail;
		CExecGroup  * m_pGroup;

	protected:
		// General Data
		CExecutive     * m_pExec;
		CExecThread    * m_pAlias;
		bool	         m_fReady;
		bool	         m_fSuspend;
		IEvent         * m_pExit;
		UINT             m_uTimer;
		UINT		 m_uSleep;
		UINT		 m_uSlept;
		DWORD		 m_eh[16];
		void	       * m_pCtx;
		PDWORD           m_pStack;
		UINT             m_uStack;
		UINT		 m_Slice;
		INT64		 m_RunTime;
		INT64		 m_AccTime;
		IWaitable      * m_pWaker;
		CExecLinkPair    m_Wait[8];
		IWaitable     ** m_pWait;
		UINT	         m_uWait;

		// Overridables
		BOOL OnCreate(void);

		// Implementation
		void AllocateStack(void);
		int  ExecuteThread(void);
		void LoadSleep(UINT uTime);

		// Entry Point
		static void ThreadProc(CExecThread *pThread);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Impersonation

STRONG_INLINE CExecThread * CExecThread::GetAlias(void)
{
	return m_pAlias ? m_pAlias : this;
	}

// Link Access

STRONG_INLINE CExecLinkPair & CExecThread::GetPair(UINT uSlot)
{
	return m_Wait[uSlot];
	}

// Attributes

STRONG_INLINE PVOID CExecThread::GetContext(void) const
{
	return m_pCtx;
	}

STRONG_INLINE PVOID CExecThread::GetImpure(void) const
{
	return m_impure;
	}

STRONG_INLINE PVOID CExecThread::GetEhData(void) const
{
	return PVOID(m_eh);
	}

STRONG_INLINE UINT CExecThread::GetPriority(void) const
{
	return m_uLevel;
	}

STRONG_INLINE bool CExecThread::IsCancelPending(void) const
{
	return m_cancel == 1;
	}

STRONG_INLINE bool CExecThread::IsReady(void) const
{
	return m_fReady;
	}

STRONG_INLINE bool CExecThread::IsSuspended(void) const
{
	return m_fSuspend;
	}

STRONG_INLINE bool CExecThread::IsBaseLevel(void) const
{
	return m_uLevel == 0;
	}

STRONG_INLINE bool CExecThread::IsSliceExpired(void) const
{
	return !IsBaseLevel() && m_AccTime >= m_Slice;
	}

STRONG_INLINE bool CExecThread::IsOnlyThread(void) const
{
	return m_pGroup->m_pHeadThread == m_pGroup->m_pTailThread;
	}

STRONG_INLINE bool CExecThread::IsWaiting(void) const
{
	return m_uWait != 0;
	}

STRONG_INLINE UINT64 CExecThread::GetRunTime(void) const
{
	return m_RunTime;
	}

STRONG_INLINE UINT64 CExecThread::GetSliceTime(void) const
{
	return m_AccTime;
	}

STRONG_INLINE bool CExecThread::IsWaker(IWaitable *pWait) const
{
	return m_pWaker == pWait;
	}

STRONG_INLINE UINT CExecThread::GetSlept(void)
{
	UINT val = m_uSlept;

	m_uSlept = 0;

	return val;
	}

// Operations

STRONG_INLINE void CExecThread::OnTick(UINT uTick)
{
	if( !m_fReady ) {

		if( m_uSleep && !--m_uSleep ) {

			m_fReady = true;

			m_pExec->Schedule(this);
			}

		m_uSlept += uTick;
		}

	if( m_uTimer ) {

		m_uTimer--;
		}
	}

STRONG_INLINE void CExecThread::InitSleep(void)
{
	m_uSleep = 0;

	m_fReady = false;
	}

STRONG_INLINE void CExecThread::InitSleep(UINT uTime)
{
	m_uSleep = 0;

	m_fReady = false;

	LoadSleep(uTime);
	}

STRONG_INLINE void CExecThread::SetAlias(CExecThread *pThread)
{
	m_pAlias = pThread;
	}

STRONG_INLINE void CExecThread::AddRunTime(UINT uTime)
{
	m_RunTime += uTime;

	m_AccTime += uTime;
	}

STRONG_INLINE void CExecThread::ResetSlice(void)
{
	m_AccTime = 0;
	}

// Implementation

STRONG_INLINE void CExecThread::LoadSleep(UINT uTime)
{
	if( uTime < NOTHING ) {

		m_uSleep = uTime ? phal->TimeToTicks(uTime) : 1;

		return;
		}

	m_uSleep = 0;
	}

// End of File

#endif
