
#include "Intern.hpp"

#include "Mutex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Implementation
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Executive.hpp"

#include "BaseHal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mutex Implementation
//

// Boost Flag

#define USE_BOOST 0

// Instantiators

IUnknown * CExecutive::Create_Mutex(PCTXT pName)
{
	return New CMutex(m_pThis);
	}

// Constructor

CMutex::CMutex(CExecutive *pExec) : CWaitable(pExec)
{
	StdSetRef();

	m_uCount = 0;
	
	m_pOwner = NULL;
	}

// Destructor

CMutex::~CMutex(void)
{
	if( m_pOwner ) {

		HostTrap(PANIC_EXECUTIVE_OWNED_MUTEX);
		}
	}

// IUnknown

HRESULT CMutex::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(IMutex);

	return E_NOINTERFACE;
	}

ULONG CMutex::AddRef(void)
{
	StdAddRef();
	}

ULONG CMutex::Release(void)
{
	StdRelease();
	}

// CWaitable

bool CMutex::WaitInit(CExecThread *pThread, UINT uSlot, bool fWait)
{
	if( !Claim(pThread) ) {

		if( !fWait ) {

			return true;
			}
		
		#if USE_BOOST

		if( pThread->GetCurrentPriority() > m_pOwner->GetCurrentPriority() ) {

			m_pExec->BoostTask(m_pOwner, pThread);
			}

		#endif

		AppendThread(pThread, uSlot);

		return true;
		}

	return false;
	}

void CMutex::WaitDone(CExecThread *pThread, UINT uSlot)
{
	#if USE_BOOST

	if( pThread->GetCurrentPriority() == m_pOwner->GetCurrentPriority() ) {

		RemoveThread(pThread, uSlot);

		FindBoost();

		return;
		}

	#endif

	RemoveThread(pThread, uSlot);
	}

// IWaitable

PVOID CMutex::GetWaitable(void)
{
	return (CWaitable *) this;
	}

BOOL CMutex::Wait(UINT uTime)
{
	// This provides a fast path for waits that do not need to
	// block, avoiding the need to enter the more complex wait
	// code or to raise the system IRQL.

	Hal_CheckMaxIrql(uTime ? IRQL_TASK : NOTHING);

	UINT ipr;

	HostRaiseIpr(ipr);

	if( Claim(CExecutive::m_pThread) ) {

		HostLowerIpr(ipr);

		return true;
		}
	else {
		HostLowerIpr(ipr);

		if( !uTime ) {

			return false;
			}
		}

	if( m_pExec->InitWait(this, uTime) ) {

		AfxAssert(m_pOwner == CExecutive::m_pThread);

		return TRUE;
		}

	return FALSE;
	}

BOOL CMutex::HasRequest(void)
{
	return m_Wake;
	}

// IMutex

void CMutex::Free(void)
{
	// !!! HACK !!!

	if( true || likely(m_pOwner == CExecutive::m_pThread) ) {

	// !!! HACK !!!

		if( !--m_uCount ) {

			#if USE_BOOST

			ReleaseBoost();

			#endif

			ListRemove();

			UINT ipr;

			HostRaiseIpr(ipr);

			if( likely(!m_Wake) ) {

				// This providers a fast path to signal the object when
				// no-one is waiting, avoiding the need to raise the IRQL.

				m_pOwner = NULL;

				HostLowerIpr(ipr);
				}
			else {
				HostLowerIpr(ipr);

				UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

				if( (m_pOwner = m_Wake.m_p) ) {

					m_uCount = 1;

					ListAppend();

					WakeOne(this);

					#if USE_BOOST

					FindBoost();

					#endif
					}

				Hal_LowerIrql(irql);
				}
			}
		
		return;
		}

	HostTrap(PANIC_EXECUTIVE_WRONG_THREAD);
	}

// Task Teardown

void CMutex::FreeMutexList(CExecThread *pThread)
{
	if( pThread->m_pOwnedHead ) {

		AfxTrace("exec: thread %u exiting with owned mutexes\n", pThread->GetIdent());

		while( pThread->m_pOwnedHead ) {

			pThread->m_pOwnedHead->Free();
			}
		}
	}

// Boost Support

#if USE_BOOST

void CMutex::FindBoost(void)
{
	UINT   uPriority = m_pOwner->GetOriginalPriority();

	CExecThread *pPriority = NULL;

	CExecThread *pThread   = m_pHead;

	while( pThread ) {

		if( pThread->GetCurrentPriority() > uPriority ) {

			uPriority = pThread->GetCurrentPriority();

			pPriority = pThread;
			}

		CLink *pLink = pThread->GetLink(this, false);
		
		pThread = pLink ? pLink->m_pNext : NULL;;
		}

	m_pExec->BoostTask(m_pOwner, pPriority);
	}

void CMutex::ReleaseBoost(void)
{
	m_pExec->BoostTask(m_pOwner, NULL);
	}

#endif

// End of File
