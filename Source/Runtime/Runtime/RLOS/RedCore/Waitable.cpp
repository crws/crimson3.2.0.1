
#include "Intern.hpp"

#include "Waitable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Implementation
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Waitable Object Implementation
//

// Macros

#define Link(n, m) (n).m_p->GetPair((n).m_s).m

// Constructor

CWaitable::CWaitable(CExecutive *pExec)
{
	m_pExec = pExec;
}

// Implementation

bool CWaitable::WakeOne(IWaitable *pThis)
{
	if( m_Wake ) {

		bool fWake = m_Wake.m_p->WakeTask(pThis);

		m_Wake = Link(m_Wake, m_Next);

		return fWake;
		}

	return false;
	}

void CWaitable::WakeAll(IWaitable *pThis)
{
	while( m_Wake ) {

		m_Wake.m_p->WakeTask(pThis);

		m_Wake = Link(m_Wake, m_Next);
		}
	}

bool CWaitable::AppendThread(CExecThread *pThread, UINT uSlot)
{
	CExecThreadLink This(pThread, uSlot);

	CExecThreadLink &Next = Link(This, m_Next);

	CExecThreadLink &Prev = Link(This, m_Prev);

	#if defined(_DEBUG)

	// NOTE -- These have to be conditional on _DEBUG as otherwise
	// they turn into __assume statements for conditions that won't
	// actually be true as the clear logic is also gated on _DEBUG.

	AfxAssert(Next.empty());

	AfxAssert(Prev.empty());

	#endif

	Next.clear();

	Prev = m_Tail;

	UINT ipr;

	HostRaiseIpr(ipr);

	(m_Tail ? Link(m_Tail, m_Next) : m_Head) = This;

	m_Tail = This;

	if( m_Wake.m_p == NULL ) {

		m_Wake = This;

		HostLowerIpr(ipr);

		return true;
		}

	HostLowerIpr(ipr);

	return false;
	}

bool CWaitable::RemoveThread(CExecThread *pThread, UINT uSlot)
{
	CExecThreadLink This(pThread, uSlot);

	CExecThreadLink &Next = Link(This, m_Next);

	CExecThreadLink &Prev = Link(This, m_Prev);

	UINT ipr;

	HostRaiseIpr(ipr);

	(Next ? Link(Next, m_Prev) : m_Tail) = Link(This, m_Prev);

	(Prev ? Link(Prev, m_Next) : m_Head) = Link(This, m_Next);

	bool fResult = false;
	
	if( m_Wake.m_p == pThread ) {

		m_Wake = Link(This, m_Next);

		fResult = true;
		}

	HostLowerIpr(ipr);

	#if defined(_DEBUG)

	Next.clear();

	Prev.clear();

	#endif

	return fResult;
	}

// End of File
