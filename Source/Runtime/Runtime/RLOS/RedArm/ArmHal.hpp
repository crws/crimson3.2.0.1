
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ArmHal_HPP

#define INCLUDE_ArmHal_HPP

//////////////////////////////////////////////////////////////////////////
//
// Debugging Flags
//

// NOTE -- Setting this will disable deferred floating point context
// switching and will allow the use of FP registers from ISPs. This is
// not a good idea, but it allows richer use of AfxTrace from ISPs. Do
// not set this value in a release build!

#define ALLOW_ISP_FP 0

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../RedCore/BaseHal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Processor Hooks
//

extern void ArmStackTrace(IDiagOutput *pOut);
extern void ArmStackTrace(IDiagOutput *pOut, DWORD pc, DWORD fp);
extern void ArmStackTrace(PBYTE &pData, DWORD pc, DWORD fp);
extern bool ArmGetFuncName(char *pName, UINT uName, DWORD pc, DWORD fp, UINT uFlags);
extern bool ArmFixAlign(DWORD Frame, DWORD Opcode, DWORD Address, UINT &uSize);

//////////////////////////////////////////////////////////////////////////
//
// Processor Modes
//

#define	ARM_MODE_MASK	0x1F
#define	ARM_MODE_USER	0x10
#define	ARM_MODE_FIQ	0x11
#define	ARM_MODE_IRQ	0x12
#define	ARM_MODE_SWI	0x13
#define	ARM_MODE_ABORT	0x17
#define	ARM_MODE_UNDEF	0x1B
#define ARM_MODE_SYSTEM	0x1F
#define	ARM_MODE_INIT	0x1F

//////////////////////////////////////////////////////////////////////////
//
// Processor Nop and Idle
//

#if defined(AEON_PROC_Cortex)

#define HostIdle()		ASM(	"wfi\n\t"			\
					)				\

#else

#define HostIdle()		ASM(	"mov r0, #0\n\t"		\
					"mcr p15, 0, r0, c7, c0, 4\n\t"	\
					: : : "r0"			\
					)				\

#endif

//////////////////////////////////////////////////////////////////////////
//
// Function Prologs
//

#define	FuncProlog()		ASM(	"mov	ip, sp		\n\t"	\
					"push	{fp, ip, lr, pc}\n\t"	\
					"sub	fp, ip, #4	\n\t"	\
					)				\

#define	FuncEpilog()		ASM(	"ldmfd sp, {fp, sp, pc}"	\
					)				\

//////////////////////////////////////////////////////////////////////////
//
// Processor Status
//

#define	HostGetThisSR(v)	ASM(	"mrs %0, cpsr"			\
					: "=r"(v)			\
					)				\

#define	HostGetPrevSR(v)	ASM(	"mrs %0, spsr"			\
					: "=r"(v)			\
					)				\

#define	HostSetThisSR(v)	ASM(	"msr cpsr, %0"			\
					:				\
					: "r"(v)			\
					)				\

#define	HostSetPrevSR(v)	ASM(	"msr spsr, %0"			\
					:				\
					: "r"(v)			\
					)				\

//////////////////////////////////////////////////////////////////////////
//
// Stack Pointer Access
//

#define	HostGetThisSP(v)	ASM(	"mov %0, sp"			\
					: "=r"(v)			\
					)				\

#define	HostGetUserSP(v)	ASM(	"stm %0, {r13}^"		\
					:				\
					: "r"(&v)			\
					)				\

#define	HostSetThisSP(v)	ASM(	"mov sp, %0"			\
					:				\
					: "r"(v)			\
					)				\

#define	HostSetUserSP(v)	ASM(	"ldm %0, {r13}^"		\
					:				\
					: "r"(&v)			\
					)				\

//////////////////////////////////////////////////////////////////////////
//
// FIQ Frame Pointer
//

#define	HostGetFiqFP(v)		ASM(	"mrs r0, cpsr		\t\n"	\
					"msr cpsr_c, #0xD1	\t\n"	\
					"mov %0, fp		\t\n"	\
					"msr cpsr, r0		\t\n"	\
					: "=r"(v)			\
					:				\
					: "r0","r8","r9","r10","r12"	\
					)				\

//////////////////////////////////////////////////////////////////////////
//
// Link Register Access
//

#define	HostGetUserLR(v)	ASM(	"stm %0, {r14}^"		\
					:				\
					: "r"(&v)			\
					)				\

#define	HostSetUserLR(v)	ASM(	"ldm %0, {r14}^"		\
					:				\
					: "r"(&v)			\
					)				\

//////////////////////////////////////////////////////////////////////////
//
// System Control Access
//

#define	HostGetSysCon(v)	ASM(	"mrc p15, 0, %0, c1, c0, 0"	\
					: "=r"(v)			\
					)				\

#define	HostGetSecCon(v)	ASM(	"mrc p15, 0, %0, c1, c1, 0"	\
					: "=r"(v)			\
					)				\

//////////////////////////////////////////////////////////////////////////
//
// Fault Information
//

#define	HostGetFsr(v)		ASM(	"mrc p15, 0, %0, c5, c0, 0"	\
					: "=r"(v)			\
					)				\

#define	HostGetFar(v)		ASM(	"mrc p15, 0, %0, c6, c0, 0"	\
					: "=r"(v)			\
					)				\

//////////////////////////////////////////////////////////////////////////
//
// VFP Information
//

#define	HostGetVfpScr(v)	ASM(	"fmrx %0, fpscr"		\
					: "=r"(v)			\
					)				\

#define HostSetVfpScr(v)	ASM(	"fmxr fpscr, %0"		\
					:				\
					: "r"(v)			\
					)				\

#define	HostGetVfpExc(v)	ASM(	"fmrx %0, fpexc"		\
					: "=r"(v)			\
					)				\

#define HostSetVfpExc(v)	ASM(	"fmxr fpexc, %0"		\
					:				\
					: "r"(v)			\
					)				\

//////////////////////////////////////////////////////////////////////////
//
// VFP Exception Flags
//

#define VFP_DISABLE		0

#define VFP_ENABLE		(1<<30)

//////////////////////////////////////////////////////////////////////////
//
// ARM Hardware Abstraction Layer
//

class CArmHal : public CBaseHal, public IDiagProvider
{
	public:
		// Constructor
		CArmHal(void);

		// Destructor
		~CArmHal(void);

		// Initialization
		void Open(void);

		// Debug Support
		void DebugRegister(void);
		void DebugRevoke(void);

		// Idle Mode
		void WaitForInterrupt(void);

		// Interrupt Controller
		bool SetLinePriority(UINT uLine, UINT uPriority);
		bool SetLineHandler(UINT uLine, IEventSink *pSink, UINT uParam);
		bool EnableLine(UINT uLine, bool fEnable);
		bool EnableLineViaIrql(UINT uLine, UINT &uSave, bool fEnable);

		// Task Context
		void * CreateContext(DWORD Stack, DWORD Entry, DWORD Param);
		void   DeleteContext(void *pCtx);
		void   SwitchContext(void *pSave, void *pLoad, DWORD Frame);
		bool   ContextUsesFp(void *pCtx);

		// Task Diagnostics
		DWORD GetActiveSp(void);
		DWORD GetSavedSp (void *pCtx);
		DWORD GetSavedPc (void *pCtx);
		void  GetFuncName(char *pName, UINT uName, void *pCtx);
		void  StackTrace (IDiagOutput *pOut, void *pCtx);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Context Structure
		struct CCtx
		{
			DWORD	m_sr;
			DWORD	m_r0;
			DWORD	m_r1;
			DWORD	m_r2;
			DWORD	m_r3;
			DWORD	m_r4;
			DWORD	m_r5;
			DWORD	m_r6;
			DWORD	m_r7;
			DWORD	m_r8;
			DWORD	m_r9;
			DWORD	m_r10;
			DWORD	m_fp;	// r11
			DWORD	m_ip;	// r12
			DWORD	m_pc;	// r15
			DWORD	m_sp;	// r13
			DWORD	m_lr;	// r14
			DWORD	m_ep;

			INT64	m_d0;
			INT64	m_d1;
			INT64	m_d2;
			INT64	m_d3;
			INT64	m_d4;
			INT64	m_d5;
			INT64	m_d6;
			INT64	m_d7;
			INT64	m_d8;
			INT64	m_d9;
			INT64	m_d10;
			INT64	m_d11;
			INT64	m_d12;
			INT64	m_d13;
			INT64	m_d14;
			INT64	m_d15;
			INT64	m_d16;
			INT64	m_d17;
			INT64	m_d18;
			INT64	m_d19;
			INT64	m_d20;
			INT64	m_d21;
			INT64	m_d22;
			INT64	m_d23;
			INT64	m_d24;
			INT64	m_d25;
			INT64	m_d26;
			INT64	m_d27;
			INT64	m_d28;
			INT64	m_d29;
			INT64	m_d30;
			INT64	m_d31;

			DWORD	m_fpscr;
			
			int	m_index;
			int	m_fpforce;
			int	m_fpused;
			};

		// Diagnostic Data
		ULONG	       m_uRefs;
		IDiagManager * m_pDiag;
		UINT           m_uProv;

		// Data Members
		static IPic  *		m_pPic;
		static CCtx  * volatile m_pActiveCtx;
		static CCtx  * volatile m_pFloatCtx;
		static UINT		m_SaveFpExc;
		static UINT		m_uIndex;
		static PCSTR   volatile m_pFault;
		static PVDWORD          m_pEntropy;

		// Dagnostic Counters
		static UINT m_uTickTotal;
		static UINT m_uTickIdle;
		static UINT m_uTickRun;
		static UINT m_uCtxSwitch;
		static UINT m_uFltSwitch;
		static UINT m_uIntSwi;
		static UINT m_uIntIrq;
		static UINT m_uIntFiq;

		// Interrupt Handlers
		static void OnService(DWORD Frame, DWORD Opcode);
		static void OnUndef(DWORD Frame);
		static void OnAbort(DWORD Frame, BOOL fCode);
		static void OnIrq(DWORD Frame);
		static void OnFiq(DWORD Frame);

		// Implementation
		static void InstallInterrupts(void);
		static bool SwitchFloat(void);
		static bool LeaveIsp(UINT Prev, DWORD Frame);
		static void Panic(UINT Code);
		static void Panic(UINT Code, DWORD Frame, DWORD fp, DWORD pc, PCTXT pInfo, ...);
		static void BuildCrashRecord(UINT Code, DWORD fp, DWORD pc);
		static void StartFault(PCTXT pName);
		static void EndFault(void);
		static void DoubleFault(PCTXT pName);

		// IPL Helper
		STRONG_INLINE static void SyncIpl(void);

		// Friends
		friend UINT Hal_RaiseIrql(UINT IRQL);
		friend bool Hal_LowerIrql(UINT IRQL);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

STRONG_INLINE bool CArmHal::LeaveIsp(UINT Prev, DWORD Frame)
{
	if( likely(Prev < IRQL_HARDWARE) ) {

		// Restore the floating point status.

		if( !ALLOW_ISP_FP ) {

			extern int _fp_printf;

			_fp_printf = m_pActiveCtx->m_fpused;

			HostSetVfpExc(m_SaveFpExc);
			}

		// Accumulate interrupt time.

		if( likely(m_fExec) ) {

			m_pExec->AccumInterruptTime();
			}

		// Implement the exit logic.

		if( likely(Prev == IRQL_TASK) ) {

			UINT mode;

			HostGetPrevSR(mode);

			if( likely((mode & ARM_MODE_MASK) == ARM_MODE_INIT) ) {

				if( likely(m_pExec) ) {

					m_pExec->DispatchThread(Frame);
					}

				m_IRQL = IRQL_TASK;

				return true;
				}
			}
		}
  
	m_IRQL = Prev;

	return false;
	}

STRONG_INLINE void CArmHal::StartFault(PCTXT pName)
{
	if( unlikely(m_pFault) ) {

		DoubleFault(pName);
		}

	m_pFault = pName;

	phal->DebugKick();
	}

STRONG_INLINE void CArmHal::EndFault(void)
{
	m_pFault = NULL;
	}

// End of File

#endif
