
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ARM Memory Copy
//

global void ArmMemCpy(PVOID d, PCVOID s, UINT n)
{
	if( likely(n) ) {

		// Check if the pointers are mutually aligned.

		if( likely(!((DWORD(d) ^ DWORD(s)) & 3)) ) {

			#if !defined(__INTELLISENSE__)

			ASM(	// Copy until we're 4-byte aligned.
				
				"	and	r8, %0, #3	\n"
				"	cmp	r8, #0		\n"
				"	beq	0f		\n"
				"	cmp	r8, #3		\n"
				"	beq	1f		\n"
				"	cmp	r8, #2		\n"
				"	beq	2f		\n"
				"	ldrb	r0, [%0,#2]	\n"
				"	strb	r0, [%1,#2]	\n"
				"2:	ldrb	r0, [%0,#1]	\n"
				"	strb	r0, [%1,#1]	\n"
				"1:	ldrb	r0, [%0,#0]	\n"
				"	strb	r0, [%1,#0]	\n"
				"	eor	r8, #3		\n"
				"	add	r8, #1		\n"
				"	add	%0, r8		\n"
				"	add	%1, r8		\n"
				"	sub	%2, r8		\n"

				// Copy blocks of 32 bytes at a time.

				"0:	cmp	%2, #32		\n"
				"	blo	2f		\n"
				"	mov	r8, %2, LSR #5	\n"
				"1:	ldmia	%0!, {r0-r7}	\n"
				"	stmia	%1!, {r0-r7}	\n"
				"	subs	r8,  #1		\n"
				"	bne	1b		\n"
				"	and	%2, #31		\n"

				// Copy blocks of 4 bytes at a time.

				"2:	cmp	%2, #4		\n"
				"	blo	2f		\n"
				"	mov	r8, %2, LSR #2	\n"
				"1:	ldmia	%0!, {r0}	\n"
				"	stmia	%1!, {r0}	\n"
				"	subs	r8,  #1		\n"
				"	bne	1b		\n"
				"	and	%2, #3		\n"

				// Copy any residual bytes

				"2:	cmp	%2, #0		\n"
				"	beq	0f		\n"
				"	cmp	%2, #1		\n"
				"	beq	1f		\n"
				"	cmp	%2, #2		\n"
				"	beq	2f		\n"
				"	ldrb	r1, [%0,#2]	\n"
				"	strb	r1, [%1,#2]	\n"
				"2:	ldrb	r1, [%0,#1]	\n"
				"	strb	r1, [%1,#1]	\n"
				"1:	ldrb	r1, [%0,#0]	\n"
				"	strb	r1, [%1,#0]	\n"
				"0:				\n"

				: "+r"(s), "+r"(d)
				: "r"(n)
				: "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8"
				);

			#endif
			}
		else {
			// Byte copy is the best we can do, although
			// the compiler will do its best to help...

			PBYTE  d1 = PBYTE(d);
			
			PCBYTE s1 = PCBYTE(s);

			while( n-- ) {
			
				*d1++ = *s1++;
				}
			}
		}
	}

// End of File
