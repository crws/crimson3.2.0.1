
/****************************************************************
/*								
/* AEON Agnostic Execution Model
/*
/* Copyright (c) 1993-2018 Red Lion Controls
/*
/* All Rights Reserved
*/

/****************************************************************
/*
/* Exception Macros
*/

.macro	NullTrap

	#if defined(_M_IMX51) || defined(_M_AM437)

	svc	#0x0101

	#else

	mov	r0, #2
	swi

	#endif

.endm

/****************************************************************
/*
/* Start Marker
*/

.section .text

.global	_vect_start

_vect_start:

/****************************************************************
/*
/* Hard Interrupt Vectors
*/

	b	IspInit
	b	IspUndef
	b	IspSvc
	b	IspAbortI
	b	IspAbortD
	b	IspAbortD
	b	IspIrq
	b	IspFiq

/****************************************************************
/*
/* Soft Interupt Vectors
*/

	.long	0	@ Undef	0x20
	.long	0	@ SVC	0x24
	.long	0	@ Abort	0x28
	.long	0	@ IRQ	0x2C
	.long	0	@ FIQ	0x30
	.long	0
	.long	0
	.long	0

/****************************************************************
/*
/* Initialization Handler
*/

IspInit:

@ Check SVC vector to see if we are running

	mov	r2, #0x24
	ldr	r2, [r2]
	cmp	r2, #0
	bne	1f

@ If no, jump to the initalization code

//	b	_init1 !!!

@ If so, trap what must be a NULL pointer call

1:	NullTrap

@ Return to caller

	mov	pc, lr

/****************************************************************
/*
/* Undefined Interrupt Handler
*/

IspUndef:

@ Adjustment

	sub	r14, r14, #4

@ Prolog

	stmfd	sp!, {r0-r12,r14}

@ Pass to soft vector if defined

	mov	r2, #0x20
	ldr	r2, [r2]
	cmp	r2, #0
	beq	1f
	mov	r0, sp
	blx	r2

@ Epilog

1:	ldmfd	sp!, {r0-r12,pc}^

/****************************************************************
/*
/* Software Interrupt Handler
*/

IspSvc:

	.global	IspSvc

@ Prolog

	stmfd	sp!, {r0-r12,r14}

@ Decode SVC call number from opcode

	ldr	r1, [lr, #-4]
	bic	r1, #0xFF000000

@ No re-enable except for service #0

	cmp	r1, #0
	bne	1f

@ Enable IRQs if previously allowed

	mrs	r2, spsr
	and	r2, r2, #0x80
	mrs	r3, cpsr
	bic	r3, r3, #0x80
	orr	r3, r3, r2
	msr	cpsr, r3

@ Pass to soft vector if defined

1:	mov	r2, #0x24
	ldr	r2, [r2]
	cmp	r2, #0
	beq	1f

	mov	r0, sp
	blx	r2

@ Epilog

1:	ldmfd	sp!, {r0-r12,pc}^

/****************************************************************
/*
/* Abort Interrupt Handler (Data)
*/

IspAbortD:

@ Adjustment

	sub	r14, r14, #4

@ Prolog

	stmfd	sp!, {r0-r12,r14}

@ Pass to soft vector if defined

	mov	r2, #0x28
	ldr	r2, [r2]
	cmp	r2, #0
	beq	1f
	mov	r0, sp
	mov	r1, #0
	blx	r2

@ Epilog

1:	ldmfd	sp!, {r0-r12,pc}^

/****************************************************************
/*
/* Abort Interrupt Handler (Instruction)
*/

IspAbortI:

@ Adjustment

	sub	r14, r14, #4

@ Prolog

	stmfd	sp!, {r0-r12,r14}

@ Pass to soft vector if defined

	mov	r2, #0x28
	ldr	r2, [r2]
	cmp	r2, #0
	beq	1f
	mov	r0, sp
	mov	r1, #1
	blx	r2

@ Epilog

1:	ldmfd	sp!, {r0-r12,pc}^

/****************************************************************
/*
/* IRQ Interrupt Handler
*/

IspIrq:

@ Adjustment

	sub	r14, r14, #4

@ Prolog

	stmfd	sp!, {r0-r12,r14}

@ Pass to soft vector if defined

	mov	r2, #0x2C
	ldr	r2, [r2]
	cmp	r2, #0
	beq	1f
	mov	r0, sp
	blx	r2

@ Epilog

1:	ldmfd	sp!, {r0-r12,pc}^

/****************************************************************
/*
/* FIQ and Monitor Interrupt Handler
*/

IspFiq:

@ Adjustment

	sub	r14, r14, #4

@ Check mode

	stmfd	sp!, {r0-r1}
	mrs	r0, cpsr
	and	r0, #0x1F
	cmp	r0, #0x11
	beq	1f
	
@ Monitor mode

	mrs	r0, spsr
	mov	r1, r14
	msr	cpsr_c, #0x00D1
	msr	spsr, r0
	stmfd	sp!, {r1}
	msr	cpsr_c, #0x00D6
	ldmfd	sp!, {r0-r1}
	msr	cpsr_c, #0x00D1
	b	2f

@ FIQ mode

1:	ldmfd	sp!, {r0-r1}
	stmfd	sp!, {r14}

@ Prolog

2: 	stmfd	sp!, {r0-r12,r14}

	mrs	r0, spsr
	stmfd	sp!, {r0}	
	
@ Pass to soft vector if defined

	mov	r2, #0x30
	ldr	r2, [r2]
	cmp	r2, #0
	beq	1f
	mov	r0, sp
	blx	r2

@ Epilog

1:	mrs	r0, cpsr
	orr	r0, #192
	msr	cpsr, r0

	ldmfd	sp!, {r0}
	msr	spsr, r0
	
	ldmfd	sp!, {r0-r12,r14,pc}^

/* End of File */
