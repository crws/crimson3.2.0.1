
#include "Intern.hpp"

#include "ArmFixAlign.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Arm Hardware Drivers
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Alignment Handler
//

// Prototypes

global	bool	ArmFixAlign(DWORD Frame, DWORD Opcode, DWORD Address, UINT &uSize);
static	bool	Fix32(PDWORD pFrame, OP32 const &op, PDWORD pData, UINT &uSize);
static	bool	Fix16(PDWORD pFrame, OP16 const &op, PDWORD pData, UINT &uSize);
static	DWORD	Get32(PDWORD pData);
static	void	Put32(PDWORD pData, DWORD dwData);
static	WORD	Get16(PDWORD pData);
static	void	Put16(PDWORD pData, WORD wData);

// Code

global	bool	ArmFixAlign(DWORD Frame, DWORD Opcode, DWORD Address, UINT &uSize)
{
	// This function attempts to emulate misaligned memory accesses. We
	// do not support all instructions that could cause such a situation,
	// but we will get most of them. Unsupported options include mode
	// translation (which isn't used on our platform) and unaligned use
	// of the ldm/stm instructions (which should not occur). We support
	// all 16-bit and 32-bit versions of ldr and str. The 8-bit versions
	// can never be misaligned and so do not need to be handled. We do
	// not support use of r13-r15, but since these are the sp, lr and pc
	// it would be very unusual for them to be accessed in this way. We
	// have also added support for the 64-bit ldrd/strd instructions.

	OP32 op32;
	
	OP16 op16;

	op32.d = Opcode;
	
	op16.d = Opcode;

	if( Fix32(PDWORD(Frame), op32, PDWORD(Address), uSize) ) {

		return true;
		}

	if( Fix16(PDWORD(Frame), op16, PDWORD(Address), uSize) ) {

		return true;
		}

	return false;
	}

static	bool	Fix32(PDWORD pFrame, OP32 const &op, PDWORD pData, UINT &uSize)
{
	if( op.x.Type == 1 ) {

		// This encoding could be 32-bit or 8-bit unsigned load/store but
		// the 8-bit version cannot occur as it can never be misaligned.

		if( op.x.Rd > 12 ) {

			// Can't access anything except r0-r12.

			return false;
			}

		if( !op.x.Mode ) {

			// Only immediate offset mode supports write back,
			// so we do not have to worry about other ones.

			if( op.x.Offset ) {

				// Only bother if the offset is non-zero.

				if( op.x.Rn > 12 ) {

					// Can't access anything except r0-r12.

					return false;
					}

				if( op.x.P ) {

					if( op.x.W ) {

						// Preindexed, so the location being accessed
						// already represents all the EA calculations.

						pFrame[op.x.Rn] = DWORD(pData);
						}
					}
				else {
					// Postindexed, so we must manually apply
					// the adjustment to the indexing register.

					if( op.x.U ) {

						pFrame[op.x.Rn] += op.x.Offset;
						}
					else
						pFrame[op.x.Rn] -= op.x.Offset;
					}
				}
			}

		if( op.x.L ) {

			// 32-bit Load

			pFrame[op.x.Rd] = Get32(pData);

			uSize = 32;

			return true;
			}
		else {
			// 32-bit Store

			Put32(pData, pFrame[op.x.Rd]);

			uSize = 32;

			return true;
			}
		}

	return false;
	}

static	bool	Fix16(PDWORD pFrame, OP16 const &op, PDWORD pData, UINT &uSize)
{
	if( op.x.Type == 0 && op.x.SubType3 == 1 && op.x.SubType0 == 1 ) {

		// This encoding could be 16-bit or 8-bit signed load/store but
		// the 8-bit version cannot occur as it can never be misaligned.
		// It could also be an extension indicated by a signed store.

		if( op.x.Rd > 12 ) {

			// Can't access anything except r0-r12.

			return false;
			}

		if( op.x.Mode ) {

			// Only immediate offset mode supports write back,
			// so we do not have to worry about other ones.

			if( op.x.OffsetL || op.x.OffsetH ) {

				// Only bother if the offset is non-zero.

				if( op.x.Rn > 12 ) {

					// Can't access anything except r0-r12.
		
					return false;
					}

				if( op.x.P ) {

					if( op.x.W ) {

						// Preindexed, so the location being accessed
						// already represents all the EA calculations.

						pFrame[op.x.Rn] = DWORD(pData);
						}
					}
				else {
					// Postindexed, so we must manually apply
					// the adjustment to the indexing register.

					BYTE Offset = (op.x.OffsetL << 0) 
						    + (op.x.OffsetH << 4);

					if( op.x.U ) {

						pFrame[op.x.Rn] += Offset;
						}
					else
						pFrame[op.x.Rn] -= Offset;
					}
				}
			}

		if( op.x.L ) {

			if( op.x.S ) {

				// 16-bit Signed Load

				DWORD Data = Get16(pData);

				if( Data & 0x8000 ) {

					Data |= 0xFFFF0000;
					}

				pFrame[op.x.Rd] = Data;

				uSize = 16;

				return true;
				}
			else {
				// 16-bit Unsigned Load

				pFrame[op.x.Rd] = Get16(pData);

				uSize = 16;

				return true;
				}
			}
		else {
			if( op.x.S ) {

				// Signed store doesn't make sense so
				// this must be an extension operation.
				
				if( op.x.H ) {

					// 64-bit Store

					Put32(pData+0, pFrame[op.x.Rd+0]);

					Put32(pData+1, pFrame[op.x.Rd+1]);

					uSize = 64;

					return true;
					}
				else {
					// 64-bit Load

					pFrame[op.x.Rd+0] = Get32(pData+0);

					pFrame[op.x.Rd+1] = Get32(pData+1);

					uSize = 64;

					return true;
					}
				}

			// 16-bit Store

			Put16(pData, pFrame[op.x.Rd]);

			uSize = 16;

			return true;
			}
		}

	return false;
	}

static	DWORD	Get32(PDWORD pData)
{
	PBYTE pByte = PBYTE(pData);

	BYTE  b0 = *pByte++;
	BYTE  b1 = *pByte++;
	BYTE  b2 = *pByte++;
	BYTE  b3 = *pByte++;

	return MAKELONG(MAKEWORD(b0,b1),MAKEWORD(b2,b3));
	}

static	void	Put32(PDWORD pData, DWORD dwData)
{
	PBYTE pByte = PBYTE(pData);

	*pByte++ = LOBYTE(LOWORD(dwData));
	*pByte++ = HIBYTE(LOWORD(dwData));
	*pByte++ = LOBYTE(HIWORD(dwData));
	*pByte++ = HIBYTE(HIWORD(dwData));
	}

static	WORD	Get16(PDWORD pData)
{
	PBYTE pByte = PBYTE(pData);

	BYTE  b0 = *pByte++;
	BYTE  b1 = *pByte++;

	return MAKEWORD(b0,b1);
	}

static	void	Put16(PDWORD pData, WORD wData)
{
	PBYTE pByte = PBYTE(pData);

	*pByte++ = LOBYTE(wData);
	*pByte++ = HIBYTE(wData);
	}

// End of File
