
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_HalSitara_HPP

#define INCLUDE_HalSitara_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../RedArm/ArmHal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCtrl437;
class CDmt437;
class CClock437;
class CWatchdog437;

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IPic * Create_Gic437(void);

extern IMmu * Create_Mmu437(IHal *pHal);

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Sitara
//

class CHalSitara : public CArmHal
{
	public:
		// Constructor
		CHalSitara(void);

		// Destructor
		~CHalSitara(void);

		// Initialization
		void Open(void);

		// IHal Timer Support
		UINT GetTickFraction(void);
		void SpinDelayFast(UINT uTime);

		// IHal Debug Support
		void DebugReset(void);
		UINT DebugRead(UINT uDelay);
		void DebugOut(char cData);
		void DebugWait(void);
		void DebugStop(void);
		void DebugKick(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Data Members
		DWORD          m_uDebugAddr;
		UINT           m_uDebugLine;
		CWatchdog437 * m_pDog;
		CCtrl437     * m_pCtrl;
		CClock437    * m_pClock;
		CDmt437      * m_pDmt[2];
		PVWORD         m_pData;
		PVWORD         m_pTest;
		PVWORD	       m_pLine;
		PVWORD	       m_pMode;
		ISemaphore   * m_pRead;
		BYTE	       m_bBuff[256];
		UINT	       m_uHead;
		UINT	       m_uTail;

		// Implementation
		void FindDebug(void);
		void CreateDevices(void);
		void StartTimer(void);

		// Friends
		friend class CPlatformSitara;
	};

// End of File

#endif
