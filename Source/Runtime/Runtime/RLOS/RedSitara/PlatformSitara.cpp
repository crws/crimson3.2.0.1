
#include "Intern.hpp"

#include "PlatformSitara.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "HalSitara.hpp"

#include "Ctrl437.hpp"

#include "Clock437.hpp"

#include "Pmic437.hpp"

#include "Pcm437.hpp"

#include "Gpio437.hpp"

#include "Adc437.hpp"

#include "Gpmc437.hpp"

#include "Elm437.hpp"

#include "Emif437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice * Create_InputQueueRLOS	 (void);
extern IDevice * Create_I2cInterface437	 (CClock437 *pClock);
extern IDevice * Create_SpiInterface437	 (CClock437 *pClock);
extern IDevice * Create_BatteryMonitor437(CAdc437 *pAdc);
extern IDevice * Create_Nand437		 (CClock437 *pClock, CGpmc437 *pGpmc, CElm437 *pElm);
extern IDevice * Create_Gpio437          (UINT iIndex);
extern IDevice * Create_Entropy		 (void);

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Sitara Device
//

// Constructor

CPlatformSitara::CPlatformSitara(BOOL fRack) : CPlatformRed(fRack)
{
	m_opp = oppTURBO;
 
	DiagRegister();
	}

// Destructor

CPlatformSitara::~CPlatformSitara(void)
{
	DiagRevoke();

	piob->RevokeGroup("dev.");

	delete m_pAdc;
	delete m_pEmif;
	delete m_pElm;
	delete m_pGpmc;
	delete m_pPmic;
	delete m_pPcm;
	}

// IUnknown

HRESULT CPlatformSitara::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IDiagProvider);

	return CPlatformBase::QueryInterface(riid, ppObject);
	}

ULONG CPlatformSitara::AddRef(void)
{
	return CPlatformBase::AddRef();
	}

ULONG CPlatformSitara::Release(void)
{
	return CPlatformBase::Release();
	}

// IDevice

BOOL CPlatformSitara::Open(void)
{
	phal->DebugWait();

	InitPriorities();
	
	m_pClock = ((CHalSitara *) phal)->m_pClock;

	m_pCtrl  = ((CHalSitara *) phal)->m_pCtrl;

	InitClocks();

	m_pPcm = New CPcm437();

	m_pAdc = New CAdc437();

	piob->RegisterSingleton("dev.gpio", 0, m_pGpio[0] = New CGpio437(0));
	piob->RegisterSingleton("dev.gpio", 1, m_pGpio[1] = New CGpio437(1));
	piob->RegisterSingleton("dev.gpio", 2, m_pGpio[2] = New CGpio437(2));
	piob->RegisterSingleton("dev.gpio", 3, m_pGpio[3] = New CGpio437(3));
	piob->RegisterSingleton("dev.gpio", 4, m_pGpio[4] = New CGpio437(4));
	piob->RegisterSingleton("dev.gpio", 5, m_pGpio[5] = New CGpio437(5));

	InitGpio();

	InitMux();

	piob->RegisterSingleton("dev.i2c",  0, Create_I2cInterface437(m_pClock));

	piob->RegisterSingleton("dev.spi",  0, Create_SpiInterface437(m_pClock));

	piob->RegisterSingleton("dev.batt", 0, Create_BatteryMonitor437(m_pAdc));

	m_pPmic = New CPmic437();

	InitRails();

	InitMpu();

	m_pGpmc = New CGpmc437();

	m_pElm  = New CElm437();

	m_pEmif = New CEmif437();

	MakeDevice("dev.input-d", 1, Create_InputQueueRLOS);

	MakeDevice("dev.input-x", 1, Create_InputQueueRLOS);

	MakeDevice("dev.entropy", 1, Create_Entropy);

	// TODO	-- Add device-specific entropy source from somewhere !!!

	InitMisc();

	InitNand();
	
	piob->RegisterSingleton("dev.nand", 0, Create_Nand437(m_pClock, m_pGpmc, m_pElm));

	RegisterBaseCommon();

	return TRUE;
	}

// IPlatform

PCTXT CPlatformSitara::GetFamily(void)
{
	return "";
}

PCTXT CPlatformSitara::GetModel(void)
{
	return "none";
	}

BOOL CPlatformSitara::IsService(void)
{
	return FALSE;
	}

BOOL CPlatformSitara::IsEmulated(void)
{
	return FALSE;
	}

BOOL CPlatformSitara::HasMappings(void)
{
	return FALSE;
	}

// IEventSink

void CPlatformSitara::OnEvent(UINT uLine, UINT uParam)
{
	}

// IDiagProvider

UINT CPlatformSitara::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)

	switch( pCmd->GetCode() ) {

		case 1:
			return DiagClocks(pOut, pCmd);

		case 2:
			return DiagRails(pOut, pCmd);

		case 3:
			return DiagSetOpp(pOut, pCmd);

		case 4:
			return DiagSetDdr(pOut, pCmd);

		case 5:
			return DiagSetBw(pOut, pCmd);

		case 6:
			return DiagSetDssClk(pOut, pCmd);

		case 7:
			return DiagTemps(pOut, pCmd);
		}

	#endif

	return 0;
	}

// Inititialisation

void CPlatformSitara::InitPriorities(void)
{
	phal->SetLinePriority(INT_TIMER2,	1);
	phal->SetLinePriority(INT_TIMER3,	1);
	phal->SetLinePriority(INT_TIMER4,	1);
	phal->SetLinePriority(INT_TIMER5,	1);
	phal->SetLinePriority(INT_I2C0,		3);
	phal->SetLinePriority(INT_ADC0_GEN,	2);
	phal->SetLinePriority(INT_ADC1_GEN,	2);
	phal->SetLinePriority(INT_MMCSD0,	2);
	phal->SetLinePriority(INT_UART0,	5);
	phal->SetLinePriority(INT_UART1,	3);
	phal->SetLinePriority(INT_UART2,	5);
	phal->SetLinePriority(INT_UART3,	5);	
	phal->SetLinePriority(INT_SPI2,		6);
	phal->SetLinePriority(INT_GPMC,		2);
	phal->SetLinePriority(INT_ELM,		2);
	phal->SetLinePriority(INT_3PGSWRXTHR0,	4);
	phal->SetLinePriority(INT_3PGSWRX0,	4);
	phal->SetLinePriority(INT_3PGSWTX0,	4);
	phal->SetLinePriority(INT_3PGSWMISC0,	4);
	phal->SetLinePriority(INT_USB0_MAIN0,	4);
	phal->SetLinePriority(INT_USB0_MISC,	4);
	phal->SetLinePriority(INT_USB1_MAIN0,	4);
	phal->SetLinePriority(INT_USB1_MISC,	4);
	}

void CPlatformSitara::InitClocks(void)
{
	m_pClock->SetCorePLLFreq(125, 2, 10, 8, 4); 

	m_pClock->SetPerPLLFreq(960, 23, 5);

	m_pClock->SetMpuFreq(800, 63, 1);

	m_pClock->SetClockMode(CClock437::clockADC1,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockELM,   CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockGPMC,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockI2C0,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockSPI2,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockGPIO0, CClock437::modeSwWakup | CClock437::modeOption);
	m_pClock->SetClockMode(CClock437::clockGPIO1, CClock437::modeSwWakup | CClock437::modeOption);
	m_pClock->SetClockMode(CClock437::clockGPIO2, CClock437::modeSwWakup | CClock437::modeOption);
	m_pClock->SetClockMode(CClock437::clockGPIO3, CClock437::modeSwWakup | CClock437::modeOption);
	m_pClock->SetClockMode(CClock437::clockGPIO4, CClock437::modeSwWakup | CClock437::modeOption);
	m_pClock->SetClockMode(CClock437::clockGPIO5, CClock437::modeSwWakup | CClock437::modeOption);
	}

void CPlatformSitara::InitRails(void)
{
	m_pPmic->PutReg(CPmic437::regDCDC1, 0x19); // Core @ 1.100V

	m_pPmic->PutReg(CPmic437::regDCDC2, 0x29); // MPU  @ 1.260V

//	m_pPmic->PutReg(CPmic437::regDCDC3, 0x12); // DDR  @ 1.350V

	m_pPmic->PutReg(CPmic437::regDCDC3, 0x18); // DDR  @ 1.500V

	m_pPmic->PutReg(CPmic437::regDCDC4, 0x32); // 3.3V @ 3.300V

	m_pPmic->Update();
	}

void CPlatformSitara::InitMpu(void)
{
	SetMpuBandwidth(300);

	SetMpuOpp(m_opp);
	}

void CPlatformSitara::InitMux(void)
{
	DWORD const muxBat[] = {

		CCtrl437::pinGPMCCSN1,	 CCtrl437::muxMode7 | CCtrl437::padPullNo,
		};

	DWORD const muxPsu[] = {

		CCtrl437::pinGPMCBE1N,   CCtrl437::muxMode7 | CCtrl437::padPullNo,
		};

	DWORD const muxI2c[] = {

		CCtrl437::pinI2C0SCL,	 CCtrl437::muxMode0 | CCtrl437::padSleepPullNo | CCtrl437::padRxActive | CCtrl437::padSlow,
		CCtrl437::pinI2C0SDA,	 CCtrl437::muxMode0 | CCtrl437::padSleepPullNo | CCtrl437::padRxActive | CCtrl437::padSlow,
		};

	DWORD const muxSpi[] = {

		CCtrl437::pinSPI2SCLK,	 CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinSPI2D0,	 CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinSPI2D1,	 CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinCAM0PCLK,	 CCtrl437::muxMode4 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM0HD,	 CCtrl437::muxMode4 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM0DATA8,	 CCtrl437::muxMode4 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM0DATA9,	 CCtrl437::muxMode4 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		};

	DWORD const muxGpmc[] = {

		CCtrl437::pinGPMCAD0,     CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCAD1,     CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCAD2,     CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCAD3,     CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCAD4,     CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCAD5,     CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCAD6,     CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCAD7,     CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCCSN0,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinGPMCADVNALE, CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinGPMCOENREN,  CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinGPMCWEN,     CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinGPMCBE0NCLE, CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinGPMCWPN,     CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinGPMCCSN3,    CCtrl437::muxMode1 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		};

	m_pCtrl->SetMux(muxBat, elements(muxBat));

	m_pCtrl->SetMux(muxPsu, elements(muxPsu));

	m_pCtrl->SetMux(muxI2c, elements(muxI2c));

	m_pCtrl->SetMux(muxSpi, elements(muxSpi));

	m_pCtrl->SetMux(muxGpmc, elements(muxGpmc));
	}

void CPlatformSitara::InitGpio(void)
{
	m_pGpio[1]->SetState(28, true);
	m_pGpio[1]->SetState(30, true);

	m_pGpio[1]->SetDirection(28, true);
	m_pGpio[1]->SetDirection(30, true);
	}

void CPlatformSitara::InitMisc(void)
{
	}

void CPlatformSitara::InitNand(void)
{
	m_pGpmc->SetLimitedAddress(true);
	m_pGpmc->SetTimeout(0);
	m_pGpmc->SetWritProtect(true);
	m_pGpmc->SetChipSelEnable(0, false);
	m_pGpmc->SetChipSelType(0, CGpmc437::typeNand);
	m_pGpmc->SetChipSelWidth(0, 8);
	m_pGpmc->SetChipSelTimeGranularity(0, 1);
	m_pGpmc->SetChipSelWaitConfig(0, 0, false);
	m_pGpmc->SetChipSelMux(0, CGpmc437::muxNone);
	m_pGpmc->SetChipSelAccessType(0, true, true);
	m_pGpmc->SetChipSelAccessMode(0, true, true);
	m_pGpmc->SetChipSelTiming(0, 0, 4, 4, false);	
	m_pGpmc->SetChipSelAdvTiming(0, 0, 3, 3, false, 0, 0, 0);
	m_pGpmc->SetChipSelWETimings(0, 2, 0, false);
	m_pGpmc->SetChipSelOETimings(0, 3, 1, false);
	m_pGpmc->SetChipSelOEMuxTimings(0, 0, 0);
	m_pGpmc->SetChipSelRdAccessTiming(0, 0, 3, 4, 4);
	m_pGpmc->SetChipSelWrAccessTiming(0, 4, 0);
	m_pGpmc->SetChipSelCycleDelayTiming(0, 0, 0, 0, 0);
	m_pGpmc->SetChipSelBase(0, 0x00000000, 256);	
	m_pGpmc->SetChipSelEnable(0, true);

	m_pGpmc->SetEccChipSelect(0);
	m_pGpmc->SetEcc16(false);
	m_pElm->SetAutoGating(true);
	m_pElm->SetBchBuffSize(0x7FF);
	m_pElm->SetPageMode(CElm437::poly0);
	}

// Diagnostics

bool CPlatformSitara::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "proc");

		pDiag->RegisterCommand(m_uProv, 1, "clocks");
		pDiag->RegisterCommand(m_uProv, 2, "rails");
		pDiag->RegisterCommand(m_uProv, 3, "setopp");
		pDiag->RegisterCommand(m_uProv, 4, "setddr");
		pDiag->RegisterCommand(m_uProv, 5, "setbw");
		pDiag->RegisterCommand(m_uProv, 6, "setdssclk");
		pDiag->RegisterCommand(m_uProv, 7, "temps");

		pDiag->Release();

		return TRUE;
		}

	return FALSE;
	}

bool CPlatformSitara::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return TRUE;
		}

	return FALSE;
	}

UINT CPlatformSitara::DiagClocks(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)
	
	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		pOut->AddProp("MAX",     "%10d", m_pCtrl->GetMaxFreq());
		pOut->AddProp("OSC",     "%10d", m_pCtrl->GetRefFreq());
		pOut->AddProp("OCR",     "%10d", m_pCtrl->GetOcrFreq());
		pOut->AddProp("PLLLS",   "%10d", m_pClock->GetCorePLLFreq());
		pOut->AddProp("PLLLJ",   "%10d", m_pClock->GetPerPLLFreq());
		pOut->AddProp("Core M4", "%10d", m_pClock->GetCoreM4Freq());
		pOut->AddProp("Core M5", "%10d", m_pClock->GetCoreM5Freq());
		pOut->AddProp("Core M6", "%10d", m_pClock->GetCoreM6Freq());
		pOut->AddProp("PER M2",  "%10d", m_pClock->GetPerM2Freq());
		pOut->AddProp("MPU",     "%10d", m_pClock->GetMpuFreq());
		pOut->AddProp("DDR",     "%10d", m_pClock->GetDdrFreq());
		pOut->AddProp("Display", "%10d", m_pClock->GetDispFreq());
		pOut->AddProp("Extern",  "%10d", m_pClock->GetExternFreq());

		pOut->EndPropList();

		return 0;
		}

	pOut->Error(NULL);

	#endif

	return 1;
	}

UINT CPlatformSitara::DiagRails(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)

	if( !pCmd->GetArgCount() ) {

		UINT v1 = m_pPmic->GetVoltage(1);
		UINT v2 = m_pPmic->GetVoltage(2);
		UINT v3 = m_pPmic->GetVoltage(3);
		UINT v4 = m_pPmic->GetVoltage(4);

		pOut->AddPropList();

		pOut->AddProp("VDCDC1", "%u.%3.3u", v1/1000, v1%1000);
		pOut->AddProp("VDCDC2", "%u.%3.3u", v2/1000, v2%1000);
		pOut->AddProp("VDCDC3", "%u.%3.3u", v3/1000, v3%1000);
		pOut->AddProp("VDCDC4", "%u.%3.3u", v4/1000, v4%1000);

		pOut->EndPropList();

		return 0;
		}

	pOut->Error(NULL);

	#endif

	return 1;
	}

UINT CPlatformSitara::DiagTemps(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)
	
	if( !pCmd->GetArgCount() ) {

		ISensor *p1 = NULL;
		ISensor *p2 = NULL;
		ISensor *p3 = NULL;

		AfxGetObject("dev.temp", 0, ISensor, p1);
		AfxGetObject("dev.temp", 1, ISensor, p2);
		AfxGetObject("dev.temp", 2, ISensor, p3);

		UINT t1 = p1 ? p1->GetData() : 0;
		UINT t2 = p2 ? p2->GetData() : 0;
		UINT t3 = p3 ? p3->GetData() : 0;

		BOOL f1 = p1 ? p1->GetAlarm() : false;
		BOOL f2 = p2 ? p2->GetAlarm() : false;
		BOOL f3 = p3 ? p3->GetAlarm() : false;

		AfxRelease(p1);
		AfxRelease(p2);
		AfxRelease(p3);

		pOut->AddPropList();

		pOut->AddProp("TEMP1", "%-d.%d", t1/10, t1%10);
		pOut->AddProp("TEMP2", "%-d.%d", t2/10, t2%10);
		pOut->AddProp("TEMP3", "%-d.%d", t3/10, t3%10);

		pOut->AddProp("ALERT1", "%d", f1);
		pOut->AddProp("ALERT2", "%d", f2);
		pOut->AddProp("ALERT3", "%d", f3);

		pOut->EndPropList();

		return 0;
		}

	pOut->Error(NULL);

	#endif

	return 1;
	}

UINT CPlatformSitara::DiagSetOpp(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)

	if( pCmd->GetArgCount() == 1 ) {

		PCTXT list[] = { "OPP50", "OPP100", "OPP120", "TURBO" };

		for( UINT n = 0; n < elements(list); n++ ) {

			if( !strcmp(pCmd->GetArg(0), list[n]) ) {

				SetMpuOpp(n);

				pOut->Print("\nproc.setopp: OPP set to %s\n", list[n]);

				return 0;
				}
			}

		pOut->Error("unknown opp name");

		return 2;
		}

	pOut->Error(NULL);

	#endif

	return 1;
	}

UINT CPlatformSitara::DiagSetDdr(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)

	if( pCmd->GetArgCount() == 1 ) {

		PCTXT list[] = { "1.50", "1.35" };

		UINT  volt[] = { 0x18,   0x12   };

		for( UINT n = 0; n < elements(list); n++ ) {

			if( !strcmp(pCmd->GetArg(0), list[n]) ) {

				m_pPmic->PutReg(CPmic437::regDCDC3, volt[n]);

				m_pPmic->Update();

				pOut->Print("\nproc.setddr: DDR set to %s\n", list[n]);

				return 0;
				}
			}

		pOut->Error("unknown voltage");

		return 2;
		}

	pOut->Error(NULL);

	#endif

	return 1;
	}

UINT CPlatformSitara::DiagSetBw(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)

	if( pCmd->GetArgCount() == 1 ) {

		UINT bw = atoi(pCmd->GetArg(0));

		if( bw >= 10 && bw <= 511 ) {

			PVDWORD pf = PVDWORD(0x44000000 + 0x5208);

			PVDWORD pi = PVDWORD(0x44000000 + 0x520C);

			SetMpuBandwidth(bw);

			pOut->Print("\nproc.setbw: bandwidth set to %u = 0x%2.2X-0x%2.2X\n", bw, *pi, *pf);

			return 0;
			}

		pOut->Error("bandwidth must be between 10 and 511");

		return 2;
		}

	pOut->Error(NULL);

	#endif

	return 1;
	}

UINT CPlatformSitara::DiagSetDssClk(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)

	if( pCmd->GetArgCount() == 1 ) {

		UINT uFreq = atoi(pCmd->GetArg(0));

		if( uFreq >= 10000000 && uFreq <= 80000000 ) {

			UINT m = uFreq / 50000;

			UINT n = 40;

			if( m > 2047 ) {

				m >>= 1;

				n <<= 1;
				}

			m_pClock->SetDispFreq(m, n - 1, 3);

			pOut->Print("\nproc.setdssclk: dss clock set to %u\n", m_pClock->GetDispFreq() / 4);

			return 0;
			}

		pOut->Error("dss clock must be between 10000000 and 80000000");

		return 2;
		}

	pOut->Error(NULL);

	#endif

	return 1;
	}

// Implementation

void CPlatformSitara::SetMpuBandwidth(UINT bw)
{
	PVDWORD pf = PVDWORD(0x44000000 + 0x5208);

	PVDWORD pi = PVDWORD(0x44000000 + 0x520C);

	PVDWORD pw = PVDWORD(0x44000000 + 0x5210);

	*pf = (bw  & 0x1F);

	*pi = (bw >> 0x05);

	*pw = 0;
	}

bool CPlatformSitara::SetMpuOpp(UINT opp)
{
	UINT  freq[] = { 63, 31, 26, 23 };

	UINT  volt[] = { 0x0A, 0x19, 0x23, 0x29 };

	if( opp < elements(freq) ) {

		// TODO -- This should change voltage first when speeding
		// up, and work as is when slowing down. We should also move
		// the whole thing to the HAL for power management.

		m_pClock->SetMpuFreq(800, freq[opp], 1);

		m_pPmic->PutReg(CPmic437::regDCDC2, volt[opp]);

		m_pPmic->Update();

		return true;
		}

	return false;
	}

// End of File
