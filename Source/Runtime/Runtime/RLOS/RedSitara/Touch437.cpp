
#include "Touch437.hpp"

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define Reg(x) (m_pBase[reg##x])

//////////////////////////////////////////////////////////////////////////
//
// AM437 Touch Screen Controller
//

// Instantiator

IDevice * Create_Touch437(BOOL fFlipX, BOOL fFlipY, UINT uMaxVar, UINT uMinVal)
{
	CTouch437 *pDevice = New CTouch437(fFlipX, fFlipY, uMaxVar, uMinVal);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CTouch437::CTouch437(BOOL fFlipX, BOOL fFlipY, UINT uMaxVar, UINT uMinVal)
{
	m_pBase   = PVDWORD(ADDR_ADC_TSC);

	m_uSlot   = addrTouch;

	m_uLine   = INT_ADC0_GEN;

	m_fFlipX  = fFlipX;

	m_fFlipY  = fFlipY;

	m_uMaxVar = uMaxVar;

	m_uMinVal = uMinVal;

	m_uState  = stateWaitDown;

	m_uCount  = 0;

	m_uWait   = 0;

	m_uValid  = 0;

	m_pTimer  = CreateTimer();
	}

// Destructor

CTouch437::~CTouch437(void)
{
	m_pTimer->Release();
	}

// IDevice

BOOL METHOD CTouch437::Open(void)
{
	if( CTouchGeneric::Open() ) { 

		LoadCalib();

		InitController();

		InitEvents();
	
		return TRUE;
		}

	return FALSE;
	}

// IEventSink

void CTouch437::OnEvent(UINT uLine, UINT uParam)
{
	if( uParam == 0 ) {

		OnTouch();
		}

	if( uParam == 1 ) {

		OnTimer();
		}
	}

// Overridables

void CTouch437::DefaultCalib(void)
{
	m_xMin = 150;
	m_xMax = 4000;
	m_yMin = 160;
	m_yMax = 3850;
	}

bool CTouch437::OnTouchValue(void)
{
	m_uCode = NOTHING;
	
	return IsActive();
	}

// Implementation

void CTouch437::InitController(void)
{
	Reg(CTRL)        |= 0xA6;

	Reg(SYSCONFIG)    = Bit(3);

	Reg(IRQWAKEUP)    = Bit(0);

	Reg(ADC_CLKDIV)   = 23;

	Reg(IDLECONFIG)	  = Bit(8);

	Reg(CHARGE_STEP)  = Bit(8);

	Reg(CHARGE_DELAY) = adcCharge;

	DWORD dwXStep     = 0x0011006B;

	DWORD dwYStep     = 0x0000018B;

	DWORD dwZStep     = 0x040000C3;
	
	DWORD dwDelay	  = adcDelay | ((adcSample - 1) << 24);

	Reg(STEPCONFIG_0) = dwZStep; // Z1
	Reg(STEPCONFIG_1) = dwXStep; // X1
	Reg(STEPCONFIG_2) = dwYStep; // Y1
	Reg(STEPCONFIG_3) = dwXStep; // X2
	Reg(STEPCONFIG_4) = dwYStep; // Y2
	Reg(STEPCONFIG_5) = dwXStep; // X3
	Reg(STEPCONFIG_6) = dwYStep; // Y3
	Reg(STEPCONFIG_7) = dwXStep; // X4
	Reg(STEPCONFIG_8) = dwYStep; // Y4
	Reg(STEPCONFIG_9) = dwZStep; // Z2

	Reg(STEPDELAY_0)  = dwDelay;
	Reg(STEPDELAY_1)  = dwDelay;
	Reg(STEPDELAY_2)  = dwDelay;
	Reg(STEPDELAY_3)  = dwDelay;
	Reg(STEPDELAY_4)  = dwDelay;
	Reg(STEPDELAY_5)  = dwDelay;
	Reg(STEPDELAY_6)  = dwDelay;
	Reg(STEPDELAY_7)  = dwDelay;
	Reg(STEPDELAY_8)  = dwDelay;
	Reg(STEPDELAY_9)  = dwDelay;

	Reg(STEPEN)       = 0x07FF;
	
	Reg(IRQEN_SET)    = intEndOfSeq | intPenUp;

	Reg(CTRL)        |= Bit(0);
	}

void CTouch437::InitEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);

	m_pTimer->SetPeriod(5);

	m_pTimer->SetHook(this, 1);

	m_pTimer->Enable(true);
	}

void CTouch437::OnTouch(void)
{
	for(;;) {

		DWORD Data = Reg(IRQSTS) & Reg(IRQEN_SET);

		bool  fHit = false;

		if( Data & intPenUp ) {

			Reg(IRQSTS) = intPenUp;

			fHit        = true;

			if( m_uState == stateWaitUp ) {

				Reg(STEPEN) = 0;

				PostEvent(stateUp);

				m_uState = stateWaitDown;

				m_uValid = 0;

				m_uWait  = ToTicks(timeWait);
				}
			}

		if( Data & intEndOfSeq ) {

			Reg(IRQSTS) = intEndOfSeq;

			fHit        = true;

			UINT const nc = 4;
			UINT const nz = 2;

			for( UINT c = 0; c < nc; c++ ) {

				m_wXSamples[c] = Reg(FIFO0DATA) & 0x0FFF;

				m_wYSamples[c] = Reg(FIFO0DATA) & 0x0FFF;
				}

			for( UINT z = 0; z < nz; z++ ) {

				m_wZSamples[z] = Reg(FIFO1DATA) & 0x0FFF;
				}

			if( m_uState == stateWaitDown ) {

				/* AfxTrace("p=%d,%d\n", m_wZSamples[0], m_wZSamples[1]); */
			
				if( CheckPressure(m_wZSamples, nz) ) {

					if( ++m_uValid >= m_uMinVal ) {

						UINT uPosX = FindTouch(m_wXSamples, nc);

						UINT uPosY = FindTouch(m_wYSamples, nc);

						if( uPosX != NOTHING && uPosY != NOTHING ) {

							m_xRaw = uPosX;

							m_yRaw = uPosY;

							if( IsValidTouch() ) {

								PostEvent(stateDown);

								m_uCount = ToTicks(timeInitial);

								m_uValid = 0;

								m_uState = stateWaitUp;
								}
							}
						}
					}
				else
					m_uValid = 0;
				}

			if( m_uState == stateWaitUp ) {

				// This should not be required as the pen-up interrupt
				// should fire before the pressure backs off this far,
				// but the code is here in case the charge time is not
				// sufficient for the pen-up to fire. The time has been
				// selected with adequate margin, but the extra code is
				// still worth including. Note that once we disable the
				// sequence, the pen-up interrupt will probably fire
				// anyway, but there's no harm from the double signal.

				/*AfxTrace("p=%d,%d\n", m_wZSamples[0], m_wZSamples[1]);*/
			
				if( CheckRelease(m_wZSamples, nz) ) {

					if( ++m_uValid >= minRelease ) {

						Reg(STEPEN) = 0;

						PostEvent(stateUp);

						m_uState = stateWaitDown;

						m_uValid = 0;

						m_uWait  = ToTicks(timeWait);
						}
					}
				else
					m_uValid = 0;
				}
			}

		if( !fHit ) {

			break;
			}
		}
	}

void CTouch437::OnTimer(void)
{
	if( m_uCount && !--m_uCount ) {

		if( m_uState == stateWaitUp ) {

			m_uCount = ToTicks(timeRepeat);

			PostEvent(stateRepeat);
			}
		}

	if( m_uWait && !--m_uWait ) {

		Reg(STEPEN) = 0x07FF;
		}
	}

UINT CTouch437::FindTouch(PWORD pSamples, UINT uCount)
{
	if( uCount ) {

		UINT uMin = NOTHING;
		
		UINT uMax = 0;
		
		UINT uSum = 0;

		for( UINT n = 0; n < uCount; n++ ) {

			MakeMin(uMin, pSamples[n]);
			
			MakeMax(uMax, pSamples[n]);

			uSum += pSamples[n];
			}

		/*AfxTrace("s=%d\n", uMax - uMin);*/

		if( uMax - uMin <= m_uMaxVar ) {

			return uSum / uCount;
			}
		}

	return NOTHING;
	}

bool CTouch437::CheckPressure(PWORD pSamples, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		if( pSamples[n] < minPressure ) {

			return false;
			}
		}

	return true;
	}

bool CTouch437::CheckRelease(PWORD pSamples, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {
		
		if( pSamples[n] > maxRelease ) {

			return false;
			}
		}

	return true;
	}

// End of File
