
#include "Intern.hpp"

#include "DisplayBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display Base Driver
//

// Constructor

CDisplayBase::CDisplayBase(void)
{
	StdSetRef();

	m_pBuff = NULL;

	m_pMem  = NULL;

	m_pLock = NULL;

	m_cx    = 0;

	m_cy    = 0;

	m_pLock = Create_Mutex();

	AfxGetObject("fram",  0, ISerialMemory, m_pMem);
	}

// Destructor

CDisplayBase::~CDisplayBase(void)
{
	free(m_pBuff);

	m_pLock->Release();

	AfxRelease(m_pMem);
	}

// IUnknown

HRESULT CDisplayBase::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IDisplay);

	return E_NOINTERFACE;
	}

ULONG CDisplayBase::AddRef(void)
{
	StdAddRef();
	}

ULONG CDisplayBase::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CDisplayBase::Open(void)
{
	return TRUE;
	}

// IDisplay

void METHOD CDisplayBase::Claim(void)
{
	m_pLock->Wait(FOREVER);
	}

void METHOD CDisplayBase::Free(void)
{
	m_pLock->Free();
	}

void METHOD CDisplayBase::GetSize(int &cx, int &cy)
{
	cx = m_cx;

	cy = m_cy;
	}

void METHOD CDisplayBase::Update(PCVOID pData)
{
	}

BOOL METHOD CDisplayBase::SetBacklight(UINT nPercent)
{
	return FALSE;
	}

UINT METHOD CDisplayBase::GetBacklight(void)
{
	return FALSE;
	}

BOOL METHOD CDisplayBase::EnableBacklight(BOOL fOn)
{
	return FALSE;
	}

BOOL METHOD CDisplayBase::IsBacklightEnabled(void)
{
	return FALSE;
	}

// Implementation

WORD CDisplayBase::LoadBacklight(WORD wDefault)
{
	if( m_pMem ) {

		WORD ds[3];

		m_pMem->GetData(addrDisplay, PBYTE(&ds), sizeof(ds));

		if( ds[0] == MAKEWORD('D','S') ) {

			return ds[1];
			}
		}
	
	return wDefault;
	}

void CDisplayBase::SaveBacklight(WORD wLevel)
{
	if( m_pMem ) {

		WORD ds[3];

		ds[0] = MAKEWORD('D','S');

		ds[1] = wLevel;

		ds[2] = 0;

		m_pMem->PutData(addrDisplay, PBYTE(&ds), sizeof(ds));
		}
	}

// End of File
