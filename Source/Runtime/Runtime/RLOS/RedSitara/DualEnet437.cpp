
#include "Intern.hpp"

#include "DualEnet437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader()

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Nic437.hpp"

#include "Clock437.hpp"

#include "Nic437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Dual Ethernet
//

// Register Access
 
#define Reg(x)	   (m_pBase[reg##x])

#define RegN(x,n)  (m_pBase[reg##x + (n)])

// ALE Bit Access

#define BitN(b, m) ((d[(b)/32] >> ((b) % 32)) & (m))

#define ALE_SIZE   1024

// Instantiator

IDevice * Create_DualEnet437(CClock437 *pClock, UINT uGpio)
{
	CDualEnet437 *pDevice = New CDualEnet437(pClock, uGpio);

	pDevice->Open();

	return (IDevice *) pDevice;
	}

// Constructor

CDualEnet437::CDualEnet437(CClock437 *pClock, UINT uGpio)
{
	StdSetRef();

	m_pBase   = PVDWORD(ADDR_CPSW);

	m_uLineRt = INT_3PGSWRXTHR0;
		       
	m_uLineRx = INT_3PGSWRX0;
		       
	m_uLineTx = INT_3PGSWTX0;
		     
	m_uLineMs = INT_3PGSWMISC0;

	m_uFreq   = pClock->GetCoreM4Freq() / 2;

	m_uGpio   = uGpio;

	m_fInit   = false;

	m_uCount  = 0;

	m_pMutex  = Create_Mutex();

	DiagRegister();
	}

// Destructor

CDualEnet437::~CDualEnet437(void)
{
	DiagRevoke();

	m_pMutex->Release();

	piob->RevokeGroup("dev.nic");
	}

// IUnknown

HRESULT CDualEnet437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CDualEnet437::AddRef(void)
{
	StdAddRef();
	}

ULONG CDualEnet437::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CDualEnet437::Open(void)
{
	ResetController();

	m_pNic[0] = New CNic437(this, 1, 0, LOWORD(m_uGpio)); 

	m_pNic[1] = New CNic437(this, 2, 1, HIWORD(m_uGpio));
	
	piob->RegisterSingleton("dev.nic", 0, (IDevice *) m_pNic[0]);

	piob->RegisterSingleton("dev.nic", 1, (IDevice *) m_pNic[1]);
	
	return TRUE;
	}

// ALE Management

void CDualEnet437::AddVlanEntry(UINT uPort, UINT uVlan)
{
	if( m_pMutex->Wait(FOREVER) ) {

		if( FindVlanEntry(uPort, uVlan) == NOTHING ) {

			UINT uIndex = FindFreeAleSlot();

			if( uIndex < NOTHING ) {

				BYTE bType = 2;

				BYTE bMask = Bit(0) | Bit(uPort);

				Reg(AleTable2)    = 0;
				Reg(AleTable1)    = (bType << 28) | (uVlan << 16);
				Reg(AleTable0)    = (bMask << 8)  | bMask;

				Reg(AleTableCtrl) = Bit(31) | uIndex;
				}
			}

		m_pMutex->Free();
		}
	}

void CDualEnet437::AddVlanAddress(UINT uVlan, MACADDR const &Addr)
{
	if( m_pMutex->Wait(FOREVER) ) {

		if( FindVlanAddress(uVlan, Addr) == NOTHING ) {

			UINT uIndex = FindFreeAleSlot();

			if( uIndex < NOTHING ) {

				BYTE bType = 3;

				Reg(AleTable2) = (0 << 2);

				Reg(AleTable1) = (bType          << 28)
					       | (uVlan          << 16)
					       | (Addr.m_Addr[0] <<  8)
					       | (Addr.m_Addr[1]      );

				Reg(AleTable0) = (Addr.m_Addr[2] << 24)
					       | (Addr.m_Addr[3] << 16)
					       | (Addr.m_Addr[4] <<  8)
					       | (Addr.m_Addr[5]      );

				Reg(AleTableCtrl) = Bit(31) | uIndex;
				}
			}

		m_pMutex->Free();
		}
	}

UINT CDualEnet437::FindFreeAleSlot(void)
{
	for( UINT i = 0; i < ALE_SIZE; i++ ) {

		Reg(AleTableCtrl) = i;

		DWORD d[3];

		d[0] = Reg(AleTable0);
		d[1] = Reg(AleTable1);
		d[2] = Reg(AleTable2);

		UINT  tp = BitN(60, 3);

		if( !tp ) {

			return i;
			}
		}

	return NOTHING;
	}

UINT CDualEnet437::FindVlanEntry(UINT uPort, UINT uVlan)
{
	for( UINT i = 0; i < ALE_SIZE; i++ ) {

		Reg(AleTableCtrl) = i;

		DWORD d[3];

		d[0] = Reg(AleTable0);
		d[1] = Reg(AleTable1);
		d[2] = Reg(AleTable2);

		UINT  tp = BitN(60, 3);

		if( tp == 2 ) {

			UINT uEntryVlan = BitN(48, 12);

			if( uVlan == uEntryVlan ) {

				return i;
				}
			}
		}

	return NOTHING;
	}

UINT CDualEnet437::FindVlanAddress(UINT uVlan, MACADDR const &Addr)
{
	for( UINT i = 0; i < ALE_SIZE; i++ ) {

		Reg(AleTableCtrl) = i;

		DWORD d[3];

		d[0] = Reg(AleTable0);
		d[1] = Reg(AleTable1);
		d[2] = Reg(AleTable2);

		UINT  tp = BitN(60, 3);

		if( tp == 3 ) {

			UINT uEntryVlan = BitN(48, 12);

			MACADDR Test;

			Test.m_Addr[0] = BitN(40, 8);
			Test.m_Addr[1] = BitN(40, 8);
			Test.m_Addr[2] = BitN(40, 8);
			Test.m_Addr[3] = BitN(40, 8);
			Test.m_Addr[4] = BitN(40, 8);
			Test.m_Addr[5] = BitN(40, 8);

			if( uVlan == uEntryVlan && !memcmp(&Addr, &Test, 6) ) {

				return i;
				}
			}
		}

	return NOTHING;
	}

void CDualEnet437::KillAleSlot(UINT uIndex)
{
	Reg(AleTable0) = 0;
	Reg(AleTable1) = 0;
	Reg(AleTable2) = 0;

	Reg(AleTableCtrl) = Bit(31) | uIndex;
	}

// PHY Management

void CDualEnet437::SetPhy(UINT uPort, UINT uPhy)
{
	if( uPort == 1 ) {

		Reg(MdioPhySel0) = uPhy;
		}
	else {
		Reg(MdioPhySel1) = uPhy;
		}
	}

void CDualEnet437::EnablePhyPoll(UINT uPort, bool fEnable)
{
	if( fEnable ) {

		if( uPort == 1 ) {

			Reg(MdioPhySel0) |= Bit(6);
			}
		else {
			Reg(MdioPhySel1) |= Bit(6);
			}
		}
	else {
		if( uPort == 1 ) {

			Reg(MdioPhySel0) &= ~Bit(6);
			}
		else {
			Reg(MdioPhySel1) &= ~Bit(6);
			}
		}
	}

void CDualEnet437::SetMac(UINT uPort, MACADDR const &Mac)
{
	if( uPort == 1 ) {

		Reg(Port1SALo) = (Mac.m_Addr[0] <<  8) |
			         (Mac.m_Addr[1] <<  0) ;

		Reg(Port1SAHi) = (Mac.m_Addr[2] << 24) |
			         (Mac.m_Addr[3] << 16) |
			         (Mac.m_Addr[4] <<  8) |
			         (Mac.m_Addr[5] <<  0) ;
		}
	else {
		Reg(Port2SALo) = (Mac.m_Addr[0] <<  8) |
			         (Mac.m_Addr[1] <<  0) ;

		Reg(Port2SAHi) = (Mac.m_Addr[2] << 24) |
			         (Mac.m_Addr[3] << 16) |
			         (Mac.m_Addr[4] <<  8) |
			         (Mac.m_Addr[5] <<  0) ;
		}
	}

// MDIO Access

void CDualEnet437::PutMii(BYTE bPhy, BYTE bAddr, WORD wData)
{
	BYTE bGo    = 1;
	BYTE bWrite = 1;

	Reg(MdioAccess0) = ( (bGo    << 31)
			   | (bWrite << 30)
			   | (bAddr  << 21)
			   | (bPhy   << 16)
			   | (wData  <<  0)
			   );
	}

void CDualEnet437::GetMii(BYTE bPhy, BYTE bAddr)
{
	BYTE bGo    = 1;
	BYTE bWrite = 0;

	Reg(MdioAccess0) = ( (bGo    << 31)
			   | (bWrite << 30)
			   | (bAddr  << 21)
			   | (bPhy   << 16)
			   );
	}

WORD CDualEnet437::ReadMiiReg(void)
{
	return LOWORD(Reg(MdioAccess0));
	}

// Controller Management

void CDualEnet437::ResetController(void)
{
	ResetDma();

	ResetMac();

	ResetSwitch();
	}

void CDualEnet437::Close(void)
{
	if( m_pMutex->Wait(FOREVER) ) {

		if( m_uCount == 0 ) {

			ResetController();

			m_fInit = false;
			}

		m_pMutex->Free();
		}
	}

void CDualEnet437::ConfigController(UINT uPort, UINT uChan)
{
	if( m_pMutex->Wait(FOREVER) ) {

		if( !m_fInit ) {

			EnableEvents();

			for( UINT i = 0; i < 8; i ++ ) {

				RegN(StateRamTxHd, i) = 0;

				RegN(StateRamRxHd, i) = 0;

				RegN(StateRamTxCp, i) = 0;

				RegN(StateRamRxCp, i) = 0;
				}

			Reg(DmaIntEoiVect)   = intEoiRxPulse;

			Reg(DmaIntEoiVect)   = intEoiTxPulse;

			Reg(DmaIntEnSet)     = Bit(0) | Bit(1);

			Reg(SsStatsEn)	     = Bit(0) | Bit(1) | Bit(2);

			Reg(AlePrescale)     = m_uFreq / 1000;

			Reg(WrC0MiscEn)      = intMdioUser;

			Reg(MdioCtrl)        = mdioNoPreamble | (0xFF << mdioClockDiv);

			Reg(MdioUserIntSet)  = 0x03;

			Reg(MdioCtrl)       |= mdioEn;

			Reg(Port0DmaRxChMap) = 0x11110000;

			Reg(Port0TxInCtrl)   = 1 << 16;

			Reg(AleCtrl)         = aleTableClear;

			m_fInit              = true;
			}

		UINT uPos = uPort - 1;

		MACADDR Addr;

		if( uPort == 1 ) {

			UINT uVlan       = 1;

			Reg(Port1Vlan)   = uVlan;

			AddVlanEntry(uPort, uVlan);

			m_pNic[uPos]->ReadMac(Addr);

			AddVlanAddress(uVlan, Addr);

			Reg(Sl1RxMaxLen) = constBuffSize;
			}
		else {
			UINT uVlan       = 2;

			Reg(Port2Vlan)   = uVlan;

			AddVlanEntry(uPort, uVlan);

			m_pNic[uPos]->ReadMac(Addr);

			AddVlanAddress(uVlan, Addr);

			Reg(Sl2RxMaxLen) = constBuffSize;
			}

		Reg(DmaTxIntEnSet) |= (1 << uChan);

		Reg(DmaRxIntEnSet) |= (1 << uChan);

		Reg(WrC0TxEn) |= (1 << uChan);

		Reg(WrC0RxEn) |= (1 << uChan);

		m_pMutex->Free();
		}
	}

void CDualEnet437::StartController(UINT uPort, DWORD dwDesc, bool fFast, bool fFull)
{
	if( m_pMutex->Wait(FOREVER) ) {

		if( m_uCount == 0 ) {

			Reg(AleCtrl)      = aleEnable | aleVlanAware;

			Reg(AlePortCtrl0) = aleForward;
			}

		if( uPort == 1 ) {

			m_uCount++;

			Reg(Sl1Ctrl) = fFull ? Bit(0) : 0;

			if( fFast ) {

				Reg(Sl1Ctrl) |= Bit(15);
				}

			Reg(Sl1Ctrl)         |= Bit(5);

			Reg(AlePortCtrl1)     = aleForward;

			RegN(StateRamRxHd, 0) = dwDesc;
			}
		else {
			m_uCount++;

			Reg(Sl2Ctrl) = fFull ? Bit(0) : 0;

			if( fFast ) {

				Reg(Sl2Ctrl) |= Bit(15);
				}

			Reg(Sl2Ctrl) |= Bit(5);

			Reg(AlePortCtrl2) = aleForward;

			RegN(StateRamRxHd, 1) = dwDesc;
			}

		Reg(DmaRxCtrl) = Bit(0);

		Reg(DmaTxCtrl) = Bit(0);

		m_pMutex->Free();
		}
	}

void CDualEnet437::StopController(UINT uPort, UINT uChan)
{
	if( m_pMutex->Wait(FOREVER) ) {

		Reg(DmaTxTeardown) = uChan;

		Reg(DmaRxTeardown) = uChan;

		if( uPort == 1 ) {

			Reg(AlePortCtrl1)  = aleDisabled;

			m_uCount--;
			}

		if( uPort == 2 ) {

			Reg(AlePortCtrl2)  = aleDisabled;

			m_uCount--;
			}

		if( m_uCount == 0 ) {

			Reg(AlePortCtrl0)  = aleDisabled;

			Reg(DmaRxCtrl)    &= ~Bit(0);

			Reg(DmaTxCtrl)    &= ~Bit(0);
			}

		m_pMutex->Free();
		}
	}

// Rx Tx Buffers

void CDualEnet437::SetRamRxHd(UINT uChan, DWORD dwNext)
{
	RegN(StateRamRxHd, uChan) = dwNext;
	}

UINT CDualEnet437::GetRamRxHd(UINT uChan)
{
	return RegN(StateRamRxHd, uChan);
	}

void CDualEnet437::SetRamRxCp(UINT uChan)
{
	RegN(StateRamRxCp, uChan) = RegN(StateRamRxCp, uChan);
	}

void CDualEnet437::SetRamRxCp(UINT uChan, DWORD dwLast)
{
	RegN(StateRamRxCp, uChan) = dwLast;
	}

void CDualEnet437::SetRamTxHd(UINT uChan, DWORD dwNext)
{
	RegN(StateRamTxHd, uChan) = dwNext;
	}

UINT CDualEnet437::GetRamTxHd(UINT uChan)
{
	return RegN(StateRamTxHd, uChan);
	}

void CDualEnet437::SetRamTxCp(UINT uChan)
{
	RegN(StateRamTxCp, uChan) = RegN(StateRamTxCp, uChan);
	}

void CDualEnet437::SetDmaTxIntEn(UINT uChan)
{
	UINT ipr;

	HostRaiseIpr(ipr);

	Reg(DmaTxIntEnSet) = (1 << uChan);

	HostLowerIpr(ipr);
	}

void CDualEnet437::ClrDmaTxIntEn(UINT uChan)
{
	UINT ipr;

	HostRaiseIpr(ipr);

	Reg(DmaTxIntEnClr) = (1 << uChan);

	HostLowerIpr(ipr);
	}

// IDiagProvider

UINT CDualEnet437::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);

		case 2:
			return DiagAle(pOut, pCmd);
		}

	return 0;
	}

// IEventSink

void CDualEnet437::OnEvent(UINT uLine, UINT uParam)
{
	if( uLine == m_uLineRx ) {

		OnRecv();

		return;
		}

	if( uLine == m_uLineTx ) {

		OnSend();

		return;
		}

	if( uLine == m_uLineMs ) {

		OnMisc();

		return;
		}
	}

// Event Handlers

void CDualEnet437::OnRecv(void)
{
	for(;;) {

		DWORD Data = Reg(WrC0RxStat);

		if( Data ) {

			if( Data & Bit(0) ) {

				m_pNic[0]->OnRecv();
				}

			if( Data & Bit(1) ) {

				m_pNic[1]->OnRecv();
				}

			continue;
			}

		break;
		}

	Reg(DmaIntEoiVect) = intEoiRxPulse;
	}

void CDualEnet437::OnSend(void)
{
	for(;;) {

		DWORD Data = Reg(WrC0TxStat);

		if( Data ) {

			if( Data & Bit(0) ) {

				m_pNic[0]->OnSend();
				}

			if( Data & Bit(1) ) {

				m_pNic[1]->OnSend();
				}

			continue;
			}

		break;
		}

	Reg(DmaIntEoiVect) = intEoiTxPulse;
	}

void CDualEnet437::OnMisc(void)
{
	for(;;) {

		DWORD Data = Reg(WrC0MiscStat) & Reg(WrC0MiscEn);

		if( Data & intMdioUser ) {

			DWORD dwAccess = Reg(MdioAccess0);

			WORD wData = LOWORD(Reg(MdioAccess0));

			UINT uPort = (dwAccess & 0x000F0000) >> 16;

			m_pNic[uPort]->OnMiiEvent(wData);

			Reg(MdioUserIntRaw) = 0x3;

			continue;
			}

		if( Data & intMdioLink ) {

			DWORD dwLink = Reg(MdioLinkIntRaw);

			Reg(MdioLinkIntRaw) = dwLink;

			continue;
			}

		break;
		}

	Reg(DmaIntEoiVect) = intEoiMisc;
	}

// Implementation

void CDualEnet437::EnableEvents(void)
{
	phal->SetLineHandler(m_uLineRx, this, 0);
	phal->SetLineHandler(m_uLineTx, this, 0);
	phal->SetLineHandler(m_uLineMs, this, 0);

	phal->EnableLine(m_uLineRx, true);
	phal->EnableLine(m_uLineTx, true);
	phal->EnableLine(m_uLineMs, true);
	}

void CDualEnet437::DisableEvents(void)
{
	if( m_pMutex->Wait(FOREVER) ) {

		if( m_uCount == 0 ) {

			phal->EnableLine(m_uLineRx, false);
			phal->EnableLine(m_uLineTx, false);
			phal->EnableLine(m_uLineMs, false);
			}

		m_pMutex->Free();
		}
	}

// Mac Management

void CDualEnet437::ResetSwitch(void)
{
	Reg(WrReset) = Bit(0);

	while( Reg(WrReset) & Bit(0) );

	Reg(SsReset) = Bit(0);

	while( Reg(SsReset) & Bit(0) );
	}

void CDualEnet437::ResetDma(void)
{
	Reg(DmaReset) = Bit(0);

	while( Reg(DmaReset) & Bit(0) );
	}

void CDualEnet437::ResetMac(void)
{
	Reg(Sl1Reset) = Bit(0);

	Reg(Sl2Reset) = Bit(0);

	while( Reg(Sl1Reset) & Bit(0) );

	while( Reg(Sl2Reset) & Bit(0) );
	}

// Diagnostics

bool CDualEnet437::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "dualnic");

		pDiag->RegisterCommand(m_uProv, 1, "status");

		pDiag->RegisterCommand(m_uProv, 2, "ale");

		pDiag->Release();

		return true;
		}

	return false;
	}

bool CDualEnet437::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return true;
		}

	return false;
	}

UINT CDualEnet437::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		DumpNetStats(pOut);

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDualEnet437::DiagAle(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		DumpAle(pOut);

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

// Debug

void CDualEnet437::DumpNetStats(IDiagOutput *pOut)
{
	pOut->Print("NetRxGood      = %d\n", Reg(NetRxGood     ));
	pOut->Print("NetRxBroad     = %d\n", Reg(NetRxBroad    ));
	pOut->Print("NetRxMulti     = %d\n", Reg(NetRxMulti    ));
	pOut->Print("NetRxPause     = %d\n", Reg(NetRxPause    ));
	pOut->Print("NetRxCrc       = %d\n", Reg(NetRxCrc      ));
	pOut->Print("NetRxAlign     = %d\n", Reg(NetRxAlign    ));
	pOut->Print("NetRxOver      = %d\n", Reg(NetRxOver     ));
	pOut->Print("NetRxJabber    = %d\n", Reg(NetRxJabber   ));
	pOut->Print("NetRxShort     = %d\n", Reg(NetRxShort    ));
	pOut->Print("NetRxFrag      = %d\n", Reg(NetRxFrag     ));
	pOut->Print("NetRxOctets    = %d\n", Reg(NetRxOctets   ));
	pOut->Print("NetTxGood      = %d\n", Reg(NetTxGood     ));
	pOut->Print("NetTxBroad     = %d\n", Reg(NetTxBroad    ));
	pOut->Print("NetTxMulti     = %d\n", Reg(NetTxMulti    ));
	pOut->Print("NetTxPause     = %d\n", Reg(NetTxPause    ));
	pOut->Print("NetTxDefer     = %d\n", Reg(NetTxDefer    ));
	pOut->Print("NetCollision   = %d\n", Reg(NetCollisions ));
	pOut->Print("NetTxColSingle = %d\n", Reg(NetTxColSingle));
	pOut->Print("NetTxColMulte  = %d\n", Reg(NetTxColMulte ));
	pOut->Print("NetExcesCol    = %d\n", Reg(NetExcesCol   ));
	pOut->Print("NetLateCol     = %d\n", Reg(NetLateCol    ));
	pOut->Print("NetTxUnder     = %d\n", Reg(NetTxUnder    ));
	pOut->Print("NetCarrier     = %d\n", Reg(NetCarrier    ));
	pOut->Print("NetTxOctets    = %d\n", Reg(NetTxOctets   ));
	pOut->Print("NetRxTx        = %d\n", Reg(NetRxTx       ));
	pOut->Print("NetNetOctets   = %d\n", Reg(NetNetOctets  ));
	pOut->Print("NetRxSofOver   = %d\n", Reg(NetRxSofOver  ));
	pOut->Print("NetRxMofOver   = %d\n", Reg(NetRxMofOver  ));
	pOut->Print("NetRxDmaOver   = %d\n", Reg(NetRxDmaOver  ));
	}

void CDualEnet437::DumpAle(IDiagOutput *pOut)
{
	pOut->Print("Address Lookup Table\n");

	for( UINT i = 0; i < ALE_SIZE; i++ ) {

		Reg(AleTableCtrl) = i;

		DWORD d[3];

		d[0] = Reg(AleTable0);
		d[1] = Reg(AleTable1);
		d[2] = Reg(AleTable2);

		UINT  tp = BitN(60, 3);

		if( tp ) {

			pOut->Print("ALE[%4d] : %8.8X %8.8X %8.8X : Type(%d) ", i, d[2], d[1], d[0], tp);

			if( tp == 1 || tp == 3 ) {

				pOut->Print("MAC: %2.2X-", BitN(40, 0xFF));
				pOut->Print("%2.2X-",      BitN(32, 0xFF));
				pOut->Print("%2.2X-",      BitN(24, 0xFF));
				pOut->Print("%2.2X-",      BitN(16, 0xFF));
				pOut->Print("%2.2X-",      BitN(8, 0xFF));
				pOut->Print("%2.2X ",      BitN(0, 0xFF));

				if( BitN(40, 1) ) {

					pOut->Print("Multi=1 ");
					pOut->Print("Port Mask:%d: ", BitN(66, 0x07));
					pOut->Print("Super:%d ",      BitN(65, 0x01));
					pOut->Print("Forward:%d ",    BitN(62, 0x03));
					}
				else {
					pOut->Print("Multi=0 ");
					pOut->Print("DLR:%d ",   BitN(68, 0x01));
					pOut->Print("Port:%d: ", BitN(66, 0x03));
					pOut->Print("Block:%d ", BitN(65, 0x01));
					}
				}

			pOut->Print("\n");
			}
		}
	}

// End of File
