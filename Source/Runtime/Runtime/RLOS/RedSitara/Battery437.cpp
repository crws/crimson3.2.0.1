
#include "Intern.hpp"

#include "Battery437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Adc437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Battery Monitor
//

// Instantiator

IDevice * Create_BatteryMonitor437(CAdc437 *pAdc)
{
	CBattery437 *pDevice = New CBattery437(pAdc);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CBattery437::CBattery437(CAdc437 *pAdc)
{
	StdSetRef();

	m_uLevel  = 0;

	m_pAdc    = pAdc;

	m_uChan   = 1;

	m_uEnable = 30;

	AfxGetObject("gpio", 1, IGpio, m_pGpio);

	m_pTimer  = CreateTimer();
	
	SignalRead();
	}

// Destructor

CBattery437::~CBattery437(void)
{
	m_pTimer->Release();

	m_pGpio->Release();
	}

// IUnknown

HRESULT CBattery437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IBatteryMonitor);

	StdQueryInterface(IBatteryMonitor);

	return E_NOINTERFACE;
	}

ULONG CBattery437::AddRef(void)
{
	StdAddRef();
	}

ULONG CBattery437::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CBattery437::Open(void)
{
	m_pTimer->SetPeriod(constPeriod);

	m_pTimer->SetHook(this, 0);

	m_pTimer->Enable(true);

	SignalRead();

	return TRUE;
	}

// IBatteryMonitor

bool METHOD CBattery437::IsBatteryGood(void)
{
	CheckRead();

	return m_uLevel > constThreshold;
	}

bool METHOD CBattery437::IsBatteryBad(void)
{
	CheckRead();

	return m_uLevel <= constThreshold;
	}

// IEventSink

void CBattery437::OnEvent(UINT uLine, UINT uParam)
{
	SignalRead();
	}

// Implementation

void CBattery437::SignalRead(void)
{
	m_fReadReq = true;
	}

void CBattery437::CheckRead(void)
{
	if( m_fReadReq ) {

		ReadBattery();

		m_fReadReq = false;
		}
	}

void CBattery437::ReadBattery(void)
{	
	m_pGpio->SetState(m_uEnable, false);

	m_uLevel = m_pAdc->GetReading(m_uChan);

	m_pGpio->SetState(m_uEnable, true);
	}

// End of File
