
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Pwm437_HPP
	
#define	INCLUDE_AM437_Pwm437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CClock437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 Enhanced PWM Module
//

class CPwm437
{
	public:
		// Constructor
		CPwm437(UINT iIndex, CClock437 *pClock);

		// Operations
		UINT GetFreq(void);
		void SetFreq(UINT uFreq);
		void SetDuty(UINT uChan, UINT uDuty);

		// Channels
		enum
		{
			pwmChanA,
			pwmChanB
			};

	protected:
		// Regsiters
		enum 
		{
			regTBCTL	 = 0x0000 / sizeof(WORD),
			regTBSTS	 = 0x0002 / sizeof(WORD),
			regTBPHSHR	 = 0x0004 / sizeof(WORD),
			regTBPHS	 = 0x0006 / sizeof(WORD),
			regTBCNT	 = 0x0008 / sizeof(WORD),
			regTBPRD	 = 0x000A / sizeof(WORD),
			regCMPCTL	 = 0x000E / sizeof(WORD),
			regCMPAHR	 = 0x0010 / sizeof(WORD),
			regCMPA		 = 0x0012 / sizeof(WORD),
			regCMPB		 = 0x0014 / sizeof(WORD),
			regAQCTLA	 = 0x0016 / sizeof(WORD),
			regAQCTLB	 = 0x0018 / sizeof(WORD),
			regAQSFRC	 = 0x001A / sizeof(WORD),
			regAQCSFRC	 = 0x001C / sizeof(WORD),
			regDBCTL	 = 0x001E / sizeof(WORD),
			regDBRED	 = 0x0020 / sizeof(WORD),
			regDBFED	 = 0x0022 / sizeof(WORD),
			regTZSEL	 = 0x0024 / sizeof(WORD),
			regTZCTL	 = 0x0028 / sizeof(WORD),
			regTZEINT	 = 0x002A / sizeof(WORD),
			regTZFLG	 = 0x002C / sizeof(WORD),
			regTZCLR	 = 0x002E / sizeof(WORD),
			regTZFRC	 = 0x0030 / sizeof(WORD),
			regETSEL	 = 0x0032 / sizeof(WORD),
			regETPS		 = 0x0034 / sizeof(WORD),
			regETFLG	 = 0x0036 / sizeof(WORD),
			regETCLR	 = 0x0038 / sizeof(WORD),
			regETFRC	 = 0x003A / sizeof(WORD),
			regPCCTL	 = 0x003C / sizeof(WORD),
			regHRCTL	 = 0x00C0 / sizeof(WORD)
			};

		// Data Members
		PVWORD m_pBase;
		UINT   m_uClock;
		UINT   m_uFreq;
		UINT   m_uPeriod;

		// Implementation 
		void  Init(void);
		DWORD FindBase(UINT iIndex) const;
	};

#endif

// End of File
