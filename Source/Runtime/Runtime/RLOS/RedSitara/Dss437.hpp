#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Dss437_HPP
	
#define	INCLUDE_AM437_Dss437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CClock437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 Display Config
//

struct CDss437Config
{
	UINT m_uBits;
	UINT m_xDisp;
	UINT m_yDisp;
	UINT m_xSize;
	UINT m_ySize;
	BOOL m_fFixAspect;
	UINT m_uHorzFront;
	UINT m_uHorzSync; 
	UINT m_uHorzBack;
	UINT m_uVertFront;
	UINT m_uVertSync;
	UINT m_uVertBack;
	UINT m_uFrames;
	UINT m_uFrequency;
	BOOL m_fInvertPCLK;
	};

//////////////////////////////////////////////////////////////////////////
//
// AM437 Display Subsystem
//

class CDss437 : public IEventSink
{
	public:
		// Constructor
		CDss437(CClock437 *pClock);

		// Destructor
		~CDss437(void);

		// Configuration
		void Configure(CDss437Config const &Config);
		void SetFrame(PBYTE pFrameBuf);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Registers
		enum 
		{
			regREVISION		= 0x0000 / sizeof(DWORD),
			regSYSCFG		= 0x0010 / sizeof(DWORD),
			regSYSSTS		= 0x0014 / sizeof(DWORD),
			regIRQSTS		= 0x0018 / sizeof(DWORD),
			regIRQEN		= 0x001C / sizeof(DWORD),
			regCTRL			= 0x0040 / sizeof(DWORD),
			regCFG			= 0x0044 / sizeof(DWORD),
			regDEFAULT_COLOR_0	= 0x004C / sizeof(DWORD),
			regDEFAULT_COLOR_1	= 0x0050 / sizeof(DWORD),
			regTRANS_COLOR_0	= 0x0054 / sizeof(DWORD),
			regTRANS_COLOR_1	= 0x0058 / sizeof(DWORD),
			regLINE_STS		= 0x005C / sizeof(DWORD),
			regLINE_NUMBER		= 0x0060 / sizeof(DWORD),
			regTIMING_H		= 0x0064 / sizeof(DWORD),
			regTIMING_V		= 0x0068 / sizeof(DWORD),
			regPOL_FREQ		= 0x006C / sizeof(DWORD),
			regDIVISOR		= 0x0070 / sizeof(DWORD),
			regGLOBAL_ALPHA		= 0x0074 / sizeof(DWORD),
			regSIZE_DIG		= 0x0078 / sizeof(DWORD),
			regSIZE_LCD		= 0x007C / sizeof(DWORD),
			regGFX_BA_0		= 0x0080 / sizeof(DWORD),
			regGFX_BA_1		= 0x0084 / sizeof(DWORD),
			regGFX_POSITION		= 0x0088 / sizeof(DWORD),
			regGFX_SIZE		= 0x008C / sizeof(DWORD),
			regGFX_ATTRS		= 0x00A0 / sizeof(DWORD),
			regGFX_FIFO_THR		= 0x00A4 / sizeof(DWORD),
			regGFX_FIFO_SIZE_STS	= 0x00A8 / sizeof(DWORD),
			regGFX_ROW_INC		= 0x00AC / sizeof(DWORD),
			regGFX_PIXEL_INC	= 0x00B0 / sizeof(DWORD),
			regGFX_WINDOW_SKIP	= 0x00B4 / sizeof(DWORD),
			regGFX_TBL_BA		= 0x00B8 / sizeof(DWORD),
			regVID1_BA_0		= 0x00BC / sizeof(DWORD),
			regVID1_BA_1		= 0x00C0 / sizeof(DWORD),
			regVID1_POSITION	= 0x00C4 / sizeof(DWORD),
			regVID1_SIZE		= 0x00C8 / sizeof(DWORD),
			regVID1_ATTRS		= 0x00CC / sizeof(DWORD),
			regVID1_FIFO_THR	= 0x00D0 / sizeof(DWORD),
			regVID1_FIFO_SIZE_STS	= 0x00D4 / sizeof(DWORD),
			regVID1_ROW_INC		= 0x00D8 / sizeof(DWORD),
			regVID1_PIXEL_INC	= 0x00DC / sizeof(DWORD),
			regVID1_FIR		= 0x00E0 / sizeof(DWORD),
			regVID1_PICTURE_SIZE	= 0x00E4 / sizeof(DWORD),
			regVID1_ACCU_0		= 0x00E8 / sizeof(DWORD),
			regVID1_ACCU_1		= 0x00EC / sizeof(DWORD),
			regVID1_FIR_COEF_H_0	= 0x00F0 / sizeof(DWORD),
			regVID1_FIR_COEF_HV_0	= 0x00F4 / sizeof(DWORD),
			regVID1_FIR_COEF_H_1	= 0x00F8 / sizeof(DWORD),
			regVID1_FIR_COEF_HV_1	= 0x00FC / sizeof(DWORD),
			regVID1_FIR_COEF_H_2	= 0x0100 / sizeof(DWORD),
			regVID1_FIR_COEF_HV_2	= 0x0104 / sizeof(DWORD),
			regVID1_FIR_COEF_H_3	= 0x0108 / sizeof(DWORD),
			regVID1_FIR_COEF_HV_3	= 0x010C / sizeof(DWORD),
			regVID1_FIR_COEF_H_4	= 0x0110 / sizeof(DWORD),
			regVID1_FIR_COEF_HV_4	= 0x0114 / sizeof(DWORD),
			regVID1_FIR_COEF_H_5	= 0x0118 / sizeof(DWORD),
			regVID1_FIR_COEF_HV_5	= 0x011C / sizeof(DWORD),
			regVID1_FIR_COEF_H_6	= 0x0120 / sizeof(DWORD),
			regVID1_FIR_COEF_HV_6	= 0x0124 / sizeof(DWORD),
			regVID1_FIR_COEF_H_7	= 0x0128 / sizeof(DWORD),
			regVID1_FIR_COEF_HV_7	= 0x012C / sizeof(DWORD),
			regVID1_CONV_COEF0	= 0x0130 / sizeof(DWORD),
			regVID1_CONV_COEF1	= 0x0134 / sizeof(DWORD),
			regVID1_CONV_COEF2	= 0x0138 / sizeof(DWORD),
			regVID1_CONV_COEF3	= 0x013C / sizeof(DWORD),
			regVID1_CONV_COEF4	= 0x0140 / sizeof(DWORD),
			regVID2_BA_0 		= 0x014C / sizeof(DWORD),
			regVID2_BA_1		= 0x0150 / sizeof(DWORD),
			regVID2_POSITION	= 0x0154 / sizeof(DWORD),
			regVID2_SIZE		= 0x0158 / sizeof(DWORD),
			regVID2_ATTRS		= 0x015C / sizeof(DWORD),
			regVID2_FIFO_THR	= 0x0160 / sizeof(DWORD),
			regVID2_FIFO_SIZE_STS	= 0x0164 / sizeof(DWORD),
			regVID2_ROW_INC		= 0x0168 / sizeof(DWORD),
			regVID2_PIXEL_INC	= 0x016C / sizeof(DWORD),
			regVID2_FIR		= 0x0170 / sizeof(DWORD),
			regVID2_PICTURE_SIZE	= 0x0174 / sizeof(DWORD),
			regVID2_ACCU_0		= 0x0178 / sizeof(DWORD),
			regVID2_ACCU_1		= 0x017C / sizeof(DWORD),
			regVID2_FIR_COEF_H_0	= 0x0180 / sizeof(DWORD),
			regVID2_FIR_COEF_HV_0	= 0x0184 / sizeof(DWORD),
			regVID2_FIR_COEF_H_1	= 0x0188 / sizeof(DWORD),
			regVID2_FIR_COEF_HV_1	= 0x018C / sizeof(DWORD),
			regVID2_FIR_COEF_H_2	= 0x0190 / sizeof(DWORD),
			regVID2_FIR_COEF_HV_2	= 0x0194 / sizeof(DWORD),
			regVID2_FIR_COEF_H_3	= 0x0198 / sizeof(DWORD),
			regVID2_FIR_COEF_HV_3	= 0x019C / sizeof(DWORD),
			regVID2_FIR_COEF_H_4	= 0x01A0 / sizeof(DWORD),
			regVID2_FIR_COEF_HV_4	= 0x01A4 / sizeof(DWORD),
			regVID2_FIR_COEF_H_5	= 0x01A8 / sizeof(DWORD),
			regVID2_FIR_COEF_HV_5	= 0x01AC / sizeof(DWORD),
			regVID2_FIR_COEF_H_6	= 0x01B0 / sizeof(DWORD),
			regVID2_FIR_COEF_HV_6	= 0x01B4 / sizeof(DWORD),
			regVID2_FIR_COEF_H_7	= 0x01B8 / sizeof(DWORD),
			regVID2_FIR_COEF_HV_7	= 0x01BC / sizeof(DWORD),
			regVID2_CONV_COEF0	= 0x01C0 / sizeof(DWORD),
			regVID2_CONV_COEF1	= 0x01C4 / sizeof(DWORD),
			regVID2_CONV_COEF2	= 0x01C8 / sizeof(DWORD),
			regVID2_CONV_COEF3	= 0x01CC / sizeof(DWORD),
			regVID2_CONV_COEF4	= 0x01D0 / sizeof(DWORD),
			regDATA_CYCLE_0		= 0x01D4 / sizeof(DWORD),
			regDATA_CYCLE_1		= 0x01D8 / sizeof(DWORD),
			regDATA_CYCLE_2		= 0x01DC / sizeof(DWORD),
			regVID1_FIR_COEF_V_0	= 0x01E0 / sizeof(DWORD),
			regVID1_FIR_COEF_V_1	= 0x01E4 / sizeof(DWORD),
			regVID1_FIR_COEF_V_2	= 0x01E8 / sizeof(DWORD),
			regVID1_FIR_COEF_V_3	= 0x01EC / sizeof(DWORD),
			regVID1_FIR_COEF_V_4	= 0x01F0 / sizeof(DWORD),
			regVID1_FIR_COEF_V_5	= 0x01F4 / sizeof(DWORD),
			regVID1_FIR_COEF_V_6	= 0x01F8 / sizeof(DWORD),
			regVID1_FIR_COEF_V_7	= 0x01FC / sizeof(DWORD),
			regVID2_FIR_COEF_V_0	= 0x0200 / sizeof(DWORD),
			regVID2_FIR_COEF_V_7	= 0x021C / sizeof(DWORD),
			regCPR_COEF_R		= 0x0220 / sizeof(DWORD),
			regCPR_COEF_G		= 0x0224 / sizeof(DWORD),
			regCPR_COEF_B		= 0x0228 / sizeof(DWORD),
			regGFX_PRELOAD		= 0x022C / sizeof(DWORD),
			regVID1_PRELOAD		= 0x0230 / sizeof(DWORD),
			regVID2_PRELOAD		= 0x0234 / sizeof(DWORD)
			};

		// Data Members
		CDss437Config m_Display;
		PVDWORD       m_pBase;
		UINT          m_uClock;
		UINT          m_uLine;
		ITimer      * m_pTimer;
		DWORD         m_dwFrame;
		BOOL	      m_fError;
		UINT	      m_uSample1;
		UINT	      m_uFrames1;
		UINT	      m_uSample2;
		UINT	      m_uFrames2;

		// Implementation
		void Init(void);
		void SetVideo1(void);
		void SetDither(UINT uMode);
		void Reset(void);
		void Commit(void);
		void EnableEvents(void);
	};

// End of File

#endif
