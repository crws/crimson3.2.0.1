
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Pmic437_HPP
	
#define	INCLUDE_AM437_Pmic437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 Power Management Module
//

class CPmic437 
{
	public:
		// Constructor
		CPmic437(void);

		// Destructor
		~CPmic437(void);

		// Operations
		BYTE GetReg(BYTE bAddr);
		BOOL PutReg(BYTE bAddr, BYTE bData);
		UINT GetVoltage(UINT uRail);
		BOOL SetVoltage(UINT uRail, UINT uVolt);
		void Update(void);
		void BreakSeal(void);
		bool Lock(void);
		void Free(void);

		// Registers
		enum
		{
			regCHIPID	= 0x00,
			regINT1		= 0x01,
			regINT2		= 0x02,
			regINTMASK1	= 0x03,
			regINTMASK2	= 0x04,
			regSTATUS	= 0x05,
			regCONTROL	= 0x06,
			regFLAG		= 0x07,
			regPASSWORD	= 0x10,
			regENABLE1	= 0x11,
			regENABLE2	= 0x12,
			regCONFIG1	= 0x13,
			regCONFIG2	= 0x14,
			regCONFIG3	= 0x15,
			regDCDC1	= 0x16,
			regDCDC2	= 0x17,
			regDCDC3	= 0x18,
			regDCDC4	= 0x19,
			regSLEW		= 0x1A,
			regLDO1		= 0x1B,
			regSEQ1		= 0x20,
			regSEQ2		= 0x21,
			regSEQ3		= 0x22,
			regSEQ4		= 0x23,
			regSEQ5		= 0x24,
			regSEQ6		= 0x25,
			regSEQ7		= 0x26,
			};

	protected:
		// Static Data
		static UINT const v0[];
		static UINT const i1[];
		static UINT const i2[];
		static UINT const bp[];

		// Data
		II2c * m_pI2c;
		BYTE   m_bChip;
		
		// Implementation
		BOOL IsPasswordRequired(BYTE bReg) const;
		BOOL PutPassword(BYTE bReg);
	};

// End of File

#endif
