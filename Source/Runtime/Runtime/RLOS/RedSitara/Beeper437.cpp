
#include "Intern.hpp"

#include "Beeper437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Pwm437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Beeper
//

// Instantiator

IDevice * Create_Beeper437(CPwm437 *pPwm, UINT uChan)
{
	CBeeper437 *pDevice = New CBeeper437(pPwm, uChan);

	pDevice->Open();

	return pDevice;
	}

// Static Data

UINT CBeeper437::m_Table[] = {

	33488,
	35479,
	37589,
	39824,
	42192,
	44701,
	47359,
	50175,
	53159,
	56320,
	59669,
	63217
	};

// Constructor

CBeeper437::CBeeper437(CPwm437 *pPwm, UINT uChan)
{
	StdSetRef();

	m_pPwm   = pPwm;

	m_uChan  = uChan;

	m_uTime  = 0;

	m_pTimer = CreateTimer();
	}

// Destructor

CBeeper437::~CBeeper437(void)
{
	m_pTimer->Release();
	}

// IUnknown

HRESULT CBeeper437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IBeeper);

	return E_NOINTERFACE;
	}

ULONG CBeeper437::AddRef(void)
{
	StdAddRef();
	}

ULONG CBeeper437::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CBeeper437::Open(void)
{
	m_pTimer->SetHook((IEventSink *) this, 0);

	m_pTimer->SetPeriod(5);

	m_pTimer->Enable(true);

	return TRUE;
	}

// IBeeper

void METHOD CBeeper437::Beep(UINT uBeep)
{
	switch( uBeep ) {

		case beepStart:

			Beep(72, 200);

			return;

		case beepPress:

			Beep(72, 50);

			return;
		
		case beepFull:

			Beep(48, 500);

			return;
		}

	BeepOff();
	}

void CBeeper437::Beep(UINT uNote, UINT uTime)
{
	UINT ipr;

	HostRaiseIpr(ipr);

	if( (m_uTime = ToTicks(uTime)) ) {

		UINT uFreq = m_Table[uNote % 12] >> (12-(uNote / 12));

		m_uSave = m_pPwm->GetFreq();

		m_pPwm->SetFreq(uFreq);

		m_pPwm->SetDuty(m_uChan, 50);
		}
	else {
		BeepOff();
		}

	HostLowerIpr(ipr);
	}

void METHOD CBeeper437::BeepOff(void)
{
	m_pPwm->SetDuty(m_uChan, 0);

	m_pPwm->SetFreq(m_uSave);
	}

// IEventSink

void CBeeper437::OnEvent(UINT uLine, UINT uParam)
{
	if( m_uTime && !--m_uTime ) {

		BeepOff();
		}
	}

// End of File
