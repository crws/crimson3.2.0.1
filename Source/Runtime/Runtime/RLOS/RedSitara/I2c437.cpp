
#include "Intern.hpp"

#include "I2c437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 I2C Controller Module
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Instantiator

IDevice * Create_I2cInterface437(CClock437 *pClock)
{
	CI2c437 *pDevice = New CI2c437(pClock);

	pDevice->Open();

	return (IDevice *)(II2c *) pDevice;
	}

// Constructor

CI2c437::CI2c437(CClock437 *pClock)
{
	StdSetRef();

	m_pBase  = PVDWORD(ADDR_I2C0);

	m_uLine  = INT_I2C0;

	m_uFreq  = pClock->GetPerM2Freq() / 4;

	m_uUnit  = 0;

	m_pLock  = Create_Mutex();

	m_pDone  = Create_AutoEvent();
	}

// Destructor

CI2c437::~CI2c437(void)
{
	m_pLock->Release();

	m_pDone->Release();
	}

// IUnknown

HRESULT CI2c437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(II2c);

	return E_NOINTERFACE;
	}

ULONG CI2c437::AddRef(void)
{
	StdAddRef();
	}

ULONG CI2c437::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL CI2c437::Open(void)
{
	InitData();
	
	InitController();

	InitEvents();
	
	return TRUE;
	}

// II2c

BOOL METHOD CI2c437::Lock(UINT uTime)
{
	if( CanWait() ) {

		return m_pLock->Wait(uTime);
		}

	return !m_fBusy;
	}

void METHOD CI2c437::Free(void)
{
	if( CanWait() ) {

		m_pLock->Free();
		}
	}

BOOL METHOD CI2c437::Send(BYTE bChip, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData)
{
	return Transfer(bChip, pHead, uHead, pData, uData, true);
	}

BOOL METHOD CI2c437::Recv(BYTE bChip, PCBYTE pHead, UINT uHead, PBYTE  pData, UINT uData)
{
	return Transfer(bChip, pHead, uHead, pData, uData, false);
	}

// IEventSink

void CI2c437::OnEvent(UINT uLine, UINT uParam)
{
	for(;;) {

		DWORD Data = Reg(IRQSTS) & Reg(IRQEN);

		if( Data & intTxFifo ) {

			OnTxFifo();
		
			Reg(IRQSTS) = intTxFifo;

			continue;
			}

		if( Data & intRxFifo ) {

			OnRxFifo();

			Reg(IRQSTS) = intRxFifo;

			continue;
			}

		if( Data & intTxRdy ) {

			Reg(IRQSTS) = intTxRdy;

			OnTxData();

			continue;
			}

		if( Data & intRxRdy ) {

			Reg(IRQSTS) = intRxRdy;

			OnRxData();

			continue;
			}

		if( Data & intNoAck ) {

			Reg(IRQSTS) = intNoAck;
			
			OnNoAck();

			continue;
			}

		if( Data & intRxOvr ) {

			Reg(IRQSTS) = intRxOvr;

			AfxTrace("CI2c437 - Rx Overrun.\n");

			continue;
			}

		if( Data & intArbLost ) {

			Reg(IRQSTS) = intArbLost;

			AfxTrace("CI2c437 - Arbitration Lost.\n");

			continue;
			}

		if( Data & intRdy ) {

			Reg(IRQSTS) = intRdy;

			OnReady();

			continue;
			}

		break;
		}
	}

// Event Handlers

void CI2c437::OnTxFifo(void)
{
	UINT uThreshold = (Reg(BUF) & 0x3F) + 1;

	UINT uFifoDepth = GetFifoDepth(); 

	UINT uFifoSpace = uFifoDepth - uThreshold;

	if( m_uState == stateHead ) {

		m_uPtr += WriteFifo(m_pHead + m_uPtr, Min(uFifoSpace, m_uHead - m_uPtr));

		if( m_uPtr == m_uHead && m_fSend ) {

			m_uState = stateData;

			m_uPtr   = 0;
			}
		
		return;
		}

	if( m_uState == stateData ) {

		m_uPtr += WriteFifo(m_pData + m_uPtr, Min(uFifoSpace, m_uData - m_uPtr));

		return;
		}
	}

void CI2c437::OnTxData(void)
{
	UINT uCount = (Reg(BUFSTAT) & 0x3F) + 1;

	if( m_uState == stateHead ) {

		m_uPtr += WriteFifo(m_pHead + m_uPtr, Min(uCount, m_uHead - m_uPtr));

		if( m_uPtr == m_uHead && m_fSend ) {

			m_uState = stateData;

			m_uPtr   = 0;
			}
		
		return;
		}

	if( m_uState == stateData ) {

		m_uPtr += WriteFifo(m_pData + m_uPtr, Min(uCount, m_uData - m_uPtr));

		return;
		}
	}

void CI2c437::OnRxData(void)
{
	UINT uCount = (Reg(BUFSTAT) >> 8) & 0x3F;

	m_uPtr += ReadFifo(m_pData + m_uPtr, uCount);
	}

void CI2c437::OnRxFifo(void)
{
	UINT uCount = ((Reg(BUF) >> 8) & 0x3F) + 1;

	m_uPtr += ReadFifo(m_pData + m_uPtr, uCount);
	}

void CI2c437::OnNoAck(void)
{
	m_fOkay = false;
	}

void CI2c437::OnReady(void)
{
	if( !m_fOkay || m_uState == stateData ) {

		m_uState = stateIdle;

		m_fBusy = false;

		m_pDone->Set();
		
		return;
		}

	if( m_uState == stateHead ) {

		m_uState = stateData;

		if( m_fSend ) {

			Reg(CTRL) |= ctrlTxMode;

			Reg(CNT)   = m_uData;
	
			m_uPtr     = WriteFifo(m_pData, Min(m_uData, GetFifoDepth()));
			}
		else {
			Reg(CTRL) &= ~ctrlTxMode;

			Reg(CNT)   = m_uData;

			m_uPtr     = 0;
			}

		Reg(CTRL) |= ctrlStartStop;

		return;
		}
	}

// Implementation

bool CI2c437::Transfer(BYTE bChip, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData, bool fSend)
{
	TestWait();

	m_pHead     = pHead;
			    
	m_uHead     = uHead;
			    
	m_pData     = PBYTE(pData);
			    
	m_uData     = uData;
			    
	m_fSend     = fSend;
			    
	m_uState    = stateStart;

	m_fOkay     = true;

	m_uPtr      = 0;

	Reg(IRQCLR) = intTxFifo | intRxFifo | intTxRdy | intRxRdy | intRdy;
		
	Reg(SA)     = bChip >> 1;

	Reg(BUF)   |= (Bit(14) | Bit(6));
		    
	Reg(CNT)    = fSend ? uHead + uData : uHead ? uHead : uData;

	Reg(CTRL)  |= ctrlMaster;

	if( uHead || fSend ) {

		Reg(CTRL) |= ctrlTxMode;

		if( uHead ) {
				
			m_uState = stateHead;

			m_uPtr   = WriteFifo(m_pHead, Min(m_uHead, GetFifoDepth()));
			}

		if( fSend && m_uPtr < GetFifoDepth() ) {

			m_uState = stateData;

			m_uPtr   = WriteFifo(m_pData, Min(m_uData, GetFifoDepth() - m_uPtr));
			}
		}
	else {
		Reg(CTRL) &= ~ctrlTxMode;
		
		m_uState   = stateData;
		}

	Reg(IRQSTS) = intAll;

	if( !uHead || !uData || fSend ) {

		Reg(CTRL) |= ctrlStartStop;
		}
	else {
		Reg(CTRL) |= ctrlStart;
		}
		
	Reg(IRQEN) = intTxFifo | intRxFifo | intTxRdy | intRxRdy | intRdy;
	
	WaitDone();

	return m_fOkay;
	}

void CI2c437::InitData(void)
{
	m_fBusy  = false;

	m_fOkay  = false;
	
	m_uState = stateIdle;
	}

void CI2c437::InitController(void)
{
	Reg(CTRL)  &= ~ctrlEnable;
	
	Reset();
	
	DWORD Freq  = 24000000;
		    
	DWORD Bus   = 100000;
		    
	DWORD Pre   = m_uFreq / Freq;
		    
	DWORD Div   = Freq / (Bus * 2);

	Reg(PSC)    = Pre - 1;
		    
	Reg(SCLL)   = Div - 7;
		    
	Reg(SCLH)   = Div - 5;
		    
	Reg(BUF)    = (((GetFifoDepth() / 4) - 1) << 8) | ((GetFifoDepth() / 2) - 1);

	Reg(IRQCLR) = intAll;

	Reg(IRQEN)  = intTxFifo | intRxFifo | intTxRdy | intRxRdy | intNoAck | intRxOvr | intArbLost | intRdy;

	Reg(CTRL)  |= ctrlEnable;
	}

void CI2c437::InitEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);
	}

void CI2c437::Reset(void)
{
	Reg(SYSCFG) |= Bit(1);

	while( Reg(SYSCFG) & Bit(1) );
	}

UINT CI2c437::WriteFifo(PCBYTE pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n ++ ) {

		Reg(DATA) = pData[n];
		}

	return uCount;
	}

UINT CI2c437::ReadFifo(PBYTE pData, UINT uCount)
{
	for( UINT n = 0; n < uCount; n ++ ) {

		pData[n] = Reg(DATA);
		}

	return uCount;
	}

UINT CI2c437::GetFifoDepth(void)
{
	DWORD dwDepth = (Reg(BUFSTAT) >> 14) & 3; 
	
	return 8 << dwDepth;
	}

bool CI2c437::CanWait(void)
{
	if( Hal_GetIrql() >= IRQL_DISPATCH ) {

		return false;
		}

	if( !GetCurrentThread() ) {

		return false;
		}

	if( GetCurrentThread()->GetIndex() <= 1 ) {

		return false;
		}

	return true;
	}

void CI2c437::TestWait(void)
{
	m_fBusy  = true;

	m_fWait  = CanWait();

	m_uState = stateIdle;

	phal->EnableLine(m_uLine, m_fWait);
	}

void CI2c437::WaitDone(void)
{
	if( !m_fWait ) {

		while( !m_pDone->Wait(0) ) {

			while( !(Reg(IRQSTS) & Reg(IRQEN)) );

			OnEvent(0, 0);
			}

		return;
		}

	m_pDone->Wait(FOREVER);
	}

// End of File
