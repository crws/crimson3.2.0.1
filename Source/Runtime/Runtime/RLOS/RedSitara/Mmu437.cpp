
#include "Intern.hpp"

#include "Mmu437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 Memory Management Unit
//

// Instantiator

IMmu * Create_Mmu437(IHal *pHal)
{
	return New CMmu437;
	}

// Constructor

CMmu437::CMmu437(void)
{
	DWORD base = 0;

	#if !defined(__INTELLISENSE__)

	ASM(	"mrc p15, 0, %0, c2, c0, 0\n\t"
		: "=r"(base)
		);

	#endif

	m_pTable = PDWORD(base);

	FindGranularity();
	}

// IMmu

bool CMmu437::IsVirtualValid(DWORD dwAddr, bool fWrite)
{
	if( dwAddr ) {

		switch( VirtualToPhysical(dwAddr) & 0xF0000000 ) {

			case ADDR_DDR1:
			case ADDR_DDR2:

				// Only the DRAM is valid for this test.

				return true;
			}
		}

	return false;
	}

DWORD CMmu437::PhysicalToVirtual(DWORD dwAddr, bool fCached)
{
	for( UINT n = 0; n < 4096; n += m_uStep ) {

		DWORD dwEntry = m_pTable[n];

		if( IsEntryValid(dwEntry) ) {

			if( (dwAddr & m_dwMask) == (dwEntry & m_dwMask) ) {

				if( IsEntryCached(dwEntry) == fCached ) {

					return (n << 20) | (dwAddr & ~m_dwMask);
					}
				}
			}
		}

	HostTrap(PANIC_DEVICE_MMU);

	return NOTHING;
	}

DWORD CMmu437::VirtualToPhysical(DWORD dwAddr)
{
	return (m_pTable[dwAddr >> 20] & 0xFFF00000) | (dwAddr & 0x000FFFFF);
	}

void CMmu437::DebugRegister(void)
{
	}

void CMmu437::DebugRevoke(void)
{
	}

// Implementation

bool CMmu437::FindGranularity(void)
{
	// This is a bit hairy but it looks at the MMU table and figures
	// out how often we have to check entries when converting from a
	// physical address to a virtual address. Without this, we might
	// have to look at 4096 entries, but using this trick we can get
	// the count down to 16 on a typical iMX51 system.

	UINT   uCount  = 4096;

	UINT64 uSpace  = 0x100000000LL;

	UINT   uEach   = UINT(uSpace / uCount);

	DWORD  dwLast  = NOTHING;

	UINT   uVirt   = 0;

	UINT   uPhys   = (m_pTable[0] & 0xFFF00000);

	UINT   uBlock  = 1;

	UINT   uLimit  = 0x80000000;

	for( UINT n = 0; n < uCount; n++ ) {

		DWORD dwEntry = m_pTable[n];

		if( n ) {

			// We basically look for runs of adjacent entries, and then
			// when the run ends, do some GCD math on the size and the base
			// addresses to figure out how many subdivisions we can use.

			if( (dwEntry & 0xFFF00000) == ((dwLast & 0xFFF00000) + 0x00100000) ) {

				uBlock++;

				if( n < uCount - 1 ) {

					dwLast = dwEntry;

					continue;
					}
				}

			UINT a = Gcd64(uSpace + uVirt, uSpace + uPhys) / uEach;

			uLimit = Gcd32(Gcd32(uLimit, a), uBlock);

			uBlock = 1;

			uVirt  = n << 20;

			uPhys  = (dwEntry & 0xFFF00000);
			}

		dwLast = dwEntry;
		}

	// Now we've figured out the subdivision size, we can
	// convert this into the mask that we use for testing.

	m_uStep  = uLimit;

	m_dwMask = 0xFFF00000;

	while( (uLimit >>= 1) ) {

		m_dwMask <<= 1;
		}

	AfxTrace("MMU mask is 0x%8.8X\n", m_dwMask);

	return true;
	}

UINT CMmu437::Gcd32(UINT a, UINT b)
{
	// Greatest Common Divisor by...
	//
	// https://en.wikipedia.org/wiki/Euclidean_algorithm

	return UINT(b ? Gcd32(b, a % b) : a);
	}

UINT CMmu437::Gcd64(UINT64 a, UINT64 b)
{
	// Greatest Common Divisor by...
	//
	// https://en.wikipedia.org/wiki/Euclidean_algorithm

	return UINT(b ? Gcd64(b, a % b) : a);
	}

// End of File
