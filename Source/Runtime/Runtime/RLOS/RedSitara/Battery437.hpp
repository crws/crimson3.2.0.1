
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Battery437_HPP
	
#define	INCLUDE_AM437_Battery437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Interfaces
//

#include "../../StdEnv/IRtc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CAdc437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 Battery Monitor
//

class CBattery437 : public IBatteryMonitor, public IEventSink
{
	public:
		// Constructor
		CBattery437(CAdc437 *pAdc);

		// Destructor
		virtual ~CBattery437(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IBatteryMonitor
		bool METHOD IsBatteryGood(void);
		bool METHOD IsBatteryBad(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Constants
		static UINT const constThreshold = 2250;
		static UINT const constPeriod	 = 7200000;

		// Data
		ULONG      m_uRefs;
		CAdc437  * m_pAdc;
		ITimer   * m_pTimer;
		UINT       m_uLevel;
		IGpio    * m_pGpio;
		UINT       m_uEnable;
		UINT       m_uChan;
		bool       m_fReadReq;

		// Implementation
		void SignalRead(void);
		void CheckRead(void);
		void ReadBattery(void);
	};

// End of File

#endif
