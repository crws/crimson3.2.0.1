
#include "Intern.hpp"

#include "Dmt437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ctrl437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 DM Timer Module
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CDmt437::CDmt437(CCtrl437 *pCtrl, UINT iIndex)
{
	m_pBase   = PVDWORD(FindBase(iIndex));

	m_uLine   = FindLine(iIndex);

	m_uClock  = pCtrl->GetRefFreq(); 

	m_pSink   = NULL;

	Reset();
	}

// IEventSink

void CDmt437::OnEvent(UINT uLine, UINT uParam)
{
	Reg(IRQSTS) = intOverflow;

	if( m_pSink ) {

		m_pSink->OnEvent(uLine, m_uParam);
		}
	}

// Attributes

UINT CDmt437::GetFraction(void)
{
	UINT uRead = Reg(TCRR);

	UINT uTime = (uRead - m_uLoad) * (m_uPeriod * 1000) / m_uLoad;

	return uTime;
	}

// Operations

void CDmt437::Wait(UINT uPeriod)
{
	UINT uLoad = FindLoadValue(uPeriod);

	for( ;;) {

		if( Reg(IRQSTS) & intOverflow ) {

			Reg(IRQSTS) = intOverflow;

			break;
			}

		if( Reg(TCRR) > uLoad ) {

			break;
			}
		}
	}

void CDmt437::Delay(UINT uPeriod)
{
	if( Reg(TCLR) & bitPeriodic ) {

		UINT uCount = 1 + uPeriod / m_uPeriod;

		Reg(IRQENCLR) = intOverflow;

		Reg(IRQSTS)   = intOverflow;

		while( uCount ) {

			if( Reg(IRQRAW) & intOverflow ) {

				uCount--;

				Reg(IRQSTS) = intOverflow;
				}
			}

		Reg(IRQENSET) = intOverflow;
		}
	else {
		while( Reg(TCLR) & bitStart );

		Reset();

		Reg(TMAR) = FindLoadValue(uPeriod);

		WaitWrite();

		Reg(TCLR) |= bitCompare;

		WaitWrite();

		Reg(IRQSTS) = intCompare;

		Reg(TCLR) |= bitStart;

		WaitWrite();

		while( !(Reg(IRQRAW) & intCompare) );

		Reg(IRQSTS) = intCompare;

		Reg(TCLR)  &= ~bitStart;

		WaitWrite();
		}
	}

void CDmt437::SetEventHandler(IEventSink *pSink, UINT uParam)
{
	m_pSink  = pSink;

	m_uParam = uParam;

	EnableEvents();
	}

void CDmt437::SetPeriodic(UINT uPeriod)
{
	m_uPeriod = uPeriod;

	m_uLoad   = FindLoadValue(uPeriod);

	Reset();

	Reg(TLDR) = 0xFFFFFFFF - m_uLoad;

	WaitWrite();

	Reg(TTGR) = 0xFFFFFFFF;

	WaitWrite();
	
	Reg(TCLR) |= (bitPeriodic | bitStart);

	WaitWrite();

	Reg(IRQENSET) = intOverflow;
	}

void CDmt437::SetSingleShot(UINT uPeriod)
{
	m_uPeriod = uPeriod;

	m_uLoad   = FindLoadValue(uPeriod);

	Reset();

	Reg(TLDR) = 0xFFFFFFFF - m_uLoad;

	WaitWrite();

	Reg(TTGR) = 0xFFFFFFFF;

	WaitWrite();

	Reg(TCLR) |= bitStart;

	WaitWrite();

	Reg(IRQENSET) = intOverflow;
	}

void CDmt437::Start(void)
{
	Reset();

	Reg(TCLR) |= bitStart;

	WaitWrite();
	}

void CDmt437::Stop(void)
{
	Reg(IRQENCLR) = intOverflow;

	Reg(TCLR)    &= ~bitStart;

	WaitWrite();
	}

void CDmt437::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);
	}

// Implementation

void CDmt437::Reset(void)
{
	Reg(CFG) |= bitReset;

	while( Reg(CFG) & bitReset );
	}

void CDmt437::WaitWrite(void)
{
	while( Reg(TWPS) );
	}

UINT CDmt437::FindLoadValue(UINT uPeriod)
{
	return (m_uClock / 1000000) * uPeriod + 1;
	}

DWORD CDmt437::FindBase(UINT iIndex)
{
	switch( iIndex ) {

		case 0: return ADDR_DMTIMER2;
		case 1: return ADDR_DMTIMER3;
		case 2: return ADDR_DMTIMER4;
		case 3: return ADDR_DMTIMER5;
		}

	return 0;
	}

UINT CDmt437::FindLine(UINT iIndex)
{
	switch( iIndex ) {

		case 0: return INT_TIMER2;
		case 1: return INT_TIMER3;
		case 2: return INT_TIMER4;
		case 3: return INT_TIMER5;
		}

	return 0;
	}

// End of File
