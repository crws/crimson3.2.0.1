
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Pru437_HPP
	
#define	INCLUDE_AM437_Pru437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 Programmable Real-Time Unit
//

class CPru437
{
	public:
		// Constructor
		CPru437(UINT iIndex, UINT uUnit);

		// Attributes
		bool IsHalted(UINT uPru) const;
		bool IsRunning(UINT uPru) const;
		
		// Operations
		void SetMux(UINT uMux);
		void Run(UINT uPru);
		void Stop(UINT uPru);
		void Reset(UINT uPru);
		bool LoadData(UINT uPru, PCBYTE pData, UINT uCount);
		bool LoadData(UINT uPru, PCBYTE pData, UINT uCount, UINT uAddr);
		bool LoadCode(UINT uPru, PCBYTE pCode, UINT uCount);
		bool LoadCode(UINT uPru, PCBYTE pCode, UINT uCount, UINT uAddr);
		bool LoadCode(UINT uPru, PCTXT pCode);

	protected:
		// Global Memory Map
		enum
		{
			memRAM0		= 0x00000,
			memRAM1		= 0x02000,
			memRAM2		= 0x10000,
			memINTC		= 0x20000,
			memPRU0CTRL	= 0x22000,
			memPRU0DEBUG	= 0x22400,
			memPRU1CTRL	= 0x24000,
			memPRU1DEBUG	= 0x24400,
			memCFG		= 0x26000,
			memUART		= 0x28000,
			memIEP		= 0x2E000,
			memECAP		= 0x30000,
			memPRU0IRAM	= 0x34000,
			memPRU1IRAM	= 0x38000,
			};

		// Control Registers
		enum
		{
			regCTRL		= 0x0000 / sizeof(DWORD),
			regSTS		= 0x0004 / sizeof(DWORD),
			regWAKEU	= 0x0008 / sizeof(DWORD),
			regCYCLE	= 0x000C / sizeof(DWORD),
			regSTALL	= 0x0010 / sizeof(DWORD),
			regCTBIR0	= 0x0020 / sizeof(DWORD),
			regCTBIR1	= 0x0024 / sizeof(DWORD),
			regCTPPR0	= 0x0028 / sizeof(DWORD),
			regCTPPR1	= 0x002C / sizeof(DWORD),
			};

		// Debug Registers
		enum
		{
			regGPREGx	= 0x0000 / sizeof(DWORD),
			regCTREGx	= 0x0080 / sizeof(DWORD),
			};

		// Interrupt Regsiters
		enum
		{
			regINTID	= 0x0000 / sizeof(DWORD),
			regCR		= 0x0004 / sizeof(DWORD),
			regGER		= 0x0010 / sizeof(DWORD),
			regGNLR		= 0x001C / sizeof(DWORD),
			regSISR		= 0x0020 / sizeof(DWORD),
			regSICR		= 0x0024 / sizeof(DWORD),
			regEISR		= 0x0028 / sizeof(DWORD),
			regEICR		= 0x002C / sizeof(DWORD),
			regHIEISR	= 0x0034 / sizeof(DWORD),
			regHIDISR	= 0x0038 / sizeof(DWORD),
			regGPIR		= 0x0080 / sizeof(DWORD),
			regSRSR0	= 0x0200 / sizeof(DWORD),
			regSRSR1	= 0x0204 / sizeof(DWORD),
			regSECR0	= 0x0280 / sizeof(DWORD),
			regSECR1	= 0x0284 / sizeof(DWORD),
			regESR0		= 0x0300 / sizeof(DWORD),
			regERS1		= 0x0304 / sizeof(DWORD),
			regECR0		= 0x0380 / sizeof(DWORD),
			regECR1		= 0x0384 / sizeof(DWORD),
			regCMRx		= 0x0400 / sizeof(DWORD),
			regHMR0		= 0x0800 / sizeof(DWORD),
			regHMR1		= 0x0804 / sizeof(DWORD),
			regHMR2		= 0x0808 / sizeof(DWORD),
			regHIPIRx	= 0x0900 / sizeof(DWORD),
			regSIPR0	= 0x0D00 / sizeof(DWORD),
			regSIPR1	= 0x0D04 / sizeof(DWORD),
			regSITR0	= 0x0D80 / sizeof(DWORD),
			regSITR1	= 0x0D84 / sizeof(DWORD),
			regHINLRx	= 0x1100 / sizeof(DWORD),
			regHIER		= 0x1500 / sizeof(DWORD),
			};

		// Config Registers
		enum
		{
			regHWREV	= 0x0000 / sizeof(DWORD),
			regSYSCFG	= 0x0004 / sizeof(DWORD), 
			regGPCFG0	= 0x0008 / sizeof(DWORD), 
			regGPCFG1	= 0x000C / sizeof(DWORD), 
			regCGR		= 0x0010 / sizeof(DWORD), 	
			regISRP		= 0x0014 / sizeof(DWORD), 	
			regISP		= 0x0018 / sizeof(DWORD), 	
			regIESP		= 0x001C / sizeof(DWORD), 	
			regIECP		= 0x0020 / sizeof(DWORD), 	
			regPMAO		= 0x0028 / sizeof(DWORD), 	
			regIEPCLK	= 0x0030 / sizeof(DWORD), 
			regSPP		= 0x0034 / sizeof(DWORD), 	
			regPINMUX	= 0x0040 / sizeof(DWORD), 	
			regSDP0CLKSELx	= 0x0048 / sizeof(DWORD), 
			regSDP0SSx	= 0x004C / sizeof(DWORD), 
			regSDP1CLKSELx	= 0x0094 / sizeof(DWORD), 
			regSDP1SSx	= 0x0098 / sizeof(DWORD), 
			regEDP0RXCFG	= 0x00E0 / sizeof(DWORD), 
			regEDP0TXCFG	= 0x00E4 / sizeof(DWORD), 
			regEDP0CFG00	= 0x00E8 / sizeof(DWORD), 
			regEDP0CFG10	= 0x00EC / sizeof(DWORD), 
			regEDP0CFG01	= 0x00F0 / sizeof(DWORD), 
			regEDP0CFG11	= 0x00F4 / sizeof(DWORD), 
			regEDP0CFG02	= 0x00F8 / sizeof(DWORD), 
			regEDP0CFG12	= 0x00FC / sizeof(DWORD), 
			regEDP1RXCFG	= 0x0100 / sizeof(DWORD), 
			regEDP1TXCFG	= 0x0104 / sizeof(DWORD), 
			regEDP1CFG00	= 0x0108 / sizeof(DWORD), 
			regEDP1CFG10	= 0x010C / sizeof(DWORD), 
			regEDP1CFG01	= 0x0110 / sizeof(DWORD), 
			regEDP1CFG11	= 0x0114 / sizeof(DWORD), 
			regEDP1CFG02	= 0x0118 / sizeof(DWORD), 
			regEDP1CFG12	= 0x011C / sizeof(DWORD), 
			};

		// Data
		PVDWORD m_pBase;
		PVDWORD m_pIntCtrl;
		PVDWORD m_pConfig;
		PVDWORD m_pControl[2];
		PVDWORD m_pDebug[2];
		PVDWORD m_pCode[2];
		PVDWORD m_pData[2];
		UINT    m_uUnit;
		UINT    m_uPos;

		// Implementation
		UINT FindBase(UINT iIndex) const;
	};

// End of File

#endif
