
#include "Intern.hpp"

#include "PlatformCanyonCmos.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

#include "Ctrl437.hpp"

#include "Pwm437.hpp"

#include "Pru437.hpp"

#include "Pcm437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Pru Code  
//

#include "Pru/Canyon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice * Create_Beeper437  (CPwm437 *pPwm, UINT uChan);
extern IDevice * Create_Uart437	   (UINT iIndex, CClock437 *pClock, CPru437 *pPru);
extern IDevice * Create_Display437 (UINT uModel, CDss437 *pDss, CPwm437 *pPwm, UINT uChan);
extern IDevice * Create_Touch437   (BOOL fFlipX, BOOL fFlipY, UINT uMaxVar, UINT uMinVal);
extern IDevice * Create_Enet437	   (CClock437 *pClock);
	
//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Canyon HMI
//

// Instantiator

IDevice * Create_PlatformCanyonCmos(UINT uModel)
{
	return (IDevice *)(CPlatformSitara *) New CPlatformCanyonCmos(uModel);
	}

// Constructor

CPlatformCanyonCmos::CPlatformCanyonCmos(UINT uModel) : CPlatformCanyon(uModel)
{
	}

// Destructor

CPlatformCanyonCmos::~CPlatformCanyonCmos(void)
{
	piob->RevokeGroup("dev.");

	delete m_pPwm[0];

	delete m_pPwm[1];

	delete m_pPwm[2];

	delete m_pPwm[3];

	delete m_pPwm[4];

	delete m_pPru;
	}

// IDevice

BOOL CPlatformCanyonCmos::Open(void)
{
	if( CPlatformCanyon::Open() ) {

		m_pPwm[0] = New CPwm437(0, m_pClock);

		m_pPwm[1] = New CPwm437(1, m_pClock);

		m_pPwm[2] = New CPwm437(2, m_pClock);

		m_pPwm[3] = New CPwm437(3, m_pClock);

		m_pPwm[4] = New CPwm437(5, m_pClock);

		m_pPru    = New CPru437(0, 0);

		InitPru();

		piob->RegisterSingleton("dev.beeper",  0, Create_Beeper437(m_pPwm[1], 0));
		
		piob->RegisterSingleton("dev.uart",    0, Create_Uart437(1, m_pClock, NULL));
		
		piob->RegisterSingleton("dev.uart",    1, Create_Uart437(0, m_pClock, NULL));
		
		piob->RegisterSingleton("dev.uart",    2, Create_Uart437(2, m_pClock, m_pPru));
		
		piob->RegisterSingleton("dev.display", 0, Create_Display437(m_uModel, m_pDss, m_pPwm[3], 0));

		piob->RegisterSingleton("dev.touch",   0, Create_Touch437(TRUE, TRUE, 20, 5));

		piob->RegisterSingleton("dev.nic",     0, Create_Enet437(m_pClock));

		InitUarts();

		InitNicMac(0);

		FindSerial();

		return TRUE;
		}

	return FALSE;
	}

// ILeds

void METHOD CPlatformCanyonCmos::SetLed(UINT uLed, UINT uState)
{
	UINT iSel = HIBYTE(uLed);

	UINT iLed = LOBYTE(uLed);

	if( iSel == 0 ) {
	
		switch( iLed ) {

			case 0: m_pPwm[4]->SetDuty(CPwm437::pwmChanA, uState == stateOff ? 0 : 100); return;
			case 1: m_pPwm[2]->SetDuty(CPwm437::pwmChanA, uState == stateOff ? 0 : 100); return;
			case 2: m_pPwm[0]->SetDuty(CPwm437::pwmChanA, uState == stateOff ? 0 : 100); return;
			}
		}
	
	CPlatformCanyon::SetLed(uLed, uState);
	}

// IInputSwitch

UINT METHOD CPlatformCanyonCmos::GetSwitches(void)
{
	return 0xFFFFFFFE | (GetSwitch(0) ? Bit(0) : 0);
	}

UINT METHOD CPlatformCanyonCmos::GetSwitch(UINT uSwitch)
{
	if( uSwitch == 0 ) {

		return m_pGpio[5]->GetState(30);
		}

	return 1;
	}

// IPortSwitch

BOOL METHOD CPlatformCanyonCmos::SetFull(UINT uUnit, BOOL fFull)
{
	switch( uUnit ) {

		case 2:
			m_pGpio[5]->SetState(10, !fFull);
			
			return TRUE;
		}

	return FALSE;
	}

// IUsbSystemPortMapper

UINT METHOD CPlatformCanyonCmos::GetPortType(UsbTreePath const &Path)
{
	if( Path.a.dwTier == 2 ) {

		switch( Path.a.dwPort2 ) {

			case 1: 
				return typeExtern;

			case 2:
				return typeOption;
			}
		}

	return CPlatformCanyon::GetPortType(Path);
	}

// Inititialisation

void CPlatformCanyonCmos::InitClocks(void)
{
	CPlatformCanyon::InitClocks();

	m_pClock->SetClockMode(CClock437::clockADC0,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM0,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM1,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM2,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM3,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM5,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPRU,   CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART1, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART2, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockMAC0,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockCPSW,  CClock437::modeSwWakup);
	}

void CPlatformCanyonCmos::InitMux(void)
{
	CPlatformCanyon::InitMux();

	DWORD const muxPwm[] = {

		CCtrl437::pinSPI0SCLK,    CCtrl437::muxMode3,
		CCtrl437::pinGPMCA2,      CCtrl437::muxMode6,
		CCtrl437::pinSPI0CS1,     CCtrl437::muxMode8,
		CCtrl437::pinCAM1FIELD,   CCtrl437::muxMode8,
		CCtrl437::pinUART3CTSN,   CCtrl437::muxMode6,
		};

	DWORD const muxPru[] = {

		CCtrl437::pinUART3RXD,    CCtrl437::muxMode5 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMCASP0ACLKX, CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMCASP0FSX,   CCtrl437::muxMode6 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		};

	DWORD const muxSwi[] = {

		CCtrl437::pinGPMCWAIT0,   CCtrl437::muxMode9 | CCtrl437::padSlow | CCtrl437::padPullUp | CCtrl437::padRxActive,
		};

	DWORD const muxSer[] = {

		CCtrl437::pinUART0RXD,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinUART0TXD,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA8,   CCtrl437::muxMode8 | CCtrl437::padPullNo | CCtrl437::padRxActive, 
		CCtrl437::pinCAM1DATA9,   CCtrl437::muxMode8 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA0,   CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA1,   CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA2,   CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA3,   CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA4,   CCtrl437::muxMode2 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA5,   CCtrl437::muxMode2 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA7,   CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMCASP0ACLKX, CCtrl437::muxMode5 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		};

	DWORD const muxNic[] = {

		CCtrl437::pinMII1TXD0,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMII1TXD1,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMII1TXD2,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMII1TXD3,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMII1RXD0,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXD1,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXD2,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXD3,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1TXEN,    CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMII1TXCLK,   CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXDV,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXCLK,   CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1CRS,     CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,	
		CCtrl437::pinMDIOCLK,     CCtrl437::muxMode0 | CCtrl437::padPullNo,
		CCtrl437::pinMDIODATA,    CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinGPIO5_8,     CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		};

	m_pCtrl->SetMux(muxPwm, elements(muxPwm));

	m_pCtrl->SetMux(muxPru, elements(muxPru));

	m_pCtrl->SetMux(muxSwi, elements(muxSwi));

	m_pCtrl->SetMux(muxSer, elements(muxSer));

	m_pCtrl->SetMux(muxNic, elements(muxNic));
	}

void CPlatformCanyonCmos::InitGpio(void)
{
	CPlatformSitara::InitGpio();

	m_pGpio[5]->SetState(8, false);

	m_pGpio[4]->SetDirection(29, true);
	m_pGpio[5]->SetDirection(8,  true);
	m_pGpio[5]->SetDirection(10, true);
	m_pGpio[5]->SetDirection(30, false);
	}

void CPlatformCanyonCmos::InitMisc(void)
{
	CPlatformCanyon::InitMisc();

	m_pCtrl->SetPWM(0, true);
	
	m_pCtrl->SetPWM(1, true);

	m_pCtrl->SetPWM(2, true);

	m_pCtrl->SetPWM(3, true);

	m_pCtrl->SetPWM(5, true);

	m_pPwm[0]->SetFreq(5000);

	m_pPwm[2]->SetFreq(5000);

	m_pPwm[4]->SetFreq(5000);

	m_pCtrl->SetMIIMode(0, CCtrl437::modeGMII);
	}

void CPlatformCanyonCmos::InitPru(void)
{
	m_pPcm->EnablePru();

	m_pPru->Stop(0);

	m_pPru->Reset(0);

	m_pPru->SetMux(0);

	m_pPru->LoadCode(0, PRU_CODE0);
	}

// End of File
