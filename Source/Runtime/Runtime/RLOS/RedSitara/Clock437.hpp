
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Clock437_HPP
	
#define	INCLUDE_AM437_Clock437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCtrl437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 Clock Module
//

class CClock437
{
	public:
		// Constructor
		CClock437(CCtrl437 *pCtrl);

		// PLL Clocks
		DWORD GetCorePLLFreq(void);
		DWORD GetCoreM4Freq(void);
		DWORD GetCoreM5Freq(void);
		DWORD GetCoreM6Freq(void);
		DWORD GetPerPLLFreq(void);
		DWORD GetPerM2Freq(void);
		DWORD GetMpuFreq(void);
		DWORD GetDdrFreq(void);
		DWORD GetDispFreq(void);
		DWORD GetExternFreq(void);
		void  SetCorePLLFreq(UINT m, UINT n, UINT m4, UINT m5, UINT m6);
		void  SetPerPLLFreq(UINT m, UINT n, UINT m2);
		void  SetMpuFreq(UINT m, UINT n, UINT m2);
		void  SetMpuFreq(UINT n);
		void  SetDdrFreq(UINT m, UINT n, UINT m2, UINT m4);
		void  SetDispFreq(UINT m, UINT n, UINT m2);

		// DLL
		void WaitDLLReady(void);

		// Peripheral Clocks
		void SetClockMode(UINT uClock, UINT uMode);
		UINT GetClockState(UINT uClock);
		void SetClockSource(UINT uClock, UINT uSource);

		// Clock Modes
		enum ClockModes
		{
			modeNoSleep	= 0,
			modeSwSleep	= 1,
			modeSwWakup	= 2, 
			modeOption	= Bit(8),
			};

		// Clock States
		enum ClockState
		{
			stateReady	= 0,
			stateBusy	= 1, 
			stateIdle	= 2,
			stateDisabled	= 3,
			};

		// Clock Sources
		enum ClockSource
		{
			sourceClockIn	= 0,
			sourceOSC	= 1,
			sourceOCR	= 2,
			};

		// Peripheral Clocks
		enum PeripheralClock
		{
			clockL3,
			clockOCMCRAM,
			clockVPFE0,
			clockVPFE1,
			clockTPCC,
			clockTPTC0,
			clockTPTC1,
			clockTPTC2,
			clockDLLAGING,
			clockL4HS,
			clockL3S,
			clockGPMC,
			clockADC0,
			clockADC1,
			clockMCASP0,
			clockMCASP1,
			clockQSPI,
			clockUSB0,
			clockUSB1,
			clockPRU,
			clockL4S,
			clockDCAN0,
			clockDCAN1,
			clockPWM0,
			clockPWM1,
			clockPWM2,
			clockPWM3,
			clockPWM4,
			clockPWM5,
			clockELM,
			clockGPIO0,
			clockGPIO1,
			clockGPIO2,
			clockGPIO3,
			clockGPIO4,
			clockGPIO5,
			clockHDQ1W,
			clockI2C0,
			clockI2C1,
			clockI2C2,
			clockMAIL0,
			clockMMC0,
			clockMMC1,
			clockMMC2,
			clockSPI0,
			clockSPI1,
			clockSPI2,
			clockSPI3,
			clockSPI4,
			clockSPINLOCK,
			clockTIMER2,
			clockTIMER3,
			clockTIMER4,
			clockTIMER5,
			clockTIMER6,
			clockTIMER7,
			clockTIMER8,
			clockTIMER9,
			clockTIMER10,
			clockTIMER11,
			clockUART0,
			clockUART1,
			clockUART2,
			clockUART3,
			clockUART4,
			clockUART5,
			clockUSBPHY0,
			clockUSBPHY1,
			clockEMIF,
			clockDLL,
			clockLCDC,
			clockDSS,
			clockCPSW,
			clockMAC0,
			clockOCPWP,
			clockRTC,
			};
				
	protected:
		// DEV Registers
		enum
		{
			regCLKOUT1		= 0x0000 / sizeof(DWORD),
			regDLLCTRL		= 0x0004 / sizeof(DWORD),
			regCLKOUT2		= 0x0008 / sizeof(DWORD),
			};

		// PLL Registers
		enum
		{
			regCLKSELTMR1		= 0x0000 / sizeof(DWORD),
			regCLKSELTMR2		= 0x0004 / sizeof(DWORD),
			regCLKSELTMR3		= 0x0008 / sizeof(DWORD),
			regCLKSELTMR4		= 0x000C / sizeof(DWORD),
			regCLKSELTMR5		= 0x0010 / sizeof(DWORD),
			regCLKSELTMR6		= 0x0014 / sizeof(DWORD),
			regCLKSELTMR7		= 0x0018 / sizeof(DWORD),
			regCLKSELTMR8		= 0x001C / sizeof(DWORD),
			regCLKSELTMR9		= 0x0020 / sizeof(DWORD),
			regCLKSELTMR10		= 0x0024 / sizeof(DWORD),
			regCLKSELTMR11		= 0x0028 / sizeof(DWORD),
			regCLKSELWDT1		= 0x002C / sizeof(DWORD),
			regCLKSELSYNCTMR	= 0x0030 / sizeof(DWORD),
			regCLKSELMAC		= 0x0034 / sizeof(DWORD),
			regCLKSELCPTS		= 0x0038 / sizeof(DWORD),
			regCLKSELGFX		= 0x003C / sizeof(DWORD),
			regCLKSELGPIO		= 0x0040 / sizeof(DWORD),
			regCLKSELICSS		= 0x0048 / sizeof(DWORD),
			regCLKSELADC1		= 0x004C / sizeof(DWORD),
			regCLKSELAGING		= 0x0050 / sizeof(DWORD),
			regCLKSELUSBPHY		= 0x0060 / sizeof(DWORD),
			};

		// WKUP Registers
		enum
		{
			regADC0			= 0x0120 / sizeof(DWORD),
			regI2C0			= 0x0340 / sizeof(DWORD),
			regUART0		= 0x0348 / sizeof(DWORD),
			regGPIO0		= 0x0368 / sizeof(DWORD),
			regCLKMODEDPLLCORE	= 0x0520 / sizeof(DWORD),
			regIDLESTDPLLCORE	= 0x0524 / sizeof(DWORD),
			regCLKSELDPLLCORE	= 0x052C / sizeof(DWORD),
			regDIVM4DPLLCORE	= 0x0538 / sizeof(DWORD),
			regDIVM5DPLLCORE	= 0x053C / sizeof(DWORD),
			regDIVM6DPLLCORE	= 0x0540 / sizeof(DWORD),
			regCLKMODEDPLLMPU	= 0x0560 / sizeof(DWORD),
			regIDLESTDPLLMPU	= 0x0564 / sizeof(DWORD),
			regCLKSELDPLLMPU	= 0x056C / sizeof(DWORD),
			regDIVM2DPLLMPU		= 0x0570 / sizeof(DWORD),
			regCLKMODEDPLLDDR	= 0x05A0 / sizeof(DWORD),
			regIDLESTDPLLDDR	= 0x05A4 / sizeof(DWORD),
			regCLKSELDPLLDDR	= 0x05AC / sizeof(DWORD),
			regDIVM2DPLLDDR		= 0x05B0 / sizeof(DWORD),
			regDIVM4DPLLDDR		= 0x05B8 / sizeof(DWORD),
			regCLKMODEDPLLPER	= 0x05E0 / sizeof(DWORD),
			regIDLESTDPLLPER	= 0x05E4 / sizeof(DWORD),
			regCLKSELDPLLPER	= 0x05EC / sizeof(DWORD),
			regDIVM2DPLLPER		= 0x05F0 / sizeof(DWORD),
			regCLKMODEDPLLDISP	= 0x0620 / sizeof(DWORD),
			regIDLESTDPLLDISP	= 0x0624 / sizeof(DWORD),
			regCLKSELDPLLDISP	= 0x062C / sizeof(DWORD),
			regDIVM2DPLLDISP	= 0x0630 / sizeof(DWORD),
			regDELTADPLLDISP	= 0x0648 / sizeof(DWORD),
			regMODFREQDPLLDISP	= 0x064C / sizeof(DWORD),
			regCLKMODEDPLLEXT	= 0x0660 / sizeof(DWORD),
			regIDLESTDPLLEXT	= 0x0664 / sizeof(DWORD),
			regCLKSELDPLLEXT	= 0x066C / sizeof(DWORD),
			regDIVM2DPLLEXT		= 0x0670 / sizeof(DWORD),
			regCLKSEL2DPLLEXT	= 0x0684 / sizeof(DWORD),
			};

		// PER Registers
		enum
		{
			regL3ST			= 0x0000 / sizeof(DWORD),
			regL3			= 0x0020 / sizeof(DWORD),
			regL3INST		= 0x0040 / sizeof(DWORD),
			regOCMCRAM		= 0x0050 / sizeof(DWORD),
			regVPFE0		= 0x0068 / sizeof(DWORD),
			regVPFE1		= 0x0070 / sizeof(DWORD),
			regTPCC			= 0x0078 / sizeof(DWORD),
			regTPTC0		= 0x0080 / sizeof(DWORD),
			regTPTC1		= 0x0088 / sizeof(DWORD),
			regTPTC2		= 0x0090 / sizeof(DWORD),
			regDLLAGING		= 0x0098 / sizeof(DWORD),
			regL4HS			= 0x00A0 / sizeof(DWORD),
			regL3S			= 0x0200 / sizeof(DWORD),
			regGPMC			= 0x0220 / sizeof(DWORD),
			regADC1			= 0x0230 / sizeof(DWORD),
			regMCASP0	 	= 0x0238 / sizeof(DWORD),
			regMCASP1		= 0x0240 / sizeof(DWORD),
			regMMC2			= 0x0248 / sizeof(DWORD),
			regQSPI			= 0x0258 / sizeof(DWORD),
			regUSB0			= 0x0260 / sizeof(DWORD),
			regUSB1			= 0x0268 / sizeof(DWORD),
			regPRUST		= 0x0300 / sizeof(DWORD),
			regPRU			= 0x0320 / sizeof(DWORD),
			regL4SST		= 0x0400 / sizeof(DWORD),
			regL4S			= 0x0420 / sizeof(DWORD),
			regDCAN0		= 0x0428 / sizeof(DWORD),
			regDCAN1		= 0x0430 / sizeof(DWORD),
			regPWM0			= 0x0438 / sizeof(DWORD),
			regPWM1			= 0x0440 / sizeof(DWORD),
			regPWM2			= 0x0448 / sizeof(DWORD),
			regPWM3			= 0x0450 / sizeof(DWORD),
			regPWM4			= 0x0458 / sizeof(DWORD),
			regPWM5			= 0x0460 / sizeof(DWORD),
			regELM			= 0x0468 / sizeof(DWORD),
			regGPIO1		= 0x0478 / sizeof(DWORD),
			regGPIO2		= 0x0480 / sizeof(DWORD),
			regGPIO3		= 0x0488 / sizeof(DWORD),
			regGPIO4		= 0x0490 / sizeof(DWORD),
			regGPIO5		= 0x0498 / sizeof(DWORD),
			regHDQ1W		= 0x04A0 / sizeof(DWORD),
			regI2C1			= 0x04A8 / sizeof(DWORD),
			regI2C2			= 0x04B0 / sizeof(DWORD),
			regMAIL0		= 0x04B8 / sizeof(DWORD),
			regMMC0			= 0x04C0 / sizeof(DWORD),
			regMMC1			= 0x04C8 / sizeof(DWORD),
			regSPI0			= 0x0500 / sizeof(DWORD),
			regSPI1			= 0x0508 / sizeof(DWORD),
			regSPI2			= 0x0510 / sizeof(DWORD),
			regSPI3			= 0x0518 / sizeof(DWORD),
			regSPI4			= 0x0520 / sizeof(DWORD),
			regSPINLOCK		= 0x0528 / sizeof(DWORD),
			regTIMER2		= 0x0530 / sizeof(DWORD),
			regTIMER3		= 0x0538 / sizeof(DWORD),
			regTIMER4		= 0x0540 / sizeof(DWORD),
			regTIMER5		= 0x0548 / sizeof(DWORD),
			regTIMER6		= 0x0550 / sizeof(DWORD),
			regTIMER7		= 0x0558 / sizeof(DWORD),
			regTIMER8		= 0x0560 / sizeof(DWORD),
			regTIMER9		= 0x0568 / sizeof(DWORD),
			regTIMER10		= 0x0570 / sizeof(DWORD),
			regTIMER11		= 0x0578 / sizeof(DWORD),
			regUART1		= 0x0580 / sizeof(DWORD),
			regUART2		= 0x0588 / sizeof(DWORD),
			regUART3		= 0x0590 / sizeof(DWORD),
			regUART4		= 0x0598 / sizeof(DWORD),
			regUART5		= 0x05A0 / sizeof(DWORD),
			regUSBPHY0		= 0x05B8 / sizeof(DWORD),
			regUSBPHY1		= 0x05C0 / sizeof(DWORD),
			regEMIFST		= 0x0700 / sizeof(DWORD),
			regEMIF			= 0x0720 / sizeof(DWORD),
			regDLL			= 0x0728 / sizeof(DWORD),
			regLCDC			= 0x0800 / sizeof(DWORD),
			regDSSST		= 0x0A00 / sizeof(DWORD),
			regDSS			= 0x0A20 / sizeof(DWORD),
			regCPSW			= 0x0B00 / sizeof(DWORD),
			regMAC0			= 0x0B20 / sizeof(DWORD),
			regOCPWPL3ST		= 0x0C00 / sizeof(DWORD),
			regOCPWP		= 0x0C20 / sizeof(DWORD),
			};

		// RTC Registers
		enum
		{
			regCLKSTCTRL		= 0x0000 / sizeof(DWORD),
			regCLKCTRL		= 0x0020 / sizeof(DWORD),
			};
					
		// Data Members
		PVDWORD	 m_pBaseWUP;
		PVDWORD  m_pBasePER;
		PVDWORD	 m_pBaseDEV;
		PVDWORD  m_pBasePLL;
		PVDWORD  m_pBaseRTC;
		UINT     m_uRef;
	};

// End of File

#endif
