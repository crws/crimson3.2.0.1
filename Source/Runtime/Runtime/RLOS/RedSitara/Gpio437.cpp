
#include "Intern.hpp"

#include "Gpio437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 GPIO Module
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Instantiator

global IDevice * Create_Gpio437(UINT iIndex)
{
	IDevice *pDevice = New CGpio437(iIndex);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CGpio437::CGpio437(UINT iIndex)
{
	StdSetRef();

	m_pBase  = PVDWORD(FindBase(iIndex));

	m_iIndex = iIndex;

	m_uLine  = FindLine(iIndex);

	memset(m_Sink, 0, sizeof(m_Sink));
	}

// IUnknown

HRESULT CGpio437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IGpio);

	return E_NOINTERFACE;
	}

ULONG CGpio437::AddRef(void)
{
	StdAddRef();
	}

ULONG CGpio437::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL CGpio437::Open(void)
{
	return TRUE;
	}

// IGpio

void CGpio437::SetDirection(UINT n, BOOL fOutput)
{
	if( fOutput ) {

		AtomicBitClr(&m_pBase[regOE], n);
		}
	else {
		AtomicBitSet(&m_pBase[regOE], n);
		}
	}

void CGpio437::SetState(UINT n, BOOL fState)
{
	if( fState ) {

		Reg(SETDATA) = Bit(n);
		}
	else {
		Reg(CLRDATA) = Bit(n);
		}
	}

BOOL CGpio437::GetState(UINT n)
{
	return m_pBase[regDATAIN] & Bit(n);	
	}

void CGpio437::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);
	}

void CGpio437::SetIntHandler(UINT n, UINT uType, IEventSink *pSink, UINT uParam)
{
	EnableEvents();

	if( n < elements(m_Sink) ) {

		switch( uType ) {

			case intLevelLo:

				AtomicBitSet(&m_pBase[regLEVEL0], n);

				break;

			case intLevelHi:
			
				AtomicBitSet(&m_pBase[regLEVEL1], n);

				break;

			case intEdgeRising:

				AtomicBitSet(&m_pBase[regRISING], n);

				break;

			case intEdgeFalling:

				AtomicBitSet(&m_pBase[regFALLING], n);

				break;

			case intEdgeAny:

				AtomicBitSet(&m_pBase[regRISING], n);

				AtomicBitSet(&m_pBase[regFALLING], n);

				break;
			}

		m_Sink[n].m_pSink  = pSink;

		m_Sink[n].m_uParam = uParam;
		}
	}

void CGpio437::SetIntEnable(UINT n, BOOL fEnable)
{
	if( n < elements(m_Sink) ) {

		if( fEnable ) {

			Reg(IRQSTSSET0) = Bit(n);
			}
		else {
			Reg(IRQSTSCLR0) = Bit(n);
			}
		}
	}

// IEventSink

void CGpio437::OnEvent(UINT uLine, UINT uParam)
{
	for(;;) {

		DWORD dwStat = Reg(IRQSTS0);
		
		if( dwStat ) {
			
			DWORD dwMask = 1;
			
			for( UINT i = 0; i < elements(m_Sink); i ++ ) {

				if( dwStat & dwMask ) {

					if( m_Sink[i].m_pSink ) {

						m_Sink[i].m_pSink->OnEvent(uLine, m_Sink[i].m_uParam);
						}
					}

				dwMask <<= 1;
				}

			Reg(IRQSTS0) = dwStat;
			
			continue;
			}

		break;
		}
	}

// Implementation

DWORD CGpio437::FindBase(UINT iIndex) const
{
	switch( iIndex ) {

		case 0: return ADDR_GPIO0;
		case 1: return ADDR_GPIO1;
		case 2: return ADDR_GPIO2;
		case 3: return ADDR_GPIO3;
		case 4: return ADDR_GPIO4;
		case 5: return ADDR_GPIO5;
		}

	return NOTHING;
	}

UINT CGpio437::FindLine(UINT iIndex) const
{
	switch( iIndex ) {

		case 0: return INT_GPIO0A;
		case 1: return INT_GPIO1A;
		case 2: return INT_GPIO2A;
		case 3: return INT_GPIO3A;
		case 4: return INT_GPIO4A;
		case 5: return INT_GPIO5A;
		}

	return NOTHING;
	}

// End of File
