
#include "Intern.hpp"

#include "Uart437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Hardware Drivers
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

#include "Pru437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 UART
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Instantiator

IDevice * Create_Uart437(UINT iIndex, CClock437 *pClock, CPru437 *pPru)
{
	CUart437 *pDevice = New CUart437(iIndex, pClock, pPru);

	pDevice->Open();

	return pDevice;
}

// Constructor

CUart437::CUart437(UINT iIndex, CClock437 *pClock, CPru437 *pPru)
{
	StdSetRef();

	m_uUnit    = iIndex;

	m_pBase    = PVWORD(FindBase(iIndex));

	m_uLine    = FindLine(iIndex);

	m_uClock   = pClock->GetPerM2Freq() / 4;

	m_pPru     = pPru;

	m_uPru	   = 0;

	m_uState   = stateClosed;

	m_pHandler = NULL;

	m_pTimer   = CreateTimer();
}

// Destructor

CUart437::~CUart437(void)
{
	m_pTimer->Release();
}

// IUnknown

HRESULT CUart437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IPortObject);

	return E_NOINTERFACE;
}

ULONG CUart437::AddRef(void)
{
	StdAddRef();
}

ULONG CUart437::Release(void)
{
	StdRelease();
}

// IDevice

BOOL METHOD CUart437::Open(void)
{
	m_pTimer->SetPeriod(5);

	m_pTimer->SetHook(this, 0);

	return TRUE;
}

// IPortObject

void METHOD CUart437::Bind(IPortHandler *pHandler)
{
	m_pHandler = pHandler;

	m_pHandler->Bind(this);
}

void METHOD CUart437::Bind(IPortSwitch *pSwitch)
{
	m_pSwitch = pSwitch;
}

UINT METHOD CUart437::GetPhysicalMask(void)
{
	return m_pSwitch->GetMask(m_uUnit);
}

BOOL METHOD CUart437::Open(CSerialConfig const &Config)
{
	if( m_uState == stateClosed ) {

		m_Config = Config;

		if( InitUart() && InitBaudRate() && InitFlags() && InitFormat() ) {

			EnableEvents();

			m_uState = stateOpen;

			m_pHandler->OnOpen(m_Config);

			EnablePort();

			EnablePru();

			m_pTimer->Enable(true);

			return true;
		}
	}

	return false;
}

void METHOD CUart437::Close(void)
{
	if( m_uState == stateOpen ) {

		m_pTimer->Enable(false);

		m_pHandler->OnClose();

		WaitDone();

		DisableEvents();

		DisablePort();

		DisablePru();

		m_uState = stateClosed;
	}
}

void METHOD CUart437::Send(BYTE bData)
{
	if( m_uState == stateOpen ) {

		EnableInterrupts(false);

		Reg(THR)  = bData;

		Reg(IER) |= intTx;

		EnableInterrupts(true);
	}
}

void METHOD CUart437::SetBreak(BOOL fBreak)
{
	if( m_uState == stateOpen ) {

		if( fBreak ) {

			Reg(FCR) |= bitTxFifoClear;

			WaitDone();

			Reg(LCR) |= bitBreakEn;
		}
		else {
			Reg(LCR) &= ~bitBreakEn;
		}
	}
}

void METHOD CUart437::EnableInterrupts(BOOL fEnable)
{
	phal->EnableLineViaIrql(m_uLine, m_uSave, fEnable);
}

void METHOD CUart437::SetOutput(UINT uOutput, BOOL fOn)
{
	if( fOn ) {

		Reg(MCR) |= bitAssertRts;
	}
	else {
		Reg(MCR) &= ~bitAssertRts;
	}
}

BOOL METHOD CUart437::GetInput(UINT uInput)
{
	return Reg(MSR) & bitCts;
}

DWORD METHOD CUart437::GetHandle(void)
{
	return NOTHING;
}

// IEventSink

void CUart437::OnEvent(UINT uLine, UINT uParam)
{
	if( m_uState == stateOpen ) {

		if( uLine == m_uLine ) {

			for(;;) {

				WORD wType = (Reg(IIR) >> 1) & 0x1F;

				if( wType == typeTxFifo ) {

					bool fData = false;

					while( !(Reg(SSR) & Bit(0)) ) {

						BYTE bData;

						if( m_pHandler->OnTxData(bData) ) {

							Reg(THR) = bData;

							fData    = true;

							continue;
						}

						Reg(IER) &= ~intTx;

						if( fData ) {

							m_pHandler->OnTxDone();
						}

						break;
					}

					continue;
				}

				if( wType == typeRxFifo || wType == typeRxTimeout ) {

					bool fData = false;

					for(;;) {

						WORD wStatus = Reg(LSR);

						if( wStatus & statRxFifo ) {

							DWORD Data = Reg(RHR);

							if( !(wStatus & (statRxParity | statRxFrame | statRxBreak)) ) {

								m_pHandler->OnRxData(Data & m_bMask);

								fData = true;
							}

							continue;
						}
						break;
					}

					if( fData ) {

						m_pHandler->OnRxDone();
					}

					continue;
				}

				break;
			}
		}

		if( uLine == NOTHING ) {

			m_pHandler->OnTimer();
		}
	}
}

// Implementation

void CUart437::Reset(void)
{
	Reg(SYSC) = Bit(1);

	while( !(Reg(SYSS) & Bit(0)) );
}

bool CUart437::InitUart(void)
{
	Reset();

	Reg(LCR)  = 0xBF;

	Reg(EFR)  = 0x10;

	Reg(LCR)  = 0x80;

	Reg(MCR)  = 0x40;

	Reg(DLL)  = 0x00;

	Reg(DLH)  = 0x00;

	Reg(FCR)  = 0x67;

	Reg(SCR)  = 0x00;

	Reg(LCR)  = 0xBF;

	Reg(MDR1) = 0x07;

	Reg(TLR)  = 0x00;

	Reg(IER)  = 0x00;

	Reg(TCR)  = 0x4C;

	return true;
}

bool CUart437::InitBaudRate(void)
{
	UINT uDiv = ((m_uClock / 16) + (m_Config.m_uBaudRate / 2)) / m_Config.m_uBaudRate;

	Reg(DLL)  = LOBYTE(LOWORD(uDiv));

	Reg(DLH)  = HIBYTE(LOWORD(uDiv));

	return true;
}

bool CUart437::InitFlags(void)
{
	if( m_Config.m_uFlags & flagFastRx ) {

		Reg(SCR) |= bitRxLevel1Bit;
	}

	return true;
}

bool CUart437::InitFormat(void)
{
	WORD wFmt0 = 0;

	WORD wFmt1 = 0;

	wFmt0 |= GetPhysical();

	wFmt1 |= GetParityInit();

	wFmt1 |= GetStopBitsInit();

	wFmt1 |= GetDataBitsInit();

	if( wFmt0 != 0xFFFF && wFmt1 != 0xFFFF ) {

		Reg(EFR) = wFmt0;

		Reg(LCR) = wFmt1;

		m_bMask  = GetDataBitsMask();
	}

	return true;
}

WORD CUart437::GetParityInit(void)
{
	switch( m_Config.m_uParity ) {

		case parityNone: return 0x0 << 3;
		case parityOdd:  return 0x1 << 3;
		case parityEven: return 0x3 << 3;
	}

	return 0xFFFF;
}

WORD CUart437::GetStopBitsInit(void)
{
	switch( m_Config.m_uStopBits ) {

		case 1: return 0x0 << 2;
		case 2: return 0x1 << 2;
	}

	return 0xFFFF;
}

WORD CUart437::GetDataBitsInit(void)
{
	switch( m_Config.m_uDataBits ) {

		case 5:
		case 6:
		case 7:
		case 8:
			return m_Config.m_uDataBits - 5;
	}

	return 0xFFFF;
}

BYTE CUart437::GetDataBitsMask(void)
{
	switch( m_Config.m_uDataBits ) {

		case 5: return 0x1F;
		case 6: return 0x3F;
		case 7:	return 0x7F;
		case 8: return 0xFF;
	}

	return 0x00;
}

WORD CUart437::GetPhysical(void)
{
	switch( m_Config.m_uPhysical ) {

		case physicalRS232:

			SetOutput(0, true);

			m_pSwitch->SetPhysical(m_uUnit, false);

			m_pSwitch->SetFull(m_uUnit, true);

			if( m_Config.m_uFlags & flagHonorCTS ) {

				return bitAutoCts | bitAutoRts;
			}

			return 0;

		case physicalRS422Master:

			m_pSwitch->SetPhysical(m_uUnit, true);

			m_pSwitch->SetFull(m_uUnit, true);

			return 0;

		case physicalRS422Slave:

			m_pSwitch->SetPhysical(m_uUnit, true);

			m_pSwitch->SetFull(m_uUnit, true);

			return 0;

		case physicalRS485:

			m_pSwitch->SetPhysical(m_uUnit, true);

			m_pSwitch->SetFull(m_uUnit, false);

			return 0;
	}

	return 0xFFFF;
}

void CUart437::EnablePort(void)
{
	Reg(MDR1) = 0x00;

	Reg(IER)  = intRx;

	m_pSwitch->EnablePort(m_uUnit, true);
}

void CUart437::DisablePort(void)
{
	Reg(IER)  = 0x00;

	Reg(MDR1) = 0x07;

	m_pSwitch->EnablePort(m_uUnit, false);
}

void CUart437::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);
}

void CUart437::DisableEvents(void)
{
	phal->EnableLine(m_uLine, false);
}

bool CUart437::CanWait(void)
{
	if( Hal_GetIrql() >= IRQL_DISPATCH ) {

		return false;
	}

	if( !GetCurrentThread() ) {

		return false;
	}

	if( GetCurrentThread()->GetIndex() <= 1 ) {

		return false;
	}

	return true;
}

bool CUart437::WaitDone(void)
{
	GetCurrentThread()->SetTimer(100);

	UINT uBits, uTime;

	uBits  = m_Config.m_uDataBits + m_Config.m_uStopBits;

	uBits += (m_Config.m_uParity == parityNone) ? 1 : 2;

	uTime  = uBits * 1000 / m_Config.m_uBaudRate;

	while( GetCurrentThread()->GetTimer() ) {

		if( Reg(LSR) & statTxDone ) {

			return true;
		}

		if( CanWait() ) {

			Sleep(uTime);
		}
	}

	return false;
}

void CUart437::EnablePru(void)
{
	if( m_pPru ) {

		DWORD dwData;

		switch( m_Config.m_uPhysical ) {

			case physicalRS422Master:

				m_pPru->Reset(m_uPru);

				dwData = 1;

				m_pPru->LoadData(m_uPru, PBYTE(&dwData), sizeof(dwData));

				m_pPru->Run(m_uPru);

				break;

			case physicalRS422Slave:

			case physicalRS485:

				m_pPru->Reset(m_uPru);

				dwData = 0;

				m_pPru->LoadData(m_uPru, PBYTE(&dwData), sizeof(dwData));

				m_pPru->Run(m_uPru);

				switch( m_Config.m_uParity ) {

					case parityNone:
					case parityOdd :

						Reg(THR) = 0x01;
						Reg(THR) = 0x00;

						break;

					case parityEven:

						Reg(THR) = 0x03;
						Reg(THR) = 0x00;

						break;
					}

				while( !(Reg(LSR) & statTxDone) );

				break;
		}
	}
}

void CUart437::DisablePru(void)
{
	if( m_pPru ) {

		switch( m_Config.m_uPhysical ) {

			case physicalRS422Slave:

			case physicalRS485:

			case physicalRS422Master:

				m_pPru->Stop(m_uPru);

				break;
		}
	}
}

UINT CUart437::FindBase(UINT iIndex) const
{
	switch( iIndex ) {

		case 0: return ADDR_UART0;
		case 1: return ADDR_UART1;
		case 2: return ADDR_UART2;
		case 3: return ADDR_UART3;
		case 4: return ADDR_UART4;
		case 5: return ADDR_UART5;
	}

	return 0;
}

UINT CUart437::FindLine(UINT iIndex) const
{
	switch( iIndex ) {

		case 0: return INT_UART0;
		case 1: return INT_UART1;
		case 2: return INT_UART2;
		case 3: return INT_UART3;
		case 4: return INT_UART4;
		case 5: return INT_UART5;
	}

	return 0;
}

// End of File
