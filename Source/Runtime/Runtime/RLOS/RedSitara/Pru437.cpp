
#include "Intern.hpp"

#include "Pru437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 Power Management Module
//

// Register Access

#define Reg(x)		(m_pBase[reg##x])

#define RegCtrl(n, x)	(m_pControl[n][reg##x])

#define RegDbg(n, x)	(m_pDebug[n][reg##x])

#define RegInt(x)	(m_pIntCtrl[reg##x])

#define RegCfg(x)	(m_pConfig[reg##x])

// Constructor

CPru437::CPru437(UINT iIndex, UINT uUnit)
{
	m_uPos	      = iIndex;

	m_uUnit       = uUnit;

	m_pBase       = PVDWORD(FindBase(iIndex));
	
	m_pConfig     = PVDWORD(DWORD(m_pBase) + memCFG);
	
	m_pIntCtrl    = PVDWORD(DWORD(m_pBase) + memINTC);
	
	m_pControl[0] = PVDWORD(DWORD(m_pBase) + memPRU0CTRL);
	
	m_pControl[1] = PVDWORD(DWORD(m_pBase) + memPRU1CTRL);
	
	m_pDebug[0]   = PVDWORD(DWORD(m_pBase) + memPRU0DEBUG);
	
	m_pDebug[1]   = PVDWORD(DWORD(m_pBase) + memPRU1DEBUG);

	m_pCode[0]    = PVDWORD(DWORD(m_pBase) + memPRU0IRAM);
	
	m_pCode[1]    = PVDWORD(DWORD(m_pBase) + memPRU1IRAM);

	m_pData[0]    = PVDWORD(DWORD(m_pBase) + (m_uUnit ? memRAM1 : memRAM0));
	
	m_pData[1]    = PVDWORD(DWORD(m_pBase) + (m_uUnit ? memRAM0 : memRAM1));
	}

// Attributes

bool CPru437::IsHalted(UINT uPru) const
{
	if( uPru < elements(m_pControl) ) {
		
		return (RegCtrl(uPru, CTRL) & Bit(15)) ? false : true;
		}
	
	return false;
	}

bool CPru437::IsRunning(UINT uPru) const
{
	if( uPru < elements(m_pControl) ) {
		
		return (RegCtrl(uPru, CTRL) & Bit(15)) ? true : false;
		}
	
	return false;
	}

// Operations

void CPru437::SetMux(UINT uMux)
{
	RegCfg(PINMUX) &= ~0xFF;

	RegCfg(PINMUX) |= uMux;
	}

void CPru437::Run(UINT uPru)
{
	if( uPru < elements(m_pControl) ) {
		
		RegCtrl(uPru, CTRL) |= Bit(1);
		}
	}

void CPru437::Stop(UINT uPru)
{
	if( uPru < elements(m_pControl) ) {
		
		RegCtrl(uPru, CTRL) &= ~Bit(1);

		while( RegCtrl(uPru, CTRL) & Bit(15) );
		}
	}

void CPru437::Reset(UINT uPru)
{
	if( uPru < elements(m_pControl) ) {
		
		RegCtrl(uPru, CTRL) &= ~Bit(0);

		while( !(RegCtrl(uPru, CTRL) & Bit(0)) );
		}
	}

bool CPru437::LoadData(UINT uPru, PCBYTE pData, UINT uCount)
{
	return LoadData(uPru, pData, uCount, 0);	
	}

bool CPru437::LoadData(UINT uPru, PCBYTE pData, UINT uCount, UINT uAddr)
{
	UINT uMax = m_uUnit ? 8 * 1024 : 4 * 1024;

	if( uPru < elements(m_pControl) ) {
		
		if( !(uAddr & 0x3) && (uAddr + uCount) <= uMax ) {

			PDWORD  pDst = PDWORD(m_pData[uPru]) + uAddr / sizeof(DWORD);

			PCDWORD pSrc = PCDWORD(pData);

			uCount = (uCount + 3) / sizeof(DWORD);

			while( uCount-- ) {

				*pDst++ = *pSrc++;
				}

			return true;
			}
		}

	return false;
	}

bool CPru437::LoadCode(UINT uPru, PCBYTE pCode, UINT uCount)
{
	return LoadCode(uPru, pCode, uCount, 0);	
	}

bool CPru437::LoadCode(UINT uPru, PCBYTE pCode, UINT uCount, UINT uAddr)
{
	UINT uMax = m_uUnit ? 12 * 1024 : 4 * 1024;

	if( IsHalted(uPru) ) {
		
		if( !(uAddr & 0x3) && (uAddr + uCount) <= uMax ) {

			PDWORD  pDst = PDWORD(m_pCode[uPru]) + uAddr / sizeof(DWORD);

			PCDWORD pSrc = PCDWORD(pCode);

			uCount = (uCount + 3) / sizeof(DWORD);

			while( uCount-- ) {

				*pDst++ = *pSrc++;
				}

			return true;
			}
		}

	return false;
	}

bool CPru437::LoadCode(UINT uPru, PCTXT pCode)
{
	enum {
		typeData   = 0,
		typeEof    = 1,
		typeAddr   = 4,
		};

	enum {
		sizeCount  = 2,
		sizeAddr   = 4,
		sizeType   = 2,
		sizeData   = 8,
		sizeCheck  = 2,
		};

	enum {
		stateStart = 0,
		stateCount = 1,
		stateAddr  = 2,
		stateType  = 3,
		stateData  = 4,
		stateCheck = 5,
		};

	UINT uState = stateStart;

	UINT uType  = 0;

	UINT uAddr  = 0;

	UINT uScan  = 0;

	UINT uRead  = 0;

	UINT uCount = 0;

	UINT uCheck = 0;

	UINT uData  = 0;

	UINT uCode[8];

	while( *pCode ) {

		BYTE b = BYTE(*pCode++);

		if( b == ':' ) {

			uState = stateCount;

			uScan  = sizeCount;

			uData  = 0;

			uCheck = 0;

			continue;
			}

		if( uScan ) {

			if( b >= 'A' && b <= 'F') {

				b -= ('A' - 0xA);
				}
			else {
				b -= '0';
				}

			uData <<= 4;

			uData  |= b;

			uScan  -= 1;
			}

		if( uState != stateCheck ) {

			if( !(uScan & 1) ) {

				uCheck += BYTE(uData);
				}
			}

		if( uScan ) {

			continue;
			}

		switch( uState ) {

			case stateCount:

				uCount = uData / 4;

				uRead  = 0;

				uState = stateAddr;

				uScan  = sizeAddr;

				break;

			case stateAddr:

				uAddr &= 0xFFFF0000;

				uAddr |= WORD(uData);

				uState = stateType;

				uScan  = sizeType;

				break;

			case stateType:

				uType  = BYTE(uData);

				uState = stateData;

				uScan  = sizeData;

				uRead  = 0;

				break;

			case stateData:

				uCode[ uRead ++ ] = uData;

				if( uRead == uCount ) {

					uState = stateCheck;

					uScan  = sizeCheck;
					}
				else {
					uScan  = sizeData;
					}

				break;
		
			case stateCheck:

				uState = stateStart;

				if( LOBYTE(uData) == LOBYTE(~uCheck+1) ) {

					switch( uType ) {

						case typeEof:

							return true;

						case typeAddr:

							uAddr = uData << 16;

							break;

						case typeData:

							if( !LoadCode(uPru, PCBYTE(uCode), uCount * sizeof(DWORD), uAddr) ) {

								AfxTrace("PRU%d Code Load Failed\n", uPru);

								return false;
								}

							break;
						}

					break;
					}

				AfxTrace("PRU%d Bad CRC\n", uPru);

				return false;
			}
		}	
	
	return true;
	}

// Implementation

UINT CPru437::FindBase(UINT iIndex) const
{
	switch( iIndex ) {

		case 0 : return ADDR_PRU_ICSS0_BASE;
		case 1 : return ADDR_PRU_ICSS1_BASE;
		}

	return 0;
	}

// End of File
