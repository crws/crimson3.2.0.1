
#include "Intern.hpp"

#include "HalSitara.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ctrl437.hpp"

#include "Clock437.hpp"

#include "Watchdog437.hpp"

#include "Dmt437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Sitara
//

// Constructor

CHalSitara::CHalSitara(void)
{
	m_pEntropy   = PVDWORD(ADDR_DMTIMER2 + 0x3C);

	m_uDebugAddr = 0;

	m_uDebugLine = 0;

	m_uHead      = 0;

	m_uTail      = 0;

	m_pRead      = NULL;
	}

// Destructor

CHalSitara::~CHalSitara(void)
{
	}

// Initialization

void CHalSitara::Open(void)
{
	FindDebug();

	CArmHal::Open();

	CreateDevices();

	StartTimer();
	}

// IHal Timer Support

UINT CHalSitara::GetTickFraction(void)
{	
	return m_pDmt[0]->GetFraction();
	}

void CHalSitara::SpinDelayFast(UINT uTime)
{
	m_pDmt[1]->Delay(uTime);
	}

// IHal Debug Support

void CHalSitara::DebugReset(void)
{
	// Not sure this is needed on Aeon...
	}

UINT CHalSitara::DebugRead(UINT uDelay)
{
	if( m_uDebugAddr ) {

		if( !m_pRead ) {

			if( AfxNewObject("semaphore", ISemaphore, m_pRead) == S_OK ) {

				m_pPic->SetLineHandler(m_uDebugLine, this, 2);

				m_pPic->EnableLine    (m_uDebugLine, true);

				*m_pLine = Bit(0);

				*m_pMode = 0x00;
				}
			}

		if( m_pRead ) {

			if( m_pRead->Wait(uDelay) ) {

				BYTE bData = m_bBuff[m_uHead];

				m_uHead = (m_uHead + 1) % elements(m_bBuff);

				return bData;
				}

			return NOTHING;
			}
		}

	Sleep(uDelay);

	return NOTHING;
	}

void CHalSitara::DebugOut(char cData)
{
	if( m_uDebugAddr ) {

		if( cData == '\n' ) {

			while( !(*m_pTest & Bit(5)) ); 

			*m_pData = '\r';
			}

		while( !(*m_pTest & Bit(5)) ); 

		*m_pData = cData;
		}
	}

void CHalSitara::DebugWait(void)
{
	if( m_uDebugAddr ) {

		while( !(*m_pTest & Bit(6)) );
		}
	}

void CHalSitara::DebugStop(void)
{
	#if defined(_DEBUG)

	if( m_uDebugAddr ) {

		PCSTR p = "*** STOP ***\n";
	
		while( *p ) {
		
			DebugOut(*p++);
			}

		for( UINT e = 0; e < 2; ) {
		
			HostMaxIpr();

			while( (*m_pTest & Bit(0)) ) {

				if( BYTE(*m_pData) == 0x1B ) {

					e++;
					}
				}
		
			m_pDog->Kick();
			}
		}

	#endif

	for(;;) HostMaxIpr();
	}

void CHalSitara::DebugKick(void)
{
	m_pDog->Kick();
	}

// IEventSink

void CHalSitara::OnEvent(UINT uLine, UINT uParam)
{
	if( uParam == 1 ) {

		CArmHal::OnEvent(uLine, uParam);

		m_pDog->Kick();
		}

	if( uParam == 2 ) {

		UINT uData = 0;

		while( (*m_pTest & Bit(0)) ) {

			BYTE bData = BYTE(*m_pData);

			UINT uNext = (m_uTail + 1) % elements(m_bBuff);

			if( uNext != m_uHead ) {

				m_bBuff[m_uTail] = bData;

				m_uTail          = uNext;

				uData++;
				}
			}

		if( uData ) {

			m_pRead->Signal(uData);
			}
		}
	}

// Power Management Hooks

// TODO -- Implement !!!

// Implementation

void CHalSitara::FindDebug(void)
{
	PVBYTE p = PVBYTE (m_uDebugAddr);

	m_pData  = PVWORD(p + 0x00);

	m_pLine  = PVWORD(p + 0x04);

	m_pTest  = PVWORD(p + 0x14);

	m_pMode  = PVWORD(p + 0x20);
	}

void CHalSitara::CreateDevices(void)
{
	m_pPic    = Create_Gic437();

	m_pMmu    = Create_Mmu437(this);

	m_pCtrl   = New CCtrl437();

	m_pClock  = New CClock437(m_pCtrl);

	m_pDmt[0] = New CDmt437(m_pCtrl, 0);

	m_pDmt[1] = New CDmt437(m_pCtrl, 1);

	m_pDog    = New CWatchdog437();

	m_pDog->Enable(50);

	m_pDog->Kick();
	}

void CHalSitara::StartTimer(void)
{
	m_pClock->SetClockSource(CClock437::clockTIMER2, CClock437::sourceOSC);

	m_pClock->SetClockSource(CClock437::clockTIMER3, CClock437::sourceOSC);

	m_pClock->SetClockMode(CClock437::clockTIMER2, CClock437::modeSwWakup);

	m_pClock->SetClockMode(CClock437::clockTIMER3, CClock437::modeSwWakup);

	m_pDmt[0]->SetPeriodic(GetTimerResolution() * 1000);

	m_pDmt[1]->SetPeriodic(1);

	m_pDmt[0]->SetEventHandler(this, 1);

	m_pDmt[0]->EnableEvents();

	m_pPic->SetLinePriority(INT_TIMER2, 1); 
	}

// End of File
