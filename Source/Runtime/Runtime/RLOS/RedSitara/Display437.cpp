
#include "Intern.hpp"

#include "Display437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Dss437.hpp"

#include "Pwm437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Display Definitions
//

global bool GetDssConfig(CDss437Config &Config, UINT Display)
{
	switch( Display ) {

		case MODEL_CANYON_04:
		case MODEL_COLORADO_04:

			// OSD OSD043T2711-65TS
			Config.m_uBits	     = 24;
			Config.m_xDisp       = 480;
			Config.m_yDisp       = 272;
			Config.m_xSize       = 480;
			Config.m_ySize       = 272;
			Config.m_fFixAspect  = true;
			Config.m_uHorzFront  = 2;
			Config.m_uHorzSync   = 1;
			Config.m_uHorzBack   = 12;
			Config.m_uVertFront  = 1;
			Config.m_uVertSync   = 1;
			Config.m_uVertBack   = 12;
			Config.m_fInvertPCLK = false;
			Config.m_uFrames     = 60;

			break;

		case MODEL_CANYON_07:
		case MODEL_COLORADO_07:
			
			// OSD OSD070T2750-19
			Config.m_uBits	     = 18;
			Config.m_xDisp       = 800;
			Config.m_yDisp       = 480;
			Config.m_xSize       = 800;
			Config.m_ySize       = 480;
			Config.m_fFixAspect  = true;
			Config.m_uHorzFront  = 85;
			Config.m_uHorzSync   = 85;
			Config.m_uHorzBack   = 85;
			Config.m_uVertFront  = 15;
			Config.m_uVertSync   = 15;
			Config.m_uVertBack   = 15;
			Config.m_fInvertPCLK = true;
			Config.m_uFrames     = 60;

			break;
			
		case MODEL_CANYON_07EQ:
		case MODEL_COLORADO_07EQ:
			
			// OSD OSD070T2750-19 QVGA Emulation
			Config.m_uBits	     = 18;
			Config.m_xDisp       = 800;
			Config.m_yDisp       = 480;
			Config.m_xSize       = 320;
			Config.m_ySize       = 240;
			Config.m_fFixAspect  = true;
			Config.m_uHorzFront  = 85;
			Config.m_uHorzSync   = 85;
			Config.m_uHorzBack   = 85;
			Config.m_uVertFront  = 15;
			Config.m_uVertSync   = 15;
			Config.m_uVertBack   = 15;
			Config.m_fInvertPCLK = true;
			Config.m_uFrames     = 60;

			break;

		case MODEL_CANYON_10:
		case MODEL_COLORADO_10:

			// NEC NLB104SV01L-01
			Config.m_uBits	     = 24;
			Config.m_xDisp	     = 800;
			Config.m_yDisp       = 600;
			Config.m_xSize       = 800;
			Config.m_ySize       = 600;
			Config.m_fFixAspect  = true;
			Config.m_uHorzFront  = 75;
			Config.m_uHorzSync   = 74;
			Config.m_uHorzBack   = 75;
			Config.m_uVertFront  = 8;
			Config.m_uVertSync   = 9;
			Config.m_uVertBack   = 8;
			Config.m_fInvertPCLK = true;
			Config.m_uFrames     = 60;

			break;

		case MODEL_CANYON_10EV:
		case MODEL_COLORADO_10EV:

			// NEC NLB104SV01L-01 VGA Emulation
			Config.m_uBits	     = 24;
			Config.m_xDisp	     = 800;
			Config.m_yDisp       = 600;
			Config.m_xSize       = 640;
			Config.m_ySize       = 480;
			Config.m_fFixAspect  = true;
			Config.m_uHorzFront  = 75;
			Config.m_uHorzSync   = 74;
			Config.m_uHorzBack   = 75;
			Config.m_uVertFront  = 8;
			Config.m_uVertSync   = 9;
			Config.m_uVertBack   = 8;
			Config.m_fInvertPCLK = true;
			Config.m_uFrames     = 60;

			break;

		case MODEL_CANYON_15:

			// NEC NL10276AC30-48D
			Config.m_uBits	     = 24;
			Config.m_xDisp       = 1024;
			Config.m_yDisp       = 768;
			Config.m_xSize       = 1024;
			Config.m_ySize       = 768;
			Config.m_fFixAspect  = true;
			Config.m_uHorzFront  = 117;
			Config.m_uHorzSync   = 117;
			Config.m_uHorzBack   = 117;
			Config.m_uVertFront  = 12;
			Config.m_uVertSync   = 13;
			Config.m_uVertBack   = 12;
			Config.m_fInvertPCLK = true;
			Config.m_uFrames     = 50;

			break;

		default:
			return false;
		}

	UINT fps = Config.m_uFrames;

	UINT tpx = Config.m_xDisp + Config.m_uHorzFront + Config.m_uHorzBack + Config.m_uHorzSync;

	UINT tpy = Config.m_yDisp + Config.m_uVertFront + Config.m_uVertBack + Config.m_uVertSync;

	Config.m_uFrequency = fps * tpx * tpy;

	return true;
	}

//////////////////////////////////////////////////////////////////////////
//
// AM437 Display Driver
//

// Instantiator

IDevice * Create_Display437(UINT uModel, CDss437 *pDss, CPwm437 *pPwm, UINT uChan)
{
	CDisplay437 *pDevice = New CDisplay437(uModel, pDss, pPwm, uChan);

	pDevice->Open();

	return (IDevice *)(IDisplay *) pDevice;
	}

// Constructor

CDisplay437::CDisplay437(UINT uModel, CDss437 *pDss, CPwm437 *pPwm, UINT uPwm)
{
	m_pPwmBacklight   = pPwm;

	m_uPwmBacklight   = uPwm;

	m_pDss            = pDss;

	m_uType           = uModel;

	m_uGpioBacklight  = 19;

	m_uGpioVoltage    = 4;

	AfxGetObject("gpio", 1, IGpio, m_pGpioBacklight);

	AfxGetObject("gpio", 5, IGpio, m_pGpioVoltage);

	FindDisplay();
	}

// Destructor

CDisplay437::~CDisplay437(void)
{
	m_pGpioBacklight->Release();

	m_pGpioVoltage->Release();
	}

// IDevice

BOOL METHOD CDisplay437::Open(void)
{
	if( CDisplaySoftPoint::Open() ) {

		m_pPwmBacklight->SetFreq(FindFrequency(m_uType));

		ConfigBacklight();

		EnableBacklight(true);

		SetBacklight(m_nBacklight = LoadBacklight(100));

		return TRUE;
		}

	return FALSE;
	}

// IDisplay

void METHOD CDisplay437::Update(PCVOID pData)
{
	Claim();

	if( m_pBuff ) {

		LockPointer(true);

		SavePointer(PDWORD(pData));

		DrawPointer(PDWORD(pData));

		ArrayCopy(m_pBuff, PDWORD(pData), m_cx * m_cy);

		LoadPointer(PDWORD(pData));

		LockPointer(false);
		}

	Free();
	}

BOOL METHOD CDisplay437::SetBacklight(UINT nPercent)
{
	m_nBacklight = nPercent;

	m_pPwmBacklight->SetDuty(m_uPwmBacklight, m_nBacklight);

	SaveBacklight(m_nBacklight);

	return TRUE;
	}

UINT METHOD CDisplay437::GetBacklight(void)
{
	return m_nBacklight;
	}

BOOL METHOD CDisplay437::EnableBacklight(BOOL fOn)
{
	m_fBacklight = fOn;

	switch( m_uType ) {
	
		case MODEL_CANYON_04:
		case MODEL_CANYON_07:
		case MODEL_CANYON_07EQ:
		case MODEL_COLORADO_04:
		case MODEL_COLORADO_07:
		case MODEL_COLORADO_07EQ:

			m_pPwmBacklight->SetDuty(m_uPwmBacklight, fOn ? m_nBacklight : 0);

			return TRUE;
			
		case MODEL_CANYON_10:
		case MODEL_CANYON_10EV:
		case MODEL_CANYON_15:
		case MODEL_COLORADO_10:
		case MODEL_COLORADO_10EV:

			m_pGpioBacklight->SetState(m_uGpioBacklight, fOn);

			m_pGpioVoltage->SetState(m_uGpioVoltage, fOn);

			return TRUE;
		}

	return FALSE;
	}

BOOL METHOD CDisplay437::IsBacklightEnabled(void)
{
	return m_fBacklight;
	}

// Implementation

void CDisplay437::FindDisplay(void)
{
	CDss437Config Config;

	if( GetDssConfig(Config, m_uType) ) {

		m_cx    = Config.m_xSize;
	
		m_cy    = Config.m_ySize;

		m_pBuff = PDWORD(memalign(32, m_cx * m_cy * 4));

		ArrayZero(m_pBuff, m_cx * m_cy);

		m_pDss->SetFrame(PBYTE(m_pBuff));

		m_pDss->Configure(Config);
		}
	}

UINT CDisplay437::FindFrequency(UINT uModel)
{
	switch( uModel ) {

		case MODEL_CANYON_10:
		case MODEL_CANYON_10EV:
		case MODEL_COLORADO_10:
		case MODEL_COLORADO_10EV:

			return 5000;
		}
	
	return 20000;
	}

void CDisplay437::ConfigBacklight(void)
{
	switch( m_uType ) {
	
		case MODEL_CANYON_04:

			break;
		
		case MODEL_CANYON_07:
		case MODEL_CANYON_07EQ:
		case MODEL_COLORADO_04:
		case MODEL_COLORADO_07:
		case MODEL_COLORADO_07EQ:
		case MODEL_CANYON_10:
		case MODEL_CANYON_10EV:
		case MODEL_CANYON_15:
		case MODEL_COLORADO_10:
		case MODEL_COLORADO_10EV:

			m_pGpioBacklight->SetState(m_uGpioBacklight, true);

			m_pGpioVoltage->SetState(m_uGpioVoltage, true);

			break;
		}
	}

// End of File
