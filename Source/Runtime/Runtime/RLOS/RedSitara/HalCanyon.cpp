
#include "Intern.hpp"

#include "HalCanyon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Canyon HMI
//

// Instantiator

bool Create_HalCanyon(void)
{
	(New CHalCanyon)->Open();

	return true;
	}

// Constructor

CHalCanyon::CHalCanyon(void)
{
	m_uDebugAddr = ADDR_UART4;

	m_uDebugLine = INT_UART4;
	}

// End of File
