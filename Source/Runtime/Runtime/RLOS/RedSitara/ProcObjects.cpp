
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern bool      Create_HalCanyon(void);

extern bool      Create_HalColorado(void);

extern bool	 Create_HalManticore(void);

extern IDevice * Create_PlatformCanyonCmos(UINT uModel);

extern IDevice * Create_PlatformCanyonLvds(UINT uModel);

extern IDevice * Create_PlatformColorado(UINT uModel);

extern IDevice * Create_PlatformManticore(UINT uModel);

//////////////////////////////////////////////////////////////////////////
//
// Sitara Processor Objects
//

// Externals

extern UINT AeonFindModel(void);

// Data

static UINT m_uModel = 0;

// Code

global bool Create_Hal(void)
{
	m_uModel = AeonFindModel();

	switch( m_uModel ) {

		case MODEL_COLORADO_04:
		case MODEL_COLORADO_07:
		case MODEL_COLORADO_07EQ:
		case MODEL_COLORADO_10:
		case MODEL_COLORADO_10EV:

			return Create_HalColorado();

		case MODEL_CANYON_04:
		case MODEL_CANYON_07:
		case MODEL_CANYON_07EQ:
		case MODEL_CANYON_10:
		case MODEL_CANYON_10EV:
		case MODEL_CANYON_15:

			return Create_HalCanyon();

		case MODEL_MANTICORE_50:
		case MODEL_MANTICORE_70:

			return Create_HalManticore();
		}

	return false;
	}

global IDevice * Create_Platform(void)
{
	AfxTrace("Model is %u\n", m_uModel);

	switch( m_uModel ) {

		case MODEL_COLORADO_04:
		case MODEL_COLORADO_07:
		case MODEL_COLORADO_07EQ:
		case MODEL_COLORADO_10:
		case MODEL_COLORADO_10EV:

			return Create_PlatformColorado(m_uModel);

		case MODEL_CANYON_04:

			return Create_PlatformCanyonCmos(m_uModel);

		case MODEL_CANYON_07:
		case MODEL_CANYON_07EQ:
		case MODEL_CANYON_10:
		case MODEL_CANYON_10EV:
		case MODEL_CANYON_15:

			return Create_PlatformCanyonLvds(m_uModel);

		case MODEL_MANTICORE_50:
		case MODEL_MANTICORE_70:

			return Create_PlatformManticore(m_uModel);
		}

	for(;;);
	}

// End of File
