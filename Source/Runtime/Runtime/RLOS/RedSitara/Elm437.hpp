
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Elm437_HPP
	
#define	INCLUDE_AM437_Elm437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 Error Location Module
//

class CElm437 : public IEventSink
{
	public:
		// Constructor
		CElm437(void);

		// Attributes
		UINT GetLine(void) const;

		// Operations
		void Reset(void);
		void SetAutoGating(bool fSet);
		void SetBchEccLevel(UINT uBits);
		void SetBchBuffSize(UINT uSize);
		void SetPageMode(UINT uMode);
		void SetSyndromeFrag(UINT uPolyNum, UINT uFragId, UINT uFragData);
		void SetSyndromeStart(UINT uPolyNum);
		bool GetErrorStatus(UINT uPolyNum);
		UINT GetErrorCount(UINT uPolyNum);
		UINT GetErrorAddr(UINT uPolyNum, UINT iError);
		void SetCompHook(UINT i, IEventSink *pSink);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// Enum Syndrome Poly Mask
		enum
		{
			poly0	= Bit(0),
			poly1	= Bit(1),
			poly2	= Bit(2),
			poly3	= Bit(3),
			poly4	= Bit(4),
			poly5	= Bit(5),
			poly6	= Bit(6),
			poly7	= Bit(7),
			};

	protected:
		// Registers
		enum
		{
			regREV		= 0x0000 / sizeof(DWORD),
			regSYSCFG	= 0x0010 / sizeof(DWORD),
			regSYSSTS	= 0x0014 / sizeof(DWORD),
			regIRQSTS	= 0x0018 / sizeof(DWORD),
			regIRQEN	= 0x001C / sizeof(DWORD),
			regLOCCFG	= 0x0020 / sizeof(DWORD),
			regPAGECTRL	= 0x0080 / sizeof(DWORD),
			regSYNDROME	= 0x0400 / sizeof(DWORD),
			regLOCSTS	= 0x0800 / sizeof(DWORD),
			regERROR	= 0x0880 / sizeof(DWORD),
			};

		// Data Members
		PVDWORD      m_pBase;
		IEventSink * m_pHook[8];
		UINT 	     m_uLine;

		// Implementation
		void Init(void);
		void EnableEvents(void);
	};

// End of File

#endif
