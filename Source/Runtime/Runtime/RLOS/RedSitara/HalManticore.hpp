
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_HalManticore_HPP

#define INCLUDE_HalManticore_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HalSitara.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Manticore
//

class CHalManticore : public CHalSitara
{
	public:
		// Constructor
		CHalManticore(void);
	};

// End of File

#endif
