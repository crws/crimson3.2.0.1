
#include "Intern.hpp"

#include "HalColorado.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Colorado HMI
//

// Instantiator

bool Create_HalColorado(void)
{
	(New CHalColorado)->Open();

	return true;
	}

// Constructor

CHalColorado::CHalColorado(void)
{
	m_uDebugAddr = ADDR_UART4;

	m_uDebugLine = INT_UART4;
	}

// End of File
