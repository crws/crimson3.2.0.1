
#include "Intern.hpp"

#include "Mspi437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Multichannel Serial Port Interface Module
//

// Register Access

#define Reg(x)       (m_pBase[reg##x])

#define RegN(x, n)   (m_pBase[reg##x + 5 * n])

// Instantiator

IDevice * Create_SpiInterface437(CClock437 *pClock)
{
	CMspi437 *pDevice = New CMspi437(pClock);

	pDevice->Open();

	return (IDevice *) pDevice;
	}

// Constructor

CMspi437::CMspi437(CClock437 *pClock)
{
	StdSetRef();

	m_pBase  = PVDWORD(ADDR_MCSPI2);

	m_uLine  = INT_SPI2;

	m_uClock = pClock->GetPerM2Freq() / 4;

	m_pReq   = NULL;

	m_uHead  = 0;
	
	m_uTail  = 0;

	m_pEvent = Create_AutoEvent();

	m_pMutex = Create_Qutex();
	}

// Destructor

CMspi437::~CMspi437(void)
{
	m_pEvent->Release();

	m_pMutex->Release();
	}

// IUnknown

HRESULT CMspi437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ISpi);

	return E_NOINTERFACE;
	}

ULONG CMspi437::AddRef(void)
{
	StdAddRef();
	}

ULONG CMspi437::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CMspi437::Open(void)
{
	Reset();

	InitController();

	InitEvents();
	
	return TRUE;
	}
		
// ISpi

BOOL METHOD CMspi437::Init(UINT uChan, CSpiConfig const &Config)
{
	if( uChan < 4 ) {

		WORD wDiv = FindDivider(Config.m_uFreq);

		BYTE bHi  = HIBYTE(wDiv) & 0xFF;

		BYTE bLo  = LOBYTE(wDiv) & 0x0F;

		BOOL fExt = wDiv & Bit(7);

		UINT uCfg = bitForce | bitNoTxD0 | (bLo << bitClockDivLo);

		uCfg |= (fExt ? bitClkExt : 0); 

		uCfg |= (Config.m_uSelect ? 0 : bitChipSelPol);
		
		uCfg |= (Config.m_uClockPol ? 0 : bitClockPol);

		uCfg |= (Config.m_uClockPha ? 0 : bitClockPha);
		
		RegN(CHCTRL, uChan) = (bHi << bitClockDivHi);

		RegN(CHCONF, uChan) = uCfg;

		return true;
		}

	return false;
	}

BOOL METHOD CMspi437::Send(UINT uChan, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData)
{
	CRequest Req;

	Req.m_uChan = uChan;
	Req.m_pHead = PBYTE(pHead);
	Req.m_uHead = uHead;
	Req.m_pData = PBYTE(pData);
	Req.m_uData = uData;
	Req.m_uMode = modeTx;

	return Request(&Req);
	}

BOOL METHOD CMspi437::Recv(UINT uChan, PCBYTE pHead, UINT uHead, PBYTE pData, UINT uData)
{
	CRequest Req;

	Req.m_uChan = uChan;
	Req.m_pHead = PBYTE(pHead);
	Req.m_uHead = uHead;
	Req.m_pData = PBYTE(pData);
	Req.m_uData = uData;
	Req.m_uMode = uHead ? modeTxRx : modeRx;

	return Request(&Req);
	}

// IEventSink

void CMspi437::OnEvent(UINT uLine, UINT uParam)
{
	for(;;) {

		DWORD Data = Reg(IRQSTS) & Reg(IRQEN);

		if( Data ) {

			if( Data & intEow ) {

				Reg(IRQSTS) = intEow;
				
				OnTransfer();

				continue;
				}
			
			if( Data & intRxData ) {

				Reg(IRQSTS) = intRxData;
				
				OnRxData();

				continue;
				}

			if( Data & intTxEmpty ) {

				Reg(IRQSTS) = intTxEmpty;

				OnTxEmpty();

				continue;
				}

			if( Data & intTxUnder ) {

				Reg(IRQSTS) = intTxUnder;
				
				OnTxUnder();

				continue;
				}

			if( Data & intRxOver ) {

				Reg(IRQSTS) = intRxOver;

				OnRxOver();

				continue;
				}

			Reg(IRQSTS) = Data;

			continue;
			}

		break;
		}
	}

// Event Handlers

void CMspi437::OnTransfer(void)
{
	Reg(IRQEN) &= ~((intTx0Under | intTx0Empty) << (4 * m_pReq->m_uChan));

	ReadFifo();

	RegN(CHCONF, m_pReq->m_uChan) &= ~bitForce;

	RegN(CHCONF, m_pReq->m_uChan) &= ~bitFifoRxEn;
	
	RegN(CHCONF, m_pReq->m_uChan) &= ~bitFifoTxEn;

	RegN(CHCTRL, m_pReq->m_uChan) &= ~bitChanEn;

	CommsDone(m_pReq);

	if( m_uHead == m_uTail ) {

		m_pReq = NULL;
		}
	else {
		m_pReq  = m_pList[m_uHead];

		m_uHead = (m_uHead + 1) % elements(m_pList);

		StartComms();
		}
	}

void CMspi437::OnRxOver(void)
{
	AfxTrace("CMspi437 RX Overrun Chan %d\n", m_pReq->m_uChan);
	}

void CMspi437::OnRxData(void)
{
	ReadFifo();
	}

void CMspi437::OnTxUnder(void)
{
	AfxTrace("CMspi437 TX Underrun Chan %d\n", m_pReq->m_uChan);
	}

void CMspi437::OnTxEmpty(void)
{
	WriteFifo();
	}

// Implementation

void CMspi437::InitController(void)
{
	Reg(IRQEN)   = intEow;
		       
	Reg(IRQSTS)  = 0x0003777F;
		       
	Reg(CTRL)    = 0x00000001;

	Reg(XFERLEV) = MAKEWORD(15, 15);
	}

void CMspi437::InitEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);
	}

bool CMspi437::Request(CRequest *pReq)
{
	if( pReq->m_uChan < 4 ) {
		
		InitWait(pReq);

		UINT uSave;

		phal->EnableLineViaIrql(m_uLine, uSave, false);

		if( m_pReq == NULL ) {

			m_pReq = pReq;

			StartComms();

			phal->EnableLineViaIrql(m_uLine, uSave, true);
			}
		else {
			UINT uNext = (m_uTail + 1) % elements(m_pList);

			if( uNext == m_uHead ) {

				AfxTrace("CMspi437: Queue Full\n");

				phal->EnableLineViaIrql(m_uLine, uSave, true);

				TermWait(pReq);

				return false;
				}

			m_pList[m_uTail] = pReq;

			m_uTail		 = uNext;

			phal->EnableLineViaIrql(m_uLine, uSave, true);
			}

		WaitDone(pReq);
			
		return true;
		}
	
	return false;
	}

void CMspi437::StartComms(void)
{
	UINT Mask = 0;
	
	switch( m_pReq->m_uMode ) {

		case modeTxRx :

			Mask = intRx0Over | intRx0Full | intTx0Under | intTx0Empty;

			break;

		case modeRx:

			Mask = intRx0Over | intRx0Full;

			break;

		case modeTx: 

			Mask = intTx0Under | intTx0Empty;

			break;
		}

	Mask <<= (4 * m_pReq->m_uChan);
	
	DWORD Config  = RegN(CHCONF, m_pReq->m_uChan);

	Reg(IRQEN)   &= ~Mask;

	Reg(IRQSTS)   = Mask;

	Reg(IRQEN)   |= Mask;

	Reg(XFERLEV) &= 0x0000FFFF;

	Reg(XFERLEV) |= (m_pReq->m_uHead + m_pReq->m_uData) << 16;

	Config |= bitFifoRxEn;
	
	Config |= bitFifoTxEn;
	
	Config |= bitForce;

	Config |= bitTurbo;

	Config &= ~(0x1F << bitWordLen);
	
	Config |= ((8-1) << bitWordLen);
	
	Config &= ~(0x03 << bitMode);
	
	Config |= (m_pReq->m_uMode << bitMode);
	
	RegN(CHCONF, m_pReq->m_uChan)  = Config;

	RegN(CHCTRL, m_pReq->m_uChan) |= bitChanEn;

	m_pReq->m_uSend = 0;

	m_pReq->m_uRecv = 0;

	WriteFifo();
	}

void CMspi437::WriteFifo(void)
{
	if( m_pReq->m_uMode == modeTxRx || m_pReq->m_uMode == modeTx ) {

		if( m_pReq->m_uSend < m_pReq->m_uHead ) {
		
			PBYTE pData = m_pReq->m_pHead + m_pReq->m_uSend;

			UINT  uSize = m_pReq->m_uHead - m_pReq->m_uSend;
		
			m_pReq->m_uSend += WriteFifo(pData, uSize);
			}

		if( m_pReq->m_uSend >= m_pReq->m_uHead ) {

			UINT  uData = m_pReq->m_uSend - m_pReq->m_uHead;

			PBYTE pData = m_pReq->m_pData + uData;

			UINT  uSize = m_pReq->m_uData - uData;
		
			if( m_pReq->m_uMode == modeTx ) {

				m_pReq->m_uSend += WriteFifo(pData, uSize);
				}

			if( m_pReq->m_uMode == modeTxRx ) {

				m_pReq->m_uSend += WriteFifo(NULL, uSize);
				}
			}
		}

	if( m_pReq->m_uMode == modeRx ) {

		if( !m_pReq->m_uSend ) {

			m_pReq->m_uSend += WriteFifo(NULL, 1);
			}
		}
	}

void CMspi437::ReadFifo(void)
{
	if( m_pReq->m_uMode == modeTxRx ) {

		if( m_pReq->m_uRecv < m_pReq->m_uHead ) {
		
			UINT uSize = m_pReq->m_uHead - m_pReq->m_uRecv;
		
			m_pReq->m_uRecv += ReadFifo(NULL, uSize);
			}
		}

	if( m_pReq->m_uMode == modeTxRx || m_pReq->m_uMode == modeRx ) {

		if( m_pReq->m_uRecv >= m_pReq->m_uHead ) {

			UINT  uData = m_pReq->m_uRecv - m_pReq->m_uHead;

			PBYTE pData = m_pReq->m_pData + uData;

			UINT  uSize = m_pReq->m_uData - uData;
		
			m_pReq->m_uRecv += ReadFifo(pData, uSize);
			}
		}
	}

UINT CMspi437::WriteFifo(PBYTE pData, UINT uCount)
{
	DWORD volatile &Stat = RegN(CHSTAT, m_pReq->m_uChan);

	DWORD volatile &Data = RegN(TXDATA, m_pReq->m_uChan);

	UINT uSend = 0;
	
	while( uSend < uCount ) {

		if( !(Stat & bitTxFifoFull) ) {

			while( !(Stat & bitTxEmpty) );

			Data = pData ? *pData++ : 0x00;

			uSend ++;
			
			continue;
			}
		
		break;
		}

	return uSend;
	}

UINT CMspi437::ReadFifo(PBYTE pData, UINT uCount)
{
	DWORD volatile &Stat = RegN(CHSTAT, m_pReq->m_uChan);

	DWORD volatile &Data = RegN(RXDATA, m_pReq->m_uChan);

	UINT uRecv = 0;
	
	while( uRecv < uCount ) {

		if( Stat & bitRxFull ) {

			DWORD d = Data;

			if( pData ) {

				*pData++ = LOBYTE(LOWORD(d));
				}

			uRecv ++;
			
			continue;
			}
		
		break;
		}

	return uRecv;
	}

void CMspi437::InitWait(CRequest *pReq)
{
	bool fWait = true;

	if( Hal_GetIrql() >= IRQL_DISPATCH ) {

		fWait = false;
		}

	else if( !GetCurrentThread() ) {

		fWait = false;
		}

	else if( GetCurrentThread()->GetIndex() <= 1 ) {

		fWait = false;
		}

	if( fWait ) {

		m_pMutex->Wait(FOREVER);

		pReq->m_fPoll = false;
		}
	else {
		pReq->m_fPoll = true;

		pReq->m_fDone = false;
		}
	}

void CMspi437::TermWait(CRequest *pReq)
{
	if( !pReq->m_fPoll ) {

		m_pMutex->Free();
		}
	}

void CMspi437::WaitDone(CRequest *pReq)
{
	if( pReq->m_fPoll ) {

		while( !pReq->m_fDone );
		}
	else {
		m_pEvent->Wait(FOREVER);

		m_pMutex->Free();

		CheckThreadCancellation();
		}
	}

void CMspi437::CommsDone(CRequest *pReq)
{
	if( pReq->m_fPoll ) {

		pReq->m_fDone = true;
		}
	else {
		m_pEvent->Set();
		}
	}

void CMspi437::Reset(void)
{
	Reg(SYSCONFIG) |= Bit(1);

	while( !(Reg(SYSSTS) & Bit(0)) );
	}

WORD CMspi437::FindDivider(UINT uFreq)
{
	UINT uDiv = (m_uClock + uFreq - 1) / uFreq;

	if( uDiv > 4096 ) {

		for( UINT i = 0; i < 16; i ++ ) {

			if( uDiv <= (1 << i) ) {

				return i;
				}
			}

		return 15;
		}

	uDiv --;

	BYTE bHi = uDiv >> 4;

	BYTE bLo = uDiv & 0xF;

	return MAKEWORD(Bit(7) | bLo, bHi);
	}

// End of File
