
#include "Intern.hpp"

#include "Dss437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader()

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Display Subsystem
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CDss437::CDss437(CClock437 *pClock)
{
	m_pBase    = PVDWORD(ADDR_DSS_DISPC);

	m_uLine    = INT_DSS;

	m_uClock   = pClock->GetDispFreq();

	m_fError   = FALSE;

	m_uFrames1 = 0;
	
	m_uSample1 = 0;

	m_uFrames2 = 0;
	
	m_uSample2 = 0;

	m_dwFrame  = 0;

	m_pTimer   = CreateTimer();
	}

// Destructor

CDss437::~CDss437(void)
{
	m_pTimer->Release();
	}

// IEventSink

void CDss437::OnEvent(UINT uLine, UINT uParam)
{
	if( uParam == 1 ) {

		DWORD Mask = Reg(IRQSTS);

		if( Mask & Bit(10) ) { 
		
			m_fError = TRUE;
			}

		if( Mask & Bit(1) ) {

			m_uFrames1++;

			m_uFrames2++;
			}

		Reg(IRQSTS) = Mask;
		}

	if( uParam == 2 ) {

		if( m_fError ) {

			#if defined(_DEBUG)

			static UINT uCount = 0;

			AfxTrace("Dss437: underlow error #%u\n", ++uCount);

			#endif

			m_fError = FALSE;
			}

		if( false ) {

			if( ++m_uSample1 == 100 ) {

				AfxTrace("Dss437: %u.%2.2u fps\n", m_uFrames1 / 100, m_uFrames1 % 100);

				m_uFrames1 = 0;

				m_uSample1 = 0;

				m_uFrames2 = 0;

				m_uSample2 = 0;
				}
			else {
				if( ++m_uSample2 == 10 ) {

					AfxTrace("Dss437: %u.%1.1u  fps\n", m_uFrames2 / 10, m_uFrames2 % 10);

					m_uFrames2 = 0;

					m_uSample2 = 0;
					}
				}
			}
		}
	}

// Configuration

void CDss437::Configure(CDss437Config const &Config)
{
	m_Display = Config;

	Reset();

	Init();

	Reg(IRQSTS) = 0xFFFFFFFF;

	Reg(IRQEN)  = Bit(10) | Bit(1);

	EnableEvents();
	}

void CDss437::SetFrame(PBYTE pFrameBuf)
{
	m_dwFrame = phal->VirtualToPhysical(phal->GetNonCachedAlias(DWORD(pFrameBuf)));
	}

// Implementation

void CDss437::Init(void)
{
	Reg(CTRL) = 0x08018008;

	switch( m_Display.m_uBits ) {

		case 12: Reg(CTRL) |= 0x000; break;
		case 16: Reg(CTRL) |= 0x100; break;
		case 18: Reg(CTRL) |= 0x200; break;
		case 24: Reg(CTRL) |= 0x300; break;
		}

	if( m_Display.m_fInvertPCLK ) {

		Reg(POL_FREQ) |= Bit(14);
		}

	Reg(SIZE_LCD) = ((m_Display.m_yDisp-1) << 16) | (m_Display.m_xDisp-1);

	Reg(TIMING_H) = ((m_Display.m_uHorzBack -1) << 20) | 
			((m_Display.m_uHorzFront-1) <<  8) |
			((m_Display.m_uHorzSync -1) <<  0);

	Reg(TIMING_V) = ((m_Display.m_uVertBack  ) << 20) |
			((m_Display.m_uVertFront ) <<  8) |
			((m_Display.m_uVertSync-1) <<  0);

	Reg(SYSCFG)   = Bit(12);

	Reg(CFG)      = Bit(14);

	UINT uDivide  = (m_uClock + 50000) / m_Display.m_uFrequency;

	Reg(DIVISOR)  = (1 << 16) | (uDivide & 0xFF);

	SetVideo1();

	Reg(CTRL) |= Bit(0);	

	Commit();

	if( m_Display.m_uBits < 24 ) {

		SetDither(2);
		}
	}

void CDss437::SetVideo1(void)
{
	// Set base address and basic information.

	Reg(VID1_BA_0)  = m_dwFrame;

	Reg(VID1_ATTRS) = 0x00820018;

	// Figure out the x and y scale factors.
	
	double sx = double(m_Display.m_xDisp) / double(m_Display.m_xSize);
	
	double sy = double(m_Display.m_yDisp) / double(m_Display.m_ySize);

	// Adjust in order to maintain aspect ratio if required.

	sx = m_Display.m_fFixAspect ? Min(sx, sy) : sx;

	sy = m_Display.m_fFixAspect ? Min(sx, sy) : sy;

	// Figure out the picture size and position.

	UINT px = m_Display.m_xSize;

	UINT py = m_Display.m_ySize;

	UINT dx = UINT(m_Display.m_xSize * sx);

	UINT dy = UINT(m_Display.m_ySize * sy);

	UINT xp = (m_Display.m_xDisp - dx) / 2;

	UINT yp = (m_Display.m_yDisp - dy) / 2;

	// Configure controller per the above values.

	Reg(VID1_SIZE)         = ((dy-1) << 16) | (dx-1);

	Reg(VID1_PICTURE_SIZE) = ((py-1) << 16) | (px-1);

	Reg(VID1_POSITION)     = ((yp-0) << 16) | (xp-0);

	// Check whether to enable image up-scaling.

	if( sx > 1.0 || sy > 1.0 ) {

		// Load the controller's scale factors.

		UINT fx = UINT(1024 / sx);

		UINT fy = UINT(1024 / sy);

		Reg(VID1_FIR)    = (fy << 16) | fx;

		// Clear the phase accumulators.

		Reg(VID1_ACCU_0) = 0;
		
		Reg(VID1_ACCU_1) = 0;

		// Default Filter Coefficients.

		static int v[8][3] = {

			// 3-Tap Vertical Up-Scaling (From Table 13-35)

			{  0, 128,  0 },
			{  3, 123,  2 },
			{ 12, 111,  5 },
			{ 32,  89,  7 },
			{  0,  64, 64 },
			{  7,  89, 32 },
			{  5, 111, 12 },
			{  2, 123,  3 },

			};

		static int h[8][5] = {

			#if 0

			// 3-Tap Horizontal Up-Scaling (Based on Table 13-35)

			{ 0,  0, 128,  0, 0 },
			{ 0,  3, 123,  2, 0 },
			{ 0, 12, 111,  5, 0 },
			{ 0, 32,  89,  7, 0 },
			{ 0,  0,  64, 64, 0 },
			{ 0,  7,  89, 32, 0 },
			{ 0,  5, 111, 12, 0 },
			{ 0,  2, 123,  3, 0 },

			#else

			// 5-Tap Horizontal Up-Scaling (From Table 13-37)

			{  0,   0, 128,   0,  0 },
			{ -1,  13, 124,  -8,  0 },
			{ -2,  30, 112, -11, -1 },
			{ -5,  51,  95, -11, -2 },
			{  0,  -9,  73,  73, -9 },
			{ -2, -11,  95,  51, -5 },
			{ -1, -11, 112,  30, -2 }, 
			{  0, - 8, 124,  13,  1 },

			#endif

			};

		// Address the start of the coefficient bank.

		PVDWORD pd = &Reg(VID1_FIR_COEF_H_0);

		// Are we scaling by an integral factor?

		bool    is = (sx == INT(sx) && sy == INT(sy));

		// Loop around each phase of the table.

		for( int p = 0; p < 8; p++ ) {

			// Use first phase repeatedly if we are
			// scaling by an integral factor. This
			// just duplicates pixels without trying
			// to blend adjacent vaues. Otherwise,
			// load the entire table per the manual.

			int s = is ? 0 : p;

			*pd++ = (BYTE(h[s][4]) <<  0) |
			        (BYTE(h[s][3]) <<  8) |
			        (BYTE(h[s][2]) << 16) |
			        (BYTE(h[s][1]) << 24) ;

			*pd++ = (BYTE(h[s][0]) <<  0) |
			        (BYTE(v[s][2]) <<  8) |
			        (BYTE(v[s][1]) << 16) |
			        (BYTE(v[s][0]) << 24) ;
			}

		// Enable scaling.

		Reg(VID1_ATTRS) |= Bit(5) | Bit(6);
		}

	// Enable the channel.

	Reg(VID1_ATTRS) |= Bit(0);
	}

void CDss437::Reset(void)
{
	if( Reg(CTRL) & Bit(0) ) {

		Reg(CTRL)   &= ~Bit(0);

		Reg(IRQSTS) |=  Bit(0);

		while( (Reg(IRQSTS) & Bit(0)) == 0 ) ;
		}

	Reg(SYSCFG) |= Bit(1);

	while( (Reg(SYSSTS) & Bit(0)) == 0 ) ;	
	}

void CDss437::Commit(void)
{
	while( (Reg(CTRL) & Bit(5)) );

	Reg(CTRL) |= Bit(5);
	}

void CDss437::SetDither(UINT uMode)
{
	if( true ) {

		Reg(CTRL) &= ~Bit(7);

		Commit();
		}

	if( uMode ) {

		Reg(CTRL) &= 0x3FFFFFFF;

		Reg(CTRL) |= ((uMode-1) % 3) << 30;

		Reg(CTRL) |= Bit(7);

		Commit();
		}
	}

void CDss437::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 1);

	phal->EnableLine(m_uLine, true);

	m_pTimer->SetPeriod(1000);

	m_pTimer->SetHook(this, 2);

	m_pTimer->Enable(true);
	}

// End of File
