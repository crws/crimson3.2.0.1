
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Display437_HPP
	
#define	INCLUDE_AM437_Display437_HPP

///////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DisplaySoftPoint.hpp"

///////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPwm437;
class CDss437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 Display Driver
//

class CDisplay437 : public CDisplaySoftPoint
{	
	public:
		// Constructor
		CDisplay437(UINT uModel, CDss437 *pDss, CPwm437 *pPwm, UINT uPwm);

		// Destructor
		~CDisplay437(void);

		// IDevice
		BOOL METHOD Open(void);

		// IDisplay
		void METHOD Update(PCVOID pData);
		BOOL METHOD SetBacklight(UINT pc);
		UINT METHOD GetBacklight(void);
		BOOL METHOD EnableBacklight(BOOL fOn);
		BOOL METHOD IsBacklightEnabled(void);

	protected:
		// Data Members
		UINT        m_uType;
		bool	    m_fBacklight;
		int         m_nBacklight;
		CDss437   * m_pDss;
		CPwm437   * m_pPwmBacklight;
		UINT        m_uPwmBacklight;
		IGpio     * m_pGpioVoltage;
		UINT        m_uGpioVoltage;
		IGpio     * m_pGpioBacklight;
		UINT        m_uGpioBacklight;

		// Implementation
		void FindDisplay(void);
		UINT FindFrequency(UINT uModel);
		void ConfigBacklight(void);
	};

// End of File

#endif
