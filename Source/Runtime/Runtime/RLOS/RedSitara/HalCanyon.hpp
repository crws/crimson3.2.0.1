
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_HalCanyon_HPP

#define INCLUDE_HalCanyon_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HalSitara.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Canyon HMI
//

class CHalCanyon : public CHalSitara
{
	public:
		// Constructor
		CHalCanyon(void);
	};

// End of File

#endif
