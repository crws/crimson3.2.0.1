
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Hardware Drivers
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_UsbDev437_HPP
	
#define	INCLUDE_AM437_UsbDev437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbBase437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Usb Device 
//

class CUsbDev437 : public CUsbBase437, public IUsbFuncHardwareDriver
{
	public:
		// Constructor
		CUsbDev437(UINT iIndex);

		// Destructor
		~CUsbDev437(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbDriver
		BOOL METHOD Bind(IUsbEvents *pEvents);
		BOOL METHOD Bind(IUsbDriver *pDriver);
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);

		// IUsbFuncHardwareDriver
		BOOL METHOD GetHiSpeedCapable(void);
		BOOL METHOD GetHiSpeedActual(void);
		BOOL METHOD SetConfig(UINT uConfig);
		BOOL METHOD SetAddress(UINT uAddr);
		UINT METHOD GetAddress(void);
		BOOL METHOD InitBulk(UINT iEndpt, BOOL fIn, UINT uMax);
		BOOL METHOD KillBulk(UINT iEndpt);
		BOOL METHOD Transfer(UsbIor &Urb);
		BOOL METHOD Abort(UsbIor &Urb);
		BOOL METHOD SetStall(UINT iEndpt, BOOL fSet);
		BOOL METHOD GetStall(UINT iEndpt);
		BOOL METHOD ResetDataToggle(UINT iEndpt);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Device Registers
		enum
		{
			regDCFG                 = 0xC700 / sizeof(DWORD),
			regDCTL                 = 0xC704 / sizeof(DWORD),
			regDEVTEN               = 0xC708 / sizeof(DWORD),
			regDSTS                 = 0xC70C / sizeof(DWORD),
			regDGCMDPAR             = 0xC710 / sizeof(DWORD),
			regDGCMD                = 0xC714 / sizeof(DWORD),
			regDALEPENA             = 0xC720 / sizeof(DWORD),
			regDEPCMDPAR2		= 0xC800 / sizeof(DWORD),
			regDEPCMDPAR1		= 0xC804 / sizeof(DWORD),
			regDEPCMDPAR0		= 0xC808 / sizeof(DWORD),
			regDEPCMD		= 0xC80C / sizeof(DWORD),
			regOEVT                 = 0xCC08 / sizeof(DWORD),
			regOEVTEN               = 0xCC0C / sizeof(DWORD),
			regOSTS                 = 0xCC10 / sizeof(DWORD),
			regOCFG                 = 0xCC00 / sizeof(DWORD),
			regOCTL                 = 0xCC04 / sizeof(DWORD),
			regADPCFG		= 0xCC20 / sizeof(DWORD),
			regADPCTL		= 0xCC24 / sizeof(DWORD),
			regADPEVT		= 0xCC28 / sizeof(DWORD),
			regADPEVTEN		= 0xCC2C / sizeof(DWORD),
			regBCFG			= 0xCC30 / sizeof(DWORD),
			regBCEVT		= 0xCC38 / sizeof(DWORD),
			regBCEVTEN		= 0xCC3C / sizeof(DWORD),
			};

		// Forward Definitions
		struct CTransfer;
		struct CEndpoint;

		// TRB Type
		enum
		{
			trbNormal		= 1,
			trbCtrlSetup		= 2,
			trbCtrlStatus2		= 3,
			trbCtrlStatus3		= 4,
			trbCtrlData		= 5,
			trbIsochronousFirst	= 6,
			trbIsochronous		= 7,
			trbLink			= 8
			};

		// Commands
		enum
		{
			cmdSetEpConfig		= 1,
			cmdSetEpTransferConfig	= 2,
			cmdGetState		= 3,
			cmdSetStall		= 4,
			cmdClearStall		= 5, 
			cmdStartTransfer	= 6, 
			cmdUpdateTransfer	= 7, 
			cmdEndTransfer		= 8,
			cmdStartConfig		= 9,
			};

		// Device Events Enable
		enum
		{
			eventVendTest		= Bit(12),
			eventError		= Bit(9),
			eventSof		= Bit(7),
			eventSuspend		= Bit(6),
			eventHibernate		= Bit(5),
			eventWakeup		= Bit(4),
			eventLink		= Bit(3),
			eventConnect		= Bit(2),
			eventReset		= Bit(1),
			eventDisconnect		= Bit(0),
			};

		// Device Events Types
		enum
		{
			devtVendTest		= 12,
			devtOverflow		= 11,
			devtCmdDone		= 10,
			devtErraticErr		= 9,
			devtSof			= 7,
			devtEop			= 6,
			devtWakeup		= 4,
			devtLink		= 3,
			devtConnect		= 2,
			devtReset		= 1,
			devtDisconnect		= 0,
			};

		// Endpoint Event Types
		enum 
		{	
			epevtCmdDone		= 7,
			epevtStream		= 6,
			epevtFifo		= 4,
			epevtNotReady		= 3,
			epevtContinue		= 2,
			epevtDone		= 1,
			};

		// Endpoint Event Status
		enum
		{
			epevsStatusReq		= 2,
			epevsDataReq		= 1,
			};

		// Interrupts
		enum
		{
			intOtgEvent		= Bit(16),
			intDriveVbusHi		= Bit(13),
			intChargeVbusHi		= Bit(12),
			intDischargeVbusHi	= Bit(11),
			intIdPullHi		= Bit(8),
			intDriveVbusLo		= Bit(5),
			intChargeVbusLo		= Bit(4),
			intDischargeVbusLo	= Bit(3),
			intIdPullLo		= Bit(0),
			};

		// Endpoint States
		enum
		{
			stateInit,
			stateReady,
			stateBusy,
			stateStall,
			stateHalted,
			};

		// Setup State
		enum
		{
			setupInit,
			setupSetupWait,
			setupSetupRecv,
			setupDataOut,
			setupDataIn,
			setupStatus2,
			setupStatus3,
			setupDone,
			};

		// Endpoint Types
		enum 
		{
			typeCtrl		= 0,
			typeIso			= 1,
			typeBulk		= 2,
			typeInt			= 3,
			typeInvalid		= 15,
			};

		// Config Action
		enum
		{
			configInit		= 0,
			configRestore		= 1,
			configModify		= 2,
			};

		// Tuning Constants
		enum
		{
			constEventLimit		= 32,
			constEndptLimit		= 32,
			constTrbLimit		= 32,
			constDataLimit		= 2048,
			};

		// TRB
		struct ALIGN(32) CEndptTrb
		{
			DWORD		  m_dwBufPtrLo;
			DWORD		  m_dwBufPtrHi;
			DWORD volatile	  m_dwBufSize      : 24;
			DWORD volatile	  m_dwPacketCount  :  2;
			DWORD		  m_dwReserved1	   :  2;
			DWORD volatile	  m_dwStatus	   :  4;
			DWORD volatile	  m_dwHwOwner	   :  1;
			DWORD		  m_dwLast	   :  1;
			DWORD		  m_dwChain	   :  1;
			DWORD		  m_dwContShort	   :  1;
			DWORD		  m_dwCtrl	   :  6;
			DWORD		  m_dwIos	   :  1;
			DWORD		  m_dwIoc	   :  1;
			DWORD		  m_dwReserved2	   :  2;
			DWORD volatile	  m_dwStreamId	   : 16;
			DWORD		  m_dwReserved3	   :  2;
			};

		// Command 
		struct CCommand
		{
			BYTE		  m_bCmd;
			DWORD		  m_dwResIdx;
			DWORD		  m_dwParam0;
			DWORD		  m_dwParam1;
			DWORD		  m_dwParam2;
			};

		// Endpoint Events
		struct CEndptEvent
		{
			DWORD		  m_dwEvent	   :  1;
			DWORD		  m_dwEpNum	   :  5;
			DWORD		  m_dwType	   :  4;
			DWORD		  m_dwReserved	   :  2;
			DWORD		  m_dwStatus	   :  4;
			DWORD		  m_dwParams	   : 16;
			};

		// Core Events
		struct CCoreEvent
		{
			DWORD		  m_dwEvent	   :  1;
			DWORD		  m_dwDevEvent	   :  7;
			DWORD		  m_dwType	   :  4;
			DWORD		  m_dwReserved1	   :  4;
			DWORD		  m_dwInfo	   :  8;
			DWORD		  m_dwReserved2	   :  8;
			};
		
		// Events
		union CEvent
		{
			CEndptEvent	  m_EndptEvent;
			CCoreEvent	  m_CoreEvent;
			};		  

		// Transfer
		struct CTransfer
		{
			UINT		  m_iIndex;
			UINT		  m_iEndpt;
			CEndptTrb       * m_pTrb;
			UINT 		  m_uTrbPhy;
			WORD		  m_wResIdx;
			PVBYTE	          m_pDataBuf;
			UINT		  m_uDataPhy;
			UINT		  m_uCount;
			UsbIor	        * m_pUrb;
			CTransfer       * m_pNext;
			CTransfer       * m_pPrev;
			};	        

		// Endpoint
		struct CEndpoint
		{
			UINT 		  m_uType   : 4;
			UINT		  m_uState  : 4;
			UINT		  m_uIndex  : 8;
			UINT		  m_uNum    : 8;
			UINT		  m_uDirIn  : 1;
			UINT		  m_uFifo   : 5;
			UINT		  m_uSpare  : 2;
			UINT		  m_uMaxPack;
			CTransfer       * m_pHead;
			CTransfer       * m_pTail;
			};

		// Data Members
		IUsbFuncHardwareEvents	* m_pDriver;
		CEvent			* m_pEventBuff;
		UINT			  m_iEventCurr;
		PBYTE			  m_pSetup;
		DWORD			  m_dwSetup;
		UINT			  m_uCtrlState;
		PBYTE			  m_pDataBuf;
		DWORD			  m_dwDataBuf;
		CEndpoint		* m_pEpList;
		CEndptTrb		* m_pTbList;
		CTransfer		* m_pTrList;
		CTransfer		* m_pFreeList[constTrbLimit];
		UINT			  m_iFreeHead;
		UINT			  m_iFreeTail;
		IEvent			* m_pCtrl;
		INT 			  m_nIntEnable;

		// Event Handlers
		void OnEventMisc(void);
		void OnEventCore(void);
		void OnEventCore(CCoreEvent const &Event);
		void OnEventCore(CEndptEvent const &Event);
		void OnReset(void);
		void OnConnect(void);
		void OnDisconnect(void);
		void OnNotReady(CEndptEvent const &Event);
		void OnTransferDone(CEndptEvent const &Event);
		void OnSetup(CTransfer *pTr);
		void OnData(CTransfer *pTr);
		void OnConfig(UINT uConfig);

		// Core
		void ResetCore(void);
		void StopController(void);
		void InitController(void);
		void InitCtrlEndpt(void);
		void StartCtrlEndpt(void);
		void StopCtrlEndpt(void);
		bool StartSetup(void);
		void AbortTransfers(void);
		void AbortTransfer(CTransfer *pTr);
		void AbortTransfer(CEndpoint *pEp);
		void ClearStalled(void);
		void EnableInterrupts(bool fEnable);

		// Core Commands
		bool SendStartConfig(UINT uResId);
		bool SendEndptConfig(CEndpoint &Ep, UINT uAction);
		bool SendEndptTransferConfig(CEndpoint &Ep);
		bool SendSetStall(UINT iEndpt);
		bool SendClearStall(UINT iEndpt);
		bool SendStartTransfer(CTransfer *pTr);
		bool SendEndTransfer(CTransfer *pTr);
		UINT SendEndptCmd(UINT iEndpt, CCommand &Cmd, bool fForce);
		bool SendDevCmd(BYTE bCmd);

		// Buffers
		void MakeEventBuffer(void);
		void InitEventBuffer(void);
		void KillEventBuffer(void);
		void MakeDataBuffers(void);
		void KillDataBuffers(void);

		// Endpoints
		void InitEndpoints(void);
		void MakeEndpoints(void);
		void KillEndpoints(void);
		UINT MapEndpoint(UINT iEndpt, bool fDirIn) const;

		// Transfers
		void InitTransfers(void);
		void MakeTransfers(void);
		void KillTransfers(void);
		UINT AllocTransfer(UINT iEndpt);
		void FreeTransfer(CTransfer *pTr);
		void FreeTransfer(UINT iIndex);
		void FreeTransfers(UINT iEndpt);
		void FreeTransfers(void);

		// Diagnostics
		void DumpTrb(CEndptTrb const &Trb);
	};

// End of File

#endif
