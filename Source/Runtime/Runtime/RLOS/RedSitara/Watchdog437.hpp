
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Watchdog437_HPP
	
#define	INCLUDE_AM437_Watchdog437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 Watchdog Module
//

class CWatchdog437
{
	public:
		// Constructor
		CWatchdog437(void);
				
		// Operations
		void Enable(UINT uTime);
		void Enable(void);
		void Disable(void);
		void Kick(void);

	protected:
		// Registers
		enum
		{
			regIDENT	= 0x0000 / sizeof(DWORD),
			regSYSTEM	= 0x0010 / sizeof(DWORD),
			regSTATUS	= 0x0014 / sizeof(DWORD),
			regINTST	= 0x0018 / sizeof(DWORD),
			regINTEN	= 0x001C / sizeof(DWORD),
			regCONTROL	= 0x0024 / sizeof(DWORD),
			regCOUNT	= 0x0028 / sizeof(DWORD),
			regLOAD		= 0x002C / sizeof(DWORD),
			regTRIGGER	= 0x0030 / sizeof(DWORD),
			regPOST		= 0x0034 / sizeof(DWORD),
			regDELAY	= 0x0044 / sizeof(DWORD),
			regSTARTSTOP	= 0x0048 / sizeof(DWORD),
			regRAWIDENT	= 0x0054 / sizeof(DWORD),
			regINTENSET	= 0x005C / sizeof(DWORD),
			regINTENCLR	= 0x0060 / sizeof(DWORD),
			};
					
		// Data Members
		PVDWORD m_pBase;

		// Implementation
		void Reset(void);
		void Wait(void);
	};

// End of File

#endif
