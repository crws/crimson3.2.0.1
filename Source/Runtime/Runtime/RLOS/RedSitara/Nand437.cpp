
#include "Intern.hpp"

#include "Nand437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

#include "Gpmc437.hpp"

#include "Elm437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Nand Flash Controller
//

// Static Data

const BYTE CNand437::m_bEccNull[constBchMax] = 
{ 
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
	0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 
	0xFF, 0xFF, 
	};

const BYTE CNand437::m_bEccGood[constBchMax] = 
{ 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 
	};

// Instantiator

IDevice * Create_Nand437(CClock437 *pClock, CGpmc437 *pGpmc, CElm437 *pElm)
{
	CNand437 *pDevice = New CNand437(pClock, pGpmc, pElm);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CNand437::CNand437(CClock437 *pClock, CGpmc437 *pGpmc, CElm437 *pElm)
{
	StdSetRef();

	m_uChip         = 0;

	m_uClock	= pClock->GetCoreM4Freq() / 2;

	m_pGpmc		= pGpmc;

	m_pElm		= pElm;

	m_uGpmcLine	= pGpmc->GetLine();
	
	m_uElmLine	= pElm->GetLine();

	m_uPolyNum	= 0;
	
	m_Geom.m_uChips = 1;

	m_Geom.m_uSpare = NOTHING;

	m_pDone         = Create_AutoEvent();

	m_pLock         = Create_Mutex();
	}

// Destructor

CNand437::~CNand437(void)
{
	LockBlocks();

	m_pDone->Release();

	m_pLock->Release();
	}

// IUnknown

HRESULT CNand437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(INandMemory);

	return E_NOINTERFACE;
	}

ULONG CNand437::AddRef(void)
{
	StdAddRef();
	}

ULONG CNand437::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CNand437::Open(void)
{
	EnableEvents();

	UnlockBlocks();

	return TRUE;
	}

// INandMemory

BOOL METHOD CNand437::GetGeometry(CNandGeometry &Geom)
{
	if( m_Geom.m_uSpare == NOTHING ) {

		Lock();

		PutNandCmd(cmdReset);

		if( WaitDone(10) ) {

			BYTE bData[5];

			PutNandCmd(cmdReadId);

			PutNandAddr(BYTE(0x00));

			ReadNandData(bData, sizeof(bData));

			if( bData[0] == 0x2C ) {

				if( bData[2] == 0x90 && bData[3] == 0xA6 ) {

					m_Geom.m_uPageSize   = 4096;
				
					m_Geom.m_uBlockSize  = 64;
				
					m_Geom.m_uPlaneSize  = 1024;
				
					m_Geom.m_uDeviceSize = 2;
				
					m_Geom.m_uSpare      = 224;

					BchConfigure(eccBch16);
					}

				if( bData[2] == 0x90 && bData[3] == 0x95 ) {

					m_Geom.m_uPageSize   = 2048;
				
					m_Geom.m_uBlockSize  = 64;
				
					m_Geom.m_uPlaneSize  = 2048;
				
					m_Geom.m_uDeviceSize = 2;
				
					m_Geom.m_uSpare      = 64;

					BchConfigure(eccBch8);
					}

				if( bData[2] == 0xD1 && bData[3] == 0x95 ) {

					m_Geom.m_uPageSize   = 2048;
				
					m_Geom.m_uBlockSize  = 64;
				
					m_Geom.m_uPlaneSize  = 2048;
				
					m_Geom.m_uDeviceSize = 4;
				
					m_Geom.m_uSpare      = 64;

					BchConfigure(eccBch8);
					}
				}
			}

		UnlockBlocks();

		Free();
		}

	Geom.m_uChips   = m_Geom.m_uChips;
				
	Geom.m_uBlocks  = m_Geom.m_uPlaneSize * m_Geom.m_uDeviceSize;

	Geom.m_uPages   = m_Geom.m_uBlockSize;

	Geom.m_uBytes   = 512;

	Geom.m_uSectors = m_Geom.m_uPageSize / Geom.m_uBytes;

	return TRUE;
	}

BOOL METHOD CNand437::GetUniqueId(UINT uChip, PBYTE pData)
{
	Lock();

	PutNandCmd(cmdReadUnique);

	PutNandAddr(BYTE(0x00));

	if( WaitDone(10) ) {

		for( UINT nPass = 0; nPass < 2; nPass ++ ) {

			for( UINT i = 0; i < 16; i ++ ) {

				if( nPass == 0 ) {
		
					pData[i] = GetNandData();
					}
				else {
					BYTE bComp = GetNandData();
				
					if( (bComp ^ pData[i]) != 0xFF ) {

						Free();

						return FALSE;
						}
					}
				}
			}

		Free();
	
		return TRUE;
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand437::IsBlockBad(UINT uChip, UINT uBlock)
{
	DWORD AddrR = m_Geom.m_uBlockSize * uBlock;

	WORD  AddrC = m_Geom.m_uPageSize;

	Lock();

	PutNandCmd(cmdReadBeg);

	PutNandAddr(AddrC);

	PutNandAddr(AddrR);

	PutNandCmd(cmdReadEnd);

	if( WaitDone(10) ) {

		if( GetNandData() == 0xFF ) {

			Free();

			return FALSE;
			}
		}

	Free();

	return TRUE;
	}

BOOL METHOD CNand437::MarkBlockBad(UINT uChip, UINT uBlock)
{
	DWORD AddrR = m_Geom.m_uBlockSize * uBlock;

	WORD  AddrC = m_Geom.m_uPageSize;

	Lock();

	PutNandCmd(cmdProgBeg);

	PutNandAddr(AddrC);

	PutNandAddr(AddrR);

	PutNandData(0x00);

	PutNandData(0x00);

	PutNandCmd(cmdProgEnd);

	if( WaitDone(10) ) {
			
		Free();

		return TRUE;
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand437::EraseBlock(UINT uChip, UINT uBlock)
{
	DWORD Addr = m_Geom.m_uBlockSize * uBlock;

	Lock();

	PutNandCmd(cmdEraseBeg);

	PutNandAddr(Addr);

	PutNandCmd(cmdEraseEnd);

	if( WaitDone(10) ) {

		Free();

		return TRUE;
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand437::WriteSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PCBYTE pData)
{
	DWORD AddrR = m_Geom.m_uBlockSize * uBlock + uPage;

	WORD  AddrC = 512 * uSect;
	
	Lock();

	PutNandCmd(cmdReadBeg);

	PutNandAddr(WORD(0));

	PutNandAddr(AddrR);

	PutNandCmd(cmdReadEnd);

	if( WaitDone(10) ) {

		BchSetWrite();

		BchDisable();

		PutNandCmd(cmdProgBeg);

		PutNandAddr(AddrC);

		PutNandAddr(AddrR);

		BchClear();

		BchEnable();

		SendNandData(pData, 512);

		BchDisable();

		PBYTE pEcc  = &m_bEcc[uSect * m_uBchBytes]; 

		WORD  AddrE = m_Geom.m_uPageSize + constBchOffset + (uSect * m_uBchBytes);

		BchCalculate(pEcc);

		PutNandCmd(cmdProgRand);

		PutNandAddr(AddrE);

		SendNandData(pEcc, m_uBchBytes);

		PutNandCmd(cmdProgEnd);
		
		if( WaitDone(10) ) {

			Free();

			return TRUE;
			}
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand437::ReadSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PBYTE pData)
{
	DWORD AddrR = m_Geom.m_uBlockSize * uBlock + uPage;

	WORD  AddrC = 512 * uSect;

	Lock();

	BchDisable();

	PutNandCmd(cmdReadBeg);

	PutNandAddr(AddrC);

	PutNandAddr(AddrR);

	PutNandCmd(cmdReadEnd);

	if( WaitDone(10) ) {

		BchSetRead();

		BchClear();

		BchEnable();

		ReadNandData(pData, 512);

		WORD AddrE = m_Geom.m_uPageSize + constBchOffset + uSect * m_uBchBytes;

		PutNandCmd(cmdReadRandBeg);

		PutNandAddr(AddrE);

		PutNandCmd(cmdReadRandEnd);

		Delay(1);
			
		ReadNandData(m_bEcc, m_uBchSize);

		if( BchCorrect(pData, m_bEcc) ) {
								
			BchDisable();

			Free();

			return TRUE;
			}

		BchDisable();
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand437::WritePage(UINT uChip, UINT uBlock, UINT uPage, PCBYTE pData)
{
	DWORD AddrR = m_Geom.m_uBlockSize * uBlock + uPage;

	WORD  AddrC = 0;

	Lock();

	PutNandCmd(cmdProgBeg);

	PutNandAddr(AddrC);

	PutNandAddr(AddrR);

	BchDisable();

	BchSetWrite();

	BchDisable();

	UINT  uSize  = 512; 

	UINT  uCount = m_Geom.m_uPageSize / uSize;

	PBYTE pEcc   = m_bEcc;
	
	for( UINT uSect = 0; uSect < uCount; uSect ++ ) { 

		BchClear();
	
		BchEnable();

		SendNandData(pData, uSize);

		BchDisable();

		BchCalculate(pEcc);

		pData += uSize;

		pEcc  += m_uBchBytes;
		}

	AddrC = m_Geom.m_uPageSize + constBchOffset;

	PutNandCmd(cmdProgRand);

	PutNandAddr(AddrC);

	SendNandData(m_bEcc, uCount * m_uBchBytes);

	PutNandCmd(cmdProgEnd);

	if( WaitDone(10) ) {

		Free();

		return TRUE;
		}

	Free();

	return FALSE;
	}

BOOL METHOD CNand437::ReadPage(UINT uChip, UINT uBlock, UINT uPage, PBYTE pData)
{
	DWORD AddrR = m_Geom.m_uBlockSize * uBlock + uPage;

	WORD  AddrC = 0;

	Lock();

	BchDisable();

	PutNandCmd(cmdReadBeg);

	PutNandAddr(AddrC);

	PutNandAddr(AddrR);

	PutNandCmd(cmdReadEnd);

	if( WaitDone(10) ) {

		UINT  uSize  = 512; 

		UINT  uCount = m_Geom.m_uPageSize / uSize;

		PBYTE pEcc   = m_bEcc;

		BchSetRead();

		BchDisable();

		for( UINT uSect = 0; uSect < uCount; uSect ++ ) { 

			BchClear();
	
			BchEnable();

			ReadNandData(pData, uSize);

			AddrC = m_Geom.m_uPageSize + constBchOffset + uSect * m_uBchBytes;

			PutNandCmd(cmdReadRandBeg);

			PutNandAddr(AddrC);

			PutNandCmd(cmdReadRandEnd);

			Delay(1);
			
			ReadNandData(pEcc, m_uBchSize);

			if( BchCorrect(pData, pEcc) ) {

				BchDisable();
			
				AddrC = (uSect + 1) * uSize;

				PutNandCmd(cmdReadRandBeg);

				PutNandAddr(AddrC);

				PutNandCmd(cmdReadRandEnd);

				Delay(1);
			
				pData += uSize;

				pEcc  += m_uBchBytes;
				
				continue;
				}

			BchDisable();
			
			Free();

			return FALSE;
			}

		Free();

		return TRUE;
		}

	Free();

	return FALSE;
	}

// IEventSink

void CNand437::OnEvent(UINT uLine, UINT uParam)
{
	if( uLine == m_uGpmcLine ) {
		
		if( uParam == m_uChip ) {

			m_pDone->Set();
			}

		return;
		}

	if( uLine == m_uElmLine ) {

		if( uParam == m_uPolyNum ) {
			
			m_pDone->Set();
			}

		return;
		}
	}

// Implementation

void CNand437::Lock(void)
{
	m_pLock->Wait(FOREVER);
	}

void CNand437::Free(void)
{
	m_pLock->Free();
	}

void CNand437::EnableEvents(void)
{
	m_pGpmc->SetChipSelEventHook(m_uChip, this);

	m_pElm->SetCompHook(m_uPolyNum, this);
	}

void CNand437::DisableEvents(void)
{
	m_pGpmc->SetChipSelEventHook(m_uChip, NULL);
	}

bool CNand437::WaitDone(UINT uWait)
{
	if( m_pDone->Wait(uWait) ) {

		return !(GetNandStatus() & 0x3);
		}

	return false;
	}

void CNand437::LockBlocks(void)
{
	PutNandCmd(cmdLockAll);
	}

void CNand437::UnlockBlocks(void)
{
	DWORD AddrLo = 0x00000000;
	
	DWORD AddrHi = m_Geom.m_uPlaneSize * m_Geom.m_uDeviceSize * m_Geom.m_uBlockSize;

	PutNandCmd(cmdUnlockLo);

	PutNandAddr(AddrLo);

	PutNandCmd(cmdUnlockHi);

	PutNandAddr(AddrHi);
	}

// Nand

void CNand437::PutNandCmd(BYTE bCmd)
{
	m_pGpmc->PutNandCmd(m_uChip, bCmd);
	}

void CNand437::PutNandAddr(BYTE bAddr)
{
	m_pGpmc->PutNandAddr(m_uChip, bAddr);
	}

void CNand437::PutNandAddr(WORD wAddr)
{
	m_pGpmc->PutNandAddr(m_uChip, LOBYTE(wAddr));
	
	m_pGpmc->PutNandAddr(m_uChip, HIBYTE(wAddr));
	}

void CNand437::PutNandAddr(DWORD dwAddr)
{
	m_pGpmc->PutNandAddr(m_uChip, LOBYTE(LOWORD(dwAddr)));

	m_pGpmc->PutNandAddr(m_uChip, HIBYTE(LOWORD(dwAddr)));

	m_pGpmc->PutNandAddr(m_uChip, LOBYTE(HIWORD(dwAddr)));
	}

void CNand437::PutNandData(BYTE bData)
{
	return m_pGpmc->PutNandData(m_uChip, bData);
	}

BYTE CNand437::GetNandData(void)
{
	return m_pGpmc->GetNandData(m_uChip);
	}

BYTE CNand437::GetNandStatus(void)
{
	PutNandCmd(cmdReadStatus);

	m_bStatus = m_pGpmc->GetNandData(m_uChip);

	PutNandCmd(cmdReadMode);

	return m_bStatus;
	}

void CNand437::ReadNandData(PBYTE pData, UINT uCount)	
{
	m_pGpmc->ReadNandData(m_uChip, pData, uCount);
	}

void CNand437::SendNandData(PCBYTE pData, UINT uCount)
{
	m_pGpmc->SendNandData(m_uChip, pData, uCount);
	}

void CNand437::Delay(UINT uDelay)
{
	phal->SpinDelayFast(uDelay);
	}

// Bch

void CNand437::BchConfigure(UINT uMode)
{
	if( uMode == eccBch8 ) {

		m_uBchSize  = 13;
		
		m_uBchSpare = 1;
		
		m_uBchBytes = m_uBchSize + m_uBchSpare;

		m_pGpmc->SetEccCode(CGpmc437::eccBCH); 

		m_pGpmc->SetEccBchConfig(8, 1, 512);

		m_pElm->SetBchEccLevel(8);

		return;
		}

	if( uMode == eccBch16 ) {

		m_uBchSize  = 26;
		
		m_uBchSpare = 0;
		
		m_uBchBytes = m_uBchSize + m_uBchSpare;
		
		m_pGpmc->SetEccCode(CGpmc437::eccBCH); 

		m_pGpmc->SetEccBchConfig(16, 1, 512);

		m_pElm->SetBchEccLevel(16);

		return;
		}
	}

void CNand437::BchEnable(void)
{
	m_pGpmc->SetEccEnable(true);
	}

void CNand437::BchDisable(void)
{
	m_pGpmc->SetEccEnable(false);
	}

void CNand437::BchClear(void)
{
	m_pGpmc->SetEccPointer(0);

	m_pGpmc->SetEccClear();

	m_pGpmc->SetEccPointer(1);
	}

void CNand437::BchSetRead(void)
{
	UINT uSize0 = m_uBchSize  * 2;

	UINT uSize1 = m_uBchSpare * 2;

	m_pGpmc->SetEccSize(uSize0, uSize1);
	}

void CNand437::BchSetWrite(void)
{
	UINT uSize0 = 0;

	UINT uSize1 = m_uBchSize * 2;

	m_pGpmc->SetEccSize(uSize0, uSize1);
	}

void CNand437::BchCalculate(PBYTE pEcc)
{
	pEcc += m_uBchSize;

	UINT iResult = 0;

	UINT uCount  = m_uBchSize;

	while( uCount ) {

		DWORD dwData = m_pGpmc->GetBchResult(iResult++);

		UINT  uBytes = Min(uCount, sizeof(DWORD));

		for( UINT n = 0; n < uBytes; n++ ) {

			*--pEcc = dwData & 0xFF;

			dwData >>= 8;
			}

		uCount -= uBytes;
		}
	}

bool CNand437::BchCorrect(PBYTE pData, PBYTE pEcc)
{
	if( memcmp(pEcc, m_bEccNull, m_uBchSize) ) {

		BchCalculate(pEcc);

		if( memcmp(pEcc, m_bEccGood, m_uBchSize) ) {

			UINT iFrag  = (m_uBchSize + sizeof(DWORD) - 1) / sizeof(DWORD);

			UINT uCount = m_uBchSize;

			while( uCount ) {

				UINT  uThis  = (uCount & 0x3) ? uCount & 0x3 : Min(uCount, sizeof(DWORD));

				DWORD dwData = 0;

				for( UINT n = 0; n < uThis; n++ ) {

					dwData <<= 8;

					dwData |= *pEcc++;

					uCount --;
					}

				m_pElm->SetSyndromeFrag(m_uPolyNum, --iFrag, dwData);
				}

			m_pElm->SetSyndromeStart(m_uPolyNum);

			if( m_pDone->Wait(10) ) {

				if( m_pElm->GetErrorStatus(m_uPolyNum) ) {

					UINT uErrCount = m_pElm->GetErrorCount(m_uPolyNum);

					if( uErrCount ) {

						/*AfxTrace("WARNING - BCH Error Count %d\n", uErrCount);*/

						UINT uLastEccBit  = m_uBchBytes * 8;

						UINT uLastDataBit = (512 + m_uBchSize) * 8;

						for( UINT i = 0; i < uErrCount; i++ ) {

							UINT uErrLoc = m_pElm->GetErrorAddr(m_uPolyNum, i);

							if( uErrLoc >= (uLastEccBit - 1) ) {

								UINT uByte = ((uLastDataBit - 1) - uErrLoc) / 8;
	
								UINT uMask = Bit(uErrLoc % 8);
					    
								pData[uByte] ^= uMask;
								}
							else {
								/*AfxTrace("WARNING - Error in BCH Bytes.\n");*/
						
								return false;
								}
							}
						}

					return true;
					}

				/*AfxTrace("WARNING - Bad Syndrome.\n");*/

				return false;
				}

			return false;
			}
		}

	return true;
	}

// End of File
