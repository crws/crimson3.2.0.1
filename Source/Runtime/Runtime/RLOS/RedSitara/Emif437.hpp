
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Emif437_HPP
	
#define	INCLUDE_AM437_Emif437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 External Memory Interface Controller
//

class CEmif437
{
	public:
		// Constructor
		CEmif437(void);

		// Operations
		void StartConfigEnable(bool fEnable);
		void SetDdrPhyConfig(bool fHalf, bool fStall, bool fCalib, bool fInvClk, bool fFastLock);
		void SetDdrPhyConfig(UINT uLockDiff, UINT uReadLat);
		void SetExtPhyCtrl(UINT uCtrl, DWORD dwData);
		void SetExtPhyCtrlBit(UINT uCtrl, UINT uBit);
		void SetPhySlaveRatio(UINT uRatio);
		void SetPhyGateLevelRatio(UINT uRatio);
		void SetPhyWriteLevelRatio(UINT uRatio);
		void SetPhyControlDelay(UINT uFifoDelay, UINT uCtrlDelay);
		void SetPhyWriteDQSDelay(UINT uRead, UINT uWrite);
		void SetPhyRank0Delay(UINT uDqOffset, UINT uGateLev, UINT uRank0, UINT uWrData);
		void SetPhyDqOffset(UINT uOffset);
		void SetPhyNumDq(UINT uWriteLevel, UINT uGateLevel);
		void SetPhyCtrlClear(bool fErc, bool fUnlock, bool fMissAligned); 
		void SetSdramConfig(UINT uType, UINT uRow, UINT uPage, UINT uBanks, UINT uWidth);
		void SetSdramDrive(UINT uTerm, UINT DQS, UINT ODT, UINT RZQ, UINT CWL, UINT CL, UINT CS);  
		void SetSdramRefresh(UINT EN, UINT SRT, UINT ASR, UINT PASR, UINT uRate);
		void SetSdramTimings1(UINT RTW, UINT RP, UINT RCD, UINT WR, UINT RA, UINT RAS, UINT RC, UINT RRD, UINT WTR);
		void SetSdramTimings2(UINT XP, UINT ODT, UINT XSNR, UINT XSRD, UINT RTP, UINT CKE);
		void SetSdramTimings3(UINT PDLL, UINT CSTA, UINT CKESR, UINT ZQCS, UINT TDQS, UINT RFC, UINT RAS);
		void SetSdramZQConfig(bool fCS1, bool fCS0, bool fDual, bool fSFEXIT, UINT ZQINIT, UINT ZQCL, UINT REF);
		void SetDLLCalib(UINT uAckWait, UINT uInterval);
		void SetPriorityClassMap(bool fEnable, WORD wMap);
		void SetConnect1ClassMap(bool fEnable, WORD wMap);
		void SetConnect2ClassMap(bool fEnable, WORD wMap);
		void SetExecuteThreshold(UINT uRead, UINT uWrite);
		void SetPriorityCounters(UINT uCount1, UINT uCount2, UINT uOld);
		void SetEnableReadWriteLeveling(bool fEnable);

	protected:
		// Registers
		enum
		{
			regID			= 0x0000 / sizeof(DWORD),
			regSTS			= 0x0004 / sizeof(DWORD),
			regSDRAMCFG1		= 0x0008 / sizeof(DWORD),
			regSDRAMCFG2		= 0x000C / sizeof(DWORD),
			regSDRAMREFCTRL		= 0x0010 / sizeof(DWORD),
			regSDRAMREFCTRLS	= 0x0014 / sizeof(DWORD),
			regSDRAMTIM1		= 0x0018 / sizeof(DWORD),
			regSDRAMTIM1S		= 0x001C / sizeof(DWORD),
			regSDRAMTIM2		= 0x0020 / sizeof(DWORD),
			regSDRAMTIM2S		= 0x0024 / sizeof(DWORD),
			regSDRAMTIM3		= 0x0028 / sizeof(DWORD),
			regSDRAMTIM3S		= 0x002C / sizeof(DWORD),
			regLPDDR2NVM		= 0x0030 / sizeof(DWORD),
			regLPDDR2NVMS		= 0x0034 / sizeof(DWORD),
			regLPDDR2POWER		= 0x0038 / sizeof(DWORD),
			regLPDDR2POWERS		= 0x003C / sizeof(DWORD),
			regLPDDR2MODEDATA	= 0x0040 / sizeof(DWORD),
			regLPDDR2MODECFG	= 0x0050 / sizeof(DWORD),
			regOCPCFG		= 0x0054 / sizeof(DWORD),
			regOCPCFGVAL1		= 0x0058 / sizeof(DWORD),
			regOCPCFGVAL2		= 0x005C / sizeof(DWORD),
			regMISC			= 0x0094 / sizeof(DWORD),
			regDLLCALIB		= 0x0098 / sizeof(DWORD),
			regDLLCALIBS		= 0x009C / sizeof(DWORD),
			regEOI			= 0x00A0 / sizeof(DWORD),
			regSDRAMCALIB		= 0x00C8 / sizeof(DWORD),
			regRDWRLVLRMPWIN	= 0x00D4 / sizeof(DWORD),
			regRDWRLVLRMPCTRL	= 0x00D8 / sizeof(DWORD),
			regRDWRLVLCTRL		= 0x00DC / sizeof(DWORD),
			regDDRPHYCTRL1		= 0x00E4 / sizeof(DWORD),
			regDDRPHYCTRL1S		= 0x00E8 / sizeof(DWORD),
			regPRICOSMAP		= 0x0100 / sizeof(DWORD),
			regCONN1COSMAP		= 0x0104 / sizeof(DWORD),
			regCONN2COSMAP		= 0x0108 / sizeof(DWORD),
			regRDWREXECTHR		= 0x0120 / sizeof(DWORD),
			regCOSCONFIG		= 0x0124 / sizeof(DWORD),
			regPHYSTS		= 0x0144 / sizeof(DWORD),
			regEXTPHYCTRL		= 0x0200 / sizeof(DWORD),
			regEXTPHYCTRLS		= 0x0204 / sizeof(DWORD),
			};

		// Data Members
		PVDWORD	m_pBase;
	};

// End of File

#endif
