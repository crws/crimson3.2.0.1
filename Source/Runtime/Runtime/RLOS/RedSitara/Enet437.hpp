
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Enet437_HPP
	
#define	INCLUDE_AM437_Enet437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CClock437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 Ethernet Module
//

class CEnet437 : public INic, public IEventSink, public IDiagProvider
{
	public:
		// Constructor
		CEnet437(CClock437 *pClock, UINT uGpio);

		// Destructor
		~CEnet437(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// INic
		bool METHOD Open(bool fFast, bool fFull);
		bool METHOD Close(void);
		bool METHOD InitMac(MACADDR const &Addr);
		void METHOD ReadMac(MACADDR &Addr);
		UINT METHOD GetCapabilities(void);
		void METHOD SetFlags(UINT uFlags);
		bool METHOD SetMulticast(MACADDR const *pList, UINT uList);
		bool METHOD IsLinkActive(void);
		bool METHOD WaitLink(UINT uTime);
		bool METHOD SendData(CBuffer *  pBuff, UINT uTime);
		bool METHOD ReadData(CBuffer * &pBuff, UINT uTime);
		void METHOD GetCounters(NICDIAG &Diag);
		void METHOD ResetCounters(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// IDiagProvider
		UINT RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// SS Registers
		enum
		{
			regSsVer		= 0x0000 / sizeof(DWORD),
			regSsCtrl		= 0x0004 / sizeof(DWORD),
			regSsReset		= 0x0008 / sizeof(DWORD),
			regSsStatsEn		= 0x000C / sizeof(DWORD),
			regSsPriType		= 0x0010 / sizeof(DWORD),
			regSsSoftIdle		= 0x0014 / sizeof(DWORD),
			regSsThruRatre		= 0x0018 / sizeof(DWORD),
			regSsGapthRes		= 0x001C / sizeof(DWORD),
			regSsTxStart		= 0x0020 / sizeof(DWORD),
			regSsFlowCtrl		= 0x0024 / sizeof(DWORD),
			regSsVlanType		= 0x0028 / sizeof(DWORD),
			regSsTstype		= 0x002C / sizeof(DWORD),
			regSsDlrType		= 0x0030 / sizeof(DWORD),
			regSsSts		= 0x0034 / sizeof(DWORD),
			};

		// Port 0 Registers
		enum
		{
			regPort0Ctrl		= 0x0100 / sizeof(DWORD),
			regPort0MaxBlocks	= 0x0108 / sizeof(DWORD),
			regPort0BlockCount	= 0x010C / sizeof(DWORD),
			regPort0TxInCtrl	= 0x0110 / sizeof(DWORD),
			regPort0Vlan		= 0x0114 / sizeof(DWORD),
			regPort0TxPriMap	= 0x0118 / sizeof(DWORD),
			regPort0DmaTxPriMap	= 0x011C / sizeof(DWORD),
			regPort0DmaRxChMap	= 0x0120 / sizeof(DWORD),
			regPort0RxDscpMap	= 0x0130 / sizeof(DWORD),
			};

		// Port 1 Registers
		enum
		{
			regPort1Ctrl		= 0x0200 / sizeof(DWORD),
			regPort1TsCtrl		= 0x0204 / sizeof(DWORD),
			regPort1MaxBlocks	= 0x0208 / sizeof(DWORD),
			regPort1BlockCount	= 0x020C / sizeof(DWORD),
			regPort1TxInCtrl	= 0x0210 / sizeof(DWORD),
			regPort1Vlan		= 0x0214 / sizeof(DWORD),
			regPort1TxPriMap	= 0x0218 / sizeof(DWORD),
			regPort1TsSeqMType   	= 0x021C / sizeof(DWORD),
			regPort1SALo      	= 0x0220 / sizeof(DWORD),
			regPort1SAHi     	= 0x0224 / sizeof(DWORD),
			regPort1SendPercent    	= 0x0228 / sizeof(DWORD),
			regPort1RxDscpMap     	= 0x0230 / sizeof(DWORD),
			};

		// Port 2 Registers
		enum
		{
			regPort2Ctrl		= 0x0300 / sizeof(DWORD),
			regPort2TsCtrl		= 0x0304 / sizeof(DWORD),
			regPort2MaxBlocks	= 0x0308 / sizeof(DWORD),
			regPort2BlockCount	= 0x030C / sizeof(DWORD),
			regPort2TxInCtrl	= 0x0310 / sizeof(DWORD),
			regPort2Vlan		= 0x0314 / sizeof(DWORD),
			regPort2TxPriMap	= 0x0318 / sizeof(DWORD),
			regPort2TsSeqMType   	= 0x031C / sizeof(DWORD),
			regPort2SALo      	= 0x0320 / sizeof(DWORD),
			regPort2SAHi     	= 0x0324 / sizeof(DWORD),
			regPort2SendPercent    	= 0x0328 / sizeof(DWORD),
			regPort2RxDscpMap     	= 0x0330 / sizeof(DWORD),
			};

		// Dma Regisgers
		enum
		{
			regDmaTxVer		= 0x0800 / sizeof(DWORD),
			regDmaTxCtrl		= 0x0804 / sizeof(DWORD),
			regDmaTxTeardown	= 0x0808 / sizeof(DWORD),
			regDmaRxVer		= 0x0810 / sizeof(DWORD),
			regDmaRxCtrl		= 0x0814 / sizeof(DWORD),
			regDmaRxTeardown	= 0x0818 / sizeof(DWORD),
			regDmaReset		= 0x081C / sizeof(DWORD),
			regDmaCtrl		= 0x0820 / sizeof(DWORD),
			regDmaStatus		= 0x0824 / sizeof(DWORD),
			regDmaRxBuffOffset	= 0x0828 / sizeof(DWORD),
			regDmaEmuCtrl		= 0x082C / sizeof(DWORD),
			regDmaTxPri		= 0x0830 / sizeof(DWORD),
			regDmaTxIntStat		= 0x0880 / sizeof(DWORD),
			regDmaTxIntMasked	= 0x0884 / sizeof(DWORD),
			regDmaTxIntEnSet	= 0x0888 / sizeof(DWORD),
			regDmaTxIntEnClr	= 0x088C / sizeof(DWORD),
			regDmaIntInVect		= 0x0890 / sizeof(DWORD),
			regDmaIntEoiVect	= 0x0894 / sizeof(DWORD),
			regDmaRxIntStat		= 0x08A0 / sizeof(DWORD),
			regDmaRxIntMasked	= 0x08A4 / sizeof(DWORD),
			regDmaRxIntEnSet	= 0x08A8 / sizeof(DWORD),
			regDmaRxIntEnClr	= 0x08AC / sizeof(DWORD),
			regDmaIntStat		= 0x08B0 / sizeof(DWORD),
			regDmaIntMasked		= 0x08B4 / sizeof(DWORD),
			regDmaIntEnSet		= 0x08B8 / sizeof(DWORD),
			regDmaIntEnClr		= 0x08BC / sizeof(DWORD),
			regDmaRxPendThresh	= 0x08C0 / sizeof(DWORD),
			regDmaRxFreeBuff	= 0x08E0 / sizeof(DWORD),
			};

		// Network Statistics Registers
		enum
		{
			regNetRxGood		= 0x0900 / sizeof(DWORD),
			regNetRxBroad		= 0x0904 / sizeof(DWORD),
			regNetRxMulti		= 0x0908 / sizeof(DWORD),
			regNetRxPause		= 0x090C / sizeof(DWORD),
			regNetRxCrc		= 0x0910 / sizeof(DWORD),
			regNetRxAlign		= 0x0914 / sizeof(DWORD),
			regNetRxOver		= 0x0918 / sizeof(DWORD),
			regNetRxJabber		= 0x091C / sizeof(DWORD),
			regNetRxShort		= 0x0920 / sizeof(DWORD),
			regNetRxFrag		= 0x0924 / sizeof(DWORD),
			regNetRxOctets		= 0x0930 / sizeof(DWORD),
			regNetTxGood		= 0x0934 / sizeof(DWORD),
			regNetTxBroad		= 0x0938 / sizeof(DWORD),
			regNetTxMulti		= 0x093C / sizeof(DWORD),
			regNetTxPause		= 0x0940 / sizeof(DWORD),
			regNetTxDefer		= 0x0944 / sizeof(DWORD),
			regNetCollisions	= 0x0948 / sizeof(DWORD),
			regNetTxColSingle	= 0x094C / sizeof(DWORD),
			regNetTxColMulte	= 0x0950 / sizeof(DWORD),
			regNetExcesCol		= 0x0954 / sizeof(DWORD),
			regNetLateCol		= 0x0958 / sizeof(DWORD),
			regNetTxUnder		= 0x095C / sizeof(DWORD),
			regNetCarrier		= 0x0960 / sizeof(DWORD),
			regNetTxOctets		= 0x0964 / sizeof(DWORD),
			regNetRxTx		= 0x0968 / sizeof(DWORD),
			regNetNetOctets		= 0x0980 / sizeof(DWORD),
			regNetRxSofOver		= 0x0984 / sizeof(DWORD),
			regNetRxMofOver		= 0x0988 / sizeof(DWORD),
			regNetRxDmaOver		= 0x098C / sizeof(DWORD),
			};

		// State Ram Registers
		enum
		{
			regStateRamTxHd		= 0x0A00 / sizeof(DWORD),
			regStateRamRxHd		= 0x0A20 / sizeof(DWORD),
			regStateRamTxCp		= 0x0A40 / sizeof(DWORD),
			regStateRamRxCp		= 0x0A60 / sizeof(DWORD),
			};

		// Time Sync Registers
		enum
		{
			regSyncVer		= 0x0C00 / sizeof(DWORD),
			regSyncCtrl		= 0x0C04 / sizeof(DWORD),
			regSyncClkSel		= 0x0C08 / sizeof(DWORD),
			regSyncEvent		= 0x0C10 / sizeof(DWORD),
			regSyncLoadData		= 0x0C14 / sizeof(DWORD),
			regSyncLoadEn		= 0x0C18 / sizeof(DWORD),
			regSyncCompData		= 0x0C1C / sizeof(DWORD),
			regSyncComplEn		= 0x0C20 / sizeof(DWORD),
			regSyncStat		= 0x0C24 / sizeof(DWORD),
			regSyncMask		= 0x0C28 / sizeof(DWORD),
			regSyncInten		= 0x0C2C / sizeof(DWORD),
			regSyncEventPop		= 0x0C30 / sizeof(DWORD),
			regSyncEventLow		= 0x0C34 / sizeof(DWORD),
			regSyncEventMid		= 0x0C38 / sizeof(DWORD),
			regSyncEventHigh	= 0x0C3C / sizeof(DWORD),
			};

		// Address Lookup Engine Registers
		enum
		{
			regAleVer		= 0x0D00 / sizeof(DWORD),
			regAleCtrl		= 0x0D08 / sizeof(DWORD),
			regAlePrescale		= 0x0D10 / sizeof(DWORD),
			regAleUnknown		= 0x0D18 / sizeof(DWORD),
			regAleTableCtrl		= 0x0D20 / sizeof(DWORD),
			regAleTable2		= 0x0D34 / sizeof(DWORD),
			regAleTable1		= 0x0D38 / sizeof(DWORD),
			regAleTable0		= 0x0D3C / sizeof(DWORD),
			regAlePortCtrl0		= 0x0D40 / sizeof(DWORD),
			regAlePortCtrl1		= 0x0D44 / sizeof(DWORD),
			regAlePortCtrl2		= 0x0D48 / sizeof(DWORD),
			};

		// SL1 Registers
		enum
		{
			regSl1Ver		= 0x0D80 / sizeof(DWORD),
			regSl1Ctrl		= 0x0D84 / sizeof(DWORD),
			regSl1Stat		= 0x0D88 / sizeof(DWORD),
			regSl1Reset		= 0x0D8C / sizeof(DWORD),
			regSl1RxMaxLen		= 0x0D90 / sizeof(DWORD),
			regSl1Backoff		= 0x0D94 / sizeof(DWORD),
			regSl1RxPause		= 0x0D98 / sizeof(DWORD),
			regSl1TxPause		= 0x0D9C / sizeof(DWORD),
			regSl1EmCtrl		= 0x0DA0 / sizeof(DWORD),
			regSl1RxPriMap		= 0x0DA4 / sizeof(DWORD),
			regSl1TxGap		= 0x0DA8 / sizeof(DWORD),
			};

		// SL2 Registers
		enum
		{
			regSl2Ver		= 0x0DC0 / sizeof(DWORD),
			regSl2Ctrl		= 0x0DC4 / sizeof(DWORD),
			regSl2Stat		= 0x0DC8 / sizeof(DWORD),
			regSl2Reset		= 0x0DCC / sizeof(DWORD),
			regSl2RxMaxLen		= 0x0DD0 / sizeof(DWORD),
			regSl2Backoff		= 0x0DD4 / sizeof(DWORD),
			regSl2RxPause		= 0x0DD8 / sizeof(DWORD),
			regSl2TxPause		= 0x0DDC / sizeof(DWORD),
			regSl2EmCtrl		= 0x0DE0 / sizeof(DWORD),
			regSl2RxPriMap		= 0x0DE4 / sizeof(DWORD),
			regSl2Txgap		= 0x0DE8 / sizeof(DWORD),
			};

		// Mdio Registers
		enum
		{
			regMdioVer		= 0x1000 / sizeof(DWORD),
			regMdioCtrl		= 0x1004 / sizeof(DWORD),
			regMdioAlive		= 0x1008 / sizeof(DWORD),
			regMdioLink		= 0x100C / sizeof(DWORD),
			regMdioLinkIntRaw	= 0x1010 / sizeof(DWORD),
			regMdioLinkIntMask	= 0x1014 / sizeof(DWORD),
			regMdioUserIntRaw	= 0x1020 / sizeof(DWORD),
			regMdioUserIntMask	= 0x1024 / sizeof(DWORD),
			regMdioUserIntSet	= 0x1028 / sizeof(DWORD),
			regMdioCmdIntClr	= 0x102C / sizeof(DWORD),
			regMdioAccess0		= 0x1080 / sizeof(DWORD),
			regMdioPhySel0		= 0x1084 / sizeof(DWORD),
			regMdioAccess1		= 0x1088 / sizeof(DWORD),
			regMdioPhySel1		= 0x108C / sizeof(DWORD),
			};

		// WR Registers
		enum
		{
			regWrVer		= 0x1200 / sizeof(DWORD),
			regWrReset		= 0x1204 / sizeof(DWORD),
			regWrCtrl		= 0x1208 / sizeof(DWORD),
			regWrIntCtrl		= 0x120C / sizeof(DWORD),
			regWrC0RxThreshEn	= 0x1210 / sizeof(DWORD),
			regWrC0RxEn		= 0x1214 / sizeof(DWORD),
			regWrC0TxEn		= 0x1218 / sizeof(DWORD),
			regWrC0MiscEn		= 0x121C / sizeof(DWORD),
			regWrC1RxThreshEn	= 0x1220 / sizeof(DWORD),
			regWrC1RxEn		= 0x1224 / sizeof(DWORD),
			regWrC1TxEn		= 0x1228 / sizeof(DWORD),
			regWrC1MiscEn		= 0x122C / sizeof(DWORD),
			regWrC2RxThreshEn	= 0x1230 / sizeof(DWORD),
			regWrC2RxEn		= 0x1234 / sizeof(DWORD),
			regWrC2TxEn		= 0x1238 / sizeof(DWORD),
			regWrC2MiscEn		= 0x123C / sizeof(DWORD),
			regWrC0RxThreshStat	= 0x1240 / sizeof(DWORD),
			regWrC0RxStat		= 0x1244 / sizeof(DWORD),
			regWrC0TxStat		= 0x1248 / sizeof(DWORD),
			regWrC0MiscStat		= 0x124C / sizeof(DWORD),
			regWrC1RxThreshStat	= 0x1250 / sizeof(DWORD),
			regWrC1RxStat		= 0x1254 / sizeof(DWORD),
			regWrC1TxStat		= 0x1258 / sizeof(DWORD),
			regWrC1MiscStat		= 0x125C / sizeof(DWORD),
			regWrC2RxThreshStat	= 0x1260 / sizeof(DWORD),
			regWrC2RxStat		= 0x1264 / sizeof(DWORD),
			regWrC2TxStat		= 0x1268 / sizeof(DWORD),
			regWrC2MiscStat		= 0x126C / sizeof(DWORD),
			regWrC0RxImax		= 0x1270 / sizeof(DWORD),
			regWrC0TxImax		= 0x1274 / sizeof(DWORD),
			regWrC1RxImax		= 0x1278 / sizeof(DWORD),
			regWrC1TxImax		= 0x127C / sizeof(DWORD),
			regWrC2RxImax		= 0x1280 / sizeof(DWORD),
			regWrC2TxImax		= 0x1284 / sizeof(DWORD),
			regWrRgmiiCtrl		= 0x1288 / sizeof(DWORD),
			};

		// PPI Programming Interface Ram Registers
		enum
		{
			regCppiRam		= 0x2000 / sizeof(DWORD),
			};

		// Buffer Descriptor
		struct CBuffDesc
		{
			DWORD		m_NextPtr;
			DWORD		m_BuffPtr;
			DWORD		m_BuffLen	: 16;
			DWORD		m_BuffOff	: 16;
			DWORD volatile	m_PacketLen	: 16;
			DWORD volatile  m_Flags		: 16;
			};

		// Ring Buffer
		struct CNicBuffer
		{
			UINT		  m_iIndex;
			CBuffDesc	* m_pDesc;
			DWORD		  m_dwDesc;
			PBYTE		  m_pData;
			DWORD		  m_dwData;
			};

		// Buffer Descriptor Flags
		enum
		{
			flagSop		= Bit(15),
			flagEop		= Bit(14),
			flagOwner	= Bit(13),
			flagEoq		= Bit(12),
			flagTeardown	= Bit(11),
			flagPassCrc	= Bit(10),
			flagLong	= Bit( 9),
			flagShort	= Bit( 8),
			flagCtrl	= Bit( 7),
			flagOverrun	= Bit( 6),
			flagErrorHi	= Bit( 5),
			flagErrorLo	= Bit( 4),
			flagToPortEn	= Bit( 4),
			flagVlan	= Bit( 3),
			flagPort	= 0,
			flagErrors	= flagLong | flagShort | flagCtrl | flagOverrun | flagErrorHi | flagErrorLo,
			};

		// Miscellaneous Interrupts
		enum
		{
			intMdioUser	= Bit(0),
			intMdioLink	= Bit(1),
			intHost		= Bit(2),
			intStat		= Bit(3),
			intEvent	= Bit(4),
			};

		// ALE Port Modes
		enum
		{
			aleDisabled,
			aleBlocked,
			aleLearn,
			aleForward,
			};

		// ALE Control Bits
		enum
		{
			aleEnable	= Bit(31),
			aleTableClear	= Bit(30),
			aleTableAgeOut	= Bit(29),
			aleUnicastFlood	= Bit(8),
			aleLearnNoVid	= Bit(7),
			aleEnableVlan	= Bit(6),
			aleDenayOui	= Bit(5),
			aleBypass	= Bit(4),
			aleRateLimiTx	= Bit(3),
			aleVlanAware	= Bit(2),
			aleAuthMode	= Bit(1),
			aleRateLimit	= Bit(0),
			};

		// Mdio Control Bits
		enum
		{
			mdioIdle	= Bit(31),
			mdioEn		= Bit(30),
			mdioUserChan	= 24,
			mdioNoPreamble	= Bit(20),
			mdioFault	= Bit(19),
			mdioFaultEn	= Bit(18),
			mdioTest	= Bit(17),
			mdioClockDiv	= 0
			};

		// Interrupt Ack Vectors
		enum
		{
			intEoiRxLevel	= 0,
			intEoiRxPulse	= 1,
			intEoiTxPulse	= 2,
			intEoiMisc	= 3,
			};
		
		// Nic States
		enum
		{
			nicInitial,
			nicClosed,
			nicOpen,
			nicNegotiate,
			nicActive,
			};

		// Phy States
		enum
		{
			phyIdle,
			phyGetIsr,
			phyGetLink,
			};

		// Mii State
		enum
		{
			miiIdle,
			miiAppPut,
			miiAppGet,
			miiIntPut,
			miiIntGet,
			};

		// Buffer Layout
		enum
		{
			constTxLimit	= 16,
			constRxLimit	= 64,
			constBuffSize	= 1522,
			constMinPacket	= 64 - 4,
			};

		// Data
		PVDWORD		 m_pBase;
		ULONG		 m_uRefs;
		UINT		 m_uProv;
		IGpio          * m_pGpioReset;
		UINT             m_uGpioReset;
		UINT		 m_uFreq;
		UINT		 m_uCore;
		UINT		 m_uChan;
		UINT		 m_uLineRt;
		UINT		 m_uLineRx;
		UINT		 m_uLineTx;
		UINT		 m_uLineMs;
		UINT		 m_uPhyReset;
		MACADDR		 m_MacAddr;
		MACADDR	       * m_pMacList;
		UINT		 m_uMacList;
		UINT		 m_uFlags;
		bool		 m_fAllowFull;
		bool		 m_fAllowFast;
		bool		 m_fUsingFull;
		bool		 m_fUsingFast;
		UINT		 m_uNicState;
		UINT		 m_uMiiState;
		UINT		 m_uPhyState;
		IEvent	       * m_pMiiFlag;
		IEvent	       * m_pLinkOkay;
		IEvent	       * m_pLinkDown;
		IMutex	       * m_pRxLock;
		IMutex	       * m_pTxLock;
		ISemaphore     * m_pRxFlag;
		ISemaphore     * m_pTxFlag;
		CBuffDesc      * m_pTxDesc;
		CBuffDesc      * m_pRxDesc;
		CNicBuffer     * m_pTxBuff;
		CNicBuffer     * m_pRxBuff;
		PBYTE		 m_pTxData;
		PBYTE		 m_pRxData;
		UINT		 m_uTxHead;
		UINT		 m_uTxTail;
		UINT		 m_uTxLast;
		UINT		 m_uRxHead;
		UINT		 m_uRxTail;
		UINT		 m_uRxLast;
		INT		 m_nPoll;

		// Event Handlers
		void OnRecv(void);
		void OnSend(void);
		void OnMisc(void);

		// Implementation
		void EnableEvents(void);
		void DisableEvents(void);

		// Mac Management
		void ResetState(void);
		void ResetController(void);
		void ConfigController(void);
		void StartController(void);
		void StopController(void);
		void ResetSwitch(void);
		void ResetDma(void);
		void ResetMac(void);
		void AddMac(PBYTE pData);
		void FreeMacList(void);
		
		// Phy Management
		void ResetPhy(bool fAssert);
		void PhyPollEnable(bool fEnable);
		void ConfigPhy(void);
		void OnMiiEvent(void);
		void OnPhyEvent(void);
		void OnLinkUp(void);
		void OnLinkDown(void);

		// Buffer Management
		void  AllocBuffers(void);
		PVOID AllocNoneCached(UINT uAlloc, UINT uAlign);
		void  ResetBuffers(void);
		void  FreeBuffers (void);
		void  StartRecv(CBuffDesc *pDesc);
		BOOL  ContinueRecv(CNicBuffer *pBuff);
		void  StartSend(CNicBuffer *pBuff);

		// Mii Managment
		WORD AppGetMii(BYTE bAddr);
		BOOL AppPutMii(BYTE bAddr, WORD wData);
		void IntGetMii(BYTE bAddr);
		void IntPutMii(BYTE bAddr, WORD wData);

		// Diagnostics
		bool DiagRegister(void);
		bool DiagRevoke(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Debug
		void DumpNetStats(void);
		void DumpDesc(CBuffDesc const &Desc);
		void DumpAle(void);
	};

// End of File

#endif
