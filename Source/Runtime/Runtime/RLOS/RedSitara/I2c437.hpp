
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_I2c437_HPP
	
#define	INCLUDE_AM437_I2c437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CClock437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 I2C Controller Module
//

class CI2c437 : public II2c, public IEventSink
{
	public:
		// Constructor
		CI2c437(CClock437 *pClock);

		// Destructor
		~CI2c437(void);
				
		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// II2c
		BOOL METHOD Lock(UINT uTime);
		void METHOD Free(void);
		BOOL METHOD Send(BYTE bChip, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData);
		BOOL METHOD Recv(BYTE bChip, PCBYTE pHead, UINT uHead, PBYTE  pData, UINT uData);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Registers
		enum
		{
			regSYSCFG	= 0x10 / sizeof(DWORD),
			regIRQRAW	= 0x24 / sizeof(DWORD),
			regIRQSTS	= 0x28 / sizeof(DWORD),
			regIRQEN	= 0x2C / sizeof(DWORD),
			regIRQCLR	= 0x30 / sizeof(DWORD),
			regWE		= 0x34 / sizeof(DWORD),
			regSYSSTS	= 0x90 / sizeof(DWORD),
			regBUF		= 0x94 / sizeof(DWORD),
			regCNT		= 0x98 / sizeof(DWORD),
			regDATA		= 0x9C / sizeof(DWORD),
			regCTRL		= 0xA4 / sizeof(DWORD),
			regOA		= 0xA8 / sizeof(DWORD),
			regSA		= 0xAC / sizeof(DWORD),
			regPSC		= 0xB0 / sizeof(DWORD),
			regSCLL		= 0xB4 / sizeof(DWORD),
			regSCLH		= 0xB8 / sizeof(DWORD),
			regSYSTEST	= 0xBC / sizeof(DWORD),
			regBUFSTAT	= 0xC0 / sizeof(DWORD),
			regOA1		= 0xC4 / sizeof(DWORD),
			regOA2		= 0xC8 / sizeof(DWORD),
			regOA3		= 0xCC / sizeof(DWORD),
			regACTOA	= 0xD0 / sizeof(DWORD),
			regSBLOCK	= 0xD4 / sizeof(DWORD),
			};

		// Control Bits
		enum
		{
			ctrlEnable	= Bit(15),
			ctrlStartByte	= Bit(11),
			ctrlMaster	= Bit(10),
			ctrlTxMode	= Bit(9),
			ctrlExpSlave	= Bit(8),
			ctrlExpOwn0	= Bit(7),
			ctrlExpOwn1	= Bit(6),
			ctrlExpOwn2	= Bit(5),
			ctrlExpOwn3	= Bit(4),
			ctrlStop	= Bit(1),
			ctrlStart	= Bit(0),
			ctrlStartStop   = ctrlStart | ctrlStop,
			};

		// Interrupts
		enum
		{
			intTxRdy	= Bit(14),
			intRxRdy	= Bit(13),
			intBusActive	= Bit(12),
			intRxOvr	= Bit(11),
			intTxUnder	= Bit(10),
			intAddr		= Bit(9),
			intBusFree	= Bit(8),
			intError	= Bit(7),
			intStart	= Bit(6),
			intGenCall	= Bit(5),
			intTxFifo	= Bit(4),
			intRxFifo	= Bit(3),
			intRdy		= Bit(2),
			intNoAck	= Bit(1),
			intArbLost	= Bit(0),
			intAll		= 0x7FFF,
			};

		// State
		enum
		{
			stateIdle,
			stateStart,
			stateHead,
			stateRestart,
			stateData,
			};
					
		// Data Members
		PVDWORD	     m_pBase;
		ULONG	     m_uRefs;
		UINT	     m_uLine;
		DWORD	     m_uFreq;
		UINT         m_uUnit;
		IMutex	   * m_pLock;
		IEvent	   * m_pDone;
		PCBYTE	     m_pHead;
		UINT	     m_uHead;
		PBYTE	     m_pData;
		UINT	     m_uData;
		bool	     m_fSend;
		bool	     m_fWait;
		bool	     m_fOkay;
		bool	     m_fBusy;
		UINT	     m_uState;
		UINT	     m_uPtr;

		// Event Handlers
		void OnTxFifo(void);
		void OnTxData(void);
		void OnRxFifo(void);
		void OnRxData(void);
		void OnNoAck(void);
		void OnReady(void);

		// Transfer
		bool Transfer(BYTE bAddr, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData, bool fSend);

		// Implementation
		void InitData(void);
		void InitController(void);
		void InitEvents(void);
		void Reset(void);
		UINT WriteFifo(PCBYTE pData, UINT uCount);
		UINT ReadFifo(PBYTE pData, UINT uCount);
		UINT GetFifoDepth(void);
		UINT GetFifoSpace(void);
		bool CanWait(void);
		void TestWait(void);
		void WaitDone(void);
	};

// End of File

#endif
