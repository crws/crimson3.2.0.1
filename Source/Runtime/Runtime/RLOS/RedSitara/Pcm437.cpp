
#include "Intern.hpp"

#include "Pcm437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 Power Management Module
//

// Register Access

#define RegPER(x) (m_pBasePER[reg##x])

// Constructor

CPcm437::CPcm437(void)
{
	m_pBasePER = PVDWORD(ADDR_PRM_PER);
	}

// Operations

void CPcm437::EnablePru(void)
{
	RegPER(RSTCTRL) &= ~Bit(1);
	}

// End of File
