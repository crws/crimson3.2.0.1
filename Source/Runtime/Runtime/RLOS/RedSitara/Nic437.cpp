
#include "DualEnet437.hpp"

#include "Nic437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

#include "DualEnet437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Macros  
//

#define RoundUp(x, n)	((x + (n-1)) & ~(n-1))

//////////////////////////////////////////////////////////////////////////
//
// Lower Level Routines
//

clink void * _malloc_r(_reent *reent, size_t size);

clink void   _free_r(_reent *reent, void *data);

///////////////////////////////////////////////////////////////////////////
//
// Broadcast Address
//

static MACADDR macBroad = { { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF } };

//////////////////////////////////////////////////////////////////////////
//
// AM437 NIC
//

// Instantiator

IDevice * Create_Nic437(CDualEnet437 *pEnet, UINT uPort, UINT uPhy, UINT uGpio)
{
	CNic437 *pDevice = New CNic437(pEnet, uPort, uPhy, uGpio);

	pDevice->Open();

	return (IDevice *) (INic *) pDevice;
	}

// Constructor

CNic437::CNic437(CDualEnet437 *pEnet, UINT uPort, UINT uPhy, UINT uGpio)
{
	StdSetRef();

	m_pEnet       = pEnet;
	
	m_uPort       = uPort;

	m_uChan       = m_uPort - 1;
		     
	m_uPhyReset   = NOTHING;

	m_uPhy        = uPhy;

	m_uPhyIntLine = LOBYTE(uGpio);

	m_pMacList    = NULL;

	m_uMacList    = 0;

	m_uNicState   = nicInitial;

	m_uPhyState   = phyIdle;

	m_uMiiState   = miiIdle;

	m_uFlags      = 0;

	m_nPoll	      = 0;

	m_pMiiFlag    = Create_AutoEvent();
		      
	m_pLinkOkay   = Create_ManualEvent();
		      
	m_pLinkDown   = Create_ManualEvent();
		      
	m_pTxLock     = Create_Mutex();
		      
	m_pRxLock     = Create_Mutex();
		      
	m_pTxFlag     = Create_Semaphore();
		      
	m_pRxFlag     = Create_Semaphore();

	AfxGetObject("gpio", HIBYTE(uGpio), IGpio, m_pGpioPhyInt);

	AfxGetObject("gpio", HIBYTE(m_uPhyReset), IGpio, m_pGpioPhyReset);

	AllocBuffers();
	}

// Destructor

CNic437::~CNic437(void)
{
	AfxRelease(m_pGpioPhyInt);
	
	AfxRelease(m_pGpioPhyReset);

	m_pMiiFlag->Release();

	m_pLinkOkay->Release();

	m_pLinkDown->Release();

	m_pTxLock->Release();

	m_pRxLock->Release();

	m_pTxFlag->Release();

	m_pRxFlag->Release();

	FreeBuffers();

	FreeMacList();
	}

// IUnknown

HRESULT CNic437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(INic);

	return E_NOINTERFACE;
	}

ULONG CNic437::AddRef(void)
{
	StdAddRef();
	}

ULONG CNic437::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CNic437::Open(void)
{
	return TRUE;
	}

// INic

bool METHOD CNic437::Open(bool fFast, bool fFull)
{
	if( m_uNicState == nicClosed ) {

		m_fAllowFast = fFast;

		m_fAllowFull = fFull;

		m_uNicState  = nicOpen;

		m_pLinkOkay->Clear();

		m_pLinkDown->Set();

		EnableEvents();

		ResetState();

		m_pEnet->SetPhy(m_uPort, m_uPhy);

		m_pEnet->ConfigController(m_uPort, m_uChan);

		ResetPhy(false);

		ConfigPhy();

		return true;
		}

	return false;
	}

bool METHOD CNic437::Close(void)
{
	if( m_uNicState >= nicOpen ) {
		
		OnLinkDown();

		m_uNicState = nicClosed;
		
		ResetPhy(true);

		ResetCounters();

		m_pEnet->Close();

		return true;
		}

	return false;
	}

bool METHOD CNic437::InitMac(MACADDR const &Addr)
{
	if( m_uNicState == nicInitial ) {

		m_pMacList    = (MACADDR *) _malloc_r(NULL, 2 * sizeof(MACADDR));
		
		m_uMacList    = 2;

		m_MacAddr     = Addr;

		m_pMacList[0] = Addr;

		m_pMacList[1] = macBroad;

		m_uNicState = nicClosed;

		return true;
		}

	return false;
	}

void METHOD CNic437::ReadMac(MACADDR &Addr)
{
	Addr = m_MacAddr; 
	}

UINT METHOD CNic437::GetCapabilities(void)
{
	return nicFilterIp | nicCheckSums | nicAddSums;
	}

void METHOD CNic437::SetFlags(UINT uFlags)
{
	m_uFlags = uFlags;
	}

bool METHOD CNic437::SetMulticast(MACADDR const *pList, UINT uList)
{
	UINT     uMacList = uList + 2;

	MACADDR *pMacList = (MACADDR *) _malloc_r(NULL, uMacList * sizeof(MACADDR));

	UINT     uMacSlot = 0;

	pMacList[uMacSlot++] = m_MacAddr;

	pMacList[uMacSlot++] = macBroad;

	if( pList && uList ) {

		while( uList-- ) {

			pMacList[uMacSlot++] = *pList++;
			}
		}

	MACADDR *pOldList = m_pMacList;

	UINT ipr;

	HostRaiseIpr(ipr);

	m_uMacList = uMacList;

	m_pMacList = pMacList;

	HostLowerIpr(ipr);

	_free_r(NULL, pOldList);

	return true;
	}

bool METHOD CNic437::IsLinkActive(void)
{
	return m_uNicState == nicActive;
	}

bool METHOD CNic437::WaitLink(UINT uTime)
{
	return m_pLinkOkay->Wait(uTime);
	}

bool METHOD CNic437::SendData(CBuffer *pBuff, UINT uTime)
{
	if( m_uNicState == nicActive ) {

		UINT uWait = WaitMultiple( m_pTxFlag,
					   m_pLinkDown,
					   uTime
					   );

		if( uWait == 1 ) {

			WORD  wSize = pBuff->GetSize();

			PBYTE pData = pBuff->GetData();

			m_pTxLock->Wait(FOREVER);	
				
			CNicBuffer *pDesc = m_pTxBuff + m_uTxTail;
			
			PBYTE       pDest = pDesc->m_pData;

			m_uTxTail = (m_uTxTail + 1) % constTxLimit;

			AddMac(pData);

			memcpy(pDest, pData, wSize);

			if( wSize < constMinPacket ) {

				memset(pDest + wSize, 0x00, constMinPacket - wSize);

				pDesc->m_pDesc->m_BuffLen = constMinPacket;
				}
			else {
				pDesc->m_pDesc->m_BuffLen = wSize;
				}

			StartSend(pDesc);
	
			m_pTxLock->Free();

			return true;
			}
		}

	return false;
	}

bool CNic437::ReadData(CBuffer * &pBuff, UINT uTime)
{
	if( m_uNicState == nicActive ) {

		for(;;) {

			UINT uWait = WaitMultiple( m_pRxFlag,
						   m_pLinkDown,
						   uTime
						   );

			if( uWait == 1 ) {

				m_pRxLock->Wait(FOREVER);

				CNicBuffer *pDesc = m_pRxBuff + m_uRxHead;

				m_uRxHead = (m_uRxHead + 1) % constRxLimit;

				if( !(pDesc->m_pDesc->m_Flags & flagErrors) ) {

					PBYTE pData  = pDesc->m_pData;

					WORD  wCount = (pDesc->m_pDesc->m_Flags & flagPassCrc) ? pDesc->m_pDesc->m_BuffLen - 4 : pDesc->m_pDesc->m_BuffLen;

					BOOL  fIp    = MotorToHost((WORD &) pData[12]) == 0x0800;

					if( !(m_uFlags & nicOnlyIp) || fIp ) {

						UINT ipr;

						HostRaiseIpr(ipr);

						for( UINT n = 0; n < m_uMacList; n++ ) {

							if( !memcmp(pData, m_pMacList + n, 6) ) {

								HostLowerIpr(ipr);

								CBuffer *pWork  = BuffAllocate(wCount);

								if( pWork ) {

									PBYTE pDest = pWork->AddTail(wCount);

									memcpy(pDest, pData, wCount);

									if( fIp ) {

										pWork->SetFlag(packetTypeIp);
										}

									pWork->SetFlag(packetTypeValid);

									pWork->SetFlag(packetSumsValid);

									StartRecv(pDesc);

									m_pRxLock->Free();

									pBuff = pWork;

									return true;
									}

								break;
								}
							}

						HostLowerIpr(ipr);
						}
					}

				StartRecv(pDesc);

				m_pRxLock->Free();

				continue;
				}

			break;
			}
		}

	pBuff = NULL;

	return false;
	}

void METHOD CNic437::GetCounters(NICDIAG &Diag)
{
	}

void METHOD CNic437::ResetCounters(void)
{
	}

// IEventSink

void CNic437::OnEvent(UINT uLine, UINT uParam)
{
	m_uPhyState = phyGetIsr;

	IntGetMii(0x1D);
	}

// Implementation

void CNic437::EnableEvents(void)
{
	if( m_pGpioPhyInt ) {

		m_pGpioPhyInt->SetDirection(m_uPhyIntLine, false);

		m_pGpioPhyInt->SetIntHandler(m_uPhyIntLine, intLevelLo, this, 0);
		}
	}

// Mac Management

void CNic437::ResetState(void)
{
	while( m_pRxFlag->Wait(5) );

	while( m_pTxFlag->Wait(5) );

	m_uFlags = 0;

	ResetBuffers();

	m_pTxFlag->Signal(constTxLimit-1);
	}

void CNic437::AddMac(PBYTE pData)
{
	pData[ 6] = m_MacAddr.m_Addr[0];
	pData[ 7] = m_MacAddr.m_Addr[1];
	pData[ 8] = m_MacAddr.m_Addr[2];
	pData[ 9] = m_MacAddr.m_Addr[3];
	pData[10] = m_MacAddr.m_Addr[4];
	pData[11] = m_MacAddr.m_Addr[5];
	}

void CNic437::FreeMacList(void)
{
	if( m_pMacList ) {

		_free_r(NULL, m_pMacList);

		m_pMacList = NULL;

		m_uMacList = 0;
		}
	}

// Phy Management

void CNic437::ResetPhy(bool fAssert)
{
	if( m_uPhyReset != NOTHING ) {

		m_pGpioPhyReset->SetState(LOWORD(m_uPhyReset), !fAssert);
		}
	else {
		m_pEnet->PutMii(m_uPhy, 0x00, Bit(15));

		Sleep(25);
		}
	}

void CNic437::PhyInterrupt(bool fEnable)
{
	if( m_pGpioPhyInt ) {
		
		m_pGpioPhyInt->SetIntEnable(m_uPhyIntLine, fEnable);
		}
	}

void CNic437::ConfigPhy(void)
{
	WORD Caps = 0x0021;

	if( m_fAllowFast ) {

		Caps |= 0x0080;
		}

	if( m_fAllowFull ) {

		Caps |= ((Caps & 0x00A0) << 1);
		}

	AppGetMii(0x1D);

	AppPutMii(0x1E, Bit(6) | Bit(4));

	AppPutMii(0x04, Caps);

	AppPutMii(0x00, Bit(12));
	}

void CNic437::OnMiiEvent(WORD wData)
{
	PhyInterrupt(true);

	switch( m_uMiiState ) {

		case miiAppPut:
		case miiAppGet:

			m_uMiiState = miiIdle;

			m_pMiiFlag->Set();

			break;

		case miiIntPut:

			m_uMiiState = miiIdle;

			break;

		case miiIntGet:

			m_uMiiState = miiIdle;

			OnPhyEvent(wData);

			break;
		}
	}

void CNic437::OnPhyEvent(WORD wData)
{
	if( m_uPhyState == phyGetIsr ) {

		if( wData & Bit(6) ) {
			
			m_uPhyState = phyGetLink;

			IntGetMii(0x1F);

			return;
			}

		if( wData & Bit(4) ) {

			m_uPhyState = phyIdle;
		
			OnLinkDown();

			IntGetMii(0x01);

			return;
			}

		return;
		}

	if( m_uPhyState == phyGetLink ) {

		if( wData & Bit(12) ) {

			m_fUsingFast = (wData & Bit(3)) ? true : false;

			m_fUsingFull = (wData & Bit(4)) ? true : false;

			m_uPhyState  = phyIdle;

			OnLinkUp();
			}

		return;
		}
	}

// Event Handlers

void CNic437::OnRecv(void)
{
	UINT uCount = 0;
		
	for(;;) {

		CNicBuffer *pBuff = &m_pRxBuff[m_uRxTail];

		CBuffDesc  *pDesc = pBuff->m_pDesc;

		if( !(pDesc->m_Flags & flagOwner) ) {
						
			m_uRxTail = (m_uRxTail + 1) % constRxLimit;
			
			uCount    = uCount + 1;

			if( pDesc->m_Flags & flagSop ) {

				if( pDesc->m_Flags & flagTeardown ) {

					break;
					}
				}

			if( pDesc->m_Flags & flagEop ) {
					
				if( pDesc->m_Flags & flagEoq ) {

					if( pDesc->m_NextPtr ) {

						AfxTrace("nic437: recv eoq misqueue recovery.\n");

						m_pEnet->SetRamRxHd(m_uChan, pDesc->m_NextPtr);								
						}

					break;
					}
				}			
			
			continue;
			}

		break;
		}

	m_pEnet->SetRamRxCp(m_uChan);

	m_pRxFlag->Signal(uCount);
	}

void CNic437::OnSend(void)
{
	UINT uCount = 0;

	while( m_uTxHead != m_uTxTail ) {

		CBuffDesc *pDesc = m_pTxBuff[m_uTxHead].m_pDesc;

		if( !(pDesc->m_Flags & flagOwner) ) {

			m_uTxHead = (m_uTxHead + 1) % constTxLimit;

			uCount    = uCount + 1;

			if( pDesc->m_Flags & flagSop ) {

				if( pDesc->m_Flags & flagTeardown ) {

					break;
					}
				}

			if( pDesc->m_Flags & flagEop ) {
					
				if( pDesc->m_Flags & flagEoq ) {

					if( pDesc->m_NextPtr ) {

						AfxTrace("nic437: send eoq misqueue recovery.\n");

						m_pEnet->SetRamTxHd(m_uChan, pDesc->m_NextPtr);

						continue;
						}

					m_uTxLast = NOTHING;

					break;
					}
				}
				
			continue;
			}

		break;
		}

	m_pEnet->SetRamTxCp(m_uChan);

	m_pTxFlag->Signal(uCount);
	}

void CNic437::OnLinkUp(void)
{
	if( m_uNicState == nicOpen ) {

		ResetState();

		m_pEnet->SetMac(m_uPort, m_MacAddr);

		DWORD dwDesc = phal->VirtualToPhysical(DWORD(m_pRxDesc));

		m_pEnet->StartController(m_uPort, dwDesc, m_fUsingFast, m_fUsingFull);

		m_pLinkDown->Clear();

		m_pLinkOkay->Set();

		m_uNicState = nicActive;
		}
	}

void CNic437::OnLinkDown(void)
{
	if( m_uNicState == nicActive ) {

		m_pLinkOkay->Clear();

		m_pLinkDown->Set();

		m_pEnet->StopController(m_uPort, m_uChan);

		m_uNicState = nicOpen;
		}
	}

// Buffer Management

void CNic437::AllocBuffers(void)
{
	m_pTxBuff = New CNicBuffer[ constTxLimit ];

	m_pRxBuff = New CNicBuffer[ constRxLimit ];

	m_pTxDesc = (CBuffDesc *) AllocNoneCached(sizeof(CBuffDesc) * constTxLimit, 16);

	m_pRxDesc = (CBuffDesc *) AllocNoneCached(sizeof(CBuffDesc) * constRxLimit, 16);

	m_pTxData = (PBYTE)       AllocNoneCached(constTxLimit * constBuffSize, 16);

	m_pRxData = (PBYTE)       AllocNoneCached(constRxLimit * constBuffSize, 16);

	ResetBuffers();
	}

PVOID CNic437::AllocNoneCached(UINT uAlloc, UINT uAlign)
{
	// TODO - Make shared.

	uAlign = RoundUp(uAlign, 64);

	uAlloc = RoundUp(uAlloc, 64);

	PVOID pMem  = memalign(uAlign, uAlloc);

	DWORD Alias = phal->GetNonCachedAlias(DWORD(pMem));

	DWORD Phy   = phal->VirtualToPhysical(DWORD(pMem));

	ProcPurgeDataCache(Phy, uAlloc);

	return PVOID(Alias);
	}

void CNic437::ResetBuffers(void)
{
	memset(m_pTxDesc, 0, sizeof(CBuffDesc) * constTxLimit);

	memset(m_pRxDesc, 0, sizeof(CBuffDesc) * constRxLimit);

	for( UINT t = 0; t < constTxLimit; t ++ ) {

		CNicBuffer &Buff = m_pTxBuff[t];

		CBuffDesc  &Desc = m_pTxDesc[t];

		Buff.m_iIndex    = t;

		Buff.m_pDesc     = &Desc;

		Buff.m_dwDesc    = phal->VirtualToPhysical(DWORD(Buff.m_pDesc));

		Buff.m_pData     = m_pTxData + t * constBuffSize;

		Buff.m_dwData    = phal->VirtualToPhysical(DWORD(Buff.m_pData));

		Desc.m_BuffPtr   = Buff.m_dwData;
		}

	for( UINT r = 0; r < constRxLimit; r ++ ) {

		CNicBuffer &Buff = m_pRxBuff[r];

		CBuffDesc  &Desc = m_pRxDesc[r];

		Buff.m_iIndex    = r;

		Buff.m_pDesc     = &Desc;

		Buff.m_dwDesc    = phal->VirtualToPhysical(DWORD(Buff.m_pDesc));

		Buff.m_pData     = m_pRxData + r * constBuffSize;

		Buff.m_dwData    = phal->VirtualToPhysical(DWORD(Buff.m_pData));

		Desc.m_BuffPtr   = Buff.m_dwData;

		Desc.m_BuffLen   = constBuffSize;

		Desc.m_Flags     = flagOwner;

		if( r < (constRxLimit - 1) ) {

			DWORD dwNext   = DWORD(&m_pRxDesc[r+1]); 

			Desc.m_NextPtr = phal->VirtualToPhysical(dwNext);
			}
		}

	m_uTxHead = 0;

	m_uTxTail = 0;

	m_uRxHead = 0;

	m_uRxTail = 0;

	m_uRxLast = constRxLimit - 1;

	m_uTxLast = NOTHING;
	}

void CNic437::FreeBuffers(void)
{
	free(m_pTxDesc);

	free(m_pRxDesc);

	free(m_pTxData);

	free(m_pRxData);

	delete [] m_pTxBuff;

	delete [] m_pRxBuff;
	}

void CNic437::StartRecv(CNicBuffer *pBuff)
{
	CNicBuffer *pLast = &m_pRxBuff[m_uRxLast];

	pBuff->m_pDesc->m_NextPtr = NULL; 

	pBuff->m_pDesc->m_BuffLen = constBuffSize;

	pBuff->m_pDesc->m_BuffOff = 0;
	
	pBuff->m_pDesc->m_Flags   = flagOwner;

	pLast->m_pDesc->m_NextPtr = pBuff->m_dwDesc;

	m_uRxLast  = (m_uRxLast + 1) % constRxLimit;

	if( !m_pEnet->GetRamRxHd(m_uChan) ) {

		AfxTrace("enet437: recv stall recovery.\n");

		DWORD dwDesc = DWORD(pBuff->m_pDesc);

		DWORD dwAddr = phal->VirtualToPhysical(dwDesc);
		
		m_pEnet->SetRamRxHd(m_uChan, dwAddr);
		}
	}

void CNic437::StartSend(CNicBuffer *pBuff)
{
	pBuff->m_pDesc->m_NextPtr   = NULL; 

	pBuff->m_pDesc->m_BuffOff   = 0;

	pBuff->m_pDesc->m_PacketLen = pBuff->m_pDesc->m_BuffLen;
	
	pBuff->m_pDesc->m_Flags     = flagSop | flagEop | flagOwner | flagToPortEn | (m_uPort);

	m_pEnet->ClrDmaTxIntEn(m_uChan);

	if( m_uTxLast != NOTHING ) {

		m_pTxBuff[m_uTxLast].m_pDesc->m_NextPtr = pBuff->m_dwDesc;
		}
	else 
		m_pEnet->SetRamTxHd(m_uChan, pBuff->m_dwDesc);

	m_uTxLast = pBuff->m_iIndex;

	m_pEnet->SetDmaTxIntEn(m_uChan);
	}

// Mii Managment

WORD CNic437::AppGetMii(BYTE bAddr)
{
	PhyInterrupt(false);

	m_uMiiState = miiAppGet;

	m_pEnet->GetMii(m_uPhy, bAddr);

	if( !m_pMiiFlag->Wait(1000) ) {

		AfxTrace("PHY Timeout on GetMII\n");

		PhyInterrupt(true);

		return 0;
		}

	return m_pEnet->ReadMiiReg();
	}

BOOL CNic437::AppPutMii(BYTE bAddr, WORD wData)
{
	PhyInterrupt(false);

	m_uMiiState = miiAppPut;

	m_pEnet->PutMii(m_uPhy, bAddr, wData);

	if( !m_pMiiFlag->Wait(1000) ) {

		AfxTrace("PHY Timeout on PutMII\n");

		PhyInterrupt(true);

		return false;
		}

	return true;
	}

void CNic437::IntGetMii(BYTE bAddr)
{
	PhyInterrupt(false);

	m_uMiiState = miiIntGet;

	m_pEnet->GetMii(m_uPhy, bAddr);
	}

void CNic437::IntPutMii(BYTE bAddr, WORD wData)
{
	m_uMiiState = miiIntPut;

	m_pEnet->PutMii(m_uPhy, bAddr, wData);
	}

// End of File
