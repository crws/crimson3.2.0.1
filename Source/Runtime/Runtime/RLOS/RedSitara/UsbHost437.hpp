
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Hardware Drivers
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_UsbHost437_HPP
	
#define	INCLUDE_AM437_UsbHost437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbBase437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Usb Host Device 
//

class CUsbHost437 : public CUsbBase437, public IUsbHostHardwareDriver
{
	public:
		// Constructor
		CUsbHost437(UINT iIndex);

		// Destructor
		~CUsbHost437(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbDriver
		BOOL METHOD Bind(IUsbEvents *pEvents);
		BOOL METHOD Bind(IUsbDriver *pDriver);
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);
	
		// IUsbHostHardwareDriver
		DWORD METHOD GetBaseAddr(void);
		UINT  METHOD GetMemAlign(UINT uType);
		void  METHOD MemCpy(PVOID d, PCVOID s, UINT n);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Registers
		enum
		{
			regCAPLENGTH		= 0x0000 / sizeof(DWORD),
			regHCSPARAMS1           = 0x0004 / sizeof(DWORD),
			regHCSPARAMS2           = 0x0008 / sizeof(DWORD),
			regHCSPARAMS3           = 0x000C / sizeof(DWORD),
			regHCCPARAMS            = 0x0010 / sizeof(DWORD),
			regDBOFF                = 0x0014 / sizeof(DWORD),
			regRTSOFF               = 0x0018 / sizeof(DWORD),
			regUSBCMD               = 0x0020 / sizeof(DWORD),
			regUSBSTS               = 0x0024 / sizeof(DWORD),
			regPAGESIZE             = 0x0028 / sizeof(DWORD),
			regDNCTRL               = 0x0034 / sizeof(DWORD),
			regCRCR_LO              = 0x0038 / sizeof(DWORD),
			regCRCR_HI              = 0x003C / sizeof(DWORD),
			regDCBAAP_LO            = 0x0050 / sizeof(DWORD),
			regDCBAAP_HI            = 0x0054 / sizeof(DWORD),
			regCONFIG               = 0x0058 / sizeof(DWORD),
			regPORTSC2              = 0x0430 / sizeof(DWORD),
			regPORTPMSC2            = 0x0434 / sizeof(DWORD),
			regPORTLI2              = 0x0438 / sizeof(DWORD),
			regPORTHLPMC2           = 0x043C / sizeof(DWORD),
			regPORTSC1              = 0x0420 / sizeof(DWORD),
			regPORTPMSC1            = 0x0424 / sizeof(DWORD),
			regPORTLI1              = 0x0428 / sizeof(DWORD),
			regPORTHLPMC1           = 0x042C / sizeof(DWORD),
			regMFINDEX              = 0x0440 / sizeof(DWORD),
			regIMAN			= 0x0460 / sizeof(DWORD),
			regIMOD			= 0x0464 / sizeof(DWORD),
			regERSTSZ		= 0x0468 / sizeof(DWORD),
			regERSTBA_LO		= 0x0470 / sizeof(DWORD),
			regERSTBA_HI		= 0x0474 / sizeof(DWORD),
			regERDP_LO		= 0x0478 / sizeof(DWORD),
			regERDP_HI		= 0x047C / sizeof(DWORD),
			regDB			= 0x04E0 / sizeof(DWORD),
			regUSBLEGSUP            = 0x08E0 / sizeof(DWORD),
			regUSBLEGCTLSTS         = 0x08E4 / sizeof(DWORD),
			regSUPTPRT2_DW		= 0x08F0 / sizeof(DWORD),
			regDCID                 = 0x0910 / sizeof(DWORD),
			regDCDB                 = 0x0914 / sizeof(DWORD),
			regDCERSTSZ             = 0x0918 / sizeof(DWORD),
			regDCERSTBA_LO          = 0x0920 / sizeof(DWORD),
			regDCERSTBA_HI          = 0x0924 / sizeof(DWORD),
			regDCERDP_LO            = 0x0928 / sizeof(DWORD),
			regDCERDP_HI            = 0x092C / sizeof(DWORD),
			regDCCTRL               = 0x0930 / sizeof(DWORD),
			regDCST                 = 0x0934 / sizeof(DWORD),
			regDCPORTSC             = 0x0938 / sizeof(DWORD),
			regDCCP_LO              = 0x0940 / sizeof(DWORD),
			regDCCP_HI              = 0x0944 / sizeof(DWORD),
			regDCDDI1               = 0x0948 / sizeof(DWORD),
			regDCDDI2               = 0x094C / sizeof(DWORD),
			};

		// Data
		IUsbHostHardwareEvents * m_pDriver;

		// Core
		void WaitReady(void);
		void ResetCore(void);
	};

// End of File

#endif
