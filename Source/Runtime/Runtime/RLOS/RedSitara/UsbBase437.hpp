
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_UsbBase437_HPP
	
#define	INCLUDE_AM437_UsbBase437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 Usb Base Class 
//

class CUsbBase437 : public IEventSink
{
	public:
		// Constructor
		CUsbBase437(UINT iIndex);

		// Destructor
		virtual ~CUsbBase437(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Wrapper Registers
		enum
		{
			regREVISION		= 0x000 / sizeof(DWORD),
			regSYSCONFIG		= 0x010 / sizeof(DWORD),
			regIRQEOIMAIN		= 0x018 / sizeof(DWORD),
			regIRQSTATUSRAW		= 0x020 / sizeof(DWORD),
			regIRQENABLESETMAIN  	= 0x028 / sizeof(DWORD),
			regIRQENABLECLRMAIN	= 0x02C / sizeof(DWORD),
			regIRQSTATUSMAIN 	= 0x024 / sizeof(DWORD),
			regIRQEOIMISC		= 0x42C / sizeof(DWORD),
			regIRQSTATUSRAWMISC	= 0x430 / sizeof(DWORD),
			regIRQSTATUSMISC	= 0x434 / sizeof(DWORD),
			regIRQENABLESETMISC	= 0x438 / sizeof(DWORD),
			regIRQENABLECLRMISC	= 0x43C / sizeof(DWORD),
			regUTMIOTGCTRL		= 0x500 / sizeof(DWORD),
			regUTMIOTGSTATUS	= 0x504 / sizeof(DWORD),
			regTXFIFODEPTH		= 0x508 / sizeof(DWORD),
			regRXFIFODEPTH		= 0x50C / sizeof(DWORD),
			regMMRAMOFFSET		= 0x700 / sizeof(DWORD),
			regFLADJ		= 0x704 / sizeof(DWORD),
			regDEBUGDATA		= 0x70C / sizeof(DWORD),
			regDEBUGCFG		= 0x708 / sizeof(DWORD),
			regDEVEBCEN		= 0x710 / sizeof(DWORD)
			};

		// Core Registers
		enum
		{
			regGSBUSCFG0            = 0xC100 / sizeof(DWORD),
			regGSBUSCFG1            = 0xC104 / sizeof(DWORD),
			regGTXTHRCFG            = 0xC108 / sizeof(DWORD),
			regGRXTHRCFG            = 0xC10C / sizeof(DWORD),
			regGCTL                 = 0xC110 / sizeof(DWORD),
			regGSTS                 = 0xC118 / sizeof(DWORD),
			regGSNPSID              = 0xC120 / sizeof(DWORD),
			regGGPIO                = 0xC124 / sizeof(DWORD),
			regGUID                 = 0xC128 / sizeof(DWORD),
			regGBUSERRADDRLO        = 0xC130 / sizeof(DWORD),
			regGBUSERRADDRHI        = 0xC134 / sizeof(DWORD),
			regGPRTBIMAPLO          = 0xC138 / sizeof(DWORD),
			regGPRTBIMAPHI          = 0xC13C / sizeof(DWORD),
			regGHWPARAMS		= 0xC140 / sizeof(DWORD),
			regGDBGFIFOSPACE        = 0xC160 / sizeof(DWORD),
			regGDBGLTSSM            = 0xC164 / sizeof(DWORD),
			regGUCTL                = 0xC12C / sizeof(DWORD),
			regGPRTBIMAP_HSLO       = 0xC180 / sizeof(DWORD),
			regGPRTBIMAP_HSHI       = 0xC184 / sizeof(DWORD),
			regGPRTBIMAP_FSLO       = 0xC188 / sizeof(DWORD),
			regGPRTBIMAP_FSHI       = 0xC18C / sizeof(DWORD),
			regGDBGLSPMUX           = 0xC170 / sizeof(DWORD),
			regGDBGLSP              = 0xC174 / sizeof(DWORD),
			regGDBGEPINFO0          = 0xC178 / sizeof(DWORD),
			regGDBGEPINFO1          = 0xC17C / sizeof(DWORD),
			regGDBGLNMCC            = 0xC168 / sizeof(DWORD),
			regGDBGBMU              = 0xC16C / sizeof(DWORD),
			regGUSB2PHYCFG          = 0xC200 / sizeof(DWORD),
			regGUSB2PHYACC          = 0xC280 / sizeof(DWORD),
			regGUSB3PIPECTL         = 0xC2C0 / sizeof(DWORD),
			regGTXFIFOSIZE		= 0xC300 / sizeof(DWORD),
			regGRXFIFOSIZE		= 0xC380 / sizeof(DWORD),
			regGEVNTADRLO		= 0xC400 / sizeof(DWORD),
			regGEVNTADRHI		= 0xC404 / sizeof(DWORD),
			regGEVNTSIZ		= 0xC408 / sizeof(DWORD),
			regGEVNTCOUNT		= 0xC40C / sizeof(DWORD),
			regGHWPARAMS8           = 0xC600 / sizeof(DWORD),
			regGHWPARAMS9           = 0xC604 / sizeof(DWORD),
			regGTXFIFOPRIDEV        = 0xC610 / sizeof(DWORD),
			regGTXFIFOPRIHST        = 0xC618 / sizeof(DWORD),
			regGRXFIFOPRIHST        = 0xC61C / sizeof(DWORD),
			regGFIFOPRIDBC          = 0xC620 / sizeof(DWORD),
			regGDMAHLRATIO          = 0xC624 / sizeof(DWORD),
			};

		// Modes
		enum
		{
			modeOnTheGo		= 0,	
			modeHost		= 1,
			modeDevice		= 2,
			};

		// Data Members
		ULONG    m_uRefs;
		PVDWORD	 m_pBaseWrap;
		PVDWORD	 m_pBaseCore;
		PVDWORD	 m_pBasePhy;  
		UINT	 m_uLineCore;
		UINT	 m_uLineMisc;

		// Operations
		void SetMode(UINT uMode);
		void ResetWrapper(void);
		void EnableEvents(void);
		void DisableEvents(void);
		void EnableCoreInts(void);
		void DisableCoreInts(void);
		void FindModule(UINT iIndex);

		// Memory Helpers
		PVOID AllocNonCached(UINT uSize);
		PVOID AllocNonCached(UINT uSize, UINT uAlign);
		void  FreeNonCached(PVOID pMem);
	};

// End of File

#endif
