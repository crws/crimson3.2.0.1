
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_SdHost437_HPP
	
#define	INCLUDE_AM437_SdHost437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../RedDev/SdHostGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CClock437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 SD Host Controller
//

class CSdHost437 : public CSdHostGeneric, public IEventSink
{
	public:
		// Constructor
		CSdHost437(CClock437 *pClock);

		// Destructor
		~CSdHost437(void);

		// IDevice
		BOOL METHOD Open(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// System Registers
		enum 
		{
			regConfig   = 0x0110 / sizeof(DWORD),
			regStatus   = 0x0114 / sizeof(DWORD), 
			regTest	    = 0x0128 / sizeof(DWORD), 
			regControl  = 0x012C / sizeof(DWORD), 
			regPower    = 0x0130 / sizeof(DWORD), 
			};

		// Interrupt Masks
		enum
		{
			irqFailCmd  = Bit(16) | Bit(17) | Bit(18) | Bit(19),
			irqFailData = Bit(20) | Bit(21) | Bit(22),
			irqCommand  = Bit(0),
			irqTransfer = Bit(1),
			irqWrite    = Bit(4),
			irqRead	    = Bit(5),
			irqInsert   = Bit(6),
			irqRemove   = Bit(7),
			irqAll	    = irqFailCmd  | irqFailData | irqCommand | irqTransfer | irqWrite | irqRead | irqInsert | irqRemove,
			};

		// Data Members
		PVDWORD	  m_pSys;
		UINT	  m_uClock;
		UINT	  m_uLine;
		UINT	  m_uDetect;
		UINT	  m_uBounce;
		IGpio   * m_pGpio;
		ITimer  * m_pTimer;
		IEvent	* m_pCommand;
		IEvent	* m_pTransfer;
		IEvent	* m_pWrite;
		IEvent	* m_pRead;
		IEvent	* m_pFailCmd;
		IEvent	* m_pFailData;

		// Controller Interface
		void ResetController(void);
		void InitController(void);
		void StopController(void);
		void SetClockSpeed(EClock Clock);
		void SetBusWidth(bool fWide);
		void SetLength(UINT uBytes);

		// Blocking Calls
		void ClearEvents(void);
		bool WaitCmdComplete(void);
		bool SendData(PCBYTE pData, UINT uCount);
		bool ReadData(PBYTE pData, UINT uCount);
		bool CheckBusy(void);

		// Implementation
		void EnableTimer(void);
		void EnableEvents(void);
		void DisableEvents(void);
		void HardReset(void);
		void SoftReset(void);
		void EnableExtClock(void);
		void EnableIntClock(void);
		void DisableExtClock(void);
	};

// End of File

#endif
