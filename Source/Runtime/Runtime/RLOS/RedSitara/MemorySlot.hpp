
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AM437_MemroySlot_HPP

#define	INCLUDE_AM437_MemroySlot_HPP

//////////////////////////////////////////////////////////////////////////
//
// Serial Memory Support
//

enum DLLAPI
{
	addrDatabase		= 0x0000,		// 4
	addrMounted		= 0x0004,		// 4
	addrLoading		= 0x0008,		// 2
	addrDisplay		= 0x000A,		// 6
	addrTimeMagic		= 0x0010,		// 4
	addrTimeZoneHours	= 0x0014,		// 1
	addrTimeDST		= 0x0015,		// 1
	addrTimeZoneMinutes	= 0x0016,		// 2

	addrLanguage		= 0x0020,		// 4
	addrMacId0		= 0x002C,		// 6
	addrMacId1		= 0x0032,		// 6

	addrRtc			= 0x0040,		// 8

	addrTouch		= 0x0050,		// 10
	addrFastFile		= 0x0060,		// 4
	addrBattery		= 0x0064,		// 2
	addrSignalLed		= 0x0068,		// 4

	addrDNS0		= 0x0070,		// 6
	addrDNS1		= 0x0076,		// 6
	addrNetCfgComp		= 0x007C,		// 2
	addrNetCfgSize		= 0x007E,		// 2
	addrNetCfgData		= 0x0080,		// 888

	addrCFData		= 0x0400,		// 512
	addrCFMedia		= 0x0600,		// 4
	addrCFLBA		= 0x0604,		// 4
	addrCFState		= 0x0608,		// 4

	addrDataLog0		= 0x0670,		// 16
	addrDataLog1		= 0x0680,		// 16
	addrDataLog2		= 0x0690,		// 16
	addrDataLog3		= 0x06A0,		// 16
	addrDataLog4		= 0x06B0,		// 16
	addrDataLog5		= 0x06C0,		// 16
	addrDataLog6		= 0x06D0,		// 16
	addrDataLog7		= 0x06E0,		// 16

	addrTrap0		= 0x0700,		// 256
	addrTrap1		= 0x0800,		// 256
	addrTrap2		= 0x0900,		// 256
	addrTrap3		= 0x0A00,		// 256
	addrTrap4		= 0x0B00,		// 256
	addrTrap5		= 0x0C00,		// 256
	addrTrap6		= 0x0D00,		// 256
	addrTrap7		= 0x0E00,		// 256

	addrIdentity		= 0x1000,		// 4096

	addrModule0		= 0x2000,		// 64
	addrModule1		= 0x2040,		// 64
	addrModule2		= 0x2080,		// 64
	addrModule3		= 0x20C0,		// 64
	addrModule4		= 0x2100,		// 64
	addrModule5		= 0x2140,		// 64
	addrModule6		= 0x2180,		// 64
	addrModule7		= 0x21C0,		// 64
	addrModule8		= 0x2200,		// 64
	addrModule9		= 0x2240,		// 64
	addrModule10		= 0x2280,		// 64
	addrModule11		= 0x22C0,		// 64
	addrModule12		= 0x2300,		// 64
	addrModule13		= 0x2340,		// 64
	addrModule14		= 0x2380,		// 64
	addrModule15		= 0x23C0,		// 64

	addrControl		= 0x3000,		// 8192
	};

// End of File

#endif
