
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Gpmc437_HPP
	
#define	INCLUDE_AM437_Gpmc437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 General Purpose Memory Controller
//

class CGpmc437 : public IEventSink
{
	public:
		// Constructor
		CGpmc437(void);

		// Attributes
		UINT GetLine(void) const;

		// Operations
		void SetTimeout(DWORD dwTimeout);
		void SetLimitedAddress(bool fLimited);
		void SetWritProtect(bool fSet);

		// Chip Select
		void SetChipSelType(UINT i, UINT uType);
		void SetChipSelWidth(UINT i, UINT uWidth);
		void SetChipSelMux(UINT i, UINT uMode);
		void SetChipSelWaitConfig(UINT i, UINT uPin, bool fActiveHigh);
		void SetChipSelBurst(UINT i, UINT uSize);
		void SetChipSelTimeGranularity(UINT i, UINT uGranularity);
		void SetChipSelAccessType(UINT i, bool fWriteAsync, bool fReadAsync);
		void SetChipSelAccessMode(UINT i, bool fWriteSingle, bool fReadSingle);
		void SetChipSelTiming(UINT i, UINT uOnTime, UINT uWrOffTime, UINT uRdOffTime, bool fExtraDelay);
		void SetChipSelAdvTiming(UINT i, UINT uOnTime, UINT uWrOffTime, UINT uRdOffTime, bool fExtraDelay, UINT uMuxOnTime, UINT uMuxWrOffTime, UINT uMuxRdOffTime);
		void SetChipSelWETimings(UINT i, UINT uOffTime, UINT uOnTime, bool fExtraDelay);
		void SetChipSelOETimings(UINT i, UINT uOffTime, UINT uOnTime, bool fExtraDelay);
		void SetChipSelOEMuxTimings(UINT i, UINT uMuxOffTime, UINT uMuxOnTime);
		void SetChipSelRdAccessTiming(UINT i, UINT uBurst, UINT uRdAccess, UINT uWrCycle, UINT uRdCycle);
		void SetChipSelWrAccessTiming(UINT i, UINT uWrAccess, UINT uWrDataMux);
		void SetChipSelCycleDelayTiming(UINT i, UINT uDelay, UINT uDelaySame, UINT uDelayDiff, UINT uTurnAround); 
		void SetChipSelBase(UINT i, DWORD dwBase, UINT uSize);
		void SetChipSelEnable(UINT i, bool fEnable);
		void SetChipSelEventHook(UINT i, IEventSink *pSink);

		// Ecc
		void  SetEccCode(UINT uCode); 
		void  SetEccChipSelect(UINT uChip);
		void  SetEcc16(bool fBit16);
		void  SetEccBchConfig(UINT uBits, UINT uWrapMode, UINT uPageSize);
		void  SetEccEnable(bool fEnable);
		void  SetEccClear(void);
		void  SetEccPointer(UINT uPointer);
		void  SetEccSize(UINT uBytes0, UINT uBytes1);
		void  SetEccResultSize(UINT uSel, UINT uSizeSel);
		DWORD GetEccResult(UINT uSelect);
		DWORD GetBchResult(UINT uSelect);

		// Nand
		void PutNandCmd (UINT iChipSel, BYTE bCmd );
		void PutNandAddr(UINT iChipSel, BYTE bAddr);
		void PutNandData(UINT iChipSel, BYTE bData);
		BYTE GetNandData(UINT iChipSel);
		void SendNandData(UINT iChipSel, PCBYTE pData, UINT uCount);
		void ReadNandData(UINT iChipSel, PBYTE pData, UINT uCount);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// Types
		enum
		{
			typeNor		= 0,
			typeNand	= 2,
			};

		// Mux
		enum
		{	
			muxNone		= 0,
			muxAAD		= 1,
			muxAddrData	= 2,
			};

		// ECC
		enum
		{
			eccHamming	= 0,
			eccBCH		= 1,
			};

	protected:
		// Registers
		enum
		{
			regREV		= 0x0000 / sizeof(DWORD),
			regSYSCFG	= 0x0010 / sizeof(DWORD),
			regSYSSTS	= 0x0014 / sizeof(DWORD),
			regIRQSTS	= 0x0018 / sizeof(DWORD),
			regIRQEN	= 0x001C / sizeof(DWORD),
			regTIMEOUT	= 0x0040 / sizeof(DWORD),
			regERRADDR	= 0x0044 / sizeof(DWORD),
			regERRTYPE	= 0x0048 / sizeof(DWORD),
			regCFG		= 0x0050 / sizeof(DWORD),
			regSTS		= 0x0054 / sizeof(DWORD),
			regCFG1x	= 0x0060 / sizeof(DWORD),
			regCFG2x	= 0x0064 / sizeof(DWORD),
			regCFG3x	= 0x0068 / sizeof(DWORD),
			regCFG4x	= 0x006C / sizeof(DWORD),
			regCFG5x	= 0x0070 / sizeof(DWORD),
			regCFG6x	= 0x0074 / sizeof(DWORD),
			regCFG7x	= 0x0078 / sizeof(DWORD),
			regCMDx		= 0x007C / sizeof(DWORD),
			regADDRx	= 0x0080 / sizeof(DWORD),
			regDATAx	= 0x0084 / sizeof(DWORD),
			regPRECFG1	= 0x01E0 / sizeof(DWORD),
			regPRECFG2	= 0x01E4 / sizeof(DWORD),
			regPRECTRL	= 0x01EC / sizeof(DWORD),
			regPRESTS	= 0x01F0 / sizeof(DWORD),
			regECCCFG	= 0x01F4 / sizeof(DWORD),
			regECCCTRL	= 0x01F8 / sizeof(DWORD),
			regSIZECFG	= 0x01FC / sizeof(DWORD),
			regECCRES	= 0x0200 / sizeof(DWORD),
			regBCHRES0	= 0x0240 / sizeof(DWORD),
			regBCHRES4	= 0x0300 / sizeof(DWORD),
			regBCHDATA	= 0x02D0 / sizeof(DWORD),
			};

		// Data Members
		PVDWORD	     m_pBase;
		IEventSink * m_pHook[2];
		UINT 	     m_uLine;
		 
		// Implementation
		void Reset(void);
		void Init(void);
		void EnableEvents(void);
	};

// End of File

#endif
