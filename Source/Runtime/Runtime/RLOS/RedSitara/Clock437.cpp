
#include "Intern.hpp"

#include "Clock437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ctrl437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Clock Module
//

// Register Access

#define RegWUP(x) (m_pBaseWUP[reg##x])

#define RegPER(x) (m_pBasePER[reg##x])

#define RegDEV(x) (m_pBaseDEV[reg##x])

#define RegPLL(x) (m_pBasePLL[reg##x])

#define RegRTC(x) (m_pBaseRTC[reg##x])

// Constructor

CClock437::CClock437(CCtrl437 *pCtrl)
{
	m_pBaseWUP = PVDWORD(ADDR_CM_WKUP);

	m_pBasePER = PVDWORD(ADDR_CM_PER);

	m_pBaseDEV = PVDWORD(ADDR_CM_DEVICE);

	m_pBasePLL = PVDWORD(ADDR_CM_DPLL);

	m_pBaseRTC = PVDWORD(ADDR_CM_RTC);

	m_uRef     = pCtrl->GetRefFreq();
	}

// PLLs

DWORD CClock437::GetCorePLLFreq(void)
{
	DWORD m = (RegWUP(CLKSELDPLLCORE) >> 8) & 0x3FF;

	DWORD n = (RegWUP(CLKSELDPLLCORE) >> 0) & 0x07F;

	return (INT64(m_uRef) * m * 2) / (n + 1);
	}

DWORD CClock437::GetCoreM4Freq(void)
{
	DWORD m = RegWUP(DIVM4DPLLCORE) & 0x01F;
	
	return GetCorePLLFreq() / m;
	}

DWORD CClock437::GetCoreM5Freq(void)
{
	DWORD m = RegWUP(DIVM5DPLLCORE) & 0x01F;
	
	return GetCorePLLFreq() / m;
	}

DWORD CClock437::GetCoreM6Freq(void)
{
	DWORD m = RegWUP(DIVM6DPLLCORE) & 0x01F;
	
	return GetCorePLLFreq() / m;
	}

DWORD CClock437::GetPerPLLFreq(void)
{
	DWORD m = (RegWUP(CLKSELDPLLPER) >> 8) & 0xFFF;

	DWORD n = (RegWUP(CLKSELDPLLPER) >> 0) & 0x0FF;

	return (INT64(m_uRef) * m) / (n + 1);
	}

DWORD CClock437::GetPerM2Freq(void)
{
	DWORD m = RegWUP(DIVM2DPLLPER) & 0x03F;
	
	return GetPerPLLFreq() / m;
	}

DWORD CClock437::GetMpuFreq(void)
{
	DWORD m  = (RegWUP(CLKSELDPLLMPU) >> 8) & 0x3FF;

	DWORD n  = (RegWUP(CLKSELDPLLMPU) >> 0) & 0x07F;

	DWORD m2 = RegWUP(DIVM2DPLLMPU) & 0x01F;

	return (INT64(m_uRef) * m) / (m2 * (n + 1));
	}

DWORD CClock437::GetDdrFreq(void)
{
	DWORD m  = (RegWUP(CLKSELDPLLDDR) >> 8) & 0x3FF;

	DWORD n  = (RegWUP(CLKSELDPLLDDR) >> 0) & 0x07F;

	DWORD m2 = RegWUP(DIVM2DPLLDDR) & 0x01F;

	DWORD m4 = RegWUP(DIVM4DPLLDDR) & 0x01F;

	return (INT64(m_uRef) * m) / (m2 * (n + 1));
	}

DWORD CClock437::GetDispFreq(void)
{
	DWORD m  = (RegWUP(CLKSELDPLLDISP) >> 8) & 0x7FF;

	DWORD n  = (RegWUP(CLKSELDPLLDISP) >> 0) & 0x07F;

	DWORD m2 = RegWUP(DIVM2DPLLDISP) & 0x01F;

	return (INT64(m_uRef) * m) / (m2 * (n + 1));
	}

DWORD CClock437::GetExternFreq(void)
{
	DWORD m  = (RegWUP(CLKSELDPLLEXT) >> 8) & 0xFFF;

	DWORD n  = (RegWUP(CLKSELDPLLEXT) >> 0) & 0x0FF;

	DWORD m2 = RegWUP(DIVM2DPLLEXT) & 0x01F;

	return (INT64(m_uRef) * m) / (m2 * (n + 1));
	}

void CClock437::SetCorePLLFreq(UINT m, UINT n, UINT m4, UINT m5, UINT m6)
{
	RegWUP(CLKMODEDPLLCORE) = 0x4;

	while( !(RegWUP(IDLESTDPLLCORE) & Bit(8)) );

	RegWUP(CLKSELDPLLCORE)  = ((m & 0x3FF) << 8) | (n & 0x07F);

	RegWUP(DIVM4DPLLCORE)   = (m4 & 0x01F);

	RegWUP(DIVM5DPLLCORE)   = (m5 & 0x01F);

	RegWUP(DIVM6DPLLCORE)   = (m6 & 0x01F);

	RegWUP(CLKMODEDPLLCORE) = 0x7;

	while( !(RegWUP(IDLESTDPLLCORE) & Bit(0)) );
	}

void CClock437::SetPerPLLFreq(UINT m, UINT n, UINT m2)
{
	RegWUP(CLKMODEDPLLPER) = 0x4;

	while( !(RegWUP(IDLESTDPLLPER) & Bit(8)) );

	RegWUP(CLKSELDPLLPER)  = ((m & 0xFFF) << 8) | (n & 0x0FF);

	RegWUP(DIVM2DPLLPER)   = (m2 & 0x03F);

	RegWUP(CLKMODEDPLLPER) = 0x7;

	while( !(RegWUP(IDLESTDPLLPER) & Bit(0)) );
	}

void CClock437::SetMpuFreq(UINT m, UINT n, UINT m2)
{
	RegWUP(CLKMODEDPLLMPU) = 0x4;

	while( !(RegWUP(IDLESTDPLLMPU) & Bit(8)) );

	RegWUP(CLKSELDPLLMPU)  = ((m & 0x3FF) << 8) | (n & 0x07F);

	RegWUP(DIVM2DPLLMPU)   = (m2 & 0x01F);

	RegWUP(CLKMODEDPLLMPU) = 0x7;

	while( !(RegWUP(IDLESTDPLLMPU) & Bit(0)) );
	}

void CClock437::SetMpuFreq(UINT n)
{
	RegWUP(CLKSELDPLLMPU) = (RegWUP(CLKSELDPLLMPU) & ~0x07F) | (n & 0x07F);
}

void CClock437::SetDdrFreq(UINT m, UINT n, UINT m2, UINT m4)
{
	RegWUP(CLKMODEDPLLDDR) = 0x4;

	while( !(RegWUP(IDLESTDPLLDDR) & Bit(8)) );

	RegWUP(CLKSELDPLLDDR)  = ((m & 0x3FF) << 8) | (n & 0x07F);

	RegWUP(DIVM2DPLLDDR)   = (m2 & 0x01F);

	RegWUP(DIVM4DPLLDDR)   = (m4 & 0x01F);

	RegWUP(CLKMODEDPLLDDR) = 0x7;

	while( !(RegWUP(IDLESTDPLLDDR) & Bit(0)) );
	}

void CClock437::SetDispFreq(UINT m, UINT n, UINT m2)
{
	RegWUP(CLKMODEDPLLDISP) = 0x4;

	while( !(RegWUP(IDLESTDPLLDISP) & Bit(8)) );

	RegWUP(CLKSELDPLLDISP)  = ((m & 0x7FF) << 8) | (n & 0x07F);

	RegWUP(DIVM2DPLLDISP)   = (m2 & 0x01F);

	RegWUP(CLKMODEDPLLDISP) = 0x7;

	while( !(RegWUP(IDLESTDPLLDISP) & Bit(0)) );
	}

// DLL 

void CClock437::WaitDLLReady(void)
{
	RegDEV(DLLCTRL) &= ~Bit(0);

	while( !(RegDEV(DLLCTRL) & Bit(2)) );
	}

// Peripheral Clocks

void CClock437::SetClockMode(UINT uClock, UINT uMode)
{
	switch( uClock ) {

		case clockL3:         RegPER(L3)        = uMode;  break;	 
		case clockOCMCRAM:    RegPER(OCMCRAM)   = uMode;  break;	 
		case clockVPFE0:      RegPER(VPFE0)     = uMode;  break;
		case clockVPFE1:      RegPER(VPFE1)     = uMode;  break;
		case clockTPCC:       RegPER(TPCC)      = uMode;  break;
		case clockTPTC0:      RegPER(TPTC0)     = uMode;  break;
		case clockTPTC1:      RegPER(TPTC1)     = uMode;  break;
		case clockTPTC2:      RegPER(TPTC2)     = uMode;  break;
		case clockDLLAGING:   RegPER(DLLAGING)  = uMode;  break;
		case clockL4HS:       RegPER(L4HS)      = uMode;  break;
		case clockL3S:        RegPER(L3S)       = uMode;  break;
		case clockGPMC:       RegPER(GPMC)      = uMode;  break;
		case clockADC0:       RegWUP(ADC0)      = uMode;  break;
		case clockADC1:       RegPER(ADC1)      = uMode;  break;
		case clockMCASP0:     RegPER(MCASP0)    = uMode;  break;
		case clockMCASP1:     RegPER(MCASP1)    = uMode;  break;
		case clockQSPI:       RegPER(QSPI)      = uMode;  break;
		case clockUSB0:       RegPER(USB0)      = uMode;  break;
		case clockUSB1:       RegPER(USB1)      = uMode;  break;
		case clockPRU:        RegPER(PRU)       = uMode;  break;
		case clockL4S:        RegPER(L4S)       = uMode;  break;
		case clockDCAN0:      RegPER(DCAN0)     = uMode;  break;
		case clockDCAN1:      RegPER(DCAN1)     = uMode;  break;
		case clockPWM0:       RegPER(PWM0)      = uMode;  break;
		case clockPWM1:       RegPER(PWM1)      = uMode;  break;
		case clockPWM2:       RegPER(PWM2)      = uMode;  break;
		case clockPWM3:       RegPER(PWM3)      = uMode;  break;
		case clockPWM4:       RegPER(PWM4)      = uMode;  break;
		case clockPWM5:       RegPER(PWM5)      = uMode;  break;
		case clockELM:        RegPER(ELM)       = uMode;  break;
		case clockGPIO0:      RegWUP(GPIO0)     = uMode;  break;
		case clockGPIO1:      RegPER(GPIO1)     = uMode;  break;
		case clockGPIO2:      RegPER(GPIO2)     = uMode;  break;
		case clockGPIO3:      RegPER(GPIO3)     = uMode;  break;
		case clockGPIO4:      RegPER(GPIO4)     = uMode;  break;
		case clockGPIO5:      RegPER(GPIO5)     = uMode;  break;
		case clockHDQ1W:      RegPER(HDQ1W)     = uMode;  break;
		case clockI2C0:	      RegWUP(I2C0)	= uMode;  break;
		case clockI2C1:       RegPER(I2C1)      = uMode;  break;
		case clockI2C2:       RegPER(I2C2)      = uMode;  break;
		case clockMAIL0:      RegPER(MAIL0)     = uMode;  break;
		case clockMMC0:       RegPER(MMC0)      = uMode;  break;
		case clockMMC1:       RegPER(MMC1)      = uMode;  break;
		case clockMMC2:       RegPER(MMC2)      = uMode;  break;
		case clockSPI0:       RegPER(SPI0)      = uMode;  break;
		case clockSPI1:       RegPER(SPI1)      = uMode;  break;
		case clockSPI2:       RegPER(SPI2)      = uMode;  break;
		case clockSPI3:       RegPER(SPI3)      = uMode;  break;
		case clockSPI4:       RegPER(SPI4)      = uMode;  break;
		case clockSPINLOCK:   RegPER(SPINLOCK)  = uMode;  break;
		case clockTIMER2:     RegPER(TIMER2)    = uMode;  break;
		case clockTIMER3:     RegPER(TIMER3)    = uMode;  break;
		case clockTIMER4:     RegPER(TIMER4)    = uMode;  break;
		case clockTIMER5:     RegPER(TIMER5)    = uMode;  break;
		case clockTIMER6:     RegPER(TIMER6)    = uMode;  break;
		case clockTIMER7:     RegPER(TIMER7)    = uMode;  break;
		case clockTIMER8:     RegPER(TIMER8)    = uMode;  break;
		case clockTIMER9:     RegPER(TIMER9)    = uMode;  break;
		case clockTIMER10:    RegPER(TIMER10)   = uMode;  break;
		case clockTIMER11:    RegPER(TIMER11)   = uMode;  break;
		case clockUART0:      RegWUP(UART0)     = uMode;  break;
		case clockUART1:      RegPER(UART1)     = uMode;  break;
		case clockUART2:      RegPER(UART2)     = uMode;  break;
		case clockUART3:      RegPER(UART3)     = uMode;  break;
		case clockUART4:      RegPER(UART4)     = uMode;  break;
		case clockUART5:      RegPER(UART5)     = uMode;  break;
		case clockUSBPHY0:    RegPER(USBPHY0)   = uMode;  break;
		case clockUSBPHY1:    RegPER(USBPHY1)   = uMode;  break;
		case clockEMIF:       RegPER(EMIF)      = uMode;  break;
		case clockDLL:        RegPER(DLL)       = uMode;  break;
		case clockLCDC:       RegPER(LCDC)      = uMode;  break;
		case clockDSS:        RegPER(DSS)       = uMode;  break;
		case clockCPSW:       RegPER(CPSW)      = uMode;  break;
		case clockMAC0:       RegPER(MAC0)      = uMode;  break;
		case clockOCPWP:      RegPER(OCPWP)     = uMode;  break;
		case clockRTC:	      RegRTC(CLKCTRL)   = uMode;  break;
		}

	while( GetClockState(uClock) != stateReady );
	}

UINT CClock437::GetClockState(UINT uClock)
{
	switch( uClock ) {
  
		case clockL3:        return (RegPER(L3)        >> 16) & 0x3;
		case clockOCMCRAM:   return (RegPER(OCMCRAM)   >> 16) & 0x3;
		case clockVPFE0:     return (RegPER(VPFE0)     >> 16) & 0x3;
		case clockVPFE1:     return (RegPER(VPFE1)     >> 16) & 0x3;
		case clockTPCC:      return (RegPER(TPCC)      >> 16) & 0x3;
		case clockTPTC0:     return (RegPER(TPTC0)     >> 16) & 0x3;
		case clockTPTC1:     return (RegPER(TPTC1)     >> 16) & 0x3;
		case clockTPTC2:     return (RegPER(TPTC2)     >> 16) & 0x3;
		case clockDLLAGING:  return (RegPER(DLLAGING)  >> 16) & 0x3;
		case clockL4HS:      return (RegPER(L4HS)      >> 16) & 0x3;
		case clockL3S:       return (RegPER(L3S)       >> 16) & 0x3;
		case clockGPMC:      return (RegPER(GPMC)      >> 16) & 0x3;
		case clockADC1:      return (RegPER(ADC1)      >> 16) & 0x3;
		case clockMCASP0:    return (RegPER(MCASP0)    >> 16) & 0x3;
		case clockMCASP1:    return (RegPER(MCASP1)    >> 16) & 0x3;
		case clockQSPI:      return (RegPER(QSPI)      >> 16) & 0x3;
		case clockUSB0:      return (RegPER(USB0)      >> 16) & 0x3;
		case clockUSB1:      return (RegPER(USB1)      >> 16) & 0x3;
		case clockPRU:       return (RegPER(PRU)       >> 16) & 0x3;
		case clockL4S:       return (RegPER(L4S)       >> 16) & 0x3;
		case clockDCAN0:     return (RegPER(DCAN0)     >> 16) & 0x3;
		case clockDCAN1:     return (RegPER(DCAN1)     >> 16) & 0x3;
		case clockPWM0:      return (RegPER(PWM0)      >> 16) & 0x3;
		case clockPWM1:      return (RegPER(PWM1)      >> 16) & 0x3;
		case clockPWM2:      return (RegPER(PWM2)      >> 16) & 0x3;
		case clockPWM3:      return (RegPER(PWM3)      >> 16) & 0x3;
		case clockPWM4:      return (RegPER(PWM4)      >> 16) & 0x3;
		case clockPWM5:      return (RegPER(PWM5)      >> 16) & 0x3;
		case clockELM:       return (RegPER(ELM)       >> 16) & 0x3;
		case clockGPIO0:     return (RegWUP(GPIO0)     >> 16) & 0x3;
		case clockGPIO1:     return (RegPER(GPIO1)     >> 16) & 0x3;
		case clockGPIO2:     return (RegPER(GPIO2)     >> 16) & 0x3;
		case clockGPIO3:     return (RegPER(GPIO3)     >> 16) & 0x3;
		case clockGPIO4:     return (RegPER(GPIO4)     >> 16) & 0x3;
		case clockGPIO5:     return (RegPER(GPIO5)     >> 16) & 0x3;
		case clockHDQ1W:     return (RegPER(HDQ1W)     >> 16) & 0x3;
		case clockI2C0:      return (RegWUP(I2C0)      >> 16) & 0x3;
		case clockI2C1:      return (RegPER(I2C1)      >> 16) & 0x3;
		case clockI2C2:      return (RegPER(I2C2)      >> 16) & 0x3;
		case clockMAIL0:     return (RegPER(MAIL0)     >> 16) & 0x3;
		case clockMMC0:      return (RegPER(MMC0)      >> 16) & 0x3;
		case clockMMC1:      return (RegPER(MMC1)      >> 16) & 0x3;
		case clockMMC2:      return (RegPER(MMC2)      >> 16) & 0x3;
		case clockSPI0:      return (RegPER(SPI0)      >> 16) & 0x3;
		case clockSPI1:      return (RegPER(SPI1)      >> 16) & 0x3;
		case clockSPI2:      return (RegPER(SPI2)      >> 16) & 0x3;
		case clockSPI3:      return (RegPER(SPI3)      >> 16) & 0x3;
		case clockSPI4:      return (RegPER(SPI4)      >> 16) & 0x3;
		case clockSPINLOCK:  return (RegPER(SPINLOCK)  >> 16) & 0x3;
		case clockTIMER2:    return (RegPER(TIMER2)    >> 16) & 0x3;
		case clockTIMER3:    return (RegPER(TIMER3)    >> 16) & 0x3;
		case clockTIMER4:    return (RegPER(TIMER4)    >> 16) & 0x3;
		case clockTIMER5:    return (RegPER(TIMER5)    >> 16) & 0x3;
		case clockTIMER6:    return (RegPER(TIMER6)    >> 16) & 0x3;
		case clockTIMER7:    return (RegPER(TIMER7)    >> 16) & 0x3;
		case clockTIMER8:    return (RegPER(TIMER8)    >> 16) & 0x3;
		case clockTIMER9:    return (RegPER(TIMER9)    >> 16) & 0x3;
		case clockTIMER10:   return (RegPER(TIMER10)   >> 16) & 0x3;
		case clockTIMER11:   return (RegPER(TIMER11)   >> 16) & 0x3;
		case clockUART0:     return (RegWUP(UART0)     >> 16) & 0x3;
		case clockUART1:     return (RegPER(UART1)     >> 16) & 0x3;
		case clockUART2:     return (RegPER(UART2)     >> 16) & 0x3;
		case clockUART3:     return (RegPER(UART3)     >> 16) & 0x3;
		case clockUART4:     return (RegPER(UART4)     >> 16) & 0x3;
		case clockUART5:     return (RegPER(UART5)     >> 16) & 0x3;
		case clockUSBPHY0:   return (RegPER(USBPHY0)   >> 16) & 0x3;
		case clockUSBPHY1:   return (RegPER(USBPHY1)   >> 16) & 0x3;
		case clockEMIF:      return (RegPER(EMIF)      >> 16) & 0x3;
		case clockDLL:       return (RegPER(DLL)       >> 16) & 0x3;
		case clockLCDC:      return (RegPER(LCDC)      >> 16) & 0x3;
		case clockDSS:       return (RegPER(DSS)       >> 16) & 0x3;
		case clockCPSW:      return (RegPER(CPSW)      >> 16) & 0x3;
		case clockMAC0:      return (RegPER(MAC0)      >> 16) & 0x3;
		case clockOCPWP:     return (RegPER(OCPWP)     >> 16) & 0x3;
		case clockRTC:       return (RegRTC(CLKCTRL)   >> 16) & 0x3;
		}

	return 0;
	}

void CClock437::SetClockSource(UINT uClock, UINT uSource)
{
	switch( uClock ) {

		case clockTIMER2:   RegPLL(CLKSELTMR2)	= uSource;  break;	 
		case clockTIMER3:   RegPLL(CLKSELTMR3)	= uSource;  break;	 
		case clockTIMER4:   RegPLL(CLKSELTMR4)	= uSource;  break;	 
		case clockTIMER5:   RegPLL(CLKSELTMR5)	= uSource;  break;	 
		case clockTIMER6:   RegPLL(CLKSELTMR6)	= uSource;  break;	 
		case clockTIMER7:   RegPLL(CLKSELTMR7)	= uSource;  break;	 
		case clockTIMER8:   RegPLL(CLKSELTMR8)	= uSource;  break;	 
		case clockTIMER9:   RegPLL(CLKSELTMR9)	= uSource;  break;	 
		case clockTIMER10:  RegPLL(CLKSELTMR10)	= uSource;  break;	 
		case clockTIMER11:  RegPLL(CLKSELTMR11)	= uSource;  break;	 
		};
	}

// End of File
