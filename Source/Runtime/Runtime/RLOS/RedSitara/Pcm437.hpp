
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Pcm437_HPP
	
#define	INCLUDE_AM437_Pcm437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 Power Management Module
//

class CPcm437
{
	public:
		// Constructor
		CPcm437(void);

		// Operations
		void EnablePru(void);

	protected:
		// PER Registers
		enum
		{
			regRSTCTRL = 0x0010 / sizeof(DWORD),
			regRSTSTAT = 0x0014 / sizeof(DWORD),
			};

		// Data Members
		PVDWORD  m_pBasePER;
	};

// End of File

#endif
