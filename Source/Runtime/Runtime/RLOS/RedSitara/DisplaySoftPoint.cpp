
#include "Intern.hpp"

#include "DisplaySoftPoint.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display Object with Soft Pointer
//

// Constructor

CDisplaySoftPoint::CDisplaySoftPoint(void)
{
	m_pSave  = NULL;

	m_pMouse = Create_Mutex();
	}

// Destructor

CDisplaySoftPoint::~CDisplaySoftPoint(void)
{
	delete [] m_pSave;

	m_pMouse->Release();
	}

// IUnknown

HRESULT CDisplaySoftPoint::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IPointer);

	return CDisplayBase::QueryInterface(riid, ppObject);
	}

ULONG CDisplaySoftPoint::AddRef(void)
{
	return CDisplayBase::AddRef();
	}

ULONG CDisplaySoftPoint::Release(void)
{
	return CDisplayBase::Release();
	}

// IDevice

BOOL CDisplaySoftPoint::Open(void)
{
	if( CDisplayBase::Open() ) {

		m_fMouse  = false;

		m_fDrawn  = false;

		m_fUpdate = false;

		m_uSave = (2 * cSize + 1) * (2 * cSize + 1);

		m_pSave = New DWORD [ m_uSave ];

		return TRUE;
		}

	return FALSE;
	}

// IPointer

void CDisplaySoftPoint::LockPointer(bool fLock)
{
	if( fLock ) {
		
		m_pMouse->Wait(FOREVER);
		}
	else {
		while( m_fUpdate ) {

			m_fUpdate = false;

			UpdatePointer();
			}

		m_pMouse->Free();
		}
	}

void CDisplaySoftPoint::SetPointerPos(int xPos, int yPos)
{
	if( m_xMouse == xPos && m_yMouse == yPos ) {

		return;
		}

	if( m_pMouse->Wait(0) ) {

		m_xMouse = xPos;

		m_yMouse = yPos;

		UpdatePointer();

		m_pMouse->Free();
		}
	else {
		m_xMouse  = xPos;

		m_yMouse  = yPos;

		m_fUpdate = true;
		}
	}

void CDisplaySoftPoint::ShowPointer(bool fShow)
{
	if( m_fMouse == fShow ) {

		return;
		}

	if( m_pMouse->Wait(0) ) {

		m_fMouse = fShow;

		UpdatePointer();

		m_pMouse->Free();
		}
	else {
		m_fMouse  = fShow;

		m_fUpdate = true;
		}
	}

// Implementation

void CDisplaySoftPoint::UpdatePointer(void)
{
	if( m_fDrawn != m_fMouse ) {

		m_pLock->Wait(FOREVER);

		if( m_fMouse ) {

			m_xDrawn = m_xMouse;

			m_yDrawn = m_yMouse;

			m_fDrawn = true;

			SavePointer(m_pBuff);

			DrawPointer(m_pBuff);
			}
		else {
			LoadPointer(m_pBuff);

			m_fDrawn = false;
			}

		m_pLock->Free();
		}
	else {
		if( m_fMouse ) {

			if( m_xDrawn != m_xMouse || m_yDrawn != m_yMouse ) {

				LoadPointer(m_pBuff);

				m_xDrawn = m_xMouse;

				m_yDrawn = m_yMouse;

				SavePointer(m_pBuff);

				DrawPointer(m_pBuff);
				}
			}
		}
	}

bool CDisplaySoftPoint::SavePointer(PDWORD pBase)
{
	if( m_fDrawn ) {

		PDWORD pSave = m_pSave;

		for( int y = -cSize; y <= +cSize; y++ ) {

			for( int x = -cSize; x <= +cSize; x++ ) {

				int mx = m_xDrawn + x;

				int my = m_yDrawn + y;

				if( mx >= 0 && mx < m_cx ) {

					if( my >= 0 && my < m_cy ) {

						*pSave++ = pBase[mx + m_cx * my];
						}
					}
				}
			}

		return true;
		}

	return false;
	}

bool CDisplaySoftPoint::LoadPointer(PDWORD pBase)
{
	if( m_fDrawn ) {

		PDWORD pSave = m_pSave;

		for( int y = -cSize; y <= +cSize; y++ ) {

			for( int x = -cSize; x <= +cSize; x++ ) {

				int mx = m_xDrawn + x;

				int my = m_yDrawn + y;

				if( mx >= 0 && mx < m_cx ) {

					if( my >= 0 && my < m_cy ) {

						pBase[mx + m_cx * my] = *pSave++;
						}
					}
				}
			}

		return true;
		}

	return false;
	}

bool CDisplaySoftPoint::DrawPointer(PDWORD pBase)
{
	if( m_fDrawn ) {

		for( int y = -cSize; y <= +cSize; y++ ) {

			for( int x = -cSize; x <= +cSize; x++ ) {

				int mx = m_xDrawn + x;

				int my = m_yDrawn + y;

				if( mx >= 0 && mx < m_cx ) {

					if( my >= 0 && my < m_cy ) {

						DWORD dwData = 0;

						if( x >= -cSize / 2 && x <= +cSize / 2 ) {

							if( y >= -cSize / 2 && y <= +cSize / 2 ) {

								dwData = ~dwData;
								}
							}

						pBase[mx + m_cx * my] = dwData;
						}
					}
				}
			}

		return true;
		}

	return false;
	}

// End of File
