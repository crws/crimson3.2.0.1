
#include "Intern.hpp"

#include "MemoryMap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define L2_LINE_SIZE	32

//////////////////////////////////////////////////////////////////////////
//
// L1 Cache
//

static void L1CacheEnable(void);
static void L1CacheDisable(void);
static void L1CacheEnableInstruction(void);
static void L1CacheDisableInstruction(void);
static void L1CacheInvalidateInstruction(void);
static void L1CacheInvalidateInstruction(UINT uAddr, UINT uSize);
static void L1CacheEnableBranch(void);
static void L1CacheDisableBranch(void);
static void L1CacheInvalidateBranch(void);
static void L1CacheEnableData(void);
static void L1CacheDisableData(void);
static void L1CacheCleanData(void);
static void L1CacheInvalidateData(void);
static void L1CacheCleanAndInvalidateData(void);
static void L1CacheCleanData(UINT uAddr, UINT uSize);
static void L1CacheInvalidateData(UINT uAddr, UINT uSize);
static void L1CacheCleanAndInvalidateData(UINT uAddr, UINT uSize);

//////////////////////////////////////////////////////////////////////////
//
// L2 Cache
//

static void L2CacheEnable(void);
static void L2CacheDisable(void);
static void L2CacheSync(void);
static void L2CacheClean(void);
static void L2CacheInvalidate(void);
static void L2CacheCleanAndInvalidate(void);
static void L2CacheClean(UINT uAddr);
static void L2CacheInvalidate(UINT uAddr);
static void L2CacheCleanAndInvalidate(UINT uAddr);
static void L2CacheClean(UINT uAddr, UINT uSize);
static void L2CacheInvalidate(UINT uAddr, UINT uSize);
static void L2CacheCleanAndInvalidate(UINT uAddr, UINT uSize);

//////////////////////////////////////////////////////////////////////////
//
// Sitara Cache Management
//

global void ProcSyncCaches(void)
{
	ProcPurgeDataCache();

	ProcInvalidateInstructionCache();
	}

global void ProcInvalidateInstructionCache(void)
{
	L1CacheInvalidateInstruction();

	L1CacheInvalidateBranch();
	}

global void ProcInvalidateDataCache(void)
{
	L2CacheInvalidate();

	L1CacheInvalidateInstruction();

	L1CacheInvalidateBranch();

	L1CacheInvalidateData();

	L2CacheInvalidate();

	L1CacheInvalidateInstruction();

	L1CacheInvalidateBranch();

	L1CacheInvalidateData();
	}

global void ProcPurgeDataCache(void)
{
	L1CacheCleanData();

	L2CacheClean();

	L2CacheCleanAndInvalidate();

	L1CacheCleanAndInvalidateData();
	}

global void ProcPurgeDataCache(UINT uAddr, UINT uSize)
{
	L1CacheCleanData(uAddr, uSize);

	L2CacheClean(uAddr, uSize);

	L2CacheCleanAndInvalidate(uAddr, uSize);

	L1CacheCleanAndInvalidateData(uAddr, uSize);
	}

//////////////////////////////////////////////////////////////////////////
//
// L1 Cache
//

static void L1CacheEnable(void)
{
	L1CacheEnableInstruction();

	L1CacheEnableBranch();

	L1CacheEnableData();
	}

static void L1CacheDisable(void)
{
	L1CacheDisableInstruction();

	L1CacheInvalidateInstruction();

	L1CacheDisableBranch();

	L1CacheInvalidateBranch();

	L1CacheDisableData();

	L1CacheCleanAndInvalidateData();
	}

static void L1CacheEnableInstruction(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	mrc	p15, #0, r0, c1, c0, #0		\n\t"
		"	orr	r0,  r0, #0x00001000		\n\t"
		"	mcr	p15, #0, r0, c1, c0, #0 	\n\t"
		:
		:
		: "r0"
		);

	#endif
	}

static void L1CacheDisableInstruction(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	mrc	p15, #0, r0, c1, c0, #0		\n\t"
		"	bic	r0,  r0, #0x00001000		\n\t"
		"	mcr	p15, #0, r0, c1, c0, #0		\n\t"
		:
		:
		: "r0"
		);

	#endif
	}

static void L1CacheInvalidateInstruction(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	mov    r0,  #0				\n\t"
		"	mcr    p15, #0, r0, c7, c5, #0		\n\t"
		"	dsb					\n\t"
		"	isb					\n\t"
		:
		:
		: "r0"
		);

	#endif
	}

static void L1CacheInvalidateInstruction(UINT uAddr, UINT uSize)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r14}				\n\t"
		"	add	r14, r0, r1               	\n\t"
		"	dmb					\n\t"
		"	mrc	p15, #0, r2, c0, c0, #1   	\n\t"
		"	ubfx	r2, r2, #0, #4            	\n\t"
		"	mov	r3, #2				\n\t"
		"	add	r3, r3, r2			\n\t"
		"	mov	r2, #1				\n\t"
		"	lsl	r2, r2, r3                	\n\t"
		"	sub	r3, r2, #1                	\n\t"
		"	bic	r0, r0, r3                	\n\t"
		"	tst	r3, r14				\n\t"
		"	bic	r14, r14, r3			\n\t"
		"	mcrne	p15, #0, r14, c7, c5, #1  	\n\t"
		"0:	mcr	p15, #0, r0, c7, c5, #1   	\n\t"
		"	adds	r0, r0, r2                	\n\t"
		"	cmp	r0, r14				\n\t"
		"	blt	0b				\n\t"
		"	dsb					\n\t"
		"	pop	{r14}				\n\t"
		:
		:
		: "r0","r1","r2","r3"
		);

	#endif
	}

static void L1CacheEnableBranch(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	mrc	p15, #0, r0, c1, c0, #0		\n\t"
		"	orr	r0,  r0, #0x00000800		\n\t"
		"	mcr	p15, #0, r0, c1, c0, #0 	\n\t"
		:
		:
		: "r0"
		);

	#endif
	}

static void L1CacheDisableBranch(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	mrc	p15, #0, r0, c1, c0, #0		\n\t"
		"	bic	r0,  r0, #0x00000800		\n\t"
		"	mcr	p15, #0, r0, c1, c0, #0  	\n\t"
		:
		:
		: "r0"
		);

	#endif
	}

static void L1CacheInvalidateBranch(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	mcr	p15, #0, r0, c7, c5, #6		\n\t"
		"	dsb					\n\t"
		"	isb					\n\t"
		:
		:
		: "r0"
		);

	#endif
	}

static void L1CacheEnableData(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	mrc	p15, #0, r0, c1, c0, #0 	\n\t"
		"	orr	r0,  r0, #0x00000004		\n\t"
		"	mcr	p15, #0, r0, c1, c0, #0		\n\t"
		:
		:
		: "r0"
		);

	#endif
	}

static void L1CacheDisableData(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r4-r11}			\n\t"
		"	mrc	p15, #0, r0, c1, c0, #0		\n\t"
		"	bic	r0,  r0, #0x00000004		\n\t"
		"	mcr	p15, #0, r0, c1, c0, #0		\n\t"
		"	pop	{r4-r11}			\n\t"
		:
		:
		: "r0"
		);

	#endif
	}

static void L1CacheCleanData(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r4-r11}			\n\t"
		"	dmb					\n\t"
		"	mrc	p15, #1, r0, c0, c0, #1		\n\t"
		"	ands	r3, r0, #0x7000000		\n\t"
		"	mov	r3, r3, lsr #23			\n\t"
		"	beq	0f				\n\t"
		"	mov	r10, #0				\n\t"
		"1:	add	r2, r10, r10, lsr #1		\n\t"
		"	mov	r1, r0, lsr r2			\n\t"
		"	and	r1, r1, #7			\n\t"
		"	cmp	r1, #2				\n\t"
		"	blt	4f				\n\t"
		"	mcr	p15, #2, r10, c0, c0, #0	\n\t"
		"	isb					\n\t"
		"	mrc	p15, #1, r1, c0, c0, #0		\n\t"
		"	and	r2, r1, #7			\n\t"
		"	add	r2, r2, #4			\n\t"
		"	ldr	r4, =0x3FF			\n\t"
		"	ands	r4, r4, r1, lsr #3		\n\t"
		"	clz	r5, r4				\n\t"
		"	ldr	r7, =0x00007FFF			\n\t"
		"	ands	r7, r7, r1, lsr #13		\n\t"
		"2:	mov	r9, r4				\n\t"
		"3:	orr	r11, r10, r9, lsl r5		\n\t"
		"	orr	r11, r11, r7, lsl r2		\n\t"
		"	mcr	p15, #0, r11, c7, c10, #2	\n\t"
		"	subs	r9, r9, #1			\n\t"
		"	bge	3b				\n\t"
		"	subs	r7, r7, #1			\n\t"
		"	bge	2b				\n\t"
		"4:	add	r10, r10, #2			\n\t"
		"	cmp	r3, r10				\n\t"
		"	bgt	1b				\n\t"
		"0:	dsb					\n\t"
		"	isb					\n\t"
		"	pop	{r4-r11}			\n\t"
		:
		:
		: "r0","r1","r2","r3"
		);

	#endif	
	}

static void L1CacheInvalidateData(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r4-r11}			\n\t"
		"	dmb					\n\t"
		"	mrc	p15, #1, r0, c0, c0, #1  	\n\t"
		"	ands	r3, r0, #0x7000000		\n\t"
		"	mov	r3, r3, lsr #23			\n\t"
		"	beq	0f				\n\t"
		"	mov	r10, #0				\n\t"
		"1:	add	r2, r10, r10, lsr #1		\n\t"
		"	mov	r1, r0, lsr r2			\n\t"
		"	and	r1, r1, #7			\n\t"
		"	cmp	r1, #2				\n\t"
		"	blt	4f				\n\t"
		"	mcr	p15, #2, r10, c0, c0, #0	\n\t"
		"	isb					\n\t"
		"	mrc	p15, #1, r1, c0, c0, #0		\n\t"
		"	and	r2, r1, #7			\n\t"
		"	add	r2, r2, #4			\n\t"
		"	ldr	r4, =0x3FF			\n\t"
		"	ands	r4, r4, r1, lsr #3		\n\t"
		"	clz	r5, r4				\n\t"
		"	ldr	r7, =0x00007FFF			\n\t"
		"	ands	r7, r7, r1, lsr #13		\n\t"
		"2:	mov	r9, r4				\n\t"
		"3:	orr	r11, r10, r9, lsl r5		\n\t"
		"	orr	r11, r11, r7, lsl r2		\n\t"
		"	mcr	p15, #0, r11, c7, c6, #2	\n\t"
		"	subs	r9, r9, #1			\n\t"
		"	bge	3b				\n\t"
		"	subs	r7, r7, #1			\n\t"
		"	bge	2b				\n\t"
		"4:	add	r10, r10, #2			\n\t"
		"	cmp	r3, r10				\n\t"
		"	bgt	1b				\n\t"
		"0:	dsb					\n\t"
		"	isb					\n\t"
		"	pop	{r4-r11}			\n\t"
		:
		:
		: "r0","r1","r2","r3"
		);

	#endif	
	}

static void L1CacheCleanAndInvalidateData(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r4-r11}			\n\t"
		"	dmb					\n\t"
		"	mrc	p15, #1, r0, c0, c0, #1		\n\t"
		"	ands	r3, r0, #0x7000000		\n\t"
		"	mov	r3, r3, lsr #23			\n\t"
		"	beq	0f				\n\t"
		"	mov	r10, #0				\n\t"
		"1:	add	r2, r10, r10, lsr #1		\n\t"
		"	mov	r1, r0, lsr r2			\n\t"
		"	and	r1, r1, #7			\n\t"
		"	cmp	r1, #2				\n\t"
		"	blt	4f				\n\t"
		"	mcr	p15, #2, r10, c0, c0, #0	\n\t"
		"	isb					\n\t"
		"	mrc	p15, #1, r1, c0, c0, #0		\n\t"
		"	and	r2, r1, #7			\n\t"
		"	add	r2, r2, #4			\n\t"
		"	ldr	r4, =0x3FF			\n\t"
		"	ands	r4, r4, r1, lsr #3		\n\t"
		"	clz	r5, r4				\n\t"
		"	ldr	r7, =0x00007FFF			\n\t"
		"	ands	r7, r7, r1, lsr #13		\n\t"
		"2:	mov	r9, r4				\n\t"
		"3:	orr	r11, r10, r9, lsl r5		\n\t"
		"	orr	r11, r11, r7, lsl r2		\n\t"
		"	mcr	p15, #0, r11, c7, c14, #2	\n\t"
		"	subs	r9, r9, #1			\n\t"
		"	bge	3b				\n\t"
		"	subs	r7, r7, #1			\n\t"
		"	bge	2b				\n\t"
		"4:	add	r10, r10, #2			\n\t"
		"	cmp	r3, r10				\n\t"
		"	bgt	1b				\n\t"
		"0:	dsb					\n\t"
		"       isb					\n\t"
		"       pop	{r4-r11}			\n\t"
		:
		:
		: "r0","r1","r2","r3"
		);

	#endif
	}

static void L1CacheCleanData(UINT uAddr, UINT uSize)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r14}				\n\t"
		"	add	r14, r0, r1               	\n\t"
		"	dmb					\n\t"
		"	mrc	p15, #0, r2, c0, c0, #1   	\n\t"
		"	ubfx	r2, r2, #16, #4           	\n\t"
		"	mov	r3, #2				\n\t"
		"	add	r3, r3, r2			\n\t"
		"	mov	r2, #1				\n\t"
		"	lsl	r2, r2, r3                	\n\t"
		"	sub	r3, r2, #1                	\n\t"
		"	bic	r0, r0, r3                	\n\t"
		"	tst	r3, r14				\n\t"
		"	bic	r14, r14, r3			\n\t"
		"	mcrne	p15, #0, r14, c7, c10, #1 	\n\t"
		"0:	mcr	p15, #0, r0 , c7, c10, #1 	\n\t"
		"	adds	r0, r0, r2                	\n\t"
		"	cmp	r0, r14 			\n\t"
		"	blt	0b				\n\t"
		"	dsb					\n\t"
		"	pop	{r14}				\n\t"
		:
		:
		: "r0","r1","r2","r3"
		);

	#endif
	}

static void L1CacheInvalidateData(UINT uAddr, UINT uSize)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r14}				\n\t"
		"	add	r14, r0, r1			\n\t"
		"	dmb					\n\t"
		"	mrc	p15, #0, r2, c0, c0, #1		\n\t"
		"	ubfx	r2, r2, #16, #4			\n\t"
		"	mov	r3, #2				\n\t"
		"	add	r3, r3, r2			\n\t"
		"	mov	r2, #1				\n\t"
		"	lsl	r2, r2, r3			\n\t"
		"	sub	r3, r2, #1			\n\t"
		"	tst	r3, r0 				\n\t"
		"	bic	r0, r0, r3			\n\t"
		"	mcrne	p15, #0, r0, c7, c14, #1	\n\t"
		"	addne	r0, r0, r2			\n\t"
		"	tst	r3, r14	 			\n\t"
		"	bic	r4, r14, r3              	\n\t"
		"	mcrne	p15, #0, r14, c7, c14, #1	\n\t"
		"	b	0f				\n\t"
		"1:	mcr	p15, #0, r0 , c7, c6, #1	\n\t"
		"	adds	r0, r0, r2			\n\t"
		"0:	cmp	r0, r14				\n\t"
		"       blt	1b				\n\t"
		"       dsb					\n\t"
		"       pop	{r14}				\n\t"
		:
		:
		: "r0","r1","r2","r3"
		);

	#endif
	}

static void L1CacheCleanAndInvalidateData(UINT uAddr, UINT uSize)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r14}				\n\t"
		"	add	r14, r0, r1               	\n\t"
		"	dmb					\n\t"
		"	mrc	p15, #0, r2, c0, c0, #1   	\n\t"
		"	ubfx	r2, r2, #16, #4           	\n\t"
		"	mov	r3, #2				\n\t"
		"	add	r3, r3, r2			\n\t"
		"	mov	r2, #1				\n\t"
		"	lsl	r2, r2, r3                	\n\t"
		"	sub	r3, r2, #1                	\n\t"
		"	bic	r0, r0, r3                	\n\t"
		"	tst	r3, r14				\n\t"
		"	bic	r14, r14, r3			\n\t"
		"	mcrne	p15, #0, r14, c7, c14, #1 	\n\t"
		"0:	mcr	p15, #0, r0 , c7, c14, #1 	\n\t"
		"	adds	r0, r0, r2                	\n\t"
		"	cmp	r0, r14	 			\n\t"
		"	blt	0b				\n\t"
		"	dsb					\n\t"
		"	pop	{r14}				\n\t"
		:
		:
		: "r0","r1","r2","r3"
		);

	#endif
	}

//////////////////////////////////////////////////////////////////////////
//
// L2 Cache
//

static void L2CacheEnable(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r4-r12}	\n\t"
		"	ldr	r12, =0x102	\n\t"
		"	mov	r0, #1		\n\t"
		"	dsb			\n\t"
		"	smc	#0		\n\t"
		"	pop	{r4-r12}	\n\t"
		:
		:
		: "r0"
		);

	#endif
	}

static void L2CacheDisable(void)
{
	#if !defined(__INTELLISENSE__)

	ASM(	"	push	{r4-r12}	\n\t"
		"	ldr	r12, =0x102	\n\t"
		"	mov	r0, #0		\n\t"
		"	dsb			\n\t"
		"	smc	#0		\n\t"
		"	pop	{r4-r12}	\n\t"
		:
		:
		: "r0"
		);

	#endif
	}

static void L2CacheSync(void)
{
	DWORD volatile &r = *PVDWORD(ADDR_MPU_PL310 + 0x0730);

	DWORD const     m = 0x00000001;

	r = 0;

	while( r & m );

	#if !defined(__INTELLISENSE__)

	ASM(	"	dsb	\n\t"
		:
		:
		:
		);

	#endif
	}

static void L2CacheClean(void)
{
	DWORD volatile &r = *PVDWORD(ADDR_MPU_PL310 + 0x07BC);

	DWORD const     m = 0x0000FFFF;

	r = m;

	while( r & m );

	L2CacheSync();
	}

static void L2CacheInvalidate(void)
{
	DWORD volatile &r = *PVDWORD(ADDR_MPU_PL310 + 0x077C);

	DWORD const     m = 0x0000FFFF;

	r = m;

	while( r & m );

	L2CacheSync();
	}

static void L2CacheCleanAndInvalidate(void)
{
	DWORD volatile &r = *PVDWORD(ADDR_MPU_PL310 + 0x07FC);

	DWORD const     m = 0x0000FFFF;

	r = m;

	while( r & m );

	L2CacheSync();
	}

static void L2CacheClean(UINT uAddr)
{
	DWORD volatile &r = *PVDWORD(ADDR_MPU_PL310 + 0x07B0);

	DWORD const     m = 0x00000001;

	r = uAddr;

	while( r & m );
	}

static void L2CacheInvalidate(UINT uAddr)
{
	DWORD volatile &r = *PVDWORD(ADDR_MPU_PL310 + 0x0770);

	DWORD const     m = 0x00000001;

	r = uAddr;

	while( r & m );
	}

static void L2CacheCleanAndInvalidate(UINT uAddr)
{
	DWORD volatile &r = *PVDWORD(ADDR_MPU_PL310 + 0x07F0);

	DWORD const     m = 0x00000001;

	r = uAddr;

	while( r & m );
	}

static void L2CacheClean(UINT uAddr, UINT uSize)
{
	uAddr &= ~(L2_LINE_SIZE-1);

	uSize &= ~(L2_LINE_SIZE-1);

	while( uSize ) {

		L2CacheClean(uAddr);

		uAddr += L2_LINE_SIZE;

		uSize -= L2_LINE_SIZE;
		}

	L2CacheSync();
	}

static void L2CacheInvalidate(UINT uAddr, UINT uSize)
{
	uAddr &= ~(L2_LINE_SIZE-1);

	uSize &= ~(L2_LINE_SIZE-1);

	while( uSize ) {

		L2CacheInvalidate(uAddr);

		uAddr += L2_LINE_SIZE;

		uSize -= L2_LINE_SIZE;
		}

	L2CacheSync();
	}

static void L2CacheCleanAndInvalidate(UINT uAddr, UINT uSize)
{
	uAddr &= ~(L2_LINE_SIZE-1);

	uSize &= ~(L2_LINE_SIZE-1);

	while( uSize ) {

		L2CacheCleanAndInvalidate(uAddr);

		uAddr += L2_LINE_SIZE;

		uSize -= L2_LINE_SIZE;
		}

	L2CacheSync();
	}

// End of File
