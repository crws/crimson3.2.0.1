
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PlatformSitara_HPP

#define INCLUDE_PlatformSitara_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../RedCore/PlatformRed.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCtrl437;
class CClock437;
class CPmic437;
class CPcm437;
class CAdc437;
class CGpmc437;
class CElm437;
class CEmif437;

/////////////////////////////////////////////////////////////////////////
//
// Platform Object for Sitara Device
//

class CPlatformSitara : public CPlatformRed, public IDiagProvider, public IEventSink
{
	public:
		// Constructor
		CPlatformSitara(BOOL fRack);

		// Destructor
		~CPlatformSitara(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IPlatform
		PCTXT METHOD GetFamily(void);
		PCTXT METHOD GetModel(void);
		BOOL  METHOD IsService(void);
		BOOL  METHOD IsEmulated(void);
		BOOL  METHOD HasMappings(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// OPP Levels
		enum
		{
			oppOPP50  = 0,
			oppOPP100 = 1,
			oppOPP120 = 2,
			oppTURBO  = 3
			};

		// Data
		UINT	     m_uProv;
		UINT         m_uModel;
		CCtrl437   * m_pCtrl;
		CClock437  * m_pClock;
		CPmic437   * m_pPmic;
		CPcm437    * m_pPcm;
		IGpio      * m_pGpio[6];
		CAdc437    * m_pAdc;
		CGpmc437   * m_pGpmc;
		CElm437    * m_pElm;
		CEmif437   * m_pEmif;
		UINT	     m_opp;

		// IDiagProvider
		UINT RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);
		
		// Inititialisation
		virtual void InitPriorities(void);
		virtual void InitClocks(void);
		virtual void InitRails(void);
		virtual void InitMpu(void);
		virtual void InitMux(void);
		virtual void InitGpio(void);
		virtual void InitMisc(void);
		virtual void InitNand(void);

		// Diagnostics
		bool DiagRegister(void);
		bool DiagRevoke(void);
		UINT DiagClocks(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagRails(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagTemps(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagSetOpp(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagSetDdr(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagSetBw(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagSetDssClk(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Implementation
		void SetMpuBandwidth(UINT bw);
		bool SetMpuOpp(UINT opp);
	};

// End of File

#endif
