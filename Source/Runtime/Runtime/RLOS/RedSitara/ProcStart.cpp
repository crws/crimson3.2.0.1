
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 Startup Code
//

// No Optimization

#pragma GCC optimize("O0")

// Externals

extern int  Main(void);

extern bool Create_Hal(void);

// Prototypes

clink	void	Start(void);
clink	void	SetSuperStack(void);
clink	void	SetOtherStacks(void);

// Code

clink void NAKED Start(void)
{
	// At this point we're executing in non-cached memory,
	// so our PC doesn't match our relocated address. That
	// is okay as long as we limit what we do, so we need
	// to set a few things up and then we can perform a
	// jump to the absolute location of the next routine,
	// setting the PC to the correct, cached address.

	DWORD p = DWORD(&SetSuperStack);

	#if !defined(__INTELLISENSE__)

	ASM(	//	Switch to supervisor
		"	ldr	r0, =0x01D3			\n\t"
		"	msr	cpsr_c, r0			\n\t"
		//	Select domain 0
		"	mov     r0, #1				\n\t"
		"	mcr     p15, #0, r0, c3, c0, #0		\n\t"
		//	Flush the TLBs
		"	mov	r0,  #0				\n\t"
		"	mcr	p15, #0, r0, c8, c7, #0		\n\t"
		//	Flush the write buffers
		"	mov	r0,  #0				\n\t"
		"	mcr	p15, #0, r0, c7, c10, #4	\n\t"
		//	Invalidate the TLBs
		"	mov	r0,  #0				\n\t"
		"	mcr	p15, #0, r0, c8, c7, #0		\n\t"
		//	Disable L2
		"	ldr	r12, =0x102			\n\t"
		"	mov	r0, #0				\n\t"
		"	dsb					\n\t"
		"	smc	#0				\n\t"
		//	Disable the L1
		"	mrc	p15, #0, r0, c1, c0, #0		\n\t"
		"	bic	r0,  r0, #0x00001000		\n\t"
		"	bic	r0,  r0, #0x00000800		\n\t"
		"	bic	r0,  r0, #0x00000004		\n\t"
		"	mcr	p15, #0, r0, c1, c0, #0		\n\t"
		//	Jump to cached memory
		"	mov	lr,  #0				\n\t"
		"	mov     r0,  %0				\n\t"
		"	bx	r0				\n\t"
		:
		: "r"(p)
		: "r0","r12"
		);

	#endif
	}

clink void NAKED SetSuperStack(void)
{
	// Now we're in cached memory but we don't have
	// a stack. So we configure a stack for supervisor
	// mode before we call the function to set up the
	// other stacks. We can't do this in this function
	// as our frame variables won't work properly.

	static DWORD stack[4096];

	DWORD  set = DWORD(stack + elements(stack));

	#if !defined(__INTELLISENSE__)

	ASM(	"mov sp, %0		\n\t"
		"mov r0, #0		\n\t"
		"str r0, [sp, #-4]!	\n\t"
		"str r0, [sp, #-4]!	\n\t"
		"str r0, [sp, #-4]!	\n\t"
		"str r0, [sp, #-4]!	\n\t"
		:
		: "r"(set)
		: "r0"
		);

	#endif

	// Before we go any further, make sure there's
	// no junk left in our data cache. This should
	// not be required, but it's worth doing.

	ProcInvalidateDataCache();
		
	// We can now enable all caches.
	
	#if !defined(__INTELLISENSE__)

	ASM(	//	Enable L1
		"	mrc	p15, #0, r0, c1, c0, #0		\n\t"
		"	orr	r0,  r0, #0x00001000		\n\t"
		"	orr	r0,  r0, #0x00000800		\n\t"
		"	orr	r0,  r0, #0x00000004		\n\t"
		"	mcr	p15, #0, r0, c1, c0, #0 	\n\t"
		//	Enable L2
		"	ldr	r12, =0x102			\n\t"
		"	mov	r0, #1				\n\t"
		"	dsb					\n\t"
		"	smc	#0				\n\t"
		//	Preload
		"	mcr	p15, #0, r0, c11, c2, #1	\n\t"
		"	dsb					\n\t"
		:
		: 
		: "r0","r12"
		);

	#endif

 	// And jump to the next bit of the startup.

	SetOtherStacks();
	}
	
clink void SetOtherStacks(void)
{
	// We executing in supervisor mode on a valid stack,
	// so we're at the point where we can do all the usual
	// C++ stuff like use frame variables and so on. We
	// loop around and setup the stacks for the other modes,
	// being careful to go back to supervisor each time so
	// that our frame variables remain valid.

	static DWORD const pmode[5] = { 0x00DB, 0x00D7, 0x00D2, 0x00D1, 0x00DF };

	static DWORD       stack[5][4096];

	for( UINT n = 0; n < elements(pmode); n++ ) {

		DWORD cpsr = pmode[n];

		DWORD load = DWORD(stack[n] + elements(stack[n]));

		DWORD back = 0x01D3;

		#if !defined(__INTELLISENSE__)

		// We dirty registers r8 - r12 (except fp) to
		// make sure that the compiler doesn't use them
		// to pass values to this code. Since we're
		// switching mode, they won't come across!

		ASM(	"msr cpsr_c, %0		\n\t"
			"mov sp, %1		\n\t"
			"mov r8, #0		\n\t"
			"str r8, [sp, #-4]!	\n\t"
			"str r8, [sp, #-4]!	\n\t"
			"str r8, [sp, #-4]!	\n\t"
			"str r8, [sp, #-4]!	\n\t"
			"msr cpsr_c, %2		\n\t"
			:
			: "r"(cpsr), "r"(load), "r"(back)
			: "r8", "r9", "r10", "r12"
			);

		#endif
		}

	// We can now create the HAL. Up until this point,
	// there are no interrupts, no fault handling and
	// very little support for debug, so be careful!

	if( Create_Hal() ) {

		// Now all that is done, we can switch into user mode
		// and jump to our main entry point, clearing the link
		// register to terminate any stack traces.

		DWORD p = DWORD(&Main);

		#if !defined(__INTELLISENSE__)

		ASM(	// Switch to user mode
			"mov	r0, #0x001F		\n\t"
			"msr	cpsr_c, r0		\n\t"
			// Jump to entry point
			"mov    lr,  #0			\n\t"
			"mov    r0,  %0			\n\t"
			"bx	r0			\n\t"
			:
			: "r"(p)
			: "r0"
			);

		#endif
		}

	// Failed to create HAL so we'll just have to reboot.

	for(;;);
	}

// End of File
