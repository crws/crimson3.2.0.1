
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_MemoryMap_HPP
	
#define	INCLUDE_AM437_MemoryMap_HPP

//////////////////////////////////////////////////////////////////////////
//
// Processor Memory Map
//

enum
{
	ADDR_GPMC_MEM			= 0x00000000,
	ADDR_QSPI			= 0x30000000,
	ADDR_MPU_ROM_SECURE		= 0x40000000,
	ADDR_MPU_ROM_PUBLIC		= 0x40030000,
	ADDR_MPU_SRAM			= 0x402F0000,
	ADDR_OCMCRAM			= 0x40300000,
	ADDR_MPU_L2_CACHE		= 0x40500000,
	ADDR_L3S_CFG			= 0x44800000,
	ADDR_L4_WKUP_AP			= 0x44C00000,
	ADDR_L4_WKUP_LA			= 0x44C00800,
	ADDR_L4_WKUP_IP0		= 0x44C01000,
	ADDR_L4_WKUP_IP1		= 0x44C01400,
	ADDR_WKUP_PROC_UMEM_L4		= 0x44D00000,
	ADDR_WKUP_PROC_DMEM_L4		= 0x44D80000,
	ADDR_OCP_SOCKET_PRM		= 0x44DF0000,
	ADDR_PRM_MPU			= 0x44DF0300,
	ADDR_PRM_GFX			= 0x44DF0400,
	ADDR_PRM_RTC			= 0x44DF0524,
	ADDR_PRM_TAMPER			= 0x44DF0624,
	ADDR_PRM_CEFUSE			= 0x44DF0700,
	ADDR_PRM_PER			= 0x44DF0800,
	ADDR_PRM_WKUP			= 0x44DF2010,
	ADDR_CM_WKUP			= 0x44DF2800,
	ADDR_PRM_DEVICE			= 0x44DF4000,
	ADDR_CM_DEVICE			= 0x44DF4100,
	ADDR_CM_DPLL			= 0x44DF4200,
	ADDR_CM_MPU			= 0x44DF8300,
	ADDR_CM_GFX			= 0x44DF8400,
	ADDR_CM_RTC			= 0x44DF8500,
	ADDR_CM_TAMPER			= 0x44DF8600,
	ADDR_CM_CEFUSE			= 0x44DF8700,
	ADDR_CM_PER			= 0x44DF8800,
	ADDR_TA_PRCM			= 0x44E00000,
	ADDR_DMTIMER0			= 0x44E05000,
	ADDR_TA_TIMER0			= 0x44E06000,
	ADDR_GPIO0			= 0x44E07000,
	ADDR_TA_GPIO0			= 0x44E08000,
	ADDR_UART0			= 0x44E09000,
	ADDR_TA_UART0			= 0x44E0A000,
	ADDR_I2C0			= 0x44E0B000,
	ADDR_TA_I2C0			= 0x44E0C000,
	ADDR_ADC_TSC			= 0x44E0D000,
	ADDR_CONTROL_MODULE		= 0x44E10000,
	ADDR_TA_CONTROL			= 0x44E30000,
	ADDR_DMTIMER1_1MS		= 0x44E31000,
	ADDR_WDT1			= 0x44E35000,
	ADDR_TA_WDT1			= 0x44E36000,
	ADDR_SMARTREFLEX0		= 0x44E37000,
	ADDR_TA_SMARTREFLEX0		= 0x44E38000,
	ADDR_SMARTREFLEX1		= 0x44E39000,
	ADDR_TA_SMARTREFLEX1		= 0x44E3A000,
	ADDR_RTCSS			= 0x44E3E000,
	ADDR_TA_RTCSS			= 0x44E3F000,
	ADDR_L4_DEBUGSS			= 0x44E40000,
	ADDR_SYNCTIMER			= 0x44E86000,
	ADDR_TA_SYNCTIMER		= 0x44E87000,
	ADDR_TPMSS_BBD_CFG_INST_0	= 0x44E88000,
	ADDR_TPMSS_CORE_CFG_INST_0	= 0x44E88000,
	ADDR_TA_TPMSS			= 0x44E92000,
	ADDR_MMCSD2			= 0x47810000,
	ADDR_QSPI_ADDRSP0		= 0x47900000,
	ADDR_L4_FW_AP			= 0x47C00000,
	ADDR_L4_FW_LA			= 0x47C00800,
	ADDR_L4_FW			= 0x47C01000,
	ADDR_EMIF_FW			= 0x47C0C000,
	ADDR_GPMC_FW			= 0x47C0E000,
	ADDR_OCMCRAM_FW			= 0x47C10000,
	ADDR_MPU_L2_CACHE_FW		= 0x47C12000,
	ADDR_GFX_FW			= 0x47C14000,
	ADDR_MMCSD2_FW			= 0x47C24000,
	ADDR_MCASP0_FW			= 0x47C26000,
	ADDR_MCASP1_FW			= 0x47C28000,
	ADDR_EDMA3TC_FW			= 0x47C30000,
	ADDR_QSPI_FW			= 0x47C34000,
	ADDR_PRU_ICSS_FW		= 0x47C36000,
	ADDR_EDMA3CC_FW			= 0x47C38000,
	ADDR_DEBUGSS_FW			= 0x47C3E000,
	ADDR_AES_FW			= 0x47C40000,
	ADDR_DES_FW			= 0x47C42000,
	ADDR_SHA_FW			= 0x47C44000,
	ADDR_ADC_TSC_FW			= 0x47C46000,
	ADDR_MAGCARD_FW			= 0x47C48000,
	ADDR_L4_PER_AP			= 0x48000000,
	ADDR_L4_PER_LA			= 0x48000800,
	ADDR_L4_PER_IP0			= 0x48001000,
	ADDR_L4_PER_IP1			= 0x48001400,
	ADDR_L4_PER_IP2			= 0x48001800,
	ADDR_L4_PER_IP3			= 0x48001C00,
	ADDR_CEFUSE			= 0x48008000,
	ADDR_TA_CEFUSE			= 0x48009000,
	ADDR_UART1			= 0x48022000,
	ADDR_TA_USART0			= 0x48023000,
	ADDR_UART2			= 0x48024000,
	ADDR_TA_USART1			= 0x48025000,
	ADDR_I2C1			= 0x4802A000,
	ADDR_TA_I2C1			= 0x4802B000,
	ADDR_MCSPI0			= 0x48030000,
	ADDR_TA_SPI0			= 0x48031000,
	ADDR_USIM0			= 0x48034000,
	ADDR_TA_USIM0			= 0x48035000,
	ADDR_USIM1			= 0x48036000,
	ADDR_TA_USIM1			= 0x48037000,
	ADDR_MCASP0_CFG			= 0x48038000,
	ADDR_TA_MCASP0			= 0x4803A000,
	ADDR_MCASP1_CFG			= 0x4803C000,
	ADDR_TA_MCASP1			= 0x4803E000,
	ADDR_DMTIMER2			= 0x48040000,
	ADDR_TA_TIMER2			= 0x48041000,
	ADDR_DMTIMER3			= 0x48042000,
	ADDR_TA_TIMER3			= 0x48043000,
	ADDR_DMTIMER4			= 0x48044000,
	ADDR_TA_TIMER4			= 0x48045000,
	ADDR_DMTIMER5			= 0x48046000,
	ADDR_TA_TIMER5			= 0x48047000,
	ADDR_DMTIMER6			= 0x48048000,
	ADDR_TA_TIMER6			= 0x48049000,
	ADDR_DMTIMER7			= 0x4804A000,
	ADDR_TA_TIMER7			= 0x4804B000,
	ADDR_GPIO1			= 0x4804C000,
	ADDR_TA_GPIO1			= 0x4804D000,
	ADDR_MMCSD0			= 0x48060000,
	ADDR_TA_MMC0			= 0x48061000,
	ADDR_ELM			= 0x48080000,
	ADDR_TA_ELM			= 0x48090000,
	ADDR_MAILBOX0			= 0x480C8000,
	ADDR_TA_MAILBOX0		= 0x480C9000,
	ADDR_SPINLOCK			= 0x480CA000,
	ADDR_TA_SPINLOCK		= 0x480CB000,
	ADDR_OCP_WP_NOC			= 0x4818C000,
	ADDR_TA_OCP_WP_NOC		= 0x4818D000,
	ADDR_P1500			= 0x4818E000,
	ADDR_TA_P1500			= 0x4818F000,
	ADDR_I2C2			= 0x4819C000,
	ADDR_TA_I2C2			= 0x4819D000,
	ADDR_MCSPI1			= 0x481A0000,
	ADDR_TA_SPI1			= 0x481A1000,
	ADDR_MCSPI2			= 0x481A2000,
	ADDR_TA_SPI2			= 0x481A3000,
	ADDR_MCSPI3			= 0x481A4000,
	ADDR_TA_SPI3			= 0x481A5000,
	ADDR_UART3			= 0x481A6000,
	ADDR_TA_USART2			= 0x481A7000,
	ADDR_UART4			= 0x481A8000,
	ADDR_TA_UART1			= 0x481A9000,
	ADDR_UART5			= 0x481AA000,
	ADDR_TA_UART2			= 0x481AB000,
	ADDR_GPIO2			= 0x481AC000,
	ADDR_TA_GPIO2			= 0x481AD000,
	ADDR_GPIO3			= 0x481AE000,
	ADDR_TA_GPIO3			= 0x481AF000,
	ADDR_DMTIMER8			= 0x481C1000,
	ADDR_TA_TIMER8			= 0x481C2000,
	ADDR_DCAN0			= 0x481CC000,
	ADDR_TA_DCAN0			= 0x481CE000,
	ADDR_DCAN1			= 0x481D0000,
	ADDR_TA_DCAN1			= 0x481D2000,
	ADDR_MMCSD1			= 0x481D8000,
	ADDR_TA_MMC1			= 0x481D9000,
	ADDR_MPU_SCU			= 0x48240000,
	ADDR_MPU_INTC			= 0x48240100,
	ADDR_MPU_GBL_TIMER		= 0x48240200,
	ADDR_MPU_PRV_TIMER		= 0x48240600,
	ADDR_MPU_DIST			= 0x48241000,
	ADDR_MPU_PL310			= 0x48242000,
	ADDR_MPU_SCM			= 0x48280004,
	ADDR_MPU_WAKEUP_GEN		= 0x48281000,
	ADDR_MPU_CMU			= 0x48290000,
	ADDR_A2OCMURR			= 0x482A0000,
	ADDR_A2ORPT			= 0x482A1000,
	ADDR_A2OMISC			= 0x482A2000,
	ADDR_PWMSS0			= 0x48300000,
	ADDR_PWMSS0_ECAP		= 0x48300100,
	ADDR_PWMSS0_EQEP		= 0x48300180,
	ADDR_PWMSS0_EPWM		= 0x48300200,
	ADDR_TA_EPWMSS0			= 0x48301000,
	ADDR_PWMSS1			= 0x48302000,
	ADDR_PWMSS1_ECAP		= 0x48302100,
	ADDR_PWMSS1_EQEP		= 0x48302180,
	ADDR_PWMSS1_EPWM		= 0x48302200,
	ADDR_TA_EPWMSS1			= 0x48303000,
	ADDR_PWMSS2			= 0x48304000,
	ADDR_PWMSS2_ECAP		= 0x48304100,
	ADDR_PWMSS2_EQEP		= 0x48304180,
	ADDR_PWMSS2_EPWM		= 0x48304200,
	ADDR_TA_EPWMSS2			= 0x48305000,
	ADDR_PWMSS3			= 0x48306000,
	ADDR_PWMSS3_EPWM		= 0x48306200,
	ADDR_TA_EPWMSS3			= 0x48307000,
	ADDR_PWMSS4			= 0x48308000,
	ADDR_PWMSS4_EPWM		= 0x48308200,
	ADDR_TA_EPWMSS4			= 0x48309000,
	ADDR_PWMSS5			= 0x4830A000,
	ADDR_PWMSS5_EPWM		= 0x4830A200,
	ADDR_TA_EPWMSS5			= 0x4830B000,
	ADDR_RNG			= 0x48310000,
	ADDR_TA_RNG			= 0x48312000,
	ADDR_CRYPTODMA			= 0x48313000,
	ADDR_TA_CRYPTODMA		= 0x48314000,
	ADDR_PKA			= 0x48318080,
	ADDR_TA_PKA			= 0x4831C000,
	ADDR_GPIO4			= 0x48320000,
	ADDR_TA_GPIO4			= 0x48321000,
	ADDR_GPIO5			= 0x48322000,
	ADDR_TA_GPIO5			= 0x48323000,
	ADDR_VPFE0			= 0x48326000,
	ADDR_TA_VPFE0			= 0x48327000,
	ADDR_VPFE1			= 0x48328000,
	ADDR_TA_VPFE1			= 0x48329000,
	ADDR_DSS_TOP			= 0x4832A000,
	ADDR_DSS_DISPC			= 0x4832A400,
	ADDR_DSS_MADDRSPACE2		= 0x4832A800,
	ADDR_DSS_MADDRSPACE3		= 0x4832AC00,
	ADDR_DSS_MADDRSPACE4		= 0x4832B000,
	ADDR_TA_DSS			= 0x4832C000,
	ADDR_DMTIMER9			= 0x4833D000,
	ADDR_TA_TIMER9			= 0x4833E000,
	ADDR_DMTIMER10			= 0x4833F000,
	ADDR_TA_TIMER10			= 0x48340000,
	ADDR_DMTIMER11			= 0x48341000,
	ADDR_TA_TIMER11			= 0x48342000,
	ADDR_MCSPI4			= 0x48345000,
	ADDR_TA_SPI4			= 0x48346000,
	ADDR_HDQ1W			= 0x48347000,
	ADDR_TA_HDQ1W			= 0x48348000,
	ADDR_MAGCARD			= 0x4834C000,
	ADDR_TA_MAG_CARD		= 0x4834E000,
	ADDR_USB0_WRAPPER		= 0x48380000,
	ADDR_USB0_DWC3			= 0x48390000,
	ADDR_USB0_PHY			= 0x483A8000,
	ADDR_USB1_WRAPPER		= 0x483C0000,
	ADDR_USB1_DWC3			= 0x483D0000,
	ADDR_USB1_PHY			= 0x483E8000,
	ADDR_ERMC			= 0x483F2000,
	ADDR_TA_OCMC8			= 0x483F4000,
	ADDR_EDMA3CC			= 0x49000000,
	ADDR_EDMA3TC0			= 0x49800000,
	ADDR_EDMA3TC1			= 0x49900000,
	ADDR_EDMA3TC2			= 0x49A00000,
	ADDR_L4_FAST_AP			= 0x4A000000,
	ADDR_L4_FAST_LA			= 0x4A000800,
	ADDR_L4_FAST			= 0x4A001000,
	ADDR_CPSW			= 0x4A100000,
	ADDR_CPSW_PORT			= 0x4A100100,
	ADDR_CPSW_CPDMA			= 0x4A100800,
	ADDR_CPSW_STATERAM		= 0x4A100A00,
	ADDR_CPSW_CPTS			= 0x4A100C00,
	ADDR_CPSW_ALE			= 0x4A100D00,
	ADDR_CPSW_SL1			= 0x4A100D80,
	ADDR_CPSW_SL2			= 0x4A100DC0,
	ADDR_CPSW_WR			= 0x4A101200,
	ADDR_CPSW_CPPI_RAM		= 0x4a102000,
	ADDR_TA_CPGMAC0			= 0x4A108000,
	ADDR_OTFA			= 0x4A400000,
	ADDR_TA_OTFA_EMIF		= 0x4A402000,
	ADDR_DEBUGSS			= 0x4B000000,
	ADDR_EMIF			= 0x4C000000,
	ADDR_GPMC_CONFIG		= 0x50000000,
	ADDR_SHA_ADDRSP0		= 0x53000000,
	ADDR_AES_ADDRSP0		= 0x53400000,
	ADDR_DES_ADDRSP0		= 0x53600000,
	ADDR_PRU_ICSS1_BASE		= 0x54400000,
	ADDR_PRU_ICSS1_DATA_RAM0	= 0x54400000,
	ADDR_PRU_ICSS1_DATA_RAM1	= 0x54402000,
	ADDR_PRU_ICSS1_SHARED_RAM	= 0x54410000,
	ADDR_PRU_ICSS1_INTC		= 0x54420000,
	ADDR_PRU_ICSS1_PRU0_CTRL	= 0x54422000,
	ADDR_PRU_ICSS1_PRU0_DEBUG	= 0x54422400,
	ADDR_PRU_ICSS1_PRU1_CTRL	= 0x54424000,
	ADDR_PRU_ICSS1_PRU1_DEBUG	= 0x54424400,
	ADDR_PRU_ICSS1_CFG		= 0x54426000,
	ADDR_PRU_ICSS1_UART		= 0x54428000,
	ADDR_PRU_ICSS1_IEP		= 0x5442E000,
	ADDR_PRU_ICSS1_ECAP		= 0x54430000,
	ADDR_PRU_ICSS1_MII_RT_CFG	= 0x54432000,
	ADDR_PRU_ICSS1_MII_MDIO		= 0x54432400,
	ADDR_PRU_ICSS0_BASE		= 0x54440000,
	ADDR_PRU_ICSS0_INTC		= 0x54460000,
	ADDR_PRU_ICSS0_PRU0_CTRL	= 0x54462000,
	ADDR_PRU_ICSS0_PRU0_DEBUG	= 0x54462400,
	ADDR_PRU_ICSS0_PRU1_CTRL	= 0x54464000,
	ADDR_PRU_ICSS0_PRU1_DEBUG	= 0x54464400,
	ADDR_PRU_ICSS0_CFG		= 0x54466000,
	ADDR_PRU_ICSS0_UART		= 0x54468000,
	ADDR_PRU_ICSS0_IEP		= 0x5446E000,
	ADDR_PRU_ICSS0_ECAP		= 0x54470000,
	ADDR_PRU_ICSS0_MII_RT_CFG	= 0x54472000,
	ADDR_PRU_ICSS0_MII_MDIO		= 0x54472400,
	ADDR_MAGCARD_DMA		= 0x54800000,
	ADDR_ADC_TSC_DMA		= 0x54C00000,
	ADDR_GFX			= 0x56000000,
	ADDR_DDR1			= 0x80000000,
	ADDR_EMIF_ADDRSP1		= 0x80000000,
	ADDR_L3F_CFG			= 0x88000000,
	ADDR_DDR2			= 0x90000000,
	};

//////////////////////////////////////////////////////////////////////////
//
// Processor Interrupt Map
//

enum
 {
	INT_PL310			= 32,
	INT_CTI0			= 33,
	INT_ELM				= 36,
	INT_NMI				= 39,
	INT_L3DEBUG			= 41,
	INT_L3APP			= 42,
	INT_PRCM			= 43,
	INT_EDMACOMP			= 44,
	INT_EDMAMPERR			= 45,
	INT_EDMAERR			= 46,
	INT_ADC0_GEN			= 48,
	INT_PRU_ICSS1_EVTOUT0		= 52,
	INT_PRU_ICSS1_EVTOUT1		= 53,
	INT_PRU_ICSS1_EVTOUT2		= 54,
	INT_PRU_ICSS1_EVTOUT3		= 55,
	INT_PRU_ICSS1_EVTOUT4		= 56,
	INT_PRU_ICSS1_EVTOUT6		= 58,
	INT_PRU_ICSS1_EVTOUT7		= 59,
	INT_MMCSD1			= 60,
	INT_MMCSD2			= 61,
	INT_I2C2			= 62,
	INT_ECAP0			= 63,
	INT_GPIO2A			= 64,
	INT_GPIO2B			= 65,
	INT_GFX				= 69,
	INT_EPWM2			= 71,
	INT_3PGSWRXTHR0			= 72,
	INT_3PGSWRX0			= 73,
	INT_3PGSWTX0			= 74,
	INT_3PGSWMISC0			= 75,
	INT_UART3			= 76,
	INT_UART4			= 77,
	INT_UART5			= 78,
	INT_ECAP1			= 79,
	INT_CCDC0			= 80,
	INT_DCAN1_0			= 81,
	INT_CCDC1			= 82,
	INT_DCAN0_PARITY		= 83,
	INT_DCAN0_0			= 84,
	INT_DCAN0_1			= 85,
	INT_DCAN1_1			= 88,
	INT_DCAN1_PARITY		= 89,
	INT_EPWM0_TZ			= 90,
	INT_EPWM1_TZ			= 91,
	INT_EPWM2_TZ			= 92,
	INT_ECAP2			= 93,
	INT_GPIO3A			= 94,
	INT_GPIO3B			= 95,
	INT_MMCSD0			= 96,
	INT_SPI0			= 97,
	INT_TIMER0			= 98,
	INT_TIMER1_1MS			= 99,
	INT_TIMER2			= 100,
	INT_TIMER3			= 101,
	INT_I2C0			= 102,
	INT_I2C1			= 103,
	INT_UART0			= 104,
	INT_UART1			= 105,
	INT_UART2			= 106,
	INT_RTC				= 107,
	INT_RTCALARM			= 108,
	INT_MB0				= 109,
	INT_EQEP0			= 111,
	INT_MCATX0			= 112,
	INT_MCARX0			= 113,
	INT_MCATX1			= 114,
	INT_MCARX1			= 115,
	INT_EPWM0			= 118,
	INT_EPWM1			= 119,
	INT_EQEP1			= 120,
	INT_EQEP2			= 121,
	INT_DMA_PIN2			= 122,
	INT_WDT1			= 123,
	INT_TIMER4			= 124,
	INT_TIMER5			= 125,
	INT_TIMER6			= 126,
	INT_TIMER7			= 127,
	INT_GPIO0A			= 128,
	INT_GPIO0B			= 129,
	INT_GPIO1A			= 130,
	INT_GPIO1B			= 131,
	INT_GPMC			= 132,
	INT_DDRERR0			= 133,
	INT_GPIO4A			= 138,
	INT_GPIO4B			= 139,
	INT_INT_TCERR0			= 144,
	INT_TCERR1			= 145,
	INT_TCERR2			= 146,
	INT_ADC1_GEN			= 147,
	INT_DMA_R_PIN0			= 155,
	INT_DMA_R_PIN1			= 156,
	INT_SPI1			= 157,
	INT_SPI2			= 158,
	INT_DSS				= 159,
	INT_TIMER8			= 163,
	INT_TIMER9			= 164,
	INT_TIMER10			= 165,
	INT_TIMER11			= 166,
	INT_SPI3			= 168,
	INT_SPI4			= 169,
	INT_QSPI			= 170,
	INT_HDQ				= 171,
	INT_EPWM3			= 173,
	INT_EPWM3TZ			= 174,
	INT_EPWM4			= 175,
	INT_EPWM4TZ			= 176,
	INT_EPWM5			= 177,
	INT_EPWM5TZ			= 178,
	INT_GPIO5A			= 180,
	INT_GPIO5B			= 181,
	INT_DMA_PIN3			= 182,
	INT_DMA_PIN4			= 183,
	INT_DMA_PIN5			= 184,
	INT_DMA_PIN6			= 185,
	INT_DMA_PIN7			= 186,
	INT_DMA_PIN8			= 187,
	INT_PRU_ICSS0_EVTOUT0		= 191,
	INT_PRU_ICSS0_EVTOUT1		= 192,
	INT_PRU_ICSS0_EVTOUT2		= 193,
	INT_PRU_ICSS0_EVTOUT3		= 194,
	INT_PRU_ICSS0_EVTOUT4		= 195,
	INT_PRU_ICSS0_EVTOUT6		= 196,
	INT_PRU_ICSS0_EVTOUT7		= 197,
	INT_USB0_MAIN0			= 200,
	INT_USB0_MAIN1			= 201,
	INT_USB0_MAIN2			= 202,
	INT_USB0_MAIN3			= 203,
	INT_USB0_MISC			= 204,
	INT_USB1_MAIN0			= 206,
	INT_USB1_MAIN1			= 207,
	INT_USB1_MAIN2			= 208,
	INT_USB1_MAIN3			= 209,
	INT_USB1_MISC			= 210,
	};

// End of File

#endif
