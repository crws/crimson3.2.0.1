
#include "Intern.hpp"

#include "Adc437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 ADC
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CAdc437::CAdc437(void)
{
	m_pBase = PVDWORD(ADDR_MAGCARD);

	m_uLine = INT_ADC1_GEN;

	m_dwRaw = 0;

	m_fBusy = false;

	m_fWait = false;

	m_pDone = Create_AutoEvent();

	InitController();

	InitEvents();
	}

// Destructor

CAdc437::~CAdc437(void)
{
	m_pDone->Release();
	}

// Operations

DWORD CAdc437::GetReading(UINT uChannel)
{
	TestWait();

	Reg(STEPEN) = Bit(uChannel + 1);

	WaitDone();

	return m_dwRaw;
	}

// IEventSink

void CAdc437::OnEvent(UINT uLine, UINT uParam)
{
	if( uParam == 0 ) {

		if( Reg(IRQSTS) & intEndOfSeq ) {

			Reg(IRQSTS) |= intEndOfSeq;

			OnConvert();
			}
		}
	}

// Implementation

void CAdc437::InitController(void)
{
	Reg(IRQEN_SET)   = intEndOfSeq;

	Reg(STEPCONFIG1) = 0x03843008;

	Reg(STEPCONFIG2) = 0x038C3008;

	Reg(STEPDELAY1)  = 250000;

	Reg(STEPDELAY2)  = 100000;

	Reg(CLKDIV)      = 23;
				
	Reg(CTRL)        = 0x00000063;
	}

void CAdc437::InitEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);
	}

void CAdc437::OnConvert(void)
{
	DWORD dwData = Reg(FIFO0DATA);

	m_dwRaw = dwData & 0x0FFF;

	m_pDone->Set();
	}

void CAdc437::TestWait(void)
{
	m_fWait = Hal_GetIrql() < IRQL_DISPATCH && GetCurrentThread() != NULL && GetThreadIndex() > 1;

	m_fBusy = true;

	phal->EnableLine(m_uLine, m_fWait);
	}

void CAdc437::WaitDone(void)
{
	if( !m_fWait ) {
		
		while( !m_pDone->Wait(0) ) {

			while( !(Reg(IRQSTS) & Reg(IRQEN_SET)) );

			OnEvent(0, 0);
			}

		m_fBusy = false;

		return;
		}

	m_pDone->Wait(FOREVER);

	m_fBusy = false;
	}

// End of File
