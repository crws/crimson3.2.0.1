
#include "Intern.hpp"

#include "PlatformManticore.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

#include "Ctrl437.hpp"

#include "Gpmc437.hpp"

#include "Elm437.hpp"

#include "Pwm437.hpp"

#include "Pru437.hpp"

#include "Pcm437.hpp"

#include "../RedDev/Identity.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Pru Code  
//

#include "Pru/Manticore.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice    * Create_RtcAbmc(void);
extern IDevice    * Create_Mr25Hxx(UINT uChan);
extern IDevice    * Create_Leds(ILeds *pLeds);
extern IDevice    * Create_InputSwitch(IInputSwitch *pSwitch);
extern IDevice    * Create_Identity(UINT uAddr);
extern IDevice    * Create_Uart437(UINT iIndex, CClock437 *pClock, CPru437 *pPru);
extern IDevice    * Create_SdHost437(CClock437 *pClock);
extern IDevice	  * Create_DualEnet437(CClock437 *pClock, UINT uGpio);
extern IUsbDriver * Create_UsbFunc437(UINT iIndex);
extern IUsbDriver * Create_UsbHost437(UINT iIndex);
extern IUsbDriver * Create_UsbHostExtensible(void);
extern IUsbDriver * Create_UsbHostExtensibleController(void);
extern ISensor	  * Create_Temp112x(BYTE bAddr, INT nLimit1, INT nLimit2);

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Manticore
//

// Instantiator

IDevice * Create_PlatformManticore(UINT uModel)
{
	return (IDevice *) (CPlatformSitara *) New CPlatformManticore(uModel);
}

// Static 

const UINT CPlatformManticore::m_uPorts[8][4] = 
{
	{ physicalRS232, physicalRS232, physicalNone,  physicalNone  },	// A
	{ physicalRS232, physicalRS485, physicalNone,  physicalNone  },	// B
	{ physicalRS232, physicalRS232, physicalNone,  physicalNone  },	// C
	{ physicalRS485, physicalRS485, physicalNone,  physicalNone  },	// D
	
	{ physicalRS232, physicalRS232, physicalRS232, physicalRS485 }, // E
	{ physicalRS232, physicalRS232, physicalRS485, physicalRS485 }, // F
	{ physicalRS232, physicalRS485, physicalRS485, physicalRS485 }, // G
	{ physicalRS485, physicalRS485, physicalRS485, physicalRS485 }, // H

	};

// Constructor

CPlatformManticore::CPlatformManticore(UINT uModel) : CPlatformSitara(TRUE)
{
	m_uModel = uModel;
}

// Destructor

CPlatformManticore::~CPlatformManticore(void)
{
	delete m_pPwm[0];

	delete m_pPwm[1];

	delete m_pPwm[2];

	delete m_pPwm[3];

	delete m_pPru[0];

	delete m_pPru[1];

	piob->RevokeGroup("dev.");
}

// IUnknown

HRESULT CPlatformManticore::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IPortSwitch);

	StdQueryInterface(IInputSwitch);

	StdQueryInterface(ILeds);

	StdQueryInterface(IUsbSystem);

	StdQueryInterface(IUsbSystemPortMapper);

	StdQueryInterface(IUsbSystemPower);

	return CPlatformSitara::QueryInterface(riid, ppObject);
}

ULONG CPlatformManticore::AddRef(void)
{
	return CPlatformSitara::AddRef();
}

ULONG CPlatformManticore::Release(void)
{
	return CPlatformSitara::Release();
}

// IDevice

BOOL CPlatformManticore::Open(void)
{
	if( CPlatformSitara::Open() ) {

		m_pPwm[0] = New CPwm437(0, m_pClock);

		m_pPwm[1] = New CPwm437(1, m_pClock);

		m_pPwm[2] = New CPwm437(2, m_pClock);

		m_pPwm[3] = New CPwm437(3, m_pClock);

		m_pPru[0] = New CPru437(0, 0);

		m_pPru[1] = New CPru437(1, 0);

		InitPru();

		piob->RegisterSingleton("dev.leds", 0, Create_Leds(this));

		piob->RegisterSingleton("dev.input-s", 0, Create_InputSwitch(this));
		
		piob->RegisterSingleton("dev.fram", 0, Create_Mr25Hxx(0));

		piob->RegisterSingleton("dev.rtc", 0, Create_RtcAbmc());

		piob->RegisterSingleton("dev.ident", 0, Create_Identity(addrIdentity));

		piob->RegisterSingleton("dev.sdhost", 0, Create_SdHost437(m_pClock));

		piob->RegisterSingleton("dev.uart", 0, Create_Uart437(1, m_pClock, NULL));

		piob->RegisterSingleton("dev.uart", 1, Create_Uart437(2, m_pClock, m_pPru[0]));

		piob->RegisterSingleton("dev.uart", 2, Create_Uart437(3, m_pClock, m_pPru[1]));

		piob->RegisterSingleton("dev.uart", 3, Create_Uart437(0, m_pClock, NULL));

		piob->RegisterSingleton("dev.enet-d", 0, Create_DualEnet437(m_pClock, MAKELONG(MAKEWORD(6, 5), MAKEWORD(7, 5))));

		piob->RegisterSingleton("dev.usbctrl-d", 0, Create_UsbFunc437(0));

		piob->RegisterSingleton("dev.usbctrl-h", 0, Create_UsbHost437(1));

		piob->RegisterSingleton("dev.usbctrl-x", 0, Create_UsbHostExtensible());

		piob->RegisterSingleton("dev.usbctrl-c", 0, Create_UsbHostExtensibleController());

		piob->RegisterSingleton("dev.temp", 0, Create_Temp112x(0x92, 800, 900));

		piob->RegisterSingleton("dev.temp", 1, Create_Temp112x(0x94, 800, 900));

		piob->RegisterSingleton("dev.temp", 2, Create_Temp112x(0x96, 800, 900));

		InitLeds();

		InitUarts();

		InitUsb();

		RegisterRedCommon();

		InitRack();

		InitBackplane();

		return TRUE;
	}

	return FALSE;
}

// IPlatform

PCTXT CPlatformManticore::GetModel(void)
{
	static char sName[8] = { 0 };

	if( sName[0] == NULL ) {

		strcpy(sName, "da?0d0?");
		
		switch( m_uModel ) {

			case MODEL_MANTICORE_10 : sName[2] = '1'; break;
			case MODEL_MANTICORE_30 : sName[2] = '3'; break;
			case MODEL_MANTICORE_50 : sName[2] = '5'; break;
			case MODEL_MANTICORE_70 : sName[2] = '7'; break;
		}

		sName[6] = 'a' + m_uSerial;
	}

	return sName;	
}

PCTXT CPlatformManticore::GetVariant(void)
{
	return GetModel();
}

UINT CPlatformManticore::GetFeatures(void)
{
	return	0 * rfGraphiteModules |
		1 * rfSerialModules   |
		0 * rfDisplay;
}

// IPortSwitch

UINT METHOD CPlatformManticore::GetCount(UINT uUnit)
{
	switch( m_uPorts[m_uSerial][uUnit] ) {

		case physicalRS232:
		case physicalRS485:

			return 1;
	}

	return 0;
}

UINT METHOD CPlatformManticore::GetMask(UINT uUnit)
{
	switch( m_uPorts[m_uSerial][uUnit] ) {

		case physicalRS232:

			return (1 << physicalRS232);

		case physicalRS485:

			return (1 << physicalRS485) | (1 << physicalRS422Master) | (1 << physicalRS422Slave);
		}

	return physicalNone;
}

UINT METHOD CPlatformManticore::GetType(UINT uUnit, UINT uLog)
{
	if( uLog == 0 ) {

		return m_uPorts[m_uSerial][uUnit];
		}

	return physicalNone;
}

BOOL METHOD CPlatformManticore::EnablePort(UINT uUnit, BOOL fEnable)
{
	switch( uUnit ) {

		case 0:
			m_pGpio[5]->SetState(25, fEnable);

			return true;

		case 1:
			m_pGpio[3]->SetState(25, fEnable);

			return true;

		case 2:
			m_pGpio[3]->SetState(17, fEnable);

			return true;

		case 3:
			m_pGpio[3]->SetState(16, fEnable);

			return true;
	}

	return false;
}

BOOL METHOD CPlatformManticore::SetPhysical(UINT uUnit, BOOL fRS485)
{
	return false;
}

BOOL METHOD CPlatformManticore::SetFull(UINT uUnit, BOOL fFull)
{
	switch( uUnit ) {

		case 2:
			m_pGpio[0]->SetState(12, !fFull);

			return true;

		case 3:
			m_pGpio[0]->SetState(13, !fFull);

			return true;
	}

	return false;
}

BOOL METHOD CPlatformManticore::SetMode(UINT uUnit, BOOL fAuto)
{
	return false;
}

// ILeds

void METHOD CPlatformManticore::SetLed(UINT uLed, UINT uState)
{
	UINT iSel = HIBYTE(uLed);

	UINT iLed = LOBYTE(uLed);

	if( iSel == 0 ) {

		switch( iLed ) {

			case 0:
				m_pPwm[2]->SetDuty(CPwm437::pwmChanA, (uState & stateRed) ? m_nLedLevel : 0);

				m_pPwm[2]->SetDuty(CPwm437::pwmChanB, (uState & stateGreen) ? m_nLedLevel : 0);

				break;

			case 1:
				m_pPwm[0]->SetDuty(CPwm437::pwmChanA, (uState & stateGreen) ? m_nLedLevel : 0);

				m_pPwm[0]->SetDuty(CPwm437::pwmChanB, (uState & stateBlue) ? m_nLedLevel : 0);

				m_pPwm[1]->SetDuty(CPwm437::pwmChanA, (uState & stateRed) ? m_nLedLevel : 0);

				break;

			case 2:
				m_pGpio[4]->SetState(26, !!(uState & stateGreen));

				m_pGpio[4]->SetState(27, !!(uState & stateRed));

				break;
		}

		return;
	}

	if( iSel == 1 ) {

		switch( iLed ) {

			case 0:
				m_pGpio[4]->SetState(28, uState != stateOff);

				break;

			case 1:
				m_pGpio[4]->SetState(29, uState != stateOff);

				break;
		}

		return;
	}
}

void METHOD CPlatformManticore::SetLedLevel(UINT uPercent, bool fPersist)
{
	m_nLedLevel = uPercent;
}

UINT METHOD CPlatformManticore::GetLedLevel(void)
{
	return m_nLedLevel;
}

// IInputSwitch

UINT METHOD CPlatformManticore::GetSwitches(void)
{
	return 0xFFFFFFFE | (GetSwitch(0) ? Bit(0) : 0);
}

UINT METHOD CPlatformManticore::GetSwitch(UINT uSwitch)
{
	if( uSwitch == 0 ) {

		return m_pGpio[2]->GetState(1);
	}

	return 1;
}

// IUsbSystem

void METHOD CPlatformManticore::OnDeviceArrival(IUsbHostFuncDriver *pDriver)
{
	IUsbDevice *pDev = NULL;

	pDriver->GetDevice(pDev);

	UsbTreePath const &Path = pDev->GetTreePath();

	if( GetPortType(Path) == typeSystem ) {

		if( pDriver->GetClass() == devHub ) {

			IUsbHubDriver *pHub = (IUsbHubDriver *) pDriver;

			UsbHubLedMap Map = {

				{ 0, 0, 0, MAKEWORD(0,1), 0, 0, 0, 0 },
				{ 0, 0, 0, MAKEWORD(1,1), 0, 0, 0, 0 },
			};

			pHub->SetLedMap(Map);
		}

		return;
	}

	if( GetPortType(Path) == typeOption ) {

		switch( Path.a.dwPort2 ) {

			case 1:
			case 2:
			case 3:
				SetSledReset(Path.a.dwPort2 - 1, false);

				break;
		}

		return;
	}
}

void METHOD CPlatformManticore::OnDeviceRemoval(IUsbHostFuncDriver *pDriver)
{
	IUsbDevice *pDev = NULL;

	pDriver->GetDevice(pDev);

	UsbTreePath const &Path = pDev->GetTreePath();

	if( GetPortType(Path) == typeOption ) {

		switch( Path.a.dwPort2 ) {

			case 1:
			case 2:
			case 3:
				SetSledReset(Path.a.dwPort2 - 1, true);

				break;
		}

		return;
	}
}

// IUsbSystemPortMapper

UINT METHOD CPlatformManticore::GetPortCount(void)
{
	return 1;
}

UINT METHOD CPlatformManticore::GetPortType(UsbTreePath const &Path)
{
	if( Path.a.dwTier == 1 ) {

		return typeSystem;
	}

	if( Path.a.dwTier == 2 ) {

		switch( Path.a.dwPort2 ) {

			case 1:
			case 2:
			case 3:
				return typeOption;

			case 4:
				return typeExtern;
		}
	}

	return typeUnknown;
}

UINT METHOD CPlatformManticore::GetPortReset(UsbTreePath const &Path)
{
	// TODO -- Is this a quicker recovery option? 

	if( Path.a.dwTier == 2 ) {

		switch( Path.a.dwPort2 ) {

			case 1: return 5 * 32 + 4;
			case 2: return 5 * 32 + 5;
			case 3: return 5 * 32 + 10;
		}
	}

	return NOTHING;
}

// IUsbSystemPower

void METHOD CPlatformManticore::OnNewDevice(UsbTreePath const &Path, UINT uPower)
{
	// TODO !!!
}

void METHOD CPlatformManticore::OnDelDevice(UsbTreePath const &Path, UINT uPower)
{
	// TODO !!!
}

// IEventSink

void CPlatformManticore::OnEvent(UINT uLine, UINT uParam)
{
	if( uLine == INT_GPIO5A ) {

		OnTempAlert();

		return;
	}

	CPlatformSitara::OnEvent(uLine, uParam);
}

// Events

void CPlatformManticore::OnTempAlert(void)
{
	if( !m_pGpio[5]->GetState(23) ) {

		// TODO -- What should we do here? Reduce the operating
		// state or otherwise slow things down?

		AfxTrace("\n\n*** OVER TEMPERATURE ALERT\n");

		return;
	}

	AfxTrace("\n\n*** OVER TEMPERATURE CLEARED\n");
}

// Inititialisation

void CPlatformManticore::InitClocks(void)
{
	CPlatformSitara::InitClocks();

	m_pClock->SetClockMode(CClock437::clockPWM0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM1, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM2, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM3, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPRU, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART1, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART2, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART3, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockMAC0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockCPSW, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUSB0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUSBPHY0, CClock437::modeSwWakup);
}

void CPlatformManticore::InitMux(void)
{
	CPlatformSitara::InitMux();

	DWORD const muxLed[] = {

		CCtrl437::pinCAM0DATA4,		CCtrl437::muxMode7,
		CCtrl437::pinCAM0DATA5,		CCtrl437::muxMode7,
		CCtrl437::pinCAM0DATA6,		CCtrl437::muxMode7,
		CCtrl437::pinCAM0DATA7,		CCtrl437::muxMode7,
	};

	DWORD const muxSwi[] = {

		CCtrl437::pinGPMCCLK,		CCtrl437::muxMode7 | CCtrl437::padSlow | CCtrl437::padPullUp | CCtrl437::padRxActive,
	};

	DWORD const muxPwm[] = {

		CCtrl437::pinSPI0SCLK,		CCtrl437::muxMode3,
		CCtrl437::pinSPI0D0,		CCtrl437::muxMode3,
		CCtrl437::pinSPI0D1,		CCtrl437::muxMode8,
		CCtrl437::pinSPI0CS0,		CCtrl437::muxMode8,
		CCtrl437::pinSPI0CS1,		CCtrl437::muxMode8,
		CCtrl437::pinECAP0INPWM0OUT,	CCtrl437::muxMode8,
		CCtrl437::pinCAM1FIELD,		CCtrl437::muxMode8,
		CCtrl437::pinCAM1WEN,		CCtrl437::muxMode8,
	};

	DWORD const muxHub[] = {

		CCtrl437::pinGPIO5_13,		CCtrl437::muxMode7 | CCtrl437::padRxActive,
		CCtrl437::pinCAM1VD,		CCtrl437::muxMode7 | CCtrl437::padSlow | CCtrl437::padRxActive,
		CCtrl437::pinCAM1PCLK,		CCtrl437::muxMode7 | CCtrl437::padSlow | CCtrl437::padRxActive,
	};

	DWORD const muxSled[] = {

		CCtrl437::pinUART1RXD,		CCtrl437::muxMode7 | CCtrl437::padSlow | CCtrl437::padRxActive,
		CCtrl437::pinUART1TXD,		CCtrl437::muxMode7 | CCtrl437::padSlow | CCtrl437::padRxActive,
		CCtrl437::pinCLKREQ,		CCtrl437::muxMode7 | CCtrl437::padSlow | CCtrl437::padRxActive,
		CCtrl437::pinSPI4SCLK,		CCtrl437::muxMode7 | CCtrl437::padSlow | CCtrl437::padRxActive | CCtrl437::padPullNo,
		CCtrl437::pinSPI4D0,		CCtrl437::muxMode7 | CCtrl437::padSlow | CCtrl437::padRxActive | CCtrl437::padPullNo,
		CCtrl437::pinGPIO5_10,		CCtrl437::muxMode7 | CCtrl437::padSlow | CCtrl437::padRxActive | CCtrl437::padPullNo,
	};

	DWORD const muxPru[] = {

		CCtrl437::pinMCASP0FSX,		CCtrl437::muxMode6 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinMCASP0ACLKX,	CCtrl437::muxMode5 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinXDMAEVTINTR1,	CCtrl437::muxMode5 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCCSN2,		CCtrl437::muxMode5 | CCtrl437::padPullUp | CCtrl437::padRxActive,
	};

	DWORD const muxSer[] = {

		CCtrl437::pinUART0RXD,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinUART0TXD,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinUART1CTSN,		CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinUART1RTSN,		CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA0,		CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA1,		CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA3,		CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA2,		CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA4,		CCtrl437::muxMode2 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA5,		CCtrl437::muxMode2 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA7,		CCtrl437::muxMode7 | CCtrl437::padPullNo ,
		CCtrl437::pinCAM1DATA6,		CCtrl437::muxMode2 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinUART3RXD,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinUART3TXD,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinUART3RTSN,		CCtrl437::muxMode7 | CCtrl437::padPullNo ,
		CCtrl437::pinUART3CTSN,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMCASP0AXR0,	CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMCASP0AHCLKR,	CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinSPI2CS0,		CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
	};

	DWORD const muxSd[] = {

		CCtrl437::pinMMC0CMD,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0CLK,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT0,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT1,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT2,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT3,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM0DATA2,		CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
	};

	DWORD const muxNic[] = {

		CCtrl437::pinMII1TXD0,		CCtrl437::muxMode1 | CCtrl437::padPullDn ,
		CCtrl437::pinMII1TXD1,		CCtrl437::muxMode1 | CCtrl437::padPullDn ,
		CCtrl437::pinMII1RXD0,		CCtrl437::muxMode1 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXD1,		CCtrl437::muxMode1 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinMII1TXEN,		CCtrl437::muxMode1 | CCtrl437::padPullDn ,
		CCtrl437::pinMII1REFCLK,	CCtrl437::muxMode0 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinMII1CRS,		CCtrl437::muxMode1 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinMDIOCLK,		CCtrl437::muxMode0 | CCtrl437::padPullUp ,
		CCtrl437::pinMDIODATA,		CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCA4,		CCtrl437::muxMode3 | CCtrl437::padPullDn ,
		CCtrl437::pinGPMCA5,		CCtrl437::muxMode3 | CCtrl437::padPullDn ,
		CCtrl437::pinGPMCA10,		CCtrl437::muxMode3 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinGPMCA11,		CCtrl437::muxMode3 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinGPMCA0,		CCtrl437::muxMode3 | CCtrl437::padPullDn ,
		CCtrl437::pinMII1COL,		CCtrl437::muxMode1 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinGPMCWAIT0,		CCtrl437::muxMode3 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinGPIO5_8,		CCtrl437::muxMode7 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPIO5_9,		CCtrl437::muxMode7 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinSPI4D1,		CCtrl437::muxMode7 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinSPI4CS0,		CCtrl437::muxMode7 | CCtrl437::padPullUp | CCtrl437::padRxActive,
	};

	DWORD const muxTemp[] = {

		CCtrl437::pinGPMCAD11,		CCtrl437::muxMode9 | CCtrl437::padSlow | CCtrl437::padPullUp | CCtrl437::padRxActive
	};

	DWORD const muxBus[] = {

		CCtrl437::pinGPMCAD9,		CCtrl437::muxMode9 | CCtrl437::padSlow | CCtrl437::padRxActive
	};

	m_pCtrl->SetMux(muxLed, elements(muxLed));

	m_pCtrl->SetMux(muxSwi, elements(muxSwi));

	m_pCtrl->SetMux(muxPwm, elements(muxPwm));

	m_pCtrl->SetMux(muxHub, elements(muxHub));

	m_pCtrl->SetMux(muxSled, elements(muxSled));

	m_pCtrl->SetMux(muxPru, elements(muxPru));

	m_pCtrl->SetMux(muxSer, elements(muxSer));

	m_pCtrl->SetMux(muxSd, elements(muxSd));

	m_pCtrl->SetMux(muxNic, elements(muxNic));

	m_pCtrl->SetMux(muxTemp, elements(muxTemp));

	m_pCtrl->SetMux(muxBus, elements(muxBus));
}

void CPlatformManticore::InitGpio(void)
{
	CPlatformSitara::InitGpio();

	m_pGpio[3]->SetState(16, false);
	m_pGpio[3]->SetState(17, false);
	m_pGpio[3]->SetState(25, false);
	m_pGpio[4]->SetState(10, false);
	m_pGpio[4]->SetState(11, false);
	m_pGpio[5]->SetState(13, false);
	m_pGpio[5]->SetState(25, false);

	m_pGpio[0]->SetDirection(12, true);
	m_pGpio[0]->SetDirection(13, true);
	m_pGpio[0]->SetDirection(14, false);
	m_pGpio[0]->SetDirection(15, false);
	m_pGpio[0]->SetDirection(24, false);
	m_pGpio[2]->SetDirection(1, false);
	m_pGpio[3]->SetDirection(16, true);
	m_pGpio[3]->SetDirection(17, true);
	m_pGpio[3]->SetDirection(25, true);
	m_pGpio[4]->SetDirection(10, true);
	m_pGpio[4]->SetDirection(11, true);
	m_pGpio[4]->SetDirection(24, false);
	m_pGpio[4]->SetDirection(26, true);
	m_pGpio[4]->SetDirection(27, true);
	m_pGpio[4]->SetDirection(28, true);
	m_pGpio[4]->SetDirection(29, true);
	m_pGpio[5]->SetDirection(4, false);
	m_pGpio[5]->SetDirection(5, false);
	m_pGpio[5]->SetDirection(6, false);
	m_pGpio[5]->SetDirection(7, false);
	m_pGpio[5]->SetDirection(10, false);
	m_pGpio[5]->SetDirection(13, true);
	m_pGpio[5]->SetDirection(23, false);
	m_pGpio[5]->SetDirection(25, true);

	m_pGpio[5]->SetIntHandler(23, intEdgeAny, this, 0);

	m_pGpio[5]->SetIntEnable(23, true);
}

void CPlatformManticore::InitMisc(void)
{
	CPlatformSitara::InitMisc();

	m_pCtrl->SetPWM(0, true);

	m_pCtrl->SetPWM(1, true);

	m_pCtrl->SetPWM(2, true);

	m_pCtrl->SetPWM(3, true);

	m_pCtrl->SetMIIMode(0, CCtrl437::modeRMII);

	m_pCtrl->SetMIIMode(1, CCtrl437::modeRMII);

	m_pCtrl->SetPhyPower(0, true);

	m_pCtrl->SetPhyPower(1, true);
}

void CPlatformManticore::InitPru(void)
{
	m_pPcm->EnablePru();

	PCTXT pCode[2] = { PRU_CODE0, PRU_CODE1 };

	for( UINT i = 0; i < 2; i++ ) {

		m_pPru[i]->Stop(0);

		m_pPru[i]->Reset(0);

		m_pPru[i]->SetMux(0);

		m_pPru[i]->LoadCode(0, pCode[i]);
	}
}

void CPlatformManticore::InitUarts(void)
{
	IIdentity *pIdentity = NULL;

	piob->GetObject("dev.ident", 0, AfxAeonIID(IIdentity), (void **) &pIdentity);

	if( pIdentity ) {

		m_uSerial = pIdentity->GetPropWord(CIdentity::propSerial);

		m_uSerial = m_uSerial >= 'A' ? m_uSerial - 'A'  : 6;

		pIdentity->Release();
		}

	for( UINT i = 0; i < 4; i ++ ) {

		IPortObject *pPort = NULL;

		AfxGetObject("uart", i, IPortObject, pPort);

		if( pPort ) {

			pPort->Bind(this);

			pPort->Release();
		}
	}
}

void CPlatformManticore::InitUsb(void)
{
	IUsbDriver *p0, *p1, *p2;

	piob->GetObject("usbctrl-h", 0, AfxAeonIID(IUsbDriver), (void **) &p0);

	piob->GetObject("usbctrl-x", 0, AfxAeonIID(IUsbDriver), (void **) &p1);

	piob->GetObject("usbctrl-c", 0, AfxAeonIID(IUsbDriver), (void **) &p2);

	if( p0 && p1 && p2 ) {

		p0->Bind(p1);

		p1->Bind(p2);
	}

	AfxRelease(p0);

	AfxRelease(p1);

	AfxRelease(p2);
}

void CPlatformManticore::InitRack(void)
{
	IUsbHostStack    *pStack;

	IExpansionSystem *pExpSys;

	IUsbSystem       *pExpUsb;

	AfxGetObject("usb.host", 0, IUsbHostStack, pStack);

	AfxGetObject("usb.rack", 0, IExpansionSystem, pExpSys);

	AfxGetObject("usb.rack", 0, IUsbSystem, pExpUsb);

	if( pStack ) {

		pStack->Init();

		pStack->Attach((IUsbSystem *) this);

		pStack->Attach(pExpUsb);
	}

	AfxRelease(pExpSys);

	AfxRelease(pStack);

	AfxRelease(pExpUsb);

	m_pGpio[5]->SetState(13, false);

	Sleep(5);

	SetHostPower(powerLow);

	m_pGpio[5]->SetState(13, true);
}

void CPlatformManticore::InitBackplane(void)
{
	m_pPwm[3]->SetFreq(1000000);

	m_pPwm[3]->SetDuty(CPwm437::pwmChanA, 50);
}

void CPlatformManticore::InitLeds(void)
{
	m_nLedLevel = 100;

	m_pPwm[0]->SetFreq(5000);
	m_pPwm[1]->SetFreq(5000);
	m_pPwm[2]->SetFreq(5000);

	SetLed(MAKEWORD(0, 0), stateOff);
	SetLed(MAKEWORD(1, 0), stateOff);
	SetLed(MAKEWORD(2, 0), stateOff);
	SetLed(MAKEWORD(0, 1), stateOff);
	SetLed(MAKEWORD(1, 1), stateOff);
}

// Implementation

void CPlatformManticore::SetHostPower(UINT uPower)
{
	switch( uPower ) {

		case powerLow:

			m_pGpio[4]->SetState(10, true);

			m_pGpio[4]->SetState(11, true);

			break;

		case powerMed:

			m_pGpio[4]->SetState(10, true);

			m_pGpio[4]->SetState(11, false);

			break;

		case powerHigh:

			m_pGpio[4]->SetState(10, false);

			m_pGpio[4]->SetState(11, false);

			break;
	}
}

void CPlatformManticore::SetSledReset(UINT uSled, bool fReset)
{
	switch( uSled ) {

		case 0:
			m_pGpio[5]->SetState(4, !fReset);

			m_pGpio[5]->SetDirection(4, !fReset);

			break;

		case 1:
			m_pGpio[5]->SetState(5, !fReset);

			m_pGpio[5]->SetDirection(5, !fReset);

			break;

		case 2:
			m_pGpio[5]->SetState(10, !fReset);

			m_pGpio[5]->SetDirection(10, !fReset);

			break;
	}
}

// End of File
