
#include "Intern.hpp"

#include "Enet437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader()

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Macros  
//

#define RoundUp(x, n)	((x + (n-1)) & ~(n-1))

//////////////////////////////////////////////////////////////////////////
//
// Lower Level Routines
//

clink void * _malloc_r(_reent *reent, size_t size);

clink void   _free_r(_reent *reent, void *data);

///////////////////////////////////////////////////////////////////////////
//
// Broadcast Address
//

static MACADDR macBroad = { { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF } };

//////////////////////////////////////////////////////////////////////////
//
// AM437 Ethernet Module
//

// Register Access

#define Reg(x)	  (m_pBase[reg##x])

#define RegN(x,n) (m_pBase[reg##x + n])

// Instantiator

IDevice * Create_Enet437(CClock437 *pClock)
{
	CEnet437 *pDevice = New CEnet437(pClock, MAKEWORD(8,5));

	pDevice->Open();

	return (IDevice *)(INic *) pDevice;
	}

// Constructor

CEnet437::CEnet437(CClock437 *pClock, UINT uGpio)
{
	StdSetRef();

	m_pBase      = PVDWORD(ADDR_CPSW);

	m_uLineRt    = INT_3PGSWRXTHR0;
		       
	m_uLineRx    = INT_3PGSWRX0;
		       
	m_uLineTx    = INT_3PGSWTX0;
		     
	m_uLineMs    = INT_3PGSWMISC0;

	m_uFreq      = pClock->GetCoreM4Freq() / 2;

	m_uCore      = 0;

	m_uChan      = 0;

	m_uGpioReset = LOBYTE(uGpio);

	m_pMacList   = NULL;

	m_uMacList   = 0;

	m_uNicState  = nicInitial;

	m_uPhyState  = phyIdle;

	m_uMiiState  = miiIdle;

	m_uFlags     = 0;

	m_nPoll	     = 0;

	m_pMiiFlag   = Create_AutoEvent();
	
	m_pLinkOkay  = Create_ManualEvent();
	
	m_pLinkDown  = Create_ManualEvent();
	
	m_pTxLock    = Create_Mutex();
	
	m_pRxLock    = Create_Mutex();
	
	m_pTxFlag    = Create_Semaphore();
	
	m_pRxFlag    = Create_Semaphore();

	AfxGetObject("gpio", HIBYTE(uGpio), IGpio, m_pGpioReset);

	AllocBuffers();

	DiagRegister();
	}

// Destructor

CEnet437::~CEnet437(void)
{
	DiagRevoke();

	AfxRelease(m_pGpioReset);

	m_pMiiFlag->Release();

	m_pLinkOkay->Release();

	m_pLinkDown->Release();

	m_pTxLock->Release();

	m_pRxLock->Release();

	m_pTxFlag->Release();

	m_pRxFlag->Release();

	FreeBuffers();

	FreeMacList();
	}

// IUnknown

HRESULT CEnet437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(INic);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CEnet437::AddRef(void)
{
	StdAddRef();
	}

ULONG CEnet437::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CEnet437::Open(void)
{
	ResetPhy(true);

	ResetController();

	ResetCounters();
	
	return TRUE;
	}

// INic

bool METHOD CEnet437::Open(bool fFast, bool fFull)
{
	if( m_uNicState == nicClosed ) {

		m_fAllowFast = fFast;

		m_fAllowFull = fFull;

		m_uNicState  = nicOpen;

		m_pLinkOkay->Clear();

		m_pLinkDown->Set();

		ResetState();

		ResetCounters();

		ConfigController();

		EnableEvents();

		ResetPhy(false);

		ConfigPhy();

		return true;
		}

	return false;
	}

bool METHOD CEnet437::Close(void)
{
	if( m_uNicState >= nicOpen ) {

		DisableEvents();

		OnLinkDown();

		m_uNicState = nicClosed;

		ResetController();

		ResetPhy(true);

		ResetCounters();

		return true;
		}

	return false;
	}

bool METHOD CEnet437::InitMac(MACADDR const &Addr)
{
	if( m_uNicState == nicInitial ) {

		m_pMacList    = (MACADDR *) _malloc_r(NULL, 2 * sizeof(MACADDR));
		
		m_uMacList    = 2;

		m_MacAddr     = Addr;

		m_pMacList[0] = Addr;

		m_pMacList[1] = macBroad;

		m_uNicState   = nicClosed;

		return true;
		}

	return false;
	}

void METHOD CEnet437::ReadMac(MACADDR &Addr)
{
	Addr = m_MacAddr; 
	}

UINT METHOD CEnet437::GetCapabilities(void)
{
	return nicFilterIp | nicCheckSums | nicAddSums;
	}

void METHOD CEnet437::SetFlags(UINT uFlags)
{
	m_uFlags = uFlags;
	}

bool METHOD CEnet437::SetMulticast(MACADDR const *pList, UINT uList)
{
	UINT     uMacList = uList + 2;

	MACADDR *pMacList = (MACADDR *) _malloc_r(NULL, uMacList * sizeof(MACADDR));

	UINT     uMacSlot = 0;

	pMacList[uMacSlot++] = m_MacAddr;

	pMacList[uMacSlot++] = macBroad;

	if( pList && uList ) {

		while( uList-- ) {

			pMacList[uMacSlot++] = *pList++;
			}
		}

	MACADDR *pOldList = m_pMacList;

	UINT ipr;

	HostRaiseIpr(ipr);

	m_uMacList = uMacList;

	m_pMacList = pMacList;

	HostLowerIpr(ipr);

	_free_r(NULL, pOldList);

	return true;
	}

bool METHOD CEnet437::IsLinkActive(void)
{
	return m_uNicState == nicActive;
	}

bool METHOD CEnet437::WaitLink(UINT uTime)
{
	return m_pLinkOkay->Wait(uTime);
	}

bool METHOD CEnet437::SendData(CBuffer *pBuff, UINT uTime)
{
	if( m_uNicState == nicActive ) {

		UINT uWait = WaitMultiple( m_pTxFlag,
					   m_pLinkDown,
					   uTime
					   );

		if( uWait == 1 ) {

			WORD  wSize = pBuff->GetSize();

			PBYTE pData = pBuff->GetData();

			m_pTxLock->Wait(FOREVER);	
				
			CNicBuffer *pDesc = m_pTxBuff + m_uTxTail;
			
			PBYTE       pDest = pDesc->m_pData;

			m_uTxTail = (m_uTxTail + 1) % constTxLimit;

			AddMac(pData);

			ArmMemCpy(pDest, pData, wSize);

			if( wSize < constMinPacket ) {

				memset(pDest + wSize, 0x00, constMinPacket - wSize);

				pDesc->m_pDesc->m_BuffLen = constMinPacket;
				}
			else {
				pDesc->m_pDesc->m_BuffLen = wSize;
				}

			StartSend(pDesc);
	
			m_pTxLock->Free();

			return true;
			}
		}

	return false;
	}

bool METHOD CEnet437::ReadData(CBuffer * &pBuff, UINT uTime)
{
	if( m_uNicState == nicActive ) {

		for(;;) {

			UINT uWait = WaitMultiple( m_pRxFlag,
						   m_pLinkDown,
						   uTime
						   );

			if( uWait == 1 ) {

				m_pRxLock->Wait(FOREVER);

				CNicBuffer *pDesc = m_pRxBuff + m_uRxHead;

				m_uRxHead = (m_uRxHead + 1) % constRxLimit;

				if( !(pDesc->m_pDesc->m_Flags & flagErrors) ) {

					PBYTE pData  = pDesc->m_pData;

					WORD  wCount = (pDesc->m_pDesc->m_Flags & flagPassCrc) ? (pDesc->m_pDesc->m_BuffLen - 4) : pDesc->m_pDesc->m_BuffLen;

					BOOL  fIp    = MotorToHost((WORD &) pData[12]) == 0x0800;

					if( !(m_uFlags & nicOnlyIp) || fIp ) {

						UINT ipr;

						HostRaiseIpr(ipr);

						for( UINT n = 0; n < m_uMacList; n++ ) {

							if( !memcmp(pData, m_pMacList + n, 6) ) {

								HostLowerIpr(ipr);

								CBuffer *pWork  = BuffAllocate(wCount);

								if( pWork ) {

									PBYTE pDest = pWork->AddTail(wCount);

									ArmMemCpy(pDest, pData, wCount);

									if( fIp ) {

										pWork->SetFlag(packetTypeIp);
										}

									pWork->SetFlag(packetTypeValid);

									pWork->SetFlag(packetSumsValid);

									ContinueRecv(pDesc);

									m_pRxLock->Free();

									pBuff = pWork;

									return true;
									}

								break;
								}
							}

						HostLowerIpr(ipr);
						}
					}

				ContinueRecv(pDesc);

				m_pRxLock->Free();

				continue;
				}

			break;
			}
		}

	pBuff = NULL;

	return false;
	}

void METHOD CEnet437::GetCounters(NICDIAG &Diag)
{
	Diag.m_TxCount = Reg(NetTxGood);
	
	Diag.m_TxFail  = 0;
	
	Diag.m_TxDisc  = Reg(NetExcesCol) + Reg(NetLateCol);
	
	Diag.m_RxCount = Reg(NetRxGood);
	
	Diag.m_RxOver  = Reg(NetRxSofOver) + Reg(NetRxMofOver) + Reg(NetRxDmaOver);
	
	Diag.m_RxDisc  = Reg(NetRxCrc) + Reg(NetRxAlign) + Reg(NetRxJabber) + Reg(NetRxShort) + Reg(NetRxFrag) + Reg(NetRxOver);
	}

void METHOD CEnet437::ResetCounters(void)
{
	Reg(NetTxGood)	  = Reg(NetTxGood);
	
	Reg(NetExcesCol)  = Reg(NetExcesCol);
		
	Reg(NetLateCol)	  = Reg(NetLateCol);
	
	Reg(NetRxGood)	  = Reg(NetRxGood);
	
	Reg(NetRxSofOver) = Reg(NetRxSofOver);
	
	Reg(NetRxMofOver) = Reg(NetRxMofOver);
	
	Reg(NetRxDmaOver) = Reg(NetRxDmaOver);
	
	Reg(NetRxCrc)     = Reg(NetRxCrc);
	
	Reg(NetRxAlign)   = Reg(NetRxAlign);
	
	Reg(NetRxJabber)  = Reg(NetRxJabber);
	
	Reg(NetRxShort)   = Reg(NetRxShort);
	
	Reg(NetRxFrag)    = Reg(NetRxFrag);
	
	Reg(NetRxOver)    = Reg(NetRxOver);
	}

// IDiagProvider

UINT CEnet437::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);
		}

	return 0;
	}

// IEventSink

void CEnet437::OnEvent(UINT uLine, UINT uParam)
{
	if( uLine == m_uLineRx ) {

		OnRecv();

		return;
		}

	if( uLine == m_uLineTx ) {

		OnSend();

		return;
		}

	if( uLine == m_uLineMs ) {

		OnMisc();

		return;
		}
	}

// Event Handlers

void CEnet437::OnRecv(void)
{
	UINT uCount = 0;
	
	for(;;) {

		DWORD Data = Reg(WrC0RxStat);
		
		if( Data & (1 << m_uChan) ) {

			for(;;) {

				CNicBuffer *pBuff = &m_pRxBuff[m_uRxTail];

				CBuffDesc  *pDesc = pBuff->m_pDesc;

				if( !(pDesc->m_Flags & flagOwner) ) {

					m_uRxTail = (m_uRxTail + 1) % constRxLimit;

					uCount    = uCount + 1;

					if( pDesc->m_Flags & flagSop ) {

						if( pDesc->m_Flags & flagTeardown ) {

							break;
							}
						}

					if( pDesc->m_Flags & flagEop ) {
					
						if( pDesc->m_Flags & flagEoq ) {

							if( pDesc->m_NextPtr ) {

								AfxTrace("Recv EOQ Misqueue recovery.\n");

								RegN(StateRamRxHd, m_uChan) = pDesc->m_NextPtr;
								}
							
							break;
							}
						}
					
					continue;
					}

				break;
				}

			RegN(StateRamRxCp, m_uChan) = RegN(StateRamRxCp, m_uChan);

			continue;
			}
		
		Reg(DmaIntEoiVect) = intEoiRxPulse;

		break;
		}

	m_pRxFlag->Signal(uCount);
	}

void CEnet437::OnSend(void)
{
	UINT uCount = 0;
	
	for(;;) {

		DWORD Data = Reg(WrC0TxStat);

		if( Data & (1 << m_uChan) ) {

			while( m_uTxHead != m_uTxTail ) {

				CBuffDesc *pDesc = m_pTxBuff[m_uTxHead].m_pDesc;

				if( !(pDesc->m_Flags & flagOwner) ) {

					m_uTxHead = (m_uTxHead + 1) % constTxLimit;

					uCount    = uCount + 1;

					if( pDesc->m_Flags & flagSop ) {

						if( pDesc->m_Flags & flagTeardown ) {

							break;
							}
						}

					if( pDesc->m_Flags & flagEop ) {
					
						if( pDesc->m_Flags & flagEoq ) {

							if( pDesc->m_NextPtr ) {

								AfxTrace("Send EOQ Misqueue recovery.\n");

								RegN(StateRamTxHd, m_uChan) = pDesc->m_NextPtr;
								
								continue;
								}

							m_uTxLast = NOTHING;

							break;
							}
						}
				
					continue;
					}

				break;
				}

			RegN(StateRamTxCp, m_uChan) = RegN(StateRamTxCp, m_uChan);

			continue;
			}
		
		Reg(DmaIntEoiVect) = intEoiTxPulse;

		break;
		}

	m_pTxFlag->Signal(uCount);
	}

void CEnet437::OnMisc(void)
{
	for(;;) {

		DWORD Data = Reg(WrC0MiscStat) & Reg(WrC0MiscEn);

		if( Data & intMdioUser ) {

			OnMiiEvent();

			Reg(MdioUserIntRaw) = 0x3;

			continue;
			}

		if( Data & intMdioLink ) {

			OnPhyEvent();

			Reg(MdioLinkIntRaw) = 0x3;

			continue;
			}

		Reg(DmaIntEoiVect) = intEoiMisc;

		break;
		}
	}

// Implementation

void CEnet437::EnableEvents(void)
{
	phal->SetLineHandler(m_uLineRx, this, 0);
	phal->SetLineHandler(m_uLineTx, this, 0);
	phal->SetLineHandler(m_uLineMs, this, 0);

	phal->EnableLine(m_uLineRx, true);
	phal->EnableLine(m_uLineTx, true);
	phal->EnableLine(m_uLineMs, true);
	}

void CEnet437::DisableEvents(void)
{
	phal->EnableLine(m_uLineRx, false);
	phal->EnableLine(m_uLineTx, false);
	phal->EnableLine(m_uLineMs, false);
	}

// Mac Management

void CEnet437::ResetState(void)
{
	UINT uWait = (Hal_GetIrql() < IRQL_DISPATCH && GetCurrentThread() && GetThreadIndex() > 1) ? 5 : 0;

	while( m_pRxFlag->Wait(uWait) );

	while( m_pTxFlag->Wait(uWait) );

	m_uFlags = 0;

	ResetBuffers();

	m_pTxFlag->Signal(constTxLimit-1);
	}

void CEnet437::ResetController(void)
{
	ResetDma();

	ResetMac();

	ResetSwitch();
	}

void CEnet437::ConfigController(void)
{
	for( UINT i = 0; i < 8; i ++ ) {

		RegN(StateRamTxHd, i) = 0;
	
		RegN(StateRamRxHd, i) = 0;
	
		RegN(StateRamTxCp, i) = 0;
	
		RegN(StateRamRxCp, i) = 0;
		}

	Reg(DmaIntEoiVect)  = intEoiRxPulse;
			   
	Reg(DmaIntEoiVect)  = intEoiTxPulse;
			   
	Reg(DmaIntEnSet)    = Bit(0) | Bit(1);
			   
	Reg(SsStatsEn)	    = Bit(0) | Bit(1);
			   
	Reg(DmaTxIntEnSet)  = (1 << m_uChan);
			   
	Reg(DmaRxIntEnSet)  = (1 << m_uChan);
			   
	Reg(WrC0TxEn)       = (1 << m_uChan);
			   
	Reg(WrC0RxEn)       = (1 << m_uChan);
			   
	Reg(Port1Vlan)      = 0x00000001;
			   
	Reg(Port1SALo)      = (m_MacAddr.m_Addr[0] <<  8) |
			      (m_MacAddr.m_Addr[1] <<  0) ;
			       
	Reg(Port1SAHi)      = (m_MacAddr.m_Addr[2] << 24) |
			      (m_MacAddr.m_Addr[3] << 16) |
			      (m_MacAddr.m_Addr[4] <<  8) |
			      (m_MacAddr.m_Addr[5] <<  0) ;
			     
	Reg(Sl1RxMaxLen)    = constBuffSize;
			   
	Reg(AlePrescale)    = m_uFreq / 1000;
			   
	Reg(WrC0MiscEn)     = intMdioUser | intMdioLink;
			   
	Reg(MdioCtrl)       = mdioNoPreamble | (0xFF << mdioClockDiv);
			   
	Reg(MdioCtrl)      |= mdioEn;
			   
	Reg(MdioUserIntSet) = 0x03;
			   
	Reg(MdioPhySel0)    = 0x00;

	m_nPoll             = 0;
	}

void CEnet437::StartController(void)
{
	Reg(AleCtrl)      = aleTableClear; 
			                       
	Reg(AleCtrl)      = aleEnable;
			            
	Reg(AlePortCtrl0) = aleForward;
			            
	Reg(AlePortCtrl1) = aleForward;
			            
	Reg(Sl1Ctrl)      = m_fUsingFull ? Bit(0) : 0;

	Reg(Sl1Ctrl)     |= m_fUsingFast ? Bit(15) : 0;
			            
	Reg(Sl1Ctrl)     |= Bit(5);
		                    
	Reg(DmaRxCtrl)    = Bit(0);
		                    
	Reg(DmaTxCtrl)    = Bit(0);

	StartRecv(m_pRxDesc);
	}

void CEnet437::StopController(void)
{
	Reg(DmaTxTeardown) = m_uChan;

	Reg(DmaRxTeardown) = m_uChan;

	Reg(DmaRxCtrl)    &= ~Bit(0);

	Reg(DmaTxCtrl)    &= ~Bit(0);

	Reg(AlePortCtrl0)  = aleDisabled;

	Reg(AlePortCtrl1)  = aleDisabled;

	Reg(AlePortCtrl2)  = aleDisabled;
	}

void CEnet437::ResetSwitch(void)
{
	Reg(WrReset) = Bit(0);

	while( Reg(WrReset) & Bit(0) );

	Reg(SsReset) = Bit(0);

	while( Reg(SsReset) & Bit(0) );
	}

void CEnet437::ResetDma(void)
{
	Reg(DmaReset) = Bit(0);

	while( Reg(DmaReset) & Bit(0) );
	}

void CEnet437::ResetMac(void)
{
	Reg(Sl1Reset) = Bit(0);

	Reg(Sl2Reset) = Bit(0);

	while( Reg(Sl1Reset) & Bit(0) );

	while( Reg(Sl2Reset) & Bit(0) );
	}

void CEnet437::AddMac(PBYTE pData)
{
	pData[ 6] = m_MacAddr.m_Addr[0];
	pData[ 7] = m_MacAddr.m_Addr[1];
	pData[ 8] = m_MacAddr.m_Addr[2];
	pData[ 9] = m_MacAddr.m_Addr[3];
	pData[10] = m_MacAddr.m_Addr[4];
	pData[11] = m_MacAddr.m_Addr[5];
	}

void CEnet437::FreeMacList(void)
{
	if( m_pMacList ) {

		_free_r(NULL, m_pMacList);

		m_pMacList = NULL;

		m_uMacList = 0;
		}
	}

// Phy Management

void CEnet437::ResetPhy(bool fAssert)
{
	if( m_pGpioReset != NULL && m_uGpioReset != NOTHING ) {

		m_pGpioReset->SetState(m_uGpioReset, !fAssert);
		}
	}

void CEnet437::PhyPollEnable(bool fEnable)
{
	if( fEnable ) {

		if( AtomicIncrement(&m_nPoll) > 0 ) { 

			Reg(MdioPhySel0) |= Bit(6);
			}
		}
	else {
		if( AtomicDecrement(&m_nPoll) <= 0 ) {

			Reg(MdioPhySel0) &= ~Bit(6);
			}
		}
	}

void CEnet437::ConfigPhy(void)
{
	WORD Caps = 0x0021;

	if( m_fAllowFast ) {

		Caps |= 0x0080;
		}

	if( m_fAllowFull ) {

		Caps |= ((Caps & 0x00A0) << 1);
		}

	AppPutMii(0x04, Caps);

	AppPutMii(0x00, Bit(12));

	PhyPollEnable(true);
	}

void CEnet437::OnMiiEvent(void)
{
	PhyPollEnable(true);

	switch( m_uMiiState ) {

		case miiAppPut:
		case miiAppGet:

			m_uMiiState = miiIdle;

			m_pMiiFlag->Set();

			break;

		case miiIntPut:

			m_uMiiState = miiIdle;

			break;

		case miiIntGet:

			m_uMiiState = miiIdle;

			OnPhyEvent();

			break;
		}
	}

void CEnet437::OnPhyEvent(void)
{
	if( m_uPhyState == phyIdle ) {

		m_uPhyState = phyGetIsr;

		IntGetMii(0x01);

		return;
		}

	if( m_uPhyState == phyGetIsr ) {

		WORD Data = LOWORD(Reg(MdioAccess0));

		if( Data & Bit(2) ) {

			m_uPhyState = phyGetLink;

			IntGetMii(0x1F);

			return;
			}

		m_uPhyState = phyIdle;

		OnLinkDown();

		return;
		}

	if( m_uPhyState == phyGetLink ) {

		WORD Data = LOWORD(Reg(MdioAccess0));

		if( Data & Bit(12) ) {

			m_fUsingFast = (Data & Bit(3)) ? true : false;

			m_fUsingFull = (Data & Bit(4)) ? true : false;

			m_uPhyState  = phyIdle;

			OnLinkUp();
			}

		return;
		}
	}

void CEnet437::OnLinkUp(void)
{
	if( m_uNicState == nicOpen ) {

		ResetState();

		StartController();

		m_pLinkDown->Clear();

		m_pLinkOkay->Set();

		m_uNicState = nicActive;
		}
	}

void CEnet437::OnLinkDown(void)
{
	if( m_uNicState == nicActive ) {

		m_pLinkOkay->Clear();

		m_pLinkDown->Set();

		StopController();

		m_uNicState = nicOpen;
		}
	}

// Buffer Management

void CEnet437::AllocBuffers(void)
{
	m_pTxBuff = New CNicBuffer[ constTxLimit ];

	m_pRxBuff = New CNicBuffer[ constRxLimit ];

	m_pTxDesc = (CBuffDesc *) AllocNoneCached(sizeof(CBuffDesc) * constTxLimit, 16);

	m_pRxDesc = (CBuffDesc *) AllocNoneCached(sizeof(CBuffDesc) * constRxLimit, 16);

	m_pTxData = (PBYTE)       AllocNoneCached(constTxLimit * constBuffSize, 16);

	m_pRxData = (PBYTE)       AllocNoneCached(constRxLimit * constBuffSize, 16);

	ResetBuffers();
	}

PVOID CEnet437::AllocNoneCached(UINT uAlloc, UINT uAlign)
{
	// TODO - Make shared.

	uAlign = RoundUp(uAlign, 64);

	uAlloc = RoundUp(uAlloc, 64);

	PVOID pMem  = memalign(uAlign, uAlloc);

	DWORD Alias = phal->GetNonCachedAlias(DWORD(pMem));

	DWORD Phy   = phal->VirtualToPhysical(DWORD(pMem));

	ProcPurgeDataCache(Phy, uAlloc);

	return PVOID(Alias);
	}

void CEnet437::ResetBuffers(void)
{
	memset(m_pTxDesc, 0, sizeof(CBuffDesc) * constTxLimit);

	memset(m_pRxDesc, 0, sizeof(CBuffDesc) * constRxLimit);

	for( UINT t = 0; t < constTxLimit; t ++ ) {

		CNicBuffer &Buff = m_pTxBuff[t];

		CBuffDesc  &Desc = m_pTxDesc[t];

		Buff.m_iIndex    = t;

		Buff.m_pDesc     = &Desc;

		Buff.m_dwDesc    = phal->VirtualToPhysical(DWORD(Buff.m_pDesc));

		Buff.m_pData     = m_pTxData + t * constBuffSize;

		Buff.m_dwData    = phal->VirtualToPhysical(DWORD(Buff.m_pData));

		Desc.m_BuffPtr   = Buff.m_dwData;
		}

	for( UINT r = 0; r < constRxLimit; r ++ ) {

		CNicBuffer &Buff = m_pRxBuff[r];

		CBuffDesc  &Desc = m_pRxDesc[r];

		Buff.m_iIndex    = r;

		Buff.m_pDesc     = &Desc;

		Buff.m_dwDesc    = phal->VirtualToPhysical(DWORD(Buff.m_pDesc));

		Buff.m_pData     = m_pRxData + r * constBuffSize;

		Buff.m_dwData    = phal->VirtualToPhysical(DWORD(Buff.m_pData));

		Desc.m_BuffPtr   = Buff.m_dwData;

		Desc.m_BuffLen   = constBuffSize;

		Desc.m_Flags     = flagOwner;

		if( r < (constRxLimit - 1) ) {

			DWORD dwNext   = DWORD(&m_pRxDesc[r+1]); 

			Desc.m_NextPtr = phal->VirtualToPhysical(dwNext);
			}
		}

	m_uTxHead = 0;

	m_uTxTail = 0;

	m_uRxHead = 0;

	m_uRxTail = 0;

	m_uRxLast = constRxLimit - 1;

	m_uTxLast = NOTHING;
	}

void CEnet437::FreeBuffers(void)
{
	free(m_pTxDesc);

	free(m_pRxDesc);

	free(m_pTxData);

	free(m_pRxData);

	delete [] m_pTxBuff;

	delete [] m_pRxBuff;
	}

void CEnet437::StartRecv(CBuffDesc *pDesc)
{
	DWORD dwDesc = DWORD(pDesc);

	DWORD dwAddr = phal->VirtualToPhysical(dwDesc);
		
	RegN(StateRamRxHd, m_uChan) = dwAddr;
	}

BOOL CEnet437::ContinueRecv(CNicBuffer *pBuff)
{
	CNicBuffer *pLast = &m_pRxBuff[m_uRxLast];

	pBuff->m_pDesc->m_NextPtr = NULL; 

	pBuff->m_pDesc->m_BuffLen = constBuffSize;

	pBuff->m_pDesc->m_BuffOff = 0;
	
	pBuff->m_pDesc->m_Flags   = flagOwner;

	pLast->m_pDesc->m_NextPtr = pBuff->m_dwDesc;

	m_uRxLast  = (m_uRxLast + 1) % constRxLimit;

	if( !RegN(StateRamRxHd, m_uChan) ) {

		AfxTrace("enet437: recv stall recovery.\n");

		StartRecv(pBuff->m_pDesc);

		return TRUE;
		}

	return FALSE;
	}

void CEnet437::StartSend(CNicBuffer *pBuff)
{
	pBuff->m_pDesc->m_NextPtr   = NULL; 

	pBuff->m_pDesc->m_BuffOff   = 0;

	pBuff->m_pDesc->m_PacketLen = pBuff->m_pDesc->m_BuffLen;
	
	pBuff->m_pDesc->m_Flags     = flagSop | flagEop | flagOwner;

	Reg(DmaTxIntEnClr) = (1 << m_uChan);

	if( m_uTxLast != NOTHING ) {

		m_pTxBuff[m_uTxLast].m_pDesc->m_NextPtr = pBuff->m_dwDesc;
		}
	else {
		RegN(StateRamTxHd, m_uChan) = pBuff->m_dwDesc;
		}

	m_uTxLast = pBuff->m_iIndex;

	Reg(DmaTxIntEnSet) = (1 << m_uChan);
	}

// Mii Managment

WORD CEnet437::AppGetMii(BYTE bAddr)
{
	PhyPollEnable(false);

	m_uMiiState = miiAppGet;

	BYTE bGo    = 1;
	BYTE bWrite = 0;
	BYTE bPhy   = 0;

	Reg(MdioAccess0) = ( (bGo    << 31)
			   | (bWrite << 30)
			   | (bAddr  << 21)
			   | (bPhy   << 16) 
			   );

	if( !m_pMiiFlag->Wait(1000) ) {

		AfxTrace("PHY Timeout on GetMII\n");

		PhyPollEnable(true);

		return 0;
		}

	return LOWORD(Reg(MdioAccess0));
	}

BOOL CEnet437::AppPutMii(BYTE bAddr, WORD wData)
{
	PhyPollEnable(false);

	m_uMiiState = miiAppGet;

	BYTE bGo    = 1;
	BYTE bWrite = 1;
	BYTE bPhy   = 0;

	Reg(MdioAccess0) = ( (bGo    << 31)
			   | (bWrite << 30)
			   | (bAddr  << 21)
			   | (bPhy   << 16)
			   | (wData  <<  0)
			   );

	if( !m_pMiiFlag->Wait(1000) ) {

		AfxTrace("PHY Timeout on PutMII\n");

		PhyPollEnable(true);

		return false;
		}

	return true;
	}

void CEnet437::IntGetMii(BYTE bAddr)
{
	PhyPollEnable(false);

	m_uMiiState = miiIntGet;

	BYTE bGo    = 1;
	BYTE bWrite = 0;
	BYTE bPhy   = 0;

	Reg(MdioAccess0) = ( (bGo    << 31)
			   | (bWrite << 30)
			   | (bAddr  << 21)
			   | (bPhy   << 16) 
			   );
	}

void CEnet437::IntPutMii(BYTE bAddr, WORD wData)
{
	m_uMiiState = miiIntPut;

	BYTE bGo    = 1;
	BYTE bWrite = 1;
	BYTE bPhy   = 0;

	Reg(MdioAccess0) = ( (bGo    << 31)
			   | (bWrite << 30)
			   | (bAddr  << 21)
			   | (bPhy   << 16)
			   | (wData  <<  0)
			   );
	}

// Diagnostics

bool CEnet437::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "nic0");

		pDiag->RegisterCommand(m_uProv, 1, "status");

		pDiag->Release();

		return true;
		}

	return false;
	}

bool CEnet437::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return true;
		}

	return false;
	}

UINT CEnet437::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		pOut->AddProp("State", "%u", m_uNicState);

		if( m_uNicState ) {

			NICDIAG Diag;

			GetCounters(Diag);

			pOut->AddProp("AllowFull", "%u", m_fAllowFull);
			pOut->AddProp("AllowFast", "%u", m_fAllowFast);
			pOut->AddProp("UsingFull", "%u", m_fUsingFull);
			pOut->AddProp("UsingFast", "%u", m_fUsingFast);
			pOut->AddProp("RxCount",   "%u", Diag.m_RxCount);
			pOut->AddProp("RxDisc",    "%u", Diag.m_RxDisc);
			pOut->AddProp("RxOver",    "%u", Diag.m_RxOver);
			pOut->AddProp("TxCount",   "%u", Diag.m_TxCount);
			pOut->AddProp("TxDisc",    "%u", Diag.m_TxDisc);
			pOut->AddProp("TxFail",    "%u", Diag.m_TxFail);
			}

		pOut->EndPropList();

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

// Debug

void CEnet437::DumpNetStats(void)
{
	#if defined(_XDEBUG)

	AfxTrace("NetRxGood      = %d\n", Reg(NetRxGood     ));
	AfxTrace("NetRxBroad     = %d\n", Reg(NetRxBroad    ));
	AfxTrace("NetRxMulti     = %d\n", Reg(NetRxMulti    ));
	AfxTrace("NetRxPause     = %d\n", Reg(NetRxPause    ));
	AfxTrace("NetRxCrc       = %d\n", Reg(NetRxCrc      ));
	AfxTrace("NetRxAlign     = %d\n", Reg(NetRxAlign    ));
	AfxTrace("NetRxOver      = %d\n", Reg(NetRxOver     ));
	AfxTrace("NetRxJabber    = %d\n", Reg(NetRxJabber   ));
	AfxTrace("NetRxShort     = %d\n", Reg(NetRxShort    ));
	AfxTrace("NetRxFrag      = %d\n", Reg(NetRxFrag     ));
	AfxTrace("NetRxOctets    = %d\n", Reg(NetRxOctets   ));
	AfxTrace("NetTxGood      = %d\n", Reg(NetTxGood     ));
	AfxTrace("NetTxBroad     = %d\n", Reg(NetTxBroad    ));
	AfxTrace("NetTxMulti     = %d\n", Reg(NetTxMulti    ));
	AfxTrace("NetTxPause     = %d\n", Reg(NetTxPause    ));
	AfxTrace("NetTxDefer     = %d\n", Reg(NetTxDefer    ));
	AfxTrace("NetCollision   = %d\n", Reg(NetCollisions ));
	AfxTrace("NetTxColSingle = %d\n", Reg(NetTxColSingle));
	AfxTrace("NetTxColMulte  = %d\n", Reg(NetTxColMulte ));
	AfxTrace("NetExcesCol    = %d\n", Reg(NetExcesCol   ));
	AfxTrace("NetLateCol     = %d\n", Reg(NetLateCol    ));
	AfxTrace("NetTxUnder     = %d\n", Reg(NetTxUnder    ));
	AfxTrace("NetCarrier     = %d\n", Reg(NetCarrier    ));
	AfxTrace("NetTxOctets    = %d\n", Reg(NetTxOctets   ));
	AfxTrace("NetRxTx        = %d\n", Reg(NetRxTx       ));
	AfxTrace("NetNetOctets   = %d\n", Reg(NetNetOctets  ));
	AfxTrace("NetRxSofOver   = %d\n", Reg(NetRxSofOver  ));
	AfxTrace("NetRxMofOver   = %d\n", Reg(NetRxMofOver  ));
	AfxTrace("NetRxDmaOver   = %d\n", Reg(NetRxDmaOver  ));
	
	#endif
	}

void CEnet437::DumpDesc(CBuffDesc const &Desc)
{
	#if defined(_XDEBUG)

	AfxTrace("Descriptor @0x%8.8X\n", &Desc);

	AfxTrace("NextPtr   = 0x%8.8X\n", Desc.m_NextPtr  );
	AfxTrace("BuffPtr   = 0x%8.8X\n", Desc.m_BuffPtr  );
	AfxTrace("BuffLen   = 0x%4.4X\n", Desc.m_BuffLen  );
	AfxTrace("BuffOff   = 0x%4.4X\n", Desc.m_BuffOff  );
	AfxTrace("PackLen   = 0x%4.4X\n", Desc.m_PacketLen);
	AfxTrace("Flags     = 0x%4.4X\n", Desc.m_Flags    );

	#endif
	}

void CEnet437::DumpAle(void)
{
	#if defined(_XDEBUG)

	AfxTrace("Address Lookup Table\n");

	for( UINT i = 0; i < 1024; i ++ ) {

		Reg(AleTableCtrl) = i;

		DWORD d[3];

		d[0] = Reg(AleTable0);
		d[1] = Reg(AleTable1);
		d[2] = Reg(AleTable2);

		#define BitN(b, m)	((d[b/32] >> (b % 32)) & m)

		UINT  tp = BitN(60, 3);

		if( tp ) {

			AfxTrace( "ALE[%4d] : %8.8X %8.8X %8.8X : Type(%d) ", i, d[2], d[1], d[0], tp);

			if( tp == 1 ) {

				AfxTrace("MAC: %2.2X-", BitN(40, 0xFF));
				AfxTrace("%2.2X-",      BitN(32, 0xFF));
				AfxTrace("%2.2X-",      BitN(24, 0xFF));
				AfxTrace("%2.2X-",      BitN(16, 0xFF));
				AfxTrace("%2.2X-",      BitN( 8, 0xFF));
				AfxTrace("%2.2X ",      BitN( 0, 0xFF));

				if( BitN(40, 1) ) {

					AfxTrace("Multi=1 ");
					AfxTrace("Port Mask:%d: ", BitN(66, 0x07));
					AfxTrace("Super:%d ",      BitN(65, 0x01));
					AfxTrace("Forward:%d ",    BitN(62, 0x03));
					}
				else {
					AfxTrace("Multi=0 ");
					AfxTrace("DLR:%d ",     BitN(68, 0x01));
					AfxTrace("Port:%d: ",   BitN(66, 0x03));
					AfxTrace("Block:%d ",   BitN(65, 0x01));
					}
				}

			AfxTrace("\n");
			}
		}

	#endif
	}

// End of File
