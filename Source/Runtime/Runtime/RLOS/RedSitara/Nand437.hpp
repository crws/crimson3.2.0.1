
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Nand437_HPP
	
#define	INCLUDE_AM437_Nand437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Coupled Devices
//

class CClock437;
class CGpmc437;
class CElm437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 Nand Flash Manager
//

class CNand437 : public INandMemory, public IEventSink
{	
	public:
		// Constructor
		CNand437(CClock437 *pClock, CGpmc437 *pGpmc, CElm437 *pElm);

		// Destructor
		~CNand437(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// INandMemory
		BOOL METHOD GetGeometry(CNandGeometry &Geom);
		BOOL METHOD GetUniqueId(UINT uChip, PBYTE pData);
		BOOL METHOD IsBlockBad(UINT uChip, UINT uBlock);
		BOOL METHOD MarkBlockBad(UINT uChip, UINT uBlock);
		BOOL METHOD EraseBlock(UINT uChip, UINT uBlock);
		BOOL METHOD WriteSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PCBYTE pData);
		BOOL METHOD ReadSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PBYTE pData);
		BOOL METHOD WritePage(UINT uChip, UINT uBlock, UINT uPage, PCBYTE pData);
		BOOL METHOD ReadPage(UINT uChip, UINT uBlock, UINT uPage, PBYTE pData);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Geometry
		struct CGeometry
		{
			UINT	m_uPageSize;
			UINT	m_uBlockSize;
			UINT	m_uPlaneSize;
			UINT	m_uDeviceSize;
			UINT	m_uSpare;
			UINT    m_uChips;
			};

		// Commands
		enum
		{
			cmdReset	= 0xFF,
			cmdReadId	= 0x90,
			cmdReadUnique	= 0xED,
			cmdReadStatus	= 0x70,
			cmdReadMode	= 0x00,
			cmdReadBeg	= 0x00,
			cmdReadEnd	= 0x30,
			cmdReadRandBeg	= 0x05,
			cmdReadRandEnd	= 0xE0,
			cmdEraseBeg	= 0x60,
			cmdEraseEnd	= 0xD0,
			cmdProgBeg	= 0x80,
			cmdProgEnd	= 0x10,
			cmdProgRand	= 0x85,
			cmdGetFeature	= 0xEE,
			cmdSetFeature	= 0xEF,
			cmdLockAll	= 0x2A,
			cmdUnlockLo	= 0x23,
			cmdUnlockHi	= 0x24,
			cmdLockTight	= 0x2C,
			cmdGetLock	= 0x7A,
			};

		// ECC Modes
		enum
		{
			eccNone,
			eccBch8,
			eccBch16,
			};

		// Constants
		enum
		{
			constBchMax	 = 26,
			constBchOffset	 = 2,
			};

		// Static Data
		static const BYTE m_bEccNull[constBchMax];
		static const BYTE m_bEccGood[constBchMax];

		// Data Members
		ULONG           m_uRefs;
		UINT		m_uGpmcLine;
		UINT		m_uElmLine;
		UINT		m_uChip;
		UINT		m_uClock;
		IEvent	      * m_pDone;
		IMutex	      * m_pLock;
		CGeometry	m_Geom;
		CGpmc437      * m_pGpmc;
		CElm437	      * m_pElm;
		UINT	        m_uPolyNum;
		BYTE		m_bStatus;
		BYTE		m_bEcc[256];
		UINT		m_uBchSize;
		UINT		m_uBchSpare;
		UINT		m_uBchBytes;

		// Implementation
		void Lock(void);
		void Free(void);
		void EnableEvents(void);
		void DisableEvents(void);
		bool WaitDone(UINT uWait);
		void UnlockBlocks(void);
		void LockBlocks(void);

		// Nand 
		void PutNandCmd(BYTE bCmd);
		void PutNandAddr(BYTE bAddr);
		void PutNandAddr(WORD wAddr);
		void PutNandAddr(DWORD dwAddr);
		BYTE GetNandData(void);
		void PutNandData(BYTE bData);
		BYTE GetNandStatus(void);
		void ReadNandData(PBYTE pDatat, UINT uCount);
		void SendNandData(PCBYTE pData, UINT uCount);
		void Delay(UINT uDelay);

		// Bch
		void BchConfigure(UINT uMode);
		void BchEnable(void);
		void BchDisable(void);
		void BchClear(void);
		void BchSetRead(void);
		void BchSetWrite(void);
		bool BchCorrect(PBYTE pData, PBYTE pEcc);
		void BchCalculate(PBYTE pEcc);
	};

// End of File

#endif
