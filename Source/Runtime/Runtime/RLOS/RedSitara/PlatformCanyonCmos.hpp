
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PlatformCanyonCmos_HPP

#define INCLUDE_PlatformCanyonCmos_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPwm437;
class CPru437;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PlatformCanyon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Canyon CMOS HMI 
//

class CPlatformCanyonCmos : public CPlatformCanyon
{
	public:
		// Constructor
		CPlatformCanyonCmos(UINT uModel);

		// Destructor
		~CPlatformCanyonCmos(void);

		// IDevice
		BOOL METHOD Open(void);

		// ILeds
		void METHOD SetLed(UINT uLed, UINT uState);

		// IInputSwitch
		UINT METHOD GetSwitches(void);
		UINT METHOD GetSwitch(UINT uSwitch);

	protected:
		// Data Members
		CPwm437 * m_pPwm[5];
		CPru437 * m_pPru;

		// IPortSwitch
		BOOL METHOD SetFull(UINT uUnit, BOOL fFull);

		// IUsbSystemPortMapper
		UINT METHOD GetPortType(UsbTreePath const &Path);

		// Inititialisation
		void InitClocks(void);
		void InitMux(void);
		void InitGpio(void);
		void InitMisc(void);
		void InitPru(void);
	};

// End of File

#endif
