
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Beeper437_HPP
	
#define	INCLUDE_AM437_Beeper437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPwm437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 Beeper
//

class CBeeper437 : public IBeeper, public IEventSink
{
	public:
		// Constructor
		CBeeper437(CPwm437 *pPwm, UINT uChan);

		// Destructor
		~CBeeper437(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IBeeper
		void METHOD Beep(UINT uBeep);
		void METHOD Beep(UINT uFreq, UINT uTime);
		void METHOD BeepOff(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Static Data
		static UINT m_Table[];

		// Data Members
		ULONG     m_uRefs;
		CPwm437 * m_pPwm;
		UINT      m_uChan;
		ITimer  * m_pTimer;
		UINT      m_uTime;
		UINT      m_uSave;
	};

// End of File

#endif
