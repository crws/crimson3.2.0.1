
#include "Intern.hpp"

#include "Watchdog437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 Watchdog Module
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CWatchdog437::CWatchdog437(void)
{
	m_pBase = PVDWORD(ADDR_WDT1);
	
	Reset();
	}

// Operations

void CWatchdog437::Enable(UINT uTime)
{
	Disable();

	Reg(LOAD)    = 0xFFFFFFFF - (uTime * 32768) / (2 * 1000) + 1;

	Reg(DELAY)   = 0x00000000;

	Reg(CONTROL) = Bit(5); 

	Enable();
	}

void CWatchdog437::Disable(void)
{
	Reg(STARTSTOP) = 0x0000AAAA;

	Wait();
	
	Reg(STARTSTOP) = 0x00005555;

	Wait();
	}

void CWatchdog437::Enable(void)
{
	Reg(STARTSTOP) = 0x0000BBBB;

	Wait();
	
	Reg(STARTSTOP) = 0x00004444;

	Wait();
	}

void CWatchdog437::Kick(void)
{
	Reg(TRIGGER) = ~Reg(TRIGGER);
	}

// Implementation 

void CWatchdog437::Reset(void)
{
	Reg(SYSTEM) |= Bit(1);

	while( Reg(SYSTEM) & Bit(1) ); 
	}

void CWatchdog437::Wait(void)
{
	while( Reg(POST) );
	}

// End of File
