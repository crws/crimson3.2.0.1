
#include "Intern.hpp"

#include "UsbDev437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Hardware Drivers
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define RegWrap(x)	(m_pBaseWrap[reg##x])

#define RegCore(x)	(m_pBaseCore[reg##x])

#define RegPhy(x)	(m_pBasePhy [reg##x])

//////////////////////////////////////////////////////////////////////////
//
// AM437 Usb Device
//

// Instantiator

IUsbDriver * Create_UsbFunc437(UINT iIndex)
{
	CUsbDev437 *p = New CUsbDev437(iIndex);

	return p;
	}

// Constructor

CUsbDev437::CUsbDev437(UINT iIndex) : CUsbBase437(iIndex)
{
	m_pCtrl      = Create_AutoEvent();

	m_pDriver    = NULL;
		     
	m_pSetup     = NULL;
		     
	m_uCtrlState = stateInit;
	
	m_pDataBuf   = NULL;
		     
	m_pTrList    = NULL;
		     
	m_pTbList    = NULL;
		     
	m_pEpList    = NULL;
		     
	m_nIntEnable = 0;

	MakeEventBuffer();

	MakeEndpoints();

	MakeTransfers();

	MakeDataBuffers();

	SetMode(modeDevice);

	piob->RegisterSingleton("usbfunchw", 0, (IUsbFuncHardwareDriver *) this);
	}

// Destructor

CUsbDev437::~CUsbDev437(void)
{
	KillEventBuffer();

	KillTransfers();

	KillEndpoints();

	KillDataBuffers();

	m_pCtrl->Release();

	AfxRelease(m_pDriver);
	}

// IUnknown

HRESULT CUsbDev437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbFuncHardwareDriver);

	StdQueryInterface(IUsbFuncHardwareDriver);

	StdQueryInterface(IUsbDriver);

	return E_NOINTERFACE;
	}

ULONG CUsbDev437::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbDev437::Release(void)
{
	StdRelease();
	}

// IUsbDriver

BOOL CUsbDev437::Bind(IUsbEvents *pDriver)
{
	if( pDriver ) {

		AfxRelease(m_pDriver);

		m_pDriver = NULL;

		pDriver->QueryInterface(AfxAeonIID(IUsbFuncHardwareEvents), (void **) &m_pDriver);

		if( m_pDriver ) {
		
			m_pDriver->OnBind(this);

			return true;
			}
		}

	return false;
	}

BOOL CUsbDev437::Bind(IUsbDriver *pDriver)
{
	if( pDriver ) {

		AfxRelease(m_pDriver);

		m_pDriver = NULL;

		pDriver->QueryInterface(AfxAeonIID(IUsbFuncHardwareEvents), (void **) &m_pDriver);

		if( m_pDriver ) {
		
			m_pDriver->OnBind(this);

			return true;
			}
		}

	return false;
	}

BOOL CUsbDev437::Init(void)
{
	if( m_pDriver ) {

		StopController();

		ResetCore();

		InitEventBuffer();

		InitTransfers();

		InitEndpoints();

		InitController();

		InitCtrlEndpt();

		m_pDriver->OnInit();

		return true;
		}
	
	return false;
	}

BOOL CUsbDev437::Start(void)
{
	if( m_pDriver ) {

		EnableEvents();

		StartCtrlEndpt();

		m_pDriver->OnStart();

		return true;
		}
	
	return false;
	}

BOOL CUsbDev437::Stop(void)
{
	if( m_pDriver ) {

		m_pDriver->OnStop();

		AbortTransfers();

		StopCtrlEndpt();
		
		StopController();

		DisableEvents();
	
		return true;
		}

	return false;
	}

// IUsbFuncHardwareDriver

BOOL CUsbDev437::GetHiSpeedCapable(void)
{
	return true;
	}

BOOL CUsbDev437::GetHiSpeedActual(void)
{
	return (RegCore(DSTS) & 0x7) == 0x00;
	}

BOOL CUsbDev437::SetConfig(UINT uConfig) 
{
	OnConfig(uConfig);

	return true;
	}

BOOL CUsbDev437::SetAddress(UINT uAddr)
{
	RegCore(DCFG) &= ~(0x7F << 3);

	RegCore(DCFG) |= (uAddr << 3);
	
	return true;
	}

UINT CUsbDev437::GetAddress(void)
{
	return (RegCore(DCFG) >> 3) & 0x7F;
	}

BOOL CUsbDev437::InitBulk(UINT iEndpt, BOOL fIn, UINT uMax)
{
	UINT iEndptPhy = MapEndpoint(iEndpt, fIn);

	if( iEndptPhy != NOTHING ) {

		CEndpoint &Ep = m_pEpList[iEndptPhy];

		if( Ep.m_uType == typeInvalid ) {

			Ep.m_uType    = typeBulk;
			Ep.m_uDirIn   = fIn;
			Ep.m_uState   = stateInit;
			Ep.m_uMaxPack = uMax;
			Ep.m_uFifo    = 0;
			Ep.m_pHead    = NULL;
			Ep.m_pTail    = NULL;
			Ep.m_uNum     = iEndpt;

			return true;
			}
		}
	
	return false;
	}

BOOL CUsbDev437::KillBulk(UINT iEndpt)
{
	iEndpt = MapEndpoint(iEndpt, false);

	if( iEndpt != NOTHING ) {

		CEndpoint &Ep = m_pEpList[iEndpt];

		AbortTransfer(&Ep);

		FreeTransfers(Ep.m_uIndex);

		RegCore(DALEPENA) &= ~(0x3 << (Ep.m_uNum * 2));

		Ep.m_uType = typeInvalid;

		return true;
		}
	
	return false;
	}

BOOL CUsbDev437::Transfer(UsbIor &Req)
{
	/*AfxTrace("CUsbDev437::Transfer(Ep=%d, DirIn=%d, Count=%d)\n", Req.m_iEndpt, Req.m_uFlags & Req.flagIn, Req.m_uCount);*/

	bool fDirIn = Req.m_uFlags & Req.flagIn;

	UINT iEndpt = MapEndpoint(Req.m_iEndpt, fDirIn);

	if( iEndpt != NOTHING ) {

		CEndpoint &Ep = m_pEpList[iEndpt];

		if( Ep.m_uType == typeInvalid ) {

			AfxTrace("CUsbDev437::Transfer - Endpoint(%d) Not Active.\n", Req.m_iEndpt);
			
			return false;
			}

		if( Ep.m_uState == stateHalted || Ep.m_uState == stateBusy ) {

			AfxTrace("CUsbDev437::Transfer - Endpoint(%d) Invalid State(%d).\n", Req.m_iEndpt, Ep.m_uState);
			
			return false;
			}

		if( fDirIn != Ep.m_uDirIn ) {

			AfxTrace("CUsbDev437::Transfer - Endpoint(%d) Direction Missmatch.\n", Req.m_iEndpt);

			return false;
			}

		if( Req.m_uCount > constDataLimit ) {

			AfxTrace("CUsbDev437::Transfer - Endpoint(%d) Data Limit Exceeded.\n", Req.m_iEndpt);

			return false;
			}

		UINT i = AllocTransfer(iEndpt);

		if( i != NOTHING ) {

			CTransfer &Tr = m_pTrList[i];

			CEndptTrb &Tb = *Tr.m_pTrb;

			Tb.m_dwBufPtrHi  = 0;
			Tb.m_dwBufPtrLo  = Tr.m_uDataPhy;
			Tb.m_dwBufSize   = Req.m_uCount;
			Tb.m_dwHwOwner   = true;
			Tb.m_dwLast	 = true;
			Tb.m_dwChain	 = false;
			Tb.m_dwContShort = false;
			Tb.m_dwIoc	 = false;
			Tr.m_pUrb        = (Req.m_uFlags & Req.flagNotify) ? &Req : NULL;
			Tr.m_uCount      = Req.m_uCount;

			if( fDirIn ) {

				ArmMemCpy(PBYTE(Tr.m_pDataBuf), PBYTE(Req.m_pData), Req.m_uCount);
				}
			
			if( Ep.m_uType == typeCtrl ) {

				m_pCtrl->Wait(FOREVER);
				
				switch( m_uCtrlState ) {

					case setupInit:
					case setupSetupWait:
					case setupSetupRecv:
					case setupDataOut:

						AfxTrace("CUsbDev437::Transfer - Setup Bad State.\n");

						FreeTransfer(i);

						return false;

					case setupDataIn:

						Tb.m_dwCtrl = trbCtrlData;

						break;

					case setupStatus2:

						Tb.m_dwCtrl = trbCtrlStatus2;

						break;

					case setupStatus3:

						Tb.m_dwCtrl = trbCtrlStatus3;

						break;
					}
		
				if( SendStartTransfer(&Tr) ) {
				
					return true;
					}
				}

			if( Ep.m_uType == typeBulk ) {

				Tb.m_dwCtrl = trbNormal;

				if( !fDirIn ) {

					Tb.m_dwBufSize = ((Req.m_uCount + Ep.m_uMaxPack - 1) / Ep.m_uMaxPack) * Ep.m_uMaxPack;

					Tr.m_uCount    = Tb.m_dwBufSize;
					}
				
				EnableInterrupts(false);

				if( SendStartTransfer(&Tr) ) {

					Ep.m_uState = stateBusy;

					EnableInterrupts(true);
					
					return true;
					}

				EnableInterrupts(true);
				}
							
			FreeTransfer(i);
			}
		}
	
	return false;
	}

BOOL CUsbDev437::Abort(UsbIor &Req)
{
	UINT iEndpt = MapEndpoint(Req.m_iEndpt, false);

	if( iEndpt != NOTHING ) {

		CEndpoint &Ep = m_pEpList[iEndpt];

		CTransfer *pTr = Ep.m_pHead;

		while( pTr ) {

			if( pTr->m_pUrb == &Req ) {

				AbortTransfer(pTr);

				FreeTransfer(pTr);

				if( Ep.m_pHead == NULL ) {

					Ep.m_uState = stateReady;
					}

				return true;
				}
			
			pTr = pTr->m_pNext;
			}	
		}
	
	return false;
	}

BOOL CUsbDev437::SetStall(UINT iEndpt, BOOL fSet)
{
	iEndpt = MapEndpoint(iEndpt, false);

	if( iEndpt != NOTHING ) {

		CEndpoint &Ep = m_pEpList[iEndpt];

		if( fSet ) {

			SendSetStall(iEndpt);

			if( iEndpt == 0 ) { 

				StartSetup();
				
				return true;
				}

			Ep.m_uState = stateStall;
			}
		else {
			SendClearStall(iEndpt);

			if( iEndpt ) {

				Ep.m_uState = stateReady;
				}
			}

		return true;
		}
	
	return false;
	}

BOOL CUsbDev437::GetStall(UINT iEndpt)
{
	iEndpt = MapEndpoint(iEndpt, false);

	if( iEndpt != NOTHING ) {

		CEndpoint &Ep = m_pEpList[iEndpt];

		return Ep.m_uState == stateStall;
		}
	
	return false;
	}

BOOL CUsbDev437::ResetDataToggle(UINT iEndpt)
{
	iEndpt = MapEndpoint(iEndpt, false);

	if( iEndpt != NOTHING ) {

		CEndpoint &Ep = m_pEpList[iEndpt];

		return true;
		}
	
	return false;
	}

// IEventSink

void CUsbDev437::OnEvent(UINT uLine, UINT uParam)
{
	if( uLine == m_uLineCore ) {

		OnEventCore();
		}

	if( uLine == m_uLineMisc ) {

		OnEventMisc();
		}
	}

// Event Handlers

void CUsbDev437::OnEventMisc(void)
{
	for(;;) {

		DWORD Data = RegWrap(IRQSTATUSMISC);

		if( Data ) {

			RegWrap(IRQSTATUSMISC) = Data;

			continue;
			}

		break;
		}
	}

void CUsbDev437::OnEventCore(void)
{
	for(;;) {

		DWORD Data = RegWrap(IRQSTATUSMAIN);

		if( Data ) {
				
			DWORD Count = RegCore(GEVNTCOUNT) / sizeof(DWORD);

			while( Count-- ) {

				CEvent &Event = m_pEventBuff[m_iEventCurr];

				if( Event.m_CoreEvent.m_dwEvent == 1 ) {
				
					OnEventCore(Event.m_CoreEvent);
					}

				if( Event.m_EndptEvent.m_dwEvent == 0 ) {

					OnEventCore(Event.m_EndptEvent);
					}

				m_iEventCurr = (m_iEventCurr + 1) % constEventLimit; 
			
				RegCore(GEVNTCOUNT) = sizeof(DWORD);
				}

			RegWrap(IRQSTATUSMAIN) = Data;

			continue;
			}

		break;
		}
	}

void CUsbDev437::OnEventCore(CCoreEvent const &Event)
{
	if( Event.m_dwEvent == 1 && Event.m_dwDevEvent == 0 ) {

		switch( Event.m_dwType ) {

			case devtReset:

				OnReset();

				break;

			case devtConnect:

				OnConnect();

				break;

			case devtDisconnect:
				
				OnDisconnect();

				break;
			}
		}
	}

void CUsbDev437::OnEventCore(CEndptEvent const &Event)
{
	if( Event.m_dwEvent == 0 ) {

		switch( Event.m_dwType ) {
			
			case epevtNotReady:

				OnNotReady(Event);

				break;

			case epevtDone:	

				OnTransferDone(Event);

				break;
			}
		}
	}

void CUsbDev437::OnReset(void)
{
	m_pDriver->OnReset();

	AbortTransfers();

	ClearStalled();

	RegCore(DCFG) &= ~(0x7F << 3);
	}

void CUsbDev437::OnConnect(void)
{
	SendEndptConfig(m_pEpList[0], configModify);

	SendEndptConfig(m_pEpList[0], configModify);
	}

void CUsbDev437::OnDisconnect(void)
{
	m_pDriver->OnDisconnect();
	}

void CUsbDev437::OnNotReady(CEndptEvent const &Event)
{
	if( Event.m_dwEpNum == 0 || Event.m_dwEpNum == 1 ) {

		switch( m_uCtrlState ) {

			case setupInit:
			case setupSetupWait:

				AfxTrace("CUsbDev437 - Host control endpoint sequence error\n");

				SetStall(0, true);

				return;

			case setupSetupRecv:
				
				if( (Event.m_dwStatus & 0x3) == epevsDataReq ) {

					m_uCtrlState = Event.m_dwEpNum == 0 ? setupDataOut : setupDataIn;

					m_pCtrl->Set();
					
					return;
					}
				
				if( (Event.m_dwStatus & 0x3) == epevsStatusReq ) {

					m_uCtrlState = setupStatus2;

					m_pCtrl->Set();

					return;
					}

				break;

			case setupDataOut:

				if( Event.m_dwEpNum == 1 ) {

					AfxTrace("CUsbDev43 - Control transfer direction missmatch.\n");

					AbortTransfer(&m_pEpList[0]);

					SetStall(0, true);
					
					return;
					}

				break;

			case setupDataIn:

				if( Event.m_dwEpNum == 0 ) {

					AfxTrace("CUsbDev43 - Control transfer direction missmatch.\n");
					
					AbortTransfer(&m_pEpList[1]);

					SetStall(0, true);

					return;
					}

				break;

			case setupStatus2:
			case setupStatus3:

				m_pCtrl->Set();

				break;
			}

		return;
		}
	}

void CUsbDev437::OnTransferDone(CEndptEvent const &Event)
{
	CEndpoint *pEp = &m_pEpList[Event.m_dwEpNum];

	CTransfer *pTr = pEp->m_pHead;

	if( Event.m_dwEpNum == 0 || Event.m_dwEpNum == 1 ) {
		
		OnSetup(pTr);
		}
	else {
		OnData(pTr);
		}
	}

void CUsbDev437::OnSetup(CTransfer *pTr)
{
	FreeTransfer(pTr);

	switch( m_uCtrlState ) {

		case setupSetupWait:

			m_uCtrlState = setupSetupRecv;

			m_pDriver->OnSetup(m_pSetup);

			break;

		case setupDataOut:
		case setupDataIn:

			m_uCtrlState = setupStatus3;

			break;

		case setupStatus2:
		case setupStatus3:

			m_uCtrlState = setupDone;

			StartSetup();

			break;
		}
	}

void CUsbDev437::OnData(CTransfer *pTr)
{
	CEndpoint *pEp = &m_pEpList[pTr->m_iEndpt];

	CEndptTrb *pTb = pTr->m_pTrb;

	pEp->m_uState = stateReady;

	if( pTr->m_pUrb ) {

		if( pTb->m_dwStatus == 0 ) {

			MakeMin(pTr->m_pUrb->m_uCount, pTr->m_uCount - pTb->m_dwBufSize);

			pTr->m_pUrb->m_uStatus = UsbIor::statPassed;

			if( !pEp->m_uDirIn ) {

				ArmMemCpy(PBYTE(pTr->m_pUrb->m_pData), PBYTE(pTr->m_pDataBuf), pTr->m_pUrb->m_uCount);
				}
			}
		else {
			pTr->m_pUrb->m_uStatus = UsbIor::statFailed;
			}

		FreeTransfer(pTr);

		m_pDriver->OnTransfer(*pTr->m_pUrb);

		return;
		}

	FreeTransfer(pTr);
	}

void CUsbDev437::OnConfig(UINT uConfig)
{
	RegCore(DALEPENA) = 0x3;

	AbortTransfers();

	SendEndptConfig(m_pEpList[1], configInit);

	SendStartConfig(2);

	DWORD dwEnable = 0;

	UINT  uFifoNum = 1;
	
	for( UINT i = 0; i < constEndptLimit; i ++ ) {

		CEndpoint &Ep = m_pEpList[i];

		if( Ep.m_uType != typeInvalid && Ep.m_uType != typeCtrl ) {

			if( Ep.m_uDirIn ) {

				Ep.m_uFifo = uFifoNum++;

				dwEnable |= Bit(Ep.m_uNum * 2 + 1);
				}
			else {
				dwEnable |= Bit(Ep.m_uNum * 2 + 0);
				}
			
			SendEndptConfig(Ep, configInit);
			
			SendEndptTransferConfig(Ep);

			Ep.m_uState = stateReady;
			}
		}

	RegCore(DALEPENA) |= dwEnable;
	}

// Core

void CUsbDev437::ResetCore(void)
{
	RegCore(DCTL) |= Bit(30);

	while( RegCore(DCTL) & Bit(30) );
	}

void CUsbDev437::StopController(void)
{
	RegCore(DCTL) &= ~Bit(31);

	while( !(RegCore(DSTS) & Bit(22)) );
	}

void CUsbDev437::InitController(void)
{
	RegWrap(UTMIOTGSTATUS)	  = 0x00000018;
				  
	RegCore(GUSB2PHYCFG)     &= ~(Bit(31) | Bit(8) | Bit(6));
				  
	RegCore(DCFG)		  = (4 << 17);
		  
	RegCore(GEVNTADRLO)	  = phal->VirtualToPhysical(DWORD(m_pEventBuff));
				  
	RegCore(GEVNTADRHI)	  = 0;
				  
	RegCore(GEVNTSIZ)	  = constEventLimit * sizeof(CEvent);
				  
	RegCore(GEVNTCOUNT)	  = 0;
				  
	RegCore(DEVTEN)		  = eventConnect | eventReset | eventDisconnect;

	RegWrap(IRQENABLESETMAIN) = 1;

	RegWrap(IRQENABLESETMISC) = 0; 

	m_nIntEnable              = 1;

	SendStartConfig(0);
	}

void CUsbDev437::InitCtrlEndpt(void)
{
	CEndpoint &Ep0 = m_pEpList[0];
	
	CEndpoint &Ep1 = m_pEpList[1];

	Ep0.m_uType    = typeCtrl;
	Ep0.m_uState   = stateInit;
	Ep0.m_uMaxPack = 64;
	Ep0.m_uDirIn   = false;
	Ep0.m_uFifo    = 0;
	Ep1.m_uType    = typeCtrl;
	Ep1.m_uState   = stateInit;
	Ep1.m_uMaxPack = 64;
	Ep1.m_uDirIn   = true;
	Ep1.m_uFifo    = 0;

	SendEndptConfig(Ep0, configInit);

	SendEndptConfig(Ep1, configInit);

	SendEndptTransferConfig(Ep0);
	
	SendEndptTransferConfig(Ep1);
	}

void CUsbDev437::StartCtrlEndpt(void)
{
	StartSetup();

	RegCore(DALEPENA) = (Bit(0) | Bit(1));

	RegCore(DCTL)    |= Bit(31);
	}

void CUsbDev437::StopCtrlEndpt(void)
{
	RegCore(DCTL)    &= ~Bit(31);

	RegCore(DALEPENA) = 0;
	}

bool CUsbDev437::StartSetup(void)
{
	AllocTransfer(0);

	CEndpoint *pEp = &m_pEpList[0];
	
	CTransfer *pTr = pEp->m_pHead;

	CEndptTrb *pTb = pTr->m_pTrb;

	pTb->m_dwBufPtrHi  = 0;
	pTb->m_dwBufPtrLo  = m_dwSetup;
	pTb->m_dwBufSize   = 8;
	pTb->m_dwHwOwner   = true;
	pTb->m_dwLast	   = true;
	pTb->m_dwChain	   = false;
	pTb->m_dwContShort = false;
	pTb->m_dwCtrl      = trbCtrlSetup;
	pTb->m_dwIoc	   = false;

	EnableInterrupts(false);

	if( SendStartTransfer(pTr) ) {

		m_uCtrlState = setupSetupWait;

		EnableInterrupts(true);

		return true;
		}

	EnableInterrupts(true);

	AfxTrace("CUsbDev437::StartSetup Failed.\n");

	return false;
	}

void CUsbDev437::AbortTransfers(void)
{
	for( UINT i = 0; i < constEndptLimit; i ++ ) {

		CEndpoint &Ep = m_pEpList[i];

		if( Ep.m_uType != typeCtrl ) { 
		
			if( Ep.m_uState == stateBusy ) {

				AbortTransfer(&Ep);

				FreeTransfers(Ep.m_uIndex);

				Ep.m_uState = stateReady;
				}
			}
		}
	}

void CUsbDev437::AbortTransfer(CEndpoint *pEp)
{
	if( pEp->m_uType != typeInvalid ) {
		
		if( pEp->m_uState == stateBusy ) {

			CTransfer *pTr = pEp->m_pHead;

			while( pTr ) {

				AbortTransfer(pTr);

				pTr = pTr->m_pNext;
				}

			pEp->m_uState = stateReady;
			}
		}
	}

void CUsbDev437::AbortTransfer(CTransfer *pTr)
{
	SendEndTransfer(pTr);

	if( pTr->m_pUrb ) {

		UsbIor &Urb = *pTr->m_pUrb;
					
		pTr->m_pUrb = NULL;
					
		Urb.m_uFlags = UsbIor::statFailed;

		m_pDriver->OnTransfer(Urb);
		}
	}

void CUsbDev437::ClearStalled(void)
{
	for( UINT i = 0; i < constEndptLimit; i ++ ) {

		CEndpoint &Ep = m_pEpList[i];

		if( Ep.m_uType != typeInvalid ) {
		
			if( Ep.m_uState == stateStall ) {

				SendClearStall(i);
				
				Ep.m_uState = stateReady;
				}
			}
		}
	}

void CUsbDev437::EnableInterrupts(bool fEnable)
{
	if( fEnable ) {

		if( AtomicIncrement(&m_nIntEnable) >= 1 ) {

			RegWrap(IRQENABLESETMAIN) = 1;
			}
		}
	else {
		if( AtomicDecrement(&m_nIntEnable) <= 0 ) {

			RegWrap(IRQENABLECLRMAIN) = 1;
			}
		}
	}

// Core Commands

bool CUsbDev437::SendStartConfig(UINT uResId)
{
	CCommand Cmd;
	
	Cmd.m_bCmd     = cmdStartConfig;
	Cmd.m_dwResIdx = uResId;
	Cmd.m_dwParam0 = 0;
	Cmd.m_dwParam1 = 0;
	Cmd.m_dwParam2 = 0;

	return SendEndptCmd(0, Cmd, false) == 0;
	}

bool CUsbDev437::SendEndptConfig(CEndpoint &Ep, UINT uAction)
{
	CCommand Cmd;

	Cmd.m_bCmd     = cmdSetEpConfig;
	Cmd.m_dwResIdx = 0;
	Cmd.m_dwParam0 = (uAction << 30) | (Ep.m_uFifo << 17) | (Ep.m_uMaxPack << 3) | (Ep.m_uType << 1);
	Cmd.m_dwParam1 = (Ep.m_uNum << 26 ) | (Ep.m_uDirIn ? Bit(25) : 0) | Bit(10) | Bit(8);
	Cmd.m_dwParam2 = 0;

	return SendEndptCmd(Ep.m_uIndex, Cmd, false) == 0;
	}

bool CUsbDev437::SendEndptTransferConfig(CEndpoint &Ep)
{
	CCommand Cmd;

	Cmd.m_bCmd     = cmdSetEpTransferConfig;
	Cmd.m_dwResIdx = 0;
	Cmd.m_dwParam0 = 1;
	Cmd.m_dwParam1 = 0;
	Cmd.m_dwParam2 = 0;

	return SendEndptCmd(Ep.m_uIndex, Cmd, false) == 0;
	}

bool CUsbDev437::SendStartTransfer(CTransfer *pTr)
{
	CCommand Cmd;

	Cmd.m_bCmd     = cmdStartTransfer;
	Cmd.m_dwResIdx = 0;
	Cmd.m_dwParam0 = 0;
	Cmd.m_dwParam1 = pTr->m_uTrbPhy;
	Cmd.m_dwParam2 = 0;

	if( SendEndptCmd(pTr->m_iEndpt, Cmd, false) == 0 ) {
		
		pTr->m_wResIdx = Cmd.m_dwResIdx & 0x7F;
		
		return true;
		}

	return false;
	}

bool CUsbDev437::SendEndTransfer(CTransfer *pTr)
{
	CCommand Cmd;

	Cmd.m_bCmd     = cmdEndTransfer;
	Cmd.m_dwResIdx = pTr->m_wResIdx;
	Cmd.m_dwParam0 = 0;
	Cmd.m_dwParam1 = 0;
	Cmd.m_dwParam2 = 0;

	if( SendEndptCmd(pTr->m_iEndpt, Cmd, true) == 0 ) {

		pTr->m_wResIdx = 0;

		return true;
		}

	return false;
	}

bool CUsbDev437::SendSetStall(UINT iEndpt)
{
	CCommand Cmd;

	Cmd.m_bCmd     = cmdSetStall;
	Cmd.m_dwResIdx = 0;
	Cmd.m_dwParam0 = 0;
	Cmd.m_dwParam0 = 0;
	Cmd.m_dwParam0 = 0;

	return SendEndptCmd(iEndpt, Cmd, false) == 0;
	}

bool CUsbDev437::SendClearStall(UINT iEndpt)
{
	CCommand Cmd;

	Cmd.m_bCmd     = cmdClearStall;
	Cmd.m_dwResIdx = 0;
	Cmd.m_dwParam0 = 0;
	Cmd.m_dwParam0 = 0;
	Cmd.m_dwParam0 = 0;

	return SendEndptCmd(iEndpt, Cmd, false) == 0;
	}

UINT CUsbDev437::SendEndptCmd(UINT iEndpt, CCommand &Cmd, bool fForce)
{
	DWORD volatile &CmdReg = m_pBaseCore[regDEPCMD     + iEndpt * 4];
	DWORD volatile &Param0 = m_pBaseCore[regDEPCMDPAR0 + iEndpt * 4];
	DWORD volatile &Param1 = m_pBaseCore[regDEPCMDPAR1 + iEndpt * 4];
	DWORD volatile &Param2 = m_pBaseCore[regDEPCMDPAR2 + iEndpt * 4];

	if( iEndpt < 32 ) {

		CmdReg  = Cmd.m_bCmd;
		
		CmdReg |= (Cmd.m_dwResIdx << 16);

		CmdReg |= (fForce ? Bit(11) : 0);
		
		Param0  = Cmd.m_dwParam0;
	
		Param1  = Cmd.m_dwParam1;

		Param2  = Cmd.m_dwParam2;

		CmdReg |= Bit(10);

		for(;;) {

			DWORD Data = CmdReg;

			if( !(Data & Bit(10)) ) {

				Cmd.m_dwResIdx = (Data >> 16) &0xFFFF; 

				return (Data >> 12) & 0xF;
				}
			}
		}

	return NOTHING;
	}

bool CUsbDev437::SendDevCmd(BYTE bCmd)
{
	RegCore(DGCMD) = bCmd;

	for(;;) {

		DWORD Data = RegCore(DGCMD);

		if( !(Data & Bit(10)) ) {

			return !(Data & Bit(15));
			}
		}

	return false;
	}

// Buffers

void CUsbDev437::MakeEventBuffer(void)
{
	m_pEventBuff = (CEvent *) AllocNonCached(constEventLimit * sizeof(CEvent), 32);
	}

void CUsbDev437::InitEventBuffer(void)
{
	memset(m_pEventBuff, 0, sizeof(CEvent) * constEventLimit);

	m_iEventCurr = 0;
	}

void CUsbDev437::KillEventBuffer(void)
{
	if( m_pEventBuff ) {

		FreeNonCached(m_pEventBuff);

		m_pEventBuff = NULL;
		}
	}

void CUsbDev437::MakeDataBuffers(void)
{
	m_pSetup    = (PBYTE) AllocNonCached(8, 16);

	m_pDataBuf  = (PBYTE) AllocNonCached(constDataLimit * constTrbLimit, 16); 

	m_dwSetup   = phal->VirtualToPhysical(DWORD(m_pSetup));

	m_dwDataBuf = phal->VirtualToPhysical(DWORD(m_pDataBuf));

	memset(m_pSetup, 0, 8);
	}

void CUsbDev437::KillDataBuffers(void)
{
	if( m_pSetup ) {

		FreeNonCached(m_pSetup);

		FreeNonCached(m_pDataBuf);

		m_pSetup   = NULL;

		m_pDataBuf = NULL;
		}
	}

// Endpoints

void CUsbDev437::InitEndpoints(void)
{
	memset(m_pEpList, 0, constEndptLimit * sizeof(CEndpoint));

	for( UINT i = 0; i < constEndptLimit; i ++ ) {

		CEndpoint &Ep = m_pEpList[i];
		
		Ep.m_uIndex = i;

		Ep.m_uType  = typeInvalid;
		}
	}

void CUsbDev437::MakeEndpoints(void)
{
	m_pEpList = New CEndpoint[ constEndptLimit ];
	}

void CUsbDev437::KillEndpoints(void)
{
	if( m_pEpList ) {

		delete [] m_pEpList;

		m_pEpList = NULL;
		}
	}

UINT CUsbDev437::MapEndpoint(UINT iEndpt, bool fDirIn) const
{
	if( iEndpt == 0 ) {
		
		return fDirIn ? 1 : 0;
		}
	
	if( iEndpt < (constEndptLimit - 1) ) {
		
		return iEndpt + 1;
		}
	
	return NOTHING;
	}

// Transfers

void CUsbDev437::InitTransfers(void)
{
	memset(m_pTbList, 0, constTrbLimit * sizeof(CEndptTrb));

	memset(m_pTrList, 0, constTrbLimit * sizeof(CTransfer));
	
	m_iFreeHead = 0;

	m_iFreeTail = 0;

	for( UINT i = 0; i < constTrbLimit; i ++ ) {

		CTransfer &Tr  = m_pTrList[i];

		CEndptTrb &Tb  = m_pTbList[i];

		Tr.m_iIndex    = i;

		Tr.m_iEndpt    = NOTHING;
		
		Tr.m_pTrb      = &Tb;

		Tr.m_uTrbPhy   = phal->VirtualToPhysical(DWORD(Tr.m_pTrb));

		Tr.m_pDataBuf  = m_pDataBuf + i * constDataLimit;

		Tr.m_uDataPhy  = phal->VirtualToPhysical(DWORD(Tr.m_pDataBuf));

		m_pFreeList[i] = &Tr;
		}
	}

void CUsbDev437::MakeTransfers(void)
{
	m_pTrList = New CTransfer [ constTrbLimit ];

	m_pTbList = (CEndptTrb *) AllocNonCached(constTrbLimit * sizeof(CEndptTrb), 32);
	}

void CUsbDev437::KillTransfers(void)
{
	if( m_pTrList ) {

		FreeNonCached(m_pTbList);

		delete m_pTrList;
		
		m_pTrList = NULL;

		m_pTbList = NULL;
		}
	}

UINT CUsbDev437::AllocTransfer(UINT iEndpt)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	UINT uNext = (m_iFreeHead + 1) % elements(m_pFreeList);

	if( uNext != m_iFreeTail ) {

		CTransfer *pTr = m_pFreeList[m_iFreeHead];
	
		m_iFreeHead = uNext;

		Hal_LowerIrql(uSave);

		CEndpoint *pEp = &m_pEpList[iEndpt];

		pTr->m_iEndpt  = iEndpt;

		AfxListAppend(pEp->m_pHead, pEp->m_pTail, pTr, m_pNext, m_pPrev); 

		return pTr->m_iIndex;
		}

	Hal_LowerIrql(uSave);

	AfxTrace("CUsbHostEnhanced - Transfer Serialisation!\n");

	return NOTHING;
	}

void CUsbDev437::FreeTransfer(UINT iIndex)
{
	if( iIndex != NOTHING ) {

		CTransfer *pTr = &m_pTrList[iIndex];

		FreeTransfer(pTr);
		}
	}

void CUsbDev437::FreeTransfer(CTransfer *pTr)
{
	if( pTr->m_iEndpt != NOTHING ) {

		CEndpoint *pEp = &m_pEpList[pTr->m_iEndpt];

		pTr->m_iEndpt  = NOTHING;

		AfxListRemove(pEp->m_pHead, pEp->m_pTail, pTr, m_pNext, m_pPrev);
		}

	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);
		
	m_pFreeList[m_iFreeTail] = pTr;

	m_iFreeTail = (m_iFreeTail + 1) % elements(m_pFreeList);

	Hal_LowerIrql(uSave);
	}

void CUsbDev437::FreeTransfers(UINT iEndpt)
{
	CEndpoint &Ep = m_pEpList[iEndpt];

	CTransfer *pScan = Ep.m_pHead;

	while( pScan ) {

		CTransfer *pNext = pScan->m_pNext;

		FreeTransfer(pScan);
		
		pScan = pNext;
		}
	}

void CUsbDev437::FreeTransfers(void)
{
	for( UINT i = 0; i < constEndptLimit; i ++ ) {

		FreeTransfers(i);
		}
	}

// Diagnostics

void CUsbDev437::DumpTrb(CEndptTrb const &Trb)
{
	#if defined(_XDEBUG)

	AfxTrace("TRB\n");
	AfxTrace("BufPtrLo    = 0x%8.8X\n", Trb.m_dwBufPtrLo);
	AfxTrace("BufPtrHi    = 0x%8.8X\n", Trb.m_dwBufPtrHi);
	AfxTrace("BufSize     = 0x%1.1X\n", Trb.m_dwBufSize);
	AfxTrace("PacketCount = 0x%1.1X\n", Trb.m_dwPacketCount);
	AfxTrace("Status      = 0x%1.1X\n", Trb.m_dwStatus);
	AfxTrace("HwOwner     = 0x%1.1X\n", Trb.m_dwHwOwner);
	AfxTrace("Last        = 0x%1.1X\n", Trb.m_dwLast);
	AfxTrace("Chain	      = 0x%1.1X\n", Trb.m_dwChain);
	AfxTrace("ContShort   = 0x%1.1X\n", Trb.m_dwContShort);
	AfxTrace("Ctrl        = 0x%1.1X\n", Trb.m_dwCtrl);
	AfxTrace("Ios         = 0x%1.1X\n", Trb.m_dwIos);
	AfxTrace("Ioc	      = 0x%1.1X\n", Trb.m_dwIoc);
	AfxTrace("StreamId    = 0x%4.4X\n", Trb.m_dwStreamId);

	#endif
	}

// End of File
