
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Touch437_HPP
	
#define	INCLUDE_AM437_Touch437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../RedDev/TouchGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Touch Screen Controller
//

class CTouch437 : public CTouchGeneric, public IEventSink
{
	public:
		// Constructor
		CTouch437(BOOL fFlipX, BOOL fFlipY, UINT uMaxVar, UINT uMinVal);

		// Destructor
		~CTouch437(void);

		// IDevice
		BOOL METHOD Open(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Constants
		static UINT const timeInitial  = 500;  // Time before initial repeat
		static UINT const timeRepeat   = 100;  // Time between subsequent repeats
		static UINT const timeWait     = 50;   // Dead time after touch release 
		static UINT const minPressure  = 65;   // Minimum valid pressure reading
		static UINT const maxRelease   = 20;   // Maximum release pressure reading
		static UINT const minValid     = 5;    // Minimum number of valid sequences
		static UINT const minRelease   = 5;    // Minimum number of release sequences
		static UINT const maxVariation = 20;   // Maximum variation between readings
		static UINT const adcCharge    = 1200; // Effectively time betwen sequences
		static UINT const adcDelay     = 120;  // Time between setup and ADC sample
		static UINT const adcSample    = 1;    // Length of ADC sampling window

		// Interrupts
		enum {
			intPenAsync = Bit(0),
			intEndOfSeq = Bit(1),
			intFifo0Thr = Bit(2),
			intFifo1Thr = Bit(5),
			intPenUp    = Bit(9),
			intPenSync  = Bit(10),
			};

		// Touch States
		enum {
			stateWaitDown,
			stateConvert,
			stateWaitUp,
			};

		// Registers
		enum {
			regSYSCONFIG		= 0x0010 / sizeof(DWORD),
			regIRQSTS_RAW		= 0x0024 / sizeof(DWORD),
			regIRQSTS		= 0x0028 / sizeof(DWORD),
			regIRQEN_SET		= 0x002C / sizeof(DWORD),
			regIRQEN_CLR		= 0x0030 / sizeof(DWORD),
			regIRQWAKEUP		= 0x0034 / sizeof(DWORD),
			regDMAEN_SET		= 0x0038 / sizeof(DWORD),
			regDMAEN_CLR		= 0x003C / sizeof(DWORD),
			regCTRL			= 0x0040 / sizeof(DWORD),
			regADCSTAT		= 0x0044 / sizeof(DWORD),
			regADCRANGE		= 0x0048 / sizeof(DWORD),
			regADC_CLKDIV		= 0x004C / sizeof(DWORD),
			regADC_MISC		= 0x0050 / sizeof(DWORD),
			regSTEPEN		= 0x0054 / sizeof(DWORD),
			regIDLECONFIG		= 0x0058 / sizeof(DWORD),
			regCHARGE_STEP		= 0x005C / sizeof(DWORD),
			regCHARGE_DELAY		= 0x0060 / sizeof(DWORD),
			regSTEPCONFIG_0		= 0x0064 / sizeof(DWORD),
			regSTEPDELAY_0		= 0x0068 / sizeof(DWORD),
			regSTEPCONFIG_1		= 0x006C / sizeof(DWORD),
			regSTEPDELAY_1		= 0x0070 / sizeof(DWORD),
			regSTEPCONFIG_2		= 0x0074 / sizeof(DWORD),
			regSTEPDELAY_2		= 0x0078 / sizeof(DWORD),
			regSTEPCONFIG_3		= 0x007C / sizeof(DWORD),
			regSTEPDELAY_3		= 0x0080 / sizeof(DWORD),
			regSTEPCONFIG_4		= 0x0084 / sizeof(DWORD),
			regSTEPDELAY_4		= 0x0088 / sizeof(DWORD),
			regSTEPCONFIG_5		= 0x008C / sizeof(DWORD),
			regSTEPDELAY_5		= 0x0090 / sizeof(DWORD),
			regSTEPCONFIG_6		= 0x0094 / sizeof(DWORD),
			regSTEPDELAY_6		= 0x0098 / sizeof(DWORD),
			regSTEPCONFIG_7		= 0x009C / sizeof(DWORD),
			regSTEPDELAY_7		= 0x00A0 / sizeof(DWORD),
			regSTEPCONFIG_8		= 0x00A4 / sizeof(DWORD),
			regSTEPDELAY_8		= 0x00A8 / sizeof(DWORD),
			regSTEPCONFIG_9		= 0x00AC / sizeof(DWORD),
			regSTEPDELAY_9		= 0x00B0 / sizeof(DWORD),
			regSTEPCONFIG_10	= 0x00B4 / sizeof(DWORD),
			regSTEPDELAY_10		= 0x00B8 / sizeof(DWORD),
			regSTEPCONFIG_11	= 0x00BC / sizeof(DWORD),
			regSTEPDELAY_11		= 0x00C0 / sizeof(DWORD),
			regSTEPCONFIG_12	= 0x00C4 / sizeof(DWORD),
			regSTEPDELAY_12		= 0x00C8 / sizeof(DWORD),
			regSTEPCONFIG_13	= 0x00CC / sizeof(DWORD),
			regSTEPDELAY_13		= 0x00D0 / sizeof(DWORD),
			regSTEPCONFIG_14	= 0x00D4 / sizeof(DWORD),
			regSTEPDELAY_14		= 0x00D8 / sizeof(DWORD),
			regSTEPCONFIG_15	= 0x00DC / sizeof(DWORD),
			regSTEPDELAY_15		= 0x00E0 / sizeof(DWORD),
			regFIFO0COUNT		= 0x00E4 / sizeof(DWORD),
			regFIFO0THR		= 0x00E8 / sizeof(DWORD),
			regDMAREQ0		= 0x00EC / sizeof(DWORD),
			regFIFO1COUNT		= 0x00F0 / sizeof(DWORD),
			regFIFO1THR		= 0x00F4 / sizeof(DWORD),
			regDMAREQ1		= 0x00F8 / sizeof(DWORD),
			regFIFO0DATA		= 0x0100 / sizeof(DWORD),
			regFIFO1DATA		= 0x0200 / sizeof(DWORD),
			};

		// Data Members
		PVDWORD      m_pBase;
		UINT         m_uLine;
		ITimer	   * m_pTimer;
		UINT         m_uState;
		UINT	     m_uValid;
		UINT	     m_uCount;
		UINT         m_uWait;
		UINT         m_uMaxVar;
		UINT	     m_uMinVal;
		WORD         m_wXSamples[8];
		WORD         m_wYSamples[8];
		WORD         m_wZSamples[8];

		// Overridables
		void DefaultCalib(void);
		bool OnTouchValue(void);

		// Implementation
		void InitController(void);
		void InitEvents(void);
		void OnTouch(void);
		void OnTimer(void);
		UINT FindTouch(PWORD pSamples, UINT uCount);
		bool CheckPressure(PWORD pSamples, UINT uCount);
		bool CheckRelease(PWORD pSamples, UINT uCount);
	};

// End of File

#endif
