
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosFileSupport_HPP

#define INCLUDE_RlosFileSupport_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/BaseFileSupport.hpp"

#include "../../StdEnv/IFileSystem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// File System Support
//

class CRlosFileSupport : public CBaseFileSupport, public IFileUtilities
{
public:
	// Constructor
	CRlosFileSupport(void);

	// Destructor
	~CRlosFileSupport(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IFileUtilities
	BOOL   METHOD IsDiskMounted(char cDrive);
	UINT   METHOD GetDiskStatus(char cDrive);
	DWORD  METHOD GetDiskIdent(char cDrive);
	UINT64 METHOD GetDiskSize(char cDrive);
	UINT64 METHOD GetDiskFree(char cDrive);
	BOOL   METHOD FormatDisk(char cDrive);
	BOOL   METHOD EjectDisk(char cDrive);

protected:
	// Data Members
	IFileManager * m_pFileMan;
	IDiskManager * m_pDiskMan;

	// Host Hooks
	bool  HostRename(CString const &From, CString const &To);
	bool  HostUnlink(CString const &Path);
	bool  HostStat(CString const &Path, struct stat *buffer);
	bool  HostUTime(CString const &Path, time_t time);
	bool  HostChMod(CString const &Path, mode_t mode);
	PVOID HostOpen(CString const &Path, int oflag, int pmode);
	void  HostClose(PVOID hFile);
	bool  HostStat(PVOID hFile, struct stat *buffer);
	int   HostRead(PVOID hFile, void *buffer, unsigned int count);
	int   HostWrite(PVOID hFile, void const *buffer, unsigned int count);
	int   HostSeek(PVOID hFile, long offset, int origin);
	bool  HostTruncate(PVOID hFile, DWORD size);
	int   HostIoCtl(PVOID hFile, int func, void *data);
	bool  HostIsValidDir(CString const &Path);
	bool  HostRmDir(CString const &Path);
	bool  HostMkDir(CString const &Path);
	int   HostScanDir(CString const &Path, struct dirent ***list, int (*selector)(struct dirent const *));
	bool  HostSync(void);

	// Implementation
	BOOL FindManagers(void);
};

// End of File

#endif
