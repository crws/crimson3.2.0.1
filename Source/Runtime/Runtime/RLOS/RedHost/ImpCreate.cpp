
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Creation Helper
//

bool CreateStaticObject(PCTXT pName, REFIID riid, void **ppObject)
{
	if( piob ) {

		int n;
	
		while( (n = AtomicCompAndSwap(PINT(ppObject), 0, 1)) <= 1 ) {

			if( n == 0 ) {

				IUnknown *pObject = NULL;

				piob->NewObject(pName, riid, (void **) &pObject);

				if( !pObject ) {

					*ppObject = NULL;

					return false;
					}

				*ppObject = pObject;

				return true;
				}

			Sleep(10);
			}

		return true;
		}

	return false;
	}

bool LocateStaticObject(PCTXT pName, REFIID riid, UINT uInst, void **ppObject)
{
	if( piob ) {

		int n;
	
		while( (n = AtomicCompAndSwap(PINT(ppObject), 0, 1)) <= 1 ) {

			if( n == 0 ) {

				IUnknown *pObject = NULL;

				piob->GetObject(pName, uInst, riid, (void **) &pObject);

				if( !pObject ) {

					*ppObject = NULL;

					return false;
					}

				*ppObject = pObject;

				return true;
				}

			Sleep(10);
			}

		return true;
		}

	return false;
	}

// End of File
