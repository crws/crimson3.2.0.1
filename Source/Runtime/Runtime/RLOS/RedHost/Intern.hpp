
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Intern_HPP

#define INCLUDE_Intern_HPP

//////////////////////////////////////////////////////////////////////////
//
// Environment Selection
//

#define AEON_NEED_RLOS

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include "../../HSL/AeonHsl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// System Call Wrappers
//

clink void   _exit(int code);
clink int    _open(char const *name, int oflag, int pmode);
clink int    _close(int fd);
clink int    _isatty(int fd);
clink int    _fstat(int fd, struct stat *buffer);
clink int    _read(int fd, void *buffer, unsigned int count);
clink int    _write(int fd, void const *buffer, unsigned int count);
clink int    _lseek(int fd, long offset, int origin);
clink int    _gettimeofday(timeval *pt, void const *tz);
clink int    _settimeofday(timeval const *pt, void *tz);

// End of File

#endif
