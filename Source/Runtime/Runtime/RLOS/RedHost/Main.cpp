
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ModuleManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Command Line Access
//

extern bool GetCommandLine(CStringArray &List);

//////////////////////////////////////////////////////////////////////////
//
// Constructors Hook
//

extern void CallCtors(void);

//////////////////////////////////////////////////////////////////////////
//
// System Object Management
//

extern void Init_RtlSupport(void);

extern void Term_RtlSupport(void);

//////////////////////////////////////////////////////////////////////////
//
// Entry Points
//

// Static Data

static CModuleManager * m_pModMan = NULL;

static BOOL             m_fTest   = NULL;

static CStringArray     m_Modules;

// Prototypes

global	int	Main(void);
global	BOOL	LoadModules(void);
global	void	FreeModules(void);
global	BOOL	FindModule(DWORD dwAddr, char *pName, DWORD &dwBase);
static	int	HostInit(void);
global	BOOL	ServiceHook(int &nCode);

// Code

global int Main(void)
{
	Init_RtlSupport();

	AfxTrace("Main\n");

	CallCtors();

	int r = HostInit();

	Term_RtlSupport();

	return r;
	}

global BOOL LoadModules(void)
{
	m_pModMan = New CModuleManager;

	for( UINT n = 0; n < m_Modules.GetCount(); n++ ) {

		m_pModMan->LoadClientModule(m_Modules[n]);
		}

	return TRUE;
	}

global void FreeModules(void)
{
	m_pModMan->FreeAllModules();

	delete m_pModMan;

	m_Modules.Empty();
	}

global BOOL FindModule(DWORD dwAddr, char *pName, DWORD &dwBase)
{
	if( m_pModMan ) {

		if( m_pModMan->FindModule(dwAddr, pName, dwBase) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

static int HostInit(void)
{
	CStringArray List;

	if( GetCommandLine(List) ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CString const &Arg = List[n];

			if( Arg[0] == '-' || Arg[0] == '/' ) {

				if( Arg.Mid(1) == "test" ) {

					m_fTest = TRUE;

					continue;
					}
			
				AfxTrace("host: bad switch '%s'\n", PCTXT(Arg)+1);

				return -1;
				}

			m_Modules.Append(Arg);
			}
		}

	if( m_Modules.IsEmpty() ) {

		m_Modules.Append(m_fTest ? "CMTest" : "CM1");
		}

	return AeonHostInit(m_fTest);
	}

global BOOL ServiceHook(int &nCode)
{
	return FALSE;
	}

// End of File
