
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Creation Helper
//

extern bool CreateStaticObject(PCTXT pName, REFIID riid, void **ppObject);

//////////////////////////////////////////////////////////////////////////
//
// Debug Trace Implementation
//

// Data

static IMutex       * m_pMutex = NULL;

static IDiagManager * m_pDiag  = NULL;

// Code

global void AfxDebug(char cData)
{
	phal->DebugOut(cData);
	}

global void AfxDebug(PCTXT pText)
{
	while( *pText ) {

		phal->DebugOut(*pText++);
		}
	}

global void AfxPrint(PCTXT pText)
{
	if( Hal_GetIrql() == IRQL_TASK ) {

		if( CreateStaticObject("exec.mutex", AfxAeonIID(IMutex), (void **) &m_pMutex) ) {

			GuardThread(TRUE);

			if( m_pMutex->Wait(FOREVER) ) {

				if( !m_pDiag ) {

					AfxGetObject("aeon.diagmanager", 0, IDiagManager, m_pDiag);
					}

				if( m_pDiag ) {

					m_pDiag->WriteToConsoles(pText);
					}

				AfxDebug(pText);

				m_pMutex->Free();
				}

			GuardThread(FALSE);

			return;
			}
		}

	AfxDebug(pText);
	}

// End of File
