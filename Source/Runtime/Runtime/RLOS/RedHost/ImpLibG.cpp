
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LibG Global Data
//

extern "C"
{
	int __dso_handle = 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// LibG Implementation
//

clink void __cxa_pure_virtual(void)
{
	HostTrap(PANIC_AEON_PURE_VIRTUAL);
	}

clink void __cxa_throw_bad_array_new_length(void)
{
	HostTrap(PANIC_AEON_ARRAY_LENGTH);
	}

clink void __cxa_bad_typeid(void)
{
	HostTrap(PANIC_AEON_BAD_TYPEID);
	}

clink void __cxa_bad_cast(void)
{
	HostTrap(PANIC_AEON_BAD_CAST);
	}

clink void __aeabi_atexit(void)
{
	}

namespace __gnu_cxx
{
	void __verbose_terminate_handler()
	{
		HostTrap(PANIC_AEON_ABORT);
		}
	}

namespace std
{
	void abort(void)
	{
		HostTrap(PANIC_AEON_ABORT);
		}
	}

// End of File
