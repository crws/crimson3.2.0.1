
#include "Intern.hpp"

#include "../../HSL/AnsiDebugConsole.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Console
//

// Data

static CAnsiDebugConsole * m_pConsole = NULL;

// Code

global BOOL InitConsole(void)
{
	m_pConsole = New CAnsiDebugConsole(NULL, TRUE, TRUE);

	return TRUE;
	}

global BOOL ExecConsole(HTHREAD hThread)
{
	SetThreadName("Console");

	m_pConsole->Hello();

	while( GetThreadExecState(hThread) < 4 ) {

		UINT c = phal->DebugRead(100);

		if( c < NOTHING ) {

			if( m_pConsole->OnChar(c) ) {

				phal->RestartSystem();
				}
			}
		}

	return FALSE;
	}

global void TermConsole(void)
{
	if( m_pConsole ) {

		delete m_pConsole;

		m_pConsole = NULL;
		}
	}

// End of File
