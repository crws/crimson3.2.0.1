
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Constructor Calls for ARM
//

#if defined(AEON_COMP_GCC) && defined(AEON_PROC_ARM)

// Data

clink int __init_array_start;

clink int __init_array_end;

// Code

BOOL CallCtors(void)
{
	// This version of GCC uses a simpler approach with markers
	// at the start and end of the ctor segment. We just scan the
	// list between those markers and call what we find.

	PDWORD p = PDWORD(&__init_array_start);

	PDWORD e = PDWORD(&__init_array_end);

	while( p < e ) {

		void (*pfnFunc)(void) = (void (*)(void)) *p++;

		(*pfnFunc)();
		}

	return TRUE;
	}

#endif

// End of File
