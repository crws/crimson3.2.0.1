
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SBrk Implementation
//

// Data

clink int	    _end;

clink unsigned long _top;

// Code

static unsigned long align(unsigned long n, unsigned long p)
{
	return p * ((n + p - 1) / p);
	}

clink void * sbrk(ptrdiff_t increment)
{
	// This is based upon the Linux implementation so is
	// more complex than it needs to be, but it's easier
	// just to bring it across. It is modified as to to
	// grab 64K memory chunks at a time.

	static unsigned long our_brk = 0;

	static unsigned long sys_brk = 0;

	if( !our_brk ) {

		our_brk = (unsigned long) &_end;

		sys_brk = our_brk;
		}

	if( our_brk + increment <= sys_brk ) {

		unsigned long old_brk = our_brk;

		our_brk = our_brk + increment;

		return (void *) old_brk;
		}
	else {
		unsigned long old_brk = our_brk;

		unsigned long new_brk = align(our_brk + increment, 65536);

		unsigned long ret_brk = new_brk;

		if( ret_brk >= _top ) {

			// TODO -- How about we return NULL for allocations
			// over 1MB that fail and make that part of our contract
			// for clients? They'd only have to check malloc / new
			// if they new they were allocating so much... !!!

			HostTrap(PANIC_AEON_OUT_OF_MEMORY);
			}

		if( ret_brk > sys_brk ) {

			sys_brk = ret_brk;

			our_brk = our_brk + increment;

			return (void *) old_brk;
			}

		return NULL;
		}
	}	

// End of File
