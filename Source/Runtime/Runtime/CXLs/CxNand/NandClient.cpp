
#include "Intern.hpp"

#include "NandClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Utilities
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Support
//

// Macros

#define	RoundUp(x) (((x) + 3) & ~3)

#define bits(x)    (sizeof(x) * 8)

// Constructor

CNandClient::CNandClient(void)
{
	m_pNandMemory = NULL;

	AfxGetObject("nand", 0, INandMemory, m_pNandMemory);

	m_pNandMemory->GetGeometry(m_Geometry);

	m_uPageSize  = m_Geometry.m_uSectors * m_Geometry.m_uBytes;

	m_pPageData  = New BYTE[m_uPageSize];

	m_pTestData  = New BYTE[m_uPageSize];

	m_pBadBlocks = NULL;

	m_uBadBlocks = 0;

	m_uGoodPages = 0;

	m_uFreePages = 0;
}

// Destructor

CNandClient::~CNandClient(void)
{
	delete[] m_pPageData;

	delete[] m_pTestData;

	delete[] m_pBadBlocks;
}

// Initialisation

void CNandClient::Init(void)
{
	m_uIndexStart = GetBlockIndex(m_BlockStart);

	m_uIndexEnd   = GetBlockIndex(m_BlockEnd);

	CreateBadBlockMap();
}

bool CNandClient::EraseFirst(bool fForce)
{
	CNandBlock Block = m_BlockStart;

	do {
		if( EraseBlock(Block, fForce) ) {

			return true;
		}

	} while( GetNextGoodBlock(Block) );

	return false;
}

bool CNandClient::EraseAll(bool fForce)
{
	CNandBlock Block = m_BlockStart;

	do {
		EraseBlock(Block, fForce);

	} while( GetNextGoodBlock(Block) );

	return true;
}

// Write with Relocation

bool CNandClient::WriteWithReloc(CNandPage &Page)
{
	// NOTE -- This is quite subtle. We are writing a new page to a block, possibly at
	// the start of the block, but most likely further down. If the write fails, we have
	// to copy the existing data from the original target block and then try again, but
	// the copy may fail or the further attempt to write may fail.

	CNandBlock const ThisBlock = m_PageCurrent;

	CNandPage  const StopPage  = m_PageCurrent;

	bool             fMarkBad  = false;

	while( !WritePage(m_PageCurrent, true) ) {

		CNandBlock NextBlock = m_PageCurrent;

		for( ;;) {

			if( GetNextGoodBlock(NextBlock) ) {

				CNandPage FromPage = ThisBlock;

				CNandPage DestPage = NextBlock;

				if( CopyPages(DestPage, FromPage, StopPage) ) {

					m_PageCurrent = DestPage;

					fMarkBad      = true;

					break;
				}

				if( DestPage == m_BlockEnd ) {

					return false;
				}

				continue;
			}

			return false;
		}
	}

	if( fMarkBad ) {

		UINT uPages  = m_Geometry.m_uPages - StopPage.m_uPage;

		m_uFreePages = m_uFreePages        - uPages;

		MarkBlockBad(ThisBlock);
	}

	m_uFreePages = m_uFreePages - 1;

	Page         = m_PageCurrent;

	return GetNextGoodPage(m_PageCurrent);
}

bool CNandClient::CopyPages(CNandPage &Dest, CNandPage &From, CNandPage Stop)
{
	CAutoArray<BYTE> pCopyData(m_uPageSize);

	while( From < Stop ) {

		if( ReadPage(From, pCopyData) ) {

			if( WritePage(Dest, pCopyData, true) ) {

				OnPageReloc(pCopyData, Dest);

				if( !GetNextGoodPage(Dest) ) {

					Dest = m_BlockEnd;

					return false;
				}

				if( !GetNextGoodPage(From) ) {

					Dest = m_BlockEnd;

					return false;
				}

				continue;
			}

			UINT uPages  = m_Geometry.m_uPages;

			m_uFreePages = m_uFreePages - uPages;

			MarkBlockBad(Dest);
		}

		return false;
	}

	return true;
}

// Bad Block Mapping

bool CNandClient::CreateBadBlockMap(void)
{
	if( !m_pBadBlocks ) {

		UINT uTotal  = m_uIndexEnd - m_uIndexStart;

		UINT uAlloc  = (uTotal + bits(DWORD) - 1) / bits(DWORD);

		m_pBadBlocks = New DWORD[uAlloc];

		memset(m_pBadBlocks, 0, uAlloc * sizeof(DWORD));

		m_uBadBlocks = 0;

		m_uGoodPages = 0;

		CNandBlock Block = m_BlockStart;

		do {
			if( m_pNandMemory->IsBlockBad(Block.m_uChip, Block.m_uBlock) ) {

				if( true ) {

					UINT n = GetBlockIndex(Block) - m_uIndexStart;

					UINT i = n / bits(DWORD);

					UINT b = n % bits(DWORD);

					m_pBadBlocks[i] |= (1 << b);

					m_uBadBlocks += 1;
				}
				else {
					// NOTE -- Only enable this to clear all bad block
					// markers from the device. Should not be done lightly
					// unless you know they've been incorrectly placed.

					m_pNandMemory->EraseBlock(Block.m_uChip, Block.m_uBlock);

					m_uGoodPages += m_Geometry.m_uPages;
				}
			}
			else
				m_uGoodPages += m_Geometry.m_uPages;

		} while( GetNextGoodBlock(Block) );

		m_uFreePages = m_uGoodPages;

		return true;
	}

	return false;
}

UINT CNandClient::GetBlockIndex(CNandBlock const &Block)
{
	return m_Geometry.m_uBlocks * Block.m_uChip + Block.m_uBlock;
}

bool CNandClient::IsBadBlock(CNandBlock const &Block)
{
	if( m_pBadBlocks ) {

		UINT n = GetBlockIndex(Block) - m_uIndexStart;

		UINT i = n / bits(DWORD);

		UINT b = n % bits(DWORD);

		return !!(m_pBadBlocks[i] & (1 << b));
	}

	return false;
}

void CNandClient::MarkBlockBad(CNandBlock const &Block)
{
	UINT n = GetBlockIndex(Block) - m_uIndexStart;

	UINT i = n / bits(DWORD);

	UINT b = n % bits(DWORD);

	m_pBadBlocks[i] |= (1 << b);

	m_pNandMemory->MarkBlockBad(Block.m_uChip, Block.m_uBlock);

	m_uBadBlocks += 1;

	m_uGoodPages -= m_Geometry.m_uPages;
}

// Navigation

bool CNandClient::SkipBadBlocks(CNandBlock &Next)
{
	if( IsBadBlock(Next) ) {

		return GetNextGoodBlock(Next);
	}

	return true;
}

bool CNandClient::GetNextGoodBlock(CNandBlock &Next, bool &fWrap)
{
	CNandBlock Scan = Next;

	for( ;;) {

		if( !Scan.IsValid() ) {

			Scan = m_BlockStart;
		}
		else {
			if( ++Scan.m_uBlock >= m_Geometry.m_uBlocks ) {

				if( !(Scan == m_BlockEnd) ) {

					Scan.m_uBlock = 0;

					if( ++Scan.m_uChip >= m_Geometry.m_uChips ) {

						return false;
					}
				}
			}

			if( Scan == m_BlockEnd ) {

				if( !fWrap ) {

					return false;
				}

				Scan  = m_BlockStart;

				fWrap = false;
			}
		}

		if( !IsBadBlock(Scan) ) {

			Next = Scan;

			return true;
		}
	}

	return false;
}

bool CNandClient::GetNextGoodBlock(CNandPage &Next, bool &fWrap)
{
	CNandBlock &Block = Next;

	if( GetNextGoodBlock(Block, fWrap) ) {

		Next.m_uPage = 0;

		return true;
	}

	return false;
}

bool CNandClient::GetNextGoodBlock(CNandBlock &Next)
{
	bool fWrap = false;

	return GetNextGoodBlock(Next, fWrap);
}

bool CNandClient::GetNextGoodBlock(CNandPage &Next)
{
	bool fWrap = false;

	return GetNextGoodBlock(Next, fWrap);
}

bool CNandClient::GetNextGoodPage(CNandPage &Next)
{
	CNandPage Scan = Next;

	if( Scan.IsValid() ) {

		if( ++Scan.m_uPage >= m_Geometry.m_uPages ) {

			Scan.m_uPage = 0;

			if( !GetNextGoodBlock(Scan) ) {

				return false;
			}
		}
	}
	else {
		if( !GetNextGoodBlock(Scan) ) {

			return false;
		}

		Scan.m_uPage = 0;
	}

	Next = Scan;

	return true;
}

bool CNandClient::GetNextGoodSector(CNandSector &Next)
{
	CNandSector Scan = Next;

	if( ++Scan.m_uSector >= m_Geometry.m_uSectors ) {

		Scan.m_uSector = 0;

		if( !GetNextGoodPage(Scan) ) {

			return false;
		}
	}

	Next = Scan;

	return true;
}

// NAND Device Wrappers

bool CNandClient::EraseBlock(CNandBlock const &Block, bool fForce)
{
	if( !IsBadBlock(Block) ) {

		if( !fForce && IsBlockEmpty(Block) ) {

			return true;
		}

		if( !m_pNandMemory->EraseBlock(Block.m_uChip, Block.m_uBlock) ) {

			MarkBlockBad(Block);

			return false;
		}

		return true;
	}

	return true;
}

bool CNandClient::ReadPage(CNandPage const &Page)
{
	return ReadPage(Page, m_pPageData);
}

bool CNandClient::WritePage(CNandPage const &Page, BOOL fCheck)
{
	return WritePage(Page, m_pPageData, fCheck);
}

bool CNandClient::ReadPage(CNandPage const &Page, PBYTE pBuffer)
{
	Trace(Page, "RD");

	return m_pNandMemory->ReadPage(Page.m_uChip, Page.m_uBlock, Page.m_uPage, pBuffer) ? true : false;
}

bool CNandClient::WritePage(CNandPage const &Page, PBYTE pBuffer, BOOL fCheck)
{
	Trace(Page, "WR");

	if( fCheck ) {

		if( m_pNandMemory->WritePage(Page.m_uChip, Page.m_uBlock, Page.m_uPage, pBuffer) ) {

			Trace(Page, "RB");

			if( m_pNandMemory->ReadPage(Page.m_uChip, Page.m_uBlock, Page.m_uPage, m_pTestData) ) {

				if( !memcmp(pBuffer, m_pTestData, m_uPageSize) ) {

					return true;
				}

				Trace(Page, "RB Mismatch");

				return false;
			}

			Trace(Page, "RB Fail");

			return false;
		}

		Trace(Page, "WR Fail");

		return false;
	}

	return m_pNandMemory->WritePage(Page.m_uChip, Page.m_uBlock, Page.m_uPage, pBuffer) ? true : false;
}

bool CNandClient::ReadSector(CNandSector const &Sector, PBYTE pBuffer)
{
	Trace(Sector, "RS");

	return m_pNandMemory->ReadSector(Sector.m_uChip,
					 Sector.m_uBlock,
					 Sector.m_uPage,
					 Sector.m_uSector,
					 pBuffer
	) ? true : false;
}

bool CNandClient::WriteSector(CNandSector const &Sector, PBYTE pBuffer, BOOL fCheck)
{
	Trace(Sector, "WS");

	if( fCheck ) {

		if( m_pNandMemory->WriteSector(Sector.m_uChip, Sector.m_uBlock, Sector.m_uPage, Sector.m_uSector, pBuffer) ) {

			if( m_pNandMemory->ReadSector(Sector.m_uChip, Sector.m_uBlock, Sector.m_uPage, Sector.m_uSector, m_pTestData) ) {

				if( !memcmp(m_pPageData, m_pTestData, m_Geometry.m_uBytes) ) {

					return true;
				}

				Trace(Sector, "RS Mismatch");

				return false;
			}

			Trace(Sector, "RS Fail");

			return false;
		}

		Trace(Sector, "WS Fail");

		return false;
	}

	return m_pNandMemory->WriteSector(Sector.m_uChip,
					  Sector.m_uBlock,
					  Sector.m_uPage,
					  Sector.m_uSector,
					  pBuffer
	) ? true : false;
}

bool CNandClient::ReadSector(CNandSector const &Sector)
{
	return ReadSector(Sector, m_pPageData);
}

bool CNandClient::WriteSector(CNandSector const &Sector, BOOL fCheck)
{
	return WriteSector(Sector, m_pPageData, fCheck);
}

// Implementation

bool CNandClient::IsBlockEmpty(CNandBlock const &Block)
{
	CNandPage Page = Block;

	while( Page.m_uPage < m_Geometry.m_uPages ) {

		if( ReadPage(Page) ) {

			PDWORD p = PDWORD(m_pPageData);

			UINT   n = m_uPageSize / sizeof(DWORD);

			while( n-- ) {

				if( *p++ + 1 ) {

					return false;
				}
			}

			Page.m_uPage++;

			continue;
		}

		return false;
	}

	return true;
}

// Overridables

bool CNandClient::OnPageReloc(PCBYTE pData, CNandPage const &Dest)
{
	return false;
}

// Debug

void CNandClient::Trace(CNandPage const &Page, char const *pAction)
{
	#if 0 && defined(_DEBUG)

	Trace("%s %u.%u.%u\n",
	      pAction,
	      Page.m_uChip,
	      Page.m_uBlock,
	      Page.m_uPage
	);

	#endif
}

void CNandClient::Trace(CNandSector const &Sector, char const *pAction)
{
	#if 0 && defined(_DEBUG)

	Trace("%s %u.%u.%u.%u\n",
	      pAction,
	      Sector.m_uChip,
	      Sector.m_uBlock,
	      Sector.m_uPage,
	      Sector.m_uSector
	);

	#endif
}

void CNandClient::Trace(char const *pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	AfxTraceArgs(pText, pArgs);

	va_end(pArgs);
}

// End of File
