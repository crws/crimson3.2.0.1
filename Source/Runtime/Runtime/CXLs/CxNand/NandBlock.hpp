
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Utilities
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandBlock_HPP

#define	INCLUDE_NandBlock_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Block Selector
//

class DLLAPI CNandBlock
{
	public:
		// Constructors
		CNandBlock(void);
		CNandBlock(UINT uChip, UINT uBlock);

		// Public Data
		UINT m_uChip;
		UINT m_uBlock;

		// Operators
		bool operator == (CNandBlock const &That) const;
		bool operator != (CNandBlock const &That) const;
		bool operator  > (CNandBlock const &That) const;
		bool operator  < (CNandBlock const &That) const;

		// Attributes
		bool IsValid(void) const;

		// Operations
		void Invalidate(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructor

STRONG_INLINE CNandBlock::CNandBlock(void)
{
	m_uChip  = 0;

	m_uBlock = 0;
	}

STRONG_INLINE CNandBlock::CNandBlock(UINT uChip, UINT uBlock)
{
	m_uChip  = uChip;

	m_uBlock = uBlock;
	}

STRONG_INLINE bool CNandBlock::operator == (CNandBlock const &That) const
{	
	if( m_uChip  == That.m_uChip ) {

		if( m_uBlock == That.m_uBlock ) {

			return true;
			}
		}

	return false;
	}

STRONG_INLINE bool CNandBlock::operator != (CNandBlock const &That) const
{	
	if( m_uChip  == That.m_uChip ) {

		if( m_uBlock == That.m_uBlock ) {

			return false;
			}
		}

	return true;
	}

STRONG_INLINE bool CNandBlock::operator > (CNandBlock const &That) const
{	
	if( m_uChip > That.m_uChip ) {
		
		return true;
		}

	if( m_uChip < That.m_uChip ) {
		
		return false;
		}

	return m_uBlock > That.m_uBlock;
	}

STRONG_INLINE bool CNandBlock::operator < (CNandBlock const &That) const
{	
	if( m_uChip < That.m_uChip ) {
		
		return true;
		}

	if( m_uChip > That.m_uChip ) {
		
		return false;
		}

	return m_uBlock < That.m_uBlock;
	}


// Attributes

STRONG_INLINE bool CNandBlock::IsValid(void) const
{
	if( m_uChip == NOTHING ) {

		return false;
		}

	if( m_uBlock == NOTHING ) {

		return false;
		}

	return true;
	}

// Operations

STRONG_INLINE void CNandBlock::Invalidate(void)
{
	m_uChip  = NOTHING;

	m_uBlock = NOTHING;
	}

// End of File

#endif
