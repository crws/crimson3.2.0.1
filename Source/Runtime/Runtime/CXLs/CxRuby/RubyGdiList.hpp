
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyGdiList_HPP
	
#define	INCLUDE_RubyGdiList_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CRubyPath;

class CRubyMatrix;

//////////////////////////////////////////////////////////////////////////
//
// Gdi List
//

class DLLAPI CRubyGdiList
{
	public:
		// Constructor
		CRubyGdiList(void);

		// Destructor
		~CRubyGdiList(void);

		// Attributes
		BOOL IsEmpty(void) const;

		// Operations
		void Empty(void);
		void Load(CRubyMatrix const *m, CRubyPath const &figure, bool over);
		void Load(CRubyMatrix const &m, CRubyPath const &figure, bool over);
		void Load(CRubyPath const &figure, bool over);
		void Translate(int x, int y, bool over);

		// Hit Testing
		void GetBoundingRect(R2 &r, bool over) const;
		void AddBoundingRect(R2 &r, bool over) const;

		// Debugging
		void Trace(PCTXT pName) const;

		// Data Members
		UINT   m_uList;
		UINT   m_uCount;
		P2   * m_pList;
		UINT * m_pCount;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

inline BOOL CRubyGdiList::IsEmpty(void) const
{
	return !m_pList;
	}

// End of File

#endif
