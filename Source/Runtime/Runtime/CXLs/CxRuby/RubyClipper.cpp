
#include "Intern.hpp"

#include "RubyClipper.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPoint.hpp"

#include "RubyVertex.hpp"

#include "RubyPath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Clipper Object
//

// Operations

bool CRubyClipper::Clip(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2) 
{
	// Perform a quick scan to see if we even have to clip,
	// only bothering with the copy operation if it proves
	// to be required. This is quicker since most cases do
	// not involve any clipping at all.

	int c = figure.GetCount();

	for( int n = 0; n < c; n++ ) {

		CRubyVertex const &p = figure[n];

		if( p.m_flags ) {

			continue;
			}

		if( p.m_x >= p1.m_x && p.m_x < p2.m_x ) {

			if( p.m_y >= p1.m_y && p.m_y < p2.m_y ) {

				continue;
				}
			}

		CRubyPath output;

		bool r = Clip(output, figure, p1, p2);

		figure = output;

		return r;
		}

	return true;
	}

bool CRubyClipper::Clip(CRubyPath &output, CRubyPath const &figure, CRubyPoint const &p1, CRubyPoint const &p2)
{
	// This is a trivial implementation for testing which simply drops
	// points that are outside the bounding box. A more complex version
	// would insert points at the intersection of the bounding box and the
	// clipped edge, and would handle dropped break points. It would also
	// consider the impact on anti-aliasing. We might just skip clipping
	// at this line and clip on scanline conversion instead.

	int c = figure.GetCount();

	for( int s = 0; s < c; ) {

		int b = figure.GetBreak(s);

		for( int n = 0; n < b; n++ ) {

			CRubyVertex const &p = figure[s+n];

			if( p.m_flags ) {

				output.Append(p);

				continue;
				}

			if( p.m_x >= p1.m_x && p.m_x < p2.m_x ) {

				if( p.m_y >= p1.m_y && p.m_y < p2.m_y ) {

					output.Append(p);
					}
				}
			}

		output.AppendBreak(figure[s + b - 1].m_flags);

		s += b;
		}

	return true;
	}

// End of File
