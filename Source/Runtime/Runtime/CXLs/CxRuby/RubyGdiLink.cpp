
#include "Intern.hpp"

#include "RubyGdiLink.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPath.hpp"

#include "RubyGdiList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Gdi Link
//

// Operations

bool CRubyGdiLink::OutputShade(CRubyGdiList const &list, PSHADER pShade, int nAlpha)
{
	if( list.m_pList ) {

		UINT n = 0;

		UINT p = 0;

		for(;;) {

			UINT c1 = list.m_pCount[n++];

			UINT c2 = list.m_pCount[n++];

			if( c1 || c2 ) {

				P2 * p1 = list.m_pList + p;

				p       = p + c1;

				P2 * p2 = list.m_pList + p;

				p       = p + c2;

				m_pGdi->SmoothPolygon(p1, c1, p2, c2, 0, pShade, nAlpha);

				continue;
				}

			break;
			}

		return true;
		}

	return false;
	}

bool CRubyGdiLink::OutputSolid(CRubyGdiList const &list, COLOR Color, int nAlpha)
{
	m_pGdi->SetBrushStyle(brushFore);

	m_pGdi->SetBrushFore(Color);

	return OutputShade(list, PSHADER(NULL), nAlpha);
	}

bool CRubyGdiLink::OutputShade(CRubyGdiList const &list, PSHADER pShade)
{
	if( list.m_pList ) {

		UINT n = 0;

		UINT p = 0;

		for(;;) {

			UINT c1 = list.m_pCount[n++];

			UINT c2 = list.m_pCount[n++];

			if( c1 || c2 ) {

				P2 * p1 = list.m_pList + p;

				p       = p + c1;

				P2 * p2 = list.m_pList + p;

				p       = p + c2;

				m_pGdi->ShadePolygon(p1, c1, p2, c2, 0, pShade);

				continue;
				}

			break;
			}

		return true;
		}

	return false;
	}

bool CRubyGdiLink::OutputSolid(CRubyGdiList const &list, COLOR Color)
{
	m_pGdi->SetBrushStyle(brushFore);

	m_pGdi->SetBrushFore(Color);

	return OutputShade(list, PSHADER(NULL));
	}

// End of File
