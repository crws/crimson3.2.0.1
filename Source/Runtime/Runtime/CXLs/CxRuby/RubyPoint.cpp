
#include "Intern.hpp"

#include "RubyPoint.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyMatrix.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Point Object
//

// Operations

void CRubyPoint::Transform(CRubyMatrix const &m)
{
	if( !m.IsIdentity() ) {

		number nx = m_x * m.m_m.m_e[0][0] + m_y * m.m_m.m_e[0][1] + m.m_m.m_e[0][2];
	
		number ny = m_x * m.m_m.m_e[1][0] + m_y * m.m_m.m_e[1][1] + m.m_m.m_e[1][2];
	
		number nk = m_x * m.m_m.m_e[2][0] + m_y * m.m_m.m_e[2][1] + m.m_m.m_e[2][2];

		m_x = nx / nk;

		m_y = ny / nk;
		}
	}

void CRubyPoint::Inverse(CRubyMatrix const &m)
{
	if( !m.IsIdentity() ) {

		number nx = m_x * m.m_i.m_e[0][0] + m_y * m.m_i.m_e[0][1] + m.m_i.m_e[0][2];
	
		number ny = m_x * m.m_i.m_e[1][0] + m_y * m.m_i.m_e[1][1] + m.m_i.m_e[1][2];
	
		number nk = m_x * m.m_i.m_e[2][0] + m_y * m.m_i.m_e[2][1] + m.m_i.m_e[2][2];

		m_x = nx / nk;

		m_y = ny / nk;
		}
	}

// Debugging

void CRubyPoint::Trace(PCTXT pName) const
{
	#if 0

	// This version produces better output for pasting
	// into Excel and then creating scatter graphs to
	// assess the quality of the output figure.

	AfxTrace( "%s,%9.4f,%9.4f\n",
		  pName,
		  m_x,
		  m_y
		  );

	#else

	// This version is easier to read on the console.

	AfxTrace( "%s = ( %9.4f, %9.4f )\n",
		  pName,
		  m_x,
		  m_y
		  );

	#endif
	}

// End of File
