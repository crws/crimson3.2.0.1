
#include "Intern.hpp"

#include "RubyMatrix.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyVector.hpp"

#include "RubyTrig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix Object
//

// Constructors

CRubyMatrix::CRubyMatrix(void)
{
	SetIdentity();
	}

CRubyMatrix::CRubyMatrix(CRubyMatrix const &That)
{
	for( int i = 0; i < 3; i++ ) {

		for( int j = 0; j < 3; j++ ) {

			m_m.m_e[i][j] = That.m_m.m_e[i][j];
			
			m_i.m_e[i][j] = That.m_i.m_e[i][j];
			}
		}

	m_fIdent = That.m_fIdent;
	}

// Assignment

CRubyMatrix const & CRubyMatrix::operator = (CRubyMatrix const &That)
{
	for( int i = 0; i < 3; i++ ) {

		for( int j = 0; j < 3; j++ ) {

			m_m.m_e[i][j] = That.m_m.m_e[i][j];
			
			m_i.m_e[i][j] = That.m_i.m_e[i][j];
			}
		}

	m_fIdent = That.m_fIdent;

	return ThisObject;
	}

// Attributes

bool CRubyMatrix::IsIdentity(void) const
{
	if( m_fIdent ) {

		return true;
		}

	for( int i = 0; i < 3; i++ ) {

		for( int j = 0; j < 3; j++ ) {

			if( num_equal(m_m.m_e[i][j], (i == j) ? 1 : 0) ) {

				continue;
				}

			return false;
			}
		}

	return true;
	}

bool CRubyMatrix::IsTranslation(void) const
{
	CRubyMatrix m = ThisObject;

	m.m_m.m_e[0][2] = 0;

	m.m_m.m_e[1][2] = 0;

	return m.IsIdentity();
	}

bool CRubyMatrix::IsSimple(void) const
{
	return IsTranslation();
	}

void CRubyMatrix::GetInverse(CRubyMatrix &m) const
{
	for( int i = 0; i < 3; i++ ) {

		for( int j = 0; j < 3; j++ ) {

			m.m_m.m_e[i][j] = m_i.m_e[i][j];
			
			m.m_i.m_e[i][j] = m_m.m_e[i][j];
			}
		}
	}

// Initialization

void CRubyMatrix::SetIdentity(void)
{
	for( int i = 0; i < 3; i++ ) {

		for( int j = 0; j < 3; j++ ) {

			m_m.m_e[i][j] = number((i == j) ? 1 : 0);

			m_i.m_e[i][j] = number((i == j) ? 1 : 0);
			}
		}

	m_fIdent = true;
	}

void CRubyMatrix::SetTranslation(number dx, number dy)
{
	m_m.m_e[0][0] = 1; m_m.m_e[0][1] = 0; m_m.m_e[0][2] = dx;
	
	m_m.m_e[1][0] = 0; m_m.m_e[1][1] = 1; m_m.m_e[1][2] = dy;
	
	m_m.m_e[2][0] = 0; m_m.m_e[2][1] = 0; m_m.m_e[2][2] = 1;

	num_add_invert(dx);
	
	num_add_invert(dy);

	m_i.m_e[0][0] = 1; m_i.m_e[0][1] = 0; m_i.m_e[0][2] = dx;
	
	m_i.m_e[1][0] = 0; m_i.m_e[1][1] = 1; m_i.m_e[1][2] = dy;
	
	m_i.m_e[2][0] = 0; m_i.m_e[2][1] = 0; m_i.m_e[2][2] = 1;

	m_fIdent = false;
	}

void CRubyMatrix::SetTranslation(CRubyVector const &v)
{
	SetTranslation(v.m_x, v.m_y);
	}

void CRubyMatrix::SetRotation(double theta)
{
	number c = CRubyTrig::Cos(theta);

	number s = CRubyTrig::Sin(theta);

	m_m.m_e[0][0] = +c; m_m.m_e[0][1] = -s; m_m.m_e[0][2] = 0;
	
	m_m.m_e[1][0] = +s; m_m.m_e[1][1] = +c; m_m.m_e[1][2] = 0;
	
	m_m.m_e[2][0] =  0; m_m.m_e[2][1] =  0; m_m.m_e[2][2] = 1;

	m_i.m_e[0][0] = +c; m_i.m_e[0][1] = +s; m_i.m_e[0][2] = 0;
	
	m_i.m_e[1][0] = -s; m_i.m_e[1][1] = +c; m_i.m_e[1][2] = 0;
	
	m_i.m_e[2][0] =  0; m_i.m_e[2][1] =  0; m_i.m_e[2][2] = 1;

	m_fIdent = false;
	}

void CRubyMatrix::SetScaleFactor(number kx, number ky)
{
	m_m.m_e[0][0] = kx; m_m.m_e[0][1] =  0; m_m.m_e[0][2] = 0;
	
	m_m.m_e[1][0] =  0; m_m.m_e[1][1] = ky; m_m.m_e[1][2] = 0;
	
	m_m.m_e[2][0] =  0; m_m.m_e[2][1] =  0; m_m.m_e[2][2] = 1;

	num_mul_invert(kx);
	
	num_mul_invert(ky);

	m_i.m_e[0][0] = kx; m_i.m_e[0][1] =  0; m_i.m_e[0][2] = 0;
	
	m_i.m_e[1][0] =  0; m_i.m_e[1][1] = ky; m_i.m_e[1][2] = 0;
	
	m_i.m_e[2][0] =  0; m_i.m_e[2][1] =  0; m_i.m_e[2][2] = 1;

	m_fIdent = false;
	}

void CRubyMatrix::SetReflectHorz(void)
{
	m_m.m_e[0][0] = +1; m_m.m_e[0][1] =  0; m_m.m_e[0][2] = 0;
	
	m_m.m_e[1][0] =  0; m_m.m_e[1][1] = -1; m_m.m_e[1][2] = 0;
	
	m_m.m_e[2][0] =  0; m_m.m_e[2][1] =  0; m_m.m_e[2][2] = 1;

	m_i.m_e[0][0] = +1; m_i.m_e[0][1] =  0; m_i.m_e[0][2] = 0;
	
	m_i.m_e[1][0] =  0; m_i.m_e[1][1] = -1; m_i.m_e[1][2] = 0;
	
	m_i.m_e[2][0] =  0; m_i.m_e[2][1] =  0; m_i.m_e[2][2] = 1;

	m_fIdent = false;
	}

void CRubyMatrix::SetReflectVert(void)
{
	m_m.m_e[0][0] = -1; m_m.m_e[0][1] =  0; m_m.m_e[0][2] = 0;
	
	m_m.m_e[1][0] =  0; m_m.m_e[1][1] = +1; m_m.m_e[1][2] = 0;
	
	m_m.m_e[2][0] =  0; m_m.m_e[2][1] =  0; m_m.m_e[2][2] = 1;

	m_i.m_e[0][0] = -1; m_i.m_e[0][1] =  0; m_i.m_e[0][2] = 0;
	
	m_i.m_e[1][0] =  0; m_i.m_e[1][1] = +1; m_i.m_e[1][2] = 0;
	
	m_i.m_e[2][0] =  0; m_i.m_e[2][1] =  0; m_i.m_e[2][2] = 1;

	m_fIdent = false;
	}

void CRubyMatrix::SetReflectDiag(void)
{
	m_m.m_e[0][0] =  0; m_m.m_e[0][1] = +1; m_m.m_e[0][2] = 0;
	
	m_m.m_e[1][0] = +1; m_m.m_e[1][1] =  0; m_m.m_e[1][2] = 0;
	
	m_m.m_e[2][0] =  0; m_m.m_e[2][1] =  0; m_m.m_e[2][2] = 1;

	m_i.m_e[0][0] =  0; m_i.m_e[0][1] = +1; m_i.m_e[0][2] = 0;
	
	m_i.m_e[1][0] = +1; m_i.m_e[1][1] =  0; m_i.m_e[1][2] = 0;
	
	m_i.m_e[2][0] =  0; m_i.m_e[2][1] =  0; m_i.m_e[2][2] = 1;

	m_fIdent = false;
	}

// Composition

void CRubyMatrix::Compose(CRubyMatrix const &That)
{
	CRubyMatrix r;

	for( int i = 0; i < 3; i++ ) {

		for( int j = 0; j < 3; j++ ) {

			number sm = 0;

			number si = 0;

			for( int k = 0; k < 3; k++ ) {

				sm += m_m.m_e[k][j] * That.m_m.m_e[i][k];

				si += m_i.m_e[i][k] * That.m_i.m_e[k][j];
				}

			r.m_m.m_e[i][j] = num_equal(sm, 0) ? 0 : sm;

			r.m_i.m_e[i][j] = num_equal(si, 0) ? 0 : si;
			}
		}

	operator = (r);

	m_fIdent = false;
	}

void CRubyMatrix::AddTranslation(number dx, number dy)
{
	CRubyMatrix m;

	m.SetTranslation(dx, dy);

	Compose(m);
	}

void CRubyMatrix::AddTranslation(CRubyVector const &v)
{
	CRubyMatrix m;

	m.SetTranslation(v);

	Compose(m);
	}

void CRubyMatrix::AddRotation(double theta)
{
	CRubyMatrix m;

	m.SetRotation(theta);

	Compose(m);
	}

void CRubyMatrix::AddScaleFactor(number kx, number ky)
{
	CRubyMatrix m;

	m.SetScaleFactor(kx, ky);

	Compose(m);
	}

void CRubyMatrix::AddReflectHorz(void)
{
	CRubyMatrix m;

	m.SetReflectHorz();

	Compose(m);
	}

void CRubyMatrix::AddReflectVert(void)
{
	CRubyMatrix m;

	m.SetReflectVert();

	Compose(m);
	}

void CRubyMatrix::AddReflectDiag(void)
{
	CRubyMatrix m;

	m.SetReflectDiag();

	Compose(m);
	}

// Debugging

void CRubyMatrix::Trace(PCTXT pName)
{
	AfxTrace("%s =\n", pName);

	for( int i = 0; i < 3; i++ ) {

		for( int p = 0; p < 2; p++ ) {

			AfxTrace("  { ");

			for( int j = 0; j < 3; j++ ) {

				if( j )
					AfxTrace(", ");

				if( p )
					AfxTrace("%8.4f", m_i.m_e[i][j]);
				else
					AfxTrace("%8.4f", m_m.m_e[i][j]);
				}

			if( p )
				AfxTrace(" }\n");
			else
				AfxTrace(" }  ");
			}
		}
	}

// End of File
