
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyGdiLink_HPP
	
#define	INCLUDE_RubyGdiLink_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RubyMatrix.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CRubyPath;

class CRubyGdiList;

//////////////////////////////////////////////////////////////////////////
//
// Gdi Link
//

class DLLAPI CRubyGdiLink
{
	public:
		// Constructor
		CRubyGdiLink(IGdi *pGdi);

		// Operations
		bool OutputShade(CRubyGdiList const &list, PSHADER pShade, int nAlpha);
		bool OutputSolid(CRubyGdiList const &list, COLOR   Color,  int nAlpha);
		bool OutputShade(CRubyGdiList const &list, PSHADER pShade);
		bool OutputSolid(CRubyGdiList const &list, COLOR   Color);

		// Data Members
		IGdi * m_pGdi;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructor

inline CRubyGdiLink::CRubyGdiLink(IGdi *pGdi)
{
	m_pGdi = pGdi;
	}

// End of File

#endif
