
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CxRuby_HPP

#define INCLUDE_CxRuby_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Numeric Type
//

#ifndef DEFINED_number

#define DEFINED_number

typedef double number;

#endif

//////////////////////////////////////////////////////////////////////////
//
// Numeric Macros
//

#define  num_add_invert(x)	((x) = -(x))

#define  num_mul_invert(x)	((x) = 1.0 / (x))

#define  num_round(x)		int((x) + (((x)<0)?-0.5:+0.5))

#define  num_sqrt(x)		(sqrt(x))

#define  num_inv_sqrt(x)	(1.0 / sqrt(x))

#define  num_abs(x)		(fabs(x))

#define	 num_epilson()		(1E-10)

#define	 num_equal(x,y)		(num_abs((x)-(y)) < num_epilson())

//////////////////////////////////////////////////////////////////////////
//
// Inverse Square Root Optimization
//

#ifndef TRUE

#undef  num_inv_sqrt

#define num_inv_sqrt(x) fisr(x)

#define	USE_FISR

#endif

//////////////////////////////////////////////////////////////////////////
//
// Real Point
//

#ifndef DEFINED_P2R

#define DEFINED_P2R

struct P2R
{
	number m_x;
	number m_y;
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// Real Rectangle
//

#ifndef DEFINED_R2R

#define DEFINED_R2R

struct R2R
{
	union {
		number m_x1;
		number m_left;
		};
	union {
		number m_y1;
		number m_top;
		};
	union {
		number m_x2;
		number m_right;
		};
	union {
		number m_y2;
		number m_bottom;
		};
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// Real Matrix
//

#ifndef DEFINED_M3R

#define DEFINED_M3R

struct M3R
{
	number m_e[3][3];
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// Fast Inverse Square Roots
//
//

#ifdef  USE_FISR

#ifndef DEFINED_FISR

#define DEFINED_FISR

// These functions use evil bit manipulation and then one round of
// Newton-Raphsom to produce a fast and approximate inverse square
// root value. They may not be used on a given platform depending
// on performance testing results.
//
// See https://en.wikipedia.org/wiki/Fast_inverse_square_root
//

inline float fisr(float v)
{
	float x2 = 0.5f * v;

	DWORD &i = (DWORD &) v;

	i = 0x5F3759DFL - (i >> 1);

	v  = v * (1.5f - (x2 * v * v));

	return v;
	}

inline double fisr(double v)
{
	double x2 = 0.5 * v;

	INT64  &i = (INT64 &) v;

	i = 0x5FE6EB50C7B537A9LL - (i >> 1);

	v  = v * (1.5 - (x2 * v * v));

	return v;
	}

#endif

#endif

// End of File

#endif
