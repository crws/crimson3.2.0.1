
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyMatrix_HPP
	
#define	INCLUDE_RubyMatrix_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CRubyVector;

class CRubyMatrix;

//////////////////////////////////////////////////////////////////////////
//
// Matrix Object
//

class DLLAPI CRubyMatrix
{
	public:
		// Constructors
		CRubyMatrix(void);
		CRubyMatrix(QUICKARG);
		CRubyMatrix(CRubyMatrix const &That);

		// Assignment
		CRubyMatrix const & operator = (CRubyMatrix const &That);

		// Attributes
		bool   IsIdentity(void) const;
		bool   IsTranslation(void) const;
		bool   IsSimple(void) const;
		number GetScale(void) const;
		number GetInverseScale(void) const;
		void   GetInverse(CRubyMatrix &m) const;

		// Initialization
		void SetIdentity(void);
		void SetTranslation(number dx, number dy);
		void SetTranslation(CRubyVector const &v);
		void SetRotation(double theta);
		void SetScaleFactor(number kx, number ky);
		void SetReflectHorz(void);
		void SetReflectVert(void);
		void SetReflectDiag(void);

		// Composition
		void Compose(CRubyMatrix const &That);
		void AddTranslation(number dx, number dy);
		void AddTranslation(CRubyVector const &v);
		void AddRotation(double theta);
		void AddScaleFactor(number kx, number ky);
		void AddReflectHorz(void);
		void AddReflectVert(void);
		void AddReflectDiag(void);

		// Debugging
		void Trace(PCTXT pName);

		// Data Members
		bool m_fIdent;
		M3R  m_m;
		M3R  m_i;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructors

inline CRubyMatrix::CRubyMatrix(QUICKARG)
{
	}

// Attributes

inline number CRubyMatrix::GetScale(void) const
{
	return num_sqrt(m_m.m_e[0][0] * m_m.m_e[1][1] - m_m.m_e[1][0] * m_m.m_e[0][1]);
	}

inline number CRubyMatrix::GetInverseScale(void) const
{
	return num_inv_sqrt(m_m.m_e[0][0] * m_m.m_e[1][1] - m_m.m_e[1][0] * m_m.m_e[0][1]);
	}

// End of File

#endif
