
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyClipper_HPP
	
#define	INCLUDE_RubyClipper_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CRubyPath;

class CRubyPoint;

//////////////////////////////////////////////////////////////////////////
//
// Clipper Object
//

class CRubyClipper
{
	public:
		// Operations
		static bool Clip(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2);
		static bool Clip(CRubyPath &output, CRubyPath const &figure, CRubyPoint const &p1, CRubyPoint const &p2);
	};

// End of File

#endif
