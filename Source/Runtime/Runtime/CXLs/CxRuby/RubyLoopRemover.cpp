
#include "Intern.hpp"

#include "RubyLoopRemover.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPoint.hpp"

#include "RubyVertex.hpp"

#include "RubyPath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Loop Remover
//

// Operations

void CRubyLoopRemover::RemoveLoops(CRubyPath &figure)
{
	CRubyPath output;

	RemoveLoops(output, figure);

	figure = output;
	}

void CRubyLoopRemover::RemoveLoops(CRubyPath &output, CRubyPath const &figure)
{
	// This code removes the loops that sometimes form when we're creating
	// broad outlines around sharply-turning figures. It works by building
	// a bounding box for each edge, and then performing a scan for N edges
	// ahead to check first for bounding box overlap, and then for an actual
	// intersection between the edges. If we find an intersection, we drop
	// the points in between the two edges and substitute the intersection.

	// A better algorithm would be:
	// https://en.wikipedia.org/wiki/Bentley%E2%80%93Ottmann_algorithm
	// Feel free to implement if you think it's worthwhile!

	int  c = figure.GetCount();

	bb * q = New bb [ c ];

	for( int s = 0; s < c; ) {

		int b = figure.GetBreak(s);

		for( int p = 0; p < 2; p++ ) {

			for( int n = 0; n < b;  ) {

				// Find the two points in the cycle.

				int i1 = (n + 0) % b;
				int i2 = (n + 1) % b;

				CRubyVertex const &p1 = figure[s+i1];
				CRubyVertex const &p2 = figure[s+i2];

				if( p == 0 ) {

					// First pass so build bounding box.

					q[n++].Set(p1, p2);
					}
				else {
					// TODO -- We need to go around the break here
					// or we might miss some stuff!!!

					// We only going to look N elements ahead and we're
					// not going to look beyond breaks. We're also not
					// going to check the next edge, as it will often
					// produce a false positive overlap.

					bool h = false;

					int  k = min(b, n + 10);

					for( int m = n + 2; m < k; m++ ) {

						// Quick bounding box check.

						if( q[n].Intersects(q[m]) ) {

							// The bounding boxes overlaps, so find the
							// indices and associated points so that we
							// can perform the real intersection check.

							int i3 = (m + 0) % b;
							int i4 = (m + 1) % b;

							CRubyVertex const &p3 = figure[s+i3];
							CRubyVertex const &p4 = figure[s+i4];

							CRubyVertex i(QUICK);

							if( FindIntersection(i, p1, p2, p3, p4) ) {

								// We've got an intersection between p1->p2 and p3->p4,
								// so we can replace this with p1->i and i->p4, thereby
								// excising the loop from our vertex list.

								output.Append(p1);

								output.Append(i);

								output.Append(p4);

								n = m + 1;

								h = true;

								break;
								}
							}
						}

					if( !h ) {

						// No intersection, so just add the 
						// point to the output and move on.

						output.Append(p1);

						n++;
						}
					}
				}
			}

		// Append break marker at end of cycle and
		// then move on to see if we have any more
		// vertices to process.

		output.AppendBreak(figure[s + b - 1].m_flags);

		s += b;
		}

	delete [] q;
	}

bool CRubyLoopRemover::FindIntersection(CRubyPoint &i, CRubyPoint const &p1, CRubyPoint const &p2, CRubyPoint const &p3, CRubyPoint const &p4)
{
	// Solve the equations to find the constants k1 and k2 that
	// represent where p1->p2 and p3->p4 overlap. t0 will be zero
	// if they are parallel. If k1 and k2 are between 0 and 1
	// then we overlap within the line segment. Other values are
	// for other points on the underlying infinite line.

	number t0 = (p1.m_x - p2.m_x) * (p4.m_y - p3.m_y) +
		    (p3.m_x - p4.m_x) * (p1.m_y - p2.m_y) ;

	if( !!t0 ) {

		// They're not parallel so solve the rest.

		number t1 = p1.m_x * (p4.m_y - p3.m_y) +
			    p3.m_x * (p1.m_y - p4.m_y) +
			    p4.m_x * (p3.m_y - p1.m_y) ;

		number t2 = p1.m_x * (p2.m_y - p3.m_y) +
			    p2.m_x * (p3.m_y - p1.m_y) +
			    p3.m_x * (p1.m_y - p2.m_y) ;

		number k1 = t1 / t0;
	
		number k2 = t2 / t0;

		// Accept as an intersection only if we are within
		// the center 99.9% of each line. This avoids false
		// positive from rounding errors where lines meet.

		if( k1 >= number(0.001) && k1 <= number(0.999) ) {
			
			if( k2 >= number(0.001) && k2 <= number(0.999) ) {

				// Find the intersection point. We could
				// equally well apply k2 and work from p2.

				i.m_x = p1.m_x + k1 * (p2.m_x - p1.m_x);
			
				i.m_y = p1.m_y + k1 * (p2.m_y - p1.m_y);

				return true;
				}
			}
		}

	return false;
	}

// End of File
