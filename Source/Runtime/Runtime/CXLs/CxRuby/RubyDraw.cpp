
#include "Intern.hpp"

#include "RubyDraw.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPoint.hpp"

#include "RubyPath.hpp"

#include "RubyTrig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Recursion Traps
//

#if defined(_DEBUG)

#define AfxEnterRecursion(k)	static int r_depth=0;		\
				static int m_depth=0;		\
				r_depth++;			\
				MakeMax(m_depth, r_depth);	\
				AfxAssert(r_depth<(k))		\

#define AfxLeaveRecursion()	r_depth--			\

#else

#define AfxEnterRecursion(k)	((void) 0)

#define AfxLeaveRecursion()	((void) 0)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Drawing Object
//

// Quality Constant

#define quality number(16)

// Drawing Methods

void CRubyDraw::Arc(CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, number r, number scale)
{
	// Draw a circular arc centered on cp with a radius of r and end points
	// of p1 and p2. Call the routine that does the work, and then add the
	// otherwise ommitted end point p2.

	ArcSeg(output, cp, p1, p2, r, scale);

	output.Append(p2);
	}

void CRubyDraw::Arc(CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, number scale)
{
	// Draw an ellipical arc centered on cp with x and y radii of r and
	// end points of p1 and p2. Call the routine that does the work, and
	// then add the otherwise ommitted end point p2.

	if( num_equal(r.m_x, r.m_y) ) {

		AfxAssert(num_equal((p1-cp).GetLength(), fabs(r.m_x)));

		AfxAssert(num_equal((p2-cp).GetLength(), fabs(r.m_x)));

		ArcSeg(output, cp, p1, p2, r.m_x, scale);
		}
	else
		ArcSeg(output, cp, p1, p2, r, scale);

	output.Append(p2);
	}

void CRubyDraw::ArcSeg(CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, number r, number scale)
{
	AfxEnterRecursion(20);

	// Draw a circular arc centered on cp with a radius of r and end points
	// of p1 and p2. We don't actually emit p2, but leave that to the caller
	// if they need it. The arguments over-determine the result and the code
	// will loop forever if they are not consistent i.e. if p1 and p2 are not
	// really on the circle of radius r from cp -- but this method makes the
	// math faster. The algorithm recursively chops the arc in half until the
	// two end points are close enough.

	// Find the vector between the end points.

	CRubyVector vn = p2 - p1;

	// Find the square of its length. We use the square to
	// avoid an early square root that might be avoidable

	number      ds = vn.GetSquare();

	// If the length is too long, we need to split the arc. We compare against
	// a pre-squared quality factor multiplied by a user-supplied scale that
	// represents the inverse square of the scale factor that will be applied
	// when converting to GDI pixels. This ensures that we get an appropriate
	// number of points for the eventual output format.

	if( ds > quality * scale ) {

		// Convert the vector to a unit vector at right angles to the
		// original. If we scale this by the radiuis and start at the
		// center point, we'll end up with a point on the circle that
		// lies halfway between the end points.

		vn.MakeNormal(ds);

		// Find that point.

		CRubyPoint rm = cp + vn * r;

		// Divide the arc into two segments and try again.

		ArcSeg(output, cp, p1, rm, r, scale);

		ArcSeg(output, cp, rm, p2, r, scale);

		AfxLeaveRecursion();

		return;
		}

	// The points are close together, so emit p1 and we're done.

	output.Append(p1);

	AfxLeaveRecursion();
	}

void CRubyDraw::ArcSeg(CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, number scale)
{
	AfxEnterRecursion(20);

	// Draw an ellipical arc centered on cp with x and y radii of r and
	// end points of p1 and p2. We don't actually emit p2, but leave that
	// to the caller if they need it. The arguments over-determine the
	// result and the code will loop forever if they are not consistent
	// i.e. if p1 and p2 are not really on the ellipse defined by r and
	// cp, but this  makes the math faster. The algorithm recursively
	// chops the arc in half until the two end points are close enough.

	// Find the vector between the end points.

	CRubyVector dv = p2 - p1;

	// Find the square of its length. We use the square to
	// avoid an early square root that might be avoidable

	number      ds = dv.GetSquare();

	// If the length is too long, we need to split the arc. We compare against
	// a pre-squared quality factor multiplied by a user-supplied scale that
	// represents the inverse square of the scale factor that will be applied
	// when converting to GDI pixels. This ensures that we get an appropriate
	// number of points for the eventual output format.

	if( ds > quality * scale ) {

		// Find a point halfway between the end points. You can argue
		// that we should find a point at half the angle but this is
		// a lot quicker and produces good point positioning.

		CRubyPoint  mp = p1 + dv * 0.5;

		// We now need to find a unit length vector from the center
		// point to our middle point. When we're done, this will contain
		// the sin and cos of theta in the polar ellipse equation.

		CRubyVector mv;

		// If the point is coincident with the center point, we are
		// drawing a half ellipse so we have to be careful to avoid
		// a degenerate case that will break our math.

		if( mp == cp ) {

			// We can simply take the vector between the end points
			// and get the normal of that, as this bisects the half
			// ellipse. We can reuse the squared length from above.

			mv = dv;

			mv.MakeNormal(ds);
			}
		else {
			// Otherwise we take the vector from the center point
			// to the middle point and convert that to a unit.

			mv = mp - cp;

			mv.MakeUnit();
			}

		// Find the ellipse radius at the required angle, using the
		// polar ellipse equation. Remeber that mv.m_x is cos theta
		// and mv.m_y is sin theta and that r contains the radii. It
		// would be nice to avoid the extra square root that this
		// step demands, but I'm not sure it's possible.

		number radius = (r.m_x * r.m_y) * num_inv_sqrt( r.m_x * r.m_x * mv.m_y * mv.m_y +
							        r.m_y * r.m_y * mv.m_x * mv.m_x );

		// Find the point that lies on the ellipse.

		CRubyPoint rm = cp + mv * radius;

		// Divide the arc into two segments and try again.

		ArcSeg(output, cp, p1, rm, r, scale);

		ArcSeg(output, cp, rm, p2, r, scale);

		AfxLeaveRecursion();

		return;
		}

	output.Append(p1);

	AfxLeaveRecursion();
	}

void CRubyDraw::Arc(CRubyPath &output, CRubyPoint const &cp, number t1, number t2, CRubyVector const &r, number scale)
{
	// Draw the elliptical arc defined by a center point,
	// two angles and two radii. As with ellipses etc. we
	// have two different methods to choose from.

	if( true ) {

		// This method finds the two end points and call the Arc routine
		// to do the work for us. This produces better output quality.

		CRubyPoint p1(cp + (CRubyVector(t1) << r));

		CRubyPoint p2(cp + (CRubyVector(t2) << r));

		if( t1 < t2 ) {

			Arc(output, cp, p1, p2, +r, scale);
			}
		else
			Arc(output, cp, p1, p2, -r, scale);
		}
	else {
		// This method uses trig at each step of the arc
		// and outputs the appropriate points manually.

		int dt = GetThetaStep(r);

		if( t1 < t2 ) {

			for( number t = t1; t < t2; t += dt ) {

				output.Append(cp + (CRubyVector(t) << r));
				}
			}
		else {
			for( number t = t1; t > t2; t -= dt ) {

				output.Append(cp + (CRubyVector(t) << r));
				}
			}

		output.Append(cp + (CRubyVector(t2) << r));
		}
	}

void CRubyDraw::Bezier(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, CRubyPoint const &p3, CRubyPoint const &p4)
{
	// Draw the Bezier curve defined by the four control points. The
	// algorithm recursively divides the curve in half until a small
	// enough segment can be generated as a pair of straight lines.

	CRubyPoint  m1 = p1 + (p2 - p1) / 2;

	CRubyPoint  m2 = p2 + (p3 - p2) / 2;

	CRubyPoint  m3 = p3 + (p4 - p3) / 2;

	CRubyPoint  n1 = m1 + (m2 - m1) / 2;

	CRubyPoint  n2 = m2 + (m3 - m2) / 2;

	CRubyVector nv = n2 - n1;

	if( nv.GetSquare() > 64 ) {

		CRubyPoint o1 = n1 + nv / 2;
	
		Bezier(output, p1, m1, n1, o1);

		Bezier(output, o1, n2, m3, p4);

		return;
		}
	
	output.Append(p1);

	output.Append(p2);

	output.Append(p3);
	}

void CRubyDraw::Ellipse(CRubyPath &output, CRubyPoint const &cp, CRubyVector const &r, number scale)
{
	// Draw the ellipse defined by a center point and two radii. We
	// delegate the work to the circle routine if possible. If not,
	// we can choose between using two arc segments or a trig loop.

	if( !num_equal(r.m_x, r.m_y) ) {

		if( true ) {

			// The recursive ellipse algorithm appears to be
			// faster on most platforms and produces equally
			// good if not better output than the trig one.

			CRubyPoint p1 = cp;

			CRubyPoint p2 = cp;

			p1.m_x -= r.m_x;

			p2.m_x += r.m_x;

			ArcSeg(output, cp, p1, p2, r, scale);

			ArcSeg(output, cp, p2, p1, r, scale);

			output.AppendHardBreak();
			}
		else {
			// Or we can just use a good old fashioned trig
			// loop based on a step derived from the radii.

			int dt = GetThetaStep(r);

			for( number t = 0; t < 360; t += dt ) {

				output.Append(cp + (r << CRubyVector(t)));
				}

			output.AppendHardBreak();
			}

		return;
		}

	// Hand off to the circle routine.

	Circle(output, cp, r.m_x);
	}

void CRubyDraw::Circle(CRubyPath &output, CRubyPoint const &cp, number r, number scale)
{
	// Draw the circle defined by a center point and a radius,
	// using two arcs to generate the intermediate points.

	CRubyPoint p1 = cp;

	CRubyPoint p2 = cp;

	p1.m_x -= r;

	p2.m_x += r;

	ArcSeg(output, cp, p1, p2, r, scale);

	ArcSeg(output, cp, p2, p1, r, scale);

	output.AppendHardBreak();
	}

void CRubyDraw::Rectangle(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2)
{
	// Draw a simple rectangle.

	CRubyPoint c1, c2, c3, c4;

	c1.m_x = p1.m_x;
	c1.m_y = p1.m_y;

	c2.m_x = p2.m_x;
	c2.m_y = p1.m_y;

	c3.m_x = p2.m_x;
	c3.m_y = p2.m_y;

	c4.m_x = p1.m_x;
	c4.m_y = p2.m_y;

	output.Append(c1);
	output.Append(c2);
	output.Append(c3);
	output.Append(c4);

	output.AppendHardBreak();
	}

void CRubyDraw::Trimmed(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number r, int style, int skip)
{
	// Draw a rectangle where each corner is truncated by a straight
	// line or an inner or outer radius. This version accepts a single
	// radius and can perform slightly faster.

	if( r ) {

		CRubyPoint p3, p4, c1, c2, c3, c4, c5, c6, c7, c8, cp;

		c1.m_x = p1.m_x + r;
		c1.m_y = p1.m_y ;

		c2.m_x = p2.m_x - r;
		c2.m_y = p1.m_y ;

		c3.m_x = p2.m_x ;
		c3.m_y = p1.m_y + r;

		c4.m_x = p2.m_x ;
		c4.m_y = p2.m_y - r;

		c5.m_x = p2.m_x - r;
		c5.m_y = p2.m_y ;

		c6.m_x = p1.m_x + r;
		c6.m_y = p2.m_y ;

		c7.m_x = p1.m_x ;
		c7.m_y = p2.m_y - r;

		c8.m_x = p1.m_x ;
		c8.m_y = p1.m_y + r;

		p3.m_x = p2.m_x;
		p3.m_y = p1.m_y;

		p4.m_x = p1.m_x;
		p4.m_y = p2.m_y;

		////////

		switch( style ) {

			case 0: // Beveled

				output.Append(c2);
				output.Append(c3);
				output.Append(c4);
				output.Append(c5);
				output.Append(c6);
				output.Append(c7);
				output.Append(c8);
				output.Append(c1);

				break;

			case 1: // Rounded

				cp.m_x = c2.m_x;
				cp.m_y = c3.m_y;

				Arc(output, cp, c2, c3, +r);

				cp.m_x = c5.m_x;
				cp.m_y = c4.m_y;

				Arc(output, cp, c4, c5, +r);

				cp.m_x = c6.m_x;
				cp.m_y = c7.m_y;

				Arc(output, cp, c6, c7, +r);
	
				cp.m_x = c1.m_x;
				cp.m_y = c8.m_y;

				Arc(output, cp, c8, c1, +r);

				break;

			case 2: // Filleted

				cp.m_x = c3.m_x;
				cp.m_y = c2.m_y;

				Arc(output, cp, c2, c3, -r);

				cp.m_x = c4.m_x;
				cp.m_y = c5.m_y;

				Arc(output, cp, c4, c5, -r);

				cp.m_x = c7.m_x;
				cp.m_y = c6.m_y;

				Arc(output, cp, c6, c7, -r);
	
				cp.m_x = c8.m_x;
				cp.m_y = c1.m_y;

				Arc(output, cp, c8, c1, -r);

				break;
			}

		output.AppendHardBreak();

		return;
		}

	Rectangle(output, p1, p2);
	}

void CRubyDraw::Trimmed(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, int style, int skip)
{
	// Draw a rectangle where each corner is truncated by a straight
	// line or an inner or outer radius. This version accepts both x
	// and y radii and is slower than the single radius version.

	if( r.m_x && r.m_y ) {

		CRubyPoint c1, c2, c3, c4, c5, c6, c7, c8, p3, p4, cp;

		c1.m_x = p1.m_x + r.m_x;
		c1.m_y = p1.m_y ;

		c2.m_x = p2.m_x - r.m_x;
		c2.m_y = p1.m_y ;

		c3.m_x = p2.m_x ;
		c3.m_y = p1.m_y + r.m_y;

		c4.m_x = p2.m_x ;
		c4.m_y = p2.m_y - r.m_y;

		c5.m_x = p2.m_x - r.m_x;
		c5.m_y = p2.m_y ;

		c6.m_x = p1.m_x + r.m_x;
		c6.m_y = p2.m_y ;

		c7.m_x = p1.m_x ;
		c7.m_y = p2.m_y - r.m_y;

		c8.m_x = p1.m_x ;
		c8.m_y = p1.m_y + r.m_y;

		p3.m_x = p2.m_x;
		p3.m_y = p1.m_y;

		p4.m_x = p1.m_x;
		p4.m_y = p2.m_y;

		////////

		switch( style ) {

			case 0: // Beveled

				if( skip & 1 )
					output.Append(p3);
				else {
					output.Append(c2);
					output.Append(c3);
					}

				if( skip & 2 )
					output.Append(p2);
				else {
					output.Append(c4);
					output.Append(c5);
					}

				if( skip & 4 )
					output.Append(p4);
				else {
					output.Append(c6);
					output.Append(c7);
					}

				if( skip & 8 )
					output.Append(p1);
				else {
					output.Append(c8);
					output.Append(c1);
					}

				break;

			case 1: // Rounded

				if( skip & 1 )
					output.Append(p3);
				else {
					cp.m_x = c2.m_x;
					cp.m_y = c3.m_y;

					Arc(output, cp, c2, c3, +r);
					}

				if( skip & 2 )
					output.Append(p2);
				else {
					cp.m_x = c5.m_x;
					cp.m_y = c4.m_y;

					Arc(output, cp, c4, c5, +r);
					}

				if( skip & 4 )
					output.Append(p4);
				else {
					cp.m_x = c6.m_x;
					cp.m_y = c7.m_y;

					Arc(output, cp, c6, c7, +r);
					}
	
				if( skip & 8 )
					output.Append(p1);
				else {
					cp.m_x = c1.m_x;
					cp.m_y = c8.m_y;

					Arc(output, cp, c8, c1, +r);
					}

				break;

			case 2: // Filleted

				if( skip & 1 )
					output.Append(p3);
				else {
					cp.m_x = c3.m_x;
					cp.m_y = c2.m_y;

					Arc(output, cp, c2, c3, -r);
					}

				if( skip & 2 )
					output.Append(p2);
				else {
					cp.m_x = c4.m_x;
					cp.m_y = c5.m_y;

					Arc(output, cp, c4, c5, -r);
					}

				if( skip & 4 )
					output.Append(p4);
				else {
					cp.m_x = c7.m_x;
					cp.m_y = c6.m_y;

					Arc(output, cp, c6, c7, -r);
					}
	
				if( skip & 8 )
					output.Append(p1);
				else {
					cp.m_x = c8.m_x;
					cp.m_y = c1.m_y;

					Arc(output, cp, c8, c1, -r);
					}

				break;
			}

		output.AppendHardBreak();

		return;
		}

	Rectangle(output, p1, p2);
	}

void CRubyDraw::Line(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2)
{
	// Output a simple line segment.
	
	output.Append(p1);

	output.Append(p2);

	output.AppendHardBreak();
	}

// Drawing Variants

void CRubyDraw::Arc(CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, number r)
{
	Arc(output, cp, p1, p2, r, 1);
	}

void CRubyDraw::Arc(CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r)
{
	Arc(output, cp, p1, p2, r, 1);
	}

void CRubyDraw::ArcSeg(CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, number r)
{
	ArcSeg(output, cp, p1, p2, r, 1);
	}

void CRubyDraw::ArcSeg(CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r)
{
	ArcSeg(output, cp, p1, p2, r, 1);
	}

void CRubyDraw::Arc(CRubyPath &output, CRubyPoint const &cp, number t1, number t2, CRubyVector const &r)
{
	Arc(output, cp, t1, t2, r, 1);
	}

void CRubyDraw::Arc(CRubyPath &output, CRubyPoint const &cp, number t1, number t2, number r)
{
	Arc(output, cp, t1, t2, CRubyVector(r, r), 1);
	}

void CRubyDraw::Ellipse(CRubyPath &output, CRubyPoint const &cp, CRubyVector const &r)
{
	Ellipse(output, cp, r, 1);
	}

void CRubyDraw::Ellipse(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2)
{
	CRubyVector ds = (p2 - p1) / 2;

	CRubyPoint  cp = (p2 + p1) / 2;

	Ellipse(output, cp, ds, 1);
	}

void CRubyDraw::Circle(CRubyPath &output, CRubyPoint const &cp, number r)
{
	Circle(output, cp, r, 1);
	}

void CRubyDraw::Beveled(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number r, int skip)
{
	Trimmed(output, p1, p2, r, 0, skip);
	}

void CRubyDraw::Beveled(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, int skip)
{
	Trimmed(output, p1, p2, r, 0, skip);
	}

void CRubyDraw::Rounded(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number r, int skip)
{
	Trimmed(output, p1, p2, r, 1, skip);
	}

void CRubyDraw::Rounded(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, int skip)
{
	Trimmed(output, p1, p2, r, 1, skip);
	}

void CRubyDraw::Filleted(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number r, int skip)
{
	Trimmed(output, p1, p2, r, 2, skip);
	}

void CRubyDraw::Filleted(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, int skip)
{
	Trimmed(output, p1, p2, r, 2, skip);
	}

// Implementation

int CRubyDraw::GetThetaStep(CRubyVector const &r)
{
	// Return the appropriate angle step for drawing an
	// arc or ellipse of given radii. The heuristic used
	// is rather spurious but it works okay...

	int dt = (2 * 180) / int(max(r.m_x, r.m_y));

	if( dt < 6 ) {

		while( 360 % dt ) {
			
			dt--;
			}
		}
	else
		dt = 6;

	return dt;
	}

// End of File
