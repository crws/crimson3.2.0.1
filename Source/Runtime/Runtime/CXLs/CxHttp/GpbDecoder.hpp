
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_GpbDecoder_HPP

#define INCLUDE_GpbDecoder_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "GpbBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Google Protocol Buffer Decoder
//

class DLLAPI CGpbDecoder : public CGpbBase
{
	public:
		// Operations
		bool Decode(CBuffer *pBuff);
		bool Decode(CByteArray const &Data);
		bool Decode(PCBYTE pData, UINT uData);

		// Field Attributes
		UINT     GetFieldCount(UINT uField) const;
		bool     IsFieldPresent(UINT uField) const;
		bool     IsFieldPresent(UINT uField, UINT uInst) const;
		SINT32   GetFieldAsInt32(UINT uField, UINT uInst, INT32 Default) const;
		SINT64   GetFieldAsInt64(UINT uField, UINT uInst, INT64 Default) const;
		UINT32   GetFieldAsUInt32(UINT uField, UINT uInst, UINT32 Default) const;
		UINT64   GetFieldAsUInt64(UINT uField, UINT uInst, UINT64 Default) const;
		SINT32   GetFieldAsSInt32(UINT uField, UINT uInst, SINT32 Default) const;
		SINT64   GetFieldAsSInt64(UINT uField, UINT uInst, SINT64 Default) const;
		bool     GetFieldAsBool(UINT uField, UINT uInst, bool Default) const;
		int      GetFieldAsEnum(UINT uField, UINT uInst, int Default) const;
		UINT32   GetFieldAsFixed32(UINT uField, UINT uInst, UINT32 Default) const;
		SINT32   GetFieldAsSFixed32(UINT uField, UINT uInst, SINT32 Default) const;
		UINT64   GetFieldAsFixed64(UINT uField, UINT uInst, UINT64 Default) const;
		SINT64   GetFieldAsSFixed64(UINT uField, UINT uInst, SINT64 Default) const;
		float    GetFieldAsFloat(UINT uField, UINT uInst, float Default) const;
		double   GetFieldAsDouble(UINT uField, UINT uInst, double Default) const;
		CString  GetFieldAsString(UINT uField, UINT uInst, CString Default) const;
		CUnicode GetFieldAsUnicode(UINT uField, UINT uInst, CUnicode Default) const;
		bool     GetMessage(CGpbDecoder &gpb, UINT uField, UINT uInst) const;
		bool     GetMessage(CByteArray &Data, UINT uField, UINT uInst) const;

		// Diagnostics
		void Dump(void);
		
	protected:
		// Field Data
		struct CFieldData
		{
			PCBYTE	m_pData;
			UINT	m_uSize;
			};

		// Field Info
		struct CFieldInfo
		{
			UINT	m_uWire;
			UINT	m_uData;
			UINT	m_uCount;
			};

		// Data Members
		CArray <CFieldData> m_Data;
		CArray <CFieldInfo> m_Info;

		// Implementation
		bool   FindField(PCBYTE &pData, UINT &uSize, UINT uField, UINT uInst, UINT uWire) const;
		bool   FindField(PCBYTE &pData, UINT uField, UINT uInst, UINT uWire) const;
		UINT32 GetVarInt32(PCBYTE &p) const;
		UINT64 GetVarInt64(PCBYTE &p) const;
		SINT32 GetZigZag32(PCBYTE &p) const;
		SINT64 GetZigZag64(PCBYTE &p) const;
		void   PrintHex(UINT64 v) const;
	};

// End of File

#endif
