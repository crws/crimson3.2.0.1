
#include "Intern.hpp"

#include "HttpStreamWriteFile.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Http File Writable Stream
//

// Constructor

CHttpStreamWriteFile::CHttpStreamWriteFile(CString const &Name, FILE *pFile) : m_Name(Name)
{
	m_pFile = pFile;

	StdSetRef();
}

CHttpStreamWriteFile::CHttpStreamWriteFile(FILE *pFile)
{
	m_pFile = pFile;

	StdSetRef();
}

// Destructor

CHttpStreamWriteFile::~CHttpStreamWriteFile(void)
{
	Abort();
}

// IUnknown

HRESULT CHttpStreamWriteFile::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IHttpStreamWrite);

	StdQueryInterface(IHttpStreamWrite);

	return E_NOINTERFACE;
}

ULONG CHttpStreamWriteFile::AddRef(void)
{
	StdAddRef();
}

ULONG CHttpStreamWriteFile::Release(void)
{
	StdRelease();
}

// IHttpStreamWrite

BOOL CHttpStreamWriteFile::Write(PCTXT pData, UINT uData)
{
	if( m_pFile ) {

		return fwrite(pData, 1, uData, m_pFile) == uData;
	}

	return FALSE;
}

void CHttpStreamWriteFile::Finalize(void)
{
	if( m_pFile ) {

		fflush(m_pFile);

		ftruncate(fileno(m_pFile), ftell(m_pFile));

		fclose(m_pFile);

		m_pFile = NULL;
	}
}

void CHttpStreamWriteFile::Abort(void)
{
	if( m_pFile ) {

		fclose(m_pFile);

		if( !m_Name.IsEmpty() ) {

			unlink(m_Name);
		}
	}
}

// End of File
