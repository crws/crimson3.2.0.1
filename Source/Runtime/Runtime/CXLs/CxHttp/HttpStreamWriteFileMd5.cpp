
#include "Intern.hpp"

#include "HttpStreamWriteFileMd5.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Http File Writable Stream with MD5 Hash
//

// Constructor

CHttpStreamWriteFileMd5::CHttpStreamWriteFileMd5(CString const &Name, FILE *pFile) : CHttpStreamWriteFile(Name, pFile)
{
	m_pHash = NULL;

	AfxNewObject("hash-md5", ICryptoHash, m_pHash);

	m_pHash->Initialize();
}

CHttpStreamWriteFileMd5::CHttpStreamWriteFileMd5(FILE *pFile) : CHttpStreamWriteFile(pFile)
{
	m_pHash = NULL;

	AfxNewObject("hash-md5", ICryptoHash, m_pHash);

	m_pHash->Initialize();
}

// Destructor

CHttpStreamWriteFileMd5::~CHttpStreamWriteFileMd5(void)
{
	AfxRelease(m_pHash);
}

// IUnknown

HRESULT CHttpStreamWriteFileMd5::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IHttpStreamHash);

	return CHttpStreamWriteFile::QueryInterface(riid, ppObject);
}

ULONG CHttpStreamWriteFileMd5::AddRef(void)
{
	return CHttpStreamWriteFile::AddRef();
}

ULONG CHttpStreamWriteFileMd5::Release(void)
{
	return CHttpStreamWriteFile::Release();
}

// IHttpStreamHash

BOOL CHttpStreamWriteFileMd5::GetHash(CString &Hash)
{
	Hash = m_pHash->GetHashString(hashHex);

	return TRUE;
}

// IHttpStreamWrite

BOOL CHttpStreamWriteFileMd5::Write(PCTXT pData, UINT uData)
{
	m_pHash->Update(PCBYTE(pData), uData);

	return CHttpStreamWriteFile::Write(pData, uData);
}

void CHttpStreamWriteFileMd5::Finalize(void)
{
	if( m_pHash ) {

		m_pHash->Finalize();
	}

	CHttpStreamWriteFile::Finalize();
}

void CHttpStreamWriteFileMd5::Abort(void)
{
	CHttpStreamWriteFile::Abort();
}

// End of File
