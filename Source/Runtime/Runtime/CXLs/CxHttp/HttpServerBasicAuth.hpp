
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpServerBasicAuth_HPP

#define	INCLUDE_HttpServerBasicAuth_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpServerAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Basic Authentication Method
//

class DLLAPI CHttpServerBasicAuth : public CHttpServerAuth
{
	public:
		// Constructor
		CHttpServerBasicAuth(CString Realm, UINT uCount);

		// Destructor
		~CHttpServerBasicAuth(void);

		// Operations
		BOOL    CanAccept(CString Meth);
		CString GetAuthHeader(CString Opaque, BOOL fStale);
		UINT	ProcessRequest(CHttpServerRequest *pReq, CString Line);
		CString GetUser(CHttpServerRequest *pReq);
		BOOL	CheckPass(CHttpServerRequest *pReq, CString Pass);

	protected:
		// Data Members
		CString   m_Realm;
		UINT      m_uCount;
		CString * m_pUser;
		CString * m_pPass;
		UINT      m_uIndex;
	};

// End of File

#endif
