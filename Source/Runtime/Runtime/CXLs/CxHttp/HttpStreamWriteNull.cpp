
#include "Intern.hpp"

#include "HttpStreamWriteNull.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Http Null Writable Stream
//

// Constructor

CHttpStreamWriteNull::CHttpStreamWriteNull(void)
{
	StdSetRef();
}

// IUnknown

HRESULT CHttpStreamWriteNull::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IHttpStreamWrite);

	StdQueryInterface(IHttpStreamWrite);

	return E_NOINTERFACE;
}

ULONG CHttpStreamWriteNull::AddRef(void)
{
	StdAddRef();
}

ULONG CHttpStreamWriteNull::Release(void)
{
	StdRelease();
}

// IHttpStreamWrite

BOOL CHttpStreamWriteNull::Write(PCTXT pData, UINT uData)
{
	return TRUE;
}

void CHttpStreamWriteNull::Finalize(void)
{
}

void CHttpStreamWriteNull::Abort(void)
{
}

// End of Null
