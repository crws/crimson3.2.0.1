
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpClientRequest_HPP
	
#define	INCLUDE_HttpClientRequest_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpRequest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class DLLAPI CHttpClientConnection;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Request
//

class DLLAPI CHttpClientRequest : public CHttpRequest
{
	public:
		// Constructor
		CHttpClientRequest(void);

		// Attributes
		BOOL   HasReplyStream(void) const;
		PCBYTE GetReplyBody  (void) const;
		UINT   GetReplySize  (void) const;
		PCTXT  GetReplyHeader(PCTXT pName, UINT uIndex) const;
		PCTXT  GetReplyHeader(PCTXT pName) const;

		// Operations
		void SetReplyStream  (IHttpStreamWrite *pStm);
		void SetRequestBody  (CBytes  Body);
		void SetRequestBody  (CString Body);
		void AddRequestHeader(CString Name, CString Value);

		// Transaction
		BOOL Transact(CHttpClientConnection *pConnect);
		BOOL Transact(CHttpClientConnection *pConnect, UINT uCode);
		BOOL Transact(CHttpClientConnection *pConnect, CString Verb, UINT uCode);
	};

// End of File

#endif
