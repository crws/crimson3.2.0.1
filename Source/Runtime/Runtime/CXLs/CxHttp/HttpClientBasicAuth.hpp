
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpClientBasicAuth_HPP

#define	INCLUDE_HttpClientBasicAuth_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpClientAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Basic Authentication Method
//

class DLLAPI CHttpClientBasicAuth : public CHttpClientAuth
{
	public:
		// Constructor
		CHttpClientBasicAuth(void);

		// Operations
		BOOL	CanAccept(CString Meth, UINT &uPriority);
		BOOL    ProcessRequest(CString Line, BOOL fFail);
		CString GetAuthHeader(CString Verb, CString Path, CBytes Body);

	protected:
		// Data Members
		CString m_Code;
	};

// End of File

#endif
