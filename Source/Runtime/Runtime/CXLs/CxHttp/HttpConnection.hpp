
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpConnection_HPP
	
#define	INCLUDE_HttpConnection_HPP

//////////////////////////////////////////////////////////////////////////
//
// References Classes
//

class     DLLAPI CHttpManager;

class     DLLAPI CHttpRequest;

interface DLLAPI IHttpStreamWrite;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Connection Options
//

class DLLAPI CHttpConnectionOptions : public CBlobbedItem
{
	public:
		// Constructor
		CHttpConnectionOptions(void);

		// Destructor
		~CHttpConnectionOptions(void);

		// Operations
		void DefaultPort(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		BOOL    m_fTls;
		UINT    m_uPort;
		CIpAddr m_Source;
		UINT    m_uSendTimeout;
		UINT    m_uRecvTimeout;
		UINT	m_uCertSource;

	private:
		// No Copying
		CHttpConnectionOptions(CHttpConnectionOptions const &That);
	};

//////////////////////////////////////////////////////////////////////////
//
// HTTP Connection Receieve Codes
//

enum
{
	httpRecvFail,
	httpRecvDone,
	httpRecvMore,
	httpRecvIdle,
	httpRecvNone
	};

//////////////////////////////////////////////////////////////////////////
//
// HTTP Connection
//

class DLLAPI CHttpConnection
{
	public:
		// Constructor
		CHttpConnection(CHttpConnectionOptions const &Opts);

		// Destructor
		~CHttpConnection(void);

		// Configuration
		void SetReplyLimit(UINT uLimit);

		// Attributes
		BOOL               IsOpen(void) const;
		UINT               GetSockState(void) const;
		CString            GetSockPeer(void) const;
		IHttpStreamWrite * GetStream(void) const;

		// Operations
		void Idle(void);
		void Abort(void);
		void Close(void);

	protected:
		// Configuration
		CHttpConnectionOptions const &m_Opts;

		// Data Members
		ISocket		 * m_pSock;
		IHttpStreamWrite * m_pStream;
		IZLib		 * m_pZLib;
		UINT		   m_uLimit;
		PTXT		   m_pData;
		UINT		   m_uSize;
		UINT		   m_uData;
		UINT		   m_uRead;
		UINT		   m_uBody;
		UINT		   m_uDone;
		BOOL		   m_fChunk;
		UINT		   m_uChunkHead;
		UINT		   m_uChunkData;
		UINT		   m_uChunkSize;

		// Overridables
		virtual BOOL OnAcceptPost(IHttpStreamWrite * &pStm, CHttpRequest *pReq, PCTXT pPath);
		virtual void OnFreeSocket(void);

		// Implementation
		void ClearData(void);
		void AddData(CString const &Text);
		void AddData(PCTXT pText);
		void AddData(CBytes Data);
		void AddData(PCBYTE pData, UINT uCount);
		BOOL IsSocketOpen(void);
		BOOL Send(void);
		BOOL Send(CString const &Data);
		BOOL Send(PCBYTE pData, UINT uData);
		void RecvInit(CHttpRequest *pReq);
		UINT RecvData(CHttpRequest *pReq, BOOL fServer);
		BOOL WriteToStream(void);
		void SetNullBody(CHttpRequest *pReq);
		BOOL NoBodyRequired(CHttpRequest *pReq);
		void BuildHeaderMap(CHttpRequest *pReq, PTXT pScan);
		INT  GrowBuffer(UINT uSize);
		INT  GrowBufferAndAdjust(CHttpRequest *pReq, UINT uSize);
		INT  GrowBufferAndAdjust(CHttpRequest *pReq);
		BOOL ProcessBody(CHttpRequest *pReq);
		BOOL ProcessGzip(CHttpRequest *pReq);
		BOOL FindZLib(void);
	};

// End of File

#endif
