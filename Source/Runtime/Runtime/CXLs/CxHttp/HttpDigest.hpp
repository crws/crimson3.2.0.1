
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpDigest_HPP

#define	INCLUDE_HttpDigest_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class DLLAPI CHttpAuth;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Digest Helper
//

class DLLAPI CHttpDigest
{
	public:
		// Constructor
		CHttpDigest(void);

		// Calculation
		CString GetResponse( CString qop,
				     CString Verb,
				     CString Path,
				     PCBYTE  pBody,
				     UINT    uBody,
				     CString User,
				     CString Pass
				     );

		// Operations
		void Clear(void);

		// Data Members
		CString	m_Algorithm;
		CString	m_qop;
		CString	m_Realm;
		CString	m_Opaque;
		CString	m_Nonce;
		CString	m_ClientNonce;
		UINT	m_uNonceCount;

	protected:
		// MD5 Helpers
		CString MD5(CString Data);
		CString MD5(CBytes  Data);
		CString	MD5(PCVOID pData, UINT uSize);
		void    MD5(CString Data, PBYTE pHash);
		void    MD5(PCVOID pData, UINT uSize, PBYTE pHash);

		// Implementation
		CString MakeNonce(void);
	};

// End of File

#endif
