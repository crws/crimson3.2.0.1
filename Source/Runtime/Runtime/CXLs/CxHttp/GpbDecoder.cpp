
#include "Intern.hpp"

#include "GpbDecoder.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Google Protocol Buffer Decoder
//

// Implementation

STRONG_INLINE bool CGpbDecoder::FindField(PCBYTE &pData, UINT &uSize, UINT uField, UINT uInst, UINT uWire) const
{
	if( uField < m_Info.GetCount() ) {

		CFieldInfo const &Info = m_Info[uField];

		if( uInst < Info.m_uCount ) {

			if( Info.m_uWire == uWire ) {

				CFieldData const &Data = m_Data[Info.m_uData + uInst];

				pData = Data.m_pData;

				uSize = Data.m_uSize;

				return true;
				}
			}
		}

	return false;
	}

STRONG_INLINE bool CGpbDecoder::FindField(PCBYTE &pData, UINT uField, UINT uInst, UINT uWire) const
{
	if( uField < m_Info.GetCount() ) {

		CFieldInfo const &Info = m_Info[uField];

		if( uInst < Info.m_uCount ) {

			if( Info.m_uWire == uWire ) {

				CFieldData const &Data = m_Data[Info.m_uData + uInst];

				pData = Data.m_pData;

				return true;
				}
			}
		}

	return false;
	}

STRONG_INLINE UINT32 CGpbDecoder::GetVarInt32(PCBYTE &p) const
{
	UINT32 v = 0;

	UINT   s = 0;

	do {
		v |= UINT32(*p & 0x7F) << s;

		s += 7;

		} while( *p++ & 0x80 );

	return v;
	}

STRONG_INLINE UINT64 CGpbDecoder::GetVarInt64(PCBYTE &p) const
{
	UINT64 v = 0;

	UINT   s = 0;

	do {
		v |= UINT64(*p & 0x7F) << s;

		s += 7;

		} while( *p++ & 0x80 );

	return v;
	}

STRONG_INLINE SINT32 CGpbDecoder::GetZigZag32(PCBYTE &p) const
{
	UINT32 v = GetVarInt32(p);

	return (v & 1) ? -(SINT32(v>>1)+1) : +(SINT32(v>>1));
	}

STRONG_INLINE SINT64 CGpbDecoder::GetZigZag64(PCBYTE &p) const
{
	UINT64 v = GetVarInt64(p);

	return (v & 1) ? -(SINT64(v>>1)+1) : +(SINT64(v>>1));
	}

// Operations

bool CGpbDecoder::Decode(CBuffer *pBuff)
{
	PCBYTE pData = pBuff->GetData();

	UINT   uData = pBuff->GetSize();

	return Decode(pData, uData);
	}

bool CGpbDecoder::Decode(CByteArray const &Data)
{
	PCBYTE pData = Data.GetPointer();

	UINT   uData = Data.GetCount();

	return Decode(pData, uData);
	}

bool CGpbDecoder::Decode(PCBYTE pData, UINT uData)
{
	PCBYTE pDone = pData + uData;

	while( pData < pDone ) {

		BYTE bTag   = *pData++;

		UINT uField = (bTag >> 3);

		UINT uWire  = (bTag &  7);

		CFieldData Data;

		switch( uWire ) {

			case wireVarint:

				Data.m_pData = pData;

				GetVarInt64(pData);

				Data.m_uSize = pData - Data.m_pData;

				break;

			case wire32Bit:

				Data.m_pData = pData;

				Data.m_uSize = 4;

				pData += 4;

				break;

			case wire64Bit:

				Data.m_pData = pData;

				Data.m_uSize = 8;

				pData += 8;

				break;

			case wireLengthDelimited:

				Data.m_uSize = GetVarInt32(pData);

				Data.m_pData = pData;

				pData += Data.m_uSize;

				break;

			default:

				AfxTrace("gpb: unknown wire type %u\n", uWire);

				return false;
			}

		while( uField >= m_Info.GetCount() ) {

			CFieldInfo Info;

			Info.m_uWire  = 0;
			Info.m_uData  = 0;
			Info.m_uCount = 0;

			m_Info.Append(Info);
			}

		CFieldInfo &Info = (CFieldInfo &) m_Info.GetAt(uField);

		if( !Info.m_uCount ) {
 
			Info.m_uWire  = uWire;
			Info.m_uData  = m_Data.GetCount();
			Info.m_uCount = 1;
			}
		else {
			if( Info.m_uWire != uWire ) {

				AfxTrace("gpb: field %u wire type changes from %u to %u\n", uField, Info.m_uWire, uWire);

				return false;
				}

			if( Info.m_uData + Info.m_uCount != m_Data.GetCount() ) {

				// The GPB specification actually allows non-contiguous
				// repeats, but right now we're not seeing them anywhere.

				AfxTrace("gpb: field %u repeats non-contiguously\n", uField);

				return false;
				}

			Info.m_uCount++;
			}

		m_Data.Append(Data);
		}

	return true;
	}

// Field Attributes

UINT CGpbDecoder::GetFieldCount(UINT uField) const
{
	if( uField < m_Info.GetCount() ) {

		return m_Info[uField].m_uCount;
		}

	return 0;
	}

bool CGpbDecoder::IsFieldPresent(UINT uField) const
{
	if( uField < m_Info.GetCount() ) {

		if( m_Info[uField].m_uCount ) {

			return true;
			}
		}

	return false;
	}

bool CGpbDecoder::IsFieldPresent(UINT uField, UINT uInst) const
{
	if( uField < m_Info.GetCount() ) {

		if( uInst < m_Info[uField].m_uCount ) {

			return true;
			}
		}

	return false;
	}

SINT32 CGpbDecoder::GetFieldAsInt32(UINT uField, UINT uInst, INT32 Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wireVarint) ) {

		return SINT32(GetVarInt32(p));
		}

	return Default;
	}

SINT64 CGpbDecoder::GetFieldAsInt64(UINT uField, UINT uInst, INT64 Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wireVarint) ) {

		return SINT64(GetVarInt64(p));
		}

	return Default;
	}

UINT32 CGpbDecoder::GetFieldAsUInt32(UINT uField, UINT uInst, UINT32 Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wireVarint) ) {

		return UINT32(GetVarInt32(p));
		}

	return Default;
	}

UINT64 CGpbDecoder::GetFieldAsUInt64(UINT uField, UINT uInst, UINT64 Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wireVarint) ) {

		return UINT64(GetVarInt64(p));
		}

	return Default;
	}

SINT32 CGpbDecoder::GetFieldAsSInt32(UINT uField, UINT uInst, SINT32 Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wireVarint) ) {

		return GetZigZag32(p);
		}

	return Default;
	}

SINT64 CGpbDecoder::GetFieldAsSInt64(UINT uField, UINT uInst, SINT64 Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wireVarint) ) {

		return GetZigZag64(p);
		}

	return Default;
	}

bool CGpbDecoder::GetFieldAsBool(UINT uField, UINT uInst, bool Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wireVarint) ) {

		return GetVarInt32(p) ? true : false;
		}

	return Default;
	}

int CGpbDecoder::GetFieldAsEnum(UINT uField, UINT uInst, int Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wireVarint) ) {

		return int(GetVarInt32(p));
		}

	return Default;
	}

UINT32 CGpbDecoder::GetFieldAsFixed32(UINT uField, UINT uInst, UINT32 Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wire32Bit) ) {

		return UINT32(IntelToHost(*PDWORD(p)));
		}

	return Default;
	}

SINT32 CGpbDecoder::GetFieldAsSFixed32(UINT uField, UINT uInst, SINT32 Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wire32Bit) ) {

		return SINT32(IntelToHost(*PDWORD(p)));
		}

	return Default;
	}

UINT64 CGpbDecoder::GetFieldAsFixed64(UINT uField, UINT uInst, UINT64 Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wire64Bit) ) {

		return UINT64(IntelToHost(*PINT64(p)));
		}

	return Default;
	}

SINT64 CGpbDecoder::GetFieldAsSFixed64(UINT uField, UINT uInst, SINT64 Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wire64Bit) ) {

		return SINT64(IntelToHost(*PINT64(p)));
		}

	return Default;
	}

float CGpbDecoder::GetFieldAsFloat(UINT uField, UINT uInst, float Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wire32Bit) ) {

		UINT32 v = UINT32(IntelToHost(*PDWORD(p)));

		return *((float *) &v);
		}

	return Default;
	}

double CGpbDecoder::GetFieldAsDouble(UINT uField, UINT uInst, double Default) const
{
	PCBYTE p;

	if( FindField(p, uField, uInst, wire32Bit) ) {

		UINT64 v = UINT64(IntelToHost(*PINT64(p)));

		return *((double *) &v);
		}

	return Default;
	}

CString CGpbDecoder::GetFieldAsString(UINT uField, UINT uInst, CString Default) const
{
	PCBYTE p;

	UINT   n;

	if( FindField(p, n, uField, uInst, wireLengthDelimited) ) {

		CString s(PCTXT(p), n);

		DecodeUtf(s);

		return s;
		}

	return Default;
	}

CUnicode CGpbDecoder::GetFieldAsUnicode(UINT uField, UINT uInst, CUnicode Default) const
{
	PCBYTE p;

	UINT   n;

	if( FindField(p, n, uField, uInst, wireLengthDelimited) ) {

		CString s(PCTXT(p), n);

		return UtfConvert(s);
		}

	return Default;
	}

bool CGpbDecoder::GetMessage(CGpbDecoder &gpb, UINT uField, UINT uInst) const
{
	PCBYTE p;

	UINT   n;

	if( FindField(p, n, uField, uInst, wireLengthDelimited) ) {

		if( gpb.Decode(p, n) ) {

			return true;
			}
		}

	return false;
	}

bool CGpbDecoder::GetMessage(CByteArray &Data, UINT uField, UINT uInst) const
{
	PCBYTE p;

	UINT   n;

	if( FindField(p, n, uField, uInst, wireLengthDelimited) ) {

		Data.Empty();

		Data.Append(p, n);

		return true;
		}

	return false;
	}

// Diagnostics

void CGpbDecoder::Dump(void)
{
	for( UINT n = 0; n < m_Info.GetCount(); n++ ) {

		CFieldInfo const &Info = m_Info[n];

		if( Info.m_uCount ) {

			AfxTrace("    Prop %u has wire type %u\n", n, Info.m_uWire);

			for( UINT m = 0; m < Info.m_uCount; m++ ) {

				CFieldData const &Data = m_Data[Info.m_uData + m];

				PCBYTE p = Data.m_pData;

				UINT   s = Data.m_uSize;

				UINT   c = 0;

				AfxTrace("        Instance %u is ", m);

				switch( Info.m_uWire ) {

					case wireVarint:

						PrintHex(GetVarInt64(p));

						break;

					case wire32Bit:

						PrintHex(IntelToHost(*PDWORD(p)));

						break;

					case wire64Bit:

						PrintHex(IntelToHost(*PINT64(p)));

						break;

					case wireLengthDelimited:

						while( c < s ) {
							
							if( p[c++] < 32 ) {

								// Probably not a string...

								break;
								}
							}

						if( c == s ) {

							AfxTrace("[%s]\n", PCTXT(CString(PCTXT(p), s)));
							}
						else
							AfxTrace("%u bytes\n", s);

						break;

					default:
						
						AfxTrace("Unknown\n");

						break;
					}
				}
			}
		}

	AfxTrace("    Done.\n\n");
	}

// Implementation

void CGpbDecoder::PrintHex(UINT64 v) const
{
	if( v>>48 ) {

		AfxTrace("0x%8.8X%8.8X\n", DWORD(v>>32), DWORD(v));

		return;
		}

	if( v>>32 ) {

		AfxTrace("0x%4.4X%8.8X\n", DWORD(v>>32), DWORD(v));

		return;
		}

	if( v>>16 ) {

		AfxTrace("0x%8.8X\n", DWORD(v));

		return;
		}

	if( v>>8 ) {

		AfxTrace("0x%4.4X\n", DWORD(v));

		return;
		}

	AfxTrace("0x%2.2X\n", DWORD(v));
	}

// End of File
