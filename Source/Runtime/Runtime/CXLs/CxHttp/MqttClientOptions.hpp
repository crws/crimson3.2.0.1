
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClientOptions_HPP

#define	INCLUDE_MqttClientOptions_HPP

//////////////////////////////////////////////////////////////////////////
//
// Mqtt Client Options
//

class DLLAPI CMqttClientOptions : public CBlobbedItem
{
public:
	// Constructor
	CMqttClientOptions(void);

	// Destructor
	~CMqttClientOptions(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Attributes
	BOOL    HasAvailablePeer(void);
	BOOL    HasAlternatePeer(void);
	PCTXT   GetPeerName(void) const;
	IPREF   GetPeerAddr(void) const;
	UINT    GetBackOff(void) const;

	// Operations
	BOOL NextPeer(void);
	void SetGoodPeer(void);

	// Config Fixup
	virtual BOOL FixConfig(void);

	// Data Members
	BOOL	m_fDebug;
	BOOL    m_fTls;
	UINT	m_uIdSrc;
	UINT	m_uCaSrc;
	UINT    m_uCheck;
	UINT	m_uFace;
	UINT    m_uPort;
	CString	m_PeerName[6];
	UINT    m_Balance;
	UINT	m_PubQos;
	UINT	m_SubQos;
	CString	m_ClientId;
	CString m_UserName;
	CString m_Password;
	UINT	m_uKeepAlive;
	UINT    m_uConnTimeout;
	UINT    m_uSendTimeout;
	UINT    m_uRecvTimeout;
	UINT    m_uBackOffTime;
	UINT    m_uBackOffMax;
	BOOL    m_fCleanSession;

protected:
	// Address Entry
	struct CAddr
	{
		UINT    m_uPeer;
		PCTXT   m_pName;
		CIpAddr m_Addr;
	};

	// Data Members
	IDnsResolver * m_pDns;
	CArray <CAddr> m_AddrList;
	UINT	       m_uInitAddr;
	UINT	       m_uThisAddr;
	UINT	       m_uBackFact;
	UINT	       m_uBackCalc;

	// Implementation
	BOOL FindResolver(void);
	BOOL ResolveName(CArray <CIpAddr> &List, PCTXT pName);
	BOOL MakeAddrList(void);
};

// End of File

#endif
