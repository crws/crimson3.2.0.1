
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HTTPURLENCODING_HPP

#define	INCLUDE_HTTPURLENCODING_HPP

//////////////////////////////////////////////////////////////////////////
//
// HTTP URL Encoding
//

class DLLAPI CHttpUrlEncoding
{
	public:		
		// Operations
		static CString Encode(CString const &Text);
		static CString Encode(CString const &Text, BOOL fSpace);
		static CString Encode(CString const &Text, PCTXT pList);
		static CString Decode(CString const &Text);

		// Implementation
		static STRONG_INLINE BYTE FromHex(char cData);
	};

// End of File

#endif
