
#include "Intern.hpp"

#include "HttpServerRequest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpServerConnection.hpp"

#include "HttpUrlEncoding.hpp"

#include "HttpTime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Reply
//

// Constructor

CHttpServerRequest::CHttpServerRequest(void)
{
	m_uVer  = 10;

	m_pAuth = NULL;
	}

// Attributes

PCTXT CHttpServerRequest::GetRequestProtocol(void) const
{
	return m_Prot;
	}

UINT CHttpServerRequest::GetRequestVersion(void) const
{
	return m_uVer;
	}

PCBYTE CHttpServerRequest::GetRequestBody(void) const
{
	return GetRemoteBody();
	}

UINT CHttpServerRequest::GetRequestSize(void) const
{
	return GetRemoteSize();
	}

PCTXT CHttpServerRequest::GetRequestHeader(PCTXT pName, UINT uIndex) const
{
	return GetRemoteHeader(pName, uIndex);
	}

PCTXT CHttpServerRequest::GetRequestHeader(PCTXT pName) const
{
	return GetRemoteHeader(pName);
	}

PVOID CHttpServerRequest::GetAuthContext(void) const
{
	return m_pAuth;
	}

CString CHttpServerRequest::GetFullPath(void) const
{
	return m_Full;
	}

CString CHttpServerRequest::GetContentType(void) const
{
	return m_Type;
	}

PCTXT CHttpServerRequest::GetCookieHeader(void) const
{
	return m_Cookie;
}

// Parameters

BOOL CHttpServerRequest::HasParam(PCTXT pName) const
{
	INDEX n = m_Params.FindName(pName);

	return !m_Params.Failed(n);
	}

CString CHttpServerRequest::GetParamString(PCTXT pName, CString Default) const
{
	INDEX n = m_Params.FindName(pName);

	return m_Params.Failed(n) ? Default : m_Params.GetData(n);
	}

UINT CHttpServerRequest::GetParamDecimal(PCTXT pName, UINT uDefault) const
{
	INDEX n = m_Params.FindName(pName);

	return m_Params.Failed(n) ? uDefault : strtoul(m_Params.GetData(n), NULL, 10);
	}

UINT CHttpServerRequest::GetParamHex(PCTXT pName, UINT uDefault) const
{
	INDEX n = m_Params.FindName(pName);

	return m_Params.Failed(n) ? uDefault : strtoul(m_Params.GetData(n), NULL, 16);
	}

// Operations

void CHttpServerRequest::SetRequestInfo(CString Line)
{
	m_Verb = Line.StripToken(' ');

	m_Path = Line.StripToken(' ');

	m_Full = m_Path;

	m_Prot = Line.StripToken('/');

	m_uVer = 10     * atoi(Line.StripToken('.'));
	
	m_uVer = m_uVer + atoi(Line);

	ParseParams();
	}

void CHttpServerRequest::SetReplyBody(PCBYTE pData, UINT uData)
{
	SetLocalBody(pData, uData);
	}

void CHttpServerRequest::SetReplyBody(CBytes Body)
{
	SetLocalBody(Body);
	}

void CHttpServerRequest::SetReplyBody(CString Body)
{
	SetLocalBody(Body);
	}

void CHttpServerRequest::SetContentType(CString Type)
{
	m_Type = Type;
	}

void CHttpServerRequest::AddReplyHeader(CString Name, CString Value)
{
	AddLocalHeader(Name, Value);
	}

void CHttpServerRequest::SetCookieHeader(CString Value)
{
	m_Cookie  = "Set-Cookie: ";

	m_Cookie  += Value;

	m_Cookie  += "\r\n";
}

void CHttpServerRequest::SetAuthContext(PVOID pAuth)
{
	m_pAuth = pAuth;
	}

// Cache Helpers

void CHttpServerRequest::AddCacheInfo(time_t timeCreated, time_t timeExpires, CString Tag)
{
	if( timeExpires ) {

		AddReplyHeader("Expires", CHttpTime::Format(timeExpires));
		}

	AddReplyHeader("Cache-Control", "public");

	AddReplyHeader("Last-Modified", CHttpTime::Format(timeCreated));

	AddReplyHeader("ETag",          Tag);
	}

BOOL CHttpServerRequest::IsUnchanged(time_t timeModified, CString Tag)
{
	PCTXT pTime = GetRequestHeader("If-Modified-Since");

	PCTXT pTag  = GetRequestHeader("If-None-Match");

	if( *pTime && *pTag ) {

		if( !strcasecmp(Tag, pTag) ) {

			if( timeModified <= CHttpTime::Parse(pTime) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Implementation

BOOL CHttpServerRequest::ParseParams(void)
{
	CString Line = m_Path;

	m_Path       = Line.StripToken('?');

	if( !Line.IsEmpty() ) {

		CStringArray List;

		Line.Tokenize(List, '&', '"');

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CString Data = List[n];

			CString Name = Data.StripToken('=');

			if( Data.GetLength() ) {

				if( true ) {

					// Do this first as Chrome likes to escape
					// the quotation marks around our data!

					Data = CHttpUrlEncoding::Decode(Data);
					}

				if( Data[0] == '"' ) {

					Data = Data.Mid(1, Data.GetLength() - 2);
					}
				}

			m_Params.Insert(Name, Data);
			}

		return TRUE;
		}

	return FALSE;
	}

// End of File
