
#include "Intern.hpp"

#include "RestCall.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cumulocity Cloud Interface
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// REST Call
//

// Constructor

CRestCall::CRestCall(void)
{
	}

CRestCall::CRestCall(CString Verb, CString Path, CString ReqType, CString RepType)
{
	m_Http.SetVerb(Verb);
	
	m_Http.SetPath(Path);

	SetReqType(ReqType);

	SetRepType(RepType);
	}

CRestCall::CRestCall(CString Verb, CString Path, CString Type)
{
	m_Http.SetVerb(Verb);
	
	m_Http.SetPath(Path);

	SetType(Type);
	}

CRestCall::CRestCall(CString Verb, CString Path)
{
	m_Http.SetVerb(Verb);
	
	m_Http.SetPath(Path);
	}

CRestCall::CRestCall(CString Path)
{
	m_Http.SetPath(Path);
	}

// Destructor

CRestCall::~CRestCall(void)
{
	}

// Attributes

UINT CRestCall::GetStatus(void) const
{
	return m_Http.GetStatus();
	}

// JSON Access

CJsonData * CRestCall::GetJsonReq(void)
{
	return &m_JsonReq;
	}

CJsonData * CRestCall::GetJsonRep(void)
{
	return &m_JsonRep;
	}

// Operations

void CRestCall::SetVerb(CString Verb)
{
	m_Http.SetVerb(Verb);
	}

void CRestCall::SetPath(CString Path)
{
	m_Http.SetPath(Path);
	}

BOOL CRestCall::SetReqType(CString ReqType)
{
	if( !ReqType.IsEmpty() ) {
	
		m_Http.AddRequestHeader("Content-Type", ReqType);

		return TRUE;
		}

	return FALSE;
	}

BOOL CRestCall::SetRepType(CString RepType)
{
	if( !RepType.IsEmpty() ) {
	
		m_Http.AddRequestHeader("Accept", RepType);

		return TRUE;
		}

	return FALSE;
	}

BOOL CRestCall::SetType(CString Type)
{
	if( !Type.IsEmpty() ) {
	
		m_Http.AddRequestHeader("Content-Type", Type);

		m_Http.AddRequestHeader("Accept",       Type);

		return TRUE;
		}

	return FALSE;
	}

// Transaction

BOOL CRestCall::Transact(CHttpClientConnection *pConnect, UINT uStatus)
{
	if( Transact(pConnect) ) {

		if( GetStatus() == uStatus ) {

			return TRUE;
			}

		/*AfxTrace("FAIL: Got %u instead of expected %u.\n\n", GetStatus(), uStatus);*/
		}

	return FALSE;
	}

BOOL CRestCall::Transact(CHttpClientConnection *pConnect)
{
	if( !m_JsonReq.IsEmpty() ) {

		m_Http.SetRequestBody(m_JsonReq.GetAsText(FALSE));
		}

	if( m_Http.Transact(pConnect) ) {

		// NOTE -- To use these traces, you will have to change the
		// buffer in the cops trace.cpp to static and 32K in size.

		/*
		AfxTrace( "%4.4u :      : SEND : %-4s %s\n", 
			  GetNow() % 10000,
			  PCTXT(m_Http.GetVerb()),
			  PCTXT(m_Http.GetPath()),
			  PCTXT(m_JsonReq.GetAsText(TRUE))
			  );*/

		/*
		AfxTrace( "SEND:\n  %s %s\n%s\n",
			  PCTXT(m_Http.GetVerb()),
			  PCTXT(m_Http.GetPath()),
			  PCTXT(m_JsonReq.GetAsText(TRUE))
			  );*/

		m_JsonRep.Parse(PCTXT(m_Http.GetReplyBody()));

		/*
		AfxTrace( "RECV:\n  %u\n%s\n",
			   m_Http.GetStatus(),
			   PCTXT(m_JsonRep.GetAsText(TRUE))
			   );*/

		return TRUE;
		}

	/*AfxTrace("FAIL: Cannot perform transaction.\n\n");*/

	return FALSE;
	}

// End of File
