
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_BASE64_HPP

#define	INCLUDE_BASE64_HPP

//////////////////////////////////////////////////////////////////////////
//
// Base-64 Encoder 
//

class DLLAPI CHttpBase64
{
	public:		
		// Attributes
		static UINT GetEncodeSize(UINT uSize);
		static UINT GetDecodeSize(CString const &Text);
		static UINT GetDecodeSize(PCTXT pText);

		// Operations
		static BOOL    Encode(PTXT pBuff, PCBYTE pData, UINT uSize, BOOL fBreak);
		static PTXT    Encode(PCBYTE pData, UINT uSize, BOOL fBreak);
		static CString Encode(PCBYTE pData, UINT uSize);
		static CString Encode(CString const &Text);
		static BOOL    Decode(PBYTE pBuff, PCTXT pText, UINT uText);
		static BOOL    Decode(PBYTE pBuff, CString const &Text);
		static BOOL    Decode(PBYTE pBuff, PCTXT pText);
		static PBYTE   Decode(CString const &Text, UINT uExtra = 0);
		static PBYTE   Decode(PCTXT pText, UINT uExtra = 0);

	protected:
		// Implementation
		static STRONG_INLINE BOOL IsBase64(BYTE cData);
		static STRONG_INLINE BYTE ByteEncode(BYTE bData);
		static STRONG_INLINE BYTE ByteDecode(BYTE cData);
	};

// End of File

#endif
