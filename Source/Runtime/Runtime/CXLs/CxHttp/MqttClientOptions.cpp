
#include "Intern.hpp"

#include "MqttClientOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client Options
//

// Constructor

CMqttClientOptions::CMqttClientOptions(void)
{
	m_fDebug        = FALSE;

	m_fTls          = TRUE;

	m_uIdSrc	= 0;

	m_uCaSrc	= 0;

	m_uCheck	= 0;

	m_uFace		= 0;

	m_uPort         = 8883;

	m_Balance       = 0;

	m_PubQos        = 1;

	m_SubQos        = 1;

	m_uConnTimeout  = 30 * 1000;

	m_uSendTimeout  = 15 * 1000;

	m_uRecvTimeout  = 15 * 1000;

	m_uBackOffTime  = 5;

	m_uBackOffMax   = 120;

	m_fCleanSession = TRUE;

	m_uKeepAlive    = 600;

	m_pDns          = NULL;

	m_uThisAddr     = NOTHING;

	m_uInitAddr     = NOTHING;

	m_uBackFact     = 0;
}

// Destructor

CMqttClientOptions::~CMqttClientOptions(void)
{
	AfxRelease(m_pDns);
}

// Initialization

void CMqttClientOptions::Load(PCBYTE &pData)
{
	GetWord(pData);

	m_fDebug = GetByte(pData);

	m_fTls   = GetByte(pData);

	m_uIdSrc = GetWord(pData);

	m_uCaSrc = GetWord(pData);

	m_uCheck = GetByte(pData);

	m_uFace  = GetWord(pData);

	m_uPort  = GetWord(pData);

	GetCoded(pData, m_PeerName[0]);

	GetCoded(pData, m_PeerName[1]);

	GetCoded(pData, m_PeerName[2]);

	GetCoded(pData, m_PeerName[3]);

	GetCoded(pData, m_PeerName[4]);

	GetCoded(pData, m_PeerName[5]);

	m_PubQos   = GetByte(pData);

	m_SubQos   = GetByte(pData);

	GetCoded(pData, m_ClientId);

	GetCoded(pData, m_UserName);

	GetCoded(pData, m_Password);

	m_uConnTimeout  = GetWord(pData);

	m_uSendTimeout  = GetWord(pData);

	m_uRecvTimeout  = GetWord(pData);

	m_uBackOffTime  = GetWord(pData);

	m_uBackOffMax   = GetWord(pData);

	m_uKeepAlive    = GetWord(pData);

	m_fCleanSession = GetByte(pData) ? FALSE : TRUE;
}

// Attributes

BOOL CMqttClientOptions::HasAvailablePeer(void)
{
	if( m_AddrList.GetCount() == 0 ) {

		// If we have no peers available, check the DNS again
		// before we give up and admit this fact to the client.

		MakeAddrList();
	}

	return m_AddrList.GetCount() > 0;
}

BOOL CMqttClientOptions::HasAlternatePeer(void)
{
	if( m_AddrList.GetCount() == 1 ) {

		CAddr Keep = m_AddrList[0];

		// If we only have one peer, we check to see if other
		// options have become available before just banging
		// our head again the current one forever.

		if( !MakeAddrList() ) {

			// Oh dear! In our attempt to expand our address
			// list, we ended up dropping the one IP that we
			// had actually managed to use! So we sneakily
			// add it back to the list and reset everything
			// to the state we were previously using.

			m_AddrList.Append(Keep);

			m_uInitAddr = 0;

			m_uThisAddr = 0;

			return FALSE;
		}
	}

	return m_AddrList.GetCount() > 1;
}

PCTXT CMqttClientOptions::GetPeerName(void) const
{
	return m_AddrList[m_uThisAddr].m_pName;
}

IPREF CMqttClientOptions::GetPeerAddr(void) const
{
	return m_AddrList[m_uThisAddr].m_Addr;
}

UINT CMqttClientOptions::GetBackOff(void) const
{
	return min(m_uBackOffTime * m_uBackFact, m_uBackOffMax);
}

// Operations

BOOL CMqttClientOptions::NextPeer(void)
{
	// Move on to the next peer, and if we end up back
	// at where we started, update the back-off delay
	// and let our caller know to wait.

	if( (m_uThisAddr = (m_uThisAddr + 1) % m_AddrList.GetCount()) == m_uInitAddr ) {

		// Increase the delay exponentially.

		m_uBackFact = m_uBackFact ? (m_uBackFact << 1) : 1;

		// Since at this point we've failed on all our available
		// IP addresses, this is a good point to regenerate the
		// list to see if anything new has become available.

		MakeAddrList();

		return TRUE;
	}

	return FALSE;
}

void CMqttClientOptions::SetGoodPeer(void)
{
	// We managed to talk to this address, so save it such that if
	// we lose connectivity, returning to this one will be the point
	// at which we triggered the back-off delay.

	m_uInitAddr = m_uThisAddr;

	m_uBackFact = 0;
}

// Config Fixup

BOOL CMqttClientOptions::FixConfig(void)
{
	return TRUE;
}

// Implementation

BOOL CMqttClientOptions::FindResolver(void)
{
	if( !m_pDns ) {

		AfxGetObject("dns", 0, IDnsResolver, m_pDns);
	}

	return m_pDns ? TRUE : FALSE;
}

BOOL CMqttClientOptions::ResolveName(CArray <CIpAddr> &List, PCTXT pName)
{
	return m_pDns->Resolve(List, pName);
}

BOOL CMqttClientOptions::MakeAddrList(void)
{
	if( FindResolver() ) {

		// Save the addresses referenced by the two
		// address indices that we use internally.

		CAddr InitAddr = { NOTHING };

		CAddr ThisAddr = { NOTHING };

		if( m_uInitAddr < m_AddrList.GetCount() ) {

			InitAddr = m_AddrList[m_uInitAddr];
		}

		if( m_uThisAddr < m_AddrList.GetCount() ) {

			ThisAddr = m_AddrList[m_uThisAddr];
		}

		// Reset them to empty and empty the list.

		m_uInitAddr = NOTHING;

		m_uThisAddr = NOTHING;

		m_AddrList.Empty();

		// Visit each available peer name.

		for( UINT uPeer = 0; uPeer < elements(m_PeerName); uPeer++ ) {

			if( !m_PeerName[uPeer].IsEmpty() ) {

				// Set up the address structure.

				CAddr Addr;

				Addr.m_uPeer = uPeer;

				Addr.m_pName = m_PeerName[uPeer];

				// And get a list of associated IPs.

				CArray <CIpAddr> List;

				if( ResolveName(List, Addr.m_pName) ) {

					if( !m_Balance ) {

						// If we're not load balancing, shuffle this
						// list so within each peer we walk through
						// randomized addresses. If we're balancing,
						// we'll shuffle the whole list later.

						List.Shuffle(0);
					}

					for( UINT n = 0; n < List.GetCount(); n++ ) {

						// Add each IP to our candidate address list.

						Addr.m_Addr = List[n];

						m_AddrList.Append(Addr);
					}
				}
			}
		}

		if( !m_AddrList.IsEmpty() ) {

			// Check if we are load balancing.

			if( m_Balance ) {

				// If so, we're going to want to shuffle the
				// entire list to make sure we work our way
				// through the possible IPs randomly.

				m_AddrList.Shuffle(0);
			}

			// Scan the list to fix-up our previous references.

			for( UINT n = 0; n < m_AddrList.GetCount(); n++ ) {

				CAddr const &Addr = m_AddrList[n];

				// If the entry matches either of the records referenced
				// by our two address indices, restore these to point
				// to the appropriate entry. We have to go through
				// all this work as they might have changed position!

				if( Addr.m_uPeer == InitAddr.m_uPeer && Addr.m_Addr == InitAddr.m_Addr ) {

					m_uInitAddr = n;
				}

				if( Addr.m_uPeer == ThisAddr.m_uPeer && Addr.m_Addr == ThisAddr.m_Addr ) {

					m_uThisAddr = n;
				}
			}

			// If we've not got a current address, start at the
			// beginning of the list. If we're load balancing,
			// this is a random IP from all peers, otherwise it's
			// a random IP from the primary peer.

			if( m_uThisAddr == NOTHING ) {

				m_uThisAddr = 0;

				m_uInitAddr = m_uThisAddr;
			}

			// If we lost our scan start, just reset it to the
			// currently used address. This will not happen too
			// often, but it keeps things sane just in case.

			if( m_uInitAddr == NOTHING ) {

				m_uInitAddr = m_uThisAddr;
			}

			return TRUE;
		}
	}

	return FALSE;
}

// End of File
