
#include "Intern.hpp"

#include "HttpServerDigestAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpServerDigest.hpp"

#include "HttpServerConnection.hpp"

#include "HttpServerRequest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Digest Authentication Method
//

// Constructor

CHttpServerDigestAuth::CHttpServerDigestAuth(CString Realm, UINT uCount)
{
	m_pHead = NULL;

	m_pTail = NULL;

	for( UINT n = 0; n < uCount; n++ ) {

		CHttpServerDigest *pDig = New CHttpServerDigest;

		AfxListAppend(m_pHead, m_pTail, pDig, m_pNext, m_pPrev);

		pDig->m_Realm = Realm;
		}

	/*AddDiagCmds(); !!!*/
	}

// Destructor

CHttpServerDigestAuth::~CHttpServerDigestAuth(void)
{
	while( m_pHead ) {

		CHttpServerDigest *pDig = m_pHead;

		AfxListRemove(m_pHead, m_pTail, pDig, m_pNext, m_pPrev);

		delete pDig;
		}
	}

// Operations

BOOL CHttpServerDigestAuth::CanAccept(CString Meth)
{
	return Meth == "Digest";
	}

CString CHttpServerDigestAuth::GetAuthHeader(CString Opaque, BOOL fStale)
{
	Lock();

	// You would think we could find the existing digest context
	// for the session that we're using, and share that between
	// connections. But while IE and Chrome handle this fine, the
	// evil that is Safari gets very confused...
	//
	// So we're always going to use the head entry. It'll either
	// be empty, or it will be the LR and we'll just overwrite
	// it with the new data. We move it to the end of the list
	// to maintain the MRU ordering and allow easier location.

	CHttpServerDigest *pDig = m_pHead;

	AfxListRemove(m_pHead, m_pTail, pDig, m_pNext, m_pPrev);

	AfxListAppend(m_pHead, m_pTail, pDig, m_pNext, m_pPrev);

	pDig->UpdateNonce(Opaque);

	Free();

	// Now we can build the actual header.
	
	CString Head;

	Head += fStale ? "stale=\"true\"," : "";

	Head += "algorithm=\"md5-sess\",";

	Head += "qop=\"auth,auth-int\",";

	Head += "realm=\""  + pDig->m_Realm  + "\",";

	Head += "nonce=\""  + pDig->m_Nonce  + "\",";

	Head += "opaque=\"" + pDig->m_Opaque + "\"";

	return "WWW-Authenticate: Digest " + Head + "\r\n";
	}

UINT CHttpServerDigestAuth::ProcessRequest(CHttpServerRequest *pReq, CString Line)
{
	Lock();

	if( ParseLine(Line) ) {

		CString Nonce = GetParam("nonce");

		// We keep the MRU entries at the tail of the list, so we
		// will start searching for the nonce from that end to try
		// and find it more quickly. We could implement an index
		// to speed it up, but most of the time, this will be fine.

		for( CHttpServerDigest *pDig = m_pTail; pDig; pDig = pDig->m_pPrev ) {

			if( pDig->MatchNonce(Nonce) ) {

				if( pDig != m_pTail ) {

					// Put the entry at the end of the list to maintain MRU.

					AfxListRemove(m_pHead, m_pTail, pDig, m_pNext, m_pPrev);

					AfxListAppend(m_pHead, m_pTail, pDig, m_pNext, m_pPrev);
					}

				pReq->SetAuthContext(pDig);

				UINT uCode = pDig->CheckRequest(this);

				Free();

				return uCode;
				}
			}

		return CHttpServerConnection::authStale;
		}

	Free();

	return CHttpServerConnection::authFail;
	}

CString CHttpServerDigestAuth::GetUser(CHttpServerRequest *pReq)
{
	CHttpServerDigest *pDig = (CHttpServerDigest *) pReq->GetAuthContext();

	if( pDig ) {

		return pDig->m_User;
		}
	
	return "";
	}

BOOL CHttpServerDigestAuth::CheckPass(CHttpServerRequest *pReq, CString Pass)
{
	CHttpServerDigest *pDig = (CHttpServerDigest *) pReq->GetAuthContext();

	if( pDig ) {

		CString Digest = pDig->GetResponse( pDig->m_qop,
						    pReq->GetVerb(),
						    pReq->GetFullPath(),
						    pReq->GetRemoteBody(),
						    pReq->GetRemoteSize(),
						    pDig->m_User,
						    Pass
						    );

		return Digest == pDig->m_Digest;
		}

	return FALSE;
	}

void CHttpServerDigestAuth::ClearSession(CString Opaque)
{
	CHttpServerDigest *pDig = m_pHead;

	while( pDig ) {

		if( pDig->MatchOpaque(Opaque) ) {

			pDig->Clear();
			}

		pDig = pDig->m_pNext;
		}
	}

// IDiagProvider

/*UINT CHttpServerDigestAuth::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStatus(pOut, pCmd);
		}

	return 0;
	}*/

// Implementation

void CHttpServerDigestAuth::Lock(void)
{
	// TODO -- Implement.
	}

void CHttpServerDigestAuth::Free(void)
{
	// TODO -- Implement.
	}

// Diagnostics

/*bool CHttpServerDigestAuth::AddDiagCmds(void)
{
	IDiagManager *pDiag = AfxGetSingleton(IDiagManager);

	if( pDiag ) {

		UINT uProv = pDiag->RegProvider(this, "w3sda");

		pDiag->RegCommand(uProv, 1, "status");

		return true;
		}

	return false;
	}

UINT CHttpServerDigestAuth::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(6);

		pOut->SetColumn(0, "Context",   "%8.8X");
		pOut->SetColumn(1, "User",      "%s");
		pOut->SetColumn(2, "Algorithm", "%s");
		pOut->SetColumn(3, "Opaque",    "%s");
		pOut->SetColumn(4, "Nonce",     "%s");
		pOut->SetColumn(5, "NC",        "%u");

		pOut->AddHead();

		pOut->AddRule('-');

		CHttpServerDigest *pDig = m_pTail;

		while( pDig ) {

			if( !pDig->m_Nonce.IsEmpty() ) {
		
				pOut->AddRow();

				pOut->SetData(0, pDig);
				pOut->SetData(1, PCTXT(pDig->m_User));
				pOut->SetData(2, PCTXT(pDig->m_Algorithm));
				pOut->SetData(3, PCTXT(pDig->m_Opaque));
				pOut->SetData(4, PCTXT(pDig->m_Nonce));
				pOut->SetData(5, pDig->m_uNonceCount);
		
				pOut->EndRow();
				}

			pDig = pDig->m_pPrev;
			}

		pOut->AddRule('-');
		
		pOut->EndTable();

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}*/

// End of File
