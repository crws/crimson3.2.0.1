
#include "Intern.hpp"

#include "HttpRequest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Request
//

// Constructor

CHttpRequest::CHttpRequest(void)
{
	m_uStatus  = 0;

	m_pRemBody = PCBYTE("");

	m_uRemBody = 0;

	m_pRemStm  = NULL;
	}

// Attributes

CString CHttpRequest::GetVerb(void) const
{
	return m_Verb;
	}

CString CHttpRequest::GetPath(void) const
{
	return m_Path;
	}

UINT CHttpRequest::GetStatus(void) const
{
	return m_uStatus;
	}

// Operations

void CHttpRequest::SetVerb(CString Verb)
{
	m_Verb = Verb;
	}

void CHttpRequest::SetPath(CString Path)
{
	m_Path = Path;
	}

void CHttpRequest::SetStatus(UINT uStatus)
{
	m_uStatus = uStatus;
	}

// Attributes

CBytes CHttpRequest::GetLocalBody(void) const
{
	return m_LocBody;
	}

CString CHttpRequest::GetLocalHeader(void) const
{
	return m_LocHead;
	}

BOOL CHttpRequest::GetRemoteStream(IHttpStreamWrite * &pStm) const
{
	return (pStm = m_pRemStm) ? TRUE : FALSE;
	}

PCBYTE CHttpRequest::GetRemoteBody(void) const
{
	return m_pRemBody;
	}

UINT CHttpRequest::GetRemoteSize(void) const
{
	return m_uRemBody;
	}

PCTXT CHttpRequest::GetRemoteHeader(PCTXT pName, UINT uIndex) const
{
	if( uIndex ) {

		CPrintf Name("%s%c", pName, '@' + uIndex);

		return GetRemoteHeader(Name);
		}

	return GetRemoteHeader(pName);
	}

PCTXT CHttpRequest::GetRemoteHeader(PCTXT pName) const
{
	INDEX n = m_RemHead.FindName(pName);

	if( m_RemHead.Failed(n) ) {

		return "";
		}

	return m_RemHead.GetData(n);
	}

// Remote Data

void CHttpRequest::ClearRemoteData(void)
{
	m_pRemBody = PCBYTE("");

	m_uRemBody = 0;

	m_RemHead.Empty();
	}

void CHttpRequest::SetRemoteBody(PCBYTE pBody, UINT uBody)
{
	static BYTE bom[] = { 0xEF, 0xBB, 0xBF };

	if( uBody >= sizeof(bom) && !memcmp(pBody, bom, sizeof(bom)) ) {

		pBody += sizeof(bom);
		}

	m_pRemBody = pBody;

	m_uRemBody = uBody;
	}

BOOL CHttpRequest::AddRemoteHeader(PCTXT pName, PCTXT pValue)
{
	return m_RemHead.Insert(pName, pValue);
	}

void CHttpRequest::AdjustRemoteHeaders(int nDelta)
{
	m_RemHead.Adjust(nDelta);
	}

// Local Data

void CHttpRequest::SetLocalBody(PCBYTE pData, UINT uData)
{
	m_LocBody.Empty();

	m_LocBody.Append(pData, uData);
	}

void CHttpRequest::SetLocalBody(CBytes Body)
{
	m_LocBody = Body;
	}

void CHttpRequest::SetLocalBody(CString Body)
{
	m_LocBody.Empty();

	m_LocBody.Append(PCBYTE(PCTXT(Body)), Body.GetLength());
	}

void CHttpRequest::AddLocalHeader(CString Name, CString Value)
{
	m_LocHead += Name;

	m_LocHead += ": ";

	m_LocHead += Value;

	m_LocHead += "\r\n";
	}

// Header Map Adjustment

BOOL CHttpRequest::CHeaderMap::Adjust(int nDelta)
{
	if( nDelta ) {

		for( INDEX i = GetHead(); !Failed(i); GetNext(i) ) {

			CPair const &Pair = m_Tree[i];

			((PCTXT &) Pair.GetName()) += nDelta;

			((PCTXT &) Pair.GetData()) += nDelta;
			}

		return TRUE;
		}

	return FALSE;
	}

// End of File
