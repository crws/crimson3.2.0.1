
#include "Intern.hpp"

#include "MqttClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MQTT Definitions
//

struct CMqttLength
{
	BYTE Length : 7;
	BYTE Continue : 1;
};

struct CMqttFixedHeader
{
	BYTE	    flagRet : 1;
	BYTE	    flagQoS : 2;
	BYTE	    flagDup : 1;
	BYTE	    Type : 4;
	CMqttLength Length[1];
};

struct CMqttPacketId
{
	BYTE msbPacket;
	BYTE lsbPacket;
};

struct CMqttConnectHeader
{
	BYTE msbLength;
	BYTE lsbLength;
	BYTE Name[4];
	BYTE Level;
	BYTE flagReserved : 1;
	BYTE flagCleanSession : 1;
	BYTE flagWillFlag : 1;
	BYTE flagWillQos : 2;
	BYTE flagWillRetain : 1;
	BYTE flagPassword : 1;
	BYTE flagUserName : 1;
	BYTE msbKeepAlive;
	BYTE lsbKeepAlive;
};

struct CMqttConnAckHeader
{
	BYTE flagSessionPresent : 1;
	BYTE flagReserved : 7;
	BYTE ReturnCode;
};

enum MqttPacketTypes
{
	packetConnect    = 1,
	packetConnAck    = 2,
	packetPublish    = 3,
	packetPubAck     = 4,
	packetPubRec     = 5,
	packetPubRel     = 6,
	packetPubComp    = 7,
	packetSubscribe  = 8,
	packetSubAck     = 9,
	packetUnscubribe = 10,
	packetUnsubAck   = 11,
	packetPingReq    = 12,
	packetPingResp   = 13,
	packetDisconnect = 14,
};

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client
//

// Macros

#define StripRx(t) ((t *) StripHead(pData, uRemain, sizeof(t)))

// Constructor

CMqttClient::CMqttClient(CMqttClientOptions &Opts) : m_Opts(Opts)
{
	UpdateTime();

	m_pUtils   = NULL;

	m_pMatrix  = NULL;

	m_pSock    = NULL;

	m_uState   = stateOpenSocket;

	m_uPhase   = NOTHING;

	m_PacketId = 1;

	m_uRxData  = 1;

	m_uTxData  = 1;

	m_pRxData  = PBYTE(malloc(m_uRxData));

	m_pTxData  = PBYTE(malloc(m_uTxData));

	m_DevName  = "iface.wwan1";

	AfxGetObject("ip", 0, INetUtilities, m_pUtils);

	NewPacket();
}

// Destructor

CMqttClient::~CMqttClient(void)
{
	AfxRelease(m_pMatrix);

	free(m_pRxData);

	free(m_pTxData);
}

// Operations

BOOL CMqttClient::Open(void)
{
	UpdateTime();

	if( m_Opts.m_fTls ) {

		ITlsLibrary *pTls = NULL;

		AfxGetObject("tls", 0, ITlsLibrary, pTls);

		if( pTls ) {

			if( pTls->CreateClientContext(m_pMatrix) ) {

				AfxGetAutoObject(pCertMan, "c3.certman", 0, ICertManager);

				if( m_Opts.m_uCaSrc ) {

					if( m_Opts.m_uCaSrc >= 100 ) {

						if( pCertMan ) {

							CByteArray Cert;

							if( pCertMan->GetTrustedCert(m_Opts.m_uCaSrc, Cert) ) {

								m_pMatrix->LoadTrustedRoots(Cert.data(), Cert.size());
							}
						}
					}
				}
				else {
					m_Opts.m_uCheck = tlsCheckNone;
				}

				if( m_Opts.m_uIdSrc ) {

					if( m_Opts.m_uIdSrc >= 100 ) {

						if( pCertMan ) {

							CByteArray Cert, Priv;

							CString    Pass;

							if( pCertMan->GetIdentityCert(m_Opts.m_uIdSrc, Cert, Priv, Pass) ) {

								m_pMatrix->LoadClientCert(Cert.data(), Cert.size(),
											  Priv.data(), Priv.size(),
											  Pass
								);
							}
						}
					}

				}
			}

			pTls->Release();
		}
	}

	if( m_Opts.m_uFace ) {

		if( m_pUtils ) {

			UINT i = m_pUtils->FindInterface(m_Opts.m_uFace);

			if( i < NOTHING ) {

				if( m_pUtils->GetInterfaceName(i, m_DevName) ) {

					m_DevName.Insert(0, "iface.");
				}
			}
		}
	}

	SetPhase(phaseInitial);

	return TRUE;
}

BOOL CMqttClient::Poll(UINT uID)
{
	if( uID == 0 ) {

		UpdateTime();

		if( m_uState == stateOpenSocket ) {

			return OpenSocket();
		}

		if( m_uState == stateWaitOpen ) {

			return WaitOpen();
		}

		if( m_uState == stateBackOff ) {

			if( m_uSecs >= m_uConn + m_uBack ) {

				m_uState = stateOpenSocket;

				return TRUE;
			}

			return FALSE;
		}

		if( IsConnectionOpen() ) {

			if( m_uState == stateSendConnect ) {

				if( SendConnect() ) {

					m_uState = stateRecvConnAck;

					m_uRecv  = m_uSecs;
				}
				else {
					Abort();

					return TRUE;
				}
			}

			if( m_uState == stateSendSub ) {

				if( SendSubscribe(m_SubList[m_uIndex].m_Filter, m_Opts.m_SubQos) ) {

					m_uState = stateRecvSubAck;

					m_uRecv  = m_uSecs;
				}
				else {
					Abort();

					return TRUE;
				}
			}

			if( m_uState == stateSendPub ) {

				if( OnAppGetData(m_pSendMsg) ) {

					if( SendPublish(m_pSendMsg->m_Topic, m_pSendMsg->m_Data) ) {

						if( m_Opts.m_PubQos == 0 ) {

							m_uState = stateWaitPubDone;

							m_uRecv  = m_uSecs;
						}
						else {
							m_uState = stateRecvPubAck;

							m_uRecv  = m_uSecs;
						}
					}
					else {
						Abort();

						return TRUE;
					}
				}
			}

			if( m_uState == stateWaitPubDone ) {

				if( !m_fTxBusy ) {

					OnClientDataSent(m_pSendMsg);

					m_uState = stateSendPub;
				}
			}

			if( m_uState == stateRecvConnAck || m_uState == stateRecvSubAck || m_uState == stateRecvPubAck ) {

				if( m_uSecs >= m_uRecv + m_Opts.m_uRecvTimeout ) {

					Abort();

					return TRUE;
				}
			}

			return PumpSend() || PumpRecv();
		}

		AfxTrace("disconnect\n");

		AbortAndStep();

		return TRUE;
	}

	return FALSE;
}

// Client Hooks

void CMqttClient::OnClientPhaseChange(void)
{
}

void CMqttClient::OnClientPublish(CByteArray const &Blob)
{
	AddPayload(Blob);
}

BOOL CMqttClient::OnClientNewData(CMqttMessage const *pMsg)
{
	return TRUE;
}

BOOL CMqttClient::OnClientGetData(CMqttMessage * &pMsg)
{
	return FALSE;
}

BOOL CMqttClient::OnClientDataSent(CMqttMessage const *pMsg)
{
	return TRUE;
}

// Handlers

BOOL CMqttClient::OnAppConnAck(void)
{
	if( m_uState == stateRecvConnAck ) {

		SetPhase(phaseConnected);

		if( m_SubList.GetCount() ) {

			m_uState = stateSendSub;

			m_uIndex = 0;
		}
		else
			StartPublishing();

		return TRUE;
	}

	return FALSE;
}

BOOL CMqttClient::OnAppSubAck(void)
{
	if( m_uState == stateRecvSubAck ) {

		if( ++m_uIndex < m_SubList.GetCount() ) {

			m_uState = stateSendSub;
		}
		else
			StartPublishing();

		return TRUE;
	}

	return FALSE;
}

BOOL CMqttClient::OnAppPubAck(void)
{
	if( m_uState == stateRecvPubAck ) {

		OnClientDataSent(m_pSendMsg);

		m_uState = stateSendPub;

		return TRUE;
	}

	AfxTrace("puback in wrong state %u\n", m_uState);

	return FALSE;
}

BOOL CMqttClient::OnAppNewData(CMqttMessage *pMsg)
{
	for( UINT n = 0; n < m_SubList.GetCount(); n++ ) {

		CSubDef const &Sub = m_SubList[n];

		UINT          uLen = Sub.m_Filter.GetLength();

		bool          fHit = false;

		if( Sub.m_Filter.Find('+') < NOTHING ) {

			CStringArray Topic;

			CStringArray Filter;

			pMsg->m_Topic.Tokenize(Topic, '/');

			Sub.m_Filter.Tokenize(Filter, '/');

			UINT n;

			for( n = 0; n < Filter.GetCount() && n < Topic.GetCount(); n++ ) {

				if( Filter[n] == "#" ) {

					if( n == Filter.GetCount() - 1 ) {

						continue;
					}
				}

				if( n < Topic.GetCount() ) {

					if( Filter[n] == "+" ) {

						continue;
					}

					if( Filter[n] == Topic[n] ) {

						continue;
					}
				}

				break;
			}

			if( n == Filter.GetCount() ) {

				if( Sub.m_Filter[uLen-1] == '#' || n == Topic.GetCount() ) {

					fHit = true;
				}
			}
		}
		else {
			if( Sub.m_Filter[uLen-1] == '#' ) {

				if( pMsg->m_Topic.StartsWith(Sub.m_Filter.Left(uLen-1)) ) {

					fHit = true;
				}
			}
			else {
				if( pMsg->m_Topic == Sub.m_Filter ) {

					fHit = true;
				}
			}
		}

		if( fHit ) {

			pMsg->SetCode(Sub.m_uTopic);

			if( OnClientNewData(pMsg) ) {

				return TRUE;
			}

			return FALSE;
		}
	}

	return FALSE;
}

BOOL CMqttClient::OnAppGetData(CMqttMessage * &pMsg)
{
	if( OnClientGetData(pMsg) ) {

		return TRUE;
	}

	if( m_uSecs >= m_uPing + m_Opts.m_uKeepAlive ) {

		SendPingReq();

		m_uPong = m_uSecs;
	}

	if( m_uPong ) {

		if( m_uSecs >= m_uPong + m_Opts.m_uRecvTimeout ) {

			Abort();

			return FALSE;
		}
	}

	return FALSE;
}

// Socket Management

BOOL CMqttClient::OpenSocket(void)
{
	if( m_Opts.HasAvailablePeer() ) {

		if( IsInterfaceUp(TRUE) ) {

			PCTXT pName = m_Opts.GetPeerName();

			IPREF Addr  = m_Opts.GetPeerAddr();

			if( m_Opts.m_fDebug ) {

				AfxTrace("connecting to host %s on %u.%u.%u.%u\n",
					 pName,
					 Addr.m_b1,
					 Addr.m_b2,
					 Addr.m_b3,
					 Addr.m_b4
				);
			}

			if( m_Opts.m_fTls ) {

				if( m_pMatrix ) {

					m_pSock = m_pMatrix->CreateSocket(NULL, pName, m_Opts.m_uCheck);
				}
			}
			else
				AfxNewObject("sock-tcp", ISocket, m_pSock);

			if( m_pSock ) {

				FindFaceAddress();

				m_pSock->SetOption(OPT_NAGLE, 0);

				if( m_pSock->Connect(Addr, m_DevAddr, WORD(m_Opts.m_uPort), 0) == S_OK ) {

					m_uState = stateWaitOpen;

					m_uConn  = m_uSecs;

					return TRUE;
				}

				AfxTrace("failed\n");

				AbortAndStep();

				return TRUE;
			}

			AfxTrace("no sock\n");

			return TRUE;
		}

		Sleep(1000);

		return TRUE;
	}

	AfxTrace("no dns\n");

	StartBackoff(30);

	return TRUE;
}

BOOL CMqttClient::WaitOpen(void)
{
	if( m_uSecs < m_uConn + m_Opts.m_uConnTimeout ) {

		UINT Phase;

		if( m_pSock->GetPhase(Phase) == S_OK ) {

			if( Phase == PHASE_OPEN ) {

				AfxTrace("connect\n");

				SetPhase(phaseConnecting);

				m_uState  = stateSendConnect;

				m_uRxSize = 0;

				return TRUE;
			}

			if( Phase == PHASE_OPENING ) {

				return FALSE;
			}
		}
	}

	AfxTrace("failed\n");

	AbortAndStep();

	return TRUE;
}

BOOL CMqttClient::IsConnectionOpen(void)
{
	if( m_pSock ) {

		UINT Phase;

		if( m_pSock->GetPhase(Phase) == S_OK ) {

			if( Phase == PHASE_OPEN ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CMqttClient::AbortAndStep(void)
{
	Abort();

	if( IsInterfaceUp(FALSE) ) {

		if( m_Opts.NextPeer() ) {

			StartBackoff(m_Opts.GetBackOff());

			return TRUE;
		}
	}

	return FALSE;
}

void CMqttClient::Abort(void)
{
	if( m_pSock ) {

		SetPhase(phaseDropped);

		m_uState = stateOpenSocket;

		m_pSock->Abort();

		m_pSock->Release();

		m_pSock = NULL;
	}
}

void CMqttClient::Close(void)
{
	if( m_pSock ) {

		SetPhase(phaseDropped);

		m_uState = stateOpenSocket;

		m_pSock->Close();

		m_pSock->Release();

		m_pSock = NULL;
	}
}

void CMqttClient::StartBackoff(UINT uBack)
{
	AfxTrace("backing off for %u\n", uBack);

	m_uState = stateBackOff;

	m_uConn  = m_uSecs;

	m_uBack  = uBack;
}

// Packet Building

void CMqttClient::NewPacket(void)
{
	m_fTxBusy = FALSE;

	m_uTxInit = 32;

	m_uTxSize = 0;
}

PBYTE CMqttClient::AddHead(UINT uSize)
{
	m_uTxInit -= uSize;

	m_uTxSize += uSize;

	return m_pTxData + m_uTxInit;
}

PBYTE CMqttClient::AddTail(UINT uSize)
{
	UINT uPrev = m_uTxInit + m_uTxSize;

	m_uTxSize += uSize;

	if( m_uTxInit + m_uTxSize >= m_uTxData ) {

		do {
			m_uTxData <<= 1;

		} while( m_uTxInit + m_uTxSize >= m_uTxData );

		m_pTxData = PBYTE(realloc(m_pTxData, m_uTxData));
	}

	return m_pTxData + uPrev;
}

BOOL CMqttClient::AddFixedHeader(UINT Type, UINT QoS)
{
	UINT uSize  = m_uTxSize;

	UINT uFixed = sizeof(CMqttFixedHeader);

	UINT uBytes = 1;

	UINT uMask  = 0x7F;

	while( uSize & ~uMask ) {

		uFixed += sizeof(CMqttLength);

		uBytes += 1;

		uMask = (uMask << 7) | 0x7F;
	}

	CMqttFixedHeader *pFixed = (CMqttFixedHeader *) AddHead(uFixed);

	pFixed->Type    = Type;
	pFixed->flagDup = 0;
	pFixed->flagQoS = BYTE(QoS);
	pFixed->flagRet = 0;

	for( UINT n = 0; n < uBytes; n++ ) {

		pFixed->Length[n].Continue = (n < uBytes - 1);

		pFixed->Length[n].Length   = BYTE(uSize & 0x7F);

		uSize = (uSize >> 7);
	}

	return TRUE;
}

void CMqttClient::AddString(CString Text)
{
	EncodeUtf(Text);

	UINT  uSize = Text.GetLength();

	PBYTE pData = AddTail(uSize + 2);

	*pData++    = HIBYTE(uSize);

	*pData++    = LOBYTE(uSize);

	memcpy(pData, PCSTR(Text), uSize);
}

void CMqttClient::AddBlob(CByteArray const &Blob)
{
	UINT  uSize = Blob.GetCount();

	PBYTE pData = AddTail(uSize + 2);

	*pData++    = HIBYTE(uSize);

	*pData++    = LOBYTE(uSize);

	memcpy(pData, Blob.GetPointer(), uSize);
}

void CMqttClient::AddPayload(CByteArray const &Blob)
{
	UINT  uSize = Blob.GetCount();

	PBYTE pData = AddTail(uSize);

	memcpy(pData, Blob.GetPointer(), uSize);
}

void CMqttClient::AddPacketId(void)
{
	if( !WORD(++m_PacketId) ) {

		m_PacketId = 1;
	}

	AddPacketId(m_PacketId);
}

void CMqttClient::AddPacketId(UINT PacketId)
{
	PBYTE pData = AddTail(2);

	*pData++    = HIBYTE(PacketId);

	*pData++    = LOBYTE(PacketId);
}

void CMqttClient::AddByte(BYTE bData)
{
	AddTail(1)[0] = bData;
}

// Packet Parsing

PCBYTE CMqttClient::StripHead(PCBYTE &pData, UINT &uRemain, UINT uSize)
{
	PCBYTE pInit = pData;

	pData   += uSize;

	uRemain -= uSize;

	return pInit;
}

BOOL CMqttClient::GetString(PCBYTE &pData, UINT &uRemain, CString &Text)
{
	if( uRemain >= 2 ) {

		BYTE msb = *pData++;

		BYTE lsb = *pData++;

		UINT len = MAKEWORD(lsb, msb);

		uRemain  = uRemain - 2;

		if( len ) {

			Text = CString(PCTXT(StripHead(pData, uRemain, len)), len);

			DecodeUtf(Text);

			return TRUE;
		}

		Text.Empty();

		return TRUE;
	}

	return FALSE;
}

BOOL CMqttClient::GetPayload(PCBYTE &pData, UINT &uRemain, CByteArray &Blob)
{
	if( uRemain ) {

		UINT   n = uRemain;

		PCBYTE p = PCBYTE(StripHead(pData, uRemain, uRemain));

		Blob.Append(p, n);

		return TRUE;
	}

	Blob.Empty();

	return TRUE;
}

// Transport

BOOL CMqttClient::Send(void)
{
	AfxTrace("send:\n");

	AfxDump(m_pTxData + m_uTxInit, m_uTxSize);

	m_fTxBusy = TRUE;

	m_uSend   = m_uSecs;

	return TRUE;
}

BOOL CMqttClient::PumpSend(void)
{
	if( m_fTxBusy ) {

		if( m_uSecs >= m_uSend + m_Opts.m_uSendTimeout ) {

			Abort();

			return TRUE;
		}

		while( IsConnectionOpen() ) {

			UINT uLump = m_uTxSize;

			UINT uCode = m_pSock->Send(m_pTxData + m_uTxInit, uLump);

			if( uCode == S_FALSE || uLump == 0 ) {

				Sleep(10);

				return TRUE;
			}

			if( uCode == S_OK ) {

				if( (m_uTxSize -= uLump) ) {

					m_uTxInit += uLump;

					continue;
				}

				m_fTxBusy = 0;

				m_uPing   = m_uSecs;

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CMqttClient::PumpRecv(void)
{
	CMqttFixedHeader *pFixed  = (CMqttFixedHeader *) m_pRxData;

	BOOL              fBusy   = FALSE;

	if( IsConnectionOpen() ) {

		UINT  uSize = m_uRxData - m_uRxSize;

		PBYTE pData = m_pRxData + m_uRxSize;

		UINT  uCode = m_pSock->Recv(pData, uSize);

		if( uCode == S_OK ) {

			if( uSize ) {

				m_uRxSize += uSize;

				if( m_uRxSize == m_uRxData ) {

					m_uRxData <<= 1;

					m_pRxData   = PBYTE(realloc(m_pRxData, m_uRxData));

					pFixed      = (CMqttFixedHeader *) m_pRxData;
				}

				fBusy = TRUE;
			}
		}

		while( m_uRxSize >= sizeof(CMqttFixedHeader) ) {

			UINT uLimit  = 1 + (m_uRxSize - sizeof(CMqttFixedHeader)) / sizeof(CMqttLength);

			UINT uFixed  = sizeof(CMqttFixedHeader);

			UINT uRemain = 0;

			UINT uShift  = 0;

			BOOL fFound  = FALSE;

			for( UINT n = 0; n < min(4, uLimit); n++ ) {

				uRemain |= (pFixed->Length[n].Length << uShift);

				if( !pFixed->Length[n].Continue ) {

					fFound = TRUE;

					break;
				}

				uFixed += 1;

				uShift += 7;
			}

			if( fFound ) {

				UINT uTotal = uFixed + uRemain;

				if( m_uRxSize >= uTotal ) {

					AfxTrace("recv:\n");

					AfxDump(m_pRxData, uFixed + uRemain);

					fBusy = TRUE;

					if( OnRecv(m_pRxData + uFixed, pFixed, uRemain) ) {

						UINT uTotal = uFixed + uRemain;

						if( m_uRxSize > uTotal ) {

							memmove(m_pRxData,
								m_pRxData + uTotal,
								m_uRxSize - uTotal
							);

							m_uRxSize -= uTotal;

							continue;
						}
						else
							m_uRxSize = 0;
					}
					else
						Abort();
				}
			}

			break;
		}
	}

	return fBusy;
}

// Requests

BOOL CMqttClient::SendConnect(void)
{
	NewPacket();

	CMqttConnectHeader *pHeader = (CMqttConnectHeader *) AddTail(sizeof(CMqttConnectHeader));

	BOOL fWill = !m_Will.m_Topic.IsEmpty();

	BOOL fUser = !m_Opts.m_UserName.IsEmpty();

	BOOL fPass = !m_Opts.m_Password.IsEmpty();

	pHeader->msbLength        = 0;
	pHeader->lsbLength        = 4;
	pHeader->Name[0]          = 'M';
	pHeader->Name[1]          = 'Q';
	pHeader->Name[2]          = 'T';
	pHeader->Name[3]          = 'T';
	pHeader->Level            = 4;
	pHeader->flagReserved     = 0;
	pHeader->flagCleanSession = m_Opts.m_fCleanSession ? 1 : 0;
	pHeader->flagWillFlag     = fWill;
	pHeader->flagWillQos      = 0;
	pHeader->flagWillRetain   = 0;
	pHeader->flagPassword     = fPass;
	pHeader->flagUserName     = fUser;
	pHeader->msbKeepAlive     = HIBYTE(m_Opts.m_uKeepAlive);
	pHeader->lsbKeepAlive     = LOBYTE(m_Opts.m_uKeepAlive);

	AddString(m_Opts.m_ClientId);

	if( fWill ) {

		AddString(m_Will.m_Topic);

		AddBlob(m_Will.m_Data);
	}

	if( fUser ) {

		AddString(m_Opts.m_UserName);
	}

	if( fPass ) {

		AddString(m_Opts.m_Password);
	}

	AddFixedHeader(packetConnect, 0);

	return Send();
}

BOOL CMqttClient::SendSubscribe(CString Topic, UINT QoS)
{
	NewPacket();

	AddPacketId();

	AddString(Topic);

	AddByte(BYTE(QoS));

	AddFixedHeader(packetSubscribe, 1);

	return Send();
}

BOOL CMqttClient::SendPublish(CString Topic, CByteArray const &Blob)
{
	NewPacket();

	AddString(Topic);

	if( m_Opts.m_PubQos ) {

		AddPacketId();
	}

	OnClientPublish(Blob);

	AddFixedHeader(packetPublish, m_Opts.m_PubQos);

	return Send();
}

BOOL CMqttClient::SendPubAck(UINT PacketId)
{
	NewPacket();

	AddPacketId(PacketId);

	AddFixedHeader(packetPubAck, 0);

	return Send();
}

BOOL CMqttClient::SendPingReq(void)
{
	NewPacket();

	AddFixedHeader(packetPingReq, 0);

	return Send();
}

BOOL CMqttClient::SendPingResp(void)
{
	NewPacket();

	AddFixedHeader(packetPingResp, 0);

	return Send();
}

// Replies

BOOL CMqttClient::OnRecv(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain)
{
	switch( pFixed->Type ) {

		case packetConnAck:

			return OnConnAck(pData, pFixed, uRemain);

		case packetSubAck:

			return OnSubAck(pData, pFixed, uRemain);

		case packetPubAck:

			return OnPubAck(pData, pFixed, uRemain);

		case packetPublish:

			return OnPublish(pData, pFixed, uRemain);

		case packetPingReq:

			return OnPingReq(pData, pFixed, uRemain);

		case packetPingResp:

			return OnPingResp(pData, pFixed, uRemain);
	}

	return FALSE;
}

BOOL CMqttClient::OnConnAck(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain)
{
	if( uRemain >= sizeof(CMqttConnAckHeader) ) {

		CMqttConnAckHeader *pHeader = StripRx(CMqttConnAckHeader);

		pData += sizeof(CMqttConnAckHeader);

		if( !pHeader->flagSessionPresent ) {

			if( pHeader->ReturnCode == 0 ) {

				return OnAppConnAck();
			}
		}

		Sleep(5000);

		Abort();
	}

	return FALSE;
}

BOOL CMqttClient::OnSubAck(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain)
{
	if( uRemain == sizeof(CMqttPacketId) + 1 ) {

		CMqttPacketId *pHeader = StripRx(CMqttPacketId);

		if( m_PacketId == MAKEWORD(pHeader->lsbPacket, pHeader->msbPacket) ) {

			BYTE QoS = *pData++;

			if( QoS == m_Opts.m_SubQos ) {

				return OnAppSubAck();
			}
		}
	}

	return FALSE;
}

BOOL CMqttClient::OnPubAck(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain)
{
	if( uRemain == sizeof(CMqttPacketId) ) {

		CMqttPacketId *pHeader = StripRx(CMqttPacketId);

		if( m_PacketId == MAKEWORD(pHeader->lsbPacket, pHeader->msbPacket) ) {

			return OnAppPubAck();
		}

		AfxTrace("puback with bad id\n");
	}

	return FALSE;
}

BOOL CMqttClient::OnPublish(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain)
{
	if( pFixed->flagQoS <= 1 ) {

		CMqttMessage Msg;

		if( GetString(pData, uRemain, Msg.m_Topic) ) {

			UINT       PacketId = 0;

			CByteArray Content;

			if( pFixed->flagQoS >= 1 ) {

				if( uRemain >= sizeof(CMqttPacketId) ) {

					CMqttPacketId *pHeader = StripRx(CMqttPacketId);

					PacketId               = MAKEWORD(pHeader->lsbPacket, pHeader->msbPacket);
				}
				else
					return FALSE;
			}

			if( GetPayload(pData, uRemain, Msg.m_Data) ) {

				if( !PacketId || SendPubAck(PacketId) ) {

					return OnAppNewData(&Msg);
				}
			}
		}
	}

	return FALSE;
}

BOOL CMqttClient::OnPingReq(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain)
{
	SendPingResp();

	return TRUE;
}

BOOL CMqttClient::OnPingResp(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain)
{
	if( m_uPong ) {

		m_uPong = 0;
	}

	return TRUE;
}

// Implementation

void CMqttClient::UpdateTime(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	UINT64 k = 1000;

	m_uSecs  = tv.tv_sec;

	m_uMilli = tv.tv_sec * k + tv.tv_usec / k;
}

void CMqttClient::AddToSubList(UINT uTopic, CString Filter)
{
	CSubDef Sub;

	Sub.m_uTopic = uTopic;

	Sub.m_Filter = Filter;

	m_SubList.Append(Sub);
}

void CMqttClient::StartPublishing(void)
{
	SetPhase(phasePublishing);

	m_uState = stateSendPub;

	m_uIndex = 0;

	m_uPing  = m_uSecs;

	m_uPong  = 0;
}

BOOL CMqttClient::SetPhase(UINT uPhase)
{
	if( m_uPhase != uPhase ) {

		m_uPhase = uPhase;

		OnClientPhaseChange();

		return TRUE;
	}

	return FALSE;
}

BOOL CMqttClient::IsInterfaceUp(BOOL fCheck)
{
	if( !m_DevName.IsEmpty() ) {

		AfxGetAutoObject(pFace, m_DevName, 0, IInterfaceStatus);

		if( pFace ) {

			CInterfaceStatusInfo Info;

			Info.m_fValid = FALSE;

			pFace->GetInterfaceStatus(Info);

			if( Info.m_fValid ) {

				if( Info.m_fOnline && (!fCheck || !Info.m_fReqOff) ) {

					return TRUE;
				}
			}

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CMqttClient::FindFaceAddress(void)
{
	if( m_Opts.m_uFace ) {

		m_DevAddr.MakeEmpty();

		UINT uSlot = m_pUtils->FindInterface(m_Opts.m_uFace);

		if( uSlot < NOTHING ) {

			m_pUtils->GetInterfaceAddr(uSlot, m_DevAddr);

			if( !m_DevAddr.IsEmpty() ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Diagnostics

void CMqttClient::AfxTrace(PCTXT pText, ...)
{
	#if 1 || defined(_DEBUG)

	if( m_Opts.m_fDebug ) {

		::AfxTrace("mqtt: ");

		va_list pArgs;

		va_start(pArgs, pText);

		AfxTraceArgs(pText, pArgs);

		va_end(pArgs);
	}

	#endif
}

void CMqttClient::AfxDump(PCVOID pData, UINT uCount)
{
	#if 1 || defined(_DEBUG)

	if( m_Opts.m_fDebug ) {

		::AfxDump(PCBYTE(pData), uCount);
	}

	#endif
}

// End of File
