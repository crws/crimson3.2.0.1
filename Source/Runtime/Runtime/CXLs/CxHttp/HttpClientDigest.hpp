
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpClientDigest_HPP

#define	INCLUDE_HttpClientDigest_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpDigest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class DLLAPI CHttpAuth;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Digest Helper
//

class DLLAPI CHttpClientDigest : public CHttpDigest
{
	public:
		// Constructor
		CHttpClientDigest(void);

		// Operations
		BOOL ProcessRequest(CHttpAuth *p);
		void UpdateNonce(void);
	};

// End of File

#endif
