
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HTTPSTREAMWRITENULL_HPP

#define	INCLUDE_HTTPSTREAMWRITENULL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpStream.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Http Null Writable Stream
//

class DLLAPI CHttpStreamWriteNull : public IHttpStreamWrite
{
public:
	// Constructor
	CHttpStreamWriteNull(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IHttpStreamWrite
	BOOL Write(PCTXT pData, UINT uData);
	void Finalize(void);
	void Abort(void);

protected:
	// Data Members
	ULONG m_uRefs;
};

// End of File

#endif
