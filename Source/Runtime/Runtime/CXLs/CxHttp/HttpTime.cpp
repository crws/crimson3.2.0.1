
#include "Intern.hpp"

#include "HttpTime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Formatted Time
//

// Operations

CString CHttpTime::Format(time_t t)
{
	struct timeval tv;

	tv.tv_sec  = t;
	tv.tv_usec = 0;

	return Format(0, &tv);
	}

CString CHttpTime::Format(struct timeval const *pt)
{
	return Format(0, pt);
	}

CString CHttpTime::Format(UINT uMode, time_t t)
{
	struct timeval tv;

	tv.tv_sec  = t;
	tv.tv_usec = 0;

	return Format(uMode, &tv);
	}

CString CHttpTime::Format(UINT uMode, struct timeval const *pt)
{
	time_t tv = pt->tv_sec;

	struct tm tm;

	gmtime_r(&tv, &tm);

	if( uMode == 0 ) {

		CString r;

		static PCTXT d[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

		static PCTXT m[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

		r.AppendPrintf("%3.3s, ", d[tm.tm_wday]);

		r.AppendPrintf("%2.2u %3.3s %4.4u ", tm.tm_mday, m[tm.tm_mon], 1900+tm.tm_year);

		r.AppendPrintf("%2.2u:%2.2u:%2.2u ", tm.tm_hour, tm.tm_min, tm.tm_sec);

		r.Append("GMT");

		return r;
		}

	if( uMode == 1 || uMode == 2) {

		CString r;

		r.AppendPrintf("%4.4u-%2.2u-%2.2u", 1900+tm.tm_year, 1+tm.tm_mon, tm.tm_mday);

		r += "T";

		r.AppendPrintf("%2.2u:%2.2u:%2.2u", tm.tm_hour, tm.tm_min, tm.tm_sec);

		if( uMode == 2 ) {

			r.AppendPrintf(".%3.3u", pt->tv_usec / 1000);
			}

		r += "Z";

		return r;
		}

	return CString();
	}

time_t CHttpTime::Parse(CString r)
{
	struct tm tm = { 0 };

	time_t tzoff = 0;

	r.TrimBoth();

	if( r.GetLength() == 29 && r[3] == ',' ) {

		// RFC 1123 Format, with possible inversion of
		// month and date from use of __DATE__ macro.

		char x[32];

		strcpy(x, r);

		if( x[7] == ' ' ) {

			// 0         1         2
			// 01234567890123456789012345678
			// Mon, 01 Jan 2012 12:34:56 GMT

			x[ 7] = x[11] = x[16] = 0;
			x[19] = x[22] = x[25] = 0;

			tm.tm_mday = atoi(x +  5) + 0;
			tm.tm_mon  = mtoi(x +  8) - 1;
			tm.tm_year = atoi(x + 12) - 1900;
			}
		else {
			// 0         1         2
			// 01234567890123456789012345678
			// Mon, Jan 01 2012 12:34:56 GMT

			x[ 8] = x[11] = x[16] = 0;
			x[19] = x[22] = x[25] = 0;

			tm.tm_mday = mtoi(x +  5) + 0;
			tm.tm_mon  = atoi(x +  9) - 1;
			tm.tm_year = atoi(x + 12) - 1900;
			}

		tm.tm_hour = atoi(x + 17);
		tm.tm_min  = atoi(x + 20);
		tm.tm_sec  = atoi(x + 23);

		if( !strcmp(x + 26, "EDT") ) {

			tzoff = 4 * 60 * 60;
			}

		if( !strcmp(x + 26, "EST") ) {

			tzoff = 5 * 60 * 60;
			}
		}
	else {
		// TODO -- Support other formats

		// Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
	      
		// Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
		}

	if( tm.tm_year ) {

		return timegm(&tm) + tzoff;
		}

	return 0;
	}

// Implementation

UINT CHttpTime::mtoi(PCTXT p)
{
	static PCTXT pList[] = { "Jan", "Feb", "Mar",
				 "Apr", "May", "Jun",
				 "Jul", "Aug", "Sep",
				 "Oct", "Nov", "Dec"
				 };

	for( UINT n = 0; n < 12; n++ ) {

		if( !strncasecmp(pList[n], p, 3) ) {

			return 1 + n;
			}
		}

	return 1;
	}

// End of File
