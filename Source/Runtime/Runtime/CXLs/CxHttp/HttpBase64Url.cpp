
#include "Intern.hpp"

#include "HttpBase64Url.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Base-64 URL Encoder 
//

// Implementation

STRONG_INLINE BOOL CHttpBase64Url::IsBase64(BYTE cData)
{
	if( cData >= 'A' && cData <= 'Z' ) return TRUE;

	if( cData >= 'a' && cData <= 'z' ) return TRUE;

	if( cData >= '0' && cData <= '9' ) return TRUE;
	
	if( cData == '-'                 ) return TRUE;
	
	if( cData == '_'                 ) return TRUE;
	
	if( cData == '='                 ) return TRUE;
	
	return FALSE;
	}

STRONG_INLINE BYTE CHttpBase64Url::ByteEncode(BYTE bData)
{
	if( bData <  26 ) return 'A' + bData;

	if( bData <  52 ) return 'a' + (bData - 26);

	if( bData <  62 ) return '0' + (bData - 52);

	if( bData == 62 ) return '-';

	return '_';
	}

STRONG_INLINE BYTE CHttpBase64Url::ByteDecode(BYTE cData)
{
	if( cData >= 'A' && cData <= 'Z' ) return cData - 'A';

	if( cData >= 'a' && cData <= 'z' ) return cData - 'a' + 26;

	if( cData >= '0' && cData <= '9' ) return cData - '0' + 52;
	
	if( cData == '-'                 ) return 62;
	
	return 63;
	}

// Attributes

UINT CHttpBase64Url::GetEncodeSize(UINT uSize)
{
	switch( uSize % 3 ) {

		case 0: return 4 * (uSize / 3) + 0;
		case 1: return 4 * (uSize / 3) + 2;
		case 2: return 4 * (uSize / 3) + 3;
		}

	return 0;
	}

UINT CHttpBase64Url::GetDecodeSize(CString const &Text)
{
	UINT s = Text.GetLength();

	UINT n = ((s + 3) / 4) * 3;

	if( s > 1 && Text[s-1] == '=' ) n--;

	if( s > 2 && Text[s-2] == '=' ) n--;

	return n;
	}

UINT CHttpBase64Url::GetDecodeSize(PCTXT pText)
{
	UINT s = strlen(pText);

	UINT n = ((s + 3) / 4) * 3;

	if( s > 1 && pText[s-1] == '=' ) n--;

	if( s > 2 && pText[s-2] == '=' ) n--;

	return n;
	}

// Operations

BOOL CHttpBase64Url::Encode(PTXT pBuff, PCBYTE pData, UINT uSize, BOOL fBreak)
{
	if( pData ) {

		PTXT   p2 = pBuff;

		PCBYTE p1 = pData;

		UINT   bc = 0;

		while( bc < uSize ) {

			UINT nb = min(3, uSize - bc);

			BYTE b0 = (nb >= 1) ? *p1++ : 0;
				
			BYTE b1 = (nb >= 2) ? *p1++ : 0;
				
			BYTE b2 = (nb >= 3) ? *p1++ : 0;

			BYTE c1 = (b0 >> 2);

			BYTE c2 = ((b0 & 3) << 4) | (b1 >> 4);

			*p2++ = ByteEncode(c1);
				
			*p2++ = ByteEncode(c2);

			if( nb >= 2 ) {

				BYTE c3 = ((b1 & 15) << 2) | (b2 >> 6);

				*p2++ = ByteEncode(c3);
				}

			if( nb >= 3 ) {

				BYTE c4 = (b2 & 0x3F);

				*p2++ = ByteEncode(c4);
				}

			/*while( nb < 3 ) {

				*p2++ = '=';

				nb++;
				}*/

			bc += nb;

			if( fBreak ) {
					
				if( bc % 57 == 0 || bc >= uSize ) {

					*p2++ = 0x0D;
						
					*p2++ = 0x0A;
					}
				}
			}

		*p2++ = 0;

		return TRUE;
		}

	return FALSE;
	}

PTXT CHttpBase64Url::Encode(PCBYTE pData, UINT uSize, BOOL fBreak)
{
	UINT uEncode = GetEncodeSize(uSize);

	PTXT pEncode = New char [ uEncode + 1 ];

	Encode(pEncode, pData, uSize, fBreak);

	return pEncode;
	}

CString CHttpBase64Url::Encode(PCBYTE pData, UINT uSize)
{
	CString Text(' ', GetEncodeSize(uSize));

	Encode(PTXT(PCTXT(Text)), pData, uSize, FALSE);

	return Text;
	}

CString CHttpBase64Url::Encode(CString const &Data)
{
	CString Text(' ', GetEncodeSize(Data.GetLength()));

	Encode(PTXT(PCTXT(Text)), PCBYTE(PCTXT(Data)), Data.GetLength(), FALSE);

	return Text;
	}

BOOL CHttpBase64Url::Decode(PBYTE pBuff, PCTXT pText, UINT uText)
{
	if( pText ) {

		PBYTE  p2 = pBuff;

		PCTXT  p1 = pText;

		UINT   es = uText;

		UINT   bc = 0;

		while( isspace(*p1) ) {

			p1++;

			es--;
			}

		while( isspace(p1[es-1]) ) {

			es--;
			}

		while( bc < es ) {

			UINT nb = min(4, es - bc);

			BYTE c1 = (nb >= 1) ? *p1++ : '=';

			BYTE c2 = (nb >= 2) ? *p1++ : '=';

			BYTE c3 = (nb >= 3) ? *p1++ : '=';

			BYTE c4 = (nb >= 4) ? *p1++ : '=';
			
			BYTE b1 = ByteDecode(c1);

			BYTE b2 = ByteDecode(c2);

			BYTE b3 = ByteDecode(c3);

			BYTE b4 = ByteDecode(c4);

			if( TRUE ) {

				*p2++ = ((b1 << 2) | (b2 >> 4));
				}		

			if( c3 != '=' ) {

				*p2++ = ((b2 & 0x0F) << 4) | (b3 >> 2);
				}

			if( c4 != '=' ) {

				*p2++ = ((b3 & 0x03) << 6) | b4;
				}

			bc += nb;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CHttpBase64Url::Decode(PBYTE pBuff, CString const &Text)
{
	return Decode(pBuff, Text, Text.GetLength());
	}

BOOL CHttpBase64Url::Decode(PBYTE pBuff, PCTXT pText)
{
	return Decode(pBuff, pText, strlen(pText));
	}

PBYTE CHttpBase64Url::Decode(CString const &Text, UINT uExtra)
{
	UINT  uLen  = Text.GetLength();

	UINT  uSize = GetDecodeSize(Text);

	PBYTE pData = New BYTE [ uSize + uExtra ];

	memset(pData + uSize, 0, uExtra);

	Decode(pData, Text, uLen);

	return pData;
	}

PBYTE CHttpBase64Url::Decode(PCTXT pText, UINT uExtra)
{
	UINT  uLen  = strlen(pText);

	UINT  uSize = GetDecodeSize(pText);

	PBYTE pData = New BYTE [ uSize + uExtra ];

	memset(pData + uSize, 0, uExtra);

	Decode(pData, pText, uLen);

	return pData;
	}

// End of File
