
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttQueuedClient_HPP

#define	INCLUDE_MqttQueuedClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClient.hpp"

#include "MqttQueuedClientOptions.hpp"

#include "MqttMessageQueue.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Queued Client
//

class DLLAPI CMqttQueuedClient : public CMqttClient
{
	public:
		// Constructor
		CMqttQueuedClient(CMqttQueuedClientOptions &Opts);

		// Destructor
		~CMqttQueuedClient(void);

		// Operations
		BOOL Open(void);
		BOOL Poll(UINT uID);

		// Attributes
		BOOL IsLive(void) const;

	protected:
		// Source Mode
		enum
		{
			srcPend,
			srcHist,
			srcLive
			};

		// Options
		CMqttQueuedClientOptions &m_Opts;

		// Message Queues
		CMqttMessageQueue m_PendQueue;
		CMqttMessageQueue m_HistQueue;
		CMqttMessageQueue m_LiveQueue;

		// Pending Files
		CStringArray m_PendFiles;

		// Data Members
		IMutex * m_pLock;
		BOOL     m_fOnline;
		BOOL	 m_fPause;
		UINT     m_uSource;

		// Client Hooks
		void OnClientPhaseChange(void);
		BOOL OnClientGetData(CMqttMessage * &pMsg);
		BOOL OnClientDataSent(CMqttMessage const *pMsg);

		// Message Hook
		virtual BOOL OnMakeHistoric(CMqttMessage *pMsg);

		// Implementation
		BOOL QueueMessage(CMqttMessage *pMsg);
		void PauseSend(BOOL fPause);
		void GoOnline(void);
		void GoLive(void);
		void GoOffline(void);
	};

// End of File

#endif
