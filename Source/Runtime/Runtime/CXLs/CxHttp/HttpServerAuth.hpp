
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpServerAuth_HPP

#define	INCLUDE_HttpServerAuth_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class DLLAPI CHttpServerRequest;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Authentication Method
//

class DLLAPI CHttpServerAuth : public CHttpAuth
{
	// TODO -- The whole CheckPass mechanism is broken as it relies
	// on the server passing the plaintext password. The auth method
	// and the server need to negotiate what form of password or hash
	// is available and validate on that basis.

	public:
		// Operations
		virtual BOOL	CanAccept(CString Meth);
		virtual CString GetAuthHeader(CString Opaque, BOOL fStale);
		virtual UINT	ProcessRequest(CHttpServerRequest *pReq, CString Line);
		virtual CString GetUser(CHttpServerRequest *pReq);
		virtual BOOL    CheckPass(CHttpServerRequest *pReq, CString Pass);
		virtual void    ClearSession(CString Opaque);
	};

// End of File

#endif
