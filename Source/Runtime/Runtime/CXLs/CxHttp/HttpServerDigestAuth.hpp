
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpServerDigestAuth_HPP

#define	INCLUDE_HttpServerDigestAuth_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpServerAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class DLLAPI CHttpServerDigest;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Digest Authentication Method
//

class DLLAPI CHttpServerDigestAuth : public CHttpServerAuth/*, public IDiagProvider !!!*/
{
	public:
		// Constructor
		CHttpServerDigestAuth(CString Realm, UINT uCount);

		// Destructor
		~CHttpServerDigestAuth(void);

		// Operations
		BOOL    CanAccept(CString Meth);
		CString GetAuthHeader(CString Opaque, BOOL fStale);
		UINT	ProcessRequest(CHttpServerRequest *pReq, CString Line);
		CString GetUser(CHttpServerRequest *pReq);
		BOOL	CheckPass(CHttpServerRequest *pReq, CString Pass);
		void    ClearSession(CString Opaque);

		// IDiagProvider
		// UINT RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Data Members
		CHttpServerDigest * m_pHead;
		CHttpServerDigest * m_pTail;

		// Implementation
		void Lock(void);
		void Free(void);

		// Diagnostics
		//bool AddDiagCmds(void);
		//UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
