
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HTTPSTREAMWRITEFILE_HPP

#define	INCLUDE_HTTPSTREAMWRITEFILE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpStream.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Http File Writable Stream
//

class DLLAPI CHttpStreamWriteFile : public IHttpStreamWrite
{
	public:
		// Constructor
		CHttpStreamWriteFile(CString const &Name, FILE *pFile);
		CHttpStreamWriteFile(FILE *pFile);

		// Destructor
		virtual ~CHttpStreamWriteFile(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IHttpStreamWrite
		BOOL Write(PCTXT pData, UINT uData);
		void Finalize(void);
		void Abort(void);

	protected:
		// Data Members
		ULONG   m_uRefs;
		CString m_Name;
		FILE *  m_pFile;
	};

// End of File

#endif
