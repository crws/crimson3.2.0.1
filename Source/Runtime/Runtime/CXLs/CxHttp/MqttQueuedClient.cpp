
#include "Intern.hpp"

#include "MqttQueuedClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MqttJsonData.hpp"

#include "HttpTime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Queued Client
//

// Constructor

CMqttQueuedClient::CMqttQueuedClient(CMqttQueuedClientOptions &Opts) : CMqttClient(Opts), m_Opts(Opts)
{
	m_pLock   = Create_Mutex();

	m_fOnline = FALSE;

	m_fPause  = FALSE;
	
	m_uSource = srcPend;
	}

// Destructor

CMqttQueuedClient::~CMqttQueuedClient(void)
{
	m_pLock->Release();
	}

// Operations

BOOL CMqttQueuedClient::Open(void)
{
	if( m_Opts.m_uBuffer == 2 ) {

		CString Path = m_Opts.m_DiskPath;

		if( m_PendQueue.BindToPath(Path, m_Opts.m_bGuid) ) {
			
			if( m_HistQueue.BindToPath(Path, m_Opts.m_bGuid) ) {

				CAutoDirentList List;

				if( List.ScanFiles(Path) ) {

					for( UINT n = 0; n < List.GetCount(); n++ ) {

						m_PendFiles.Append(List[n]->d_name);
						}

					m_PendFiles.Sort();
					}

				m_HistQueue.DiskSave();

				return CMqttClient::Open();
				}
			}

		m_Opts.m_uBuffer = 1;
		}

	return CMqttClient::Open();
	}

BOOL CMqttQueuedClient::Poll(UINT uID)
{
	if( uID == 0 ) {

		if( m_pSock ) {

			if( m_uSource == srcLive ) {

				if( !IsInterfaceUp(TRUE) ) {

					Close();
				}
			}
		}
	}

	if( uID == 2 ) {

		if( m_Opts.m_uBuffer == 2 ) {

			BOOL fBusy = FALSE;
		
			if( m_PendQueue.Poll(m_pLock) ) {

				fBusy = TRUE;
				}

			if( m_HistQueue.Poll(m_pLock) ) {

				fBusy = TRUE;
				}

			if( m_uSource == srcPend ) {

				while( m_PendQueue.IsEmpty() ) {

					if( m_PendFiles.IsEmpty() ) {

						m_PendQueue.FileDone(m_pLock);

						m_uSource = srcHist;

						break;
						}
					else {
						PauseSend(TRUE);

						m_PendQueue.FileDone(m_pLock);

						if( !m_PendQueue.DiskLoad(m_PendFiles[0]) ) {

							AfxTrace("failed to load file\n");
							}

						m_PendFiles.Remove(0);

						PauseSend(FALSE);
						}
					}
				}

			if( m_HistQueue.NeedNewFile() ) {

				PauseSend(TRUE);

				if( m_HistQueue.FileDone(m_pLock) ) {

					AfxTrace("pended %s\n", PCTXT(m_HistQueue.GetFilename()));

					m_PendFiles.Append(m_HistQueue.GetFilename());

					m_uSource = srcPend;
					}

				m_HistQueue.DiskSave();

				PauseSend(FALSE);

				return TRUE;
				}

			return fBusy;
			}
		}

	return CMqttClient::Poll(uID);
	}

// Attributes

BOOL CMqttQueuedClient::IsLive(void) const
{
	return m_uSource == srcLive;
	}

// Client Hooks

void CMqttQueuedClient::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseDropped ) {

		GoOffline();
		}

	CMqttClient::OnClientPhaseChange();
	}

BOOL CMqttQueuedClient::OnClientGetData(CMqttMessage * &pMsg)
{
	m_pLock->Wait(FOREVER);

	if( !m_fPause ) {

		if( m_uSource == srcLive ) {

			if( m_LiveQueue.GetMessage(pMsg) ) {

				m_pLock->Free();

				return TRUE;
				}
			}
		else {
			if( m_Opts.m_uBuffer == 2 ) {

				if( m_PendQueue.GetMessage(pMsg) ) {

					m_uSource = srcPend;

					m_pLock->Free();

					return TRUE;
					}
			
				if( m_uSource == srcPend ) {

					m_pLock->Free();

					return FALSE;
					}
				}

			if( m_HistQueue.GetMessage(pMsg) ) {

				m_pLock->Free();

				return TRUE;
				}

			GoLive();
			}
		}

	m_pLock->Free();

	return FALSE;
	}

BOOL CMqttQueuedClient::OnClientDataSent(CMqttMessage const *pMsg)
{
	m_pLock->Wait(FOREVER);

	switch( m_uSource ) {

		case srcPend:
			
			m_PendQueue.StepToNext(TRUE);
			
			break;

		case srcHist:
			
			m_HistQueue.StepToNext(TRUE);
			
			break;

		case srcLive:
			
			m_LiveQueue.StepToNext(TRUE);
			
			break;
		}

	m_pLock->Free();

	return TRUE;
	}

// Message Hook

BOOL CMqttQueuedClient::OnMakeHistoric(CMqttMessage *pMsg)
{
	CMqttJsonData Json;

	if( pMsg->GetJson(Json) ) {

		if( Json.HasName("adhoc") ) {

			if( Json.GetValue("adhoc") == "true" ) {

				return FALSE;
				}
			}

		Json.Delete  ("historic");

		Json.AddValue("historic", "true");
		
		pMsg->SetData(Json);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CMqttQueuedClient::QueueMessage(CMqttMessage *pMsg)
{
	m_pLock->Wait(FOREVER);

	BOOL fOkay = ((m_uSource == srcLive) ? m_LiveQueue : m_HistQueue).AddMessage(pMsg);

	m_pLock->Free();

	return fOkay;
	}

void CMqttQueuedClient::PauseSend(BOOL fPause)
{
	m_pLock->Wait(FOREVER);

	m_fPause = fPause;

	m_pLock->Free();
	}

void CMqttQueuedClient::GoOnline(void)
{
	AfxTrace("going online\n");

	m_pLock->Wait(FOREVER);

	m_fOnline = TRUE;

	if( !m_PendFiles.IsEmpty() || !m_PendQueue.IsEmpty() ) {

		m_uSource = srcPend;
		}
	else {
		if( !m_HistQueue.IsEmpty() ) {

			AfxTrace("we have history!\n");

			m_uSource = srcHist;
			}
		else
			GoLive();
		}

	m_pLock->Free();
	}

void CMqttQueuedClient::GoLive(void)
{
	m_uSource = srcLive;

	SetPhase(phaseLive);
	}

void CMqttQueuedClient::GoOffline(void)
{
	if( m_fOnline ) {

		m_pLock->Wait(FOREVER);

		if( IsLive() ) {

			if( !m_LiveQueue.IsEmpty() ) {

				AfxTrace("requeuing %u live messages\n", m_LiveQueue.GetCount());

				while( !m_LiveQueue.IsEmpty() ) {

					CMqttMessage *pMsg;

					m_LiveQueue.GetMessage(pMsg);

					m_LiveQueue.StepToNext(FALSE);

					if( OnMakeHistoric(pMsg) ) {

						m_HistQueue.AddMessage(pMsg);

						continue;
						}

					delete pMsg;
					}
				}

			m_uSource = srcHist;
			}

		m_fOnline = FALSE;

		m_pLock->Free();
		}
	}

// End of File
