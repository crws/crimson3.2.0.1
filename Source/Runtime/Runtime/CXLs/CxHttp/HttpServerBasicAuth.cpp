
#include "Intern.hpp"

#include "HttpServerBasicAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpServerConnection.hpp"

#include "HttpServerRequest.hpp"

#include "HttpBase64.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Basic Authentication Method
//

// Constructor

CHttpServerBasicAuth::CHttpServerBasicAuth(CString Realm, UINT uCount)
{
	m_Realm  = Realm;

	m_uCount = uCount;

	m_pUser  = New CString [ uCount ];

	m_pPass  = New CString [ uCount ];

	m_uIndex = 0;
	}

// Destructor

CHttpServerBasicAuth::~CHttpServerBasicAuth(void)
{
	delete [] m_pUser;

	delete [] m_pPass;
	}

// Operations

BOOL CHttpServerBasicAuth::CanAccept(CString Meth)
{
	return Meth == "Basic";
	}

CString CHttpServerBasicAuth::GetAuthHeader(CString Opaque, BOOL fStale)
{
	return CPrintf("WWW-Authenticate: Basic realm=\"%s\"\r\n", PCTXT(m_Realm));
	}

UINT CHttpServerBasicAuth::ProcessRequest(CHttpServerRequest *pReq, CString Line)
{
	PBYTE   pMem = CHttpBase64::Decode(Line, 1);

	CString Text = PCTXT(pMem);

	delete  pMem;

	m_pUser[m_uIndex] = Text.StripToken(':');

	m_pPass[m_uIndex] = Text;

	pReq->SetAuthContext(PVOID(m_uIndex));

	m_uIndex = (m_uIndex + 1) % m_uCount;

	return CHttpServerConnection::authOkay;
	}

CString CHttpServerBasicAuth::GetUser(CHttpServerRequest *pReq)
{
	UINT uIndex = UINT(pReq->GetAuthContext());

	return m_pUser[uIndex];
	}

BOOL CHttpServerBasicAuth::CheckPass(CHttpServerRequest *pReq, CString Pass)
{
	UINT uIndex = UINT(pReq->GetAuthContext());

	return !strcmp(Pass, m_pPass[uIndex]);
	}

// End of File
