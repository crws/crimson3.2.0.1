
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttMessageQueue_HPP

#define	INCLUDE_MqttMessageQueue_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMqttMessage;

//////////////////////////////////////////////////////////////////////////
//
// MQTT Message Queue
//

class DLLAPI CMqttMessageQueue
{
	public:
		// Constructor
		CMqttMessageQueue(void);

		// Destructor
		~CMqttMessageQueue(void);

		// Attributes
		BOOL    IsEmpty(void) const;
		BOOL    IsFull(void) const;
		BOOL    NeedNewFile(void) const;
		CString GetFilename(void) const;
		UINT    GetCount(void) const;

		// Operations
		BOOL Poll(IMutex *pLock);
		BOOL BindToPath(CString Path, PCBYTE pGuid);
		BOOL DiskLoad(CString Name);
		BOOL DiskSave(void);
		BOOL FileDone(IMutex *pLock);
		BOOL AddMessage(CMqttMessage *  pMsg);
		BOOL GetMessage(CMqttMessage * &pMsg);
		BOOL StepToNext(BOOL fFree);

	protected:
		// Disk Mode
		enum
		{
			modeNone,
			modeLoad,
			modeSave
			};

		// Block Sizes
		static UINT const blockWrite =    64 * 1024;
		static UINT const blockRead  =  1024 * 1024;
		static UINT const fileLimit  =   512 * 1024;

		// Message List
		typedef CList <CMqttMessage *> CMessageList;

		// Data Members
		CMessageList m_List;
		UINT         m_uSecs;
		UINT         m_uLast;
		UINT	     m_uMode;
		UINT	     m_uBytes;
		UINT	     m_uCount;
		UINT	     m_uSaved;
		CString	     m_Path;
		CString	     m_Name;
		PCBYTE       m_pGuid;
		INDEX	     m_SendIndex;
		INDEX	     m_SaveIndex;

		// Filing Data
		CAutoFile m_File;
		UINT      m_uLength;

		// Implementation
		void UpdateTime(void);
		BOOL CreateSaveFile(void);
		BOOL CheckFileValid(void);
		BOOL CheckForKills(IMutex *pLock, BOOL fFile);
		void CheckForSaves(IMutex *pLock);
		void Debug(CMqttMessage const *pMsg, PCTXT pAction, ...);

		// Filing System Access
		BOOL FindFilingSystem(void);
		BOOL FreeFilingSystem(void);
		BOOL IsMounted(void);
		BOOL FileExists(void);
		BOOL DeleteFile(void);
		BOOL OpenFile(BOOL fWrite);
		BOOL WriteFile(PCVOID pData, UINT uSize);
		BOOL ReadFile(PVOID pData, UINT uSize);
		BOOL AppendFile(void);
		BOOL SetFilePos(UINT uPos);
		UINT GetFilePos(void);
		BOOL EndOfFile(UINT uRead);
		BOOL CloseFile(void);
	};

// End of File

#endif
