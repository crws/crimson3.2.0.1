
#include "Intern.hpp"

#include "HttpUrlEncoding.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP URL Encoding
//

// Implementation

STRONG_INLINE BYTE CHttpUrlEncoding::FromHex(char cData)
{
	if( cData >= '0' && cData <= '9' ) {

		return cData - '0';
		}

	if( cData >= 'A' && cData <= 'F' ) {

		return cData - 'A' + 10;
		}
	
	if( cData >= 'a' && cData <= 'f' ) {

		return cData - 'a' + 10;
		}

	return 0;
	}

// Operations

CString CHttpUrlEncoding::Encode(CString const &Text)
{
	return Encode(Text, TRUE);
	}

CString CHttpUrlEncoding::Encode(CString const &Text, BOOL fSpace)
{
	PCTXT pList = " %!*'();:@&=+$,/?#[]";

	return Encode(Text, fSpace ? pList+0 : pList+1);
	}

CString CHttpUrlEncoding::Encode(CString const &Text, PCTXT pList)
{
	if( !Text.IsEmpty() ) {

		PCTXT   pHex  = "0123456789ABCDEF";

		PCTXT   pText = PCTXT(Text);

		UINT    uFrom = 0;

		CString Output;

		while( pText[uFrom] ) {

			PCTXT pFrom = pText + uFrom;

			UINT  uSpan = strcspn(pFrom , pList);

			PCTXT pFind = pFrom + uSpan;

			if( *pFind ) {

				// We've found one of the encoded set.

				if( !uFrom ) {

					// Assume 25% increase in size before
					// we start the process of building
					// the output string to avoid a lot
					// of reallocation as we work.

					UINT uAlloc = 5 * Text.GetLength() / 4;

					Output.Expand(uAlloc + 1);
					}

				if( *pFind == '%' ) {

					// If it is percent, add the characters
					// before it and the matched character,
					// and then add a second percent sign.

					Output.Append(pFrom, uSpan + 1);

					Output.Append('%');
					}
				else {
					// Otherwise, add the characters before
					// it, followed by a percent sign and then
					// the hex encoding of the character.

					Output.Append(pFrom, uSpan);

					Output.Append('%');

					Output.Append(pHex[*pFind / 16]);
					
					Output.Append(pHex[*pFind % 16]);
					}

				// Skip the encoded characters and
				// the character string before it.

				uFrom += uSpan + 1;

				continue;
				}

			if( uFrom ) {

				// Nothing found, so if we're not at the
				// start of the process, add what is left
				// of the string and we are finished.

				Output.Append(pFrom);

				break;
				}

			// Nothing found and we're at the start of
			// the encoding process, so just return the
			// input string as we've nothing to do.

			return Text;
			}

		return Output;
		}

	return Text;
	}

CString CHttpUrlEncoding::Decode(CString const &Text)
{
	if( !Text.IsEmpty() ) {

		PCTXT   pText = PCTXT(Text);

		UINT    uFrom = 0;

		CString Output;

		while( pText[uFrom] ) {

			PCTXT pFrom = pText + uFrom;

			PTXT  pFind = PTXT(strchr(pFrom , '%'));

			if( pFind ) {

				// We've found a percent sign.

				if( !uFrom ) {

					// Allocate output string to same size
					// as intput string to avoid a lot of 
					// reallocation as we work.

					UINT uAlloc = Text.GetLength();

					Output.Expand(uAlloc + 1);
					}

				if( pFind[1] == '%' ) {

					// The percent sign is doubled up
					// so add the characters before it
					// and the percent itself, and then
					// skip those and the extra percent.

					UINT uSpan = pFind - pFrom;

					Output.Append(pFrom, uSpan + 1);

					uFrom += uSpan;
					uFrom += 2;

					continue;
					}

				if( pFind[1] && pFind[2] ) {

					// The percent sign is not doubled so
					// add the characters before it and add
					// the decoded value of the next two
					// characters in hex. Skip all those.

					UINT uSpan = pFind - pFrom;

					Output.Append(pFrom, uSpan);

					BYTE bData = 16 * FromHex(pFind[1])
							+ FromHex(pFind[2])
							;

					Output.Append(char(bData));

					uFrom += uSpan;
					uFrom += 3;

					continue;
					}

				// Bad percent sequence at the end of the
				// string, so just drop out and return what
				// we have decoded so far.
				
				break;
				}

			if( uFrom ) {

				// Nothing found, so if we're not at the
				// start of the process, add what is left
				// of the string and we are finished.

				Output.Append(pFrom);

				break;
				}

			// Nothing found and we're at the start of
			// the decoding process, so just return the
			// input string as we've nothing to do.

			return Text;
			}

		return Output;
		}

	return Text;
	}

// End of File
