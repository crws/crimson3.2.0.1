
#include "Intern.hpp"

#include "HttpClientConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpClientRequest.hpp"

#include "HttpClientBasicAuth.hpp"

#include "HttpClientDigestAuth.hpp"

#include "HttpClientNtlmAuth.hpp"

#include "HttpClientManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Connection Options
//

// Constructor

CHttpClientConnectionOptions::CHttpClientConnectionOptions(void)
{
	m_uCheck       = 0;

	m_uConnTimeout = 30000;

	m_fCompRequest = FALSE;

	m_fCompReply   = TRUE;

 	m_uVer         = 11;
	}

// Initialization

void CHttpClientConnectionOptions::Load(PCBYTE &pData)
{
	CHttpConnectionOptions::Load(pData);

	m_uCheck       = GetByte(pData);
	
	m_uConnTimeout = GetWord(pData) * 1000;
	
	m_fCompRequest = GetByte(pData);
	
	m_fCompReply   = GetByte(pData);
	
	m_uVer         = GetByte(pData);
	}

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Connection
//

// Constructor

CHttpClientConnection::CHttpClientConnection(CHttpClientManager *pManager, CHttpClientConnectionOptions const &Opts) : CHttpConnection(Opts), m_Opts(Opts)
{
	m_pManager   = pManager;

	m_uSize      = 4096;

	m_pData	     = PTXT(malloc(m_uSize));

	m_IP.m_b1    = 0;

	m_sVer[0]    = char('0' + m_Opts.m_uVer / 10);

	m_sVer[1]    = '.';

	m_sVer[2]    = char('0' + m_Opts.m_uVer % 10);

	m_sVer[3]    = 0;

	m_pMethod[0] = New CHttpClientBasicAuth;

	m_pMethod[1] = New CHttpClientDigestAuth;

	m_pMethod[2] = NULL; // New CHttpClientNtlmAuth;

	m_pAuth      = NULL;
	}

// Destructor

CHttpClientConnection::~CHttpClientConnection(void)
{
	for( UINT n = 0; n < elements(m_pMethod); n++ ) {
		
		if( m_pMethod[n] ) {

			delete m_pMethod[n];
			}
		}
	}

// Operations

BOOL CHttpClientConnection::SetServer(PCTXT pHost, PCTXT pUser, PCTXT pPass)
{
	m_Host = pHost;

	m_User = pUser;

	m_Pass = pPass;

	m_Cookies.Empty();

	return TRUE;
	}

BOOL CHttpClientConnection::Transact(CHttpClientRequest *pReq)
{
	while( CheckConnection() ) {

		BuildRequest(pReq);

		pReq->ClearRemoteData();

		if( Send() && Recv(pReq) ) {

			UINT uStatus;

			CheckClose (pReq);

			ScanCookies(pReq);

			// TODO -- Handle null return from strchr
		
			if( (uStatus = atoi(strchr(m_pData, ' ') + 1)) == 401 ) {

				if( m_Pass.IsEmpty() ) {

					return FALSE;
					}

				if( ProcessAuth(pReq, TRUE) ) {

					continue;
					}
				}
			else
				ProcessAuth(pReq, FALSE);

			pReq->SetStatus(uStatus);

			return TRUE;
			}

		Abort();

		break;
		}

	return FALSE;
	}

// Implementation

BOOL CHttpClientConnection::Recv(CHttpRequest *pReq)
{
	RecvInit(pReq);

	SetTimer(m_Opts.m_uRecvTimeout);

	while( GetTimer() ) {

		switch( RecvData(pReq, FALSE) ) {

			case httpRecvFail:

				return FALSE;

			case httpRecvDone:

				return TRUE;

			case httpRecvMore:

				SetTimer(m_Opts.m_uRecvTimeout);

				continue;

			case httpRecvIdle:

				Sleep(5);

				continue;
			}
		}

	return FALSE;
	}

void CHttpClientConnection::ScanCookies(CHttpClientRequest *pReq)
{
	for( UINT n = 0;; n++ ) {

		CString Cookie = pReq->GetReplyHeader("Set-Cookie", n);

		if( Cookie.GetLength() ) {

			CString Name = Cookie.StripToken('=');

			CString Data = Cookie.StripToken(';', '\"');

			INDEX   Find = m_Cookies.FindName(Name);

			if( m_Cookies.Failed(Find) ) {

				m_Cookies.Insert (Name, Data);
				}
			else
				m_Cookies.SetData(Find, Data);

			continue;
			}

		break;
		}
	}

void CHttpClientConnection::BuildRequest(CHttpClientRequest *pReq)
{
	// TODO -- We don't support request compression. We'd need to
	// handle the fall-back case, since there's no real way of
	// knowing if the server will accept the compressed data and
	// it's not entirely clear how it would reject it!

	ClearData();

	AddData(pReq->GetVerb());
		
	AddData(" ");
		
	AddData(pReq->GetPath());
		
	AddData(" HTTP/");

	AddData(m_sVer);
			
	AddData("\r\n");

	AddData("Host: ");

	AddData(m_Host);
		
	AddData("\r\n");

	AddData("Connection: keep-alive\r\n");

	AddCompression(pReq);

	AddCookies();

	AddData(pReq->GetLocalHeader());
		
	CBytes Body = pReq->GetLocalBody();

	AddData(m_pAuth ? m_pAuth->GetAuthHeader(pReq->GetVerb(), pReq->GetPath(), Body) : "");

	AddData(CPrintf("Content-Length: %u\r\n\r\n", Body.GetCount()));

	// TODO -- The C3 web server uses a techniques whereby we build a list of
	// either static strings, pointers or files to be sent. This avoids some
	// copying of data. We could adopt the same here, and on the server side.

	AddData(Body);
	}

BOOL CHttpClientConnection::AddCompression(CHttpClientRequest *pReq)
{
	if( m_Opts.m_fCompReply ) {

		// We can't handle compression on streams right now.

		if( !pReq->HasReplyStream() ) {

			if( FindZLib() ) { 

				AddData("Accept-Encoding: gzip\r\n");

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CHttpClientConnection::AddCookies(void)
{
	BOOL fInit = TRUE;

	for( INDEX n = m_Cookies.GetHead(); !m_Cookies.Failed(n); m_Cookies.GetNext(n) ) {

		AddData(fInit ? "Cookie: " : "; ");

		if( m_Cookies.GetData(n).Find('"') < NOTHING ) {

			AddData( CPrintf( "%s=\"%s\"",
					  PCTXT(m_Cookies.GetName(n)),
					  PCTXT(m_Cookies.GetData(n))
					  ));
			}
		else {
			AddData( CPrintf( "%s=%s",
					  PCTXT(m_Cookies.GetName(n)),
					  PCTXT(m_Cookies.GetData(n))
					  ));
			}

		fInit = FALSE;
		}

	if( !fInit ) {

		AddData("\r\n");

		return TRUE;
		}

	return FALSE;
	}

void CHttpClientConnection::ResetAuth(void)
{
	for( UINT n = 0; n < elements(m_pMethod); n++ ) {

		if( m_pMethod[n] ) {

			m_pMethod[n]->SetCredentials(m_User, m_Pass);

			m_pMethod[n]->Reset();
			}
		}

	if( m_Opts.m_fTls && m_Pass.GetLength() ) {

		// For secure links, we can send Basic authentication
		// on the initial request without waiting for a 401.

		m_pAuth = m_pMethod[0];

		m_pAuth->ProcessRequest("", TRUE);
		}
	}

BOOL CHttpClientConnection::ProcessAuth(CHttpClientRequest *pReq, BOOL fFail)
{
	CString Data;

	if( !m_pAuth ) {
		
		CString AuthData  = "";

		UINT    uPriority = 0;

		for( UINT h = 0;; h++ ) {

			if( (Data = pReq->GetReplyHeader("WWW-Authenticate", h)).GetLength() ) {

				CString Meth = Data.StripToken(' ');

				for( UINT n = 0; n < elements(m_pMethod); n++ ) {

					if( m_pMethod[n] ) {
						
						if( m_pMethod[n]->CanAccept(Meth, uPriority) ) {

							m_pAuth  = m_pMethod[n];
						
							AuthData = Data;

							break;
							}
						}
					}

				continue;
				}

			break;
			}

		if( m_pAuth ) {

			if( m_pAuth->ProcessRequest(AuthData, fFail) ) {

				return TRUE;
				}
			}

		return FALSE;
		}
	else {
		for( UINT h = 0;; h++ ) {

			if( (Data = pReq->GetReplyHeader("Authentication-Info", h)).GetLength() ) {

				if( !m_pAuth->ProcessInfo( Data,
							   pReq->GetPath(),
							   pReq->GetRemoteBody(),
							   pReq->GetRemoteSize()
							   ) ) {

					return FALSE;
					}

				continue;
				}

			break;
			}

		if( (Data = pReq->GetReplyHeader("WWW-Authenticate")).GetLength() ) {

			UINT    uWin = 0;

			CString Meth = Data.StripToken(' ');

			if( m_pAuth->CanAccept(Meth, uWin) ) {

				if( m_pAuth->ProcessRequest(Data, fFail) ) {

					return TRUE;
					}
				}

			// TODO -- This won't deal with the case whereby we send Basic auth
			// on a secure link and then the server turns around and refuses to
			// accept it, coming back with another form of authentication.

			return FALSE;
			}

		return TRUE;
		}
	}

BOOL CHttpClientConnection::CheckConnection(void)
{
	if( m_pSock ) {

		UINT Phase;

		if( m_pSock->GetPhase(Phase) == S_OK ) {

			if( Phase == PHASE_OPEN ) {

				return TRUE;
				}
			}

		Abort();

		return FALSE;
		}
	else {
		m_IP = m_pManager->ResolveName(m_Host);

		if( !m_IP.m_dw ) {

			return FALSE;
			}

		if( m_Opts.m_fTls ) {

			m_pSock = m_pManager->CreateTlsSocket(m_Host, m_Opts.m_uCheck);
			}
		else
			m_pSock = m_pManager->CreateStdSocket();

		if( m_pSock ) {

			if( m_pSock->Connect(m_IP, m_Opts.m_Source, WORD(m_Opts.m_uPort), WORD(0)) == S_OK ) {

				SetTimer(m_Opts.m_uConnTimeout);

				while( GetTimer() ) {

					UINT Phase;

					if( m_pSock->GetPhase(Phase) == S_OK ) {

						if( Phase == PHASE_OPEN ) {

							ResetAuth();

							return TRUE;
							}

						if( Phase == PHASE_OPENING ) {

							Sleep(10);

							continue;
							}
						}

					return FALSE;
					}
				}
			}

		return FALSE;
		}
	}

BOOL CHttpClientConnection::CheckClose(CHttpRequest *pReq)
{
	PCTXT pConn = pReq->GetRemoteHeader("Connection");

	if( !strcasecmp(pConn, "close") ) {

		Close();

		return TRUE;
		}

	return FALSE;
	}

// End of File
