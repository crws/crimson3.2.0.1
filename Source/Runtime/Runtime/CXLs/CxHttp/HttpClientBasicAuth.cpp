
#include "Intern.hpp"

#include "HttpClientBasicAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpBase64.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Basic Authentication Method
//

// Constructor

CHttpClientBasicAuth::CHttpClientBasicAuth(void)
{
	}

// Operations

BOOL CHttpClientBasicAuth::CanAccept(CString Meth, UINT &uPriority)
{
	if( Meth == "Basic" ) {

		if( uPriority < priorityBasic ) {

			uPriority = priorityBasic;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CHttpClientBasicAuth::ProcessRequest(CString Line, BOOL fFail)
{
	if( m_uState == 0 ) {

		m_Code   = CHttpBase64::Encode(m_User + ":" + m_Pass);

		m_uState = 1;

		return TRUE;
		}

	if( m_uState == 1 ) {

		if( fFail ) {

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

CString CHttpClientBasicAuth::GetAuthHeader(CString Verb, CString Path, CBytes Body)
{
	return "Authorization: Basic " + m_Code + "\r\n";
	}

// End of File
