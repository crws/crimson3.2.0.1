
#include "Intern.hpp"

#include "HttpConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpManager.hpp"

#include "HttpRequest.hpp"

#include "HttpStream.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Connection Options
//

// Constants

static UINT const maxMessage = 1024 * 256;

static UINT const maxChunk   = 1024 * 256;

// Constructor

CHttpConnectionOptions::CHttpConnectionOptions(void)
{
	m_fTls         = FALSE;

	m_uPort        = 0;

	m_uSendTimeout = 30000;

	m_uRecvTimeout = 30000;

	m_uCertSource  = 0;
}

// Destructor

CHttpConnectionOptions::~CHttpConnectionOptions(void)
{
}

// Operations

void CHttpConnectionOptions::DefaultPort(void)
{
	if( !m_uPort ) {

		m_uPort = m_fTls ? 443 : 80;
	}
}

// Initialization

void CHttpConnectionOptions::Load(PCBYTE &pData)
{
	GetWord(pData);

	m_fTls         = GetByte(pData);

	m_uPort        = GetWord(pData);

	m_uSendTimeout = GetWord(pData) * 1000;

	m_uRecvTimeout = GetWord(pData) * 1000;

	m_uCertSource  = GetWord(pData);
}

//////////////////////////////////////////////////////////////////////////
//
// HTTP Connection
//

// Constructor

CHttpConnection::CHttpConnection(CHttpConnectionOptions const &Opts) : m_Opts(Opts)
{
	m_pSock  = NULL;

	m_pZLib  = NULL;

	m_uLimit = maxMessage;

	RecvInit(NULL);
}

// Destructor

CHttpConnection::~CHttpConnection(void)
{
	if( m_pSock ) {

		m_pSock->Abort();

		m_pSock->Release();

		OnFreeSocket();
	}

	free(m_pData);

	AfxRelease(m_pZLib);

	AfxRelease(m_pStream);
}

// Configuration

void CHttpConnection::SetReplyLimit(UINT uLimit)
{
	m_uLimit = uLimit;
}

// Attributes

BOOL CHttpConnection::IsOpen(void) const
{
	return m_pSock ? TRUE : FALSE;
}

UINT CHttpConnection::GetSockState(void) const
{
	UINT State;

	if( m_pSock ) {

		m_pSock->GetPhase(State);

		return State;
	}

	return 0;
}

CString CHttpConnection::GetSockPeer(void) const
{
	if( m_pSock ) {

		UINT State;

		m_pSock->GetPhase(State);

		if( State == PHASE_IDLE ) {

			return "LISTEN";
		}
		else {
			IPADDR IP;

			WORD   Port;

			m_pSock->GetRemote(IP, Port);

			return CPrintf("%u.%u.%u.%u:%u", IP.m_b1, IP.m_b2, IP.m_b3, IP.m_b4, Port);
		}
	}

	return "NONE";
}

IHttpStreamWrite * CHttpConnection::GetStream(void) const
{
	return m_pStream;
}

// Operations

void CHttpConnection::Idle(void)
{
	if( m_pSock ) {

		// This can be called by clients when idle to keep the SSL
		// traffic pumping under the hood, and to ensure thet we
		// promptly close any sockets that the other end closes.

		UINT Phase;

		if( m_pSock->GetPhase(Phase) == S_OK ) {

			if( Phase == PHASE_CLOSING ) {

				Close();
			}
		}
	}
}

void CHttpConnection::Abort(void)
{
	if( m_pSock ) {

		m_pSock->Abort();

		m_pSock->Release();

		m_pSock = NULL;

		OnFreeSocket();
	}
}

void CHttpConnection::Close(void)
{
	if( m_pSock ) {

		m_pSock->Close();

		m_pSock->Release();

		m_pSock = NULL;

		OnFreeSocket();
	}
}

// Overridables

BOOL CHttpConnection::OnAcceptPost(IHttpStreamWrite * &pStm, CHttpRequest *pReq, PCTXT pPath)
{
	return FALSE;
}

void CHttpConnection::OnFreeSocket(void)
{
}

// Implementation

void CHttpConnection::ClearData(void)
{
	m_uData = 0;
}

void CHttpConnection::AddData(CString const &Text)
{
	AddData(PCBYTE(PCTXT(Text)), Text.GetLength());
}

void CHttpConnection::AddData(PCTXT pText)
{
	AddData(PCBYTE(pText), strlen(pText));
}

void CHttpConnection::AddData(CBytes Data)
{
	AddData(Data.GetPointer(), Data.GetCount());
}

void CHttpConnection::AddData(PCBYTE pData, UINT uCount)
{
	if( m_uData + uCount > m_uSize ) {

		while( m_uData + uCount > (m_uSize = 2 * m_uSize) );

		m_pData = PTXT(realloc(m_pData, m_uSize));
	}

	memcpy(m_pData + m_uData, pData, uCount);

	m_uData += uCount;
}

BOOL CHttpConnection::IsSocketOpen(void)
{
	if( m_pSock ) {

		UINT Phase;

		if( m_pSock->GetPhase(Phase) == S_OK ) {

			if( Phase == PHASE_OPEN ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CHttpConnection::Send(void)
{
	return Send(PCBYTE(m_pData), m_uData);
}

BOOL CHttpConnection::Send(CString const &Data)
{
	PCBYTE pData = PCBYTE(PCTXT(Data));

	UINT   uData = Data.GetLength();

	return Send(pData, uData);
}

BOOL CHttpConnection::Send(PCBYTE pData, UINT uData)
{
	SetTimer(m_Opts.m_uSendTimeout);

	while( GetTimer() ) {

		UINT uSend = uData;

		if( m_pSock->Send(PBYTE(pData), uSend) == S_OK ) {

			if( (uData -= uSend) ) {

				SetTimer(m_Opts.m_uSendTimeout);

				pData += uSend;

				continue;
			}

			return TRUE;
		}

		if( !IsSocketOpen() ) {

			break;
		}

		Sleep(5);
	}

	return FALSE;
}

void CHttpConnection::RecvInit(CHttpRequest *pReq)
{
	// The next two functions receive HTTP requests and replies. They are quite
	// complex as they have to handle several different ways of figuring out the
	// size of the body, and have to deal with the fact that the data may arrive
	// in several distinct TCP/IP frames with elements split between them.

	if( pReq ) {

		pReq->GetRemoteStream(m_pStream);
	}
	else
		m_pStream = NULL;

	m_uData   = 0;

	m_uRead   = 0;

	m_uBody   = 0;

	m_uDone   = NOTHING;

	m_fChunk  = FALSE;

	m_uChunkHead = 0;

	m_uChunkData = 0;

	m_uChunkSize = 0;
}

UINT CHttpConnection::RecvData(CHttpRequest *pReq, BOOL fServer)
{
	// Receive how ever much data we can fit
	// into what is left of our receive buffer.

	PTXT pRecv = m_pData + m_uData;

	UINT uRecv = m_uSize - m_uData - 1;

	if( m_pSock->Recv(PBYTE(pRecv), uRecv) == S_OK ) {

		m_uData += uRecv;

		m_uRead += uRecv;

		pRecv[uRecv] = 0;

		if( !m_uBody ) {

			// If we haven't found the start of the body, look
			// for the double CR-LF from just before the newly
			// received data to see if we've got enough text to
			// know where the headers end.

			PCTXT pFind = "\r\n\r\n";

			UINT  uFind = 4;

			PCTXT pBody = strstr(max(m_pData, pRecv - uFind), pFind);

			if( pBody ) {

				// We now have the start of a possible body, so we
				// can do a bit of work to see if one is present and
				// to figure out how large it is going to be.

				if( !fServer && !strncmp(m_pData, "100 ", 4) ) {

					// We have receieved a 100 CONTINUE reply in which case since we never
					// sent an associated Expect request, we know that the server is just
					// tickling us to let us know it is working on things. We can reset our
					// receive content and start the process again. If we ever support the
					// continue mechanism, we should return this information to the caller.

					m_uRead = 0;

					m_uData = 0;

					return httpRecvMore;
				}

				// Remember where the body starts and read
				// the headers from the data we've built up.

				m_uBody = pBody + uFind - m_pData;

				BuildHeaderMap(pReq, m_pData);

				if( !fServer && NoBodyRequired(pReq) ) {

					// Some replies never have a body so we know 
					// we are done at this point and can return.

					SetNullBody(pReq);

					return httpRecvDone;
				}
				else {
					// We look at the headers to see if we can figure out
					// if the body is present and, if so, how long it is.

					PCTXT pEncode = pReq->GetRemoteHeader("Transfer-Encoding");

					if( *pEncode ) {

						PCTXT pSpace = strchr(pEncode, ' ');

						UINT  uCount = pSpace ? (pSpace - pEncode) : strlen(pEncode);

						if( !strncasecmp(pEncode, "chunked", uCount) ) {

							// Chunked wins over Content-Length as if both are
							// present we must still strip the chunk markers. We
							// remember the start of the chunk length field and
							// turn on the dechunking state machine.

							m_fChunk     = TRUE;

							m_uChunkHead = m_uBody;

							m_uChunkData = 0;
						}
						else {
							// We do not know how to handle other encodings.

							return httpRecvFail;
						}
					}
					else {
						PCTXT pLength = pReq->GetRemoteHeader("Content-Length");

						if( *pLength ) {

							m_uDone = m_uBody + strtoul(pLength, NULL, 10);

							if( !m_pStream ) {

								if( OnAcceptPost(m_pStream, NULL, NULL) ) {

									CString Line = CString(PCSTR(m_pData));

									CString Verb = Line.StripToken(' ');

									if( Verb == "POST" ) {

										CString Path = Line.StripToken(' ');

										OnAcceptPost(m_pStream, pReq, Path);
									}
								}
							}

							if( m_pStream ) {

								// We're using an output stream for the body
								// so grow the buffer enough for us to store
								// a reasonable chunk before we dump it.

								UINT uBlock = maxChunk;

								GrowBufferAndAdjust(pReq, m_uBody + uBlock + 1);
							}
							else {
								// We can expand the buffer at this point to make sure
								// we shall have enough space for the whole message.

								if( m_uDone > m_uLimit ) {

									return httpRecvFail;
								}

								GrowBufferAndAdjust(pReq, m_uDone + 1);
							}
						}
						else {
							if( fServer ) {

								// Requests without Content-Length or chunking
								// do not have a body, so we can return now. A
								// reply may still have a body that is terminated
								// by the session being closed, so we have to
								// keep listening if we are in client mode.

								SetNullBody(pReq);

								return httpRecvDone;
							}
						}
					}
				}
			}
			else {
				// No body yet, so we make sure no-one has
				// snuck an extra NUL into the stream...

				if( strlen(pRecv) != uRecv ) {

					return httpRecvFail;
				}
			}
		}

		if( m_fChunk ) {

			// This is the dechunker. It switches between two states, looking at
			// m_uChunkData to decide whether we're processing a chunk or getting
			// ready to receive the next length header. We have to remember that
			// we are receiving the data from the socket progressively, and not
			// everything we need may yet be in the buffer. Conversely, we may
			// have several chunks in the buffer to be processed in sequence.

			for( ;;) {

				if( !m_uChunkData ) {

					PCTXT pChunk = strstr(m_pData + m_uChunkHead, "\r\n");

					if( pChunk ) {

						// If the length header has a CR-LF terminator, we can
						// figure out how long the chunk is and switch into the
						// state where we receive the data. For anything other
						// than the first chunk, we move the header start back
						// to overwrite the previous CR-LF. Note that we add 2
						// to the chunk length to include the closing CR-LF.

						PTXT pStop = NULL;

						m_uChunkData = 2 + pChunk - m_pData;

						m_uChunkSize = 2 + strtoul(m_pData + m_uChunkHead, &pStop, 16);

						if( m_uChunkSize > maxChunk ) {

							return httpRecvFail;
						}

						if( m_pStream ) {

							// We're in stream mode so make sure we have
							// a reasonable buffer to hold either the chunk
							// or the next MTU from the TCP/IP connection.

							UINT uBlock = m_uChunkSize + 8;

							MakeMax(uBlock, 2048);

							GrowBufferAndAdjust(pReq, m_uChunkData + uBlock + 1);
						}

						if( *pStop == '\r' || *pStop == ' ' || *pStop == ';' ) {

							if( m_uChunkHead > m_uBody ) {

								m_uChunkHead -= 2;
							}

							// Loop around to process the chunk if
							// we have already received enough data.

							continue;
						}

						return httpRecvFail;
					}
				}
				else {
					if( m_uData >= m_uChunkData + m_uChunkSize ) {

						// If we get here, we have received the
						// chunk and its closing CR-LF sequence.

						if( m_uChunkSize == 2 ) {

							// We've got a zero sized chunk so we're nearly done,
							// but first we have to check if we have any trailing
							// headers. If so, we have to stay in this state and
							// receiving data until we see a double CR-LF.

							PTXT pTrail = m_pData + m_uChunkData;

							if( strncmp(pTrail, "\r\n", 2) ) {

								if( !strstr(pTrail, "\r\n\r\n") ) {

									// Nope, so go get some more data.

									break;
								}

								// Yep, so parse the trailers.

								BuildHeaderMap(pReq, pTrail);
							}

							if( m_pStream ) {

								// We're using a stream so return
								// to the caller with a null body.

								SetNullBody(pReq);

								return httpRecvDone;
							}
							else {
								m_pData[m_uChunkHead] = 0;

								if( ProcessBody(pReq) ) {

									return httpRecvDone;
								}

								return httpRecvFail;
							}
						}

						if( m_pStream ) {

							// Write the chunk to the stream and move everything else
							// that we have down to where the previous chunk header
							// started. Reset the pointers to point to the next chunk
							// header and get ready to process it. We could in theory
							// save up a few chunks to allow the stream object more to
							// run at, but that's more work and probably not needed.

							m_pStream->Write(m_pData + m_uChunkData, m_uChunkSize - 2);

							memmove(m_pData + m_uChunkHead,
								m_pData + m_uChunkData + m_uChunkSize,
								m_uData - m_uChunkHead - m_uChunkSize
							);

							m_uData      -= m_uChunkData + m_uChunkSize;

							m_uData      += m_uChunkHead;

							m_uChunkData  = 0;
						}
						else {
							// Move the chunk down in the memory to overwrite the length header
							// and the previous CR-LF, if one is present. Reset the pointers to
							// mark the next chunk header and get ready to process it.

							memmove(m_pData + m_uChunkHead,
								m_pData + m_uChunkData,
								m_uData - m_uChunkHead // Really?
							);

							m_uData      -= m_uChunkData;

							m_uData      += m_uChunkHead;

							m_uChunkHead += m_uChunkSize;

							m_uChunkData  = 0;
						}

						// Loop around to check if the length header is present.

						continue;
					}
				}

				// Give up and head back for more socket data.

				break;
			}
		}

		if( m_uRead >= m_uDone ) {

			// We've received enough bytes to meet
			// the Content-Length that was specified.

			if( !m_pStream ) {

				if( ProcessBody(pReq) ) {

					return httpRecvDone;
				}

				return httpRecvFail;
			}
			else {
				// We're using a stream so dump the data and
				// return to the caller with a null body.

				WriteToStream();

				SetNullBody(pReq);

				return httpRecvDone;
			}
		}

		if( m_uData == m_uSize - 1 ) {

			// The buffer is full.

			if( !m_pStream || m_fChunk || !m_uBody ) {

				// We're working in memory, so assuming it has
				// not got to the size limit, double the buffer
				// size and adjust any header field pointers.

				if( m_uSize == m_uLimit ) {

					return httpRecvFail;
				}

				GrowBufferAndAdjust(pReq);
			}
			else {
				// We're using a stream in Content-Length
				// mode so just dump the contents to that
				// stream and then reset the pointer to
				// start the reading process all over again.

				WriteToStream();

				m_uData = m_uBody;
			}
		}

		return httpRecvMore;
	}

	if( !IsSocketOpen() ) {

		// If the socket has closed and we have finished
		// receiving the headers, and we're not chunking
		// and do not know the Content-Length, we have
		// received a body that was terminated by a close.

		if( m_uBody ) {

			if( !m_fChunk && m_uDone == NOTHING ) {

				if( m_pStream ) {

					WriteToStream();

					SetNullBody(pReq);

					return httpRecvDone;
				}
				else {
					if( ProcessBody(pReq) ) {

						return httpRecvDone;
					}

					return httpRecvFail;
				}
			}
		}

		return httpRecvFail;
	}

	return httpRecvIdle;
}

BOOL CHttpConnection::WriteToStream(void)
{
	PTXT pWrite = m_pData + m_uBody;

	UINT uWrite = m_uData - m_uBody;

	return m_pStream->Write(pWrite, uWrite);
}

void CHttpConnection::SetNullBody(CHttpRequest *pReq)
{
	if( m_pStream ) {

		m_pStream->Finalize();
	}

	pReq->SetRemoteBody(PCBYTE(""), 0);
}

BOOL CHttpConnection::NoBodyRequired(CHttpRequest *pReq)
{
	if( m_pData[0] == '1' ) {

		// Reply codes 1xx never receive a body.

		return TRUE;
	}

	if( m_pData[0] == '2' || m_pData[0] == '3' ) {

		if( m_pData[1] == '0' && m_pData[2] == '4' ) {

			// Reply codes 204 and 304 never receive a body.

			return TRUE;
		}
	}

	if( pReq->GetVerb() == "HEAD" ) {

		// HEAD never receives a body in its reply,
		// even if it has a Content-Length header.

		return TRUE;
	}

	return FALSE;
}

void CHttpConnection::BuildHeaderMap(CHttpRequest *pReq, PTXT pScan)
{
	// Build a map of the headers in the buffer, replacing the appropriate
	// characters with nulls so that we can reference the strings in place
	// instead of copying them. This means that we have to be careful if
	// we realloc the buffer, as the pointers will need to be adjusted.

	for( ;;) {

		PTXT pLine = pScan;

		PTXT pEnd  = strstr(pScan, "\r\n");

		// Note pEnd should always be non-NULL as we don't get this far
		// unless there is a double CR-LF sequence in the message text,
		// but it doesn't hurt to check it one more time...

		if( pEnd ) {

			*pEnd = 0;

			if( pEnd > pScan ) {

				PTXT pSep = strchr(pLine, ':');

				if( pSep ) {

					PTXT pData = pSep + 1;

					while( *pData && isspace(*pData) ) {

						pData++;
					}

					while( pEnd[2] == ' ' || pEnd[2] == '\t' ) {

						// The next line starts with a whitespace, so we should collapse
						// it into this line by converting the CR-LF to spaces and then
						// looking for the next CR-LF. There will always be a next CR-LF
						// as once again, we know there's a double CR-LF in there.

						pEnd[0] = ' ';

						pEnd[1] = ' ';

						pEnd    = strstr(pEnd, "\r\n");

						*pEnd   = 0;
					}

					*pSep  = 0;

					UINT n = 1;

					while( !pReq->AddRemoteHeader(pLine, pData) ) {

						// We already have this header, so we replace the colon with
						// the letter A, B, C etc. and null out the space, allowing us
						// to store multiple occurences of the header in our map. This
						// is only possible if a space is following the colon, which the 
						// RFC doesn't technically demand, but which everyone does...

						pSep[0] = char('@' + n++);

						pSep[1] = 0;
					}
				}

				pScan = pEnd + 2;

				continue;
			}
		}

		break;
	}
}

INT CHttpConnection::GrowBuffer(UINT uSize)
{
	m_uSize    = uSize;

	PTXT pMove = PTXT(realloc(m_pData, m_uSize));

	INT  nMove = pMove - m_pData;

	m_pData    = pMove;

	return nMove;
}

INT CHttpConnection::GrowBufferAndAdjust(CHttpRequest *pReq, UINT uSize)
{
	if( uSize > m_uSize ) {

		INT nMove = GrowBuffer(uSize);

		pReq->AdjustRemoteHeaders(nMove);

		return nMove;
	}

	return 0;
}

INT CHttpConnection::GrowBufferAndAdjust(CHttpRequest *pReq)
{
	UINT uSize = min(m_uLimit, 2 * m_uSize);

	return GrowBufferAndAdjust(pReq, uSize);
}

BOOL CHttpConnection::ProcessBody(CHttpRequest *pReq)
{
	PCTXT pEncode = pReq->GetRemoteHeader("Content-Encoding");

	if( *pEncode ) {

		if( !strcasecmp(pEncode, "gzip") ) {

			return ProcessGzip(pReq);
		}

		return FALSE;
	}

	pReq->SetRemoteBody(PCBYTE(m_pData + m_uBody), m_uData - m_uBody);

	return TRUE;
}

BOOL CHttpConnection::ProcessGzip(CHttpRequest *pReq)
{
	if( FindZLib() ) {

		for( ;;) {

			UINT z = m_pZLib->Expand(PCBYTE(m_pData + m_uBody),
						 m_uData - m_uBody,
						 PBYTE(m_pData + m_uData),
						 m_uSize - m_uData - 1
			);

			if( z == NOTHING ) {

				if( m_uSize == m_uLimit ) {

					return FALSE;
				}

				GrowBufferAndAdjust(pReq);

				continue;
			}

			if( z ) {

				m_uBody  = m_uData;

				m_uData += z;

				m_pData[m_uData] = 0;

				pReq->SetRemoteBody(PCBYTE(m_pData + m_uBody), m_uData - m_uBody);

				return TRUE;
			}

			break;
		}
	}

	return FALSE;
}

BOOL CHttpConnection::FindZLib(void)
{
	if( !m_pZLib ) {

		AfxNewObject("zlib", IZLib, m_pZLib);
	}

	return m_pZLib ? TRUE : FALSE;
}

// End of File
