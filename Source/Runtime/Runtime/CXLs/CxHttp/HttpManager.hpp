
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpManager_HPP
	
#define	INCLUDE_HttpManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// HTTP Manager
//

class DLLAPI CHttpManager
{
	public:
		// Constructor
		CHttpManager(void);

		// Destructor
		~CHttpManager(void);

		// Operations
		IPADDR  ResolveName(PCTXT pHost);
		BOOL    IsTlsEnabled(void);
		BOOL    CompressType(CString Type);
		BOOL    TypeDidNotCompress(CString Type);
		CString GetMimeType(PCTXT pType);
		void    AddMimeType(PCTXT pType, CString Mime);
		void    AddExtendedTypes(void);

	protected:
		// Data Members
		ITlsLibrary *          m_pTls;
		CTree <CString>	       m_NoComp;
		CMap  <PCTXT, CString> m_Mimes;

		// Implementation
		void AddDefTypes(void);
	};

// End of File

#endif
