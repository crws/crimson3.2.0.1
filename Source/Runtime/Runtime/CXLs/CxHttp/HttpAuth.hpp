
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpAuth_HPP

#define	INCLUDE_HttpAuth_HPP

//////////////////////////////////////////////////////////////////////////
//
// HTTP Authentication Priorities
//

enum
{
	priorityBasic  = 100,
	priorityDigest = 200,
	priorityNTML   = 300
	};

//////////////////////////////////////////////////////////////////////////
//
// HTTP Authentication Base Class
//

class DLLAPI CHttpAuth
{
	public:
		// Attributes
		CString GetParam(CString Name) const;

	protected:
		// Data Members
		CStringArray	     m_List;
		CMap <CString, UINT> m_Keys;

		// Implementation
		BOOL ParseLine(CString Line);
	};

// End of File

#endif
