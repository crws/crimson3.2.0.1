
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpClientManager_HPP
	
#define	INCLUDE_HttpClientManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CHttpClientConnection;

class CHttpClientConnectionOptions;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Manager
//

class DLLAPI CHttpClientManager : public CHttpManager
{
	public:
		// Constructor
		CHttpClientManager(void);

		// Destructor
		~CHttpClientManager(void);

		// Operations
		BOOL                    Open(void);
		BOOL			LoadTrustedRoots(PCBYTE pRoot, UINT uRoot);
		BOOL			LoadClientCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass);
		CHttpClientConnection * CreateConnection(CHttpClientConnectionOptions const &Opts);
		ISocket		      * CreateStdSocket(void);
		ISocket		      * CreateTlsSocket(PCTXT pName, UINT uCheck);

	protected:
		// Data Members
		ITlsClientContext * m_pClient;
	};	

// End of File

#endif
