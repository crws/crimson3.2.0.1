
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpServerManager_HPP

#define	INCLUDE_HttpServerManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CHttpServer;

class CHttpServerConnection;

class CHttpServerConnectionOptions;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Manager
//

class DLLAPI CHttpServerManager : public CHttpManager
{
public:
	// Constructor
	CHttpServerManager(void);

	// Destructor
	~CHttpServerManager(void);

	// Operations
	BOOL                    Open(void);
	BOOL			LoadTrustedRoots(PCBYTE pRoot, UINT uRoot);
	BOOL			LoadServerCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass);
	CHttpServerConnection * CreateConnection(CHttpServer *pServer, CHttpServerConnectionOptions const &Opts);
	CHttpServerConnection * CreateRedirect(CHttpServer *pServer, CHttpServerConnectionOptions const &Opts, UINT uPort, BOOL fTls);
	ISocket		      * CreateStdSocket(void);
	ISocket		      * CreateTlsSocket(void);

protected:
	// Data Members
	ITlsServerContext * m_pServer;
};

// End of File

#endif
