
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpClientNtlmAuth_HPP

#define	INCLUDE_HttpClientNtlmAuth_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpClientAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Ntlm Authentication Method
//

class DLLAPI CHttpClientNtlmAuth : public CHttpClientAuth
{
	public:
		// Constructor
		CHttpClientNtlmAuth(void);

		// Operations
		BOOL	CanAccept(CString Meth, UINT &uPriority);
		BOOL    ProcessRequest(CString Line, BOOL fFail);
		CString GetAuthHeader(CString Verb, CString Path, CString Body);
	};

// End of File

#endif
