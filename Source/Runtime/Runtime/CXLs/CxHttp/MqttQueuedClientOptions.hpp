
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttQueuedClientOptions_HPP

#define	INCLUDE_MqttQueuedClientOptions_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Queued Client Options
//

class DLLAPI CMqttQueuedClientOptions : public CMqttClientOptions
{
	public:
		// Constructor
		CMqttQueuedClientOptions(void);

		// Initialization
		void Load(PCBYTE &pData, PCBYTE pGuid);

		// Data Members
		UINT    m_uBuffer;
		CString m_DiskPath;
		BYTE    m_bGuid[16];
	};

// End of File

#endif
