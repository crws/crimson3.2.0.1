
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpRequest_HPP

#define	INCLUDE_HttpRequest_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class     DLLAPI CHttpConnection;

interface DLLAPI IHttpStreamWrite;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Request
//

// TODO -- Add some method to set the remote body to a file or stream
// so that we can send straight from the filing system without having
// to copy the data into a buffer first. We could even compress on
// the fly if we were prepared to using chunking and we could get the
// zlib compression library to work incrementally.

class DLLAPI CHttpRequest
{

public:
	// Constructor
	CHttpRequest(void);

	// Attributes
	CString GetVerb(void) const;
	CString GetPath(void) const;
	UINT    GetStatus(void) const;

	// Operations
	void SetVerb(CString Verb);
	void SetPath(CString Path);
	void SetStatus(UINT uStatus);

	// Attributes
	CBytes  GetLocalBody(void) const;
	CString GetLocalHeader(void) const;
	BOOL    GetRemoteStream(IHttpStreamWrite * &pStm) const;
	PCBYTE  GetRemoteBody(void) const;
	UINT    GetRemoteSize(void) const;
	PCTXT   GetRemoteHeader(PCTXT pName, UINT uIndex) const;
	PCTXT   GetRemoteHeader(PCTXT pName) const;

	// Remote Data
	void ClearRemoteData(void);
	void SetRemoteBody(PCBYTE pBody, UINT uBody);
	BOOL AddRemoteHeader(PCTXT pName, PCTXT pValue);
	void AdjustRemoteHeaders(int nDelta);

protected:
	// Header Map
	class CHeaderMap : public CMap <PCTXT, PCTXT>
	{
	public:
		// Operations
		BOOL Adjust(int nDelta);
	};

	// Data Members
	CString		   m_Verb;
	CString		   m_Path;
	UINT		   m_uStatus;
	CByteArray	   m_LocBody;
	CString		   m_LocHead;
	IHttpStreamWrite * m_pRemStm;
	PCBYTE		   m_pRemBody;
	UINT		   m_uRemBody;
	CHeaderMap	   m_RemHead;

	// Local Data
	void SetLocalBody(PCBYTE pData, UINT uData);
	void SetLocalBody(CBytes  Body);
	void SetLocalBody(CString Body);
	void AddLocalHeader(CString Name, CString Value);
};

// End of File

#endif
