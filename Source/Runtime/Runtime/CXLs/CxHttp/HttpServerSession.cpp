
#include "Intern.hpp"

#include "HttpServerSession.hpp"

#include "HttpServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Session
//

// Constructor

CHttpServerSession::CHttpServerSession(CHttpServer *pServer)
{
	m_pServer = pServer;

	m_uRefs   = 0;

	m_uIdle   = 0;

	MakeOpaque();
}

// Destructor

CHttpServerSession::~CHttpServerSession(void)
{
}

// Reference Counting

BOOL CHttpServerSession::AddRef(void)
{
	m_uRefs++;

	return TRUE;
}

BOOL CHttpServerSession::Release(void)
{
	if( !--m_uRefs ) {

		m_uIdle = GetTickCount();

		FreeData();

		return TRUE;
	}

	return FALSE;
}

// Attributes

BOOL CHttpServerSession::IsDiscardable(UINT uTime) const
{
	if( m_uRefs == 0 ) {

		if( GetTickCount() - m_uIdle > ToTicks(uTime) ) {

			return TRUE;
		}
	}

	return FALSE;
}

UINT CHttpServerSession::GetRefCount(void) const
{
	return m_uRefs;
}

CString CHttpServerSession::GetOpaque(void) const
{
	return m_Opaque;
}

PCTXT CHttpServerSession::GetOpaquePtr(void) const
{
	return m_Opaque;
}

BOOL CHttpServerSession::HasUser(void) const
{
	return !m_User.IsEmpty();
}

CString CHttpServerSession::GetUser(void) const
{
	return m_User;
}

CString CHttpServerSession::GetReal(void) const
{
	return m_Real.IsEmpty() ? m_User : m_Real;
}

BOOL CHttpServerSession::CheckOpaque(PCTXT pOpaque) const
{
	return !strcmp(m_Opaque, pOpaque);
}

BOOL CHttpServerSession::CheckUser(CString User) const
{
	return m_User.IsEmpty() || m_User == User;
}

// Operations

void CHttpServerSession::TickleSession(void)
{
	OnTickle();
}

void CHttpServerSession::ClearUserInfo(void)
{
	m_User.Empty();

	m_Real.Empty();

	MakeOpaque();
}

void CHttpServerSession::SetUser(CString User)
{
	m_User = User;
}

void CHttpServerSession::SetReal(CString Real)
{
	m_Real = Real;
}

// Diagnostics

UINT CHttpServerSession::GetDiagColCount(void)
{
	return 5;
}

void CHttpServerSession::GetDiagCols(IDiagOutput *pOut)
{
	pOut->SetColumn(0, "Session", "%8.8X");
	pOut->SetColumn(1, "Refs", "%u");
	pOut->SetColumn(2, "User", "%s");
	pOut->SetColumn(3, "Real", "%s");
	pOut->SetColumn(4, "Opaque", "%s");
	pOut->SetColumn(5, "Age", "%u");
}

void CHttpServerSession::GetDiagInfo(IDiagOutput *pOut)
{
	pOut->SetData(0, this);
	pOut->SetData(1, m_uRefs);
	pOut->SetData(2, PCTXT(m_User));
	pOut->SetData(3, PCTXT(m_Real));
	pOut->SetData(4, PCTXT(m_Opaque));
	pOut->SetData(5, m_uRefs ? 0 : ToTime(GetTickCount() - m_uIdle) / 1000);
}

// Overridables

void CHttpServerSession::OnTickle(void)
{
}

void CHttpServerSession::FreeData(void)
{
}

// Implementation

void CHttpServerSession::MakeOpaque(void)
{
	IEntropy *pEntropy;

	AfxGetObject("entropy", 0, IEntropy, pEntropy);

	if( pEntropy ) {

		BYTE bData[16];

		BYTE bHash[16];

		pEntropy->GetEntropy(bData, 16);

		pEntropy->Release();

		md5(bData, 16, bHash);

		PCTXT pHex = "0123456789abcdef";

		UINT  c   = 0;

		char  sHash[17];

		for( UINT n = 0; n < 8; n++ ) {

			BYTE bData = (bHash[n] ^ bHash[8+n]);

			sHash[c++] = pHex[bData/16];

			sHash[c++] = pHex[bData%16];
		}

		sHash[c++] = 0;

		m_Opaque   = sHash;

		return;
	}

	AfxTrace("http: no entropy source available\n");

	m_Opaque = "0123456789ABCDEF";
}

// End of File
