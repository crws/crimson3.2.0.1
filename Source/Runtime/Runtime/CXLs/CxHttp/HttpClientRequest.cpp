
#include "Intern.hpp"

#include "HttpClientRequest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpClientConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Request
//

// Constructor

CHttpClientRequest::CHttpClientRequest(void)
{
	}

// Attributes

BOOL CHttpClientRequest::HasReplyStream(void) const
{
	return m_pRemStm ? TRUE : FALSE;
	}

PCBYTE CHttpClientRequest::GetReplyBody(void) const
{
	return GetRemoteBody();
	}

UINT CHttpClientRequest::GetReplySize(void) const
{
	return GetRemoteSize();
	}

PCTXT CHttpClientRequest::GetReplyHeader(PCTXT pName, UINT uIndex) const
{
	return GetRemoteHeader(pName, uIndex);
	}

PCTXT CHttpClientRequest::GetReplyHeader(PCTXT pName) const
{
	return GetRemoteHeader(pName);
	}

// Operations

void CHttpClientRequest::SetReplyStream(IHttpStreamWrite *pStm)
{
	m_pRemStm = pStm;	
	}

void CHttpClientRequest::SetRequestBody(CBytes Body)
{
	SetLocalBody(Body);
	}

void CHttpClientRequest::SetRequestBody(CString Body)
{
	SetLocalBody(Body);
	}

void CHttpClientRequest::AddRequestHeader(CString Name, CString Value)
{
	AddLocalHeader(Name, Value);
	}

// Transaction

BOOL CHttpClientRequest::Transact(CHttpClientConnection *pConnect)
{
	return pConnect->Transact(this);
	}

BOOL CHttpClientRequest::Transact(CHttpClientConnection *pConnect, UINT uCode)
{
	return pConnect->Transact(this) && m_uStatus == uCode;
	}

BOOL CHttpClientRequest::Transact(CHttpClientConnection *pConnect, CString Verb, UINT uCode)
{
	SetVerb(Verb);

	return pConnect->Transact(this) && m_uStatus == uCode;
	}

// End of File
