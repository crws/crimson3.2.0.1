
#include "Intern.hpp"

#include "JsonData.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// JSON Data
//

// Static Data

TCHAR const CJsonData::m_cEsc[] = T("\\\"\'\b\f\n\r\t");

TCHAR const CJsonData::m_cRep[] = T("\\\"\'bfnrt");

// Constructor

CJsonData::CJsonData(void)
{
	m_fList  = FALSE;

	m_uIndex = 0;
}

CJsonData::CJsonData(BOOL fList)
{
	m_fList  = fList;

	m_uIndex = 0;
}

// Destructor

CJsonData::~CJsonData(void)
{
	for( INDEX n = m_Tree.GetHead(); !m_Tree.Failed(n); m_Tree.GetNext(n) ) {

		CJsonPair const &Pair = m_Tree[n];

		if( Pair.m_pSub ) {

			delete Pair.m_pSub;
		}
	}
}

// Operations

void CJsonData::Empty(void)
{
	if( !m_Tree.IsEmpty() ) {

		for( INDEX n = m_Tree.GetHead(); !m_Tree.Failed(n); m_Tree.GetNext(n) ) {

			CJsonPair const &Pair = m_Tree[n];

			if( Pair.m_pSub ) {

				delete Pair.m_pSub;
			}
		}

		m_Tree.Empty();
	}

	m_uIndex = 0;
}

BOOL CJsonData::Parse(PCTXT pText)
{
	Empty();

	if( ParseBlock(pText) ) {

		if( *pText ) {

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

void CJsonData::AddObject(CString Name)
{
	m_Tree.Insert(CJsonPair(Name, New CJsonData));
}

void CJsonData::AddChild(CString Name, BOOL fList, CJsonData * &pSub)
{
	if( Name.Count('.') ) {

		CString    Path   = Name.StripToken('.');

		CJsonData *pChild = GetChild(Path);

		if( !pChild ) {

			AddChild(Path, FALSE, pChild);
		}

		if( pChild ) {

			pChild->AddChild(Name, fList, pSub);
		}
	}
	else {
		m_Tree.Insert(CJsonPair(Name, pSub = New CJsonData(fList)));
	}
}

void CJsonData::AddObject(CString Name, CJsonData * &pSub)
{
	AddChild(Name, FALSE, pSub);
}

void CJsonData::AddList(CString Name, CJsonData * &pSub)
{
	AddChild(Name, TRUE, pSub);
}

void CJsonData::AddValue(CString Name, CString Value, UINT Type)
{
	if( Name.Count('.') ) {

		CString    Path   = Name.StripToken('.');

		CJsonData *pChild = GetChild(Path);

		if( pChild == NULL ) {

			AddChild(Path, FALSE, pChild);
		}

		if( pChild ) {

			pChild->AddValue(Name, Value, Type);
		}
	}
	else {
		m_Tree.Remove(Name);

		m_Tree.Insert(CJsonPair(Name, Value, Type));
	}
}

void CJsonData::AddValue(CString Name, CString Value)
{
	if( Name.Count('.') ) {

		CString    Path   = Name.StripToken('.');

		CJsonData *pChild = GetChild(Path);

		if( pChild == NULL ) {

			AddChild(Path, FALSE, pChild);
		}

		if( pChild ) {

			pChild->AddValue(Name, Value);
		}
	}
	else {
		m_Tree.Remove(Name);

		m_Tree.Insert(CJsonPair(Name, Value));
	}
}

void CJsonData::AddNull(CString Name)
{
	m_Tree.Insert(CJsonPair(Name, T("null"), jsonNull));
}

void CJsonData::AddChild(BOOL fList, CJsonData * &pSub)
{
	m_Tree.Insert(CJsonPair(CreateName(m_uIndex++), pSub = New CJsonData(fList)));
}

void CJsonData::AddObject(CJsonData * &pSub)
{
	AddChild(FALSE, pSub);
}

void CJsonData::AddList(CJsonData * &pSub)
{
	AddChild(TRUE, pSub);
}

void CJsonData::AddMember(CString Value)
{
	m_Tree.Insert(CJsonPair(CreateName(m_uIndex++), Value));
}

BOOL CJsonData::Delete(CString Name)
{
	if( Name.Count('.') ) {

		CJsonData *pChild = GetChild(Name.StripToken('.'));

		if( pChild ) {

			return pChild->Delete(Name);
		}
	}
	else {
		INDEX n = m_Tree.Find(Name);

		if( !m_Tree.Failed(n) ) {

			CJsonPair const &Pair = m_Tree[n];

			if( Pair.m_pSub ) {

				delete Pair.m_pSub;
			}

			m_Tree.Remove(n);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CJsonData::Delete(UINT uIndex)
{
	return Delete(CreateName(uIndex));
}

void CJsonData::SetValue(INDEX Index, CString Value)
{
	((CJsonPair &) m_Tree[Index]).m_Value = Value;
}

// Attributes

CString CJsonData::GetAsText(BOOL fPretty) const
{
	CString t;

	UINT uLevel = 0;

	if( fPretty ) {

		t += CString(' ', 2 * uLevel);
	}

	if( TRUE ) {

		t += GetAsText(fPretty, uLevel);
	}

	if( fPretty ) {

		t += "\r\n";
	}

	return t;
}

BOOL CJsonData::GetNames(CStringArray &Names) const
{
	for( INDEX n = m_Tree.GetHead(); !m_Tree.Failed(n); m_Tree.GetNext(n) ) {

		CJsonPair const &Pair = m_Tree[n];

		Names.Append(Pair.m_Name);
	}

	return !Names.IsEmpty();
}

BOOL CJsonData::HasName(CString Name) const
{
	if( m_Tree.Failed(m_Tree.Find(Name)) ) {

		return FALSE;
	}

	return TRUE;
}

BOOL CJsonData::HasName(UINT uIndex) const
{
	if( m_Tree.Failed(m_Tree.Find(CreateName(uIndex))) ) {

		return FALSE;
	}

	return TRUE;
}

UINT CJsonData::GetType(CString Name) const
{
	INDEX n = m_Tree.Find(Name);

	if( !m_Tree.Failed(n) ) {

		return m_Tree[n].m_Type;
	}

	return jsonAuto;
}

UINT CJsonData::GetType(UINT uIndex) const
{
	INDEX n = m_Tree.Find(CreateName(uIndex));

	if( !m_Tree.Failed(n) ) {

		return m_Tree[n].m_Type;
	}

	return jsonAuto;
}

CString CJsonData::GetValue(CString Name) const
{
	return GetValue(Name, CString());
}

CString CJsonData::GetValue(UINT uIndex) const
{
	return GetValue(uIndex, CString());
}

CString CJsonData::GetValue(CString Name, CString Default) const
{
	if( Name.Count('.') ) {

		CJsonData *pChild = GetChild(Name.StripToken('.'));

		if( pChild ) {

			return pChild->GetValue(Name, Default);
		}
	}
	else {
		INDEX n = m_Tree.Find(Name);

		if( !m_Tree.Failed(n) ) {

			return m_Tree[n].m_Value;
		}
	}

	return Default;
}

CString CJsonData::GetValue(UINT uIndex, CString Default) const
{
	INDEX n = m_Tree.Find(CreateName(uIndex));

	if( !m_Tree.Failed(n) ) {

		return m_Tree[n].m_Value;
	}

	return Default;
}

CJsonData * CJsonData::GetChild(CString Name) const
{
	if( Name.Count('.') ) {

		CJsonData *pChild = GetChild(Name.StripToken('.'));

		if( pChild ) {

			return pChild->GetChild(Name);
		}
	}
	else {
		INDEX n = m_Tree.Find(Name);

		if( !m_Tree.Failed(n) ) {

			return m_Tree[n].m_pSub;
		}
	}

	return NULL;
}

CJsonData * CJsonData::GetChild(UINT uIndex) const
{
	INDEX n = m_Tree.Find(CreateName(uIndex));

	if( !m_Tree.Failed(n) ) {

		return m_Tree[n].m_pSub;
	}

	return NULL;
}

CString CJsonData::GetPathValue(PCTXT pPath) const
{
	PCTXT pFind = strchr(pPath, '.');

	if( pFind ) {

		CJsonData *pChild;

		if( isdigit(pPath[0]) ) {

			pChild = GetChild(atoi(pPath));
		}
		else
			pChild = GetChild(CString(pPath, pFind - pPath));

		if( pChild ) {

			return pChild->GetPathValue(pFind + 1);
		}

		return CString();
	}

	return GetValue(pPath);
}

CJsonData * CJsonData::GetPathChild(PCTXT pPath) const
{
	PCTXT pFind = strchr(pPath, '.');

	if( pFind ) {

		CJsonData *pChild;

		if( isdigit(pPath[0]) ) {

			pChild = GetChild(atoi(pPath));
		}
		else
			pChild = GetChild(CString(pPath, pFind - pPath));

		if( pChild ) {

			return pChild->GetPathChild(pFind + 1);
		}

		return NULL;
	}

	if( isdigit(pPath[0]) ) {

		return GetChild(atoi(pPath));
	}

	return GetChild(pPath);
}

// Implementation

BOOL CJsonData::ParseBlock(PCTXT &pText)
{
	UINT    uState = 0;

	CString Name, Data;

	while( *pText ) {

		TCHAR c = *pText++;

		if( isspace(c) ) {

			if( uState != 2 && uState != 6 && uState != 7 && uState != 8 ) {

				continue;
			}
		}

		switch( uState ) {

			case 0: // Look for opening brace or opening bracket.

				if( c == (m_fList ? '[' : '{') ) {

					if( m_fList ) {

						Name   = CreateName(m_uIndex++);

						uState = 5;
					}
					else
						uState = 1;

					continue;
				}

				return FALSE;

			case 1: // Look for opening quote of name or closing brace or bracket.

				if( c == (m_fList ? ']' : '}') ) {

					uState = 10;

					continue;
				}

				if( c == '\"' ) {

					Name.Empty();

					uState = 2;

					continue;
				}

				return FALSE;

			case 2: // Build up name while looking for closing quote.

				if( c == '\\' ) {

					uState = 3;

					continue;
				}

				if( c == '\"' ) {

					uState = 4;

					continue;
				}

				Name += c;

				continue;

			case 3: // Include escaped character and continue building name.

				if( c == 'u' ) {

					if( strlen(pText) >= 4 ) {

						DWORD u = strtoul(CString(pText, 4), NULL, 16);

						if( u ) {

							if( !HIBYTE(u) ) {

								Name += char(u);
							}
							else
								Name += CPrintf("&#%u;", u);
						}

						pText += 4;

						uState = 3;

						continue;
					}

					return FALSE;
				}
				else {
					PCTXT pFind;

					if( (pFind = strchr(m_cRep, c)) ) {

						Name += m_cEsc[pFind - m_cRep];
					}
					else
						Name += c;
				}

				uState = 3;

				continue;

			case 4: // Look for colon

				if( c == ':' ) {

					uState = 5;

					continue;
				}

				return FALSE;

			case 5: // Look for brace, bracket, quote or bare value.

				if( m_fList && c == ']' ) {

					m_uIndex--;

					uState = 10;

					continue;
				}

				if( c == '{' || c == '[' ) {

					pText--;

					CJsonData *pSub;

					AddChild(Name, c == '[', pSub);

					if( pSub->ParseBlock(pText) ) {

						uState = 9;

						continue;
					}

					return FALSE;
				}

				if( c == '\"' ) {

					Data.Empty();

					uState = 6;

					continue;
				}

				Data   = CString(c, 1);

				uState = 8;

				continue;

			case 6: // Build up data while looking for closing quote.

				if( c == '\\' ) {

					uState = 7;

					continue;
				}

				if( c == '\"' ) {

					AddValue(Name, Data, jsonString);

					uState = 9;

					continue;
				}

				Data += c;

				continue;

			case 7: // Include escaped character and continue building data.

				if( c == 'u' ) {

					if( strlen(pText) >= 4 ) {

						DWORD u = strtoul(CString(pText, 4), NULL, 16);

						if( !HIBYTE(u) ) {

							Data += char(u);
						}
						else
							Data += CPrintf("&#%u;", u);

						pText += 4;

						uState = 6;

						continue;
					}

					return FALSE;
				}
				else {
					PCTXT pFind;

					if( (pFind = strchr(m_cRep, c)) ) {

						Data += m_cEsc[pFind - m_cRep];
					}
					else
						Data += c;
				}

				uState = 6;

				continue;

			case 8: // Build up bare value while looking space, comma or child close.

				if( isspace(c) || c == ',' || c == '}' || c == ']' ) {

					pText--;

					if( Data == "null" ) {

						AddValue(Name, Data, jsonNull);
					}
					else
						AddValue(Name, Data, jsonNumber);

					uState = 9;

					continue;
				}

				Data += c;

				continue;

			case 9: // Look for comma or closing brace or bracket.

				if( c == ',' ) {

					if( m_fList ) {

						Name   = CreateName(m_uIndex++);

						uState = 5;
					}
					else
						uState = 1;

					continue;
				}

				if( c == (m_fList ? ']' : '}') ) {

					uState = 10;

					continue;
				}

				return FALSE;

			case 10: // Return next non-space characters to caller.

				pText--;

				return TRUE;
		}
	}

	if( uState == 10 ) {

		return TRUE;
	}

	return FALSE;
}

CString CJsonData::GetAsText(BOOL fPretty, UINT uLevel) const
{
	if( !IsEmpty() ) {

		CString t;

		t += fPretty ? (m_fList ? "[\r\n" : "{\r\n") : (m_fList ? "[" : "{");

		uLevel++;

		BOOL fFirst = TRUE;

		for( INDEX n = m_Tree.GetHead(); !m_Tree.Failed(n); m_Tree.GetNext(n) ) {

			CJsonPair const &Pair = m_Tree[n];

			if( !fFirst ) {

				t += fPretty ? ",\r\n" : ",";
			}

			if( fPretty ) {

				t += CString(' ', 2 * uLevel);
			}

			if( !m_fList ) {

				t += "\"";

				t += UniEncode(Pair.m_Name);

				t += fPretty ? "\": " : "\":";
			}

			if( Pair.m_pSub ) {

				t += Pair.m_pSub->GetAsText(fPretty, uLevel);
			}
			else {
				UINT Type = Pair.m_Type;

				if( Type == jsonAuto ) {

					Type = FindAutoType(Pair.m_Value);
				}

				if( Type == jsonNull ) {

					t += "null";
				}

				if( Type == jsonNumber ) {

					t += Pair.m_Value;
				}

				if( Type == jsonBool ) {

					t += Pair.m_Value;
				}

				if( Type == jsonString ) {

					t += "\"";

					if( Pair.m_Value.FindOne(m_cEsc) < NOTHING ) {

						CString e;

						PCTXT   v = Pair.m_Value;

						for( UINT n = 0; v[n]; v++ ) {

							PCTXT f = strchr(m_cEsc, v[n]);

							if( f ) {

								e += '\\';

								e += m_cRep[f - m_cEsc];
							}
							else
								e += v[n];
						}

						t += UniEncode(e);
					}
					else
						t += UniEncode(Pair.m_Value);

					t += "\"";
				}
			}

			fFirst = FALSE;
		}

		if( fPretty ) {

			if( !fFirst ) {

				t += "\r\n";
			}

			t += CString(' ', 2 * --uLevel);
		}

		t += m_fList ? T("]") : T("}");

		return t;
	}

	return m_fList ? T("[]") : T("{}");
}

CString CJsonData::UniEncode(CString Text) const
{
	PCTXT pHex = T("0123456789ABCDEF");

	for( ;;) {

		UINT uPos = Text.Find(T("&#"));

		if( uPos < NOTHING ) {

			PCTXT pFind = PCTXT(Text) + uPos;

			PTXT  pEnd  = NULL;

			UINT  uChar = strtoul(pFind + 2, &pEnd, 10);

			if( *pEnd == ';' ) {

				UINT uLen = pEnd - pFind + 1;

				if( uLen > 6 ) {

					Text.Delete(uPos, uLen - 6);
				}

				if( uLen < 6 ) {

					TCHAR s[] = T("0123456");

					s[6 - uLen] = 0;

					Text.Insert(uPos, s);
				}

				Text.SetAt(uPos+0, '\\');
				Text.SetAt(uPos+1, 'u');
				Text.SetAt(uPos+2, pHex[(uChar>>12)&15]);
				Text.SetAt(uPos+3, pHex[(uChar>> 8)&15]);
				Text.SetAt(uPos+4, pHex[(uChar>> 4)&15]);
				Text.SetAt(uPos+5, pHex[(uChar>> 0)&15]);

				continue;
			}
		}

		break;
	}

	return Text;
}

CString CJsonData::CreateName(UINT uIndex) const
{
	return CPrintf("%4.4X", uIndex);
}

UINT CJsonData::FindAutoType(CString const &Data) const
{
	if( !Data.IsEmpty() ) {

		if( isspace(Data[0]) || isspace(Data[Data.GetLength()-1]) ) {

			CString Text = Data;

			Text.TrimBoth();

			return IsNumber(Text) ? jsonNumber : jsonString;
		}

		return IsNumber(Data) ? jsonNumber : jsonString;
	}

	return jsonString;
}

BOOL CJsonData::IsNumber(PCTXT pText) const
{
	// Check if a string conforms to the rather strict JSON
	// numeric formatting rules. This means no leading plus, no
	// leading zeroes, no leading decimal point. If it doesn't
	// conform, we're going to have to treat it as a string.

	if( *pText == '-' ) {

		pText++;
	}

	if( isdigit(*pText) ) {

		if( *pText == '0' ) {

			pText++;
		}
		else {
			do {
				pText++;

			} while( isdigit(*pText) );
		}

		if( *pText == '.' ) {

			pText++;

			if( !isdigit(*pText) ) {

				return FALSE;
			}

			do {
				pText++;

			} while( isdigit(*pText) );
		}

		if( *pText == 'E' || *pText == 'e' ) {

			pText++;

			if( *pText == '-' || *pText == '+' ) {

				pText++;
			}

			if( !isdigit(*pText) ) {

				return FALSE;
			}

			do {
				pText++;

			} while( isdigit(*pText) );
		}

		return !*pText;
	}

	return FALSE;
}

// End of File
