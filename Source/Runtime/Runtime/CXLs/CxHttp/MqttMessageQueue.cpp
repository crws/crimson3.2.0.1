
#include "Intern.hpp"

#include "MqttMessageQueue.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MqttMessage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Message Queue
//

// Constructor

CMqttMessageQueue::CMqttMessageQueue(void)
{
	m_uLast     = 0;
	
	m_uMode     = modeNone;
	
	m_uBytes    = 0;

	m_pGuid     = NULL;
	
	m_SendIndex = NULL;
	
	m_SaveIndex = NULL;
	}

// Destructor

CMqttMessageQueue::~CMqttMessageQueue(void)
{
	while( !m_List.IsEmpty() ) {

		INDEX         Index = m_List.GetHead();
		
		CMqttMessage *pMsg = m_List.GetAt(Index);

		delete pMsg;

		m_List.Remove(Index);
		}
	}

// Attributes

BOOL CMqttMessageQueue::IsEmpty(void) const
{
	if( m_uMode > modeNone ) {

		return m_List.Failed(m_SendIndex);
		}

	return m_List.IsEmpty();
	}

BOOL CMqttMessageQueue::IsFull(void) const
{
	if( m_uMode > modeNone ) {

		if( m_uBytes >= fileLimit ) {

			return TRUE;
			}

		return FALSE;
		}

	if( m_List.GetCount() >= 4 * 60 * 60 ) {

		return TRUE;
		}

	return FALSE;
	}

CString CMqttMessageQueue::GetFilename(void) const
{
	return m_Name;
	}

BOOL CMqttMessageQueue::NeedNewFile(void) const
{
	return m_uMode == modeSave && m_uSaved >= fileLimit;
	}

UINT CMqttMessageQueue::GetCount(void) const
{
	return m_List.GetCount();
	}	

// Operations

BOOL CMqttMessageQueue::Poll(IMutex *pLock)
{
	if( m_uMode == modeSave ) {

		UpdateTime();

		// Check if we have enough data to write or if enough
		// time has passed since we last data wrote to disk.

		if( m_uBytes >= blockWrite || m_uSecs >= m_uLast + 30 ) {

			if( CreateSaveFile() ) {

				// Check for any pending kills.

				CheckForKills(pLock, TRUE);

				// Check for any pending saves.

				CheckForSaves(pLock);

				// Check for any resulting kills.

				CheckForKills(pLock, TRUE);
				}

			// And update the last save time.
			
			m_uLast = m_uSecs;

			return TRUE;
			}
		}

	if( m_uMode == modeLoad ) {

		UpdateTime();

		if( m_uSecs >= m_uLast + 30 ) {

			// Check for any pending kills.

			CheckForKills(pLock, TRUE);

			// And update the last save time.
			
			m_uLast = m_uSecs;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CMqttMessageQueue::BindToPath(CString Path, PCBYTE pGuid)
{
	m_Path  = Path;

	m_pGuid = pGuid;

	if( !m_Path.EndsWith("\\") ) {

		m_Path.Append("\\");
		}

	// TODO -- Does this need to be recursive? !!!

	mkdir(m_Path, 0);

	AfxTrace("Bound to %s\n", PCTXT(m_Path));

	return TRUE;
	}

BOOL CMqttMessageQueue::DiskLoad(CString Name)
{
	m_Name   = Name.StartsWith(m_Path) ? Name : m_Path + Name;

	m_uCount = 0;

	AfxTrace("DiskLoad %s\n", PCTXT(m_Name));

	if( OpenFile(FALSE) ) {

		if( CheckFileValid() ) {

			while( !EndOfFile(2 * sizeof(DWORD)) ) {

				DWORD dwInit = 0;

				DWORD dwSize = 0;

				DWORD dwTerm = 0;

				ReadFile(&dwInit, sizeof(dwInit));

				ReadFile(&dwSize, sizeof(dwSize));

				if( dwInit == MAKELONG(MAKEWORD('S', 'S'), MAKEWORD('S', 'S')) ) {

					if( dwSize <= blockRead ) {

						CByteArray Block;

						Block.SetCount(dwSize);

						if( !EndOfFile(dwSize + 4) ) {

							UINT uPos = GetFilePos();

							ReadFile(PVOID(Block.GetPointer()), dwSize);

							ReadFile(&dwTerm, sizeof(dwTerm));

							if( dwTerm == MAKELONG(MAKEWORD('E', 'E'), MAKEWORD('E', 'E')) ) {

								PCBYTE        pData = Block.GetPointer();

								PCBYTE        pLast = pData + Block.GetCount();
		
								CMqttMessage *pMsg  = New CMqttMessage;

								while( pData < pLast ) {

									UINT uRead = pMsg->Load(pData);

									if( pMsg->m_uCode < 255 ) {

										pMsg->m_fKill = FALSE;

										pMsg->m_uPos  = uPos;

										m_List.Append(pMsg);

										Debug(pMsg, "Load");

										pMsg = New CMqttMessage;

										m_uCount++;
										}

									uPos += uRead;
									}

								delete pMsg;

								continue;
								}
							}
						}
					}

				break;
				}

			m_SendIndex = m_List.GetHead();

			CloseFile();

			UpdateTime();

			m_uMode = modeLoad;

			m_uLast = m_uSecs;

			return TRUE;
			}

		CloseFile();

		DeleteFile();
		}

	return FALSE;
	}

BOOL CMqttMessageQueue::DiskSave(void)
{
	AfxTrace("DiskSave\n");

	m_Name.Empty();

	UpdateTime();

	m_uMode   = modeSave;

	m_uCount  = 0;

	m_uLast   = m_uSecs;

	m_uSaved  = 0;

	return TRUE;
	}

BOOL CMqttMessageQueue::FileDone(IMutex *pLock)
{
	if( m_uMode == modeLoad ) {

		// Check for any pending kills.

		CheckForKills(pLock, FALSE);

		// We can safely delete the file.

		DeleteFile();

		// Clear the mode.

		m_uMode = modeNone;

		// Always return true.

		return TRUE;
		}

	if( m_uMode == modeSave ) {

		// Check for any pending kills.

		CheckForKills(pLock, TRUE);

		// Everything from the start of the list up to the
		// save index can now be removed as it's been saved
		// to the file we're about to switch away from.

		pLock->Wait(FOREVER);

		INDEX Send = m_SendIndex;

		for(;;) {
			
			INDEX Index = m_List.GetHead();
		
			if( Index != m_SaveIndex ) {

				AfxAssert(!m_List.Failed(Index));

				if( Index == Send ) {

					m_List.GetNext(Send);
					}

				CMqttMessage *pMsg = m_List.GetAt(Index);

				Debug(pMsg, "Drop");

				delete pMsg;

				m_List.Remove(Index);

				continue;
				}

			break;
			}
		
		// When we start sending again, we shall
		// be sending from the last saved item.

		AfxAssert(m_SaveIndex == Send);

		m_SendIndex = m_SaveIndex;

		pLock->Free();

		// If the file has no data, delete it and
		// let the caller know. Otherwise, let them
		// know they can add the file to the pending
		// list so that it can be sent later.

		if( !m_uCount ) {

			DeleteFile();

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CMqttMessageQueue::AddMessage(CMqttMessage *pMsg)
{
	if( !IsFull() ) {

		// Indicate this is a new message
		// and append it to our queue.

		pMsg->m_fKill = FALSE;

		pMsg->m_uPos  = 0;

		INDEX Index   = m_List.Append(pMsg);

		if( m_uMode > modeNone ) {

			// If we're working with a file, load the two
			// queue management pointers if they are null.

			if( !m_SendIndex ) {

				m_SendIndex = Index;
				}

			if( !m_SaveIndex ) {

				m_SaveIndex = Index;
				}

			if( m_uMode == modeSave ) {

				// If we're saving to a file, update
				// the number of outstanding bytes.

				UINT size = pMsg->GetBytes();

				m_uBytes += size;

				Debug(pMsg, "Queue %u", size);
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CMqttMessageQueue::GetMessage(CMqttMessage * &pMsg)
{
	if( m_uMode > modeNone ) {

		// If we're working with a file, read the
		// message referenced by the send index.

		if( m_SendIndex ) {

			pMsg = m_List.GetAt(m_SendIndex);

			Debug(pMsg, "Send");

			return TRUE;
			}

		return FALSE;
		}
	else {
		// Otherwise, just read the first message.

		if( !m_List.IsEmpty() ) {

			pMsg = m_List.GetAt(m_List.GetHead());

			Debug(pMsg, "Send");

			return TRUE;
			}

		return FALSE;
		}
	}

BOOL CMqttMessageQueue::StepToNext(BOOL fFree)
{
	if( m_uMode > modeNone ) {

		// If we're working with a file, we are going
		// to rely on the filing thread to remove and
		// delete the message as there might be a need
		// to update the file contents.

		CMqttMessage *pMsg = m_List.GetAt(m_SendIndex);

		// Non-deleting mode is not supported.

		AfxAssert(fFree);

		// Step the send index on to the next message.

		m_List.GetNext(m_SendIndex);

		// Indicate that deletion is required.

		pMsg->m_fKill = TRUE;
		}
	else {
		// Otherwise, delete the message if we are
		// asked to and remove it from the queue.

		INDEX         Index = m_List.GetHead();
		
		CMqttMessage *pMsg = m_List.GetAt(Index);

		if( fFree ) {

			delete pMsg;
			}

		m_List.Remove(Index);
		}

	return TRUE;
	}

// Implementation

void CMqttMessageQueue::UpdateTime(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	m_uSecs = tv.tv_sec;
	}

BOOL CMqttMessageQueue::CreateSaveFile(void)
{
	if( IsMounted() ) {

		if( m_Name.IsEmpty() ) {

			m_Name = m_Path;

			m_Name.AppendPrintf("%8.8u.D%2.2u", m_uSecs / 100, m_uSecs % 100);

			if( FileExists() ) {

				DeleteFile();
				}

			if( OpenFile(TRUE) ) {

				DWORD dwMagic   = MAKELONG(MAKEWORD('M', 'Q'), MAKEWORD('T', 'T'));

				DWORD dwVersion = 1;

				WriteFile(&dwMagic, sizeof(dwMagic));

				WriteFile(&dwVersion, sizeof(dwVersion));

				WriteFile(m_pGuid, 16);

				CloseFile();

				return TRUE;
				}

			m_Name.Empty();

			return FALSE;
			}

		return TRUE;
		}

	Sleep(5000);

	return FALSE;
	}

BOOL CMqttMessageQueue::CheckFileValid(void)
{
	DWORD dwMagic   = 0;

	DWORD dwVersion = 0;

	CGuid Guid;

	ReadFile(&dwMagic, sizeof(dwMagic));

	ReadFile(&dwVersion, sizeof(dwVersion));

	ReadFile(&Guid, sizeof(Guid));

	if( dwMagic == MAKELONG(MAKEWORD('M', 'Q'), MAKEWORD('T', 'T')) ) {
		
		if( dwVersion == 1 ) {

			if( !memcmp(&Guid, m_pGuid, 16) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CMqttMessageQueue::CheckForKills(IMutex *pLock, BOOL fFile)
{
	pLock->Wait(FOREVER);

	INDEX Index = m_List.GetHead();

	BOOL  fKill = TRUE;

	BOOL  fOpen = FALSE;
			
	while( !m_List.Failed(Index) ) {

		CMqttMessage *pMsg = m_List.GetAt(Index);

		// Check to see if the message should be killed.

		if( pMsg->m_fKill ) {

			if( m_SaveIndex == Index ) {

				// If we've hit the save index, step it
				// on to the next record as there is no
				// point saving this data to disk.

				m_List.GetNext(m_SaveIndex);

				// Clear the killing assert flag.

				fKill = FALSE;
				}

			if( m_SendIndex == Index ) {

				// If we've hit the send index, we have
				// a problem as nothing before the send
				// index should have its kill flag set!

				AfxAssert(FALSE);
				}

			// Check if the message has been written.

			if( pMsg->m_uPos ) {

				Debug(pMsg, "Kill");

				// The message has been written so we
				// should not have hit our save index yet.

				AfxAssert(fKill);

				// Check to see if we should update the file.

				if( fFile ) {

					// Release the lock, kill the message in
					// the file, and claim the lock again.

					pLock->Free();

					if( fOpen || OpenFile(TRUE) ) {

						SetFilePos(pMsg->m_uPos);

						// Replace the uCode value with 255.

						BYTE bKill = 255;

						WriteFile(&bKill, sizeof(bKill));

						// Reduce the saved message count.

						AfxAssert(m_uCount);

						m_uCount--;

						// Indicate the file is open.

						fOpen = TRUE;
						}

					pLock->Wait(FOREVER);
					}
				else {
					// If not, just reduce the saved message
					// count to keep the housekeeping valid.

					AfxAssert(m_uCount);

					m_uCount--;
					}
				}
			else {
				// The message has not been written so we
				// must have hit our save index by now.

				AfxAssert(!fKill);

				// Reduce the number of outstanding bytes.

				UINT size = pMsg->GetBytes();

				m_uBytes -= size;

				Debug(pMsg, "Delete %u", size);

				AfxAssert(!HIBYTE(HIWORD(m_uBytes)));
				}

			// Save the index and step to next.

			INDEX Remove = Index;

			m_List.GetNext(Index);

			// Remove the node from the list.

			m_List.Remove(Remove);

			// Delete the message.

			delete pMsg;

			// Go around and check for more.

			continue;
			}

		// Stop on the first non-killed message.

		break;
		}

	pLock->Free();

	if( fOpen ) {

		CloseFile();

		return TRUE;
		}

	return FALSE;
	}

void CMqttMessageQueue::CheckForSaves(IMutex *pLock)
{
	if( m_uBytes ) {

		// This is where we'll build the block.

		CByteArray Block;

		// Create an array to store the element offsets.

		CArray <UINT > ListPos;

		// Find the first element that needs to be saved.

		pLock->Wait(FOREVER);

		INDEX Index = m_SaveIndex;

		while( !m_List.Failed(Index) ) {

			// Find the message.

			CMqttMessage *pMsg = m_List.GetAt(Index);

			// It should not hgave been saved yet.

			AfxAssert(!pMsg->m_uPos);

			// Save the block position.

			ListPos.Append(Block.GetCount());

			// And add it to the block.

			pMsg->Save(Block);

			Debug(pMsg, "Save");

			// Step on to the next message.

			m_List.GetNext(Index);
			}

		AfxAssert(Block.GetCount());

		// We can let the other threads access to list
		// now as we have a copy of everything we need.

		pLock->Free();

		// Open the file to write the data.

		if( OpenFile(TRUE) ) {

			// Confirm that the file is valid.

			if( !CheckFileValid() ) {

				// If not, go ahead and close it.

				CloseFile();

				// Make sure the filing system is
				// still mounted such that the failure
				// represents a genuinely bad file.

				if( IsMounted() ) {

					// If so, we need to kill this file and
					// start again with a new one. We can do
					// this by clearing the filename so that
					// the next call will recreate it.

					DeleteFile();

					m_Name.Empty();
					}

				return;
				}

			// Append to the file.

			AppendFile();

			// Prepare the various data items.

			DWORD dwInit = MAKELONG(MAKEWORD('S', 'S'), MAKEWORD('S', 'S'));

			DWORD dwSize = Block.GetCount();

			DWORD dwTerm = MAKELONG(MAKEWORD('E', 'E'), MAKEWORD('E', 'E'));

			// Write the start marker.

			if( WriteFile(&dwInit, sizeof(dwInit)) ) {

				// Write the block size.

				if( WriteFile(&dwSize, sizeof(dwSize)) ) {

					// Save the file position.

					UINT uPos = GetFilePos();

					// Write the block data.

					if( WriteFile(Block.GetPointer(), dwSize) ) {

						// Write the end marker.

						if( WriteFile(&dwTerm, sizeof(dwTerm)) ) {

							// Save the file size.

							UINT uLength = GetFilePos();

							// Close and commit the file.

							CloseFile();

							// Now reopen the file and check its length
							// to confirm the data was really committed.

							if( OpenFile(FALSE) ) {

								if( uLength == m_uLength ) {

									// All if good so acquire the lock and walk around
									// the list of block offsets so that we can update
									// the positions of the messages in the file.

									pLock->Wait(FOREVER);

									for( UINT n = 0; n < ListPos.GetCount(); n++ ) {

										// Find the message.

										CMqttMessage *pMsg = m_List.GetAt(m_SaveIndex);

										// Update the position.

										pMsg->m_uPos = uPos + ListPos[n];

										// Reduce the number of outstanding bytes.

										int size  = pMsg->GetBytes();

										m_uBytes -= size;

										Debug(pMsg, "SetPos %u", size);

										AfxAssert(!HIBYTE(HIWORD(m_uBytes)));

										// Increase the saved message count.

										m_uCount++;

										// Step on the save index.

										m_List.GetNext(m_SaveIndex);
										}

									pLock->Free();
	
									CloseFile();

									// Update the length so that we can
									// trip the file replacement check.

									m_uSaved = m_uLength;

									return;
									}
								}
							else
								return;
							}
						}
					}
				}

			CloseFile();
			}
		}
	}

void CMqttMessageQueue::Debug(CMqttMessage const *pMsg, PCTXT pAction, ...)
{
/*	va_list pArgs;

	va_start(pArgs, pAction);

	CMqttJsonData Json;

	pMsg->GetJson(Json);

	AfxTrace("%s - ", PCTXT(Json.GetValue("timestamp")));

	AfxTraceArgs(pAction, pArgs);

	AfxTrace("\n");

	va_end(pArgs);
*/	}

// Filing System Access

BOOL CMqttMessageQueue::IsMounted(void)
{
	AfxGetAutoObject(pUtils, "aeon.filesupport", 0, IFileUtilities);

	if( pUtils ) {

		return pUtils->IsDiskMounted('C');
	}

	return TRUE;
	}

BOOL CMqttMessageQueue::FileExists(void)
{
	return CAutoFile(m_Name, "r");
	}

BOOL CMqttMessageQueue::DeleteFile(void)
{
	return !unlink(m_Name);
	}

BOOL CMqttMessageQueue::OpenFile(BOOL fWrite)
{
	if( fWrite ) {

		m_File.Open(m_Name, "r+", "w+");
		}
	else
		m_File.Open(m_Name, "r");

	if( m_File ) {

		m_uLength = m_File.GetSize();

		m_File.Seek(0);

		return TRUE;
		}

	return FALSE;
	}

BOOL CMqttMessageQueue::WriteFile(PCVOID pData, UINT uSize)
{
	if( m_File ) {

		if( m_File.Write(PCBYTE(pData), uSize) == uSize ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CMqttMessageQueue::ReadFile(PVOID pData, UINT uSize)
{
	if( m_File ) {

		if( m_File.Read(PBYTE(pData), uSize) == uSize ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CMqttMessageQueue::AppendFile(void)
{
	if( m_File ) {

		m_File.SeekEnd();

		return TRUE;
		}

	return FALSE;
	}

BOOL CMqttMessageQueue::SetFilePos(UINT uPos)
{
	if( m_File ) {

		m_File.Seek(uPos);

		return TRUE;
		}

	return FALSE;
	}

UINT CMqttMessageQueue::GetFilePos(void)
{
	if( m_File ) {

		return m_File.Tell();
		}

	return 0;
	}

BOOL CMqttMessageQueue::EndOfFile(UINT uRead)
{
	if( m_File ) {

		return m_File.GetPos() + uRead > m_uLength;
		}

	return TRUE;
	}

BOOL CMqttMessageQueue::CloseFile(void)
{
	if( m_File ) {

		m_File.Close();

		return TRUE;
		}

	return FALSE;
	}

// End of File
