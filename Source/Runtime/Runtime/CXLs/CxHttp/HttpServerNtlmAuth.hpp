
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpServerNtlmAuth_HPP

#define	INCLUDE_HttpServerNtlmAuth_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpServerAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Ntlm Authentication Method
//

class DLLAPI CHttpServerNtlmAuth : public CHttpServerAuth
{
	public:
		// Constructor
		CHttpServerNtlmAuth(CString Realm, UINT uCount);

		// Destructor
		~CHttpServerNtlmAuth(void);

		// Operations
		BOOL    CanAccept(CString Meth);
		CString GetAuthHeader(CString Opaque, BOOL fStale);
	};

// End of File

#endif
