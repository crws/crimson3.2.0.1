
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpServerSession_HPP

#define	INCLUDE_HttpServerSession_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class DLLAPI CHttpServer;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Session
//

class DLLAPI CHttpServerSession
{
public:
	// Constructor
	CHttpServerSession(CHttpServer *pServer);

	// Destructor
	virtual ~CHttpServerSession(void);

	// Reference Counting
	BOOL AddRef(void);
	BOOL Release(void);

	// Attributes
	BOOL    IsDiscardable(UINT uTime) const;
	UINT    GetRefCount(void) const;
	CString GetOpaque(void) const;
	PCTXT   GetOpaquePtr(void) const;
	BOOL	HasUser(void) const;
	CString GetUser(void) const;
	CString GetReal(void) const;
	BOOL    CheckOpaque(PCTXT pOpaque) const;
	BOOL    CheckUser(CString User) const;

	// Operations
	void TickleSession(void);
	void ClearUserInfo(void);
	void SetUser(CString User);
	void SetReal(CString Real);

	// Diagnostics
	virtual UINT GetDiagColCount(void);
	virtual void GetDiagCols(IDiagOutput *pOut);
	virtual void GetDiagInfo(IDiagOutput *pOut);

	// Session List
	CHttpServerSession * m_pNext;
	CHttpServerSession * m_pPrev;

protected:
	// Data Members
	CHttpServer * m_pServer;
	UINT	      m_uRefs;
	UINT	      m_uIdle;
	CString	      m_Opaque;
	CString	      m_User;
	CString	      m_Real;

	// Overridables
	virtual void OnTickle(void);
	virtual void FreeData(void);

	// Implementation
	void MakeOpaque(void);
};

// End of File

#endif
