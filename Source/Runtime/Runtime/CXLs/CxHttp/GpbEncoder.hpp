
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_GpbEncoder_HPP

#define INCLUDE_GpbEncoder_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "GpbBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Google Protocol Buffer Encoder
//

class DLLAPI CGpbEncoder : public CGpbBase
{
public:
	// Operations
	bool Encode(CBuffer *pBuff);
	bool Encode(CByteArray &Data);
	void Reset(UINT uSize = 0);

	// Data Access
	CByteArray const & GetData(void) const;

	// Attributes
	UINT GetSize(void) const;

	// Field Operations
	bool AddFieldAsInt32(UINT uField, INT32 Value);
	bool AddFieldAsInt64(UINT uField, INT64 Value);
	bool AddFieldAsUInt32(UINT uField, UINT32 Value);
	bool AddFieldAsUInt64(UINT uField, UINT64 Value);
	bool AddFieldAsSInt32(UINT uField, SINT32 Value);
	bool AddFieldAsSInt64(UINT uField, SINT64 Value);
	bool AddFieldAsBool(UINT uField, bool Value);
	bool AddFieldAsEnum(UINT uField, int Value);
	bool AddFieldAsFixed32(UINT uField, UINT32 Value);
	bool AddFieldAsSFixed32(UINT uField, SINT32 Value);
	bool AddFieldAsFixed64(UINT uField, UINT64 Value);
	bool AddFieldAsSFixed64(UINT uField, SINT64 Value);
	bool AddFieldAsFloat(UINT uField, float Value);
	bool AddFieldAsDouble(UINT uField, double Value);
	bool AddFieldAsString(UINT uField, CString Value);
	bool AddFieldAsUnicode(UINT uField, CUnicode Value);
	bool AddMessage(UINT uField, CByteArray const &Data);
	bool AddMessage(UINT uField, CGpbEncoder const &gpb);

	// Diagnostics
	void Dump(void);

protected:
	// Data Members
	CByteArray m_Data;

	// Implementation
	void AddField(UINT uField, UINT uWire);
	UINT SetVarInt32(PBYTE pData, UINT32 Value) const;
	UINT SetVarInt64(PBYTE pData, UINT64 Value) const;
	UINT SetZigZag32(PBYTE pData, SINT32 Value) const;
	UINT SetZigZag64(PBYTE pData, SINT64 Value) const;
};

// End of File

#endif
