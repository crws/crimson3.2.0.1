

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpServerConnection_HPP

#define	INCLUDE_HttpServerConnection_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class DLLAPI CHttpServerManager;

class DLLAPI CHttpServer;

class DLLAPI CHttpServerRequest;

class DLLAPI CHttpServerAuth;

class DLLAPI CHttpServerSession;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Connection Options
//

class DLLAPI CHttpServerConnectionOptions : public CHttpConnectionOptions
{
public:
	// Constructor
	CHttpServerConnectionOptions(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Data Members
	BOOL  m_fCompReply;
	UINT  m_uMaxKeep;
	DWORD m_AllowMask;
	DWORD m_AllowAddr;
};

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Connection
//

class DLLAPI CHttpServerConnection : public CHttpConnection
{
public:
	// Constructor
	CHttpServerConnection(CHttpServerManager *pManager, CHttpServer *pServer, CHttpServerConnectionOptions const &Opts, UINT uRePort, BOOL fReTls);

	// Destructor
	~CHttpServerConnection(void);

	// Attributes
	IPADDR  GetRemote(void) const;
	UINT    GetAuthStatus(void) const;
	CString GetUser(void) const;
	BOOL    GetSession(CHttpServerSession * &pSess) const;

	// Operations
	void BindAuth(CHttpServerAuth **pMethod, UINT uMethod);
	void BindSession(CHttpServerSession *pSess);
	UINT Recv(CHttpServerRequest * &pReq);
	BOOL Send400(void);
	BOOL Send401(void);
	BOOL Send403(void);
	BOOL Send(UINT uCode);
	BOOL Send(void);
	BOOL CheckPass(CString Pass);
	BOOL ClearSessionAuth(void);
	void ClearKeepAlive(void);
	void KillRequest(void);

	// Auth Status
	enum
	{
		authNew,
		authSame,
		authStale,
		authOkay,
		authFail,
	};

protected:
	// Configuration
	CHttpServerConnectionOptions const &m_Opts;

	// Data Members
	CHttpServerManager * m_pManager;
	CHttpServer        * m_pServer;
	UINT		     m_uRePort;
	BOOL		     m_fReTls;
	CHttpServerSession * m_pSess;
	BOOL		     m_fKeep;
	IPADDR		     m_Remote;
	CHttpServerRequest * m_pReq;
	CHttpServerAuth   ** m_pMethod;
	UINT		     m_uMethod;
	CHttpServerAuth    * m_pAuth;
	UINT		     m_uAuth;
	CString		     m_User;

	// Overridables
	BOOL OnAcceptPost(IHttpStreamWrite * &pStm, CHttpRequest *pReq, PCTXT pPath);
	void OnFreeSocket(void);

	// Implementation
	BOOL    AddGzip(PCBYTE pData, UINT uData);
	BOOL    SendData(void);
	BOOL    CheckConnection(void);
	void    ResetAuth(void);
	BOOL	AddAuthHeaders(UINT uStatus);
	BOOL    ProcessAuth(CHttpServerRequest *pReq);
	void    ResetKeep(void);
	BOOL	AddKeepHeaders(void);
	BOOL    ProcessKeep(CHttpServerRequest *pReq);
	BOOL    NoBodyRequired(UINT uStatus);
	CString GetStatusText(UINT uStatus);
};

// End of File

#endif
