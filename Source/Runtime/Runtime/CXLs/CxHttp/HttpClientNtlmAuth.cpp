
#include "Intern.hpp"

#include "HttpClientNtlmAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP NTLM Authentication Method
//

// Constructor

CHttpClientNtlmAuth::CHttpClientNtlmAuth(void)
{
	}

// Operations

BOOL CHttpClientNtlmAuth::CanAccept(CString Meth, UINT &uPriority)
{
/*	if( Meth == "NTLM" ) {

		if( uPriority < priorityNTLM ) {

			uPriority = priorityNTLM;

			return TRUE;
			}
		}

*/	return FALSE;
	}

BOOL CHttpClientNtlmAuth::ProcessRequest(CString Line, BOOL fFail)
{
	return FALSE;
	}

CString CHttpClientNtlmAuth::GetAuthHeader(CString Verb, CString Path, CString Body)
{
	return "";
	}

// End of File
