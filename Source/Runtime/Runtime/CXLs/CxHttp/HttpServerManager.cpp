
#include "Intern.hpp"

#include "HttpServerManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpServerConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Manager
//

// Constructor

CHttpServerManager::CHttpServerManager(void)
{
	m_pServer = NULL;
}

// Destructor

CHttpServerManager::~CHttpServerManager(void)
{
	AfxRelease(m_pServer);
}

// Operations

BOOL CHttpServerManager::Open(void)
{
	if( m_pTls ) {

		m_pTls->CreateServerContext(m_pServer);

		if( m_pServer ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CHttpServerManager::LoadTrustedRoots(PCBYTE pRoot, UINT uRoot)
{
	if( m_pServer ) {

		if( m_pServer->LoadTrustedRoots(pRoot, uRoot) ) {

			return TRUE;
		}

		return FALSE;
	}

	AfxAssert(FALSE);

	return FALSE;
}

BOOL CHttpServerManager::LoadServerCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass)
{
	if( m_pServer ) {

		if( m_pServer->LoadServerCert(pCert, uCert, pPriv, uPriv, pPass) ) {

			return TRUE;
		}

		return FALSE;
	}

	AfxAssert(FALSE);

	return FALSE;
}

CHttpServerConnection * CHttpServerManager::CreateConnection(CHttpServer *pServer, CHttpServerConnectionOptions const &Opts)
{
	return New CHttpServerConnection(this, pServer, Opts, 0, FALSE);
}

CHttpServerConnection * CHttpServerManager::CreateRedirect(CHttpServer *pServer, CHttpServerConnectionOptions const &Opts, UINT uPort, BOOL fTls)
{
	return New CHttpServerConnection(this, pServer, Opts, uPort, fTls);
}

ISocket	* CHttpServerManager::CreateStdSocket(void)
{
	ISocket *pSock = NULL;

	AfxNewObject("sock-tcp", ISocket, pSock);

	return pSock;
}

ISocket	* CHttpServerManager::CreateTlsSocket(void)
{
	return m_pServer ? m_pServer->CreateSocket(NULL) : NULL;
}

// End of File
