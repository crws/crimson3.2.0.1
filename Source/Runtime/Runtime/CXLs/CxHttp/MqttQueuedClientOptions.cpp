
#include "Intern.hpp"

#include "MqttQueuedClientOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MQTT Queued Client Options
//

// Constructor

CMqttQueuedClientOptions::CMqttQueuedClientOptions(void)
{
	m_uBuffer = 0;

	memset(m_bGuid, 0x55, sizeof(m_bGuid));
	}

// Initialization

void CMqttQueuedClientOptions::Load(PCBYTE &pData, PCBYTE pGuid)
{
	CMqttClientOptions::Load(pData);

	m_uBuffer = GetByte(pData);

	memcpy(m_bGuid, pGuid, 16);
	}

// End of File
