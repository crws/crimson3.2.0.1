
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CxPdf_HPP

#define INCLUDE_CxPdf_HPP

//////////////////////////////////////////////////////////////////////////
//
// PDF Context
//

struct CPdfContext
{
	PCTXT   m_pFile;
	int     m_nPage;
	int     m_nRes;
	int	m_nPages;
	int	m_nWidth;
	int	m_nHeight;
	int	m_nSize;
	PBYTE	m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// PDF Result Codes
//

enum {
	pdfMemoryError	= -1,
	pdfFail		=  0,
	pdfSuccess	=  1
	};

//////////////////////////////////////////////////////////////////////////
//
// PDF APIs
//

extern void PdfInit(void);

extern UINT PdfLoadPage(CPdfContext &Ctx);

extern void PdfTerm(void);

// End of File

#endif
