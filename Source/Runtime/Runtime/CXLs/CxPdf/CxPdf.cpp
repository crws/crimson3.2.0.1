
#include "Intern.hpp"

#include "xpdf.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

//////////////////////////////////////////////////////////////////////////
//
// Client Extension Registration
//

void Register_CxPdf(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Serialization Support
//

clink void gInitMutex(void **p)
{
	IMutex * &pm = (IMutex * &) *p;

	pm = Create_Mutex();
	}

clink void gDestroyMutex(void **p)
{
	IMutex * &pm = (IMutex * &) *p;

	pm->Release();

	pm = NULL;
	}

clink void gLockMutex(void **p)
{
	IMutex * &pm = (IMutex * &) *p;

	pm->Wait(FOREVER);
	}

clink void gUnlockMutex(void **p)
{
	IMutex * &pm = (IMutex * &) *p;

	pm->Free();
	}

clink long gIncrement(long *p)
{
	return AtomicIncrement(p);
	}

clink long gDecrement(long *p)
{
	return AtomicDecrement(p);
	}

//////////////////////////////////////////////////////////////////////////
//
// PDF APIs
//

static void GuardProc(IThread *pThread, void *pData)
{
	AtomicCompAndSwap(PINT(pData), 'KPDF', 0);
	}

static GBool AbortProc(void *pData)
{
	return PINT(pData) ? gFalse : gTrue;
	}

global void PdfInit(void)
{
	globalParams = New GlobalParams(NULL);

	globalParams->setEnableFreeType("yes");

	globalParams->setAntialias("yes");
	}

global void PdfTerm(void)
{
	delete globalParams;

	globalParams = NULL;
	}

global UINT PdfLoadPage(CPdfContext &Ctx)
{
	CAutoPointer<PDFDoc> pDoc(new PDFDoc(new GString(Ctx.m_pFile)));

	if( pDoc->isOk() ) {

		SplashColor paperColor;

		paperColor[0] = 255;
		paperColor[1] = 255;
		paperColor[2] = 255;

		int volatile cnx = 'KPDF';

		GuardThread(GuardProc, PVOID(&cnx));

		CAutoPointer<SplashOutputDev> pSplash(new SplashOutputDev(splashModeBGR8, 
									  1, 
									  gFalse, 
									  paperColor
									  ));

		pSplash->startDoc(pDoc->getXRef());

		pDoc->displayPage( pSplash,
				   Ctx.m_nPage+1,
				   Ctx.m_nRes,
				   Ctx.m_nRes,
				   0,
				   gFalse,
				   gTrue,
				   gFalse,
				   AbortProc,
				   PVOID(&cnx)
				   );
		
		GuardThread(FALSE);
		
		CheckThreadCancellation();

		cnx = 0;

		Ctx.m_nPages  = pDoc->getNumPages();

		Ctx.m_nWidth  = pSplash->getBitmap()->getWidth();

		Ctx.m_nHeight = pSplash->getBitmap()->getHeight();

		Ctx.m_nSize   = pSplash->getBitmap()->getRowSize();

		PBYTE pData   = pSplash->getBitmap()->getDataPtr();

		UINT  uAlloc  = Ctx.m_nSize * Ctx.m_nHeight;

		Ctx.m_pData   = PBYTE(malloc(uAlloc));

		if( Ctx.m_pData ) {

			memcpy(Ctx.m_pData, pData, uAlloc);

			return pdfSuccess;
			}

		return pdfMemoryError;
		}

	return pdfFail;
	}

// End of File
