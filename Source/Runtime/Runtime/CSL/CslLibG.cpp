
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Creation Helper
//

#if defined(AEON_COMP_GCC)

bool CreateStaticObject(PCTXT pName, REFIID riid, void **ppObject)
{
	if( piob ) {

		int n;

		while( (n = AtomicCompAndSwap(PINT(ppObject), 0, 1)) <= 1 ) {

			if( n == 0 ) {

				IUnknown *pObject = NULL;

				piob->NewObject(pName, riid, (void **) &pObject);

				if( !pObject ) {

					*ppObject = NULL;

					return FALSE;
				}

				*ppObject = pObject;

				return true;
			}

			Sleep(10);
		}

		return true;
	}

	return false;
}

#endif

//////////////////////////////////////////////////////////////////////////
//
// LibG Global Data
//

#if defined(AEON_COMP_GCC)

extern "C"
{
	int __dso_handle = 0;
}

#endif

//////////////////////////////////////////////////////////////////////////
//
// LibG Implementation
//

#if defined(AEON_COMP_GCC)

// Data

static IMutex * m_pMutex = NULL;

// Code

clink void __cxa_pure_virtual(void)
{
	// TODO -- This is actually a fatal exit for this
	// thread and maybe more as the stack is left in
	// a horrible state... !!!

	HostBreak();
}

clink int __cxa_guard_acquire(int *p)
{
	if( CreateStaticObject("exec.qutex", AfxAeonIID(IMutex), (void **) &m_pMutex) ) {

		m_pMutex->Wait(FOREVER);
	}

	if( !(*p & 1) ) {

		*p |= 1;

		return 1;
	}

	return 0;
}

clink void __cxa_guard_release(int *p)
{
	if( m_pMutex ) {

		m_pMutex->Free();
	}
}

clink void __cxa_guard_abort(int *p)
{
	*p = 0;

	if( m_pMutex ) {

		m_pMutex->Free();
	}
}

clink void __cxa_throw_bad_array_new_length(void)
{
	HostBreak();
}

clink void __cxa_bad_typeid(void)
{
	HostBreak();
}

clink void __cxa_bad_cast(void)
{
	HostBreak();
}

clink void __aeabi_atexit(void)
{
}

namespace __gnu_cxx
{
	void __verbose_terminate_handler()
	{
		::abort();
	}
}

namespace std
{
	void abort(void)
	{
		::abort();
	}
}

#endif

// End of File
