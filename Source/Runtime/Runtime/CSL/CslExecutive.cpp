
#include "Intern.hpp"

#include <sys/reent.h>

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#define  CLIENT_APIs

#include "../HSL/HslExecutive.cpp"

// End of File
