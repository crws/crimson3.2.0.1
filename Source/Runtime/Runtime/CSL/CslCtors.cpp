
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Constructor Calls for MSVC
//

#if defined(AEON_COMP_MSVC)

// Sections

#pragma section(".CRT$XCA", read)

#pragma section(".CRT$XCZ", read)

// Markers

global DWORD __declspec(allocate(".CRT$XCA")) _xc_a = 0;

global DWORD __declspec(allocate(".CRT$XCZ")) _xc_z = 0;

// Code

BOOL CallCtors(void)
{
	PDWORD p1 = &_xc_a + 1;

	PDWORD p2 = &_xc_z;

	while( p1 < p2 ) {

		void (*pfnFunc)(void) = (void (*)(void)) *p1++;

		(*pfnFunc)();
		}

	return TRUE;
	}

#endif

//////////////////////////////////////////////////////////////////////////
//
// Constructor Calls for Older GCC
//

#if defined(AEON_COMP_GCC) && __GNUC__ <= 4

// Data

#if !defined(__INTELLISENSE__)

static DWORD ctors __attribute__ ((used, section (".ctors")));

#else

static DWORD ctors;

#endif

// Code

BOOL CallCtors(void)
{
	// The GNU CLIB uses special markers in the ctor segment
	// to deliniate things, but we can't use those source file
	// names so we have to use a different trick. We just use
	// a variable that is placed in the ctor segment, and then
	// scan backwards for 0xFFFFFFFF, which marks the start. We
	// can then walk the list and call any constructors we find.

	PDWORD p = &ctors;

	while( *--p + 1 ) {

		}

	while( *++p + 1 ) {

		if( *p ) {

			void (*pfnFunc)(void) = (void (*)(void)) *p;

			(*pfnFunc)();
			}
		}

	return TRUE;
	}

#endif

//////////////////////////////////////////////////////////////////////////
//
// Constructor Calls for Newer GCC
//

#if defined(AEON_COMP_GCC) && __GNUC__ >= 5

// Data

clink int __init_array_start;

clink int __init_array_end;

// Code

BOOL CallCtors(void)
{
	// This version of GCC uses a simpler approach owith markers
	// at the start and end of the ctor segment. We just scan the
	// list between those markers and call what we find.

	PDWORD p = PDWORD(&__init_array_start);

	PDWORD e = PDWORD(&__init_array_end);

	while( p < e ) {

		void (*pfnFunc)(void) = (void (*)(void)) *p++;

		(*pfnFunc)();
		}

	return TRUE;
	}

#endif

// End of File
