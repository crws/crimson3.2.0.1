
#include "Intern.hpp"

#include "PcapBuffer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Packet Capture Buffer
//

// Constructor

CPcapBuffer::CPcapBuffer(void)
{
	m_uLimit = 256 * 1024;

	m_pLock  = Create_Rutex();
	}

// Destructor

CPcapBuffer::~CPcapBuffer(void)
{
	AfxRelease(m_pLock);
}

// Attributes

UINT CPcapBuffer::GetSize(void) const
{
	return m_Data.GetCount();
	}

PCBYTE CPcapBuffer::GetData(void) const
{
	return m_Data.GetPointer();
	}

// Operations

void CPcapBuffer::Empty(void)
{
	m_Data.Empty();
	}

void CPcapBuffer::SetLimit(UINT uLimit)
{
	m_uLimit = uLimit;
	}

void CPcapBuffer::Initialize(UINT uNet)
{
	CAutoLock Lock(m_pLock);

	m_Data.Empty();

	struct pcap_hdr
	{
		DWORD magic_number;   /* magic number */
		WORD  version_major;  /* major version number */
		WORD  version_minor;  /* minor version number */
		DWORD thiszone;       /* GMT to local correction */
		DWORD sigfigs;        /* accuracy of timestamps */
		DWORD snaplen;        /* max length of captured packets, in octets */
		DWORD network;        /* data link type */
		};

	pcap_hdr h;

	h.magic_number  = 0xa1b2c3d4;
	h.version_major = 2;
	h.version_minor = 4;
	h.thiszone      = 0;
	h.sigfigs       = 0;
	h.snaplen       = 65536;
	h.network       = uNet;

	m_Data.Append(PCBYTE(&h), sizeof(h));
	}

BOOL CPcapBuffer::Capture(PCBYTE pData1, UINT uData1, PCBYTE pData2, UINT uData2)
{
	CAutoLock Lock(m_pLock);

	if( m_Data.GetCount() < m_uLimit ) {

		struct pcaprec_hdr
		{
			DWORD ts_sec;         /* timestamp seconds */
			DWORD ts_usec;        /* timestamp microseconds */
			DWORD incl_len;       /* number of octets of packet saved in file */
			DWORD orig_len;       /* actual length of packet */
			};

		pcaprec_hdr h;

		UINT t = ToTime(GetTickCount());

		h.ts_sec  = (t / 1000);

		h.ts_usec = (t % 1000) * 1000;

		h.incl_len = uData1 + uData2;

		h.orig_len = uData1 + uData2;

		m_Data.Append(PCBYTE(&h), sizeof(h));

		if( pData1 && uData1 ) {
		
			m_Data.Append(pData1, uData1);
			}

		if( pData2 && uData2 ) {
		
			m_Data.Append(pData2, uData2);
			}
		}

	return FALSE;
	}

BOOL CPcapBuffer::Capture(PCBYTE pData1, UINT uData1)
{
	return Capture(pData1, uData1, NULL, 0);
	}

// End of File
