
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TcpStream_HPP
	
#define	INCLUDE_TcpStream_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PcapFilter.hpp"

#include "PcapBuffer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Stream Creator
//

class DLLAPI CTcpStream : public IPacketCapture
{
	public:
		// Constructor
		CTcpStream(void);

		// Destructor
		~CTcpStream(void);

		// Operations
		PVOID CreateStream(IPREF LocAddr, WORD LocPort, IPREF RemAddr, WORD RemPort);
		void  LogStream(PVOID hStream, PCBYTE pData, UINT uData, BOOL fRecv);
		void  DeleteStream(PVOID hStream, BOOL fReset);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPacketCapture
		BOOL  METHOD IsCaptureRunning(void);
		PCSTR METHOD GetCaptureFilter(void);
		UINT  METHOD GetCaptureSize(void);
		BOOL  METHOD CopyCapture(PBYTE pData, UINT uSize);
		BOOL  METHOD StartCapture(PCSTR pFilter);
		void  METHOD StopCapture(void);
		void  METHOD KillCapture(void);

	protected:
		// TCP Context
		struct CCtx
		{
			MACADDR m_Mac;
			DWORD	m_Ip;
			DWORD	m_Seq;
			WORD	m_Port;
			WORD	m_More;
			};

		// Stream Record
		struct CStream
		{
			CCtx m_Ctx[2];
			};

		// TCP Types
		enum
		{
			tcpData = 0,
			tcpSyn  = 1,
			tcpFin  = 2,
			tcpRst  = 4,
			tcpAck  = 8,
		};

		// Data Members
		ULONG	    m_uRefs;
		bool        m_fCapture;
		CPcapFilter m_CaptFilter;
		CPcapBuffer m_CaptBuffer;
		IMutex *    m_pMutex;
		WORD        m_IpSeq;

		// Implementation
		void SetContext(CStream *pStream, int Loc, BYTE bMac, DWORD Ip, WORD Port, DWORD Seq);
		void TcpSend(CStream *pStream, int Loc, int Type, PCBYTE pData, UINT nData);
		void AddToSequence(CStream *pStream, int Loc, int Type, UINT nData);
};

// End of File

#endif
