
#include "intern.hpp"

#include "TcpStream.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Header Structures
//

#pragma pack(1)

struct MACHEADER
{
	CMacAddr m_Dest;
	CMacAddr m_Src;
	WORD	 m_Type;
	};

struct IPHEADER
{
	BYTE	m_HdrLen  : 4;
	BYTE	m_Version : 4;
	BYTE	m_Type;
	WORD	m_TotalLen;
	WORD	m_Id;
	WORD	m_Fragment;
	BYTE	m_TTL;
	BYTE	m_Protocol;
	WORD	m_Checksum;
	DWORD	m_IpSrc;
	DWORD	m_IpDest;
};

struct TCPHEADER
{
	WORD	m_LocPort;
	WORD	m_RemPort;
	DWORD   m_Seq;
	DWORD   m_Ack;

	BYTE	m_Unused : 4;
	BYTE	m_Offset : 4;

	BYTE	m_FlagFIN : 1;
	BYTE	m_FlagSYN : 1;
	BYTE	m_FlagRST : 1;
	BYTE	m_FlagPSH : 1;
	BYTE	m_FlagACK : 1;
	BYTE	m_FlagURG : 1;
	BYTE	m_FlagECN : 1;
	BYTE	m_FlagCWR : 1;

	WORD	m_Wnd;
	WORD	m_Checksum;
	WORD	m_Urgent;
};

struct FRAME
{
	MACHEADER m;
	IPHEADER  i;
	TCPHEADER t;
};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Stream Creator
//

// Constructor

CTcpStream::CTcpStream(void)
{
	StdSetRef();

	m_fCapture = NULL;
	
	m_IpSeq    = 0;
	}

// Destructor

CTcpStream::~CTcpStream(void)
{
	}

// Operations

PVOID CTcpStream::CreateStream(IPREF LocAddr, WORD LocPort, IPREF RemAddr, WORD RemPort)
{
	CStream *pStream = New CStream;

	SetContext(pStream, 0, 0x11, (DWORD &) LocAddr.m_b1, LocPort, 10000);

	SetContext(pStream, 1, 0x22, (DWORD &) RemAddr.m_b1, RemPort, 20000);

	TcpSend(pStream, 0, tcpSyn, NULL, 0);

	return pStream;
	}

void CTcpStream::LogStream(PVOID hStream, PCBYTE pData, UINT uData, BOOL fRecv)
{
	if( hStream ) {

		CStream *pStream = (CStream *) hStream;

		while( uData ) {

			UINT uLump = Min(uData, 1280u);

			TcpSend(pStream, fRecv ? 1 : 0, tcpAck, pData, uLump);

			pData += uLump;

			uData -= uLump;
			}
		}
	}

void CTcpStream::DeleteStream(PVOID hStream, BOOL fReset)
{
	if( hStream ) {

		CStream *pStream = (CStream *) hStream;

		TcpSend(pStream, 0, fReset ? (tcpRst | tcpAck) : tcpFin, NULL, 0);

		delete pStream;
		}
	}

// IUnknown

HRESULT CTcpStream::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPacketCapture);

	StdQueryInterface(IPacketCapture);

	return E_NOINTERFACE;
	}

ULONG CTcpStream::AddRef(void)
{
	StdAddRef();
	}

ULONG CTcpStream::Release(void)
{
	StdRelease();
	}

// IPacketCapture

BOOL CTcpStream::IsCaptureRunning(void)
{
	return m_fCapture ? TRUE : FALSE;
	}

PCSTR CTcpStream::GetCaptureFilter(void)
{
	return m_CaptFilter.GetFilter();
	}

UINT CTcpStream::GetCaptureSize(void)
{
	return m_fCapture ? 0 : m_CaptBuffer.GetSize();
	}

BOOL CTcpStream::CopyCapture(PBYTE pData, UINT uSize)
{
	if( !m_fCapture ) {
		
		UINT uUsed = m_CaptBuffer.GetSize();

		if( uUsed && uSize <= uUsed ) {

			memcpy(pData, m_CaptBuffer.GetData(), uSize);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTcpStream::StartCapture(PCSTR pFilter)
{
	if( !m_fCapture ) {

		if( m_CaptFilter.Parse(pFilter) ) {

			m_CaptBuffer.Initialize(1);

			m_fCapture = true;

			return TRUE;
			}

		m_CaptBuffer.Empty();
		}

	return FALSE;
	}

void CTcpStream::StopCapture(void)
{
	m_fCapture = false;
	}

void CTcpStream::KillCapture(void)
{
	m_fCapture = false;

	m_CaptBuffer.Empty();
	}

// Implementation

void CTcpStream::SetContext(CStream *pStream, int Loc, BYTE bMac, DWORD Ip, WORD Port, DWORD Seq)
{
	pStream->m_Ctx[Loc].m_Ip   = Ip;

	pStream->m_Ctx[Loc].m_Port = Port;

	pStream->m_Ctx[Loc].m_Seq  = Seq;

	memset(pStream->m_Ctx[Loc].m_Mac.m_Addr + 1, bMac, 5);

	pStream->m_Ctx[Loc].m_Mac.m_Addr[0] = 0;
	}

void CTcpStream::TcpSend(CStream *pStream, int Loc, int Type, PCBYTE pData, UINT nData)
{
	if( m_fCapture ) {

		int  Rem     = 1 - Loc;

		WORD LocPort = HostToNet(pStream->m_Ctx[Loc].m_Port);

		WORD RemPort = HostToNet(pStream->m_Ctx[Rem].m_Port);

		if( m_CaptFilter.StoreTcp(LocPort, RemPort, Loc == 0) ) {

			FRAME p;

			p.m.m_Src  = pStream->m_Ctx[Loc].m_Mac;
			p.m.m_Dest = pStream->m_Ctx[Rem].m_Mac;
			p.m.m_Type = HostToNet(WORD(0x800));

			p.i.m_HdrLen   = sizeof(p.i) / sizeof(DWORD);
			p.i.m_Version  = 4;
			p.i.m_Type     = 0;
			p.i.m_TotalLen = HostToNet(WORD(sizeof(p.i) + sizeof(p.t) + nData));
			p.i.m_Id       = HostToNet(m_IpSeq++);
			p.i.m_Fragment = 0;
			p.i.m_TTL      = 128;
			p.i.m_Protocol = 6;
			p.i.m_Checksum = 0;
			p.i.m_IpSrc    = pStream->m_Ctx[Loc].m_Ip;
			p.i.m_IpDest   = pStream->m_Ctx[Rem].m_Ip;

			DWORD Ack = (Type & tcpAck) ? pStream->m_Ctx[Rem].m_Seq : 0;

			p.t.m_LocPort  = LocPort;
			p.t.m_RemPort  = RemPort;
			p.t.m_Seq      = HostToNet(pStream->m_Ctx[Loc].m_Seq);
			p.t.m_Ack      = HostToNet(Ack);
			p.t.m_FlagFIN  = (Type & tcpFin) ? 1 : 0;
			p.t.m_FlagSYN  = (Type & tcpSyn) ? 1 : 0;
			p.t.m_FlagRST  = (Type & tcpRst) ? 1 : 0;
			p.t.m_FlagPSH  = 0;
			p.t.m_FlagACK  = (Type & tcpAck) ? 1 : 0;
			p.t.m_FlagURG  = 0;
			p.t.m_FlagECN  = 0;
			p.t.m_FlagCWR  = 0;
			p.t.m_Unused   = 0;
			p.t.m_Offset   = WORD(sizeof(p.t) / sizeof(DWORD));
			p.t.m_Wnd      = HostToNet(WORD(65000));
			p.t.m_Checksum = 0;
			p.t.m_Urgent   = 0;

			AddToSequence(pStream, Loc, Type, nData);

			m_CaptBuffer.Capture(PCBYTE(&p), sizeof(p), pData, nData);

			if( !(Type & tcpAck) && (Type & (tcpSyn | tcpFin)) ) {

				TcpSend(pStream, Rem, Type | tcpAck, NULL, 0);

				TcpSend(pStream, Loc, tcpAck, NULL, 0);
				}

			return;
			}
		}

	AddToSequence(pStream, Loc, Type, nData);
	}

void CTcpStream::AddToSequence(CStream *pStream, int Loc, int Type, UINT nData)
{
	if( Type & tcpSyn ) {

		nData++;
		}

	if( Type & tcpFin ) {

		nData++;
		}

	pStream->m_Ctx[Loc].m_Seq += nData;
	}

// End of File
