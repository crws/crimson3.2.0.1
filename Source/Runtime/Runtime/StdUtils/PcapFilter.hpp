
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PcapFilter_HPP

#define	INCLUDE_PcapFilter_HPP

//////////////////////////////////////////////////////////////////////////
//
// Packet Capture Filter
//

class DLLAPI CPcapFilter
{
	public:
		// Constructor
		CPcapFilter(void);

		// Attributes
		CString GetFilter(void) const;

		// Operations
		void Clear(void);
		BOOL Parse(CString Filter);

		// Filtering
		BOOL StoreTcp(WORD wLocPort, WORD wRemPort, BOOL fSend) const;
		BOOL StoreUdp(WORD wLocPort, WORD wRemPort, BOOL fSend) const;
		BOOL StoreArp(BOOL fSend) const;
		BOOL StoreIcmp(BOOL fSend) const;

	protected:
		// Data Members
		CString m_Filter;
		WORD	m_wTcp;
		WORD	m_wUdp;
		BOOL	m_fArp;
		BOOL    m_fIcmp;
		BOOL	m_fWeb;
	};

// End of File

#endif
