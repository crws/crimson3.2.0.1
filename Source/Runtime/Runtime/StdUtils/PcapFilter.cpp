
#include "Intern.hpp"

#include "PcapFilter.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Packet Capture Filter
//

// Constructor

CPcapFilter::CPcapFilter(void)
{
	m_Filter = "none";

	Clear();
	}

// Attributes

CString CPcapFilter::GetFilter(void) const
{
	return m_Filter;
	}

// Operations

void CPcapFilter::Clear(void)
{
	m_wTcp  = 0;

	m_wUdp  = 0;

	m_fArp  = FALSE;

	m_fIcmp = FALSE;

	m_fWeb  = FALSE;
	}

BOOL CPcapFilter::Parse(CString Filter)
{
	if( Filter.IsEmpty() || Filter == "none" ) {

		Clear();

		m_Filter = "none";
		}
	else {
		m_Filter = Filter;

		while( !Filter.IsEmpty() ) {

			CString Data = Filter.StripToken(';');

			CString Name = Data.StripToken('=');

			if( Name == "tcp" ) {

				m_wTcp = (Data.IsEmpty() || Data == "all") ? 0xFFFF : HostToNet(WORD(atoi(Data)));
				}

			if( Name == "udp" ) {

				m_wUdp = (Data.IsEmpty() || Data == "all") ? 0xFFFF : HostToNet(WORD(atoi(Data)));
				}

			if( Name == "icmp" ) {

				m_fIcmp = TRUE;
				}

			if( Name == "arp" ) {

				m_fArp = TRUE;
				}

			if( Name == "web" ) {

				m_fWeb = TRUE;
				}
			}
		}

	return TRUE;
	}

// Filtering

BOOL CPcapFilter::StoreTcp(WORD wLocPort, WORD wRemPort, BOOL fSend) const
{
	if( m_wTcp ) {

		if( m_wTcp == 0xFFFF ) {

			if( !m_fWeb ) {

				WORD wPort = NetToHost(fSend ? wLocPort : wRemPort);

				if( wPort == 80 || wPort == 443 || wPort == 8080 || wPort == 4443 ) {

					return FALSE;
					}
				}
				
			return TRUE;
			}
			
		if( wRemPort == m_wTcp || wLocPort == m_wTcp ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPcapFilter::StoreUdp(WORD wLocPort, WORD wRemPort, BOOL fSend) const
{
	if( m_wUdp ) {

		if( m_wUdp == 0xFFFF || wRemPort == m_wUdp || wLocPort == m_wUdp ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPcapFilter::StoreArp(BOOL fSend) const
{
	return m_fArp;
	}

BOOL CPcapFilter::StoreIcmp(BOOL fSend) const
{
	return m_fIcmp;
	}

// End of File
