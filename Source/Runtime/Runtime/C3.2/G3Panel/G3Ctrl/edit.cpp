
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Static Text Control
//

class CEditControl : public CStdControl, public IEditControl, public IContainer
{
	public:
		// Constructor
		CEditControl(void);

		// Destructor
		~CEditControl(void);

		// Management
		void Release(void);
		
		// Creation
		void Create( IContainer * pParent,
			     IManager   * pManager,
			     R2 const   & Rect,
			     UINT	  uID,
			     UINT	  uFlags,
			     UINT	  uStyle
			     );

		// Drawing
		void Draw( IManager *pManager,
			   UINT      uCode
			   );

		// Hit Testing
		BOOL HitTest(IRegion *pDirty);
		BOOL HitTest(P2 const &Point);

		// Core Attributes
		void GetRect(R2 &Rect);

		// Core Operations
		void Enable(BOOL fEnable);

		// Messages
		BOOL OnMessage(UINT uCode, DWORD dwParam);

		// Text Operations
		void SetText(PCUTF pText);
		void GetText(PUTF  pText);

		// Notification
		void OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData);

		// Style Access
		void GetStyle(IStyle * &pStyle);

	protected:
		// Visual Style
		IGdiFont * m_pFont;
		COLOR	   m_colBorder;
		COLOR	   m_colSelectFore;
		COLOR	   m_colSelectBack;
		COLOR	   m_colTextFore;
		COLOR	   m_colTextBack;
		COLOR	   m_colTextGrey;

		// State Data
		WORD m_sText[maxString];
		WORD m_sEdit[maxString];
		BOOL m_fFocus;
		BOOL m_fShowReq;
		BOOL m_fShowAct;
		BOOL m_fEat;

		// UI Manager
		IManager *m_pManager;

		// Implementation
		void DefStyle(void);
		void GetStyle(void);
		void Draw(IGdi *pGDI);
		void ShowKeyPad(void);
		void HideKeyPad(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Static Text Control
//

// Instantiator

IEditControl * Create_EditControl(void)
{
	return New CEditControl;
	}

// Constructor

CEditControl::CEditControl(void)
{
	m_fEnable  = TRUE;

	m_fFocus   = FALSE;

	m_fShowReq = FALSE;

	m_fShowAct = FALSE;

	m_fEat     = FALSE;

	SetText(L"Edit");

	DefStyle();
	}

// Destructor

CEditControl::~CEditControl(void)
{
	}

// Management

void CEditControl::Release(void)
{
	delete this;
	}

// Creation

void CEditControl::Create(IContainer *pParent, IManager *pManager, R2 const &Rect, UINT uID, UINT uFlags, UINT uStyle)
{
	m_pParent  = pParent;
	
	m_pManager = pManager;

	m_Rect     = Rect;

	m_uID      = uID;

	m_uFlags   = uFlags;

	m_uStyle   = uStyle;

	GetStyle();
	}

// Drawing

void CEditControl::Draw(IManager *pManager, UINT uCode)
{
	if( uCode == drawInitial || m_fDirty ) {

		IGdi *pGDI = pManager->GetGDI();

		Draw(pGDI);

		pManager->GetDirtyRegion()->AddRect(m_Rect);

		m_fDirty = FALSE;
		}

	if( m_fShowAct - m_fShowReq ) {

		if( m_fShowAct = m_fShowReq ) {

			pManager->ShowPopup(m_pKeyPad, TRUE);
			}
		else
			pManager->HidePopup();
		}
	}

// Hit Testing

BOOL CEditControl::HitTest(IRegion *pDirty)
{
	return pDirty->HitTest(m_Rect);
	}

BOOL CEditControl::HitTest(P2 const &Point)
{
	return m_fEnable && PtInRect(m_Rect, Point);
	}

// Messages

BOOL CEditControl::OnMessage(UINT uCode, DWORD dwParam)
{
	if( uCode == msgSkipFocus ) {

		return !m_fEnable;
		}

	if( uCode == msgSetFocus ) {

		m_fFocus = TRUE;

		m_fEat   = dwParam ? TRUE : FALSE;

		m_fDirty = TRUE;

		return TRUE;
		}

	if( uCode == msgKillFocus ) {

		m_fFocus = FALSE;

		m_fDirty = TRUE;
		
		HideKeyPad();

		return TRUE;
		}

	if( uCode == msgTouchDown ) {

		if( m_fEat ) {

			m_fEat = FALSE;

			return FALSE;
			}

		return TRUE;
		}

	if( uCode == msgTouchUp ) {

		ShowKeyPad();

		return TRUE;
		}

	if( uCode == msgKeyDown ) {

		if( dwParam == 0x0D ) {

			wstrcpy(m_sText, m_sEdit);

			m_fDirty = TRUE;

			HideKeyPad();

			SendNotify(0, 0);

			return TRUE;
			}

		if( dwParam == 0x1B ) {

			HideKeyPad();

			return TRUE;
			}

		if( dwParam == 0x7F ) {

			if( m_sEdit[0] ) {

				m_sEdit[wstrlen(m_sEdit)-1] = 0;

				m_pKeyPad->SetPadText(m_sEdit);

				return TRUE;
				}

			return FALSE;
			}

		if( dwParam >= 0x20 && dwParam != 0x7F ) {

			UINT n = wstrlen(m_sEdit);

			m_sEdit[n+0] = WORD(dwParam);

			m_sEdit[n+1] = 0;

			m_pKeyPad->SetPadText(m_sEdit);

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

// Core Attributes

void CEditControl::GetRect(R2 &Rect)
{
	Rect = m_Rect;
	}

// Core Operations

void CEditControl::Enable(BOOL fEnable)
{
	if( m_fEnable - fEnable ) {

		m_fEnable = fEnable;

		m_fDirty  = TRUE;
		}
	}

// Text Operations

void CEditControl::SetText(PCUTF pText)
{
	if( wstrcmp(m_sText, pText) ) {

		wstrcpy(m_sText, pText);

		m_fDirty = TRUE;
		}
	}

void CEditControl::GetText(PUTF pText)
{
	wstrcpy(pText, m_sText);
	}

// Notification

void CEditControl::OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData)
{
	}

// Style Access

void CEditControl::GetStyle(IStyle * &pStyle)
{
	if( m_pParent ) {
		
		m_pParent->GetStyle(pStyle);
		}
	}

// Implementation

void CEditControl::DefStyle(void)
{
	m_colBorder     = GetRGB(0,0,0);

	m_colSelectFore = GetRGB( 0, 0, 0);
	
	m_colSelectBack = GetRGB(18,18,24);

	m_colTextFore   = GetRGB( 0, 0, 0);
	
	m_colTextBack   = GetRGB(31,31,31);

	m_colTextGrey   = GetRGB(15,15,15);

	m_pFont         = NULL;
	}

void CEditControl::GetStyle(void)
{
	GetStyleColor (colButtonBorder, m_colBorder);

	GetStyleColor (colSelectFore,   m_colSelectFore);
	
	GetStyleColor (colSelectBack,   m_colSelectBack);

	GetStyleColor (colTextFore,     m_colTextFore);

	GetStyleColor (colTextBack,     m_colTextBack);

	GetStyleColor (colTextGrey,     m_colTextGrey);

	GetStyleFont  (fontButton,      m_pFont);
	}

void CEditControl::Draw(IGdi *pGDI)
{
	pGDI->SetBrushFore(m_colTextBack);

	pGDI->SetPenFore  (m_colBorder);

	pGDI->FillRect(PassRect(m_Rect));

	pGDI->DrawRect(PassRect(m_Rect));

	pGDI->SelectFont(m_pFont);

	int cy = pGDI->GetTextHeight("X");

	int np = 2;

	int xp = m_Rect.x1 + 2;

	int yp = m_Rect.y1 + (m_Rect.y2 - m_Rect.y1 - cy - 2 * np) / 2;
	
	if( m_fEnable ) {

		if( m_fFocus ) {

			pGDI->SetTextFore (m_colSelectFore);

			pGDI->SetTextBack (m_colSelectBack);

			pGDI->SetBrushFore(m_colSelectBack);
			}
		else {
			pGDI->SetTextFore (m_colTextFore);

			pGDI->SetTextBack (m_colTextBack);

			pGDI->SetBrushFore(m_colTextBack);
			}
		}
	else {
		pGDI->SetTextFore (m_colTextGrey);

		pGDI->SetTextBack (m_colTextBack);

		pGDI->SetBrushFore(m_colTextBack);
		}

	if( m_sText[0] ) {

		// REV3 -- Clip text horizontally?

		WORD sText[maxString];

		MakeNarrow(sText, m_sText);

		int cx = pGDI->GetTextWidth(sText);

		int x3 = xp + cx + 1;

		pGDI->TextOut(xp, yp + np, sText);

		pGDI->FillRect(xp, yp, x3, yp + np);

		pGDI->FillRect(xp, yp + cy + np, x3, yp + cy + 2 * np);

		pGDI->FillRect(xp + cx, yp + np, x3, yp + cy + np);
		}
	}

void CEditControl::ShowKeyPad(void)
{
	if( !m_fShowReq ) {

		MakeKeyPad(m_pManager, 0);

		wstrcpy(m_sEdit, m_sText);

		m_pKeyPad->SetPadHost(this);

		m_pKeyPad->SetPadText(m_sEdit);

		m_fShowReq = TRUE;

		m_fDirty   = TRUE;
		}
	}

void CEditControl::HideKeyPad(void)
{
	if( m_fShowReq ) {

		m_fShowReq = FALSE;

		m_fDirty   = TRUE;
		}
	}

// End of File
