
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Progress Bar Control
//

class CProgressBar : public CStdControl, public IProgressBar
{
	public:
		// Constructor
		CProgressBar(void);

		// Destructor
		~CProgressBar(void);

		// Management
		void Release(void);
		
		// Creation
		void Create( IContainer * pParent,
			     IManager   * pManager,
			     R2 const   & Rect,
			     UINT	  uID,
			     UINT	  uFlags,
			     UINT	  uStyle
			     );

		// Drawing
		void Draw( IManager *pManager,
			   UINT      uCode
			   );

		// Hit Testing
		BOOL HitTest(IRegion *pDirty);
		BOOL HitTest(P2 const &Point);

		// Core Attributes
		void GetRect(R2 &Rect);

		// Core Operations
		void Enable(BOOL fEnable);

		// Messages
		BOOL OnMessage(UINT uCode, DWORD dwParam);

		// Progress Operations
		void SetValue(int nValue);
		void SetRange(int nMin, int nMax);
		int  GetValue(void);

	protected:
		// Shader Reference
		static CProgressBar * m_pObj;

		// Core Data
		BOOL m_fHorz;

		// Visual Style
		COLOR m_colBorder;
		COLOR m_colProgBack;
		COLOR m_colProgShadeA;
		COLOR m_colProgShadeB;

		// State Data
		int  m_nValue;
		int  m_nMin;
		int  m_nMax;

		// Implementation
		void DefStyle(void);
		void GetStyle(void);
		void Draw(IGdi *pGDI);
		void ClipValue(void);

		// Shaders
		static BOOL Shader1(IGdi *pGDI, int p, int c);
		static BOOL Shader2(IGdi *pGDI, int p, int c);
	};

//////////////////////////////////////////////////////////////////////////
//
// Progress Bar Control
//

// Instantiator

IProgressBar * Create_ProgressBar(void)
{
	return New CProgressBar;
	}

// Shader Reference

CProgressBar * CProgressBar::m_pObj = NULL;

// Constructor

CProgressBar::CProgressBar(void)
{
	m_nValue = 0;

	m_nMin   = 0;

	m_nMax   = 100;

	DefStyle();
	}

// Destructor

CProgressBar::~CProgressBar(void)
{
	}

// Management

void CProgressBar::Release(void)
{
	delete this;
	}

// Creation

void CProgressBar::Create(IContainer *pParent, IManager *pManager, R2 const &Rect, UINT uID, UINT uFlags, UINT uStyle)
{
	m_pParent = pParent;

	m_Rect    = Rect;

	m_uID     = uID;

	m_uFlags  = uFlags;

	m_uStyle  = uStyle;

	m_fHorz   = ((Rect.x2-Rect.x1) > (Rect.y2-Rect.y1));

	GetStyle();
	}

// Drawing

void CProgressBar::Draw(IManager *pManager, UINT uCode)
{
	if( uCode == drawInitial || m_fDirty ) {

		IGdi *pGDI = pManager->GetGDI();

		Draw(pGDI);

		pManager->GetDirtyRegion()->AddRect(m_Rect);

		m_fDirty = FALSE;
		}
	}

// Hit Testing

BOOL CProgressBar::HitTest(IRegion *pDirty)
{
	return pDirty->HitTest(m_Rect);
	}

BOOL CProgressBar::HitTest(P2 const &Point)
{
	return FALSE;
	}

// Core Attributes

void CProgressBar::GetRect(R2 &Rect)
{
	Rect = m_Rect;
	}

// Core Operations

void CProgressBar::Enable(BOOL fEnable)
{
	if( m_fEnable - fEnable ) {

		m_fEnable = fEnable;

		m_fDirty  = TRUE;
		}
	}

// Messages

BOOL CProgressBar::OnMessage(UINT uCode, DWORD dwParam)
{
	return FALSE;
	}

// Progress Operations

void CProgressBar::SetValue(int nValue)
{
	m_nValue = nValue;

	// REV3 -- Check for change at pixel level?

	m_fDirty = TRUE;

	ClipValue();
	}

void CProgressBar::SetRange(int nMin, int nMax)
{
	m_nMin   = nMin;

	m_nMax   = nMax;

	m_fDirty = TRUE;

	ClipValue();
	}

int CProgressBar::GetValue(void)
{
	return m_nValue;
	}

// Implementation

void CProgressBar::DefStyle(void)
{
	m_colBorder     = GetRGB( 0, 0, 0);

	m_colProgBack   = GetRGB(28,28,28);

	m_colProgShadeA = GetRGB( 0, 0, 8);

	m_colProgShadeB = GetRGB( 0, 0,31);
	}

void CProgressBar::GetStyle(void)
{
	GetStyleColor(colBorder,     m_colBorder    );

	GetStyleColor(colProgBack,   m_colProgBack  );

	GetStyleColor(colProgShadeA, m_colProgShadeA);

	GetStyleColor(colProgShadeB, m_colProgShadeB);
	}

void CProgressBar::Draw(IGdi *pGDI)
{
	pGDI->SetBrushFore(m_colProgBack);

	pGDI->SetPenFore  (m_colBorder);

	pGDI->FillRect(PassRect(m_Rect));

	pGDI->DrawRect(PassRect(m_Rect));

	if( m_fEnable ) {

		int xp, yp, cx, cy;

		if( m_fHorz ) {
		
			m_pObj = this;

			int y1 = m_Rect.y1 + 2;

			int y2 = m_Rect.y2 - 2;

			int x1 = m_Rect.x1 + 2;

			int x2 = m_Rect.x2 - 2;

			xp = x1;

			cx = (x2 - x1) * (m_nValue - m_nMin) / (m_nMax - m_nMin);

			yp = y1;

			cy = y2 - y1;

			pGDI->ShadeRect(xp, yp, xp+cx, yp+cy, Shader1);
			}
		else {
			m_pObj = this;

			int x1 = m_Rect.x1 + 2;

			int x2 = m_Rect.x2 - 2;

			int y1 = m_Rect.y1 + 2;

			int y2 = m_Rect.y2 - 2;

			xp = x1;

			cx = x2 - x1;

			yp = y1;

			cy = (y2 - y1) * (m_nValue - m_nMin) / (m_nMax - m_nMin);

			pGDI->ShadeRect(xp, yp, xp+cx, yp+cy, Shader2);
			}
		}
	}

void CProgressBar::ClipValue(void)
{
	MakeMax(m_nValue, m_nMin);

	MakeMin(m_nValue, m_nMax);
	}

// Shaders

BOOL CProgressBar::Shader1(IGdi *pGDI, int p, int c)
{
	static COLOR q = 0;

	if( c ) {

		COLOR k = Mix( m_pObj->m_colProgShadeB,
			       m_pObj->m_colProgShadeA,
			       abs(c/2-p),
			       c/2
			       );

		if( k - q ) {

			pGDI->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = -1;

	return FALSE;
	}

BOOL CProgressBar::Shader2(IGdi *pGDI, int p, int c)
{
	static COLOR q = 0;

	if( c ) {

		COLOR k = Mix( m_pObj->m_colProgShadeB,
			       m_pObj->m_colProgShadeA,
			       abs(c/2-p),
			       c/2
			       );

		if( k - q ) {

			pGDI->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = -1;

	return TRUE;
	}

// End of File
