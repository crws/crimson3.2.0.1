
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Push Button Control
//

class CPushButton : public CStdButton
{
	public:
		// Constructor
		CPushButton(void);

		// Destructor
		~CPushButton(void);

		// Messages
		UINT OnMessage(UINT uCode, UINT uParam);

	protected:
		// State Data
		BOOL m_fDefault;

		// Overridables
		void OnDraw    (IGdi *pGDI);
		void OnCreate  (IGdi *pGDI);
		void OnValidate(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Push Button Control
//

// Instantiator

IButton * Create_PushButton(void)
{
	return New CPushButton;
	}

// Constructor

CPushButton::CPushButton(void)
{
	m_fDefault = FALSE;
	}

// Destructor

CPushButton::~CPushButton(void)
{
	}

// Messages

UINT CPushButton::OnMessage(UINT uCode, UINT uParam)
{
	UINT uMessage = LOWORD(uCode);

	BOOL fLocal   = HIWORD(uCode);

	if( uMessage == msgSkipFocus ) {

		return !m_fEnable;
		}

	if( uMessage == msgTouchDown ) {

		m_fPress = TRUE;

		m_fDirty = TRUE;

		if( !(m_uFlags & bsLatch) ) {

			SendNotify(uCode, 0);
			}

		return TRUE;
		}

	if( m_fPress ) {

		if( uMessage == msgTouchRepeat ) {

			m_fDirty = TRUE;

			if( !(m_uFlags & bsLatch) ) {

				SendNotify(uCode, 0);
				}

			return TRUE;
			}

		if( uMessage == msgTouchUp ) {

			m_fPress = FALSE;

			m_fDirty = TRUE;

			if( m_uFlags & bsLatch ) {

				m_uState = !m_uState;

				SendNotify(0, m_uState);
				}
			else
				SendNotify(0, 0);

			SendNotify(uCode, 0);

			return TRUE;
			}
		}

	return FALSE;
	}

// Overridables

void CPushButton::OnCreate(IGdi *pGDI)
{
	m_fDefault = (m_uFlags & bsDefault) ? TRUE : FALSE;
	}

void CPushButton::OnDraw(IGdi *pGDI)
{
	pGDI->ResetAll();

	R2 Rect = m_Rect;

	if( m_uFlags & bsThick ) {

		pGDI->SetPenWidth(3);

		DeflateRect(Rect, 1, 1);
		}

	pGDI->SetPenFore  (m_colBorder);

	pGDI->ShadeRounded(PassRect(Rect), 8, GetShader(m_uState));
	
	pGDI->DrawRounded (PassRect(Rect), 8);

	pGDI->SelectFont  (m_pFont);

	pGDI->SetTextTrans(modeTransparent);

	pGDI->SetTextFore (m_fEnable ? m_colText : m_colDisabled);

	int cx = pGDI->GetTextWidth (m_sText);
	
	int cy = pGDI->GetTextHeight(m_sText);

	int xp = Rect.x1 + (Rect.x2 - Rect.x1 - cx) / 2;

	int yp = Rect.y1 + (Rect.y2 - Rect.y1 - cy) / 2;

	pGDI->TextOut(xp, yp, m_sText);
	}

void CPushButton::OnValidate(void)
{
	m_uState = m_uState ? 1 : 0;
	}

// End of File
