
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Drop Down List Control
//

class CDropDownList : public CStdControl, public IDropDownList, public IContainer
{
	public:
		// Constructor
		CDropDownList(void);

		// Destructor
		~CDropDownList(void);

		// Management
		void Release(void);
		
		// Creation
		void Create( IContainer * pParent,
			     IManager   * pManager,
			     R2 const   & Rect,
			     UINT	  uID,
			     UINT	  uFlags,
			     UINT	  uStyle
			     );

		// Drawing
		void Draw( IManager *pManager,
			   UINT      uCode
			   );

		// Hit Testing
		BOOL HitTest(IRegion *pDirty);
		BOOL HitTest(P2 const &Point);

		// Core Attributes
		void GetRect(R2 &Rect);

		// Core Operations
		void Enable(BOOL fEnable);

		// Messages
		BOOL OnMessage(UINT uCode, DWORD dwParam);

		// List Operations
		UINT  AddString(PCTXT pText, DWORD dwParam);
		UINT  AddString(PCUTF pText, DWORD dwParam);
		UINT  GetSelect(void);
		DWORD GetSelectData(void);
		UINT  GetText(PTXT pText, UINT uItem);
		UINT  GetText(PUTF pText, UINT uItem);
		void  SetSelect(UINT uSelect);
		void  SelectByParam(DWORD dwParam);
		void  SortData(void);

		// Drop Down Operations
		void ShowDropDown(void);
		void HideDropDown(void);

		// Notification
		void OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData);

		// Style Access
		void GetStyle(IStyle * &pStyle);

	protected:
		// Visual Style
		int   m_nSize;
		int   m_nHeight;
		int   m_nScreen;
		COLOR m_colBorder;
		COLOR m_colSelectFore;
		COLOR m_colSelectBack;
		COLOR m_colTextFore;
		COLOR m_colTextBack;
		COLOR m_colTextGrey;

		// Children
		IControl   *m_pTouch;
		IButton    *m_pButton;
		IListBox   *m_pListBox;

		// Layout
		R2 m_TextRect;
		R2 m_DropRect;
		R2 m_ListRect;

		// State
		BOOL m_fFocus;
		BOOL m_fShowReq;
		BOOL m_fShowAct;
		BOOL m_fEat;

		// Implementation
		void DefStyle(void);
		void GetStyle(void);
		void Draw(IGdi *pGDI);
		void FlipState(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Drop Down List Control
//

// Instantiator

IDropDownList * Create_DropDownList(void)
{
	return New CDropDownList;
	}

// Constructor

CDropDownList::CDropDownList(void)
{
	m_pTouch   = NULL;

	m_pButton  = Create_IconButton();

	m_pListBox = Create_ListBox();

	m_fShowReq = FALSE;

	m_fShowAct = FALSE;

	m_fFocus   = FALSE;

	m_fEat     = FALSE;

	DefStyle();
	}

// Destructor

CDropDownList::~CDropDownList(void)
{
	m_pButton->Release();

	m_pListBox->Release();
	}

// Management

void CDropDownList::Release(void)
{
	delete this;
	}

// Creation

void CDropDownList::Create(IContainer *pParent, IManager *pManager, R2 const &Rect, UINT uID, UINT uFlags, UINT uStyle)
{
	m_pParent = pParent;

	m_Rect    = Rect;

	m_uID     = uID;

	m_uFlags  = uFlags;

	m_uStyle  = uStyle;

	GetStyle();

	m_TextRect.x1 = Rect.x1 + 0;
	m_TextRect.y1 = Rect.y1 + 0;
	m_TextRect.x2 = Rect.x2 - 2 - m_nSize;
	m_TextRect.y2 = Rect.y1 + 0 + m_nHeight;

	m_DropRect.x1 = Rect.x2 - 1 - m_nSize;
	m_DropRect.y1 = Rect.y1 + 0;
	m_DropRect.x2 = Rect.x2 - 3;
	m_DropRect.y2 = Rect.y1 + 0 + m_nHeight;

	if( Rect.y2 <= m_nScreen ) {

		m_ListRect.x1 = Rect.x1 + 0;
		m_ListRect.y1 = Rect.y1 + 1 + m_nHeight;
		m_ListRect.x2 = Rect.x2 - 0;
		m_ListRect.y2 = Rect.y2 - 0;

		m_pListBox->Create(this, pManager, m_ListRect, 2, 0, 0);
		}
	else {
		m_ListRect.x1 = Rect.x1 + 0;
		m_ListRect.y1 = Rect.y1 - 1 - (Rect.y2 - Rect.y1 - m_nHeight);
		m_ListRect.x2 = Rect.x2 - 0;
		m_ListRect.y2 = Rect.y1 - 1;

		m_pListBox->Create(this, pManager, m_ListRect, 2, lbsInverted, 0);
		}

	m_pButton ->Create(this, pManager, m_DropRect, 1, 0, 0);

	m_pButton ->SetText(L"D");
	}

// Drawing

void CDropDownList::Draw(IManager *pManager, UINT uCode)
{
	if( uCode == drawInitial || m_fDirty ) {

		IGdi *pGDI = pManager->GetGDI();

		Draw(pGDI);

		m_fDirty = FALSE;
		}

	if( m_fShowAct - m_fShowReq ) {

		if( m_fShowAct = m_fShowReq ) {

			pManager->ShowPopup(m_pListBox, FALSE);
			}
		else
			pManager->HidePopup();
		}

	m_pButton->Draw(pManager, uCode);
	}

// Hit Testing

BOOL CDropDownList::HitTest(IRegion *pDirty)
{
	return pDirty->HitTest(m_Rect);
	}

BOOL CDropDownList::HitTest(P2 const &Point)
{
	if( m_fEnable ) {

		R2 Rect = m_Rect;

		if( !m_fShowAct ) {

			Rect.y2 = m_TextRect.y2;
			}

		return PtInRect(Rect, Point);
		}

	return FALSE;
	}

// Core Attributes

void CDropDownList::GetRect(R2 &Rect)
{
	Rect = m_Rect;
	}

// Core Operations

void CDropDownList::Enable(BOOL fEnable)
{
	if( m_fEnable - fEnable ) {

		m_fEnable = fEnable;

		m_fDirty  = TRUE;
		}
	}

// Messages

BOOL CDropDownList::OnMessage(UINT uCode, DWORD dwParam)
{
	if( uCode == msgSkipFocus ) {

		return !m_fEnable;
		}

	if( uCode == msgSetFocus ) {

		m_fFocus = TRUE;

		m_fEat   = dwParam ? TRUE : FALSE;

		m_fDirty = TRUE;

		return TRUE;
		}

	if( uCode == msgKillFocus ) {

		m_fFocus = FALSE;

		m_fDirty = TRUE;

		HideDropDown();

		return TRUE;
		}

	if( uCode == msgTouchDown ) {

		P2 Point;

		Point.x = LOWORD(dwParam);
		Point.y = HIWORD(dwParam);

		if( m_pButton->HitTest(Point) ) {

			m_fEat = FALSE;

			if( m_pButton->OnMessage(uCode, dwParam) ) {

				m_pTouch = m_pButton;

				return TRUE;
				}

			return FALSE;
			}

		if( m_fEat ) {

			m_fEat = FALSE;

			return FALSE;
			}

		return TRUE;
		}

	if( uCode == msgTouchRepeat ) {

		if( m_pTouch ) {

			if( m_pTouch->OnMessage(uCode, dwParam) ) {

				return TRUE;
				}

			return m_fDirty;
			}
		}

	if( uCode == msgTouchUp ) {

		if( m_pTouch ) {

			BOOL fDone = m_pTouch->OnMessage(uCode, dwParam);

			m_pTouch   = NULL;

			return fDone;
			}
		else {
			P2 Point;

			Point.x = LOWORD(dwParam);
			Point.y = HIWORD(dwParam);

			if( PtInRect(m_TextRect, Point) ) {

				FlipState();

				return TRUE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

// List Box Operations

UINT CDropDownList::AddString(PCTXT pText, DWORD dwParam)
{
	return m_pListBox->AddString(pText, dwParam);
	}

UINT CDropDownList::AddString(PCUTF pText, DWORD dwParam)
{
	return m_pListBox->AddString(pText, dwParam);
	}

UINT CDropDownList::GetSelect(void)
{
	return m_pListBox->GetSelect();
	}

DWORD CDropDownList::GetSelectData(void)
{
	return m_pListBox->GetSelectData();
	}

UINT CDropDownList::GetText(PTXT pText, UINT uItem)
{
	return m_pListBox->GetText(pText, uItem);
	}

UINT CDropDownList::GetText(PUTF pText, UINT uItem)
{
	return m_pListBox->GetText(pText, uItem);
	}

void CDropDownList::SetSelect(UINT uSelect)
{
	m_pListBox->SetSelect(uSelect);
	}

void CDropDownList::SelectByParam(DWORD dwParam)
{
	m_pListBox->SelectByParam(dwParam);
	}

void CDropDownList::SortData(void)
{
	m_pListBox->SortData();
	}

// Drop Down Operations

void CDropDownList::ShowDropDown(void)
{
	if( !m_fShowReq ) {

		m_fShowReq = TRUE;

		m_fDirty   = TRUE;
		}
	}

void CDropDownList::HideDropDown(void)
{
	if( m_fShowReq ) {

		m_fShowReq = FALSE;

		m_fDirty   = TRUE;
		}
	}

// Notification

void CDropDownList::OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData)
{
	if( uID == 1 && uCode == msgTouchUp ) {

		FlipState();
		}

	if( uID == 2 ) {

		SendNotify(GetSelect(), GetSelectData());

		HideDropDown();
		}
	}

// Style Access

void CDropDownList::GetStyle(IStyle * &pStyle)
{
	if( m_pParent ) {
		
		m_pParent->GetStyle(pStyle);
		}
	}

// Implementation

void CDropDownList::DefStyle(void)
{
	m_nSize		= 30;

	m_nHeight       = 30;

	m_nScreen       = 240;

	m_colBorder     = GetRGB( 0, 0, 0);

	m_colSelectFore = GetRGB( 0, 0, 0);
	
	m_colSelectBack = GetRGB(18,18,24);

	m_colTextFore   = GetRGB( 0, 0, 0);
	
	m_colTextBack   = GetRGB(31,31,31);
	
	m_colTextGrey   = GetRGB(24,24,24);
	}

void CDropDownList::GetStyle(void)
{
	GetStyleMetric(metricScrollSize,  m_nSize);

	GetStyleMetric(metricComboHeight, m_nHeight);

	GetStyleMetric(metricScreenCy,    m_nScreen);

	GetStyleColor (colBorder,         m_colBorder);

	GetStyleColor (colSelectFore,     m_colSelectFore);
	
	GetStyleColor (colSelectBack,     m_colSelectBack);

	GetStyleColor (colTextFore,       m_colTextFore);

	GetStyleColor (colTextBack,       m_colTextBack);

	GetStyleColor (colTextGrey,       m_colTextGrey);
	}

void CDropDownList::Draw(IGdi *pGDI)
{
	pGDI->SetBrushFore(m_colTextBack);

	pGDI->SetPenFore  (m_colBorder);

	pGDI->FillRect(PassRect(m_TextRect));
	
	pGDI->DrawRect(PassRect(m_TextRect));

	int cy = pGDI->GetTextHeight("X");

	int np = 2;

	int xp = m_TextRect.x1 + 2;

	int yp = m_TextRect.y1 + (m_TextRect.y2 - m_TextRect.y1 - cy - 2 * np) / 2;

	int x2 = m_TextRect.x2 - 2;

	if( GetSelect() < NOTHING ) {

		WORD sText[256];

		GetText   (sText, GetSelect());

		MakeNarrow(sText, sText);

		if( m_fEnable ) {

			if( m_fFocus ) {

				pGDI->SetTextFore (m_colSelectFore);

				pGDI->SetTextBack (m_colSelectBack);

				pGDI->SetBrushFore(m_colSelectBack);
				}
			else {
				pGDI->SetTextFore (m_colTextFore);

				pGDI->SetTextBack (m_colTextBack);

				pGDI->SetBrushFore(m_colTextBack);
				}
			}
		else {
			pGDI->SetTextFore (m_colTextGrey);

			pGDI->SetTextBack (m_colTextBack);

			pGDI->SetBrushFore(m_colTextBack);
			}

		// REV3 -- Clip text horizontally?

		int cx = pGDI->GetTextWidth(sText);

		pGDI->TextOut(xp, yp + np, sText);

		pGDI->FillRect(xp, yp, x2, yp + np);

		pGDI->FillRect(xp, yp + cy + np, x2, yp + cy + 2 * np);

		pGDI->FillRect(xp + cx, yp + np, x2, yp + cy + np);
		}
	}

void CDropDownList::FlipState(void)
{
	if( m_fShowReq ) {

		HideDropDown();

		return;
		}

	ShowDropDown();
	}

// End of File
