
//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_STDCTRL_HPP
	
#define	INCLUDE_STDCTRL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Control Base Class
//

class CStdControl
{
	public:
		// Constructor
		CStdControl(void);

		// Destructor
		virtual ~CStdControl(void);

	protected:
		// Control Count
		static UINT m_uCount;

		// Style Data
		static BOOL  m_fStyles;
		static COLOR m_Shade1A;
		static COLOR m_Shade1B;
		static COLOR m_Shade2A;
		static COLOR m_Shade2B;

		// Control Data
		IControl   * m_pThis;
		INotify    * m_pNotify;
		R2	     m_Rect;
		UINT	     m_uID;
		UINT	     m_uFlags;
		UINT	     m_uStyle;
		IStyle     * m_pStyle;

		// State Data
		BOOL m_fEnable;
		BOOL m_fDirty;

		// Style Helpers
		BOOL FindStyle     (void);
		void GetStyleColor (UINT uCode, COLOR      &Color);
		void GetStyleMetric(UINT uCode, int        &nSize);
		void GetStyleFont  (UINT uCode, IGdiFont * &pFont);

		// Notification
		BOOL SendNotify(UINT uCode, int nData);

		// Style Data
		void GetStyles(void);

		// Color Mixing
		static COLOR Mix16(COLOR a, COLOR b, int p, int c);
		static DWORD Mix32(COLOR a, COLOR b, int p, int c);

		// Shaders
		static BOOL Shader1(IGdi *pGDI, int p, int c);
		static BOOL Shader2(IGdi *pGDI, int p, int c);
		static BOOL Shader3(IGdi *pGDI, int p, int c);
	};

// End of File

#endif
