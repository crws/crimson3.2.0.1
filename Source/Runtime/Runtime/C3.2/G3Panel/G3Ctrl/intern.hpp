
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Controls Library
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3ctrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cursor Modes
//

enum
{
	cursorLast  = 0x8000,
	cursorAll   = 0x8001,
	cursorError = 0x8002,
	cursorNone  = 0x8003,
	};

// End of File

#endif
