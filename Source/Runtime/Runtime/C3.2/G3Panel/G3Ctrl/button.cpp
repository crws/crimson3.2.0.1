
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Button Base Class
//

// Constructor

CStdButton::CStdButton(void)
{
	m_pThis  = this;

	m_fPress = FALSE;

	m_uState = 0;

	wstrcpy(m_sText, L"Button");

	DefStyle();
	}

// Destructor

CStdButton::~CStdButton(void)
{
	}

// Management

UINT CStdButton::Release(void)
{
	delete this;

	return 0;
	}

// Creation

void CStdButton::Create(IGdi *pGDI, INotify *pNotify, R2 const &Rect, UINT uID, UINT uFlags, UINT uStyle, IStyle *pStyle)
{
	m_pNotify = pNotify;

	m_Rect    = Rect;

	m_uID     = uID;

	m_uFlags  = uFlags;

	m_uStyle  = uStyle;

	m_pStyle  = pStyle;

	GetStyle();

	OnCreate(pGDI);
	}

// Drawing

void CStdButton::DrawPrep(IGdi *pGDI, IRegion *pErase)
{
	}

void CStdButton::DrawExec(IGdi *pGDI, IRegion *pDirty)
{
	if( m_fDirty || HitTest(pDirty) ) {

		OnDraw(pGDI);

		pDirty->AddRect(m_Rect);

		m_fDirty = FALSE;
		}
	}

// Touch Mapping

void CStdButton::TouchMap(ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_Rect));
	}

// Hit Testing

BOOL CStdButton::HitTest(IRegion *pDirty)
{
	return pDirty->HitTest(m_Rect);
	}

BOOL CStdButton::HitTest(P2 const &Point)
{
	return m_fEnable && PtInRect(m_Rect, Point);
	}

// Core Attributes

void CStdButton::GetRect(R2 &Rect)
{
	Rect = m_Rect;
	}

// Core Operations

void CStdButton::Enable(BOOL fEnable)
{
	if( m_fEnable - fEnable ) {

		m_fEnable = fEnable;

		m_fDirty  = TRUE;
		}
	}

// Messages

BOOL CStdButton::OnMessage(UINT uCode, DWORD dwParam)
{
	return FALSE;
	}

// Text Operations

void CStdButton::SetText(PCUTF pText)
{
	if( wstrcmp(m_sText, pText) ) {

		wstrcpy(m_sText, pText);

		m_fDirty = TRUE;
		}
	}

void CStdButton::GetText(PUTF pText)
{
	wstrcpy(pText, m_sText);
	}

// State Operations

void CStdButton::SetState(UINT uState)
{
	if( m_uState - uState ) {

		m_uState = uState;

		OnValidate();

		m_fDirty = TRUE;
		}
	}

UINT CStdButton::GetState(void)
{
	return m_uState;
	}

// Overridables

void CStdButton::OnCreate(IGdi *pGDI)
{
	}

void CStdButton::OnDraw(IGdi *pGDI)
{
	}

void CStdButton::OnValidate(void)
{
	}

// Shader Selection

PSHADER CStdButton::GetShader(UINT uState)
{
	if( m_fPress ) {

		return Shader2;
		}

	if( uState ) {

		return Shader3;
		}

	return Shader1;
	}

// Implementation

void CStdButton::DefStyle(void)
{
	m_colBorder   = GetRGB(0,0,0);

	m_colText     = GetRGB(0,0,0);

	m_colDisabled = GetRGB(18,18,18);

	m_pFont       = NULL;
	}

void CStdButton::GetStyle(void)
{
	GetStyleColor(colButtonBorder,   m_colBorder);

	GetStyleColor(colButtonText,     m_colText);

	GetStyleColor(colButtonDisabled, m_colDisabled);

	GetStyleFont (fontButton,        m_pFont);
	}

// End of File
