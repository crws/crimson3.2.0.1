
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Static Text Control
//

class CStaticText : public CStdControl, public IStaticText
{
	public:
		// Constructor
		CStaticText(void);

		// Destructor
		~CStaticText(void);

		// Management
		void Release(void);
		
		// Creation
		void Create( IContainer * pParent,
			     IManager   * pManager,
			     R2 const   & Rect,
			     UINT	  uID,
			     UINT	  uFlags,
			     UINT	  uStyle
			     );

		// Drawing
		void Draw( IManager *pManager,
			   UINT      uCode
			   );

		// Hit Testing
		BOOL HitTest(IRegion *pDirty);
		BOOL HitTest(P2 const &Point);

		// Core Attributes
		void GetRect(R2 &Rect);

		// Core Operations
		void Enable(BOOL fEnable);

		// Messages
		BOOL OnMessage(UINT uCode, DWORD dwParam);

		// Text Operations
		void SetText(PCUTF pText);
		void GetText(PUTF  pText);

	protected:
		// Visual Style
		IGdiFont * m_pFont;
		COLOR	   m_colText;
		COLOR	   m_colDisabled;

		// State Data
		WORD m_sText[maxString];

		// Implementation
		void DefStyle(void);
		void GetStyle(void);
		void Draw(IGdi *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Static Text Control
//

// Instantiator

IStaticText * Create_StaticText(void)
{
	return New CStaticText;
	}

// Constructor

CStaticText::CStaticText(void)
{
	m_fEnable = TRUE;

	SetText(L"Static Text");

	DefStyle();
	}

// Destructor

CStaticText::~CStaticText(void)
{
	}

// Management

void CStaticText::Release(void)
{
	delete this;
	}

// Creation

void CStaticText::Create(IContainer *pParent, IManager *pManager, R2 const &Rect, UINT uID, UINT uFlags, UINT uStyle)
{
	m_pParent = pParent;

	m_Rect    = Rect;

	m_uID     = uID;

	m_uFlags  = uFlags;

	m_uStyle  = uStyle;

	GetStyle();
	}

// Drawing

void CStaticText::Draw(IManager *pManager, UINT uCode)
{
	if( uCode == drawInitial || m_fDirty ) {

		IGdi *pGDI = pManager->GetGDI();

		Draw(pGDI);

		pManager->GetDirtyRegion()->AddRect(m_Rect);

		m_fDirty = FALSE;
		}
	}

// Hit Testing

BOOL CStaticText::HitTest(IRegion *pDirty)
{
	return pDirty->HitTest(m_Rect);
	}

BOOL CStaticText::HitTest(P2 const &Point)
{
	return FALSE;
	}

// Messages

BOOL CStaticText::OnMessage(UINT uCode, DWORD dwParam)
{
	if( uCode == msgSkipFocus ) {

		return TRUE;
		}

	return FALSE;
	}

// Core Attributes

void CStaticText::GetRect(R2 &Rect)
{
	Rect = m_Rect;
	}

// Core Operations

void CStaticText::Enable(BOOL fEnable)
{
	if( m_fEnable - fEnable ) {

		m_fEnable = fEnable;

		m_fDirty  = TRUE;
		}
	}

// Text Operations

void CStaticText::SetText(PCUTF pText)
{
	if( wstrcmp(m_sText, pText) ) {

		wstrcpy(m_sText, pText);

		m_fDirty = TRUE;
		}
	}

void CStaticText::GetText(PUTF pText)
{
	wstrcpy(pText, m_sText);
	}

// Implementation

void CStaticText::DefStyle(void)
{
	m_colText     = GetRGB(0,0,0);

	m_colDisabled = GetRGB(15,15,15);

	m_pFont       = NULL;
	}

void CStaticText::GetStyle(void)
{
	GetStyleColor(colButtonText,     m_colText);

	GetStyleColor(colButtonDisabled, m_colDisabled);

	GetStyleFont (fontButton,        m_pFont);
	}

void CStaticText::Draw(IGdi *pGDI)
{
	pGDI->SelectFont  (m_pFont);

	pGDI->SetTextTrans(modeTransparent);

	pGDI->SetTextFore (m_fEnable ? m_colText : m_colDisabled);

	int cx = pGDI->GetTextWidth (m_sText);
	
	int cy = pGDI->GetTextHeight(m_sText);

	int xp = m_Rect.x1 + 0 * (m_Rect.x2 - m_Rect.x1 - cx) / 2;

	int yp = m_Rect.y1 + 1 * (m_Rect.y2 - m_Rect.y1 - cy) / 2;

	pGDI->TextOut(xp, yp, m_sText);
	}

// End of File
