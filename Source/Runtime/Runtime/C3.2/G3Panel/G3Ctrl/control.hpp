
//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CONTROL_HPP
	
#define	INCLUDE_CONTROL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <gdi2.hpp>

//////////////////////////////////////////////////////////////////////////
//
// String Length
//

#define	maxString 256

//////////////////////////////////////////////////////////////////////////
//
// Interfaces
//

interface ITouchMap;
interface IContainer;
interface IStyle;
interface IMsgSink;
interface IControl;
interface IControlWithText;
interface IButton;
interface IStaticText;
interface IScrollBar;
interface IProgressBar;
interface IListBox;
interface IDropDownList;
interface IKeyPad;
interface IEditControl;

//////////////////////////////////////////////////////////////////////////
//
// Visual Style Colors
//

enum
{
	colBackground,
	colButtonBorder,
	colButtonText,
	colButtonDisabled,
	colButtonShade1A,
	colButtonShade1B,
	colButtonShade2A,
	colButtonShade2B,
	colBorder,
	colSelectFore,
	colSelectBack,
	colTextFore,
	colTextBack,
	colTextGrey,
	colProgBack,
	colProgShadeA,
	colProgShadeB,
	};

//////////////////////////////////////////////////////////////////////////
//
// Visual Style Metrics
//

enum
{
	metricScreenCx,
	metricScreenCy,
	metricCheckSize,
	metricRadioSize,
	metricScrollSize,
	metricComboHeight,
	};

//////////////////////////////////////////////////////////////////////////
//
// Visual Style Fonts
//

enum
{
	fontButton
	};

//////////////////////////////////////////////////////////////////////////
//
// Messages Types
//

enum
{
	msgSetFocus,
	msgKillFocus,
	msgSkipFocus,
	msgKeyDown,
	msgKeyRepeat,
	msgKeyUp,
	msgTouchDown,
	msgTouchRepeat,
	msgTouchUp,
	};

//////////////////////////////////////////////////////////////////////////
//
// Drawing Codes
//

enum
{
	drawInitial,
	drawChanges,
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Flags
//

enum
{
	lbsInverted = 1
	};

//////////////////////////////////////////////////////////////////////////
//
// Base Interface
//

interface IBase
{
	// Management
	virtual void Release(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Window Manager
//

interface IManager : public IBase
{
	// GDI Access
	virtual IGdi * GetGDI(void) = 0;

	// Popup Management
	virtual void ShowPopup(IControl *pPopup, BOOL fLock) = 0;
	virtual void HidePopup(void)                         = 0;

	// Top-Level Container
	virtual IContainer * GetTopContainer(void) = 0;

	// Update Regions
	virtual IRegion * GetDirtyRegion(void) = 0;
	virtual IRegion * GetEraseRegion(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Container
//

interface IContainer : public IBase
{
	// Notification
	virtual void OnNotify( IControl * pCtrl,
			       UINT	  uID,
			       UINT	  uCode,
			       int	  nData
			       ) = 0;

	// Style Access
	virtual void GetStyle( IStyle * & pStyle
			       ) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Style Information
//

interface IStyle : public IBase
{
	// Attributes
	virtual void GetColor(   UINT    uStyle,
				 UINT    uCode,
				 COLOR & Color
				 ) = 0;

	virtual void GetMetric(  UINT    uStyle,
				 UINT    uCode,
				 int   & nSize
				 ) = 0;

	virtual void GetFont(    UINT         uStyle,
				 UINT	      uCode,
				 IGdiFont * & pFont
				 ) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Message Sink
//

interface IMsgSink : public IBase
{
	// Messages
	virtual UINT OnMessage(UINT uCode, DWORD dwParam) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Basic Control
//

interface IControl : public IMsgSink
{
	// Creation
	virtual void Create( IContainer * pParent,
			     IManager   * pManager,
			     R2 const   & Rect,
			     UINT	  uID,
			     UINT	  uFlags,
			     UINT	  uStyle
			     ) = 0;

	// Drawing
	virtual void Draw( IManager *pManager,
			   UINT     uCode
			   ) = 0;

	// Hit Testing
	virtual BOOL HitTest(IRegion *pDirty) = 0;
	virtual BOOL HitTest(P2 const &Point) = 0;

	// Core Attributes
	virtual void GetRect(R2 &Rect) = 0;

	// Core Operations
	virtual void Enable(BOOL fEnable) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Control with Text
//

interface IControlWithText : public IControl
{
	// Text Operations
	virtual void SetText(PCUTF pText) = 0;
	virtual void GetText(PUTF  pText) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Button Control
//

interface IButton : public IControlWithText
{
	// State Operations
	virtual void SetState(UINT uState) = 0;
	virtual UINT GetState(void)        = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Static Text
//

interface IStaticText: public IControlWithText
{
	// No Additional Members
	};

//////////////////////////////////////////////////////////////////////////
//
// Scroll Bar
//

interface IScrollBar : public IControl
{
	// Scroll Operations
	virtual void SetValue(int nValue)         = 0;
	virtual void SetRange(int nMin, int nMax) = 0;
	virtual int  GetValue(void)               = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Progress Bar
//

interface IProgressBar : public IScrollBar
{
	// No Additional Members
	};

//////////////////////////////////////////////////////////////////////////
//
// List Box
//

interface IListBox : public IControl
{
	// List Box Operations
	virtual UINT  AddString(PCTXT pText, DWORD dwParam) = 0;
	virtual UINT  AddString(PCUTF pText, DWORD dwParam) = 0;
	virtual UINT  GetSelect(void)			    = 0;
	virtual DWORD GetSelectData(void)		    = 0;
	virtual UINT  GetText(PTXT pText, UINT uItem)	    = 0;
	virtual UINT  GetText(PUTF pText, UINT uItem)	    = 0;
	virtual void  SetSelect(UINT uSelect)		    = 0;
	virtual void  SelectByParam(DWORD dwParam)	    = 0;
	virtual void  SortData(void)			    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Drop Down List
//

interface IDropDownList : public IListBox
{
	// Drop Down List
	virtual void ShowDropDown(void) = 0;
	virtual void HideDropDown(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Key Pad
//

interface IKeyPad : public IControl
{
	// Key Pad Operations
	virtual void GetPadSize(P2       &Size ) = 0;
	virtual void SetPadHost(IMsgSink *pHost) = 0;
	virtual void SetPadText(PCUTF     pText) = 0;
	virtual void SetPadCrsr(UINT      uPos ) = 0;
	virtual void SetPadDefs(void           ) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Edit Control
//

interface IEditControl : public IControlWithText
{
	// No Additional Members
	};

//////////////////////////////////////////////////////////////////////////
//
// Standard Control Creation
//

extern IButton       * Create_PushButton(void);

extern IButton       * Create_IconButton(void);

extern IButton       * Create_RadioButton(void);

extern IButton       * Create_CheckBox(void);

extern IStaticText   * Create_StaticText(void);

extern IScrollBar    * Create_ScrollBar(void);

extern IProgressBar  * Create_ProgressBar(void);

extern IListBox      * Create_ListBox(void);

extern IDropDownList * Create_DropDownList(void);

extern IKeyPad       * Create_KeyPad(UINT uType);

extern IEditControl  * Create_EditControl(void);

// End of File

#endif
