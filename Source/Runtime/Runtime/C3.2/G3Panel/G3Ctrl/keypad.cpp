
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hangul Jamo Codes
//

enum
{
	// Null Value

	jamoNull,

	// Simple Consonants

	jamoG,
	jamoN,
	jamoD,
	jamoL,
	jamoM,
	jamoB,
	jamoS,
	jamoNG,
	jamoJ,
	jamoC,
	jamoK,
	jamoT,
	jamoP,
	jamoH,

	// Double Letters

	jamoGG,
	jamoDD,
	jamoBB,
	jamoSS,
	jamoJJ,

	// Consonants Clusters

	jamoGS,
	jamoNJ,
	jamoNH,
	jamoLG,
	jamoLM,
	jamoLB,
	jamoLS,
	jamoLT,
	jamoLP,
	jamoLH,
	jamoBS,

	// Simple Vowels

	jamoA,
	jamoEO,
	jamoO,
	jamoU,
	jamoEU,
	jamoI,

	// Iotized Vowels

	jamoYA,
	jamoYEO,
	jamoYO,
	jamoYU,

	// Dipthongs

	jamoAE,
	jamoYAE,
	jamoE,
	jamoYE,
	jamoWA,
	jamoWAE,
	jamoOE,
	jamoWEO,
	jamoWE,
	jamoWI,
	jamoYI
	};

//////////////////////////////////////////////////////////////////////////
//
// Key Pad Control
//

class CKeypad : public CStdControl, public IKeypad, public INotify, public IStyle
{
	public:
		// Constructor
		CKeypad(IGdi *pGDI, UINT uType);

		// Destructor
		~CKeypad(void);

		// Management
		UINT Release(void);

		// Creation
		void Create( IGdi     * pGDI,
			     INotify  * pNotify,
			     R2 const & Rect,
			     UINT       uID,
			     UINT       uFlags,
			     UINT	uStyle,
			     IStyle   * pStyle
			     );

		// Drawing
		void DrawPrep(IGdi *pGDI, IRegion *pErase);
		void DrawExec(IGdi *pGDI, IRegion *pDirty);

		// Touch Mapping
		void TouchMap(ITouchMap *pTouch);

		// Hit Testing
		BOOL HitTest(IRegion *pDirty);
		BOOL HitTest(P2 const &Point);

		// Core Attributes
		void GetRect(R2 &Rect);

		// Core Operations
		void Enable(BOOL fEnable);

		// Messages
		UINT OnMessage(UINT uCode, UINT uParam);

		// Key Pad Operations
		void GetPadSize(S2       &Size );
		void SetPadHost(IMsgSink *pHost);
		void SetPadHead(PCUTF     pHead);
		void SetPadFoot(PCUTF     pFoot);
		void SetPadText(PCUTF     pText);
		void SetPadCrsr(UINT      uPos );
		void SetPadDefs(void           );

		// Notification
		void OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData);

		// IStyle Methods
		void GetColor (UINT uStyle, UINT uCode, COLOR      &Color);
		void GetMetric(UINT uStyle, UINT uCode, int        &nSize);
		void GetFont  (UINT uStyle, UINT uCode, IGdiFont * &pFont);

	protected:
		// Pad Modes
		enum
		{
			modeLower,
			modeCaps,
			modeShift,
			modeShift2,
			modePinYin,
			modeHangulLower,
			modeHangulUpper,
			};

		// Key Codes
		enum {
			keyExotic = 'y',
			keyCaps	  = 'c',
			keyShift  = 's',
			keyEnter  = 'e',
			keyClose  = 'x',
			keySpace  = 'b',
			keyDelete = 'd',
			keyRaise  = 'r',
			keyLower  = 'l',
			keySign   = 'i',
			keyNext   = 'n',
			keyPrev   = 'p',
			keyLeft   = '<',
			keyRight  = '>',
			};

		// PinYin Nodes
		enum {
			nodeInternal = 'I',
			nodeLeaf     = 'L',
			};

		// Host Object
		IMsgSink * m_pHost;

		// Style Source
		IStyle   * m_pStyle;

		// Options
		UINT m_uType;
		BOOL m_fHead;
		BOOL m_fFoot;
		BOOL m_fRamp;
		BOOL m_fHide;
		UINT m_uLayout;

		// Layout
		int        m_dx, m_dy;
		int        m_xp, m_yp;
		int        m_cx, m_cy;
		R2         m_Display;
		BOOL	   m_fSmall;
		UINT	   m_uFont1;
		UINT	   m_uFont3;
		UINT	   m_uFont6;
		UINT	   m_uFontD;
		UINT	   m_uFontP;
		UINT	   m_uFontH;
		UINT	   m_uFontF;
		int	   m_yHead;
		int	   m_yFoot;

		// Children
		IControl  * m_pTouch;
		IControl ** m_pList;
		UINT	    m_uSpace;
		UINT        m_uCount;
		UINT	    m_uRows;
		UINT	    m_uCols;

		// State Data
		UINT  m_uMode;
		BOOL  m_fFrame;
		UINT  m_uState;
		UINT  m_uHist[8];
		char  m_sPin [8];
		WCHAR m_sHead[256];
		WCHAR m_sFoot[256];
		WCHAR m_sData[256];
		UINT  m_uDepth;
		int   m_nGlyphs;
		int   m_nStart;
		UINT  m_uCursor;
		int   m_jamo1;
		int   m_jamo2;
		int   m_jamo3;
		WCHAR m_Hangul;

		// Key Creation
		IButton * CreateKey(char cCode);

		// Implementation
		void  StartPinYin(void);
		void  StartHangul(void);
		void  FindDisplay(void);
		void  MakeKeys(IGdi *pGDI);
		UINT  GetRowCount(void);
		UINT  GetColCount(void);
		PCTXT GetRowData(UINT uRow);
		UINT  GetInitMode(void);
		BOOL  UseRedScheme(void);
		void  FindFonts(void);
		BOOL  UseBigFonts(void);
		BOOL  IsKeyWide(char cCode);
		BOOL  IsKeyLatching(char cCode);
		BOOL  IsKeyEnabled(int nPos, char cCode);
		UINT  GetKeyStyle(char cCode);
		UINT  GetKeyState(char cCode);
		void  GetKeyText(PUTF pText, char cCode);
		WORD  Transform(UINT uMode, int nPos, char cCode);
		WORD  TransformNormal(UINT uMode, int nPos, char cCode);
		WORD  TransformHebrew(UINT uMode, int nPos, char cCode);
		WORD  TransformFrench(UINT uMode, int nPos, char cCode);
		void  SetMode(UINT uMode);
		void  DrawEntry(IGdi *pGDI);
		void  DrawText(IGdi *pGDI, R2 Rect, UINT uFont, PUTF pText, BOOL fNum);
		void  DrawHead(IGdi *pGDI, R2 Rect);
		void  DrawFoot(IGdi *pGDI, R2 Rect);

		// Hangul Support
		int   GetJamoIndex1(int jamo);
		int   GetJamoIndex2(int jamo);
		int   GetJamoIndex3(int jamo);
		BOOL  ComposeJamo(int &first, int second, int pos);
		short GetJamoChar(int jamo);
		short MakeHangul(int j1, int j2, int j3);
		int   GetKeyJamo(char cKey);
	};

//////////////////////////////////////////////////////////////////////////
//
// Key Pad Control
//

// Externals

extern PCWORD m_pPinYin[];

// Instantiator

IKeypad * Create_Keypad(IGdi *pGDI, UINT uType)
{
	return New CKeypad(pGDI, uType);
	}

// Constructor

CKeypad::CKeypad(IGdi *pGDI, UINT uType)
{
	m_uType   = uType;

	m_fHead   = (m_uType & kpsShowHead) ? TRUE : FALSE;

	m_fFoot   = (m_uType & kpsShowFoot) ? TRUE : FALSE;

	m_fRamp   = (m_uType & kpsShowRamp) ? TRUE : FALSE;

	m_fHide   = (m_uType & kpsHideNav ) ? TRUE : FALSE;

	m_uLayout = (m_uType & kpsLayout  );

	m_pHost   = NULL;

	m_pStyle  = NULL;

	m_pTouch  = NULL;

	m_pList   = NULL;

	m_uCount  = 0;

	m_uCursor = cursorAll;

	m_uMode   = GetInitMode();

	m_fFrame  = TRUE;

	m_yHead   = 20;

	m_yFoot   = 20;

	m_dx      = pGDI->GetCx();

	m_dy      = pGDI->GetCy();

	m_fSmall  = (m_dx <= 320);

	m_uFont1  = fontHei16;

	m_uFont3  = fontHei16;

	m_uFont6  = m_fSmall ? fontHei12 : fontHei16;

	m_uFontD  = fontHei16Bold;

	m_uFontP  = fontSwiss0712;

	m_uFontH  = fontHei16Bold;

	m_uFontF  = fontHei16;

	FindFonts();

	wstrcpy(m_sHead, L"HEAD");

	wstrcpy(m_sFoot, L"FOOT");

	wstrcpy(m_sData, L"");
	}

// Destructor

CKeypad::~CKeypad(void)
{
	if( m_pList ) {

		for( UINT n = 0; n < m_uCount; n++ ) {

			if( m_pList[n] ) {

				m_pList[n]->Release();
				}
			}

		delete [] m_pList;
		}
	}

// Management

UINT CKeypad::Release(void)
{
	delete this;

	return 0;
	}

// Creation

void CKeypad::Create(IGdi *pGDI, INotify *pNotify, R2 const &Rect, UINT uID, UINT uFlags, UINT uStyle, IStyle *pStyle)
{
	m_pNotify = pNotify;

	m_Rect    = Rect;

	m_uID     = uID;

	m_uFlags  = uFlags;

	m_uStyle  = uStyle;

	m_pStyle  = pStyle;

	MakeKeys(pGDI);

	FindDisplay();
	}

// Drawing

void CKeypad::DrawPrep(IGdi *pGDI, IRegion *pErase)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_pList[n] ) {

			m_pList[n]->DrawPrep(pGDI, pErase);
			}
		}
	}

void CKeypad::DrawExec(IGdi *pGDI, IRegion *pDirty)
{
	if( m_fFrame || pDirty->HitTest(m_Rect) ) {

		COLOR c1, c2, c3, c4;

		if( UseRedScheme() ) {

			c1 = GetRGB(0,0,0);

			c2 = GetRGB(30,10,10);

			c3 = GetRGB(30,26,26);

			c4 = GetRGB(26,26,26);
			}
		else {
			c1 = GetRGB(0,0,0);

			c2 = GetRGB(24,24,24);

			c3 = GetRGB(30,30,28);

			c4 = GetRGB(26,26,26);
			}

		R2 Rect = m_Rect;

		pGDI->ResetAll();
	
		pGDI->SetPenFore(c1);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);

		pGDI->SetPenFore(c2);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);

		pGDI->SetPenFore(c2);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);

		pGDI->SetBrushFore(c3);

		pGDI->FillRect(PassRect(Rect));

		if( m_fHead ) {

			R2 Head = Rect;

			Head.y2 = Head.y1 + m_yHead;

			pGDI->SetBrushFore(c2);

			pGDI->FillRect(PassRect(Head));

			DrawHead(pGDI, Head);
			}

		if( m_fFoot ) {

			R2 Foot = Rect;

			Foot.y1 = Foot.y2 - m_yFoot;

			pGDI->SetBrushFore(c4);

			pGDI->FillRect(PassRect(Foot));

			DrawFoot(pGDI, Foot);
			}

		pDirty->AddRect(m_Rect);

		m_fFrame = FALSE;
		}

	if( m_fDirty || pDirty->HitTest(m_Display) ) {

		DrawEntry(pGDI);

		pDirty->AddRect(m_Display);

		m_fDirty = FALSE;
		}

	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_pList[n] ) {

			m_pList[n]->DrawExec(pGDI, pDirty);
			}
		}
	}

// Touch Mapping

void CKeypad::TouchMap(ITouchMap *pTouch)
{
	pTouch->ClearMap();

	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_pList[n] ) {

			m_pList[n]->TouchMap(pTouch);
			}
		}
	}

// Hit Testing

BOOL CKeypad::HitTest(IRegion *pDirty)
{
	return pDirty->HitTest(m_Rect);
	}

BOOL CKeypad::HitTest(P2 const &Point)
{
	return m_fEnable && PtInRect(m_Rect, Point);
	}

// Core Attributes

void CKeypad::GetRect(R2 &Rect)
{
	Rect = m_Rect;
	}

// Core Operations

void CKeypad::Enable(BOOL fEnable)
{
	if( m_fEnable - fEnable ) {

		m_fEnable = fEnable;

		m_fDirty  = TRUE;
		}
	}

// Messages

UINT CKeypad::OnMessage(UINT uCode, UINT uParam)
{
	BOOL fLocal   = HIWORD(uCode);

	UINT uMessage = LOWORD(uCode);

	if( uMessage == msgSkipFocus ) {

		return !m_fEnable;
		}

	if( uMessage == msgTouchDown ) {

		P2 Point;

		Point.x = LOWORD(uParam);
		Point.y = HIWORD(uParam);

		for( UINT n = 0; n < m_uCount; n++ ) {

			if( m_pList[n] ) {

				if( m_pList[n]->HitTest(Point) ) {

					if( m_pList[n]->OnMessage(uCode, uParam) ) {

						m_pTouch = m_pList[n];

						return TRUE;
						}

					return FALSE;
					}
				}
			}

		return FALSE;
		}

	if( uMessage == msgTouchRepeat ) {

		if( m_pTouch ) {

			if( m_pTouch->OnMessage(uCode, uParam) ) {

				return TRUE;
				}

			return TRUE;
			}
		}

	if( uMessage == msgTouchUp ) {

		if( m_pTouch ) {

			// NOTE : We might get deleted during OnMessage!

			IControl *pTouch = m_pTouch;

			m_pTouch         = NULL;

			pTouch->OnMessage(uCode, uParam);

			return TRUE;
			}

		return TRUE;
		}

	if( uMessage == msgKeyDown ) {

		if( uParam <= 0x7F ) {

			return m_pHost->OnMessage(uCode, uParam);
			}

		return FALSE;
		}

	if( uMessage == msgKeyUp ) {

		if( uParam <= 0x7F ) {

			return m_pHost->OnMessage(uCode, uParam);
			}

		return FALSE;
		}

	return FALSE;
	}

// Key Pad Operations

void CKeypad::GetPadSize(S2 &Size)
{
	int nr = GetRowCount();

	int nc = GetColCount();

	int xe = (m_fSmall ?  6 :  8);

	int ye = (m_fSmall ?  6 :  8) + (m_fHead ? m_yHead : 0) + (m_fFoot ? m_yFoot: 0);

	int cs = (m_fSmall ? 34 : 40);

	switch( m_uType & kpsMaximum ) {

		case kpsNormal:  cs =   cs; break;
		case kpsLarge:   cs =   50; break;
		case kpsLarger:  cs =   60; break;
		case kpsMaximum: cs = 9999; break;
		}

	if( cs * nc + xe >= m_dx || cs * nr + ye >= m_dy ) {

		int cx = (m_dx - 2 - xe) / nc;

		int cy = (m_dy - 2 - ye) / nr;

		if( m_fSmall ) {

			if( cx < 30 ) {

				cx = min(cx, 2*cy);

				cy = min(cy, 2*cx);

				Size.cx = cx * nc + xe;

				Size.cy = cy * nr + ye;

				return;
				}
			}

		cs = min(cx, cy);
		}

	Size.cx = cs * nc + xe;

	Size.cy = cs * nr + ye;
	}

void CKeypad::SetPadHost(IMsgSink *pHost)
{
	m_pHost = pHost;
	}

void CKeypad::SetPadHead(PCUTF pText)
{
	if( m_fHead ) {

		CUnicode Text = UniVisual(pText);

		if( wstrcmp(m_sHead, Text) ) {
		
			wstrcpy(m_sHead, Text);

			m_fFrame = TRUE;
			}
		}
	}

void CKeypad::SetPadFoot(PCUTF pText)
{
	if( m_fFoot ) {

		CUnicode Text = UniVisual(pText);

		if( wstrcmp(m_sFoot, Text) ) {
		
			wstrcpy(m_sFoot, Text);

			m_fFrame = TRUE;
			}
		}
	}

void CKeypad::SetPadText(PCUTF pText)
{
	if( wstrcmp(m_sData, pText) ) {
	
		wstrcpy(m_sData, pText);

		m_fDirty = TRUE;
		}
	}

void CKeypad::SetPadCrsr(UINT uPos)
{
	if( m_uCursor != uPos ) {

		m_uCursor = uPos;

		m_fDirty  = TRUE;
		}
	}

void CKeypad::SetPadDefs(void)
{
	if( LOBYTE(m_uType) == 20 ) {

		SetMode(modeLower);

		return;
		}

	SetMode(modeCaps);
	}

// Notification

void CKeypad::OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData)
{
	UINT uMessage = LOWORD(uCode);

	BOOL fLocal   = HIWORD(uCode);

	UINT uKeyDown = MAKELONG(msgKeyDown, fLocal);

	UINT uKeyUp   = MAKELONG(msgKeyUp,   fLocal);

	if( m_uMode == modePinYin ) {

		if( uMessage == msgTouchDown || uMessage == msgTouchRepeat ) {

			if( uID == keyDelete ) {

				if( m_uDepth ) {

					m_uState         = m_uHist[--m_uDepth];

					m_sPin[m_uDepth] = 0;
					
					SetMode(modePinYin);

					return;
					}

				m_pHost->OnMessage(uKeyDown, 0x7F);

				return;
				}

			if( uID == keySpace ) {

				if( m_uDepth ) {

					PCWORD p = m_pPinYin[m_uState];

					if( IntelToHost(p[0]) == nodeLeaf ) {

						if( m_nGlyphs > 36 ) {

							m_nStart = m_nStart ? 0 : 36;

							SetMode(modePinYin);
							}
						}

					return;
					}

				m_pHost->OnMessage(uKeyDown, 0x20);

				return;
				}
			}

		if( uMessage == msgTouchDown ) {

			if( isupper(uID) || isdigit(uID) ) {

				PCWORD p = m_pPinYin[m_uState];

				if( IntelToHost(p[0]) == nodeInternal ) {

					char c = char(tolower(uID));

					for( UINT n = 0; IntelToHost(p[1+2*n]); n++ ) {

						if( IntelToHost(p[1+2*n]) == c ) {

							m_sPin [m_uDepth  ] = c;

							m_uHist[m_uDepth++] = m_uState;

							m_sPin [m_uDepth  ] = 0;

							m_uState            = IntelToHost(p[2+2*n]);

							if( IntelToHost((p = m_pPinYin[m_uState])[0]) == nodeLeaf ) {

								int m;

								for( m = 0; p[1+m]; m++ );

								m_nGlyphs = m;

								m_nStart  = 0;
								}

							SetMode(modePinYin);

							return;
							}
						}
					}
				else {
					WCHAR w[2];

					((IButton *) pCtrl)->GetText(w);

					if( m_pHost->OnMessage(uKeyDown, w[0]) ) {

						StartPinYin();
						}
					}
				}
			}
		}
	
	if( m_uMode == modeHangulUpper || m_uMode == modeHangulLower ) {

		if( uMessage == msgTouchUp ) {

			if( uID == keyEnter ) {

				if( m_uState ) {

					m_pHost->OnMessage(uKeyDown, m_Hangul);

					m_uState = 0;

					m_fDirty = TRUE;
					}

				m_pHost->OnMessage(uKeyDown, 0x0D);

				return;
				}

			if( uID == keyNext ) {

				if( m_uState ) {

					m_pHost->OnMessage(uKeyDown, m_Hangul);

					m_uState = 0;

					m_fDirty = TRUE;
					}
				else
					m_pHost->OnMessage(uKeyDown, 0x09);

				return;
				}
			}

		if( uMessage == msgTouchDown || uMessage == msgTouchRepeat ) {

			if( uID == keyDelete ) {

				if( m_uState ) {

					m_uState = 0;

					m_fDirty = TRUE;
					}
				else
					m_pHost->OnMessage(uKeyDown, 0x7F);

				return;
				}

			if( uID == keySpace ) {

				if( m_uState ) {

					m_pHost->OnMessage(uKeyDown, m_Hangul);

					m_uState = 0;

					m_fDirty = TRUE;
					}

				m_pHost->OnMessage(uKeyDown, 0x20);

				return;
				}

			if( isdigit(uID) || ispunct(uID) ) {

				WCHAR w[2];

				((IButton *) pCtrl)->GetText(w);

				if( m_uState ) {

					m_pHost->OnMessage(uKeyDown, m_Hangul);

					m_uState = 0;

					m_fDirty = TRUE;
					}

				if( m_pHost->OnMessage(uKeyDown, w[0]) ) {

					if( m_uMode == modeHangulUpper ) {
						
						SetMode(modeHangulLower);
						}
					}
				}
			}

		if( uMessage == msgTouchDown ) {

			if( isupper(uID) ) {

				char cCode = (m_uMode == modeHangulUpper) ? uID : tolower(uID);

				int  jamo  = GetKeyJamo(cCode);

				switch( m_uState ) {

					case 0:
						if( GetJamoIndex1(jamo) ) {

							m_jamo1  = jamo;

							m_Hangul = GetJamoChar(jamo);

							m_uState = 1;
							}
						else {
							if( GetJamoIndex2(jamo) ) {

								m_Hangul = GetJamoChar(jamo);

								m_pHost->OnMessage(uKeyDown, m_Hangul);

								m_uState = 0;
								}
							}
						break;

					case 1:
						if( GetJamoIndex2(jamo) ) {

							m_jamo2  = jamo;

							m_Hangul = MakeHangul(m_jamo1, m_jamo2, 0);

							m_uState = 2;
							}
						else {
							if( ComposeJamo(m_jamo1, jamo, 1) ) {

								m_Hangul = GetJamoChar(m_jamo1);

								m_uState = 1;
								}
							else {
								if( GetJamoIndex1(jamo) ) {

									m_pHost->OnMessage(uKeyDown, m_Hangul);

									m_jamo1  = jamo;

									m_Hangul = GetJamoChar(jamo);

									m_uState = 1;
									}
								}
							}
						break;

					case 2:
						if( GetJamoIndex3(jamo) ) {

							m_jamo3  = jamo;

							m_Hangul = MakeHangul(m_jamo1, m_jamo2, m_jamo3);

							m_uState = 3;
							}
						else {
							if( ComposeJamo(m_jamo2, jamo, 2) ) {

								m_Hangul = MakeHangul(m_jamo1, m_jamo2, 0);

								m_uState = 2;
								}
							else {
								if( GetJamoIndex1(jamo) ) {

									m_pHost->OnMessage(uKeyDown, m_Hangul);

									m_jamo1  = jamo;

									m_Hangul = GetJamoChar(jamo);

									m_uState = 1;
									}
								else {
									if( GetJamoIndex2(jamo) ) {

										m_pHost->OnMessage(uKeyDown, m_Hangul);

										m_Hangul = GetJamoChar(jamo);

										m_pHost->OnMessage(uKeyDown, m_Hangul);

										m_uState = 0;
										}
									}
								}
							}
						break;

					case 3:
						if( ComposeJamo(m_jamo3, jamo, 3) ) {

							m_Hangul = MakeHangul(m_jamo1, m_jamo2, m_jamo3);

							m_uState = 3;
							}
						else {
							if( GetJamoIndex1(jamo) ) {

								m_pHost->OnMessage(uKeyDown, m_Hangul);

								m_jamo1 = jamo;

								m_Hangul = GetJamoChar(jamo);

								m_uState = 1;
								}
							else {
								if( GetJamoIndex2(jamo) ) {

									m_pHost->OnMessage(uKeyDown, m_Hangul);

									m_Hangul = GetJamoChar(jamo);

									m_pHost->OnMessage(uKeyDown, m_Hangul);

									m_uState = 0;
									}
								}
							}
						break;
					}

				if( m_uMode == modeHangulUpper ) {
					
					SetMode(modeHangulLower);
					}

				m_fDirty = TRUE;
				}
			}
		}

	if( uID == keyCaps && uMessage == 0 ) {

		SetMode(nData ? modeCaps  : modeLower);
		}

	if( uID == keyShift && uMessage == 0 ) {

		if( m_uMode == modeHangulLower || m_uMode == modeHangulUpper ) {

			SetMode(nData ? modeHangulUpper : modeHangulLower);
			}
		else {
			if( m_uMode == modeLower || m_uMode == modeCaps ) {
				
				SetMode(modeShift); 
				}
			
			else if( m_uMode == modeShift ) {
				
				SetMode(modeShift2);
				}
			
			else if( m_uMode == modeShift2 ) {
				
				SetMode(modeLower);
				}
			}
		}

	if( uID == keyExotic && uMessage == 0 ) {

		if( nData ) {

			if( m_uType & kpsPinYin ) {
			
				StartPinYin();
				}

			if( m_uType & kpsHangul ) {

				StartHangul();
				}
			}
		else {
			if( m_uMode == modeHangulLower || m_uMode == modeHangulUpper ) {

				if( m_uState ) {

					m_pHost->OnMessage(uKeyDown, m_Hangul);
					}
				}

			SetMode(modeLower);
			}
		}

	if( uID == keyClose && uMessage == msgTouchUp ) {

		m_pHost->OnMessage(uKeyDown, 0x1B);

		return;
		}

	if( uID == keyEnter && uMessage == msgTouchUp ) {

		m_pHost->OnMessage(uKeyDown, 0x0D);

		return;
		}

	if( uID == keyPrev && uMessage == msgTouchUp ) {

		m_pHost->OnMessage(uKeyDown, 0x08);

		return;
		}

	if( uID == keyNext && uMessage == msgTouchUp ) {

		m_pHost->OnMessage(uKeyDown, 0x09);

		return;
		}

	if( uID == keyLeft && uMessage == msgTouchUp ) {

		m_pHost->OnMessage(uKeyDown, '<');

		return;
		}

	if( uID == keyRight && uMessage == msgTouchUp ) {

		m_pHost->OnMessage(uKeyDown, '>');

		return;
		}

	if( uID == keyRaise && uMessage == msgTouchUp ) {

		m_pHost->OnMessage(uKeyUp, 0x0B);

		return;
		}

	if( uID == keyLower && uMessage == msgTouchUp ) {

		m_pHost->OnMessage(uKeyUp, 0x0A);

		return;
		}

	if( m_uMode <= modeShift2 ) {

		if( uID == keySign && uMessage == msgTouchDown ) {

			m_pHost->OnMessage(uKeyDown, '+');

			return;
			}

		if( uMessage == msgTouchDown || uMessage == msgTouchRepeat ) {

			if( uID == keyDelete ) {

				m_pHost->OnMessage(uKeyDown, 0x7F);

				return;
				}

			if( uID == keySpace ) {

				m_pHost->OnMessage(uKeyDown, 0x20);

				return;
				}

			if( uID == keyRaise ) {

				m_pHost->OnMessage(uKeyDown, 0x0B);

				return;
				}

			if( uID == keyLower ) {

				m_pHost->OnMessage(uKeyDown, 0x0A);

				return;
				}

			if( isupper(uID) || isdigit(uID) || ispunct(uID) ) {

				WCHAR w[2];

				((IButton *) pCtrl)->GetText(w);

				if( m_pHost->OnMessage(msgKeyDown, w[0]) ) {

					if( m_uMode == modeShift ) {
						
						SetMode(modeLower);
						}
					
					if( m_uMode == modeShift2 ) {

						SetMode(modeLower);
						}
					}
				}
			}
		}

	}

// IStyle Methods

void CKeypad::GetColor(UINT uStyle, UINT uCode, COLOR &Color)
{
	if( uStyle == 1 || uStyle == 3 ) {

		if( uCode == colButtonText ) {

			Color = GetRGB(0,0,15);

			return;
			}
		}

	if( uStyle == 2 ) {

		if( uCode == colButtonText ) {

			Color = GetRGB(31,0,0);

			return;
			}
		}

	if( uStyle == 4 ) {

		if( uCode == colButtonText ) {

			Color = GetRGB(12,0,0);

			return;
			}
		}

	if( uStyle == 5 ) {

		if( uCode == colButtonText ) {

			Color = GetRGB(0,12,0);

			return;
			}
		}

	if( m_pStyle ) {

		m_pStyle->GetColor(uStyle, uCode, Color);
		}
	}

void CKeypad::GetMetric(UINT uStyle, UINT uCode, int &nSize)
{
	if( m_pStyle ) {

		m_pStyle->GetMetric(uStyle, uCode, nSize);
		}
	}

void CKeypad::GetFont(UINT uStyle, UINT uCode, IGdiFont * &pFont)
{
	if( uStyle == 1 ) {

		if( uCode == fontButton ) {

			pFont = (IGdiFont *) m_uFont1;

			return;
			}
		}

	if( uStyle == 3 ) {

		if( uCode == fontButton ) {

			pFont = (IGdiFont *) m_uFont3;

			return;
			}
		}

	if( uStyle == 6 ) {

		if( uCode == fontButton ) {

			pFont = (IGdiFont *) m_uFont6;

			return;
			}
		}

	if( m_pStyle ) {

		m_pStyle->GetFont(uStyle, uCode, pFont);
		}

	pFont = (IGdiFont *) m_uFontD;
	}

// Key Creation

IButton * CKeypad::CreateKey(char cCode)
{
	if( m_fHide && !m_fRamp ) {

		switch( cCode ) {

			case keyNext:
			case keyPrev:
				
				return NULL;
			}
		}

	switch( cCode ) {

		case keyExotic:

			if( !(m_uType & (kpsPinYin | kpsHangul)) ) {

				return NULL;
				}
			break;
		}

	switch( cCode ) {

		case keyEnter:
		case keyClose:
		case keyDelete:
		case keyRaise:
		case keyLower:
		case keyNext:
		case keyPrev:
		case keyLeft:
		case keyRight:
			return Create_IconButton();
		}

	return Create_PushButton();
	}

// Implementation

void CKeypad::StartPinYin(void)
{
	m_uState  = 0;

	m_uDepth  = 0;

	m_sPin[0] = 0;

	SetMode(modePinYin);
	}

void CKeypad::StartHangul(void)
{
	m_uState  = 0;

	m_jamo1   = 0;

	m_jamo2   = 0;
	
	m_jamo3   = 0;

	SetMode(modeHangulLower);
	}

void CKeypad::FindDisplay(void)
{
	PCTXT p = GetRowData(0);

	int   n;

	for( n = 0; p[n] == ' '; n++ );

	m_Display.x1 = m_xp;

	m_Display.y1 = m_yp;
	
	m_Display.x2 = m_xp + n * m_cx;
	
	m_Display.y2 = m_yp + 1 * m_cy;

	DeflateRect(m_Display, 2, 2);
	}
	
void CKeypad::MakeKeys(IGdi *pGDI)
{
	m_uRows  = GetRowCount();

	m_uCols  = GetColCount();

	int gap  = m_fSmall ? 1: 4;

	R2 Rect  = m_Rect;

	Rect.x1  = Rect.x1 + gap;

	Rect.x2  = Rect.x2 - gap;

	Rect.y1  = Rect.y1 + gap + (m_fHead ? m_yHead : 0);

	Rect.y2  = Rect.y2 - gap - (m_fFoot ? m_yFoot : 0);

	m_cx     = ((Rect.x2 - Rect.x1) / m_uCols) & ~1;

	m_cy     = ((Rect.y2 - Rect.y1) / m_uRows) & ~1;

	m_xp     = Rect.x1 + ((Rect.x2 - Rect.x1) - m_cx * m_uCols) / 2;

	m_yp     = Rect.y1 + ((Rect.y2 - Rect.y1) - m_cy * m_uRows) / 2;

	m_pList  = New IControl * [ m_uRows * m_uCols ];

	m_uCount = 0;

	for( UINT r = 0; r < m_uRows; r++ ) {

		PTXT d = strdup(GetRowData(r));

		if( m_fRamp ) {
			
			PTXT n = strchr(d, keyNext);
			PTXT p = strchr(d, keyPrev);

			if( n ) *n = keyRaise;
			if( p ) *p = keyLower;
			}

		for( UINT c = 0; d[c]; c++ ) {

			if( d[c] > 32 ) {

				IButton *p = CreateKey(d[c]);

				if( p ) {

					R2 Rect;

					int gap = m_fSmall ? 1 : 2;

					Rect.x1 = m_xp + c * m_cx + gap;
					
					Rect.y1 = m_yp + r * m_cy + gap;
					
					Rect.x2 = Rect.x1  + m_cx - gap * 2;
					
					Rect.y2 = Rect.y1  + m_cy - gap * 2;

					if( IsKeyWide(d[c]) ) { 

						Rect.x2 += m_cx;
						}

					WCHAR t[10]  = { 0 };

					UINT uFlags = 0;

					UINT uStyle = GetKeyStyle  (d[c]);

					GetKeyText(t, d[c]);

					if( IsKeyLatching(d[c]) ) {

						uFlags |= bsLatch;
						}

					if( (m_uType & kpsMaximum) >= kpsLarge ) {

						uFlags |= bsBold;
						}

					if( (m_uType & kpsMaximum) >= kpsLarger ) {

						uFlags |= bsThick;
						}

					p->Create  (pGDI, this, Rect, d[c], uFlags, uStyle, this);

					p->SetState(GetKeyState(d[c]));

					p->SetText (t);

					if( d[c] == keySpace ) {

						m_uSpace = m_uCount;
						}
					}

				m_pList[m_uCount++] = p;
				}			
			}

		free(d);
		}
	}

UINT CKeypad::GetRowCount(void)
{
	UINT r;

	for( r = 0; GetRowData(r)[0]; r++ );

	return r;
	}

UINT CKeypad::GetColCount(void)
{
	UINT  c = 0;

	PCTXT p = NULL;

	UINT  r;

	for( r = 0; (p = GetRowData(r))[0]; r++ ) {

		UINT n = strlen(p);

		MakeMax(c, n);
		}

	return c;
	}

PCTXT CKeypad::GetRowData(UINT uRow)
{
	if( LOBYTE(m_uType) == keypadAlpha ) {

		if( m_uLayout == kpsNormal ) {

			if( uRow == 0 ) return "           x";
			if( uRow == 1 ) return "1234567890d ";
			if( uRow == 2 ) return "yQWERTYUIOPn";
			if( uRow == 3 ) return "c ASDFGHJKLp";
			if( uRow == 4 ) return "s ZXCVBNMb e";
			}

		if( m_uLayout == kpsHebrew ) {

			if( uRow == 0 ) return "            x";
			if( uRow == 1 ) return "1234567890-d ";
			if( uRow == 2 ) return "yQWERTYUIOP[n";
			if( uRow == 3 ) return "c ASDFGHJKL,p";
			if( uRow == 4 ) return "s ZXCVBNM?.be";
			}

		if( m_uLayout == kpsFrench ) {

			if( uRow == 0 ) return "            x";
			if( uRow == 1 ) return "1234567890+d ";
			if( uRow == 2 ) return "yQWERTYUIOP n";
			if( uRow == 3 ) return "c ASDFGHJKLMp";
			if( uRow == 4 ) return "s ZXCVBN?.b e";
			}
		}

	if( LOBYTE(m_uType) == keypadNumeric ) {

		if( uRow == 0 ) return "   x";
		if( uRow == 1 ) return "789n";
		if( uRow == 2 ) return "456p";
		if( uRow == 3 ) return "123 ";
		if( uRow == 4 ) return "i0.e";
		}

	if( LOBYTE(m_uType) == keypadUnsigned ) {

		if( uRow == 0 ) return "   x";
		if( uRow == 1 ) return "789n";
		if( uRow == 2 ) return "456p";
		if( uRow == 3 ) return "123 ";
		if( uRow == 4 ) return " 0.e";
		}

	if( LOBYTE(m_uType) == keypadExponent ) {

		if( uRow == 0 ) return "   x";
		if( uRow == 1 ) return "789n";
		if( uRow == 2 ) return "456p";
		if( uRow == 3 ) return "123E";
		if( uRow == 4 ) return "i0.e";
		}

	if( LOBYTE(m_uType) == keypadHex ) {

		if( uRow == 0 ) return "    x";
		if( uRow == 1 ) return "789Fn";
		if( uRow == 2 ) return "456Ep";
		if( uRow == 3 ) return "123D ";
		if( uRow == 4 ) return "0ABCe";
		}

	if( LOBYTE(m_uType) == keypadOctal ) {

		if( uRow == 0 ) return "   x";
		if( uRow == 1 ) return "67 n";
		if( uRow == 2 ) return "345p";
		if( uRow == 3 ) return "012e";
		}

	if( LOBYTE(m_uType) == keypadBinary ) {

		if( uRow == 0 ) return "    x";
		if( uRow == 1 ) return "01pne";
		}

	if( LOBYTE(m_uType) == keypadNudge1 ) {

		if( uRow == 0 ) return "    x";
		if( uRow == 1 ) return "rlpne";
		}

	if( LOBYTE(m_uType) == keypadNudge2 ) {

		if( uRow == 0 ) return "   x";
		if( uRow == 1 ) return "rlpn";
		}

	if( LOBYTE(m_uType) == keypadNudge3 ) {

		if( uRow == 0 ) return "      x";
		if( uRow == 1 ) return "rl<>pne";
		}

	if( LOBYTE(m_uType) == keypadLogon ) {

		if( uRow == 0 ) return "           x";
		if( uRow == 1 ) return "1234567890d ";
		if( uRow == 2 ) return " QWERTYUIOP ";
		if( uRow == 3 ) return "c ASDFGHJKL ";
		if( uRow == 4 ) return "s ZXCVBNMb e";
		}

	if( LOBYTE(m_uType) == keypadChange ) {

		if( uRow == 0 ) {

			if( (m_uType & kpsMaximum) == kpsMaximum ) {
				
				return "       ex";
				}

			return "           ex";
			}
		}

	if( LOBYTE(m_uType) == keypadMessage ) {

		if( uRow == 0 ) return "        x";
		}

	if( LOBYTE(m_uType) == keypadSecurity ) {

		if( uRow == 0 ) return "       ex";
		}

	return "";
	}

UINT CKeypad::GetInitMode(void)
{
	switch( LOBYTE(m_uType) ) {

		case keypadLogon:

			return modeLower;

		case keypadAlpha:

			if( m_uLayout == kpsHebrew ) {

				return modeLower;
				}

			return modeCaps;
		}
	
	return modeCaps;
	}

BOOL CKeypad::UseRedScheme(void)
{
	switch( LOBYTE(m_uType) ) {

		case keypadLogon:
		case keypadChange:
		case keypadMessage:
		case keypadSecurity:

			return TRUE;
		}

	return FALSE;
	}

void CKeypad::FindFonts(void)
{
	if( !m_fSmall ) {

		if( !(m_uType & kpsHangul) ) {

			if( UseBigFonts() ) {

				if( (m_uType & kpsMaximum) == kpsLarge ) {
					
					m_uFontD = fontBig24;
					}

				if( (m_uType & kpsMaximum) == kpsLarger ) {

					m_uFontH = fontHei24Bold;

					m_uFontF = fontHei24;

					m_yHead  = 30;

					m_yFoot  = 30;
					
					m_uFont6 = fontBig24;

					m_uFontD = fontBig32;
					}

				if( (m_uType & kpsMaximum) == kpsMaximum ) {

					m_uFontH = fontHei24Bold;

					m_uFontF = fontHei24;

					m_yHead  = 30;

					m_yFoot  = 30;
					
					switch( m_dy ) {

						case 600:

							m_uFont6 = fontBig64;

							m_uFontD = fontBig96;

							break;

						case 768:

							m_uFont6 = fontBig64;

							m_uFontD = fontBig96;

							break;

						default:

							m_uFont6 = fontBig32;

							m_uFontD = fontBig64;

							break;
						}
					}
				}
			else {
				if( (m_uType & kpsMaximum) >= kpsLarger ) {

					m_uFontH = fontHei24Bold;

					m_uFontF = fontHei24;

					m_yHead  = 30;

					m_yFoot  = 30;
					
					m_uFont1 = fontHei24;
					
					m_uFont3 = fontHei24;

					m_uFont6 = fontHei24;

					m_uFontD = fontHei24Bold;

					m_uFontP = fontHei16;
					}
				}
			}
		}
	}

BOOL CKeypad::UseBigFonts(void)
{
	switch( LOBYTE(m_uType) ) {

		case keypadAlpha:
		case keypadNudge1:
		case keypadNudge2:
		case keypadNudge3:
		case keypadLogon:
		case keypadChange:
		case keypadMessage:
		case keypadSecurity:

			return FALSE;
		}

	return TRUE;
	}

BOOL CKeypad::IsKeyWide(char cCode)
{
	switch( cCode ) {

		case keyCaps:   return TRUE;
		case keyShift:  return TRUE;
		case keySpace:  
			
			switch( LOBYTE(m_uType) ) {
				
				case keypadAlpha:
				
					return !(m_uLayout == kpsHebrew);
				}

			return TRUE;

		case keyDelete: return TRUE;
		}

	return FALSE;
	}

BOOL CKeypad::IsKeyLatching(char cCode)
{
	switch( cCode ) {

		case keyExotic: return TRUE;
		case keyCaps:   return TRUE;
		case keyShift:  return TRUE;
		}

	return FALSE;
	}

BOOL CKeypad::IsKeyEnabled(int nPos, char cCode)
{
	if( m_uMode == modePinYin ) {

		switch( cCode ) {

			case keyCaps:  return FALSE;
			case keyShift: return FALSE;
			}

		if( cCode == keySpace ) {

			if( m_uDepth ) {

				PCWORD p = m_pPinYin[m_uState];

				if( IntelToHost(p[0]) == nodeLeaf ) {

					return m_nGlyphs > 36;
					}

				return FALSE;
				}

			return TRUE;
			}

		if( isupper(cCode) || isdigit(cCode) ) {

			PCWORD p = m_pPinYin[m_uState];

			if( IntelToHost(p[0]) == nodeInternal ) {

				char c = char(tolower(cCode));

				for( UINT n = 0; p[1+2*n]; n++ ) {

					if( IntelToHost(p[1+2*n]) == c ) {

						return TRUE;
						}
					}

				return FALSE;
				}

			return nPos + m_nStart < m_nGlyphs;
			}
		}

	if( m_uMode == modeHangulLower || m_uMode == modeHangulUpper ) {

		switch( cCode ) {

			case keyCaps:  return FALSE;
			}
		}

	return TRUE;
	}

UINT CKeypad::GetKeyStyle(char cCode)
{
	switch( cCode ) {

		case keyDelete: return 1;
		case keyCaps:   return 1;
		case keyShift:  return 1;
		case keyRaise:  return 5;
		case keyLower:  return 5;
		case keyNext:   return 1;
		case keyPrev:   return 1;
		case keyExotic: return 2;
		case keySpace:  return 3;
		case keyClose:  return 4;
		case keyEnter:  return 5;
		case keySign:   return 6;
		case keyLeft:   return 5;
		case keyRight:  return 5;
		}

	return 0;
	}

UINT CKeypad::GetKeyState(char cCode)
{
	switch( cCode ) {

		case keyCaps:   return m_uMode == modeCaps;
		case keyShift:  return m_uMode == modeShift  || m_uMode == modeShift2 || m_uMode == modeHangulUpper;
		case keyExotic: return m_uMode == modePinYin || m_uMode >= modeHangulLower;
		}

	return FALSE;
	}

void CKeypad::GetKeyText(PUTF pText, char cCode)
{
	if( cCode == keyExotic ) {

		if( m_uType & kpsPinYin ) {

			wstrcpy(pText, L"\x62FC");

			return;
			}

		if( m_uType & kpsHangul ) {

			wstrcpy(pText, L"\xD55C");

			return;
			}
		}

	if( cCode == keySpace ) {

		switch( LOBYTE(m_uType) ) {
			
			case keypadAlpha:

				if( m_uLayout == kpsHebrew ) {
					
					wstrcpy(pText, L"spc");  return;
					}

				break;
			}
		}

	switch( cCode ) {

		case keyEnter:  wstrcpy(pText, L"E");      return;
		case keyClose:  wstrcpy(pText, L"X");      return;
		case keyDelete: wstrcpy(pText, L"B");      return;
		case keyRaise:	wstrcpy(pText, L"u");      return;
		case keyLower:	wstrcpy(pText, L"d");      return;
		case keyNext:	wstrcpy(pText, L"R");      return;
		case keyPrev:	wstrcpy(pText, L"L");      return;
		case keyCaps:   wstrcpy(pText, L"caps");   return;
		case keyShift:  wstrcpy(pText, L"shift");  return;
		case keySpace:  wstrcpy(pText, L"space");  return;
		case keySign:	wstrcpy(pText, L"+/-");    return;
		case keyLeft:	wstrcpy(pText, L"l");      return;
		case keyRight:	wstrcpy(pText, L"r");      return;
		}
	
	pText[0] = Transform(m_uMode, 0, cCode);
	
	pText[1] = 0;
	}

WORD CKeypad::Transform(UINT uMode, int nPos, char cCode)
{
	if( uMode == modePinYin ) {

		PCWORD p = m_pPinYin[m_uState];

		if( IntelToHost(p[0]) == nodeLeaf ) {

			nPos += m_nStart;

			if( nPos < m_nGlyphs ) {

				return IntelToHost(p[1+nPos]);
				}

			return 32;
			}

		return char(tolower(cCode));
		}

	if( uMode == modeHangulLower ) {

		short c = GetKeyJamo(tolower(cCode));

		if( c > 0 ) {

			return GetJamoChar(c);
			}
		}

	if( uMode == modeHangulUpper ) {

		short c = GetKeyJamo(toupper(cCode));

		if( c > 0 ) {

			return GetJamoChar(c);
			}
		}

	if( LOBYTE(m_uType) == keypadAlpha ) {

		switch( m_uLayout ) {

			case kpsNormal:	return TransformNormal(uMode, nPos, cCode);
			case kpsHebrew:	return TransformHebrew(uMode, nPos, cCode);
			case kpsFrench:	return TransformFrench(uMode, nPos, cCode);
			}
		}

	return TransformNormal(uMode, nPos, cCode);
	}

WORD CKeypad::TransformNormal(UINT uMode, int nPos, char cCode)
{
	if( uMode == modeLower ) {

		return char(tolower(cCode));
		}

	if( uMode == modeShift ) {

		if( cCode >= '0' && cCode <= '9' ) {

			PCTXT p = ".!?&@+-[],";

			return p[cCode - '0'];
			}

		return cCode;
		}

	if( uMode == modeShift2 ) {
		
		if( cCode >= '0' && cCode <= '9' ) {

			PCTXT p = ".!?&@+-[],";

			return p[cCode - '0'];
			}

		if( cCode >= 'A' && cCode <= 'Z' ) {
			      
			PCTXT p = "\\  ^`<>;/=*_  '\"$~|{: %)}(";

			return p[cCode - 'A'];
			}
		
		return cCode;
		}

	if( uMode == modeCaps ) {

		return cCode;
		}

	return cCode;
	}

WORD CKeypad::TransformHebrew(UINT uMode, int nPos, char cCode)
{
	if( uMode == modeLower ) {
		
		switch( cCode ) {

			case 'Q':	return '/';
			case 'W':	return L'\x5E7';
			case 'E':	return L'\x5E8';
			case 'R':	return L'\x5D0';
			case 'T':	return L'\x5D8';
			case 'Y':	return L'\x5D5';
			case 'U':	return L'\x5DF';
			case 'I':	return L'\x5DD';
			case 'O':	return L'\x5E4';
			case 'P':	return ']';
			case '[':	return '[';

			case 'A':	return L'\x5E9';
			case 'S':	return L'\x5D3';
			case 'D':	return L'\x5D2';
			case 'F':	return L'\x5DB';
			case 'G':	return L'\x5E2';
			case 'H':	return L'\x5D9';
			case 'J':	return L'\x5D7';
			case 'K':	return L'\x5DC';
			case 'L':	return L'\x5DA';
			case ',':	return L'\x5E3';

			case 'Z':	return L'\x5D6';
			case 'X':	return L'\x5E1';
			case 'C':	return L'\x5D1';
			case 'V':	return L'\x5D4';
			case 'B':	return L'\x5E0';
			case 'N':	return L'\x5DE';
			case 'M':	return L'\x5E6';
			case '?':	return L'\x5EA';
			case '.':	return L'\x5E5';
			}
		
		return cCode;
		}

	if( uMode == modeShift ) {

		if( cCode >= '0' && cCode <= '9' ) {
		
			PCTXT p = "_!@#%^&*()";
			
			return p[cCode - '0'];
			}
		
		switch( cCode ) {

			case '-':	return '+';
			}

		return cCode;
		}

	if( uMode == modeShift2 ) {
		
		if( cCode >= '0' && cCode <= '9' ) {
		
			PCTXT p = "_!@#%^&*()";
			
			return p[cCode - '0'];
			}

		if( cCode >= 'A' && cCode <= 'Z' ) {
			        
			PCTXT p = "\\  =`<>[/]    '\"$~|{; : } ";

			return p[cCode - 'A'];
			}
		
		return cCode;
		}

	if( uMode == modeCaps ) {

		return cCode;
		}

	return cCode;
	}

WORD CKeypad::TransformFrench(UINT uMode, int nPos, char cCode)
{
	if( uMode == modeLower ) {

		return WCHAR(wtolower(TransformFrench(modeCaps, nPos, cCode)));
		}

	if( uMode == modeShift ) {

		if( cCode >= '0' && cCode <= '9' ) {

			PCUTF p = L"]&\x0E9![-\x0E8,\x0E7\x0E0";

			return p[cCode - '0'];
			}
		
		switch( cCode ) {

			case '?':	return '@';
			case '.':	return L'\x0F9';
			}

		return cCode;
		}

	if( uMode == modeShift2 ) {
		
		if( cCode >= '0' && cCode <= '9' ) {

			PCUTF p = L"]&\x0E9![-\x0E8,\x0E7\x0E0";

			return p[cCode - '0'];
			}

		if( cCode >= 'A' && cCode <= 'Z' ) {

			PCTXT p = "\\  ^`()/*$#\"% =_<~|{- >+}:";
	           
			return p[cCode - 'A'];
			}
		
		return cCode;
		}

	if( uMode == modeCaps ) {

		if( cCode >= 'A' && cCode <= 'Z' ) {

			PCUTF p = L"QBCDEFGHIJKLMNOPARSTUVZXYW";

			return p[cCode - 'A'];
			}

		return cCode;
		}

	return cCode;
	}

void CKeypad::SetMode(UINT uMode)
{
	if( m_uMode - uMode || uMode == modePinYin ) {

		m_uMode = uMode;

		UINT nc = 0;

		UINT ni = 0;

		for( UINT r = 0; r < m_uRows; r++ ) {

			PCTXT d = GetRowData(r);

			for( UINT c = 0; d[c]; c++ ) {
	
				if( d[c] > 32 ) {

					IButton *p = (IButton *) m_pList[nc++];

					if( p ) {

						if( !IsKeyEnabled(ni, d[c]) ) {

							p->SetState(0);

							p->Enable(FALSE);
							}
						else {
							p->SetState(GetKeyState(d[c]));

							p->Enable(TRUE);
							}

						if( !islower(d[c]) ) {

							WCHAR t[2] = { 0, 0 };

							t[0] = Transform(uMode, ni++, d[c]);

							p->SetText(t);
							}

						if ( d[c] == keyShift ) {

							WCHAR t[2] = { 0, 0 };

							t[0] = Transform(uMode, ni++, d[c]);

							if ( m_uMode == modeLower || m_uMode == modeCaps ) {
								
								p->SetText(L"shift");
								}
							
							if ( m_uMode == modeShift ) {
								
								p->SetState(0);
								
								p->SetText(L"more");
								}
							
							if ( m_uMode == modeShift2 ) {
								
								p->SetState(0);
								
								p->SetText(L"back");
								}
							}

						}
					}

				}
			}

		IButton *p = (IButton *) m_pList[m_uSpace];

		if( m_uMode == modePinYin ) {
			
			if( m_uDepth > 0 ) {
				
				if( m_nStart ) {

					p->SetText(L"\x4E0A\x9875");
					}
				else
					p->SetText(L"\x4E0B\x9875");
				}
			else
				p->SetText(L"\x7A7A\x683C");
			}
		else {
			WCHAR t[10] = { 0 };

			GetKeyText(t, keySpace);

			p->SetText(t);			
			}

		m_fDirty = TRUE;
		}
	}

void CKeypad::DrawEntry(IGdi *pGDI)
{
	R2 Rect = m_Display;

	pGDI->ResetAll();

	pGDI->SelectFont  (m_uFont6);

	pGDI->SetTextBack (GetRGB(31,31,31));

	pGDI->SetTextFore (GetRGB(0,0,0));

	pGDI->SetBrushFore(GetRGB(31,31,31));

	pGDI->SetPenFore  (GetRGB(0,0,0));

	pGDI->FillRect(PassRect(Rect));

	pGDI->DrawRect(PassRect(Rect));

	if( (m_uType & kpsMaximum) >= kpsLarger ) {

		DeflateRect(Rect, 1, 1);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);

		pGDI->DrawRect(PassRect(Rect));
		}

	pGDI->SetBrushFore(GetRGB(0,0,15));

	PCUTF pt = m_sData;

	int   cy = pGDI->GetTextHeight("X");

	int   cx = pGDI->GetTextWidth (pt);

	int   ty = Rect.y1 + (Rect.y2 - Rect.y1 - cy) / 2;

	int   tx = Rect.x1 + 4;

	int   rx = 6;

	UINT  sp = 0;

	if( m_uMode == modePinYin ) {

		rx += 7 * m_uDepth;
		}

	if( m_uMode == modeHangulLower || m_uMode == modeHangulUpper ) {

		rx += m_uState ? 7 : 0;
		}

	while( tx + cx > Rect.x2 - rx ) {

		WCHAR s[2] = { *pt++, 0 };

		cx -= pGDI->GetTextWidth(s);

		sp += 1;
		}

	if( m_uCursor == cursorAll || m_uCursor == cursorError || m_uCursor == cursorNone ) {

		if( m_uCursor == cursorAll ) {

			pGDI->SetTextFore(GetRGB(31,31,31));

			pGDI->SetTextBack(GetRGB(0,0,15));

			pGDI->SetPenFore (GetRGB(0,0,15));
			}

		if( m_uCursor == cursorError ) {

			pGDI->SetTextFore(GetRGB(31,31,31));

			pGDI->SetTextBack(GetRGB(31,0,0));

			pGDI->SetPenFore (GetRGB(31,0,0));
			}

		if( m_uMode < modePinYin ) {

			if( m_uCursor != cursorNone ) {

				int xs = pGDI->GetTextWidth(pt);

				pGDI->DrawLine(tx, ty-1,  tx+xs, ty-1 );

				pGDI->DrawLine(tx, ty+cy, tx+xs, ty+cy);
				}

			pGDI->TextOut(tx, ty, UniVisual(pt));

			return;
			}
		}

	if( m_uCursor == cursorLast ) {

		pGDI->TextOut(tx, ty, UniVisual(pt));

		tx += cx;
		}
	else {
		for( UINT n = 0; !n || pt[n-1]; n++ ) {

			WCHAR data = pt[n] ? pt[n] : spaceNarrow;

			WCHAR s[2] = { data , 0 };

			if( n + sp == m_uCursor || m_uCursor == cursorAll ) {

				pGDI->PushFont();

				pGDI->SetTextFore(GetRGB(31,31,31));

				pGDI->SetTextBack(GetRGB(0,0,15));

				pGDI->SetPenFore (GetRGB(0,0,15));

				pGDI->TextOut(tx, ty, s);

				int xs = pGDI->GetTextWidth(s);

				pGDI->DrawLine(tx, ty-1,  tx+xs, ty-1 );

				pGDI->DrawLine(tx, ty+cy, tx+xs, ty+cy);

				tx += xs;

				pGDI->PullFont();
				}
			else {
				pGDI->TextOut(tx, ty, s);

				tx += pGDI->GetTextWidth(s);
				}
			}
		}

	if( m_uMode == modePinYin ) {

		if( m_uDepth ) {

			pGDI->SelectFont(m_uFontP);

			int cy = pGDI->GetTextHeight("X");

			int ty = Rect.y1 + (Rect.y2 - Rect.y1 - cy) / 2;

			pGDI->SetTextFore(GetRGB(31,0,0));

			pGDI->SetTextBack(GetRGB(31,31,31));

			pGDI->TextOut(tx, ty, m_sPin);

			tx += pGDI->GetTextWidth(m_sPin);

			pGDI->SetBrushFore(GetRGB(31,0,0));

			pGDI->FillRect(tx+1, ty, tx+3, ty+cy);

			return;
			}

		pGDI->SetBrushFore(GetRGB(31,0,0));
		}

	if( m_uMode == modeHangulLower || m_uMode == modeHangulUpper ) {

		if( m_uState ) {

			pGDI->SelectFont(m_uFont6);

			int cy = pGDI->GetTextHeight("X");

			int ty = Rect.y1 + (Rect.y2 - Rect.y1 - cy) / 2;

			pGDI->SetTextFore(GetRGB(31,0,0));

			pGDI->SetTextBack(GetRGB(31,31,31));

			WCHAR s[2] = { m_Hangul, 0 };

			pGDI->TextOut(tx, ty, s);

			tx += pGDI->GetTextWidth(s);

			pGDI->SetBrushFore(GetRGB(31,0,0));

			pGDI->FillRect(tx+1, ty, tx+3, ty+cy);

			return;
			}
		}

	if( m_uCursor == cursorLast ) {

		CArray <UINT> MemToVis;
		
		CArray <UINT> VisToMem;

		CArray <UINT> MemLevel;
		
		UniVisual(pt, MemToVis, VisToMem, MemLevel);

		UINT uCount = MemToVis.GetCount();

		if( uCount ) {
			
			UINT uPos = VisToMem[uCount - 1];

			tx -= cx;

			for( UINT n = 0; n < uPos + 1; n ++ ) {

				tx += pGDI->GetTextWidth(pt[MemToVis[n]]);
				}

			if( !uPos && (MemLevel[MemToVis[uPos]] & 1) ) {

				tx -= pGDI->GetTextWidth(pt[MemToVis[uPos]]);
				}				
			}

		pGDI->FillRect(tx-1, ty-1, tx+1, ty+cy+1);
		}
	}

void CKeypad::DrawText(IGdi *pGDI, R2 Rect, UINT uFont, PUTF pText, BOOL fNum)
{
	pGDI->ResetAll();

	pGDI->SetTextTrans(modeTransparent);

	pGDI->SetTextFore (UseRedScheme() ? GetRGB(31,31,31) : GetRGB(0,0,0));

	pGDI->SelectFont  (uFont);

	PUTF pt = pText;

	int  cx = pGDI->GetTextWidth(pt);

	int  sx = Rect.x2 - Rect.x1 - 2;

	if( cx > sx ) {

		if( fNum ) {

			for( UINT n = 0; pt[n]; n++ ) {

				if( pt[n] == '.' ) {

					UINT i;

					for( i = 1; pt[n+i]; i++ ) {

						if( !isdigit(pt[n+i]) ) {

							break;
							}
						}

					wstrcpy(pt + n, pt + n + i);
					}
				}

			if( (cx = pGDI->GetTextWidth(pt)) > sx ) {

				return;
				}
			}
		else {
			for( UINT n = wstrlen(pt); n; n-- ) {

				wstrcpy(pt + n, L"...");

				cx = pGDI->GetTextWidth(pt);

				if( cx > sx ) {

					continue;
					}

				break;
				}
			}
		}

	int cy = pGDI->GetTextHeight(pt);

	int xp = Rect.x1 + (Rect.x2 - Rect.x1 - cx) / 2;

	int yp = Rect.y1 + (Rect.y2 - Rect.y1 - cy) / 2;

	pGDI->TextOut(xp, yp+1, pt);
	}

void CKeypad::DrawHead(IGdi *pGDI, R2 Rect)
{
	DrawText(pGDI, Rect, m_uFontH, m_sHead, FALSE);
	}

void CKeypad::DrawFoot(IGdi *pGDI, R2 Rect)
{
	DrawText(pGDI, Rect, m_uFontF, m_sFoot, TRUE);
	}

// Hangul Support

int CKeypad::GetJamoIndex1(int jamo)
{
	switch( jamo ) {

		case jamoG:	return 1;
		case jamoGG:	return 2;
		case jamoN:	return 3;
		case jamoD:	return 4;
		case jamoDD:	return 5;
		case jamoL:	return 6;
		case jamoM:	return 7;
		case jamoB:	return 8;
		case jamoBB:	return 9;
		case jamoS:	return 10;
		case jamoSS:	return 11;
		case jamoNG:	return 12;
		case jamoJ:	return 13;
		case jamoJJ:	return 14;
		case jamoC:	return 15;
		case jamoK:	return 16;
		case jamoT:	return 17;
		case jamoP:	return 18;
		case jamoH:	return 19;
		}

	return 0;
	}

int CKeypad::GetJamoIndex2(int jamo)
{
	switch( jamo ) {

		case jamoA:	return 1;
		case jamoAE:	return 2;
		case jamoYA:	return 3;
		case jamoYAE:	return 4;
		case jamoEO:	return 5;
		case jamoE:	return 6;
		case jamoYEO:	return 7;
		case jamoYE:	return 8;
		case jamoO:	return 9;
		case jamoWA:	return 10;
		case jamoWAE:	return 11;
		case jamoOE:	return 12;
		case jamoYO:	return 13;
		case jamoU:	return 14;
		case jamoWEO:	return 15;
		case jamoWE:	return 16;
		case jamoWI:	return 17;
		case jamoYU:	return 18;
		case jamoEU:	return 19;
		case jamoYI:	return 20;
		case jamoI:	return 21;
		}

	return 0;
	}

int CKeypad::GetJamoIndex3(int jamo)
{
	switch( jamo ) {

		case jamoG:	return 1;
		case jamoGG:	return 2;
		case jamoGS:	return 3;
		case jamoN:	return 4;
		case jamoNJ:	return 5;
		case jamoNH:	return 6;
		case jamoD:	return 7;
		case jamoL:	return 8;
		case jamoLG:	return 9;
		case jamoLM:	return 10;
		case jamoLB:	return 11;
		case jamoLS:	return 12;
		case jamoLT:	return 13;
		case jamoLP:	return 14;
		case jamoLH:	return 15;
		case jamoM:	return 16;
		case jamoB:	return 17;
		case jamoBS:	return 18;
		case jamoS:	return 19;
		case jamoSS:	return 20;
		case jamoNG:	return 21;
		case jamoJ:	return 22;
		case jamoC:	return 23;
		case jamoK:	return 24;
		case jamoT:	return 25;
		case jamoP:	return 26;
		case jamoH:	return 27;
		}

	return 0;
	}

BOOL CKeypad::ComposeJamo(int &first, int second, int pos)
{
	static int const comp[][3] = 

	{
		// Consonants Clusters

		{	jamoGS,		jamoG,	 jamoS	},
		{	jamoNJ,		jamoN,	 jamoJ	},
		{	jamoNH,		jamoN,	 jamoH	},
		{	jamoLG,		jamoL,	 jamoG	},
		{	jamoLM,		jamoL,	 jamoM	},
		{	jamoLB,		jamoL,	 jamoB	},
		{	jamoLS,		jamoL,	 jamoS	},
		{	jamoLT,		jamoL,	 jamoT	},
		{	jamoLP,		jamoL, 	 jamoP	},
		{	jamoLH,		jamoL,	 jamoH	},
		{	jamoBS,		jamoB,	 jamoS	},

		// Dipthongs

		{	jamoAE,		jamoA,	 jamoI	},
		{	jamoYAE,	jamoYA,	 jamoI	},	
		{	jamoE,		jamoEO,	 jamoI	},	
		{	jamoYE,		jamoYEO, jamoI	},
		{	jamoWA,		jamoO,	 jamoA	},
		{	jamoWAE,	jamoO,	 jamoAE },
		{	jamoOE,		jamoO,	 jamoI	},	
		{	jamoWEO,	jamoU,	 jamoEO },
		{	jamoWE,		jamoU,	 jamoE  },
		{	jamoWI,		jamoU,	 jamoI  },
		{	jamoYI,		jamoEU,	 jamoI	},

		{	-1,		-1,	 -1	},

		};

	for( int n = 0; comp[n][0] > 0; n++ ) {

		if( comp[n][1] == first && comp[n][2] == second ) {

			bool okay = 0;

			switch( pos ) {

				case 1:
					okay = (GetJamoIndex1(comp[n][0]) > 0);
					break;

				case 2:
					okay = (GetJamoIndex2(comp[n][0]) > 0);
					break;

				case 3:
					okay = (GetJamoIndex3(comp[n][0]) > 0);
					break;
				}

			if( okay ) {

				first = comp[n][0];

				return true;
				}

			return false;
			}
		}

	return false;
	}

short CKeypad::GetJamoChar(int jamo)
{
	int n;

	if( (n = GetJamoIndex1(jamo)) > 0 ) {

		return 0x1100 + n - 1;
		}

	if( (n = GetJamoIndex2(jamo)) > 0 ) {

		return 0x1161 + n - 1;
		}

	if( (n = GetJamoIndex3(jamo)) > 0 ) {

		return 0x11A8 + n - 1;
		}

	return '?';
	}

short CKeypad::MakeHangul(int j1, int j2, int j3)
{
	return GetJamoIndex3(j3) + (GetJamoIndex2(j2) - 1) * 28 + (GetJamoIndex1(j1) - 1) * 588 + 44032;
	}

int CKeypad::GetKeyJamo(char cKey)
{
	switch( cKey ) {

		case 'q':	return jamoB;
		case 'w':	return jamoJ;
		case 'e':	return jamoD;
		case 'r':	return jamoG;
		case 't':	return jamoS;
		case 'y':	return jamoYO;
		case 'u':	return jamoYEO;
		case 'i':	return jamoYA;
		case 'o':	return jamoAE;
		case 'p':	return jamoE;

		case 'a':	return jamoM;
		case 's':	return jamoN;
		case 'd':	return jamoNG;
		case 'f':	return jamoL;
		case 'g':	return jamoH;
		case 'h':	return jamoO;
		case 'j':	return jamoEO;
		case 'k':	return jamoA;
		case 'l':	return jamoI;

		case 'z':	return jamoK;
		case 'x':	return jamoT;
		case 'c':	return jamoC;
		case 'v':	return jamoP;
		case 'b':	return jamoYU;
		case 'n':	return jamoU;
		case 'm':	return jamoEU;

		case 'Q':	return jamoBB;
		case 'W':	return jamoJJ;
		case 'E':	return jamoDD;
		case 'R':	return jamoGG;
		case 'T':	return jamoSS;
		case 'Y':	return jamoYO;
		case 'U':	return jamoYEO;
		case 'I':	return jamoYA;
		case 'O':	return jamoYAE;
		case 'P':	return jamoYE;

		case 'A':	return jamoM;
		case 'S':	return jamoN;
		case 'D':	return jamoNG;
		case 'F':	return jamoL;
		case 'G':	return jamoH;
		case 'H':	return jamoO;
		case 'J':	return jamoEO;
		case 'K':	return jamoA;
		case 'L':	return jamoI;

		case 'Z':	return jamoK;
		case 'X':	return jamoT;
		case 'C':	return jamoC;
		case 'V':	return jamoP;
		case 'B':	return jamoYU;
		case 'N':	return jamoU;
		case 'M':	return jamoEU;
		}

	return -1;
	}

// End of File
