
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scroll Bar Control
//

class CScrollBar : public CStdControl, public IScrollBar, public IContainer
{
	public:
		// Constructor
		CScrollBar(void);

		// Destructor
		~CScrollBar(void);

		// Management
		void Release(void);
		
		// Creation
		void Create( IContainer * pParent,
			     IManager   * pManager,
			     R2 const   & Rect,
			     UINT	  uID,
			     UINT	  uFlags,
			     UINT	  uStyle
			     );

		// Drawing
		void Draw( IManager *pManager,
			   UINT      uCode
			   );

		// Hit Testing
		BOOL HitTest(IRegion *pDirty);
		BOOL HitTest(P2 const &Point);

		// Core Attributes
		void GetRect(R2 &Rect);

		// Core Operations
		void Enable(BOOL fEnable);

		// Messages
		BOOL OnMessage(UINT uCode, DWORD dwParam);

		// Scroll Operations
		void SetValue(int nValue);
		void SetRange(int nMin, int nMax);
		int  GetValue(void);

		// Notification
		void OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData);

		// Style Access
		void GetStyle(IStyle * &pStyle);

	protected:
		// Core Data
		BOOL m_fHorz;

		// Children
		IControl *m_pTouch;
		IButton  *m_pNeg;
		IButton  *m_pPos;

		// State Data
		int  m_nValue;
		int  m_nMin;
		int  m_nMax;
		BOOL m_fPress;

		// Implementation
		void Draw(IGdi *pGDI);
		void ClipValue(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Scroll Bar Control
//

// Instantiator

IScrollBar * Create_ScrollBar(void)
{
	return New CScrollBar;
	}

// Constructor

CScrollBar::CScrollBar(void)
{
	m_pTouch   = NULL;

	m_pNeg     = Create_IconButton();

	m_pPos     = Create_IconButton();

	m_nValue   = 0;

	m_nMin     = 0;

	m_nMax     = 100;

	m_fPress   = FALSE;
	}

// Destructor

CScrollBar::~CScrollBar(void)
{
	m_pNeg->Release();

	m_pPos->Release();
	}

// Management

void CScrollBar::Release(void)
{
	delete this;
	}

// Creation

void CScrollBar::Create(IContainer *pParent, IManager *pManager, R2 const &Rect, UINT uID, UINT uFlags, UINT uStyle)
{
	m_pParent = pParent;

	m_Rect    = Rect;

	m_uID     = uID;

	m_uFlags  = uFlags;

	m_uStyle  = uStyle;

	m_fHorz   = ((Rect.x2-Rect.x1) > (Rect.y2-Rect.y1));

	R2 RectNeg, RectPos;

	if( m_fHorz ) {

		RectNeg.x1 = Rect.x1 + 2;
		RectNeg.y1 = Rect.y1 + 2;
		RectNeg.x2 = Rect.x1 + (Rect.y2 - Rect.y1 - 4);
		RectNeg.y2 = Rect.y2 - 2;

		RectPos.x1 = Rect.x2 - (Rect.y2 - Rect.y1 - 4);
		RectPos.y1 = Rect.y1 + 2;
		RectPos.x2 = Rect.x2 - 2;
		RectPos.y2 = Rect.y2 - 2;
		}
	else {
		RectNeg.x1 = Rect.x1 + 2;
		RectNeg.y1 = Rect.y1 + 2;
		RectNeg.x2 = Rect.x2 - 2;
		RectNeg.y2 = Rect.y1 + (Rect.x2 - Rect.x1 - 4);

		RectPos.x1 = Rect.x1 + 2;
		RectPos.y1 = Rect.y2 - (Rect.x2 - Rect.x1 - 4);
		RectPos.x2 = Rect.x2 - 2;
		RectPos.y2 = Rect.y2 - 2;
		}

	m_pNeg->Create(this, pManager, RectNeg, 0, 0, 0);

	m_pPos->Create(this, pManager, RectPos, 1, 0, 0);

	m_pNeg->SetText(m_fHorz ? L"L" : L"U");

	m_pPos->SetText(m_fHorz ? L"R" : L"D");
	}

// Drawing

void CScrollBar::Draw(IManager *pManager, UINT uCode)
{
	if( uCode == drawInitial || m_fDirty ) {

		IGdi *pGDI = pManager->GetGDI();

		Draw(pGDI);

		pManager->GetDirtyRegion()->AddRect(m_Rect);

		uCode    = drawInitial;

		m_fDirty = FALSE;
		}

	m_pNeg->Draw(pManager, uCode);

	m_pPos->Draw(pManager, uCode);
	}

// Hit Testing

BOOL CScrollBar::HitTest(IRegion *pDirty)
{
	return pDirty->HitTest(m_Rect);
	}

BOOL CScrollBar::HitTest(P2 const &Point)
{
	return m_fEnable && PtInRect(m_Rect, Point);
	}

// Core Attributes

void CScrollBar::GetRect(R2 &Rect)
{
	Rect = m_Rect;
	}

// Core Operations

void CScrollBar::Enable(BOOL fEnable)
{
	if( m_fEnable - fEnable ) {

		m_fEnable = fEnable;

		m_pNeg->Enable(m_fEnable);

		m_pPos->Enable(m_fEnable);

		m_fDirty  = TRUE;
		}
	}

// Messages

BOOL CScrollBar::OnMessage(UINT uCode, DWORD dwParam)
{
	if( uCode == msgSkipFocus ) {

		return !m_fEnable;
		}

	if( uCode == msgTouchDown ) {

		P2 Point;

		Point.x = LOWORD(dwParam);
		Point.y = HIWORD(dwParam);

		if( m_pNeg->HitTest(Point) ) {

			if( m_pNeg->OnMessage(uCode, dwParam) ) {

				m_pTouch = m_pNeg;

				return TRUE;
				}

			return FALSE;
			}

		if( m_pPos->HitTest(Point) ) {

			if( m_pPos->OnMessage(uCode, dwParam) ) {

				m_pTouch = m_pPos;

				return TRUE;
				}

			return FALSE;
			}

		if( m_fEnable ) {

			if( m_fHorz ) {

				int y1 = m_Rect.y1 + 2;

				int y2 = m_Rect.y2 - 2;

				int cy = y2 - y1;

				int cx = 2 * cy / 3;

				int x1 = m_Rect.x1 + cy + 3 + cx / 2;

				int x2 = m_Rect.x2 - cy - 3 - cx / 2;

				m_nValue = m_nMin + (Point.x - x1) * (m_nMax - m_nMin) / (x2 - x1);
				}
			else {
				int x1 = m_Rect.x1 + 2;

				int x2 = m_Rect.x2 - 2;

				int cx = x2 - x1;

				int cy = 2 * cx / 3;

				int y1 = m_Rect.y1 + cx + 3 + cy / 2;

				int y2 = m_Rect.y2 - cx - 3 - cy / 2;

				m_nValue = m_nMin + (Point.y - y1) * (m_nMax - m_nMin) / (y2 - y1);
				}
			
			ClipValue();

			m_fPress = TRUE;

			m_fDirty = TRUE;

			return TRUE;
			}

		return FALSE;
		}

	if( uCode == msgTouchRepeat ) {

		if( m_pTouch ) {

			if( m_pTouch->OnMessage(uCode, dwParam) ) {

				m_fDirty = TRUE;
				
				return TRUE;
				}
			
			return m_fDirty;
			}
		}

	if( uCode == msgTouchUp ) {

		if( m_pTouch ) {

			BOOL fDone = m_pTouch->OnMessage(uCode, dwParam);

			m_pTouch   = NULL;

			return fDone;
			}

		m_fPress = FALSE;

		m_fDirty = TRUE;

		return TRUE;
		}

	return FALSE;
	}

// Scroll Operations

void CScrollBar::SetValue(int nValue)
{
	m_nValue = nValue;

	// REV3 -- Check for change at pixel level?

	m_fDirty = TRUE;

	ClipValue();
	}

void CScrollBar::SetRange(int nMin, int nMax)
{
	m_nMin   = nMin;

	m_nMax   = nMax;

	m_fDirty = TRUE;

	ClipValue();
	}

int CScrollBar::GetValue(void)
{
	return m_nValue;
	}

// Notification

void CScrollBar::OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData)
{
	if( uCode == msgTouchDown || uCode == msgTouchRepeat ) {

		if( uID == 0 ) {

			m_nValue = max(m_nMin, m_nValue - 1);

			m_fDirty = TRUE;

			SendNotify(0, m_nValue);
			}

		if( uID == 1 ) {

			m_nValue = min(m_nMax, m_nValue + 1);

			m_fDirty = TRUE;

			SendNotify(0, m_nValue);
			}
		}
	}

// Style Access

void CScrollBar::GetStyle(IStyle * &pStyle)
{
	if( m_pParent ) {
		
		m_pParent->GetStyle(pStyle);
		}
	}

// Implementation

void CScrollBar::Draw(IGdi *pGDI)
{
	pGDI->SetBrushFore(GetRGB(29,29,29));

	pGDI->FillRect(PassRect(m_Rect));

	pGDI->SetPenFore(0);

	pGDI->DrawRect(PassRect(m_Rect));

	if( m_fEnable ) {

		int xp, yp, cx, cy;

		if( m_fHorz ) {

			int y1 = m_Rect.y1 + 2;

			int y2 = m_Rect.y2 - 2;

			cy = y2 - y1;

			cx = 2 * cy / 3;

			int x1 = m_Rect.x1 + cy + 3;

			int x2 = m_Rect.x2 - cy - 3 - cx;

			xp = x1 + (x2 - x1) * (m_nValue - m_nMin) / (m_nMax - m_nMin);

			yp = y1;
			}
		else {
			int x1 = m_Rect.x1 + 2;

			int x2 = m_Rect.x2 - 2;

			cx = x2 - x1;

			cy = 2 * cx / 3;

			int y1 = m_Rect.y1 + cx + 3;

			int y2 = m_Rect.y2 - cx - 3 - cy;

			xp = x1;

			yp = y1 + (y2 - y1) * (m_nValue - m_nMin) / (m_nMax - m_nMin);
			}

		pGDI->ShadeRounded(xp, yp, xp+cx, yp+cy, 5, m_fPress ? Shader2 : Shader1);
		
		pGDI->DrawRounded (xp, yp, xp+cx, yp+cy, 5);
		}
	}

void CScrollBar::ClipValue(void)
{
	MakeMax(m_nValue, m_nMin);

	MakeMin(m_nValue, m_nMax);

	SendNotify(0, m_nValue);
	}

// End of File
