
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Radio Button Control
//

class CRadioButton : public CStdButton
{
	public:
		// Constructor
		CRadioButton(void);

		// Destructor
		~CRadioButton(void);

		// Messages
		BOOL OnMessage(UINT uCode, DWORD dwParam);

		// Text Operations
		void SetText(PCUTF pText);
		void GetText(PUTF  pText);

	protected:
		// Visual Style
		int  m_nSize;

		// State Data
		WORD m_sText[10][maxString];
		UINT m_uCount;

		// Overridables
		void OnCreate  (IGdi *pGDI);
		void OnDraw    (IGdi *pGDI);
		void OnValidate(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Radio Button Control
//

// Instantiator

IButton * Create_RadioButton(void)
{
	return New CRadioButton;
	}

// Constructor

CRadioButton::CRadioButton(void)
{
	SetText(L"OPTION-1|OPTION-2|OPTION-3");

	m_nSize = 24;
	}

// Destructor

CRadioButton::~CRadioButton(void)
{
	}

// Messages

BOOL CRadioButton::OnMessage(UINT uCode, DWORD dwParam)
{
	if( uCode == msgSkipFocus ) {

		return !m_fEnable;
		}

	if( uCode == msgTouchDown ) {

		int ys = (m_Rect.y2 - m_Rect.y1 - 8) / m_uCount;

		int y1 = m_Rect.y1 + 4;

		int yp = HIWORD(dwParam);

		m_fPress = TRUE;

		m_uState = (yp - y1) / ys;

		m_uState = min(m_uState, m_uCount-1);

		m_fDirty = TRUE;

		SendNotify(0, m_uState);

		return TRUE;
		}

	if( uCode == msgTouchUp ) {

		m_fPress = FALSE;

		m_fDirty = TRUE;

		return TRUE;
		}

	return FALSE;
	}

// Text Operations

void CRadioButton::SetText(PCUTF pText)
{
	UINT n = 0, s = 0, c = 0;

	do {
		if( pText[n] == 0 || pText[n] == '|' ) {

			for( UINT i = s; i < n; i++ ) {

				m_sText[c][i-s] = pText[i];
				}

			m_sText[c++][i-s] = 0;

			s = n + 1;
			}

		} while( pText[n++] );

	m_uCount = c;

	m_fDirty = TRUE;
	}

void CRadioButton::GetText(PUTF pText)
{
	}

// Overridables

void CRadioButton::OnCreate(IGdi *pGDI)
{
	GetStyleMetric(metricRadioSize, m_nSize);
	}

void CRadioButton::OnDraw(IGdi *pGDI)
{
	int ys = (m_Rect.y2 - m_Rect.y1 - 8) / m_uCount;

	int y1 = m_Rect.y1 + 4;

	for( UINT n = 0; n < m_uCount; n++ ) {
	
		int y2 = y1 + ys;

		int cb = m_nSize;

		int xb = m_Rect.x1;

		int yb = (y1 + y2 - cb) / 2;

		BOOL fState = (m_uState == n);

		BOOL fPress = (m_fPress && fState);

		pGDI->SetPenFore  (m_fEnable ? m_colBorder : m_colDisabled);

		pGDI->ShadeEllipse(xb, yb, xb+cb, yb+cb, etWhole, fPress ? Shader2 : Shader1);

		pGDI->DrawEllipse (xb, yb, xb+cb, yb+cb, etWhole);

		if( fState ) {

			pGDI->SetBrushFore(m_fEnable ? m_colText : m_colDisabled);

			pGDI->FillEllipse(xb+4, yb+4, xb+cb-4, yb+cb-4, etWhole);
			}

		pGDI->SelectFont  (m_pFont);

		pGDI->SetTextTrans(modeTransparent);

		pGDI->SetTextFore (m_fEnable ? m_colText : m_colDisabled);

		int cy = pGDI->GetTextHeight(m_sText[n]);

		int xp = xb + 3 * cb / 2;

		int yp = y1 + (y2 - y1 - cy) / 2;

		pGDI->TextOut(xp, yp, m_sText[n]);

		y1 = y2;
		}
	}

void CRadioButton::OnValidate(void)
{
	m_uState = m_uState % m_uCount;
	}

// End of File
