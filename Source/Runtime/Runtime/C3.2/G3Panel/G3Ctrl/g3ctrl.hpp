
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Controls Library
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3CTRL_HPP

#define	INCLUDE_G3CTRL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <StdEnv.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Shader Support
//

#if 0

#define Mixer	Mix16

#define MIXINV  COL_INVALID

typedef COLOR   MIXCOL;

#else

#define Mixer	Mix32

#define	MIXINV  0

typedef DWORD   MIXCOL;
		
#endif

//////////////////////////////////////////////////////////////////////////
//
// Interfaces
//

interface IStyle;
interface INotify;
interface IMsgSink;
interface IControl;
interface IControlWithText;
interface IButton;
interface IStaticText;
interface IScrollBar;
interface IProgressBar;
interface IListBox;
interface IDropDownList;
interface IKeypad;
interface IEditControl;

//////////////////////////////////////////////////////////////////////////
//
// Visual Style Colors
//

enum
{
	colBackground,
	colButtonBorder,
	colButtonText,
	colButtonDisabled,
	colButtonShade1A,
	colButtonShade1B,
	colButtonShade2A,
	colButtonShade2B,
	colBorder,
	colSelectFore,
	colSelectBack,
	colTextFore,
	colTextBack,
	colTextGrey,
	colProgBack,
	colProgShadeA,
	colProgShadeB,
	};

//////////////////////////////////////////////////////////////////////////
//
// Visual Style Metrics
//

enum
{
	metricScreenCx,
	metricScreenCy,
	metricCheckSize,
	metricRadioSize,
	metricScrollSize,
	metricComboHeight,
	};

//////////////////////////////////////////////////////////////////////////
//
// Visual Style Fonts
//

enum
{
	fontButton
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Messages
//

enum
{
	msgSetFocus	= 100,
	msgKillFocus	= 101,
	msgSkipFocus	= 102,
	msgKeyDown	= 103,
	msgKeyUp	= 104,
	msgTouchInit	= 105,
	msgTouchDown	= 106,
	msgTouchRepeat	= 107,
	msgTouchUp	= 108,
	msgBlockRemote  = 109,
	msgGoInvisible  = 110,
	};

//////////////////////////////////////////////////////////////////////////
//
// List Box Flags
//

enum
{
	lbsInverted = 0x00000001
	};

//////////////////////////////////////////////////////////////////////////
//
// Button Flags
//

enum
{
	bsLatch   = 0x00000001,
	bsDefault = 0x00000002,
	bsBold    = 0x00000004,
	bsThick   = 0x00000008,
	};

//////////////////////////////////////////////////////////////////////////
//
// Keypad Types
//

enum
{
	keypadAlpha	= 1,
	keypadNumeric	= 2,
	keypadUnsigned  = 3,
	keypadExponent  = 4,
	keypadHex	= 5,
	keypadOctal     = 6,
	keypadBinary    = 7,
	keypadNudge1	= 8,
	keypadNudge2	= 9,
	keypadNudge3    = 10,
	keypadLogon	= 20,
	keypadChange    = 21,
	keypadMessage	= 22,
	keypadSecurity	= 23,
	};

//////////////////////////////////////////////////////////////////////////
//
// Keypad Flags
//

enum
{
	kpsHideNav  = 0x00010000,
	kpsShowRamp = 0x00020000,
	kpsShowHead = 0x00040000,
	kpsShowFoot = 0x00080000,
	kpsNormal   = 0x00000000,
	kpsLarge    = 0x00100000,
	kpsLarger   = 0x00200000,
	kpsMaximum  = 0x00F00000,
	kpsPinYin   = 0x01000000,
	kpsHangul   = 0x02000000,
	kpsLayout   = 0xF0000000,
	kpsHebrew   = 0x10000000,
	kpsFrench   = 0x20000000,
	};

//////////////////////////////////////////////////////////////////////////
//
// Style Information
//

interface IStyle
{
	// Attributes
	virtual void GetColor (UINT uStyle, UINT uCode, COLOR      & Color) = 0;
	virtual void GetMetric(UINT uStyle, UINT uCode, int        & nSize) = 0;
	virtual void GetFont  (UINT uStyle, UINT uCode, IGdiFont * & pFont) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Notification Sink
//

interface INotify
{
	// Notification
	virtual void OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Message Sink
//

interface IMsgSink
{
	// Message Sink
	virtual UINT OnMessage(UINT uMsg, UINT uParam) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Basic Control
//

interface IControl : public IMsgSink
{
	// Creation
	virtual void Create( IGdi     * pGDI,
			     INotify  * pNotify,
			     R2 const & Rect,
			     UINT       uID,
			     UINT       uFlags,
			     UINT	uStyle,
			     IStyle   * pStyle
			     ) = 0;

	// Deletion
	virtual UINT Release(void) = 0;

	// Drawing
	virtual void DrawPrep(IGdi *pGDI, IRegion *pErase) = 0;
	virtual void DrawExec(IGdi *pGDI, IRegion *pDirty) = 0;

	// Touch Mapping
	virtual void TouchMap(ITouchMap *pTouch) = 0;
	
	// Hit Testing
	virtual BOOL HitTest (IRegion *pDirty) = 0;
	virtual BOOL HitTest (P2 const &Pos)   = 0;

	// Core Attributes
	virtual void GetRect(R2 &Rect) = 0;

	// Core Operations
	virtual void Enable(BOOL fEnable) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Control with Text
//

interface IControlWithText : public IControl
{
	// Text Operations
	virtual void SetText(PCUTF pText) = 0;
	virtual void GetText(PUTF  pText) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Button Control
//

interface IButton : public IControlWithText
{
	// State Operations
	virtual void SetState(UINT uState) = 0;
	virtual UINT GetState(void)        = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Static Text
//

interface IStaticText: public IControlWithText
{
	// No Additional Members
	};

//////////////////////////////////////////////////////////////////////////
//
// Scroll Bar
//

interface IScrollBar : public IControl
{
	// Scroll Operations
	virtual void SetValue(int nValue)         = 0;
	virtual void SetRange(int nMin, int nMax) = 0;
	virtual int  GetValue(void)               = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Progress Bar
//

interface IProgressBar : public IScrollBar
{
	// No Additional Members
	};

//////////////////////////////////////////////////////////////////////////
//
// List Box
//

interface IListBox : public IControl
{
	// List Box Operations
	virtual UINT  AddString(PCTXT pText, DWORD dwParam) = 0;
	virtual UINT  AddString(PCUTF pText, DWORD dwParam) = 0;
	virtual UINT  GetSelect(void)			    = 0;
	virtual DWORD GetSelectData(void)		    = 0;
	virtual UINT  GetText(PTXT pText, UINT uItem)	    = 0;
	virtual UINT  GetText(PUTF pText, UINT uItem)	    = 0;
	virtual void  SetSelect(UINT uSelect)		    = 0;
	virtual void  SelectByParam(DWORD dwParam)	    = 0;
	virtual void  SortData(void)			    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Drop Down List
//

interface IDropDownList : public IListBox
{
	// Drop Down List
	virtual void ShowDropDown(void) = 0;
	virtual void HideDropDown(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Key Pad
//

interface IKeypad : public IControl
{
	// Key Pad Operations
	virtual void GetPadSize(S2       &Size ) = 0;
	virtual void SetPadHost(IMsgSink *pHost) = 0;
	virtual void SetPadHead(PCUTF     pHead) = 0;
	virtual void SetPadFoot(PCUTF     pFoot) = 0;
	virtual void SetPadText(PCUTF     pText) = 0;
	virtual void SetPadCrsr(UINT      uPos ) = 0;
	virtual void SetPadDefs(void           ) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Edit Control
//

interface IEditControl : public IControlWithText
{
	// No Additional Members
	};

//////////////////////////////////////////////////////////////////////////
//
// Standard Control Creation
//

extern IButton       * Create_PushButton(void);

extern IButton       * Create_IconButton(void);

extern IButton       * Create_RadioButton(void);

extern IButton       * Create_CheckBox(void);

extern IStaticText   * Create_StaticText(void);

extern IScrollBar    * Create_ScrollBar(void);

extern IProgressBar  * Create_ProgressBar(void);

extern IListBox      * Create_ListBox(void);

extern IDropDownList * Create_DropDownList(void);

extern IKeypad       * Create_Keypad(IGdi *pGDI, UINT uType);

extern IEditControl  * Create_EditControl(void);

// End of File

#endif
