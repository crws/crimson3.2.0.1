
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_WebFileData_HPP

#define	INCLUDE_WebFileData_HPP

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

struct DLLAPI CWebFileData
{
	PCTXT  m_pType;
	PCTXT  m_pName;
	PCBYTE m_pData;
	UINT   m_uSize;
	};

//////////////////////////////////////////////////////////////////////////
//
// File Data Registration
//

extern void DLLAPI AddWebFile(CWebFileData const *pFile);

// End of File

#endif
