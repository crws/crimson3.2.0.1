
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_WebConsole_HPP

#define	INCLUDE_WebConsole_HPP

//////////////////////////////////////////////////////////////////////////
//
// Aeon Compatability
//

#if !defined(AEON_ENVIRONMENT)

#define METHOD 

#define StdSetRef(x)

typedef unsigned long ULONG;

#endif

//////////////////////////////////////////////////////////////////////////
//
// Web Debug Console
//

class CWebConsole : public IDiagConsole
{
	public:
		// Constructor
		CWebConsole(void);

		// Destructor
		~CWebConsole(void);

		// Operations
		void Enable(BOOL fEnable);
		void Read(CString &Text);
		void Exec(CString const &Cmd);
	
		// IUnknown
		#if defined(AEON_ENVIRONMENT)
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);
		#endif

		// IDiagConsole
		void METHOD Write(PCTXT pText);

	protected:
		// Data Members
		ULONG          m_uRefs;
		IDiagManager * m_pDiag;
		UINT           m_uCon;
		BOOL           m_fEnable;
		IMutex       * m_pMutex;
		CString	       m_Color;
		CString        m_Pend;
		CStringArray   m_List;
	};

// End of File

#endif
