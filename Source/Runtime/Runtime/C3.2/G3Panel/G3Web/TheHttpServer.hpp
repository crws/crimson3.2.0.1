
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TheHttpServer_HPP

#define	INCLUDE_TheHttpServer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CEnhancedWebServer;

class CWebFileLibrary;

class CTheHttpServerSession;

//////////////////////////////////////////////////////////////////////////
//
// Web Request Context
//

struct CWebReqContext
{
	CTheHttpServerSession * pSess;
	CHttpServerConnection * pCon;
	CHttpServerRequest    * pReq;
	};

//////////////////////////////////////////////////////////////////////////
//
// Web Server Object
//

class CTheHttpServer : public CHttpServer
{
	public:
		// Constructor
		CTheHttpServer(CEnhancedWebServer *pServer, CHttpServerManager *pManager, CHttpServerOptions const &Opts);

		// Destructor
		~CTheHttpServer(void);

	protected:
		// Data Members
		CEnhancedWebServer * m_pServer;
		time_t		     m_timeBoot;
		time_t		     m_timeComp;
		UINT		     m_nBuff;

		// Overridables
		CString FindPass   (CString User);
		CString FindReal   (CString User);
		BOOL    SkipAuth   (CHttpServerRequest *  pReq);
		void    ServePage  (CHttpServerSession *  pSess, CConnectionInfo &Info, CHttpServerRequest *pReq);
		void    MakeSession(CHttpServerSession * &pSess);
		void    KillSession(CHttpServerSession *  pSess);

		// Implementation
		CString GetPath(CHttpServerRequest *pReq);
		BOOL    StripSession(CHttpServerRequest *pReq, CString &Text);
		BOOL    ServeCustom(CWebReqContext const &Ctx, CString Name);
		BOOL    ServeFile(CWebReqContext const &Ctx, CString Name, BOOL fExpire);
	};

// End of File

#endif
