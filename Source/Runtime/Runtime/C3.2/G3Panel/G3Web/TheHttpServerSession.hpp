
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TheHttpServerSession_HPP

#define	INCLUDE_TheHttpServerSession_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "WebConsole.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Web Server Session
//

class CTheHttpServerSession : public CHttpServerSession
{
	public:
		// Constructor
		CTheHttpServerSession(CHttpServer *pServer, UINT cwRem, UINT uBits, UINT &nBuff);

		// Destructor
		~CTheHttpServerSession(void);

		// Operations
		BOOL AllocBuffers(void);
		void Throttle(UINT uLimit);

		// Diagnostics
		UINT GetDiagColCount(void);
		void GetDiagCols(IDiagOutput *pOut);
		void GetDiagInfo(IDiagOutput *pOut);

		// Data Members
		UINT        m_cwRem;
		UINT        m_uBits;
		UINT      & m_nBuff;
		PBYTE       m_pBuff1;
		PBYTE       m_pBuff2;
		PBYTE       m_pWork;
		DWORD       m_dwLast;
		CWebConsole m_Console;

	protected:
		// Overridables
		void FreeData(void);
	};

// End of File

#endif
