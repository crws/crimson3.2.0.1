
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_WebRle_HPP

#define	INCLUDE_WebRle_HPP

//////////////////////////////////////////////////////////////////////////
//
// Red Lion RLE Compressor
//

template <typename TData> class CWebRle
{
	public:
		// Operations
		static BOOL Compress(PBYTE &pComp, TData const *pData, UINT uData);

		// Implementation
		static STRONG_INLINE void PutByte(PBYTE &pComp, BYTE bData);
		static STRONG_INLINE void PutData(PBYTE &pComp, TData Data);
		static STRONG_INLINE void PutRun (PBYTE &pComp, UINT uSize, TData Data);
		static STRONG_INLINE void MemCpy (PBYTE &pComp, TData const *pData, UINT n);
		static STRONG_INLINE void PutMix (PBYTE &pComp, UINT uSize, TData const *pData);
	};

/////////////////////////////////////////////////////////////////////////
//
// Red Lion RLE Compressor
//

// Helper Macros

#undef	TP1

#undef	TP2

#define	TP1 template <typename TData>

#define	TP2 CWebRle <TData>

// RLE Constants

static UINT const minMix = 3;

static UINT const maxMix = 254;

static UINT const minRun = 3;

static UINT const maxRun = 254;

// Implementation

TP1 STRONG_INLINE void TP2::PutByte(PBYTE &pComp, BYTE bData)
{
	*pComp++ = bData;
	}

TP1 STRONG_INLINE void TP2::PutData(PBYTE &pComp, TData Data)
{
	if( sizeof(Data) == 1 ) {

		PutByte(pComp, Data);
		}

	if( sizeof(Data) == 2 ) {

		PutByte(pComp, LOBYTE(Data));
		PutByte(pComp, HIBYTE(Data));
		}

	if( sizeof(Data) == 4 ) {

		PutByte(pComp, LOBYTE(LOWORD(Data)));
		PutByte(pComp, HIBYTE(LOWORD(Data)));
		PutByte(pComp, LOBYTE(HIWORD(Data)));
//		PutByte(pComp, HIBYTE(HIWORD(Data)));
		}
	}

TP1 STRONG_INLINE void TP2::PutRun(PBYTE &pComp, UINT uSize, TData Data)
{
	PutByte(pComp, BYTE(uSize));

	PutData(pComp, Data);
	}

TP1 STRONG_INLINE void TP2::MemCpy(PBYTE &pComp, TData const *pData, UINT n)
{
	while( n-- ) PutData(pComp, *pData++);
	}

TP1 STRONG_INLINE void TP2::PutMix(PBYTE &pComp, UINT uSize, TData const *pData)
{
	if( uSize < minMix ) {

		for( ; uSize; uSize-- ) {

			PutRun(pComp, 1, *pData++);
			}
		}
	else {
		UINT uChop = maxMix;

		for(;;) {

			if( uSize <= uChop ) {

				PutByte(pComp, 0);

				PutByte(pComp, BYTE(uSize));

				MemCpy(pComp, pData, uSize);

				return;
				}

			if( uSize - uChop < minMix ) {

				uChop -= 4;
				}

			PutByte(pComp, 0);

			PutByte(pComp, BYTE(uChop));

			MemCpy(pComp, pData, uChop);

			pData += uChop;

			uSize -= uChop;
			}
		}
}

// Operations

TP1 BOOL TP2::Compress(PBYTE &pComp, TData const *pData, UINT uData)
{
	UINT          uSame = 0;

	TData	      tData = *pData;

	TData	      tTest = 0;

	TData const * pPend = pData;

	UINT          uPend = 0;

	for( ; uData; uData-- ) {

		if( (tTest = *pData++) == tData ) {

			if( ++uSame >= maxRun ) {
			
				if( uPend ) {

					PutMix(pComp, uPend, pPend);

					uPend = 0;
					}

				PutRun(pComp, uSame, tData);

				uSame = 0;

				pPend = pData;
				}
			}
		else {
			if( uSame >= minRun ) {

				if( uPend ) {

					PutMix(pComp, uPend, pPend);

					uPend = 0;
					}

				PutRun(pComp, uSame, tData);

				tData = tTest;

				uSame = 1;

				pPend = pData - 1;
				}
			else {
				tData = tTest;

				uPend = uPend + uSame;

				uSame = 1;
				}
			}
		}

	if( uSame >= minRun ) {

		if( uPend ) {

			PutMix(pComp, uPend, pPend);
			}

		PutRun(pComp, uSame, tData);
		}
	else {
		uPend = uPend + uSame;

		PutMix(pComp, uPend, pPend);
		}

	PutByte(pComp, 0);

	PutByte(pComp, 0);

	return TRUE;
	}

// End of File

#endif
