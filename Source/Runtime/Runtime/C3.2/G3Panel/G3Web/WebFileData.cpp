
#include "Intern.hpp"

#include "WebFileData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "WebFileLibrary.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Style Sheets
//

static BYTE const bootstrap_theme_min_css[] = {
	#include "bootstrap-theme.min.css.dat"
	0
	};

static BYTE const bootstrap_min_css[] = {
	#include "bootstrap.min.css.dat"
	0
	};

static BYTE const ie10_viewport_bug_workaround_css[] = {
	#include "ie10-viewport-bug-workaround.css.dat"
	0
	};

static BYTE const logon_css[] = {
	#include "logon.css.dat"
	0
	};

static BYTE const theme_css[] = {
	#include "theme.css.dat"
	0
	};

//////////////////////////////////////////////////////////////////////////
//
// JavaScript
//

static BYTE const bootstrap_min_js[] = {
	#include "bootstrap.min.js.dat"
	0
	};

static BYTE const dataview_js[] = {
	#include "dataview.js.dat"
	0
	};

static BYTE const html5shiv_min_js[] = {
	#include "html5shiv.min.js.dat"
	0
	};

static BYTE const ie10_viewport_bug_workaround_js[] = {
	#include "ie10-viewport-bug-workaround.js.dat"
	0
	};

static BYTE const jquery_min_js[] = {
	#include "jquery.min.js.dat"
	0
	};

static BYTE const logoff_js[] = {
	#include "logoff.js.dat"
	0
	};

static BYTE const logon_js[] = {
	#include "logon.js.dat"
	0
	};

static BYTE const remote_js[] = {
	#include "remote.js.dat"
	0
	};

static BYTE const respond_min_js[] = {
	#include "respond.min.js.dat"
	0
	};

static BYTE const session_js[] = {
	#include "session.js.dat"
	0
	};

static BYTE const syscmd_js[] = {
	#include "syscmd.js.dat"
	0
	};

static BYTE const sysdebug_js[] = {
	#include "sysdebug.js.dat"
	0
	};

static BYTE const syspcap_js[] = {
	#include "syspcap.js.dat"
	0
	};

//////////////////////////////////////////////////////////////////////////
//
// Fonts
//

static BYTE const glyphicons_eot[] = {
	#include "glyphicons-halflings-regular.eot.dat"
	0
	};

static BYTE const glyphicons_svg[] = {
	#include "glyphicons-halflings-regular.svg.dat"
	0
	};

static BYTE const glyphicons_ttf[] = {
	#include "glyphicons-halflings-regular.ttf.dat"
	0
	};

static BYTE const glyphicons_woff[] = {
	#include "glyphicons-halflings-regular.woff.dat"
	0
	};

static BYTE const glyphicons_woff2[] = {
	#include "glyphicons-halflings-regular.woff2.dat"
	0
	};

//////////////////////////////////////////////////////////////////////////
//
// HTML
//

static BYTE const data_htm[] = {
	#include "data.htm.dat"
	0
	};

static BYTE const dataview_htm[] = {
	#include "dataview.htm.dat"
	0
	};

static BYTE const default_htm[] = {
	#include "default.htm.dat"
	0
	};

static BYTE const logoff_htm[] = {
	#include "logoff.htm.dat"
	0
	};

static BYTE const logon_htm[] = {
	#include "logon.htm.dat"
	0
	};

static BYTE const logs_htm[] = {
	#include "logs.htm.dat"
	0
	};

static BYTE const remote_htm[] = {
	#include "remote.htm.dat"
	0
	};

static BYTE const rvzoom_htm[] = {
	#include "rvzoom.htm.dat"
	0
	};

static BYTE const rvcenter_htm[] = {
	#include "rvcenter.htm.dat"
	0
	};

static BYTE const rvscale_htm[] = {
	#include "rvscale.htm.dat"
	0
	};

static BYTE const stdhead_htm[] = {
	#include "stdhead.htm.dat"
	0
	};

static BYTE const stdnavbar_htm[] = {
	#include "stdnavbar.htm.dat"
	0
	};

static BYTE const system_htm[] = {
	#include "system.htm.dat"
	0
	};

static BYTE const syscmd_htm[] = {
	#include "syscmd.htm.dat"
	0
	};

static BYTE const sysdebug_htm[] = {
	#include "sysdebug.htm.dat"
	0
	};

static BYTE const syspcap_htm[] = {
	#include "syspcap.htm.dat"
	0
	};

static BYTE const writeokay_htm[] = {
	#include "writeokay.htm.dat"
	0
	};

static BYTE const writefail_htm[] = {
	#include "writefail.htm.dat"
	0
	};

//////////////////////////////////////////////////////////////////////////
//
// Icons
//

static BYTE const favicon_ico[] = {
	#include "favicon.ico.dat"
	0
	};

//////////////////////////////////////////////////////////////////////////
//
// File Helper
//

#define Entry(t, f, p) { t, f, p, sizeof(p) }

//////////////////////////////////////////////////////////////////////////
//
// File Table
//

CWebFileData CWebFileLibrary::m_Files[] = {

	// Style Sheets

	Entry( "css",	"/css/bootstrap-theme.min.css",			bootstrap_theme_min_css			),
	Entry( "css",	"/css/bootstrap.min.css",			bootstrap_min_css			),
	Entry( "css",	"/css/ie10-viewport-bug-workaround.css",	ie10_viewport_bug_workaround_css	),
	Entry( "css",	"/css/logon.css",				logon_css				),
	Entry( "css",	"/css/theme.css",				theme_css				),

	// Fonts

	Entry( "eot",	"/fonts/glyphicons-halflings-regular.eot",	glyphicons_eot				),
	Entry( "svg",	"/fonts/glyphicons-halflings-regular.svg",	glyphicons_svg				),
	Entry( "ttf",	"/fonts/glyphicons-halflings-regular.ttf",	glyphicons_ttf				),
	Entry( "woff",	"/fonts/glyphicons-halflings-regular.woff",	glyphicons_woff				),
	Entry( "woff2",	"/fonts/glyphicons-halflings-regular.woff2",	glyphicons_woff2			),

	// HTML

	Entry( "htm",	"/html/data.htm",				data_htm				),
	Entry( "htm",	"/html/dataview.htm",				dataview_htm				),
	Entry( "htm",	"/html/default.htm",				default_htm				),
	Entry( "htm",	"/html/logoff.htm",				logoff_htm				),
	Entry( "htm",	"/html/logon.htm",				logon_htm				),
	Entry( "htm",	"/html/logs.htm",				logs_htm				),
	Entry( "htm",	"/html/remote.htm",				remote_htm				),
	Entry( "htm",	"/html/rvzoom.htm",				rvzoom_htm				),
	Entry( "htm",	"/html/rvcenter.htm",				rvcenter_htm				),
	Entry( "htm",	"/html/rvscale.htm",				rvscale_htm				),
	Entry( "htm",	"/html/stdhead.htm",				stdhead_htm				),
	Entry( "htm",	"/html/stdnavbar.htm",				stdnavbar_htm				),
	Entry( "htm",	"/html/system.htm",				system_htm				),
	Entry( "htm",	"/html/syscmd.htm",				syscmd_htm				),
	Entry( "htm",	"/html/sysdebug.htm",				sysdebug_htm				),
	Entry( "htm",	"/html/syspcap.htm",				syspcap_htm				),
	Entry( "htm",	"/html/writeokay.htm",				writeokay_htm				),
	Entry( "htm",	"/html/writefail.htm",				writefail_htm				),

	// JavaScript

	Entry( "js",	"/js/bootstrap.min.js",				bootstrap_min_js			),
	Entry( "js",	"/js/dataview.js",				dataview_js				),
	Entry( "js",	"/js/html5shiv.min.js",				html5shiv_min_js			),
	Entry( "js",	"/js/ie10-viewport-bug-workaround.js",		ie10_viewport_bug_workaround_js		),
	Entry( "js",	"/js/jquery.min.js",				jquery_min_js				),
	Entry( "js",	"/js/logoff.js",				logoff_js				),
	Entry( "js",	"/js/logon.js",					logon_js				),
	Entry( "js",	"/js/remote.js",				remote_js				),
	Entry( "js",	"/js/respond.min.js",				respond_min_js				),
	Entry( "js",	"/js/session.js",				session_js				),
	Entry( "js",	"/js/syscmd.js",				syscmd_js				),
	Entry( "js",	"/js/sysdebug.js",				sysdebug_js				),
	Entry( "js",	"/js/syspcap.js",				syspcap_js				),

	// Icons

	Entry( "ico",   "/icons/favicon.ico",				favicon_ico				),

	// Terminator

	{ NULL, NULL, NULL, 0 }

	};

// End of File
