
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Enhanced Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Static Data
//

var xhr;
var cmd;
var enable;
var running;

//////////////////////////////////////////////////////////////////////////
//
// System Command Support
//

function pageMain() {

	cmd = getParam("cmd");

	if (getParam("rt") == 1) {

		$("#real-time-div").css("display", "");

		$("#real-time-box").change(function () { onClick($(this).prop("checked")); });

		enable  = 0;

		running = 0;

		xhr = new XMLHttpRequest();
	}
}

function onClick(check) {

	if (check) {

		if (!running) {

			enable = 1;

			updateCmd();
		}
	}
	else {

		enable = 0;
	}
}

function updateCmd() {

	var url = "/ajax/syscmd-update.htm?cmd=" + cmd + cacheBreaker();

	xhr.onload = dataLoaded;

	xhr.onerror = dataFailed;

	xhr.open("GET", url, true);

	xhr.send(null);

	running = 1;
}

function dataLoaded() {

	if (xhr.status == 200) {

		$("#cmd-data").html(xhr.responseText);

		if (enable) {

			setTimeout(updateCmd, 500);
		}
		else
			running = 0;

		return;
	}

	if (check401(xhr.status)) {

		return;
	}

	dataFailed();
}

function dataFailed() {

	$("#cmd-data").html("ERROR");

	if (enable) {

		setTimeout(updateCmd, 50);
	}
	else
		running = 0;
}

// End of File
