
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Static Data
//

var xhr;

var edit = -1;
var save = -1;
var page = -1;
var mode = -1;
var idle =  0;

var timeout;

//////////////////////////////////////////////////////////////////////////
//
// Remote Data Support
//

function pageMain() {

	page = getParam("page");

	xhr = new XMLHttpRequest();

	$("button[id^=button]").each(function (n) { this.onclick = function () { editTag(n); }; });

	$("#myEdit").on("shown.bs.modal", function (e) { $("#edit-okay").focus(); $(".edit-focus").focus().select(); });

	$("#edit-okay").on("click", function (e) { submitData(); });
	
	$("#edit-cancel").on("click", function (e) { hideEdit(); });

	updateTags();
}

function updateTags() {

	var url = "/ajax/dataview-read.htm?page=" + page + cacheBreaker();

	xhr.onload = dataLoaded;

	xhr.onerror = dataFailed;

	xhr.open("GET", url, true);

	xhr.send(null);
}

function dataLoaded() {

	if (xhr.status == 200 || xhr.status == 222) {

		var list = xhr.responseText.split("\n");

		for (var n = 0; n < list.length; n++) {

			var fields = list[n].split("\t");

			if (edit == n) {

				$("#edit-saved").val(fields[0]);
			}

			if (fields.length == 3) {

				$("#data" + n).html(fields[0]).css("color", fields[1]).css("background-color", fields[2]);
			}
			else {

				$("#data" + n).html(fields[0]);
			}
		}

		if (xhr.status == 200) {

			setTimeout(updateTags, 10);
		}
		else
			idle = 1;

		return;
	}

	if (check401(xhr.status)) {

		return;
	}

	dataFailed();
}

function dataFailed() {

	$("td[id^=data]").html("ERROR");

	setTimeout(updateTags, 50);
}

function useRadio(list) {

	if (list.length > 4) {

		var total = 0;

		for (var n = 0; n < list.length; n++) {

			if ((total += list[n].length) > 32) {

				return false;
			}
		}
	}

	return true;
}

function editTag(n) {

	edit = n;

	save = n;

	var data = $("#data" + n).html();

	var name = $("#name" + n).html();

	var form = $("#form" + n).html();

	var html = "";

	$("#edit-block").html("");

	alertHide();

	if (!form.length) {

		html += "<input id='edit-input' type='text' class='form-control edit-focus' autocomplete='off'>";

		$("#edit-block").html(html);

		$("#edit-input").on("keypress", function (e) { if (e.which == 13) submitData(); });

		$("#edit-input").val(data);

		mode = 1;
	}
	else {

		var list = form.split(",");

		if (useRadio(list)) {

			html += "<div class='btn-group' data-toggle='buttons'>";

			for (var n = 0; n < list.length; n++) {

				var s = list[n];

				if (data == s) {

					html += "<label class='btn btn-default active'>";

					html += "<input name='edit-radio' type='radio' class='edit-focus' checked value='";
				}
				else {

					html += "<label class='btn btn-default'>";

					html += "<input name='edit-radio' type='radio' value='";
				}

				html += s + "'>" + s + "</label>";
			}

			html += "</div>";

			$("#edit-block").html(html);

			mode = 2;
		}
		else {

			html += "<select id='edit-input' class='form-control edit-focus'>";

			for (var n = 0; n < list.length; n++) {

				var s = list[n];

				if (data == s) {

					html += "<option selected value='";
				}
				else {

					html += "<option value='";
				}

				html += s + "'>" + s + "</option>";
			}

			html += "</select>";

			$("#edit-block").html(html);

			$("#edit-input").on("keypress", function (e) { if (e.which == 13) submitData(); });

			mode = 3;
		}
	}

	$("#edit-saved").val(data);

	$("#edit-title").html(name);

	$("#myEdit").modal("show").trigger("shown");
}

function hideEdit() {

	$("#myEdit").modal("hide");

	edit = -1;
}

function getEditValue() {

	if (mode == 1 || mode == 3) {

		return $("#edit-input").val();
	}

	if (mode == 2) {

		return $('input[name=edit-radio]:checked').val();
	}

	return "";
}

function submitData() {

	var val = getEditValue();

	var xhr = new XMLHttpRequest();

	var url = "/ajax/dataview-write.htm?page=" + page + "&tag=" + edit + "&data=\"" + val + "\"" + cacheBreaker();

	xhr.open("GET", url, true);

	xhr.onload = function () {

		if (xhr.status == 200) {

			if (xhr.response.length) {

				alertShow("alert-success", "Success", xhr.response);

				if (idle) {

					updateTags();
				}
			}
		}
		else {

			alertShow("alert-danger", "Error", xhr.response);
		}
	};

	xhr.onerror = function () {

		alertShow("alert-danger", "Error", "Failed to send write to server.");
	};

	xhr.send(null);

	hideEdit();
}

function alertHide() {

	clearTimeout(timeout);

	$("#alertdiv").remove();
}

function alertShow(coloring, tagword, message) {

	$("#alert-placeholder").html("<div id='alertdiv' class='alert " + coloring + " alert-dismissable'>" +
								  "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>" +
								  "<span><strong>" + tagword + "!</strong> " + message + "</span></div>"
								  );

	timeout = setTimeout(alertHide, 2500);
}

// End of File
