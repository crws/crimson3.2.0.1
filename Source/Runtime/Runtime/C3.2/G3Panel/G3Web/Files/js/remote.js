
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Static Data
//

// Display Metrics

var info;

// Event Queue

var queue = new Queue();

// Zoom Status

var nMode = 0;
var xZoom = 1.0;
var yZoom = 1.0;

// 8-bit Palette

var palette;

// Other Data

var cvs;
var ctx;
var xhr;
var timer;
var ping;
var nored;
var valid;

//////////////////////////////////////////////////////////////////////////
//
// Remote View Support
//

function pageMain(mode) {

	// Called once the main document is ready. Get ourselves a drawing context,
	// hook various mouse and keyboard related events and then kick-off a timer
	// to load the info about the page layout.

	nMode = mode;

	nored = false;

	valid = false;

	cvs = document.getElementById("the-canvas");

	ctx = cvs.getContext("2d");

	xhr = new XMLHttpRequest();

	catchInput();

	window.onbeforeunload = function () { nored = true; };

	myTimer(loadInfo, 10);
}

function myTimer(f, t) {

	// Reset a timer that we use to trigger various updates.

	clearTimeout(timer);

	timer = setTimeout(f, t);
}

function loadPing() {

	// Load the periodic timer we use to post null key events every now and
	// then. The idea is that we want to maintain a distinct TCP/IP connection
	// that we're going to use for key and mouse events. We don't want the
	// other end closing it on us, so we fake events to keep it active.

	if (!ping) {

		ping = setInterval(function () { postKey(0); }, 10000);
	}
}

function clearPing() {

	// Clear the periodic timer. We do this only when an error occurs
	// as we then want to focus on reestablishing the connection without
	// the null key events firing at the same time.

	if (ping) {

		clearInterval(ping);

		ping = 0;
	}
}

function catchInput() {

	// Hook the event handlers used to capture the mouse and the keyboard. The
	// mouse click is compared against the display image and list of keys that
	// we received from the server. Several events are used to make sure we get
	// all the keyboard entry that we're interested in.

	cvs.addEventListener("mousedown", function (e) {

		e.preventDefault();

		var xp = (e.pageX - cvs.offsetLeft) / xZoom;

		var yp = (e.pageY - cvs.offsetTop) / yZoom;

		for (var n = 0; n < info.keys.length; n++) {

			if (xp >= info.keys[n].x1 && xp < info.keys[n].x2) {

				if (yp >= info.keys[n].y1 && yp < info.keys[n].y2) {

					postKey(info.keys[n].vk);

					break;
				}
			}
		}

		if (xp >= info.xpImage && xp < info.xpImage + info.scale * info.cxImage) {

			if (yp >= info.ypImage && yp < info.ypImage + info.scale * info.cyImage) {

				postMouse((xp - info.xpImage) / info.scale, (yp - info.ypImage) / info.scale);
			}
		}

		cvs.focus();
	});

	cvs.addEventListener("click", function (e) {

		e.preventDefault();
	});

	cvs.addEventListener("dblclick", function (e) {

		e.preventDefault();
	});

	document.addEventListener("keydown", function (e) {

		0
		|| checkKey(e, "Backspace", 0x7F)
		|| checkKey(e, "Esape", 0x1B)
		|| checkKey(e, "Esc", 0x1B)
		|| checkKey(e, "ArrowLeft", 0x08)
		|| checkKey(e, "ArrowRight", 0x09)
		|| checkKey(e, "ArrowUp", 0x0B)
		|| checkKey(e, "ArrowDown", 0x0A)
		|| checkKey(e, "Left", 0x08)
		|| checkKey(e, "Right", 0x09)
		|| checkKey(e, "Up", 0x0B)
		|| checkKey(e, "Down", 0x0A)
		;
	});

	document.addEventListener("keypress", function (e) {

		if (!e.altKey && !e.ctrlKey) {

			if ("charCode" in e) {

				if ((e.charCode >= 32 && e.charCode <= 126) || e.charCode == 13) {

					postKey(e.charCode);

					e.preventDefault();

					return;
				}
			}

			if ("keyCode" in e) {

				if (e.keyCode == 13) {

					postKey(e.keyCode);

					e.preventDefault();

					return;
				}
			}
		}
	});
}

function checkKey(e, name, c) {

	// Helper to compare a key event against specified criteria.

	if ("key" in e) {

		if (e.key == name) {

			postKey(c);

			e.preventDefault();

			return true;
		}
	}

	return false;
}

function postEvent(e) {

	// Post an event to the server, or queue it if we're busy sending. We
	// keep the current event in the queue and use that as an indicator that
	// a send is currently in process. It's remove on the completion events.

	if (queue.isEmpty()) {

		queue.enqueue(e);

		sendEvent();

		return;
	}

	queue.enqueue(e);
}

function sendEvent() {

	// Send the next event in the queue, and set the completion handlers
	// to remove it and then send anything else that we have left. This is
	// used to avoid having lots of TCP/IP connections opened by our
	// trying to send keys more quickly than the link can handle. It really
	// only comes into play on modem or other high-latency links.

	if (!queue.isEmpty()) {

		console.log("send " + queue.peek());

		var xhr = new XMLHttpRequest();

		xhr.open("GET", queue.peek(), true);

		xhr.onload = function () { queue.dequeue(); sendEvent(); };

		xhr.onerror = function () { queue.dequeue(); sendEvent(); };

		xhr.send(null);
	}
}

function postKey(c) {

	// Post a key event, reseting the ping timer.

	if (!c || valid) {

		if (c) {

			clearPing();

			loadPing();
		}

		postEvent("/ajax/remote-event.htm?t=0&c=" + c + "&" + cacheBreaker());
	}
}

function postMouse(x, y) {

	// Post a key event, reseting the ping timer.

	if (valid) {

		clearPing();

		loadPing();

		postEvent("/ajax/remote-event.htm?t=1&x=" + x + "&y=" + y + "&" + cacheBreaker());
	}
}

function stdError(status, func) {

	// Standard error handler for certain eventualities. If we
	// get a 401, we must have lost authorization in form mode
	// so we need to redirect to the logon page. If we get a
	// 503 and we have a retry handler, the session limit has
	// been reached, so call the code to inform the user.

	if (check401(xhr.status)) {

		return;
	}

	if (status == 503) {

		if (func) {

			sessionLimit(func);

			return true;
		}
	}

	return false;
}

function loadInfo() {

	// Load the layout info from the server and extract it from the resulting
	// JSON stream. Configure the canvas, and then load the frame image. If we
	// an error that isn't handled by stdError, keep trying after a 500ms delay.

	xhr.open("GET", getParam("info") + "?" + cacheBreaker(), true);

	xhr.onload = function () {

		if (xhr.status == 200) {

			info = JSON.parse(xhr.response);

			if (nMode == 2 || nMode == 3) {

				info.xpImage = 0;
				info.ypImage = 0;
				info.cxFrame = info.cxImage * info.scale;
				info.cyFrame = info.cyImage * info.scale;

				info.keys = [];
			}

			setZoom();

			loadFrame();
		}
		else {

			if (!stdError(xhr.status, null)) {

				myTimer(loadInfo, 500);
			}
		}
	};

	xhr.onerror = function () {

		myTimer(loadInfo, 500);
	};

	xhr.send(null);
}

function setZoom() {

	// If we're in zoom mode, figure out the scale factor required for the
	// image to fill the entirity of the canvas. Apply that scale, and then
	// set a listener to allow us to redraw on a resize event.

	if (nMode == 0) {

		cvs.width = info.cxFrame;
		
		cvs.height = info.cyFrame;
	}

	if (nMode == 1 || nMode == 3) {

		// TODO -- Find a better way of getting the real window size. This is not perfect!

		var w = window.innerWidth  || document.documentElement.clientWidth  || document.body.clientWidth;
		
		var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

		w -= 20; // TODO -- Why 20?
		
		h -= 20; // TODO -- Why 20?

		cvs.width  = w;
		
		cvs.height = h;

		xZoom = w / info.cxFrame;
		
		yZoom = h / info.cyFrame;

		if (false) {
			
			xZoom = (xZoom < yZoom) ? xZoom : yZoom;
			
			yZoom = (xZoom < yZoom) ? xZoom : yZoom;
		}

		ctx.setTransform(1, 0, 0, 1, 0, 0);

		ctx.scale(xZoom, yZoom);

		window.addEventListener("resize", reSize);
	}

	if (nMode == 2) {

		cvs.width = info.cxFrame;
		
		cvs.height = info.cyFrame;

		var w = window.innerWidth  || document.documentElement.clientWidth  || document.body.clientWidth;
		
		var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

		cvs.style.position = "absolute";
		
		cvs.style.left = "" + ((w - cvs.width ) / 2) + "px";
		
		cvs.style.top  = "" + ((h - cvs.height) / 2) + "px";

		window.addEventListener("resize", reSize);
	}
}

function reSize(e) {

	// If we're in zoom mode and resize has occured, cancel the current
	// transaction, clear the output canvas and restart at the load frame
	// stage, ensuring that the scale or position get recalculated.

	if (nMode >= 1 ) {

		xhr.abort();

		setZoom();

		ctx.beginPath();

		ctx.rect(0, 0, cvs.width, cvs.height);

		ctx.fillStyle = "white";

		ctx.fill();

		myTimer(loadFrame, 100);
	}
}

function sessionLimit(func) {

	// This function is called if we get a 503 error at a couple of key
	// points, which indicates that the server can't allocate the memory
	// required to track display changes. Display an error message and
	// set a timer to retry via the appropriate function after a delay.

	ctx.font = "32px sans-serif";

	var text = "Session Limit Exceeded";

	var cx = ctx.measureText(text).width;

	var cy = 32;

	var xp = info.xpImage + (info.cxImage - cx) / 2;

	var yp = info.ypImage + (info.cyImage - cy) / 2;

	ctx.beginPath();

	ctx.rect(info.xpImage, info.ypImage, info.cxImage, info.cyImage);

	ctx.fillStyle = "black";

	ctx.fill();

	ctx.fillStyle = "white";

	ctx.fillText(text, xp, yp);

	clearPing();

	myTimer(func, 5000);
}

function loadFrame() {

	// Load the frame image and display it on the canvas. Optionally
	// show a debugging aid for the key locations. Move on to either
	// load the palette in 8-bit mode, or to load the display image
	// in 16-bit mode. If we get an error, retry after a short delay.

	if (nMode < 2) {

		var frame = new Image;

		frame.onload = function () {

			ctx.drawImage(frame, 0, 0);

			if (false) {

				for (var n = 0; n < info.keys.length; n++) {

					ctx.beginPath();

					var cx = info.keys[n].x2 - info.keys[n].x1;

					var cy = info.keys[n].y2 - info.keys[n].y1;

					ctx.rect(info.keys[n].x1, info.keys[n].y1, cx, cy);

					ctx.lineWidth = "2";

					ctx.strokeStyle = "white";

					ctx.stroke();
				}
			}

			if (info.bits == 8) {

				loadPalette();
			}
			else {

				loadImage();
			}
		};

		frame.onerror = function () {

			myTimer(loadFrame, 500);
		};

		frame.src = info.file;
	}
	else {

		if (info.bits == 8) {

			loadPalette();
		}
		else {

			loadImage();
		}
	}
}

function loadPalette() {

	// Load the palette that will be used to expand 8-bit RLE
	// streams. Move on to the load the display image. If we
	// get an error, retry after a short delay unless we need
	// have handled it via stdError.

	xhr.open("GET", "/ajax/remote-palette.blob", true);

	xhr.responseType = "arraybuffer";

	xhr.onload = function () {

		if (xhr.status == 200) {

			var blob = xhr.response;

			palette = new Uint8Array(xhr.response);

			loadImage();
		}
		else {

			if (!stdError(xhr.status, loadPalette)) {

				myTimer(loadPalette, 500);
			}
		}
	};

	xhr.onerror = function () {

		myTimer(loadPalette, 500);
	};

	xhr.send(null);
}

function loadImage() {

	// Load the entire display image RLE stream and display it
	// on the canvas. Move on to load deltas to allow us to only
	// update the changed areas. If we get an error, show that
	// the screen is no longer live and then retry unless we have
	// handled it via stdError.

	xhr.open("GET", "/ajax/remote-display.blob?" + cacheBreaker(), true);

	xhr.responseType = "arraybuffer";

	xhr.onload = function () {

		if (xhr.status == 200) {

			var array = new Uint8Array(xhr.response);

			var rpos = 0;

			if (info.bits == 8)
				drawRle08(array, rpos, info.xpImage, info.ypImage, info.cxImage, info.cyImage);

			if (info.bits == 16)
				drawRle16(array, rpos, info.xpImage, info.ypImage, info.cxImage, info.cyImage);

			if (info.bits == 24)
				drawRle24(array, rpos, info.xpImage, info.ypImage, info.cxImage, info.cyImage);

			valid = true;

			loadDelta();

			postKey(0);
		}
		else {

			if (!stdError(xhr.status, loadImage)) {

				imageFailed();
			}
		}
	};

	xhr.onerror = imageFailed;

	xhr.send(null);
}

function imageFailed() {

	// Show a big red cross to indicate that the current data is
	// no longer valid as a result of lost communication. We do
	// this to avoid stale data being acted upon.

	if (!nored) {

		ctx.lineWidth = 16;

		ctx.strokeStyle = "red";
		
		ctx.lineCap = "round";

		ctx.beginPath();

		ctx.moveTo(info.xpImage + 20, info.ypImage + 20);

		ctx.lineTo(info.xpImage + info.cxImage * info.scale - 20, info.ypImage + info.cyImage * info.scale - 20);

		ctx.stroke();

		ctx.beginPath();

		ctx.moveTo(info.xpImage + info.cxImage * info.scale - 20, info.ypImage + 20);

		ctx.lineTo(info.xpImage + 20, info.ypImage + info.cyImage * info.scale - 20);

		ctx.stroke();

		clearPing();

		myTimer(loadImage, 500);
	}

	valid = false;
}

function loadDelta() {

	// Load the display delta stream, walking through each updated
	// rectangle, displaying it on the canvas. Kick off the process
	// again, and also kick off the ping timer. If we get an error,
	// show that the screen is no longer live and then retry, unless
	// we have handled it via stdError.

	xhr.open("GET", "/ajax/remote-delta.blob?" + cacheBreaker(), true);

	xhr.responseType = "arraybuffer";

	xhr.onload = function () {

		if (xhr.status == 200) {

			var array = new Uint8Array(xhr.response);

			var rpos = 0;

			while (true) {

				var y1 = array[rpos + 0] + (array[rpos + 1] << 8);
				var y2 = array[rpos + 2] + (array[rpos + 3] << 8);

				if (y1 || y2) {

					rpos += 4;

					var x1 = array[rpos + 0] + (array[rpos + 1] << 8);
					var x2 = array[rpos + 2] + (array[rpos + 3] << 8);

					rpos += 4;

					if (info.bits == 8)
						rpos = drawRle08(array, rpos, info.xpImage + x1, info.ypImage + y1, x2 - x1, y2 - y1);

					if (info.bits == 16)
						rpos = drawRle16(array, rpos, info.xpImage + x1, info.ypImage + y1, x2 - x1, y2 - y1);

					if (info.bits == 24)
						rpos = drawRle24(array, rpos, info.xpImage + x1, info.ypImage + y1, x2 - x1, y2 - y1);
				}
				else
					break;
			}

			time = Date.now();

			loadDelta();

			loadPing();
		}
		else {

			if (!stdError(xhr.status, loadImage)) {

				imageFailed();
			}
		}
	};

	xhr.onerror = imageFailed;

	xhr.send(null);
}

function drawRle08(array, rpos, xp, yp, cx, cy) {

	// Expand a 8-bit RLE stream and display on the canvas.

	var image = ctx.createImageData(cx, cy);

	var wpos = 0;

	for (; ; ) {

		var n = array[rpos++];

		if (n) {

			var b = 4 * array[rpos++];

			while (n--) {

				image.data[wpos++] = palette[b + 0];
				image.data[wpos++] = palette[b + 1];
				image.data[wpos++] = palette[b + 2];
				image.data[wpos++] = palette[b + 3];
			}

		} else {

			n = array[rpos++];

			if (n) {

				while (n--) {

					var b = 4 * array[rpos++];

					image.data[wpos++] = palette[b + 0];
					image.data[wpos++] = palette[b + 1];
					image.data[wpos++] = palette[b + 2];
					image.data[wpos++] = palette[b + 3];
				}

			} else {

				break;
			}
		}
	}

	drawImage(image, xp, yp, cx, cy);

	return rpos;
}

function drawRle16(array, rpos, xp, yp, cx, cy) {

	// Expand a 16-bit RLE stream and display on the canvas.

	var image = ctx.createImageData(cx, cy);

	var wpos = 0;

	for (; ; ) {

		var n = array[rpos++];

		if (n) {

			var w = array[rpos] + (array[rpos + 1] << 8);

			rpos += 2;

			var r = ((w >> 10) & 31);
			var g = ((w >> 5) & 31);
			var b = ((w >> 0) & 31);

			while (n--) {

				image.data[wpos++] = r << 3;
				image.data[wpos++] = g << 3;
				image.data[wpos++] = b << 3;
				image.data[wpos++] = 0xFF;
			}

		} else {

			n = array[rpos++];

			if (n) {

				while (n--) {

					var w = array[rpos] + (array[rpos + 1] << 8);

					rpos += 2;

					var r = ((w >> 10) & 31);
					var g = ((w >> 5) & 31);
					var b = ((w >> 0) & 31);

					image.data[wpos++] = r << 3;
					image.data[wpos++] = g << 3;
					image.data[wpos++] = b << 3;
					image.data[wpos++] = 0xFF;
				}

			} else {

				break;
			}
		}
	}

	drawImage(image, xp, yp, cx, cy);

	return rpos;
}

function drawRle24(array, rpos, xp, yp, cx, cy) {

	// Expand a 24-bit RLE stream and display on the canvas.

	var image = ctx.createImageData(cx, cy);

	var wpos = 0;

	for (; ; ) {

		var n = array[rpos++];

		if (n) {

			var b = array[rpos++];
			var g = array[rpos++];
			var r = array[rpos++];

			while (n--) {

				image.data[wpos++] = r;
				image.data[wpos++] = g;
				image.data[wpos++] = b;
				image.data[wpos++] = 0xFF;
			}

		} else {

			n = array[rpos++];

			if (n) {

				while (n--) {

					var b = array[rpos++];
					var g = array[rpos++];
					var r = array[rpos++];

					image.data[wpos++] = r;
					image.data[wpos++] = g;
					image.data[wpos++] = b;
					image.data[wpos++] = 0xFF;
				}

			} else {

				break;
			}
		}
	}

	drawImage(image, xp, yp, cx, cy);

	return rpos;
}

function drawImage(image, xp, yp, cx, cy) {

	// Draw an image on the canvas, scaling it via a secondary
	// canvas object if we're working in zoom mode or scaled
	// mode, and writing it straight to the canvas otherwise.

	if (nMode == 1 || nMode == 3 || info.scale > 1) {

		var temp = document.createElement("canvas");

		temp.width = cx;

		temp.height = cy;

		temp.getContext("2d").putImageData(image, 0, 0);

		if (info.scale > 1) {

			xp = info.scale * (xp - info.xpImage) + info.xpImage;
			yp = info.scale * (yp - info.ypImage) + info.ypImage;
			cx = info.scale * cx;
			cy = info.scale * cy;

			ctx.drawImage(temp, xp, yp, cx, cy);
		}
		else {

			ctx.drawImage(temp, xp, yp);
		}
	}
	else {

		ctx.putImageData(image, xp, yp);
	}
}

// End of File
