
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Enhanced Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Network Capture Support
//

// Data

var xhr;
var start;
var busy;

// Code

function pageMain() {

	xhr = new XMLHttpRequest();

	busy = true;

	var src = getParam("source");

	if (src) {

		$("#pcap-source").val(src);
	}

	setDownload();

	fetchStatus();

	$("#pcap-source").change(changeSource);

	$("#pcap-tcp-enable").change(doEnables);

	$("#pcap-udp-enable").change(doEnables);

	$("#pcap-icmp-enable").change(doEnables);

	$("#pcap-arp-enable").change(doEnables);

	$("#pcap-start").on("click", onClick);
}

function setDownload() {

	var src = $("#pcap-source").val();

	var url = "/ajax/syspcap-read.pcap?src=" + src + cacheBreaker();

	$("#pcap-href").attr("href", url);
}

function changeSource() {

	setDownload();

	$("#pcap-info").css("display", "hide");

	$("#pcap-source").prop("disabled", 1);

	setUrlArg("source", $("#pcap-source").val());

	busy = true;

	fetchStatus();
}

function updateUi(text) {

	var list = text.split(",");

	if (parseInt(list[0]) > 0) {

		if (true) {

			var msg;
			var rem;
			var add;

			if (parseInt(list[2]) > 0) {

				start = 0;

				msg = $("#text-stop").html();
				rem = "btn-success";
				add = "btn-danger";

				$("#pcap-port").prop("disabled", true);
			}
			else {

				start = 1;

				msg = $("#text-start").html();
				rem = "btn-danger";
				add = "btn-success";

				$("#pcap-port").prop("disabled", false);
			}

			$("#pcap-start").html(msg).removeClass(rem).addClass(add);
		}

		if (true) {

			var tcp = 0;
			var udp = 0;
			var arp = 0;
			var icmp = 0;
			var web = 0;

			var filter = list[1].split(";");

			for (var n = 0; n < filter.length; n++) {

				if (filter[n] != "") {

					var p = filter[n].split('=');

					if (p[0] == "tcp") {

						tcp = (p[1] == "all" || p[1] == "") ? 0xFFFF : parseInt(p[1]);
					}

					if (p[0] == "udp") {

						udp = (p[1] == "all" || p[1] == "") ? 0xFFFF : parseInt(p[1]);
					}

					if (p[0] == "arp") {

						arp = 1;
					}

					if (p[0] == "icmp") {

						icmp = 1;
					}

					if (p[0] == "web") {

						web = 1;
					}
				}
			}

			$("#pcap-tcp-port").val((!tcp || tcp == 0xFFFF) ? "" : tcp.toString());

			$("#pcap-udp-port").val((!udp || udp == 0xFFFF) ? "" : udp.toString());

			$("#pcap-tcp-enable").val(tcp ? ((tcp == 0xFFFF) ? (web ? 1 : 3) : 2) : 0);

			$("#pcap-udp-enable").val(udp ? ((udp == 0xFFFF) ? 1 : 2) : 0);

			$("#pcap-icmp-enable").val(icmp);

			$("#pcap-arp-enable").val(arp);

			doEnables();
		}

		$("#pcap-size").html(list[3] + " " + $("#text-avail").html());

		$("#pcap-link").css("display", parseInt(list[3]) ? "" : "none");

		$("#pcap-form").css("display", "");

		$("#pcap-port-fail").css("display", "none");

		$("#pcap-port-okay").css("display", "");
	}
	else {

		$("#pcap-source").prop("disabled", 0);

		$("#pcap-form").css("display", "");

		$("#pcap-port-okay").css("display", "none");

		$("#pcap-port-fail").css("display", "");
	}

	busy = false;
}

function doEnables() {

	if (start) {

		var src = $("#pcap-source").val();

		$("#pcap-tcp-enable").prop("disabled", 0);

		$("#pcap-udp-enable").prop("disabled", src == "99");

		$("#pcap-arp-enable").prop("disabled", src == "99");

		$("#pcap-icmp-enable").prop("disabled", src == "99");

		$("#pcap-tcp-port").prop("disabled", $("#pcap-tcp-enable").val() != "2");

		$("#pcap-udp-port").prop("disabled", $("#pcap-udp-enable").val() != "2");

		$("#pcap-tcp-label").css("color", $("#pcap-tcp-enable").val() != "2" ? "#888" : "#000");

		$("#pcap-udp-label").css("color", $("#pcap-udp-enable").val() != "2" ? "#888" : "#000");

		var none = ($("#pcap-tcp-enable").val() == "0" &&
					$("#pcap-udp-enable").val() == "0" &&
					$("#pcap-icmp-enable").val() == "0" &&
					$("#pcap-arp-enable").val() == "0"
					);

		$("#pcap-start").prop("disabled", none);
	}
	else {

		$("#pcap-entry select").prop("disabled", 1);

		$("#pcap-entry input").prop("disabled", 1);

		$("#pcap-start").prop("disabled", 0);
	}

	$("#pcap-source").prop("disabled", 0);
}

function fetchStatus() {

	var src = $("#pcap-source").val();

	var url = "/ajax/syspcap-status.htm?src=" + src + cacheBreaker();

	xhr.onload = statusLoaded;

	xhr.onerror = statusFailed;

	xhr.open("GET", url, true);

	xhr.send(null);
}

function statusLoaded() {

	if (xhr.status == 200) {

		updateUi(xhr.responseText);

		return;
	}

	if (check401(xhr.status)) {

		return;
	}

	statusFailed();
}

function statusFailed() {

	busy = false;
}

function onClick() {

	if (!busy) {

		var src = $("#pcap-source").val();

		var url = "/ajax/syspcap-control.htm?src=" + src;

		if (start) {

			var filter = "";

			switch (parseInt($("#pcap-tcp-enable").val())) {
				case 1:
					filter += "tcp=all;web;";
					break;
				case 2:
					filter += "tcp=" + $("#pcap-tcp-port").val() + ";";
					break;
				case 3:
					filter += "tcp=all;";
					break;
			}

			switch (parseInt($("#pcap-udp-enable").val())) {
				case 1:
					filter += "udp=all;";
					break;
				case 2:
					filter += "udp=" + $("#pcap-udp-port").val() + ";";
					break;
			}

			switch (parseInt($("#pcap-icmp-enable").val())) {
				case 1:
					filter += "icmp;";
					break;
			}

			switch (parseInt($("#pcap-arp-enable").val())) {
				case 1:
					filter += "arp;";
					break;
			}

			url += "&action=start&filter=" + filter;
		}
		else
			url += "&action=stop";

		url += cacheBreaker();

		xhr.onload = commandLoaded;

		xhr.onerror = commandFailed;

		xhr.open("GET", url, true);

		$("#pcap-source").prop("disabled", 1);

		busy = true;

		xhr.send(null);
	}
}

function commandLoaded() {

	if (xhr.status == 200) {

		updateUi(xhr.responseText);

		return;
	}

	if (check401(xhr.status)) {

		return;
	}

	commandFailed();
}

function commandFailed() {

	$("#pcap-source").prop("disabled", 0);

	busy = false;
}

// End of File
