
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Log Off Support
//

// Code

function pageMain() {

	var xhr = new XMLHttpRequest();

	xhr.open("GET", "/logoff.sub?nc=" + (new Date()).getTime(), true);

	xhr.send(null);
}

// End of File
