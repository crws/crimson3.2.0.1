
#include "Intern.hpp"

#include "StringTable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Static Data
//

static UINT m_uLang = C3L_ENGLISH_US;

//////////////////////////////////////////////////////////////////////////
//
// EN-US String Table
//

static PCTXT GetWebStringEn(UINT id)
{
	switch( id ) {

		case IDS_NEW_VALUE:		return "New Value";
		case IDS_OLD_VALUE:		return "Old Value";
		case IDS_OK:			return "OK";
		case IDS_CANCEL:		return "Cancel";
		case IDS_PLS_CLOSE_ALL:		return "Please close all browser windows to complete the log off process.";
		case IDS_PLS_LOG_ON:		return "Please Log On";
		case IDS_USER_NAME:		return "User Name";
		case IDS_PASSWORD:		return "Password";
		case IDS_HOME:			return "Home";
		case IDS_DATA:			return "Data";
		case IDS_LOGS:			return "Logs";
		case IDS_REMOTE:		return "Remote";
		case IDS_USER:			return "User";
		case IDS_LOG_OFF:		return "Log Off";
		case IDS_VIEW_DATA:		return "View Data";
		case IDS_VIEW_DATA_DESC:	return "Display a list showing available data pages.";
		case IDS_VIEW_LOGS:		return "View Logs";
		case IDS_VIEW_CONT_DESC:	return "Download continuous files from the data logger.";
		case IDS_VIEW_BATCHES:		return "View Batches";
		case IDS_VIEW_BATCHES_DESC:	return "Download batched files from the data logger.";
		case IDS_VIEW_LOGS_DESC:	return "Download files from the data logger.";
		case IDS_REMOTE_VIEW:		return "Remote View";
		case IDS_REMOTE_CTRL_DESC:	return "Display an interactive view of the device's display.";
		case IDS_REMOTE_VIEW_DESC:	return "Display a read-only view of the device's display.";
		case IDS_USER_SITE:		return "User Site";
		case IDS_USER_SITE_DESC:	return "Access the custom website for this device.";
		case IDS_NO_PERMISSIONS:	return "You do not have permission to access any features.";
		case IDS_NAME:			return "Name";
		case IDS_VALUE:			return "Value";
		case IDS_EDITING:		return "Editing";
		case IDS_EDIT_DOTS:		return "Edit...";
		case IDS_EVENT_LOG:		return "Event Log";
		case IDS_SECURITY_LOG:		return "Security Log";
		case IDS_LIST_BATCHES:		return "List of Available Batches";
		case IDS_LIST_LOGS_IN:		return "List of Available Logs in %s";
		case IDS_LIST_FILES_IN_IN:	return "List of Files in %s in %s";
		case IDS_LIST_FILES:		return "List of Available Files";
		case IDS_LIST_FILES_IN:		return "List of Files in %s";
		case IDS_NO_DATA:		return "No Data Available";
		case IDS_FAILED_WRITE_X:	return "Failed to write to %s";
		case IDS_FAILED_WRITE_TAG:	return "Failed to write to tag";
		case IDS_X_SET_TO_Y:		return "%s set to %s.";
		case IDS_BATCHES:		return "Batches";
		case IDS_DESCRIPTION:		return "Description";
		case IDS_LIST_LOGS:		return "List of Available Logs";
		case IDS_EVENTS:		return "Events";
		case IDS_SECURITY:		return "Security";
		case IDS_PAGE:			return "Page";
		case IDS_AVAILABLE_PAGES:	return "Available Pages";
		case IDS_LOG_OFF_CURRENT:	return "Log Off Current User";
		case IDS_LOG_ON:		return "Log On";
		case IDS_ACCESS_DENIED:		return "Access is denied.";
		case IDS_BACK:			return "Back";
		case IDS_ENH_WEB_SERVER:	return "Enhanced Web Server";
		case IDS_WELCOME:		return "Welcome to the Enhanced Web Server!";
		case IDS_OPTION:		return "Option";
		case IDS_SIZE:			return "Size";
		case IDS_WRITE_OKAY_HEAD:	return "Write Complete";
		case IDS_WRITE_OKAY_BODY:	return "The data values were written successfully.";
		case IDS_WRITE_FAIL_HEAD:	return "Write Failed";
		case IDS_WRITE_FAIL_BODY:	return "At least one data value was not written successfully.";
		case IDS_CLICK_TO_RETURN:	return "Back";
		case IDS_SYSTEM:		return "Diagnostics";
		case IDS_SYSTEM_TITLE:		return "System Diagnostics";
		case IDS_SYSTEM_PAGES:		return "System Diagnostics";
		case IDS_SYSTEM_DESC:		return "Access diagnostic information about this device.";
		case IDS_PCAP_NAME:		return "Network Capture";
		case IDS_PCAP_DESC:		return "Download information captured from the device's Ethernet ports.";
		case IDS_PCAP_START:		return "Start Capture"; 
		case IDS_PCAP_STOP:		return "Stop Capture";
		case IDS_PCAP_BYTES:		return "bytes available.";
		case IDS_PCAP_SRC:		return "Capture Source";
		case IDS_PCAP_NOT:		return "The selected port is not available for capture.";
		case IDS_PCAP_TCP:		return "TCP Capture";
		case IDS_PCAP_TCP_PORT:		return "TCP Port";
		case IDS_PCAP_UDP:		return "UDP Capture";
		case IDS_PCAP_UDP_PORT:		return "UDP Port";
		case IDS_PCAP_ARP:		return "Management Frames";
		case IDS_PCAP_DOWN:		return "Download the network capture.";
		case IDS_CMD_UPDATE:		return "Update in Real Time";
		case IDS_DBG_CONSOLE:		return "Debug Console";
		case IDS_DBG_FREEZE:		return "Freeze Display";
		case IDS_PCAP_SSL:		return "SSL Data";
		case IDS_PCAP_DISABLED:		return "Disabled";
		case IDS_PCAP_ENABLED:		return "Enabled";
		case IDS_PCAP_SPECIFIC:		return "Specific Port";
		case IDS_PCAP_ALL:		return "All Traffic";
		case IDS_PCAP_NON_WEB:		return "All Non-Web Traffic";
		case IDS_DBG_INTERACT:		return "Interact with the system's debug console.";
		case IDS_NET_CFG:		return "Network Config";	
		case IDS_NET_STS:		return "Network Status";	
		case IDS_NET_RTS:		return "Network Routes";	
		case IDS_NET_STS1:		return "Ethernet 1 Status";
		case IDS_NET_STS2:		return "Ethernet 2 Status";
		case IDS_NET_DCFG:		return "Display the configuration of the network stack.";	
		case IDS_NET_DSTS:		return "Display the sockets opened by the network stack.";	
		case IDS_NET_DRTS:		return "Display the network routing table.";			
		case IDS_NET_DSTS1:		return "Display the status of Ethernet port 1.";		
		case IDS_NET_DSTS2:		return "Display the status of Ethernet port 2.";		
		}

	return "[STRING-EN]";
	}

//////////////////////////////////////////////////////////////////////////
//
// FR-FR String Table
//

static PCTXT GetWebStringFr(UINT id)
{
	switch( id ) {

		case IDS_NEW_VALUE:		return "Nouvelle Valeur";
		case IDS_OLD_VALUE:		return "Ancienne Valeur";
		case IDS_OK:			return "OK";
		case IDS_CANCEL:		return "Effacer";
		case IDS_PLS_CLOSE_ALL:		return "Fermez toutes les fen�tres du navigateur pour terminer le processus de d&eacute;connexion.";
		case IDS_PLS_LOG_ON:		return "SVP Enregistrez-Vous";
		case IDS_USER_NAME:		return "Nom d'Utilisateur";
		case IDS_PASSWORD:		return "Mot de Passe";
		case IDS_HOME:			return "Retour";
		case IDS_DATA:			return "Donn&eacute;es";
		case IDS_LOGS:			return "Journaux";
		case IDS_REMOTE:		return "A Distance";
		case IDS_USER:			return "Utilisateur";
		case IDS_LOG_OFF:		return "D&eacute;connexion";
		case IDS_VIEW_DATA:		return "Visualisation des Donn&eacute;es";
		case IDS_VIEW_DATA_DESC:	return "Affiche une liste montrant les donn&eacute;es disponibles.";
		case IDS_VIEW_LOGS:		return "Visualisation des Enregistrements";
		case IDS_VIEW_CONT_DESC:	return "T&eacute;l&eacute;charge les fichiers d'enregistrements continus de l'enregistreur de donn&eacute;es.";
		case IDS_VIEW_BATCHES:		return "Visualisation des Lots";
		case IDS_VIEW_BATCHES_DESC:	return "T&eacute;l&eacute;chargez les fichiers de lots de l'enregistreur de donn&eacute;es.";
		case IDS_VIEW_LOGS_DESC:	return "T&eacute;l&eacute;chargez les fichiers &agrave; partir de l'enregistreur de donn&eacute;es.";
		case IDS_REMOTE_VIEW:		return "Visualisation &agrave; Distance";
		case IDS_REMOTE_CTRL_DESC:	return "Affiche une vue interactive de l'affichage de l'IHM.";
		case IDS_REMOTE_VIEW_DESC:	return "Affiche une vue en lecture seule de l'affichage de l'IHM.";
		case IDS_USER_SITE:		return "Site Web Personnalis&eacute;";
		case IDS_USER_SITE_DESC:	return "Acc&eacute;s au site web personnalis&eacute; de cet IHM.";
		case IDS_NO_PERMISSIONS:	return "Vous n'avez pas la permission d'acc&eacute;der &agrave; toutes les fonctionnalit&eacute;s.";
		case IDS_NAME:			return "Nom";
		case IDS_VALUE:			return "Valeur";
		case IDS_EDITING:		return "Edition";
		case IDS_EDIT_DOTS:		return "Modifier...";
		case IDS_EVENT_LOG:		return "Journaux des Ev&eacute;nements";
		case IDS_SECURITY_LOG:		return "Journaux Niveau d'Acc&egrave;s";
		case IDS_LIST_BATCHES:		return "Liste des Lots Disponibles";
		case IDS_LIST_LOGS_IN:		return "Liste des Entr&eacute;es Disponibles %s";
		case IDS_LIST_FILES_IN_IN:	return "Liste des Fichiers en %s en %s";
		case IDS_LIST_FILES:		return "Liste des Fichiers Disponibles";
		case IDS_LIST_FILES_IN:		return "Liste des Fichiers en %s";
		case IDS_NO_DATA:		return "Pas de Donn&eacute;es Disponibles";
		case IDS_FAILED_WRITE_X:	return "Impossible d'&eacute;crire sur %s.";
		case IDS_FAILED_WRITE_TAG:	return "&eacute;chec de l'&eacute;criture sur l'&eacute;tiquette.";
		case IDS_X_SET_TO_Y:		return "%s r&eacute;gl&eacute; sur %s.";
		case IDS_BATCHES:		return "Lots";
		case IDS_DESCRIPTION:		return "Description";
		case IDS_LIST_LOGS:		return "Liste des Entr&eacute;es Disponibles";
		case IDS_EVENTS:		return "Ev&eacute;nements";
		case IDS_SECURITY:		return "Niveau d'Acc&egrave;s";
		case IDS_PAGE:			return "Page";
		case IDS_AVAILABLE_PAGES:	return "Pages Disponibles";
		case IDS_LOG_OFF_CURRENT:	return "Fermez La Session Utilisateur en Cours";
		case IDS_LOG_ON:		return "Enregistrez-Vous";
		case IDS_ACCESS_DENIED:		return "L'acc&egrave;s est refus&eacute;.";
		case IDS_BACK:			return "Back";
		case IDS_ENH_WEB_SERVER:	return "Serveur Web";
		case IDS_WELCOME:		return "Bienvenue sur le Serveur Web!";
		case IDS_OPTION:		return "Option";
		case IDS_SIZE:			return "Taille";
		case IDS_WRITE_OKAY_HEAD:	return "�criture Termin�e";
		case IDS_WRITE_OKAY_BODY:	return "Les valeurs de donn�es ont �t� �crites avec succ�s.";
		case IDS_WRITE_FAIL_HEAD:	return "�chec de L'�criture";
		case IDS_WRITE_FAIL_BODY:	return "Au moins une valeur de donn�es n'a pas �t� �crite avec succ�s.";
		case IDS_CLICK_TO_RETURN:	return "Back";
		}

	return "[STRING-FR]";
	}

//////////////////////////////////////////////////////////////////////////
//
// DE-DE String Table
//

static PCTXT GetWebStringDe(UINT id)
{
	switch( id ) {

		case IDS_NEW_VALUE:		return "Neuer Wert";
		case IDS_OLD_VALUE:		return "Alter Wert";
		case IDS_OK:			return "OK";
		case IDS_CANCEL:		return "Abbrechen";
		case IDS_PLS_CLOSE_ALL:		return "Bitte alle Browserfenster schlie&szlig;en um das Abmelden zu beenden.";
		case IDS_PLS_LOG_ON:		return "Bitte Einloggen";
		case IDS_USER_NAME:		return "Benutzername";
		case IDS_PASSWORD:		return "Passwort";
		case IDS_HOME:			return "Zur&uuml;ck";
		case IDS_DATA:			return "Daten";
		case IDS_LOGS:			return "Aufzeichnungen";
		case IDS_REMOTE:		return "Fernzugriff";
		case IDS_USER:			return "Bediener";
		case IDS_LOG_OFF:		return "Abmelden";
		case IDS_VIEW_DATA:		return "Dantenansicht";
		case IDS_VIEW_DATA_DESC:	return "Liste mit verf&uuml;gbaren Datenseiten anzeigen.";
		case IDS_VIEW_LOGS:		return "Aufzeichnugen ansehen";
		case IDS_VIEW_CONT_DESC:	return "Kontinuierliche Aufzeichnungen aus dem Datenlogger laden.";
		case IDS_VIEW_BATCHES:		return "Chargenaufzeichnung ansehen";
		case IDS_VIEW_BATCHES_DESC:	return "Chargenaufzeichnungen aus dem Datenlogger laden.";
		case IDS_VIEW_LOGS_DESC:	return "Dateien aus dem Dantelogger laden.";
		case IDS_REMOTE_VIEW:		return "Fernansicht";
		case IDS_REMOTE_CTRL_DESC:	return "Interaktive Ansicht des Ger&auml;tedisplays darstellen.";
		case IDS_REMOTE_VIEW_DESC:	return "Schreibgesch&uuml;tzte Ansicht des Ger&auml;tedisplays darstellen.";
		case IDS_USER_SITE:		return "Bediener-Webseite";
		case IDS_USER_SITE_DESC:	return "Zugriff auf die Bediener-Webseite des Ger&auml;tes.";
		case IDS_NO_PERMISSIONS:	return "Sie haben keine Berechtigung auf Funktionen zuzugreifen.";
		case IDS_NAME:			return "Name";
		case IDS_VALUE:			return "Wert";
		case IDS_EDITING:		return "Bearbeitung";
		case IDS_EDIT_DOTS:		return "Bearbeiten...";
		case IDS_EVENT_LOG:		return "Ereignisaufzeichnung";
		case IDS_SECURITY_LOG:		return "Sicherheitsaufzeichnung";
		case IDS_LIST_BATCHES:		return "Liste verf&uuml;gbarer Chargenaufzeichnungen";
		case IDS_LIST_LOGS_IN:		return "Liste verf&uuml;gbarer Aufzeichnungen in %s";
		case IDS_LIST_FILES_IN_IN:	return "Liste verf&uuml;gbarer Dateien in %s in %s";
		case IDS_LIST_FILES:		return "Liste verf&uuml;gbarer Dateien";
		case IDS_LIST_FILES_IN:		return "Liste verf&uuml;gbarer Dateien in %s";
		case IDS_NO_DATA:		return "Keine Daten verf&uuml;gbar";
		case IDS_FAILED_WRITE_X:	return "Fehler beim Schreiben auf %s.";
		case IDS_FAILED_WRITE_TAG:	return "Fehler beim Schreiben auf tag.";
		case IDS_X_SET_TO_Y:		return "%s auf %s setzen.";
		case IDS_BATCHES:		return "Chargenaufzeichnungen";
		case IDS_DESCRIPTION:		return "Beschreibung";
		case IDS_LIST_LOGS:		return "Liste verf&uuml;gbarer Aufzeichnungen";
		case IDS_EVENTS:		return "Ereignis";
		case IDS_SECURITY:		return "Sicherheits";
		case IDS_PAGE:			return "Seite";
		case IDS_AVAILABLE_PAGES:	return "Verf&uuml;gbare Seiten";
		case IDS_LOG_OFF_CURRENT:	return "Aktuellen Benutzer Abmelden";
		case IDS_LOG_ON:		return "Einloggen";
		case IDS_ACCESS_DENIED:		return "Der Zugriff wurde verweigert.";
		case IDS_BACK:			return "Back";
		case IDS_ENH_WEB_SERVER:	return "Erweiterter Webserver";
		case IDS_WELCOME:		return "Willkommen zum Erweiterten Webserver!";
		case IDS_OPTION:		return "Option";
		case IDS_SIZE:			return "Gr&ouml;&szlig;e";
		case IDS_WRITE_OKAY_HEAD:	return "Schreiben Komplett";
		case IDS_WRITE_OKAY_BODY:	return "Die Datenwerte wurden erfolgreich geschrieben.";
		case IDS_WRITE_FAIL_HEAD:	return "Schreiben Fehlgeschlagen";
		case IDS_WRITE_FAIL_BODY:	return "Mindestens ein Datenwert wurde nicht erfolgreich geschrieben.";
		case IDS_CLICK_TO_RETURN:	return "Back";
		case IDS_SYSTEM:		return "System";
		case IDS_SYSTEM_TITLE:		return "Systemdiagnose";
		case IDS_SYSTEM_PAGES:		return "Systemdiagnose";
		case IDS_SYSTEM_DESC:		return "Zugriff auf Diagnoseinformationen &uuml;ber dieses Ger&auml;t.";
		case IDS_PCAP_NAME:		return "Netzwerk erfassen";
		case IDS_PCAP_DESC:		return "Informationen herunterladen, die von den Ethernet-Anschl&uuml;ssen des Ger&auml;ts erfasst wurden.";
		case IDS_PCAP_START:		return "Erfassen starten ";
		case IDS_PCAP_STOP:		return "Erfassen stoppen";
		case IDS_PCAP_BYTES:		return "verf&uuml;gbare Bytes";
		case IDS_PCAP_SRC:		return "Quelle erfassen";
		case IDS_PCAP_NOT:		return "Der ausgew&auml;hlte Anschluss ist nicht zur Erfassungverf&uuml;gbar.";
		case IDS_PCAP_TCP:		return "TCP Erfassung";
		case IDS_PCAP_TCP_PORT:		return "TCP-Port";
		case IDS_PCAP_UDP:		return "UDP Erfassung";
		case IDS_PCAP_UDP_PORT:		return "UDP-Anschluss";
		case IDS_PCAP_ARP:		return "ARP Erfassung";
		case IDS_PCAP_DOWN:		return "Die Netzwerkerfassung herunterladen.";
		case IDS_CMD_UPDATE:		return "Update in Echtzeit";
		case IDS_DBG_CONSOLE:		return "Debug-Konsole";
		case IDS_DBG_FREEZE:		return "Display einfrieren";
		case IDS_PCAP_SSL:		return "SSL Daten";
		case IDS_PCAP_DISABLED:		return "Deaktiviert";
		case IDS_PCAP_ENABLED:		return "Aktiviert";
		case IDS_PCAP_SPECIFIC:		return "Angegebener Anschluss";
		case IDS_PCAP_ALL:		return "Gesamter Verkehr";
		case IDS_PCAP_NON_WEB:		return "Gesamter Nicht-Web-Verkehr";
		case IDS_DBG_INTERACT:		return "Interaktion mit der Debug-Konsole des Systems.";
		case IDS_NET_CFG:		return "Netzwerk Konfig";
		case IDS_NET_STS:		return "Netzwerkstatus";
		case IDS_NET_RTS:		return "Netzwerkrouten";
		case IDS_NET_STS1:		return "Ethernet 1 Status";
		case IDS_NET_STS2:		return "Ethernet 2 Status";
		case IDS_NET_DCFG:		return "Die Konfiguration des Netzwerk-Stapels anzeigen.";
		case IDS_NET_DSTS:		return "Die Sockets anzeigen, die vom Netzwerk-Stapel ge&ouml;ffnet wurden.";
		case IDS_NET_DRTS:		return "Die Routingtabelle des Netzwerks anzeigen.";
		case IDS_NET_DSTS1:		return "Den Status von Ethernet-Anschluss 1 anzeigen.";
		case IDS_NET_DSTS2:		return "Den Status von Ethernet-Anschluss 2 anzeigen.";
		}

	return "[STRING-DE]";
	}

//////////////////////////////////////////////////////////////////////////
//
// String API
//

global void SetWebLanguage(PCTXT pHeader)
{
	if( strlen(pHeader) >= 2 ) {

		char c1 = tolower(pHeader[0]);

		char c2 = tolower(pHeader[1]);

		if( c1 == 'e' && c2 == 'n' ) { m_uLang = C3L_ENGLISH_US; return; }

		//if( c1 == 'f' && c2 == 'r' ) { m_uLang = C3L_FRENCH_FR;  return; }

		if( c1 == 'd' && c2 == 'e' ) { m_uLang = C3L_GERMAN_DE;  return; }
		}

	m_uLang = C3L_ENGLISH_US;
	}

global PCTXT GetWebString(UINT id)
{
	PCTXT p = NULL;

	switch( m_uLang ) {

		case C3L_FRENCH_FR:

			if( (p = GetWebStringFr(id))[0] != '[' ) {

				return p;
				}

			break;

		case C3L_GERMAN_DE:

			if( (p = GetWebStringDe(id))[0] != '[' ) {

				return p;
				}

			break;
		}

	return GetWebStringEn(id);
	}

// End of File
