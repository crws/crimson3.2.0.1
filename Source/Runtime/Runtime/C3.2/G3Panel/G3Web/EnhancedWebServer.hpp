
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EnhancedWebServer_HPP

#define	INCLUDE_EnhancedWebServer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class  CTheHttpServer;

class  CTheHttpServerSession;

class  CWebFileLibrary;

struct CWebReqContext;

//////////////////////////////////////////////////////////////////////////
//
// Enhanced Web Server
//

class CEnhancedWebServer : public CWebBase
{
	public:
		// Constructor
		CEnhancedWebServer(void);

		// Destructor
		~CEnhancedWebServer(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		UINT GetDatabaseTime(void) const;
		UINT GetWebBufferSize(void) const;
		UINT GetWebBufferBits(void) const;
		BOOL AllowLogs(CString &File);

		// Operations
		CString FindPass(CString User);
		CString FindReal(CString User);
		BOOL    CanAccessHtml(CString Name);
		BOOL    ExpandAllElements(CWebReqContext const &Ctx, CString &Text, BOOL &fCache);
		BOOL	ExecuteAjax(CWebReqContext const &Ctx, CString Name);
		BOOL    ReplyWithFile(CWebReqContext const &Ctx, CString Name);
		BOOL    ReplyToPost(CWebReqContext const &Ctx, CString Name);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Param Access
		C3INT GetWebParamInt(PCTXT pName);
		C3INT GetWebParamHex(PCTXT pName);
		PUTF  GetWebParamStr(PCTXT pName);
		PUTF  GetWebUser(UINT n);
		void  ClearWebUsers(void);

	protected:
		// Tag Reference
		struct CTagRef
		{
			CTag * pTag;
			UINT   uPos;
			};

		// System Command
		struct CSysCmd
		{
			UINT   uName;
			UINT   uDesc;
			char * pCmd;
			bool   fRt;
			bool   fEn;
			};

		// System Commands
		static CSysCmd m_Cmds[];

		// Data Members
		CHttpServerOptions   * m_pOpts;
		CHttpServerManager   * m_pManager;
		CTheHttpServer       * m_pServer;
		CWebFileLibrary      * m_pLibrary;
		CSecurityManager     * m_pSecure;
		CWebReqContext const * m_pCtx;
		int		       m_cxRem;
		int		       m_cyRem;
		UINT		       m_cwRem;
		UINT		       m_uBits;
		PDWORD		       m_pRemCols;

		// Server Management
		BOOL OpenServer(void);
		BOOL CloseServer(void);

		// Element Expansion
		BOOL    ExpandNewElements(CWebReqContext const &Ctx, CString &Text, BOOL &fCache);
		BOOL    ExpandOldElements(CWebReqContext const &Ctx, CString &Text, BOOL &fCache);
		BOOL    IsTagElement(CWebReqContext const &Ctx, CString Name);
		BOOL    GetTagElement(CWebReqContext const &Ctx, CString Name, CTagRef &Ref);
		CString GetElementData(CWebReqContext const &Ctx, CString Name, BOOL &fCache);

		// Page Handlers
		CString ExpandGlobal(CWebReqContext const &Ctx, CString Name);
		CString ExpandDataIndex(CWebReqContext const &Ctx, CString Name);
		CString ExpandDataView(CWebReqContext const &Ctx, CString Name);
		CString ExpandLogsView(CWebReqContext const &Ctx, CString Name);
		CString ExpandLogsNames(BOOL fBatch, CString Link);
		CString ExpandLogsDirs(CString Path, CString Link, BOOL fTime);
		CString ExpandLogsFiles(CString Path);
		CString ExpandSystem(CWebReqContext const &Ctx, CString Name);

		// Form Submission
		BOOL PostWriteTags(CWebReqContext const &Ctx);
		void Redirect(CWebReqContext const &Ctx, CString Page);
		BOOL ParseUrlEncodedBody(CZeroMap <CString, CString> &Vars, CString Body);

		// Ajax Handlers
		BOOL ExecuteDataViewRead(CWebReqContext const &Ctx);
		BOOL ExecuteDataViewWrite(CWebReqContext const &Ctx);
		BOOL ExecuteSysCmdUpdate(CWebReqContext const &Ctx);
		BOOL ExecuteSysDebugUpdate(CWebReqContext const &Ctx);
		BOOL ExecuteSysPcapStatus(CWebReqContext const &Ctx);
		BOOL ExecuteSysPcapControl(CWebReqContext const &Ctx);
		BOOL ExecuteSysPcapRead(CWebReqContext const &Ctx);
		BOOL ExecuteRemoteEvent(CWebReqContext const &Ctx);
		BOOL ExecuteRemotePalette(CWebReqContext const &Ctx);
		BOOL ExecuteRemoteDisplay08(CWebReqContext const &Ctx);
		BOOL ExecuteRemoteDisplay16(CWebReqContext const &Ctx);
		BOOL ExecuteRemoteDisplay24(CWebReqContext const &Ctx);
		BOOL ExecuteRemoteDelta08(CWebReqContext const &Ctx);
		BOOL ExecuteRemoteDelta16(CWebReqContext const &Ctx);
		BOOL ExecuteRemoteDelta24(CWebReqContext const &Ctx);

		// Display Rendering
		BOOL MakeDisplayBlob08(CHttpServerRequest *pReq, PBYTE pWork, PCBYTE pBits, int y1, int y2, int x1, int x2);
		BOOL MakeDisplayBlob08(PBYTE &pComp, PBYTE pWork, PCBYTE pBits, int y1, int y2, int x1, int x2);
		BOOL MakeDisplayBlob16(CHttpServerRequest *pReq, PBYTE pWork, PCWORD pBits, int y1, int y2, int x1, int x2);
		BOOL MakeDisplayBlob16(PBYTE &pComp, PBYTE pWork, PCWORD pBits, int y1, int y2, int x1, int x2);
		BOOL MakeDisplayBlob24(CHttpServerRequest *pReq, PBYTE pWork, PCDWORD pBits, int y1, int y2, int x1, int x2);
		BOOL MakeDisplayBlob24(PBYTE &pComp, PBYTE pWork, PCDWORD pBits, int y1, int y2, int x1, int x2);

		// Implementation
		void    SafeDec(int &i);
		void    SafeInc(int &i);
		CString GetRemoteUrl(void);
		DWORD   GetWinColor(COLOR Color);
		BOOL    AllowAccess(DWORD Access);
		BOOL    CanEditTag(CTag *pTag);
		BOOL	CanAccessAtLeastOnePage(void);
		BOOL    CanAccessPage(CWebPage *pPage);
		void    AddListItem(CString &d, CHttpServerRequest *pReq, CString Url, CString Text);
		BOOL    StripSession(CHttpServerRequest *pReq, CString &Text);
		BOOL    FakeKey(UINT uState, UINT uCode);
		BOOL    FakeTouch(UINT uState, int xPos, int yPos);
		BOOL	Restrict(CHttpServerConnection *pCon);
		BOOL	WaitForTags(CTagRef *pRefs, UINT uRefs);
		BOOL    ShowLogOff(void);
		BOOL    CanEditTag(UINT uPage, UINT uTag);
		BOOL    RunCommand(char **ppText, UINT n);
		void    MakeOptionTable(CString &t, PCTXT p, CString d);
		void    CheckCmds(void);

		// Comparison Helpers
		static int cmpl(PCBYTE  p1, PCBYTE  p2, int n);
		static int cmpr(PCBYTE  p1, PCBYTE  p2, int n);
		static int cmpl(PCWORD  p1, PCWORD  p2, int n);
		static int cmpr(PCWORD  p1, PCWORD  p2, int n);
		static int cmpl(PCDWORD p1, PCDWORD p2, int n);
		static int cmpr(PCDWORD p1, PCDWORD p2, int n);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

STRONG_INLINE void CEnhancedWebServer::SafeDec(int &i)
{
	if( i > 0 ) i--;
	}

STRONG_INLINE void CEnhancedWebServer::SafeInc(int &i)
{
	if( i < m_cyRem - 1 ) i++;
	}

// End of File

#endif
