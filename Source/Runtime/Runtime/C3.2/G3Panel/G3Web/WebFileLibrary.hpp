
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_WebFileLibrary_HPP

#define	INCLUDE_WebFileLibrary_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class  CEnhancedWebServer;

struct CWebFileData;

struct CWebReqContext;

//////////////////////////////////////////////////////////////////////////
//
// File Library
//

class CWebFileLibrary
{
	public:
		// Constructor
		CWebFileLibrary(CEnhancedWebServer *pServer, CHttpServerManager *pManager);

		// Destructor
		~CWebFileLibrary(void);

		// Operations
		void    AddFile(CWebFileData const *pFile);
		BOOL    ReplyWithFile(CWebReqContext const &Ctx, CString Name);
		CString GetFileText(CWebReqContext const &Ctx, CString Name);

		// Instance Pointer
		static CWebFileLibrary * m_pThis;

	protected:
		// File Table
		static CWebFileData m_Files[];

		// File Index
		typedef CMap <CString, CWebFileData const *> CFileIndex;

		// Data Members
		CEnhancedWebServer * m_pServer;
		CHttpServerManager * m_pManager;
		time_t		     m_timeComp;
		time_t		     m_timeBoot;
		CFileIndex           m_Index;

		// Implementation
		BOOL MakeIndex(void);
		void FindTimes(void);
	};

// End of File

#endif
