
#include "Intern.hpp"

#include "TheHttpServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "TheHttpServerSession.hpp"

#include "EnhancedWebServer.hpp"

#include "StringTable.hpp"

#include "WebFileLibrary.hpp"

#include "../G3Comms/secure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Web Server
//

// Static Data

static UINT const defExpire = 24 * 60 * 60;

// Constructor

CTheHttpServer::CTheHttpServer(CEnhancedWebServer *pServer, CHttpServerManager *pManager, CHttpServerOptions const &Opts) : CHttpServer(pManager, Opts)
{
	m_pServer    = pServer;

	m_timeBoot   = time(NULL);

	m_timeComp   = CHttpTime::Parse("Day, " __DATE__ " " __TIME__ " EST");

	m_nBuff      = 0;

	m_SessionKey = "C32-App-Session";
	}

// Destructor

CTheHttpServer::~CTheHttpServer(void)
{
	}

// Overridables

CString CTheHttpServer::FindPass(CString User)
{
	return m_pServer->FindPass(User);
	}

CString CTheHttpServer::FindReal(CString User)
{
	return m_pServer->FindReal(User);
	}

void CTheHttpServer::ServePage(CHttpServerSession *pSession, CConnectionInfo &Info, CHttpServerRequest *pReq)
{
	// Setup the request context we'll be using.

	CWebReqContext Ctx;

	Ctx.pSess = (CTheHttpServerSession *) pSession;
	
	Ctx.pCon  = Info.m_pCon;
	
	Ctx.pReq  = pReq;

	// Parse the URI into the initial
	// path element and the remainder.

	CString Full = GetPath(Ctx.pReq);

	UINT    uPos = Full.FindRev('/');

	CString Name = Full.Mid (uPos+1);

	CString Path = Full.Left(uPos+1);

	// Are we handling a GET request?

	if( Ctx.pReq->GetVerb() == "GET" ) {
		
		// Requests for the favorite icon are special-cased
		// as browsers ping this URL a lot and we don't want
		// to trigger any security sessions etc.

		if( Full == "/favicon.ico" ) {

			if( m_pServer->m_CustIcon && ServeCustom(Ctx, "/favicon.ico") ) {

				return;
				}

			if( m_pServer->ReplyWithFile(Ctx, "/assets/icons/favicon.ico") ) {

				return;
				}

			Ctx.pReq->SetStatus(404);

			return;
			}

		// Check for re-entry from the runtime server and
		// allow it without a user check at this point.

		if( Full == "/ajax/jump-runtime.htm" ) {

			if( m_pServer->ExecuteAjax(Ctx, Name) ) {

				return;
				}
			}

		// Requests in the /custom folder are for the custom
		// stylesheet or script package. Again, we handle these
		// before even thinking about form-based security.

		if( Path.Left(8) == "/custom/" ) {

			// Custom script?

			if( Name == "custom.js" ) {

				// If enabled, reply with the file from the memory card.

				if( m_pServer->m_CustJs && ServeCustom(Ctx, "/custom.js") ) {

					return;
					}

				// Otherwise reply with an empty default.

				Ctx.pReq->SetContentType(m_pManager->GetMimeType("js"));

				Ctx.pReq->SetReplyBody("// Empty Script\r\n");

				Ctx.pReq->AddCacheInfo(m_timeComp, time(NULL) + defExpire, "DFile:0001");

				Ctx.pReq->SetStatus(200);

				return;
				}

			// Custom stylesheet?

			if( Name == "custom.css" ) {

				// If enabled, reply with the file from the memory card.

				if( m_pServer->m_CustCss && ServeCustom(Ctx, "/custom.css") ) {

					return;
					}

				// Otherwise reply with an empty default.

				Ctx.pReq->SetContentType(m_pManager->GetMimeType("css"));

				Ctx.pReq->SetReplyBody("/* Empty Style Sheet */\r\n");

				Ctx.pReq->AddCacheInfo(m_timeComp, time(NULL) + defExpire, "DFile:0002");

				Ctx.pReq->SetStatus(200);

				return;
				}

			// Barf on anything else.

			Ctx.pReq->SetStatus(404);

			return;
			}

		// Requests in the /assets folder are for the standard
		// scripts, stylesheets etc. that support the website.
		// We hand these off to the file manager to fulfil.

		if( Path.Left(8) == "/assets/" ) {

			if( m_pServer->ReplyWithFile(Ctx, Full) ) {

				return;
				}
			}

		// This is a good time to check the language.

		SetWebLanguage(pReq->GetRequestHeader("Accept-Language"));

		// If we've got this far, we need to start thinking
		// about form-based authentication if it is enabled.

		if( m_Opts.m_uAuthMethod == methodForm ) {

			// Is this a file in the root directory?

			if( Path == "/" ) {

				// Logon page?

				if( Name == "logon.htm" ) {

					// Serve the custom page if one is enabled,
					// or hand off to the file manager to fulfil.

					if( !m_pServer->m_CustLogon || !ServeCustom(Ctx, "/logon.htm") ) {

						if( !m_pServer->ReplyWithFile(Ctx, Full) ) {

							Ctx.pReq->SetStatus(404);
							}
						}

					return;
					}

				// Logoff page?

				if( Name == "logoff.htm" ) {

					// There isn't a real logoff page for form-based
					// authentication. Instead we clear the sesion
					// information and then redirect back to the logon
					// page so a New user can enter their details.

					Ctx.pSess->ClearUserInfo();

					CString Body;

					Body += "<!DOCTYPE html>\r\n";
					Body += "<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n";
					Body += "<script type='text/javascript'>window.location='/logon.htm?uri=/default.htm'</script>";
					Body += "</html>\r\n";

					Ctx.pReq->SetReplyBody(Body);

					Ctx.pReq->SetStatus(200);

					return;
					}

				// Logon script?
						
				if( Name == "logon.sub" ) {

					// This page is used to submit logon information from the
					// form on the logon page. The credentials are sent in the
					// clear in the URI, but that's okay as this method should
					// only be used over HTTPS.

					CString User = Ctx.pReq->GetParamString("user", "");

					CString Pass = Ctx.pReq->GetParamString("pass", "");

					// If the password is empty or invalid, reply with a suitable
					// error message that will be picked up by the logon script
					// and relayed to the user. Close the connection and pause a
					// little to prevent repeat attacks.

					if( Pass.IsEmpty() || FindPass(User).CompareC(Pass) ) {

						Ctx.pReq->SetReplyBody(GetWebString(IDS_ACCESS_DENIED));

						Ctx.pReq->SetStatus(401);

						Ctx.pCon->ClearKeepAlive();

						Sleep(500);
						}
					else {
						// Otherwise, we can set the user and their real name.

						Ctx.pSess->SetUser(User);

						Ctx.pSess->SetReal(FindReal(User));

						// We're going to use the first eight of the session
						// opaque value as our URL-stored session identifier.

						Ctx.pReq->SetReplyBody(Ctx.pSess->GetOpaque().Left(8));

						Ctx.pReq->SetStatus(200);
						}

					return;
					}
				}

			// Do we have a valid user right now?

			if( !Ctx.pSess->HasUser() ) {

				// Are we accessing an ajax resource?

				if( Path == "/ajax/" ) {

					// If so, we're probably on a remote viee or data
					// page, so send a reply that our scripts will pickup
					// to redirect the user to the logon page.

					Ctx.pReq->SetReplyBody("Unauthorized.");

					Ctx.pReq->SetStatus(401);
					}
				else {
					// Otherwise, unauthorized things are afoot so we need
					// to bounce the request and redirect the user to the logon
					// page. We pass the current URI in the logon path so that
					// the script can redirect after upon sucess, but we are
					// careful to first remove any session information.

					CString Body;

					CString Path = Ctx.pReq->GetFullPath();

					StripSession(Ctx.pReq, Path);

					// Simple HTML to redirect us to the logon page.

					Body += "<!DOCTYPE html>\r\n";
					Body += "<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n";
					Body += "<script type='text/javascript'>window.location='/logon.htm?uri=";
					Body += Path;
					Body += "';</script>\r\n";
					Body += "</html>\r\n";

					Ctx.pReq->SetReplyBody(Body);

					Ctx.pReq->SetStatus(200);
					}

				return;
				}

			// Tickle the security manager so they switch
			// to the required user. We might have a few
			// going at once between different clients.

			FindPass(Ctx.pSess->GetUser());
			}

		// Are we using HTTP authentication?
		
		if( m_Opts.m_uAuthMethod == methodHttp ) {

			// If so, an attempt to logoff will present the user with
			// a page from file library that asks them to close all
			// windows to complete the logoff. That page will send an
			// ajax request for this script, which we use to shutdown
			// the session and close any existing connections.

			if( Path == "/" ) {

				if( Name == "logoff.sub" ) {

					EndSession(Ctx.pSess, Info.n);

					Ctx.pSess->ClearUserInfo();

					Ctx.pReq->SetReplyBody("OK");

					Ctx.pReq->SetStatus(200);

					return;
					}
				}
			}

		// Now we can start serving real pages. Start
		// by considering those in the root directory.

		if( Path == "/" ) {

			// Serve the customer home page if there is one.

			if( m_pServer->m_CustHome ) {
				
				if( Name == "default.htm" ) {
					
					if( ServeCustom(Ctx, "/default.htm") ) {

						return;
						}
					}
				}

			// Ask the file library to serve the content.

			if( m_pServer->ReplyWithFile(Ctx, Full) ) {

				return;
				}

			// And if that doesn't work and if root directory
			// redirection is enabled, try again but working
			// from the user site directory.

			if( m_pServer->m_UserSite ) {
				
				if( m_pServer->m_UserRoot ) {

					if( ServeCustom(Ctx, Full) ) {

						return;
						}
					}
				}
			}

		// Are we accessing the user site? If so and if
		// it is enabled, try to serve the file contents.

		if( Path.Left(6) == "/user/" ) {

			if( m_pServer->m_UserSite ) {

				if( ServeCustom(Ctx, Full.Mid(5)) ) {

					return;
					}
				}
			}

		// Are we using the path mechanism to access one
		// of the log files? If so, check permissions and
		// enables and then serve the required file.

		if( Path.Left(7) == "/files/" ) {

			CString File = Full.Mid(6);

			if( m_pServer->AllowLogs(File) ) {

				if( ServeFile(Ctx, File, FALSE) ) {

					return;
					}
				}
			}


		// If we're accessing an ajax resource, call the
		// serve object to handle the appropriate request.

		if( Path == "/ajax/" ) {

			if( m_pServer->ExecuteAjax(Ctx, Name) ) {

				return;
				}
			}

		// No joy so return with a 404 reply code.
		
		Ctx.pReq->SetStatus(404);

		return;
		}

	// Are we handling a POST request?

	if( Ctx.pReq->GetVerb() == "POST" ) {

		// Offer the server the chance to handle.

		if( m_pServer->ReplyToPost(Ctx, Full) ) {

			return;
			}

		// No joy so return with a 404 reply code.

		Ctx.pReq->SetStatus(404);

		return;
		}

	// Unsuppoted verb.

	Ctx.pReq->SetStatus(405);
	}

BOOL CTheHttpServer::SkipAuth(CHttpServerRequest *pReq)
{
	// We don't apply HTTP security to the favorite
	// icon as many browsers ping it incessantly.

	if( pReq->GetPath() == "/favicon.ico" ) {

		return TRUE;
		}

 	return CHttpServer::SkipAuth(pReq);
	}

void CTheHttpServer::MakeSession(CHttpServerSession * &pSess)
{
	pSess = New CTheHttpServerSession( this,
					   m_pServer->GetWebBufferSize(),
					   m_pServer->GetWebBufferBits(),
					   m_nBuff
					   );
	}

void CTheHttpServer::KillSession(CHttpServerSession *pSess)
{
	delete pSess;
	}

// Implementation

CString CTheHttpServer::GetPath(CHttpServerRequest *pReq)
{
	CString Full = pReq->GetPath();

	if( Full == "/" ) {
		
		Full = "/default.htm";
		}

	return Full;
	}

BOOL CTheHttpServer::StripSession(CHttpServerRequest *pReq, CString &Text)
{
	CString Sess = pReq->GetParamString("session", "");

	if( Sess.GetLength() ) {

		UINT uPos = Text.Find("session=");

		UINT uLen = 8 + Sess.GetLength();

		if( Text[uPos+uLen] == '&' ) {

			Text.Delete(uPos, uLen+1);
			}
		else
			Text.Delete(uPos, uLen);

		if( Text.Right(1) == "?" ) {

			Text.Delete(Text.GetLength() - 1, 1);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CTheHttpServer::ServeCustom(CWebReqContext const &Ctx, CString Name)
{
	Name = "\\WEB" + Name;

	return ServeFile(Ctx, Name, TRUE);
	}

BOOL CTheHttpServer::ServeFile(CWebReqContext const &Ctx, CString Name, BOOL fExpire)
{
	CAutoFile File(Name, "r");

	if( File ) {

		time_t timeCreated = File.GetUnix();

		time_t timeExpires = fExpire ? time(NULL) + defExpire : 0;

		DWORD crc = CRC32(PCBYTE(PCTXT(Name)), Name.GetLength());

		CPrintf CacheTag("CFile:%8.8x:%8.8x", crc, timeCreated);

		if( !Ctx.pReq->IsUnchanged(timeCreated , CacheTag) ) {

			// We use a CString object to hold the buffer
			// even when the data is binary as it avoids
			// our having to copy it an extra time.

			UINT    uSize = File.GetSize();

			CString Text;

			Text.Expand(uSize+1);

			PBYTE  pData = PBYTE(PCTXT(Text));

			if( File.Read(pData, uSize) == uSize ) {

				File.Close();

				BOOL    fCache = TRUE;

				CString Type   = CFilename(Name).GetType();

				CString Mime   = m_pManager->GetMimeType(Type);

				if( Mime == "text/html" ) {

					if( m_pServer->m_SameOrigin ) {

						Ctx.pReq->AddReplyHeader("X-Frame-Options", "SAMEORIGIN");
					}

					Text.FixLength(uSize);

					m_pServer->ExpandAllElements(Ctx, Text, fCache);

					Ctx.pReq->SetReplyBody(Text);
					}
				else
					Ctx.pReq->SetReplyBody(pData, uSize);

				if( fCache ) {

					Ctx.pReq->AddCacheInfo(timeCreated, timeExpires, CacheTag);
					}

				Ctx.pReq->SetContentType(Mime);

				Ctx.pReq->SetStatus(200);

				return TRUE;
				}
			}
		else {
			File.Close();

			Ctx.pReq->AddCacheInfo(timeCreated, timeExpires, CacheTag);

			Ctx.pReq->SetStatus(304);

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
