
#include "Intern.hpp"

#include "EnhancedWebServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "../G3Comms/secure.hpp"

#include "StringTable.hpp"

#include "TheHttpServer.hpp"

#include "TheHttpServerSession.hpp"

#include "WebFileLibrary.hpp"

#include "WebRle.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Enhanced Web Server
//

// System Commands

CEnhancedWebServer::CSysCmd CEnhancedWebServer::m_Cmds[] = {

	/* 0 */	{ IDS_NET_CFG,    IDS_NET_DCFG,     "ip.config",        false,	true	},
	/* 1 */	{ IDS_NET_STS,    IDS_NET_DSTS,     "ip.status",        true,	true	},
	/* 2 */	{ IDS_NET_RTS,    IDS_NET_DRTS,     "ip.routes",        false,	true	},
	/* 3 */	{ IDS_NET_STS1,	  IDS_NET_DSTS1,    "eth0.all",         true,   true	},
	/* 4 */	{ IDS_NET_STS2,	  IDS_NET_DSTS2,    "eth1.all",         true,   true	},
};

// Externals

extern DWORD DispGetPaletteEntry(UINT uIndex);

// Instantiator

CWebBase * Create_WebServer(void)
{
	return New CEnhancedWebServer;
}

// Constructor

CEnhancedWebServer::CEnhancedWebServer(void)
{
	m_pOpts    = New CHttpServerOptions;

	m_pManager = NULL;

	m_pServer  = NULL;

	m_pLibrary = NULL;

	m_pSecure  = CCommsSystem::m_pThis->m_pSecure;

	m_pCtx     = NULL;

	CheckCmds();
}

// Destructor

CEnhancedWebServer::~CEnhancedWebServer(void)
{
	delete m_pOpts;
}

// Initialization

void CEnhancedWebServer::Load(PCBYTE &pData)
{
	ValidateLoad("CWebBase", pData);

	if( (m_Enable = GetByte(pData)) ) {

		m_DbTime = GetLong(pData);

		m_pList->Load(pData);

		m_pOpts->Load(pData);

		m_pOpts->DefaultPort();

		GetCoded(pData, m_pTitle);
		GetCoded(pData, m_pHeader);
		GetCoded(pData, m_pHome);

		m_CustHome      = GetByte(pData);
		m_CustIcon      = GetByte(pData);
		m_CustLogon     = GetByte(pData);
		m_CustCss       = GetByte(pData);
		m_CustJs        = GetByte(pData);

		m_RemoteView    = GetByte(pData);
		m_RemoteCtrl    = GetByte(pData);
		m_RemoteZoom    = GetByte(pData);
		m_RemoteFact    = GetByte(pData);
		m_RemoteBits    = GetByte(pData);
		m_RemoteSec     = GetLong(pData);

		m_LogsView      = GetByte(pData);
		m_LogsBatches   = GetByte(pData);
		m_LogsSec       = GetLong(pData);

		m_UserSite      = GetByte(pData);
		m_UserMenu      = GetByte(pData);
		m_UserRoot      = GetByte(pData);
		m_UserDelay     = GetWord(pData);
		m_UserSec       = GetLong(pData);

		m_SystemPages   = GetByte(pData);
		m_SystemConsole = GetByte(pData);
		m_SystemCapture = GetByte(pData);
		m_SystemJump    = GetByte(pData);
		m_SystemSec     = GetLong(pData);

		if( !(m_Source = GetByte(pData)) ) {

			GetCoded(pData, m_pUser);
			GetCoded(pData, m_pReal);
			GetCoded(pData, m_pPass);
		}
		else {
			m_Local = GetByte(pData);
		}

		m_Restrict = GetByte(pData);
		m_SecMask  = GetAddr(pData);
		m_SecAddr  = GetAddr(pData);

		if( m_Restrict == 2 ) {

			m_pOpts->m_AllowMask = m_SecMask;
			m_pOpts->m_AllowAddr = m_SecAddr;
		}

		GetCoded(pData, m_pPort);

		m_SameOrigin = GetByte(pData);
	}

	CWebBase::Load(pData);
}

// Attributes

UINT CEnhancedWebServer::GetDatabaseTime(void) const
{
	return m_DbTime;
}

UINT CEnhancedWebServer::GetWebBufferSize(void) const
{
	return m_cwRem;
}

UINT CEnhancedWebServer::GetWebBufferBits(void) const
{
	if( m_RemoteBits == 24 ) {

		return 32;
	}

	return m_RemoteBits;
}

BOOL CEnhancedWebServer::AllowLogs(CString &File)
{
	if( m_LogsView && AllowAccess(m_LogsSec) ) {

		// Check for drive prefix

		if( File.GetLength() > 3 ) {

			if( File[0] == '/' && File[2] == '-' ) {

				if( File[1] == 'C' || File[1] == 'D' || File[1] == 'E' ) {

					File.Delete(0, 1);

					File.SetAt(1, ':');

					CString Temp = File.Mid(2);

					return AllowLogs(Temp);
				}

				return FALSE;
			}
		}

		// Only allow access to suitable directories.

		if( File.StartsWith("/logs/") || File.StartsWith("/batch/") ) {

			// Detect any climbing back up the directory tree.

			if( File.Find("/../") == NOTHING && File.Find("\\..\\") == NOTHING ) {

				// And make sure only CSV files can be read.

				if( File.Right(4) == ".csv" ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

// Operations

CString CEnhancedWebServer::FindPass(CString User)
{
	if( m_Source ) {

		BOOL fAddUser = (m_Local == 0);

		return m_pSecure->GetPassword(User, 1 << 14, fAddUser);
	}
	else {
		if( GetItemData(m_pUser, "") == User ) {

			return GetItemData(m_pPass, "");
		}

		return "";
	}
}

CString CEnhancedWebServer::FindReal(CString User)
{
	PCTXT pDefault = "User Name";

	if( m_Source ) {

		UINT uIndex = m_pSecure->GetUserIndex(User);

		if( uIndex < NOTHING ) {

			return m_pSecure->GetUserRealName(uIndex);
		}
	}
	else {
		if( GetItemData(m_pUser, "") == User ) {

			return GetItemData(m_pReal, pDefault);
		}
	}

	return pDefault;
}

BOOL CEnhancedWebServer::CanAccessHtml(CString Name)
{
	if( Name == "/remote.htm" || (Name.StartsWith("/rv") && Name.EndsWith(".htm")) ) {

		return m_RemoteView && AllowAccess(m_RemoteSec);
	}

	if( Name.StartsWith("/sys") && Name.EndsWith(".htm") ) {

		if( m_SystemPages && AllowAccess(m_SystemSec) ) {

			if( Name == "/sysdebug.htm" ) {

				return m_SystemConsole ? TRUE : FALSE;
			}

			if( Name == "/syspcap.htm" ) {

				return m_SystemCapture ? TRUE : FALSE;
			}

			return TRUE;
		}

		return FALSE;
	}

	if( Name == "/logs.htm" ) {

		return m_LogsView && AllowAccess(m_LogsSec);
	}

	return TRUE;
}

BOOL CEnhancedWebServer::ExpandAllElements(CWebReqContext const &Ctx, CString &Text, BOOL &fCache)
{
	CWebReqContext const *pSave = m_pCtx;

	m_pCtx = &Ctx;

	ExpandNewElements(Ctx, Text, fCache);

	ExpandOldElements(Ctx, Text, fCache);

	m_pCtx = pSave;

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteAjax(CWebReqContext const &Ctx, CString Name)
{
	if( Name == "jump-runtime.htm" ) {

		Ctx.pCon->ClearKeepAlive();

		Ctx.pReq->AddReplyHeader("Location", CPrintf("/default.htm?session=%8.8X", Ctx.pSess));

		Ctx.pReq->SetStatus(302);

		return TRUE;
	}

	if( Name == "jump-config.htm" ) {

		Ctx.pCon->ClearKeepAlive();

		CString Link;

		if( g_pPxe->GetSysWebLink(Link) ) {

			CString From = Ctx.pReq->GetRequestHeader("Referrer");

			CString Full = Ctx.pReq->GetRequestHeader("Host");

			CString Host = Full.Left(Full.FindRev(':'));

			Ctx.pReq->AddReplyHeader("Location", "https://" + Host + Link);

			Ctx.pReq->SetStatus(302);

			return TRUE;
		}

		Ctx.pReq->SetStatus(404);

		return TRUE;
	}

	if( Name == "dataview-read.htm" ) {

		return ExecuteDataViewRead(Ctx);
	}

	if( Name == "dataview-write.htm" ) {

		return ExecuteDataViewWrite(Ctx);
	}

	if( Name == "remote-event.htm" ) {

		if( m_RemoteView && AllowAccess(m_RemoteSec) ) {

			return ExecuteRemoteEvent(Ctx);
		}

		Ctx.pReq->SetStatus(403);

		return TRUE;
	}

	if( Name == "remote-palette.blob" ) {

		if( m_RemoteView && AllowAccess(m_RemoteSec) ) {

			return ExecuteRemotePalette(Ctx);
		}

		Ctx.pReq->SetStatus(403);

		return TRUE;
	}

	if( Name == "remote-display.blob" ) {

		if( m_RemoteView && AllowAccess(m_RemoteSec) ) {

			switch( m_uBits ) {

				case  8:
					return ExecuteRemoteDisplay08(Ctx);

				case 16:
					return ExecuteRemoteDisplay16(Ctx);

				case 24:
					return ExecuteRemoteDisplay24(Ctx);
			}
		}

		Ctx.pReq->SetStatus(403);

		return TRUE;
	}

	if( Name == "remote-delta.blob" ) {

		if( m_RemoteView && AllowAccess(m_RemoteSec) ) {

			switch( m_uBits ) {

				case  8:
					return ExecuteRemoteDelta08(Ctx);

				case 16:
					return ExecuteRemoteDelta16(Ctx);

				case 24:
					return ExecuteRemoteDelta24(Ctx);
			}
		}

		Ctx.pReq->SetStatus(403);

		return TRUE;
	}

	if( Name == "syscmd-update.htm" ) {

		return ExecuteSysCmdUpdate(Ctx);
	}

	if( Name == "sysdebug-update.htm" ) {

		return ExecuteSysDebugUpdate(Ctx);
	}

	if( Name == "syspcap-status.htm" ) {

		return ExecuteSysPcapStatus(Ctx);
	}

	if( Name == "syspcap-control.htm" ) {

		return ExecuteSysPcapControl(Ctx);
	}

	if( Name == "syspcap-read.pcap" ) {

		return ExecuteSysPcapRead(Ctx);
	}

	return FALSE;
}

BOOL CEnhancedWebServer::ReplyWithFile(CWebReqContext const &Ctx, CString Name)
{
	return m_pLibrary->ReplyWithFile(Ctx, Name);
}

BOOL CEnhancedWebServer::ReplyToPost(CWebReqContext const &Ctx, CString Name)
{
	if( Name == "/scripts/write-tags" ) {

		return PostWriteTags(Ctx);
	}

	return FALSE;
}

// Task Entries

void CEnhancedWebServer::TaskInit(UINT uID)
{
	m_pList->PreRegister();

	m_pGDI     = CCommsSystem::m_pThis->GetGDI();

	m_pRemCols = New DWORD[256];

	for( UINT c = 0; c < 256; c++ ) {

		m_pRemCols[c] = m_pGDI->GetPaletteEntry(c);
	}

	m_cxRem = m_pGDI->GetCx();

	m_cyRem = m_pGDI->GetCy();

	m_cwRem = m_cxRem * m_cyRem;

	m_uBits = m_RemoteBits;

	g_pPxe->SetAppWebLink(CPrintf("%s:%u/ajax/jump-runtime.htm", m_pOpts->m_fTls ? "https" : "http", m_pOpts->m_uPort));
}

void CEnhancedWebServer::TaskExec(UINT uID)
{
	for( ;;) {

		BOOL fOpen = OpenServer();

		UINT uCert = g_pPxe->GetDefCertStep();

		for( ;;) {

			if( g_pPxe->GetDefCertStep() != uCert ) {

				if( m_pServer->IsIdle() ) {

					break;
				}
			}

			if( fOpen && !m_pServer->Service() ) {

				Sleep(5);
			}
			else
				ForceSleep(10);
		}

		CloseServer();
	}
}

void CEnhancedWebServer::TaskStop(UINT uID)
{
}

void CEnhancedWebServer::TaskTerm(UINT uID)
{
	CloseServer();

	delete[] m_pRemCols;
}

// Param Access

C3INT CEnhancedWebServer::GetWebParamInt(PCTXT pName)
{
	if( m_pCtx ) {

		return m_pCtx->pReq->GetParamDecimal(pName, 0);
	}

	return 0;
}

C3INT CEnhancedWebServer::GetWebParamHex(PCTXT pName)
{
	if( m_pCtx ) {

		return m_pCtx->pReq->GetParamHex(pName, 0);
	}

	return 0;
}

PUTF CEnhancedWebServer::GetWebParamStr(PCTXT pName)
{
	if( m_pCtx ) {

		return wstrdup(m_pCtx->pReq->GetParamString(pName, ""));
	}

	return wstrdup(L"");
}

PUTF CEnhancedWebServer::GetWebUser(UINT n)
{
	return wstrdup(L"");
}

void CEnhancedWebServer::ClearWebUsers(void)
{
}

// Server Management

BOOL CEnhancedWebServer::OpenServer(void)
{
	m_pManager = New CHttpServerManager;

	m_pManager->AddExtendedTypes();

	if( m_pManager->Open() ) {

		if( m_pOpts->m_fTls ) {

			BOOL fGood = FALSE;

			if( m_pOpts->m_uCertSource >= 100 ) {


			}

			if( !fGood ) {

				CByteArray Cert;
				CByteArray Priv;
				CString    Pass;

				if( g_pPxe->GetDefCertStep() ) {

					CByteArray Data[2];

					if( g_pPxe->GetDefCertData(Data, Pass) ) {

						m_pManager->LoadServerCert(Data[0].data(),
									   Data[0].size(),
									   Data[1].data(),
									   Data[1].size(),
									   Pass
						);
					}
				}
			}
		}

		if( m_pPort ) {

			m_pOpts->m_uPort = m_pPort->ExecVal();
		}

		GuardThread(TRUE);

		m_pServer  = New CTheHttpServer(this, m_pManager, *m_pOpts);

		m_pLibrary = New CWebFileLibrary(this, m_pManager);

		GuardThread(FALSE);

		return TRUE;
	}

	delete m_pManager;

	m_pManager = NULL;

	return FALSE;
}

BOOL CEnhancedWebServer::CloseServer(void)
{
	if( m_pManager ) {

		m_pServer->Term();

		delete m_pServer;

		delete m_pLibrary;

		delete m_pManager;

		return TRUE;
	}

	return FALSE;
}

// Element Expansion

BOOL CEnhancedWebServer::ExpandNewElements(CWebReqContext const &Ctx, CString &Text, BOOL &fCache)
{
	CTagRef *pRefs = NULL;

	for( UINT p = 0; p < 3; p++ ) {

		UINT uRefs = 0;

		UINT p0    = 0;

		for( ;;) {

			UINT p1 = Text.Find("<%", p0);

			if( p1 < NOTHING ) {

				UINT p2 = Text.Find("%>", p1);

				if( p2 < NOTHING ) {

					CString Name = Text.Mid(p1 + 2, p2 - p1 - 2);

					if( p == 0 ) {

						if( IsTagElement(Ctx, Name) ) {

							uRefs++;

							p0 = p2 + 2;
						}
						else {
							CString Data = GetElementData(Ctx, Name, fCache);

							Text = Text.Left(p1) + Data + Text.Mid(p2+2);

							p0   = p1 + Data.GetLength();
						}
					}

					if( p == 1 ) {

						if( IsTagElement(Ctx, Name) ) {

							CTagRef &Ref = pRefs[uRefs];

							GetTagElement(Ctx, Name, Ref);

							uRefs++;
						}

						p0 = p2 + 2;
					}

					if( p == 2 ) {

						if( IsTagElement(Ctx, Name) ) {

							CTagRef &Ref = pRefs[uRefs];

							CString Data;

							if( Ref.pTag ) {

								Data = UniConvert(Ref.pTag->GetAsText(Ref.uPos, fmtStd));

								Text = Text.Left(p1) + Data + Text.Mid(p2+2);

								p0   = p1 + Data.GetLength();
							}
							else
								p0 = p2 + 2;

							uRefs++;
						}
						else
							p0 = p2 + 2;
					}

					continue;
				}
			}

			break;
		}

		if( p == 0 ) {

			if( !uRefs ) {

				break;
			}

			pRefs = New CTagRef[uRefs];
		}

		if( p == 1 ) {

			WaitForTags(pRefs, uRefs);
		}

		if( p == 2 ) {

			delete[] pRefs;
		}
	}

	return TRUE;
}

BOOL CEnhancedWebServer::ExpandOldElements(CWebReqContext const &Ctx, CString &Text, BOOL &fCache)
{
	CTagRef *pRefs = NULL;

	for( UINT p = 0; p < 3; p++ ) {

		UINT uRefs = 0;

		UINT p0    = 0;

		for( ;;) {

			UINT p1 = Text.Find("[[", p0);

			if( p1 < NOTHING ) {

				UINT p2 = Text.Find("]]", p1);

				if( p2 < NOTHING ) {

					CString Name = Text.Mid(p1 + 2, p2 - p1 - 2);

					if( p == 0 ) {

						p0 = p2 + 2;
					}

					if( p == 1 ) {

						CTagRef &Ref = pRefs[uRefs];

						if( isdigit(Name[0]) ) {

							UINT uSep = Name.Find(':');

							Ref.pTag  = m_pSrc->GetItem(atoi(Name));

							Ref.uPos  = (uSep == NOTHING) ? 0 : atoi(Name.Mid(uSep+1));
						}
						else {
							UINT uSep = Name.Find(':');

							UINT uTag = m_pSrc->FindByName(UniConvert(Name.Left(uSep)));

							Ref.pTag  = m_pSrc->GetItem(uTag);

							Ref.uPos  = (uSep == NOTHING) ? 0 : atoi(Name.Mid(uSep+1));
						}

						p0 = p2 + 2;
					}

					if( p == 2 ) {

						CTagRef &Ref = pRefs[uRefs];

						CString Data;

						if( Ref.pTag ) {

							Data = UniConvert(Ref.pTag->GetAsText(Ref.uPos, fmtStd));

							Text = Text.Left(p1) + Data + Text.Mid(p2+2);

							p0   = p1 + Data.GetLength();
						}
						else
							p0 = p2 + 2;
					}

					uRefs++;

					continue;
				}
			}

			break;
		}

		if( p == 0 ) {

			if( !uRefs ) {

				break;
			}

			pRefs  = New CTagRef[uRefs];

			fCache = FALSE;
		}

		if( p == 1 ) {

			WaitForTags(pRefs, uRefs);
		}

		if( p == 2 ) {

			delete[] pRefs;
		}
	}

	return TRUE;
}

BOOL CEnhancedWebServer::IsTagElement(CWebReqContext const &Ctx, CString Name)
{
	return Name.StartsWith("=Tags.");
}

BOOL CEnhancedWebServer::GetTagElement(CWebReqContext const &Ctx, CString Name, CTagRef &Ref)
{
	Ref.pTag = NULL;

	Ref.uPos = 0;

	CString   Rest = Name.Mid(6);

	CString   Find = Rest.StripToken('[');

	UINT uTag = m_pSrc->FindByName(UniConvert(Find));

	Ref.pTag  = m_pSrc->GetItem(uTag);

	Ref.uPos  = atoi(Rest);

	return TRUE;
}

CString CEnhancedWebServer::GetElementData(CWebReqContext const &Ctx, CString Name, BOOL &fCache)
{
	if( Name[0] == '=' ) {

		CString Type = Name.StripToken('.');

		if( Type == "=String" ) {

			return GetWebString(atoi(Name));
		}

		if( Type == "=Global" ) {

			return ExpandGlobal(Ctx, Name);
		}

		if( Type == "=DataIndex" ) {

			return ExpandDataIndex(Ctx, Name);
		}

		if( Type == "=DataView" ) {

			return ExpandDataView(Ctx, Name);
		}

		if( Type == "=Logs" ) {

			fCache = FALSE;

			return ExpandLogsView(Ctx, Name);
		}

		if( Type == "=System" ) {

			fCache = FALSE;

			return ExpandSystem(Ctx, Name);
		}

		return CPrintf("!!Unknown Variable %s.%s!!",
			       PCTXT(Type)+1,
			       PCTXT(Name)
		);
	}
	else {
		CString Cmd = Name.StripToken(' ');

		if( Cmd == "Include" ) {

			CString Data = m_pLibrary->GetFileText(Ctx, Name);

			if( !Data.IsEmpty() ) {

				ExpandAllElements(Ctx, Data, fCache);

				return Data;
			}

			return CPrintf("!!Cannot Include %s!!",
				       PCTXT(Name)
			);
		}

		if( Cmd == "NoCache" ) {

			fCache = FALSE;

			return "";
		}

		return CPrintf("!!Unknown Command %s!!",
			       PCTXT(Cmd)
		);
	}
}

// Page Handlers

CString CEnhancedWebServer::ExpandGlobal(CWebReqContext const &Ctx, CString Name)
{
	CString d;

	if( Name == "Home" ) {

		d = GetItemData(m_pHome, "C3 Web+");

		return d;
	}

	if( Name == "Title" ) {

		d = GetItemData(m_pTitle, GetWebString(IDS_ENH_WEB_SERVER));

		return d;
	}

	if( Name == "PageHeader" ) {

		d = GetItemData(m_pHeader, GetWebString(IDS_WELCOME));

		return d;
	}

	if( Name == "RemoteUrl" ) {

		d = GetRemoteUrl();

		return d;
	}

	if( Name == "MainNavBar" ) {

		d += "<ul class='nav navbar-nav'>"
			"<span id='c3-navbar-head'></span>";

		AddListItem(d, Ctx.pReq, "/default.htm", GetWebString(IDS_HOME));

		d += "<span id='c3-navbar-home'></span>";

		if( CanAccessAtLeastOnePage() ) {

			if( Ctx.pReq->GetPath() == "/dataview.htm" ) {

				d += "<li class='dropdown active'>";
			}
			else
				d += "<li class='dropdown'>";

			d += "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>";

			d += GetWebString(IDS_DATA);

			d += "<span class='caret'></span></a><ul class='dropdown-menu'>";

			d += ExpandDataIndex(Ctx, "Dropdown");

			d += "</ul></li>";
		}

		if( m_LogsView ) {

			if( AllowAccess(m_LogsSec) ) {

				if( Ctx.pReq->GetPath() == "/logs.htm" && !Ctx.pReq->GetParamDecimal("m", 0) ) {

					d += "<li class='dropdown active'>";
				}
				else
					d += "<li class='dropdown'>";

				d += "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>";

				d += GetWebString(IDS_LOGS);

				d += "<span class='caret'></span></a><ul class='dropdown-menu'>";

				d += ExpandLogsView(Ctx, "Dropdown");

				d += "</ul></li>";

				if( m_LogsBatches ) {

					if( CDataLogger::m_pThis->m_EnableBatch ) {

						AddListItem(d, Ctx.pReq, "/logs.htm?m=1", GetWebString(IDS_BATCHES));
					}
				}
			}
		}

		if( m_RemoteView ) {

			if( AllowAccess(m_RemoteSec) ) {

				AddListItem(d, Ctx.pReq, GetRemoteUrl(), GetWebString(IDS_REMOTE));
			}
		}

		if( m_UserSite && m_UserMenu && !m_CustHome ) {

			if( AllowAccess(m_UserSec) ) {

				AddListItem(d, Ctx.pReq, "/user/default.htm", GetWebString(IDS_USER));
			}
		}

		if( m_SystemPages ) {

			if( AllowAccess(m_SystemSec) ) {

				if( Ctx.pReq->GetPath() == "/system.htm" ) {

					d += "<li class='dropdown active'>";
				}
				else
					d += "<li class='dropdown'>";

				d += "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>";

				d += GetWebString(IDS_SYSTEM);

				d += "<span class='caret'></span></a><ul class='dropdown-menu'>";

				d += ExpandSystem(Ctx, "Dropdown");

				d += "</ul></li>";
			}
		}

		d += "<span id='c3-navbar-tail'></span>";

		d += "</ul><ul class='nav navbar-nav navbar-right'>";

		if( ShowLogOff() ) {

			d += "<li class='dropdown'>"
				"<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>"
				"<span class='glyphicon glyphicon-user'></span>&nbsp;&nbsp;";

			d += Ctx.pSess->GetReal();

			d += "<span class='caret'></span></a><ul class='dropdown-menu'><li><a href='/logoff.htm'>";

			d += GetWebString(IDS_LOG_OFF);

			d += "</a></li>";
		}

		if( m_SystemJump ) {

			d += "<li>";

			d += "<a href='/ajax/jump-config.htm'>";

			d += "<span class='glyphicon glyphicon-play'></span>&nbsp;";

			d += "System";

			d += "</a></li>";
		}

		d += "</ul></li>";

		d += "</ul>";

		return d;
	}

	if( Name == "MainMenu" ) {

		if( CanAccessAtLeastOnePage() ) {

			d += "<tr><td><a href='/data.htm'>";

			d += GetWebString(IDS_VIEW_DATA);

			d += "</a></td><td>";

			d += GetWebString(IDS_VIEW_DATA_DESC);

			d += "</td></tr>\n";
		}

		if( m_LogsView ) {

			if( AllowAccess(m_LogsSec) ) {

				if( m_LogsBatches && CDataLogger::m_pThis->m_EnableBatch ) {

					d += "<tr><td><a href='/logs.htm'>";

					d += GetWebString(IDS_VIEW_LOGS);

					d += "</a></td><td>";

					d += GetWebString(IDS_VIEW_CONT_DESC);

					d += "</td></tr>\n";

					d += "<tr><td><a href='/logs.htm?m=1'>";

					d += GetWebString(IDS_VIEW_BATCHES);

					d += "</a></td><td>";

					d += GetWebString(IDS_VIEW_BATCHES_DESC);

					d += "</td></tr>\n";
				}
				else {
					d += "<tr><td><a href='/logs.htm'>";

					d += GetWebString(IDS_VIEW_LOGS);

					d += "</a></td><td>";

					d += GetWebString(IDS_VIEW_LOGS_DESC);

					d += "</td></tr>\n";
				}
			}
		}

		if( m_RemoteView ) {

			if( AllowAccess(m_RemoteSec) ) {

				if( m_RemoteCtrl && !Restrict(Ctx.pCon) ) {

					d.AppendPrintf("<tr>"
						       "<td><a href='%s'>%s</a></td>"
						       "<td>%s</td>"
						       "</tr>\n",
						       PCTXT(GetRemoteUrl()),
						       GetWebString(IDS_REMOTE_VIEW),
						       GetWebString(IDS_REMOTE_CTRL_DESC)
					);
				}
				else {
					d.AppendPrintf("<tr>"
						       "<td><a href='%s'>%s</a></td>"
						       "<td>%s</td>"
						       "</tr>\n",
						       PCTXT(GetRemoteUrl()),
						       GetWebString(IDS_REMOTE_VIEW),
						       GetWebString(IDS_REMOTE_VIEW_DESC)
					);
				}
			}
		}

		if( m_UserSite && m_UserMenu && !m_CustHome ) {

			if( AllowAccess(m_UserSec) ) {

				d += "<tr><td><a href='/user/default.htm'>";

				d += GetWebString(IDS_USER_SITE);

				d += "</a></td><td>";

				d += GetWebString(IDS_USER_SITE_DESC);

				d += "</td></tr>\n";
			}
		}

		if( m_SystemPages ) {

			if( AllowAccess(m_SystemSec) ) {

				d += "<tr><td><a href='/system.htm'>";

				d += GetWebString(IDS_SYSTEM_PAGES);

				d += "</a></td><td>";

				d += GetWebString(IDS_SYSTEM_DESC);

				d += "</td></tr>\n";
			}
		}

		if( d.IsEmpty() ) {

			d += "<p>";

			d += GetWebString(IDS_NO_PERMISSIONS);

			d += "</p>";
		}
		else {
			CString t;

			MakeOptionTable(t, "c3-menu", d);

			return t;
		}

		return d;
	}

	AfxTrace("Unknown Element Global.%s\n", PCTXT(Name));

	return Name;
}

CString CEnhancedWebServer::ExpandDataIndex(CWebReqContext const &Ctx, CString Name)
{
	CString d;

	if( Name == "Dropdown" ) {

		for( UINT n = 0; n < m_pList->m_uCount; n++ ) {

			CWebPage *pPage = m_pList->m_ppPage[n];

			if( pPage ) {

				if( !pPage->m_Hide ) {

					if( CanAccessPage(pPage) ) {

						CPrintf Link("/dataview.htm?page=%u", 1 + n);

						AddListItem(d, Ctx.pReq, Link, pPage->GetTitle());
					}
				}
			}
		}

		return d;
	}

	if( Name == "List" ) {

		for( UINT n = 0; n < m_pList->m_uCount; n++ ) {

			CWebPage *pPage = m_pList->m_ppPage[n];

			if( pPage ) {

				if( !pPage->m_Hide ) {

					if( CanAccessPage(pPage) ) {

						d.AppendPrintf("<tr><td><a href='/dataview.htm?page=%u'>%s</a></td>\n",
							       1 + n,
							       PCTXT(pPage->GetTitle())
						);
					}
				}
			}
		}

		return d;
	}

	AfxTrace("Unknown Element DataList.%s\n", PCTXT(Name));

	return Name;
}

CString CEnhancedWebServer::ExpandDataView(CWebReqContext const &Ctx, CString Name)
{
	CString d;

	UINT    uPage = Ctx.pReq->GetParamDecimal("page", 0);

	if( uPage >= 1 && uPage <= m_pList->m_uCount ) {

		CWebPage *pPage = m_pList->m_ppPage[uPage-1];

		if( pPage ) {

			if( !pPage->m_Hide ) {

				if( CanAccessPage(pPage) ) {

					if( Name == "Header" ) {

						d = pPage->GetTitle();

						return d;
					}

					if( Name == "TagHead" ) {

						BOOL fEditPage = pPage->m_Edit && !Restrict(Ctx.pCon);

						BOOL fEditTags = FALSE;

						if( fEditPage ) {

							for( UINT n = 0; n < pPage->m_uTags; n++ ) {

								CDataRef &Ref = (CDataRef &) pPage->m_pTags[n];

								UINT     uTag = Ref.t.m_Index;

								UINT     uPos = Ref.t.m_Array;

								CTag *   pTag = CCommsSystem::m_pThis->m_pTags->m_pTags->GetItem(uTag);

								if( pTag && CanEditTag(pTag) ) {

									fEditTags = TRUE;

									break;
								}
							}
						}

						d += "<tr><th>";

						d += GetWebString(IDS_NAME);

						d += "</th><th>";

						d += GetWebString(IDS_VALUE);

						d += "</th>";

						if( fEditTags ) {

							d += "<th>";

							d += GetWebString(IDS_EDITING);

							d += "</th>";
						}

						d += "</tr>\n";

						return d;
					}

					if( Name == "TagList" ) {

						BOOL fEditPage = pPage->m_Edit && !Restrict(Ctx.pCon);

						BOOL fEditTags = FALSE;

						for( UINT p = 0; p < 2; p++ ) {

							for( UINT n = 0; n < pPage->m_uTags; n++ ) {

								CDataRef &Ref = (CDataRef &) pPage->m_pTags[n];

								UINT     uTag = Ref.t.m_Index;

								UINT     uPos = Ref.t.m_Array;

								CTag *   pTag = CCommsSystem::m_pThis->m_pTags->m_pTags->GetItem(uTag);

								if( p == 0 ) {

									if( fEditPage ) {

										if( pTag && CanEditTag(pTag) ) {

											fEditTags = TRUE;

											break;
										}
									}
								}

								if( p == 1 ) {

									if( pTag ) {

										d += "<tr>";

										d.AppendPrintf("<td id='name%u'>%s</td>",
											       n,
											       PCTXT(UniConvert(pTag->GetLabel(uPos)))
										);

										d.AppendPrintf("<td id='data%u'>%s</td>",
											       n,
											       PCTXT(UniConvert(pTag->GetAsText(uPos, fmtStd)))
										);

										if( fEditTags ) {

											d += "<td>";

											if( CanEditTag(pTag) ) {

												d.AppendPrintf("<button id='button%u' type='button' class='btn btn-xs'>"
													       "%s"
													       "</button>",
													       n,
													       GetWebString(IDS_EDIT_DOTS)
												);

												d.AppendPrintf("<div id='form%u' style='display: none;'>%s</div>",
													       n,
													       PCTXT(pTag->GetStates())
												);
											}
											else {
												d.AppendPrintf("<button id='button%u' type='button' style='display: none;'>"
													       "</button>",
													       n
												);
											}

											d += "</td>";
										}

										d += "</tr>\n";
									}
								}
							}
						}

						return d;
					}

					AfxTrace("Unknown Element DataView.%s\n", PCTXT(Name));
				}
			}
		}
	}

	if( Name == "Header" ) {

		return "Invalid Page";
	}

	return d;
}

CString CEnhancedWebServer::ExpandLogsView(CWebReqContext const &Ctx, CString Name)
{
	CString d;

	if( Name == "Header" ) {

		BOOL    fUseBat = Ctx.pReq->GetParamDecimal("m", 0);

		CString BatName = Ctx.pReq->GetParamString("b", "");

		CString LogName = Ctx.pReq->GetParamString("l", "");

		if( LogName == "events" ) {

			LogName = GetWebString(IDS_EVENT_LOG);
		}

		if( LogName == "secure" ) {

			LogName = GetWebString(IDS_SECURITY_LOG);
		}

		if( fUseBat ) {

			if( BatName.IsEmpty() ) {

				return GetWebString(IDS_LIST_BATCHES);
			}

			if( LogName.IsEmpty() ) {

				return CPrintf(GetWebString(IDS_LIST_LOGS_IN),
					       PCTXT(BatName)
				);
			}

			return CPrintf(GetWebString(IDS_LIST_FILES_IN_IN),
				       PCTXT(LogName),
				       PCTXT(BatName)
			);
		}

		if( LogName.IsEmpty() ) {

			return GetWebString(IDS_LIST_LOGS);
		}

		return CPrintf(GetWebString(IDS_LIST_FILES_IN),
			       PCTXT(LogName)
		);
	}

	if( Name == "List" ) {

		BOOL    fUseBat  = Ctx.pReq->GetParamDecimal("m", 0);

		CString BatName  = Ctx.pReq->GetParamString("b", "");

		CString LogName  = Ctx.pReq->GetParamString("l", "");

		CString LogPath  = Ctx.pReq->GetParamString("p", LogName);

		UINT    LogDrive = Ctx.pReq->GetParamDecimal("d", 0);

		if( !LogName.IsEmpty() && (!fUseBat || !BatName.IsEmpty()) ) {
			
			if( LogPath == "events" ) {

				LogPath.MakeUpper();
			}

			if( fUseBat ) {

				CString Path = BatName + "\\" + LogPath;

				d = ExpandLogsFiles(CPrintf("%c:\\BATCH\\%s", 'C' + CDataLogger::m_pThis->m_BatchDrive, PCSTR(Path)));
				}
			else {
				d = ExpandLogsFiles(CPrintf("%c:\\LOGS\\%s", 'C' + LogDrive, PCSTR(LogPath)));
			}
			
			if( !d.IsEmpty() ) {

				CString t;

				t += "<table class='table'><thead><tr><th>";

				t += GetWebString(IDS_NAME);

				t += "</th><th>";

				t += GetWebString(IDS_SIZE);

				t += "</th></tr></thead><tbody>";

				t += d;

				t += "</tbody></table>";

				return t;
			}

			return CPrintf("<p>%s</p>", GetWebString(IDS_NO_DATA));
		}
		else {
			if( fUseBat ) {

				if( !BatName.IsEmpty() ) {

					d = ExpandLogsNames(TRUE, "m=1&b=" + BatName + "&l=%s&p=%s&d=%u");
				}
				else
					d = ExpandLogsDirs(CPrintf("%c:\\BATCH", 'C' + CDataLogger::m_pThis->m_BatchDrive), "m=1&b=%s&d=%u", TRUE);
			}
			else
				d = ExpandLogsNames(FALSE, "l=%s&p=%s&d=%u");

			if( !d.IsEmpty() ) {

				CString t;

				t += "<table class='table'><thead><tr><th>";

				t += GetWebString(IDS_NAME);

				t += "</th></tr></thead><tbody>";

				t += d;

				t += "</tbody></table>";

				return t;
			}

			return CPrintf("<p>%s</p>", GetWebString(IDS_NO_DATA));
		}
	}

	if( Name == "Back" ) {

		BOOL    fUseBat = Ctx.pReq->GetParamDecimal("m", 0);

		CString BatName = Ctx.pReq->GetParamString("b", "");

		CString LogName = Ctx.pReq->GetParamString("l", "");

		if( fUseBat ) {

			if( !BatName.IsEmpty() ) {

				if( !LogName.IsEmpty() ) {

					d = "/logs.htm?m=1&b=" + BatName;
				}
				else
					d = "/logs.htm?m=1";
			}
		}
		else {
			if( !LogName.IsEmpty() ) {

				d = "/logs.htm";
			}
		}

		if( !d.IsEmpty() ) {

			CPrintf a("<a href='%s'>%s</a>", PCTXT(d), GetWebString(IDS_BACK));

			return a;
		}
	}

	if( Name == "Dropdown" ) {

		CString d;

		CDataLogList *pList = CDataLogger::m_pThis->m_pLogs;

		for( UINT n = 0; n < pList->m_uCount; n++ ) {

			CDataLog *pLog = pList->m_ppLog[n];

			if( pLog ) {

				CPrintf Link("/logs.htm?l=%s&p=%s&d=%u", PCTXT(pLog->m_Name), PCTXT(pLog->m_Path), pLog->m_Drive);

				AddListItem(d, Ctx.pReq, Link, pLog->m_Name);
			}
		}

		if( CEventLogger::m_pThis->m_LogToDisk ) {

			CPrintf Link("/logs.htm?l=%s&d=%u", "events", CEventLogger::m_pThis->m_Drive);

			AddListItem(d, Ctx.pReq, Link, GetWebString(IDS_EVENTS));
		}

		if( CCommsSystem::m_pThis->m_pSecure->m_LogEnable ) {

			CPrintf Link("/logs.htm?l=%s&d=%u", "secure", CCommsSystem::m_pThis->m_pSecure->m_Drive);

			AddListItem(d, Ctx.pReq, Link, GetWebString(IDS_SECURITY));
		}

		return d;
	}

	return d;
}

CString CEnhancedWebServer::ExpandLogsNames(BOOL fBatch, CString Link)
{
	CString d;

	CDataLogList *pList = CDataLogger::m_pThis->m_pLogs;

	for( UINT n = 0; n < pList->m_uCount; n++ ) {

		CDataLog *pLog = pList->m_ppLog[n];

		if( pLog ) {

			if( !fBatch || pLog->m_WithBatch ) {

				CPrintf a(Link, PCTXT(pLog->m_Name), PCTXT(pLog->m_Path), fBatch ? CDataLogger::m_pThis->m_BatchDrive : pLog->m_Drive);

				d.AppendPrintf("<tr><td><a href='/logs.htm?%s'>%s</a></td></tr>",
					       PCTXT(a),
					       PCTXT(pLog->m_Name)
				);
			}
		}
	}

	if( CEventLogger::m_pThis->m_LogToDisk ) {

		if( !fBatch || CEventLogger::m_pThis->m_WithBatch ) {

			CPrintf a(Link, PCTXT("events"), PCTXT("EVENTS"), fBatch ? CDataLogger::m_pThis->m_BatchDrive : CEventLogger::m_pThis->m_Drive);

			d.AppendPrintf("<tr><td><a href='/logs.htm?%s'>%s</a></td></tr>",
				       PCTXT(a),
				       GetWebString(IDS_EVENTS)
			);
		}
	}

	if( CCommsSystem::m_pThis->m_pSecure->m_LogEnable ) {

		if( !fBatch || CCommsSystem::m_pThis->m_pSecure->m_WithBatch ) {

			CPrintf a(Link, PCTXT("secure"), PCTXT("SECURE"), fBatch ? CDataLogger::m_pThis->m_BatchDrive : CCommsSystem::m_pThis->m_pSecure->m_Drive);

			d.AppendPrintf("<tr><td><a href='/logs.htm?%s'>%s</a></td></tr>",
				       PCTXT(a),
				       GetWebString(IDS_SECURITY)
			);
		}
	}

	return d;
}

CString CEnhancedWebServer::ExpandLogsDirs(CString Path, CString Link, BOOL fTime)
{
	if( !chdir(Path) ) {

		CAutoDirentList List;

		if( fTime ? List.ScanDirsByTimeFwd(".") : List.ScanDirs(".") ) {

			CString d;

			for( UINT n = 0; n < List.GetCount(); n++ ) {

				CFilename Name = List[n]->d_name;

				CPrintf a(Link, PCTXT(Name));

				d.AppendPrintf("<tr><td><a href='/logs.htm?%s'>%s</a></td></tr>", PCTXT(a), PCTXT(Name));
			}

			return d;
		}
	}

	return "";
}

CString CEnhancedWebServer::ExpandLogsFiles(CString Path)
{
	if( !chdir(Path) ) {

		if( Path[1] == ':' ) {

			Path.SetAt(1, '-');
		}

		CAutoDirentList List;

		if( List.ScanFilesByTimeFwd(".") ) {

			CString d;

			for( UINT n = 0; n < List.GetCount(); n++ ) {

				CFilename Name = List[n]->d_name;

				if( !stricmp(Name.GetType(), "csv") ) {

					CString Link = "\\files\\" + Path + "\\" + CString(Name);

					char full[MAX_PATH];

					PathMakeAbsolute(full, Name);

					struct stat s;

					stat(full, &s);

					d.AppendPrintf("<tr><td><a href='%s'>%s</a></td><td>%u</td></tr>", PCTXT(Link), PCTXT(Name), s.st_size);
				}
			}

			return d;
		}
	}

	return "";
}

CString CEnhancedWebServer::ExpandSystem(CWebReqContext const &Ctx, CString Name)
{
	CString d;

	if( Name == "Dropdown" ) {

		if( m_SystemPages ) {

			if( AllowAccess(m_SystemSec) ) {

				if( m_SystemConsole ) {

					AddListItem(d, Ctx.pReq, CPrintf("/sysdebug.htm?m=%u", m_SystemConsole), GetWebString(IDS_DBG_CONSOLE));
				}

				if( m_SystemCapture ) {

					AddListItem(d, Ctx.pReq, "/syspcap.htm", GetWebString(IDS_PCAP_NAME));
				}

				for( int n = 0; n < elements(m_Cmds); n++ ) {

					CSysCmd const &Cmd = m_Cmds[n];

					if( Cmd.fEn ) {

						AddListItem(d, Ctx.pReq, CPrintf("/syscmd.htm?cmd=%u&rt=%u", n, Cmd.fRt), GetWebString(Cmd.uName));
					}
				}
			}
		}

		return d;
	}

	if( Name == "Menu" ) {

		if( m_SystemPages ) {

			if( AllowAccess(m_SystemSec) ) {

				if( m_SystemConsole ) {

					d.AppendPrintf("<tr><td><a href='/sysdebug.htm?m=%u'>%s</a></td><td>%s<td></tr>\n",
						       m_SystemConsole,
						       GetWebString(IDS_DBG_CONSOLE),
						       GetWebString(IDS_DBG_INTERACT)
					);
				}

				if( m_SystemCapture ) {

					d.AppendPrintf("<tr><td><a href='/syspcap.htm'>%s</a></td><td>%s<td></tr>\n",
						       GetWebString(IDS_PCAP_NAME),
						       GetWebString(IDS_PCAP_DESC)
					);
				}

				for( int n = 0; n < elements(m_Cmds); n++ ) {

					CSysCmd const &Cmd = m_Cmds[n];

					if( Cmd.fEn ) {

						d.AppendPrintf("<tr><td><a href='/syscmd.htm?cmd=%u&rt=%u'>%s</a></td><td>%s<td></tr>\n",
							       n,
							       Cmd.fRt,
							       GetWebString(Cmd.uName),
							       GetWebString(Cmd.uDesc)
						);
					}
				}
			}
		}

		CString t;

		MakeOptionTable(t, NULL, d);

		return t;
	}

	if( Name == "CmdHead" ) {

		if( m_SystemPages ) {

			if( AllowAccess(m_SystemSec) ) {

				UINT  n = Ctx.pReq->GetParamDecimal("Cmd", NOTHING);

				if( n < elements(m_Cmds) ) {

					return GetWebString(m_Cmds[n].uName);
				}

				return "Invalid Command";
			}
		}
	}

	if( Name == "CmdBody" ) {

		if( m_SystemPages ) {

			if( AllowAccess(m_SystemSec) ) {

				UINT  n = Ctx.pReq->GetParamDecimal("Cmd", NOTHING);

				char *p = NULL;

				if( RunCommand(&p, n) ) {

					CString t(p);

					free(p);

					return t;
				}

				return "The command is invalid.";
			}
		}
	}

	AfxTrace("Unknown Element System.%s\n", PCTXT(Name));

	return Name;
}

// Comparison Helpers

STRONG_INLINE int CEnhancedWebServer::cmpl(PCBYTE p1, PCBYTE p2, int n)
{
	int i = 0;

	p1 += 0;
	p2 += 0;

	while( n-- && *p1++ == *p2++ ) i++;

	return i;
}

STRONG_INLINE int CEnhancedWebServer::cmpr(PCBYTE p1, PCBYTE p2, int n)
{
	int i = 0;

	p1 += n;
	p2 += n;

	while( n-- && *--p1 == *--p2 ) i++;

	return i;
}

STRONG_INLINE int CEnhancedWebServer::cmpl(PCWORD p1, PCWORD p2, int n)
{
	int i = 0;

	p1 += 0;
	p2 += 0;

	while( n-- && *p1++ == *p2++ ) i++;

	return i;
}

STRONG_INLINE int CEnhancedWebServer::cmpr(PCWORD p1, PCWORD p2, int n)
{
	int i = 0;

	p1 += n;
	p2 += n;

	while( n-- && *--p1 == *--p2 ) i++;

	return i;
}

STRONG_INLINE int CEnhancedWebServer::cmpl(PCDWORD p1, PCDWORD p2, int n)
{
	int i = 0;

	p1 += 0;
	p2 += 0;

	while( n-- && *p1++ == *p2++ ) i++;

	return i;
}

STRONG_INLINE int CEnhancedWebServer::cmpr(PCDWORD p1, PCDWORD p2, int n)
{
	int i = 0;

	p1 += n;
	p2 += n;

	while( n-- && *--p1 == *--p2 ) i++;

	return i;
}

// Form Submission

BOOL CEnhancedWebServer::PostWriteTags(CWebReqContext const &Ctx)
{
	CZeroMap <CString, CString> Vars;

	ParseUrlEncodedBody(Vars, PCTXT(Ctx.pReq->GetRequestBody()));

	CString Back  = Vars["back"];

	UINT    uPage = atoi(Vars["page"]);

	BOOL    fOkay = TRUE;

	for( UINT t = 1;; t++ ) {

		CString const &Tag = Vars[CPrintf("Tag%u", t)];

		CString const &Pos = Vars[CPrintf("Pos%u", t)];

		CString const &Val = Vars[CPrintf("Data%u", t)];

		if( !Tag.IsEmpty() ) {

			UINT uTag = atoi(Tag);

			UINT uPos = 0;

			if( !Pos.IsEmpty() ) {

				uPos = atoi(Pos);
			}
			else {
				UINT c = Tag.Find(':');

				if( c < NOTHING ) {

					uPos = atoi(PCTXT(Tag) + c + 1);
				}
			}

			if( CanEditTag(uPage, uTag) ) {

				CTag *pTag = m_pSrc->GetItem(uTag);

				if( pTag ) {

					if( !pTag->SetAsText(uPos, Val) ) {

						fOkay = FALSE;
					}
				}
			}
			else
				fOkay = FALSE;

			continue;
		}

		break;
	}

	if( Back.IsEmpty() ) {

		Back = "/default.htm";
	}

	if( !fOkay ) {

		if( atoi(Vars["silent"]) ) {

			Ctx.pReq->SetStatus(204);

			return TRUE;
		}

		Redirect(Ctx, "/writefail.htm?back=" + Back);

		return TRUE;
	}

	Redirect(Ctx, "/writeokay.htm?back=" + Back);

	return TRUE;
}

void CEnhancedWebServer::Redirect(CWebReqContext const &Ctx, CString Page)
{
	CString Body;

	CString Sess = Ctx.pReq->GetParamString("session", "");

	if( Sess.IsEmpty() ) {

		// Fall back to the session we established
		// via cookies if we don't have the parameter
		// to tell us. I think there are some cases
		// where this won't work...

		Sess = Ctx.pSess->GetOpaque().Left(8);
	}

	Page += "&session=" + Sess;

	Body += "<!DOCTYPE html>\r\n";

	Body += "<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n";

	Body += "<script type='text/javascript'>window.location='" + Page + "'</script>";

	Body += "</html>\r\n";

	Ctx.pReq->SetReplyBody(Body);

	Ctx.pReq->SetStatus(200);
}

BOOL CEnhancedWebServer::ParseUrlEncodedBody(CZeroMap <CString, CString> &Vars, CString Body)
{
	CStringArray List;

	Body.Tokenize(List, '&');

	UINT n;

	for( n = 0; n < List.GetCount(); n++ ) {

		CString Data = List[n];

		CString Name = Data.StripToken('=');

		Data = CHttpUrlEncoding::Decode(Data);

		Vars.Insert(Name, Data);
	}

	return n > 0;
}

// Ajax Handlers

BOOL CEnhancedWebServer::ExecuteDataViewRead(CWebReqContext const &Ctx)
{
	UINT uPage = Ctx.pReq->GetParamDecimal("page", 0);

	if( uPage >= 1 && uPage <= m_pList->m_uCount ) {

		CWebPage *pPage = m_pList->m_ppPage[uPage-1];

		if( pPage ) {

			if( CanAccessPage(pPage) ) {

				// TODO -- This is an example of where a multithreaded
				// server would do a lot better. We are going to block
				// at this point waiting for data, and all web requests
				// are going to be delayed in the meantime...

				pPage->SetScan(scanOnce);

				pPage->WaitAvailable();

				CString d;

				for( UINT n = 0; n < pPage->m_uTags; n++ ) {

					CDataRef &Ref = (CDataRef &) pPage->m_pTags[n];

					UINT     uTag = Ref.t.m_Index;

					UINT     uPos = Ref.t.m_Array;

					CTag *   pTag = CCommsSystem::m_pThis->m_pTags->m_pTags->GetItem(uTag);

					d += UniConvert(pTag->GetAsText(uPos, fmtStd));

					if( pPage->m_Colors ) {

						DWORD Pair = pTag->GetColorPair(0);

						DWORD Fore = GetWinColor(LOWORD(Pair));

						DWORD Back = GetWinColor(HIWORD(Pair));

						d.AppendPrintf("\t#%6.6X\t#%6.6X", Fore, Back);
					}

					d += "\n";
				}

				Ctx.pReq->SetReplyBody(d);

				Ctx.pReq->SetContentType("text/plain");

				Ctx.pReq->SetStatus(pPage->m_Refresh ? 200 : 222);

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CEnhancedWebServer::ExecuteDataViewWrite(CWebReqContext const &Ctx)
{
	UINT uPage = Ctx.pReq->GetParamDecimal("page", 0);

	if( uPage && uPage <= m_pList->m_uCount ) {

		CWebPage *pPage = m_pList->m_ppPage[uPage-1];

		if( pPage ) {

			if( CanAccessPage(pPage) ) {

				UINT n = Ctx.pReq->GetParamDecimal("tag", NOTHING);

				if( n < pPage->m_uTags ) {

					CDataRef &Ref = (CDataRef &) pPage->m_pTags[n];

					UINT     uTag = Ref.t.m_Index;

					UINT     uPos = Ref.t.m_Array;

					CTag *   pTag = CCommsSystem::m_pThis->m_pTags->m_pTags->GetItem(uTag);

					if( pTag ) {

						CString Name = UniConvert(pTag->GetLabel(uPos));

						if( pPage->m_Edit && !Restrict(Ctx.pCon) ) {

							CString Prev = UniConvert(pTag->GetAsText(uPos, fmtStd));

							CString Data = Ctx.pReq->GetParamString("data", "");

							if( !strcmp(Prev, Data) ) {

								Ctx.pReq->SetStatus(200);

								Ctx.pReq->SetContentType("text/plain");

								return TRUE;
							}

							if( pTag->SetAsText(uPos, Data) ) {

								CPrintf Text(GetWebString(IDS_X_SET_TO_Y),
									     PCTXT(Name),
									     PCTXT(Data)
								);

								Ctx.pReq->SetReplyBody(Text);

								Ctx.pReq->SetStatus(200);

								Ctx.pReq->SetContentType("text/plain");

								return TRUE;
							}
						}

						CPrintf Text(GetWebString(IDS_FAILED_WRITE_X),
							     PCTXT(Name)
						);

						Ctx.pReq->SetReplyBody(Text);

						Ctx.pReq->SetStatus(403);

						Ctx.pReq->SetContentType("text/plain");
					}
					else {
						Ctx.pReq->SetReplyBody(GetWebString(IDS_FAILED_WRITE_TAG));

						Ctx.pReq->SetStatus(403);

						Ctx.pReq->SetContentType("text/plain");
					}

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CEnhancedWebServer::ExecuteSysCmdUpdate(CWebReqContext const &Ctx)
{
	if( m_SystemPages ) {

		if( AllowAccess(m_SystemSec) ) {

			UINT  n = Ctx.pReq->GetParamDecimal("Cmd", NOTHING);

			char *p = NULL;

			if( RunCommand(&p, n) ) {

				Ctx.pReq->SetReplyBody(p);

				Ctx.pReq->SetStatus(200);

				Ctx.pReq->SetContentType("text/html");

				free(p);

				return TRUE;
			}

			Ctx.pReq->SetStatus(404);

			return TRUE;
		}
	}

	Ctx.pReq->SetStatus(403);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteSysDebugUpdate(CWebReqContext const &Ctx)
{
	if( m_SystemPages && m_SystemConsole ) {

		if( AllowAccess(m_SystemSec) ) {

			BOOL    fInit = Ctx.pReq->GetParamDecimal("init", 0);

			CString Text;

			if( fInit ) {

				Ctx.pSess->m_Console.Enable(TRUE);

				Ctx.pSess->m_Console.Read(Text);

				Ctx.pSess->m_Console.Exec("auto-hello");
			}
			else {
				if( m_SystemConsole == 2 ) {

					CString Cmd = Ctx.pReq->GetParamString("cmd", "");

					if( !Cmd.IsEmpty() ) {

						if( Cmd == "help" ) {

							Cmd = "diag.help";
						}

						Ctx.pSess->m_Console.Exec(Cmd);
					}
				}
			}

			Ctx.pSess->m_Console.Read(Text);

			Ctx.pReq->SetReplyBody(Text);

			Ctx.pReq->SetStatus(200);

			Ctx.pReq->SetContentType("text/html");

			return TRUE;
		}
	}

	Ctx.pReq->SetStatus(403);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteSysPcapStatus(CWebReqContext const &Ctx)
{
	if( m_SystemPages && m_SystemCapture ) {

		if( AllowAccess(m_SystemSec) ) {

			IPacketCapture *pCap = NULL;

			UINT            uCap = Ctx.pReq->GetParamDecimal("src", 0);

			#if defined(AEON_ENVIRONMENT)

			AfxGetObject("net.pcap", uCap, IPacketCapture, pCap);

			#else

			pCap = AfxGetDevice(IPacketCapture, uCap);

			#endif

			if( pCap ) {

				CPrintf Text("1,%s,%u,%u",
					     pCap->GetCaptureFilter(),
					     pCap->IsCaptureRunning(),
					     pCap->GetCaptureSize()
				);

				Ctx.pReq->SetReplyBody(Text);

				#if defined(AEON_ENVIRONMENT)

				pCap->Release();

				#endif
			}
			else
				Ctx.pReq->SetReplyBody("0");

			Ctx.pReq->SetStatus(200);

			Ctx.pReq->SetContentType("text/html");

			return TRUE;
		}
	}

	Ctx.pReq->SetStatus(403);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteSysPcapControl(CWebReqContext const &Ctx)
{
	if( m_SystemPages && m_SystemCapture ) {

		if( AllowAccess(m_SystemSec) ) {

			IPacketCapture *pCap = NULL;

			UINT            uCap = Ctx.pReq->GetParamDecimal("src", 0);

			#if defined(AEON_ENVIRONMENT)

			AfxGetObject("net.pcap", uCap, IPacketCapture, pCap);

			#else

			pCap = AfxGetDevice(IPacketCapture, uCap);

			#endif

			if( pCap ) {

				CString Cmd = Ctx.pReq->GetParamString("action", "none");

				if( Cmd == "start" ) {

					CString Filter = Ctx.pReq->GetParamString("filter", "none");

					pCap->StartCapture(Filter);
				}

				if( Cmd == "stop" ) {

					pCap->StopCapture();
				}

				CPrintf Text("1,%s,%u,%u",
					     pCap->GetCaptureFilter(),
					     pCap->IsCaptureRunning(),
					     pCap->GetCaptureSize()
				);

				Ctx.pReq->SetReplyBody(Text);

				#if defined(AEON_ENVIRONMENT)

				pCap->Release();

				#endif
			}
			else
				Ctx.pReq->SetReplyBody("0");

			Ctx.pReq->SetStatus(200);

			Ctx.pReq->SetContentType("text/html");

			return TRUE;
		}
	}

	Ctx.pReq->SetStatus(403);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteSysPcapRead(CWebReqContext const &Ctx)
{
	if( m_SystemPages && m_SystemCapture ) {

		if( AllowAccess(m_SystemSec) ) {

			IPacketCapture *pCap = NULL;

			UINT            uCap = Ctx.pReq->GetParamDecimal("src", 0);

			#if defined(AEON_ENVIRONMENT)

			AfxGetObject("net.pcap", uCap, IPacketCapture, pCap);

			#else

			pCap = AfxGetDevice(IPacketCapture, uCap);

			#endif

			if( pCap ) {

				UINT uSize = pCap->GetCaptureSize();

				if( uSize ) {

					CByteArray Data;

					Data.SetCount(uSize);

					pCap->CopyCapture(PBYTE(Data.GetPointer()), uSize);

					Ctx.pReq->SetReplyBody(Data);

					Ctx.pReq->SetStatus(200);

					Ctx.pReq->SetContentType("application/vnd.tcpdump.pcap");

					#if defined(AEON_ENVIRONMENT)

					pCap->Release();

					#endif

					return TRUE;
				}

				#if defined(AEON_ENVIRONMENT)

				pCap->Release();

				#endif
			}

			Ctx.pReq->SetStatus(404);

			return TRUE;
		}
	}

	Ctx.pReq->SetStatus(403);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteRemoteEvent(CWebReqContext const &Ctx)
{
	if( !Restrict(Ctx.pCon) ) {

		UINT uType = Ctx.pReq->GetParamDecimal("t", NOTHING);

		if( uType == 0 ) {

			int c = Ctx.pReq->GetParamDecimal("c", 0);

			if( c ) {

				FakeKey(stateDown, c);

				FakeKey(stateUp, c);
			}
		}

		if( uType == 1 ) {

			int x = Ctx.pReq->GetParamDecimal("x", 0);

			int y = Ctx.pReq->GetParamDecimal("y", 0);

			if( x >= 0 && x < m_cxRem && y >= 0 && y < m_cyRem ) {

				FakeTouch(stateDown, x, y);

				FakeTouch(stateUp, x, y);
			}
		}
	}

	Ctx.pReq->SetReplyBody("OK");

	Ctx.pReq->SetContentType("text/plain");

	Ctx.pReq->SetStatus(200);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteRemotePalette(CWebReqContext const &Ctx)
{
	if( Ctx.pSess->AllocBuffers() ) {

		PBYTE pData = Ctx.pSess->m_pWork;

		UINT  uData = 0;

		for( UINT n = 0; n < 256; n++ ) {

			DWORD c = m_pRemCols[n];

			pData[uData++] = LOBYTE(HIWORD(c));
			pData[uData++] = HIBYTE(LOWORD(c));
			pData[uData++] = LOBYTE(LOWORD(c));
			pData[uData++] = 0xFF;
		}

		Ctx.pReq->SetReplyBody(pData, uData);

		Ctx.pReq->SetContentType("application/binary");

		Ctx.pReq->SetStatus(200);

		return TRUE;
	}

	Ctx.pReq->SetStatus(503);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteRemoteDisplay08(CWebReqContext const &Ctx)
{
	if( Ctx.pSess->AllocBuffers() ) {

		PBYTE pb1 = PBYTE(Ctx.pSess->m_pBuff1);

		PBYTE pb2 = PBYTE(Ctx.pSess->m_pBuff2);

		m_pGDI->Render(PBYTE(Ctx.pSess->m_pBuff1), MAKEWORD(8, 1));

		MakeDisplayBlob08(Ctx.pReq, Ctx.pSess->m_pWork, pb1, 0, m_cyRem, 0, m_cxRem);

		memcpy(pb2, pb1, sizeof(*pb1) * m_cwRem);

		return TRUE;
	}

	Ctx.pReq->SetStatus(503);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteRemoteDisplay16(CWebReqContext const &Ctx)
{
	if( Ctx.pSess->AllocBuffers() ) {

		PWORD pb1 = PWORD(Ctx.pSess->m_pBuff1);

		PWORD pb2 = PWORD(Ctx.pSess->m_pBuff2);

		m_pGDI->Render(PBYTE(Ctx.pSess->m_pBuff1), MAKEWORD(16, 1));

		MakeDisplayBlob16(Ctx.pReq, Ctx.pSess->m_pWork, pb1, 0, m_cyRem, 0, m_cxRem);

		memcpy(pb2, pb1, sizeof(*pb1) * m_cwRem);

		return TRUE;
	}

	Ctx.pReq->SetStatus(503);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteRemoteDisplay24(CWebReqContext const &Ctx)
{
	if( Ctx.pSess->AllocBuffers() ) {

		PDWORD pb1 = PDWORD(Ctx.pSess->m_pBuff1);

		PDWORD pb2 = PDWORD(Ctx.pSess->m_pBuff2);

		m_pGDI->Render(PBYTE(Ctx.pSess->m_pBuff1), MAKEWORD(32, 1));

		MakeDisplayBlob24(Ctx.pReq, Ctx.pSess->m_pWork, pb1, 0, m_cyRem, 0, m_cxRem);

		memcpy(pb2, pb1, sizeof(*pb1) * m_cwRem);

		return TRUE;
	}

	Ctx.pReq->SetStatus(503);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteRemoteDelta08(CWebReqContext const &Ctx)
{
	if( Ctx.pSess->AllocBuffers() ) {

		Ctx.pSess->Throttle(100);

		m_pGDI->Render(PBYTE(Ctx.pSess->m_pBuff1), MAKEWORD(8, 1));

		PBYTE pComp = Ctx.pSess->m_pWork + 1 * m_cwRem;

		PBYTE pInit = pComp;

		int    y1   = -1;

		int    s1   = -1;

		int    s2   = -1;

		PBYTE  pb1  = PBYTE(Ctx.pSess->m_pBuff1);

		PBYTE  pb2  = PBYTE(Ctx.pSess->m_pBuff2);

		PCBYTE p1   = pb1;

		PCBYTE p2   = pb2;

		int cl, ml  = m_cxRem;

		int cr, mr  = m_cxRem;

		for( int y = 0; y <= m_cyRem; y++ ) {

			if( (cl = cmpl(p1, p2, m_cxRem)) == m_cxRem ) {

				cr = m_cxRem;
			}
			else
				cr = cmpr(p1, p2, m_cxRem);

			if( y == m_cyRem || cl == m_cxRem ) {

				if( y1 >= 0 ) {

					s1 = y1;

					s2 =  y;

					y1 = -1;
				}
			}
			else {
				if( y1 < 0 ) {

					if( s1 >= 0 ) {

						if( s2 + 4 > y ) {

							y1 = s1;
						}
						else {
							SafeDec(s1);
							SafeInc(s2);
							SafeDec(ml);
							SafeDec(mr);

							*pComp++ = LOBYTE(s1);
							*pComp++ = HIBYTE(s1);
							*pComp++ = LOBYTE(s2);
							*pComp++ = HIBYTE(s2);
							*pComp++ = LOBYTE(ml);
							*pComp++ = HIBYTE(ml);
							*pComp++ = LOBYTE(m_cxRem - mr);
							*pComp++ = HIBYTE(m_cxRem - mr);

							MakeDisplayBlob08(pComp, Ctx.pSess->m_pWork, pb1, s1, s2, ml, m_cxRem - mr);

							s1 = -1;

							y1 =  y;

							ml = m_cxRem;

							mr = m_cxRem;
						}
					}
					else
						y1 = y;
				}

				MakeMin(ml, cl);

				MakeMin(mr, cr);
			}

			p1 += m_cxRem;

			p2 += m_cxRem;
		}

		if( s1 >= 0 ) {

			SafeDec(s1);
			SafeInc(s2);
			SafeDec(ml);
			SafeDec(mr);

			*pComp++ = LOBYTE(s1);
			*pComp++ = HIBYTE(s1);
			*pComp++ = LOBYTE(s2);
			*pComp++ = HIBYTE(s2);
			*pComp++ = LOBYTE(ml);
			*pComp++ = HIBYTE(ml);
			*pComp++ = LOBYTE(m_cxRem - mr);
			*pComp++ = HIBYTE(m_cxRem - mr);

			MakeDisplayBlob08(pComp, Ctx.pSess->m_pWork, pb1, s1, s2, ml, m_cxRem - mr);
		}

		*pComp++ = 0;
		*pComp++ = 0;
		*pComp++ = 0;
		*pComp++ = 0;

		Ctx.pReq->SetReplyBody(pInit, pComp - pInit);

		Ctx.pReq->SetContentType("application/binary");

		Ctx.pReq->SetStatus(200);

		memcpy(pb2, pb1, sizeof(*pb1) * m_cwRem);

		return TRUE;
	}

	Ctx.pReq->SetStatus(503);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteRemoteDelta16(CWebReqContext const &Ctx)
{
	if( Ctx.pSess->AllocBuffers() ) {

		Ctx.pSess->Throttle(100);

		m_pGDI->Render(PBYTE(Ctx.pSess->m_pBuff1), MAKEWORD(16, 1));

		PBYTE pComp = Ctx.pSess->m_pWork + 2 * m_cwRem;

		PBYTE pInit = pComp;

		int    y1   = -1;

		int    s1   = -1;

		int    s2   = -1;

		PWORD  pb1  = PWORD(Ctx.pSess->m_pBuff1);

		PWORD  pb2  = PWORD(Ctx.pSess->m_pBuff2);

		PCWORD p1   = pb1;

		PCWORD p2   = pb2;

		int cl, ml  = m_cxRem;

		int cr, mr  = m_cxRem;

		for( int y = 0; y <= m_cyRem; y++ ) {

			if( (cl = cmpl(p1, p2, m_cxRem)) == m_cxRem ) {

				cr = m_cxRem;
			}
			else
				cr = cmpr(p1, p2, m_cxRem);

			if( y == m_cyRem || cl == m_cxRem ) {

				if( y1 >= 0 ) {

					s1 = y1;

					s2 =  y;

					y1 = -1;
				}
			}
			else {
				if( y1 < 0 ) {

					if( s1 >= 0 ) {

						if( s2 + 4 > y ) {

							y1 = s1;
						}
						else {
							SafeDec(s1);
							SafeInc(s2);
							SafeDec(ml);
							SafeDec(mr);

							*pComp++ = LOBYTE(s1);
							*pComp++ = HIBYTE(s1);
							*pComp++ = LOBYTE(s2);
							*pComp++ = HIBYTE(s2);
							*pComp++ = LOBYTE(ml);
							*pComp++ = HIBYTE(ml);
							*pComp++ = LOBYTE(m_cxRem - mr);
							*pComp++ = HIBYTE(m_cxRem - mr);

							MakeDisplayBlob16(pComp, Ctx.pSess->m_pWork, pb1, s1, s2, ml, m_cxRem - mr);

							s1 = -1;

							y1 =  y;

							ml = m_cxRem;

							mr = m_cxRem;
						}
					}
					else
						y1 = y;
				}

				MakeMin(ml, cl);

				MakeMin(mr, cr);
			}

			p1 += m_cxRem;

			p2 += m_cxRem;
		}

		if( s1 >= 0 ) {

			SafeDec(s1);
			SafeInc(s2);
			SafeDec(ml);
			SafeDec(mr);

			*pComp++ = LOBYTE(s1);
			*pComp++ = HIBYTE(s1);
			*pComp++ = LOBYTE(s2);
			*pComp++ = HIBYTE(s2);
			*pComp++ = LOBYTE(ml);
			*pComp++ = HIBYTE(ml);
			*pComp++ = LOBYTE(m_cxRem - mr);
			*pComp++ = HIBYTE(m_cxRem - mr);

			MakeDisplayBlob16(pComp, Ctx.pSess->m_pWork, pb1, s1, s2, ml, m_cxRem - mr);
		}

		*pComp++ = 0;
		*pComp++ = 0;
		*pComp++ = 0;
		*pComp++ = 0;

		Ctx.pReq->SetReplyBody(pInit, pComp - pInit);

		Ctx.pReq->SetContentType("application/binary");

		Ctx.pReq->SetStatus(200);

		memcpy(pb2, pb1, sizeof(*pb1) * m_cwRem);

		return TRUE;
	}

	Ctx.pReq->SetStatus(503);

	return TRUE;
}

BOOL CEnhancedWebServer::ExecuteRemoteDelta24(CWebReqContext const &Ctx)
{
	if( Ctx.pSess->AllocBuffers() ) {

		Ctx.pSess->Throttle(100);

		m_pGDI->Render(PBYTE(Ctx.pSess->m_pBuff1), MAKEWORD(32, 1));

		PBYTE pComp = Ctx.pSess->m_pWork + 4 * m_cwRem;

		PBYTE pInit = pComp;

		int     y1  = -1;

		int     s1  = -1;

		int     s2  = -1;

		PDWORD  pb1 = PDWORD(Ctx.pSess->m_pBuff1);

		PDWORD  pb2 = PDWORD(Ctx.pSess->m_pBuff2);

		PCDWORD p1  = pb1;

		PCDWORD p2  = pb2;

		int cl, ml = m_cxRem;

		int cr, mr = m_cxRem;

		for( int y = 0; y <= m_cyRem; y++ ) {

			if( (cl = cmpl(p1, p2, m_cxRem)) == m_cxRem ) {

				cr = m_cxRem;
			}
			else
				cr = cmpr(p1, p2, m_cxRem);

			if( y == m_cyRem || cl == m_cxRem ) {

				if( y1 >= 0 ) {

					s1 = y1;

					s2 =  y;

					y1 = -1;
				}
			}
			else {
				if( y1 < 0 ) {

					if( s1 >= 0 ) {

						if( s2 + 4 > y ) {

							y1 = s1;
						}
						else {
							SafeDec(s1);
							SafeInc(s2);
							SafeDec(ml);
							SafeDec(mr);

							*pComp++ = LOBYTE(s1);
							*pComp++ = HIBYTE(s1);
							*pComp++ = LOBYTE(s2);
							*pComp++ = HIBYTE(s2);
							*pComp++ = LOBYTE(ml);
							*pComp++ = HIBYTE(ml);
							*pComp++ = LOBYTE(m_cxRem - mr);
							*pComp++ = HIBYTE(m_cxRem - mr);

							MakeDisplayBlob24(pComp, Ctx.pSess->m_pWork, pb1, s1, s2, ml, m_cxRem - mr);

							s1 = -1;

							y1 =  y;

							ml = m_cxRem;

							mr = m_cxRem;
						}
					}
					else
						y1 = y;
				}

				MakeMin(ml, cl);

				MakeMin(mr, cr);
			}

			p1 += m_cxRem;

			p2 += m_cxRem;
		}

		if( s1 >= 0 ) {

			SafeDec(s1);
			SafeInc(s2);
			SafeDec(ml);
			SafeDec(mr);

			*pComp++ = LOBYTE(s1);
			*pComp++ = HIBYTE(s1);
			*pComp++ = LOBYTE(s2);
			*pComp++ = HIBYTE(s2);
			*pComp++ = LOBYTE(ml);
			*pComp++ = HIBYTE(ml);
			*pComp++ = LOBYTE(m_cxRem - mr);
			*pComp++ = HIBYTE(m_cxRem - mr);

			MakeDisplayBlob24(pComp, Ctx.pSess->m_pWork, pb1, s1, s2, ml, m_cxRem - mr);
		}

		*pComp++ = 0;
		*pComp++ = 0;
		*pComp++ = 0;
		*pComp++ = 0;

		Ctx.pReq->SetReplyBody(pInit, pComp - pInit);

		Ctx.pReq->SetContentType("application/binary");

		Ctx.pReq->SetStatus(200);

		memcpy(pb2, pb1, sizeof(*pb1) * m_cwRem);

		return TRUE;
	}

	Ctx.pReq->SetStatus(503);

	return TRUE;
}

// Display Rendering

BOOL CEnhancedWebServer::MakeDisplayBlob08(CHttpServerRequest *pReq, PBYTE pWork, PCBYTE pBits, int y1, int y2, int x1, int x2)
{
	PBYTE pComp = pWork + 1 * m_cwRem;

	PBYTE pInit = pComp;

	if( MakeDisplayBlob08(pComp, pWork, pBits, y1, y2, x1, x2) ) {

		pReq->SetReplyBody(pInit, pComp - pInit);

		pReq->SetContentType("application/binary");

		pReq->SetStatus(200);

		return TRUE;
	}

	return FALSE;
}

BOOL CEnhancedWebServer::MakeDisplayBlob08(PBYTE &pComp, PBYTE pWork, PCBYTE pBits, int y1, int y2, int x1, int x2)
{
	PBYTE  pInit = PBYTE(pWork);

	PBYTE  pData = pInit;

	PCBYTE pFrom = pBits + x1 + m_cxRem * y1;

	while( y1++ < y2 ) {

		memcpy(pData, pFrom, 2 * (x2 - x1));

		pData += x2 - x1;

		pFrom += m_cxRem;
	}

	CWebRle <BYTE> rle;

	return rle.Compress(pComp, pInit, pData - pInit);
}

BOOL CEnhancedWebServer::MakeDisplayBlob16(CHttpServerRequest *pReq, PBYTE pWork, PCWORD pBits, int y1, int y2, int x1, int x2)
{
	PBYTE pComp = pWork + 2 * m_cwRem;

	PBYTE pInit = pComp;

	if( MakeDisplayBlob16(pComp, pWork, pBits, y1, y2, x1, x2) ) {

		pReq->SetReplyBody(pInit, pComp - pInit);

		pReq->SetContentType("application/binary");

		pReq->SetStatus(200);

		return TRUE;
	}

	return FALSE;
}

BOOL CEnhancedWebServer::MakeDisplayBlob16(PBYTE &pComp, PBYTE pWork, PCWORD pBits, int y1, int y2, int x1, int x2)
{
	PWORD  pInit = PWORD(pWork);

	PWORD  pData = pInit;

	PCWORD pFrom = pBits + x1 + m_cxRem * y1;

	while( y1++ < y2 ) {

		memcpy(pData, pFrom, 2 * (x2 - x1));

		pData += x2 - x1;

		pFrom += m_cxRem;
	}

	CWebRle <WORD> rle;

	return rle.Compress(pComp, pInit, pData - pInit);
}

BOOL CEnhancedWebServer::MakeDisplayBlob24(CHttpServerRequest *pReq, PBYTE pWork, PCDWORD pBits, int y1, int y2, int x1, int x2)
{
	PBYTE pComp = pWork + 4 * m_cwRem;

	PBYTE pInit = pComp;

	if( MakeDisplayBlob24(pComp, pWork, pBits, y1, y2, x1, x2) ) {

		pReq->SetReplyBody(pInit, pComp - pInit);

		pReq->SetContentType("application/binary");

		pReq->SetStatus(200);

		return TRUE;
	}

	return FALSE;
}

BOOL CEnhancedWebServer::MakeDisplayBlob24(PBYTE &pComp, PBYTE pWork, PCDWORD pBits, int y1, int y2, int x1, int x2)
{
	PDWORD  pInit = PDWORD(pWork);

	PDWORD  pData = pInit;

	PCDWORD pFrom = pBits + x1 + m_cxRem * y1;

	while( y1++ < y2 ) {

		memcpy(pData, pFrom, 4 * (x2 - x1));

		pData += x2 - x1;

		pFrom += m_cxRem;
	}

	CWebRle <DWORD> rle;

	return rle.Compress(pComp, pInit, pData - pInit);
}

// Implementation

CString CEnhancedWebServer::GetRemoteUrl(void)
{
	extern PCTXT GetWebRemoteInfo(int cx, int cy, int zoom, int bits);

	PCTXT pRoot = NULL;

	switch( m_RemoteZoom ) {

		case 0: pRoot = "remote";   break;
		case 1: pRoot = "rvzoom";   break;
		case 2: pRoot = "rvcenter"; break;
		case 3: pRoot = "rvscale";  break;
	}

	CString Path;

	Path.Printf("/%s.htm?info=%s", pRoot, GetWebRemoteInfo(m_cxRem, m_cyRem, m_RemoteFact, m_RemoteBits));

	return Path;
}

DWORD CEnhancedWebServer::GetWinColor(COLOR Color)
{
	if( Color == 32767 ) {

		return 0xFFFFFF;
	}

	BYTE r = GetRED(Color);
	BYTE g = GetGREEN(Color);
	BYTE b = GetBLUE(Color);

	r *= 8;
	g *= 8;
	b *= 8;

	return ((r<<16)|(g<<8)|(b<<0));
}

BOOL CEnhancedWebServer::AllowAccess(DWORD Access)
{
	if( m_pOpts->m_uAuthMethod == methodAnon ) {

		return TRUE;
	}

	return m_pSecure->AllowAccess(Access);
}

BOOL CEnhancedWebServer::CanEditTag(CTag *pTag)
{
	return pTag->CanWrite();
}

BOOL CEnhancedWebServer::CanAccessAtLeastOnePage(void)
{
	for( UINT n = 0; n < m_pList->m_uCount; n++ ) {

		CWebPage *pPage = m_pList->m_ppPage[n];

		if( pPage ) {

			if( !pPage->m_Hide ) {

				if( CanAccessPage(pPage) ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CEnhancedWebServer::CanAccessPage(CWebPage *pPage)
{
	if( AllowAccess(pPage->m_PageSec) ) {

		return TRUE;
	}

	return FALSE;
}

void CEnhancedWebServer::AddListItem(CString &d, CHttpServerRequest *pReq, CString Link, CString Text)
{
	CString Path = pReq->GetFullPath();

	StripSession(pReq, Path);

	CString Flag = (Path == Link) ? " class='active'" : "";

	d.AppendPrintf("<li%s><a href='%s'>%s</a></li>",
		       PCTXT(Flag),
		       PCTXT(Link),
		       PCTXT(Text)
	);
}

BOOL CEnhancedWebServer::StripSession(CHttpServerRequest *pReq, CString &Text)
{
	CString Sess = pReq->GetParamString("session", "");

	if( Sess.GetLength() ) {

		UINT uPos = Text.Find("session=");

		UINT uLen = 8 + Sess.GetLength();

		if( Text[uPos+uLen] == '&' ) {

			Text.Delete(uPos, uLen+1);
		}
		else
			Text.Delete(uPos, uLen);

		if( Text.Right(1) == "?" ) {

			Text.Delete(Text.GetLength() - 1, 1);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CEnhancedWebServer::FakeKey(UINT uState, UINT uCode)
{
	if( m_RemoteCtrl ) {

		if( m_pSecure->AllowRemote() ) {

			CInput Input;

			Input.x.i.m_Type   = typeKey;

			Input.x.i.m_State  = uState;

			Input.x.i.m_Local  = FALSE;

			Input.x.d.k.m_Code = uCode;

			InputStore(Input.m_Ref);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CEnhancedWebServer::FakeTouch(UINT uState, int xPos, int yPos)
{
	if( m_RemoteCtrl ) {

		if( m_pSecure->AllowRemote() ) {

			CInput Input;

			Input.x.i.m_Type   = typeTouch;

			Input.x.i.m_State  = uState;

			Input.x.i.m_Local  = FALSE;

			Input.x.d.t.m_XPos = (xPos+2) / 4;

			Input.x.d.t.m_YPos = (yPos+2) / 4;

			InputStore(Input.m_Ref);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CEnhancedWebServer::Restrict(CHttpServerConnection *pCon)
{
	if( m_Restrict == 1 ) {

		DWORD Peer = pCon->GetRemote().m_dw;

		DWORD Test = (m_SecMask & Peer);

		DWORD Addr = (m_SecMask & m_SecAddr);

		if( Test == Addr ) {

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CEnhancedWebServer::WaitForTags(CTagRef *pRefs, UINT uRefs)
{
	BOOL fScan = TRUE;

	SetTimer(m_UserDelay * 1000);

	while( GetTimer() ) {

		UINT t;

		for( t = 0; t < uRefs; t++ ) {

			if( pRefs[t].pTag ) {

				CDataRef Ref;

				Ref.m_Ref     = 0;

				Ref.x.m_Array = pRefs[t].uPos;

				if( fScan ) {

					pRefs[t].pTag->SetScan(Ref, scanOnce);
				}
				else {
					if( !pRefs[t].pTag->IsAvail(Ref) ) {

						Sleep(10);

						break;
					}
				}
			}
		}

		if( fScan ) {

			fScan = FALSE;
		}
		else {
			if( t == uRefs ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CEnhancedWebServer::ShowLogOff(void)
{
	switch( m_pOpts->m_uAuthMethod ) {

		case methodForm:
		case methodHttp:

			return TRUE;

		case methodAnon:

			return FALSE;
	}

	return FALSE;
}

BOOL CEnhancedWebServer::CanEditTag(UINT uPage, UINT uTag)
{
	if( m_pOpts->m_uAuthMethod ) {

		if( m_Source ) {

			return TRUE;
		}
	}

	if( !uPage ) {

		for( UINT n = 0; n < m_pList->m_uCount; n++ ) {

			CWebPage *pItem = m_pList->m_ppPage[n];

			if( pItem->m_Edit ) {

				if( pItem->HasTag(uTag) ) {

					return TRUE;
				}
			}
		}

		return FALSE;
	}

	if( --uPage < m_pList->m_uCount ) {

		CWebPage *pItem = m_pList->m_ppPage[uPage];

		if( pItem->m_Edit ) {

			if( pItem->HasTag(uTag) ) {

				return TRUE;
			}

			return FALSE;
		}
	}

	return FALSE;
}

BOOL CEnhancedWebServer::RunCommand(char **ppText, UINT n)
{
	if( n < elements(m_Cmds) ) {

		CSysCmd const &Cmd = m_Cmds[n];

		if( Cmd.fEn ) {

			IDiagManager *pDiag;

			#if defined(AEON_ENVIRONMENT)

			AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

			pDiag->RunCommand(ppText, 1, Cmd.pCmd);

			pDiag->Release();

			#else

			pDiag = AfxGetSingleton(IDiagManager);

			pDiag->RunCommand(ppText, 1, Cmd.pCmd);

			#endif

			return TRUE;
		}
	}

	return FALSE;
}

void CEnhancedWebServer::MakeOptionTable(CString &t, PCTXT p, CString d)
{
	t += "<table class='table'><thead><tr><th>";

	t += GetWebString(IDS_OPTION);

	t += "</th><th>";

	t += GetWebString(IDS_DESCRIPTION);

	t += "</th></tr></thead><tbody>";

	if( p ) {

		t += CPrintf("<tr id='%s-head'></tr>", p);

		t += d;

		t += CPrintf("<tr id='%s-tail'></tr>", p);
	}
	else
		t += d;

	t += "</tbody></table>";
}

void CEnhancedWebServer::CheckCmds(void)
{
	IDiagManager *pDiag;

	#if defined(AEON_ENVIRONMENT)

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	#else

	pDiag = AfxGetSingleton(IDiagManager);

	#endif

	for( int n = 0; n < elements(m_Cmds); n++ ) {

		CSysCmd &Cmd = m_Cmds[n];

		Cmd.fEn = !pDiag->RunCommand(NULL, 0, Cmd.pCmd);
	}

	#if defined(AEON_ENVIRONMENT)

	pDiag->Release();

	#endif
}

// End of File
