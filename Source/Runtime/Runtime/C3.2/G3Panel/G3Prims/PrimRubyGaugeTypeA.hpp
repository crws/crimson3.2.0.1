
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGagueTypeA_HPP
	
#define	INCLUDE_PrimRubyGagueTypeA_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Gauge Primitive
//

class CPrimRubyGaugeTypeA : public CPrimRubyGaugeBase
{
	public:
		// Constructor
		CPrimRubyGaugeTypeA(void);

		// Destructor
		~CPrimRubyGaugeTypeA(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void MovePrim(int cx, int cy);

		// Drawing
		void DrawBezel(IGDI *pGDI);

		// Data Members
		COLOR m_Color[6];

	protected:
		// Data Members
		CRubyGdiList m_listFace;
		CRubyGdiList m_listOuter;
		CRubyGdiList m_listInner;
		CRubyGdiList m_listRing1;
		CRubyGdiList m_listRing2;
		CRubyGdiList m_listRing3;
		CRubyGdiList m_listRing4;

		// Color Schemes
		COLOR GetColor(UINT n);
		BOOL  IsNaked(void);
	};

// End of File

#endif
