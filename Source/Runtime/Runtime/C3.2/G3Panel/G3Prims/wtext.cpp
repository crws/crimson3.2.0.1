
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Primitive with Text
//

// Constructor

CPrimWithText::CPrimWithText(void)
{
	m_pTextItem = NULL;

	m_pDataItem = NULL;

	m_pAction   = NULL;

	m_Delay     = 0;

	m_fTrans    = FALSE;

	m_fAlias    = FALSE;

	m_fPress    = FALSE;

	m_fTime     = FALSE;
	}

// Destructor

CPrimWithText::~CPrimWithText(void)
{
	delete m_pTextItem;

	delete m_pDataItem;

	delete m_pAction;
	}

// Initialization

void CPrimWithText::Load(PCBYTE &pData)
{
	CPrim::Load(pData);

	if( GetByte(pData) ) {

		m_pTextItem = New CPrimText(this);

		m_pTextItem->Load(pData);
		}

	if( GetByte(pData) ) {

		m_pDataItem = New CPrimData(this);

		m_pDataItem->Load(pData);
		}

	if( GetByte(pData) ) {

		m_pAction = New CPrimAction;

		m_pAction->Load(pData);

		m_Delay = m_pAction->m_Delay;
		}

	if( m_pTextItem || m_pDataItem ) {

		m_TextRect.x1 = GetWord(pData);
		m_TextRect.y1 = GetWord(pData);
		m_TextRect.x2 = GetWord(pData);
		m_TextRect.y2 = GetWord(pData);
		}

	if( m_pTextItem ) {

		if( m_pTextItem->IsTransparent() ) {

			if( m_pTextItem->IsAntiAliased() ) {

				m_fAlias = TRUE;
				}

			m_fTrans = TRUE;
			}
		}

	if( m_pDataItem ) {

		if( m_pDataItem->IsTransparent() ) {

			if( m_pDataItem->IsAntiAliased() ) {

				m_fAlias = TRUE;
				}

			m_fTrans = TRUE;
			}
		}
	}

// Overridables

void CPrimWithText::SetScan(UINT Code)
{
	if( m_pTextItem ) {

		m_pTextItem->SetScan(Code);
		}

	if( m_pDataItem ) {

		m_pDataItem->SetScan(Code);
		}

	if( m_pAction ) {

		m_pAction->SetScan(Code);
		}

	CPrim::SetScan(Code);
	}

void CPrimWithText::MovePrim(int cx, int cy)
{
	m_TextRect.x1 += cx;

	m_TextRect.y1 += cy;

	m_TextRect.x2 += cx;

	m_TextRect.y2 += cy;

	CPrim::MovePrim(cx, cy);
	}

void CPrimWithText::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrim::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		if( m_pTextItem ) {

			if( m_pTextItem->DrawPrep(pGDI) ) {

				m_fChange = TRUE;
				}
			}

		if( m_pDataItem ) {

			if( m_pDataItem->DrawPrep(pGDI) ) {

				m_fChange = TRUE;
				}
			}

		if( m_fTrans ) {

			if( m_fAlias ) {

				(m_fChange ? Erase : Trans).Append(m_DrawRect);
				}
			else {
				if( m_fChange ) {

					Erase.Append(m_DrawRect);
					}
				}
			}
		}
	}

void CPrimWithText::DrawPrim(IGDI *pGDI)
{
	if( m_pTextItem ) {

		m_pTextItem->DrawItem(pGDI, m_TextRect);
		}

	if( m_pDataItem ) {

		m_pDataItem->DrawItem(pGDI, m_TextRect);
		}
	}

void CPrimWithText::LoadTouchMap(ITouchMap *pTouch)
{
	if( m_pDataItem ) {
		
		if( m_pDataItem->m_Entry ) {

			pTouch->FillRect(PassRect(m_TextRect));

			return;
			}
		}

	if( m_pAction ) {

		pTouch->FillRect(PassRect(m_DrawRect));
		}
	}

UINT CPrimWithText::OnMessage(UINT uMsg, UINT uParam)
{
	if( IsVisible() ) {

		if( m_pDataItem ) {		
		
			if( m_pDataItem->m_Entry ) {

				return m_pDataItem->OnMessage(uMsg, uParam);
				}
			}
		}

	switch( uMsg ) {
		
		case msgTakeFocus:

			return OnTakeFocus(uMsg, uParam);

		case msgKillFocus:

			if( !m_Delay && m_fPress ) {

				OnTouchUp();

				return TRUE;
				}
			
			return FALSE;

		case msgTouchInit:
		case msgTouchDown:

			if( !m_fTime ) {

				m_px    = 100 * (int(LOWORD(uParam)) - m_DrawRect.x1) / (m_DrawRect.x2 - m_DrawRect.x1);

				m_py    = 100 * (int(HIWORD(uParam)) - m_DrawRect.y1) / (m_DrawRect.y2 - m_DrawRect.y1);

				BOOL rv = OnTouchDown();

				m_uTime = GetTickCount();

				return rv;
				}

			return 0;

		case msgTouchRepeat:

			return OnTouchRepeat();

		case msgTouchUp:

			if( SetDelayTimer(m_uTime, m_Delay) ) {

				m_fTime = TRUE;

				return 0;
				}

			return OnTouchUp();

		case msgTimer:

			m_fTime = FALSE;

			return OnTouchUp();

		case msgIsPressed:

			return m_fPress;

		case msgBlockRemote:

			return OnBlockRemote();
		}

	return CPrim::OnMessage(uMsg, uParam);
	}

// Message Handlers

UINT CPrimWithText::OnTakeFocus(UINT uMsg, UINT uParam)
{
	if( uParam == 0 ) {

		if( m_pAction ) {
			
			if( m_pAction->IsAvail() && m_pAction->IsEnabled() ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	if( uParam == 1 ) {

		if( m_pAction ) {

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

UINT CPrimWithText::OnTouchDown(void)
{
	m_fPress  = TRUE;

	m_fChange = TRUE;

	m_pAction->RunEvent(stateDown,   TRUE, m_px, m_py);

	m_pAction->RunEvent(stateRepeat, TRUE, m_px, m_py);

	return TRUE;
	}

UINT CPrimWithText::OnTouchRepeat(void)
{
	if( m_pAction->RunEvent(stateRepeat, TRUE, m_px, m_py) ) {

		return TRUE;
		}

	return FALSE;
	}

UINT CPrimWithText::OnTouchUp(void)
{
	m_fPress  = FALSE;

	m_fChange = TRUE;

	m_pAction->RunEvent(stateUp, TRUE, m_px, m_py);

	return TRUE;
	}

UINT CPrimWithText::OnBlockRemote(void)
{
	return m_pAction && m_pAction->m_Local;
	}

// End of File
