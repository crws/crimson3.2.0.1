
#include "intern.hpp"

#include "PrimRubySimpleImage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyShade.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Image Primitive
//

// Constructor

CPrimRubySimpleImage::CPrimRubySimpleImage(void)
{
	m_pImage = NULL;

	m_pColor = NULL;

	m_pShade = New CPrimRubyShade;
	}

// Destructor

CPrimRubySimpleImage::~CPrimRubySimpleImage(void)
{
	delete m_pImage;

	delete m_pColor;

	delete m_pShade;
	}

// Initialization

void CPrimRubySimpleImage::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubySimpleImage", pData);

	CPrimRubyWithText::Load(pData);

	if( GetByte(pData) ) {

		m_pImage = New CPrimImage;

		m_pImage->Load(pData);
		}

	GetCoded(pData, m_pColor);

	m_pShade->Load(pData);

	FindImageRect();
	}

// Overridables

void CPrimRubySimpleImage::SetScan(UINT Code)
{
	SetItemScan(m_pColor, Code);

	m_pShade->SetScan(Code);

	CPrimRubyWithText::SetScan(Code);
	}

void CPrimRubySimpleImage::MovePrim(int cx, int cy)
{
	CPrimRubyWithText::MovePrim(cx, cy);

	FindImageRect();
	}

void CPrimRubySimpleImage::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRubyWithText::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_pShade->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( !m_pImage || m_pImage->IsTransparent(m_ImageRect) ) {

			if( m_pImage && m_pImage->IsAntiAliased() ) {

				(m_fChange ? Erase : Trans).Append(m_bound);
				}
			else {
				if( m_fChange ) {

					Erase.Append(m_bound);
					}
				}
			}
		}
	}

void CPrimRubySimpleImage::DrawPrim(IGDI *pGDI)
{
	if( m_pImage ) {

		UINT rop = m_Ctx.m_fColor ? 0 : ropDisable;

		if( m_pShade->m_Pattern ) {

			m_pImage->DrawItem(pGDI, m_ImageRect, rop, m_pShade);
			}
		else
			m_pImage->DrawItem(pGDI, m_ImageRect, rop);
		}
	else {
		pGDI->ResetFont();

		pGDI->SetTextTrans(modeTransparent);

		PCUTF pt = L"IMG";

		int   cx = pGDI->GetTextWidth(pt);

		int   cy = pGDI->GetTextHeight(pt);

		int   xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

		int   yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 2;

		pGDI->TextOut(xp, yp, pt);
		}

	CPrimRubyWithText::DrawPrim(pGDI);
	}

// Implementation

void CPrimRubySimpleImage::FindImageRect(void)
{
	m_ImageRect = m_DrawRect;
	}

// Context Creation

void CPrimRubySimpleImage::FindCtx(CCtx &Ctx)
{
	Ctx.m_fColor = GetItemData(m_pColor, C3INT(1));
	}

// Context Check

BOOL CPrimRubySimpleImage::CCtx::operator == (CCtx const &That) const
{
	return m_fColor == That.m_fColor;
	}

// End of File
