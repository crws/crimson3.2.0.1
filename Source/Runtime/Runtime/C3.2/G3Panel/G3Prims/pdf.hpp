
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PDF_HPP
	
#define	INCLUDE_PDF_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "viewer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PDF Request
//

class CPDFRequest : public IBase
{
	public:
		// Constructor
		CPDFRequest(void);

		// Destructor
		~CPDFRequest(void);

		// IBase
		UINT Release(void);

		// Operations
		void QueueEvent(UINT uMsg);

	public:
		// Request Data
		HTASK	m_hTask;
		UINT    m_uHandle;
		CString	m_File;
		int     m_nPage;
		int     m_nRes;

		// Display Data
		int     m_nPages;
		S2      m_Bitmap;
		int     m_nSize;
		PBYTE   m_pData;
		
		// List Data
		CPDFRequest * m_pNext;
		CPDFRequest * m_pPrev;
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- PDF Viewer
//

class CPrimPDFViewer : public CPrimViewer
{
	public:
		// Constructor
		CPrimPDFViewer(void);

		// Destructor
		~CPrimPDFViewer(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		CCodedText * m_pFile;
		CCodedText * m_pTxtCannot;
		CCodedText * m_pTxtWorking;
		CCodedText * m_pBtnLeft;
		CCodedText * m_pBtnRight;
		CCodedText * m_pBtnPrev;
		CCodedText * m_pBtnNext;
		CCodedText * m_pBtnZoomIn;
		CCodedText * m_pBtnZoomOut;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			CUnicode      m_File;
			UINT          m_uScroll;
			int           m_nPage;
			int           m_nRes;
			P2            m_Origin;
			UINT          m_uState;
			BOOL          m_fLoad;
			CPDFRequest * m_pDisplay;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Buttons
		enum {
			buttonLeft	= 0,
			buttonRight	= 1,
			buttonZoomIn	= 2,
			buttonZoomOut	= 3,
			buttonPrev	= 4,
			buttonNext	= 5,
			};
		
		// Context Data
		CCtx m_Ctx;

		// Data Members
		UINT          m_uScroll;
		int           m_nPage;		
		int           m_nRes;
		P2            m_Origin;
		UINT          m_uState;
		BOOL          m_fLoad;
		CPDFRequest * m_pDisplay;
		
		// Messaging Data
		UINT  m_uParam;

		// Layout Data
		int   m_nScroll;
		int   m_yHead;
		R2    m_Scroll;

		UINT  m_uTimer;
		int   m_nLine;
		int   m_nDelta;

		// Message Handlers
		BOOL OnTouchDown(UINT uParam);
		BOOL OnTouchRepeat(void);

		// Event Hooks
		BOOL OnMakeList(void);
		BOOL OnEnable(void);
		BOOL OnBtnDown(UINT n);
		BOOL OnBtnRepeat(UINT n);
		BOOL OnBtnUp(UINT n);
		BOOL OnTouchWork(P2 Pos);

		// Layout Calculation
		void FindScroll(R2 &Rect, UINT n, int nPos);
		void FindLayout(IGDI *pGDI);

		// Scroll Handlers
		BOOL OnTouchScroll(P2 Pos);

		// Implementation
		void LoadFile(CCtx const &Ctx);
		void DrawTitle  (IGDI *pGDI);
		void DrawScroll (IGDI *pGDI);
		void DrawPDF    (IGDI *pGDI);
		void DrawWorking(IGDI *pGDI);
		void DrawButton (IGDI *pGDI, R2 Rect, BOOL fPress, BOOL fEnable);
		void FindPoints (P2 *t, P2 *b, R2 &r, int s);

		// Context Handling
		void FindCtx(CCtx &Ctx);
		void ClearCtx(CCtx &Ctx);
	};

// Instantiator

extern CPrim * Create_PrimPDFViewer(void);

// End of File

#endif
