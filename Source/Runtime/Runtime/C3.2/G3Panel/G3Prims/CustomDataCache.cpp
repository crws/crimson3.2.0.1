
#include "intern.hpp"

#include "CustomDataCache.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Custom Data Cache
//

// Constructor

CCustomDataCache::CCustomDataCache(void)
{
	m_dwPitch = 0;

	m_dwStart = 0;
	
	m_uChans  = 0;

	m_pForm   = NULL;
	
	m_pData   = NULL;

	m_uBuff   = 0;

	m_uHead   = 0;

	m_uTail   = 0;

	//
	m_pFile = Create_File();
	}

// Destructor

CCustomDataCache::~CCustomDataCache(void)
{
	m_pFile->Release();

	Clean();
	}

// Operations

BOOL CCustomDataCache::LoadFromFile(CString const &Root, CUnicode const &File)
{
	Clean();

	char  cDrive = Root[1] == ':' ? Root[0] : 'C';	

	if( FindFileSystem(cDrive) ) {

		if( m_pFileSys->ChangeDir(PCTXT(Root)) ) {

			CFilename const &Name = CFilename(UniConvert(File));

			if( m_pFile->Open(Name, fileRead) ) {

				DWORD dwHead;

				if( m_pFile->Read(PBYTE(&dwHead), sizeof(dwHead)) == sizeof(dwHead) ) {
					
					if( dwHead == 0xFF00DDFF ) {

						if( LoadFile() ) {

							//ShowForm();

							//ShowData();
							}
						}
					}

				m_pFile->Close();

				FreeFileSystem();

				return TRUE;
				}

			AfxTrace("file open failed\n");
			}

		FreeFileSystem();
		}

	return FALSE;
	}

// Attributes

UINT CCustomDataCache::GetChanCount(void)
{
	return m_uChans;
	}

DWORD CCustomDataCache::GetPitchTime(void)
{
	return m_dwPitch;
	}

DWORD CCustomDataCache::GetStartTime(void)
{
	return m_dwStart;
	}

DWORD CCustomDataCache::GetFinalTime(void)
{
	return m_dwStart + (m_dwPitch * 5) * m_uCount;
	}

// Data Access

BOOL CCustomDataCache::IsEmpty(void)
{
	return m_uHead == m_uTail;
	}

UINT CCustomDataCache::GetInitSlot(void)
{
	return (m_uTail + m_uBuff - 1) % m_uBuff;
	}

BOOL CCustomDataCache::GetNextSlot(UINT &uSlot)
{
	if( uSlot == m_uHead ) {

		return FALSE;
		}

	uSlot = (uSlot + m_uBuff - 1) % m_uBuff;

	return TRUE;
	}

DWORD CCustomDataCache::GetSlotTime(UINT uSlot)
{
	return m_dwStart + uSlot * m_dwPitch * 5;
	}

DWORD CCustomDataCache::GetSlotData(UINT uSlot, UINT uChan)
{
	return m_pData[uSlot * m_uChans + uChan];
	}

PDWORD CCustomDataCache::GetSlotData(UINT uSlot)
{
	return m_pData + uSlot * m_uChans;
	}

// Log Access

BOOL CCustomDataCache::HasCoverage(DWORD dwTime)
{
	return TRUE;
	}

DWORD CCustomDataCache::GetInitTime(void)
{
	return 0;
	}

// Format Access

DWORD CCustomDataCache::GetChanMin(UINT uChan)
{
	return m_pForm ? m_pForm[uChan].m_dwMin : 0;
	}

DWORD CCustomDataCache::GetChanMax(UINT uChan)
{
	return m_pForm ? m_pForm[uChan].m_dwMax : 0;
	}

// Filing System Helpers

BOOL CCustomDataCache::FindFileSystem(char cDrive)
{
	return (m_pFileSys = AfxGetSingleton(IFileManager)->LockFileSystem(cDrive)) != NULL;
	}

void CCustomDataCache::FreeFileSystem(void)
{
	AfxGetSingleton(IFileManager)->FreeFileSystem(m_pFileSys, true);
	}

// Implementation

BOOL CCustomDataCache::LoadFile(void)
{
	if( m_pFile->Read(PBYTE(&m_uChans), sizeof(m_uChans)) == sizeof(m_uChans) ) {

		if( m_pFile->Read(PBYTE(&m_dwStart), sizeof(m_dwStart)) == sizeof(m_dwStart) ) {

			if( m_pFile->Read(PBYTE(&m_dwPitch), sizeof(m_dwPitch)) == sizeof(m_dwPitch) ) {

				if( m_pFile->Read(PBYTE(&m_uCount), sizeof(m_uCount)) == sizeof(m_uCount) ) {
					
					return LoadForm() && LoadData();
					}
				}			
			}
		}

	return FALSE;
	}

BOOL CCustomDataCache::LoadForm(void)
{
	UINT uAlloc = m_uChans * sizeof(CFormat);

	m_pForm = New CFormat [ uAlloc ];

	return m_pFile->Read(PBYTE(m_pForm), uAlloc) == uAlloc;
	}

BOOL CCustomDataCache::LoadData(void)
{
	AllocBuffer();

	BOOL fValid = TRUE;

	PDWORD pData = New DWORD [ m_uChans ];

	UINT   uData = m_uChans * sizeof(DWORD);

	for( UINT uScan = 0; fValid && uScan < m_uCount; uScan ++ ) {

		if( (fValid = m_pFile->Read(PBYTE(pData), uData) == uData) ) {

			Append(pData);				
			}
		}

	delete [] pData;

	return fValid;
	}

void CCustomDataCache::Clean(void)
{
	if( m_pForm ) {
	
		delete [] m_pForm;

		m_pForm = NULL;
		}
	
	if( m_pData ) {
	
		delete [] m_pData;

		m_pData = NULL;
		}
	}

void CCustomDataCache::AllocBuffer(void)
{
	m_pData = New DWORD [ m_uChans * m_uCount ];

	m_uBuff = m_uCount;

	m_uHead = 0;

	m_uTail = 0;
	}

void CCustomDataCache::Append(PDWORD pData)
{
	if( m_uCount ) {

		UINT uNext = (m_uTail + 1) % m_uBuff;

		UINT uPos  = m_uTail * m_uChans;

		BOOL fWrap = FALSE;

		if( m_uHead == uNext ) {

			m_uHead = (m_uHead + 1) % m_uBuff;		

			fWrap = TRUE;
			}

		memcpy( m_pData + uPos, 
			pData, 
			sizeof(DWORD) * m_uChans
			);
			
		m_uTail  = uNext;
		}
	}

// Development

void CCustomDataCache::ShowForm(void)
{
	for( UINT uChan = 0; uChan < m_uChans; uChan ++ ) {

		CFormat &Form = m_pForm[uChan];

		AfxTrace("form %u\t", uChan);

		AfxTrace("min %u, max %u, dps %u", Form.m_dwMin, Form.m_dwMax, Form.m_dwDPs);
		
		AfxTrace("\n");
		}
	}

void CCustomDataCache::ShowData(void)
{
	UINT uSlot = GetInitSlot();

	do {
		AfxTrace("slot %u\t", uSlot);

		for( UINT uChan = 0; uChan < m_uChans; uChan ++ ) {

			DWORD dwData = GetSlotData(uSlot, uChan);

			AfxTrace("<%u>", dwData);
			}

		AfxTrace("\n");

		} while( GetNextSlot(uSlot) );

	}

// End of File
