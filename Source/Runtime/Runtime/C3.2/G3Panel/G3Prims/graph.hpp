
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_GRAPH_HPP
	
#define	INCLUDE_GRAPH_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "legacy.hpp"

#include "scale.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyBarGraph;
class CPrimLegacyVertBarGraph;
class CPrimLegacyHorzBarGraph;
class CPrimLegacyScatterGraph;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Bar Graph
//

class CPrimLegacyBarGraph : public CPrimLegacyFigure
{
	public:
		// Constructor
		CPrimLegacyBarGraph(void);

		// Destructor
		~CPrimLegacyBarGraph(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);

		// Item Properties
		CCodedItem * m_pValue;
		CCodedItem * m_pCount;
		CCodedItem * m_pMin;
		CCodedItem * m_pMax;
		CPrimBrush * m_pFill;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			BOOL      m_fAvail;
			C3REAL  * m_pVal;
			UINT      m_uCount;
			C3REAL    m_rMin;
			C3REAL    m_rMax;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);
		void MakeCtx(CCtx &Ctx);
		void FreeCtx(CCtx &Ctx);

		// Attributes
		BOOL IsAvail(void) const;
		
		// Implementation
		void DrawBack(IGDI *pGDI, R2 &Rect);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Bar Graph
//

class CPrimLegacyVertBarGraph : public CPrimLegacyBarGraph
{
	public:
		// Constructor
		CPrimLegacyVertBarGraph(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Bar Graph
//

class CPrimLegacyHorzBarGraph : public CPrimLegacyBarGraph
{
	public:
		// Constructor
		CPrimLegacyHorzBarGraph(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLineGraph;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Line Graph
//

class CPrimLineGraph : public CCodedHost
{
	public:
		// Constructor
		CPrimLineGraph(void);

		// Destructor
		~CPrimLineGraph(void);

		// Operations
		void SetScan(UINT Code);
		void DrawItem(IGDI *pGDI, R2 Rect);
		BOOL CheckCtx(void);

		// Attributes
		BOOL   IsAvail(void) const;
		C3REAL GetMinX(void);
		C3REAL GetMaxX(void);
		C3REAL GetMinY(void);
		C3REAL GetMaxY(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Item Properties
		CCodedItem * m_pValueX;
		CCodedItem * m_pValueY;
		CCodedItem * m_pCount;
		CCodedItem * m_pMinX;
		CCodedItem * m_pMaxX;
		CCodedItem * m_pMinY;
		CCodedItem * m_pMaxY;
		CPrimColor * m_pDataFill;
		CPrimColor * m_pDataLine;
		CPrimColor * m_pBestLine;
		UINT	     m_Style;
		UINT	     m_Fit;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			BOOL      m_fAvail;
			C3REAL  * m_pValX;
			C3REAL  * m_pValY;
			UINT      m_uCount;
			C3REAL    m_rMinX;
			C3REAL    m_rMaxX;
			C3REAL    m_rMinY;
			C3REAL    m_rMaxY;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);
		void MakeCtx(CCtx &Ctx);
		void FreeCtx(CCtx &Ctx);

		// Implementation
		int ToInt(double i);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Scatter Graph
//

class CPrimLegacyScatterGraph : public CPrimLegacyFigure
{
	public:
		// Constructor
		CPrimLegacyScatterGraph(void);

		// Destructor
		~CPrimLegacyScatterGraph(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Item Properties
		CPrimLineGraph  ** m_pLine;
		UINT		   m_uCount;
		UINT               m_GridType;
		CPrimColor       * m_pVertCol;
		CPrimColor       * m_pHorzCol;
		UINT               m_GridFont;
		UINT               m_HorzGap;
		UINT               m_VertPrecise;
		UINT               m_HorzPrecise;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR  m_VertCol;
			COLOR  m_HorzCol;
			C3REAL m_GridMinX;
			C3REAL m_GridMaxX;
			C3REAL m_GridMinY;
			C3REAL m_GridMaxY;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx         m_Ctx;

		// Layout Data
		int          m_x1;
		int          m_x2;
		int          m_y1;
		int          m_y2;
		int          m_fy;
		int          m_f;
		int          m_ySize;
		int          m_xSize;
		CScaleHelper m_VertHelper;
		CScaleHelper m_HorzHelper;

		// Implementation
		void DrawGrid(IGDI *pGDI, R2 Rect);

		// Context Creation
		void FindCtx(CCtx &Ctx);
		void FindLayout(IGDI *pGDI);
	};

// End of File

#endif
