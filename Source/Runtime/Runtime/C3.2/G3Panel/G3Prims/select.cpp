
#include "intern.hpp"

#include "select.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Selector Switch
//

// Constructor

CPrimLegacySelector::CPrimLegacySelector(void)
{
	m_pKnob   = New CPrimBrush;

	m_pEdge   = New CPrimPen;

	m_pBack   = New CPrimColor(GetRGB(0, 0, 0));

	m_pPanel  = New CPrimBrush;

	m_fPressL = FALSE;

	m_fPressR = FALSE;
}

// Destructor

CPrimLegacySelector::~CPrimLegacySelector(void)
{
	delete m_pKnob;

	delete m_pEdge;

	delete m_pBack;

	delete m_pPanel;
}

// Initialization

void CPrimLegacySelector::Load(PCBYTE &pData)
{
	CPrimRich::Load(pData);

	m_ShowStates = GetByte(pData);

	m_pKnob->Load(pData);

	m_pEdge->Load(pData);

	m_pBack->Load(pData);

	m_pPanel->Load(pData);
}

// Overridables

void CPrimLegacySelector::SetScan(UINT Code)
{
	m_pKnob->SetScan(Code);

	m_pEdge->SetScan(Code);

	m_pBack->SetScan(Code);

	CDispFormat *pFormat = NULL;

	if( GetDataFormat(pFormat) ) {

		pFormat->SetScan(Code);
	}

	CPrimRich::SetScan(Code);
}

void CPrimLegacySelector::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRich::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CDispColor *pColor;

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
		}

		if( m_pKnob->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
		}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
		}

		if( m_pPanel->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
		}

		if( !GetDataColor(pColor) ) {

			if( m_fChange ) {

				Erase.Append(m_DrawRect);
			}
		}
	}
}

void CPrimLegacySelector::LoadTouchMap(ITouchMap *pTouch)
{
	if( m_Entry ) {

		R2 Rect = m_DrawRect;

		int x1 = Rect.x1;
		int y1 = Rect.y1;
		int x2 = Rect.x2;
		int y2 = Rect.y2;

		if( m_ShowStates && (y2 - y1) > (x2 - x1) ) {

			Rect.y1 = y2 - (x2 - x1);

			DeflateRect(Rect, 4, 4);
		}

		pTouch->FillEllipse(PassRect(Rect), etWhole);
	}
}

UINT CPrimLegacySelector::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgTakeFocus:

			return OnTakeFocus(uMsg, uParam);

		case msgTouchRepeat:

			return OnTouchRepeat();

		case msgTouchUp:

			return OnTouchUp();

		case msgIsPressed:

			return m_Ctx.m_fPressL || m_Ctx.m_fPressR;
	}

	return CPrimRich::OnMessage(uMsg, uParam);
}

// Message Handlers

UINT CPrimLegacySelector::OnTakeFocus(UINT uMsg, UINT uParam)
{
	if( m_Entry ) {

		if( uParam == 0 ) {

			return m_Ctx.m_fAvail && m_Ctx.m_fEnable;
		}

		if( uParam == 1 ) {

			return TRUE;
		}
	}

	return FALSE;
}

UINT CPrimLegacySelector::OnTouchRepeat(void)
{
	return FALSE;
}

UINT CPrimLegacySelector::OnTouchUp(void)
{
	if( m_fPressL ) {

		m_fPressL = FALSE;

		return TRUE;
	}

	if( m_fPressR ) {

		m_fPressR = FALSE;

		return TRUE;
	}

	return FALSE;
}

// Context Creation

void CPrimLegacySelector::FindCtx(CCtx &Ctx)
{
	if( m_pValue && IsAvail() ) {

		Ctx.m_Data   = m_pValue->ExecVal();

		Ctx.m_fAvail = TRUE;
	}
	else {
		Ctx.m_Data = 0;

		Ctx.m_fAvail = FALSE;
	}

	Ctx.m_fEnable = (!m_pEnable || m_pEnable->ExecVal());

	Ctx.m_fPressL = m_fPressL;

	Ctx.m_fPressR = m_fPressR;
}

// Context Check

BOOL CPrimLegacySelector::CCtx::operator == (CCtx const &That) const
{
	return m_fAvail   == That.m_fAvail  &&
		m_Data     == That.m_Data    &&
		m_fEnable  == That.m_fEnable &&
		m_fPressL  == That.m_fPressL &&
		m_fPressR  == That.m_fPressR;;
}

// Implementation

void CPrimLegacySelector::DrawKnob(IGDI *pGDI, R2 Rect, INT nPos)
{
	int x1 = Rect.x1;
	int y1 = Rect.y1;
	int x2 = Rect.x2;
	int y2 = Rect.y2;

	pGDI->ResetAll();

	m_pEdge->DrawEllipse(pGDI, Rect, etWhole);

	DeflateRect(Rect, 3, 3);

	pGDI->SetBrushFore(m_pBack->GetColor());

	pGDI->FillEllipse(PassRect(Rect), etWhole);

	m_pEdge->DrawEllipse(pGDI, Rect, etWhole);

	if( m_Ctx.m_fAvail ) {

		double th = nPos / 180.0 * acos(double(-1));

		double r1 = min(x2-x1, y2-y1) * 0.25;
		double r2 = 3 * r1;

		double co = cos(th);
		double si = sin(th);

		int xp = (x1+x2)/2;
		int yp = (y1+y2)/2;

		P2 Pt[4];

		Pt[0].x = int(xp - si * r2/2 - co * r1/2 + 0.5);
		Pt[0].y = int(yp - co * r2/2 + si * r1/2 + 0.5);

		Pt[1].x = int(Pt[0].x + co * r1 + 0.5);
		Pt[1].y = int(Pt[0].y - si * r1 + 0.5);

		Pt[2].x = int(Pt[1].x + si * r2 + 0.5);
		Pt[2].y = int(Pt[1].y + co * r2 + 0.5);

		Pt[3].x = int(Pt[2].x - co * r1 + 0.5);
		Pt[3].y = int(Pt[2].y + si * r1 + 0.5);

		m_pKnob->FillPolygon(pGDI, Pt, elements(Pt), 0);

		m_pEdge->DrawPolygon(pGDI, Pt, elements(Pt), 0);
	}

	m_Left  = Rect;

	m_Left.x2 -= (x2 - x1) / 2;

	m_Right = Rect;

	m_Right.x1 += (x2 - x1) / 2;
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Two-State Selector Switch
//

// Constructor

CPrimLegacySelectorTwo::CPrimLegacySelectorTwo(void)
{
}

// Initialization

void CPrimLegacySelectorTwo::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacySelectorTwo", pData);

	CPrimLegacySelector::Load(pData);
}

// Overridables

void CPrimLegacySelectorTwo::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	int x1 = Rect.x1;
	int y1 = Rect.y1;
	int x2 = Rect.x2;
	int y2 = Rect.y2;

	if( m_ShowStates && (y2 - y1) > (x2 - x1) ) {

		SetColors(pGDI, 2);

		if( !m_TagColor ) {

			m_pPanel->FillRect(pGDI, Rect);
		}
		else
			pGDI->FillRect(PassRect(Rect));

		Rect.y1 = y2 - (x2 - x1);

		SelectFont(pGDI, m_Font);

		int yp = y1 + (Rect.y1 - y1 - pGDI->GetTextHeight(L"X")) / 2;

		if( yp >= y1 ) {

			CDispFormat *pFormat;

			if( GetDataFormat(pFormat) ) {

				CUnicode s0 = pFormat->Format(FALSE, typeInteger, fmtPad);

				CUnicode s1 = pFormat->Format(TRUE, typeInteger, fmtPad);

				pGDI->TextOut(x1 + 4,
					      yp,
					      UniVisual(s0)
				);

				pGDI->TextOut(x2 - 4 - pGDI->GetTextWidth(s1),
					      yp,
					      UniVisual(s1)
				);
			}
		}

		DeflateRect(Rect, 4, 4);
	}

	INT nPos = m_Ctx.m_Data ? -45 : +45;

	DrawKnob(pGDI, Rect, nPos);
}

UINT CPrimLegacySelectorTwo::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgTouchInit:
		case msgTouchDown:

			return OnTouchDown(uParam);
	}

	return CPrimLegacySelector::OnMessage(uMsg, uParam);
}

// Message Handlers

UINT CPrimLegacySelectorTwo::OnTouchDown(UINT uParam)
{
	if( m_pValue ) {

		UINT Type = m_pValue->GetType();

		if( Type == typeReal ) {

			C3REAL Value = I2R(m_Ctx.m_Data) ? 0.0f : 1.0f;

			m_pValue->SetValue(R2I(Value), Type, setNone);

			Execute(m_pOnComplete);

			return TRUE;
		}

		if( Type == typeInteger ) {

			C3INT Value = C3INT(m_Ctx.m_Data) ? 0 : 1;

			m_pValue->SetValue(DWORD(Value), Type, setNone);

			Execute(m_pOnComplete);

			return TRUE;
		}
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Multi-State Selector Switch
//

// Constructor

CPrimLegacySelectorMulti::CPrimLegacySelectorMulti(void)
{
}

// Initialization

void CPrimLegacySelectorMulti::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacySelectorMulti", pData);

	CPrimLegacySelector::Load(pData);
}

// Overridables

void CPrimLegacySelectorMulti::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	int x1 = Rect.x1;
	int y1 = Rect.y1;
	int x2 = Rect.x2;
	int y2 = Rect.y2;

	UINT uCount = GetCount();

	UINT uIndex = NOTHING;

	CDispFormat *pFormat = NULL;

	if( GetDataFormat(pFormat) ) {

		CDispFormatMultiList *pList = ((CDispFormatMulti *) pFormat)->m_pList;

		uIndex = pList->FindData(m_Ctx.m_Data);
	}

	if( uIndex == NOTHING ) {

		uIndex = uCount - 1;
	}

	if( m_ShowStates && (y2 - y1) > (x2 - x1) ) {

		SetColors(pGDI, 2);

		if( !m_TagColor ) {

			m_pPanel->FillRect(pGDI, Rect);
		}
		else
			pGDI->FillRect(PassRect(Rect));

		Rect.y1 = y2 - (x2 - x1);

		SelectFont(pGDI, m_Font);

		int cy = pGDI->GetTextHeight(L"X");

		int yp = y1 + (Rect.y1 - y1 - cy) / 2;

		if( yp >= y1 && uCount ) {

			int dx = (x2 - x1) / (2 * uCount);

			CDispFormatMultiList *pList = ((CDispFormatMulti *) pFormat)->m_pList;

			for( UINT n = 0; n < uCount; n++ ) {

				CDispFormatMultiEntry *pEntry = (CDispFormatMultiEntry *) pList->GetItem(n);

				if( !pEntry->m_pText->GetText().IsEmpty() ) {

					CUnicode s = pEntry->m_pText->GetText();

					int     cx = pGDI->GetTextWidth(s);

					int     xp = x1 + dx * (1 + 2 * n) - cx/2;

					pGDI->TextOut(xp, yp, UniVisual(s));

					if( n == uIndex ) {

						pGDI->SelectPen(penFore);

						pGDI->DrawLine(xp, yp+cy, xp+cx, yp+cy);
					}
				}
			}
		}

		DeflateRect(Rect, 4, 4);
	}

	INT nPos = uCount > 1 ? (45 - uIndex * (90 / (uCount - 1))) : 45;

	DrawKnob(pGDI, Rect, nPos);
}

UINT CPrimLegacySelectorMulti::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgTouchInit:
		case msgTouchDown:

			return OnTouchDown(uParam);
	}

	return CPrimLegacySelector::OnMessage(uMsg, uParam);
}

// Message Handlers

UINT CPrimLegacySelectorMulti::OnTouchDown(UINT uParam)
{
	P2 Pos;

	Pos.x = LOWORD(uParam);

	Pos.y = HIWORD(uParam);

	if( PtInRect(m_Left, Pos) ) {

		GoLeft();

		m_fPressL = TRUE;

		return TRUE;
	}

	if( PtInRect(m_Right, Pos) ) {

		GoRight();

		m_fPressR = TRUE;

		return TRUE;
	}

	return FALSE;
}

// Implementation

void CPrimLegacySelectorMulti::GoLeft(void)
{
	if( m_pValue ) {

		CDispFormat *pFormat = NULL;

		if( GetDataFormat(pFormat) ) {

			CDispFormatMultiList *pList = ((CDispFormatMulti *) pFormat)->m_pList;

			UINT uCount = GetCount();

			UINT uFind  = pList->FindData(m_Ctx.m_Data);

			UINT uPrev  = pList->FindPrev(uFind, uCount);

			if( uPrev < NOTHING ) {

				CDispFormatMultiEntry *pItem = pList->GetItem(uPrev);

				UINT                    Type = m_pValue->GetType();

				DWORD                  Data  = pItem->m_pData->Execute(Type);

				m_pValue->SetValue(Data, Type, setNone);

				Execute(m_pOnComplete);
			}
		}

	}
}

void CPrimLegacySelectorMulti::GoRight(void)
{
	if( m_pValue ) {

		CDispFormat *pFormat = NULL;

		if( GetDataFormat(pFormat) ) {

			CDispFormatMultiList *pList = ((CDispFormatMulti *) pFormat)->m_pList;

			UINT uCount = GetCount();

			UINT uFind  = pList->FindData(m_Ctx.m_Data);

			UINT uNext  = pList->FindNext(uFind, uCount);

			if( uNext < NOTHING ) {

				CDispFormatMultiEntry *pItem = pList->GetItem(uNext);

				UINT                    Type = m_pValue->GetType();

				DWORD                  Data  = pItem->m_pData->Execute(Type);

				m_pValue->SetValue(Data, Type, setNone);

				Execute(m_pOnComplete);
			}
		}
	}
}

UINT CPrimLegacySelectorMulti::GetCount(void)
{
	UINT uCount = 0;

	CDispFormat *pFormat = NULL;

	if( GetDataFormat(pFormat) ) {

		CDispFormatMultiList *pList = ((CDispFormatMulti *) pFormat)->m_pList;

		for( UINT n = 0; n < pList->GetItemCount(); n++ ) {

			CDispFormatMultiEntry *pItem = pList->GetItem(n);

			if( !pItem->m_pText->GetText().IsEmpty() ) {

				uCount++;
			}
		}
	}

	return uCount;
}

// End of File
