
#include "intern.hpp"

#include "PrimRubyGaugeTypeAL.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Linear Gauge Primitive
//

// A lot of this code is in common with TypeAR, which makes me think
// we ought to move it into a base class. But how far back? Is it common
// to all gauges? Or maybe the cached redraw code is actually common
// to all Ruby primitives? It'll wait, but we should look at it...

// Constructor

CPrimRubyGaugeTypeAL::CPrimRubyGaugeTypeAL(void)
{
	m_pGdi = NULL;
	}

// Destructor

CPrimRubyGaugeTypeAL::~CPrimRubyGaugeTypeAL(void)
{
	if( m_pGdi ) {

		m_pGdi->Release();

		delete [] m_pData;

		delete [] m_pCopy;
		}
	}

// Initialization

void CPrimRubyGaugeTypeAL::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubyGaugeTypeAL", pData);

	CPrimRubyGaugeTypeA::Load(pData);

	if( !IsNaked() ) {

		m_cx    = m_bound.x2 - m_bound.x1;

		m_cy    = m_bound.y2 - m_bound.y1;

		m_pData = New DWORD [ m_cx * m_cy ];

		m_pCopy = New DWORD [ m_cx * m_cy ];

		AfxNewObject("gdi", IGdi, m_pGdi);

		m_pGdi->Create(m_cx, m_cy, m_pData);
		}

	m_PointStyle = GetByte(pData);

	LoadNumber(pData, m_lineFact);
	LoadNumber(pData, m_posMin);
	LoadNumber(pData, m_posMax);
	LoadNumber(pData, m_posPoint);
	LoadNumber(pData, m_posSweep);
	LoadNumber(pData, m_posMajor);
	LoadNumber(pData, m_posMinor);
	LoadNumber(pData, m_posOuter);
	LoadNumber(pData, m_posBug);
	LoadNumber(pData, m_posBand);
	LoadNumber(pData, m_posCenter);
	}

// Overridables

void CPrimRubyGaugeTypeAL::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRuby::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		TestScaleEtc();

		TestPointer();

		if( m_uChange >= 1 ) {

			if( IsNaked() ) {

				MakeMax(m_uChange, 4);
				}
			}

		if( m_uChange >= 2 ) {

			MakeMax(m_uChange, 2);

			m_fChange = TRUE;
			}

		(m_fChange ? Erase : Trans).Append(m_bound);
		}
	}

void CPrimRubyGaugeTypeAL::DrawExec(IGDI *pGDI, IRegion *pDirty)
{
	if( m_fShow ) {

		if( HitTest(pDirty) ) {
			
			MakeMax(m_uChange, 2);

			m_fChange = TRUE;
			}

		if( m_fChange ) {

			DrawPrim(pGDI);

			R2 Rect = GetBackRect();

			pDirty->AddRect(Rect);

			m_fChange = FALSE;
			}
		}
	}

void CPrimRubyGaugeTypeAL::DrawPrim(IGDI *pGdi)
{
	if( m_uChange == 4 ) {

		if( IsNaked() ) {

			// In naked mode, we can draw straight
			// on to the canvas. Not sure this is the
			// best way but until we've fixed the
			// gamma issue in the GDI, it looks better.

			DrawScaleEtc(pGdi);

			InitPaths();

			MakePaths();

			MakeLists();

			DrawPointer(pGdi);

			return;
			}

		// Everything changed so clear the buffer and
		// draw the bezel. Take a copy of the buffer
		// and then draw the scale etc. over the top.

		ArrayZero(m_pData, m_cx * m_cy);

		DrawBezel(m_pGdi);

		ArrayCopy(m_pCopy, m_pData, m_cx * m_cy);

		DrawScaleEtc(m_pGdi);
		}

	if( m_uChange == 3 ) {

		// Just the scale changed so restore the
		// buffer to just hold the bezel and then
		// draw the scale etc. ovet the top.

		ArrayCopy(m_pData, m_pCopy, m_cx * m_cy);

		DrawScaleEtc(m_pGdi);
		}

	if( m_uChange >= 1 ) {

		if( m_uChange >= 2 ) {

			// The pointer changed and so did the scale etc.
			// or we were asked to erase, so copy that from
			// our buffer and get ready to draw the pointer.

			pGdi->BitBlt( m_bound.x1,
				      m_bound.y1,
				      m_cx,
				      m_cy,
				      0,
				      PCBYTE(m_pData),
				      ropBlend
				      );

			// TODO -- We don't actually have to do this
			// if it's just an erase but does that matter?

			InitPaths();

			MakePaths();

			MakeLists();
			}
		else {
			// Only the pointer changed so find the minimum
			// reactangle that encloses the old pointer and
			// the New pointer and restore that from our
			// buffer. No need to alpha blend as we know that
			// this doesn't include any edges.

			R2 wipe;

			m_pathPoint.GetBoundingRect(wipe);

			InitPaths();

			MakePaths();

			MakeLists();

			m_pathPoint.AddBoundingRect(wipe);

			InflateRect(wipe, 1, 1);

			int xp = wipe.x1 - m_bound.x1;

			int yp = wipe.y1 - m_bound.y1;

			int cb = 4 * (m_bound.x2 - m_bound.x1);

			pGdi->BitBlt( wipe.x1,
				      wipe.y1,
				      wipe.x2 - wipe.x1,
				      wipe.y2 - wipe.y1,
				      cb,
				      PCBYTE(m_pData) + xp * 4 + yp * cb,
				      0
				      );
			}

		// Draw the pointer over the top.

		DrawPointer(pGdi);
		}

	m_uChange = 0;
	}

// Drawing

void CPrimRubyGaugeTypeAL::TestPointer(void)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_fChange = TRUE;

		MakeMax(m_uChange, 1);

		m_Ctx = Ctx;
		}
	}

void CPrimRubyGaugeTypeAL::DrawPointer(IGDI *pGdi)
{
	CRubyGdiLink gdi(pGdi);

	gdi.OutputSolid(m_listPoint, m_Ctx.m_PointColor, 0);
	}

// Path Management

void CPrimRubyGaugeTypeAL::InitPaths(void)
{
	m_pathPoint.Empty();
	}

void CPrimRubyGaugeTypeAL::MakePaths(void)
{
	PreparePoint();
	}

void CPrimRubyGaugeTypeAL::MakeLists(void)
{
	m_pathPoint.Transform(m_m1);

	m_listPoint.Load(m_pathPoint, true);
	}

// Scale Building

void CPrimRubyGaugeTypeAL::MakeScaleEtc(void)
{
	PrepareTicks();

	PrepareBand(m_pathBand1, m_BandShow1, CPrimRubyGaugeTypeA::m_Ctx.m_BandMin1, CPrimRubyGaugeTypeA::m_Ctx.m_BandMax1);

	PrepareBand(m_pathBand2, m_BandShow2, CPrimRubyGaugeTypeA::m_Ctx.m_BandMin2, CPrimRubyGaugeTypeA::m_Ctx.m_BandMax2);

	PrepareBug (m_pathBug1,  m_BugShow1,  CPrimRubyGaugeTypeA::m_Ctx.m_BugValue1);

	PrepareBug (m_pathBug2,  m_BugShow2,  CPrimRubyGaugeTypeA::m_Ctx.m_BugValue2);
	}

// Scaling

number CPrimRubyGaugeTypeAL::GetPos(number v)
{
	number a = GetValue(m_pMin, 0);

	number b = GetValue(m_pMax, 100);

	if( IsNAN(v) || IsNAN(a) || IsNAN(b) ) {

		return m_posMin;
		}

	if( !num_equal(a, b) ) {

		MakeMin(v, Max(a, b));

		MakeMax(v, Min(a, b));

		return m_posMin + (m_posMax - m_posMin) * (v - a) / (b - a);
		}

	return m_posMin;
	}

// Path Preparation

void CPrimRubyGaugeTypeAL::PrepareTicks(void)
{
	number s  = fabs(m_posMax - m_posMin) / (m_Major * m_Minor);

	int    q  = 0;

	number a1 = min(m_posMin, m_posMax);

	number a2 = max(m_posMin, m_posMax);

	for( number t = a1; t < a2 + s / 2; t += s, q += 1 ) {

		if( q % m_Minor ) {

			CRubyPoint p1(m_posCenter - m_posMinor, t);
			
			CRubyPoint p2(m_posCenter + m_posMinor, t);

			if( m_Minor % 2 == 0 && q % m_Minor == m_Minor / 2 ) {

				m_d.Line(m_pathMinor, p1, p2, m_lineFact * 1.5, CRubyStroker::endFlat);
				}
			else
				m_d.Line(m_pathMinor, p1, p2, m_lineFact * 1.0, CRubyStroker::endFlat);
			}
		else {
			CRubyPoint p1(m_posCenter - m_posMajor, t);
			
			CRubyPoint p2(m_posCenter + m_posMajor, t);

			m_d.Line(m_pathMajor, p1, p2, m_lineFact * 2.0, CRubyStroker::endFlat);
			}
		}
	}

void CPrimRubyGaugeTypeAL::PreparePoint(void)
{
	if( m_PointMode == 0 ) {

		number t = GetPos(m_Ctx.m_Value);

		CRubyPoint p1(m_posCenter - m_posPoint, t);
		CRubyPoint p2(m_posCenter + m_posPoint, t);
		
		m_d.Line(m_pathPoint, p1, p2, m_lineFact * 4.0, m_PointStyle, m_PointStyle);
		}

	if( m_PointMode == 1 ) {

		number t1 = GetPos(CPrimRubyGaugeTypeA::m_Ctx.m_Min);

		number t2 = GetPos(m_Ctx.m_Value);

		CRubyPoint p1(m_posCenter - m_posSweep, t1);
		CRubyPoint p2(m_posCenter + m_posSweep, t1);
		CRubyPoint p3(m_posCenter + m_posSweep, t2);
		CRubyPoint p4(m_posCenter - m_posSweep, t2);

		m_pathPoint.Append(p1);
		m_pathPoint.Append(p2);
		m_pathPoint.Append(p3);
		m_pathPoint.Append(p4);

		m_pathPoint.AppendHardBreak();
		}
	}

BOOL CPrimRubyGaugeTypeAL::PrepareBand(CRubyPath &path, BOOL fShow, number minValue, number maxValue)
{
	if( fShow ) {

		number t1 = GetPos(minValue);
		number t2 = GetPos(maxValue);

		CRubyPoint p1(m_posCenter + m_posOuter, t1);
		CRubyPoint p2(m_posCenter + m_posBand,  t1);
		CRubyPoint p3(m_posCenter + m_posBand,  t2);
		CRubyPoint p4(m_posCenter + m_posOuter, t2);

		path.Append(p1);
		path.Append(p2);
		path.Append(p3);
		path.Append(p4);

		path.AppendHardBreak();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRubyGaugeTypeAL::PrepareBug(CRubyPath &path, BOOL fShow, number bugValue)
{
	if( fShow ) {

		number t1 = GetPos(bugValue);
		number t2 = t1 - 4;
		number t3 = t1 + 4;

		CRubyPoint p1(m_posCenter - m_posOuter, t1);
		CRubyPoint p2(m_posCenter - m_posBug,   t2);
		CRubyPoint p3(m_posCenter - m_posBug,   t3);

		path.Append(p1);
		path.Append(p2);
		path.Append(p3);

		path.AppendHardBreak();

		return TRUE;
		}

	return FALSE;
	}

// Context Creation

void CPrimRubyGaugeTypeAL::FindCtx(CCtx &Ctx)
{
	Ctx.m_Value      = GetValue(m_pValue, 25);
	
	Ctx.m_PointColor = m_pPointColor->GetColor();
	}

// Context Check

BOOL CPrimRubyGaugeTypeAL::CCtx::operator == (CCtx const &That) const
{
	// TODO -- Avoid de minimis changes?

	return m_Value      == That.m_Value      &&
	       m_PointColor == That.m_PointColor ;
	}

// End of File
