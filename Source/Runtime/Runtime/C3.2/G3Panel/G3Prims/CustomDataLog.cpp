
#include "intern.hpp"

#include "CustomDataLog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Custom Data Log File
//

// Constructor

CCustomDataLog::CCustomDataLog(UINT uChans)
{
	m_uChans   = uChans;

	m_dwStart  = 0;

	m_dwPitch  = 10;

	m_dwMin    = 0;

	m_dwMax    = 100;

	m_dwDPs    = 2;

	m_uSamps   = 8000;

	m_pFile    = Create_File();

	m_pFileMan = AfxGetSingleton(IFileManager);

	m_pFileSys = NULL;

	//

	m_dwStart  = (GetNow() * 5) - (m_dwPitch * 5) * m_uSamps;
	}

// Destructor

CCustomDataLog::~CCustomDataLog(void)
{
	m_pFile->Release();
	}

// Operations

BOOL CCustomDataLog::MakeFile(PCTXT pName)
{
	LockSession();

	if( m_pFile->Open(pName, fileWrite) || m_pFile->Create(pName) ) {

		WriteHead();

		WriteForm();

		WriteData();

		m_pFile->Close();	

		AfxTrace("file pass\n");

		FreeSession();

		return TRUE;
		}

	AfxTrace("file fail\n");

	FreeSession();

	return FALSE;
	}

// Implementation

void CCustomDataLog::WriteHead(void)
{
	DWORD dwHead = 0xFF00DDFF;	

	m_pFile->Write(PBYTE(&dwHead),    sizeof(dwHead));

	m_pFile->Write(PBYTE(&m_uChans),  sizeof(m_uChans));

	m_pFile->Write(PBYTE(&m_dwStart), sizeof(m_dwStart));

	m_pFile->Write(PBYTE(&m_dwPitch), sizeof(m_dwPitch));

	m_pFile->Write(PBYTE(&m_uSamps),  sizeof(m_uSamps));
	}

void CCustomDataLog::WriteForm(void)
{
	for( UINT uChan = 0; uChan < m_uChans; uChan ++ ) {

		m_pFile->Write(PBYTE(&m_dwMin), sizeof(m_dwMin));

		m_pFile->Write(PBYTE(&m_dwMax), sizeof(m_dwMax));

		m_pFile->Write(PBYTE(&m_dwDPs), sizeof(m_dwDPs));
		}
	}

void CCustomDataLog::WriteData(void)
{
	UINT uRamp = 0;

	PDWORD pData = New DWORD [ m_uChans ];

	memset(pData, 0, m_uChans * sizeof(DWORD));

	for( UINT uSamp = 0; uSamp < m_uSamps; uSamp ++ ) {

		for( UINT uChan = 0; uChan < m_uChans; uChan ++ ) {

			switch( uChan ) {

				case 0:
					pData[uChan] = (pData[uChan] + 1) % m_dwMax;
					break;

				case 1:
					pData[uChan] = rand() % m_dwMax;
					break;

				case 2:
					pData[uChan] = (pData[0] + pData[1]) / 2;
					break;

				case 3:
					pData[uChan] = (uSamp & 0x07) > 3 ? m_dwMin : m_dwMax;
					break;

				case 4:
					pData[uChan] = rand() % (m_dwMax / 3);
					break;

				case 5:
					pData[uChan] = (m_dwMax / 2) + rand() % (m_dwMax / 4);
					break;
				
				default:
					pData[uChan] = 0;
					break;
				}
			}

		m_pFile->Write(PBYTE(pData), sizeof(DWORD) * m_uChans);

		if( !(uSamp % 1000) ) {
				
			Sleep(50);
			}
		}

	delete [] pData;
	}

void CCustomDataLog::LockSession(void)
{
	for(;;) {

		m_pFileSys = m_pFileMan->LockFileSystem();
	
		if( m_pFileSys ) {

			break;
			}

		Sleep(1000);
		}
	}

void CCustomDataLog::FreeSession(void)
{
	m_pFileMan->FreeFileSystem(m_pFileSys, true);
	}

// End of File
