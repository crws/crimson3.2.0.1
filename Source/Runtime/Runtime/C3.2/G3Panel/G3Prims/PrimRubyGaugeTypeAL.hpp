
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeTypeAL_HPP
	
#define	INCLUDE_PrimRubyGaugeTypeAL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeTypeA.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Linear Gauge Primitive
//

class CPrimRubyGaugeTypeAL : public CPrimRubyGaugeTypeA
{
	public:
		// Constructor
		CPrimRubyGaugeTypeAL(void);

		// Destructor
		~CPrimRubyGaugeTypeAL(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawExec(IGDI *pGDI, IRegion *pDirty);
		void DrawPrim(IGDI *pGDI);

		// Drawing
		void TestPointer(void);
		void DrawPointer(IGDI *pGDI);

		// Data Members
		UINT  m_PointStyle;

	protected:
		// Data Members
		int	      m_cx;
		int	      m_cy;
		IGdi        * m_pGdi;
		PDWORD        m_pData;
		PDWORD        m_pCopy;
		number	      m_lineFact;
		number	      m_posMin;
		number	      m_posMax;
		number	      m_posPoint;
		number	      m_posSweep;
		number	      m_posMajor;
		number	      m_posMinor;
		number        m_posOuter;
		number	      m_posBug;
		number	      m_posBand;
		number	      m_posCenter;
		CRubyPath     m_pathPoint;
		CRubyGdiList  m_listPoint;

		// Context Record
		struct CCtx
		{
			// Data Members
			number m_Value;
			COLOR  m_PointColor;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Path Management
		void InitPaths(void);
		void MakePaths(void);
		void MakeLists(void);

		// Scale Building
		void MakeScaleEtc(void);

		// Scaling
		number GetPos(number v);

		// Path Preparation
		void PrepareTicks(void);
		void PreparePoint(void);
		BOOL PrepareBand(CRubyPath &path, BOOL fShow, number minValue, number maxValue);
		BOOL PrepareBug(CRubyPath &path, BOOL fShow, number bugValue);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

// End of File

#endif
