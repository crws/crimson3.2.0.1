
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyBrush_HPP
	
#define	INCLUDE_RubyBrush_HPP

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Brush
//

class DLLAPI CPrimRubyBrush : public CCodedHost
{
	public:
		// Constructor
		CPrimRubyBrush(void);

		// Destructor
		~CPrimRubyBrush(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsNull(void) const;

		// Operations
		void SetScan(UINT Code);
		BOOL DrawPrep(IGDI *pGDI);

		// Drawing
		BOOL Fill(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver);

		// Item Properties
		UINT         m_Pattern;
		CPrimColor * m_pColor1;
		CPrimColor * m_pColor2;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR m_Color1;
			COLOR m_Color2;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Implementation
		void DoLoad(PCBYTE &pData);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

// End of File

#endif
