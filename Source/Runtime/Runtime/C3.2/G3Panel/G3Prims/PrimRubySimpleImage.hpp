
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubySimpleImage_HPP
	
#define	INCLUDE_PrimRubySimpleImage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPrimRubyShade;

//////////////////////////////////////////////////////////////////////////
//
// Ruby Image Primitive
//

class CPrimRubySimpleImage : public CPrimRubyWithText
{
	public:
		// Constructor
		CPrimRubySimpleImage(void);

		// Destructor
		~CPrimRubySimpleImage(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		CPrimImage     * m_pImage;
		CCodedItem     * m_pColor;
		CPrimRubyShade * m_pShade;

	protected:
		// Data Members
		R2 m_ImageRect;

		// Context Record
		struct CCtx
		{
			// Data Members
			BOOL m_fColor;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Implementation
		void FindImageRect(void);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

// End of File

#endif
