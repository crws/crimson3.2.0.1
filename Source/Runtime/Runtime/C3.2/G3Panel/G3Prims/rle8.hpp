
//////////////////////////////////////////////////////////////////////////
//
// G3 Bitmap Compressor
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RLE8_HPP

#define	INCLUDE_RLE8_HPP

//////////////////////////////////////////////////////////////////////////
//
// Windows RLE8 Compressor
//

extern	UINT	MakeRLE8(PBYTE pComp, PCBYTE pData, UINT uWidth, BOOL fFirst, BOOL fLast);

//////////////////////////////////////////////////////////////////////////
//
// Segment Selection
//

#if defined(_M_M68K)

#define RLE8 __attribute__ ((section (".rle8")))

#else

#define	RLE8 /**/

#endif

// End of File

#endif
