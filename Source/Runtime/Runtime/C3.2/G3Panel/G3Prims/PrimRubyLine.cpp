
#include "intern.hpp"

#include "PrimRubyLine.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyPenLine.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Line Primitive
//

// Constructor

CPrimRubyLine::CPrimRubyLine(void)
{
	m_pLine = New CPrimRubyPenLine;

	m_pEdge = New CPrimRubyPenEdge;
	}

// Destructor

CPrimRubyLine::~CPrimRubyLine(void)
{
	delete m_pLine;

	delete m_pEdge;
	}

// Initialization

void CPrimRubyLine::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubyLine", pData);

	CPrimRuby::Load(pData);

	m_fFast = GetByte(pData);

	m_pLine->Load(pData);
	m_pEdge->Load(pData);

	LoadList(pData, m_listLine);
	LoadList(pData, m_listEdge);
	LoadList(pData, m_listTrim);
	}

// Overridables

void CPrimRubyLine::SetScan(UINT Code)
{
	m_pLine->SetScan(Code);

	m_pEdge->SetScan(Code);

	CPrimRuby::SetScan(Code);
	}

void CPrimRubyLine::MovePrim(int cx, int cy)
{
	m_listLine.Translate(cx, cy, !m_fFast);

	m_listEdge.Translate(cx, cy, true);

	m_listTrim.Translate(cx, cy, true);

	CPrimRuby::MovePrim(cx, cy);
	}

void CPrimRubyLine::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRuby::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		if( m_pLine->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		(m_fChange ? Erase : Trans).Append(m_bound);
		}
	}

void CPrimRubyLine::DrawPrim(IGDI *pGDI)
{
	m_pLine->Fill(pGDI, m_listLine, !m_fFast);

	m_pEdge->Fill(pGDI, m_listEdge, TRUE);

	m_pEdge->Trim(pGDI, m_listTrim, TRUE);
	}

// End of File
