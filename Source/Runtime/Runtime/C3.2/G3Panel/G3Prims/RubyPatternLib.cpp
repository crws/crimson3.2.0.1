
#include "intern.hpp"

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Pattern Library
//

// Static Data

BOOL  CRubyPatternLib::m_ShadeMode = 0;

COLOR CRubyPatternLib::m_ShadeCol1 = 0;

COLOR CRubyPatternLib::m_ShadeCol2 = 0;

// Constructors

CRubyPatternLib::CRubyPatternLib(void)
{
	}

// Destructors

CRubyPatternLib::~CRubyPatternLib(void)
{
	}

// Initialization

void CRubyPatternLib::Load(PCBYTE &pData)
{
	ValidateLoad("CRubyPatternLib", pData);

	for( UINT n = 0; n < 128; n++ ) {

		m_List[n] = GetWord(pData);
		}
	}

// Gdi Configuration

PSHADER CRubyPatternLib::SetGdi(IGdi *pGdi, UINT uCode, COLOR Fore, COLOR Back)
{
	if( uCode <= 14 ) {

		pGdi->SelectBrush(uCode);

		pGdi->SetBrushFore(Fore);

		pGdi->SetBrushBack(Back);

		return NULL;
		}

	if( uCode <= 19 ) {

		pGdi->SelectBrush(brushFore);

		m_ShadeMode = uCode % 4 / 2;

		m_ShadeCol1 = Fore;

		m_ShadeCol2 = Back;
			
		return (uCode % 2) ? ShaderLinear : ShaderMiddle;
		}

	if( uCode == 20 ) {

		pGdi->SelectBrush(brushFore);

		m_ShadeCol1 = Fore;

		m_ShadeCol2 = Back;
			
		return ShaderChrome;
		}

	if( uCode == 21 ) {

		pGdi->SelectBrush(brushFore);

		m_ShadeCol1 = Fore;

		m_ShadeCol2 = Back;
			
		return ShaderHorzCylinder;
		}

	if( uCode == 22 ) {

		pGdi->SelectBrush(brushFore);

		m_ShadeCol1 = Fore;

		m_ShadeCol2 = Back;
			
		return ShaderVertCylinder;
		}

	if( uCode >= 128 && uCode <= 160 ) {

		WORD   hItem = m_List[uCode - 128];

		PCBYTE pData = PCBYTE(g_pDbase->LockItem(hItem));

		if( pData ) {

			if( GetWord(pData) == IDC_IMAGE && GetWord(pData) == 0x1234 ) {

				if( GetWord(pData) == 0x0001 ) {

					int  xAct  = GetWord(pData);

					int  yAct  = GetWord(pData);

					BOOL fTile = GetByte(pData);

					GetByte(pData);

					pGdi->SetBrushBits(pData, xAct, yAct);

					return NULL;
					}
				}
			}

		pGdi->SelectBrush(brushNull);

		return NULL;
		}

	pGdi->SelectBrush(brushNull);

	return NULL;
	}

void CRubyPatternLib::Release(IGdi *pGdi, UINT uCode)
{
	if( uCode >= 128 && uCode <= 160 ) {

		pGdi->SelectBrush(brushNull);

		WORD hItem = m_List[uCode - 128];

		g_pDbase->FreeItem(hItem);
		}
	}

// Color Mixing

COLOR CRubyPatternLib::Mix16(COLOR a, COLOR b, int p, int c)
{
	if( c ) {

		if( p <= 0 ) {

			return a;
			}

		if( p >= c ) {

			return b;
			}

		int ra = GetRED  (a);
		int ga = GetGREEN(a);
		int ba = GetBLUE (a);

		int rb = GetRED  (b);
		int gb = GetGREEN(b);
		int bb = GetBLUE (b);

		int rc = ra + ((rb - ra) * p / c);
		int gc = ga + ((gb - ga) * p / c);
		int bc = ba + ((bb - ba) * p / c);

		return GetRGB(rc, gc, bc);
		}

	return a;
	}

DWORD CRubyPatternLib::Mix32(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	int rb = GetRED  (b);
	int gb = GetGREEN(b);
	int bb = GetBLUE (b);

	ra = (ra << 3) | (ra >> 2);
	ga = (ga << 3) | (ga >> 2);
	ba = (ba << 3) | (ba >> 2);

	rb = (rb << 3) | (rb >> 2);
	gb = (gb << 3) | (gb >> 2);
	bb = (bb << 3) | (bb >> 2);

	if( c ) {

		if( p <= 0 ) {

			return MAKELONG(MAKEWORD(ba, ga), MAKEWORD(ra, 0xFF));
			}

		if( p >= c ) {

			return MAKELONG(MAKEWORD(bb, gb), MAKEWORD(rb, 0xFF));
			}

		int rc = ra + ((rb - ra) * p / c);
		int gc = ga + ((gb - ga) * p / c);
		int bc = ba + ((bb - ba) * p / c);

		return MAKELONG(MAKEWORD(bc, gc), MAKEWORD(rc, 0xFF));
		}

	return MAKELONG(MAKEWORD(ba, ga), MAKEWORD(ra, 0xFF));
	}

// Shaders

BOOL CRubyPatternLib::ShaderLinear(IGDI *pGdi, int p, int c)
{
	static COLOR q16 = 0;
	static DWORD q32 = 0;
	static BOOL  f32 = 0;

	if( c ) {

		if( f32 ) {

			DWORD k32 = Mix32(m_ShadeCol1, m_ShadeCol2, p, c);

			if( k32 - q32 ) {

				pGdi->SetBrushFore(k32);

				q32 = k32;

				return TRUE;
				}
			}
		else {
			COLOR k16 = Mix16(m_ShadeCol1, m_ShadeCol2, p, c);

			if( k16 - q16 ) {

				pGdi->SetBrushFore(k16);

				q16 = k16;

				return TRUE;
				}
			}

		return FALSE;
		}

	f32 = (pGdi->GetColorFormat() == colorRGB888);

	q16 = 0xFFFF;

	q32 = 0x00000000;

	return m_ShadeMode;
	}

BOOL CRubyPatternLib::ShaderMiddle(IGDI *pGdi, int p, int c)
{
	static COLOR q16 = 0;
	static DWORD q32 = 0;
	static BOOL  f32 = 0;

	if( c ) {

		if( f32 ) {

			DWORD k32 = Mix32(m_ShadeCol2, m_ShadeCol1, abs(c/2-p), c/2);

			if( k32 - q32 ) {

				pGdi->SetBrushFore(k32);

				q32 = k32;

				return TRUE;
				}
			}
		else {
			COLOR k16 = Mix16(m_ShadeCol2, m_ShadeCol1, abs(c/2-p), c/2);

			if( k16 - q16 ) {

				pGdi->SetBrushFore(k16);

				q16 = k16;

				return TRUE;
				}
			}

		return FALSE;
		}

	f32 = (pGdi->GetColorFormat() == colorRGB888);

	q16 = 0xFFFF;

	q32 = 0x00000000;

	return m_ShadeMode;
	}

BOOL CRubyPatternLib::ShaderTable(IGdi *pGdi, int p, int c, int size, int const *pos, int const *col)
{
	static COLOR q16 = 0;
	static DWORD q32 = 0;
	static BOOL  f32 = 0;

	static COLOR ca  = 0;
	static COLOR cb  = 0;
	static UINT  n   = 0;
	
	if( c ) {

		int pc = (c - p) * 1000 / c;

		if( n ) {

			if( pc <= pos[n] ) {

				while( pc <= pos[n] ) {
			
					n--;
					}

				ca = Mix16(m_ShadeCol1, m_ShadeCol2, col[n+0], 31);
				
				cb = Mix16(m_ShadeCol1, m_ShadeCol2, col[n+1], 31);
				}
			}

		if( f32 ) {

			DWORD k32 = Mix32(ca, cb, pc - pos[n], pos[n+1] - pos[n]);

			if( k32 - q32 ) {

				pGdi->SetBrushFore(k32);

				q32 = k32;

				return TRUE;
				}
			}
		else {
			COLOR k16 = Mix16(ca, cb, pc - pos[n], pos[n+1] - pos[n]);

			if( k16 - q16 ) {

				pGdi->SetBrushFore(k16);

				q16 = k16;

				return TRUE;
				}
			}

		return FALSE;
		}

	f32 = (pGdi->GetColorFormat() == colorRGB888);

	q16 = 0xFFFF;

	q32 = 0x00000000;

	n   = size - 1;

	return FALSE;
	}

BOOL CRubyPatternLib::ShaderChrome(IGdi *pGdi, int p, int c)
{
	static int pos[] = { 0, 250, 300, 400, 500, 900, 1000 };
	static int col[] = { 0,  10,   2,  31,  29,  16,   31 };

	BOOL f = ShaderTable(pGdi, p, c, elements(pos), pos, col);

	return c ? f : FALSE;
	}

BOOL CRubyPatternLib::ShaderHorzCylinder(IGdi *pGdi, int p, int c)
{
	static int pos[] = { 0,	100, 200, 300, 400, 500, 600, 700, 800,	900, 1000 };
	static int col[] = { 0,	 19,  25,  28,	30,  31,  30,  28,  25,	 19,	0 };

	BOOL f = ShaderTable(pGdi, p, c, elements(pos), pos, col);

	return c ? f : FALSE;
	}

BOOL CRubyPatternLib::ShaderVertCylinder(IGdi *pGdi, int p, int c)
{
	static int pos[] = { 0,	100, 200, 300, 400, 500, 600, 700, 800,	900, 1000 };
	static int col[] = { 0,	 19,  25,  28,	30,  31,  30,  28,  25,	 19,	0 };

	BOOL f = ShaderTable(pGdi, p, c, elements(pos), pos, col);

	return c ? f : TRUE;
	}

// End of File
