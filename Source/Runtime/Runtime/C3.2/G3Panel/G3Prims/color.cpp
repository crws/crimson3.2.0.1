
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Color
//

// Constructor

CPrimColor::CPrimColor(COLOR NotAvail)
{
	m_NotAvail = NotAvail;
	}

// Destructor

CPrimColor::~CPrimColor(void)
{
	}

// Initialization

void CPrimColor::Load(PCBYTE &pData)
{
	CCodedItem::Load(pData);
	}

// Color Access

COLOR CPrimColor::GetColor(void)
{
	if( !IsEmpty() ) {

		if( m_uCode == 8 ) {

			if( m_pCode[4] == 0xB3 ) {

				BYTE hi = PBYTE(m_pCode + 5)[0];

				BYTE lo = PBYTE(m_pCode + 5)[1];

				return MAKEWORD(lo, hi);
				}
			}

		if( IsAvail() ) {

			DWORD r = ExecVal();

			return COLOR(r);
			}

		return m_NotAvail;
		}

	return 0xFFFF;
	}

// End of File
