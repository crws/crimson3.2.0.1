
#include "intern.hpp"

#include "viewer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Generic Viewer
//

// Hair Space

#define textHair L"\x200A"

// Constructor

CPrimViewer::CPrimViewer(void)
{
	m_pBack    = New CPrimColor(naBack);

	m_FontHead = fontHei16;

	m_FontText = fontHei16;

	m_FontMenu = fontHei16;

	m_uEnable  = 0;

	m_uPress   = NOTHING;

	m_fInit    = FALSE;
	}

// Destructor

CPrimViewer::~CPrimViewer(void)
{
	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		delete m_List[n].m_pFill;

		delete m_List[n].m_pHilite;

		delete m_List[n].m_pShadow;
		}

	delete m_pBack;
	}

// Initialization

void CPrimViewer::Load(PCBYTE &pData)
{
	CPrim::Load(pData);

	m_FontHead = GetWord(pData);

	m_FontText = m_FontHead;

	m_FontMenu = GetWord(pData);

	m_pBack->Load(pData);
	}

// Overridables

void CPrimViewer::SetScan(UINT Code)
{
	m_pBack->SetScan(Code);

	CPrim::SetScan(Code);
	}

void CPrimViewer::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( !m_fInit ) {

		OnMakeList();

		FindLayout(pGDI);

		MakeButtonLists();

		m_fInit = TRUE;
		}

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}
		}

	CPrim::DrawPrep(pGDI, Erase, Trans);
	}

void CPrimViewer::DrawPrim(IGDI *pGDI)
{
	pGDI->ResetBrush();

	pGDI->SetBrushFore(m_pBack->GetColor());

	pGDI->FillRect(PassRect(m_Work));

	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->FillRect(PassRect(m_Menu));

	SelectFont(pGDI, m_FontMenu);

	pGDI->SetTextTrans(modeTransparent);

	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		R2 Rect;

		FindButton(Rect, n, c);

		UINT    uCode  = m_List[n].m_uCode;

		CUnicode Label = UniVisual(m_List[n].m_pLabel->GetText());

		BOOL   fPress  = (m_Ctx.m_uPress == uCode);

		BOOL   fEnable = (m_Ctx.m_uEnable & (1 << uCode));

		DrawButton(pGDI, n, Rect, Label, fPress, fEnable);
		}
	}

void CPrimViewer::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_DrawRect));
	}

UINT CPrimViewer::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgTakeFocus:

			return OnTakeFocus(uMsg, uParam);

		case msgKillFocus:

			if( m_uPress < NOTHING ) {

				OnTouchUp();

				return TRUE;
				}
			
			return FALSE;

		case msgTouchInit:
		case msgTouchDown:

			return OnTouchDown(uParam);

		case msgTouchRepeat:

			return OnTouchRepeat();

		case msgTouchUp:

			return OnTouchUp();
		}

	return CPrim::OnMessage(uMsg, uParam);
	}

// Message Handlers

BOOL CPrimViewer::OnTakeFocus(UINT uMsg, UINT uParam)
{
	return TRUE;
	}

BOOL CPrimViewer::OnTouchDown(UINT uParam)
{
	P2 Pos;
	
	Pos.x = LOWORD(uParam);
	
	Pos.y = HIWORD(uParam);

	if( PtInRect(m_Work, Pos) ) {

		if( OnTouchWork(Pos) ) {

			return TRUE;
			}

		return FALSE;
		}
	else {
		UINT c = m_List.GetCount();

		for( UINT n = 0; n < c; n++ ) {

			UINT uCode = m_List[n].m_uCode;

			if( m_Ctx.m_uEnable & (1 << uCode) ) {

				R2 Rect;

				FindButton(Rect, n, c);

				if( PtInRect(Rect, Pos) ) {

					m_uPress = uCode;

					OnBtnDown(m_uPress);

					return TRUE;
					}
				}
			}

		return FALSE;
		}
	}

BOOL CPrimViewer::OnTouchRepeat(void)
{
	if( m_Ctx.m_uEnable & (1 << m_uPress) ) {

		OnBtnRepeat(m_uPress);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimViewer::OnTouchUp(void)
{
	OnBtnUp(m_uPress);

	m_uPress = NOTHING;

	return TRUE;
	}

// Overridables

BOOL CPrimViewer::OnMakeList(void)
{
	return FALSE;
	}

BOOL CPrimViewer::OnEnable(void)
{
	return FALSE;
	}

BOOL CPrimViewer::OnBtnDown(UINT n)
{
	return FALSE;
	}

BOOL CPrimViewer::OnBtnRepeat(UINT n)
{
	return FALSE;
	}

BOOL CPrimViewer::OnBtnUp(UINT n)
{
	return FALSE;
	}

BOOL CPrimViewer::OnTouchWork(P2 Pos)
{
	return FALSE;
	}

// Layout Calculation

void CPrimViewer::FindLayout(IGDI *pGDI)
{
	m_Menu = m_DrawRect;

	m_Work = m_Menu;

	m_Menu.top    = m_Menu.bottom - GetButtonHeight(pGDI);

	m_Work.bottom = m_Menu.top;
	}

void CPrimViewer::FindButton(R2 &Rect, UINT n, UINT c)
{
	Rect.x1 = m_Menu.x1 + (n + 0) * (m_Menu.x2 - m_Menu.x1) / c + 1;
	
	Rect.x2 = m_Menu.x1 + (n + 1) * (m_Menu.x2 - m_Menu.x1) / c - 1;

	Rect.y1 = m_Menu.y1 + 1;

	Rect.y2 = m_Menu.y2 - 1;
	}

void CPrimViewer::MakeButtonLists(void)
{
	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		R2 Rect;

		FindButton(Rect, n, c);

		R2 OuterRect = Rect;

		R2 InnerRect = OuterRect;

		int cx = OuterRect.x2 - OuterRect.x1;

		int bb = 4;

		DeflateRect(InnerRect, bb, bb);

		CRubyPoint p1(OuterRect, 1);
		CRubyPoint p2(OuterRect, 3);
		CRubyPoint w1(InnerRect, 1);
		CRubyPoint w2(InnerRect, 3);

		CRubyPath pathFill, pathHilite, pathShadow;

		CRubyDraw::Rectangle(pathFill, w1, w2);

		////////

		pathHilite.Append(CRubyPoint(p1.m_x, p1.m_y));
		pathHilite.Append(CRubyPoint(w1.m_x, w1.m_y));
		pathHilite.Append(CRubyPoint(w1.m_x, w2.m_y));
		pathHilite.Append(CRubyPoint(p1.m_x, p2.m_y));
	
		pathHilite.AppendHardBreak();

		pathHilite.Append(CRubyPoint(p1.m_x, p1.m_y));
		pathHilite.Append(CRubyPoint(p2.m_x, p1.m_y));
		pathHilite.Append(CRubyPoint(w2.m_x, w1.m_y));
		pathHilite.Append(CRubyPoint(w1.m_x, w1.m_y));
	
		pathHilite.AppendHardBreak();

		////////

		pathShadow.Append(CRubyPoint(p2.m_x, p1.m_y));
		pathShadow.Append(CRubyPoint(p2.m_x, p2.m_y));
		pathShadow.Append(CRubyPoint(w2.m_x, w2.m_y));
		pathShadow.Append(CRubyPoint(w2.m_x, w1.m_y));
	
		pathShadow.AppendHardBreak();

		pathShadow.Append(CRubyPoint(p2.m_x, p2.m_y));
		pathShadow.Append(CRubyPoint(p1.m_x, p2.m_y));
		pathShadow.Append(CRubyPoint(w1.m_x, w2.m_y));
		pathShadow.Append(CRubyPoint(w2.m_x, w2.m_y));
	
		pathShadow.AppendHardBreak();

		////////

		m_List[n].m_pFill  ->Load(pathFill,   true);

		m_List[n].m_pHilite->Load(pathHilite, true);

		m_List[n].m_pShadow->Load(pathShadow, true);
		}
	}

// Drawing Helpers

int CPrimViewer::GetButtonHeight(IGDI *pGDI)
{
	SelectFont(pGDI, m_FontMenu);

	return pGDI->GetTextHeight(L"X") + 16;
	}

void CPrimViewer::DrawButton(IGDI *pGDI, UINT n, R2 Rect, CUnicode const &Text, BOOL fPress, BOOL fEnable)
{
	pGDI->SetTextFore (fEnable ? GetRGB(0,0,0) : GetRGB(18,18,18));

	CRubyGdiLink gdi(pGDI);

	if( fPress ) {

		gdi.OutputSolid(*m_List[n].m_pShadow, GetRGB(24,24,24), 255);

		gdi.OutputSolid(*m_List[n].m_pHilite, GetRGB( 8, 8, 8), 255);
		}
	else {
		gdi.OutputSolid(*m_List[n].m_pHilite, GetRGB(24,24,24), 255);

		gdi.OutputSolid(*m_List[n].m_pShadow, GetRGB( 8, 8, 8), 255);
		}

	gdi.OutputSolid(*m_List[n].m_pFill,   GetRGB(16,16,16), 255);

	int cx = pGDI->GetTextWidth (Text);

	int cy = pGDI->GetTextHeight(Text);

	int xp = Rect.x1 + (Rect.x2 - Rect.x1 - cx) / 2;

	int yp = Rect.y1 + (Rect.y2 - Rect.y1 - cy) / 2;

	if( fPress ) {

		xp += 1;

		yp += 1;
		}

	pGDI->TextOut(xp, yp+1, Text);
	}

void CPrimViewer::MakeDigitsFixed(CUnicode &Text)
{
	MakeDigitsFixed(PUTF(PCUTF(Text)));
	}

void CPrimViewer::MakeDigitsFixed(PUTF pText)
{
	while( *pText ) {

		if( *pText >= digitSimple && *pText < digitSimple + 10 ) {

			*pText -= digitSimple;

			*pText += digitFixed;
			}

		pText++;
		}
	}

// Button List

void CPrimViewer::AddButton(CCodedText *pText, UINT uCode)
{
	CButtonDef Button;

	Button.m_pLabel  = pText;

	Button.m_uCode   = uCode;

	Button.m_pFill   = New CRubyGdiList;

	Button.m_pHilite = New CRubyGdiList;

	Button.m_pShadow = New CRubyGdiList;

	m_List.Append(Button);
	}

// Context Handling

void CPrimViewer::FindCtx(CCtx &Ctx)
{
	m_uEnable = 0;

	OnEnable();

	Ctx.m_uPress  = m_uPress;

	Ctx.m_uEnable = m_uEnable;
	}

// Context Check

BOOL CPrimViewer::CCtx::operator == (CCtx const &That) const
{
	return m_uPress  == That.m_uPress  &&
	       m_uEnable == That.m_uEnable ;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Viewer Unsupported
//

CPrimViewerUnsupported::CPrimViewerUnsupported(void)
{
	}

// Initialization

void CPrimViewerUnsupported::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimViewerUnsupported", pData);

	CPrimViewer::Load(pData);

	UINT uSize = GetWord(pData);

	pData     += uSize;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Generic List Viewer
//

// Constructor

CPrimListViewer::CPrimListViewer(void)
{
	m_fPage      = FALSE;
	m_fHead      = FALSE;
	m_fInit      = FALSE;
	m_Ctx.m_uSeq = 0;
	m_uTimer     = 0;
	m_uTimeout   = 30;
	m_nLines     = 0;
	m_nNewTop    = 0;
	m_nNewPos    = -1;
	m_fFocus     = FALSE;
	}

// Destructor

CPrimListViewer::~CPrimListViewer(void)
{
	}

// Overridables

void CPrimListViewer::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( !m_fInit ) {

		CPrimViewer::DrawPrep(pGDI, Erase, Trans);

		FindLayout(pGDI);

		m_fInit = TRUE;
		}

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			BOOL fSave = FALSE;

			if( Ctx.m_nPos != m_Ctx.m_nPos ) {

				fSave = TRUE;
				}

			if( Ctx.m_uSeq != m_Ctx.m_uSeq ) {

				ListGetData(Ctx.m_nTop);

				if( ListKeepPos(Ctx.m_nTop, Ctx.m_nPos) ) {

					if( Ctx.m_nPos < 0 ) {

						Ctx.m_nTop = 0;
						}
					else {
						MakeMin(Ctx.m_nTop, Ctx.m_nPos);
				
						MakeMax(Ctx.m_nTop, Ctx.m_nPos - m_nRows + 1);
						}
					}
				else {
					if( Ctx.m_nPos >= m_nLines ) {

						if( m_nLines ) {

							Ctx.m_nPos = m_nLines - 1;

							MakeMin(Ctx.m_nTop, Ctx.m_nPos);
				
							MakeMax(Ctx.m_nTop, Ctx.m_nPos - m_nRows + 1);
							}
						else {
							Ctx.m_nPos = -1;

							Ctx.m_nTop =  0;
							}
						}
					}

				fSave = TRUE;
				}
			else {
				if( Ctx.m_nTop != m_Ctx.m_nTop ) {

					ListGetData(Ctx.m_nTop);
					}
				}

			if( fSave ) {

				ListSavePos(Ctx.m_nPos, Ctx.m_nTop);
				}
			
			m_Ctx     = Ctx;

			m_nNewTop = m_Ctx.m_nTop;

			m_nNewPos = m_Ctx.m_nPos;

			m_fChange = TRUE;
			}
		}

	CPrimViewer::DrawPrep(pGDI, Erase, Trans);
	}

void CPrimListViewer::DrawPrim(IGDI *pGDI)
{
	CPrimViewer::DrawPrim(pGDI);

	pGDI->ResetBrush();

	int yp = m_DrawRect.y1 + 2;

	if( m_fHead ) {
		
		SelectFont(pGDI, m_FontHead);

		yp = yp - 1;

		ListDrawHead(pGDI, yp);

		yp = yp + m_yHead + 6;
		}

	if( !m_nLines ) {

		SelectFont(pGDI, m_FontText);

		ListDrawEmpty(pGDI, yp);
		}
	else {
		SelectFont(pGDI, m_FontText);

		int nLast = m_Ctx.m_nTop + m_nRows;

		int nEnd  = min(nLast, m_nLines);

		int nRow  = m_Ctx.m_nTop;

		ListDrawStart(pGDI, nRow);

		while( nRow < nEnd ) {

			if( nRow == m_Ctx.m_nPos ) {

				ListDrawRow(pGDI, nRow, yp, TRUE);
				}
			else
				ListDrawRow(pGDI, nRow, yp, FALSE);

			yp    = yp + m_yFont + 1;

			nRow  = nRow + 1;
			}

		ListDrawEnd(pGDI);
		}
	}

UINT CPrimListViewer::OnMessage(UINT uMsg, UINT uParam)
{
	if( uMsg == msgOneSecond ) {

		if( m_uTimer && !--m_uTimer ) {

			m_nNewTop =  0;

			m_nNewPos = -1;
			}
		}

	if( uMsg == msgSetFocus ) {

		m_fFocus = TRUE;
		}

	if( uMsg == msgKillFocus ) {

		m_fFocus = FALSE;
		}

	return CPrimViewer::OnMessage(uMsg, uParam);
	}

// Event Handlers

BOOL CPrimListViewer::OnEnable(void)
{
	if( (m_Ctx.m_nPos < 0 && m_nLines) || m_Ctx.m_nPos > 0 ) {

		if( m_fPage ) {
			
			m_uEnable |= (1 << 0);
			}

		m_uEnable |= (1 << 2);
		}

	if( (m_Ctx.m_nPos < 0 && m_nLines) || m_Ctx.m_nPos < m_nLines - 1 ) {

		if( m_fPage ) {

			m_uEnable |= (1 << 1);
			}

		m_uEnable |= (1 << 3);
		}

	return TRUE;
	}

BOOL CPrimListViewer::OnBtnDown(UINT n)
{
	if( n == 0 ) {

		if( m_Ctx.m_nPos < 0 ) {

			m_nNewPos = m_nLines - 1;
			}
		else {
			m_nNewPos = m_Ctx.m_nPos - m_nRows;

			MakeMax(m_nNewPos, 0);
			}
		}

	if( n == 1 ) {

		if( m_Ctx.m_nPos < 0 ) {

			m_nNewPos = m_Ctx.m_nTop;
			}
		else {
			m_nNewPos = m_Ctx.m_nPos + m_nRows;

			MakeMin(m_nNewPos, m_nLines - 1);
			}
		}

	if( n == 2 ) {
		
		if( m_Ctx.m_nPos < 0 ) {

			m_nNewPos = m_nLines - 1;
			}
		else
			m_nNewPos = m_Ctx.m_nPos - 1;
		}

	if( n == 3 ) {

		if( m_Ctx.m_nPos < 0 ) {

			m_nNewPos = m_Ctx.m_nTop;
			}
		else
			m_nNewPos = m_Ctx.m_nPos + 1;
		}

	if( m_nNewPos != m_Ctx.m_nPos ) {

		MakeMin(m_nNewTop, m_nNewPos);
		
		MakeMax(m_nNewTop, m_nNewPos - m_nRows + 1);
		}

	m_uTimer = m_uTimeout;
	
	return TRUE;
	}

BOOL CPrimListViewer::OnBtnRepeat(UINT n)
{
	if( n == 2 || n == 3 ) {

		OnBtnDown(n);
		}

	if( n == 0 || n == 1 ) {

		OnBtnDown(n);
		}

	m_uTimer = m_uTimeout;

	return TRUE;
	}

BOOL CPrimListViewer::OnBtnUp(UINT n)
{
	m_uTimer = m_uTimeout;

	return TRUE;
	}

BOOL CPrimListViewer::OnTouchWork(P2 Pos)
{
	if( m_fHead ) {

		int fy = (m_yHead + 1);

		Pos.y -= fy + 4;
		}

	int yLine = Pos.y - m_Work.y1 - 2;

	int nLine = yLine / (m_yFont + 1);

	if( nLine + m_Ctx.m_nTop < m_nLines ) {

		m_nNewPos = nLine + m_Ctx.m_nTop;

		return TRUE;
		}

	return FALSE;
	}

// List Hooks

UINT CPrimListViewer::ListGetSequence(void)
{
	return 0;
	}

void CPrimListViewer::ListGetData(int nTop)
{
	}

void CPrimListViewer::ListSavePos(int nPos, int nTop)
{
	}

BOOL CPrimListViewer::ListKeepPos(int nTop, int &nPos)
{
	return FALSE;
	}

void CPrimListViewer::ListDrawHead(IGDI *pGDI, int yp)
{
	}

void CPrimListViewer::ListDrawEmpty(IGDI *pGDI, int yp)
{
	}

void CPrimListViewer::ListDrawStart(IGDI *pGDI, int nTop)
{
	}

void CPrimListViewer::ListDrawRow(IGDI *pGDI, int nRow, int yp, BOOL fSelect)
{
	}

void CPrimListViewer::ListDrawEnd(IGDI *pGDI)
{
	}

// Layout Helpers

void CPrimListViewer::FindLayout(IGDI *pGDI)
{
	SelectFont(pGDI, m_FontText);

	m_yFont = pGDI->GetTextHeight(L"X");

	SelectFont(pGDI, m_FontHead);

	m_yHead = pGDI->GetTextHeight(L"X");

	int fy  = (m_yFont + 1);

	int cy  = (m_Work.y2 - m_Work.y1 - 3) - (m_fHead ? m_yHead + 5 : 0);

	m_nRows = cy / fy;
	}

// Drawing Helpers

void CPrimListViewer::DrawRowBack(IGDI *pGDI, int yp)
{
	R2 Rect;

	Rect.x1 = m_DrawRect.x1 + 1;
	Rect.x2 = m_DrawRect.x2 - 1;
	Rect.y1 = yp - 1;
	Rect.y2 = yp + m_yFont;

	if( m_Ctx.m_fFocus ) {

		pGDI->SetBrushFore(GetRGB(26,26,31));
		}
	else
		pGDI->SetBrushFore(GetRGB(26,26,26));

	pGDI->FillRect(PassRect(Rect));
	}

void CPrimListViewer::DrawMark(IGDI *pGDI, int nRow, int xp, int yp)
{
	if( nRow == 0 || nRow == m_nLines - 1 ) {

		// REV3 -- Use triangles?

		pGDI->SelectBrush(brushBack);

		pGDI->SetBrushBack(GetRGB(8,8,8));

		R2 Rect;

		Rect.x1 = xp;
		Rect.x2 = xp + 10;
		Rect.y1 = yp + 3;
		Rect.y2 = yp + m_yFont - 4;

		pGDI->FillRect(PassRect(Rect));

		pGDI->SelectBrush(brushFore);
		}
	}

void CPrimListViewer::DrawNumber(IGDI *pGDI, int nRow, int d, int xp, int yp)
{
	CUnicode Text;

	Text = UniConvert(CPrintf("%*.*u/%*.*u", d, d, nRow + 1, d, d, m_nLines));

	MakeDigitsFixed(Text);

	pGDI->TextOut(xp, yp, Text);
	}

// Context Handling

void CPrimListViewer::FindCtx(CCtx &Ctx)
{
	Ctx.m_uSeq   = ListGetSequence();

	Ctx.m_nTop   = m_nNewTop;

	Ctx.m_nPos   = m_nNewPos;

	Ctx.m_fFocus = m_fFocus;
	}

// Context Check

BOOL CPrimListViewer::CCtx::operator == (CCtx const &That) const
{
	return m_uSeq   == That.m_uSeq   &&
	       m_nTop   == That.m_nTop   &&
	       m_nPos   == That.m_nPos   &&
	       m_fFocus == That.m_fFocus ;;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Alarm Viewer
//

// Constructor

CPrimAlarmViewer::CPrimAlarmViewer(void)
{
	m_IncTime     = 1;
	m_IncMarks    = 1;
	m_IncNum      = 0;
	m_ColInactive = MAKELONG(GetRGB(31,31,31),GetRGB( 0,16, 0));
	m_ColActive   = MAKELONG(GetRGB(31, 0, 0),GetRGB(31,31,31));
	m_ColAccepted = MAKELONG(GetRGB(15,15, 0),GetRGB(31,31,31));
	m_ColWaitAcc  = MAKELONG(GetRGB( 0, 0,15),GetRGB(31,31,31));
	m_pTxtEmpty   = NULL;
	m_pBtnPgUp    = NULL;
	m_pBtnPgDn    = NULL;
	m_pBtnPrev    = NULL;
	m_pBtnNext    = NULL;
	m_pBtnMute    = NULL;
	m_pBtnAccept  = NULL;
	m_pBtnHelp    = NULL;
	m_ShowMute    = 1;
	m_ShowAccept  = 1;
	m_ShowHelp    = 1;
	m_pOnHelp     = NULL;
	m_pFormat     = New CDispFormatTimeDate;
	m_fReverse    = 0;
	m_pAlarms     = CCommsSystem::m_pThis->m_pEvents;
	m_pHead       = NULL;
	m_pPos        = NULL;
	m_Source      = NOTHING;
	m_Code        = NOTHING;
	m_fInit       = FALSE;
	m_UsePriority = 0;
	m_Priority[0] = MAKELONG(GetRGB(31,31,31), GetRGB(31, 0, 0));
	m_Priority[1] = MAKELONG(GetRGB(31,31,31), GetRGB( 0, 0, 0));
	m_Priority[2] = MAKELONG(GetRGB( 0, 0, 0), GetRGB(31,31, 0));
	m_Priority[3] = MAKELONG(GetRGB(31,31,31), GetRGB( 0, 0,31));
	m_Priority[4] = MAKELONG(GetRGB(31,31,31), GetRGB(31, 0,31));
	m_Priority[5] = MAKELONG(GetRGB(31,31,31), GetRGB( 0,31,31));
	m_Priority[6] = MAKELONG(GetRGB(31,31,31), GetRGB(15,15,15));
	m_Priority[7] = MAKELONG(GetRGB(31,31,31), GetRGB( 8, 8, 8));
	}

// Destructor

CPrimAlarmViewer::~CPrimAlarmViewer(void)
{
	delete m_pTxtEmpty;
	delete m_pBtnPgUp;
	delete m_pBtnPgDn;
	delete m_pBtnPrev;
	delete m_pBtnNext;
	delete m_pBtnMute;
	delete m_pBtnAccept;
	delete m_pBtnHelp;
	delete m_pOnHelp;
	delete m_pFormat;

	FreeAlarmList();
	}

// Initialization

void CPrimAlarmViewer::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimAlarmViewer", pData);

	CPrimViewer::Load(pData);

	m_IncTime  = GetByte(pData);
	m_IncMarks = GetByte(pData);
	m_IncNum   = GetByte(pData);

	m_ColInactive = GetLong(pData);

	if( (m_UsePriority = GetByte(pData)) ) {

		for( UINT n = 0; n < elements(m_Priority); n ++ ) {

			m_Priority[n] = GetLong(pData);
			}
		}
	else	
		m_ColActive = GetLong(pData);

	m_ColAccepted = GetLong(pData);
	m_ColWaitAcc  = GetLong(pData);

	GetCoded(pData, m_pTxtEmpty);

	GetCoded(pData, m_pBtnPgUp);
	GetCoded(pData, m_pBtnPgDn);
	GetCoded(pData, m_pBtnPrev);
	GetCoded(pData, m_pBtnNext);
	GetCoded(pData, m_pBtnMute);
	GetCoded(pData, m_pBtnAccept);
	GetCoded(pData, m_pBtnHelp);

	m_ShowPage   = GetByte(pData);
	m_ShowMute   = GetByte(pData);
	m_ShowAccept = GetByte(pData);
	m_ShowHelp   = GetByte(pData);

	GetCoded(pData, m_pOnHelp);

	pData += 1;

	m_pFormat->Load(pData);

	m_fReverse = GetByte(pData);

	m_uTimeout = GetLong(pData);
	}

// Overridables

void CPrimAlarmViewer::SetScan(UINT Code)
{
	SetItemScan(m_pTxtEmpty,  Code);

	SetItemScan(m_pBtnPgUp,   Code);
	SetItemScan(m_pBtnPgDn,   Code);
	SetItemScan(m_pBtnPrev,   Code);
	SetItemScan(m_pBtnNext,   Code);
	SetItemScan(m_pBtnMute,   Code);
	SetItemScan(m_pBtnAccept, Code);
	SetItemScan(m_pBtnHelp,   Code);

	SetItemScan(m_pOnHelp, Code);

	m_pFormat->SetScan(Code);

	CPrimViewer::SetScan(Code);
	}

void CPrimAlarmViewer::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( !m_fInit ) {

		FindLayout(pGDI);

		m_fInit = TRUE;
		}

	CPrimListViewer::DrawPrep(pGDI, Erase, Trans);
	}

// Event Hooks

BOOL CPrimAlarmViewer::OnMakeList(void)
{
	m_List.Empty();

	if( m_ShowPage ) {

		AddButton(m_pBtnPgUp,   0);

		AddButton(m_pBtnPgDn,   1);

		m_fPage = TRUE;
		}

	if( TRUE ) {

		AddButton(m_pBtnPrev,   2);
		
		AddButton(m_pBtnNext,   3);
		}

	if( m_ShowMute ) {
		
		AddButton(m_pBtnMute,   4);
		}

	if( m_ShowAccept ) {
		
		AddButton(m_pBtnAccept, 5);
		}

	if( m_ShowHelp ) {
		
		AddButton(m_pBtnHelp,   6);
		}

	return TRUE;
	}

BOOL CPrimAlarmViewer::OnEnable(void)
{
	if( g_pPxe->GetSiren() ) {

		m_uEnable |= (1 << 4);
		}

	if( m_pPos && (m_pPos->m_State == alarmActive || m_pPos->m_State == alarmWaitAccept) ) {

		m_uEnable |= (1 << 5);
		}

	if( m_pOnHelp && m_pOnHelp->IsAvail() ) {

		if( m_Code < NOTHING ) {

			m_uEnable |= (1 << 6);
			}
		}

	return CPrimListViewer::OnEnable();
	}

BOOL CPrimAlarmViewer::OnBtnUp(UINT n)
{
	if( n == 4 ) {

		g_pPxe->SetSiren(FALSE);
		}

	if( n == 5 ) {

		m_pAlarms->AcceptAlarm(m_Source, m_Code);
		}

	if( n == 6 ) {

		if( m_pOnHelp ) {
			
			DWORD args[4];

			if( m_Source == 0 ) {

				UINT  t = LOWORD(m_Code);

				UINT  a = HIBYTE(HIWORD(m_Code));

				UINT  i = LOBYTE(HIWORD(m_Code));

				args[0] = MAKELONG(t, a);

				args[1] = t;
				
				args[2] = a;
				
				args[3] = i;
				}
			else {
				args[0] = -1;
				
				args[1] = -1;
				
				args[2] = -1;
				
				args[3] = -1;
				}

			m_pOnHelp->Execute(typeVoid, args);
			}
		}

	return CPrimListViewer::OnBtnUp(n);
	}

// List Hooks

UINT CPrimAlarmViewer::ListGetSequence(void)
{
	return m_pAlarms->GetAlarmSequence();
	}

void CPrimAlarmViewer::ListGetData(int nTop)
{
	CAutoGuard Guard;

	FreeAlarmList();

	m_pAlarms->LockAlarmData();
	
	m_nLines = m_pAlarms->GetTotalAlarms();

	m_pHead  = NULL;

	m_pAlarms->ReadAlarmList(m_pHead, nTop, m_nRows, m_fReverse);

	m_pAlarms->FreeAlarmData();
	}

void CPrimAlarmViewer::ListSavePos(int nPos, int nTop)
{
	if( nPos < 0 ) {

		m_pPos   = NULL;

		m_Source = NOTHING;

		m_Code   = NOTHING;
		}
	else {
		nPos -= nTop;

		CActiveAlarm *pInfo = m_pHead;

		while( pInfo && nPos-- ) {

			pInfo = pInfo->m_pNext;
			}

		m_pPos   = pInfo;

		m_Source = pInfo->m_Source;

		m_Code   = pInfo->m_Code;
		}
	}

BOOL CPrimAlarmViewer::ListKeepPos(int nTop, int &nPos)
{
	if( m_Code < NOTHING ) {

		CActiveAlarm *pInfo = m_pHead;

		while( pInfo ) {

			if( pInfo->m_Source == m_Source ) {

				if( pInfo->m_Code == m_Code ) {

					break;
					}
				}

			pInfo = pInfo->m_pNext;

			nTop  = nTop + 1;
			}

		if( pInfo ) {

			m_pPos   = pInfo;

			m_Source = pInfo->m_Source;

			m_Code   = pInfo->m_Code;

			nPos     = nTop;

			return TRUE;
			}

		// REV3 -- Since we only read a section of
		// the list, sometimes we can't find the item
		// we're looking for. We need to think about
		// this, perhaps asking the source to find the
		// item for us since it has the whole list...
		}

	return FALSE;
	}

void CPrimAlarmViewer::ListDrawEmpty(IGDI *pGDI, int yp)
{
	pGDI->SetTextTrans(modeOpaque);

	pGDI->SetTextFore (LOWORD(m_ColInactive));

	pGDI->SetTextBack (HIWORD(m_ColInactive));

	pGDI->SetBrushFore(HIWORD(m_ColInactive));

	pGDI->SelectBrush (brushFore);

	CUnicode Text = textHair + m_pTxtEmpty->GetText() + textHair;

	int      cx   = pGDI->GetTextWidth(Text);

	pGDI->FillRect(m_xCol1, yp - 1, m_xCol1 + cx, yp);

	pGDI->TextOut (m_xCol1, yp, UniVisual(Text));
	}

void CPrimAlarmViewer::ListDrawStart(IGDI *pGDI, int nTop)
{
	pGDI->SetTextTrans(modeTransparent);

	m_pDraw = m_pHead;

	}

void CPrimAlarmViewer::ListDrawRow(IGDI *pGDI, int nRow, int yp, BOOL fSelect)
{
	DWORD Color = FindColor(m_pDraw->m_State);

	pGDI->SetTextFore(LOWORD(Color));

	if( fSelect ) {

		DrawRowBack(pGDI, yp);
		}

	if( m_IncMarks ) {

		DrawMark(pGDI, nRow, m_xCol1, yp);
		}

	if( m_IncNum ) {

		DrawNumber(pGDI, nRow, m_IncNum + 1, m_xCol2, yp);
		}

	if( m_IncTime ) {

		CUnicode Time = m_pFormat->Format( m_pDraw->m_Time,
						   typeInteger,
						   fmtPad
						   );

		pGDI->TextOut(m_xCol3, yp, Time);
		}

	if( TRUE ) {

		CUnicode Text = UniVisual(m_pDraw->m_Text);

		pGDI->TextOut(m_xCol4, yp, Text);
		}

	m_pDraw = m_pDraw->m_pNext;
	}

void CPrimAlarmViewer::ListDrawEnd(IGDI *pGDI)
{
	}

// Layout Calculation

void CPrimAlarmViewer::FindLayout(IGDI *pGDI)
{
	SelectFont(pGDI, m_FontText);

	m_xCol1 = m_DrawRect.x1 + 2;

	m_xCol2 = m_xCol1 + GetWidth1(pGDI);

	m_xCol3 = m_xCol2 + GetWidth2(pGDI);

	m_xCol4 = m_xCol3 + GetWidth3(pGDI);
	}

int CPrimAlarmViewer::GetWidth1(IGDI *pGDI)
{
	if( m_IncMarks ) {

		return 16;
		}

	return 0;
	}

int CPrimAlarmViewer::GetWidth2(IGDI *pGDI)
{
	if( m_IncNum ) {

		CUnicode Text;

		int d = 1 + m_IncNum;

		Text  = UniConvert(CPrintf("%*.*u/%*.*u", d, d, 0, d, d, 0));

		MakeDigitsFixed(Text);

		return pGDI->GetTextWidth(Text) + 8;
		}

	return 0;
	}

int CPrimAlarmViewer::GetWidth3(IGDI *pGDI)
{
	if( m_IncTime ) {

		CUnicode Time = m_pFormat->Format( GetNow(),
						   typeInteger,
						   fmtPad
						   );

		return pGDI->GetTextWidth(Time) + 8;
		}

	return 0;
	}

// Implementation

DWORD CPrimAlarmViewer::FindColor(UINT uState)
{
	switch( uState ) {

		case alarmActive:
		case alarmAutoAccept:

			if( m_UsePriority ) {

				return m_Priority[m_pDraw->m_Level-1];
				}
			else 
				return m_ColActive;

			break;

		case alarmAccepted:

			if( m_UsePriority ) {

				return m_Priority[m_pDraw->m_Level-1];
				}
			else 
				return m_ColAccepted;

			break;

		case alarmWaitAccept:

			return m_ColWaitAcc;
		}

	return 0x7FFF0000;
	}

void CPrimAlarmViewer::FreeAlarmList(void)
{
	while( m_pHead ) {

		CActiveAlarm *pInfo = m_pHead;

		m_pHead           = m_pHead->m_pNext;

		delete pInfo;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Event Viewer
//

// Constructor

CPrimEventViewer::CPrimEventViewer(void)
{
	m_IncTime   = 1;
	m_IncMarks  = 1;
	m_IncNum    = 0;
	m_IncType   = 1;
	m_ColEmpty  = MAKELONG(GetRGB( 0, 0, 0), GetRGB(31,31,31));
	m_ColEvent  = MAKELONG(GetRGB( 0, 0, 0), GetRGB(31,31,31));
	m_ColAlarm  = MAKELONG(GetRGB(31, 0, 0), GetRGB(31,31,31));
	m_ColAccept = MAKELONG(GetRGB(15,15, 0), GetRGB(31,31,31));
	m_ColClear  = MAKELONG(GetRGB( 0,15, 0), GetRGB(31,31,31));
	m_pTxtEmpty = NULL;
	m_pBtnPgUp  = NULL;
	m_pBtnPgDn  = NULL;
	m_pBtnPrev  = NULL;
	m_pBtnNext  = NULL;
	m_pBtnClear = NULL;
	m_ShowClear = 1;
	m_pUseClear = NULL;
	m_pFormat   = New CDispFormatTimeDate;
	m_pEvents   = CEventLogger::m_pThis;
	m_fInit     = FALSE;
	}

// Destructor

CPrimEventViewer::~CPrimEventViewer(void)
{
	delete m_pTxtEmpty;
	delete m_pBtnPgUp;
	delete m_pBtnPgDn;
	delete m_pBtnPrev;
	delete m_pBtnNext;
	delete m_pBtnClear;
	delete m_pUseClear;
	delete m_pFormat;
	}

// Initialization

void CPrimEventViewer::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimEventViewer", pData);

	CPrimViewer::Load(pData);

	m_IncTime  = GetByte(pData);
	m_IncMarks = GetByte(pData);
	m_IncNum   = GetByte(pData);
	m_IncType  = GetByte(pData);

	GetCoded(pData, m_pTxtEmpty);

	GetCoded(pData, m_pBtnPgUp);
	GetCoded(pData, m_pBtnPgDn);
	GetCoded(pData, m_pBtnPrev);
	GetCoded(pData, m_pBtnNext);
	GetCoded(pData, m_pBtnClear);

	m_ShowPage  = GetByte(pData);
	m_ShowClear = GetByte(pData);

	GetCoded(pData, m_pUseClear);

	pData += 1;

	m_pFormat->Load(pData);

	m_ColEmpty  = GetLong(pData);
	m_ColEvent  = GetLong(pData);
	m_ColAlarm  = GetLong(pData);
	m_ColAccept = GetLong(pData);
	m_ColClear  = GetLong(pData);

	m_uTimeout  = GetLong(pData);
	}

// Overridables

void CPrimEventViewer::SetScan(UINT Code)
{
	SetItemScan(m_pBtnPgUp,  Code);
	SetItemScan(m_pBtnPgDn,  Code);
	SetItemScan(m_pBtnPrev,  Code);
	SetItemScan(m_pBtnNext,  Code);
	SetItemScan(m_pBtnClear, Code);

	SetItemScan(m_pUseClear, Code);

	m_pFormat->SetScan(Code);

	CPrimViewer::SetScan(Code);
	}

void CPrimEventViewer::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( !m_fInit ) {

		FindLayout(pGDI);

		m_fInit = TRUE;
		}

	CPrimListViewer::DrawPrep(pGDI, Erase, Trans);
	}

// Event Hooks

BOOL CPrimEventViewer::OnMakeList(void)
{
	m_List.Empty();

	if( m_ShowPage ) {

		AddButton(m_pBtnPgUp,  0);

		AddButton(m_pBtnPgDn,  1);

		m_fPage = TRUE;
		}

	if( TRUE ) {

		AddButton(m_pBtnPrev,  2);
		
		AddButton(m_pBtnNext,  3);
		}

	if( m_ShowClear ) {
		
		AddButton(m_pBtnClear, 4);
		}

	return TRUE;
	}

BOOL CPrimEventViewer::OnEnable(void)
{
	if( m_nLines ) {

		if( GetItemData(m_pUseClear, C3INT(1)) ) {

			m_uEnable |= (1 << 4);
			}
		}

	return CPrimListViewer::OnEnable();
	}

BOOL CPrimEventViewer::OnBtnUp(UINT n)
{
	if( n == 4 ) {

		m_pEvents->LogClear();
		}

	return CPrimListViewer::OnBtnUp(n);
	}

// List Hooks

UINT CPrimEventViewer::ListGetSequence(void)
{
	return m_pEvents->GetEventSequence();
	}

void CPrimEventViewer::ListGetData(int nTop)
{
	m_nLines = m_pEvents->GetEventCount();
	}

void CPrimEventViewer::ListDrawEmpty(IGDI *pGDI, int yp)
{
	pGDI->SetTextTrans(modeOpaque);

	pGDI->SetTextFore (LOWORD(m_ColEmpty));

	pGDI->SetTextBack (HIWORD(m_ColEmpty));

	pGDI->SetBrushFore(HIWORD(m_ColEmpty));

	pGDI->SelectBrush (brushFore);

	CUnicode Text = textHair + m_pTxtEmpty->GetText() + textHair;

	int      cx   = pGDI->GetTextWidth(Text);

	pGDI->FillRect(m_xCol1, yp - 1, m_xCol1 + cx, yp);

	pGDI->TextOut (m_xCol1, yp, UniVisual(Text));
	}

void CPrimEventViewer::ListDrawStart(IGDI *pGDI, int nTop)
{
	pGDI->SetTextTrans(modeTransparent);

	m_pEvents->LockEventData();
	}

void CPrimEventViewer::ListDrawRow(IGDI *pGDI, int nRow, int yp, BOOL fSelect)
{
	DWORD Color = FindColor(m_pEvents->GetEventType(nRow));

	pGDI->SetTextFore(LOWORD(Color));

	if( fSelect ) {

		DrawRowBack(pGDI, yp);
		}

	if( m_IncMarks ) {

		DrawMark(pGDI, nRow, m_xCol1, yp);
		}

	if( m_IncNum ) {

		DrawNumber(pGDI, nRow, m_IncNum + 1, m_xCol2, yp);
		}

	if( m_IncTime ) {

		DWORD    Time = m_pEvents->GetEventTime(nRow);

		CUnicode When = m_pFormat->Format( Time,
						   typeInteger,
						   fmtPad
						   );

		pGDI->TextOut(m_xCol3, yp, When);
		}

	if( m_IncType ) {

		CUnicode Type = UniVisual(m_pEvents->GetEventName(nRow));

		pGDI->TextOut(m_xCol4, yp, Type);
		}

	if( TRUE ) {

		CUnicode Text = UniVisual(m_pEvents->GetEventText(nRow));

		pGDI->TextOut(m_xCol5, yp, Text);
		}
	}

void CPrimEventViewer::ListDrawEnd(IGDI *pGDI)
{
	m_pEvents->FreeEventData();
	}

// Layout Calculation

void CPrimEventViewer::FindLayout(IGDI *pGDI)
{
	SelectFont(pGDI, m_FontText);

	m_xCol1 = m_DrawRect.x1 + 2;

	m_xCol2 = m_xCol1 + GetWidth1(pGDI);

	m_xCol3 = m_xCol2 + GetWidth2(pGDI);
	
	m_xCol4 = m_xCol3 + GetWidth3(pGDI);
	
	m_xCol5 = m_xCol4 + GetWidth4(pGDI);
	}

int CPrimEventViewer::GetWidth1(IGDI *pGDI)
{
	if( m_IncMarks ) {

		return 16;
		}

	return 0;
	}

int CPrimEventViewer::GetWidth2(IGDI *pGDI)
{
	if( m_IncNum ) {

		CUnicode Text;

		int d = 1 + m_IncNum;

		Text  = UniConvert(CPrintf("%*.*u/%*.*u", d, d, 0, d, d, 0));

		MakeDigitsFixed(Text);

		return pGDI->GetTextWidth(Text) + 8;
		}

	return 0;
	}

int CPrimEventViewer::GetWidth3(IGDI *pGDI)
{
	if( m_IncTime ) {

		CUnicode Time = m_pFormat->Format( GetNow(),
						   typeInteger,
						   fmtPad
						   );

		return pGDI->GetTextWidth(Time) + 8;
		}

	return 0;
	}

int CPrimEventViewer::GetWidth4(IGDI *pGDI)
{
	if( m_IncType ) {

		CUnicode Type = L"Accept";

		return pGDI->GetTextWidth(Type) + 8;
		}

	return 0;
	}

// Implementation

DWORD CPrimEventViewer::FindColor(UINT Type)
{
	WORD Fore = LOWORD(m_ColEvent);

	switch( Type ) {

		case eventAlarm:  Fore = LOWORD(m_ColAlarm); break;

		case eventAccept: Fore = LOWORD(m_ColAccept); break;

		case eventClear:  Fore = LOWORD(m_ColClear); break;
		}

	return MAKELONG(Fore, HIWORD(m_ColEmpty));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- User Manager
//

// Constructor

CPrimUserManager::CPrimUserManager(void)
{
	m_pBtnPrev = NULL;
	m_pBtnNext = NULL;
	m_pBtnPass = NULL;
	m_pSecure  = CCommsSystem::m_pThis->m_pSecure;
	m_fInit    = FALSE;
	}

// Destructor

CPrimUserManager::~CPrimUserManager(void)
{
	delete m_pBtnPrev;
	delete m_pBtnNext;
	delete m_pBtnPass;
	}

// Initialization

void CPrimUserManager::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimUserManager", pData);

	CPrimViewer::Load(pData);

	GetCoded(pData, m_pBtnPrev);
	GetCoded(pData, m_pBtnNext);
	GetCoded(pData, m_pBtnPass);

	m_uTimeout = GetLong(pData);
	}

// Overridables

void CPrimUserManager::SetScan(UINT Code)
{
	SetItemScan(m_pBtnPrev, Code);
	SetItemScan(m_pBtnNext, Code);
	SetItemScan(m_pBtnPass, Code);

	CPrimViewer::SetScan(Code);
	}

void CPrimUserManager::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( !m_fInit ) {

		FindLayout(pGDI);

		m_fInit = TRUE;
		}

	CPrimListViewer::DrawPrep(pGDI, Erase, Trans);
	}

// Event Hooks

BOOL CPrimUserManager::OnMakeList(void)
{
	m_List.Empty();

	AddButton(m_pBtnPrev, 0);
	
	AddButton(m_pBtnNext, 1);

	AddButton(m_pBtnPass, 2);

	return TRUE;
	}

BOOL CPrimUserManager::OnEnable(void)
{
	if( m_Ctx.m_nPos >= 0 ) {

		m_uEnable |= 0x07;
		}  

	return CPrimListViewer::OnEnable();
	}

BOOL CPrimUserManager::OnBtnDown(UINT n)
{
	if( n == 0 ) {

		if( m_Ctx.m_nPos < 0 ) {

			m_nNewPos = m_nLines - 1;
			}
		else {
			m_nNewPos = m_Ctx.m_nPos - 1;

			MakeMax(m_nNewPos, 0);
			}
		}

	if( n == 1 ) {

		if( m_Ctx.m_nPos < 0 ) {

			m_nNewPos = m_Ctx.m_nTop;
			}
		else {
			m_nNewPos = m_Ctx.m_nPos + 1;

			MakeMin(m_nNewPos, m_nLines - 1);
			}
		}

	if( n == 2 ) {
	
		if( m_Ctx.m_nPos < 0 ) {

			m_nNewPos = m_nLines - 1;
			}
		else
			m_nNewPos = m_Ctx.m_nPos;
		}

	if( m_nNewPos != m_Ctx.m_nPos ) {

		MakeMin(m_nNewTop, m_nNewPos);
		
		MakeMax(m_nNewTop, m_nNewPos - m_nRows + 1);
		}

	m_uTimer = m_uTimeout;
	
	return TRUE;
	}


BOOL CPrimUserManager::OnBtnUp(UINT n)
{
	if( n == 2 ) {

		m_pSecure->SetPassword(m_Ctx.m_nPos);
		}

	return CPrimListViewer::OnBtnUp(n);
	}

// List Hooks

UINT CPrimUserManager::ListGetSequence(void)
{
	return 1;
	}

void CPrimUserManager::ListGetData(int nTop)
{
	m_nLines = m_pSecure->GetUserCount();
	}

void CPrimUserManager::ListDrawEmpty(IGDI *pGDI, int yp)
{
	pGDI->SetTextTrans(modeTransparent);

	pGDI->SetTextFore (0);

	CUnicode Text;
	
	Text += textHair;
	
	Text += L"No Users Defined";
	
	Text += textHair;

	pGDI->TextOut(m_xCol1, yp, UniVisual(Text));
	}

void CPrimUserManager::ListDrawStart(IGDI *pGDI, int nTop)
{
	pGDI->SetTextTrans(modeTransparent);
	}

void CPrimUserManager::ListDrawRow(IGDI *pGDI, int nRow, int yp, BOOL fSelect)
{
	pGDI->SetTextFore(0);

	if( fSelect ) {

		DrawRowBack(pGDI, yp);
		}

	CUserItem *pUser = m_pSecure->GetUserPtr(nRow);

	CUnicode    User = UniConvert(pUser->GetUserName());
	
	CUnicode    Real =            pUser->GetRealName();

	pGDI->TextOut(m_xCol1, yp, UniVisual(User));

	pGDI->TextOut(m_xCol2, yp, UniVisual(Real));
	}

void CPrimUserManager::ListDrawEnd(IGDI *pGDI)
{
	}

// Layout Calculation

void CPrimUserManager::FindLayout(IGDI *pGDI)
{
	SelectFont(pGDI, m_FontText);

	m_xCol1 = m_DrawRect.x1 + 2;

	m_xCol2 = m_xCol1 + GetWidth1(pGDI);
	}

int CPrimUserManager::GetWidth1(IGDI *pGDI)
{
	return 100;
	}

// End of File
