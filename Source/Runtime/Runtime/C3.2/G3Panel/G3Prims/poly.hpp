
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_POLY_HPP
	
#define	INCLUDE_POLY_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "geom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimPoly;
class CPrimPolyTrans;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline
//

class CPrimPoly : public CPrimGeom
{
	public:
		// Constructor
		CPrimPoly(void);

		// Destructor
		~CPrimPoly(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		CCodedItem * m_pSpinPos;
		CCodedItem * m_pSpinMin;
		CCodedItem * m_pSpinMax;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			int m_t;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Data Members
		UINT  m_uCount;
		DWORD m_dwRound;
		P2 *  m_pList;
		BOOL  m_fInit;
		M3    m_mScale;
		CCtx  m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline (Text Outside Figure)
//

class CPrimPolyTrans : public CPrimPoly
{
	public:
		// Constructor
		CPrimPolyTrans(void);

		// Destructor
		~CPrimPolyTrans(void);
	};

// End of File

#endif
