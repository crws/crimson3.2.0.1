
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_VIEWER_HPP
	
#define	INCLUDE_VIEWER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "../g3comms/events.hpp"

#include "../g3comms/secure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimViewer;
class CPrimListViewer;
class CPrimAlarmViewer;
class CPrimEventViewer;
class CPrimFileViewer;
class CPrimUserManager;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Generic Viewer
//

class CPrimViewer : public CPrim
{
	public:
		// Constructor
		CPrimViewer(void);

		// Destructor
		~CPrimViewer(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		CPrimColor * m_pBack;
		UINT         m_FontHead;
		UINT         m_FontText;
		UINT         m_FontMenu;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			UINT m_uPress;
			UINT m_uEnable;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		struct CButtonDef
		{
			CCodedText   * m_pLabel;
			UINT           m_uCode;
			CRubyGdiList * m_pFill;
			CRubyGdiList * m_pHilite;
			CRubyGdiList * m_pShadow;
			};

		// Layout Data
		BOOL m_fInit;
		R2   m_Menu;
		R2   m_Work;

		// Data Members
		CCtx                m_Ctx;
		CArray <CButtonDef> m_List;
		UINT	            m_uPress;
		UINT	            m_uEnable;

		// Message Handlers
		BOOL OnTakeFocus(UINT uMsg, UINT uParam);
		BOOL OnTouchDown(UINT uParam);
		BOOL OnTouchRepeat(void);
		BOOL OnTouchUp(void);

		// Event Hooks
		virtual BOOL OnMakeList(void);
		virtual BOOL OnEnable(void);
		virtual BOOL OnBtnDown(UINT n);
		virtual BOOL OnBtnRepeat(UINT n);
		virtual BOOL OnBtnUp(UINT n);
		virtual BOOL OnTouchWork(P2 Pos);

		// Layout Calculation
		void FindLayout(IGDI *pGDI);
		void FindButton(R2 &Rect, UINT n, UINT c);
		void MakeButtonLists(void);
			
		// Drawing Helpers
		int  GetButtonHeight(IGDI *pGDI);
		void DrawButton(IGDI *pGDI, UINT n, R2 Rect, CUnicode const &Text, BOOL fPress, BOOL fEnable);
		void FindPoints(P2 *t, P2 *b, R2 &r, int s);
		void MakeDigitsFixed(CUnicode &Text);
		void MakeDigitsFixed(PUTF pText);

		// Button List
		void AddButton(CCodedText *pText, UINT uCode);

		// Context Handling
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- File Viewer
//

class CPrimViewerUnsupported : public CPrimViewer
{
	public:
		// Constructor
		CPrimViewerUnsupported(void);

		// Initialization
		void Load(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Generic List Viewer
//

class CPrimListViewer : public CPrimViewer
{
	public:
		// Constructor
		CPrimListViewer(void);

		// Destructor
		~CPrimListViewer(void);

		// Overridables
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		UINT OnMessage(UINT uMsg, UINT uParam);

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			UINT m_uSeq;
			int  m_nTop;
			int  m_nPos;
			BOOL m_fFocus;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Option Data
		BOOL m_fHead;
		BOOL m_fPage;

		// Layout Data
		BOOL m_fInit;
		int  m_yFont;
		int  m_yHead;
		int  m_nRows;

		// Data Members
		CCtx m_Ctx;
		UINT m_uTimer;
		UINT m_uTimeout;
		int  m_nLines;
		int  m_nNewTop;
		int  m_nNewPos;
		BOOL m_fFocus;

		// Event Hooks
		BOOL OnEnable(void);
		BOOL OnBtnDown(UINT n);
		BOOL OnBtnRepeat(UINT n);
		BOOL OnBtnUp(UINT n);
		BOOL OnTouchWork(P2 Pos);

		// List Hooks
		virtual UINT ListGetSequence(void);
		virtual void ListGetData(int nTop);
		virtual void ListSavePos(int nPos, int nTop);
		virtual BOOL ListKeepPos(int nTop, int &nPos);
		virtual void ListDrawHead(IGDI *pGDI, int yp);
		virtual void ListDrawEmpty(IGDI *pGDI, int yp);
		virtual void ListDrawStart(IGDI *pGDI, int nTop);
		virtual void ListDrawRow(IGDI *pGDI, int nRow, int yp, BOOL fSelect);
		virtual void ListDrawEnd(IGDI *pGDI);

		// Layout Calculation
		void FindLayout(IGDI *pGDI);

		// Drawing Helpers
		void DrawRowBack(IGDI *pGDI, int yp);
		void DrawMark(IGDI *pGDI, int nRow, int xp, int yp);
		void DrawNumber(IGDI *pGDI, int nRow, int d, int xp, int yp);

		// Context Handling
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Alarm Viewer
//

class CPrimAlarmViewer : public CPrimListViewer
{
	public:
		// Constructor
		CPrimAlarmViewer(void);

		// Destructor
		~CPrimAlarmViewer(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);

		// Data Members
		UINT	      m_IncTime;
		UINT	      m_IncMarks;
		UINT	      m_IncNum;
		UINT	      m_ColInactive;
		UINT	      m_ColActive;
		UINT	      m_ColAccepted;
		UINT	      m_ColWaitAcc;
		CCodedText  * m_pTxtEmpty;
		CCodedText  * m_pBtnPgUp;
		CCodedText  * m_pBtnPgDn;
		CCodedText  * m_pBtnPrev;
		CCodedText  * m_pBtnNext;
		CCodedText  * m_pBtnMute;
		CCodedText  * m_pBtnAccept;
		CCodedText  * m_pBtnHelp;
		UINT          m_ShowPage;
		UINT          m_ShowMute;
		UINT          m_ShowHelp;
		UINT          m_ShowAccept;
		CCodedItem  * m_pOnHelp;
		CDispFormat * m_pFormat;
		UINT          m_UsePriority;
		UINT          m_Priority[8];

	protected:
		// Data Members
		IAlarmStatus  * m_pAlarms;
		CActiveAlarm  * m_pHead;
		CActiveAlarm  * m_pDraw;
		CActiveAlarm  * m_pPos;
		UINT	        m_Source;
		UINT		m_Code;
		BOOL		m_fInit;
		int             m_xCol1;
		int             m_xCol2;
		int             m_xCol3;
		int             m_xCol4;
		BOOL		m_fReverse;

		// Event Hooks
		BOOL OnMakeList(void);
		BOOL OnEnable(void);
		BOOL OnBtnUp(UINT n);

		// List Hooks
		UINT ListGetSequence(void);
		void ListGetData(int nTop);
		void ListSavePos(int nPos, int nTop);
		BOOL ListKeepPos(int nTop, int &nPos);
		void ListDrawEmpty(IGDI *pGDI, int yp);
		void ListDrawStart(IGDI *pGDI, int nTop);
		void ListDrawRow(IGDI *pGDI, int nRow, int yp, BOOL fSelect);
		void ListDrawEnd(IGDI *pGDI);

		// Layout Calculation
		void FindLayout(IGDI *pGDI);
		int  GetWidth1(IGDI *pGDI);
		int  GetWidth2(IGDI *pGDI);
		int  GetWidth3(IGDI *pGDI);
		int  GetWidth4(IGDI *pGDI);

		// Implementation
		DWORD FindColor(UINT uState);
		void  FreeAlarmList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Event Viewer
//

class CPrimEventViewer : public CPrimListViewer
{
	public:
		// Constructor
		CPrimEventViewer(void);

		// Destructor
		~CPrimEventViewer(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);

		// Data Members
		UINT	      m_IncTime;
		UINT	      m_IncMarks;
		UINT	      m_IncNum;
		UINT	      m_IncType;
		UINT	      m_ColEmpty;
		UINT	      m_ColEvent;
		UINT	      m_ColAlarm;
		UINT	      m_ColAccept;
		UINT	      m_ColClear;
		CCodedText  * m_pTxtEmpty;
		CCodedText  * m_pBtnPgUp;
		CCodedText  * m_pBtnPgDn;
		CCodedText  * m_pBtnPrev;
		CCodedText  * m_pBtnNext;
		CCodedText  * m_pBtnClear;
		UINT          m_ShowPage;
		UINT          m_ShowClear;
		CCodedItem  * m_pUseClear;
		CDispFormat * m_pFormat;

	protected:
		// Data Members
		CEventLogger * m_pEvents;
		BOOL	       m_fInit;
		int            m_xCol1;
		int            m_xCol2;
		int            m_xCol3;
		int            m_xCol4;
		int            m_xCol5;

		// Event Hooks
		BOOL OnMakeList(void);
		BOOL OnEnable(void);
		BOOL OnBtnUp(UINT n);

		// List Hooks
		UINT ListGetSequence(void);
		void ListGetData(int nTop);
		void ListDrawEmpty(IGDI *pGDI, int yp);
		void ListDrawStart(IGDI *pGDI, int nTop);
		void ListDrawRow(IGDI *pGDI, int nRow, int yp, BOOL fSelect);
		void ListDrawEnd(IGDI *pGDI);

		// Layout Calculation
		void FindLayout(IGDI *pGDI);
		int  GetWidth1(IGDI *pGDI);
		int  GetWidth2(IGDI *pGDI);
		int  GetWidth3(IGDI *pGDI);
		int  GetWidth4(IGDI *pGDI);

		// Implementation
		DWORD FindColor(UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- User Manager
//

class CPrimUserManager : public CPrimListViewer
{
	public:
		// Constructor
		CPrimUserManager(void);

		// Destructor
		~CPrimUserManager(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);

		// Data Members
		CCodedText  * m_pBtnPrev;
		CCodedText  * m_pBtnNext;
		CCodedText  * m_pBtnPass;

	protected:
		// Data Members
		CSecurityManager * m_pSecure;
		BOOL	           m_fInit;
		int                m_xCol1;
		int                m_xCol2;

		// Event Hooks
		BOOL OnMakeList(void);
		BOOL OnEnable(void);
		BOOL OnBtnDown(UINT n);
		BOOL OnBtnUp(UINT n);

		// List Hooks
		UINT ListGetSequence(void);
		void ListGetData(int nTop);
		void ListDrawEmpty(IGDI *pGDI, int yp);
		void ListDrawStart(IGDI *pGDI, int nTop);
		void ListDrawRow(IGDI *pGDI, int nRow, int yp, BOOL fSelect);
		void ListDrawEnd(IGDI *pGDI);

		// Layout Calculation
		void FindLayout(IGDI *pGDI);
		int  GetWidth1(IGDI *pGDI);
	};

// End of File

#endif
