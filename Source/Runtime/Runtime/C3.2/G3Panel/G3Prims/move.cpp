
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Movement Primitive
//

// Constructor

CPrimMove::CPrimMove(void)
{
	m_fInit = FALSE;
	}

// Destructor

CPrimMove::~CPrimMove(void)
{
	}

// Overridables

void CPrimMove::MovePrim(int cx, int cy)
{
	m_InitRect.x1 += cx;

	m_InitRect.x2 += cx;

	m_InitRect.y1 += cy;

	m_InitRect.y2 += cy;

	CPrimSet::MovePrim(cx, cy);
	}

void CPrimMove::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( !m_fInit ) {

		m_InitRect = m_DrawRect;

		GetUsedRect(m_DrawRect);

		m_Ctx.m_xp = 0;

		m_Ctx.m_yp = 0;

		m_fInit    = TRUE;
		}

	CCtx Ctx;

	FindCtx(Ctx);

	if( SetShow(Erase, m_fValid) ) {

		if( !(m_Ctx == Ctx) ) {

			int dx = m_InitRect.left - m_DrawRect.left + Ctx.m_xp;

			int dy = m_InitRect.top  - m_DrawRect.top  + Ctx.m_yp;

			Erase.Append(m_DrawRect);

			MoveList(dx, dy);

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}
		else {
			if( m_fChange ) {

				Erase.Append(m_DrawRect);
				}
			}

		PrepList(pGDI, Erase, Trans);
		}
	}

// Implementation

void CPrimMove::GetUsedRect(R2 &Rect)
{
	SetRectEmpty(Rect);

	UINT c = m_pList->m_uCount;

	for( UINT n = 0; n < c; n++ ) {

		CPrim *pPrim = m_pList->m_ppPrim[n];

		CombineRects(Rect, Rect, pPrim->GetBackRect());
		}
	}

void CPrimMove::MoveList(int cx, int cy)
{
	m_DrawRect.left   += cx;
	
	m_DrawRect.right  += cx;

	m_DrawRect.top    += cy;

	m_DrawRect.bottom += cy;

	CPrimSet::MoveList(cx, cy);
	}

// Context Creation

void CPrimMove::FindCtx(CCtx &Ctx)
{
	Ctx.m_xp = 0;

	Ctx.m_yp = 0;

	m_fValid = TRUE;
	}

// Context Check

BOOL CPrimMove::CCtx::operator == (CCtx const &That) const
{
	return m_xp == That.m_xp && m_yp == That.m_yp;
	}

//////////////////////////////////////////////////////////////////////////
//
// 2D Movement Primitive
//

// Constructor

CPrimMove2D::CPrimMove2D(void)
{
	m_pPosX = NULL;

	m_pMinX = NULL;

	m_pMaxX = NULL;

	m_pPosY = NULL;

	m_pMinY = NULL;

	m_pMaxY = NULL;
	}

// Destructor

CPrimMove2D::~CPrimMove2D(void)
{
	delete m_pPosX;
	
	delete m_pMinX;
	
	delete m_pMaxX;

	delete m_pPosY;
	
	delete m_pMinY;
	
	delete m_pMaxY;
	}

// Initialization

void CPrimMove2D::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimMove2D", pData);

	CPrimSet::Load(pData);

	GetCoded(pData, m_pPosX);

	GetCoded(pData, m_pMinX);

	GetCoded(pData, m_pMaxX);

	GetCoded(pData, m_pPosY);

	GetCoded(pData, m_pMinY);

	GetCoded(pData, m_pMaxY);
	}

// Overridables

void CPrimMove2D::SetScan(UINT Code)
{
	CPrimSet::SetScan(Code);

	SetItemScan(m_pPosX, Code);

	SetItemScan(m_pMinX, Code);

	SetItemScan(m_pMaxX, Code);

	SetItemScan(m_pPosY, Code);

	SetItemScan(m_pMinY, Code);

	SetItemScan(m_pMaxY, Code);
	}

// Context Creation

void CPrimMove2D::FindCtx(CCtx &Ctx)
{
	CPrimMove::FindCtx(Ctx);

	C3REAL fx = 0.0;

	C3REAL fy = 0.0;

	if( m_pPosX ) {

		if( IsItemAvail(m_pPosX) && IsItemAvail(m_pMinX) && IsItemAvail(m_pMaxX) ) {

			C3REAL pos = GetItemData(m_pPosX, C3REAL(  0));
			
			C3REAL min = GetItemData(m_pMinX, C3REAL(  0));
			
			C3REAL max = GetItemData(m_pMaxX, C3REAL(100));

			if( max - min ) {

				fx = (pos - min) / (max - min);

				MakeMax(fx, 0.0);

				MakeMin(fx, 1.0);
				}
			}
		else
			m_fValid = FALSE;
		}

	if( m_pPosY ) {

		if( IsItemAvail(m_pPosY) && IsItemAvail(m_pMinY) && IsItemAvail(m_pMaxY) ) {

			C3REAL pos = GetItemData(m_pPosY, C3REAL(  0));
			
			C3REAL min = GetItemData(m_pMinY, C3REAL(  0));
			
			C3REAL max = GetItemData(m_pMaxY, C3REAL(100));

			if( max - min ) {

				fy = (pos - min) / (max - min);

				MakeMax(fy, 0.0);

				MakeMin(fy, 1.0);
				}
			}
		else
			m_fValid = FALSE;
		}

	if( m_fValid ) {
				
		int cx   = m_DrawRect.right  - m_DrawRect.left;
				
		int cy   = m_DrawRect.bottom - m_DrawRect.top;

		int mx   = m_InitRect.right  - m_InitRect.left - cx;

		int my   = m_InitRect.bottom - m_InitRect.top  - cy;

		Ctx.m_xp = int(mx * fx + 0.5);

		Ctx.m_yp = int(my * fy + 0.5);
		}

	}

//////////////////////////////////////////////////////////////////////////
//
// Polar Movement Primitive
//

// Constructor

CPrimMovePolar::CPrimMovePolar(void)
{
	m_pPosT = NULL;

	m_pMinT = NULL;

	m_pMaxT = NULL;

	m_pPosR = NULL;

	m_pMinR = NULL;

	m_pMaxR = NULL;
	}

// Destructor

CPrimMovePolar::~CPrimMovePolar(void)
{
	delete m_pPosT;
	
	delete m_pMinT;
	
	delete m_pMaxT;

	delete m_pPosR;
	
	delete m_pMinR;
	
	delete m_pMaxR;
	}

// Initialization

void CPrimMovePolar::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimMovePolar", pData);

	CPrimSet::Load(pData);

	GetCoded(pData, m_pPosT);

	GetCoded(pData, m_pMinT);

	GetCoded(pData, m_pMaxT);

	GetCoded(pData, m_pPosR);

	GetCoded(pData, m_pMinR);

	GetCoded(pData, m_pMaxR);
	}

// Overridables

void CPrimMovePolar::SetScan(UINT Code)
{
	CPrimSet::SetScan(Code);

	SetItemScan(m_pPosT, Code);

	SetItemScan(m_pMinT, Code);

	SetItemScan(m_pMaxT, Code);

	SetItemScan(m_pPosR, Code);

	SetItemScan(m_pMinR, Code);

	SetItemScan(m_pMaxR, Code);
	}

// Context Creation

void CPrimMovePolar::FindCtx(CCtx &Ctx)
{
	CPrimMove::FindCtx(Ctx);

	C3REAL p = 3.1415926f;

	C3REAL t = 0.0;

	C3REAL r = 1.0;

	if( m_pPosT ) {

		if( IsItemAvail(m_pPosT) && IsItemAvail(m_pMinT) && IsItemAvail(m_pMaxT) ) {

			C3REAL pos = GetItemData(m_pPosT, C3REAL(  0));
			
			C3REAL min = GetItemData(m_pMinT, C3REAL(  0));
			
			C3REAL max = GetItemData(m_pMaxT, C3REAL(360));

			if( max - min ) {

				t = 2*p * (pos - min) / (max - min) - p/2;
				}
			}
		else
			m_fValid = FALSE;
		}

	if( m_pPosR ) {

		if( IsItemAvail(m_pPosR) && IsItemAvail(m_pMinR) && IsItemAvail(m_pMaxR) ) {

			C3REAL pos = GetItemData(m_pPosR, C3REAL(  0));
			
			C3REAL min = GetItemData(m_pMinR, C3REAL(  0));
			
			C3REAL max = GetItemData(m_pMaxR, C3REAL(100));

			if( max - min ) {

				r = (pos - min) / (max - min);

				MakeMax(r, 0.0);

				MakeMin(r, 1.0);
				}
			}
		else
			m_fValid = FALSE;
		}

	if( m_fValid ) {

		int cx   = m_DrawRect.right  - m_DrawRect.left;

		int cy   = m_DrawRect.bottom - m_DrawRect.top;

		int mx   = m_InitRect.right  - m_InitRect.left;

		int my   = m_InitRect.bottom - m_InitRect.top;

		int rx   = (mx - cx) / 2;

		int ry   = (my - cy) / 2;

		Ctx.m_xp = rx + int(r * cos(t) * rx);

		Ctx.m_yp = ry + int(r * sin(t) * ry);
		}
	}

// End of File
