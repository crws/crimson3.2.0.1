
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DIAL_HPP
	
#define	INCLUDE_DIAL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyDial;
class CPrimLegacyWholeDial;
class CPrimLegacyHalfDial;
class CPrimLegacyQuadDial;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Dial Gauge
//

class CPrimLegacyDial : public CPrimRich
{
	public:
		// Constructor
		CPrimLegacyDial(void);

		// Destructor
		~CPrimLegacyDial(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawExec(IGDI *pGDI, IRegion *pDirty);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		CPrimPen     * m_pEdge;
		CPrimBrush   * m_pBack;
		CPrimColor   * m_pFill;

		// Public Data
		R2           m_DialRect;
		UINT	     m_Orient;
		UINT	     m_Entity;
		UINT         m_Flip;
		UINT	     m_Major;
		UINT	     m_Minor;
		INT	     m_Start;
		INT	     m_Sweep;
		P2	     m_Origin;
		INT          m_nRadius1;
		INT          m_nRadius2;
		INT          m_nRadius3;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			BOOL   m_fAvail;
			C3REAL m_Min;
			C3REAL m_Max;
			C3REAL m_Pos;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);
		
		// Implementation
		void DrawScale(IGDI *pGDI, int nRad1, int nRad2);
		void DrawPointer(IGDI *pGDI, P2 Org, INT nRadius, INT nAngle);
		void DrawRect(IGDI *pGDI, P2 Org, INT nRad1, INT nRad2, INT nAngle);
		void DrawLine(IGDI *pGDI, P2 Org, INT nRad1, INT nRad2, INT nAngle);
		int  MulDiv(int a, int b, int c);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Whole Dial Gauge
//

class CPrimLegacyWholeDial : public CPrimLegacyDial
{
	public:
		// Constructor
		CPrimLegacyWholeDial(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);

	protected:

		// Implementation
		void DrawLegend(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Half Dial Gauge
//

class CPrimLegacyHalfDial : public CPrimLegacyDial
{
	public:
		// Constructor
		CPrimLegacyHalfDial(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);

	protected:

		// Implementation
		void DrawLegend(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Quad Dial Gauge
//

class CPrimLegacyQuadDial : public CPrimLegacyDial
{
	public:
		// Constructor
		CPrimLegacyQuadDial(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);

	protected:

		// Implementation
		void DrawLegend(IGDI *pGDI);
	};

// End of File

#endif
