
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGeom_HPP
	
#define	INCLUDE_PrimRubyGeom_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyWithText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPrimRubyTankFill;
class CPrimRubyPenEdge;
	
//////////////////////////////////////////////////////////////////////////
//
// Ruby Geometric Primitive
//

class CPrimRubyGeom : public CPrimRubyWithText
{
	public:
		// Constructor
		CPrimRubyGeom(void);

		// Destructor
		~CPrimRubyGeom(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		CPrimRubyTankFill * m_pFill;
		CPrimRubyPenEdge  * m_pEdge;

	protected:
		// Data Members
		BOOL	     m_fFast;
		CRubyGdiList m_listFill;
		CRubyGdiList m_listEdge;
		CRubyGdiList m_listTrim;

		// Implementation
		void DoLoad(PCBYTE &pData);
	};

// End of File

#endif
