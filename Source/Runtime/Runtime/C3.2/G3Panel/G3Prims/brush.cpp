
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Brush
//

// Static Data

BOOL  CPrimBrush::m_ShadeMode = 0;

COLOR CPrimBrush::m_ShadeCol1 = 0;

COLOR CPrimBrush::m_ShadeCol2 = 0;

// Constructor

CPrimBrush::CPrimBrush(void)
{
	m_Pattern = brushFore;

	m_pColor1 = New CPrimColor(naBack);

	m_pColor2 = New CPrimColor(naFeature);
	}

// Destructor

CPrimBrush::~CPrimBrush(void)
{
	delete m_pColor1;

	delete m_pColor2;
	}

// Initialization

void CPrimBrush::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimBrush", pData);

	DoLoad(pData);
	}

// Attributes

BOOL CPrimBrush::IsNull(void) const
{
	return m_Pattern == brushNull;
	}

// Operations

void CPrimBrush::SetScan(UINT Code)
{
	SetItemScan(m_pColor1, Code); 
	
	SetItemScan(m_pColor2, Code); 
	}

BOOL CPrimBrush::DrawPrep(IGDI *pGDI)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_Ctx = Ctx;

		return TRUE;
		}

	return FALSE;
	}

// Drawing

void CPrimBrush::FillRect(IGDI *pGDI, R2 Rect)
{
	if( !IsNull() ) {

		if( m_Pattern < 16 ) {

			pGDI->SetBrushFore (m_Ctx.m_Color1);

			pGDI->SetBrushBack (m_Ctx.m_Color2);

			pGDI->SetBrushStyle(m_Pattern);

			pGDI->FillRect(PassRect(Rect));
			}
		else {
			UINT uStyle = m_Pattern - 16;

			m_ShadeMode = uStyle / 2;

			m_ShadeCol1 = m_Ctx.m_Color1;

			m_ShadeCol2 = m_Ctx.m_Color2;
			
			PSHADER pShader = (uStyle % 2) ? Shader1 : Shader2;

			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeRect(PassRect(Rect), pShader);
			}
		}
	}

void CPrimBrush::FillEllipse(IGDI *pGDI, R2 Rect, UINT uType)
{
	if( !IsNull() ) {

		if( m_Pattern < 16 ) {

			pGDI->SetBrushFore (m_Ctx.m_Color1);

			pGDI->SetBrushBack (m_Ctx.m_Color2);

			pGDI->SetBrushStyle(m_Pattern);

			pGDI->FillEllipse(PassRect(Rect), uType);
			}
		else {
			UINT uStyle = m_Pattern - 16;

			m_ShadeMode = uStyle / 2;

			m_ShadeCol1 = m_Ctx.m_Color1;

			m_ShadeCol2 = m_Ctx.m_Color2;
			
			PSHADER pShader = (uStyle % 2) ? Shader1 : Shader2;

			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeEllipse(PassRect(Rect), uType, pShader);
			}
		}
	}

void CPrimBrush::FillWedge(IGDI *pGDI, R2 Rect, UINT uType)
{
	if( !IsNull() ) {

		if( m_Pattern < 16 ) {

			pGDI->SetBrushFore (m_Ctx.m_Color1);

			pGDI->SetBrushBack (m_Ctx.m_Color2);

			pGDI->SetBrushStyle(m_Pattern);

			pGDI->FillWedge(PassRect(Rect), uType);
			}
		else {
			UINT uStyle = m_Pattern - 16;

			m_ShadeMode = uStyle / 2;

			m_ShadeCol1 = m_Ctx.m_Color1;

			m_ShadeCol2 = m_Ctx.m_Color2;
			
			PSHADER pShader = (uStyle % 2) ? Shader1 : Shader2;

			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeWedge(PassRect(Rect), uType, pShader);
			}
		}
	}

void CPrimBrush::FillPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound)
{
	if( !IsNull() ) {

		if( m_Pattern < 16 ) {

			pGDI->SetBrushFore (m_Ctx.m_Color1);

			pGDI->SetBrushBack (m_Ctx.m_Color2);

			pGDI->SetBrushStyle(m_Pattern);

			pGDI->FillPolygon(pList, uCount, dwRound);
			}
		else {
			UINT uStyle = m_Pattern - 16;

			m_ShadeMode = uStyle /2;

			m_ShadeCol1 = m_Ctx.m_Color1;

			m_ShadeCol2 = m_Ctx.m_Color2;
			
			PSHADER pShader = (uStyle % 2) ? Shader1 : Shader2;

			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadePolygon(pList, uCount, dwRound, pShader);
			}
		}
	}

// Color Mixing

#if 0

COLOR CPrimBrush::Mix16(COLOR a, COLOR b, int p, int c)
{
	if( c ) {

		int ra = GetRED  (a);
		int ga = GetGREEN(a);
		int ba = GetBLUE (a);

		int rb = GetRED  (b);
		int gb = GetGREEN(b);
		int bb = GetBLUE (b);

		int rc = ra + ((rb - ra) * p / c);
		int gc = ga + ((gb - ga) * p / c);
		int bc = ba + ((bb - ba) * p / c);

		return GetRGB(rc, gc, bc);
		}

	return a;
	}

#else

DWORD CPrimBrush::Mix32(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	int rb = GetRED  (b);
	int gb = GetGREEN(b);
	int bb = GetBLUE (b);

	ra = (ra << 3) | (ra >> 2);
	ga = (ga << 3) | (ga >> 2);
	ba = (ba << 3) | (ba >> 2);

	rb = (rb << 3) | (rb >> 2);
	gb = (gb << 3) | (gb >> 2);
	bb = (bb << 3) | (bb >> 2);

	int rc = ra + ((rb - ra) * p / c);
	int gc = ga + ((gb - ga) * p / c);
	int bc = ba + ((bb - ba) * p / c);

	return MAKELONG(MAKEWORD(bc, gc), MAKEWORD(rc, 0xFF));
	}

#endif

// Shaders

BOOL CPrimBrush::Shader1(IGDI *pGDI, int p, int c)
{
	static MIXCOL q = 0;

	if( c ) {

		MIXCOL k = Mixer(m_ShadeCol1, m_ShadeCol2, p, c);

		if( k - q ) {

			pGDI->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = MIXINV;

	return m_ShadeMode;
	}

BOOL CPrimBrush::Shader2(IGDI *pGDI, int p, int c)
{
	static MIXCOL q = 0;

	if( c ) {

		MIXCOL k = Mixer( m_ShadeCol2,
			          m_ShadeCol1,
			          abs(c/2-p),
			          c/2
			          );

		if( k - q ) {

			pGDI->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = MIXINV;

	return m_ShadeMode;
	}

// Implementation

void CPrimBrush::DoLoad(PCBYTE &pData)
{
	m_Pattern = GetByte(pData);

	m_pColor1->Load(pData);

	m_pColor2->Load(pData);
	}

// Context Creation

void CPrimBrush::FindCtx(CCtx &Ctx)
{
	Ctx.m_Color1 = m_pColor1->GetColor();

	Ctx.m_Color2 = m_pColor2->GetColor();
	}

// Context Check

BOOL CPrimBrush::CCtx::operator == (CCtx const &That) const
{
	return m_Color1 == That.m_Color1 &&
	       m_Color2 == That.m_Color2 ;
	}

// End of File
