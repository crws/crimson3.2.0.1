
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "uitask.hpp"

#include "pdftask.hpp"

//////////////////////////////////////////////////////////////////////////
//
// UI System Item
//

// Instance Pointer

CUISystem * CUISystem::m_pThis = NULL;

// Constructor

CUISystem::CUISystem(void)
{
	m_pThis  = this;

	BOOL fUI = (WhoGetActiveGroup() >= SW_GROUP_3A);

	m_pUI    = fUI ? New CUIManager : NULL;

	m_pTask  = fUI ? New CUITask : NULL;

	m_pMusic = WhoHasFeature(rfPlayMusic) ? New CMusicPlayer : NULL;

	m_pPDF   = WhoHasFeature(rfPdfViewer) ? New CPDFTask : NULL;

	m_hTask  = NULL;

	m_pGDI   = NULL;
}

// Destructor

CUISystem::~CUISystem(void)
{
	AfxRelease(m_pGDI);

	delete m_pUI;

	delete m_pTask;

	delete m_pMusic;

	delete m_pPDF;

	m_pThis = NULL;
}

// Initialization

void CUISystem::Load(PCBYTE &pData)
{
	ValidateLoad("CUISystem", pData);

	DoLoad(pData);

	if( m_pUI ) {

		m_pUserServer = New CUIDataServer(this);

		m_pDataServer = m_pUserServer;
	}
	else {
		m_pUserServer = NULL;

		m_pDataServer = New CDataServer(this);
	}

	m_pUsedServer = m_pDataServer;
}

// Server Access

IDataServer * CUISystem::GetDataServer(void) const
{
	if( m_pUI ) {

		if( GetCurrentTask() == m_hTask ) {

			return m_pUsedServer;
		}

		return m_pUserServer;
	}

	return m_pDataServer;
}

// Operations

void CUISystem::SetDataServer(IDataServer *pData)
{
	m_pUsedServer = pData;
}

void CUISystem::RegisterUITask(void)
{
	m_hTask = GetCurrentTask();

	m_pLang->RegisterUITask();
}

// System Calls

void CUISystem::SystemSave(void)
{
	if( m_pTask ) {

		m_pTask->Shutdown();
	}

	CCommsSystem::SystemSave();
}

void CUISystem::GetTaskList(CTaskList &List)
{
	CCommsSystem::GetTaskList(List);

	if( m_pUI ) {

		// TODO -- What about priority flipping?!!!

		BOOL fFlip = FALSE && !WhoHasFeature(rfDisplay);

		CTaskDef Task;

		Task.m_Name   = "USER";
		Task.m_pEntry = m_pTask;
		Task.m_uID    = 0;
		Task.m_uCount = 1;
		Task.m_uLevel = fFlip ? 2600 : 8000;
		Task.m_uStack = 32768;

		List.Append(Task);
	}

	if( m_pMusic ) {

		m_pMusic->GetTaskList(List);
	}

	if( m_pPDF ) {

		m_pPDF->GetTaskList(List);
	}
}

BOOL CUISystem::IsPriority(void)
{
	if( m_pUI ) {

		return GetCurrentTask() == m_hTask;
	}

	return FALSE;
}

// Implementation

void CUISystem::DoLoad(PCBYTE &pData)
{
	CCommsSystem::DoLoad(pData);

	if( m_pUI ) {

		int cx = GetWord(pData);

		int cy = GetWord(pData);

		m_pGDI = Create_GDI();

		m_pGDI->Create(cx, cy, NULL);

		GetItem(pData, m_pUI, IDC_UI_MANAGER);
	}
	else {
		GetWord(pData);

		GetWord(pData);

		GetWord(pData);
	}
}

// End of File
