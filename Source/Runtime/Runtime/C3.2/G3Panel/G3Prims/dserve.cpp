
#include "intern.hpp"

#include "uitask.hpp"

#include "rle8.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// UI Data Server
//

// Function Macros

#define	DefFunc(ID, Name) { ID, 0, (PVOID) &Name, #Name, 0 },

// Function Table

CUIDataServer::CFunc const CUIDataServer::m_FuncTable[] =
{
	// NOTE: List must be kept in order!

	DefFunc(0x2000, GotoPage)
	DefFunc(0x2001, SetLanguage)
	DefFunc(0x2002, Beep)
	DefFunc(0x2003, PlayRTTTL)
	DefFunc(0x2031, SirenOff)
	DefFunc(0x2032, SirenOn)
	DefFunc(0x2042, ShowPopup)
	DefFunc(0x2043, HidePopup)
	DefFunc(0x204E, DispOn)
	DefFunc(0x204F, DispOff)
	DefFunc(0x2052, GotoPrevious)
	DefFunc(0x2055, ShowMenu)
	DefFunc(0x2060, GetUpDownData)
	DefFunc(0x2061, GetUpDownStep)
	DefFunc(0x2089, LogOn)
	DefFunc(0x208A, LogOff)
	DefFunc(0x208B, TestAccess)
	DefFunc(0x2092, PostKey)
	DefFunc(0x20AA, HasAccess)
	DefFunc(0x20BC, Flash)
	DefFunc(0x20BD, ColFlash)
	DefFunc(0x20BE, ColPick2)
	DefFunc(0x20BF, ColBlend)
	DefFunc(0x20C0, GotoNext)
	DefFunc(0x20C1, CanGotoPrevious)
	DefFunc(0x20C2, CanGotoNext)
	DefFunc(0x20CB, PrintScreenToFile)
	DefFunc(0x20CC, ColGetRed)
	DefFunc(0x20CD, ColGetGreen)
	DefFunc(0x20CE, ColGetBlue)
	DefFunc(0x20CF, ColGetRGB)
	DefFunc(0x20D5, ColPick4)
	DefFunc(0x20D6, ColSelFlash)
	DefFunc(0x20DA, GetLanguage)
	DefFunc(0x20DC, ShowNested)
	DefFunc(0x20DD, HideAllPopups)
	DefFunc(0x20DE, ShowModal)
	DefFunc(0x20DF, EndModal)
	DefFunc(0x20E4, SaveSecurityDatabase)
	DefFunc(0x20E5, LoadSecurityDatabase)
	DefFunc(0x20E6, GetCurrentUserName)
	DefFunc(0x20E7, GetCurrentUserRealName)
	DefFunc(0x20E8, GetCurrentUserRights)
	DefFunc(0x20E9, HasAllAccess)
	DefFunc(0x2151, StopRTTTL)
};

// Static Data

CUIDataServer * CUIDataServer::m_pThis = NULL;

// Constructor

CUIDataServer::CUIDataServer(CUISystem *pSystem) : CDataServer(pSystem)
{
	m_pSystem = pSystem;

	m_pUI     = m_pSystem->m_pUI;

	m_pTask   = m_pSystem->m_pTask;

	m_fFreeze = FALSE;

	m_pThis   = this;

	m_pPrim   = NULL;
}

// Destructor

CUIDataServer::~CUIDataServer(void)
{
	m_pThis = NULL;
}

// Attributes

CPrim * CUIDataServer::GetPrim(void) const
{
	return m_pPrim;
}

// Operations

void CUIDataServer::SetPrim(CPrim *pPrim)
{
	m_pPrim = pPrim;
}

// IBase Methods

UINT CUIDataServer::Release(void)
{
	delete this;

	return 0;
}

// IDataServer Methods

WORD CUIDataServer::CheckID(WORD ID)
{
	return CDataServer::CheckID(ID);
}

BOOL CUIDataServer::IsAvail(DWORD ID)
{
	return CDataServer::IsAvail(ID);
}

BOOL CUIDataServer::SetScan(DWORD ID, UINT Code)
{
	return CDataServer::SetScan(ID, Code);
}

DWORD CUIDataServer::GetData(DWORD ID, UINT Type, UINT Flags)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( !Ref.x.m_HasBit ) {

		if( !Ref.x.m_IsTag ) {

			switch( Ref.t.m_Index ) {

				case 0x7C00: return m_pTask->GetBrightness(0);
				case 0x7C01: return m_pTask->GetContrast();
				case 0x7C03: return m_pTask->GetUpdateRate();
				case 0x7C04: return m_pTask->GetDrawCount();
				case 0x7C06: return g_pPxe->GetSiren();
				case 0x7C0B: return IsPressed();
				case 0x7C0E: return m_pTask->GetBrightness(1);
			}
		}
		else {
			if( Ref.t.m_Index >= 0x7F80 ) {

				//AfxTrace("Widget At Top Level!\n");
			}
		}
	}

	return CDataServer::GetData(ID, Type, Flags);
}

BOOL CUIDataServer::SetData(DWORD ID, UINT Type, UINT Flags, DWORD Data)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( !Ref.x.m_HasBit ) {

		if( !Ref.x.m_IsTag ) {

			switch( Ref.t.m_Index ) {

				case 0x7C00: m_pTask->SetBrightness(0, Data); return TRUE;
				case 0x7C01: m_pTask->SetContrast(Data);    return TRUE;
				case 0x7C0E: m_pTask->SetBrightness(1, Data); return TRUE;
			}
		}
		else {
			if( Ref.t.m_Index >= 0x7F80 ) {

				AfxTrace("Widget At Top Level!\n");
			}
		}
	}

	return CDataServer::SetData(ID, Type, Flags, Data);
}

DWORD CUIDataServer::GetProp(DWORD ID, WORD Prop, UINT Type)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( !Ref.t.m_IsTag ) {

		// REV3 -- The page in question could already
		// be instantiated by the UI manager, so we could
		// ask it in order to avoid reloading the data.

		CDispPage *pPage = New CDispPage;

		PCBYTE pData = PCBYTE(g_pDbase->LockItem(Ref.t.m_Index));

		if( pData ) {

			if( GetWord(pData) == IDC_DISP_PAGE ) {

				pPage->Fast(pData);

				g_pDbase->FreeItem(Ref.t.m_Index);

				DWORD Data = pPage->GetProp(Prop, Type);

				delete pPage;

				return Data;
			}

			g_pDbase->FreeItem(Ref.t.m_Index);
		}

		delete pPage;
	}

	return CDataServer::GetProp(ID, Prop, Type);
}

DWORD CUIDataServer::RunFunc(WORD ID, UINT uParam, PDWORD pParam)
{
	DWORD Data = 0;

	if( ID >= 0x7F80 && ID <= 0x7FFF ) {

		AfxTrace("Widget Action At Top Level!\n");

		return 0;
	}

	if( CDataServer::RunFunc(Data, ID, uParam, pParam) ) {

		return Data;
	}

	if( RunFunc(Data, ID, uParam, pParam) ) {

		return Data;
	}

	return Data;
}

BOOL CUIDataServer::GetName(DWORD ID, UINT m, PSTR pName, UINT uName)
{
	if( CDataServer::GetName(ID, m, pName, uName) ) {

		return TRUE;
	}

	if( m == 1 ) {

		if( FindName(ID, pName, uName, m_FuncTable, elements(m_FuncTable)) ) {

			return TRUE;
		}
	}

	if( m == typeObject + 1 ) {

		CDataRef  &Ref   = (CDataRef &) ID;

		CDispPage *pPage = New CDispPage;

		PCBYTE pData = PCBYTE(g_pDbase->LockItem(Ref.t.m_Index));

		if( pData ) {

			if( GetWord(pData) == IDC_DISP_PAGE ) {

				pPage->Fast(pData);

				g_pDbase->FreeItem(Ref.t.m_Index);

				strcpy(pName, UniConvert(pPage->m_Name));

				delete pPage;

				return TRUE;
			}

			g_pDbase->FreeItem(Ref.t.m_Index);
		}

		delete pPage;
	}

	return FALSE;
}

// Implementation

BOOL CUIDataServer::RunFunc(DWORD &Data, WORD ID, UINT uParam, PDWORD pParam)
{
	return ScanTable(Data, ID, uParam, pParam, m_FuncTable, elements(m_FuncTable));
}

BOOL CUIDataServer::IsPressed(void)
{
	if( m_pPrim && m_pPrim->IsPressed() ) {

		return TRUE;
	}

	return FALSE;
}

WORD CUIDataServer::GetWord(PCBYTE &pData)
{
	Item_Align2(pData);

	WORD x = MotorToHost(*PCWORD(pData));

	pData += sizeof(x);

	return x;
}

// Function Library

void CUIDataServer::GotoPage(UINT p)
{
	p = CUISystem::m_pThis->GetDataServer()->CheckID(p);

	m_pThis->m_pTask->GotoPage(p);
}

void CUIDataServer::GotoPrevious(void)
{
	m_pThis->m_pTask->GotoPrevious();
}

void CUIDataServer::GotoNext(void)
{
	m_pThis->m_pTask->GotoNext();
}

C3INT CUIDataServer::CanGotoPrevious(void)
{
	return m_pThis->m_pTask->CanGotoPrevious();
}

C3INT CUIDataServer::CanGotoNext(void)
{
	return m_pThis->m_pTask->CanGotoNext();
}

void CUIDataServer::SetLanguage(UINT c)
{
	CUISystem::m_pThis->m_pLang->SetCurrentSlot(c);

	CUISystem::m_pThis->m_pTask->ForceRedraw();
}

C3INT CUIDataServer::GetLanguage(void)
{
	return CUISystem::m_pThis->m_pLang->GetCurrentSlot();
}

void CUIDataServer::PlayRTTTL(PUTF p)
{
	if( WhoHasFeature(rfPlayMusic) ) {

		CUISystem::m_pThis->m_pMusic->PlayTune(p);
	}
}

void CUIDataServer::StopRTTTL(void)
{
	if( WhoHasFeature(rfPlayMusic) ) {

		CUISystem::m_pThis->m_pMusic->StopTune();
	}
}

void CUIDataServer::SirenOff(void)
{
	g_pPxe->SetSiren(FALSE);
}

void CUIDataServer::SirenOn(void)
{
	g_pPxe->SetSiren(TRUE);
}

void CUIDataServer::ShowPopup(UINT p)
{
	p = CUISystem::m_pThis->GetDataServer()->CheckID(p);

	m_pThis->m_pTask->ShowSimplePopup(p);
}

void CUIDataServer::ShowNested(UINT p)
{
	p = CUISystem::m_pThis->GetDataServer()->CheckID(p);

	m_pThis->m_pTask->ShowNestedPopup(p);
}

void CUIDataServer::ShowMenu(UINT p)
{
	p = CUISystem::m_pThis->GetDataServer()->CheckID(p);

	m_pThis->m_pTask->ShowPopupMenu(p);
}

void CUIDataServer::HidePopup(void)
{
	m_pThis->m_pTask->HidePopup();
}

void CUIDataServer::HideAllPopups(void)
{
	m_pThis->m_pTask->HideAllPopups();
}

C3INT CUIDataServer::ShowModal(UINT p)
{
	p = CUISystem::m_pThis->GetDataServer()->CheckID(p);

	return m_pThis->m_pTask->ExecModalPopup(p);
}

void CUIDataServer::EndModal(C3INT c)
{
	m_pThis->m_pTask->EndModal(c);
}

void CUIDataServer::DispOn(void)
{
	DispEnableBacklight(TRUE);
}

void CUIDataServer::DispOff(void)
{
	DispEnableBacklight(FALSE);
}

void CUIDataServer::LogOn(void)
{
	CUISystem::m_pThis->m_pSecure->LogOn();
}

void CUIDataServer::LogOff(void)
{
	CUISystem::m_pThis->m_pSecure->LogOff();
}

C3INT CUIDataServer::TestAccess(C3INT r, PUTF p)
{
	CString s = UniConvert(p);

	Free(p);

	return CUISystem::m_pThis->m_pSecure->TestAccess(DWORD(r << 16), s);
}

C3INT CUIDataServer::HasAccess(C3INT r)
{
	return CUISystem::m_pThis->m_pSecure->HasAccess(DWORD(r << 16));
}

void CUIDataServer::PostKey(C3INT c, C3INT t)
{
	CInput Input;

	Input.x.i.m_Type   = typeKey;

	Input.x.i.m_State  = t ? t - 1 : 0;

	Input.x.i.m_Local  = TRUE;

	Input.x.d.k.m_Code = c;

	InputStore(Input.m_Ref);

	if( t == 0 ) {

		Input.x.i.m_State = 1;

		InputStore(Input.m_Ref);
	}
}

C3INT CUIDataServer::GetUpDownData(C3INT d, C3INT r)
{
	if( r > 1 ) {

		C3INT x = (d % (2 * r));

		if( x >= r ) {

			return 2 * r - 1 - x;
		}
		else
			return x;
	}

	return 0;
}

C3INT CUIDataServer::GetUpDownStep(C3INT d, C3INT r)
{
	if( r > 1 ) {

		C3INT x = (d % (2 * r));

		if( x >= r ) {

			return -1;
		}
		else
			return +1;
	}

	return 0;
}

C3INT CUIDataServer::Flash(C3INT f)
{
	if( !m_pThis->m_fFreeze ) {

		if( f ) {

			int nCount = GetTickCount();

			int nRate  = ToTicks(1000);

			int nDenom = nRate / (2 * f);

			if( nDenom ) {

				return (nCount / nDenom) % 2;
			}
		}
	}

	return TRUE;
}

C3INT CUIDataServer::ColFlash(C3INT f, C3INT c1, C3INT c2)
{
	return Flash(f) ? c1 : c2;
}

C3INT CUIDataServer::ColSelFlash(C3INT e, C3INT f, C3INT c1, C3INT c2, C3INT c3)
{
	return e ? (Flash(f) ? c2 : c3) : c1;
}

C3INT CUIDataServer::ColPick2(C3INT n, C3INT c1, C3INT c2)
{
	return n ? c1 : c2;
}

C3INT CUIDataServer::ColPick4(C3INT n1, C3INT n2, C3INT c1, C3INT c2, C3INT c3, C3INT c4)
{
	return n2 ? (n1 ? c1 : c2) : (n1 ? c3 : c4);
}

C3INT CUIDataServer::ColBlend(C3REAL d, C3REAL f, C3REAL t, C3INT c1, C3INT c2)
{
	if( t - f ) {

		MakeMin(d, t);

		MakeMax(d, f);

		int c  = 1000;

		int p  = int(c * (d - f) / (t - f));

		int ra = GetRED(c1);
		int ga = GetGREEN(c1);
		int ba = GetBLUE(c1);

		int rb = GetRED(c2);
		int gb = GetGREEN(c2);
		int bb = GetBLUE(c2);

		int rc = ra + ((rb - ra) * p / c);
		int gc = ga + ((gb - ga) * p / c);
		int bc = ba + ((bb - ba) * p / c);

		return GetRGB(rc, gc, bc);
	}

	return c1;
}

C3INT CUIDataServer::ColGetRed(C3INT c)
{
	return GetRED(c) << 3;
}

C3INT CUIDataServer::ColGetGreen(C3INT c)
{
	return GetGREEN(c) << 3;
}

C3INT CUIDataServer::ColGetBlue(C3INT c)
{
	return GetBLUE(c) << 3;
}

C3INT CUIDataServer::ColGetRGB(C3INT r, C3INT g, C3INT b)
{
	return GetRGB(r >> 3, g >> 3, b >> 3);
}

C3INT CUIDataServer::PrintScreenToFile(PUTF p, PUTF f, C3INT c)
{
	CAutoArray<WCHAR> pp(p);

	CAutoArray<WCHAR> pf(f);

	if( WhoHasFeature(rfPrintScreen) ) {

		char a[MAX_PATH];

		AdjustName(a, pp.TakeOver());

		if( *f ) {

			UINT j = strlen(a);

			for( UINT i = 0; (a[j++] = f[i]); i++ );
		}
		else {
			UINT uSize = strlen(a);

			for( UINT i = 0;; i++ ) {

				SPrintf(a + uSize, "pic%5.5u.bmp", i);

				if( !CAutoFile(a, "r") ) {

					break;
				}
			}
		}

		CAutoFile File(a, "w");

		if( File ) {

			switch( c ) {

				case 0:
					if( SaveRaw(File) ) {

						return 1;
					}
					break;

				case 1:
					if( SaveRLE(File) ) {

						return 1;
					}
					break;
			}

			File.Close();

			unlink(a);
		}
	}

	return 0;
}

C3INT CUIDataServer::SaveSecurityDatabase(C3INT m, PUTF p)
{
	char a[MAX_PATH];

	AdjustName(a, p);

	return CUISystem::m_pThis->m_pSecure->SaveDatabase(m, a);
}

C3INT CUIDataServer::LoadSecurityDatabase(C3INT m, PUTF p)
{
	char a[MAX_PATH];

	AdjustName(a, p);

	return CUISystem::m_pThis->m_pSecure->LoadDatabase(m, a);
}

PUTF CUIDataServer::GetCurrentUserName(void)
{
	return wstrdup(CUISystem::m_pThis->m_pSecure->GetCurrentUserName());
}

PUTF CUIDataServer::GetCurrentUserRealName(void)
{
	return wstrdup(CUISystem::m_pThis->m_pSecure->GetCurrentUserRealName());
}

C3INT CUIDataServer::GetCurrentUserRights(void)
{
	DWORD r = CUISystem::m_pThis->m_pSecure->GetCurrentUserRights();

	return DWORD(r >> 16);
}

C3INT CUIDataServer::HasAllAccess(C3INT r)
{
	return CUISystem::m_pThis->m_pSecure->HasAllAccess(DWORD(r << 16));
}

// Screen Dump Helpers

BOOL CUIDataServer::SaveRaw(CAutoFile &File)
{
	IGDI *pGDI  = CUISystem::m_pThis->GetGDI();

	int   cx    = pGDI->GetCx();

	int   cy    = pGDI->GetCy();

	UINT  uBuff = cx * cy * 2;

	CAutoArray<BYTE> pBuff(uBuff);

	////////

	m_pThis->m_fFreeze = TRUE;

	m_pThis->m_pTask->WaitUpdate();

	pGDI->Render(pBuff, 16);

	m_pThis->m_fFreeze = FALSE;

	////////

	static BYTE bHead[] = {

		0x42, 0x4D, 0x36, 0xB4, 0x04, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x36, 0x00, 0x00, 0x00,

		0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x10, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

	};

////////

	((PDWORD) (bHead +  2))[0] = HostToIntel(DWORD(uBuff + sizeof(bHead)));

	((PDWORD) (bHead + 18))[0] = HostToIntel(DWORD(+cx));

	((PDWORD) (bHead + 22))[0] = HostToIntel(DWORD(-cy));

	((PDWORD) (bHead + 30))[0] = HostToIntel(DWORD(0));

	((PDWORD) (bHead + 34))[0] = HostToIntel(DWORD(uBuff));

	File.Write(bHead, sizeof(bHead));

	File.Write(pBuff, uBuff);

	return TRUE;
}

BOOL CUIDataServer::SaveRLE(CAutoFile &File)
{
	IGDI *pGDI   = CUISystem::m_pThis->GetGDI();

	int   cx     = pGDI->GetCx();

	int   cy     = pGDI->GetCy();

	UINT  uBits  = cx * cy;

	UINT  uExtra = 96 * cx;

	UINT  uBuff  = uBits + uExtra + 1024;

	PBYTE pData  = NULL;

	CAutoArray<BYTE> pBuff(uBuff);

	////////

	pData  = pBuff;

	pData += uBuff;

	pData -= uBits;

	////////

	m_pThis->m_fFreeze = TRUE;

	m_pThis->m_pTask->WaitUpdate();

	pGDI->Render(pData, 8);

	m_pThis->m_fFreeze = FALSE;

	////////

	SetTaskLimit(50, 5);

	BOOL   fOkay = TRUE;

	BOOL   fComp = TRUE;

	UINT   uSize = 0;

	PCBYTE pLine = pData;

	for( int n = 0; n < cy; n++ ) {

		BOOL fLast  = (n == cy - 1);

		BOOL fFirst = (n == 0);

		UINT uLine  = MakeRLE8(pBuff + uSize,
				       pLine,
				       cx,
				       fFirst,
				       fLast
		);

		uSize += uLine;

		pLine += cx;

		if( uSize >= uBits + uExtra ) {

			fComp = FALSE;

			break;
		}

		if( pLine <= pBuff + uSize ) {

			fOkay = FALSE;

			break;
		}
	}

	SetTaskLimit(0, 0);

	////////

	if( fOkay ) {

		static BYTE bHead[] = {

			0x42, 0x4D, 0x36, 0xB4, 0x04, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x36, 0x04, 0x00, 0x00,

			0x28, 0x00, 0x00, 0x00, 0x80, 0x02, 0x00, 0x00,
			0xE0, 0x01, 0x00, 0x00, 0x01, 0x00, 0x08, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0xB0, 0x04, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

		};

	////////

		if( fComp ) {

			((PDWORD) (bHead +  2))[0] = HostToIntel(DWORD(uSize + 1024 + sizeof(bHead)));

			((PDWORD) (bHead + 18))[0] = HostToIntel(DWORD(cx));

			((PDWORD) (bHead + 22))[0] = HostToIntel(DWORD(cy));

			((PDWORD) (bHead + 30))[0] = HostToIntel(DWORD(1));

			((PDWORD) (bHead + 34))[0] = HostToIntel(DWORD(uSize));

			File.Write(bHead, sizeof(bHead));

			SavePal(File);

			File.Write(pBuff, uSize);

			return TRUE;
		}

		((PDWORD) (bHead +  2))[0] = HostToIntel(DWORD(uBits + 1024 + sizeof(bHead)));

		((PDWORD) (bHead + 18))[0] = HostToIntel(DWORD(cx));

		((PDWORD) (bHead + 22))[0] = HostToIntel(DWORD(cy));

		((PDWORD) (bHead + 30))[0] = HostToIntel(DWORD(0));

		((PDWORD) (bHead + 34))[0] = HostToIntel(DWORD(uBits));

		File.Write(bHead, sizeof(bHead));

		SavePal(File);

		File.Write(pData, uBits);

		return TRUE;
	}

	return FALSE;
}

BOOL CUIDataServer::SavePal(CAutoFile &File)
{
	// REV3 -- Factor out and combined with web server?

	IGDI *pGDI  = CUISystem::m_pThis->GetGDI();

	CAutoArray<BYTE> pCols(1024);

	for( UINT c = 0; c < 256; c++ ) {

		DWORD rgb = pGDI->GetPaletteEntry(c);

		PDWORD(PBYTE(pCols))[c] = HostToIntel(rgb);
	}

	File.Write(pCols, 1024);

	return TRUE;
}

// End of File
