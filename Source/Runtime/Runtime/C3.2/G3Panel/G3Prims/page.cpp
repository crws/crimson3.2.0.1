
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Page Item
//

// Sort Pointer

CDispPage * CDispPage::m_pSort = NULL;

// Constructor

CDispPage::CDispPage(void)
{
	m_pList   = New CPrimList;

	m_pEvents = New CEventMap;

	m_pProps  = New CDispPageProps;
	}

// Destructor

CDispPage::~CDispPage(void)
{
	delete m_pList;

	delete m_pEvents;

	delete m_pProps;
	}

// Initialization

void CDispPage::Load(PCBYTE &pData)
{
	ValidateLoad("CDispPage", pData);

	m_Size.cx = GetWord(pData);
	
	m_Size.cy = GetWord(pData);

	m_Rect.x1 = 0;

	m_Rect.y1 = 0;

	m_Rect.x2 = m_Size.cx;

	m_Rect.y2 = m_Size.cy;

	m_Name    = GetWide(pData);

	m_pProps ->Load(pData);

	m_pEvents->Load(pData);

	m_pList  ->Load(pData, NULL);
	}

void CDispPage::Fast(PCBYTE &pData)
{
	ValidateLoad("CDispPage", pData);

	m_Size.cx = GetWord(pData);
	
	m_Size.cy = GetWord(pData);

	m_Name    = GetWide(pData);

	m_pProps ->Fast(pData);
	}

// Popup Adjustment

void CDispPage::FindPopup(void)
{
	UINT uCount = m_pList->m_uCount;

	if( uCount ) {

		R2 Span;

		Span.x1 = +99999999;
		Span.y1 = +99999999;
		Span.x2 = -99999999;
		Span.y2 = -99999999;
		
		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pPrim = m_pList->m_ppPrim[n];

			CombineRects(Span, Span, pPrim->GetBackRect());
			}

		InflateRect(Span, 4, 4);

		m_Rect = Span;
		}
	else {
		m_Rect.x1 = 0;
		
		m_Rect.y1 = 0;
		
		m_Rect.x2 = 100;
		
		m_Rect.y2 = 100;
		}
	}

void CDispPage::MovePopup(int cx, int cy)
{
	UINT uCount = m_pList->m_uCount;

	if( uCount ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pPrim = m_pList->m_ppPrim[n];

			pPrim->MovePrim(cx, cy);
			}
		}

	m_Rect.x1 += cx;

	m_Rect.y1 += cy;

	m_Rect.x2 += cx;

	m_Rect.y2 += cy;
	}

void CDispPage::PopupMenu(void)
{
	int  cx = -m_Rect.x1;

	int  cy = 0;

	UINT uCount = m_pList->m_uCount;

	if( uCount ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pPrim = m_pList->m_ppPrim[n];

			pPrim->MovePrim(cx, cy);
			}
		}

	m_Rect.x1  = 0;

	m_Rect.y1  = 0;

	m_Rect.x2 += cx;

	m_Rect.y2  = m_Size.cy;
	}

// Attributes

COLOR CDispPage::GetBackColor(void) const
{
	return m_pProps->m_pBack->GetColor();
	}

BOOL CDispPage::IsEventReady(UINT uEvent) const
{
	UINT uCount = m_Event.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = m_Event[n];

		if( !pPrim->SendMessage(msgTestEvent, uEvent) ) {

			return FALSE;
			}
		}

	return TRUE;
	}

// Operations

BOOL CDispPage::SetScan(UINT Code)
{
	CUIDataServer * pData  = CUISystem::m_pThis->m_pUserServer;

	CPrim         * pPrev  = pData->GetPrim();

	UINT            uCount = m_pList->m_uCount;
	
	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = m_pList->m_ppPrim[n];

		pData->SetPrim(pPrim);

		pPrim->SetScan(Code);
		}

	pData->SetPrim(pPrev);

	m_pEvents->SetScan(Code);

	m_pProps->SetScan(Code);

	return TRUE;
	}

void CDispPage::FireEvent(UINT uEvent)
{
	UINT uCount = m_Event.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = m_Event[n];

		pPrim->SendMessage(msgFireEvent, uEvent);
		}
	}

BOOL CDispPage::BuildFocusList(CDispPage *pMaster)
{
	m_Event.Empty();

	m_Focus.Empty();

	m_Order.Empty();

	m_Event.Expand(32);

	m_Focus.Expand(32);

	m_Order.Expand(32);

	MakeEventList(m_pList);

	MakeFocusList(m_pList);

	if( pMaster ) {

		MakeEventList(pMaster->m_pList);

		MakeFocusList(pMaster->m_pList);
		}

	SortFocusList();

	return TRUE;
	}

BOOL CDispPage::FindFocus(CPrim * &pFocus, UINT &uFocus, P2 const &Pos)
{
	UINT uCount = m_Focus.GetCount();

	UINT uIndex = uCount - 1;

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = m_Focus[uIndex];

		if( pPrim->HitTest(Pos) ) {

			if( pPrim->SendMessage(msgTakeFocus, 0) ) {

				for( UINT i = 0; i < uCount; i++ ) {

					if( m_Order[i] == uIndex ) {

						uFocus = i;

						pFocus = pPrim;

						return TRUE;
						}
					}
				}
			}

		uIndex--;
		}

	return FALSE;
	}

BOOL CDispPage::NextFocus(CPrim * &pFocus, UINT &uFocus)
{
	UINT uCount = m_Focus.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		if( uFocus == NOTHING ) {

			uFocus = uCount - 1;
			}
		else
			uFocus = (uFocus + uCount + 1) % uCount;

		CPrim *pPrim = m_Focus[m_Order[uFocus]];

		if( pPrim->OnMessage(msgUsesKeypad, 0) ) {

			if( pPrim->SendMessage(msgTakeFocus, 0) ) {

				pFocus = pPrim;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CDispPage::PrevFocus(CPrim * &pFocus, UINT &uFocus)
{
	UINT uCount = m_Focus.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		uFocus = (uFocus + uCount - 1) % uCount;

		CPrim *pPrim = m_Focus[m_Order[uFocus]];

		if( pPrim->OnMessage(msgUsesKeypad, 0) ) {

			if( pPrim->SendMessage(msgTakeFocus, 0) ) {

				pFocus = pPrim;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CDispPage::DrawPrep(IGDI *pGDI, IRegion *pErase, R2 const *pClip, UINT uClip)
{
	CUIDataServer * pData  = CUISystem::m_pThis->m_pUserServer;

	CPrim         * pPrev  = pData->GetPrim();

	UINT            uCount = m_pList->m_uCount;

	CR2Array Trans;

	CR2Array Erase;

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = m_pList->m_ppPrim[n];

		UINT   c;

		for( c = 0; c < uClip; c++ ) {

			if( RectInRect(pClip[c], pPrim->m_DrawRect) ) {

				break;
				}
			}

		if( c == uClip ) {

			pData->SetPrim(pPrim);

			pGDI->ResetAll();

			pPrim->DrawPrep(pGDI, Erase, Trans);
			}
		}

	pData->SetPrim(pPrev);

	if( pErase ) {

		for( UINT n = 0; n < Erase.GetCount(); n++ ) {

			pErase->AddRect(Erase[n]);
			}

		if( Trans.GetCount() ) {

			UINT  c     = Trans.GetCount();

			PBYTE pDone = New BYTE [ c ];

			BOOL  fFlip = FALSE;

			memset(pDone, 0, c);

			for(;;) {

				BOOL fHit = FALSE;

				for( UINT n = 0; n < c; n++ ) {

					UINT i = fFlip ? (c - 1 - n) : n;

					if( !pDone[i] && pErase->HitTest(Trans[i]) ) {

						pErase->AddRect(Trans[i]);

						pDone[i] = 1;

						fHit     = TRUE;
						}
					}

				if( !fHit ) {

					break;
					}

				fFlip = !fFlip;
				}

			delete [] pDone;
			}
		}

	return TRUE;
	}

BOOL CDispPage::DrawExec(IGDI *pGDI, IRegion *pDirty, R2 const *pClip, UINT uClip)
{
	CUIDataServer * pData  = CUISystem::m_pThis->m_pUserServer;

	CPrim         * pPrev  = pData->GetPrim();

	UINT            uCount = m_pList->m_uCount;

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = m_pList->m_ppPrim[n];

		UINT   c;

		for( c = 0; c < uClip; c++ ) {

			if( RectInRect(pClip[c], pPrim->m_DrawRect) ) {

				break;
				}
			}

		if( c == uClip ) {

			pData->SetPrim(pPrim);

			pGDI->ResetAll();

			pPrim->DrawExec(pGDI, pDirty);
			}
		}

	pData->SetPrim(pPrev);

	return TRUE;
	}

BOOL CDispPage::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	if( pTouch ) {

		pTouch->ClearMap();

		UINT uCount = m_Focus.GetCount();

		for( UINT n = 0; n < uCount; n++ ) {

			CPrim *pPrim = m_Focus[n];

			pPrim->LoadTouchMap(pGDI, pTouch);
			}
		}

	return TRUE;
	}

// Property Access

DWORD CDispPage::GetProp(WORD ID, UINT Type)
{
	switch( ID ) {

		case ppName:
			return DWORD(wstrdup(m_Name));

		case ppLabel:
			return FindLabel();

		case ppDescription:
			return FindDesc();
		}

	if( Type == typeString ) {

		return DWORD(wstrdup(L""));
		}

	return 0;
	}

// Implementation

void CDispPage::MakeEventList(CPrimList *pList)
{
	UINT uCount = pList->m_uCount;

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = pList->m_ppPrim[n];

		if( pPrim->OnMessage(msgHasEvents, 0) ) {

			m_Event.Append(pPrim);
			}

		if( pPrim->OnMessage(msgIsGroup, 0) ) {

			CPrimGroup *pGroup = (CPrimGroup *) pPrim;

			MakeEventList(pGroup->m_pList);
			}
		}
	}

void CDispPage::MakeFocusList(CPrimList *pList)
{
	UINT uCount = pList->m_uCount;

	for( UINT n = 0; n < uCount; n++ ) {

		CPrim *pPrim = pList->m_ppPrim[n];

		if( pPrim->OnMessage(msgTakeFocus, 1) ) {

			m_Focus.Append(pPrim);

			m_Order.Append(m_Order.GetCount());
			}

		if( pPrim->OnMessage(msgIsGroup, 0) ) {

			CPrimGroup *pGroup = (CPrimGroup *) pPrim;

			MakeFocusList(pGroup->m_pList);
			}
		}
	}

void CDispPage::SortFocusList(void)
{
	UINT  uCount = m_Order.GetCount();

	PVOID pData  = PVOID(m_Order.GetPointer());

	m_pSort      = this;

	UINT uOrder  = m_pProps->m_EntryOrder ? m_pProps->m_EntryOrder - 1 : CUISystem::m_pThis->m_pUI->m_EntryOrder;

	switch( uOrder ) {

		case 0:
			qsort(pData, uCount, sizeof(UINT), SortFuncRows);
			break;

		case 1:
			qsort(pData, uCount, sizeof(UINT), SortFuncCols);
			break;
		}
	}

DWORD CDispPage::FindLabel(void)
{
	if( m_pProps->m_pLabel ) {

		return DWORD(wstrdup(m_pProps->m_pLabel->GetText()));
		}

	return DWORD(wstrdup(m_Name));
	}

DWORD CDispPage::FindDesc(void)
{
	return DWORD(wstrdup(m_pProps->m_Desc));
	}

// Sort Function

int CDispPage::SortFuncRows(PCVOID s1, PCVOID s2)
{
	CPrim *p1 = m_pSort->m_Focus[*PUINT(s1)];
	
	CPrim *p2 = m_pSort->m_Focus[*PUINT(s2)];

	int n1 = MAKELONG(p1->m_DrawRect.x1, p1->m_DrawRect.y1);

	int n2 = MAKELONG(p2->m_DrawRect.x1, p2->m_DrawRect.y1);

	if( n1 < n2 ) return -1;
	
	if( n1 > n2 ) return +1;

	return 0;
	}

int CDispPage::SortFuncCols(PCVOID s1, PCVOID s2)
{
	CPrim *p1 = m_pSort->m_Focus[*PUINT(s1)];
	
	CPrim *p2 = m_pSort->m_Focus[*PUINT(s2)];

	int n1 = MAKELONG(p1->m_DrawRect.y1, p1->m_DrawRect.x1);

	int n2 = MAKELONG(p2->m_DrawRect.y1, p2->m_DrawRect.x1);

	if( n1 < n2 ) return -1;
	
	if( n1 > n2 ) return +1;

	return 0;
	}

// End of File
