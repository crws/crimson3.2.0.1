
#include "intern.hpp"

#include "pdf.hpp"

#include "uitask.hpp"

#include "pdftask.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PDF Request
//

// Constructor

CPDFRequest::CPDFRequest(void)
{
	m_hTask     = GetCurrentTask();

	m_uHandle   = NOTHING;

	m_nPage     = 0;
	m_nRes      = 70;
	m_nPages    = 1;
	m_Bitmap.cx = 0;
	m_Bitmap.cy = 0;
	m_nSize     = 0;
	m_pData     = NULL;
	m_pNext     = NULL;
	m_pPrev     = NULL;
	}

// Destructor

CPDFRequest::~CPDFRequest(void)
{
	Free(m_pData);
	}

// IBase

UINT CPDFRequest::Release(void)
{
	delete this;

	return 1;
	}

// Operations

void CPDFRequest::QueueEvent(UINT uMsg)
{
	CUISystem::m_pThis->m_pTask->QueueEvent( m_uHandle, uMsg, UINT(this) );
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- PDF Viewer
//

// Constructor

CPrimPDFViewer::CPrimPDFViewer(void)
{
	m_pFile		= NULL;
	m_pTxtCannot	= NULL;
	m_pTxtWorking	= NULL;
	m_pBtnLeft	= NULL;
	m_pBtnRight	= NULL;
	m_pBtnPrev	= NULL;
	m_pBtnNext	= NULL;
	m_pBtnZoomIn	= NULL;
	m_pBtnZoomOut	= NULL;	

	m_uScroll	= 0;
	m_nPage		= 0;
	m_nRes		= 70;
	m_Origin.x	= 0;
	m_Origin.y	= 0;
	m_uState        = 0;
	m_fLoad         = FALSE;

	m_pDisplay      = NULL;

	m_nLine         = 1;

	m_uTimer	= 0;
	
	ClearCtx(m_Ctx);

	CUISystem::m_pThis->m_pTask->RegisterPrim(this);
	}

// Destructor

CPrimPDFViewer::~CPrimPDFViewer(void)
{
	delete m_pFile;
	delete m_pTxtCannot;
	delete m_pTxtWorking;
	delete m_pBtnLeft;
	delete m_pBtnRight;
	delete m_pBtnPrev;
	delete m_pBtnNext;
	delete m_pBtnZoomIn;
	delete m_pBtnZoomOut;

	CUISystem::m_pThis->m_pPDF->ClearRequests(m_uHandle);

	CUISystem::m_pThis->m_pTask->DeregisterPrim(this);

	if( m_pDisplay ) {
		
		m_pDisplay->Release();
		}
	}

// Initialization

void CPrimPDFViewer::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimPDFViewer", pData);

	CPrimViewer::Load(pData);

	GetWord (pData);
	
	GetCoded(pData, m_pFile);

	GetCoded(pData, m_pTxtCannot);
	GetCoded(pData, m_pTxtWorking);
	
	GetCoded(pData, m_pBtnLeft);
	GetCoded(pData, m_pBtnRight);
	GetCoded(pData, m_pBtnPrev);
	GetCoded(pData, m_pBtnNext);
	GetCoded(pData, m_pBtnZoomIn);
	GetCoded(pData, m_pBtnZoomOut);
	}

// Overridables

void CPrimPDFViewer::SetScan(UINT Code)
{
	SetItemScan(m_pFile,       Code);
	
	SetItemScan(m_pTxtCannot,  Code);
	SetItemScan(m_pTxtWorking, Code);

	SetItemScan(m_pBtnLeft,    Code);
	SetItemScan(m_pBtnRight,   Code);
	SetItemScan(m_pBtnPrev,    Code);
	SetItemScan(m_pBtnNext,    Code);
	SetItemScan(m_pBtnZoomIn,  Code);
	SetItemScan(m_pBtnZoomOut, Code);

	CPrimViewer::SetScan(Code);
	}

void CPrimPDFViewer::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( !m_fInit ) {

		CPrimViewer::DrawPrep(pGDI, Erase, Trans);

		FindLayout(pGDI);

		m_fInit = TRUE;
		}

	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		if( !(m_Ctx.m_pDisplay == Ctx.m_pDisplay) ) {

			if( m_Ctx.m_pDisplay ) {
				
				CPDFRequest *pDisplay = m_Ctx.m_pDisplay;

				m_Ctx.m_pDisplay      = m_pDisplay;
				
				pDisplay->Release();				
				}
			}

		if( m_Ctx.m_File != Ctx.m_File ) {

			if( !Ctx.m_File.IsEmpty() ) {

				m_Origin.x = 0;

				m_Origin.y = 0;

				m_fLoad    = TRUE;
				}
			}

		if( Ctx.m_fLoad ) {

			LoadFile(Ctx);
			
			m_fLoad = FALSE;
			}

		m_Ctx     = Ctx;

		m_fChange = TRUE;
		}

	CPrimViewer::DrawPrep(pGDI, Erase, Trans);
	}

void CPrimPDFViewer::DrawPrim(IGDI *pGDI)
{
	CPrimViewer::DrawPrim(pGDI);

	DrawTitle  (pGDI);

	DrawScroll (pGDI);

	DrawPDF    (pGDI);

	DrawWorking(pGDI);
	}

UINT CPrimPDFViewer::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case 301:

			m_pDisplay = (CPDFRequest *) uParam;

			m_uState = 2;
			
			return TRUE;

		case 302:

			m_pDisplay = NULL;

			m_uState   = 0;
			
			return TRUE;

		case 303:

			m_pDisplay = NULL;

			m_uState   = 3;

			return TRUE;

		default:
			break;
		}

	switch( uMsg ) {

		case msgOneSecond:
			
			if( m_uTimer && !--m_uTimer ) {

				m_uTimer   = 3;
				
				m_nDelta <<= 1;
				}

			break;

		case msgTouchDown:

			if( OnTouchDown(uParam) ) {
				
				m_uParam = uParam;

				return TRUE;
				}

			break;

		case msgTouchRepeat:

			if( OnTouchRepeat() ) {
				
				return TRUE;
				}

			break;

		case msgTouchUp:

			if( m_uScroll )	{

				m_uScroll  = 0;
				
				m_Origin.x = 0;

				m_Origin.y = 0;

				m_fLoad    = TRUE;
				}

			m_uParam  = 0;

			m_uTimer  = 0;

			break;
		}

	return CPrimViewer::OnMessage(uMsg, uParam);
	}

// Message Handlers

BOOL CPrimPDFViewer::OnTouchDown(UINT uParam)
{
	P2 Pos;
	
	Pos.x = LOWORD(uParam);
	
	Pos.y = HIWORD(uParam);

	if( PtInRect(m_Work, Pos) ) {

		return OnTouchWork(Pos);
		}

	if( PtInRect(m_Scroll, Pos) ) {

		m_uParam = uParam;

		return OnTouchScroll(Pos);
		}

	return FALSE;
	}

BOOL CPrimPDFViewer::OnTouchRepeat(void)
{
	P2 Pos;
	
	Pos.x = LOWORD(m_uParam);
	
	Pos.y = HIWORD(m_uParam);

	if( PtInRect(m_Work, Pos) ) {

		return OnTouchWork(Pos);
		}

	if( PtInRect(m_Scroll, Pos) ) {

		return OnTouchScroll(Pos);
		}

	return FALSE;
	}

// Event Hooks

BOOL CPrimPDFViewer::OnMakeList(void)
{
	m_List.Empty();

	AddButton(m_pBtnLeft,    buttonLeft);

	AddButton(m_pBtnRight,   buttonRight);

	AddButton(m_pBtnPrev,    buttonPrev);

	AddButton(m_pBtnNext,    buttonNext);

	AddButton(m_pBtnZoomIn,  buttonZoomIn);

	AddButton(m_pBtnZoomOut, buttonZoomOut);

	return TRUE;
	}

BOOL CPrimPDFViewer::OnEnable(void)
{
	int     cx = m_Work.x2 - m_Work.x1;

	int     cy = m_Work.y2 - m_Work.y1;

	int     bx = m_Ctx.m_pDisplay ? m_Ctx.m_pDisplay->m_Bitmap.cx : 0;

	int     by = m_Ctx.m_pDisplay ? m_Ctx.m_pDisplay->m_Bitmap.cy : 0;

	BOOL fRqst = TRUE;

	m_uEnable |= (bx > cx) ? (1 << buttonLeft)    : 0;

	m_uEnable |= (bx > cx) ? (1 << buttonRight)   : 0;

	m_uEnable |= fRqst     ? (1 << buttonZoomIn)  : 0;

	m_uEnable |= fRqst     ? (1 << buttonZoomOut) : 0;

	m_uEnable |= (by > cy) ? (1 << buttonPrev)    : 0;

	m_uEnable |= (by > cy) ? (1 << buttonNext)    : 0;

	return CPrimViewer::OnEnable();
	}

BOOL CPrimPDFViewer::OnBtnDown(UINT n)
{
	switch( n ) {

		case buttonLeft:
		case buttonRight:
		case buttonPrev:
		case buttonNext:

			m_nDelta = 5;

			m_uTimer = 3;

			break;
		}

	OnBtnRepeat(n);

	return CPrimViewer::OnBtnDown(n);
	}

BOOL CPrimPDFViewer::OnBtnRepeat(UINT n)
{
	switch( n ) {

		case buttonLeft:
			m_Origin.x -= m_nDelta;
			break;

		case buttonRight:
			m_Origin.x += m_nDelta;
			break;

		case buttonZoomIn:
			m_nRes     += 10;
			break;

		case buttonZoomOut:
			m_nRes     -= 10;
			break;

		case buttonPrev:
			m_Origin.y -= m_nDelta;
			break;

		case buttonNext:
			m_Origin.y += m_nDelta;
			break;
		}

	MakeMin(m_Origin.x, m_Ctx.m_pDisplay->m_Bitmap.cx - (m_Work.x2 - m_Work.x1));

	MakeMin(m_Origin.y, m_Ctx.m_pDisplay->m_Bitmap.cy - (m_Work.y2 - m_Work.y1));

	MakeMax(m_Origin.x, 0);

	MakeMax(m_Origin.y, 0);

	MakeMax(m_nRes, 10);

	MakeMin(m_nRes, 100);

	return CPrimViewer::OnBtnDown(n);
	}

BOOL CPrimPDFViewer::OnBtnUp(UINT n)
{
	if( n == buttonZoomIn ) {

		m_fLoad    = TRUE;
		}

	if( n == buttonZoomOut ) {

		m_fLoad    = TRUE;
		}

	return CPrimViewer::OnBtnUp(n);
	}

BOOL CPrimPDFViewer::OnTouchWork(P2 Pos)
{
	if( m_Origin.x != 0 || m_Origin.y != 0 ) {

		m_Origin.x = 0;

		m_Origin.y = 0;

		m_fLoad    = TRUE;
		}

	return FALSE;
	}

// Layout Calculation

void CPrimPDFViewer::FindLayout(IGDI *pGDI)
{
	SelectFont(pGDI, m_FontHead);

	m_yHead     = pGDI->GetTextHeight(L"X") + 4;

	m_nScroll   = 30;

	m_Scroll.x1 = m_DrawRect.x2 + 1 - m_nScroll;
	m_Scroll.x2 = m_DrawRect.x2 - 1;
	m_Scroll.y1 = m_DrawRect.y1 + 1 + m_yHead;
	m_Scroll.y2 = m_Menu.top    - 1;

	m_Work.y1  += m_yHead;
	m_Work.x2  -= m_nScroll;
	}

void CPrimPDFViewer::FindScroll(R2 &Rect, UINT n, int nPos)
{
	R2 Scroll[3];

	/////////
	
	Scroll[0] = m_Scroll;

	Scroll[1] = m_Scroll;

	Scroll[2] = m_Scroll;

	/////////
	
	Scroll[0].bottom = Scroll[0].top + m_nScroll;

	Scroll[1].top    = Scroll[1].bottom - m_nScroll;

	if( m_Ctx.m_pDisplay ) {

		if( m_Ctx.m_pDisplay->m_nPages > 1 ) {

			int       nRange = Scroll[1].top - Scroll[0].bottom;

			int       nThumb = nRange / (m_Ctx.m_pDisplay->m_nPages - 1);

			MakeMax(nThumb, m_nScroll / 2);

			nRange -= nThumb;

			Scroll[2].top    = Scroll[0].bottom - 1 + (nPos * nRange) / (m_Ctx.m_pDisplay->m_nPages - 1);

			Scroll[2].bottom = Scroll[2].top    + 1 + nThumb;
			}
		else {
			Scroll[2].top    = Scroll[0].bottom - 1;
			
			Scroll[2].bottom = Scroll[2].top    + 1;
			}

		if( n == 3 ) {

			Rect.left   = Scroll[0].left;		
			Rect.top    = Scroll[2].bottom;
			Rect.right  = Scroll[0].right;		
			Rect.bottom = Scroll[1].top;

			return;
			}

		if( n == 4 ) {

			Rect.left   = Scroll[0].left;
			Rect.top    = Scroll[0].bottom;
			Rect.right  = Scroll[0].right;
			Rect.bottom = Scroll[2].top;
			
			return;
			}
		}

	if( n == 5 ) {
		
		Rect.left   = Scroll[0].left;
		Rect.top    = Scroll[0].bottom;
		Rect.right  = Scroll[0].right;
		Rect.bottom = Scroll[1].top;

		return;
		}

	Rect = Scroll[n];
	}

// Scroll Handlers

BOOL CPrimPDFViewer::OnTouchScroll(P2 Pos)
{
	BOOL fEnable = (m_Ctx.m_pDisplay != NULL);

	for( UINT n = 0; fEnable && n < 5; n ++ ) {

		R2 Rect;
		
		FindScroll(Rect, n, m_nPage);

		if( PtInRect(Rect, Pos) ) {

			if( n == 0 ) {
				
				if( m_nPage - m_nLine >= 0 ) {
					
					m_nPage -= m_nLine;

					m_uScroll |= (1 << n);
					}
				}

			if( n == 1 ) {

				if( m_nPage + m_nLine < m_Ctx.m_pDisplay->m_nPages ) {
				
					m_nPage += m_nLine;

					m_uScroll |= (1 << n);
					}
				}

			if( n == 2 ) {

				continue;				
				}

			if( n == 3 || n == 4 ) {
				
				R2 Work;
				
				FindScroll(Work, 5, m_nPage);
				
				int    cy = Work.bottom - Work.top;

				int    py = Pos.y - Work.top;

				int nPage = (py * m_Ctx.m_pDisplay->m_nPages) / cy;

				if( nPage < m_Ctx.m_pDisplay->m_nPages ) {

					m_nPage    = nPage;

					m_uScroll |= (1 << n);
					}
				}

			MakeMin(m_nPage, m_Ctx.m_pDisplay->m_nPages - 1);

			MakeMax(m_nPage, 0);

			return m_uScroll > 0;
			}
		}
	
	return FALSE;
	}

// Implementation

void CPrimPDFViewer::LoadFile(CCtx const &Ctx)
{
	m_uState = 1;

	CString File;

	File += "C:\\PDF\\";

	File += UniConvert(Ctx.m_File);

	CPDFRequest *pRqst = New CPDFRequest;

	pRqst->m_uHandle   = m_uHandle;

	pRqst->m_File      = File;

	pRqst->m_nPage     = Ctx.m_nPage;

	pRqst->m_nRes      = Ctx.m_nRes;

	CUISystem::m_pThis->m_pPDF->SubmitRequest(pRqst);
	}

void CPrimPDFViewer::DrawTitle(IGDI *pGDI)
{
	R2 Rect;

	Rect.x1 = m_DrawRect.x1 + 1;
	Rect.x2 = m_DrawRect.x2 - 1;
	Rect.y1 = m_DrawRect.y1;
	Rect.y2 = m_DrawRect.y1 + m_yHead;

	CUnicode Text;

	Text += m_Ctx.m_File;

	if( !m_Ctx.m_File.IsEmpty() ) {

		Text += " - ";
		}

	switch( m_Ctx.m_uState ) {

		case 0:
			Text += m_pTxtCannot->GetText(L"Cannot Load File");
			break;
		
		case 1:
			Text += CPrintf("Loading Page %d", m_Ctx.m_nPage + 1);
			break;
		
		case 2:
			Text += CPrintf("Showing Page %d of %d", m_Ctx.m_nPage + 1, m_Ctx.m_pDisplay->m_nPages);
			break;

		case 3:
			Text += "Not Enough Memory to Display PDF";
			break;
		}

	if( FALSE ) {

		Text += CPrintf(", org %d, %d", PassPoint(m_Ctx.m_Origin));

		Text += CPrintf(", res %d", m_Ctx.m_nRes);
		}
	
	pGDI->SelectFont(m_FontText);
	
	pGDI->SetTextFore (0x7FFF);

	pGDI->SetBrushFore(GetRGB(20,0,0));

	pGDI->FillRect(PassRect(Rect));

	pGDI->TextOut(Rect.x1 + 2, Rect.y1 + 2, Text);
	}

void CPrimPDFViewer::DrawScroll(IGDI *pGDI)
{
	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->FillRect(PassRect(m_Scroll));

	for( UINT n = 0; n < 3; n ++ ) {

		R2 Rect;

		FindScroll(Rect, n, m_Ctx.m_nPage);

		DeflateRect(Rect, 1, 1);

		BOOL fPress = (m_Ctx.m_uScroll & (1 << n));

		DrawButton(pGDI, Rect, fPress, TRUE);
		}
	}

void CPrimPDFViewer::DrawPDF(IGDI *pGDI)
{
	if( m_Ctx.m_pDisplay ) {
		
		int xp    = m_Work.x1;

		int yp    = m_Work.y1;

		int cx    = m_Work.x2 - m_Work.x1;

		int cy    = m_Work.y2 - m_Work.y1;

		int dx    = (cx - m_Ctx.m_pDisplay->m_Bitmap.cx);

		int dy    = (cy - m_Ctx.m_pDisplay->m_Bitmap.cy);

		int nSize = m_Ctx.m_pDisplay->m_nSize;

		MakeMax(dx, 0);

		MakeMax(dy, 0);

		PBYTE pWork = m_Ctx.m_pDisplay->m_pData + (m_Ctx.m_Origin.y * nSize) + 3 * m_Ctx.m_Origin.x;

		int x = m_Ctx.m_pDisplay->m_Bitmap.cx;

		int y = m_Ctx.m_pDisplay->m_Bitmap.cy; 

		PDWORD pData = New DWORD [x * y];

		for( int r = 0, i = 0; r < y; r++ ) {
				
			PCBYTE pLine = pWork + r * x * 3;

			for( int n = 0; n < x; n++ ) {

				BYTE  r  = *pLine++;
				BYTE  g  = *pLine++;
				BYTE  b  = *pLine++;
					
				pData[i]  = (r | g<<8 | b<<16);

				i++;
				}
			}

		nSize = nSize / 3 * 4;

		pGDI->SetBrushFore(m_pBack->GetColor());
		
		pGDI->FillRect(PassRect(m_Work));

		pGDI->BitBlt( xp + dx / 2, 
				yp + dy / 2,
				cx - dx,
				cy - dy,
				nSize,
				PBYTE(pData),
				ropSRCCOPY
				);

		delete [] pData;
		}
	}

void CPrimPDFViewer::DrawWorking(IGDI *pGDI)
{
	BOOL fDraw = (m_Ctx.m_uState == 1);

	if( fDraw && m_pTxtWorking->IsAvail() ) {

		CUnicode Text = m_pTxtWorking->GetText() + L"...";

		pGDI->SelectFont(m_FontText);
		
		int cx = pGDI->GetTextWidth(Text);
		
		int cy = pGDI->GetTextHeight(L"X");

		R2 Rect;

		Rect.x1 = (m_Work.x1 + m_Work.x2 - cx) / 2;

		Rect.y1 = (m_Work.y1 + m_Work.y2 - cy) / 2;

		Rect.x2 = (m_Work.x1 + m_Work.x2 + cx) / 2;

		Rect.y2 = (m_Work.y1 + m_Work.y2 + cy) / 2;

		InflateRect(Rect, 8, 8);

		pGDI->SetTextFore (0x7FFF);

		pGDI->SetBrushFore(GetRGB(20,0,0));

		pGDI->FillRect(PassRect(Rect));

		pGDI->TextOut(Rect.x1 + 8, Rect.y1 + 8, Text);
		
		pGDI->SetPenFore(GetRGB(31,31,31));

		DeflateRect(Rect, 1, 1);
		
		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 2, 2);
		
		pGDI->DrawRect(PassRect(Rect));
		}
	}

void CPrimPDFViewer::DrawButton(IGDI *pGDI, R2 Rect, BOOL fPress, BOOL fEnable)
{
	P2 h[6], s[6];

	FindPoints(h, s, Rect, 2);

	pGDI->SetTextFore (fEnable ? GetRGB(0,0,0) : GetRGB(18,18,18));

	pGDI->SelectBrush (brushFore);

	pGDI->SetBrushFore(GetRGB(8,8,8));

	pGDI->FillPolygon (fPress ? h : s, 6, 0);

	pGDI->SetBrushFore(GetRGB(24,24,24));

	pGDI->FillPolygon (fPress ? s : h, 6, 0);
	}

void CPrimPDFViewer::FindPoints(P2 *t, P2 *b, R2 &r, int s)
{
	R2 Rect1 = r;

	R2 Rect2 = r;

	DeflateRect(Rect2, s - 1, s - 1);

	Rect1.x2--;
	Rect1.y2--;
	Rect2.x2--;
	Rect2.y2--;

	t[0].x = Rect1.x1; t[0].y = Rect1.y2;
	t[1].x = Rect1.x1; t[1].y = Rect1.y1;
	t[2].x = Rect1.x2; t[2].y = Rect1.y1;
	t[3].x = Rect2.x2; t[3].y = Rect2.y1;
	t[4].x = Rect2.x1; t[4].y = Rect2.y1;
	t[5].x = Rect2.x1; t[5].y = Rect2.y2;

	b[0].x = Rect1.x1; b[0].y = Rect1.y2;
	b[1].x = Rect1.x2; b[1].y = Rect1.y2;
	b[2].x = Rect1.x2; b[2].y = Rect1.y1;
	b[3].x = Rect2.x2; b[3].y = Rect2.y1;
	b[4].x = Rect2.x2; b[4].y = Rect2.y2;
	b[5].x = Rect2.x1; b[5].y = Rect2.y2;

	Rect2.x1 += 1;
	Rect2.y1 += 1;

	r = Rect2;
	}

// Context Handling

void CPrimPDFViewer::FindCtx(CCtx &Ctx)
{
	CUnicode File;

	if( m_pFile && m_pFile->IsAvail() ) {

		File = m_pFile->GetText();
		}
		
	Ctx.m_File      = File;
		
	Ctx.m_uScroll   = m_uScroll;

	Ctx.m_nPage     = m_nPage;

	Ctx.m_nRes      = m_nRes;

	Ctx.m_Origin.x  = m_Origin.x;

	Ctx.m_Origin.y  = m_Origin.y;

	Ctx.m_uState    = m_uState;

	Ctx.m_fLoad     = m_fLoad;

	Ctx.m_pDisplay  = m_pDisplay;
	}

void CPrimPDFViewer::ClearCtx(CCtx &Ctx)
{
	Ctx.m_File.Empty();

	Ctx.m_uScroll   = 0;

	Ctx.m_nPage     = 0;

	Ctx.m_nRes      = 0;

	Ctx.m_Origin.x  = 0;

	Ctx.m_Origin.y  = 0;

	Ctx.m_uState    = 0;

	Ctx.m_fLoad     = FALSE;

	Ctx.m_pDisplay  = NULL;
	}

// Context Check

BOOL CPrimPDFViewer::CCtx::operator == (CCtx const &That) const
{
	if( !m_File.CompareC(That.m_File) ) {
		
		return  m_uScroll   == That.m_uScroll   &&
			m_nPage     == That.m_nPage     &&
			m_nRes      == That.m_nRes      &&
			m_Origin.x  == That.m_Origin.x  &&
			m_Origin.y  == That.m_Origin.y  &&
			m_uState    == That.m_uState    &&
			m_fLoad     == That.m_fLoad     &&
			m_pDisplay  == That.m_pDisplay  ;;
		}

	return FALSE;
	}

// Instantiator

global CPrim * Create_PrimPDFViewer(void)
{
	if( WhoHasFeature(rfPdfViewer) ) {

		return New CPrimPDFViewer();
		}

	return New CPrimViewerUnsupported();
	}

// End of File
