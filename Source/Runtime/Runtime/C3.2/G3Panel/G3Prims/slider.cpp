
#include "intern.hpp"

#include "slider.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Slider
//

// Constructor

CPrimLegacySlider::CPrimLegacySlider(void)
{
	m_Mode    = 1;

	m_Accel   = 1;

	m_Entry   = 1;

	m_pKnob   = New CPrimBrush;

	m_pEdge   = New CPrimPen;

	m_pBack   = New CPrimColor(GetRGB(0,0,0));

	m_fPress1 = FALSE;

	m_fPress2 = FALSE;

	m_Step    = 0;
	}

// Destructor

CPrimLegacySlider::~CPrimLegacySlider(void)
{
	delete m_pKnob;

	delete m_pEdge;

	delete m_pBack;
	}

// Initialization

void CPrimLegacySlider::Load(PCBYTE &pData)
{
	CPrimRich::Load(pData);

	m_Mode  = GetByte(pData);

	m_Accel = GetByte(pData);

	m_Entry = GetByte(pData);

	m_pKnob->Load(pData);

	m_pEdge->Load(pData);

	m_pBack->Load(pData);
	}

// Overridables

void CPrimLegacySlider::SetScan(UINT Code)
{
	m_pKnob->SetScan(Code);

	m_pEdge->SetScan(Code);

	m_pBack->SetScan(Code);

	CPrimRich::SetScan(Code);
	}

void CPrimLegacySlider::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRich::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_pKnob->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}
		}
	}

void CPrimLegacySlider::LoadTouchMap(ITouchMap *pTouch)
{
	// REV3 -- It would be nice to load the buttons or
	// the slider according to which we're using, but we
	// have not completed the layout at this point...

	pTouch->FillRect(PassRect(m_DrawRect));
	}

UINT CPrimLegacySlider::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgTakeFocus:

			return OnTakeFocus(uMsg, uParam);

		case msgKillFocus:

			if( m_fPress1 || m_fPress2 ) {

				OnTouchUp();

				return TRUE;
				}
			
			return FALSE;

		case msgTouchInit:
		case msgTouchDown:

			return OnTouchDown(uParam);

		case msgTouchRepeat:

			return OnTouchRepeat();

		case msgTouchUp:

			return OnTouchUp();

		case msgIsPressed:

			return m_Ctx.m_fPress1 || m_Ctx.m_fPress2;
		}

	return CPrimRich::OnMessage(uMsg, uParam);
	}

// Message Handlers

UINT CPrimLegacySlider::OnTakeFocus(UINT uMsg, UINT uParam)
{
	if( m_Entry ) {

		if( uParam == 0 ) {

			return m_Ctx.m_fAvail && m_Ctx.m_fEnable;
			}

		if( uParam == 1 ) {

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CPrimLegacySlider::OnTouchDown(UINT uParam)
{
	if( m_Mode ) {

		P2 Pos;
		
		Pos.x = LOWORD(uParam);
		
		Pos.y = HIWORD(uParam);

		if( PtInRect(m_Btn1, Pos) ) {

			m_fPress1 = TRUE;

			StepData();

			return TRUE;
			}

		if( PtInRect(m_Btn2, Pos) ) {

			m_fPress2 = TRUE;

			StepData();

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CPrimLegacySlider::OnTouchRepeat(void)
{
	if( m_fPress1 || m_fPress2 ) {
	
		StepData();

		return TRUE;
		}

	return FALSE;
	}

UINT CPrimLegacySlider::OnTouchUp(void)
{
	if( m_fPress1 ) {

		m_fPress1 = FALSE;

		m_Step    = 0;

		return TRUE;
		}

	if( m_fPress2 ) {

		m_fPress2 = FALSE;

		m_Step    = 0;

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CPrimLegacySlider::DrawBase(IGDI *pGDI, R2 &Rect)
{
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);
		}

	pGDI->ResetBrush();

	pGDI->SetBrushFore(m_Ctx.m_Back);

	pGDI->FillRect(PassRect(Rect));
	}

void CPrimLegacySlider::DrawBtnFrame(IGDI *pGDI, R2 &Rect, BOOL fPress)
{
	P2 h[6];
	
	P2 s[6];

	FindPoints(h, s, Rect, 2);

	pGDI->SelectBrush(brushFore);

	pGDI->SetBrushFore(GetRGB(8,8,8));

	pGDI->FillPolygon(fPress ? h : s, 6, 0);

	pGDI->SetBrushFore(GetRGB(24,24,24));

	pGDI->FillPolygon(fPress ? s : h, 6, 0);
	}

void CPrimLegacySlider::FindPoints(P2 *t, P2 *b, R2 &r, int s)
{
	R2 Rect1 = r;

	R2 Rect2 = r;

	DeflateRect(Rect2, s - 1, s - 1);

	Rect1.x2--;
	Rect1.y2--;
	Rect2.x2--;
	Rect2.y2--;

	t[0].x = Rect1.x1; t[0].y = Rect1.y2;
	t[1].x = Rect1.x1; t[1].y = Rect1.y1;
	t[2].x = Rect1.x2; t[2].y = Rect1.y1;
	t[3].x = Rect2.x2; t[3].y = Rect2.y1;
	t[4].x = Rect2.x1; t[4].y = Rect2.y1;
	t[5].x = Rect2.x1; t[5].y = Rect2.y2;

	b[0].x = Rect1.x1; b[0].y = Rect1.y2;
	b[1].x = Rect1.x2; b[1].y = Rect1.y2;
	b[2].x = Rect1.x2; b[2].y = Rect1.y1;
	b[3].x = Rect2.x2; b[3].y = Rect2.y1;
	b[4].x = Rect2.x2; b[4].y = Rect2.y2;
	b[5].x = Rect2.x1; b[5].y = Rect2.y2;

	Rect2.x1 += 1;
	Rect2.y1 += 1;

	r = Rect2;
	}

void CPrimLegacySlider::SetBtnColor(IGDI *pGDI)
{
	pGDI->SetBrushFore(m_Ctx.m_fEnable ? GetRGB(0,0,0) : GetRGB(12,12,12));
	}

void CPrimLegacySlider::StepData(void)
{
	if( m_pValue ) {

		UINT Type = m_pValue->GetType();

		INT  Sign = m_fPress1 ? +1 : -1;

		CDispFormat *pFormat;

		if( GetDataFormat(pFormat) ) {

			if( !m_Step ) {

				m_Step   = pFormat->GetStep(Type);

				m_uCount = 0;

				m_uLevel = m_Accel;
				}
			else {
				if( m_uLevel ) {

					if( pFormat->SpeedUp(++m_uCount) ) {

						if( pFormat->SpeedUp(m_Ctx.m_Val, Type, m_Step) ) {

							m_uLevel = m_uLevel - 1;

							m_uCount = 0;
							}
						}
					}
				}
			}
		else {
			if( !m_Step ) {

				if( Type == typeReal ) {
	
					m_Step = R2I(1);
					}
				else
					m_Step = DWORD(1);

				m_uCount = 0;
				}
			}

		if( Type == typeReal ) {

			C3REAL Value = I2R(m_Ctx.m_Val) + Sign * I2R(m_Step);

			MakeMin(Value, I2R(m_Ctx.m_Max));

			MakeMax(Value, I2R(m_Ctx.m_Min));

			m_pValue->SetValue(R2I(Value), Type, setNone);

			Execute(m_pOnComplete);
			}

		if( Type == typeInteger ) {

			if( !m_Step ) {

				CDispFormat *pFormat;

				if( GetDataFormat(pFormat) ) {

					m_Step = pFormat->GetStep(Type);
					}
				else
					m_Step = DWORD(1);
				}

			C3INT Value = C3INT(m_Ctx.m_Val) + Sign * C3INT(m_Step);

			MakeMin(Value, C3INT(m_Ctx.m_Max));

			MakeMax(Value, C3INT(m_Ctx.m_Min));

			m_pValue->SetValue(DWORD(Value), Type, setNone);

			Execute(m_pOnComplete);
			}
		}
	}

BOOL CPrimLegacySlider::SetFromPos(C3REAL Pos)
{
	if( m_pValue ) {

		UINT Type = m_pValue->GetType();

		if( Type == typeReal ) {

			C3REAL Min   = I2R(m_Ctx.m_Min);

			C3REAL Max   = I2R(m_Ctx.m_Max);

			C3REAL Value = Min + (Max - Min) * Pos;

			MakeMin(Value, Max);

			MakeMax(Value, Min);

			m_pValue->SetValue(R2I(Value), Type, setNone);

			Execute(m_pOnComplete);

			return TRUE;
			}

		if( Type == typeInteger ) {

			C3INT Min   = C3INT(m_Ctx.m_Min);

			C3INT Max   = C3INT(m_Ctx.m_Max);

			C3INT Value = Min + C3INT((Max - Min) * Pos);

			MakeMin(Value, Max);

			MakeMax(Value, Min);

			m_pValue->SetValue(DWORD(Value), Type, setNone);

			Execute(m_pOnComplete);

			return TRUE;
			}
		}

	return FALSE;
	}

// Context Creation

void CPrimLegacySlider::FindCtx(CCtx &Ctx)
{
	if( m_pValue ) {

		if( IsAvail() ) {

			UINT Type = m_pValue->GetType();

			Ctx.m_fAvail = TRUE;

			Ctx.m_Val    = m_pValue->Execute(Type);

			Ctx.m_Min    = GetMinValue(Type);
			
			Ctx.m_Max    = GetMaxValue(Type);

			Ctx.m_Pos    = 0;

			if( Type == typeReal ) {

				C3REAL rVal = I2R(Ctx.m_Val);

				C3REAL rMin = I2R(Ctx.m_Min);

				C3REAL rMax = I2R(Ctx.m_Max);

				MakeMin(rVal, rMax);

				MakeMax(rVal, rMin);

				if( rMax - rMin ) {

					Ctx.m_Pos = (rVal - rMin) / (rMax - rMin);
					}
				}
			else {
				C3INT nVal = Ctx.m_Val;

				C3INT nMin = Ctx.m_Min;

				C3INT nMax = Ctx.m_Max;

				MakeMin(nVal, nMax);

				MakeMax(nVal, nMin);

				if( nMax - nMin ) {

					Ctx.m_Pos = C3REAL(nVal - nMin) / C3REAL(nMax - nMin);
					}
				}
			}
		else {
			Ctx.m_fAvail = FALSE;

			Ctx.m_Pos    = 0.25;
			}
		}
	else {
		Ctx.m_fAvail = TRUE;
		
		Ctx.m_Pos    = 0.25;
		}

	if( !m_Entry ) {

		Ctx.m_Min = 0;

		Ctx.m_Max = 0;

		Ctx.m_Val = 0;
		}

	Ctx.m_Back    = m_pBack->GetColor();

	Ctx.m_fEnable = (!m_pEnable || m_pEnable->ExecVal());

	Ctx.m_fPress1 = m_fPress1;

	Ctx.m_fPress2 = m_fPress2;
	}

// Context Check

BOOL CPrimLegacySlider::CCtx::operator == (CCtx const &That) const
{
	return m_fAvail  == That.m_fAvail  &&
	       m_Val     == That.m_Val     &&
	       m_Min     == That.m_Min     &&
	       m_Max     == That.m_Max     &&
	       m_Pos     == That.m_Pos     &&
	       m_Back    == That.m_Back    &&
	       m_fEnable == That.m_fEnable &&
	       m_fPress1 == That.m_fPress1 &&
	       m_fPress2 == That.m_fPress2 ;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Slider
//

// Constructor

CPrimLegacyVertSlider::CPrimLegacyVertSlider(void)
{
	}

// Destructor

CPrimLegacyVertSlider::~CPrimLegacyVertSlider(void)
{
	}

// Initialization

void CPrimLegacyVertSlider::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyVertSlider", pData);

	CPrimLegacySlider::Load(pData);
	}

// Overridables

void CPrimLegacyVertSlider::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	DrawBase(pGDI, Rect);

	R2 Slot = Rect;

	DrawSlot(pGDI, Slot);

	m_yMin = Slot.y2 - 2;
	
	m_yMax = Slot.y1 + 2;

	if( m_Ctx.m_fAvail ) {
		
		int yPos = m_yMin + C3INT((m_yMax - m_yMin) * m_Ctx.m_Pos);

		R2 Knob;

		Knob.x1 = Rect.x1 + 2;
		
		Knob.x2 = Rect.x2 - 2;
		
		Knob.y1 = yPos - 8;
		
		Knob.y2 = yPos + 8;

		DrawBtnFrame(pGDI, Knob, FALSE);

		m_pKnob->FillRect(pGDI, Knob);
		}
	}

UINT CPrimLegacyVertSlider::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgTouchInit:
		case msgTouchDown:

			return OnTouchDown(uParam);
		}

	return CPrimLegacySlider::OnMessage(uMsg, uParam);
	}

// Message Handlers

UINT CPrimLegacyVertSlider::OnTouchDown(UINT uParam)
{
	if( m_Mode < 2 ) {

		int yPos = HIWORD(uParam);

		if( yPos >= m_yMax - 8 && yPos <= m_yMin + 8 ) {

			C3REAL Pos = C3REAL(yPos - m_yMin) / C3REAL(m_yMax - m_yMin);

			SetFromPos(Pos);

			return TRUE;
			}
		}

	return CPrimLegacySlider::OnTouchDown(uParam);
	}

// Implementation

void CPrimLegacyVertSlider::DrawSlot(IGDI *pGDI, R2 &Rect)
{
	int xm = (Rect.x1 + Rect.x2) / 2;

	if( m_Mode ) {

		int cy = (Rect.y2 - Rect.y1);

		int cx = (Rect.x2 - Rect.x1);

		MakeMin(cx, cy / 3);

		R2 Btn1 = Rect;
		R2 Btn2 = Rect;

		Btn1.x1 = Rect.x1 + 2;
		Btn1.x2 = Rect.x2 - 2;
		Btn1.y1 = Rect.y1 + 2;
		Btn1.y2 = Rect.y1 + cx - 4;

		Btn2.x1 = Rect.x1 + 2;
		Btn2.x2 = Rect.x2 - 2;
		Btn2.y1 = Rect.y2 - cx + 4;
		Btn2.y2 = Rect.y2 - 2;

		Rect.y1 = Btn1.y2 + 1;
		Rect.y2 = Btn2.y1 - 1;

		m_Btn1  = Btn1;
		m_Btn2  = Btn2;

		DrawBtn1(pGDI, Btn1);
		DrawBtn2(pGDI, Btn2);
		}

	Rect.x1 = xm - 2;
	Rect.x2 = xm + 2;
	
	Rect.y1 = Rect.y1 + 8;
	Rect.y2 = Rect.y2 - 8;

	pGDI->SetBrushFore(COLOR(0));

	pGDI->FillRect(PassRect(Rect));
	}

void CPrimLegacyVertSlider::DrawBtn1(IGDI *pGDI, R2 Rect)
{
	DrawBtnFrame(pGDI, Rect, m_Ctx.m_fPress1);

	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->FillRect(PassRect(Rect));

	P2 t[3];

	t[0].x = Rect.x1 + 4;
	
	t[1].x = Rect.x2 - 5;
	
	t[2].x = (t[0].x + t[1].x) / 2;

	t[0].y = Rect.y2 - 5;
	
	t[1].y = Rect.y2 - 5;
	
	t[2].y = Rect.y1 + 4;

	SetBtnColor(pGDI);

	pGDI->FillPolygon(t, 3, 0);
	}

void CPrimLegacyVertSlider::DrawBtn2(IGDI *pGDI, R2 Rect)
{
	DrawBtnFrame(pGDI, Rect, m_Ctx.m_fPress2);

	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->FillRect(PassRect(Rect));

	P2 t[3];

	t[0].x = Rect.x1 + 4;
	
	t[1].x = Rect.x2 - 5;
	
	t[2].x = (t[0].x + t[1].x) / 2;

	t[0].y = Rect.y1 + 4;
	
	t[1].y = Rect.y1 + 4;
	
	t[2].y = Rect.y2 - 5;

	SetBtnColor(pGDI);

	pGDI->FillPolygon(t, 3, 0);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Slider
//

// Constructor

CPrimLegacyHorzSlider::CPrimLegacyHorzSlider(void)
{
	}

// Destructor

CPrimLegacyHorzSlider::~CPrimLegacyHorzSlider(void)
{
	}

// Initialization

void CPrimLegacyHorzSlider::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyHorzSlider", pData);

	CPrimLegacySlider::Load(pData);
	}

// Overridables

void CPrimLegacyHorzSlider::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	DrawBase(pGDI, Rect);

	R2 Slot = Rect;

	DrawSlot(pGDI, Slot);

	m_xMin = Slot.x1 + 2;
	
	m_xMax = Slot.x2 - 2;
	
	if( m_Ctx.m_fAvail ) {
		
		int xPos = m_xMin + C3INT((m_xMax - m_xMin) * m_Ctx.m_Pos);

		R2 Knob;

		Knob.x1 = xPos - 8;
		
		Knob.x2 = xPos + 8;

		Knob.y1 = Rect.y1 + 2;
		
		Knob.y2 = Rect.y2 - 2;

		DrawBtnFrame(pGDI, Knob, FALSE);

		m_pKnob->FillRect(pGDI, Knob);
		}
	}

UINT CPrimLegacyHorzSlider::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgTouchInit:
		case msgTouchDown:

			return OnTouchDown(uParam);
		}

	return CPrimLegacySlider::OnMessage(uMsg, uParam);
	}

// Message Handlers

UINT CPrimLegacyHorzSlider::OnTouchDown(UINT uParam)
{
	if( m_Mode < 2 ) {

		int xPos = LOWORD(uParam);

		if( xPos >= m_xMin - 8 && xPos <= m_xMax + 8 ) {

			C3REAL Pos = C3REAL(xPos - m_xMin) / C3REAL(m_xMax - m_xMin);

			SetFromPos(Pos);

			return TRUE;
			}
		}

	return CPrimLegacySlider::OnTouchDown(uParam);
	}

// Implementation

void CPrimLegacyHorzSlider::DrawSlot(IGDI *pGDI, R2 &Rect)
{
	int ym = (Rect.y1 + Rect.y2) / 2;

	if( m_Mode ) {

		int cx = (Rect.x2 - Rect.x1);

		int cy = (Rect.y2 - Rect.y1);

		MakeMin(cy, cx / 3);

		R2 Btn1 = Rect;
		R2 Btn2 = Rect;

		Btn1.x1 = Rect.x2 - cy + 4;
		Btn1.x2 = Rect.x2 - 2;
		Btn1.y1 = Rect.y1 + 2;
		Btn1.y2 = Rect.y2 - 2;

		Btn2.x1 = Rect.x1 + 2;
		Btn2.x2 = Rect.x1 + cy - 4;
		Btn2.y1 = Rect.y1 + 2;
		Btn2.y2 = Rect.y2 - 2;

		Rect.x1 = Btn2.x2 + 1;
		Rect.x2 = Btn1.x1 - 1;

		m_Btn1  = Btn1;
		m_Btn2  = Btn2;

		DrawBtn1(pGDI, Btn1);
		DrawBtn2(pGDI, Btn2);
		}
 	
	Rect.x1 = Rect.x1 + 8;
	Rect.x2 = Rect.x2 - 8;

	Rect.y1 = ym - 2;
	Rect.y2 = ym + 2;

	pGDI->SetBrushFore(COLOR(0));

	pGDI->FillRect(PassRect(Rect));
	}

void CPrimLegacyHorzSlider::DrawBtn1(IGDI *pGDI, R2 Rect)
{
	DrawBtnFrame(pGDI, Rect, m_Ctx.m_fPress1);

	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->FillRect(PassRect(Rect));

	P2 t[3];

	t[0].x = Rect.x1 + 4;
	
	t[1].x = Rect.x1 + 4;
	
	t[2].x = Rect.x2 - 5;

	t[0].y = Rect.y1 + 4;
	
	t[1].y = Rect.y2 - 5;
	
	t[2].y = (t[0].y + t[1].y) / 2;

	SetBtnColor(pGDI);

	pGDI->FillPolygon(t, 3, 0);
	}

void CPrimLegacyHorzSlider::DrawBtn2(IGDI *pGDI, R2 Rect)
{
	DrawBtnFrame(pGDI, Rect, m_Ctx.m_fPress2);

	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->FillRect(PassRect(Rect));

	P2 t[3];

	t[0].x = Rect.x2 - 5;
	
	t[1].x = Rect.x2 - 5;
	
	t[2].x = Rect.x1 + 4;

	t[0].y = Rect.y1 + 4;
	
	t[1].y = Rect.y2 - 5;
	
	t[2].y = (t[0].y + t[1].y) / 2;

	SetBtnColor(pGDI);

	pGDI->FillPolygon(t, 3, 0);
	}

// End of File
