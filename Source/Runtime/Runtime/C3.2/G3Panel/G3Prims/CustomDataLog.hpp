
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CustomDataLog_HPP
	
#define	INCLUDE_CustomDataLog_HPP

//////////////////////////////////////////////////////////////////////////
//
// Custom Data Log File
//

class CCustomDataLog
{
	public:
		// Constructor
		CCustomDataLog(UINT uChans);

		// Destructor
		~CCustomDataLog(void);

		// Operations
		BOOL MakeFile(PCTXT pPath);

	protected:
		// Data
		UINT	m_uChans;
		DWORD	m_dwStart;
		DWORD	m_dwPitch;
		DWORD   m_dwMin;
		DWORD   m_dwMax;
		DWORD   m_dwDPs;
		DWORD   m_uSamps;

		IFile *	m_pFile;

		// System Objects
		IFileManager   * m_pFileMan;
		IFilingSystem  * m_pFileSys;

		// Implementation
		void WriteHead(void);
		void WriteForm(void);
		void WriteData(void);

		void LockSession(void);
		void FreeSession(void);
	};

// End of File

#endif
