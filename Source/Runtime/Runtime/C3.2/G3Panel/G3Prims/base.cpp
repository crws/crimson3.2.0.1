
#include "intern.hpp"

#include "uitask.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Primitives
//

#include "poly.hpp"
#include "bevel.hpp"
#include "button.hpp"
#include "scale.hpp"
#include "legacy.hpp"
#include "viewer.hpp"
#include "trend.hpp"
#include "touch.hpp"
#include "dial.hpp"
#include "graph.hpp"
#include "bar.hpp"
#include "ticker.hpp"
#include "slider.hpp"
#include "select.hpp"
#include "quick.hpp"
#include "pdf.hpp"
#include "file.hpp"
#include "remote.hpp"
#include "camera.hpp"
#include "barcode.hpp"

#include "PrimRubyLine.hpp"
#include "PrimRubyGeom.hpp"
#include "PrimRubySimpleImage.hpp"
#include "PrimRubyAnimImage.hpp"
#include "PrimRubyBevel.hpp"
#include "PrimRubyBevelButton.hpp"
#include "PrimRubyGradButton.hpp"
#include "PrimRubyGaugeTypeAR.hpp"
#include "PrimRubyGaugeTypeAL.hpp"

/*
#include "PrimMurphyTrendViewer.hpp"
*/

//////////////////////////////////////////////////////////////////////////
//
// Graphics Primitive
//

// Class Creation

CPrim * CPrim::MakeObject(UINT uType)
{
	switch( uType ) {

		case 0x01: return New CPrimRect;
		case 0x02: return New CPrimEllipse;
		case 0x03: return New CPrimWedge;
		case 0x04: return New CPrimEllipseQuad;
		case 0x05: return New CPrimEllipseHalf;
		case 0x06: return New CPrimTextBox;
		case 0x07: return New CPrimDataBox;
		case 0x08: return New CPrimPoly;
		case 0x09: return New CPrimBevel;
		case 0x0A: return New CPrimAnimImage;
		case 0x0B: return New CPrimSimpleImage;
		case 0x0C: return New CPrimGroup;
		case 0x0D: return New CPrimWidget;
		case 0x0E: return New CPrimLine;
		case 0x0F: return New CPrimGradButton;
		case 0x10: return New CPrimImageIndicator;
		case 0x11: return New CPrimImageIllumButton;
		case 0x12: return New CPrimImageButton;
		case 0x13: return New CPrimImageToggle2;
		case 0x14: return New CPrimImageToggle3;
		case 0x15: return New CPrimBevelButton;
		case 0x16: return New CPrimPolyTrans;
		case 0x17: return New CPrimVertScale;
		case 0x18: return New CPrimLegacyVertSlider;
		case 0x19: return New CPrimLegacyHorzSlider;
		case 0x1A: return New CPrimAlarmViewer;
		case 0x1B: return New CPrimEventViewer;
		case 0x1C: return Create_PrimFileViewer();
		case 0x1D: return New CPrimTrendViewer;
		case 0x1E: return New CPrimUserManager;
		case 0x1F: return New CPrimTouchCalib;
		case 0x20: return New CPrimTouchTester;		
		case 0x21: return New CPrimLegacyAlarmTicker;		
		case 0x22: return New CPrimLegacyVertScale;
		case 0x23: return New CPrimLegacyHorzScale;		
		case 0x24: return New CPrimLegacyWholeDial;
		case 0x25: return New CPrimLegacyHalfDial;
		case 0x26: return New CPrimLegacyQuadDial;
		case 0x27: return New CPrimLegacyCfImage;
		case 0x28: return New CPrimLegacyVertBarGraph;
		case 0x29: return New CPrimLegacyHorzBarGraph;
		case 0x2A: return New CPrimLegacyScatterGraph;
		case 0x2B: return New CPrimLegacyVertBar;
		case 0x2C: return New CPrimLegacyHorzBar;
		case 0x2D: return New CPrimLegacySelectorTwo;
		case 0x2E: return New CPrimLegacySelectorMulti;
		case 0x2F: return New CPrimLegacyShadow;
		case 0x30: return New CPrimMove2D;
		case 0x31: return New CPrimMovePolar;
		case 0x32: return New CPrimQuickPlot;
		case 0x33: return Create_PrimPDFViewer();
		case 0x34: return New CPrimRemoteDisplay;
		case 0x35: return New CPrimCamera;
		case 0x36: return New CPrimBarcode;

//		case 0x40: return New CPrimMurphyTrendViewer;

		case 0x80: return New CPrimRubyLine;
		case 0x81: return New CPrimRubyGeom;
		case 0x82: return New CPrimRubySimpleImage;
		case 0x83: return New CPrimRubyAnimImage;
		case 0x84: return New CPrimRubyBevel;
		case 0x85: return New CPrimRubyBevelButton;
		case 0x86: return New CPrimRubyGradButton;

		case 0xC0: return New CPrimRubyGaugeTypeAR;
		case 0xC1: return New CPrimRubyGaugeTypeAL;
		}

	AfxTrace("Unknown Primitive %2.2X!\n", uType);

	Sleep(1000);

	return NULL;
	}

// Static Data

BOOL	     CPrim::m_fSuspend;

BOOL         CPrim::m_fStacked;

BOOL	     CPrim::m_fExStack;

CWidgetStack CPrim::m_Stack;

// Constructor

CPrim::CPrim(void)
{
	m_pVisible = NULL;

	m_fShow    = TRUE;

	m_fChange  = TRUE;

	m_uHandle  = NOTHING;
	}

// Destructor

CPrim::~CPrim(void)
{
	delete m_pVisible;
	}

// IBase

UINT CPrim::Release(void)
{
	delete this;

	return 0;
	}

// Initialization

void CPrim::Load(PCBYTE &pData)
{
	m_DrawRect.x1 = GetWord(pData);
	m_DrawRect.y1 = GetWord(pData);
	m_DrawRect.x2 = GetWord(pData);
	m_DrawRect.y2 = GetWord(pData);

	GetCoded(pData, m_pVisible);
	}

// Stack Control

void CPrim::ClearWidgetStack(void)
{
	m_fSuspend = FALSE;

	m_fStacked = FALSE;

	m_Stack.Empty();
	}

void CPrim::SuspendWidgetStack(CWidgetStack &Stack)
{
	LeaveWidgetStack(m_Stack);

	Stack = m_Stack;

	m_Stack.Empty();

	m_fStacked = FALSE;
	}

void CPrim::RestoreWidgetStack(CWidgetStack &Stack)
{
	m_Stack    = Stack;

	m_fStacked = (Stack.GetCount() > 0);

	EnterWidgetStack(m_Stack);
	}

void CPrim::SuspendWidgetStack(void)
{
	if( !m_fSuspend ) {

		LeaveWidgetStack(m_Stack);

		m_fExStack = m_fStacked;

		m_fStacked = FALSE;

		m_fSuspend = TRUE;
		}
	else {
		AfxTrace("*** CPrim::SuspendWidgetStack was nested\n");

		Sleep(100);

		HostTrap(3);
		}
	}

void CPrim::RestoreWidgetStack(void)
{
	if( m_fSuspend ) {

		EnterWidgetStack(m_Stack);

		m_fStacked = m_fExStack;

		m_fSuspend = FALSE;
		}
	else {
		AfxTrace("*** CPrim::RestoreWidgetStack out of order\n");

		Sleep(100);

		HostTrap(3);
		}
	}

void CPrim::TempLeave(CPrimWidget *pWidget)
{
	if( m_fStacked ) {

		UINT c = m_Stack.GetCount();

		if( c == 0 ) {

			AfxTrace("*** CPrim::TempLeave with no widget\n");

			Sleep(100);

			HostTrap(3);
			}

		if( m_Stack[c-1] != pWidget ) {

			AfxTrace("*** CPrim::TempLeave with incorrect widget\n");

			Sleep(100);

			HostTrap(3);
			}

		m_Stack.Remove(c-1, 1);
		}

	pWidget->Leave();
	}

void CPrim::TempEnter(CPrimWidget *pWidget)
{
	if( m_fStacked ) {
	
		m_Stack.Append(pWidget);
		}

	pWidget->Enter();
	}

// Attributes

BOOL CPrim::IsPressed(void) const
{
	CPrim *pPrim = (CPrim *) this;

	if( pPrim->OnMessage(msgIsPressed, 0) ) {
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CPrim::IsVisible(void) const
{
	if( m_pParent ) {
		
		if( !m_pParent->IsVisible() ) {
			
			return FALSE;
			}		
		}

	return m_fShow;
	}

UINT CPrim::GetHandle(void)
{
	return m_uHandle;
	}

// Operations

UINT CPrim::SendMessage(UINT uMsg, UINT uParam)
{
	CUIDataServer *pData = CUISystem::m_pThis->m_pUserServer;

	CPrim         *pPrev = pData->GetPrim();

	UINT           uCode = 0;

	pData->SetPrim(this);

	if( !m_fSuspend ) {

		if( m_Stack.IsEmpty() ) {

			if( BuildWidgetStack(m_Stack) ) {

				EnterWidgetStack(m_Stack);

				uCode = OnMessage(uMsg, uParam);

				LeaveWidgetStack(m_Stack);

				ClearWidgetStack();
				}
			else
				uCode = OnMessage(uMsg, uParam);
			}
		else {
			AfxTrace("*** CPrim::SendMessage was nested %u, %u\n", uMsg, uParam);

			Sleep(100);

			HostTrap(3);
			}
		}
	else {
		AfxTrace("*** CPrim::SendMessage while suspended\n");

		Sleep(100);

		HostTrap(3);
		}

	pData->SetPrim(pPrev);

	return uCode;
	}

void CPrim::SetHandle(UINT uHandle)
{
	m_uHandle = uHandle;
	}

// Overridables

void CPrim::SetScan(UINT Code)
{
	SetItemScan(m_pVisible, Code);
	}

BOOL CPrim::HitTest(P2 const &Pos)
{
	if( m_fShow ) {

		R2 Rect = GetBackRect();
	
		return PtInRect(Rect, Pos);
		}

	return FALSE;
	}

BOOL CPrim::HitTest(IRegion *pDirty)
{
	R2 Rect = GetBackRect();

	return pDirty->HitTest(Rect);
	}

void CPrim::MovePrim(int cx, int cy)
{
	m_DrawRect.x1 += cx;

	m_DrawRect.y1 += cy;

	m_DrawRect.x2 += cx;

	m_DrawRect.y2 += cy;
	}

void CPrim::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	SetShow(Erase, TRUE);
	}

void CPrim::DrawExec(IGDI *pGDI, IRegion *pDirty)
{
	if( m_fShow ) {

		if( m_fChange || HitTest(pDirty) ) {

			DrawPrim(pGDI);

			R2 Rect = GetBackRect();

			pDirty->AddRect(Rect);

			m_fChange = FALSE;
			}
		}
	}

void CPrim::DrawPrim(IGDI *pGDI)
{
	}

void CPrim::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_DrawRect));
	}

UINT CPrim::OnMessage(UINT uMsg, UINT uPram)
{
	if( uMsg == msgGoInvisible ) {

		m_fShow = FALSE;
		
		return TRUE;
		}

	return FALSE;
	}

R2 CPrim::GetBackRect(void)
{
	return m_DrawRect;
	}

// Stack Helpers

void CPrim::EnterWidgetStack(CWidgetStack &Stack)
{
	UINT c = Stack.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		UINT p = n;

		Stack[p]->Enter();
		}
	}

void CPrim::LeaveWidgetStack(CWidgetStack &Stack)
{
	UINT c = Stack.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		UINT p = c - 1 - n;

		Stack[p]->Leave();
		}
	}

// Implementation

BOOL CPrim::SelectFont(IGDI *pGDI, UINT Font)
{
	if( Font < 0x100 ) {

		pGDI->SelectFont(Font);

		return TRUE;
		}

	return CUISystem::m_pThis->m_pUI->m_pFonts->Select(pGDI, Font);
	}

BOOL CPrim::BuildWidgetStack(CWidgetStack &Stack)
{
	for( CPrim *pWalk = m_pParent; pWalk; pWalk = pWalk->m_pParent ) {

		if( pWalk->OnMessage(msgIsGroup, 0) == 2 ) {

			CPrimWidget *pWidget = (CPrimWidget *) pWalk;

			Stack.Insert(0, pWidget);
			}
		}

	if( Stack.GetCount() ) {

		m_fStacked = TRUE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrim::SetShow(CR2Array &Erase, BOOL fShow)
{
	if( fShow && m_pVisible ) {
		
		if( !m_pVisible->IsAvail() || !m_pVisible->ExecVal() ) {

			fShow = FALSE;
			}
		}

	if( m_fShow != fShow ) {

		if( fShow ) {

			m_fChange = TRUE;
			}
		else {
			Erase.Append(GetBackRect());
			
			OnMessage(msgGoInvisible, 0);
			}

		m_fShow = fShow;
		}

	return m_fShow;
	}

BOOL CPrim::SetDelayTimer(UINT uStart, UINT uDelay)
{
	if( uDelay ) {

		UINT uTicks = ToTicks(uDelay);

		UINT uTime  = GetTickCount();

		UINT uGone  = uTime - uStart;

		if( uGone < uTicks ) {

			CUITask *pTask = CUISystem::m_pThis->m_pTask;

			if( pTask->SetDelayTimer(this, uTicks - uGone) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// End of File
