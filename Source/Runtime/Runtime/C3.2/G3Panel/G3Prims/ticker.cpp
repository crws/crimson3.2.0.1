
#include "intern.hpp"

#include "ticker.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Alarm Ticker
//

// Hair Space

#define textHair L"\x200A"

// Constructor

CPrimLegacyAlarmTicker::CPrimLegacyAlarmTicker(void)
{
	m_Font            = fontHei16;	
	m_pEdge           = New CPrimPen;
	m_NoAlarm         = MAKELONG(GetRGB( 0, 0, 0), GetRGB( 0,31, 0));
	m_Alarm           = MAKELONG(GetRGB(31,31,31), GetRGB(31, 0, 0));
	m_Accept          = MAKELONG(GetRGB( 0, 0, 0), GetRGB(31,31, 0));
	m_AcceptBlink     = 1;
	m_UseNoAlarmLabel = 1;
	m_UsePriority     = 0;
	m_Priority[0]     = MAKELONG(GetRGB(31,31,31), GetRGB(31, 0, 0));
	m_Priority[1]     = MAKELONG(GetRGB(31,31,31), GetRGB( 0, 0, 0));
	m_Priority[2]     = MAKELONG(GetRGB( 0, 0, 0), GetRGB(31,31, 0));
	m_Priority[3]     = MAKELONG(GetRGB(31,31,31), GetRGB( 0, 0,31));
	m_Priority[4]     = MAKELONG(GetRGB(31,31,31), GetRGB(31, 0,31));
	m_Priority[5]     = MAKELONG(GetRGB(31,31,31), GetRGB( 0,31,31));
	m_Priority[6]     = MAKELONG(GetRGB(31,31,31), GetRGB(15,15,15));
	m_Priority[7]     = MAKELONG(GetRGB(31,31,31), GetRGB( 8, 8, 8));
	m_pNoAlarmLabel   = NULL;
	m_pFormat         = New CDispFormatTimeDate;
	m_Ctx.m_uSeq      = 0;
	m_uIndex          = 0;
	m_uCount          = 0;
	m_uTicks          = 0;
	m_pAlarms         = CCommsSystem::m_pThis->m_pEvents;
	m_pHead           = NULL;
	m_EnableBlink     = 1;
	m_pBlinkRate      = NULL;
	m_pAlarmTransition= NULL;
	}

// Destructor

CPrimLegacyAlarmTicker::~CPrimLegacyAlarmTicker(void)
{
	FreeAlarmList();

	delete m_pEdge;

	delete m_pFormat;
	
	delete m_pNoAlarmLabel;

	delete m_pBlinkRate;

	delete m_pAlarmTransition;
	}

// Initialization

void CPrimLegacyAlarmTicker::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyAlarmTicker", pData);
	
	CPrim::Load(pData);

	m_pEdge->Load(pData);

	m_Font        = GetWord(pData);

	m_IncCount    = GetByte(pData);

	m_IncTime     = GetByte(pData);
	
	m_NoAlarm     = GetLong(pData);
	
	m_Accept      = GetLong(pData);

	m_AcceptBlink = GetByte(pData);

	if( (m_UsePriority = GetByte(pData)) ) {

		for( UINT n = 0; n < elements(m_Priority); n ++ ) {

			m_Priority[n] = GetLong(pData);
			}
		}
	else	
		m_Alarm = GetLong(pData);

	m_UseNoAlarmLabel = GetByte(pData);

	GetCoded(pData, m_pNoAlarmLabel);

	pData += 1;

	m_pFormat->Load(pData);
	
	m_Margin.x1 = GetWord(pData);
	m_Margin.y1 = GetWord(pData);
	m_Margin.x2 = GetWord(pData);
	m_Margin.y2 = GetWord(pData);

	GetCoded(pData, m_pBlinkRate);
	
	GetCoded(pData, m_pAlarmTransition);
	
	m_EnableBlink  = GetByte(pData);
	}

// Overridables

void CPrimLegacyAlarmTicker::SetScan(UINT Code)
{
	m_pEdge->SetScan(Code);

	SetItemScan(m_pNoAlarmLabel,   Code);

	m_pFormat->SetScan(Code);
	
	CPrim::SetScan(Code);
	}

void CPrimLegacyAlarmTicker::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrim::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			if( Ctx.m_uSeq != m_Ctx.m_uSeq ) {

				CAutoGuard Guard;

				FreeAlarmList();

				m_uCount = m_pAlarms->GetActiveAlarms();

				m_pHead  = NULL;

				m_pAlarms->ReadAlarmList(m_pHead, 0, m_uCount, FALSE);

				m_pDraw  = m_pHead;

				m_uState = 0;

				m_uTicks = GetTickCount();
				}
			
			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_uCount ) {
			
			UINT uAlarmTime      = 0;

			UINT uTime           = 0;

			UINT alarmTransition = GetItemData(m_pAlarmTransition, C3INT(2));

			UINT uBlinkRate      = GetItemData(m_pBlinkRate, C3INT(500));

			UINT uTicks          = GetTickCount();

			if ( m_EnableBlink ) {
				
				uTime      = ToTicks(uBlinkRate);
				
				uAlarmTime = ToTicks(alarmTransition * 1000);
				}
			else {
				uTime      = ToTicks(alarmTransition * 1000);
				
				uAlarmTime = uTime;
				}
		
			if( uTicks - m_uTicks >= uAlarmTime ) {
				
				if( m_pDraw->m_pNext ) {

					m_uIndex = m_uIndex + 1;
					
					m_pDraw  = m_pDraw->m_pNext;
					}
				else {
					m_uIndex = 0;
					
					m_pDraw  = m_pHead;
					}

				m_uState = 0;
				
				m_uTicks = uTicks;
				}

			if( uTicks - m_uTicks >= m_uState * uTime ) {

				m_uState  = m_uState + 1;

				m_fChange = TRUE;
				}
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}
		}
	}

void CPrimLegacyAlarmTicker::DrawExec(IGDI *pGDI, IRegion *pErase)
{
	CPrim::DrawExec(pGDI, pErase);
	}

void CPrimLegacyAlarmTicker::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	SelectFont(pGDI, m_Font);

	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);
		}

	if( !m_uCount ) {
		
		DrawNoAlarms(pGDI, Rect);
		}
	else {
		if( m_uState & 1 ) {

			DrawAlarm(pGDI, Rect);
			}
		else {
			UINT Alarm;
			
			if( m_pDraw->m_State == alarmAccepted ) {

				if( m_AcceptBlink ) {

					Alarm = m_Accept;
					}
				else {

					DrawAlarm(pGDI, Rect);

					return;
					}
				}
			else {
				if( m_UsePriority ) {
					
					Alarm = m_Priority[min(m_pDraw->m_Level-1, elements(m_Priority))];
					}
				else 
					Alarm = m_Alarm;
				}

			pGDI->SetBrushFore(HIWORD(Alarm));

			pGDI->SelectBrush (brushFore);

			pGDI->FillRect(PassRect(Rect));
			}
		}	
	}

// Context Creation

void CPrimLegacyAlarmTicker::FindCtx(CCtx &Ctx)
{
	Ctx.m_uSeq = m_pAlarms->GetAlarmSequence();
	}

// Context Check

BOOL CPrimLegacyAlarmTicker::CCtx::operator == (CCtx const &That) const
{
	return m_uSeq  == That.m_uSeq;
	}

// List Management

void CPrimLegacyAlarmTicker::FreeAlarmList(void)
{
	while( m_pHead ) {

		CActiveAlarm *pInfo = m_pHead;

		m_pHead             = m_pHead->m_pNext;

		delete pInfo;
		}
	}

// Implementation

void CPrimLegacyAlarmTicker::DrawNoAlarms(IGDI *pGDI, R2 Rect)
{
	pGDI->SetBrushFore(HIWORD(m_NoAlarm));

	pGDI->SelectBrush (brushFore);

	pGDI->FillRect(PassRect(Rect));

	if( m_UseNoAlarmLabel ) {

		pGDI->SetTextTrans(modeOpaque);

		pGDI->SetTextFore (LOWORD(m_NoAlarm));

		pGDI->SetTextBack (HIWORD(m_NoAlarm));

		CUnicode Text = textHair + m_pNoAlarmLabel->GetText() + textHair;

		Rect.left    += m_Margin.left;

		Rect.right   -= m_Margin.right;
		
		Rect.top     += m_Margin.top;
		
		Rect.bottom  -= m_Margin.bottom;

		int      cy   = pGDI->GetTextHeight(Text);

		pGDI->TextOut (Rect.left, (Rect.bottom + Rect.top - cy) / 2, UniVisual(Text));
		}
	}

void CPrimLegacyAlarmTicker::DrawAlarm(IGDI *pGDI, R2 Rect)
{
	CUnicode Text;

	if( m_IncCount ) {

		char s[32];

		SPrintf(s, "%3.3u/%3.3u  ", m_uIndex + 1, m_uCount);

		Text = s;
		}

	if( m_IncTime ) {

		Text += m_pFormat->Format( m_pDraw->m_Time, typeInteger, fmtPad);
		
		Text += "  ";
		}

	if( TRUE ) {
		
		Text += UniVisual(m_pDraw->m_Text);
		}

	pGDI->SetTextTrans(modeOpaque);

	if( m_pDraw->m_State == alarmAccepted ) {

		pGDI->SetBrushFore(HIWORD(m_Accept));

		pGDI->SelectBrush (brushFore);

		pGDI->FillRect(PassRect(Rect));
		
		pGDI->SetTextFore (LOWORD(m_Accept));

		pGDI->SetTextBack (HIWORD(m_Accept));
		}
	else {
		UINT Alarm = m_UsePriority ? m_Priority[min(m_pDraw->m_Level-1, elements(m_Priority))] : m_Alarm;

		pGDI->SetBrushFore(HIWORD(Alarm));

		pGDI->SelectBrush (brushFore);

		pGDI->FillRect(PassRect(Rect));
		
		pGDI->SetTextFore (LOWORD(Alarm));

		pGDI->SetTextBack (HIWORD(Alarm));
		}

	Rect.left   += m_Margin.left;

	Rect.right  -= m_Margin.right;
	
	Rect.top    += m_Margin.top;
	
	Rect.bottom -= m_Margin.bottom;

	int      cy  = pGDI->GetTextHeight(Text);

	pGDI->TextOut (Rect.left, (Rect.bottom + Rect.top - cy) / 2, Text);
	}

// End of File
