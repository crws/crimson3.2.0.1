
#include "intern.hpp"

#include "PrimRubyTankFill.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Tank Fill
//

// Constants

#define INVALID -1E30f

// Static Data

UINT   CPrimRubyTankFill::m_ShadeMode = 0;

COLOR  CPrimRubyTankFill::m_ShadeCol3 = 0;

C3REAL CPrimRubyTankFill::m_ShadeData = 0;

// Constructor

CPrimRubyTankFill::CPrimRubyTankFill(void)
{
	m_Mode    = 0;

	m_pValue  = NULL;

	m_pMin    = NULL;

	m_pMax    = NULL;

	m_pColor3 = New CPrimColor(naFeature);
	}

// Destructor

CPrimRubyTankFill::~CPrimRubyTankFill(void)
{
	delete m_pValue;

	delete m_pMin;

	delete m_pMax;

	delete m_pColor3;
	}

// Initialization

void CPrimRubyTankFill::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubyTankFill", pData);

	DoLoad(pData);
	}

// Operations

void CPrimRubyTankFill::SetScan(UINT Code)
{
	SetItemScan(m_pValue,  Code);

	SetItemScan(m_pMin,    Code);
	
	SetItemScan(m_pMax,    Code);
	
	SetItemScan(m_pColor3, Code);

	CPrimRubyBrush::SetScan(Code);
	}

BOOL CPrimRubyTankFill::DrawPrep(IGDI *pGDI)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_Ctx = Ctx;

		CPrimRubyBrush::DrawPrep(pGDI);

		return TRUE;
		}

	return CPrimRubyBrush::DrawPrep(pGDI);
	}

// Drawing

BOOL CPrimRubyTankFill::Fill(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver)
{
	if( !IsNull() ) {

		CPrimRubyBrush::Fill(pGdi, list, fOver);

		if( PrepTankFill() ) {

			CRubyGdiLink link(pGdi);

			if( fOver )
				link.OutputShade(list, Shader, 0);
			else
				link.OutputShade(list, Shader);
			}

		return TRUE;
		}

	return FALSE;
	}

// Shader

BOOL CPrimRubyTankFill::Shader(IGDI *pGDI, int p, int c)
{
	static int nTrip = 0;

	static int nMode = 0;

	if( c ) {

		if( p == 0 ) {

			nTrip = Round(c * m_ShadeData);

			if( m_ShadeMode == 1 || m_ShadeMode == 4 ) {

				nTrip = c - nTrip;

				nMode = 0;
				}

			if( m_ShadeMode == 2 || m_ShadeMode == 3 ) {

				nMode = 1;
				}

			if( nTrip ) {

				if( nMode == 0 ) {

					pGDI->SetBrushStyle(brushFore);
		
					pGDI->SetBrushFore(m_ShadeCol3);
					}

				if( nMode == 1 ) {

					pGDI->SetBrushStyle(brushNull);
					}

				return TRUE;
				}
			}

		if( p >= nTrip ) {

			if( nMode < 2 ) {

				if( nMode == 0 ) {
	
					pGDI->SetBrushStyle(brushNull);
					}

				if( nMode == 1 ) {

					pGDI->SetBrushStyle(brushFore);

					pGDI->SetBrushFore(m_ShadeCol3);
					}

				nMode = 2;

				return TRUE;
				}
			}

		return FALSE;
		}

	if( m_ShadeMode == 1 || m_ShadeMode == 2 ) {

		return FALSE;
		}

	return TRUE;
	}

// Rounding

C3INT CPrimRubyTankFill::Round(C3REAL Data)
{
	return (Data >= 0) ? int(Data + 0.5) : int(Data - 0.5);
	}

// Implementation

void CPrimRubyTankFill::DoLoad(PCBYTE &pData)
{
	CPrimRubyBrush::DoLoad(pData);

	if( (m_Mode = GetByte(pData)) ) {

		GetCoded(pData, m_pValue);

		GetCoded(pData, m_pMin);

		GetCoded(pData, m_pMax);

		m_pColor3->Load(pData);
		}
	}

BOOL CPrimRubyTankFill::PrepTankFill(void)
{
	if( m_Mode ) {

		if( m_Ctx.m_Data != INVALID ) {

			m_ShadeData = m_Ctx.m_Data;
		
			m_ShadeMode = m_Mode;

			m_ShadeCol3 = m_Ctx.m_Color3;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRubyTankFill::IsValueAvail(void)
{
	return IsItemAvail(m_pValue) &&
	       IsItemAvail(m_pMin  ) &&
	       IsItemAvail(m_pMax  ) ;
	}

// Context Creation

void CPrimRubyTankFill::FindCtx(CCtx &Ctx)
{
	if( m_Mode ) {

		if( IsValueAvail() ) {

			C3REAL Min = GetItemData(m_pMin, C3REAL(  0));

			C3REAL Max = GetItemData(m_pMax, C3REAL(100));

			if( Min != Max ) {

				C3REAL Value = GetItemData(m_pValue, C3REAL(25));

				Ctx.m_Data   = (Value - Min) / (Max - Min);

				Ctx.m_Color3 = m_pColor3->GetColor();

				MakeMax(Ctx.m_Data, 0.0);

				MakeMin(Ctx.m_Data, 1.0);

				return;
				}
			}

		Ctx.m_Color3 = m_pColor3->GetColor();

		Ctx.m_Data   = INVALID;
		}
	else {
		Ctx.m_Color3 = 0;

		Ctx.m_Data   = INVALID;
		}
	}

// Context Check

BOOL CPrimRubyTankFill::CCtx::operator == (CCtx const &That) const
{
	return m_Color3 == That.m_Color3 &&
	       m_Data   == That.m_Data   ;
	}

// End of File
