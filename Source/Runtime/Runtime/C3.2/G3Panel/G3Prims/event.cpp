
#include "intern.hpp"

#include "uitask.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Event Map
//

// Constructor

CEventMap::CEventMap(void)
{
	m_uCount = 0;

	m_pEvent = NULL;
	}

// Destructor

CEventMap::~CEventMap(void)
{
	delete [] m_pEvent;
	}

// Initialization

void CEventMap::Load(PCBYTE &pData)
{
	ValidateLoad("CEventMap", pData);

	m_uCount = GetWord(pData);

	if( m_uCount ) {

		m_pEvent = New CEventEntry [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			m_pEvent[n].Load(pData);
			}
		}
	}

// Attributes

BOOL CEventMap::IsAvail(UINT uCode) const
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_pEvent[n].m_Code == uCode ) {

			return m_pEvent[n].IsAvail();
			}
		}

	return TRUE;
	}

// Operations

void CEventMap::SetScan(UINT Code)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		m_pEvent[n].SetScan(Code);
		}
	}

BOOL CEventMap::RunEvent(UINT uCode, UINT uTrans, BOOL fLocal)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CEventEntry &Event = m_pEvent[n];

		if( Event.m_Code == uCode ) {

			if( Event.IsEnabled() ) {

				if( uTrans == stateDown ) {

					if( Event.m_fTime ) {

						return FALSE;
						}

					BOOL fValue   = Event.RunEvent(uTrans, fLocal);

					Event.m_uTime = GetTickCount();

					return fValue;
					}

				if( uTrans == stateUp ) {

					if( SetDelayTimer(Event) ) {

						Event.m_fTime = TRUE;

						return TRUE;
						}
					}

				return Event.RunEvent(uTrans, fLocal);
				}

			return FALSE;
			}
		}

	return FALSE;
	}

// Implementation

BOOL CEventMap::SetDelayTimer(CEventEntry &Event)
{
	if( Event.m_Delay ) {

		UINT uStart = Event.m_uTime;

		UINT uTicks = ToTicks(Event.m_Delay);

		UINT uTime  = GetTickCount();

		UINT uGone  = uTime - uStart;

		if( uGone < uTicks ) {

			CUITask *pTask = CUISystem::m_pThis->m_pTask;

			if( pTask->SetDelayTimer(&Event, uTicks - uGone) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Event Entry
//

// Constructor

CEventEntry::CEventEntry(void)
{
	m_fTime = FALSE;
	}

// Destructor

CEventEntry::~CEventEntry(void)
{
	}

// Initialization

void CEventEntry::Load(PCBYTE &pData)
{
	ValidateLoad("CEventEntry", pData);

	CPrimAction::DoLoad(pData);

	m_Code = GetWord(pData);
	}

// End of File
