
#include "intern.hpp"

#include "PrimRubyGradButton.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyTankFill.hpp"

#include "PrimRubyPenEdge.hpp"

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graduated Button Primitive
//

// Constructor

CPrimRubyGradButton::CPrimRubyGradButton(void)
{
	m_pColor1  = New CPrimColor(naBack);

	m_pColor2  = New CPrimColor(naFeature);

	m_pEdge    = New CPrimRubyPenEdge;
	}

// Destructor

CPrimRubyGradButton::~CPrimRubyGradButton(void)
{
	delete m_pColor1;

	delete m_pColor2;

	delete m_pEdge;
	}

// Initialization

void CPrimRubyGradButton::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubyGradButton", pData);

	CPrimRubyWithText::Load(pData);

	m_fFast = GetByte(pData);

	m_pColor1->Load(pData);
	m_pColor2->Load(pData);
	m_pEdge  ->Load(pData);

	LoadList(pData, m_listFill);
	LoadList(pData, m_listEdge);
	LoadList(pData, m_listTrim);
	}

// Overridables

void CPrimRubyGradButton::SetScan(UINT Code)
{
	CPrimRubyWithText::SetScan(Code);

	m_pColor1->SetScan(Code);
	m_pColor2->SetScan(Code);
	m_pEdge  ->SetScan(Code);
	}

void CPrimRubyGradButton::MovePrim(int cx, int cy)
{
	CPrimRubyWithText::MovePrim(cx, cy);

	m_listFill.Translate(cx, cy, !m_fFast);
	
	m_listEdge.Translate(cx, cy, true);
	
	m_listTrim.Translate(cx, cy, true);
	}

void CPrimRubyGradButton::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRubyWithText::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {
		
		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		(m_fChange ? Erase : Trans).Append(m_bound);
		}
	}

void CPrimRubyGradButton::DrawPrim(IGDI *pGDI)
{
	CRubyPatternLib *pLib = CUISystem::m_pThis->m_pUI->m_pPatterns;

	PSHADER pShader = pLib->SetGdi( pGDI,
					17,
					IsPressed() ? m_Ctx.m_Color2 : m_Ctx.m_Color1,
					IsPressed() ? m_Ctx.m_Color1 : m_Ctx.m_Color2
					);

	CRubyGdiLink link(pGDI);

	if( !m_fFast ) {

		link.OutputShade(m_listFill, pShader, 0);
		}
	else
		link.OutputShade(m_listFill, pShader);

	m_pEdge->Fill(pGDI, m_listEdge, TRUE);

	m_pEdge->Trim(pGDI, m_listTrim, TRUE);

	CPrimRubyWithText::DrawPrim(pGDI);
	}

void CPrimRubyGradButton::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_bound));
	}

// Context Creation

void CPrimRubyGradButton::FindCtx(CCtx &Ctx)
{
	Ctx.m_Color1 = m_pColor1->GetColor();

	Ctx.m_Color2 = m_pColor2->GetColor();
	}

// Context Check

BOOL CPrimRubyGradButton::CCtx::operator == (CCtx const &That) const
{
	return m_Color1 == That.m_Color1 &&
	       m_Color2 == That.m_Color2 ;
	}

// End of File
