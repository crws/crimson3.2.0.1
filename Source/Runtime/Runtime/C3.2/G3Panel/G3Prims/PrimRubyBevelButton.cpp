
#include "intern.hpp"

#include "PrimRubyBevelButton.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyBrush.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Bevel Primitives
//

// Constructor

CPrimRubyBevelButton::CPrimRubyBevelButton(void)
{
	}

// Initialization

void CPrimRubyBevelButton::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubyBevelButton", pData);

	CPrimRubyBevelBase::Load(pData);
	}

// Overridables

void CPrimRubyBevelButton::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	m_Style = IsPressed();

	CPrimRubyBevelBase::DrawPrep(pGDI, Erase, Trans);
	}

void CPrimRubyBevelButton::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_bound));
	}

// End of File
