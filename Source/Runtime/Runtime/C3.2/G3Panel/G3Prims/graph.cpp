
#include "intern.hpp"

#include "graph.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Bar Graph
//

// Constructor

CPrimLegacyBarGraph::CPrimLegacyBarGraph(void)
{
	m_pValue   = NULL;

	m_pCount   = NULL;

	m_pMin     = NULL;
	
	m_pMax     = NULL;

	m_pFill	   = New CPrimBrush;
	}

// Destructor

CPrimLegacyBarGraph::~CPrimLegacyBarGraph(void)
{
	delete m_pValue;

	delete m_pCount;

	delete m_pMin;
	
	delete m_pMax;

	delete m_pFill;
	
	FreeCtx(m_Ctx);
	}

// Initialization

void CPrimLegacyBarGraph::Load(PCBYTE &pData)
{
	CPrimLegacyFigure::Load(pData);
	
	GetCoded(pData, m_pValue);

	GetCoded(pData, m_pCount);

	GetCoded(pData, m_pMin);

	GetCoded(pData, m_pMax);

	m_pFill->Load(pData);

	m_Ctx.m_pVal = NULL;
	}

// Overidables

void CPrimLegacyBarGraph::SetScan(UINT Code)
{
	SetItemScan(m_pValue, Code);

	SetItemScan(m_pCount,  Code);

	SetItemScan(m_pMin,   Code);
	SetItemScan(m_pMax,   Code);

	m_pFill->SetScan(Code);

	CPrimLegacyFigure::SetScan(Code);
	}

void CPrimLegacyBarGraph::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimLegacyFigure::DrawPrep(pGDI, Erase, Trans);
	
	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			FreeCtx(m_Ctx);

			m_Ctx = Ctx;
			
			m_fChange = TRUE;			
			}		
		else
			FreeCtx(Ctx);

		if( m_pFill->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( m_pBack->IsNull() ) {

			if( m_fChange ) {

				Erase.Append(m_DrawRect);
				}
			}
		}
	}

// Context Creation

void CPrimLegacyBarGraph::FindCtx(CCtx &Ctx)
{
	if( m_pValue ) {

		if( (Ctx.m_fAvail = IsAvail()) ) {
			
			IDataServer *pData = CUISystem::m_pThis->GetDataServer();

			UINT  uCount = GetItemData(m_pCount, 0);
			
			C3REAL rMin  = m_pMin ? I2R(m_pMin->Execute(typeReal)) : I2R(0);

			C3REAL rMax  = m_pMax ? I2R(m_pMax->Execute(typeReal)) : I2R(100);
			
			Ctx.m_uCount = uCount;
			
			Ctx.m_rMin   = rMin;

			Ctx.m_rMax   = rMax;

			MakeCtx(Ctx);

			DWORD      v = m_pValue->Execute(typeLValue);

			CDataRef  &r = (CDataRef &) v;

			for( UINT n = 0; n < uCount; n++ ) {

				C3REAL rVal = I2R(pData->GetData(v, typeReal, getNone));

				MakeMax(rVal, rMin);

				MakeMin(rVal, rMax);

				Ctx.m_pVal[n] = rVal;

				r.x.m_Array++;
				}

			return;
			}
		}

	Ctx.m_fAvail = FALSE;

	Ctx.m_uCount = 0;

	Ctx.m_pVal   = NULL;

	Ctx.m_rMin = I2R(0);

	Ctx.m_rMax = I2R(100);
	}

void CPrimLegacyBarGraph::MakeCtx(CCtx &Ctx)
{
	Ctx.m_pVal = New C3REAL [ Ctx.m_uCount ];
	}

void CPrimLegacyBarGraph::FreeCtx(CCtx &Ctx)
{
	delete [] Ctx.m_pVal;
	}

// Context Check

BOOL CPrimLegacyBarGraph::CCtx::operator == (CCtx const &That) const
{
	return m_fAvail    == That.m_fAvail                                     &&
	       m_uCount    == That.m_uCount                                     &&
	       m_rMin      == That.m_rMin			                &&
	       m_rMax      == That.m_rMax			                &&
	       memcmp(m_pVal, That.m_pVal, sizeof(C3REAL) * That.m_uCount) == 0 ;;
	}

// Attributes

BOOL CPrimLegacyBarGraph::IsAvail(void) const
{
	if( !IsItemAvail(m_pCount) ) {
		
		return FALSE;
		}

	if( !IsItemAvail(m_pValue) ) {
		
		return FALSE;
		}

	if( !IsItemAvail(m_pMin) ) {
		
		return FALSE;
		}

	if( !IsItemAvail(m_pMax) ) {
		
		return FALSE;
		}

	return TRUE;
	}

// Implementation

void CPrimLegacyBarGraph::DrawBack(IGDI *pGDI, R2 &Rect)
{
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);
		}

	pGDI->ResetBrush();

	m_pBack->FillRect(pGDI, Rect);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Bar Graph
//

// Constructor

CPrimLegacyVertBarGraph::CPrimLegacyVertBarGraph(void)
{
	}

// Initialization

void CPrimLegacyVertBarGraph::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyVertBarGraph", pData);

	CPrimLegacyBarGraph::Load(pData);
	}

// Overidables

void CPrimLegacyVertBarGraph::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	DrawBack(pGDI, Rect);

	C3REAL rDelta = m_Ctx.m_rMax - m_Ctx.m_rMin;

	if( rDelta ) {

		int x1 = Rect.x1;
		int y1 = Rect.y1;
		int x2 = Rect.x2;
		int y2 = Rect.y2;

		for( UINT n = 0; n < m_Ctx.m_uCount; n++ ) {

			int xa = x1 + (n+0) * (x2 - x1 + 1) / m_Ctx.m_uCount;

			int xb = x1 + (n+1) * (x2 - x1 + 1) / m_Ctx.m_uCount;

			int yp = y2 - int((m_Ctx.m_pVal[n] - m_Ctx.m_rMin) * (y2 - y1) / rDelta);

			R2 Fill;
			
			Fill.x1 = xa;
			Fill.y1 = yp;
			Fill.x2 = xb-1;
			Fill.y2 = y2;
			
			m_pFill->FillRect(pGDI, Fill);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Bar Graph
//

// Constructor

CPrimLegacyHorzBarGraph::CPrimLegacyHorzBarGraph(void)
{
	}

// Initialization

void CPrimLegacyHorzBarGraph::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyHorzBarGraph", pData);

	CPrimLegacyBarGraph::Load(pData);
	}

// Overidables

void CPrimLegacyHorzBarGraph::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	DrawBack(pGDI, Rect);
	
	C3REAL rDelta = m_Ctx.m_rMax - m_Ctx.m_rMin;

	if( rDelta ) {

		int x1 = Rect.x1;
		int y1 = Rect.y1;
		int x2 = Rect.x2;
		int y2 = Rect.y2;

		for( UINT n = 0; n < m_Ctx.m_uCount; n++ ) {

			int xp = x1 + int((m_Ctx.m_pVal[n] - m_Ctx.m_rMin) * (x2 - x1) / rDelta);

			int ya = y2 - (n+1) * (y2 - y1 + 1) / m_Ctx.m_uCount;

			int yb = y2 - (n+0) * (y2 - y1 + 1) / m_Ctx.m_uCount;

			R2 Fill;
			
			Fill.x1 = x1;
			Fill.y1 = ya+1;
			Fill.x2 = xp;
			Fill.y2 = yb;
			
			m_pFill->FillRect(pGDI, Fill);
			}
		}
	}

// End of File
