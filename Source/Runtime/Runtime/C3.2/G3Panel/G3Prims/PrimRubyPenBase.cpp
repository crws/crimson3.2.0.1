
#include "intern.hpp"

#include "PrimRubyPenBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Edge Pen
//

// Constructor

CPrimRubyPenBase::CPrimRubyPenBase(void)
{
	m_Width = 1;

	m_Edge  = CRubyStroker::edgeCenter;

	m_Join  = CRubyStroker::joinMiter;
	}

// Initialization

void CPrimRubyPenBase::Load(PCBYTE &pData)
{
	CPrimRubyBrush::DoLoad(pData);

	m_Width = GetByte(pData);
	m_Edge  = GetByte(pData);
	m_Join  = GetByte(pData);
	}

// Attributes

BOOL CPrimRubyPenBase::IsNull(void) const
{
	return m_Width == 0;
	}

int CPrimRubyPenBase::GetWidth(void) const
{
	return m_Width;
	}

int CPrimRubyPenBase::GetInnerWidth(void) const
{
	switch( m_Edge ) {

		case CRubyStroker::edgeCenter:
			
			return (m_Width + 1) / 2;

		case CRubyStroker::edgeInner:

			return m_Width;

		case CRubyStroker::edgeOuter:

			return 0;
		}

	return 0;
	}

int CPrimRubyPenBase::GetOuterWidth(void) const
{
	switch( m_Edge ) {

		case CRubyStroker::edgeCenter:
			
			return (m_Width + 1) / 2;

		case CRubyStroker::edgeOuter:

			return m_Width;

		case CRubyStroker::edgeInner:

			return 0;
		}

	return 0;
	}

// Stroking

BOOL CPrimRubyPenBase::StrokeExact(CRubyPath &output, CRubyPath const &figure)
{
	if( !IsNull() ) {

		CRubyStroker s;

		s.SetEdgeMode ((CRubyStroker::EdgeMode)  m_Edge);

		s.SetJoinStyle((CRubyStroker::JoinStyle) m_Join);

		s.StrokeLoop(output, figure, 0, m_Width);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRubyPenBase::StrokeEdge(CRubyPath &output, CRubyPath const &figure)
{
	if( !IsNull() ) {

		CRubyStroker s;

		if( m_Width == 1 ) {

			s.SetEdgeMode((CRubyStroker::edgeOuter));

			s.SetJoinStyle((CRubyStroker::joinBevel));
			}
		else {
			s.SetEdgeMode ((CRubyStroker::EdgeMode)  m_Edge);

			s.SetJoinStyle((CRubyStroker::JoinStyle) m_Join);
			}

		s.StrokeLoop(output, figure, 0, m_Width);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRubyPenBase::StrokeOpen(CRubyPath &output, CRubyPath const &figure)
{
	if( !IsNull() ) {

		CRubyStroker s;

		if( m_Width == 1 ) {

			s.SetEdgeMode ((CRubyStroker::edgeOuter));

			s.SetJoinStyle((CRubyStroker::joinBevel));
			}
		else {
			s.SetEdgeMode ((CRubyStroker::EdgeMode)  m_Edge);

			s.SetJoinStyle((CRubyStroker::JoinStyle) m_Join);
			}

		s.SetEndStyle ((CRubyStroker::endFlat));

		s.StrokeOpen(output, figure, 0, m_Width);
		
		return TRUE;
		}

	return FALSE;
	}

// Drawing

BOOL CPrimRubyPenBase::Fill(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver)
{
	if( !IsNull() ) {

		return CPrimRubyBrush::Fill(pGdi, list, fOver);
		}

	return FALSE;
	}

// End of File
