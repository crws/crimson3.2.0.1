
#include "intern.hpp"

#include "PrimRubyBevelBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyBrush.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby BevelBase Primitives
//

// Constructor

CPrimRubyBevelBase::CPrimRubyBevelBase(void)
{
	m_pFill   = New CPrimRubyBrush;

	m_pEdge   = New CPrimRubyPenEdge;

	m_pHilite = New CPrimColor(naFeature);

	m_pShadow = New CPrimColor(naBack);

	m_Style   = 0;
	}

// Destructor

CPrimRubyBevelBase::~CPrimRubyBevelBase(void)
{
	delete m_pFill;

	delete m_pEdge;

	delete m_pHilite;

	delete m_pShadow;
	}

// Initialization

void CPrimRubyBevelBase::Load(PCBYTE &pData)
{
	CPrimRubyWithText::Load(pData);

	m_Style = GetByte(pData);

	m_pFill  ->Load(pData);
	m_pEdge  ->Load(pData);
	m_pHilite->Load(pData);
	m_pShadow->Load(pData);

	LoadList(pData, m_listFill);
	LoadList(pData, m_listEdge);
	LoadList(pData, m_listHilite);
	LoadList(pData, m_listShadow);
	}

// Overridables

void CPrimRubyBevelBase::SetScan(UINT Code)
{
	CPrimRubyWithText::SetScan(Code);

	m_pFill  ->SetScan(Code);
	m_pEdge  ->SetScan(Code);
	m_pHilite->SetScan(Code);
	m_pShadow->SetScan(Code);
	}

void CPrimRubyBevelBase::MovePrim(int cx, int cy)
{
	CPrimRubyWithText::MovePrim(cx, cy);

	m_listFill  .Translate(cx, cy, true);
	
	m_listEdge  .Translate(cx, cy, true);
	
	m_listHilite.Translate(cx, cy, true);
	
	m_listShadow.Translate(cx, cy, true);
	}

void CPrimRubyBevelBase::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRubyWithText::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_pFill->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		(m_fChange ? Erase : Trans).Append(m_bound);
		}
	}

void CPrimRubyBevelBase::DrawPrim(IGDI *pGDI)
{
	CRubyGdiLink link(pGDI);

	if( m_Ctx.m_Style == 0 ) {

		link.OutputSolid(m_listHilite, m_Ctx.m_Hilite, 0);

		link.OutputSolid(m_listShadow, m_Ctx.m_Shadow, 0);
		}

	if( m_Ctx.m_Style == 1 ) {

		link.OutputSolid(m_listHilite, m_Ctx.m_Shadow, 0);

		link.OutputSolid(m_listShadow, m_Ctx.m_Hilite, 0);
		}

	m_pFill->Fill(pGDI, m_listFill, TRUE);

	m_pEdge->Fill(pGDI, m_listEdge, TRUE);

	CPrimRubyWithText::DrawPrim(pGDI);
	}

// Context Creation

void CPrimRubyBevelBase::FindCtx(CCtx &Ctx)
{
	Ctx.m_Style  = m_Style;

	Ctx.m_Hilite = m_pHilite->GetColor();

	Ctx.m_Shadow = m_pShadow->GetColor();
	}

// Context Check

BOOL CPrimRubyBevelBase::CCtx::operator == (CCtx const &That) const
{
	return m_Style  == That.m_Style  &&
	       m_Hilite == That.m_Hilite &&
	       m_Shadow == That.m_Shadow ;
	}

// End of File
