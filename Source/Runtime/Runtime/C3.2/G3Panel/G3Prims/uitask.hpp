
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UITASK_HPP

#define	INCLUDE_UITASK_HPP

/////////////////////////////////////////////////////////////////////////
//
// Other Header
//

#include "../g3comms/secure.hpp"

#include "../g3comms/lang.hpp"

/////////////////////////////////////////////////////////////////////////
//
// UI Task
//

class CUITask : public CCodedHost, 
		public ITaskEntry, 
		public IMsgSink, 
		public ISecureUI
{
	public:
		// Constructor
		SLOW CUITask(void);

		// Destructor
		SLOW ~CUITask(void);

		// IBase
		UINT Release(void);

		// ITaskEntry
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// IMsgSink
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Attributes
		C3INT GetBrightness(UINT uType) const;
		C3INT GetContrast(void) const;
		C3INT GetUpdateRate(void) const;
		C3INT GetDrawCount(void) const;
		BOOL  CanGotoPrevious(void) const;
		BOOL  CanGotoNext(void) const;

		// Operations
		void  SetBrightness(UINT uType, C3INT nData);
		void  SetContrast(C3INT nData);
		BOOL  GotoPage(UINT hPage);
		BOOL  ShowSimplePopup(UINT hPage);
		BOOL  ShowNestedPopup(UINT hPage);
		BOOL  ShowPopupMenu(UINT hPage);
		void  GotoPrevious(void);
		void  GotoNext(void);
		void  HidePopup(void);
		void  HideAllPopups(void);
		void  ForceRedraw(void);
		void  Shutdown(void);
		C3INT ExecModalPopup(UINT hPage);
		void  EndModal(C3INT Code);
		BOOL  SetDelayTimer(CEventEntry *pEvent, UINT uDelay);
		BOOL  SetDelayTimer(CPrim *pPrim, UINT uDelay);
		void  WaitUpdate(void);
		void  RegisterPrim(CPrim *pPrim);
		void  DeregisterPrim(CPrim *pPrim);
		void  QueueEvent(UINT uHandle, UINT uEvent, UINT uParam);

		// Status Hooks
		BOOL CanShowKeypad(void);
		BOOL IsRunningCode(void);
		UINT GetKeypadTimeout(void);

		// Keypad Hooks
		void KeypadPush(void);
		void KeypadPull(void);
		void KeypadShow(UINT uType, PCTXT pTitle);
		void KeypadSetText(PCTXT pText, UINT uCursor);
		BOOL KeypadEvent(CInput &i);

		// Drawing Hooks
		void SecStartDraw(void);
		void SecUpdate(void);
		void SecEndDraw(void);
		void SecRunEvents(void);

		// Widget Hooks
		void SuspendWidget(void);
		void RestoreWidget(void);

	protected:
		// Goto Verbs
		enum
		{
			gotoNone,
			gotoPage,
			gotoSimplePopup,
			gotoNestedPopup,
			gotoPopupMenu,
			gotoHidePopup,
			gotoHideAll,
			gotoBack,
			gotoForward,
			};

		// Goto Context
		struct CGoto
		{
			UINT	m_uVerb;
			UINT	m_hPage;
			BOOL	m_fLast;
			};

		// Page Context
		struct CPage
		{
			UINT	    m_hPage;
			CDispPage * m_pPage;
			BOOL	    m_fLast;
			BOOL        m_fMenu;
			CPrim	  * m_pFocus;
			UINT	    m_uFocus;
			R2	    m_Rect;
			};

		// Delay Timer
		struct CTimer
		{
			CEventEntry * m_pEvent;
			CPrim       * m_pPrim;
			UINT          m_uFire;
			CTimer      * m_pNext;
			CTimer      * m_pPrev;
			};

		// Primitive Event
		struct CEvent
		{
			UINT          m_uHandle;
			UINT          m_uEvent;
			UINT          m_uParam;
			CEvent      * m_pNext;
			CEvent      * m_pPrev;
			};

		// Data Members
		CSecurityManager * m_pSecure;
		IGDI             * m_pGDI;
		CEventMap        * m_pEvents;
		ITouchMap        * m_pTouch;
		IMutex		 * m_pLock;
		IEvent           * m_pDraw;
		R2		   m_Dirty;
		HTASK		   m_hTask;
		BOOL		   m_fInit;
		UINT		   m_hInit;
		CCodedItem	 * m_pOnStart;
		CCodedItem	 * m_pOnInit;
		CCodedItem	 * m_pOnUpdate;
		CCodedItem	 * m_pOnSecond;
		UINT	           m_PopAlignH;
		UINT	           m_PopAlignV;
		UINT		   m_PopKeypad;
		UINT		   m_GMC1;
		UINT		   m_uTimePad;
		UINT		   m_uTimeDisp;
		UINT               m_uPadSize;
		UINT               m_uPadLayout;
		BOOL		   m_fShowNext;
		BOOL		   m_fAutoEntry;
		CGoto		   m_Goto;
		UINT		   m_hPrev[50];
		UINT		   m_hNext[50];
		UINT		   m_uPrev;
		UINT		   m_uNext;
		CPage		   m_Pile[8];
		R2		   m_Clip[8];
		UINT		   m_uPtr;
		UINT		   m_uClip;
		UINT		   m_uPageTimeout;
		UINT		   m_uDispTimeout;
		UINT		   m_fDispOff;
		BOOL		   m_fModal;
		C3INT		   m_nModal;
		UINT		   m_hMaster;
		CDispPage	 * m_pMaster;
		UINT		   m_uDispCount;
		UINT		   m_uDispRate;
		UINT		   m_uTempRate;
		UINT		   m_uLastTick;
		BOOL		   m_fRunCode;
		BOOL		   m_fRedraw;
		BOOL		   m_fShutdown;
		UINT		   m_uKeypad;
		UINT		   m_uKeySave;
		IKeypad		 * m_pKeySave;
		BOOL		   m_fKeyLock;
		IKeypad		 * m_pKeypad;
		BOOL		   m_fKeyInit;
		R2		   m_KeyRect;
		CTimer	         * m_pHeadTimer;
		CTimer	         * m_pTailTimer;
		CEvent	         * m_pHeadEvent;
		CEvent	         * m_pTailEvent;
		IMutex           * m_pLockEvent;
		CPrim            * m_Prims[32];

		// Implementation
		void MainLoop(void);
		BOOL MainDraw(void);
		BOOL DrawPage(BOOL fPad);
		BOOL DrawPrep(BOOL fPad, IGDI *pGDI, IRegion *pErase);
		BOOL DrawExec(BOOL fPad, IGDI *pGDI, IRegion *pErase);
		void SetDirty(R2 const &Rect);
		void DirtyAll(void);
		BOOL IsClipped(R2 const &Rect, R2 const *pClip, UINT uClip);
		BOOL IsNullGoto(void);
		BOOL IsShowPopup(UINT uVerb);
		BOOL IsShowNested(UINT uVerb);
		BOOL IsHidePopup(UINT uVerb);
		BOOL IsValidPage(UINT hPage);
		void CheckGoto(BOOL fInit);
		BOOL HideTopPopup(void);
		BOOL TestAccess(UINT hPage);
		void LoadPage(UINT uSlot, UINT hPage, BOOL fLast, BOOL fWait, UINT uMove);
		void KillPage(CDispPage *pPage, BOOL fWait);
		void CheckTick(BOOL fTimer);
		void CheckInput(void);
		void FindTimingData(UINT &uCount, UINT &uTimer);
		UINT FocusMessage(UINT uMsg, UINT uParam);
		BOOL FocusKey(UINT uCode, UINT uTrans, BOOL fLocal);
		BOOL DefaultKey(UINT uCode, UINT uTrans, BOOL fLocal);
		BOOL RunKeyEvent(UINT uCode, UINT uTrans, BOOL fLocal);
		BOOL RunKeyEvent(CEventMap *pMap, UINT uCode, UINT uTrans, BOOL fLocal);
		BOOL IsKeyAvailable(UINT uCode, UINT uTrans);
		void RunAction(CCodedItem *pItem, BOOL fWait);
		void RunAction(CDispPage *pPage, UINT uEvent, BOOL fWait);
		BOOL FocusBlocked(BOOL fLocal);
		BOOL FindNewFocus(P2 const &Pos);
		BOOL HasFocus(void);
		void SetFocus(CPrim *pFocus, UINT uFocus);
		void KillFocus(BOOL fAbort);
		BOOL NextFocus(void);
		BOOL PrevFocus(void);
		void CheckFocus(void);
		void PushPrev(void);
		void PushPrev(UINT hPage);
		void PushNext(UINT hPage);
		BOOL PullPrev(UINT &hPage);
		BOOL PullNext(UINT &hPage);
		void ShowKeypad(void);
		void ShowKeypad(IKeypad *pKeypad, BOOL fCreate);
		BOOL HideKeypad(void);
		void TidyKeypad(void);
		void LoadTimeout(void);
		void CheckTimeout(void);
		void FireTimeout(void);
		BOOL ClearScreen(void);
		void ShowFault(void);
		void ShowMessage(PCTXT pText);
		void TextOut(int yp, PCTXT pText);
		void WaitRelease(UINT uType, UINT uCode);
		WORD GetWord(PCBYTE &pData);
		void CheckTimers(void);
		void WaitForTimers(void);
		void PumpEvents(void);
		void PurgeEvents(UINT uSlot);
		BOOL HasMenu(void);
		BOOL UseKeys(void);
	};

// End of File

#endif
