
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CAMERA_HPP
	
#define	INCLUDE_CAMERA_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "geom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Camera
//

class CPrimCamera : public CPrimGeom
{
	public:
		// Constructor
		CPrimCamera(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawExec(IGDI *pGDI, IRegion *pDirty);
		void DrawPrim(IGDI *pGDI);

		// Configuration Data
		UINT	     m_uPort;
		UINT	     m_uDevice;
		UINT         m_uScale;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			UINT   m_uFrame;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};
		
		// Context Data
		CCtx  m_Ctx;

		// Data
		BOOL		m_fInit;
		IProxyCamera  * m_pCamera;

		// Drawing
		void DrawBase(IGDI *pGDI, R2 &Rect);
		void DrawSeq (IGDI *pGDI, R2  Rect);

		// Scaling
		void ScaleImageNearest (PCDWORD pSource, int iWidth, int iHeight, float Scale, PDWORD pOutput);
		void ScaleImageBilinear(PCDWORD pSource, int iWidth, int iHeight, float Scale, PDWORD pOutput);

		// Context Handling
		void FindCtx(CCtx &Ctx);
		void ClearCtx(CCtx &Ctx);
	};

// End of File

#endif
