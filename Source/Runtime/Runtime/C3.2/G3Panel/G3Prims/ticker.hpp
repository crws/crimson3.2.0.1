
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TICKER_HPP
	
#define	INCLUDE_TICKER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "../g3comms/events.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyAlarmTicker;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Alarm Ticker
//

class CPrimLegacyAlarmTicker : public CPrim
{
	public:
		// Constructor
		CPrimLegacyAlarmTicker(void);

		// Destructor
		~CPrimLegacyAlarmTicker(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawExec(IGDI *pGDI, IRegion *pDirty);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		CPrimPen    * m_pEdge;
		UINT          m_Font;
		UINT	      m_NoAlarm;
		UINT	      m_Alarm;
		UINT	      m_Accept;
		UINT	      m_AcceptBlink;
		UINT	      m_UseNoAlarmLabel;
		UINT	      m_IncCount;
		UINT	      m_IncTime;
		UINT          m_UsePriority;
		UINT          m_Priority[8];
		CDispFormat * m_pFormat;
		CCodedText  * m_pNoAlarmLabel;
		R2            m_Margin;
		UINT          m_EnableBlink;
		CCodedItem  * m_pBlinkRate;
		CCodedItem  * m_pAlarmTransition;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			UINT	m_uSeq;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Data Members
		IAlarmStatus * m_pAlarms;
		CActiveAlarm * m_pHead;
		CActiveAlarm * m_pDraw;
		UINT           m_uIndex;
		UINT           m_uCount;
		UINT           m_uTicks;
		UINT           m_uState;

		// Context Creation
		void FindCtx(CCtx &Ctx);

		// List Management
		void FreeAlarmList(void);

		// Implementation
		void DrawNoAlarms(IGDI *pGDI, R2 Rect);
		void DrawAlarm   (IGDI *pGDI, R2 Rect);
	};

// End of File

#endif
