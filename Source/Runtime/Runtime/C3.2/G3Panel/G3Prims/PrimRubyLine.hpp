
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyLine_HPP
	
#define	INCLUDE_PrimRubyLine_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRuby.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPrimRubyPenLine;
class CPrimRubyPenEdge;
	
//////////////////////////////////////////////////////////////////////////
//
// Ruby Line Primitive
//

class CPrimRubyLine : public CPrimRuby
{
	public:
		// Constructor
		CPrimRubyLine(void);

		// Destructor
		~CPrimRubyLine(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		CPrimRubyPenLine * m_pLine;
		CPrimRubyPenEdge * m_pEdge;

	protected:
		// Data Members
		BOOL	     m_fFast;
		CRubyGdiList m_listLine;
		CRubyGdiList m_listEdge;
		CRubyGdiList m_listTrim;
	};

// End of File

#endif
