
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hpp"

#include "uitask.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// UI Task
//

// Constructor

CUITask::CUITask(void)
{
}

// Destructor

CUITask::~CUITask(void)
{
}

// IBase

UINT CUITask::Release(void)
{
	return 1;
}

// ITaskEntry

void CUITask::TaskInit(UINT uID)
{
	memset(m_Pile, 0, sizeof(m_Pile));

	m_hTask        = GetCurrentTask();

	m_pGDI	       = CUISystem::m_pThis->GetGDI();

	m_fInit        = TRUE;

	m_pSecure      = CUISystem::m_pThis->m_pSecure;
	m_hInit        = CUISystem::m_pThis->m_pUI->m_hPage;
	m_pEvents      = CUISystem::m_pThis->m_pUI->m_pEvents;
	m_pOnStart     = CUISystem::m_pThis->m_pUI->m_pOnStart;
	m_pOnInit      = CUISystem::m_pThis->m_pUI->m_pOnInit;
	m_pOnUpdate    = CUISystem::m_pThis->m_pUI->m_pOnUpdate;
	m_pOnSecond    = CUISystem::m_pThis->m_pUI->m_pOnSecond;
	m_uTimePad     = CUISystem::m_pThis->m_pUI->m_TimePad;
	m_uTimeDisp    = 0;
	m_fShowNext    = CUISystem::m_pThis->m_pUI->m_ShowNext;
	m_fAutoEntry   = CUISystem::m_pThis->m_pUI->m_AutoEntry;
	m_uPadSize     = CUISystem::m_pThis->m_pUI->m_PadSize;
	m_uPadLayout   = CUISystem::m_pThis->m_pUI->m_PadLayout;
	m_PopAlignH    = CUISystem::m_pThis->m_pUI->m_PopAlignH;
	m_PopAlignV    = CUISystem::m_pThis->m_pUI->m_PopAlignV;
	m_PopKeypad    = CUISystem::m_pThis->m_pUI->m_PopKeypad;
	m_GMC1         = CUISystem::m_pThis->m_pUI->m_GMC1;

	m_pTouch       = Create_TouchMap(m_pGDI);

	m_pDraw        = Create_AutoEvent();

	m_pLock	       = Create_Rutex();

	m_uPtr         = 0;
	m_uClip        = 0;
	m_uPageTimeout = 0;
	m_uDispTimeout = 0;
	m_fDispOff     = FALSE;
	m_fModal       = FALSE;
	m_nModal       = 0;
	m_hMaster      = 0;
	m_pMaster      = NULL;
	m_uDispCount   = 0;
	m_uDispRate    = 0;
	m_uTempRate    = 0;
	m_uLastTick    = 0;
	m_uPrev        = 0;
	m_uNext        = 0;
	m_uKeypad      = 0;
	m_uKeySave     = 0;
	m_pKeySave     = NULL;
	m_fKeyLock     = FALSE;
	m_pKeypad      = NULL;
	m_fKeyInit     = FALSE;
	m_fRunCode     = FALSE;
	m_fRedraw      = FALSE;
	m_fShutdown    = FALSE;
	m_pHeadTimer   = NULL;
	m_pTailTimer   = NULL;
	m_pHeadEvent   = NULL;
	m_pTailEvent   = NULL;

	if( TouchCheck() ) {

		TouchSetMap(m_pTouch->GetBuffer());
	}

	m_Goto.m_hPage = m_hInit;

	m_Goto.m_uVerb = gotoPage;

	m_Goto.m_fLast = FALSE;

	CUISystem::m_pThis->m_pUI->SetScan(scanTrue);

	CUISystem::m_pThis->m_pUI->ApplyMute();

	CUISystem::m_pThis->RegisterUITask();

	m_pSecure->RegisterUI(this);

	CPrim::ClearWidgetStack();

	memset(m_Prims, 0, sizeof(m_Prims));

	m_pLockEvent = Create_Mutex();;
}

void CUITask::TaskExec(UINT uID)
{
	DirtyAll();

	InputEmpty();

	RunAction(m_pOnStart, FALSE);

	ShowFault();

	CheckGoto(TRUE);

	CheckGoto(TRUE);

	for( ;;) {

		for( ;;) {

			MainLoop();

			if( m_Goto.m_uVerb || m_fShutdown ) {

				break;
			}
		}

		if( m_fShutdown ) {

			Sleep(FOREVER);
		}

		CheckGoto(FALSE);
	}
}

void CUITask::TaskStop(UINT uID)
{
}

void CUITask::TaskTerm(UINT uID)
{
	m_pSecure->ShutdownUI();

	if( m_pKeypad ) {

		m_pKeypad->Release();
	}

	for( UINT n = 0; n <= m_uPtr; n++ ) {

		delete m_Pile[n].m_pPage;
	}

	if( m_pTouch ) {

		if( TouchCheck() ) {

			TouchSetMap(NULL);
		}

		m_pTouch->Release();
	}

	AfxRelease(m_pLockEvent);

	AfxRelease(m_pDraw);

	AfxRelease(m_pLock);

	DispEnableBacklight(TRUE);

	CUISystem::m_pThis->m_pUI->CancelMute();
}

// IMsgSink

UINT CUITask::OnMessage(UINT uMsg, UINT uParam)
{
	BOOL fLocal   = HIWORD(uMsg);

	UINT uMessage = LOWORD(uMsg);

	if( m_fKeyLock ) {

		INPUT  dwData = 0;

		CInput &i     = (CInput &) dwData;

		if( uMessage == msgKeyDown ) {

			i.x.i.m_Type   = typeKey;

			i.x.i.m_State  = stateDown;

			i.x.i.m_Local  = fLocal;

			i.x.d.k.m_Code = uParam;

			InputStore(dwData);

			return TRUE;
		}

		if( uMessage == msgKeyUp ) {

			i.x.i.m_Type   = typeKey;

			i.x.i.m_State  = stateUp;

			i.x.i.m_Local  = fLocal;

			i.x.d.k.m_Code = uParam;

			InputStore(dwData);

			return TRUE;
		}

		return FALSE;
	}

	if( uMessage == msgKeyDown ) {

		if( uParam == 0x09 ) {

			return NextFocus();
		}

		if( uParam == 0x08 ) {

			return PrevFocus();
		}
	}

	return FocusMessage(uMessage, uParam);
}

// Attributes

C3INT CUITask::GetBrightness(UINT uType) const
{
	if( uType == 0 ) {

		return DispGetBrightness();
	}

	if( uType == 1 ) {

		return KeyGetLEDLevel();
	}

	return 0;
}

C3INT CUITask::GetContrast(void) const
{
	return DispGetContrast();
}

C3INT CUITask::GetUpdateRate(void) const
{
	return m_uDispRate;
}

C3INT CUITask::GetDrawCount(void) const
{
	return m_uDispCount;
}

BOOL CUITask::CanGotoPrevious(void) const
{
	return m_uPrev > 0;
}

BOOL CUITask::CanGotoNext(void) const
{
	return m_uNext > 0;
}

// Operations

void CUITask::SetBrightness(UINT uType, C3INT nData)
{
	if( uType == 0 ) {

		DispSetBrightness(nData, TRUE);
	}

	if( uType == 1 ) {

		KeySetLEDLevel(nData, TRUE);
	}
}

void CUITask::SetContrast(C3INT nData)
{
	DispSetContrast(nData, TRUE);
}

BOOL CUITask::GotoPage(UINT hPage)
{
	if( hPage ) {

		CAutoLock Lock(m_pLock);

		m_Goto.m_uVerb = gotoPage;

		m_Goto.m_hPage = hPage;

		m_Goto.m_fLast = FALSE;

		return TRUE;
	}

	return FALSE;
}

BOOL CUITask::ShowSimplePopup(UINT hPage)
{
	if( hPage ) {

		CAutoLock Lock(m_pLock);

		m_Goto.m_uVerb = gotoSimplePopup;

		m_Goto.m_hPage = hPage;

		m_Goto.m_fLast = FALSE;

		return TRUE;
	}

	return FALSE;
}

BOOL CUITask::ShowNestedPopup(UINT hPage)
{
	if( hPage ) {

		CAutoLock Lock(m_pLock);

		m_Goto.m_uVerb = gotoNestedPopup;

		m_Goto.m_hPage = hPage;

		m_Goto.m_fLast = FALSE;

		return TRUE;
	}

	return FALSE;
}

BOOL CUITask::ShowPopupMenu(UINT hPage)
{
	if( hPage ) {

		CAutoLock Lock(m_pLock);

		m_Goto.m_uVerb = gotoPopupMenu;

		m_Goto.m_hPage = hPage;

		m_Goto.m_fLast = FALSE;

		return TRUE;
	}

	return FALSE;
}

void CUITask::GotoPrevious(void)
{
	CAutoLock Lock(m_pLock);

	m_Goto.m_uVerb = gotoBack;

	m_Goto.m_fLast = TRUE;
}

void CUITask::GotoNext(void)
{
	CAutoLock Lock(m_pLock);

	m_Goto.m_uVerb = gotoForward;

	m_Goto.m_fLast = FALSE;
}

void CUITask::HidePopup(void)
{
	CAutoLock Lock(m_pLock);

	m_Goto.m_uVerb = gotoHidePopup;

	m_Goto.m_hPage = NULL;

	m_Goto.m_fLast = FALSE;
}

void CUITask::HideAllPopups(void)
{
	CAutoLock Lock(m_pLock);

	m_Goto.m_uVerb = gotoHideAll;

	m_Goto.m_hPage = NULL;

	m_Goto.m_fLast = FALSE;
}

void CUITask::ForceRedraw(void)
{
	m_fRedraw = TRUE;
}

void CUITask::Shutdown(void)
{
	m_fShutdown = TRUE;
}

C3INT CUITask::ExecModalPopup(UINT hPage)
{
	if( GetCurrentTask() == m_hTask ) {

		for( UINT n = 0; n <= m_uPtr; n++ ) {

			CPage &Page = m_Pile[n];

			if( hPage == Page.m_hPage ) {

				return -100;
			}
		}

		if( TestAccess(hPage) ) {

			CWidgetStack Stack;

			CPrim::SuspendWidgetStack(Stack);

			HideKeypad();

			KillFocus(TRUE);

			m_fModal = TRUE;

			LoadPage(++m_uPtr, hPage, FALSE, TRUE, 1);

			LoadTimeout();

			for( ;;) {

				if( !m_fModal ) {

					HideKeypad();

					KillFocus(TRUE);

					HideTopPopup();

					CPage &Page = m_Pile[m_uPtr];

					Page.m_pPage->LoadTouchMap(m_pGDI, m_pTouch);

					LoadTimeout();

					CPrim::RestoreWidgetStack(Stack);

					return m_nModal;
				}

				MainLoop();
			}
		}

		return -300;
	}

	return -200;
}

void CUITask::EndModal(C3INT Code)
{
	if( m_fModal ) {

		m_nModal = Code;

		m_fModal = FALSE;
	}
}

BOOL CUITask::SetDelayTimer(CEventEntry *pEvent, UINT uDelay)
{
	CTimer *pTimer   = New CTimer;

	pTimer->m_pEvent = pEvent;

	pTimer->m_pPrim  = NULL;

	pTimer->m_uFire  = GetTickCount() + uDelay;

	AfxListAppend(m_pHeadTimer,
		      m_pTailTimer,
		      pTimer,
		      m_pNext,
		      m_pPrev
	);

	return TRUE;
}

BOOL CUITask::SetDelayTimer(CPrim *pPrim, UINT uDelay)
{
	CTimer *pTimer   = New CTimer;

	pTimer->m_pEvent = NULL;

	pTimer->m_pPrim  = pPrim;

	pTimer->m_uFire  = GetTickCount() + uDelay;

	AfxListAppend(m_pHeadTimer,
		      m_pTailTimer,
		      pTimer,
		      m_pNext,
		      m_pPrev
	);

	return TRUE;
}

void CUITask::WaitUpdate(void)
{
	if( m_hTask == GetCurrentTask() ) {

		CPrim::SuspendWidgetStack();

		MainDraw();

		CPrim::RestoreWidgetStack();

		DispUpdate(m_pGDI->GetBuffer());
	}
	else {
		m_pDraw->Wait(FOREVER);

		m_pDraw->Wait(FOREVER);
	}
}

void CUITask::RegisterPrim(CPrim *pPrim)
{
	for( UINT n = 0; n < elements(m_Prims); n++ ) {

		if( m_Prims[n] == NULL ) {

			m_Prims[n] = pPrim;

			pPrim->SetHandle(n);

			break;
		}
	}
}

void CUITask::DeregisterPrim(CPrim *pPrim)
{
	UINT uSlot = pPrim->GetHandle();

	PurgeEvents(uSlot);

	m_Prims[uSlot] = NULL;
}

void CUITask::QueueEvent(UINT uHandle, UINT uEvent, UINT uParam)
{
	CEvent    *pEvent = New CEvent;

	pEvent->m_uHandle = uHandle;

	pEvent->m_uEvent  = uEvent;

	pEvent->m_uParam  = uParam;

	m_pLockEvent->Wait(FOREVER);

	AfxListAppend(m_pHeadEvent,
		      m_pTailEvent,
		      pEvent,
		      m_pNext,
		      m_pPrev
	);

	m_pLockEvent->Free();
}

// Status Hooks

BOOL CUITask::CanShowKeypad(void)
{
	return TRUE;
}

BOOL CUITask::IsRunningCode(void)
{
	return m_fRunCode;
}

UINT CUITask::GetKeypadTimeout(void)
{
	return m_uTimePad * 1000;
}

// Keypad Hooks

void CUITask::KeypadPush(void)
{
	m_fKeyLock = TRUE;

	if( m_pKeypad ) {

		m_pKeySave = m_pKeypad;

		m_uKeySave = m_uKeypad;

		TidyKeypad();
	}
}

void CUITask::KeypadPull(void)
{
	if( m_uKeypad ) {

		HideKeypad();
	}

	if( m_uKeySave ) {

		m_uKeypad = m_uKeySave;

		m_uKeySave = 0;

		ShowKeypad(m_pKeySave, FALSE);

		m_fKeyInit = TRUE;
	}

	m_fKeyLock = FALSE;
}

void CUITask::KeypadShow(UINT uType, PCTXT pTitle)
{
	if( m_uKeypad != uType ) {

		HideKeypad();

		m_uKeypad = uType;

		ShowKeypad();
	}

	m_pKeypad->SetPadHead(UniConvert(pTitle));
}

void CUITask::KeypadSetText(PCTXT pText, UINT uCursor)
{
	m_pKeypad->SetPadText(UniConvert(pText));

	m_pKeypad->SetPadCrsr(uCursor);
}

BOOL CUITask::KeypadEvent(CInput &i)
{
	if( i.x.i.m_Type == typeTouch ) {

		P2 Pos;

		Pos.x = i.x.d.t.m_XPos * 4;

		Pos.y = i.x.d.t.m_YPos * 4;

		UINT uMsg   = 0;

		UINT uParam = MAKELONG(Pos.x, Pos.y);

		BOOL fLocal = i.x.i.m_Local;

		switch( i.x.i.m_State ) {

			case stateDown:
				uMsg = msgTouchDown;
				break;

			case stateRepeat:
				uMsg = msgTouchRepeat;
				break;

			case stateUp:
				uMsg = msgTouchUp;
				break;
		}

		if( m_pKeypad->OnMessage(MAKELONG(uMsg, fLocal), uParam) ) {

			return TRUE;
		}
	}

	return FALSE;
}

// Drawing Hooks

void CUITask::SecStartDraw(void)
{
	if( m_fShutdown ) {

		Sleep(FOREVER);
	}

	m_pGDI->BufferClaim();
}

void CUITask::SecUpdate(void)
{
	g_pDbase->LockPendingItems(TRUE);

	m_fRunCode = TRUE;

	CPrim::SuspendWidgetStack();

	DrawPage(FALSE);

	CPrim::RestoreWidgetStack();

	m_fRunCode = FALSE;

	g_pDbase->LockPendingItems(FALSE);
}

void CUITask::SecEndDraw(void)
{
	m_pGDI->BufferFree();

	DispUpdate(m_pGDI->GetBuffer());
}

void CUITask::SecRunEvents(void)
{
	for( UINT n = 0; n <= m_uPtr; n++ ) {

		CPage &Page = m_Pile[n];

		RunAction(Page.m_pPage->m_pProps->m_pOnUpdate, FALSE);

		RunAction(Page.m_pPage, 2, FALSE);
	}

	RunAction(m_pOnUpdate, FALSE);

	CheckTick(FALSE);

	CheckTimers();

	PumpEvents();
}

// Widget Hooks

void CUITask::SuspendWidget(void)
{
	CPrim::SuspendWidgetStack();
}

void CUITask::RestoreWidget(void)
{
	CPrim::RestoreWidgetStack();
}

// Implementation

void CUITask::MainLoop(void)
{
	CheckFocus();

	BOOL fDraw = MainDraw();

	CheckTick(TRUE);

	CheckTimers();

	PumpEvents();

	if( fDraw ) {

		for( UINT n = 0; n <= m_uPtr; n++ ) {

			CPage &Page = m_Pile[n];

			RunAction(Page.m_pPage->m_pProps->m_pOnUpdate, FALSE);

			RunAction(Page.m_pPage, 2, FALSE);
		}

		if( TRUE ) {

			RunAction(m_pOnUpdate, FALSE);
		}

		DispUpdate(m_pGDI->GetBuffer());
	}

	CheckInput();

	m_uDispCount = (m_uDispCount + 1) % 24000;

	m_uTempRate  = m_uTempRate + 1;

	CheckTimeout();
}

BOOL CUITask::MainDraw(void)
{
	g_pDbase->LockPendingItems(TRUE);

	m_pGDI->BufferClaim();

	BOOL fCode = m_fRunCode;

	m_fRunCode = TRUE;

	BOOL fDraw = DrawPage(FALSE);

	m_fRunCode = fCode;

	m_pGDI->BufferFree();

	g_pDbase->LockPendingItems(FALSE);

	// TODO -- Add pulse to IEvent?

	m_pDraw->Set();

	m_pDraw->Clear();

	return fDraw;
}

BOOL CUITask::DrawPage(BOOL fPad)
{
	IRegion *pDirty = Create_StdRegion();

	BOOL     fDebug = FALSE;

	if( m_fRedraw ) {

		DirtyAll();

		m_fRedraw = FALSE;

		fDebug    = FALSE;
	}

	if( !IsRectEmpty(m_Dirty) ) {

		pDirty->AddRect(m_Dirty);

		SetRectEmpty(m_Dirty);

		fDebug = FALSE;
	}

	if( TRUE ) {

		DrawPrep(fPad, m_pGDI, pDirty);
	}

	if( !pDirty->IsEmpty() ) {

		COLOR Color;

		if( m_pMaster ) {

			Color = m_pMaster->GetBackColor();
		}
		else {
			CPage &Page = m_Pile[0];

			Color = Page.m_pPage->GetBackColor();
		}

		if( fDebug ) {

			static UINT k = 0;

			Color = GetRGB(8+k, 8+k, 8+k);

			k = (k+1) % 16;
		}

		m_pGDI->ResetBrush();

		m_pGDI->SetBrushFore(Color);

		pDirty->Fill(m_pGDI);
	}

	if( TRUE ) {

		DrawExec(fPad, m_pGDI, pDirty);
	}

	if( pDirty->IsEmpty() ) {

		pDirty->Release();

		g_pPxe->KickTimeout(1);

		return FALSE;
	}

	pDirty->Release();

	g_pPxe->KickTimeout(1);

	return TRUE;
}

BOOL CUITask::DrawPrep(BOOL fPad, IGDI *pGDI, IRegion *pErase)
{
	if( !fPad ) {

		if( m_pMaster ) {

			m_pMaster->DrawPrep(m_pGDI, pErase, m_Clip, m_uClip);
		}

		for( UINT n = 0; n <= m_uPtr; n++ ) {

			CPage &Page = m_Pile[n];

			Page.m_pPage->DrawPrep(m_pGDI, pErase, m_Clip + n, m_uClip - n);
		}
	}

	if( m_pKeypad ) {

		m_pKeypad->DrawPrep(m_pGDI, pErase);
	}

	return TRUE;
}

BOOL CUITask::DrawExec(BOOL fPad, IGDI *pGDI, IRegion *pDirty)
{
	if( !fPad ) {

		if( m_pMaster ) {

			m_pMaster->DrawExec(m_pGDI, pDirty, m_Clip, m_uClip);
		}

		for( UINT n = 0; n <= m_uPtr; n++ ) {

			CPage &Page = m_Pile[n];

			if( n > 0 ) {

				R2 Rect = Page.m_Rect;

				if( IsClipped(Rect, m_Clip + n, m_uClip - n) ) {

					continue;
				}

				R2 Edge = Rect;

				R2 Dirt;

				// REV3 -- Would be better to add a function to IRegion
				// to allow us to get the intersection of the current
				// region and a specified rectangle. Using the bounding
				// rectangle of the region often over-estimates...

				pDirty->GetRect(Dirt);

				IntersectRects(Edge, Edge, Dirt);

				if( !IsRectEmpty(Edge) ) {

					COLOR Color = Page.m_pPage->GetBackColor();

					m_pGDI->ResetBrush();

					R2 Line = Rect;

					R2 Fill = Rect;

					DeflateRect(Line, 1, 1);

					DeflateRect(Fill, 2, 2);

					IntersectRects(Line, Line, Dirt);

					IntersectRects(Fill, Fill, Dirt);

					m_pGDI->SetBrushFore(GetRGB(0x00, 0x00, 0x00));

					m_pGDI->FillRect(PassRect(Edge));

					m_pGDI->SetBrushFore(GetRGB(0xFF, 0xFF, 0xFF));

					m_pGDI->FillRect(PassRect(Line));

					m_pGDI->SetBrushFore(Color);

					m_pGDI->FillRect(PassRect(Fill));

					pDirty->AddRect(Edge);
				}
			}

			Page.m_pPage->DrawExec(m_pGDI, pDirty, m_Clip + n, m_uClip - n);
		}
	}

	if( m_pKeypad ) {

		m_pKeypad->DrawExec(m_pGDI, pDirty);
	}

	return TRUE;
}

BOOL CUITask::IsClipped(R2 const &Rect, R2 const *pClip, UINT uClip)
{
	for( UINT n = 0; n < uClip; n++ ) {

		if( RectInRect(pClip[n], Rect) ) {

			return TRUE;
		}
	}

	return FALSE;
}

void CUITask::SetDirty(R2 const &Rect)
{
	CombineRects(m_Dirty, m_Dirty, Rect);
}

void CUITask::DirtyAll(void)
{
	m_Dirty.x1 = 0;
	m_Dirty.y1 = 0;
	m_Dirty.x2 = m_pGDI->GetCx();
	m_Dirty.y2 = m_pGDI->GetCy();
}

BOOL CUITask::IsNullGoto(void)
{
	if( m_Goto.m_uVerb == gotoPage ) {

		CPage &Page = m_Pile[0];

		if( m_Goto.m_hPage == Page.m_hPage ) {

			if( m_uPtr ) {

				m_Goto.m_uVerb = gotoHideAll;

				return FALSE;
			}

			return TRUE;
		}

		return FALSE;
	}

	if( IsShowPopup(m_Goto.m_uVerb) ) {

		for( UINT n = 0; n <= m_uPtr; n++ ) {

			CPage &Page = m_Pile[n];

			if( m_Goto.m_hPage == Page.m_hPage ) {

				return TRUE;
			}
		}

		return FALSE;
	}

	if( IsHidePopup(m_Goto.m_uVerb) ) {

		if( !m_uPtr ) {

			return TRUE;
		}

		return FALSE;
	}

	if( m_Goto.m_uVerb == gotoBack ) {

		if( !m_uPrev ) {

			return TRUE;
		}

		return FALSE;
	}

	if( m_Goto.m_uVerb == gotoForward ) {

		if( !m_uNext ) {

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CUITask::IsShowPopup(UINT uVerb)
{
	switch( uVerb ) {

		case gotoSimplePopup:
		case gotoNestedPopup:
		case gotoPopupMenu:

			return TRUE;
	}

	return FALSE;
}

BOOL CUITask::IsShowNested(UINT uVerb)
{
	switch( uVerb ) {

		case gotoNestedPopup:

			return TRUE;
	}

	return FALSE;
}

BOOL CUITask::IsHidePopup(UINT uVerb)
{
	switch( uVerb ) {

		case gotoHidePopup:
		case gotoHideAll:

			return TRUE;
	}

	return FALSE;
}

BOOL CUITask::IsValidPage(UINT hPage)
{
	PCBYTE pData = PCBYTE(g_pDbase->LockItem(hPage));

	if( pData ) {

		if( GetWord(pData) == IDC_DISP_PAGE ) {

			if( GetWord(pData) == 0x1234 ) {

				g_pDbase->FreeItem(hPage);

				return TRUE;
			}
		}

		g_pDbase->FreeItem(hPage);
	}

	return FALSE;
}

void CUITask::CheckGoto(BOOL fInit)
{
	CAutoLock Lock(m_pLock);

	if( !IsNullGoto() ) {

		CGoto Goto     = m_Goto;

		m_Goto.m_uVerb = gotoNone;

		Lock.Free();

		if( IsShowPopup(Goto.m_uVerb) ) {

			// LATER -- Is this check compatible with was C2 does?

			if( !IsValidPage(m_Goto.m_hPage) ) {

				return;
			}

			if( TestAccess(Goto.m_hPage) ) {

				HideKeypad();

				WaitForTimers();

				KillFocus(TRUE);

				if( !IsShowNested(Goto.m_uVerb) ) {

					HideTopPopup();
				}

				UINT uMove = (Goto.m_uVerb == gotoPopupMenu) ? 2 : 1;

				LoadPage(++m_uPtr, Goto.m_hPage, Goto.m_fLast, TRUE, uMove);

				LoadTimeout();
			}

			return;
		}

		if( IsHidePopup(Goto.m_uVerb) ) {

			HideKeypad();

			WaitForTimers();

			KillFocus(TRUE);

			UINT uCount = 1;

			if( Goto.m_uVerb == gotoHideAll ) {

				uCount = m_uPtr;
			}

			while( uCount-- ) {

				HideTopPopup();
			}

			CPage &Page = m_Pile[m_uPtr];

			Page.m_pPage->LoadTouchMap(m_pGDI, m_pTouch);

			LoadTimeout();

			return;
		}

		UINT hPage = 0;

		BOOL fPush = FALSE;

		if( Goto.m_uVerb == gotoPage ) {

			if( !IsValidPage(m_Goto.m_hPage) ) {

				return;
			}

			hPage = Goto.m_hPage;

			fPush = !fInit;
		}

		if( Goto.m_uVerb == gotoBack ) {

			PullPrev(hPage);

			fPush = FALSE;
		}

		if( Goto.m_uVerb == gotoForward ) {

			PullNext(hPage);

			fPush = FALSE;
		}

		if( hPage ) {

			if( fInit || TestAccess(hPage) ) {

				if( fPush ) {

					PushPrev();
				}

				HideKeypad();

				WaitForTimers();

				KillFocus(TRUE);

				while( m_uPtr ) {

					CDispPage *pPage = m_Pile[m_uPtr--].m_pPage;

					KillPage(pPage, TRUE);

					m_uClip--;
				}

				ClearScreen();

				CDispPage *pPage = m_Pile[0].m_pPage;

				LoadPage(0, hPage, Goto.m_fLast, TRUE, 0);

				LoadTimeout();

				KillPage(pPage, TRUE);
			}
		}

		return;
	}

	m_Goto.m_uVerb = gotoNone;
}

BOOL CUITask::HideTopPopup(void)
{
	if( m_uPtr ) {

		CPage &Kill = m_Pile[m_uPtr];

		SetDirty(Kill.m_Rect);

		KillPage(Kill.m_pPage, TRUE);

		m_uClip -= 1;

		m_uPtr  -= 1;

		return TRUE;
	}

	return FALSE;
}

BOOL CUITask::TestAccess(UINT hPage)
{
	CDispPage *pPage = New CDispPage;

	PCBYTE pData = PCBYTE(g_pDbase->LockItem(hPage));

	if( pData ) {

		if( GetWord(pData) == IDC_DISP_PAGE ) {

			pPage->Fast(pData);

			g_pDbase->FreeItem(hPage);

			if( m_pSecure->AllowPageChange(pPage->m_pProps->m_pSec) ) {

				delete pPage;

				return TRUE;
			}
		}
		else {
			g_pDbase->FreeItem(hPage);
		}
	}

	delete pPage;

	return FALSE;
}

void CUITask::LoadPage(UINT uSlot, UINT hPage, BOOL fLast, BOOL fWait, UINT uMove)
{
	CAutoGuard Guard;

	CPage &Page = m_Pile[uSlot];

	Page.m_hPage  = hPage;

	Page.m_fLast  = fLast;

	Page.m_pFocus = NULL;

	Page.m_uFocus = 0;

	Page.m_pPage = New CDispPage;

	PCBYTE pData = PCBYTE(g_pDbase->LockItem(Page.m_hPage));

	WORD   Class = GetWord(pData);

	Page.m_pPage->Load(pData);

	g_pDbase->FreeItem(hPage);

	if( uMove == 1 ) {

		Page.m_pPage->FindPopup();

		CDispPageProps *pProps = Page.m_pPage->m_pProps;

		R2 Rect = Page.m_pPage->m_Rect;

		int ah = pProps->m_PopAlign ? pProps->m_PopAlignH : m_PopAlignH;

		int av = pProps->m_PopAlign ? pProps->m_PopAlignV : m_PopAlignV;

		int cx = Rect.x2 - Rect.x1;

		int cy = Rect.y2 - Rect.y1;

		int xp = ah * (m_pGDI->GetCx() - cx) / 4;

		int yp = av * (m_pGDI->GetCy() - cy) / 4;

		int dx = xp - Rect.x1;

		int dy = yp - Rect.y1;

		Page.m_pPage->MovePopup(dx, dy);
	}

	if( uMove == 2 ) {

		Page.m_pPage->FindPopup();

		Page.m_pPage->PopupMenu();
	}

	Page.m_fMenu = (uMove == 2);

	Page.m_Rect  = Page.m_pPage->m_Rect;

	if( m_fInit ) {

		Guard.Guard(FALSE);

		RunAction(m_pOnInit, fWait);

		m_fInit = FALSE;

		Guard.Guard(TRUE);
	}

	if( uSlot == 0 ) {

		UINT hMaster = Page.m_pPage->m_pProps->m_Master;

		if( m_hMaster != hMaster ) {

			CDispPage *pMaster = m_pMaster;

			if( (m_hMaster = hMaster) ) {

				m_pMaster = New CDispPage;

				PCBYTE pData = PCBYTE(g_pDbase->LockItem(hMaster));

				WORD   Class = GetWord(pData);

				m_pMaster->Load(pData);

				g_pDbase->FreeItem(hMaster);

				m_pMaster->SetScan(scanTrue);

				RunAction(m_pMaster->m_pProps->m_pOnSelect, fWait);
			}
			else
				m_pMaster = NULL;

			KillPage(pMaster, TRUE);
		}

		if( Page.m_pPage->m_pProps->m_NoBack ) {

			m_uPrev = 0;
		}

		m_pSecure->PageSelected(Page.m_pPage->m_pProps->m_pSec);

		Page.m_pPage->BuildFocusList(m_pMaster);
	}
	else {
		m_Clip[m_uClip] = Page.m_Rect;

		m_uClip         = m_uClip + 1;

		if( !m_fModal ) {

			if( m_Pile[0].m_pPage->m_pProps->m_PopMaster ) {

				Page.m_pPage->BuildFocusList(m_pMaster);
			}
			else
				Page.m_pPage->BuildFocusList(NULL);
		}
		else
			Page.m_pPage->BuildFocusList(NULL);
	}

	Page.m_pPage->LoadTouchMap(m_pGDI, m_pTouch);

	Page.m_pPage->SetScan(scanTrue);

	RunAction(Page.m_pPage->m_pProps->m_pOnSelect, fWait);

	RunAction(Page.m_pPage, 0, fWait);

	SetDirty(Page.m_Rect);
}

void CUITask::KillPage(CDispPage *pPage, BOOL fWait)
{
	if( pPage ) {

		CAutoPointer<CDispPage> Page(pPage);

		WaitForTimers();

		RunAction(pPage, 1, fWait);

		RunAction(pPage->m_pProps->m_pOnRemove, fWait);

		pPage->SetScan(scanFalse);
	}
}

void CUITask::CheckTick(BOOL fTimer)
{
	UINT uTick = GetNow();

	if( m_uLastTick != uTick ) {

		if( m_uLastTick ) {

			for( UINT n = 0; n <= m_uPtr; n++ ) {

				CPage &Page = m_Pile[n];

				RunAction(Page.m_pPage->m_pProps->m_pOnSecond, FALSE);

				RunAction(Page.m_pPage, 3, FALSE);
			}

			RunAction(m_pOnSecond, FALSE);

			if( fTimer ) {

				FocusMessage(msgOneSecond, 0);

				if( m_uPageTimeout && !--m_uPageTimeout ) {

					FireTimeout();
				}

				if( m_uDispTimeout && !--m_uDispTimeout ) {

					DispEnableBacklight(FALSE);

					m_fDispOff = TRUE;
				}
			}

			m_uDispRate = m_uTempRate;
		}

		m_uTempRate = 0;

		m_uLastTick = uTick;
	}
}

void CUITask::CheckInput(void)
{
	UINT uCount, uTimer;

	FindTimingData(uCount, uTimer);

	for( UINT n = 0; n < uCount; n++ ) {

		if( m_Goto.m_uVerb ) {

			break;
		}

		UINT  uMatch = 0;

		INPUT dwRead = InputRead(uTimer);

		if( dwRead ) {

			CInput &i    = (CInput &) dwRead;

			CPage  &Page = m_Pile[m_uPtr];

			if( Page.m_fMenu ) {

				TouchTranslate(i);
			}

			m_pSecure->ProcessEvent(dwRead);

			UINT uType  = i.x.i.m_Type;

			UINT uCode  = i.x.d.k.m_Code;

			UINT uTrans = i.x.i.m_State;

			BOOL fLocal = i.x.i.m_Local;

			if( m_fDispOff && uTrans == stateDown ) {

				WaitRelease(uType, uCode);

				DispEnableBacklight(TRUE);

				m_fDispOff = FALSE;

				LoadTimeout();

				break;
			}

			if( uType == typeKey ) {

				if( !FocusKey(uCode, uTrans, fLocal) ) {

					if( !RunKeyEvent(uCode, uTrans, fLocal) ) {

						if( !DefaultKey(uCode, uTrans, fLocal) ) {

							continue;
						}
					}
				}

				uMatch = 2;
			}

			if( uType == typeTouch ) {

				P2 Pos;

				Pos.x = i.x.d.t.m_XPos * 4;

				Pos.y = i.x.d.t.m_YPos * 4;

				UINT uMsg   = 0;

				UINT uParam = MAKELONG(Pos.x, Pos.y);

				switch( uTrans ) {

					case stateDown:
						uMsg = msgTouchDown;
						break;

					case stateRepeat:
						uMsg = msgTouchRepeat;
						break;

					case stateUp:
						uMsg = msgTouchUp;
						break;
				}

				if( m_pKeypad ) {

					if( m_pKeypad->OnMessage(MAKELONG(uMsg, fLocal), uParam) ) {

						uMatch = 2;
					}
					else
						uMatch = 1;
				}

				if( uMatch == 0 ) {

					if( uTrans == stateDown ) {

						if( FindNewFocus(Pos) ) {

							if( !FocusBlocked(fLocal) ) {

								UINT uMsg = msgTouchInit;

								FocusMessage(uMsg, MAKELONG(Pos.x, Pos.y));
							}

							uMatch = 2;
						}
					}
				}

				if( uMatch == 0 ) {

					CPage &Page = m_Pile[m_uPtr];

					if( Page.m_pFocus ) {

						if( Page.m_pFocus->HitTest(Pos) ) {

							if( FocusBlocked(fLocal) ) {

								continue;
							}

							if( uMsg ) {

								if( FocusMessage(uMsg, uParam) ) {

									uMatch = 2;
								}
								else
									uMatch = 1;
							}
						}
					}
				}
			}

			if( uMatch > 1 ) {

				LoadTimeout();

				break;
			}
		}
	}

	if( HasFocus() ) {

		UINT uKeypad = FocusMessage(msgTestKeypad, 0);

		if( m_uKeypad != uKeypad ) {

			if( m_uKeypad ) {

				HideKeypad();
			}

			if( (m_uKeypad = uKeypad) ) {

				ShowKeypad();

				m_fKeyInit = TRUE;
			}
			else
				KillFocus(FALSE);
		}

		if( m_uKeypad ) {

			if( m_fKeyInit ) {

				FocusMessage(msgInitKeypad, UINT(m_pKeypad));

				m_fKeyInit = FALSE;
			}

			FocusMessage(msgDrawKeypad, UINT(m_pKeypad));
		}
	}

	m_pSecure->ProcessEvent(0);

	ForceSleep(10);
}

void CUITask::FindTimingData(UINT &uCount, UINT &uTimer)
{
	UINT u = m_Pile[0].m_pPage->m_pProps->m_Update;

	// REV3 -- Zero should get database default.

	static UINT const c[] = { 4,  4,  2,  1 };

	static UINT const t[] = { 25, 25, 25, 10 };

	uCount = c[u];

	uTimer = t[u];
}

UINT CUITask::FocusMessage(UINT uMsg, UINT uParam)
{
	CPage &Page = m_Pile[m_uPtr];

	if( Page.m_pFocus ) {

		return Page.m_pFocus->SendMessage(uMsg, uParam);
	}

	return 0;
}

BOOL CUITask::FocusKey(UINT uCode, UINT uTrans, BOOL fLocal)
{
	if( m_pKeypad ) {

		if( uTrans == stateDown || uTrans == stateRepeat ) {

			if( m_pKeypad->OnMessage(MAKELONG(msgKeyDown, fLocal), uCode) ) {

				return TRUE;
			}
		}

		if( uTrans == stateUp ) {

			if( m_pKeypad->OnMessage(MAKELONG(msgKeyUp, fLocal), uCode) ) {

				return TRUE;
			}
		}
	}
	else {
		if( uTrans == stateDown || uTrans == stateRepeat ) {

			if( FocusMessage(msgKeyDown, uCode) ) {

				return TRUE;
			}
		}

		if( uTrans == stateUp ) {

			if( FocusMessage(msgKeyUp, uCode) ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CUITask::DefaultKey(UINT uCode, UINT uTrans, BOOL fLocal)
{
	if( uTrans == stateDown ) {

		CPage &Page = m_Pile[m_uPtr];

		if( uCode == VK_ENTER || isalnum(uCode) ) {

			if( isalnum(uCode) ) {

				CDispPageProps *pProps = Page.m_pPage->m_pProps;

				if( pProps->m_AutoEntry ) {

					if( pProps->m_AutoEntry == 1 ) {

						return FALSE;
					}
				}
				else {
					if( !m_fAutoEntry ) {

						return FALSE;
					}
				}
			}

			if( !Page.m_pFocus || !Page.m_pFocus->OnMessage(msgUsesKeypad, 0) ) {

				CPrim * pFocus = NULL;

				UINT    uFocus = NOTHING;

				if( !Page.m_pPage->NextFocus(pFocus, uFocus) ) {

					return FALSE;
				}

				SetFocus(pFocus, uFocus);
			}

			if( isalnum(uCode) ) {

				CInput Input;

				Input.x.i.m_Type   = typeKey;

				Input.x.i.m_State  = uTrans;

				Input.x.i.m_Local  = fLocal;

				Input.x.d.k.m_Code = uCode;

				InputStore(Input.m_Ref);
			}

			FocusMessage(msgShowKeypad, 0);

			m_fKeyInit = TRUE;

			return TRUE;
		}

		if( uCode == VK_NEXT ) {

			GotoPage(Page.m_pPage->m_pProps->m_PageNext);

			return TRUE;
		}

		if( uCode == VK_PREV ) {

			GotoPage(Page.m_pPage->m_pProps->m_PagePrev);

			return TRUE;
		}

		if( uCode == VK_EXIT ) {

			if( m_pKeypad ) {

				HideKeypad();

				KillFocus(TRUE);

				return TRUE;
			}

			GotoPage(Page.m_pPage->m_pProps->m_PageExit);

			return TRUE;
		}

		if( uCode == VK_MENU ) {

			GotoPage(m_hInit);

			return TRUE;
		}

		if( uCode == VK_MUTE ) {

			g_pPxe->SetSiren(FALSE);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CUITask::RunKeyEvent(UINT uCode, UINT uTrans, BOOL fLocal)
{
	if( IsKeyAvailable(uCode, uTrans) ) {

		for( INT n = m_uPtr; n >= 0; n-- ) {

			CPage &Page = m_Pile[n];

			if( RunKeyEvent(Page.m_pPage->m_pEvents, uCode, uTrans, fLocal) ) {

				return TRUE;
			}
		}

		if( m_pMaster ) {

			if( RunKeyEvent(m_pMaster->m_pEvents, uCode, uTrans, fLocal) ) {

				return TRUE;
			}
		}

		if( RunKeyEvent(m_pEvents, uCode, uTrans, fLocal) ) {

			return TRUE;
		}

		if( uTrans == stateDown ) {

			return RunKeyEvent(uCode, stateRepeat, fLocal);
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CUITask::RunKeyEvent(CEventMap *pMap, UINT uCode, UINT uTrans, BOOL fLocal)
{
	if( pMap->RunEvent(uCode, uTrans, fLocal) ) {

		if( uTrans == stateDown ) {

			pMap->RunEvent(uCode, stateRepeat, fLocal);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CUITask::IsKeyAvailable(UINT uCode, UINT uTrans)
{
	CPage &Page = m_Pile[0];

	if( Page.m_pPage->m_pEvents->IsAvail(uCode) ) {

		if( m_pEvents->IsAvail(uCode) ) {

			return TRUE;
		}
	}

	if( uTrans == stateDown ) {

		ShowMessage("NOT READY");

		WaitRelease(typeKey, uCode);
	}

	if( uTrans == stateUp ) {

		return TRUE;
	}

	return FALSE;
}

void CUITask::RunAction(CCodedItem *pItem, BOOL fWait)
{
	if( pItem ) {

		BOOL fShow = FALSE;

		SetTimer(10000);

		while( GetTimer() ) {

			if( pItem->IsAvail() ) {

				BOOL fCode = m_fRunCode;

				m_fRunCode = TRUE;

				pItem->Execute(typeVoid);

				m_fRunCode = fCode;

				return;
			}

			if( !fWait ) {

				return;
			}

			if( !fShow ) {

				ShowMessage("WORKING 1");

				fShow = TRUE;
			}

			Sleep(100);
		}

		ShowMessage("TIMEOUT");

		Beep(48, 500);

		Sleep(1000);
	}
}

void CUITask::RunAction(CDispPage *pPage, UINT uEvent, BOOL fWait)
{
	if( pPage ) {

		BOOL fShow = FALSE;

		SetTimer(10000);

		while( GetTimer() ) {

			if( pPage->IsEventReady(uEvent) ) {

				BOOL fCode = m_fRunCode;

				m_fRunCode = TRUE;

				pPage->FireEvent(uEvent);

				m_fRunCode = fCode;

				return;
			}

			if( !fWait ) {

				return;
			}

			if( !fShow ) {

				ShowMessage("WORKING 2");

				fShow = TRUE;
			}

			Sleep(100);
		}

		ShowMessage("TIMEOUT");

		Beep(48, 500);

		Sleep(1000);
	}
}

BOOL CUITask::FocusBlocked(BOOL fLocal)
{
	if( !fLocal ) {

		CPage &Page  = m_Pile[m_uPtr];

		if( Page.m_pFocus ) {

			return Page.m_pFocus->OnMessage(msgBlockRemote, 0);
		}
	}

	return FALSE;
}

BOOL CUITask::FindNewFocus(P2 const &Pos)
{
	CPage & Page  = m_Pile[m_uPtr];

	CPrim * pPrim = NULL;

	UINT    uPrim = 0;

	if( Page.m_pPage->FindFocus(pPrim, uPrim, Pos) ) {

		if( Page.m_pFocus != pPrim ) {

			SetFocus(pPrim, uPrim);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CUITask::HasFocus(void)
{
	CPage &Page = m_Pile[m_uPtr];

	return Page.m_pFocus ? TRUE : FALSE;
}

void CUITask::SetFocus(CPrim *pFocus, UINT uFocus)
{
	CPage &Page = m_Pile[m_uPtr];

	KillFocus(FALSE);

	Page.m_pFocus = pFocus;

	Page.m_uFocus = uFocus;

	Page.m_pFocus->SendMessage(msgSetFocus, TRUE);
}

void CUITask::KillFocus(BOOL fAbort)
{
	CPage &Page = m_Pile[m_uPtr];

	if( Page.m_pFocus ) {

		Page.m_pFocus->SendMessage(msgKillFocus, fAbort);

		Page.m_pFocus = NULL;
	}

	m_pSecure->FocusChanged();
}

BOOL CUITask::NextFocus(void)
{
	CPage & Page   = m_Pile[m_uPtr];

	UINT    uFocus = Page.m_uFocus;

	CPrim * pFocus = NULL;

	if( Page.m_pPage->NextFocus(pFocus, uFocus) ) {

		if( uFocus != Page.m_uFocus ) {

			if( FocusMessage(msgKeyDown, 0x0D) != editError ) {

				SetFocus(pFocus, uFocus);

				FocusMessage(msgShowKeypad, 0);

				m_fKeyInit = TRUE;

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CUITask::PrevFocus(void)
{
	CPage & Page   = m_Pile[m_uPtr];

	UINT    uFocus = Page.m_uFocus;

	CPrim * pFocus = NULL;

	if( Page.m_pPage->PrevFocus(pFocus, uFocus) ) {

		if( uFocus != Page.m_uFocus ) {

			if( FocusMessage(msgKeyDown, 0x0D) != editError ) {

				SetFocus(pFocus, uFocus);

				FocusMessage(msgShowKeypad, 0);

				m_fKeyInit = TRUE;

				return TRUE;
			}
		}
	}

	return FALSE;
}

void CUITask::CheckFocus(void)
{
	CPage &Page = m_Pile[m_uPtr];

	if( Page.m_pFocus ) {

		if( !Page.m_pFocus->IsVisible() ) {

			HideKeypad();

			KillFocus(TRUE);

			return;
		}

		if( !Page.m_pFocus->SendMessage(msgTakeFocus, 0) ) {

			HideKeypad();

			KillFocus(TRUE);

			return;
		}
	}
}

void CUITask::PushPrev(void)
{
	PushPrev(m_Pile[0].m_hPage);

	m_uNext = 0;
}

void CUITask::PushPrev(UINT hPage)
{
	if( m_uPrev < elements(m_hPrev) ) {

		m_hPrev[m_uPrev++] = hPage;

		return;
	}

	memmove(m_hPrev + 0,
		m_hPrev + 1,
		sizeof(m_hPrev) - sizeof(m_hPrev[0])
	);

	m_hPrev[m_uPrev - 1] = hPage;
}

void CUITask::PushNext(UINT hPage)
{
	if( m_uNext < elements(m_hNext) ) {

		m_hNext[m_uNext++] = hPage;

		return;
	}

	memmove(m_hNext + 0,
		m_hNext + 1,
		sizeof(m_hNext) - sizeof(m_hNext[0])
	);

	m_hNext[m_uNext - 1] = hPage;
}

BOOL CUITask::PullPrev(UINT &hPage)
{
	if( m_uPrev ) {

		PushNext(m_Pile[0].m_hPage);

		hPage = m_hPrev[--m_uPrev];

		return TRUE;
	}

	return FALSE;
}

BOOL CUITask::PullNext(UINT &hPage)
{
	if( m_uNext ) {

		PushPrev(m_Pile[0].m_hPage);

		hPage = m_hNext[--m_uNext];

		return TRUE;
	}

	return FALSE;
}

void CUITask::ShowKeypad(void)
{
	UINT uFlags = 0;

	switch( m_uPadSize ) {

		case 0:
			uFlags |= kpsNormal;

			break;

		case 1:
			uFlags |= kpsLarge;

			break;

		case 2:
			uFlags |= kpsLarger;

			break;

		case 3:
			uFlags |= kpsMaximum;

			break;
	}

	if( m_uPadLayout ) {

		uFlags |= ((m_uPadLayout << 28) & kpsLayout);
	}

	switch( CUISystem::m_pThis->m_pLang->GetEffectiveCode() ) {

		case C3L_SIMP_CHINESE:

			uFlags |= kpsPinYin;

			break;

		case C3L_KOREAN_FULL:

			uFlags |= kpsHangul;

			break;
	}

	if( !m_fShowNext ) {

		uFlags |= kpsHideNav;
	}

	IKeypad *pKeypad = Create_Keypad(m_pGDI, m_uKeypad | uFlags);

	ShowKeypad(pKeypad, TRUE);
}

void CUITask::ShowKeypad(IKeypad *pKeypad, BOOL fCreate)
{
	m_pKeypad = pKeypad;

	S2 size;

	m_pKeypad->GetPadSize(size);

	CPage &Page = m_Pile[m_uPtr];

	CDispPageProps *pProps = Page.m_pPage->m_pProps;

	m_KeyRect.x1 = 0;

	m_KeyRect.y1 = 0;

	m_KeyRect.x2 = m_KeyRect.x1 + size.cx;

	m_KeyRect.y2 = m_KeyRect.y1 + size.cy;

	int al = pProps->m_PopAlign ? pProps->m_PopKeypad : m_PopKeypad;

	int ah = pProps->m_PopKeypad ? pProps->m_PopAlignH : m_PopAlignH;

	int av = pProps->m_PopKeypad ? pProps->m_PopAlignV : m_PopAlignV;

	int mx = (m_pGDI->GetCx() - size.cx) / 4;

	int my = (m_pGDI->GetCy() - size.cy) / 4;

	int xp = al ? ah * mx : (mx * 2);

	int yp = al ? av * my : (my * 4) / 3;

	int dx = xp - m_KeyRect.x1;

	int dy = yp - m_KeyRect.y1;

	m_KeyRect.x1 += dx;

	m_KeyRect.x2 += dx;

	m_KeyRect.y1 += dy;

	m_KeyRect.y2 += dy;

	if( fCreate ) {

		m_pKeypad->Create(m_pGDI,
				  NULL,
				  m_KeyRect,
				  0,
				  0,
				  0,
				  NULL
		);

		m_pKeypad->SetPadHost(this);
	}

	if( m_pTouch ) {

		m_pKeypad->TouchMap(m_pTouch);
	}

	m_Clip[m_uClip] = m_KeyRect;

	m_uClip         = m_uClip + 1;

	LoadTimeout();
}

BOOL CUITask::HideKeypad(void)
{
	if( m_pKeypad ) {

		m_pKeypad->Release();

		TidyKeypad();

		if( !m_fKeyLock ) {

			m_pSecure->KeypadHidden();
		}

		return TRUE;
	}

	return FALSE;
}

void CUITask::TidyKeypad(void)
{
	m_pKeypad = NULL;

	m_uKeypad = 0;

	m_uClip   = m_uClip - 1;

	CPage &Page = m_Pile[m_uPtr];

	Page.m_pPage->LoadTouchMap(m_pGDI, m_pTouch);

	SetDirty(m_KeyRect);

	LoadTimeout();
}

void CUITask::LoadTimeout(void)
{
	if( m_pKeypad ) {

		m_uPageTimeout = m_uTimePad;

		m_uDispTimeout = m_uTimeDisp;
	}
	else {
		CPage &Page    = m_Pile[m_uPtr];

		m_uPageTimeout = Page.m_pPage->m_pProps->m_Timeout;

		m_uDispTimeout = m_uTimeDisp;
	}

	if( m_fDispOff ) {

		DispEnableBacklight(TRUE);

		m_fDispOff = TRUE;
	}
}

void CUITask::CheckTimeout(void)
{
	UINT uTimeDisp = GetItemData(CUISystem::m_pThis->m_pUI->m_pTimeDisp, 0);

	if( m_uTimeDisp != uTimeDisp ) {

		m_uTimeDisp  = uTimeDisp;

		LoadTimeout();
	}
}

void CUITask::FireTimeout(void)
{
	if( m_pKeypad ) {

		HideKeypad();

		KillFocus(TRUE);
	}
	else {
		CPage      &Page = m_Pile[m_uPtr];

		CCodedItem *pAct = Page.m_pPage->m_pProps->m_pOnTimeout;

		if( !pAct ) {

			if( m_uPtr ) {

				if( m_fModal ) {

					EndModal(-1);
				}
				else
					HidePopup();
			}
			else {
				UINT hPage = Page.m_pPage->m_pProps->m_PageExit;

				GotoPage(hPage);
			}

			return;
		}

		RunAction(pAct, FALSE);

		m_uPageTimeout = Page.m_pPage->m_pProps->m_Timeout;
	}
}

BOOL CUITask::ClearScreen(void)
{
	// REV3 -- This is only needed for pages that are going
	// to be slow to load, so is there some heuristic we can
	// use to decide whether to use it or not?

	if( FALSE ) {

		m_pGDI->BufferClaim();

		if( m_pMaster ) {

			m_pGDI->ClearScreen(m_pMaster->GetBackColor());
		}
		else {
			CDispPage *pPage = m_Pile[0].m_pPage;

			if( pPage ) {

				m_pGDI->ClearScreen(pPage->GetBackColor());
			}
			else
				m_pGDI->ClearScreen(0);
		}

		m_pGDI->BufferFree();

		DispUpdate(m_pGDI->GetBuffer());

		return TRUE;
	}

	return FALSE;
}

void CUITask::ShowFault(void)
{
	DWORD dwData[4];

	if( TrapGetData(1, dwData) && dwData[2] ) {

		char  s[64]  = { 0 };

		BYTE  bType  = LOBYTE(dwData[2] >> 18);

		DWORD dwAddr = dwData[3];

		if( !m_GMC1 ) {

			TrapAcknowledge();

			return;
		}

		sprintf(s,
			"%2.2X-%4.4X-%4.4X-3%3.3u",
			bType,
			HIWORD(dwAddr),
			LOWORD(dwAddr),
			C3_BUILD
		);

		int dx = m_pGDI->GetCx();

		int dy = m_pGDI->GetCy();

		m_pGDI->BufferClaim();

		m_pGDI->ClearScreen(0);

		m_pGDI->SelectBrush(brushBack);

		m_pGDI->SelectFont((dx == 240) ? fontSwiss0712 : fontSwiss1216);

		int xb = (dx == 240) ? 110 : 130;

		int x1 = dx / 2 - xb;

		int y1 = 2 * dy / 10 - 10;

		int x2 = dx / 2 + xb;

		int y2 = 2 * dy / 10 + 26;

		m_pGDI->SetBackColor(GetRGB(31, 0, 0));

		m_pGDI->SetForeColor(GetRGB(31, 31, 31));

		m_pGDI->FillRect(x1, y1, x2, y2);

		TextOut(2 * dy / 10, "SOFTWARE EXCEPTION");

		m_pGDI->SetBackColor(0);

		TextOut(4 * dy / 10, "GMC");

		TextOut(5 * dy / 10, s);

		if( !UseKeys() ) {

			if( m_pTouch ) {

				m_pTouch->FillRect(0, 0, dx, dy);
			}

			TextOut(7 * dy / 10, "Touch screen to continue");
		}
		else {
			TextOut(7 * dy / 10, "Power Cycle to continue");

			TrapAcknowledge();
		}

		m_pGDI->BufferFree();

		DispUpdate(m_pGDI->GetBuffer());

		for( ;;) {

			INPUT dwRead = InputRead(FOREVER);

			if( dwRead ) {

				CInput &i = (CInput &) dwRead;

				if( !UseKeys() ) {

					if( i.x.i.m_Type == typeTouch ) {

						if( i.x.i.m_State == stateUp ) {

							if( m_pTouch ) {

								m_pTouch->ClearMap();
							}

							TrapAcknowledge();

							break;
						}
					}
				}
				else {
					if( i.x.i.m_Type == typeKey ) {

						if( i.x.i.m_State == stateUp ) {

							if( i.x.d.k.m_Code == VK_MENU ) {

								TrapAcknowledge();

								break;
							}
						}
					}
				}
			}
		}
	}
}

void CUITask::ShowMessage(PCTXT pText)
{
	P2 p[8];

	int xp = m_pGDI->GetCx() / 2;

	int yp = m_pGDI->GetCy() / 2;

	int ra = 80;

	for( UINT n = 0; n < 8; n++ ) {

		double a = 22.5 + 45 * n;

		double r = a / 180 * 3.1415926;

		p[n].x = int(xp + ra * cos(r));

		p[n].y = int(yp + ra * sin(r));
	}

	m_pGDI->ResetAll();

	m_pGDI->SelectFont(fontHei16Bold);

	m_pGDI->SetTextTrans(modeTransparent);

	m_pGDI->SetPenCaps(capsBoth);

	m_pGDI->SetPenWidth(13);

	m_pGDI->SetPenFore(GetRGB(0, 0, 0));

	m_pGDI->DrawPolygon(p, 8, 0);

	m_pGDI->SetBrushFore(GetRGB(31, 0, 0));

	m_pGDI->FillPolygon(p, 8, 0);

	m_pGDI->SetPenWidth(9);

	m_pGDI->SetPenFore(GetRGB(31, 31, 31));

	m_pGDI->DrawPolygon(p, 8, 0);

	int cx = m_pGDI->GetTextWidth(pText);

	int cy = m_pGDI->GetTextHeight(pText);

	m_pGDI->TextOut(xp - cx / 2, yp - cy / 2, pText);

	DispUpdate(m_pGDI->GetBuffer());

	R2 Rect;

	Rect.x1 = xp - ra - 15;
	Rect.y1 = yp - ra - 15;
	Rect.x2 = xp + ra + 15;
	Rect.y2 = yp + ra + 15;

	SetDirty(Rect);
}

void CUITask::TextOut(int yp, PCTXT pText)
{
	int dx = m_pGDI->GetCx();

	int cx = m_pGDI->GetTextWidth(pText);

	int xp = (dx - cx) / 2;

	m_pGDI->TextOut(xp, yp, pText);
}

void CUITask::WaitRelease(UINT uType, UINT uCode)
{
	for( ;;) {

		INPUT dwRead = InputRead(FOREVER);

		if( dwRead ) {

			CInput &i = (CInput &) dwRead;

			if( i.x.i.m_Type == uType ) {

				if( uType == typeTouch || i.x.d.k.m_Code == uCode ) {

					if( i.x.i.m_State == stateUp ) {

						break;
					}
				}
			}
		}
	}
}

WORD CUITask::GetWord(PCBYTE &pData)
{
	Item_Align2(pData);

	WORD x = MotorToHost(*PCWORD(pData));

	pData += sizeof(x);

	return x;
}

void CUITask::CheckTimers(void)
{
	CTimer *pScan = m_pHeadTimer;

	UINT    uTime = GetTickCount();

	while( pScan ) {

		CTimer *pNext = pScan->m_pNext;

		if( INT(pScan->m_uFire - uTime) <= 0 ) {

			AfxListRemove(m_pHeadTimer,
				      m_pTailTimer,
				      pScan,
				      m_pNext,
				      m_pPrev
			);

			if( pScan->m_pEvent ) {

				// REV3 -- Is this correct re fLocal?

				pScan->m_pEvent->m_fTime = FALSE;

				pScan->m_pEvent->RunEvent(stateUp, TRUE);
			}

			if( pScan->m_pPrim ) {

				pScan->m_pPrim->SendMessage(msgTimer, 0);
			}

			delete pScan;
		}

		pScan = pNext;
	}
}

void CUITask::WaitForTimers(void)
{
	while( m_pHeadTimer ) {

		CheckTimers();

		Sleep(50);
	}
}

void CUITask::PurgeEvents(UINT uSlot)
{
	CEvent *pScan = m_pHeadEvent;

	while( pScan ) {

		CEvent *pNext = pScan->m_pNext;

		if( uSlot == pScan->m_uHandle ) {

			m_pLockEvent->Wait(FOREVER);

			AfxListRemove(m_pHeadEvent,
				      m_pTailEvent,
				      pScan,
				      m_pNext,
				      m_pPrev
			);

			m_pLockEvent->Free();

			delete pScan;
		}

		pScan = pNext;
	}
}

void CUITask::PumpEvents(void)
{
	CEvent *pScan = m_pHeadEvent;

	while( pScan ) {

		CEvent *pNext = pScan->m_pNext;

		if( pScan->m_uHandle < elements(m_Prims) ) {

			CPrim  *pPrim = m_Prims[pScan->m_uHandle];

			if( pPrim ) {

				pPrim->SendMessage(pScan->m_uEvent, pScan->m_uParam);
			}
		}
		else
			AfxTrace("*** Handle not found %8.8X handle %X\n", pScan, pScan->m_uHandle);

		m_pLockEvent->Wait(FOREVER);

		AfxListRemove(m_pHeadEvent,
			      m_pTailEvent,
			      pScan,
			      m_pNext,
			      m_pPrev
		);

		m_pLockEvent->Free();

		delete pScan;

		pScan = pNext;
	}
}

BOOL CUITask::UseKeys(void)
{
	return FALSE;
}

// End of File
