
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyWithText_HPP
	
#define	INCLUDE_PrimRubyWithText_HPP

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive with Text
//

class CPrimRubyWithText : public CPrimWithText
{
	public:
		// Constructor
		CPrimRubyWithText(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		R2   GetBackRect(void);
		void MovePrim(int cx, int cy);

	protected:
		// Data Members
		R2 m_bound;

		// Implementation
		BOOL LoadList(PCBYTE &pData, CRubyGdiList &list);
	};

// End of File

#endif
