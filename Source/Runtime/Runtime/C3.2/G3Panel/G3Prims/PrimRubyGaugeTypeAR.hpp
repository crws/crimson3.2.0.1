
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeTypeAR_HPP
	
#define	INCLUDE_PrimRubyGaugeTypeAR_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeTypeA.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Radial Gauge Primitive
//

class CPrimRubyGaugeTypeAR : public CPrimRubyGaugeTypeA
{
	public:
		// Constructor
		CPrimRubyGaugeTypeAR(void);

		// Destructor
		~CPrimRubyGaugeTypeAR(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawExec(IGDI *pGDI, IRegion *pDirty);
		void DrawPrim(IGDI *pGDI);

		// Drawing
		void TestPointer(void);
		void DrawPointer(IGDI *pGDI);

		// Data Members
		UINT  m_PointStyle;
		COLOR m_Color[2];

	protected:
		// Data Members
		int	      m_cx;
		int	      m_cy;
		IGdi        * m_pGdi;
		PDWORD        m_pData;
		PDWORD        m_pCopy;
		R2	      m_rectPivot;
		CRubyPoint    m_pointPivot;
		number	      m_radiusPivot;
		number	      m_angleMin;
		number	      m_angleMax;
		number	      m_lineFact;
		CRubyVector   m_radiusBug;
		CRubyVector   m_radiusOuter;
		CRubyVector   m_radiusMajor;
		CRubyVector   m_radiusMinor;
		CRubyVector   m_radiusBand;
		CRubyVector   m_radiusPoint;
		CRubyVector   m_radiusSweep;
		CRubyPath     m_pathPoint;
		CRubyGdiList  m_listPoint;
		CRubyGdiList  m_listPivot1;
		CRubyGdiList  m_listPivot2;

		// Context Record
		struct CCtx
		{
			// Data Members
			number m_Value;
			COLOR  m_PointColor;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Path Management
		void InitPaths(void);
		void MakePaths(void);
		void MakeLists(void);

		// Scale Building
		void MakeScaleEtc(void);

		// Scaling
		number GetAngle(number v);

		// Path Preparation
		void PrepareTicks(void);
		void PreparePoint(void);
		BOOL PrepareBand(CRubyPath &path, BOOL fShow, number minValue, number maxValue);
		BOOL PrepareBug(CRubyPath &path, BOOL fShow, number bugValue);

		// Color Schemes
		COLOR GetColor(UINT n);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

// End of File

#endif
