
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TREND_HPP
	
#define	INCLUDE_TREND_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "viewer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimTrendViewer;

//////////////////////////////////////////////////////////////////////////
//
// Point Array Type Defintion
//

typedef CArray<P2> CPointArray;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Trend Viewer
//

class CPrimTrendViewer : public CPrimViewer
{
	public:
		// Constructor
		CPrimTrendViewer(void);

		// Destructor
		~CPrimTrendViewer(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);

		// Data Members
		UINT	      m_Log;
		UINT	      m_Width;
		CCodedItem  * m_pPenMask;
		CCodedItem  * m_pFillMask;
		UINT	      m_PenWeight;
		UINT          m_ShowData;
		UINT          m_ShowCursor;
		UINT          m_DataBox;
		BOOL          m_fUseFill;
		UINT	      m_FontTitle;
		UINT	      m_FontData;
		UINT	      m_GridTime;
		UINT	      m_GridMode;
		UINT	      m_GridMinor;
		UINT	      m_GridMajor;
		CCodedItem  * m_pGridMin;
		CCodedItem  * m_pGridMax;
		CCodedItem  * m_pRects;
		UINT	      m_Precise;
		CPrimColor  * m_pColTitle;
		CPrimColor  * m_pColLabel;
		CPrimColor  * m_pColData;
		CPrimColor  * m_pColMajor;
		CPrimColor  * m_pColMinor;
		CPrimColor  * m_pColCursor;
		CPrimColor  * m_pColPen[16];
		CPrimColor  * m_pColFill[16];
		CCodedText  * m_pBtnPgLeft;
		CCodedText  * m_pBtnLeft;
		CCodedText  * m_pBtnLive;
		CCodedText  * m_pBtnRight;
		CCodedText  * m_pBtnPgRight;
		CCodedText  * m_pBtnIn;
		CCodedText  * m_pBtnOut;
		CCodedText  * m_pBtnLoad;
		CDispFormat * m_pFormat;
		UINT	      m_fUseLoad;

	protected:
		// Buttons
		enum {
			btnPageLeft	= 0,
			btnStepLeft	= 1,
			btnLive		= 2,
			btnStepRight	= 3,
			btnPageRight	= 4,
			btnZoomIn	= 5,
			btnZoomOut	= 6,
			btnLoad		= 7,
			};

		// Context Record
		struct CCtx
		{
			// Data Members
			UINT   m_uScale;
			UINT   m_uRects;
			DWORD  m_dwCursor;
			DWORD  m_dwTime;
			DWORD  m_dwLeft;
			DWORD  m_dwWide;
			BOOL   m_fLive;
			COLOR  m_ColTitle;
			COLOR  m_ColLabel;
			COLOR  m_ColData;
			COLOR  m_ColMajor;
			COLOR  m_ColMinor;
			COLOR  m_ColCursor;
			C3REAL m_GridMin;
			C3REAL m_GridMax;
			DWORD  m_dwLineMask;
			DWORD  m_dwFillMask;
			BOOL   m_fMemory;
			BOOL   m_fLoad;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Scale Record
		struct CScale
		{
			DWORD m_dwWidth;
			DWORD m_dwStep0;
			DWORD m_dwStep1;
			};

		// Static Data
		static CScale const m_Scale[];

		// Source Items
		CDataLog * m_pLog;
		UINT       m_uTagCount;
		BOOL	   m_fTagData;
		PUINT      m_pTagType;
		PDWORD     m_pTagData;
		PDWORD     m_pTagMin;
		PDWORD     m_pTagMax;

		// Core Layout
		BOOL m_fInit;
		int  m_xm;
		int  m_x1;
		int  m_x2;
		int  m_y1;
		int  m_y2;

		// Data Layout
		P2  m_Inits[32];

		// Grid Layout
		int    m_yGap;
		double m_Limit;
		double m_GridSpan;
		double m_DrawStep;
		int    m_StepMin;
		int    m_StepMax;
		int    m_StepSpan;
		C3REAL m_DrawMin;
		C3REAL m_DrawMax;

		// Data Members
		CCtx  m_Ctx;
		UINT  m_uScale;
		BOOL  m_fLive;
		BOOL  m_fMemory;
		DWORD m_dwNow;
		DWORD m_dwTime;
		DWORD m_dwCursor;
		BOOL  m_fLoad;

		CDataLogCache * m_pCache;

		// Event Hooks
		BOOL OnMakeList(void);
		BOOL OnEnable(void);
		BOOL OnBtnDown(UINT n);
		BOOL OnBtnRepeat(UINT n);
		BOOL OnBtnUp(UINT n);
		BOOL OnTouchWork(P2 Pos);

		// Navigation
		BOOL HasDataToLeft(void);
		void StepNeg(DWORD s);
		void StepPos(DWORD s);

		// Tag Data
		BOOL MakeTagData(void);
		BOOL FindTagLimits(void);
		void FreeTagData(void);

		// Grid Drawing
		void DrawGrid(IGDI *pGDI);
		void DrawGridData(IGDI *pGDI, UINT uFlags);
		void DrawGridManual(IGDI *pGDI, UINT uFlags);
		void DrawGridAuto(IGDI *pGDI, UINT uFlags);
		void DrawGridTime(IGDI *pGDI);
		int  HorzLine(int y1, int y2, int yp, int yc);
		int  HorzLine(int y1, int y2, double f);

		// Info Drawing
		void DrawInfo(IGDI *pGDI);
		void DrawInfoLeft(IGDI *pGDI);
		void DrawInfoCenter(IGDI *pGDI);
		void DrawInfoRight(IGDI *pGDI);
		void DrawInfo(IGDI *pGDI, int j, int r, CString  const &Text);
		void DrawInfo(IGDI *pGDI, int j, int r, CUnicode const &Text);
		void FormatWidth(CString &Text, DWORD dwTime);

		// Data Plotting
		void DrawPlot(IGDI *pGDI);
		int  ScaleData(UINT uChan, DWORD Data);
		void SetTextColor(IGDI *pGDI, UINT uChan);
		void SetPenColor(IGDI *pGDI, UINT uChan);

		// Data Values
		void DrawData(IGDI *pGDI);

		// Cursor Drawing
		BOOL  DrawCursor(IGDI *pGDI);
		DWORD PosToTime(int xPos);
		int   TimeToPos(DWORD dwTime);

		// Layout Helpers
		void LayoutCore(IGDI *pGDI);
		BOOL LayoutData(IGDI *pGDI);
		void LayoutGrid(IGDI *pGDI);

		// Implementation
		double FindStep(double Range, double Limit);
		void   FindDrawLimits(void);
		BOOL   CanShowType(UINT Type);

		// Context Handling
		void FindCtx(CCtx &Ctx);
		void ClearCtx(CCtx &Ctx);
	};

// End of File

#endif
