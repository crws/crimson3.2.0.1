
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPenLine_HPP
	
#define	INCLUDE_PrimRubyPenLine_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyPenBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Line Pen
//

class DLLAPI CPrimRubyPenLine : public CPrimRubyPenBase
{
	public:
		// Constructor
		CPrimRubyPenLine(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Stroking
		BOOL Stroke(CRubyPath &output, CRubyPath const &figure);

		// Item Properties
		UINT m_End1;
		UINT m_End2;
	};

// End of File

#endif
