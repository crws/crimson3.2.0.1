
#include "intern.hpp"

#include "../g3comms/secure.hpp"

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// UI Manager Object
//

// Constructor

CUIManager::CUIManager(void)
{
	m_hPage      = NOTHING;
	m_pPage      = NULL;
	m_pEvents    = New CEventMap;
	m_pFonts     = New CFontManager;
	m_pPatterns  = New CRubyPatternLib;
	m_pOnStart   = NULL;
	m_pOnInit    = NULL;
	m_pOnUpdate  = NULL;
	m_pOnSecond  = NULL;
	m_TimeLock   = 30;
	m_TimePad    = 30;
	m_PadSize    = 0;
	m_PadLayout  = 0;
	m_ShowNext   = 1;
	m_AutoEntry  = 0;
	m_EntryOrder = 0;
	m_TwoFlag    = 1;
	m_TwoMulti   = 1;
	m_Beeper     = 0;
	m_PopAlignH  = 2;
	m_PopAlignV  = 2;
	m_GMC1	     = 1;
	m_pLast      = NULL;
	m_uLast      = GetTickCount();
	m_uOkay      = 0;
	m_LedAlarm   = 0;
	m_LedOrb     = 0;
	m_LedHome    = 0;
	m_PopKeypad  = 0;

	m_pTimeDisp  = NULL;
	}

// Destructor

CUIManager::~CUIManager(void)
{
	delete m_pPage;

	delete m_pEvents;

	delete m_pFonts;

	delete m_pPatterns;

	delete m_pOnStart;

	delete m_pOnInit;

	delete m_pOnUpdate;

	delete m_pOnSecond;

	delete m_pTimeDisp;
	}

// Initialization

void CUIManager::Load(PCBYTE &pData)
{
	ValidateLoad("CUIManager", pData);

	m_hPage = GetWord(pData);

	m_pEvents->Load(pData);

	m_pFonts->Load(pData);

	m_pPatterns->Load(pData);

	GetCoded(pData, m_pOnStart);
	GetCoded(pData, m_pOnInit);
	GetCoded(pData, m_pOnUpdate);
	GetCoded(pData, m_pOnSecond);

	m_TimeLock   = GetWord(pData);
	m_TimePad    = GetWord(pData);
	m_PadSize    = GetByte(pData);
	m_PadLayout  = GetByte(pData);
	m_PadNumeric = GetByte(pData);
	m_ShowNext   = GetByte(pData);
	m_AutoEntry  = GetByte(pData);
	m_EntryOrder = GetByte(pData);
	m_TwoFlag    = GetByte(pData);
	m_TwoMulti   = GetByte(pData);
	m_TwoNumeric = GetByte(pData);
	m_Beeper     = GetByte(pData);
	m_PopAlignH  = GetByte(pData);
	m_PopAlignV  = GetByte(pData);
	m_GMC1       = GetByte(pData);
	m_LedAlarm   = GetByte(pData);
	m_LedOrb     = GetByte(pData);
	m_LedHome    = GetByte(pData);
	m_PopKeypad  = GetByte(pData);

	g_pPxe->SetLedMode( (m_LedAlarm << 0) | 
			    (m_LedOrb   << 1) | 
			    (m_LedHome  << 2) );

	GetCoded(pData, m_pTimeDisp);
	}

// Attributes

UINT CUIManager::GetEditFlags(void) const
{
	UINT uFlags = 0;

	if( m_TwoFlag         ) uFlags |= flagTwoFlag;

	if( m_TwoMulti        ) uFlags |= flagTwoMulti;

	if( m_TwoNumeric      ) uFlags |= flagTwoNumeric;

	if( m_PadNumeric == 1 ) uFlags |= flagShowRamp;

	if( m_PadNumeric == 2 ) uFlags |= flagRampOnly;

	return uFlags;
	}

// Operations

void CUIManager::SetScan(UINT Code)
{
	SetItemScan(m_pOnStart,  Code);
	SetItemScan(m_pOnInit,   Code);
	SetItemScan(m_pOnUpdate, Code);
	SetItemScan(m_pOnSecond, Code);

	m_pEvents->SetScan(Code);

	SetItemScan(m_pTimeDisp,  Code);
	}

void CUIManager::ApplyMute(void)
{
	if( KeyCheck() ) {

		KeySetBeep(!(m_Beeper & 1));
		}

	if( TouchCheck() ) {

		TouchSetBeep(!(m_Beeper & 2));
		}
	}

void CUIManager::CancelMute(void)
{
	if( KeyCheck() ) {

		KeySetBeep(TRUE);
		}

	if( TouchCheck() ) {

		TouchSetBeep(TRUE);
		}
	}

BOOL CUIManager::Protect(PVOID pItem, UINT Protect, UINT uTrans)
{
	PVOID pLast = m_pLast;

	UINT  uTime = GetTickCount();

	UINT  uGone = uTime - m_uLast;

	m_pLast = pItem;

	m_uLast = uTime;

	if( Protect ) {

		if( uTrans == stateDown ) {

			if( Protect == 1 ) {

				m_uOkay = AreYouSure();

				return m_uOkay > 0;
				}
			else {
				if( m_uOkay && pLast == pItem ) {

					if( uGone < ToTicks(m_TimeLock * 1000) ) {

						return TRUE;
						}
					}

				if( Protect == 2 ) {

					m_uOkay = UnlockAction();

					return FALSE;
					}

				m_uOkay = 2 * UnlockAction();

				return FALSE;
				}
			}
		else {
			if( m_uOkay && pLast == pItem ) {

				if( Protect == 3 && uTrans == stateUp ) {

					m_uOkay--;
					}

				return TRUE;
				}
			}

		m_uOkay = 0;

		return FALSE;
		}

	m_uOkay = 0;

	return TRUE;
	}

// Implementation

BOOL CUIManager::AreYouSure(void)
{
	return CCommsSystem::m_pThis->m_pSecure->SimplePrompt("Confirm Operation of Action?");
	}

BOOL CUIManager::UnlockAction(void)
{
	return CCommsSystem::m_pThis->m_pSecure->SimplePrompt("Do You Want To Unlock This Action?");
	}

// End of File
