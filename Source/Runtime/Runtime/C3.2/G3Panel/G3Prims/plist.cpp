
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive List
//

// Constructor

CPrimList::CPrimList(void)
{
	m_uCount = 0;
	
	m_ppPrim = NULL;
	}

// Destructor

CPrimList::~CPrimList(void)
{
	while( m_uCount-- ) {

		delete m_ppPrim[m_uCount];
		}
	
	delete [] m_ppPrim;
	}

// Initialization

void CPrimList::Load(PCBYTE &pData, CPrim *pParent)
{
	ValidateLoad("CPrimList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		m_ppPrim = New CPrim * [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CPrim *pPrim = CPrim::MakeObject(GetByte(pData));

			if( (m_ppPrim[n] = pPrim) ) {

				pPrim->m_pParent = pParent;

				pPrim->Load(pData);
				}
			}
		}
	}

// End of File
