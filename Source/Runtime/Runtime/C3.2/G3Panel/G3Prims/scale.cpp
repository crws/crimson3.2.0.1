
#include "intern.hpp"

#include "scale.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Vertical Scale
//

// Constructor

CPrimVertScale::CPrimVertScale(void)
{
	m_Precise     = 1;

	m_UseAll      = 0;

	m_ShowLabels  = 2;

	m_ShowUnits   = 1;

	m_Orientation = 0;

	m_pLineCol    = New CPrimColor(naBack);

	m_pTextCol    = New CPrimColor(naText);

	m_pUnitCol    = New CPrimColor(naText);

	m_TextFont    = fontHei16;

	m_UnitFont    = fontHei16Bold;

	m_FontBase    = 2;
	
	memset(&m_Ctx, 0xFF, sizeof(m_Ctx));
	}

// Destructor

CPrimVertScale::~CPrimVertScale(void)
{
	delete m_pLineCol;

	delete m_pTextCol;

	delete m_pUnitCol;
	}

// Initialization

void CPrimVertScale::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimVertScale", pData);

	CPrimRich::Load(pData);

	m_Precise     = GetByte(pData);
	m_UseAll      = GetByte(pData);
	m_ShowLabels  = GetByte(pData);
	m_ShowUnits   = GetByte(pData);
	m_Orientation = GetByte(pData);
	
	m_pLineCol->Load(pData);
	m_pTextCol->Load(pData);
	m_pUnitCol->Load(pData);
	
	m_TextFont = GetWord(pData);
	m_UnitFont = GetWord(pData);
	m_FontBase = GetByte(pData);
	}

// Overridables

void CPrimVertScale::SetScan(UINT Code)
{
	m_pLineCol->SetScan(Code);
	
	m_pTextCol->SetScan(Code);
	
	m_pUnitCol->SetScan(Code);

	CPrimRich::SetScan(Code);
	}

void CPrimVertScale::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	BOOL fShow = TRUE;

	if( !IsAvail() ) {

		fShow = FALSE;
		}
	else {
		if( m_pVisible && !m_pVisible->ExecVal() ) {

			fShow = FALSE;
			}
		}

	if( m_fShow != fShow ) {

		if( fShow ) {

			m_fChange = TRUE;
			}
		else {
			Erase.Append(m_DrawRect);
			
			OnMessage(msgGoInvisible, 0);
			}

		m_fShow = fShow;
		}
	
	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;

			FindLayout(pGDI);
			}

		if( m_fChange ) {
			
			Erase.Append(m_DrawRect);
			}
		}
	}

void CPrimVertScale::DrawPrim(IGDI *pGDI)
{
	pGDI->ResetAll();

	pGDI->SetBrushFore(m_Ctx.m_LineCol);

	pGDI->SetTextFore (m_Ctx.m_TextCol);

	pGDI->SetTextTrans(modeTransparent);

	SelectFont(pGDI, m_TextFont);

	int StepSpan = m_Helper.GetStepSpan();

	int    Steps = max(1, StepSpan);

	BOOL   fFlip = m_Orientation == 1;

	for( int n = 0; n <= Steps; n++ ) {

		double Value = m_Helper.GetStepValue(n);

		int    yPos  = -1;

		int    xTick = 12;

		if( !m_Precise && StepSpan ) {

			yPos = m_y2 - (m_ySize * n) / StepSpan;
			}
		else {
			if( n == 0 ) {

				Value = m_Ctx.m_DataMin;

				yPos  = m_y2;
				}

			if( n == Steps ) {

				Value = m_Ctx.m_DataMax;

				yPos  = m_y1;
				}

			if( yPos < 0 ) {

				double d = (m_Ctx.m_DataMax - m_Ctx.m_DataMin);

				double f = (Value - m_Ctx.m_DataMin) / d;

				yPos     = m_y2 - int(m_ySize * f);
				}
			}

		if( n == 1 ) {

			if( yPos > m_y2 - m_fm ) {

				if( !Value) {

					if( fFlip ) {
						
						pGDI->FillRect(m_x2 - xTick, yPos, m_x2, yPos + 1);
						}
					else
						pGDI->FillRect(m_x1, yPos, m_x1 + xTick, yPos + 1);
					}

				continue;
				}
			}

		if( n == Steps - 1 ) {

			if( yPos < m_y1 + m_fm ) {

				if( !Value ) {
					
					if( fFlip ) {
						
						pGDI->FillRect(m_x2 - xTick, yPos, m_x2, yPos + 1);
						}
					else
						pGDI->FillRect(m_x1, yPos, m_x1 + xTick, yPos + 1);
					}

				continue;
				}
			}

		UINT     Flags = fmtPad | ((m_ShowUnits == 1) ? 0 : fmtBare);

		CUnicode Label = FindValueText(R2I(C3REAL(Value)), typeReal, Flags);

		int      nMove = m_fy / 2 - m_FontBase;

		BOOL     fDraw = (m_ShowLabels > 1);

		if( n == 0 ) { 

			if( m_UseAll ) {
			
				nMove = m_fy - m_FontBase * 2;
				}

			fDraw = (m_ShowLabels > 0);

			xTick = 18;
			}

		if( n == Steps ) {

			if( m_UseAll ) {
			
				nMove = 0;
				}

			fDraw = (m_ShowLabels > 0);

			xTick = 18;
			}

		if( fDraw ) {

			if( fFlip ) {

				int cx = pGDI->GetTextWidth(Label);
				
				pGDI->TextOut(m_x2 - 24 - cx, yPos - nMove, Label);
				}
			else
				pGDI->TextOut(m_x1 + 24, yPos - nMove, Label);
			}

		if( fFlip ) {
			
			pGDI->FillRect(m_x2 - xTick, yPos, m_x2, yPos + 1);
			}
		else
			pGDI->FillRect(m_x1, yPos, m_x1 + xTick, yPos + 1);
		}

	if( m_ShowUnits == 2 ) {

		CUnicode Units = FindValueText(R2I(0.0), typeReal, fmtUnits);

		UINT     uLen  = 0;

		Units.TrimBoth();

		if( (uLen = Units.GetLength()) ) {

			pGDI->SetTextFore(m_Ctx.m_UnitCol);

			SelectFont(pGDI, m_UnitFont);

			int cy = pGDI->GetTextHeight(L"0") + 2;

			int xp = m_x2 - pGDI->GetTextWidth(L"W") / 2; 

			int yp = m_y1 + (m_y2 - m_y1 - uLen * cy) / 2;

			for( UINT n = 0; n < uLen; n++ ) {

				WCHAR a[2] = { Units[n], 0 };

				int cx = pGDI->GetTextWidth(a);

				pGDI->TextOut(xp - cx / 2, yp, a);

				yp += cy;
				}
			}
		}

	if( fFlip ) {
		
		pGDI->FillRect(m_x2 - 1, m_y1, m_x2, m_y2);
		}
	else
		pGDI->FillRect(m_x1, m_y1, m_x1 + 1, m_y2);
	}

// Implementation

C3REAL CPrimVertScale::GetMinScale(void)
{
	if( m_pValue && m_TagLimits ) {

		CTag *pTag;

		if( GetTagItem(pTag) ) {

			DWORD     n = m_pValue->Execute(typeLValue);

			CDataRef &r = (CDataRef &) n;

			C3REAL    v = I2R(pTag->GetMinValue(r.x.m_Array, typeReal));

			if( pTag->GetDataType() == typeInteger ) {

				AdjustLimit(v);
				}

			return v;
			}
		}

	if( m_pLimitMin ) {

		C3REAL v = I2R(m_pLimitMin->Execute(typeReal));

		if( m_pLimitMin->GetType() == typeInteger ) {

			AdjustLimit(v);
			}
		
		return v;
		}
	
	return 0;
	}

C3REAL CPrimVertScale::GetMaxScale(void)
{
	if( m_pValue && m_TagLimits ) {

		CTag *pTag;

		if( GetTagItem(pTag) ) {

			DWORD     n = m_pValue->Execute(typeLValue);

			CDataRef &r = (CDataRef &) n;

			C3REAL    v = I2R(pTag->GetMaxValue(r.x.m_Array, typeReal));

			if( pTag->GetDataType() == typeInteger ) {

				AdjustLimit(v);
				}

			return v;
			}
		}

	if( m_pLimitMax ) {

		C3REAL v = I2R(m_pLimitMax->Execute(typeReal));

		if( m_pLimitMax->GetType() == typeInteger ) {

			AdjustLimit(v);
			}
		
		return v;
		}
	
	return 100;
	}

BOOL CPrimVertScale::AdjustLimit(C3REAL &v)
{
	CDispFormat *pFormat;

	if( GetDataFormat(pFormat) ) {

		if( pFormat->IsNumber() ) {

			CDispFormatNumber *pNum = (CDispFormatNumber *) pFormat;

			v = C3REAL(v / pow(double(10), int(pNum->m_After)));

			return TRUE;
			}
		}

	return FALSE;
	}

// Context Creation

void CPrimVertScale::FindCtx(CCtx &Ctx)
{
	Ctx.m_LineCol = m_pLineCol->GetColor();
	
	Ctx.m_TextCol = m_pTextCol->GetColor();
	
	Ctx.m_UnitCol = m_pUnitCol->GetColor();

	Ctx.m_DataMin  = GetMinScale();

	Ctx.m_DataMax  = GetMaxScale();
	}

void CPrimVertScale::FindLayout(IGDI *pGDI)
{
	SelectFont(pGDI, m_TextFont);

	m_x1 = m_DrawRect.x1;

	m_x2 = m_DrawRect.x2;

	m_y1 = m_DrawRect.y1;
	
	m_y2 = m_DrawRect.y2 - 1;

	m_fy = pGDI->GetTextHeight(L"0");

	m_fm = m_fy + 2;

	if( m_UseAll ) {

		m_fm += m_fy / 2;
		}
	else {
		m_y1 += m_fy / 2;

		m_y2 -= m_fy / 2;
		}

	m_ySize      = m_y2 - m_y1;

	double Limit = min(10, m_ySize / (m_fy + 4));

	m_Helper.SetLimits(m_Ctx.m_DataMin, m_Ctx.m_DataMax);

	m_Helper.CalcDecimalStep(Limit);
	}

// Context Check

BOOL CPrimVertScale::CCtx::operator == (CCtx const &That) const
{
	return m_LineCol == That.m_LineCol &&
	       m_TextCol == That.m_TextCol &&
	       m_UnitCol == That.m_UnitCol &&
	       m_DataMin == That.m_DataMin &&
	       m_DataMax == That.m_DataMax ;
	}

//////////////////////////////////////////////////////////////////////////
//
// Scale Helper
//

// Constructor

CScaleHelper::CScaleHelper(void)
{
	SetLimits(0, 100);	
	}

CScaleHelper::CScaleHelper(double DataMin, double DataMax)
{
	SetLimits(DataMin, DataMax);
	}

// Operations

void CScaleHelper::SetLimits(double DataMin, double DataMax)
{
	m_DataMin  = DataMin;
	
	m_DataMax  = DataMax;

	m_DataSpan = fabs(DataMax - DataMin);
	}

void CScaleHelper::CalcDecimalStep(double Limit)
{
	m_DrawStep = FindDrawStep(Limit);

	m_StepMin  = int(floor(m_DataMin / m_DrawStep));

	m_StepMax  = int(ceil (m_DataMax / m_DrawStep));
	}

void CScaleHelper::CalcTimeStep(double Limit)
{
	// TODO -- implement

	m_DrawStep = FindDrawStep(Limit);

	m_StepMin  = int(floor(m_DataMin / m_DrawStep));

	m_StepMax  = int(ceil (m_DataMax / m_DrawStep));
	}

// Attributes

double CScaleHelper::GetDrawStep(void)
{
	return m_DrawStep;
	}

double CScaleHelper::GetStepValue(int n)
{
	return (m_StepMin + n) * m_DrawStep;
	}

int CScaleHelper::GetStepCount(void)
{
	int StepSpan = m_StepMax - m_StepMin;

	return max(1, StepSpan);
	}

int CScaleHelper::GetStepSpan(void)
{
	return m_StepMax - m_StepMin;
	}

double CScaleHelper::FindDrawStep(double Limit)
{
	double Range = m_DataSpan;

	//////
	
	double Gap  = Range / Limit;

	double Mult = pow(10.0, floor(log10(Gap)));

	for( int p = 0; p < 2; p++ ) {

		double nList[] = { 1, 2, 5, 10 };

		for( int n = 0; n < 4; n++ ) {

			double Find = nList[n] * Mult;

			if( Gap <= Find ) {

				if( p == 1 ) {
					
					return Find;
					}
				
				if( Range / Find == floor(Range / Find) ) {

					return Find;
					}
				}
			}
		}

	AfxAssert(FALSE);

	return Range;
	}

// End of File
