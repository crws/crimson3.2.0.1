
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyTankFill_HPP
	
#define	INCLUDE_RubyTankFill_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyBrush.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Tank Fill
//

class DLLAPI CPrimRubyTankFill : public CPrimRubyBrush
{
	public:
		// Constructor
		CPrimRubyTankFill(void);

		// Destructor
		~CPrimRubyTankFill(void);

		// Operations
		void Set(COLOR Color);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		void SetScan(UINT Code);
		BOOL DrawPrep(IGDI *pGDI);

		// Drawing
		BOOL Fill(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver);

		// Item Properties
		UINT         m_Mode;
		CCodedItem * m_pValue;
		CCodedItem * m_pMin;
		CCodedItem * m_pMax;
		CPrimColor * m_pColor3;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR  m_Color3;
			C3REAL m_Data;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Static Data
		static UINT   m_ShadeMode;
		static COLOR  m_ShadeCol3;
		static C3REAL m_ShadeData;

		// Shader
		static BOOL Shader(IGDI *pGDI, int p, int c);

		// Rounding
		static C3INT Round(C3REAL Data);

		// Implementation
		void DoLoad(PCBYTE &pData);
		BOOL PrepTankFill(void);
		BOOL IsValueAvail(void);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

// End of File

#endif
