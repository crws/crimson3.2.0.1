
#include "intern.hpp"

#include "dial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Dial Gauge
//

// Constructor

CPrimLegacyDial::CPrimLegacyDial(void)
{
	m_pEdge     = New CPrimPen;

	m_pBack     = New CPrimBrush;

	m_pFill     = New CPrimColor(GetRGB(12,0,0));
	}

// Destructor

CPrimLegacyDial::~CPrimLegacyDial(void)
{
	delete m_pEdge;

	delete m_pBack;

	delete m_pFill;
	}

// Initialization

void CPrimLegacyDial::Load(PCBYTE &pData)
{
	CPrimRich::Load(pData);

	m_pEdge->Load(pData);

	m_pBack->Load(pData);

	m_pFill->Load(pData);

	m_DialRect.x1 = GetWord(pData);
	m_DialRect.y1 = GetWord(pData);
	m_DialRect.x2 = GetWord(pData);
	m_DialRect.y2 = GetWord(pData);

	m_Entity   = GetByte(pData);
	m_Orient   = GetByte(pData);

	m_Flip     = GetByte(pData);

	m_Major    = GetByte(pData);
	m_Minor    = GetByte(pData);

	m_Start    = GetWord(pData);
	m_Sweep    = GetWord(pData);

	m_Origin.x = GetWord(pData);
	m_Origin.y = GetWord(pData);

	m_nRadius1 = GetWord(pData);
	m_nRadius2 = GetWord(pData);
	m_nRadius3 = GetWord(pData);
	}

// Overridables

void CPrimLegacyDial::SetScan(UINT Code)
{
	m_pEdge->SetScan(Code);

	m_pBack->SetScan(Code);

	m_pFill->SetScan(Code);
	
	CPrimRich::SetScan(Code);
	}

void CPrimLegacyDial::MovePrim(int cx, int cy)
{
	m_DialRect.x1 += cx;
	
	m_DialRect.y1 += cy;
	
	m_DialRect.x2 += cx;
	
	m_DialRect.y2 += cy;

	m_Origin.x    += cx;

	m_Origin.y    += cy;

	CPrimRich::MovePrim(cx, cy);
	}

void CPrimLegacyDial::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRich::DrawPrep(pGDI, Erase, Trans);
	
	BOOL fShow  = TRUE;

	if( m_pVisible ) {
		
		if( !m_pVisible->IsAvail() || !m_pVisible->ExecVal() ) {

			fShow = FALSE;
			}
		}

	if( m_fShow != fShow ) {

		if( fShow ) {

			m_fChange = TRUE;
			}
		else
			Erase.Append(m_DialRect);

		m_fShow = fShow;
		}

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_pBack->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( m_pBack->IsNull() ) {

			if( m_fChange ) {

				Erase.Append(m_DrawRect);
				}
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}
		}
	}

void CPrimLegacyDial::DrawExec(IGDI *pGDI, IRegion *pErase)
{
	if( m_fShow ) {

		if( m_fChange || HitTest(pErase) ) {

			DrawPrim(pGDI);

			pErase->AddRect(m_DialRect);

			m_fChange = FALSE;
			}
		}
	}

void CPrimLegacyDial::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DialRect;

	pGDI->ResetBrush();

	m_pBack->FillEllipse(pGDI, Rect, m_Orient + m_Entity);
	
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawEllipse(pGDI, Rect, m_Orient + m_Entity);
		}

	pGDI->ResetBrush();

	DrawScale(pGDI, m_nRadius1, m_nRadius2);

	DrawPointer(pGDI, m_Origin, m_nRadius3, INT(m_Ctx.m_Pos));
	}

// Context Creation

void CPrimLegacyDial::FindCtx(CCtx &Ctx)
{
	if( m_pValue ) {

		if( (Ctx.m_fAvail = IsAvail()) ) {

			C3REAL Min = I2R(GetMinValue(typeReal));

			C3REAL Max = I2R(GetMaxValue(typeReal));

			Ctx.m_Min  = Min;

			Ctx.m_Max  = Max;

			Ctx.m_Pos  = C3REAL(m_Start);

			if( Min != Max ) {

				C3REAL Val = I2R(m_pValue->Execute(typeReal));

				MakeMin(Val, Max);

				MakeMax(Val, Min);

				Ctx.m_Pos  -= ((Val - Min) * m_Sweep) / (Max - Min);
				}
			
			return;
			}

		Ctx.m_Pos    = C3REAL(m_Start);

		Ctx.m_Min    = 0.0;

		Ctx.m_Max    = 100.0;
		}
	else {
		Ctx.m_fAvail = FALSE;

		Ctx.m_Pos    = C3REAL(m_Start);

		Ctx.m_Min    = 0.0;

		Ctx.m_Max    = 100.0;
		}
	}

// Context Check

BOOL CPrimLegacyDial::CCtx::operator == (CCtx const &That) const
{
	return m_fAvail  == That.m_fAvail   &&
	       m_Min     == That.m_Min      &&
	       m_Max     == That.m_Max      &&
	       m_Pos     == That.m_Pos      ;;
	}

// Implementation

void CPrimLegacyDial::DrawScale(IGDI *pGDI, int nRad1, int nRad2)
{
	int  nRad3 = (nRad1 + nRad2) / 2;

	int   nDiv = m_Major * m_Minor;

	for( int n = 0; n <= nDiv; n ++ ) {
		
		int nAngle = m_Start - MulDiv(m_Sweep, n, nDiv);
		
		if( n % m_Minor ) {

			DrawLine( pGDI, 
				  m_Origin, 
				  m_Flip ? nRad3 : nRad1, 
				  m_Flip ? nRad2 : nRad3, 
				  nAngle
				  );
			}
		else		
			DrawRect( pGDI, 
				  m_Origin, 
				  nRad1, 
				  nRad2, 
				  nAngle 
				  );
		}
	}

void CPrimLegacyDial::DrawPointer(IGDI *pGDI, P2 Org, INT nRadius, INT nAngle)
{
	double Theta = nAngle / 180.0 * acos(double(-1)); 

	double   Cos = cos(Theta);
	double   Sin = sin(Theta);

	double nRad1 = double(nRadius) * 0.75;
	double nRad2 = double(nRadius);

	double nWid1 = 2.0;
	double nWid2 = 3.5;

	P2 Pt[5];

	Pt[0].x = int(Org.x - nWid1*Sin + 0.5);
	Pt[0].y = int(Org.y - nWid1*Cos + 0.5);

	Pt[1].x = int(Org.x + nRad1*Cos - nWid2*Sin + 0.5);
	Pt[1].y = int(Org.y - nRad1*Sin - nWid2*Cos + 0.5);
	
	Pt[2].x = int(Org.x + nRad2*Cos + 0.5);
	Pt[2].y = int(Org.y - nRad2*Sin + 0.5);
	
	Pt[3].x = int(Org.x + nRad1*Cos + nWid2*Sin + 0.5);
	Pt[3].y = int(Org.y - nRad1*Sin + nWid2*Cos + 0.5);

	Pt[4].x = int(Org.x + nWid1*Sin + 0.5);
	Pt[4].y = int(Org.y + nWid1*Cos + 0.5);
	
	pGDI->SetBrushFore(m_pFill->GetColor());

	pGDI->FillPolygon(Pt, elements(Pt), 0);
	
	m_pEdge->DrawPolygon(pGDI, Pt, elements(Pt), 0);
	}

void CPrimLegacyDial::DrawRect(IGDI *pGDI, P2 Org, INT nRad1, INT nRad2, INT nAngle)
{
	double Theta = nAngle / 180.0 * acos(double(-1));

	double   Cos = cos(Theta);
	double   Sin = sin(Theta);

	double  nLen = nRad2 - nRad1;

	double nWid1 = 4;
	double nWid2 = nWid1 / 2;

	P2 Pt[4];

	Pt[0].x = int(Org.x   + nRad1*Cos - nWid2*Sin + 0.5);
	Pt[0].y = int(Org.y   - nRad1*Sin - nWid2*Cos + 0.5);

	Pt[1].x = int(Pt[0].x + nLen*Cos + 0.5);
	Pt[1].y = int(Pt[0].y - nLen*Sin + 0.5);

	Pt[2].x = int(Pt[1].x + nWid1*Sin + 0.5);
	Pt[2].y = int(Pt[1].y + nWid1*Cos + 0.5);

	Pt[3].x = int(Pt[2].x - nLen*Cos + 0.5);
	Pt[3].y = int(Pt[2].y + nLen*Sin + 0.5);
	
	pGDI->SetBrushFore(m_pFill->GetColor());

	pGDI->FillPolygon(Pt, elements(Pt), 0);
	
	m_pEdge->DrawPolygon(pGDI, Pt, elements(Pt), 0);
	}

void CPrimLegacyDial::DrawLine(IGDI *pGDI, P2 Org, INT nRad1, INT nRad2, INT nAngle)
{
	double Theta = nAngle / 180.0 * acos(double(-1));

	double   Cos = cos(Theta);
	double   Sin = sin(Theta);

	P2 Pt[2];

	Pt[0].x = int(Org.x + nRad1*Cos + 0.5);
	Pt[0].y = int(Org.y - nRad1*Sin + 0.5);

	Pt[1].x = int(Org.x + nRad2*Cos + 0.5);
	Pt[1].y = int(Org.y - nRad2*Sin + 0.5);

	pGDI->SetPenFore(m_pEdge->m_pColor->GetColor());
	
	pGDI->DrawLine(PassPoint(Pt[0]), PassPoint(Pt[1]));
	}

int CPrimLegacyDial::MulDiv(int a, int b, int c)
{
	return (a * b) / c;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Whole Dial Gauge
//

// Constructor

CPrimLegacyWholeDial::CPrimLegacyWholeDial(void)
{
	}

// Initialization

void CPrimLegacyWholeDial::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyWholeDial", pData);
	
	CPrimLegacyDial::Load(pData);
	}

// Overridables

void CPrimLegacyWholeDial::DrawPrim(IGDI *pGDI)
{
	CPrimLegacyDial::DrawPrim(pGDI);
	
	DrawLegend(pGDI);
	}

// Implementation

void CPrimLegacyWholeDial::DrawLegend(IGDI *pGDI)
{
	if( m_Content < 3 ) {

		SelectFont(pGDI);

		SetColors(pGDI, 2);

		R2 Work;

		Work.x1 = m_Origin.x;

		Work.y1 = m_Origin.y + 10;

		Work.x2 = Work.x1;
		
		Work.y2 = Work.y1;

		if( m_Content > 0 ) {

			int    cx = GetTextWidth(pGDI,  CPrimRich::m_Ctx.m_Label);

			int    cy = GetTextHeight(pGDI, CPrimRich::m_Ctx.m_Label);

			Work.x1  -= cx / 2;
			
			Work.x2  += cx / 2;
			
			Work.y2  += cy;
			
			DrawLabel(pGDI, Work);

			Work.y1 += cy;
			}

		if( m_Content < 2 ) {

			// init
			Work.x1   = Work.x2 = m_Origin.x;

			int    cx = GetTextWidth(pGDI,  CPrimRich::m_Ctx.m_Value);

			int    cy = GetTextHeight(pGDI, CPrimRich::m_Ctx.m_Value);

			Work.x1  -= cx / 2;
			
			Work.x2  += cx / 2;
			
			Work.y2  += cy;
			
			DrawValue(pGDI, Work);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Half Dial Gauge
//

// Constructor

CPrimLegacyHalfDial::CPrimLegacyHalfDial(void)
{
	}

// Initialization

void CPrimLegacyHalfDial::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyHalfDial", pData);
	
	CPrimLegacyDial::Load(pData);
	}

// Overridables

void CPrimLegacyHalfDial::DrawPrim(IGDI *pGDI)
{
	CPrimLegacyDial::DrawPrim(pGDI);
	
	if( m_pEdge->m_Width ) {

		R2 Rect = m_DialRect;

		Rect.x2--;

		switch( m_Orient ) {

			case 0:
				Rect.right  = Rect.left + m_pEdge->m_Width;

				break;

			case 1:
				Rect.top    = Rect.bottom - m_pEdge->m_Width;

				break;

			case 2:
				Rect.left   = Rect.right - m_pEdge->m_Width;

				break;

			case 3:
				Rect.bottom = Rect.top + m_pEdge->m_Width;

				break;
			}

		m_pEdge->DrawRect(pGDI, Rect);
		}
	
	DrawLegend(pGDI);
	}

// Implementation

void CPrimLegacyHalfDial::DrawLegend(IGDI *pGDI)
{
	if( m_Content < 3 ) {

		SelectFont(pGDI);

		SetColors(pGDI, 2);

		R2   Work;

		int  dx[] = { +10,   0, -10,   0 };

		int  dy[] = {   0, -10,   0, +10 };

		Work.x1   = m_Origin.x + dx[m_Orient];

		Work.y1   = m_Origin.y + dy[m_Orient];

		int ySize = GetTextHeight(pGDI, CPrimRich::m_Ctx.m_Label);

		switch( m_Orient ) {

			case 0:
				Work.y1 -= (m_Content == 1) ? ySize : ySize / 2;

				break;

			case 1:
				Work.y1 -= (m_Content == 1) ? (2 * ySize) : ySize;

				break;

			case 2:
				Work.y1 -= (m_Content == 1) ? ySize : ySize / 2;

				break;

			case 3:
				break;
			}

		Work.x2 = Work.x1;
		
		Work.y2 = Work.y1;

		if( m_Content > 0 ) {

			int xSize = GetTextWidth(pGDI, CPrimRich::m_Ctx.m_Label);

			switch( m_Orient ) {
				
				case 0:
					Work.x2 += xSize;
					break;
				
				case 1:
					Work.x1 -= xSize / 2;
					
					Work.x2 += xSize / 2;
					
					break;
				
				case 2:
					Work.x1 -= xSize;
					
					break;
				
				case 3:
					Work.x1 -= xSize / 2;
					
					Work.x2 += xSize / 2;
					
					break;
				}

			Work.y2 += ySize;

			DrawLabel(pGDI, Work);

			Work.y1 = Work.y2;
			}

		if( m_Content < 2 ) {

			int xSize = GetTextWidth(pGDI, CPrimRich::m_Ctx.m_Value);

			Work.x1 = m_Origin.x + dx[m_Orient];

			Work.x2 = Work.x1;

			switch( m_Orient ) {
				
				case 0:
					Work.x2 += xSize;
					
					break;
				
				case 1:
					Work.x1 -= xSize / 2;
					
					Work.x2 += xSize / 2;
					
					break;
				
				case 2:
					Work.x1 -= xSize;
					
					break;
				
				case 3:
					Work.x1 -= xSize / 2;
					
					Work.x2 += xSize / 2;
					
					break;
				}

			Work.y2 += ySize;

			DrawValue(pGDI, Work);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Quad Dial Gauge
//

// Constructor

CPrimLegacyQuadDial::CPrimLegacyQuadDial(void)
{
	}

// Initialization

void CPrimLegacyQuadDial::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyQuadDial", pData);
	
	CPrimLegacyDial::Load(pData);
	}

// Overridables

void CPrimLegacyQuadDial::DrawPrim(IGDI *pGDI)
{
	CPrimLegacyDial::DrawPrim(pGDI);
	
	if( m_pEdge->m_Width ) {

		R2  Rect1 = m_DialRect;

		R2  Rect2 = m_DialRect;

		switch( m_Orient ) {

			case 0:
				Rect1.top   = Rect2.bottom - m_pEdge->m_Width;

				Rect2.right = Rect2.left   + m_pEdge->m_Width;

				break;

			case 1:
				Rect1.left  = Rect1.right  - m_pEdge->m_Width;

				Rect2.top   = Rect2.bottom - m_pEdge->m_Width;

				break;

			case 2:
				Rect1.bottom = Rect1.top   + m_pEdge->m_Width;

				Rect2.left   = Rect2.right - m_pEdge->m_Width;

				break;

			case 3:
				Rect1.right  = Rect1.left  + m_pEdge->m_Width;

				Rect2.bottom = Rect2.top   + m_pEdge->m_Width;

				break;
			}

		m_pEdge->DrawRect(pGDI, Rect1);

		m_pEdge->DrawRect(pGDI, Rect2);
		}
	
	DrawLegend(pGDI);
	}

// Implementation

void CPrimLegacyQuadDial::DrawLegend(IGDI *pGDI)
{
	if( m_Content < 3 ) {

		SelectFont(pGDI);

		SetColors(pGDI, 2);

		R2   Work;

		int  dx[] = { +10, -10, -10, +10 };

		int  dy[] = { -10, -10, +10, +10 };

		Work.x1   = m_Origin.x + dx[m_Orient];

		Work.y1   = m_Origin.y + dy[m_Orient];

		Work.x2   = Work.x1;

		int ySize = GetTextHeight(pGDI, CPrimRich::m_Ctx.m_Label);

		switch( m_Orient ) {

			case 0:				
				Work.y1 -= (m_Content == 1) ? (2 * ySize) : ySize;

				break;

			case 1:
				Work.y1 -= (m_Content == 1) ? (2 * ySize) : ySize;

				break;

			case 2:
				
				break;

			case 3:
				break;
			}

		Work.y2 = Work.y1;

		if( m_Content > 0 ) {

			int xSize = GetTextWidth(pGDI, CPrimRich::m_Ctx.m_Label);

			switch( m_Orient ) {
				
				case 0:
					Work.x2 += xSize;
					break;
				
				case 1:
					Work.x1 -= xSize;
					break;
				
				case 2:
					Work.x1 -= xSize;
					break;
				
				case 3:
					Work.x2 += xSize;
					break;
				}

			Work.y2 += ySize;

			DrawLabel(pGDI, Work);

			Work.y1  = Work.y2;
			}

		if( m_Content < 2 ) {

			int xSize = GetTextWidth(pGDI, CPrimRich::m_Ctx.m_Value);
			
			Work.x1 = m_Origin.x + dx[m_Orient];

			Work.x2 = Work.x1;

			switch( m_Orient ) {
				
				case 0:
					Work.x2 += xSize;
					break;
				
				case 1:
					Work.x1 -= xSize;
					break;
				
				case 2:
					Work.x1 -= xSize;
					break;
				
				case 3:
					Work.x2 += xSize;
					break;
				}

			Work.y2 += ySize;

			DrawValue(pGDI, Work);
			}
		}
	}

// End of File
