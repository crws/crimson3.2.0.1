
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BAR_HPP
	
#define	INCLUDE_BAR_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyBar;
class CPrimLegacyVertBar;
class CPrimLegacyHorzBar;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Bar
//

class CPrimLegacyBar : public CPrimRich
{
	public:
		// Constructor
		CPrimLegacyBar(void);

		// Destructor
		~CPrimLegacyBar(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawExec(IGDI *pGDI, IRegion *pDirty);

		// Item Properties
		CPrimPen     * m_pEdge;
		CPrimBrush   * m_pFill;
		CPrimColor   * m_pColor3;
		UINT           m_uMode;
		BOOL           m_fShowSP;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			BOOL     m_fAvail;
			COLOR    m_Color3;
			DWORD    m_Val;
			DWORD    m_Min;
			DWORD    m_Max;
			C3REAL   m_Pos;
			C3REAL   m_SP;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);

		// Implementation
		void FillRect(IGDI *pGDI, R2 Rect);

		// Rounding
		C3INT Round(C3REAL Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Bar
//

class CPrimLegacyVertBar : public CPrimLegacyBar
{
	public:
		// Constructor
		CPrimLegacyVertBar(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);

	protected:
		// Implementation
		void DrawSP(IGDI *pGDI, R2 Rect);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Bar
//

class CPrimLegacyHorzBar : public CPrimLegacyBar
{
	public:
		// Constructor
		CPrimLegacyHorzBar(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);

	protected:
		// Implementation
		void DrawSP(IGDI *pGDI, R2 Rect);
	};

// End of File

#endif
