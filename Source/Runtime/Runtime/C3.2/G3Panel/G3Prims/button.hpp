
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BUTTON_HPP
	
#define	INCLUDE_BUTTON_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimGradButton;
class CPrimImageBtnBase;
class CPrimImageIndicator;
class CPrimImageIllumButton;
class CPrimImageButton;
class CPrimImageToggle;
class CPrimImageToggle2;
class CPrimImageToggle3;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Graduated Button
//

class CPrimGradButton : public CPrimWithText
{
	public:
		// Constructor
		CPrimGradButton(void);

		// Destructor
		~CPrimGradButton(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);

		// Data Members
		CPrimColor * m_pColor1;
		CPrimColor * m_pColor2;
		CPrimPen   * m_pEdge;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR m_Color1;
			COLOR m_Color2;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Static Data
		static BOOL  m_ShadeMode;
		static COLOR m_ShadeCol1;
		static COLOR m_ShadeCol2;

		// Color Mixing
		static COLOR Mix16(COLOR a, COLOR b, int p, int c);
		static DWORD Mix32(COLOR a, COLOR b, int p, int c);

		// Shaders
		static BOOL Shader(IGDI *pGDI, int p, int c);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Button Base
//

class CPrimImageBtnBase : public CPrimWithText
{
	public:
		// Constructor
		CPrimImageBtnBase(void);

		// Destructor
		~CPrimImageBtnBase(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		void MovePrim(int cx, int cy);

		// Data Members
		CPrimImage * m_pImage[4];
		R2           m_ImageRect;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			UINT m_uIndex;
			BOOL m_fEnable;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;
		BOOL m_fAlias;

		// Image Selection
		virtual UINT GetIndex(void);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Indicator
//

class CPrimImageIndicator : public CPrimImageBtnBase
{
	public:
		// Constructor
		CPrimImageIndicator(void);

		// Destructor
		~CPrimImageIndicator(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);

		// Data Members
		CCodedItem * m_pState;

	protected:
		// Image Selection
		UINT GetIndex(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Illuminated Button
//

class CPrimImageIllumButton : public CPrimImageIndicator
{
	public:
		// Constructor
		CPrimImageIllumButton(void);

		// Destructor
		~CPrimImageIllumButton(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAvail(void) const;

		// Overridables
		void SetScan(UINT Code);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		CCodedItem * m_pValue;
		UINT         m_Button;
		UINT	     m_Delay;
		CCodedItem * m_pEnable;
		UINT         m_Local;
		UINT	     m_Protect;

	protected:
		// Data Members
		BOOL m_fValue;
		BOOL m_fState;
		BOOL m_fInit;
		BOOL m_fTime;
		UINT m_uTime;

		// Message Handlers
		UINT OnTakeFocus(UINT uMsg, UINT uParam);
		UINT OnTouchDown(void);
		UINT OnTouchRepeat(void);
		UINT OnTouchUp(void);

		// Image Selection
		UINT GetIndex(void);

		// Implementation
		void GetValue(void);
		void SetValue(C3INT Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Button
//

class CPrimImageButton : public CPrimImageBtnBase
{
	public:
		// Constructor
		CPrimImageButton(void);

		// Destructor
		~CPrimImageButton(void);

		// Overridables
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);

		// Initialization
		void Load(PCBYTE &pData);

	protected:
		// Implementation
		UINT GetIndex(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Toggle Base Class
//

class CPrimImageToggle : public CPrimImageBtnBase
{
	public:
		// Constructor
		CPrimImageToggle(void);

		// Destructor
		~CPrimImageToggle(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAvail(void) const;

		// Overridables
		void SetScan(UINT Code);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		CCodedItem * m_pValue;
		CCodedItem * m_pEnable;
		UINT         m_Local;
		UINT	     m_Protect;
		UINT         m_Axis;
		UINT	     m_Mode;
		UINT	     m_Delay;
		UINT         m_Default;
		CCodedItem * m_pPress1;
		CCodedItem * m_pRelease1;
		CCodedItem * m_pPress2;
		CCodedItem * m_pRelease2;

	protected:
		// Data Members
		UINT m_uValue;
		UINT m_uInit;
		UINT m_uSeg;
		BOOL m_fTime;
		UINT m_uTime;

		// Message Handlers
		UINT OnTakeFocus(UINT uMsg, UINT uParam);

		// Implementation
		void GetValue(void);
		void SetValue(C3INT Data);
		UINT FindSeg(UINT uParam);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image 2-State Toggle
//

class CPrimImageToggle2 : public CPrimImageToggle
{
	public:
		// Constructor
		CPrimImageToggle2(void);

		// Destructor
		~CPrimImageToggle2(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		UINT m_ValA;
		UINT m_ValB;

	protected:
		// Message Handlers
		UINT OnTouchDown(void);
		UINT OnTouchRepeat(void);
		UINT OnTouchUp(void);

		// Image Selection
		UINT GetIndex(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image 3-State Toggle
//

class CPrimImageToggle3 : public CPrimImageToggle
{
	public:
		// Constructor
		CPrimImageToggle3(void);

		// Destructor
		~CPrimImageToggle3(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		UINT m_ValA;
		UINT m_ValB;
		UINT m_ValC;

	protected:
		// Message Handlers
		UINT OnTouchDown(void);
		UINT OnTouchRepeat(void);
		UINT OnTouchUp(void);

		// Image Selection
		UINT GetIndex(void);
	};

// End of File

#endif
