
#include "intern.hpp"

#include "bevel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Bevel Base Class
//

// Constructor

CPrimBevelBase::CPrimBevelBase(void)
{
	m_Border  = 4;

	m_pFace   = New CPrimBrush;

	m_pHilite = New CPrimColor(naFeature);

	m_pShadow = New CPrimColor(naBack);
	}

// Destructor

CPrimBevelBase::~CPrimBevelBase(void)
{
	delete m_pFace;

	delete m_pHilite;

	delete m_pShadow;
	}

// Download Support

void CPrimBevelBase::Load(PCBYTE &pData)
{
	CPrimWithText::Load(pData);

	m_Border = GetByte(pData);

	m_pFace->Load(pData);

	m_pHilite->Load(pData);

	m_pShadow->Load(pData);
	}

// Overridables

void CPrimBevelBase::SetScan(UINT Code)
{
	m_pFace->SetScan(Code);

	m_pHilite->SetScan(Code);

	m_pShadow->SetScan(Code);

	CPrimWithText::SetScan(Code);
	}

void CPrimBevelBase::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimWithText::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_pFace->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}
		}
	}

// Implementation

void CPrimBevelBase::FindPoints(P2 *t, P2 *b, R2 &r)
{
	R2 Rect1 = r;

	R2 Rect2 = r;

	DeflateRect(Rect2, m_Border - 1, m_Border - 1);

	Rect1.x2--;
	Rect1.y2--;
	Rect2.x2--;
	Rect2.y2--;

	t[0].x = Rect1.x1; t[0].y = Rect1.y2;
	t[1].x = Rect1.x1; t[1].y = Rect1.y1;
	t[2].x = Rect1.x2; t[2].y = Rect1.y1;
	t[3].x = Rect2.x2; t[3].y = Rect2.y1;
	t[4].x = Rect2.x1; t[4].y = Rect2.y1;
	t[5].x = Rect2.x1; t[5].y = Rect2.y2;

	b[0].x = Rect1.x1; b[0].y = Rect1.y2;
	b[1].x = Rect1.x2; b[1].y = Rect1.y2;
	b[2].x = Rect1.x2; b[2].y = Rect1.y1;
	b[3].x = Rect2.x2; b[3].y = Rect2.y1;
	b[4].x = Rect2.x2; b[4].y = Rect2.y2;
	b[5].x = Rect2.x1; b[5].y = Rect2.y2;
	    
	Rect2.x1 += 1;
	Rect2.y1 += 1;

	r = Rect2;
	}

// Context Creation

void CPrimBevelBase::FindCtx(CCtx &Ctx)
{
	Ctx.m_Hilite = m_pHilite->GetColor();
	
	Ctx.m_Shadow = m_pShadow->GetColor();
	}

// Context Check

BOOL CPrimBevelBase::CCtx::operator == (CCtx const &That) const
{
	return m_Hilite == That.m_Hilite &&
	       m_Shadow == That.m_Shadow ;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Bevel
//

// Constructor

CPrimBevel::CPrimBevel(void)
{
	m_Type = 0;
	}

// Destructor

CPrimBevel::~CPrimBevel(void)
{
	m_Type = 0;
	}

// Download Support

void CPrimBevel::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimBevel", pData);

	CPrimBevelBase::Load(pData);

	m_Type = GetByte(pData);
	}

// Overridables

void CPrimBevel::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	P2 h[6];
	
	P2 s[6];

	FindPoints(h, s, Rect);

	pGDI->SelectBrush(brushFore);

	pGDI->SetBrushFore(m_Ctx.m_Shadow);

	pGDI->FillPolygon((m_Type == 1) ? h : s, 6, 0);

	pGDI->SetBrushFore(m_Ctx.m_Hilite);

	pGDI->FillPolygon((m_Type == 1) ? s : h, 6, 0);

	if( m_Type == 2 ) {

		FindPoints(s, h, Rect);

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(m_Ctx.m_Shadow);

		pGDI->FillPolygon(s, 6, 0);

		pGDI->SetBrushFore(m_Ctx.m_Hilite);

		pGDI->FillPolygon(h, 6, 0);
		}

	m_pFace->FillRect(pGDI, Rect);

	CPrimBevelBase::DrawPrim(pGDI);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Button with Bevel
//

// Constructor

CPrimBevelButton::CPrimBevelButton(void)
{
	}

// Download Support

void CPrimBevelButton::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimBevelButton", pData);

	CPrimBevelBase::Load(pData);
	}

// Overridables

void CPrimBevelButton::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	P2 h[6];
	
	P2 s[6];

	FindPoints(h, s, Rect);

	pGDI->SelectBrush(brushFore);

	pGDI->SetBrushFore(m_Ctx.m_Shadow);

	BOOL p = IsPressed();

	pGDI->FillPolygon(p ? h : s, 6, 0);

	pGDI->SetBrushFore(m_Ctx.m_Hilite);

	pGDI->FillPolygon(p ? s : h, 6, 0);

	m_pFace->FillRect(pGDI, Rect);

	// REV3 -- Show disabled text differently?

	CPrimBevelBase::DrawPrim(pGDI);
	}

void CPrimBevelButton::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_DrawRect));
	}

// End of File
