
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Font Manager
//

// Constructor

CFontManager::CFontManager(void)
{
	m_uCount = 0;

	m_ppFont = NULL;
	}

// Destructor

CFontManager::~CFontManager(void)
{
	while( m_uCount-- ) {

		delete m_ppFont[m_uCount];
		}

	delete [] m_ppFont;
	}

// Initialization

void CFontManager::Load(PCBYTE &pData)
{
	ValidateLoad("CFontManager", pData);

	m_uCount = GetWord(pData);

	m_ppFont = New CFontItem * [ m_uCount ];

	for( UINT n = 0; n < m_uCount; n++ ) {

		CFontItem * pFont = NULL;

		UINT        hFont = GetWord(pData);

		if( hFont < 0xFFFF ) {

			PCBYTE pInit = PCBYTE(g_pDbase->LockItem(hFont));

			if( pInit ) {

				if( GetWord(pInit) == IDC_FONT ) {
			
					pFont = New CFontItem;

					pFont->Load(pInit);
					}

				g_pDbase->PendItem(hFont, TRUE);
				}
			}

		m_ppFont[n] = pFont;
		}
	}

// Font Selection

BOOL CFontManager::Select(IGDI *pGDI, UINT uFont)
{
	if( uFont < 0x100 ) {

		pGDI->SelectFont(uFont);

		return TRUE;
		}
	else {
		CFontItem *pFont = m_ppFont[uFont - 0x100];

		if( pFont ) {

			pGDI->SelectFont(pFont);

			return TRUE;
			}

		pGDI->SelectFont(fontDefault);

		return FALSE;
		}
	}

// Attributes

int CFontManager::GetHeight(UINT uFont)
{
	if( uFont >= 0x100 ) {

		CFontItem *pFont = m_ppFont[uFont - 0x100];

		if( pFont ) {

			return pFont->GetGlyphHeight(0);
			}

		return 16;
		}

	switch( uFont ) {

		case fontSwiss0406: return 6;
		case fontSwiss0708: return 8;
		case fontSwiss0512: return 12;
		case fontSwiss0712: return 12;
		case fontSwiss1216: return 16;
		case fontTimes0808: return 8;
		case fontTimes0812: return 12;
		case fontTimes1612: return 12;
		case fontHei10:	    return 10;
		case fontHei10Bold: return 10;
		case fontHei12:	    return 12;
		case fontHei12Bold: return 12;
		case fontHei14:	    return 14;
		case fontHei14Bold: return 14;
		case fontHei16:	    return 16;
		case fontHei16Bold: return 16;
		case fontHei18:	    return 18;
		case fontHei18Bold: return 18;
		case fontHei20:	    return 20;
		case fontHei20Bold: return 20;
		case fontHei24:	    return 24;
		case fontHei24Bold: return 24;
		case fontBig24:	    return 24;
		case fontBig32:	    return 32;
		case fontBig64:	    return 64;
		case fontBig96:	    return 96;

		case fontDefault:   return 16;

		}

	return 16;
	}

BOOL CFontManager::IsAntiAliased(UINT uFont)
{
	if( uFont >= 0x100 ) {

		CFontItem *pFont = m_ppFont[uFont - 0x100];

		if( pFont ) {

			return pFont->IsAntiAliased();
			}

		return FALSE;
		}

	switch( uFont ) {

		case fontBig24:
		case fontBig32:
		case fontBig64:
		case fontBig96:

			return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Font Item
//

// Constructor

CFontItem::CFontItem(void)
{
	StdSetRef();

	m_Size   = 0;
	m_nBase  = 0;
	m_uCount = 0;
	m_pList  = NULL;
	m_pPos   = NULL;
	m_pWidth = NULL;
	m_pData  = NULL;
	m_fFill  = FALSE;
	m_xSpace = 0;
	m_xDigit = 0;
	}

// Destructor

CFontItem::~CFontItem(void)
{
	}

// Initialization

void CFontItem::Load(PCBYTE &pData)
{
	ValidateLoad("CFontItem", pData);

	GetLong(pData);
		 
	m_Size   = GetWord(pData);

	m_nBase  = GetWord(pData);

	m_uCount = GetWord(pData);

	GetWord(pData);

	m_pList  = PCWORD(pData); pData += sizeof(WORD) * m_uCount;
	
	m_pWidth = PCWORD(pData); pData += sizeof(WORD) * m_uCount;
	
	m_pPos   = PCUINT(pData); pData += sizeof(UINT) * m_uCount;

	m_pData  = PCBYTE(pData);

	m_cFrom  = MotorToHost(m_pList[0]);

	m_cTo    = MotorToHost(m_pList[m_uCount - 1]);

	m_xSpace = GetGlyphWidth(' ');

	m_xDigit = GetGlyphWidth('4');
	}

// Attributes

BOOL CFontItem::IsAntiAliased(void)
{
	UINT n;

	UINT c = '?';

	if( (n = FindGlyph(c)) < NOTHING ) {

		if( MotorToHost(DWORD(m_pPos[n])) < NOTHING ) {

			PCBYTE pData   = m_pData + MotorToHost(DWORD(m_pPos[n]));

			int    xCell   = GetByte(pData);

			int    yCell   = GetByte(pData);

			int    nStride = GetByte(pData);

			BOOL   fSmooth = GetByte(pData);
			
			return fSmooth;
			}
		}

	return FALSE;
	}

// IUnknown

HRESULT CFontItem::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IGdiFont);

	StdQueryInterface(IGdiFont);

	return E_NOINTERFACE;
	}

ULONG CFontItem::AddRef(void)
{
	StdAddRef();
	}

ULONG CFontItem::Release(void)
{
	StdRelease();
	}

// Font Attributes

BOOL CFontItem::IsProportional(void)
{
	return TRUE;
	}

int CFontItem::GetBaseLine(void)
{
	return m_nBase;
	}

int CFontItem::GetGlyphWidth(WORD c)
{
	UINT n;

	if( m_xSpace ) {

		switch( c ) {

			case spaceNormal:
			case spaceNoBreak:

				return m_xSpace;

			case spaceNarrow:

				return m_xSpace / 2;

			case spaceHair:

				return 1;

			case spaceFigure:

				return m_xDigit;
			}

		if( IsFixed(c) ) {

			return m_xDigit;
			}
		}

	if( IsExplicitCode(c) ) {		
		
		return 0;
		}

	if( (n = FindGlyph(c)) < NOTHING ) {

		return MotorToHost(m_pWidth[n]);
		}

	if( c == '?' ) {

		// No question mark available so we have
		// to use a square for missing glyphs.

		return m_Size;
		}

	return GetGlyphWidth('?');
	}

int CFontItem::GetGlyphHeight(WORD c)
{
	return m_Size;
	}

// Font Operations

void CFontItem::InitBurst(IGDI *pGDI, CLogFont const &Font)
{
	if( Font.m_Trans == modeOpaque ) {

		pGDI->PushBrush();

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(Font.m_Back);

		m_fFill = TRUE;
		}

	m_Fore = Font.m_Fore;

	m_Back = Font.m_Back;
	}

void CFontItem::DrawGlyph(IGDI *pGDI, int &x, int &y, WORD c)
{
	UINT n;

	if( IsSpace(c) ) {

		int cx = GetGlyphWidth(c);

		if( m_fFill ) {

			pGDI->FillRect(x, y, x+cx, y+m_Size);
			}

		x += cx;

		return;
		}

	if( IsFixed(c) ) {

		if( IsFixedDigit(c) ) {

			c -= digitFixed;

			c += digitSimple;
			}
		else {
			c -= letterFixed;

			c += letterSimple;
			}

		int cx = GetGlyphWidth(c);

		int x1 = (m_xDigit - cx + 0) / 2;

		int x2 = (m_xDigit - cx + 1) / 2;

		if( x1 && m_fFill ) {

			pGDI->FillRect(x, y, x+x1, y+m_Size);
			}

		x += x1;

		DrawGlyph(pGDI, x, y, c);

		if( x2 && m_fFill ) {

			pGDI->FillRect(x, y, x+x2, y+m_Size);
			}

		x += x2;

		return;
		}

	if( IsExplicitCode(c) ) {
		
		return;
		}

	if( (n = FindGlyph(c)) < NOTHING ) {

		if( MotorToHost(DWORD(m_pPos[n])) < NOTHING ) {

			PCBYTE pData   = m_pData + MotorToHost(DWORD(m_pPos[n]));

			int    xCell   = GetByte(pData);

			int    yCell   = GetByte(pData);

			int    xPos    = x + xCell;

			int    yPos    = y + yCell;

			int    nStride = GetByte(pData);

			BOOL   fSmooth = GetByte(pData);

			int    cx      = GetWord(pData);

			int    cy      = GetWord(pData);
			
			if( m_fFill ) {

				// REV3 -- Just drawing missing sections?

				pGDI->FillRect( x,
						y,
						x + MotorToHost(m_pWidth[n]),
						y + m_Size
						);
				}

			if( fSmooth ) {

				if( m_fFill ) {

					pGDI->CharBlt( xPos,
						       yPos,
						       cx,
						       cy,
						       nStride,
						       pData,
						       m_Back,
						       m_Fore
						       );
					}
				else {
					pGDI->CharBlt( xPos,
						       yPos,
						       cx,
						       cy,
						       nStride,
						       pData,
						       m_Fore
						       );
					}
				}
			else {
				pGDI->CharBlt( xPos,
					       yPos,
					       cx,
					       cy,
					       nStride,
					       pData
					       );
				}
			}
		else {
			if( m_fFill ) {

				pGDI->FillRect( x,
						y,
						x + MotorToHost(m_pWidth[n]),
						y + m_Size
						);
				}
			}

		x += MotorToHost(m_pWidth[n]);

		return;
		}

	if( c == '?' ) {

		// No question mark available so we
		// use a square for missing glyphs. 

		if( m_fFill ) {

			pGDI->FillRect( x,
					y,
					x + m_Size,
					y + m_Size
					);
			}

		pGDI->PushBrush();

		pGDI->SetBrushFore(m_Fore);

		pGDI->FillRect( x + 1,
				y + 1,
				x + m_Size - 2,
				y + m_Size - 2
				);

		pGDI->PullBrush();

		x += m_Size;

		return;
		}

	DrawGlyph(pGDI, x, y, '?');
	}

void CFontItem::BurstDone(IGDI *pGDI, CLogFont const &Font)
{
	if( m_fFill ) {

		pGDI->PullBrush();

		m_fFill = FALSE;
		}
	}

// Implementation

UINT CFontItem::FindGlyph(WCHAR cData)
{
	if( cData >= m_cFrom && cData <= m_cTo ) {

		int n1 = 0;
		
		int n2 = m_uCount - 1;

		for(;;) {
		
			int   np = (n1 + n2) / 2;
		
			WCHAR cp = MotorToHost(m_pList[np]);

			if( cp == cData ) {

				return np;
				}

			if( cp < cData ) {

				n1 = np + 1; 
				}
			else
				n2 = np - 1;
					
			if( n1 > n2 ) {

				break;
				}
			}
		}

	return NOTHING;
	}

BOOL CFontItem::IsFixed(WORD c)
{
	return IsFixedDigit(c) || IsFixedLetter(c);
	}

BOOL CFontItem::IsFixedDigit(WORD cData)
{
	if( cData >= digitFixed && cData < digitFixed + 10 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CFontItem::IsFixedLetter(WORD c)
{
	if( c >= letterFixed && c < letterFixed + 6 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CFontItem::IsSpace(WCHAR cData)
{
	switch( cData ) {

		case spaceNormal:
		case spaceNarrow:
		case spaceHair:
		case spaceNoBreak:
		case spaceFigure:

			return TRUE;
		}

	return FALSE;
	}

BOOL CFontItem::IsExplicitCode(WCHAR cData)
{
	switch( cData ) {

		case uniLRE:
		case uniRLE:
		case uniLRO:
		case uniRLO:
		case uniLRM:
		case uniRLM:
		case uniPDF:
		
			return TRUE;
		}

	return FALSE;
	}

// End of File
