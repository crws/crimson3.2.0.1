
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SLIDER_HPP
	
#define	INCLUDE_SLIDER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacySlider;
class CPrimLegacyVertSlider;
class CPrimLegacyHorzSlider;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Slider
//

class CPrimLegacySlider : public CPrimRich
{
	public:
		// Constructor
		CPrimLegacySlider(void);

		// Destructor
		~CPrimLegacySlider(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void LoadTouchMap(ITouchMap *pTouch);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		UINT         m_Mode;
		UINT	     m_Accel;
		CPrimPen   * m_pEdge;
		CPrimBrush * m_pKnob;
		CPrimColor * m_pBack;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			BOOL   m_fAvail;
			DWORD  m_Val;
			DWORD  m_Min;
			DWORD  m_Max;
			C3REAL m_Pos;
			COLOR  m_Back;
			BOOL   m_fEnable;
			BOOL   m_fPress1;
			BOOL   m_fPress2;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Data Members
		BOOL  m_fPress1;
		BOOL  m_fPress2;
		R2    m_Btn1;
		R2    m_Btn2;
		DWORD m_Step;
		UINT  m_uCount;
		UINT  m_uLevel;
		
		// Message Handlers
		UINT OnTakeFocus(UINT uMsg, UINT uParam);
		UINT OnTouchDown(UINT uParam);
		UINT OnTouchRepeat(void);
		UINT OnTouchUp(void);

		// Implementation
		void DrawBase(IGDI *pGDI, R2 &Rect);
		void DrawBtnFrame(IGDI *pGDI, R2 &Rect, BOOL fPress);
		void FindPoints(P2 *t, P2 *b, R2 &r, int s);
		void SetBtnColor(IGDI *pGDI);
		void StepData(void);
		BOOL SetFromPos(C3REAL Pos);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Slider
//

class CPrimLegacyVertSlider : public CPrimLegacySlider
{
	public:
		// Constructor
		CPrimLegacyVertSlider(void);

		// Destructor
		~CPrimLegacyVertSlider(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
		UINT OnMessage(UINT uMsg, UINT uParam);

	protected:
		// Data Members
		int m_yMin;
		int m_yMax;

		// Message Handlers
		UINT OnTouchDown(UINT uParam);

		// Implementation
		void DrawSlot(IGDI *pGDI, R2 &Rect);
		void DrawBtn1(IGDI *pGDI, R2 Rect);
		void DrawBtn2(IGDI *pGDI, R2 Rect);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Slider
//

class CPrimLegacyHorzSlider : public CPrimLegacySlider
{
	public:
		// Constructor
		CPrimLegacyHorzSlider(void);

		// Destructor
		~CPrimLegacyHorzSlider(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
		UINT OnMessage(UINT uMsg, UINT uParam);

	protected:
		// Data Members
		int m_xMin;
		int m_xMax;

		// Message Handlers
		UINT OnTouchDown(UINT uParam);

		// Implementation
		void DrawSlot(IGDI *pGDI, R2 &Rect);
		void DrawBtn1(IGDI *pGDI, R2 Rect);
		void DrawBtn2(IGDI *pGDI, R2 Rect);
	};

// End of File

#endif
