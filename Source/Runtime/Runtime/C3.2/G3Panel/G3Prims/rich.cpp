
#include "intern.hpp"

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Rich Legacy Base Class
//

// Constructor

CPrimRich::CPrimRich(void)
{
	m_pValue       = NULL;
	m_Entry        = 0;
	m_TagLimits    = 1;
	m_TagLabel     = 1;
	m_TagFormat    = 1;
	m_TagColor     = 0;
	m_pEnable      = NULL;
	m_pOnSetFocus  = NULL;
	m_pOnKillFocus = NULL;
	m_pOnComplete  = NULL;
	m_pLabel       = NULL;
	m_pLimitMin    = NULL;
	m_pLimitMax    = NULL;
	m_FormType     = 0;
	m_ColType      = 0;
	m_UseBack      = 1;
	m_pFormat      = NULL;
	m_pColor       = NULL;
	m_pTextColor   = New CPrimColor(naText);
	m_pTextShadow  = New CPrimColor(naNone);
	m_uTag         = 0;
	
	m_fFocus       = FALSE;

	m_uPress       = 0;
	}

// Destructor

CPrimRich::~CPrimRich(void)
{
	delete m_pValue;
	delete m_pEnable;
	delete m_pOnSetFocus;
	delete m_pOnKillFocus;
	delete m_pOnComplete;
	delete m_pLabel;
	delete m_pLimitMin;
	delete m_pLimitMax;
	delete m_pFormat;
	delete m_pColor;
	delete m_pTextColor;
	delete m_pTextShadow;
	}

// Initialization

void CPrimRich::Load(PCBYTE &pData)
{
	CPrim::Load(pData);

	GetCoded(pData, m_pValue);

	m_Entry = GetByte(pData);

	if( (m_uTag = GetWord(pData)) ) {

		m_TagLimits = GetByte(pData);
		m_TagLabel  = GetByte(pData);
		m_TagFormat = GetByte(pData);
		m_TagColor  = GetByte(pData);
		}

	if( m_Entry ) {

		GetCoded(pData, m_pEnable);
		GetCoded(pData, m_pOnSetFocus);
		GetCoded(pData, m_pOnKillFocus);
		GetCoded(pData, m_pOnComplete);
		}

	GetCoded(pData, m_pLabel);
	GetCoded(pData, m_pLimitMin);
	GetCoded(pData, m_pLimitMax);

	if( GetByte(pData) ) {

		m_pFormat = CDispFormat::MakeObject(GetByte(pData));

		if( m_pFormat ) {

			m_pFormat->Load(pData);
			}
		}

	if( GetByte(pData) ) {

		m_pColor = CDispColor::MakeObject(GetByte(pData));

		if( m_pColor ) {

			m_pColor->Load(pData);
			}
		}

	if( !m_pColor ) {
	
		m_pTextColor->Load(pData);
		
		m_pTextShadow->Load(pData);
		}

	m_UseBack = GetByte(pData);

	m_Font    = GetWord(pData);

	m_Content = GetByte(pData);

	m_Flash   = GetByte(pData);

	m_Margin.top      = 1;

	m_Margin.left     = 1;
	
	m_Margin.right    = 1;
	
	m_Margin.bottom   = 1;
	}

// Overridables

void CPrimRich::SetScan(UINT Code)
{
	CCodedText  *pLabel;

	CDispFormat *pFormat;
	
	CDispColor  *pColor;

	if( GetDataLabel(pLabel) ) {

		pLabel->SetScan(Code);
		}

	if( GetDataFormat(pFormat) ) {

		pFormat->SetScan(Code);
		}

	if( GetDataColor(pColor) ) {

		pColor->SetScan(Code);
		}

	SetItemScan(m_pValue,       Code);
	
	SetItemScan(m_pEnable,      Code);
	
	SetItemScan(m_pOnSetFocus,  Code);
	
	SetItemScan(m_pOnKillFocus, Code);
	
	SetItemScan(m_pOnComplete,  Code);
	
	SetItemScan(m_pLimitMin,    Code);
	
	SetItemScan(m_pLimitMax,    Code);
	
	SetItemScan(m_pTextColor,   Code);
	
	SetItemScan(m_pTextShadow,  Code);

	CPrim::SetScan(Code);
	}

void CPrimRich::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrim::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx            = Ctx;

			m_fChange        = TRUE;
			}
		}
	}

UINT CPrimRich::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgSetFocus:

			Execute(m_pOnSetFocus);

			break;

		case msgKillFocus:

			Execute(m_pOnKillFocus);

			break;
		}

	return CPrim::OnMessage(uMsg, uParam);
	}

// Attributes

BOOL CPrimRich::IsAvail(void) const
{
	CCodedText  *pLabel;

	CDispFormat *pFormat;
	
	CDispColor  *pColor;

	if( GetDataLabel(pLabel) ) {

		if( !pLabel->IsAvail() ) {

			return FALSE;
			}
		}

	if( GetDataFormat(pFormat) ) {

		if( !pFormat->IsAvail() ) {

			return FALSE;
			}
		}

	if( GetDataColor(pColor) ) {

		if( !pColor->IsAvail() ) {

			return FALSE;
			}
		}

	if( !IsItemAvail(m_pValue) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pEnable) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pOnSetFocus) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pOnKillFocus) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pOnComplete) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pLimitMin) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pLimitMax) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pTextColor) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pTextShadow) ) {

		return FALSE;
		}
	
	return TRUE;
	}

BOOL CPrimRich::IsTagRef(void) const
{
	return m_uTag ? TRUE : FALSE;
	}

BOOL CPrimRich::GetTagItem(CTag * &pTag) const
{
	if( IsTagRef() ) {

		CCommsSystem *pSystem = CCommsSystem::m_pThis;

		CTagList     *pTags   = pSystem->m_pTags->m_pTags;

		if( (pTag = pTags->GetItem(m_uTag - 1)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetTagLabel(CCodedText * &pLabel) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pLabel ) {

			pLabel = pTag->m_pLabel;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetTagFormat(CDispFormat * &pFormat) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pFormat ) {

			pFormat = pTag->m_pFormat;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetTagColor(CDispColor * &pColor) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pColor ) {

			pColor = pTag->m_pColor;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetDataLabel(CCodedText * &pLabel) const
{
	if( (pLabel = m_pLabel) ) {

		return TRUE;
		}

	if( m_TagLabel ) {
		
		if( GetTagLabel(pLabel) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetDataFormat(CDispFormat * &pFormat) const
{
	if( (pFormat = m_pFormat) ) {

		return TRUE;
		}

	if( m_TagFormat ) {
		
		if( GetTagFormat(pFormat) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetDataColor(CDispColor * &pColor) const
{
	if( (pColor = m_pColor) ) {

		return TRUE;
		}

	if( m_TagColor ) {
			
		if( GetTagColor(pColor) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Limit Access

DWORD CPrimRich::GetMinValue(UINT Type) const
{
	if( m_pValue && m_TagLimits ) {

		CTag *pTag;

		if( GetTagItem(pTag) ) {

			DWORD     n = m_pValue->Execute(typeLValue);

			CDataRef &r = (CDataRef &) n;

			return pTag->GetMinValue(r.x.m_Array, Type);
			}
		}

	if( m_pLimitMin ) {

		return m_pLimitMin->Execute(Type);
		}

	if( Type == typeReal ) {

		return R2I(0);
		}

	return 0;
	}

DWORD CPrimRich::GetMaxValue(UINT Type) const
{
	if( m_pValue && m_TagLimits ) {

		CTag *pTag;

		if( GetTagItem(pTag) ) {

			DWORD     n = m_pValue->Execute(typeLValue);

			CDataRef &r = (CDataRef &) n;

			return pTag->GetMaxValue(r.x.m_Array, Type);
			}
		}

	if( m_pLimitMax ) {

		return m_pLimitMax->Execute(Type);
		}

	if( Type == typeReal ) {

		return R2I(100);
		}

	return 100;
	}

// Context Creation

void CPrimRich::FindCtx(CCtx &Ctx)
{
	UINT  Type = typeVoid;

	DWORD Data = 0;

	if( m_pValue ) {

		if( IsAvail() ) {

			Type         = m_pValue->GetType();			

			Data         = m_pValue->Execute(Type);
			}
		}

	if( m_Content < 3 ) {

		if( m_Content > 0 ) {

			Ctx.m_Label = FindLabelText();
			}

		if( m_Content < 2 ) {

			Ctx.m_Value = FindValueText(Data, Type, fmtPad);
			}
		}

	CDispColor *pColor;

	if( GetDataColor(pColor) ) {

		DWORD Pair = pColor->GetColorPair(Data, Type);

		if( m_UseBack ) {

			Ctx.m_Fore1 = LOWORD(Pair);

			Ctx.m_Back1 = HIWORD(Pair);
			}
		else {
			Ctx.m_Fore1 = LOWORD(Pair);

			Ctx.m_Back1 = 0x8000;
			}
		
		if( m_fFocus ) {

			Ctx.m_Fore2 = HIWORD(Pair);

			Ctx.m_Back2 = LOWORD(Pair);
			}

		Ctx.m_Shadow = 0x8000;
		}
	else {
		if( m_fFocus ) {

			Ctx.m_Fore1  = m_pTextColor->GetColor();

			Ctx.m_Back1  = 0x8000;

			Ctx.m_Fore2  = FindCompColor(Ctx.m_Fore1);

			Ctx.m_Back2  = Ctx.m_Fore1;

			Ctx.m_Shadow = m_pTextShadow->GetColor();
			}
		else {
			Ctx.m_Fore1  = m_pTextColor->GetColor();

			Ctx.m_Back1  = 0x8000;

			Ctx.m_Shadow = m_pTextShadow->GetColor();
			}
		}

	if( !m_fFocus ) {

		Ctx.m_Fore2 = Ctx.m_Fore1;

		Ctx.m_Back2 = Ctx.m_Back1;
		}

	if( !Type ) {

		Ctx.m_Fore2 = naText;

		Ctx.m_Back2 = naNone;
		}

	if( !m_Entry ) {

		Ctx.m_fFocus = m_fFocus;

		Ctx.m_uPress = IsPressed();
		}
	else {
		Ctx.m_fFocus = m_fFocus;

		Ctx.m_uPress = m_uPress;
		}
	}

// Context Check

BOOL CPrimRich::CCtx::operator == (CCtx const &That) const
{
	if( !m_Label.CompareC(That.m_Label) ) {
		
		if( !m_Value.CompareC(That.m_Value) ) {
		
			return m_Fore1  == That.m_Fore1  &&
			       m_Fore2  == That.m_Fore2  &&
			       m_Back1  == That.m_Back1  &&
			       m_Back2  == That.m_Back2  &&
			       m_Shadow == That.m_Shadow &&
			       m_fFocus == That.m_fFocus &&
			       m_uPress == That.m_uPress ;;
			}
		}
	
	return FALSE;
	}

// Attributes

int CPrimRich::GetTextWidth(IGDI *pGDI, PCUTF pText)
{
	int nSize = pGDI->GetTextWidth(pText);

	nSize += m_Margin.top;

	nSize += m_Margin.bottom;

	return nSize;
	}

int CPrimRich::GetTextHeight(IGDI *pGDI, PCUTF pText)
{
	int nSize = pGDI->GetTextHeight(pText);

	nSize += m_Margin.left;

	nSize += m_Margin.right;

	return nSize;
	}

// Operations

BOOL CPrimRich::SelectFont(IGDI *pGDI)
{
	return SelectFont(pGDI, m_Font);
	}

BOOL CPrimRich::SelectFont(IGDI *pGDI, UINT Font)
{
	if( Font < 0x100 ) {

		pGDI->SelectFont(Font);

		return TRUE;
		}

	return CUISystem::m_pThis->m_pUI->m_pFonts->Select(pGDI, Font);
	}

// Text Extraction

CUnicode CPrimRich::FindLabelText(void)
{
	CCodedText *pLabel;

	if( GetDataLabel(pLabel) ) {

		DWORD n = 0;

		if( m_pValue ) {

			CDataRef &Ref = (CDataRef &) n;

			n = m_pValue->Execute(typeLValue);

			n = Ref.x.m_Array;
			}

		return pLabel->GetText(L"", &n);
		}
	else {
		CTag *pTag = NULL;

		if( GetTagItem(pTag) ) {

			CUnicode Name = pTag->m_Name;

			UINT     uPos = Name.FindRev(L'.');

			if( uPos < NOTHING ) {

				return Name.Mid(uPos + 1);
				}

			return Name;
			}
		}

	return L"Label";
	}

CUnicode CPrimRich::FindValueText(DWORD Data, UINT Type, UINT Flags)
{
	CDispFormat *pFormat;

	if( GetDataFormat(pFormat) ) {

		return pFormat->Format(Data, Type, Flags);
		}

	return CDispFormat::GeneralFormat(Data, Type, Flags);
	}

// Drawing

void CPrimRich::DrawLabel(IGDI *pGDI, R2 Rect)
{
	Rect.left   += m_Margin.left;

	Rect.right  -= m_Margin.right;
	
	Rect.top    += m_Margin.top;
	
	Rect.bottom -= m_Margin.bottom;

	int xPos = (Rect.x2 + Rect.x1 - pGDI->GetTextWidth(m_Ctx.m_Label)) / 2;

	int yPos = (Rect.y2 + Rect.y1 - pGDI->GetTextHeight(m_Ctx.m_Label)) / 2;

	pGDI->TextOut(xPos, yPos, UniVisual(m_Ctx.m_Label));
	}

void CPrimRich::DrawValue(IGDI *pGDI, R2 Rect)
{
	Rect.left   += m_Margin.left;

	Rect.right  -= m_Margin.right;
	
	Rect.top    += m_Margin.top;
	
	Rect.bottom -= m_Margin.bottom;

	int xPos = (Rect.x2 + Rect.x1 - pGDI->GetTextWidth(m_Ctx.m_Value)) / 2;

	int yPos = (Rect.y2 + Rect.y1 - pGDI->GetTextHeight(m_Ctx.m_Value)) / 2;

	if( !(m_Ctx.m_Shadow & 0x8000) ) {

		pGDI->SetTextFore(m_Ctx.m_Shadow);

		pGDI->TextOut(xPos + 1, yPos + 2, m_Ctx.m_Value);
		
		SetColors(pGDI, 2);
		}
	
	pGDI->TextOut(xPos, yPos, m_Ctx.m_Value);
	}

// Color Setting

void CPrimRich::SetColors(IGDI *pGDI, UINT uMode)
{
	switch( uMode ) {

		case 1:
			SetColors(pGDI, m_Ctx.m_Fore1, m_Ctx.m_Back1);
			break;

		case 2:
			SetColors(pGDI, m_Ctx.m_Fore2, m_Ctx.m_Back2);
			break;

		case 3:
			SetColors(pGDI, m_Ctx.m_Shadow, 0x8000);
			break;
		}
	}

void CPrimRich::SetColors(IGDI *pGDI, COLOR Fore, COLOR Back)
{
	pGDI->SetTextFore(Fore);

	if( Back >= 0x8000 ) {

		pGDI->SetTextTrans(modeTransparent);

		pGDI->SelectBrush(brushNull);
		}
	else {
		pGDI->SetTextBack(Back);

		pGDI->SetTextTrans(modeOpaque);

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(Back);
		}
	}

COLOR CPrimRich::FindCompColor(COLOR c)
{
	BYTE  r = GetRED  (c);
	BYTE  g = GetGREEN(c);
	BYTE  b = GetBLUE (c);

	BYTE  i = BYTE((14*r + 45*g + 5*b) / 64);

	if( i >= 15 ) {

		return GetRGB(0,0,0);
		}

	return GetRGB(31,31,31);
	}

// End of File
