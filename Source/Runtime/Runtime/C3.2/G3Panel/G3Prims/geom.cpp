
#include "intern.hpp"

#include "geom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Geometric Figure
//

// Constructor

CPrimGeom::CPrimGeom(void)
{
	m_pFill = New CPrimTankFill;

	m_pEdge = New CPrimPen;
	}

// Destructor

CPrimGeom::~CPrimGeom(void)
{
	delete m_pFill;

	delete m_pEdge;
	}

// Initialization

void CPrimGeom::Load(PCBYTE &pData)
{
	CPrimWithText::Load(pData);

	m_pFill->Load(pData);

	if( !m_pFill->IsNull() ) {

		m_fTrans = FALSE;
		}

	m_pEdge->Load(pData);
	}

// Overridables

void CPrimGeom::SetScan(UINT Code)
{
	m_pFill->SetScan(Code);

	m_pEdge->SetScan(Code);

	CPrimWithText::SetScan(Code);
	}

void CPrimGeom::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimWithText::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		if( m_pFill->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Rectangle
//

// Constructor

CPrimRect::CPrimRect(void)
{
	}

// Destructor

CPrimRect::~CPrimRect(void)
{
	}

// Initialization

void CPrimRect::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRect", pData);

	CPrimGeom::Load(pData);
	}

// Overridables

void CPrimRect::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillRect(pGDI, Rect);

	m_pEdge->DrawRect(pGDI, Rect);

	CPrimWithText::DrawPrim(pGDI);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Ellipse
//

// Constructor

CPrimEllipse::CPrimEllipse(void)
{
	}

// Destructor

CPrimEllipse::~CPrimEllipse(void)
{
	}

// Initialization

void CPrimEllipse::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimEllipse", pData);

	CPrimGeom::Load(pData);
	}

// Overridables

void CPrimEllipse::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillEllipse(pGDI, Rect, etWhole);

	m_pEdge->DrawEllipse(pGDI, Rect, etWhole);

	CPrimWithText::DrawPrim(pGDI);
	}

void CPrimEllipse::LoadTouchMap(ITouchMap *pTouch)
{
	pTouch->FillEllipse(PassRect(m_DrawRect), etWhole);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Wedge
//

// Constructor

CPrimWedge::CPrimWedge(void)
{
	m_Orient = 0;
	}

// Destructor

CPrimWedge::~CPrimWedge(void)
{
	m_Orient = 0;
	}

// Initialization

void CPrimWedge::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimWedge", pData);

	CPrimGeom::Load(pData);

	m_Orient = GetByte(pData);
	}

// Overridables

void CPrimWedge::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillWedge(pGDI, Rect, m_Orient + 1);

	m_pEdge->DrawWedge(pGDI, Rect, m_Orient + 1);

	CPrimWithText::DrawPrim(pGDI);
	}

void CPrimWedge::LoadTouchMap(ITouchMap *pTouch)
{
	pTouch->FillWedge(PassRect(m_DrawRect), m_Orient + 1);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Ellipse Quadrant
//

// Constructor

CPrimEllipseQuad::CPrimEllipseQuad(void)
{
	}

// Destructor

CPrimEllipseQuad::~CPrimEllipseQuad(void)
{
	}

// Initialization

void CPrimEllipseQuad::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimEllipseQuad", pData);

	CPrimGeom::Load(pData);

	m_Orient = GetByte(pData);
	}

// Overridables

void CPrimEllipseQuad::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillEllipse(pGDI, Rect, m_Orient + 1);

	m_pEdge->DrawEllipse(pGDI, Rect, m_Orient + 1);

	CPrimWithText::DrawPrim(pGDI);
	}

void CPrimEllipseQuad::LoadTouchMap(ITouchMap *pTouch)
{
	pTouch->FillEllipse(PassRect(m_DrawRect), m_Orient + 1);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Ellipse Half
//

// Constructor

CPrimEllipseHalf::CPrimEllipseHalf(void)
{
	m_Orient = 0;
	}

// Destructor

CPrimEllipseHalf::~CPrimEllipseHalf(void)
{
	}

// Initialization

void CPrimEllipseHalf::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimEllipseHalf", pData);

	CPrimGeom::Load(pData);

	m_Orient = GetByte(pData);
	}

// Overridables

void CPrimEllipseHalf::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillEllipse(pGDI, Rect, m_Orient + 5);

	m_pEdge->DrawEllipse(pGDI, Rect, m_Orient + 5);

	CPrimWithText::DrawPrim(pGDI);
	}

void CPrimEllipseHalf::LoadTouchMap(ITouchMap *pTouch)
{
	pTouch->FillEllipse(PassRect(m_DrawRect), m_Orient + 5);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Text Box
//

// Constructor

CPrimTextBox::CPrimTextBox(void)
{
	}

// Destructor

CPrimTextBox::~CPrimTextBox(void)
{
	}

// Initialization

void CPrimTextBox::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimTextBox", pData);

	CPrimGeom::Load(pData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Data Box
//

// Constructor

CPrimDataBox::CPrimDataBox(void)
{
	}

// Destructor

CPrimDataBox::~CPrimDataBox(void)
{
	}

// Initialization

void CPrimDataBox::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimDataBox", pData);

	CPrimGeom::Load(pData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Animatable Image
//

// Constructor

CPrimAnimImage::CPrimAnimImage(void)
{
	m_Count  = 1;

	m_pValue = NULL;

	m_pGray  = NULL;

	m_pShow  = NULL;

	m_fAny   = FALSE;

	memset(m_pImage, 0, sizeof(m_pImage));
	}

// Destructor

CPrimAnimImage::~CPrimAnimImage(void)
{
	delete m_pValue;

	delete m_pGray;

	delete m_pShow;

	while( m_Count-- ) {

		delete m_pImage[m_Count];
		}
	}

// Initialization

void CPrimAnimImage::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimAnimImage", pData);

	CPrimGeom::Load(pData);

	m_Count = GetByte(pData);

	for( UINT n = 0; n < m_Count; n++ ) {

		if( GetByte(pData) ) {

			m_pImage[n] = New CPrimImage;
		
			m_pImage[n]->Load(pData);

			m_fAny = TRUE;
			}
		}

	GetCoded(pData, m_pValue);

	GetCoded(pData, m_pGray);

	GetCoded(pData, m_pShow);
	}

// Overridables

void CPrimAnimImage::SetScan(UINT Code)
{
	SetItemScan(m_pValue, Code);

	SetItemScan(m_pGray,  Code);

	SetItemScan(m_pShow,  Code);

	CPrimWithText::SetScan(Code);
	}

void CPrimAnimImage::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRect::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_pFill->IsNull() ) {

			CPrimImage *pImage = m_pImage[m_Ctx.m_uImage];

			if( !pImage || pImage->IsTransparent(m_DrawRect) ) {

				if( pImage && pImage->IsAntiAliased() ) {

					(m_fChange ? Erase : Trans).Append(m_DrawRect);
					}
				else {
					if( m_fChange ) {

						Erase.Append(m_DrawRect);
						}
					}
				}
			}
		}
	}

void CPrimAnimImage::DrawPrim(IGDI *pGDI)
{
	if( TRUE ) {

		R2 Rect = m_DrawRect;

		m_pEdge->AdjustRect(Rect);

		m_pFill->FillRect(pGDI, Rect);

		m_pEdge->DrawRect(pGDI, Rect);
		}

	if( m_fAny ) {

		if( m_Ctx.m_fShow ) {

			CPrimImage *pImage = m_pImage[m_Ctx.m_uImage];

			if( pImage ) {

				R2   Rect = m_DrawRect;

				int  nAdj = 2 * m_pEdge->GetWidth();

				UINT rop  = m_Ctx.m_fColor ? 0 : ropDisable;

				DeflateRect(Rect, nAdj, nAdj);

				pImage->DrawItem(pGDI, Rect, rop);
				}
			}
		}
	else {
		pGDI->ResetFont();

		pGDI->SetTextTrans(modeTransparent);

		PCUTF pt = L"IMG";

		int   cx = pGDI->GetTextWidth(pt);

		int   cy = pGDI->GetTextHeight(pt);

		int   xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

		int   yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 2;

		pGDI->TextOut(xp, yp, pt);
		}

	CPrimWithText::DrawPrim(pGDI);
	}

// Context Creation

void CPrimAnimImage::FindCtx(CCtx &Ctx)
{
	Ctx.m_uImage = GetItemData(m_pValue, C3INT(0)) % m_Count;

	Ctx.m_fColor = GetItemData(m_pGray,  C3INT(1));
	
	Ctx.m_fShow  = GetItemData(m_pShow,  C3INT(1));
	}

// Context Check

BOOL CPrimAnimImage::CCtx::operator == (CCtx const &That) const
{
	return m_uImage == That.m_uImage &&
	       m_fColor == That.m_fColor &&
	       m_fShow  == That.m_fShow  ;
	}

// End of File
