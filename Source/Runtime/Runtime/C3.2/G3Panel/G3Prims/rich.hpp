
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RICH_HPP
	
#define	INCLUDE_RICH_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimRich;

//////////////////////////////////////////////////////////////////////////
//
// Rich Property Codes
//

enum {
	propEntry  = 0x01,
	propLimits = 0x02,
	propLabel  = 0x04,
	propFormat = 0x08,
	propColor  = 0x10,
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Rich Base Class
//

class CPrimRich : public CPrim
{
	public:
		// Constructor
		CPrimRich(void);

		// Denstructor
		~CPrimRich(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Attributes
		BOOL IsAvail(void) const;
		BOOL IsTagRef(void) const;
		BOOL GetTagItem(CTag * &pTag) const;
		BOOL GetTagLabel(CCodedText * &pLabel) const;
		BOOL GetTagFormat(CDispFormat * &pFormat) const;
		BOOL GetTagColor(CDispColor * &pColor) const;
		BOOL GetDataLabel(CCodedText * &pLabel) const;
		BOOL GetDataFormat(CDispFormat * &pFormat) const;
		BOOL GetDataColor(CDispColor * &pColor) const;

		// Limit Access
		DWORD GetMinValue(UINT Type) const;
		DWORD GetMaxValue(UINT Type) const;

		// Data Members
		CCodedItem *  m_pValue;
		UINT	      m_Entry;
		UINT	      m_TagLimits;
		UINT	      m_TagLabel;
		UINT	      m_TagFormat;
		UINT	      m_TagColor;
		CCodedItem  * m_pEnable;
		CCodedItem  * m_pOnSetFocus;
		CCodedItem  * m_pOnKillFocus;
		CCodedItem  * m_pOnComplete;
		CCodedText  * m_pLabel;
		CCodedItem  * m_pLimitMin;
		CCodedItem  * m_pLimitMax;
		UINT	      m_FormType;
		UINT	      m_ColType;
		UINT	      m_UseBack;
		CDispFormat * m_pFormat;
		CDispColor  * m_pColor;
		CPrimColor  * m_pTextColor;
		CPrimColor  * m_pTextShadow;
		UINT          m_Font;
		UINT          m_Content;
		UINT          m_Flash;
		R2            m_Margin;

	protected:

		// Context Record
		struct CCtx
		{
			// Data Members

			CUnicode m_Label;
			CUnicode m_Value;
			COLOR    m_Back1;
			COLOR    m_Back2;
			COLOR    m_Fore1;
			COLOR    m_Fore2;
			COLOR    m_Shadow;
			BOOL     m_fFocus;
			UINT     m_uPress;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Data Members
		UINT m_uTag;
		BOOL m_fFocus;
		UINT m_uPress;

		// Context Creation
		void FindCtx(CCtx &Ctx);

		// Attributes
		int GetTextWidth(IGDI *pGDI, PCUTF pText);
		int GetTextHeight(IGDI *pGDI, PCUTF pText);

		// Operations
		BOOL SelectFont(IGDI *pGDI);
		BOOL SelectFont(IGDI *pGDI, UINT Font);

		// Text Extraction
		CUnicode FindLabelText(void);
		CUnicode FindValueText(DWORD Data, UINT Type, UINT Flags);

		// Drawing
		void DrawLabel(IGDI *pGDI, R2 Rect);
		void DrawValue(IGDI *pGDI, R2 Rect);

		// Color Setting
		void SetColors(IGDI *pGDI, UINT uMode);
		void SetColors(IGDI *pGDI, COLOR Back, COLOR Fore);

		// Implementation
		COLOR FindCompColor(COLOR c);
	};

// End of File

#endif
