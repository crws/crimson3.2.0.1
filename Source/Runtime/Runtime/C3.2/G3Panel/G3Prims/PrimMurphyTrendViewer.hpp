
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimMurphyTrendViewer_HPP
	
#define	INCLUDE_PrimMurphyTrendViewer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "viewer.hpp"

#include "CustomDataCache.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- FW Murphy Trend Viewer
//

class CPrimMurphyTrendViewer : public CPrimViewer
{
	public:
		// Constructor
		CPrimMurphyTrendViewer(void);

		// Destructor
		~CPrimMurphyTrendViewer(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);

		// Data Members
		UINT	      m_Log;
		UINT	      m_Width;
		CCodedItem  * m_pPenMask;
		UINT	      m_PenWeight;
		UINT          m_ShowData;
		UINT          m_ShowCursor;
		UINT          m_DataBox;
		UINT	      m_FontTitle;
		UINT	      m_FontData;
		UINT	      m_GridTime;
		UINT	      m_GridMode;
		UINT	      m_GridMinor;
		UINT	      m_GridMajor;
		CCodedItem  * m_pGridMin;
		CCodedItem  * m_pGridMax;
		UINT	      m_Precise;
		CPrimColor  * m_pColTitle;
		CPrimColor  * m_pColLabel;
		CPrimColor  * m_pColData;
		CPrimColor  * m_pColMajor;
		CPrimColor  * m_pColMinor;
		CPrimColor  * m_pColCursor;
		CPrimColor  * m_pColPen[16];
		CCodedText  * m_pBtnPgLeft;
		CCodedText  * m_pBtnLeft;
		CCodedText  * m_pBtnLive;
		CCodedText  * m_pBtnRight;
		CCodedText  * m_pBtnPgRight;
		CCodedText  * m_pBtnIn;
		CCodedText  * m_pBtnOut;
		CCodedText  * m_pBtnLoad;
		CDispFormat * m_pFormat;
		UINT	      m_fUseLoad;
		CCodedText  * m_pRoot;
		CCodedText  * m_pFile;
		CCodedItem  * m_pZoomMax;
		CCodedItem  * m_pZoomMin;
		CCodedItem  * m_pZoomInit;
		CCodedItem  * m_pPerLane;
		CCodedItem  * m_pTimeStart;
		CCodedItem  * m_pTimeEnd;

	protected:
		// Buttons
		enum {
			btnPageLeft	= 0,
			btnStepLeft	= 1,
			btnLive		= 2,
			btnStepRight	= 3,
			btnPageRight	= 4,
			btnZoomIn	= 5,
			btnZoomOut	= 6,
			btnLoad		= 7,
			};

		// Context Record
		struct CCtx
		{
			// Data Members
			UINT     m_uScale;
			DWORD    m_dwCursor;
			DWORD    m_dwTime;
			DWORD    m_dwLeft;
			DWORD    m_dwWide;
			BOOL     m_fLive;
			COLOR    m_ColTitle;
			COLOR    m_ColLabel;
			COLOR    m_ColData;
			COLOR    m_ColMajor;
			COLOR    m_ColMinor;
			COLOR    m_ColCursor;
			C3REAL   m_GridMin;
			C3REAL   m_GridMax;
			DWORD    m_dwLineMask;
			UINT     m_uPerLane;
			BOOL     m_fMemory;
			BOOL     m_fLoad;
			CUnicode m_File;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Scale Record
		struct CScale
		{
			DWORD m_dwWidth;
			DWORD m_dwStep0;
			DWORD m_dwStep1;
			};

		// Static Data
		static CScale const m_Scale[];

		// Lane Record
		struct CLane {

			int yTop;
			int yBottom;
			};

		// Lane
		CLane * m_pLane;

		// Core Layout
		BOOL m_fInit;
		int  m_xm;
		int  m_x1;
		int  m_x2;
		int  m_y1;
		int  m_y2;

		// Data Layout
		P2  m_Inits[32];

		// Grid Layout
		int    m_yGap;
		double m_Limit;
		double m_GridSpan;
		double m_DrawStep;
		int    m_StepMin;
		int    m_StepMax;
		int    m_StepSpan;
		C3REAL m_DrawMin;
		C3REAL m_DrawMax;

		// Data Members
		CCtx   m_Ctx;
		UINT   m_uScale;
		BOOL   m_fLive;
		BOOL   m_fMemory;
		DWORD  m_dwTime;
		DWORD  m_dwLast;
		DWORD  m_dwCursor;
		BOOL   m_fLoad;
		PDWORD m_pShowData;
		BOOL   m_fShowData;
		UINT   m_uZoomInit;
		UINT   m_uZoomMin;
		UINT   m_uZoomMax;

		CCustomDataCache * m_pCache;

		// Event Hooks
		BOOL OnMakeList(void);
		BOOL OnEnable(void);
		BOOL OnBtnDown(UINT n);
		BOOL OnBtnRepeat(UINT n);
		BOOL OnBtnUp(UINT n);
		BOOL OnTouchWork(P2 Pos);

		// Navigation
		BOOL HasDataToLeft(void);
		void StepNeg(DWORD s);
		void StepPos(DWORD s);

		// Grid Drawing
		void DrawGrid(IGDI *pGDI);
		void DrawGridData(IGDI *pGDI, UINT uFlags);
		void DrawGridManual(IGDI *pGDI, UINT uFlags);
		void DrawGridAuto(IGDI *pGDI, UINT uFlags);
		void DrawGridTime(IGDI *pGDI);
		int  HorzLine(int y1, int y2, int yp, int yc);
		int  HorzLine(int y1, int y2, double f);

		// Info Drawing
		void DrawInfo(IGDI *pGDI);
		void DrawInfoLeft(IGDI *pGDI);
		void DrawInfoCenter(IGDI *pGDI);
		void DrawInfoRight(IGDI *pGDI);
		void DrawInfo(IGDI *pGDI, int j, int r, CString  const &Text);
		void DrawInfo(IGDI *pGDI, int j, int r, CUnicode const &Text);
		void FormatWidth(CString &Text, DWORD dwTime);

		// Data Plotting
		void DrawPlot(IGDI *pGDI);
		int  ScaleData(UINT uChan, DWORD Data);
		void SetTextColor(IGDI *pGDI, UINT uChan);
		void SetPenColor(IGDI *pGDI, UINT uChan);

		// Data Values
		void DrawData(IGDI *pGDI);

		// Cursor Drawing
		BOOL  DrawCursor(IGDI *pGDI);
		DWORD PosToTime(int xPos);
		int   TimeToPos(DWORD dwTime);

		// Layout Helpers
		void LayoutCore(IGDI *pGDI);
		BOOL LayoutData(IGDI *pGDI);
		void LayoutGrid(IGDI *pGDI);

		// Implementation
		double FindStep(double Range, double Limit);
		void   FindDrawLimits(void);
		BOOL   CanShowType(UINT Type);
		void   WriteTime(CCodedItem *pTime, DWORD dwTime);
		void   MakeShowData(void);
		void   LayoutLanes(void);

		// Context Handling
		void FindCtx(CCtx &Ctx);
		void ClearCtx(CCtx &Ctx);
		};

// End of File

#endif
