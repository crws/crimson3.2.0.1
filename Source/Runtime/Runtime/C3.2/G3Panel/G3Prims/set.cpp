
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Group Primitive
//

// Constructor

CPrimSet::CPrimSet(void)
{
	m_pList   = New CPrimList;

	m_fChange = FALSE;
	}

// Destructor

CPrimSet::~CPrimSet(void)
{
	delete m_pList;
	}

// Initialization

void CPrimSet::Load(PCBYTE &pData)
{
	CPrim::Load(pData);

	m_pList->Load(pData, this);
	}

// Overridables

void CPrimSet::SetScan(UINT Code)
{
	UINT c = m_pList->m_uCount;

	for( UINT n = 0; n < c; n++ ) {

		CPrim *pPrim = m_pList->m_ppPrim[n];

		pPrim->SetScan(Code);
		}

	CPrim::SetScan(Code);
	}

void CPrimSet::MovePrim(int cx, int cy)
{
	MoveList(cx, cy);

	CPrim::MovePrim(cx, cy);
	}

void CPrimSet::DrawExec(IGDI *pGDI, IRegion *pDirty)
{
	if( m_fShow ) {

		if( m_fChange ) {

			pDirty->AddRect(m_DrawRect);

			m_fChange = FALSE;
			}

		CUIDataServer * pData = CUISystem::m_pThis->m_pUserServer;

		CPrim         * pPrev = pData->GetPrim();
		
		UINT c = m_pList->m_uCount;

		for( UINT n = 0; n < c; n++ ) {

			CPrim *pPrim = m_pList->m_ppPrim[n];

			pData->SetPrim(pPrim);

			pPrim->DrawExec(pGDI, pDirty);
			}

		pData->SetPrim(pPrev);
		}
	}

UINT CPrimSet::OnMessage(UINT uMsg, UINT uParam)
{
	if( uMsg == msgGoInvisible ) {

		UINT c = m_pList->m_uCount;

		for( UINT n = 0; n < c; n++ ) {

			CPrim *pPrim = m_pList->m_ppPrim[n];

			pPrim->OnMessage(msgGoInvisible, 0);
			}

		return TRUE;
		}

	return CPrim::OnMessage(uMsg, uParam);
	}

R2 CPrimSet::GetBackRect(void)
{
	R2 Back = CPrim::GetBackRect();

	UINT c = m_pList->m_uCount;

	for( UINT n = 0; n < c; n++ ) {

		CPrim *pPrim = m_pList->m_ppPrim[n];

		R2 Rect = pPrim->GetBackRect();

		MakeMin(Back.left,   Rect.left);
		MakeMin(Back.top,    Rect.top);
		MakeMax(Back.right,  Rect.right);
		MakeMax(Back.bottom, Rect.bottom);
		}

	return Back;
	}
	
// Implementation

void CPrimSet::MoveList(int cx, int cy)
{
	UINT c = m_pList->m_uCount;

	for( UINT n = 0; n < c; n++ ) {

		CPrim *pPrim = m_pList->m_ppPrim[n];

		pPrim->MovePrim(cx, cy);
		}
	}

void CPrimSet::PrepList(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CUIDataServer * pData = CUISystem::m_pThis->m_pUserServer;

	CPrim         * pPrev = pData->GetPrim();
	
	UINT c = m_pList->m_uCount;

	for( UINT n = 0; n < c; n++ ) {

		CPrim *pPrim = m_pList->m_ppPrim[c-1-n];

		pData->SetPrim(pPrim);

		pPrim->DrawPrep(pGDI, Erase, Trans);
		}

	pData->SetPrim(pPrev);
	}

// End of File
