
#include "intern.hpp"

#include "PrimRubyGaugeTypeA.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Gauge Primitive
//

// Constructor

CPrimRubyGaugeTypeA::CPrimRubyGaugeTypeA(void)
{
	}

// Destructor

CPrimRubyGaugeTypeA::~CPrimRubyGaugeTypeA(void)
{
	}

// Initialization

void CPrimRubyGaugeTypeA::Load(PCBYTE &pData)
{
	CPrimRubyGaugeBase::Load(pData);

	m_Color[0] = GetWord(pData);
	m_Color[1] = GetWord(pData);
	m_Color[2] = GetWord(pData);
	m_Color[3] = GetWord(pData);
	m_Color[4] = GetWord(pData);
	m_Color[5] = GetWord(pData);

	LoadList(pData, m_listFace);
	LoadList(pData, m_listOuter);
	LoadList(pData, m_listInner);
	LoadList(pData, m_listRing1);
	LoadList(pData, m_listRing2);
	LoadList(pData, m_listRing3);
	LoadList(pData, m_listRing4);
			
	if( !IsNaked() ) {

		m_m2.AddTranslation(-m_bound.x1, -m_bound.y1);

		m_listFace .Translate(-m_bound.x1, -m_bound.y1, true);
		m_listOuter.Translate(-m_bound.x1, -m_bound.y1, true);
		m_listInner.Translate(-m_bound.x1, -m_bound.y1, true);
		m_listRing1.Translate(-m_bound.x1, -m_bound.y1, true);
		m_listRing2.Translate(-m_bound.x1, -m_bound.y1, true);
		m_listRing3.Translate(-m_bound.x1, -m_bound.y1, true);
		m_listRing4.Translate(-m_bound.x1, -m_bound.y1, true);
		}
	}

// Overridables

void CPrimRubyGaugeTypeA::MovePrim(int cx, int cy)
{
	if( IsNaked() ) {

		m_m2.AddTranslation(cx, cy);
		}

	CPrimRubyGaugeBase::MovePrim(cx, cy);
	}

// Drawing

void CPrimRubyGaugeTypeA::DrawBezel(IGDI *pGDI)
{
	CRubyGdiLink gdi(pGDI);

	if( !IsNaked() ) {

		if( GetColor(1) & 0x8000 ) {

			gdi.OutputSolid(m_listFace,   GetColor(0),  0);
			gdi.OutputShade(m_listInner,  ShaderDark,   0);
			gdi.OutputShade(m_listOuter,  ShaderChrome, 0);
			}
		else {
			gdi.OutputSolid(m_listFace,   GetColor(0), 0);
			gdi.OutputShade(m_listInner,  ShaderDark,  0);
			gdi.OutputSolid(m_listOuter,  GetColor(1), 0);
			}

		gdi.OutputSolid(m_listRing1,  GetColor(2), 0);
		gdi.OutputSolid(m_listRing2,  GetColor(3), 0);
		gdi.OutputSolid(m_listRing3,  GetColor(4), 0);
		gdi.OutputSolid(m_listRing4,  GetColor(5), 0);
		}
	}

// Color Schemes

COLOR CPrimRubyGaugeTypeA::GetColor(UINT n)
{
	return m_Color[n];
	}

BOOL CPrimRubyGaugeTypeA::IsNaked(void)
{
	return m_Color[0] == 0x8000;
	}

// End of File
