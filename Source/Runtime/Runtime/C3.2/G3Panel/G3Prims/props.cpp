
#include "intern.hpp"

#include "../g3comms/secure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Page Properties
//

// Constructor

CDispPageProps::CDispPageProps(void)
{
	m_pLabel     = NULL;
	m_pSec       = New CSecDesc;
	m_pBack      = New CPrimColor(GetRGB(0,0,0));
	m_Master     = 0;
	m_pOnSelect  = NULL;
	m_pOnRemove  = NULL;
	m_pOnUpdate  = NULL;
	m_pOnSecond  = NULL;
	m_pOnTimeout = NULL;
	m_PageNext   = 0;
	m_PagePrev   = 0;
	m_PageExit   = 0;
	m_AutoEntry  = 0;
	m_EntryOrder = 0;
	m_NoBack     = 0;
	m_Timeout    = 0;
	m_Update     = 0;
	m_PopAlign   = 0;
	m_PopAlignH  = 2;
	m_PopAlignV  = 2;
	m_PopMaster  = 1;
	m_PopKeypad  = 0;
	}

// Destructor

CDispPageProps::~CDispPageProps(void)
{
	delete m_pLabel;

	delete m_pSec;

	delete m_pBack;

	delete m_pOnSelect;
	
	delete m_pOnRemove;
	
	delete m_pOnUpdate;
	
	delete m_pOnSecond;
	
	delete m_pOnTimeout;
	}

// Initialization

void CDispPageProps::Load(PCBYTE &pData)
{
	ValidateLoad("CDispPageProps", pData);

	GetCoded(pData, m_pLabel);

	m_Desc = GetWide(pData);

	m_pSec->Load(pData);

	m_pBack->Load(pData);

	m_Master = GetWord(pData);

	GetCoded(pData, m_pOnSelect);
	GetCoded(pData, m_pOnRemove);
	GetCoded(pData, m_pOnUpdate);
	GetCoded(pData, m_pOnSecond);
	GetCoded(pData, m_pOnTimeout);

	m_PageNext = GetWord(pData);
	m_PagePrev = GetWord(pData);
	m_PageExit = GetWord(pData);

	m_AutoEntry  = GetByte(pData);
	m_EntryOrder = GetByte(pData);
	m_NoBack     = GetByte(pData);
	m_Timeout    = GetWord(pData);
	m_Update     = GetByte(pData);
	m_PopAlign   = GetByte(pData);
	m_PopAlignH  = GetByte(pData);
	m_PopAlignV  = GetByte(pData);
	m_PopMaster  = GetByte(pData);
	m_PopKeypad  = GetByte(pData);
	}

void CDispPageProps::Fast(PCBYTE &pData)
{
	ValidateLoad("CDispPageProps", pData);

	GetCoded(pData, m_pLabel);

	m_Desc = GetWide(pData);

	m_pSec->Load(pData);
	}

// Operations

void CDispPageProps::SetScan(UINT Code)
{
	SetItemScan(m_pLabel,     Code);
	SetItemScan(m_pOnRemove,  Code);
	SetItemScan(m_pOnSelect,  Code);
	SetItemScan(m_pOnRemove,  Code);
	SetItemScan(m_pOnUpdate,  Code);
	SetItemScan(m_pOnSecond,  Code);
	SetItemScan(m_pOnTimeout, Code);

	m_pBack->SetScan(Code);
	}

// End of File
