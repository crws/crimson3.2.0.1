
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_FILE_HPP
	
#define	INCLUDE_FILE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "viewer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- File Viewer
//

class CPrimFileViewer : public CPrimListViewer
{
	public:
		// Constructor
		CPrimFileViewer(void);

		// Destructor
		~CPrimFileViewer(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		CCodedText * m_pRoot;
		UINT	     m_Number;
		UINT         m_Sort;
		UINT	     m_IncCSV;
		UINT	     m_IncTXT;
		UINT	     m_IncLOG;
		CCodedText * m_pTxtEmpty;
		CCodedText * m_pTxtCannot;
		CCodedText * m_pTxtEnd;
		CCodedText * m_pBtnDown;
		CCodedText * m_pBtnUp;
		CCodedText * m_pBtnPrev;
		CCodedText * m_pBtnNext;
		CCodedText * m_pBtnScan;
		CCodedText * m_pBtnLeft;
		CCodedText * m_pBtnRight;
		UINT         m_ShowScan;
		UINT         m_ShowLeft;

	protected:
		// Data Members
		BOOL	        m_fInit;
		UINT	        m_uSeq;
		UINT	        m_uList;
		CFilename     * m_pList;
		CString		m_Root;
		BOOL	        m_fLoad;
		UINT	        m_uFile;
		DWORD	        m_dwExtent;
		int	        m_nWidth;
		int	        m_nSpace;
		int	        m_nLeft;
		UINT	        m_uData;
		CString       * m_pData;
		PDWORD	        m_pPos;
		INT	        m_nTop;
		BOOL	        m_fCSV;
		BOOL	        m_fEnd;

		// Event Hooks
		BOOL OnMakeList(void);
		BOOL OnEnable(void);
		BOOL OnBtnDown(UINT n);
		BOOL OnBtnRepeat(UINT n);
		BOOL OnBtnUp(UINT n);

		// List Hooks
		UINT ListGetSequence(void);
		void ListGetData(int nTop);
		void ListDrawHead(IGDI *pGDI, int yp);
		void ListDrawEmpty(IGDI *pGDI, int yp);
		void ListDrawStart(IGDI *pGDI, int nTop);
		void ListDrawRow(IGDI *pGDI, int nRow, int yp, BOOL fSelect);
		void ListDrawEnd(IGDI *pGDI);

		// File Handling
		void LoadList(void);
		void LoadFile(void);
		void LoadNextLine(void);
		void LoadPrevLine(void);
		UINT LineCount(CAutoFile &File);
		void LoadData(CAutoFile &File, UINT uLine);
		BOOL IsBreak(BYTE bData);
		BOOL IsEndOfFile(BYTE bData);
		BOOL FileChanged(void);
		BOOL ViewChanged(void);
		BOOL IsPrint(BYTE bData);
	};

// Instantiator

extern CPrim * Create_PrimFileViewer(void);

// End of File

#endif
