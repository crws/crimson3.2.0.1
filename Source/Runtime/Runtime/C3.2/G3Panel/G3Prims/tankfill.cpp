
#include "intern.hpp"

#include <float.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Brush
//

// Constants

#define INVALID (-FLT_MAX)

// Static Data

COLOR  CPrimTankFill::m_ShadeCol3 = 0;

C3REAL CPrimTankFill::m_ShadeData = 0;

// Constructor

CPrimTankFill::CPrimTankFill(void)
{
	m_Mode    = 0;

	m_pValue  = NULL;

	m_pMin    = NULL;

	m_pMax    = NULL;

	m_pColor3 = New CPrimColor(naFeature);
	}

// Destructor

CPrimTankFill::~CPrimTankFill(void)
{
	delete m_pValue;

	delete m_pMin;

	delete m_pMax;

	delete m_pColor3;
	}

// Initialization

void CPrimTankFill::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimTankFill", pData);

	CPrimBrush::DoLoad(pData);

	if( (m_Mode = GetByte(pData)) ) {

		GetCoded(pData, m_pValue);

		GetCoded(pData, m_pMin);

		GetCoded(pData, m_pMax);

		m_pColor3->Load(pData);
		}
	}

// Operations

void CPrimTankFill::SetScan(UINT Code)
{
	SetItemScan(m_pValue,  Code);

	SetItemScan(m_pMin,    Code);
	
	SetItemScan(m_pMax,    Code);
	
	SetItemScan(m_pColor3, Code);

	CPrimBrush::SetScan(Code);
	}

BOOL CPrimTankFill::DrawPrep(IGDI *pGDI)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_Ctx = Ctx;

		CPrimBrush::DrawPrep(pGDI);

		return TRUE;
		}

	return CPrimBrush::DrawPrep(pGDI);
	}

// Drawing

void CPrimTankFill::FillRect(IGDI *pGDI, R2 Rect)
{
	if( m_Mode ) {

		if( PrepTankFill() ) {
			
			CPrimBrush::FillRect(pGDI, Rect);

			pGDI->SetBrushStyle(brushFore);

			pGDI->SetBrushFore(m_Ctx.m_Color3);

			switch( m_Mode ) {

				case 1:
					Rect.y2 -= Round(m_Ctx.m_Data * (Rect.y2 - Rect.y1));
					break;

				case 2:
					Rect.y1 += Round(m_Ctx.m_Data * (Rect.y2 - Rect.y1));
					break;

				case 3:
					Rect.x1 += Round(m_Ctx.m_Data * (Rect.x2 - Rect.x1));
					break;

				case 4:
					Rect.x2 -= Round(m_Ctx.m_Data * (Rect.x2 - Rect.x1));
					break;
				}

			pGDI->FillRect(PassRect(Rect));
			}
		else {
			pGDI->SetBrushStyle(brushFore);

			pGDI->SetBrushFore(m_Ctx.m_Color3);

			pGDI->FillRect(PassRect(Rect));
			}

		return;
		}

	CPrimBrush::FillRect(pGDI, Rect);
	}

void CPrimTankFill::FillEllipse(IGDI *pGDI, R2 Rect, UINT uType)
{
	if( m_Mode ) {

		CPrimBrush::FillEllipse(pGDI, Rect, uType);

		if( PrepTankFill() ) {
			
			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeEllipse(PassRect(Rect), uType, Shader);
			}
		else {
			pGDI->SetBrushStyle(brushFore);

			pGDI->SetBrushFore(m_Ctx.m_Color3);

			pGDI->FillEllipse(PassRect(Rect), uType);
			}

		return;
		}

	CPrimBrush::FillEllipse(pGDI, Rect, uType);
	}

void CPrimTankFill::FillWedge(IGDI *pGDI, R2 Rect, UINT uType)
{
	if( m_Mode ) {

		CPrimBrush::FillWedge(pGDI, Rect, uType);

		if( PrepTankFill() ) {
			
			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeWedge(PassRect(Rect), uType, Shader);
			}
		else {
			pGDI->SetBrushStyle(brushFore);

			pGDI->SetBrushFore(m_Ctx.m_Color3);

			pGDI->FillWedge(PassRect(Rect), uType);
			}

		return;
		}

	CPrimBrush::FillWedge(pGDI, Rect, uType);
	}

void CPrimTankFill::FillPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound)
{
	if( m_Mode ) {

		CPrimBrush::FillPolygon(pGDI, pList, uCount, dwRound);

		if( PrepTankFill() ) {
			
			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadePolygon(pList, uCount, dwRound, Shader);
			}
		else {
			pGDI->SetBrushStyle(brushFore);

			pGDI->SetBrushFore(m_Ctx.m_Color3);

			pGDI->FillPolygon(pList, uCount, dwRound);
			}

		return;
		}

	CPrimBrush::FillPolygon(pGDI, pList, uCount, dwRound);
	}

// Shaders

BOOL CPrimTankFill::Shader(IGDI *pGDI, int p, int c)
{
	static int nTrip = 0;

	static int nMode = 0;

	if( c ) {

		if( p == 0 ) {

			nTrip = Round(c * m_ShadeData);

			if( m_ShadeMode == 1 || m_ShadeMode == 4 ) {

				nTrip = c - nTrip;

				nMode = 0;
				}

			if( m_ShadeMode == 2 || m_ShadeMode == 3 ) {

				nMode = 1;
				}

			if( nTrip ) {

				if( nMode == 0 ) {

					pGDI->SetBrushStyle(brushFore);
		
					pGDI->SetBrushFore(m_ShadeCol3);
					}

				if( nMode == 1 ) {

					pGDI->SetBrushStyle(brushNull);
					}

				return TRUE;
				}
			}

		if( p >= nTrip ) {

			if( nMode < 2 ) {

				if( nMode == 0 ) {
	
					pGDI->SetBrushStyle(brushNull);
					}

				if( nMode == 1 ) {

					pGDI->SetBrushStyle(brushFore);

					pGDI->SetBrushFore(m_ShadeCol3);
					}

				nMode = 2;

				return TRUE;
				}
			}

		return FALSE;
		}

	if( m_ShadeMode == 1 || m_ShadeMode == 2 ) {

		return FALSE;
		}

	return TRUE;
	}

// Rounding

C3INT CPrimTankFill::Round(C3REAL Data)
{
	return int(Data + 0.5);
	}

// Implementation

BOOL CPrimTankFill::PrepTankFill(void)
{
	if( m_Ctx.m_Data != INVALID ) {

		m_ShadeData = m_Ctx.m_Data;
		
		m_ShadeMode = m_Mode;

		m_ShadeCol3 = m_Ctx.m_Color3;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimTankFill::IsValueAvail(void)
{
	return IsItemAvail(m_pValue) &&
	       IsItemAvail(m_pMin  ) &&
	       IsItemAvail(m_pMax  ) ;
	}

// Context Creation

void CPrimTankFill::FindCtx(CCtx &Ctx)
{
	if( m_Mode ) {

		if( IsValueAvail() ) {

			C3REAL Min = GetItemData(m_pMin, C3REAL(  0));

			C3REAL Max = GetItemData(m_pMax, C3REAL(100));

			if( Min != Max ) {

				C3REAL Value = GetItemData(m_pValue, C3REAL(25));

				Ctx.m_Data   = (Value - Min) / (Max - Min);

				Ctx.m_Color3 = m_pColor3->GetColor();

				MakeMax(Ctx.m_Data, 0.0);

				MakeMin(Ctx.m_Data, 1.0);

				return;
				}
			}

		Ctx.m_Color3 = m_pColor3->GetColor();

		Ctx.m_Data   = INVALID;
		}
	else {
		Ctx.m_Color3 = 0;

		Ctx.m_Data   = INVALID;
		}
	}

// Context Check

BOOL CPrimTankFill::CCtx::operator == (CCtx const &That) const
{
	return m_Color3 == That.m_Color3 &&
	       m_Data   == That.m_Data   ;
	}

// End of File
