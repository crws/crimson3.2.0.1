
#include "intern.hpp"

#include "file.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- File Viewer
//

// Constructor

CPrimFileViewer::CPrimFileViewer(void)
{
	m_pRoot      = NULL;
	m_Number     = 1;
	m_Sort       = 1;
	m_IncCSV     = 1;
	m_IncTXT     = 0;
	m_IncLOG     = 0;
	m_pTxtEmpty  = NULL;
	m_pTxtCannot = NULL;
	m_pTxtEnd    = NULL;
	m_pBtnDown   = NULL;
	m_pBtnUp     = NULL;
	m_pBtnPrev   = NULL;
	m_pBtnNext   = NULL;
	m_pBtnScan   = NULL;
	m_pBtnLeft   = NULL;
	m_pBtnRight  = NULL;
	m_ShowScan   = 1;
	m_ShowLeft   = 1;
	m_fHead      = TRUE;
	m_uSeq       = 1;
	m_fInit      = FALSE;
	m_uList      = 0;
	m_pList      = NULL;
	m_pData      = NULL;
	m_pPos       = NULL;
	m_fLoad      = FALSE;
	}

// Destructor

CPrimFileViewer::~CPrimFileViewer(void)
{
	delete m_pRoot;
	delete m_pTxtEmpty;
	delete m_pTxtCannot;
	delete m_pTxtEnd;
	delete m_pBtnDown;
	delete m_pBtnUp;
	delete m_pBtnPrev;
	delete m_pBtnNext;
	delete m_pBtnScan;
	delete m_pBtnLeft;
	delete m_pBtnRight;

	delete [] m_pList;
	delete [] m_pData;
	delete [] m_pPos;
	}

// Initialization

void CPrimFileViewer::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimFileViewer", pData);

	CPrimViewer::Load(pData);

	GetWord (pData);

	GetCoded(pData, m_pRoot);

	m_FontText = GetWord(pData);

	m_Number   = GetByte(pData);
	m_Sort     = GetByte(pData);
	
	m_IncCSV   = GetByte(pData);
	m_IncTXT   = GetByte(pData);
	m_IncLOG   = GetByte(pData);

	GetCoded(pData, m_pTxtEmpty);
	GetCoded(pData, m_pTxtCannot);
	GetCoded(pData, m_pTxtEnd);

	GetCoded(pData, m_pBtnDown);
	GetCoded(pData, m_pBtnUp);
	GetCoded(pData, m_pBtnPrev);
	GetCoded(pData, m_pBtnNext);
	GetCoded(pData, m_pBtnScan);
	GetCoded(pData, m_pBtnLeft);
	GetCoded(pData, m_pBtnRight);

	m_ShowScan = GetByte(pData);
	m_ShowLeft = GetByte(pData);
	}

// Overridables

void CPrimFileViewer::SetScan(UINT Code)
{
	SetItemScan(m_pTxtEmpty,  Code);
	SetItemScan(m_pTxtCannot, Code);
	SetItemScan(m_pTxtEnd,    Code);

	SetItemScan(m_pBtnDown,   Code);
	SetItemScan(m_pBtnUp,     Code);
	SetItemScan(m_pBtnPrev,   Code);
	SetItemScan(m_pBtnNext,   Code);
	SetItemScan(m_pBtnScan,   Code);
	SetItemScan(m_pBtnLeft,   Code);
	SetItemScan(m_pBtnRight,  Code);

	CPrimViewer::SetScan(Code);
	}

void CPrimFileViewer::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( !m_fInit ) {

		if( TRUE ) {

			SelectFont(pGDI, m_FontText);

			int fx   = pGDI->GetTextWidth("X");

			int cx   = m_DrawRect.x2 - m_DrawRect.x1 - 2;

			m_nSpace = cx / fx - (m_Number ? 6 : 0);
			}

		CPrimViewer::DrawPrep(pGDI, Erase, Trans);

		CPrimListViewer::FindLayout(pGDI);

		CPrimListViewer::MakeButtonLists();

		CPrimListViewer::m_fInit = TRUE;

		m_uData = m_nRows;

		m_pData = New CString [ m_uData + 1 ];

		m_pPos  = New DWORD   [ m_uData + 1 ];
		
		LoadList();

		m_fLoad = TRUE;

		m_fInit = TRUE;
		}

	if( FileChanged() ) {

		m_fLoad = TRUE;

		m_uSeq++;
		}
	else {
		if( m_fLoad ) {

			m_nNewPos = -1;

			m_nNewTop =  0;

			m_uSeq++;
			}
		}

	CPrimListViewer::DrawPrep(pGDI, Erase, Trans);
	}

UINT CPrimFileViewer::OnMessage(UINT uMsg, UINT uParam)
{
	if( uMsg == msgOneSecond ) {

		return FALSE;
		}

	return CPrimListViewer::OnMessage(uMsg, uParam);
	}

// Event Hooks

BOOL CPrimFileViewer::OnMakeList(void)
{
	m_List.Empty();
	
	AddButton(m_pBtnUp,   0);

	AddButton(m_pBtnDown, 1);

	AddButton(m_pBtnPrev, 2);
	
	AddButton(m_pBtnNext, 3);

	if( m_ShowScan ) {
	
		AddButton(m_pBtnScan,  4);
		}

	if( m_ShowLeft ) {
	
		AddButton(m_pBtnLeft,  5);

		AddButton(m_pBtnRight, 6);
		}

	return TRUE;
	}

BOOL CPrimFileViewer::OnEnable(void)
{
	if( (m_Ctx.m_nPos < 0 && m_nLines) || m_Ctx.m_nPos > 0 ) {

		m_uEnable |= (1 << 0);
		}

	if( (m_Ctx.m_nPos < 0 && m_nLines) || m_Ctx.m_nPos < m_nLines - 1 ) {

		m_uEnable |= (1 << 1);
		}

	if( m_uList && m_uFile > 0 ) {

		m_uEnable |= (1 << 2);
		}

	if( m_uList && m_uFile < m_uList - 1 ) {

		m_uEnable |= (1 << 3);
		}

	if( m_ShowScan ) {
		
		m_uEnable |= (1 << 4);
		}

	if( m_nWidth > m_nSpace ) {

		if( m_nLeft ) {

			m_uEnable |= (1 << 5);
			}

		if( m_nLeft < m_nWidth - m_nSpace ) {

			m_uEnable |= (1 << 6);
			}
		}

	return TRUE;
	}

BOOL CPrimFileViewer::OnBtnDown(UINT n)
{
 	if( n == 2 ) {

		if( m_uFile ) {

			m_uFile = m_uFile - 1;

			m_fLoad = TRUE;
			}
		}

	if( n == 3 ) {

		if( m_uFile + 1 < m_uList ) {

			m_uFile = m_uFile + 1;

			m_fLoad = TRUE;
			}
		}

	if( n == 4 ) {

		if( m_pList ) {

			delete [] m_pList;

			m_pList = NULL;

			m_uList = 0;
			}

		LoadList();

		m_uFile = 0;

		m_fLoad = TRUE;
		}

	if( n == 5 ) {

		m_fChange = TRUE;

		m_nLeft--;
		}

	if( n == 6 ) {

		m_fChange = TRUE;

		m_nLeft++;
		}

	return CPrimListViewer::OnBtnDown(n);
	}

BOOL CPrimFileViewer::OnBtnRepeat(UINT n)
{
	if( n == 2 || n == 3 ) {

		OnBtnDown(n);
		}

	if( n == 5 || n == 6 ) {

		OnBtnDown(n);
		}

	return CPrimListViewer::OnBtnRepeat(n);
	}

BOOL CPrimFileViewer::OnBtnUp(UINT n)
{
	return CPrimListViewer::OnBtnUp(n);
	}

// List Hooks

UINT CPrimFileViewer::ListGetSequence(void)
{
	return m_uSeq;
	}

void CPrimFileViewer::ListGetData(int nTop)
{
	if( m_fLoad ) {

		LoadFile();

		m_fLoad = FALSE;
		}
	else {
		while( nTop < m_nTop ) {

			for( UINT n = m_uData; n > 0; n-- ) {

				m_pData[n] = m_pData[n-1];

				m_pPos [n] = m_pPos [n-1];
				}

			LoadPrevLine();

			m_nTop--;
			}

		while( nTop > m_nTop ) {

			for( UINT n = 0; n < m_uData; n++ ) {

				m_pData[n] = m_pData[n+1];

				m_pPos [n] = m_pPos [n+1];
				}

			LoadNextLine();

			m_nTop++;
			}
		}
	}

void CPrimFileViewer::ListDrawHead(IGDI *pGDI, int yp)
{
	R2 Rect;

	Rect.x1 = m_DrawRect.x1 + 1;
	Rect.x2 = m_DrawRect.x2 - 1;
	Rect.y1 = yp;
	Rect.y2 = yp + m_yHead + 4;

	pGDI->SetTextFore (0x7FFF);

	pGDI->SetBrushFore(GetRGB(20,0,0));

	pGDI->FillRect(PassRect(Rect));

	if( m_uList ) {

		CString Text;

		Text.Printf("File %s  (%u of %u)", PCTXT(m_pList[m_uFile]), m_uFile+1, m_uList);

		pGDI->TextOut(m_DrawRect.x1 + 2, yp + 2, Text);

		return;
		}

	CUnicode Text = UniVisual(m_pTxtEmpty->GetText(L""));

	pGDI->TextOut(m_DrawRect.x1 + 2, yp + 2, Text);
	}

void CPrimFileViewer::ListDrawEmpty(IGDI *pGDI, int yp)
{
	}

void CPrimFileViewer::ListDrawStart(IGDI *pGDI, int nTop)
{
	pGDI->SetTextTrans(modeTransparent);

	SelectFont(pGDI, m_FontText);

	m_fEnd = FALSE;
	}

void CPrimFileViewer::ListDrawRow(IGDI *pGDI, int nRow, int yp, BOOL fSelect)
{
	if( fSelect ) {

		DrawRowBack(pGDI, yp);
		}

	if( !m_fEnd ) {

		int   xp    = m_DrawRect.x1 + 2;

		PCTXT pText = m_pData[nRow - m_Ctx.m_nTop];

		if( IsEndOfFile(pText[0]) ) {

			if( m_Number ) {

				xp += pGDI->GetTextWidth("00000 ");
				}

			CUnicode End = UniVisual(m_pTxtEnd->GetText(L"End of File"));

			pGDI->SetTextFore(GetRGB(31,0,0));

			pGDI->TextOut(xp, yp, End);

			m_fEnd = TRUE;
			}
		else {
			if( m_Number ) {

				CPrintf Line("%5.5u ", 1 + nRow);

				pGDI->SetTextFore(GetRGB(0,0,15));

				pGDI->TextOut(xp, yp, Line);

				xp += pGDI->GetTextWidth(Line);
				}

			if( m_nWidth > m_nSpace ) {

				if( m_nLeft < int(strlen(pText)) ) {

					CString Text(pText + m_nLeft, m_nSpace);

					pGDI->SetTextFore(0);

					pGDI->TextOut(xp, yp, Text);
					}
				}
			else {
				pGDI->SetTextFore(0);

				pGDI->TextOut(xp, yp, pText);
				}
			}
		}
	}

void CPrimFileViewer::ListDrawEnd(IGDI *pGDI)
{
	}

// File Handling

void CPrimFileViewer::LoadList(void)
{
	m_dwExtent = NOTHING;

	m_Root     = UniConvert(m_pRoot->GetText(L"/"));
	
	m_Root.Replace('/', '\\');

	if( !chdir(m_Root) ) {

		CAutoDirentList List;

		switch( m_Sort ) {

			case 0:
				List.ScanFiles(".");
				break;

			case 1:
				List.ScanFilesByTimeFwd(".");
				break;

			case 2:
				List.ScanFilesByTimeRev(".");
				break;
			}

		if( List.GetCount() ) {

			UINT uFind = List.GetCount();

			m_pList    = New CFilename [ uFind ];

			m_uList    = 0;

			for( UINT n = 0; n < uFind; n++ ) {

				CFilename &Name = m_pList[m_uList];

				Name = List[n]->d_name;

				PCTXT pType = Name.GetType();

				BOOL  fShow = FALSE;

				if( pType ) {

					if( !fShow && m_IncCSV && !stricmp(pType, "csv") ) {

						fShow = TRUE;
					}

					if( !fShow && m_IncTXT && !stricmp(pType, "txt") ) {

						fShow = TRUE;
					}

					if( !fShow && m_IncLOG && !stricmp(pType, "log") ) {

						fShow = TRUE;
					}
				}

				if( fShow ) {

					m_uList++;
					}
				}

			if( !m_uList ) {

				delete [] m_pList;

				m_pList = NULL;
				}
			}
		}

	m_uFile = 0;
	}

void CPrimFileViewer::LoadFile(void)
{
	if( m_pList && m_uFile < m_uList ) {

		if( !chdir(m_Root) ) {

			CFilename const &Name = m_pList[m_uFile];

			CAutoFile File(Name, "r");

			if( File ) {

				m_dwExtent = File.GetSize();

				m_nLines   = LineCount(File);

				m_fCSV     = !stricmp(Name.GetType(), "csv");

				m_nWidth   = 0;

				m_nTop     = 0;

				m_nLeft    = 0;

				LoadData(File, 0);

				return;
				}
			}
		}

	m_nLines   = 0;

	m_dwExtent = NOTHING;
	}

void CPrimFileViewer::LoadNextLine(void)
{
	CFilename const &Name = m_pList[m_uFile];

	CAutoFile File(Name, "r");

	if( File ) {

		File.Seek(m_pPos[m_uData]);

		LoadData(File, m_uData - 1);
		}
	}

void CPrimFileViewer::LoadPrevLine(void)
{
	CFilename const &Name = m_pList[m_uFile];

	CAutoFile File(Name, "r");

	if( File ) {

		UINT uSize = min(m_pPos[0], 1024);

		CAutoArray<BYTE> pData(uSize);

		File.Seek(m_pPos[0] - uSize);

		File.Read(pData, uSize);

		UINT uPos = uSize - 1;

		while( uPos &&  IsBreak(pData[uPos]  ) ) uPos--;

		UINT uEnd = uPos;

		while( uPos && !IsBreak(pData[uPos-1]) ) uPos--;

		PCTXT pText    = PCTXT(pData + uPos);

		UINT  uLen     = uEnd - uPos + 1;

		m_pPos [0] = m_pPos[0] - uSize + uPos;

		m_pData[0] = CString(pText, uLen);
		}
	}

UINT CPrimFileViewer::LineCount(CAutoFile &File)
{
	UINT  uCount = 0;

	UINT  uSize  = 8192;

	CAutoArray<BYTE> pData(uSize);

	UINT  uRead  = File.Read(pData, uSize);

	UINT  uPos   = 0;

	for(;;) {

		if( uPos < uRead ) {

			if( IsBreak(pData[uPos++]) ) {

				while( uPos < uRead ) {

					if( !IsBreak(pData[uPos]) ) {

						break;
						}
					else
						uPos++;
					}

				uCount++;
				}
			}
		else {
			if( (uRead = File.Read(pData, uSize)) ) {

				uPos = 0;

				continue;
				}

			uCount++;

			break;
			}
		}

	File.Seek(0);

	return uCount;
	}

void CPrimFileViewer::LoadData(CAutoFile &File, UINT uLine)
{
	DWORD dwPos  = File.GetPos();

	UINT  uSize  = 8192;

	CAutoArray<BYTE> pData(uSize);

	UINT  uRead  = File.Read(pData, uSize);

	UINT  uPos   = 0;

	CString Label;

	for(;;) {

		BOOL fOkay = FALSE;

		UINT uEnd  = uPos;

		while( uEnd < uRead ) {

			if( IsBreak(pData[uEnd]) ) {

				PCTXT pText    = PCTXT(pData + uPos);

				UINT  uLen     = uEnd - uPos;

				m_pPos [uLine] = dwPos + uPos;

				m_pData[uLine] = CString(pText, uLen);

				MakeMax(m_nWidth, (int) uLen);

				// REV3 -- Expand tabs in CSV data?

				uLine = uLine + 1;

				uPos  = uEnd  + 1;

				while( uPos < uRead ) {

					if( !IsBreak(pData[uPos]) ) {

						fOkay = TRUE;

						if( uLine == m_uData ) {

							m_pPos[uLine] = dwPos + uPos;

							return;
							}

						break;
						}
					else
						uPos++;
					}

				break;
				}

			uEnd++;
			}

		if( !fOkay ) {

			if( uRead < uSize ) {

				m_pPos [uLine] = dwPos + uPos;

				m_pData[uLine] = CString(26, 1);

				return;
				}

			if( uEnd == uPos ) {

				dwPos = dwPos + uRead;
		
				uRead = File.Read(pData, uSize);

				uPos  = 0;
				}
			else {
				if( uPos > uEnd ) {

					dwPos = dwPos + uRead;

					uRead = File.Read(pData, uSize);

					uPos  = 0;

					while( uPos < uRead ) {

						if( !IsBreak(pData[uPos]) ) {

							break;
							}
						else
							uPos++;
						}
					}
				else {
					UINT uMove = uEnd - uPos;

					memmove(pData, pData + uPos, uMove);

					dwPos = dwPos + uRead - uMove;

					uRead = File.Read(pData + uMove, uSize - uMove);

					uRead = uRead + uMove;

					uPos  = 0;

					if( TRUE ) {

						PBYTE pTest = pData + uMove;

						while( uPos < uRead ) {

							if( IsPrint(pTest[uPos]) ) {
							
								break;
								}
							else
								uPos++;
							}
						}
					}
				}
			}
		}
	}

BOOL CPrimFileViewer::IsBreak(BYTE bData)
{
	return bData == 0x0A || bData == 0x0D;
	}

BOOL CPrimFileViewer::IsEndOfFile(BYTE bData)
{
	return bData == 0x1A;
	}

BOOL CPrimFileViewer::IsPrint(BYTE bData)
{
	return bData > 0x1F && bData < 0x7F;
	}

BOOL CPrimFileViewer::FileChanged(void)
{
	if( m_dwExtent < NOTHING ) {

		CFilename const &Name = m_pList[m_uFile];

		CAutoFile File(Name, "r");

		if( File ) {

			// REV3 -- What if the file shrinks? Reload all?

			// REV3 -- Scroll with file if we're on EOF line?

			if( m_dwExtent == File.GetSize() ) {

				return FALSE;
				}

			int nPrev  = m_nLines;

			m_dwExtent = File.GetSize();

			m_nLines   = LineCount(File);

			if( m_Ctx.m_nTop + m_nRows >= nPrev ) {

				File.Seek(m_pPos[0]);

				LoadData(File, 0);
				}

			return TRUE;
			}

		m_dwExtent = NOTHING;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimFileViewer::ViewChanged(void)
{
	if( m_fChange ) {

		m_fChange = FALSE;

		return TRUE;
		}

	return FALSE;
	}

// Instantiator

global CPrim * Create_PrimFileViewer(void)
{
	if( WhoHasFeature(rfFileViewer) ) {

		return New CPrimFileViewer();
		}

	return New CPrimViewerUnsupported();
	}

// End of File
