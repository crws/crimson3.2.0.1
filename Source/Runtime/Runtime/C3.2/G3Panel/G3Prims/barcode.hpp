
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BARCODE_HPP
	
#define	INCLUDE_BARCODE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimBarcode;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Barcode
//

class CPrimBarcode : public CPrim
{
	public:
		// Constructor
		CPrimBarcode(void);

		// Destructor
		~CPrimBarcode(void);

		// Initialization
		void Load(PCBYTE &pData);
		
		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		CCodedText * m_pValue;
		UINT	     m_Code;
		UINT	     m_NoShrink;
		UINT	     m_NoGrow;
		UINT	     m_NoNonInt;
		UINT	     m_Border;
		CPrimColor * m_pColor1;
		CPrimColor * m_pColor2;
		INT	     m_AlignH;
		INT	     m_AlignV;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			CUnicode m_Value;
			COLOR	 m_Paper;
			COLOR	 m_Ink;
			PBYTE	 m_pData;
			int	 m_xSize;
			int	 m_ySize;
			int	 m_nStep;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);

		// Implementation
		UINT GetMask(void);
	};

// End of File

#endif
