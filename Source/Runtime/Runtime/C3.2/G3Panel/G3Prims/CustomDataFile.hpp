
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CustomDataFile_HPP
	
#define	INCLUDE_CustomDataFile_HPP

//////////////////////////////////////////////////////////////////////////
//
// Custom Data File
//

class CCustomDataFile
{
	public:
		// Constructor
		CCustomDataFile(CString const &Root, CUnicode const &File);

		// Destructor
		~CCustomDataFile(void);

		// Operations
		BOOL LoadFile(CUnicode const &File);

	protected:
		// Data Members
		CString		m_Root;
		CUnicode        m_File;
		IFilingSystem * m_pFileSys;
		IFile         * m_pFile;

		// Implementation
		BOOL IsCustom(void);
		BOOL IsRedLion(void);
		void LoadData(IFile *pFile);
		void LoadRedLionData(IFile *pFile);
		void LoadMurphyData(IFile *pFile);

		// Filing System Helpers
		BOOL FindFileSystem(char cDrive);
		void FreeFileSystem(void);
	};

// End of File

#endif
