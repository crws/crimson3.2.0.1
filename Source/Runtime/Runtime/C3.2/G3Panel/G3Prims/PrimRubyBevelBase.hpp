
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyBevelBase_HPP
	
#define	INCLUDE_PrimRubyBevelBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyWithText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPrimRubyBrush;
class CPrimRubyPenEdge;

//////////////////////////////////////////////////////////////////////////
//
// Ruby BevelBase Primitive
//

class CPrimRubyBevelBase : public CPrimRubyWithText
{
	public:
		// Constructor
		CPrimRubyBevelBase(void);

		// Destructor
		~CPrimRubyBevelBase(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		UINT		   m_Style;
		CPrimRubyBrush   * m_pFill;
		CPrimRubyPenEdge * m_pEdge;
		CPrimColor       * m_pHilite;
		CPrimColor       * m_pShadow;

	protected:
		// Data Members
		CRubyGdiList m_listFill;
		CRubyGdiList m_listEdge;
		CRubyGdiList m_listHilite;
		CRubyGdiList m_listShadow;

		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR m_Hilite;
			COLOR m_Shadow;
			UINT  m_Style;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

// End of File

#endif
