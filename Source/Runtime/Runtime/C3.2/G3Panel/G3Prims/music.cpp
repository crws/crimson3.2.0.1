
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Music Player
//

// Constructor

CMusicPlayer::CMusicPlayer(void)
{
	m_pEvent = Create_AutoEvent();

	m_pLock  = Create_Rutex();

	m_pTune  = NULL;
	}

// Destructor

CMusicPlayer::~CMusicPlayer(void)
{
	AfxRelease(m_pEvent);

	AfxRelease(m_pLock);
	}

// Task List

void CMusicPlayer::GetTaskList(CTaskList &List)
{
	CTaskDef Task;

	Task.m_Name   = "MUSIC";
	Task.m_pEntry = this;
	Task.m_uID    = 0;
	Task.m_uCount = 1;
	Task.m_uLevel = 8800;
	Task.m_uStack = 0;

	List.Append(Task);
	}

// Task Entries

void CMusicPlayer::TaskInit(UINT uID)
{
	}

void CMusicPlayer::TaskExec(UINT uID)
{
	for(;;) {

		m_pEvent->Wait(FOREVER);

		PlayRTTTL(m_pTune);

		Free(m_pTune);

		m_pTune = NULL;
		}
	}

void CMusicPlayer::TaskStop(UINT uID)
{
	}

void CMusicPlayer::TaskTerm(UINT uID)
{
	}

// Operations

BOOL CMusicPlayer::PlayTune(PUTF pTune)
{
	CAutoLock Lock(m_pLock);

	if( !m_pTune ) {

		m_pTune = pTune;

		Lock.Free();

		m_pEvent->Set();

		return TRUE;
		}

	Lock.Free();

	Free(pTune);

	return FALSE;
	}

BOOL CMusicPlayer::StopTune(void)
{
	StopRTTTL();

	return TRUE;
	}

// Implementation

BOOL CMusicPlayer::PlayRTTTL(PCUTF pText)
{
	UINT  uState	   = 0;

	UINT  uDefScale    = 6;

	UINT  uDefDuration = 4;

	UINT  uBPM         = 63;

	UINT  uScale	   = 0;

	UINT  uNote	   = 0;

	UINT  uDuration	   = 0;

	BOOL  fDotted	   = FALSE;

	PUINT pRead	   = NULL;

	m_fStop            = FALSE;

	while( *pText && !m_fStop ) {

		char c = tolower(*pText++);

		if( isspace(c) ) {

			continue;
			}

		switch( uState ) {

			case 0:
				if( c == ':' ) {

					uState = 1;
					}
				break;

			case 1:
				if( c == ':' ) {

					uState = 4;
					}
				else {
					pRead = NULL;

					if( c == 'd' ) {
						
						pRead = &uDefDuration;
						}
					
					if( c == 'o' ) {
						
						pRead = &uDefScale;
						}
					
					if( c == 'b' ) {
						
						pRead = &uBPM;
						}

					if( pRead ) {

						*pRead = 0;

						uState = 2;
						}
					else
						return FALSE;
					}
				break;

			case 2:
				if( c == '=' ) {

					uState = 3;

					break;
					}

				return FALSE;

			case 3:
				if( isdigit(c) ) {

					*pRead *= 10;

					*pRead += c - '0';

					break;
					}

				if( c == ':' ) {

					uState = 4;

					break;
					}

				if( c == ',' ) {

					uState = 1;

					break;
					}

				return FALSE;

			case 4:
				if( isdigit(c) ) {

					fDotted   = FALSE;

					uDuration = c - '0';

					uState    = 5;
					}
				else {
					fDotted   = FALSE;

					uDuration = uDefDuration;

					uNote     = GetNote(c);

					uScale    = uDefScale;

					uState    = 6;
					}
				break;

			case 5:
				if( isdigit(c) ) {

					uDuration *= 10;

					uDuration += c - '0';
					}
				else {
					uNote  = GetNote(c);

					uScale = uDefScale;

					uState = 6;
					}
				break;

			case 6:
				if( c == '.' ) {

					fDotted = TRUE;

					break;
					}

				if( c == '#' ) {

					uNote += 1;

					break;
					}

				if( isdigit(c) ) {

					uScale = c - '0';

					break;
					}

				if( c == ',' ) {

					PlayNote(uNote, uScale, uDuration, fDotted, uBPM);

					uState = 4;
					
					break;
					}

				return FALSE;
			}
		}

	if( uState == 6 ) {

		PlayNote(uNote, uScale, uDuration, fDotted, uBPM);

		return TRUE;
		}

	return FALSE;
	}

void CMusicPlayer::StopRTTTL(void)
{
	m_fStop = TRUE;
	}

UINT CMusicPlayer::GetNote(WCHAR c)
{
	switch( c ) {

		case 'c': return  0;
		case 'd': return  2;
		case 'e': return  4;
		case 'f': return  5;
		case 'g': return  7;
		case 'a': return  9;
		case 'b': return 11;
		case 'h': return 11;

		}

	return 9999;
	}

void CMusicPlayer::PlayNote(UINT uNote, UINT uScale, UINT uDuration, BOOL fDotted, UINT uBPM)
{
	UINT uBeat = (4 * 60000) / uBPM;

	UINT uTime = uBeat / uDuration;

	if( fDotted ) {
		
		uTime += uTime / 2;
		}

	if( uNote != 9999 ) {

		Beep(uNote + uScale * 12, uTime);
		}

	Sleep(9 * uTime / 8);
	}

// End of File
