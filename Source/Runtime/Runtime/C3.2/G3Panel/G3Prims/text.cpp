
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Text Block
//

// Constructor

CPrimText::CPrimText(CPrim *pPrim) : CPrimBlock(pPrim)
{
	m_pText   = NULL;

	m_pColor  = New CPrimColor(naText);

	m_pShadow = New CPrimColor(naNone);
	}

// Destructor

CPrimText::~CPrimText(void)
{
	delete m_pText;

	delete m_pColor;

	delete m_pShadow;
	}

// Initialization

void CPrimText::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimText", pData);

	CPrimBlock::Load(pData);

	GetCoded(pData, m_pText);

	m_pColor->Load(pData);

	m_pShadow->Load(pData);
	}

// Attributes

BOOL CPrimText::IsEmpty(void) const
{
	return !m_pText;
	}

BOOL CPrimText::IsTransparent(void) const
{
	return TRUE;
	}

// Operations

void CPrimText::SetScan(UINT Code)
{
	SetItemScan(m_pText,   Code);

	SetItemScan(m_pColor,  Code);
	
	SetItemScan(m_pShadow, Code);
	}

BOOL CPrimText::DrawPrep(IGDI *pGDI)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_Ctx = Ctx;

		return TRUE;
		}

	return FALSE;
	}

void CPrimText::DrawItem(IGDI *pGDI, R2 Rect)
{
	SelectFont(pGDI);

	Rect.left   += m_Margin.left;

	Rect.right  -= m_Margin.right;
	
	Rect.top    += m_Margin.top;
	
	Rect.bottom -= m_Margin.bottom;

	m_Flow.Flow(pGDI, UniVisual(m_Ctx.m_Text), Rect);

	CTextFlow::CFormat Fmt;

	Fmt.m_AlignH   = m_AlignH;
	
	Fmt.m_AlignV   = m_AlignV;
	
	Fmt.m_Lead     = m_Lead;
	
	Fmt.m_Color    = m_pColor->GetColor();
	
	Fmt.m_Shadow   = m_pShadow->GetColor();
	
	Fmt.m_fMove    = m_Ctx.m_fPress;
	
	Fmt.m_MoveDir  = m_MoveDir;
	
	Fmt.m_MoveStep = m_MoveStep;

	m_Flow.Draw(pGDI, Fmt);
	}

// Context Creation

void CPrimText::FindCtx(CCtx &Ctx)
{
	if( m_pText->IsAvail() ) {

		Ctx.m_Text = m_pText->GetText();
		}
	else
		Ctx.m_Text = L"---";

	Ctx.m_Shadow = m_pShadow->GetColor();
	
	Ctx.m_Color  = m_pColor->GetColor();

	Ctx.m_fPress = m_pPrim->IsPressed();
	}

// Context Check

BOOL CPrimText::CCtx::operator == (CCtx const &That) const
{
	if( !m_Text.CompareC(That.m_Text) ) {

		return m_Shadow == That.m_Shadow &&
		       m_Color  == That.m_Color  &&
		       m_fPress == That.m_fPress ;
		}

	return FALSE;
	}

// End of File
