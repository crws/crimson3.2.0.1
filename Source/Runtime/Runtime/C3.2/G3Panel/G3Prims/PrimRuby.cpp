
#include "intern.hpp"

#include "PrimRuby.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive
//

// Constructor

CPrimRuby::CPrimRuby(void)
{
	}

// Initialization

void CPrimRuby::Load(PCBYTE &pData)
{
	CPrim::Load(pData);

	m_bound.x1 = short(GetWord(pData) + 0);
	m_bound.y1 = short(GetWord(pData) + 0);
	m_bound.x2 = short(GetWord(pData) + 1);
	m_bound.y2 = short(GetWord(pData) + 1);
	}

// Overridables

R2 CPrimRuby::GetBackRect(void)
{
	return m_bound;
	}

void CPrimRuby::MovePrim(int cx, int cy)
{
	m_bound.x1 += cx;
	m_bound.y1 += cy;
	m_bound.x2 += cx;
	m_bound.y2 += cy;

	CPrim::MovePrim(cx, cy);
	}

// Implementation

BOOL CPrimRuby::LoadList(PCBYTE &pData, CRubyGdiList &list)
{
	UINT n;

	if( (n = GetWord(pData)) ) {

		list.m_uCount = n;

		list.m_pCount = New UINT [ n ];

		for( UINT c = 0; c < n; c++ ) {

			list.m_pCount[c] = GetWord(pData);
			}
		}
	else {
		list.m_uCount = 0;

		list.m_pCount = NULL;
		}

	if( (n = GetWord(pData)) ) {

		list.m_uList = n;

		list.m_pList = New P2 [ n ];

		for( UINT c = 0; c < n; c++ ) {

			list.m_pList[c].x = int(short(GetWord(pData)));

			list.m_pList[c].y = int(short(GetWord(pData)));
			}
		}
	else {
		list.m_uList = 0;

		list.m_pList = NULL;
		}

	return TRUE;
	}

BOOL CPrimRuby::LoadNumber(PCBYTE &pData, number &n)
{
	DWORD *p = (DWORD *) &n;

	p[0] = GetLong(pData);
	p[1] = GetLong(pData);

	return TRUE;
	}

BOOL CPrimRuby::LoadPoint(PCBYTE &pData, CRubyPoint &p)
{
	LoadNumber(pData, p.m_x);
	
	LoadNumber(pData, p.m_y);

	return TRUE;
	}

BOOL CPrimRuby::LoadVector(PCBYTE &pData, CRubyVector &v)
{
	LoadNumber(pData, v.m_x);
	
	LoadNumber(pData, v.m_y);

	return TRUE;
	}

BOOL CPrimRuby::LoadMatrix(PCBYTE &pData, CRubyMatrix &m)
{
	m.m_fIdent = GetByte(pData);

	for( int i = 0; i < 3; i++ ) {

		for( int j = 0; j < 3; j++ ) {
			
			LoadNumber(pData, m.m_m.m_e[i][j]);
			}
		}

	return TRUE;
	}

// End of File
