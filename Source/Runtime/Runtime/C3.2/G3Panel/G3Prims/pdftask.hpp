
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PDFTASK_HPP

#define	INCLUDE_PDFTASK_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "pdf.hpp"

/////////////////////////////////////////////////////////////////////////
//
// PDF Task
//

class CPDFTask : public ITaskEntry
{
	public:
		// Constructor
		SLOW CPDFTask(void);

		// Destructor
		SLOW ~CPDFTask(void);

		// IBase
		UINT Release(void);

		// Operations
		void AllowRestart(void);
		void SubmitRequest(CPDFRequest *pRqst);
		void ClearRequests(UINT uHandle);

		// Task List
		void GetTaskList(CTaskList &List);

		// ITaskEntry
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

	protected:
		// Data
		BOOL volatile m_fFree;
		HTASK         m_hTask;
		IEvent      * m_pFlag;
		IMutex      * m_pLock;
		CPDFRequest * m_pHead;
		CPDFRequest * m_pTail;
		UINT          m_uHandle;

		// Implementation
		void RestartTask(void);
	};

// End of File

#endif
