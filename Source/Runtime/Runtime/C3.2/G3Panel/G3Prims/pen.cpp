
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Pen
//

// Constructor

CPrimPen::CPrimPen(void)
{
	m_Width  = 1;

	m_Corner = 0;

	m_pColor = New CPrimColor(naPen);
	}

// Destructor

CPrimPen::~CPrimPen(void)
{
	delete m_pColor;
	}

// Initialization

void CPrimPen::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimPen", pData);

	m_Width  = GetByte(pData);

	m_Corner = GetByte(pData);

	m_pColor->Load(pData);
	}

// Attributes

BOOL CPrimPen::IsNull(void) const
{
	return m_Width == 0;
	}

int CPrimPen::GetAdjust(void) const
{
	return m_Width ? int((m_Width - 1) / 2) : 0;
	}

int CPrimPen::GetWidth(void) const
{
	return int(m_Width);
	}

// Operations

BOOL CPrimPen::AdjustRect(R2 &Rect)
{
	if( m_Width > 1 ) {

		int n = GetAdjust();

		DeflateRect(Rect, n, n);

		return TRUE;
		}

	return FALSE;
	}

void CPrimPen::Configure(IGDI *pGDI)
{
	pGDI->SetPenStyle(penFore);

	pGDI->SetPenCaps (capsBoth);

	pGDI->SetPenFore (m_Ctx.m_Color);

	pGDI->SetPenWidth(m_Width);
	}

void CPrimPen::SetScan(UINT Code)
{
	SetItemScan(m_pColor, Code);
	}

BOOL CPrimPen::DrawPrep(IGDI *pGDI)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_Ctx = Ctx;

		return TRUE;
		}

	return FALSE;
	}

// Drawing

void CPrimPen::DrawRect(IGDI *pGDI, R2 Rect)
{
	if( m_Width > 0 ) {

		pGDI->SetPenStyle(penFore);

		pGDI->SetPenFore (m_Ctx.m_Color);

		pGDI->SetPenWidth(m_Width);

		pGDI->SetPenCaps(m_Corner ? capsNone : capsStart);

		pGDI->DrawRect(PassRect(Rect));
		}
	}

void CPrimPen::DrawEllipse(IGDI *pGDI, R2 Rect, UINT uType)
{
	if( m_Width > 0 ) {

		pGDI->SetPenStyle(penFore);

		pGDI->SetPenFore (m_Ctx.m_Color);

		pGDI->SetPenWidth(m_Width);

		pGDI->SetPenCaps (capsStart);

		pGDI->DrawEllipse(PassRect(Rect), uType);
		}
	}

void CPrimPen::DrawWedge(IGDI *pGDI, R2 Rect, UINT uType)
{
	if( m_Width > 0 ) {

		pGDI->SetPenStyle(penFore);

		pGDI->SetPenFore (m_Ctx.m_Color);

		pGDI->SetPenWidth(m_Width);

		pGDI->SetPenCaps (capsStart);

		pGDI->DrawWedge(PassRect(Rect), uType);
		}
	}

void CPrimPen::DrawPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound)
{
	// REV3 -- If this edge is part of a polygon that is going
	// to be filled with a horizontal shader, we don't get 100%
	// pixel coherence as the edge will be drawn vertically. To
	// fix it, we're going to have to have an argument to draw
	// polygon to indicate which way to draw it, and we're going
	// to have to have our owner tell us about what kind of fill
	// is being used. A better fix would be to mod the gdi code
	// to achieve coherence, but that's hard...

	if( m_Width > 0 ) {

		AfxAssert(!m_Corner);

		pGDI->SetPenStyle(penFore);

		pGDI->SetPenFore (m_Ctx.m_Color);

		pGDI->SetPenWidth(m_Width);

		pGDI->SetPenCaps (capsStart);

		pGDI->DrawPolygon(pList, uCount, dwRound);
		}
	}

// Context Creation

void CPrimPen::FindCtx(CCtx &Ctx)
{
	Ctx.m_Color = m_pColor->GetColor();
	}

// Context Check

BOOL CPrimPen::CCtx::operator == (CCtx const &That) const
{
	return m_Color == That.m_Color;
	}

// End of File
