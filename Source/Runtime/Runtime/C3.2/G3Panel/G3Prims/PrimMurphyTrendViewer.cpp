
#include "intern.hpp"

#include "PrimMurphyTrendViewer.hpp"

#include "CustomDataLog.hpp"

#include "CustomDataFile.hpp"

#include "CustomDataCache.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- FW Murphy Trend Viewer
//

// Scale Table

CPrimMurphyTrendViewer::CScale const CPrimMurphyTrendViewer::m_Scale[] = {

	{	30,		6,		30		},
	{	60,		10,		30		},
	{	120,		10,		30		},
	{	180,		10,		30		},
	{	240,		30,		120		},
	{	300,		30,		240		},
	{	600,		30,		240		},
	{	1200,		60,		600		},
	{	1800,		60,		1200		},
	{	3600,		60,		1200		},
	{	7200,		120,		2400		},
	{	10800,		240,		2400		},
	{	14400,		240,		2400		},
	{	18000,		300,		3600		},
	{	21600,		300,		3600		},
	{	25200,		300,		3600		},
	{	28800,		300,		3600		},
	{	32400,		300,		3600		},
	{	36000,		600,		3600		},
	{	39600,		600,		7200		},
	{	43200,		600,		7200		},
	{	86400,		1800,		28800		},
	{	172800,		3600,		28800		},

	};

// Constructor

CPrimMurphyTrendViewer::CPrimMurphyTrendViewer(void)
{
	m_pRoot         = NULL;
	m_pFile         = NULL;
	m_pZoomMin      = NULL;
	m_pZoomMax      = NULL;
	m_pZoomInit     = NULL;
	m_pPerLane      = NULL;
	m_pTimeStart    = NULL;
	m_pTimeEnd      = NULL;
	m_Width         = 4;
	m_pPenMask      = NULL;
	m_ShowData      = 0;
	m_ShowCursor    = 0;
	m_DataBox       = 0;
	m_FontTitle     = fontHei16Bold;
	m_FontData      = fontHei16;
	m_GridTime      = 0;
	m_GridMode      = 0;
	m_GridMajor     = 10;
	m_GridMinor     = 2;
	m_pGridMin      = NULL;
	m_pGridMax      = NULL;
	m_DrawMin       = 0;
	m_DrawMax       = 0;
	m_Precise       = 1;
	m_pColTitle     = New CPrimColor(naText);
	m_pColLabel     = New CPrimColor(naText);
	m_pColData      = New CPrimColor(naText);
	m_pColMajor     = New CPrimColor(naPen);
	m_pColMinor     = New CPrimColor(naPen);
	m_pColCursor    = New CPrimColor(naPen);
	m_pColPen[0x0]  = New CPrimColor(naPen);
	m_pColPen[0x1]  = New CPrimColor(naPen);
	m_pColPen[0x2]  = New CPrimColor(naPen);
	m_pColPen[0x3]  = New CPrimColor(naPen);
	m_pColPen[0x4]  = New CPrimColor(naPen);
	m_pColPen[0x5]  = New CPrimColor(naPen);
	m_pColPen[0x6]  = New CPrimColor(naPen);
	m_pColPen[0x7]  = New CPrimColor(naPen);
	m_pColPen[0x8]  = New CPrimColor(naPen);
	m_pColPen[0x9]  = New CPrimColor(naPen);
	m_pColPen[0xA]  = New CPrimColor(naPen);
	m_pColPen[0xB]  = New CPrimColor(naPen);
	m_pColPen[0xC]  = New CPrimColor(naPen);
	m_pColPen[0xD]  = New CPrimColor(naPen);
	m_pColPen[0xE]  = New CPrimColor(naPen);
	m_pColPen[0xF]  = New CPrimColor(naPen);
	m_pBtnPgLeft    = NULL;
	m_pBtnLeft      = NULL;
	m_pBtnLive      = NULL;
	m_pBtnRight     = NULL;
	m_pBtnPgRight   = NULL;
	m_pBtnIn        = NULL;
	m_pBtnOut       = NULL;
	m_pBtnLoad      = NULL;
	m_pFormat       = New CDispFormatTimeDate;
	m_fInit         = FALSE;
	m_uScale        = m_Width;
	m_fLive         = TRUE;
	m_fMemory       = TRUE;
	m_dwTime	= 0;
	m_dwLast	= 0;
	m_dwCursor      = 0;
	m_uEnable       = NOTHING;
	m_fLoad         = FALSE;
	m_fUseLoad      = TRUE;
	m_fShowData     = FALSE;
	m_pShowData     = NULL;
	m_pLane         = NULL;

	m_pCache        = NULL; 

	ClearCtx(m_Ctx);
	}

// Destructor

CPrimMurphyTrendViewer::~CPrimMurphyTrendViewer(void)
{
	if( m_pLane ) {

		delete [] m_pLane;

		m_pLane = NULL;
		}

	if( m_pCache ) {
		
		delete m_pCache;

		m_pCache = NULL; 
		}

	if( m_pShowData ) {

		delete [] m_pShowData;

		m_pShowData = NULL;
		}

	delete m_pPenMask;
	delete m_pGridMin;
	delete m_pGridMax;
	delete m_pColTitle;
	delete m_pColLabel;
	delete m_pColData;
	delete m_pColMajor;
	delete m_pColMinor;
	delete m_pColCursor;
	delete m_pColPen[0x0];
	delete m_pColPen[0x1];
	delete m_pColPen[0x2];
	delete m_pColPen[0x3];
	delete m_pColPen[0x4];
	delete m_pColPen[0x5];
	delete m_pColPen[0x6];
	delete m_pColPen[0x7];
	delete m_pColPen[0x8];
	delete m_pColPen[0x9];
	delete m_pColPen[0xA];
	delete m_pColPen[0xB];
	delete m_pColPen[0xC];
	delete m_pColPen[0xD];
	delete m_pColPen[0xE];
	delete m_pColPen[0xF];
	delete m_pBtnPgLeft;
	delete m_pBtnLeft;
	delete m_pBtnLive;
	delete m_pBtnRight;
	delete m_pBtnPgRight;
	delete m_pBtnIn;
	delete m_pBtnOut;
	delete m_pBtnLoad;
	delete m_pFormat;
	delete m_pTimeEnd;
	delete m_pTimeStart;
	delete m_pRoot;
	delete m_pFile;
	delete m_pPerLane;
	delete m_pZoomInit;
	delete m_pZoomMin;
	delete m_pZoomMax;
	}

// Initialization

void CPrimMurphyTrendViewer::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimMurphyTrendViewer", pData);

	CPrimViewer::Load(pData);

	GetCoded(pData, m_pRoot);
	GetCoded(pData, m_pFile);
	GetCoded(pData, m_pZoomMin);
	GetCoded(pData, m_pZoomMax);
	GetCoded(pData, m_pZoomInit);
	GetCoded(pData, m_pPerLane);
	GetCoded(pData, m_pTimeStart);
	GetCoded(pData, m_pTimeEnd);

	m_Width = GetByte(pData);

	GetCoded(pData, m_pPenMask);

	m_PenWeight  = GetByte(pData);
	m_ShowData   = GetByte(pData);
	m_ShowCursor = GetByte(pData);
	m_DataBox    = GetByte(pData);
	
	m_FontTitle  = GetWord(pData);
	m_FontData   = GetWord(pData);

	m_GridTime   = GetByte(pData);
	m_GridMode   = GetByte(pData);
	m_GridMajor  = GetByte(pData);
	m_GridMinor  = GetByte(pData);
	
	GetCoded(pData, m_pGridMin);
	GetCoded(pData, m_pGridMax);

	m_Precise = GetByte(pData);

	m_pColTitle ->Load(pData);
	m_pColLabel ->Load(pData);
	m_pColData  ->Load(pData);
	m_pColMajor ->Load(pData);
	m_pColMinor ->Load(pData);
	m_pColCursor->Load(pData);

	m_pColPen[0x0]->Load(pData);
	m_pColPen[0x1]->Load(pData);
	m_pColPen[0x2]->Load(pData);
	m_pColPen[0x3]->Load(pData);
	m_pColPen[0x4]->Load(pData);
	m_pColPen[0x5]->Load(pData);
	m_pColPen[0x6]->Load(pData);
	m_pColPen[0x7]->Load(pData);
	m_pColPen[0x8]->Load(pData);
	m_pColPen[0x9]->Load(pData);
	m_pColPen[0xA]->Load(pData);
	m_pColPen[0xB]->Load(pData);
	m_pColPen[0xC]->Load(pData);
	m_pColPen[0xD]->Load(pData);
	m_pColPen[0xE]->Load(pData);
	m_pColPen[0xF]->Load(pData);

	GetCoded(pData, m_pBtnPgLeft);
	GetCoded(pData, m_pBtnLeft);
	GetCoded(pData, m_pBtnLive);
	GetCoded(pData, m_pBtnRight);
	GetCoded(pData, m_pBtnPgRight);
	GetCoded(pData, m_pBtnIn);
	GetCoded(pData, m_pBtnOut);
	GetCoded(pData, m_pBtnLoad);

	pData += 1;

	m_pFormat->Load(pData);	

	m_uScale = m_Width;
	}

// Overridables

void CPrimMurphyTrendViewer::SetScan(UINT Code)
{
	if( m_GridMode == 3 ) {

		SetItemScan(m_pGridMin, Code);
		SetItemScan(m_pGridMax, Code);
		}

	m_pColTitle ->SetScan(Code);
	m_pColLabel ->SetScan(Code);
	m_pColData  ->SetScan(Code);
	m_pColMajor ->SetScan(Code);
	m_pColMinor ->SetScan(Code);
	m_pColCursor->SetScan(Code);

	for( UINT n = 0; n < 16; n++ ) {

		m_pColPen[n]->SetScan(Code);
		}

	SetItemScan(m_pBtnPgLeft,  Code);
	SetItemScan(m_pBtnLeft,    Code);
	SetItemScan(m_pBtnLive,    Code);
	SetItemScan(m_pBtnRight,   Code);
	SetItemScan(m_pBtnPgRight, Code);
	SetItemScan(m_pBtnIn,      Code);
	SetItemScan(m_pBtnOut,     Code);
	SetItemScan(m_pBtnLoad,    Code);
	SetItemScan(m_pPenMask,    Code);

	SetItemScan(m_pFile,      Code);
	SetItemScan(m_pZoomMax,   Code);
	SetItemScan(m_pZoomMin,   Code);
	SetItemScan(m_pZoomInit,  Code);
	SetItemScan(m_pPerLane,   Code);
	SetItemScan(m_pTimeStart, Code);
	SetItemScan(m_pTimeEnd,   Code);

	CPrimViewer::SetScan(Code);
	}

void CPrimMurphyTrendViewer::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( !m_fInit ) {

		CPrimViewer::DrawPrep(pGDI, Erase, Trans);

		m_uZoomInit = GetItemData(m_pZoomInit, C3INT(4));

		m_uZoomMin  = GetItemData(m_pZoomMin, C3INT(0));
		
		m_uZoomMax  = GetItemData(m_pZoomMax, C3INT(elements(m_Scale)));

		MakeMax(m_uZoomMax, 1);

		MakeMin(m_uZoomMax, elements(m_Scale) - 1);

		MakeMax(m_uZoomMax, m_uZoomMin);

		MakeMin(m_uZoomMin, m_uZoomMax);

		MakeMax(m_uZoomInit, m_uZoomMin);

		MakeMin(m_uZoomInit, m_uZoomMax);

		m_uScale    = m_uZoomInit;

		LayoutCore(pGDI);

		m_fInit = TRUE;
		}

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {
			
			if( m_Ctx.m_File != Ctx.m_File ) {

				if( !Ctx.m_File.IsEmpty() ) {

					CString Root = UniConvert(m_pRoot->GetText(L"/"));
						
					Root.Replace('/', '\\');

					if( m_pCache ) {

						delete m_pCache;

						m_pCache = NULL;
						}

					m_pCache = New CCustomDataCache;

					if( m_pCache->LoadFromFile(Root, Ctx.m_File) ) {

						MakeShowData();
						
						m_dwTime = m_pCache->GetStartTime();

						m_dwLast = m_pCache->GetFinalTime();
						}
					else {
						AfxTrace("failed to load file [%s]\n", 
						          PCTXT(UniConvert(Ctx.m_File))
							  );
						}					
					}
				}

			if( m_Ctx.m_fLoad != Ctx.m_fLoad ) {

				// Development !!!!

				CString Root = UniConvert(m_pRoot->GetText(L"/"));
						
				Root.Replace('/', '\\');
				
				CString File = CPrintf("C:\\%s\\myFile.raw", PCTXT(Root));			

				CCustomDataLog(6).MakeFile(PCTXT(File));
				}

			if( Ctx.m_dwTime ) {

				WriteTime(m_pTimeEnd,   Ctx.m_dwTime / 5);

				WriteTime(m_pTimeStart, Ctx.m_dwLeft / 5);
				}

			m_Ctx      = Ctx;

			m_dwTime   = m_Ctx.m_dwTime;

			m_dwCursor = m_Ctx.m_dwCursor;

			m_fChange  = TRUE;

			m_fLoad    = FALSE;

			LayoutCore(pGDI);

			LayoutGrid(pGDI);

			FindDrawLimits();

			LayoutLanes();
			}
		}

	CPrimViewer::DrawPrep(pGDI, Erase, Trans);
	}

void CPrimMurphyTrendViewer::DrawPrim(IGDI *pGDI)
{
	CPrimViewer::DrawPrim(pGDI);

	if( m_Ctx.m_dwTime ) {

		DrawPlot(pGDI);

		DrawCursor(pGDI);

		DrawInfo(pGDI);

		DrawData(pGDI);

		pGDI->SetPenStyle(penFore);

		pGDI->SetForeColor(GetRGB(0x07, 0x07, 0x07));

		pGDI->DrawRect(m_x1, m_y1, m_x2, m_y2);

		return;
		}

	DrawInfo(pGDI);
	}

void CPrimMurphyTrendViewer::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	if( m_ShowCursor ) {

		pTouch->FillRect(PassRect(m_DrawRect));
		}
	else {
		CPrimViewer::FindLayout(pGDI);
		
		pTouch->FillRect(PassRect(m_Menu));
		}
	}

// Event Hooks

BOOL CPrimMurphyTrendViewer::OnMakeList(void)
{
	m_List.Empty();

	AddButton(m_pBtnPgLeft,  btnPageLeft);
	
	AddButton(m_pBtnLeft,    btnStepLeft);

	AddButton(m_pBtnLive,    btnLive);
	
	AddButton(m_pBtnRight,   btnStepRight);
	
	AddButton(m_pBtnPgRight, btnPageRight);
	
	AddButton(m_pBtnIn,      btnZoomIn);
	
	AddButton(m_pBtnOut,     btnZoomOut);

	if( WhoHasFeature(rfLogToDisk) ) {

		if( m_fUseLoad ) {

			AddButton(m_pBtnLoad, btnLoad);
			}
		}

	return TRUE;
	}

BOOL CPrimMurphyTrendViewer::OnEnable(void)
{
	if( m_Ctx.m_dwTime ) {

		if( m_Ctx.m_dwCursor ) {

			if( HasDataToLeft() ) {

				m_uEnable |= (1 << btnPageLeft);
				}

			if( m_Ctx.m_dwCursor > m_Ctx.m_dwLeft ) {

				m_uEnable |= (1 << btnStepLeft);
				}
			}
		else {
			if( HasDataToLeft() ) {

				m_uEnable |= (1 << btnStepLeft) | (1 << btnPageLeft);
				}
			}

		if( m_Ctx.m_dwCursor ) {

			if( m_Ctx.m_dwTime < m_dwLast ) {

				m_uEnable |= (1 << btnPageRight);
				}

			if( m_Ctx.m_dwCursor < m_Ctx.m_dwTime ) {

				m_uEnable |= (1 << btnStepRight);
				}
			}
		else {
			if( m_Ctx.m_dwTime < m_dwLast ) {

				m_uEnable |= (1 << btnStepRight) | (1 << btnPageRight);
				}
			}

		if( !m_Ctx.m_fLive || m_Ctx.m_dwCursor ) {

			m_uEnable |= (1 << btnLive);
			}

		if( m_uScale > 0 ) {

			if( m_Ctx.m_dwWide >  5 * m_pCache->GetPitchTime() ) {

				m_uEnable |= (1 << btnZoomIn);
				}
			}

		if( m_uScale < m_uZoomMax ) {

			m_uEnable |= (1 << btnZoomOut);
			}
		}

	if( TRUE ) {
			
		m_uEnable |= (1 << btnLoad);
		}

	return TRUE;
	}

BOOL CPrimMurphyTrendViewer::OnBtnDown(UINT n)
{
	if( n == btnLive ) {

		m_dwCursor = 0;

		m_fLive    = TRUE;
		
		return TRUE;
		}

	if( n <= btnPageRight ) {

		switch( n ) {

			case btnPageLeft:

				StepNeg(5 * m_Scale[m_Ctx.m_uScale].m_dwStep1);

				break;

			case btnStepLeft:

				if( m_Ctx.m_dwCursor ) {

					m_dwCursor -= 5 * m_pCache->GetPitchTime();

					return TRUE;
					}

				StepNeg(5 * m_Scale[m_Ctx.m_uScale].m_dwStep0 / 2);

				break;

			case btnStepRight:

				if( m_Ctx.m_dwCursor ) {

					m_dwCursor += 5 * m_pCache->GetPitchTime();

					return TRUE;
					}

				StepPos(5 * m_Scale[m_Ctx.m_uScale].m_dwStep0 / 2);

				break;

			case btnPageRight:

				StepPos(5 * m_Scale[m_Ctx.m_uScale].m_dwStep1);

				break;
			}

		m_fLive = FALSE;
		
		return TRUE;
		}

	if( n == btnZoomIn ) {

		m_uScale--;
		
		return TRUE;
		}

	if( n == btnZoomOut ) {

		m_uScale++;
		
		return TRUE;
		}

	if( n == btnLoad ) {

		m_fLoad = TRUE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimMurphyTrendViewer::OnBtnRepeat(UINT n)
{
	return OnBtnDown(n);
	}

BOOL CPrimMurphyTrendViewer::OnBtnUp(UINT n)
{
	return FALSE;
	}

BOOL CPrimMurphyTrendViewer::OnTouchWork(P2 Pos)
{
	if( m_ShowCursor ) {

		m_dwCursor = PosToTime(Pos.x);

		m_fLive    = FALSE;

		return TRUE;
		}

	return FALSE;
	}

// Navigation

BOOL CPrimMurphyTrendViewer::HasDataToLeft(void)
{
	if( m_Ctx.m_dwTime ) {		

		return m_pCache->HasCoverage(m_Ctx.m_dwLeft - 1);
		}

	return FALSE;
	}

void CPrimMurphyTrendViewer::StepNeg(DWORD s)
{
	m_dwTime -= s - 1;

	m_dwTime /= s;

	m_dwTime *= s;

	MakeMax(m_dwTime, m_pCache->GetInitTime() + m_Ctx.m_dwWide);
	}

void CPrimMurphyTrendViewer::StepPos(DWORD s)
{
	m_dwTime += s + 1;

	m_dwTime /= s;

	m_dwTime *= s;

	MakeMin(m_dwTime, m_dwLast);
	}

// Grid Drawing

void CPrimMurphyTrendViewer::DrawGrid(IGDI *pGDI)
{
	if( m_GridTime ) {

		DrawGridData(pGDI, 2);

		DrawGridTime(pGDI);

		DrawGridData(pGDI, 1);

		return;
		}

	DrawGridData(pGDI, 3);
	}

void CPrimMurphyTrendViewer::DrawGridData(IGDI *pGDI, UINT uFlags)
{
	switch( m_GridMode ) {

		case 1:
			if( uFlags & 1 ) {

				DrawGridManual(pGDI, uFlags);
				}
			break;
		
		case 2:
			if( uFlags & 3 ) {

				DrawGridManual(pGDI, uFlags);
				}
			break;

		case 3:
			if( uFlags & 1 ) {

				DrawGridAuto(pGDI, uFlags);
				}
			break;
		}
	}

void CPrimMurphyTrendViewer::DrawGridManual(IGDI *pGDI, UINT uFlags)
{
	if( m_pCache ) {

		UINT uChans = m_pCache->GetChanCount();

		for( UINT uChan = 0; uChan < uChans; uChan ++ ) {

			CLane const &Lane = m_pLane[uChan];

			pGDI->ResetBrush();

			pGDI->SetBrushFore(m_Ctx.m_ColMajor);

			int nMajor = m_GridMajor;

			int nMinor = m_GridMinor;

			int yPos1  = 0;

			int yPos2  = 0;

			int yPos3  = 0;

			for( int n1 = 0; n1 <= nMajor; n1++ ) {

				yPos2 = yPos1;

				yPos1 = HorzLine(Lane.yTop, Lane.yBottom, n1, nMajor);

				if( uFlags & 1 ) {

					pGDI->SetBrushFore(m_Ctx.m_ColMajor);

					pGDI->FillRect(m_x1, yPos1, m_x2, yPos1 + 1);
					}

				if( uFlags & 2 ) {

					pGDI->SetBrushFore(m_Ctx.m_ColMinor);

					if( n1 > 0 ) {
				
						if( m_GridMode == 2 ) {

							for( int n2 = 1; n2 < nMinor; n2++ ) { 

								yPos3 = HorzLine(yPos1, yPos2, n2, nMinor);

								pGDI->FillRect(m_x1, yPos3, m_x2, yPos3 + 1);
								}
							}
						}
					}
				}
			}
		}
	}

void CPrimMurphyTrendViewer::DrawGridAuto(IGDI *pGDI, UINT uFlags)
{
	if( m_pCache ) {

		UINT uChans = m_pCache->GetChanCount();

		for( UINT uChan = 0; uChan < uChans; uChan ++ ) {

			CLane const &Lane = m_pLane[uChan];

			pGDI->ResetBrush();

			pGDI->SetBrushFore(m_Ctx.m_ColMajor);

			int Steps = max(1, m_StepSpan);

			for( int n = 0; n <= Steps; n++ ) {

				double Value = (m_StepMin + n) * m_DrawStep;

				int    yPos  = -1;				

				if( !m_Precise && m_StepSpan ) {

					if( n == 0 ) {

						m_DrawMin = Value;
						}

					if( n == Steps ) {

						m_DrawMax = Value;
						}

					yPos = HorzLine(Lane.yTop, Lane.yBottom/*m_y1, m_y2*/, n, m_StepSpan);
					}
				else {
					if( n == 0 ) {

						Value = m_DrawMin = m_Ctx.m_GridMin;

						yPos  = (m_y2 - 1);
						}

					if( n == Steps ) {

						Value = m_DrawMax = m_Ctx.m_GridMax;

						yPos  = m_y1;
						}

					if( yPos < 0 ) {

						double d = (m_Ctx.m_GridMax - m_Ctx.m_GridMin);

						double f = (Value - m_Ctx.m_GridMin) / d;

						yPos     = HorzLine(Lane.yTop, Lane.yBottom/*m_y1, m_y2*/, f);
						}
					}

				pGDI->FillRect(m_x1, yPos, m_x2, yPos + 1);
				}			
			}		
		}
	}

void CPrimMurphyTrendViewer::DrawGridTime(IGDI *pGDI)
{
	DWORD gw = 5 * m_Scale[m_Ctx.m_uScale].m_dwWidth;

	DWORD s0 = 5 * m_Scale[m_Ctx.m_uScale].m_dwStep0;

	DWORD s1 = 5 * m_Scale[m_Ctx.m_uScale].m_dwStep1;

	DWORD t0 = m_Ctx.m_dwTime;

	DWORD t1 = t0 / s0 * s0;

	int   x2 = m_x2 - m_xm;

	int   cx = m_x2 - m_x1 - m_xm;

	for(;;) {

		int px = x2 - (cx * (t0 - t1)) / gw;

		if( px >= m_x1 ) {

			if( t1 % s1 ) {

				pGDI->SetBrushFore(m_Ctx.m_ColMinor);
				}
			else
				pGDI->SetBrushFore(m_Ctx.m_ColMajor);

			pGDI->FillRect(px, m_y1, px + 1, m_y2);

			t1 -= s0;

			continue;
			}

		break;
		}
	}

int CPrimMurphyTrendViewer::HorzLine(int y1, int y2, int yp, int yc)
{
	return (y2 - 1) - ((y2 - y1 - 1) * yp) / yc;
	}

int CPrimMurphyTrendViewer::HorzLine(int y1, int y2, double f)
{
	return (y2 - 1) - int((y2 - y1 - 1) * f);
	}

// Info Drawing

void CPrimMurphyTrendViewer::DrawInfo(IGDI *pGDI)
{
	SelectFont(pGDI, m_FontText);

	pGDI->SetTextFore(m_Ctx.m_ColLabel);

	pGDI->SetTextTrans(modeTransparent);

	if( m_Ctx.m_dwTime ) {

		DrawInfoLeft(pGDI);

		DrawInfoCenter(pGDI);

		DrawInfoRight(pGDI);

		return;
		}

	DrawInfo(pGDI, 1, 1, "WAITING FOR DATA");
	}

void CPrimMurphyTrendViewer::DrawInfoLeft(IGDI *pGDI)
{
	DWORD t = m_Ctx.m_dwTime - 5 * m_Scale[m_Ctx.m_uScale].m_dwWidth;
	
	DrawInfo(pGDI, 0, 0, m_pFormat->Format(t / 5, typeInteger, fmtPad | fmtDate));

	DrawInfo(pGDI, 0, 1, m_pFormat->Format(t / 5, typeInteger, fmtPad | fmtTime));
	}

void CPrimMurphyTrendViewer::DrawInfoCenter(IGDI *pGDI)
{
	CString Text1;

	FormatWidth(Text1, m_Scale[m_Ctx.m_uScale].m_dwWidth);

	if( m_GridTime ) {

		CString Text2;

		FormatWidth(Text2, m_Scale[m_Ctx.m_uScale].m_dwStep0);

		DrawInfo(pGDI, 1, 0, "Width " + Text1 + "   Grid " + Text2);
		}
	else
		DrawInfo(pGDI, 1, 0, "Width " + Text1);

	if( m_Ctx.m_fMemory ) {

		CString Text;

		Text = "HISTORICAL";			

		if( m_Ctx.m_dwCursor ) {

			Text += " - CURSOR";
			}

		DrawInfo(pGDI, 1, 1, Text);		
		}
	else
		DrawInfo(pGDI, 1, 1, "OUT OF MEMORY");
	}

void CPrimMurphyTrendViewer::DrawInfoRight(IGDI *pGDI)
{
	DWORD t = m_Ctx.m_dwTime;
	
	DrawInfo(pGDI, 2, 0, m_pFormat->Format(t / 5, typeInteger, fmtPad | fmtDate));

	DrawInfo(pGDI, 2, 1, m_pFormat->Format(t / 5, typeInteger, fmtPad | fmtTime));
	}

void CPrimMurphyTrendViewer::DrawInfo(IGDI *pGDI, int j, int r, CString const &Text)
{
	int xp = m_x1 + 4 + j * (m_x2 - m_x1 - 8 - pGDI->GetTextWidth(Text)) / 2;

	int yp = m_y1 + 4 + r * (pGDI->GetTextHeight(Text) + 2);

	pGDI->TextOut(xp, yp, Text);
	}

void CPrimMurphyTrendViewer::DrawInfo(IGDI *pGDI, int j, int r, CUnicode const &Text)
{
	int xp = m_x1 + 4 + j * (m_x2 - m_x1 - 8 - pGDI->GetTextWidth(Text)) / 2;

	int yp = m_y1 + 4 + r * (pGDI->GetTextHeight(Text) + 2);

	pGDI->TextOut(xp, yp, Text);
	}

void CPrimMurphyTrendViewer::FormatWidth(CString &Text, DWORD dwTime)
{
	if( dwTime < 60 ) {

		Text.Printf("%us", dwTime);

		return;
		}

	if( dwTime % 60 ) {

		Text.Printf("%um%2.2s", dwTime / 60, dwTime % 60);

		return;
		}

	if( dwTime < 3600 ) {

		Text.Printf("%um", dwTime / 60);

		return;
		}

	if( dwTime % 3600 ) {

		Text.Printf("%uh%2.2m", dwTime / 3600, dwTime % 3600 / 60);

		return;
		}

	Text.Printf("%uh", dwTime / 3600);
	}

// Data Drawing

void CPrimMurphyTrendViewer::DrawPlot(IGDI *pGDI)
{
	if( m_pCache ) {

		int   xLeft  = m_x1 + m_xm;

		int   xRight = m_x2 - m_xm;

		int   xWidth = xRight - xLeft;

		DWORD dwFind = m_Ctx.m_dwCursor ? m_Ctx.m_dwCursor : m_Ctx.m_dwTime;

		UINT  uHead  = NOTHING;

		UINT  uTail  = NOTHING;

		UINT  uCount = 0;

		if( !m_pCache->IsEmpty() ) {

			UINT uSlot  = m_pCache->GetInitSlot();
			
			UINT uState = 0;

			for(;;) {

				DWORD dwTime  = m_pCache->GetSlotTime(uSlot);

				if( uState == 0 ) {

					if( dwTime <= m_Ctx.m_dwTime ) {

						if( dwTime == m_Ctx.m_dwTime || uHead == NOTHING ) {

							uHead  = uSlot;
	
							uCount = 1;
							}
						else
							uCount = 2;

						uState = 1;
						}
					else
						uHead = uSlot;
					}
				else {
					uCount++;

					if( dwTime <= m_Ctx.m_dwTime - m_Ctx.m_dwWide ) {

						uTail = uSlot;

						break;
						}
					}

				if( !m_pCache->GetNextSlot(uSlot) ) {

					uTail = uSlot;

					break;
					}
				}
			}

		if( uCount > 1 ) {

			struct CPos
			{
				int  x;
				int  y;
				bool m;
				};

			UINT   uChans = m_pCache->GetChanCount();

			UINT   uLimit = uCount * uChans;

			CPos * pPos   = New CPos [ uLimit ];

			UINT   uPtr   = 0;

			UINT   uSlot  = uHead;

			DWORD  dwTime = 0;

			DWORD  dwPrev = 0;

			// Lanes

			for(;;) {

				dwPrev = dwTime;

				dwTime = m_pCache->GetSlotTime(uSlot);

				int xp = xRight - (xWidth * int(m_Ctx.m_dwTime - dwTime) / int(m_Ctx.m_dwWide));

				for( UINT uChan = 0; uChan < uChans; uChan++ ) {

					if( m_Ctx.m_dwLineMask & (1<<uChan) ) {

						if( CanShowType(typeInteger) ) {

							UINT uData = m_pCache->GetSlotData(uSlot, uChan);

							if( dwTime == dwFind ) {

								m_pShowData[uChan] = uData;

								m_fShowData        = TRUE;
								}

							pPos[uPtr].x = xp;

							pPos[uPtr].y = ScaleData(uChan, uData);

							pPos[uPtr].m = FALSE;

							uPtr++;
							}
						}
					}

				if( uSlot == uTail ) {

					break;
					}

				if( !m_pCache->GetNextSlot(uSlot) ) {

					break;
					}
				}

			if( TRUE ) {

				UINT uUsed  = uPtr / uCount;

				UINT uRight = 0;

				UINT uLeft  = uPtr - uUsed;

				if( pPos[uRight].x > xRight ) {

					for( UINT c = uRight; c < uRight + uUsed; c++ ) {

						UINT p = c + uUsed;

						pPos[c].y = pPos[c].y - (pPos[c].y - pPos[p].y) * (pPos[c].x - xRight) / (pPos[c].x - pPos[p].x);

						pPos[c].x = xRight;
						}
					}

				if( pPos[uLeft].x < xLeft ) {

					for( UINT c = uLeft; c < uLeft + uUsed; c++ ) {

						UINT p = c - uUsed;

						pPos[c].y = pPos[c].y - (pPos[c].y - pPos[p].y) * (pPos[c].x - xLeft) / (pPos[c].x - pPos[p].x);

						pPos[c].x = xLeft;
						}
					}

				pGDI->SetPenTrans(modeTransparent);

				pGDI->SetPenStyle(penDotted);

				CRubyStroker rs;

				rs.SetEndStyle (rs.endFlat);

				rs.SetJoinStyle(rs.joinBevel);

				CRubyGdiLink gdi(pGDI);

				for( UINT uPass = 0; uPass < 2; uPass++ ) {

					UINT uInit = 0;

					for( UINT uChan = 0; uChan < uChans; uChan++ ) {

						if( m_Ctx.m_dwLineMask & (1<<uChan) ) {

							if( CanShowType(typeInteger) ) {

								COLOR ColorLine = m_pColPen [uChan % 16]->GetColor();

								UINT  uScan = uInit + uUsed;

								bool  fMiss = false;

								bool  fLine = (m_Ctx.m_dwLineMask & (1<<uChan));								

								if( uPass == 0 ) {
									
									fLine = false;
									}

								if( fLine ) {

									CRubyPath line;

									pGDI->SetPenFore(ColorLine);

									int xp;

									for( UINT uPos = 1; uPos < uCount; uPos++ ) {

										if( uPos == 1 || fMiss - pPos[uScan].m ) {
	
											if( uPos > 1 ) {

												if( !fMiss ) {

													if( fLine ) {

														line.AppendLineBreak();
														}
													}
												}

											if( (fMiss = pPos[uScan].m) ) {

												UINT uPrev = uScan - uUsed;

												pGDI->MoveTo((xp = pPos[uPrev].x), pPos[uPrev].y);
												}
											else {
												UINT uPrev = uScan - uUsed;

												if( fLine ) {

													line.Append((xp = pPos[uPrev].x), pPos[uPrev].y);
													}
												}
											}

										if( xp - pPos[uScan].x ) {

											if( fMiss ) {

												pGDI->LineTo((xp = pPos[uScan].x), pPos[uScan].y);
												}
											else {
												if( fLine ) {

													line.Append((xp = pPos[uScan].x), pPos[uScan].y);
													}
												}
											}

										uScan += uUsed;
										}

									if( line.GetCount() ) {

										if( !fMiss ) {

											line.AppendLineBreak();
											}

										CRubyPath    path;

										CRubyGdiList list;

										rs.StrokeOpen(path, line, 0, 0.25 * (m_PenWeight+1));

										list.Load(path, true);

										gdi.OutputSolid(list, ColorLine, 255);
										}
									}

								uInit++;
								}
							}
						}

					if( uPass == 0 ) {

						DrawGrid(pGDI);
						}
					}

				pGDI->ResetPen();
				}

			delete [] pPos;			
			}
		}
	}

int CPrimMurphyTrendViewer::ScaleData(UINT uChan, DWORD Data)
{
	C3REAL Min = 0;

	C3REAL Max = 0;

	C3REAL Val = 0;

	if( TRUE ) {

		if( m_GridMode == 3 ) {

			Min = m_DrawMin;
			
			Max = m_DrawMax;
			}
		else {
			Min = m_pCache->GetChanMin(uChan);

			Max = m_pCache->GetChanMax(uChan);
			}

		Val = C3REAL(C3INT(Data));
		}

	if( Max - Min ) {

		int y1 = m_pLane[uChan].yTop;

		int y2 = m_pLane[uChan].yBottom;

		int cy = y2 - y1;

		MakeMin(Val, Max);

		MakeMax(Val, Min);

		return y2 - int(cy * (Val - Min) / (Max - Min));
		}

	return m_y2 - 2;
	}

void CPrimMurphyTrendViewer::SetTextColor(IGDI *pGDI, UINT uChan)
{
	CPrimColor *pPen = m_pColPen[uChan % 16];

	COLOR      Color = pPen->GetColor();

	pGDI->SetTextFore(Color);
	}

void CPrimMurphyTrendViewer::SetPenColor(IGDI *pGDI, UINT uChan)
{
	CPrimColor *pPen = m_pColPen[uChan % 16];

	COLOR      Color = pPen->GetColor();

	pGDI->SetPenFore(Color);
	}

// Cursor Drawing

BOOL CPrimMurphyTrendViewer::DrawCursor(IGDI *pGDI)
{
	if( m_dwCursor ) {

		pGDI->ResetBrush();
		
		pGDI->SetBrushFore(m_Ctx.m_ColCursor);

		int px = TimeToPos(m_dwCursor);

		if( px - 4 > m_x1 ) {

			pGDI->FillWedge(px-4, m_y1,   px, m_y1+8, etQuad1);
			
			pGDI->FillWedge(px-4, m_y2-8, px, m_y2,   etQuad4);
			}

		pGDI->FillRect(px, m_y1, px+1, m_y2);

		if( px + 5 < m_x2 ) {

			pGDI->FillWedge(px+1, m_y2-8, px+5, m_y2,   etQuad3);
			
			pGDI->FillWedge(px+1, m_y1,   px+5, m_y1+8, etQuad2);
			}

		if( TRUE ) {

			CUnicode Text = m_pFormat->Format(m_dwCursor / 5, typeInteger, fmtPad | fmtTime);

			pGDI->ResetFont();

			SelectFont(pGDI, m_FontData);

			int  cx = pGDI->GetTextWidth(Text) + 4;

			int  cy = pGDI->GetTextHeight(Text) + 2;

			int  xp = (px + cx) > m_x2 ? px - cx : px + 4;

			int  yp = m_y2 - cy;
			
			pGDI->SetTextFore(m_Ctx.m_ColCursor);

			pGDI->SetBackMode(modeTransparent);

			pGDI->TextOut(xp, yp, Text);
			}
		
		return TRUE;
		}

	return FALSE;
	}

DWORD CPrimMurphyTrendViewer::PosToTime(int xPos)
{
	if( xPos >= m_x1 && xPos < m_x2 ) {

		int   xRight = m_x2 - m_xm;

		int   xWidth = m_x2 - m_x1 - m_xm;

		DWORD dwTime = m_Ctx.m_dwTime - ((xRight - xPos) * m_Ctx.m_dwWide / xWidth);		

		if( TRUE ) {

			UINT   uSlot = m_pCache->GetInitSlot();

			DWORD     t1 = m_pCache->GetSlotTime(uSlot);

			DWORD dwInit = t1;

			while( m_pCache->GetNextSlot(uSlot) ) {

				DWORD t2 = m_pCache->GetSlotTime(uSlot);

				if( dwTime >= t2 && dwTime <= t1 ) {

					return dwTime <= (t1 + t2) / 2 ? t2 : t1;
					}

				t1 = t2;
				}			

			return dwInit;
			}
		else {
			DWORD dwTick = 5 * m_pCache->GetPitchTime();

			dwTime += dwTick / 2;

			dwTime /= dwTick;

			dwTime *= dwTick;

			return dwTime;
			}
		}

	return 0;
	}

int CPrimMurphyTrendViewer::TimeToPos(DWORD dwTime)
{
	int xRight = m_x2 - m_xm;

	int xWidth = m_x2 - m_x1 - m_xm;

	return xRight - ((m_Ctx.m_dwTime - dwTime) * xWidth / m_Ctx.m_dwWide);
	}

// Data Values

void CPrimMurphyTrendViewer::DrawData(IGDI *pGDI)
{
	if( (m_ShowData || m_Ctx.m_dwCursor) && m_fShowData ) {

		int xp = m_x1 + 1;

		int yp = m_y2 + 2;

		if( m_DataBox == 1 ) {

			xp = m_Work.x1 + 1;

			yp = m_y1 + 2;
			}

		if( m_DataBox == 2 ) {

			xp = m_x2 + 1;

			yp = m_y1 + 2;
			}

		pGDI->ResetFont();

		SelectFont(pGDI, m_FontData);

		pGDI->SetTextTrans(modeTransparent);

		pGDI->SetTextFore(m_Ctx.m_ColData);

		UINT uChans = m_pCache->GetChanCount();

		for( UINT uChan = 0; uChan < uChans; uChan++ ) {

			if( m_Ctx.m_dwLineMask & (1<<uChan) ) {

				if( CanShowType(typeInteger) ) {

					SetTextColor(pGDI, uChan);

					CDispFormatNumber Format;

					CUnicode Text = Format.Format(m_pShowData[uChan], typeInteger, fmtStd);

					int xi = m_Inits[uChan].x;

					int yi = m_Inits[uChan].y;

					pGDI->TextOut(xp + xi, yp + yi, Text);
					}
				}
			}
		}
	}

// Layout Helpers

void CPrimMurphyTrendViewer::LayoutCore(IGDI *pGDI)
{
	m_x1 = m_Work.x1;

	m_x2 = m_Work.x2;

	m_y1 = m_Work.y1;
	
	m_y2 = m_Work.y2;

	m_xm = 1;

	if( m_PenWeight > 4 ) m_xm++;

	if( m_PenWeight > 8 ) m_xm++;

	LayoutData(pGDI);
	}

BOOL CPrimMurphyTrendViewer::LayoutData(IGDI *pGDI)
{
	if( m_ShowData || m_Ctx.m_dwCursor ) {

		SelectFont(pGDI, m_FontData);

		int xi = 0;

		int yi = 0;

		int wx = (m_DataBox == 0) ? m_x2 - m_x1 - 2 : 0;

		UINT uChans = m_pCache->GetChanCount();

		for( UINT uChan = 0; uChan < uChans; uChan++ ) {

			if( m_Ctx.m_dwLineMask & (1<<uChan) ) {

				if( CanShowType(typeInteger) ) {

					CDispFormatNumber Format;

					CUnicode Text = Format.Format(0xFFFFFFFF, typeInteger, fmtPad);

					int cx = pGDI->GetTextWidth(Text);

					int cy = pGDI->GetTextHeight("X");

					if( m_DataBox == 0 ) {

						if( xi || yi ) {

							if( xi + cx >= wx ) {

								xi    = 0;
							
								yi    = yi + cy + 2;

								m_y2 -= (cy + 2);
								}
							}
						else 
							m_y2 -= (cy + 2);

						m_Inits[uChan].x = xi;

						m_Inits[uChan].y = yi;

						xi += cx + 10;
						}
					else {				
						m_Inits[uChan].x = xi;

						m_Inits[uChan].y = yi;

						yi += cy + 2;

						if( cx > wx ) {
						
							wx = cx;
							}
						}
					}
				}
			}

		switch( m_DataBox ) {
			
			case 0:
				m_y2  -= 4;
				break;		

			case 1:
				m_x1 += wx;
				break;

			case 2:
				m_x2 -= wx + 1;
				break;
			}

		return TRUE;
		}

	return FALSE;
	}

void CPrimMurphyTrendViewer::LayoutGrid(IGDI *pGDI)
{
	if( m_GridMode == 3 ) {

		m_yGap     = 10;

		m_Limit    = min(10, (m_y2 - m_y1) / (m_yGap + 4));

		m_GridSpan = fabs(m_Ctx.m_GridMax - m_Ctx.m_GridMin);

		m_DrawStep = FindStep(m_GridSpan, m_Limit);

		m_StepMin  = int(floor(m_Ctx.m_GridMin / m_DrawStep));

		m_StepMax  = int(ceil (m_Ctx.m_GridMax / m_DrawStep));

		m_StepSpan = m_StepMax - m_StepMin;
		}
	}

// Implementation

double CPrimMurphyTrendViewer::FindStep(double Range, double Limit)
{
	double Gap  = Range / Limit;

	double Mult = pow(10.0, floor(log10(Gap)));

	for( int p = 0; p < 2; p++ ) {

		double nList[] = { 1, 2, 5, 10 };

		for( int n = 0; n < 4; n++ ) {

			double Find = nList[n] * Mult;

			if( Gap <= Find ) {

				if( p == 1 ) {
					
					return Find;
					}
				
				if( Range / Find == floor(Range / Find) ) {

					return Find;
					}
				}
			}
		}

	AfxAssert(FALSE);

	return Range;
	}

void CPrimMurphyTrendViewer::FindDrawLimits(void)
{
	if( m_GridMode == 3 ) {

		if( !m_Precise && m_StepSpan ) {

			m_DrawMin = m_StepMin * m_DrawStep;

			int iLast = max(1, m_StepSpan);

			m_DrawMax = (m_StepMin + iLast) * m_DrawStep;
			}
		else {
			m_DrawMin = m_Ctx.m_GridMin;

			m_DrawMax = m_Ctx.m_GridMax;
			}
		}
	}

BOOL CPrimMurphyTrendViewer::CanShowType(UINT Type)
{
	return Type == typeInteger || Type == typeReal;
	}

void CPrimMurphyTrendViewer::WriteTime(CCodedItem *pItem, DWORD Data)
{
	if( pItem ) {

		if( pItem->IsAvail() ) {

			UINT  Type = pItem->GetType();
				
			if( Type == typeReal ) {

				pItem->SetValue(R2I(Data), Type, setNone);
				}
			else {
				pItem->SetValue(DWORD(Data), Type, setNone);
				}
			}
		}
	}

void CPrimMurphyTrendViewer::MakeShowData(void)
{
	if( m_pCache ) {

		if( m_pShowData ) {

			delete [] m_pShowData;

			m_pShowData = NULL;
			}

		UINT uChans = m_pCache->GetChanCount();

		m_pShowData = New DWORD [ uChans ];

		memset(m_pShowData, 0, uChans * sizeof(DWORD));
		}
	}

void CPrimMurphyTrendViewer::LayoutLanes(void)
{
	if( m_pCache ) {

		UINT uChans   = m_pCache->GetChanCount();

		UINT uPerLane = m_Ctx.m_uPerLane;

		MakeMin(uPerLane, uChans);

		if( m_pLane ) {

			delete [] m_pLane;

			m_pLane = NULL;
			}		

		m_pLane       = New CLane [ uChans ];

		int yTop      = m_y1 + 2;

		int yBottom   = m_y2 - 2;

		int yHeight   = yBottom - yTop;

		UINT uLanes   = uPerLane ? (uChans + uPerLane - 1) / uPerLane : 1;

		int nLane     = yHeight / uLanes;

		UINT uCount   = 0;

		for( UINT uChan = 0; uChan < uChans; uChan ++ ) {

			CLane & Lane = m_pLane[uChan];

			Lane.yTop    = yTop;

			Lane.yBottom = Lane.yTop + nLane;

			if( uPerLane ) {

				if( ++uCount >= uPerLane ) {

					uCount = 0;

					yTop += nLane;
					}
				}
			}
		}
	}

// Context Handling

void CPrimMurphyTrendViewer::FindCtx(CCtx &Ctx)
{
	Ctx.m_uScale   = m_uScale;

	Ctx.m_fLive    = m_fLive;

	Ctx.m_dwCursor = m_dwCursor;

	Ctx.m_dwTime   = Ctx.m_fLive ? m_dwLast : m_dwTime;

	Ctx.m_dwWide   = 5 * m_Scale[Ctx.m_uScale].m_dwWidth;

	Ctx.m_dwLeft   = Ctx.m_dwTime - Ctx.m_dwWide;
	
	if( m_GridMode == 3 ) {

		Ctx.m_GridMin = GetItemData(m_pGridMin, C3REAL(  0.0));

		Ctx.m_GridMax = GetItemData(m_pGridMax, C3REAL(100.0));
		}
	else {
		Ctx.m_GridMin = 0.0;

		Ctx.m_GridMax = 0.0;
		}

	if( Ctx.m_dwCursor ) {

		MakeMin(Ctx.m_dwCursor, Ctx.m_dwTime);

		MakeMax(Ctx.m_dwCursor, Ctx.m_dwLeft);
		}
	
	Ctx.m_ColTitle   = m_pColTitle ->GetColor();
	
	Ctx.m_ColLabel   = m_pColLabel ->GetColor();
	
	Ctx.m_ColData    = m_pColData  ->GetColor();
	
	Ctx.m_ColMajor   = m_pColMajor ->GetColor();
	
	Ctx.m_ColMinor   = m_pColMinor ->GetColor();
	
	Ctx.m_ColCursor  = m_pColCursor->GetColor();

	Ctx.m_dwLineMask = GetItemData(m_pPenMask,  C3INT(NOTHING));

	Ctx.m_uPerLane   = GetItemData(m_pPerLane,  C3INT(0));

	Ctx.m_fMemory    = m_fMemory;

	Ctx.m_fLoad      = m_fLoad;

	CUnicode File;

	if( m_pFile && m_pFile->IsAvail() ) {

		File = m_pFile->GetText();
		}

	Ctx.m_File      = File;
	}

void CPrimMurphyTrendViewer::ClearCtx(CCtx &Ctx)
{
	Ctx.m_uScale   = 0;

	Ctx.m_dwCursor = 0;

	Ctx.m_dwTime   = 0;

	Ctx.m_dwWide   = 0;

	Ctx.m_dwLeft   = 0;

	Ctx.m_fLive    = 0;

	Ctx.m_GridMin = 0.0;

	Ctx.m_GridMax = 0.0;
	
	Ctx.m_ColTitle   = 0;
	
	Ctx.m_ColLabel   = 0;
	
	Ctx.m_ColData    = 0;
	
	Ctx.m_ColMajor   = 0;
	
	Ctx.m_ColMinor   = 0;
	
	Ctx.m_ColCursor  = 0;

	Ctx.m_dwLineMask = 0;

	Ctx.m_uPerLane   = 0;

	Ctx.m_fMemory    = 0;

	Ctx.m_fLoad      = 0;

	Ctx.m_File.Empty();
	}

// Context Check

BOOL CPrimMurphyTrendViewer::CCtx::operator == (CCtx const &That) const
{
	if( !m_File.CompareC(That.m_File) ) {

		return m_uScale     == That.m_uScale      &&
		       m_dwCursor   == That.m_dwCursor    &&
		       m_dwTime     == That.m_dwTime      &&
		       m_dwWide     == That.m_dwWide      &&
		       m_dwLeft     == That.m_dwLeft      &&
		       m_fLive      == That.m_fLive       &&
		       m_GridMin    == That.m_GridMin     &&
		       m_GridMax    == That.m_GridMax     &&
		       m_ColTitle   == That.m_ColTitle    &&
		       m_ColLabel   == That.m_ColLabel    &&
		       m_ColData    == That.m_ColData     &&
		       m_ColMajor   == That.m_ColMajor    &&
		       m_ColMinor   == That.m_ColMinor    &&
		       m_ColCursor  == That.m_ColCursor   &&
		       m_dwLineMask == That.m_dwLineMask  &&
		       m_uPerLane   == That.m_uPerLane    &&
		       m_fMemory    == That.m_fMemory     &&
		       m_fLoad      == That.m_fLoad       ;;
		       }

	return FALSE;
	}


// End of File
