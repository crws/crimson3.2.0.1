
#include "intern.hpp"

#include "remote.hpp"

#include "uitask.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Remote Display
//

// Constructor

CPrimRemoteDisplay::CPrimRemoteDisplay(void)
{
	m_fInit    = FALSE;

	m_pData    = NULL;

	m_pDisplay = NULL;

	CUISystem::m_pThis->m_pTask->RegisterPrim(this);
	}

// Destructor

CPrimRemoteDisplay::~CPrimRemoteDisplay(void)
{
	delete [] m_pData;

	if( m_pDisplay ) {

		m_pDisplay->Remove(m_uHandle);
		}

	CUISystem::m_pThis->m_pTask->DeregisterPrim(this);
	}

// Initialization

void CPrimRemoteDisplay::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRemoteDisplay", pData);

	CPrim::Load(pData);

	m_uPort     = GetByte(pData);

	m_uDrop     = GetByte(pData);

	m_fPortrait = GetByte(pData) ? TRUE : FALSE;
	}

// Overridables

void CPrimRemoteDisplay::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( !m_fInit ) {

		IProxyDisplayList *pDisplays = CCommsSystem::m_pThis->m_pComms->m_pDisplays;

		if( pDisplays ) {

			m_pDisplay  = pDisplays->FindProxy(m_uPort);
			}

		int      cx = m_DrawRect.x2 - m_DrawRect.x1;

		int      cy = m_DrawRect.y2 - m_DrawRect.y1;

		UINT uAlloc = (cx * 2) * cy;

		m_pData     = New BYTE [ uAlloc ];

		m_fInit     = TRUE;
		}

	CPrim::DrawPrep(pGDI, Erase, Trans);
	}

void CPrimRemoteDisplay::DrawExec(IGDI *pGDI, IRegion *pDirty)
{
	if( m_fShow ) {

		DrawPrim(pGDI);
		}
	}

void CPrimRemoteDisplay::DrawPrim(IGDI *pGDI)
{
	if( WhoHasFeature(rfRemoteDisplay) ) {

		if( m_pDisplay ) {

			int cx = DispGetCx();

			int cy = DispGetCy();

			int px = m_DrawRect.x1;

			int py = m_DrawRect.y1;

			int dx = m_DrawRect.x2 - m_DrawRect.x1;

			int dy = m_DrawRect.y2 - m_DrawRect.y1;

			MakeMin(dx, cx - px);

			MakeMin(dy, cy - py);

			PDWORD  pDest = PDWORD(m_pData);

			PCDWORD pFrom = PDWORD(pGDI->GetBuffer()) + px + py * cx;

			for( int r = 0; r < dy; r++ ) {

				for( int n = 0; n < dx; n++ ) {

					*pDest++ = *pFrom++;
					}

				pFrom += cx - dx;
				}

			m_pDisplay->Render( m_uHandle,
					    m_uDrop, 
					    m_pData, 
					    cx, 
					    cy, 
					    m_fPortrait
					    );
			}
		}
	}

// End of File
