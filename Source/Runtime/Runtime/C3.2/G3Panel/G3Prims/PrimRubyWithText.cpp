
#include "intern.hpp"

#include "PrimRubyWithText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive with Text
//

// Constructor

CPrimRubyWithText::CPrimRubyWithText(void)
{
	}

// Initialization

void CPrimRubyWithText::Load(PCBYTE &pData)
{
	CPrimWithText::Load(pData);

	m_bound.x1 = short(GetWord(pData) + 0);
	m_bound.y1 = short(GetWord(pData) + 0);
	m_bound.x2 = short(GetWord(pData) + 1);
	m_bound.y2 = short(GetWord(pData) + 1);
	}

// Overridables

R2 CPrimRubyWithText::GetBackRect(void)
{
	return m_bound;
	}

void CPrimRubyWithText::MovePrim(int cx, int cy)
{
	m_bound.x1 += cx;
	m_bound.y1 += cy;
	m_bound.x2 += cx;
	m_bound.y2 += cy;

	CPrimWithText::MovePrim(cx, cy);
	}

// Implementation

BOOL CPrimRubyWithText::LoadList(PCBYTE &pData, CRubyGdiList &list)
{
	UINT n;

	if( (n = GetWord(pData)) ) {

		list.m_uCount = n;

		list.m_pCount = New UINT [ n ];

		for( UINT c = 0; c < n; c++ ) {

			list.m_pCount[c] = GetWord(pData);
			}
		}
	else {
		list.m_uCount = 0;

		list.m_pCount = NULL;
		}

	if( (n = GetWord(pData)) ) {

		list.m_uList = n;

		list.m_pList = New P2 [ n ];

		for( UINT c = 0; c < n; c++ ) {

			list.m_pList[c].x = int(short(GetWord(pData)));

			list.m_pList[c].y = int(short(GetWord(pData)));
			}
		}
	else {
		list.m_uList = 0;

		list.m_pList = NULL;
		}

	return TRUE;
	}

// End of File
