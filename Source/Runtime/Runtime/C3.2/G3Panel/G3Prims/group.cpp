
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Group Primitive
//

// Constructor

CPrimGroup::CPrimGroup(void)
{
	}

// Destructor

CPrimGroup::~CPrimGroup(void)
{
	}

// Initialization

void CPrimGroup::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimGroup", pData);

	CPrimSet::Load(pData);
	}

// Overridables

void CPrimGroup::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimSet::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		PrepList(pGDI, Erase, Trans);
		}
	}

UINT CPrimGroup::OnMessage(UINT uMsg, UINT uParam)
{
	if( uMsg == msgIsGroup ) {

		return 1;
		}

	return CPrimSet::OnMessage(uMsg, uParam);
	}

// End of File
