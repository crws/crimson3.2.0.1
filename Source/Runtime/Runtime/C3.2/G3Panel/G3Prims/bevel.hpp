
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BEVEL_HPP
	
#define	INCLUDE_BEVEL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimBevelBase;
class CPrimBevel;
class CPrimBevelButton;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Bevel Base Class
//

class CPrimBevelBase : public CPrimWithText
{
	public:
		// Constructor
		CPrimBevelBase(void);

		// Destructor
		~CPrimBevelBase(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void SetScan(UINT Code);

		// Data Members
		UINT         m_Border;
		CPrimBrush * m_pFace;
		CPrimColor * m_pHilite;
		CPrimColor * m_pShadow;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR m_Hilite;
			COLOR m_Shadow;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Implementation
		void FindPoints(P2 *t, P2 *b, R2 &r);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Bevel
//

class CPrimBevel : public CPrimBevelBase
{
	public:
		// Constructor
		CPrimBevel(void);

		// Destructor
		~CPrimBevel(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);

		// Data Members
		UINT m_Type;
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Bevel Button
//

class CPrimBevelButton : public CPrimBevelBase
{
	public:
		// Constructor
		CPrimBevelButton(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);
	};

// End of File

#endif
