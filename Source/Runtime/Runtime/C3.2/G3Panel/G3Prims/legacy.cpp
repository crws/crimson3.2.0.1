
#include "intern.hpp"

#include "legacy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Scale
//

// Constructor

CPrimLegacyScale::CPrimLegacyScale(void)
{
	m_pLine     = New CPrimColor(naFeature);

	m_pBack     = New CPrimBrush;

	m_Style     = 0;
	
	m_Orient    = 0;

	m_Major     = 5;

	m_Minor     = 2;

	m_Interval  = 1;

	m_pLimitMin = NULL;

	m_pLimitMax = NULL;
	}

// Destructor

CPrimLegacyScale::~CPrimLegacyScale(void)
{
	delete m_pLine;

	delete m_pBack;
	}

// Initialization

void CPrimLegacyScale::Load(PCBYTE &pData)
{
	CPrim::Load(pData);

	m_pLine->Load(pData);

	m_pBack->Load(pData);

	m_Orient = GetByte(pData);
	
	m_Major  = GetByte(pData);
	
	m_Minor  = GetByte(pData);

	if( (m_Style = GetByte(pData)) ) {
		
		GetCoded(pData, m_pLimitMin);
		
		GetCoded(pData, m_pLimitMax);
	
		m_Interval = GetByte(pData);
		}
	}

// Overridables

void CPrimLegacyScale::SetScan(UINT Code)
{
	m_pLine->SetScan(Code);

	m_pBack->SetScan(Code);
	
	SetItemScan(m_pLimitMin, Code);
	
	SetItemScan(m_pLimitMax, Code);

	CPrim::SetScan(Code);
	}

void CPrimLegacyScale::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrim::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_pBack->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}
		}
	}

// Context Creation

void CPrimLegacyScale::FindCtx(CCtx &Ctx)
{
	if( m_Style == 1 ) {

		C3INT Min = m_pLimitMin ? m_pLimitMin->ExecVal() : 0;

		C3INT Max = m_pLimitMax ? m_pLimitMax->ExecVal() : 100;

		Ctx.m_Major = abs(Max - Min) / m_Interval;
		
		Ctx.m_Minor = m_Minor;

		if( Ctx.m_Major == 0 ) {

			Ctx.m_Major = 1;
			}
		}
	else {
		Ctx.m_Minor = m_Minor;

		Ctx.m_Major = m_Major;
		}
	}

// Context Check

BOOL CPrimLegacyScale::CCtx::operator == (CCtx const &That) const
{
	return m_Minor == That.m_Minor &&
	       m_Major == That.m_Major ;;
	}

// Implementation

int CPrimLegacyScale::MulDivRound(int a, int b, int c)
{
	return (a * b + c / 2) / c;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Scale
//

// Constructor

CPrimLegacyVertScale::CPrimLegacyVertScale(void)
{
	}

// Initialization

void CPrimLegacyVertScale::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyVertScale", pData);
	
	CPrimLegacyScale::Load(pData);
	}

// Overridables

void CPrimLegacyVertScale::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	pGDI->ResetBrush();

	m_pBack->FillRect(pGDI, Rect);

	pGDI->SetBrushFore(m_pLine->GetColor());
	
	int xp = Rect.x1;

	int yp = Rect.y1;

	int cx = Rect.x2 - Rect.x1;

	int cy = Rect.y2 - Rect.y1;

	if( m_Orient ) {

		pGDI->FillRect(xp + cx - 1, yp, xp + cx, yp + cy);
		}
	else
		pGDI->FillRect(xp, yp, xp + 1, yp + cy);
	
	int xMajor = cx;
	
	int xMinor = max(cx / 2, 2);
	
	int nDiv = m_Ctx.m_Major * m_Ctx.m_Minor;

	for( int nPos = 0; nPos <= nDiv; nPos++ ) {

		int yd = yp + MulDivRound(cy - 1, nPos, nDiv);

		int xs = (nPos % m_Ctx.m_Minor) ? xMinor : xMajor;

		if( m_Orient ) {

			pGDI->FillRect(xp + cx - xs, yd, xp + cx, yd + 1);
			}
		else
			pGDI->FillRect(xp, yd, xp + xs, yd + 1);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Scale
//

// Constructor

CPrimLegacyHorzScale::CPrimLegacyHorzScale(void)
{
	}

// Initialization

void CPrimLegacyHorzScale::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyHorzScale", pData);
	
	CPrimLegacyScale::Load(pData);
	}

// Overridables

void CPrimLegacyHorzScale::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	pGDI->ResetBrush();

	m_pBack->FillRect(pGDI, Rect);

	pGDI->SetBrushFore(m_pLine->GetColor());
	
	int xp = Rect.x1;

	int yp = Rect.y1;

	int cx = Rect.x2 - Rect.x1;

	int cy = Rect.y2 - Rect.y1;

	int yMajor = cy;
	
	int yMinor = max(cy / 2, 2);

	if( m_Orient ) {

		pGDI->FillRect(xp, yp + cy - 1, xp + cx, yp + cy);
		}
	else
		pGDI->FillRect(xp, yp, xp + cx, yp + 1);

	int nDiv = m_Ctx.m_Major * m_Ctx.m_Minor;

	for( int nPos = 0; nPos <= nDiv; nPos++ ) {

		int xd = xp + MulDivRound(cx - 1, nPos, nDiv);

		int ys = (nPos % m_Ctx.m_Minor) ? yMinor : yMajor;

		if( m_Orient ) {

			pGDI->FillRect(xd, yp + cy - ys, xd + 1, yp + cy);
			}
		else
			pGDI->FillRect(xd, yp, xd + 1, yp + ys);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Figure
//

// Constructor

CPrimLegacyFigure::CPrimLegacyFigure(void)
{
	m_pEdge = New CPrimPen;

	m_pBack = New CPrimBrush;
	}

// Destructor

CPrimLegacyFigure::~CPrimLegacyFigure(void)
{
	delete m_pEdge;

	delete m_pBack;
	}

// Initialization

void CPrimLegacyFigure::Load(PCBYTE &pData)
{
	CPrim::Load(pData);

	m_pEdge->Load(pData);

	m_pBack->Load(pData);
	}

// Overridables

void CPrimLegacyFigure::SetScan(UINT Code)
{
	m_pEdge->SetScan(Code);

	m_pBack->SetScan(Code);

	CPrim::SetScan(Code);
	}

void CPrimLegacyFigure::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrim::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		if( m_pBack->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}
		}
	}

void CPrimLegacyFigure::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;
	
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);
		}

	pGDI->ResetBrush();

	m_pBack->FillRect(pGDI, Rect);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Figure
//

// Constructor

CPrimLegacyShadow::CPrimLegacyShadow(void)
{
	}

// Destructor

CPrimLegacyShadow::~CPrimLegacyShadow(void)
{
	}

// Initialization

void CPrimLegacyShadow::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyShadow", pData);
	
	CPrimLegacyFigure::Load(pData);

	m_Style  = GetByte(pData);

	m_Border = GetByte(pData);
	}

// Overridables

void CPrimLegacyShadow::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	if( m_Style == 0 ) {

		R2 Back = Rect;

		int s =	2 * m_pEdge->m_Width;

		MakeMax(s, 2);

		Back.x1 += s;
		Back.y1 += s;

		Rect.x2 -= s;
		Rect.y2 -= s;

		m_pEdge->DrawRect(pGDI, Back);

		m_pBack->FillRect(pGDI, Rect);

		m_pEdge->DrawRect(pGDI, Rect);
		}
	else {
		COLOR Shadow = GetRGB(15,15,15);

		COLOR Hilite = GetRGB(31,31,31);
		
		P2 h[6];
		
		P2 s[6];

		FindPoints(h, s, Rect);

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(Shadow);

		pGDI->FillPolygon((m_Style == 2) ? h : s, 6, 0);

		pGDI->SetBrushFore(Hilite);

		pGDI->FillPolygon((m_Style == 2) ? s : h, 6, 0);
		
		m_pBack->FillRect(pGDI, Rect);
		}
	}

// Implementation

void CPrimLegacyShadow::FindPoints(P2 *t, P2 *b, R2 &r)
{
	R2 Rect1 = r;

	R2 Rect2 = r;

	DeflateRect(Rect2, m_Border - 1, m_Border - 1);

	Rect1.x2--;
	Rect1.y2--;
	Rect2.x2--;
	Rect2.y2--;

	t[0].x = Rect1.x1; t[0].y = Rect1.y2;
	t[1].x = Rect1.x1; t[1].y = Rect1.y1;
	t[2].x = Rect1.x2; t[2].y = Rect1.y1;
	t[3].x = Rect2.x2; t[3].y = Rect2.y1;
	t[4].x = Rect2.x1; t[4].y = Rect2.y1;
	t[5].x = Rect2.x1; t[5].y = Rect2.y2;

	b[0].x = Rect1.x1; b[0].y = Rect1.y2;
	b[1].x = Rect1.x2; b[1].y = Rect1.y2;
	b[2].x = Rect1.x2; b[2].y = Rect1.y1;
	b[3].x = Rect2.x2; b[3].y = Rect2.y1;
	b[4].x = Rect2.x2; b[4].y = Rect2.y2;
	b[5].x = Rect2.x1; b[5].y = Rect2.y2;
	    
	Rect2.x1 += 1;
	Rect2.y1 += 1;

	r = Rect2;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy CF Image
//

// Constructor

CPrimLegacyCfImage::CPrimLegacyCfImage(void)
{
	m_pValue = NULL;

	m_pShow  = NULL;

	m_pImage = NULL;
	}

// Destructor

CPrimLegacyCfImage::~CPrimLegacyCfImage(void)
{
	delete m_pValue;

	delete m_pShow;

	delete m_pImage;
	}

// Initialization

void CPrimLegacyCfImage::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyCfImage", pData);

	CPrimLegacyFigure::Load(pData);

	GetCoded(pData, m_pValue);

	GetCoded(pData, m_pShow);
	}

// Overridables

void CPrimLegacyCfImage::SetScan(UINT Code)
{
	CPrimLegacyFigure::SetScan(Code);

	SetItemScan(m_pValue, Code);

	SetItemScan(m_pShow,  Code);
	}

void CPrimLegacyCfImage::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimLegacyFigure::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			if( !(m_Ctx.m_Value == Ctx.m_Value) ) {

				delete [] m_pImage;

				m_pImage = NULL;

				LoadImage(Ctx.m_Value);
				}

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			} 

		if( !m_pImage ) {

			if( LoadImage(m_Ctx.m_Value) ) {

				m_fChange = TRUE;
				}
			}
		}
	}

void CPrimLegacyCfImage::DrawPrim(IGDI *pGDI)
{
	CPrimLegacyFigure::DrawPrim(pGDI);

	if( m_Ctx.m_Show ) {

		if( m_pImage ) {

			int rop = ropSRCCOPY;

			rop    |= (m_pImage[5] & 0x01) ? ropRLE   : 0;

			rop    |= (m_pImage[5] & 0x02) ? ropTrans : 0;

			int s   = IntelToHost(PWORD(m_pImage)[3]);

			int ix  = IntelToHost(PWORD(m_pImage)[4]);

			int iy  = IntelToHost(PWORD(m_pImage)[5]);

			int xp  = m_DrawRect.x1;

			int yp  = m_DrawRect.y1;

			int cx  = m_DrawRect.x2 - m_DrawRect.x1;

			int cy  = m_DrawRect.y2 - m_DrawRect.y1;

			if( ix <= cx && iy <= cy ) {

				xp += (cx - ix) / 2;

				yp += (cy - iy) / 2;

				pGDI->BitBlt(	xp,
						yp,
						ix,
						iy,
						s,
						m_pImage + 12,
						rop
						);
				}
			else {
				ShowMessage(pGDI, "PRIMITIVE TOO SMALL TO SHOW IMAGE");
				}
			}
		}
	}

// Context Creation

void CPrimLegacyCfImage::FindCtx(CCtx &Ctx)
{
	if( IsItemAvail(m_pValue) && IsItemAvail(m_pShow) ) {

		Ctx.m_Value = GetItemData(m_pValue, -1);

		Ctx.m_Show  = GetItemData(m_pShow,   1);
		}
	else {
		Ctx.m_Value = -1;

		Ctx.m_Show  =  0;
		}
	}

// Context Check

BOOL CPrimLegacyCfImage::CCtx::operator == (CCtx const &That) const
{
	return m_Value == That.m_Value &&
	       m_Show  == That.m_Show  ;;
	}

// Implementation

BOOL CPrimLegacyCfImage::LoadImage(C3INT Image)
{
	CPrintf   Name("C:\\pics\\pic%3.3u.g3p", Image);

	CAutoFile File(Name, "r");

	if( File ) {

		UINT uSize = File.GetSize();

		m_pImage   = New BYTE [ uSize ];

		SetTaskLimit(50, 5);

		if( File.Read(m_pImage, uSize) == uSize ) {

			SetTaskLimit(0, 0);

			char c = m_pImage[2];

			if( c == 'W' || c == 'G' || c == 'V' ) {

				// NOTE -- Kill RLE on 16-bit images.

				m_pImage[5] &= 0x02;
				}
					
			return TRUE;
			}

		SetTaskLimit(0, 0);

		delete [] m_pImage;

		m_pImage = NULL;
		}

	return FALSE;
	}

void CPrimLegacyCfImage::ShowMessage(IGDI *pGDI, PCTXT pMsg)
{
	int yt = (m_DrawRect.y1 + m_DrawRect.y2 - pGDI->GetTextHeight(pMsg)) / 2;

	int xt = (m_DrawRect.x1 + m_DrawRect.x2 - pGDI->GetTextWidth(pMsg) ) / 2;

	pGDI->TextOut(xt, yt, pMsg);
	}

// End of File
