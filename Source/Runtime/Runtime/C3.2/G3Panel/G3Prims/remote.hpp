
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_REMOTE_HPP
	
#define	INCLUDE_REMOTE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Remote Display
//

class CPrimRemoteDisplay : public CPrim
{
	public:
		// Constructor
		CPrimRemoteDisplay(void);

		// Destructor
		~CPrimRemoteDisplay(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawExec(IGDI *pGDI, IRegion *pDirty);
		void DrawPrim(IGDI *pGDI);

		// Configuration Data
		UINT m_uPort;
		UINT m_uDrop;
		BOOL m_fPortrait;

	protected:
		// Data
		BOOL	        m_fInit;
		IProxyDisplay * m_pDisplay;
		PBYTE		m_pData;
	};

// End of File

#endif
