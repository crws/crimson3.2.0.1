

#include "intern.hpp"

#include "PrimRubyGaugeTypeAR.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Radial Gauge Primitive
//

// Constructor

CPrimRubyGaugeTypeAR::CPrimRubyGaugeTypeAR(void)
{
	m_pGdi = NULL;
	}

// Destructor

CPrimRubyGaugeTypeAR::~CPrimRubyGaugeTypeAR(void)
{
	if( m_pGdi ) {

		m_pGdi->Release();

		delete [] m_pData;

		delete [] m_pCopy;
		}
	}

// Initialization

void CPrimRubyGaugeTypeAR::MovePrim(int cx, int cy)
{
	CPrimRubyGaugeTypeA::MovePrim(cx, cy);

	m_listPivot1.Translate(cx, cy, true);

	m_listPivot2.Translate(cx, cy, true);

	m_listPivot2.GetBoundingRect(m_rectPivot, true);
	}

void CPrimRubyGaugeTypeAR::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubyGaugeTypeAR", pData);

	CPrimRubyGaugeTypeA::Load(pData);

	if( !IsNaked() ) {

		m_cx    = m_bound.x2 - m_bound.x1;

		m_cy    = m_bound.y2 - m_bound.y1;

		m_pData = New DWORD [ m_cx * m_cy ];

		m_pCopy = New DWORD [ m_cx * m_cy ];

		AfxNewObject("gdi", IGdi, m_pGdi);

		m_pGdi->Create(m_cx, m_cy, m_pData);
		}

	m_PointStyle = GetByte(pData);

	m_Color[0] = GetWord(pData);
	m_Color[1] = GetWord(pData);

	LoadList(pData, m_listPivot1);
	LoadList(pData, m_listPivot2);

	m_listPivot2.GetBoundingRect(m_rectPivot, true);

	LoadPoint (pData, m_pointPivot);
	LoadNumber(pData, m_radiusPivot);
	LoadNumber(pData, m_angleMin);
	LoadNumber(pData, m_angleMax);
	LoadNumber(pData, m_lineFact);
	LoadVector(pData, m_radiusBug);
	LoadVector(pData, m_radiusOuter);
	LoadVector(pData, m_radiusMajor);
	LoadVector(pData, m_radiusMinor);
	LoadVector(pData, m_radiusBand);
	LoadVector(pData, m_radiusPoint);
	LoadVector(pData, m_radiusSweep);
	}

// Overridables

void CPrimRubyGaugeTypeAR::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRuby::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		TestScaleEtc();

		TestPointer();

		if( m_uChange >= 1 ) {

			if( IsNaked() ) {

				MakeMax(m_uChange, 4);
				}
			}

		if( m_uChange >= 2 ) {

			MakeMax(m_uChange, 2);

			m_fChange = TRUE;
			}

		(m_fChange ? Erase : Trans).Append(m_bound);
		}
	}

void CPrimRubyGaugeTypeAR::DrawExec(IGDI *pGDI, IRegion *pDirty)
{
	if( m_fShow ) {

		if( HitTest(pDirty) ) {
			
			MakeMax(m_uChange, 2);

			m_fChange = TRUE;
			}

		if( m_fChange ) {

			DrawPrim(pGDI);

			R2 Rect = GetBackRect();

			pDirty->AddRect(Rect);

			m_fChange = FALSE;
			}
		}
	}

void CPrimRubyGaugeTypeAR::DrawPrim(IGDI *pGdi)
{
	if( m_uChange == 4 ) {

		if( IsNaked() ) {

			// In naked mode, we can draw straight
			// on to the canvas. Not sure this is the
			// best way but until we've fixed the
			// gamma issue in the GDI, it looks better.

			DrawScaleEtc(pGdi);

			InitPaths();

			MakePaths();

			MakeLists();

			DrawPointer(pGdi);

			return;
			}

		// Everything changed so clear the buffer and
		// draw the bezel. Take a copy of the buffer
		// and then draw the scale etc. over the top.

		ArrayZero(m_pData, m_cx * m_cy);

		DrawBezel(m_pGdi);

		ArrayCopy(m_pCopy, m_pData, m_cx * m_cy);

		DrawScaleEtc(m_pGdi);
		}

	if( m_uChange == 3 ) {

		// Just the scale changed so restore the
		// buffer to just hold the bezel and then
		// draw the scale etc. ovet the top.

		ArrayCopy(m_pData, m_pCopy, m_cx * m_cy);

		DrawScaleEtc(m_pGdi);
		}

	if( m_uChange >= 1 ) {

		if( m_uChange >= 2 ) {

			// The pointer changed and so did the scale etc.
			// or we were asked to erase, so copy that from
			// our buffer and get ready to draw the pointer.

			pGdi->BitBlt( m_bound.x1,
				      m_bound.y1,
				      m_cx,
				      m_cy,
				      0,
				      PCBYTE(m_pData),
				      ropBlend
				      );

			// TODO -- We don't actually have to do this
			// if it's just an erase but does that matter?

			InitPaths();

			MakePaths();

			MakeLists();
			}
		else {
			// Only the pointer changed so find the minimum
			// reactangle that encloses the pivot, the old
			// pointer and the New pointer and restore that
			// from our buffer. No need to alpha blend as we
			// know that this doesn't include any edges.

			R2 wipe = m_rectPivot;

			m_pathPoint.AddBoundingRect(wipe);

			InitPaths();

			MakePaths();

			MakeLists();

			m_pathPoint.AddBoundingRect(wipe);

			InflateRect(wipe, 1, 1);

			int xp = wipe.x1 - m_bound.x1;

			int yp = wipe.y1 - m_bound.y1;

			int cb = 4 * (m_bound.x2 - m_bound.x1);

			pGdi->BitBlt( wipe.x1,
				      wipe.y1,
				      wipe.x2 - wipe.x1,
				      wipe.y2 - wipe.y1,
				      cb,
				      PCBYTE(m_pData) + xp * 4 + yp * cb,
				      0
				      );
			}

		// Draw the pointer over the top.

		DrawPointer(pGdi);
		}

	m_uChange = 0;
	}

// Drawing

void CPrimRubyGaugeTypeAR::TestPointer(void)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_fChange = TRUE;

		MakeMax(m_uChange, 1);

		m_Ctx = Ctx;
		}
	}

void CPrimRubyGaugeTypeAR::DrawPointer(IGDI *pGdi)
{
	CRubyGdiLink gdi(pGdi);

	gdi.OutputSolid(m_listPoint, m_Ctx.m_PointColor, 0);

	if( !m_PointMode ) {

		gdi.OutputSolid(m_listPivot1, GetColor(0), 0);

		gdi.OutputSolid(m_listPivot2, GetColor(1), 0);
		}
	}

// Path Management

void CPrimRubyGaugeTypeAR::InitPaths(void)
{
	m_pathPoint.Empty();
	}

void CPrimRubyGaugeTypeAR::MakePaths(void)
{
	PreparePoint();
	}

void CPrimRubyGaugeTypeAR::MakeLists(void)
{
	m_pathPoint.Transform(m_m1);

	m_listPoint.Load(m_pathPoint,  true);
	}

// Scale Building

void CPrimRubyGaugeTypeAR::MakeScaleEtc(void)
{
	PrepareTicks();

	PrepareBand(m_pathBand1, m_BandShow1, CPrimRubyGaugeTypeA::m_Ctx.m_BandMin1, CPrimRubyGaugeTypeA::m_Ctx.m_BandMax1);

	PrepareBand(m_pathBand2, m_BandShow2, CPrimRubyGaugeTypeA::m_Ctx.m_BandMin2, CPrimRubyGaugeTypeA::m_Ctx.m_BandMax2);

	PrepareBug (m_pathBug1,  m_BugShow1,  CPrimRubyGaugeTypeA::m_Ctx.m_BugValue1);

	PrepareBug (m_pathBug2,  m_BugShow2,  CPrimRubyGaugeTypeA::m_Ctx.m_BugValue2);
	}

// Scaling

number CPrimRubyGaugeTypeAR::GetAngle(number v)
{
	number a = CPrimRubyGaugeTypeA::m_Ctx.m_Min;

	number b = CPrimRubyGaugeTypeA::m_Ctx.m_Max;

	if( IsNAN(v) || IsNAN(a) || IsNAN(b) ) {

		return m_angleMin;
		}

	if( !num_equal(a, b) ) {

		MakeMin(v, Max(a, b));

		MakeMax(v, Min(a, b));

		return m_angleMin + (m_angleMax - m_angleMin) * (v - a) / (b - a);
		}

	return m_angleMin;
	}

// Path Preparation

void CPrimRubyGaugeTypeAR::PrepareTicks(void)
{
	number s  = fabs(m_angleMax - m_angleMin) / (m_Major * m_Minor);

	int    q  = 0;

	number a1 = min(m_angleMin, m_angleMax);

	number a2 = max(m_angleMin, m_angleMax);

	for( number t = a1; t < a2 + s / 2; t += s, q += 1 ) {

		if( q % m_Minor ) {

			CRubyVector cs(t);

			CRubyPoint  p1(m_pointPivot + (cs << m_radiusOuter));

			CRubyPoint  p2(m_pointPivot + (cs << m_radiusMinor));

			if( m_Minor % 2 == 0 && q % m_Minor == m_Minor / 2 ) {

				m_d.Line(m_pathMinor, p1, p2, m_lineFact * 1.5, CRubyStroker::endFlat);
				}
			else
				m_d.Line(m_pathMinor, p1, p2, m_lineFact * 1.0, CRubyStroker::endFlat);
			}
		else {
			CRubyVector cs(t);

			CRubyPoint  p1(m_pointPivot + (cs << m_radiusOuter));

			CRubyPoint  p2(m_pointPivot + (cs << m_radiusMajor));

			m_d.Line(m_pathMajor, p1, p2, m_lineFact * 2.0, CRubyStroker::endFlat);
			}
		}
	}

void CPrimRubyGaugeTypeAR::PreparePoint(void)
{
	if( m_PointMode == 0 ) {

		CRubyVector cs(GetAngle(m_Ctx.m_Value));

		CRubyPoint  pp(m_pointPivot + (cs << m_radiusPoint));

		m_d.Line(m_pathPoint, m_pointPivot, pp, m_lineFact * 5.0, CRubyStroker::arrowNone, m_PointStyle);
		}

	if( m_PointMode == 1 ) {

		number t1 = GetAngle(CPrimRubyGaugeTypeA::m_Ctx.m_Min);

		number t2 = GetAngle(m_Ctx.m_Value);

		m_d.Arc(m_pathPoint, m_pointPivot, t1, t2, m_radiusPoint, m_scale);

		m_d.Arc(m_pathPoint, m_pointPivot, t2, t1, m_radiusSweep, m_scale);

		m_pathPoint.AppendHardBreak();
		}
	}

BOOL CPrimRubyGaugeTypeAR::PrepareBand(CRubyPath &path, BOOL fShow, number minValue, number maxValue)
{
	if( fShow ) {

		number t1 = GetAngle(minValue);

		number t2 = GetAngle(maxValue);

		m_d.Arc(path, m_pointPivot, t1, t2, m_radiusBand,  m_scale);

		m_d.Arc(path, m_pointPivot, t2, t1, m_radiusOuter, m_scale);

		path.AppendHardBreak();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRubyGaugeTypeAR::PrepareBug(CRubyPath &path, BOOL fShow, number bugValue)
{
	if( fShow ) {

		number t1 = GetAngle(bugValue);

		number t2 = t1 - 2.5;
		number t3 = t1 + 2.5;

		CRubyPoint p1 = m_pointPivot + (CRubyVector(t1) << m_radiusOuter);
		CRubyPoint p2 = m_pointPivot + (CRubyVector(t2) << m_radiusBug  );
		CRubyPoint p3 = m_pointPivot + (CRubyVector(t3) << m_radiusBug  );

		path.Append(p1);
		path.Append(p2);
		path.Append(p3);

		path.AppendHardBreak();

		return TRUE;
		}

	return FALSE;
	}

// Color Schemes

COLOR CPrimRubyGaugeTypeAR::GetColor(UINT n)
{
	return m_Color[n];
	}

// Context Creation

void CPrimRubyGaugeTypeAR::FindCtx(CCtx &Ctx)
{
	Ctx.m_Value      = GetValue(m_pValue, 25);
	
	Ctx.m_PointColor = m_pPointColor->GetColor();
	}

// Context Check

BOOL CPrimRubyGaugeTypeAR::CCtx::operator == (CCtx const &That) const
{
	// TODO -- Avoid de minimis changes?

	return m_Value      == That.m_Value      &&
	       m_PointColor == That.m_PointColor ;
	}

// End of File
