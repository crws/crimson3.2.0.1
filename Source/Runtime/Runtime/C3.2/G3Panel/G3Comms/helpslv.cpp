
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Slave Helper Implementation
//

class CSlaveHelper : public ISlaveHelper
{
	public:
		// Constructor
		CSlaveHelper(CCommsDevice *pDevice);
	
		// Destructor
		~CSlaveHelper(void);
	
		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ISlaveHelper
		CCODE METHOD Read(AREF Addr, PDWORD pData, UINT uCount);
		CCODE METHOD Read(AREF Addr, PDWORD pData, IMetaData **ppm);
		CCODE METHOD Write(AREF Addr, PDWORD pData, UINT uCount);
		
	protected:
		// Meta Data Proxy
		class CMetaDataProxy : public IMetaData
		{
			public:
				// Constructor
				CMetaDataProxy(CCommsMapReg *pMap);

				// IUnknown
				HRM QueryInterface(REFIID riid, void **ppObject);
				ULM AddRef(void);
				ULM Release(void);

				// IMetaData
				DWORD METHOD GetMetaData(WORD ID);

			protected:
				// Data Members
				ULONG          m_uRefs;
				CCommsMapReg * m_pMap;
			};

		// IO Types
		enum
		{
			ioGet,
			ioPut
			};

		// Data Members
		ULONG		     m_uRefs;
		UINT                 m_uPort;
		CCommsDevice       * m_pDevice;
		CCommsMapBlockList * m_pBlocks;

		// IO Helpers
		CCODE Transfer(AREF Addr, PDWORD pData, UINT uCount, IMetaData **ppm, UINT uMode);
		BOOL  Transfer(CCommsMapBlock *pBlock, UINT uIndex, PDWORD pData, UINT uCount, UINT uMode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Slave Helper Implementation
//

// Instantiation

ISlaveHelper * Create_SlaveHelper(CCommsDevice *pDevice)
{
	return New CSlaveHelper(pDevice);
	}

// Constructor

CSlaveHelper::CSlaveHelper(CCommsDevice *pDevice)
{
	StdSetRef();

	m_pDevice = pDevice;

	m_pBlocks = pDevice ? pDevice->m_pMapBlocks : NULL;
	}
// Destructor

CSlaveHelper::~CSlaveHelper(void)
{
	}
	
// IUnknown

HRESULT CSlaveHelper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ISlaveHelper);

	StdQueryInterface(ISlaveHelper);

	return E_NOINTERFACE;
	}

ULONG CSlaveHelper::AddRef(void)
{
	StdAddRef();
	}

ULONG CSlaveHelper::Release(void)
{
	StdRelease();
	}

// IO Entries

CCODE METHOD CSlaveHelper::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return Transfer(Addr, pData, uCount, NULL, ioGet);
	}

CCODE METHOD CSlaveHelper::Read(AREF Addr, PDWORD pData, IMetaData **ppm)
{
	return Transfer(Addr, pData, 1, ppm, ioGet);
	}

CCODE METHOD CSlaveHelper::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return Transfer(Addr, pData, uCount, NULL, ioPut);
	}

// IO Helpers

CCODE CSlaveHelper::Transfer(AREF Addr, PDWORD pData, UINT uCount, IMetaData **ppm, UINT uMode)
{
	if( ppm ) {

		*ppm = NULL;
		}

	if( m_pBlocks ) {

		UINT ReqTable = Addr.a.m_Table;

		UINT ReqType  = Addr.a.m_Type;
		
		UINT ReqStart = Addr.a.m_Offset;

		UINT ReqExtra = Addr.a.m_Extra;

		////////
		
		if( uMode == ioGet ) {
			
			if( pData ) {

				memset(pData, 0, uCount * sizeof(DWORD));
				}
			}
		
		////////

		UINT uHits = 0;
		
		for( UINT uBlock = 0; uBlock < m_pBlocks->m_uCount; uBlock++ ) {

			CCommsMapBlock *pBlock = m_pBlocks->m_ppBlock[uBlock];

			if( !pBlock ) {

				continue;
				}

			if( uMode == ioPut && !pBlock->m_Write ) {

				continue;
				}

			UINT Scale    = pBlock->m_Factor;

			UINT ReqEnd   = ReqStart + Scale * uCount;

			AREF BlkAddr  = AREF(pBlock->m_Addr);

			UINT BlkTable = BlkAddr.a.m_Table;

			UINT BlkType  = BlkAddr.a.m_Type;

			UINT BlkExtra = BlkAddr.a.m_Extra;

			if( BlkTable == ReqTable && BlkType == ReqType && BlkExtra == ReqExtra ) {

				UINT BlkStart = BlkAddr.a.m_Offset;

				UINT BlkSize  = pBlock->m_Size;

				UINT BlkEnd   = BlkStart + Scale * BlkSize;

				////////

				UINT s = max(ReqStart, BlkStart);
				
				UINT e = min(ReqEnd, BlkEnd);

				if( s < e ) {

					UINT bo = (s - BlkStart) / Scale;
					
					UINT ro = (s - ReqStart) / Scale;
				
					UINT bc = (e - s) / Scale;

					////////

					if( ppm && !*ppm ) {

						CCommsMapReg *pMap = pBlock->m_pRegs->m_ppReg[bo];

						if( pMap ) {

							if( pMap->IsMapped() ) {

								*ppm = New CMetaDataProxy(pMap);
								}
							}
						}

					if( pData ) {

						if( Transfer( pBlock,
							      bo,
							      pData + ro,
							      bc,
							      uMode
							      ) ) {

							uHits++;
							}
						}
					else
						return CCODE_SUCCESS;
					}
				}
			}

		return uHits ? CCODE_SUCCESS : CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

BOOL CSlaveHelper::Transfer(CCommsMapBlock *pBlock, UINT uIndex, PDWORD pData, UINT uCount, UINT uMode)
{
	CCommsMapRegList *pList = pBlock->m_pRegs;

	UINT		  Flags = pBlock->GetFlags();

	BOOL              fUsed = FALSE;
	
	for( UINT i = 0; i < uCount; i++ ) {

		CCommsMapReg *pMap = pList->m_ppReg[uIndex];

		if( pMap ) {

			if( pMap->IsMapped() ) {

				if( uMode == ioGet ) {

					pData[i] = pMap->GetValue(Flags);
					}
				
				if( uMode == ioPut ) {

					pMap->SetValue(pData[i], Flags);
					}

				fUsed = TRUE;
				}
			}

		uIndex++;
		}
	
	return fUsed;
	}

//////////////////////////////////////////////////////////////////////////
//
// Slave Helper Meta Data Proxy
//

// Constructor

CSlaveHelper::CMetaDataProxy::CMetaDataProxy(CCommsMapReg *pMap)
{
	StdSetRef();

	m_pMap = pMap;
	}

// IUnknown

HRESULT CSlaveHelper::CMetaDataProxy::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IMetaData);

	StdQueryInterface(IMetaData);

	return E_NOINTERFACE;
	}

ULONG CSlaveHelper::CMetaDataProxy::AddRef(void)
{
	StdAddRef();
	}

ULONG CSlaveHelper::CMetaDataProxy::Release(void)
{
	StdRelease();
	}

// IMetaData

DWORD MCALL CSlaveHelper::CMetaDataProxy::GetMetaData(WORD ID)
{
	return m_pMap->GetMetaData(ID);
	}

// End of File
