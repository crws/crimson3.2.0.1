
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Helper Implementation
//

class CEthernetIPHelper : public IEthernetIPHelper 
{
	public:
		// Constructor
		CEthernetIPHelper(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IEthernetIPHelper
		WORD        METHOD GetVendorId(void);
		BOOL        METHOD Open(void);
		void        METHOD Close(void);
		IImplicit * METHOD GetImplicit(void);
		IExplicit * METHOD GetExplicit(void);

	protected:
		// Data
		ULONG         m_uRefs;
		IEtherNetIp * m_pStack;
		IImplicit   * m_pImplicit;
		IExplicit   * m_pExplicit;

		// Destructor
		~CEthernetIPHelper(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet-IP Helper Implementation
//

// Instantiator

global IEthernetIPHelper * Create_EthernetIPHelper(void)
{
	return New CEthernetIPHelper;
	}

// Constructor

CEthernetIPHelper::CEthernetIPHelper(void)
{
	StdSetRef();

	m_pStack    = NULL;

	m_pImplicit = NULL;

	m_pExplicit = NULL;

	AfxGetObject("comms.enetip", 0, IEtherNetIp, m_pStack);
	}

// IUnknown

HRESULT CEthernetIPHelper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IEthernetIPHelper);

	StdQueryInterface(IEthernetIPHelper);

	return E_NOINTERFACE;
	}

ULONG CEthernetIPHelper::AddRef(void)
{
	StdAddRef();
	}

ULONG CEthernetIPHelper::Release(void)
{
	StdRelease();
	}

// IEthernetIPHelper

WORD METHOD CEthernetIPHelper::GetVendorId(void)
{
	return m_pStack->GetVendorId();
	}

BOOL METHOD CEthernetIPHelper::Open(void)
{
	return m_pStack->Open();
	}

void METHOD CEthernetIPHelper::Close(void)
{
	if( m_pImplicit ) {
		
		m_pStack->FreeImplicit(m_pImplicit);

		m_pImplicit = NULL;
		}

	if( m_pExplicit ) {

		m_pStack->FreeExplicit(m_pExplicit);

		m_pExplicit = NULL;
		}
	
	m_pStack->Close();
	}

IImplicit * METHOD CEthernetIPHelper::GetImplicit(void)
{
	if( !m_pImplicit ) {

		if( m_pStack->Connect() ) {

			m_pImplicit = m_pStack->GetImplicit();
			}
		}

	return m_pImplicit;
	}

IExplicit * METHOD CEthernetIPHelper::GetExplicit(void)
{
	if( !m_pExplicit ) {

		if( m_pStack->Connect() ) {

			m_pExplicit = m_pStack->GetExplicit();
			}
		}

	return m_pExplicit;
	}

// Proetected Destructor

CEthernetIPHelper::~CEthernetIPHelper(void)
{
	m_pStack->Release();

	m_pStack = NULL;
	}	

// End of File
