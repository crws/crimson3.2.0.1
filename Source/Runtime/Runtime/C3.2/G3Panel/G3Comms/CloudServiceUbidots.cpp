
#include "Intern.hpp"

#include "Service.hpp"

#include "CloudServiceUbidots.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientUbidots.hpp"

#include "MqttClientOptionsUbidots.hpp"

#include "CloudDeviceDataSet.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ubidots MQTT Service
//

// Instantiator

IService * Create_CloudServiceUbidots(void)
{
	return New CCloudServiceUbidots;
}

// Constructor

CCloudServiceUbidots::CCloudServiceUbidots(void)
{
	m_Name = "UBIDOTS";
}

// Initialization

void CCloudServiceUbidots::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudServiceUbidots", pData);

	CCloudServiceCrimson::Load(pData);

	for( UINT n = 0; n < m_uSet; n++ ) {

		if( m_pSet[n]->m_Array == 0 ) {

			m_pSet[n]->AddRewrite('[', '-');

			m_pSet[n]->AddRewrite(']', 0);
		}

		if( m_pSet[n]->m_Tree == 0 ) {

			m_pSet[n]->AddRewrite('.', '-');
		}

		m_pSet[n]->m_Array |= 2;
	}

	CMqttClientOptionsUbidots *pOpts = New CMqttClientOptionsUbidots;

	pOpts->Load(pData);

	m_pOpts   = pOpts;

	CheckHistory(200);

	m_pClient = New CMqttClientUbidots(this, *pOpts);

	m_pOpts->m_DiskPath = "C:\\MQTT\\UBIDOTS";

	FindConfigGuid(pOpts->GetExtra());
}

// Service ID

UINT CCloudServiceUbidots::GetID(void)
{
	return 15;
}

// End of File
