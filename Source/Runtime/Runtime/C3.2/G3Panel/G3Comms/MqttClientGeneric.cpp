
#include "Intern.hpp"

#include "Service.hpp"

#include "Matrix.hpp"

#include "MqttClientGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudServiceGeneric.hpp"

#include "MqttClientOptionsGeneric.hpp"

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client
//

// Constructor

CMqttClientGeneric::CMqttClientGeneric(CCloudServiceGeneric *pService, CMqttClientOptionsGeneric &Opts) : CMqttClientJson(pService, Opts), m_Opts(Opts)
{
	}

// Operations

BOOL CMqttClientGeneric::Open(void)
{
	SubstTopic(m_Opts.m_PubTopic);

	SubstTopic(m_Opts.m_SubTopic);

	m_Will.SetTopic(ApplyWillSuffix(m_Opts.m_PubTopic, TRUE));

	m_Will.SetData(GetWillData());

	return CMqttClientJson::Open();
}

// Client Hooks

void CMqttClientGeneric::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseInitial ) {

		if( HasWrites() ) {

			if( m_Opts.m_SubTopic != "none" ) {

				AddToSubList(subData, m_Opts.m_SubTopic);
				}
			}
		}

	CMqttClientJson::OnClientPhaseChange();
	}

BOOL CMqttClientGeneric::OnClientNewData(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode == subData ) {

		// Hack for Ubidots!

		if( pMsg->m_Topic.EndsWith("/lv") ) {

			UINT s;

			if( (s = pMsg->m_Topic.GetLength()) > 3 ) {

				CString Top = pMsg->m_Topic.Left(s - 3);

				CString Tag = Top.TokenLast('/');

				if( !Tag.IsEmpty() ) {

					if( Tag != "timestamp" && Tag != "connected" ) {

						CString Val;

						if( pMsg->GetText(Val) ) {

							CMqttJsonData Json;

							Json.AddValue(Tag, Val);

							OnWrite(Json, "");
							}
						}
					}
				}

			return TRUE;
			}

		CMqttJsonData Json;

		if( pMsg->GetJson(Json) ) {

			OnWrite(Json, "");
			}

		return TRUE;
		}

	return TRUE;
	}

// Publish Hook

BOOL CMqttClientGeneric::GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg)
{
	if( FALSE ) {

		uMode = CCloudDataSet::modeForce;
		}

	if( CMqttClientJson::GetPubMessage(uTopic, uTime, fTemp, uMode, pMsg) ) {

		CCloudDataSet *pSet = m_pService->GetDataSet(uTopic);

		pMsg->SetTopic(m_Opts.m_PubTopic + pSet->m_Suffix);

		return TRUE;
		}

	return FALSE;
	}

// End of File
