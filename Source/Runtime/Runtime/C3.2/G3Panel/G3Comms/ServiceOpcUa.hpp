

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ServiceOpcUa_HPP
	
#define	INCLUDE_ServiceOpcUa_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC-A Server
//

class CServiceOpcUa : public CServiceItem,
		      public ITaskEntry,
		      public IOpcUaServerHost
{
	public:
		// Constructor
		CServiceOpcUa(void);

		// Destructor
		~CServiceOpcUa(void);

		// Initialization
		void Load(PCBYTE &pData);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IService
		UINT GetID(void);
		void GetTaskList(CTaskList &List);

		// ITaskEntry
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// IOpcUaServerHost
		bool    LoadModel(IOpcUaDataModel *pModel);
		bool    OpenSession(UINT si, CString const &User, CString const &Pass);
		void    CloseSession(UINT si);
		bool    SkipValue(UINT si, UINT ns, UINT id);
		void    SetValueDouble(UINT si, UINT ns, UINT id, UINT n, double d);
		void    SetValueInt(UINT si, UINT ns, UINT id, UINT n, int d);
		void    SetValueInt64(UINT si, UINT ns, UINT id, UINT n, INT64 d);
		void    SetValueString(UINT si, UINT ns, UINT id, UINT n, PCTXT d);
		double  GetValueDouble(UINT si, UINT ns, UINT id, UINT n);
		UINT    GetValueInt(UINT si, UINT ns, UINT id, UINT n);
		UINT64  GetValueInt64(UINT si, UINT ns, UINT id, UINT n);
		CString GetValueString(UINT si, UINT ns, UINT id, UINT n);
		timeval GetValueTime(UINT si, UINT ns, UINT id, UINT n);
		bool    GetDesc(CString &Desc, UINT ns, UINT id);
		bool    GetSourceTimeStamp(timeval &t, UINT ns, UINT id);
		bool    HasEvents(CArray <UINT> &List, UINT ns, UINT id);
		bool    HasCustomHistory(UINT ns, UINT id);
		bool    InitHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory);
		bool	HasChanged(UINT ns, UINT id, UINT uType, DWORD *pHistory);
		void	KillHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory);
		UINT	GetWireType(UINT uType);

		// Item Properties
		UINT	     m_uDebug;
		CCodedItem * m_pEnable;
		CCodedItem * m_pServer;
		CCodedItem * m_pEndpoint;
		UINT	     m_uPort;
		UINT	     m_uLayout;
		UINT	     m_uTree;
		UINT	     m_uArray;
		UINT	     m_uProps;
		UINT	     m_uPubSub;
		BOOL	     m_fHistEnable;
		UINT	     m_uHistSample;
		UINT	     m_uHistQuota;
		UINT	     m_uHistTime;
		UINT	     m_uAnon;
		CString	     m_User;
		CString	     m_Pass;
		UINT	     m_uWrite;
		UINT	     m_uSet1;
		UINT	     m_uSet2;
		UINT	     m_uTags;
		DWORD	   * m_pTags;

	protected:
		// Tag Info
		struct CTagInfo
		{
			CDataRef   m_Ref;
			CTag     * m_pTag;
			CString    m_Label;
			CString    m_Value;
			UINT	   m_Type;
			DWORD      m_id;
			};

		// Data Members
		IOpcUaServer       * m_pOpcUa;
		BOOL		     m_fInit;
		CString	             m_Server;
		CString	             m_Endpoint;
		CTagList           * m_pSrc;
		CTagInfo           * m_pInfo;
		CMap <DWORD, UINT>   m_Index;
		DWORD		     m_SessActive;
		DWORD		     m_SessAuth;

		// Implementation
		BOOL LoadTagList(PCBYTE &pData);
		void InitTagList(void);
		bool Adjust(UINT &ns, UINT &id, UINT &n, UINT x);
		UINT GetOpcType(UINT Type);
		void GetConfigGuid(PBYTE pGuid);
		bool IsSessionActive(UINT si);
		bool IsSessionAuthenticated(UINT si);
		bool AllowWrites(UINT si);
		bool AllowAccess(UINT si, UINT n);
	};

// End of File

#endif
