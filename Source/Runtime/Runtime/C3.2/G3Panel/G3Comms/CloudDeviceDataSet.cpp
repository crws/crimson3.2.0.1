
#include "Intern.hpp"

#include "CloudDeviceDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cloud Data Set
//

// Constructor

CCloudDeviceDataSet::CCloudDeviceDataSet(void)
{
	m_Gps  = 1;

	m_Cell = 1;
}

// Destructor

CCloudDeviceDataSet::~CCloudDeviceDataSet(void)
{
}

// Initialization

void CCloudDeviceDataSet::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudDeviceDataSet", pData);

	CCloudDataSet::Load(pData);

	m_Gps  = GetByte(pData);

	m_Cell = GetByte(pData);
}

// Json Formatting

BOOL CCloudDeviceDataSet::GetJson(CMqttJsonData *pRep, CMqttJsonData *pDes, CString Ident, UINT64 uTime, UINT uRoot, UINT uCode, UINT uMode)
{
	if( uRoot == 0 && uCode < 2 ) {

		UINT       Type;

		CMqttJsonData *pDevice;

		pRep->AddChild("device", FALSE, pDevice);

		pDevice->AddValue("status", "okay");

		if( Ident.GetLength() ) {

			pDevice->AddValue("cid", Ident);
		}

		if( uCode == 1 ) {

			Type = jsonBool;
		}
		else
			Type = jsonAuto;

		if( m_Gps ) {

			CMqttJsonData *pGps;

			pDevice->AddChild("location", FALSE, pGps);

			CString State = AddGpsData(pGps, Type) ? "true" : "false";

			pGps->AddValue("valid", State, Type);
		}

		if( m_Cell ) {

			CMqttJsonData *pCell;

			pDevice->AddChild("cellular", FALSE, pCell);

			CString State = AddCellData(pCell, Type) ? "true" : "false";

			pCell->AddValue("valid", State, Type);
		}
	}

	return TRUE;
}

// List Formatting

UINT CCloudDeviceDataSet::GetCount(void)
{
	UINT uCount = 1;

	if( m_Gps ) {

		uCount += 5;
	}

	if( m_Cell ) {

		uCount += 14;
	}

	return uCount;
}

UINT CCloudDeviceDataSet::GetProps(void)
{
	return 0;
}

BOOL CCloudDeviceDataSet::GetData(UINT uIndex, CString &Name, DWORD &Data, UINT &Type, BOOL &Free, UINT uMode)
{
	if( uIndex < 1 ) {

		if( uIndex == 0 ) {

			Name = "Status";
			Data = DWORD(wstrdup(L"okay"));
			Type = typeString;
			Free = TRUE;

			return TRUE;
		}
	}
	else {
		uIndex -= 1;
	}

	if( m_Gps ) {

		if( uIndex < 5 ) {

			if( uIndex == 0 ) {

				m_Location.m_uFix = 0;

				AfxGetAutoObject(pLocation, "c3.location", 0, ILocationSource);

				if( pLocation ) {

					pLocation->GetLocationData(m_Location);
				}

				Name = "Location/Valid";
				Data = DWORD(wstrdup(m_Location.m_uFix ? L"true" : L"false"));
				Type = typeString;
				Free = TRUE;
			}
			else {
				switch( uIndex ) {

					case 1:
						Name = "Location/Fix";
						Data = C3INT(m_Location.m_uFix);
						Type = typeInteger;
						Free = FALSE;
						break;

					case 2:
						Name = "Location/Lat";
						Data = (m_Location.m_uFix >= 2) ? R2I(m_Location.m_Lat) : 0;
						Type = typeReal;
						Free = FALSE;
						break;

					case 3:
						Name = "Location/Long";
						Data = (m_Location.m_uFix >= 2) ? R2I(m_Location.m_Long) : 0;
						Type = typeReal;
						Free = FALSE;
						break;

					case 4:
						Name = "Location/Alt";
						Data = (m_Location.m_uFix >= 3) ? R2I(m_Location.m_Alt) : 0;
						Type = typeReal;
						Free = FALSE;
						break;
				}
			}

			return TRUE;
		}
		else {
			uIndex -= 5;
		}
	}

	if( m_Cell ) {

		if( uIndex < 14 ) {

			if( uIndex == 0 ) {

				m_CellInfo.m_fValid = FALSE;

				AfxGetAutoObject(pCell, "net.cell", 0, ICellStatus);

				if( pCell ) {

					pCell->GetCellStatus(m_CellInfo);
				}

				Name = "Cellular/Valid";
				Data = DWORD(wstrdup(m_CellInfo.m_fValid ? L"true" : L"false"));
				Type = typeString;
				Free = TRUE;
			}
			else {
				switch( uIndex ) {

					case 1:
						Name = "Cellular/Online";
						Data = DWORD(wstrdup((m_CellInfo.m_fValid && m_CellInfo.m_fOnline) ? "true" : "false"));
						Type = typeString;
						Free = TRUE;
						break;

					case 2:
						Name = "Cellular/Register";
						Data = DWORD(wstrdup((m_CellInfo.m_fValid && m_CellInfo.m_fRegister) ? "true" : "false"));
						Type = typeString;
						Free = TRUE;
						break;

					case 3:
						Name = "Cellular/Roam";
						Data = DWORD(wstrdup((m_CellInfo.m_fValid && m_CellInfo.m_fRoam) ? "true" : "false"));
						Type = typeString;
						Free = TRUE;
						break;

					case 4:
						Name = "Cellular/Slot";
						Data = m_CellInfo.m_fValid ? m_CellInfo.m_uSlot : 0;
						Type = typeInteger;
						Free = FALSE;
						break;

					case 5:
						Name = "Cellular/Service";
						Data = DWORD(wstrdup(m_CellInfo.m_fValid ? m_CellInfo.m_Service : ""));
						Type = typeString;
						Free = TRUE;
						break;

					case 6:
						Name = "Cellular/Carrier";
						Data = DWORD(wstrdup(m_CellInfo.m_fValid ? m_CellInfo.m_Carrier : ""));
						Type = typeString;
						Free = TRUE;
						break;

					case 7:
						Name = "Cellular/Iccid";
						Data = DWORD(wstrdup(m_CellInfo.m_fValid ? m_CellInfo.m_Iccid : ""));
						Type = typeString;
						Free = TRUE;
						break;

					case 8:
						Name = "Cellular/Addr";
						Data = DWORD(wstrdup(m_CellInfo.m_fValid ? m_CellInfo.m_Addr.GetAsText() : ""));
						Type = typeString;
						Free = TRUE;
						break;

					case 9:
						Name = "Cellular/Imei";
						Data = DWORD(wstrdup(m_CellInfo.m_fValid ? m_CellInfo.m_Imei : ""));
						Type = typeString;
						Free = TRUE;
						break;

					case 10:
						Name = "Cellular/Network";
						Data = DWORD(wstrdup(m_CellInfo.m_fValid ? m_CellInfo.m_Network : ""));
						Type = typeString;
						Free = TRUE;
						break;

					case 11:
						Name = "Cellular/Model";
						Data = DWORD(wstrdup(m_CellInfo.m_fValid ? m_CellInfo.m_Model : ""));
						Type = typeString;
						Free = TRUE;
						break;

					case 12:
						Name = "Cellular/Signal";
						Data = m_CellInfo.m_fValid ? m_CellInfo.m_uSignal : 0;
						Type = typeInteger;
						Free = FALSE;
						break;

					case 13:
						Name = "Cellular/Time";
						Data = m_CellInfo.m_fValid ? (getmonosecs() - m_CellInfo.m_CTime) : 0;
						Type = typeInteger;
						Free = FALSE;
						break;
				}
			}

			return TRUE;
		}
		else {
			uIndex -= 14;
		}
	}

	return FALSE;
}

BOOL CCloudDeviceDataSet::GetProp(UINT uIndex, UINT uProp, CString &Name, DWORD &Data, UINT &Type)
{
	return FALSE;
}

BOOL CCloudDeviceDataSet::SetData(UINT uIndex, DWORD Data, UINT Type)
{
	return TRUE;
}

// Implementation

BOOL CCloudDeviceDataSet::AddGpsData(CMqttJsonData *pJson, UINT Type)
{
	AfxGetAutoObject(pLocation, "c3.location", 0, ILocationSource);

	if( pLocation ) {

		CLocationSourceInfo Info;

		pLocation->GetLocationData(Info);

		if( Info.m_uFix >= 2 ) {

			pJson->AddValue("fix", CPrintf("%u", Info.m_uFix));

			pJson->AddValue("lat", CPrintf("%.7f", Info.m_Lat));

			pJson->AddValue("long", CPrintf("%.7f", Info.m_Long));

			if( Info.m_uFix >= 3 ) {

				pJson->AddValue("alt", CPrintf("%d", int(Info.m_Alt)));
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCloudDeviceDataSet::AddCellData(CMqttJsonData *pJson, UINT Type)
{
	AfxGetAutoObject(pCell, "net.cell", 0, ICellStatus);

	if( pCell ) {

		CCellStatusInfo Info;

		pCell->GetCellStatus(Info);

		pJson->AddValue("online", Info.m_fOnline ? "true" : "false", Type);

		pJson->AddValue("register", Info.m_fRegister ? "true" : "false", Type);

		pJson->AddValue("roam", Info.m_fRoam ? "true" : "false", Type);

		pJson->AddValue("slot", CPrintf("%u", Info.m_uSlot));

		pJson->AddValue("service", Info.m_Service, jsonString);

		pJson->AddValue("carrier", Info.m_Carrier, jsonString);

		pJson->AddValue("iccid", Info.m_Iccid, jsonString);

		pJson->AddValue("addr", Info.m_Addr.GetAsText());

		pJson->AddValue("imei", Info.m_Imei, jsonString);

		pJson->AddValue("network", Info.m_Network, jsonString);

		pJson->AddValue("model", Info.m_Model, jsonString);

		pJson->AddValue("signal", CPrintf("%u", Info.m_uSignal));

		pJson->AddValue("time", CPrintf("%u", getmonosecs() - Info.m_CTime));

		return TRUE;
	}

	return FALSE;
}

// End of File
