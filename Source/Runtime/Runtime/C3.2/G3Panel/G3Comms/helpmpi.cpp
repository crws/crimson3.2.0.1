
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2001 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MPI Helper Implementation
//

class CMPIHelper : public IMPIHelper 
{
	public:
		// Constructor
		CMPIHelper(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

	protected:
		// Data Members
		UINT m_uRefs;
	};

//////////////////////////////////////////////////////////////////////////
//
// MPI Helper Implementation
//

// Instantiator

global IMPIHelper * Create_MPIHelper(void)
{
	return New CMPIHelper;
	}

// Constructor

CMPIHelper::CMPIHelper(void)
{
	StdSetRef();
	}

// IUnknown

HRESULT CMPIHelper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IMPIHelper);

	StdQueryInterface(IMPIHelper);

	return E_NOINTERFACE;
	}

ULONG CMPIHelper::AddRef(void)
{
	StdAddRef();
	}

ULONG CMPIHelper::Release(void)
{
	StdRelease();
	}

// End of File
