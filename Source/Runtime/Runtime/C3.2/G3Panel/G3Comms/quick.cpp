
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tag Quick Plot
//

// Constructor

CTagQuickPlot::CTagQuickPlot(CTag *pTag)
{
	m_pTag    = pTag;

	m_Mode    = 0;
	
	m_Rewind  = 0;
	
	m_pEnable = NULL;
	
	m_pStore  = NULL;
	
	m_pTime   = NULL;
	
	m_pLook   = NULL;
	
	m_Points  = 120;
	}

// Destructor

CTagQuickPlot::~CTagQuickPlot(void)
{
	delete m_pEnable;

	delete m_pStore;

	delete m_pTime;

	delete m_pLook;
	}

// Initialization

void CTagQuickPlot::Load(PCBYTE &pData)
{
	ValidateLoad("CTagQuickPlot", pData);

	m_Mode   = GetByte(pData);

	m_Rewind = GetByte(pData);

	GetCoded(pData, m_pEnable);

	GetCoded(pData, m_pStore);
	
	GetCoded(pData, m_pLook);
	
	GetCoded(pData, m_pTime);
	
	m_Points = GetWord(pData);
	}

// Attributes

UINT CTagQuickPlot::GetSequence(void) const
{
	return m_uSeq;
	}

UINT CTagQuickPlot::GetPointLimit(void) const
{
	return m_nUsed;
	}

UINT CTagQuickPlot::GetPointCount(void) const
{
	if( m_Mode == 1 ) {

		if( m_nUsed )
			return (m_uTail + m_nUsed - m_uHead) % m_nUsed;
		else
			return 0;
		}

	return m_uTail;
	}

void CTagQuickPlot::GetData(C3REAL *pData)
{
	if( m_Mode == 1 ) {

		// REV3 -- We can use two memcpy calls here
		// to make this somewhat quicker. Perhaps we
		// could have a buffer API as this is used
		// in quite a few places.

		UINT uScan = m_uHead;

		while( uScan - m_uTail ) {

			*pData++ = I2R(m_pData[uScan]);

			uScan    = (uScan + 1) % m_nUsed;
			}

		return;
		}

	memcpy(pData, m_pData, m_uTail * sizeof(DWORD));
	}

// Operations

void CTagQuickPlot::SetScan(UINT Code)
{
	SetItemScan(m_pEnable, Code);
	
	SetItemScan(m_pStore,  Code);

	SetItemScan(m_pLook,   Code);

	SetItemScan(m_pTime,   Code);

	CDataRef Ref;

	Ref.m_Ref = 0;

	m_pTag->SetScan(Ref, Code);
	}

void CTagQuickPlot::Init(void)
{
	m_pData = New DWORD [ m_Points ];

	m_uLook = 0;

	m_nUsed = 0;

	m_fRun  = FALSE;

	m_uSeq  = 0;

	SetScan(scanTrue);
	}

void CTagQuickPlot::Poll(void)
{
	if( CheckRun() ) {

		if( IsItemAvail(m_pTime) ) {

			if( CheckLook() ) {

				UINT uTime = GetItemData(m_pTime, C3INT(GetNow()));

				if( m_uInit == NOTHING ) {

					if( m_Mode == 1 || m_Mode == 2 ) {

						m_uInit = uTime;
						}
					else
						m_uInit = 0;
					}

				if( uTime >= m_uInit ) {

					INT   nSlot = (uTime - m_uInit) * m_nUsed / m_uLook;

					DWORD Data  = FindData();

					if( m_Mode == 1 ) {

						if( nSlot > m_nSlot ) {

							INT nStore = min(nSlot - m_nSlot - 1, m_nUsed - 1);

							while( nStore-- ) {

								StoreData(NOTHING);
								}

							StoreData(Data);

							m_nSlot = nSlot;

							m_uSeq += 1;
							}

						if( nSlot < m_nSlot - m_nUsed / 2 ) {

							// REV3 -- Could we be more clever here and
							// actually implement some kind of shift when
							// time moves back? The logic here clears the
							// data if we move back more than half the
							// buffer to avoid clears on time adjustments.

							ClearData();
							}
						}
					else {
						if( nSlot < m_nUsed ) {

							if( nSlot < m_nSlot ) {

								if( m_Rewind ) {

									UINT uMove = m_nSlot       - nSlot;

									UINT uSize = sizeof(DWORD) * nSlot;

									memmove(m_pData, m_pData + uMove, uSize);
									}

								m_pData[nSlot] = Data;

								m_uTail = nSlot + 1;

								m_nSlot = nSlot;

								m_uSeq += 1;
								}

							if( nSlot > m_nSlot ) {

								for( INT s = m_nSlot + 1; s < nSlot; s++ ) {

									m_pData[s] = NOTHING;
									}

								m_pData[nSlot] = Data;

								m_uTail = nSlot + 1;

								m_nSlot = nSlot;

								m_uSeq += 1;
								}
							}
						}

					m_uTime = uTime;
					}
				else
					ClearData();
				}
			}
		}
	}

// Implementation

void CTagQuickPlot::ClearData(void)
{
	m_uHead = 0;

	m_uTail = 0;

	m_uTime = 0;

	m_nSlot = -1;

	m_uInit = NOTHING;

	m_uSeq += 1;
	}

void CTagQuickPlot::StoreData(DWORD Data)
{
	UINT uNext = (m_uTail + 1) % m_nUsed;

	if( uNext == m_uHead ) {

		m_uHead = (m_uHead + 1) % m_nUsed;
		}

	m_pData[m_uTail] = Data;

	m_uTail = uNext;
	}

BOOL CTagQuickPlot::CheckRun(void)
{
	if( IsItemAvail(m_pEnable) ) {

		BOOL fRun = GetItemData(m_pEnable, 1);

		if( fRun && !m_fRun ) {

			ClearData();

			m_fRun = TRUE;
			}

		if( !fRun && m_fRun ) {

			m_fRun = FALSE;
			}
		}

	return m_fRun;
	}

BOOL CTagQuickPlot::CheckLook(void)
{
	if( IsItemAvail(m_pLook) ) {

		UINT uLook = GetItemData(m_pLook, 120);

		if( m_uLook != uLook ) {

			ClearData();

			m_uLook = uLook;

			m_nUsed = min(m_uLook, m_Points);
			}
		}

	return m_uLook > 0;
	}

DWORD CTagQuickPlot::FindData(void)
{
	CDataRef Ref;

	Ref.m_Ref  = 0;

	if( m_pTag->IsAvail(Ref) ) {

		if( GetItemData(m_pStore, 1) ) {

			return m_pTag->GetData(Ref, typeReal, getNone);
			}
		}

	return NOTHING;
	}

// End of File
