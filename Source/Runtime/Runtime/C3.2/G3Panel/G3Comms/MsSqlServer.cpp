
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "SqlClient.hpp"

#include "MsSqlServer.hpp"

#include "TdsPreLogin.hpp"
#include "TdsLogin.hpp"
#include "TdsResponse.hpp"
#include "TdsSQLBatch.hpp"
#include "TdsBulkLoad.hpp"
#include "TdsDataSet.hpp"

#include "FakeSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Microsoft SQL Server Connection Implmentation
//

// Time Constants

static UINT const timeSocketReadDelay	 = 20;
static UINT const timeSocketReadMaxWait	 = 5000;
static UINT const timeSocketSendDelay	 = 10;
static UINT const countSocketSendRetries = 100;
static UINT const maxPacketSize          = (8 * 1024);

// Instantiator

ISqlConnection * Create_MSSqlServerConnection(void)
{
	return New CMsSqlSeverConnection();
}

// Constructors

CMsSqlSeverConnection::CMsSqlSeverConnection(void)
{
	m_uMaxPacketSize = maxPacketSize;

	m_pSock          = NULL;

	m_fOpen          = FALSE;

	m_pTls           = NULL;

	m_pInstance      = NULL;
}

// Destructor

CMsSqlSeverConnection::~CMsSqlSeverConnection(void)
{
	if( m_pTls ) {

		m_pTls->Release();
	}
}

// Operations

BOOL CMsSqlSeverConnection::Open(IPADDR IP, WORD wPort, UINT uTls, PCTXT pUser, PCTXT pPass, PCTXT pDatabase)
{
	m_IP        = IP;

	m_wPort     = wPort;

	m_uTlsMode  = uTls;

	m_pUser     = pUser;

	m_pPass     = pPass;

	m_pDatabase = pDatabase;

	if( m_pInstance ) {

		UINT uPort = FindInstancePort(m_pInstance);

		if( uPort == NOTHING ) {

			return FALSE;
		}

		m_wPort = WORD(uPort);
	}

	if( OpenSocket(TRUE, m_wPort) ) {

		if( ServerLogin() ) {

			m_fOpen = TRUE;

			return TRUE;
		}
	}

	return FALSE;
}

void CMsSqlSeverConnection::Close(void)
{
	m_fOpen = FALSE;

	CloseSocket(FALSE);
}

BOOL CMsSqlSeverConnection::IsOpen(void)
{
	return m_fOpen;
}

void CMsSqlSeverConnection::SetInstance(PCTXT pInstance)
{
	m_pInstance = pInstance;
}

// SQL Statement Helpers

CSqlStatement * CMsSqlSeverConnection::CreateStatement(void)
{
	return New CSqlStatement(this);
}

BOOL CMsSqlSeverConnection::ExecuteQuery(PCTXT pQuery)
{
	// LATER -- Implement for writing to database

	return FALSE;
}

BOOL CMsSqlSeverConnection::ExecuteReadQuery(PCTXT pQuery, CResultSet &Results)
{
	CTdsDataSet DataSet;

	if( SendSQLBatchCommandsAndGetReply(DataSet, pQuery) ) {

		PopulateResultSet(DataSet, Results);

		return TRUE;
	}

	return FALSE;
}

// Implementation

void CMsSqlSeverConnection::PopulateResultSet(CTdsDataSet &DataSet, CResultSet &ResultSet)
{
	for( UINT m = 0; m < DataSet.GetNumColumns(); m++ ) {

		UINT uType = TdsToSqlType(DataSet.GetColumnType(m));

		CSqlColumn Column(DataSet.GetColumnName(m), uType, DataSet.GetColumnTypeSize(m));

		ResultSet.AddColumn(Column);
	}

	for( UINT n = 0; n < DataSet.GetNumRows(); n++ ) {

		CSqlRow Row;

		for( UINT u = 0; u < DataSet.GetNumColumns(); u++ ) {

			UINT uSize = DataSet.GetColumnTypeSize(u);

			CSqlData Data;

			UINT uType = DataSet.GetColumnType(u);

			if( DataSet.IsString(uType) ) {

				PCTXT s = DataSet.GetVarcharField(n, u);

				Data.SetData(PCBYTE(s), strlen(s) + 1);
			}
			else {
				if( uSize == 1 ) {

					BYTE  b = DataSet.GetByteField(n, u);

					Data.SetData(&b, 1);
				}

				if( uSize == 2 ) {

					WORD  w = DataSet.GetShortField(n, u);

					Data.SetData(PCBYTE(&w), 2);
				}

				if( uSize == 4 ) {

					DWORD dw = DataSet.GetLongField(n, u);

					Data.SetData(PCBYTE(&dw), 4);
				}

				if( uSize == 8 ) {

					INT64  i = DataSet.GetLongLongField(n, u);

					Data.SetData(PCBYTE(&i), 8);
				}
			}

			Row.AddData(Data);
		}

		ResultSet.AddRow(Row);
	}
}

UINT CMsSqlSeverConnection::FindInstancePort(PCTXT pInstance)
{
	if( OpenSocket(FALSE, 1434) ) {

		UINT uPort = NOTHING;

		PBYTE pData = new BYTE[1024];

		memset(pData, 0, 1024);

		pData[0] = 4;

		UINT uLen = strlen(pInstance) + 1;

		strncpy(PTXT(pData + 1), pInstance, 1024 - 1);

		if( m_pSock->Send(pData, uLen) == S_OK ) {

			CBuffer * pBuff = NULL;

			SetTimer(timeSocketReadMaxWait);

			while( GetTimer() ) {

				if( m_pSock->Recv(pBuff) == S_OK ) {

					memset(pData, 0, 1024);

					memcpy(pData, pBuff->GetData() + 3, pBuff->GetSize());

					BuffRelease(pBuff);

					PTXT pReply = PTXT(pData);

					UINT uPort = ParseInstancePort(pInstance, pReply);

					delete[] pData;

					CloseSocket(FALSE);

					return uPort;
				}

				if( !CheckSocket() ) {

					break;
				}

				Sleep(timeSocketReadDelay);
			}
		}

		CloseSocket(TRUE);

		delete[] pData;
	}

	return NOTHING;
}

UINT CMsSqlSeverConnection::ParseInstancePort(PCTXT pInstance, PTXT pReply)
{
	CString Work(pReply);

	CStringArray Tokens;

	Work.Tokenize(Tokens, ';');

	BOOL fInstance = FALSE;

	BOOL fPort = FALSE;

	UINT uPort = NOTHING;

	for( UINT n = 0; n < Tokens.GetCount(); n += 2 ) {

		CString Key   = Tokens[n];

		CString Value = Tokens[n + 1];

		if( Key.CompareN("InstanceName") == 0 ) {

			if( Value.CompareN(pInstance) == 0 ) {

				fInstance = TRUE;
			}
		}

		if( Key.CompareN("tcp") == 0 ) {

			uPort = strtol(PCTXT(Value), NULL, 10);

			fPort = TRUE;
		}

		if( fInstance && fPort ) {

			return uPort;
		}
	}

	return NOTHING;
}

UINT CMsSqlSeverConnection::TdsToSqlType(UINT uType)
{
	switch( uType ) {

		case 0x68:
		case 0x2D:
		case 0x32:
			return sqlTypeBinary;

		case 0x2F:
		case 0xAF:
			return sqlTypeChar;

		case 0x26:
		case 0x38:
			return sqlTypeInt;

		case 0x6D:
		case 0x3B:
		case 0x3E:
			return sqlTypeFloat;

		case 0x3D:
		case 0x6F:
			return sqlTypeDatetime;

		case 0xA7:
			return sqlTypeVarChar;

		case 0xE7:
			return sqlTypeNVarChar;
	}

	return sqlTypeInt;
}

CTdsBytes * CMsSqlSeverConnection::MakeCommandBytes(CTdsBytes& Command, PCSTR pCommand)
{
	UINT uLength = strlen(pCommand);

	for( int i = 0; i < uLength; i++ ) {

		Command.AddByte(pCommand[i]);

		Command.AddByte(0);
	}

	return &Command;
}

BOOL CMsSqlSeverConnection::SendSQLBatchCommandsAndGetReply(CTdsPacket& ReceivedPacket, PCSTR pCommand)
{
	UINT uLength = strlen(pCommand);

	CTdsBytes Command(uLength * 2);

	MakeCommandBytes(Command, pCommand);

	UINT uIndex = 0;

	while( uIndex != NOTHING ) {

		CTdsSqlBatch Packet(m_uMaxPacketSize);

		uIndex = Packet.Append(Command, uIndex, m_uMaxPacketSize);

		if( uIndex != NOTHING ) {

			SendCommand(Packet);
		}
		else {
			return SendCommandAndGetReply(Packet, ReceivedPacket);
		}
	}

	return FALSE;
}

BOOL CMsSqlSeverConnection::SendBulkData(CTdsBulkLoad& PendingBulkData, CTdsPacket& Response, BOOL fFinished)
{
	UINT uIndex = 0;

	while( PendingBulkData.GetSize() > m_uMaxPacketSize - TDS_HEADER_BYTES ) {

		CTdsBulkLoad Packet(m_uMaxPacketSize, TRUE);

		while( Packet.GetSize() < m_uMaxPacketSize ) {

			Packet.AddByte(PendingBulkData.GetByte(uIndex));
		}

		Packet.EndPacket(statusNormal);

		PendingBulkData.RemoveHead(uIndex);

		uIndex = 0;

		SendCommand(Packet);
	}

	if( fFinished ) {

		CTdsBulkLoad Packet(m_uMaxPacketSize, TRUE);

		while( uIndex < PendingBulkData.GetSize() ) {

			Packet.AddByte(PendingBulkData.GetByte(uIndex));
		}

		Packet.EndPacket(statusEnd);

		return SendCommandAndGetReply(Packet, Response);
	}

	return FALSE;
}

BOOL CMsSqlSeverConnection::SendCommand(CTdsPacket& SentPacket)
{
	if( !CheckSocket() ) {

		return FALSE;
	}

	UINT  uSize = SentPacket.GetSize();

	PBYTE pData = SentPacket.GetDataMod();

	while( uSize ) {

		UINT uCopy    = min(1280, uSize);

		UINT uRetries = countSocketSendRetries;

		while( !(m_pSock->Send(pData, uCopy) == S_OK) ) {

			if( !(--uRetries) ) {

				return FALSE;
			}

			Sleep(timeSocketSendDelay);

			uCopy = min(1280, uSize);
		}

		uSize -= uCopy;

		pData += uCopy;
	}

	return TRUE;
}

BOOL CMsSqlSeverConnection::SendCommandAndGetReply(CTdsPacket& SentPacket, CTdsPacket& ReceivedPacket)
{
	if( SendCommand(SentPacket) ) {

		BOOL fMulti = TRUE;

		BOOL fFirst = TRUE;

		CTdsBytes Bytes;

		SetTimer(timeSocketReadMaxWait);

		while( fMulti && GetTimer() ) {

			BYTE bData[8];

			UINT uRecv = 8;

			if( m_pSock->Recv(bData, uRecv) == S_OK ) {

				UINT uLen = MAKEWORD(bData[3], bData[2]) - 8;

				if( fFirst ) {

					Bytes.AddData(bData, 8);

					fFirst = FALSE;
				}

				if( bData[1] == 0x01 ) {

					fMulti = FALSE;
				}
				else {
					fMulti = TRUE;
				}

				UINT uData = uLen;

				while( uData > 0 ) {

					if( !CheckSocket() ) {

						CloseSocket(TRUE);

						return FALSE;
					}

					PBYTE pData = new BYTE[maxPacketSize];

					UINT  uRx   = min(uData, maxPacketSize);

					if( m_pSock->Recv(pData, uRx) != S_OK ) {

						CloseSocket(TRUE);

						delete[] pData;

						return FALSE;
					}

					Bytes.AddData(pData, uRx);

					delete[] pData;

					uData -= uRx;

					Sleep(timeSocketReadDelay);
				}
			}

			Sleep(timeSocketReadDelay);
		}

		return ReceivedPacket.Parse(Bytes.GetData(), Bytes.GetSize()) > 0;
	}

	return FALSE;
}

// Database Actions

BOOL CMsSqlSeverConnection::ServerLogin(void)
{
	CTdsPreLogin SendMsg, RecvMsg;

	SendMsg.Create("", 0x12345678, m_uTlsMode > 0);

	if( SendCommandAndGetReply(SendMsg, RecvMsg) ) {

		BYTE bEncrypt = RecvMsg.GetEncryption();

		if( bEncrypt == 1 || bEncrypt == 3 ) {

			if( !SwitchToTls() ) {

				return false;
			}
		}
		else {
			if( m_uTlsMode == 2 ) {

				CloseSocket(true);

				return false;
			}
		}

		CTdsLogin    SendMsg;

		CTdsResponse RecvMsg;

		SendMsg.Create("Crimson3Host",
			       m_pUser,
			       m_pPass,
			       "Crimson3",
			       m_pDatabase,
			       m_uMaxPacketSize
		);

		if( SendCommandAndGetReply(SendMsg, RecvMsg) ) {

			if( !RecvMsg.HaveErrorFromServer() ) {

				if( RecvMsg.GetPacketSizeValue() < m_uMaxPacketSize ) {

					m_uMaxPacketSize = RecvMsg.GetPacketSizeValue();
				}

				return true;
			}
		}
	}

	CloseSocket(true);

	return false;
}

bool CMsSqlSeverConnection::SwitchToTls(void)
{
	// TDS is a little strange in that the initial TLS negotiation is
	// carried out by embedding the handshake messages in TDS prelogin
	// packets. To handle this, we create a fake child socket that will
	// capture the TLS transmissions and let us feed respones back. Once
	// the TLS socket is happy, we switch its to use our TDS socket as
	// its child socket and let everything run in TLS from there.

	if( CreateTlsContext() ) {

		CFakeSocket *pFake = New CFakeSocket;

		ISocket	    *pSock = m_pTls->CreateSocket(pFake, NULL, 0);

		pSock->Connect(CIPAddr(), 0);

		for( ;;) {

			UINT Phase = 0;

			for( UINT n = 0; n < 10; n++ ) {

				pSock->GetPhase(Phase);
			}

			if( Phase == PHASE_OPEN ) {

				pSock->SetOption(OPT_SET_CHILD, UINT(m_pSock));

				pFake->Release();

				m_pSock = pSock;

				return true;
			}

			if( Phase == PHASE_OPENING ) {

				PCBYTE pData;

				UINT   uData;

				// We should have data to send, so grab it.

				if( pFake->GetTxData(pData, uData) ) {

					/*AfxTrace("SEND:\n");

					AfxDump(pData, uData);

					AfxTrace("\n");*/

					// Create a prelogin packet with a payload.

					CTdsPreLogin SendMsg, RecvMsg;

					SendMsg.AddPayload(pData, uData);

					SendMsg.EndPacket();

					// Exchange packets with the server.

					if( SendCommandAndGetReply(SendMsg, RecvMsg) ) {

						// Find the reply payload contents.

						pData = RecvMsg.GetPayloadData();

						uData = RecvMsg.GetPayloadSize();

						if( uData ) {

							// And pass them on to the TLS socket.

							/*AfxTrace("RECV:\n");

							AfxDump(pData, uData);

							AfxTrace("\n");*/

							pFake->SetRxData(pData, uData);

							continue;
						}
					}
				}
			}

			break;
		}

	// This will also release pFake.

		pSock->Release();
	}

	return false;
}

// Socket Management

BOOL CMsSqlSeverConnection::CheckSocket(void)
{
	if( m_pSock ) {

		UINT Phase;

		m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
		}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CMsSqlSeverConnection::OpenSocket(void)
{
	if( !CheckSocket() ) {

		m_pSock = NULL;

		AfxNewObject("sock-tcp", ISocket, m_pSock);

		if( m_pSock ) {

			if( m_pSock->Connect(m_IP, m_wPort) == S_OK ) {

				SetTimer(5000);

				while( GetTimer() ) {

					UINT Phase;

					m_pSock->GetPhase(Phase);

					if( Phase == PHASE_OPEN ) {

						return TRUE;
					}

					if( Phase == PHASE_CLOSING ) {

						CloseSocket(FALSE);

						return FALSE;
					}

					if( Phase == PHASE_ERROR ) {

						CloseSocket(TRUE);

						return FALSE;
					}

					Sleep(10);
				}

				CloseSocket(TRUE);
			}
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CMsSqlSeverConnection::OpenSocket(BOOL fTCP, WORD wPort)
{
	if( !CheckSocket() ) {

		m_pSock = NULL;

		AfxNewObject(fTCP ? "sock-tcp" : "sock-udp", ISocket, m_pSock);

		if( m_pSock ) {

			if( m_pSock->Connect(m_IP, wPort) == S_OK ) {

				SetTimer(5000);

				while( GetTimer() ) {

					UINT Phase;

					m_pSock->GetPhase(Phase);

					if( Phase == PHASE_OPEN ) {

						return TRUE;
					}

					if( Phase == PHASE_CLOSING ) {

						CloseSocket(FALSE);

						return FALSE;
					}

					if( Phase == PHASE_ERROR ) {

						CloseSocket(TRUE);

						return FALSE;
					}

					Sleep(10);
				}

				CloseSocket(TRUE);
			}
		}

		return FALSE;
	}

	return TRUE;
}

void CMsSqlSeverConnection::CloseSocket(BOOL fAbort)
{
	if( m_pSock ) {

		if( fAbort )

			m_pSock->Abort();
		else
			m_pSock->Close();

		m_pSock->Release();

		m_pSock = NULL;

		m_fOpen = FALSE;
	}
}

BOOL CMsSqlSeverConnection::CreateTlsContext(void)
{
	if( g_pMatrix ) {

		if( !m_pTls ) {

			g_pMatrix->CreateClientContext(m_pTls);

			return m_pTls ? TRUE : FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

// End of File
