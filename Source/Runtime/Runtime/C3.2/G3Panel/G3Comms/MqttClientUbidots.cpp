
#include "Intern.hpp"

#include "Service.hpp"

#include "Matrix.hpp"

#include "MqttClientUbidots.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CloudServiceUbidots.hpp"

#include "MqttClientOptionsUbidots.hpp"

#include "CloudDataSet.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client
//

// Constructor

CMqttClientUbidots::CMqttClientUbidots(CCloudServiceUbidots *pService, CMqttClientOptionsUbidots &Opts) : CMqttClientJson(pService, Opts), m_Opts(Opts)
{
	m_Will.SetTopic(m_Opts.m_PubTopic);

	m_Will.SetData(GetWillData());

	UINT c = m_pService->GetSetCount();

	for( UINT n = 0; n < c; n++ ) {

		CCloudDataSet *pDataSet = m_pService->GetDataSet(n);

		if( n == 0 ) {

			pDataSet->m_Mode = 0;
		}
		else {
			CCloudTagSet *pTagSet   = (CCloudTagSet *) pDataSet;

			pTagSet->m_Write        = 0;

			if( pDataSet->m_Mode == 3 ) {

				pTagSet->m_Write = 1;
			}
		}
	}
}

// Client Hooks

void CMqttClientUbidots::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseInitial ) {

		UINT c = m_pService->GetSetCount();

		for( UINT n = 1; n < c; n++ ) {

			CCloudTagSet *pSet = (CCloudTagSet *) m_pService->GetDataSet(n);

			if( pSet->HasWrites() ) {

				CStringArray TagNames;

				if( pSet->GetTagNames(TagNames) ) {

					for( UINT tag = 0; tag < TagNames.GetCount(); tag++ ) {

						CString SubTopic = m_Opts.m_PubTopic + "/" + TagNames[tag].ToLower() + "/lv";

						AddToSubList(subData, SubTopic);
					}
				}
			}
		}
	}

	CMqttClientJson::OnClientPhaseChange();
}

BOOL CMqttClientUbidots::OnClientNewData(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode == subData ) {

		if( pMsg->m_Topic.EndsWith("/lv") ) {

			UINT s;

			if( (s = pMsg->m_Topic.GetLength()) > 3 ) {

				CString Top = pMsg->m_Topic.Left(s - 3);

				CString Tag = Top.TokenLast('/');

				if( !Tag.IsEmpty() ) {

					if( Tag != "timestamp" && Tag != "connected" ) {

						CString Val;

						if( pMsg->GetText(Val) ) {

							CMqttJsonData Json;

							Json.AddValue(Tag, Val);

							OnWrite(Json, "");
						}
					}
				}
			}

			return TRUE;
		}

		CMqttJsonData Json;

		if( pMsg->GetJson(Json) ) {

			OnWrite(Json, "");
		}

		return TRUE;
	}

	return TRUE;
}

// Publish Hook

BOOL CMqttClientUbidots::GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg)
{
	if( FALSE ) {

		uMode = CCloudDataSet::modeForce;
	}

	if( CMqttClientJson::GetPubMessage(uTopic, uTime, fTemp, uMode, pMsg) ) {

		pMsg->SetTopic(m_Opts.m_PubTopic);

		return TRUE;
	}

	return FALSE;
}

// End of File
