
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SERVSQL_HPP
	
#define	INCLUDE_SERVSQL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "service.hpp"

#include "stdclient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTdsPacket;
class CTdsSQLBatch;
class CTdsResponse;
class CTdsBulkLoad;
class CTdsBytes;
class CSqlSync;

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

#if !defined(__GNUC__)

AfxDefineType(ULONGLONG, unsigned __int64);

#else

AfxDefineType(ULONGLONG, unsigned long long);

#endif

//////////////////////////////////////////////////////////////////////////
//
// SQL Sync Service
//

class CSqlSync : public CServiceItem,
		 public CStdClient,
		 public ITaskEntry,
		 public IServiceSqlSync
{
	public:
		// Constructor
		CSqlSync(void);

		// Destructor
		~CSqlSync(void);

		// Initialization
		void Load(PCBYTE &pData);

		// IService
		UINT GetID(void);
		void GetTaskList(CTaskList &List);

		// ITaskEntry
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// IServiceSqlSync
		void Force(void);
		BOOL IsRunning(void);
		UINT GetSyncTime(UINT uType);
		UINT GetLastStatus(void);

		// Socket Information
		UINT GetCmdPort(void);
		DWORD GetServer(void);

		// Connection Information
		CString & GetUser(void);
		CString & GetPassword(void);
		CString & GetDatabaseName(void);

		// Table Information.
		CString & GetTablePrefix(void);

		// Item Properties
		CCodedItem  * m_pEnable;
		CCodedItem  * m_pDatabaseServer;
		CCodedItem  * m_pPort;
		CCodedItem  * m_pTlsMode;
		CCodedItem  * m_pUser;
		CCodedItem  * m_pPass;
		CCodedItem  * m_pDatabaseName;
		CCodedItem  * m_pTablePrefix;		
		CCodedItem  * m_pFreq;
		CCodedItem  * m_pFreqMins;
		CCodedItem  * m_pDelay;
		CCodedItem  * m_pLogFile;
		CCodedItem  * m_pForce;
		CCodedItem  * m_pCheckLabels;
		CCodedItem  * m_pFireTriggers;
		CCodedItem  * m_pDrive;
		UINT          m_uPrimaryKey;

	protected:
		// Configuration
		struct CConfig
		{
			BOOL	m_fEnable;
			DWORD	m_uDatabaseServer;
			UINT	m_uPort;
			UINT    m_uTlsMode;
			CString	m_User;
			CString	m_Pass;
			CString	m_DatabaseName;
			CString m_TablePrefix;
			UINT	m_uFreqH;
			UINT	m_uFreqM;
			UINT	m_uDelay;
			BOOL	m_fLogFile;
			BOOL	m_fForce;
			BOOL    m_fCheckLabels;
			BOOL    m_fFireTriggers;
			UINT    m_uDrive;
			};

		// Column Info
		struct CColumnInfo
		{
			CString	m_ColumnName;
			BYTE	m_eSQLDataType;
			WORD	m_wSQLDataSize;
			};

		// Column Values
		struct CColumnValues
		{
			CString m_Value;
			CString m_Time;
			CString m_Date;
			WORD	m_Size;
			};

		// Status values
		enum {
			sqlStatPending	= 0,
			sqlStatSuccess	= 1,
			sqlStatFailure	= 2
			};

		// Primary Key Types
		enum {
			sqlKeyDatetime	= 0,
			sqlKeyAutoInc	= 1
			};

		// Column Value Results
		enum {
			sqlValueDone,
			sqlValueFound,
			sqlValueEndOfBuff,
			};

		// Data Members
		CConfig			m_Cfg;
		char			m_cSep;
		CArray<CColumnInfo>	m_ColumnInfo;
		CColumnValues **	m_ColValues;
		CString			m_ColDate;
		CString			m_ColTime;
		CString			m_LastDateSent;
		CString			m_LastTimeSent;
		CString			m_CurrentPathName;
		WORD			m_uMaxPacketSize;
		BOOL			m_fForce;
		BOOL			m_fHasSignature;
		CStringArray		m_LogNames;
		BOOL			m_fIsRunning;
		UINT			m_uLastSuccess;
		UINT			m_uLastFailure;
		UINT			m_uLastStarted;
		UINT			m_uStatus;
		ISemaphore *		m_pLock;

		// Implementation
		void FindConfig(CConfig &Cfg);
		bool ProcessAllLogFiles();
		bool ProcessFile(PCSTR szFileName, DWORD uLastTime, PCSTR szTableName);
		void UpdateEndStatus(BOOL fSuccess);
		void UpdateStartStatus(void);
		UINT FindMaxLine(void);

		// Socket Commands
		bool ConnectToSql(void);
		bool SwitchToTls(void);
		bool SendSqlBatchCommandsAndGetReply(CTdsPacket& clsReceivedPacket, PCSTR pCommand, PCSTR sDumpText);
		bool SendCommand(CTdsPacket& clsSentPacket);
		bool SendCommandAndGetReply(CTdsPacket& clsSentPacket, CTdsPacket& clsReceivedPacket);
		bool SendBulkData(CTdsBulkLoad& clsBulkData, CTdsPacket& clsResponse, bool fFinal);

		// Tracing
		void DumpPacket(PCSTR szDescriptor, CTdsPacket &clsPacket);
		void TraceInfo(PCSTR szMessage);
		void TraceError(PCSTR szMethod, PCSTR szMessage);
		void TraceFileError(PCSTR szMethod, PCSTR szMessage, PCSTR szFileName);
		void TracePacketErrors(CTdsResponse &clsResponsePacket);
		void TraceParameter(PCSTR szParamName, int iValue);
		void TraceTimeDate(PCSTR szDescriptor, DWORD uTimeDate);
		void TraceColumns(void);
		void TraceSqlCommand(PCSTR szCommandName, PCSTR szCommand);
		void LogProgress(CAutoFile &File);

		// File and File System Access
		BOOL   ReadNextItem(CAutoFile &File, PSTR szItem, BOOL fSkip, BOOL fAllowComma);
		BOOL   ReadNextItem(PBYTE pData, UINT uSize, UINT &uPos, PSTR szItem, BOOL fSkip, BOOL fAllowComma);
		BOOL   ReadNextLine(CAutoFile &File, PSTR szLine);
		void   ReplaceInvalidSQLChars(PSTR szValue);
		void   CreateTypeLookupName(PSTR szColumnName, PSTR szColumnNameTypeLookup);
		BOOL   LooksLikeArray(PSTR szValue);
		void   ReadColumnNames(CAutoFile &File);
		UINT   ReadColumnValues(CAutoFile &File, bool fOne, bool fTime = false, UINT uSearchTime = 0);
		DWORD  GetFirstTimeDateInFile(CAutoFile &File);
		void   WriteLastTimeDateProcessed(CString &szLastDate, CString &szLastTime);
		void   ReadLastTimeDateProcessed(CString &szLastDate, CString &szLastTime);

		// CSV Selection
		static int CsvSelect(struct dirent const *p);

		// SQL Command Creation
		void	    FormSQLColumnList(CString* szTheCommand, bool fAddPrimaryKey);
		CString   * FormTableCreationSQLCommand(PCSTR szTableName);
		CString   * FormInsertSQLCommand(CAutoFile &File, PCSTR szTableName, PCSTR szDate, PCSTR szTime);
		CString   * FormInsertBulkSQLCommand(CAutoFile &File, PCSTR szTableName);
		bool	    FormAndSendBulkLoadPackets(CAutoFile &File, CString &szFirstDateInPacket, CString &szFirstTimeInPacket, CString &szLastDateInPacket, CString &szLastTimeInPacket, CTdsPacket &clsResponse);
		void	    FormSQLDateTime(CString* szTheCommand, PCSTR szDate, PCSTR szTime);
		CString   * FormDeleteSQLCommand(PCSTR szTableName, PCSTR szStartDate, PCSTR szStartTime, PCSTR szEndDate, PCSTR szEndTime);
		CTdsBytes * MakeCommandBytes(CTdsBytes& bCommand, PCSTR pCommand);
		bool        ExecuteQuery(CString const &Query, CTdsPacket &Response);

		// Primary Key Operations
		bool SetPrimaryKey(CString const &Table);
		bool SetAutoIncrementKey(CString const &Table);
		bool SetDatetimeKey(CString const &Table);
		bool HasPrimaryKey(CString const &Table);
		UINT GetPrimaryKeyType(CString const &Table);
		bool RemoveKeyConstraint(CString const &Table);
		bool RemoveDatetimeKey(CString const &Table);
		bool RemoveIdentKey(CString const &Table);

		// Time and Date Methods
		DWORD     FATTimeToInternalTime(CTime &clsTime);
		DWORD     ParseTimeDate(PCSTR szDate, PCSTR szTime);
		DWORD     GetMillis(PCSTR szTime);
		ULONGLONG ConvertTimeDateToSQL(DWORD TimeDate, DWORD Millis = 0);
	};

// End of File

#endif
