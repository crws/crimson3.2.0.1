
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SECURE_HPP
	
#define	INCLUDE_SECURE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "datalog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CSecurityManager;
class CUserList;
class CUserItem;
class CSecDesc;

//////////////////////////////////////////////////////////////////////////
//
// Group Rights
//

#define	allowNone	UINT(0x00<<30)
#define	allowUsers	UINT(0x01<<30)
#define	allowAnyone	UINT(0x02<<30)
#define	allowDefault	UINT(0x03<<30)
#define	allowMask	UINT(0x03<<30)
#define	allowProgram	UINT(0x01<<29)
#define	allowCheck	UINT(0x01<<28)
#define	allowMaint	UINT(0x01<<15)
#define	allowWebServer	UINT(0x01<<14)

//////////////////////////////////////////////////////////////////////////
//
// Logging Options
//

#define	logNone		0
#define	logUser		1
#define	logAll		2
#define	logDefault	3

//////////////////////////////////////////////////////////////////////////
//
// UI Support
//

interface ISecureUI
{
	// Status Hooks
	virtual BOOL CanShowKeypad(void) = 0;
	virtual BOOL IsRunningCode(void) = 0;
	virtual UINT GetKeypadTimeout(void) = 0;

	// Keypad Hooks
	virtual void KeypadPush(void) = 0;
	virtual void KeypadPull(void) = 0;
	virtual void KeypadShow(UINT uType, PCTXT pTitle) = 0;
	virtual void KeypadSetText(PCTXT pText, UINT uCursor) = 0;
	virtual BOOL KeypadEvent(CInput &i) = 0;

	// Drawing Hooks
	virtual void SecStartDraw(void) = 0;
	virtual void SecUpdate(void) = 0;
	virtual void SecEndDraw(void) = 0;
	virtual void SecRunEvents(void) = 0;

	// Widget Hooks
	virtual void SuspendWidget(void) = 0;
	virtual void RestoreWidget(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// User Manager
//

class CSecurityManager : public CItem, public ITaskEntry
{
	public:
		// Constructor
		CSecurityManager(void);

		// Destructor
		~CSecurityManager(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Management
		void Init(void);
		void Term(void);

		// Task List
		void GetTaskList(CTaskList &List);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Attributes
		UINT        GetUserCount(void);
		UINT        GetUserIndex(PCTXT pUser);
		CString     GetUserRealName(UINT uIndex);
		CUserItem * GetUserPtr(UINT uIndex);
		CString     GetCurrentUserName(void);
		CString     GetCurrentUserRealName(void);
		DWORD       GetCurrentUserRights(void);

		// Operations
		BOOL SaveDatabase(UINT uMode, PCTXT pName);
		BOOL LoadDatabase(UINT uMode, PCTXT pName);

		// Logging Operations
		void LogNewBatch(UINT uSlot, DWORD Secs, PCTXT pName);
		void LogCheck(void);
		void LogSave(void);

		// UI Task Entries
		void RegisterUI(ISecureUI *pUI);
		void ProcessEvent(INPUT Event);
		void FocusChanged(void);
		void KeypadHidden(void);
		void PageSelected(CSecDesc *pDesc);
		void ShutdownUI(void);

		// Permission Checks
		BOOL SimplePrompt(PCTXT pPrompt);
		BOOL AllowAccess(DWORD Access);
		BOOL AllowWrite(BOOL fMap, CSecDesc *pDesc);
		BOOL AllowWrite(CTag *pTag, UINT uPos, BOOL fMap, CSecDesc *pDesc, DWORD Data, UINT Type);
		BOOL AllowPageChange(CSecDesc *pDesc);
		BOOL HasAllAccess(DWORD Access);
		BOOL HasAccess(DWORD Access);
		BOOL TestAccess(DWORD Access, PCTXT pPrompt);
		BOOL TestAccess(DWORD Access, UINT uCode);

		// User Validation
		BOOL    ValidateLogon(PCTXT pUser, PCTXT pPass, DWORD Access);
		DWORD   ValidateLogon(PCTXT pUser, PCTXT pPass);
		CString GetPassword(PCTXT pUser, DWORD Access, BOOL fAddUser);
		BOOL    AllowRemote(void);

		// User Operations
		void LogOn(void);
		void LogOff(void);

		// Manager APIs
		void AddCard(UINT uIndex);
		void AddFinger(UINT uIndex);
		void SetPassword(UINT uIndex);

		// Data Members
		UINT        m_Handle;
		CUserList * m_pUsers;
		UINT	    m_DefMapped;
		UINT	    m_DefLocal;
		UINT	    m_DefPage;
		UINT	    m_LogMapped;
		UINT	    m_LogLocal;
		UINT	    m_LogEnable;
		UINT	    m_FileLimit;
		UINT	    m_FileCount;
		UINT	    m_WithBatch;
		UINT        m_SignLogs;
		UINT        m_Drive;
		UINT	    m_UserTime;
		UINT	    m_UserClear;
		UINT	    m_CheckTime;
		UINT	    m_Every;

	protected:
		// Queue Entry
		struct CEntry
		{
			DWORD	 m_Time;
			CString  m_Line;
			CEntry * m_pNext;
			CEntry * m_pPrev;
			};

		// Logging Queue
		ISemaphore * m_pWait;
		CEntry     * m_pHead;
		CEntry     * m_pTail;

		// Data Members
		ISecureUI  * m_pUI;
		HTASK        m_hUI;
		IMutex     * m_pLock;
		IEvent     * m_pAccept;
		CUnicode     m_TagName;
		CUnicode     m_OldData;
		CUnicode     m_NewData;
		UINT	     m_uEnroll;
		CUserItem  * m_pEnroll;
		CUserItem  * m_pRemote;
		CLogHelper * m_pHelp;
		char	     m_cSep;

		// Event Context
		INPUT	     m_Event;
		INPUT	     m_Last;
		BOOL	     m_fAsked;
		BOOL	     m_fFailed;
		BOOL	     m_fMaint;
		UINT	     m_uTime;
		UINT	     m_uCheckCode;
		UINT	     m_uCheckTime;
		CUserItem  * m_pUser;

		// Other Tasks
		UINT	     m_uTaskList[8];
		CUserItem *  m_pTaskUser[8];

		// UI Engine
		BOOL    m_fBusy;
		UINT	m_uState;
		BOOL	m_fDone;
		BOOL    m_fChange;
		CString	m_Prompt;
		CString	m_ReqUser;
		CString	m_ReqPass;
		CString m_RepPass;

		// Card Data
		IPortObject  * m_pCardPort;
		IDataHandler * m_pCardData;

		// User Database
		BOOL LookupUser(void);
		BOOL HasAllRights(DWORD Access);
		BOOL HasRights(DWORD Access);

		// UI Engine
		BOOL CheckBefore(UINT uCode);
		BOOL GetCredentials(void);
		void ModalLoop(UINT uState);
		void SetAbort(void);
		void SetState(UINT uState);
		void NewState(void);
		BOOL OnKey(UINT uTrans, BOOL fLocal, UINT uCode);
		BOOL EnterString(UINT uTrans, UINT uCode, CString &Text, UINT uNext);
		void ShowString(void);

		// Logging
		BOOL BuildChange(CTag *pTag, UINT uPos, DWORD Data, UINT Type);
		void BuildPrompt(void);
		void LogStartup(void);
		void LogLogon(void);
		void LogLogoff(void);
		void LogChange(void);
		void LogChange(CUserItem *pUser, CTag *pTag, UINT uPos, DWORD Data, UINT Type);
		void LogChange(CUnicode TagName, CUnicode OldData, CUnicode NewData, CUnicode User, UINT uTag);
		void LogEvent(CString const &Line, UINT uTag);

		// Task Users
		void        AddTaskUser(CUserItem *pUser);
		CUserItem * GetTaskUser(void);

		// Implementation
		DWORD GetAccess (BOOL fMap, CSecDesc *pDesc);
		DWORD GetLogging(BOOL fMap, CSecDesc *pDesc);
		BOOL  CheckTimeout(void);
		void  ShowKeypad(UINT uType, PCTXT pText);
		void  PostSecurityEvent(UINT uCode);
		void  PostUpEvent(void);
		BOOL  IsSameEvent(void);
		BOOL  IsSameState(UINT s1, UINT s2);
		BOOL  ConvertIndex(UINT uIndex);
		BOOL  IsUITask(void);
		void  UpdateSep(void);

		// CSV Encoding
		CString Encode(CUnicode Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// User List
//

class CUserList : public CItem
{
	public:
		// Constructor
		CUserList(void);

		// Destructor
		~CUserList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		void LoadCreds(void);
		void SaveCreds(void);

		// Data Members
		UINT         m_uCount;
		CUserItem ** m_ppUser;
	};

//////////////////////////////////////////////////////////////////////////
//
// User Item
//

class CUserItem : public CCodedHost
{
	public:
		// Constructor
		CUserItem(void);

		// Destructor
		~CUserItem(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		CString  GetUserName(void) const;
		CUnicode GetRealName(void) const;

		// Operations
		void LoadCred(void);
		void SaveCred(BOOL fCommit);

		// Credentials
		struct CCred
		{
			WORD	m_wMagic;
			char	m_sPass[16];
			char	m_sCard[7];
			BYTE	m_bBioSlot;
			};

		// Data Members
		DWORD        m_Addr;
		CString      m_Name;
		CCodedText * m_pRealName;
		CCodedText * m_pPassword;
		UINT         m_Force;
		DWORD	     m_Rights;

		// Credentials
		CCred m_Cred;
	};

//////////////////////////////////////////////////////////////////////////
//
// Security Descriptor
//

class CSecDesc : public CItem
{
	public:
		// Constructor
		CSecDesc(void);

		// Destructor
		~CSecDesc(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Item Properties
		UINT m_Access;
		UINT m_Logging;
	};

// End of File

#endif
