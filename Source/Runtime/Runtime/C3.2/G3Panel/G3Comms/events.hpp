
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EVENTS_HPP
	
#define	INCLUDE_EVENTS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "loghelp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Event Information
//

/*struct CEventInfo
{
	DWORD	 m_Time;
	DWORD	 m_Type   :2;
	DWORD    m_Source :3;
	DWORD    m_HasText:1;
	DWORD    m_Code   :26;
	CUnicode m_Text;
	};*/

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CEventManager;
class CEventLogger;
class CAlarmManager;

//////////////////////////////////////////////////////////////////////////
//
// Event Manager
//

class CEventManager : public IEventConsumer, public IAlarmStatus, public IEventManager
{
	public:
		// Constructor
		CEventManager(void);

		// Destructor
		~CEventManager(void);

		// Initialization
		void Load(void);

		// Serialization
		BOOL Lock(void);
		void Free(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IEventConsumer
		UINT	       Register(IEventSource *pSource, PCTXT pName);
		BOOL	       LogEvent(DWORD dwTime, UINT Source, UINT Type, UINT Code, CUnicode const &Text);
		BOOL	       LogEvent(DWORD dwTime, UINT Source, UINT Type, UINT Code);
		CActiveAlarm * FindAlarm(UINT Source, UINT Code);
		CActiveAlarm * MakeAlarm(UINT Source, UINT Code);
		void	       EditAlarm(CActiveAlarm *pInfo);
		void           EditAlarm(CActiveAlarm *pInfo, UINT State);
		void           MoveAlarm(CActiveAlarm *pInfo);

		// IAlarmStatus
		void LockAlarmData(void);
		void FreeAlarmData(void);
		UINT GetAlarmSequence(void);
		UINT GetTotalAlarms(void);
		UINT GetActiveAlarms(void);
		UINT GetUnacceptedAlarms(void);
		UINT GetUnacceptedAndAutoAlarms(void);
		UINT ReadAlarmList(CActiveAlarm * &pHead, UINT uPos, UINT uSize, BOOL fReverse);
		void AcceptAlarm(CActiveAlarm *pInfo);
		void AcceptAlarm(UINT Source, UINT Code);
		BOOL AcceptAlarmEx(UINT Source, UINT Method, UINT Code);
		void AcceptAll(void);

		// IEventManager
		IEventSource * FindSource(UINT Source);

		// Data Members
		CEventLogger * m_pLogger;

	protected:
		ULONG	       m_uRefs;
		IMutex       * m_pLock;
		BOOL	       m_fBusy;
		IEventSource * m_pSource[8];
		CActiveAlarm * m_pHead;
		CActiveAlarm * m_pTail;
		UINT           m_uCount[6];
		UINT	       m_uSequence;
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Logger
//

class CEventLogger : public IEventLogger
{
	public:
		// Constructor
		CEventLogger(CEventManager *pManager);

		// Destructor
		~CEventLogger(void);

		// Initialization
		void Load(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IEventLogger
		void     LockEventData(void);
		void     FreeEventData(void);
		UINT     GetEventSequence(void);
		UINT     GetEventCount(void);
		CUnicode GetEventText(UINT uPos);
		CUnicode GetEventName(UINT uPos);
		DWORD    GetEventTime(UINT uPos);
		UINT     GetEventType(UINT uPos);
		void	 AddEvent(CEventInfo const &Info);

		// Attributes
		CUnicode GetLastText(BOOL fAll);
		CUnicode GetLastType(BOOL fAll);
		DWORD    GetLastTime(BOOL fAll);

		// Operations
		void LogCheck(void);
		void LogLoad(void);
		void LogSave(void);
		void LogNewBatch(UINT uSlot, DWORD Secs, PCTXT pName);
		BOOL LogEvent(CEventInfo const &Info, BOOL fMemory);
		BOOL LogInitEvent(CEventInfo const &Info);
		void LogClear(void);
		
		// Item Properties
		UINT m_LogToDisk;
		UINT m_FileLimit;
		UINT m_FileCount;
		UINT m_WithBatch;
		UINT m_SignLogs;
		UINT m_LogToPort;
		UINT m_Drive;

		// Instance Pointer
		static CEventLogger * m_pThis;

	protected:
		// Data Members
		ULONG		m_uRefs;
		CEventManager * m_pManager;
		CLogHelper    * m_pHelp;
		IMutex        * m_pSave;
		CEventInfo      m_Buffer[1000];
		UINT            m_uSize;
		UINT	        m_uSeq;
		UINT	        m_uCount;
		BOOL	        m_fWrap;
		BOOL	        m_fLost;
		UINT            m_uHead;
		UINT            m_uTail;
		UINT            m_uSave;
		char		m_cSep;

		// Port Output
		void PrintEvent(CEventInfo const &Info);
		
		// File Output
		BOOL WriteHead(CFastFile &csv);
		void WriteTime(CFastFile &csv);
		void WriteData(CFastFile &csv);

		// Implementation
		UINT FindLastIndex(BOOL fAll);
	};

//////////////////////////////////////////////////////////////////////////
//
// Alarm Manager
//

class CAlarmManager
{
	public:
		// Constructor
		CAlarmManager(void);

		// Destructor
		~CAlarmManager(void);
		
		// Attributes
		BOOL IsAlarmActive(UINT uIndex);

		// Operations
		BOOL UpdateAlarm(CTagEvent *pEvent, UINT uIndex, CUnicode const &Text, BOOL fState);
		BOOL AcceptAlarm(UINT uIndex);

	protected:
		// Data Members
		CEventManager * m_pEvents;

		// Object Access
		CTagEvent * GetObject(CActiveAlarm *pInfo);

		// Implementation
		BOOL Action(CActiveAlarm *pInfo, UINT uAction);
		void OnFire(CActiveAlarm *pInfo);
		void OnRefire(CActiveAlarm *pInfo);
		void OnFire(CActiveAlarm *pInfo, UINT uMask);
		void OnAccept(CActiveAlarm *pInfo);
		void OnClear(CActiveAlarm *pInfo);
		void LogEvent(UINT Type, CActiveAlarm *pInfo);
		void RunAction(CCodedItem *pAction);
	};

// End of File

#endif
