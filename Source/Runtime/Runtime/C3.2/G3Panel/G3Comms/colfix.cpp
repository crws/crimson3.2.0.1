
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fixed Display Color
//

// Constructor

CDispColorFixed::CDispColorFixed(void)
{
	m_Color = MAKELONG(GetRGB(31,31,31),GetRGB(0,0,0));
	}

// Destructor

CDispColorFixed::~CDispColorFixed(void)
{
	}

// Initialization

void CDispColorFixed::Load(PCBYTE &pData)
{
	ValidateLoad("CDispColorFixed", pData);

	m_Color = GetLong(pData);
	}

// Color Access

DWORD CDispColorFixed::GetColorPair(DWORD Data, UINT Type)
{
	return m_Color;
	}

// End of File
