
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Linked Display Color
//

// Constructor

CDispColorLinked::CDispColorLinked(void)
{
	m_pColor = NULL;
	}

// Destructor

CDispColorLinked::~CDispColorLinked(void)
{
	}

// Initialization

void CDispColorLinked::Load(PCBYTE &pData)
{
	ValidateLoad("CDispColorLinked", pData);

	m_Tag = GetWord(pData);
	}

// Scan Control

BOOL CDispColorLinked::IsAvail(void)
{
	if( FindColor() ) {

		return m_pColor->IsAvail();
		}

	return TRUE;
	}

BOOL CDispColorLinked::SetScan(UINT Code)
{
	if( FindColor() ) {

		return m_pColor->SetScan(Code);
		}

	return TRUE;
	}

// Color Access

DWORD CDispColorLinked::GetColorPair(DWORD Data, UINT Type)
{
	if( FindColor() ) {

		return m_pColor->GetColorPair(Data, Type);
		}

	return CDispColor::GetColorPair(Data, Type);
	}

// Implementation

BOOL CDispColorLinked::FindColor(void)
{
	if( !m_pColor ) {

		CTagList *pTags = CCommsSystem::m_pThis->m_pTags->m_pTags;

		CTag     *pTag  = pTags->GetItem(m_Tag);

		if( pTag ) {

			if( (m_pColor = pTag->m_pColor) ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	return TRUE;
	}

// End of File
