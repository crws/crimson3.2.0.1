  
#include "intern.hpp"

#include "service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instance Pointers
//

IServiceFileSync    * g_pServiceFileSync    = NULL;

IServiceMail        * g_pServiceMail        = NULL;

IServiceSqlSync     * g_pServiceSqlSync     = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Services Collection
//

// Constructor

CServices::CServices(void)
{
	m_uCount = 0;

	m_ppServ = NULL;
	}

// Destructor

CServices::~CServices(void)
{
	while( m_uCount-- ) {

		if( m_ppServ[m_uCount] ) {

			m_ppServ[m_uCount]->Release();
			}
		}

	delete [] m_ppServ;
	}

// Initialization

void CServices::Load(PCBYTE &pData)
{
	ValidateLoad("CServices", pData);

	m_uCount = GetWord(pData);
	
	m_ppServ = New IService * [ m_uCount + 1 ];

	for( UINT i = 0; i < m_uCount; i++ ) {

		UINT       hServ = GetWord(pData);

		PCBYTE     pInit = PCBYTE(g_pDbase->LockItem(hServ));

		IService * pServ = NULL;

		if( pInit ) {

			if( GetWord(pInit) == IDC_SERVICE ) {

				BYTE bServ = GetByte(pInit);

				switch( bServ ) {

					case  1: pServ = Create_ServiceFileSync       (); break;
					case  3: pServ = Create_ServiceMail           (); break;
					case  7: pServ = Create_ServiceSqlSync        (); break;
					case 10: pServ = Create_ServiceOpcUa          (); break;
					case  8: pServ = Create_CloudServiceCumulocity(); break;
					case  9: pServ = Create_CloudServiceAws       (); break;
					case 11: pServ = Create_CloudServiceSparkplug (); break;
					case 12: pServ = Create_CloudServiceAzure     (); break;
					case 13: pServ = Create_CloudServiceGeneric   (); break;
					case 14: pServ = Create_CloudServiceGoogle    (); break;
					case 15: pServ = Create_CloudServiceUbidots   (); break;
					}

				if( pServ ) {

					pServ->LoadService(pInit);
					}
				else
					AfxTrace("Unsupported Service : Code = %u\n", bServ);
				}

			g_pDbase->FreeItem(hServ);
			}

		m_ppServ[i] = pServ;
		}
	}

// Task List

void CServices::GetTaskList(CTaskList &List)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppServ[n] ) {

			m_ppServ[n]->GetTaskList(List);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Service Base Class
//

// Constructor

CServiceItem::CServiceItem(void)
{
	StdSetRef();
	}

// Destructor

CServiceItem::~CServiceItem(void)
{
	}

// IUnknown

HRESULT CServiceItem::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IService);

	StdQueryInterface(IService);

	return E_NOINTERFACE;
	}

ULONG CServiceItem::AddRef(void)
{
	StdAddRef();
	}

ULONG CServiceItem::Release(void)
{
	StdRelease();
	}

// IService

void CServiceItem::LoadService(PCBYTE &pData)
{
	Load(pData);
	}

UINT CServiceItem::GetID(void)
{
	return 0;
	}

void CServiceItem::GetTaskList(CTaskList &List)
{
	}

// End of File
