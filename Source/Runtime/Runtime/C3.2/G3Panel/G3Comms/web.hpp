
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_WEB_HPP
	
#define	INCLUDE_WEB_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CWebBase;
class CWebPageList;
class CWebPage;

//////////////////////////////////////////////////////////////////////////
//
// Instantiator
//

extern CWebBase * Create_WebServer(void);

//////////////////////////////////////////////////////////////////////////
//
// Web Server Base Class
//

class CWebBase : public CCodedHost, public ITaskEntry
{
	public:
		// Constructor
		CWebBase(void);

		// Destructor
		~CWebBase(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Task List
		void GetTaskList(CTaskList &List);

		// Param Access
		virtual C3INT GetWebParamInt(PCTXT pName) = 0;
		virtual C3INT GetWebParamHex(PCTXT pName) = 0;
		virtual PUTF  GetWebParamStr(PCTXT pName) = 0;
		virtual PUTF  GetWebUser   (UINT n) = 0;
		virtual void  ClearWebUsers(void)   = 0;

		// Data Members
		UINT           m_Enable;
		UINT	       m_DbTime;
		CWebPageList * m_pList;
		CCodedText   * m_pTitle;
		CCodedText   * m_pHeader;
		CCodedText   * m_pHome;
		UINT	       m_RemoteView;
		UINT	       m_RemoteCtrl;
		UINT	       m_RemoteZoom;
		UINT	       m_RemoteFact;
		UINT	       m_RemoteBits;
		UINT	       m_RemoteSec;
		UINT	       m_LogsView;
		UINT	       m_LogsBatches;
		UINT	       m_LogsSec;
		UINT	       m_Source;
		UINT           m_Local;
		CCodedText   * m_pUser;
		CCodedText   * m_pReal;
		CCodedText   * m_pPass;
		UINT	       m_Restrict;
		UINT	       m_SecMask;
		UINT	       m_SecAddr;
		UINT	       m_UserSite;
		UINT	       m_UserMenu;
		UINT	       m_UserRoot;
		UINT	       m_UserDelay;
		UINT	       m_UserSec;
		UINT	       m_CustHome;
		UINT	       m_CustIcon;
		UINT	       m_CustLogon;
		UINT	       m_CustCss;
		UINT	       m_CustJs;
		UINT	       m_SystemPages;
		UINT	       m_SystemConsole;
		UINT	       m_SystemCapture;
		UINT	       m_SystemJump;
		UINT	       m_SystemSec;
		CCodedItem   * m_pPort;
		UINT           m_SameOrigin;

	protected:
		// Data Members
		IGDI     * m_pGDI;
		CTagList * m_pSrc;
	};

//////////////////////////////////////////////////////////////////////////
//
// Web Page List
//

class CWebPageList : public CItem
{
	public:
		// Constructor
		CWebPageList(void);
		
		// Destructor
		~CWebPageList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		void PreRegister(void);

		// Data Members
		UINT        m_uCount;
		CWebPage ** m_ppPage;
	};

//////////////////////////////////////////////////////////////////////////
//
// Web Page
//

class CWebPage : public CCodedHost
{
	public:
		// Constructor
		CWebPage(void);

		// Destructor
		~CWebPage(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		CString GetTitle(void) const;
		BOOL    HasTag(UINT uTag) const;
		
		// Operations
		void PreRegister(void);
		void SetScan(UINT Code);
		BOOL WaitAvailable(void);

		// Data Members
		CTagList   * m_pSrc;
		CString      m_Name;
		CCodedText * m_pTitle;
		UINT	     m_Delay;
		UINT	     m_Edit;
		UINT	     m_Hide;
		UINT         m_Refresh;
		UINT         m_Colors;
		UINT	     m_PageSec;
		UINT	     m_uTags;
		DWORD	   * m_pTags;
		BYTE	   * m_pType;

	protected:
		// Implementation
		BOOL LoadTagList(PCBYTE &pData);
	};

// End of File

#endif
