
#include "intern.hpp"

#include "proxy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Raw Proxy Object
//

class CProxyRaw : public CProxy, public CCodedHost
{
	public:
		// Constructor
		CProxyRaw(void);

		// Destructor
		~CProxyRaw(void);

		// Entry Points
		void Init(UINT uID);
		void Task(UINT uID);
		void Term(UINT uID);

	protected:
		// Data Members
		BOOL         m_fOpen;
		CCodedItem * m_pCode;

		// Implementation
		void RunCode(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw Proxy Object
//

// Instantiator

CProxy * Create_ProxyRaw(void)
{
	return New CProxyRaw;
	}

// Constructor

CProxyRaw::CProxyRaw(void)
{
	m_fOpen  = FALSE;

	m_pCode  = NULL;
	}

// Destructor

CProxyRaw::~CProxyRaw(void)
{
	}

// Entry Points

void CProxyRaw::Init(UINT uID)
{
	PCBYTE pData = m_pPort->m_pConfig;

	if( GetWord(pData) == 0x1234 ) {

		GetCoded(pData, m_pCode);
		
		if( m_pCode ) {

			m_pCode->SetScan(scanTrue);
			}
		}
	}

void CProxyRaw::Task(UINT uID)
{
	m_pCommsHelper->WontReturn();

	if( m_pPort->Open(m_pComms) ) {

		m_fOpen = TRUE;

		for(;;) {

			RunCode();
			
			m_pComms->Service();

			ForceSleep(20);
			}
		}
	
	Sleep(FOREVER);
	}

void CProxyRaw::Term(UINT uID)
{
	if( m_fOpen ) {

		m_pPort->Term();
		}
	}

// Implementation

void CProxyRaw::RunCode(void)
{
	if( m_pCode ) {

		if( m_pCode->IsAvail() ) {

			m_pCode->Execute(typeVoid);
			}
		}
	}

// End of File
