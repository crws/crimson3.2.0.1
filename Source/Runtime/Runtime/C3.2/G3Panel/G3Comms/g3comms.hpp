
/////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3COMMS_HPP

#define	INCLUDE_G3COMMS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <c3basis.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Float Helpers
//

#define IsNAN(r) isnanf(r)

#define IsINF(r) isinff(r)

//////////////////////////////////////////////////////////////////////////
//
// Class Identifiers
//

#define IDC_COMMS		0x6001
#define IDC_TAG_MANAGER		0x6002
#define IDC_TAG_BLOCK		0x6003
#define IDC_PROG_MANAGER	0x6004
#define IDC_PROGRAM		0x6005
#define IDC_SERVICE		0x6006
#define IDC_WEB_SERVER		0x6007
#define IDC_LOGGER		0x6008
#define IDC_SECURITY		0x6009
#define	IDC_LANGUAGE		0x600A
#define	IDC_CONTROL		0x600B
#define	IDC_SQL_MANAGER		0x600C

//////////////////////////////////////////////////////////////////////////
//
// DLD Type Definition
//

typedef void (MCALL *PDLD)(void *pData, UINT *pSize);

//////////////////////////////////////////////////////////////////////////
//
// Driver Creation
//

extern BOOL CreateDriver(UINT ID, void *pData, UINT *pSize);

//////////////////////////////////////////////////////////////////////////
//
// Driver PNG Support 
//

extern BOOL Png2Bmp(PBYTE pData, UINT uBytes, PBYTE pBitmap);

//////////////////////////////////////////////////////////////////////////
//
// Driver JPEG Support 
//

extern BOOL Jpeg2Bmp(PBYTE pData, UINT &uBytes, PBYTE &pBitmap, UINT uScale);

//////////////////////////////////////////////////////////////////////////
//
// Availability Checks
//

enum
{
	availNone = 0,
	availData = 1,
	};

//////////////////////////////////////////////////////////////////////////
//
// Error Types
//

enum
{
	errorInit = 0,
	errorNone = 1,
	errorSoft = 2,
	errorHard = 3
	};

//////////////////////////////////////////////////////////////////////////
//
// I/O Types
//

enum
{
	ioGet = 0,
	ioPut = 1
	};

//////////////////////////////////////////////////////////////////////////
//
// I/O Counters
//

enum
{
	countAttempt = 0,
	countSuccess = 1,
	countNoData  = 2,
	countRetry   = 3,
	countBusy    = 4,
	countSilent  = 5,
	countError   = 6,
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Types
//

enum
{
	eventEvent  = 0,
	eventAlarm  = 1,
	eventAccept = 2,
	eventClear  = 3
	};

//////////////////////////////////////////////////////////////////////////
//
// Alarm States
//

enum
{
	alarmIdle       = 0,
	alarmActive     = 1,
	alarmAutoAccept = 2,
	alarmWaitAccept = 3,
	alarmAccepted   = 4,
	alarmTotal	= 5,
	};

//////////////////////////////////////////////////////////////////////////
//
// Active Alarm
//

/*struct CActiveAlarm
{
	DWORD		m_Time;
	UINT		m_Source;
	UINT		m_Code;
	UINT		m_Level;
	CUnicode	m_Text;
	UINT		m_State;
	PVOID		m_pData;
	CActiveAlarm *	m_pNext;
	CActiveAlarm *	m_pPrev;
	};*/

//////////////////////////////////////////////////////////////////////////
//
// Event Interfaces
//

interface IEventConsumer;
interface IEventSource;
interface IEventLogger;
interface IAlarmStatus;

//////////////////////////////////////////////////////////////////////////
//
// Event Consumer Interface
//

interface IEventConsumer
{
	virtual UINT		Register(	IEventSource * pSource,
						PCTXT          pName
						) = 0;
	
	virtual BOOL		LogEvent(	DWORD		 dwTime,
						UINT             Source,
						UINT             Type,
						UINT             Code,
						CUnicode const & Text
						) = 0;
	
	virtual BOOL		LogEvent(	DWORD dwTime,
						UINT  Source,
						UINT  Type,
						UINT  Code
						) = 0;

	virtual CActiveAlarm *	FindAlarm(	UINT Source,
						UINT Code
						) = 0;
	
	virtual CActiveAlarm *	MakeAlarm(	UINT Source,
						UINT Code
						) = 0;

	virtual void		EditAlarm(	CActiveAlarm * pInfo,
						UINT	       State
						) = 0;

	virtual void		MoveAlarm(	CActiveAlarm * pInfo
						) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Source Interface
//

/*interface IEventSource
{
	virtual BOOL GetEventText(	CUnicode & Text,
					UINT       Code
					) = 0;

	virtual void AcceptAlarm(	UINT Method,
					UINT Code
					) = 0;

	virtual void AcceptAlarm(	CActiveAlarm * pInfo
					) = 0;
	};*/

//////////////////////////////////////////////////////////////////////////
//
// Event Logger Interface
//

/*interface IEventLogger
{
	virtual void	 LockEventData(		void
						) = 0;

	virtual void	 FreeEventData(		void
						) = 0;

	virtual UINT     GetEventSequence(	void
						) = 0;
	
	virtual UINT     GetEventCount(		void
						) = 0;
	
	virtual CUnicode GetEventText(		UINT uPos
						) = 0;
	
	virtual CUnicode GetEventName(		UINT uPos
						) = 0;
	
	virtual DWORD    GetEventTime(		UINT uPos
						) = 0;
	
	virtual UINT     GetEventType(		UINT uPos
						) = 0;
	};*/

//////////////////////////////////////////////////////////////////////////
//
// Alarm Status Interface
//

interface IAlarmStatus
{
	virtual void LockAlarmData(		void
						) = 0;

	virtual void FreeAlarmData(		void
						) = 0;

	virtual UINT GetAlarmSequence(		void
						) = 0;
	
	virtual UINT GetTotalAlarms(		void
						) = 0;
	
	virtual UINT GetActiveAlarms(		void
						) = 0;
	
	virtual UINT GetUnacceptedAlarms(	void
						) = 0;

	virtual UINT GetUnacceptedAndAutoAlarms(void
						) = 0;

	virtual UINT ReadAlarmList(		CActiveAlarm * & pHead,
						UINT             uPos,
						UINT		 uSize,
						BOOL		 fReverse
						) = 0;

	virtual void AcceptAlarm(		CActiveAlarm * pInfo
						) = 0;

	virtual void AcceptAlarm(		UINT Source,
						UINT Code
						) = 0;

	virtual void AcceptAll(			void
						) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Proxy Base Interface
//

interface IProxyBase
{
	virtual BOOL CheckPort(		UINT uPort
					) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Proxy Camera Interface
//

interface IProxyCamera : public IProxyBase
{
	// Image Access
	virtual void   ClaimData(	void
					) = 0;

	virtual void   FreeData(	void
					) = 0;

	virtual PCBYTE GetData(		UINT uDevice
					) = 0;

	virtual UINT GetInfo(		UINT uDevice, 
					UINT uIndex
					) = 0;

	// Inspection Transfer
	virtual BOOL SaveSetup(		UINT uDevice, 
					UINT uIndex, 
					PCTXT pName
					) = 0;

	virtual BOOL LoadSetup(		UINT uDevice, 
					UINT uIndex, 
					PCTXT pName
					) = 0;
	
	virtual BOOL UseSetup(		UINT uDevice, 
					UINT uIndex
					) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Camera Proxy Definition List Interface
//

interface IProxyCameraList
{
	virtual	IProxyCamera *	FindProxy(	UINT uPort
						) = 0;

	virtual	void		Append(		IProxyCamera *pProxy
						) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Proxy Display Interface
//

interface IProxyDisplay : public IProxyBase
{
	virtual void Render(		UINT uHandle,
					UINT uDrop, 
					PBYTE pData, 
					int cx, 
					int cy, 
					BOOL fPortrait
					) = 0;

	virtual void Remove(		UINT uHandle
					) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Display Proxy Definition List Interface
//

interface IProxyDisplayList
{
	virtual	IProxyDisplay *	FindProxy(	UINT uPort
						) = 0;

	virtual	void		Append(		IProxyDisplay *pProxy
						) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsManager;
class CTagManager;
class CEventManager;
class CAlarmManager;
class CProgramManager;
class CWebBase;
class CDataLogger;
class CSecurityManager;
class CLangManager;
class CSqlManager;
class CDataServer;
class CTag;
class CTagList;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

interface IStratonControl;

class CCommsSystem;

//////////////////////////////////////////////////////////////////////////
//
// Communications System Item
//

class CCommsSystem : public CSystemItem
{
	public:
		// Constructor
		CCommsSystem(void);

		// Destructor
		~CCommsSystem(void);

		// Initialization
		void Load(PCBYTE &pData);

		// System Calls
		void SystemStart(void);
		void SystemStop(void);
		void SystemSave(void);
		void SystemInit(void);
		void SystemTerm(void);
		void GetTaskList(CTaskList &List);
		UINT GetCommsStatus(void);
		BOOL GetDataServer(IDataServer * &pData);
		BOOL IsRunningControl(void);

		// Server Access
		virtual IDataServer * GetDataServer(void) const;

		// GDI Access
		IGDI * GetGDI(void) const;

		// Item Properties
		WORD		   m_Build;
		CCommsManager    * m_pComms;
		CTagManager      * m_pTags;
		CEventManager    * m_pEvents;
		CAlarmManager    * m_pAlarms;
		CProgramManager  * m_pPrograms;
		CWebBase         * m_pWeb;
		CDataLogger      * m_pLog;
		CSecurityManager * m_pSecure;
		CLangManager     * m_pLang;
		CSqlManager      * m_pSql;
		IStratonControl  * m_pControl;

		// Item Handles
		UINT m_hComms;
		UINT m_hTags;
		UINT m_hProgs;
		UINT m_hLog;
		UINT m_hSecure;
		UINT m_hLang;
		UINT m_hWeb;
		UINT m_hSql;

		// Instance Pointer
		static CCommsSystem * m_pThis;

	protected:
		// Data Members
		CDataServer * m_pDataServer;
		IDataServer * m_pUsedServer;
		IGDI        * m_pGDI;

		// Implementation
		void DoLoad(PCBYTE &pData);
		BOOL LoadControl(PCBYTE &pData);
		BOOL LoadSql(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCodedHost;
class CCodedItem;
class CCodedText;

//////////////////////////////////////////////////////////////////////////
//
// Coded Host
//

class CCodedHost : public CItem
{
	public:
		// Loading Helpers
		static void GetCoded (PCBYTE &pData, CCodedItem * &pItem);
		static void GetCoded (PCBYTE &pData, CCodedText * &pText);
		static void GetCoded (PCBYTE &pData, CString &Text);
		static void SkipCoded(PCBYTE &pData);
		static BOOL FixCoded (CString &Text, BOOL fRequired);

	protected:
		// Availability
		static BOOL     IsItemAvail(CCodedItem *pItem);
		static BOOL     SetItemScan(CCodedItem *pItem, UINT Code);
		static DWORD	GetItemAddr(CCodedItem *pItem, DWORD  Default);
		static C3REAL   GetItemData(CCodedItem *pItem, C3REAL Default);
		static C3INT    GetItemData(CCodedItem *pItem, C3INT  Default);
		static CUnicode GetItemData(CCodedItem *pItem, PCUTF pDefault);
		static CString  GetItemData(CCodedItem *pItem, PCTXT pDefault);
		static C3REAL   GetItemData(CCodedItem *pItem, C3REAL Default, PDWORD pParam);
		static C3INT    GetItemData(CCodedItem *pItem, C3INT  Default, PDWORD pParam);
		static CUnicode GetItemData(CCodedItem *pItem, PCUTF pDefault, PDWORD pParam);
		static CString  GetItemData(CCodedItem *pItem, PCTXT pDefault, PDWORD pParam);

		// Execution
		static BOOL Execute(CCodedItem *pAction);

		// Data Lifetime
		static BOOL FreeData(DWORD Data, UINT Type);

		// Null Data
		static DWORD GetNull(UINT Type);

		// Encryption
		static BOOL Crypto(PBYTE pData, UINT uSize);
	};

//////////////////////////////////////////////////////////////////////////
//
// Coded Item
//

class CCodedItem : public CItem
{
	public:
		// Constructor
		CCodedItem(void);

		// Destructor
		~CCodedItem(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Server Access
		IDataServer * GetDataServer(void) const;

		// Attributes
		BOOL IsEmpty(void) const;
		BOOL IsWritable(void) const;
		BOOL IsConst(void) const;
		UINT GetRefCount(void) const;
		UINT GetType(void) const;
		UINT GetFlags(void) const;
		UINT GetTagIndex(void) const;

		// Operations
		BOOL SetValue(DWORD Data, UINT Type, UINT Flags);

		// Reference Access
		CDataRef const & GetRef(UINT uRef) const;

		// Execution
		BOOL  IsAvail(void);
		BOOL  SetScan(UINT Code);
		DWORD ExecVal(void);
		DWORD ExecVal(PDWORD pParam);
		DWORD Execute(UINT Type);
		DWORD Execute(UINT Type, PDWORD pParam);
		DWORD GetNull(UINT Type);
		DWORD GetNull(void);

		// Item Properties
		UINT	m_uRefs;
		PDWORD  m_pRefs;
		UINT    m_uCode;
		PBYTE   m_pCode;
	};

//////////////////////////////////////////////////////////////////////////
//
// Coded Text
//

class CCodedText : public CCodedItem
{
	public:
		// Attributes
		CUnicode GetText(void);
		CUnicode GetText(UINT uLang);
		CUnicode GetText(PCUTF pDefault);
		CUnicode GetText(UINT uLang, PCUTF pDefault);
		CUnicode GetText(PCUTF pDefault, PDWORD pParam);
		CUnicode GetText(UINT uLang, PCUTF pDefault, PDWORD pParam);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDataServer;

//////////////////////////////////////////////////////////////////////////
//
// Data Server ABI
//

#if !defined(__INTELLISENSE__) && defined(AEON_PROC_ARM)

#define DS_ABI __attribute__((pcs("aapcs")))

#else

#define DS_ABI

#endif

//////////////////////////////////////////////////////////////////////////
//
// Data Server
//

class CDataServer : public IDataServer
{
	public:
		// Constructor
		CDataServer(CCommsSystem *pSystem);

		// Destructor
		~CDataServer(void);

		// IBase Methods
		UINT Release(void);

		// IDataServer Methods
		WORD  CheckID(WORD  ID);
		BOOL  IsAvail(DWORD ID);
		BOOL  SetScan(DWORD ID, UINT Code);
		DWORD GetData(DWORD ID, UINT Type, UINT Flags);
		BOOL  SetData(DWORD ID, UINT Type, UINT Flags, DWORD Data);
		DWORD GetProp(DWORD ID, WORD Prop, UINT Type);
		DWORD RunFunc(WORD  ID, UINT uParam, PDWORD pParam);
		BOOL  GetName(DWORD ID, UINT m, PSTR pName, UINT uName);

	protected:
		// Function Entry
		struct CFunc
		{
			WORD	m_ID;
			WORD	m_Pad1;
			PVOID	m_pFunc;
			PCSTR   m_pName;
			PVOID	m_Pad2;
			};

		union CReal64 
		{
			C3INT	i[2];
			double	d;
			};

		union CLong64
		{
			C3INT	i[2];
			INT64   l;
			};

		enum
		{ 
			operationAdd,
			operationSub,
			operationMul,
			operationDiv,
			operationMax,
			operationMin,
			operationPow,
			operationAbs,
			operationSqrt,
			operationSin,
			operationCos,
			operationTan,
			operationATan,
			operationATan2,
			operationACos,
			operationASin,
			operationExp,
			operationExp10,
			operationLog,
			operationLog10,
			operationGT,
			operationGTE,
			operationLT,
			operationLTE,
			operationEQ,
			operationNEQ,
			operationMinus,
			operationInc,
			operationDec
			};

		struct CFile
		{
			INT	m_nFile;
			INT	m_nFlags;
			char	m_sName[MAX_PATH];
			};

		// Function Table
		static CFunc const m_FuncTable[];

		// Static Data
		static CDataServer    * m_pThis;
		static IMutex         * m_pLock;
		static IFileUtilities * m_pFileUtils;
		static INetUtilities  * m_pNetUtils;
		static CFile            m_File[16];
		static CAutoDirentList  m_Find;
		static UINT	        m_iFind;
		
		// Data Members
		CCommsSystem    * m_pSystem;
		CCommsManager   * m_pComms;
		CTagList        * m_pTags;
		CProgramManager * m_pProgs;

		// Implementation
		BOOL RunFunc(DWORD &Data, WORD ID, UINT uParam, PDWORD pParam);

		// Function Caller
		static BOOL ScanTable( DWORD       & Data,
				       WORD          ID,
				       UINT          uParam,
				       PDWORD        pParam,
				       CFunc const * pTable,
				       UINT          uCount
				       );

		// Name Location
		static BOOL FindName( WORD	    ID,
				      PSTR	    pName,
				      UINT	    uName, 
				      CFunc const * pTable,
				      UINT          uCount
				      );

		// Core Functions
		static C3INT NOP(void);
		static void  Sleep(C3INT t);
		static void  StopSystem(void);
		static void  CommitAndReset(void);
		static void  CommitAndVoidWarranty(void);
		static void  ClearEvents(void);
		static C3INT GetLastEventTime(C3INT a);
		static PUTF  GetLastEventType(C3INT a);
		static PUTF  GetLastEventText(C3INT a);
		static void  LogSave(void);
		static void  LogHeader(C3INT n, PUTF p);
		static void  LogComment(C3INT n, PUTF p);
		static void  LogBatchHeader(C3INT n, PUTF p);
		static void  LogBatchComment(C3INT n, PUTF p);
		static C3INT IsLoggingActive(void);
		static void  AlarmAcceptAll(void);
		static void  AlarmAcceptTag(UINT Tag, UINT Index, UINT Event);
		static void  AlarmAcceptEx(UINT Source, UINT Method, UINT Code);
		static C3INT GetAlarmTag(C3INT n);
		static C3INT GetVersionInfo(C3INT n);
		static PUTF  GetRestartInfo(C3INT n);
		static PUTF  GetRestartCode(C3INT n);
		static PUTF  GetRestartText(C3INT n);
		static C3INT GetRestartTime(C3INT n);
		static PUTF  GetModelName(C3INT g);
		static C3INT SaveConfigFile(PUTF p);
		static C3INT IsBatteryLow(void);
		static C3INT EnumOptionCard(C3INT  s);
		static void  EnableBatteryCheck(C3INT f);
		static void  SetIconLed(C3INT n, C3INT s);
		static C3INT GetLastTouch(void);
		static PUTF  GetSerialNumber(void);
		static PUTF  GetPersonalityString(PUTF n);
		static C3INT GetPersonalityInt(PUTF n);
		static DWORD GetPersonalityFloat(PUTF n);
		static C3INT GetPersonalityIp(PUTF n);
		static void  SetPersonalityString(PUTF n, PUTF d);
		static void  SetPersonalityInt(PUTF n, C3INT d);
		static void  SetPersonalityFloat(PUTF n, C3REAL d) DS_ABI;
		static void  SetPersonalityIp(PUTF n, C3INT d);
		static void  CommitPersonality(C3INT b);
		static C3INT GetSystemIo(PUTF n);
		static void  SetSystemIo(PUTF n, C3INT v);

		// Data Manipulation
		static void SetInt(DWORD r, C3INT d);
		static void SetReal(DWORD r, C3REAL d) DS_ABI;
		static void ForceInt(DWORD r, C3INT d);
		static void ForceReal(DWORD r, C3REAL d) DS_ABI;
		static void FillInt(DWORD i, DWORD v, UINT n);
		static void FillReal(DWORD i, DWORD v, UINT n);
		static void FillData(DWORD i, DWORD v, UINT n, UINT t);
		static void CopyInt(DWORD d, DWORD s, UINT n);
		static void CopyReal(DWORD d, DWORD s, UINT n);
		static void CopyData(DWORD d, DWORD s, UINT n, UINT t);
		static void ForceCopyInt(DWORD d, DWORD s, UINT n);
		static void ForceCopyReal(DWORD d, DWORD s, UINT n);
		static void ForceCopy(DWORD d, DWORD s, UINT n, UINT t);

		// Math Functions
		static DWORD Ident(DWORD d);
		static C3INT Random(C3INT n);
		static C3INT MulDiv(C3INT a, C3INT b, C3INT c);
		static C3INT Scale(C3INT d, C3INT r1, C3INT r2, C3INT e1, C3INT e2);
		static C3INT MinInt(C3INT a, C3INT b);
		static C3INT MaxInt(C3INT a, C3INT b);
		static C3INT AbsInt(C3INT a);
		static C3INT SgnInt(C3INT a);
		static DWORD SqrtInt(C3INT a);
		static C3INT PowInt(C3INT b, C3INT e);
		static DWORD MinReal(C3REAL a, C3REAL b) DS_ABI;
		static DWORD MaxReal(C3REAL a, C3REAL b) DS_ABI;
		static DWORD AbsReal(C3REAL a) DS_ABI;
		static DWORD SgnReal(C3REAL a) DS_ABI;		
		static DWORD SqrtReal(C3REAL a) DS_ABI;
		static DWORD PowReal(C3REAL b, C3REAL e) DS_ABI;
		static DWORD PI(void);
		static DWORD Deg2Rad(C3REAL a) DS_ABI;
		static DWORD Rad2Deg(C3REAL a) DS_ABI;
		static DWORD Cos(C3REAL a) DS_ABI;
		static DWORD Sin(C3REAL a) DS_ABI;
		static DWORD Tan(C3REAL a) DS_ABI;
		static DWORD ArcCos(C3REAL a) DS_ABI;
		static DWORD ArcSin(C3REAL a) DS_ABI;
		static DWORD ArcTan1(C3REAL a) DS_ABI;
		static DWORD ArcTan2(C3REAL a, C3REAL b) DS_ABI;
		static DWORD Log(C3REAL a) DS_ABI;
		static DWORD Exp(C3REAL a) DS_ABI;
		static DWORD Log10(C3REAL a) DS_ABI;
		static DWORD Exp10(C3REAL a) DS_ABI;
		static DWORD MulU32(UINT a, UINT b);

		// 64-bit Floating Point
		static void  CReal64BinaryOp(C3INT res, C3INT a, C3INT b, int operation);
		static void  CReal64UnaryOp(C3INT res, C3INT a, int operation);
		static BOOL  CReal64Compare(C3INT a, C3INT b, int operation);
		static void  MulR64(C3INT res, C3INT a, C3INT b);
		static void  AddR64(C3INT res, C3INT a, C3INT b);
		static void  SubR64(C3INT res, C3INT a, C3INT b);
		static void  DivR64(C3INT res, C3INT a, C3INT b);
		static void  PowR64(C3INT res, C3INT a, C3INT b);
		static void  MaxR64(C3INT res, C3INT a, C3INT b);
		static void  MinR64(C3INT res, C3INT a, C3INT b);
		static void  AbsR64(C3INT res, C3INT a);
		static void  SqrtR64(C3INT res, C3INT a);
		static void  SinR64(C3INT res, C3INT a);
		static void  CosR64(C3INT res, C3INT a);
		static void  TanR64(C3INT res, C3INT a);
		static void  ASinR64(C3INT res, C3INT a);
		static void  ACosR64(C3INT res, C3INT a);
		static void  ATanR64(C3INT res, C3INT a);
		static void  ATan2R64(C3INT res, C3INT a, C3INT b);
		static void  ExpR64(C3INT res, C3INT a);
		static void  Exp10R64(C3INT res, C3INT a);
		static void  LogR64(C3INT res, C3INT a);
		static void  Log10R64(C3INT res, C3INT a);
		static DWORD GreaterR64(C3INT a, C3INT b);
		static DWORD LessR64(C3INT a, C3INT b);
		static DWORD GreaterEqR64(C3INT a, C3INT b);
		static DWORD LessEqR64(C3INT a, C3INT b);
		static DWORD EqR64(C3INT a, C3INT b);
		static DWORD NEqR64(C3INT a, C3INT b);
		static DWORD AddU32(UINT a, UINT b);
		static DWORD SubU32(UINT a, UINT b);
		static DWORD DivU32(UINT a, UINT b);
		static DWORD CompU32(UINT a, UINT b);
		static DWORD MaxU32(UINT a, UINT b);
		static DWORD MinU32(UINT a, UINT b);
		static DWORD RShU32(UINT a, UINT b);
		static DWORD ModU32(UINT a, UINT b);
		static void  UnaryMinusR64(UINT res, UINT a);
		static void  IncR64(UINT res, UINT a);
		static void  DecR64(UINT res, UINT a);

		// Statistical Functions
		static C3INT SumInt(DWORD i, UINT n);
		static DWORD MeanInt(DWORD i, UINT n);
		static DWORD StdDevInt(DWORD i, UINT n);
		static DWORD PopDevInt(DWORD i, UINT n);
		static DWORD DevInt(DWORD i, UINT n, UINT m);
		static DWORD SumReal(DWORD i, UINT n);
		static DWORD MeanReal(DWORD i, UINT n);
		static DWORD StdDevReal(DWORD i, UINT n);
		static DWORD PopDevReal(DWORD i, UINT n);
		static DWORD DevReal(DWORD i, UINT n, UINT m);

		// String Functions
		static C3INT Len(PUTF p);
		static PUTF  Left(PUTF p, UINT n);
		static PUTF  Right(PUTF p, UINT n);
		static PUTF  Mid(PUTF p, UINT s, UINT n);
		static C3INT Find(PUTF p, UINT c, UINT s);
		static PUTF  Strip(PUTF p, UINT t);

		// Data Conversion
		static C3INT TextToInt(PUTF p, UINT r);
		static DWORD TextToReal(PUTF p);
		static void  TextToR64(PUTF p, C3INT r);
		static void  TextToL64(PUTF p, C3INT l, C3INT r);
		static DWORD TextToAddr(PUTF p);
		static void  TextToData(PUTF p, DWORD r, UINT n);
		static PUTF  IntToText(C3INT d, UINT r, UINT c);
		static PUTF  RealToText(C3REAL d, UINT n) DS_ABI;
		static PUTF  AddrToText(DWORD d);
		static PUTF  DataToText(DWORD r, UINT n);
		static PUTF  AsTextInt(C3INT d);
		static PUTF  AsTextReal(C3REAL d) DS_ABI;
		static PUTF  AsTextR64(C3INT d);
		static PUTF  AsTextL64(C3INT l, C3INT r, C3INT c);
		static PUTF  AsTextR64UserWidth(PUTF width, C3INT d);
		static PUTF  AsTextR64WithFormat(PUTF format, C3INT d);
		static PUTF  DecToTextInt(C3INT d, C3INT s, C3INT b, C3INT a, C3INT l, C3INT g);
		static PUTF  DecToTextReal(C3REAL d, C3INT s, C3INT b, C3INT a, C3INT l, C3INT g) DS_ABI;
		static DWORD R64ToReal(C3INT a);
		static C3INT R64ToInt(C3INT a);
		static void  RealToR64(C3INT res, C3REAL a) DS_ABI;
		static void  IntToR64(C3INT res, C3INT a);

		// Time and Date
		static C3INT Date(C3INT y, C3INT m, C3INT d);
		static C3INT Time(C3INT h, C3INT m, C3INT s);
		static C3INT GetNowTime(void);
		static C3INT GetNowDate(void);
		static C3INT SetNow(C3INT t);

		// Disk Management
		static C3INT DriveStatus(void);
		static void  DriveEject(void);
		static C3INT DriveFormat(void);
		static C3INT DriveStatusEx(UINT n);
		static void  DriveEjectEx(UINT n);
		static C3INT DriveFormatEx(UINT n);
		static C3INT GetDiskFreeBytes(C3INT d);
		static C3INT GetDiskFreePercent(C3INT d);
		static C3INT GetDiskSizeBytes(C3INT d);
		static void  MountCompactFlash(C3INT m);

		// File Handling
		static PUTF  FindFileFirst(PUTF n);
		static PUTF  FindFileNext(void);
		static C3INT CreateDirectory(PUTF n);
		static C3INT DeleteDirectory(PUTF n);
		static C3INT KillDirectory(PUTF n);
		static C3INT CopyFiles(PUTF s, PUTF d, UINT n);
		static C3INT MoveFiles(PUTF s, PUTF d, UINT n);
		static PUTF  GetAutoCopyStatusText(void);
		static UINT  GetAutoCopyStatusCode(void);
		static C3INT CreateFile(PUTF n);
		static C3INT RenameFile(C3INT f, PUTF n);
		static C3INT DeleteFile(C3INT f);
		static C3INT OpenFile(PUTF n, C3INT m);
		static void  CloseFile(C3INT f);
		static C3INT FileSeek(C3INT f, C3INT n);
		static C3INT FileTell(C3INT f);
		static PUTF  FileReadLine(C3INT f);
		static PUTF  FileRead(C3INT f, C3INT c);
		static C3INT GetFileByte(C3INT f);
		static C3INT GetFileData(C3INT f, DWORD v, C3INT n);
		static C3INT FileWrite(C3INT f, PUTF n);
		static C3INT FileWriteLine(C3INT f, PUTF n);
		static C3INT FileWriteHelp(C3INT f, PUTF n, BOOL fCRLF);
		static C3INT PutFileByte(C3INT f, C3INT d);
		static C3INT PutFileData(C3INT f, DWORD v, C3INT n);
		static void  AdjustName(PTXT d, PUTF s);
		static void  RemoteName(PTXT d, PUTF s);
		static C3INT IsBatchNameValid(PUTF n);
		static C3INT IsBatchNameValidEx(C3INT s, PUTF n);

		// Mail Support
		static void SendMail(UINT r, PUTF s, PUTF b);
		static void SendFile(UINT r, PUTF f);
		static void SendFileEx(UINT r, PUTF f, PUTF s, DWORD g);
		static void SendMailTo(PUTF r, PUTF s, PUTF b);
		static void SendFileTo(PUTF r, PUTF f);
		static void SendMailToAck(PUTF r, PUTF s, PUTF b, DWORD d);
		static void SendFileToAck(PUTF r, PUTF f, DWORD d);

		// FTP Client
		static C3INT FtpPutFile(UINT s, PUTF loc, PUTF rem, C3INT del);
		static C3INT FtpGetFile(UINT s, PUTF loc, PUTF rem, C3INT del);

		// Batch Control
		static void NewBatch(PUTF p);
		static void EndBatch(void);
		static PUTF GetBatch(void);
		static void NewBatchEx(UINT s, PUTF p);
		static void EndBatchEx(UINT s);
		static PUTF GetBatchEx(UINT s);

		// Network Config
		static PUTF  GetNetId(C3INT Port);
		static PUTF  GetNetIp(C3INT Port);
		static PUTF  GetNetMask(C3INT Port);
		static PUTF  GetNetGate(C3INT Port);
		static PUTF  GetInterfaceStatus(C3INT Face);
		static INT   ResolveDNS(PUTF p);
		static C3INT NetworkPing(C3INT IP, C3INT Timeout);
		
		// Network Properties
		static PUTF  GetModemProperty(C3INT n, PUTF p);
		static PUTF  GetWifiProperty(C3INT n, PUTF p);
		static DWORD GetLocationProperty(C3INT n, PUTF p);

		// Tag Access
		static C3INT GetIntTag(C3INT n);
		static DWORD GetRealTag(C3INT n);
		static PUTF  GetStringTag(C3INT n);
		static PUTF  GetFormattedTag(C3INT n);
		static PUTF  GetTagLabel(C3INT n);
		static C3INT FindTagIndex(PUTF p);
		static void  SetIntTag(C3INT n, C3INT d);
		static void  SetRealTag(C3INT n, C3REAL d) DS_ABI;
		static void  SetStringTag(C3INT n, PUTF p);
		
		// Comms
		static void  ReadData(DWORD r, C3INT n);
		static C3INT WaitData(DWORD r, C3INT n, C3INT t);
		static void  WriteAll(void);
	
		// Comms Ports
		static C3INT GetPortConfig(C3INT Port, C3INT Param);
		static C3INT IsPortRemote(C3INT Port);
		static C3INT DrvCtrl(C3INT Port, C3INT f, PUTF v);

		// Comms Devices
		static void  EnableDevice(C3INT Dev);
		static void  DisableDevice(C3INT Dev);
		static void  ControlDevice(C3INT Dev, C3INT State);
		static C3INT IsDeviceOnline(C3INT Dev);
		static C3INT GetDeviceStatus(C3INT Dev);
		static C3INT IsWriteQueueEmpty(C3INT Dev);
		static C3INT EmptyWriteQueue(C3INT Dev);
		static C3INT DevCtrl(C3INT Dev, C3INT f, PUTF v);

		// Raw Ports
		static C3INT RawWrite(UINT n, BYTE b);
		static C3INT RawPrint(UINT n, PUTF t);
		static C3INT RawPrintEx(UINT n, PUTF t);		
		static C3INT RawSendData(UINT Port, UINT DataStart, UINT Count);
		static C3INT RawRead(UINT n, UINT t);
		static PUTF  RawInput(UINT n, UINT s, UINT e, UINT t, UINT c);
		static void  RawClose(UINT n);
		static void  RawSetRTS(UINT n, UINT s);
		static C3INT RawGetCTS(UINT n);

		// CAN Raw Port
		static C3INT InitRxCAN(UINT n, C3INT id, C3INT dlc);
		static C3INT InitTxCAN(UINT n, C3INT id, C3INT dlc);
		static C3INT RxCAN(UINT n, DWORD r, C3INT id);
		static C3INT TxCAN(UINT n, DWORD r, C3INT id);
		static C3INT InitRxCANMailBox(UINT n, C3INT mb, C3INT mask, C3INT filter, C3INT dlc);
		static C3INT InitTxCANMailBox(UINT n, C3INT mb, C3INT id, C3INT dlc);
		static PUTF  RxCANMail(UINT n);
		static C3INT TxCANMail(UINT n, C3INT mb, DWORD r);

		// Web Server
		static C3INT GetWebParamInt(PUTF n);
		static C3INT GetWebParamHex(PUTF n);
		static PUTF  GetWebParamStr(PUTF n);
		static PUTF  GetWebUser(C3INT n);
		static void  ClearWebUsers(void);

		// License
		static PUTF GetLicenseState(C3INT n);

		// SQL Sync
		static C3INT ForceSQLSync(void);
		static C3INT IsSQLSyncRunning(void);
		static C3INT GetLastSQLSyncStatus(void);
		static C3INT GetLastSQLSyncTime(C3INT Type);

		// SQL
		static void  RunQuery(PUTF Query);
		static void  RunAllQueries(void);
		static C3INT GetSQLConnectionStatus(void);		
		static C3INT GetQueryTime(PUTF Query);
		static C3INT GetQueryStatus(PUTF Query);

		// Camera Support
		static C3INT GetCameraData(C3INT Port, C3INT Camera, C3INT Param);
		static C3INT SaveCameraSetup(C3INT Port, C3INT Camera, C3INT Index, PUTF File);
		static C3INT LoadCameraSetup(C3INT Port, C3INT Camera, C3INT Index, PUTF File);
		static C3INT UseCameraSetup(C3INT Port, C3INT Camera, C3INT Index);

		// Honeywell Specific
		static void HonFindProf(int TimeUnits, int RampType, DWORD ptrRamp, DWORD ptrTime, DWORD ptrData, DWORD ptrAuxD, DWORD ptrXPos, DWORD ptrYPos, DWORD ptrAPos, DWORD ptrBPos, DWORD ptrCountX, DWORD ptrCountA, DWORD ptrLimit);
		static int  HonFindTime(int TimeUnits, int RampType, DWORD ptrRamp, DWORD ptrTime, DWORD ptrData, C3REAL Segment, C3REAL Remain) DS_ABI;
		static PUTF HonGetLogParamS(int n, int i);
		static int  HonGetLogParamI(int n, int i);
		static void HonSetLogParamI(int n, int i, int v);
		static int  HonGetLogParamA(void);

		// Debugging
		static void DebugPrint(PUTF pText);
		static void DebugStackTrace(void);
		static void DebugDumpLocals(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDispFormat;
class CDispFormatLinked;
class CDispFormatFlag;
class CDispFormatNumber;
class CDispFormatSci;
class CDispFormatIPAddr;
class CDispFormatMulti;
class CDispFormatMultiList;
class CDispFormatMultiEntry;
class CDispFormatTimeDate;
class CDispFormatString;

//////////////////////////////////////////////////////////////////////////
//
// Editing Context
//

struct CEditCtx
{
	CCodedItem  * m_pValue;
	CCodedItem  * m_pValid;
	CDispFormat * m_pFormat;
	UINT	      m_uFlags;
	UINT          m_Type;
	DWORD         m_Data;
	DWORD	      m_Min;
	DWORD	      m_Max;
	CUnicode      m_Edit;
	CUnicode      m_Foot;
	UINT	      m_uKeypad;
	UINT	      m_uCursor;
	UINT	      m_uField;
	BOOL	      m_fDefault;
	BOOL	      m_fError;
	BOOL	      m_fHide;
	UINT          m_uAccel;
	DWORD         m_Step;
	UINT	      m_uCount;
	};

//////////////////////////////////////////////////////////////////////////
//
// Editing Flags
//

enum 
{
	flagTwoFlag	= 0x0001,
	flagTwoMulti	= 0x0002,
	flagTwoNumeric	= 0x0004,
	flagShowRamp	= 0x0100,
	flagRampOnly	= 0x0200,
	};

//////////////////////////////////////////////////////////////////////////
//
// Editing Actions
//

enum
{
	editNone   = 0,
	editUpdate = 1,
	editReload = 2,
	editAbort  = 3,
	editCommit = 4,
	editError  = 5,
	};

//////////////////////////////////////////////////////////////////////////
//
// Cursor Modes
//

enum
{
	cursorLast  = 0x8000,
	cursorAll   = 0x8001,
	cursorError = 0x8002,
	cursorNone  = 0x8003,
	};

//////////////////////////////////////////////////////////////////////////
//
// Format Flags
//

enum
{
	fmtStd	 = 0x00,
	fmtPad	 = 0x01,
	fmtBare  = 0x02,
	fmtUnits = 0x04,
	fmtTime  = 0x08,
	fmtDate  = 0x10,
	fmtANSI	 = 0x20,
	fmtShow  = 0x40,
	};

//////////////////////////////////////////////////////////////////////////
//
// Display Format
//

class CDispFormat : public CCodedHost
{
	public:
		// Class Creation
		static CDispFormat * MakeObject(UINT uType);

		// General Formatting
		static CUnicode GeneralFormat(DWORD Data, UINT Type, UINT Flags);

		// General Parsing
		static BOOL GeneralParse(DWORD &Data, CString Text, UINT Type);

		// General Editing
		static UINT GeneralEdit(CEditCtx *pEdit, UINT uCode);

		// Constructor
		CDispFormat(void);

		// Destructor
		~CDispFormat(void);

		// Type Checking
		virtual BOOL IsNumber(void);
		virtual BOOL IsMulti(void);

		// Scan Control
		virtual BOOL IsAvail(void);
		virtual BOOL SetScan(UINT Code);

		// Formatting
		virtual CUnicode Format(DWORD Data, UINT Type, UINT Flags);

		// Parsing
		virtual BOOL Parse(DWORD &Data, CString Text, UINT Type);

		// Presentation
		virtual CString GetStates(void);

		// Editing
		virtual UINT Edit(CEditCtx *pEdit, UINT uCode);

		// Limit Access
		virtual BOOL  NeedsLimits(void);
		virtual DWORD GetMin(UINT Type);
		virtual DWORD GetMax(UINT Type);

		// Stepping
		virtual DWORD GetStep(UINT Type);
		virtual BOOL  SpeedUp(UINT uCount);
		virtual BOOL  SpeedUp(DWORD Data, UINT Type, DWORD &Step);

	protected:
		// Editing Implementation
		static UINT GeneralEditNumeric(CEditCtx *pEdit, UINT uCode);
		static UINT GeneralEditInteger(CEditCtx *pEdit, UINT uCode);
		static UINT GeneralEditReal   (CEditCtx *pEdit, UINT uCode);
		static UINT GeneralEditString (CEditCtx *pEdit, UINT uCode);

		// Implementation
		static void MakeDigitsFixed(CUnicode &Text);
		static void MakeLettersFixed(CUnicode &Text);
		static void MakeDigitsFixed(PUTF pText);
		static void MakeLettersFixed(PUTF pText);
		static BOOL Validate(CEditCtx *pEdit, DWORD Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Linked Display Format
//

class CDispFormatLinked : public CDispFormat
{
	public:
		// Constructor
		CDispFormatLinked(void);

		// Destructor
		~CDispFormatLinked(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Formatting
		CUnicode Format(DWORD Data, UINT Type, UINT Flags);

		// Parsing
		BOOL Parse(DWORD &Data, CString Text, UINT Type);

		// Presentation
		CString GetStates(void);

		// Editing
		UINT Edit(CEditCtx *pEdit, UINT uCode);

		// Limit Access
		BOOL  NeedsLimits(void);
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Stepping
		DWORD GetStep(UINT Type);
		BOOL  SpeedUp(UINT uCount);
		BOOL  SpeedUp(DWORD Data, UINT Type, DWORD &Step);

		// Item Properties
		UINT m_Tag;

	protected:
		// Data Members
		CDispFormat * m_pFormat;
		CTag        * m_pTag;

		// Implementation
		BOOL FindFormat(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Flag Display Format
//

class CDispFormatFlag : public CDispFormat
{
	public:
		// Constructor
		CDispFormatFlag(void);
		
		// Destructor
		~CDispFormatFlag(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Formatting
		CUnicode Format(DWORD Data, UINT Type, UINT Flags);

		// Parsing
		BOOL Parse(DWORD &Data, CString Text, UINT Type);

		// Presentation
		CString GetStates(void);

		// Editing
		UINT Edit(CEditCtx *pEdit, UINT uCode);

		// Limit Access
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Item Properties
		CCodedText * m_pOff;
		CCodedText * m_pOn;
	};

//////////////////////////////////////////////////////////////////////////
//
// Number Display Format
//

class CDispFormatNumber : public CDispFormat
{
	public:
		// Constructor
		CDispFormatNumber(void);

		// Destructor
		~CDispFormatNumber(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Type Checking
		BOOL IsNumber(void);

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Formatting
		CUnicode Format(DWORD Data, UINT Type, UINT Flags);

		// Parsing
		BOOL Parse(DWORD &Data, CString Text, UINT Type);

		// Editing
		UINT Edit(CEditCtx *pEdit, UINT uCode);

		// Limit Access
		BOOL  NeedsLimits(void);
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Stepping
		DWORD GetStep(UINT Type);
		BOOL  SpeedUp(UINT uCount);
		BOOL  SpeedUp(DWORD Data, UINT Type, DWORD &Step);

		// Item Properties
		UINT	     m_Radix;
		UINT	     m_Before;
		UINT	     m_After;
		UINT	     m_Leading;
		UINT	     m_Group;
		UINT	     m_Signed;
		CCodedText * m_pPrefix;
		CCodedText * m_pUnits;
		CCodedItem * m_pDynDP;

	protected:
		// Implementation
		UINT  GetLimit(void);
		UINT  GetRadix(void);
		INT64 Power(UINT n);
		BOOL  IsGroupBoundary(UINT n);
		WCHAR GetGroupChar(void);
		WCHAR GetPointChar(void);
		UINT  GetAfter(void);

		// Edit Handlers
		UINT OnEditBegin (CEditCtx *pEdit, UINT uCode);
		UINT OnEditRaise (CEditCtx *pEdit);
		UINT OnEditLower (CEditCtx *pEdit);
		UINT OnEditRamp  (CEditCtx *pEdit, BOOL fRaise);
		UINT OnEditEnter (CEditCtx *pEdit);
		UINT OnEditClose (CEditCtx *pEdit);
		UINT OnEditDelete(CEditCtx *pEdit);
		UINT OnEditChar  (CEditCtx *pEdit, UINT uCode);

		double Round(double rData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Scientific Display Format
//

class CDispFormatSci : public CDispFormat
{
	public:
		// Constructor
		CDispFormatSci(void);

		// Destructor
		~CDispFormatSci(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Formatting
		CUnicode Format(DWORD Data, UINT Type, UINT Flags);

		// Editing
		UINT Edit(CEditCtx *pEdit, UINT uCode);

		// Limit Access
		BOOL  NeedsLimits(void);
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Item Properties
		UINT         m_After;
		UINT         m_ManSign;
		UINT         m_ExpSign;
		CCodedText * m_pPrefix;
		CCodedText * m_pUnits;
		CCodedItem * m_pDynDP;

	protected:
		// Implementation
		WCHAR GetPointChar(void);
		UINT  GetAfter(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// IP Address Display Format
//

class CDispFormatIPAddr : public CDispFormat
{
	public:
		// Constructor
		CDispFormatIPAddr(void);

		// Destructor
		~CDispFormatIPAddr(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Formatting
		CUnicode Format(DWORD Data, UINT Type, UINT Flags);

		// Parsing
		BOOL Parse(DWORD &Data, CString Text, UINT Type);

		// Editing
		UINT Edit(CEditCtx *pEdit, UINT uCode);

		// Limit Access
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Display Format
//

class CDispFormatMulti : public CDispFormat
{
	public:
		// Constructor
		CDispFormatMulti(void);

		// Destructor
		~CDispFormatMulti(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Formatting
		CUnicode Format(DWORD Data, UINT Type, UINT Flags);

		// Parsing
		BOOL Parse(DWORD &Data, CString Text, UINT Type);

		// Type Checking
		BOOL IsMulti(void);

		// Presentation
		CString GetStates(void);

		// Editing
		UINT Edit(CEditCtx *pEdit, UINT uCode);

		// Limit Access
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Item Properties
		CCodedText           * m_pDefault;
		CCodedItem           * m_pLimit;
		UINT                   m_Range;
		CDispFormatMultiList * m_pList;

	protected:
		// Implementation
		UINT GetCount(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi-State List
//

class CDispFormatMultiList : public CItem
{
	public:
		// Constructor
		CDispFormatMultiList(void);

		// Destructor
		~CDispFormatMultiList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Item Access
		CDispFormatMultiEntry * GetItem(UINT uPos) const;

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Attributes
		UINT GetItemCount(void) const;
		
		// Navigation
		UINT FindData(DWORD Data);
		UINT FindNext(UINT uSlot, UINT uCount);
		UINT FindPrev(UINT uSlot, UINT uCount);

	protected:
		// Data Members
		UINT			m_uCount;
		CDispFormatMultiEntry * m_pEntry;
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Entry
//

class CDispFormatMultiEntry : public CCodedHost
{
	public:
		// Constructor
		CDispFormatMultiEntry(void);

		// Destructor
		~CDispFormatMultiEntry(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Data Matching
		BOOL MatchData(DWORD Data, UINT Type, BOOL fRange, CUnicode &Text);

		// Data Testing
		int CompareData(DWORD Data, UINT uType);

		// Item Properties
		CCodedItem * m_pData;
		CCodedText * m_pText;
	};

//////////////////////////////////////////////////////////////////////////
//
// Time and Date Display Format
//

class CDispFormatTimeDate : public CDispFormat
{
	public:
		// Constructor
		CDispFormatTimeDate(void);

		// Destructor
		~CDispFormatTimeDate(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Formatting
		CUnicode Format(DWORD Data, UINT Type, UINT Flags);
		
		// Parsing
		BOOL Parse(DWORD &Data, CString Text, UINT Type);

		// Editing
		UINT Edit(CEditCtx *pEdit, UINT uCode);

		// Limit Access
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Item Properties
		UINT	     m_Mode;
		UINT	     m_Secs;
		UINT	     m_TimeForm;
		UINT	     m_TimeSep;
		CCodedText * m_pAM;
		CCodedText * m_pPM;
		UINT	     m_DateForm;
		UINT         m_DateSep;
		UINT	     m_Year;
		UINT	     m_Month;

	protected:
		// Data Members
		UINT  m_uCount;
		UINT  m_uFields;
		UINT  m_uTotal;
		UINT  m_uLang;
		DWORD m_Format[20];

		// Implementation
		void BuildFormat(void);
		void BuildTime(void);
		void BuildDate(void);
		void BuildDateLocale(void);
		void AddDate(void);
		void AddMonth(void);
		void AddYear(void);
		void AddDateSep(void);
		void AddForm(UINT uCode, UINT uLen);

		// Localization
		BOOL     CheckLanguage(void);
		UINT     GetLocaleTime(void);
		UINT     GetLocaleDate(void);
		CUnicode GetMonthName(UINT uMonth);
		CUnicode GetAMPM(UINT uHour);

		//Parse Helpers
		void LoadDate(CTime &Time, CString &EditString);
		void LoadTime(CTime &Time, CString &EditString);

		// Edit Support
		UINT  FindCursor(UINT uField);
		void  FirstField(CEditCtx *pEdit);
		void  LastField(CEditCtx *pEdit);
		UINT  SpinUp(CEditCtx *pEdit);
		UINT  SpinDown(CEditCtx *pEdit);
		void  Translate(CTime &Time, C3INT Data);
		C3INT Translate(CTime const &Time);
		void  CheckMonth(CTime &Time);
	};

//////////////////////////////////////////////////////////////////////////
//
// String Display Format
//

class CDispFormatString : public CDispFormat
{
	public:
		// Constructor
		CDispFormatString(void);

		// Destructor
		~CDispFormatString(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		void SetLength(UINT Length);

		// Formatting
		CUnicode Format(DWORD Data, UINT Type, UINT Flags);
		
		// Parsing
		BOOL Parse(DWORD &Data, CString Text, UINT Type);

		// Editing
		UINT Edit(CEditCtx *pEdit, UINT uCode);

	protected:
		// Data Members
		UINT     m_Length;
		CUnicode m_Template;

		// Implementation
		BOOL  IsFormat(WCHAR cData);
		PCUTF GetList(WCHAR cData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDispColor;
class CDispColorLinked;
class CDispColorFixed;
class CDispColorFlag;
class CDispColorMulti;
class CDispColorMultiList;
class CDispColorMultiEntry;

//////////////////////////////////////////////////////////////////////////
//
// Display Color
//

class CDispColor : public CCodedHost
{
	public:
		// Class Enumeration
		static CDispColor * MakeObject(UINT uType);

		// Constructor
		CDispColor(void);

		// Destructor
		~CDispColor(void);

		// Scan Control
		virtual BOOL IsAvail(void);
		virtual BOOL SetScan(UINT Code);

		// Color Access
		virtual DWORD GetColorPair(DWORD Data, UINT uType);
		virtual COLOR GetForeColor(DWORD Data, UINT uType);
		virtual COLOR GetBackColor(DWORD Data, UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Linked Display Format
//

class CDispColorLinked : public CDispColor
{
	public:
		// Constructor
		CDispColorLinked(void);

		// Destructor
		~CDispColorLinked(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Color Access
		DWORD GetColorPair(DWORD Data, UINT uType);

		// Item Properties
		UINT m_Tag;

	protected:
		// Data Members
		CDispColor * m_pColor;

		// Implementation
		BOOL FindColor(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Fixed Display Color
//

class CDispColorFixed : public CDispColor
{
	public:
		// Constructor
		CDispColorFixed(void);

		// Destructor
		~CDispColorFixed(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Color Access
		DWORD GetColorPair(DWORD Data, UINT uType);

		// Item Properties
		UINT m_Color;
	};

//////////////////////////////////////////////////////////////////////////
//
// Flag Display Color
//

class CDispColorFlag : public CDispColor
{
	public:
		// Constructor
		CDispColorFlag(void);

		// Destructor
		~CDispColorFlag(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Color Access
		DWORD GetColorPair(DWORD Data, UINT Type);

		// Item Properties
		UINT m_On;
		UINT m_Off;
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Display Color
//

class CDispColorMulti : public CDispColor
{
	public:
		// Constructor
		CDispColorMulti(void);

		// Destructor
		~CDispColorMulti(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Color Access
		DWORD GetColorPair(DWORD Data, UINT Type);

		// Item Properties
		UINT		      m_Default;
		UINT                  m_Range;
		CDispColorMultiList * m_pList;
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Color List
//

class CDispColorMultiList : public CItem
{
	public:
		// Constructor
		CDispColorMultiList(void);
		
		// Destructor
		~CDispColorMultiList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Item Access
		CDispColorMultiEntry * GetItem(UINT uPos) const;

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Attributes
		UINT GetItemCount(void) const;

	protected:
		// Data Members
		UINT		       m_uCount;
		CDispColorMultiEntry * m_pEntry;
	};

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Color Entry
//

class CDispColorMultiEntry : public CCodedHost
{
	public:
		// Constructor
		CDispColorMultiEntry(void);

		// Destructor
		~CDispColorMultiEntry(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Scan Control
		BOOL IsAvail(void);
		BOOL SetScan(UINT Code);

		// Data Matching
		BOOL MatchData(DWORD Data, UINT Type, BOOL fRange, DWORD &Color);

		// Item Properties
		CCodedItem * m_pData;
		UINT         m_Color;
	};

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSysBlock;
class CSecDesc;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTagManager;
class CTagList;
class CTag;
class CTagFolder;
class CTag;
class CTagSimple;
class CTagNumeric;
class CTagFlag;
class CTagString;
class CTagPollable;
class CTagEvent;
class CTagEventNumeric;
class CTagEventFlag;
class CTagTrigger;
class CTagTriggerNumeric;
class CTagTriggerFlag;
class CTagQuickPlot;

//////////////////////////////////////////////////////////////////////////
//
// Standard Properties for Tags
//

enum
{
	tpAsText	= 1,
	tpLabel		= 2,
	tpDescription	= 3,
	tpPrefix	= 4,
	tpUnits		= 5,
	tpSetPoint	= 6,
	tpMinimum	= 7,
	tpMaximum	= 8,
	tpForeColor	= 9,
	tpBackColor	= 10,
	tpName		= 11,
	tpIndex		= 12,
	tpAlarms	= 13,
	tpTextOff	= 14,
	tpTextOn	= 15,
	tpStateCount	= 16,
	tpDeadband	= 17,
	tpStateText	= 1000,
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag Manager
//

class CTagManager : public CItem,
		    public ITaskEntry,
		    public IEventSource,
		    public IDiagProvider
{
	public:
		// Constructor
		CTagManager(void);

		// Destructor
		~CTagManager(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Task List
		void GetTaskList(CTaskList &List);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Operations
		void Commit(void);
		void CommitAndReset(void);
		void LockQuickPlots(void);
		void FreeQuickPlots(void);

		// IEventSource
		BOOL GetEventText(CUnicode &Text, UINT Code);
		void AcceptAlarm (UINT Method, UINT Code);
		void AcceptAlarm (CActiveAlarm *pInfo);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Item Properties
		UINT	   m_LogToDisk;
		UINT	   m_FileLimit;
		UINT	   m_FileCount;
		UINT	   m_WithBatch;
		UINT       m_SignLogs;
		UINT	   m_LogToPort;
		UINT	   m_Drive;
		CTagList * m_pTags;

	protected:
		// Data Members
		ULONG m_uRefs;

		// Quick List
		CArray <CTagQuickPlot *> m_QuickList;

		// Persist Data
		IMutex * m_pQuick;
		UINT     m_uCommit;

		// Diagnostic Data
		IDiagManager * m_pDiag;
		UINT           m_uProv;

		// Quick Plot Task
		void QuickPlotInit(void);
		void QuickPlotExec(void);
		void QuickPlotTerm(void);

		// Scanner Task
		void ScannerInit(void);
		void ScannerExec(void);
		void ScannerTerm(void);

		// Persist Task
		void PersistInit(void);
		void PersistExec(void);
		void PersistTerm(void);

		// Diagnostics
		BOOL DiagRegister(void);
		BOOL DiagRevoke(void);
		UINT DiagList(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagGet(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagSet(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Implementation
		UINT MakeQuickList(void);
		void RunAction(CCodedItem *pCode, UINT uWait);
		BOOL FindTag(CTag * &pTag, CDataRef &Ref, CString Name);
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag List
//

class CTagList : public CItem
{
	public:
		// Constructor
		CTagList(void);

		// Destructor
		~CTagList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Item Access
		CTag * GetItem(UINT uPos) const;

		// Attributes
		UINT GetCount(void) const;

		// Operations
		void Purge(void);
		void Force(void);
		void SetPollScan(UINT uCode);
		void Poll(UINT uDelta);
		UINT FindByName(PCUTF pName);

	protected:
		// Type Definitions
		typedef CMap <CUnicode, UINT> CTagMap;
		
		// Data Members
		UINT    m_uCount;
		UINT    m_uPoll;
		CTag ** m_ppTag;
		CTag ** m_ppPoll;
		CTagMap m_Map;
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag Base Class
//

class CTag : public CCodedHost
{
	public:
		// Class Creation
		static CTag * MakeObject(UINT uType);

		// Property Helpers
		static UINT  GetPropCount(void);
		static DWORD GetDefProp(WORD ID, UINT Type);
		static PCTXT GetPropName(WORD ID);
		static UINT  GetPropType(WORD ID, UINT Type);

		// Constructor
		CTag(void);

		// Destructor
		~CTag(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		CAddress GetAddress(void) const;

		// Limit Access
		DWORD GetMinValue(UINT uPos, UINT Type);
		DWORD GetMaxValue(UINT uPos, UINT Type);

		// Service Access
		DWORD    GetColorPair(UINT uPos);
		CUnicode GetLabel(UINT uPos);
		CUnicode GetLabel(UINT uPos, BOOL fLoc);
		CUnicode GetAlias(UINT uPos);
		CUnicode GetAsText(UINT uPos, DWORD Data, UINT Type, UINT Flags);
		CUnicode GetAsText(UINT uPos, UINT Flags);
		BOOL     SetAsText(UINT uPos, CString const &Text);
		CString  GetStates(void);

		// Attributes
		virtual UINT GetDataType(void) const;
		virtual BOOL IsArray(void) const;
		virtual UINT GetExtent(void) const;
		virtual BOOL CanWrite(void) const;

		// Operations
		virtual void Purge(void);
		virtual void Force(void);
		virtual void SetIndex(UINT uIndex);
		virtual BOOL SetPollScan(UINT uScan);
		virtual void Poll(UINT uDelta);

		// Text Access
		virtual BOOL GetEventText(CUnicode &Text, UINT uItem, UINT uPos);

		// Quick Plot
		virtual BOOL GetQuickPlot(CTagQuickPlot * &pQuick);
		
		// Evaluation
		virtual BOOL  IsAvail(CDataRef const &Ref);
		virtual BOOL  IsAvail(CDataRef const &Ref, UINT Flags);
		virtual BOOL  SetScan(CDataRef const &Ref, UINT Code);
		virtual DWORD GetData(CDataRef const &Ref, UINT Type, UINT Flags);
		virtual BOOL  IsValid(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		virtual BOOL  SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		virtual DWORD GetProp(CDataRef const &Ref, WORD ID, UINT Type);

		// Deadband
		virtual void InitPrevious(DWORD &Prev);
		virtual BOOL HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev);
		virtual void KillPrevious(DWORD &Prev);

		// Data Members
		UINT	      m_uTag;
		CUnicode      m_Name;
		CUnicode      m_Desc;
		DWORD         m_Ref;
		CCodedItem  * m_pValue;
		CCodedItem  * m_pSim;
		CCodedText  * m_pLabel;
		CCodedText  * m_pAlias;
		CDispFormat * m_pFormat;
		CDispColor  * m_pColor;
		BYTE	      m_FormType;	// LATER -- Get from format pointer?
		BYTE	      m_ColType;	// LATER -- Get from color  pointer?

	protected:
		// Data Members
		UINT             m_uBits;
		UINT		 m_uRegs;
		CCommsSysBlock * m_pBlock;
		INT		 m_nPos;

		// Property Access
		DWORD FindAsText(CDataRef const &Ref, UINT Type, DWORD Data);
		DWORD FindAsText(CDataRef const &Ref);
		DWORD FindLabel(CDataRef const &Ref);
		DWORD FindFore(CDataRef const &Ref);
		DWORD FindBack(CDataRef const &Ref);
		DWORD FindEventStatusMask(CDataRef const &Ref);

		// Implementation
		BOOL Resolve(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Tag Folder
//

class CTagFolder : public CTag
{
	public:
		// Constructor
		CTagFolder(void);

		// Destructor
		~CTagFolder(void);

		// Initialization
		void Load(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic Data Tag
//

class CDataTag : public CTag
{
	public:
		// Constructor
		CDataTag(void);

		// Destructor
		~CDataTag(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsArray(void) const;
		UINT GetExtent(void) const;
		BOOL CanWrite(void) const;

		// Operations
		void Force(void);

		// Item Properties
		UINT         m_Extent;
		UINT         m_Access;
		UINT	     m_RdMode;
		UINT         m_Addr;
		CSecDesc   * m_pSec;
		CCodedItem * m_pOnWrite;

	protected:
		// Implementation
		BOOL AllowWrite(CDataRef const &Ref, DWORD Data, UINT Type);
		BOOL LocalData(void);
		BOOL IsNumeric(UINT Type);
		BOOL IsInteger(UINT Type);
		BOOL IsString (UINT Type);
	};

//////////////////////////////////////////////////////////////////////////
//
// Simple Tag Item
//

class CTagSimple : public CDataTag
{
	public:
		// Constructor
		CTagSimple(void);

		// Destructor
		~CTagSimple(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		UINT GetDataType(void) const;
		BOOL CanWrite(void) const;

		// Evaluation
		DWORD GetProp(CDataRef const &Ref, WORD ID, UINT Type);
	};

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Item
//

class CTagNumeric : public CDataTag
{
	public:
		// Constructor
		CTagNumeric(void);

		// Destructor
		~CTagNumeric(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		UINT GetDataType(void) const;

		// Operations
		void SetIndex(UINT uIndex);
		BOOL SetPollScan(UINT uCode);
		void Poll(UINT uDelta);

		// Text Access
		BOOL GetEventText(CUnicode &Text, UINT uItem, UINT uPos);

		// Quick Plot
		BOOL GetQuickPlot(CTagQuickPlot * &pQuick);

		// Evaluation
		BOOL  IsAvail(CDataRef const &Ref, UINT Flags);
		BOOL  SetScan(CDataRef const &Ref, UINT Code);
		DWORD GetData(CDataRef const &Ref, UINT Type, UINT Flags);
		BOOL  IsValid(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		BOOL  SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		DWORD GetProp(CDataRef const &Ref, WORD ID, UINT Type);

		// Deadband
		BOOL HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev);

		// Item Properties
		UINT                 m_Manipulate;
		UINT                 m_TreatAs;
		UINT                 m_ScaleTo;
		CCodedItem         * m_pDataMin;
		CCodedItem         * m_pDataMax;
		CCodedItem         * m_pDispMin;
		CCodedItem         * m_pDispMax;
		UINT                 m_HasSP;
		CCodedItem         * m_pSetpoint;
		UINT		     m_LimitType;
		CCodedItem	   * m_pLimitMin;
		CCodedItem	   * m_pLimitMax;
		CCodedItem	   * m_pDeadband;
		CTagEventNumeric   * m_pEvent1;
		CTagEventNumeric   * m_pEvent2;
		CTagTriggerNumeric * m_pTrigger1;
		CTagTriggerNumeric * m_pTrigger2;
		CTagQuickPlot      * m_pQuickPlot;

	protected:
		// Data Members
		UINT    m_uType;
		UINT    m_uSize;
		DWORD * m_pData;
		BOOL    m_fPoll;

		// Property Location
		DWORD FindPrefix(CDataRef const &Ref);
		DWORD FindUnits(CDataRef const &Ref);
		DWORD FindSP(CDataRef const &Ref, UINT Type);
		DWORD FindMin(CDataRef const &Ref, UINT Type);
		DWORD FindMax(CDataRef const &Ref, UINT Type);
		DWORD FindDeadband(CDataRef const &Ref, UINT Type);
		DWORD FindEventStatusMask(CDataRef const &Ref);

		// Implementation
		int    FindPos (CDataRef const &Ref);
		void   InitType(void);
		BOOL   InitData(void);
		void   SaveData(UINT n);
		void   TransToDisp(DWORD &Data, UINT Flags, UINT uPos);
		void   TransToData(DWORD &Data, UINT Flags, UINT uPos);
		void   Manipulate (DWORD &Data, BOOL fGet);
		void   ScaleToDisp(DWORD &Data, UINT uPos);
		void   ScaleToData(DWORD &Data, UINT uPos);
		DWORD  FindMask(void);
		DWORD  FindSignBit(void);
		double Round(double rData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Item
//

class CTagFlag : public CDataTag
{
	public:
		// Constructor
		CTagFlag(void);

		// Destructor
		~CTagFlag(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		UINT GetDataType(void) const;

		// Operations
		void SetIndex(UINT uIndex);
		BOOL SetPollScan(UINT uCode);
		void Poll(UINT uDelta);

		// Text Access
		BOOL GetEventText(CUnicode &Text, UINT uItem, UINT uPos);

		// Evaluation
		BOOL  IsAvail(CDataRef const &Ref, UINT Flags);
		BOOL  SetScan(CDataRef const &Ref, UINT Code);
		DWORD GetData(CDataRef const &Ref, UINT Type, UINT Flags);
		BOOL  SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		DWORD GetProp(CDataRef const &Ref, WORD ID, UINT Type);
		
		// Item Properties
		UINT		  m_TreatAs;
		UINT		  m_TakeBit;
		UINT              m_Manipulate;
		UINT		  m_Atomic;
		UINT              m_HasSP;
		CCodedItem      * m_pSetpoint;
		CTagEventFlag   * m_pEvent1;
		CTagEventFlag   * m_pEvent2;
		CTagTriggerFlag * m_pTrigger1;
		CTagTriggerFlag * m_pTrigger2;

	protected:
		// Data Members
		UINT   m_uSize;
		BYTE * m_pData;
		BOOL   m_fPoll;

		// Property Location
		DWORD FindSP(CDataRef const &Ref, UINT Type);
		DWORD FindEventStatusMask(CDataRef const &Ref);

		// Implementation
		int   FindPos (CDataRef const &Ref);
		DWORD FindMask(CDataRef const &Ref);
		void  Manipulate(DWORD &Data);
		void  EditBit(DWORD &dwData, DWORD dwMask, DWORD Data);
		DWORD MakeOneZero(DWORD Data);
		BOOL  InitData(void);
		void  SaveData(UINT n);
	};

//////////////////////////////////////////////////////////////////////////
//
// String Tag Item
//

class CTagString : public CDataTag
{
	public:
		// Constructor
		CTagString(void);

		// Destructor
		~CTagString(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		UINT GetDataType(void) const;

		// Operations
		void Purge(void);

		// Evaluation
		BOOL  IsAvail(CDataRef const &Ref, UINT Flags);
		BOOL  SetScan(CDataRef const &Ref, UINT Code);
		DWORD GetData(CDataRef const &Ref, UINT Type, UINT Flags);
		BOOL  SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		DWORD GetProp(CDataRef const &Ref, WORD ID, UINT Type);

		// Deadband
		void InitPrevious(DWORD &Prev);
		BOOL HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev);
		void KillPrevious(DWORD &Prev);

		// Item Properties
		UINT m_Encode;
		UINT m_Length;

	protected:
		// Data Members
		UINT       m_uStep;
		UINT       m_uSize;
		CUnicode * m_pData;

		// Implementation
		int   FindPos (CDataRef const &Ref);
		void  FindStep(void);
		BOOL  InitData(void);
		void  SaveData(UINT n);
		void  Decode(PUTF   pText, PCDWORD pData);
		void  Encode(PDWORD pData, PCUTF   pText);
		WCHAR GetChar(PCUTF &pText);
		UINT  GetCharBits(void);
		WCHAR DecodeHex(BYTE bHex);
		DWORD Flip(DWORD d);
		WORD  Flip(WORD  d);
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic Tag Pollable Item
//

class CTagPollable : public CCodedHost
{
	public:
		// Constructor
		CTagPollable(void);

		// Destructor
		~CTagPollable(void);

		// Operations
		void SetIndex(UINT uIndex);
		void SetCount(UINT uCount);

		// Item Properties
		UINT m_Mode;
		UINT m_Delay;

	protected:
		// Context
		struct CCtx
		{
			UINT	m_fInit :1;
			UINT	m_fLast :1;
			UINT	m_fState:1;
			UINT	m_uTimer;
			DWORD	m_HV;
			};

		// Data Members
		UINT   m_uIndex;
		CCtx * m_pCtx;

		// Event Hook
		virtual void OnChange(UINT uPos, BOOL fChange, DWORD PV);

		// Startup Mode
		virtual BOOL FireOnInit(void);

		// Implementation
		void PollFlag   (UINT uPos, C3INT Enable, DWORD  SP, DWORD  PV, UINT uDelta);
		void PollInteger(UINT uPos, C3INT Enable, C3INT  SP, C3INT  PV, C3INT  Value, C3INT  Hyst, UINT uDelta);
		void PollReal   (UINT uPos, C3INT Enable, C3REAL SP, C3REAL PV, C3REAL Value, C3REAL Hyst, UINT uDelta);
		void PollUpdate (UINT uPos, UINT uDelta, BOOL fState, BOOL fChange, DWORD PV);
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic Tag Event
//

class CTagEvent : public CTagPollable
{
	public:
		// Constructor
		CTagEvent(void);

		// Destructor
		~CTagEvent(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAvail(void) const;
		BOOL IsActive(UINT uPos) const;

		// Operations
		void SetScan(UINT Code);

		// Text Access
		BOOL GetEventText(CUnicode &Text, UINT uPos);

		// Item Properties
		BOOL	      m_Const;
		CCodedText  * m_pLabel;
		UINT	      m_Trigger;
		UINT	      m_Accept;
		UINT	      m_Priority;
		UINT	      m_Print;
		UINT	      m_Siren;
		UINT	      m_Mail;
		CCodedItem  * m_pOnActive;
		CCodedItem  * m_pOnClear;
		CCodedItem  * m_pOnAccept;
		CCodedItem  * m_pOnEvent;
		CCodedItem  * m_pEnable;

	protected:
		// Event Hook
		void OnChange(UINT uPos, BOOL fChange, DWORD PV);

		// Startup Mode
		BOOL FireOnInit(void);

		// Implementation
		BOOL FireAlarm(UINT uPos, BOOL fState);
		BOOL FireEvent(UINT n);
	};

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Event
//

class CTagEventNumeric : public CTagEvent
{
	public:
		// Instantiator
		static CTagEventNumeric * Create(PCBYTE &pData);

		// Constructor
		CTagEventNumeric(void);

		// Destructor
		~CTagEventNumeric(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAvail(void) const;

		// Operations
		void SetScan(UINT Code);
		void Poll(UINT uPos, DWORD SP, DWORD PV, UINT Type, UINT uDelta);

		// Item Properties
		CCodedItem * m_pValue;
		CCodedItem * m_pHyst;
	};

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Event
//

class CTagEventFlag : public CTagEvent
{
	public:
		// Instantiator
		static CTagEventFlag * Create(PCBYTE &pData);

		// Constructor
		CTagEventFlag(void);

		// Destructor
		~CTagEventFlag(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAvail(void) const;

		// Operations
		void SetScan(UINT Code);
		void Poll(UINT uPos, DWORD SP, DWORD PV, UINT uDelta);
	};

//////////////////////////////////////////////////////////////////////////
//
// Generic Tag Trigger
//

class CTagTrigger : public CTagPollable
{
	public:
		// Constructor
		CTagTrigger(void);

		// Destructor
		~CTagTrigger(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAvail(void) const;

		// Operations
		void SetScan(UINT Code);

		// Item Properties
		CCodedItem * m_pAction;

	protected:
		// Event Hook
		void OnChange(UINT uPos, BOOL fChange, DWORD PV);
	};

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Trigger
//

class CTagTriggerNumeric : public CTagTrigger
{
	public:
		// Instantiator
		static CTagTriggerNumeric * Create(PCBYTE &pData);

		// Constructor
		CTagTriggerNumeric(void);

		// Destructor
		~CTagTriggerNumeric(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsAvail(void) const;

		// Operations
		void SetScan(UINT Code);
		void Poll(UINT uPos, DWORD SP, DWORD PV, UINT Type, UINT uDelta);

		// Item Properties
		CCodedItem * m_pValue;
		CCodedItem * m_pHyst;
	};

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Trigger
//

class CTagTriggerFlag : public CTagTrigger
{
	public:
		// Instantiator
		static CTagTriggerFlag * Create(PCBYTE &pData);

		// Constructor
		CTagTriggerFlag(void);

		// Destructor
		~CTagTriggerFlag(void);

		// Attributes
		BOOL IsAvail(void) const;

		// Operations
		void SetScan(UINT Code);
		void Poll(UINT uPos, DWORD SP, DWORD PV, UINT uDelta);

		// Initialization
		void Load(PCBYTE &pData);
	};


//////////////////////////////////////////////////////////////////////////
//
// Tag Quick Plot
//

class CTagQuickPlot : public CCodedHost
{
	public:
		// Constructor
		CTagQuickPlot(CTag *pTag);

		// Destructor
		~CTagQuickPlot(void);

		// Attributes
		UINT GetSequence(void) const;
		UINT GetPointLimit(void) const;
		UINT GetPointCount(void) const;

		// Operations
		void GetData(C3REAL *pData);
		void SetScan(UINT Code);
		void Init(void);
		void Poll(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Item Properties
		UINT	     m_Mode;
		UINT	     m_Rewind;
		CCodedItem * m_pEnable;
		CCodedItem * m_pStore;
		CCodedItem * m_pTime;
		CCodedItem * m_pLook;
		UINT	     m_Points;

	protected:
		// Data Members
		CTag * m_pTag;
		PDWORD m_pData;
		BOOL   m_fRun;
		UINT   m_uLook;
		INT    m_nUsed;
		UINT   m_uHead;
		UINT   m_uTail;
		INT    m_nSlot;
		UINT   m_uInit;
		UINT   m_uTime;
		UINT   m_uSeq;

		// Implementation
		void  ClearData(void);
		void  StoreData(DWORD Data);
		BOOL  CheckRun(void);
		BOOL  CheckLook(void);
		DWORD FindData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProxy;
class CServices;
class CUSBHostItem;
class CWriteQueue;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCommsManager;
class CEthernetItem;
class CRackItem;
class CCommsPortList;
class CCommsPort;
class CCommsPortObject;
class CCommsPortSerial;
class CCommsPortNetwork;
class CCommsPortVirtual;
class CCommsPortCAN;
class CCommsPortProfibus;
class CCommsPortFireWire;
class CCommsPortDevNet;
class CCommsPortCatLink;
class CCommsPortMPI;
class CCommsPortRack;
class CCommsDeviceList;
class CCommsDevice;
class CCommsSysBlockList;
class CCommsSysBlock;
class CCommsMapBlockList;
class CCommsMapBlock;
class CCommsMapRegList;
class CCommsMapReg;
class CCommsMappingList;
class CCommsMapping;

//////////////////////////////////////////////////////////////////////////
//
// Communications Manager
//

class CCommsManager : public CCodedHost, public IDiagProvider
{
	public:
		// Constructor
		CCommsManager(void);

		// Destructor
		~CCommsManager(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		DWORD GetCommsError(void) const;
		UINT  GetCommsStatus(void) const;

		// Registration
		void RegPort  (UINT uPort,   CCommsPort     *pPort  );
		void RegDevice(UINT uDevice, CCommsDevice   *pDevice);
		void RegBlock (UINT uBlock,  CCommsSysBlock *pBlock );

		// Item Lookup
		ICommsRawPort  * FindRawPort(UINT uPort  ) const;
		CCommsPort     * FindPort   (UINT uPort  ) const;
		CCommsDevice   * FindDevice (UINT uDevice) const;
		CCommsSysBlock * FindBlock  (UINT uBlock ) const;

		// Block Resolution
		CCommsSysBlock * Resolve(CDataRef const &Src, int &nPos);

		// Address Lookup
		BOOL FindAddress(CAddress const &Addr, CDataRef &Ref, CCommsDevice * pDev);

		// Task List
		void GetTaskList(CTaskList &List);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Item Properties
		UINT		     m_Handle;
		UINT		     m_MaxPort;
		UINT		     m_MaxDevice;
		UINT		     m_MaxBlock;
		UINT		     m_StrPad;
		CCodedItem	   * m_pOnEarly;
		CCodedItem	   * m_pOnStart;
		CCodedItem	   * m_pOnApply;
		CCodedItem	   * m_pOnSecond;
		CEthernetItem	   * m_pEthernet;
		CCommsPortList     * m_pPorts;
		CServices	   * m_pServices;
		CRackItem          * m_pRack;

		// Lookup Tables
		CCommsPort     ** m_ppPort;
		CCommsDevice   ** m_ppDevice;
		CCommsSysBlock ** m_ppBlock;

		// Public Data
		IProxyDisplayList  * m_pDisplays;
		IProxyCameraList   * m_pCameras;

	protected:
		// Data Members
		ULONG m_uRefs;

		// Diag Status
		IDiagManager * m_pDiag;
		UINT           m_uProv;

		// Diagnostics
		BOOL DiagRegister(void);
		BOOL DiagRevoke(void);
		UINT DiagBlocks(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagDevs(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagPorts(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Backplane Confguration
//

class CRackItem : public CItem
{
	public:
		// Constructor
		CRackItem(void);

		// Destructor
		~CRackItem(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Task List
		void GetTaskList(CTaskList &List);

		// Item Properties
		CCommsPortList * m_pPorts;
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Configuration
//

class CEthernetItem : public CItem
{
public:
	// Constructor
	CEthernetItem(void);

	// Destructor
	~CEthernetItem(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Task List
	void GetTaskList(CTaskList &List, UINT &uLevel);

protected:
	// Item Properties
	CCommsPortList  * m_pPorts;
};

//////////////////////////////////////////////////////////////////////////
//
// Comms Port List
//

class CCommsPortList : public CItem
{
	public:
		// Constructor
		CCommsPortList(void);

		// Destructor
		~CCommsPortList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL HasModem(void) const;

		// Task List
		void GetTaskList(CTaskList &List, UINT &uLevel);

		// Data Members
		UINT	      m_uCount;
		CCommsPort ** m_ppPort;
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Port
//

class CCommsPort : public CCodedHost, public ITaskEntry
{
	public:
		// Class Creation
		static CCommsPort * MakeObject(UINT uType);

		// Constructor
		CCommsPort(void);

		// Destructor
		~CCommsPort(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsModem (void) const;

		// Operations
		void AddCount(UINT uType, UINT uCode);

		// Task List
		virtual void GetTaskList(CTaskList &List, UINT &uLevel);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Proxy Calls
		virtual BOOL METHOD Open(IPortHandler *pData, UINT uFlags);
		virtual BOOL METHOD Open(ICommsDriver *pComms);
		virtual void METHOD Poll(void);
		virtual void METHOD Term(void);

		// Management
		virtual BOOL IsRemote(void);
		virtual UINT GetConfig(UINT uParam);
		virtual void CommsDebug(IDiagOutput *pOut, BOOL &fHead);
		virtual UINT Control(PVOID pCtx, UINT uCode, PCTXT pValue);

		// Item Properties
		UINT		   m_Number;
		UINT	           m_PortPhys;
		UINT	           m_PortLog;
		UINT	           m_Binding;
		UINT	           m_DriverID;
		UINT		   m_DriverRpc;
		PBYTE		   m_pImage;
		UINT		   m_uImage;
		PBYTE	           m_pConfig;
		UINT               m_uConfig;
		CCommsDeviceList * m_pDevices;
		IDriver		 * m_pDriver;
		ICommsDriver     * m_pComms;
		IMutex		 * m_pMutex;

	protected:
		// Data Members
		CProxy   * m_pProxy;
		UINT       m_uCount[2][7];
		char       m_sPath [256];
		DWORD      m_dwContext;
		UINT	   m_hModule;

		// DLD Support
		BOOL LoadDLD(void);
		BOOL FreeDLD(void);

		// Mutex Management
		BOOL MutexClaim(void);
		BOOL MutexFree(void);

		// Debug Helpers
		BOOL ShowHeader(IDiagOutput *pOut);
		BOOL ShowStatus(IDiagOutput *pOut);
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Port with Port Object
//

class CCommsPortObject : public CCommsPort
{
	public:
		// Constructor
		CCommsPortObject(void);

		// Destructor
		~CCommsPortObject(void);

		// Proxy Calls
		BOOL METHOD Open(IPortHandler *pData, UINT uFlags);
		BOOL METHOD Open(ICommsDriver *pDriver);
		void METHOD Term(void);

		// Management
		UINT GetConfig(UINT uParam);
		UINT Control(PVOID pCtx, UINT uCode, PCTXT pValue);

	protected:
		// Data Members
		IPortObject   * m_pPort;
		UINT		m_uPhysMask;
		CSerialConfig   m_Config;

		// Overridables
		virtual BOOL FindObject(void);
		virtual void FreeObject(void);
		virtual BOOL FindConfig(void);

		// Implementation
		BOOL AdjustForLogPort(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Serial Comms Port
//

class CCommsPortSerial : public CCommsPortObject
{
	public:
		// Constructor
		CCommsPortSerial(void);

		// Destructor
		~CCommsPortSerial(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Proxy Calls
		BOOL METHOD Open(IPortHandler *pData, UINT uFlags);
		BOOL METHOD Open(ICommsDriver *pDriver);
		void METHOD Poll(void);
		void METHOD Term(void);

		// Management
		BOOL IsRemote(void);

		// Item Properties
		UINT         m_Phys;
		CCodedItem * m_pBaud;
		CCodedItem * m_pData;
		CCodedItem * m_pStop;
		CCodedItem * m_pParity;
		CCodedItem * m_pMode;
		CCodedItem * m_pShareEnable;
		CCodedItem * m_pSharePort;

	protected:
		// Data Members
		IPortGrabber * m_pGrabber;
		BOOL	       m_fRemote;
		BOOL           m_fShare;
		UINT           m_uShare;
		ISocket      * m_pShare;
		IDataHandler * m_pLocal;

		// Overridables
		BOOL FindConfig(void);

		// Implementation
		BOOL GoShared(void);
		BOOL IsPureShared(void);
		UINT GetShareTimeout(void);
		void SharePort(void);
		BOOL ShareData(void);
		BOOL ShareSend(void);
		BOOL ShareRecv(void);
		void Suspend(void);
		void Resume(void);
		void GrabPort(void);
		void GrabDone(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Network Comms Port
//

class CCommsPortNetwork : public CCommsPort
{
	public:
		// Constructor
		CCommsPortNetwork(void);

		// Destructor
		~CCommsPortNetwork(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Proxy Calls
		BOOL METHOD Open(IPortHandler *pData, UINT uFlags);
		BOOL METHOD Open(ICommsDriver *pDriver);
		void METHOD Term(void);

		// Management
		UINT Control(PVOID pCtx, UINT uCode, PCTXT pValue);
	};

//////////////////////////////////////////////////////////////////////////
//
// Virtual Comms Port
//

class CCommsPortVirtual : public CCommsPortObject, public IPortObject
{
	public:
		// Constructor
		CCommsPortVirtual(void);

		// Destructor
		~CCommsPortVirtual(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Task List
		void GetTaskList(CTaskList &List, UINT &uLevel);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IPortObject
		void  METHOD Bind(IPortHandler *pHandler);
		void  METHOD Bind(IPortSwitch  *pSwitch);
		UINT  METHOD GetPhysicalMask(void);
		BOOL  METHOD Open(CSerialConfig const &Config);
		void  METHOD Close(void);
		void  METHOD Send(BYTE bData);
		void  METHOD SetBreak(BOOL fBreak);
		void  METHOD EnableInterrupts(BOOL fEnable);
		void  METHOD SetOutput(UINT uOutput, BOOL fOn);
		BOOL  METHOD GetInput(UINT uInput);
		DWORD METHOD GetHandle(void);
		
		// Port Handlers
		BOOL METHOD OnEvent(void);
		void METHOD OnTimer(void);

		// Item Properties
		CCodedItem * m_pMode;
		CCodedItem * m_pIP;
		CCodedItem * m_pNum;
		CCodedItem * m_pLinkOpt;
		CCodedItem * m_pTimeout;

	protected:

		enum {
			linkDefault	= 0,
			linkKeepAlive	= 1,
			linkTimeout	= 2
			};

		// Data Members
		ULONG          m_uRefs;		
		IPortHandler * m_pHandler;
		IMutex       * m_pSuspend;
		IEvent       * m_pSend;
		BYTE	       m_bSend;
		BOOL	       m_fOpen;
		HTASK	       m_hTask;
		UINT	       m_uMode;
		DWORD	       m_IP;
		UINT	       m_uPort;
		ISocket      * m_pSock;
		CBuffer      * m_pBuff;
		UINT	       m_uLast;
		UINT	       m_uTimeout;

		// Overridables
		BOOL FindObject(void);
		void FreeObject(void);

		// Implementation
		void AllocSocket(void);
		BOOL CheckSocket(void);
		void CloseSocket(BOOL fAbort);
		BOOL IsInactive(void);
		void DumpSend(void);
		void SendData(void);
		void RecvData(void);
		void PollData(void);
		void SetLast(BOOL fInit);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Comms Port
//

class CCommsPortCAN : public CCommsPortObject
{
	public:
		// Constructor
		CCommsPortCAN(void);

		// Destructor
		~CCommsPortCAN(void);

		// Initialization
		void Load(PCBYTE &pData);
	
	protected:
		// Item Properties
		UINT m_Baud;

		// Overridables
		BOOL FindObject(void);
		BOOL FindConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Profibus Comms Port
//

class CCommsPortProfibus : public CCommsPortObject
{
	public:
		// Constructor
		CCommsPortProfibus(void);

		// Destructor
		~CCommsPortProfibus(void);

		// Initialization
		void Load(PCBYTE &pData);

	protected:
		// Overridables
		BOOL FindObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// FireWire Comms Port
//

class CCommsPortFireWire : public CCommsPortObject
{
	public:
		// Constructor
		CCommsPortFireWire(void);

		// Destructor
		~CCommsPortFireWire(void);

		// Initialization
		void Load(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Comms Port
//

class CCommsPortDevNet : public CCommsPortObject
{
	public:
		// Constructor
		CCommsPortDevNet(void);

		// Destructor
		~CCommsPortDevNet(void);

		// Initialization
		void Load(PCBYTE &pData);

	protected:
		// Item Properties
		UINT m_Baud;

		// Overridables
		BOOL FindObject(void);
		BOOL FindConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CatLink Comms Port
//

class CCommsPortCatLink : public CCommsPortObject
{
	public:
		// Constructor
		CCommsPortCatLink(void);

		// Destructor
		~CCommsPortCatLink(void);

		// Initialization
		void Load(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// MPI Comms Port
//

class CCommsPortMPI : public CCommsPortObject
{
	public:
		// Constructor
		CCommsPortMPI(void);

		// Destructor
		~CCommsPortMPI(void);

		// Initialization
		void Load(PCBYTE &pData);

	protected:
		// Item Properties
		UINT m_ThisDrop;

		// Overridables
		BOOL FindConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Port
//

class CCommsPortRack : public CCommsPortObject
{
	public:
		// Constructor
		CCommsPortRack(void);

		// Destructor
		~CCommsPortRack(void);

		// Initialization
		void Load(PCBYTE &pData);

	protected:
		// Overridables
		BOOL FindConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN J1939 Comms Port
//

class CCommsPortJ1939 : public CCommsPortCAN
{
	public:
		// Constructor
		CCommsPortJ1939(void);

		// Initialization
		void Load(PCBYTE &pData);

	protected:
		// Overridables
		BOOL FindObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Comms Port
//

class CCommsPortTest : public CCommsPortObject
{
	public:
		// Constructor
		CCommsPortTest(void);

		// Destructor
		~CCommsPortTest(void);

		// Initialization
		void Load(PCBYTE &pData);

	protected:
		
		// Overridables
		BOOL FindObject(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Device List
//

class CCommsDeviceList : public CItem
{
	public:
		// Constructor
		CCommsDeviceList(void);

		// Destructor
		~CCommsDeviceList(void);

		// Initialization
		void Load(PCBYTE &pData, CCommsPort *pPort);

		// Operations
		void SetError(UINT uError);

		// Data Members
		UINT	        m_uCount;
		CCommsDevice ** m_ppDevice;
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Device
//

class CCommsDevice : public CCodedHost, public ICommsDevice
{
	public:
		// Constructor
		CCommsDevice(void);

		// Destructor
		~CCommsDevice(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Queue Access
		CWriteQueue * GetQueue(UINT n) const;

		// Attributes
		BOOL HasError(void) const;
		UINT GetError(void) const;
		UINT GetStatus(void) const;
		UINT GetInvite(void) const;
		BOOL IsEnabled(void) const;
		BOOL HasQueue(void) const;
		BOOL IsQueueEmpty(void) const;
		BOOL HasPreempt(void) const;

		// Operations
		void Bind(CCommsPort *pPort);
		BOOL Tickle(void);
		void Suspend(void);
		void Resume(void);
		void Enable(BOOL fEnable);
		void SetRespond(BOOL fRespond);
		void SetProblem(BOOL fProblem);
		void SetError(UINT uError);
		void SetInvite(UINT uInvite);
		void MarkInvalid(void);
		BOOL EnableQueue(void);
		void LockDevice(void);
		void FreeDevice(void);
		BOOL QueueWrite(CCommsSysBlock *pBlock, INT nPos, DWORD Data, DWORD dwMask);
		BOOL QueueWrite(CCommsSysBlock *pBlock, INT nPos, DWORD Data);
		void FlushQueue(CCommsSysBlock *pBlock, INT nPos);
		void FlushQueue(CCommsSysBlock *pBlock);
		void EmptyQueue(void);
		UINT Control(UINT uCode, PCTXT pValue);
		void CommsDebug(IDiagOutput *pOut, BOOL &fHead);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICommsDevice
		PCBYTE METHOD GetConfig(void);
		UINT   METHOD GetConfigSize(void);
		void   METHOD SetContext(PVOID pData);
		PVOID  METHOD GetContext(void);

		// Item Properties
		UINT                 m_Number;
		CCodedItem         * m_pEnable;
		UINT                 m_Split;
		UINT                 m_Delay;
		UINT		     m_Transact;
		UINT		     m_Preempt;
		UINT		     m_Spanning;
		PBYTE		     m_pConfig;
		UINT                 m_uConfig;
		CCommsSysBlockList * m_pSysBlocks;
		CCommsMapBlockList * m_pMapBlocks;

	protected:
		// Data Members
		ULONG         m_uRefs;
		CCommsPort  * m_pPort;
		BOOL          m_fQueue;
		CWriteQueue * m_pQueue[2];
		IMutex      * m_pMutex;
		PVOID         m_pContext;
		UINT          m_uError;
		UINT          m_uInvite;
		BOOL	      m_fEnable;
		BOOL	      m_fSuspend;
		BOOL	      m_fRespond0;
		BOOL	      m_fProblem0;
		BOOL	      m_fRespond1;
		BOOL	      m_fProblem1;
		UINT	      m_uTime;

		// Debug Helpers
		BOOL ShowHeader(IDiagOutput *pOut);
		BOOL ShowStatus(IDiagOutput *pOut);

		// Implementation
		BOOL AllocQueues(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Block List
//

class CCommsSysBlockList : public CItem
{
	public:
		// Constructor
		CCommsSysBlockList(void);

		// Destructor
		~CCommsSysBlockList(void);

		// Initialization
		void Load(PCBYTE &pData);
		
		// Attributes
		BOOL HasError(void) const;

		// Operations
		void Bind(CCommsDevice *pDevice);
		void SetError(UINT uError);
		void UpdateTimers(UINT uTime);
		void MarkInvalid(void);
		void ClearDirty(void);

		// Data Members
		UINT	          m_uCount;
		CCommsSysBlock ** m_ppBlock;
		IMutex          * m_pLock;
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Block
//

class CCommsSysBlock : public CItem
{
	public:
		// Constructor
		CCommsSysBlock(IMutex *pLock);

		// Destructor
		~CCommsSysBlock(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Adress Access
		CAddress GetAddress(INT nPos) const;
		BOOL     HasAddress(DWORD Addr, INT &nPos, CCommsDevice * pDev) const;

		// Attributes
		BOOL  IsNamed(void) const;
		BOOL  IsActive(void) const;
		UINT  GetError(void) const;
		DWORD GetData(INT nPos) const;
		BOOL  IsAvail(INT nPos) const;
		BOOL  ShouldRead(INT nPos) const;
		BOOL  CouldRead(INT nPos) const;
		BOOL  ShouldWrite(INT nPos) const;
		BOOL  IsCached(INT nPos) const;

		// Operations
		void   Bind(CCommsDevice *pDevice);
		void   MarkInvalid(void);
		void   ClearDirty(void);
		void   SetError(UINT uError);
		void   UpdateTimers(UINT uTime);
		BOOL   SetScan(INT nPos, UINT  Code);
		BOOL   SetCommsData(INT nPos);
		BOOL   SetCommsData(INT nPos, DWORD Data);
		DWORD  GetWriteData(INT nPos);
		PDWORD GetWriteData(INT nPos, INT nCount);
		BOOL   SetWriteData(INT nPos, UINT Flags, DWORD dwMask, DWORD Data);
		BOOL   SetWriteData(INT nPos, UINT Flags, DWORD Data);
		void   SetWriteDone(INT nPos);
		void   SetQueueDone(INT nPos);
		void   CommsDebug(IDiagOutput *pOut, BOOL &fHead);

		// Item Properties
		UINT       m_Number;
		UINT       m_Named;
		UINT       m_Space;
		INT        m_Factor;
		INT        m_Align;
		INT        m_Base;
		INT        m_Index;
		INT        m_Size;
		PWORD      m_pList;

	protected:
		// Entry Info
		struct CInfo
		{
			DWORD m_uScan  :11;
			DWORD m_uQueue : 9;
			DWORD m_bTimer : 3;
			DWORD m_fScan  : 1;
			DWORD m_fUser  : 1;
			DWORD m_fOnce  : 1;
			DWORD m_fBroke : 1;
			DWORD m_fAvail : 1;
			DWORD m_fDirty : 1;
			DWORD m_fWrite : 1;
			DWORD m_fQueue : 1;
			DWORD m_fCache : 1;
			};

		// Constants
		enum
		{
			maxScan  = 2047,
			maxQueue = 511,
			maxTimer = 7,
			};

		// Data Members
		IMutex       * m_pLock;
		CCommsDevice * m_pDevice;
		CInfo	     * m_pInfo;
		DWORD	     * m_pData;
		UINT	       m_uError;
		UINT	       m_uScan;
		UINT	       m_uUser;
		UINT	       m_uOnce;
		UINT	       m_uDirty;

		// Debug Helpers
		BOOL ShowHeader(IDiagOutput *pOut);
		BOOL ShowStatus(IDiagOutput *pOut);

		// Implementation
		void AllocData(void);
		void SetDirty(CInfo &Info);
		void KillDirty(CInfo &Info);
		void KillUser(CInfo &Info);
		void KillOnce(CInfo &Info);
	};

//////////////////////////////////////////////////////////////////////////
//
// Mapping Block List
//

class CCommsMapBlockList : public CItem
{
	public:
		// Constructor
		CCommsMapBlockList(void);

		// Destructor
		~CCommsMapBlockList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL HasError(void) const;

		// Operations
		void Bind(CCommsDevice *pDevice);
		void SetError(UINT uError);

		// Data Members
		UINT	          m_uCount;
		CCommsMapBlock ** m_ppBlock;
	};

//////////////////////////////////////////////////////////////////////////
//
// Mapping Block
//

class CCommsMapBlock : public CCodedHost
{
	public:
		// Constructor
		CCommsMapBlock(void);

		// Destructor
		~CCommsMapBlock(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Adress Access
		CAddress GetAddress(INT nPos) const;

		// Attributes
		BOOL   IsActive(void) const;
		UINT   GetError(void) const;
		DWORD  GetData(INT nPos) const;
		BOOL   ShouldRead(INT nPos) const;
		BOOL   CouldRead(INT nPos) const;
		BOOL   ShouldWrite(INT nPos) const;
		UINT   GetFlags(void) const;

		// Operations
		void   Bind(CCommsDevice *pDevice);
		BOOL   UpdateWriteData(void);
		BOOL   UpdateReadData(void);
		void   MarkDone(void);
		void   SetError(UINT uError);
		BOOL   SetCommsData(INT nPos, DWORD Data);
		PDWORD GetWriteData(INT nPos);
		void   SetWriteDone(INT nPos);

		// Item Properties
		CAddress	    m_Addr;
		UINT		    m_Type;
		INT		    m_Size;
		UINT		    m_Write;
		UINT		    m_Factor;
		UINT		    m_Scaled;
		UINT		    m_Update;
		UINT		    m_Period;
		UINT		    m_Slave;
		CCodedItem        * m_pReq;
		CCodedItem        * m_pAck;
		CCommsMapRegList  * m_pRegs;

	protected:
		// Entry Info
		struct CInfo
		{
			BYTE m_fBroke: 1;
			BYTE m_fValid: 1;
			BYTE m_fDirty: 1;
			BYTE m_fLocal: 1;
			BYTE m_fPad  : 4;
			};

		// Data Members
		CCommsDevice *  m_pDevice;
		PDWORD		m_pData;
		CInfo *		m_pInfo;
		UINT		m_uDirty;
		UINT		m_uError;
		UINT		m_uLast;

		// Implementation
		BOOL InitData(void);
		BOOL InitWriteData(void);
		BOOL InitReadData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Mapping Register List
//

class CCommsMapRegList : public CItem
{
	public:
		// Constructor
		CCommsMapRegList(void);

		// Destructor
		~CCommsMapRegList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		BOOL SetScan(UINT Code);

		// Data Members
		UINT	        m_uCount;
		CCommsMapReg ** m_ppReg;
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Mapping Register
//

class CCommsMapReg : public CItem
{
	public:
		// Constructor
		CCommsMapReg(void);

		// Destructor
		~CCommsMapReg(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL  IsMapped(void) const;
		BOOL  IsAvail(void) const;
		DWORD GetValue(UINT Flags) const;
		DWORD GetMetaData(WORD ID) const;

		// Operations
		BOOL SetScan(UINT Code);
		BOOL SetValue(DWORD Data, UINT Flags);
		
		// Item Properties
		UINT   m_uRefs;
		PDWORD m_pRefs;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CProgramManager;
class CProgramItem;

//////////////////////////////////////////////////////////////////////////
//
// Program Manager
//

class CProgramManager : public CItem, public ITaskEntry, public IDiagProvider
{
	public:
		// Constructor
		CProgramManager(void);

		// Destructor
		~CProgramManager(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Task List
		void GetTaskList(CTaskList &List);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Operations
		BOOL  IsAvail(UINT hProg);
		BOOL  SetScan(UINT hProg, UINT Code);
		BOOL  FindName(UINT hProg, PTXT pName, UINT uName);
		BOOL  Dispatch(CProgramItem *pProg, PDWORD pParam, UINT uLevel);
		DWORD Run(UINT hProg, PDWORD pParam);
		void  SetEarly(BOOL fEarly);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Type Definitions
		typedef CMap <UINT,     CProgramItem *> CProgramMap;
		typedef CMap <CString , UINT>           CProgramIndex;
		
		// Data Members
		ULONG	       m_uRefs;
		IDiagManager * m_pDiag;
		UINT	       m_uProv;
		BOOL	       m_fEarly;
		CProgramMap    m_Map;
		CProgramIndex  m_Index;
		CArray <UINT>  m_Handles;
		IEvent       * m_pRun;
		IMutex	     * m_pList;
		IMutex	     * m_pLock[5];
		ISemaphore   * m_pFlag[5];
		ISemaphore   * m_pDone[5];
		CProgramItem * m_pHead[5];
		CProgramItem * m_pTail[5];
		IMutex       * m_pRunLock;

		// Location
		CProgramItem * Find(UINT hProg);

		// Implementation
		void ProgInit(void);

		// Diagnostics
		BOOL DiagRegister(void);
		BOOL DiagRevoke(void);
		UINT DiagList(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagRun(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Program Item
//

class CProgramItem : public CCodedHost
{
	public:
		// Constructor
		CProgramItem(void);

		// Destructor
		~CProgramItem(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		BOOL  IsAvail(void);
		BOOL  SetScan(UINT Code);
		BOOL  PreRegister(void);
		DWORD Execute(PDWORD pParam);

		// Linked List
		CProgramItem * m_pNext;
		CProgramItem * m_pPrev;
		BOOL	       m_fTemp;
		BOOL	       m_fList;
		PDWORD	       m_pArgs;

		// Item Properties
		CUnicode     m_Name;
		CCodedItem * m_pCode;
		UINT	     m_BkGnd;
		UINT	     m_Comms;
		UINT	     m_Timeout;
		UINT	     m_Run;
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Control Interface
//

interface IStratonControl : public IBase
{
	virtual void LoadControl(PCBYTE &pData)		 = 0;
	virtual BOOL Service    (PCBYTE pIn, PBYTE pOut) = 0;
	virtual BOOL IsRunning  (void)			 = 0;
	virtual void GetTaskList(CTaskList &List)	 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Control Instance Pointer
//

extern	IStratonControl * g_pControl;

//////////////////////////////////////////////////////////////////////////
//
// Straton Control Instantiator
//

extern IStratonControl * Create_StratonControl(void);

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class     CSqlQueryList;
class     CSqlQuery;
class     CSqlColDef;
class     CSqlRecord;
class     CSqlStatement;
interface ISqlConnection;

//////////////////////////////////////////////////////////////////////////
//
// SQL Manager
//

class CSqlManager : public CCodedHost, public ITaskEntry
{
	public:
		// Constructor
		CSqlManager(void);

		// Destructor
		~CSqlManager(void);

		// Initialization
		void LoadSqlManager(PCBYTE &pData);

		// Task List
		void GetTaskList(CTaskList &List);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// User Status and Query Functions
		void  UserExecuteQuery(CString const &Query);
		void  UserExecuteAll(void);
		C3INT GetConnectionStatus(void);
		C3INT GetQueryStatus(CString const &Query);
		C3INT GetQueryLastRun(CString const &Query);

	protected:

		// Data Members
		UINT            m_Enable;
		UINT            m_Status;
		WORD            m_wPort;
		UINT		m_uTlsMode;
		CCodedItem    * m_pServer;
		CString         m_User;
		CString         m_Pass;
		CString         m_Database;
		CSqlQueryList * m_pQueries;
		CString         m_Instance;
		UINT            m_Connect;

		// Status
		enum {
			sqlServerPending,
			sqlServerSuccess,
			sqlServerFailed
			};

		// Data Members
		ISqlConnection * m_pConnection;
		CSqlStatement  * m_pStatement;

		// Implementation
		ISqlConnection * CreateConnection(UINT uType);

		// Query Servicing
		void ServiceQueries(void);
		BOOL RunQuery(CSqlQuery *pQuery);		
	};

//////////////////////////////////////////////////////////////////////////
//
// Nofify Codes
//

enum {
	notifySysLoaded   = 0,
	notifySysStarted  = 1,
	notifySysStopped  = 2,
	notifySysCleanup  = 3,
	};  

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Manager Interface
//

interface IUsbManager : public IUnknown
{
	virtual BOOL Open(void)				= 0;
	virtual BOOL Close(void)			= 0;
	virtual void Notify(UINT uMsg, DWORD dwParam)	= 0;
	};

// End of File

#endif
