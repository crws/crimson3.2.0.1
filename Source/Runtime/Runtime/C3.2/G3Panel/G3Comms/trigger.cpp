
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tag Trigger
//

// Constructor

CTagTrigger::CTagTrigger(void)
{
	m_pAction = NULL;
	}

// Destructor

CTagTrigger::~CTagTrigger(void)
{
	delete m_pAction;
	}

// Initialization

void CTagTrigger::Load(PCBYTE &pData)
{
	CCodedHost::Load(pData);

	m_Mode  = GetByte(pData);
	
	m_Delay = GetWord(pData);
	
	GetCoded(pData, m_pAction);
	}

// Attributes

BOOL CTagTrigger::IsAvail(void) const
{
	if( !IsItemAvail(m_pAction) ) {

		return FALSE;
		}

	return TRUE;
	}

// Operations

void CTagTrigger::SetScan(UINT uCode)
{
	SetItemScan(m_pAction, uCode);
	}

// Change Hook

void CTagTrigger::OnChange(UINT uPos, BOOL fChange, DWORD PV)
{
	CCtx &Ctx = m_pCtx[uPos];

	if( Ctx.m_fState ) {

		if( m_pAction ) {

			m_pAction->Execute(typeVoid, PDWORD(&uPos));
			}
		}

	CTagPollable::OnChange(uPos, fChange, PV);
	}

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Trigger
//

// Instantiator

CTagTriggerNumeric * CTagTriggerNumeric::Create(PCBYTE &pData)
{
	if( GetByte(pData) ) {

		CTagTriggerNumeric *pItem = New CTagTriggerNumeric;

		pItem->Load(pData);

		return pItem;
		}

	return NULL;
	}

// Constructor

CTagTriggerNumeric::CTagTriggerNumeric(void)
{
	m_pValue = NULL;

	m_pHyst  = NULL;
	}

// Destructor

CTagTriggerNumeric::~CTagTriggerNumeric(void)
{
	delete m_pValue;

	delete m_pHyst;
	}

// Initialization

void CTagTriggerNumeric::Load(PCBYTE &pData)
{
	ValidateLoad("TagTriggerNumeric", pData);

	CTagTrigger::Load(pData);

	GetCoded(pData, m_pValue);

	GetCoded(pData, m_pHyst);
	}

// Attributes

BOOL CTagTriggerNumeric::IsAvail(void) const
{
	if( CTagTrigger::IsAvail() ) {

		if( !IsItemAvail(m_pValue) ) {

			return FALSE;
			}

		if( !IsItemAvail(m_pHyst) ) {

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Operations

void CTagTriggerNumeric::SetScan(UINT uCode)
{
	CTagTrigger::SetScan(uCode);

	SetItemScan(m_pValue, uCode);
	
	SetItemScan(m_pHyst,  uCode);
	}

void CTagTriggerNumeric::Poll(UINT uPos, DWORD SP, DWORD PV, UINT Type, UINT uDelta)
{
	if( IsAvail() ) {

		if( Type == typeInteger ) {

			C3INT Value = GetItemData(m_pValue, 0, PDWORD(&uPos));

			C3INT Hyst  = GetItemData(m_pHyst,  0, PDWORD(&uPos));

			PollInteger(uPos, 1, SP, PV, Value, Hyst, uDelta);
			}

		if( Type == typeReal ) {

			C3REAL Value = GetItemData(m_pValue, C3REAL(0), PDWORD(&uPos));

			C3REAL Hyst  = GetItemData(m_pHyst,  C3REAL(0), PDWORD(&uPos));

			PollReal(uPos, 1, I2R(SP), I2R(PV), Value, Hyst, uDelta);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Trigger
//

// Instantiator

CTagTriggerFlag * CTagTriggerFlag::Create(PCBYTE &pData)
{
	if( GetByte(pData) ) {

		CTagTriggerFlag *pItem = New CTagTriggerFlag;

		pItem->Load(pData);

		return pItem;
		}

	return NULL;
	}

// Constructor

CTagTriggerFlag::CTagTriggerFlag(void)
{
	}

// Destructor

CTagTriggerFlag::~CTagTriggerFlag(void)
{
	}

// Initialization

void CTagTriggerFlag::Load(PCBYTE &pData)
{
	ValidateLoad("TagTriggerFlag", pData);

	CTagTrigger::Load(pData);
	}

// Attributes

BOOL CTagTriggerFlag::IsAvail(void) const
{
	if( CTagTrigger::IsAvail() ) {

		return TRUE;
		}

	return FALSE;
	}

// Operations

void CTagTriggerFlag::SetScan(UINT uCode)
{
	CTagTrigger::SetScan(uCode);
	}

void CTagTriggerFlag::Poll(UINT uPos, DWORD SP, DWORD PV, UINT uDelta)
{
	if( IsAvail() ) {

		PollFlag(uPos, 1, SP, PV, uDelta);
		}
	}

// End of File
