
#include "Intern.hpp"

#include "FakeSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fake Socket
//

// Constructor

CFakeSocket::CFakeSocket(void)
{
	m_uTxBuff = 0;

	m_uRxBuff = 0;

	StdSetRef();
}

// Operations

bool CFakeSocket::GetTxData(PCBYTE &pData, UINT &uSize)
{
	if( m_uTxBuff ) {

		pData = m_bTxBuff;

		uSize = m_uTxBuff;

		m_uTxBuff = 0;

		return true;
	}

	return false;
}

bool CFakeSocket::SetRxData(PCBYTE pData, UINT uSize)
{
	if( !m_uRxBuff ) {

		memcpy(m_bRxBuff, pData, uSize);

		m_uRxBuff = uSize;

		return true;
	}

	return false;
}

// IUnknown

HRESULT CFakeSocket::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ISocket);

	StdQueryInterface(ISocket);

	return E_NOINTERFACE;
}

ULONG CFakeSocket::AddRef(void)
{
	StdAddRef();
}

ULONG CFakeSocket::Release(void)
{
	StdRelease();
}

// ISocket

HRESULT CFakeSocket::Listen(WORD Loc)
{
	return E_FAIL;
}

HRESULT CFakeSocket::Listen(IPADDR const &Ip, WORD Loc)
{
	return E_FAIL;
}

HRESULT CFakeSocket::Connect(IPADDR const &Ip, WORD Rem)
{
	return Connect(Ip, 0, 0);
}

HRESULT CFakeSocket::Connect(IPADDR const &Ip, WORD Rem, WORD Loc)
{
	return S_OK;
}

HRESULT CFakeSocket::Connect(IPADDR const &Ip, IPADDR const &If, WORD Rem, WORD Loc)
{
	return S_OK;
}

HRESULT CFakeSocket::Recv(PBYTE pData, UINT &uSize, UINT uTime)
{
	if( Recv(pData, uSize) == S_OK ) {

		return S_OK;
	}

	Sleep(uTime);

	return E_FAIL;
}

HRESULT CFakeSocket::Recv(PBYTE pData, UINT &uSize)
{
	uSize = Min(uSize, m_uRxBuff);

	if( uSize ) {

		memcpy(pData, m_bRxBuff, uSize);

		memmove(m_bRxBuff, m_bRxBuff + uSize, m_uRxBuff - uSize);

		m_uRxBuff -= uSize;

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CFakeSocket::Send(PBYTE pData, UINT &uSize)
{
	if( m_uTxBuff + uSize < sizeof(m_bTxBuff) ) {

		memcpy(m_bTxBuff + m_uTxBuff, pData, uSize);

		m_uTxBuff += uSize;

		return S_OK;
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CFakeSocket::Recv(CBuffer * &pBuff, UINT uTime)
{
	Sleep(uTime);

	pBuff = NULL;

	return E_FAIL;
}

HRESULT CFakeSocket::Recv(CBuffer * &pBuff)
{
	pBuff = NULL;

	return E_FAIL;
}

HRESULT CFakeSocket::Send(CBuffer *pBuff)
{
	return S_OK;
}

HRESULT CFakeSocket::GetLocal(IPADDR &Ip)
{
	return E_FAIL;
}

HRESULT CFakeSocket::GetRemote(IPADDR &Ip)
{
	return E_FAIL;
}

HRESULT CFakeSocket::GetLocal(IPADDR &Ip, WORD &Port)
{
	return E_FAIL;
}

HRESULT CFakeSocket::GetRemote(IPADDR &Ip, WORD &Port)
{
	return E_FAIL;
}

HRESULT CFakeSocket::GetPhase(UINT &Phase)
{
	Phase = PHASE_OPEN;

	return S_OK;
}

HRESULT CFakeSocket::SetOption(UINT uOption, UINT uValue)
{
	return S_OK;
}

HRESULT CFakeSocket::Abort(void)
{
	return S_OK;
}

HRESULT CFakeSocket::Close(void)
{
	return S_OK;
}

// End of File
