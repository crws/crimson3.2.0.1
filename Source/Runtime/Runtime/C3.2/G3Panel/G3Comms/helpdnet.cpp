
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Helper Implementation
//

class CDeviceNetHelper : public IDeviceNetHelper 
{
	public:
		// Constructor
		CDeviceNetHelper(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDeviceNetHelper
		void METHOD Attach(IPortObject *pPort);
		void METHOD Detach(void);
		BOOL METHOD Open(void);
		void METHOD Close(void);
		void METHOD SetMac(BYTE bMac);
		BOOL METHOD EnableBitStrobe(WORD wRecvSize, WORD wSendSize);
		BOOL METHOD EnablePolled(WORD wRecvSize, WORD wSendSize);
		BOOL METHOD EnableData(WORD wRecvSize, WORD wSendSize);
		BOOL METHOD IsOnLine(void);
		UINT METHOD Read (UINT uId, PBYTE pData, UINT uSize);
		UINT METHOD Write(UINT uId, PCBYTE pData, UINT uSize);
	
	protected:
		// Data
		IDeviceNetPort * m_pPort;
		ULONG            m_uRefs;
	};

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Helper Implementation
//

// Instantiator

global IDeviceNetHelper * Create_DeviceNetHelper(void)
{
	return New CDeviceNetHelper;
	}

// Constructor

CDeviceNetHelper::CDeviceNetHelper(void)
{
	StdSetRef();

	m_pPort = NULL;
	}

// IUnknown

HRESULT CDeviceNetHelper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDeviceNetHelper);

	StdQueryInterface(IDeviceNetHelper);

	return E_NOINTERFACE;
	}

ULONG CDeviceNetHelper::AddRef(void)
{
	StdAddRef();
	}

ULONG CDeviceNetHelper::Release(void)
{
	StdRelease();
	}

// IDeviceNetHelper

void METHOD CDeviceNetHelper::Attach(IPortObject *pPort)
{
	m_pPort = (IDeviceNetPort *) pPort;
	}

void METHOD CDeviceNetHelper::Detach(void)
{
	m_pPort = NULL;
	}

BOOL METHOD CDeviceNetHelper::Open(void)
{
	return ((IDevice *) m_pPort)->Open();
	}

void METHOD CDeviceNetHelper::Close(void)
{
	m_pPort->Close();
	}

void METHOD CDeviceNetHelper::SetMac(BYTE bMac)
{
	m_pPort->SetMac(bMac);
	}

BOOL METHOD CDeviceNetHelper::EnableBitStrobe(WORD wRecvSize, WORD wSendSize)
{
	return m_pPort->EnableBitStrobe(wRecvSize, wSendSize);
	}

BOOL METHOD CDeviceNetHelper::EnablePolled(WORD wRecvSize, WORD wSendSize)
{
	return m_pPort->EnablePolled(wRecvSize, wSendSize);
	}

BOOL METHOD CDeviceNetHelper::EnableData(WORD wRecvSize, WORD wSendSize)
{
	return m_pPort->EnableData(wRecvSize, wSendSize);
	}

BOOL METHOD CDeviceNetHelper::IsOnLine(void)
{
	return m_pPort->IsOnLine();
	}

UINT METHOD CDeviceNetHelper::Read(UINT uId, PBYTE pData, UINT uSize)
{
	UINT uCount = m_pPort->Read(uId, pData, uSize);

	return uCount == NOTHING ? CCODE_ERROR : uCount;
	}

UINT MCALL CDeviceNetHelper::Write(UINT uId, PCBYTE pData, UINT uSize)
{
	UINT uCount = m_pPort->Write(uId, pData, uSize);

	return uCount == NOTHING ? CCODE_ERROR : uCount;
	}

// End of File
