
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PROXY_HPP

#define	INCLUDE_PROXY_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CProxy;

//////////////////////////////////////////////////////////////////////////
//
// Comms Proxy Object
//

class CProxy
{
	public:
		// Instatantation
		static CProxy * Create(CCommsPort *pPort);

		// Constructor
		CProxy(void);

		// Destructor
		virtual ~CProxy(void);

		// Binding
		virtual BOOL Bind(CCommsPort *pPort, IDriver *pDriver);

		// Task Count
		virtual UINT GetTaskCount(void);

		// Entry Points
		virtual void Init(UINT uTask);
		virtual void Task(UINT uTask);
		virtual void Stop(UINT uTask);
		virtual void Term(UINT uTask);

		// Suspension
		virtual void Suspend(void);
		virtual void Resume(void);

		// Control
		virtual UINT Control(void *pContext, UINT uFunc, PCTXT pValue);

		// List
		CProxy * m_pNext;
		CProxy * m_pPrev;

	protected:
		// Data Members
		CCommsPort   * m_pPort;
		IDriver      * m_pDriver;
		ICommsDriver * m_pComms;
		ICommsHelper * m_pCommsHelper;

		// Driver Creation
		static IDriver * AllocDriver(CCommsPort *pPort);
		static IDriver * LoadFromLib(WORD DriverID);

		// Implementation
		static BOOL GetSpecial(CProxy * &pProxy, UINT DriverID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Proxy Instantiators
//

CProxy * Create_ProxyHoneywell(BOOL fNet);
CProxy * Create_ProxySimple(void);
CProxy * Create_ProxyMaster(void);
CProxy * Create_ProxySlave(void);
CProxy * Create_ProxyProCon(void);
CProxy * Create_ProxyRaw(void);
CProxy * Create_ProxyRack(void);
CProxy * Create_ProxyDisplay(void);
CProxy * Create_ProxyCamera(void);

//////////////////////////////////////////////////////////////////////////
//
// List Instantiators
//

IProxyCameraList  * Create_ProxyCameraList(void);
IProxyDisplayList * Create_ProxyDisplayList(void);

// End of File

#endif
