
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SERVICE_HPP
	
#define	INCLUDE_SERVICE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Tag List Location
//

#define afxTagList CCommsSystem::m_pThis->m_pTags->m_pTags

//////////////////////////////////////////////////////////////////////////
//
// Service Interface
//

interface IService : public IUnknown
{
	AfxDeclareIID(1000, 1);

	virtual void LoadService(PCBYTE &pData)   = 0;
	virtual UINT GetID	(void)		  = 0;
	virtual void GetTaskList(CTaskList &List) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Time Sync Service Interface
//

interface IServiceTimeSync
{
	virtual DWORD GetLogTime(void)         = 0;
	virtual void  SetGPSTime(DWORD dwTime) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// File Sync Service Interface
//

interface IServiceFileSync
{
	virtual BOOL Sync(void)						= 0;
	virtual BOOL PutFile(PCTXT pLocal, PCTXT pRemote, BOOL fDelete) = 0;
	virtual BOOL GetFile(PCTXT pLocal, PCTXT pRemote, BOOL fDelete) = 0;
	virtual BOOL SyncFrom(PCTXT pPath)				= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Mail Service Interface
//

interface IServiceMail
{
	virtual BOOL SendFile(UINT  uIndex, CFilename Name, PCTXT pSubject, DWORD dwFlags)   = 0;
	virtual BOOL SendFile(UINT  uIndex, CFilename Name)				     = 0;
	virtual BOOL SendFile(CString Rcpt, CFilename Name)				     = 0;
	virtual BOOL SendFile(CString Rcpt, CFilename Name, DWORD dwAck)		     = 0;
	virtual BOOL SendMail(UINT  uIndex, PCTXT pSubject, PCTXT pMessage)		     = 0;
	virtual BOOL SendMail(CString Rect, PCTXT pSubject, PCTXT pMessage)		     = 0;
	virtual BOOL SendMail(CString Rect, PCTXT pSubject, PCTXT pMessage, DWORD dwAck)     = 0;
	virtual BOOL SendMail(UINT  uIndex, PCTXT pSubject, PCTXT pMessage, DWORD dwDone)    = 0;
	virtual BOOL SendMail(UINT  uIndex, PCTXT pType, DWORD dwTime, CUnicode const &Text) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// DNS Resolver Interface
//

interface IServiceDNSResolver
{
	virtual void AddFace(UINT uFace, UINT uMode, UINT DNS1, UINT DNS2) = 0;
	virtual BOOL Resolve(PCTXT pHost, CIPAddr &Addr)		   = 0;
	virtual BOOL Resolve(PCTXT pHost, CArray <CIPAddr> &List)	   = 0;
	virtual void ShowCache(void)					   = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Sync Interface
//

interface IServiceSqlSync
{
	virtual void Force(void)	     = 0;
	virtual BOOL IsRunning(void)	     = 0;
	virtual UINT GetSyncTime(UINT uType) = 0;
	virtual UINT GetLastStatus(void)     = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Instatiators
//

extern IService * Create_ServiceFileSync       (void);
extern IService * Create_ServiceMail           (void);
extern IService * Create_ServiceSqlSync        (void);
extern IService * Create_ServiceOpcUa	       (void);
extern IService * Create_CloudServiceCumulocity(void);
extern IService * Create_CloudServiceAws       (void);
extern IService * Create_CloudServiceSparkplug (void);
extern IService * Create_CloudServiceAzure     (void);
extern IService * Create_CloudServiceGeneric   (void);
extern IService * Create_CloudServiceGoogle    (void);
extern IService * Create_CloudServiceUbidots(void);

//////////////////////////////////////////////////////////////////////////
//
// Instance Pointers
//

extern IServiceFileSync    * g_pServiceFileSync;
extern IServiceMail        * g_pServiceMail;
extern IServiceSqlSync     * g_pServiceSqlSync;

//////////////////////////////////////////////////////////////////////////
//
// Services Collection
//

class CServices : public CItem
{
	public:
		// Constructor
		CServices(void);

		// Destructor
		~CServices(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Task List
		void GetTaskList(CTaskList &List);

		// Mail Handler
		void OnMail(PCTXT pNum, PCTXT pMsg);

		// Item Properties
		UINT        m_uCount;
		IService ** m_ppServ;
	};

//////////////////////////////////////////////////////////////////////////
//
// Service Base Class
//

class CServiceItem : public IService, public CCodedHost
{
	public:
		// Constructor
		CServiceItem(void);

		// Destructor
		~CServiceItem(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IService
		void LoadService(PCBYTE &pData);
		UINT GetID      (void);
		void GetTaskList(CTaskList &List);

	protected:
		// Data Members
		ULONG m_uRefs;
	};

// End of File

#endif
