
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SQL_HPP
	
#define	INCLUDE_SQL_HPP

#include "SqlClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CSqlManager;
class CSqlQueryList;
class CSqlQuery;
class CSqlColDef;
class CSqlRecord;
class CSqlFilter;

//////////////////////////////////////////////////////////////////////////
//
// SQL Column Definition
//

class CSqlColDef : public CItem
{	
	public:
		// Constructor
		CSqlColDef(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Records
		BOOL UpdateRecords(CResultSet const &ResultSet);

		// Data Members
		CString       m_Name;
		UINT          m_uCol;
		UINT          m_uType;
		UINT          m_uRecords;
		BOOL          m_fArray;
		CSqlRecord ** m_ppRecords;

	protected:

		// Implementation
		void LoadRecords(PCBYTE &pData);
		UINT GetTypeFromSql(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Record
//

class CSqlRecord : public CCodedHost
{
	public:
		// Constructor
		CSqlRecord(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		CCodedItem   * m_pValue;
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Query List
//

class CSqlQueryList : public CItem
{
	public:
		// Constructor
		CSqlQueryList(void);

		// Destructor
		~CSqlQueryList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		UINT	     m_uCount;
		CSqlQuery ** m_ppQuery;

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Query
//

class CSqlQuery : public CItem
{
	public:
		// Constructor
		CSqlQuery(UINT uPos);

		// Destructor
		~CSqlQuery(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		BOOL    NeedsToExecute(void);
		void    Touch(void);
		BOOL    UpdateTagData(CResultSet const &Results);
		void    Force(void);
		UINT    GetStatus(void);
		UINT    GetLastRun(void);
		CString BuildSql(CString const &Database);

		// Query Status
		enum {
			sqlQueryPending = 0,
			sqlQuerySuccess = 1,
			sqlQueryPartial = 2,
			sqlQueryFailed  = 3
			};

		void SetStatus(UINT uStatus);
		BOOL IsBroken(void);

		// Data Members
		UINT            m_Enable;
		CString		m_Name;
		CString		m_Table;
		CString         m_Condition;
		UINT            m_SortMode;
		UINT            m_SortColumn;
		CString         m_SQL;
		CString         m_Schema;
		CSqlColDef   ** m_ppColumns;
		CSqlFilter   ** m_ppFilters;
		WORD            m_wFilters;
		BOOL            m_fBroken;

	protected:
		// Data Members
		IMutex     * m_pLock;
		BOOL         m_fReady;
		UINT         m_uPos;
		UINT         m_uColumns;
		UINT         m_uRows;
		UINT         m_uUpdate;
		UINT         m_uLastTicks;
		UINT         m_uLastRun;
		UINT         m_uStatus;

		// Loading Helpers
		void LoadColumns(PCBYTE &pData);
		void LoadFilters(PCBYTE &pData);

		// Format Helpers
		CString FormatDatetime(UINT uTime);
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter
//

class CSqlFilter : public CCodedHost
{
	public:
		// Constructors
		CSqlFilter(void);

		// Helpers
		BOOL    NeedsOperand(void);
		CString GetOperator(void);
		CString GetBinding(void);

		void Load(PCBYTE &pData);

		enum {
			opEqual,
			opNotEqual,
			opLess,
			opLessEqual,
			opGreater,
			opGreaterEqual,
			opLike,
			opNull,
			opNotNull
			};

		enum {
			bindAnd,
			bindOr
			};

		// Properties
		CCodedItem * m_pValue;
		UINT         m_Column;

	protected:

		// Data Members
		UINT         m_Operator;
		UINT         m_Binding;
	};

// End of File

#endif
