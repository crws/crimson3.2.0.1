  
#include "intern.hpp"

#include "servftpc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// FTP Client Service
//

// Path Separators

static char const sepLocal  = '\\';

static char const sepRemote = '/';

// Buffer Sizes

static UINT const buffMisc = 64;

static UINT const buffPath = 256;

// Time Constants

static UINT const timeSockTimeout = 30000;

static UINT const timeListTimeout = 60000;

static UINT const timeListReload  = 30000;

static UINT const timeSyncBackoff = 10000;

static UINT const timeSyncCheck   = 500;

static UINT const timePollDelay   = 10;

// Instantiator

IService * Create_ServiceFileSync(void)
{
	return New CFileSync;
	}

// Constructor

CFileSync::CFileSync(void)
{
	m_pReady           = NULL;
	m_pLock		   = NULL;
	m_pEnable          = NULL;
	m_pLogSync         = NULL;
	m_pFtpServer       = NULL;
	m_pFtpPort         = NULL;
	m_pFtpSSL          = NULL;
	m_pUser            = NULL;
	m_pPass            = NULL;
	m_pPassive         = NULL;
	m_pPerFile         = NULL;
	m_pTestSize        = NULL;
	m_pKeep	           = NULL;
	m_pLogFile         = NULL;
	m_pFtpBase         = NULL;
	m_pFreq            = NULL;
	m_pFreqMins        = NULL;
	m_pDelay           = NULL;
	g_pServiceFileSync = this;
	}

// Destructor

CFileSync::~CFileSync(void)
{
	delete m_pEnable;
	delete m_pLogSync;
	delete m_pFtpServer;
	delete m_pFtpPort;
	delete m_pFtpSSL;
	delete m_pUser;
	delete m_pPass;
	delete m_pPassive;
	delete m_pPerFile;
	delete m_pTestSize;
	delete m_pKeep;
	delete m_pLogFile;
	delete m_pFtpBase;
	delete m_pFreq;
	delete m_pFreqMins;
	delete m_pDelay;
	delete m_pDrive;

	g_pServiceFileSync = NULL;
	}

// Initialization

void CFileSync::Load(PCBYTE &pData)
{
	ValidateLoad("CFileSync", pData);

	m_fDebug = GetByte(pData);

	GetCoded(pData, m_pEnable);
	GetCoded(pData, m_pLogSync);
	GetCoded(pData, m_pFtpServer);
	GetCoded(pData, m_pFtpPort);
	GetCoded(pData, m_pFtpSSL);
	GetCoded(pData, m_pUser);
	GetCoded(pData, m_pPass);
	GetCoded(pData, m_pPassive);
	GetCoded(pData, m_pPerFile);
	GetCoded(pData, m_pTestSize);
	GetCoded(pData, m_pKeep);
	GetCoded(pData, m_pLogFile);
	GetCoded(pData, m_pFtpBase);
	GetCoded(pData, m_pFreq);
	GetCoded(pData, m_pFreqMins);
	GetCoded(pData, m_pDelay);
	GetCoded(pData, m_pDrive);
	}

// IService

UINT CFileSync::GetID(void)
{
	return 1;
	}

void CFileSync::GetTaskList(CTaskList &List)
{
	m_Cfg.m_fEnable = GetItemData(m_pEnable, C3INT(0));

	if( m_Cfg.m_fEnable ) {

		CTaskDef Task;

		Task.m_Name   = "SYNC";
		Task.m_pEntry = this;
		Task.m_uID    = 0;
		Task.m_uCount = 1;
 		Task.m_uLevel = 1400;
		Task.m_uStack = 4096;

		List.Append(Task);
		}
	}

// ITaskEntry

void CFileSync::TaskInit(UINT uID)
{
	m_pDataSock = NULL;

	m_pRemList  = NULL;

	m_pLocList  = NULL;

	m_uLinger   = 0;

	m_pLock     = Create_Mutex();

	m_pReady    = Create_ManualEvent();
	}

void CFileSync::TaskExec(UINT uID)
{
	FindConfig(m_Cfg);

	m_pReady->Set();

	if( m_Cfg.m_fLogSync ) {

		do {
			Sleep(timeSyncBackoff);

			} while( !Sync() );

		BOOL fPrev = FALSE;

		for(;;) {

			UINT uTime  = GetNow();

			UINT uFreq  = m_Cfg.m_uFreqM * 60 + m_Cfg.m_uFreqH * 60 * 60;

			UINT uDelay = m_Cfg.m_uDelay * 60;

			if( uFreq ) {

				BOOL fTest = (uTime % uFreq > uDelay);

				if( fTest && !fPrev ) {

					while( !Sync() ) {

						Sleep(timeSyncBackoff);
						}
					}

				fPrev = fTest;

				PollLinger();
				}

			Sleep(timeSyncCheck);
			}
		}
	else {
		for(;;) {

			PollLinger();

			Sleep(timeSyncCheck);
			}
		}
	}

void CFileSync::TaskStop(UINT uID)
{
	}

void CFileSync::TaskTerm(UINT uID)
{
	if( m_pLock ) {

		m_pLock->Release();
		}

	if( m_pReady ) {

		m_pReady->Release();
		}
	}

// IServiceFileSync

BOOL CFileSync::Sync(void)
{
	if( m_pLock && m_pLock->Wait(FOREVER) ) {

		if( SyncFrom("\\LOGS") && SyncFrom("\\BATCH") ) {

			if( !m_Cfg.m_uKeep ) {

				SessionDone();
				}

			m_pLock->Free();

			return TRUE;
			}

		if( !m_Cfg.m_uKeep ) {

			SessionDone();
			}

		m_pLock->Free();
		}

	return FALSE;
	}

BOOL CFileSync::SyncFrom(PCTXT pPath)
{
	char cDrive = 'C' + m_Cfg.m_uDrive;

	CPrintf FullPath("%c:%s", cDrive, pPath);

	if( !chdir(FullPath) ) {

		CAutoDirentList List;

		if( List.ScanDirs(".") ) {

			for( UINT n = 0; n < List.GetCount(); n++ ) {

				char sName[buffPath];

				PathJoin(sName, pPath, List[n]->d_name);

				if( !SyncFrom(sName) ) {

					return FALSE;
					}
				}

			return TRUE;
			}
		else {
			m_LocPath = FullPath;

			m_RemPath = m_Cfg.m_FtpBase;

			m_RemPath += pPath;

			for( UINT n = 0; m_RemPath[n]; n++ ) {

				if( m_RemPath[n] == sepLocal ) {

					m_RemPath.SetAt(n, sepRemote);
					}
				}

			if( m_RemPath[0] != sepRemote ) {

				m_RemPath.Insert(0, sepRemote);
				}

			m_uMode = modeSync;

			return PerformSync(TRUE);
			}
		}

	return TRUE;
	}

BOOL CFileSync::PutFile(PCTXT pLocal, PCTXT pRemote, BOOL fDelete)
{
	if( m_pReady && m_pReady->Wait(FOREVER) ) {

		if( Parse(sepLocal, pLocal, m_LocPath, m_LocName) ) {

			if( Parse(sepRemote, pRemote, m_RemPath, m_RemName) ) {

				if( CAutoFile(pLocal, "r") ) {

					CAutoLock Lock(m_pLock);

					m_uMode = modeSend;

					if( PerformSync(m_Cfg.m_uKeep > 0) ) {

						if( fDelete ) {

							unlink(pLocal);
							}

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL CFileSync::GetFile(PCTXT pLocal, PCTXT pRemote, BOOL fDelete)
{
	if( m_pReady && m_pReady->Wait(FOREVER) ) {

		if( Parse(sepLocal, pLocal, m_LocPath, m_LocName) ) {

			if( Parse(sepRemote, pRemote, m_RemPath, m_RemName) ) {

				CAutoLock Lock(m_pLock);

				m_uMode = fDelete ? modeMove : modeRetr;

				if( PerformSync(m_Cfg.m_uKeep > 0) ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Implementation

void CFileSync::FindConfig(CConfig &Cfg)
{
	Cfg.m_fLogSync  = GetItemData(m_pLogSync,   C3INT(0)   );
	Cfg.m_FtpServer = GetItemAddr(m_pFtpServer, DWORD(0)   );
	Cfg.m_FtpPort   = GetItemData(m_pFtpPort,   C3INT(0)   );
	Cfg.m_uSSL      = GetItemData(m_pFtpSSL,    C3INT(0)   );
	Cfg.m_User      = GetItemData(m_pUser,      "anonymous");
	Cfg.m_Pass      = GetItemData(m_pPass,      ""         );
	Cfg.m_fPassive  = GetItemData(m_pPassive,   C3INT(0)   );
	Cfg.m_fPerFile  = GetItemData(m_pPerFile,   C3INT(0)   );
	Cfg.m_fTestSize = GetItemData(m_pTestSize,  C3INT(1)   );
	Cfg.m_uKeep     = GetItemData(m_pKeep,      C3INT(0)   );
	Cfg.m_fLogFile  = GetItemData(m_pLogFile,   C3INT(0)   );
	Cfg.m_FtpBase   = GetItemData(m_pFtpBase,   ""         );
	Cfg.m_uFreqH    = GetItemData(m_pFreq,      C3INT(0)   );
	Cfg.m_uFreqM    = GetItemData(m_pFreqMins,  C3INT(0)   );
	Cfg.m_uDelay    = GetItemData(m_pDelay,     C3INT(0)   );
	Cfg.m_uDrive    = GetItemData(m_pDrive,     C3INT(0)   );

	m_LogFile       = Cfg.m_fLogFile ? "\\FTP.LOG" : "";
	}

// Configuration

UINT CFileSync::GetCmdPort(void)
{
	return m_Cfg.m_FtpPort;
	}

DWORD CFileSync::GetServer(void)
{
	if( m_pFtpServer ) {

		if( !m_pFtpServer->IsConst() ) {

			m_Cfg.m_FtpServer = GetItemAddr(m_pFtpServer, DWORD(0));
			}
		}

	return m_Cfg.m_FtpServer;
	}

// Sync Handler

BOOL CFileSync::PerformSync(BOOL fKeep)
{
	if( OpenCmdSocket(FALSE, m_Cfg.m_uSSL == 1) ) {

		if( MakeLocList() ) {

			UINT uTarget = fKeep ? stateQuit : stateDone;

			while( m_uState != stateIdle && m_uState != uTarget ) {

				switch( m_uState ) {
			
					case stateConnect:	HandleConnect();	break;
					case stateSSL:		HandleSSL();		break;
					case stateUser:		HandleUser();		break;
					case statePass:		HandlePass();		break;
					case statePBSZ:		HandlePBSZ();		break;
					case stateProt:		HandleProt();		break;
					case stateType:		HandleType();		break;
					case stateStru:		HandleStru();		break;
					case stateMode:		HandleMode();		break;
					case stateChDir:	HandleChDir();		break;
					case stateMkDir:	HandleMkDir();		break;
					case stateListPort:	HandleListPort();	break;
					case stateListData:	HandleListData();	break;
					case stateSendPort:	HandleSendPort();	break;
					case stateSendData:	HandleSendData();	break;
					case stateSizePort:	HandleSizePort();	break;
					case stateSizeData:	HandleSizeData();	break;
					case stateRetrPort:	HandleRetrPort();	break;
					case stateRetrData:	HandleRetrData();	break;
					case stateDelete:	HandleDelete();		break;
					case stateQuit:		HandleQuit();		break;
					}
				}

			if( !fKeep || m_uState != uTarget ) {

				AbortDataSocket();

				CloseCmdSocket();

				return m_uState == uTarget;
				}
			else {
				m_uLinger = GetTickCount();

				m_uState  = stateChDir;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CFileSync::SessionDone(void)
{
	if( m_pCmdSock ) {

		HandleQuit();

		AbortDataSocket();

		CloseCmdSocket();
		}
	}

void CFileSync::PollLinger(void)
{
	if( m_uLinger ) {

		if( m_pLock->Wait(FOREVER) ) {

			UINT uTime = GetTickCount();

			UINT uGone = uTime - m_uLinger;

			if( uGone >= m_Cfg.m_uKeep * ToTicks(1000) ) {

				SessionDone();

				m_uLinger = 0;
				}

			m_pLock->Free();
			}
		}
	}

// State Handlers

void CFileSync::HandleConnect(void)
{
	if( RecvReply() ) {

		switch( m_uReplyCode ) {

			case 220:
				m_uState = (m_Cfg.m_uSSL == 2) ? stateSSL : stateUser;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleSSL(void)
{
	if( SendSSL() && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 234:
				m_uState = SwitchCmdSocket() ? stateUser : stateIdle;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleUser(void)
{
	if( SendUser(m_Cfg.m_User) && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 202:
			case 230:
				m_uState = (m_Cfg.m_uSSL == 2) ? statePBSZ : stateType;
				break;

			case 331:
				m_uState = statePass;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandlePass(void)
{
	if( SendPass(m_Cfg.m_Pass) && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 202:
			case 230:
				m_uState = (m_Cfg.m_uSSL == 2) ? statePBSZ : stateType;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandlePBSZ(void)
{
	if( SendPBSZ() && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 200:
				m_uState = stateProt;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleProt(void)
{
	if( SendProt() && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 200:
				m_uState = stateType;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleType(void)
{
	if( SendType("A") && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 200:
			case 202:
				m_uState = stateStru;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleStru(void)
{
	if( SendStru() && RecvReply() ) {

		switch( m_uReplyCode ) {

			// NOTE -- We accept 500 as some broken servers
			// may send this code even though the RFC demands
			// that they accept the command.

			case 200:
			case 202:
			case 250:
			case 500:
				m_uState = stateMode;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleMode(void)
{
	if( SendMode() && RecvReply() ) {

		switch( m_uReplyCode ) {

			// NOTE -- We accept 500 as some broken servers
			// may send this code even though the RFC demands
			// that they accept the command.

			case 200:
			case 202:
			case 500:
				m_uState = stateChDir;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleChDir(void)
{
	char sPath[buffPath];

	strcpy(sPath, PCTXT(m_RemPath));

	if( strcmp(sPath, "/") ) {

		strcat(sPath, "/");
		}

	if( SendChDir(sPath) && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 550:
				SwitchToMkDir();
				break;

			case 250:
				SwitchToTransfer();
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleMkDir(void)
{
	char sPath[buffPath];

	PTXT pFind = sPath;

	strcpy(sPath, PCTXT(m_RemPath));

	strcat(sPath, "/");

	for( UINT n = 0; n < m_uIndex; n++ ) {

		if( !(pFind = strchr(pFind, sepRemote)) ) {

			SwitchToTransfer();

			return;
			}

		pFind++;
		}

	pFind[0] = 0;

	if( SendMkDir(sPath) && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 550:
			case 250:
			case 257:
				m_uIndex = m_uIndex + 1;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleListPort(void)
{
	if( m_uIndex < m_uLocCount ) {

		HandlePort(stateListData, FALSE);

		return;
		}

	m_uState = stateQuit;
	}

void CFileSync::HandleListData(void)
{
	char sPath[buffPath];

	if( m_Cfg.m_fPerFile ) {

		CString const &Name = m_pLocList[m_uIndex];

		strcpy(m_LocFile.sName, Name);

		strcpy(m_RemFile.sName, Name);

		strcpy(sPath, PCTXT(m_RemPath));

		strcat(sPath, "/");

		strcat(sPath, m_RemFile.sName);
		}
	else {
		strcpy(sPath, PCTXT(m_RemPath));

		strcat(sPath, "/*");
		}

	if( SendList(sPath) && RecvReply() ) {

		if( m_uReplyCode == 550 || m_uReplyCode == 226 ) {

			// NOTE -- We accept 226 as some broken servers send
			// this code to indicate that the file list is empty
			// and that they have no data to send.

			ClearRemList();

			CloseDataSocket();

			m_uState = stateSendPort;

			return;
			}

		if( IsConnection() ) {

			if( WaitDataSocket() ) {

				if( ReadListOutput() ) {

					CloseDataSocket();

					if( RecvReply() ) {
						
						switch( m_uReplyCode ) {
							
							case 550:
							case 226:

								m_uState = stateSendPort;

								return;
							}
						}
					}
				}
			}
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleSendPort(void)
{
	if( m_uIndex < m_uLocCount ) {

		if( m_uMode == modeSync ) {

			CString const &Name = m_pLocList[m_uIndex];

			if( !m_Cfg.m_fPerFile ) {

				strcpy(m_LocFile.sName, Name);

				strcpy(m_RemFile.sName, Name);
				}

			PVOID p = bsearch( &m_RemFile,
					   m_pRemList,
					   m_uRemCount,
					   sizeof(CFileInfo),
					   RemSortFunc
					   );

			if( p ) {

				CFileInfo *pRem = (CFileInfo *) p;

				char sFull[MAX_PATH];

				PathJoin(sFull, m_LocPath, Name);

				CAutoFile File(sFull, "r");

				if( File && pRem->uSize >= File.GetSize() ) {

					SwitchToNextFile();

					return;
					}
				}

			HandlePort(stateSendData, TRUE);
			}
		else {
			strcpy(m_LocFile.sName, m_LocName);

			strcpy(m_RemFile.sName, m_RemName);

			HandlePort(stateSendData, TRUE);
			}

		return;
		}

	m_uState = stateQuit;
	}

void CFileSync::HandleSendData(void)
{
	if( SendStor(m_RemPath, m_RemFile.sName) && RecvReply() ) {

		if( m_uReplyCode == 550 ) {

			CloseDataSocket();

			SwitchToNextFile();

			return;
			}

		if( IsConnection() ) {

			if( WaitDataSocket() ) {

				if( SendFileData() ) {

					CloseDataSocket();

					if( RecvReply() ) {
						
						switch( m_uReplyCode ) {
							
							case 226:

								SwitchToNextFile();

								return;
							}
						}
					}
				}
			}
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleSizePort(void)
{
	if( m_uIndex == m_uLocCount ) {

		if( m_uMode == modeMove ) {

			m_uState = stateDelete;

			return;
			}

		m_uState = stateQuit;

		return;
		}

	HandlePort(stateSizeData, FALSE);
	}

void CFileSync::HandleSizeData(void)
{
	if( SendList(m_RemName) && RecvReply() ) {

		if( IsConnection() ) {

			if( WaitDataSocket() ) {

				if( ReadListOutput() ) {

					CloseDataSocket();

					if( RecvReply() ) {
						
						switch( m_uReplyCode ) {
							
							case 550:
							case 226:

								if( m_uRemCount == 1 ) {

									if( m_pRemList[0].uSize ) {

										m_uState = stateRetrPort;

										return;
										}
									}
								break;
							}
						}
					}
				}
			}
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleRetrPort(void)
{
	if( m_uIndex == m_uLocCount ) {

		if( m_uMode == modeMove ) {

			m_uState = stateDelete;

			return;
			}

		m_uState = stateQuit;

		return;
		}

	strcpy(m_LocFile.sName, m_LocName);

	strcpy(m_RemFile.sName, m_RemName);

	strlwr(m_LocFile.sName);

	strlwr(m_RemFile.sName);

	HandlePort(stateRetrData, FALSE);
	}

void CFileSync::HandleRetrData(void)
{
	if( SendRetr(m_RemPath, m_RemFile.sName) && RecvReply() ) {

		if( IsConnection() ) {

			if( WaitDataSocket() ) {

				if( RecvFileData() ) {

					CloseDataSocket();

					if( RecvReply() ) {
						
						switch( m_uReplyCode ) {
							
							case 226:

								m_uIndex = m_uIndex + 1;

								m_uState = m_Cfg.m_fTestSize ? stateSizePort : stateRetrPort;

								return;
							}
						}
					}
				}
			}
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleDelete(void)
{
	if( SendDele(m_RemPath, m_RemFile.sName) && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 250:
				m_uState = stateQuit;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

void CFileSync::HandleQuit(void)
{
	if( SendQuit() && RecvReply() ) {

		switch( m_uReplyCode ) {

			case 221:
				m_uState = stateDone;
				break;

			default:
				m_uState = stateIdle;
				break;
			}

		return;
		}

	m_uState = stateIdle;
	}

// State Transitions

void CFileSync::SwitchToMkDir(void)
{
	m_uIndex = 2;

	m_uState = stateMkDir;
	}

void CFileSync::SwitchToTransfer(void)
{
	switch( m_uMode ) {

		case modeSync:

			m_uIndex = 0;
			
			m_uState = stateListPort;
			
			break;

		case modeSend:

			m_uIndex = 0;

			m_uState = stateSendPort;

			break;

		case modeRetr:
		case modeMove:

			m_uIndex = 0;

			m_uState = m_Cfg.m_fTestSize ? stateSizePort : stateRetrPort;

			break;
		}
	}

void CFileSync::SwitchToNextFile(void)
{
	if( m_Cfg.m_fPerFile ) {

		m_uIndex = m_uIndex + 1;

		m_uState = stateListPort;
		}
	else {
		m_uIndex = m_uIndex + 1;

		m_uState = stateSendPort;
		}
	}

// Response Check

BOOL CFileSync::IsConnection(void)
{
	// NOTE -- We ought to accept only one of these based
	// on whether we're active or passive, but there are
	// broken FTP servers out there that send 150 when they
	// ought to be sending 125. We therefore take both.

	switch( m_uReplyCode ) {
		
		case 125:
		case 150:

			return TRUE;
		}

	return FALSE;
	}

// Port Helper

BOOL CFileSync::HandlePort(UINT uState, BOOL fSend)
{
	if( m_Cfg.m_fPassive ) {

		if( SendPasv() && RecvReply() ) {

			if( m_uReplyCode == 227 ) {

				UINT uPort = ParsePort();

				Log("<<< Port is %u\n", uPort);

				if( uPort ) {

					if( OpenDataSocket(uPort, fSend, m_Cfg.m_uSSL ? TRUE : FALSE) ) {

						m_uState = uState;

						return TRUE;
						}
					}
				}
			}
		}
	else {
		if( SendPort(fSend) && RecvReply() ) {

			if( m_uReplyCode == 200 ) {

				m_uState = uState;

				return TRUE;
				}
			}
		}

	m_uState = stateIdle;

	return FALSE;
	}

// Commands

BOOL CFileSync::SendSSL(void)
{
	return Send("AUTH TLS\r\n");
	}

BOOL CFileSync::SendPBSZ(void)
{
	return Send("PBSZ 0\r\n");
	}

BOOL CFileSync::SendProt(void)
{
	return Send("PROT P\r\n");
	}

BOOL CFileSync::SendUser(PCTXT pUser)
{
	char s[buffMisc];

	SPrintf(s, "USER %s\r\n", pUser);

	return Send(s);
	}

BOOL CFileSync::SendPass(PCTXT pPass)
{
	char s[buffMisc];

	SPrintf(s, "PASS %s\r\n", pPass);

	return Send(s);
	}

BOOL CFileSync::SendType(PCTXT pType)
{
	char s[buffMisc];

	SPrintf(s, "TYPE %s\r\n", pType);

	return Send(s);
	}

BOOL CFileSync::SendStru(void)
{
	return Send("STRU F\r\n");
	}

BOOL CFileSync::SendMode(void)
{
	return Send("MODE S\r\n");
	}

BOOL CFileSync::SendMkDir(PCTXT pPath)
{
	char s[buffPath];

	SPrintf(s, "MKD %s\r\n", pPath);

	return Send(s);
	}

BOOL CFileSync::SendChDir(PCTXT pPath)
{
	char s[buffPath];

	SPrintf(s, "CWD %s\r\n", pPath);

	return Send(s);
	}

BOOL CFileSync::SendPort(BOOL fSend)
{
	if( OpenDataSocket(0, fSend, FALSE) ) {

		char s[buffMisc];

		DWORD Addr;

		WORD  Port;

		m_pDataSock->GetLocal((IPADDR &) Addr, Port);

		m_pCmdSock ->GetLocal((IPADDR &) Addr);

		SPrintf( s,
			 "PORT %u,%u,%u,%u,%u,%u\r\n",
			 PBYTE(&Addr)[0],
			 PBYTE(&Addr)[1],
			 PBYTE(&Addr)[2],
			 PBYTE(&Addr)[3],
			 HIBYTE(Port),
			 LOBYTE(Port)
			 );

		return Send(s);
		}

	return FALSE;
	}

BOOL CFileSync::SendPasv(void)
{
	return Send("PASV\r\n");
	}

BOOL CFileSync::SendList(PCTXT pPath)
{
	char s[buffPath];

	SPrintf(s, "LIST %s\r\n", pPath);

	return Send(s);
	}

BOOL CFileSync::SendStor(PCTXT pPath, PCTXT pFile)
{
	char s[buffPath];

	if( !strcmp(pPath, "/") ) pPath++;

	SPrintf(s, "STOR %s/%s\r\n", pPath, pFile);

	return Send(s);
	}

BOOL CFileSync::SendRetr(PCTXT pPath, PCTXT pFile)
{
	char s[buffPath];

	if( !strcmp(pPath, "/") ) pPath++;

	SPrintf(s, "RETR %s/%s\r\n", pPath, pFile);

	return Send(s);
	}

BOOL CFileSync::SendDele(PCTXT pPath, PCTXT pFile)
{
	char s[buffPath];

	if( !strcmp(pPath, "/") ) pPath++;

	SPrintf(s, "DELE %s/%s\r\n", pPath, pFile);

	return Send(s);
	}

BOOL CFileSync::SendQuit(void)
{
	return Send("QUIT\r\n");
	}

// Socket Management

BOOL CFileSync::OpenDataSocket(UINT uPort, BOOL fSend, BOOL fSSL)
{
	if( fSSL && !m_pTls ) {

		if( !uPort || !CreateTlsContext() ) {

			return FALSE;
			}
		}

	if( fSSL && m_pTls ) {

		m_pDataSock = m_pTls->CreateSocket(NULL, NULL, 0);
		}
	else {
		m_pDataSock = NULL;

		AfxNewObject("sock-tcp", ISocket, m_pDataSock);
	}

	if( m_pDataSock ) {

		if( fSend ) {

			UINT uOption = OPT_SEND_QUEUE;

			UINT uValue  = 255;

			m_pDataSock->SetOption(uOption, uValue);
			}
		else {
			UINT uOption = OPT_RECV_QUEUE;

			UINT uValue  = 255;

			m_pDataSock->SetOption(uOption, uValue);
			}

		if( uPort ) {

			DWORD IP = GetServer();

			if( m_pDataSock->Connect(IPREF(IP), uPort) == S_OK ) {

				return TRUE;
				}
			else {
				AbortDataSocket();

				Log("*** Data socket failed to connect\n");

				return FALSE;
				}
			}
		else {
			if( m_pDataSock->Listen(0) == S_OK ) {

				return TRUE;
				}
			else {
				AbortDataSocket();

				Log("*** Data socket failed to listen\n");

				return FALSE;
				}
			}
		}

	Log("*** No data socket available\n");

	return FALSE;
	}

BOOL CFileSync::WaitDataSocket(void)
{
	SetTimer(timeSockTimeout);

	while( GetTimer() ) {

		UINT Phase;

		m_pDataSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {

			return TRUE;
			}

		if( Phase == PHASE_CLOSING ) {

			return TRUE;
			}

		if( Phase == PHASE_ERROR ) {

			break;
			}

		Sleep(timePollDelay);
		}

	Log("*** Data socket timeout\n");

	return FALSE;
	}

void CFileSync::CloseDataSocket(void)
{
	if( m_pDataSock ) {

		m_pDataSock->Close();

		m_pDataSock->Release();

		m_pDataSock = NULL;
		}
	}

void CFileSync::AbortDataSocket(void)
{
	if( m_pDataSock ) {

		m_pDataSock->Abort();

		m_pDataSock->Release();

		m_pDataSock = NULL;
		}
	}

// Directory Parsing

BOOL CFileSync::ReadListOutput(void)
{
	ClearRemList();

	CBuffer *pBuff = NULL;

	SetTimer(timeListTimeout);
	
	while( GetTimer() ) {

		if( m_pDataSock->Recv(pBuff) == S_OK ) {

			PTXT pText = PTXT(pBuff->GetData());

			UINT uSize = pBuff->GetSize();

			while( uSize-- ) {
				
				ParseRemList(*pText++);
				}

			BuffRelease(pBuff);

			SetTimer(timeListReload);
			}
		else {
			UINT uPhase = 0;

			m_pDataSock->GetPhase(uPhase);

			if( uPhase == PHASE_OPEN ) {

				Sleep(timePollDelay);

				continue;
				}

			if( uPhase == PHASE_CLOSING ) {

				SortRemList();

				return TRUE;
				}

			break;
			}
		}

	Log("*** List output timeout\n");

	return FALSE;
	}

void CFileSync::ClearRemList(void)
{
	delete [] m_pRemList;

	m_uRemCount = 0;

	m_uRemAlloc = 0;

	m_pRemList  = NULL;

	m_uRemPtr   = 0;
	}

void CFileSync::GrowRemList(void)
{
	if( m_uRemCount == m_uRemAlloc ) {

		UINT       uAlloc = m_uRemAlloc + 32;

		CFileInfo *pList  = New CFileInfo [ uAlloc ];

		if( m_pRemList ) {

			UINT uSize = m_uRemAlloc * sizeof(CFileInfo);

			memcpy(pList, m_pRemList, uSize);

			delete [] m_pRemList;
			}

		m_pRemList  = pList;

		m_uRemAlloc = uAlloc;
		}
	}

void CFileSync::ParseRemList(char cData)
{
	if( cData == '\n' ) {

		m_sRemLine[m_uRemPtr] = 0;

		m_uRemPtr = 0;

		ParseRemLine();

		return;
		}

	if( isprint(cData) ) {

		if( m_uRemPtr < sizeof(m_sRemLine) - 1 ) {

			m_sRemLine[m_uRemPtr] = cData;

			m_uRemPtr += 1;
			}
		}
	}

void CFileSync::ParseRemLine(void)
{
	int n;

	for( n = 0; n < 6; n++ ) {

		if( !IsRightsChar(m_sRemLine[n]) ) {

			break;
			}
		}

	BOOL fUnix = (n == 6);

	UINT uSize = fUnix ? 4 : 2;

	UINT uName = fUnix ? 8 : 3;

	ParseRemLine(uSize, uName);
	}

void CFileSync::ParseRemLine(UINT uSize, UINT uName)
{
	PTXT pToken = strtok(m_sRemLine, " ");

	UINT uToken = 0;

	CFileInfo Info;

	while( pToken ) {

		if( uToken == uSize ) {

			Info.uSize = atoi(pToken);
			}

		if( uToken == uName ) {

			if( strlen(pToken) < sizeof(Info.sName) - 1 ) {

				strcpy(Info.sName, pToken);
				}
			else {
				uToken = 999;

				break;
				}
			}

		pToken = strtok(NULL, " ");

		uToken = uToken + 1;
		}

	if( uToken < 10 ) {

		GrowRemList();

		m_pRemList[m_uRemCount] = Info;

		m_uRemCount++;
		}
	}

void CFileSync::SortRemList(void)
{
	qsort(m_pRemList, m_uRemCount, sizeof(CFileInfo), RemSortFunc);
	}

BOOL CFileSync::IsRightsChar(char cData)
{
	switch( cData ) {

		case 'r':
		case 'w':
		case 'x':
		case '-':
			return TRUE;
		}

	return FALSE;
	}

// Directory Building

void CFileSync::ClearLocList(void)
{
	delete [] m_pLocList;

	m_pLocList  = NULL;

	m_uLocCount = 0;
	}

BOOL CFileSync::MakeLocList(void)
{
	ClearLocList();

	if( !chdir(m_LocPath) ) {

		if( m_uMode == modeSync ) {

			CAutoDirentList List;

			if( List.ScanFiles(".") ) {

				for( UINT uPass = 0; uPass < 2; uPass++ ) {

					m_uLocCount = 0;

					for( UINT uSlot = 0; uSlot < List.GetCount(); uSlot++ ) {

						CFilename Name(List[uSlot]->d_name);

						if( !stricmp(Name.GetType(), "csv") ) {

							if( uPass == 1 ) {

								m_pLocList[m_uLocCount] = Name;
								}

							m_uLocCount++;
							}
						}

					if( uPass == 0 ) {

						if( !m_uLocCount ) {

							return FALSE;
							}

						m_pLocList = New CString [ m_uLocCount ];
						}
					}

				SortLocList();

				return TRUE;
				}
			}

		if( m_uMode == modeSend ) {

			m_uLocCount   = 1;

			m_pLocList    = new CString [ m_uLocCount ];

			m_pLocList[0] = m_LocName;

			return TRUE;
			}

		if( m_uMode == modeRetr || m_uMode == modeMove ) {

			m_uLocCount = 1;

			m_pLocList  = NULL;

			return TRUE;
			}
		}

	return FALSE;
	}

void CFileSync::SortLocList(void)
{
	qsort(m_pLocList, m_uLocCount, sizeof(CString), LocSortFunc);
	}

// File Transmission

BOOL CFileSync::SendFileData(void)
{
	char sFull[MAX_PATH];

	PathJoin(sFull, m_LocPath, m_LocFile.sName);

	CAutoFile File(sFull, "r");

	if( File ) {

		if( SendFile(m_pDataSock, File) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CFileSync::RecvFileData(void)
{
	char sFull[MAX_PATH];

	PathJoin(sFull, m_LocPath, m_LocFile.sName);

	CAutoFile File(sFull, "r+", "w+");

	if( File ) {

		if( RecvFile(m_pDataSock, File) ) {

			File.Truncate();

			if( m_Cfg.m_fTestSize ) {

				UINT uLength = File.GetPos();

				UINT uTarget = m_pRemList[0].uSize;

				if( uLength - uTarget ) {

					Log("*** RETR file size is %u vs. %u\n", uLength, m_pRemList[0].uSize);

					File.Close();

					unlink(sFull);

					return FALSE;
					}
				}

			return TRUE;
			}

		if( m_Cfg.m_fTestSize ) {

			File.Close();

			unlink(sFull);
			}
		}

	return FALSE;
	}

// Sort Functions

int CFileSync::RemSortFunc(PCVOID p1, PCVOID p2)
{
	CFileInfo *f1 = (CFileInfo *) p1;
	
	CFileInfo *f2 = (CFileInfo *) p2;

	return stricmp(f1->sName, f2->sName);
	}

int CFileSync::LocSortFunc(PCVOID p1, PCVOID p2)
{
	CString const *s1 = (CString const *) p1;

	CString const *s2 = (CString const *) p2;

	return strcmp(*s1, *s2);
	}

// Name Parsing

BOOL CFileSync::Parse(char cSep, CString Full, CString &Path, CString &Name)
{
	UINT uPos = Full.FindRev(cSep);

	if( uPos == NOTHING ) {

		Path = CString(cSep, 1);

		Name = Full;
		}
	else {
		if( uPos == 0 ) {

			Path = CString(cSep, 1);

			Name = Full.Mid(1);
			}
		else {
			Path = Full.Left(uPos);

			Name = Full.Mid (uPos+1);
			}
		}

	return !Path.IsEmpty() && !Name.IsEmpty();
	}

// Port Parsing

UINT CFileSync::ParsePort(void)
{
	PTXT pRest = m_sReplyData + 5;

	PTXT pOpen = strchr(pRest, '(');

	if( pOpen ) {

		PTXT pDone = strchr(pOpen, ')');

		if( pDone ) {

			*pDone        = ',';

			PTXT pScan    = pOpen + 1;

			BYTE bData[6] = { 0 };

			for( UINT n = 0; pScan && n < 6; n++ ) {

				bData[n] = atoi(pScan);

				pScan = strchr(pScan, ',');

				pScan = pScan ? pScan + 1 : NULL;
				}

			if( pScan ) {

				UINT uPort = MAKEWORD(bData[5], bData[4]);

				return uPort;
				}
			}
		}

	return 0;
	}

// End of File
