
#include "Intern.hpp"

#include "Service.hpp"

#include "Matrix.hpp"

#include "MqttClientJson.hpp"

#include <sys/time.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudServiceCrimson.hpp"

#include "CloudTagSet.hpp"

#include "MqttClientOptionsJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson JSON MQTT Client
//

// Constructor

CMqttClientJson::CMqttClientJson(CCloudServiceCrimson *pService, CMqttClientOptionsJson &Opts) : CMqttClientCrimson(pService, Opts), m_Opts(Opts)
{
}

// Client Hooks

void CMqttClientJson::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseConnected ) {

		m_pService->SetStatus(2);
	}

	if( m_uPhase == phasePublishing ) {

		m_Opts.SetGoodPeer();

		m_pService->SetStatus(4);

		GoOnline();
	}

	CMqttClientCrimson::OnClientPhaseChange();
}

BOOL CMqttClientJson::OnClientDataSent(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode < m_PubList.GetCount() ) {

		ShowSentData(pMsg);
	}

	return CMqttClientCrimson::OnClientDataSent(pMsg);
}

// Publish Hook

BOOL CMqttClientJson::GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg)
{
	CCloudDataSet *pSet = m_pService->GetDataSet(uTopic);

	if( pSet->IsTriggered() ) {

		// LATER -- Can we avoid creating all these child
		// objects if the data hasn't actually changed?

		CMqttJsonData  Json;

		CMqttJsonData *pTop = &Json;

		CMqttJsonData *pRep = &Json;

		CMqttJsonData *pDes = NULL;

		if( !m_ObjTop.IsEmpty() ) {

			Json.AddChild(m_ObjTop, FALSE, pTop);

			pRep = pTop;
		}

		if( !m_ObjRep.IsEmpty() ) {

			pTop->AddChild(m_ObjRep, FALSE, pRep);
		}

		if( !m_ObjDes.IsEmpty() ) {

			pTop->AddChild(m_ObjDes, FALSE, pDes);
		}

		if( uMode == CCloudDataSet::modeDelta ) {

			uMode = m_Opts.m_Mode;
		}

		if( OnRead(pSet, uTime, fTemp, uMode, pRep, pDes) ) {

			pMsg = New CMqttMessage;

			pMsg->SetData(Json);

			return TRUE;
		}
	}

	return FALSE;
}

// Read Helper

BOOL CMqttClientJson::OnRead(CCloudDataSet *pSet, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttJsonData *pRep, CMqttJsonData *pDes)
{
	CString Site = m_pService->GetSiteIdent();

	if( pSet->GetJson(pRep, pDes, Site, uTime, m_Opts.m_Root, m_Opts.m_Code, uMode) ) {

		switch( m_Opts.m_Code ) {

			case 0:
				pRep->AddValue("connected", "true", jsonString);
				break;

			case 1:
				pRep->AddValue("connected", "true", jsonBool);
				break;

			case 2:
				pRep->AddValue("connected", "1", jsonNumber);
				break;
		}

		if( m_Opts.m_Root < 2 ) {

			struct timeval tv;

			tv.tv_sec  = UINT(uTime / 1000);

			tv.tv_usec = UINT(uTime % 1000 * 1000);

			switch( m_Opts.m_Code ) {

				case 0:
				case 1:
					pRep->AddValue("timestamp", CHttpTime::Format(2, &tv), jsonString);
					break;

				case 2:
					pRep->AddValue("timestamp", CPrintf("%u", tv.tv_sec), jsonNumber);
					break;
			}

			if( pSet->UseHistory() ) {

				bool fNorm = !fTemp;

				bool fLive = IsLive();

				switch( m_Opts.m_Code ) {

					case 0:
						pRep->AddValue("adhoc", fNorm ? "false" : "true", jsonString);
						pRep->AddValue("historic", fLive ? "false" : "true", jsonString);
						break;

					case 1:
						pRep->AddValue("adhoc", fNorm ? "false" : "true", jsonBool);
						pRep->AddValue("historic", fLive ? "false" : "true", jsonBool);
						break;

					case 2:
						pRep->AddValue("adhoc", fNorm ? "0" : "1", jsonNumber);
						pRep->AddValue("historic", fLive ? "0" : "1", jsonNumber);
						break;
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}

// Write Helper

BOOL CMqttClientJson::OnWrite(CMqttJsonData const &Json, CString Child)
{
	CMqttJsonData const *pState = &Json;

	if( !Child.IsEmpty() ) {

		pState = pState->GetChild(Child);
	}

	if( pState ) {

		CMqttJsonData const *pTags = m_Opts.m_Root ? pState : pState->GetChild("tags");

		if( pTags ) {

			CArray <CMqttJsonData const *> NodeStack;

			CArray <INDEX            > WalkStack;

			UINT     uDepth = 0;

			UINT	uDone   = 0;

			CString Current = ".$";

			INDEX	Index   = pTags->GetHead();

			NodeStack.Append(pTags);

			WalkStack.Append(NULL);

			for( ;;) {

				CMqttJsonData const *pNode = NodeStack[uDepth];

				if( pNode->Failed(Index) ) {

					if( uDepth ) {

						Index = WalkStack[uDepth];

						NodeStack.Remove(uDepth);

						WalkStack.Remove(uDepth);

						Current = Current.Mid(Current.FindRev('.')+1);

						uDepth--;
					}
					else
						break;
				}
				else {
					CMqttJsonData *pChild = pNode->GetChild(Index);

					if( pChild ) {

						Current += '.';

						Current += pNode->GetName(Index);

						pNode->GetNext(Index);

						NodeStack.Append(pChild);

						WalkStack.Append(Index);

						Index = pChild->GetHead();

						uDepth++;
					}
					else {
						if( pNode->GetType(Index) != jsonNull ) {

							// LATER -- This depends on the tree mode!

							CString Name = pNode->GetName(Index);

							CString Data = pNode->GetValue(Index);

							CString Base = Name.Mid(Name.FindRev('.')+1);

							CString Full = (Current + "." + Name).Mid(3);

							UINT    c    = m_pService->GetSetCount();

							for( UINT p = 0; p < m_PubList.GetCount(); p++ ) {

								UINT s = m_PubList[p].m_uTopic;

								if( s >= 1 && s < c ) {

									CCloudTagSet *pSet = (CCloudTagSet *) m_pService->GetDataSet(s);

									if( pSet->SetTagData(Full, Data) ) {

										uDone |= (1<<p);
									}
								}
							}
						}

						pNode->GetNext(Index);
					}
				}
			}

			for( UINT p = 0; p < m_PubList.GetCount(); p++ ) {

				if( uDone & (1<<p) ) {

					ForceUpdate(p);
				}
			}

			return TRUE;
		}
	}

	return FALSE;
}

// Implementation

CString CMqttClientJson::GetWillData(void)
{
	CString Json = "{}";

	switch( m_Opts.m_Code ) {

		case 0:
			Json = "{\"connected\":\"false\"}";
			break;

		case 1:
			Json = "{\"connected\":false}";
			break;

		case 2:
			Json = "{\"connected\":0}";
			break;
	}

	if( !m_ObjRep.IsEmpty() ) {

		Json = "{\"" + m_ObjRep + "\":" + Json + "}";
	}

	if( !m_ObjTop.IsEmpty() ) {

		Json = "{\"" + m_ObjTop + "\":" + Json + "}";
	}

	return Json;
}

CString CMqttClientJson::ApplyWillSuffix(CString const &Topic, BOOL fApply)
{
	if( fApply ) {

		for( UINT n = 0; n < m_pService->m_uSet; n++ ) {

			CCloudDataSet *pSet = m_pService->GetDataSet(n);

			if( pSet ) {

				if( pSet->IsEnabled() ) {

					if( !pSet->m_Suffix.IsEmpty() ) {

						return Topic + pSet->m_Suffix;
					}
				}
			}
		}
	}

	return Topic;
}

void CMqttClientJson::ShowSentData(CMqttMessage const *pMsg)
{
	#if 0

	CMqttJsonData Json;

	if( pMsg->GetJson(Json) ) {

		CMqttJsonData *pData = &Json;

		if( !m_ObjTop.IsEmpty() ) {

			pData = pData->GetChild(m_ObjTop);
		}

		if( !m_ObjRep.IsEmpty() ) {

			pData = pData->GetChild(m_ObjRep);
		}

		CString x = pData->GetValue("timestamp");

		CString h = pData->GetValue("historic");

		CString a = pData->GetValue("adhoc");

		if( h.IsEmpty() ) {

			AfxTrace("%u,%s\n", pMsg->m_uCode, PCTXT(x));
		}
		else
			AfxTrace("%u,%s,%s,%s\n", pMsg->m_uCode, PCTXT(x), PCTXT(h), PCTXT(a));
	}

	#endif
	}

// End of File
