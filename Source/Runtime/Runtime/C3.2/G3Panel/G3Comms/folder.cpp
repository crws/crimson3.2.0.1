
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Tag Folder
//

// Constructor

CTagFolder::CTagFolder(void)
{
	}

// Destructor

CTagFolder::~CTagFolder(void)
{
	}

// Initialization

void CTagFolder::Load(PCBYTE &pData)
{
	ValidateLoad("CTagFolder", pData);

	CTag::Load(pData);
	}

// End of File
