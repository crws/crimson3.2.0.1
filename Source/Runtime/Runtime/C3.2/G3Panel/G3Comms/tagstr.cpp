
#include "intern.hpp"

#include "secure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// String Tag Item
//

// Constructor

CTagString::CTagString(void)
{
	m_Encode = 0;
	m_Length = 16;
	m_pSec   = New CSecDesc;
	m_uStep  = 0;
	m_uSize  = 0;
	m_pData  = NULL;
	}

// Destructor

CTagString::~CTagString(void)
{
	}

// Initialization

void CTagString::Load(PCBYTE &pData)
{
	ValidateLoad("CTagString", pData);

	CDataTag::Load(pData);

	m_Encode  = GetByte(pData);

	m_Length  = GetWord(pData);

	m_pSec->Load(pData);

	GetCoded(pData, m_pOnWrite);

	if( !m_pFormat ) {

		if( m_Addr || m_Ref ) {

			CDispFormatString *pFormat = New CDispFormatString;

			pFormat->SetLength(m_Length);

			m_pFormat = pFormat;
			}
		}

	FindStep();

	InitData();
	}

// Attributes

UINT CTagString::GetDataType(void) const
{
	return typeString;
	}

// Operations

void CTagString::Purge(void)
{
	delete [] m_pData;
	}

// Evaluation

BOOL CTagString::IsAvail(CDataRef const &Ref, UINT Flags)
{
	if( !IsItemAvail(m_pOnWrite) ) {

		return FALSE;
		}

	if( LocalData() ) {

		return TRUE;
		}

	if( m_Ref ) {

		if( m_pBlock ) {

			if( (Flags & availData) || !m_Extent || !m_RdMode ) {

				INT nPos = FindPos(Ref);

				for( UINT n = 0; n < m_uStep; n++ ) {

					if( !m_pBlock->IsAvail(nPos + n) ) {

						return FALSE;
						}
					}
				}

			return TRUE;
			}

		return FALSE;
		}

	return CDataTag::IsAvail(Ref, 0); 
	}

BOOL CTagString::SetScan(CDataRef const &Ref, UINT Code)
{
	SetItemScan(m_pOnWrite, Code);

	if( LocalData() ) {

		return TRUE;
		}

	if( m_Ref ) {

		if( m_pBlock ) {

			if( m_Extent ) {

				if( !m_RdMode ) {

					for( UINT i = 0; i < m_uRegs; i++ ) {

						int nPos = m_nPos + i;

						m_pBlock->SetScan(nPos, Code);
						}
					}

				else if( m_RdMode == 1 ) {

					if( Code == scanUser ) {

						int nPos = FindPos(Ref);

						for( UINT n = 0; n < m_uStep; n++ ) {

							m_pBlock->SetScan(nPos + n, Code);
							}

						return TRUE;
						}

					return FALSE;
					}

				return TRUE;
				}

			int nPos = FindPos(Ref);

			for( UINT n = 0; n < m_uStep; n++ ) {

				m_pBlock->SetScan(nPos + n, Code);
				}

			return TRUE;
			}

		return FALSE;
		}

	return CDataTag::SetScan(Ref, Code);
	}

DWORD CTagString::GetData(CDataRef const &Ref, UINT Type, UINT Flags)
{
	if( Flags & getNoString ) {

		return 0;
		}

	if( IsString(Type) ) {

		UINT n = Ref.x.m_Array;

		if( n < m_uSize ) {

			if( LocalData() ) {

				return DWORD(wstrdup(m_pData[n]));
				}

			if( m_Ref ) {

				if( m_pBlock ) {

					PUTF   pText = PUTF  (Malloc(sizeof(WCHAR) * (m_Length + 1)));

					PDWORD pData = PDWORD(Malloc(m_uStep * sizeof(DWORD)));

					int    nPos  = FindPos(Ref);

					if( m_Extent ) {

						if( m_RdMode >= 2 ) {

							int nSpan = m_RdMode - 2;

							int nFrom = nPos - (nSpan+0) * m_uStep;

							int nTo   = nPos + (nSpan+1) * m_uStep;

							for( int n = nFrom; n < nTo; n++ ) {

								m_pBlock->SetScan(n, scanOnce);
								}
							}
						}

					for( UINT n = 0; n < m_uStep; n++ ) {

						pData[n] = m_pBlock->GetData(nPos++);
						}

					Decode(pText, pData);

					pText[m_Length] = 0;

					Free(pData);
					
					return DWORD(pText);
					}
				}

			return CDataTag::GetData(Ref, Type, Flags);
			}
		}

	return GetNull(Type);
	}

BOOL CTagString::SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	if( IsString(Type) ) {

		UINT n = Ref.x.m_Array;

		if( n < m_uSize ) {

			if( m_Access < 2 || m_pOnWrite ) {

				if( !(Flags & setForce) ) {

					DWORD Read = GetData(Ref, Type, Flags);

					if( !wstrcmp(PUTF(Data), PUTF(Read)) ) {

						Free(PUTF(Data));

						Free(PUTF(Read));

						return TRUE;
						}

					Free(PUTF(Read));
					}

				if( !AllowWrite(Ref, Data, Type) ) {

					return FALSE;
					}

				if( m_pOnWrite ) {

					DWORD Copy   = DWORD(wstrdup(PUTF(Data)));

					DWORD Args[] = { Copy, n };

					m_pOnWrite->Execute(typeVoid, Args);
					}

				if( m_Access < 2 ) {

					if( LocalData() ) {

						m_pData[n] = PCUTF(Data);

						if( m_Addr ) {

							SaveData(n);
							}

						if( !m_Ref ) {

							Free(PUTF(Data));

							return TRUE;
							}
						}

					if( m_Ref ) {

						if( m_pBlock ) {

							PUTF   pText = PUTF  (Data);

							PDWORD pData = PDWORD(alloca(m_uStep * sizeof(DWORD)));

							int    nPos  = FindPos(Ref);

							memset(pData, 0, m_uStep * sizeof(DWORD));

							Encode(pData, pText);

							for( UINT n = 0; n < m_uStep; n++ ) {

								m_pBlock->SetWriteData(nPos++, Flags, pData[n]);
								}

							Free(pText);

							return TRUE;
							}
						}
					}
				}
			}
		}

	return CDataTag::SetData(Ref, Data, Type, Flags);
	}

DWORD CTagString::GetProp(CDataRef const &Ref, WORD ID, UINT Type)
{
	return CDataTag::GetProp(Ref, ID, Type);
	}

// Deadband

void CTagString::InitPrevious(DWORD &Prev)
{
	Prev = 0;
	}

BOOL CTagString::HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev)
{
	if( Prev == 0 ) {

		Data = GetData(Ref, typeString, getNone);

		return TRUE;
		}
	else {
		PUTF pRead = PUTF(GetData(Ref, typeString, getNone));

		PUTF pLast = PUTF(Prev);

		if( wstrcmp(pRead, pLast) ) {

			free(PUTF(Data));

			Data = DWORD(pRead);

			return TRUE;
			}

		free(pRead);

		return FALSE;
		}
	}

void CTagString::KillPrevious(DWORD &Prev)
{
	free(PTXT(Prev));

	Prev = 0;
	}

// Implementation

int CTagString::FindPos(CDataRef const &Ref)
{
	return m_nPos + Ref.x.m_Array * m_uStep;
	}

void CTagString::FindStep(void)
{
	if( m_Ref ) {

		if( m_Encode ) {

			UINT uChar = GetCharBits();

			UINT uPack = m_uBits / uChar;

			m_uStep    = (m_Length + uPack - 1) / uPack;

			return;
			}
		
		m_uStep = m_Length;
		}
	}

BOOL CTagString::InitData(void)
{
	if( LocalData() ) {

		m_uSize = max(m_Extent, 1);

		m_pData = New CUnicode [ m_uSize ];

		if( m_pSim ) {

			PUTF Data = PUTF(m_pSim->ExecVal());

			for( UINT n = 0; n < m_uSize; n++ ) {

				m_pData[n] = Data;
				}

			Free(Data);
			}
		else {
			if( m_Addr ) {

				UINT uStep = sizeof(WCHAR) * (m_Length + 1);

				PUTF pText = PUTF(alloca(uStep));

				for( UINT n = 0; n < m_uSize; n++ ) {

					g_pPersist->GetData( PBYTE(pText),
							     m_Addr + n * uStep,
							     uStep
							     );

					m_pData[n] = pText;
					}
				}
			}

		return TRUE;
		}

	m_uSize = max(m_Extent, 1);

	return FALSE;
	}

void CTagString::SaveData(UINT n)
{
	UINT s = sizeof(WCHAR) * (m_Length + 1);

	PUTF p = PUTF(alloca(s));
	
	UINT a = m_Addr + n * s;

	memset  (p, 0, s);

	wstrncpy(p, m_pData[n], m_Length);

	g_pPersist->PutData(PBYTE(p), a, s);
	}

void CTagString::Decode(PUTF pText, PCDWORD pData)
{
	if( m_Encode ) {

		UINT  uChar  = GetCharBits();

		DWORD dwMask = (1 << uChar) - 1;

		DWORD dwData = 0;

		UINT  uBits  = 0;

		for( UINT n = 0; n < m_Length; n++ ) {

			if( !uBits ) {

				dwData = *pData++;

				uBits  = m_uBits;

				if( m_uBits == 32 ) {

					switch( m_Encode ) {

						case 2:
						case 4:
						case 5:
							dwData = Flip(DWORD(dwData));
							break;
						}
					}
				else {
					switch( m_Encode ) {

						case 2:
						case 4:
						case 5:
							dwData = Flip(WORD(dwData));
							break;
						}
					}
				}

			if( uChar == 4 ) {

				BYTE dig = ((dwData >> (m_uBits - uChar)) & dwMask);

				pText[n] = DecodeHex(dig);

				dwData <<= uChar;
				}
			else {
				pText[n] = WCHAR(dwData & dwMask);

				dwData >>= uChar;
				}

			uBits = uBits - uChar;
			}

		return;
		}

	for( UINT n = 0; n < m_Length; n++ ) {

		pText[n] = pData[n];
		}
	}

void CTagString::Encode(PDWORD pData, PCUTF pText)
{
	if( m_Encode ) {

		UINT  uChar  = GetCharBits();

		DWORD dwMask = (1 << uChar) - 1;

		DWORD dwData = 0;

		UINT  uBits  = 0;

		UINT  uLen   = m_Length;

		while( uLen % 4 ) {

			uLen++;
			}

		for( UINT n = 0; n < uLen; n++ ) {

			dwData = (dwData << uChar) | (GetChar(pText) & dwMask);
			
			uBits  = uBits + uChar;

			if( uBits == m_uBits ) {

				if( m_uBits == 32 ) {

					switch( m_Encode ) {

						case 1:
							dwData = Flip(DWORD(dwData));
							break;

						case 3:
							dwData = MAKELONG(HIWORD(dwData),LOWORD(dwData));
							break;
						}
					}
				else {
					switch( m_Encode ) {

						case 1:
						case 3:
							dwData = Flip(WORD(dwData));
							break;
						}
					}

				*pData++ = dwData;

				uBits    = 0;
				}
			}

		return;
		}

	for( UINT n = 0; n < m_Length; n++ ) {

		pData[n] = GetChar(pText);
		}
	}

WCHAR CTagString::GetChar(PCUTF &pText)
{
	if( *pText ) {

		return *pText++;
		}

	if( CCommsSystem::m_pThis->m_pComms->m_StrPad ) {

		return 0x00;
		}

	return 0x20;
	}

UINT CTagString::GetCharBits(void)
{
	switch( m_Encode ) {

		case 1: return 8;
		case 2: return 8;
		case 3: return 16;
		case 4: return 16;
		case 5: return 4;
		case 6: return 4;
		}

	return 16;
	}

WCHAR CTagString::DecodeHex(BYTE bHex)
{
	return L"0123456789ABCDEF"[bHex];
	}

DWORD CTagString::Flip(DWORD d)
{
	WORD lo = Flip(LOWORD(d));
	WORD hi = Flip(HIWORD(d));

	return MAKELONG(hi,lo);
	}

WORD CTagString::Flip(WORD d)
{
	BYTE lo = LOBYTE(d);
	BYTE hi = HIBYTE(d);

	return MAKEWORD(hi,lo);
	}

// End of File
