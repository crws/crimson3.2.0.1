
#include "intern.hpp"

#include "secure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Tag
//

// Constructor

CDataTag::CDataTag(void)
{
	m_Extent   = 0;

	m_Access   = 0;

	m_RdMode   = 0;

	m_Addr     = 0;

	m_pSec     = NULL;

	m_pOnWrite = NULL;
	}

// Destructor

CDataTag::~CDataTag(void)
{
	delete m_pSec;

	delete m_pOnWrite;
	}

// Initialization

void CDataTag::Load(PCBYTE &pData)
{
	CTag::Load(pData);

	m_Extent = GetWord(pData);

	m_Access = GetByte(pData);

	m_RdMode = GetByte(pData);

	m_Addr   = GetLong(pData);

	m_uRegs  = GetWord(pData);

	MakeMax(m_uRegs, 1);
	}

// Attributes

BOOL CDataTag::IsArray(void) const
{
	return m_Extent > 0;
	}

UINT CDataTag::GetExtent(void) const
{
	return m_Extent;
	}

BOOL CDataTag::CanWrite(void) const
{
	if( m_Access < 2 || m_pOnWrite ) {

		BOOL fMap = m_Ref ? TRUE : FALSE;

		return CCommsSystem::m_pThis->m_pSecure->AllowWrite(fMap, m_pSec);
		}

	return FALSE;
	}

// Operations

void CDataTag::Force(void)
{
	if( m_Ref ) {
		
		if( m_Access == 1 ) {

			UINT n = m_Extent ? m_Extent : 1;

			UINT t = GetDataType();

			for( UINT i = 0; i < n; i++ ) {

				CDataRef Ref;

				Ref.m_Ref     = 0;

				Ref.x.m_Array = i;

				DWORD Data = GetData(Ref, t, getNone);

				SetData(Ref, Data, t, setForce | setDirect);
				}
			}
		}
	}

// Implementation

BOOL CDataTag::AllowWrite(CDataRef const &Ref, DWORD Data, UINT Type)
{
	UINT uPos = Ref.x.m_Array;

	BOOL fMap = m_Ref ? TRUE : FALSE;

	return CCommsSystem::m_pThis->m_pSecure->AllowWrite(this, uPos, fMap, m_pSec, Data, Type);
	}

BOOL CDataTag::LocalData(void)
{
	return (m_Ref == 0 && m_pValue == NULL) || m_Access == 1;
	}

BOOL CDataTag::IsNumeric(UINT Type)
{
	return Type == typeReal || IsInteger(Type);
	}

BOOL CDataTag::IsInteger(UINT Type)
{
	return Type == typeVoid || Type == typeInteger || Type == typeLogical;
	}

BOOL CDataTag::IsString(UINT Type)
{
	return Type == typeString;
	}

// End of File
