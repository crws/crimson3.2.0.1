
#include "intern.hpp"

#include "lang.hpp"

#include <math.h>

#include <limits.h>

#include <float.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scientific Display Format
//

// Constructor

CDispFormatSci::CDispFormatSci(void)
{
	m_After	  = 5;

	m_ManSign = 0;

	m_ExpSign = 1;

	m_pPrefix = NULL;

	m_pUnits  = NULL;

	m_pDynDP  = NULL;
	}

// Destructor

CDispFormatSci::~CDispFormatSci(void)
{
	delete m_pPrefix;

	delete m_pUnits;

	delete m_pDynDP;
	}

// Initialization

void CDispFormatSci::Load(PCBYTE &pData)
{
	ValidateLoad("CDispFormatSci", pData);

	m_After   = GetByte(pData);

	m_ManSign = GetByte(pData);
	
	m_ExpSign = GetByte(pData);

	GetCoded(pData, m_pPrefix);
	
	GetCoded(pData, m_pUnits);

	GetCoded(pData, m_pDynDP);
	}

// Scan Control

BOOL CDispFormatSci::IsAvail(void)
{
	if( !IsItemAvail(m_pUnits) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pPrefix) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pDynDP) ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CDispFormatSci::SetScan(UINT Code)
{
	SetItemScan(m_pUnits,  Code);

	SetItemScan(m_pPrefix, Code);

	SetItemScan(m_pDynDP, Code);
	
	return TRUE;
	}

// Formatting

CUnicode CDispFormatSci::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		CUnicode Text;
		
		if( m_pPrefix ) {

			Text += m_pPrefix->GetText();
			}
		
		if( m_pUnits ) {

			Text += m_pUnits->GetText();
			}

		return Text;
		}

	if( Type == typeInteger ) {

		C3REAL r = C3REAL(INT(Data));

		return Format(R2I(r), typeReal, Flags);
		}
	
	if( Type == typeReal ) {

		C3REAL r = I2R(Data);
	
		if( IsNAN(r) ) {

			return L"NAN";
			}

		if( IsINF(r) ) {

			if( r > 0 ) {

				return L"+INF";
				}

			return L"-INF";
			}
			
		double   e = r ? floor(log10(fabs(r)) + 0.5) : 0;

		double   m = r / pow(10, e);

		UINT     a = GetAfter();

		CUnicode Text;

		// REV3 -- Can we use some special characters for the plus
		// and minus to ensure that they are the same size as a digit
		// when using a variable pitch font?

		if( m < 0 ) {
			
			Text += '-';
			
			m = fabs(m);
			}
		else {
			if( m_ManSign ) {
				
				Text += '+';
				}
			else {
				if( Flags & fmtPad ) {

					if( Flags & fmtANSI ) {
						
						Text += spaceNormal;
						}
					else
						Text += spaceFigure;
					}
				}
			}

		if( m ) {

			m += 0.5 / pow(double(10), int(1 + a));
			}

		if( m && m < 1 ) {
			
			m *= 10;

			e -= 1;
			}

		char s[64];

		gcvt(m, 1 + a, s);

		UINT n = strlen(s);

		if( a ) {

			if( n == 1 ) {

				s[n++] = '.';
				}

			while( n < 2 + a ) {

				s[n++] = '0';
				}
			}
		else {
			if( s[n-1] == '.' ) {

				s[--n] = 0;
				}
			}

		s[n++] = 'E';

		if( e < 0 ) {
			
			s[n++] = '-';
			
			e = fabs(e);
			}
		else {
			if( m_ExpSign ) {
				
				s[n++] = '+';
				}
			}

		sprintf(s + n, "%u", INT(e));

		if( GetAfter() ) {

			Text += s[0];

			Text += GetPointChar();

			Text += s+2;
			}
		else
			Text += s;

		if( Flags & fmtPad ) {

			if( !(Flags & fmtANSI) ) {

				MakeDigitsFixed(Text);
				}
			}

		if( !(Flags & fmtBare) ) {

			if( m_pPrefix ) {
				
				Text = m_pPrefix->GetText() + Text;
				}

			if( m_pUnits ) {
				
				Text = Text + m_pUnits->GetText();
				}
			}

		return Text;
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Editing

UINT CDispFormatSci::Edit(CEditCtx *pEdit, UINT uCode)
{
	if( uCode == 0x00 ) {

		pEdit->m_uKeypad  = keypadExponent;

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		pEdit->m_fError   = TRUE;

		return editUpdate;
		}

	if( uCode == 0x0D ) {

		if( !pEdit->m_fDefault ) {

			double Read = 0;

			if( pEdit->m_Edit.GetLength() ) {

				CString Edit = UniConvert(pEdit->m_Edit);

				Read         = strtod (Edit, NULL);
				}

			if( pEdit->m_Type == typeReal ) {

				C3REAL Data = C3REAL(Read);

				C3REAL Min  = I2R(pEdit->m_Min);

				C3REAL Max  = I2R(pEdit->m_Max);

				if( Data >= Min && Data <= Max ) {

					if( Validate(pEdit, R2I(Data)) ) {
						
						if( pEdit->m_pValue->SetValue(R2I(Data), typeReal, setNone) ) {

							return editCommit;
							}

						return editUpdate;
						}
					}
				}
			else {
				C3INT Data = C3INT(Read);

				C3INT Min  = C3INT(pEdit->m_Min);

				C3INT Max  = C3INT(pEdit->m_Max);

				if( Data >= Min && Data <= Max ) {

					if( Validate(pEdit, DWORD(Data)) ) {

						if( pEdit->m_pValue->SetValue(DWORD(Data), typeInteger, setNone) ) {

							return editCommit;
							}

						return editUpdate;
						}
					}
				}

			pEdit->m_uCursor = cursorError;

			pEdit->m_fError  = TRUE;

			return editError;
			}

		return editAbort;
		}

	if( uCode == 0x1B ) {

		if( pEdit->m_fDefault ) {

			return editAbort;
			}

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		pEdit->m_fError   = FALSE;

		return editUpdate;
		}

	if( uCode == 0x7F ) {

		if( pEdit->m_fDefault || pEdit->m_fError ) {

			pEdit->m_Edit.Empty();

			pEdit->m_uCursor  = cursorLast;

			pEdit->m_fDefault = FALSE;

			pEdit->m_fError   = false;

			return editUpdate;
			}
		else {
			while( !pEdit->m_Edit.IsEmpty() ) {

				UINT uLength  = pEdit->m_Edit.GetLength();

				pEdit->m_Edit = pEdit->m_Edit.Left(uLength - 1);

				if( pEdit->m_Edit.Right(1) == L"-" ) {

					continue;
					}

				pEdit->m_uCursor = cursorLast;

				return editUpdate;
				}

			return editNone;
			}
		}

	if( uCode >= 0x20 ) {

		if( pEdit->m_fDefault || pEdit->m_fError ) {

			pEdit->m_Edit.Empty();

			pEdit->m_uCursor  = cursorLast;

			pEdit->m_fDefault = FALSE;

			pEdit->m_fError   = FALSE;
			}

		UINT uExp = pEdit->m_Edit.Find('E');

		UINT uDot = pEdit->m_Edit.Find('.');

		UINT uLen = pEdit->m_Edit.GetLength();

		if( uExp < NOTHING ) {

			if( uCode == '+' ) {

				if( pEdit->m_Edit[uExp+1] == '-' ) {

					pEdit->m_Edit.Delete(uExp+1, 1);
			
					return editUpdate;
					}

				if( pEdit->m_Edit.GetLength() < 16 ) {

					pEdit->m_Edit.Insert(uExp+1, '-');

					return editUpdate;
					}

				return editNone;
				}

			if( uCode >= '0' && uCode <= '9' ) {

				UINT uDig = uLen - uExp - 1;

				if( pEdit->m_Edit[uExp+1] == '-' ) {

					uDig--;
					}

				if( uDig < 2 ) {

					pEdit->m_Edit += WCHAR(uCode);

					return editUpdate;
					}

				return editNone;
				}
			}
		else {
			if( uCode == '+' || uCode == '-' ) {

				if( pEdit->m_Edit[0] == '-' ) {

					pEdit->m_Edit.Delete(0, 1);
			
					return editUpdate;
					}

				if( pEdit->m_Edit.GetLength() < 16 ) {

					pEdit->m_Edit.Insert(0, '-');

					return editUpdate;
					}

				return editNone;
				}

			if( uCode == '.' ) {

				if( uDot < NOTHING ) {

					return editNone;
					}

				if( pEdit->m_Edit.IsEmpty() ) {

					pEdit->m_Edit = L"0";
					}

				pEdit->m_Edit += WCHAR(uCode);

				return editUpdate;
				}

			if( uCode >= '0' && uCode <= '9' ) {

				if( uDot < NOTHING ) {

					if( uLen - uDot - 1 < GetAfter() ) {

						pEdit->m_Edit += WCHAR(uCode);

						return editUpdate;
						}

					return editNone;
					}

				if( TRUE ) {

					if( pEdit->m_Edit.GetLength() < 16 ) {

						pEdit->m_Edit += WCHAR(uCode);

						return editUpdate;
						}
					}
				else {
					if( uLen == 0 ) {

						pEdit->m_Edit += WCHAR(uCode);

						return editUpdate;
						}

					if( uLen == 1 ) {
						
						if( pEdit->m_Edit[0] == '-' ) {

							pEdit->m_Edit += WCHAR(uCode);

							return editUpdate;
							}
						}
					}

				return editNone;
				}

			if( uCode == 'E' || uCode == 'e' ) {

				if( uLen == 0 ) {

					pEdit->m_Edit += '1';
					}

				if( uLen == 1 ) {

					if( pEdit->m_Edit[0] == '-' ) {

						pEdit->m_Edit += '1';
						}
					}

				pEdit->m_Edit += WCHAR('E');

				return editUpdate;
				}
			}
		}

	return editNone;
	}

// Limit Access

BOOL CDispFormatSci::NeedsLimits(void)
{
	return TRUE;
	}

DWORD CDispFormatSci::GetMin(UINT Type)
{
	if( Type == typeInteger ) {
		
		return DWORD(INT_MIN);
		}

	if( Type == typeReal ) {
		
		return R2I(-FLT_MAX);
		}
	
	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatSci::GetMax(UINT Type)
{
	if( Type == typeInteger ) {
		
		return DWORD(INT_MAX);
		}

	if( Type == typeReal ) {
		
		return R2I(+FLT_MAX);
		}

	return CDispFormat::GetMax(Type);
	}

WCHAR CDispFormatSci::GetPointChar(void)
{
	return CCommsSystem::m_pThis->m_pLang->GetDecPointChar();
	}

UINT CDispFormatSci::GetAfter(void)
{
	if( m_pDynDP ) {

		UINT uAfter = m_pDynDP->ExecVal();

		return max(0, min(m_After, uAfter));
		}

	return m_After;
	}

// End of File
