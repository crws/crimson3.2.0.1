//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TDS_TdsResponse_HPP

#define INCLUDE_TDS_TdsResponse_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TdsPacket.hpp"


#define ENVCHANGE_DATABASE	1
#define ENVCHANGE_LANGUAGE	2
#define	ENVCHANGE_CHARACTERSET	3
#define ENVCHANGE_PACKETSIZE	4
#define ENVCHANGE_SQLCOLLATION	7

#define NUM_INFO_MESSAGES	10
#define NUM_ERR_MESSAGES	10
#define	MAX_MESSAGE_LEN		1024

#define MAX_PROP_LEN		256


//////////////////////////////////////////////////////////////////////////
//
// TDS Response Packet
//

class CTdsResponse : public CTdsPacket
{
	public:
		// Constructors
		CTdsResponse(void);
		~CTdsResponse(void);

		// Attributes
		bool	HaveErrorFromServer() const;
		USHORT	GetNumErrMessages() const;
		PCSTR	GetErrorMessage(USHORT &pIndex) const;
		LONG	GetErrorCode(USHORT &pIndex) const;
		PCSTR	GetDatabase() const;
		PCSTR	GetLanguage() const;
		PCSTR	GetCharacterSet() const;
		PCSTR	GetPacketSize() const;
		USHORT  GetPacketSizeValue() const;
		PCSTR	GetSQLCollation() const;
		USHORT  GetNumInfoMessages() const;
		PCSTR   GetInfoMessage(USHORT &pIndex) const;

		// Parsing
		int Parse(PCBYTE pData, UINT uSize);

	protected:
		// Data Members
		WORD	m_wStatus;
		CHAR*	m_szErrorMessages[NUM_ERR_MESSAGES];
		LONG	m_lErrorCodes[NUM_ERR_MESSAGES];
		USHORT	m_uNumErrMessages;
		bool	m_fParsedAnErrorFromServer;
		CHAR*	m_szDatabase;
		CHAR*	m_szLanguage;
		CHAR*	m_szCharacterSet;
		CHAR*	m_szPacketSize;
		USHORT	m_uPacketSize;
		CHAR*	m_szSQLCollation;
		CHAR*	m_szInfoMessages[NUM_INFO_MESSAGES];
		USHORT	m_uNumInfoMessages;

	private:
		// Helper methods
		void SetError(CHAR* sMessage);
		void ReadNewAndOldValues(UINT &uPos, CHAR* szNewValue, CHAR* szOldValue);
		bool ReadEnvchangeTokenStream(UINT &uPos);
		bool ReadErrorTokenStream(UINT &uPos);
		bool ReadInfoTokenStream(UINT &uPos);
		bool ReadLoginAckTokenStream(UINT &uPos);
	};


// End of File

#endif
