
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Helper Object Implementation
//

class CHelper : public ICommsHelper
{
	public:
		// Constructor
		CHelper(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Helper
		void METHOD WontReturn(void);
		void METHOD StopSystem(void);
		BOOL METHOD MoreHelp(WORD ID, void **pHelp); 

	protected:
		// Data Members
		ULONG m_uRefs;
	};

////////////////////////////////////////////////////////////////////////
//
// Helper Object Implementation 
//

// Instantiator

ICommsHelper * Create_CommsHelper(void)
{
	return New CHelper;
	}

// Constructor

CHelper::CHelper(void)
{
	StdSetRef();
	}

// IUnknown

HRESULT CHelper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ICommsHelper);

	StdQueryInterface(ICommsHelper);

	return E_NOINTERFACE;
	}

ULONG CHelper::AddRef(void)
{
	StdAddRef();
	}

ULONG CHelper::Release(void)
{
	StdRelease();
	}

// ICommsHelper

void CHelper::WontReturn(void)
{
	// !!!
	}

void CHelper::StopSystem(void)
{
	// !!!
	}

BOOL CHelper::MoreHelp(WORD ID, void **pHelp)
{
	IEthernetIPHelper * Create_EthernetIPHelper(void);
	IDisplayHelper    * Create_DisplayHelper   (void);
	ISyncHelper       * Create_SyncHelper      (void);
	IDeviceNetHelper  * Create_DeviceNetHelper (void);
	IExtraHelper      * Create_ExtraHelper     (void);
	IMPIHelper        * Create_MPIHelper       (void);
	IDnpHelper        * Create_DnpHelper       (void);

	switch( ID ) {

		case IDH_ENETIP:	
			*pHelp = Create_EthernetIPHelper();
			break;

		case IDH_DISPLAY:
			*pHelp = Create_DisplayHelper();
			break;

		case IDH_SYNC:
			*pHelp = Create_SyncHelper();
			break;

		case IDH_DEVNET:
			*pHelp = Create_DeviceNetHelper();
			break;

		case IDH_EXTRA:
			*pHelp = Create_ExtraHelper();
			break;

		case IDH_MPI:
			*pHelp = Create_MPIHelper();
			break;

		case IDH_DNP:
			*pHelp = Create_DnpHelper();
			break;

		default:
			*pHelp = NULL;
			break;
		}

	return *pHelp ? TRUE : FALSE;
	}

// End of File
