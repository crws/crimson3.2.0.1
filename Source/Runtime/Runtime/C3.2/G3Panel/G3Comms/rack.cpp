
#include "intern.hpp"


//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Backplane Configuration
//

// Constructor

CRackItem::CRackItem(void)
{
	m_pPorts = New CCommsPortList;
	}

// Destructor

CRackItem::~CRackItem(void)
{
	delete m_pPorts;
	}

// Initialization

void CRackItem::Load(PCBYTE &pData)
{
	ValidateLoad("CRackItem", pData);	

	m_pPorts->Load(pData);
	}

// Task List

void CRackItem::GetTaskList(CTaskList &List)
{
	UINT uLevel = 9000;

	m_pPorts->GetTaskList(List, uLevel);
	}

// End of File
