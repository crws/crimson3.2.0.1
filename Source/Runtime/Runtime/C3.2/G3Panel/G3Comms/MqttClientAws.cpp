
#include "Intern.hpp"

#include "Service.hpp"

#include "Matrix.hpp"

#include "MqttClientAws.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudServiceAws.hpp"

#include "MqttClientOptionsAws.hpp"

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for AWS
//

// Constructor

CMqttClientAws::CMqttClientAws(CCloudServiceAws *pService, CMqttClientOptionsAws &Opts) : CMqttClientJson(pService, Opts), m_Opts(Opts)
{
}

// Operations

BOOL CMqttClientAws::Open(void)
{
	if( m_Opts.m_Shadow ) {

		m_ObjTop = "state";

		m_ObjRep = "reported";

		m_ObjDes = "desired";

		m_Opts.m_Pub = "$aws/things/" + m_Opts.m_ClientId + "/shadow/update";

		m_Opts.m_Sub = m_Opts.m_Pub + "/delta";
	}
	else {
		if( m_Opts.m_Pub.IsEmpty() ) {

			m_Opts.m_Pub = m_Opts.m_ClientId;
		}

		if( m_Opts.m_Sub.IsEmpty() ) {

			m_Opts.m_Sub = m_Opts.m_ClientId + "/sub";
		}

		SubstTopic(m_Opts.m_Pub);

		SubstTopic(m_Opts.m_Sub);
	}

	m_Will.SetTopic(ApplyWillSuffix(m_Opts.m_Pub, !m_Opts.m_Shadow));

	m_Will.SetData(GetWillData());

	return CMqttClientJson::Open();
}

// Client Hooks

void CMqttClientAws::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseInitial ) {

		if( HasWrites() ) {

			AddToSubList(subDelta, m_Opts.m_Sub);
		}
	}

	CMqttClientJson::OnClientPhaseChange();
}

BOOL CMqttClientAws::OnClientNewData(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode == subDelta ) {

		CMqttJsonData Json;

		if( pMsg->GetJson(Json) ) {

			OnWrite(Json, "state");
		}

		return TRUE;
	}

	return TRUE;
}

// Publish Hook

BOOL CMqttClientAws::GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg)
{
	if( CMqttClientJson::GetPubMessage(uTopic, uTime, fTemp, uMode, pMsg) ) {

		if( !m_Opts.m_Shadow ) {

			CCloudDataSet *pSet = m_pService->GetDataSet(uTopic);

			pMsg->SetTopic(m_Opts.m_Pub + pSet->m_Suffix);
		}
		else {
			pMsg->SetTopic(m_Opts.m_Pub);
		}

		return TRUE;
	}

	return FALSE;
}

// End of File
