
#include "intern.hpp"

#include "events.hpp"

#include "service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic Tag Event
//

// Constructor

CTagEvent::CTagEvent(void)
{
	m_pLabel    = NULL;
	m_Trigger   = 0;
	m_Accept    = 0;
	m_Priority  = 1;
	m_Print	    = FALSE;
	m_Siren	    = FALSE;
	m_Mail      = 0xFFFF;
	m_pOnActive = NULL;
	m_pOnClear  = NULL;
	m_pOnAccept = NULL;
	m_pOnEvent  = NULL;
	m_pEnable   = NULL;
	}

// Destructor

CTagEvent::~CTagEvent(void)
{
	delete m_pLabel;
	delete m_pOnActive;
	delete m_pOnClear;
	delete m_pOnAccept;
	delete m_pOnEvent;
	delete m_pEnable;
	}

// Initialization

void CTagEvent::Load(PCBYTE &pData)
{
	CCodedHost::Load(pData);

	m_Mode  = GetByte(pData);

	m_Const = GetByte(pData);
	
	GetCoded(pData, m_pLabel);
	
	m_Trigger  = GetByte(pData);
	m_Delay    = GetWord(pData);
	m_Accept   = GetByte(pData);
	m_Priority = GetByte(pData);
	m_Print    = GetByte(pData);
	m_Siren    = GetByte(pData);
	m_Mail     = GetWord(pData);
	
	GetCoded(pData, m_pOnActive);
	
	GetCoded(pData, m_pOnClear);

	GetCoded(pData, m_pOnAccept);

	GetCoded(pData, m_pOnEvent);
	
	GetCoded(pData, m_pEnable);
	}

// Attributes

BOOL CTagEvent::IsAvail(void) const
{
	if( !IsItemAvail(m_pLabel) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pOnActive) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pOnClear) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pOnAccept) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pOnEvent) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pEnable) ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CTagEvent::IsActive(UINT uPos) const
{
	UINT uIndex = (m_uIndex | (LOBYTE(uPos) << 16));

	return CCommsSystem::m_pThis->m_pAlarms->IsAlarmActive(uIndex);
	}

// Operations

void CTagEvent::SetScan(UINT uCode)
{
	SetItemScan(m_pLabel,    uCode);
	
	SetItemScan(m_pOnActive, uCode);
	
	SetItemScan(m_pOnClear,  uCode);
	
	SetItemScan(m_pOnAccept, uCode);

	SetItemScan(m_pOnEvent,  uCode);
	
	SetItemScan(m_pEnable,   uCode);
	}

// Text Access

BOOL CTagEvent::GetEventText(CUnicode &Text, UINT uPos)
{
	// NOTE -- We did not ought to need the uPos param
	// as labels that depend on the index will not have
	// the constant flag set and won't get accessed in
	// this manner, but it doesn't do any harm!

	Text = m_pLabel->GetText(L"Untitled Event", PDWORD(&uPos));

	return TRUE;
	}

// Change Hook

void CTagEvent::OnChange(UINT uPos, BOOL fChange, DWORD PV)
{
	CCtx &Ctx = m_pCtx[uPos];

	switch( m_Trigger ) {

		case 0:
			FireAlarm(uPos, Ctx.m_fState);
			
			break;

		case 1:
			if( Ctx.m_fState ) {

				FireAlarm(uPos, TRUE);
				}
			break;

		case 2:
			if( Ctx.m_fState ) {

				FireEvent(uPos);
				}
			break;
		}

	CTagPollable::OnChange(uPos, fChange, PV);
	}

// Startup Mode

BOOL CTagEvent::FireOnInit(void)
{
	if( m_Trigger == 0 ) {

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CTagEvent::FireAlarm(UINT uPos, BOOL fState)
{
	CUnicode Text;

	if( fState && !m_Const ) {

		DWORD p[2];

		p[0] = uPos;

		p[1] = m_uIndex;

		Text = m_pLabel->GetText(L"Untitled Alarm", p);
		}

	if( TRUE ) {

		UINT uIndex = (m_uIndex | (LOBYTE(uPos) << 16));

		CCommsSystem::m_pThis->m_pAlarms->UpdateAlarm( this,
							       uIndex,
							       Text,
							       fState
							       );
		}

	return TRUE;
	}

BOOL CTagEvent::FireEvent(UINT uPos)
{
	CUnicode Text;

	if( m_pOnEvent && m_pOnEvent->IsAvail() ) {

		m_pOnEvent->Execute(typeVoid);
		}

	if( !m_Const || m_Mail < 0xFFFF ) {

		Text = m_pLabel->GetText(L"Untitled Event", PDWORD(&uPos));
		}

	if( !m_Const ) {

		UINT uIndex = (m_uIndex | (LOBYTE(uPos) << 16));
	
		CCommsSystem::m_pThis->m_pEvents->LogEvent( 0,
							    0,
							    eventEvent,
							    uIndex,
							    Text
							    );
		}
	else {
		UINT uIndex = (m_uIndex | (LOBYTE(uPos) << 16));
	
		CCommsSystem::m_pThis->m_pEvents->LogEvent( 0,
							    0,
							    eventEvent,
							    uIndex
							    );
		}

	if( g_pServiceMail ) {

		if( m_Mail < 0xFFFF ) {

			DWORD Time = GetNow();

			g_pServiceMail->SendMail( m_Mail,
						  "Event",
						  Time,
						  Text
						  );
			}
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Event
//

// Instantiator

CTagEventNumeric * CTagEventNumeric::Create(PCBYTE &pData)
{
	if( GetByte(pData) ) {

		CTagEventNumeric *pItem = New CTagEventNumeric;

		pItem->Load(pData);

		return pItem;
		}

	return NULL;
	}

// Constructor

CTagEventNumeric::CTagEventNumeric(void)
{
	m_pValue = NULL;

	m_pHyst  = NULL;
	}

// Destructor

CTagEventNumeric::~CTagEventNumeric(void)
{
	delete m_pValue;

	delete m_pHyst;
	}

// Initialization

void CTagEventNumeric::Load(PCBYTE &pData)
{
	ValidateLoad("TagEventNumeric", pData);

	CTagEvent::Load(pData);

	GetCoded(pData, m_pValue);

	GetCoded(pData, m_pHyst);
	}

// Attributes

BOOL CTagEventNumeric::IsAvail(void) const
{
	if( CTagEvent::IsAvail() ) {

		if( !IsItemAvail(m_pValue) ) {

			return FALSE;
			}

		if( !IsItemAvail(m_pHyst) ) {

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Operations

void CTagEventNumeric::SetScan(UINT uCode)
{
	CTagEvent::SetScan(uCode);

	SetItemScan(m_pValue, uCode);

	SetItemScan(m_pHyst,  uCode);
	}

void CTagEventNumeric::Poll(UINT uPos, DWORD SP, DWORD PV, UINT Type, UINT uDelta)
{
	if( IsAvail() ) {

		if( Type == typeInteger ) {

			C3INT Enable = GetItemData(m_pEnable, 1, PDWORD(&uPos));

			C3INT Value  = GetItemData(m_pValue,  0, PDWORD(&uPos));

			C3INT Hyst   = GetItemData(m_pHyst,   0, PDWORD(&uPos));

			PollInteger(uPos, Enable, SP, PV, Value, Hyst, uDelta);
			}

		if( Type == typeReal ) {

			C3INT  Enable = GetItemData(m_pEnable, 1, PDWORD(&uPos));

			C3REAL Value  = GetItemData(m_pValue, C3REAL(0), PDWORD(&uPos));

			C3REAL Hyst   = GetItemData(m_pHyst,  C3REAL(0), PDWORD(&uPos));

			PollReal(uPos, Enable, I2R(SP), I2R(PV), Value, Hyst, uDelta);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Event
//

// Instantiator

CTagEventFlag * CTagEventFlag::Create(PCBYTE &pData)
{
	if( GetByte(pData) ) {

		CTagEventFlag *pItem = New CTagEventFlag;

		pItem->Load(pData);

		return pItem;
		}

	return NULL;
	}

// Constructor

CTagEventFlag::CTagEventFlag(void)
{
	}

// Destructor

CTagEventFlag::~CTagEventFlag(void)
{
	}

// Initialization

void CTagEventFlag::Load(PCBYTE &pData)
{
	ValidateLoad("TagEventFlag", pData);

	CTagEvent::Load(pData);
	}

// Attributes

BOOL CTagEventFlag::IsAvail(void) const
{
	if( CTagEvent::IsAvail() ) {

		return TRUE;
		}

	return FALSE;
	}

// Operations

void CTagEventFlag::SetScan(UINT uCode)
{
	CTagEvent::SetScan(uCode);
	}

void CTagEventFlag::Poll(UINT uPos, DWORD SP, DWORD PV, UINT uDelta)
{
	if( IsAvail() ) {

		C3INT Enable = GetItemData(m_pEnable, 1, PDWORD(&uPos));

		PollFlag(uPos, Enable, SP, PV, uDelta);
		}
	}

// End of File
