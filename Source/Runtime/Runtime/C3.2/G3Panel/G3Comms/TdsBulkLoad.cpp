//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "Intern.hpp"

#include "TdsBulkLoad.hpp"


//////////////////////////////////////////////////////////////////////////
//
// TDS Bulk Load Packet
//


// Constructors

CTdsBulkLoad::CTdsBulkLoad(void) : CTdsPacket(typeBulkLoad)
{
	AddByte(TDS_COLMETADATA_TOKEN);

	}


CTdsBulkLoad::CTdsBulkLoad(UINT uBufferSize) : CTdsPacket(typeBulkLoad, uBufferSize)
{
	AddByte(TDS_COLMETADATA_TOKEN);

	}

CTdsBulkLoad::CTdsBulkLoad(UINT uBufferSize, bool fAddHeader) : CTdsPacket(typeBulkLoad, uBufferSize, fAddHeader)
{
	
	}

CTdsBulkLoad::~CTdsBulkLoad(void)
{
	}


// Operations

void CTdsBulkLoad::AddColumnCount(USHORT uColumnCount)
{
	AddUShortReverse(uColumnCount);
	}


void CTdsBulkLoad::AddColumn(PCSTR pColumnName, TdsDataType eColumnType, USHORT uColumnSize, BOOL fNullable)
{
	AddULong(0);				// User Type
	
	if (fNullable)				// Flags
		AddUShortReverse(0x05);
	else
		AddUShortReverse(0x04);
	
	AddByte(BYTE(eColumnType));		// Type

	// Add the type length information.
	switch (eColumnType)
	{
		case typeIntNType:
			AddByte((BYTE) uColumnSize);
			break;

		case typeFltNType:
			AddByte((BYTE) uColumnSize);
			break;

		case typeDateTimNType:
			AddByte((BYTE) uColumnSize);
			break;

		case typeBigVarChrType:
			AddUShortReverse(uColumnSize);
			AddByte(0x09);	// Collation info
			AddByte(0x04);
			AddByte(0xD0);
			AddByte(0x00);
			AddByte(0x34);
			break;
		}

	AddReverseCharText(pColumnName);	// Column Name
	}


void CTdsBulkLoad::StartRow()
{
	AddByte(TDS_ROW_TOKEN);
	}


void CTdsBulkLoad::AddFieldInt1(BYTE bValue)
{
	AddByte(0x01);
	AddByte(bValue);
	}


void CTdsBulkLoad::AddFieldInt2(SHORT sValue)
{
	AddByte(0x02);
	AddUShortReverse(sValue);
	}


void CTdsBulkLoad::AddFieldInt4(LONG lValue)
{
	AddByte(0x04);
	AddULongReverse(lValue);
	}


void CTdsBulkLoad::AddFieldInt8(LONGLONG lValue)
{
	AddByte(0x08);
	AddULongLongReverse(lValue);
	}


void CTdsBulkLoad::AddFieldFloat4(FLOAT gValue)
{
	AddByte(0x04);
	AddFloatReverse(gValue);
	}


void CTdsBulkLoad::AddFieldFloat8(DOUBLE dValue)
{
	AddByte(0x08);
	AddDoubleReverse(dValue);
	}


void CTdsBulkLoad::AddFieldDateTime(ULONGLONG lValue)
{
	AddByte(0x08);
	AddULongReverse(lValue >> 32);
	AddULongReverse(lValue & 0x00000000FFFFFFFF);
	}


void CTdsBulkLoad::AddFieldVarchar(PCSTR pValue)
{
	AddUShortReverse(USHORT(strlen(pValue)));
	AddTextNoTerminator(pValue);
	}


void CTdsBulkLoad::AddFieldVarchar(PCSTR pValue, UINT uSize)
{
	AddUShortReverse(USHORT(uSize));
	AddTextNoTerminator(pValue, uSize);
	}


void CTdsBulkLoad::EndPacket(bool fEnd)
{
	PBYTE pData = m_Data.GetData();
	UINT  uSize = m_Data.GetSize();

	pData[0] = BYTE(m_Type);
	pData[1] = BYTE(fEnd);
	pData[2] = HIBYTE(uSize);
	pData[3] = LOBYTE(uSize);
	pData[4] = HIBYTE(m_Spid);
	pData[5] = LOBYTE(m_Spid);
	pData[6] = BYTE(GetPacketID());
	pData[7] = 0;
	}
// End of File
