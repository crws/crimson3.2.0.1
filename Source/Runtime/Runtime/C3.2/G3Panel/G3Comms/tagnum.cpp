
#include "intern.hpp"

#include "secure.hpp"

#include <limits.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Item
//

// Constructor

CTagNumeric::CTagNumeric(void)
{
	m_Manipulate = 0;
	m_TreatAs    = 0;
	m_ScaleTo    = 0;
	m_pDataMin   = NULL;
	m_pDataMax   = NULL;
	m_pDispMin   = NULL;
	m_pDispMax   = NULL;
	m_HasSP      = 0;
	m_pSetpoint  = NULL;
	m_LimitType  = 0;
	m_pLimitMin  = NULL;
	m_pLimitMax  = NULL;
	m_pDeadband  = NULL;
	m_pEvent1    = NULL;
	m_pEvent2    = NULL;
	m_pTrigger1  = NULL;
	m_pTrigger2  = NULL;
	m_pSec       = New CSecDesc;
	m_pQuickPlot = NULL;
	m_uType      = 0;
	m_uSize      = 0;
	m_pData      = NULL;
	m_fPoll	     = FALSE;
	}

// Destructor

CTagNumeric::~CTagNumeric(void)
{
	delete m_pDataMin;
	delete m_pDataMax;
	delete m_pDispMin;
	delete m_pDispMax;
	delete m_pSetpoint;
	delete m_pLimitMin;
	delete m_pLimitMax;
	delete m_pDeadband;
	delete m_pEvent1;
	delete m_pEvent2;
	delete m_pTrigger1;
	delete m_pTrigger2;
	delete m_pQuickPlot;
	delete m_pData;
	}

// Initialization

void CTagNumeric::Load(PCBYTE &pData)
{
	ValidateLoad("CTagNumeric", pData);

	CDataTag::Load(pData);

	if( TRUE ) {

		m_Manipulate = GetByte(pData);
		m_TreatAs    = GetByte(pData);
		m_ScaleTo    = GetByte(pData);
		m_HasSP      = GetByte(pData);
		m_LimitType  = GetByte(pData);
		}

	if( m_ScaleTo ) {

		GetCoded(pData, m_pDataMin);
		GetCoded(pData, m_pDataMax);
		GetCoded(pData, m_pDispMin);
		GetCoded(pData, m_pDispMax);
		}

	if( m_HasSP ) {

		GetCoded(pData, m_pSetpoint);
		}

	if( TRUE ) {

		GetCoded(pData, m_pLimitMin);
		GetCoded(pData, m_pLimitMax);
		GetCoded(pData, m_pDeadband);
		}

	if( TRUE ) {

		m_pEvent1   = CTagEventNumeric  ::Create(pData);
		m_pEvent2   = CTagEventNumeric  ::Create(pData);
		m_pTrigger1 = CTagTriggerNumeric::Create(pData);
		m_pTrigger2 = CTagTriggerNumeric::Create(pData);
		}

	if( TRUE ) {

		m_pSec->Load(pData);
		}

	if( GetByte(pData) ) {

		m_pQuickPlot = New CTagQuickPlot(this);

		m_pQuickPlot->Load(pData);
		}

	GetCoded(pData, m_pOnWrite);

	InitType();

	InitData();
	}

// Attributes

UINT CTagNumeric::GetDataType(void) const
{
	return m_uType;
	}

// Operations

void CTagNumeric::SetIndex(UINT uIndex)
{
	if( m_pEvent1 ) {
		
		m_pEvent1->SetIndex(MAKELONG(uIndex, 0x0100));

		m_pEvent1->SetCount(m_Extent);
		}

	if( m_pEvent2 ) {
		
		m_pEvent2->SetIndex(MAKELONG(uIndex, 0x0200));

		m_pEvent2->SetCount(m_Extent);
		}

	if( m_pTrigger1 ) {

		m_pTrigger1->SetCount(m_Extent);
		}

	if( m_pTrigger2 ) {

		m_pTrigger2->SetCount(m_Extent);
		}

	CTag::SetIndex(uIndex);
	}

BOOL CTagNumeric::SetPollScan(UINT uCode)
{
	if( m_pEvent1 ) {
		
		m_pEvent1->SetScan(uCode);

		m_fPoll = TRUE;
		}

	if( m_pEvent2 ) {
		
		m_pEvent2->SetScan(uCode);

		m_fPoll = TRUE;
		}

	if( m_pTrigger1 ) {
		
		m_pTrigger1->SetScan(uCode);

		m_fPoll = TRUE;
		}

	if( m_pTrigger2 ) {
		
		m_pTrigger2->SetScan(uCode);

		m_fPoll = TRUE;
		}

	if( m_pSetpoint ) {

		m_fPoll = TRUE;
		}

	if( m_fPoll ) {

		UINT c = m_Extent ? m_Extent : 1;

		for( UINT n = 0; n < c; n++ ) {

			CDataRef Ref;

			Ref.m_Ref     = 0;

			Ref.x.m_Array = n;

			SetScan(Ref, uCode);
			}

		SetItemScan(m_pSetpoint, uCode);

		return TRUE;
		}

	return FALSE;
	}

void CTagNumeric::Poll(UINT uDelta)
{
	if( m_fPoll ) {

		UINT c = m_Extent ? m_Extent : 1;

		if( !IsItemAvail(m_pSetpoint) ) {

			return;
			}

		for( UINT n = 0; n < c; n++ ) {

			CDataRef Ref;

			Ref.m_Ref     = 0;

			Ref.x.m_Array = n;

			if( IsAvail(Ref, availNone) ) {

				DWORD SP = 0;

				DWORD PV = GetData(Ref, m_uType, getNone);

				if( m_pSetpoint ) {

					SP = m_pSetpoint->Execute(m_uType, PDWORD(&n));
					}

				if( n < 256 ) {

					if( m_pEvent1 ) {
						
						m_pEvent1->Poll(n, SP, PV, m_uType, uDelta);
						}

					if( m_pEvent2 ) {
						
						m_pEvent2->Poll(n, SP, PV, m_uType, uDelta);
						}
					}

				if( m_pTrigger1 ) {
					
					m_pTrigger1->Poll(n, SP, PV, m_uType, uDelta);
					}

				if( m_pTrigger2 ) {
					
					m_pTrigger2->Poll(n, SP, PV, m_uType, uDelta);
					}
				}
			}
		}
	}

BOOL CTagNumeric::GetEventText(CUnicode &Text, UINT uItem, UINT uPos)
{
	if( uItem == 1 ) {

		if( m_pEvent1 ) {

			return m_pEvent1->GetEventText(Text, uPos);
			}
		}

	if( uItem == 2 ) {

		if( m_pEvent2 ) {

			return m_pEvent2->GetEventText(Text, uPos);
			}
		}

	return FALSE;
	}

// Quick Plot

BOOL CTagNumeric::GetQuickPlot(CTagQuickPlot * &pQuick)
{
	pQuick = m_pQuickPlot;

	return pQuick ? TRUE : FALSE;
	}

// Evaluation

BOOL CTagNumeric::IsAvail(CDataRef const &Ref, UINT Flags)
{
	if( !IsItemAvail(m_pLimitMin) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pLimitMax) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pDeadband) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pDataMin) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pDataMax) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pDispMin) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pDispMax) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pOnWrite) ) {

		return FALSE;
		}

	if( LocalData() ) {

		return TRUE;
		}

	if( m_Ref ) {

		if( m_pBlock ) {

			if( (Flags & availData) || !m_Extent || !m_RdMode ) {

				int nPos = FindPos(Ref);
				
				return m_pBlock->IsAvail(nPos);
				}

			return TRUE;
			}

		return FALSE;
		}

	return CDataTag::IsAvail(Ref, 0);
	}

BOOL CTagNumeric::SetScan(CDataRef const &Ref, UINT Code)
{
	SetItemScan(m_pLimitMin, Code);
	
	SetItemScan(m_pLimitMax, Code);

	SetItemScan(m_pDeadband, Code);

	SetItemScan(m_pDataMin,  Code);
	
	SetItemScan(m_pDataMax,  Code);
	
	SetItemScan(m_pDispMin,  Code);
	
	SetItemScan(m_pDispMax,  Code);

	SetItemScan(m_pOnWrite,  Code);

	if( LocalData() ) {

		return TRUE;
		}

	if( m_Ref ) {

		if( m_pBlock ) {

			if( m_Extent ) {

				if( !m_RdMode ) {

					for( UINT i = 0; i < m_uRegs; i++ ) {

						int nPos = m_nPos + i;

						m_pBlock->SetScan(nPos, Code);
						}
					}

				else if( m_RdMode == 1 ) {

					if( Code == scanUser ) {

						int nPos = FindPos(Ref);

						return m_pBlock->SetScan(nPos, Code);
						}

					return FALSE;
					}

				return TRUE;
				}

			int nPos = FindPos(Ref);

			return m_pBlock->SetScan(nPos, Code);
			}

		return FALSE;
		}

	return CDataTag::SetScan(Ref, Code);
	}

DWORD CTagNumeric::GetData(CDataRef const &Ref, UINT Type, UINT Flags)
{
	if( Type ) {

		if( Type == typeReal && IsInteger(m_uType) ) {

			DWORD Data = GetData(Ref, typeInteger, Flags);

			return R2I(C3REAL(C3INT(Data)));
			}

		if( m_uType == typeReal && IsInteger(Type) ) {

			DWORD Data = GetData(Ref, typeReal, Flags);

			return C3INT(I2R(Data));
			}
		}

	if( Type == m_uType || Type == typeVoid ) {

		UINT n = Ref.x.m_Array;

		if( n < m_uSize ) {

			if( LocalData() ) {

				DWORD Data = m_pData[n];

				TransToDisp(Data, Flags, n);

				return Data;
				}

			if( m_Ref ) {

				if( m_pBlock ) {

					int nPos = FindPos(Ref);

					if( m_Extent ) {

						if( m_RdMode >= 2 ) {

							int nSpan = m_RdMode - 2;

							int nFrom = nPos - (nSpan+0);

							int nTo   = nPos + (nSpan+1);

							for( int n = nFrom; n < nTo; n++ ) {

								m_pBlock->SetScan(n, scanOnce);
								}
							}
						}

					DWORD Data = m_pBlock->GetData(nPos);

					TransToDisp(Data, Flags, n);

					return Data;
					}

				return GetNull(Type);
				}

			DWORD Data = CDataTag::GetData(Ref, Type, Flags);

			TransToDisp(Data, Flags, n);

			return Data;
			}
		}

	return GetNull(Type);
	}

BOOL CTagNumeric::IsValid(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	if( Type == m_uType ) {

		if( (Flags & getMaskSource) <= getScaled ) {

			DWORD Min = FindMin(Ref, Type);

			DWORD Max = FindMax(Ref, Type);

			if( Type == typeInteger ) {

				if( C3INT(Data) < C3INT(Min) ) {

					return FALSE;
					}

				if( C3INT(Data) > C3INT(Max) ) {

					return FALSE;
					}

				return TRUE;
				}

			if( Type == typeReal ) {

				if( I2R(Data) < I2R(Min) ) {

					return FALSE;
					}

				if( I2R(Data) > I2R(Max) ) {

					return FALSE;
					}

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagNumeric::SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	if( Type ) {

		if( m_uType == typeReal && IsInteger(Type) ) {

			Data = R2I(C3REAL(C3INT(Data)));

			Type = m_uType;
			}

		if( Type == typeReal && IsInteger(m_uType) ) {

			Data = C3INT(I2R(Data));

			Type = m_uType;
			}
		}

	if( Type == m_uType || Type == typeVoid ) {

		UINT n = Ref.x.m_Array;

		if( n < m_uSize ) {

			if( m_Access < 2 || m_pOnWrite ) {

				if( !(Flags & setForce) ) {

					DWORD Read = GetData(Ref, Type, Flags);

					if( Read == Data ) {

						return TRUE;
						}
					}

				if( !AllowWrite(Ref, Data, Type) ) {

					return FALSE;
					}

				if( m_pOnWrite ) {

					DWORD Args[] = { Data, n };

					m_pOnWrite->Execute(typeVoid, Args);
					}

				if( m_Access < 2 ) {

					if( TRUE ) {

						TransToData(Data, Flags, n);
						}

					if( LocalData() ) {

						m_pData[n] = Data;

						if( m_Addr ) {

							SaveData(n);
							}

						if( !m_Ref ) {

							return TRUE;
							}
						}

					if( m_Ref ) {

						if( m_pBlock ) {

							int nPos = FindPos(Ref);

							m_pBlock->SetWriteData(nPos, Flags, Data);

							return TRUE;
							}

						return CDataTag::SetData(Ref, Data, Flags, Type);
						}

					return m_pValue->SetValue(Data, typeVoid, Flags);
					}
				}
			}
		}

	return CDataTag::SetData(Ref, Data, Flags, Type);
	}

DWORD CTagNumeric::GetProp(CDataRef const &Ref, WORD ID, UINT Type)
{
	switch( ID ) {

		case tpPrefix:
			return FindPrefix(Ref);

		case tpUnits:
			return FindUnits(Ref);

		case tpSetPoint:
			return FindSP (Ref, Type ? Type : GetDataType());

		case tpMinimum:
			return FindMin(Ref, Type ? Type : GetDataType());

		case tpMaximum:
			return FindMax(Ref, Type ? Type : GetDataType());

		case tpAlarms:
			return FindEventStatusMask(Ref);
		}

	if( ID == tpStateCount ) {

		return FindMax(Ref, typeInteger) + 1;
		}

	if( ID >= tpStateText && ID < tpStateText + 1000 ) {

		return FindAsText(Ref, typeInteger, ID - tpStateText);
		}

	return CDataTag::GetProp(Ref, ID, Type);
	}

// Deadband

BOOL CTagNumeric::HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev)
{
	if( Prev == 0x80000000 ) {

		Data = GetData(Ref, m_uType, getNone);

		return TRUE;
		}
	else {
		if( m_uType == typeReal ) {

			C3REAL Read = I2R(GetData(Ref, typeReal, getNone));

			C3REAL Last = I2R(Prev);

			C3REAL Dead = I2R(FindDeadband(Ref, typeReal));

			if( Dead ? (fabs(Read - Last) >= Dead) : (Read != Last) ) {

				Data = R2I(Read);

				return TRUE;
				}

			return FALSE;
			}

		if( m_uType == typeInteger ) {

			C3INT Read = GetData(Ref, typeInteger, getNone);

			C3INT Last = C3INT(Prev);

			C3INT Dead = FindDeadband(Ref, typeInteger);

			if( Dead ? (abs(Read - Last) >= Dead) : (Read != Last) ) {

				Data = Read;

				return TRUE;
				}

			return FALSE;
			}
		}

	return CTag::HasChanged(Ref, Data, Prev);
	}

// Property Access

DWORD CTagNumeric::FindPrefix(CDataRef const &Ref)
{
	if( m_FormType == 1 ) {

		CDispFormatNumber *pNumber = (CDispFormatNumber *) m_pFormat;

		if( pNumber->m_pPrefix ) {

			DWORD n = Ref.x.m_Array;

			return pNumber->m_pPrefix->Execute(typeString, &n);
			}
		}

	if( m_FormType == 2 ) {

		CDispFormatSci *pSci = (CDispFormatSci *) m_pFormat;

		if( pSci->m_pPrefix ) {

			DWORD n = Ref.x.m_Array;

			return pSci->m_pPrefix->Execute(typeString, &n);
			}
		}

	return DWORD(wstrdup(L""));
	}

DWORD CTagNumeric::FindUnits(CDataRef const &Ref)
{
	if( m_FormType == 1 ) {

		CDispFormatNumber *pNumber = (CDispFormatNumber *) m_pFormat;

		if( pNumber->m_pUnits ) {

			DWORD n = Ref.x.m_Array;

			return pNumber->m_pUnits->Execute(typeString, &n);
			}
		}

	if( m_FormType == 2 ) {

		CDispFormatSci *pSci = (CDispFormatSci *) m_pFormat;

		if( pSci->m_pUnits ) {

			DWORD n = Ref.x.m_Array;

			return pSci->m_pUnits->Execute(typeString, &n);
			}
		}

	return DWORD(wstrdup(L""));
	}

DWORD CTagNumeric::FindSP(CDataRef const &Ref, UINT Type)
{
	if( m_pSetpoint ) {

		DWORD n = Ref.x.m_Array;

		return m_pSetpoint->Execute(Type, &n);
		}

	return GetNull(Type);
	}

DWORD CTagNumeric::FindMin(CDataRef const &Ref, UINT Type)
{
	if( !m_pFormat || m_pFormat->NeedsLimits() ) {

		if( m_pLimitMin ) {

			DWORD n = Ref.x.m_Array;

			DWORD x = m_pLimitMin->Execute(Type, &n);

			return x;
			}

		if( m_LimitType == 0 ) {

			if( m_pDispMin ) {

				DWORD n = Ref.x.m_Array;

				return m_pDispMin->Execute(Type, &n);
				}
			}
		}

	if( m_pFormat ) {

		return m_pFormat->GetMin(Type);
		}

	if( Type == typeInteger ) {

		return 0;
		}

	return R2I(0);
	}

DWORD CTagNumeric::FindMax(CDataRef const &Ref, UINT Type)
{
	if( !m_pFormat || m_pFormat->NeedsLimits() ) {

		if( m_pLimitMax ) {

			DWORD n = Ref.x.m_Array;

			return m_pLimitMax->Execute(Type, &n);
			}

		if( m_LimitType == 0 ) {

			if( m_pDispMax ) {

				DWORD n = Ref.x.m_Array;

				return m_pDispMax->Execute(Type, &n);
				}
			}
		}

	if( m_pFormat ) {

		return m_pFormat->GetMax(Type);
		}

	if( Type == typeInteger ) {

		return 999999;
		}

	return R2I(999999);
	}

DWORD CTagNumeric::FindDeadband(CDataRef const &Ref, UINT Type)
{
	if( !m_pFormat || m_pFormat->NeedsLimits() ) {

		if( m_pDeadband ) {

			DWORD n = Ref.x.m_Array;

			return m_pDeadband->Execute(Type, &n);
			}
		}

	if( Type == typeInteger ) {

		return 0;
		}

	return R2I(0);
	}

DWORD CTagNumeric::FindEventStatusMask(CDataRef const &Ref)
{
	DWORD n = Ref.x.m_Array;

	DWORD m = 0;

	if( m_pEvent1 && m_pEvent1->IsActive(n) ) {

		m |= 1;
		}

	if( m_pEvent2 && m_pEvent2->IsActive(n) ) {

		m |= 2;
		}

	return m;
	}

// Implementation

void CTagNumeric::InitType(void)
{
	switch( m_ScaleTo ) {

		case 0:
			switch( m_TreatAs ) {

				case 0:
				case 1:
				case 3:
					m_uType = typeInteger;
					
					return;

				case 2:
					m_uType = typeReal;
					
					return;
				}
			break;

		case 1:
			m_uType = typeInteger;

			return;

		case 2:
			m_uType = typeReal;

			return;
		}

	AfxAssert(FALSE);

	m_uType = typeVoid;
	}

BOOL CTagNumeric::InitData(void)
{
	if( LocalData() ) {

		m_uSize = max(m_Extent, 1);

		m_pData = New DWORD [ m_uSize ];

		if( m_pSim ) {

			DWORD Data = m_pSim->ExecVal();

			for( UINT n = 0; n < m_uSize; n++ ) {

				m_pData[n] = Data;
				}
			}
		else {
			if( m_Addr ) {

				g_pPersist->GetData( PBYTE(m_pData),
						m_Addr,
						m_uSize * sizeof(DWORD)
						);
				}
			else {
				if( GetDataType() == typeInteger ) {

					for( UINT n = 0; n < m_uSize; n++ ) {

						m_pData[n] = 0;
						}
					}
				else {
					for( UINT n = 0; n < m_uSize; n++ ) {

						m_pData[n] = R2I(0.0);
						}
					}
				}
			}

		return TRUE;
		}

	m_uSize = max(m_Extent, 1);

	return FALSE;
	}

void CTagNumeric::SaveData(UINT n)
{
	UINT a = m_Addr + n * sizeof(DWORD);

	LONG d = m_pData[n];

	g_pPersist->PutLong(a, d);
	}

int CTagNumeric::FindPos(CDataRef const &Ref)
{
	return m_nPos + Ref.x.m_Array;
	}

void CTagNumeric::TransToDisp(DWORD &Data, UINT Flags, UINT uPos)
{
	if( (Flags & getMaskSource) <= getManipulated ) {

		Manipulate(Data, TRUE);
		}

	if( (Flags & getMaskSource) <= getScaled ) {

		ScaleToDisp(Data, uPos);
		}
	}
						
void CTagNumeric::TransToData(DWORD &Data, UINT Flags, UINT uPos)
{
	if( (Flags & setMaskSource) <= setScaled ) {

		ScaleToData(Data, uPos);
		}

	if( (Flags & setMaskSource) <= setManipulated ) {

		Manipulate(Data, FALSE);
		}
	}

void CTagNumeric::Manipulate(DWORD &Data, BOOL fGet)
{
	if( m_Manipulate == 1 ) {

		Data = Data ^ FindMask();

		return;
		}

	if( m_Manipulate == 2 ) {

		DWORD Work = 0;

		DWORD BitD = (1 << (m_uBits - 1));

		DWORD BitW = 1;

		while( BitD ) {

			if( Data & BitD ) {

				Work |= BitW;
				}

			BitD >>= 1;
			BitW <<= 1;
			}

		Data = Work;

		return;
		}

	if( m_Manipulate == 3 ) {

		if( m_uBits == 16 ) {

			BYTE b0 = LOBYTE(LOWORD(Data));
			BYTE b1 = HIBYTE(LOWORD(Data));

			Data = MAKEWORD(b1, b0);
	
			return;
			}

		if( m_uBits == 32 ) {

			BYTE b0 = LOBYTE(LOWORD(Data));
			BYTE b1 = HIBYTE(LOWORD(Data));
			BYTE b2 = LOBYTE(HIWORD(Data));
			BYTE b3 = HIBYTE(HIWORD(Data));

			Data = MAKELONG(MAKEWORD(b3,b2),MAKEWORD(b1, b0));
	
			return;
			}
	
		return;
		}

	if( m_Manipulate == 4 ) {

		if( m_uBits == 32 ) {

			WORD w0 = LOWORD(Data);
			WORD w1 = HIWORD(Data);

			Data = MAKELONG(w1, w0);
	
			return;
			}
	
		return;
		}

	if( m_Manipulate == 5 ) {

		DWORD Work = 0;

		UINT  s1   = 1;

		UINT  s2   = 1;

		UINT  f1   = fGet ? 16 : 10;

		UINT  f2   = fGet ? 10 : 16;

		for( UINT n = 0; n < m_uBits / 4; n++ ) {

			Work += s2 * ((Data / s1) % f1);

			s1   *= f1;

			s2   *= f2;
			}

		Data = Work;
		}
	}

void CTagNumeric::ScaleToDisp(DWORD &Data, UINT uPos)
{
	if( m_TreatAs == 0 ) {

		if( m_uBits < 32 ) {

			if( Data & FindSignBit() ) {

				Data |= ~FindMask();
				}
			}
		}

	if( m_TreatAs == 1 ) {

		if( m_uBits < 32 ) {

			Data &= FindMask();
			}
		}

	if( m_ScaleTo ) {

		double rData    = 0;

		double rDataMin = 0;
		
		double rDataMax = 0;

		double rDispMin = 0;
		
		double rDispMax = 0;

		if( m_TreatAs == 2 ) {

			rData    = I2R(Data);

			rDataMin = GetItemData(m_pDataMin, C3REAL(  0.0), PDWORD(&uPos));

			rDataMax = GetItemData(m_pDataMax, C3REAL(100.0), PDWORD(&uPos));
			}
		else {
			rData    = C3INT(Data);

			rDataMin = GetItemData(m_pDataMin, C3INT(  0), PDWORD(&uPos));

			rDataMax = GetItemData(m_pDataMax, C3INT(100), PDWORD(&uPos));
			}

		if( rDataMax == rDataMin ) {

			rData  = 1.0;
			}
		else {
			rData -= rDataMin;

			rData /= rDataMax - rDataMin;
			}

		if( m_ScaleTo == 1 ) {

			rDispMin = GetItemData(m_pDispMin, C3INT(  0), PDWORD(&uPos));

			rDispMax = GetItemData(m_pDispMax, C3INT(100), PDWORD(&uPos));
			}

		if( m_ScaleTo == 2 ) {

			rDispMin = GetItemData(m_pDispMin, C3REAL(  0.0), PDWORD(&uPos));

			rDispMax = GetItemData(m_pDispMax, C3REAL(100.0), PDWORD(&uPos));
			}

		if( TRUE ) {

			rData *= rDispMax - rDispMin;

			rData += rDispMin;
			}

		if( m_ScaleTo == 1 ) {

			rData = Round(rData);

			MakeMin(rData, INT_MAX);

			MakeMax(rData, INT_MIN);

			Data = C3INT(rData);
			}

		if( m_ScaleTo == 2 ) {

			Data = R2I(rData);
			}
		}
	}

void CTagNumeric::ScaleToData(DWORD &Data, UINT uPos)
{
	if( m_ScaleTo ) {

		double rData    = 0;

		double rDataMin = 0;
		
		double rDataMax = 0;

		double rDispMin = 0;
		
		double rDispMax = 0;

		if( m_ScaleTo == 1 ) {

			rData    = C3INT(Data);

			rDispMin = GetItemData(m_pDispMin, C3INT(  0), PDWORD(&uPos));

			rDispMax = GetItemData(m_pDispMax, C3INT(100), PDWORD(&uPos));
			}

		if( m_ScaleTo == 2 ) {

			rData    = I2R(Data);

			rDispMin = GetItemData(m_pDispMin, C3REAL(  0.0), PDWORD(&uPos));

			rDispMax = GetItemData(m_pDispMax, C3REAL(100.0), PDWORD(&uPos));
			}

		if( rDispMax == rDispMin ) {

			rData  = 1.0;
			}
		else {
			rData -= rDispMin;

			rData /= rDispMax - rDispMin;
			}

		if( m_TreatAs == 2 ) {

			rDataMin = GetItemData(m_pDataMin, C3REAL(  0.0), PDWORD(&uPos));

			rDataMax = GetItemData(m_pDataMax, C3REAL(100.0), PDWORD(&uPos));
			}
		else {
			rDataMin = GetItemData(m_pDataMin, C3INT(  0), PDWORD(&uPos));

			rDataMax = GetItemData(m_pDataMax, C3INT(100), PDWORD(&uPos));
			}

		if( TRUE ) {

			rData *= rDataMax - rDataMin;

			rData += rDataMin;
			}

		if( m_TreatAs == 0 || m_TreatAs == 3 ) {

			rData = Round(rData);

			DWORD Bit = FindSignBit();

			MakeMax(rData, double(Bit) * -1.0);

			MakeMin(rData, double(Bit) + -1.0);

			Data = C3INT(rData);
			}

		if( m_TreatAs == 1 ) {
			
			rData = Round(rData);

			MakeMax(rData, 0);

			MakeMin(rData, FindMask());

			Data = DWORD(rData);
			}

		if( m_TreatAs == 2 ) {

			Data = R2I(rData);
			}
		}
	}

DWORD CTagNumeric::FindMask(void)
{
	return (0xFFFFFFFF >> (32 - m_uBits));
	}

DWORD CTagNumeric::FindSignBit(void)
{
	return (0x80000000 >> (32 - m_uBits));
	}

double CTagNumeric::Round(double rData)
{
	// REV3 -- Make rounding optional?

	if( rData ) {

		if( rData > 0 ) {

			return floor(rData + 0.5);
			}
		else
			return ceil (rData - 0.5);
		}

	return 0;
	}

// End of File
