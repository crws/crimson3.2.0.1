
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "datalog.hpp"

#include "lang.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Buffered File
//

// Static Data

static UINT const m_uBits = 1024;

// Constructor

CFastFile::CFastFile(void)
{
	m_fOpen = FALSE;

	m_uSize = 16384;

	m_pData = NULL;

	m_pSalt = NULL;

	m_uPtr  = 0;

	m_fSign = FALSE;

	m_cSep  = char(CCommsSystem::m_pThis->m_pLang->GetListSepChar());

	m_pFile = NULL;
}

// Destructor

CFastFile::~CFastFile(void)
{
	if( m_fOpen ) {

		fclose(m_pFile);
	}

	if( m_fSign ) {

		delete[] m_pSalt;

		return;
	}

	delete[] m_pData;
}

// Conversion

FILE * CFastFile::GetFile(void)
{
	return m_pFile;
}

BOOL CFastFile::IsValid(void)
{
	return m_fOpen;
}

// Operations

BOOL CFastFile::Close(void)
{
	if( m_fOpen ) {

		m_fOpen = FALSE;

		Commit();

		fclose(m_pFile);

		m_pFile = NULL;

		return TRUE;
	}

	return FALSE;
}

BOOL CFastFile::Commit(void)
{
	if( m_uPtr ) {

		if( m_fSign ) {

			m_uPtr = m_uSig;

			SignData();

			// NOTE -- Keep the signature block in the salt buffer so
			// that we can use it in the next signature calculation to
			// ensure that blocks are not moved around in a file.

			m_uSaltValid = m_uPtr - m_uSig - 1;

			memcpy(m_pData - m_uSaltValid, m_pData + m_uSig + 1, m_uSaltValid);
		}

		SetTaskLimit(25, 5);

		fwrite(m_pData, 1, m_uPtr, m_pFile);

		SetTaskLimit(0, 0);

		m_uPtr = 0;

		return TRUE;
	}

	return FALSE;
}

BOOL CFastFile::Switch(BOOL &fHead, PCTXT pName)
{
	Close();

	if( !(m_pFile = fopen(pName, "r+")) ) {

		if( !(m_pFile = fopen(pName, "w+")) ) {

			return FALSE;
		}

		if( m_fSign ) {

			m_uSaltValid = 0;
		}

		fHead = TRUE;
	}
	else {
		if( m_fSign ) {

			// NOTE -- If we're re-opning a file, scan backwards to find
			// the last signature block and copy it into the salt buffer
			// so that we can use it in the next signature calculation.

			AllocBuffer();

			fseek(m_pFile, -int(m_uSaltAlloc), SEEK_END);

			fread(m_pSalt, sizeof(BYTE), m_uSaltAlloc, m_pFile);

			for( UINT n = m_uSaltAlloc - 1; n; n-- ) {

				if( m_pSalt[n] == m_cSep ) {

					m_uSaltValid = m_uSaltAlloc - n - 1;

					m_fOpen      = TRUE;

					return TRUE;
				}
			}

			m_uSaltValid = 0;
		}
		else
			fseek(m_pFile, 0, SEEK_END);
	}

	m_fOpen = TRUE;

	return TRUE;
}

BOOL CFastFile::Write(PCBYTE pData, UINT uSize)
{
	if( m_fOpen ) {

		AllocBuffer();

		if( uSize >= m_uSize - 1024 ) {

			// NOTE -- This must never happen when we're signing!

			Commit();

			fwrite(pData, 1, uSize, m_pFile);
		}
		else {
			if( m_uPtr + uSize > m_uSize ) {

				Commit();
			}

			memcpy(m_pData + m_uPtr, pData, uSize);

			m_uPtr += uSize;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CFastFile::Write(CUnicode const &Wide)
{
	PCUTF s = Wide;

	PTXT  p = PTXT(alloca(Wide.GetLength()+1));

	UINT  n;

	for( n = 0; (p[n] = BYTE(s[n])); n++ );

	return Write(PCBYTE(p), n);
}

BOOL CFastFile::Write(PCTXT pText)
{
	return Write(PCBYTE(pText), strlen(pText));
}

BOOL CFastFile::Write(char cData)
{
	return Write(PCBYTE(&cData), 1);
}

BOOL CFastFile::VPrintf(PCTXT pText, va_list pArgs)
{
	if( m_fOpen ) {

		Commit(1024);

		m_uPtr += VSPrintf(PTXT(m_pData + m_uPtr), pText, pArgs);

		return TRUE;
	}

	return FALSE;
}

BOOL CFastFile::Printf(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	VPrintf(pText, pArgs);

	va_end(pArgs);

	return TRUE;
}

// UTF-8 Encoding

BOOL CFastFile::AddBOM(void)
{
	CString Line = UtfConvert(CUnicode(L"\xFEFF"));

	Write(PBYTE(PCTXT(Line)), Line.GetLength());

	return TRUE;
}

// Signing

BOOL CFastFile::SignEnable(void)
{
	PCBYTE p  = NICGetMac();

	m_bMAC[0] = p[3];

	m_bMAC[1] = p[4];

	m_bMAC[2] = p[5];

	m_pHex    = "0123456789ABCDEF";

	m_fSign   = TRUE;

	return TRUE;
}

void CFastFile::EndOfLine(void)
{
	if( m_fOpen && m_fSign ) {

		m_uPtr = m_uPtr - 2;

		m_uSig = m_uPtr;

		UINT uMono = GetMonotonic();

		SaveSigBlock('0', uMono);

		SaveCRLF();

		// NOTE -- This commit number must be large enough
		// for any possible line to still fit in the buffer.

		Commit(4096);
	}
}


// Signing

UINT CFastFile::GetMonotonic(void)
{
	// NOTE -- This doesn't need to be interlocked as we
	// can accept the same sequence number being used twice
	// into two different files. The only condition tha
	// must be maintained is monotonicity within a file.

	UINT x;

	FRAMGetData(Mem(FastFile), PBYTE(&x), sizeof(x));

	x++;

	FRAMPutData(Mem(FastFile), PBYTE(&x), sizeof(x));

	return x;
}

void CFastFile::SaveSigBlock(char cType, UINT uMono)
{
	m_pData[m_uPtr++] = m_cSep;

	m_pData[m_uPtr++] = '0';

	m_pData[m_uPtr++] = cType;

	m_pData[m_uPtr++] = '-';

	m_pData[m_uPtr++] = m_pHex[(uMono>>28) & 15];
	m_pData[m_uPtr++] = m_pHex[(uMono>>24) & 15];
	m_pData[m_uPtr++] = m_pHex[(uMono>>20) & 15];
	m_pData[m_uPtr++] = m_pHex[(uMono>>16) & 15];
	m_pData[m_uPtr++] = m_pHex[(uMono>>12) & 15];
	m_pData[m_uPtr++] = m_pHex[(uMono>> 8) & 15];
	m_pData[m_uPtr++] = m_pHex[(uMono>> 4) & 15];
	m_pData[m_uPtr++] = m_pHex[(uMono>> 0) & 15];

	m_pData[m_uPtr++] = '-';

	m_pData[m_uPtr++] = m_pHex[m_bMAC[0]/16];
	m_pData[m_uPtr++] = m_pHex[m_bMAC[0]%16];
	m_pData[m_uPtr++] = m_pHex[m_bMAC[1]/16];
	m_pData[m_uPtr++] = m_pHex[m_bMAC[1]%16];
	m_pData[m_uPtr++] = m_pHex[m_bMAC[2]/16];
	m_pData[m_uPtr++] = m_pHex[m_bMAC[2]%16];
}

void CFastFile::SaveCRLF(void)
{
	m_pData[m_uPtr++] = '\r';

	m_pData[m_uPtr++] = '\n';
}

void CFastFile::SignData(void)
{
	UINT  uMono = GetMonotonic();

	PBYTE pData = m_pData - m_uSaltValid;

	UINT  uData = m_uPtr  + m_uSaltValid;

	SetTaskLimit(50, 10);

	AfxNewAutoObject(pSign, "crypto.sign-legacy", ICryptoSign);

	if( pSign ) {

		static char const SLOW sPass[] = "98@(73as24!*&!@hkkh";

		if( pSign->Initialize(NULL, m_uBits, sPass) ) {

			pSign->Update(pData, uData);

			pSign->Finalize();

			UINT  uSig = pSign->GetSigSize();

			PBYTE pSig = New BYTE[uSig];

			memcpy(pSig, pSign->GetSigData(), uSig);

			SaveSigBlock('1', uMono);

			SaveSign(pSig, uSig);

			SaveCRLF();

			delete[] pSig;
		}
	}

	SetTaskLimit(0, 0);
}

void CFastFile::SaveSign(PBYTE pSign, UINT uSign)
{
	UINT uValid = uSign;

	UINT uTotal = m_uBits / 8;

	m_pData[m_uPtr++] = '-';

	for( UINT n = 0; n < uTotal; n++ ) {

		if( n < uValid ) {

			m_pData[m_uPtr++] = m_pHex[pSign[n]/16];

			m_pData[m_uPtr++] = m_pHex[pSign[n]%16];
		}
		else {
			m_pData[m_uPtr++] = '0';

			m_pData[m_uPtr++] = '0';
		}
	}
}

// Implementation

BOOL CFastFile::AllocBuffer(void)
{
	if( !m_pData ) {

		if( m_fSign ) {

			// NOTE -- If we're signing, use a large buffer and allocate
			// enough space before the buffer for us to store the last
			// signature block to include in the next signature round.

			m_uSaltAlloc = 64 + m_uBits / 4;

			m_uSize      = 2 * m_uSize;

			m_pSalt      = New BYTE[m_uSize + m_uSaltAlloc];

			m_pData      = m_pSalt + m_uSaltAlloc;
		}
		else {
			m_uSaltAlloc = 0;

			m_pData      = New BYTE[m_uSize];
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CFastFile::Commit(UINT uLimit)
{
	AllocBuffer();

	if( m_uPtr + uLimit > m_uSize ) {

		Commit();

		return TRUE;
	}

	return FALSE;
}

// End of File
