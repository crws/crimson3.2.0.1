
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client Options
//

// Constructor

CMqttClientOptionsGeneric::CMqttClientOptionsGeneric(void)
{
	}

// Initialization

void CMqttClientOptionsGeneric::Load(PCBYTE &pData)
{
	CMqttClientOptionsJson::Load(pData);

	GetCoded(pData, m_PubTopic);

	GetCoded(pData, m_SubTopic);

	m_NoDollar = GetByte(pData);
	}

// Config Fixup

BOOL CMqttClientOptionsGeneric::FixConfig(void)
{
	FixCoded(m_PubTopic, FALSE);

	FixCoded(m_SubTopic, FALSE);

	if( CMqttClientOptionsJson::FixConfig() ) {

		SetDefaults();

		return TRUE;
	}

	return FALSE;
}

// Attributes

CString CMqttClientOptionsGeneric::GetExtra(void) const
{
	CString Extra;

	Extra += m_PubTopic + '&';
	
	Extra += m_SubTopic;

	return Extra;
	}

// Implementation

void CMqttClientOptionsGeneric::SetDefaults(void)
{
	if( m_PubTopic.IsEmpty() ) {

		m_PubTopic = "$crimson/generic/" + m_ClientId + "/pub";

		if( m_NoDollar ) {

			m_PubTopic.Delete(0, 1);
			}
		}

	if( m_SubTopic.IsEmpty() ) {

		m_SubTopic = "$crimson/generic/" + m_ClientId + "/sub";

		if( m_NoDollar ) {

			m_SubTopic.Delete(0, 1);
			}
		}
	}

// End of File
