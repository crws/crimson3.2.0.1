
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LOGHELP_HPP
	
#define	INCLUDE_LOGHELP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Interfaces
//

interface ISign;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CFastFile;
class CLogHelper;

//////////////////////////////////////////////////////////////////////////
//
// Buffered File
//

class CFastFile
{
	public:
		// Constructor
		CFastFile(void);

		// Destructor
		~CFastFile(void);

		// Attributes
		FILE * GetFile(void);
		BOOL   IsValid(void);

		// Operations
		BOOL Close(void);
		BOOL Commit(void);
		BOOL Switch(BOOL &fHead, PCTXT pName);
		BOOL Write(PCBYTE pData, UINT uSize);
		BOOL Write(CUnicode const &Wide);
		BOOL Write(PCTXT pText);
		BOOL Write(char cData);
		BOOL VPrintf(PCTXT pText, va_list pArgs);
		BOOL Printf(PCTXT pText, ...);

		// UTF-8 Encoding
		BOOL AddBOM(void);

		// Signing
		BOOL SignEnable(void);
		void EndOfLine(void);

	protected:
		// Data Members
		BOOL    m_fOpen;
		FILE  * m_pFile;
		UINT    m_uSize;
		UINT	m_uSaltAlloc;
		UINT	m_uSaltValid;
		PBYTE   m_pData;
		PBYTE   m_pSalt;
		UINT    m_uPtr;
		UINT	m_uSig;
		BOOL    m_fSign;
		PCTXT   m_pHex;
		BYTE    m_bMAC[3];
		char    m_cSep;

		// Signing
		UINT GetMonotonic(void);
		void SaveSigBlock(char cType, UINT uMono);
		void SaveCRLF(void);
		void SignData(void);
		void SaveSign(PBYTE pSign, UINT uSign);

		// Implementation
		BOOL AllocBuffer(void);
		BOOL Commit(UINT uLimit);
	};

//////////////////////////////////////////////////////////////////////////
//
// Log Helper
//

class CLogHelper
{
	public:
		// Constructor
		CLogHelper(void);

		// Destructor
		~CLogHelper(void);

		// Configuration
		void SetBasePath(PCTXT pBase);
		void SetFileCount(UINT uCount);
		void SetFileLimit(UINT yLimit);
		void SetBatchInfo(UINT uSlot, UINT uBatch);
		void AddExtension(PCTXT pType);
		void SetMail(UINT uMail);
		void SetDrive(UINT uDrive, UINT uBatchDrive);

		// Operations
		void InitData(void);
		BOOL SaveInit(void);
		BOOL FindFiles(CFastFile *pFile, BOOL *pHead, UINT uType, DWORD Time);
		void SaveStep(DWORD Time);
		void SaveDone(void);
		void NewBatch(DWORD Time, PCTXT pName);

		// Static Config
		static void UseSetLayout(BOOL fSet);

	protected:
		// Static Data
		static BOOL m_fSet;

		// Config Data
		CString m_Base;
		CString m_Type[4];
		UINT    m_uType;
		UINT    m_uCount;
		UINT    m_uLimit;
		UINT    m_uSlot;
		UINT    m_uBatch;
		UINT    m_uMail;

		// Data Members
		DWORD   m_Volume;
		DWORD   m_Last;
		CString m_BatchName;
		CString m_NewBatch;
		DWORD   m_NewStart;
		CString m_Drive;
		CString m_Batch;

		// Name Generation
		CString MakeName(UINT uType, UINT uPath, DWORD Secs);
		CString MakePath(UINT uPath);

		// Implementation
		BOOL MakeNew(DWORD Secs);
		void CheckVolume(void);
		void CheckFiles(void);
		BOOL FindDirectories(void);
		void CheckGUID(void);
		void DeleteOldFiles(void);
		void DeleteOldBatches(void);
		void DeleteContents(UINT uLeave);
		void ArchiveFiles(void);
		void LockSession(void);
		void FreeSession(void);
		void EmptyCurrentDir(void);
	};

// End of File

#endif
