
#include "Intern.hpp"

#include "TdsBytes.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// TDS Byte Stream
//

// Constructor

CTdsBytes::CTdsBytes(UINT uSize)
{
	m_uAlloc = uSize;

	m_pData  = New BYTE [ uSize ];
	
	m_uPtr   = 0;
	}

CTdsBytes::CTdsBytes(CTdsBytes const &That)
{
	m_uAlloc = That.m_uAlloc;

	m_uPtr   = That.m_uPtr;

	m_pData  = New BYTE[ m_uAlloc ];

	memcpy(m_pData, That.m_pData, That.m_uPtr);
	}

// Destructor

CTdsBytes::~CTdsBytes(void)
{
	delete [] m_pData;
	}

// Assigment

CTdsBytes & CTdsBytes::operator = (CTdsBytes &That)
{
	m_uAlloc = That.m_uAlloc;

	m_uPtr   = That.m_uPtr;

	m_pData  = New BYTE[ m_uAlloc ];

	memcpy(m_pData, That.m_pData, That.m_uPtr);

	return *this;
	}

// Attributes

UINT CTdsBytes::GetSize(void) const
{
	return m_uPtr;
	}

PBYTE CTdsBytes::GetData(void) const
{
	return m_pData;
	}

// Reading

BYTE CTdsBytes::GetByte(UINT &uPos) const
{
	if( uPos < m_uPtr ) {

		return m_pData[uPos++];
		}
	return 0;
	}

USHORT CTdsBytes::GetUShort(UINT &uPos) const
{
	BYTE hi = GetByte(uPos);

	BYTE lo = GetByte(uPos);

	return MAKEWORD(lo, hi);
	}

ULONG CTdsBytes::GetULong(UINT &uPos) const
{
	USHORT hi = GetUShort(uPos);

	USHORT lo = GetUShort(uPos);

	return MAKELONG(lo, hi);
	}

ULONGLONG CTdsBytes::GetULongLong(UINT &uPos) const
{
	ULONG hi = GetULong(uPos);

	ULONG lo = GetULong(uPos);

	return (((ULONGLONG)hi) << 32) | (ULONGLONG)lo;
	}

FLOAT CTdsBytes::GetFloat(UINT &uPos) const
{
	FLOAT Value = 0;

	for (UINT uIndex=0; uIndex<4; uIndex++)
		((BYTE*)&Value)[uIndex] = GetByte(uPos);

	return Value;
	}

DOUBLE CTdsBytes::GetDouble(UINT &uPos) const
{
	DOUBLE Value = 0;

	for (UINT uIndex=0; uIndex<8; uIndex++)
		((BYTE*)&Value)[uIndex] = GetByte(uPos);

	return Value;
	}

USHORT CTdsBytes::GetUShortReverse(UINT &uPos) const
{
	BYTE lo = GetByte(uPos);

	BYTE hi = GetByte(uPos);

	return MAKEWORD(lo, hi);
	}

ULONG CTdsBytes::GetULongReverse(UINT &uPos) const
{
	USHORT lo = GetUShortReverse(uPos);

	USHORT hi = GetUShortReverse(uPos);

	return MAKELONG(lo, hi);
	}

ULONGLONG CTdsBytes::GetULongLongReverse(UINT &uPos) const
{
	ULONG lo = GetULongReverse(uPos);

	ULONG hi = GetULongReverse(uPos);

	return (((ULONGLONG)hi) << 32) | (ULONGLONG)lo;
	}

DOUBLE CTdsBytes::GetDoubleReverse(UINT &uPos) const
{
	DOUBLE Value = 0;

	for (INT iIndex=7; iIndex>=0; iIndex--)
		((BYTE*)&Value)[iIndex] = GetByte(uPos);

	return Value;
	}

FLOAT CTdsBytes::GetFloatReverse(UINT &uPos) const
{
	FLOAT Value = 0;

	for (INT iIndex=3; iIndex>=0; iIndex--)
		((BYTE*)&Value)[iIndex] = GetByte(uPos);

	return Value;
	}

PCSTR CTdsBytes::GetText(UINT &uPos) const
{
	if( uPos < m_uPtr ) {

		PCSTR pt = PCSTR(m_pData + uPos);

		uPos += strlen(pt);

		uPos += 1;

		return pt;
		}

	return "";
	}


bool CTdsBytes::GetByteCharacterData(CHAR* pOutputString, USHORT uLength, UINT &uPos) const
{
	if( uPos < m_uPtr ) {

		for (USHORT uIndex=0; uIndex < uLength - 1; uIndex++)
			pOutputString[uIndex] = (CHAR) GetByte(uPos);
		pOutputString[uLength - 1] = '\0';

		return true;

		}

	return false;

	}


bool CTdsBytes::GetReverseCharacterData(CHAR* pOutputString, USHORT uLength, UINT &uPos) const
{
	if( uPos < m_uPtr ) {

		for (USHORT uIndex=0; uIndex < uLength - 1; uIndex++)
			pOutputString[uIndex] = (CHAR) GetUShortReverse(uPos);
		pOutputString[uLength - 1] = '\0';

		return true;

		}

	return false;

	}


// Operations

void CTdsBytes::Clear(void)
{
	m_uPtr = 0;
	}

void CTdsBytes::AddByte(BYTE bData)
{
	if( m_uPtr + 1 > m_uAlloc ) {

		ExpandBuffer(m_uPtr + 1);
		}

	m_pData[m_uPtr++] = bData;
	}

void CTdsBytes::AddUShort(USHORT uData)
{
	AddByte(HIBYTE(uData));

	AddByte(LOBYTE(uData));
	}

void CTdsBytes::AddULong(ULONG uData)
{
	AddUShort(HIWORD(uData));

	AddUShort(LOWORD(uData));
	}

void CTdsBytes::AddULongLong(ULONGLONG uData)
{
	AddULong((ULONG)(uData >> 32));

	AddULong((ULONG)(uData & 0x00000000FFFFFFFF));
	}

void CTdsBytes::AddUShortReverse(USHORT uData)
{
	AddByte(LOBYTE(uData));

	AddByte(HIBYTE(uData));
	}

void CTdsBytes::AddULongReverse(ULONG uData)
{
	AddUShortReverse(LOWORD(uData));

	AddUShortReverse(HIWORD(uData));
	}

void CTdsBytes::AddULongLongReverse(ULONGLONG uData)
{
	AddULongReverse((ULONG)(uData & 0x00000000FFFFFFFF));

	AddULongReverse((ULONG)(uData >> 32));
	}

void CTdsBytes::AddFloatReverse(FLOAT uData)
{
	for (INT i=sizeof(FLOAT) - 1; i >= 0; i--)
		AddByte(((BYTE*)&uData)[i]);
	}


void CTdsBytes::AddDoubleReverse(DOUBLE uData)
{
	for (INT i=sizeof(DOUBLE) - 1; i >= 0; i--)
		AddByte(((BYTE*)&uData)[i]);
	}


void CTdsBytes::AddText(PCSTR pText)
{
	AddData(PCBYTE(pText), strlen(pText) + 1);
	}

void CTdsBytes::AddReverseCharText(PCSTR pText)
{
	AddByte(BYTE(strlen(pText)));
	for (UINT uIndex=0; uIndex < strlen(pText); uIndex++)
	{
		AddByte((BYTE)pText[uIndex]);
		AddByte(0x00);
		}
	}

void CTdsBytes::AddData(PCBYTE pData, UINT uSize)
{
	if( m_uPtr + uSize > m_uAlloc ) {

		ExpandBuffer(m_uPtr + uSize);
		}

	memcpy(m_pData + m_uPtr, pData, uSize);

	m_uPtr += uSize;
	}

void CTdsBytes::RemoveHead(UINT uSize)
{
	if(uSize <= m_uPtr) {
		memcpy(m_pData, m_pData + uSize, m_uPtr - uSize);
		m_uPtr -= uSize;
		}
	else {
		Clear();
		}
	}

void CTdsBytes::Dump(void)
{
	for( UINT n = 0; n < m_uPtr; n++ ) {

		if( n % 16 == 0 ) {

			AfxTrace("%4.4X : ", n);
			}

		if( true ) {

			AfxTrace("%2.2X ", m_pData[n]);
			}

		if( n % 16 == 15 ) {

			AfxTrace("\n");
			}
		}

	AfxTrace("\n");
	}


void CTdsBytes::DumpASCII(void)
{
	for( UINT n = 0; n < m_uPtr; n++ ) {

		if( n % 16 == 0 ) {

			AfxTrace("%4.4X : ", n);
			}

		if( true ) {

			char cNextChar = m_pData[n];
			if ((cNextChar == 10) || (cNextChar == 13)		// LF/CR
				|| (cNextChar == 7)				// Bell
				|| (cNextChar == 9) || (cNextChar == 11)	// Tabs
				|| (cNextChar == 12)				// Form Feed
				|| (cNextChar == 8) || (cNextChar == 127))	// Backspace/Delete
				AfxTrace (" ");
			else if (cNextChar < 32)	// Anything below space.
				AfxTrace (" ");
			else
				AfxTrace("%c", cNextChar);
			}

		if( n % 16 == 15 ) {

			AfxTrace("\n");
			}
		}

	AfxTrace("\n");
	}

// Memory Management

void CTdsBytes::ExpandBuffer(UINT uNewSize)
{
	PBYTE pCopy = New BYTE[ m_uAlloc ];

	memcpy(pCopy, m_pData, m_uPtr);

	delete [] m_pData;

	while( m_uAlloc < uNewSize ) {

		m_uAlloc <<= 2;
		}

	m_pData = New BYTE[ m_uAlloc ];

	memcpy(m_pData, pCopy, m_uPtr);

	delete [] pCopy;
	}

// End of File
