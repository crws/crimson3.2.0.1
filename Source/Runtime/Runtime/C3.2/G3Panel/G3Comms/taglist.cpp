
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tag List
//

// Constructor

CTagList::CTagList(void)
{
	m_uCount = 0;

	m_uPoll  = 0;

	m_ppTag  = NULL;

	m_ppPoll = NULL;
	}

// Destructor

CTagList::~CTagList(void)
{
	while( m_uCount-- ) {

		delete m_ppTag[m_uCount];
		}

	Free(m_ppPoll);

	Free(m_ppTag);
	}

// Initialization

void CTagList::Load(PCBYTE &pData)
{
	ValidateLoad("CTagList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		m_ppTag  = (CTag **) New CTag * [ m_uCount ];

		UINT blk = GetWord(pData);

		while( blk-- ) {

			WORD   hBlock = GetWord(pData);

			PCBYTE pBlock = PCBYTE(g_pDbase->LockItem(hBlock));

			if( pBlock ) {

				if( GetWord(pBlock) == IDC_TAG_BLOCK ) {

					if( GetWord(pBlock) == 0x1234 ) {

						UINT uMin = GetWord(pBlock);

						UINT uMax = GetWord(pBlock);

						for( UINT n = uMin; n < uMax; n++ ) {

							CTag *pTag = NULL;

							if( GetByte(pBlock) ) {

								pTag = CTag::MakeObject(GetByte(pBlock));
								}

							if( (m_ppTag[n] = pTag) ) {

								pTag->Load(pBlock);

								pTag->SetIndex(n);
							
								m_Map.Insert(pTag->m_Name, n);
								}
							}
						}
					}

				g_pDbase->FreeItem(hBlock);
				}
			}
		}
	}

// Item Access

CTag * CTagList::GetItem(UINT uPos) const
{
	if( uPos < m_uCount ) {

		return m_ppTag[uPos];
		}

	return NULL;
	}

// Attributes

UINT CTagList::GetCount(void) const
{
	return m_uCount;
	}

// Operations

void CTagList::Purge(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppTag[n] ) {

			m_ppTag[n]->Purge();
			}
		}
	}

void CTagList::Force(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppTag[n] ) {

			m_ppTag[n]->Force();
			}
		}
	}

void CTagList::SetPollScan(UINT uCode)
{
	if( uCode == scanTrue ) {

		if( m_ppPoll ) {

			Free(m_ppPoll);

			m_uPoll = 0;
			}

		UINT mem = m_uCount * sizeof(CTag *);

		m_ppPoll = (CTag **) Malloc(mem);
		}

	if( TRUE ) {

		for( UINT n = 0; n < m_uCount; n++ ) {

			if( m_ppTag[n] ) {

				if( m_ppTag[n]->SetPollScan(uCode) ) {

					if( uCode == scanTrue ) {

						m_ppPoll[m_uPoll] = m_ppTag[n];

						m_uPoll++;
						}
					}
				}
			}
		}

	if( uCode == scanTrue ) {

		UINT mem = m_uPoll * sizeof(CTag *);

		m_ppPoll = (CTag **) ReAlloc(m_ppPoll, mem);
		}
	}

void CTagList::Poll(UINT uDelta)
{
	UINT p = 0;

	for( UINT n = 0; n < m_uPoll; n++ ) {

		m_ppPoll[n]->Poll(uDelta);

		if( ++p == (TRUE ? 100 : 10) ) {

			Sleep(10);

			p = 0;
			}
		}
	}

UINT CTagList::FindByName(PCUTF pName)
{
	INDEX Index = m_Map.FindName(pName);

	if( !m_Map.Failed(Index) ) {
		
		return m_Map.GetData(Index);
		}

	return NOTHING;
	}

// End of File
