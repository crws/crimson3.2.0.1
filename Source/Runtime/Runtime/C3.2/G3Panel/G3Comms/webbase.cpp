
#include "intern.hpp"

#include "web.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Web Server Base Class
//

// Constructor

CWebBase::CWebBase(void)
{
	m_pGDI          = NULL;
	m_pSrc          = NULL;
	m_Enable        = FALSE;
	m_DbTime        = 0;
	m_pList         = New CWebPageList;
	m_pTitle        = NULL;
	m_pHeader       = NULL;
	m_pHome	        = NULL;
	m_RemoteView    = FALSE;
	m_RemoteCtrl    = FALSE;
	m_RemoteZoom    = 0;
	m_RemoteFact    = 2;
	m_RemoteBits    = 16;
	m_RemoteSec     = 0;
	m_LogsView      = FALSE;
	m_LogsBatches   = FALSE;
	m_LogsSec       = 0;
	m_Source        = 0;
	m_Local         = 0;
	m_pUser         = NULL;
	m_pReal         = NULL;
	m_pPass         = NULL;
	m_Restrict      = 0;
	m_SecMask       = 0;
	m_SecAddr       = 0;
	m_UserSite      = FALSE;
	m_UserMenu      = TRUE;
	m_UserRoot      = FALSE;
	m_UserDelay     = 5;
	m_UserSec       = FALSE;
	m_CustHome      = FALSE;
	m_CustIcon      = FALSE;
	m_CustLogon     = FALSE;
	m_CustCss       = FALSE;
	m_CustJs        = FALSE;
	m_SystemPages   = FALSE;
	m_SystemConsole = FALSE;
	m_SystemCapture = FALSE;
	m_SystemJump    = FALSE;
	m_SystemSec     = 0;
	m_pPort         = NULL;
	m_SameOrigin    = TRUE;
	}

// Destructor

CWebBase::~CWebBase(void)
{
	delete m_pList;

	delete m_pTitle;
	delete m_pHome;
	
	delete m_pUser;
	delete m_pReal;
	delete m_pPass;

	delete m_pPort;
	}

// Initialization

void CWebBase::Load(PCBYTE &pData)
{
	m_pSrc = CCommsSystem::m_pThis->m_pTags->m_pTags;
	}

// Task List

void CWebBase::GetTaskList(CTaskList &List)
{
	if( m_Enable ) {

		// TODO -- What about priority flipping???!!!

		BOOL fFlip = FALSE && !WhoHasFeature(rfDisplay);
	
		CTaskDef Task;

		Task.m_Name   = "W3SERV";
		Task.m_pEntry = this;
		Task.m_uID    = 0;
		Task.m_uCount = 1;
		Task.m_uLevel = fFlip ? 8000 : 2600;
		Task.m_uStack = 4096;

		List.Append(Task);
		}
	else {
		extern void AllowPdfRestart(void);

		AllowPdfRestart();
		}
	}

// End of File
