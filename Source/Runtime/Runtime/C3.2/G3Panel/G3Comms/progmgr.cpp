
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Program Manager
//

// Constructor

CProgramManager::CProgramManager(void)
{
	StdSetRef();

	m_pList     = Create_Mutex();

	m_pRunLock  = Create_Mutex();

	m_pDiag     = NULL;

	m_uProv     = 0;

	m_fEarly    = FALSE;

	DiagRegister();
}

// Destructor

CProgramManager::~CProgramManager(void)
{
	DiagRevoke();

	INDEX n = m_Map.GetHead();

	while( !m_Map.Failed(n) ) {

		CProgramItem *pProg = m_Map.GetData(n);

		delete pProg;

		m_Map.GetNext(n);
	}

	m_pRunLock->Release();

	m_pList->Release();
}

// Initialization

void CProgramManager::Load(PCBYTE &pData)
{
	ValidateLoad("CProgramManager", pData);

	UINT uCount;

	if( (uCount = GetWord(pData)) ) {

		for( UINT n = 0; n < uCount; n++ ) {

			UINT    hProg = GetLong(pData);

			CString Name  = UniConvert(GetWide(pData));

			BOOL    fKeep = GetByte(pData);

			if( fKeep ) {

				m_Handles.Append(hProg);
			}

			m_Index.Insert(Name, hProg);
		}
	}
}

// Task List

void CProgramManager::GetTaskList(CTaskList &List)
{
	UINT p[] = { 800, 810, 820, 830, 840 };

	for( UINT n = 0; n < elements(p); n++ ) {

		CTaskDef Task;

		Task.m_Name   = "DISPATCH";
		Task.m_pEntry = this;
		Task.m_uID    = n;
		Task.m_uCount = 1;
		Task.m_uLevel = p[n];
		Task.m_uStack = 0;

		List.Append(Task);
	}
}

// Task Entries

void CProgramManager::TaskInit(UINT uID)
{
	m_pLock[uID] = Create_Mutex();

	m_pFlag[uID] = Create_Semaphore(0);

	m_pDone[uID] = (uID == 4) ? Create_Semaphore(0) : NULL;

	m_pHead[uID] = NULL;

	m_pTail[uID] = NULL;

	ProgInit();
}

void CProgramManager::TaskExec(UINT uID)
{
	while( m_pFlag[uID]->Wait(FOREVER) ) {

		CProgramItem *pProg = NULL;

		PDWORD        pArgs = NULL;

		if( m_pLock[uID]->Wait(FOREVER) ) {

			pProg = m_pHead[uID];

			pArgs = pProg->m_pArgs;

			if( !pProg->m_fTemp ) {

				AfxListRemove(m_pHead[uID],
					      m_pTail[uID],
					      pProg,
					      m_pNext,
					      m_pPrev
				);
			}

			pProg->m_pArgs = NULL;

			pProg->m_fList = FALSE;

			m_pLock[uID]->Free();
		}

		if( pProg ) {

			SetTaskLimit(50, 5);

			pProg->Execute(pArgs);

			SetTaskLimit(0, 0);

			if( pArgs ) {

				delete[] pArgs;
			}

			if( pProg->m_fTemp ) {

				AfxListRemove(m_pHead[uID],
					      m_pTail[uID],
					      pProg,
					      m_pNext,
					      m_pPrev
				);

				delete pProg;
			}

			if( m_pDone[uID] ) {

				m_pDone[uID]->Signal(1);
			}

			ForceSleep(5);
		}
	}
}

void CProgramManager::TaskStop(UINT uID)
{
}

void CProgramManager::TaskTerm(UINT uID)
{
	if( m_pDone[uID] ) {

		m_pDone[uID]->Release();
	}

	m_pFlag[uID]->Release();

	m_pLock[uID]->Release();
}

// Operations

BOOL CProgramManager::IsAvail(UINT hProg)
{
	CProgramItem *pProg = Find(hProg);

	if( pProg ) {

		return pProg->IsAvail();
	}

	return FALSE;
}

BOOL CProgramManager::SetScan(UINT hProg, UINT Code)
{
	CProgramItem *pProg = Find(hProg);

	if( pProg ) {

		return pProg->SetScan(Code);
	}

	return FALSE;
}

BOOL CProgramManager::FindName(UINT hProg, PTXT pName, UINT uName)
{
	CProgramItem *pProg = Find(hProg);

	if( pProg ) {

		strcpy(pName, UniConvert(pProg->m_Name));

		return TRUE;
	}

	return FALSE;
}

BOOL CProgramManager::Dispatch(CProgramItem *pProg, PDWORD pParam, UINT uLevel)
{
	if( m_pLock[uLevel]->Wait(FOREVER) ) {

		if( pProg->m_fList ) {

			m_pLock[uLevel]->Free();

			return FALSE;
		}

		if( pProg->m_fTemp ) {

			if( m_pHead[uLevel] ) {

				delete pProg;

				m_pLock[uLevel]->Free();

				return FALSE;
			}
		}

		AfxListAppend(m_pHead[uLevel],
			      m_pTail[uLevel],
			      pProg,
			      m_pNext,
			      m_pPrev
		);

		if( pProg->m_pCode->m_pCode[3] ) {

			UINT uCount = pProg->m_pCode->m_pCode[3];

			UINT uAlloc = uCount * sizeof(DWORD);

			pProg->m_pArgs = New DWORD[uCount];

			memcpy(pProg->m_pArgs, pParam, uAlloc);
		}
		else
			pProg->m_pArgs = NULL;

		pProg->m_fList = TRUE;

		m_pLock[uLevel]->Free();

		m_pFlag[uLevel]->Signal(1);

		return TRUE;
	}

	return FALSE;
}

DWORD CProgramManager::Run(UINT hProg, PDWORD pParam)
{
	CProgramItem *pProg = Find(hProg);

	if( pProg && pProg->m_pCode && pProg->m_pCode->m_pCode ) {

		if( pProg->m_BkGnd ) {

			UINT uLevel = pProg->m_BkGnd - 1;

			Dispatch(pProg, pParam, uLevel);

			return 0;
		}

		if( m_fEarly ) {

			// Avoid waiting for comms items during early
			// startup as drivers are not yet running...

			return pProg->m_pCode->Execute(typeVoid, pParam);
		}

		return pProg->Execute(pParam);
	}

	return 0;
}

void CProgramManager::SetEarly(BOOL fEarly)
{
	m_fEarly = fEarly;
}

// IUnknown

HRESULT CProgramManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDiagProvider);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
}

ULONG CProgramManager::AddRef(void)
{
	StdAddRef();
}

ULONG CProgramManager::Release(void)
{
	StdRelease();
}

// IDiagProvider

UINT CProgramManager::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	UINT uCmd;

	switch( (uCmd = pCmd->GetCode()) ) {

		case 1:
			return DiagList(pOut, pCmd);

		case 2:
			return DiagRun(pOut, pCmd);
	}

	return 0;
}

// Location

CProgramItem * CProgramManager::Find(UINT hProg)
{
	if( m_pList->Wait(FOREVER) ) {

		INDEX n = m_Map.FindName(hProg);

		if( m_Map.Failed(n) ) {

			CProgramItem *pProg = NULL;

			PCBYTE        pData = PCBYTE(g_pDbase->LockItem(hProg));

			if( pData ) {

				if( GetWord(pData) == IDC_PROGRAM ) {

					pProg = New CProgramItem;

					pProg->Load(pData);

					pProg->PreRegister();

					m_Map.Insert(hProg, pProg);
				}

				g_pDbase->FreeItem(hProg);
			}

			m_pList->Free();

			return pProg;
		}

		CProgramItem *pProg = m_Map.GetData(n);

		m_pList->Free();

		return pProg;
	}

	return NULL;
}

void CProgramManager::ProgInit(void)
{
	UINT c = m_Handles.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		Find(m_Handles[n]);
	}
}

// Diagnostics

BOOL CProgramManager::DiagRegister(void)
{
	AfxGetObject("diagmanager", 0, IDiagManager, m_pDiag);

	if( m_pDiag ) {

		m_uProv = m_pDiag->RegisterProvider(this, "progs");

		m_pDiag->RegisterCommand(m_uProv, 1, "list");

		m_pDiag->RegisterCommand(m_uProv, 2, "run");

		return TRUE;
	}

	return FALSE;
}

BOOL CProgramManager::DiagRevoke(void)
{
	if( m_pDiag ) {

		m_pDiag->RevokeProvider(m_uProv);

		m_pDiag->Release();

		m_pDiag = NULL;

		return TRUE;
	}

	return FALSE;
}

UINT CProgramManager::DiagList(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 0 || pCmd->GetArgCount() == 1 ) {

		pOut->AddTable(1);

		pOut->SetColumn(0, "Name", "%s");

		pOut->AddHead();

		pOut->AddRule('-');

		for( INDEX n = m_Index.GetHead(); !m_Index.Failed(n); m_Index.GetNext(n) ) {

			CString Name = m_Index.GetName(n);

			if( pCmd->GetArgCount() == 0 || Name.StartsWith(pCmd->GetArg(0)) ) {

				pOut->AddRow();

				pOut->SetData(0, PCTXT(Name));

				pOut->EndRow();
			}
		}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CProgramManager::DiagRun(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		INDEX n = m_Index.FindName(pCmd->GetArg(0));

		if( !m_Index.Failed(n) ) {

			UINT          hProg = m_Index.GetData(n);

			CProgramItem *pProg = Find(hProg);

			if( pProg ) {
				
				if( pProg->m_pCode && pProg->m_pCode->m_pCode ) {

					if( !pProg->m_pCode->m_pCode[3] ) {

						m_pRunLock->Wait(FOREVER);

						while( m_pDone[4]->Wait(0) );

						Dispatch(pProg, NULL, 4);

						if( m_pDone[4]->Wait(5000) ) {

							m_pRunLock->Free();

							pOut->Print("Done\n");

							return 0;
						}

						m_pRunLock->Free();

						pOut->Error("program did not complete");

						return 4;
					}

					pOut->Error("cannot run program with parameters");

					return 3;
				}

				pOut->Error("program has no code");

				return 5;
			}
		}

		pOut->Error("program not found");

		return 2;
	}

	pOut->Error(NULL);

	return 1;
}

// End of File
