
#include "intern.hpp"

#include "sql.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Referenced Implementations
//

extern ISqlConnection * Create_MSSqlServerConnection(void);

//////////////////////////////////////////////////////////////////////////
//
// Sql Manager
//

// Constructor

CSqlManager::CSqlManager(void)
{
	m_Enable        = FALSE;

	m_pServer       = NULL;

	m_Status        = sqlServerPending;

	m_pQueries      = New CSqlQueryList;

	m_pConnection   = NULL;

	m_pStatement    = NULL;

	m_Connect       = 0;
}

// Destructor

CSqlManager::~CSqlManager(void)
{
	delete m_pServer;

	delete m_pConnection;

	delete m_pStatement;

	delete m_pQueries;
}

// Initialization

void CSqlManager::LoadSqlManager(PCBYTE &pData)
{
	ValidateLoad("CSqlManager", pData);

	m_Enable = GetByte(pData);

	GetCoded(pData, m_pServer);

	m_wPort    = GetWord(pData);

	m_uTlsMode = GetByte(pData);

	m_User     = UniConvert(GetWide(pData));

	m_Pass     = UniConvert(GetWide(pData));

	m_Database = UniConvert(GetWide(pData));

	m_pQueries->Load(pData);

	m_Connect  = GetByte(pData);

	m_Instance = UniConvert(GetWide(pData));
}

// Task List

void CSqlManager::GetTaskList(CTaskList &List)
{
	CTaskDef Task;

	Task.m_Name   = "SQLQUERY";
	Task.m_pEntry = this;
	Task.m_uID    = 0;
	Task.m_uCount = 1;
	Task.m_uLevel = 8300;
	Task.m_uStack = 4096;

	List.Append(Task);
}

// Task Entries

void CSqlManager::TaskInit(UINT uID)
{

}

void CSqlManager::TaskExec(UINT uID)
{
	m_pConnection = CreateConnection(dbMsSqlServer);

	if( m_Connect == 1 ) {

		m_pConnection->SetInstance(PCTXT(m_Instance));
	}

	m_pStatement  = m_pConnection->CreateStatement();

	m_Status      = sqlServerPending;

	for( ;;) {

		if( m_Enable ) {

			ServiceQueries();

			Sleep(500);
		}
		else {
			Sleep(1000);
		}
	}
}

void CSqlManager::TaskStop(UINT uID)
{
}

void CSqlManager::TaskTerm(UINT uID)
{
}

// User Status and Query Functions

void CSqlManager::UserExecuteQuery(CString const &Query)
{
	for( UINT n = 0; n < m_pQueries->m_uCount; n++ ) {

		CSqlQuery *pQuery = m_pQueries->m_ppQuery[n];

		if( pQuery ) {

			if( !Query.CompareN(pQuery->m_Name) ) {

				pQuery->Force();

				break;
			}
		}
	}
}

void CSqlManager::UserExecuteAll(void)
{
	for( UINT n = 0; n < m_pQueries->m_uCount; n++ ) {

		CSqlQuery *pQuery = m_pQueries->m_ppQuery[n];

		if( pQuery ) {

			pQuery->Force();
		}
	}
}

C3INT CSqlManager::GetConnectionStatus(void)
{
	return m_Status;
}

C3INT CSqlManager::GetQueryStatus(CString const &Query)
{
	for( UINT n = 0; n < m_pQueries->m_uCount; n++ ) {

		CSqlQuery *pQuery = m_pQueries->m_ppQuery[n];

		if( pQuery ) {

			if( !Query.CompareN(pQuery->m_Name) ) {

				return pQuery->GetStatus();
			}
		}
	}

	return 0;
}

C3INT CSqlManager::GetQueryLastRun(CString const &Query)
{
	for( UINT n = 0; n < m_pQueries->m_uCount; n++ ) {

		CSqlQuery *pQuery = m_pQueries->m_ppQuery[n];

		if( pQuery ) {

			if( !Query.CompareN(pQuery->m_Name) ) {

				return pQuery->GetLastRun();
			}
		}
	}

	return 0;
}

// Implementation

ISqlConnection * CSqlManager::CreateConnection(UINT uType)
{
	switch( uType ) {

		case dbMsSqlServer:

			return Create_MSSqlServerConnection();
	}

	return NULL;
}

// Query Servicing

void CSqlManager::ServiceQueries(void)
{
	BOOL fWork = FALSE;

	for( UINT m = 0; m < m_pQueries->m_uCount; m++ ) {

		CSqlQuery * pQuery = m_pQueries->m_ppQuery[m];

		if( pQuery ) {

			pQuery->Touch();

			if( pQuery->NeedsToExecute() ) {

				fWork = TRUE;

				break;
			}
		}
	}

	if( fWork ) {

		DWORD IP = GetItemAddr(m_pServer, 0);

		if( m_pConnection->Open(IPREF(IP), m_wPort, m_uTlsMode, m_User, m_Pass, m_Database) ) {

			m_Status = sqlServerSuccess;

			for( UINT n = 0; n < m_pQueries->m_uCount; n++ ) {

				CSqlQuery * pQuery = m_pQueries->m_ppQuery[n];

				if( pQuery ) {

					pQuery->Touch();

					if( !pQuery->IsBroken() && pQuery->NeedsToExecute() ) {

						RunQuery(pQuery);
					}
				}
			}

			m_pConnection->Close();
		}
		else {
			m_Status = sqlServerFailed;
		}
	}
}

BOOL CSqlManager::RunQuery(CSqlQuery *pQuery)
{
	if( pQuery && pQuery->m_Enable ) {

		CResultSet Results;

		if( m_pStatement->ExecRead(pQuery->BuildSql(m_Database), Results) ) {

			if( pQuery->UpdateTagData(Results) ) {

				return TRUE;
			}
		}
		else {
			pQuery->SetStatus(CSqlQuery::sqlQueryFailed);

			return FALSE;
		}
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//
// Sql Query List
//

// Constructor

CSqlQueryList::CSqlQueryList(void)
{
	m_uCount = 0;

	m_ppQuery  = NULL;
}

// Destructor

CSqlQueryList::~CSqlQueryList(void)
{
	while( m_uCount-- ) {

		delete m_ppQuery[m_uCount];
	}

	delete[] m_ppQuery;
}

// Initialization

void CSqlQueryList::Load(PCBYTE &pData)
{
	ValidateLoad("CSqlQueryList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		m_ppQuery = New CSqlQuery *[m_uCount];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CSqlQuery *pQuery = NULL;

			if( GetByte(pData) ) {

				pQuery = New CSqlQuery(n+1);

				pQuery->Load(pData);
			}

			m_ppQuery[n] = pQuery;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Sql Query Item
//

// Constructor

CSqlQuery::CSqlQuery(UINT uPos)
{
	m_uPos       = uPos;

	m_uStatus    = sqlQueryPending;

	m_uLastRun   = 0;

	m_uColumns   = 0;

	m_uRows      = 0;

	m_fReady     = FALSE;

	m_pLock      = Create_Mutex();

	m_uUpdate    = 5;

	m_uLastTicks = 0;

	m_ppColumns  = NULL;

	m_ppFilters  = NULL;

	m_fBroken    = FALSE;
}

// Destructor

CSqlQuery::~CSqlQuery(void)
{
	if( m_pLock ) {

		m_pLock->Release();
	}

	for( UINT c = 0; c < m_uColumns; c++ ) {

		delete m_ppColumns[c];
	}

	for( UINT n = 0; n < m_wFilters; n++ ) {

		delete m_ppFilters[n];
	}

	delete[] m_ppColumns;

	delete[] m_ppFilters;
}

// Initialization

void CSqlQuery::Load(PCBYTE &pData)
{
	ValidateLoad("CSqlQuery", pData);

	m_Enable    = GetByte(pData);

	m_Name      = UniConvert(GetWide(pData));

	m_Table     = UniConvert(GetWide(pData));

	if( m_Enable ) {

		m_uColumns   = GetByte(pData);

		m_uRows      = GetWord(pData);

		m_uUpdate    = GetWord(pData);

		m_SortMode   = GetByte(pData);

		m_SortColumn = GetWord(pData);

		m_fBroken    = GetByte(pData);

		m_SQL        = UniConvert(GetWide(pData));

		m_Schema     = UniConvert(GetWide(pData));

		LoadColumns(pData);

		LoadFilters(pData);
	}
}

// Loading Helpers

void CSqlQuery::LoadColumns(PCBYTE &pData)
{
	if( GetWord(pData) == 0x1234 ) {

		WORD wCount = 0;

		if( (wCount = GetWord(pData)) ) {

			m_ppColumns = New CSqlColDef *[m_uColumns];

			UINT uPos = 0;

			for( UINT n = 0; n < wCount; n++ ) {

				if( GetByte(pData) == 1 ) {

					CSqlColDef *pColumn = New CSqlColDef;

					pColumn->Load(pData);

					pColumn->m_uCol = uPos;

					m_ppColumns[uPos] = pColumn;

					uPos++;
				}
			}
		}
	}
}

void CSqlQuery::LoadFilters(PCBYTE &pData)
{
	if( GetWord(pData) == 0x1234 ) {

		if( (m_wFilters = GetWord(pData)) ) {

			m_ppFilters = new CSqlFilter *[m_wFilters];

			for( UINT n = 0; n < m_wFilters; n++ ) {

				CSqlFilter *pFilter = new CSqlFilter;

				pFilter->Load(pData);

				m_ppFilters[n] = pFilter;
			}
		}
	}
}

// Format Helpers

CString CSqlQuery::FormatDatetime(UINT uTime)
{
	UINT uYear  = GetYear(uTime);
	UINT uMonth = GetMonth(uTime);
	UINT uDate  = GetDate(uTime);
	UINT uHour  = GetHour(uTime);
	UINT uMin   = GetMin(uTime);
	UINT uSec   = GetSec(uTime);

	return CPrintf("%u-%u-%u %2.2u:%2.2u:%2.2u.000",
		       uYear,
		       uMonth,
		       uDate,
		       uHour,
		       uMin,
		       uSec
	);
}

// Operations

BOOL CSqlQuery::NeedsToExecute(void)
{
	BOOL fReady = FALSE;

	m_pLock->Wait(FOREVER);

	fReady = m_fReady;

	m_pLock->Free();

	return fReady;
}

void CSqlQuery::Touch(void)
{
	m_pLock->Wait(FOREVER);

	if( m_Enable == 1 ) {

		if( !m_uLastTicks ) {

			m_fReady = TRUE;
		}
		else {
			UINT uTicks = GetTickCount();

			if( ToTime(uTicks - m_uLastTicks) > (m_uUpdate * 1000 * 60) ) {

				m_fReady = TRUE;
			}
		}
	}

	m_pLock->Free();
}

BOOL CSqlQuery::UpdateTagData(CResultSet const &Results)
{
	UINT uStatus = sqlQueryPending;

	SetStatus(uStatus);

	UINT uCount = 0;

	for( UINT c = 0; c < m_uColumns; c++ ) {

		CSqlColDef * pColumn = m_ppColumns[c];

		if( pColumn->UpdateRecords(Results) ) {

			uCount++;
		}
	}

	if( uCount == m_uColumns ) {

		uStatus = sqlQuerySuccess;
	}

	else if( uCount > 0 ) {

		uStatus = sqlQueryPartial;
	}
	else {
		uStatus = sqlQueryFailed;
	}

	SetStatus(uStatus);

	return uCount > 0;
}

void CSqlQuery::SetStatus(UINT uStatus)
{
	m_pLock->Wait(FOREVER);

	m_uLastTicks = GetTickCount();

	m_uLastRun   = GetNow();

	m_fReady     = FALSE;

	m_uStatus    = uStatus;

	m_pLock->Free();
}

BOOL CSqlQuery::IsBroken(void)
{
	return m_fBroken;
}

void CSqlQuery::Force(void)
{
	m_pLock->Wait(FOREVER);

	m_fReady = TRUE;

	m_pLock->Free();
}

UINT CSqlQuery::GetStatus(void)
{
	UINT uStatus = sqlQueryPending;

	m_pLock->Wait(FOREVER);

	uStatus = m_uStatus;

	m_pLock->Free();

	return uStatus;
}

UINT CSqlQuery::GetLastRun(void)
{
	UINT uLast = 0;

	m_pLock->Wait(FOREVER);

	uLast = m_uLastRun;

	m_pLock->Free();

	return uLast;
}

CString CSqlQuery::BuildSql(CString const &Database)
{
	CString Sql(CPrintf("SELECT TOP %u ", m_uRows));

	for( UINT n = 0; n < m_uColumns; n++ ) {

		Sql += CPrintf("[%s]", PCTXT(m_ppColumns[n]->m_Name));

		if( m_uColumns > 1 && n != m_uColumns - 1 ) {

			Sql += ", ";
		}
	}

	Sql += "\n";

	CString Schema = m_Schema.IsEmpty() ? "dbo" : m_Schema;

	Sql += CPrintf(" FROM [%s].[%s].[%s]\n", PCTXT(Database), PCTXT(Schema), PCTXT(m_Table));

	BOOL fFirst = TRUE;

	for( UINT k = 0; k < m_wFilters; k++ ) {

		CSqlFilter *pFilter = m_ppFilters[k];

		if( !pFilter ) {

			continue;
		}

		if( fFirst ) {

			Sql += " WHERE ";

			fFirst = FALSE;
		}
		else {
			Sql += CPrintf(" %s ", PCTXT(pFilter->GetBinding()));
		}

		CString Text = pFilter->GetOperator();

		if( pFilter->NeedsOperand() ) {

			if( pFilter->m_pValue ) {

				UINT uType = pFilter->m_pValue->GetType();

				if( uType == typeInteger ) {

					UINT uSqlType =  m_ppColumns[pFilter->m_Column]->m_uType;

					if( uSqlType == sqlTypeDatetime ) {

						UINT uDate = pFilter->m_pValue->ExecVal();

						CString Date = FormatDatetime(uDate);

						Text += CPrintf(" '%s' ", PCTXT(Date));
					}
					else {
						C3INT val = pFilter->m_pValue->ExecVal();

						Text += CPrintf(" %i", val);
					}
				}

				if( uType == typeString ) {

					CUnicode val(PUTF(pFilter->m_pValue->ExecVal()));

					Text += CPrintf(" '%s'", PCTXT(UniConvert(val)));
				}

				if( uType == typeReal ) {

					DWORD Data = pFilter->m_pValue->ExecVal();

					C3REAL Val = *((float *) &Data);

					char s[32];

					gcvt(Val, 7, s);

					Text += CPrintf(" %s", s);
				}
			}
		}

		CString Filter = CPrintf("[%s] %s\n",
					 PCTXT(m_ppColumns[pFilter->m_Column]->m_Name),
					 PCTXT(Text)
		);

		Sql += Filter;
	}

	if( m_SortMode > 0 && m_SortColumn < m_uColumns ) {

		PCTXT pName = m_ppColumns[m_SortColumn]->m_Name;

		if( pName ) {

			Sql += CPrintf(" ORDER BY [%s] %s\n",
				       pName,
				       m_SortMode == 1 ? "ASC" : "DESC"
			);
		}
	}

	m_SQL = Sql;

	return Sql;
}


///////////////////////////////////////////////////////////////////////////
//
// SQL Column Definition
//

 // Constructor

CSqlColDef::CSqlColDef(void)
{
	m_uCol      = 0;

	m_uType     = 0;

	m_ppRecords = NULL;

	m_uRecords  = 0;
}

// Initialization

void CSqlColDef::Load(PCBYTE &pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uType = GetLong(pData);

		m_Name  = UniConvert(GetWide(pData));

		LoadRecords(pData);
	}
}

// Records

BOOL CSqlColDef::UpdateRecords(CResultSet const &ResultSet)
{
	if( m_ppRecords ) {

		CSqlColumn const *pColumn = ResultSet.GetColumn(m_uCol);

		if( !pColumn || pColumn->GetType() != m_uType ) {

			return FALSE;
		}

		for( UINT r = 0; r < m_uRecords; r++ ) {

			CSqlRecord * pRecord = m_ppRecords[r];

			CCodedItem * pCoded  = pRecord->m_pValue;

			if( pCoded ) {

				UINT uType = GetTypeFromSql();

				if( uType == typeString ) {

					PCUTF pText = ResultSet.GetTextField(r, m_uCol);

					pCoded->SetValue(DWORD(pText), typeString, setNone);
				}

				if( uType == typeReal ) {

					double d = ResultSet.GetFloatField(r, m_uCol);

					pCoded->SetValue(R2I(d), typeReal, setNone);
				}

				if( uType == typeInteger ) {

					DWORD dwData = ResultSet.GetIntegerField(r, m_uCol);

					pCoded->SetValue(dwData, typeInteger, setNone);
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}

// Implementation

void CSqlColDef::LoadRecords(PCBYTE &pData)
{
	if( GetWord(pData) == 0x1234 ) {

		WORD wRecords = 0;

		if( (wRecords = GetWord(pData)) ) {

			m_uRecords  = wRecords;

			m_ppRecords = New CSqlRecord *[m_uRecords];

			for( UINT n = 0; n < m_uRecords; n++ ) {

				CSqlRecord *pRecord = NULL;

				pRecord = New CSqlRecord;

				pRecord->Load(pData);

				m_ppRecords[n] = pRecord;
			}
		}
	}
}

UINT CSqlColDef::GetTypeFromSql(void)
{
	switch( m_uType ) {

		// Fall-through is intended

		case sqlTypeBinary:
		case sqlTypeInt:
		case sqlTypeDatetime:

			return typeInteger;

		case sqlTypeFloat:

			return typeReal;

		case sqlTypeChar:
		case sqlTypeVarChar:
		case sqlTypeNVarChar:

			return typeString;
	}

	return typeInteger;
}

//////////////////////////////////////////////////////////////////////////
//
// SQL Record
//

// Constructor

CSqlRecord::CSqlRecord(void)
{
	m_pValue = NULL;
}

// Initialization

void CSqlRecord::Load(PCBYTE &pData)
{
	if( GetWord(pData) == 0x1234 ) {

		GetCoded(pData, m_pValue);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter
//

// Constructor

CSqlFilter::CSqlFilter(void)
{
	m_pValue   = NULL;

	m_Column   = 0;

	m_Operator = opEqual;

	m_Binding  = bindAnd;
}

// Helpers

BOOL CSqlFilter::NeedsOperand(void)
{
	switch( m_Operator ) {

		case opNull:
		case opNotNull:

			return FALSE;
	}

	return TRUE;
}

CString CSqlFilter::GetOperator(void)
{
	PCTXT pOps[] = { "=",
			 "<>",
			 "<" ,
			 "<=",
			 ">" ,
			 ">=",
			 "LIKE",
			 "IS NULL",
			 "IS NOT NULL"
	};

	return pOps[m_Operator];
}

CString CSqlFilter::GetBinding(void)
{
	switch( m_Binding ) {

		case bindAnd: return "AND";

		case bindOr:  return "OR";
	}

	return "";
}

void CSqlFilter::Load(PCBYTE &pData)
{
	ValidateLoad("CSqlFilter", pData);

	m_Column = GetLong(pData);

	m_Operator = GetByte(pData);

	GetCoded(pData, m_pValue);

	m_Binding = GetByte(pData);
}

// End of File
