
#include "intern.hpp"

#include "proxy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Region Object
//

class CRegion {

	public:
		// Constructor
		CRegion(ICommsDisplay *pDisplay);

		// Destructor
		~CRegion(void);

		// Operations
		void Capture(PBYTE pDisp, int cx, int cy, BOOL fPortrait);
		void Service(void);

	public:
		// Public Data
		UINT      m_uHandle;
		UINT      m_uDrop;
		PBYTE     m_pData;
		PBYTE     m_pLast;
		UINT      m_uSize;
		BOOL      m_fValid;
		CRegion * m_pNext;
		CRegion * m_pPrev;

	protected:
		// Data
		ICommsDisplay * m_pDisplay;
	};

//////////////////////////////////////////////////////////////////////////
//
// Region Object
//

// Constructor

CRegion::CRegion(ICommsDisplay *pDisplay)
{
	m_pDisplay = pDisplay;

	m_uHandle  = NOTHING;

	m_uDrop    = 0;

	m_pData    = NULL;

	m_pLast    = NULL;

	m_uSize    = 0;

	m_fValid   = FALSE;

	m_pNext    = NULL;

	m_pPrev    = NULL;
	}

// Destructor

CRegion::~CRegion(void)
{
	delete [] m_pData;

	delete [] m_pLast;
	}

// Operations

void CRegion::Capture(PBYTE pDisp, int cx, int cy, BOOL fPortrait)
{
	if( !m_fValid ) {

		memcpy( m_pLast, m_pData, m_uSize);

		m_pDisplay->Capture( m_pData, 
				     pDisp, 
				     0, 0, 
				     cx, cy, 
				     cx, 
				     fPortrait
				     );

		m_fValid = TRUE;
		}
	}

void CRegion::Service(void)
{
	if( m_fValid ) {

		m_pDisplay->Service( m_uDrop, 
				     m_pLast, 
				     m_pData, 
				     m_uSize
				     );
		
		m_fValid = FALSE;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Display Proxy Object
//

class CProxyDisplay : public CProxy, public IProxyDisplay
{
	public:
		// Constructor
		CProxyDisplay(void);

		// Destructor
		~CProxyDisplay(void);

		// Binding
		BOOL Bind(CCommsPort *pPort, IDriver *pDriver);

		// IProxyDisplay
		BOOL CheckPort(UINT uPort);
		void Render(UINT uHandle, UINT uDrop, PBYTE pData, int cx, int cy, BOOL fPortrait);
		void Remove(UINT uHandle);

		// Entry Points
		void Init(UINT uID);
		void Task(UINT uID);
		void Term(UINT uID);

	protected:
		// Region List
		CRegion	* m_pHead;
		CRegion	* m_pTail;

		// Data Members
		BOOL            m_fOpen;
		ICommsDisplay * m_pDisplay;
		IMutex        * m_pLock;
	};

//////////////////////////////////////////////////////////////////////////
//
// Display Proxy Object
//

// Instantiator

CProxy * Create_ProxyDisplay(void)
{
	return New CProxyDisplay;
	}

// Constructor

CProxyDisplay::CProxyDisplay(void)
{
	m_fOpen    = FALSE;

	m_pDisplay = NULL;

	m_pHead    = NULL;

	m_pTail    = NULL;

	m_pLock    = Create_Mutex();

	CCommsSystem::m_pThis->m_pComms->m_pDisplays->Append(this);
	}

// Destructor

CProxyDisplay::~CProxyDisplay(void)
{
	AfxRelease(m_pDisplay);

	m_pLock->Release();
	}

// Binding

BOOL CProxyDisplay::Bind(CCommsPort *pPort, IDriver *pDriver)
{
	if( CProxy::Bind(pPort, pDriver) ) {

		m_pDriver->QueryInterface(AfxAeonIID(ICommsDisplay), (void **) &m_pDisplay);

		if( m_pDisplay ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// IProxyDisplay

BOOL CProxyDisplay::CheckPort(UINT uPort)
{
	return uPort == m_pPort->m_Number;
	}

void CProxyDisplay::Render(UINT uHandle, UINT uDrop, PBYTE pData, int cx, int cy, BOOL fPortrait)
{
	if( m_fOpen ) {

		CRegion *pScan = m_pHead;

		while( pScan ) {

			CRegion *pNext = pScan->m_pNext;

			if( uHandle == pScan->m_uHandle ) {

				break;
				}

			pScan = pNext;
			}

		if( !pScan ) {
	
			UINT        uAlloc = m_pDisplay->GetSize(cx, cy);

			CRegion  * pRegion = New CRegion(m_pDisplay);

			pRegion->m_uHandle = uHandle;

			pRegion->m_uDrop   = uDrop;

			pRegion->m_pData   = New BYTE [ uAlloc ];

			pRegion->m_pLast   = New BYTE [ uAlloc ];

			pRegion->m_uSize   = uAlloc;

			memset(pRegion->m_pData, 0, uAlloc);

			m_pLock->Wait(FOREVER);

			AfxListAppend( m_pHead, 
				       m_pTail, 
				       pRegion, 
				       m_pNext, 
				       m_pPrev
				       );

			m_pLock->Free();

			pScan = pRegion;
			}

		pScan->Capture(pData, cx, cy, fPortrait);
		}
	}

void CProxyDisplay::Remove(UINT uHandle)
{
	CRegion *pScan = m_pHead;

	while( pScan ) {

		CRegion *pNext = pScan->m_pNext;

		if( uHandle == pScan->m_uHandle ) {
			
			break;
			}

		pScan = pNext;
		}

	if( pScan ) {

		m_pLock->Wait(FOREVER);

		AfxListRemove( m_pHead,
			       m_pTail,
			       pScan,
			       m_pNext,
			       m_pPrev
			       );

		delete pScan;

		m_pLock->Free();
		}
	}

// Entry Points

void CProxyDisplay::Init(UINT uID)
{
	m_pDisplay = (ICommsDisplay *) m_pDriver;
	}

void CProxyDisplay::Task(UINT uID)
{
	m_pCommsHelper->WontReturn();

	if( m_pPort->Open(m_pComms) ) {

		m_fOpen   = TRUE;

		for(;;) {

			m_pLock->Wait(FOREVER);

			CRegion *pScan = m_pHead;

			while( pScan ) {

				pScan->Service();

				pScan = pScan->m_pNext;
				}

			m_pLock->Free();

			m_pPort->Poll();

			ForceSleep(20);
			}
		}

	Sleep(FOREVER);
	}

void CProxyDisplay::Term(UINT uID)
{
	if( m_fOpen ) {

		m_pPort->Term();
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Display Proxy Definition
//

class CProxyDisplayDef
{
	public:
		// Constructor
		CProxyDisplayDef(IProxyDisplay *pProxy);

		// Attribute
		BOOL CheckPort(UINT uPort);

		// Data
		IProxyDisplay	     * m_pProxy;
		CProxyDisplayDef     * m_pNext;
		CProxyDisplayDef     * m_pPrev;
	};

//////////////////////////////////////////////////////////////////////////
//
// Display Proxy Definition
//

// Constructor

CProxyDisplayDef::CProxyDisplayDef(IProxyDisplay *pProxy)
{
	m_pProxy = pProxy;
	}

// Attribute

BOOL CProxyDisplayDef::CheckPort(UINT uPort)
{
	return m_pProxy->CheckPort(uPort);
	}

//////////////////////////////////////////////////////////////////////////
//
// Display Proxy Definition List
//

class CProxyDisplayDefList : public IProxyDisplayList
{
	public:
		// Constructor
		CProxyDisplayDefList(void);

		// Destructor
		~CProxyDisplayDefList(void);

		// IProxyDisplayList
		IProxyDisplay * FindProxy(UINT uPort);
		void            Append(IProxyDisplay *pProxy);

		// List
		CProxyDisplayDef * m_pHead;
		CProxyDisplayDef * m_pTail;
	};

//////////////////////////////////////////////////////////////////////////
//
// Display Proxy Definition List
//

// Instantiator

IProxyDisplayList * Create_ProxyDisplayList(void)
{
	return New CProxyDisplayDefList;
	}

// Constructor

CProxyDisplayDefList::CProxyDisplayDefList(void)
{
	m_pHead = NULL;

	m_pTail = NULL;
	}

// Destructor

CProxyDisplayDefList::~CProxyDisplayDefList(void)
{
	}

// Attributes

IProxyDisplay * CProxyDisplayDefList::FindProxy(UINT uPort)
{
	CProxyDisplayDef *pScan = m_pHead;

	while( pScan && !pScan->CheckPort(uPort) ) {

		pScan = pScan->m_pNext;
		}

	return pScan ? pScan->m_pProxy : NULL;
	}

// Operations

void CProxyDisplayDefList::Append(IProxyDisplay *pProxy)
{
	CProxyDisplayDef   *pDef = New CProxyDisplayDef(pProxy);

	AfxListAppend( m_pHead, 
		       m_pTail, 
		       pDef, 
		       m_pNext, 
		       m_pPrev
		       );
	}

// End of File
