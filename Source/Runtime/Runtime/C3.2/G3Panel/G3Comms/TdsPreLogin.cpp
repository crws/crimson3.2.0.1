
#include "Intern.hpp"

#include "TdsPreLogin.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// TDS Pre-Login Packet
//

// Constructors

CTdsPreLogin::CTdsPreLogin(void) : CTdsPacket(typePreLogin)
{
	m_uCount   = 0;

	m_pPayload = NULL;
	
	m_uPayload = 0;
	}

// Attributes

USHORT CTdsPreLogin::GetVersion(void) const
{
	COption const *pOpt = FindOption(0x00);

	if( pOpt ) {

		UINT uPos = 0;

		return pOpt->m_Data.GetULong(uPos) >> 24;
		}

	return 0;
	}

USHORT CTdsPreLogin::GetBuild(void) const
{
	COption const *pOpt = FindOption(0x00);

	if( pOpt ) {

		UINT uPos = 0;

		return pOpt->m_Data.GetULong(uPos) & 65535;
		}

	return 0;
	}

USHORT CTdsPreLogin::GetSubBuild(void) const
{
	COption const *pOpt = FindOption(0x00);

	if( pOpt ) {

		UINT uPos = 4;

		return pOpt->m_Data.GetUShort(uPos);
		}

	return 0;
	}

BYTE CTdsPreLogin::GetEncryption(void) const
{
	COption const *pOpt = FindOption(0x01);

	if( pOpt ) {

		UINT uPos = 0;

		return pOpt->m_Data.GetByte(uPos);
		}

	return 0;
	}

PCSTR CTdsPreLogin::GetInstance(void) const
{
	COption const *pOpt = FindOption(0x02);

	if( pOpt ) {

		UINT uPos = 0;

		return pOpt->m_Data.GetText(uPos);
		}

	return 0;
	}

ULONG CTdsPreLogin::GetTheadId(void) const
{
	COption const *pOpt = FindOption(0x03);

	if( pOpt ) {

		UINT uPos = 0;

		return pOpt->m_Data.GetULong(uPos);
		}

	return 0;
	}

BYTE CTdsPreLogin::GetMars(void) const
{
	COption const *pOpt = FindOption(0x04);

	if( pOpt ) {

		UINT uPos = 0;

		return pOpt->m_Data.GetByte(uPos);
		}

	return 0;
	}

PCBYTE CTdsPreLogin::GetPayloadData(void) const
{
	return m_pPayload;
	}

UINT CTdsPreLogin::GetPayloadSize(void) const
{
	return m_uPayload;
	}

// Operations

void CTdsPreLogin::Empty(void)
{
	m_uCount = 0;
	}

int CTdsPreLogin::Parse(PCBYTE pData, UINT uSize)
{
	Empty();

	int n = CTdsPacket::Parse(pData, uSize);

	if( n > 0 ) {

		if( m_Type == typeResponse ) {

			UINT uPos = 8;

			while( uPos < uSize ) {

				BYTE bToken = m_Data.GetByte(uPos);

				if( bToken < 255 ) {

					USHORT uFrom = m_Data.GetUShort(uPos) + 8;

					USHORT uData = m_Data.GetUShort(uPos) + 0;

					m_Opt[m_uCount].m_bToken = bToken;

					m_Opt[m_uCount].m_Data.AddData(pData + uFrom, uData);

					m_uCount++;

					continue;
					}

				return +1;
				}
			}

		if( m_Type == typePreLogin ) {

			m_pPayload = m_Data.GetData() + 8;

			m_uPayload = m_Data.GetSize() - 8;

			return +1;
			}

		return -1;
		}

	return n;
	}

void CTdsPreLogin::Create(PCSTR pName, ULONG uThread, BOOL fEncrypt)
{
	AddVersion(9, 0, 0);

	AddEncryption(fEncrypt ? 1 : 2);

	AddInstance(pName);

	AddThreadId(uThread);

	AddMars(false);

	EndPacket();
	}

void CTdsPreLogin::AddVersion(USHORT uVersion, USHORT uBuild, USHORT uSubBuild)
{	
	m_Opt[m_uCount].m_bToken = 0x00;

	m_Opt[m_uCount].m_Data.AddULong(uVersion << 24 | uBuild);

	m_Opt[m_uCount].m_Data.AddUShort(uSubBuild);

	m_uCount++;
	}

void CTdsPreLogin::AddEncryption(BYTE bMode)
{
	m_Opt[m_uCount].m_bToken = 0x01;

	m_Opt[m_uCount].m_Data.AddByte(bMode);

	m_uCount++;
	}

void CTdsPreLogin::AddInstance(PCSTR pName)
{
	m_Opt[m_uCount].m_bToken = 0x02;

	m_Opt[m_uCount].m_Data.AddText(pName);

	m_uCount++;
	}

void CTdsPreLogin::AddThreadId(ULONG uThread)
{
	m_Opt[m_uCount].m_bToken = 0x03;

	m_Opt[m_uCount].m_Data.AddULong(uThread);

	m_uCount++;
	}

void CTdsPreLogin::AddMars(bool fEnable)
{
	m_Opt[m_uCount].m_bToken = 0x04;

	m_Opt[m_uCount].m_Data.AddByte(BYTE(fEnable));

	m_uCount++;
	}

void CTdsPreLogin::AddPayload(PCBYTE pData, UINT uSize)
{
	m_pPayload = pData;
	
	m_uPayload = uSize;
	}

void CTdsPreLogin::EndPacket(void)
{
	UINT p = 1 + 5 * m_uCount;

	UINT n;

	for( n = 0; n < m_uCount; n++ ) {

		COption const &Opt = m_Opt[n];

		UINT          uLen = Opt.m_Data.GetSize();

		m_Data.AddByte(Opt.m_bToken);

		m_Data.AddUShort(USHORT(p));

		m_Data.AddUShort(USHORT(uLen));

		p += uLen;
		}

	if( m_uCount ) {

		m_Data.AddByte(0xFF);
		}

	for( n = 0; n < m_uCount; n++ ) {

		COption const &Opt = m_Opt[n];

		m_Data.AddData( Opt.m_Data.GetData(),
				Opt.m_Data.GetSize()
				);
		}

	if( m_pPayload ) {

		m_Data.AddData(m_pPayload, m_uPayload);
		}

	CTdsPacket::EndPacket();
	}

// Implementation

CTdsPreLogin::COption const * CTdsPreLogin::FindOption(BYTE bToken) const
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		COption const &Opt = m_Opt[n];

		if( Opt.m_bToken == bToken ) {

			return &Opt;
			}
		}

	return NULL;
	}

// End of File
