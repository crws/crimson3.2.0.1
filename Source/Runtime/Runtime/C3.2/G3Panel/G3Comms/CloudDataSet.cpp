
#include "Intern.hpp"

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cloud Data Set
//

// Constructor

CCloudDataSet::CCloudDataSet(void)
{
	m_pReq = NULL;

	m_pAck = NULL;

	m_fAck = FALSE;
}

// Destructor

CCloudDataSet::~CCloudDataSet(void)
{
	delete m_pReq;

	delete m_pAck;
}

// Initialization

void CCloudDataSet::Load(PCBYTE &pData)
{
	m_Mode    = GetByte(pData);

	m_History = GetByte(pData);

	m_Scan    = GetWord(pData) * 100;

	m_Force   = GetWord(pData);

	LoadTimer();

	GetCoded(pData, m_pReq);

	GetCoded(pData, m_pAck);

	GetCoded(pData, m_Suffix);
}

// Attributes

BOOL CCloudDataSet::IsEnabled(void) const
{
	return m_Mode >= 1;
}

UINT CCloudDataSet::GetPeriod(void) const
{
	return (m_Mode == 1) ? m_Scan : 1;
}

BOOL CCloudDataSet::UseHistory(void) const
{
	return m_History ? TRUE : FALSE;
}

BOOL CCloudDataSet::IsTriggered(void)
{
	if( m_Mode == 1 || m_Mode == 2 ) {

		BOOL fReq = GetItemData(m_pReq, TRUE);

		if( m_Mode == 1 ) {

			return fReq;
		}

		if( m_Mode == 2 ) {

			if( m_fAck ) {

				if( !fReq ) {

					SetAck(FALSE);
				}

				return FALSE;
			}

			return fReq;
		}
	}

	return FALSE;
}

// Operations

void CCloudDataSet::Tick(void)
{
	if( m_Mode == 1 || m_Mode == 2 ) {

		if( !m_fAck ) {

			if( m_Timer && !--m_Timer ) {

				ResetHistory();
			}
		}
	}
}

void CCloudDataSet::Init(void)
{
	SetItemScan(m_pReq, scanTrue);

	SetItemScan(m_pAck, scanTrue);

	LoadTimer();
}

BOOL CCloudDataSet::Fixup(void)
{
	FixCoded(m_Suffix, FALSE);

	if( !m_Suffix.StartsWith('/') ) {

		m_Suffix.Insert(0, '/');
	}

	return TRUE;
}

void CCloudDataSet::ResetHistory(void)
{
	SetAck(FALSE);

	LoadTimer();
}

void CCloudDataSet::DataWasSent(void)
{
	SetAck(TRUE);
}

// Implementation

void CCloudDataSet::SetAck(BOOL fAck)
{
	if( m_Mode == 2 ) {

		if( m_pAck ) {

			m_pAck->SetValue(fAck, typeInteger, setNone);
		}

		m_fAck = fAck;
	}
}

void CCloudDataSet::LoadTimer(void)
{
	m_Timer = (m_Force == 0xFFFF) ? 0 : m_Force;
}

// End of File
