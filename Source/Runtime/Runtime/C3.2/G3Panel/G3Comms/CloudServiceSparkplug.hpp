
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudServiceSparkplug_HPP

#define	INCLUDE_CloudServiceSparkplug_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CloudServiceCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMqttClientOptionsSparkplug;

class CMqttClientSparkplug;

//////////////////////////////////////////////////////////////////////////
//
// Sparkplug MQTT Service
//

class CCloudServiceSparkplug : public CCloudServiceCrimson
{
	public:
		// Constructor
		CCloudServiceSparkplug(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Service ID
		UINT GetID(void);
	};

// End of File

#endif
