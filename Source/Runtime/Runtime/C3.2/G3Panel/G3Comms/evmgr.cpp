
#include "intern.hpp"

#include "datalog.hpp"

#include "events.hpp"

#include "service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Event Manager
//

// Constants

static UINT const timeScale = 5;

static UINT const rateScale = 10;

// Constructor

CEventManager::CEventManager(void)
{
	m_pLogger   = New CEventLogger(this);
	
	m_pLock     = Create_Mutex();

	m_fBusy	    = FALSE;

	m_pHead     = NULL;

	m_pTail     = NULL;

	m_uSequence = 0;

	memset(m_uCount,  0, sizeof(m_uCount));

	memset(m_pSource, 0, sizeof(m_pSource));

	g_pEvStg->BindManager(this);

	StdSetRef();
	}

// Destructor

CEventManager::~CEventManager(void)
{
	for( CActiveAlarm *pInfo = m_pHead; pInfo; ) {

		CActiveAlarm *pNext = pInfo->m_pNext;

		delete pInfo;

		pInfo = pNext;
		}

	m_pLock->Release();

	delete m_pLogger;

	g_pEvStg->BindManager(NULL);
	}

// Initialization

void CEventManager::Load(void)
{
	m_pLogger->Load();
	}

// Serialization

BOOL CEventManager::Lock(void)
{
	return m_pLock->Wait(FOREVER);
	}

void CEventManager::Free(void)
{
	m_pLock->Free();
	}

// IUnknown

HRESULT CEventManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IEventManager);

	StdQueryInterface(IEventManager);

	return E_NOINTERFACE;
	}

ULONG CEventManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CEventManager::Release(void)
{
	StdRelease();
	}

// IEventConsumer

UINT CEventManager::Register(IEventSource *pSource, PCTXT pName)
{
	if( !strcmp(pName, "C3") ) {

		m_pSource[0] = pSource;

		return 0;
		}

	if( !strcmp(pName, "HC") ) {

		m_pSource[1] = pSource;

		return 1;
		}

	return NOTHING;
	}

BOOL CEventManager::LogEvent(DWORD dwTime, UINT Source, UINT Type, UINT Code, CUnicode const &Text)
{
	if( Source < NOTHING ) {

		if( !dwTime ) {

			dwTime = /*g_pServiceTimeSync ? g_pServiceTimeSync->GetLogTime() :*/ GetNowTimes5();
			}

		if( m_pLock->Wait(FOREVER) ) {

			CEventInfo Info;

			Info.m_Time    = dwTime;

			Info.m_Source  = Source;

			Info.m_Type    = Type;

			Info.m_HasText = 1;

			Info.m_Code    = Code;

			Info.m_Text    = Text;

			m_pLogger->LogEvent(Info, TRUE);

			m_pLock->Free();

			CCommsSystem::m_pThis->m_pLog->LogEvent(Info);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEventManager::LogEvent(DWORD dwTime, UINT Source, UINT Type, UINT Code)
{
	if( Source < NOTHING ) {

		if( !dwTime ) {

			dwTime = /*g_pServiceTimeSync ? g_pServiceTimeSync->GetLogTime() :*/ GetNowTimes5();
			}

		if( m_pLock->Wait(FOREVER) ) {

			CEventInfo Info;

			Info.m_Time    = dwTime;

			Info.m_Source  = Source;

			Info.m_Type    = Type;

			Info.m_HasText = 0;

			Info.m_Code    = Code;

			m_pLogger->LogEvent(Info, TRUE);

			m_pLock->Free();
			
			CCommsSystem::m_pThis->m_pLog->LogEvent(Info);

			return TRUE;
			}
		}

	return FALSE;
	}

CActiveAlarm * CEventManager::FindAlarm(UINT Source, UINT Code)
{
	CActiveAlarm *pInfo = m_pHead;

	while( pInfo ) {

		if( pInfo->m_Source == Source ) {

			if( pInfo->m_Code == Code ) {

				return pInfo;
				}
			}

		pInfo = pInfo->m_pNext;
		}

	return NULL;
	}

CActiveAlarm * CEventManager::MakeAlarm(UINT Source, UINT Code)
{
	CActiveAlarm *pInfo = New CActiveAlarm;

	pInfo->m_Time       = 0;

	pInfo->m_Source     = Source;

	pInfo->m_Code       = Code;

	pInfo->m_Level      = 0;

	pInfo->m_pData      = NULL;

	pInfo->m_State      = alarmIdle;

	AfxListAppend(m_pHead, m_pTail, pInfo, m_pNext, m_pPrev);

	m_uCount[alarmIdle ]++;

	m_uCount[alarmTotal]++;

	m_uSequence++;

	return pInfo;
	}

void CEventManager::EditAlarm(CActiveAlarm *pInfo)
{
	m_uSequence++;
	}

void CEventManager::EditAlarm(CActiveAlarm *pInfo, UINT State)
{
	UINT Leave = pInfo->m_State;

	if( State == alarmIdle ) {

		AfxListRemove(m_pHead, m_pTail, pInfo, m_pNext, m_pPrev);

		delete pInfo;

		m_uCount[Leave     ]--;

		m_uCount[alarmIdle ]--;

		m_uCount[alarmTotal]--;
		}
	else {
		pInfo->m_State = State;

		m_uCount[Leave]--;
	
		m_uCount[State]++;
		}

	if( Leave != alarmIdle ) {

		if( !m_uCount[alarmActive] ) {
			
			if( !m_uCount[alarmWaitAccept] ) {

				if( !m_uCount[alarmAutoAccept] ) {

					g_pPxe->SetSiren(FALSE);
					}
				}
			}
		}

	m_uSequence++;

	if( GetUnacceptedAlarms() ) {

		g_pPxe->SetAlarm(2);

		return;
		}

	g_pPxe->SetAlarm(GetActiveAlarms() ? 1 : 0);
	}

void CEventManager::MoveAlarm(CActiveAlarm *pInfo)
{
	AfxListRemove(m_pHead, m_pTail, pInfo, m_pNext, m_pPrev);

	CActiveAlarm *pScan = m_pHead;
		
	while( pScan ) {

		if( pScan->m_Level == pInfo->m_Level ) {

			if( pScan->m_Time < pInfo->m_Time ) {

				break;
				}
			}

		if( pScan->m_Level > pInfo->m_Level ) {

			break;
			}

		pScan = pScan->m_pNext;
		}

	AfxListInsert(m_pHead, m_pTail, pInfo, m_pNext, m_pPrev, pScan);
	}

// IAlarmStatus

void CEventManager::LockAlarmData(void)
{
	m_pLock->Wait(FOREVER);
	}

void CEventManager::FreeAlarmData(void)
{
	m_pLock->Free();
	}

UINT CEventManager::GetTotalAlarms(void)
{
	return m_uCount[alarmTotal];
	}

UINT CEventManager::GetActiveAlarms(void)
{
	if( WhoHasFeature(rfHoneywell) ) {

		return m_uCount[alarmActive    ] +
		       m_uCount[alarmAutoAccept] +
		       m_uCount[alarmAccepted  ] + 
		       m_uCount[alarmWaitAccept];
		}

	return m_uCount[alarmTotal];
	}

UINT CEventManager::GetUnacceptedAlarms(void)
{
	return m_uCount[alarmActive    ] +
	       m_uCount[alarmWaitAccept] ;
	}

UINT CEventManager::GetUnacceptedAndAutoAlarms(void)
{
	return m_uCount[alarmActive    ] +
	       m_uCount[alarmWaitAccept] +
	       m_uCount[alarmAutoAccept];
	}

UINT CEventManager::GetAlarmSequence(void)
{
	return m_uSequence;
	}

UINT CEventManager::ReadAlarmList(CActiveAlarm * &pHead, UINT uPos, UINT uSize, BOOL fReverse)
{
	if( m_pLock->Wait(FOREVER) ) {

		CActiveAlarm * pTail = NULL;

		CActiveAlarm * pInfo = m_pHead;
				   
		UINT           uCopy = 0;

		if( fReverse ) {
			
			UINT uTotal = GetTotalAlarms();

			if(uSize < uTotal) {

				uPos = uTotal - uPos - uSize;
				}
			}

		while( pInfo ) {

			if( !uPos-- ) {

				break;
				}

			pInfo = pInfo->m_pNext;
			}

		while( pInfo ) {

			if( !uSize-- ) {

				break;
				}

			CActiveAlarm *pCopy = New CActiveAlarm;

			pCopy->m_Time       = pInfo->m_Time / 5;

			pCopy->m_Source     = pInfo->m_Source;

			pCopy->m_Code       = pInfo->m_Code;

			pCopy->m_Level      = pInfo->m_Level;

			pCopy->m_Text       = pInfo->m_Text;

			pCopy->m_State      = pInfo->m_State;

			pCopy->m_pData      = pInfo->m_pData;

			if( pCopy->m_Text.IsEmpty() ) {

				IEventSource *pSource = FindSource(pInfo->m_Source);

				if( pSource ) {

					pSource->GetEventText( pCopy->m_Text,
							       pCopy->m_Code
							       );
					}
				}

			if( fReverse ) {

				AfxListInsert(pHead, pTail, pCopy, m_pNext, m_pPrev, pHead);
				}
			else
				AfxListAppend(pHead, pTail, pCopy, m_pNext, m_pPrev);

			uCopy = uCopy + 1;

			pInfo = pInfo->m_pNext;
			}

		m_pLock->Free();

		return uCopy;
		}

	return 0;
	}

void CEventManager::AcceptAlarm(UINT Source, UINT Code)
{
	if( m_pLock->Wait(FOREVER) ) {

		if( !m_fBusy ) {

			m_fBusy = TRUE;

			CActiveAlarm *pInfo = m_pHead;

			while( pInfo ) {

				if( pInfo->m_Source == Source ) {

					if( pInfo->m_Code == Code ) {

						AcceptAlarm(pInfo);

						break;
						}
					}

				pInfo = pInfo->m_pNext;
				}

			m_fBusy = FALSE;
			}

		m_pLock->Free();
		}
	}

BOOL CEventManager::AcceptAlarmEx(UINT Source, UINT Method, UINT Code)
{
	IEventSource *pSource = FindSource(Source);

	if( pSource ) {

		pSource->AcceptAlarm(Method, Code);

		return TRUE;
		}

	return FALSE;
	}

void CEventManager::AcceptAlarm(CActiveAlarm *pInfo)
{
	if( pInfo->m_State == alarmActive || pInfo->m_State == alarmWaitAccept ) {

		IEventSource *pSource = FindSource(pInfo->m_Source);

		if( pSource ) {

			pSource->AcceptAlarm(pInfo);
			}
		}
	}

void CEventManager::AcceptAll(void)
{
	if( m_pLock->Wait(FOREVER) ) {

		if( !m_fBusy ) {

			m_fBusy = TRUE;

			CActiveAlarm *pInfo = m_pTail;

			while( pInfo ) {

				CActiveAlarm *pPrev = pInfo->m_pPrev;

				AcceptAlarm(pInfo);

				pInfo = pPrev;
				}

			m_fBusy = FALSE;
			}

		m_pLock->Free();
		}
	}

// Source Location

IEventSource * CEventManager::FindSource(UINT Source)
{
	if( Source < elements(m_pSource) ) {

		return m_pSource[Source];
		}

	return NULL;
	}

// End of File
