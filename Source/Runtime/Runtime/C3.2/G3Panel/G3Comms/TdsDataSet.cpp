//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "Intern.hpp"

#include "TdsDataSet.hpp"


//////////////////////////////////////////////////////////////////////////
//
// TDS DataSet (COLMETADATA) Packet
//


// Constructors

CTdsDataSet::CTdsDataSet(void) : CTdsPacket(typeResponse)
{
	m_uNumColumns = 0;
	m_pColumns = NULL;

	m_uNumRows = 0;

	}


CTdsDataSet::CTdsDataSet(UINT BufferSize) : CTdsPacket(typeResponse, BufferSize)
{
	m_uNumColumns = 0;
	m_pColumns = NULL;

	m_uNumRows = 0;

	}


CTdsDataSet::~CTdsDataSet(void)
{
	FreeColumnInfo();

	}


// Attributes

UINT CTdsDataSet::GetNumColumns(void) const
{
	return m_uNumColumns;
	}


BYTE CTdsDataSet::GetColumnType(UINT &uIndex) const
{
	if ((m_pColumns != NULL) && (uIndex < m_uNumColumns))
		return m_pColumns[uIndex].ColumnType;
	else
		return 0;
	}


USHORT CTdsDataSet::GetColumnTypeSize(UINT &uIndex) const
{
	if ((m_pColumns != NULL) && (uIndex < m_uNumColumns))
		return m_pColumns[uIndex].ColumnTypeSize;
	else
		return 0;
	}

PCSTR CTdsDataSet::GetColumnName(UINT &uIndex) const
{
	if ((m_pColumns != NULL) && (uIndex < m_uNumColumns))
		return m_pColumns[uIndex].ColumnName;
	else
		return NULL;
	}

UINT CTdsDataSet::GetNumRows(void) const
{
	return m_uNumRows;
	}

// Parsing

int CTdsDataSet::Parse(PCBYTE pData, UINT uSize)
{
	int n = CTdsPacket::Parse(pData, uSize);

	if( n > 0 ) {

		// Fail if this packet is not a response packet with COLMETADATA stream.
		UINT uBytePos = 8;

		if ((GetType() != typeResponse) || (GetByte(uBytePos) != TDS_COLMETADATA_TOKEN)) {
			
			return false;
			}

		// Read the number of columns.
		m_uNumColumns = GetUShortReverse(uBytePos);

		// Read the column information.
		ReadColumnInfo(uBytePos);

		// Count the number of rows.
		UINT uHoldBytePos = uBytePos;
		ParseOrdering(uBytePos);
		CountRows(uBytePos);
		uBytePos = uHoldBytePos;

		// Allocate storage for the row data.
		CreateRowStorage();

		ParseOrdering(uBytePos);

		// Parse the row data.
		ParseRows(uBytePos);

		return 1;
		}

	return n;
	}

// Operations


BYTE  CTdsDataSet::GetByteField(UINT uRowIndex, UINT uColIndex)
{
	if ((m_pColumns != NULL) && (uRowIndex < m_uNumRows) && (uColIndex < m_uNumColumns))
		return ((BYTE*)m_pColumns[uColIndex].RowData)[uRowIndex];
	else
		return 0;
	}


SHORT  CTdsDataSet::GetShortField(UINT uRowIndex, UINT uColIndex)
{
	if ((m_pColumns != NULL) && (uRowIndex < m_uNumRows) && (uColIndex < m_uNumColumns))
		return ((SHORT*)m_pColumns[uColIndex].RowData)[uRowIndex];
	else
		return 0;
	}


LONG  CTdsDataSet::GetLongField(UINT uRowIndex, UINT uColIndex)
{
	if ((m_pColumns != NULL) && (uRowIndex < m_uNumRows) && (uColIndex < m_uNumColumns))
		return ((LONG*)m_pColumns[uColIndex].RowData)[uRowIndex];
	else
		return 0;
	}


LONGLONG  CTdsDataSet::GetLongLongField(UINT uRowIndex, UINT uColIndex)
{
	if ((m_pColumns != NULL) && (uRowIndex < m_uNumRows) && (uColIndex < m_uNumColumns))
		return ((LONGLONG*)m_pColumns[uColIndex].RowData)[uRowIndex];
	else
		return 0;
	}


FLOAT  CTdsDataSet::GetFloatField(UINT uRowIndex, UINT uColIndex)
{
	if ((m_pColumns != NULL) && (uRowIndex < m_uNumRows) && (uColIndex < m_uNumColumns))
		return ((FLOAT*)m_pColumns[uColIndex].RowData)[uRowIndex];
	else
		return 0.0;
	}


DOUBLE  CTdsDataSet::GetDoubleField(UINT uRowIndex, UINT uColIndex)
{
	if ((m_pColumns != NULL) && (uRowIndex < m_uNumRows) && (uColIndex < m_uNumColumns))
		return ((DOUBLE*)m_pColumns[uColIndex].RowData)[uRowIndex];
	else
		return 0.0;
	}


PCSTR  CTdsDataSet::GetVarcharField(UINT uRowIndex, UINT uColIndex)
{
	if ((m_pColumns != NULL) && (uRowIndex < m_uNumRows) && (uColIndex < m_uNumColumns))
		return (PCSTR)(m_pColumns[uColIndex].RowData + (uRowIndex * (m_pColumns[uColIndex].ColumnTypeSize + 1)));
	else
		return NULL;
	}

// Helper operations

void CTdsDataSet::FreeColumnInfo()
{
	if (m_pColumns != NULL)
	{
		// Free components of each column.
		for (USHORT uColIndex = 0; uColIndex < m_uNumColumns; uColIndex++)
		{
			// Free column name string.
			if (m_pColumns[uColIndex].ColumnName != NULL)
				delete [] m_pColumns[uColIndex].ColumnName;

			// Free row data.
			if (m_pColumns[uColIndex].RowData != NULL)
				delete [] m_pColumns[uColIndex].RowData;
			}

		// Free the column info array.
		delete [] m_pColumns;
		}

	}


void CTdsDataSet::CreateColumnInfo()
{
	FreeColumnInfo();
	m_pColumns = New CColumnInfo [m_uNumColumns];

	}


bool CTdsDataSet::ReadColumnInfo(UINT &uPos)
{
	// Create the column info array.
	CreateColumnInfo();

	// Read the column metadata from the message.
	for (USHORT uColIndex = 0; uColIndex < m_uNumColumns; uColIndex++)
	{
		uPos += 6;  // Skip user type and flags.

		// Read column type
		m_pColumns[uColIndex].ColumnType = GetByte(uPos);

		// Read type size

		if( !IsFixedSizeType(m_pColumns[uColIndex].ColumnType) ) {

			m_pColumns[uColIndex].ColumnTypeSize = USHORT(GetVarTypeSize(m_pColumns[uColIndex].ColumnType, uPos));

			if( HasCollationInfo(m_pColumns[uColIndex].ColumnType) ) {

				uPos += 5;  // Skip collation
				}
			}
		else {
			m_pColumns[uColIndex].ColumnTypeSize = USHORT(GetFixedSize(m_pColumns[uColIndex].ColumnType));
			}

		// Read column name
		BYTE bNameLen = GetByte(uPos);
		m_pColumns[uColIndex].ColumnName = New CHAR [bNameLen + 1];
		GetReverseCharacterData((CHAR*)m_pColumns[uColIndex].ColumnName, bNameLen + 1, uPos);

		// Mark the row data as empty.
		m_pColumns[uColIndex].RowData = NULL;
		}

	// Indicate success.
	return true;

	}


void CTdsDataSet::CountRows(UINT &uPos)
{
	m_uNumRows = 0;

	// Keep counting as long as there is a next row.
	while( GetByte(uPos) == TDS_ROW_TOKEN ) {

		// Increment the count.
		m_uNumRows++;

		// Read to the next row or the end of the row data.
		for( UINT uColIndex = 0; uColIndex < m_uNumColumns; uColIndex++ ) {

			if( IsFixedSizeType(m_pColumns[uColIndex].ColumnType) ) {

				uPos += m_pColumns[uColIndex].ColumnTypeSize;
				}
			else {
				WORD wLen = WORD(GetVarTypeSize(m_pColumns[uColIndex].ColumnType, uPos));

				if( wLen < 0xFFFF ) {

					uPos += wLen;
					}
				}
			}
		}
	}


void CTdsDataSet::CreateRowStorage()
{
	UINT rowbytes = 0;

	// Create row storage for each column.
	for (UINT uIndex=0; uIndex < m_uNumColumns; uIndex++)
	{
		// Determine the memory size required for each value.
		UINT uSize = m_pColumns[uIndex].ColumnTypeSize + 1;

		rowbytes += (m_uNumRows * uSize);

		// Allocate the storage for the entire row.
		m_pColumns[uIndex].RowData = New BYTE [m_uNumRows * uSize];
		}
	}


void CTdsDataSet::ParseRows(UINT &uPos)
{
	UINT  uRowIndex = 0;
	UINT  uFieldSize;
	CHAR* pFieldStart = NULL;

	// Keep parsing as long as there is a next row.
	while ( GetByte(uPos) == TDS_ROW_TOKEN)
	{
		// Read column values up to the next row or the end of the row data.
		for (UINT uColIndex=0; uColIndex < m_uNumColumns; uColIndex++) {

			UINT uType = m_pColumns[uColIndex].ColumnType;

			if( IsFixedSizeType(uType) ) {

				UINT uTypeSize = m_pColumns[uColIndex].ColumnTypeSize;

				if( IsDateTime(uType) ) {

					if( uTypeSize == 8 ) {

						((ULONGLONG*)m_pColumns[uColIndex].RowData)[uRowIndex] = ParseDateTime(uType, uTypeSize, uPos);
						}
					}
				else {
					switch( uTypeSize ) {

						case 1:
							((BYTE     *)m_pColumns[uColIndex].RowData)[uRowIndex] = GetByte(uPos);

							break;
						case 2:
							((USHORT   *)m_pColumns[uColIndex].RowData)[uRowIndex] = GetUShortReverse(uPos);

							break;
						case 4:
							((ULONG    *)m_pColumns[uColIndex].RowData)[uRowIndex] = GetULongReverse(uPos);

							break;
						case 8:
							((ULONGLONG*)m_pColumns[uColIndex].RowData)[uRowIndex] = GetULongLongReverse(uPos);

							break;

						default:
							break;
						}
					}
				}
			else {
				uFieldSize  = GetVarTypeSize(m_pColumns[uColIndex].ColumnType, uPos);

				bool fIsNull = false;

				if( uFieldSize == 0 ) {

					fIsNull = true;
					}

				if( IsString(uType) ) {

					pFieldStart = (CHAR*)(&m_pColumns[uColIndex].RowData[uRowIndex * (m_pColumns[uColIndex].ColumnTypeSize + 1)]);

					if( uFieldSize == 0xFFFF ) {

						for( UINT n = 0; n < m_pColumns[uColIndex].ColumnTypeSize; n++ ) {

							pFieldStart[n] = '\0';
							}
						}
					else {
						if( IsWideString(m_pColumns[uColIndex].ColumnType) ) {

							GetReverseCharacterData(pFieldStart, USHORT((uFieldSize/2)+1), uPos);
							}
						else {
							GetByteCharacterData(pFieldStart, USHORT(uFieldSize+1), uPos);
							}
						}
					}
				
				else if( IsDateTime(uType) ) {

					// LATER -- additional Datetime/Date/Time types

					if( uFieldSize == 8 ) {

						((ULONGLONG*)m_pColumns[uColIndex].RowData)[uRowIndex] = fIsNull ? 0 : ParseDateTime(uType, uFieldSize, uPos);
						}
					}
				else {
					UINT uTest = fIsNull ? m_pColumns[uColIndex].ColumnTypeSize : uFieldSize;

					switch( uTest ) {

						case 1:
							((BYTE     *)m_pColumns[uColIndex].RowData)[uRowIndex] = fIsNull ? 0 : GetByte(uPos);

							break;
						case 2:
							((USHORT   *)m_pColumns[uColIndex].RowData)[uRowIndex] = fIsNull ? 0 : GetUShortReverse(uPos);

							break;
						case 4:
							((ULONG    *)m_pColumns[uColIndex].RowData)[uRowIndex] = fIsNull ? 0 : GetULongReverse(uPos);

							break;
						case 8:
							((ULONGLONG*)m_pColumns[uColIndex].RowData)[uRowIndex] = fIsNull ? 0 : GetULongLongReverse(uPos);

							break;

						default:

							break;
						}
					}
				}
			}

		// Next row.
		uRowIndex++;
		}
	}

void CTdsDataSet::ParseOrdering(UINT &uPos)
{
	UINT uOrigPos = uPos;

	if( GetByte(uPos) == 0xA9 ) {

		WORD wLen = GetUShortReverse(uPos);

		for( UINT n = 0; n < wLen; n++ ) {

			GetByte(uPos);
			}
		}
	else {
		uPos = uOrigPos;
		}
	}

// Type Helpers

bool CTdsDataSet::IsFixedSizeType(UINT uType)
{
	return (uType & 0x30) == 0x30;
	}

bool CTdsDataSet::IsString(UINT uType)
{
	switch( uType ) {

		case 0x27:
		case 0x67:
		case 0xA7:
		case 0xAF:
		case 0xE7:
		case 0x2F:
		case 0xEF:
			return true;
		}

	return false;
	}

bool CTdsDataSet::IsDateTime(UINT uType)
{
	switch( uType ) {

		case 0x3D:
		case 0x6F:
			return true;
		}

	return false;
	}

bool CTdsDataSet::IsWideString(UINT uType)
{
	switch( uType ) {

		case 0x67:
		case 0xE7:
			return true;
		}

	return false;
	}

bool CTdsDataSet::HasCollationInfo(UINT uType)
{
	switch( uType ) {

		case 0x23:
		case 0x63:
		case 0xA7:
		case 0xAF:
		case 0xE7:
		case 0xEF:

			return true;
		}

	return false;
	}

UINT CTdsDataSet::GetVarWidth(UINT uType)
{
	switch( uType ) {

		case 0x24:
		case 0x25:
		case 0x26:
		case 0x27:
		case 0x2D:
		case 0x2F:
		case 0x67:
		case 0x68:
		case 0x6A:
		case 0x6C:
		case 0x6D:
		case 0x6E:
		case 0x6F:
			return 1;
 
		case 0xA5:
		case 0xA7:
		case 0xAD:
		case 0xAF:
		case 0xE7:
		case 0xEF:
			return 2;

		case 0x22:
		case 0x23:
		case 0x62:
		case 0x63:
		case 0xE1:
			return 4;
		}

	return 0;
	}

UINT CTdsDataSet::GetFixedSize(UINT uType)
{
	if( IsFixedSizeType(uType) ) {

		BYTE bSize = (uType & 0x0C) >> 2;

		switch( bSize ) {

			case 0:
				return 1;
			case 1:
				return 2;
			case 2:
				return 4;
			case 3:
				return 8;
			}
		}

	return 0;
	}

UINT CTdsDataSet::GetVarTypeSize(UINT uType, UINT &uPos)
{
	UINT uWidth = GetVarWidth(uType);

	switch( uWidth ) {

		case 1:
			return GetByte(uPos);

		case 2:
			return GetUShortReverse(uPos);

		case 4:
			return GetULongReverse(uPos);
		}

	return 0;
	}

UINT CTdsDataSet::ParseDateTime(UINT uType, UINT uSize, UINT &uPos)
{
	UINT t = 0;

	if( IsDateTime(uType) ) {

		if( uSize == 8 ) {

			int Days    = GetULongReverse(uPos);

			int Seconds = GetULongReverse(uPos);

			// NOTE -- 35429 = The number of days between 1/1/1997 and 1/1/1900

			t           = (Days - 35429) * 60 * 60 * 24;

			t          += (Seconds / 300);
			}
		}

	return t;
	}

// End of File
