
#include "intern.hpp"

#include "events.hpp"

#include "datalog.hpp"

#include "secure.hpp"

#include "service.hpp"

#include "lang.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Logger
//

#define MallocOrNull(x) Malloc(x) /*!!!C3!!!*/

// Constants

static UINT const timeScale    = 5;

static UINT const rateScale    = 10;

static UINT const savePeriod   = 120;

static UINT const maxPerLog    = 1024 * 512;

static UINT const minForSystem = 1024 * 2048;

static UINT const maxTotal     = 1024 * 4096;

// Instance Pointer

CDataLogger * CDataLogger::m_pThis = NULL;

// Constructor

CDataLogger::CDataLogger(void)
{
	m_EnableBatch = FALSE;

	m_EnableSets  = FALSE;

	m_BatchCount  = 10;

	m_BatchDrive  = 0;

	m_CSVEncode   = 0;

	m_pLogs       = New CDataLogList;

	m_pInfo       = NULL;

	m_pInitSave   = Create_ManualEvent();

	m_pInitScan   = Create_ManualEvent();

	m_pSave       = Create_AutoEvent();

	m_pDone	      = Create_ManualEvent();

	m_fChange     = FALSE;

	m_pThis       = this;
}

// Destructor

CDataLogger::~CDataLogger(void)
{
	delete m_pLogs;

	delete m_pInfo;

	m_pInitSave->Release();

	m_pInitScan->Release();

	m_pSave->Release();

	m_pDone->Release();

	m_pThis = NULL;
}

// Initialization

void CDataLogger::Load(PCBYTE &pData)
{
	ValidateLoad("CDataLogger", pData);

	FindPoolSize();

	m_EnableBatch = GetByte(pData);

	m_EnableSets  = GetByte(pData);

	m_BatchCount  = GetWord(pData);

	m_BatchDrive  = GetByte(pData);

	m_CSVEncode   = GetByte(pData);

	m_pLogs->Load(pData);

	if( GetByte(pData) ) {

		m_pInfo = New CDataLogInfo;

		m_pInfo->Load(pData);
	}

	if( WhoHasFeature(rfLogToDisk) ) {

		CLogHelper::UseSetLayout(m_EnableSets);
	}

	m_pEvents = CEventLogger::m_pThis;

	m_pSecure = CCommsSystem::m_pThis->m_pSecure;

	LoadBatches();
}

// Task List

void CDataLogger::GetTaskList(CTaskList &List)
{
	CTaskDef Task;

	Task.m_Name   = "LOGSCAN";
	Task.m_pEntry = this;
	Task.m_uID    = 0;
	Task.m_uCount = 1;
	Task.m_uLevel = 8400;
	Task.m_uStack = 0;

	List.Append(Task);

	if( m_pLogs->m_uUsed ) {

		if( m_pLogs->HasPolled() ) {

			Task.m_Name   = "LOGPOLL";
			Task.m_pEntry = this;
			Task.m_uID    = 1;
			Task.m_uCount = 1;
			Task.m_uLevel = 8600;
			Task.m_uStack = 0;

			List.Append(Task);
		}
	}

	Task.m_Name   = "LOGSAVE";
	Task.m_pEntry = this;
	Task.m_uID    = 2;
	Task.m_uCount = 1;
	Task.m_uLevel = 3200;
	Task.m_uStack = 0;

	List.Append(Task);
}

// Task Entries

void CDataLogger::TaskInit(UINT uID)
{
	switch( uID ) {

		case 0: ScanInit(); break;
		case 1: PollInit(); break;
		case 2: SaveInit(); break;
	}
}

void CDataLogger::TaskExec(UINT uID)
{
	switch( uID ) {

		case 0: ScanExec(); break;
		case 1: PollExec(); break;
		case 2: SaveExec(); break;
	}
}

void CDataLogger::TaskStop(UINT uID)
{
}

void CDataLogger::TaskTerm(UINT uID)
{
	switch( uID ) {

		case 0: ScanTerm(); break;
		case 1: PollTerm(); break;
		case 2: SaveTerm(); break;
	}
}

// Attributes

UINT CDataLogger::GetEachSize(void) const
{
	// REV3 -- An optimization would be to pro rate the pool
	// between the logs based on the number of tags they contain
	// or even some weighting set by the user.

	return min(m_uPool / m_pLogs->m_uCount, maxPerLog);
}

BOOL CDataLogger::IsActive(void) const
{
	return m_pLogs->IsActive();
}

PCTXT CDataLogger::GetBatch(UINT uSlot) const
{
	if( uSlot < 8 ) {

		return m_Batch[uSlot];
	}

	return "";
}

// Operations

void CDataLogger::NewBatch(UINT uSlot, PCTXT pName)
{
	if( m_EnableBatch ) {

		if( uSlot < 8 ) {

			if( strlen(pName) <= 8 ) {

				if( m_Batch[uSlot] != pName ) {

					DWORD Time = GetLogTime();

					m_pLogs->LogNewBatch(uSlot, Time, pName);

					m_pEvents->LogNewBatch(uSlot, Time, pName);

					m_pSecure->LogNewBatch(uSlot, Time, pName);

					m_Batch[uSlot] = pName;

					m_Start[uSlot] = Time;

					SaveBatch(uSlot);

					m_pSave->Set();
				}
			}
		}
	}
}

void CDataLogger::LogEvent(CEventInfo const &Info)
{
	m_pLogs->LogEvent(Info);
}

BOOL CDataLogger::LogComment(UINT uLog, PCTXT pText, BOOL fHead)
{
	if( uLog < m_pLogs->m_uCount ) {

		CDataLog *pLog = m_pLogs->m_ppLog[uLog];

		if( pLog ) {

			if( pLog->m_Comments ) {

				CEventInfo Info;

				Info.m_Time    = GetLogTime();

				Info.m_Source  = 7;

				Info.m_Type    = 0;

				Info.m_HasText = 1;

				Info.m_Code    = 0;

				Info.m_Text    = pText;

				if( fHead ) {

					if( pLog->m_WithBatch ) {

						Info.m_Time = m_Start[pLog->m_WithBatch - 1];

						Info.m_Type = 1;
					}
				}

				pLog->LogEvent(Info);

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CDataLogger::BatchComment(UINT uSlot, PCTXT pText, BOOL fHead)
{
	for( UINT uLog = 0; uLog < m_pLogs->m_uCount; uLog++ ) {

		CDataLog *pLog = m_pLogs->m_ppLog[uLog];

		if( pLog->m_WithBatch == uSlot + 1 ) {

			LogComment(uLog, pText, fHead);
		}
	}

	return TRUE;
}

void CDataLogger::ForceSave(void)
{
	if( m_pInitSave->Wait(0) ) {

		m_pDone->Clear();

		m_pSave->Set();
	}
}

void CDataLogger::WaitSave(void)
{
	if( m_pInitSave->Wait(0) ) {

		m_pDone->Wait(FOREVER);
	}
}

// Scan Task

void CDataLogger::ScanInit(void)
{
	m_pLogs->PreRegister();
}

void CDataLogger::ScanExec(void)
{
	m_pInitSave->Wait(FOREVER);

	UINT  dt = m_pLogs->GetHCF();

	DWORD t1 = GetLogTime();

	if( m_EnableBatch ) {

		for( UINT uSlot = 0; uSlot < 8; uSlot++ ) {

			if( !m_Batch[uSlot].IsEmpty() ) {

				m_pLogs->LogNewBatch(uSlot, t1, m_Batch[uSlot]);

				m_pEvents->LogNewBatch(uSlot, t1, m_Batch[uSlot]);

				m_pSecure->LogNewBatch(uSlot, t1, m_Batch[uSlot]);

				m_Start[uSlot] = t1;
			}
		}
	}

	m_pInitScan->Set();

	for( ;;) {

		DWORD t2 = ((t1 + dt - 1) / dt) * dt;

		DWORD t3 = GetLogTime();

		DWORD t4 = 0;

		while( !m_fChange ) {

			t4 = t3;

			t3 = GetLogTime();

			if( t3 < t4 || t3 > t4 + 30 * dt ) {

				m_fChange = TRUE;

				break;
			}

			if( t3 < t2 ) {

				UINT t = (1000 / timeScale) * (t2 - t3);

				Sleep(min(t, 30 * 1000));

				continue;
			}

			break;
		}

		if( t2 <= t3 ) {

			m_pLogs->LogData(t2);

			if( t2 % (savePeriod * timeScale) == 0 ) {

				m_pSave->Set();
			}

			t3++;
		}

		if( m_fChange ) {

			t1        = t3;

			m_fChange = FALSE;

			continue;
		}

		t1 = t2 + 1;
	}
}

void CDataLogger::ScanTerm(void)
{
}

// Poll Task

void CDataLogger::PollInit(void)
{
}

void CDataLogger::PollExec(void)
{
	m_pInitScan->Wait(FOREVER);

	for( ;;) {

		DWORD t = GetLogTime();

		m_pLogs->LogPoll(t);

		Sleep(25);
	}
}

void CDataLogger::PollTerm(void)
{
}

// Save Task

void CDataLogger::SaveInit(void)
{
}

void CDataLogger::SaveExec(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		SetTimer(1000);

		m_pEvents->LogCheck();

		m_pSecure->LogCheck();

		m_pLogs->LogCheck();

		m_pLogs->LogLoad();

		Sleep(GetTimer());

		m_pInitSave->Set();

		for( ;;) {

			m_pSave->Wait(FOREVER);

			m_pEvents->LogSave();

			m_pSecure->LogSave();

			m_pLogs->LogSave();

			m_pDone->Set();
		}
	}

	for( ;;) Sleep(FOREVER);
}

void CDataLogger::SaveTerm(void)
{
}

// Implementation

DWORD CDataLogger::GetLogTime(void)
{
	return /*g_pServiceTimeSync ? g_pServiceTimeSync->GetLogTime() :*/ GetNowTimes5();
}

void CDataLogger::LoadBatches(void)
{
	for( UINT n = 0; n < 8; n++ ) {

		LoadBatch(n);
	}
}

void CDataLogger::LoadBatch(UINT uSlot)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		UINT  uAddr = uSlot ? (Mem(DataLog1) + 0x10 * (uSlot-1)) : Mem(DataLog0);

		DWORD Magic = 0;

		FRAMGetData(uAddr+0x00, PBYTE(&Magic), 4);

		if( Magic == 0x42414241 ) {

			char sBatch[10];

			FRAMGetData(uAddr+0x04, PBYTE(sBatch), 10);

			m_Batch[uSlot] = sBatch;
		}
	}
}

void CDataLogger::SaveBatch(UINT uSlot)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		UINT  uAddr = uSlot ? (Mem(DataLog1) + 0x10 * (uSlot-1)) : Mem(DataLog0);

		DWORD Magic = 0x42414241;

		FRAMPutData(uAddr+0x04, PBYTE(PCTXT(m_Batch[uSlot])), 10);

		FRAMPutData(uAddr+0x00, PBYTE(&Magic), 4);
	}
}

void CDataLogger::FindPoolSize(void)
{
	m_uPool = maxTotal;
}

//////////////////////////////////////////////////////////////////////////
//
// Data Log Info (Honeywell Specific)
//

// Constructor

CDataLogInfo::CDataLogInfo(void)
{
	for( UINT n = 0; n < elements(m_pData); n++ ) {

		m_pData[n] = NULL;
	}
}

// Destructor

CDataLogInfo::~CDataLogInfo(void)
{
	for( UINT n = 0; n < elements(m_pData); n++ ) {

		delete m_pData[n];
	}
}

// Initialization

void CDataLogInfo::Load(PCBYTE &pData)
{
	ValidateLoad("CDataLogInfo", pData);

	for( UINT n = 0; n < elements(m_pData); n++ ) {

		GetCoded(pData, m_pData[n]);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Data Log List
//

// Constructor

CDataLogList::CDataLogList(void)
{
	m_uCount = 0;

	m_uUsed  = 0;

	m_ppLog  = NULL;
}

// Destructor

CDataLogList::~CDataLogList(void)
{
	while( m_uCount-- ) {

		delete m_ppLog[m_uCount];
	}

	delete[] m_ppLog;
}

// Initialization

void CDataLogList::Load(PCBYTE &pData)
{
	ValidateLoad("CDataLogList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		m_ppLog = New CDataLog *[m_uCount];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CDataLog *pLog = NULL;

			if( GetByte(pData) ) {

				pLog = New CDataLog(n+1);

				pLog->Load(pData);

				if( pLog->m_uTags ) {

					m_uUsed++;
				}
			}

			m_ppLog[n] = pLog;
		}
	}
}

// Attibutes

BOOL CDataLogList::IsActive(void) const
{
	if( m_uUsed ) {

		for( UINT n = 0; n < m_uCount; n++ ) {

			CDataLog *pLog = m_ppLog[n];

			if( pLog ) {

				if( pLog->IsActive() ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CDataLogList::HasPolled(void) const
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDataLog *pLog = m_ppLog[n];

		if( pLog ) {

			if( pLog->IsPolled() ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

UINT CDataLogList::GetHCF(void) const
{
	UINT h = savePeriod * timeScale;

	for( UINT n = 0; n < m_uCount; n++ ) {

		CDataLog *pLog = m_ppLog[n];

		if( pLog ) {

			if( !pLog->IsPolled() ) {

				UINT u;

				u = pLog->m_Update;

				u = (u * timeScale + rateScale / 2) / rateScale;

				h = HCF(h, u);
			}
		}
	}

	return h;
}

// Operations

void CDataLogList::PreRegister(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDataLog *pLog = m_ppLog[n];

		if( pLog ) {

			pLog->PreRegister();
		}
	}
}

void CDataLogList::LogCheck(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDataLog *pLog = m_ppLog[n];

		if( pLog ) {

			pLog->LogCheck();
		}
	}
}

void CDataLogList::LogLoad(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDataLog *pLog = m_ppLog[n];

		if( pLog ) {

			pLog->LogLoad();
		}
	}
}

void CDataLogList::LogData(DWORD dwTime)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDataLog *pLog = m_ppLog[n];

		if( pLog ) {

			if( !pLog->IsPolled() ) {

				if( pLog->LogTime(dwTime) ) {

					pLog->LogData(dwTime);
				}
			}
		}
	}
}

void CDataLogList::LogPoll(DWORD dwTime)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDataLog *pLog = m_ppLog[n];

		if( pLog ) {

			if( pLog->IsPolled() ) {

				pLog->LogPoll(dwTime);
			}
		}
	}
}

void CDataLogList::LogSave(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDataLog *pLog = m_ppLog[n];

		if( pLog ) {

			pLog->LogSave();
		}
	}
}

void CDataLogList::LogNewBatch(UINT uSlot, DWORD Time, PCTXT pName)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDataLog *pLog = m_ppLog[n];

		if( pLog ) {

			pLog->LogNewBatch(uSlot, Time, pName);
		}
	}
}

void CDataLogList::LogEvent(CEventInfo const &Info)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDataLog *pLog = m_ppLog[n];

		if( pLog ) {

			if( pLog->m_Comments ) {

				pLog->LogEvent(Info);
			}
		}
	}
}

// Implementation

UINT CDataLogList::HCF(UINT a, UINT b) const
{
	while( b ) {

		UINT c = a % b;

		a = b;

		b = c;
	}

	return a;
}

//////////////////////////////////////////////////////////////////////////
//
// Data Log Item
//

// Constructor

CDataLog::CDataLog(UINT uPos)
{
	m_uPos       = uPos;

	m_pSrc       = CCommsSystem::m_pThis->m_pTags->m_pTags;

	m_Type       = 0;

	m_Update     = 600;

	m_FileLimit  = 60 * 24;

	m_FileCount  = 7;

	m_WithBatch  = FALSE;

	m_SignLogs   = FALSE;

	m_Comments   = FALSE;

	m_Merge      = FALSE;

	m_pEnable    = NULL;

	m_pTrigger   = NULL;

	m_uTags      = 0;

	m_pTags      = NULL;

	m_pType      = NULL;

	m_pData      = NULL;

	m_pTime      = NULL;

	m_pLock      = Create_Mutex();

	m_pHelp      = WhoHasFeature(rfLogToDisk) ? New CLogHelper : NULL;

	m_uRaw       = 0;

	m_pRaw       = NULL;

	m_pHead      = NULL;

	m_pTail      = NULL;

	m_fText      = FALSE;

	m_fPoll	     = FALSE;

	m_cSep	     = ',';

	m_Null.m_Ref = 0;

	m_Drive      = 0;
}

// Destructor

CDataLog::~CDataLog(void)
{
	FreeRawList();

	delete m_pEnable;
	delete m_pTrigger;

	delete[] m_pTags;
	delete[] m_pType;
	delete[] m_pData;
	delete[] m_pTime;

	if( WhoHasFeature(rfLogToDisk) ) {

		delete m_pHelp;
	}

	m_pLock->Release();
}

// Initialization

void CDataLog::Load(PCBYTE &pData)
{
	ValidateLoad("CDataLog", pData);

	m_Name      = UniConvert(GetWide(pData));

	m_Path      = UniConvert(GetWide(pData));

	m_Type      = GetByte(pData);

	m_Update    = GetLong(pData);

	m_FileLimit = GetLong(pData);

	m_FileCount = GetWord(pData);

	m_WithBatch = GetByte(pData);

	m_SignLogs  = GetByte(pData);

	m_Comments  = GetByte(pData);

	GetCoded(pData, m_pEnable);

	GetCoded(pData, m_pTrigger);

	LoadTagList(pData);

	if( GetByte(pData) ) {

		m_Merge = 1;

		LoadMergeList(pData);
	}

	m_Drive   = GetByte(pData);

	CheckPath();

	ConfigHelper();

	AllocBuffer();
}

// Attributes

BOOL CDataLog::IsPolled(void) const
{
	return m_Type == 1;
}

BOOL CDataLog::IsActive(void) const
{
	return m_uTags && IsEnabled();
}

BOOL CDataLog::IsEnabled(void) const
{
	return GetItemData(m_pEnable, C3INT(TRUE));
}

BOOL CDataLog::LogTime(DWORD dwTime) const
{
	DWORD t = dwTime;

	DWORD u = m_Update * timeScale / rateScale;

	return !(t % u);
}

// Operations

void CDataLog::PreRegister(void)
{
	if( m_uTags ) {

		// REV3 -- Take on and off scan based on enable?

		for( UINT n = 0; n < m_uTags; n++ ) {

			if( m_pType[n] ) {

				if( m_pType[n] == typeString ) {

					m_fText = TRUE;
				}

				CDataRef const &Ref = (CDataRef &) m_pTags[n];

				UINT     uTag = Ref.t.m_Index;

				CTag   * pTag = m_pSrc->GetItem(uTag);

				pTag->SetScan(Ref, scanTrue);
			}
		}

		SetItemScan(m_pEnable, scanTrue);

		SetItemScan(m_pTrigger, scanTrue);
	}
}

void CDataLog::LogCheck(void)
{
	if( m_uTags ) {

		if( WhoHasFeature(rfLogToDisk) ) {

			if( m_pHelp ) {

				m_pHelp->InitData();
			}
		}
	}
}

void CDataLog::LogLoad(void)
{
	if( m_uTags ) {

		if( WhoHasFeature(rfLogToDisk) ) {

			CFilename Path = PCTXT("\\LOGS\\" + m_Path);

			if( !chdir(Path) ) {

				MakeRawList();

				ReadRawList();

				CopyRawData();
			}
		}
	}
}

void CDataLog::LogData(DWORD dwTime)
{
	if( m_uTags ) {

		if( IsEnabled() ) {

			LockData();

			UINT uNext = (m_uTail + 1) % m_uBuff;

			UINT uPos  = m_uTail * m_uTags;

			if( m_uHead == uNext ) {

				m_uHead = (m_uHead + 1) % m_uBuff;

				m_fWrap = TRUE;
			}

			if( uNext == m_uSave ) {

				FreeStrings();

				m_uSave = (m_uSave + 1) % m_uBuff;

				m_fLost = TRUE;
			}

			for( UINT n = 0; n < m_uTags; n++ ) {

				UINT Type = m_pType[n];

				if( Type ) {

					CDataRef const &Ref = (CDataRef &) m_pTags[n];

					UINT  uTag = Ref.t.m_Index;

					CTag *pTag = m_pSrc->GetItem(uTag);

					DWORD Data = pTag->GetData(Ref, Type, getNone);

					m_pData[uPos++] = Data;

					continue;
				}

				m_pData[uPos++] = 0;
			}

			m_pTime[m_uTail] = dwTime;

			m_uTail = uNext;

			FreeData();
		}

		m_dwLast = dwTime;

		m_fValid = TRUE;
	}
}

void CDataLog::LogPoll(DWORD dwTime)
{
	if( m_uTags ) {

		BOOL fPoll = GetItemData(m_pTrigger, C3INT(TRUE));

		if( fPoll ) {

			if( !m_fPoll ) {

				LogData(dwTime);
			}
		}

		m_fPoll = fPoll;
	}
}

void CDataLog::LogSave(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( m_uTags ) {

			CAutoGuard Guard;

			if( TRUE ) {

				m_cSep = char(CCommsSystem::m_pThis->m_pLang->GetListSepChar());
			}

			if( m_pHelp->SaveInit() ) {

				CFastFile csv[2];

				CFastFile raw[2];

				if( m_SignLogs ) {

					csv[0].SignEnable();

					csv[1].SignEnable();
				}

				while( m_uSave != m_uTail ) {

					DWORD Time = m_pTime[m_uSave];

					BOOL  fCsvHead[2] = { FALSE, FALSE };

					BOOL  fRawHead[2] = { FALSE, FALSE };

					if( !m_pHelp->FindFiles(csv, fCsvHead, 0, Time) ) {

						break;
					}

					if( m_fSaveRaw ) {

						if( !m_pHelp->FindFiles(raw, fRawHead, 1, Time) ) {

							break;
						}
					}

					CComment *pScan = m_pHead;

					UINT      uScan = 0;

					while( pScan && pScan->m_Info.m_Time <= Time ) {

						uScan++;

						pScan = pScan->m_pNext;
					}

					for( UINT n = 0; n < 2; n++ ) {

						if( fCsvHead[n] ) {

							switch( CCommsSystem::m_pThis->m_pLog->m_CSVEncode ) {

								case 1:
									csv[n].AddBOM();
									break;

								default:
									break;
							}

							WriteCsvHeader(csv[n]);
						}

						if( fRawHead[n] ) {

							WriteRawHeader(raw[n]);
						}

						if( uScan ) {

							pScan = m_pHead;

							for( UINT c = 0; c < uScan; c++ ) {

								WriteComment(csv[n], pScan->m_Info);

								csv[n].EndOfLine();

								pScan = pScan->m_pNext;
							}
						}

						WriteTime(csv[n], raw[n]);

						WriteData(csv[n], raw[n], n == 1);

						csv[n].EndOfLine();
					}

					if( uScan ) {

						LockData();

						for( UINT c = 0; c < uScan; c++ ) {

							pScan = m_pHead;

							AfxListRemove(m_pHead, m_pTail, pScan, m_pNext, m_pPrev);

							delete pScan;
						}

						FreeData();
					}

					m_uSave = (m_uSave + 1) % m_uBuff;

					m_pHelp->SaveStep(Time);
				}

				m_pHelp->SaveDone();

				csv[0].Close();
				csv[1].Close();

				raw[0].Close();
				raw[1].Close();
			}
		}
	}
}

void CDataLog::LogNewBatch(UINT uSlot, DWORD Time, PCTXT pName)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( m_uTags ) {

			if( m_WithBatch == uSlot + 1 ) {

				UINT uCount = CDataLogger::m_pThis->m_BatchCount;

				m_pHelp->SetBatchInfo(uSlot, uCount);

				m_pHelp->NewBatch(Time, pName);
			}
		}
	}
}

BOOL CDataLog::LogEvent(CEventInfo const &Info)
{
	if( m_uTags ) {

		if( m_Comments ) {

			if( HasInterest(Info) ) {

				CComment *pComment = New CComment;

				pComment->m_Info   = Info;

				LockData();

				AfxListAppend(m_pHead,
					      m_pTail,
					      pComment,
					      m_pNext,
					      m_pPrev
				);

				FreeData();

				return TRUE;
			}
		}
	}

	return FALSE;
}

void CDataLog::LockData(void)
{
	m_pLock->Wait(FOREVER);
}

void CDataLog::FreeData(void)
{
	m_pLock->Free();
}

// Display Range

BOOL CDataLog::LoadDisplayData(CDataLogCache *pCache, DWORD t1, DWORD t2)
{
	UINT np = 0;

	LockData();

	if( !IsEmpty() ) {

		UINT uSlot = GetInitSlot();

		do {
			DWORD dwTime = GetSlotTime(uSlot);

			if( dwTime <= t1 ) {

				np++;

				if( dwTime < t2 ) {

					break;
				}
			}

		} while( GetNextSlot(uSlot) );
	}

	FreeData();

	return LoadDisplayData(pCache, t1, t2, np);
}

BOOL CDataLog::LoadDisplayData(CDataLogCache *pCache, DWORD t1, DWORD t2, UINT np)
{
	if( pCache ) {

		np += 2;

		if( pCache->Alloc(np) ) {

			UINT  uAlloc = np;

			PUINT pList  = (PUINT) MallocOrNull(uAlloc * sizeof(UINT));

			if( !pList ) {

				pCache->Clean();

				pCache->m_uAlloc = 0;

				return FALSE;
			}

			UINT uLive = 0;

			LockData();

			if( !IsEmpty() ) {

				UINT uSlot = GetInitSlot();

				UINT uPrev = NOTHING;

				do {
					DWORD dwTime = GetSlotTime(uSlot);

					if( dwTime <= t1 ) {

						if( !uLive ) {

							if( uPrev < NOTHING ) {

								pList[uLive++] = uPrev;
							}
						}

						if( TRUE ) {

							pList[uLive++] = uSlot;
						}

						if( dwTime < t2 ) {

							break;
						}
					}

					if( uLive >= uAlloc ) {

						break;
					}

					uPrev = uSlot;

				} while( GetNextSlot(uSlot) );
			}

			if( FALSE ) {

				UINT uRaw = uAlloc - uLive;

				if( uRaw ) {

					CFilename Path = PCTXT("\\LOGS\\" + m_Path);

					if( !chdir(Path) ) {

						FreeRawList();

						MakeRawList();

						ReadRawList();

						LoadRawData(pCache, t1, t2, uRaw);
					}
				}
			}

			while( uLive-- ) {

				UINT   uSlot = pList[uLive];

				PDWORD pTime = m_pTime + uSlot;

				PDWORD pData = m_pData + uSlot * m_uTags;

				pCache->Append(*pTime, pData);
			}

			Free(pList);

			FreeData();

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

// Data Access

DWORD CDataLog::GetTickTime(void)
{
	return m_Update * timeScale / rateScale;
}

UINT CDataLog::GetChanCount(void)
{
	return m_uTags;
}

UINT CDataLog::GetChanType(UINT uChan)
{
	return m_pType[uChan];
}

CTag * CDataLog::GetChanItem(UINT uChan)
{
	CDataRef const &Ref = (CDataRef &) m_pTags[uChan];

	UINT     uTag = Ref.t.m_Index;

	return m_pSrc->GetItem(uTag);
}

DWORD CDataLog::GetChanMin(UINT uChan)
{
	CTag * pTag  = GetChanItem(uChan);

	UINT   uType = m_pType[uChan];

	return pTag->GetMinValue(0, uType);
}

DWORD CDataLog::GetChanMax(UINT uChan)
{
	CTag * pTag  = GetChanItem(uChan);

	UINT   uType = m_pType[uChan];

	return pTag->GetMaxValue(0, uType);
}

BOOL CDataLog::HasCoverage(DWORD dwTime)
{
	return dwTime >= GetInitTime();
}

BOOL CDataLog::IsEmpty(void)
{
	return m_uTail == m_uHead;
}

DWORD CDataLog::GetInitTime(void)
{
	DWORD dwInit = 0;

	for( UINT r = 0; r < m_uRaw; r++ ) {

		CRaw const &Raw = m_pRaw[r];

		if( Raw.m_fValid ) {

			DWORD dwTime1 = Raw.m_dwTime2;

			DWORD dwTime2 = Raw.m_dwTime1;

			if( !dwInit || dwTime2 < dwInit ) {

				dwInit = dwTime2;
			}
		}
	}

	if( dwInit && dwInit < m_pTime[m_uHead] ) {

		return dwInit;
	}

	return m_pTime[m_uHead];
}

DWORD CDataLog::GetLastTime(void)
{
	return m_fValid ? m_dwLast : 0;
}

UINT CDataLog::GetInitSlot(void)
{
	return (m_uTail + m_uBuff - 1) % m_uBuff;
}

BOOL CDataLog::GetNextSlot(UINT &uSlot)
{
	if( uSlot == m_uHead ) {

		return FALSE;
	}

	uSlot = (uSlot + m_uBuff - 1) % m_uBuff;

	return TRUE;
}

DWORD CDataLog::GetSlotTime(UINT uSlot)
{
	return m_pTime[uSlot];
}

DWORD CDataLog::GetSlotData(UINT uSlot, UINT uChan)
{
	return m_pData[uSlot * m_uTags + uChan];
}

PDWORD CDataLog::GetSlotData(UINT uSlot)
{
	return m_pData + uSlot * m_uTags;
}

// Implementation

BOOL CDataLog::LoadTagList(PCBYTE &pData)
{
	if( (m_uTags = GetWord(pData)) ) {

		m_pTags = New DWORD[m_uTags];

		m_pType = New  BYTE[m_uTags];

		for( UINT n = 0; n < m_uTags; n++ ) {

			DWORD          Data = GetLong(pData);

			CDataRef const &Ref = (CDataRef &) Data;

			UINT           uTag = Ref.t.m_Index;

			CTag         * pTag = m_pSrc->GetItem(uTag);

			if( pTag ) {

				UINT uType  = pTag->GetDataType();

				m_pType[n]  = BYTE(uType);

				m_pTags[n]  = Data;

				continue;
			}

			m_pType[n] = typeVoid;

			m_pTags[n] = 0xFFFF;
		}

		return TRUE;
	}

	return FALSE;
}

void CDataLog::LoadMergeList(PCBYTE &pData)
{
	UINT uMore = GetWord(pData);

	if( uMore ) {

		for( UINT n = 0; n < uMore; n++ ) {

			DWORD          Data = GetLong(pData);

			CDataRef const &Ref = (CDataRef &) Data;

			UINT           uTag = Ref.t.m_Index;

			LoadMergeTag(uTag);
		}
	}

	if( m_uTags ) {

		for( UINT n = 0; n < m_uTags; n++ ) {

			CDataRef const &Ref = (CDataRef &) m_pTags[n];

			UINT           uTag = Ref.t.m_Index;

			LoadMergeTag(uTag);
		}
	}
}

BOOL CDataLog::LoadMergeTag(UINT uTag)
{
	if( uTag < 0xFFFF ) {

		CTag *pTag = m_pSrc->GetItem(uTag);

		if( pTag->m_Ref ) {

			CAddress Addr = pTag->GetAddress();

			// REV3 -- Ought to check it's really Honeywell...

			if( Addr.a.m_Extra == 0 ) {

				if( Addr.a.m_Table == 102 ) {

					m_MergeSigs.Insert(Addr.a.m_Offset);
				}
			}
		}

		m_MergeTags.Insert(uTag);

		return TRUE;
	}

	return FALSE;
}

BOOL CDataLog::CheckPath(void)
{
	if( m_Path.IsEmpty() ) {

		m_Path.Printf("LOG%u", m_uPos);

		return FALSE;
	}

	return TRUE;
}

BOOL CDataLog::ConfigHelper(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		m_pHelp->SetDrive(m_Drive, CDataLogger::m_pThis->m_BatchDrive);

		m_pHelp->SetBasePath(m_Path);

		m_pHelp->SetFileLimit(m_Update * m_FileLimit / rateScale);

		m_pHelp->SetFileCount(m_FileCount);

		m_pHelp->AddExtension("CSV");

		m_pHelp->AddExtension("RAW");

		m_fSaveRaw = TRUE;
	}

	return TRUE;
}

void CDataLog::AllocBuffer(void)
{
	if( m_uTags ) {

		UINT ea = CDataLogger::m_pThis->GetEachSize();

		UINT ts = sizeof(DWORD) * (m_uTags + 1);

		UINT n1 = 2  * savePeriod * rateScale / m_Update + 2;

		UINT n2 = ea / ts;

		m_uBuff = max(n1, n2);

		m_pData = New DWORD[m_uBuff * m_uTags];

		m_pTime = New DWORD[m_uBuff];

		memset(m_pData, 0, sizeof(DWORD) * m_uBuff * m_uTags);

		memset(m_pTime, 0, sizeof(DWORD) * m_uBuff);
	}

	m_uHead  = 0;

	m_uTail  = 0;

	m_uSave  = 0;

	m_fLost  = FALSE;

	m_fWrap  = FALSE;

	m_fValid = FALSE;
}

BOOL CDataLog::FreeStrings(void)
{
	if( m_fText ) {

		UINT uPos = m_uTail * m_uTags;

		for( UINT n = 0; n < m_uTags; n++ ) {

			if( m_pType[n] == typeString ) {

				DWORD Data = m_pData[uPos + n];

				Free(PUTF(Data));
			}
		}
	}

	return TRUE;
}

BOOL CDataLog::HasInterest(CEventInfo const &Info)
{
	if( m_Merge ) {

		if( Info.m_Source == 0 ) {

			UINT uTag = LOWORD(Info.m_Code);

			if( uTag == 0xFF00 ) {

				return TRUE;
			}

			if( uTag == 0xFE00 ) {

				return FALSE;
			}

			if( !m_MergeTags.Failed(m_MergeTags.Find(uTag)) ) {

				return TRUE;
			}

			return FALSE;
		}

		if( Info.m_Source == 1 ) {

			UINT uSig = LOWORD(Info.m_Code);

			if( !m_MergeSigs.Failed(m_MergeSigs.Find(uSig)) ) {

				return TRUE;
			}

			return FALSE;
		}
	}

	if( Info.m_Source == 7 ) {

		return Info.m_Type != 2;
	}

	return FALSE;
}

// Raw File List

void CDataLog::MakeRawList(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		m_uRaw = m_FileCount;

		m_pRaw = New CRaw[m_uRaw];

		memset(m_pRaw, 0, sizeof(CRaw) * m_uRaw);
	}
}

BOOL CDataLog::ReadRawList(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CAutoDirentList List;

		if( List.ScanFilesByTimeRev(".") ) {

			// NOTE : Filter RAW

			UINT r = 0;

			UINT n = 0;

			while( n < List.GetCount() && r < m_uRaw ) {

				CFilename Name = List[n]->d_name;

				if( !stricmp("raw", Name.GetType()) ) {

					CAutoFile File(PCTXT(Name), "r");

					if( File ) {

						if( TestRawHead(File) ) {

							DWORD p1 = File.Tell();

							DWORD t1 = 0;

							File.Read(PBYTE(&t1), sizeof(t1));

							////////

							INT nPitch = sizeof(DWORD) * (1 + m_uTags);

							File.SeekEnd(-1 * nPitch);

							DWORD p2 = File.Tell();

							DWORD t2 = 0;

							File.Read(PBYTE(&t2), sizeof(t2));

							////////

							m_pRaw[r].m_Name    = Name;

							m_pRaw[r].m_fValid  = TRUE;

							m_pRaw[r].m_uPitch  = nPitch;

							m_pRaw[r].m_uCount  = 1 + (p2 - p1) / nPitch;

							m_pRaw[r].m_dwPos1  = p1;

							m_pRaw[r].m_dwPos2  = p2;

							m_pRaw[r].m_dwTime1 = t1;

							m_pRaw[r].m_dwTime2 = t2;

							r++;
						}
					}
				}
				n++;
			}

			return TRUE;
		}
	}

	return FALSE;
}

void CDataLog::LoadRawData(CDataLogCache *pCache, DWORD t1, DWORD t2, UINT np)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		for( UINT r = 0; r < m_uRaw; r++ ) {

			CRaw const &Raw = m_pRaw[r];

			if( Raw.m_fValid ) {

				DWORD dwTime1 = Raw.m_dwTime2;

				DWORD dwTime2 = Raw.m_dwTime1;

				if( t2 >= dwTime2 && t2 <= dwTime1 ) {

					pCache->LoadRawData(Raw, t1, t2);

					continue;
				}

				if( t1 >= dwTime2 && t1 <= dwTime1 ) {

					pCache->LoadRawData(Raw, t1, dwTime2);

					continue;
				}

				if( dwTime2 >= t2 && dwTime1 <= t1 ) {

					pCache->LoadRawData(Raw, dwTime1, dwTime2);

					continue;
				}
			}
		}
	}
}

void CDataLog::CopyRawData(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		for( UINT r = 0; r < m_uRaw; r++ ) {

			CRaw const &Raw = m_pRaw[r];

			if( Raw.m_fValid ) {

				UINT uAlloc = (m_uTail + m_uBuff - m_uHead) % m_uBuff;

				UINT uSpace = m_uBuff - 1 - uAlloc;

				UINT uCount = min(uSpace, Raw.m_uCount);

				if( uCount ) {

					CAutoFile File(Raw.m_Name, "r");

					if( File ) {

						DWORD Pos   = Raw.m_dwPos2 - Raw.m_uPitch * (uCount - 1);

						UINT  uSlot = (m_uHead + m_uBuff - uCount) % m_uBuff;

						m_uHead     = uSlot;

						File.Seek(Pos);

						while( uCount-- ) {

							DWORD *pTime = m_pTime + uSlot;

							DWORD *pData = m_pData + uSlot * m_uTags;

							File.Read(PBYTE(pTime), sizeof(DWORD));

							File.Read(PBYTE(pData), sizeof(DWORD) * m_uTags);

							uSlot = (uSlot + 1) % m_uBuff;
						}
					}
				}
				else
					break;
			}
		}

		m_uSave  = m_uTail;

		m_dwLast = m_pTime[(m_uTail + m_uBuff - 1) % m_uBuff];

		m_fValid = TRUE;
	}
}

BOOL CDataLog::TestRawHead(FILE *pFile)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		BYTE bType[5];

		if( fread(bType, sizeof(BYTE), 4, pFile) == 4 ) {

			bType[4] = 0;

			if( !stricmp(PCTXT(bType), "RAW2") ) {

				UINT uTags = 0;

				if( fread(PBYTE(&uTags), sizeof(uTags), 1, pFile) == 1 ) {

					if( uTags == m_uTags ) {

						CAutoArray<DWORD> Tags(uTags);

						CAutoArray<BYTE>  Type(uTags);

						fread(PDWORD(Tags), sizeof(DWORD), uTags, pFile);

						fread(PBYTE(Type), sizeof(BYTE), uTags, pFile);

						if( !memcmp(Tags, m_pTags, sizeof(DWORD) * uTags) ) {

							if( !memcmp(Type, m_pType, sizeof(BYTE) * uTags) ) {

								return TRUE;
							}
						}
					}
				}
			}
		}
	}

	return FALSE;
}

void CDataLog::FreeRawList(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( m_pRaw ) {

			delete[] m_pRaw;

			m_pRaw = NULL;
		}
	}
}


// File Output

BOOL CDataLog::WriteCsvHeader(CFastFile &File)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CString Line;

		Line.Expand(1024);

		Line += "Date";

		Line += m_cSep;

		Line += "Time";

		if( m_Comments ) {

			Line += m_cSep;

			Line += "Type";
		}

		for( UINT n = 0; n < m_uTags; n++ ) {

			CString Name;

			if( m_pType[n] ) {

				CDataRef const &Ref = (CDataRef &) m_pTags[n];

				UINT     uTag = Ref.t.m_Index;

				UINT     uPos = Ref.t.m_Array;

				CTag   * pTag = m_pSrc->GetItem(uTag);

				Name          = Encode(pTag->GetLabel(uPos));
			}

			Line += m_cSep;

			Line += Name;
		}

		if( m_SignLogs ) {

			Line += m_cSep;

			Line += "Signature";
		}

		Line += "\r\n";

		File.Write(PBYTE(PCTXT(Line)), Line.GetLength());
	}

	return TRUE;
}

BOOL CDataLog::WriteRawHeader(CFastFile &File)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		PBYTE pHead = PBYTE("RAW2");

		UINT  uHead = 4;

		File.Write(pHead, uHead);

		PBYTE pData = PBYTE(&m_uTags);

		UINT  uSize = sizeof(DWORD);

		File.Write(pData, uSize);

		PBYTE pTags = PBYTE(m_pTags);

		UINT  uTags = sizeof(DWORD) * m_uTags;

		File.Write(pTags, uTags);

		PBYTE pType = PBYTE(m_pType);

		UINT  uType = sizeof(BYTE) * m_uTags;

		File.Write(pType, uType);
	}

	return TRUE;
}

void CDataLog::WriteTime(CFastFile &csv, CFastFile &raw)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( csv.IsValid() ) {

			DWORD Time = m_pTime[m_uSave];

			DWORD Secs = Time / timeScale;

			csv.Printf("%4.4u/%2.2u/%2.2u%c%2.2u:%2.2u:%2.2u",
				   GetYear(Secs),
				   GetMonth(Secs),
				   GetDate(Secs),
				   m_cSep,
				   GetHour(Secs),
				   GetMin(Secs),
				   GetSec(Secs)
			);

			if( rateScale > 1 && m_Update % rateScale ) {

				UINT uFract = (Time % timeScale) * (10 / timeScale);

				csv.Printf(".%u", uFract);
			}
		}

		if( raw.IsValid() ) {

			DWORD  Time  = m_pTime[m_uSave];

			PCBYTE pData = PCBYTE(&Time);

			UINT   uSize = sizeof(DWORD);

			raw.Write(pData, uSize);
		}
	}
}

void CDataLog::WriteData(CFastFile &csv, CFastFile &raw, BOOL fFree)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( csv.IsValid() ) {

			if( m_Comments ) {

				csv.Write(m_cSep);

				csv.Write("\"Data\"");
			}

			UINT uPos = m_uSave * m_uTags;

			for( UINT n = 0; n < m_uTags; n++ ) {

				UINT Type = m_pType[n];

				if( Type ) {

					CDataRef const &Ref = (CDataRef &) m_pTags[n];

					UINT     uTag = Ref.t.m_Index;

					CTag   * pTag = m_pSrc->GetItem(uTag);

					DWORD    Data = m_pData[uPos + n];

					CString  Text = Encode(pTag->GetAsText(0, Data, Type, fmtBare));

					if( Text.Find(m_cSep) < NOTHING ) {

						csv.Write(m_cSep);

						csv.Write('\"');

						csv.Write(Text);

						csv.Write('\"');
					}
					else {
						csv.Write(m_cSep);

						csv.Write(Text);
					}
				}
			}

			csv.Write("\r\n");
		}

		if( fFree ) {

			if( m_fText ) {

				UINT uPos = m_uSave * m_uTags;

				for( UINT n = 0; n < m_uTags; n++ ) {

					if( m_pType[n] == typeString ) {

						DWORD Data = m_pData[uPos + n];

						m_pData[uPos + n] = NULL;

						Free(PUTF(Data));
					}
				}
			}
		}

		if( raw.IsValid() ) {

			PCBYTE pData = PCBYTE(m_pData + m_uSave * m_uTags);

			UINT   uSize = sizeof(DWORD) * m_uTags;

			raw.Write(pData, uSize);
		}
	}
}

void CDataLog::WriteComment(CFastFile &csv, CEventInfo const &Info)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( csv.IsValid() ) {

			DWORD Time = Info.m_Time;

			DWORD Secs = Time / timeScale;

			BOOL  fRaw = FALSE;

			csv.Printf("%4.4u/%2.2u/%2.2u%c%2.2u:%2.2u:%2.2u",
				   GetYear(Secs),
				   GetMonth(Secs),
				   GetDate(Secs),
				   m_cSep,
				   GetHour(Secs),
				   GetMin(Secs),
				   GetSec(Secs)
			);

			if( rateScale > 1 && m_Update % rateScale ) {

				UINT uFract = (Time % timeScale) * (10 / timeScale);

				csv.Printf(".%u", uFract);
			}

			if( Info.m_Source == 7 ) {

				if( Info.m_Type == 0 ) {

					csv.Write(m_cSep);

					csv.Write("\"Comment\"");

					csv.Write(m_cSep);
				}

				if( Info.m_Type == 1 ) {

					csv.Write(m_cSep);

					csv.Write("\"Header\"");

					csv.Write(m_cSep);
				}

				if( Info.m_Type == 2 ) {

					csv.Write(m_cSep);

					csv.Write("\"Security\"");

					csv.Write(m_cSep);

					fRaw = TRUE;
				}
			}
			else {
				PCUTF p = L"Event";

				switch( Info.m_Type ) {

					case eventAlarm:  p = L"Alarm";  break;
					case eventAccept: p = L"Accept"; break;
					case eventClear:  p = L"Clear";  break;
				}

				csv.Write(m_cSep);

				csv.Write('\"');

				csv.Write(p);

				csv.Write('\"');

				csv.Write(m_cSep);
			}

			if( !fRaw ) {

				csv.Write("\"");
			}

			if( Info.m_HasText ) {

				csv.Write(Info.m_Text);
			}
			else {
				CEventManager * pEvents = CCommsSystem::m_pThis->m_pEvents;

				IEventSource  * pSource = pEvents->FindSource(Info.m_Source);

				CUnicode        Text    = L"Untitled Event";

				if( pSource ) {

					pSource->GetEventText(Text, Info.m_Code);
				}

				csv.Write(Text);
			}

			if( !fRaw ) {

				csv.Write("\"");

				csv.Write("\r\n");
			}
		}
	}
}

// CSV Encoding

CString CDataLog::Encode(CUnicode Text)
{
	switch( CCommsSystem::m_pThis->m_pLog->m_CSVEncode ) {

		case 1:
		case 2:
			return UtfConvert(Text);

		default:

			Text.Remove(uniLRM);

			Text.Remove(uniPDF);

			return UniConvert(Text);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Data Log Cache
//

// Constructor

CDataLogCache::CDataLogCache(UINT uCount)
{
	m_uCount = uCount;

	m_pData  = NULL;

	m_pTime  = NULL;

	m_uBuff  = 0;

	m_uHead  = 0;

	m_uTail  = 0;

	m_uAlloc = 0;

	m_dwLast = 0;

	m_fValid = FALSE;
}

// Destructor

CDataLogCache::~CDataLogCache(void)
{
	Clean();
}

// Management

BOOL CDataLogCache::Alloc(UINT uAlloc)
{
	uAlloc = AdjustSize(uAlloc);

	if( uAlloc != m_uAlloc ) {

		m_uAlloc = uAlloc;

		m_uBuff  = uAlloc + 2;

		Clean();

		m_pData = PDWORD(MallocOrNull(sizeof(DWORD) * m_uBuff * m_uCount));

		m_pTime = PDWORD(MallocOrNull(sizeof(DWORD) * m_uBuff));
	}

	if( m_pTime && m_pData ) {

		memset(m_pData, 0, sizeof(DWORD) * m_uBuff * m_uCount);

		memset(m_pTime, 0, sizeof(DWORD) * m_uBuff);

		m_uHead  = 0;

		m_uTail  = 0;

		m_dwLast = 0;

		m_fValid = FALSE;

		return TRUE;
	}

	m_uBuff  = 0;

	m_uAlloc = 0;

	Clean();

	return FALSE;
}

void CDataLogCache::Clean(void)
{
	if( m_pTime ) {

		Free(m_pTime);

		m_pTime = NULL;
	}

	if( m_pData ) {

		Free(m_pData);

		m_pData = NULL;
	}
}

// Operations

BOOL CDataLogCache::Append(DWORD dwTime, PDWORD pData)
{
	if( m_uCount ) {

		UINT uNext = (m_uTail + 1) % m_uBuff;

		UINT uPos  = m_uTail * m_uCount;

		BOOL fWrap = FALSE;

		if( m_uHead == uNext ) {

			m_uHead = (m_uHead + 1) % m_uBuff;

			fWrap = TRUE;
		}

		memcpy(m_pData + uPos,
		       pData,
		       sizeof(DWORD) * m_uCount
		);

		m_pTime[m_uTail] = dwTime;

		m_uTail  = uNext;

		m_fValid = TRUE;

		m_dwLast = dwTime;

		return fWrap;
	}

	return FALSE;
}

void CDataLogCache::LoadRawData(CRaw const &Raw, DWORD t1, DWORD t2)
{
	CAutoFile File(Raw.m_Name, "r");

	if( File ) {

		DWORD Pos = Raw.m_dwPos1;

		File.Seek(Pos);

		PDWORD pTime = New DWORD[1];

		PDWORD pData = New DWORD[m_uCount];

		while( Pos <= Raw.m_dwPos2 ) {

			File.Read(PBYTE(pTime), sizeof(DWORD));

			File.Read(PBYTE(pData), sizeof(DWORD) * m_uCount);

			Pos += Raw.m_uPitch;

			if( *pTime < t2 ) {

				continue;
			}

			if( *pTime > t1 ) {

				break;
			}

			Append(*pTime, pData);
		}

		delete pTime;

		delete pData;
	}
}

// Data Access

BOOL CDataLogCache::IsEmpty(void)
{
	return m_uHead == m_uTail;
}

UINT CDataLogCache::GetInitSlot(void)
{
	return (m_uTail + m_uBuff - 1) % m_uBuff;
}

BOOL CDataLogCache::GetNextSlot(UINT &uSlot)
{
	if( uSlot == m_uHead ) {

		return FALSE;
	}

	uSlot = (uSlot + m_uBuff - 1) % m_uBuff;

	return TRUE;
}

DWORD CDataLogCache::GetSlotTime(UINT uSlot)
{
	return m_pTime[uSlot];
}

DWORD CDataLogCache::GetSlotData(UINT uSlot, UINT uChan)
{
	return m_pData[uSlot * m_uCount + uChan];
}

PDWORD CDataLogCache::GetSlotData(UINT uSlot)
{
	return m_pData + uSlot * m_uCount;
}

// Implementation

UINT CDataLogCache::AdjustSize(UINT uAlloc)
{
	return ((uAlloc + 3) & ~3);
}

// Development

PCTXT CDataLogCache::FormatTime(DWORD dwTime)
{
	CDispFormatTimeDate Time;

	Time.m_Mode     = 2;

	Time.m_TimeForm = 1;

	Time.m_Secs     = 1;

	return PCTXT(UniConvert(Time.Format(dwTime/5, typeInteger, fmtStd)));
}

// End of File
