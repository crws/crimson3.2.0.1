
#include "intern.hpp"

#include "proxy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Simple Proxy Object
//

class CProxySimple : public CProxy
{
	public:
		// Constructor
		CProxySimple(void);

		// Destructor
		~CProxySimple(void);

		// Entry Points
		void Init(UINT uID);
		void Task(UINT uID);
		void Term(UINT uID);

	protected:
		// Data Members
		BOOL m_fOpen;
	};

//////////////////////////////////////////////////////////////////////////
//
// Simple Proxy Object
//

// Instantiator

CProxy * Create_ProxySimple(void)
{
	return New CProxySimple;
	}

// Constructor

CProxySimple::CProxySimple(void)
{
	m_fOpen = FALSE;
	}

// Destructor

CProxySimple::~CProxySimple(void)
{
	}

// Entry Points

void CProxySimple::Init(UINT uID)
{
	}

void CProxySimple::Task(UINT uID)
{
	m_pCommsHelper->WontReturn();

	if( m_pPort->Open(m_pComms) ) {

		m_fOpen = TRUE;

		for(;;) {
			
			m_pComms->Service();

			m_pPort->Poll();

			ForceSleep(20);
			}
		}
	
	Sleep(FOREVER);
	}

void CProxySimple::Term(UINT uID)
{
	if( m_fOpen ) {

		if( m_pDriver->GetFlags() & DF_CALL_CLOSE ) {

			m_pComms->Close();
			}

		m_pPort->Term();
		}
	}

// End of File
