
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "secure.hpp"

#include "service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Extra Helper Implementation
//

class CExtraHelper : public IExtraHelper
{
public:
	// Constructor
	CExtraHelper(void);

	// Destructor
	~CExtraHelper(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IExtraHelper
	BOOL  METHOD GetRawPort(UINT uPort, ICommsRawPort **pPort);
	UINT  METHOD GetTimeStamp(UINT &Fraction);
	UINT  METHOD FindTagIndex(PCTXT pLabel);
	PTXT  METHOD GetTagLabel(UINT uIndex);
	PTXT  METHOD FormatTagData(UINT uIndex);
	BOOL  METHOD ValidateLogon(PCTXT u, PCTXT p, DWORD r);
	DWORD METHOD GetNow(void);
	BOOL  METHOD SetNow(DWORD dwTime);
	void  METHOD ExpCatLinkMode(BOOL fSlow);
	PCTXT METHOD WhoGetName(BOOL fGeneric);
	void  METHOD SetGPSTime(DWORD Time);
	PBYTE METHOD Png2RLCBmp(PBYTE pData, UINT uBytes);
	PBYTE METHOD Jpeg2RLCBmp(PBYTE pData, UINT uBytes, UINT uScale);
	PBYTE METHOD WinBmp2RLCBmp(PBYTE pData, UINT uBytes);
	BOOL  METHOD WriteFile(PCTXT pName, PBYTE pData, UINT  uCount);
	BOOL  METHOD ReadFile(PCTXT pName, PBYTE pData, UINT &uCount);
	void  METHOD GetCodedItem(PCBYTE &pData, PVOID &pItem);
	DWORD METHOD ExecuteCoded(PVOID pItem, UINT uType);
	DWORD METHOD GetDnsAddress(PCTXT pName, DWORD Default);
	BOOL  METHOD GetMacId(UINT uPort, PBYTE pMac);
	BOOL  METHOD IsBroadcast(DWORD Ip);
	BOOL  METHOD GetBroadcast(DWORD Ip, DWORD &Broadcast);
	BOOL  METHOD EnumBroadcast(UINT &uFace, DWORD &Broadcast);

protected:
	// Data Members
	ULONG		m_uRefs;
	INetUtilities * m_pUtils;
};

//////////////////////////////////////////////////////////////////////////
//
// Extra Helper Implementation
//

// Instantiator

IExtraHelper * Create_ExtraHelper(void)
{
	return New CExtraHelper;
}

// Constructor

CExtraHelper::CExtraHelper(void)
{
	m_pUtils = NULL;

	AfxGetObject("ip", 0, INetUtilities, m_pUtils);

	StdSetRef();
}

// Destructor

CExtraHelper::~CExtraHelper(void)
{
	AfxRelease(m_pUtils);
}

// IUnknown

HRESULT CExtraHelper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IExtraHelper);

	StdQueryInterface(IExtraHelper);

	return E_NOINTERFACE;
}

ULONG CExtraHelper::AddRef(void)
{
	StdAddRef();
}

ULONG CExtraHelper::Release(void)
{
	StdRelease();
}

// IExtraHelper

BOOL METHOD CExtraHelper::GetRawPort(UINT uPort, ICommsRawPort **pPort)
{
	*pPort = CCommsSystem::m_pThis->m_pComms->FindRawPort(LOBYTE(uPort));

	return *pPort ? TRUE : FALSE;
}

UINT METHOD CExtraHelper::GetTimeStamp(UINT &Fraction)
{
//	AfxRaiseIRQL(IRQL_TIMER);	!!!

	DWORD t1 = GetNow();

	DWORD t2 = GetNowFraction();

//	AfxLowerIRQL();			!!!

	Fraction = t2;

	return t1;
}

UINT METHOD CExtraHelper::FindTagIndex(PCTXT pName)
{
	CTagList *pList = CCommsSystem::m_pThis->m_pTags->m_pTags;

	return pList->FindByName(UniConvert(pName));
}

PTXT METHOD CExtraHelper::GetTagLabel(UINT uIndex)
{
	CTagList *pList = CCommsSystem::m_pThis->m_pTags->m_pTags;

	CTag     *pTag  = pList->GetItem(uIndex);

	if( pTag ) {

		if( !pTag->m_Name.IsEmpty() ) {

			return strdup(UniConvert(pTag->m_Name));
		}
	}

	return NULL;
}

PTXT METHOD CExtraHelper::FormatTagData(UINT uIndex)
{
	CTagList *pList = CCommsSystem::m_pThis->m_pTags->m_pTags;

	CTag     *pTag  = pList->GetItem(uIndex);

	if( pTag ) {

		CDataRef Ref;

		Ref.m_Ref = 0;

		BOOL fReg = TRUE;

		SetTimer(2500);

		while( GetTimer() ) {

			if( pTag->IsAvail(Ref) ) {

				CUnicode Text = pTag->GetAsText(0, getNone);

				return Text.IsEmpty() ? NULL : strdup(UniConvert(Text));
			}

			if( fReg ) {

				pTag->SetScan(Ref, scanOnce);

				fReg = FALSE;
			}

			Sleep(50);
		}
	}

	return NULL;
}

BOOL METHOD CExtraHelper::ValidateLogon(PCTXT pUser, PCTXT pPass, DWORD dwAccess)
{
	return CCommsSystem::m_pThis->m_pSecure->ValidateLogon(pUser, pPass, dwAccess);
}

DWORD METHOD CExtraHelper::GetNow(void)
{
	return ::GetNow();
}

BOOL METHOD CExtraHelper::SetNow(DWORD dwTime)
{
	CTime Time;

	Time.uSeconds = GetSec(dwTime);
	Time.uMinutes = GetMin(dwTime);
	Time.uHours   = GetHour(dwTime);
	Time.uDate    = GetDate(dwTime);
	Time.uMonth   = GetMonth(dwTime);
	Time.uYear    = GetYear(dwTime);

	return SetTime(Time);
}

void METHOD CExtraHelper::ExpCatLinkMode(BOOL fSlow)
{
}

PCTXT METHOD CExtraHelper::WhoGetName(BOOL fGeneric)
{
	return ::WhoGetName(fGeneric);
}

void METHOD CExtraHelper::SetGPSTime(DWORD Time)
{
	/*if( g_pServiceTimeSync ) {

		g_pServiceTimeSync->SetGPSTime(Time);
		}*/
}

PBYTE METHOD CExtraHelper::Png2RLCBmp(PBYTE pData, UINT uBytes)
{
/*!!!	if( pData ) {

		UINT IHDR   = 0x49484452;

		UINT x	    = 0;

		UINT y      = 0;

		UINT bDepth = 0;

		UINT bColor = 0;

		UINT uSize  = 0;

		UINT uMult  = 0;

		for( UINT u = 0; u < uBytes; u++ ) {

			DWORD dwType = MotorToHost(PU4(pData + u)[0]);

			if( dwType == IHDR ) {

				u += 4;

				x = MotorToHost(PU4(pData + u)[0]);

				u += 4;

				y = MotorToHost(PU4(pData + u)[0]);

				u += 4;

				bDepth  = pData[u++];

				bColor  = pData[u++];

				uMult   = bDepth > 8 ? 3 : 1;

				uSize   = x * y * uMult;

				break;
				}
			}

		if( uSize > 0 ) {

			PBYTE pWork = New BYTE [ sizeof(BITMAP_RLC) + uSize ];

			BITMAP_RLC *pBitmap = (BITMAP_RLC *) pWork;

			pBitmap->Format = bDepth > 8 ? image24bitColor : image8bitGreyScale;

			pBitmap->Frame  = MotorToHost(PU4(pData + uBytes - 16)[0]);

			pBitmap->Height = y;

			pBitmap->Stride = x * uMult;

			pBitmap->Width = x;

			if( Png2Bmp(pData, uSize, pBitmap->Data) ) {

				return pWork;
				}

			delete pWork;
			}
		}

!!!*/	return NULL;
}

PBYTE METHOD CExtraHelper::Jpeg2RLCBmp(PBYTE pData, UINT uBytes, UINT uScale)
{
/*!!!	PBYTE pWork = NULL;

	if( !Jpeg2Bmp(pData, uBytes, pWork, uScale) ) {

		return NULL;
		}

	if( pWork ) {

		PBYTE pImg = WinBmp2RLCBmp(pWork, uBytes);

		delete [] pWork;

		return pImg;
		}

!!!*/	return NULL;
}

PBYTE METHOD CExtraHelper::WinBmp2RLCBmp(PBYTE pWork, UINT uBytes)
{
/*!!!	if( pWork[0] != 'B' || pWork[1] != 'M' ) {

		return FALSE;
		}

	UINT uSize = sizeof(BITMAP_RLC) + uBytes - 54;

	WORD w = HostToIntel(PU2(pWork + 0x12)[0]);

	WORD h = HostToIntel(PU2(pWork + 0x16)[0]);

	WORD b = HostToIntel(PU2(pWork + 0x1C)[0]);

	if( b == 8 ) {

		uSize = uSize * 3;
		}

	PBYTE pImg = New BYTE[ uSize ];

	BITMAP_RLC *pBitmap = (BITMAP_RLC *) pImg;

	pBitmap->Format = image24bitColor;

	pBitmap->Frame  = 0;

	pBitmap->Height = h;

	UINT s = 3;

	pBitmap->Stride = s * w;

	pBitmap->Width  = w;

	PDWORD pPalette = NULL;

	if( b == 8 ) {

		pPalette = New DWORD[ 256 ];

		memset(pPalette, 0, sizeof(DWORD) * 256);

		for( UINT n = 0; n < 256; n++ ) {

			pPalette[n] = IntelToHost(PU4(pWork + 54)[n]);
			}
		}

	if( !((w * s) & 3) && b == 24 ) {

		memcpy(pBitmap->Data, pWork + 54, uBytes - 54);
		}
	else {
		PBYTE pSrc = pWork + 54;

		PBYTE pDst = PBYTE(pBitmap->Data);

		if( pPalette ) {

			pSrc += 1024;
			}

		UINT uStride = w;

		if( b > 8 ) {

			uStride = uStride * 3;
			}

		UINT uRow = ((uStride + 3) & ~3);

		for( UINT y = 0; y < h; y++ ) {

			for( UINT x = 0; x < uRow; x++ ) {

				if( x >= uStride ) {

					// Skip the pad bytes

					pSrc++;
					}
				else {
					if( !pPalette ) {

						*pDst++ = *pSrc++;
						}
					else {
						DWORD dwColor = pPalette[*pSrc++];

						BYTE red   = (dwColor & 0x000000FF) >>  0;
						BYTE green = (dwColor & 0x0000FF00) >>  8;
						BYTE blue  = (dwColor & 0x00FF0000) >> 16;

						*pDst++ = red;
						*pDst++ = green;
						*pDst++ = blue;
						}
					}
				}
			}
		}

	if( pPalette ) {

		delete [] pPalette;
		}

	return pImg;

!!!*/	return NULL;
}

BOOL METHOD CExtraHelper::WriteFile(PCTXT pName, PBYTE pData, UINT uCount)
{
	CAutoFile File(pName, "r+", "w+");

	if( File ) {

		UINT uDone = File.Write(PBYTE(pData), uCount);

		File.Truncate();

		return uDone == uCount;
	}

	return FALSE;
}

BOOL MCALL CExtraHelper::ReadFile(PCTXT pName, PBYTE pData, UINT &uCount)
{
	CAutoFile File(pName, "r");

	if( File ) {

		uCount = File.Read(pData, uCount);

		return uCount ? TRUE : FALSE;
	}

	return FALSE;
}

void METHOD CExtraHelper::GetCodedItem(PCBYTE &pData, PVOID &pItem)
{
	CCodedItem * pCoded = New CCodedItem;

	if( pCoded ) {

		pItem = pCoded;

		pCoded->Load(pData);
	}
}

DWORD METHOD CExtraHelper::ExecuteCoded(PVOID pItem, UINT uType)
{
	CCodedItem * pCoded = (CCodedItem *) pItem;

	if( pCoded ) {

		return pCoded->Execute(uType);
	}

	return 0;
}

DWORD METHOD CExtraHelper::GetDnsAddress(PCTXT pName, DWORD Default)
{
	if( pName ) {

		if( WhoHasFeature(rfDnsApi) ) {

			AfxGetAutoObject(pDns, "net.dns", 0, IDnsResolver);

			if( pDns ) {

				CIPAddr ip = pDns->Resolve(pName);

				if( !ip.IsEmpty() ) {

					return MotorToHost((DWORD &) ip);
				}
			}
		}
	}

	return Default;
}

// IExtraHelper - MAC Access

BOOL METHOD CExtraHelper::GetMacId(UINT uPort, PBYTE pMac)
{
	CMACAddr Addr;

	if( NICReadMac(uPort, Addr) ) {

		memcpy(pMac, Addr.m_Addr, sizeof(Addr.m_Addr));

		return TRUE;
	}

	return FALSE;
}

BOOL METHOD CExtraHelper::IsBroadcast(DWORD Ip)
{
	return m_pUtils ? m_pUtils->IsBroadcast(IPREF(Ip)) : FALSE;
}

BOOL METHOD CExtraHelper::GetBroadcast(DWORD Ip, DWORD &Broad)
{
	return m_pUtils ? m_pUtils->GetBroadcast(IPREF(Ip), (CIPAddr &) Broad) : FALSE;
}

BOOL METHOD CExtraHelper::EnumBroadcast(UINT &uFace, DWORD &Broad)
{
	return m_pUtils ? m_pUtils->EnumBroadcast(uFace, (CIPAddr &) Broad) : FALSE;
}

// End of File
