
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "SqlClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Statement
//

// Constructor

CSqlStatement::CSqlStatement(ISqlConnection *pConnection)
{
	m_pConnection = pConnection;
	}

// Query Execution

BOOL CSqlStatement::Exec(PCTXT pQuery)
{
	return m_pConnection->ExecuteQuery(pQuery);
	}

BOOL CSqlStatement::ExecRead(PCTXT pQuery, CResultSet &Results)
{
	return m_pConnection->ExecuteReadQuery(pQuery, Results);
	}

BOOL CSqlStatement::ExecWrite(PCTXT pQuery)
{
	// LATER -- Implement writing

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// SQL Data Object
//

// Constructor

CSqlData::CSqlData(void)
{
	m_pData = NULL;

	m_uSize = 0;
	}

CSqlData::CSqlData(CSqlData const &That)
{
	m_pData = New BYTE[ That.m_uSize ];

	m_uSize = That.m_uSize;

	memcpy(m_pData, That.m_pData, That.m_uSize);
	}

// Destructor

CSqlData::~CSqlData(void)
{
	delete [] m_pData;
	}

// Data Access

PCBYTE CSqlData::GetData(void) const
{
	return m_pData;
	}

UINT   CSqlData::GetSize(void) const
{
	return m_uSize;
	}

void CSqlData::SetData(PCBYTE pSource, UINT uSize)
{
	if( m_pData ) {

		delete [] m_pData;

		m_pData = NULL;
		}

	if( pSource ) {

		m_pData = New BYTE[ uSize ];

		m_uSize = uSize;

		memcpy(m_pData, pSource, uSize);
		}
	}

// Operators

CSqlData & CSqlData::operator = (CSqlData const &That)
{
	SetData(That.m_pData, That.m_uSize);

	return *this;
	}

//////////////////////////////////////////////////////////////////////////
//
// SQL Row
//

// Constructors

CSqlRow::CSqlRow(void)
{
	}

// Destructor

CSqlRow::~CSqlRow(void)
{
	}

// Data Access

CSqlData const * CSqlRow::GetData(UINT uCol) const
{
	if( uCol < m_RowData.GetCount() ) {

		return &m_RowData.GetAt(uCol);
		}

	return NULL;
	}

UINT CSqlRow::GetCount(void) const
{
	return m_RowData.GetCount();
	}

void CSqlRow::AddData(CSqlData &Data)
{
	m_RowData.Append(Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// SQL Column
//

// Constructors

CSqlColumn::CSqlColumn(void)
{
	m_uSqlType = sqlTypeBinary;

	m_uSize    = 0;
	}

CSqlColumn::CSqlColumn(PCTXT pName, UINT uType, UINT uSize)
{
	m_Name     = pName;

	m_uSqlType = uType;

	m_uSize    = uSize;
	}

// Destructor

CSqlColumn::~CSqlColumn(void)
{
	}

// Attributes

PCTXT CSqlColumn::GetName(void) const
{
	return PCTXT(m_Name);
	}

UINT CSqlColumn::GetType(void) const
{
	return m_uSqlType;
	}

//////////////////////////////////////////////////////////////////////////
//
// SQL Result Set
//

// Constructors

CResultSet::CResultSet(void)
{
	}

// Destructor

CResultSet::~CResultSet(void)
{
	}

// Attributes

UINT CResultSet::GetRowCount(void) const
{
	return m_Rows.GetCount();
	}

UINT CResultSet::GetColumnCount(void) const
{
	return m_Cols.GetCount();
	}

UINT CResultSet::GetIntegerField(UINT uRow, UINT uCol) const
{
	CSqlRow  Row  = m_Rows.GetAt(uRow);

	CSqlData const *pData = Row.GetData(uCol);

	if( pData ) {

		PCBYTE p =  pData->GetData();

		switch( pData->GetSize() ) {

			case 1:
				return *p;

			case 2:
				return *(WORD *) p;

			case 4:
				return *(DWORD *) p;

			case 8:
				{
					// LATER -- True 64-bit support?

					INT64 iLong = *(INT64 *) p;

					return (UINT) iLong;
				}
			}
		}

	return 0;
	}

INT64 CResultSet::GetInt64Field(UINT uRow, UINT uCol) const
{
	CSqlRow  Row  = m_Rows.GetAt(uRow);

	CSqlData const *pData = Row.GetData(uCol);

	if( pData ) {

		PCBYTE p = pData->GetData();

		return *(INT64 *) p;
		}

	return 0;
	}

double CResultSet::GetFloatField(UINT uRow, UINT uCol) const
{
	CSqlRow  Row  = m_Rows.GetAt(uRow);

	CSqlData const *pData = Row.GetData(uCol);

	if( pData ) {

		PCBYTE p = pData->GetData();

		switch( pData->GetSize() ) {

			case 4:
				return *(float  *) p;

			case 8:
				return *(double *) p;
			}
		}

	return 0.0;
	}

PCUTF CResultSet::GetTextField(UINT uRow, UINT uCol) const
{
	CSqlRow  Row  = m_Rows.GetAt(uRow);

	CSqlData const *pData = Row.GetData(uCol);

	if( pData ) {

		PCTXT p = (PCTXT) pData->GetData();

		WORD wLen = strlen(p) + 1;

		PUTF  pCopy = PUTF (Malloc(wLen * sizeof(WCHAR)));

		for( UINT i = 0; i < wLen-1; i++ ) {

			pCopy[i] = WCHAR(p[i]);
			}

		pCopy[wLen - 1] = '\0';

		return pCopy;
		}

	return wstrdup(L"");
	}

PCUTF CResultSet::GetWideTextField(UINT uRow, UINT uCol) const
{
	CSqlRow  Row  = m_Rows.GetAt(uRow);

	CSqlData const *pData = Row.GetData(uCol);

	if( pData ) {

		PCUTF p = (PCUTF) pData->GetData();

		WORD wLen = wstrlen(p) + 1;
		
		PUTF  pCopy = PUTF (Malloc(wLen * sizeof(WCHAR)));

		for( UINT i = 0; i < wLen; i++ ) {

			pCopy[i] = p[i];
			}

		return pCopy;
		}

	return NULL;
	}

CSqlRow const * CResultSet::GetRow(UINT uPos)
{
	if( uPos < m_Rows.GetCount() ) {

		return &m_Rows[uPos];
		}

	return NULL;
	}

CSqlColumn const * CResultSet::GetColumn(UINT uPos) const
{
	if( uPos < m_Cols.GetCount() ) {

		return &m_Cols[uPos];
		}

	return NULL;
	}

// Result Set Building

void CResultSet::AddColumn(CSqlColumn const &Column)
{
	m_Cols.Append(Column);
	}

void CResultSet::AddRow(CSqlRow const &Row)
{
	m_Rows.Append(Row);
	}

// End of File
