
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "SqlClient.hpp"

#ifndef INCLUDE_MS_SQL_SERVER

#define INCLUDE_MS_SQL_SERVER

//////////////////////////////////////////////////////////////////////////
//
// Forware Declarations
//

class CTdsPacket;
class CTdsSQLBatch;
class CTdsResponse;
class CTdsBulkLoad;
class CTdsBytes;
class CTdsDataSet;

//////////////////////////////////////////////////////////////////////////
//
// MS SQL Server Connection Implmentation
//

class CMsSqlSeverConnection : public ISqlConnection
{
	public:
		// Constructor
		CMsSqlSeverConnection(void);

		// Destructor
		~CMsSqlSeverConnection(void);

		// Operations
		BOOL Open(IPADDR IP, WORD wPort, UINT uTls, PCTXT pUser, PCTXT pPass, PCTXT pDatabase);
		void Close(void);
		BOOL IsOpen(void);
		void SetInstance(PCTXT pInstance);

		CSqlStatement * CreateStatement(void);

		// Statement Helpers
		BOOL ExecuteQuery(PCTXT pQuery);
		BOOL ExecuteReadQuery(PCTXT pQuery, CResultSet &Results);

	protected:
		// Data Members
		UINT			m_uMaxPacketSize;
		IMatrixClientContext  *	m_pTls;
		ISocket		      * m_pSock;
		IPADDR			m_IP;
		WORD			m_wPort;
		UINT			m_uTlsMode;
		PCTXT			m_pUser;
		PCTXT			m_pPass;
		PCTXT			m_pDatabase;
		PCTXT			m_pInstance;
		BOOL			m_fOpen;

		// Implementation
		void PopulateResultSet(CTdsDataSet &DataSet, CResultSet &ResultSet);
		UINT FindInstancePort(PCTXT pInstance);
		UINT ParseInstancePort(PCTXT pInstance, PTXT pReply);
		UINT TdsToSqlType(UINT uType);

		// TDS Communication Helpers
		BOOL SendSQLBatchCommandsAndGetReply(CTdsPacket& ReceivedPacket, PCSTR pCommand);
		BOOL SendCommand(CTdsPacket& SentPacket);
		BOOL SendCommandAndGetReply(CTdsPacket& SentPacket, CTdsPacket& ReceivedPacket);
		BOOL SendBulkData(CTdsBulkLoad& BulkData, CTdsPacket& Response, BOOL fFinal);
		CTdsBytes * MakeCommandBytes(CTdsBytes& Command, PCSTR pCommand);

		// Database Actions
		BOOL ServerLogin(void);
		bool SwitchToTls(void);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		BOOL OpenSocket(BOOL fTCP, WORD wPort);
		void CloseSocket(BOOL fAbort);
		BOOL CreateTlsContext(void);
	};

#endif

// End of File
