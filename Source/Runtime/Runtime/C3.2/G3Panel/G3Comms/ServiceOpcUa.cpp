
#include "intern.hpp"

#include "ServiceOpcUa.hpp"

#include "OpcIds.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Service
//

// Instantiator

IService * Create_ServiceOpcUa(void)
{
	return New CServiceOpcUa;
}

// Constructor

CServiceOpcUa::CServiceOpcUa(void)
{
	StdSetRef();

	m_pSrc        = CCommsSystem::m_pThis->m_pTags->m_pTags;
	m_uDebug      = 0;
	m_pEnable     = NULL;
	m_pServer     = NULL;
	m_pEndpoint   = NULL;
	m_pEnable     = NULL;
	m_uPort       = 4840;
	m_uLayout     = 0;
	m_uTree       = 1;
	m_uArray      = 0;
	m_uProps      = NOTHING;
	m_fHistEnable = FALSE;
	m_uHistSample = 1;
	m_uHistQuota  = 50;
	m_uHistTime   = 65;
	m_uAnon       = 1;
	m_uWrite      = 0;
	m_uTags       = 0;
	m_pOpcUa      = NULL;
	m_pTags       = NULL;
	m_pInfo       = NULL;
	m_SessActive  = 0;
	m_SessAuth    = 0;
}

// Destructor

CServiceOpcUa::~CServiceOpcUa(void)
{
	delete m_pEnable;

	delete m_pServer;

	delete m_pEndpoint;

	delete[] m_pTags;

	delete[] m_pInfo;
}

// Initialization

void CServiceOpcUa::Load(PCBYTE &pData)
{
	ValidateLoad("CServiceOpcUa", pData);

	m_uDebug = GetByte(pData);

	GetCoded(pData, m_pEnable);

	GetCoded(pData, m_pServer);

	GetCoded(pData, m_pEndpoint);

	m_uPort       = GetWord(pData);

	m_uLayout     = GetByte(pData);

	m_uTree       = GetByte(pData);

	m_uArray      = GetByte(pData);

	m_uProps      = GetLong(pData);

	m_uPubSub     = GetByte(pData);

	m_fHistEnable = GetByte(pData);

	m_uHistSample = GetWord(pData);

	m_uHistQuota  = GetByte(pData);

	m_uHistTime   = GetByte(pData);

	m_uAnon       = GetByte(pData);

	GetCoded(pData, m_User);

	GetCoded(pData, m_Pass);

	m_uWrite      = GetByte(pData);

	LoadTagList(pData);
}

// IUnknown

HRESULT CServiceOpcUa::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IService);

	StdQueryInterface(IService);

	StdQueryInterface(IOpcUaServerHost);

	return E_NOINTERFACE;
}

ULONG CServiceOpcUa::AddRef(void)
{
	StdAddRef();
}

ULONG CServiceOpcUa::Release(void)
{
	StdRelease();
}

// IService

UINT CServiceOpcUa::GetID(void)
{
	return 10;
}

void CServiceOpcUa::GetTaskList(CTaskList &List)
{
	if( GetItemData(m_pEnable, C3INT(0)) ) {

		m_Server   = UtfConvert(GetItemData(m_pServer, L""));

		m_Endpoint = UtfConvert(GetItemData(m_pEndpoint, L""));

		CTaskDef Task;

		Task.m_Name   = "OPCUA";
		Task.m_pEntry = this;
		Task.m_uID    = 0;
		Task.m_uCount = 1;
		Task.m_uLevel = 2265;
		Task.m_uStack = 0;

		List.Append(Task);
	}
}

// ITaskEntry

void CServiceOpcUa::TaskInit(UINT uID)
{
	InitTagList();

	AfxNewObject("opcua-server", IOpcUaServer, m_pOpcUa);

	m_fInit = FALSE;
}

void CServiceOpcUa::TaskExec(UINT uID)
{
	FixCoded(m_User, FALSE);

	FixCoded(m_Pass, FALSE);

	BYTE bGuid[16];

	GetConfigGuid(bGuid);

	COpcUaServerConfig Config;

	Config.m_uSize   = sizeof(Config);
	Config.m_uDebug  = m_uDebug;
	Config.m_uSample = m_uHistSample;
	Config.m_uQuota  = m_uHistQuota;
	Config.m_uTime   = m_uHistTime;
	Config.m_pGuid   = bGuid;

	CString Name = m_Endpoint;

	if( Name.IsEmpty() ) {

		g_pPxe->GetHostName(Name);
	}

	for( ;;) {

		// TODO -- We should support multiple endpoints!

		if( Name.IsEmpty() ) {

			AfxGetAutoObject(pUtils, "ip", 0, INetUtilities);

			UINT c = pUtils->GetInterfaceCount();

			for( UINT n = 0; n < c; n++ ) {

				if( pUtils->IsInterfaceUp(n) ) {

					CIpAddr Addr;

					if( pUtils->GetInterfaceAddr(n, Addr) ) {

						if( !Addr.IsLoopback() ) {

							Name = Addr.GetAsText();

							break;
						}
					}
				}
			}
		}

		if( !Name.IsEmpty() ) {

			if( m_Server.IsEmpty() ) {

				m_Server = Name;
			}

			if( g_pPxe->GetDefCertStep() ) {

				CString    Pass;

				CByteArray Data[2];

				if( g_pPxe->GetDefCertData(Data, Pass) ) {

					Config.m_Cert = Data[0];
					Config.m_Priv = Data[1];
					Config.m_Pass = Pass;

					m_pOpcUa->OpcLoad(Config);

					CPrintf ep("opc.tcp://%s:%u", PCTXT(Name), m_uPort);

					AfxTrace("opc: server   is %s\n", PCTXT(m_Server));

					AfxTrace("opc: endpoint is %s\n", PCTXT(ep));

					CAutoGuard Guard;

					if( m_pOpcUa->OpcInit(this, ep) ) {

						m_fInit = TRUE;

						Guard.Free();

						if( m_pOpcUa->OpcExec() ) {

							for( ;;) Sleep(FOREVER);
						}
					}

					return;
				}
			}
		}

		Sleep(200);
	}
}

void CServiceOpcUa::TaskStop(UINT uID)
{
	if( m_fInit ) {

		m_pOpcUa->OpcStop();
	}
}

void CServiceOpcUa::TaskTerm(UINT uID)
{
	if( m_fInit ) {

		m_pOpcUa->OpcTerm();
	}

	m_pOpcUa->Release();
}

// IOpcUaServerHost

bool CServiceOpcUa::LoadModel(IOpcUaDataModel *pModel)
{
	if( m_uLayout == 0 ) {

		pModel->AddObject(1, 1, 0, OpcUaId_FolderType, "V1");

		pModel->AddOrganizes(0, OpcUaId_ObjectsFolder, 1, 1);

		if( m_uTags ) {

			pModel->AddObject(1, 2, 0, OpcUaId_FolderType, "Arrays");

			pModel->AddOrganizes(1, 1, 1, 2);

			pModel->AddVariable(1, 3, 0, OpcUaId_BaseDataVariableType, "Integer", 0, OpcUaId_Int32, 1, m_uTags, OpcUa_AccessLevels_CurrentReadOrWrite, false);
			pModel->AddVariable(1, 4, 0, OpcUaId_BaseDataVariableType, "Double", 0, OpcUaId_Double, 1, m_uTags, OpcUa_AccessLevels_CurrentReadOrWrite, false);
			pModel->AddVariable(1, 5, 0, OpcUaId_BaseDataVariableType, "String", 0, OpcUaId_String, 1, m_uTags, OpcUa_AccessLevels_CurrentReadOrWrite, false);

			pModel->AddComponent(1, 2, 1, 3);
			pModel->AddComponent(1, 2, 1, 4);
			pModel->AddComponent(1, 2, 1, 5);

			pModel->AddObject(1, 6, 0, OpcUaId_FolderType, "Tags");

			pModel->AddOrganizes(1, 1, 1, 6);
		}
	}

	if( m_uTags ) {

		CMap <CString, UINT> Map;

		UINT next = 10;

		for( UINT n = 0; n < m_uTags; n++ ) {

			CTagInfo &Info = m_pInfo[n];

			UINT      Type = GetOpcType(Info.m_Type);

			if( Type ) {

				UINT    id   = Info.m_id;

				UINT    acc  = OpcUa_AccessLevels_CurrentRead;

				CString Name = UtfConvert(Info.m_pTag->m_Name);

				if( Info.m_pTag->IsArray() ) {

					UINT i = Info.m_pTag->GetExtent();

					UINT d = 1;

					while( i >= 10 ) {

						i /= 10;

						d += 1;
					}

					if( m_uArray == 0 ) {

						Name.AppendPrintf("[%*.*u]", d, d, Info.m_Ref.t.m_Array);
					}
					else
						Name.AppendPrintf(".%*.*u", d, d, Info.m_Ref.t.m_Array);
				}

				UINT nsRoot = (m_uLayout == 0) ? 1 : 0;

				UINT idRoot = (m_uLayout == 0) ? 6 : OpcUaId_ObjectsFolder;

				if( m_uTree ) {

					UINT dot = Name.FindRev('.');

					if( dot < NOTHING ) {

						CString Path = Name.Left(dot);

						if( m_uTree == 1 ) {

							Name = Name.Mid(dot + 1);
						}

						CStringArray List;

						Path.Tokenize(List, '.');

						CString Walk;

						UINT    nsWalk = nsRoot;

						UINT    idWalk = idRoot;

						for( UINT n = 0; n < List.GetCount(); n++ ) {

							if( n ) {

								Walk += '.';
							}

							Walk   += List[n];

							INDEX i = Map.FindName(Walk);

							if( Map.Failed(i) ) {

								pModel->AddObject(1, next, 0, OpcUaId_FolderType, List[n]);

								pModel->AddOrganizes(nsWalk, idWalk, 1, next);

								Map.Insert(Walk, next);

								idWalk = next++;

								nsWalk = 1;
							}
							else {
								idWalk = Map.GetData(i);

								nsWalk = 1;
							}
						}

						nsRoot = nsWalk;

						idRoot = idWalk;
					}
				}

				if( m_uWrite && Info.m_pTag->CanWrite() ) {

					acc = OpcUa_AccessLevels_CurrentReadOrWrite;
				}

				if( m_fHistEnable ) {

					acc |= OpcUa_AccessLevels_HistoryRead;
				}

				if( n >= m_uSet1 ) {

					acc |= 0x0100;
				}

				pModel->AddVariable(2,
						    id,
						    0,
						    OpcUaId_BaseDataVariableType,
						    Name,
						    0,
						    Type,
						    0,
						    0,
						    acc,
						    m_fHistEnable ? true : false
				);

				// TODO -- Add the historicizing stuff!!!!

				if( m_uProps ) {

					UINT c = CTag::GetPropCount();

					for( UINT p = 1; p <= c; p++ ) {

						if( m_uProps & (1 << (p-1)) ) {

							CString Name = CTag::GetPropName(WORD(p));

							UINT    Type = GetOpcType(CTag::GetPropType(WORD(p), Info.m_Type));

							pModel->AddVariable(3,
									    id * 100 + p,
									    0,
									    OpcUaId_BaseDataVariableType,
									    Name,
									    0,
									    Type,
									    0,
									    0,
									    OpcUa_AccessLevels_CurrentRead,
									    false
							);

							pModel->AddProperty(2, id, 3, id * 100 + p);
						}
					}

					if( m_uProps & 0x80000000 ) {

						if( Info.m_pTag->m_pFormat && Info.m_pTag->m_pFormat->IsMulti() ) {

							UINT s = Info.m_pTag->GetProp(Info.m_Ref, tpStateCount, typeInteger);

							if( s ) {

								pModel->AddVariable(3,
										    id * 100 + 50,
										    0,
										    OpcUaId_BaseDataVariableType,
										    "StateText",
										    0,
										    OpcUaId_String,
										    1,
										    s,
										    OpcUa_AccessLevels_CurrentRead,
										    false
								);

								pModel->AddProperty(2, id, 3, id * 100 + 50);
							}
						}
					}
				}

				pModel->AddComponent(nsRoot, idRoot, 2, id);
			}
		}
	}

	return true;
}

bool CServiceOpcUa::OpenSession(UINT si, CString const &User, CString const &Pass)
{
	if( si < 32 ) {

		DWORD m = (1<<si);

		if( User.IsEmpty() ) {

			if( m_uAnon ) {

				m_SessActive |=  m;

				m_SessAuth   &= ~m;

				return true;
			}
		}
		else {
			if( !strcasecmp(User, m_User) && !strcmp(Pass, m_Pass) ) {

				m_SessActive |= m;

				m_SessAuth   |= m;

				return true;
			}
		}
	}

	return false;
}

void CServiceOpcUa::CloseSession(UINT si)
{
	if( si < 32 ) {

		DWORD m = (1<<si);

		m_SessActive &= ~m;

		m_SessAuth   &= ~m;
	}
}

bool CServiceOpcUa::SkipValue(UINT si, UINT ns, UINT id)
{
	return false;
}

void CServiceOpcUa::SetValueDouble(UINT si, UINT ns, UINT id, UINT n, double d)
{
	if( IsSessionActive(si) ) {

		if( AllowWrites(si) ) {

			if( !Adjust(ns, id, n, 4) ) {

				return;
			}

			if( AllowAccess(si, n) ) {

				if( ns == 1 ) {

					if( id == 4 ) {

						CTagInfo &Info = m_pInfo[n];

						if( Info.m_Type == typeString ) {

							return;
						}

						Info.m_pTag->SetData(Info.m_Ref, R2I(C3REAL(d)), typeReal, setNone);
					}
				}
			}
		}
	}
}

void CServiceOpcUa::SetValueInt(UINT si, UINT ns, UINT id, UINT n, int d)
{
	if( IsSessionActive(si) ) {

		if( AllowWrites(si) ) {

			if( !Adjust(ns, id, n, 3) ) {

				return;
			}

			if( AllowAccess(si, n) ) {

				if( ns == 1 ) {

					if( id == 3 ) {

						CTagInfo &Info = m_pInfo[n];

						if( Info.m_Type == typeString ) {

							return;
						}

						Info.m_pTag->SetData(Info.m_Ref, d, typeInteger, setNone);
					}
				}
			}
		}
	}
}

void CServiceOpcUa::SetValueInt64(UINT si, UINT ns, UINT id, UINT n, INT64 d)
{
	if( IsSessionActive(si) ) {

		if( AllowWrites(si) ) {

		}
	}
}

void CServiceOpcUa::SetValueString(UINT si, UINT ns, UINT id, UINT n, PCTXT d)
{
	if( IsSessionActive(si) ) {

		if( AllowWrites(si) ) {

			if( !Adjust(ns, id, n, 5) ) {

				return;
			}

			if( AllowAccess(si, n) ) {

				if( ns == 1 ) {

					if( id == 5 ) {

						if( AllowAccess(si, n) ) {

							CTagInfo &Info = m_pInfo[n];

							if( Info.m_Type == typeString ) {

								PUTF p = wstrdup(UtfConvert(d));

								Info.m_pTag->SetData(Info.m_Ref, DWORD(p), typeString, setNone);
							}
						}
					}
				}
			}
		}
	}
}

double CServiceOpcUa::GetValueDouble(UINT si, UINT ns, UINT id, UINT n)
{
	if( IsSessionActive(si) ) {

		if( !Adjust(ns, id, n, 4) ) {

			return 0;
		}

		if( AllowAccess(si, n) ) {

			if( ns == 1 ) {

				if( id == 4 ) {

					CTagInfo &Info = m_pInfo[n];

					if( Info.m_Type == typeString ) {

						return 0;
					}

					return I2R(Info.m_pTag->GetData(Info.m_Ref, typeReal, getNone));
				}
			}

			if( ns == 3 ) {

				UINT  r = id / 100;

				UINT  p = id % 100;

				INDEX f = m_Index.FindName(r);

				if( !m_Index.Failed(f) ) {

					UINT      ndx  = m_Index[f];

					CTagInfo &Info = m_pInfo[ndx];

					return C3INT(Info.m_pTag->GetProp(Info.m_Ref, WORD(p), typeReal));
				}
			}
		}
	}

	return 0;
}

UINT CServiceOpcUa::GetValueInt(UINT si, UINT ns, UINT id, UINT n)
{
	if( IsSessionActive(si) ) {

		if( !Adjust(ns, id, n, 3) ) {

			return 0;
		}

		if( AllowAccess(si, n) ) {

			if( ns == 1 ) {

				if( id == 3 ) {

					CTagInfo &Info = m_pInfo[n];

					if( Info.m_Type == typeString ) {

						return 0;
					}

					return C3INT(Info.m_pTag->GetData(Info.m_Ref, typeInteger, getNone));
				}
			}

			if( ns == 3 ) {

				UINT  r = id / 100;

				UINT  p = id % 100;

				INDEX f = m_Index.FindName(r);

				if( !m_Index.Failed(f) ) {

					UINT      ndx  = m_Index[f];

					CTagInfo &Info = m_pInfo[ndx];

					if( p == tpStateCount ) {

						if( !Info.m_pTag->m_pFormat || !Info.m_pTag->m_pFormat->IsMulti() ) {

							return 0;
						}
					}

					return C3INT(Info.m_pTag->GetProp(Info.m_Ref, WORD(p), typeInteger));
				}
			}
		}
	}

	return 0;
}

UINT64 CServiceOpcUa::GetValueInt64(UINT si, UINT ns, UINT id, UINT n)
{
	if( IsSessionActive(si) ) {

	}

	return 0;
}

CString CServiceOpcUa::GetValueString(UINT si, UINT ns, UINT id, UINT n)
{
	if( IsSessionActive(si) ) {

		if( ns == 0 ) {

			switch( id ) {

				case OpcUaId_Server_ServerArray:

					return m_Server;

				case OpcUaId_Server_ServerStatus_BuildInfo_ManufacturerName:

					return "Red Lion Controls";

				case OpcUaId_Server_ServerStatus_BuildInfo_ProductName:

					return "Crimson-Based Device";

				case OpcUaId_Server_ServerStatus_BuildInfo_ProductUri:

					return "http://www.redlion.net";

				case OpcUaId_Server_ServerStatus_BuildInfo_ApplicationUri:

					return "http://www.redlion.net/crimson";

				case OpcUaId_Server_ServerStatus_BuildInfo_SoftwareVersion:

					return "Crimson 3.1";
			}
		}

		if( !Adjust(ns, id, n, 5) ) {

			return "";
		}

		if( AllowAccess(si, n) ) {

			if( ns == 1 ) {

				if( id == 5 ) {

					CTagInfo &Info = m_pInfo[n];

					CString   Text;

					if( Info.m_Type == typeString ) {

						PUTF pText = PUTF(Info.m_pTag->GetData(Info.m_Ref, typeString, getNone));

						Text       = UtfConvert(pText);

						free(pText);
					}
					else {
						CUnicode uni = Info.m_pTag->GetAsText(Info.m_Ref.t.m_Array, fmtStd);

						Text         = UtfConvert(uni);
					}

					return Text;
				}
			}

			if( ns == 3 ) {

				UINT  r = id / 100;

				UINT  p = id % 100;

				INDEX f = m_Index.FindName(r);

				if( !m_Index.Failed(f) ) {

					UINT      ndx  = m_Index[f];

					CTagInfo &Info = m_pInfo[ndx];

					if( p == 50 ) {

						PUTF pText = PUTF(Info.m_pTag->GetProp(Info.m_Ref, WORD(tpStateText + n), typeString));

						return UtfConvert(pText);
					}
					else {
						PUTF pText = PUTF(Info.m_pTag->GetProp(Info.m_Ref, WORD(p), typeString));

						return UtfConvert(pText);
					}
				}
			}
		}
	}

	return "";
}

timeval CServiceOpcUa::GetValueTime(UINT si, UINT ns, UINT id, UINT n)
{
	if( IsSessionActive(si) ) {

	}

	timeval t;

	t.tv_sec  = 0;
	t.tv_usec = 0;

	return t;
}

bool CServiceOpcUa::GetDesc(CString &Desc, UINT ns, UINT id)
{
	if( ns == 2 ) {

		INDEX f = m_Index.FindName(id);

		if( !m_Index.Failed(f) ) {

			UINT      ndx  = m_Index[f];

			CTagInfo &Info = m_pInfo[ndx];

			PUTF pText = PUTF(Info.m_pTag->GetProp(Info.m_Ref, tpDescription, typeString));

			Desc       = UtfConvert(pText);

			return true;
		}
	}

	return false;
}

bool CServiceOpcUa::GetSourceTimeStamp(timeval &t, UINT ns, UINT id)
{
	return false;
}

bool CServiceOpcUa::HasEvents(CArray <UINT> &List, UINT ns, UINT id)
{
	return false;
}

bool CServiceOpcUa::HasCustomHistory(UINT ns, UINT id)
{
	if( ns == 2 ) {

		INDEX f = m_Index.FindName(id);

		if( !m_Index.Failed(f) ) {

			return true;
		}
	}

	return false;
}

bool CServiceOpcUa::InitHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	if( ns == 2 ) {

		INDEX f = m_Index.FindName(id);

		if( !m_Index.Failed(f) ) {

			UINT      ndx  = m_Index[f];

			CTagInfo &Info = m_pInfo[ndx];

			DWORD     Data;

			Info.m_pTag->InitPrevious(pHistory[0]);

			Info.m_pTag->InitPrevious(Data);

			if( Info.m_pTag->HasChanged(Info.m_Ref, Data, pHistory[0]) ) {

				Info.m_pTag->KillPrevious(pHistory[0]);

				pHistory[0] = Data;

				return true;
			}

			Info.m_pTag->KillPrevious(Data);

			return true;
		}

		return false;
	}

	return false;
}

bool CServiceOpcUa::HasChanged(UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	if( ns == 2 ) {

		INDEX f = m_Index.FindName(id);

		if( !m_Index.Failed(f) ) {

			UINT      ndx  = m_Index[f];

			CTagInfo &Info = m_pInfo[ndx];

			DWORD     Data;

			Info.m_pTag->InitPrevious(Data);

			if( Info.m_pTag->HasChanged(Info.m_Ref, Data, pHistory[0]) ) {

				Info.m_pTag->KillPrevious(pHistory[0]);

				pHistory[0] = Data;

				return true;
			}

			Info.m_pTag->KillPrevious(Data);

			return false;
		}

		return false;
	}

	return false;
}

void CServiceOpcUa::KillHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	if( ns == 2 ) {

		INDEX f = m_Index.FindName(id);

		if( !m_Index.Failed(f) ) {

			UINT      ndx  = m_Index[f];

			CTagInfo &Info = m_pInfo[ndx];

			Info.m_pTag->KillPrevious(pHistory[0]);

			return;
		}

		return;
	}
}

UINT CServiceOpcUa::GetWireType(UINT uType)
{
	return uType;
}

// Implementation

BOOL CServiceOpcUa::LoadTagList(PCBYTE &pData)
{
	if( (m_uTags = GetWord(pData)) ) {

		CTree<DWORD> Check;

		m_pTags = New DWORD[m_uTags];

		UINT tp = 0;

		if( true ) {

			m_uSet1 = GetWord(pData);

			for( UINT n = 0; n < m_uSet1; n++ ) {

				DWORD ref = GetLong(pData);

				if( Check.Insert(ref) ) {

					m_pTags[tp++] = ref;
				}
			}

			m_uSet1 = tp;
		}

		if( true ) {

			m_uSet2 = GetWord(pData);

			for( UINT n = 0; n < m_uSet2; n++ ) {

				DWORD ref = GetLong(pData);

				if( Check.Insert(ref) ) {

					m_pTags[tp++] = ref;
				}
			}

			m_uSet2 = tp - m_uSet1;
		}

		m_uTags = tp;

		return TRUE;
	}

	return FALSE;
}

void CServiceOpcUa::InitTagList(void)
{
	if( m_uTags ) {

		m_pInfo = New CTagInfo[m_uTags];

		for( UINT n = 0; n < m_uTags; n++ ) {

			CTagInfo &Info = m_pInfo[n];

			Info.m_Ref  = (CDataRef &) m_pTags[n];

			Info.m_pTag = m_pSrc->GetItem(Info.m_Ref.t.m_Index);

			if( Info.m_pTag ) {

				Info.m_pTag->SetScan(Info.m_Ref, scanTrue);

				Info.m_Label = UtfConvert(Info.m_pTag->GetLabel(Info.m_Ref.t.m_Array));

				Info.m_Type  = Info.m_pTag->GetDataType();

				Info.m_id    = Info.m_Ref.t.m_Index + 1000 * Info.m_Ref.t.m_Array;

				m_Index.Insert(Info.m_id, n);
			}
		}
	}
}

bool CServiceOpcUa::Adjust(UINT &ns, UINT &id, UINT &n, UINT x)
{
	if( ns == 2 ) {

		INDEX f = m_Index.FindName(id);

		if( !m_Index.Failed(f) ) {

			ns = 1;

			id = x;

			n  = m_Index[f];
		}
	}

	return true;
}

UINT CServiceOpcUa::GetOpcType(UINT Type)
{
	UINT uType = 0;

	switch( Type ) {

		case typeInteger: uType = OpcUaId_Int32;  break;
		case typeReal:    uType = OpcUaId_Double; break;
		case typeString:  uType = OpcUaId_String; break;
	}

	return uType;
}

void CServiceOpcUa::GetConfigGuid(PBYTE pGuid)
{
	CByteArray Data;

	BYTE bDbase[16];

	g_pDbase->GetVersion(bDbase);

	Data.Append(bDbase, 16);

	Data.Append(PCBYTE(&m_uHistSample), sizeof(m_uHistSample));

	Data.Append(PCBYTE(&m_uTags), sizeof(m_uTags));

	for( UINT n = 0; n < m_uTags; n++ ) {

		CTagInfo &Info = m_pInfo[n];

		Data.Append(PCBYTE(&Info.m_Type), sizeof(Info.m_Type));

		Data.Append(PCBYTE(&Info.m_Ref), sizeof(Info.m_Ref));
	}

	extern void MD5(PBYTE data, int bytes, PBYTE hash);

	MD5(PBYTE(Data.GetPointer()), Data.GetCount(), pGuid);
}

bool CServiceOpcUa::IsSessionActive(UINT si)
{
	return si == 1000 || si == 2000 || ((si < 32) && (m_SessActive & (1<<si)));
}

bool CServiceOpcUa::IsSessionAuthenticated(UINT si)
{
	return (si < 32) && (m_SessAuth & (1<<si));
}

bool CServiceOpcUa::AllowWrites(UINT si)
{
	return m_uWrite && (m_uWrite == 1 || IsSessionAuthenticated(si));
}

bool CServiceOpcUa::AllowAccess(UINT si, UINT n)
{
	return (n < m_uSet1) || IsSessionAuthenticated(si);
}

// End of File
