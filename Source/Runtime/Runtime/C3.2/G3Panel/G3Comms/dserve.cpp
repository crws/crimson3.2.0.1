
#include "intern.hpp"

#include "datalog.hpp"

#include "events.hpp"

#include "service.hpp"

#include "sql.hpp"

#include "web.hpp"

#include <fcntl.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Server
//

// Add Functions for Win32

#if defined(WIN32)

#define fmax max

#define fmin min

#endif

// Constants

#define	pi 3.14159265

// Externals

clink int mapname(char *name, int add);

// Function Macros

#define	DefFunc(ID, Name) { ID, 0, (PVOID) &Name, #Name, 0 },

// Function Table

CDataServer::CFunc const CDataServer::m_FuncTable[] =
{
	// NOTE: List must be kept in order!

	DefFunc(0x1000, MinInt)
	DefFunc(0x1001, MinReal)
	DefFunc(0x1002, MaxInt)
	DefFunc(0x1003, MaxReal)
	DefFunc(0x1004, AbsInt)
	DefFunc(0x1005, AbsReal)
	DefFunc(0x1006, SgnInt)
	DefFunc(0x1007, SgnReal)
	DefFunc(0x2004, NOP)
	DefFunc(0x2005, Sleep)
	DefFunc(0x2006, PI)
	DefFunc(0x2007, Deg2Rad)
	DefFunc(0x2008, Rad2Deg)
	DefFunc(0x2009, Cos)
	DefFunc(0x200A, Sin)
	DefFunc(0x200B, Tan)
	DefFunc(0x200C, RawWrite)
	DefFunc(0x200D, RawPrint)
	DefFunc(0x200E, RawRead)
	DefFunc(0x200F, Left)
	DefFunc(0x2010, Right)
	DefFunc(0x2011, Mid)
	DefFunc(0x2012, TextToInt)
	DefFunc(0x2013, IntToText)
	DefFunc(0x2014, RawInput)
	DefFunc(0x2015, SqrtInt)
	DefFunc(0x2016, SqrtReal)
	DefFunc(0x2017, PowInt)
	DefFunc(0x2018, PowReal)
	DefFunc(0x2020, Date)
	DefFunc(0x2021, Time)
	DefFunc(0x2022, GetYear)
	DefFunc(0x2023, GetMonth)
	DefFunc(0x2024, GetDate)
	DefFunc(0x2025, GetDays)
	DefFunc(0x2026, GetWeeks)
	DefFunc(0x2027, GetDay)
	DefFunc(0x2028, GetWeek)
	DefFunc(0x2029, GetWeekYear)
	DefFunc(0x202A, GetHour)
	DefFunc(0x202B, GetMin)
	DefFunc(0x202C, GetSec)
	DefFunc(0x202D, GetMonthDays)
	DefFunc(0x202E, GetNow)
	DefFunc(0x202F, Len)
	DefFunc(0x2030, Random)
	DefFunc(0x2033, SumInt)
	DefFunc(0x2034, SumReal)
	DefFunc(0x2035, MeanInt)
	DefFunc(0x2036, MeanReal)
	DefFunc(0x2037, StdDevInt)
	DefFunc(0x2038, StdDevReal)
	DefFunc(0x2039, PopDevInt)
	DefFunc(0x203A, PopDevReal)
	DefFunc(0x203B, FillInt)
	DefFunc(0x203C, FillReal)
	DefFunc(0x203D, CopyInt)
	DefFunc(0x203E, CopyReal)
	DefFunc(0x203F, Ident)
	DefFunc(0x2040, Ident)
	DefFunc(0x2041, DriveFormat)
	DefFunc(0x2044, ArcCos)
	DefFunc(0x2045, ArcSin)
	DefFunc(0x2046, ArcTan1)
	DefFunc(0x2047, ArcTan2)
	DefFunc(0x2048, Log)
	DefFunc(0x2049, Exp)
	DefFunc(0x204A, Log10)
	DefFunc(0x204B, Exp10)
	DefFunc(0x204C, TextToReal)
	DefFunc(0x204D, Find)
	DefFunc(0x2050, ReadData)
	DefFunc(0x2051, ReadData)
	DefFunc(0x2053, DriveStatus)
	DefFunc(0x2054, DriveEject)
	DefFunc(0x2056, RawClose)
	DefFunc(0x2057, DataToText)
	DefFunc(0x2058, Strip)
	DefFunc(0x2059, StopSystem)
	DefFunc(0x205A, GetNowTime)
	DefFunc(0x205B, GetNowDate)
	DefFunc(0x205C, SetNow)
	DefFunc(0x205D, ClearEvents)
	DefFunc(0x205E, ReadData)
	DefFunc(0x205F, SendMail)
	DefFunc(0x2062, MulDiv)
	DefFunc(0x2063, Scale)
	DefFunc(0x2064, EnableDevice)
	DefFunc(0x2065, DisableDevice)
	DefFunc(0x2066, DecToTextInt)
	DefFunc(0x2067, DecToTextReal)
	DefFunc(0x2068, ControlDevice)
	DefFunc(0x2069, IsDeviceOnline)
	DefFunc(0x206A, FindFileFirst)
	DefFunc(0x206B, FindFileNext)
	DefFunc(0x206C, OpenFile)
	DefFunc(0x206D, CloseFile)
	DefFunc(0x206E, FileReadLine)
	DefFunc(0x206F, GetNetId)
	DefFunc(0x2070, GetNetIp)
	DefFunc(0x2071, GetInterfaceStatus)
	DefFunc(0x2072, FileWriteLine)
	DefFunc(0x2073, CreateFile)
	DefFunc(0x2074, SetInt)
	DefFunc(0x2075, SetReal)
	DefFunc(0x2076, DeleteFile)
	DefFunc(0x2077, CreateDirectory)
//	DefFunc(0x2078, SetNetConfig)
	DefFunc(0x2079, TextToAddr)
	DefFunc(0x207A, DrvCtrl)
	DefFunc(0x207B, DevCtrl)
	DefFunc(0x207C, GetNetMask)
	DefFunc(0x207D, GetNetGate)
	DefFunc(0x207E, DeleteDirectory)
	DefFunc(0x207F, WaitData)
	DefFunc(0x2080, WaitData)
	DefFunc(0x2081, WriteAll)
	DefFunc(0x2082, FileWrite)
	DefFunc(0x2083, FileRead)
	DefFunc(0x2084, NewBatch)
	DefFunc(0x2085, EndBatch)
	DefFunc(0x2086, GetBatch)
	DefFunc(0x2088, CommitAndReset)
	DefFunc(0x208C, SendFile)
	DefFunc(0x208D, GetDiskFreeBytes)
	DefFunc(0x208E, GetDiskFreePercent)
	DefFunc(0x208F, GetDiskSizeBytes)
	DefFunc(0x2090, GetPortConfig)
	DefFunc(0x2091, RenameFile)
	DefFunc(0x2093, LogSave)
//	DefFunc(0x2094, AlarmAccept)
	DefFunc(0x2095, AlarmAcceptAll)
	DefFunc(0x2096, FtpPutFile)
	DefFunc(0x2097, FtpGetFile)
	DefFunc(0x2098, GetIntTag)
	DefFunc(0x2099, GetRealTag)
	DefFunc(0x209A, GetStringTag)
	DefFunc(0x209B, GetFormattedTag)
	DefFunc(0x209C, GetTagLabel)
	DefFunc(0x209D, FindTagIndex)
	DefFunc(0x209E, GetCameraData)
//	DefFunc(0x209F, GetStreamData)
	DefFunc(0x20A0, SaveCameraSetup)
	DefFunc(0x20A1, LoadCameraSetup)
	DefFunc(0x20A2, UseCameraSetup)
//	DefFunc(0x20A3, GetAlarmTag)
	DefFunc(0x20A4, SetIntTag)
	DefFunc(0x20A5, SetRealTag)
	DefFunc(0x20A6, RawSetRTS)
	DefFunc(0x20A7, RawGetCTS)
	DefFunc(0x20A8, IsWriteQueueEmpty)
	DefFunc(0x20A9, EmptyWriteQueue)
	DefFunc(0x20AB, PutFileByte)
	DefFunc(0x20AC, GetFileByte)
	DefFunc(0x20AD, PutFileData)
	DefFunc(0x20AE, GetFileData)
	DefFunc(0x20AF, FileTell)
	DefFunc(0x20B0, FileSeek)
	DefFunc(0x20B1, ForceCopyInt)
	DefFunc(0x20B2, ForceCopyReal)
	DefFunc(0x20B3, ForceInt)
	DefFunc(0x20B4, ForceReal)
	DefFunc(0x20B5, SetStringTag)
	DefFunc(0x20B6, CopyFiles)
	DefFunc(0x20B7, MoveFiles)
	DefFunc(0x20B8, DriveStatusEx)
	DefFunc(0x20B9, DriveEjectEx)
	DefFunc(0x20BA, DriveFormatEx)
	DefFunc(0x20BB, IsPortRemote)
	DefFunc(0x20C3, GetAutoCopyStatusText)
	DefFunc(0x20C4, GetAutoCopyStatusCode)
	DefFunc(0x20C5, LogComment)
	DefFunc(0x20C6, KillDirectory)
	DefFunc(0x20C7, IsLoggingActive)
	DefFunc(0x20C8, GetLastEventTime)
	DefFunc(0x20C9, GetLastEventText)
	DefFunc(0x20CA, GetLastEventType)
	DefFunc(0x20D0, GetVersionInfo)
	DefFunc(0x20D1, GetRestartInfo)
	DefFunc(0x20D2, GetRestartCode)
	DefFunc(0x20D3, GetRestartText)
	DefFunc(0x20D4, GetRestartTime)
	DefFunc(0x20D7, GetDeviceStatus)
	DefFunc(0x20D8, AsTextInt)
	DefFunc(0x20D9, AsTextReal)
	DefFunc(0x20DB, GetModelName)
	DefFunc(0x20E0, NewBatchEx)
	DefFunc(0x20E1, EndBatchEx)
	DefFunc(0x20E2, GetBatchEx)
	DefFunc(0x20E3, SaveConfigFile)
	DefFunc(0x20EA, IsBatteryLow)
	DefFunc(0x20EB, AlarmAcceptTag)
	DefFunc(0x20EC, AlarmAcceptEx)
	DefFunc(0x20ED, HonFindProf)
	DefFunc(0x20EE, HonFindTime)
	DefFunc(0x20EF, LogHeader)
	DefFunc(0x20F0, IsBatchNameValid)
	DefFunc(0x20F1, IsBatchNameValidEx)
	DefFunc(0x20F2, HonGetLogParamS)
	DefFunc(0x20F3, HonGetLogParamI)
	DefFunc(0x20F4, HonSetLogParamI)
	DefFunc(0x20F5, LogBatchComment)
	DefFunc(0x20F6, LogBatchHeader)
	DefFunc(0x20F7, HonGetLogParamA)
	DefFunc(0x20F8, EnumOptionCard)
	DefFunc(0x20F9, ResolveDNS)
	DefFunc(0x20FA, SendFileEx)
	DefFunc(0x20FB, MountCompactFlash)
	DefFunc(0x20FC, EnableBatteryCheck)
	DefFunc(0x20FD, GetWebParamInt)
	DefFunc(0x20FE, GetWebParamHex)
	DefFunc(0x20FF, GetWebParamStr)
	DefFunc(0x2100, GetAlarmTag)
	DefFunc(0x2101, MulU32)
	DefFunc(0x2102, MulR64)
	DefFunc(0x2103, AsTextR64)
	DefFunc(0x2104, TextToR64)
	DefFunc(0x2105, AddR64)
	DefFunc(0x2106, SubR64)
	DefFunc(0x2107, DivR64)
	DefFunc(0x2108, PowR64)
	DefFunc(0x2109, MaxR64)
	DefFunc(0x210A, MinR64)
	DefFunc(0x210B, AbsR64)
	DefFunc(0x210C, SqrtR64)
	DefFunc(0x210D, SinR64)
	DefFunc(0x210E, CosR64)
	DefFunc(0x210F, TanR64)
	DefFunc(0x2110, ASinR64)
	DefFunc(0x2111, ACosR64)
	DefFunc(0x2112, ATanR64)
	DefFunc(0x2113, ATan2R64)
	DefFunc(0x2114, ExpR64)
	DefFunc(0x2115, Exp10R64)
	DefFunc(0x2116, LogR64)
	DefFunc(0x2117, Log10R64)
	DefFunc(0x2118, AddU32)
	DefFunc(0x2119, SubU32)
	DefFunc(0x211A, DivU32)
	DefFunc(0x211B, CompU32)
	DefFunc(0x211C, MaxU32)
	DefFunc(0x211D, MinU32)
	DefFunc(0x211E, RShU32)
	DefFunc(0x211F, ModU32)
	DefFunc(0x2120, GreaterR64)
	DefFunc(0x2121, LessR64)
	DefFunc(0x2122, GreaterEqR64)
	DefFunc(0x2123, LessEqR64)
	DefFunc(0x2124, EqR64)
	DefFunc(0x2125, NEqR64)
	DefFunc(0x2126, UnaryMinusR64)
	DefFunc(0x2127, IncR64)
	DefFunc(0x2128, DecR64)
	DefFunc(0x2129, IntToR64)
	DefFunc(0x212A, RealToR64)
	DefFunc(0x212B, R64ToInt)
	DefFunc(0x212C, R64ToReal)
	DefFunc(0x212D, CommitAndVoidWarranty)
	DefFunc(0x212E, InitRxCAN)
	DefFunc(0x212F, InitTxCAN)
	DefFunc(0x2130, RxCAN)
	DefFunc(0x2131, TxCAN)
	DefFunc(0x2132, InitRxCANMailBox)
	DefFunc(0x2133,	InitTxCANMailBox)
	DefFunc(0x2134, RxCANMail)
	DefFunc(0x2135, TxCANMail)
	DefFunc(0x2136,	ForceSQLSync)
	DefFunc(0x2137,	SetIconLed)
	DefFunc(0x2138, AsTextR64WithFormat)
	DefFunc(0x2139, RawPrintEx)
	DefFunc(0x213A, IsSQLSyncRunning)
	DefFunc(0x213B, GetLastSQLSyncStatus)
	DefFunc(0x213C, GetLastSQLSyncTime)
	DefFunc(0x213D, NetworkPing)
	DefFunc(0x213E, RawSendData)
	DefFunc(0x213F, GetWebUser)
	DefFunc(0x2140, ClearWebUsers)
	DefFunc(0x2141, GetLicenseState)
	DefFunc(0x2142, RunQuery)
	DefFunc(0x2143, RunAllQueries)
	DefFunc(0x2144, GetSQLConnectionStatus)
	DefFunc(0x2145, GetQueryTime)
	DefFunc(0x2146, GetQueryStatus)
	DefFunc(0x2147, GetSerialNumber)
	DefFunc(0x2148, SendMailTo)
	DefFunc(0x2149, SendFileTo)
	DefFunc(0x214A, SendMailToAck)
	DefFunc(0x214B, SendFileToAck)
	DefFunc(0x214C, AsTextL64)
	DefFunc(0x214D, TextToL64)
	DefFunc(0x214E, DebugPrint)
	DefFunc(0x214F, DebugStackTrace)
	DefFunc(0x2150, DebugDumpLocals)
	DefFunc(0x2152, GetPersonalityString)
	DefFunc(0x2153, GetPersonalityInt)
	DefFunc(0x2154, GetPersonalityFloat)
	DefFunc(0x2155, GetPersonalityIp)
	DefFunc(0x2156, SetPersonalityString)
	DefFunc(0x2157, SetPersonalityInt)
	DefFunc(0x2158, SetPersonalityFloat)
	DefFunc(0x2159, SetPersonalityIp)
	DefFunc(0x215A, GetModemProperty)
	DefFunc(0x215B, GetWifiProperty)
	DefFunc(0x215C, GetLocationProperty)
	DefFunc(0x215E, GetSystemIo)
	DefFunc(0x215F, SetSystemIo)
	DefFunc(0x2160, CommitPersonality)

};

// Static Data

CDataServer	  * CDataServer::m_pThis      = NULL;

IMutex		  * CDataServer::m_pLock      = NULL;

IFileUtilities    * CDataServer::m_pFileUtils = NULL;

INetUtilities     * CDataServer::m_pNetUtils  = NULL;

UINT		    CDataServer::m_iFind      = 0;

CAutoDirentList	    CDataServer::m_Find;

CDataServer::CFile  CDataServer::m_File[16];

// Constructor

CDataServer::CDataServer(CCommsSystem *pSystem)
{
	m_pSystem    = pSystem;

	m_pComms     = m_pSystem->m_pComms;

	m_pTags      = m_pSystem->m_pTags->m_pTags;

	m_pProgs     = m_pSystem->m_pPrograms;

	m_pThis      = this;

	m_pLock      = Create_Mutex();

	m_pFileUtils = NULL;

	for( UINT i = 0; i < elements(m_File); i++ ) {

		m_File[i].m_nFile = -1;
	}

	AfxGetObject("aeon.filesupport", 0, IFileUtilities, m_pFileUtils);

	AfxGetObject("ip", 0, INetUtilities, m_pNetUtils);
}

// Destructor

CDataServer::~CDataServer(void)
{
	m_pThis = NULL;

	AfxRelease(m_pFileUtils);

	AfxRelease(m_pNetUtils);

	for( UINT f = 0; f < elements(m_File); f++ ) {

		if( m_File[f].m_nFile != -1 ) {

			close(m_File[f].m_nFile);

			m_File[f].m_nFile = -1;
		}
	}

	m_Find.Empty();

	m_pLock->Release();
}

// IBase Methods

UINT CDataServer::Release(void)
{
	delete this;

	return 0;
}

// IDataServer Methods

WORD CDataServer::CheckID(WORD ID)
{
	return ID;
}

BOOL CDataServer::IsAvail(DWORD ID)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.x.m_IsTag ) {

		if( Ref.t.m_Index < 0x7F80 ) {

			CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

			if( pTag ) {

				return pTag->IsAvail(Ref);
			}
		}

		return FALSE;
	}
	else {
		if( Ref.b.m_Block ) {

			int             nPos   = 0;

			CCommsSysBlock *pBlock = m_pComms->Resolve(Ref, nPos);

			if( pBlock ) {

				return pBlock->IsAvail(nPos);
			}

			return FALSE;
		}

		if( Ref.x.m_Index < 0x7C00 ) {

			UINT hProg = Ref.x.m_Index;

			return m_pProgs->IsAvail(hProg);
		}
	}

	return TRUE;
}

BOOL CDataServer::SetScan(DWORD ID, UINT Code)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.x.m_IsTag ) {

		if( Ref.t.m_Index < 0x7F80 ) {

			CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

			if( pTag ) {

				return pTag->SetScan(Ref, Code);
			}
		}

		return FALSE;
	}
	else {
		if( Ref.b.m_Block ) {

			int             nPos   = 0;

			CCommsSysBlock *pBlock = m_pComms->Resolve(Ref, nPos);

			if( pBlock ) {

				return pBlock->SetScan(nPos, Code);
			}

			return FALSE;
		}

		if( Ref.x.m_Index < 0x7C00 ) {

			UINT hProg = Ref.x.m_Index;

			return m_pProgs->SetScan(hProg, Code);
		}
	}

	return TRUE;
}

DWORD CDataServer::GetData(DWORD ID, UINT Type, UINT Flags)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.t.m_HasBit ) {

		DWORD dwMask   = (1 << Ref.b.m_BitRef);

		Ref.t.m_HasBit = 0;

		Ref.t.m_BitRef = 0;

		DWORD dwData   = GetData(ID, typeInteger, Flags);

		if( dwData & dwMask ) {

			return TRUE;
		}

		return FALSE;
	}

	if( Ref.x.m_IsTag ) {

		if( Ref.t.m_Index < 0x7F80 ) {

			CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

			if( pTag ) {

				return pTag->GetData(Ref, Type, Flags);
			}
		}
	}
	else {
		if( Ref.b.m_Block ) {

			int             nPos   = 0;

			CCommsSysBlock *pBlock = m_pComms->Resolve(Ref, nPos);

			if( pBlock ) {

				return pBlock->GetData(nPos);
			}
		}
		else {
			switch( Ref.t.m_Index ) {

				case 0x7C02: return R2I(C3REAL(pi));
				case 0x7C05: return CCommsSystem::m_pThis->m_pComms->GetCommsError();
				case 0x7C07: return CCommsSystem::m_pThis->m_pEvents->GetActiveAlarms();
				case 0x7C0C: return CCommsSystem::m_pThis->m_pEvents->GetUnacceptedAlarms();
				case 0x7C08: return GetDaylight();
				case 0x7C09: return GetTimeZone();
				case 0x7C0A: return GetTimeZoneMins();
				case 0x7C0D: return GetNow();
				case 0x7C0F: return CCommsSystem::m_pThis->m_pEvents->GetUnacceptedAndAutoAlarms();
			}
		}
	}

	if( Type == typeReal ) {

		return R2I(0.0);
	}

	if( Type == typeString ) {

		return DWORD(wstrdup(L""));
	}

	return 0;
}

BOOL CDataServer::SetData(DWORD ID, UINT Type, UINT Flags, DWORD Data)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.t.m_HasBit ) {

		DWORD dwMask   = (1 << Ref.b.m_BitRef);

		Ref.t.m_HasBit = 0;

		Ref.t.m_BitRef = 0;

		DWORD dwData   = GetData(ID, Type, Flags);

		if( Data ) {

			if( !(dwData & dwMask) ) {

				dwData |= dwMask;

				return SetData(ID, Type, Flags, dwData);
			}

			return TRUE;
		}
		else {
			if( dwData & dwMask ) {

				dwData &= ~dwMask;

				return SetData(ID, Type, Flags, dwData);
			}

			return TRUE;
		}
	}

	if( Ref.x.m_IsTag ) {

		if( Ref.t.m_Index < 0x7F80 ) {

			CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

			if( pTag ) {

				return pTag->SetData(Ref, Data, Type, Flags);
			}
		}
	}
	else {
		if( Ref.b.m_Block ) {

			int             nPos   = 0;

			CCommsSysBlock *pBlock = m_pComms->Resolve(Ref, nPos);

			if( pBlock ) {

				pBlock->SetWriteData(nPos, Flags, Data);

				return TRUE;
			}
		}
		else {
			if( Ref.t.m_Index == 0x7C08 ) {

				SetDaylight(BOOL(Data));

				return TRUE;
			}

			if( Ref.t.m_Index == 0x7C09 ) {

				if( C3INT(Data) >= -12 && C3INT(Data) <= +12 ) {

					SetTimeZone(C3INT(Data));

					return TRUE;
				}
			}

			if( Ref.t.m_Index == 0x7C0A ) {

				if( C3INT(Data) >= -12 * 60 && C3INT(Data) <= +12 * 60 ) {

					SetTimeZoneMins(C3INT(Data));

					return TRUE;
				}
			}

			if( Ref.t.m_Index == 0x7C0D ) {

				SetNow(Data);

				return TRUE;
			}
		}
	}

	if( Type == typeString ) {

		Free(PUTF(Data));
	}

	return FALSE;
}

DWORD CDataServer::GetProp(DWORD ID, WORD Prop, UINT Type)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.t.m_IsTag ) {

		if( Prop ) {

			if( Ref.t.m_Index < 0x7F80 ) {

				CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

				if( pTag ) {

					return pTag->GetProp(Ref, Prop, Type);
				}
			}
		}

		return CTag::GetDefProp(Prop, Type);
	}

	if( Type == typeReal ) {

		return R2I(0.0);
	}

	if( Type == typeString ) {

		return DWORD(wstrdup(L""));
	}

	return 0;
}

DWORD CDataServer::RunFunc(WORD ID, UINT uParam, PDWORD pParam)
{
	DWORD Data = 0;

	RunFunc(Data, ID, uParam, pParam);

	return Data;
}

BOOL CDataServer::GetName(DWORD ID, UINT m, PSTR pName, UINT uName)
{
	if( m == 0 ) {

		CDataRef &Ref = (CDataRef &) ID;

		if( Ref.x.m_IsTag ) {

			if( Ref.t.m_Index < 0x7F80 ) {

				CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

				strcpy(pName, UniConvert(pTag->m_Name));

				if( pTag->IsArray() ) {

					strcat(pName, CPrintf("[%u]", Ref.t.m_Array));
				}

				if( Ref.t.m_HasBit ) {

					strcat(pName, CPrintf(".%u", Ref.b.m_BitRef));
				}

				return TRUE;
			}
		}
	}

	if( m == 1 ) {

		if( ID & 0x8000 ) {

			UINT hProg = (ID & 0x7FFF);

			if( m_pProgs->FindName(hProg, pName, uName) ) {

				return TRUE;
			}
		}
		else {
			if( FindName(ID, pName, uName, m_FuncTable, elements(m_FuncTable)) ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Implementation

BOOL CDataServer::RunFunc(DWORD &Data, WORD ID, UINT uParam, PDWORD pParam)
{
	if( ID & 0x8000 ) {

		UINT hProg = (ID & 0x7FFF);

		Data = m_pProgs->Run(hProg, pParam);

		return TRUE;
	}

	return ScanTable(Data, ID, uParam, pParam, m_FuncTable, elements(m_FuncTable));
}

// Function Caller

BOOL CDataServer::ScanTable(DWORD &Data, WORD ID, UINT uParam, PDWORD pParam, CFunc const *pTable, UINT uCount)
{
	int  n1 = 0;

	int  n2 = uCount - 1;

	int  np = 0;

	WORD ti = 0;

	for( ;;) {

		np = (n1 + n2) / 2;

		ti = pTable[np].m_ID;

		if( ti == ID ) {

			DWORD  fp = DWORD(pTable[np].m_pFunc);

			PDWORD pp = pParam;

			switch( uParam ) {

				case 0:
					Data =	((DWORD(*)(void))(fp))
						();
					break;

				case 1:
					Data =	((DWORD(*)(DWORD))(fp))
						(pp[0]);
					break;

				case 2:
					Data =	((DWORD(*)(DWORD, DWORD))(fp))
						(pp[0], pp[1]);
					break;

				case 3:
					Data =	((DWORD(*)(DWORD, DWORD, DWORD))(fp))
						(pp[0], pp[1], pp[2]);
					break;

				case 4:
					Data =	((DWORD(*)(DWORD, DWORD, DWORD, DWORD))(fp))
						(pp[0], pp[1], pp[2], pp[3]);
					break;

				case 5:
					Data =	((DWORD(*)(DWORD, DWORD, DWORD, DWORD, DWORD))(fp))
						(pp[0], pp[1], pp[2], pp[3], pp[4]);
					break;

				case 6:
					Data =	((DWORD(*)(DWORD, DWORD, DWORD, DWORD, DWORD, DWORD))(fp))
						(pp[0], pp[1], pp[2], pp[3], pp[4], pp[5]);
					break;

				case 7:
					Data =	((DWORD(*)(DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD))(fp))
						(pp[0], pp[1], pp[2], pp[3], pp[4], pp[5], pp[6]);
					break;

				case 8:
					Data =	((DWORD(*)(DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD))(fp))
						(pp[0], pp[1], pp[2], pp[3], pp[4], pp[5], pp[6], pp[7]);
					break;

				case 9:
					Data =	((DWORD(*)(DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD))(fp))
						(pp[0], pp[1], pp[2], pp[3], pp[4], pp[5], pp[6], pp[7], pp[8]);
					break;

				case 10:
					Data =	((DWORD(*)(DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD, DWORD))(fp))
						(pp[0], pp[1], pp[2], pp[3], pp[4], pp[5], pp[6], pp[7], pp[8], pp[9]);
					break;

				default:
					Data = 0;
					break;
			}

			return TRUE;
		}

		if( ti < ID ) {

			n1 = np + 1;
		}
		else
			n2 = np - 1;

		if( n1 > n2 ) {

			return FALSE;
		}
	}

	return FALSE;
}

// Name Location

BOOL CDataServer::FindName(WORD	ID, PSTR pName, UINT uName, CFunc const * pTable, UINT uCount)
{
	int  n1 = 0;

	int  n2 = uCount - 1;

	WORD ti;

	int  np = 0;

	while( n1 <= n2 ) {

		np = (n1 + n2) / 2;

		ti = pTable[np].m_ID;

		if( ti == ID ) {

			strcpy(pName, pTable[np].m_pName);

			return TRUE;
		}

		if( ti < ID ) {

			n1 = np + 1;
		}
		else
			n2 = np - 1;
	}

	return FALSE;
}

// Core Functions

C3INT CDataServer::NOP(void)
{
	return 0;
}

void CDataServer::Sleep(C3INT t)
{
	::Sleep(t);
}

void CDataServer::StopSystem(void)
{
	g_pPxe->SystemStop();
}

void CDataServer::CommitAndReset(void)
{
	CCommsSystem::m_pThis->m_pTags->CommitAndReset();
}

void CDataServer::CommitAndVoidWarranty(void)
{
	CCommsSystem::m_pThis->m_pTags->Commit();
}

void CDataServer::ClearEvents(void)
{
	CEventLogger::m_pThis->LogClear();
}

C3INT CDataServer::GetLastEventTime(C3INT a)
{
	return CEventLogger::m_pThis->GetLastTime(a);
}

PUTF CDataServer::GetLastEventText(C3INT a)
{
	return wstrdup(CEventLogger::m_pThis->GetLastText(a));
}

PUTF CDataServer::GetLastEventType(C3INT a)
{
	return wstrdup(CEventLogger::m_pThis->GetLastType(a));
}

void CDataServer::LogSave(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CDataLogger::m_pThis->ForceSave();
	}
}

void CDataServer::LogComment(C3INT n, PUTF p)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CDataLogger::m_pThis->LogComment(LOBYTE(n), UniConvert(p), FALSE);
	}

	Free(p);
}

void CDataServer::LogHeader(C3INT n, PUTF p)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CDataLogger::m_pThis->LogComment(LOBYTE(n), UniConvert(p), TRUE);
	}

	Free(p);
}

void CDataServer::LogBatchComment(C3INT n, PUTF p)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CDataLogger::m_pThis->BatchComment(LOBYTE(n), UniConvert(p), FALSE);
	}

	Free(p);
}

void CDataServer::LogBatchHeader(C3INT n, PUTF p)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CDataLogger::m_pThis->BatchComment(LOBYTE(n), UniConvert(p), TRUE);
	}

	Free(p);
}

C3INT CDataServer::IsLoggingActive(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		return CDataLogger::m_pThis->IsActive();
	}

	return 0;
}

void CDataServer::AlarmAcceptAll(void)
{
	CCommsSystem::m_pThis->m_pEvents->AcceptAll();
}

void CDataServer::AlarmAcceptTag(UINT Tag, UINT Index, UINT Event)
{
	DWORD Code = 0;

	Code |= Tag;

	Code |= Index << 16;

	Code |= Event << 24;

	CCommsSystem::m_pThis->m_pEvents->AcceptAlarm(0, Code);
}

void CDataServer::AlarmAcceptEx(UINT Source, UINT Method, UINT Code)
{
	CCommsSystem::m_pThis->m_pEvents->AcceptAlarmEx(Source, Method, Code);
}

C3INT CDataServer::GetAlarmTag(C3INT n)
{
	CDataRef Ref;

	Ref.m_Ref = 0;

	Ref.t.m_Index = n;
	Ref.t.m_IsTag = 1;

	return m_pThis->GetProp(Ref.m_Ref, tpAlarms, typeInteger);
}

C3INT CDataServer::GetVersionInfo(C3INT n)
{
	switch( n ) {

		case 1:		return AppGetBootVersion();

		case 2:		return C3_BUILD;

		case 3:		return CCommsSystem::m_pThis->m_Build;

		default:	return -1;
	}
}

PUTF CDataServer::GetRestartInfo(C3INT n)
{
	if( ++n >= 1 && n <= 7 ) {

		DWORD dwData[4];

		if( TrapGetData(n, dwData) ) {

			char sText[128];

			DWORD Time = dwData[1];

			SPrintf(sText,
				"%2.2u/%2.2u/%2.2u %2.2u:%2.2u:%2.2u  ",
				GetMonth(Time),
				GetDate(Time),
				GetYear(Time) % 100,
				GetHour(Time),
				GetMin(Time),
				GetSec(Time)
			);

			if( !dwData[3] ) {

				strcat(sText, "Software Reset");
			}
			else {

				UINT  uType  = ((dwData[2] >> 18) & 0xFF);

				DWORD dwAddr = dwData[3];

				SPrintf(sText + strlen(sText),
					"Exception 0x%2.2X at %8.8X",
					uType,
					dwAddr
				);
			}

			return wstrdup(sText);
		}

		return wstrdup("00/00/00 00:00:00  Hardware Reset");
	}

	return wstrdup(L"00/00/00 00:00:00  Unknown");
}

PUTF CDataServer::GetRestartCode(C3INT n)
{
	if( ++n >= 1 && n <= 7 ) {

		DWORD dwData[4];

		if( TrapGetData(n, dwData) ) {

			char  s[64]  = { 0 };

			BYTE  bType  = LOBYTE(dwData[2] >> 18);

			DWORD dwAddr = dwData[3];

			sprintf(s,
				"%2.2X-%4.4X-%4.4X-3%3.3u",
				bType,
				HIWORD(dwAddr),
				LOWORD(dwAddr),
				C3_BUILD
			);

			return wstrdup(s);
		}
	}

	return wstrdup("00-0000-0000-00");
}

PUTF CDataServer::GetRestartText(C3INT n)
{
	if( ++n >= 1 && n <= 7 ) {

		DWORD dwData[4];

		if( TrapGetData(n, dwData) ) {

			char sText[128];

			DWORD Time = dwData[1];

			if( !dwData[3] ) {

				strcpy(sText, "Software Reset");
			}
			else {
				UINT  uType  = ((dwData[2] >> 18) & 0xFF);

				DWORD dwAddr = dwData[3];

				SPrintf(sText,
					"Exception 0x%2.2X at %8.8X",
					uType,
					dwAddr
				);
			}

			return wstrdup(sText);
		}

		return wstrdup("Hardware Reset");
	}

	return wstrdup(L"Unknown");
}

C3INT CDataServer::GetRestartTime(C3INT n)
{
	if( ++n >= 1 && n <= 7 ) {

		DWORD dwData[4];

		if( TrapGetData(n, dwData) ) {

			return dwData[1];
		}
	}

	return 0;
}

PUTF CDataServer::GetModelName(C3INT g)
{
	if( g == 1 ) {

		char sModel[32];

		WhoGetModel(sModel);

		return wstrdup(sModel);
	}

	return wstrdup("");
}

PUTF CDataServer::GetSerialNumber(void)
{
	AfxGetAutoObject(pPlatform, "platform", 0, IPlatform);

	return wstrdup(pPlatform->GetSerial());
}

C3INT CDataServer::SaveConfigFile(PUTF p)
{
	char a[MAX_PATH];

	AdjustName(a, p);

	return g_pPxe->ImageSave(a);
}

C3INT CDataServer::IsBatteryLow(void)
{
	return !IsBatteryOK();
}

C3INT CDataServer::EnumOptionCard(C3INT s)
{
	return 0;
}

void CDataServer::EnableBatteryCheck(C3INT f)
{
	BYTE bData[2];

	bData[0] = f ? 'B' : 0x00;

	bData[1] = f ? 'm' : 0x00;

	FRAMPutData(Mem(Battery), bData, 2);
}

void CDataServer::SetIconLed(C3INT n, C3INT s)
{
	UINT uMode = g_pPxe->GetLedMode();

	if( n ) {

		n--;
	}

	if( n == 0 && !(uMode & (1 << n)) ) {

		return;
	}

	if( n == 1 && !(uMode & (1 << n)) ) {

		return;
	}

	if( n == 2 && !(uMode & (1 << n)) ) {

		return;
	}

	KeySetLED(n, !!s);
}

PUTF CDataServer::GetPersonalityString(PUTF n)
{
	CString Data;

	g_pPxe->GetPersonality(UniConvert(n), Data);

	free(n);

	return wstrdup(Data);
}

C3INT CDataServer::GetPersonalityInt(PUTF n)
{
	CString Data;

	g_pPxe->GetPersonality(UniConvert(n), Data);

	free(n);

	return strtol(Data, NULL, 10);
}

DWORD CDataServer::GetPersonalityFloat(PUTF n)
{
	CString Data;

	g_pPxe->GetPersonality(UniConvert(n), Data);

	free(n);

	return R2I(strtof(Data, NULL));
}

C3INT CDataServer::GetPersonalityIp(PUTF n)
{
	CString Data;

	g_pPxe->GetPersonality(UniConvert(n), Data);

	free(n);

	return HostToMotor(CIpAddr(Data).GetValue());
}

void CDataServer::SetPersonalityString(PUTF n, PUTF d)
{
	g_pPxe->SetPersonality(UniConvert(n), UniConvert(d));

	free(n);

	free(d);
}

void CDataServer::SetPersonalityInt(PUTF n, C3INT d)
{
	g_pPxe->SetPersonality(UniConvert(n), CPrintf("%d", d));

	free(n);
}

void CDataServer::SetPersonalityFloat(PUTF n, C3REAL d)
{
	g_pPxe->SetPersonality(UniConvert(n), CPrintf("%f", d));

	free(n);
}

void CDataServer::SetPersonalityIp(PUTF n, C3INT d)
{
	g_pPxe->SetPersonality(UniConvert(n), CIpAddr(HostToMotor(DWORD(d))).GetAsText());

	free(n);
}

void CDataServer::CommitPersonality(C3INT b)
{
	g_pPxe->CommitPersonality(b ? TRUE : FALSE);
}

C3INT CDataServer::GetSystemIo(PUTF n)
{
	CString Prop(UniConvert(n));

	Free(n);

	AfxGetAutoObject(pIo, "dev.devio", 0, IDeviceIo);

	if( pIo ) {

		if( Prop == "DO" ) {

			return pIo->GetDigitalOut() ? 1 : 0;
		}

		if( Prop == "DI" ) {

			return pIo->GetDigitalIn() ? 1 : 0;
		}

		if( Prop == "AI" ) {

			return pIo->GetAnalogIn();
		}

		if( Prop == "VI" ) {

			return pIo->GetPsuVoltage();
		}
	}

	return 0;
}

void CDataServer::SetSystemIo(PUTF n, C3INT v)
{
	CString Prop(UniConvert(n));

	Free(n);

	AfxGetAutoObject(pIo, "dev.devio", 0, IDeviceIo);

	if( pIo ) {

		if( Prop == "DO" ) {

			pIo->SetDigitalOut(v ? true : false);
		}
	}
}

// Data Manipulation

void CDataServer::SetInt(DWORD r, C3INT d)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	pData->SetData(r, typeInteger, setDirect, d);
}

void CDataServer::SetReal(DWORD r, C3REAL d)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	pData->SetData(r, typeReal, setDirect, R2I(d));
}

void CDataServer::ForceInt(DWORD r, C3INT d)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	pData->SetData(r, typeInteger, setDirect | setForce, d);
}

void CDataServer::ForceReal(DWORD r, C3REAL d)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	pData->SetData(r, typeReal, setDirect | setForce, R2I(d));
}

void CDataServer::FillInt(DWORD i, DWORD v, UINT n)
{
	FillData(i, v, n, typeInteger);
}

void CDataServer::FillReal(DWORD i, DWORD v, UINT n)
{
	FillData(i, v, n, typeReal);
}

void CDataServer::FillData(DWORD i, DWORD v, UINT n, UINT t)
{
	CDataRef &Ref = (CDataRef &) i;

	for( UINT j = 0; j < n; j++ ) {

		m_pThis->SetData(Ref.m_Ref, t, setDirect, v);

		Ref.x.m_Array++;
	}
}

void CDataServer::CopyInt(DWORD d, DWORD s, UINT n)
{
	CopyData(d, s, n, typeInteger);
}

void CDataServer::CopyReal(DWORD d, DWORD s, UINT n)
{
	CopyData(d, s, n, typeReal);
}

void CDataServer::CopyData(DWORD d, DWORD s, UINT n, UINT t)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	CDataRef    &Ref   = (CDataRef &) d;

	CDataRef    &Src   = (CDataRef &) s;

	DWORD        Data  = 0;

	for( UINT j = 0; j < n; j++ ) {

		Data = pData->GetData(Src.m_Ref, t, getNone);

		pData->SetData(Ref.m_Ref, t, setDirect, Data);

		Ref.x.m_Array++;

		Src.x.m_Array++;
	}
}

void CDataServer::ForceCopyInt(DWORD d, DWORD s, UINT n)
{
	ForceCopy(d, s, n, typeInteger);
}

void CDataServer::ForceCopyReal(DWORD d, DWORD s, UINT n)
{
	ForceCopy(d, s, n, typeReal);
}

void CDataServer::ForceCopy(DWORD d, DWORD s, UINT n, UINT t)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	CDataRef    &Ref   = (CDataRef &) d;

	CDataRef    &Src   = (CDataRef &) s;

	DWORD        Data  = 0;

	for( UINT j = 0; j < n; j++ ) {

		Data = pData->GetData(Src.m_Ref, t, getNone);

		pData->SetData(Ref.m_Ref, t, setForce | setDirect, Data);

		Ref.x.m_Array++;

		Src.x.m_Array++;
	}
}

// Math Functions

DWORD CDataServer::Ident(DWORD d)
{
	return d;
}

C3INT CDataServer::Random(C3INT n)
{
	return n ? (rand() % n) : n;
}

C3INT CDataServer::MulDiv(C3INT a, C3INT b, C3INT c)
{
	if( c ) {

		return C3INT(INT64(a) * INT64(b) / INT64(c));
	}

	return 0;
}

C3INT CDataServer::Scale(C3INT d, C3INT r1, C3INT r2, C3INT e1, C3INT e2)
{
	if( r1 - r2 && e1 - e2 ) {

		INT64 a = d;

		a -= r1;

		a *= (e2 - e1);

		a /= (r2 - r1);

		a += e1;

		return C3INT(a);
	}

	return 0;
}

C3INT CDataServer::MinInt(C3INT a, C3INT b)
{
	return (a < b) ? a : b;
}

C3INT CDataServer::MaxInt(C3INT a, C3INT b)
{
	return (a > b) ? a : b;
}

C3INT CDataServer::AbsInt(C3INT a)
{
	return (a < 0) ? -a : +a;
}

C3INT CDataServer::SgnInt(C3INT a)
{
	return a ? (a < 0) ? -1 : +1 : 0;
}

DWORD CDataServer::SqrtInt(C3INT a)
{
	return R2I(((a > 0) ? sqrt(float(a)) : 0));
}

C3INT CDataServer::PowInt(C3INT b, C3INT e)
{
	return C3INT(pow(float(b), e));
}

DWORD CDataServer::MinReal(C3REAL a, C3REAL b)
{
	return R2I((a < b) ? a : b);
}

DWORD CDataServer::MaxReal(C3REAL a, C3REAL b)
{
	return R2I((a > b) ? a : b);
}

DWORD CDataServer::AbsReal(C3REAL a)
{
	return R2I((a < 0) ? -a : +a);
}

DWORD CDataServer::SgnReal(C3REAL a)
{
	return R2I(a ? (a < 0) ? -1.0f : +1.0f : 0);
}

DWORD CDataServer::SqrtReal(C3REAL a)
{
	return R2I((a > 0) ? sqrt(a) : 0);
}

DWORD CDataServer::PowReal(C3REAL b, C3REAL e)
{
	return R2I(pow(b, e));
}

DWORD CDataServer::PI(void)
{
	return R2I(C3REAL(pi));
}

DWORD CDataServer::Deg2Rad(C3REAL a)
{
	return R2I(a / 180 * pi);
}

DWORD CDataServer::Rad2Deg(C3REAL a)
{
	return R2I(a * 180 / pi);
}

DWORD CDataServer::Cos(C3REAL a)
{
	return R2I(cos(a));
}

DWORD CDataServer::Sin(C3REAL a)
{
	return R2I(sin(a));
}

DWORD CDataServer::Tan(C3REAL a)
{
	return R2I(tan(a));
}

DWORD CDataServer::ArcCos(C3REAL a)
{
	return R2I(acos(a));
}

DWORD CDataServer::ArcSin(C3REAL a)
{
	return R2I(asin(a));
}

DWORD CDataServer::ArcTan1(C3REAL a)
{
	return R2I(atan(a));
}

DWORD CDataServer::ArcTan2(C3REAL a, C3REAL b)
{
	return R2I(atan2(a, b));
}

DWORD CDataServer::Log(C3REAL a)
{
	return R2I(log(a));
}

DWORD CDataServer::Exp(C3REAL a)
{
	return R2I(exp(a));
}

DWORD CDataServer::Log10(C3REAL a)
{
	return R2I(log10(a));
}

DWORD CDataServer::Exp10(C3REAL a)
{
	return R2I(pow(10, a));
}

DWORD CDataServer::MulU32(UINT a, UINT b)
{
	return a * b;
}

DWORD CDataServer::AddU32(UINT a, UINT b)
{
	return a + b;
}

DWORD CDataServer::SubU32(UINT a, UINT b)
{
	return a - b;
}

DWORD CDataServer::DivU32(UINT a, UINT b)
{
	return b ? (a / b) : 0;
}

DWORD CDataServer::CompU32(UINT a, UINT b)
{
	if( a == b ) {

		return 0;
	}
	else if( a > b ) {

		return 1;
	}
	else {
		return -1;
	}
}

DWORD CDataServer::MaxU32(UINT a, UINT b)
{
	return (a > b) ? a : b;
}

DWORD CDataServer::MinU32(UINT a, UINT b)
{
	return (a < b) ? a : b;
}

DWORD CDataServer::RShU32(UINT a, UINT b)
{
	return a >> b;
}

DWORD CDataServer::ModU32(UINT a, UINT b)
{
	return b ? (a % b) : 0;
}

// 64-bit Floating Point

void CDataServer::CReal64BinaryOp(C3INT res, C3INT a, C3INT b, int operation)
{
	CDataRef &RefA = (CDataRef &) a;
	CDataRef &RefB = (CDataRef &) b;
	CDataRef &RefR = (CDataRef &) res;

	CReal64 Rr, Ra, Rb;

	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	Ra.i[1] = pData->GetData(RefA.m_Ref, typeInteger, getNone);

	RefA.x.m_Array++;

	Ra.i[0] = pData->GetData(RefA.m_Ref, typeInteger, getNone);

	Rb.i[1] = pData->GetData(RefB.m_Ref, typeInteger, getNone);

	RefB.x.m_Array++;

	Rb.i[0] = pData->GetData(RefB.m_Ref, typeInteger, getNone);

	switch( operation ) {

		case operationMul:
			Rr.d = Ra.d * Rb.d;
			break;

		case operationAdd:
			Rr.d = Ra.d + Rb.d;
			break;

		case operationSub:
			Rr.d = Ra.d - Rb.d;
			break;

		case operationDiv:
			Rr.d = Ra.d / Rb.d;
			break;

		case operationMax:
			Rr.d = fmax(Ra.d, Rb.d);
			break;

		case operationMin:
			Rr.d = fmin(Ra.d, Rb.d);
			break;

		case operationPow:
			Rr.d = pow(Ra.d, Rb.d);
			break;

		case operationATan2:
			Rr.d = atan2(Ra.d, Rb.d);
			break;

		default:
			Rr.d = sqrt(-1.0); // NaN
			break;
	}

	m_pThis->SetData(RefR.m_Ref, typeInteger, setDirect | setForce, Rr.i[1]);

	RefR.x.m_Array++;

	m_pThis->SetData(RefR.m_Ref, typeInteger, setDirect | setForce, Rr.i[0]);
}

void CDataServer::CReal64UnaryOp(C3INT res, C3INT a, int operation)
{
	CDataRef &RefA = (CDataRef &) a;
	CDataRef &RefR = (CDataRef &) res;

	CReal64 Rr, Ra;

	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	Ra.i[1] = pData->GetData(RefA.m_Ref, typeInteger, getNone);

	RefA.x.m_Array++;

	Ra.i[0] = pData->GetData(RefA.m_Ref, typeInteger, getNone);

	switch( operation ) {

		case operationAbs:
			Rr.d = fabs(Ra.d);
			break;

		case operationSqrt:
			Rr.d = sqrt(Ra.d);
			break;

		case operationSin:
			Rr.d = sin(Ra.d);
			break;

		case operationCos:
			Rr.d = cos(Ra.d);
			break;

		case operationTan:
			Rr.d = tan(Ra.d);
			break;

		case operationASin:
			Rr.d = asin(Ra.d);
			break;

		case operationACos:
			Rr.d = acos(Ra.d);
			break;

		case operationATan:
			Rr.d = atan(Ra.d);
			break;

		case operationExp:
			Rr.d = exp(Ra.d);
			break;

		case operationExp10:
			Rr.d = pow(10, Ra.d);
			break;

		case operationLog:
			Rr.d = log(Ra.d);
			break;

		case operationLog10:
			Rr.d = log10(Ra.d);
			break;

		case operationMinus:
			Rr.d = -Ra.d;
			break;

		case operationInc:
			Rr.d = ++Ra.d;
			break;

		case operationDec:
			Rr.d = --Ra.d;
			break;

		default:
			Rr.d = sqrt(-1.0); // Nan
			break;
	}

	m_pThis->SetData(RefR.m_Ref, typeInteger, setDirect | setForce, Rr.i[1]);

	RefR.x.m_Array++;

	m_pThis->SetData(RefR.m_Ref, typeInteger, setDirect | setForce, Rr.i[0]);
}

BOOL CDataServer::CReal64Compare(C3INT a, C3INT b, int operation)
{
	CDataRef &RefA = (CDataRef &) a;
	CDataRef &RefB = (CDataRef &) b;

	CReal64 Ra, Rb;

	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	Ra.i[1] = pData->GetData(RefA.m_Ref, typeInteger, getNone);

	RefA.x.m_Array++;

	Ra.i[0] = pData->GetData(RefA.m_Ref, typeInteger, getNone);

	Rb.i[1] = pData->GetData(RefB.m_Ref, typeInteger, getNone);

	RefB.x.m_Array++;

	Rb.i[0] = pData->GetData(RefB.m_Ref, typeInteger, getNone);

	switch( operation ) {

		case operationGT:	return Ra.d > Rb.d;
		case operationGTE:	return Ra.d >= Rb.d;
		case operationLT:	return Ra.d < Rb.d;
		case operationLTE:	return Ra.d <= Rb.d;
		case operationEQ:	return Ra.d == Rb.d;
		case operationNEQ:	return Ra.d != Rb.d;
	}

	return FALSE;
}

void CDataServer::MulR64(C3INT res, C3INT a, C3INT b)
{
	CReal64BinaryOp(res, a, b, operationMul);
}

void CDataServer::AddR64(C3INT res, C3INT a, C3INT b)
{
	CReal64BinaryOp(res, a, b, operationAdd);
}

void CDataServer::SubR64(C3INT res, C3INT a, C3INT b)
{
	CReal64BinaryOp(res, a, b, operationSub);
}

void CDataServer::DivR64(C3INT res, C3INT a, C3INT b)
{
	CReal64BinaryOp(res, a, b, operationDiv);
}

void CDataServer::PowR64(C3INT res, C3INT a, C3INT b)
{
	CReal64BinaryOp(res, a, b, operationPow);
}

void CDataServer::MaxR64(C3INT res, C3INT a, C3INT b)
{
	CReal64BinaryOp(res, a, b, operationMax);
}

void CDataServer::MinR64(C3INT res, C3INT a, C3INT b)
{
	CReal64BinaryOp(res, a, b, operationMin);
}

void CDataServer::AbsR64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationAbs);
}

void CDataServer::SqrtR64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationSqrt);
}

void CDataServer::SinR64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationSin);
}

void CDataServer::CosR64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationCos);
}

void CDataServer::TanR64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationTan);
}

void CDataServer::ASinR64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationASin);
}

void CDataServer::ACosR64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationACos);
}

void CDataServer::ATanR64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationATan);
}

void CDataServer::ATan2R64(C3INT res, C3INT a, C3INT b)
{
	CReal64BinaryOp(res, a, b, operationATan2);
}

void CDataServer::ExpR64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationExp);
}

void CDataServer::Exp10R64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationExp10);
}

void CDataServer::LogR64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationLog);
}

void CDataServer::Log10R64(C3INT res, C3INT a)
{
	CReal64UnaryOp(res, a, operationLog10);
}

DWORD CDataServer::GreaterR64(C3INT a, C3INT b)
{
	return CReal64Compare(a, b, operationGT);
}

DWORD CDataServer::LessR64(C3INT a, C3INT b)
{
	return CReal64Compare(a, b, operationLT);
}

DWORD CDataServer::GreaterEqR64(C3INT a, C3INT b)
{
	return CReal64Compare(a, b, operationGTE);
}

DWORD CDataServer::LessEqR64(C3INT a, C3INT b)
{
	return CReal64Compare(a, b, operationLTE);
}

DWORD CDataServer::EqR64(C3INT a, C3INT b)
{
	return CReal64Compare(a, b, operationEQ);
}

DWORD CDataServer::NEqR64(C3INT a, C3INT b)
{
	return CReal64Compare(a, b, operationNEQ);
}

void CDataServer::UnaryMinusR64(UINT res, UINT a)
{
	CReal64UnaryOp(res, a, operationMinus);
}

void CDataServer::IncR64(UINT res, UINT a)
{
	CReal64UnaryOp(res, a, operationInc);
}

void CDataServer::DecR64(UINT res, UINT a)
{
	CReal64UnaryOp(res, a, operationDec);
}


// Statistical Functions

C3INT CDataServer::SumInt(DWORD i, UINT n)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	CDataRef    &Ref   = (CDataRef &) i;

	C3INT        Sum   = 0;

	for( UINT j = 0; j < n; j++ ) {

		Sum += pData->GetData(Ref.m_Ref, typeInteger, getNone);

		Ref.x.m_Array++;
	}

	return Sum;
}

DWORD CDataServer::MeanInt(DWORD i, UINT n)
{
	return R2I(n ? C3INT(SumInt(i, n)) / C3REAL(n) : 0);
}

DWORD CDataServer::StdDevInt(DWORD i, UINT n)
{
	return DevInt(i, n, n - 1);
}

DWORD CDataServer::PopDevInt(DWORD i, UINT n)
{
	return DevInt(i, n, n);
}

DWORD CDataServer::DevInt(DWORD i, UINT n, UINT m)
{
	if( n && m ) {

		IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

		CDataRef    &Ref   = (CDataRef &) i;

		C3REAL       SumX  = 0;

		C3REAL       Sum2  = 0;

		C3REAL       Var   = 0;

		for( UINT j = 0; j < n; j++ ) {

			C3REAL x = C3REAL(pData->GetData(Ref.m_Ref, typeInteger, getNone));

			SumX += x;

			Sum2 += x * x;

			Ref.x.m_Array++;
		}

		Var += (n * Sum2 - SumX * SumX);

		Var /= (n * m);

		return R2I(sqrt(Var));
	}

	return 0;
}

DWORD CDataServer::SumReal(DWORD i, UINT n)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	CDataRef    &Ref   = (CDataRef &) i;

	C3REAL       Sum   = 0;

	for( UINT j = 0; j < n; j++ ) {

		Sum += I2R(pData->GetData(Ref.m_Ref, typeReal, getNone));

		Ref.x.m_Array++;
	}

	return R2I(Sum);
}

DWORD CDataServer::MeanReal(DWORD i, UINT n)
{
	return R2I(n ? I2R(SumReal(i, n)) / n : 0);
}

DWORD CDataServer::StdDevReal(DWORD i, UINT n)
{
	return DevReal(i, n, n - 1);
}

DWORD CDataServer::PopDevReal(DWORD i, UINT n)
{
	return DevReal(i, n, n);
}

DWORD CDataServer::DevReal(DWORD i, UINT n, UINT m)
{
	if( n && m ) {

		IDataServer * pData = CCommsSystem::m_pThis->GetDataServer();

		CDataRef    & Ref   = (CDataRef &) i;

		C3REAL        SumX  = 0;

		C3REAL        Sum2  = 0;

		C3REAL        Var   = 0;

		for( UINT j = 0; j < n; j++ ) {

			C3REAL x = I2R(pData->GetData(Ref.m_Ref, typeReal, getNone));

			SumX += x;

			Sum2 += x * x;

			Ref.x.m_Array++;
		}

		Var += (n * Sum2 - SumX * SumX);

		Var /= (n * m);

		return R2I(sqrt(Var));
	}

	return 0;
}

// String Functions

C3INT CDataServer::Len(PUTF p)
{
	UINT uLen = wstrlen(p);

	Free(p);

	return uLen;
}

PUTF CDataServer::Left(PUTF p, UINT n)
{
	UINT uLen = wstrlen(p);

	if( n < uLen ) {

		p[n] = 0;
	}

	return p;
}

PUTF CDataServer::Right(PUTF p, UINT n)
{
	UINT uLen = wstrlen(p);

	if( n < uLen ) {

		PUTF pWork = wstrdup(p + uLen - n);

		Free(p);

		return pWork;
	}

	return p;
}

PUTF CDataServer::Mid(PUTF p, UINT s, UINT n)
{
	UINT uLen = wstrlen(p);

	if( s < uLen ) {

		if( s + n < uLen ) {

			p[s + n] = 0;
		}

		PUTF pWork = wstrdup(p + s);

		Free(p);

		return pWork;
	}

	PUTF pWork = wstrdup("");

	Free(p);

	return pWork;
}

C3INT CDataServer::Find(PUTF p, UINT c, UINT s)
{
	PUTF pSrc  = p;

	UINT uPos  = 0;

	PUTF pFind;

	for( ;;) {

		if( !c || !(pFind = wstrchr(pSrc, c)) ) {

			uPos = NOTHING;

			break;
		}

		uPos = pFind - p;

		pSrc = pFind + 1;

		if( s ) {

			s--;

			continue;
		}

		break;
	}

	Free(p);

	return uPos;
}

PUTF CDataServer::Strip(PUTF p, UINT t)
{
	PUTF  pText  = p;

	PUTF  pCopy  = p;

	WCHAR cTarg  = t;

	UINT  uCount = wstrlen(p) + 1;

	while( uCount-- ) {

		if( cTarg != *pText ) {

			*pCopy++ = *pText;
		}

		pText++;
	}

	return p;
}

// Data Conversion

C3INT CDataServer::TextToInt(PUTF p, UINT r)
{
	PUTF s = p;

	for( ;;) {

		switch( *s ) {

			case spaceNormal:
			case spaceNoBreak:
			case spaceFigure:
			case spaceHair:
			case spaceNarrow:

				s++;

				continue;
		}

		break;
	}

	for( int n = 0; s[n]; n++ ) {

		if( s[n] >= digitFixed && s[n] <= digitFixed + 9 ) {

			s[n] -= digitFixed;

			s[n] += digitSimple;
		}
	}

	C3INT d = strtol(UniConvert(s), NULL, r);

	Free(p);

	return d;
}

DWORD CDataServer::TextToReal(PUTF p)
{
	PUTF s = p;

	for( ;;) {

		switch( *s ) {

			case spaceNormal:
			case spaceNoBreak:
			case spaceFigure:
			case spaceHair:
			case spaceNarrow:

				s++;

				continue;
		}

		break;
	}

	for( int n = 0; s[n]; n++ ) {

		if( s[n] >= digitFixed && s[n] <= digitFixed + 9 ) {

			s[n] -= digitFixed;

			s[n] += digitSimple;
		}
	}

	C3REAL d = strtod(UniConvert(s), NULL);

	Free(p);

	return R2I(d);
}

void CDataServer::TextToR64(PUTF p, C3INT r)
{
	PUTF s = p;

	for( ;;) {

		switch( *s ) {

			case spaceNormal:
			case spaceNoBreak:
			case spaceFigure:
			case spaceHair:
			case spaceNarrow:

				s++;

				continue;
		}

		break;
	}

	CDataRef &Ref = (CDataRef &) r;

	CReal64 u;

	u.d = strtod(UniConvert(s), NULL);

	m_pThis->SetData(Ref.m_Ref, typeInteger, setDirect | setForce, u.i[1]);

	Ref.x.m_Array++;

	m_pThis->SetData(Ref.m_Ref, typeInteger, setDirect | setForce, u.i[0]);

	Free(p);
}

void CDataServer::TextToL64(PUTF p, C3INT l, C3INT r)
{
	PUTF s = p;

	for( ;;) {

		switch( *s ) {

			case spaceNormal:
			case spaceNoBreak:
			case spaceFigure:
			case spaceHair:
			case spaceNarrow:

				s++;

				continue;
		}

		break;
	}

	CDataRef &Ref = (CDataRef &) l;

	CLong64 u;

	u.l = (INT64) strtoull(UniConvert(s), NULL, r);

	m_pThis->SetData(Ref.m_Ref, typeInteger, setDirect | setForce, u.i[1]);

	Ref.x.m_Array++;

	m_pThis->SetData(Ref.m_Ref, typeInteger, setDirect | setForce, u.i[0]);

	Free(p);
}

DWORD CDataServer::TextToAddr(PUTF p)
{
	CString t = UniConvert(p);

	BYTE b1 = atoi(t.StripToken('.'));
	BYTE b2 = atoi(t.StripToken('.'));
	BYTE b3 = atoi(t.StripToken('.'));
	BYTE b4 = atoi(t.StripToken('.'));

	DWORD d = MAKELONG(MAKEWORD(b4, b3), MAKEWORD(b2, b1));

	Free(p);

	return d;
}

void CDataServer::TextToData(PUTF p, DWORD r, UINT n)
{
	CDataRef &Ref  = (CDataRef &) r;

	while( n-- ) {

		m_pThis->SetData(Ref.m_Ref, typeInteger, setDirect, *p);

		if( *p ) p++;

		Ref.x.m_Array++;
	}

	Free(p);
}

PUTF CDataServer::IntToText(C3INT d, UINT r, UINT c)
{
	PUTF pData = PUTF(Malloc(2 * (c + 1)));

	if( r ) {

		pData[c] = 0;

		while( c-- ) {

			pData[c] = "0123456789ABCDEF"[d % r];

			d /= r;
		}
	}
	else
		pData[0] = 0;

	return pData;
}

PUTF CDataServer::RealToText(C3REAL d, UINT n)
{
	return wstrdup(CPrintf("%0.*f", d, n));
}

PUTF CDataServer::AddrToText(DWORD d)
{
	BYTE b1 = PBYTE(&d)[0];
	BYTE b2 = PBYTE(&d)[1];
	BYTE b3 = PBYTE(&d)[2];
	BYTE b4 = PBYTE(&d)[3];

	return wstrdup(CPrintf("%u.%u.%u.%u", b1, b2, b3, b4));
}

PUTF CDataServer::DataToText(DWORD r, UINT n)
{
	IDataServer * pData = CCommsSystem::m_pThis->GetDataServer();

	PUTF          pText = PUTF(Malloc(2 * (n + 1)));

	CDataRef    & Ref   = (CDataRef &) r;

	DWORD         Data  = 0;

	UINT i;

	for( i = 0; i < n; i++ ) {

		if( !(i % 4) ) {

			Data = pData->GetData(Ref.m_Ref, typeInteger, getNone);

			Ref.x.m_Array++;
		}

		char cData = (Data & 0xFF);

		pText[i]   = cData;

		Data     >>= 8;
	}

	pText[i] = 0;

	return pText;
}

PUTF CDataServer::AsTextInt(C3INT d)
{
	char s[32];

	SPrintf(s, "%d", d);

	return wstrdup(s);
}

PUTF CDataServer::AsTextReal(C3REAL d)
{
	char s[32];

	PTXT w = s;

	if( d < 0 ) {

		// NOTE : gcvt is not reliable between platforms as
		// to how it treats negative numbers, so we do the
		// conversion ourselves to maintain the same ouptut.

		d    = -d;

		*w++ = '-';
	}

	gcvt(d, 5, w);

	char *p = strchr(w, 'e');

	if( p ) {

		p[0] = 'E';

		if( p[2] == '0' ) {

			if( p[3] == '0' ) {

				p[2] = p[4];
				p[3] = 0;
			}
			else {
				p[2] = p[3];
				p[3] = p[4];
				p[4] = 0;
			}
		}
	}

	return wstrdup(s);
}

PUTF CDataServer::AsTextR64(C3INT d)
{
	return AsTextR64UserWidth(wstrdup(L"10"), d);
}

PUTF CDataServer::AsTextL64(C3INT l, C3INT r, C3INT c)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	CLong64 u;

	CDataRef &Ref = (CDataRef &) l;

	u.i[1] = pData->GetData(Ref.m_Ref, typeInteger, getNone);

	Ref.x.m_Array++;

	u.i[0] = pData->GetData(Ref.m_Ref, typeInteger, getNone);

	CString s;

	if( r ) {

		if( r == 10 ) {

			INT64 d   = (INT64) u.l;

			BOOL fNeg = BOOL(d >> 63);

			if( fNeg ) {

				d =~d;

				d++;

				c++;
			}

			PUTF t = PUTF(Malloc(2 * (c + 1)));

			t[c]   = 0;

			while( c-- ) {

				if( c == 0 && fNeg ) {

					t[0] = '-';
				}
				else {
					t[c] = "0123456789ABCDEF"[d % r];

					d /= r;
				}
			}

			return t;
		}

		else if( r == 16 ) {

			UINT Format[2] = { UINT(c - 8), UINT(min(8, c)) };

			if( u.i[0] > 0xFFFF || c > 8 ) {

				s.Printf("%*.*X%*.*X", Format[0], Format[0], u.i[1],
					 Format[1], Format[1], u.i[0]);
			}
			else {
				s.Printf("%*.*X", Format[1], Format[1], u.i[0]);
			}
		}
	}

	return wstrdup(s);
}

PUTF  CDataServer::AsTextR64UserWidth(PUTF width, C3INT d)
{
	CString a = UniConvert(width);

	Free(width);

	BYTE w = atoi(a.StripToken('.'));

	MakeMin(w, 64);

	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	char s[64];

	CReal64 r;

	PTXT result = s;

	CDataRef &Ref = (CDataRef &) d;

	r.i[1] = pData->GetData(Ref.m_Ref, typeInteger, getNone);

	Ref.x.m_Array++;

	r.i[0] = pData->GetData(Ref.m_Ref, typeInteger, getNone);

	if( r.d < 0 ) {

		// note : gcvt is not reliable between platforms as
		// to how it treats negative numbers, so we do the
		// conversion ourselves to maintain the same ouptut.

		r.d    = -r.d;

		*result++ = '-';
	}

	gcvt(r.d, w, result);

	char *e = strchr(result, 'e');

	if( e ) {

		e[0] = 'E';

		if( e[2] == '0' ) {

			if( e[3] == '0' ) {

				e[2] = e[4];
				e[3] = 0;
			}
			else {
				e[2] = e[3];
				e[3] = e[4];
				e[4] = 0;
			}
		}
	}

	return wstrdup(s);
}

PUTF  CDataServer::AsTextR64WithFormat(PUTF format, C3INT d)
{
	CString a = UniConvert(format);

	if( a.Find('.') == NOTHING ) {

		return AsTextR64UserWidth(format, d);
	}

	Free(format);

	BYTE w = atoi(a.StripToken('.'));	// width
	BYTE p = atoi(a.StripToken('.'));	// precision
	BYTE f = atoi(a.StripToken('.'));	// flags

	MakeMin(w, 64);

	MakeMin(p, 64);

	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	CReal64 r;

	CDataRef &Ref = (CDataRef &) d;

	r.i[1] = pData->GetData(Ref.m_Ref, typeInteger, getNone);

	Ref.x.m_Array++;

	r.i[0] = pData->GetData(Ref.m_Ref, typeInteger, getNone);

	int  dec = 0;
	int sign = 0;

	PTXT Text = ecvt(r.d, w, &dec, &sign);

	CString s = "NAN";

	if( Text ) {

		if( dec < strlen(Text) - p ) {

			Text = fcvt(r.d, p, &dec, &sign);
		}

		if( Text ) {

			s = Text;

			if( f & 1 ) {

				while( s.GetLength() < w ) {

					s.Insert(0, '0');

					dec++;
				}
			}

			BOOL fExp = dec < 0 || dec > w;

			if( fExp ) {

				s.Insert(s.GetLength() - p, '.');

				dec -= w - p;
			}
			else {
				s.Insert(dec, '.');
			}

			if( f & 2 ) {

				UINT uLast = s.GetLength() - 1;

				while( s.GetAt(uLast) == '0' && s.GetAt(uLast - 1) != '.' ) {

					s = s.Mid(0, uLast);

					uLast--;
				}
			}

			if( fExp ) {

				s += "E% +d";

				s.Printf(s, dec);
			}

			if( sign ) {

				s.Insert(0, '-');
			}
		}
	}

	return wstrdup(s);
}


PUTF CDataServer::DecToTextInt(C3INT d, C3INT s, C3INT b, C3INT a, C3INT l, C3INT g)
{
	UINT Lead[] = { 1, 0, 2 };

	CDispFormatNumber Form;

	Form.m_Radix   = 0;
	Form.m_Before  = max(1, b);
	Form.m_After   = a;
	Form.m_Leading = (l < 3) ? Lead[l] : l;
	Form.m_Group   = g;
	Form.m_Signed  = s;

	return wstrdup(Form.Format(d, typeInteger, fmtPad | fmtANSI));
}

PUTF CDataServer::DecToTextReal(C3REAL d, C3INT s, C3INT b, C3INT a, C3INT l, C3INT g)
{
	UINT Lead[] = { 1, 0, 2 };

	CDispFormatNumber Form;

	Form.m_Radix   = 0;
	Form.m_Before  = max(1, b);
	Form.m_After   = a;
	Form.m_Leading = (l < 3) ? Lead[l] : l;
	Form.m_Group   = g;
	Form.m_Signed  = s;

	return wstrdup(Form.Format(R2I(d), typeReal, fmtPad | fmtANSI));
}

DWORD CDataServer::R64ToReal(C3INT a)
{
	CDataRef    &Ref   = (CDataRef &) a;

	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	CReal64 r;

	r.i[1] = pData->GetData(Ref.m_Ref, typeInteger, getNone);

	Ref.x.m_Array++;

	r.i[0] = pData->GetData(Ref.m_Ref, typeInteger, getNone);

	return R2I(C3REAL(r.d));
}

C3INT CDataServer::R64ToInt(C3INT a)
{
	CDataRef &Ref = (CDataRef &) a;

	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	CReal64 r;

	r.i[1] = pData->GetData(Ref.m_Ref, typeInteger, getNone);

	Ref.x.m_Array++;

	r.i[0] = pData->GetData(Ref.m_Ref, typeInteger, getNone);

	return C3INT(r.d);
}

void  CDataServer::RealToR64(C3INT res, C3REAL a)
{
	CDataRef &Ref = (CDataRef &) res;

	CReal64 r;

	r.d = (double) a;

	m_pThis->SetData(Ref.m_Ref, typeInteger, setDirect, r.i[1]);

	Ref.x.m_Array++;

	m_pThis->SetData(Ref.m_Ref, typeInteger, setDirect, r.i[0]);
}

void  CDataServer::IntToR64(C3INT res, C3INT a)
{
	CDataRef &Ref = (CDataRef &) res;

	CReal64 r;

	r.d = (double) a;

	m_pThis->SetData(Ref.m_Ref, typeInteger, setDirect, r.i[1]);

	Ref.x.m_Array++;

	m_pThis->SetData(Ref.m_Ref, typeInteger, setDirect, r.i[0]);
}

// Time and Date

C3INT CDataServer::Date(C3INT y, C3INT m, C3INT d)
{
	if( d >= 1 && d <= 31 ) {

		return ::Date(y, m, d);
	}

	return 0;
}

C3INT CDataServer::Time(C3INT h, C3INT m, C3INT s)
{
	return ::Time(h % 24, m % 60, s % 60);
}

C3INT CDataServer::GetNowTime(void)
{
	DWORD t = GetNow();

	t = t % (60 * 60 * 24);

	return t;
}

C3INT CDataServer::GetNowDate(void)
{
	DWORD t = GetNow();

	t = t - t % (60 * 60 * 24);

	return t;
}

C3INT CDataServer::SetNow(C3INT t)
{
	CTime Time;

	Time.uSeconds = GetSec(t);
	Time.uMinutes = GetMin(t);
	Time.uHours   = GetHour(t);
	Time.uDate    = GetDate(t);
	Time.uMonth   = GetMonth(t);
	Time.uYear    = GetYear(t);

	if( SetTime(Time) ) {

		SystemTimeChange();

		return TRUE;
	}

	return FALSE;
}

// Disk Management

C3INT CDataServer::DriveStatus(void)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		if( m_pFileUtils ) {

			return m_pFileUtils->GetDiskStatus('C');
		}
	}

	return 0;
}

void CDataServer::DriveEject(void)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		if( m_pFileUtils ) {

			m_pFileUtils->EjectDisk('C');
		}
	}
}

C3INT CDataServer::DriveFormat(void)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		if( m_pFileUtils ) {

			return m_pFileUtils->FormatDisk('C');
		}
	}

	return 0;
}

C3INT CDataServer::DriveStatusEx(UINT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		if( m_pFileUtils ) {

			CHAR cDrive = n ? n : 'C';

			return m_pFileUtils->GetDiskStatus(cDrive);
		}
	}

	return 0;
}

void CDataServer::DriveEjectEx(UINT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		if( m_pFileUtils ) {

			CHAR cDrive = n ? n : 'C';

			m_pFileUtils->EjectDisk(cDrive);
		}
	}
}

C3INT CDataServer::DriveFormatEx(UINT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		if( m_pFileUtils ) {

			CHAR cDrive = n ? n : 'C';

			return m_pFileUtils->FormatDisk(cDrive);
		}
	}

	return 0;
}

C3INT CDataServer::GetDiskFreeBytes(C3INT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		if( m_pFileUtils ) {

			CHAR  cDrive = n ? n : 'C';

			C3INT nKilos = C3INT(m_pFileUtils->GetDiskFree(cDrive) / 1024);

			return nKilos;
		}
	}

	return 0;
}

C3INT CDataServer::GetDiskFreePercent(C3INT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		if( m_pFileUtils ) {

			CHAR   cDrive = n ? n : 'C';

			UINT64 f = m_pFileUtils->GetDiskFree(cDrive);

			UINT64 s = m_pFileUtils->GetDiskSize(cDrive);

			C3INT  r = s ? C3INT((f * 100.0 + 50.0) / s) : 0;

			return r;
		}
	}

	return 0;
}

C3INT CDataServer::GetDiskSizeBytes(C3INT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		if( m_pFileUtils ) {

			CHAR  cDrive = n ? n : 'C';

			C3INT nKilos = C3INT(m_pFileUtils->GetDiskSize(cDrive) / 1024);

			return nKilos;
		}
	}

	return 0;
}

void CDataServer::MountCompactFlash(C3INT m)
{
}

// File Management

PUTF CDataServer::FindFileFirst(PUTF n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		char a[MAX_PATH];

		AdjustName(a, n);

		m_Find.Empty();

		if( m_Find.Scan(a) ) {

			m_iFind = 0;

			return FindFileNext();
		}
	}
	else
		free(n);

	return wstrdup("");
}

PUTF CDataServer::FindFileNext(void)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		if( m_Find ) {

			PUTF p = wstrdup(m_Find[m_iFind++]->d_name);

			if( m_iFind == m_Find.GetCount() ) {

				m_Find.Empty();
			}

			return p;
		}
	}

	return wstrdup("");
}

C3INT CDataServer::CreateDirectory(PUTF n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		char a[MAX_PATH];

		AdjustName(a, n);

		if( mkdir(a, 0) != -1 ) {

			return 1;
		}
	}
	else
		free(n);

	return 0;
}

C3INT CDataServer::DeleteDirectory(PUTF n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		char a[MAX_PATH];

		AdjustName(a, n);

		if( rmdir(a) != -1 ) {

			return 1;
		}
	}
	else
		free(n);

	return 0;
}

C3INT CDataServer::KillDirectory(PUTF n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		char a[MAX_PATH];

		AdjustName(a, n);

		CAutoDirentList List;

		if( List.Scan(a) ) {

			for( UINT i = 0; i < List.GetCount(); i++ ) {

				struct dirent const *p = List[i];

				if( p->d_type == DT_DIR ) {

					if( p->d_name[0] != '.' ) {

						char b[MAX_PATH];

						PathMakeAbsolute(b, a);

						PathAppend(b, p->d_name);

						if( !KillDirectory(wstrdup(b)) ) {

							return 0;
						}
					}

					continue;
				}

				if( p->d_type == DT_REG ) {

					char b[MAX_PATH];

					PathMakeAbsolute(b, a);

					PathAppend(b, p->d_name);

					if( unlink(b) == -1 ) {

						return 0;
					}

					continue;
				}
			}
		}

		if( rmdir(a) != -1 ) {

			return 1;
		}
	}
	else
		free(n);

	return 0;
}

C3INT CDataServer::CopyFiles(PUTF s, PUTF d, UINT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		char da[MAX_PATH];

		char sa[MAX_PATH];

		AdjustName(sa, s);

		AdjustName(da, d);

		CAutoObject<IFileCopier> Copier("fs.copier", AfxAeonIID(IFileCopier));

		if( Copier ) {

			mapname(da, false);

			mapname(sa, false);

			UINT         uFlags  = 0;

			if( n & 1 ) uFlags |= copyRecursive;

			if( n & 2 ) uFlags |= copyOverwrite;

			if( n & 4 ) uFlags |= copyAll;

			else	    uFlags |= copyNew;

			return Copier->CopyDir(sa, da, uFlags);
		}

		return 0;
	}
	else {
		free(s);

		free(d);
	}

	return 0;
}

C3INT CDataServer::MoveFiles(PUTF s, PUTF d, UINT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		char da[MAX_PATH];

		char sa[MAX_PATH];

		AdjustName(sa, s);

		AdjustName(da, d);

		CAutoObject<IFileCopier> Copier("fs.copier", AfxAeonIID(IFileCopier));

		if( Copier ) {

			mapname(da, false);

			mapname(sa, false);

			UINT         uFlags  = 0;

			if( n & 1 ) uFlags |= copyRecursive;

			if( n & 2 ) uFlags |= copyOverwrite;

			if( n & 4 ) uFlags |= copyAll;

			else	    uFlags |= copyNew;

			return Copier->MoveDir(sa, da, uFlags);
		}

		return 0;
	}
	else {
		free(s);

		free(d);
	}

	return 0;
}

PUTF CDataServer::GetAutoCopyStatusText(void)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		#if 0 // !!!
		if( CMemoryStickItem::m_pThis ) {

			return CMemoryStickItem::m_pThis->GetStatusText();
}
		#endif
		}

	return wstrdup(L"None");
	}

UINT CDataServer::GetAutoCopyStatusCode(void)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		#if 0 // !!!
		if( CMemoryStickItem::m_pThis ) {

			return CMemoryStickItem::m_pThis->GetStatusCode();
}
		#endif
		}

	return 0;
	}

C3INT CDataServer::CreateFile(PUTF n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		char a[MAX_PATH];

		AdjustName(a, n);

		CAutoFile File;

		if( !File.Open(a, "r") ) {

			if( File.Open(a, "a") ) {

				return 1;
			}
		}

		return 0;
	}
	else
		free(n);

	return 0;
}

C3INT CDataServer::RenameFile(C3INT f, PUTF n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		char a[MAX_PATH];

		AdjustName(a, n);

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			CFile &File = m_File[f];

			if( File.m_nFile != -1 ) {

				off_t Off = lseek(File.m_nFile, 0, SEEK_CUR);

				close(File.m_nFile);

				if( rename(File.m_sName, a) != -1 ) {

					PathMakeAbsolute(File.m_sName, a);

					if( (File.m_nFile = open(File.m_sName, File.m_nFlags)) != -1 ) {

						lseek(File.m_nFile, Off, SEEK_SET);

						return 1;
					}
				}

				File.m_nFile = open(File.m_sName, File.m_nFlags);

				lseek(File.m_nFile, Off, SEEK_SET);
			}
		}
	}
	else
		free(n);

	return 0;
}

C3INT CDataServer::DeleteFile(C3INT f)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			CFile &File = m_File[f];

			if( File.m_nFile != -1 ) {

				close(File.m_nFile);

				File.m_nFile = -1;

				if( unlink(File.m_sName) != -1 ) {

					return 1;
				}
			}
		}
	}

	return 0;
}

C3INT CDataServer::OpenFile(PUTF n, C3INT m)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		char a[MAX_PATH];

		AdjustName(a, n);

		CAutoLock Lock(m_pLock);

		for( UINT f = 0; f < elements(m_File); f++ ) {

			CFile &File = m_File[f];

			if( File.m_nFile == -1 ) {

				CFilename Name(a);

				// TODO -- We used to use cached files!!!!

				File.m_nFlags = 0;

				if( m == 0 ) {

					File.m_nFlags |= O_RDONLY;
				}

				if( m >= 1 ) {

					File.m_nFlags |= O_RDWR;
				}

				if( m == 2 ) {

					File.m_nFlags |= O_APPEND;
				}

				if( (File.m_nFile = open(a, File.m_nFlags)) != -1 ) {

					PathMakeAbsolute(File.m_sName, a);

					return 1 + f;
				}

				break;
			}
		}
	}
	else
		free(n);

	return 0;
}

void CDataServer::CloseFile(C3INT f)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			INT &nFile = m_File[f].m_nFile;

			if( nFile != -1 ) {

				close(nFile);

				nFile = -1;

				return;
			}
		}
	}
}

C3INT CDataServer::FileSeek(C3INT f, C3INT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			INT nFile = m_File[f].m_nFile;

			if( nFile != -1 ) {

				INT nSize = lseek(nFile, 0, SEEK_END);

				MakeMin(n, nSize);

				lseek(nFile, n, SEEK_SET);

				return 1;
			}
		}

		return -1;
	}

	return 0;
}

C3INT CDataServer::FileTell(C3INT f)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			INT nFile = m_File[f].m_nFile;

			if( nFile != -1 ) {

				return lseek(nFile, 0, SEEK_CUR);
			}
		}

		return -1;
	}

	return 0;
}

PUTF CDataServer::FileReadLine(C3INT f)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			INT nFile = m_File[f].m_nFile;

			if( nFile != -1 ) {

				UINT uSize = 512;

				CAutoArray<CHAR> Data(uSize);

				UINT uRead = read(nFile, PCHAR(Data), uSize - 1);

				if( uRead != -1 ) {

					Data[uRead] = 0;

					PTXT pFind  = strstr(PCHAR(Data), "\r\n");

					if( pFind ) {

						int nBack = uRead - ((pFind + 2) - PCHAR(Data));

						lseek(nFile, -nBack, SEEK_CUR);

						*pFind = 0;

						return wstrdup(PCHAR(Data));
					}
				}
			}
		}
	}

	return wstrdup("");
}

PUTF CDataServer::FileRead(C3INT f, C3INT c)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			INT nFile = m_File[f].m_nFile;

			if( nFile != -1 ) {

				UINT uSize = c;

				MakeMin(uSize, 512);

				if( uSize ) {

					CAutoArray<CHAR> Data(uSize);

					UINT uRead = read(nFile, PCHAR(Data), uSize - 1);

					if( uRead != -1 ) {

						Data[uRead] = 0;

						return wstrdup(PCHAR(Data));
					}
				}
			}
		}
	}

	return wstrdup("");
}

C3INT CDataServer::GetFileByte(C3INT f)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			INT nFile = m_File[f].m_nFile;

			if( nFile != -1 ) {

				BYTE b = 0;

				if( read(nFile, &b, 1) == 1 ) {

					return b;
				}
			}
		}

		return -1;
	}

	return 0;
}

C3INT CDataServer::GetFileData(C3INT f, DWORD v, C3INT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			INT nFile = m_File[f].m_nFile;

			if( nFile != -1 ) {

				CAutoArray<BYTE> Buff(n);

				IDataServer    * pData = CCommsSystem::m_pThis->GetDataServer();

				CDataRef       & Ref   = (CDataRef &) v;

				if( (n = read(nFile, PBYTE(Buff), n)) != -1 ) {

					for( C3INT i = 0; i < n; i++ ) {

						pData->SetData(Ref.m_Ref, typeInteger, setDirect, Buff[i]);

						Ref.x.m_Array++;
					}
				}

				return n;
			}
		}
	}

	return 0;
}

C3INT CDataServer::FileWrite(C3INT f, PUTF n)
{
	return FileWriteHelp(f, n, FALSE);
}

C3INT CDataServer::FileWriteLine(C3INT f, PUTF n)
{
	return FileWriteHelp(f, n, TRUE);
}

C3INT CDataServer::FileWriteHelp(C3INT f, PUTF n, BOOL fEnd)
{
	C3INT w = 0;

	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			INT nFile = m_File[f].m_nFile;

			if( nFile != -1 ) {

				UINT uSize = wstrlen(n) + (fEnd ? 2 : 0);

				CAutoArray<CHAR> Data(uSize + 1);

				UINT i;

				for( i = 0; (Data[i] = n[i]); i++ );

				if( fEnd ) {

					Data[i++] = '\r';
					Data[i++] = '\n';
				}

				w = write(nFile, PCHAR(Data), uSize);
			}
		}
	}

	Free(n);

	return w;
}

C3INT CDataServer::PutFileByte(C3INT f, C3INT d)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			INT nFile = m_File[f].m_nFile;

			if( nFile != -1 ) {

				BYTE b = BYTE(d);

				return write(nFile, &b, 1);
			}

			return -1;
		}
	}

	return -1;
}

C3INT CDataServer::PutFileData(C3INT f, DWORD v, C3INT n)
{
	if( WhoHasFeature(rfFileSystemApi) ) {

		CAutoLock Lock(m_pLock);

		if( UINT(--f) < elements(m_File) ) {

			INT nFile = m_File[f].m_nFile;

			if( nFile != -1 ) {

				CAutoArray<BYTE> Buff(n);

				IDataServer    * pData = CCommsSystem::m_pThis->GetDataServer();

				CDataRef       & Ref   = (CDataRef &) v;

				for( C3INT i = 0; i < n; i++ ) {

					DWORD Data = pData->GetData(Ref.m_Ref, typeInteger, getNone);

					Buff[i]    = BYTE(Data);

					Ref.x.m_Array++;
				}

				return write(nFile, PBYTE(Buff), n);
			}
		}
	}

	return 0;
}

void CDataServer::AdjustName(PTXT d, PUTF n)
{
	for( UINT i = 0; (d[i] = n[i]); i++ ) {

		if( d[i] == '/' ) {

			d[i] = '\\';
		}
	}

	Free(n);
}

void CDataServer::RemoteName(PTXT d, PUTF n)
{
	for( UINT i = 0; (d[i] = n[i]); i++ ) {

		if( d[i] == '\\' ) {

			d[i] = '/';
		}
	}

	Free(n);
}

C3INT CDataServer::IsBatchNameValid(PUTF n)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		char a[MAX_PATH];

		char b[MAX_PATH];

		AdjustName(a, n);

		if( getcwd(b, MAX_PATH) ) {

			CString Path("%c:\\BATCH\\");

			Path.Printf(Path, CDataLogger::m_pThis->m_BatchDrive + 'C');

			Path.Append(a);

			if( chdir(PCTXT(Path)) ) {

				chdir(b);

				return FALSE;
			}

			return TRUE;
		}
	}
	else
		free(n);

	return 0;
}

C3INT CDataServer::IsBatchNameValidEx(C3INT s, PUTF n)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		char a[MAX_PATH];

		char b[MAX_PATH];

		AdjustName(a, n);

		if( getcwd(b, MAX_PATH) ) {

			CString Path("%c:\\BATCH\\");

			Path.Printf(Path, CDataLogger::m_pThis->m_BatchDrive + 'C');

			if( CDataLogger::m_pThis->m_EnableSets || s ) {

				Path.AppendPrintf("Set%u\\", 1 + s);
			}

			Path.Append(a);

			if( chdir(PCTXT(Path)) ) {

				chdir(b);

				return FALSE;
			}

			return TRUE;
		}
	}
	else
		free(n);

	return 0;
}

// Mail Support

void CDataServer::SendMail(UINT r, PUTF s, PUTF b)
{
	if( g_pServiceMail ) {

		g_pServiceMail->SendMail(r,
					 UniConvert(s),
					 UniConvert(b)
		);
	}

	Free(s);

	Free(b);
}

void CDataServer::SendFile(UINT r, PUTF f)
{
	if( g_pServiceMail ) {

		char a[MAX_PATH];

		AdjustName(a, f);

		g_pServiceMail->SendFile(r,
					 CFilename(a)
		);

		return;
	}

	Free(f);
}

void CDataServer::SendFileEx(UINT r, PUTF f, PUTF s, DWORD g)
{
	if( g_pServiceMail ) {

		char a[MAX_PATH];

		AdjustName(a, f);

		g_pServiceMail->SendFile(r,
					 CFilename(a),
					 UniConvert(s),
					 g
		);

		return;
	}

	Free(s);

	Free(f);
}

void CDataServer::SendMailTo(PUTF r, PUTF s, PUTF b)
{
	if( g_pServiceMail ) {

		g_pServiceMail->SendMail(UniConvert(r),
					 UniConvert(s),
					 UniConvert(b)
		);
	}

	Free(r);

	Free(s);

	Free(b);
}

void CDataServer::SendFileTo(PUTF r, PUTF f)
{
	if( g_pServiceMail ) {

		char a[MAX_PATH];

		AdjustName(a, f);

		g_pServiceMail->SendFile(UniConvert(r),
					 CFilename(a)
		);

		Free(r);

		return;
	}

	Free(r);

	Free(f);
}

void CDataServer::SendMailToAck(PUTF r, PUTF s, PUTF b, DWORD d)
{
	if( g_pServiceMail ) {

		g_pServiceMail->SendMail(UniConvert(r),
					 UniConvert(s),
					 UniConvert(b),
					 d
		);
	}

	Free(r);

	Free(s);

	Free(b);
}

void CDataServer::SendFileToAck(PUTF r, PUTF f, DWORD d)
{
	if( g_pServiceMail ) {

		char a[MAX_PATH];

		AdjustName(a, f);

		g_pServiceMail->SendFile(UniConvert(r),
					 CFilename(a),
					 d
		);

		Free(r);

		return;
	}

	Free(r);

	Free(f);
}

// FTP Client

C3INT CDataServer::FtpPutFile(UINT s, PUTF loc, PUTF rem, C3INT del)
{
	if( g_pServiceFileSync ) {

		if( !s ) {

			char loca[MAX_PATH];

			char rema[MAX_PATH];

			AdjustName(loca, loc);

			RemoteName(rema, rem);

			return g_pServiceFileSync->PutFile(loca, rema, del);
		}
	}

	Free(loc);

	Free(rem);

	return FALSE;
}

C3INT CDataServer::FtpGetFile(UINT s, PUTF loc, PUTF rem, C3INT del)
{
	if( g_pServiceFileSync ) {

		if( !s ) {

			char loca[MAX_PATH];

			char rema[MAX_PATH];

			AdjustName(loca, loc);

			RemoteName(rema, rem);

			return g_pServiceFileSync->GetFile(loca, rema, del);
		}
	}

	Free(loc);

	Free(rem);

	return FALSE;
}

// Batch Control

void CDataServer::NewBatch(PUTF p)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CDataLogger::m_pThis->NewBatch(0, UniConvert(p));
	}

	Free(p);
}

void CDataServer::EndBatch(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CDataLogger::m_pThis->NewBatch(0, "");
	}
}

PUTF CDataServer::GetBatch(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		return wstrdup(CDataLogger::m_pThis->GetBatch(0));
	}

	return wstrdup("");
}

void CDataServer::NewBatchEx(UINT s, PUTF p)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CDataLogger::m_pThis->NewBatch(s, UniConvert(p));
	}

	Free(p);
}

void CDataServer::EndBatchEx(UINT s)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CDataLogger::m_pThis->NewBatch(s, "");
	}
}

PUTF CDataServer::GetBatchEx(UINT s)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		return wstrdup(CDataLogger::m_pThis->GetBatch(s));
	}

	return wstrdup("");
}

// Network Status

PUTF CDataServer::GetNetId(C3INT Port)
{
	CMACAddr Mac;

	if( Port ) {

		Port -= 1;
	}

	if( NICReadMac(Port, Mac) ) {

		#if defined(AEON_ENVIRONMENT)

		return wstrdup(Mac.GetAsText());

		#else

		char s[32] = { 0 };

		Mac.AppendAsText(s);

		return wstrdup(s);

		#endif
	}

	return wstrdup(L"");
}

PUTF CDataServer::GetNetIp(C3INT Port)
{
	if( m_pNetUtils ) {

		CIPAddr Addr;

		if( !Port ) {

			Port += 1;
		}

		if( m_pNetUtils->GetInterfaceAddr(Port, Addr) ) {

			return wstrdup(Addr.GetAsText());
		}
	}

	return wstrdup(L"NO IP");
}

PUTF CDataServer::GetNetMask(C3INT Port)
{
	if( m_pNetUtils ) {

		CIPAddr Mask;

		if( !Port ) {

			Port += 1;
		}

		if( m_pNetUtils->GetInterfaceMask(Port, Mask) ) {

			return wstrdup(Mask.GetAsText());
		}
	}

	return wstrdup(L"NO MASK");
}

PUTF CDataServer::GetNetGate(C3INT Port)
{
	if( m_pNetUtils ) {

		CIPAddr Gate;

		if( !Port ) {

			Port += 1;
		}

		if( m_pNetUtils->GetInterfaceGate(Port, Gate) ) {

			return wstrdup(Gate.GetAsText());
		}
	}

	return wstrdup(L"NO GATE");
}

PUTF CDataServer::GetInterfaceStatus(C3INT Face)
{
	if( m_pNetUtils ) {

		CString Status;

		m_pNetUtils->GetInterfaceStatus(Face, Status);

		return wstrdup(Status);
	}

	return wstrdup(L"NO FACE");
}

INT CDataServer::ResolveDNS(PUTF p)
{
	if( WhoHasFeature(rfDnsApi) ) {

		CString n = UniConvert(p);

		AfxGetAutoObject(pDns, "net.dns", 0, IDnsResolver);

		if( pDns ) {

			CIpAddr ip = pDns->Resolve(n);

			Free(p);

			return MotorToHost((DWORD &) ip);
		}
	}

	Free(p);

	return 0;
}

C3INT CDataServer::NetworkPing(C3INT IP, C3INT Timeout)
{
	if( m_pNetUtils ) {

		CIPAddr Addr = HostToMotor(DWORD(IP));

		UINT    uRtt = m_pNetUtils->Ping(Addr, UINT(Timeout));

		if( uRtt < NOTHING ) {

			return 1;
		}
	}

	return 0;
}

// Network Properties

PUTF CDataServer::GetModemProperty(C3INT n, PUTF p)
{
	CString Prop(UniConvert(p));

	Free(p);

	AfxGetAutoObject(pCell, "net.cell", n, ICellStatus);

	if( pCell ) {

		CCellStatusInfo Info;

		pCell->GetCellStatus(Info);

		if( !Prop.CompareN("online") ) {

			return wstrdup(Info.m_fOnline ? "TRUE" : "FALSE");
		}

		if( !Prop.CompareN("register") ) {

			return wstrdup(Info.m_fRegister ? "TRUE" : "FALSE");
		}

		if( !Prop.CompareN("roam") ) {

			return wstrdup(Info.m_fRoam ? "TRUE" : "FALSE");
		}

		if( !Prop.CompareN("slot") ) {

			char s[32] = { 0 };

			SPrintf(s, "%u", Info.m_uSlot);

			return wstrdup(s);
		}

		if( !Prop.CompareN("service") ) {

			return wstrdup(Info.m_Service);
		}

		if( !Prop.CompareN("carrier") ) {

			return wstrdup(Info.m_Carrier);
		}

		if( !Prop.CompareN("iccid") ) {

			return wstrdup(Info.m_Iccid);
		}

		if( !Prop.CompareN("imsi") ) {

			return wstrdup(Info.m_Imsi);
		}

		if( !Prop.CompareN("addr") ) {

			return wstrdup(Info.m_Addr.GetAsText());
		}

		if( !Prop.CompareN("imei") ) {

			return wstrdup(Info.m_Imei);
		}

		if( !Prop.CompareN("state") ) {

			return wstrdup(Info.m_State);
		}

		if( !Prop.CompareN("network") ) {

			return wstrdup(Info.m_Network);
		}

		if( !Prop.CompareN("model") ) {

			return wstrdup(Info.m_Model);
		}

		if( !Prop.CompareN("version") ) {

			return wstrdup(Info.m_Version);
		}

		if( !Prop.CompareN("signal") ) {

			char s[32] = { 0 };

			SPrintf(s, "%u", Info.m_uSignal);

			return wstrdup(s);
		}

		if( !Prop.CompareN("time") ) {

			char s[32] = { 0 };

			UINT uTime = 0;

			if( Info.m_fOnline ) {

				uTime = getmonosecs() - Info.m_CTime;
			}

			SPrintf(s, "%u", uTime);

			return wstrdup(s);
		}
	}

	return wstrdup("Unknown");
}

PUTF CDataServer::GetWifiProperty(C3INT n, PUTF p)
{
	CString Prop(UniConvert(p));

	Free(p);

	AfxGetAutoObject(pWifi, "net.wifi", n, IWiFiStatus);

	if( pWifi ) {

		CWiFiStatusInfo Info;

		pWifi->GetWiFiStatus(Info);

		if( !Prop.CompareN("online") ) {

			return wstrdup(Info.m_fOnline ? "TRUE" : "FALSE");
		}

		if( !Prop.CompareN("apmode") ) {

			return wstrdup(Info.m_fApMode ? "AP" : "STATION");
		}

		if( !Prop.CompareN("channel") ) {

			char s[32] = { 0 };

			SPrintf(s, "%u", Info.m_uChannel);

			return wstrdup(s);
		}

		if( !Prop.CompareN("peermac") ) {

			return wstrdup(Info.m_PeerMac.GetAsText());
		}

		if( !Prop.CompareN("state") ) {

			return wstrdup(Info.m_State);
		}

		if( !Prop.CompareN("network") ) {

			return wstrdup(Info.m_Network);
		}

		if( !Prop.CompareN("model") ) {

			return wstrdup(Info.m_Model);
		}

		if( !Prop.CompareN("version") ) {

			return wstrdup(Info.m_Version);
		}

		if( !Prop.CompareN("signal") ) {

			char s[32] = { 0 };

			SPrintf(s, "%u", Info.m_uSignal);

			return wstrdup(s);
		}

		if( !Prop.CompareN("time") ) {

			char s[32] = { 0 };

			UINT uTime = getmonosecs() - Info.m_CTime;

			uTime = uTime - uTime % 60;

			SPrintf(s, "%u", uTime);

			return wstrdup(s);
		}
	}

	return wstrdup("Unknown");
}

DWORD CDataServer::GetLocationProperty(C3INT n, PUTF p)
{
	CString Prop(UniConvert(p));

	Free(p);

	PCSTR pName = n ? "net.location" : "c3.location";

	AfxGetAutoObject(pLocation, pName, n ? n-1 : 0, ILocationSource);

	if( pLocation ) {

		CLocationSourceInfo Info;

		pLocation->GetLocationData(Info);

		if( !Prop.CompareN("fix") ) {

			return R2I((C3REAL) Info.m_uFix);
		}

		if( !Prop.CompareN("seq") ) {

			return R2I((C3REAL) Info.m_uSeq);
		}

		if( !Prop.CompareN("lat") ) {

			return R2I(Info.m_Lat);
		}

		if( !Prop.CompareN("long") ) {

			return R2I(Info.m_Long);
		}

		if( !Prop.CompareN("alt") ) {

			return R2I(Info.m_Alt);
		}
	}

	return R2I(0.0);
}

// Tag Access

C3INT CDataServer::GetIntTag(C3INT n)
{
	CDataRef Ref;

	Ref.m_Ref = 0;

	Ref.t.m_Index = n;
	Ref.t.m_IsTag = 1;

	return m_pThis->GetData(Ref.m_Ref, typeInteger, getNone);
}

DWORD CDataServer::GetRealTag(C3INT n)
{
	CDataRef Ref;

	Ref.m_Ref = 0;

	Ref.t.m_Index = n;
	Ref.t.m_IsTag = 1;

	return m_pThis->GetData(Ref.m_Ref, typeReal, getNone);
}

PUTF CDataServer::GetStringTag(C3INT n)
{
	CDataRef Ref;

	Ref.m_Ref = 0;

	Ref.t.m_Index = n;
	Ref.t.m_IsTag = 1;

	return PUTF(m_pThis->GetData(Ref.m_Ref, typeString, getNone));
}

PUTF CDataServer::GetFormattedTag(C3INT n)
{
	CDataRef Ref;

	Ref.m_Ref = 0;

	Ref.t.m_Index = n;
	Ref.t.m_IsTag = 1;

	return PUTF(m_pThis->GetProp(Ref.m_Ref, tpAsText, typeString));
}

PUTF CDataServer::GetTagLabel(C3INT n)
{
	CDataRef Ref;

	Ref.m_Ref = 0;

	Ref.t.m_Index = n;
	Ref.t.m_IsTag = 1;

	return PUTF(m_pThis->GetProp(Ref.m_Ref, tpLabel, typeString));
}

C3INT CDataServer::FindTagIndex(PUTF p)
{
	C3INT n = m_pThis->m_pTags->FindByName(p);

	Free(p);

	return n;
}

void CDataServer::SetIntTag(C3INT n, C3INT d)
{
	if( n >= 0 && n < 32768 ) {

		CDataRef Ref;

		Ref.m_Ref = 0;

		Ref.t.m_Index = n;
		Ref.t.m_IsTag = 1;

		m_pThis->SetData(Ref.m_Ref, typeInteger, setNone, d);
	}
}

void CDataServer::SetRealTag(C3INT n, C3REAL d)
{
	if( n >= 0 && n < 32768 ) {

		CDataRef Ref;

		Ref.m_Ref = 0;

		Ref.t.m_Index = n;
		Ref.t.m_IsTag = 1;

		m_pThis->SetData(Ref.m_Ref, typeReal, setNone, R2I(d));
	}
}

void CDataServer::SetStringTag(C3INT n, PUTF p)
{
	if( n >= 0 && n < 32768 ) {

		CDataRef Ref;

		Ref.m_Ref = 0;

		Ref.t.m_Index = n;
		Ref.t.m_IsTag = 1;

		m_pThis->SetData(Ref.m_Ref, typeString, setNone, DWORD(p));
	}
}

// Comms

void CDataServer::ReadData(DWORD r, C3INT n)
{
	CDataRef &Ref = (CDataRef &) r;

	for( C3INT i = 0; i < n; i++ ) {

		m_pThis->SetScan(r, scanUser);

		Ref.x.m_Array++;
	}
}

C3INT CDataServer::WaitData(DWORD r, C3INT n, C3INT t)
{
	ReadData(r, n);

	SetTimer(t);

	while( GetTimer() ) {

		DWORD    Work = r;

		CDataRef &Ref = (CDataRef &) Work;

		CTag *   pTag = NULL;

		C3INT    i;

		if( Ref.x.m_IsTag ) {

			if( Ref.t.m_Index < 0x7F80 ) {

				pTag = m_pThis->m_pTags->GetItem(Ref.t.m_Index);
			}
		}

		for( i = 0; i < n; i++ ) {

			if( pTag && !pTag->IsAvail(Ref, availData) ) {

				break;
			}

			if( !m_pThis->IsAvail(r) ) {

				break;
			}

			Ref.x.m_Array++;
		}

		if( i < n ) {

			Sleep(5);

			continue;
		}

		return 1;
	}

	return 0;
}

void CDataServer::WriteAll(void)
{
	m_pThis->m_pTags->Force();
}

// Comms Ports

C3INT CDataServer::GetPortConfig(C3INT Port, C3INT Param)
{
	CCommsPort *pPort = m_pThis->m_pComms->FindPort(LOBYTE(Port));

	if( pPort ) {

		return pPort->GetConfig(Param);
	}

	return -1;
}

C3INT CDataServer::IsPortRemote(C3INT Port)
{
	if( WhoHasFeature(rfPortSharing) ) {

		CCommsPort *pPort = m_pThis->m_pComms->FindPort(LOBYTE(Port));

		if( pPort ) {

			return pPort->IsRemote();
		}
	}

	return 0;
}

C3INT CDataServer::DrvCtrl(C3INT Port, C3INT f, PUTF v)
{
	CCommsPort *pPort = m_pThis->m_pComms->FindPort(LOBYTE(Port));

	if( pPort ) {

		C3INT r = pPort->Control(NULL, f, UniConvert(v));

		Free(v);

		return r;
	}

	Free(v);

	return 0;
}

// Comms Devices

void CDataServer::EnableDevice(C3INT Dev)
{
	CCommsDevice *pDev = m_pThis->m_pComms->FindDevice(LOBYTE(Dev));

	if( pDev ) {

		pDev->Enable(TRUE);
	}
}

void CDataServer::DisableDevice(C3INT Dev)
{
	CCommsDevice *pDev = m_pThis->m_pComms->FindDevice(LOBYTE(Dev));

	if( pDev ) {

		pDev->Enable(FALSE);
	}
}

void CDataServer::ControlDevice(C3INT Dev, C3INT State)
{
	CCommsDevice *pDev = m_pThis->m_pComms->FindDevice(LOBYTE(Dev));

	if( pDev ) {

		pDev->Enable(State);
	}
}

C3INT CDataServer::IsDeviceOnline(C3INT Dev)
{
	CCommsDevice *pDev = m_pThis->m_pComms->FindDevice(LOBYTE(Dev));

	if( pDev ) {

		return pDev->GetError() == errorNone;
	}

	return 0;
}

C3INT CDataServer::GetDeviceStatus(C3INT Dev)
{
	CCommsDevice *pDev = m_pThis->m_pComms->FindDevice(LOBYTE(Dev));

	if( pDev ) {

		return pDev->GetStatus();
	}

	return 0;
}

C3INT CDataServer::IsWriteQueueEmpty(C3INT Dev)
{
	CCommsDevice *pDev = m_pThis->m_pComms->FindDevice(LOBYTE(Dev));

	if( pDev ) {

		return pDev->IsQueueEmpty();
	}

	return TRUE;
}

C3INT CDataServer::EmptyWriteQueue(C3INT Dev)
{
	CCommsDevice *pDev = m_pThis->m_pComms->FindDevice(LOBYTE(Dev));

	if( pDev ) {

		pDev->EmptyQueue();
	}

	return TRUE;
}

C3INT CDataServer::DevCtrl(C3INT Dev, C3INT f, PUTF v)
{
	CCommsDevice *pDev = m_pThis->m_pComms->FindDevice(LOBYTE(Dev));

	if( pDev ) {

		C3INT r = pDev->Control(f, UniConvert(v));

		Free(v);

		return r;
	}

	Free(v);

	return 0;
}

// Raw Ports

C3INT CDataServer::RawWrite(UINT n, BYTE b)
{
	ICommsRawPort *pRaw = m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pRaw ) {

		return pRaw->Write(b, FOREVER);
	}

	return 0;
}

C3INT CDataServer::RawPrint(UINT n, PUTF t)
{
	ICommsRawPort *pRaw = m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pRaw ) {

		for( UINT i = 0; t[i]; i++ ) {

			if( !pRaw->Write(t[i], FOREVER) ) {

				Free(t);

				return 0;
			}
		}

		Free(t);

		return 1;
	}

	Free(t);

	return 0;
}

C3INT CDataServer::RawPrintEx(UINT n, PUTF t)
{
	ICommsRawPort *pRaw = m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pRaw ) {

		UINT  a = wstrlen(t);

		PBYTE p = New BYTE[a];

		for( UINT i = 0; t[i]; i++ ) {

			p[i] = t[i];
		}

		BOOL r = pRaw->Write(p, a, FOREVER);

		Free(t);

		delete p;

		return r;

	}

	Free(t);

	return 0;
}

C3INT CDataServer::RawSendData(UINT Port, UINT DataStart, UINT Count)
{
	ICommsRawPort *pRaw = m_pThis->m_pComms->FindRawPort(LOBYTE(Port));

	if( pRaw ) {

		CDataRef &Ref = (CDataRef &) DataStart;

		PBYTE pData = New BYTE[Count];

		DWORD Data = 0;

		for( UINT n = 0; n < Count; n++ ) {

			Data = m_pThis->GetData(Ref.m_Ref, typeInteger, getNone);

			pData[n] = BYTE(Data);

			Ref.t.m_Array++;
		}

		UINT uRet = pRaw->Write(pData, Count, FOREVER);

		delete[] pData;

		return uRet;
	}

	return 0;
}

C3INT CDataServer::RawRead(UINT n, UINT t)
{
	ICommsRawPort *pRaw = m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pRaw ) {

		return pRaw->Read(t);
	}

	return NOTHING;
}

PUTF CDataServer::RawInput(UINT n, UINT s, UINT e, UINT t, UINT c)
{
	ICommsRawPort *pRaw = m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pRaw ) {

		BOOL fTimed = TRUE;

		BOOL fFirst = FALSE;

		BOOL fData  = !e && !c;

		UINT uCount = 0;

		if( !c ) c      = 255;

		if( !s ) fFirst = TRUE;

		if( !t ) fTimed = FALSE;

		SetTimer(t);

		PTXT pData = PTXT(Malloc(c + 1));

		pData[0]   = 0;

		for( ;;) {

			UINT uData = pRaw->Read(20);

			if( uData == NOTHING ) {

				if( fTimed && !GetTimer() ) {

					PUTF pText = fData ? wstrdup(pData) : wstrdup("");

					delete pData;

					return pText;
				}

				continue;
			}

			if( !fFirst ) {

				if( uData == s ) {

					SetTimer(t);

					fFirst = TRUE;

					continue;
				}
			}

			if( fFirst ) {

				if( uData == e ) {

					PUTF pText = wstrdup(pData);

					delete pData;

					return pText;
				}
				else {
					pData[uCount++] = char(uData);

					pData[uCount] = 0;

					if( uCount == c ) {

						PUTF pText = wstrdup(pData);

						delete pData;

						return pText;
					}

					if( !s ) {

						SetTimer(t);
					}
				}
			}
		}
	}

	return wstrdup("");
}

void CDataServer::RawClose(UINT n)
{
	ICommsRawPort *pRaw = m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pRaw ) {

		pRaw->Disconnect();
	}
}

void CDataServer::RawSetRTS(UINT n, UINT s)
{
	ICommsRawPort *pRaw = m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pRaw ) {

		pRaw->SetRTS(s);
	}
}

C3INT CDataServer::RawGetCTS(UINT n)
{
	ICommsRawPort *pRaw = m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pRaw ) {

		return pRaw->GetCTS();
	}

	return 0;
}

// CAN Raw Port

C3INT CDataServer::InitRxCAN(UINT n, C3INT id, C3INT dlc)
{
	ICommsRawCAN * pCAN = (ICommsRawCAN *) m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pCAN ) {

		if( pCAN->MakePDU(id, dlc, TRUE, TRUE) ) {

			return 1;
		}
	}

	return 0;
}

C3INT CDataServer::InitTxCAN(UINT n, C3INT id, C3INT dlc)
{
	ICommsRawCAN * pCAN = (ICommsRawCAN *) m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pCAN ) {

		if( pCAN->MakePDU(id, dlc, TRUE, FALSE) ) {

			return 1;
		}
	}

	return 0;
}

C3INT CDataServer::RxCAN(UINT n, DWORD r, C3INT id)
{
	ICommsRawCAN * pCAN = (ICommsRawCAN *) m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	CDataRef &Ref = (CDataRef &) r;

	if( pCAN ) {

		UINT uDLC = pCAN->GetDLC(id, TRUE);

		if( uDLC ) {

			uDLC = uDLC > 4 ? 2 : 1;

			PDWORD Data = PDWORD(alloca(uDLC * sizeof(DWORD)));

			if( pCAN->RxPDU(id, Data) ) {

				for( UINT u = 0; u < uDLC; u++, Ref.t.m_Array++ ) {

					m_pThis->SetData(Ref.m_Ref, typeInteger, 0, Data[u]);
				}

				return 1;
			}
		}
	}

	return 0;
}

C3INT CDataServer::TxCAN(UINT n, DWORD r, C3INT id)
{
	ICommsRawCAN * pCAN = (ICommsRawCAN *) m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	CDataRef &Ref = (CDataRef &) r;

	if( pCAN ) {

		UINT uDLC = pCAN->GetDLC(id, FALSE);

		if( uDLC < NOTHING ) {

			uDLC = uDLC > 4 ? 2 : 1;

			PDWORD Data = PDWORD(alloca(uDLC * sizeof(DWORD)));

			for( UINT u = 0; u < uDLC; u++, Ref.t.m_Array++ ) {

				Data[u] = m_pThis->GetData(Ref.m_Ref, typeInteger, 0);
			}

			if( pCAN->TxPDU(id, Data) ) {

				return 1;
			}
		}
	}

	return 0;
}

C3INT CDataServer::InitRxCANMailBox(UINT n, C3INT mb, C3INT mask, C3INT filter, C3INT dlc)
{
	ICommsRawCAN * pCAN = (ICommsRawCAN *) m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pCAN ) {

		if( pCAN->MakeRxMailBox(mb, mask, filter, dlc) ) {

			return 1;
		}
	}

	return 0;
}

C3INT CDataServer::InitTxCANMailBox(UINT n, C3INT mb, C3INT id, C3INT dlc)
{
	ICommsRawCAN * pCAN = (ICommsRawCAN *) m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pCAN ) {

		if( pCAN->MakeTxMailBox(mb, id, dlc) ) {

			return 1;
		}
	}

	return 0;
}

PUTF  CDataServer::RxCANMail(UINT n)
{
	ICommsRawCAN * pCAN = (ICommsRawCAN *) m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	if( pCAN ) {

		char sText[25] = { 0 };

		if( pCAN->RxMail(sText) ) {

			return wstrdup(sText);
		}
	}

	return wstrdup(L"");
}

C3INT CDataServer::TxCANMail(UINT n, C3INT mb, DWORD r)
{
	ICommsRawCAN * pCAN = (ICommsRawCAN *) m_pThis->m_pComms->FindRawPort(LOBYTE(n));

	CDataRef &Ref = (CDataRef &) r;

	if( pCAN ) {

		UINT uDLC = pCAN->GetMailDLC(mb);

		if( uDLC < NOTHING ) {

			uDLC = uDLC > 4 ? 2 : 1;

			PDWORD Data = PDWORD(alloca(uDLC * sizeof(DWORD)));

			for( UINT u = 0; u < uDLC; u++, Ref.t.m_Array++ ) {

				Data[u] = m_pThis->GetData(Ref.m_Ref, typeInteger, 0);
			}

			if( pCAN->TxMailBox(mb, Data) ) {

				return 1;
			}
		}
	}

	return 0;
}

// Web Server

C3INT CDataServer::GetWebParamInt(PUTF n)
{
	if( WhoHasFeature(rfWebServer) ) {

		CWebBase *pWeb = m_pThis->m_pSystem->m_pWeb;

		CString   Name = UniConvert(n);

		Free(n);

		return pWeb->GetWebParamInt(Name);
	}

	Free(n);

	return 0;
}

C3INT CDataServer::GetWebParamHex(PUTF n)
{
	if( WhoHasFeature(rfWebServer) ) {

		CWebBase *pWeb = m_pThis->m_pSystem->m_pWeb;

		CString   Name = UniConvert(n);

		Free(n);

		return pWeb->GetWebParamHex(Name);
	}

	Free(n);

	return 0;
}

PUTF CDataServer::GetWebParamStr(PUTF n)
{
	if( WhoHasFeature(rfWebServer) ) {

		CWebBase *pWeb = m_pThis->m_pSystem->m_pWeb;

		CString   Name = UniConvert(n);

		Free(n);

		return pWeb->GetWebParamStr(Name);
	}

	Free(n);

	return wstrdup(L"");
}

PUTF CDataServer::GetWebUser(C3INT n)
{
	if( WhoHasFeature(rfWebServer) ) {

		CWebBase *pWeb = m_pThis->m_pSystem->m_pWeb;

		return pWeb->GetWebUser(n);
	}

	return wstrdup(L"");
}


void CDataServer::ClearWebUsers(void)
{
	if( WhoHasFeature(rfWebServer) ) {

		CWebBase *pWeb = m_pThis->m_pSystem->m_pWeb;

		pWeb->ClearWebUsers();
	}
}

// License

PUTF CDataServer::GetLicenseState(int Type)
{
	if( g_pLicense ) {

		if( g_pLicense->Validate(Type) ) {

			return wstrdup("License Module Installed");
		}

		return wstrdup("License Module Not Found");
	}

	return wstrdup("No License Object");
}

// SQL Sync

C3INT CDataServer::ForceSQLSync(void)
{
	if( g_pServiceSqlSync ) {

		g_pServiceSqlSync->Force();

		return 1;
	}

	return 0;
}


C3INT CDataServer::IsSQLSyncRunning(void)
{
	if( g_pServiceSqlSync ) {

		return 	g_pServiceSqlSync->IsRunning();
	}

	return 0;
}

C3INT CDataServer::GetLastSQLSyncStatus(void)
{
	if( g_pServiceSqlSync ) {

		return 	g_pServiceSqlSync->GetLastStatus();
	}

	return 0;
}

C3INT CDataServer::GetLastSQLSyncTime(C3INT Type)
{
	if( g_pServiceSqlSync ) {

		return g_pServiceSqlSync->GetSyncTime(Type);
	}

	return 0;
}

// SQL

void CDataServer::RunQuery(PUTF Query)
{
	CString s = UniConvert(Query);

	Free(Query);

	if( m_pThis->m_pSystem->m_pSql ) {

		m_pThis->m_pSystem->m_pSql->UserExecuteQuery(s);
	}
}

void CDataServer::RunAllQueries(void)
{
	if( m_pThis->m_pSystem->m_pSql ) {

		m_pThis->m_pSystem->m_pSql->UserExecuteAll();
	}
}

C3INT CDataServer::GetSQLConnectionStatus(void)
{
	if( m_pThis->m_pSystem->m_pSql ) {

		return m_pThis->m_pSystem->m_pSql->GetConnectionStatus();
	}

	return 0;
}

C3INT CDataServer::GetQueryTime(PUTF Query)
{
	CString s = UniConvert(Query);

	Free(Query);

	if( m_pThis->m_pSystem->m_pSql ) {

		return m_pThis->m_pSystem->m_pSql->GetQueryLastRun(s);
	}

	return 0;
}

C3INT CDataServer::GetQueryStatus(PUTF Query)
{
	CString s = UniConvert(Query);

	Free(Query);

	if( m_pThis->m_pSystem->m_pSql ) {

		return m_pThis->m_pSystem->m_pSql->GetQueryStatus(s);
	}

	return 0;
}

// Camera Support

C3INT CDataServer::GetCameraData(C3INT Port, C3INT Camera, C3INT Param)
{
	if( WhoHasFeature(rfCamera) ) {

		IProxyCamera *pCamera = m_pThis->m_pComms->m_pCameras->FindProxy(Port);

		if( pCamera ) {

			return pCamera->GetInfo(Camera, Param);
		}
	}

	return 0;
}

C3INT CDataServer::SaveCameraSetup(C3INT Port, C3INT Camera, C3INT Index, PUTF File)
{
	if( WhoHasFeature(rfCamera) ) {

		IProxyCamera *pCamera = m_pThis->m_pComms->m_pCameras->FindProxy(Port);

		CString Name = UniConvert(File);

		if( pCamera ) {

			Free(File);

			return pCamera->SaveSetup(Camera, Index, Name);
		}
	}

	Free(File);

	return 0;
}

C3INT CDataServer::LoadCameraSetup(C3INT Port, C3INT Camera, C3INT Index, PUTF File)
{
	if( WhoHasFeature(rfCamera) ) {

		IProxyCamera *pCamera = m_pThis->m_pComms->m_pCameras->FindProxy(Port);

		CString Name = UniConvert(File);

		if( pCamera ) {

			Free(File);

			return pCamera->LoadSetup(Camera, Index, Name);
		}
	}

	Free(File);

	return 0;
}

C3INT CDataServer::UseCameraSetup(C3INT Port, C3INT Camera, C3INT Index)
{
	if( WhoHasFeature(rfCamera) ) {

		IProxyCamera *pCamera = m_pThis->m_pComms->m_pCameras->FindProxy(Port);

		if( pCamera ) {

			return pCamera->UseSetup(Camera, Index);
		}
	}

	return 0;
}

// Honeywell Specific

void CDataServer::HonFindProf(int TimeUnits, int RampType, DWORD ptrRamp, DWORD ptrTime, DWORD ptrData, DWORD ptrAuxD, DWORD ptrXPos, DWORD ptrYPos, DWORD ptrAPos, DWORD ptrBPos, DWORD ptrCountX, DWORD ptrCountA, DWORD ptrLimit)
{
	if( WhoHasFeature(rfHoneywell) ) {

		IDataServer *pData   = CCommsSystem::m_pThis->GetDataServer();

		CDataRef    &refRamp = (CDataRef &) ptrRamp;

		CDataRef    &refTime = (CDataRef &) ptrTime;

		CDataRef    &refData = (CDataRef &) ptrData;

		CDataRef    &refAuxD = (CDataRef &) ptrAuxD;

		CDataRef    &refXPos = (CDataRef &) ptrXPos;

		CDataRef    &refYPos = (CDataRef &) ptrYPos;

		CDataRef    &refAPos = (CDataRef &) ptrAPos;

		CDataRef    &refBPos = (CDataRef &) ptrBPos;

		C3INT Scale  = TimeUnits ? 60 : 3600;

		C3INT CountX = 0;

		C3INT CountA = 0;

		C3INT Limit  = 0;

		for( UINT n = 0; n < 50; n++ ) {

			C3REAL Time = I2R(pData->GetData(ptrTime, typeReal, 0));

			if( Time > 0 ) {

				C3REAL Data = I2R(pData->GetData(ptrData, typeReal, 0));

				C3REAL AuxD = I2R(pData->GetData(ptrAuxD, typeReal, 0));

				if( TRUE ) {

					pData->SetData(ptrAPos, typeInteger, 0, Limit);

					pData->SetData(ptrBPos, typeReal, 0, R2I(AuxD));

					CountA += 1;

					refAPos.x.m_Array += 1;

					refBPos.x.m_Array += 1;
				}

				if( pData->GetData(ptrRamp, typeInteger, 0) ) {

					if( !RampType ) {

						refData.x.m_Array += 1;

						C3REAL Next = I2R(pData->GetData(ptrData, typeReal, 0));

						C3REAL Step = fabs(Next - Data);

						Time        = Step / Time;

						refData.x.m_Array -= 1;
					}

					pData->SetData(ptrXPos, typeInteger, 0, Limit);

					pData->SetData(ptrYPos, typeReal, 0, R2I(Data));

					Limit  += C3INT(Scale * Time);

					CountX += 1;

					refXPos.x.m_Array += 1;

					refYPos.x.m_Array += 1;
				}
				else {
					pData->SetData(ptrXPos, typeInteger, 0, Limit);

					pData->SetData(ptrYPos, typeReal, 0, R2I(Data));

					Limit  += C3INT(Scale * Time);

					CountX += 1;

					refXPos.x.m_Array += 1;

					refYPos.x.m_Array += 1;

					pData->SetData(ptrXPos, typeInteger, 0, Limit);

					pData->SetData(ptrYPos, typeReal, 0, R2I(Data));

					CountX += 1;

					refXPos.x.m_Array += 1;

					refYPos.x.m_Array += 1;
				}

				if( TRUE ) {

					pData->SetData(ptrAPos, typeInteger, 0, Limit);

					pData->SetData(ptrBPos, typeReal, 0, R2I(AuxD));

					CountA += 1;

					refAPos.x.m_Array += 1;

					refBPos.x.m_Array += 1;
				}
			}

			refRamp.x.m_Array += 1;

			refTime.x.m_Array += 1;

			refData.x.m_Array += 1;

			refAuxD.x.m_Array += 1;
		}

		pData->SetData(ptrCountA, typeInteger, 0, CountA);

		pData->SetData(ptrCountX, typeInteger, 0, CountX);

		pData->SetData(ptrLimit, typeInteger, 0, Limit);
	}
}

int CDataServer::HonFindTime(int TimeUnits, int RampType, DWORD ptrRamp, DWORD ptrTime, DWORD ptrData, C3REAL Segment, C3REAL Remain)
{
	if( WhoHasFeature(rfHoneywell) ) {

		C3INT Count = C3INT(Segment);

		if( Count > 0 ) {

			IDataServer *pData   = CCommsSystem::m_pThis->GetDataServer();

			CDataRef    &refRamp = (CDataRef &) ptrRamp;

			CDataRef    &refTime = (CDataRef &) ptrTime;

			CDataRef    &refData = (CDataRef &) ptrData;

			C3INT Scale = TimeUnits ? 60 : 3600;

			C3INT Total = 0;

			for( INT n = 0; n < Count; n++ ) {

				C3REAL Time = I2R(pData->GetData(ptrTime, typeReal, 0));

				if( Time > 0 ) {

					if( pData->GetData(ptrRamp, typeInteger, 0) ) {

						if( !RampType ) {

							C3REAL Data = I2R(pData->GetData(ptrData, typeReal, 0));

							refData.x.m_Array += 1;

							C3REAL Next = I2R(pData->GetData(ptrData, typeReal, 0));

							C3REAL Step = fabs(Next - Data);

							Time        = Step / Time;

							refData.x.m_Array -= 1;
						}
					}

					Total += C3INT(Scale * Time);
				}

				refRamp.x.m_Array += 1;

				refTime.x.m_Array += 1;

				refData.x.m_Array += 1;
			}

			Total -= C3INT(Remain);

			return Total;
		}
	}

	return 0;
}

PUTF CDataServer::HonGetLogParamS(int n, int i)
{
	if( WhoHasFeature(rfHoneywell) ) {

		CDataLogInfo *pInfo = CDataLogger::m_pThis->m_pInfo;

		if( pInfo ) {

			if( i == 1 || i == 4 ) {

				n = n * 5 + i;

				if( pInfo->m_pData[n] ) {

					pInfo->m_pData[n]->SetScan(scanOnce);

					return wstrdup(pInfo->m_pData[n]->GetText(L""));
				}
			}
		}
	}

	return wstrdup(L"");
}

int CDataServer::HonGetLogParamI(int n, int i)
{
	if( WhoHasFeature(rfHoneywell) ) {

		CDataLogInfo *pInfo = CDataLogger::m_pThis->m_pInfo;

		if( pInfo ) {

			if( i == 0 || i == 2 || i == 3 ) {

				n = n * 5 + i;

				if( pInfo->m_pData[n] ) {

					pInfo->m_pData[n]->SetScan(scanOnce);

					return int(pInfo->m_pData[n]->ExecVal());
				}
			}
		}
	}

	return 0;
}

void CDataServer::HonSetLogParamI(int n, int i, int v)
{
	if( WhoHasFeature(rfHoneywell) ) {

		CDataLogInfo *pInfo = CDataLogger::m_pThis->m_pInfo;

		if( pInfo ) {

			if( i == 2 ) {

				n = n * 5 + i;

				if( pInfo->m_pData[n] ) {

					DWORD Ref = pInfo->m_pData[n]->Execute(typeLValue);

					if( Ref ) {

						m_pThis->SetData(Ref, typeInteger, setNone, v);
					}
				}
			}
		}
	}
}

int CDataServer::HonGetLogParamA(void)
{
	if( WhoHasFeature(rfHoneywell) ) {

		CDataLogInfo *pInfo = CDataLogger::m_pThis->m_pInfo;

		if( pInfo ) {

			BOOL fAvail = TRUE;

			for( UINT n = 0; n < elements(pInfo->m_pData); n++ ) {

				if( pInfo->m_pData[n] ) {

					if( fAvail ) {

						if( !pInfo->m_pData[n]->IsAvail() ) {

							fAvail = FALSE;
						}
					}

					pInfo->m_pData[n]->SetScan(scanOnce);
				}
			}

			return fAvail;
		}
	}

	return FALSE;
}

// Debugging

void CDataServer::DebugPrint(PUTF pText)
{
	extern void C3ExecPrint(PCTXT pLine);

	C3ExecPrint(UniConvert(pText));

	free(pText);
}

void CDataServer::DebugStackTrace(void)
{
	extern void C3ExecStackTrace(void);

	C3ExecStackTrace();
}

void CDataServer::DebugDumpLocals(void)
{
	extern void C3ExecDumpLocals(void);

	C3ExecDumpLocals();
}

// End of File
