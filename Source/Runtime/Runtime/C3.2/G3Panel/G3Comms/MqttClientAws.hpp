
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClientAws_HPP

#define	INCLUDE_MqttClientAws_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCloudServiceAws;

class CMqttClientOptionsAws;

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for AWS
//

class CMqttClientAws : public CMqttClientJson
{
public:
	// Constructor
	CMqttClientAws(CCloudServiceAws *pService, CMqttClientOptionsAws &Opts);

	// Operations
	BOOL Open(void);

protected:
	// Sub Topics
	enum
	{
		subDelta = 0,
	};

	// Options
	CMqttClientOptionsAws &m_Opts;

	// Client Hooks
	void OnClientPhaseChange(void);
	BOOL OnClientNewData(CMqttMessage const *pMsg);

	// Publish Hook
	BOOL GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg);
};

// End of File

#endif
