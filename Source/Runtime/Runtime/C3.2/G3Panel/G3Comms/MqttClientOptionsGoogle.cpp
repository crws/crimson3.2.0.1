
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsGoogle.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Matrix Cyrptography
//

#if !defined(AEON_ENVIRONMENT)

#include "../../../drive/extra/matrix/crypto.hpp"

#endif

//////////////////////////////////////////////////////////////////////////
//
// Google MQTT Client Options
//

// Constructor

CMqttClientOptionsGoogle::CMqttClientOptionsGoogle(void)
{
	m_pPrivData  = NULL;

	m_uPrivData  = 0;
}

// Destructor

CMqttClientOptionsGoogle::~CMqttClientOptionsGoogle(void)
{
	delete[] m_pPrivData;
}

// Initialization

void CMqttClientOptionsGoogle::Load(PCBYTE &pData)
{
	CMqttClientOptionsJson::Load(pData);

	GetCoded(pData, m_Project);

	GetCoded(pData, m_Region);

	GetCoded(pData, m_Registry);

	GetCoded(pData, m_Device);

	LoadFile(pData, m_pPrivData, m_uPrivData);
}

// Config Fixup

BOOL CMqttClientOptionsGoogle::FixConfig(void)
{
	if( CMqttClientOptionsJson::FixConfig() ) {

		FixCoded(m_Project, FALSE);

		FixCoded(m_Region, FALSE);

		FixCoded(m_Registry, FALSE);

		FixCoded(m_Device, FALSE);

		m_ClientId  = "projects/"    + m_Project;

		m_ClientId += "/locations/"  + m_Region;

		m_ClientId += "/registries/" + m_Registry;

		m_ClientId += "/devices/"    + m_Device;

		m_PubTopic.Printf("/devices/%s/events", PCTXT(m_Device));

		m_SubTopic.Printf("/devices/%s/config", PCTXT(m_Device));

		return TRUE;
	}

	return FALSE;
}

// Attributes

CString CMqttClientOptionsGoogle::GetExtra(void) const
{
	CString Extra;

	Extra += m_Project  + '&';
	Extra += m_Region   + '&';
	Extra += m_Registry + '&';
	Extra += m_Device;

	return Extra;
}

// Operations

void CMqttClientOptionsGoogle::MakeCredentials(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	DWORD t = tv.tv_sec;

	DWORD h = 60*60;

	DWORD d = 24*h;

	CString s1("{\"alg\":\"RS256\",\"typ\":\"JWT\"}");

	CPrintf s2("{\"aud\":\"%s\",\"iat\":%u,\"exp\":%u}", PCTXT(m_Project), t-1*h, t+1*d);

	CString e1 = CHttpBase64Url::Encode(s1);

	CString e2 = CHttpBase64Url::Encode(s2);

	CString et = e1 + "." + e2;

	#if defined(AEON_ENVIRONMENT)

	ICryptoSign *pSign = NULL;

	if( AfxNewObject("sign-rsa", ICryptoSign, pSign) == S_OK ) {

		pSign->SetHash("hash-sha256");

		if( pSign->Initialize(m_pPrivData, m_uPrivData, "") ) {

			pSign->Update(et);

			pSign->Finalize();

			et += ".";

			et += pSign->GetSigString(hashUrl64);
		}
		else
			et.Empty();

		pSign->Release();
	}
	else
		et.Empty();

	#else

	psRsaKey_t key;

	char name[32];

	MakeFile(name, m_pPrivData, m_uPrivData);

	if( !psPkcs1ParsePrivFile(NULL, name, NULL, &key) ) {

		////////

		BYTE bHash[32];

		psSha256_t sha;

		psSha256Init(&sha);

		psSha256Update(&sha, PCBYTE(PCTXT(et)), et.GetLength());

		psSha256Final(&sha, bHash);

		////////

		UINT  uSign = key.size;

		PBYTE pSign = New BYTE[uSign];

		if( !privRsaEncryptSignedElement(NULL, &key, bHash, 32, pSign, uSign, NULL) ) {

			CString e3 = CHttpBase64Url::Encode(pSign, uSign);

			et += ".";

			et += e3;
		}
		else
			et.Empty();

		delete[] pSign;

		psRsaClearKey(&key);
	}

	#endif

	m_UserName  = "unused";

	m_Password = et;
}

// Implementation

BOOL CMqttClientOptionsGoogle::LoadFile(PCBYTE &pData, PBYTE &pFile, UINT &uFile)
{
	if( (uFile = GetWord(pData)) ) {

		pFile = New BYTE[uFile];

		memcpy(pFile, pData, uFile);

		pData += uFile;

		return TRUE;
	}

	return FALSE;
}

void CMqttClientOptionsGoogle::MakeFile(char *pName, PCBYTE pData, UINT uSize)
{
	// Note the sizing here! We allocate one more byte and put
	// a NUL in there to terminate the data -- but we do not
	// include that NUL in the size of the file. It is just
	// there to stop Matrix wildly running beyond the end.

	#if !defined(AEON_ENVIRONMENT)

	PBYTE pCopy = PBYTE(psMalloc(NULL, uSize + 1));

	memcpy(pCopy, pData, uSize);

	pCopy[uSize] = 0;

	SPrintf(pName, "%X,%X", pCopy, uSize);

	#endif
}

// End of File
