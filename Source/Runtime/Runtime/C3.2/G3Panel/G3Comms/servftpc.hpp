
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SERVFTPC_HPP
	
#define	INCLUDE_SERVFTPC_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "service.hpp"

#include "stdclient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// FTP Client Service
//

class CFileSync : public CServiceItem,
		  public ITaskEntry,
		  public IServiceFileSync,
		  public CStdClient
{
	public:
		// Constructor
		CFileSync(void);

		// Destructor
		~CFileSync(void);

		// Initialization
		void Load(PCBYTE &pData);

		// IService
		UINT GetID(void);
		void GetTaskList(CTaskList &List);

		// ITaskEntry
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// IServiceFileSync
		BOOL Sync(void);
		BOOL PutFile(PCTXT pLocal, PCTXT pRemote, BOOL fDelete);
		BOOL GetFile(PCTXT pLocal, PCTXT pRemote, BOOL fDelete);
		BOOL SyncFrom(PCTXT pPath);

		// Item Properties
		CCodedItem * m_pEnable;
		CCodedItem * m_pLogSync;
		CCodedItem * m_pFtpServer;
		CCodedItem * m_pFtpPort;
		CCodedItem * m_pFtpSSL;
		CCodedText * m_pUser;
		CCodedText * m_pPass;
		CCodedItem * m_pPassive;
		CCodedItem * m_pPerFile;
		CCodedItem * m_pTestSize;
		CCodedItem * m_pKeep;
		CCodedItem * m_pLogFile;
		CCodedText * m_pFtpBase;
		CCodedItem * m_pFreq;
		CCodedItem * m_pFreqMins;
		CCodedItem * m_pDelay;
		CCodedItem * m_pDrive;

	protected:
		// Transfer Modes
		enum
		{
			modeSync	= 0,
			modeSend	= 1,
			modeRetr	= 2,
			modeMove	= 3
			};

		// Client States
		enum
		{
			stateIdle,
			stateConnect,
			stateSSL,
			stateUser,
			statePass,
			statePBSZ,
			stateProt,
			stateType,
			stateStru,
			stateMode,
			stateChDir,
			stateMkDir,
			stateListPort,
			stateListData,
			stateSendPort,
			stateSendData,
			stateSizePort,
			stateSizeData,
			stateRetrPort,
			stateRetrData,
			stateDelete,
			stateQuit,
			stateDone,
			};

		// Configuration
		struct CConfig
		{
			BOOL	m_fEnable;
			BOOL	m_fLogSync;
			DWORD   m_FtpServer;
			UINT    m_FtpPort;
			UINT    m_uSSL;
			CString m_User;
			CString m_Pass;
			BOOL	m_fPassive;
			BOOL	m_fPerFile;
			BOOL	m_fTestSize;
			UINT    m_uKeep;
			BOOL	m_fLogFile;
			CString m_FtpBase;
			UINT	m_uFreqH;
			UINT	m_uFreqM;
			UINT	m_uDelay;
			UINT    m_uDrive;
			};

		// File Info
		struct CFileInfo
		{
			char sName[32];
			UINT uSize;
			};

		// Data Members
		CConfig	        m_Cfg;
		IMutex	    *   m_pLock;
		IEvent	    *   m_pReady;
		UINT	        m_uIndex;
		UINT	        m_uLinger;
		UINT	        m_uMode;
		ISocket     *   m_pDataSock;
		CString	        m_LocPath;
		CString	        m_LocName;
		CString	        m_RemPath;
		CString	        m_RemName;
		CFileInfo   *   m_pRemList;
		UINT            m_uRemCount;
		UINT            m_uRemAlloc;
		char	        m_sRemLine[128];
		UINT	        m_uRemPtr;
		CString	      * m_pLocList;
		UINT            m_uLocCount;
		CFileInfo       m_LocFile;
		CFileInfo       m_RemFile;

		// Implementation
		void FindConfig(CConfig &Cfg);

		// Configuration
		UINT  GetCmdPort(void);
		DWORD GetServer(void);

		// Sync Handler
		BOOL PerformSync(BOOL fKeep);
		void SessionDone(void);
		void PollLinger(void);

		// State Handlers
		void HandleConnect(void);
		void HandleSSL(void);
		void HandlePBSZ(void);
		void HandleProt(void);
		void HandleUser(void);
		void HandlePass(void);
		void HandleType(void);
		void HandleStru(void);
		void HandleMode(void);
		void HandleChDir(void);
		void HandleMkDir(void);
		void HandleListPort(void);
		void HandleListData(void);
		void HandleSendPort(void);
		void HandleSendData(void);
		void HandleSizePort(void);
		void HandleSizeData(void);
		void HandleRetrPort(void);
		void HandleRetrData(void);
		void HandleDelete(void);
		void HandleQuit(void);

		// State Transitions
		void SwitchToMkDir(void);
		void SwitchToTransfer(void);
		void SwitchToNextFile(void);

		// Response Check
		BOOL IsConnection(void);

		// Port Helper
		BOOL HandlePort(UINT uState, BOOL fSend);

		// Commands
		BOOL SendSSL(void);
		BOOL SendPBSZ(void);
		BOOL SendProt(void);
		BOOL SendUser(PCTXT pUser);
		BOOL SendPass(PCTXT pPass);
		BOOL SendType(PCTXT pType);
		BOOL SendStru(void);
		BOOL SendMode(void);
		BOOL SendMkDir(PCTXT pPath);
		BOOL SendChDir(PCTXT pPath);
		BOOL SendPort(BOOL fSend);
		BOOL SendPasv(void);
		BOOL SendList(PCTXT pPath);
		BOOL SendStor(PCTXT pPath, PCTXT pFile);
		BOOL SendRetr(PCTXT pPath, PCTXT pFile);
		BOOL SendDele(PCTXT pPath, PCTXT pFile);
		BOOL SendQuit(void);

		// Socket Management
		BOOL OpenDataSocket(UINT uPort, BOOL fSend, BOOL fSSL);
		BOOL WaitDataSocket(void);
		void CloseDataSocket(void);
		void AbortDataSocket(void);

		// Directory Parsing
		BOOL ReadListOutput(void);
		void ClearRemList(void);
		void GrowRemList(void);
		void ParseRemList(char cData);
		void ParseRemLine(void);
		void ParseRemLine(UINT uSize, UINT uName);
		void SortRemList(void);
		BOOL IsRightsChar(char cData);

		// Directory Building
		void ClearLocList(void);
		BOOL MakeLocList(void);
		void SortLocList(void);

		// File Transmission
		BOOL SendFileData(void);
		BOOL RecvFileData(void);

		// Sort Functions
		static int RemSortFunc(PCVOID p1, PCVOID p2);
		static int LocSortFunc(PCVOID p1, PCVOID p2);

		// Name Parsing
		BOOL Parse(char cSep, CString Full, CString &Path, CString &Name);

		// Port Parsing
		UINT ParsePort(void);
	};

// End of File

#endif
