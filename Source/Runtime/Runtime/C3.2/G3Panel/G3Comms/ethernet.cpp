
#include "intern.hpp"

#include "service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Configuration
//

// Constructor

CEthernetItem::CEthernetItem(void)
{
	m_pPorts = New CCommsPortList;
	}

// Destructor

CEthernetItem::~CEthernetItem(void)
{
	delete m_pPorts;
	}

// Initialization

void CEthernetItem::Load(PCBYTE &pData)
{
	ValidateLoad("CEthernetItem", pData);

	m_pPorts->Load(pData);
	}

// Task List

void CEthernetItem::GetTaskList(CTaskList &List, UINT &uLevel)
{
	m_pPorts->GetTaskList(List, uLevel);
	}

// End of File
