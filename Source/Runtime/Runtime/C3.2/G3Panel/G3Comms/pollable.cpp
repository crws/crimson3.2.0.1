
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tag Pollable Item
//

// Constructor

CTagPollable::CTagPollable(void)
{
	m_Mode   = 0;

	m_Delay	 = 0;

	m_uIndex = NOTHING;
		 
	m_pCtx   = NULL;
	}

// Destructor

CTagPollable::~CTagPollable(void)
{
	delete [] m_pCtx;
	}

// Operations

void CTagPollable::SetIndex(UINT uIndex)
{
	m_uIndex = uIndex;
	}

void CTagPollable::SetCount(UINT uCount)
{
	UINT c = uCount ? uCount : 1;

	m_pCtx = New CCtx [ c ];

	for( UINT n = 0; n < c; n++ ) {

		CCtx &Ctx = m_pCtx[n];

		Ctx.m_fInit   = FALSE;
		
		Ctx.m_fState  = FALSE;
		
		Ctx.m_fLast   = FALSE;
		
		Ctx.m_HV      = 0;
		
		Ctx.m_uTimer  = NOTHING;
		}
	}

// Event Hook

void CTagPollable::OnChange(UINT uPos, BOOL fChange, DWORD PV)
{
	CCtx &Ctx = m_pCtx[uPos];

	if( Ctx.m_fState ) {

		if( fChange ) {

			Ctx.m_fState = FALSE;

			Ctx.m_HV     = PV;
			}
		}
	}

// Startup Mode

BOOL CTagPollable::FireOnInit(void)
{
	return FALSE;
	}

// Implementation

void CTagPollable::PollFlag(UINT uPos, C3INT Enable, DWORD SP, DWORD PV, UINT uDelta)
{
	CCtx &Ctx    = m_pCtx[uPos];

	BOOL fState  = FALSE;

	BOOL fChange = FALSE;

	if( Enable ) {

		switch( m_Mode ) {

			case 1:
				fState = PV ? TRUE : FALSE;
				break;

			case 2:
				fState = PV ? FALSE : TRUE;
				break;

			case 3:
				fState = (!PV != !SP);
				break;

			case 4:
				fState = (!PV && SP);
				break;

			case 5:
				fState = (PV && !SP);
				break;

			case 6:
				fState = (!PV == !SP);
				break;
			}

		if( m_Mode == 7 ) {

			if( !Ctx.m_fInit ) {

				Ctx.m_HV = PV;
				}

			fState  = (!PV != !Ctx.m_HV);
			
			fChange = TRUE;
			}
		}
	else {
		if( m_Mode == 7 ) {

			Ctx.m_HV = PV;
			}
		}

	PollUpdate(uPos, uDelta, fState, fChange, PV);
	}

void CTagPollable::PollInteger(UINT uPos, C3INT Enable, C3INT SP, C3INT PV, C3INT Value, C3INT Hyst, UINT uDelta)
{
	CCtx &Ctx    = m_pCtx[uPos];

	BOOL fState  = FALSE;

	BOOL fChange = FALSE;

	if( Enable ) {

		switch( m_Mode ) {

			case 1:
				fState = (PV == Value);
				break;

			case 2:
				fState = (PV != Value);
				break;

			case 3:
				fState = (PV >= Value - (Ctx.m_fState ? Hyst : 0));
				break;

			case 4:
				fState = (PV <= Value + (Ctx.m_fState ? Hyst : 0));
				break;

			case 5:
				fState = (PV >= SP + Value - (Ctx.m_fState ? Hyst : 0));
				break;

			case 6:
				fState = (PV <= SP - Value + (Ctx.m_fState ? Hyst : 0));
				break;
			}

		if( m_Mode == 7 ) {

			BOOL fAbove = (PV >= SP + Value - (Ctx.m_fState ? Hyst : 0));
			
			BOOL fBelow = (PV <= SP - Value + (Ctx.m_fState ? Hyst : 0));

			fState = (fAbove || fBelow);
			}

		if( m_Mode == 8 ) {

			BOOL fAbove = (PV >= SP + Value - (Ctx.m_fState ? Hyst : 0));
			
			BOOL fBelow = (PV <= SP - Value + (Ctx.m_fState ? Hyst : 0));

			fState = (!fAbove && !fBelow);
			}

		if( m_Mode == 9 ) {

			if( PV < C3INT(Ctx.m_HV) || !Ctx.m_fInit ) {

				Ctx.m_HV = PV;
				}

			if( PV >= C3INT(Ctx.m_HV) + max(1, Value) ) {

				fState = TRUE;
				}

			fChange = TRUE;
			}

		if( m_Mode == 10 ) {

			if( PV > C3INT(Ctx.m_HV) || !Ctx.m_fInit ) {

				Ctx.m_HV = PV;
				}

			if( PV <= C3INT(Ctx.m_HV) - max(1, Value) ) {

				fState = TRUE;
				}

			fChange = TRUE;
			}

		if( m_Mode == 11 ) {

			if( !Ctx.m_fInit ) {

				Ctx.m_HV = PV;
				}

			if( PV >= C3INT(Ctx.m_HV) + max(1, Value) ) {

				fState = TRUE;
				}

			if( PV <= C3INT(Ctx.m_HV) - max(1, Value) ) {

				fState = TRUE;
				}

			fChange = TRUE;
			}
		}
	else {
		if( m_Mode >= 9 ) {

			Ctx.m_HV = PV;
			}
		}

	PollUpdate(uPos, uDelta, fState, fChange, PV);
	}

void CTagPollable::PollReal(UINT uPos, C3INT Enable, C3REAL SP, C3REAL PV, C3REAL Value, C3REAL Hyst, UINT uDelta)
{
	CCtx &Ctx    = m_pCtx[uPos];

	BOOL fState  = FALSE;

	BOOL fChange = FALSE;

	if( Enable ) {

		switch( m_Mode ) {

			case 1:
				fState = (PV == Value);
				break;

			case 2:
				fState = (PV != Value);
				break;

			case 3:
				fState = (PV >= Value - (Ctx.m_fState ? Hyst : 0));
				break;

			case 4:
				fState = (PV <= Value + (Ctx.m_fState ? Hyst : 0));
				break;

			case 5:
				fState = (PV >= SP + Value - (Ctx.m_fState ? Hyst : 0));
				break;

			case 6:
				fState = (PV <= SP - Value + (Ctx.m_fState ? Hyst : 0));
				break;
			}

		if( m_Mode == 7 ) {

			BOOL fAbove = (PV >= SP + Value - (Ctx.m_fState ? Hyst : 0));
			
			BOOL fBelow = (PV <= SP - Value + (Ctx.m_fState ? Hyst : 0));

			fState = (fAbove || fBelow);
			}

		if( m_Mode == 8 ) {

			BOOL fAbove = (PV >= SP + Value - (Ctx.m_fState ? Hyst : 0));
			
			BOOL fBelow = (PV <= SP - Value + (Ctx.m_fState ? Hyst : 0));

			fState = (!fAbove && !fBelow);
			}

		if( m_Mode == 9 ) {

			if( PV < I2R(Ctx.m_HV) || !Ctx.m_fInit ) {

				Ctx.m_HV = R2I(PV);
				}

			if( PV != I2R(Ctx.m_HV) ) {

				if( PV >= I2R(Ctx.m_HV) + Value ) {

					fState = TRUE;
					}
				}

			fChange = TRUE;
			}

		if( m_Mode == 10 ) {

			if( PV > I2R(Ctx.m_HV) || !Ctx.m_fInit ) {

				Ctx.m_HV = R2I(PV);
				}

			if( PV != I2R(Ctx.m_HV) ) {

				if( PV <= I2R(Ctx.m_HV) - Value ) {

					fState = TRUE;
					}
				}

			fChange = TRUE;
			}

		if( m_Mode == 11 ) {

			if( !Ctx.m_fInit ) {

				Ctx.m_HV = R2I(PV);
				}

			if( PV != I2R(Ctx.m_HV) ) {

				if( PV >= I2R(Ctx.m_HV) + Value ) {

					fState = TRUE;
					}

				if( PV <= I2R(Ctx.m_HV) - Value ) {

					fState = TRUE;
					}
				}

			fChange = TRUE;
			}
		}
	else {
		if( m_Mode >= 9 ) {

			Ctx.m_HV = R2I(PV);
			}
		}

	PollUpdate(uPos, uDelta, fState, fChange, R2I(PV));
	}

void CTagPollable::PollUpdate(UINT uPos, UINT uDelta, BOOL fState, BOOL fChange, DWORD PV)
{
	CCtx &Ctx = m_pCtx[uPos];

	if( !Ctx.m_fInit ) {

		Ctx.m_fState = !fState;
		
		Ctx.m_fLast  = FireOnInit() ? !fState : fState;

		Ctx.m_fInit  = TRUE;
		}

	if( Ctx.m_fState != fState ) {

		Ctx.m_uTimer = m_Delay;

		Ctx.m_fState = fState;
		}

	if( Ctx.m_uTimer < NOTHING ) {

		if( uDelta >= Ctx.m_uTimer ) {

			if( Ctx.m_fLast - Ctx.m_fState ) {

				OnChange(uPos, fChange, PV);

				Ctx.m_fLast = Ctx.m_fState;
				}

			Ctx.m_uTimer = NOTHING;
			}
		else
			Ctx.m_uTimer -= uDelta;
		}
	}

// End of File
