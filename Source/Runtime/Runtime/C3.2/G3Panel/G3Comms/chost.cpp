
#include "intern.hpp"

#include "..\..\Crimson\C3Core\rc4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Coded Host
//

// Loading Helpers

void CCodedHost::GetCoded(PCBYTE &pData, CCodedItem * &pItem)
{
	UINT uMode = GetByte(pData);

	if( uMode == 1 ) {

		pItem = New CCodedItem;

		pItem->Load(pData);

		return;
	}

	if( uMode == 2 ) {

		UINT  uSize = GetWord(pData);

		PBYTE pCopy = New BYTE[uSize];

		memcpy(pCopy, pData, uSize);

		pData += uSize;

		Crypto(pCopy, uSize);

		PCBYTE pTemp = pCopy;

		pItem = New CCodedItem;

		pItem->Load(pTemp);

		delete[] pCopy;

		return;
	}
}

void CCodedHost::GetCoded(PCBYTE &pData, CCodedText * &pText)
{
	UINT uMode = GetByte(pData);

	if( uMode == 1 ) {

		pText = New CCodedText;

		pText->Load(pData);

		return;
	}

	if( uMode == 2 ) {

		UINT  uSize = GetWord(pData);

		PBYTE pCopy = New BYTE[uSize];

		memcpy(pCopy, pData, uSize);

		pData += uSize;

		Crypto(pCopy, uSize);

		PCBYTE pTemp = pCopy;

		pText = New CCodedText;

		pText->Load(pTemp);

		delete[] pCopy;

		return;
	}
}

void CCodedHost::GetCoded(PCBYTE &pData, CString &Text)
{
	switch( GetByte(pData) ) {

		case 0:
		{
			Text.Empty();
		}
		break;

		case 1:
		{
			// TODO -- This needs to be hacked once Kathy's
			// alignment and other mods are merged across!!!

			CByteArray Data;

			WORD wMark = GetWord(pData);

			Data.Append(HIBYTE(wMark));
			Data.Append(LOBYTE(wMark));

			UINT uRefs = GetWord(pData);

			Data.Append(HIBYTE(uRefs));
			Data.Append(LOBYTE(uRefs));

			while( uRefs-- ) {

				DWORD r = GetLong(pData);

				Data.Append(HIBYTE(HIWORD(r)));
				Data.Append(LOBYTE(HIWORD(r)));
				Data.Append(HIBYTE(LOWORD(r)));
				Data.Append(LOBYTE(LOWORD(r)));
			}

			UINT uCode = GetWord(pData);

			Data.Append(HIBYTE(uCode));
			Data.Append(LOBYTE(uCode));

			Data.Append(pData, uCode);

			pData += uCode;

			Text = CString(' ', Data.size());

			memcpy(PBYTE(PCSTR(Text)), Data.data(), Data.size());
		}
		break;

		default:
		{
			AfxAssert(FALSE);
		}
		break;
	}
}

void CCodedHost::SkipCoded(PCBYTE &pData)
{
	UINT uMode = GetByte(pData);

	if( uMode == 1 ) {

		GetWord(pData);

		UINT uRefs = GetWord(pData);

		if( uRefs ) {

			Item_Align4(pData);

			pData += sizeof(DWORD) * uRefs;
		}

		UINT uCode = GetWord(pData);

		pData += sizeof(BYTE) * uCode;

		return;
	}

	if( uMode == 2 ) {

		UINT uSize = GetWord(pData);

		pData += uSize;

		return;
	}
}

BOOL CCodedHost::FixCoded(CString &Text, BOOL fRequired)
{
	if( Text.GetLength() ) {

		PCBYTE pData = PCBYTE(PCSTR(Text));

		CAutoPointer<CCodedItem> pItem(New CCodedItem);

		pItem->Load(pData);

		pItem->SetScan(scanTrue);

		for( SetTimer(5000); GetTimer(); Sleep(25) ) {

			if( pItem->IsAvail() ) {

				PUTF p = PUTF(pItem->Execute(typeString));

				Text = UniConvert(p);

				Free(p);

				if( !Text.IsEmpty() ) {

					pItem->SetScan(scanFalse);

					return TRUE;
				}

				break;
			}
		}

		pItem->SetScan(scanFalse);
	}

	return !fRequired;
}

// Availability

BOOL CCodedHost::IsItemAvail(CCodedItem *pItem)
{
	if( pItem ) {

		return pItem->IsAvail();
	}

	return TRUE;
}

BOOL CCodedHost::SetItemScan(CCodedItem *pItem, UINT Code)
{
	if( pItem ) {

		return pItem->SetScan(Code);
	}

	return TRUE;
}

DWORD CCodedHost::GetItemAddr(CCodedItem *pItem, DWORD Default)
{
	if( pItem ) {

		return HostToMotor(DWORD(pItem->Execute(typeInteger)));
	}

	return Default;
}

C3REAL CCodedHost::GetItemData(CCodedItem *pItem, C3REAL Default)
{
	if( pItem ) {

		return I2R(pItem->Execute(typeReal));
	}

	return Default;
}

C3INT CCodedHost::GetItemData(CCodedItem *pItem, C3INT Default)
{
	if( pItem ) {

		return C3INT(pItem->Execute(typeInteger));
	}

	return Default;
}

CUnicode CCodedHost::GetItemData(CCodedItem *pItem, PCUTF pDefault)
{
	if( pItem ) {

		PUTF     p = PUTF(pItem->Execute(typeString));

		CUnicode s = p;

		Free(p);

		return s;
	}

	return pDefault;
}

CString CCodedHost::GetItemData(CCodedItem *pItem, PCTXT pDefault)
{
	if( pItem ) {

		PUTF    p = PUTF(pItem->Execute(typeString));

		CString s = UniConvert(p);

		Free(p);

		return s;
	}

	return pDefault;
}

C3REAL CCodedHost::GetItemData(CCodedItem *pItem, C3REAL Default, PDWORD pParam)
{
	if( pItem ) {

		return I2R(pItem->Execute(typeReal, pParam));
	}

	return Default;
}

C3INT CCodedHost::GetItemData(CCodedItem *pItem, C3INT Default, PDWORD pParam)
{
	if( pItem ) {

		return C3INT(pItem->Execute(typeInteger, pParam));
	}

	return Default;
}

CUnicode CCodedHost::GetItemData(CCodedItem *pItem, PCUTF pDefault, PDWORD pParam)
{
	if( pItem ) {

		PUTF     p = PUTF(pItem->Execute(typeString, pParam));

		CUnicode s = p;

		Free(p);

		return s;
	}

	return pDefault;
}

CString CCodedHost::GetItemData(CCodedItem *pItem, PCTXT pDefault, PDWORD pParam)
{
	if( pItem ) {

		PUTF    p = PUTF(pItem->Execute(typeString, pParam));

		CString s = UniConvert(p);

		Free(p);

		return s;
	}

	return pDefault;
}

// Execution

BOOL CCodedHost::Execute(CCodedItem *pAction)
{
	if( pAction ) {

		pAction->Execute(typeVoid);

		return TRUE;
	}

	return FALSE;
}

// Data Lifetime

BOOL CCodedHost::FreeData(DWORD Data, UINT Type)
{
	if( Type == typeString ) {

		Free(PTXT(Data));

		return TRUE;
	}

	return FALSE;
}

// Null Data

DWORD CCodedHost::GetNull(UINT Type)
{
	switch( Type ) {

		case typeInteger:

			return 0;

		case typeReal:

			return R2I(0);

		case typeString:

			return DWORD(wstrdup(L""));
	}

	return 0;
}

// Encryption

BOOL CCodedHost::Crypto(PBYTE pData, UINT uSize)
{
	rc4_key key;

	PCSTR pPass = "PineappleHead";

	prepare_key(PBYTE(pPass), strlen(pPass), &key);

	rc4(pData, uSize, &key);

	return TRUE;
}

// End of File
