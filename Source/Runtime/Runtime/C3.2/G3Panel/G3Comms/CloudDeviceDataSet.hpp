
#include "Intern.hpp"

#include "Service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudDeviceDataSet_HPP

#define	INCLUDE_CloudDeviceDataSet_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cloud Tag Set
//

class CCloudDeviceDataSet : public CCloudDataSet
{
public:
	// Constructor
	CCloudDeviceDataSet(void);

	// Destructor
	~CCloudDeviceDataSet(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Json Formatting
	BOOL GetJson(CMqttJsonData *pRep, CMqttJsonData *pDes, CString Ident, UINT64 uTime, UINT uRoot, UINT uCode, UINT uMode);

	// List Formatting
	UINT GetCount(void);
	UINT GetProps(void);
	BOOL GetData(UINT uIndex, CString &Name, DWORD &Data, UINT &Type, BOOL &Free, UINT uMode);
	BOOL GetProp(UINT uIndex, UINT uProp, CString &Name, DWORD &Data, UINT &Type);
	BOOL SetData(UINT uIndex, DWORD Data, UINT Type);

	// Data Members
	UINT m_Gps;
	UINT m_Cell;

protected:
	// Data Members
	CCellStatusInfo     m_CellInfo;
	CLocationSourceInfo m_Location;

	// Implementation
	BOOL AddGpsData(CMqttJsonData *pJson, UINT Type);
	BOOL AddCellData(CMqttJsonData *pJson, UINT Type);
};

// End of File

#endif
