
#include "Intern.hpp"

#include "Service.hpp"

#include "CloudServiceAws.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MqttClientAws.hpp"

#include "MqttClientOptionsAws.hpp"

#include "CloudDeviceDataSet.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AWS Cloud Service
//

// Instantiator

IService * Create_CloudServiceAws(void)
{
	return New CCloudServiceAws;
	}

// Constructor

CCloudServiceAws::CCloudServiceAws(void)
{
	m_Name = "AWS";
	}

// Initialization

void CCloudServiceAws::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudServiceAws", pData);

	CCloudServiceCrimson::Load(pData);

	CMqttClientOptionsAws *pOpts = New CMqttClientOptionsAws;

	pOpts->Load(pData);

	m_pOpts   = pOpts;

	CheckHistory(1000);

	m_pClient = New CMqttClientAws(this, *pOpts);

	m_pOpts->m_DiskPath.Printf("%c:\\MQTT\\AWS", 'C' + pOpts->m_Drive);

	FindConfigGuid(pOpts->GetExtra());
	}

// Service ID

UINT CCloudServiceAws::GetID(void)
{
	return 9;
	}

// End of File
