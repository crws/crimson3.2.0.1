
#include "Intern.hpp"

#include "Service.hpp"

#include "Matrix.hpp"

#include "MqttClientAzure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudServiceAzure.hpp"

#include "MqttClientOptionsAzure.hpp"

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for Azure
//

// Constructor

CMqttClientAzure::CMqttClientAzure(CCloudServiceAzure *pService, CMqttClientOptionsAzure &Opts) : CMqttClientJson(pService, Opts), m_Opts(Opts)
{
	}

// Operations

BOOL CMqttClientAzure::Open(void)
{
	if( m_Opts.m_Twin ) {

		m_PubTopic = "$iothub/twin/PATCH/properties/reported/";
	}
	else {
		m_PubTopic = "devices/" + m_Opts.m_ClientId + "/messages/" + m_Opts.m_Pub;
	}

	m_Will.SetTopic(ApplyWillSuffix(m_PubTopic, !m_Opts.m_Twin) + '/');

	m_Will.SetData(GetWillData());

	m_uLast = m_uSecs;

	return CMqttClientJson::Open();
}

BOOL CMqttClientAzure::Poll(UINT uID)
{
	if( uID == 0 ) {

		if( m_Opts.m_Twin ) {

			if( m_uLast - m_uSecs ) {

				BOOL fBusy = CMqttClientJson::Poll(uID);

				if( m_uPhase == phasePublishing || m_uPhase == phaseLive ) {

					for( UINT n = 0; n < elements(m_uTime); n++ ) {

						if( m_uTime[n] ) {
					
							if( m_uSecs >= m_uTime[n] ) {

								fBusy = TRUE;

								Abort();

								break;
								}
							}
						}
					}

				m_uLast = m_uSecs;

				return fBusy;
				}
			}
		}

	return CMqttClientJson::Poll(uID);
	}

// Client Hooks

void CMqttClientAzure::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseInitial ) {

		if( m_Opts.m_Twin ) {

			if( HasWrites() ) {

				AddToSubList(subWrite, "$iothub/twin/PATCH/properties/desired/#");
				}

			AddToSubList(subResp, "$iothub/twin/res/#");
			}

		CString Topic = "devices/" + m_Opts.m_ClientId + "/messages/" + m_Opts.m_Sub + "/#";

		AddToSubList(subDevice, Topic);
		}

	if( m_uPhase == phaseConnecting ) {

		m_Opts.MakeCredentials(5);
		}

	if( m_uPhase == phaseConnected ) {

		if( m_Opts.m_Twin ) {

			memset(m_fSent, 0, sizeof(m_fSent));

			memset(m_uTime, 0, sizeof(m_uTime));

			m_fRunning = !HasWrites();
			}
		else
			m_fRunning = TRUE;
		}

	CMqttClientJson::OnClientPhaseChange();
	}

BOOL CMqttClientAzure::OnClientNewData(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode == subResp ) {

		CStringArray List;

		pMsg->m_Topic.Tokenize(List, '/');

		if( List[0] == "$iothub" ) {

			if( List[1] == "twin" ) {

				if( List[2] == "res" ) {

					if( atoi(List[3]) / 100 == 2 ) {

						CString Rest = List[4];

						Rest.StripToken('=');

						UINT rid = atoi(Rest);

						if( rid >= 1 && rid <= pubCount ) {

							UINT uTopic = rid - 1;

							if( uTopic <= pubExtended ) {

								CCloudDataSet *pSet = m_pService->GetDataSet(uTopic);

								pSet->DataWasSent();
								}

							if( uTopic == pubGetTwin ) {

								CMqttJsonData Json;
								
								if( pMsg->GetJson(Json) ) {

									OnWrite(Json, "desired");
									}

								m_fRunning = TRUE;
								}

							m_fSent[uTopic] = FALSE;

							m_uTime[uTopic] = 0;

							return TRUE;
							}
						}
					}
				}
			}

		Abort();

		return TRUE;
		}

	if( pMsg->m_uCode == subWrite || pMsg->m_uCode == subDevice ) {

		CMqttJsonData Json;
								
		if( pMsg->GetJson(Json) ) {

			OnWrite(Json, "");
			}

		return TRUE;
		}

	return TRUE;
	}

BOOL CMqttClientAzure::OnClientGetData(CMqttMessage * &pMsg)
{
	if( !m_fRunning ) {

		UINT uTopic = pubGetTwin;

		if( !m_fSent[uTopic] ) {

			pMsg = New CMqttMessage;

			pMsg->SetCode (uTopic);

			pMsg->SetTopic(CPrintf("$iothub/twin/GET/?$rid=%u", 1+uTopic));

			pMsg->SetData ("{}");

			return TRUE;
			}

		return FALSE;
		}

	return CMqttClientJson::OnClientGetData(pMsg);
	}

BOOL CMqttClientAzure::OnClientDataSent(CMqttMessage const *pMsg)
{
	ShowSentData(pMsg);

	if( pMsg->m_uCode < m_PubList.GetCount() ) {

		CPub &Pub   = (CPub &) m_PubList[pMsg->m_uCode];

		UINT uTopic = Pub.m_uTopic;

		if( uTopic < pubExtended ) {

			if( !Pub.m_fHistory ) {

				Pub.m_fPend = FALSE;
				}

			if( m_Opts.m_Twin ) {

				m_fSent[uTopic] = TRUE;

				m_uTime[uTopic] = m_uSecs + m_Opts.m_uSendTimeout;
				}
			else {
				CCloudDataSet *pSet = m_pService->GetDataSet(uTopic);

				pSet->DataWasSent();
				}
			}

		return CMqttQueuedClient::OnClientDataSent(pMsg);
		}
	else {
		UINT uTopic = pubGetTwin;

		m_fSent[uTopic] = TRUE;

		m_uTime[uTopic] = m_uSecs + m_Opts.m_uSendTimeout;

		return TRUE;
		}
	}

// Publish Hook

BOOL CMqttClientAzure::GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg)
{
	if( uTopic < pubExtended ) {

		if( m_Opts.m_Twin ) {

			if( !m_fSent[uTopic] ) {

				if( m_fRunning ) {

					if( CMqttClientJson::GetPubMessage(uTopic, uTime, fTemp, uMode, pMsg) ) {

						pMsg->SetTopic(m_PubTopic + CPrintf("/?$rid=%u", 1+uTopic));

						return TRUE;
						}
					}
				}

			return FALSE;
			}
		else {
			if( CMqttClientJson::GetPubMessage(uTopic, uTime, fTemp, uMode, pMsg) ) {

				CCloudDataSet *pSet = m_pService->GetDataSet(uTopic);

				pMsg->SetTopic(m_PubTopic + pSet->m_Suffix + '/');

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// End of File
