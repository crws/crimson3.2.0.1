
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsAws.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for AWS Options
//

// Constructor

CMqttClientOptionsAws::CMqttClientOptionsAws(void)
{
	}

// Destructor

CMqttClientOptionsAws::~CMqttClientOptionsAws(void)
{
	}

// Initialization

void CMqttClientOptionsAws::Load(PCBYTE &pData)
{
	CMqttClientOptionsJson::Load(pData);

	m_Shadow = GetByte(pData);

	GetCoded(pData, m_Pub);

	GetCoded(pData, m_Sub);
}

// Config Fixup

BOOL CMqttClientOptionsAws::FixConfig(void)
{
	FixCoded(m_Pub, FALSE);

	FixCoded(m_Sub, FALSE);

	return CMqttClientOptionsJson::FixConfig();
}

// Attributes

CString CMqttClientOptionsAws::GetExtra(void) const
{
	return m_ClientId;
	}

// End of File
