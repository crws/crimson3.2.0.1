
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudServiceUbidots_HPP

#define	INCLUDE_CloudServiceUbidots_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CloudServiceCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMqttClientOptionsUbidots;

class CMqttClientGeneric;

//////////////////////////////////////////////////////////////////////////
//
// Ubidots MQTT Service
//

class CCloudServiceUbidots : public CCloudServiceCrimson
{
public:
	// Constructor
	CCloudServiceUbidots(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Service ID
	UINT GetID(void);
};

// End of File

#endif
