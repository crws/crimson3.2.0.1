

#include "Intern.hpp"

#include "Service.hpp"

#include "Matrix.hpp"

#include "MqttClientGoogle.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudServiceGoogle.hpp"

#include "MqttClientOptionsGoogle.hpp"

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Google MQTT Client
//

// Constructor

CMqttClientGoogle::CMqttClientGoogle(CCloudServiceGoogle *pService, CMqttClientOptionsGoogle &Opts) : CMqttClientJson(pService, Opts), m_Opts(Opts)
{
}

// Operations

BOOL CMqttClientGoogle::Open(void)
{
	m_Will.SetTopic(m_Opts.m_PubTopic);

	m_Will.SetData(GetWillData());

	return CMqttClientJson::Open();
}

// Client Hooks

void CMqttClientGoogle::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseInitial ) {

		if( HasWrites() ) {

			AddToSubList(subData, m_Opts.m_SubTopic);
		}
	}

	if( m_uPhase == phaseConnecting ) {

		m_Opts.MakeCredentials();
	}

	CMqttClientJson::OnClientPhaseChange();
}

BOOL CMqttClientGoogle::OnClientNewData(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode == subData ) {

		CMqttJsonData Json;

		if( pMsg->GetJson(Json) ) {

			OnWrite(Json, "");
		}

		return TRUE;
	}

	return TRUE;
}

// Publish Hook

BOOL CMqttClientGoogle::GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg)
{
	if( CMqttClientJson::GetPubMessage(uTopic, uTime, fTemp, uMode, pMsg) ) {

		pMsg->SetTopic(m_Opts.m_PubTopic);

		return TRUE;
	}

	return FALSE;
}

// End of File
