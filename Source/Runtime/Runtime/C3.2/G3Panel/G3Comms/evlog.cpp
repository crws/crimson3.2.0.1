
#include "intern.hpp"

#include "events.hpp"

#include "datalog.hpp"

#include "service.hpp"

#include "lang.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Event Logger
//

// Constants

static UINT const timeScale = 5;

static UINT const rateScale = 10;

// Instance Pointer

CEventLogger * CEventLogger::m_pThis = NULL;

// Constructor

CEventLogger::CEventLogger(CEventManager *pManager)
{
	m_LogToDisk  = 0;

	m_FileLimit  = 60;

	m_FileCount  = 12;

	m_WithBatch  = FALSE;

	m_SignLogs   = FALSE;

	m_LogToPort  = 0;

	m_pManager   = pManager;

	m_pHelp      = NULL;

	m_pSave      = Create_Mutex();

	m_uSize      = elements(m_Buffer);

	m_uSeq       = 1;

	m_uCount     = 0;

	m_fLost      = FALSE;

	m_fWrap      = FALSE;

	m_uHead      = 0;

	m_uTail      = 0;

	m_uSave      = 0;

	m_cSep	     = ',';

	m_Drive      = 0;

	m_pThis = this;

	StdSetRef();
}

// Destructor

CEventLogger::~CEventLogger(void)
{
	m_pSave->Release();

	delete m_pHelp;

	m_pThis = NULL;
}

// Initialization

void CEventLogger::Load(void)
{
	m_LogToDisk = CCommsSystem::m_pThis->m_pTags->m_LogToDisk;

	m_FileLimit = CCommsSystem::m_pThis->m_pTags->m_FileLimit;

	m_FileCount = CCommsSystem::m_pThis->m_pTags->m_FileCount;

	m_WithBatch = CCommsSystem::m_pThis->m_pTags->m_WithBatch;

	m_SignLogs  = CCommsSystem::m_pThis->m_pTags->m_SignLogs;

	m_LogToPort = CCommsSystem::m_pThis->m_pTags->m_LogToPort;

	m_Drive     = CCommsSystem::m_pThis->m_pTags->m_Drive;

	if( WhoHasFeature(rfLogToDisk) ) {

		if( m_LogToDisk ) {

			m_pHelp = New CLogHelper;

			m_pHelp->SetDrive(m_Drive, CDataLogger::m_pThis->m_BatchDrive);

			m_pHelp->SetBasePath("EVENTS");

			m_pHelp->SetFileLimit(m_FileLimit * 60);

			m_pHelp->SetFileCount(m_FileCount);

			m_pHelp->AddExtension("CSV");
		}
	}
}

// IUnknown

HRESULT CEventLogger::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IEventLogger);

	StdQueryInterface(IEventLogger);

	return E_NOINTERFACE;
}

ULONG CEventLogger::AddRef(void)
{
	StdAddRef();
}

ULONG CEventLogger::Release(void)
{
	StdRelease();
}

// IEventLogger

void CEventLogger::LockEventData(void)
{
	m_pManager->Lock();
}

void CEventLogger::FreeEventData(void)
{
	m_pManager->Free();
}

UINT CEventLogger::GetEventSequence(void)
{
	return m_uSeq;
}

UINT CEventLogger::GetEventCount(void)
{
	return m_uCount;
}

CUnicode CEventLogger::GetEventText(UINT uPos)
{
	UINT             uSlot = (m_uTail + m_uSize - 1 - uPos) % m_uSize;

	CEventInfo const &Info = m_Buffer[uSlot];

	if( !Info.m_HasText ) {

		CUnicode      Text    = L"Untitled Event";

		IEventSource *pSource = m_pManager->FindSource(Info.m_Source);

		if( pSource ) {

			if( pSource->GetEventText(Text, Info.m_Code) ) {

				return Text;
			}
		}

		return Text;
	}

	return Info.m_Text;
}

CUnicode CEventLogger::GetEventName(UINT uPos)
{
	UINT             uSlot = (m_uTail + m_uSize - 1 - uPos) % m_uSize;

	CEventInfo const &Info = m_Buffer[uSlot];

	switch( Info.m_Type ) {

		case eventEvent:  return L"Event";
		case eventAlarm:  return L"Alarm";
		case eventAccept: return L"Accept";
		case eventClear:  return L"Clear";
	}

	return L"";
}

DWORD CEventLogger::GetEventTime(UINT uPos)
{
	UINT             uSlot = (m_uTail + m_uSize - 1 - uPos) % m_uSize;

	CEventInfo const &Info = m_Buffer[uSlot];

	return Info.m_Time / 5;
}

UINT CEventLogger::GetEventType(UINT uPos)
{
	UINT             uSlot = (m_uTail + m_uSize - 1 - uPos) % m_uSize;

	CEventInfo const &Info = m_Buffer[uSlot];

	return Info.m_Type;
}

void CEventLogger::AddEvent(CEventInfo const &Info)
{
	LogEvent(Info, FALSE);
}

// Attributes

CUnicode CEventLogger::GetLastText(BOOL fAll)
{
	UINT uPos = FindLastIndex(fAll);

	return (uPos < NOTHING) ? GetEventText(uPos) : L"";
}

CUnicode CEventLogger::GetLastType(BOOL fAll)
{
	UINT uPos = FindLastIndex(fAll);

	return (uPos < NOTHING) ? GetEventName(uPos) : L"";
}

DWORD CEventLogger::GetLastTime(BOOL fAll)
{
	UINT uPos = FindLastIndex(fAll);

	return (uPos < NOTHING) ? GetEventTime(uPos) : 0;
}

// Operations

void CEventLogger::LogCheck(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( m_pHelp ) {

			m_pHelp->InitData();
		}
	}
}

void CEventLogger::LogLoad(void)
{
	if( m_pManager->Lock() ) {

		g_pEvStg->ReadMemory(this);

		m_uSave = m_uTail;

		m_pManager->Free();
	}
}

void CEventLogger::LogSave(void)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( m_pHelp ) {

			if( m_pSave->Wait(FOREVER) ) {

				CAutoGuard Guard;

				if( TRUE ) {

					m_cSep = char(CCommsSystem::m_pThis->m_pLang->GetListSepChar());
				}

				if( m_pHelp->SaveInit() ) {

					CFastFile csv[2];

					if( m_SignLogs ) {

						csv[0].SignEnable();

						csv[1].SignEnable();
					}

					while( m_uSave != m_uTail ) {

						DWORD Time     = m_Buffer[m_uSave].m_Time;

						BOOL  fHead[2] = { FALSE, FALSE };

						if( !m_pHelp->FindFiles(csv, fHead, 0, Time) ) {

							break;
						}

						for( UINT n = 0; n < 2; n++ ) {

							if( fHead[n] ) {

								WriteHead(csv[n]);
							}

							WriteTime(csv[n]);

							WriteData(csv[n]);

							csv[n].EndOfLine();
						}

						m_uSave = (m_uSave + 1) % m_uSize;

						m_pHelp->SaveStep(Time);
					}

					m_pHelp->SaveDone();

					csv[0].Close();
					csv[1].Close();
				}

				m_pSave->Free();
			}

			return;
		}
	}

	m_uSave = m_uTail;
}

void CEventLogger::LogNewBatch(UINT uSlot, DWORD Time, PCTXT pName)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( m_pHelp ) {

			if( m_WithBatch == uSlot + 1 ) {

				UINT uCount = CDataLogger::m_pThis->m_BatchCount;

				m_pHelp->SetBatchInfo(uSlot, uCount);

				m_pHelp->NewBatch(Time, pName);
			}
		}
	}
}

BOOL CEventLogger::LogEvent(CEventInfo const &Info, BOOL fMemory)
{
	if( TRUE ) {

		UINT uNext = (m_uTail + 1) % m_uSize;

		if( m_LogToDisk ) {

			if( uNext == m_uSave ) {

				m_fLost = TRUE;

				return FALSE;
			}
		}

		if( uNext == m_uHead ) {

			m_uHead  = (m_uHead + 1) % m_uSize;

			m_uCount = m_uCount - 1;

			m_fWrap  = TRUE;
		}

		m_Buffer[m_uTail] = Info;

		m_uTail  = uNext;

		m_uSeq   = m_uSeq   + 1;

		m_uCount = m_uCount + 1;
	}

	if( fMemory ) {

		g_pEvStg->WriteMemory(Info);

		PrintEvent(Info);
	}

	return TRUE;
}

BOOL CEventLogger::LogInitEvent(CEventInfo const &Info)
{
	UINT uNext = (m_uTail + 1) % m_uSize;

	if( uNext == m_uHead ) {

		m_uHead  = (m_uHead + 1) % m_uSize;

		m_uCount = m_uCount - 1;

		m_fWrap  = TRUE;
	}

	m_Buffer[m_uTail] = Info;

	m_uTail  = uNext;

	m_uSave  = m_uTail;

	m_uSeq   = m_uSeq   + 1;

	m_uCount = m_uCount + 1;

	return TRUE;
}

void CEventLogger::LogClear(void)
{
	if( m_pManager->Lock() ) {

		if( m_pSave->Wait(FOREVER) ) {

			m_uHead  = 0;

			m_uTail  = 0;

			m_uSave  = 0;

			m_uCount = 0;

			for( UINT n = 0; n < m_uSize; n++ ) {

				m_Buffer[n].m_Text.Empty();
			}

			m_fLost = FALSE;

			m_fWrap = FALSE;

			m_uSeq  = m_uSeq + 1;

			g_pEvStg->ClearMemory();

			m_pSave->Free();
		}

		m_pManager->Free();
	}
}

// Port Output

void CEventLogger::PrintEvent(CEventInfo const &Info)
{
	if( m_LogToPort ) {

		// TODO -- Format time according to format object?

		DWORD Time = Info.m_Time;

		DWORD Secs = Time / 5;

		CString Line;

		Line.Printf("%2.2u/%2.2u/%4.4u  %2.2u:%2.2u:%2.2u",
			    GetMonth(Secs),
			    GetDate(Secs),
			    GetYear(Secs),
			    GetHour(Secs),
			    GetMin(Secs),
			    GetSec(Secs)
		);

		Line += "  ";

		switch( Info.m_Type ) {

			case 1:
				Line += " ***  ";
				break;
			case 2:
				Line += " Acc  ";
				break;

			case 3:
				Line += " Clr  ";
				break;
		}

		Line += "  ";

		CUnicode Text = Info.m_Text;

		if( !Info.m_HasText ) {

			IEventSource *pSource = m_pManager->FindSource(Info.m_Source);

			if( pSource ) {

				pSource->GetEventText(Text, Info.m_Code);
			}
		}

		Line += UniConvert(Text);

		Line += "\r\n";

		ICommsRawPort * pPort = CCommsSystem::m_pThis->m_pComms->FindRawPort(m_LogToPort);

		if( pPort ) {

			pPort->Write(PBYTE(PCTXT(Line)), strlen(Line), FOREVER);
		}
	}
}

// File Output

BOOL CEventLogger::WriteHead(CFastFile &csv)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		CString Line;

		Line += "Date";

		Line += m_cSep;

		Line += "Time";

		Line += m_cSep;

		Line += "Type";

		Line += m_cSep;

		Line += "Description";

		if( m_SignLogs ) {

			Line += m_cSep;

			Line += "Signature";
		}

		Line += "\r\n";

		csv.Write(PCTXT(Line));
	}

	return TRUE;
}

void CEventLogger::WriteTime(CFastFile &csv)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( csv.IsValid() ) {

			DWORD Time = m_Buffer[m_uSave].m_Time;

			DWORD Secs = Time / timeScale;

			csv.Printf("%4.4u/%2.2u/%2.2u%c%2.2u:%2.2u:%2.2u",
				   GetYear(Secs),
				   GetMonth(Secs),
				   GetDate(Secs),
				   m_cSep,
				   GetHour(Secs),
				   GetMin(Secs),
				   GetSec(Secs)
			);
		}
	}
}

void CEventLogger::WriteData(CFastFile &csv)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		if( csv.IsValid() ) {

			CEventInfo const &Info = m_Buffer[m_uSave];

			CUnicode          Text = L"Untitled Event";

			if( !Info.m_HasText ) {

				IEventSource *pSource = m_pManager->FindSource(Info.m_Source);

				if( pSource ) {

					pSource->GetEventText(Text, Info.m_Code);
				}
			}
			else
				Text = Info.m_Text;

			csv.Write(m_cSep);

			switch( Info.m_Type ) {

				case eventEvent:  csv.Write("Event"); break;
				case eventAlarm:  csv.Write("Alarm"); break;
				case eventAccept: csv.Write("Accept"); break;
				case eventClear:  csv.Write("Clear"); break;
			}

			csv.Write(m_cSep);

			csv.Write(Text);

			csv.Write("\r\n");
		}
	}
}

// Implementation

UINT CEventLogger::FindLastIndex(BOOL fAll)
{
	if( fAll ) {

		if( m_uCount ) {

			return 0;
		}

		return NOTHING;
	}

	for( UINT uPos = 0; uPos < m_uCount; uPos++ ) {

		UINT n = (m_uTail + m_uSize - 1 - uPos) % m_uSize;

		if( m_Buffer[n].m_Type == eventEvent ) {

			return uPos;
		}
	}

	return NOTHING;
}

// End of File
