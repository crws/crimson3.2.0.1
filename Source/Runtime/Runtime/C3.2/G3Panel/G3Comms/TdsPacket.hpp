
//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TDS_TdsPacket_HPP

#define INCLUDE_TDS_TdsPacket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TdsBytes.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TDS Token Stream Types
//

#define TDS_ROW_TOKEN		0xD1
#define TDS_COLMETADATA_TOKEN	0x81

#define	TDS_DONE_TOKEN		0xFD
#define	TDS_ENVCHANGE_TOKEN	0xE3
#define TDS_ERROR_TOKEN		0xAA
#define TDS_INFO_TOKEN		0xAB
#define TDS_LOGIN_ACK_TOKEN	0xAD

#define TDS_HEADER_BYTES        8


//////////////////////////////////////////////////////////////////////////
//
// TDS Packet Types
//

enum TdsType
{
	typeNull	= 0,
	typeSqlBatch	= 1,
	typeOldLogin	= 2,
	typeRpc		= 3,
	typeResponse	= 4,
	typeAttention	= 6,
	typeBulkLoad	= 7,
	typeTransReq	= 14,
	typeLogin7	= 16,
	typeSSPI	= 17,
	typePreLogin	= 18,
	};


//////////////////////////////////////////////////////////////////////////
//
// TDS Packet Status
//

enum TdsStatus
{
	statusNormal	= 0x00,
	statusEnd	= 0x01,
	statusIgnore	= 0x02,
	statusReset1	= 0x08,
	statusReset2	= 0x10
	};


//////////////////////////////////////////////////////////////////////////
//
// TDS Data Type
//

enum TdsDataType
{
	typeIntNType		= 0x26,
	typeFltNType		= 0x6D,
	typeDateTimNType	= 0x6F,
	typeBigVarChrType	= 0xA7
	};


//////////////////////////////////////////////////////////////////////////
//
// TDS Packet
//

class CTdsPacket
{
	public:
		// Constructors
		CTdsPacket(void);
		CTdsPacket(TdsType Type);
		CTdsPacket(TdsType Type, UINT BufferSize);
		CTdsPacket(TdsType Type, UINT BufferSize, bool fAddHeader);

		// Attributes
		TdsType GetType(void) const;
		PCBYTE  GetData(void) const;
		PBYTE	GetDataMod(void) const;
		UINT    GetSize(void) const;

		// Read
		BYTE   GetByte(UINT &uPos);
		FLOAT  GetFloat(UINT &uPos);
		DOUBLE GetDouble(UINT &uPos);
		USHORT GetUShortReverse(UINT &uPos);
		ULONG  GetULongReverse(UINT &uPos);
		ULONGLONG  GetULongLongReverse(UINT &uPos);
		DOUBLE GetDoubleReverse(UINT &uPos);
		bool   GetByteCharacterData(CHAR* pOutputString, USHORT uLength, UINT &uPos);
		bool   GetReverseCharacterData(CHAR* pOutputString, USHORT uLength, UINT &uPos);
		static UINT GetPacketID(void);

		// Parsing
		virtual int Parse(PCBYTE pData, UINT uSize);

		// Operations
		void SetSpid(WORD Spid);
		void EndPacket(void);
		void Dump(void);
		void DumpASCII(void);

		// Packet Building
		void AddByte(BYTE bData);
		void AddUShort(USHORT uData);
		void AddULong(ULONG uData);
		void AddULongLong(ULONGLONG uData);
		void AddUShortReverse(USHORT uData);
		void AddULongReverse(ULONG uData);
		void AddULongLongReverse(ULONGLONG uData);
		void AddFloat(FLOAT uData);
		void AddFloatReverse(FLOAT uData);
		void AddDouble(DOUBLE dData);
		void AddDoubleReverse(DOUBLE dData);
		void AddText(PCSTR pText);
		void AddReverseCharText(PCSTR pText);
		void AddTextNoTerminator(PCSTR pText);
		void AddTextNoTerminator(PCSTR pText, UINT uSize);
		void RemoveHead(UINT uSize);

	protected:
		// Data Members
		TdsType     m_Type;
		WORD        m_Spid;
		CTdsBytes   m_Data;
		static UINT m_uPacketID;
	};

// End of File

#endif
