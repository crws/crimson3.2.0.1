
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Sync Object Implementation
//

class CSyncHelper : public ISyncHelper 
{
	public:
		// Constructor
		CSyncHelper(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ISyncHelper
		IEvent     * METHOD CreateAutoEvent(void);
		IEvent     * METHOD CreateManualEvent(void);
		ISemaphore * METHOD CreateSemaphore(UINT uCount);
		IMutex     * METHOD CreateMutex(void);

	protected:
		// Data Members
		ULONG m_uRefs;
	};

//////////////////////////////////////////////////////////////////////////
//
// Sync Helper Implementation
//

// Instantiator

global ISyncHelper * Create_SyncHelper(void)
{
	return New CSyncHelper;
	}

// Constructor

CSyncHelper::CSyncHelper(void)
{
	StdSetRef();
	}

// IUnknown

HRESULT CSyncHelper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ISyncHelper);

	StdQueryInterface(ISyncHelper);

	return E_NOINTERFACE;
	}

ULONG CSyncHelper::AddRef(void)
{
	StdAddRef();
	}

ULONG CSyncHelper::Release(void)
{
	StdRelease();
	}

// ISyncHelper

IEvent * METHOD CSyncHelper::CreateAutoEvent(void)
{
	return Create_AutoEvent();
	}

IEvent * METHOD CSyncHelper::CreateManualEvent(void)
{
	return Create_ManualEvent();
	}

ISemaphore * METHOD CSyncHelper::CreateSemaphore(UINT uCount)
{
	return Create_Semaphore(uCount);
	}

IMutex * METHOD CSyncHelper::CreateMutex(void)
{
	return Create_Mutex();
	}

// End of File
