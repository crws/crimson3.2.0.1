
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_QUEUE_HPP
	
#define	INCLUDE_QUEUE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Write Entry
//

struct CWrite
{
	CCommsSysBlock * m_pBlock;
	INT		 m_nPos;
	DWORD		 m_Data;
	DWORD		 m_dwMask;
	};

//////////////////////////////////////////////////////////////////////////
//
// Write Queue
//

class CWriteQueue
{
	public:
		// Constructor
		CWriteQueue(CCommsDevice *pDevice, UINT uSize, UINT uShare);

		// Destructor
		~CWriteQueue(void);

		// Serialization
		void LockQueue(void) const;
		void FreeQueue(void) const;

		// Attributes
		BOOL IsEmpty(void) const;
		BOOL IsFilling(void) const;
		UINT GetCount(void) const;
		UINT GetLimit(void) const;

		// Operations
		BOOL GetEntry(UINT uSlot, CWrite &Write);
		void DropLast(void);
		void WriteDone(UINT uCount);
		void WriteFail(void);
		void StepHead(UINT nCount);
		void AddWrite(CWrite const &Write);
		void AddWrite(CCommsSysBlock *pBlock, INT nPos, DWORD Data, DWORD dwMask);
		void AddWrite(CCommsSysBlock *pBlock, INT nPos, DWORD Data);
		void FlushEntry(CCommsSysBlock *pBlock, INT nPos);
		void FlushBlock(CCommsSysBlock *pBlock);
		void FlushAll(void);
		void EmptyAll(void);

	protected:
		// Data Members
		CCommsDevice * m_pDevice;
		CWrite       * m_pData;
		UINT	       m_uSize;
		UINT	       m_uShare;
		IMutex	     * m_pLock;
		UINT	       m_uHead;
		UINT	       m_uTail;
		UINT	       m_uBusy;

		// Implementation
		BOOL Throttle(void);
	};

// End of File

#endif
