
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "service.hpp"

#include "datalog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Log Helper
//

// Static Data

BOOL CLogHelper::m_fSet = FALSE;

// Constructor

CLogHelper::CLogHelper(void)
{
	m_uType     = 0;

	m_uSlot     = 0;

	m_uBatch    = 0;

	m_BatchName = "";

	m_NewStart  = 0;

	m_uMail     = NOTHING;

	m_Volume    = NOTHING;

	m_Drive     = "C:";

	m_Batch     = "C:\\BATCH";
}

// Destructor

CLogHelper::~CLogHelper(void)
{
}

// Configuration

void CLogHelper::SetBasePath(PCTXT pBase)
{
	m_Base = pBase;
}

void CLogHelper::SetFileCount(UINT uCount)
{
	m_uCount = uCount;
}

void CLogHelper::SetFileLimit(UINT uLimit)
{
	m_uLimit = uLimit;
}

void CLogHelper::SetBatchInfo(UINT uSlot, UINT uBatch)
{
	m_uSlot  = uSlot;

	m_uBatch = uBatch;
}

void CLogHelper::AddExtension(PCTXT pType)
{
	m_Type[m_uType++] = pType;
}

void CLogHelper::SetMail(UINT uMail)
{
	m_uMail = uMail;
}

void CLogHelper::SetDrive(UINT uDrive, UINT uBatchDrive)
{
	m_Drive.Printf("%c:", 'C' + uDrive);

	m_Batch.Printf("%c:\\BATCH", 'C' + uBatchDrive);
}

// Operations

void CLogHelper::InitData(void)
{
	LockSession();

	CheckVolume();

	FreeSession();
}

BOOL CLogHelper::SaveInit(void)
{
	LockSession();

	if( FindDirectories() ) {

		m_Last = 0;

		return TRUE;
	}

	FreeSession();

	return FALSE;
}

BOOL CLogHelper::FindFiles(CFastFile *pFile, BOOL *pHead, UINT uType, DWORD Time)
{
	if( MakeNew(Time) ) {

		for( UINT uPath = 0; uPath < 2; uPath++ ) {

			CString Name = MakeName(uType, uPath, Time);

			if( !Name.IsEmpty() ) {

				PCTXT pName = PCTXT(Name);

				if( pFile[uPath].Switch(pHead[uPath], pName) ) {

					if( uPath == 0 && pHead[uPath] ) {

						CString Path = MakePath(uPath);

						chdir(Path);

						ArchiveFiles();

						DeleteOldFiles();
					}

					continue;
				}

				return FALSE;
			}
		}
	}

	return TRUE;
}

void CLogHelper::SaveStep(DWORD Time)
{
	m_Last = Time;
}

void CLogHelper::SaveDone(void)
{
	FreeSession();
}

void CLogHelper::NewBatch(DWORD Time, PCTXT pName)
{
	m_NewBatch = pName;

	m_NewStart = Time;
}

// Static Config

void CLogHelper::UseSetLayout(BOOL fSet)
{
	m_fSet = fSet;
}

// Name Generation

CString CLogHelper::MakeName(UINT uType, UINT uPath, DWORD Time)
{
	if( !uPath || !uType ) {

		CString Path = MakePath(uPath);

		if( !Path.IsEmpty() ) {

			CString Name = "";

			DWORD   Secs = Time / 5;

			DWORD   Base = Secs / m_uLimit * m_uLimit;

			if( m_uLimit < 60 * 60 ) {

				Name.Printf("%2.2u%2.2u%2.2u%2.2u",
					    GetMonth(Base) % 100,
					    GetDate(Base) % 100,
					    GetHour(Base) % 100,
					    GetMin(Base) % 100
				);
			}
			else {
				Name.Printf("%2.2u%2.2u%2.2u%2.2u",
					    GetYear(Base) % 100,
					    GetMonth(Base) % 100,
					    GetDate(Base) % 100,
					    GetHour(Base) % 100
				);
			}

			Name += '.';

			Name += m_Type[uType];

			return Path + "\\" + Name;
		}
	}

	return "";
}

CString CLogHelper::MakePath(UINT uPath)
{
	CString Path;

	if( uPath == 0 ) {

		Path = m_Drive + "\\LOGS\\" + m_Base;
	}

	if( uPath == 1 ) {

		if( m_uBatch ) {

			if( !m_BatchName.IsEmpty() ) {

				CString Slot;

				if( m_fSet || m_uSlot ) {

					Slot.Printf("SET%u\\", 1 + m_uSlot);
				}

				Path = m_Batch + "\\" + Slot + m_BatchName + "\\" + m_Base;
			}
		}
	}

	return Path;
}

// Implementation

BOOL CLogHelper::MakeNew(DWORD Time)
{
	if( m_NewStart && Time >= m_NewStart ) {

		m_NewStart  = 0;

		m_BatchName = m_NewBatch;

		CheckFiles();

		DeleteOldBatches();

		return TRUE;
	}

	if( m_Last == 0 ) {

		CheckVolume();

		return TRUE;
	}

	if( m_Last / 5 / m_uLimit != Time / 5 / m_uLimit ) {

		CheckVolume();

		return TRUE;
	}

	return FALSE;
}

void CLogHelper::CheckVolume(void)
{
	AfxGetAutoObject(pUtils, "aeon.filesupport", 0, IFileUtilities);

	DWORD Volume = pUtils ? pUtils->GetDiskIdent('C') : 0x12345678;

	if( m_Volume != Volume ) {

		CheckFiles();

		m_Volume = Volume;
	}
}

void CLogHelper::CheckFiles(void)
{
	if( FindDirectories() ) {

		for( UINT uPath = 0; uPath < 2; uPath++ ) {

			CString Path = MakePath(uPath);

			if( !Path.IsEmpty() ) {

				chdir(Path);

				CheckGUID();

				if( uPath == 0 ) {

					DeleteOldFiles();
				}
			}
		}
	}
}

BOOL CLogHelper::FindDirectories(void)
{
	for( UINT uPath = 0; uPath < 2; uPath++ ) {

		CString Path = MakePath(uPath);

		if( !Path.IsEmpty() ) {

			if( !chdir(Path) ) {

				continue;
			}

			if( mkdir(Path, 0) ) {

				return FALSE;
			}
		}
	}

	return TRUE;
}

void CLogHelper::CheckGUID(void)
{
	BYTE bGuid[16];

	g_pDbase->GetVersion(bGuid);

	if( true ) {

		CAutoFile File("guid.bin", "r");

		if( File ) {

			BYTE bData[16];

			if( File.Read(bData, 16) == 16 ) {

				if( !memcmp(bGuid, bData, 16) ) {

					return;
				}
			}
		}
	}

	if( true ) {

		EmptyCurrentDir();

		CAutoFile File("guid.bin", "w");

		if( File ) {

			File.Write(bGuid, 16);
		}
	}
}

void CLogHelper::DeleteOldFiles(void)
{
	CAutoDirentList List;

	if( List.ScanFilesByTimeRev(".") ) {

		UINT uFind[4];

		for( UINT t = 0; t < m_uType; t++ ) {

			uFind[t] = 0;
		}

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CFilename Name = List[n]->d_name;

			BOOL      fDel = TRUE;

			for( UINT t = 0; t < m_uType; t++ ) {

				if( m_Type[t] == Name.GetType() ) {

					if( uFind[t]++ < m_uCount ) {

						fDel = FALSE;
					}

					break;
				}
			}

			if( fDel && stricmp(Name.GetType(), "bin") ) {

				unlink(Name);
			}
		}
	}
}

void CLogHelper::DeleteOldBatches(void)
{
	// REV3 -- This will screw-up if a previous batch is re-activated and
	// it is the oldest existing subdirectory in list. We'll end up killing
	// the one we really want to keep! The fix to touch the directory time
	// stamp somehow, perhaps when we check its GUID?

	chdir(m_Batch);

	if( m_fSet || m_uSlot ) {

		CString Slot;

		Slot.Printf("SET%u", 1 + m_uSlot);

		chdir(Slot);
	}

	DeleteContents(m_uBatch);
}

void CLogHelper::DeleteContents(UINT uLeave)
{
	CAutoDirentList List;

	if( List.ScanByTimeFwd(".") ) {

		UINT uCount = 0;

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			struct dirent *d = List[n];

			if( d->d_type == DT_DIR ) {

				if( !strcmp(d->d_name, ".") || !strcmp(d->d_name, "..") ) {

					continue;
				}

				if( ++uCount > uLeave ) {

					chdir(d->d_name);

					EmptyCurrentDir();

					chdir("..");

					rmdir(d->d_name);
				}
			}

			if( d->d_type == DT_REG ) {

				unlink(d->d_name);
			}
		}
	}

	chdir("..");
}

void CLogHelper::ArchiveFiles(void)
{
	if( g_pServiceMail ) {

		if( m_uMail < NOTHING ) {

			CAutoDirentList List;

			if( List.ScanFilesByTimeFwd(".") ) {

				for( UINT n = 0; n < List.GetCount(); n++ ) {

					CFilename Name = List[n]->d_name;

					if( !stricmp(Name.GetType(), "bin") ) {

						continue;
					}

					if( false ) {

						// !!!FS!!! No archive bit in Linux...

						char path[MAX_PATH];

						PathMakeAbsolute(path, Name);

						g_pServiceMail->SendFile(m_uMail, path);
					}
				}
			}
		}
	}
}

void CLogHelper::EmptyCurrentDir(void)
{
	CAutoDirentList List;

	if( List.Scan(".") ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			struct dirent *d = List[n];

			if( d->d_type == DT_DIR ) {

				if( !strcmp(d->d_name, ".") || !strcmp(d->d_name, "..") ) {

					continue;
				}

				if( !chdir(d->d_name) ) {

					EmptyCurrentDir();

					chdir("..");
				}

				rmdir(d->d_name);
			}

			if( d->d_type == DT_REG ) {

				unlink(d->d_name);
			}
		}
	}
}

void CLogHelper::LockSession(void)
{
	// !!!FS!!! Is this the correct approach?

	GuardThread(true);
}

void CLogHelper::FreeSession(void)
{
	GuardThread(false);
}

// End of File
