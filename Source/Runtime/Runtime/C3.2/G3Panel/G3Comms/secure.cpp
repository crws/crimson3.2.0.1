
#include "intern.hpp"

#include "secure.hpp"

#include "service.hpp"

#include "..\..\Crimson\C3Core\rc4.hpp"

#include "lang.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Security Manager
//

// Cursor Types

#define	cursorLast	0x8000
#define	cursorNone	0x8003

// Event Codes

#define	secFingerOkay	(VK_SECURE+0)
#define	secFingerFail	(VK_SECURE+1)
#define	secCardOkay	(VK_SECURE+2)
#define	secCardFail	(VK_SECURE+3)
#define	secFingerLift	(VK_SECURE+4)
#define	secFingerDown	(VK_SECURE+5)

// UI States

#define	stateAuthInit	0x00
#define	stateAuthCheck	0x01
#define	stateAuthPrompt	0x02
#define	stateGetUser	0x03
#define	stateGetPass	0x04
#define	stateAuthDone	0x05
#define	stateBadCred	0x06
#define	stateBadRights	0x07
#define	stateAddCard	0x08
#define	stateAddFinger	0x09
#define	stateSetPass	0x0A
#define	stateRepPass	0x0B
#define	stateSetDone	0x0C
#define	stateSetError	0x0D

// Constructor

CSecurityManager::CSecurityManager(void)
{
	m_pUsers    = New CUserList;
	m_DefMapped = allowAnyone | allowProgram;
	m_DefLocal  = allowAnyone | allowProgram;
	m_DefPage   = allowAnyone;
	m_LogMapped = 0;
	m_LogLocal  = 0;
	m_LogEnable = 0;
	m_FileLimit = 60;
	m_FileCount = 24;
	m_WithBatch = 0;
	m_SignLogs  = 0;
	m_UserTime  = 600;
	m_UserClear = FALSE;
	m_CheckTime = 5;
	m_Every     = FALSE;
	m_cSep	    = ',';
	m_Drive     = 0;
}

// Destructor

CSecurityManager::~CSecurityManager(void)
{
	delete m_pUsers;
}

// Initialization

void CSecurityManager::Load(PCBYTE &pData)
{
	ValidateLoad("CSecurityManager", pData);

	m_DefLocal  = GetLong(pData);
	m_DefMapped = GetLong(pData);
	m_DefPage   = GetLong(pData);
	m_LogLocal  = GetByte(pData);
	m_LogMapped = GetByte(pData);
	m_LogEnable = GetByte(pData);
	m_FileLimit = GetWord(pData);
	m_FileCount = GetWord(pData);
	m_WithBatch = GetByte(pData);
	m_SignLogs  = GetByte(pData);
	m_UserTime  = GetWord(pData);
	m_UserClear = GetByte(pData);
	m_CheckTime = GetWord(pData);

	m_pUsers->Load(pData);

	m_Drive     = GetByte(pData);
}

// Management

void CSecurityManager::Init(void)
{
	m_pUI	     = NULL;

	m_hUI	     = NULL;

	m_fBusy      = FALSE;

	m_Event      = 0;

	m_fAsked     = FALSE;

	m_fFailed    = FALSE;

	m_fMaint     = FALSE;

	m_uTime	     = 0;

	m_uCheckCode = 0;

	m_uCheckTime = 0;

	m_pUser	     = NULL;

	m_pRemote    = NULL;

	m_uEnroll    = 0;

	m_pAccept    = Create_ManualEvent();

	m_pLock	     = Create_Rutex();

	m_pHelp      = NULL;

	m_pWait      = Create_Semaphore(240);

	m_pHead      = NULL;

	m_pTail      = NULL;

	if( m_LogEnable ) {

		if( WhoHasFeature(rfLogToDisk) ) {

			m_pHelp = New CLogHelper;

			m_pHelp->SetDrive(m_Drive, CDataLogger::m_pThis->m_BatchDrive);

			m_pHelp->SetBasePath("SECURE");

			m_pHelp->SetFileCount(m_FileCount);

			m_pHelp->SetFileLimit(m_FileLimit * 60);

			m_pHelp->AddExtension("CSV");
		}
	}

	memset(m_pTaskUser, 0, sizeof(m_pTaskUser));

	memset(m_uTaskList, 0, sizeof(m_uTaskList));

	m_pUsers->LoadCreds();
}

void CSecurityManager::Term(void)
{
	AfxRelease(m_pWait);

	AfxRelease(m_pAccept);

	AfxRelease(m_pLock);

	delete m_pHelp;
}

// Task List

void CSecurityManager::GetTaskList(CTaskList &List)
{
}

// Task Entries

void CSecurityManager::TaskInit(UINT uID)
{
}

void CSecurityManager::TaskExec(UINT uID)
{
}

void CSecurityManager::TaskStop(UINT uID)
{
}

void CSecurityManager::TaskTerm(UINT uID)
{
}

// Attributes

UINT CSecurityManager::GetUserCount(void)
{
	UINT uCount = 0;

	for( UINT n = 0; n < m_pUsers->m_uCount; n++ ) {

		if( m_pUsers->m_ppUser[n] ) {

			uCount++;
		}
	}

	return uCount;
}

UINT CSecurityManager::GetUserIndex(PCTXT pUser)
{
	for( UINT n = 0; n < m_pUsers->m_uCount; n++ ) {

		CUserItem *pScan = m_pUsers->m_ppUser[n];

		if( pScan ) {

			if( !stricmp(pUser, pScan->GetUserName()) ) {

				return n;
			}
		}
	}

	return NOTHING;
}

CString CSecurityManager::GetUserRealName(UINT uIndex)
{
	for( UINT n = 0; n < m_pUsers->m_uCount; n++ ) {

		if( m_pUsers->m_ppUser[n] ) {

			if( !uIndex-- ) {

				return UniConvert(m_pUsers->m_ppUser[n]->GetRealName());
			}
		}
	}

	return "";
}

CUserItem * CSecurityManager::GetUserPtr(UINT uIndex)
{
	for( UINT n = 0; n < m_pUsers->m_uCount; n++ ) {

		if( m_pUsers->m_ppUser[n] ) {

			if( !uIndex-- ) {

				return m_pUsers->m_ppUser[n];
			}
		}
	}

	return NULL;
}

CString CSecurityManager::GetCurrentUserName(void)
{
	return (m_fAsked && m_pUser) ? m_pUser->GetUserName() : "";
}

CString CSecurityManager::GetCurrentUserRealName(void)
{
	return (m_fAsked && m_pUser) ? UniConvert(m_pUser->GetRealName()) : "";
}

DWORD CSecurityManager::GetCurrentUserRights(void)
{
	return (m_fAsked && m_pUser) ? m_pUser->m_Rights : 0;
}

// Operations

BOOL CSecurityManager::SaveDatabase(UINT uMode, PCTXT pName)
{
	CAutoFile File(pName, "r+", "w+");

	if( File ) {

		if( uMode == 0 || uMode == 1 ) {

			UINT  uBuff     = 128 * 1024;

			CAutoArray<BYTE> pData(uBuff);

			PBYTE pSave     = pData;

			char  sMark[13] = "xxxxC3SEC000";

			sMark[0] = BYTE(rand());
			sMark[1] = BYTE(rand());
			sMark[2] = BYTE(rand());
			sMark[3] = BYTE(rand());

			sMark[11] = '0' + uMode;

			memcpy(pSave, sMark, 12);

			pSave += 12;

			g_pDbase->GetVersion(pSave);

			pSave += 16;

			for( UINT n = 0; n < m_pUsers->m_uCount; n++ ) {

				CUserItem *pUser = m_pUsers->m_ppUser[n];

				if( pUser ) {

					*pSave++ = LOBYTE(n);
					*pSave++ = HIBYTE(n);

					memcpy(pSave,
					       &pUser->m_Cred,
					       sizeof(pUser->m_Cred)
					);

					pSave += sizeof(CUserItem::CCred);
				}
			}

			*pSave++ = 0xFF;
			*pSave++ = 0xFF;

			if( uMode == 1 ) {

				CItemInfo Info;

				UINT   hItem = CCommsSystem::m_pThis->m_hSecure;

				PCVOID pData = g_pDbase->LockItem(hItem, Info);

				*pSave++ = LOBYTE(LOWORD(Info.m_uSize));

				*pSave++ = HIBYTE(LOWORD(Info.m_uSize));

				memcpy(pSave, pData, Info.m_uSize);

				pSave += Info.m_uSize;

				g_pDbase->FreeItem(hItem);
			}

			UINT  uSize = pSave - pData;

			rc4_key key;

			prepare_key(pData, 12, &key);

			rc4(pData+4, uSize-4, &key);

			File.Write(pData, uSize);

			File.Truncate();

			return TRUE;
		}

		File.Close();
	}

	unlink(pName);

	return FALSE;
}

BOOL CSecurityManager::LoadDatabase(UINT uMode, PCTXT pName)
{
	CAutoFile File(pName, "r");

	if( File ) {

		if( uMode == 0 || uMode == 1 ) {

			UINT  uBuff = 128 * 1024;

			CAutoArray<BYTE> pData(uBuff);

			UINT  uSize = File.Read(pData, uBuff);

			File.Close();

			if( uSize < uBuff ) {

				char sMark[13] = "xxxxC3SEC000";

				sMark[0] = pData[0];
				sMark[1] = pData[1];
				sMark[2] = pData[2];
				sMark[3] = pData[3];

				sMark[11] = '0' + uMode;

				rc4_key key;

				prepare_key(PBYTE(sMark), 12, &key);

				rc4(pData+4, uSize-4, &key);

				if( !memcmp(pData, sMark, 12) ) {

					PBYTE pScan = pData + 12;

					if( uMode == 1 ) {

						BYTE bGuid[16];

						g_pDbase->GetVersion(bGuid);

						if( !memcmp(pScan, bGuid, 16) ) {

							pScan += 16;

							for( ;;) {

								BYTE lo = *pScan++;
								BYTE hi = *pScan++;

								UINT n  = MAKEWORD(lo, hi);

								if( n == 0xFFFF ) {

									break;
								}

								if( n < m_pUsers->m_uCount ) {

									CUserItem *pUser = m_pUsers->m_ppUser[n];

									if( pUser ) {

										memcpy(&pUser->m_Cred,
										       pScan,
										       sizeof(pUser->m_Cred)
										);
									}
								}

								pScan += sizeof(CUserItem::CCred);
							}

							if( uMode == 1 ) {

								BYTE lo = *pScan++;
								BYTE hi = *pScan++;

								UINT hItem = CCommsSystem::m_pThis->m_hSecure;

								UINT uSize  = MAKEWORD(lo, hi);

								m_pUsers->SaveCreds();

								g_pPxe->SystemUpdate(hItem, pScan, uSize);

								for( ;;) Sleep(FOREVER);
							}

							m_pUsers->SaveCreds();

							return TRUE;
						}
					}
				}
			}
		}
	}

	return FALSE;
}

// Logging Operations

void CSecurityManager::LogNewBatch(UINT uSlot, DWORD Time, PCTXT pName)
{
	if( m_pHelp ) {

		if( m_WithBatch == uSlot + 1 ) {

			UINT uCount = CDataLogger::m_pThis->m_BatchCount;

			m_pHelp->SetBatchInfo(uSlot, uCount);

			m_pHelp->NewBatch(Time, pName);
		}
	}
}

void CSecurityManager::LogCheck(void)
{
	if( m_pHelp ) {

		m_pHelp->InitData();
	}
}

void CSecurityManager::LogSave(void)
{
	if( m_pHelp ) {

		if( m_pHead ) {

			UpdateSep();

			if( m_pHelp->SaveInit() ) {

				CAutoGuard Guard;

				UINT uCount = 0;

				CFastFile csv[2];

				if( m_SignLogs ) {

					csv[0].SignEnable();

					csv[1].SignEnable();
				}

				while( m_pHead ) {

					CEntry *pEntry   = m_pHead;

					DWORD   Time     = pEntry->m_Time;

					DWORD   Secs     = Time / 5;

					BOOL    fHead[2] = { FALSE, FALSE };

					if( !m_pHelp->FindFiles(csv, fHead, 0, Time) ) {

						break;
					}

					for( UINT n = 0; n < 2; n++ ) {

						if( csv[n].IsValid() ) {

							if( fHead[n] ) {

								switch( CCommsSystem::m_pThis->m_pLog->m_CSVEncode ) {

									case 1:
										csv[n].AddBOM();
										break;
								}

								CString Line;

								Line += "Date";

								Line += m_cSep;

								Line += "Time";

								Line += m_cSep;

								Line += "Type";

								Line += m_cSep;

								Line += "Tag";

								Line += m_cSep;

								Line += "From";

								Line += m_cSep;

								Line += "To";

								Line += m_cSep;

								Line += "User";

								if( m_SignLogs ) {

									Line += m_cSep;

									Line += "Signature";
								}

								Line += "\r\n";

								csv[n].Write(PCBYTE(PCTXT(Line)), Line.GetLength());
							}

							csv[n].Printf("%4.4u/%2.2u/%2.2u%c%2.2u:%2.2u:%2.2u%c",
								      GetYear(Secs),
								      GetMonth(Secs),
								      GetDate(Secs),
								      m_cSep,
								      GetHour(Secs),
								      GetMin(Secs),
								      GetSec(Secs),
								      m_cSep
							);

							CString const &Line = pEntry->m_Line;

							csv[n].Write(PCBYTE(PCTXT(Line)), Line.GetLength());

							csv[n].EndOfLine();
						}
					}

					CAutoLock Lock(m_pLock);

					AfxListRemove(m_pHead, m_pTail, pEntry, m_pNext, m_pPrev);

					Lock.Free();

					m_pHelp->SaveStep(Time);

					delete pEntry;

					uCount += 1;
				}

				m_pHelp->SaveDone();

				csv[0].Close();
				csv[1].Close();

				m_pWait->Signal(uCount);
			}
		}
	}
}

// UI Task Entries

void CSecurityManager::RegisterUI(ISecureUI *pUI)
{
	LogStartup();

	m_pUI = pUI;

	m_hUI = GetCurrentTask();
}

void CSecurityManager::ProcessEvent(INPUT Event)
{
	m_Event   = Event;

	m_fFailed = FALSE;

	CheckTimeout();
}

void CSecurityManager::FocusChanged(void)
{
	if( m_Every ) {

		m_fAsked = FALSE;
	}
}

void CSecurityManager::KeypadHidden(void)
{
	if( m_Every ) {

		m_fAsked = FALSE;
	}
}

void CSecurityManager::PageSelected(CSecDesc *pDesc)
{
	if( pDesc && (pDesc->m_Access & allowMaint) ) {

		if( m_pUser && (m_pUser->m_Rights & allowMaint) ) {

			m_fMaint = TRUE;

			return;
		}
	}

	m_fMaint     = FALSE;

	m_uCheckCode = 0;
}

void CSecurityManager::ShutdownUI(void)
{
	if( m_fBusy ) {

		m_pUI->KeypadPull();

		m_fBusy = FALSE;
	}
}

// Permission Checks

BOOL CSecurityManager::SimplePrompt(PCTXT pPrompt)
{
	if( m_pUI->CanShowKeypad() ) {

		m_Prompt = pPrompt;

		ModalLoop(stateAuthPrompt);

		PostUpEvent();

		return m_uState == stateAuthDone;
	}

	return FALSE;
}

BOOL CSecurityManager::AllowAccess(DWORD Access)
{
	if( Access == allowDefault ) {

		return TRUE;
	}

	if( (Access & allowMask) == allowAnyone ) {

		return TRUE;
	}

	if( (Access & allowMask) == allowUsers ) {

		return TRUE;
	}

	if( !IsUITask() ) {

		CUserItem *pUser = GetTaskUser();

		if( pUser ) {

			if( Access & pUser->m_Rights ) {

				return TRUE;
			}
		}
		else {
			if( Access & allowProgram ) {

				return TRUE;
			}
		}

		return FALSE;
	}

	if( m_pUser ) {

		if( Access & m_pUser->m_Rights ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CSecurityManager::AllowWrite(BOOL fMap, CSecDesc *pDesc)
{
	return AllowAccess(GetAccess(fMap, pDesc));
}

BOOL CSecurityManager::AllowWrite(CTag *pTag, UINT uPos, BOOL fMap, CSecDesc *pDesc, DWORD Data, UINT Type)
{
	DWORD Access  = GetAccess(fMap, pDesc);

	BYTE  Logging = GetLogging(fMap, pDesc);

	if( !IsUITask() ) {

		CUserItem *pUser = GetTaskUser();

		if( pUser ) {

			BOOL fOkay = FALSE;

			if( (Access & allowMask) == allowAnyone ) {

				fOkay = TRUE;
			}

			if( (Access & allowMask) == allowUsers ) {

				fOkay = TRUE;
			}

			if( Access & pUser->m_Rights ) {

				fOkay = TRUE;
			}

			if( fOkay ) {

				if( Logging == logAll || Logging == logUser ) {

					LogChange(pUser, pTag, uPos, Data, Type);
				}

				return TRUE;
			}

			return FALSE;
		}
		else {
			if( Access & allowProgram ) {

				if( Logging == logAll ) {

					LogChange(pUser, pTag, uPos, Data, Type);
				}

				return TRUE;
			}
		}

		return FALSE;
	}
	else {
		if( Access & ~(allowProgram | allowCheck) ) {

			// Fast-path to avoid building prompts and
			// change when we know we won't need them!

			if( (Access & allowMask) == allowAnyone ) {

				if( !(Access & allowCheck) ) {

					if( !Logging ) {

						return TRUE;
					}
				}
			}

			if( BuildChange(pTag, uPos, Data, Type) ) {

				BuildPrompt();

				if( TestAccess(Access, DWORD(pDesc)) ) {

					if( Logging == logAll || Logging == logUser ) {

						LogChange();
					}

					return TRUE;
				}

				return FALSE;
			}

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CSecurityManager::AllowPageChange(CSecDesc *pDesc)
{
	DWORD Access;

	if( !pDesc || pDesc->m_Access == allowDefault ) {

		Access = m_DefPage;
	}
	else
		Access = pDesc->m_Access;

	////////

	if( Access & ~(allowProgram | allowCheck) ) {

		m_Prompt  = "Must Authenticate to Access Page";

		m_fChange = FALSE;

		return TestAccess(Access, UINT(0));
	}

	return TRUE;
}

BOOL CSecurityManager::HasAllAccess(DWORD Access)
{
	if( m_fAsked ) {

		if( IsUITask() ) {

			CheckTimeout();
		}

		if( m_fMaint ) {

			if( (Access & allowMask) == allowUsers ) {

				return TRUE;
			}

			if( Access & allowMaint ) {

				return TRUE;
			}
		}

		return HasAllRights(Access);
	}

	return FALSE;
}

BOOL CSecurityManager::HasAccess(DWORD Access)
{
	if( m_fAsked ) {

		if( IsUITask() ) {

			CheckTimeout();
		}

		if( (Access & allowMask) == allowAnyone ) {

			return TRUE;
		}

		if( m_fMaint ) {

			if( (Access & allowMask) == allowUsers ) {

				return TRUE;
			}

			if( Access & allowMaint ) {

				return TRUE;
			}
		}

		return HasRights(Access);
	}

	return FALSE;
}

BOOL CSecurityManager::TestAccess(DWORD Access, PCTXT pPrompt)
{
	if( IsUITask() ) {

		m_Prompt  = pPrompt;

		m_fChange = FALSE;

		return TestAccess(Access, UINT(0));
	}

	return FALSE;
}

BOOL CSecurityManager::TestAccess(DWORD Access, UINT uCode)
{
	if( (Access & allowMask) == allowAnyone ) {

		if( uCode ) {

			if( Access & allowCheck ) {

				if( m_fFailed ) {

					return FALSE;
				}

				if( !CheckBefore(uCode) ) {

					m_fFailed    = TRUE;

					m_uCheckCode = 0;

					return FALSE;
				}

				m_uCheckCode = uCode;

				m_uCheckTime = GetTickCount();
			}
		}

		return TRUE;
	}

	if( m_fMaint ) {

		if( (Access & allowMask) == allowUsers ) {

			return TRUE;
		}

		if( Access & allowMaint ) {

			return TRUE;
		}
	}

	if( m_fFailed ) {

		return FALSE;
	}

	if( m_fAsked ) {

		if( uCode ) {

			if( Access & allowCheck ) {

				if( !CheckBefore(uCode) ) {

					m_fFailed    = TRUE;

					m_uCheckCode = 0;

					return FALSE;
				}
			}
		}
	}

	if( m_fAsked || GetCredentials() ) {

		if( m_fAsked || LookupUser() ) {

			if( HasRights(Access) ) {

				if( !m_fAsked ) {

					if( m_ReqPass.IsEmpty() ) {

						Beep(96, 100);
					}
				}

				if( !m_fAsked ) {

					PostUpEvent();

					m_Last   = m_Event;

					m_fAsked = TRUE;
				}

				if( uCode ) {

					m_uCheckCode = uCode;

					m_uCheckTime = GetTickCount();
				}

				m_uTime = GetTickCount();

				return TRUE;
			}
			else {
				if( !m_Every || !m_fAsked ) {

					Beep(84, 250);

					ModalLoop(stateBadRights);

					PostUpEvent();
				}

				m_fAsked     = TRUE;

				m_fFailed    = TRUE;

				m_uCheckCode = 0;

				return FALSE;
			}
		}
		else {
			m_ReqUser.Empty();

			Beep(72, 250);

			ModalLoop(stateBadCred);
		}
	}

	PostUpEvent();

	m_fFailed    = TRUE;

	m_uCheckCode = 0;

	return FALSE;
}

// User Validation

BOOL CSecurityManager::ValidateLogon(PCTXT pUser, PCTXT pPass, DWORD Access)
{
	return (ValidateLogon(pUser, pPass) & Access) ? TRUE : FALSE;
}

DWORD CSecurityManager::ValidateLogon(PCTXT pUser, PCTXT pPass)
{
	for( UINT n = 0; n < m_pUsers->m_uCount; n++ ) {

		CUserItem *pScan = m_pUsers->m_ppUser[n];

		if( pScan ) {

			if( !stricmp(pUser, pScan->GetUserName()) ) {

				if( !strcmp(pPass, pScan->m_Cred.m_sPass) ) {

					AddTaskUser(pScan);

					return pScan->m_Rights | allowUsers;
				}

				break;
			}
		}
	}

	return 0;
}

CString CSecurityManager::GetPassword(PCTXT pUser, DWORD Access, BOOL fAddUser)
{
	for( UINT n = 0; n < m_pUsers->m_uCount; n++ ) {

		CUserItem *pScan = m_pUsers->m_ppUser[n];

		if( pScan ) {

			if( !stricmp(pUser, pScan->GetUserName()) ) {

				if( (pScan->m_Rights | allowUsers) & Access ) {

					if( fAddUser ) {

						AddTaskUser(pScan);
					}

					return pScan->m_Cred.m_sPass;
				}

				break;
			}
		}
	}

	return "";
}

BOOL CSecurityManager::AllowRemote(void)
{
	if( (m_pRemote = GetTaskUser()) ) {

		if( m_fAsked && m_pUser ) {

			DWORD Local  = (m_pUser->m_Rights   | allowWebServer);

			DWORD Remote = (m_pRemote->m_Rights | allowWebServer);

			return (Remote & Local) == Local;
		}

		return TRUE;
	}

	return TRUE;
}

// User Operations

void CSecurityManager::LogOn(void)
{
	if( IsUITask() ) {

		m_fFailed    = FALSE;

		m_fAsked     = FALSE;

		m_fMaint     = FALSE;

		m_uCheckCode = 0;

		m_Prompt     = "Please Log-On to Security System";

		m_fChange    = FALSE;

		TestAccess(allowUsers, UINT(0));
	}
}

void CSecurityManager::LogOff(void)
{
	if( m_fAsked && m_pUser ) {

		LogLogoff();
	}

	m_Event      = 0;

	m_fFailed    = FALSE;

	m_fAsked     = FALSE;

	m_fMaint     = FALSE;

	m_uCheckCode = 0;
}

// Manager APIs

void CSecurityManager::AddCard(UINT uIndex)
{
	if( ConvertIndex(uIndex) ) {

		if( TestAccess(2, "Must Authenticate to Add Card") ) {

			m_uEnroll = 1;

			m_Prompt  = "Scan New access card...";

			ModalLoop(stateAddCard);

			PostUpEvent();
		}
	}
}

void CSecurityManager::AddFinger(UINT uIndex)
{
	if( ConvertIndex(uIndex) ) {

		if( TestAccess(2, "Must Authenticate to Add Finger") ) {

			m_uEnroll = 2;

			m_Prompt  = "Please wait...";

			ModalLoop(stateAddFinger);

			PostUpEvent();
		}
	}
}

void CSecurityManager::SetPassword(UINT uIndex)
{
	if( ConvertIndex(uIndex) ) {

		if( TestAccess(2, "Must Authenticate to Set Password") ) {

			m_ReqPass.Empty();

			m_RepPass.Empty();

			ModalLoop(stateSetPass);

			PostUpEvent();
		}
	}
}

// User Database

BOOL CSecurityManager::LookupUser(void)
{
	for( UINT n = 0; n < m_pUsers->m_uCount; n++ ) {

		CUserItem *pUser = m_pUsers->m_ppUser[n];

		if( pUser ) {

			if( !stricmp(m_ReqUser, pUser->GetUserName()) ) {

				if( m_ReqPass.IsEmpty() ) {

					m_pUser = pUser;

					LogLogon();

					return TRUE;
				}

				if( !strcmp(m_ReqPass, pUser->m_Cred.m_sPass) ) {

					m_pUser = pUser;

					LogLogon();

					return TRUE;
				}
			}
		}
	}

	m_pUser = NULL;

	return FALSE;
}

BOOL CSecurityManager::HasAllRights(DWORD Access)
{
	if( m_pUser ) {

		if( (Access & m_pUser->m_Rights) == Access ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CSecurityManager::HasRights(DWORD Access)
{
	if( m_pUser ) {

		if( (Access & allowMask) == allowUsers ) {

			return TRUE;
		}

		if( Access & m_pUser->m_Rights ) {

			return TRUE;
		}

		if( m_pUser == m_pEnroll ) {

			if( (Access & 2) && (m_pUser->m_Rights & 1) ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

// UI Engine

BOOL CSecurityManager::CheckBefore(UINT uCode)
{
	if( m_uCheckCode == uCode ) {

		if( m_CheckTime ) {

			UINT uTime = GetTickCount();

			UINT uTest = ToTicks(1000) * m_CheckTime;

			if( uTime - m_uCheckTime <= uTest ) {

				return TRUE;
			}
		}
	}

	if( m_pUI->CanShowKeypad() ) {

		ModalLoop(stateAuthCheck);

		PostUpEvent();

		return m_uState == stateAuthDone;
	}

	return TRUE;
}

BOOL CSecurityManager::GetCredentials(void)
{
	if( m_pUI->CanShowKeypad() ) {

		if( m_UserClear ) {

			m_ReqUser.Empty();
		}

		m_ReqPass.Empty();

		ModalLoop(stateAuthInit);

		return m_uState == stateAuthDone;
	}

	return FALSE;
}

void CSecurityManager::ModalLoop(UINT uState)
{
	GuardTask(TRUE);

	InputEmpty();

	////////

	m_pUI->SecStartDraw();

	m_pUI->KeypadPush();

	SetState(uState);

	ShowString();

	m_pUI->SecUpdate();

	m_pUI->SecEndDraw();

	////////

	m_fBusy = TRUE;

	m_fDone = FALSE;

	GuardTask(FALSE);

	while( !m_fDone ) {

		SetTimer(m_pUI->GetKeypadTimeout());

		for( ;;) {

			m_pUI->SuspendWidget();

			BOOL  fMatch = FALSE;

			INPUT dwRead = InputRead(50);

			m_pUI->RestoreWidget();

			if( dwRead ) {

				CInput &i = (CInput &) dwRead;

				if( m_pUI->KeypadEvent(i) ) {

					fMatch = TRUE;
				}

				if( i.x.i.m_Type == typeKey ) {

					UINT uTrans = i.x.i.m_State;

					BOOL fLocal = i.x.i.m_Local;

					UINT uCode  = i.x.d.k.m_Code;

					if( OnKey(uTrans, fLocal, uCode) ) {

						fMatch = TRUE;
					}
				}
			}

			if( fMatch ) {

				m_pUI->SecStartDraw();

				ShowString();

				m_pUI->SecUpdate();

				m_pUI->SecEndDraw();

				break;
			}
			else {
				if( !GetTimer() ) {

					SetAbort();

					break;
				}

				m_pUI->SecStartDraw();

				m_pUI->SecUpdate();

				m_pUI->SecEndDraw();
			}
		}

		m_pUI->SecRunEvents();
	}

	GuardTask(TRUE);

	m_pUI->KeypadPull();

	m_fBusy = FALSE;

	GuardTask(FALSE);
}

void CSecurityManager::SetAbort(void)
{
	m_pAccept->Clear();

	m_uEnroll = 0;

	m_uState  = NOTHING;

	m_fDone   = TRUE;
}

void CSecurityManager::SetState(UINT uState)
{
	m_uState = uState;

	NewState();
}

void CSecurityManager::NewState(void)
{
	switch( m_uState ) {

		case stateAuthInit:

			if( m_fChange ) {

				ShowKeypad(keypadChange, "CONFIRM CHANGE");
			}
			else
				ShowKeypad(keypadSecurity, "SECURITY MANAGER");

			break;

		case stateAuthCheck:

			ShowKeypad(keypadChange, "CHECK BEFORE OPERATE");

			break;

		case stateAuthPrompt:

			ShowKeypad(keypadSecurity, "ACTION IS PROTECTED");

			break;

		case stateGetUser:

			ShowKeypad(keypadLogon, "ENTER USERNAME");

			break;

		case stateGetPass:

			ShowKeypad(keypadLogon, "ENTER PASSWORD");

			break;

		case stateAuthDone:

			m_fDone = TRUE;

			break;

		case stateBadCred:
		case stateBadRights:
		case stateAddCard:
		case stateAddFinger:
		case stateSetError:

			ShowKeypad(keypadMessage, "SECURITY MANAGER");

			break;

		case stateSetPass:

			ShowKeypad(keypadLogon, "ENTER NEW PASSWORD");

			break;

		case stateRepPass:

			ShowKeypad(keypadLogon, "REPEAT NEW PASSWORD");

			break;

		case stateSetDone:

			if( !strcmp(m_RepPass, m_ReqPass) ) {

				strcpy(m_pEnroll->m_Cred.m_sPass, m_ReqPass);

				m_pEnroll->SaveCred(TRUE);

				m_fDone = TRUE;
			}
			else {
				Beep(60, 250);

				SetState(stateSetError);
			}
			break;
	}

	switch( m_uState ) {

		case stateAuthInit:
		case stateAddCard:
		case stateAddFinger:

			m_pAccept->Set();

			break;

		default:
			m_pAccept->Clear();

			break;
	}
}

BOOL CSecurityManager::OnKey(UINT uTrans, BOOL fLocal, UINT uCode)
{
	if( m_uState == stateAuthInit ) {

		if( uTrans == stateDown ) {

			if( uCode == secFingerFail ) {

				m_Prompt = "FINGER DID NOT READ -- TRY AGAIN";

				m_fChange = FALSE;

				Beep(60, 250);

				return TRUE;
			}

			if( uCode == secFingerOkay ) {

				SetState(stateAuthDone);

				return TRUE;
			}

			if( uCode == secCardFail ) {

				m_Prompt = "UNKNOWN ACCESS CARD -- TRY AGAIN";

				m_fChange = FALSE;

				Beep(60, 250);

				return TRUE;
			}

			if( uCode == secCardOkay ) {

				SetState(stateAuthDone);

				return TRUE;
			}

			if( uCode == VK_EXIT ) {

				SetAbort();

				return TRUE;
			}

			if( uCode == VK_ENTER ) {

				if( !fLocal && m_pRemote ) {

					m_ReqUser = m_pRemote->GetUserName();

					m_ReqPass = m_pRemote->m_Cred.m_sPass;

					SetState(stateAuthDone);

					return TRUE;
				}

				SetState(stateGetUser);

				return TRUE;
			}
		}
	}

	if( m_uState == stateAuthCheck || m_uState == stateAuthPrompt ) {

		if( uTrans == stateDown ) {

			if( uCode == VK_EXIT ) {

				SetAbort();

				return TRUE;
			}

			if( uCode == VK_ENTER ) {

				SetState(stateAuthDone);

				return TRUE;
			}
		}
	}

	if( m_uState == stateGetUser ) {

		if( EnterString(uTrans, uCode, m_ReqUser, stateGetPass) ) {

			return TRUE;
		}

		return FALSE;
	}

	if( m_uState == stateGetPass ) {

		if( EnterString(uTrans, uCode, m_ReqPass, stateAuthDone) ) {

			return TRUE;
		}

		return FALSE;
	}

	if( m_uState == stateBadCred ) {

		if( uTrans == stateDown ) {

			if( uCode == VK_EXIT ) {

				SetAbort();

				return TRUE;
			}
		}
	}

	if( m_uState == stateBadRights ) {

		if( uTrans == stateDown ) {

			if( uCode == VK_EXIT ) {

				SetAbort();

				return TRUE;
			}
		}
	}

	if( m_uState == stateAddCard ) {

		if( uTrans == stateDown ) {

			if( uCode == secCardFail ) {

				m_Prompt = "CARD IN USE -- TRY AGAIN";

				Beep(60, 250);

				return TRUE;
			}

			if( uCode == secCardOkay ) {

				SetAbort();

				return TRUE;
			}

			if( uCode == VK_EXIT ) {

				SetAbort();

				return TRUE;
			}
		}
	}

	if( m_uState == stateAddFinger ) {

		if( uTrans == stateDown ) {

			if( uCode == secFingerFail ) {

				m_Prompt = "ENROLLMENT FAILED";

				Beep(60, 250);

				return TRUE;
			}

			if( uCode == secFingerDown ) {

				m_Prompt = "Place finger on sensor...";

				Beep(96, 100);

				return TRUE;
			}

			if( uCode == secFingerLift ) {

				m_Prompt = "Remove finger...";

				Beep(96, 100);

				return TRUE;
			}

			if( uCode == secFingerOkay ) {

				SetAbort();

				return TRUE;
			}

			if( uCode == VK_EXIT ) {

				SetAbort();

				return TRUE;
			}
		}
	}

	if( m_uState == stateSetPass ) {

		if( EnterString(uTrans, uCode, m_ReqPass, stateRepPass) ) {

			return TRUE;
		}

		return FALSE;
	}

	if( m_uState == stateRepPass ) {

		if( EnterString(uTrans, uCode, m_RepPass, stateSetDone) ) {

			return TRUE;
		}

		return FALSE;
	}

	if( m_uState == stateSetError ) {

		if( uTrans == stateDown ) {

			if( uCode == VK_EXIT ) {

				SetAbort();

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CSecurityManager::EnterString(UINT uTrans, UINT uCode, CString &Text, UINT uNext)
{
	if( uTrans == stateDown ) {

		if( uCode == VK_EXIT ) {

			m_fDone = TRUE;

			return TRUE;
		}

		if( uCode == VK_ENTER ) {

			if( Text.GetLength() ) {

				SetState(uNext);

				return TRUE;
			}

			return FALSE;
		}

		if( uCode >= 32 && uCode < 126 ) {

			if( Text.GetLength() < 15 ) {

				Text += char(uCode);

				return TRUE;
			}
		}

		if( uCode == 0x7F ) {

			if( Text.GetLength() ) {

				Text = Text.Left(Text.GetLength()-1);

				return TRUE;
			}
		}
	}

	return FALSE;
}

void CSecurityManager::ShowString(void)
{
	if( m_uState == stateAuthInit ) {

		m_pUI->KeypadSetText(m_Prompt, cursorNone);
	}

	if( m_uState == stateAuthCheck ) {

		m_pUI->KeypadSetText(m_Prompt, cursorNone);
	}

	if( m_uState == stateAuthPrompt ) {

		m_pUI->KeypadSetText(m_Prompt, cursorNone);
	}

	if( m_uState == stateGetUser ) {

		m_pUI->KeypadSetText(m_ReqUser, cursorLast);
	}

	if( m_uState == stateGetPass ) {

		UINT    uLen = m_ReqPass.GetLength();

		CString Text = CString('*', uLen);

		m_pUI->KeypadSetText(Text, cursorLast);
	}

	if( m_uState == stateBadCred ) {

		m_pUI->KeypadSetText("INVALID USER CREDENTIALS", cursorNone);
	}

	if( m_uState == stateBadRights ) {

		m_pUI->KeypadSetText("INSUFFICIENT USER RIGHTS", cursorNone);
	}

	if( m_uState == stateAddCard ) {

		m_pUI->KeypadSetText(m_Prompt, cursorNone);
	}

	if( m_uState == stateAddFinger ) {

		m_pUI->KeypadSetText(m_Prompt, cursorNone);
	}

	if( m_uState == stateSetPass ) {

		UINT    uLen = m_ReqPass.GetLength();

		CString Text = CString('*', uLen);

		m_pUI->KeypadSetText(Text, cursorLast);
	}

	if( m_uState == stateRepPass ) {

		UINT    uLen = m_RepPass.GetLength();

		CString Text = CString('*', uLen);

		m_pUI->KeypadSetText(Text, cursorLast);
	}

	if( m_uState == stateSetError ) {

		m_pUI->KeypadSetText("PASSWORDS DO NOT MATCH", cursorNone);
	}
}

// Logging

BOOL CSecurityManager::BuildChange(CTag *pTag, UINT uPos, DWORD Data, UINT Type)
{
	m_TagName = pTag->GetLabel(uPos);

	m_OldData = pTag->GetAsText(uPos, fmtStd);

	m_NewData = pTag->GetAsText(uPos, Data, Type, fmtStd);

	if( m_OldData.CompareC(m_NewData) ) {

		m_OldData.TrimBoth();

		m_NewData.TrimBoth();

		if( m_OldData.IsEmpty() ) m_OldData = "[empty]";

		if( m_NewData.IsEmpty() ) m_NewData = "[empty]";

		return TRUE;
	}

	return FALSE;
}

void CSecurityManager::BuildPrompt(void)
{
	m_Prompt  = "Change ";

	m_Prompt += UniConvert(m_TagName);

	m_Prompt += " from ";

	m_Prompt += UniConvert(m_OldData);

	m_Prompt += " to ";

	m_Prompt += UniConvert(m_NewData);

	m_Prompt += '?';

	m_fChange = TRUE;
}

void CSecurityManager::LogStartup(void)
{
	CString Line;

	UpdateSep();

	Line.Printf("\"Startup\"%c%c%c%c\r\n",
		    m_cSep,
		    m_cSep,
		    m_cSep,
		    m_cSep
	);

	LogEvent(Line, 0xFF00);
}

void CSecurityManager::LogLogon(void)
{
	if( !m_Every ) {

		CString Line;

		UpdateSep();

		Line.Printf("\"Logon\"%c%c%c%c%s\r\n",
			    m_cSep,
			    m_cSep,
			    m_cSep,
			    m_cSep,
			    PCTXT(Encode(m_pUser->GetRealName()))
		);

		LogEvent(Line, 0xFF00);
	}
}

void CSecurityManager::LogLogoff(void)
{
	if( !m_Every ) {

		CString Line;

		UpdateSep();

		Line.Printf("\"Logoff\"%c%c%c%c%s\r\n",
			    m_cSep,
			    m_cSep,
			    m_cSep,
			    m_cSep,
			    PCTXT(Encode(m_pUser->GetRealName()))
		);

		LogEvent(Line, 0xFF00);
	}
}

void CSecurityManager::LogChange(CUserItem *pUser, CTag *pTag, UINT uPos, DWORD Data, UINT Type)
{
	CUnicode User    = pUser ? pUser->GetRealName() : CUnicode(L"System");

	CUnicode TagName = pTag->GetLabel(uPos);

	CUnicode OldData = pTag->GetAsText(uPos, fmtStd);

	CUnicode NewData = pTag->GetAsText(uPos, Data, Type, fmtStd);

	LogChange(TagName, OldData, NewData, User, pTag->m_uTag);
}

void CSecurityManager::LogChange(void)
{
	CUnicode User = (m_fAsked && m_pUser) ? m_pUser->GetRealName() : CUnicode(L"Anonymous");

	LogChange(m_TagName, m_OldData, m_NewData, User, 0xFE00);
}

void CSecurityManager::LogChange(CUnicode TagName, CUnicode OldData, CUnicode NewData, CUnicode User, UINT uTag)
{
	CString Line;

	UpdateSep();

	OldData.TrimBoth();

	NewData.TrimBoth();

	if( OldData.IsEmpty() ) {

		OldData = "[empty]";
	}

	if( NewData.IsEmpty() ) {

		NewData = "[empty]";
	}

	if( OldData.Find(m_cSep) < NOTHING ) {

		OldData = L'"' + OldData + L'"';
	}

	if( NewData.Find(m_cSep) < NOTHING ) {

		NewData = L'"' + NewData + L'"';
	}

	Line.Printf("\"Change\"%c\"%s\"%c%s%c%s%c\"%s\"\r\n",
		    m_cSep,
		    PCTXT(Encode(TagName)),
		    m_cSep,
		    PCTXT(Encode(OldData)),
		    m_cSep,
		    PCTXT(Encode(NewData)),
		    m_cSep,
		    PCTXT(Encode(User))
	);

	LogEvent(Line, uTag);
}

void CSecurityManager::LogEvent(CString const &Line, UINT uTag)
{
	if( WhoHasFeature(rfLogToDisk) ) {

		DWORD Time = /*g_pServiceTimeSync ? g_pServiceTimeSync->GetLogTime() :*/ GetNowTimes5();

		if( m_pHelp ) {

			m_pWait->Wait(250);

			CEntry *pEntry = New CEntry;

			pEntry->m_Time = Time;

			pEntry->m_Line = PCTXT(Line);

			CAutoLock Lock(m_pLock);

			AfxListAppend(m_pHead, m_pTail, pEntry, m_pNext, m_pPrev);
		}

		CEventInfo Info;

		Info.m_Time    = Time;

		Info.m_Source  = 7;

		Info.m_Type    = 2;

		Info.m_HasText = 1;

		Info.m_Code    = uTag;

		Info.m_Text    = Line;

		CCommsSystem::m_pThis->m_pLog->LogEvent(Info);
	}
}

// Task Users

void CSecurityManager::AddTaskUser(CUserItem *pUser)
{
	UINT e = NOTHING;

	UINT i = GetCurrentTaskIndex();

	UINT n;

	for( n = 0; n < elements(m_uTaskList); n++ ) {

		if( m_uTaskList[n] == 0 ) {

			e = n;

			continue;
		}

		if( m_uTaskList[n] == i ) {

			break;
		}
	}

	if( n == elements(m_uTaskList) ) {

		n = e;
	}

	if( n < NOTHING ) {

		m_uTaskList[n] = i;

		m_pTaskUser[n] = pUser;
	}
}

CUserItem * CSecurityManager::GetTaskUser(void)
{
	UINT i = GetCurrentTaskIndex();

	for( UINT n = 0; n < elements(m_uTaskList); n++ ) {

		if( m_uTaskList[n] == i ) {

			return m_pTaskUser[n];
		}
	}

	return NULL;
}

// Implementation		

DWORD CSecurityManager::GetAccess(BOOL fMap, CSecDesc *pDesc)
{
	if( !pDesc || pDesc->m_Access == allowDefault ) {

		return fMap ? m_DefMapped : m_DefLocal;
	}

	return pDesc->m_Access;
}

DWORD CSecurityManager::GetLogging(BOOL fMap, CSecDesc *pDesc)
{
	if( !pDesc || pDesc->m_Logging == logDefault ) {

		return fMap ? m_LogMapped : m_LogLocal;
	}

	return pDesc->m_Logging;
}

BOOL CSecurityManager::CheckTimeout(void)
{
	if( m_fAsked && m_pUser && !m_fMaint ) {

		UINT uTime = GetTickCount();

		if( m_Every ) {

			if( !IsSameEvent() || uTime - m_uTime > ToTicks(4000) ) {

				m_fAsked  = FALSE;

				return FALSE;
			}
		}
		else {
			if( uTime - m_uTime > m_UserTime * ToTicks(1000) ) {

				m_fAsked = FALSE;

				return FALSE;
			}
		}
	}

	return TRUE;
}

void CSecurityManager::ShowKeypad(UINT uType, PCTXT pText)
{
	m_pUI->KeypadShow(uType | kpsShowHead, pText);
}

void CSecurityManager::PostSecurityEvent(UINT uCode)
{
	CInput Input;

	Input.x.i.m_Type   = typeKey;

	Input.x.i.m_State  = stateDown;

	Input.x.i.m_Local  = TRUE;

	Input.x.d.k.m_Code = uCode;

	InputStore(Input.m_Ref);
}

void CSecurityManager::PostUpEvent(void)
{
	if( m_Event ) {

		CInput &i = (CInput &) m_Event;

		if( i.x.i.m_State == stateDown ) {

			INPUT   c = m_Event;

			CInput &i = (CInput &) c;

			i.x.i.m_State = stateUp;

			InputStore(c);
		}
	}
}

BOOL CSecurityManager::IsSameEvent(void)
{
	CInput &i1 = (CInput &) m_Event;

	CInput &i2 = (CInput &) m_Last;

	if( i1.x.i.m_Type == i2.x.i.m_Type ) {

		if( IsSameState(i1.x.i.m_State, i2.x.i.m_State) ) {

			if( i1.x.i.m_Local == i2.x.i.m_Local ) {

				if( i1.x.i.m_Type == typeTouch ) {

					return TRUE;
				}

				if( i1.x.d.k.m_Code == i2.x.d.k.m_Code ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CSecurityManager::IsSameState(UINT s1, UINT s2)
{
	if( s1 == s2 ) {

		return TRUE;
	}

	if( s1 == stateDown && s2 == stateRepeat ) {

		return TRUE;
	}

	if( s2 == stateDown && s1 == stateRepeat ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CSecurityManager::ConvertIndex(UINT uIndex)
{
	for( UINT n = 0; n < m_pUsers->m_uCount; n++ ) {

		if( m_pUsers->m_ppUser[n] ) {

			if( !uIndex-- ) {

				m_pEnroll = m_pUsers->m_ppUser[n];

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CSecurityManager::IsUITask(void)
{
	if( GetCurrentTask() == m_hUI ) {

		if( !m_pUI->IsRunningCode() ) {

			return TRUE;
		}
	}

	return FALSE;
}

void CSecurityManager::UpdateSep(void)
{
	m_cSep = char(CCommsSystem::m_pThis->m_pLang->GetListSepChar());
}

// CSV Encoding

CString CSecurityManager::Encode(CUnicode Text)
{
	switch( CCommsSystem::m_pThis->m_pLog->m_CSVEncode ) {

		case 1:
		case 2:

			return UtfConvert(Text);

		default:
			Text.Remove(uniLRM);

			Text.Remove(uniPDF);

			return UniConvert(Text);
	}
}

// End of File
