
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hpp"

#ifndef INCLUDE_SQL_CLIENT

#define INCLUDE_SQL_CLIENT

// Forward declarations

interface ISqlConnection;
class     CSqlStatement;
class     CSqlData;
class     CSqlDataRead;
class     CSqlDataWrite;
class     CResultSet;

//////////////////////////////////////////////////////////////////////////
//
// SQL Client Connection Constants
//

enum
{
	dbMsSqlServer,
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Data Types
//

enum {
	sqlTypeBinary	= 0,
	sqlTypeChar	= 1,
	sqlTypeInt	= 2,
	sqlTypeFloat	= 3,
	sqlTypeDatetime	= 4,
	sqlTypeVarChar	= 5,
	sqlTypeNVarChar	= 6
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Connection Interface
//

interface ISqlConnection
{
	virtual BOOL Open(IPADDR IP, WORD wPort, UINT uTls, PCTXT pUser, PCTXT pPass, PCTXT pDatabase) = 0;
	virtual void Close(void)                                                                       = 0;
	virtual void SetInstance(PCTXT pInstance)                                                      = 0;
	virtual CSqlStatement * CreateStatement(void)                                                  = 0;
	virtual BOOL ExecuteQuery(PCTXT pQuery)                                                        = 0;
	virtual BOOL ExecuteReadQuery(PCTXT pQuery, CResultSet &Results)                               = 0;
	virtual BOOL IsOpen(void)                                                                      = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Statement
//

class CSqlStatement
{
	public:
		// Constructor
		CSqlStatement(ISqlConnection *pConnection);

		// Query Execution
		BOOL Exec(PCTXT pQuery);
		BOOL ExecRead(PCTXT pQuery, CResultSet &Results);
		BOOL ExecWrite(PCTXT pQuery);

	protected:

		// Data Members
		ISqlConnection * m_pConnection;
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Data Object
//

class CSqlData
{
	public:
		// Constructors
		CSqlData(void);
		CSqlData(CSqlData const &That);

		// Destructor
		~CSqlData(void);

		// Data Access
		PCBYTE GetData(void) const;
		UINT   GetSize(void) const;
		UINT   GetType(void) const;
		void   SetData(PCBYTE pSource, UINT uSize);

		// Operators
		CSqlData &operator = (CSqlData const &That);

	protected:

		// Data Members
		PBYTE  m_pData;
		UINT   m_uSize;
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Row
//

class CSqlRow
{
	public:
		// Constructors
		CSqlRow(void);

		// Destructor
		~CSqlRow(void);

		// Data Access
		CSqlData const * GetData(UINT uCol) const;
		UINT GetCount(void) const;
		void AddData(CSqlData &Data);

	protected:

		typedef CArray<CSqlData> CDataArray;

		// Data Members
		CDataArray m_RowData;
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Column
//

class CSqlColumn
{
	public:
		// Constructors
		CSqlColumn(void);
		CSqlColumn(PCTXT pName, UINT uType, UINT uSize);

		// Destructor
		~CSqlColumn(void);

		// Attributes
		PCTXT GetName(void) const;
		UINT  GetType(void) const;

	protected:

		// Data Members
		CString m_Name;
		UINT    m_uSqlType;
		UINT    m_uSize;
		
	};

//////////////////////////////////////////////////////////////////////////
//
// SQL Result Set
//

class CResultSet
{
	public:
		// Constructors
		CResultSet(void);

		// Destructor
		~CResultSet(void);

		// Attributes
		UINT GetRowCount(void)    const;
		UINT GetColumnCount(void) const;

		// Data Access
		UINT   GetIntegerField(UINT uRow, UINT uCol)  const;
		INT64  GetInt64Field(UINT uRow, UINT uCol)    const;
		double GetFloatField(UINT uRow, UINT uCol)    const;
		PCUTF  GetTextField(UINT uRow, UINT uCol)     const;
		PCUTF  GetWideTextField(UINT uRow, UINT uCol) const;

		CSqlRow    const * GetRow(UINT uPos);
		CSqlColumn const * GetColumn(UINT uPos) const;

		// Result Set Building
		void AddColumn(CSqlColumn const &Column);
		void AddRow(CSqlRow const &Row);

	protected:

		// Typedefs
		typedef CArray<CSqlRow>    CRowArray;
		typedef CArray<CSqlColumn> CColArray;

		// Data Members
		CRowArray  m_Rows;
		CColArray  m_Cols;
	};

#endif

// End of File
