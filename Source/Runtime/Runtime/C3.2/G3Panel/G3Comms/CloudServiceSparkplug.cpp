
#include "Intern.hpp"

#include "Service.hpp"

#include "CloudServiceSparkplug.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MqttClientSparkplug.hpp"

#include "MqttClientOptionsSparkplug.hpp"

#include "CloudDeviceDataSet.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Sparkplug Cloud Service
//

// Instantiator

IService * Create_CloudServiceSparkplug(void)
{
	return New CCloudServiceSparkplug;
	}

// Constructor

CCloudServiceSparkplug::CCloudServiceSparkplug(void)
{
	m_Name = "SPB";
	}

// Initialization

void CCloudServiceSparkplug::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudServiceSparkplug", pData);

	CCloudServiceCrimson::Load(pData);

	for( UINT n = 0; n < m_uSet; n++ ) {

		m_pSet[n]->m_Array = 1;
		}

	CMqttClientOptionsSparkplug *pOpts = New CMqttClientOptionsSparkplug;

	pOpts->Load(pData);

	m_pOpts   = pOpts;

	CheckHistory(200);

	m_pClient = New CMqttClientSparkplug(this, *pOpts);

	m_pOpts->m_DiskPath.Printf("%c:\\MQTT\\SPB", 'C' + pOpts->m_Drive);

	FindConfigGuid(pOpts->GetExtra());
	}

// Service ID

UINT CCloudServiceSparkplug::GetID(void)
{
	return 9;
	}

// End of File
