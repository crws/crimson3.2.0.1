
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display Helper Implementation
//

class CDisplayHelper : public IDisplayHelper 
{
	public:
		// Constructor
		CDisplayHelper(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDisplayHelper
		UINT METHOD GetType(void);
		INT  METHOD GetCx(void);
		INT  METHOD GetCy(void);

	protected:
		// Data Members
		UINT m_uRefs;
	};

//////////////////////////////////////////////////////////////////////////
//
// Display Helper Implementation
//

// Instantiator

global IDisplayHelper * Create_DisplayHelper(void)
{
	return New CDisplayHelper;
	}

// Constructor

CDisplayHelper::CDisplayHelper(void)
{
	StdSetRef();
	}

// IUnknown

HRESULT CDisplayHelper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDisplayHelper);

	StdQueryInterface(IDisplayHelper);

	return E_NOINTERFACE;
	}

ULONG CDisplayHelper::AddRef(void)
{
	StdAddRef();
	}

ULONG CDisplayHelper::Release(void)
{
	StdRelease();
	}

// IDisplayHelper

UINT METHOD CDisplayHelper::GetType(void)
{
	int cx = DispGetCx();

	int cy = DispGetCy();

	if( cx == 128 && cy == 64 ) {
		
		return 0;
		}
	
	return 2;	
	}

INT METHOD CDisplayHelper::GetCx(void)
{
	return DispGetCx();
	}

INT METHOD CDisplayHelper::GetCy(void)
{
	return DispGetCy();
	}

// End of File
