
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Flag Display Color
//

// Constructor

CDispColorFlag::CDispColorFlag(void)
{
	m_On  = MAKELONG(GetRGB(0,31,0),GetRGB(0,0,0));

	m_Off = MAKELONG(GetRGB(31,0,0),GetRGB(0,0,0));
	}

// Destructor

CDispColorFlag::~CDispColorFlag(void)
{
	}

// Initialization

void CDispColorFlag::Load(PCBYTE &pData)
{
	ValidateLoad("CDispColorFlag", pData);

	m_On  = GetLong(pData);

	m_Off = GetLong(pData);
	}

// Color Access

DWORD CDispColorFlag::GetColorPair(DWORD Data, UINT Type)
{
	if( Type == typeReal ) {

		C3REAL Real = I2R(Data);

		return Real ? m_On : m_Off;
		}

	return Data ? m_On : m_Off;
	}

// End of File
