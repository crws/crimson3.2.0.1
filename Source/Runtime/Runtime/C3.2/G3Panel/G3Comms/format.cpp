
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display Format
//

// Class Creation

CDispFormat * CDispFormat::MakeObject(UINT uType)
{
	switch( uType ) {

		case 1: return New CDispFormatNumber;
		case 2: return New CDispFormatSci;
		case 3: return New CDispFormatTimeDate;
		case 4: return New CDispFormatIPAddr;
		case 5: return New CDispFormatFlag;
		case 6: return New CDispFormatMulti;
		case 7: return New CDispFormatLinked;
		case 8: return New CDispFormatString;
		}

	return NULL;
	}

// General Formatting

CUnicode CDispFormat::GeneralFormat(DWORD Data, UINT Type, UINT Flags)
{
	if( !(Flags & fmtUnits) ) {

		if( Type ) {

			CUnicode Text;

			if( Type == typeInteger ) {

				char s[32];

				SPrintf(s, "%d", C3INT(Data));

				Text = s;
				}

			if( Type == typeReal ) {

				C3REAL r = I2R(Data);

				if( IsNAN(r) ) {

					return L"NAN";
					}

				if( IsINF(r) ) {

					if( r > 0 ) {

						return L"+INF";
						}

					return L"-INF";
					}

				char s[32];

				PTXT w = s;

				if( r < 0 ) {

					// NOTE : gcvt is not reliable between platforms as
					// to how it treats negative numbers, so we do the
					// conversion ourselves to maintain the same ouptut.

					r    = -r;

					*w++ = '-';
					}

				gcvt(r, 5, w);

				char *p = strchr(w, 'e');

				if( p ) {

					p[0] = 'E';

					if( p[2] == '0' ) {

						if( p[3] == '0' ) {

							p[2] = p[4];
							p[3] = 0;
							}
						else {
							p[2] = p[3];
							p[3] = p[4];
							p[4] = 0;
							}
						}

					if( *--p == '.' ) {

						memmove(p, p+1, strlen(p));
						}
					}
				else {
					int n = strlen(s);

					if( n ) {
						
						if( s[n-1] == '.' ) {

							s[n-1] = 0;
							}
						}
					}

				Text = s;
				}

			if( Type == typeString ) {

				Text = Data ? PCUTF(Data) : L"";

				return Text;
				}

			if( Flags & fmtPad ) {

				if( !(Flags & fmtANSI) ) {

					MakeDigitsFixed(Text);
					}
				}

			return Text;
			}

		return L"---";
		}

	return L"";
	}

// General Parsing

BOOL CDispFormat::GeneralParse(DWORD &Data, CString Text, UINT Type)
{
	if( Type == typeInteger ) {

		Data = strtol(Text, NULL, 10);

		return TRUE;
		}

	if( Type == typeReal ) {

		Data = R2I(strtod(Text, NULL));

		return TRUE;
		}

	if( Type == typeString ) {

		Data = DWORD(wstrdup(UniConvert(Text)));

		return TRUE;
		}

	return FALSE;
	}

// General Editing

UINT CDispFormat::GeneralEdit(CEditCtx *pEdit, UINT uCode)
{
	switch( pEdit->m_Type ) {

		case typeInteger:

			return GeneralEditInteger(pEdit, uCode);

		case typeReal:

			return GeneralEditReal(pEdit, uCode);

		case typeString:

			return GeneralEditString(pEdit, uCode);
		}

	return editNone;
	}

// Constructor

CDispFormat::CDispFormat(void)
{
	}

// Destructor

CDispFormat::~CDispFormat(void)
{
	}

// Type Checking

BOOL CDispFormat::IsNumber(void)
{
	return FALSE;
	}

BOOL CDispFormat::IsMulti(void)
{
	return FALSE;
	}

// Scan Control

BOOL CDispFormat::IsAvail(void)
{
	return TRUE;
	}

BOOL CDispFormat::SetScan(UINT Code)
{
	return TRUE;
	}

// Formatting

CUnicode CDispFormat::Format(DWORD Data, UINT Type, UINT Flags)
{
	return GeneralFormat(Data, Type, Flags);
	}

// Parsing

BOOL CDispFormat::Parse(DWORD &Data, CString Text, UINT Type)
{
	return GeneralParse(Data, Text, Type);
	}

// Presentation

CString CDispFormat::GetStates(void)
{
	return "";
	}

// Editing

UINT CDispFormat::Edit(CEditCtx *pEdit, UINT uCode)
{
	return GeneralEdit(pEdit, uCode);
	}

// Limit Access

BOOL CDispFormat::NeedsLimits(void)
{
	return FALSE;
	}

DWORD CDispFormat::GetMin(UINT Type)
{
	if( Type == typeInteger ) {

		return 0;
		}

	return R2I(0);
	}

DWORD CDispFormat::GetMax(UINT Type)
{
	if( Type == typeInteger ) {

		return 999999;
		}

	return R2I(999999);
	}

// Stepping

DWORD CDispFormat::GetStep(UINT Type)
{
	if( Type == typeInteger ) {

		return 1;
		}

	if( Type == typeReal ) {

		return R2I(1);
		}

	return 0;
	}

BOOL CDispFormat::SpeedUp(UINT uCount)
{
	return FALSE;
	}

BOOL CDispFormat::SpeedUp(DWORD Data, UINT Type, DWORD &Step)
{
	return FALSE;
	}

// Editing Implementation

UINT CDispFormat::GeneralEditNumeric(CEditCtx *pEdit, UINT uCode)
{
	if( uCode == 0x00 ) {

		pEdit->m_uKeypad  = keypadNumeric;

		pEdit->m_Foot    += GeneralFormat(pEdit->m_Min, pEdit->m_Type, fmtBare);

		pEdit->m_Foot    += L" - ";

		pEdit->m_Foot    += GeneralFormat(pEdit->m_Max, pEdit->m_Type, fmtBare);

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		pEdit->m_fError   = TRUE;

		return editUpdate;
		}

	if( uCode == 0x0D ) {

		if( !pEdit->m_fDefault ) {

			double Read = 0;

			if( pEdit->m_Edit.GetLength() ) {

				CString Edit = UniConvert(pEdit->m_Edit);

				Read = strtod(Edit, NULL);
				}

			if( pEdit->m_Type == typeReal ) {

				C3REAL Data = C3REAL(Read);

				C3REAL Min  = I2R(pEdit->m_Min);

				C3REAL Max  = I2R(pEdit->m_Max);

				if( Data >= Min && Data <= Max ) {

					if( Validate(pEdit, R2I(Data)) ) {
						
						if( pEdit->m_pValue->SetValue(R2I(Data), typeReal, setNone) ) {

							return editCommit;
							}

						return editUpdate;
						}
					}
				}
			else {
				C3INT Sign = (Read < 0) ? -1 : +1;

				C3INT Data = C3INT(fabs(Read) + 0.5) * Sign;

				C3INT Min  = C3INT(pEdit->m_Min);

				C3INT Max  = C3INT(pEdit->m_Max);

				if( Data >= Min && Data <= Max ) {

					if( Validate(pEdit, DWORD(Data)) ) {
						
						if( pEdit->m_pValue->SetValue(DWORD(Data), typeInteger, setNone) ) {

							return editCommit;
							}

						return editUpdate;
						}
					}
				}

			pEdit->m_uCursor = cursorError;

			pEdit->m_fError  = TRUE;

			return editError;
			}

		return editAbort;
		}

	if( uCode == 0x1B ) {

		if( pEdit->m_fDefault ) {

			return editAbort;
			}

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		pEdit->m_fError   = FALSE;

		return editUpdate;
		}

	if( uCode == 0x7F ) {

		if( pEdit->m_fDefault ) {

			pEdit->m_Edit.Empty();

			pEdit->m_uCursor  = cursorLast;

			pEdit->m_fDefault = FALSE;

			return editUpdate;
			}

		return editNone;
		}

	if( uCode >= 0x20 ) {

		if( uCode == '.' ) {

			if( pEdit->m_Type == typeInteger ) {
				
				return editNone;
				}
			}

		if( pEdit->m_fDefault || pEdit->m_fError ) {

			pEdit->m_Edit.Empty();

			pEdit->m_uCursor  = cursorLast;

			pEdit->m_fDefault = FALSE;

			pEdit->m_fError   = FALSE;
			}

		if( uCode == '+' ) {

			if( pEdit->m_Edit[0] == '-' ) {

				pEdit->m_Edit.Delete(0, 1);
		
				return editUpdate;
				}
			else {
				if( pEdit->m_Edit.GetLength() < 16 ) {

					pEdit->m_Edit.Insert(0, '-');

					return editUpdate;
					}
				}
			}

		if( (uCode >= '0' && uCode <= '9') || uCode == '.' ) {

			UINT uPos = pEdit->m_Edit.Find('.');

			UINT uLen = pEdit->m_Edit.GetLength();

			if( uCode == '.' ) {

				if( uPos < NOTHING ) {

					return editNone;
					}

				if( pEdit->m_Edit.IsEmpty() ) {

					pEdit->m_Edit = L"0";
					}

				pEdit->m_Edit += WCHAR(uCode);

				return editUpdate;
				}

			if( uPos == NOTHING ) {

				if( pEdit->m_Edit[0] == '-' ) {

					uLen--;
					}

				pEdit->m_Edit += WCHAR(uCode);

				return editUpdate;
				}

			pEdit->m_Edit += WCHAR(uCode);

			return editUpdate;
			}
		}

	return editNone;
	}

UINT CDispFormat::GeneralEditInteger(CEditCtx *pEdit, UINT uCode)
{
	return GeneralEditNumeric(pEdit, uCode);
	}

UINT CDispFormat::GeneralEditReal(CEditCtx *pEdit, UINT uCode)
{
	return GeneralEditNumeric(pEdit, uCode);
	}

UINT CDispFormat::GeneralEditString(CEditCtx *pEdit, UINT uCode)
{
	if( uCode == 0x00 ) {

		pEdit->m_uKeypad  = keypadAlpha;

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		return editUpdate;
		}

	if( uCode == 0x0D ) {

		PUTF pText = wstrdup(pEdit->m_Edit);

		if( !Validate(pEdit, DWORD(pText)) ) {
						
			return editError;
			}

		if( pEdit->m_pValue->SetValue(DWORD(pText), typeString, setNone) ) {

			return editCommit;
			}

		return editUpdate;
		}

	if( uCode == 0x1B ) {

		return editAbort;
		}

	if( uCode == 0x7F ) {

		if( pEdit->m_fDefault ) {

			pEdit->m_fDefault = FALSE;
			
			pEdit->m_uCursor  = cursorLast;

			return editUpdate;
			}

		UINT uLen = pEdit->m_Edit.GetLength();

		if( uLen ) {

			pEdit->m_Edit.Delete(uLen - 1, 1);

			return editUpdate;
			}

		return editNone;
		}

	if( uCode >= 0x20 ) {

		if( pEdit->m_fDefault ) {

			pEdit->m_Edit.Empty();
			
			pEdit->m_fDefault = FALSE;
			
			pEdit->m_uCursor  = cursorLast;
			}

		if( pEdit->m_Edit.GetLength() < 200 ) {

			pEdit->m_Edit += WCHAR(uCode);

			return editUpdate;
			}

		return editNone;
		}

	return editNone;
	}

// Implementation

void CDispFormat::MakeDigitsFixed(CUnicode &Text)
{
	MakeDigitsFixed(PUTF(PCUTF(Text)));
	}

void CDispFormat::MakeLettersFixed(CUnicode &Text)
{
	MakeLettersFixed(PUTF(PCUTF(Text)));
	}

void CDispFormat::MakeDigitsFixed(PUTF pText)
{
	while( *pText ) {

		if( *pText >= digitSimple && *pText < digitSimple + 10 ) {

			*pText -= digitSimple;

			*pText += digitFixed;
			}

		pText++;
		}
	}

void CDispFormat::MakeLettersFixed(PUTF pText)
{
	while( *pText ) {

		if( *pText >= letterSimple && *pText < letterSimple + 6 ) {

			*pText -= letterSimple;

			*pText += letterFixed;
			}

		pText++;
		}
	}

BOOL CDispFormat::Validate(CEditCtx *pEdit, DWORD Data)
{
	if( pEdit->m_pValid ) {

		if( !pEdit->m_pValid->IsAvail() ) {
			
			return FALSE;
			}
		
		if( !pEdit->m_pValid->Execute(pEdit->m_Type, &Data) ) {
			
			return FALSE;
			}
		}

	return TRUE;
	}

// End of File
