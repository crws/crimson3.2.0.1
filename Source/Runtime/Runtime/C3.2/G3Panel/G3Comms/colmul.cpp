
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Display Color
//

// Constructor

CDispColorMulti::CDispColorMulti(void)
{
	m_pList   = New CDispColorMultiList;

	m_Default = MAKELONG(GetRGB(31,31,31),GetRGB(0,0,0));

	m_Range   = 0;
	}

// Destructor

CDispColorMulti::~CDispColorMulti(void)
{
	delete m_pList;
	}

// Initialization

void CDispColorMulti::Load(PCBYTE &pData)
{
	ValidateLoad("CDispColorMulti", pData);

	m_Default = GetLong(pData);

	m_Range   = GetByte(pData);

	m_pList->Load(pData);
	}

// Scan Control

BOOL CDispColorMulti::IsAvail(void)
{
	return m_pList->IsAvail();
	}

BOOL CDispColorMulti::SetScan(UINT Code)
{
	return m_pList->SetScan(Code);
	}

// Color Access

DWORD CDispColorMulti::GetColorPair(DWORD Data, UINT Type)
{
	UINT  c = m_pList->GetItemCount();

	DWORD d = m_Default;

	for( UINT n = 0; n < c; n++ ) {

		CDispColorMultiEntry *pItem = m_pList->GetItem(c - 1 - n);

		if( pItem ) {

			if( pItem->MatchData(Data, Type, m_Range, d) ) {

				break;
				}
			}
		}

	return d;
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Color List
//

// Constructor

CDispColorMultiList::CDispColorMultiList(void)
{
	m_uCount = 0;

	m_pEntry = NULL;
	}

// Destructor

CDispColorMultiList::~CDispColorMultiList(void)
{
	delete [] m_pEntry;
	}

// Initialization

void CDispColorMultiList::Load(PCBYTE &pData)
{
	ValidateLoad("CDispColorMultiList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		m_pEntry = New CDispColorMultiEntry [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			m_pEntry[n].Load(pData);
			}
		}
	}

// Item Access

CDispColorMultiEntry * CDispColorMultiList::GetItem(UINT uPos) const
{
	return m_pEntry + uPos;
	}

// Scan Control

BOOL CDispColorMultiList::IsAvail(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDispColorMultiEntry *pItem = GetItem(n);
		
		if( pItem ) {

			if( !pItem->IsAvail() ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CDispColorMultiList::SetScan(UINT Code)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CDispColorMultiEntry *pItem = GetItem(n);
		
		if( pItem ) {

			pItem->SetScan(Code);
			}
		}

	return TRUE;
	}

// Attributes

UINT CDispColorMultiList::GetItemCount(void) const
{
	return m_uCount;
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Color Entry
//

// Constructor

CDispColorMultiEntry::CDispColorMultiEntry(void)
{
	m_pData = NULL;

	m_Color = MAKELONG(GetRGB(31,31,31),GetRGB(0,0,0));
	}

// Destructor

CDispColorMultiEntry::~CDispColorMultiEntry(void)
{
	delete m_pData;
	}

// Initialization

void CDispColorMultiEntry::Load(PCBYTE &pData)
{
	ValidateLoad("CDispColorMultiEntry", pData);

	GetCoded(pData, m_pData);

	m_Color = GetLong(pData);
	}

// Scan Control

BOOL CDispColorMultiEntry::IsAvail(void)
{
	if( !IsItemAvail(m_pData) ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CDispColorMultiEntry::SetScan(UINT Code)
{
	SetItemScan(m_pData, Code);

	return TRUE;
	}

// Data Matching

BOOL CDispColorMultiEntry::MatchData(DWORD Data, UINT Type, BOOL fRange, DWORD &Color)
{
	if( m_pData ) {

		DWORD Test = m_pData->ExecVal();

		if( fRange ) {

			if( Type == typeInteger ) {

				if( INT(Data) >= INT(Test) ) {

					Color = m_Color;

					return TRUE;
					}
				}

			if( Type == typeReal ) {

				if( I2R(Data) >= I2R(Test) ) {

					Color = m_Color;

					return TRUE;
					}
				}

			return FALSE;
			}

		if( Data == Test ) {

			Color = m_Color;

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
