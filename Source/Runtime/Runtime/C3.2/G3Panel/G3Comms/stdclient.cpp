
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "stdclient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Constants
//

static UINT const timeTimeout      = (5*60*1000);

static UINT const timeInitTimeout  = (5*60*1000);

static UINT const timeDataTimeout  = (2*60*1000);

static UINT const timeSendTimeout  = (3*60*1000);

static UINT const timeSendDelay	   = 10;

static UINT const timeRecvTimeout  = (1*60*1000);

static UINT const timeRecvDelay	   = 10;

static UINT const timeBuffDelay    = 100;

//////////////////////////////////////////////////////////////////////////
//
// Standard RFC Endpoint
//

// Constructor

CStdEndpoint::CStdEndpoint(void)
{
	m_pCmdSock = NULL;

	m_fDebug   = TRUE;
}

// Transport

BOOL CStdEndpoint::Send(ISocket *pSock, PCTXT pText)
{
	UINT uSize  = strlen(pText);

	UINT uLimit = 1280;

	Log(">>> %s", strlen(pText) > 128 ? PCTXT(CString(pText).Left(4) + "...\n") : pText);

	while( uSize ) {

		CBuffer *pBuff = BuffAllocate(uLimit);

		if( pBuff ) {

			PBYTE pData = pBuff->GetData();

			UINT  uCopy = min(uLimit, uSize);

			memcpy(pData, pText, uCopy);

			pBuff->AddTail(uCopy);

			SetTimer(timeSendTimeout);

			while( pSock->Send(pBuff) == E_FAIL ) {

				UINT Phase;

				pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					if( GetTimer() ) {

						Sleep(timeSendDelay);

						continue;
					}

					Log("*** Send socket timeout\n");
				}
				else
					Log("*** Send socket error\n");

				BuffRelease(pBuff);

				return FALSE;
			}

			pText += uCopy;

			uSize -= uCopy;
		}
		else
			Sleep(timeBuffDelay);
	}

	return TRUE;
}

BOOL CStdEndpoint::Send(PCTXT pText)
{
	return Send(m_pCmdSock, pText);
}

// Socket Management

BOOL CStdEndpoint::CheckCmdSocket(void)
{
	if( m_pCmdSock ) {

		UINT Phase;

		m_pCmdSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			AbortCmdSocket();

			return FALSE;
		}

		if( Phase == PHASE_CLOSING ) {

			CloseCmdSocket();

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

void CStdEndpoint::CloseCmdSocket(void)
{
	if( m_pCmdSock ) {

		m_pCmdSock->Close();

		m_pCmdSock->Release();

		m_pCmdSock = NULL;
	}
}

void CStdEndpoint::AbortCmdSocket(void)
{
	if( m_pCmdSock ) {

		m_pCmdSock->Close();

		m_pCmdSock->Release();

		m_pCmdSock = NULL;
	}
}

// File Transmission

BOOL CStdEndpoint::SendFile(ISocket *pSock, FILE *pFile)
{
	return SendFile(pSock, pFile, FALSE);
}

BOOL CStdEndpoint::SendFile(ISocket *pSock, FILE *pFile, BOOL fCode)
{
	DWORD t1 = GetNow();

	DWORD nn = 0;

	CBase64 Base64;

	UINT  uLimit = fCode ? 1280 : 1280;

	UINT  uChunk = fCode ? 912 : 1280;

	PBYTE pWork  = fCode ? New BYTE[uChunk] : NULL;

	SetTaskLimit(50, 5);

	for( ;;) {

		CBuffer *pBuff = BuffAllocate(uLimit);

		if( pBuff ) {

			PBYTE pData = pBuff->GetData();

			PBYTE pRead = fCode ? pWork : pData;

			UINT  uSize = fread(pRead, 1, uChunk, pFile);

			if( uSize ) {

				if( fCode ) {

					UINT uDone = uLimit;

					/*CByteArray Data = CBase64::ToBase64(pRead, uSize);

					Base64.Attach(pRead, uSize);

					Base64.Encode(pData, uDone, TRUE);*/

					pBuff->AddTail(uDone);
				}
				else
					pBuff->AddTail(uSize);

				SetTimer(timeSendTimeout);

				while( pSock->Send(pBuff) == E_FAIL ) {

					UINT Phase;

					pSock->GetPhase(Phase);

					if( Phase == PHASE_OPEN ) {

						if( GetTimer() ) {

							Sleep(timeSendDelay);

							continue;
						}

						Log("*** Send file timeout\n");
					}
					else
						Log("*** Send file error\n");

					BuffRelease(pBuff);

					SetTaskLimit(0, 0);

					delete[] pWork;

					return FALSE;
				}

				nn += uSize;
			}
			else {
				BuffRelease(pBuff);

				SetTaskLimit(0, 0);

				DWORD t2 = GetNow();

				Log("=== Sent %u bytes in %u seconds (%u bps)\n",
				    nn,
				    (t2 - t1),
				    (t2 - t1) ? (nn / (t2 - t1)) : 0
				);

				delete[] pWork;

				return TRUE;
			}

			continue;
		}

		Sleep(timeBuffDelay);
	}

	SetTaskLimit(0, 0);

	return FALSE;
}

BOOL CStdEndpoint::RecvFile(ISocket *pSock, FILE *pFile)
{
	DWORD t1 = GetNow();

	DWORD nn = 0;

	SetTaskLimit(50, 5);

	SetTimer(timeRecvTimeout);

	for( ;;) {

		CBuffer *pBuff = NULL;

		if( pSock->Recv(pBuff) == E_FAIL ) {

			UINT Phase;

			pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				if( GetTimer() ) {

					Sleep(timeRecvDelay);

					continue;
				}
			}

			if( Phase == PHASE_CLOSING ) {

				pSock->Close();

				SetTaskLimit(0, 0);

				DWORD t2 = GetNow();

				Log("=== Recv %u bytes in %u seconds (%u bps)\n",
				    nn,
				    (t2 - t1),
				    (t2 - t1) ? (nn / (t2 - t1)) : 0
				);

				return TRUE;
			}

			if( Phase == PHASE_OPEN ) {

				Log("*** Recv file timeout\n");
			}
			else
				Log("*** Recv file error\n");

			SetTaskLimit(0, 0);

			return FALSE;
		}

		fwrite(pBuff->GetData(), 1, pBuff->GetSize(), pFile);

		nn += pBuff->GetSize();

		BuffRelease(pBuff);

		SetTimer(timeRecvTimeout);
	}
}

// Logging

void CStdEndpoint::Log(PCTXT pForm, ...)
{
	DWORD Secs = GetNow();

	if( !m_LogFile.IsEmpty() ) {

		CAutoFile File(m_LogFile, "a+");

		if( File ) {

			char sText[512];

			va_list pArgs;

			va_start(pArgs, pForm);

			SPrintf(sText,
				"%4.4u/%2.2u/%2.2u %2.2u:%2.2u:%2.2u ",
				GetYear(Secs),
				GetMonth(Secs),
				GetDate(Secs),
				GetHour(Secs),
				GetMin(Secs),
				GetSec(Secs)
			);

			VSPrintf(sText + strlen(sText),
				 pForm,
				 pArgs
			);

			UINT uLen = strlen(sText);

			sText[uLen-1] = '\r';
			sText[uLen-0] = '\n';
			sText[uLen+1] = 0x00;

			File.Write(sText, uLen + 1);

			va_end(pArgs);
		}
	}

	if( m_fDebug ) {

		if( AfxHasDebug() ) {

			va_list pArgs;

			va_start(pArgs, pForm);

			AfxTrace("%4.4u/%2.2u/%2.2u %2.2u:%2.2u:%2.2u ",
				 GetYear(Secs),
				 GetMonth(Secs),
				 GetDate(Secs),
				 GetHour(Secs),
				 GetMin(Secs),
				 GetSec(Secs)
			);

			AfxTraceArgs(pForm,
				     pArgs
			);

			va_end(pArgs);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Standard RFC Client
//

// Constructor

CStdClient::CStdClient(void)
{
	m_uOption = NOTHING;

	m_pTls    = NULL;
}

// Destructor

CStdClient::~CStdClient(void)
{
	if( m_pTls ) {

		m_pTls->Release();
	}
}

// Configuration

UINT CStdClient::GetCmdPort(void)
{
	return 0;
}

DWORD CStdClient::GetServer(void)
{
	return 0;
}

// Transport

BOOL CStdClient::RecvReply(void)
{
	return RecvReply(timeTimeout);
}

BOOL CStdClient::RecvReply(UINT uTimeout)
{
	m_fReplyMore = FALSE;

	m_uReplyPtr  = 0;

	m_uReplyCode = 0;

	m_Reply.Empty();

	SetTimer(uTimeout);

	while( GetTimer() ) {

		char cData = 0;

		UINT uSize = 1;

		if( m_pCmdSock->Recv(PBYTE(&cData), uSize) == S_OK ) {

			m_Reply += cData;

			OnReply(cData);

			if( m_uReplyCode ) {

				if( !m_fReplyMore ) {

					Log("<<< %3.3u\n", m_uReplyCode);

					m_sReplyData[m_uReplyPtr] = 0;

					return TRUE;
				}
			}
		}
		else {
			UINT Phase;

			m_pCmdSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				Sleep(timeRecvDelay);

				continue;
			}

			Log("*** Recv socket error\n");

			m_uReplyCode = 999;

			return FALSE;
		}
	}

	Log("*** Recv socket timeout\n");

	m_uReplyCode = 999;

	return FALSE;
}

void CStdClient::OnReply(char cData)
{
	if( cData == '\n' ) {

		m_sReplyData[4] = 0;

		if( m_fReplyMore ) {

			if( m_sReplyData[3] == ' ' ) {

				if( m_uReplyCode == UINT(atoi(m_sReplyData)) ) {

					m_fReplyMore = FALSE;
				}
			}
		}
		else {
			m_fReplyMore = (m_sReplyData[3] == '-');

			m_uReplyCode = UINT(atoi(m_sReplyData));
		}

		m_uReplyPtr = 0;

		return;
	}

	if( isprint(cData) ) {

		if( m_uReplyPtr < sizeof(m_sReplyData) ) {

			m_sReplyData[m_uReplyPtr] = cData;

			m_uReplyPtr++;
		}
	}
}

// Socket Management

BOOL CStdClient::OpenCmdSocket(BOOL fLarge, BOOL fSSL)
{
	if( fSSL && !m_pTls ) {

		if( !CreateTlsContext() ) {

			return FALSE;
		}
	}

	if( CheckCmdSocket() ) {

		return TRUE;
	}

	for( UINT n = 0; n < 5; n++ ) {

		if( fSSL && m_pTls ) {

			m_pCmdSock = m_pTls->CreateSocket(NULL, NULL, 0);
		}
		else {
			m_pCmdSock = NULL;

			AfxNewObject("sock-tcp", ISocket, m_pCmdSock);
		}

		if( m_pCmdSock ) {

			if( fLarge ) {

				UINT uOption = OPT_SEND_QUEUE;

				UINT uValue  = 255;

				m_pCmdSock->SetOption(uOption, uValue);
			}

			DWORD Server = 0;

			if( m_uOption == NOTHING ) {

				Server = GetServer();
			}
			else {
				AfxGetAutoObject(pUtils, "ip", 0, INetUtilities);

				Server = pUtils->GetOption(m_uOption);
			}

			if( !Server ) {

				AbortCmdSocket();

				return FALSE;
			}

			if( m_pCmdSock->Connect(IPREF(Server), GetCmdPort()) == S_OK ) {

				SetTimer(timeInitTimeout);

				while( GetTimer() ) {

					UINT Phase;

					m_pCmdSock->GetPhase(Phase);

					if( Phase == PHASE_OPEN ) {

						m_uState = 1;

						return TRUE;
					}

					if( Phase == PHASE_CLOSING ) {

						m_pCmdSock->Close();
					}

					if( Phase == PHASE_ERROR ) {

						break;
					}

					Sleep(timeRecvDelay);
				}

				AbortCmdSocket();
			}
		}

		Sleep(1000 * (1<<n));
	}

	AbortCmdSocket();

	return FALSE;
}

BOOL CStdClient::SwitchCmdSocket(void)
{
	if( CreateTlsContext() ) {

		m_pCmdSock = m_pTls->CreateSocket(m_pCmdSock, NULL, 0);

		if( m_pCmdSock ) {

			IPADDR Ip;

			WORD Port = GetCmdPort();

			m_pCmdSock->GetRemote(Ip);

			if( m_pCmdSock->Connect(Ip, Port) == S_OK ) {

				SetTimer(timeInitTimeout);

				while( GetTimer() ) {

					UINT Phase;

					m_pCmdSock->GetPhase(Phase);

					if( Phase == PHASE_OPENING ) {

						Sleep(10);

						continue;
					}

					if( Phase == PHASE_OPEN ) {

						return TRUE;
					}

					break;
				}
			}
		}
	}

	return FALSE;
}

BOOL CStdClient::CreateTlsContext(void)
{
	if( g_pMatrix ) {

		if( !m_pTls ) {

			g_pMatrix->CreateClientContext(m_pTls);

			return m_pTls ? TRUE : FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//
// Standard RFC Server
//

// Constructor

CStdServer::CStdServer(void)
{
	m_pTls = NULL;
}

// Destructor

CStdServer::~CStdServer(void)
{
	if( m_pTls ) {

		m_pTls->Release();
	}
}

// Configuration

UINT CStdServer::GetCmdPort(void)
{
	return 0;
}

// Transport

BOOL CStdServer::SendReply(UINT uCode, PCTXT pText)
{
	char sBuff[128];

	SPrintf(sBuff, "%3.3u %s\r\n", uCode, pText);

	return Send(sBuff);
}

BOOL CStdServer::RecvCommand(void)
{
	CBuffer *pBuff = NULL;

	UINT    uPhase = 0;

	for( ;;) {

		if( m_pCmdSock->Recv(pBuff) == S_OK ) {

			PTXT pText = PTXT(pBuff->GetData());

			UINT uSize = pBuff->GetSize();

			while( uSize-- ) OnCommand(*pText++);

			BuffRelease(pBuff);

			if( m_uCmdState == 2 ) {

				// NOTE -- Extra null needed for parsing.

				m_sCmdData[m_uCmdPtr++] = 0;

				m_sCmdData[m_uCmdPtr++] = 0;

				m_uCmdState = 0;

				m_uCmdPtr   = 0;

				Log("<<< %s\n", m_sCmdData);

				return TRUE;
			}
		}
		else {
			m_pCmdSock->GetPhase(uPhase);

			if( uPhase == PHASE_OPEN ) {

				return FALSE;
			}

			Log("*** Recv socket error\n");

			AbortCmdSocket();

			return FALSE;
		}
	}

	return FALSE;
}

void CStdServer::OnCommand(char cData)
{
	if( m_uCmdState == 0 ) {

		if( cData == '\n' ) {

			m_uCmdState = 2;

			return;
		}

		if( isprint(cData) ) {

			m_sCmdData[m_uCmdPtr++] = cData;

			if( m_uCmdPtr == sizeof(m_sCmdData) - 1 ) {

				m_uCmdState = 1;
			}

			return;
		}
	}

	if( m_uCmdState == 1 ) {

		if( cData == '\n' ) {

			m_uCmdState = 2;

			return;
		}
	}
}

// Socket Management

BOOL CStdServer::OpenCmdSocket(BOOL fLarge)
{
	if( !m_pCmdSock ) {

		AfxNewObject("sock-tcp", ISocket, m_pCmdSock);

		if( m_pCmdSock ) {

			if( fLarge ) {

				UINT uOption = OPT_SEND_QUEUE;

				UINT uValue  = 255;

				m_pCmdSock->SetOption(uOption, uValue);
			}

			if( m_pCmdSock->Listen(GetCmdPort()) == S_OK ) {

				m_uCmdState = 0;

				m_uCmdPtr   = 0;

				return TRUE;
			}

			Sleep(1000);

			AbortCmdSocket();

			return FALSE;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CStdServer::WaitCmdSocket(void)
{
	if( m_pCmdSock ) {

		UINT Phase;

		m_pCmdSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {

			return TRUE;
		}

		if( Phase == PHASE_ERROR ) {

			return TRUE;
		}

		if( Phase == PHASE_CLOSING ) {

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CStdServer::SwitchCmdSocket(void)
{
	if( CreateTlsContext() ) {

		m_pCmdSock = m_pTls->CreateSocket(m_pCmdSock);

		if( m_pCmdSock ) {

			WORD Port = GetCmdPort();

			if( m_pCmdSock->Listen(Port) == S_OK ) {

				SetTimer(timeInitTimeout);

				while( GetTimer() ) {

					UINT Phase;

					m_pCmdSock->GetPhase(Phase);

					if( Phase == PHASE_OPENING ) {

						Sleep(10);

						continue;
					}

					if( Phase == PHASE_OPEN ) {

						return TRUE;
					}

					break;
				}
			}
		}
	}

	return FALSE;
}

BOOL CStdServer::CreateTlsContext(void)
{
	if( g_pMatrix ) {

		if( !m_pTls ) {

			g_pMatrix->CreateServerContext(m_pTls);

			return m_pTls ? TRUE : FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

// End of File
