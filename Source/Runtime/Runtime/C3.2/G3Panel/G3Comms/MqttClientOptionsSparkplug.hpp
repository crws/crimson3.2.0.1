
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqqtClientOptionsSparkplug_HPP

#define	INCLUDE_MqqtClientOptionsSparkplug_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientOptionsCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for Sparkplug Options
//

class CMqttClientOptionsSparkplug : public CMqttClientOptionsCrimson
{
public:
	// Constructor
	CMqttClientOptionsSparkplug(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Config Fixup
	BOOL FixConfig(void);

	// Attributes
	CString GetExtra(void) const;

	// Data Members
	BOOL    m_fReboot;
	CString m_GroupId;
	CString m_NodeId;
	CString m_TagsFolder;
	CString m_PriAppId;

protected:
	// Implementation
	BOOL MakeClientId(void);
};

// End of File

#endif
