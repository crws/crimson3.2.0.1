
#include "intern.hpp"

#include "secure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// User List
//

// Constructor

CUserList::CUserList(void)
{
	m_uCount = 0;

	m_ppUser = NULL;
	}

// Destructor

CUserList::~CUserList(void)
{
	while( m_uCount-- ) {

		delete m_ppUser[m_uCount];
		}

	delete [] m_ppUser;
	}

// Initialization

void CUserList::Load(PCBYTE &pData)
{
	ValidateLoad("CUserList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		m_ppUser = New CUserItem * [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CUserItem *pUser = NULL;

			if( GetByte(pData) ) {

				pUser = New CUserItem;

				pUser->Load(pData);
				}

			m_ppUser[n] = pUser;
			}
		}
	}

// Operations

void CUserList::LoadCreds(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppUser[n] ) {

			m_ppUser[n]->LoadCred();
			}
		}
	}

void CUserList::SaveCreds(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppUser[n] ) {

			m_ppUser[n]->SaveCred(FALSE);
			}
		}

	CCommsSystem::m_pThis->m_pTags->Commit();
	}

//////////////////////////////////////////////////////////////////////////
//
// User Item
//

// Constructor

CUserItem::CUserItem(void)
{
	m_pRealName = NULL;

	m_pPassword = NULL;

	m_Force     = 0;

	m_Addr      = 0;

	m_Rights    = 0;
	}

// Destructor

CUserItem::~CUserItem(void)
{
	delete m_pRealName;

	delete m_pPassword;
	}

// Attributes

CString CUserItem::GetUserName(void) const
{
	// REV3 -- This ought to be an expression, too.

	return m_Name;
	}

CUnicode CUserItem::GetRealName(void) const
{
	return m_pRealName->GetText(UniConvert(m_Name));
	}

// Operations

void CUserItem::LoadCred(void)
{
	g_pPersist->GetData( PBYTE(&m_Cred),
			m_Addr,
			sizeof(m_Cred)
			);

	if( m_Cred.m_wMagic != 0x8794 ) {

		memset(&m_Cred, 0, sizeof(m_Cred));

		m_Cred.m_wMagic   = 0x8794;

		m_Cred.m_bBioSlot = 255;

		strcpy(m_Cred.m_sPass, UniConvert(m_pPassword->GetText(L"")));

		SaveCred(TRUE);
		}

	if( m_Force ) {

		strcpy(m_Cred.m_sPass, UniConvert(m_pPassword->GetText(L"")));

		SaveCred(TRUE);
		}
	}

void CUserItem::SaveCred(BOOL fCommit)
{
	g_pPersist->PutData( PBYTE(&m_Cred),
			m_Addr,
			sizeof(m_Cred)
			);

	if( fCommit ) {

		CCommsSystem::m_pThis->m_pTags->Commit();
		}
	}

// Initialization

void CUserItem::Load(PCBYTE &pData)
{
	ValidateLoad("CUserItem", pData);

	m_Addr = GetLong(pData);

	m_Name = UniConvert(GetWide(pData));

	GetCoded(pData, m_pRealName);

	GetCoded(pData, m_pPassword);

	m_Rights = GetLong(pData);

	m_Force  = GetByte(pData);
	}

// End of File
