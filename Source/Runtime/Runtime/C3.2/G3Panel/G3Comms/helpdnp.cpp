
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Helper Implementation
//

class CDnpHelper : public IDnpHelper 
{
	public:
		// Constructor
		CDnpHelper(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDnpHelper
		IDnpChannelConfig * METHOD GetConfig(IPADDR IP, IPADDR IP2, WORD wTcp, WORD wUdp, WORD wTO);
		IDnpChannel       * METHOD OpenChannel(IDataHandler *pHandler, WORD wSrc, BOOL fMaster);
		IDnpChannel       * METHOD OpenChannel(IDnpChannelConfig *pConfig,  WORD wSrc, BOOL fMaster);
		BOOL      	    METHOD CloseChannel(void *pVoid, WORD wSrc, BOOL fMaster);
		IDnpSession	  * METHOD OpenSession(IDnpChannel *pChannel, WORD wDest, WORD wTO, DWORD dwLink, void *pCfg);
		BOOL      	    METHOD CloseSession(IDnpSession *pSession);
		BOOL	  	    METHOD CloseSessions(IDnpChannel *pChannel);
		void		    METHOD Service(void);
		
	protected:
		// Data
		ULONG   m_uRefs;
		IDnp  * m_pDnp;

		// Destructor
		~CDnpHelper(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP Helper Implementation
//

// Instantiator

global IDnpHelper * Create_DnpHelper(void)
{
	return New CDnpHelper;
	}

// Constructor

CDnpHelper::CDnpHelper(void)
{
	StdSetRef();

	m_pDnp = NULL;

	AfxGetObject("comms.dnp3", 0, IDnp, m_pDnp);
	}

// IUnknown

HRESULT CDnpHelper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDnpHelper);

	StdQueryInterface(IDnpHelper);

	return E_NOINTERFACE;
	}

ULONG CDnpHelper::AddRef(void)
{
	StdAddRef();
	}

ULONG CDnpHelper::Release(void)
{
	StdRelease();
	}

// IDnpHelper

IDnpChannelConfig * METHOD CDnpHelper::GetConfig(IPADDR IP, IPADDR IP2, WORD wTcp, WORD wUdp, WORD wTO)
{
	if( m_pDnp ) {

		return m_pDnp->GetConfig(IP, IP2, wTcp, wUdp, wTO);
		}

	return NULL;
	}

IDnpChannel * METHOD CDnpHelper::OpenChannel(IDataHandler *pHandler, WORD wSrc, BOOL fMaster)
{
	if( m_pDnp ) {

		if( m_pDnp->Init() ) {

			return m_pDnp->OpenChannel(pHandler, wSrc, fMaster);
			}
		}

	return NULL;
	}

IDnpChannel * METHOD CDnpHelper::OpenChannel(IDnpChannelConfig *pConfig, WORD wSrc, BOOL fMaster)
{
	if( m_pDnp ) {

		if( m_pDnp->Init() ) {

			return m_pDnp->OpenChannel(pConfig, wSrc, fMaster);
			}
		}

	return NULL;
	}

BOOL METHOD CDnpHelper::CloseChannel(void *pVoid, WORD wSrc, BOOL fMaster)
{
	if( m_pDnp ) {

		return m_pDnp->CloseChannel(pVoid, wSrc, fMaster);
		}

	return FALSE;
	}

IDnpSession * METHOD CDnpHelper::OpenSession(IDnpChannel *pChannel, WORD wDest, WORD wTO, DWORD dwLink, void *pCfg)
{
	if( m_pDnp ) {

		return m_pDnp->OpenSession(pChannel, wDest, wTO, dwLink, pCfg);
		}

	return FALSE;
	}

BOOL METHOD CDnpHelper::CloseSession(IDnpSession * pSession)
{
	if( m_pDnp ) {

		return m_pDnp->CloseSession(pSession);
		}

	return FALSE;
	}

BOOL METHOD CDnpHelper::CloseSessions(IDnpChannel *pChannel)
{
	if( pChannel ) {

		return pChannel->CloseSessions();
		}

	return FALSE;
	}

void METHOD CDnpHelper::Service(void)
{
	if( m_pDnp ) {

		m_pDnp->Service();
		}
	}

// Protected Destructor

CDnpHelper::~CDnpHelper(void)
{
	m_pDnp->Release();

	m_pDnp = NULL;
	}

// End of File
