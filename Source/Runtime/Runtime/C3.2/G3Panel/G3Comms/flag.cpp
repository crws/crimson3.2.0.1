
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Flag Display Format
//

// Constructor

CDispFormatFlag::CDispFormatFlag(void)
{
	m_pOn  = NULL;

	m_pOff = NULL;
	}

// Destructor

CDispFormatFlag::~CDispFormatFlag(void)
{
	delete m_pOn;

	delete m_pOff;
	}

// Initialization

void CDispFormatFlag::Load(PCBYTE &pData)
{
	ValidateLoad("CDispFormatFlag", pData);

	GetCoded(pData, m_pOn);

	GetCoded(pData, m_pOff);
	}

// Scan Control

BOOL CDispFormatFlag::IsAvail(void)
{
	if( !IsItemAvail(m_pOn) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pOff) ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CDispFormatFlag::SetScan(UINT Code)
{
	SetItemScan(m_pOn,  Code);

	SetItemScan(m_pOff, Code);

	return TRUE;
	}

// Formatting

CUnicode CDispFormatFlag::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		return L"";
		}

	if( Type == typeInteger ) {

		if( Data ) {
			
			return m_pOn->GetText(L"ON");
			}

		return m_pOff->GetText(L"OFF");
		}

	if( Type == typeReal ) {

		if( I2R(Data) ) {
			
			return m_pOn->GetText(L"ON");
			}

		return m_pOff->GetText(L"OFF");
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Parsing

BOOL CDispFormatFlag::Parse(DWORD &Data, CString Text, UINT Type)
{
	if( Type == typeInteger || Type == typeReal ) {

		CString State0 = UniConvert(m_pOff->GetText(L"OFF"));

		CString State1 = UniConvert(m_pOn ->GetText(L"ON" ));

		if( Text == State0 ) {

			if( Type == typeInteger ) {

				Data = 0;

				return TRUE;
				}

			Data = R2I(0.0);

			return TRUE;
			}

		if( Text == State1 ) {

			if( Type == typeInteger ) {

				Data = 1;

				return TRUE;
				}

			Data = R2I(1.0);

			return TRUE;
			}

		return FALSE;
		}

	return GeneralParse(Data, Text, Type);
	}

// Presentation

CString CDispFormatFlag::GetStates(void)
{
	CString States;

	for( UINT n = 0; n < 2; n++ ) {

		CString Text = UniConvert(n ? m_pOn->GetText(L"ON") : m_pOff->GetText(L"OFF"));

		Text.Replace("&", "&amp;");
		Text.Replace(",", "&sep;");
		Text.Replace("<", "&lt;" );
		Text.Replace(">", "&gt;" );

		if( !States.IsEmpty() ) {

			States += ",";
			}

		States += Text;
		}
	
	return States;
	}

// Editing

UINT CDispFormatFlag::Edit(CEditCtx *pEdit, UINT uCode)
{
	if( uCode == 0x00 ) {

		if( pEdit->m_uFlags & flagTwoFlag ) {

			pEdit->m_uKeypad = keypadNudge1;
			}
		else
			pEdit->m_uKeypad = keypadNudge2;

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		return editUpdate;
		}

	if( uCode == 0x1B ) {

		if( pEdit->m_uFlags & flagTwoFlag ) {

			if( !pEdit->m_fDefault ) {

				pEdit->m_uCursor  = cursorAll;

				pEdit->m_fDefault = TRUE;

				return editUpdate;
				}
			}

		return editAbort;
		}

	if( uCode == 0x0D ) {

		if( pEdit->m_uFlags & flagTwoFlag ) {

			if( !pEdit->m_fDefault ) {

				DWORD Data = pEdit->m_pValue->Execute(pEdit->m_Type);

				if( pEdit->m_Data == Data ) {

					return editAbort;
					}

				if( Validate(pEdit, pEdit->m_Data) ) {

					if( pEdit->m_pValue->SetValue(pEdit->m_Data, pEdit->m_Type, setNone) ) {

						return editCommit;
						}

					return editUpdate;
					}

				pEdit->m_uCursor = cursorError;

				pEdit->m_fError  = TRUE;

				return editError;
				}

			return editAbort;
			}

		return editNone;
		}

	if( uCode == 0x0B ) {

		if( pEdit->m_uFlags & flagTwoFlag ) {

			if( !pEdit->m_Data ) {

				pEdit->m_Data     = GetMax(pEdit->m_Type);

				pEdit->m_Edit     = Format(pEdit->m_Data, pEdit->m_Type, fmtStd);

				pEdit->m_uCursor  = cursorLast;

				pEdit->m_fDefault = FALSE;

				return editUpdate;
				}

			return editNone;
			}

		if( Validate(pEdit, 1) ) {

			pEdit->m_pValue->SetValue(1, typeInteger, setNone);

			return editUpdate;
			}

		return editNone;
		}

	if( uCode == 0x0A ) {

		if( pEdit->m_uFlags & flagTwoFlag ) {

			if( pEdit->m_Data ) {

				pEdit->m_Data     = GetMin(pEdit->m_Type);

				pEdit->m_Edit     = Format(pEdit->m_Data, pEdit->m_Type, fmtStd);

				pEdit->m_uCursor  = cursorLast;

				pEdit->m_fDefault = FALSE;

				return editUpdate;
				}

			return editNone;
			}

		if( Validate(pEdit, 0) ) {

			pEdit->m_pValue->SetValue(0, typeInteger, setNone);

			return editUpdate;
			}

		return editNone;
		}

	return editNone;
	}

// Limit Access

DWORD CDispFormatFlag::GetMin(UINT Type)
{
	if( Type == typeInteger ) {

		return FALSE;
		}

	if( Type == typeReal ) {

		return R2I(FALSE);
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatFlag::GetMax(UINT Type)
{
	if( Type == typeInteger ) {

		return TRUE;
		}

	if( Type == typeReal ) {

		return R2I(TRUE);
		}

	return CDispFormat::GetMax(Type);
	}

// End of File
