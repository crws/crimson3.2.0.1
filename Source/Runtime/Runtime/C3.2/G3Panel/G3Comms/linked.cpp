
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Linked Display Format
//

// Constructor

CDispFormatLinked::CDispFormatLinked(void)
{
	m_pFormat = NULL;
	}

// Destructor

CDispFormatLinked::~CDispFormatLinked(void)
{
	}

// Initialization

void CDispFormatLinked::Load(PCBYTE &pData)
{
	ValidateLoad("CDispFormatLinked", pData);

	m_Tag = GetWord(pData);
	}

// Scan Control

BOOL CDispFormatLinked::IsAvail(void)
{
	if( FindFormat() ) {

		return m_pFormat->IsAvail();
		}

	return TRUE;
	}

BOOL CDispFormatLinked::SetScan(UINT Code)
{
	if( FindFormat() ) {

		return m_pFormat->SetScan(Code);
		}

	return TRUE;
	}

// Formatting

CUnicode CDispFormatLinked::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( FindFormat() ) {

		return m_pFormat->Format(Data, Type, Flags);
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Parsing

BOOL CDispFormatLinked::Parse(DWORD &Data, CString Text, UINT Type)
{
	if( FindFormat() ) {

		return m_pFormat->Parse(Data, Text, Type);
		}

	return GeneralParse(Data, Text, Type);
	}

// Presentation

CString CDispFormatLinked::GetStates(void)
{
	if( FindFormat() ) {

		return m_pFormat->GetStates();
		}

	return "";
	}

// Editing

UINT CDispFormatLinked::Edit(CEditCtx *pEdit, UINT uCode)
{
	if( FindFormat() ) {

		return m_pFormat->Edit(pEdit, uCode);
		}

	return CDispFormat::GeneralEdit(pEdit, uCode);
	}

// Limit Access

BOOL CDispFormatLinked::NeedsLimits(void)
{
	if( FindFormat() ) {

		return m_pFormat->NeedsLimits();
		}

	return TRUE;
	}

DWORD CDispFormatLinked::GetMin(UINT Type)
{
	// REV3 -- Ought to pass uPos into here.

	if( FindFormat() ) {

		return m_pTag->GetMinValue(0, Type);
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatLinked::GetMax(UINT Type)
{
	// REV3 -- Ought to pass uPos into here.

	if( FindFormat() ) {

		return m_pTag->GetMaxValue(0, Type);
		}

	return CDispFormat::GetMax(Type);
	}

// Stepping

DWORD CDispFormatLinked::GetStep(UINT Type)
{
	if( FindFormat() ) {

		return m_pFormat->GetStep(Type);
		}

	return CDispFormat::GetStep(Type);
	}

BOOL CDispFormatLinked::SpeedUp(UINT uCount)
{
	if( FindFormat() ) {

		return m_pFormat->SpeedUp(uCount);
		}

	return CDispFormat::SpeedUp(uCount);
	}

BOOL CDispFormatLinked::SpeedUp(DWORD Data, UINT Type, DWORD &Step)
{
	if( FindFormat() ) {

		return m_pFormat->SpeedUp(Data, Type, Step);
		}

	return CDispFormat::SpeedUp(Data, Type, Step);
	}

// Implementation

BOOL CDispFormatLinked::FindFormat(void)
{
	if( !m_pFormat ) {
		
		m_pTag = CCommsSystem::m_pThis->m_pTags->m_pTags->GetItem(m_Tag);

		if( m_pTag ) {

			if( (m_pFormat = m_pTag->m_pFormat) ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	return TRUE;
	}

// End of File
