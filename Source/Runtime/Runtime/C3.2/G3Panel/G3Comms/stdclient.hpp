
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_STDCLIENT_HPP

#define	INCLUDE_STDCLIENT_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <ctype.h>

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStdEndpoint;
class CStdClient;
class CStdServer;

//////////////////////////////////////////////////////////////////////////
//
// Standard RFC Endpoint
//

class CStdEndpoint
{
	public:
		// Constructor
		CStdEndpoint(void);

	protected:
		// Data Members
		CString	    m_LogFile;
		BOOL	    m_fDebug;
		ISocket   * m_pCmdSock;

		// Transport
		BOOL Send(ISocket *pSock, PCTXT pText);
		BOOL Send(PCTXT pText);

		// Socket Management
		BOOL CheckCmdSocket(void);
		void CloseCmdSocket(void);
		void AbortCmdSocket(void);

		// File Transmission
		BOOL SendFile(ISocket *pSock, FILE *pFile);
		BOOL SendFile(ISocket *pSock, FILE *pFile, BOOL fCode);
		BOOL RecvFile(ISocket *pSock, FILE *pFile);

		// Logging
		void Log(PCTXT pForm, ...);
	};

/////////////////////////////////////////////////////////////////////////
//
// Standard RFC Client
//

class CStdClient : public CStdEndpoint
{
	public:
		// Constructor
		CStdClient(void);

		// Destructor
		~CStdClient(void);

	protected:
		// Data Members
		UINT	    m_uOption;
		char	    m_sReplyData[128];
		BOOL	    m_fReplyMore;
		UINT	    m_uReplyPtr;
		UINT	    m_uReplyCode;
		UINT	    m_uState;
		CString	    m_Reply;
		
		// TLS Support
		IMatrixClientContext * m_pTls;
		
		// Configuration
		virtual UINT  GetCmdPort(void);
		virtual DWORD GetServer(void);

		// Transport
		BOOL RecvReply(UINT uTimeout);
		BOOL RecvReply(void);
		void OnReply(char cData);

		// Socket Management
		BOOL OpenCmdSocket(BOOL fLarge, BOOL fSSL = FALSE);
		BOOL SwitchCmdSocket(void);
		BOOL CreateTlsContext(void);
	};

/////////////////////////////////////////////////////////////////////////
//
// Standard RFC Server
//

class CStdServer : public CStdEndpoint
{
	public:
		// Constructor
		CStdServer(void);

		// Destructor
		~CStdServer(void);

	protected:
		// Data Members
		char m_sCmdData[1024];
		UINT m_uCmdState;
		UINT m_uCmdPtr;

		// TLS Support
		IMatrixServerContext * m_pTls;
		
		// Configuration
		virtual UINT GetCmdPort(void);

		// Transport
		BOOL SendReply(UINT uCode, PCTXT pText);
		BOOL RecvCommand(void);
		void OnCommand(char cData);

		// Socket Management
		BOOL OpenCmdSocket(BOOL fLarge);
		BOOL WaitCmdSocket(void);
		BOOL SwitchCmdSocket(void);
		BOOL CreateTlsContext(void);
	};

// End of File

#endif
