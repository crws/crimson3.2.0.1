
#include "intern.hpp"

#include "queue.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Comms Device List
//

// Constructor

CCommsDeviceList::CCommsDeviceList(void)
{
	m_uCount   = 0;

	m_ppDevice = NULL;
	}

// Destructor

CCommsDeviceList::~CCommsDeviceList(void)
{
	while( m_uCount-- ) {

		delete m_ppDevice[m_uCount];
		}

	delete [] m_ppDevice;
	}

// Initialization

void CCommsDeviceList::Load(PCBYTE &pData, CCommsPort *pPort)
{
	ValidateLoad("CCommsDeviceList", pData);

	if( (m_uCount = GetWord(pData)) ) { 

		m_ppDevice = New CCommsDevice * [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CCommsDevice *pDevice = NULL;

			if( TRUE ) {

				pDevice = New CCommsDevice;
				}

			if( (m_ppDevice[n] = pDevice) ) {

				pDevice->Load(pData);

				pDevice->Bind(pPort);
				}
			}
		}
	}

// Operations

void CCommsDeviceList::SetError(UINT uError)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CCommsDevice *pDevice = m_ppDevice[n];

		if( pDevice ) {

			pDevice->SetError(errorHard);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Communications Device
//

// Constructor

CCommsDevice::CCommsDevice(void)
{
	StdSetRef();

	m_Number     = 0;

	m_pEnable    = NULL;

	m_Split      = 0;

	m_Delay      = 0;

	m_Transact   = 1;
	
	m_Preempt    = 0;

	m_Spanning   = 1;

	m_pConfig    = NULL;

	m_uConfig    = 0;

	m_pSysBlocks = New CCommsSysBlockList;

	m_pMapBlocks = New CCommsMapBlockList;

	m_pPort      = NULL;

	m_fQueue     = FALSE;

	m_pQueue[0]  = NULL;

	m_pQueue[1]  = NULL;

	m_pMutex     = NULL;

	m_pContext   = NULL;

	m_uError     = errorInit;

	m_uInvite    = 0;

	m_fEnable    = TRUE;

	m_fSuspend   = FALSE;

	m_fRespond0  = FALSE;

	m_fProblem0  = FALSE;

	m_fRespond1  = FALSE;

	m_fProblem1  = FALSE;

	m_uTime      = GetNow();
	}

// Destructor

CCommsDevice::~CCommsDevice(void)
{
	if( m_pMutex ) {

		m_pMutex->Release();
		}

	delete m_pEnable;

	delete m_pConfig;

	delete m_pSysBlocks;

	delete m_pMapBlocks;

	delete m_pQueue[0];

	delete m_pQueue[1];
	}

// Initialization

void CCommsDevice::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsDevice", pData);

	m_Number   = GetWord(pData);

	GetCoded(pData, m_pEnable);

	m_Split    = GetByte(pData);

	m_Delay    = GetWord(pData);

	m_Transact = GetByte(pData);

	m_Preempt  = GetByte(pData);

	m_Spanning = GetByte(pData);

	m_pConfig  = GetCopy(pData, m_uConfig);

	m_pSysBlocks->Load(pData);

	m_pMapBlocks->Load(pData);

	AllocQueues();

	CCommsSystem::m_pThis->m_pComms->RegDevice(m_Number, this);
	}

// Queue Access

CWriteQueue * CCommsDevice::GetQueue(UINT n) const
{
	return m_pQueue[1-n];
	}

// Attributes

BOOL CCommsDevice::HasError(void) const
{
	return m_uError == errorSoft || m_uError == errorHard;
	}

UINT CCommsDevice::GetError(void) const
{
	return m_uError;
	}

UINT CCommsDevice::GetStatus(void) const
{
	UINT uState = m_uError;

	if( m_pSysBlocks->HasError() ) {

		uState |= 0x0010;
		}

	if( m_pMapBlocks->HasError() ) {

		uState |= 0x0020;
		}

	if( m_fSuspend ) {

		uState |= 0x0040;
		}

	if( m_fRespond1 ) {

		uState |= 0x0100;
		}

	if( m_fProblem1 ) {

		uState |= 0x0200;
		}

	if( m_pQueue[0] && m_pQueue[0]->IsFilling() ) {

		uState |= 0x1000;
		}

	if( m_pQueue[1] && m_pQueue[1]->IsFilling() ) {

		uState |= 0x2000;
		}

	return uState;
	}

UINT CCommsDevice::GetInvite(void) const
{
	return m_uInvite;
	}

BOOL CCommsDevice::IsEnabled(void) const
{
	return m_fEnable && GetItemData(m_pEnable, C3INT(1));
	}

BOOL CCommsDevice::HasQueue(void) const
{
	return m_fQueue;
	}

BOOL CCommsDevice::IsQueueEmpty(void) const
{
	if( m_fQueue ) {
	
		if( m_pQueue[0] && !m_pQueue[0]->IsEmpty() ) {

			return FALSE;
			}
	
		if( m_pQueue[1] && !m_pQueue[1]->IsEmpty() ) {

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CCommsDevice::HasPreempt(void) const
{
	if( m_fQueue && m_Preempt ) {

		if( m_uError == errorNone ) {

			if( IsEnabled() ) {

				if( !IsQueueEmpty() ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Operations

void CCommsDevice::Bind(CCommsPort *pPort)
{
	m_pPort = pPort;
	}

BOOL CCommsDevice::Tickle(void)
{
	UINT uTime = GetNow();

	if( m_uTime != uTime ) {

		m_pSysBlocks->UpdateTimers(1);

		m_fRespond1 = m_fRespond0;

		m_fProblem1 = m_fProblem0;

		m_fRespond0 = FALSE;

		m_fProblem0 = FALSE;

		m_uTime     = uTime;

		return TRUE;
		}

	return FALSE;
	}

void CCommsDevice::Suspend(void)
{
	m_fRespond0  = FALSE;

	m_fProblem0  = FALSE;

	m_fRespond1  = FALSE;

	m_fProblem1  = FALSE;

	m_fSuspend   = TRUE;

	SetError(errorInit);
	}

void CCommsDevice::Resume(void)
{
	m_fSuspend = FALSE;
	}

void CCommsDevice::Enable(BOOL fEnable)
{
	m_fEnable = fEnable;
	}

void CCommsDevice::SetRespond(BOOL fRespond)
{
	if( fRespond ) {

		m_fRespond0 = TRUE;
		}
	}

void CCommsDevice::SetProblem(BOOL fProblem)
{
	if( fProblem ) {
		
		m_fProblem0 = TRUE;
		}
	}

void CCommsDevice::SetError(UINT uError)
{
	if( m_uError != uError ) {

		if( uError == errorNone ) {

			m_pMapBlocks->SetError(uError);
			}

		if( uError == errorInit || uError == errorSoft ) {

			if( m_fQueue ) {

				LockDevice();

				for( UINT n = 0; n < 2; n++ ) {

					if( m_pQueue[n] ) {

						m_pQueue[n]->FlushAll();
						}
					}

				m_pSysBlocks->SetError(uError);

				m_pMapBlocks->SetError(uError);

				FreeDevice();
				}
			else {
				m_pSysBlocks->SetError(uError);

				m_pMapBlocks->SetError(uError);
				}
			}

		m_uError = uError;
		}
	}

void CCommsDevice::SetInvite(UINT uInvite)
{
	m_uInvite = uInvite;
	}

void CCommsDevice::MarkInvalid(void)
{
	m_pSysBlocks->MarkInvalid();
	}

BOOL CCommsDevice::EnableQueue(void)
{
	if( m_Transact ) {

		m_fQueue = TRUE;

		return TRUE;
		}

	return FALSE;
	}

void CCommsDevice::LockDevice(void)
{
	m_pMutex->Wait(FOREVER);
	}

void CCommsDevice::FreeDevice(void)
{
	m_pMutex->Free();
	}

BOOL CCommsDevice::QueueWrite(CCommsSysBlock *pBlock, INT nPos, DWORD Data, DWORD dwMask)
{
	CWriteQueue *pQueue = m_pQueue[0];

	if( m_Split ) {

		if( CSystemItem::m_pThis->IsPriority() ) {

			pQueue = m_pQueue[1];
			}
		}

	pQueue->AddWrite(pBlock, nPos, Data, dwMask);

	return TRUE;
	}

BOOL CCommsDevice::QueueWrite(CCommsSysBlock *pBlock, INT nPos, DWORD Data)
{
	return QueueWrite(pBlock, nPos, Data, NOTHING);
	}

void CCommsDevice::FlushQueue(CCommsSysBlock *pBlock, INT nPos)
{
	for( UINT n = 0; n < 2; n++ ) {

		if( m_pQueue[n] ) {

			m_pQueue[n]->FlushEntry(pBlock, nPos);
			}
		}
	}

void CCommsDevice::FlushQueue(CCommsSysBlock *pBlock)
{
	for( UINT n = 0; n < 2; n++ ) {

		if( m_pQueue[n] ) {

			m_pQueue[n]->FlushBlock(pBlock);
			}
		}
	}

void CCommsDevice::EmptyQueue(void)
{
	for( UINT n = 0; n < 2; n++ ) {

		if( m_pQueue[n] ) {

			m_pQueue[n]->EmptyAll();
			}
		}

	m_pSysBlocks->ClearDirty();
	}

UINT CCommsDevice::Control(UINT uCode, PCTXT pValue)
{
	if( m_pPort ) {

		return m_pPort->Control(m_pContext, uCode, pValue);
		}

	return 0;
	}

void CCommsDevice::CommsDebug(IDiagOutput *pOut, BOOL &fHead)
{
	if( m_pSysBlocks->m_uCount || m_pMapBlocks->m_uCount ) {

		if( fHead ) {

			ShowHeader(pOut);

			fHead = FALSE;
			}

		ShowStatus(pOut);
		}
	}

// IUnknown

HRESULT CCommsDevice::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ICommsDevice);

	StdQueryInterface(ICommsDevice);

	return E_NOINTERFACE;
	}

ULONG CCommsDevice::AddRef(void)
{
	StdAddRef();
	}

ULONG CCommsDevice::Release(void)
{
	StdRelease();
	}

// ICommsDevice

PCBYTE CCommsDevice::GetConfig(void)
{
	return m_pConfig;
	}

UINT CCommsDevice::GetConfigSize(void)
{
	return m_uConfig;
	}

void CCommsDevice::SetContext(PVOID pData)
{
	m_pContext = pData;
	}

PVOID CCommsDevice::GetContext(void)
{
	return m_pContext;
	}

// Debug Helpers

BOOL CCommsDevice::ShowHeader(IDiagOutput *pOut)
{
	pOut->AddTable(5);

	pOut->SetColumn(0, "Num",  "%u");
	pOut->SetColumn(1, "Stat", "%4.4X");
	pOut->SetColumn(2, "Tran", "%u");
	pOut->SetColumn(3, "Q0",   "%+s");
	pOut->SetColumn(4, "Q1",   "%+s");

	pOut->AddHead();

	pOut->AddRule('-');

	return TRUE;
	}

BOOL CCommsDevice::ShowStatus(IDiagOutput *pOut)
{
	pOut->AddRow();

	pOut->SetData(0, m_Number);

	pOut->SetData(1, GetStatus());

	pOut->SetData(2, m_fQueue);

	pOut->SetData(3, m_pQueue[0] ? PCTXT(CPrintf("%u", m_pQueue[0]->GetCount())) : "-");

	pOut->SetData(4, m_pQueue[1] ? PCTXT(CPrintf("%u", m_pQueue[1]->GetCount())) : "-");

	pOut->EndRow();

	return TRUE;
	}

// Implementation

BOOL CCommsDevice::AllocQueues(void)
{
	if( m_Transact ) {

		if( m_Split ) {

			m_pQueue[0] = New CWriteQueue(this, 256, 32);

			m_pQueue[1] = New CWriteQueue(this, 256,  8);
			}
		else
			m_pQueue[0] = New CWriteQueue(this, 512, 16);

		m_pMutex = Create_Mutex();

		m_pSysBlocks->Bind(this);

		m_pMapBlocks->Bind(this);

		return TRUE;
		}

	return FALSE;
	}

// End of File
