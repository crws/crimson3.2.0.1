//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "Intern.hpp"

#include "TdsResponse.hpp"


//////////////////////////////////////////////////////////////////////////
//
// TDS Response Packet
//


// Constructors

CTdsResponse::CTdsResponse(void) : CTdsPacket(typeResponse)
{
	m_wStatus = 0;

	m_szErrorMessages[0] = New CHAR [ NUM_ERR_MESSAGES * MAX_MESSAGE_LEN ];

	UINT uIndex;
	for( uIndex = 1; uIndex < NUM_ERR_MESSAGES; uIndex++ )
		m_szErrorMessages[uIndex] = m_szErrorMessages[0] + uIndex * MAX_MESSAGE_LEN;

	m_fParsedAnErrorFromServer = false;
	m_uNumErrMessages = 0;
	m_szErrorMessages[0][0] = '\0';

	m_szDatabase = New CHAR [ MAX_PROP_LEN ];
	m_szDatabase[0] = '\0';

	m_szLanguage = New CHAR [ MAX_PROP_LEN ];
	m_szLanguage[0] = '\0';

	m_szCharacterSet = New CHAR [ MAX_PROP_LEN ];
	m_szCharacterSet[0] = '\0';

	m_szPacketSize = New CHAR [ MAX_PROP_LEN ];
	m_szPacketSize[0] = '\0';
	m_uPacketSize = 0;

	m_szSQLCollation = New CHAR [ MAX_PROP_LEN ];
	m_szSQLCollation[0] = '\0';

	m_szInfoMessages[0] = New CHAR [ NUM_INFO_MESSAGES * MAX_MESSAGE_LEN ];

	for( uIndex = 1; uIndex < NUM_INFO_MESSAGES; uIndex++ )
		m_szInfoMessages[uIndex] = m_szInfoMessages[0] + uIndex * MAX_MESSAGE_LEN;

	m_uNumInfoMessages = 0;
	m_szInfoMessages[0][0] = '\0';
	}


CTdsResponse::~CTdsResponse(void)
{
	delete[] m_szErrorMessages[0];

	delete[] m_szInfoMessages[0];

	delete[] m_szDatabase;

	delete[] m_szLanguage;

	delete[] m_szCharacterSet;

	delete[] m_szPacketSize;

	delete[] m_szSQLCollation;
	}


// Attributes

bool CTdsResponse::HaveErrorFromServer() const
{
	return m_fParsedAnErrorFromServer;
	}


USHORT  CTdsResponse::GetNumErrMessages() const
{
	return m_uNumErrMessages;
	}


PCSTR CTdsResponse::GetErrorMessage(USHORT &pIndex) const
{
	if (pIndex < m_uNumErrMessages)
		return m_szErrorMessages[pIndex];
	else
		return NULL;
	}


LONG CTdsResponse::GetErrorCode(USHORT &pIndex) const
{
	if (pIndex < m_uNumErrMessages)
		return m_lErrorCodes[pIndex];
	else
		return -1;
	}


PCSTR	CTdsResponse::GetDatabase() const
{
	return m_szDatabase;
	}


PCSTR	CTdsResponse::GetLanguage() const
{
	return m_szLanguage;
	}


PCSTR	CTdsResponse::GetCharacterSet() const
{
	return m_szCharacterSet;
	}


PCSTR	CTdsResponse::GetPacketSize() const
{
	return m_szPacketSize;
	}


USHORT  CTdsResponse::GetPacketSizeValue() const
{
	return m_uPacketSize;
	}


PCSTR	CTdsResponse::GetSQLCollation() const
{
	return m_szSQLCollation;
	}


USHORT  CTdsResponse::GetNumInfoMessages() const
{
	return m_uNumInfoMessages;
	}


PCSTR   CTdsResponse::GetInfoMessage(USHORT &pIndex) const
{
	if (pIndex < m_uNumInfoMessages)
		return m_szInfoMessages[pIndex];
	else
		return NULL;
	}


// Operations

int CTdsResponse::Parse(PCBYTE pData, UINT uSize)
{
	// Error message flag used for errors returned from server.
	m_fParsedAnErrorFromServer = false;

	int n = CTdsPacket::Parse(pData, uSize);

	if( n > 0 ) {

		// If not a response packet, then indicate failure.
		if (GetType() != typeResponse)
		{
			SetError("Packet is not a response packet type.");
			return -1;
			}

		// Parse each token stream of the response packet until a DONE is encountered.
		UINT uPos = 8;
		BYTE bNextTokenStream;
		do {
			// Read next token type.
			bNextTokenStream = GetByte(uPos);

			// Handle next stream.
			switch (bNextTokenStream)
			{
				case TDS_DONE_TOKEN:
					m_wStatus = GetUShortReverse(uPos);
					break;

				case TDS_ENVCHANGE_TOKEN:
					if (! ReadEnvchangeTokenStream(uPos))
						return -1;
					break;

				case TDS_ERROR_TOKEN:
					if (! ReadErrorTokenStream(uPos))
						return -1;
					m_fParsedAnErrorFromServer = true;
					break;

				case TDS_INFO_TOKEN:
					if (! ReadInfoTokenStream(uPos))
						return -1;
					break;

				case TDS_LOGIN_ACK_TOKEN:
					if (! ReadLoginAckTokenStream(uPos))
						return -1;
					break;

				default:
				{
					CHAR Message[128];
					sprintf (Message, "Unknown token stream type encountered: %x", bNextTokenStream);
					SetError(Message);
					return -1;
					}
				}

			} while (bNextTokenStream != TDS_DONE_TOKEN);

		if( m_wStatus & 0x02 ) {

			SetError("Server error bit set");
			}

		return +1;
		}

	return n;
	}

bool CTdsResponse::ReadEnvchangeTokenStream(UINT &uPos)
{
	USHORT uLength = GetUShortReverse(uPos);

	BYTE bType = GetByte(uPos);

	CHAR	szOldValue[256];
	switch (bType)
	{
		case ENVCHANGE_DATABASE:
			ReadNewAndOldValues(uPos, m_szDatabase, szOldValue);
			break;

		case ENVCHANGE_LANGUAGE:
			ReadNewAndOldValues(uPos, m_szLanguage, szOldValue);
			break;

		case ENVCHANGE_CHARACTERSET:
			ReadNewAndOldValues(uPos, m_szCharacterSet, szOldValue);
			break;

		case ENVCHANGE_PACKETSIZE:
			ReadNewAndOldValues(uPos, m_szPacketSize, szOldValue);
			sscanf(m_szPacketSize, "%hu", &m_uPacketSize);
			break;

		default:
			uPos += uLength - 1;
			break;
		}

	return true;

	}


bool CTdsResponse::ReadErrorTokenStream(UINT &uPos)
{
	if (m_uNumErrMessages >= NUM_ERR_MESSAGES)
	{
		SetError("Too many error messages in this packet");
		return false;
		}

	USHORT uLength = GetUShortReverse(uPos);

	m_lErrorCodes[m_uNumErrMessages] = GetULongReverse(uPos);

	BYTE bState = GetByte(uPos);

	BYTE bClass = GetByte(uPos);

	USHORT uMsgLen = GetUShortReverse(uPos);
	GetReverseCharacterData(m_szErrorMessages[m_uNumErrMessages++], uMsgLen + 1, uPos);

	CHAR szTemp[256];
	BYTE bServerNameLen = GetByte(uPos);
	GetReverseCharacterData(szTemp, bServerNameLen + 1, uPos);

	BYTE bProcNameLen = GetByte(uPos);
	GetReverseCharacterData(szTemp, bProcNameLen + 1, uPos);
	
	ULONG uLineNumber = GetULongReverse(uPos);

	return true;
	}


bool CTdsResponse::ReadInfoTokenStream(UINT &uPos)
{
	if (m_uNumInfoMessages >= NUM_INFO_MESSAGES)
	{
		SetError("Too many informational messages in this packet");
		return false;
		}

	USHORT uLength = GetUShortReverse(uPos);

	LONG lNumber = GetULongReverse(uPos);

	BYTE bState = GetByte(uPos);

	BYTE bClass = GetByte(uPos);

	USHORT uMsgLen = GetUShortReverse(uPos);
	GetReverseCharacterData(m_szInfoMessages[m_uNumInfoMessages], uMsgLen + 1, uPos);
	m_uNumInfoMessages++;

	CHAR szTemp[256];
	BYTE bServerNameLen = GetByte(uPos);
	GetReverseCharacterData(szTemp, bServerNameLen + 1, uPos);

	BYTE bProcNameLen = GetByte(uPos);
	GetReverseCharacterData(szTemp, bProcNameLen + 1, uPos);

	ULONG uLineNumber = GetULongReverse(uPos);

	return true;
	}


bool CTdsResponse::ReadLoginAckTokenStream(UINT &uPos)
{
	USHORT uLength = GetUShortReverse(uPos);

	BYTE bInterface = GetByte(uPos);

	ULONG uTDSVersion = GetULongReverse(uPos);

	CHAR szTemp[256];
	BYTE bProgNameLen = GetByte(uPos);
	GetReverseCharacterData(szTemp, bProgNameLen + 1, uPos);

	BYTE bMajorVersion = GetByte(uPos);
	BYTE bMinorVersion = GetByte(uPos);
	BYTE bBuildNumHi = GetByte(uPos);
	BYTE bBuildNumLo = GetByte(uPos);

	return true;
	}


// Helper Methods

void CTdsResponse::ReadNewAndOldValues(UINT &uPos, CHAR* szNewValue, CHAR* szOldValue)
{
	BYTE bNewValueLen = GetByte(uPos);
	GetReverseCharacterData(szNewValue, bNewValueLen + 1, uPos);

	BYTE bOldValueLen = GetByte(uPos);
	GetReverseCharacterData(szOldValue, bOldValueLen + 1, uPos);
	}


void CTdsResponse::SetError(CHAR* sMessage)
{
	strcpy(m_szErrorMessages[0], sMessage);

	m_uNumErrMessages = 1;

	m_fParsedAnErrorFromServer = true;
	}

// End of File
