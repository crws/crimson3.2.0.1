
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// System Block List
//

// Constructor

CCommsMapRegList::CCommsMapRegList(void)
{
	m_uCount = 0;

	m_ppReg  = NULL;
	}

// Destructor

CCommsMapRegList::~CCommsMapRegList(void)
{
	while( m_uCount-- ) {

		delete m_ppReg[m_uCount];
		}

	delete [] m_ppReg;
	}

// Initialization

void CCommsMapRegList::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsMapRegList", pData);

	if( (m_uCount = GetWord(pData)) ) { 

		m_ppReg = New CCommsMapReg * [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CCommsMapReg *pReg = NULL;

			if( GetByte(pData) ) {

				pReg = New CCommsMapReg;
				}

			if( (m_ppReg[n] = pReg) ) {

				pReg->Load(pData);
				}
			}
		}
	}

// Operations

BOOL CCommsMapRegList::SetScan(UINT uCode)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppReg[n] ) {

			m_ppReg[n]->SetScan(uCode);
			}
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Register Mapping
//

// Constructor

CCommsMapReg::CCommsMapReg(void)
{
	m_uRefs = 0;

	m_pRefs = NULL;
	}

// Destructor

CCommsMapReg::~CCommsMapReg(void)
{
	delete [] m_pRefs;
	}

// Attributes

BOOL CCommsMapReg::IsMapped(void) const
{
	for( UINT n = 0; n < m_uRefs; n++ ) {

		if( m_pRefs[n] ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCommsMapReg::IsAvail(void) const
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	if( m_uRefs > 1 ) {

		UINT m = 0;

		for( UINT n = 0; n < m_uRefs; n++ ) {

			if( m_pRefs[n] ) {

				if( !pData->IsAvail(m_pRefs[n]) ) {

					return FALSE;
					}

				m++;
				}
			}

		return m ? TRUE : FALSE;
		}

	if( m_pRefs[0] ) {

		return pData->IsAvail(m_pRefs[0]);
		}

	return FALSE;
	}

DWORD CCommsMapReg::GetValue(UINT Flags) const
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	if( m_uRefs > 1 ) {

		DWORD Data = 0;

		DWORD Mask = 1;

		for( UINT n = 0; n < m_uRefs; n++ ) {

			if( m_pRefs[n] ) {

				if( pData->GetData(m_pRefs[n], typeVoid, Flags) ) {

					Data |= Mask;
					}
				}

			Mask <<= 1;
			}

		return Data;
		}

	if( m_pRefs[0] ) {

		return pData->GetData(m_pRefs[0], typeVoid, Flags);
		}

	return 0;
	}

DWORD CCommsMapReg::GetMetaData(WORD ID) const
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	if( m_pRefs[0] ) {

		return pData->GetProp(m_pRefs[0], ID, typeVoid);
		}

	return 0;
	}

// Operations

BOOL CCommsMapReg::SetScan(UINT uCode)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	for( UINT n = 0; n < m_uRefs; n++ ) {

		if( m_pRefs[n] ) {

			pData->SetScan(m_pRefs[n], uCode);
			}
		}

	return TRUE;
	}

BOOL CCommsMapReg::SetValue(DWORD Data, UINT Flags)
{
	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	if( m_uRefs > 1 ) {

		DWORD Mask = 1;

		for( UINT n = 0; n < m_uRefs; n++ ) {

			if( m_pRefs[n] ) {

				if( Data & Mask ) {

					pData->SetData(m_pRefs[n], typeInteger, Flags, 1);
					}
				else
					pData->SetData(m_pRefs[n], typeInteger, Flags, 0);
				}

			Mask <<= 1;
			}

		return TRUE;
		}

	if( m_pRefs[0] ) {

		return pData->SetData(m_pRefs[0], typeVoid, Flags, Data);
		}

	return TRUE;
	}

// Initialization

void CCommsMapReg::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsMapReg", pData);

	m_uRefs = GetByte(pData);

	m_pRefs = New DWORD [ m_uRefs ];

	for( UINT n = 0; n < m_uRefs; n++ ) {

		m_pRefs[n] = GetLong(pData);
		}
	}

// End of File
