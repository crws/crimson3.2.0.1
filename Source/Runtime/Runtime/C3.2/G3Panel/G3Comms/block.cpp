
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Serialization
//

#define	FastLock()	m_pLock->Wait(FOREVER);

#define	FastFree()	m_pLock->Free();

//////////////////////////////////////////////////////////////////////////
//
// System Block List
//

// Constructor

CCommsSysBlockList::CCommsSysBlockList(void)
{
	m_uCount  = 0;

	m_ppBlock = NULL;

	m_pLock   = Create_Rutex();
	}

// Destructor

CCommsSysBlockList::~CCommsSysBlockList(void)
{
	while( m_uCount-- ) {

		delete m_ppBlock[m_uCount];
		}

	delete [] m_ppBlock;

	AfxRelease(m_pLock);
	}

// Initialization

void CCommsSysBlockList::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsSysBlockList", pData);

	if( (m_uCount = GetWord(pData)) ) { 

		m_ppBlock = New CCommsSysBlock * [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CCommsSysBlock *pBlock = NULL;

			if( GetByte(pData) ) {

				pBlock = New CCommsSysBlock(m_pLock);
				}

			if( (m_ppBlock[n] = pBlock) ) {

				pBlock->Load(pData);
				}
			}
		}
	}

// Attributes

BOOL CCommsSysBlockList::HasError(void) const
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppBlock[n] ) {

			UINT uError = m_ppBlock[n]->GetError();
			
			if( uError == errorNone || uError == errorInit ) {

				continue;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Operations

void CCommsSysBlockList::Bind(CCommsDevice *pDevice)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppBlock[n] ) {

			m_ppBlock[n]->Bind(pDevice);
			}
		}
	}

void CCommsSysBlockList::SetError(UINT uError)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppBlock[n] ) {

			m_ppBlock[n]->SetError(uError);
			}
		}
	}

void CCommsSysBlockList::UpdateTimers(UINT uTime)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppBlock[n] ) {

			m_ppBlock[n]->UpdateTimers(uTime);
			}
		}
	}

void CCommsSysBlockList::MarkInvalid(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppBlock[n] ) {

			m_ppBlock[n]->MarkInvalid();
			}
		}
	}

void CCommsSysBlockList::ClearDirty(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppBlock[n] ) {

			m_ppBlock[n]->ClearDirty();
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// System Block
//

// Constructor

CCommsSysBlock::CCommsSysBlock(IMutex *pLock)
{
	m_pLock   = pLock;
	m_Number  = 0;
	m_Named   = 0;
	m_Space   = 0;
	m_Factor  = 1;
	m_Align   = 0;
	m_Index   = 0;
	m_Base    = 0;
	m_Size    = 0;
	m_pList   = NULL;
	m_pDevice = NULL;
	m_pData   = NULL;
	m_pInfo   = NULL;
	m_uError  = errorInit;
	m_uScan   = 0;
	m_uUser   = 0;
	m_uOnce   = 0;
	m_uDirty  = 0;
	}

// Destructor

CCommsSysBlock::~CCommsSysBlock(void)
{
	delete m_pList;

	delete m_pData;

	delete m_pInfo;
	}

// Initialization

void CCommsSysBlock::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsSysBlock", pData);

	m_Number = GetWord(pData);
	m_Named  = GetByte(pData);
	m_Space  = GetWord(pData);
	m_Factor = GetByte(pData);
	m_Index  = GetWord(pData);
	m_Base   = GetWord(pData);
	m_Size   = GetWord(pData);

	if( IsNamed() ) {

		m_pList = New WORD [ m_Size ];

		for( INT n = 0; n < m_Size; n++ ) {

			m_pList[n] = GetWord(pData);
			}
		}

	AllocData();

	CCommsSystem::m_pThis->m_pComms->RegBlock(m_Number, this);
	}

// Adress Access

CAddress CCommsSysBlock::GetAddress(INT nPos) const
{
	DWORD Addr = 0;

	if( nPos < m_Size ) {

		if( m_Named ) {
	
			WORD Index = m_pList[nPos];

			Addr       = MAKELONG(Index, m_Space);
			}
		else {
			WORD Index = m_Index + nPos * m_Factor;

			Addr       = MAKELONG(Index, m_Space);
			}
		}

	return (CAddress &) Addr;
	}

BOOL CCommsSysBlock::HasAddress(DWORD Addr, INT &nPos, CCommsDevice * pDev) const
{
	if( m_pDevice == pDev ) {

		if( m_Named ) {

			for( INT n = 0; n < m_Size; n++ ) {

				if( m_pList[n] == Addr ) {

					nPos = n;

					return TRUE;
					}
				}

			return FALSE;
			}
		else {
			UINT Space = HIWORD(Addr);

			INT  Index = LOWORD(Addr);

			if( Space == m_Space ) {
			
				if( Index >= m_Index ) {

					if( Index < m_Index + m_Size * m_Factor ) {

						nPos = (Index - m_Index) / m_Factor;

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

// Attributes

BOOL CCommsSysBlock::IsNamed(void) const
{
	return m_Named;
	}

BOOL CCommsSysBlock::IsActive(void) const
{
	return m_uScan || m_uUser || m_uOnce || m_uDirty;
	}

UINT CCommsSysBlock::GetError(void) const
{
	return m_uError;
	}

DWORD CCommsSysBlock::GetData(INT nPos) const
{
	return m_pData[nPos];
	}

BOOL CCommsSysBlock::IsAvail(INT nPos) const
{
	if( nPos >= 0 && nPos < m_Size ) {

		CInfo const &Info = m_pInfo[nPos];

		return !Info.m_fBroke && Info.m_fAvail;
		}

	return FALSE;
	}

BOOL CCommsSysBlock::ShouldRead(INT nPos) const
{
	CInfo const &Info = m_pInfo[nPos];

	if( !Info.m_fBroke && !Info.m_fDirty && !Info.m_fQueue ) {
		
		return Info.m_fScan || Info.m_fUser || Info.m_fOnce;
		}

	return FALSE;
	}

BOOL CCommsSysBlock::CouldRead(INT nPos) const
{
	CInfo const &Info = m_pInfo[nPos];

	return !Info.m_fBroke;
	}

BOOL CCommsSysBlock::ShouldWrite(INT nPos) const
{
	CInfo const &Info = m_pInfo[nPos];

	return !Info.m_fBroke && Info.m_fDirty;
	}

BOOL CCommsSysBlock::IsCached(INT nPos) const
{
	CInfo const &Info = m_pInfo[nPos];

	return !Info.m_fBroke && Info.m_fCache;
	}

// Operations

void CCommsSysBlock::Bind(CCommsDevice *pDevice)
{
	m_pDevice = pDevice;
	}

void CCommsSysBlock::SetError(UINT uError)
{
	if( m_uError != errorHard ) {

		if( m_uError != uError ) {

			if( m_uError == errorInit || m_uError == errorNone ) {

				if( uError == errorNone ) {

					m_uError = uError;

					return;
					}

				if( m_pDevice ) {

					if( m_pDevice->HasQueue() ) {

						m_pDevice->LockDevice();

						m_pDevice->FlushQueue(this);

						MarkInvalid();

						m_uError = uError;

						m_pDevice->FreeDevice();

						return;
						}
					}

				MarkInvalid();
				}

			m_uError = uError;
			}
		}
	}

void CCommsSysBlock::MarkInvalid(void)
{
	for( INT nPos = 0; nPos < m_Size; nPos++ ) {

		CInfo &Info = m_pInfo[nPos];

		FastLock();

		Info.m_fAvail = FALSE;

		Info.m_bTimer = 0;

		KillOnce(Info);

		FastFree();
		}
	}

void CCommsSysBlock::ClearDirty(void)
{
	for( INT nPos = 0; nPos < m_Size; nPos++ ) {

		CInfo &Info = m_pInfo[nPos];

		FastLock();

		KillDirty(Info);

		FastFree();
		}
	}

void CCommsSysBlock::UpdateTimers(UINT uTime)
{
	for( INT nPos = 0; nPos < m_Size; nPos++ ) {

		CInfo &Info = m_pInfo[nPos];

		if( !Info.m_fDirty && !Info.m_fQueue ) {

			if( Info.m_bTimer ) {

				if( Info.m_bTimer <= uTime ) {

					Info.m_fAvail = FALSE;

					Info.m_bTimer = 0;

					continue;
					}

				Info.m_bTimer -= uTime;
				}
			}
		}
	}

BOOL CCommsSysBlock::SetScan(INT nPos, UINT Code)
{
	if( nPos >= 0 && nPos < m_Size ) {

		if( Code == scanFalse ) {

			CInfo &Info = m_pInfo[nPos];

			FastLock();

			if( Info.m_uScan < maxScan ) {

				if( !--Info.m_uScan ) {

					Info.m_fScan = FALSE;

					m_uScan--;
					}
				}

			FastFree();
			}

		if( Code == scanTrue ) {

			CInfo &Info = m_pInfo[nPos];

			FastLock();

			if( Info.m_uScan < maxScan ) {

				if( !Info.m_uScan++ ) {

					Info.m_fScan = TRUE;

					m_uScan++;
					}
				}

			FastFree();
			}

		if( Code == scanUser ) {

			CInfo &Info = m_pInfo[nPos];

			FastLock();

			if( !Info.m_fScan && !Info.m_fUser ) {

				Info.m_bTimer = 0;

				Info.m_fAvail = FALSE;

				Info.m_fUser  = TRUE;

				m_uUser++;
				}

			FastFree();
			}

		if( Code == scanOnce ) {

			CInfo &Info = m_pInfo[nPos];

			FastLock();

			if( !Info.m_fScan && !Info.m_fOnce ) {

				Info.m_fOnce = TRUE;

				m_uOnce++;
				}

			FastFree();
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCommsSysBlock::SetCommsData(INT nPos)
{
	CInfo &Info = m_pInfo[nPos];

	FastLock();

	if( !Info.m_fDirty && !Info.m_fQueue ) {

		Info.m_fAvail = TRUE;

		Info.m_bTimer = 4;

		KillUser(Info);

		KillOnce(Info);
		}

	FastFree();

	return TRUE;
	}

BOOL CCommsSysBlock::SetCommsData(INT nPos, DWORD Data)
{
	CInfo &Info = m_pInfo[nPos];

	FastLock();

	if( !Info.m_fDirty && !Info.m_fQueue ) {

		m_pData[nPos] = Data;

		Info.m_fAvail = TRUE;

		Info.m_bTimer = Info.m_fUser ? 0 : 4;

		KillUser(Info);

		KillOnce(Info);
		}

	FastFree();

	return TRUE;
	}

DWORD CCommsSysBlock::GetWriteData(INT nPos)
{
	CInfo &Info   = m_pInfo[nPos];

	FastLock();

	DWORD Data    = m_pData[nPos];

	Info.m_fWrite = TRUE;

	FastFree();

	return Data;
	}

PDWORD CCommsSysBlock::GetWriteData(INT nPos, INT nCount)
{
	PDWORD pData = New DWORD [ nCount ];

	for( INT n = 0; n < nCount; n++ ) {

		CInfo &Info   = m_pInfo[nPos + n];

		FastLock();

		pData[n]      = m_pData[nPos + n];

		Info.m_fWrite = TRUE;

		FastFree();
		}

	return pData;
	}

BOOL CCommsSysBlock::SetWriteData(INT nPos, UINT Flags, DWORD Data, DWORD dwMask)
{
	CInfo &Info = m_pInfo[nPos];

	if( Flags & setFlush ) {
		
		FastLock();

		if( !--Info.m_uQueue ) {

			Info.m_fQueue = FALSE;
			}

		FastFree();
		}
	else {
		if( Info.m_fAvail ) {

			if( !(Flags & setForce) ) {

				if( !(m_pData[nPos] & dwMask) == !Data ) {
						
					Info.m_fCache = TRUE;

					return TRUE;
					}
				}
			}

		if( m_pDevice ) {

			if( m_pDevice->HasQueue() ) {

				m_pDevice->LockDevice();

				if( m_uError == errorInit || m_uError == errorNone ) {

					if( !(Flags & setDirect) ) {
					
						FastLock();

						m_pData[nPos] = Data ? (m_pData[nPos] | dwMask) : (m_pData[nPos] & ~dwMask);

						Info.m_fWrite = FALSE;

						Info.m_fQueue = TRUE;

						Info.m_fCache = TRUE;

						Info.m_uQueue++;

						KillDirty(Info);

						FastFree();

						m_pDevice->QueueWrite(this, nPos, Data, dwMask);

						m_pDevice->FreeDevice();

						return TRUE;
						}

					m_pDevice->FlushQueue(this, nPos);
					}

				m_pDevice->FreeDevice();
				}
			}
		}

	FastLock();

	m_pData[nPos] = Data ? (m_pData[nPos] | dwMask) : (m_pData[nPos] & ~dwMask);

	Info.m_fWrite = FALSE;

	Info.m_fCache = TRUE;

	SetDirty(Info);

	FastFree();

	return TRUE;
	}

BOOL CCommsSysBlock::SetWriteData(INT nPos, UINT Flags, DWORD Data)
{
	CInfo &Info = m_pInfo[nPos];

	if( Flags & setFlush ) {
		
		FastLock();

		if( !--Info.m_uQueue ) {

			Info.m_fQueue = FALSE;
			}

		FastFree();
		}
	else {
		if( Info.m_fAvail ) {

			if( !(Flags & setForce) ) {

				if( m_pData[nPos] == Data ) {
						
					Info.m_fCache = TRUE;

					return TRUE;
					}
				}
			}

		if( m_pDevice ) {

			if( m_pDevice->HasQueue() ) {

				m_pDevice->LockDevice();

				if( m_uError == errorInit || m_uError == errorNone ) {

					if( !(Flags & setDirect) ) {
					
						FastLock();

						m_pData[nPos] = Data;

						Info.m_fWrite = FALSE;

						Info.m_fQueue = TRUE;

						Info.m_fCache = TRUE;

						Info.m_uQueue++;

						KillDirty(Info);

						FastFree();

						m_pDevice->QueueWrite(this, nPos, Data);

						m_pDevice->FreeDevice();

						return TRUE;
						}

					m_pDevice->FlushQueue(this, nPos);
					}

				m_pDevice->FreeDevice();
				}
			}
		}

	FastLock();

	m_pData[nPos] = Data;

	Info.m_fWrite = FALSE;

	Info.m_fCache = TRUE;

	SetDirty(Info);

	FastFree();

	return TRUE;
	}

void CCommsSysBlock::SetWriteDone(INT nPos)
{
	CInfo &Info = m_pInfo[nPos];

	FastLock();

	if( Info.m_fWrite ) {

		KillDirty(Info);
		}

	FastFree();
	}

void CCommsSysBlock::SetQueueDone(INT nPos)
{
	CInfo &Info = m_pInfo[nPos];

	FastLock();

	if( !--Info.m_uQueue ) {

		Info.m_fQueue = FALSE;
		}

	FastFree();
	}

void CCommsSysBlock::CommsDebug(IDiagOutput *pOut, BOOL &fHead)
{
	if( m_pDevice ) {

		if( fHead ) {

			ShowHeader(pOut);

			fHead = FALSE;
			}

		ShowStatus(pOut);
		}
	}

// Debug Helpers

BOOL CCommsSysBlock::ShowHeader(IDiagOutput *pOut)
{
	pOut->AddTable(10);

	pOut->SetColumn(0, "Num",     "%u");
	pOut->SetColumn(1, "Dev",     "%u");
	pOut->SetColumn(2, "Address", "%8.8X");
	pOut->SetColumn(3, "Size",    "%u");
	pOut->SetColumn(4, "State",   "%u");
	pOut->SetColumn(5, "Scan",    "%u");
	pOut->SetColumn(6, "User",    "%u");
	pOut->SetColumn(7, "Once",    "%u");
	pOut->SetColumn(8, "Dirty",   "%u");
	pOut->SetColumn(9, "Queue",   "%u");

	pOut->AddHead();

	pOut->AddRule('-');

	return TRUE;
	}

BOOL CCommsSysBlock::ShowStatus(IDiagOutput *pOut)
{
	CAddress Addr   = GetAddress(0);

	UINT     uQueue = 0;

	for( INT nPos = 0; nPos < m_Size; nPos++ ) {

		CInfo &Info = m_pInfo[nPos];

		if( Info.m_fQueue ) {

			uQueue++;
			}
		}

	pOut->AddRow();

	pOut->SetData(0, m_Number);
	pOut->SetData(1, m_pDevice->m_Number);
	pOut->SetData(2, (DWORD &) Addr);
	pOut->SetData(3, m_Size);
	pOut->SetData(4, m_uError);
	pOut->SetData(5, m_uScan);
	pOut->SetData(6, m_uUser);
	pOut->SetData(7, m_uOnce);
	pOut->SetData(8, m_uDirty);
	pOut->SetData(9, uQueue);

	pOut->EndRow();

	return TRUE;
	}

// Implementation

void CCommsSysBlock::AllocData(void)
{
	m_pData = New DWORD [ m_Size ];

	m_pInfo = New CInfo [ m_Size ];

	memset(m_pData, 0x00, sizeof(DWORD) * m_Size);

	memset(m_pInfo, 0x00, sizeof(CInfo) * m_Size);
	}

void CCommsSysBlock::SetDirty(CInfo &Info)
{
	if( !Info.m_fDirty ) {

		Info.m_fDirty = TRUE;

		m_uDirty++;
		}
	}

void CCommsSysBlock::KillDirty(CInfo &Info)
{
	if( Info.m_fDirty ) {

		Info.m_fDirty = FALSE;

		m_uDirty--;
		}
	}

void CCommsSysBlock::KillUser(CInfo &Info)
{
	if( Info.m_fUser ) {

		Info.m_fUser = FALSE;

		m_uUser--;
		}
	}

void CCommsSysBlock::KillOnce(CInfo &Info)
{
	if( Info.m_fOnce ) {

		Info.m_fOnce = FALSE;

		m_uOnce--;
		}
	}

// End of File
