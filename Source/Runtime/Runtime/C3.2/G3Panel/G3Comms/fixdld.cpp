
#include "intern.hpp"

#include "proxy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Communications Port
//

// Embedded DLD Support

BOOL CCommsPort::LoadDLD(void)
{
	if( m_pImage && m_uImage ) {

		AfxGetAutoObject(pManager, "modman", 0, IModuleManager);

		if( pManager ) {

			CPrintf Name("Driver%4.4X", m_DriverID);

			AfxNewObject(Name, IDriver, m_pDriver);

			if( !m_pDriver ) {

				if( !m_hModule ) {

					pManager->LoadClientModule(m_hModule, Name, m_pImage, m_uImage);
				}

				if( !m_hModule ) {

					// Debug Hook for Win32.

					#if defined(_DEBUG) && defined(AEON_PLAT_WIN32)

					CPrintf File("!..\\..\\..\\DLDs\\Win32\\Debug\\%4.4X", m_DriverID);

					pManager->LoadClientModule(m_hModule, File);

					#endif

					// Debug Hook for Linux.

					#if defined(_DEBUG) && defined(AEON_PLAT_LINUX)

					CPrintf File("!../../../DLDs/LinuxA8/Debug/%4.4X", m_DriverID);

					pManager->LoadClientModule(m_hModule, File);

					#endif
				}

				AfxNewObject(Name, IDriver, m_pDriver);
				}

			if( m_pDriver ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CCommsPort::FreeDLD(void)
{
	if( m_hModule ) {

		IModuleManager *pManager = NULL;

		AfxGetObject("modman", 0, IModuleManager, pManager);

		pManager->FreeClientModule(m_hModule);

		pManager->Release();
		}
	
	return TRUE;
	}

// End of File
