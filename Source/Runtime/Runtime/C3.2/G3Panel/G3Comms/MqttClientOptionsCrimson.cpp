
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client Options
//

// Constructor

CMqttClientOptionsCrimson::CMqttClientOptionsCrimson(void)
{
	m_Reconn = 0;
}

// Initialization

void CMqttClientOptionsCrimson::Load(PCBYTE &pData)
{
	BYTE bGuid[16];

	g_pDbase->GetVersion(bGuid);

	CMqttQueuedClientOptions::Load(pData, bGuid);

	m_Mode   = GetByte(pData);

	m_Reconn = GetByte(pData);

	m_Drive  = GetByte(pData);
}

// Config Fixup

BOOL CMqttClientOptionsCrimson::FixConfig(void)
{
	for( UINT n = 0; n < elements(m_PeerName); n++ ) {

		if( !FixCoded(m_PeerName[n], n ? FALSE : TRUE) ) {

			return FALSE;
		}
	}

	FixCoded(m_ClientId, FALSE);

	FixCoded(m_UserName, FALSE);
	
	FixCoded(m_Password, FALSE);

	return CMqttQueuedClientOptions::FixConfig();
}

// Loading Helpers

void CMqttClientOptionsCrimson::GetCoded(PCBYTE &pData, CString &Text)
{
	switch( GetByte(pData) ) {

		case 0:
		{
			Text.Empty();
		}
		break;

		case 1:
		{
			// TODO -- This needs to be hacked once Kathy's
			// alignment and other mods are merged across!!!

			CByteArray Data;

			WORD wMark = GetWord(pData);

			Data.Append(HIBYTE(wMark));
			Data.Append(LOBYTE(wMark));

			UINT uRefs = GetWord(pData);

			Data.Append(HIBYTE(uRefs));
			Data.Append(LOBYTE(uRefs));

			while( uRefs-- ) {

				DWORD r = GetLong(pData);

				Data.Append(HIBYTE(HIWORD(r)));
				Data.Append(LOBYTE(HIWORD(r)));
				Data.Append(HIBYTE(LOWORD(r)));
				Data.Append(LOBYTE(LOWORD(r)));
			}

			UINT uCode = GetWord(pData);

			Data.Append(HIBYTE(uCode));
			Data.Append(LOBYTE(uCode));

			Data.Append(pData, uCode);

			pData += uCode;

			Text = CString(' ', Data.size());

			memcpy(PBYTE(PCSTR(Text)), Data.data(), Data.size());
		}
		break;

		default:
		{
			AfxAssert(FALSE);
		}
		break;
	}
}

BOOL CMqttClientOptionsCrimson::FixCoded(CString &Text, BOOL fRequired)
{
	if( Text.GetLength() ) {

		PCBYTE pData = PCBYTE(PCSTR(Text));

		CAutoPointer<CCodedItem> pItem(New CCodedItem);

		pItem->Load(pData);

		pItem->SetScan(scanTrue);

		for( SetTimer(5000); GetTimer(); Sleep(25) ) {

			if( pItem->IsAvail() ) {

				PUTF p = PUTF(pItem->Execute(typeString));

				Text = UniConvert(p);

				Free(p);

				if( !Text.IsEmpty() ) {

					pItem->SetScan(scanFalse);

					return TRUE;
				}

				break;
			}
		}

		pItem->SetScan(scanFalse);
	}

	return !fRequired;
}

// End of File
