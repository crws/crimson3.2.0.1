
#include "intern.hpp"

#include "proxy.hpp"

#include "queue.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Master Proxy Object
//

class CProxyMaster : public CProxy
{
	public:
		// Constructor
		CProxyMaster(BOOL fProCon);

		// Destructor
		~CProxyMaster(void);

		// Binding
		BOOL Bind(CCommsPort *pPort, IDriver *pDriver);

		// Task Count
		UINT GetTaskCount(void);

		// Entry Points
		void Init(UINT uID);
		void Task(UINT uID);
		void Stop(UINT uID);
		void Term(UINT uID);

		// Suspension
		void Suspend(void);
		void Resume(void);

	protected:
		// Comms Status
		enum
		{
			commsOkay,
			commsBusy,
			commsError,
			commsSilent,
			commsHard,
			};

		// Data Members
		BOOL		   m_fProCon;
		BOOL		   m_fOpen;
		ICommsMaster     * m_pMaster;
		ISlaveHelper     * m_pSlaveHelper;
		UINT		   m_uFlags;
		IEvent		 * m_pTask1;
		CCommsManager    * m_pManager;
		CCommsDeviceList * m_pDevices;
		UINT               m_uDevices;
		CCommsDevice     * m_pCurrent;
		CCommsDevice     * m_pDev;
		UINT		   m_uLast;
		UINT		   m_uScan;
		UINT		   m_uNext;
		BOOL		   m_fRespond;
		BOOL		   m_fProblem;

		// Service Routine
		void ServiceDriver(void);
		void CheckPreempt(void);
		void CheckFlags(void);

		// Device Handling
		void ServiceDevice(void);
		BOOL IsDeviceOnline(void);
		BOOL SendDevicePing(BOOL fDef);
		BOOL SwitchDevice(void);
		BOOL CheckDevice(void);
		BOOL CheckDelay(void);
		INT  FindGapLimit(void);
		void EnableQueues(void);

		// System Blocks
		void ServiceSysBlocks(void);
		BOOL ServiceSysBlockPuts(CCommsSysBlock *pBlock);
		BOOL ServiceSysBlockGets(CCommsSysBlock *pBlock);
		BOOL SetBlockError(CCommsSysBlock *pBlock, UINT uCode);
		INT  FindPutCount(CCommsSysBlock *pBlock, INT nPos);
		INT  FindGetCount(CCommsSysBlock *pBlock, INT nPos);

		// Mapping Blocks
		void ServiceMapBlocks(void);
		BOOL ServiceMapBlockPuts(CCommsMapBlock *pBlock);
		BOOL ServiceMapBlockGets(CCommsMapBlock *pBlock);
		BOOL SetBlockError(CCommsMapBlock *pBlock, UINT uCode);
		INT  FindPutCount(CCommsMapBlock *pBlock, INT nPos);
		INT  FindGetCount(CCommsMapBlock *pBlock, INT nPos);

		// Write Queue
		BOOL ServiceWriteQueue(void);

		// Data Transfer
		UINT DevIO(CAddress Addr, PDWORD pData, INT nCount, INT nScale, UINT uType, INT &nSkip);
		UINT DevIO(CAddress Addr, DWORD Data, DWORD dwMask);

		// Implementation
		BOOL Notify(PCTXT pEvent);
	};

//////////////////////////////////////////////////////////////////////////
//
// Master Proxy Object
//

// Helper Creation

extern ISlaveHelper * Create_SlaveHelper(CCommsDevice *pDevice);

// Debug Flag

#define PX_DEBUG FALSE

// Instantiator

CProxy * Create_ProxyMaster(void)
{
	return New CProxyMaster(FALSE);
	}

CProxy * Create_ProxyProCon(void)
{
	return New CProxyMaster(TRUE);
	}

// Constructor

CProxyMaster::CProxyMaster(BOOL fProCon)
{
	m_fProCon      = fProCon;

	m_fOpen        = FALSE;

	m_pMaster      = NULL;

	m_pSlaveHelper = NULL;

	m_uFlags       = 0;

	m_pTask1       = NULL;
	}

// Destructor

CProxyMaster::~CProxyMaster(void)
{
	AfxRelease(m_pMaster);

	AfxRelease(m_pSlaveHelper);
	}

// Binding

BOOL CProxyMaster::Bind(CCommsPort *pPort, IDriver *pDriver)
{
	if( CProxy::Bind(pPort, pDriver) ) {

		m_pDriver->QueryInterface(AfxAeonIID(ICommsMaster), (void **) &m_pMaster);

		if( m_pMaster ) {

			if( m_fProCon ) {

				ICommsSlave *pSlave = NULL;

				m_pDriver->QueryInterface(AfxAeonIID(ICommsSlave), (void **) &pSlave);

				if( pSlave ) {

					CCommsDeviceList *pDevices = m_pPort->m_pDevices;

					CCommsDevice     *pDevice  = NULL;

					if( pDevices->m_uCount ) {

						pDevice = pDevices->m_ppDevice[0];
						}

					m_pSlaveHelper = Create_SlaveHelper(pDevice);

					pSlave->SetHelper(m_pSlaveHelper);

					pSlave->Release();
					}
				}

			m_uFlags = m_pMaster->GetMasterFlags();

			return TRUE;
			}
		}

	return FALSE;
	}

// Task Count

UINT CProxyMaster::GetTaskCount(void)
{
	if( m_uFlags & MF_SERVICE_TASK ) {

		return 2;
		}

	return 1;
	}

// Entry Points

void CProxyMaster::Init(UINT uID)
{
	if( uID == 0 ) {

		m_pManager = CCommsSystem::m_pThis->m_pComms;

		m_pDevices = m_pPort->m_pDevices;

		m_uDevices = m_pDevices->m_uCount;

		m_pCurrent = NULL;

		m_pDev	   = NULL;

		m_uLast    = 0;

		m_uScan	   = 0;

		m_uNext	   = 0;

		Notify("Init");
		}

	if( uID == 1 ) {

		m_pTask1 = Create_ManualEvent();
		}
	}

void CProxyMaster::Task(UINT uID)
{
	if( uID == 0 ) {

		if( m_pPort->Open(m_pComms) ) {

			m_fOpen = TRUE;

			CheckFlags();

			EnableQueues();

			if( m_pTask1 ) {

				m_pTask1->Set();
				}

			for(;;) {

				ServiceDriver();

				ForceSleep(50);
				}
			}
		}

	if( uID == 1 ) {

		m_pTask1->Wait(FOREVER);

		m_pComms->Service();
		}
	}

void CProxyMaster::Stop(UINT uID)
{
	if( uID == 0 ) {

		Notify("Stop");
		}
	}

void CProxyMaster::Term(UINT uID)
{
	if( uID == 0 ) {

		if( m_fOpen ) {

			Notify("Term");

			m_pPort->Term();
			}
		}

	if( uID == 1 ) {

		m_pTask1->Release();
		}
	}

// Suspension

void CProxyMaster::Suspend(void)
{
	for( UINT uDevice = 0; uDevice < m_uDevices; uDevice++ ) {

		CCommsDevice *pDev = m_pDevices->m_ppDevice[uDevice];

		if( pDev ) {

			pDev->Suspend();
			}
		}
	}

void CProxyMaster::Resume(void)
{
	for( UINT uDevice = 0; uDevice < m_uDevices; uDevice++ ) {

		CCommsDevice *pDev = m_pDevices->m_ppDevice[uDevice];

		if( pDev ) {

			pDev->Resume();
			}
		}

	m_uLast = 0;

	m_uScan	= 0;

	m_uNext	= 0;
	}

// Service Routine

void CProxyMaster::ServiceDriver(void)
{
	m_uScan = m_uScan + 1;

	m_uNext = max(m_uNext, m_uScan + 4);

	for( UINT uDevice = 0; uDevice < m_uDevices; uDevice++ ) {

		m_pDev = m_pDevices->m_ppDevice[uDevice];

		if( m_pDev ) {

			m_fRespond = FALSE;

			m_fProblem = FALSE;

			if( !m_pDev->IsEnabled() ) {

				m_pDev->SetError(errorInit);
				}
			else
				ServiceDevice();

			m_pDev->SetRespond(m_fRespond);

			m_pDev->SetProblem(m_fProblem);

			m_pDev->Tickle();
			}

		m_pPort->Poll();
		}
	}

void CProxyMaster::CheckPreempt(void)
{
	for( UINT uDevice = 0; uDevice < m_uDevices; uDevice++ ) {

		CCommsDevice *pDev = m_pDevices->m_ppDevice[uDevice];

		CCommsDevice *pOld = m_pDev;

		if( pDev != pOld ) {
			
			if( pDev->HasPreempt() ) {

				m_pDev->SetRespond(m_fRespond);

				m_pDev->SetProblem(m_fProblem);

				m_pDev     = pDev;

				m_fRespond = FALSE;

				m_fProblem = FALSE;

				ServiceWriteQueue();

				m_pDev->SetRespond(m_fRespond);

				m_pDev->SetProblem(m_fProblem);

				m_pDev = pOld;
				}
			}
		}
	}

void CProxyMaster::CheckFlags(void)
{
	if( m_fOpen ) {

		if( m_pDriver->GetFlags() & DF_REMOTED ) {

			m_uFlags = m_pMaster->GetMasterFlags();
			}
		}
	}

// Device Handling

void CProxyMaster::ServiceDevice(void)
{
	if( IsDeviceOnline() ) {

		ServiceSysBlocks();
		}

	if( IsDeviceOnline() ) {

		ServiceMapBlocks();
		}

	if( !m_pTask1 ) {

		m_pComms->Service();
		}
	}

BOOL CProxyMaster::IsDeviceOnline(void)
{
	UINT uError;
	
	switch( (uError = m_pDev->GetError()) ) {

		case errorNone:

			return TRUE;

		case errorSoft:
		case errorInit:

			if( uError == errorSoft ) {

				if( m_pDev->GetInvite() != m_uScan ) {

					return FALSE;
					}
				}

			if( SendDevicePing(TRUE) ) {

				m_pDev->SetError(errorNone);

				return TRUE;
				}

			m_pDev->SetError(errorSoft);

			m_pDev->SetInvite(m_uNext++);

			return FALSE;

		case errorHard:

			return FALSE;
		}

	return FALSE;
	}

BOOL CProxyMaster::SendDevicePing(BOOL fDef)
{
	if( SwitchDevice() ) {

		CCODE cCode = m_pMaster->Ping();

		if( PX_DEBUG || COMMS_SUCCESS(cCode) ) {

			if( cCode == 2 ) {

				// NOTE -- Basis returns 2 as a default, so
				// we use this to indicate that Ping was not
				// really implemented by the driver.

				return fDef;
				}
		
			m_fRespond = TRUE;

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CProxyMaster::SwitchDevice(void)
{
	if( m_pCurrent != m_pDev ) {

		if( m_pCurrent ) {

			m_pComms->DeviceClose(TRUE);
			}

		if( COMMS_SUCCESS(m_pComms->DeviceOpen(m_pDev)) ) {

			m_pCurrent = m_pDev;

			return TRUE;
			}

		m_pCurrent = NULL;

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyMaster::CheckDevice(void)
{
	if( !SendDevicePing(FALSE) ) {

		m_pDev->SetInvite(m_uNext++);

		m_pDev->SetError (errorSoft);

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyMaster::CheckDelay(void)
{
	// REV3 -- This is not quite what the docs say! This imposes
	// a minimum time between the start of transactions, but the
	// help says that it ought to just implement a fixed delay
	// after we recieve the reply. Which is correct???

	if( m_pDev->m_Delay ) {

		UINT uDelay = ToTicks(m_pDev->m_Delay);

		UINT uTime  = GetTickCount();

		UINT uGone  = uTime - m_uLast;

		if( uGone < uDelay ) {

			Sleep(ToTime(uDelay - uGone));

			m_uLast = GetTickCount();

			return TRUE;
			}

		m_uLast = uTime;
		}

	return FALSE;
	}

INT CProxyMaster::FindGapLimit(void)
{
	if( m_uFlags & MF_NO_SPANNING ) {
			
		return 1;
		}

	return m_pDev->m_Spanning ? 4 : 1;
	}

void CProxyMaster::EnableQueues(void)
{
	for( UINT uDevice = 0; uDevice < m_uDevices; uDevice++ ) {

		CCommsDevice *pDev = m_pDevices->m_ppDevice[uDevice];

		if( pDev ) {

			pDev->EnableQueue();
			}
		}
	}

// System Blocks

void CProxyMaster::ServiceSysBlocks(void)
{
	CCommsSysBlockList *pBlocks = m_pDev->m_pSysBlocks;

	UINT                uCount  = pBlocks->m_uCount;

	for( UINT uBlock = 0; uBlock < uCount; uBlock++ ) {

		CCommsSysBlock *pBlock = pBlocks->m_ppBlock[uBlock];

		if( pBlock ) {

			if( !ServiceWriteQueue() ) {

				break;
				}

			if( pBlock->GetError() == errorHard ) {

				continue;
				}

			if( pBlock->GetError() == errorSoft ) {

				pBlock->SetError(errorInit);
				}

			if( pBlock->IsActive() ) {

				if( SwitchDevice() ) {

					if( !ServiceSysBlockPuts(pBlock) ) {

						break;
						}

					if( !ServiceSysBlockGets(pBlock) ) {

						break;
						}

					CheckPreempt();
					}

				continue;
				}
			}
		}
	
	ServiceWriteQueue();
	}

BOOL CProxyMaster::ServiceSysBlockPuts(CCommsSysBlock *pBlock)
{
	for( INT nPos = 0; nPos < pBlock->m_Size; ) {

		if( pBlock->ShouldWrite(nPos) ) {

			INT      nCount = FindPutCount(pBlock, nPos);

			INT      nScale = pBlock->m_Factor;

			CAddress Addr   = pBlock->GetAddress(nPos);

			PDWORD   pData  = pBlock->GetWriteData(nPos, nCount);

			INT	 nSkip  = -1;

			UINT	 uCode  = DevIO(Addr, pData, nCount, nScale, ioPut, nSkip);

			if( uCode == commsOkay || uCode == commsHard ) {

				for( INT nReg = 0; nReg < nCount; nReg++ ) {

					pBlock->SetWriteDone(nPos + nReg);
					}
				}
			else {
				if( !CheckDevice() ) {

					delete [] pData;

					return FALSE;
					}
				}

			delete [] pData;

			nPos += nCount;

			continue;
			}

		nPos += 1;
		}

	return TRUE;
	}

BOOL CProxyMaster::ServiceSysBlockGets(CCommsSysBlock *pBlock)
{
	for( INT nPos = 0; nPos < pBlock->m_Size; ) {

		if( pBlock->ShouldRead(nPos) ) {

			INT      nCount = FindGetCount(pBlock, nPos);

			CAutoArray<DWORD> pData(nCount);

			INT      nScale = pBlock->m_Factor;

			CAddress Addr   = pBlock->GetAddress(nPos);

			INT	 nSkip  = -1;

			UINT	 uCode  = DevIO(Addr, pData, nCount, nScale, ioGet, nSkip);

			if( uCode == commsOkay ) {

				for( INT nReg = 0; nReg < nCount; nReg++ ) {

					if( nReg == nSkip ) {

						nSkip = INT(pData[nReg]);

						continue;
						}

					pBlock->SetCommsData(nPos + nReg, pData[nReg]);
					}
				}
			else {
				if( SetBlockError(pBlock, uCode) ) {

					if( m_pDev->GetError() == errorNone ) {

						return TRUE;
						}

					return FALSE;
					}
				}

			nPos += nCount;

			continue;
			}

		nPos += 1;
		}

	pBlock->SetError(errorNone);

	return TRUE;
	}

BOOL CProxyMaster::SetBlockError(CCommsSysBlock *pBlock, UINT uCode)
{
	if( uCode == commsHard ) {

		pBlock->SetError(errorHard);

		return TRUE;
		}

	if( uCode == commsError ) {

		if( SendDevicePing(FALSE) ) {

			pBlock->SetError(errorSoft);
			}
		else {
			m_pDev->SetInvite(m_uNext++);

			m_pDev->SetError (errorSoft);
			}

		return TRUE;
		}

	if( uCode == commsSilent ) {

		return FALSE;
		}

	return TRUE;
	}

INT CProxyMaster::FindPutCount(CCommsSysBlock *pBlock, INT nPos)
{
	if( !pBlock->IsNamed() ) {

		INT nEnd;

		for( nEnd = nPos + 1; nEnd < pBlock->m_Size; nEnd++ ) {

			if( pBlock->ShouldWrite(nEnd) ) {

				continue;
				}

			break;
			}

		return nEnd - nPos;
		}

	return 1;
	}

INT CProxyMaster::FindGetCount(CCommsSysBlock *pBlock, INT nPos)
{
	if( !pBlock->IsNamed() ) {

		INT nGap = FindGapLimit();

		INT nHit = nPos + 1;

		for( INT nEnd = nPos + 1; nEnd < pBlock->m_Size; nEnd++ ) {

			if( pBlock->ShouldRead(nEnd) ) {

				nGap = FindGapLimit();

				nHit = nEnd + 1;

				continue;
				}

			if( !pBlock->CouldRead(nEnd) ) {

				break;
				}

			if( !--nGap ) {

				break;
				}
			}

		return nHit - nPos;
		}

	return 1;
	}

// Mapping Blocks

void CProxyMaster::ServiceMapBlocks(void)
{
	CCommsMapBlockList *pBlocks = m_pDev->m_pMapBlocks;

	UINT                uCount  = pBlocks->m_uCount;

	for( UINT uBlock = 0; uBlock < uCount; uBlock++ ) {

		CCommsMapBlock *pBlock = pBlocks->m_ppBlock[uBlock];

		if( pBlock ) {

			if( pBlock->GetError() == errorHard ) {

				continue;
				}

			if( pBlock->IsActive() ) {

				if( SwitchDevice() ) {

					if( pBlock->m_Write ) {

						if( !ServiceMapBlockGets(pBlock) ) {

							break;
							}
						}
					else {
						if( !ServiceMapBlockPuts(pBlock) ) {

							break;
							}
						}
					}

				pBlock->MarkDone();

				CheckPreempt();
				}
			}
		}
	}

BOOL CProxyMaster::ServiceMapBlockPuts(CCommsMapBlock *pBlock)
{
	if( pBlock->UpdateWriteData() ) {

		for( INT nPos = 0; nPos < pBlock->m_Size; ) {

			if( pBlock->ShouldWrite(nPos) ) {

				INT      nCount = FindPutCount(pBlock, nPos);

				INT      nScale = pBlock->m_Factor;

				CAddress Addr   = pBlock->GetAddress(nPos);

				PDWORD   pData  = pBlock->GetWriteData(nPos);

				INT	 nSkip  = -1;
	
				UINT	 uCode  = DevIO(Addr, pData, nCount, nScale, ioPut, nSkip);

				if( uCode == commsOkay || uCode == commsHard ) {

					for( INT nReg = 0; nReg < nCount; nReg++ ) {

						pBlock->SetWriteDone(nPos + nReg);
						}
					}
				else {
					if( !CheckDevice() ) {

						return FALSE;
						}
					}

				nPos += nCount;

				continue;
				}

			nPos += 1;
			}
		}

	return TRUE;
	}

BOOL CProxyMaster::ServiceMapBlockGets(CCommsMapBlock *pBlock)
{
	if( pBlock->UpdateReadData() ) {

		for( INT nPos = 0; nPos < pBlock->m_Size; ) {

			if( pBlock->ShouldRead(nPos) ) {

				INT      nCount = FindGetCount(pBlock, nPos);

				CAutoArray<DWORD> pData(nCount);

				INT      nScale = pBlock->m_Factor;

				CAddress Addr   = pBlock->GetAddress(nPos);

				INT	 nSkip  = -1;

				UINT	 uCode  = DevIO(Addr, pData, nCount, nScale, ioGet, nSkip);

				if( uCode == commsOkay ) {

					for( INT nReg = 0; nReg < nCount; nReg++ ) {

						if( nReg == nSkip ) {

							nSkip = INT(pData[nReg]);

							continue;
							}

						pBlock->SetCommsData(nPos + nReg, pData[nReg]);
						}
					}
				else {
					if( SetBlockError(pBlock, uCode) ) {

						if( m_pDev->GetError() == errorNone ) {

							return TRUE;
							}

						return FALSE;
						}
					}

				nPos += nCount;

				continue;
				}

			nPos += 1;
			}
		}

	return TRUE;
	}

BOOL CProxyMaster::SetBlockError(CCommsMapBlock *pBlock, UINT uCode)
{
	if( uCode == commsHard ) {

		pBlock->SetError(errorHard);

		return TRUE;
		}

	if( uCode == commsError ) {

		if( !SendDevicePing(FALSE) ) {

			m_pDev->SetInvite(m_uNext++);

			m_pDev->SetError (errorSoft);
			}

		return TRUE;
		}

	if( uCode == commsSilent ) {

		return FALSE;
		}

	return TRUE;
	}

INT CProxyMaster::FindPutCount(CCommsMapBlock *pBlock, INT nPos)
{
	INT nEnd;

	for( nEnd = nPos + 1; nEnd < pBlock->m_Size; nEnd++ ) {

		if( pBlock->ShouldWrite(nEnd) ) {

			continue;
			}

		break;
		}

	return nEnd - nPos;
	}

INT CProxyMaster::FindGetCount(CCommsMapBlock *pBlock, INT nPos)
{
	INT nGap = FindGapLimit();

	INT nHit = nPos + 1;

	for( INT nEnd = nPos + 1; nEnd < pBlock->m_Size; nEnd++ ) {

		if( pBlock->ShouldRead(nEnd) ) {

			nGap = FindGapLimit();

			nHit = nEnd + 1;

			continue;
			}

		if( !pBlock->CouldRead(nEnd) ) {

			break;
			}

		if( !--nGap ) {

			break;
			}
		}

	return nHit - nPos;
	}

// Write Queue

BOOL CProxyMaster::ServiceWriteQueue(void)
{
	CAutoArray<DWORD> pData;

	for( UINT n = 0; n < 2; n++ ) {

		CWriteQueue *pQueue = m_pDev->GetQueue(n);

		if( pQueue ) {

			if( !pQueue->IsEmpty() ) {

				if( !pData ) {

					pData.Alloc(128);
					}
				
				UINT   uLimit = pQueue->GetLimit();

				UINT   uCount = 0;

				CWrite Write;

				while( pQueue->GetEntry(0, Write) ) {

					if( Write.m_dwMask < NOTHING ) {

						if( SwitchDevice() ) {

							CAddress Addr  = Write.m_pBlock->GetAddress(Write.m_nPos);

							UINT     uCode = commsSilent;

							INT      nSkip = -1;

							if( m_uFlags & MF_ATOMIC_WRITE ) {

								uCode = DevIO(Addr, Write.m_Data, Write.m_dwMask);
								}

							if( uCode == commsSilent ) {

								uCode = DevIO(Addr, pData, 1, 0, ioGet, nSkip);
									
								if( uCode == commsOkay || uCode == commsHard ) {

									if( Write.m_Data ) {

										pData[0] |=  Write.m_dwMask;
										}
									else
										pData[0] &= ~Write.m_dwMask;

									uCode = DevIO(Addr, pData, 1, 0, ioPut, nSkip);
									}
								}

							if( uCode == commsOkay || uCode == commsHard ) {

								pQueue->WriteDone(1);
	
								continue;
								}

							if( !CheckDevice() ) {

								pQueue->WriteFail();

								return FALSE;
								}

							pQueue->GetEntry(0, Write);

							pQueue->AddWrite(Write);

							pQueue->StepHead(1);

							continue;
							}
						}
					else {
						pData[0] = Write.m_Data;

						INT nPos = 1;

						if( !Write.m_pBlock->IsNamed() ) {

							while( nPos < 128 ) {

								CWrite Check;

								if( pQueue->GetEntry(nPos, Check) ) {

									if( Check.m_dwMask == NOTHING ) {
											  
										if( Check.m_pBlock == Write.m_pBlock ) {

											if( Check.m_nPos == Write.m_nPos + nPos ) {

												pData[nPos++] = Check.m_Data;

												continue;
												}
											}
										}

									pQueue->DropLast();
									}
										  
								break;
								}
							}

						if( SwitchDevice() ) {

							INT	 nCount = nPos;

							INT      nScale = Write.m_pBlock->m_Factor;

							CAddress Addr   = Write.m_pBlock->GetAddress(Write.m_nPos);

							INT	 nSkip  = -1;

							UINT	 uCode  = DevIO(Addr, pData, nCount, nScale, ioPut, nSkip);

							if( uCode == commsOkay || uCode == commsHard ) {

								pQueue->WriteDone(nCount);
								}
							else {
								if( !CheckDevice() ) {

									pQueue->WriteFail();
			
									return FALSE;
									}

								for( INT n = 0; n < nCount; n++ ) {

									pQueue->GetEntry(n, Write);

									pQueue->AddWrite(Write);
									}

								pQueue->StepHead(nCount);
								}

							if( (uCount += nPos) >= uLimit ) {

								break;
								}

							continue;
							}
						}

					pQueue->WriteFail();

					return FALSE;
					}
				}
			}
		}

	return TRUE;
	}

// Data Transfer

UINT CProxyMaster::DevIO(CAddress Addr, PDWORD pData, INT nCount, INT nScale, UINT uType, INT &nSkip)
{
	INT  nPos   = 0;

	INT  nLast  = 0;

	INT  nTries = 6;

	INT  nRetry = nTries;

	while( nCount ) {

		CheckDelay();

		m_pPort->AddCount(uType, countAttempt);

		CCODE cCode = 0;

		switch( uType ) {

			case ioPut:

				if( PX_DEBUG ) {

					AfxTrace("Put %8.8X %u\n", (DWORD &) Addr, nCount);

					cCode = nCount;
					}
				else
					cCode = m_pMaster->Write(Addr, pData + nPos, nCount);
					
				break;

			case ioGet:

				if( PX_DEBUG ) {

					AfxTrace("Get %8.8X %u\n", (DWORD &) Addr, nCount);

					memset(pData + nPos, 0, sizeof(DWORD) * nCount);

					cCode = nCount;
					}
				else {
					memset(pData + nPos, 0, nCount * sizeof(DWORD));

					cCode = m_pMaster->Read(Addr, pData + nPos, nCount);
					}

				break;
			}

		if( COMMS_SUCCESS(cCode) ) {

			m_pPort->AddCount(uType, countSuccess);
			}

		if( cCode & CCODE_NO_DATA ) {

			m_fRespond = TRUE;

			m_fProblem = TRUE;

			if( uType == ioGet ) {

				if( nSkip < 0 ) {

					nSkip = nPos;

					nLast = nPos;
					}
				else {
					pData[nLast] = nPos;

					nLast        = nPos;
					}

				pData[nPos] = NOTHING;
				}

			cCode = 1;
			}

		if( COMMS_SUCCESS(cCode) ) {

			m_fRespond = TRUE;

			int nMoved = int(cCode);

			if( nMoved < nCount ) {

				nPos   += nMoved;

				nCount -= nMoved;

				nRetry  = nTries;

				Addr.a.m_Offset += nMoved * nScale;

				continue;
				}

			break;
			}

		if( !(cCode & CCODE_HARD) ) {

			if( !(cCode & CCODE_NO_RETRY) ) {

				if( cCode & CCODE_BUSY ) {

					m_fRespond = TRUE;

					if( (nRetry -= 1) > 0 ) {

						m_pPort->AddCount(uType, countRetry);

						continue;
						}
					}
				else {
					m_fProblem = TRUE;

					if( (nRetry -= 2) > 0 ) {

						m_pPort->AddCount(uType, countRetry);

						continue;
						}
					}
				}

			if( cCode & CCODE_BUSY ) {

				m_fProblem = TRUE;

				m_pPort->AddCount(uType, countBusy);

				return commsBusy;
				}

			if( cCode & CCODE_SILENT ) {

				m_pPort->AddCount(uType, countSilent);

				return commsSilent;
				}

			m_fProblem = TRUE;

			m_pPort->AddCount(uType, countError);

			return commsError;
			}

		m_fProblem = TRUE;

		m_pPort->AddCount(uType, countError);

		return commsHard;
		}

	return commsOkay;
	}

UINT CProxyMaster::DevIO(CAddress Addr, DWORD Data, DWORD dwMask)
{
	INT  nTries = 6;

	INT  nRetry = nTries;

	UINT uType  = ioPut;

	for(;;) {

		CheckDelay();

		m_pPort->AddCount(uType, countAttempt);

		CCODE cCode;
		
		if( TRUE ) {
			
			cCode = m_pMaster->Atomic(Addr, Data, dwMask);
			}

		if( COMMS_SUCCESS(cCode) ) {

			m_pPort->AddCount(uType, countSuccess);
			}

		if( cCode & CCODE_NO_DATA ) {

			m_fRespond = TRUE;

			m_fProblem = TRUE;

			cCode = 1;
			}

		if( COMMS_SUCCESS(cCode) ) {

			m_fRespond = TRUE;

			break;
			}

		if( !(cCode & CCODE_HARD) ) {

			if( !(cCode & CCODE_NO_RETRY) ) {

				if( cCode & CCODE_BUSY ) {

					m_fRespond = TRUE;

					if( (nRetry -= 1) > 0 ) {

						m_pPort->AddCount(uType, countRetry);

						continue;
						}
					}
				else {
					m_fProblem = TRUE;

					if( (nRetry -= 2) > 0 ) {

						m_pPort->AddCount(uType, countRetry);

						continue;
						}
					}
				}

			if( cCode & CCODE_BUSY ) {

				m_fProblem = TRUE;

				m_pPort->AddCount(uType, countBusy);

				return commsBusy;
				}

			if( cCode & CCODE_SILENT ) {

				m_pPort->AddCount(uType, countSilent);

				return commsSilent;
				}

			m_fProblem = TRUE;

			m_pPort->AddCount(uType, countError);

			return commsError;
			}

		m_fProblem = TRUE;

		m_pPort->AddCount(uType, countError);

		return commsHard;
		}

	return commsOkay;
	}

// Implementation

BOOL CProxyMaster::Notify(PCTXT pEvent)
{
	if( m_uFlags & MF_TASK_NOTIFY ) {

		m_pComms->DrvCtrl(NOTHING, pEvent);

		return TRUE;
		}

	return FALSE;
	}

// End of File
