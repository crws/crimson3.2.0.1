
#include "intern.hpp"

#include "lang.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Number Display Format
//

// Constructor

CDispFormatNumber::CDispFormatNumber(void)
{
	m_Radix	  = 0;

	m_Before  = 5;

	m_After	  = 0;

	m_Leading = 1;

	m_Group	  = 0;

	m_Signed  = 1;

	m_pPrefix = NULL;

	m_pUnits  = NULL;

	m_pDynDP  = NULL;
}

// Destructor

CDispFormatNumber::~CDispFormatNumber(void)
{
	delete m_pPrefix;

	delete m_pUnits;

	delete m_pDynDP;
}

// Initialization

void CDispFormatNumber::Load(PCBYTE &pData)
{
	ValidateLoad("CDispFormatNumber", pData);

	m_Radix   = GetByte(pData);

	m_Before  = GetByte(pData);

	m_After   = GetByte(pData);

	m_Leading = GetByte(pData);

	m_Group   = GetByte(pData);

	m_Signed  = GetByte(pData);

	GetCoded(pData, m_pPrefix);

	GetCoded(pData, m_pUnits);

	GetCoded(pData, m_pDynDP);
}

// Type Checking

BOOL CDispFormatNumber::IsNumber(void)
{
	return TRUE;
}

// Scan Control

BOOL CDispFormatNumber::IsAvail(void)
{
	if( !IsItemAvail(m_pUnits) ) {

		return FALSE;
	}

	if( !IsItemAvail(m_pPrefix) ) {

		return FALSE;
	}

	if( !IsItemAvail(m_pDynDP) ) {

		return FALSE;
	}

	return TRUE;
}

BOOL CDispFormatNumber::SetScan(UINT Code)
{
	SetItemScan(m_pUnits, Code);

	SetItemScan(m_pPrefix, Code);

	SetItemScan(m_pDynDP, Code);

	return TRUE;
}

// Formatting

CUnicode CDispFormatNumber::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		CUnicode Text;

		if( m_pPrefix ) {

			Text += m_pPrefix->GetText();
		}

		if( m_pUnits ) {

			Text += m_pUnits->GetText();
		}

		return Text;
	}

	if( Type == typeVoid ) {

		CUnicode Text;

		if( !(Flags & fmtBare) ) {

			if( m_pPrefix ) {

				Text += m_pPrefix->GetText();
			}
		}

		if( m_Signed ) {

			Text += '-';
		}

		if( m_Before ) {

			Text += CUnicode('-', m_Before);
		}

		if( GetAfter() ) {

			Text += '.';

			Text += CUnicode('-', GetAfter());
		}

		if( !(Flags & fmtBare) ) {

			if( m_pUnits ) {

				Text += m_pUnits->GetText();
			}
		}

		return Text;
	}

	if( Type == typeReal || Type == typeInteger ) {

		CUnicode Text;

		INT64    nData = 0;

		if( Type == typeReal ) {

			double r = I2R(Data);

			if( IsNAN(r) ) {

				return L"NAN";
			}

			if( IsINF(r) ) {

				if( r > 0 ) {

					return L"+INF";
				}

				return L"-INF";
			}

			if( fabs(r) >= Power(m_Before) ) {

				if( r > 0 ) {

					return L"+BIG";
				}

				return L"-BIG";
			}

			r *= double(Power(GetAfter()));

			if( r < 0 ) r -= 0.5;

			if( r > 0 ) r += 0.5;

			nData = INT64(r);
		}

		UINT  uTotal = m_Before + GetAfter();

		BOOL    fBig = uTotal > 18;

		if( Type == typeInteger ) {

			nData = C3INT(Data);

			if( m_Signed ) {

				if( fBig || abs(nData) >= Power(m_Before + GetAfter()) ) {

					if( nData > 0 ) {

						return L"+BIG";
					}

					return L"-BIG";
				}
			}
			else {
				nData &= 0xFFFFFFFF;

				if( fBig || nData >= Power(m_Before + GetAfter()) ) {

					return L"+BIG";
				}
			}
		}

		UINT  uRadix = GetRadix();

		INT64 Factor = Power(uTotal - 1);

		BOOL  fHide  = (m_Leading >=  1) ? TRUE : FALSE;

		BOOL  fPad   = (m_Leading ==  2) ? FALSE : !!(Flags & fmtPad);

		WCHAR cSpace = (Flags & fmtANSI) ? spaceNormal : spaceFigure;

		WCHAR cGroup = GetGroupChar();

		WCHAR cPoint = GetPointChar();

		// REV3 -- Can we use some special characters for the plus
		// and minus to ensure that they are the same size as a digit
		// when using a variable pitch font?

		if( m_Signed ) {

			if( nData < 0 ) {

				nData = -nData;

				Text += '-';
			}
			else {
				if( m_Signed == 1 ) {

					if( fPad ) {

						Text += cSpace;
					}
				}
				else {
					if( !nData ) {

						if( fPad ) {

							Text += cSpace;
						}
					}
					else
						Text += '+';
				}
			}
		}

		if( m_Radix == 1 ) {

			if( !(Flags & fmtANSI) ) {

				Text +=	uniLRM;
			}
		}

		MakeMin(uTotal, 18);

		for( UINT n = 0; n < uTotal; n++ ) {

			PCUTF  m_pHex = L"0123456789ABCDEF";

			WCHAR  cDigit = '*';

			if( (Flags & fmtShow) || m_Radix < 4 ) {

				cDigit = m_pHex[nData / Factor % uRadix];

				if( n == m_Before - 1 ) {

					fHide = FALSE;
				}

				if( cDigit == '0' ) {

					if( fHide ) {

						if( fPad ) {

							cDigit = cSpace;
						}
						else
							cDigit = 0;
					}
				}
				else
					fHide = FALSE;
			}

			if( cDigit ) {

				Text += cDigit;
			}

			if( n == m_Before - 1 && GetAfter() ) {

				Text += cPoint;
			}

			if( m_Group ) {

				if( n < m_Before - 1 ) {

					if( IsGroupBoundary(m_Before - 1 - n) ) {

						if( fHide ) {

							if( fPad ) {

								Text += cSpace;
							}
						}
						else
							Text += cGroup;
					}
				}
			}

			Factor /= uRadix;
		}

		if( m_Radix == 1 ) {

			if( !(Flags & fmtANSI) ) {

				Text += uniPDF;
			}
		}

		if( Flags & fmtPad ) {

			if( !(Flags & fmtANSI) ) {

				if( m_Radix == 1 ) {

					MakeLettersFixed(Text);
				}

				MakeDigitsFixed(Text);
			}
		}

		if( !(Flags & fmtBare) ) {

			if( m_pPrefix ) {

				Text = m_pPrefix->GetText() + Text;
			}

			if( m_pUnits ) {

				Text = Text + m_pUnits->GetText();
			}
		}

		return Text;
	}

	return CDispFormat::Format(Data, Type, Flags);
}

// Parsing

BOOL CDispFormatNumber::Parse(DWORD &Data, CString Text, UINT Type)
{
	Text.Remove(GetGroupChar());

	if( Type == typeInteger ) {

		UINT uRadix = GetRadix();

		if( uRadix == 10 ) {

			if( GetAfter() ) {

				double Real = strtod(Text, NULL);

				C3INT  Sign = (Real < 0) ? -1 : +1;

				Data = C3INT(fabs(Real) * Power(GetAfter()) + 0.5) * Sign;
			}
			else {
				if( m_Signed ) {

					Data = strtol(Text, NULL, uRadix);
				}
				else
					Data = strtoul(Text, NULL, uRadix);
			}
		}
		else
			Data = strtoul(Text, NULL, uRadix);

		return TRUE;
	}

	return GeneralParse(Data, Text, Type);
}

// Editing

UINT CDispFormatNumber::Edit(CEditCtx *pEdit, UINT uCode)
{
	if( uCode == 0x00 ) return OnEditBegin(pEdit, uCode);

	if( uCode == 0x0A ) return OnEditRamp(pEdit, FALSE);

	if( uCode == 0x0B ) return OnEditRamp(pEdit, TRUE);

	if( uCode == 0x0D ) return OnEditEnter(pEdit);

	if( uCode == 0x1B ) return OnEditClose(pEdit);

	if( uCode == 0x7F ) return OnEditDelete(pEdit);

	if( uCode >= 0x20 ) return OnEditChar(pEdit, uCode);

	return editNone;
}

// Limit Access

BOOL CDispFormatNumber::NeedsLimits(void)
{
	return TRUE;
}

DWORD CDispFormatNumber::GetMin(UINT Type)
{
	if( Type == typeInteger ) {

		if( m_Signed ) {

			UINT uRadix = GetRadix();

			if( uRadix ) {

				UINT  uTotal = m_Before + GetAfter();

				INT64 nValue = Power(uTotal);

				nValue -=  1;

				nValue *= -1;

				return DWORD(nValue);
			}
		}

		return 0;
	}

	if( Type == typeReal ) {

		C3REAL rData = 0;

		if( m_Signed ) {

			UINT uRadix = GetRadix();

			if( uRadix ) {

				C3REAL rWork  = 0;

				UINT   uTotal = m_Before + GetAfter();

				for( UINT n = 0; n < uTotal; n++ ) {

					if( n == 0 || n == m_Before ) {

						rWork  = C3REAL(uRadix - 1);

						rWork *= -1.0;
					}

					if( n < m_Before ) {

						rData += rWork;

						rWork *= uRadix;
					}
					else {
						rWork /= uRadix;

						rData += rWork;
					}
				}
			}
		}

		return R2I(rData);
	}

	return CDispFormat::GetMin(Type);
}

DWORD CDispFormatNumber::GetMax(UINT Type)
{
	if( Type == typeInteger ) {

		UINT uRadix = GetRadix();

		if( uRadix ) {

			UINT  uTotal = m_Before + GetAfter();

			INT64 nValue = Power(uTotal);

			nValue -=  1;

			nValue *= +1;

			return DWORD(nValue);
		}

		return 0;
	}

	if( Type == typeReal ) {

		C3REAL rData = 0;

		UINT    uRadix = GetRadix();

		if( uRadix ) {

			C3REAL rWork = 0;

			UINT uTotal = m_Before + GetAfter();

			for( UINT n = 0; n < uTotal; n++ ) {

				if( n == 0 || n == m_Before ) {

					rWork = C3REAL(uRadix - 1);
				}

				if( n < m_Before ) {

					rData += rWork;

					rWork *= uRadix;
				}
				else {
					rWork /= uRadix;

					rData += rWork;
				}
			}
		}

		return R2I(rData);
	}

	return CDispFormat::GetMax(Type);
}

// Stepping

DWORD CDispFormatNumber::GetStep(UINT Type)
{
	if( Type == typeInteger ) {

		return 1;
	}

	if( Type == typeReal ) {

		double r = GetRadix();

		double s = 1.0 / pow(r, int(GetAfter()));

		return R2I(s);
	}

	return 0;
}

BOOL CDispFormatNumber::SpeedUp(UINT uCount)
{
	UINT r = GetRadix();

	if( uCount >= r ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CDispFormatNumber::SpeedUp(DWORD Data, UINT Type, DWORD &Step)
{
	if( Type == typeInteger ) {

		C3INT n = C3INT(Data);

		C3INT s = C3INT(Step);

		C3INT d = abs(n / s);

		C3INT r = GetRadix();

		if( d % r == 0 ) {

			Step = DWORD(s * r);

			return TRUE;
		}

		return FALSE;
	}

	if( Type == typeReal ) {

		C3REAL n = I2R(Data);

		C3REAL s = I2R(Step);

		C3INT  d = C3INT(fabs(n / s) + 0.5);

		C3INT  r = GetRadix();

		if( d % r == 0 ) {

			Step = R2I(s * r);

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

// Implementation

UINT CDispFormatNumber::GetRadix(void)
{
	switch( m_Radix ) {

		case 0: return 10;
		case 1: return 16;
		case 2: return 2;
		case 3: return 8;
		case 4: return 10;
	}

	return 0;
}

UINT CDispFormatNumber::GetLimit(void)
{
	switch( m_Radix ) {

		case 0: return 10;
		case 1: return 8;
		case 2: return 32;
		case 3: return 10;
		case 4: return 10;
	}

	return 0;
}

INT64 CDispFormatNumber::Power(UINT n)
{
	INT64 p = 1;

	INT64 r = GetRadix();

	while( n-- ) p *= r;

	return p ? p : r;
}

BOOL CDispFormatNumber::IsGroupBoundary(UINT n)
{
	if( n ) {

		switch( m_Radix ) {

			case 1: return n % 4 == 0;
			case 2: return n % 4 == 0;
			case 3: return n % 3 == 0;
		}

		return CCommsSystem::m_pThis->m_pLang->IsGroupBoundary(n);
	}

	return FALSE;
}

WCHAR CDispFormatNumber::GetGroupChar(void)
{
	switch( m_Radix ) {

		case 1: return '-';
		case 2: return '-';
		case 3: return '-';
	}

	return CCommsSystem::m_pThis->m_pLang->GetNumGroupChar();
}

WCHAR CDispFormatNumber::GetPointChar(void)
{
	return CCommsSystem::m_pThis->m_pLang->GetDecPointChar();
}

UINT CDispFormatNumber::GetAfter(void)
{
	if( m_pDynDP ) {

		UINT uAfter = m_pDynDP->ExecVal();

		return max(0, min(m_After, uAfter));
	}

	return m_After;
}

// Edit Handlers

UINT CDispFormatNumber::OnEditBegin(CEditCtx *pEdit, UINT uCode)
{
	if( pEdit->m_uFlags & flagRampOnly ) {

		if( pEdit->m_uFlags & flagTwoNumeric ) {

			pEdit->m_uKeypad = keypadNudge1;
		}
		else
			pEdit->m_uKeypad = keypadNudge2;
	}
	else {
		UINT uFlag = (pEdit->m_uFlags & flagShowRamp) ? kpsShowRamp : 0;

		switch( GetRadix() ) {

			case 10: pEdit->m_uKeypad = keypadNumeric | uFlag; break;
			case 16: pEdit->m_uKeypad = keypadHex     | uFlag; break;
			case  8: pEdit->m_uKeypad = keypadOctal   | uFlag; break;
			case  2: pEdit->m_uKeypad = keypadBinary  | uFlag; break;
		}
	}

	pEdit->m_Foot    += Format(pEdit->m_Min, pEdit->m_Type, fmtBare | fmtShow);

	pEdit->m_Foot    += L" - ";

	pEdit->m_Foot    += Format(pEdit->m_Max, pEdit->m_Type, fmtBare | fmtShow);

	pEdit->m_uCursor  = cursorAll;

	pEdit->m_fDefault = TRUE;

	pEdit->m_fError   = TRUE;

	pEdit->m_fHide    = (m_Radix == 4);

	pEdit->m_Step     = 0;

	pEdit->m_uCount   = 0;

	return editUpdate;
}

UINT CDispFormatNumber::OnEditRamp(CEditCtx *pEdit, BOOL fRaise)
{
	if( !pEdit->m_fDefault ) {

		double Read = 0;

		if( pEdit->m_Edit.GetLength() ) {

			CString Edit = UniConvert(pEdit->m_Edit);

			UINT	Base = GetRadix();

			if( Base == 10 ) {

				Read = strtod(Edit, NULL);
			}
			else
				Read = strtoul(Edit, NULL, Base);
		}

		if( pEdit->m_Type == typeReal ) {

			C3REAL Data = C3REAL(Read);

			pEdit->m_Data = R2I(Data);
		}
		else {
			UINT Base = GetRadix();

			if( Base == 10 ) {

				C3INT Sign = (Read < 0) ? -1 : +1;

				C3INT Data = C3INT(fabs(Read) * Power(GetAfter()) + 0.5) * Sign;

				pEdit->m_Data = DWORD(Data);
			}
			else {
				DWORD Data = DWORD(Read * Power(GetAfter()));

				pEdit->m_Data = DWORD(Data);
			}
		}
	}

	if( !pEdit->m_Step ) {

		pEdit->m_Step   = GetStep(pEdit->m_Type);

		pEdit->m_uCount = 0;
	}
	else {
		if( pEdit->m_uAccel ) {

			if( SpeedUp(++pEdit->m_uCount) ) {

				if( SpeedUp(pEdit->m_Data, pEdit->m_Type, pEdit->m_Step) ) {

					pEdit->m_uAccel = pEdit->m_uAccel - 1;

					pEdit->m_uCount = 0;
				}
			}
		}
	}

	DWORD Type = pEdit->m_Type;

	if( Type == typeReal ) {

		C3REAL Sign = fRaise ? +1.0 : -1.0;

		C3REAL Step = Sign * I2R(pEdit->m_Step);

		C3REAL Real = I2R(pEdit->m_Data);

		INT64 nMult = Power(GetAfter());

		INT64 nData = INT64(Real * nMult);

		C3REAL Clip = fRaise ? I2R(pEdit->m_Max) : I2R(pEdit->m_Min);

		C3REAL Done = fRaise ? I2R(pEdit->m_Min) : I2R(pEdit->m_Max);

		C3REAL Unit = Sign / nMult;

		INT64 nStep = INT64(Step * nMult);

		INT64 nClip = INT64(Round(Clip * nMult));

		for( ;;) {

			nData += nStep;

			if( fRaise ) {

				if( nData > nClip ) {

					while( !Validate(pEdit, R2I(Clip)) && Clip > Done ) {

						Clip -= Unit;
					}

					pEdit->m_Data = R2I(Clip);

					break;
				}
			}
			else {
				if( nData < nClip ) {

					while( !Validate(pEdit, R2I(Clip)) && Clip < Done ) {

						Clip -= Unit;
					}

					pEdit->m_Data = R2I(Clip);

					break;
				}
			}

			C3REAL Test = double(nData) / nMult;

			if( Validate(pEdit, R2I(Test)) ) {

				pEdit->m_Data = R2I(Test);

				break;
			}
		}
	}

	if( Type == typeInteger ) {

		INT   Sign = fRaise ? +1 : -1;

		C3INT Data = C3INT(pEdit->m_Data);

		C3INT Step = Sign * C3INT(pEdit->m_Step);

		C3INT Done = fRaise ? C3INT(pEdit->m_Min) : C3INT(pEdit->m_Max);

		C3INT Clip = fRaise ? C3INT(pEdit->m_Max) : C3INT(pEdit->m_Min);

		for( ;;) {

			Data += Step;

			BOOL fRamp = fRaise ? (Data >= Clip) : (Data <= Clip);

			if( fRamp ) {

				while( !Validate(pEdit, Clip) && Clip != Done ) {

					Clip -= Sign;
				}

				pEdit->m_Data = DWORD(Clip);

				break;
			}

			if( Validate(pEdit, Data) ) {

				pEdit->m_Data = DWORD(Data);

				break;
			}
		}
	}

	pEdit->m_Edit     = Format(pEdit->m_Data, Type, fmtBare);

	pEdit->m_uCursor  = cursorAll;

	pEdit->m_fDefault = FALSE;

	if( pEdit->m_uFlags & flagTwoNumeric ) {

		return editUpdate;
	}

	if( pEdit->m_pValue->SetValue(DWORD(pEdit->m_Data), Type, setNone) ) {

		return editUpdate;
	}

	return editNone;
}

UINT CDispFormatNumber::OnEditEnter(CEditCtx *pEdit)
{
	if( !pEdit->m_fDefault ) {

		double Read = 0;

		if( pEdit->m_Edit.GetLength() ) {

			CString Edit = UniConvert(pEdit->m_Edit);

			UINT	Base = GetRadix();

			if( Base == 10 ) {

				Read = strtod(Edit, NULL);
			}
			else
				Read = strtoul(Edit, NULL, Base);
		}

		if( pEdit->m_Type == typeReal ) {

			C3REAL Data = C3REAL(Read);

			C3REAL Min  = I2R(pEdit->m_Min);

			C3REAL Max  = I2R(pEdit->m_Max);

			if( Data >= Min && Data <= Max ) {

				if( Validate(pEdit, R2I(Data)) ) {

					if( pEdit->m_pValue->SetValue(R2I(Data), typeReal, setNone) ) {

						return editCommit;
					}

					return editUpdate;
				}
			}
		}
		else {
			if( GetRadix() == 10 ) {

				C3INT Sign = (Read < 0) ? -1 : +1;

				C3INT Data = C3INT(fabs(Read) * Power(GetAfter()) + 0.5) * Sign;

				C3INT Min  = C3INT(pEdit->m_Min);

				C3INT Max  = C3INT(pEdit->m_Max);

				if( Data >= Min && Data <= Max ) {

					if( Validate(pEdit, DWORD(Data)) ) {

						if( pEdit->m_pValue->SetValue(DWORD(Data), typeInteger, setNone) ) {

							return editCommit;
						}

						return editUpdate;
					}
				}
			}
			else {
				DWORD Data = DWORD(Read * Power(GetAfter()));

				DWORD Min  = DWORD(pEdit->m_Min);

				DWORD Max  = DWORD(pEdit->m_Max);

				if( Data >= Min && Data <= Max ) {

					if( Validate(pEdit, DWORD(Data)) ) {

						if( pEdit->m_pValue->SetValue(DWORD(Data), typeInteger, setNone) ) {

							return editCommit;
						}

						return editUpdate;
					}
				}
			}
		}

		pEdit->m_uCursor = cursorError;

		pEdit->m_fError  = TRUE;

		return editError;
	}

	return editAbort;
}

UINT CDispFormatNumber::OnEditClose(CEditCtx *pEdit)
{
	if( pEdit->m_fDefault ) {

		return editAbort;
	}

	pEdit->m_uCursor  = cursorAll;

	pEdit->m_fDefault = TRUE;

	pEdit->m_fError   = FALSE;

	return editUpdate;
}

UINT CDispFormatNumber::OnEditDelete(CEditCtx *pEdit)
{
	if( pEdit->m_fDefault || pEdit->m_fError ) {

		pEdit->m_Edit.Empty();

		pEdit->m_uCursor  = cursorLast;

		pEdit->m_fDefault = FALSE;

		pEdit->m_fError   = FALSE;

		return editUpdate;
	}
	else {
		while( !pEdit->m_Edit.IsEmpty() ) {

			UINT uLength  = pEdit->m_Edit.GetLength();

			pEdit->m_Edit = pEdit->m_Edit.Left(uLength - 1);

			if( pEdit->m_Edit.Right(1) == L"-" ) {

				continue;
			}

			pEdit->m_uCursor = cursorLast;

			return editUpdate;
		}
	}

	return editNone;
}

UINT CDispFormatNumber::OnEditChar(CEditCtx *pEdit, UINT uCode)
{
	if( uCode == '.' ) {

		if( GetAfter() == 0 ) {

			return editNone;
		}
	}

	if( uCode == '+' || uCode == '-' ) {

		if( m_Signed == 0 ) {

			return editNone;
		}
	}

	if( pEdit->m_uCursor == cursorError || pEdit->m_uCursor == cursorAll ) {

		pEdit->m_Edit.Empty();

		pEdit->m_uCursor  = cursorLast;

		pEdit->m_fDefault = FALSE;

		pEdit->m_fError   = FALSE;
	}

	if( uCode == '+' || uCode == '-' ) {

		if( pEdit->m_Edit[0] == '-' ) {

			pEdit->m_Edit.Delete(0, 1);

			return editUpdate;
		}
		else {
			if( pEdit->m_Edit.GetLength() < 16 ) {

				pEdit->m_Edit.Insert(0, '-');

				return editUpdate;
			}
		}
	}

	if( TRUE ) {

		UINT uPos = pEdit->m_Edit.Find('.');

		UINT uLen = pEdit->m_Edit.GetLength();

		if( uCode == '.' ) {

			if( uPos < NOTHING ) {

				return editNone;
			}

			if( pEdit->m_Edit.IsEmpty() ) {

				pEdit->m_Edit = L"0";
			}

			pEdit->m_Edit += WCHAR(uCode);

			return editUpdate;
		}

		if( wtoupper(uCode) >= '0' && wtoupper(uCode) <= "0123456789ABCDEF"[GetRadix() - 1] ) {

			if( uPos == NOTHING ) {

				if( pEdit->m_Edit[0] == '-' ) {

					uLen--;
				}

				if( uLen < m_Before ) {

					pEdit->m_Edit += WCHAR(uCode);

					return editUpdate;
				}

				return editNone;
			}

			if( uLen - uPos - 1 < GetAfter() ) {

				pEdit->m_Edit += wtoupper(WCHAR(uCode));

				return editUpdate;
			}
		}
	}

	return editNone;
}

double CDispFormatNumber::Round(double rData)
{
	// REV3 -- Make rounding optional?

	if( rData ) {

		if( rData > 0 ) {

			return floor(rData + 0.5);
		}
		else
			return ceil(rData - 0.5);
	}

	return 0;
}

// End of File
