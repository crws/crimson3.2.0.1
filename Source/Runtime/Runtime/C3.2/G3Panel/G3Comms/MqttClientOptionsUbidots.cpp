
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsUbidots.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client Options
//

// Constructor

CMqttClientOptionsUbidots::CMqttClientOptionsUbidots(void)
{
}

// Initialization

void CMqttClientOptionsUbidots::Load(PCBYTE &pData)
{
	CMqttClientOptionsJson::Load(pData);

	GetCoded(pData, m_Token);

	GetCoded(pData, m_Device);

	m_Root = 2;
}

// Config Fixup

BOOL CMqttClientOptionsUbidots::FixConfig(void)
{
	BOOL fFix = CMqttClientOptionsJson::FixConfig();

	FixCoded(m_Token, FALSE);

	FixCoded(m_Device, FALSE);

	m_PubTopic = "/v1.6/devices/" + m_Device.ToLower();

	m_SubTopic = "/v1.6/devices/" + m_Device.ToLower() + "/+/lv";

	m_UserName = m_Token;

	return fFix;
}

// Attributes

CString CMqttClientOptionsUbidots::GetExtra(void) const
{
	CString Extra;

	Extra += m_PubTopic + '&';
	
	Extra += m_SubTopic;

	return Extra;
}

// End of File
