
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsSparkplug.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for Sparkplug Options
//

// Constructor

CMqttClientOptionsSparkplug::CMqttClientOptionsSparkplug(void)
{
	m_fReboot    = FALSE;

	m_GroupId    = "Aeon Devices";

	m_NodeId     = "AeonNode1";

	m_TagsFolder = "Tags";
}

// Initialization

void CMqttClientOptionsSparkplug::Load(PCBYTE &pData)
{
	CMqttClientOptionsCrimson::Load(pData);

	m_fReboot = GetByte(pData) ? TRUE : FALSE;

	GetCoded(pData, m_GroupId);

	GetCoded(pData, m_NodeId);

	GetCoded(pData, m_TagsFolder);

	GetCoded(pData, m_PriAppId);

	m_Reconn = 1;

	m_SubQos = 0;
}

// Config Fixup

BOOL CMqttClientOptionsSparkplug::FixConfig(void)
{
	if( FixCoded(m_GroupId, TRUE) && FixCoded(m_NodeId, TRUE) ) {

		FixCoded(m_TagsFolder, FALSE);

		FixCoded(m_PriAppId, FALSE);

		if( CMqttClientOptionsCrimson::FixConfig() ) {

			MakeClientId();

			return TRUE;
		}

	}

	return FALSE;
}

// Attributes

CString CMqttClientOptionsSparkplug::GetExtra(void) const
{
	CString Extra;

	Extra += m_GroupId  + '&';
	Extra += m_NodeId   + '&';
	Extra += m_ClientId;

	return Extra;
}

// Implementation

BOOL CMqttClientOptionsSparkplug::MakeClientId(void)
{
	if( m_ClientId.IsEmpty() ) {

		g_pPxe->GetHostName(m_ClientId);

		return TRUE;
	}

	return FALSE;
}

// End of File
