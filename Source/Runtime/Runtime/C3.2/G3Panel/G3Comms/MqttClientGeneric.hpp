
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClientGeneric_HPP

#define	INCLUDE_MqttClientGeneric_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCloudServiceGeneric;

class CMqttClientOptionsGeneric;

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client
//

class CMqttClientGeneric : public CMqttClientJson
{
public:
	// Constructor
	CMqttClientGeneric(CCloudServiceGeneric *pService, CMqttClientOptionsGeneric &Opts);

	// Operations
	BOOL Open(void);

protected:
	// Sub Topics
	enum
	{
		subData = 0,
	};

	// Options
	CMqttClientOptionsGeneric &m_Opts;

	// Client Hooks
	void OnClientPhaseChange(void);
	BOOL OnClientNewData(CMqttMessage const *pMsg);

	// Publish Hook
	BOOL GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg);
};

// End of File

#endif
