
#include "Intern.hpp"

#include "Service.hpp"

#include "Matrix.hpp"

#include "MqttClientSparkplug.hpp"

#include <sys/time.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudServiceSparkplug.hpp"

#include "MqttClientOptionsSparkplug.hpp"

#include "CloudDeviceDataSet.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for Sparkplug
//

// Constructor

CMqttClientSparkplug::CMqttClientSparkplug(CCloudServiceSparkplug *pService, CMqttClientOptionsSparkplug &Opts) : CMqttClientCrimson(pService, Opts), m_Opts(Opts)
{
	m_BdSeq  = GetTickCount();

	m_BdTime = m_uMilli;

	m_uLast  = m_uSecs;
}

// Operations

BOOL CMqttClientSparkplug::Open(void)
{
	m_Will.m_Topic = "spBv1.0/" + m_Opts.m_GroupId + "/NDEATH/" + m_Opts.m_NodeId;

	MakeDeathCert(m_Will.m_Data);

	return CMqttClientCrimson::Open();
}

BOOL CMqttClientSparkplug::Poll(UINT uID)
{
	if( uID == 0 ) {

		if( m_uLast - m_uSecs ) {

			BOOL fBusy = CMqttClientCrimson::Poll(uID);

			if( m_uOnline && !--m_uOnline ) {

				AbortAndStep();

				fBusy = TRUE;
			}

			m_uLast = m_uSecs;

			return fBusy;
		}
	}

	return CMqttClientCrimson::Poll(uID);
}

// Client Hooks

void CMqttClientSparkplug::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseInitial ) {

		UINT c = m_pService->GetSetCount();

		for( UINT s = 1; s < c; s++ ) {

			CCloudTagSet *pTag = (CCloudTagSet *) m_pService->GetDataSet(s);

			pTag->m_Props      = 0;
		}

		AddToSubList(subNCmd, "spBv1.0/" + m_Opts.m_GroupId + "/NCMD/" + m_Opts.m_NodeId);

		AddToSubList(subState, "STATE/#");
	}

	if( m_uPhase == phaseConnected ) {

		m_pService->SetStatus(2);
	}

	if( m_uPhase == phasePublishing ) {

		if( m_Opts.m_PriAppId.IsEmpty() ) {

			GoOnline();
		}
		else {
			if( m_Opts.HasAlternatePeer() ) {

				m_uOnline = 3 * m_Opts.m_uKeepAlive / 4;
			}
			else
				m_uOnline = 0;

			m_pService->SetStatus(3);
		}
	}

	CMqttClientCrimson::OnClientPhaseChange();
}

void CMqttClientSparkplug::OnClientPublish(CByteArray const &Blob)
{
	CGpbEncoder ndata;

	ndata.AddFieldAsUInt64(propMsgTimestamp, m_uMilli);

	ndata.AddFieldAsUInt64(propMsgSeq, m_bSeq++);

	AddPayload(ndata.GetData());

	AddPayload(Blob);
}

BOOL CMqttClientSparkplug::OnClientNewData(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode == subState ) {

		OnAppState(pMsg->m_Topic, pMsg->m_Data);

		return TRUE;
	}

	if( pMsg->m_uCode == subNCmd ) {

		OnNodeCommand(pMsg->m_Data);

		return TRUE;
	}

	return CMqttClientCrimson::OnClientNewData(pMsg);
}

BOOL CMqttClientSparkplug::OnClientGetData(CMqttMessage * &pMsg)
{
	if( m_fOnline ) {

		if( !m_fBorn ) {

			pMsg   = New CMqttMessage;

			m_bSeq = 0;

			pMsg->SetCode(pubNBirth);

			pMsg->SetTopic("spBv1.0/" + m_Opts.m_GroupId + "/NBIRTH/" + m_Opts.m_NodeId);

			MakeBirthCert(pMsg->m_Data);

			return TRUE;
		}

		return CMqttClientCrimson::OnClientGetData(pMsg);
	}

	return FALSE;
}

BOOL CMqttClientSparkplug::OnClientDataSent(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode == pubNBirth ) {

		m_fBorn = TRUE;
	}

	if( pMsg->m_uCode < m_PubList.GetCount() ) {

		ShowSentData(pMsg);
	}

	return CMqttClientCrimson::OnClientDataSent(pMsg);
}

// Publish Hook

BOOL CMqttClientSparkplug::GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg)
{
	CGpbEncoder ndata;

	if( uMode == CCloudDataSet::modeDelta ) {

		uMode = m_Opts.m_Mode;
	}

	if( AddData(ndata, uTopic, uTime, uMode) ) {

		pMsg = New CMqttMessage;

		ndata.Encode(pMsg->m_Data);

		pMsg->SetTopic("spBv1.0/" + m_Opts.m_GroupId + "/NDATA/" + m_Opts.m_NodeId);

		return TRUE;
	}

	return FALSE;
}

// Message Hook

BOOL CMqttClientSparkplug::OnMakeHistoric(CMqttMessage *pMsg)
{
	if( pMsg->m_uCode < m_PubList.GetCount() ) {

		CPub const &Pub = m_PubList[pMsg->m_uCode];

		if( Pub.m_fHistory ) {

			CGpbDecoder dmsg;

			CGpbEncoder emsg;

			dmsg.Decode(pMsg->m_Data);

			for( UINT m = 0;; m++ ) {

				CByteArray Data;

				if( dmsg.GetMessage(Data, propMsgMetrics, m) ) {

					CGpbDecoder dtag;

					dtag.Decode(Data);

					if( !dtag.IsFieldPresent(propMetricIsHistorical) ) {

						CGpbEncoder etag;

						etag.AddFieldAsBool(propMetricIsHistorical, true);

						Data.Append(etag.GetData());
					}

					emsg.AddMessage(propMsgMetrics, Data);

					continue;
				}

				break;
			}

			pMsg->SetData(emsg.GetData());

			return TRUE;
		}
	}

	return FALSE;
}

// State Handler

BOOL CMqttClientSparkplug::OnAppState(CString Topic, CByteArray const &Blob)
{
	if( !m_Opts.m_PriAppId.IsEmpty() ) {

		if( Topic == "STATE/" + m_Opts.m_PriAppId ) {

			if( Blob.GetCount() == 6 ) {

				if( !strncasecmp(PCTXT(Blob.GetPointer()), "ONLINE", 6) ) {

					GoOnline();

					return TRUE;
				}
			}

			if( Blob.GetCount() == 7 ) {

				if( !strncasecmp(PCTXT(Blob.GetPointer()), "OFFLINE", 7) ) {

					if( m_Opts.HasAlternatePeer() ) {

						AbortAndStep();
					}
					else {
						GoOffline();

						SetPhase(phaseConnected);
					}

					return TRUE;
				}
			}
		}
	}

	return TRUE;
}

// Command Handler

BOOL CMqttClientSparkplug::OnNodeCommand(CByteArray const &Blob)
{
	CGpbDecoder cmd;

	cmd.Decode(Blob);

	if( cmd.IsFieldPresent(propMsgMetrics) ) {

		UINT c = cmd.GetFieldCount(propMsgMetrics);

		for( UINT n = 0; n < c; n++ ) {

			CGpbDecoder metric;

			if( cmd.GetMessage(metric, propMsgMetrics, n) ) {

				UINT uAlias = UINT(metric.GetFieldAsUInt64(propMetricAlias, 0, NOTHING));

				UINT uSet   = NOTHING;

				UINT uTag   = NOTHING;

				if( uAlias == NOTHING ) {

					CString Name = metric.GetFieldAsString(propMetricName, 0, "");

					if( !Name.IsEmpty() ) {

						CString Type = Name.StripToken('/');

						if( Type == "Node Control" ) {

							if( Name == "Rebirth" ) {

								AfxTrace("rebirth requested\n");

								m_fBorn = FALSE;

								return TRUE;
							}

							if( Name == "Next Server" ) {

								AfxTrace("next server requested\n");

								if( m_Opts.HasAlternatePeer() ) {

									AbortAndStep();
								}

								return TRUE;
							}

							if( Name == "Reboot" ) {

								AfxTrace("reboot requested\n");

								if( m_Opts.m_fReboot ) {

									Abort();

									Sleep(1000);

									g_pPxe->RestartSystem(0, 55);

									for( ;;) Sleep(FOREVER);
								}

								return TRUE;
							}

							return TRUE;
						}

						if( Type == m_Opts.m_TagsFolder ) {

							Name.Replace('/', '.');

							UINT c = m_pService->GetSetCount();

							for( UINT p = 0; p < m_PubList.GetCount(); p++ ) {

								UINT s = m_PubList[p].m_uTopic;

								if( s >= 1 && s < c ) {

									CCloudTagSet *pSet = (CCloudTagSet *) m_pService->GetDataSet(s);

									if( (uTag = pSet->FindTag(Name)) < NOTHING ) {

										uSet = s;

										break;
									}
								}
							}
						}
					}
				}
				else {
					uSet = uAlias / 1000 - 1;

					uTag = uAlias % 1000 - 1;
				}

				if( uSet >= 1 && uSet < NOTHING ) {

					CCloudTagSet *pSet = (CCloudTagSet *) m_pService->GetDataSet(uSet);

					if( uTag < pSet->GetCount() ) {

						UINT uType = metric.GetFieldAsUInt32(propMetricDataType, 0, 0);

						if( uType == sptInt32 ) {

							C3INT n = metric.GetFieldAsInt32(propMetricIntValue, 0, 0);

							pSet->SetData(uTag, DWORD(n), typeInteger);
						}

						if( uType == sptFloat ) {

							C3REAL r = metric.GetFieldAsFloat(propMetricFloatValue, 0, 0);

							pSet->SetData(uTag, R2I(r), typeReal);
						}

						if( uType == sptString ) {

							CUnicode t = metric.GetFieldAsUnicode(propMetricStringValue, 0, L"");

							PUTF     p = wstrdup(t);

							pSet->SetData(uTag, DWORD(p), typeString);
						}

						ForceUpdate(GetTopicCode(uSet));
					}
				}
			}
		}
	}

	return TRUE;
}

// Message Building

BOOL CMqttClientSparkplug::MakeBirthCert(CByteArray &Blob)
{
	CGpbEncoder nbirth;

	nbirth.AddFieldAsString(propMsgUuid, "UUID");

	AddBdSeq(nbirth);

	AddProps(nbirth);

	AddCtrls(nbirth);

	UINT c = m_pService->GetSetCount();

	for( UINT n = 0; n < c; n++ ) {

		AddData(nbirth, n, m_uMilli, CCloudDataSet::modeBirth);
	}

	nbirth.Encode(Blob);

	return TRUE;
}

BOOL CMqttClientSparkplug::MakeDeathCert(CByteArray &Blob)
{
	CGpbEncoder ndeath;

	AddBdSeq(ndeath);

	ndeath.Encode(Blob);

	return TRUE;
}

BOOL CMqttClientSparkplug::AddBdSeq(CGpbEncoder &msg)
{
	CGpbEncoder bdseq;

	bdseq.AddFieldAsString(propMetricName, "bdSeq");

	bdseq.AddFieldAsUInt64(propMetricAlias, 100000);

	bdseq.AddFieldAsUInt64(propMetricTimestamp, m_BdTime);

	bdseq.AddFieldAsUInt32(propMetricDataType, sptUInt64);

	bdseq.AddFieldAsUInt64(propMetricLongValue, m_BdSeq);

	msg.AddMessage(propMsgMetrics, bdseq);

	return TRUE;
}

BOOL CMqttClientSparkplug::AddProps(CGpbEncoder &msg)
{
	AddProp(msg, "Hardware Make", "Red Lion Controls");

	AddProp(msg, "Hardware Model", "Crimson Device");

	AddProp(msg, "OS", "Crimson");

	AddProp(msg, "OS Version", "3.20");

	return TRUE;
}

BOOL CMqttClientSparkplug::AddCtrls(CGpbEncoder &msg)
{
	if( m_Opts.m_fReboot ) {

		AddCtrl(msg, "Reboot");
	}

	AddCtrl(msg, "Rebirth");

	AddCtrl(msg, "Next Server");

	return TRUE;
}

BOOL CMqttClientSparkplug::AddProp(CGpbEncoder &msg, CString Name, CString Value)
{
	CGpbEncoder prop;

	prop.AddFieldAsString(propMetricName, "Properties/" + Name);

	prop.AddFieldAsUInt64(propMetricTimestamp, m_uMilli);

	prop.AddFieldAsUInt32(propMetricDataType, sptString);

	prop.AddFieldAsString(propMetricStringValue, Value);

	msg.AddMessage(propMsgMetrics, prop);

	return TRUE;
}

BOOL CMqttClientSparkplug::AddCtrl(CGpbEncoder &msg, CString Name)
{
	CGpbEncoder prop;

	prop.AddFieldAsString(propMetricName, "Node Control/" + Name);

	prop.AddFieldAsUInt32(propMetricDataType, sptBoolean);

	prop.AddFieldAsBool(propMetricBooleanValue, false);

	msg.AddMessage(propMsgMetrics, prop);

	return TRUE;
}

BOOL CMqttClientSparkplug::AddData(CGpbEncoder &msg, UINT uTopic, UINT64 uTime, UINT uMode)
{
	CCloudDataSet *pSet = m_pService->GetDataSet(uTopic);

	if( pSet->IsTriggered() || (pSet->IsEnabled() && uMode == CCloudDataSet::modeBirth) ) {

		UINT uAlias = 1000 * (uTopic + 1);

		if( uTopic == pubDevice ) {

			return AddData(msg, pSet, "Device/", uAlias, uTime, uMode);
		}
		else {
			CCloudTagSet *pTag = (CCloudTagSet *) pSet;

			CString       Root = m_Opts.m_TagsFolder + '/';

			if( pTag->HasLabel() ) {

				Root += pTag->GetLabel(FALSE);

				Root += '/';
			}

			return AddData(msg, pSet, Root, uAlias, uTime, uMode);
		}
	}

	return FALSE;
}

BOOL CMqttClientSparkplug::AddData(CGpbEncoder &msg, CCloudDataSet *pSet, CString Root, UINT uAlias, UINT64 uTime, UINT uMode)
{
	UINT i = msg.GetSize();

	UINT c = pSet->GetCount();

	BOOL f = FALSE;

	for( UINT n = 0; n < c; n++ ) {

		CString Name;

		DWORD   Data;

		UINT    Type;

		BOOL    Free;

		if( pSet->GetData(n, Name, Data, Type, Free, uMode) ) {

			CGpbEncoder tag;

			if( uMode == CCloudDataSet::modeIfAny ) {

				msg.Reset(i);

				uMode = CCloudDataSet::modeForce;

				n     = NOTHING;

				continue;
			}

			if( uMode == CCloudDataSet::modeBirth ) {

				Name.Replace('.', '/');

				tag.AddFieldAsString(propMetricName, Root + Name);
			}

			if( Type == typeInteger ) {

				tag.AddFieldAsUInt32(propMetricDataType, sptInt32);

				tag.AddFieldAsUInt32(propMetricIntValue, Data);
			}

			if( Type == typeReal ) {

				tag.AddFieldAsUInt32(propMetricDataType, sptFloat);

				tag.AddFieldAsFloat(propMetricFloatValue, I2R(Data));
			}

			if( Type == typeString ) {

				tag.AddFieldAsUInt32(propMetricDataType, sptString);

				tag.AddFieldAsUnicode(propMetricStringValue, PCUTF(Data));
			}

			if( Free ) {

				free(PVOID(Data));
			}

			if( uMode == CCloudDataSet::modeBirth ) {

				CGpbEncoder set;

				AddProp(set, pSet, n, tpDescription, "tooltip");

				if( Type != typeString ) {

					AddProp(set, pSet, n, tpUnits, "engUnit");

					AddProp(set, pSet, n, tpMinimum, "engLow");

					AddProp(set, pSet, n, tpMaximum, "engHigh");
				}

				if( set.GetSize() ) {

					tag.AddMessage(propMetricPropertySet, set);
				}
			}
			else {
				if( !IsLive() ) {

					tag.AddFieldAsBool(propMetricIsHistorical, true);
				}
			}

			tag.AddFieldAsUInt64(propMetricAlias, uAlias + n + 1);

			tag.AddFieldAsUInt64(propMetricTimestamp, uTime);

			msg.AddMessage(propMsgMetrics, tag);

			f = TRUE;
		}
	}

	return f;
}

BOOL CMqttClientSparkplug::AddProp(CGpbEncoder &set, CCloudDataSet *pSet, UINT uIndex, UINT uProp, CString Prop)
{
	CString Name;

	DWORD   Data;

	UINT    Type;

	if( pSet->GetProp(uIndex, uProp, Name, Data, Type) ) {

		CGpbEncoder value;

		if( Type == typeInteger ) {

			value.AddFieldAsUInt32(propPropValueType, sptInt32);

			value.AddFieldAsUInt32(propPropValueIntValue, Data);
		}

		if( Type == typeReal ) {

			value.AddFieldAsUInt32(propPropValueType, sptFloat);

			value.AddFieldAsFloat(propPropValueFloatValue, I2R(Data));
		}

		if( Type == typeString ) {

			value.AddFieldAsUInt32(propPropValueType, sptString);

			value.AddFieldAsUnicode(propPropValueStringValue, PCUTF(Data));

			free(PTXT(Data));
		}

		set.AddFieldAsString(propPropSetKey, Prop);

		set.AddMessage(propPropSetValue, value);

		return TRUE;
	}

	return FALSE;
}

// Implementation

void CMqttClientSparkplug::GoOnline(void)
{
	m_Opts.SetGoodPeer();

	m_fBorn   = FALSE;

	m_uOnline = 0;

	m_pService->SetStatus(4);

	CMqttClientCrimson::GoOnline();
}

void CMqttClientSparkplug::ShowSentData(CMqttMessage const *pMsg)
{
	#if 0

	CString Text;

	Text.AppendPrintf("%u", pMsg->m_uCode);

	CGpbDecoder dmsg;

	dmsg.Decode(pMsg->m_Data);

	for( UINT m = 0;; m++ ) {

		CByteArray Data;

		if( dmsg.GetMessage(Data, propMsgMetrics, m) ) {

			CGpbDecoder dtag;

			dtag.Decode(Data);

			if( dtag.IsFieldPresent(propMetricTimestamp) ) {

				UINT64 t = dtag.GetFieldAsUInt64(propMetricTimestamp, 0, 0);

				UINT64 k = 1000;

				struct timeval tv;

				tv.tv_sec  = UINT(t / k);

				tv.tv_usec = UINT(t % k) * 1000;

				Text.AppendPrintf(", time=%s", PCTXT(CHttpTime::Format(2, &tv)));

				if( dtag.IsFieldPresent(propMetricIsHistorical) ) {

					if( dtag.GetFieldAsBool(propMetricIsHistorical, 0, false) ) {

						Text.AppendPrintf(", historic=true");
					}
					else
						Text.AppendPrintf(", historic=false");
				}
			}
		}

		break;
	}

	AfxTrace("%s\n", PCTXT(Text));

	#endif
}

// End of File
