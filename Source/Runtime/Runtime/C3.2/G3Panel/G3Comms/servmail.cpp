
#include "intern.hpp"

#include "servmail.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mail Manager Service
//

// Instantiator

IService * Create_ServiceMail(void)
{
	return New CMailManager;
}

// Constructor

CMailManager::CMailManager(void)
{
	m_pLock	     = Create_Mutex();
	m_pContacts  = New CMailContacts;
	m_pPanelName = NULL;
	m_DateForm   = 0;
	m_pRelay     = NULL;
	m_pOnSMS     = NULL;
	m_uRecv	     = 0;
	m_XpNames[0] = "msg.smtp";
	m_XpNames[1] = "msg.sms";
	m_XpNames[2] = "msg.ftp";

	memset(m_uSend, 0, sizeof(m_uSend));

	g_pServiceMail = this;

	RegisterWithBroker();
}

// Destructor

CMailManager::~CMailManager(void)
{
	g_pServiceMail = NULL;

	RevokeFromBroker();

	for( UINT t = 0; t < 3; t++ ) {

		if( m_uSend[t] ) {

			AfxGetAutoObject(pTrans, m_XpNames[t], 0, IMsgTransport);

			if( pTrans ) {

				pTrans->RevokeSendNotify(m_uSend[t]);
			}
		}
	}

	delete m_pContacts;
	delete m_pPanelName;
	delete m_pRelay;
	delete m_pOnSMS;

	AfxRelease(m_pLock);
}

// Initialization

void CMailManager::Load(PCBYTE &pData)
{
	ValidateLoad("CMailManager", pData);

	m_pContacts->Load(pData);

	GetCoded(pData, m_pPanelName);

	m_DateForm = GetByte(pData);

	GetCoded(pData, m_pRelay);

	GetCoded(pData, m_pOnSMS);
}

// IUnknown

HRM CMailManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IMsgSendNotify);

	return CServiceItem::QueryInterface(riid, ppObject);
}

ULM CMailManager::AddRef(void)
{
	return CServiceItem::AddRef();
}

ULM CMailManager::Release(void)
{
	return CServiceItem::Release();
}

// Service ID

UINT CMailManager::GetID(void)
{
	return 3;
}

// IMailService

BOOL CMailManager::SendFile(UINT uIndex, CFilename Name, PCTXT pSubject, DWORD dwFlags)
{
	if( !Name.IsEmpty() ) {

		CMailInfo Info;

		Info.m_File    = Name;

		Info.m_dwDone  = 0;

		Info.m_dwAck   = 0;

		Info.m_Subject = pSubject;

		Info.m_Message.Printf("This email contains the following attachments:\r\n\r\n%s\r\n", PCTXT(Name));

		return SendMail(uIndex, Info);
	}

	return FALSE;
}

BOOL CMailManager::SendFile(UINT uIndex, CFilename Name)
{
	if( !Name.IsEmpty() ) {

		CMailInfo Info;

		Info.m_File    = Name;

		Info.m_dwDone  = 0;

		Info.m_dwAck   = 0;

		Info.m_Subject.Printf("Emailing: %s", PCTXT(Name));

		Info.m_Message.Printf("This email contains the following attachments:\r\n\r\n%s\r\n", PCTXT(Name));

		return SendMail(uIndex, Info);
	}

	return FALSE;
}

BOOL CMailManager::SendFile(CString Rcpt, CFilename Name)
{
	if( !Name.IsEmpty() ) {

		CMailInfo Info;

		Info.m_File    = Name;

		Info.m_dwDone  = 0;

		Info.m_dwAck   = 0;

		Info.m_Subject.Printf("Emailing: %s", PCTXT(Name));

		Info.m_Message.Printf("This email contains the following attachments:\r\n\r\n%s\r\n", PCTXT(Name));

		return SendMail(Rcpt, Info);
	}

	return FALSE;
}

BOOL CMailManager::SendFile(CString Rcpt, CFilename Name, DWORD dwAck)
{
	if( !Name.IsEmpty() ) {

		CMailInfo Info;

		Info.m_File    = Name;

		Info.m_dwDone  = 0;

		Info.m_dwAck   = dwAck;

		Info.m_Subject.Printf("Emailing: %s", PCTXT(Name));

		Info.m_Message.Printf("This email contains the following attachments:\r\n\r\n%s\r\n", PCTXT(Name));

		return SendMail(Rcpt, Info);
	}

	return FALSE;
}

BOOL CMailManager::SendMail(UINT uIndex, PCTXT pSubject, PCTXT pMessage)
{
	CMailInfo Info;

	Info.m_dwDone  = 0;

	Info.m_dwAck   = 0;

	Info.m_Subject = pSubject;

	Info.m_Message = pMessage;

	return SendMail(uIndex, Info);
}

BOOL CMailManager::SendMail(UINT uIndex, PCTXT pSubject, PCTXT pMessage, DWORD dwSkip)
{
	CMailInfo Info;

	Info.m_dwDone  = dwSkip;

	Info.m_dwAck   = 0;

	Info.m_Subject = pSubject;

	Info.m_Message = pMessage;

	return SendMail(uIndex, Info);
}

BOOL CMailManager::SendMail(UINT uIndex, PCTXT pType, DWORD dwTime, CUnicode const &Text)
{
	FindConfig(m_Cfg);

	CDispFormatTimeDate Format;

	Format.m_Mode     = 3;

	Format.m_Secs     = 1;

	Format.m_DateForm = m_DateForm;

	CUnicode Time = Format.Format(dwTime,
				      typeInteger,
				      fmtStd
	);

	CPrintf  Head = CPrintf("%s - %s",
				PCTXT(m_Cfg.m_PanelName),
				PCTXT(UniConvert(Text))
	);

	CPrintf  Body = CPrintf("%s at %s - %s",
				pType,
				PCTXT(UniConvert(Time)),
				PCTXT(UniConvert(Text))
	);

	return SendMail(uIndex, Head, Body);
}

BOOL CMailManager::SendMail(CString Rcpt, PCTXT pSubject, PCTXT pMessage)
{
	CMailInfo Info;

	Info.m_dwDone  = 0;

	Info.m_dwAck   = 0;

	Info.m_Subject = pSubject;

	Info.m_Message = pMessage;

	return SendMail(Rcpt, Info);
}

BOOL CMailManager::SendMail(CString Rcpt, PCTXT pSubject, PCTXT pMessage, DWORD dwAck)
{
	CMailInfo Info;

	Info.m_dwDone  = 0;

	Info.m_dwAck   = dwAck;

	Info.m_Subject = pSubject;

	Info.m_Message = pMessage;

	return SendMail(Rcpt, Info);
}

// IMsgSendNotify

void CMailManager::OnMsgSent(CMsgPayload const *pMessage, BOOL fOkay)
{
	if( fOkay ) {

		DWORD dwAck  = pMessage->m_Param[0];

		DWORD dwMask = (1<<pMessage->m_Param[1]);

		SetAckMask(dwAck, dwMask);
	}
}

// IMsgRecvNotify

BOOL CMailManager::OnMsgRecv(PCTXT pName, CMsgPayload const *pMessage)
{
	if( !strcmp(pName, "sms") ) {

		if( m_pOnSMS || m_Cfg.m_uRelay ) {

			FindConfig(m_Cfg);

			if( m_pOnSMS ) {

				if( m_pOnSMS->IsAvail() ) {

					CString Data;

					Data += pMessage->m_OrigAddr;

					Data += ':';

					Data += pMessage->m_Message;

					DWORD dwParam[1];

					dwParam[0] = DWORD(wstrdup(UniConvert(Data)));

					SetTaskLimit(25, 5);

					m_pOnSMS->Execute(typeVoid, dwParam);

					SetTaskLimit(0, 0);
				}
			}

			if( m_Cfg.m_uRelay ) {

				AfxGetAutoObject(pSms, "msg.sms", 0, IMsgTransport);

				if( pSms ) {

					CString Msg;

					Msg += (m_Cfg.m_uRelay == 2) ? pMessage->m_OrigAddr : m_Cfg.m_PanelName;

					Msg += " > ";

					Msg += pMessage->m_Message;

					for( UINT n = 0; n < m_pContacts->m_uCount; n++ ) {

						CMailAddress *pAddr = m_pContacts->m_ppAddr[n];

						if( pAddr ) {

							CStringArray List;

							if( pAddr->Parse(List) ) {

								UINT  c = List.GetCount();

								UINT  m = 0;

								DWORD d = 0;

								for( UINT r = 1; r < c; r++ ) {

									CString const &Name = List[r];

									if( pSms->CanAcceptAddress(Name) ) {

										if( Name == pMessage->m_OrigAddr ) {

											d |= (1 << (r-1));

											m += 1;
										}
									}
								}

								if( m > 0 && m < c ) {

									SendMail(n, "", Msg, d);
								}
							}
						}
					}
				}
			}

			return TRUE;
		}
	}

	return FALSE;
}

// Implementation

BOOL CMailManager::SendMail(UINT uIndex, CMailInfo &Info)
{
	if( uIndex < NOTHING ) {

		CMailAddress *pRcpt = m_pContacts->m_ppAddr[uIndex];

		if( pRcpt ) {

			if( pRcpt->Parse(Info.m_Rcpt) ) {

				return SendMail(Info);
			}
		}
	}

	return FALSE;
}

BOOL CMailManager::SendMail(CString Rcpt, CMailInfo &Info)
{
	if( !Rcpt.IsEmpty() ) {

		while( !Rcpt.IsEmpty() ) {

			UINT uPos = Rcpt.Find(';');

			if( uPos < NOTHING ) {

				CString Item = Rcpt.Left(uPos);

				Item.TrimBoth();

				if( !Item.IsEmpty() ) {

					Info.m_Rcpt.Append(Item);
				}

				Rcpt = Rcpt.Mid(uPos + 1);

				continue;
			}

			Rcpt.TrimBoth();

			if( !Rcpt.IsEmpty() ) {

				Info.m_Rcpt.Append(Rcpt);
			}

			break;
		}

		if( Info.m_Rcpt.GetCount() ) {

			Info.m_Rcpt.Insert(0, "");

			return SendMail(Info);
		}
	}

	return FALSE;
}

BOOL CMailManager::SendMail(CMailInfo &Info)
{
	if( Info.m_Rcpt.GetCount() >= 2 ) {

		SetAckData(Info.m_dwAck, Info.m_dwDone);

		UINT uCount = Info.m_Rcpt.GetCount() - 1;

		for( UINT n = 0; n < uCount; n++ ) {

			if( !(Info.m_dwDone & (1<<n)) ) {

				BOOL          fGone = FALSE;

				CString const &Name = Info.m_Rcpt[n+1];

				for( UINT t = 0; t < 3; t++ ) {

					AfxGetAutoObject(pTrans, m_XpNames[t], 0, IMsgTransport);

					if( pTrans ) {

						if( pTrans->CanAcceptAddress(Name) ) {

							m_uSend[t] = pTrans->RegisterSendNotify(m_uSend[t], this);

							CAutoPointer<CMsgPayload> pMessage(New CMsgPayload);

							pMessage->m_RcptName   = Info.m_Rcpt[0];
							pMessage->m_RcptAddr   = Info.m_Rcpt[n+1];
							pMessage->m_Subject    = Info.m_Subject;
							pMessage->m_Message    = Info.m_Message;
							pMessage->m_AttachFile = Info.m_File;
							pMessage->m_Param[0]   = Info.m_dwAck;
							pMessage->m_Param[1]   = n;
							pMessage->m_SendNotify = m_uSend[t];

							if( pTrans->QueueMessage(pMessage) ) {

								pMessage.TakeOver();

								fGone = TRUE;
							}

							break;
						}
					}
				}
			}
		}
	}

	return TRUE;
}

void CMailManager::FindConfig(CConfig &Cfg)
{
	CString UnitName;

	g_pPxe->GetUnitName(UnitName);

	Cfg.m_uRelay    = GetItemData(m_pRelay, C3INT(0));

	Cfg.m_PanelName = GetItemData(m_pPanelName, UnitName);
}

void CMailManager::SetAckData(DWORD dwAck, DWORD dwData)
{
	if( dwAck ) {

		CAutoLock Lock(m_pLock);

		IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

		pData->SetData(dwAck, typeInteger, setNone, dwData);
	}
}

void CMailManager::SetAckMask(DWORD dwAck, DWORD dwMask)
{
	if( dwAck ) {

		CAutoLock Lock(m_pLock);

		IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

		DWORD       dwData = pData->GetData(dwAck, typeInteger, getNone);

		pData->SetData(dwAck, typeInteger, setNone, dwData | dwMask);
	}
}

void CMailManager::RegisterWithBroker(void)
{
	AfxGetAutoObject(pBroker, "msg.broker", 0, IMsgBroker);

	if( pBroker ) {

		m_uRecv = pBroker->RegisterRecvNotify(this);
	}
}

void CMailManager::RevokeFromBroker(void)
{
	AfxGetAutoObject(pBroker, "msg.broker", 0, IMsgBroker);

	if( pBroker ) {

		pBroker->RevokeRecvNotify(m_uRecv);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Mail Address
//

// Constructor

CMailAddress::CMailAddress(void)
{
	m_pAddr   = NULL;

	m_pEnable = NULL;
}

// Destructor

CMailAddress::~CMailAddress(void)
{
	delete m_pAddr;

	delete m_pEnable;
}

// Initialization

void CMailAddress::Load(PCBYTE &pData)
{
	ValidateLoad("CMailAddress", pData);

	UINT  s = GetWord(pData) - 1;

	PCUTF p = PCUTF(pData);

	while( s-- ) {

		m_Name += GetWord(pData);
	}

	GetWord(pData);

	GetCoded(pData, m_pAddr);

	GetCoded(pData, m_pEnable);
}

// Operations

void CMailAddress::PreRegister(void)
{
	SetItemScan(m_pAddr, scanTrue);

	SetItemScan(m_pEnable, scanTrue);
}

BOOL CMailAddress::Parse(CStringArray &List)
{
	List.Empty();

	if( IsItemAvail(m_pEnable) ) {

		if( GetItemData(m_pEnable, TRUE) ) {

			List.Append(m_Name);

			if( m_pAddr ) {

				CString Addr = GetItemData(m_pAddr, "");

				while( !Addr.IsEmpty() ) {

					UINT uPos = Addr.Find(';');

					if( uPos < NOTHING ) {

						CString Item = Addr.Left(uPos);

						Item.TrimBoth();

						if( !Item.IsEmpty() ) {

							List.Append(Item);
						}

						Addr = Addr.Mid(uPos + 1);

						continue;
					}

					Addr.TrimBoth();

					if( !Addr.IsEmpty() ) {

						List.Append(Addr);
					}

					break;
				}

				return List.GetCount() > 1;
			}
		}
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//
// Mail Contacts
//

// Constructor

CMailContacts::CMailContacts(void)
{
	m_uCount = 0;

	m_ppAddr = NULL;
}

// Destructor

CMailContacts::~CMailContacts(void)
{
	while( m_uCount-- ) {

		delete m_ppAddr[m_uCount];
	}

	delete[] m_ppAddr;
}

// Initialization

void CMailContacts::Load(PCBYTE &pData)
{
	ValidateLoad("CMailContacts", pData);

	m_uCount = GetWord(pData);

	m_ppAddr = New CMailAddress *[m_uCount];

	for( UINT i = 0; i < m_uCount; i++ ) {

		CMailAddress *pAddr = NULL;

		if( GetByte(pData) ) {

			pAddr = New CMailAddress;

			pAddr->Load(pData);
		}

		m_ppAddr[i] = pAddr;
	}
}

// Operations

void CMailContacts::PreRegister(void)
{
	for( UINT i = 0; i < m_uCount; i++ ) {

		if( m_ppAddr[i] ) {

			m_ppAddr[i]->PreRegister();
		}
	}
}

// End of File
