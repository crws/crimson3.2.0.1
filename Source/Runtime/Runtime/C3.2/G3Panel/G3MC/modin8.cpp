
#include "intern.hpp"

#include "modin8.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// TC8, INI8, INV8 Module Configuration
//

// Instantiator

CModule * Create_IN8(void)
{
	return New CIN8Module;
	}

// Constructor

CIN8Module::CIN8Module(void)
{
	}

// Destructor

CIN8Module::~CIN8Module(void)
{
	}

// Overridables

void CIN8Module::OnLoad(PCBYTE &pData)
{
	switch( m_ModelID ) {

		case ID_CSTC8:
		case ID_CSINI8:
		case ID_CSINV8:
			
			m_FirmID = FIRM_8IN;
			
			break;

		case ID_CSINI8L:
		case ID_CSINV8L:
			
			m_FirmID = FIRM_8INL;
			
			break;
		}

	GetWord(pData);
	}

// End of File
