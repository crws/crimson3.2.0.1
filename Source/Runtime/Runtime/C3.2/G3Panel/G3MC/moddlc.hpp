
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODDLC_HPP

#define	INCLUDE_MODDLC_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "import/dlcprops.h"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DLC Module Configuration
//

class CDLCModule : public CModule
{
	public:
		// Constructor
		SLOW CDLCModule(void);

		// Destructor
		SLOW ~CDLCModule(void);

	public:
		// Config Data
		WORD	m_TempUnits[2];
		SHORT	m_ProcMin[2];
		SHORT	m_ProcMax[2];

	protected:
		// Data Members
		BOOL	m_fDelta[2][4];
		LONG	m_p1[2];
		LONG	m_p2[2];
		LONG	m_fs[2];
		BOOL	m_tc[2];
		BOOL	m_fTuning[2];

		// Overridables
		void  OnLoad(PCBYTE &pData);
		void  OnReadPersistData(CProxyRack *pRack);
		void  OnWritePersistData(CProxyRack *pRack);
		void  OnFilterInit(WORD PropID, DWORD Data);
		BOOL  OnFilterWrite(WORD PropID, DWORD Data);
		DWORD LinkToDisp(WORD PropID, DWORD Data);
		DWORD DispToLink(WORD PropID, DWORD Data);

		// Implementation
		void FindScaling(BOOL fLoop);
		LONG MakeDispLong(WORD Data, BOOL fDelta, BOOL fLoop);
		LONG MakeLinkLong(WORD Data, BOOL fDelta);
		BOOL IsScaled(WORD PropID, BOOL &fDelta);
	};	

// End of File

#endif
