
#include "intern.hpp"

#include "moddlc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Manticore PID2 Module Configuration
//

class CManticorePid2Module : public CDLCModule
{
	public:
		// Constructor
		CManticorePid2Module(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Manticore PID2 Module Configuration
//

// Instantiator

CModule * Create_ManticorePid2Module(void)
{
	return New CManticorePid2Module;
	}

// Constructor

CManticorePid2Module::CManticorePid2Module(void)
{	
	}

// Overridables

void CManticorePid2Module::OnLoad(PCBYTE &pData)
{
	m_FirmID = GetByte(pData);

	CDLCModule::OnLoad(pData);
	}

// End of File
