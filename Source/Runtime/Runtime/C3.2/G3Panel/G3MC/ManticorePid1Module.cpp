
#include "intern.hpp"

#include "modslc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Manticore PID1 Module Configuration
//

class CManticorePid1Module : public CSLCModule
{
	public:
		// Constructor
		CManticorePid1Module(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Manticore PID1 Module Configuration
//

// Instantiator

CModule * Create_ManticorePid1Module(void)
{
	return New CManticorePid1Module;
	}

// Constructor

CManticorePid1Module::CManticorePid1Module(void)
{	
	}

// Overridables

void CManticorePid1Module::OnLoad(PCBYTE &pData)
{
	m_FirmID = GetByte(pData);

	CSLCModule::OnLoad(pData);
	}

// End of File
