
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Module_HPP

#define	INCLUDE_Module_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProxyRack;

//////////////////////////////////////////////////////////////////////////
//
// Module Configuration
//

class CModule : public CItem
{
public:
	// Constructor
	CModule(void);

	// Destructor
	~CModule(void);

	// Creation
	static CModule * Create(BYTE Model);

	// Binding
	void Bind(CCommsDevice *pDevice, BYTE bDrop);

	// Persistance
	void Load(PCBYTE &pData);

	// Attributes
	BYTE  GetDrop(void) const;
	DWORD GetSlot(void) const;
	UINT  GetSlotNumber(void) const;

	// Tunneling
	BOOL Tunnel(PBYTE pData);

	// Comms Hooks
	BOOL TxData(CProxyRack *pProxy);
	void RxData(CProxyRack *pProxy);
	void RxFail(CProxyRack *pProxy);

protected:
	// Comms States
	enum
	{
		stateBootForceReset,
		stateBootCheckModel,
		stateBootCheckVersion,
		stateBootProgramReset,
		stateBootClearProgram,
		stateBootProgramSize,
		stateBootWriteProgram,
		stateBootWriteVerify,
		stateBootWriteVersion,
		stateBootStartProgram,
		stateConfigCheckVersion,
		stateConfigClearConfig,
		stateConfigWriteConfig,
		stateConfigWriteVersion,
		stateConfigWritePersist,
		stateConfigWriteCached,
		stateConfigStartSystem,
		stateConfigCheckStatus,
		stateData1,
		stateData2,
		stateReadPersist,
		stateTunnel
	};

	// Core Data
	BYTE		     m_ModelID;
	BYTE		     m_FirmID;
	CCommsDevice       * m_pDevice;
	CCommsSysBlockList * m_pBlocks;
	UINT                 m_uBlocks;
	BYTE		     m_bDrop;
	DWORD		     m_dwSlot;
	WORD		     m_wNumber;
	BYTE		     m_Guid[16];
	PCBYTE		     m_pInitData;
	UINT		     m_uState;
	UINT		     m_uData;
	BYTE		     m_bData[128];

	// Firmware
	PCBYTE	 m_pFirmData;
	DWORD	 m_dwFirmAddr;
	DWORD	 m_dwFirmSize;
	PCBYTE	 m_pData;
	BOOL     m_fAddr32;

	// Read Data
	UINT	 m_uReadBlock;
	INT	 m_nReadPos;
	UINT     m_uReadSlot;
	UINT     m_uReadFrom[128];

	// Write Data
	UINT	 m_uWriteBlock;
	INT	 m_nWritePos;
	UINT     m_uWriteSlot;
	UINT     m_uWriteFrom[128];
	BOOL     m_fWriteWrap;

	// Tunnel Data
	IEvent * m_pTunnelFree;
	IEvent * m_pTunnelDone;
	PBYTE    m_pTunnelData;
	BOOL	 m_fTunnelDone;
	BOOL	 m_fTunnelGood;

	// Persist Data
	UINT	 m_uPersistSize;
	BOOL	 m_fPersistRead;
	WORD	 m_Persist[14];

	// Implementation
	void ReadGuid(PCBYTE &pData);
	void ScanInit(PCBYTE &pData);
	BOOL HasPersistData(void);
	BOOL FindPersistData(void);
	void ParsePersistData(PBYTE pData);
	BOOL CheckRead(CProxyRack *pProxy);
	BOOL CheckWrite(CProxyRack *pProxy, BOOL fLimit);
	BOOL CheckWriteBlock(CProxyRack *pProxy, BOOL fLimit);
	BOOL CheckWriteQueue(CProxyRack *pProxy, BOOL fLimit);
	BOOL CheckCache(CProxyRack *pProxy);
	void ParseRead(PBYTE pData);
	void DoneWrite(void);
	void SetOnline(void);

	// Overridables
	virtual void  OnLoad(PCBYTE &pData);
	virtual void  OnReadPersistData(CProxyRack *pProxy);
	virtual void  OnWritePersistData(CProxyRack *pProxy);
	virtual void  OnFilterInit(WORD PropID, DWORD Data);
	virtual BOOL  OnFilterWrite(WORD PropID, DWORD Data);
	virtual DWORD LinkToDisp(WORD PropID, DWORD Data);
	virtual DWORD DispToLink(WORD PropID, DWORD Data);
};

// End of File

#endif
