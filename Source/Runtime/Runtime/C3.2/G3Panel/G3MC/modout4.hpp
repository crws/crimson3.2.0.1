
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODOUT4_HPP

#define	INCLUDE_MODOUT4_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "import/out4props.h"

//////////////////////////////////////////////////////////////////////////
//
// OUT4 Module Configuration
//

class COUT4Module : public CModule
{
	public:
		// Constructor
		SLOW COUT4Module(void);

		// Destructor
		SLOW ~COUT4Module(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);
	};	

// End of File

#endif
