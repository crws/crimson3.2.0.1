
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#include "ManticoreModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Manticore Module Configuration
//

// Instantiator

CModule * Create_ManticoreModule(void)
{
	return New CManticoreModule();
	}

// Constructor

CManticoreModule::CManticoreModule(void)
{
	}

// // Overridables

void CManticoreModule::OnLoad(PCBYTE &pData)
{
	m_FirmID = GetByte(pData);
	}

// End of File
