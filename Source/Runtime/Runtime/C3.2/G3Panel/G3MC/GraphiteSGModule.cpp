
#include "intern.hpp"

#include "modsg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Graphite SG1 Module Configuration
//

class CGraphiteSG1Module : public CSGModule
{
	public:
		// Constructor
		SLOW CGraphiteSG1Module(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphite SG1 Module Configuration
//

// Instantiator

CModule * Create_GMSG1(void)
{
	return New CGraphiteSG1Module;
	}

// Constructor

CGraphiteSG1Module::CGraphiteSG1Module(void)
{	
	m_FirmID = FIRM_GMSG1;
	}

// Overridables

void CGraphiteSG1Module::OnLoad(PCBYTE &pData)
{
	m_wNumber = GetWord(pData);

	m_dwSlot  = GetLong(pData);

	CSGModule::OnLoad(pData);
	}

// End of File
