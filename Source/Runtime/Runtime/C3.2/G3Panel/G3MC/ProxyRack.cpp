
#include "intern.hpp"

#include "ProxyRack.hpp"

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Identifiers
//

#include "..\..\..\HXLs\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Rack Proxy Object
//

// Instantiator

CProxy * Create_ProxyRack(void)
{
	return New CProxyRack;
}

// Static Data

CProxyRack * CProxyRack::m_pThis = NULL;

// Error Access

global BOOL RackHasError(void)
{
	return CProxyRack::m_pThis ? CProxyRack::m_pThis->HasError() : FALSE;
}

// Tunnel Access

global BOOL RackTunnel(BYTE bDrop, PBYTE pData)
{
	return CProxyRack::m_pThis ? CProxyRack::m_pThis->Tunnel(bDrop, pData) : FALSE;
}

// Constructor

CProxyRack::CProxyRack(void)
{
	m_pThis = this;
}

// Destructor

CProxyRack::~CProxyRack(void)
{
	m_pThis = NULL;
}

// Attributes

BOOL CProxyRack::HasError(void)
{
	return m_uError ? TRUE : FALSE;
}

// Entry Points

void CProxyRack::Init(UINT uTask)
{
	MakeModules();

	InitComms();
}

void CProxyRack::Task(UINT uTask)
{
	MainLoop();

	for(;;) Sleep(FOREVER);
}

void CProxyRack::Term(UINT uTask)
{
	ClosePort();

	delete[] m_ppModule;
}

// Suspension

void CProxyRack::Suspend(void)
{
}

void CProxyRack::Resume(void)
{
}

// Firmware

BOOL CProxyRack::GetFirmGUID(BYTE FirmID, PBYTE bData)
{
	PCBYTE pList = m_pFirmData;

	for( UINT n = 0; n < m_uFirmCount; n++ ) {

		if( pList[0] == FirmID ) {

			UINT   uItem = MotorToHost(*PWORD(pList+2));

			PCBYTE pFile = PCBYTE(g_pDbase->LockItem(uItem));

			if( pFile ) {

				Item_Align2(pFile);

				pFile += sizeof(WORD);

				Item_Align4(pFile);

				if( MotorToHost(PDWORD(pFile)[0]) ) {

					pFile += sizeof(DWORD);

					memcpy(bData, pFile, 16);

					g_pDbase->FreeItem(uItem);

					return TRUE;
				}

				g_pDbase->FreeItem(uItem);
			}
		}

		pList += 4;
	}

	return FALSE;
}

PCBYTE CProxyRack::GetFirmData(BYTE FirmID)
{
	PCBYTE pList = m_pFirmData;

	for( UINT n = 0; n < m_uFirmCount; n++ ) {

		if( pList[0] == FirmID ) {

			UINT   uItem = MotorToHost(*PWORD(pList+2));

			PCBYTE pFile = PCBYTE(g_pDbase->LockItem(uItem));

			if( pFile ) {

				Item_Align2(pFile);

				pFile += sizeof(WORD);

				Item_Align4(pFile);

				if( MotorToHost(PDWORD(pFile)[0]) ) {

					pFile += sizeof(DWORD);

					g_pDbase->FreeItem(uItem);

					return pFile + 16;
				}

				g_pDbase->FreeItem(uItem);
			}
		}

		pList += 4;
	}

	return NULL;
}

DWORD CProxyRack::GetFirmSize(BYTE FirmID)
{
	PCBYTE pList = m_pFirmData;

	for( UINT n = 0; n < m_uFirmCount; n++ ) {

		if( pList[0] == FirmID ) {

			UINT   uItem = MotorToHost(*PWORD(pList+2));

			PCBYTE pFile = PCBYTE(g_pDbase->LockItem(uItem));

			if( pFile ) {

				Item_Align2(pFile);

				pFile += sizeof(WORD);

				Item_Align4(pFile);

				DWORD dwSize = MotorToHost(PDWORD(pFile)[0]);

				g_pDbase->FreeItem(uItem);

				return dwSize;
			}
		}

		pList += 4;
	}

	return 0;
}

// Shared API	

BOOL CProxyRack::RxGeneral(void)
{
	return Request(m_pDriver->RxGeneral(m_bDrop), FALSE);
}

BOOL CProxyRack::RxResponse(void)
{
	return Response(m_pDriver->RxGeneral(m_bDrop));
}

// Boot Loader API

BOOL CProxyRack::BootTxForceReset(void)
{
	Debug("Send(%2.2u) - Boot.ForceReset\n", m_bDrop);

	Recycle();

	return Request(m_pDriver->BootTxForceReset(m_bDrop), FALSE);
}

BOOL CProxyRack::BootTxCheckModel(void)
{
	Debug("Send(%2.2u) - Boot.CheckModel\n", m_bDrop);

	return Request(m_pDriver->BootTxCheckModel(m_bDrop), FALSE);
}

BOOL CProxyRack::BootTxCheckVersion(PBYTE pGuid)
{
	Debug("Send(%2.2u) - Boot.CheckVersion\n", m_bDrop);

	return Request(m_pDriver->BootTxCheckVersion(m_bDrop, pGuid), FALSE);
}

BOOL CProxyRack::BootTxProgramReset(void)
{
	Debug("Send(%2.2u) - Boot.ProgramReset\n", m_bDrop);

	Recycle();

	return Request(m_pDriver->BootTxProgramReset(m_bDrop), FALSE);
}

BOOL CProxyRack::BootTxClearProgram(DWORD dwSize)
{
	Debug("Send(%2.2u) - Boot.ClearProgram(Size=%d)\n", m_bDrop, dwSize);

	return Request(m_pDriver->BootTxClearProgram(m_bDrop, dwSize), TRUE);
}

BOOL CProxyRack::BootTxProgramSize(DWORD dwSize)
{
	Debug("Send(%2.2u) - Boot.ProgramSize %8.8X\n", m_bDrop, dwSize);

	return Request(m_pDriver->BootTxProgramSize(m_bDrop, dwSize), TRUE);
}

BOOL CProxyRack::BootTxWriteProgram(WORD wAddr, PBYTE pData, WORD wCount)
{
	Debug("Send(%2.2u) - Boot.WriteProgram %4.4X\n", m_bDrop, wAddr);

	return Request(m_pDriver->BootTxWriteProgram(m_bDrop, wAddr, pData, wCount), TRUE);
}

BOOL CProxyRack::BootTxWriteProgram32(DWORD dwAddr, PBYTE pData, WORD wCount)
{
	Debug("Send(%2.2u) - Boot.WriteProgram32 %8.8X\n", m_bDrop, dwAddr);

	return Request(m_pDriver->BootTxWriteProgram32(m_bDrop, dwAddr, pData, wCount), TRUE);
}

BOOL CProxyRack::BootTxWriteVerify(DWORD Crc32)
{
	Debug("Send(%2.2u) - Boot.WriteVerify %8.8X\n", m_bDrop, Crc32);

	return Request(m_pDriver->BootTxWriteVerify(m_bDrop, Crc32), TRUE);
}

BOOL CProxyRack::BootTxWriteVersion(PBYTE pGuid)
{
	Debug("Send(%2.2u) - Boot.WriteVersion\n", m_bDrop);

	return Request(m_pDriver->BootTxWriteVersion(m_bDrop, pGuid), TRUE);
}

BOOL CProxyRack::BootTxStartProgram(void)
{
	Debug("Send(%2.2u) - Boot.StartProgram\n", m_bDrop);

	Recycle();

	return Request(m_pDriver->BootTxStartProgram(m_bDrop), TRUE);
}

BOOL CProxyRack::BootRxCheckModel(BOOL &fSame, BYTE ModelID)
{
	fSame = TRUE;

	BYTE bModel = 0;

	UINT uResp  = m_pDriver->BootRxCheckModel(m_bDrop, bModel);

	if( uResp == msgFrame ) {

		if( ModelID == ModelLookup(bModel) ) {

			Debug("Recv(%2.2u) - Boot.CheckModel  OKAY  Act:%2.2u Req:%2.2u\n",
			      m_bDrop,
			      ModelLookup(bModel),
			      ModelID
			);

			return TRUE;
		}

		if( ModelID == 0x01 ) {

			if( bModel == 0x00 || bModel == 0x55 || bModel == 0xFF ) {

				Debug("Recv(%2.2u) - Boot.CheckModel  OKAY  Act:%2.2u Req:%2.2u\n",
				      m_bDrop,
				      bModel,
				      ModelID
				);

				return TRUE;
			}
		}

		Debug("Recv(%2.2u) - Boot.CheckModel  FAIL  Act:%2.2u Req:%2.2u\n",
		      m_bDrop,
		      bModel,
		      ModelID
		);

		fSame = FALSE;

		return TRUE;
	}

	if( uResp == msgNak ) {

		if( ModelID == 0x01 && bModel == 0xFF ) {

			Debug("Recv(%2.2u) - Boot.CheckModel  OKAY  Ass:01 Req:01\n",
			      m_bDrop
			);

			return TRUE;
		}

		Debug("Recv(%2.2u) - Boot.CheckModel  FAIL  Ass:01 Req:%2.2u\n",
		      m_bDrop,
		      ModelID
		);

		fSame = FALSE;

		return TRUE;
	}

	Debug("Recv(%2.2u) Boot.CheckModel  ERROR\n",
	      m_bDrop
	);

	fSame = FALSE;

	return FALSE;
}

BOOL CProxyRack::BootRxWriteVerify(BOOL &fSame)
{
	return Response(m_pDriver->BootRxWriteVerify(m_bDrop, fSame));
}

BOOL CProxyRack::BootRxCheckVersion(BOOL &fSame)
{
	return Response(m_pDriver->BootRxCheckVersion(m_bDrop, fSame));
}

// Configuration API

BOOL CProxyRack::ConfigTxCheckVersion(PBYTE pGuid)
{
	Debug("Send(%2.2u) - Config.CheckVersion\n", m_bDrop);

	return Request(m_pDriver->ConfigTxCheckVersion(m_bDrop, pGuid), FALSE);
}

BOOL CProxyRack::ConfigTxClearConfig(void)
{
	Debug("Send(%2.2u) - Config.ClearConfig\n", m_bDrop);

	return Request(m_pDriver->ConfigTxClearConfig(m_bDrop), FALSE);
}

BOOL CProxyRack::ConfigTxWriteConfig(PCBYTE &pData)
{
	Debug("Send(%2.2u) - Config.WriteConfig\n", m_bDrop);

	return Request(m_pDriver->ConfigTxWriteConfig(m_bDrop, pData), FALSE);
}

BOOL CProxyRack::ConfigTxWriteVersion(PBYTE pGuid)
{
	Debug("Send(%2.2u) - Config.WriteVersion\n", m_bDrop);

	return Request(m_pDriver->ConfigTxWriteVersion(m_bDrop, pGuid), FALSE);
}

BOOL CProxyRack::ConfigTxStartSystem(void)
{
	Debug("Send(%2.2u) - Config.StartSystem\n", m_bDrop);

	return Request(m_pDriver->ConfigTxStartSystem(m_bDrop), FALSE);
}

BOOL CProxyRack::ConfigTxCheckStatus(void)
{
	Debug("Send(%2.2u) - Config.CheckStatus\n", m_bDrop);

	return Request(m_pDriver->ConfigTxCheckStatus(m_bDrop), FALSE);
}

BOOL CProxyRack::ConfigRxCheckVersion(BOOL &fSame)
{
	return Response(m_pDriver->ConfigRxCheckVersion(m_bDrop, fSame));
}

BOOL CProxyRack::ConfigRxWriteConfig(void)
{
	return Response(m_pDriver->ConfigRxWriteConfig(m_bDrop));
}

BOOL CProxyRack::ConfigRxCheckStatus(BOOL &fRun)
{
	return Response(m_pDriver->ConfigRxCheckStatus(m_bDrop, fRun));
}

// Data Transfer API

void CProxyRack::DataTxStartFrame(void)
{
	m_pDriver->DataTxStartFrame();
}

BOOL CProxyRack::DataTxWrite(WORD PropID, DWORD Data)
{
	return m_pDriver->DataTxWrite(PropID, Data);
}

BOOL CProxyRack::DataTxRead(WORD PropID)
{
	return m_pDriver->DataTxRead(PropID);
}

BOOL CProxyRack::DataTxSend(void)
{
	/*Debug("Send(%2.2u) - Data.Data\n", m_bDrop);*/

	return Request(m_pDriver->DataTxSend(m_bDrop), FALSE);
}

BOOL CProxyRack::DataRxData(PBYTE &pData)
{
	return m_pDriver->DataRxData(m_bDrop, pData) == msgFrame;
}

BOOL CProxyRack::DataTooFull(WORD PropID)
{
	return m_pDriver->DataTooFull(PropID);
}

// Tunneling

BOOL CProxyRack::Tunnel(BYTE bDrop, PBYTE pData)
{
	if( m_ppModule ) {

		for( UINT n = 0; n < m_uCount; n++ ) {

			CModule *pModule = m_ppModule[n];

			if( pModule ) {

				if( pModule->GetDrop() == bDrop ) {

					return pModule->Tunnel(pData);
				}
			}
		}
	}

	return FALSE;
}

// Tunneling API

BOOL CProxyRack::TunnelTx(PBYTE pData)
{
	return Request(m_pDriver->TunnelTx(m_bDrop, pData), FALSE);
}

BOOL CProxyRack::TunnelRx(PBYTE pData)
{
	return Response(m_pDriver->TunnelRx(m_bDrop, pData));
}

// Implementation

void CProxyRack::MakeModules(void)
{
	m_uTotal   = m_pPort->m_pDevices->m_uCount;

	m_uCount   = m_uTotal - 1;

	m_ppModule = New CModule *[m_uCount];

	for( UINT n = 0; n < m_uTotal; n++ ) {

		CCommsDevice *pDevice = m_pPort->m_pDevices->m_ppDevice[n];

		PCBYTE        pConfig = pDevice->GetConfig();

		BYTE          Model   = pConfig[2];

		if( n == 0 ) {

			m_pMaster    = pDevice;

			m_uFirmCount = MotorToHost(*PWORD(pConfig + 6));

			m_pFirmData  = pConfig + 8;
		}
		else {
			BYTE bDrop = BYTE(n-1);

			m_ppModule[bDrop] = CModule::Create(Model);

			m_ppModule[bDrop]->Bind(pDevice, bDrop);

			m_ppModule[bDrop]->Load(pConfig);
		}
	}
}

void CProxyRack::InitComms(void)
{
	m_uDog     = 0;

	m_uDiv     = 0;

	m_uError   = 0;

	m_uRetry   = 0;

	m_uSlow    = 0;

	m_fRecycle = FALSE;

	m_uRecycle = 0;

	for( UINT n = 0; n < m_uCount; n++ ) {

		m_uState[n] = stateInit;

		m_uTicks[n] = 0;
	}

	OpenPort();
}

void CProxyRack::MainLoop(void)
{
	if( CheckCount(TRUE) ) {

		UINT uScan = 0;

		UINT uPoll = 0;

		UINT uTime = 0;

		for(;;) {

			uTime   = GetTickCount();

			m_uMask = 1;

			for( UINT n = 0; n < m_uCount; n++ ) {

				if( UseDrop(n) ) {

					switch( m_uState[m_bDrop] ) {

						case stateInit:

							StateInit();

							break;

						case stateIdle:

							StateIdle();

							break;

						case stateReply:

							StateReply();

							break;
					}
				}

				m_uMask <<= 1;
			}

			if( ++uScan >= 1 ) {

				if( m_uError ) {

					for(;;) {

						uPoll   = (uPoll + 1) % m_uCount;

						m_uMask = (1 << uPoll);

						if( m_uError & m_uMask ) {

							UseDrop(uPoll);

							StateError();

							break;
						}
					}
				}
			}
			else
				uScan = 0;

			if( !m_uDiv-- ) {

				m_uDog  = (m_uDog + 1) % 10000;

				m_uDiv = 10;

				UpdateMaster();
			}

			if( GetTickCount() == uTime ) {

				Sleep(5);
			}
		}
	}
}

void CProxyRack::UpdateMaster(void)
{
	CCommsSysBlockList *pBlocks = m_pMaster->m_pSysBlocks;

	for( UINT uBlock = 0; uBlock < pBlocks->m_uCount; uBlock++ ) {

		CCommsSysBlock *pBlock = pBlocks->m_ppBlock[uBlock];

		INT             nSize  = pBlock->m_Size;

		for( INT nPos = 0; nPos < nSize; nPos++ ) {

			if( pBlock->ShouldRead(nPos) ) {

				CAddress Addr = pBlock->GetAddress(nPos);

				BYTE     Prop = LOBYTE(Addr.m_Ref);

				WORD     Data = 0;

				if( Prop >= 0x10 && Prop <= 0x1F ) {

					if( WhoHasFeature(rfSerialModules) ) {

						if( m_uError & (1 << (Prop - 0x10)) ) {

							Data = 1;
						}
					}

					if( WhoHasFeature(rfGraphiteModules) ) {

						for( UINT n = 0; n < m_uCount; n++ ) {

							if( m_ppModule[n]->GetSlotNumber() == (Prop - 0x10) ) {

								if( m_uError & (1 << n) ) {

									Data = 1;
								}
							}
						}
					}
				}

				if( Prop == 0x30 ) {

					Data = WORD(m_uDog);
				}

				pBlock->SetCommsData(nPos, Data);
			}
		}
	}
}

BOOL CProxyRack::UseDrop(UINT uIndex)
{
	if( (m_pDrop = m_ppModule[uIndex]) ) {

		m_bDrop = uIndex;

		return TRUE;
	}

	return FALSE;
}

BOOL CProxyRack::CheckCount(BOOL fWait)
{
	if( m_uCount == NOTHING ) {

		RackSetError(ERROR_BAD_CONFIG);

		return FALSE;
	}

	if( m_uCount ) {

		RackSetError(ERROR_OKAY);

		return TRUE;
	}

	RackSetError(ERROR_OKAY);

	return FALSE;
}

void CProxyRack::StateInit(void)
{
	m_uState[m_bDrop] = stateIdle;
}

void CProxyRack::StateIdle(void)
{
	if( m_uRecycle & m_uMask ) {

		m_uSlow &= ~m_uMask;

		switch( SendEnq() ) {

			case msgBusy:
			case msgNone:

				return;
		}

		m_uRecycle &= ~m_uMask;
	}

	if( m_pDrop->TxData(this) ) {

		if( RxResponse() ) {

			m_pDrop->RxData(this);

			m_uState[m_bDrop] = stateIdle;
		}
		else {
			m_uState[m_bDrop] = stateReply;

			m_uTicks[m_bDrop] = GetTickCount();
		}
	}
}

void CProxyRack::StateReply(void)
{
	UINT uGone = GetTickCount() - m_uTicks[m_bDrop];

	if( m_uSlow & m_uMask ) {

		if( uGone < ToTicks(15) ) {

			return;
		}
	}

	switch( SendEnq() ) {

		case msgAck:
		case msgFrame:

			m_pDrop->RxData(this);

			m_uState[m_bDrop] = stateIdle;

			ModuleError(commsOkay, "");

			break;

		case msgBusy:

			if( uGone > ToTicks(5000) ) {

				ModuleError(commsHard, "Out on time on ENQ");

				return;
			}

			break;

		case msgNone:

			ModuleError(commsSoft, "No reply on ENQ");

			break;

		case msgError:

			ModuleError(commsSoft, "Frame error on ENQ");

			break;

		case msgNak:

			ModuleError(commsSoft, "Negative ack on ENQ");

			break;

		default:

			ModuleError(commsHard, "Other reply to ENQ");

			break;
	}
}

void CProxyRack::StateError(void)
{
	if( PingModule() ) {

		Debug("Send(%2.2u) - PING - Okay\n", m_bDrop);

		ModuleError(commsOkay, "");

		m_uState[m_bDrop] = stateIdle;

		return;
	}

	Debug("Send(%2.2u) - PING - Failed\n", m_bDrop);
}

BOOL CProxyRack::PingModule(void)
{
	switch( SendEnq() ) {

		case msgFrame:
		case msgNak:
		case msgBusy:
		case msgAck:

			return TRUE;
	}

	return FALSE;
}

void CProxyRack::ModuleError(UINT uCode, PCTXT pText)
{
	if( uCode == commsOkay ) {

		if( m_uError & m_uMask ) {

			m_uError  &= ~m_uMask;

			m_uSlow   &= ~m_uMask;
		}

		m_uRetry &= ~m_uMask;

		return;
	}

	if( uCode == commsSoft ) {

		if( m_uRetry & m_uMask ) {

			ModuleError(commsHard, pText);

			return;
		}

		Debug("Fail(%2.2u) - Soft - %s\n", m_bDrop, pText);

		m_uRetry |= m_uMask;

		return;
	}

	if( uCode == commsHard ) {

		if( !(m_uError & m_uMask) ) {

			Debug("Fail(%2.2u) - Hard - %s\n", m_bDrop, pText);

			m_uError         |= m_uMask;

			m_uState[m_bDrop] = stateError;

			m_pDrop->RxFail(this);
		}

		return;
	}
}

UINT CProxyRack::ModelLookup(BYTE bModel)
{
	if( WhoHasFeature(rfGraphiteModules) ) {

		switch( 0x1000 | bModel ) {

			case pidPid1: return LOBYTE(ID_GMPID1);
			case pidPid2: return LOBYTE(ID_GMPID2);
			case pidOut4: return LOBYTE(ID_GMOUT4);
			case pidDio14: return LOBYTE(ID_GMDIO14);
			case pidTc8: return LOBYTE(ID_GMTC8);
			case pidIni8: return LOBYTE(ID_GMINI8);
			case pidInv8: return LOBYTE(ID_GMINV8);
			case pidRtd6: return LOBYTE(ID_GMRTD6);
			case pidSg1: return LOBYTE(ID_GMSG1);
			case pidUin4: return LOBYTE(ID_GMUIN4);
			case pidRate: return LOBYTE(ID_GMRC);
		}
	}

	return bModel;
}

// Port Access

BOOL CProxyRack::OpenPort(void)
{
	if( WhoHasFeature(rfSerialModules) ) {

		m_pRack   = Create_ManticoreHandler();

		m_pDriver = Create_ManticoreDriver(m_pRack);

		IPortHandler *pPort;

		m_pRack->QueryInterface(AfxAeonIID(IPortHandler), (void **) &pPort);

		CAutoObject<IPortHandler> Port(pPort);

		return m_pPort->Open(pPort, 0);
	}

	if( WhoHasFeature(rfGraphiteModules) ) {

		m_pRack   = Create_GraphiteHandler(m_uCount);

		m_pDriver = Create_GraphiteDriver(m_pRack);

		return TRUE;
	}

	return FALSE;
}

void CProxyRack::ClosePort(void)
{
	if( WhoHasFeature(rfSerialModules) ) {

		m_pPort->Term();

		m_pDriver->Release();
	}

	if( WhoHasFeature(rfGraphiteModules) ) {

		m_pDriver->Release();
	}

	RackSetError(ERROR_OKAY);
}

// Transport

UINT CProxyRack::SendEnq(void)
{
	UINT uMsg = m_pDriver->SendEnq(m_bDrop);

	switch( uMsg ) {

		case msgFrame:
		case msgBusy:
		case msgNak:
		case msgAck:

			return uMsg;

		case msgNone:

			if( m_uSlow & m_uMask ) {

				return msgBusy;
			}

			return msgNone;

		default:
			return msgError;
	}
}

BOOL CProxyRack::Request(UINT uMsg, BOOL fSlow)
{
	switch( uMsg ) {

		case msgAck:
		case msgFrame:

			if( fSlow ) {

				m_uSlow |= m_uMask;
			}
			else {
				m_uSlow &= ~m_uMask;
			}

			if( m_fRecycle ) {

				m_uRecycle |= m_uMask;

				m_fRecycle  = FALSE;
			}

			ModuleError(commsOkay, "");

			return TRUE;

		case msgPoll:

			ModuleError(commsOkay, "");

			return TRUE;

		case msgBusy:

			ModuleError(commsHard, "Busy on request frame");

			return FALSE;

		case msgNone:

			ModuleError(commsSoft, "No reply on frame");

			return FALSE;

		case msgError:

			ModuleError(commsSoft, "Frame error on frame");

			return FALSE;

		case msgNak:

			ModuleError(commsSoft, "Negative ack on frame");

			return FALSE;

		default:

			ModuleError(commsHard, "Other reply to frame");

			return FALSE;
	}
}

BOOL CProxyRack::Response(UINT uMsg)
{
	return uMsg == msgFrame;
}

void CProxyRack::Recycle(void)
{
	m_fRecycle = m_pDriver->Recycle(m_bDrop);
}

// Debug Function

void CProxyRack::Debug(PCTXT pForm, ...)
{
	#if defined(_XDEBUG)

	va_list pArgs;

	va_start(pArgs, pForm);

	AfxTraceArgs(pForm, pArgs);

	va_end(pArgs);

	#endif
}

// End of File
