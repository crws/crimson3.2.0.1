
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODRTD6_HPP

#define	INCLUDE_MODRTD6_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CSRTD6 Module Configuration
//

class CRTD6Module : public CModule
{
	public:
		// Constructor
		SLOW CRTD6Module(void);

		// Destructor
		SLOW ~CRTD6Module(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);
	};

// End of File

#endif
