
#include "intern.hpp"

#include "moddio14.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DIO Module Configuration
//

// Instantiator

CModule * Create_DIO14(void)
{
	return New CDIOModule;
	}

// Constructor

CDIOModule::CDIOModule(void)
{
	m_FirmID = FIRM_DIO14;
	}

// Destructor

CDIOModule::~CDIOModule(void)
{
	}

// Overridables

void CDIOModule::OnLoad(PCBYTE &pData)
{
	GetWord(pData);
	}

WORD CDIOModule::LinkToDisp(WORD PropID, WORD Data)
{
	return Data;
	}

WORD CDIOModule::DispToLink(WORD PropID, WORD Data)
{
	return Data;
	}

// End of File
