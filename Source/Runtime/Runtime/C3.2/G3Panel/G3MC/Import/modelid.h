
//////////////////////////////////////////////////////////////////////////
//
// Modular Controller Model and Firmware ID's
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODELID_H

#define	INCLUDE_MODELID_H

//////////////////////////////////////////////////////////////////////////
//
// Model ID's
//

#define ID_GMPID1	0x1006
#define ID_GMPID2	0x1007
#define ID_GMUIN4	0x1008
#define ID_GMOUT4	0x1009
#define ID_GMDIO14	0x100A
#define ID_GMTC8	0x100B
#define ID_GMINI8	0x100C
#define ID_GMINV8	0x100D
#define ID_GMRTD6	0x100E
#define ID_GMSG1	0x100F
#define ID_GMJ1939	0x1010
#define ID_GMRS		0x1011
#define ID_GMRC		0x1012
#define ID_DADIDO	0x1020
#define ID_DAUIN6	0x1022
#define ID_DAAO8	0x1023
#define ID_DADIRO	0x1024
#define ID_DAPID1_RA	0x1027
#define ID_DAPID1_SA	0x1029
#define ID_DAPID2_SM	0x1028
#define ID_DAPID2_RO	0x102A
#define ID_DAPID2_SO	0x102B
#define ID_DARO8	0x102C
#define ID_DAMIX4	0x102D
#define ID_DAMIX2	0x102E

#define ID_DASG1	0x10FF	// !!! Check This

//////////////////////////////////////////////////////////////////////////
//
// Firmware ID's
//

#define FIRM_GMPID1	1		// GMPID1.bin
#define FIRM_GMPID2	2		// GMPID2.bin
#define FIRM_GMUIN4	3		// GMUIN4.bin
#define FIRM_GMOUT4	4		// GMOUT4.bin
#define FIRM_GMDIO14	5		// GMDIO14.bin
#define FIRM_GMIN8	6		// GMIN8.bin
#define FIRM_GMRTD6	7		// GMRTD6.bin
#define FIRM_GMSG1	8		// GMSG1.bin
#define FIRM_GMCN	9		// GMCN.bin
#define FIRM_GMDN	10		// GMDN.bin
#define FIRM_GMJ1939	11		// GMJ1939.bin
#define FIRM_GMPB	12		// GMPB.bin
#define FIRM_GMRS	13		// GMRS.bin
#define FIRM_GMDNP3	14		// GMDNP3.bin
#define FIRM_GMCDL	15		// GMCDL.bin
#define FIRM_GMRC	16		// GMRC.bin
#define FIRM_DADIDO	17		// DADIDO.bin
#define FIRM_DADIRO	18		// DADIRO.bin
#define FIRM_DAUIN6	19		// DAUIN6.bin
#define FIRM_DAPID1	20		// DAPID1.bin
#define FIRM_DAPID2	21		// DAPID2.bin
#define FIRM_DASG1	22		// DASG1.bin
#define FIRM_DAAO8	23		// DAAO8.bin
#define FIRM_DARO8	24		// DARO8.bin

//////////////////////////////////////////////////////////////////////////
//
// Model IDs
//

#define	ID_MASTER	0
#define ID_CSPID1	1
#define ID_CSPID2	2
#define ID_CSTC8	3
#define ID_CSINI8	4
#define ID_CSINV8	5
#define ID_CSDIO14	6
#define ID_CSRTD6	7
#define ID_CSOPI8	8
#define ID_CSOPV8	9
#define ID_CSINI8L	10
#define ID_CSINV8L	11
#define ID_CSSG		12
#define ID_CSOUT4	13
#define ID_CSTC8ISO	14

//////////////////////////////////////////////////////////////////////////
//
// Model ID's
//

#define ID_CSPID1	1
#define ID_CSPID2	2
#define ID_CSTC8	3
#define ID_CSINI8	4
#define ID_CSINV8	5
#define ID_CSDIO14	6
#define ID_CSRTD6	7
#define ID_CSOPI8	8
#define ID_CSOPV8	9
#define ID_CSINI8L	10
#define ID_CSINV8L	11
#define ID_CSSG		12
#define ID_CSOUT4	13
#define ID_CSTC8ISO	14

//////////////////////////////////////////////////////////////////////////
//
// Firmware ID's
//

#define	FIRM_NONE	0
#define FIRM_PID1	1
#define FIRM_PID2	2
#define FIRM_8IN	3
#define FIRM_DIO14	4
#define FIRM_RTD6	5
#define FIRM_8INL	6
#define FIRM_SG		7
#define FIRM_OUT4	8
#define FIRM_TC8ISO	9
#define FIRM_UIN4	10

// End of File

#endif
