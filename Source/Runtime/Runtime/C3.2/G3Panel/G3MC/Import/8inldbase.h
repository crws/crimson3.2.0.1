
//////////////////////////////////////////////////////////////////////////
//
// R245 Modular Controller - 8 Analog Input Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_8INLDBASE_H

#define	INCLUDE_8INLDBASE_H

//////////////////////////////////////////////////////////////////////////
//
// Out of Range Value
//

#define	UPSCALE		((LONG) 0x7000000)
#define	FULLSCALE	((LONG) 300000)

//////////////////////////////////////////////////////////////////////////
//
// Process Current and Process Volt Input Ranges
//

#define	RANGE_020MA	1
#define	RANGE_420MA	2

#define	RANGE_010V	1
#define	RANGE_PM10V	2

//////////////////////////////////////////////////////////////////////////
//
// Model Numbers
//

#define	INI8		2
#define	INV8		3

//////////////////////////////////////////////////////////////////////////
//
// Module Hardware Constants
// 
	
#define	HDW_CHANNELS	8
#define LIN_POINTS	100

//////////////////////////////////////////////////////////////////////////
//
// Calibration Data
//

typedef struct tagCalib8INL
{
	WORD	InputVN10[HDW_CHANNELS];
	WORD	InputVP10[HDW_CHANNELS];
	WORD	InputI0[HDW_CHANNELS];
	WORD	InputI4[HDW_CHANNELS];
	WORD	InputI20[HDW_CHANNELS];

	} CALIB8INL;

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfig8INL
{
	BYTE	ChanEnable[HDW_CHANNELS];
	WORD	InputFilter;
	BYTE	PIRange;
	BYTE	PVRange;
	INT	ProcessMin[HDW_CHANNELS];
	INT	ProcessMax[HDW_CHANNELS];
	BYTE	SquareRt[HDW_CHANNELS];
	WORD	InputSlope[HDW_CHANNELS];
	INT	InputOffset[HDW_CHANNELS];
	BYTE	LinSegments[HDW_CHANNELS];
	WORD	LinInput[HDW_CHANNELS][LIN_POINTS];
	WORD	LinPV[HDW_CHANNELS][LIN_POINTS];
	BYTE	GUID[16];
	BYTE	Valid;

	} CONFIG8INL;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatus8INL
{
	BYTE	InputAlarm[HDW_CHANNELS];
	LONG	Input[HDW_CHANNELS];
	float	PV[HDW_CHANNELS];
	float	DeltaT[HDW_CHANNELS];
	BYTE	Running;

	} STATUS8INL;

// End of File

#endif
