
//////////////////////////////////////////////////////////////////////////
//
// R245 Modular Controller - PID Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_COMMS_H

#define	INCLUDE_COMMS_H

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#if defined(_M_IMX51) || defined(_M_AM437) // !!!!

#define	PACKET_SIZE		63

#else

#define	PACKET_SIZE		36

#endif

//////////////////////////////////////////////////////////////////////////
//
// Service Codes
//

#define	SERV_BOOT		0x01
#define	SERV_CONFIG		0x02
#define	SERV_DATA		0x03
#define	SERV_CALIB		0x04
#define	SERV_TUNNEL		0x05

//////////////////////////////////////////////////////////////////////////
//
// Shared Codes
//

#define	OP_ACK			0x01
#define	OP_NAK			0x02
#define	OP_REPLY		0x03

//////////////////////////////////////////////////////////////////////////
//
// Boot Loader Opcodes
//

#define	OP_CHECK_VERSION	0x10
#define	OP_CLEAR_PROGRAM	0x11
#define	OP_WRITE_PROGRAM	0x12
#define	OP_WRITE_VERSION	0x13
#define	OP_START_PROGRAM	0x14
#define	OP_CHECK_HWIDENT	0x15
#define	OP_FORCE_RESET		0x16
#define	OP_WRITE_MAC		0x17
#define OP_CHECK_MODEL		0x18
#define OP_PROGRAM_SIZE		0x19
#define OP_WRITE_PROGRAM32	0x1A

//////////////////////////////////////////////////////////////////////////
//
// Configuration Opcodes
//

#define	OP_CHECK_VERSION	0x10
#define	OP_CLEAR_CONFIG		0x11
#define	OP_WRITE_CONFIG		0x12
#define	OP_WRITE_VERSION	0x13
#define	OP_START_SYSTEM		0x14
#define	OP_CHECK_STATUS		0x15
#define OP_WRITE_CONFIG1	0x16


//////////////////////////////////////////////////////////////////////////
//
// Data Transfer Opcodes
//

#define	OP_DATA			0x10

//////////////////////////////////////////////////////////////////////////
//
// Calibration Opcodes
//

#define	OP_CALIB_REQ		0x10
#define	OP_CALIB_READ		0x11

//////////////////////////////////////////////////////////////////////////
//
// Tunneling Opcodes
//

#define	OP_TUNNEL		0x10

//////////////////////////////////////////////////////////////////////////
//
// Command Codes
//

#define	CMD_READ		0x00
#define	CMD_WRITE		0x40
#define	CMD_SET			0x80
#define	CMD_CLEAR		0xC0

// End of File

#endif
