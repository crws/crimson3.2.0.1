
//////////////////////////////////////////////////////////////////////////
//
// R245 Modular Controller - AMAT POD Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PODPROPS_H

#define	INCLUDE_PODPROPS_H

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	MAKEPROP(type, id)	MAKEWORD(id, type)

//////////////////////////////////////////////////////////////////////////
//
// Object IDs
//

#define	OBJ_LOOP_1		0x01

//////////////////////////////////////////////////////////////////////////
//
// Property Types
//

#define	TYPE_BOOL		0x01
#define	TYPE_BYTE		0x02
#define	TYPE_WORD		0x03
#define	TYPE_LONG		0x04
#define	TYPE_REAL		0x05

//////////////////////////////////////////////////////////////////////////
//
// Property IDs
//

#define	PROPID_E84_INPUT	MAKEPROP(TYPE_BYTE, 0x01)
#define	PROPID_E84_OUTPUT	MAKEPROP(TYPE_BYTE, 0x02)

#define	PROPID_RS232_BAUD1	MAKEPROP(TYPE_BYTE, 0x03)
#define	PROPID_RS232_BAUD2	MAKEPROP(TYPE_BYTE, 0x04)
#define	PROPID_RS232_BAUD3	MAKEPROP(TYPE_BYTE, 0x05)
#define	PROPID_RS232_BAUD4	MAKEPROP(TYPE_BYTE, 0x06)

#define	PROPID_RS232_BITS1	MAKEPROP(TYPE_BYTE, 0x07)
#define	PROPID_RS232_BITS2	MAKEPROP(TYPE_BYTE, 0x08)
#define	PROPID_RS232_BITS3	MAKEPROP(TYPE_BYTE, 0x09)
#define	PROPID_RS232_BITS4	MAKEPROP(TYPE_BYTE, 0x0A)

#define	PROPID_RS232_PARITY1	MAKEPROP(TYPE_BYTE, 0x0B)
#define	PROPID_RS232_PARITY2	MAKEPROP(TYPE_BYTE, 0x0C)
#define	PROPID_RS232_PARITY3	MAKEPROP(TYPE_BYTE, 0x0D)
#define	PROPID_RS232_PARITY4	MAKEPROP(TYPE_BYTE, 0x0E)

#define	PROPID_RS232_STOP1	MAKEPROP(TYPE_BYTE, 0x0F)
#define	PROPID_RS232_STOP2	MAKEPROP(TYPE_BYTE, 0x10)
#define	PROPID_RS232_STOP3	MAKEPROP(TYPE_BYTE, 0x11)
#define	PROPID_RS232_STOP4	MAKEPROP(TYPE_BYTE, 0x12)

#define	PROPID_RS232_FLAGS	MAKEPROP(TYPE_BYTE, 0x13)

// End of File

#endif
