
//////////////////////////////////////////////////////////////////////////
//
// R245 Modular Controller - 6 Channel RTD Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RTD6PROPS_H

#define	INCLUDE_RTD6PROPS_H

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	MAKEPROP(type, id)	MAKEWORD(id, type)

//////////////////////////////////////////////////////////////////////////
//
// Object IDs
//

#define	OBJ_LOOP_1		0x01

//////////////////////////////////////////////////////////////////////////
//
// Property Types
//

#define	TYPE_BOOL		0x01
#define	TYPE_BYTE		0x02
#define	TYPE_WORD		0x03
#define	TYPE_LONG		0x04
#define	TYPE_REAL		0x05

//////////////////////////////////////////////////////////////////////////
//
// Property IDs
//

#define	PROPID_INPUT_TYPE1	MAKEPROP(TYPE_BYTE, 0x01)
#define	PROPID_INPUT_TYPE2	MAKEPROP(TYPE_BYTE, 0x02)
#define	PROPID_INPUT_TYPE3	MAKEPROP(TYPE_BYTE, 0x03)
#define	PROPID_INPUT_TYPE4	MAKEPROP(TYPE_BYTE, 0x04)
#define	PROPID_INPUT_TYPE5	MAKEPROP(TYPE_BYTE, 0x05)
#define	PROPID_INPUT_TYPE6	MAKEPROP(TYPE_BYTE, 0x06)

#define	PROPID_INPUT_ALARM1	MAKEPROP(TYPE_BOOL, 0x09)
#define	PROPID_INPUT_ALARM2	MAKEPROP(TYPE_BOOL, 0x0A)
#define	PROPID_INPUT_ALARM3	MAKEPROP(TYPE_BOOL, 0x0B)
#define	PROPID_INPUT_ALARM4	MAKEPROP(TYPE_BOOL, 0x0C)
#define	PROPID_INPUT_ALARM5	MAKEPROP(TYPE_BOOL, 0x0D)
#define	PROPID_INPUT_ALARM6	MAKEPROP(TYPE_BOOL, 0x0E)

#define	PROPID_INPUT_FILTER	MAKEPROP(TYPE_WORD, 0x32) // SAME AS PID1

#define	PROPID_PV1		MAKEPROP(TYPE_REAL, 0x33)
#define	PROPID_PV2		MAKEPROP(TYPE_REAL, 0x34)
#define	PROPID_PV3		MAKEPROP(TYPE_REAL, 0x35)
#define	PROPID_PV4		MAKEPROP(TYPE_REAL, 0x36)
#define	PROPID_PV5		MAKEPROP(TYPE_REAL, 0x37)
#define	PROPID_PV6		MAKEPROP(TYPE_REAL, 0x38)

#define PROPID_INPUT_OFFSET1	MAKEPROP(TYPE_WORD, 0x3B)
#define PROPID_INPUT_OFFSET2	MAKEPROP(TYPE_WORD, 0x3C)
#define PROPID_INPUT_OFFSET3	MAKEPROP(TYPE_WORD, 0x3D)
#define PROPID_INPUT_OFFSET4	MAKEPROP(TYPE_WORD, 0x3E)
#define PROPID_INPUT_OFFSET5	MAKEPROP(TYPE_WORD, 0x3F)
#define PROPID_INPUT_OFFSET6	MAKEPROP(TYPE_WORD, 0x40)

#define PROPID_INPUT_SLOPE1	MAKEPROP(TYPE_WORD, 0x43)
#define PROPID_INPUT_SLOPE2	MAKEPROP(TYPE_WORD, 0x44)
#define PROPID_INPUT_SLOPE3	MAKEPROP(TYPE_WORD, 0x45)
#define PROPID_INPUT_SLOPE4	MAKEPROP(TYPE_WORD, 0x46)
#define PROPID_INPUT_SLOPE5	MAKEPROP(TYPE_WORD, 0x47)
#define PROPID_INPUT_SLOPE6	MAKEPROP(TYPE_WORD, 0x48)

#define PROPID_CHAN_ENABLE1	MAKEPROP(TYPE_BYTE, 0x77)
#define PROPID_CHAN_ENABLE2	MAKEPROP(TYPE_BYTE, 0x78)
#define PROPID_CHAN_ENABLE3	MAKEPROP(TYPE_BYTE, 0x79)
#define PROPID_CHAN_ENABLE4	MAKEPROP(TYPE_BYTE, 0x7A)
#define PROPID_CHAN_ENABLE5	MAKEPROP(TYPE_BYTE, 0x7B)
#define PROPID_CHAN_ENABLE6	MAKEPROP(TYPE_BYTE, 0x7C)

#define PROPID_TEMP_UNITS	MAKEPROP(TYPE_WORD, 0x7F) // SAME AS 8IN

// End of File

#endif
