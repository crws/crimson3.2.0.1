
#include "intern.hpp"

#include "modsg.hpp"

#include "ProxyRack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Module Configuration
//

// Instantiator

CModule * Create_SG(void)
{
	return New CSGModule;
	}

// Constructor

CSGModule::CSGModule(void)
{
	m_ModelID      = ID_CSSG;

	m_FirmID       = FIRM_SG;

	m_uPersistSize = 4;

	m_fTuning      = FALSE;

	m_Rewrite      = 0;
	}

// Destructor

CSGModule::~CSGModule(void)
{
	}

// Overridables

void CSGModule::OnLoad(PCBYTE &pData)
{
	m_PVLimitLo	= SHORT(GetWord(pData));

	m_PVLimitHi	= SHORT(GetWord(pData));

	m_InputRange[0] = GetWord(pData);

	m_InputRange[1] = GetWord(pData);

	GetWord(pData);
	}

void CSGModule::OnReadPersistData(CProxyRack *pRack)
{
	pRack->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_P);

	pRack->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_I);

	pRack->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_D);

	pRack->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_FILTER);
	}

void CSGModule::OnWritePersistData(CProxyRack *pRack)
{
	pRack->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_P, m_Persist[1]);

	pRack->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_I, m_Persist[2]);

	pRack->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_D, m_Persist[3]);

	pRack->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_FILTER,  m_Persist[4]);
	}

void CSGModule::OnDataExchange(CProxyRack *pRack)
{
	BOOL Accepted = TRUE;

	while( m_Rewrite && Accepted ) {

		UINT Index  = m_Rewrite - 1;

		WORD PropID = GetScaledID(Index);

		if( PropID ){

			WORD Data = DispToLink(PropID, m_ScaledData[Index]);

			Accepted  = pRack->DataTxWrite(PropID, Data);
			}

		if( Accepted )

			m_Rewrite--;
		}
	}

void CSGModule::OnFilterRead(WORD PropID, WORD Data)
{
	UINT Index = 0;

	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_PV_LIMIT_LO):  m_PVLimitLo = SHORT(Data);  break;

			case LOBYTE(PROPID_PV_LIMIT_HI):  m_PVLimitHi = SHORT(Data);  break;
			}
		}

	if( IsScaledData(PropID, Index) ){

		m_ScaledData[Index] = SHORT(LinkToDisp(PropID, Data));
		}
	}

void CSGModule::OnFilterInit(WORD PropID, WORD Data)
{
	UINT Index = 0;

	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_PV_LIMIT_LO):  m_PVLimitLo = SHORT(Data);  break;

			case LOBYTE(PROPID_PV_LIMIT_HI):  m_PVLimitHi = SHORT(Data);  break;
			}
		}

	if( IsScaledData(PropID, Index) ){

		m_InitData[Index] = Data;
		}

	if( PropID == LAST_INIT_ITEM ) {

		for( UINT i = 0; i < SCALED_DATA_COUNT; i++ ){

			m_ScaledData[i] = SHORT(LinkToDisp(GetScaledID(i), m_InitData[i]));
			}
		}
	}

BOOL CSGModule::OnFilterWrite(WORD PropID, WORD Data)
{
	UINT Index = 0;

	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_REQ_TUNE):

				if( Data ) {

					m_fTuning = TRUE;
					}

				else {
					if( m_fTuning ) {

						m_fTuning = FALSE;

						return TRUE;
						}

					m_fTuning = FALSE;
					}

				break;

			case LOBYTE(PROPID_PV_LIMIT_LO):

				m_PVLimitLo = SHORT(Data);

				m_Rewrite   = SCALED_DATA_COUNT;

				break;

			case LOBYTE(PROPID_PV_LIMIT_HI):

				m_PVLimitHi = SHORT(Data);

				m_Rewrite   = SCALED_DATA_COUNT;

				break;
			}
		}

	if( IsScaledData(PropID, Index) ){

		m_ScaledData[Index] = SHORT(LinkToDisp(PropID, Data));
		}

	return FALSE;
	}

WORD CSGModule::LinkToDisp(WORD PropID, WORD Data)
{
	BOOL fDelta;

	BYTE Channel;

	if( IsProcess(PropID, fDelta) ) {

		LONG a = MakeLinkLong(Data, fDelta);

		a -= 0;

		a *= (m_PVLimitHi - m_PVLimitLo);

		if( a > 0 ) a += (30000 / 2);

		if( a < 0 ) a -= (30000 / 2);

		a /= 30000;

		a += fDelta ? 0 : m_PVLimitLo;

		return WORD(a);
		}

	if( IsSignal(PropID, Channel) ) {

		double b = (LONG)Data;

		LONG Lo  = (m_InputRange[Channel] == SG_INPUT_33MV) ? -33000 : -20000;

		LONG Hi  = (m_InputRange[Channel] == SG_INPUT_33MV) ?  33000 :  20000;

		b -= 30000;

		b *= (Hi - Lo);

		if( b > 0 ) b += 60000 / 2;

		if( b < 0 ) b -= 60000 / 2;

		b /= 60000;

		return SHORT(b);
		}

	return Data;
	}

WORD CSGModule::DispToLink(WORD PropID, WORD Data)
{
	BOOL fDelta;

	BYTE Channel;

	if( IsProcess(PropID, fDelta) ) {

		LONG a = MakeDispLong(Data, fDelta);

		if( LOBYTE(PropID) == LOBYTE(PROPID_REQ_SP) ){
			
			if( a < m_PVLimitLo )  a = m_PVLimitLo;

			if( a > m_PVLimitHi )  a = m_PVLimitHi;
			}

		a -= fDelta ? 0 : m_PVLimitLo;

		a *= 30000;

		if( a > 0 ) a += (m_PVLimitHi - m_PVLimitLo) / 2;

		if( a < 0 ) a -= (m_PVLimitHi - m_PVLimitLo) / 2;

		a /= (m_PVLimitHi - m_PVLimitLo);

		a += 0;

		return WORD(a);
		}

	if( IsSignal(PropID, Channel) ) {

		double b = SHORT(Data);

		LONG Lo  = (m_InputRange[Channel] == SG_INPUT_33MV) ? -33000 : -20000;

		LONG Hi  = (m_InputRange[Channel] == SG_INPUT_33MV) ?  33000 :  20000;

		b -= Lo;

		b *= 60000;

		if( b > 0 ) b += (Hi - Lo) / 2;

		if( b < 0 ) b -= (Hi - Lo) / 2;

		b /= (Hi - Lo);

		return WORD(b);
		}

	return Data;
	}

// Implementation

LONG CSGModule::MakeLinkLong(WORD Data, BOOL fDelta)
{
	if( !fDelta ) {

		return WORD(Data);
		}

	return SHORT(Data);
	}

LONG CSGModule::MakeDispLong(WORD Data, BOOL fDelta)
{
	return SHORT(Data);
	}

BOOL CSGModule::IsProcess(WORD PropID, BOOL &fDelta)
{
	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_ERROR):
			case LOBYTE(PROPID_SET_HYST):
			case LOBYTE(PROPID_SET_DEAD):
			case LOBYTE(PROPID_SET_RAMP):

				fDelta = TRUE;

				return TRUE;

			case LOBYTE(PROPID_PV):
			case LOBYTE(PROPID_ACT_SP):
			case LOBYTE(PROPID_REQ_SP):

				fDelta = FALSE;
				
				return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CSGModule::IsSignal(WORD PropID, BYTE &Channel)
{
	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_SIGLOKEY_1):
			case LOBYTE(PROPID_SIGHIKEY_1):
			case LOBYTE(PROPID_SIGLOAPP_1):
			case LOBYTE(PROPID_SIGHIAPP_1):

				Channel = 0;

				return TRUE;

			case LOBYTE(PROPID_SIGLOKEY_2):
			case LOBYTE(PROPID_SIGHIKEY_2):
			case LOBYTE(PROPID_SIGLOAPP_2):
			case LOBYTE(PROPID_SIGHIAPP_2):

				Channel = 1;

				return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CSGModule::IsScaledData(WORD PropID, UINT &Index)
{
	UINT i = 0;

	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_SET_HYST):  i = 1;  break;
			case LOBYTE(PROPID_SET_DEAD):  i = 2;  break;
			case LOBYTE(PROPID_SET_RAMP):  i = 3;  break;
			case LOBYTE(PROPID_REQ_SP):    i = 4;  break;

			}
		}

	if( (i > 0) && (i <= SCALED_DATA_COUNT) ){

		Index = i - 1;

		return TRUE;
		}

	return FALSE;
	}

WORD CSGModule::GetScaledID(UINT Index)
{
	switch( Index ){

		case 0:  return ((OBJ_LOOP_1 << 11) | PROPID_SET_HYST);
		case 1:  return ((OBJ_LOOP_1 << 11) | PROPID_SET_DEAD);
		case 2:  return ((OBJ_LOOP_1 << 11) | PROPID_SET_RAMP);
		case 3:  return ((OBJ_LOOP_1 << 11) | PROPID_REQ_SP);

		}

	return 0;
	}

// End of File
