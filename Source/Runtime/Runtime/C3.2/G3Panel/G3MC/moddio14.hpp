
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODDIO_HPP

#define	INCLUDE_MODDIO_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DIO Module Configuration
//

class CDIOModule : public CModule
{
	public:
		// Constructor
		SLOW CDIOModule(void);

		// Destructor
		SLOW ~CDIOModule(void);

		// Overridables
		void OnLoad(PCBYTE &pData);
		WORD LinkToDisp(WORD PropID, WORD Data);
		WORD DispToLink(WORD PropID, WORD Data);
	};

// End of File

#endif
