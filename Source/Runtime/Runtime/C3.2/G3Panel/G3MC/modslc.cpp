
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hpp"

#include "modslc.hpp"

#include "ProxyRack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SLC Module Configuration
//

// Instantiator

CModule * Create_SLC(void)
{
	return New CSLCModule;
	}

// Constructor

CSLCModule::CSLCModule(void)
{
	m_FirmID       = FIRM_PID1;

	m_uPersistSize = 4;

	m_fTuning      = FALSE;
	}

// Destructor

CSLCModule::~CSLCModule(void)
{
	}

// Overridables

void CSLCModule::OnLoad(PCBYTE &pData)
{
	m_TempUnits = GetWord(pData);

	m_ProcMin   = GetWord(pData);
	
	m_ProcMax   = GetWord(pData);

	GetWord(pData);

	FindScaling();
	}

void CSLCModule::OnReadPersistData(CProxyRack *pProxy)
{
	pProxy->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_P);

	pProxy->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_I);

	pProxy->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_D);

	pProxy->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_FILTER);
	}

void CSLCModule::OnWritePersistData(CProxyRack *pProxy)
{
	pProxy->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_P, m_Persist[1]);

	pProxy->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_I, m_Persist[2]);

	pProxy->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_D, m_Persist[3]);

	pProxy->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_FILTER,  m_Persist[4]);
	}

void CSLCModule::OnFilterInit(WORD PropID, DWORD Data)
{
	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {

		INT n;

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_ALARM_MODE_1): n = 0; break;
			case LOBYTE(PROPID_ALARM_MODE_2): n = 1; break;
			case LOBYTE(PROPID_ALARM_MODE_3): n = 2; break;
			case LOBYTE(PROPID_ALARM_MODE_4): n = 3; break;

			default:
				return;
			}

		switch( Data ) {

			case ALARM_NULL:

				m_fDelta[n] = TRUE;
				
				break;

			case ALARM_ABS_LO:
			case ALARM_ABS_HI:
				
				m_fDelta[n] = FALSE;
				
				break;

			default:
				
				m_fDelta[n] = TRUE;
				
				break;
			}
		}
	}

BOOL CSLCModule::OnFilterWrite(WORD PropID, DWORD Data)
{
	if( PropID == ((OBJ_LOOP_1 << 11) | PROPID_REQ_TUNE) ) {

		if( Data ) {

			m_fTuning = TRUE;
			}
		else {
			if( m_fTuning ) {

				m_fTuning = FALSE;

				return TRUE;
				}

			m_fTuning = FALSE;
			}
		}

	return FALSE;
	}

DWORD CSLCModule::LinkToDisp(WORD PropID, DWORD Data)
{
	BOOL fDelta;

	if( IsScaled(PropID, fDelta) ) {

		LONG a = MakeLinkLong(Data, fDelta);

		a -= 0;

		a *= (m_p2 - m_p1);

		if( a > 0 ) a += m_fs / 2;

		if( a < 0 ) a -= m_fs / 2;

		a /= m_fs;

		a += fDelta ? 0 : m_p1;

		return WORD(a);
		}

	return Data;
	}

DWORD CSLCModule::DispToLink(WORD PropID, DWORD Data)
{
	BOOL fDelta;

	if( IsScaled(PropID, fDelta) ) {

		LONG a = MakeDispLong(Data, fDelta);

		a -= fDelta ? 0 : m_p1;

		a *= m_fs;

		if( a > 0 ) a += (m_p2 - m_p1) / 2;

		if( a < 0 ) a -= (m_p2 - m_p1) / 2;

		a /= (m_p2 - m_p1);

		a += 0;

		return WORD(a);
		}

	return Data;
	}

// Implementation

void CSLCModule::FindScaling(void)
{
	switch( m_TempUnits ) {

		case 0:
			m_p1 = 0;
			m_p2 = 10000;
			m_fs = 20000;
			m_tc = TRUE;
			break;

		case 1:
			m_p1 = -4597;
			m_p2 = 13403;
			m_fs = 20000;
			m_tc = TRUE;
			break;

		case 2:
			m_p1 = -2732;
			m_p2 =  7268;
			m_fs = 20000;
			m_tc = TRUE;
			break;

		default:
			m_p1 = m_ProcMin;
			m_p2 = m_ProcMax;
			m_fs = 30000;
			m_tc = FALSE;
			break;
		}

	m_fDelta[0] = FALSE;
	m_fDelta[1] = FALSE;
	m_fDelta[2] = FALSE;
	m_fDelta[3] = FALSE;
	}

LONG CSLCModule::MakeLinkLong(WORD Data, BOOL fDelta)
{
	if( !fDelta ) {

		return WORD(Data);
		}

	return SHORT(Data);
	}

LONG CSLCModule::MakeDispLong(WORD Data, BOOL fDelta)
{
	if( m_tc ) {

		if( fDelta ) {

			return SHORT(Data);
			}

		if( Data > 50000U ) {

			return SHORT(Data);
			}

		return WORD(Data);
		}

	return SHORT(Data);
	}

BOOL CSLCModule::IsScaled(WORD PropID, BOOL &fDelta)
{
	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_DELTA_PV):
			case LOBYTE(PROPID_ERROR):
			case LOBYTE(PROPID_SET_HYST):
			case LOBYTE(PROPID_SET_DEAD):
			case LOBYTE(PROPID_INPUT_OFFSET):
			case LOBYTE(PROPID_SET_RAMP):
			case LOBYTE(PROPID_ALARM_HYST_1):
			case LOBYTE(PROPID_ALARM_HYST_2):
			case LOBYTE(PROPID_ALARM_HYST_3):
			case LOBYTE(PROPID_ALARM_HYST_4):
				
				fDelta = TRUE;
				
				return TRUE;

			case LOBYTE(PROPID_INPUT):
			case LOBYTE(PROPID_RANGE_LO):
			case LOBYTE(PROPID_RANGE_HI):
			case LOBYTE(PROPID_ACT_SP):
			case LOBYTE(PROPID_PV):
			case LOBYTE(PROPID_REQ_SP):
			case LOBYTE(PROPID_ALT_SP):
			case LOBYTE(PROPID_ALT_PV):
			case LOBYTE(PROPID_COLD_JUNC):
			case LOBYTE(PROPID_P00_SP):
			case LOBYTE(PROPID_P01_SP):
			case LOBYTE(PROPID_P02_SP):
			case LOBYTE(PROPID_P03_SP):
			case LOBYTE(PROPID_P04_SP):
			case LOBYTE(PROPID_P05_SP):
			case LOBYTE(PROPID_P06_SP):
			case LOBYTE(PROPID_P07_SP):
			case LOBYTE(PROPID_P08_SP):
			case LOBYTE(PROPID_P09_SP):
			case LOBYTE(PROPID_P10_SP):
			case LOBYTE(PROPID_P11_SP):
			case LOBYTE(PROPID_P12_SP):
			case LOBYTE(PROPID_P13_SP):
			case LOBYTE(PROPID_P14_SP):
			case LOBYTE(PROPID_P15_SP):
			case LOBYTE(PROPID_P16_SP):
			case LOBYTE(PROPID_P17_SP):
			case LOBYTE(PROPID_P18_SP):
			case LOBYTE(PROPID_P19_SP):
			case LOBYTE(PROPID_P20_SP):
			case LOBYTE(PROPID_P21_SP):
			case LOBYTE(PROPID_P22_SP):
			case LOBYTE(PROPID_P23_SP):
			case LOBYTE(PROPID_P24_SP):
			case LOBYTE(PROPID_P25_SP):
			case LOBYTE(PROPID_P26_SP):
			case LOBYTE(PROPID_P27_SP):
			case LOBYTE(PROPID_P28_SP):
			case LOBYTE(PROPID_P29_SP):
			case LOBYTE(PROPID_P30_SP):
			case LOBYTE(PROPID_P31_SP):

				fDelta = FALSE;
				
				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_1):
				
				fDelta = m_fDelta[0];

				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_2):
				
				fDelta = m_fDelta[1];

				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_3):
				
				fDelta = m_fDelta[2];

				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_4):
				
				fDelta = m_fDelta[3];

				return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

// End of File
