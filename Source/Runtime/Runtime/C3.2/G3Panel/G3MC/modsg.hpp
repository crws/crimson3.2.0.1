
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODSG_HPP

#define	INCLUDE_MODSG_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "import/sgprops.h"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define SG_INPUT_33MV		2

#define SCALED_DATA_COUNT	4

#define LAST_INIT_ITEM		((OBJ_MAPPER << 11) | PROPID_CYCLE_TIME_3)

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Module Configuration
//

class CSGModule : public CModule
{
	public:
		// Constructor
		CSGModule(void);

		// Destructor
		~CSGModule(void);

	protected:
		// Data Members
		LONG	m_PVLimitLo;
		LONG	m_PVLimitHi;
		WORD	m_InputRange[2];
		BOOL	m_fTuning;
		SHORT	m_ScaledData[SCALED_DATA_COUNT];
		WORD	m_InitData[SCALED_DATA_COUNT];
		UINT	m_Rewrite;

		// Overridables
		void OnLoad(PCBYTE &pData);
		void OnReadPersistData(CProxyRack *pRack);
		void OnWritePersistData(CProxyRack *pRack);
		void OnDataExchange(CProxyRack *pRack);
		void OnFilterRead(WORD PropID, WORD Data);
		void OnFilterInit(WORD PropID, WORD Data);
		BOOL OnFilterWrite(WORD PropID, WORD Data);
		WORD LinkToDisp(WORD PropID, WORD Data);
		WORD DispToLink(WORD PropID, WORD Data);

		// Implementation
		LONG MakeDispLong(WORD Data, BOOL fDelta);
		LONG MakeLinkLong(WORD Data, BOOL fDelta);
		BOOL IsProcess(WORD PropID, BOOL &fDelta);
		BOOL IsSignal(WORD PropID, BYTE &Channel);
		BOOL IsScaledData(WORD PropID, UINT &Index);
		WORD GetScaledID(UINT Index);

	};	

// End of File

#endif
