
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Crc16Ccitt_HPP

#define	INCLUDE_Crc16Ccitt_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CRC16-CCITT-FALSE
//

class CCrc16Ccitt
{
	public:
		// Constructor
		INLINE CCrc16Ccitt(void);
			
		// Attributes
		INLINE WORD GetValue(void) const;
			
		// Operations
		INLINE void Clear(void);
		INLINE void Preset(void);
		INLINE void Add(BYTE b);
		INLINE void Add(PCBYTE p, UINT n);
			
	protected:
		// Lookup Data
		static WORD CODE_SEG m_Table[];

		// Data Members
		WORD m_Crc;
	};

//////////////////////////////////////////////////////////////////////////
//
// CRC16-CCITT-FALSE
//

INLINE CCrc16Ccitt::CCrc16Ccitt(void)
{
	Clear();
	}

// Attributes

INLINE WORD CCrc16Ccitt::GetValue(void) const
{
	return m_Crc;
	}

// Operations

INLINE void CCrc16Ccitt::Clear(void)
{
	m_Crc = 0x0000;
	}

INLINE void CCrc16Ccitt::Preset(void)
{
	m_Crc = 0xFFFF;
	}

INLINE void CCrc16Ccitt::Add(BYTE b)
{
	m_Crc = ((m_Crc >> 8) ^ b) + (m_Crc << 8);

	m_Crc = (m_Crc & 0xFF00) ^ m_Table[m_Crc & 0x00FF];              
	}

INLINE void CCrc16Ccitt::Add(PCBYTE p, UINT n)
{
	while( n-- ) {

		Add(*p++);              
		}
	}

// End of File

#endif
