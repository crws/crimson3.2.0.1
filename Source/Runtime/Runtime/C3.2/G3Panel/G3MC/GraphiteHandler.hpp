
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GraphiteHandler_HPP

#define	INCLUDE_GraphiteHandler_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "ProxyRack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphite Handler
//

class CGraphiteHandler : public IRackHandler, public IExpansionEvents
{
public:
	// Constructor
	CGraphiteHandler(UINT uCount);

	// Destructor
	~CGraphiteHandler(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IRackHandler
	UINT METHOD Transact(BYTE bDrop, PBYTE pData, UINT uCount);
	UINT METHOD Transact(BYTE bDrop, UINT  Msg);
	BOOL METHOD GetReply(BYTE bDrop, PBYTE pData);
	BOOL METHOD Recycle(BYTE bDrop);

	// IExpansionEvents
	void METHOD OnDeviceArrival(UINT iSlot, UINT iDriver);
	void METHOD OnDeviceRemoval(UINT iSlot, UINT iDriver);

protected:
	// State
	enum
	{
		stateDiscover,
		stateNone,
		stateDetached,
		stateAttached,
		stateReady,
		stateRecycle,
	};

	// Flags
	enum
	{
		flagRecv    = Bit(0),
		flagSend    = Bit(1),
		flagRecycle = Bit(2),
		flagLocked  = Bit(3),
	};

	// Custom Data
	struct CModuleComms
	{
		DWORD		     m_uSlot;
		IUsbHostBulkDriver * m_pDriver;
		IUsbDevice         * m_pDevice;
		UINT volatile	     m_uState;
		UINT                 m_uFlags;
		UINT		     m_uTimeout;
		BYTE 	             m_bBuff[PACKET_SIZE+1];
		UINT		     m_uCount;
	};

	// Data Members
	ULONG	           m_uRefs;
	IExpansionSystem * m_pRack;
	UINT		   m_uCount;
	CModuleComms	 * m_pList;
	IMutex           * m_pMutex;
	CMap<UINT, UINT>   m_Lookup;

	// Implementation
	BOOL CheckModule(BYTE bSlot);
	void SetError(BYTE bDrop);
	BOOL FindSlot(BYTE bDrop);
	BOOL FindDriver(BYTE bDrop);
	BOOL CheckDriver(BYTE bDrop);
	void Shutdown(void);
	void AbortComms(UINT uDrop);
	BOOL SetRemoveLock(UINT uDrop);
	BOOL FreeRemoveLock(UINT uDrop);
};

// End of File

#endif