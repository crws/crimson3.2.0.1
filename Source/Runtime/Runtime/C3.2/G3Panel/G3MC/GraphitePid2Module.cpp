
#include "intern.hpp"

#include "moddlc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Graphite PID2 Module Configuration
//

class CGraphitePid2Module : public CDLCModule
{
	public:
		// Constructor
		CGraphitePid2Module(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphite PID2 Module Configuration
//

// Instantiator

CModule * Create_GMPID2(void)
{
	return New CGraphitePid2Module;
	}

// Constructor

CGraphitePid2Module::CGraphitePid2Module(void)
{	
	m_FirmID = FIRM_GMPID2;
	}

// Overridables

void CGraphitePid2Module::OnLoad(PCBYTE &pData)
{
	m_wNumber = GetWord(pData);

	m_dwSlot  = GetLong(pData);

	CDLCModule::OnLoad(pData);
	}

// End of File
