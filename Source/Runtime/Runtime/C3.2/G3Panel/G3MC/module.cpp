
#include "intern.hpp"

#include "ProxyRack.hpp"

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Module Configuration
//

// Constructor

CModule::CModule(void)
{
	m_ModelID      = 0;

	m_FirmID       = 0;

	m_pDevice      = NULL;

	m_pBlocks      = NULL;

	m_uBlocks      = 0;

	m_bDrop        = 0xFF;

	m_wNumber      = 0xFFFF;

	m_dwSlot       = 0xFFFFFFFF;

	m_uData        = WhoHasFeature(rfGraphiteModules) ? 32 : 128;

	m_uState       = stateBootCheckModel;

	m_uReadBlock   = 0;

	m_nReadPos     = 0;

	m_uReadSlot    = 0;

	m_uWriteBlock  = 0;

	m_nWritePos    = 0;

	m_uWriteSlot   = 0;

	m_pTunnelFree  = Create_AutoEvent();

	m_pTunnelDone  = Create_AutoEvent();

	m_pTunnelData  = NULL;

	m_uPersistSize = 0;

	m_fPersistRead = FALSE;

	m_fAddr32      = FALSE;

	m_pTunnelFree->Set();

	m_pTunnelDone->Clear();
}

// Destructor

CModule::~CModule(void)
{
	m_pTunnelFree->Release();

	m_pTunnelDone->Release();
}

// Binding

void CModule::Bind(CCommsDevice *pDevice, BYTE bDrop)
{
	m_pDevice = pDevice;

	m_pBlocks = m_pDevice->m_pSysBlocks;

	m_uBlocks = m_pBlocks->m_uCount;

	m_bDrop   = bDrop;

	m_pDevice->EnableQueue();
}

// Persistance

void CModule::Load(PCBYTE &pData)
{
	ValidateLoad("CModule", pData);

	m_ModelID = GetByte(pData);

	OnLoad(pData);

	ReadGuid(pData);

	Item_Align2(pData);

	m_pInitData = pData;

	ScanInit(pData);
}

// Attributes

BYTE CModule::GetDrop(void) const
{
	if( m_dwSlot < NOTHING ) {

		return LOBYTE(m_wNumber);
	}

	return m_bDrop;
}

DWORD CModule::GetSlot(void) const
{
	return m_dwSlot;
}

UINT CModule::GetSlotNumber(void) const
{
	return m_wNumber;
}

// Tunneling

BOOL CModule::Tunnel(PBYTE pData)
{
	if( m_uState == stateData2 ) {

		if( m_pTunnelFree->Wait(FOREVER) ) {

			m_fTunnelGood = FALSE;

			m_fTunnelDone = FALSE;

			m_pTunnelData = pData;

			m_pTunnelDone->Wait(FOREVER);

			BOOL    fGood = m_fTunnelGood;

			m_pTunnelData = NULL;

			m_pTunnelFree->Set();

			return fGood;
		}
	}

	return FALSE;
}

// Comms Hooks

BOOL CModule::TxData(CProxyRack *pProxy)
{
	if( m_uState == stateBootForceReset ) {

		return pProxy->BootTxForceReset();
	}

	if( m_uState == stateBootCheckModel ) {

		return pProxy->BootTxCheckModel();
	}

	if( m_uState == stateBootCheckVersion ) {

		BYTE bData[16];

		if( !pProxy->GetFirmGUID(m_FirmID, bData) ) {

			m_uState = stateBootStartProgram;

			return pProxy->BootTxStartProgram();
		}

		return pProxy->BootTxCheckVersion(bData);
	}

	if( m_uState == stateBootProgramReset ) {

		return pProxy->BootTxProgramReset();
	}

	if( m_uState == stateBootClearProgram ) {

		return pProxy->BootTxClearProgram(m_dwFirmSize);
	}

	if( m_uState == stateBootProgramSize ) {

		return pProxy->BootTxProgramSize(m_dwFirmSize);
	}

	if( m_uState == stateBootWriteProgram ) {

		UINT uData = Min(m_uData, m_dwFirmSize - m_dwFirmAddr);

		memcpy(m_bData,
		       m_pFirmData + m_dwFirmAddr,
		       uData
		);

		if( m_fAddr32 ) {

			return pProxy->BootTxWriteProgram32(m_dwFirmAddr, m_bData, uData);
		}

		return pProxy->BootTxWriteProgram(m_dwFirmAddr, m_bData, uData);
	}

	if( m_uState == stateBootWriteVerify ) {

		DWORD Crc32 = ~CRC32(m_pFirmData, m_dwFirmSize, 0xFFFFFFFF);

		return pProxy->BootTxWriteVerify(Crc32);
	}

	if( m_uState == stateBootWriteVersion ) {

		BYTE bData[16];

		pProxy->GetFirmGUID(m_FirmID, bData);

		return pProxy->BootTxWriteVersion(bData);
	}

	if( m_uState == stateBootStartProgram ) {

		return pProxy->BootTxStartProgram();
	}

	if( m_uState == stateConfigCheckVersion ) {

		return pProxy->ConfigTxCheckVersion(m_Guid);
	}

	if( m_uState == stateConfigClearConfig ) {

		return pProxy->ConfigTxClearConfig();
	}

	if( m_uState == stateConfigWriteConfig ) {

		return pProxy->ConfigTxWriteConfig(m_pData);
	}

	if( m_uState == stateConfigWriteVersion ) {

		return pProxy->ConfigTxWriteVersion(m_Guid);
	}

	if( m_uState == stateConfigWritePersist ) {

		pProxy->DataTxStartFrame();

		OnWritePersistData(pProxy);

		return pProxy->DataTxSend();
	}

	if( m_uState == stateConfigWriteCached ) {

		pProxy->DataTxStartFrame();

		CheckCache(pProxy);

		return pProxy->DataTxSend();
	}

	if( m_uState == stateConfigStartSystem ) {

		return pProxy->ConfigTxStartSystem();
	}

	if( m_uState == stateConfigCheckStatus ) {

		return pProxy->ConfigTxCheckStatus();
	}

	if( m_uState == stateData1 ) {

		pProxy->DataTxStartFrame();

		CheckWrite(pProxy, FALSE);

		return pProxy->DataTxSend();
	}

	if( m_uState == stateData2 ) {

		pProxy->DataTxStartFrame();

		CheckWrite(pProxy, TRUE);

		CheckRead(pProxy);

		return pProxy->DataTxSend();
	}

	if( m_uState == stateReadPersist ) {

		pProxy->DataTxStartFrame();

		OnReadPersistData(pProxy);

		return pProxy->DataTxSend();
	}

	if( m_uState == stateTunnel ) {

		return pProxy->TunnelTx(m_pTunnelData);
	}

	return FALSE;
}

void CModule::RxData(CProxyRack *pProxy)
{
	if( m_uState == stateBootForceReset ) {

		if( pProxy->RxGeneral() ) {

			m_uState = stateBootCheckModel;

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateBootCheckModel ) {

		BOOL fSame;

		if( pProxy->BootRxCheckModel(fSame, m_ModelID) ) {

			if( fSame ) {

				m_uState = stateBootCheckVersion;
			}
			else {
				m_uState = stateBootForceReset;
			}

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateBootCheckVersion ) {

		BOOL fSame;

		if( pProxy->BootRxCheckVersion(fSame) ) {

			if( fSame ) {

				m_uState = stateBootStartProgram;
			}
			else {
				m_uState = stateBootProgramReset;
			}

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateBootProgramReset ) {

		if( pProxy->RxGeneral() ) {

			m_uState     = stateBootClearProgram;

			m_pFirmData  = pProxy->GetFirmData(m_FirmID);

			m_dwFirmSize = pProxy->GetFirmSize(m_FirmID);

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateBootClearProgram ) {

		if( pProxy->RxGeneral() ) {

			m_dwFirmAddr = 0x0000;

			if( m_dwFirmSize ) {

				if( WhoHasFeature(rfGraphiteModules) ) {

					m_uState = stateBootWriteProgram;
				}
				else {
					m_uState = stateBootProgramSize;
				}
			}
			else {
				m_uState = stateBootStartProgram;
			}

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateBootProgramSize ) {

		if( pProxy->RxGeneral() ) {

			m_uState = stateBootWriteProgram;

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateBootWriteProgram ) {

		if( pProxy->RxGeneral() ) {

			m_dwFirmAddr += m_uData;

			if( m_dwFirmAddr >= m_dwFirmSize ) {

				if( WhoHasFeature(rfGraphiteModules) ) {

					m_uState = stateBootWriteVersion;
				}
				else {
					m_uState = stateBootWriteVerify;
				}
			}
		}

		else {
			if( !WhoHasFeature(rfGraphiteModules) ) {

				m_uState = stateBootClearProgram;

				return;
			}
		}

		return;
	}

	if( m_uState == stateBootWriteVerify ) {

		BOOL fSame;

		if( pProxy->BootRxWriteVerify(fSame) ) {

			if( fSame ) {

				m_uState = stateBootStartProgram;

				return;
			}
			else {
				m_uState = stateBootClearProgram;

				return;
			}
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateBootWriteVersion ) {

		if( pProxy->RxGeneral() ) {

			m_uState = stateBootStartProgram;

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateBootStartProgram ) {

		if( pProxy->RxGeneral() ) {

			m_uState = stateConfigCheckVersion;

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateConfigCheckVersion ) {

		BOOL fSame;

		if( pProxy->ConfigRxCheckVersion(fSame) ) {

			if( fSame ) {

				if( FindPersistData() ) {

					m_uState = stateConfigWritePersist;
				}
				else {
					m_uState = stateConfigStartSystem;
				}
			}
			else {
				m_uState = stateConfigClearConfig;
			}

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateConfigClearConfig ) {

		if( pProxy->RxGeneral() ) {

			m_pData  = m_pInitData;

			m_uState = stateConfigWriteConfig;

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateConfigWriteConfig ) {

		if( pProxy->ConfigRxWriteConfig() ) {

			if( !((PWORD) (m_pData))[0] ) {

				m_uState = stateConfigWriteVersion;
			}

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateConfigWriteVersion ) {

		if( pProxy->RxGeneral() ) {

			if( FindPersistData() ) {

				m_uState = stateConfigWritePersist;
			}
			else {
				m_uState = stateConfigStartSystem;
			}

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateConfigWritePersist ) {

		PBYTE pData;

		if( pProxy->DataRxData(pData) ) {

			m_uWriteBlock = 0;

			m_nWritePos   = 0;

			m_uWriteSlot  = 0;

			m_fWriteWrap  = FALSE;

			if( m_uBlocks ) {

				m_uState = stateConfigWriteCached;
			}
			else {
				m_uState = stateConfigStartSystem;
			}

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateConfigWriteCached ) {

		PBYTE pData;

		if( pProxy->DataRxData(pData) ) {

			DoneWrite();

			if( m_fWriteWrap ) {

				m_uState = stateConfigStartSystem;
			}

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateConfigStartSystem ) {

		if( pProxy->RxGeneral() ) {

			m_uState = stateConfigCheckStatus;

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateConfigCheckStatus ) {

		BOOL fRun;

		if( pProxy->ConfigRxCheckStatus(fRun) ) {

			if( fRun ) {

				if( m_uPersistSize && !HasPersistData() ) {

					SetOnline();

					m_uState = stateReadPersist;
				}
				else {
					SetOnline();

					m_uState = stateData2;
				}
			}
			else {
				SetOnline();

				m_uState = stateData1;
			}

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateData1 ) {

		PBYTE pData;

		if( pProxy->DataRxData(pData) ) {

			DoneWrite();

			m_uState = stateConfigCheckStatus;

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateData2 ) {

		PBYTE pData;

		if( pProxy->DataRxData(pData) ) {

			DoneWrite();

			ParseRead(pData);

			if( m_fPersistRead ) {

				m_uState = stateReadPersist;

				return;
			}

			if( m_pTunnelData && !m_fTunnelDone ) {

				m_uState = stateTunnel;

				return;
			}

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateReadPersist ) {

		PBYTE pData;

		if( pProxy->DataRxData(pData) ) {

			ParsePersistData(pData);

			m_uState = stateData2;

			return;
		}

		RxFail(pProxy);

		return;
	}

	if( m_uState == stateTunnel ) {

		if( pProxy->TunnelRx(m_pTunnelData) ) {

			m_fTunnelGood = TRUE;

			m_fTunnelDone = TRUE;

			m_pTunnelDone->Set();

			m_uState = stateData2;

			return;
		}

		RxFail(pProxy);

		return;
	}

	RxFail(pProxy);
}

void CModule::RxFail(CProxyRack *pProxy)
{
	m_pDevice->SetError(errorSoft);

	if( m_pTunnelData ) {

		m_fTunnelGood = FALSE;

		m_fTunnelDone = TRUE;

		m_pTunnelDone->Set();
	}

	m_uState = stateBootCheckModel;
}

// Implementation

void CModule::ReadGuid(PCBYTE &pData)
{
	memcpy(m_Guid, pData, 16);

	pData += 16;
}

void CModule::ScanInit(PCBYTE &pData)
{
	for(;;) {

		WORD Cmd = GetWord(pData);

		if( Cmd ) {

			if( (HIBYTE(Cmd) & 0xC0) == 0x40 ) {

				WORD Data = GetWord(pData);

				WORD Prop = (Cmd & 0x3FFF);

				OnFilterInit(Prop, Data);
			}
		}
		else
			break;
	}
}

BOOL CModule::HasPersistData(void)
{
	return m_Persist[0] == 0x1234;
}

BOOL CModule::FindPersistData(void)
{
	if( m_uPersistSize ) {

		PersistGet( m_bDrop,
			    PBYTE (m_Persist),
			    sizeof(m_Persist)
			    );

		return HasPersistData();
	}

	return FALSE;
}

void CModule::ParsePersistData(PBYTE pData)
{
	BOOL fChange = !HasPersistData();

	for( UINT n = 0; n < m_uPersistSize; n++ ) {

		BYTE hi   = *(pData++);

		BYTE lo   = *(pData++);

		WORD Data = MAKEWORD(lo, hi);

		if( m_Persist[1 + n] != Data ) {

			m_Persist[1 + n] = Data;

			fChange = TRUE;
		}
	}

	if( fChange ) {

		m_Persist[0] = 0x1234;

		PersistPut( m_bDrop,
			    PBYTE (m_Persist),
			    sizeof(m_Persist)
		            );
	}
}

BOOL CModule::CheckRead(CProxyRack *pProxy)
{
	m_uReadSlot = 0;

	if( m_uBlocks ) {

		UINT uStart = m_uReadBlock;

		do {
			CCommsSysBlock *pBlock = m_pBlocks->m_ppBlock[m_uReadBlock];

			if( pBlock ) {

				while( m_nReadPos < pBlock->m_Size ) {

					if( pBlock->ShouldRead(m_nReadPos) ) {

						CAddress Addr = pBlock->GetAddress(m_nReadPos);

						WORD     Prop = LOWORD  (Addr.m_Ref);

						DWORD    From = MAKELONG(m_nReadPos, m_uReadBlock);

						if( pProxy->DataTxRead(Prop) ) {

							m_uReadFrom[m_uReadSlot] = From;

							m_uReadSlot++;

							m_nReadPos++;

							continue;
						}

						return TRUE;
					}

					m_nReadPos++;
				}
			}

			m_nReadPos   = 0;

			m_uReadBlock = (m_uReadBlock+1) % m_uBlocks;

		} while( m_uReadBlock - uStart );
	}

	return m_uReadSlot > 0;
}

BOOL CModule::CheckWrite(CProxyRack *pProxy, BOOL fLimit)
{
	m_uWriteSlot = 0;

	CheckWriteQueue(pProxy, fLimit);

	CheckWriteBlock(pProxy, fLimit);

	return m_uWriteSlot > 0;
}

BOOL CModule::CheckWriteBlock(CProxyRack *pProxy, BOOL fLimit)
{
	if( m_uBlocks ) {

		UINT uStart = m_uWriteBlock;

		do {
			CCommsSysBlock *pBlock = m_pBlocks->m_ppBlock[m_uWriteBlock];

			if( pBlock ) {

				while( m_nWritePos < pBlock->m_Size ) {

					if( pBlock->ShouldWrite(m_nWritePos) ) {

						CAddress Addr = pBlock->GetAddress(m_nWritePos);

						WORD     Prop = LOWORD(Addr.m_Ref);

						if( fLimit && pProxy->DataTooFull(Prop) ) {

							return TRUE;
						}
						else {
							DWORD Data = pBlock->GetWriteData(m_nWritePos);

							DWORD Wire = DispToLink(Prop, Data);

							DWORD From = MAKELONG(m_nWritePos, m_uWriteBlock);

							if( pProxy->DataTxWrite(Prop, Wire) ) {

								if( m_uPersistSize ) {

									if( OnFilterWrite(Prop, Data) ) {

										m_fPersistRead = TRUE;
									}
								}

								m_uWriteFrom[m_uWriteSlot] = From;

								m_uWriteSlot++;

								m_nWritePos++;

								continue;
							}

							return TRUE;
						}
					}

					m_nWritePos++;
				}
			}

			m_nWritePos   = 0;

			m_uWriteBlock = (m_uWriteBlock+1) % m_uBlocks;

		} while( m_uWriteBlock - uStart );
	}

	return TRUE;
}

BOOL CModule::CheckWriteQueue(CProxyRack *pProxy, BOOL fLimit)
{
	for( UINT n = 0; n < 2; n++ ) {

		CWriteQueue *pQueue = m_pDevice->GetQueue(n);

		if( pQueue ) {

			if( !pQueue->IsEmpty() ) {

				UINT   uLimit = pQueue->GetLimit();

				UINT   uCount = 0;

				CWrite Write;

				while( pQueue->GetEntry(uCount, Write) ) {

					CAddress Addr = Write.m_pBlock->GetAddress(Write.m_nPos);

					WORD     Prop = LOWORD(Addr.m_Ref);

					if( fLimit && pProxy->DataTooFull(Prop) ) {

						pQueue->DropLast();

						return TRUE;
					}
					else {
						DWORD Data = Write.m_Data;

						DWORD Wire = DispToLink(Prop, Data);

						DWORD From = MAKELONG(n, 10000);

						if( pProxy->DataTxWrite(Prop, Wire) ) {

							if( m_uPersistSize ) {

								if( OnFilterWrite(Prop, Data) ) {

									m_fPersistRead = TRUE;
								}
							}

							m_uWriteFrom[m_uWriteSlot] = From;

							m_uWriteSlot++;

							if( ++uCount == uLimit ) {

								break;
							}

							continue;
						}

						pQueue->DropLast();

						return TRUE;
					}
				}
			}
		}
	}

	return TRUE;
}

BOOL CModule::CheckCache(CProxyRack *pProxy)
{
	if( m_uBlocks ) {

		UINT uStart = m_uWriteBlock;

		do {
			CCommsSysBlock *pBlock = m_pBlocks->m_ppBlock[m_uWriteBlock];

			if( pBlock ) {

				while( m_nWritePos < pBlock->m_Size ) {

					if( pBlock->IsCached(m_nWritePos) ) {

						CAddress Addr = pBlock->GetAddress(m_nWritePos);

						WORD     Prop = LOWORD(Addr.m_Ref);

						DWORD    Data = pBlock->GetWriteData(m_nWritePos);

						DWORD    Wire = DispToLink(Prop, Data);

						DWORD    From = MAKELONG(m_nWritePos, m_uWriteBlock);

						if( pProxy->DataTxWrite(Prop, Wire) ) {

							if( m_uPersistSize ) {

								if( OnFilterWrite(Prop, Data) ) {

									m_fPersistRead = TRUE;
								}
							}

							m_uWriteFrom[m_uWriteSlot] = From;

							m_uWriteSlot++;

							m_nWritePos++;

							continue;
						}

						return TRUE;
					}

					m_nWritePos++;
				}
			}

			m_nWritePos   = 0;

			m_uWriteBlock = (m_uWriteBlock+1) % m_uBlocks;

		} while( m_uWriteBlock - uStart );

		m_fWriteWrap = TRUE;
	}

	return TRUE;
}

void CModule::ParseRead(PBYTE pData)
{
	for( UINT i = 0; i < m_uReadSlot; i++ ) {

		UINT            uBlock = HIWORD(m_uReadFrom[i]);

		INT             nPos   = LOWORD(m_uReadFrom[i]);

		CCommsSysBlock *pBlock = m_pBlocks->m_ppBlock[uBlock];

		CAddress        Addr   = pBlock->GetAddress(nPos);

		WORD            Prop   = LOWORD(Addr.m_Ref);

		LONG            Data   = 0;

		switch( (HIBYTE(Prop) & 0x07) ) {

			case TYPE_BOOL:

				Data = *(pData++) ? 1 : 0;

				pBlock->SetCommsData(nPos, Data);

				break;

			case TYPE_BYTE:

				Data = *(pData++);

				pBlock->SetCommsData(nPos, Data);

				break;

			case TYPE_WORD:

				Data = MAKEWORD(pData[1], pData[0]);

				Data = LinkToDisp(Prop, Data);

				pBlock->SetCommsData(nPos, Data);

				pData += 2;

				break;

			case TYPE_LONG:
			case TYPE_REAL:

				Data = MAKEWORD(pData[1], pData[0]);

				Data = LinkToDisp(Prop, Data);

				pBlock->SetCommsData(nPos, Data);

				pData += 2;

				break;

			case TYPE_INT32:
			case TYPE_UINT32:

				Data = MAKELONG(MAKEWORD(pData[3], pData[2]), MAKEWORD(pData[1], pData[0]));

				Data = LinkToDisp(Prop, Data);

				pBlock->SetCommsData(nPos, Data);

				pData += 4;

				break;

			default:

				pBlock->SetCommsData(nPos, Data);

				break;
		}
	}
}

void CModule::DoneWrite(void)
{
	for( UINT i = 0; i < m_uWriteSlot; i++ ) {

		UINT uBlock = HIWORD(m_uWriteFrom[i]);

		INT  nPos   = LOWORD(m_uWriteFrom[i]);

		if( uBlock == 10000 ) {

			CWriteQueue *pQueue = m_pDevice->GetQueue(nPos);

			pQueue->WriteDone(1);
		}
		else {
			CCommsSysBlock *pBlock = m_pBlocks->m_ppBlock[uBlock];

			pBlock->SetWriteDone(nPos);
		}
	}
}

void CModule::SetOnline(void)
{
	m_pDevice->SetError(errorNone);

	for( UINT n = 0; n < m_uBlocks; n++ ) {

		m_pBlocks->m_ppBlock[n]->SetError(errorNone);
	}

	m_uReadBlock  = 0;

	m_nReadPos    = 0;

	m_uReadSlot   = 0;

	m_uWriteBlock = 0;

	m_nWritePos   = 0;

	m_uWriteSlot  = 0;
}

// Overridables

void CModule::OnLoad(PCBYTE &pData)
{
	////////////////////////////////////////////////////////////
	// Read module-specfic config data from stream.
	////////////////////////////////////////////////////////////
}

void CModule::OnReadPersistData(CProxyRack *pProxy)
{
	////////////////////////////////////////////////////////////
	// Call pProxy to add read requests for persisted data.
	////////////////////////////////////////////////////////////
}

void CModule::OnWritePersistData(CProxyRack *pProxy)
{
	////////////////////////////////////////////////////////////
	// Call pProxy to add write requests for persisted data.
	////////////////////////////////////////////////////////////
}

void CModule::OnFilterInit(WORD Prop, DWORD Data)
{
	////////////////////////////////////////////////////////////
	// Filter initialization data as required to find scaling etc.
	////////////////////////////////////////////////////////////
}

BOOL CModule::OnFilterWrite(WORD Prop, DWORD Data)
{
	////////////////////////////////////////////////////////////
	// Return TRUE if a write indicates data needs persisting.
	////////////////////////////////////////////////////////////

	return FALSE;
}

DWORD CModule::LinkToDisp(WORD Prop, DWORD Data)
{
	////////////////////////////////////////////////////////////
	// Scale a module's value to the form seen by the user.
	////////////////////////////////////////////////////////////

	return Data;
}

DWORD CModule::DispToLink(WORD Prop, DWORD Data)
{
	////////////////////////////////////////////////////////////
	// Scale a user's value to the form used by the module.
	////////////////////////////////////////////////////////////

	return Data;
}

// End of File
