
#include "intern.hpp"

#include "ManticoreDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Manticore Driver
//

// Instantiator

IRackDriver * Create_ManticoreDriver(IRackHandler *pHandler)
{
	return New CManticoreDriver(pHandler);
}

// Constructor

CManticoreDriver::CManticoreDriver(IRackHandler *pHandler)
{
	StdSetRef();

	m_pHandler = pHandler;
}

// Destructor

CManticoreDriver::~CManticoreDriver(void)
{
	AfxRelease(m_pHandler);
}

// IUnknown

HRESULT CManticoreDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IRackDriver);

	StdQueryInterface(IRackDriver);

	return E_NOINTERFACE;
}

ULONG CManticoreDriver::AddRef(void)
{
	StdAddRef();
}

ULONG CManticoreDriver::Release(void)
{
	StdRelease();
}

// IRackDriver

UINT CManticoreDriver::RxGeneral(BYTE bDrop)
{
	return m_uMsg;
}

UINT CManticoreDriver::BootTxForceReset(BYTE bDrop)
{
	NewPacket(servApp, bootEnterLoader);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::BootTxCheckModel(BYTE bDrop)
{
	NewPacket(servApp, bootCheckModel);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::BootTxCheckVersion(BYTE bDrop, PBYTE pGuid)
{
	m_SemVer[bDrop] = *PCDWORD(pGuid);

	NewPacket(servApp, bootCheckVersion);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::BootTxProgramReset(BYTE bDrop)
{
	NewPacket(servApp, appBootLoader);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::BootTxClearProgram(BYTE bDrop, DWORD dwSize)
{
	NewPacket(servBoot, bootClearProgram);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::BootTxProgramSize(BYTE bDrop, DWORD dwSize)
{
	NewPacket(servBoot, bootProgramSize);

	AddLong(dwSize);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::BootTxWriteProgram(BYTE bDrop, WORD wAddr, PBYTE pData, WORD wCount)
{
	NewPacket(servBoot, bootWriteProgram);

	AddData(pData, wCount);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::BootTxWriteProgram32(BYTE bDrop, DWORD dwAddr, PBYTE pData, WORD wCount)
{
	NewPacket(servBoot, bootWriteProgram);

	AddData(pData, wCount);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::BootTxWriteVerify(BYTE bDrop, DWORD Crc32)
{
	NewPacket(servBoot, bootVerifyCrc);

	AddLong(Crc32);

	m_fKeepAck = TRUE;

	return PutFrame(bDrop);
}

UINT CManticoreDriver::BootTxWriteVersion(BYTE bDrop, PBYTE pGuid)
{
	return msgError;
}

UINT CManticoreDriver::BootTxStartProgram(BYTE bDrop)
{
	NewPacket(servBoot, bootStartProgram);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::BootRxCheckModel(BYTE bDrop, BYTE &ModelID)
{
	if( m_uMsg == msgFrame ) {

		ModelID = m_bData[3] + 0x20;

		return msgFrame;
	}

	return msgError;
}

UINT CManticoreDriver::BootRxWriteVerify(BYTE bDrop, BOOL &fSame)
{
	if( m_uMsg == msgAck ) {

		fSame = TRUE;

		Debug("Info(%2.2u) - Verify CRC: OK\n", bDrop);

		return msgFrame;
	}

	if( m_uMsg == msgFrame ) {

		if( m_bData[0] == servBoot ) {

			if( m_bData[1] == opNak ) {

				fSame = FALSE;

				Debug("Warn(%2.2u) - Verify CRC: FAIL\n", bDrop);

				return msgFrame;
			}
		}
	}

	return msgError;
}

UINT CManticoreDriver::BootRxCheckVersion(BYTE bDrop, BOOL &fSame)
{
	if( m_uMsg == msgFrame ) {

		if( m_bData[0] == servApp ) {

			if( m_bData[1] == bootCheckVersion ) {

				DWORD const &Data = (DWORD &) m_bData[3];

				DWORD const &Test = (DWORD &) m_SemVer[bDrop];

				fSame = (Data == Test);

				Debug("Info(%2.2u) - Firmware Version %d.%d.%d-%d\n", bDrop, m_bData[3], m_bData[4], m_bData[5], m_bData[6]);

				return msgFrame;
			}

			if( m_bData[1] == opNak ) {

				fSame = FALSE;

				Debug("Info(%2.2u) - Firmware Invalid\n", bDrop);

				return msgFrame;
			}
		}
	}

	return msgError;
}

UINT CManticoreDriver::ConfigTxCheckVersion(BYTE bDrop, PBYTE pGuid)
{
	NewPacket(servConfig, configCheckVersion);

	AddData(pGuid, 16);

	m_fNoHeader = TRUE;

	return PutFrame(bDrop);
}

UINT CManticoreDriver::ConfigTxClearConfig(BYTE bDrop)
{
	NewPacket(servConfig, configClearConfig);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::ConfigTxWriteConfig(BYTE bDrop, PCBYTE &pData)
{
	NewPacket(servConfig, configWriteConfig);

	for(;;) {

		Item_Align2(pData);

		WORD Comm = MotorToHost((LPWORD(pData))[0]);

		if( Comm ) {

			UINT uCmd  = (HIBYTE(Comm) & 0xC0);

			UINT uType = (HIBYTE(Comm) & 0x07);

			Debug("Cmd=0x%2.2X, Type=0x%2.2X", uCmd, uType);

			if( uCmd == CMD_SET ) {

				Debug(", SET OBJ=%d PROP:0x%2.2X\n", ((HIBYTE(Comm) >> 3) & 0x7), LOBYTE(Comm));

				if( m_uPtr < sizeof(m_bData) - 2 ) {

					AddWord(Comm);

					pData += sizeof(WORD);

					continue;
				}

				break;
			}

			if( uCmd == CMD_CLEAR ) {

				Debug(", CLR OBJ=%d PROP:0x%2.2X\n", ((HIBYTE(Comm) >> 3) & 0x7), LOBYTE(Comm));

				if( m_uPtr < sizeof(m_bData) - 2 ) {

					AddWord(Comm);

					pData += sizeof(WORD);

					continue;
				}

				break;
			}

			if( uCmd == CMD_WRITE ) {

				Debug(", WRITE OBJ=%d PROP:0x%2.2X", ((HIBYTE(Comm) >> 3) & 0x7), LOBYTE(Comm));

				if( uType == 0x02 ) {

					if( m_uPtr < sizeof(m_bData) - 3 ) {

						BYTE Data = BYTE(MotorToHost((LPWORD(pData))[1]));

						Debug(", Data=0x%2.2X\n", Data);

						AddWord(Comm);

						AddByte(Data);

						pData += (sizeof(WORD) + sizeof(BYTE));

						continue;
					}

					break;
				}

				if( uType == 0x03 || uType == 0x04 || uType == 0x05 ) {

					if( m_uPtr < sizeof(m_bData) - 4 ) {

						WORD Data = MotorToHost((LPWORD(pData))[1]);

						Debug(", Data=0x%4.4X\n", Data);

						AddWord(Comm);

						AddWord(Data);

						pData += (sizeof(WORD) + sizeof(WORD));

						continue;
					}

					break;
				}

				if( uType == 0x06 || uType == 0x07 ) {

					if( m_uPtr < sizeof(m_bData) - 6 ) {

						pData += sizeof(WORD);

						Item_Align4(pData);

						DWORD Data = MotorToHost((LPDWORD(pData))[0]);

						Debug(", Data=0x%8.8X\n", Data);

						AddWord(Comm);

						AddLong(Data);

						pData += sizeof(DWORD);

						continue;
					}

					break;
				}
			}
		}
		else
			break;
	}

	return PutFrame(bDrop);
}

UINT CManticoreDriver::ConfigTxWriteVersion(BYTE bDrop, PBYTE pGuid)
{
	Debug("ConfigTxWriteVersion\n");

	/*AfxDump(pGuid, 16);*/

	NewPacket(servConfig, configWriteVersion);

	AddData(pGuid, 16);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::ConfigTxStartSystem(BYTE bDrop)
{
	NewPacket(servConfig, configStartSystem);

	return PutFrame(bDrop);
}

UINT CManticoreDriver::ConfigTxCheckStatus(BYTE bDrop)
{
	NewPacket(servConfig, configCheckStatus);

	m_fNoHeader = TRUE;

	return PutFrame(bDrop);
}

UINT CManticoreDriver::ConfigRxCheckVersion(BYTE bDrop, BOOL &fSame)
{
	if( m_uMsg == msgFrame ) {

		Debug("Info(%2.2u) Version = %s\n", bDrop, m_bData[0] ? "SAME" : "NOT SAME");

		fSame = m_bData[0];

		return msgFrame;
	}

	return msgError;
}

UINT CManticoreDriver::ConfigRxWriteConfig(BYTE bDrop)
{
	return msgFrame;
}

UINT CManticoreDriver::ConfigRxCheckStatus(BYTE bDrop, BOOL &fRun)
{
	if( m_uMsg == msgFrame ) {

		Debug("Info(%2.2u) - Status = %s\n", bDrop, m_bData[0] ? "RUNNING" : "STOPPED");

		fRun = m_bData[0];

		return msgFrame;
	}

	return msgError;
}

void CManticoreDriver::DataTxStartFrame(void)
{
	NewPacket(servData, dataData);

	m_fNoHeader = true;
}

BOOL CManticoreDriver::DataTxWrite(WORD PropID, DWORD Data)
{
	UINT uType = HIBYTE(PropID) & 0x07;

	if( uType == TYPE_BOOL ) {

		if( m_uPtr < sizeof(m_bData) - 2 ) {

			if( Data ) {

				Debug("DATA SET : 0x%4.4X\n", PropID);

				AddWord(PropID | (CMD_SET   << 8));
			}
			else {
				Debug("DATA CLR : 0x%4.4X\n", PropID);

				AddWord(PropID | (CMD_CLEAR << 8));
			}

			return TRUE;
		}

		return FALSE;
	}

	if( uType == TYPE_BYTE ) {

		if( m_uPtr < sizeof(m_bData) - 3 ) {

			Debug("DATA WRITE : 0x%4.4X : 0x%2.2X\n", PropID, BYTE(Data));

			AddWord(PropID | (CMD_WRITE << 8));

			AddByte(Data);

			return TRUE;
		}

		return FALSE;
	}

	if( uType == TYPE_WORD || uType == TYPE_LONG || uType == TYPE_REAL ) {

		if( m_uPtr < sizeof(m_bData) - 4 ) {

			Debug("DATA WRITE : 0x%4.4X : 0x%4.4X\n", PropID, WORD(Data));

			AddWord(PropID | (CMD_WRITE << 8));

			AddWord(Data);

			return TRUE;
		}

		return FALSE;
	}

	if( uType == TYPE_INT32 || uType == TYPE_UINT32 ) {

		if( m_uPtr < sizeof(m_bData) - 6 ) {

			Debug("DATA WRITE : 0x%4.4X : 0x%8.8X\n", PropID, DWORD(Data));

			AddWord(PropID | (CMD_WRITE << 8));

			AddLong(Data);

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CManticoreDriver::DataTxRead(WORD PropID)
{
	if( m_uPtr < sizeof(m_bData) - 2 ) {

		/*Debug("DATA READ : 0x%4.4X\n", PropID);*/

		AddWord(PropID | (CMD_READ << 8));

		return TRUE;
	}

	return FALSE;
}

UINT CManticoreDriver::DataTxSend(BYTE bDrop)
{
	return PutFrame(bDrop);
}

UINT CManticoreDriver::DataRxData(BYTE bDrop, PBYTE &pData)
{
	pData = m_bData;

	return msgFrame;
}

BOOL CManticoreDriver::DataTooFull(WORD PropID)
{
	UINT uType = HIBYTE(PropID) & 0x07;

	if( uType == TYPE_BOOL ) {

		if( m_uPtr < sizeof(m_bData) - 4 ) {

			return FALSE;
		}

		return TRUE;
	}

	if( uType == TYPE_BYTE ) {

		if( m_uPtr < sizeof(m_bData) - 5 ) {

			return FALSE;
		}

		return TRUE;
	}

	if( uType == TYPE_WORD || uType == TYPE_LONG || uType == TYPE_REAL ) {

		if( m_uPtr < sizeof(m_bData) - 6 ) {

			return FALSE;
		}

		return TRUE;
	}

	if( uType == TYPE_INT32 || uType == TYPE_UINT32 ) {

		if( m_uPtr < sizeof(m_bData) - 8 ) {

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

UINT CManticoreDriver::TunnelTx(BYTE bDrop, PBYTE pData)
{
	NewPacket(servCalib, pData[2]);

	m_fNoHeader = TRUE;

	AddByte(pData[4]);

	if( pData[2] == OP_CALIB_REQ ) {

		AddWord(MAKEWORD(pData[6], pData[5]));
	}

	return PutFrame(bDrop);
}

UINT CManticoreDriver::TunnelRx(BYTE bDrop, PBYTE pData)
{
	pData[0] = servCalib;

	pData[1] = 0x00;

	memcpy(pData + 2, m_bData, 4);

	return msgFrame;
}

UINT CManticoreDriver::SendEnq(BYTE bDrop)
{
	m_uMsg = m_pHandler->Transact(bDrop, msgEnq);

	switch( m_uMsg ) {

		case msgNone:
		case msgBusy:
		case msgNak:
		case msgAck:

			return m_uMsg;

		case msgFrame:

			m_pHandler->GetReply(bDrop, m_bData);

			m_pHandler->Transact(bDrop, msgAck);

			return msgFrame;
	}

	return msgError;
}

BOOL CManticoreDriver::Recycle(BYTE bDrop)
{
	return m_pHandler->Recycle(bDrop);
}

// Frame Building

void CManticoreDriver::NewPacket(BYTE bService, BYTE bCommand)
{
	m_bData[0]  = bService;

	m_bData[1]  = bCommand;

	m_bData[2]  = 0;

	m_uPtr      = 3;

	m_uMsg      = msgNone;

	m_fNoHeader = FALSE;

	m_fKeepAck  = FALSE;
}

void CManticoreDriver::AddByte(BYTE bData)
{
	m_bData[m_uPtr++] = bData;
}

void CManticoreDriver::AddWord(WORD wData)
{
	m_bData[m_uPtr++] = HIBYTE(wData);

	m_bData[m_uPtr++] = LOBYTE(wData);
}

void CManticoreDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
}

void CManticoreDriver::AddData(PBYTE pData, UINT uCount)
{
	memcpy(m_bData + m_uPtr, pData, uCount);

	m_uPtr += uCount;
}

// Transport

UINT CManticoreDriver::PutFrame(BYTE bDrop)
{
	m_bData[2] = m_uPtr - 3;

	m_uMsg = m_pHandler->Transact(bDrop, m_bData, m_uPtr);

	switch( m_uMsg ) {

		case msgFrame:

			m_pHandler->GetReply(bDrop, m_bData);

			if( !m_fNoHeader && m_bData[1] == opSyn ) {

				return m_uMsg = msgPoll;
			}

			return m_uMsg;

		case msgAck:

			return m_fKeepAck ? m_uMsg : m_uMsg = msgFrame;

		case msgNone:
		case msgError:
		case msgNak:
		case msgBusy:
		case msgEnq:

			return m_uMsg;
	}

	return m_uMsg = msgError;
}

// Debugging

void CManticoreDriver::Debug(PCTXT pForm, ...)
{
	#if defined(_XDEBUG)

	va_list pArgs;

	va_start(pArgs, pForm);

	AfxTraceArgs(pForm, pArgs);

	va_end(pArgs);

	#endif
	}

// End of File
