
#include "intern.hpp"

#include "modtc8i.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Isolated TC8 Module Configuration
//

// Instantiator

CModule * Create_TC8I(void)
{
	return New CTC8IModule;
	}

// Constructor

CTC8IModule::CTC8IModule(void)
{
	m_FirmID = FIRM_TC8ISO;
	}

// Destructor

CTC8IModule::~CTC8IModule(void)
{
	}

// Overridables

void CTC8IModule::OnLoad(PCBYTE &pData)
{
	GetWord(pData);
	}

// End of File
