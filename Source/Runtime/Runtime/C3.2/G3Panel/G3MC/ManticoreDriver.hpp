
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ManticoreDriver_HPP

#define	INCLUDE_ManticoreDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Interfaces
//

#include "RackInterface.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Manticore Driver
//

class CManticoreDriver : public IRackDriver
{
public:
	// Constructor
	CManticoreDriver(IRackHandler *pHandler);

	// Destructor
	~CManticoreDriver(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IRackDriver
	UINT RxGeneral(BYTE bDrop);
	UINT BootTxForceReset(BYTE bDrop);
	UINT BootTxCheckModel(BYTE bDrop);
	UINT BootTxCheckVersion(BYTE bDrop, PBYTE pGuid);
	UINT BootTxProgramReset(BYTE bDrop);
	UINT BootTxClearProgram(BYTE bDrop, DWORD dwSize);
	UINT BootTxProgramSize(BYTE bDrop, DWORD dwSize);
	UINT BootTxWriteProgram(BYTE bDrop, WORD wAddr, PBYTE pData, WORD wCount);
	UINT BootTxWriteProgram32(BYTE bDrop, DWORD dwAddr, PBYTE pData, WORD wCount);
	UINT BootTxWriteVerify(BYTE bDrop, DWORD Crc32);
	UINT BootTxWriteVersion(BYTE bDrop, PBYTE pGuid);
	UINT BootTxStartProgram(BYTE bDrop);
	UINT BootRxCheckModel(BYTE bDrop, BYTE &ModelID);
	UINT BootRxWriteVerify(BYTE bDrop, BOOL &fSame);
	UINT BootRxCheckVersion(BYTE bDrop, BOOL &fSame);
	UINT ConfigTxCheckVersion(BYTE bDrop, PBYTE pGuid);
	UINT ConfigTxClearConfig(BYTE bDrop);
	UINT ConfigTxWriteConfig(BYTE bDrop, PCBYTE &pData);
	UINT ConfigTxWriteVersion(BYTE bDrop, PBYTE pGuid);
	UINT ConfigTxStartSystem(BYTE bDrop);
	UINT ConfigTxCheckStatus(BYTE bDrop);
	UINT ConfigRxCheckVersion(BYTE bDrop, BOOL &fSame);
	UINT ConfigRxWriteConfig(BYTE bDrop);
	UINT ConfigRxCheckStatus(BYTE bDrop, BOOL &fRun);
	void DataTxStartFrame(void);
	BOOL DataTxWrite(WORD PropID, DWORD Data);
	BOOL DataTxRead(WORD PropID);
	UINT DataTxSend(BYTE bDrop);
	UINT DataRxData(BYTE bDrop, PBYTE &pData);
	BOOL DataTooFull(WORD PropID);
	UINT TunnelTx(BYTE bDrop, PBYTE pData);
	UINT TunnelRx(BYTE bDrop, PBYTE pData);
	UINT SendEnq(BYTE bDrop);
	BOOL Recycle(BYTE bDrop);

protected:
	// Service Codes
	enum
	{
		servBoot		= 0x01,
		servConfig		= 0x02,
		servData		= 0x03,
		servCalib		= 0x04,
		servTunnel		= 0x05,
		servApp			= 0x06,
	};

	// Shared
	enum
	{
		opAck			= 0x01,
		opNak			= 0x02,
		opSyn			= 0x03,
	};

	// Boot
	enum
	{
		bootCheckVersion	= 0x10,
		bootClearProgram	= 0x11,
		bootWriteProgram	= 0x12,
		bootVerifyCrc		= 0x13,
		bootStartProgram	= 0x14,
		bootCheckHardware	= 0x15,
		bootForceReset		= 0x16,
		bootCheckModel		= 0x18,
		bootProgramSize		= 0x19,
		bootEnterLoader		= 0x1A,
	};

	// System
	enum
	{
		systemCheckVersion	= 0x10,
		systemClearConfig	= 0x11,
		systemWriteConfig	= 0x12,
		systemStopSystem	= 0x13,
		systemStartSystem	= 0x14,
	};

	// Config
	enum
	{
		configCheckVersion	= 0x10,
		configClearConfig	= 0x11,
		configWriteConfig	= 0x12,
		configWriteVersion	= 0x13,
		configStartSystem	= 0x14,
		configCheckStatus	= 0x15,
	};

	// Data
	enum
	{
		dataData		= 0x10,
		dataSync		= 0x11,
	};

	// Calibration 
	enum
	{
		calibRead		= 0x10,
		calibWrite		= 0x11,
		calibSave		= 0x12,
		calibWriteFull		= 0x13,
		calibReadFull		= 0x14,
		calibAutoCheck		= 0x15,
		calibAutoStart		= 0x16,
	};

	// Application
	enum
	{
		appCheckVersion		= 0x10,
		appCheckHardware	= 0x15,
		appForceReset		= 0x16,
		appWriteMac		= 0x17,
		appCheckModel		= 0x18,
		appBootLoader		= 0x1A,
		appCheckSerial		= 0x1B,
	};

	// Data
	ULONG          m_uRefs;
	IRackHandler * m_pHandler;
	BYTE	       m_bData[255];
	UINT	       m_uPtr;
	UINT           m_uMsg;
	BOOL	       m_fKeepAck;
	BOOL	       m_fNoHeader;
	DWORD          m_SemVer[16];

	// Frame Building
	void NewPacket(BYTE bService, BYTE bCommand);
	void AddByte(BYTE bData);
	void AddWord(WORD wData);
	void AddLong(DWORD dwData);
	void AddData(PBYTE pData, UINT uCount);

	// Transport
	UINT PutFrame(BYTE bDrop);

	// Debugging
	void Debug(PCTXT pForm, ...);
};

// End of File

#endif
