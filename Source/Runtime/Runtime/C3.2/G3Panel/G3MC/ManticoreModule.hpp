
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ManticoreModule_HPP

#define	INCLUDE_ManticoreModule_HPP

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Manticore Module Configuration
//

class CManticoreModule : public CModule
{
	public:
		// Constructor
		CManticoreModule(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);		
		};

// End of File

#endif
