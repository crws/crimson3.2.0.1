
#include "intern.hpp"

#include "moddlc.hpp"

#include "ProxyRack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DLC Module Configuration
//

// Instantiator

CModule * Create_DLC(void)
{
	return New CDLCModule;
	}

// Constructor

CDLCModule::CDLCModule(void)
{
	m_FirmID       = FIRM_PID2;

	m_uPersistSize = 8;

	m_fTuning[0]   = FALSE;

	m_fTuning[1]   = FALSE;
	}

// Destructor

CDLCModule::~CDLCModule(void)
{
	}

// Overridables

void CDLCModule::OnLoad(PCBYTE &pData)
{
	for( BOOL n= 0; n < 2; n++ ) {

		m_TempUnits[n]	= GetWord(pData);
		
		m_ProcMin  [n]	= GetWord(pData);
		
		m_ProcMax  [n]	= GetWord(pData);
		
		GetWord(pData);
	
		FindScaling(n);
		}			
	}

void CDLCModule::OnReadPersistData(CProxyRack *pRack)
{
	pRack->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_P);

	pRack->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_I);

	pRack->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_D);

	pRack->DataTxRead((OBJ_LOOP_1 << 11) | PROPID_AUTO_FILTER);

	pRack->DataTxRead((OBJ_LOOP_2 << 11) | PROPID_AUTO_CONST_P);

	pRack->DataTxRead((OBJ_LOOP_2 << 11) | PROPID_AUTO_CONST_I);

	pRack->DataTxRead((OBJ_LOOP_2 << 11) | PROPID_AUTO_CONST_D);

	pRack->DataTxRead((OBJ_LOOP_2 << 11) | PROPID_AUTO_FILTER);
	}

void CDLCModule::OnWritePersistData(CProxyRack *pRack)
{
	pRack->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_P, m_Persist[1]);

	pRack->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_I, m_Persist[2]);

	pRack->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_CONST_D, m_Persist[3]);

	pRack->DataTxWrite((OBJ_LOOP_1 << 11) | PROPID_AUTO_FILTER,  m_Persist[4]);

	pRack->DataTxWrite((OBJ_LOOP_2 << 11) | PROPID_AUTO_CONST_P, m_Persist[5]);	

	pRack->DataTxWrite((OBJ_LOOP_2 << 11) | PROPID_AUTO_CONST_I, m_Persist[6]);	

	pRack->DataTxWrite((OBJ_LOOP_2 << 11) | PROPID_AUTO_CONST_D, m_Persist[7]);	

	pRack->DataTxWrite((OBJ_LOOP_2 << 11) | PROPID_AUTO_FILTER,  m_Persist[8]);
	}

void CDLCModule::OnFilterInit(WORD PropID, DWORD Data)
{
	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {

		INT n;

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_ALARM_MODE_1): n = 0; break;
			case LOBYTE(PROPID_ALARM_MODE_2): n = 1; break;
			case LOBYTE(PROPID_ALARM_MODE_3): n = 2; break;
			case LOBYTE(PROPID_ALARM_MODE_4): n = 3; break;

			default:
				return;
			}

		switch( Data ) {

			case ALARM_NULL:
				m_fDelta[0][n] = TRUE;
				break;

			case ALARM_ABS_LO:
			case ALARM_ABS_HI:
				m_fDelta[0][n] = FALSE;
				break;

			default:
				m_fDelta[0][n] = TRUE;
				break;
			}
		}

	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_2 ) {

		INT n;

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_ALARM_MODE_1): n = 0; break;
			case LOBYTE(PROPID_ALARM_MODE_2): n = 1; break;
			case LOBYTE(PROPID_ALARM_MODE_3): n = 2; break;
			case LOBYTE(PROPID_ALARM_MODE_4): n = 3; break;

			default:
				return;
			}

		switch( Data ) {

			case ALARM_NULL:
				m_fDelta[1][n] = TRUE;
				break;

			case ALARM_ABS_LO:
			case ALARM_ABS_HI:
				m_fDelta[1][n] = FALSE;
				break;

			default:
				m_fDelta[1][n] = TRUE;
				break;
			}
		}
	}

BOOL CDLCModule::OnFilterWrite(WORD PropID, DWORD Data)
{
	if( PropID == ((OBJ_LOOP_1 << 11) | PROPID_REQ_TUNE) ) {

		if( Data ) {

			m_fTuning[0] = TRUE;
			}
		else {
			if( m_fTuning[0] ) {

				m_fTuning[0] = FALSE;

				return TRUE;
				}

			m_fTuning[0] = FALSE;
			}
		}

	if( PropID == ((OBJ_LOOP_2 << 11) | PROPID_REQ_TUNE) ) {

		if( Data ) {

			m_fTuning[1] = TRUE;
			}
		else {
			if( m_fTuning[1] ) {

				m_fTuning[1] = FALSE;

				return TRUE;
				}

			m_fTuning[1] = FALSE;
			}
		}

	return FALSE;
	}

DWORD CDLCModule::LinkToDisp(WORD PropID, DWORD Data)
{
	BOOL fDelta;

	if( IsScaled(PropID, fDelta) ) {

		if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {  
						
			LONG a = MakeLinkLong(Data, fDelta);

			a -= 0;

			a *= (m_p2[0] - m_p1[0]);

			if( a > 0 ) a += m_fs[0] / 2;

			if( a < 0 ) a -= m_fs[0] / 2;

			a /= m_fs[0];

			a += fDelta ? 0 : m_p1[0];

			return WORD(a);
			}
		
		if( ((PropID >> 11) & 0x07) == OBJ_LOOP_2 ) {
						
			LONG a = MakeLinkLong(Data, fDelta);

			a -= 0;

			a *= (m_p2[1] - m_p1[1]);

			if( a > 0 ) a += m_fs[1] / 2;

			if( a < 0 ) a -= m_fs[1] / 2;

			a /= m_fs[1];

			a += fDelta ? 0 : m_p1[1];

			return WORD(a);
			}
		}	

	return Data;
	}


DWORD CDLCModule::DispToLink(WORD PropID, DWORD Data)
{
	BOOL fDelta;

	if( IsScaled(PropID, fDelta) ) {

		if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {	
			
			LONG a = MakeDispLong(Data, fDelta, 0);

			a -= fDelta ? 0 : m_p1[0];

			a *= m_fs[0];

			if( a > 0 ) a += (m_p2[0] - m_p1[0]) / 2;

			if( a < 0 ) a -= (m_p2[0] - m_p1[0]) / 2;

			a /= (m_p2[0] - m_p1[0]);

			a += 0;

			return WORD(a);
			}
						

		if( ((PropID >> 11) & 0x07) == OBJ_LOOP_2 ) {
			
			LONG a = MakeDispLong(Data, fDelta, 1);

			a -= fDelta ? 0 : m_p1[1];

			a *= m_fs[1];

			if( a > 0 ) a += (m_p2[1] - m_p1[1]) / 2;

			if( a < 0 ) a -= (m_p2[1] - m_p1[1]) / 2;

			a /= (m_p2[1] - m_p1[1]);

			a += 0;

			return WORD(a);
			}
		}

	return Data;
	}

// Implementation

void CDLCModule::FindScaling(BOOL fLoop)
{
	switch( m_TempUnits[fLoop] ) {

		case 0:
			m_p1[fLoop] = 0;
			m_p2[fLoop] = 10000;
			m_fs[fLoop] = 20000;
			m_tc[fLoop] = TRUE;
			break;

		case 1:
			m_p1[fLoop] = -4597;
			m_p2[fLoop] = 13403;
			m_fs[fLoop] = 20000;
			m_tc[fLoop] = TRUE;
			break;

		case 2:
			m_p1[fLoop] = -2732;
			m_p2[fLoop] =  7268;
			m_fs[fLoop] = 20000;
			m_tc[fLoop] = TRUE;
			break;

		default:
			m_p1[fLoop] = m_ProcMin[fLoop];
			m_p2[fLoop] = m_ProcMax[fLoop];
			m_fs[fLoop] = 30000;
			m_tc[fLoop] = FALSE;
			break;
		}

	m_fDelta[fLoop][0] = FALSE;
	m_fDelta[fLoop][1] = FALSE;
	m_fDelta[fLoop][2] = FALSE;
	m_fDelta[fLoop][3] = FALSE;
	}

LONG CDLCModule::MakeLinkLong(WORD Data, BOOL fDelta)
{
	if( !fDelta )
		return WORD(Data);

	return SHORT(Data);
	}

LONG CDLCModule::MakeDispLong(WORD Data, BOOL fDelta, BOOL fLoop)
{
	if( m_tc[fLoop] ) {

		if( fDelta )
			return SHORT(Data);

		if( Data > 50000U )
			return SHORT(Data);

		return WORD(Data);
		}

	return SHORT(Data);
	}

BOOL CDLCModule::IsScaled(WORD PropID, BOOL &fDelta)
{
	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_1 ) {

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_DELTA_PV):
			case LOBYTE(PROPID_ERROR):
			case LOBYTE(PROPID_SET_HYST):
			case LOBYTE(PROPID_SET_DEAD):
			case LOBYTE(PROPID_INPUT_OFFSET):
			case LOBYTE(PROPID_SET_RAMP):
			case LOBYTE(PROPID_ALARM_HYST_1):
			case LOBYTE(PROPID_ALARM_HYST_2):
			case LOBYTE(PROPID_ALARM_HYST_3):
			case LOBYTE(PROPID_ALARM_HYST_4):
				
				fDelta = TRUE;
				
				return TRUE;

			case LOBYTE(PROPID_INPUT):
			case LOBYTE(PROPID_RANGE_LO):
			case LOBYTE(PROPID_RANGE_HI):
			case LOBYTE(PROPID_ACT_SP):
			case LOBYTE(PROPID_PV):
			case LOBYTE(PROPID_REQ_SP):
			case LOBYTE(PROPID_ALT_SP):
			case LOBYTE(PROPID_ALT_PV):
			case LOBYTE(PROPID_COLD_JUNC):
			case LOBYTE(PROPID_P00_SP):
			case LOBYTE(PROPID_P01_SP):
			case LOBYTE(PROPID_P02_SP):
			case LOBYTE(PROPID_P03_SP):
			case LOBYTE(PROPID_P04_SP):
			case LOBYTE(PROPID_P05_SP):
			case LOBYTE(PROPID_P06_SP):
			case LOBYTE(PROPID_P07_SP):
			case LOBYTE(PROPID_P08_SP):
			case LOBYTE(PROPID_P09_SP):
			case LOBYTE(PROPID_P10_SP):
			case LOBYTE(PROPID_P11_SP):
			case LOBYTE(PROPID_P12_SP):
			case LOBYTE(PROPID_P13_SP):
			case LOBYTE(PROPID_P14_SP):
			case LOBYTE(PROPID_P15_SP):
			case LOBYTE(PROPID_P16_SP):
			case LOBYTE(PROPID_P17_SP):
			case LOBYTE(PROPID_P18_SP):
			case LOBYTE(PROPID_P19_SP):
			case LOBYTE(PROPID_P20_SP):
			case LOBYTE(PROPID_P21_SP):
			case LOBYTE(PROPID_P22_SP):
			case LOBYTE(PROPID_P23_SP):
			case LOBYTE(PROPID_P24_SP):
			case LOBYTE(PROPID_P25_SP):
			case LOBYTE(PROPID_P26_SP):
			case LOBYTE(PROPID_P27_SP):
			case LOBYTE(PROPID_P28_SP):
			case LOBYTE(PROPID_P29_SP):
			case LOBYTE(PROPID_P30_SP):
			case LOBYTE(PROPID_P31_SP):

				fDelta = FALSE;
				
				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_1):
				
				fDelta = m_fDelta[0][0];

				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_2):
				
				fDelta = m_fDelta[0][1];

				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_3):
				
				fDelta = m_fDelta[0][2];

				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_4):
				
				fDelta = m_fDelta[0][3];

				return TRUE;
			}

		return FALSE;
		}

	if( ((PropID >> 11) & 0x07) == OBJ_LOOP_2 ) {

		switch( LOBYTE(PropID) ) {

			case LOBYTE(PROPID_DELTA_PV):
			case LOBYTE(PROPID_ERROR):
			case LOBYTE(PROPID_SET_HYST):
			case LOBYTE(PROPID_SET_DEAD):
			case LOBYTE(PROPID_INPUT_OFFSET):
			case LOBYTE(PROPID_SET_RAMP):
			case LOBYTE(PROPID_ALARM_HYST_1):
			case LOBYTE(PROPID_ALARM_HYST_2):
			case LOBYTE(PROPID_ALARM_HYST_3):
			case LOBYTE(PROPID_ALARM_HYST_4):
				
				fDelta = TRUE;
				
				return TRUE;

			case LOBYTE(PROPID_INPUT):
			case LOBYTE(PROPID_RANGE_LO):
			case LOBYTE(PROPID_RANGE_HI):
			case LOBYTE(PROPID_ACT_SP):
			case LOBYTE(PROPID_PV):
			case LOBYTE(PROPID_REQ_SP):
			case LOBYTE(PROPID_ALT_SP):
			case LOBYTE(PROPID_ALT_PV):
			case LOBYTE(PROPID_COLD_JUNC):
			case LOBYTE(PROPID_P00_SP):
			case LOBYTE(PROPID_P01_SP):
			case LOBYTE(PROPID_P02_SP):
			case LOBYTE(PROPID_P03_SP):
			case LOBYTE(PROPID_P04_SP):
			case LOBYTE(PROPID_P05_SP):
			case LOBYTE(PROPID_P06_SP):
			case LOBYTE(PROPID_P07_SP):
			case LOBYTE(PROPID_P08_SP):
			case LOBYTE(PROPID_P09_SP):
			case LOBYTE(PROPID_P10_SP):
			case LOBYTE(PROPID_P11_SP):
			case LOBYTE(PROPID_P12_SP):
			case LOBYTE(PROPID_P13_SP):
			case LOBYTE(PROPID_P14_SP):
			case LOBYTE(PROPID_P15_SP):
			case LOBYTE(PROPID_P16_SP):
			case LOBYTE(PROPID_P17_SP):
			case LOBYTE(PROPID_P18_SP):
			case LOBYTE(PROPID_P19_SP):
			case LOBYTE(PROPID_P20_SP):
			case LOBYTE(PROPID_P21_SP):
			case LOBYTE(PROPID_P22_SP):
			case LOBYTE(PROPID_P23_SP):
			case LOBYTE(PROPID_P24_SP):
			case LOBYTE(PROPID_P25_SP):
			case LOBYTE(PROPID_P26_SP):
			case LOBYTE(PROPID_P27_SP):
			case LOBYTE(PROPID_P28_SP):
			case LOBYTE(PROPID_P29_SP):
			case LOBYTE(PROPID_P30_SP):
			case LOBYTE(PROPID_P31_SP):

				fDelta = FALSE;
				
				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_1):
				
				fDelta = m_fDelta[1][0];

				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_2):
				
				fDelta = m_fDelta[1][1];

				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_3):
				
				fDelta = m_fDelta[1][2];

				return TRUE;

			case LOBYTE(PROPID_ALARM_DATA_4):
				
				fDelta = m_fDelta[1][3];

				return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

// End of File
