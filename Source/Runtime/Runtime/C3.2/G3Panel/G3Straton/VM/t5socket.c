/*****************************************************************************
T5Socket.c : socket interface - CAN BE CHANGED WHEN PORTING
(c) COPALP 2002
*****************************************************************************/

#include "t5vm.h"

#include "..\platform.h"

#ifdef T5DEF_ETHERNET

/*****************************************************************************
T5Socket_Initialize
Initialize the Socket interface
*****************************************************************************/

T5_RET T5Socket_Initialize (T5HND_CS pfCallback)
{
    return socketInitialize(pfCallback);
}

/*****************************************************************************
T5Socket_Terminate
Release the socket interface
*****************************************************************************/

void T5Socket_Terminate (void)
{
	socketTerminate();
}

/*****************************************************************************
T5Socket_CreateListeningSocket
Create a non blocking listening socket
Parameters:
    wPort (IN) ethernet port number
    wMaxCnx (IN) max number of allowed connections
    pSocket (OUT) created socket if OK
return: OK or ERROR
*****************************************************************************/

T5_RET T5Socket_CreateListeningSocket (T5_WORD wPort, T5_WORD wMaxCnx,
                                       T5_PTSOCKET pSocket,
                                       T5HND_CS pfCallback)
{
    return socketCreateListeningSocket (wPort, wMaxCnx, pSocket, pfCallback);
}

#ifdef T5DEF_SOCKETV2

T5_RET T5Socket_CreateListeningSocketOn (T5_WORD wPort, T5_WORD wMaxCnx,
                                         T5_PTCHAR szAddr,
                                         T5_PTSOCKET pSocket,
                                         T5HND_CS pfCallback)
{
    return socketCreateListeningSocketOn(wPort, wMaxCnx, szAddr, pSocket, pfCallback);
}

#endif /*T5DEF_SOCKETV2*/

/*****************************************************************************
T5Socket_CloseSocket
Close a socket
Parameters:
    sock (IN) socket to be closed
*****************************************************************************/

void T5Socket_CloseSocket (T5_SOCKET sock)
{
	socketCloseSocket(sock);
}

/*****************************************************************************
T5Socket_Accept
Accept non blokcing sockets
Parameters:
    sockListen (IN) handle of the listening socket
return: accepted socket or T5_INVSOCKET if nothing
*****************************************************************************/

T5_SOCKET T5Socket_Accept (T5_SOCKET sockListen, T5HND_CS pfCallback)
{
    return socketAccept(sockListen, pfCallback);
}

/*****************************************************************************
T5Socket_Send
Send data on a non blocking client socket
Parameters:
    sock (IN) client socket
    wSize (IN) number of bytes to send
    pData (IN) pointer to data to send
    pbFail (OUT) set to TRUE on error: socket must be closed
return: number of bytes actually sent
*****************************************************************************/

T5_WORD T5Socket_Send (T5_SOCKET sock, T5_WORD wSize, T5_PTR pData,
                       T5_PTBOOL pbFail)
{
    return socketSend(sock, wSize, pData, pbFail);
}

/*****************************************************************************
T5Socket_Receive
Receive data on a non blocking client socket
Parameters:
    sock (IN) client socket
    wSize (IN) number of bytes to read
    pData (OUT) buffer where to store received data
    pbFail (OUT) set to TRUE on error: socket must be closed
return: number of bytes actually received
*****************************************************************************/

T5_WORD T5Socket_Receive (T5_SOCKET sock, T5_WORD wSize, T5_PTR pData,
                          T5_PTBOOL pbFail)
{
    return socketReceive(sock, wSize, pData, pbFail);
}

/*****************************************************************************
T5Socket_CreateConnectedSocket
Create a non blocking socket connected to a remote server
- USED FOR MODBUS I/Os - 
Parameters:
    szAddr (IN) address of the server
    wPort (IN) ethernet port number of the server
    pSocket (OUT) created socket if OK
    pbWait (OUT) set to TRUE if connect is not fully performed
return: OK or ERROR
*****************************************************************************/

#ifdef T5DEF_TCPCLIENT

T5_RET T5Socket_CreateConnectedSocket (T5_PTCHAR szAddr, T5_WORD wPort,
                                       T5_PTSOCKET pSocket,
                                       T5_PTBOOL pbWait,
                                       T5HND_CS pfCallback)
{
    return socketCreateConnectedSocket(szAddr, wPort, pSocket, pbWait, pfCallback);;
}

#endif /*T5DEF_TCPCLIENT*/

/*****************************************************************************
T5Socket_CheckPendingConnect
Check pending connection a non blocking socket
The socket was created by T5Socket_CreateConnectedSocket()
- USED FOR MODBUS I/Os - 
Parameters:
    sock (IN) client socket
    pbFail (OUT) set to TRUE on error: socket must be closed
return: OK if connection complete / ERROR if fail or still pending
*****************************************************************************/

#ifdef T5DEF_TCPCLIENT

T5_RET T5Socket_CheckPendingConnect (T5_SOCKET sock, T5_PTBOOL pbFail)
{
    return socketCheckPendingConnect ( sock,  pbFail);
}

#endif /*T5DEF_TCPCLIENT*/

/****************************************************************************/
/* UDP management functions */

#ifdef T5DEF_UDP

/*****************************************************************************
T5Socket_UdpCreate
Create a UDP socket - must be non blocking socket
Parameters:
    wPort (IN) ethernet port number - bind required if not 0
    pSocket (OUT) created socket if OK
return: OK or ERROR
*****************************************************************************/

T5_RET T5Socket_UdpCreate (T5_WORD wPort, T5_PTSOCKET pSocket)
{
    *pSocket = T5_INVSOCKET;
    return socketUpdCreate(wPort, pSocket);
}

#ifdef T5DEF_SOCKETV2

T5_RET T5Socket_AcceptBroadcast (T5_SOCKET sock)
{
    return socketAcceptBroadcast(sock);
}

#endif /*T5DEF_SOCKETV2*/

/*****************************************************************************
T5Socket_UdpSendTo
Send data on a UDP socket
Parameters:
    sock (IN) UDP socket
    pAddr (IN) pointer to the socket address
    pData (IN) pointer to data to send
    wSize (IN) number of bytes to send
return: OK or ERROR
*****************************************************************************/

T5_RET T5Socket_UdpSendTo (T5_SOCKET sock, T5_PTR pAddr,
                           T5_PTR pData, T5_WORD wSize)
{
    return socketUdpSendTo(sock, pAddr, pData, wSize);
}

/*****************************************************************************
T5Socket_UdpRecvFrom
Receive data on a UDP socket
Parameters:
    sock (IN) UDP socket
    pData (OUT) pointer to buffer where to copy received data
    wMax (IN) size of the buffer where to copy received data
    pAddr (OUT) pointer to the socket address
return: number or received bytes (0 if nothing or error)
*****************************************************************************/

T5_WORD T5Socket_UdpRecvFrom (T5_SOCKET sock, T5_PTR pData, T5_WORD wMax,
                              T5_PTR pAddr)
{
    return socketUpdRecvFrom(sock, pData, wMax, pAddr);
}

/*****************************************************************************
T5Socket_UdpAddrMake
Fill a socket address structure
Parameters:
    pAddr (OUT) pointer to the socket address to construct
    szAddr (IN) IP address
    wPort (IN) ETHERNET port number
return: OK or ERROR
*****************************************************************************/

void T5Socket_UdpAddrMake (T5_PTR pAddr, T5_PTCHAR szAddr, T5_WORD wPort)
{
	sockUpdAddrMake(pAddr, szAddr, wPort);
}

/****************************************************************************/

#endif /*T5DEF_UDP*/

/****************************************************************************/

#endif /*T5DEF_ETHERNET*/

/* eof **********************************************************************/
