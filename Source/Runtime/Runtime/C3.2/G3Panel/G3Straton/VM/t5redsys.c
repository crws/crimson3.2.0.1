/*****************************************************************************
T5RedSys.c : Redundancy - system calls
(c) COPALP 2006
*****************************************************************************/

#include "t5vm.h"
#include "t5redapi.h"

/****************************************************************************/

void T5RedSys_StartThread (PFT5REDTH pfThread, T5_BOOL bHighPrio,
                                  T5_PTR pArgs)
{
}

void T5RedSys_SetCurThreadPriority (T5_BOOL bHighPrio)
{
}

void T5RedSys_Sleep (T5_DWORD dwMilliseconds)
{
}

T5_DWORD T5RedSys_GetTickCount (void)
{
    return 0;
}

T5_PTR T5RedSys_Malloc (T5_DWORD dwSize)
{
    return NULL;
}

void T5RedSys_Free (T5_PTR ptr)
{
}

/* eof **********************************************************************/
