/*****************************************************************************
T5Serial.c : serial interface - TO BE FILLED WHEN PORTING
(c) COPALP 2002
*****************************************************************************/

#include "t5vm.h"

#include "..\platform.h"

#ifdef T5DEF_SERIAL

/*****************************************************************************
T5Serial_Initialize
Initialize a comm port descriptor (reset to invalid state)
Parameters:
    pSerial (OUT) pointer to the port descriptor
*****************************************************************************/

void T5Serial_Initialize (T5_PTSERIAL pSerial)
{
serialInitialize(pSerial);
}

/*****************************************************************************
T5Serial_IsValid
Test if a comm port descriptor is valid (port is open)
Warning: data may be loaded on Hot Restart. Check if not null is not enough!
Parameters:
    pSerial (IN) pointer to the port descriptor
Return: TRUE if the port is open
*****************************************************************************/

T5_BOOL T5Serial_IsValid (T5_PTSERIAL pSerial)
{
    return serialIsValid(pSerial);
}

/*****************************************************************************
T5Serial_Open
Open a serial port
Parameters:
    pSerial (IN/OUT) pointer to the port descriptor
    szConfig (IN) configuration strings (comm settings)
Return: TRUE if the port is open successfully
*****************************************************************************/

T5_BOOL T5Serial_Open (T5_PTSERIAL pSerial, T5_PTCHAR szConfig)
{
    return serialOpen(pSerial, szConfig);
}

/*****************************************************************************
T5Serial_Close
Close a serial port
Parameters:
    pSerial (IN/OUT) pointer to the port descriptor
*****************************************************************************/

void T5Serial_Close (T5_PTSERIAL pSerial)
{
serialClose(pSerial);
}

/*****************************************************************************
T5Serial_Send
Send data to a serial port
Parameters:
    pSerial (IN/OUT) pointer to the port descriptor
    wSize (IN) number of bytes to send
    pData (IN) pointer to data to send
Return: number of bytes sent
*****************************************************************************/

T5_WORD T5Serial_Send (T5_PTSERIAL pSerial, T5_WORD wSize, T5_PTR pData)
{
    return serialSend(pSerial, wSize, pData);
}

/*****************************************************************************
T5Serial_Receive
Receive data on a serial port
Parameters:
    pSerial (IN/OUT) pointer to the port descriptor
    wSize (IN) wished number of bytes (maximum read)
    pData (IN) pointer to buffer where to store received bytes
Return: number of bytes received
*****************************************************************************/

T5_WORD T5Serial_Receive (T5_PTSERIAL pSerial, T5_WORD wSize, T5_PTR pData)
{
    return serialReceive(pSerial, wSize, pData);
}

/****************************************************************************/

#endif /*T5DEF_SERIAL*/

/* eof **********************************************************************/
