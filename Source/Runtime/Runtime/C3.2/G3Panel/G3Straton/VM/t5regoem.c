/*****************************************************************************
T5RegOem.c : T5 Registry - OEM interface to system

TO BE FILLED / ADJUSTED WHILE PORTING

(c) COPALP 2008
*****************************************************************************/

#include "t5vm.h"

/****************************************************************************/

#ifdef T5DEF_REG

/*****************************************************************************
T5RegOem_GetFileSize
Get the size of a backup block/file
Parameters:
    wID (IN) block type - T5REGOEM_XML=xml file / T5REGOEM_REG=binary registry
    szName (IN) block/file name
Return: length in bytes or 0 if fail
*****************************************************************************/

T5_DWORD T5RegOem_GetFileSize (T5_WORD wID, T5_PTCHAR szName)
{
    return 0L;
}

/*****************************************************************************
T5RegOem_OpenWrite
Open a backup block/file for writing
Parameters:
    wID (IN) block type - T5REGOEM_XML=xml file / T5REGOEM_REG=binary registry
    szName (IN) block/file name
    dwSize (IN) wished free space for writing
Return: OEM file ID or NULL if fail
*****************************************************************************/

T5_DWORD T5RegOem_OpenWrite (T5_WORD wID, T5_PTCHAR szName, T5_DWORD dwSize)
{
    return 0L;
}

/*****************************************************************************
T5RegOem_OpenRead
Open a backup block/file for reading
Parameters:
    wID (IN) block type - T5REGOEM_XML=xml file / T5REGOEM_REG=binary registry
    szName (IN) block/file name
Return: OEM file ID or NULL if fail
*****************************************************************************/

T5_DWORD T5RegOem_OpenRead (T5_WORD wID, T5_PTCHAR szName)
{
    return 0L;
}

/*****************************************************************************
T5RegOem_Close
Close an open backup block/file
Parameters:
    dwf (IN) OEM file ID or NULL if fail
*****************************************************************************/

void T5RegOem_Close (T5_DWORD dwf)
{
}

/*****************************************************************************
T5RegOem_Write
Write data to backup block/file
Parameters:
    dwf (IN) OEM file ID or NULL if fail
    dwSize (IN) number of bytes to write
    pData (IN) pointer to application buffer
Return: TRUE if OK
*****************************************************************************/

T5_BOOL T5RegOem_Write (T5_DWORD dwf, T5_DWORD dwSize, T5_PTBYTE pData)
{
    return FALSE;
}

/*****************************************************************************
T5RegOem_Read
Read data from backup block/file
Parameters:
    dwf (IN) OEM file ID or NULL if fail
    dwSize (IN) number of bytes to write
    pData (OUT) pointer to application buffer
Return: TRUE if OK
*****************************************************************************/

T5_BOOL T5RegOem_Read (T5_DWORD dwf, T5_DWORD dwSize, T5_PTBYTE pData)
{
    return FALSE;
}

/*****************************************************************************
T5RegOem_AllocRAM
Allocate block in RAM memory
Parameters:
    dwSize (IN) number of bytes to allocate
Return: OEM memory ID or NULL if fail
*****************************************************************************/

T5_DWORD T5RegOem_AllocRAM (T5_DWORD dwSize)
{
    return 0L;
}

/*****************************************************************************
T5RegOem_LinkRAM
Get pointer to allocated block in RAM memory
Parameters:
    dwRam (IN) OEM memory ID or NULL if fail
Return: pointer to memory or NULL if fail
*****************************************************************************/

T5_PTR T5RegOem_LinkRAM (T5_DWORD dwram)
{
    return NULL;
}

/*****************************************************************************
T5RegOem_UnlinkRAM
Release pointer to allocated block in RAM memory
Parameters:
    dwRam (IN) OEM memory ID or NULL if fail
    pRam (IN) pointer to memory
*****************************************************************************/

void T5RegOem_UnlinkRAM (T5_DWORD dwram, T5_PTR pRam)
{
}

/*****************************************************************************
T5RegOem_FreeRAM
Release allocated block in RAM memory
Parameters:
    dwRam (IN) OEM memory ID or NULL if fail
    pRam (IN) pointer to memory
*****************************************************************************/

void T5RegOem_FreeRAM (T5_DWORD dwram, T5_PTR pRam)
{
}

/****************************************************************************/

#endif /*T5DEF_REG*/

/* eof **********************************************************************/
