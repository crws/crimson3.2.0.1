/*****************************************************************************
T5unicod.c : UNICODE conversions - TO BE FILLED FOR PORT
(c) COPALP 2002
*****************************************************************************/

#include "t5vm.h"

/****************************************************************************/

#ifdef T5DEF_UNICODE

/****************************************************************************/

T5_PTCHAR T5Unicod_AllocUtf8Conversion (T5_PTCHAR szAnsi)
{
    return NULL;
}

T5_PTCHAR T5Unicod_AllocAnsiConversion (T5_PTCHAR szUtf8)
{
    return NULL;
}

void T5Unicod_FreeConversion (T5_PTCHAR szBuffer)
{
}

/****************************************************************************/

#endif /*T5DEF_UNICODE*/

/* eof **********************************************************************/
