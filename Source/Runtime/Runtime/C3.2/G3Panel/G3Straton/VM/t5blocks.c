/*****************************************************************************
T5Blocks.c :custom functions and FBs - TO BE COMPLETED WHEN PORTING
(c) COPA-DATA France 2002

Implementation of blocks and functions of the "Plus!" library
Functions and FBs of the Plus! library are pre-declared in the Workbench
and supported by the STRATON simulator:

Counters with edge detection: CTUr, CTDr, CTUDr

*****************************************************************************/

#include "t5vm.h"

/****************************************************************************/
/* PID block */

/* Input arguments:
_P_AUTO             TRUE = auto / FALSE = manual
_P_PV               input
_P_SP               set point
_P_XOUTMANU         output value for manual mode
_P_KP               gain
_P_TI               integral time
_P_TD               derivate time
_P_TS               scan time
_P_XMIN             minimum output value
_P_XMAX             maximum output value
_P_I_SEL            TRUE = validate intergral term
_P_INT_HOLD         TRUE = hold integral sum
_P_I_ITL_ON         TRUE = reset integral sum to ITLVAL
_P_I_ITLVAL         integral sum reset value
_P_DEADB_ERR        deadband half width
_P_FFD              disturbation on output
*/

#define _P_AUTO         (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[0]))
#define _P_PV           (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[1]))
#define _P_SP           (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[2]))
#define _P_XOUTMANU     (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[3]))
#define _P_KP           (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[4]))
#define _P_TI           (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[5]))
#define _P_TD           (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[6]))
#define _P_TS           (*((T5_PTDWORD)(T5GET_DBTIME(pBase))+pArgs[7]))
#define _P_XMIN         (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[8]))
#define _P_XMAX         (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[9]))
#define _P_I_SEL        (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[10]))
#define _P_INT_HOLD     (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[11]))
#define _P_I_ITL_ON     (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[12]))
#define _P_I_ITLVAL     (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[13]))
#define _P_DEADB_ERR    (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[14]))
#define _P_FFD          (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[15]))

/* Output arguments:
_P_XOUT             output
_P_ER               error value
_P_XOUT_P           P value
_P_XOUT_I           I value
_P_XOUT_D           D value
_P_XOUT_HLM         TRUE if high limit reached
_P_XOUT_LLM         TRUE if low limiit reached
*/

#define _P_XOUT         (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[16]))
#define _P_ER           (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[17]))
#define _P_XOUT_P       (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[18]))
#define _P_XOUT_I       (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[19]))
#define _P_XOUT_D       (*((T5_PTREAL)(T5GET_DBDATA32(pBase))+pArgs[20]))
#define _P_XOUT_HLM     (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[21]))
#define _P_XOUT_LLM     (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[22]))

/* private block data */

typedef struct
{
    T5_DWORD dwLastCall;    /* time stamp of last active call */
    T5_REAL  fErr;          /* last calculated error (for deadband) */
    T5_REAL  fInt;          /* integral sum */
    T5_BOOL  bOver;         /* out of limit during last call */
    T5_BOOL  bInit;         /* init done */
    T5_BOOL  bRes1;
    T5_BOOL  bRes2;
} _str_FB_PID_CONT;

/* easy access to private data items */

#define _D_LASTCALL (pData->dwLastCall)
#define _D_ERR      (pData->fErr)
#define _D_INT      (pData->fInt)
#define _D_OVER     (pData->bOver)

/* handler */

T5_DWORD PID (
    T5_WORD wCommand,
    T5PTR_DB pBase,
    T5_PTR pClass,
    T5_PTR pInst,
    T5_PTWORD pArgs)
{
    _str_FB_PID_CONT *pData;    /* pointer to private instance data */
    T5PTR_DBSTATUS pStatus;     /* STRAON VM private data - for time stamp */
    T5_DWORD dwElapsed;         /* elapsed time since last sample - msec */
    T5_REAL fElapsed;           /* elapsed time since last sample - sec */
    T5_REAL fErr;               /* for error calculation and deadband */
    T5_REAL fErrPrev;           /* error at previous sample */
    T5_REAL fInt;               /* integral term after gates */
    T5_REAL fIntAdd;            /* integral increase value */
    T5_REAL fDer;               /* Derivate term after gates */
    T5_REAL fOut;               /* Output value */

    pData = (_str_FB_PID_CONT *)pInst;
    switch (wCommand)
    {
    /* activation of the block - main algorithm *****************************/
    case T5FBCMD_ACTIVATE :

        /* get and check time elapsed since last sample */
        pStatus = T5GET_DBSTATUS(pBase);
        if (!pData->bInit)
        {
            _D_LASTCALL = pStatus->dwTimStamp;
            pData->bInit = TRUE;
        }
        dwElapsed = T5_BOUNDTIME(pStatus->dwTimStamp - _D_LASTCALL);
        fElapsed  = ((T5_REAL)dwElapsed) / 1000.0f;
        if (dwElapsed == 0L || dwElapsed < _P_TS)
            return 0L;
        _D_LASTCALL = pStatus->dwTimStamp;

        /* remember previous error for derivate */
        fErrPrev = _D_ERR;

        /* calculate New error */
        fErr = _P_SP - _P_PV;

        /* deadband */
        if (_P_DEADB_ERR < 0.0f)
        {
            _D_ERR = fErr;
        }
        else if (fErr <= (_D_ERR - _P_DEADB_ERR) || fErr >= (_D_ERR + _P_DEADB_ERR))
        {
            _D_ERR = fErr;
        }
        _P_ER = _D_ERR;

        /* derivate (fErrPrev has no gain) - apply gain to full derivate */
        fDer = (fErr - fErrPrev) * _P_TD * _P_KP;
        _P_XOUT_D = fDer;

        /* gain */
        fErr = _D_ERR * _P_KP;
        _P_XOUT_P = fErr;

        /* integral - now fErr has the gain applied */
        if (_P_I_ITL_ON)
        {
            _D_INT = _P_I_ITLVAL;
        }
        /* hold integral if INT_HOLD or XOUT out of bounds or manual mode */
#ifndef T5DEF_PID_INTDESAT
        else if (!_P_INT_HOLD && !_D_OVER && _P_AUTO && _P_TI != 0.0f)
        {
            fIntAdd = ((fElapsed * fErr) / _P_TI);
            _D_INT += fIntAdd;
        }
#else /*T5DEF_PID_INTDESAT*/
        else if (!_P_INT_HOLD && _P_AUTO && _P_TI != 0.0f)
        {
            fIntAdd = ((fElapsed * fErr) / _P_TI);
            if (_D_OVER)
            {
                if ((fIntAdd * _D_INT) >= 0)
                    fIntAdd = 0;
            }
            _D_INT += fIntAdd;
        }
#endif /*T5DEF_PID_INTDESAT*/
        if (_P_I_SEL)
        {
            fInt = _D_INT;
        }
        else
        {
            fInt = 0.0f;
        }
        _P_XOUT_I = fInt;

        /* auto/manual */
        if (_P_AUTO)
        {
            fOut = fErr + fInt + fDer;
        }
        else
        {
            fOut = _P_XOUTMANU;
        }

        /* disturbation on output */
        fOut += _P_FFD;

        /* check limits on output */
        if (fOut < _P_XMIN)
        {
            _P_XOUT = _P_XMIN;
            _P_XOUT_HLM = FALSE;
            _P_XOUT_LLM = TRUE;
            _D_OVER = TRUE;

        }
        else if (fOut > _P_XMAX)
        {
            _P_XOUT = _P_XMAX;
            _P_XOUT_HLM = TRUE;
            _P_XOUT_LLM = FALSE;
            _D_OVER = TRUE;
        }
        else
        {
            _P_XOUT = fOut;
            _P_XOUT_HLM = FALSE;
            _P_XOUT_LLM = FALSE;
            _D_OVER = FALSE;
        }

        /* done */
        return 0L;

    /* private data initialization ******************************************/
    case T5FBCMD_INITINSTANCE :
    case T5FBCMD_HOTRESTART :
        pStatus = T5GET_DBSTATUS(pBase);
        _D_LASTCALL = pStatus->dwTimStamp;
        pData->bInit = FALSE;
        return 0L;

    /* other calls for STRATON **********************************************/
    case T5FBCMD_SIZEOFINSTANCE :
        return (T5_DWORD)sizeof(_str_FB_PID_CONT);
    case T5FBCMD_ACCEPTCT :
        return 1L;
    default :
        return 0L;
    }
}

/* Undefine argument list and private data items */

#undef _D_LASTCALL
#undef _D_ERR
#undef _D_INT

#undef _P_AUTO
#undef _P_PV
#undef _P_SP
#undef _P_XOUTMANU
#undef _P_KP
#undef _P_TI
#undef _P_TD
#undef _P_TS
#undef _P_XMIN
#undef _P_XMAX
#undef _P_I_SEL
#undef _P_INT_HOLD
#undef _P_I_ITL_ON
#undef _P_I_ITLVAL
#undef _P_DEADB_ERR
#undef _P_FFD
#undef _P_XOUT
#undef _P_ER
#undef _P_XOUT_P
#undef _P_XOUT_I
#undef _P_XOUT_D
#undef _P_XOUT_HLM
#undef _P_XOUT_LLM

/****************************************************************************/
/* CTUr - Up Counter with Rising edge detection */
/* Function block */

#define _P_CU   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[0]))
#define _P_RESET   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[1]))
#define _P_PV   (*((T5_PTLONG)(T5GET_DBDATA32(pBase))+pArgs[2]))
#define _P_Q   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[3]))
#define _P_CV   (*((T5_PTLONG)(T5GET_DBDATA32(pBase))+pArgs[4]))

typedef struct
{
    T5_LONG  dwCount;
    T5_BOOL  bCuPrev;
    T5_BOOL  bResetPrev;
} _str_FB_CTUR;

T5_DWORD CTUR (
    T5_WORD wCommand,
    T5PTR_DB pBase,
    T5_PTR pClass,
    T5_PTR pInst,
    T5_PTWORD pArgs)
{
    _str_FB_CTUR *pData;
    T5_BOOL bCu, bReset;
    
    pData = (_str_FB_CTUR *)pInst;
    switch (wCommand)
    {
    case T5FBCMD_ACTIVATE :
        bCu = _P_CU && !(pData->bCuPrev);
        bReset = _P_RESET && !(pData->bResetPrev);
        pData->bCuPrev = _P_CU;
        pData->bResetPrev = _P_RESET;
        if (bReset)
            pData->dwCount = 0L;
        else if (bCu && pData->dwCount < _P_PV)
            pData->dwCount += 1L;
        _P_Q = (pData->dwCount >= _P_PV);
        _P_CV = pData->dwCount;
        return 0L;
    case T5FBCMD_SIZEOFINSTANCE :
        return (T5_DWORD)sizeof(_str_FB_CTUR);
    default :
        return 0L;
    }
}

#undef _P_CU
#undef _P_RESET
#undef _P_PV
#undef _P_Q
#undef _P_CV

/****************************************************************************/
/* CTUd - Down Counter with Rising edge detection */

#define _P_CD   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[0]))
#define _P_LOAD   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[1]))
#define _P_PV   (*((T5_PTLONG)(T5GET_DBDATA32(pBase))+pArgs[2]))
#define _P_Q   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[3]))
#define _P_CV   (*((T5_PTLONG)(T5GET_DBDATA32(pBase))+pArgs[4]))

typedef struct
{
    T5_LONG  dwCount;
    T5_BOOL  bCdPrev;
    T5_BOOL  bLoadPrev;
} _str_FB_CTDR;

T5_DWORD CTDR (
    T5_WORD wCommand,
    T5PTR_DB pBase,
    T5_PTR pClass,
    T5_PTR pInst,
    T5_PTWORD pArgs)
{
    _str_FB_CTDR *pData;
    T5_BOOL bCd, bLoad;
    
    pData = (_str_FB_CTDR *)pInst;
    switch (wCommand)
    {
    case T5FBCMD_ACTIVATE :
        bCd = _P_CD && !(pData->bCdPrev);
        bLoad = _P_LOAD && !(pData->bLoadPrev);
        pData->bCdPrev = _P_CD;
        pData->bLoadPrev = _P_LOAD;
        
        if (bLoad)
            pData->dwCount = _P_PV;
        else if (bCd && pData->dwCount > 0L) 
            pData->dwCount -= 1L;
        _P_Q = (pData->dwCount <= 0L);
        _P_CV = pData->dwCount;
        return 0L;
    case T5FBCMD_SIZEOFINSTANCE :
        return (T5_DWORD)sizeof(_str_FB_CTDR);
    default :
        return 0L;
    }
}

#undef _P_CD
#undef _P_LOAD
#undef _P_PV
#undef _P_Q
#undef _P_CV

/****************************************************************************/
/* CTUDr - Up/Down Counter with Rising edge detection */

#define _P_CU   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[0]))
#define _P_CD   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[1]))
#define _P_RESET   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[2]))
#define _P_LOAD   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[3]))
#define _P_PV   (*((T5_PTLONG)(T5GET_DBDATA32(pBase))+pArgs[4]))
#define _P_QU   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[5]))
#define _P_QD   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[6]))
#define _P_CV   (*((T5_PTLONG)(T5GET_DBDATA32(pBase))+pArgs[7]))

typedef struct
{
    T5_LONG  dwCount;
    T5_BOOL  bCuPrev;
    T5_BOOL  bResetPrev;
    T5_BOOL  bCdPrev;
    T5_BOOL  bLoadPrev;
} _str_FB_CTUDR;

T5_DWORD CTUDR (
    T5_WORD wCommand,
    T5PTR_DB pBase,
    T5_PTR pClass,
    T5_PTR pInst,
    T5_PTWORD pArgs)
{
    _str_FB_CTUDR *pData;
    T5_BOOL bCu, bReset;
    T5_BOOL bCd, bLoad;
    
    pData = (_str_FB_CTUDR *)pInst;
    switch (wCommand)
    {
    case T5FBCMD_ACTIVATE :
        bCu = _P_CU && !(pData->bCuPrev);
        bReset = _P_RESET && !(pData->bResetPrev);
        bCd = _P_CD && !(pData->bCdPrev);
        bLoad = _P_LOAD && !(pData->bLoadPrev);
        pData->bCuPrev = _P_CU;
        pData->bResetPrev = _P_RESET;
        pData->bCdPrev = _P_CD;
        pData->bLoadPrev = _P_LOAD;
        if (bReset)
            pData->dwCount = 0L;
        else if (bLoad)
            pData->dwCount = _P_PV;
        else if (bCu && pData->dwCount < _P_PV)
            pData->dwCount += 1L;
        else if (bCd && !bCu && pData->dwCount > 0L)
            pData->dwCount -= 1L;
        _P_QU = (pData->dwCount >= _P_PV);
        _P_QD = (pData->dwCount <= 0L);
        _P_CV = pData->dwCount;
        return 0L;
    case T5FBCMD_SIZEOFINSTANCE :
        return (T5_DWORD)sizeof(_str_FB_CTUDR);
    default :
        return 0L;
    }
}

#undef _P_CU
#undef _P_CD
#undef _P_RESET
#undef _P_LOAD
#undef _P_PV
#undef _P_QU
#undef _P_QD
#undef _P_CV

/****************************************************************************/
/* list of registered functions and blocks */

typedef struct _s_pfb
{
    T5_PTCHAR szName;
    T5HND_FB pFb;
}
str_pfb;

static str_pfb PFB[] = {
#ifdef T5DEF_TXB
    T5TXB_FBLIST_STATIC
#endif /*T5DEF_TXB*/
#ifdef T5DEF_XML
    T5XML_FBLIST_STATIC
#endif /*T5DEF_XML*/
#ifdef T5DEF_BARRAY
    T5BARRAY_FBLIST_STATIC
#endif /*T5DEF_BARRAY*/

    { "PID", PID },
    { "CTUR", CTUR },
    { "CTDR", CTDR },
    { "CTUDR", CTUDR },

    /* continue with your custom blocks and functions... */

{ T5_PTNULL, T5_PTNULL } };


/*****************************************************************************
T5Blocks_GetFirstID
start enumerating custom blocks
Parameters:
    pdwID (OUT) ID of the first block
return: TRUE if ok
*****************************************************************************/

T5_BOOL T5Blocks_GetFirstID (T5_PTDWORD pdwID)
{
    *pdwID = 0L;

    return (PFB[*pdwID].pFb != T5_PTNULL);
}

/*****************************************************************************
T5Blocks_GetNextID
enumerate custom blocks
Parameters:
    pdwID (IN/OUT) ID of the current block - set to next ID on output
return: TRUE if ok
*****************************************************************************/

T5_BOOL T5Blocks_GetNextID (T5_PTDWORD pdwID)
{
    *pdwID += 1L;

    if (*pdwID >= (sizeof (PFB) / sizeof (str_pfb)))
        return FALSE;

    return (PFB[*pdwID].pFb != T5_PTNULL);
}

/*****************************************************************************
T5Blocks_GetName
return the name of a custom block
Parameters:
    dwID (IN) ID of the block
return: pointer to block name (UPPERCASE) or NULL if bad ID
*****************************************************************************/

T5_PTCHAR T5Blocks_GetName (T5_DWORD dwID)
{
    if (dwID >= (sizeof (PFB) / sizeof (str_pfb)))
        return T5_PTNULL;

    return PFB[dwID].szName;
}

/*****************************************************************************
T5Blocks_GetProc
return the handler procedure of a custom block
Parameters:
    dwID (IN) ID of the block
return: pointer to block handler or NULL if bad ID
*****************************************************************************/

T5HND_FB T5Blocks_GetProc (T5_DWORD dwID)
{
    if (dwID >= (sizeof (PFB) / sizeof (str_pfb)))
        return T5_PTNULL;

    return PFB[dwID].pFb;
}

/* eof **********************************************************************/
