/*****************************************************************************
T5Bdll.c :custom FBs in DLLs - TO BE CUSTOMIZED WHEN PORTING
(c) COPALP 2002
*****************************************************************************/

#include "t5vm.h"

/****************************************************************************/

#ifdef T5DEF_DLLBLOCKS

/****************************************************************************/

T5_DWORD T5Bdll_OpenDynaLinks (void)
{
    return 0L;
}

void T5Bdll_CloseDynaLinks (void)
{
}

T5_BOOL T5Bdll_StartEnumerate (void)
{
    return FALSE;
}

void T5Bdll_StopEnumerate (void)
{
}

T5_BOOL T5Bdll_GetFirstID (T5_PTDWORD pdwID)
{
    return FALSE;
}

T5_BOOL T5Bdll_GetNextID (T5_PTDWORD pdwID)
{
    return FALSE;
}

T5_PTCHAR T5Bdll_GetName (T5_DWORD dwID)
{
    return NULL;
}

T5HND_FB T5Bdll_GetProc (T5_DWORD dwID)
{
    return NULL;
}

/****************************************************************************/

#endif /*T5DEF_DLLBLOCKS*/

/* eof **********************************************************************/
