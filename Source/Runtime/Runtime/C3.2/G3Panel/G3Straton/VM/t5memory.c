/*****************************************************************************
T5Memory.c : system memory management - TO BE COMPLETED WHEN PORTING
(c) COPALP 2002
*****************************************************************************/

#include "t5vm.h"

#include "..\platform.h"

/*****************************************************************************
T5Memory_Alloc
allocates a block of memory
Parameters:
    mmb (IN/OUT) memory block descriptor
    dwSize (IN) wished block size
    pConf (IN) pointer to OEM data
return: OK or error
*****************************************************************************/

T5_RET T5Memory_Alloc (T5PTR_MMB mmb, T5_DWORD dwSize, T5_PTR pConf)
{
    return memoryAlloc(mmb, dwSize, pConf);
}

/*****************************************************************************
T5Memory_Load
allocates a block of memory and load contents from backup support
Parameters:
    mmb (IN/OUT) memory block descriptor
    szAppName (IN) application name
    pConf (IN) pointer to OEM data
return: OK or error
*****************************************************************************/

T5_RET T5Memory_Load (T5PTR_MMB mmb, T5_PTCHAR szAppName, T5_PTR pConf)
{
    return memoryLoad(mmb, szAppName, pConf);
}

/*****************************************************************************
T5Memory_Free
release a block of memory
Parameters:
    mmb (IN/OUT) memory block descriptor
    pConf (IN) pointer to OEM data
return: OK or error
*****************************************************************************/

T5_RET T5Memory_Free (T5PTR_MMB mmb, T5_PTR pConf)
{
    return memoryFree(mmb, pConf);
}

/*****************************************************************************
T5Memory_Link
establish a link to a block of memory
Parameters:
    mmb (IN/OUT) memory block descriptor
    pConf (IN) pointer to OEM data
return: OK or error
*****************************************************************************/

T5_RET T5Memory_Link (T5PTR_MMB mmb, T5_PTR pConf)
{
    return memoryLink(mmb, pConf);
}

/*****************************************************************************
T5Memory_Link
establish a link to a block of memory
Parameters:
    mmb (IN/OUT) memory block descriptor
    pConf (IN) pointer to OEM data
return: OK or error
*****************************************************************************/

T5_RET T5Memory_Unlink (T5PTR_MMB mmb, T5_PTR pConf)
{
    return memoryUnlink(mmb, pConf);
}

/*****************************************************************************
T5Memory_StartWrite
start writing backup support for download
Parameters:
    mmb (IN/OUT) memory block descriptor
    pLoad (IN) download information
    pConf (IN) pointer to OEM data
return: OK or error
*****************************************************************************/

T5_RET T5Memory_StartWrite (T5PTR_MMB mmb, T5PTR_CSLOAD pLoad, T5_PTR pConf)
{
    return T5RET_ERROR;
}

/*****************************************************************************
T5Memory_StartWrite
start writing backup support for download
Parameters:
    mmb (IN/OUT) memory block descriptor
    pLoad (IN) download information
    pConf (IN) pointer to OEM data
return: OK or error
*****************************************************************************/

T5_RET T5Memory_Write (T5PTR_MMB mmb, T5PTR_CSLOAD pLoad, T5_PTR pConf)
{
    return T5RET_ERROR;
}

/*****************************************************************************
T5Memory_EndWrite
finish writing backup support at the end of download
Parameters:
    mmb (IN/OUT) memory block descriptor
    pLoad (IN) download information
    pConf (IN) pointer to OEM data
return: OK or error
*****************************************************************************/

T5_RET T5Memory_EndWrite (T5PTR_MMB mmb, T5PTR_CSLOAD pLoad, T5_PTR pConf)
{
    return T5RET_ERROR;
}

/*****************************************************************************
T5Memory_Save
Save a memory block to backup support
Parameters:
    mmb (IN/OUT) memory block descriptor
    szAppName (IN) application name
    pConf (IN) pointer to OEM data
return: OK or error
*****************************************************************************/

T5_RET T5Memory_Save (T5PTR_MMB mmb, T5_PTCHAR szAppName, T5_PTR pConf)
{
    return T5RET_ERROR;
}

/* eof **********************************************************************/
