/*****************************************************************************
T5Can.c :    CAN-bus hardware interface - TO BE FILLED WHEN PORTING

(c) COPALP 2006
*****************************************************************************/

#include "t5vm.h"

#ifdef T5DEF_CANBUS

/*****************************************************************************
T5Can_PreOpen
Called by the STRATON driver before opening CAN ports
*****************************************************************************/

void T5Can_PreOpen (void)
{
}

/*****************************************************************************
T5Can_Open
Open a CAN port
Parameters:
    pPort (IN/OUT) pointer to the port object in driver memory
    szSettings (IN) configuration string entered in configuration
Return: OK or error
*****************************************************************************/

T5_RET T5Can_Open (T5PTR_DBCANPORT pPort, T5_PTCHAR szSettings)
{
    return T5RET_OK;
}

/*****************************************************************************
T5Can_PostClose
Called by the STRATON driver after closing CAN ports
*****************************************************************************/

void T5Can_PostClose (void)
{
}

/*****************************************************************************
T5Can_Close
Close a CAN port
Parameters:
    pPort (IN/OUT) pointer to the port object in driver memory
*****************************************************************************/

void T5Can_Close (T5PTR_DBCANPORT pPort)
{
}

/*****************************************************************************
T5Can_OnPreExchange
Called by the STRAATON driver before exchanging driver data
You can use T5VMCan_FindRcvMsg to find a received message in driver memory
Call T5VMCan_MarkRcvMsg to indicate that a message was received
Parameters:
    pPort (IN/OUT) pointer to the port object in driver memory
*****************************************************************************/

void T5Can_OnPreExchange (T5PTR_DBCANPORT pPort)
{
}

/*****************************************************************************
T5Can_OnPostExchange
Called by the STRATON driver after exchanging driver data
You can use T5VMCan_PeekNextSndMsg/T5VMCan_PopNextSndMsg to get the list
of messages to be sent
Parameters:
    pPort (IN/OUT) pointer to the port object in driver memory
*****************************************************************************/


void T5Can_OnPostExchange (T5PTR_DBCANPORT pPort)
{
}

/*****************************************************************************
T5Can_GetBusOffFlag
Get the CAN "Bus Off" error flag
Parameters:
    pPort (IN/OUT) pointer to the port object in driver memory
Return: BUS OFF flag
*****************************************************************************/

T5_BOOL T5Can_GetBusOffFlag (T5PTR_DBCANPORT pPort)
{
    return FALSE;
}

/*****************************************************************************
T5Can_GetErrors
Get the CAN errors (hardware specific)
Parameters:
    pPort (IN/OUT) pointer to the port object in driver memory
Return: CAN errors value
*****************************************************************************/

T5_DWORD T5Can_GetErrors (T5PTR_DBCANPORT pPort)
{
    return 0L;
}

/*****************************************************************************
T5CAN_OnSpcOut
This function is called when a New "hardware specific control" value is passed
to a configured CAN message
Parameters:
    pPort (IN/OUT) pointer to the port object in driver memory
    pMsg (IN/OUT) pointer to the message object in driver memory
*****************************************************************************/

void T5CAN_OnSpcOut (T5PTR_DBCANPORT pPort, T5PTR_DBCANMSG pMsg)
{
}

/****************************************************************************/
/* Raw access for commmunication gateway */

#ifdef T5DEF_CANRAW

T5_DWORD T5CANRaw_Open (T5_WORD wRate, T5_BOOL b2B, T5_PTCHAR szSettings)
{
    /* open the port and return a non null handle if OK */
    return 0L;
}

void T5CANRaw_Close (T5_DWORD dwPort)
{
    /* close the port */
}

T5_BOOL T5CANRaw_PopRcv (T5_DWORD dwPort, T5_PTDWORD pdwID,
                         T5_PTBYTE pbLen, T5_PTBYTE data)
{
    /* pop message received */
    return FALSE;
}

T5_BOOL T5CANRaw_Send (T5_DWORD dwPort, T5_DWORD dwID, T5_BYTE bLen,
                       T5_PTBYTE data, T5_BOOL b2B, T5_BOOL bRTR)
{
    /* send message */
    return FALSE;
}

#endif /*T5DEF_CANRAW*/

/****************************************************************************/

#endif /*T5DEF_CANBUS*/

/* eof **********************************************************************/
