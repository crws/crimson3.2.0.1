/*****************************************************************************
T5asi.c    : ASi hardware interface - TO BE FILLED WHEN PORTING
(c) COPALP 2002
*****************************************************************************/

#include "t5vm.h"

#ifdef T5DEF_ASI

/*****************************************************************************
T5Asi_IsHWOpen
Returs the status of the hardware
You must handle a static flag in this file to remember if HW is initialized.
This is because the same code is used for Hot Restart and On Line Change.
This function is called to know if the Open routine has to be called or not.
Return: TRUE if hardware initialized
*****************************************************************************/

T5_BOOL T5Asi_IsHWOpen (void)
{
    return FALSE;
}

/*****************************************************************************
T5Asi_Init
Initializes all masters
Parameters:
    pASI (IN/OUT) full description of ASi I/O mapping
Remarks:
    use the "hMaster" members in master descriptors for storing any handle
Return: OK or error (if error the application don't start
*****************************************************************************/

T5_RET T5Asi_Init (T5PTR_ASI pASI)
{
    return T5RET_OK;
}

/*****************************************************************************
T5Asi_Exit
Closes all masters
Parameters:
    pASI (IN/OUT) full description of ASi I/O mapping
*****************************************************************************/

void T5Asi_Exit (T5PTR_ASI pASI)
{
}

/*****************************************************************************
T5Asi_Exchange
Perform a full data exchange for all masters:
- Exchange all output data image
- Refresh all input data image
- Refresh master execution flags
- Refresh list of projected slaves
- Refresh list of active slaves
- Refresh list of detected slaves
- Refresh list of corrupted slaves
- Refresh list of slaves with periph fault
Parameters:
    pASI (IN/OUT) full description of ASi I/O mapping
*****************************************************************************/

void T5Asi_Exchange (T5PTR_ASI pASI)
{
}

/*****************************************************************************
T5Asi_ReadConfig
Read the complete ASi configuration from all masters
- Refresh lists of projected and detected profiles
- Refresh list of projected slaves
- Refresh list of active slaves
- Refresh list of detected slaves
- Refresh list of corrupted slaves
- Refresh list of slaves with periph fault
Parameters:
    pASI (IN/OUT) full description of ASi I/O mapping
*****************************************************************************/

void T5Asi_ReadConfig (T5PTR_ASI pASI)
{
}

/*****************************************************************************
T5Asi_ApplyConfig
Apply the ASi configuration to all masters
- Set list of projected profiles (in static def, FFFF means 'slave not here')
- Set list of projected slaves
Parameters:
    pASI (IN/OUT) full description of ASi I/O mapping
*****************************************************************************/

void T5Asi_ApplyConfig (T5PTR_ASI pASI)
{
}

/*****************************************************************************
T5Asi_ReadPP
Read permanent parameters
Parameters:
    pASI (IN/OUT) full description of ASi I/O mapping
    wMaster (IN) index of the master - WARNING: STARTS AT OFFSET #1 !
    wSlave (IN) address of the slave (1..31 / 33..63)
Return: parameters value
*****************************************************************************/

T5_BYTE T5Asi_ReadPP (T5PTR_ASI pASI, T5_WORD wMaster, T5_WORD wSlave)
{
    return 0;
}

/*****************************************************************************
T5Asi_WritePP
Write permanent parameters
Parameters:
    pASI (IN/OUT) full description of ASi I/O mapping
    wMaster (IN) index of the master - WARNING: STARTS AT OFFSET #1 !
    wSlave (IN) address of the slave (1..31 / 33..63)
    bValue (IN) Parameters value
Return: TRUE if OK
*****************************************************************************/

T5_BOOL T5Asi_WritePP (T5PTR_ASI pASI, T5_WORD wMaster, T5_WORD wSlave,
                       T5_BYTE bValue)
{
    return FALSE;
}

/*****************************************************************************
T5Asi_SendParameter
Send parameters
Parameters:
    pASI (IN/OUT) full description of ASi I/O mapping
    wMaster (IN) index of the master - WARNING: STARTS AT OFFSET #1 !
    wSlave (IN) address of the slave (1..31 / 33..63)
    bValue (IN) Parameters value
Return: TRUE if OK
*****************************************************************************/

T5_BOOL T5Asi_SendParameter (T5PTR_ASI pASI, T5_WORD wMaster, T5_WORD wSlave,
                             T5_BYTE bValue)
{
    return FALSE;
}

/*****************************************************************************
T5Asi_ReadPI
Read actual parameters
Parameters:
    pASI (IN/OUT) full description of ASi I/O mapping
    wMaster (IN) index of the master - WARNING: STARTS AT OFFSET #1 !
    wSlave (IN) address of the slave (1..31 / 33..63)
Return: parameters value
*****************************************************************************/

T5_BYTE T5Asi_ReadPI (T5PTR_ASI pASI, T5_WORD wMaster, T5_WORD wSlave)
{
    return 0;
}

/*****************************************************************************
T5Asi_StorePI
Store actual parameters as permanent parameters
Parameters:
    pASI (IN/OUT) full description of ASi I/O mapping
    wMaster (IN) index of the master - WARNING: STARTS AT OFFSET #1 !
Return: TRUE if OK
*****************************************************************************/

T5_BOOL T5Asi_StorePI (T5PTR_ASI pASI, T5_WORD wMaster)
{
    return FALSE;
}

/****************************************************************************/

#endif /*T5DEF_ASI*/

/* eof **********************************************************************/
