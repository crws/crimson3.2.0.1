/*****************************************************************************
T5RedRpl.c : Redundancy - replication link
(c) COPALP 2006
*****************************************************************************/

#include "t5vm.h"
#include "t5redapi.h"

/****************************************************************************/

T5_DWORD T5RedRpl_GetThisAddr (void)
{
    return 0;
}

T5_DWORD T5RedRpl_GetPartnerAddr (T5_PTCHAR szPartner)
{
    return 0;
}

void T5RedRpl_SetBlockingSocket (T5_SOCKET sock)
{
}

T5_RET T5Rpl_CreateListeningSocket (T5_WORD wPort, T5_WORD wMaxCnx,
                                    T5_PTSOCKET pSocket)
{
    return T5RET_ERROR;
}

void T5Rpl_CloseSocket (T5_SOCKET sock)
{
}

T5_SOCKET T5Rpl_Accept (T5_SOCKET sockListen)
{
    return T5_INVSOCKET;
}

T5_WORD T5Rpl_Send (T5_SOCKET sock, T5_WORD wSize, T5_PTR pData,
                    T5_PTBOOL pbFail)
{
    return 0;
}

T5_WORD T5Rpl_Receive (T5_SOCKET sock, T5_WORD wSize, T5_PTR pData,
                       T5_PTBOOL pbFail)
{
    return 0;
}

T5_RET T5Rpl_CreateConnectedSocket (T5_PTCHAR szAddr, T5_WORD wPort,
                                    T5_PTSOCKET pSocket, T5_PTBOOL pbWait)
{
    return T5RET_ERROR;
}

T5_RET T5Rpl_CheckPendingConnect (T5_SOCKET sock, T5_PTBOOL pbFail)
{
    return T5RET_ERROR;
}

/* eof **********************************************************************/
