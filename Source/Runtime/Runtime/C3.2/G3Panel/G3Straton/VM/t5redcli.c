/*****************************************************************************
T5RedCli.c : Redundancy - client
(c) COPALP 2006
*****************************************************************************/

#include "t5vm.h"
#include "t5redapi.h"

/****************************************************************************/

#ifdef T5RED_GTWUSESOCKET

#define T5REDGTW_CONNECT(a,p,s,b)   T5Socket_CreateConnectedSocket(a,p,s,b,NULL)
#define T5REDGTW_CCHECKCONNECT      T5Socket_CheckPendingConnect
#define T5REDGTW_CLOSE              T5Socket_CloseSocket
#define T5REDGTW_SEND               T5Socket_Send
#define T5REDGTW_RECEIVE            T5Socket_Receive

#else /*T5RED_GTWUSESOCKET*/

#define T5REDGTW_CONNECT        T5Rpl_CreateConnectedSocket
#define T5REDGTW_CCHECKCONNECT  T5Rpl_CheckPendingConnect
#define T5REDGTW_CLOSE          T5Rpl_CloseSocket
#define T5REDGTW_SEND           T5Rpl_Send
#define T5REDGTW_RECEIVE        T5Rpl_Receive

#endif /*T5RED_GTWUSESOCKET*/

/****************************************************************************/

#ifndef T5REDVM_BEGININIT
#define T5REDVM_BEGININIT /*nothing*/
#endif /*T5REDVM_BEGININIT*/

#ifndef T5REDVM_ENDINIT
#define T5REDVM_ENDINIT /*nothing*/
#endif /*T5REDVM_ENDINIT*/

/****************************************************************************/

#define GTW_RUN      1   /* hand shaking: thread is running */
#define GTW_KILLING  2   /* hand shaking: thread is asked to stop */
#define GTW_KILLED   0   /* hand shaking: thread has finished */

/****************************************************************************/

typedef struct
{
    T5_DWORD  dwCodeHere;
    T5_DWORD  dwVMID;
    T5PTR_MM  pmmData;
    T5PTR_DB  pDB;
    T5PTR_CS  pcsData;
    T5_PTR    pEvData;
    T5_SOCKET sockGTW;
    T5_DWORD  dwLastReg;
    T5_DWORD  dwSwitchTime;
    T5_WORD   wGTWAlive;
    T5_BOOL   bRunning;     /* master is running an application */
    T5_BOOL   bClientMode;  /* client is connected to master */
    T5_BOOL   bDBHere;      /* client has the complete master DB (ready) */
    T5_BOOL   res1;
    T5_BOOL   res2;
    T5_BOOL   res3;
} str_T5redcli;

/****************************************************************************/

void T5CliGtw_Thread (T5_PTR pArgs);

static T5_BOOL _BeSlave (str_T5redcli *pClient, T5_SOCKET sock,
                         T5_PTCHAR szPartner, T5_WORD wGtwPort);
static T5_WORD _ForwardFrame (str_T5redcli *pClient, T5_PTBYTE pQ, T5_PTBYTE pA);
static T5_WORD _ServeVM (str_T5redcli *pClient,
                         T5_PTBYTE pIn, T5_PTBYTE pOut, T5_PTBOOL pbDone);
static T5_WORD _ServeUser (str_T5redcli *pClient,
                           T5_PTBYTE pIn, T5_PTBYTE pOut, T5_PTBOOL pbDone);
static void _GetPartnerAppCode (T5_SOCKET sock);
static void _HandleLocalReqs (str_T5redcli *pClient,
                              T5_WORD wReq, T5_WORD wCaller, T5_PTBYTE pA);
static void _OnMasterStart (str_T5redcli *pClient, T5_SOCKET sock);
static void _OnMasterStop (str_T5redcli *pClient, T5_SOCKET sock);
static T5_BOOL _PrepareVMDB (str_T5redcli *pClient);
static void _ReleaseVMDB (str_T5redcli *pClient);
static T5_BOOL _GiveVMDB (str_T5redcli *pClient);
static void _GetDB (str_T5redcli *pClient, T5_SOCKET sock);
static void _StartGatewayThread (str_T5redcli *pClient);
static void _StopGatewayThread (str_T5redcli *pClient);
static void _ReceivePacket (T5_SOCKET sock, T5_DWORD dwSize, T5_DWORD dwOffset,
                            T5_PTR pRcv, T5_PTBOOL pbFail);
static void _CleanDB (str_T5redcli *pClient);

/****************************************************************************/

#define _S_IDLESTATE ((pClient->bRunning) ? T5REDS_SLVRUNNING : T5REDS_SLVIDLE)

/****************************************************************************/

#ifndef T5RED_ALV_TIMEOUT
#define T5RED_ALV_TIMEOUT 500
#endif /*T5RED_ALV_TIMEOUT*/

static T5_RET _T5RedCli_CheckAliveSignal (T5_BOOL bStrict)
{
#ifdef T5RED_ALVNOTSTRICT
    if (!bStrict)
        return T5REDALV_UNKNOWN;
#endif /*T5RED_ALVNOTSTRICT*/

    return T5RedAlv_ReadAliveSignal ();
}


T5_PTR T5RedCli_Init (void)
{
    str_T5redcli *pClient;

    pClient = T5RedSys_Malloc (sizeof (str_T5redcli));
    pClient->dwCodeHere = 0L;
    pClient->dwVMID = 0;
    pClient->pmmData = NULL;
    pClient->pDB = NULL;
    pClient->pcsData = NULL;
    pClient->pEvData = NULL;
    pClient->sockGTW = T5_INVSOCKET;
    pClient->wGTWAlive = GTW_KILLED;
    pClient->bRunning = FALSE;
    pClient->bClientMode = FALSE;

    return pClient;
}

void T5RedCli_Exit (T5_PTR pClientData)
{
    if (pClientData == NULL)
        return;

    T5RedSys_Free ((str_T5redcli *)pClientData);
}

T5_BOOL T5RedCli_IsClientEstablished (T5_PTR pClientData)
{
    str_T5redcli *pClient;

    if (pClientData == NULL)
        return FALSE;

    pClient = (str_T5redcli *)pClientData;
    return pClient->bClientMode;
}

T5_BOOL T5RedCli_WaitToBeMaster (T5_DWORD dwVMID, T5_PTR pClientData,
                                 T5_PTCHAR szPartner, T5_WORD wPort,
                                 T5PTR_CS pCS, T5_WORD wGtwPort,
                                 T5PTR_MM pMM, T5_PTR pEV,
                                 T5_PTDWORD pdwSwitchTime)
{
    T5_SOCKET sock;
    T5_BOOL bWait, bFail;
    T5_BOOL bIAmPrio;
    T5_CHAR sz[4];
    T5_DWORD dwLast;
    str_T5redcli *pClient;

    if (pClientData == NULL)
        return TRUE;

    pClient = (str_T5redcli *)pClientData;

    /* initialize data */
    pClient->dwVMID = dwVMID;
    pClient->pmmData = pMM;
    pClient->pcsData = pCS;
    pClient->pEvData = pEV;
    pClient->pDB = NULL;
    pClient->sockGTW = T5_INVSOCKET;
    pClient->wGTWAlive = GTW_KILLED;
    pClient->bRunning = FALSE;
    pClient->bClientMode = FALSE;
    pClient->bDBHere = FALSE;
    pClient->dwSwitchTime = 0L;

    /* highest IP address has priority at startup to be master */
    bIAmPrio = (T5RedRpl_GetPartnerAddr (szPartner)  < T5RedRpl_GetThisAddr ());

    if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD)
        return TRUE;

    /* don't need high priority before connection */
    T5RedSys_SetCurThreadPriority (FALSE);

    /* loop connecting as a slave */
    while (1)
    {
        /* dead signal: be master */
        if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD)
            return TRUE;

        /* cant connect and no alive signal: be master */
        if (T5Rpl_CreateConnectedSocket (szPartner, wPort, &sock, &bWait)
            != T5RET_OK && _T5RedCli_CheckAliveSignal (FALSE) != T5REDALV_ALIVE)
            return TRUE;

        /* wait for connection */
        bFail = FALSE;
        dwLast = T5RedSys_GetTickCount ();
        if (!(bWait && !bFail))
            T5RedSys_Sleep (10);
        while (bWait && !bFail)
        {
            if (T5Red_ShouldTerminate (pClient->dwVMID))
            {
#ifdef T5DEF_REDBUSDRVEXT
                if (pClient->pDB == NULL)
                    T5BusDrv_ClosePassive ();
#endif /*T5DEF_REDBUSDRVEXT*/
                _CleanDB (pClient);
                T5Rpl_CloseSocket (sock);
                return FALSE;
            }

            if (T5_BOUNDTIME (T5RedSys_GetTickCount()-dwLast) > T5RED_CNX_TIMEOUT)
            {
                _CleanDB (pClient);
                T5Rpl_CloseSocket (sock);
                return FALSE;
            }

            if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD)
			{
                T5Rpl_CloseSocket (sock);
                return TRUE;
			}

            /* connection is established */
            if (T5Rpl_CheckPendingConnect (sock, &bFail) == T5RET_OK)
            {
                bWait = FALSE;
                /* wait for partner identification */
                while (T5Rpl_Receive (sock, 4, sz, &bFail) != 4 && !bFail)
				{
					if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD)
					{
						T5Rpl_CloseSocket (sock);
						return TRUE;
					}
                    T5RedSys_Sleep (10);
				}
                if (!bFail)
                {
                    /* I decide first to be the master! */
                    if (sz[1] != T5RED_MASTER && bIAmPrio)
                    {
                        T5Rpl_CloseSocket (sock);
                        return FALSE;
                    }
                    /* else: the other is the master: loop as slave */
                    if (!_BeSlave (pClient, sock, szPartner, wGtwPort))
                    {
                        _CleanDB (pClient);
                        T5Rpl_CloseSocket (sock);
                        return FALSE;
                    }
                    /* if no alive link: consider ETHERNET as the alive link */
                    if (_T5RedCli_CheckAliveSignal (FALSE) != T5REDALV_ALIVE)
                    {
                        *pdwSwitchTime = pClient->dwSwitchTime;
                        T5Rpl_CloseSocket (sock);
                        return TRUE;
                    }
                }
                dwLast = T5RedSys_GetTickCount ();
            }
			else
				T5RedSys_Sleep (10);
            /* no connection or connection lost */
            if (bFail)
            {
                /* if no alive link: consider ETHERNET as the alive link */
                T5Rpl_CloseSocket (sock);
                if (_T5RedCli_CheckAliveSignal (FALSE) != T5REDALV_ALIVE)
                {
                    _CleanDB (pClient);
                    return FALSE;
                }
            }
        }
    }

    return TRUE;
}

T5_BOOL T5RedCli_WaitToBeMasterLoop (T5_DWORD dwVMID, T5_PTR pClientData,
                                     T5_PTCHAR szPartner, T5_WORD wPort,
                                     T5PTR_CS pCS, T5_WORD wGtwPort,
                                     T5PTR_MM pMM, T5_PTR pEV,
                                     T5_PTDWORD pdwSwitchTime)
{
    T5_BOOL bRet, bAlive;
    str_T5redcli *pClient;
    T5_DWORD dwLast;

    pClient = (str_T5redcli *)pClientData;
    do
    {
        bRet = T5RedCli_WaitToBeMaster (dwVMID, pClientData, szPartner, wPort, pCS, wGtwPort,
                                        pMM, pEV, pdwSwitchTime);

        dwLast = T5RedSys_GetTickCount ();
        do
        {
            bAlive = (_T5RedCli_CheckAliveSignal (TRUE) == T5REDALV_ALIVE);
            T5RedSys_Sleep (10);
        } while (bAlive && (T5_BOUNDTIME(T5RedSys_GetTickCount()-dwLast) < (2*T5RED_ALV_TIMEOUT)));

        if (bRet && bAlive)
            _CleanDB (pClient);
    }
    while (bAlive && !T5Red_ShouldTerminate (pClient->dwVMID));
    return bRet;
}

static T5_BOOL _BeSlave (str_T5redcli *pClient, T5_SOCKET sock,
                         T5_PTCHAR szPartner, T5_WORD wGtwPort)
{
    T5_BOOL bFail;
    T5_CHAR sz[10];
    T5_WORD wNb;
    T5_DWORD dwLast, dwNow;
    T5_BOOL bBreak=FALSE, bWait, bGtwFail;

    T5REDLOG ("C> Slave mode - connected to partner server...");

    bFail = FALSE;
    pClient->bClientMode = TRUE;

    /* release memory if looping on TCP reconnections */
	if (pClient->pDB != NULL)
	{
		_ReleaseVMDB (pClient);
		pClient->pDB = NULL;
	}
    pClient->bRunning = FALSE;

    /* before anything, check/load the partner app code */
    _GetPartnerAppCode (sock);
    pClient->bDBHere = FALSE;

    /* send our state */
    T5Rpl_Send (sock, 4, _S_IDLESTATE, &bFail);

    /* connect to partner main port for gateway job */
    if (wGtwPort != 0)
        T5REDGTW_CONNECT (szPartner, wGtwPort, &(pClient->sockGTW), &bWait);
    else
        pClient->sockGTW = T5_INVSOCKET;
    pClient->wGTWAlive = GTW_KILLED;

    /* important to serve master as fast as possible! */
    T5RedSys_SetCurThreadPriority (TRUE);

    /* loop as slave */
    dwLast = T5RedSys_GetTickCount ();
    while (!bBreak && !T5Red_ShouldTerminate (pClient->dwVMID))
    {
        if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD)
        {
            T5REDLOG ("C> dead signal detected");
            bBreak = TRUE;
        }
        else
        {
            /* establish comm with host */
            if (pClient->sockGTW != T5_INVSOCKET && bWait)
            {
                if (T5REDGTW_CCHECKCONNECT (pClient->sockGTW, &bGtwFail) == T5RET_OK)
                {
                    T5REDLOG ("C> Connection established with partner host link");
                    _StartGatewayThread (pClient);
                    bWait = FALSE;
                }
                else if (bGtwFail)
                {
                    T5REDGTW_CLOSE (pClient->sockGTW);
                    pClient->sockGTW = T5_INVSOCKET;
                }
            }
        }
        if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD)
        {
            T5REDLOG ("C> dead signal detected");
            bBreak = TRUE;
        }
        else
        {
            /* comm with the partner server */
            wNb = T5Rpl_Receive (sock, 4, sz, &bFail);
            if (!bFail && wNb == 4)
            {
                /* check if code needs to be updated */
                if (sz[1] == T5RED_MASTER && sz[2] == T5REDS_CODECHG)
                {
                    _OnMasterStop (pClient, sock);
                    _GetPartnerAppCode (sock);
                }
                /* check if full DB is sent */
                else if (sz[1] == T5RED_MASTER && sz[2] == T5REDS_SENDDB)
                    _GetDB (pClient, sock);

                /*...*/

                /* survey start/stop info from the master */
                if (!pClient->bRunning && sz[2] == T5REDS_RUNNING)
                    _OnMasterStart (pClient, sock);
                else if (pClient->bRunning && sz[2] == T5REDS_IDLE)
                    _OnMasterStop (pClient, sock);

                /* send our state for next exchange */
                T5Rpl_Send (sock, 4, _S_IDLESTATE, &bFail);
                /* no sleep here - T5RedSys_Sleep (1); */
                dwLast = T5RedSys_GetTickCount ();
            }
            /* exit if IP connection lost */
            if (bFail)
            {
                T5REDLOG ("C> Connection lost when receiving");
                bBreak = TRUE;
            }
            /* check timeout during conversation */
            else
            {
                dwNow = T5RedSys_GetTickCount ();
                if (T5_BOUNDTIME(dwNow-dwLast) > T5RED_CLI_TIMEOUT)
                {
                    T5REDLOG ("C> connection lost on timeout");
                    bBreak = TRUE;
                }
                else
                {
                    T5RedSys_Sleep (1);
                }
            }
        }
    }

    /* restore default priority before being master */
    T5RedSys_SetCurThreadPriority (FALSE);

    /* stop gateway job */
    _StopGatewayThread (pClient);
    if (pClient->sockGTW != T5_INVSOCKET)
    {
        T5REDGTW_CLOSE (pClient->sockGTW);
        pClient->sockGTW = T5_INVSOCKET;
    }

    /* unlink from app DB */
    if (pClient->pDB != NULL)
    {
        /* if switch to master mode, keep VM memory alive */
        if (bBreak && _T5RedCli_CheckAliveSignal (TRUE) != T5REDALV_ALIVE && _GiveVMDB (pClient))
        {
            pClient->dwSwitchTime = T5RedUti_RegisterSwitch (pClient->dwLastReg);
        }
        /* if termination asked by the user, release the memory */
        else
		{
            _ReleaseVMDB (pClient);
			pClient->pDB = NULL;
            bBreak = FALSE;
		}
    }
    else
        bBreak = FALSE;

    pClient->bClientMode = FALSE;

    return bBreak;
}

/****************************************************************************/

static void _StartGatewayThread (str_T5redcli *pClient)
{
    pClient->wGTWAlive = GTW_RUN;
    T5RedSys_StartThread (T5CliGtw_Thread, FALSE, pClient);
}

static void _StopGatewayThread (str_T5redcli *pClient)
{
    if (pClient->wGTWAlive != GTW_RUN)
        return;

	T5REDLOG ("C> Gateway thread will now stop");

    pClient->wGTWAlive = GTW_KILLING;
#ifndef T5DEF_WAITREDGTWEND
    if (T5Red_ShouldTerminate (pClient->dwVMID))
#endif /*T5DEF_WAITREDGTWEND*/
    {
        while (pClient->wGTWAlive != GTW_KILLED)
            T5RedSys_Sleep (10);
    }

	T5REDLOG ("C> Gateway thread is stopped");
}

static T5_PTR __pTrack;

void T5CliGtw_Thread (T5_PTR pArgs)
{
    T5_WORD wReq, wCaller;
    T5_PTBYTE pQ, pA;
    T5_WORD wLen;
    str_T5redcli *pClient;
    T5_BOOL bDone;

    pClient = (str_T5redcli *)pArgs;

    __pTrack = pClient->pcsData;

    while (pClient->wGTWAlive == GTW_RUN
        && _T5RedCli_CheckAliveSignal (FALSE) != T5REDALV_DEAD)
    {
        T5CS_Activate (pClient->pcsData);
        while (pClient->wGTWAlive == GTW_RUN
            && _T5RedCli_CheckAliveSignal (FALSE) != T5REDALV_DEAD
            && (wReq = T5CS_GetQuestion (pClient->pcsData, &wCaller)) != 0)
        {
            if (pClient->sockGTW == T5_INVSOCKET)
                T5CS_SendRC (pClient->pcsData, wCaller, T5RET_ERROR);
            else
            {
                bDone = FALSE;
                switch (wReq)
                {
                case T5CSRQ_VMREQ :
                    pQ = (T5_PTBYTE)T5CS_GetVMRequest (pClient->pcsData, wCaller);
                    pA = (T5_PTBYTE)T5CS_GetVMAnswerBuffer (pClient->pcsData, wCaller);
                    wLen = _ServeVM (pClient, pQ, pA, &bDone);
                    if (bDone)
                        T5CS_SendVMAnswer (pClient->pcsData, wCaller, wLen);
                    break;
                case T5CSRQ_USER :
                    pQ = (T5_PTBYTE)T5CS_GetRequestFrame (pClient->pcsData, wCaller);
                    pA = (T5_PTBYTE)T5CS_GetAnswerFrameBuffer (pClient->pcsData, wCaller);
                    wLen = _ServeUser (pClient, pQ, pA, &bDone);
                    if (bDone)
                        T5CS_SendAnswerFrame (pClient->pcsData, wCaller, wLen);
                    break;
                default :
                    break;
                }

                if (!bDone)
                {
                    pQ = (T5_PTBYTE)T5CS_GetRequestFrame (pClient->pcsData, wCaller);
                    pA = (T5_PTBYTE)T5CS_GetAnswerFrameBuffer (pClient->pcsData, wCaller);
                    wLen = _ForwardFrame (pClient, pQ, pA);
                    if (wLen != 0)
                    {
                        _HandleLocalReqs (pClient, wReq, wCaller, pA);
                        T5CS_SendAnswerFrame (pClient->pcsData, wCaller, wLen);
                    }
                    else
                        T5CS_SendRC (pClient->pcsData, wCaller, T5RET_ERROR);
                }
            }
            T5CS_ReleaseQuestion (pClient->pcsData, wCaller);
        }
        T5RedSys_Sleep (10);
    }
    pClient->wGTWAlive = GTW_KILLED;
}

static void _HandleLocalReqs (str_T5redcli *pClient, T5_WORD wReq, T5_WORD wCaller,
                              T5_PTBYTE pA)
{
    T5STR_CSLOAD load;
    T5_WORD wFlags;

    switch (wReq)
    {
    case T5CSRQ_VMREQ :
        if (pA[4] == 1 && pA[10] == 2)
        {
            T5_COPYFRAMEWORD (&wFlags, pA+13);
            wFlags |= T5FLAG_REDPSV;
            T5_COPYFRAMEWORD (pA+13, &wFlags);
        }
        break;
    case T5CSRQ_BEGINLOAD :
    case T5CSRQ_LOAD :
    case T5CSRQ_ENDLOAD :
        if (T5CS_GetLoadInfo (pClient->pcsData, wCaller, &load)
            && load.bType == T5CSLOADCODE)
        {
            T5MM_WriteCodeRequest (pClient->pmmData, wReq, &load);
        }
        break;
    default : break;
    }
}

static T5_WORD _ForwardFrame (str_T5redcli *pClient, T5_PTBYTE pQ, T5_PTBYTE pA)
{
    T5_WORD wLen, w, wExpect;
    T5_BOOL bFail;
    T5_PTBYTE pRcv;
    T5_DWORD dwStart;

    bFail = FALSE;
    pRcv = pA;
    dwStart = T5RedSys_GetTickCount ();
    /* send */
    T5_COPYFRAMEWORD (&wLen, pQ+2);
    wLen += 4; /* +4 = the header! */
    while (!bFail && wLen && pClient->wGTWAlive == GTW_RUN)
    {
        if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD
            || T5_BOUNDTIME(T5RedSys_GetTickCount () - dwStart) > T5RED_GTW_TIMEOUT)
            return 0;
        w = T5REDGTW_SEND (pClient->sockGTW, wLen, pQ, &bFail);
        if (w)
        {
            wLen -= w;
            pQ += w;
        }
        T5RedSys_Sleep (10);
    }
    /* read header */
    wLen = 0;
    while (wLen < 4 && !bFail && pClient->wGTWAlive == GTW_RUN)
    {
        if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD
            || T5_BOUNDTIME(T5RedSys_GetTickCount () - dwStart) > T5RED_GTW_TIMEOUT)
            return 0;
        w = T5REDGTW_RECEIVE (pClient->sockGTW, (T5_WORD)(4-wLen), pRcv, &bFail);
        if (w)
        {
            wLen += w;
            pRcv += w;
        }
        if (wLen < 4)
            T5RedSys_Sleep (10);
    }
    T5_COPYFRAMEWORD (&wLen, pA+2);
    /* read the rest */
    wExpect = wLen;
    while (wExpect && !bFail && pClient->wGTWAlive == GTW_RUN)
    {
        if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD
            || T5_BOUNDTIME(T5RedSys_GetTickCount () - dwStart) > T5RED_GTW_TIMEOUT)
            return 0;
        w = T5REDGTW_RECEIVE (pClient->sockGTW, (wExpect), pRcv, &bFail);
        if (w)
        {
            wExpect -= w;
            pRcv += w;
        }
        if (wExpect > 0)
            T5RedSys_Sleep (10);
    }
    return (wLen + 4); /* +4 = the header */
}

static T5_WORD _ServeVM (str_T5redcli *pClient,
                         T5_PTBYTE pIn, T5_PTBYTE pOut, T5_PTBOOL pbDone)
{
    T5_RET wRet, wPort;
    T5_WORD wLenIn, wLenOut;
    T5_DWORD dwPass;
    T5PTR_DBPRIVATE pPrivate;

    T5TRACECOMDW("Serve", pIn);
    T5_COPYFRAMEWORD(&wLenIn, pIn+2);
    wLenOut = 0;
    switch (pIn[1])
    {
    case T5REQ_INIT :
        if (pClient->pDB == NULL)
            wRet = T5RET_OK;
        else
        {
            if (wLenIn < 4)
                dwPass = 0;
            else
            {
                T5_COPYFRAMEDWORD(&dwPass, pIn+4);
            }
            pPrivate = T5GET_DBPRIVATE(pClient->pDB);
            if (pPrivate->dwPassword == 0 || pPrivate->dwPassword == dwPass)
                wRet = T5RET_OK;
            else
                wRet = T5RET_ERROR;
        }
        wPort = 0;
        T5_COPYFRAMEWORD(pOut+4, &wPort);
        wLenOut = 2;
        *pbDone = TRUE;
        break;
    case T5REQ_SUBSCRIBE :
    case T5REQ_UNSUBSCRIBE :
    case T5REQ_SUBSCRIBEL :
    case T5REQ_UNSUBSCRIBEL :
        wRet = T5RET_ERROR;
        *pbDone = TRUE;
        break;
    default :
        *pbDone = FALSE;
        break;
    }
    if (!(*pbDone))
        return 0;

    pOut[0] = pIn[0]; /* echo caller ID */
    if (wRet == T5RET_OK) pOut[1] = 0;
    else pOut[1] = 0xff;
    T5_COPYFRAMEWORD(pOut+2, &wLenOut);
    return (wLenOut + 4);
}

static T5_WORD _ServeUser (str_T5redcli *pClient,
                           T5_PTBYTE pIn, T5_PTBYTE pOut, T5_PTBOOL pbDone)
{
    T5_PTCHAR pCmd, pAnswer;

    *pbDone = FALSE;

    pCmd = (T5_PTCHAR)(pIn + 5);
    pCmd[pIn[3]-2] = 0;

    if (T5_STRCMP (pCmd, "t5red?") != 0)
        return 0;

    pAnswer = "t5red=C;";

    pOut[0] = pIn[0];
    pOut[1] = pIn[1];
    pOut[2] = 0;
    pOut[3] = (T5_BYTE)(T5_STRLEN(pAnswer) + 2);
    pOut[4] = T5CSRQ_USER;
    T5_STRCPY (pOut+5, pAnswer);

    *pbDone = TRUE;
    return (T5_STRLEN(pAnswer) + 6);
}

/****************************************************************************/

static void _GetPartnerAppCode (T5_SOCKET sock)
{
    T5_BOOL bFail;
    T5_CHAR sz[20];
    T5_DWORD dwCRCM, dwCRCS, dwSize;
    T5STR_MMB mmb;
    T5_PTBYTE pWrite;
    T5_WORD w, wPacket;
    T5_DWORD dwLast;

    bFail = FALSE;
    /* ask for code checksum and size */
    T5Rpl_Send (sock, 4, T5REDS_SLVCODECRC, &bFail);
    while (!bFail && T5Rpl_Receive (sock, 12, sz, &bFail) != 12)
        T5RedSys_Sleep (10);
    if (bFail)
        return;

    T5_COPYFRAMEDWORD (&dwCRCM, sz+4);
    T5_COPYFRAMEDWORD (&dwSize, sz+8);
    dwCRCS = T5RedUti_GetLocalCodeCRC (NULL, NULL);

    T5REDLOG ("C> Check CRCs: Local=%08lX - Master=%08lX (size=%lu)",
            dwCRCS, dwCRCM, dwSize);

    /* check if same code */
    if (dwCRCS == dwCRCM)
    {
        T5REDLOG ("C> App code is up to date");
        return;
    }

    /* check if code available */
    if (dwSize == 0)
    {
        T5REDLOG ("C> No code available from master");
        /* could remove local code... */
        return;
    }

    /* ask for updated code */
    T5_MEMSET (&mmb, 0, sizeof (mmb));
    mmb.wID = T5MM_CODE;
    if (T5Memory_Alloc (&mmb, dwSize, NULL) != T5RET_OK)
    {
        T5REDLOG ("C> ERROR: Cannot allocate memory for code");
        return;
    }

    if (T5Memory_Link (&mmb, NULL) != T5RET_OK)
    {
        T5REDLOG ("C> ERROR: Cannot link to memory for code");
        T5Memory_Free (&mmb, NULL);
        return;
    }
    pWrite = (T5_PTBYTE)(mmb.pData);

    T5Rpl_Send (sock, 4, T5REDS_SLVCODETFR, &bFail);
    dwLast = T5RedSys_GetTickCount ();
    while (dwSize && !bFail)
    {
        if (T5_BOUNDTIME(T5RedSys_GetTickCount () - dwLast) > T5RED_CLI_TIMEOUT)
            bFail = TRUE;
        else
        {
            wPacket = (dwSize > T5RED_PACK_SIZE) ? T5RED_PACK_SIZE : (T5_WORD)dwSize;
            w = T5Rpl_Receive (sock, wPacket, pWrite, &bFail);
            if (w)
            {
                dwSize -= (T5_DWORD)w;
                pWrite += w;
                dwLast = T5RedSys_GetTickCount ();
            }
            else
                T5RedSys_Sleep (10);
        }
    }
    T5REDLOG ("C> App code loaded from master");

    T5Memory_Unlink (&mmb, NULL);

    if (T5Memory_Save (&mmb, NULL, NULL) != T5RET_OK)
        T5REDLOG ("C> ERROR: Cannot save memory for code");
    T5Memory_Free (&mmb, NULL);
}

static void _OnMasterStart (str_T5redcli *pClient, T5_SOCKET sock)
{
    if (T5MM_LoadCode (pClient->pmmData, NULL) != T5RET_OK)
    {
        T5REDLOG ("C> ERROR: Cannot load application code");
        return;
    }

    if (!_PrepareVMDB (pClient))
        return;
    
    T5REDLOG ("C> Master is running an application!");
    pClient->bRunning = TRUE;
}

static void _OnMasterStop (str_T5redcli *pClient, T5_SOCKET sock)
{
    T5REDLOG ("C> Master has stopped the application (asked by debugger)!");

    if (pClient->pDB != NULL)
    {
        _ReleaseVMDB (pClient);
        pClient->pDB = NULL;
        pClient->bDBHere = FALSE;
    }
    T5RedUti_RegisterAppCode (pClient, NULL);
    pClient->bRunning = FALSE;
}

static T5_BOOL _PrepareVMDB (str_T5redcli *pClient)
{
    T5_PTR pCode;
    T5STR_DBTABLE tApp[T5MAX_TABLE];
    T5_WORD wRet;
    T5_DWORD dwSize;

    if ((pCode = T5MM_LinkCode (pClient->pmmData)) == NULL)
    {
        T5REDLOG ("C> ERROR: Cannot link to application code");
        return FALSE;
    }

    T5RedUti_RegisterAppCode (pClient, pCode);

    T5_MEMSET (tApp, 0, sizeof (tApp));
    wRet = T5VM_GetDBSize (pCode, tApp);
    if (wRet != T5RET_OK)
        T5REDLOG ("C> ERROR: Cannot get application size");
    else
    {
        dwSize = T5VM_GetTotalDBSize (tApp);
        wRet = T5MM_AllocRawVMDB (pClient->pmmData, dwSize);
        if (wRet != T5RET_OK)
            T5REDLOG ("C> ERROR: Cannot allocate application data");
        else
        {
            pClient->pDB = (T5PTR_DB)T5MM_LinkVMDB (pClient->pmmData);
            if (pClient->pDB == NULL)
            {
                T5REDLOG ("C> ERROR: Cannot link to application data");
                T5MM_FreeVMDB (pClient->pmmData);
                wRet = T5RET_ERROR;
            }
            else
            {
                T5_MEMCPY (pClient->pDB, tApp, sizeof (tApp));
                T5VM_BuildDBMainLinks (pClient->pDB);
                T5REDLOG ("C> App memory allocated: %lu bytes", dwSize);
            }
        }
    }

    T5REDVM_BEGININIT
    if (wRet == T5RET_OK)
    {
        wRet = T5VM_BuildEx (pCode, pClient->pDB, FALSE, FALSE);
    }
    if (wRet == T5RET_OK)
        T5VM_PrepareRetains (pClient->pDB, T5RED_RETAINSETTINGS(pClient->dwVMID));
    if (wRet == T5RET_OK)
        wRet = T5VM_OpenModbus (pClient->pDB);
    if (wRet == T5RET_OK && pClient->pEvData != NULL)
        wRet = T5EA_Open (pCode, pClient->pDB, pClient->pEvData);
    T5REDVM_ENDINIT

    T5MM_UnlinkCode (pClient->pmmData);
    return (wRet == T5RET_OK);
}

static void _GetDB (str_T5redcli *pClient, T5_SOCKET sock)
{
    T5_BOOL bFail;
    T5_BOOL bStop;
    T5_BYTE bTable;
    T5_DWORD dwBuf, dwSize, dwOffset;

    if (pClient->pDB == NULL)
        return;

    pClient->dwLastReg = T5RedUti_RegisterPartnerCycle ();

    bFail = FALSE;
    bStop = FALSE;
    while (!bFail && !bStop)
    {
#ifdef T5DEF_REDBUSDRVEXT
        T5BusDrv_ExchangePassive ();
#endif /*T5DEF_REDBUSDRVEXT*/
		if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD)
			bFail = TRUE;
		else
		{
			_ReceivePacket (sock, 1, 0, &bTable, &bFail);
			if (bTable == 0xff)
				bStop = TRUE;
			else
			{
				if (!bFail)
                {
                    _ReceivePacket (sock, 4, 0, &dwBuf, &bFail);
				    T5_COPYFRAMEDWORD (&dwSize, &dwBuf);
                }
				if (!bFail)
				{
				    _ReceivePacket (sock, 4, 0, &dwBuf, &bFail);
				    T5_COPYFRAMEDWORD (&dwOffset, &dwBuf);
                }
				if (!bFail)
				{
					_ReceivePacket (sock, dwSize, dwOffset,
                                    pClient->pDB[bTable].pData, &bFail);
				}
			}
        }
    }
    pClient->bDBHere = TRUE;
}

static void _ReceivePacket (T5_SOCKET sock, T5_DWORD dwSize, T5_DWORD dwOffset,
                            T5_PTR pRcv, T5_PTBOOL pbFail)
{
    T5_DWORD dwLast;
    T5_PTBYTE pWrite;
    T5_WORD w, wPacket;

    pWrite = (T5_PTBYTE)pRcv;
    pWrite += dwOffset;
    dwLast = T5RedSys_GetTickCount ();
    while (dwSize && !(*pbFail))
    {
        if (_T5RedCli_CheckAliveSignal (FALSE) == T5REDALV_DEAD
			|| T5_BOUNDTIME(T5RedSys_GetTickCount () - dwLast) > T5RED_CLI_TIMEOUT)
            *pbFail = TRUE;
        else
        {
            wPacket = (dwSize > T5RED_PACK_SIZE) ? T5RED_PACK_SIZE : (T5_WORD)dwSize;
            w = T5Rpl_Receive (sock, wPacket, pWrite, pbFail);
            if (w)
            {
                dwSize -= (T5_DWORD)w;
                pWrite += w;
                dwLast = T5RedSys_GetTickCount ();
            }
            /* No Sleep here - goes as fast as possible */
        }
    }
}

static void _ReleaseVMDB (str_T5redcli *pClient)
{
    T5REDLOG ("C> App memory released");

    T5REDVM_BEGININIT
    if (pClient->pEvData != NULL)
        T5EA_Close (pClient->pDB, pClient->pEvData);

    T5VM_CloseModbus (pClient->pDB);

    T5VMTic_Exit (pClient->pDB);
    T5VMFbl_ShutDown (pClient->pDB);
#ifdef T5DEF_ASI
    T5VMAsi_Close (T5GET_ASI(pClient->pDB));
#endif /*T5DEF_ASI*/

#ifdef T5DEF_BUSDRV
#ifdef T5DEF_REDBUSDRVEXT
    T5BusDrv_ClosePassive ();
#else /*T5DEF_REDBUSDRVEXT*/
    T5BusDrv_Close (pClient->pDB);
#endif /*T5DEF_REDBUSDRVEXT*/
#endif /*T5DEF_BUSDRV*/
    T5Ios_Close (T5VMCode_GetIODef (T5GET_DBPRIVATE(pClient->pDB)->pCode),
                 pClient->pDB, T5GET_DBIODATA(pClient->pDB));
    T5VMRsc_FreeAll (pClient->pDB);
    T5REDVM_ENDINIT

    T5MM_UnlinkVMDB (pClient->pmmData);
    T5MM_FreeVMDB (pClient->pmmData);
	T5MM_FreeCode (pClient->pmmData);
}

static T5_BOOL _GiveVMDB (str_T5redcli *pClient)
{
    T5_PTR pCode;
    T5PTR_DBSTATUS pStatus;
    T5_BOOL bRet;
    T5PTR_DBPRIVATE pPrivate;

    bRet = TRUE;
    pCode = T5MM_LinkCode (pClient->pmmData);
    pPrivate = T5GET_DBPRIVATE(pClient->pDB);
    pPrivate->pCode = pCode;

    pStatus = T5GET_DBSTATUS(pClient->pDB);
    pStatus->wNbLog = 0;
    pStatus->wNbUnres = 0;
    pStatus->wExecSize = 0;
    pStatus->dwTimStamp = T5RedSys_GetTickCount ();
    pStatus->wNbActime = 0; /* dont work! */

    T5REDVM_BEGININIT
    T5VMCode_SetProgramPointers (pCode, pClient->pDB);
#ifdef T5RED_REPLICATEVSI
    T5VMMap_HotRestart (pClient->pDB, pCode);
#endif /*T5RED_REPLICATEVSI*/
    T5VMFbl_InitInstances (pCode, pClient->pDB, FALSE);
    T5VMFbl_InitNewInstances (pClient->pDB, pClient->pDB[T5TB_FBINST].wNbItemUsed);

    T5VMTmr_HotRestartTimers (pClient->pDB);

    T5Ios_Open (T5VMCode_GetIODef (pCode), pClient->pDB, T5GET_DBIODATA(pClient->pDB));
#ifdef T5DEF_BUSDRV
    if (T5BusDrv_Build (pClient->pDB, pCode) != T5RET_OK)
    {
        bRet = FALSE;
#ifdef T5DEF_REDBUSDRVEXT
        T5Red_OnSwitchError (pClient->dwVMID);
#endif /*T5DEF_REDBUSDRVEXT*/
    }
#endif /*T5DEF_BUSDRV*/

#ifdef T5DEF_DDKC
    if (bRet)
    {
        if (T5DDKC_BuildHot (pClient->pDB, pCode) != T5RET_OK)
        {
#ifdef T5DEF_REDBUSDRVEXT
            T5Red_OnSwitchError (pClient->dwVMID);
#endif /*T5DEF_REDBUSDRVEXT*/
            bRet = FALSE;
        }
    }
#endif /*T5DEF_DDKC*/


    if (bRet)
        T5VM_OpenExtData (pClient->pDB);
    T5REDVM_ENDINIT

    T5MM_UnlinkCode (pClient->pmmData);
    T5MM_UnlinkVMDB (pClient->pmmData);

    T5REDLOG ("C> App memory prepared for active mode");
    return bRet;
}

static void _CleanDB (str_T5redcli *pClient)
{
    /* COPALP: cleanup DB if allocated */
    if (pClient->pDB != NULL)
    {
        T5REDLOG ("C> Releasing VMDB..."); 
        _ReleaseVMDB (pClient);
        pClient->pDB = NULL;
    }
}

/* eof **********************************************************************/
