/*****************************************************************************
T5retain.c : support of RETAIN variables - CAN BE CHANGED FOR PORT PURPOSE
(c) COPALP 2002
*****************************************************************************/

#include "t5vm.h"

#include "..\platform.h"

/****************************************************************************/

#ifdef T5DEF_RETAIN

/*****************************************************************************
T5Retain_CanLoad
Check if RETAIN variables can be loaded
Parameters:
    dwCrc (IN) expected RETAIN data CRC
    pArgs (IN) configuration string from the main caller
Return: TRUE if valid data available
*****************************************************************************/

T5_BOOL T5Retain_CanLoad (T5_DWORD dwCrc, T5_PTR pArgs)
{
    return retainCanLoad(dwCrc, pArgs);
}

/*****************************************************************************
T5Retain_Load
Load RETAIN variables
Parameters:
    pRetain (IN) pointers and size in the DB
    pArgs (IN) configuration string from the main caller
Return: OK or error
*****************************************************************************/

T5_RET T5Retain_Load (T5PTR_DBRETAIN pRetain, T5_PTR pArgs)
{
    return retainLoad(pRetain, pArgs);
}

/*****************************************************************************
T5Retain_CanStore
Check if RETAIN variables can be stored (enough space)
Parameters:
    pRetain (IN) pointers and size in the DB
    pArgs (IN) configuration string from the main caller
Return: TRUE if valid data available
*****************************************************************************/

T5_BOOL T5Retain_CanStore (T5PTR_DBRETAIN pRetain, T5_PTR pArgs)
{
    return retainCanStore(pRetain, pArgs);
}

/*****************************************************************************
T5Retain_ShutDown
Called at shutdown for closing the storage of RETAIN variables
Parameters:
    pRetain (IN) pointers and size in the DB
    pArgs (IN) configuration string from the main caller
*****************************************************************************/

void T5Retain_ShutDown (T5PTR_DBRETAIN pRetain, T5_PTR pArgs)
{
	retainShutDown(pRetain, pArgs);
	}

/*****************************************************************************
T5Retain_CycleExchange
Called on each cycle for managing RETAIN variables
Parameters:
    pRetain (IN) pointers and size in the DB
    pArgs (IN) configuration string from the main caller
*****************************************************************************/

void T5Retain_CycleExchange (T5PTR_DBRETAIN pRetain)
{
	retainCycleExchange(pRetain);
	}

/****************************************************************************/

#endif /*T5DEF_RETAIN*/

/* eof **********************************************************************/
