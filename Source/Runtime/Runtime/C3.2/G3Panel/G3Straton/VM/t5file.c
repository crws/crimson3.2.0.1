/*****************************************************************************
T5file.c : file management - CAN BE CHANGED FOR PORT PURPOSE
(c) COPALP 2002
*****************************************************************************/

#include "t5vm.h"

/****************************************************************************/

#ifdef T5DEF_FILESUPPORTED

/*****************************************************************************
T5File_Open
Open a file for READ or WRITE - always in BINARY mode
Parameters:
    szPathname (IN) pathname of the file (C string)
    bWrite (IN) TRUE for write access - FALSE for read
return: handle of the open file or NULL if fail
*****************************************************************************/

T5_LONG T5File_Open (T5_PTCHAR szPathname, T5_BOOL bWrite)
{
    return 0L;
}

T5_LONG T5File_OpenAppend (T5_PTCHAR szPathname)
{
    return 0L;
}

/*****************************************************************************
T5File_Close
close an open file
Parameters:
    hFile (IN) handle of a valid open file
*****************************************************************************/

void T5File_Close (T5_LONG hFile)
{
}

/*****************************************************************************
T5File_Eof
check for end of file
Parameters:
    hFile (IN) handle of a valid open file
return: TRUE if EOF is encountered
*****************************************************************************/

T5_BOOL T5File_Eof (T5_LONG hFile)
{
    return TRUE;
}

/*****************************************************************************
T5File_Read
read binary data in native format from file
Parameters:
    hFile (IN) handle of a valid open file
    pData (IN) pointer where to store read data
    dwSize (IN) wished data size in native format
return: TRUE if OK
*****************************************************************************/

T5_BOOL T5File_Read (T5_LONG hFile, T5_PTR pData, T5_LONG dwSize)
{
    return FALSE;
}

/*****************************************************************************
T5File_Write
write binary data in native format to file
Parameters:
    hFile (IN) handle of a valid open file
    pData (IN) pointer to the data
    dwSize (IN) wished data size in native format
return: TRUE if OK
*****************************************************************************/

T5_BOOL T5File_Write (T5_LONG hFile, T5_PTR pData, T5_LONG dwSize)
{
    return FALSE;
}

/*****************************************************************************
T5File_ReadLine
read a text line from file
Parameters:
    hFile (IN) handle of a valid open file
    pString (IN) pointer to the string buffer (C string)
    bMaxLen (IN) maximum allowed size for the read string
return: number of characters read
*****************************************************************************/

T5_BYTE T5File_ReadLine (T5_LONG hFile, T5_PTCHAR pString, T5_BYTE bMaxLen)
{
    return 0;
}

/*****************************************************************************
T5File_WriteLine
write a text line to file: string plus EOS character(s)
Parameters:
    hFile (IN) handle of a valid open file
    pString (IN) pointer to the string (C string)
return: TRUE if OK
*****************************************************************************/

T5_BOOL T5File_WriteLine (T5_LONG hFile, T5_PTCHAR pString)
{
    return FALSE;
}

/*****************************************************************************
T5File_Exist
test if a file exists
Parameters:
    pPath (IN) file pathname
return: TRUE if exist
*****************************************************************************/

#ifdef T5DEF_FILEMGT

T5_BOOL T5File_Exist (T5_PTCHAR pPath)
{
    return FALSE;
}

#endif /*T5DEF_FILEMGT*/

/*****************************************************************************
T5File_GetSize
get the size of a file
Parameters:
    pPath (IN) file pathname
return: size in bytes
*****************************************************************************/

#ifdef T5DEF_FILEMGT

T5_DWORD T5File_GetSize (T5_PTCHAR pPath)
{
    return 0L;
}

#endif /*T5DEF_FILEMGT*/

/*****************************************************************************
T5File_Copy
copy a file
Parameters:
    pSrc (IN) source file pathname
    pDst (IN) destination file pathname
return: TRUE if OK
*****************************************************************************/

#ifdef T5DEF_FILEMGT

T5_BOOL T5File_Copy (T5_PTCHAR pSrc, T5_PTCHAR pDst)
{
    return FALSE;
}

#endif /*T5DEF_FILEMGT*/

/*****************************************************************************
T5File_Delete
remove a file
Parameters:
    pPath (IN) file pathname
return: TRUE if OK
*****************************************************************************/

#ifdef T5DEF_FILEMGT

T5_BOOL T5File_Delete (T5_PTCHAR pPath)
{
    return FALSE;
}

#endif /*T5DEF_FILEMGT*/

/*****************************************************************************
T5File_Rename
rename a file
Parameters:
    pPath (IN) source file pathname
    pNewName (IN) New file name
return: TRUE if OK
*****************************************************************************/

#ifdef T5DEF_FILEMGT

T5_BOOL T5File_Rename (T5_PTCHAR pPath, T5_PTCHAR pNewName)
{
    return FALSE;
}

#endif /*T5DEF_FILEMGT*/

/****************************************************************************/

#endif /*T5DEF_FILESUPPORTED*/

/* eof **********************************************************************/
