
#include "intern.hpp"

#include "vm/t5vm.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Variable Wrapper
//

// Constructor

CVariable::CVariable(T5PTR_DBMAP pVar)
{
	m_pVar = pVar;
	}

// Development

void CVariable::ShowAll(void)
{
	AfxTrace("symbol\t<%s>\n",	GetSymbol());
	AfxTrace("type\t<%d>\n",	GetType());
	AfxTrace("numtag\t<%d>\n",	GetNumTag());
	AfxTrace("profile\t<%s>\n",	GetProfileName());
	}

// Attributes

BOOL CVariable::IsExtern(void)
{	
	return GetType() & T5C_EXTERN;
	}

BOOL CVariable::IsArray (void)
{
	return GetDim() > 0;
	}

BOOL CVariable::IsAllowed(void)
{
	T5_WORD wType = GetType() & ~T5C_EXTERN;

	switch( wType ) {
				
		case T5C_BOOL:

		case T5C_SINT:
		case T5C_USINT:

		case T5C_INT:
		case T5C_UINT:

		case T5C_DINT:
		case T5C_UDINT:

		case T5C_REAL:

		case T5C_LINT:
		case T5C_ULINT:
		case T5C_LREAL:

		case T5C_TIME:

		case T5C_STRING:

		/*case T5C_COMPLEX:*/

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CVariable::IsProfile(PCTXT pName)
{
	T5_PTCHAR pProfile = GetProfileName();

	return pProfile && !stricmp(pProfile, pName);
	}

CString CVariable::GetPropAsText(void)
{
	T5_PTR pProps = GetProps();

	if( pProps ) {

		CVariableData::CProfile const &Profile = *((CVariableData::CProfile *) pProps);

		return CPrintf("src:%8.8X, acc %d", 
				Profile.m_dwSrc,
				Profile.m_dwAcc
				);
		}

	return CString("<not set>");
	}

CString	CVariable::GetDataAsText(void)
{
	T5_WORD wType = GetType() & ~T5C_EXTERN;

	switch( wType ) {
				
		case T5C_BOOL:

			return CString(GetDWordValue() ? "true" : "false");

		case T5C_SINT:
		case T5C_USINT:

			return CPrintf("%d", GetDWordValue());

		case T5C_INT:
		case T5C_UINT:

			return CPrintf("%d", GetDWordValue());

		case T5C_DINT:
		case T5C_UDINT:

			return CPrintf("%d", GetDWordValue());

		case T5C_REAL:

			if( TRUE ) {

				T5_REAL r = GetRealValue();

				char s[32];

				gcvt(r, 5, s);

				return CString(s);
				}

			return CPrintf("%r", GetRealValue());

		case T5C_LINT:
		case T5C_ULINT:

			return CPrintf("%ll", GetLIntValue());

		case T5C_LREAL:

			if( TRUE ) {

				T5_LREAL r = GetLRealValue();

				int  dec = 4;
				int sign = 0;

				return CString(ecvt(r, 8, &dec, &sign)); 
				}

			break;

		case T5C_TIME:

			if( TRUE ) {
				
				T5_DWORD d = GetDWordValue();

				return CPrintf("%d", d);
				}

		case T5C_STRING:

			if( TRUE ) {

				T5_BYTE bLen;

				T5_PTBYTE p = GetStringValue(bLen);

				char s[255];

				memcpy(s, p, bLen);
				
				return CString(s, bLen);
				}

		/*case T5C_COMPLEX:*/

			break;
		}

	return CString("<nosup>");
	}

// Operations

T5_PTCHAR CVariable::GetSymbol(void)
{
	return T5Map_GetSymbol(m_pVar);
	}

T5_WORD	CVariable::GetType(void)
{
	return T5Map_GetType(m_pVar);
	}

T5_WORD	CVariable::GetIndex(void)
{
	return T5Map_GetIndex(m_pVar);
	}

T5_WORD	CVariable::GetDim(void)
{
	return T5Map_GetDim(m_pVar);
	}

T5_WORD	CVariable::GetStringLength(void)
{
	return T5Map_GetStringLenth(m_pVar);
	}

T5_WORD	CVariable::GetNumTag(void)
{
	return T5Map_GetNumTag(m_pVar);
	}

T5_PTCHAR CVariable::GetProfileName(void)
{
	return T5Map_GetProfileName(m_pVar);
	}

T5_PTR CVariable::GetProps(void)
{
	return T5Map_GetProps(m_pVar);
	}

T5_WORD CVariable::GetPropsSize(void)
{
	return T5Map_GetPropsSize(m_pVar);
	}

// VM Access

T5_LONG	CVariable::GetLongValue(void)
{
	return T5Map_GetLongValue(m_pVar);
	}

T5_DWORD CVariable::GetDWordValue(void)
{
	return T5Map_GetDWordValue(m_pVar);
	}

T5_LINT CVariable::GetLIntValue(void)
{
	return T5Map_GetLIntValue(m_pVar);
	}

T5_REAL	CVariable::GetRealValue(void)
{
	return T5Map_GetRealValue(m_pVar);
	}

T5_LREAL CVariable::GetLRealValue(void)
{
	return T5Map_GetLRealValue(m_pVar);
	}

T5_PTBYTE CVariable::GetStringValue(T5_BYTE &bLen)
{
	return T5Map_GetStringValue(m_pVar, &bLen);
	}

T5_RET CVariable::SetLongValue(T5_LONG Value)
{
	return T5Map_SetLongValue(m_pVar, Value);
	}

T5_RET CVariable::SetDWordValue(T5_DWORD Value)
{
	return T5Map_SetDWordValue(m_pVar, Value);
	}

T5_RET CVariable::SetLIntValue(T5_LINT Value)
{
	return T5Map_SetLIntValue(m_pVar, Value);
	}

T5_RET CVariable::SetRealValue(T5_REAL Value)
{
	return T5Map_SetRealValue(m_pVar, Value);
	}

T5_RET CVariable::SetLRealValue(T5_LREAL Value)
{
	return T5Map_SetLRealValue(m_pVar, Value);
	}

T5_RET CVariable::SetStringValue(T5_PTBYTE Value, T5_BYTE bLen)
{
	return T5Map_SetStringValue(m_pVar, Value, bLen);
	}

// End of File
