
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_VARDATA_HPP

#define	INCLUDE_VARDATA_HPP

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - BOOL
//

class CVariableBool : public CVariableData
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableBool(T5PTR_DBMAP pVar);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - BOOL Array
//

class CVariableBoolArray : public CVariableDataArray
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableBoolArray(T5PTR_DBMAP pVar, T5_PTR pArr, UINT uSize);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);

	protected:
		// Data
		T5_PTBOOL	m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - BYTE
//

class CVariableByte : public CVariableData
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableByte(T5PTR_DBMAP pVar);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);

	protected:
		// Data
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - BYTE
//

class CVariableByteArray : public CVariableDataArray
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableByteArray(T5PTR_DBMAP pVar, T5_PTR pArr, UINT uSize);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);

	protected:
		// Data
		T5_PTBYTE	m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - WORD
//

class CVariableWord : public CVariableData
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableWord(T5PTR_DBMAP pVar);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);

	protected:
		// Data
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - WORD
//

class CVariableWordArray : public CVariableDataArray
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableWordArray(T5PTR_DBMAP pVar, T5_PTR pArr, UINT Size);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);

	protected:
		// Data
		T5_PTWORD	m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - LONG
//

class CVariableLong : public CVariableData
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableLong(T5PTR_DBMAP pVar);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - LONG
//

class CVariableLongArray : public CVariableDataArray
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableLongArray(T5PTR_DBMAP pVar, T5_PTR pArr, UINT uSize);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);

	protected:
		// Data
		T5_PTLONG	m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - REAL
//

class CVariableReal : public CVariableData
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableReal(T5PTR_DBMAP pVar);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);

	protected:
		// Data
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - REAL Array
//

class CVariableRealArray : public CVariableDataArray
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableRealArray(T5PTR_DBMAP pVar, T5_PTR pData, UINT uSize);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);

	protected:
		// Data
		T5_PTREAL	m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - TIME
//

class CVariableTime : public CVariableData
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableTime(T5PTR_DBMAP pVar);

		// Operations
		void	GetData(void);
		void	SetData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - TIME
//

class CVariableTimeArray : public CVariableDataArray
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableTimeArray(T5PTR_DBMAP pVar, T5_PTR pArr, UINT uSize);

		// Operations
		void	GetData(void);
		void	SetData(void);

	protected:
		// Data
		T5_PTDWORD	m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - String
//

class CVariableString : public CVariableData
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableString(T5PTR_DBMAP pVar);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);

	protected:
		// Data
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - String Array
//

class CVariableStringArray : public CVariableDataArray
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableStringArray(T5PTR_DBMAP pVar, T5_PTR pArr, UINT uSize);

		// Operations
		void	GetData(void);
		void	SetData(void);

		// Diagnostic
		void ShowData(IDiagOutput *pOut);

	protected:
		// String
		#pragma pack(1)

		struct CStringHead {
			
			BYTE	m_bStride;
			BYTE	m_bLength;
			};

		struct CStringInfo {
			
			CStringHead	h;
			char		s[0];
			};

		#pragma pack()

		// Data
		T5_PTBYTE	m_pData;
		UINT		m_uPitch;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - Complex
//

class CVariableComplex : public CVariableData
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableComplex(T5PTR_DBMAP pVar);
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - Complex Array
//

class CVariableComplexArray : public CVariableData
{
	public:
		// Instantiator
		static CVariableData * Create(CVariableMap &Map, T5PTR_DBMAP pVar);

		// Constructor
		CVariableComplexArray(T5PTR_DBMAP pVar);
	};

// End of File

#endif
