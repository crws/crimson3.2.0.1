
#include "intern.hpp"

#include "vardata.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Profile Manager
//

// Pointer

global CProfileManager * g_pPM	= NULL;

// Instantiator

global void Create_ProfileManager(T5PTR_DB pDbase)
{
	New CProfileManager(pDbase, "Crimson");
	}

// Constructor

CProfileManager::CProfileManager(T5PTR_DB pDbase, PCTXT pName)
{
	StdSetRef();

	g_pPM    = this;

	m_pDiag  = NULL;
	
	m_uCount = 0;

	m_ppData = NULL;

	m_pDiag  = NULL;

	Load(pDbase, pName);

	AddDiagCmds();
	}

// Destructor

CProfileManager::~CProfileManager(void)
{
	if( m_pDiag ) {

		m_pDiag->RevokeProvider(m_uProv);

		m_pDiag->Release();

		m_pDiag = NULL;
		}

	Kill();
	}

// Management

void CProfileManager::Load(T5PTR_DB pDbase, PCTXT pName)
{
	m_Name = pName;

	m_VarMap.Bind(pDbase);

	Count();

	Alloc();

	Build();
	}

void CProfileManager::Kill(void)
{
	while( m_uCount-- ) {

		if( m_ppData[m_uCount] ) {
		
			delete m_ppData[m_uCount];
			}
		}

	if( m_ppData ) {

		Free(m_ppData);

		m_ppData = NULL;
		}
	}

// Operations

void CProfileManager::GetData(void)
{
	for( UINT n = 0; n < m_uCount; n ++ ) {

		CVariableData *pVar = m_ppData[n];

		if( pVar->IsAvail() ) {

			pVar->GetData();
			}
		}
	}

void CProfileManager::SetData(void)
{
	for( UINT n = 0; n < m_uCount; n ++ ) {

		CVariableData *pVar = m_ppData[n];

		if( pVar->IsAvail() ) {

			if( !pVar->IsReadOnly() ) {

				pVar->SetData();
				}
			}
		}
	}

// IUnknown

HRESULT CProfileManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagProvider);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CProfileManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CProfileManager::Release(void)
{
	StdRelease();
	}

// Implementation

void CProfileManager::Count(void)
{
	T5PTR_DBMAP pVar = m_VarMap.GetFirst();	

	for( m_uCount = 0; pVar; ) {

		CVariable Var(pVar);

		if( Var.IsAllowed() ) {

			if( Var.IsProfile(m_Name) ) {
				
				m_uCount ++;
				}
			}

		pVar = m_VarMap.GetNext();
		}
	}

void CProfileManager::Alloc(void)
{
	UINT uSize = m_uCount * sizeof(CVariableData *);

	m_ppData   = (CVariableData **) Malloc(uSize);

	memset(m_ppData, 0x00, uSize);
	}

void CProfileManager::Build(void)
{
	if( m_uCount ) {

		T5PTR_DBMAP pVar = m_VarMap.GetFirst();

		for( UINT n = 0; pVar; ) {

			CVariable Var(pVar);

			if( Var.IsAllowed() ) {

				if( Var.IsProfile(m_Name) ) {

					CVariableData *pData = Create(pVar);

					m_ppData[n ++] = pData;
					}
				}

			pVar = m_VarMap.GetNext();
			}
		}
	}

CVariableData * CProfileManager::Create(T5PTR_DBMAP pVar)
{
	T5_WORD c, t;

	BOOL   fArray = m_VarMap.IsArrayOfSingleVar(pVar, c, t) != NULL;

	if( fArray ) {

		T5_WORD wType = CVariable(pVar).GetType();

		switch( wType & ~T5C_EXTERN ) {
				
			case T5C_BOOL:		
				return CVariableBoolArray::Create(m_VarMap, pVar);

			case T5C_SINT:
			case T5C_USINT:
				return CVariableByteArray::Create(m_VarMap, pVar);	

			case T5C_INT:
			case T5C_UINT:
				return CVariableWordArray::Create(m_VarMap, pVar);

			case T5C_DINT:
			case T5C_UDINT:		
				return CVariableLongArray::Create(m_VarMap, pVar);

			case T5C_REAL:
				return CVariableRealArray::Create(m_VarMap, pVar);

			case T5C_TIME:
				return CVariableTimeArray::Create(m_VarMap, pVar);

			case T5C_STRING:
				return CVariableStringArray::Create(m_VarMap, pVar);

			case T5C_COMPLEX:
				return CVariableComplexArray::Create(m_VarMap, pVar);

			default:
				return NULL;
			}
		}
	else {
		T5_WORD wType = CVariable(pVar).GetType();

		switch( wType & ~T5C_EXTERN ) {
				
			case T5C_BOOL:		
				return CVariableBool::Create(m_VarMap, pVar);

			case T5C_SINT:
			case T5C_USINT:
				return CVariableByte::Create(m_VarMap, pVar);	

			case T5C_INT:
			case T5C_UINT:
				return CVariableWord::Create(m_VarMap, pVar);

			case T5C_DINT:
			case T5C_UDINT:		
				return CVariableLong::Create(m_VarMap, pVar);

			case T5C_REAL:
				return CVariableReal::Create(m_VarMap, pVar);

			case T5C_TIME:
				return CVariableTime::Create(m_VarMap, pVar);

			case T5C_STRING:
				return CVariableString::Create(m_VarMap, pVar);

			case T5C_COMPLEX:
				return CVariableComplex::Create(m_VarMap, pVar);

			default:
				return NULL;
			}
		}
	}

// End of File
