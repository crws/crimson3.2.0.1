
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Trace
//

// Prototypes

static	void	SLOW	TraceArgs(PCTXT pText, va_list pArgs);

// Code

clink void Trace(const char *pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	AfxTraceArgs(pText, pArgs);

	va_end(pArgs);
	}

static	void	TraceArgs(PCTXT pText, va_list pArgs)
{
	char sText[512];

	VSPrintf(sText, pText, pArgs);

	AfxTrace("<<<");

	AfxTrace(sText);

	AfxTrace(">>>");

	/*CStratonSystem::m_pThis->m_pES->Trace(sText);*/
	}

// End of File
