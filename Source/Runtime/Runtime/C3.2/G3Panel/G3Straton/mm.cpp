
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Memory Manager
//

// Pointer

global CMemoryManager * g_pMM	= NULL;

// Instantiator

global void Create_MemoryManager(void)
{
	New CMemoryManager;
	}

// Constructor

CMemoryManager::CMemoryManager(void)
{
	g_pMM   = this;

	m_pData = NULL;

	Open();
	}

// Operations

void CMemoryManager::Open(void)
{
	m_pData = New T5STR_MM;

	T5MM_Open(m_pData, NULL);
	}

void CMemoryManager::Close(void)
{
	T5MM_Close(m_pData);

	delete m_pData;
	}

BOOL CMemoryManager::AllocRawVMDB(DWORD dwSize)
{	
	return T5MM_AllocRawVMDB(m_pData, dwSize) == T5RET_OK;
	}

PVOID CMemoryManager::LinkVMDB(DWORD dwSize)
{	
	return T5MM_LinkVMDB(m_pData);
	}

void	CMemoryManager::UnlinkVMDB(void)
{
	T5MM_UnlinkVMDB(m_pData);
	}

void	CMemoryManager::SaveVMDB(PCTXT pName)
{
	T5MM_SaveVMDB(m_pData, T5_PTCHAR(pName));
	}

BOOL	CMemoryManager::FreeVMDB(void)
{
	return T5MM_FreeVMDB(m_pData) == T5RET_OK;
	}

BOOL CMemoryManager::LoadCode(void)
{
	return T5MM_LoadCode(m_pData, "t5") == T5RET_OK;
	}

void CMemoryManager::FreeCode(void)
{
	T5MM_FreeCode(m_pData);
	}

PVOID CMemoryManager::LinkCode(void)
{
	return T5MM_LinkCode(m_pData);
	}

void CMemoryManager::UnlinkCode(void)
{
	T5MM_UnlinkCode(m_pData);
	}

T5_RET	CMemoryManager::WriteCodeRequest(T5_WORD wRequest, T5PTR_CSLOAD pLoad)
{
	return T5MM_WriteCodeRequest(m_pData, wRequest, pLoad);
	}

T5_RET	CMemoryManager::WriteSyncRequest(T5_WORD wRequest, T5PTR_CSLOAD pLoad)
{
	return T5MM_WriteSyncRequest(m_pData, wRequest, pLoad);
	}

// End of File
