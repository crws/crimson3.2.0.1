
#include "intern.hpp"

#include "vm\t5vm.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Options

#define USE_LOCKED_ITEM	1

// Data

static	PCBYTE	m_pFile	= NULL;

static	UINT	m_uSize	= 0;

// Prototpes

global	BOOL	SaveFile(PCBYTE pData, UINT uSize);
static	T5_RET	LoadCode(T5PTR_MMB mmb);
static	T5_RET	LoadVMDB(T5PTR_MMB mmb);
static	BOOL	LoadFile(PBYTE &pData, UINT &uSize);
static	BOOL	LoadFromCard(PCTXT pPath, PBYTE pData, UINT &uSize);
static	BOOL	SaveToCard(PCTXT pPath, PCBYTE &pData, UINT &uSize);

// Code

global	BOOL	SaveFile(PCBYTE pData, UINT uSize)
{
	#ifdef USE_LOCKED_ITEM

	m_pFile = pData;

	m_uSize = uSize;

	::Dump("executable from locked databse item", PBYTE(m_pFile), 256);

	return TRUE;

	#else

	PCTXT pPath = "\\straton\\appli.xti";

	return SaveToCard(pPath, pData, uSize);

	#endif
	}

clink	T5_RET	memoryAlloc(T5PTR_MMB mmb, T5_DWORD dwSize, T5_PTR pConf)
{
	if( mmb->wFlags == 0 ) {

		PBYTE pData = PBYTE(Malloc(dwSize));

		if( pData ) {

			memset(pData, 0, dwSize);

			mmb->mem	= pData;

			mmb->wFlags	= T5MMB_LOADED;

			mmb->wNbLink	= 0;

			mmb->dwSize	= dwSize;

			mmb->pData	= NULL;
			
			return T5RET_OK;
			}
		}

	return T5RET_ERROR;
	}

clink	T5_RET	memoryLoad(T5PTR_MMB mmb, T5_PTCHAR szAppName, T5_PTR pConf)
{
	if( mmb->wFlags == 0 ) {

		switch( mmb->wID ) {

			case T5MM_CODE:

				return LoadCode(mmb);

			case T5MM_VMDB:

				return LoadVMDB(mmb);

			case T5MM_CHANGE:

				break;

			default:

				AfxTrace("memory type (%d) not supported\n", mmb->wID);

				HostBreak();

				return T5RET_ERROR;
			}		
		}

	return T5RET_ERROR;
	}

clink	T5_RET	 memoryFree(T5PTR_MMB mmb, T5_PTR pConf)
{
	if( mmb->wFlags == T5MMB_LOADED ) {

		#if !defined USE_LOCKED_ITEM

		Free(mmb->mem);

		#endif

		mmb->mem	= NULL;

		mmb->wFlags	= 0;

		mmb->wNbLink	= 0;

		mmb->dwSize	= 0;

		mmb->pData	= NULL;

		return T5RET_OK;
		}

	return T5RET_ERROR;
	}

clink	T5_RET	 memoryLink(T5PTR_MMB mmb, T5_PTR pConf)
{
	if( mmb->wFlags & T5MMB_LOADED ) {

		mmb->wNbLink += 1;

		mmb->wFlags  |= T5MMB_LINKED;

		mmb->pData    = mmb->mem;
		
		return T5RET_OK;
		}

	return T5RET_ERROR;
	}

clink	T5_RET	 memoryUnlink(T5PTR_MMB mmb, T5_PTR pConf)
{
	if( mmb->wFlags & T5MMB_LOADED ) {
		
		if( mmb->wFlags & T5MMB_LINKED ) {

			mmb->wNbLink -= 1;

			if( mmb->wNbLink == 0 ) {

				mmb->wFlags &= ~T5MMB_LINKED;
				
				mmb->pData   = NULL;
				}
			
			return T5RET_OK;
			}			
		}

	return T5RET_ERROR;
	}

static	T5_RET	LoadCode(T5PTR_MMB mmb)
{
	UINT uSize;

	PBYTE pData;

	if( LoadFile(pData, uSize) ) {

		mmb->mem     = pData;

		mmb->dwSize  = uSize;

		mmb->pData   = NULL;
			 
		mmb->wNbLink = 0;

		mmb->wFlags  = T5MMB_LOADED;

		return T5RET_OK;
		}
	else {
		mmb->mem    = NULL;
			
		mmb->dwSize = 0;

		mmb->pData  = NULL;

		mmb->wNbLink = 0;

		mmb->wFlags = 0;
		
		return T5RET_OK;
		}

	return T5RET_ERROR;
	}

static	T5_RET	LoadVMDB(T5PTR_MMB mmb)
{
	return T5RET_ERROR;
	}

static	BOOL	LoadFile(PBYTE &pData, UINT &uSize)
{
	#ifdef USE_LOCKED_ITEM

	pData = PBYTE(m_pFile);

	uSize = m_uSize;

	return TRUE;

	#else

	PCTXT pPath = "\\straton\\appli.xti";

	pData = NULL;

	uSize = 0;

	if( LoadFromCard(pPath, NULL, uSize) ) {

		pData = PBYTE(Malloc(uSize));

		if( LoadFromCard(pPath, pData, uSize) ) {

			::Dump("loaded form card", pData, 256);
			
			return TRUE;
			}

		Free(pData);

		pData = NULL;
		}

	return FALSE;

	#endif
	}

static BOOL LoadFromCard(PCTXT pPath, PBYTE pData, UINT &uSize)
{
	CAutoGuard Guard;

	CAutoFile  File(pPath, "r");

	if( File ) { 

		if( pData ) {

			if( File.Read(pData, uSize) ) {
			
				return TRUE;
				}
			}
		else {
			uSize = File.GetSize();
			
			return TRUE;
			}
		}

	return FALSE;
	}

static BOOL SaveToCard(PCTXT pPath, PCBYTE &pData, UINT &uSize)
{
	CAutoGuard Guard;

	CAutoFile  File(pPath, "r+", "w+");

	if( File ) {

		UINT uDone = File.Write(PBYTE(pData), uSize);

		File.Truncate();

		return uDone == uSize;
		}

	return FALSE;
	}

// End of File
