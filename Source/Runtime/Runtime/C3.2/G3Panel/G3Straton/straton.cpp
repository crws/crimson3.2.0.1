
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Control Manager
//

// Constants

#define	timeLicense (10 * 1000)

// Hooks

global BOOL Straton_Service(PCBYTE pIn, PBYTE pOut)
{
	if( g_pControl ) {

		return g_pControl->Service(pIn, pOut);
		}

	return FALSE;
	}

// Instantiator

global IStratonControl * Create_StratonControl(void)
{
	return New CControlItem;
	}

// Constructor

CControlItem::CControlItem(void)
{
	g_pControl = this;

	m_pProject = New CControlProject;

	m_fValid   = FALSE;

	m_fUseVM   = FALSE;

	m_pMutex   = NULL;
	}

// Destructor

CControlItem::~CControlItem(void)
{
	delete m_pProject;

	g_pControl = NULL;
	}

// IBase

UINT CControlItem::Release(void)
{
	delete this;

	return 0;
	}

// IStratonControl

void CControlItem::LoadControl(PCBYTE &pData)
{
	Load(pData);
	}

void CControlItem::GetTaskList(CTaskList &List)
{
	if( m_fValid ) {

		if( m_pProject->HasLicense() || m_pProject->HasHooks() ) {

			if( m_pProject->m_uPoll ) {

				if( m_pProject->IsNonEmpty() ) {

					m_fUseVM = TRUE;
					}

				if( TRUE ) {

					CTaskDef Task;

					Task.m_Name   = "MAIN";
					Task.m_pEntry = this;
					Task.m_uID    = 0;
					Task.m_uCount = 1;
					Task.m_uLevel = 7500;
					Task.m_uStack = 4 * 1024;

					List.Append(Task);
					}

				if( TRUE ) {

					CTaskDef Task;

					Task.m_Name   = "VIRT";
					Task.m_pEntry = this;
					Task.m_uID    = 1;
					Task.m_uCount = 1;
					Task.m_uLevel = 8500;
					Task.m_uStack = 4 * 1024;

					List.Append(Task);
					}
				}

			return;
			}
		}
	}

BOOL CControlItem::IsRunning(void)
{
	return m_fValid && m_pProject->IsNonEmpty();
	}

BOOL CControlItem::Service(PCBYTE pIn, PBYTE pOut)
{
	if( g_pVM ) {

		if( m_fUseVM ) {

			m_pMutex->Wait(FOREVER);

			g_pVM->Service(pIn, pOut);

			m_pMutex->Free();

			return TRUE;
			}
		}

	return FALSE;
	}

// Initialization

void CControlItem::Load(PCBYTE &pData)
{
	ValidateLoad("CControlItem", pData);

	if( GetByte(pData) ) {

		m_pProject->Load(pData);

		m_fValid = TRUE;
		}
	}

// Task Entries

void CControlItem::TaskInit(UINT uID)
{
	switch( uID ) {
		
		case 0:	MainInit();	break;
		case 1:	VirtInit();	break;
		}
	}

void CControlItem::TaskExec(UINT uID)
{
	switch( uID ) {
		
		case 0:	MainExec();	break;
		case 1:	VirtExec();	break;
		}
	}

void CControlItem::TaskStop(UINT uID)
{
	}

void CControlItem::TaskTerm(UINT uID)
{
	switch( uID ) {
		
		case 0:	MainTerm();	break;
		case 1:	VirtTerm();	break;
		}
	}

// Entry Points

void CControlItem::MainInit(void)
{
	}

void CControlItem::MainExec(void)
{
	for(;;) Sleep(FOREVER);
	}

void CControlItem::MainTerm(void)
{
	}

void CControlItem::VirtInit(void)
{
	m_pMutex = Create_Mutex();

	m_pMutex->Wait(FOREVER);

	m_pProject->SetScan();

	if( m_fUseVM ) {

		Create_MemoryManager();

		Create_VirtualMachine(m_pProject);

		g_pVM->Init();
		}

	m_pMutex->Free();
	}

void CControlItem::VirtExec(void)
{
	BOOL fCheck = FALSE;

	UINT uCycle = m_pProject->m_uScan;

	UINT uLast1 = GetTickCount();

	UINT uLast2 = uLast1;

	for(;;) {
		
		// MikeG -- The pattern here is important. You can't pick a time in the
		// future by adding a constant to GetTickCount, as it is possible that
		// the value will wrap around in the extreme future. (Okay, it's not
		// that possible, but it's still good coding.) Instead, you subtract the
		// last time from the current time, and even if this wraps around, the
		// unsigned math will give you the right answer for how long has passed.

		UINT uTime = GetTickCount();

		if( uTime - uLast1 >= ToTicks(timeLicense) ) {

			if( fCheck && m_fUseVM && !m_pProject->HasLicense() ) {

				break;
				}

			uLast1 = uTime;
			}

		if( uTime - uLast2 >= ToTicks(uCycle) ) {

			SetTaskLimit(10, 10);
			
			m_pMutex->Wait(FOREVER);

			if( m_pProject->HasHooks() ) {

				m_pProject->RunHooks();
				}

			if( m_fUseVM ) {
	
				g_pVM->Poll();
				}

			m_pMutex->Free();
	
			SetTaskLimit(0, 0);

			uLast2 = uTime;
			}

		// We sleep for the shorter of the two remaining times.

		INT e1 = timeLicense - ToTime(uTime - uLast1);

		INT e2 = uCycle      - ToTime(uTime - uLast2);

		INT e3 = min(e1, e2);

		if( e3 > 0 ) {

			Sleep(e3);
			}
		}

	for(;;) Sleep(FOREVER);
	}

void CControlItem::VirtTerm(void)
{
	if( m_fUseVM ) {

		g_pVM->Term();

		delete g_pVM;

		g_pVM = NULL;
		}

	m_pMutex->Release();
	}

// End of File
