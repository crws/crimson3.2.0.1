
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// IDiagProvider

UINT CProfileManager::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case diagStatus: return DiagStatus(pOut, pCmd);
		case diagVars:	 return DiagVars  (pOut, pCmd);
		case diagData:	 return DiagData  (pOut, pCmd);
		}

	return 0;
	}

// Diagnostics

BOOL CProfileManager::AddDiagCmds(void)
{
	#if defined(_DEBUG)	

	AfxGetObject("diagmanager", 0, IDiagManager, m_pDiag);

	if( m_pDiag ) {

		m_uProv = m_pDiag->RegisterProvider(this, "pm");

		m_pDiag->RegisterCommand(m_uProv, CProfileManager::diagStatus, "status");
		m_pDiag->RegisterCommand(m_uProv, CProfileManager::diagVars,   "vars"  );
		m_pDiag->RegisterCommand(m_uProv, CProfileManager::diagData,   "data"  );

		return TRUE;
		}

	#endif

	return FALSE;
	}

// Commands

UINT CProfileManager::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		pOut->AddProp("State",  "%u", m_uCount);

		pOut->EndPropList();
		
		return 0;
		}

	#endif	

	pOut->Error(NULL);

	return 1;
	}

UINT CProfileManager::DiagVars(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(6);

		pOut->SetColumn(0, "Symbol",  "%s");
		pOut->SetColumn(1, "Idx",     "%d");
		pOut->SetColumn(2, "Num",     "%d");
		pOut->SetColumn(3, "Type",    "%d");
		pOut->SetColumn(4, "Profile", "%s");
		pOut->SetColumn(5, "Props",   "%s");

		pOut->AddHead();

		pOut->AddRule('-');

		T5PTR_DBMAP i = m_VarMap.GetFirst();

		for( ; i; i = m_VarMap.GetNext() ) {

			pOut->AddRow();

			CVariable Var(i);

			pOut->SetData(0, Var.GetSymbol());
			pOut->SetData(1, Var.GetIndex());
			pOut->SetData(2, Var.GetNumTag());
			pOut->SetData(3, Var.GetType());
			pOut->SetData(4, PCTXT(Var.GetProfileName()));
			pOut->SetData(5, PCTXT(Var.GetPropAsText()));

			pOut->EndRow();
			}

		pOut->EndTable();
		
		return 0;
		}

	#endif	

	pOut->Error(NULL);

	return 1;
	}

UINT CProfileManager::DiagData(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {
		
		pOut->AddTable(4);

		pOut->SetColumn(0, "Symbol",    "%s");
		pOut->SetColumn(1, "Index",     "%d");
		pOut->SetColumn(2, "Value",     "%s");
		pOut->SetColumn(3, "Array",     "%d");

		pOut->AddHead();

		pOut->AddRule('-');

		T5PTR_DBMAP i = m_VarMap.GetFirst();

		for(; i; i = m_VarMap.GetNext()) {

			pOut->AddRow();

			CVariable Var(i);

			pOut->SetData(0, Var.GetSymbol());
			pOut->SetData(1, Var.GetIndex());
			pOut->SetData(2, PCTXT(Var.GetDataAsText()));

			pOut->SetData(3, Var.IsArray());

			pOut->EndRow();
			}

		pOut->EndTable();
		
		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

// End of File

