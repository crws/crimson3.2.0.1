
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#include "vardata.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item
//

// Constructor

CVariableData::CVariableData(T5PTR_DBMAP pVar)
{
	m_pVar      = pVar;

	CVariable Var(m_pVar);

	m_Profile = *((CProfile *) Var.GetProps());	

	m_pServer = CCommsSystem::m_pThis->GetDataServer();

	m_pServer->SetScan(m_Profile.m_dwSrc, scanTrue);

	//
	m_fReadOnly = !!(m_Profile.m_dwAcc & 1);
	}

// Destructor

CVariableData::~CVariableData(void)
{
	}

// Operations

void CVariableData::GetData(void)
{
	}

void CVariableData::SetData(void)
{
	}

// Attributes

BOOL CVariableData::IsReadOnly(void)
{
	return m_fReadOnly;
	}

// Diagnostic

void CVariableData::ShowData(IDiagOutput *pOut)
{	
	}

// Access

BOOL CVariableData::IsAvail(void)
{
	return m_pServer->IsAvail(m_Profile.m_dwSrc);
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - Array
//

CVariableDataArray::CVariableDataArray(T5PTR_DBMAP pVar, UINT uSize) : CVariableData(pVar)
{
	m_uSize = uSize;
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - BOOL
//

// Instantiator

CVariableData * CVariableBool::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	return New CVariableBool(pVar);
	}

// Constructor

CVariableBool::CVariableBool(T5PTR_DBMAP pVar) : CVariableData(pVar)
{
	}

// Operations

void CVariableBool::GetData(void)
{
	CVariable Var(m_pVar);

	T5_LONG Data = m_pServer->GetData( m_Profile.m_dwSrc, 
					   typeVoid, 
					   getNone
					   );

	Var.SetLongValue(Data);
	}

void CVariableBool::SetData(void)
{
	CVariable Var(m_pVar);

	T5_LONG Data = Var.GetLongValue();

	m_pServer->SetData( m_Profile.m_dwSrc, 
			    typeVoid, 
			    setDirect, 
			    Data
			    );
	}

// Diagnostic

void CVariableBool::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - BOOL Array
//

// Instantiator

CVariableData * CVariableBoolArray::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	T5_WORD c, t;

	T5_PTR pData = Map.IsArrayOfSingleVar(pVar, c, t);

	return New CVariableBoolArray(pVar, pData, c);
	}

// Constructor

CVariableBoolArray::CVariableBoolArray(T5PTR_DBMAP pVar, T5_PTR pData, UINT uSize) : CVariableDataArray(pVar, uSize)
{
	m_pData = T5_PTBOOL(pData);
	}

// Operations

void CVariableBoolArray::GetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		T5_BOOL  Data = m_pServer->GetData( Src.m_Ref, 
						    typeVoid, 
						    setDirect 
						    );

		m_pData[n] = Data;

		Src.t.m_Array ++;
		}
	}

void CVariableBoolArray::SetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {		

		T5_BOOL  Data = m_pData[n];

		m_pServer->SetData( Src.m_Ref, 
				    typeVoid, 
				    setDirect, 
				    Data
				    );

		Src.t.m_Array ++;
		}
	}

// Diagnostic

void CVariableBoolArray::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - BYTE
//

// Instantiator

CVariableData * CVariableByte::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	return New CVariableByte(pVar);
	}

// Constructor

CVariableByte::CVariableByte(T5PTR_DBMAP pVar) : CVariableData(pVar)
{
	}

// Operations

void CVariableByte::GetData(void)
{
	CVariable Var(m_pVar);

	T5_BYTE Data = m_pServer->GetData( m_Profile.m_dwSrc, 
					   typeVoid, 
					   getNone
					   );

	Var.SetLongValue(Data);
	}

void CVariableByte::SetData(void)
{
	CVariable Var(m_pVar);

	T5_BYTE Data = Var.GetLongValue();
	
	m_pServer->SetData( m_Profile.m_dwSrc, 
			    typeVoid, 
			    setDirect, 
			    Data
			    );
	}

// Diagnostic

void CVariableByte::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - BYTE Array
//

// Instantiator

CVariableData * CVariableByteArray::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	T5_WORD c, t;

	T5_PTR pData = Map.IsArrayOfSingleVar(pVar, c, t);

	return New CVariableByteArray(pVar, pData, c);
	}

// Constructor

CVariableByteArray::CVariableByteArray(T5PTR_DBMAP pVar, T5_PTR pData, UINT uSize) : CVariableDataArray(pVar, uSize)
{
	m_pData = T5_PTBYTE(pData);
	}

// Operations

void CVariableByteArray::GetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		T5_BYTE  Data = m_pServer->GetData( Src.m_Ref, 
						    typeVoid, 
						    setDirect 
						    );

		m_pData[n] = Data;

		Src.t.m_Array ++;
		}
	}

void CVariableByteArray::SetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		T5_BYTE  Data = m_pData[n];

		m_pServer->SetData( Src.m_Ref, 
				    typeVoid, 
				    setDirect, 
				    Data
				    );

		Src.t.m_Array ++;
		}
	}

// Diagnostic

void CVariableByteArray::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - WORD
//

// Instantiator

CVariableData * CVariableWord::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	return New CVariableWord(pVar);
	}

// Constructor

CVariableWord::CVariableWord(T5PTR_DBMAP pVar) : CVariableData(pVar)
{
	}

// Operations

void CVariableWord::GetData(void)
{
	CVariable Var(m_pVar);

	T5_WORD Data = m_pServer->GetData( m_Profile.m_dwSrc, 
					 typeVoid, 
					 getNone
					 );

	Var.SetLongValue(Data);
	}

void CVariableWord::SetData(void)
{
	CVariable Var(m_pVar);

	T5_WORD Data = Var.GetLongValue();
	
	m_pServer->SetData( m_Profile.m_dwSrc, 
			  typeVoid, 
			  setDirect, 
			  Data
			  );
	}

// Diagnostic

void CVariableWord::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - WORD Array
//

// Instantiator

CVariableData * CVariableWordArray::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	T5_WORD c, t;

	T5_PTR pData = Map.IsArrayOfSingleVar(pVar, c, t);

	return New CVariableWordArray(pVar, pData, c);
	}

// Constructor

CVariableWordArray::CVariableWordArray(T5PTR_DBMAP pVar, T5_PTR pData, UINT uSize) : CVariableDataArray(pVar, uSize)
{
	m_pData = T5_PTWORD(pData);
	}

// Operations

void CVariableWordArray::GetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		T5_WORD  Data = m_pServer->GetData( Src.m_Ref, 
						    typeVoid, 
						    setDirect 
						    );

		m_pData[n] = Data;

		Src.t.m_Array ++;
		}
	}

void CVariableWordArray::SetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		T5_WORD  Data = m_pData[n];

		m_pServer->SetData( Src.m_Ref, 
				    typeVoid, 
				    setDirect, 
				    Data
				    );

		Src.t.m_Array ++;
		}
	}

// Diagnostic

void CVariableWordArray::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - LONG
//

// Instantiator

CVariableData * CVariableLong::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	return New CVariableLong(pVar);
	}

// Constructor

CVariableLong::CVariableLong(T5PTR_DBMAP pVar) : CVariableData(pVar)
{
	}

// Operations

void CVariableLong::GetData(void)
{
	CVariable Var(m_pVar);

	T5_LONG Data = m_pServer->GetData( m_Profile.m_dwSrc, 
					   typeInteger, 
					   getNone
					   );

	Var.SetLongValue(Data);
	}

void CVariableLong::SetData(void)
{
	CVariable Var(m_pVar);

	T5_LONG Data = Var.GetLongValue();

	m_pServer->SetData( m_Profile.m_dwSrc, 
				typeInteger, 
				setDirect, 
				Data
				);
	}

// Diagnostic

void CVariableLong::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - LONG Array
//

// Instantiator

CVariableData * CVariableLongArray::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	T5_WORD c, t;

	T5_PTR pData = Map.IsArrayOfSingleVar(pVar, c, t);

	return New CVariableLongArray(pVar, pData, c);
	}

// Constructor

CVariableLongArray::CVariableLongArray(T5PTR_DBMAP pVar, T5_PTR pData, UINT uSize) : CVariableDataArray(pVar, uSize)
{
	m_pData = T5_PTLONG(pData);
	}

// Operations

void CVariableLongArray::GetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		T5_LONG  Data = m_pServer->GetData( Src.m_Ref, 
						    typeVoid, 
						    setDirect 
						    );

		m_pData[n] = Data;

		Src.t.m_Array ++;
		}
	}

void CVariableLongArray::SetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		T5_LONG  Data = m_pData[n];

		m_pServer->SetData( Src.m_Ref, 
				    typeVoid, 
				    setDirect, 
				    Data
				    );

		Src.t.m_Array ++;
		}
	}

// Diagnostic

void CVariableLongArray::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - REAL
//

// Instantiator

CVariableData * CVariableReal::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	return New CVariableReal(pVar);
	}

// Constructor

CVariableReal::CVariableReal(T5PTR_DBMAP pVar) : CVariableData(pVar)
{
	}

// Operations

void CVariableReal::GetData(void)
{
	CVariable Var(m_pVar);

	DWORD Data = m_pServer->GetData( m_Profile.m_dwSrc, 
					 typeReal, 
					 getNone
					 );

	Var.SetRealValue(I2R(Data));
	}

void CVariableReal::SetData(void)
{
	CVariable Var(m_pVar);

	T5_REAL Data = Var.GetRealValue();
	
	m_pServer->SetData( m_Profile.m_dwSrc, 
			    typeReal, 
			    setDirect, 
			    R2I(Data)
			    );
	}

// Diagnostic

void CVariableReal::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - REAL Array
//

// Instantiator

CVariableData * CVariableRealArray::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	T5_WORD c, t;

	T5_PTR pData = Map.IsArrayOfSingleVar(pVar, c, t);

	return New CVariableRealArray(pVar, pData, c);
	}

// Constructor

CVariableRealArray::CVariableRealArray(T5PTR_DBMAP pVar, T5_PTR pData, UINT uSize) : CVariableDataArray(pVar, uSize)
{
	m_pData = T5_PTREAL(pData);
	}

// Operations

void CVariableRealArray::GetData(void)
{
	CDataRef  Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		DWORD Data = m_pServer->GetData( Src.m_Ref, 
						 typeVoid, 
						 setDirect 
						 );

		m_pData[n] = I2R(Data);

		Src.t.m_Array ++;
		}
	}

void CVariableRealArray::SetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {		

		T5_REAL  Data = m_pData[n];

		m_pServer->SetData( Src.m_Ref, 
				    typeVoid, 
				    setDirect, 
				    R2I(Data)
				    );

		Src.t.m_Array ++;
		}
	}

// Diagnostic

void CVariableRealArray::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - TIME
//

// Instantiator

CVariableData * CVariableTime::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	return New CVariableTime(pVar);
	}

// Constructor

CVariableTime::CVariableTime(T5PTR_DBMAP pVar) : CVariableData(pVar)
{
	}

// Operations

void CVariableTime::GetData(void)
{
	CVariable Var(m_pVar);

	T5_DWORD Data = m_pServer->GetData( m_Profile.m_dwSrc, 
					   typeVoid, 
					   getNone
					   );

	Var.SetDWordValue(Data);
	}

void CVariableTime::SetData(void)
{
	CVariable Var(m_pVar);

	T5_DWORD Data = Var.GetDWordValue();

	m_pServer->SetData( m_Profile.m_dwSrc, 
			    typeVoid, 
			    setDirect, 
			    Data
			    );
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - TIME - Array
//

// Instantiator

CVariableData * CVariableTimeArray::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	T5_WORD c, t;

	T5_PTR pData = Map.IsArrayOfSingleVar(pVar, c, t);

	return New CVariableTimeArray(pVar, pData, c);
	}

// Constructor

CVariableTimeArray::CVariableTimeArray(T5PTR_DBMAP pVar, T5_PTR pArr, UINT uSize) : CVariableDataArray(pVar, uSize)
{
	m_pData = T5_PTDWORD(pArr);
	}

// Operations

void CVariableTimeArray::GetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		T5_DWORD  Data = m_pServer->GetData( Src.m_Ref, 
						     typeVoid, 
						     setDirect 
						     );

		m_pData[n] = Data;

		Src.t.m_Array ++;
		}
	}

void CVariableTimeArray::SetData(void)
{
	CDataRef Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		T5_DWORD  Data = m_pData[n];

		m_pServer->SetData( Src.m_Ref, 
				    typeVoid, 
				    setDirect, 
				    Data
				    );

		Src.t.m_Array ++;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - STRING
//

// Instantiator

CVariableData * CVariableString::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	return New CVariableString(pVar);
	}

// Constructor

CVariableString::CVariableString(T5PTR_DBMAP pVar) : CVariableData(pVar)
{
	}

// Operations

void CVariableString::GetData(void)
{
	CVariable Var(m_pVar);

	PUTF  pData = PUTF(m_pServer->GetData( m_Profile.m_dwSrc, 
					       typeString, 
					       getNone
					       ));

	CUnicode Text(pData);

	Free(pData);

	UINT   uLen = Text.GetLength();

	Var.SetStringValue(T5_PTBYTE(PCTXT(UniConvert(Text))), uLen);
	}

void CVariableString::SetData(void)
{
	CVariable Var(m_pVar);

	T5_BYTE bLen;

	PCTXT pData = PCTXT(Var.GetStringValue(bLen));

	CString Text(pData);

	m_pServer->SetData( m_Profile.m_dwSrc,
			    typeString,
			    setDirect,
			    DWORD(wstrdup(UniConvert(Text)))
			    );
	}

// Diagnostic

void CVariableString::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - STRING Array
//

// Instantiator

CVariableData * CVariableStringArray::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	T5_WORD c, t;

	T5_PTR pData = Map.IsArrayOfSingleVar(pVar, c, t);

	return New CVariableStringArray(pVar, pData, c);
	}

// Constructor

CVariableStringArray::CVariableStringArray(T5PTR_DBMAP pVar, T5_PTR pArr, UINT uSize) : CVariableDataArray(pVar, uSize)
{
	CVariable Var(m_pVar);

	m_pData   = T5_PTBYTE(pArr);

	m_uPitch  = Var.GetStringLength() + sizeof(CStringHead) + 1;
	}

// Operations

void CVariableStringArray::GetData(void)
{
	CDataRef  Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		// read

		PUTF  pData = PUTF(m_pServer->GetData( Src.m_Ref, 
						       typeString, 
						       getNone
						       ));

		// make

		CUnicode Text(pData);

		Free(pData);

		// save

		CStringInfo &String = (CStringInfo &) *(m_pData + Src.t.m_Array * m_uPitch);

		String.h.m_bLength  = BYTE(Text.GetLength());

		MakeMin(String.h.m_bLength, String.h.m_bStride);

		memcpy(String.s, UniConvert(Text), String.h.m_bLength);		
		
		// next

		Src.t.m_Array ++;
		}
	}

void CVariableStringArray::SetData(void)
{
	CDataRef  Src = (CDataRef &) m_Profile.m_dwSrc;

	for( UINT n = 0; n < m_uSize; n ++ ) {

		// read

		PBYTE pData = m_pData + Src.t.m_Array * m_uPitch;

		CStringInfo &String = (CStringInfo &) *pData;		

		CString Text(String.s);

		// save

		m_pServer->SetData( Src.m_Ref,
				    typeString,
				    setDirect,
				    DWORD(wstrdup(UniConvert(Text)))
				    );

		// next

		Src.t.m_Array ++;
		}
	}

// Diagnostic

void CVariableStringArray::ShowData(IDiagOutput *pOut)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - COMPLEX
//

// Instantiator

CVariableData * CVariableComplex::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	CVariableData *pData = New CVariableComplex(pVar);

	return pData;
	}

// Constructor

CVariableComplex::CVariableComplex(T5PTR_DBMAP pVar) : CVariableData(pVar)
{
	AfxTrace("EXTERNAL DATA TYPE NOT IMPLEMENTED - complex\n");

	HostBreak();
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Data Item - COMPLEX Array
//

// Instantiator

CVariableData * CVariableComplexArray::Create(CVariableMap &Map, T5PTR_DBMAP pVar)
{
	return New CVariableComplexArray(pVar);
	}

// Constructor

CVariableComplexArray::CVariableComplexArray(T5PTR_DBMAP pVar) : CVariableData(pVar)
{
	AfxTrace("EXTERNAL DATA TYPE NOT IMPLEMENTED - complex array\n");

	HostBreak();
	}

// End of File

