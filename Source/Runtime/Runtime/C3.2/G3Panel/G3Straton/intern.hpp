
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3straton.hpp"

#include "vm\t5vm.h"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CControlProject;
class CVariableList;
class CVariableItem;
class CExecuteFile;

//////////////////////////////////////////////////////////////////////////
//
// Control Project
//

class CControlProject : public CCodedHost
{
	public:
		// Constructor
		CControlProject(void);

		// Destructor
		~CControlProject(void);

		// Attributes
		BOOL HasLicense(void);
		BOOL HasHooks(void);
		BOOL UseWarmStart(void);
		BOOL UseFastScan(void);
		BOOL IsNonEmpty(void);

		// Operations
		void SetScan(void);
		void RunHooks(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Item Properties
		BOOL		m_fLicense;
		BOOL		m_uPoll;
		UINT		m_uScan;
		CExecuteFile  * m_pExec;
		CVariableList * m_pVars;
		CCodedItem    * m_pMode;
		CCodedItem    * m_pInitHook;
		CCodedItem    * m_pExecHook;

	protected:
		// Data Members
		BOOL m_fInit;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable List
//

class CVariableList : public CItem
{
	public:
		// Constructor
		CVariableList(void);

		// Destructor
		~CVariableList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		UINT             m_uCount;
		UINT             m_uUsed;
		CVariableItem ** m_ppVar;
	};

//////////////////////////////////////////////////////////////////////////
//
// Variable Item
//

class CVariableItem : public CCodedHost
{
	public:
		// Constructor
		CVariableItem(void);

		// Destructor
		~CVariableItem(void);
		
		//
		void SetScan(UINT uCode);
		DWORD GetData(void);
		void SetData(DWORD dwData);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		CCodedItem *  m_pValue;

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Executable File Item
//

class CExecuteFile : public CItem
{
	public:
		// Constructor
		CExecuteFile(void);

		// Destructor
		~CExecuteFile(void);

		// Attributes
		BOOL IsNonEmpty(void);

		// Initialization
		void Load(PCBYTE &pData);

	protected:
		// Data
		UINT m_Handle;
		UINT m_uSize;
	};

//////////////////////////////////////////////////////////////////////////
//
// Retentive
//

interface IStratonRetain
{
	// Methods
	virtual BOOL CanLoad(DWORD dwCrc)		= 0;
	virtual BOOL Load(T5PTR_DBRETAIN pRetain)	= 0;
	virtual BOOL CanSave(T5PTR_DBRETAIN pRetain)	= 0;
	virtual void Save(T5PTR_DBRETAIN pRetain)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Retentive Base Class
//

class CRetainBase : public IStratonRetain
{
	public:
		// Constructor
		CRetainBase(void);

	protected:
		// Data
		UINT	m_uAddr;
		DWORD	m_dwDirty[7];

		// Implementaiton
		UINT GetSize(T5PTR_DBRETAIN pRetain);
		BOOL IsDirty(T5PTR_DBRETAIN pRetain);
		void MakeDirty(T5PTR_DBRETAIN pRetain);
	};

//////////////////////////////////////////////////////////////////////////
//
// Retentive
//

class CRetainFile : public CRetainBase
{
	public:
		// Constructor
		CRetainFile(void);

		// IStratonRetain
		BOOL CanLoad(DWORD dwCrc);
		BOOL Load(T5PTR_DBRETAIN pRetain);
		BOOL CanSave(T5PTR_DBRETAIN pRetain);
		void Save(T5PTR_DBRETAIN pRetain);

	protected:
		// Data
		PCTXT m_pPath;

		// Implementaiton
		void Save(CAutoFile &File, T5_PTR pData, UINT uSize);
		void Load(CAutoFile &File, T5_PTR pData, UINT uSize);
	};

//////////////////////////////////////////////////////////////////////////
//
// Retentive
//

class CRetainMram : public CRetainBase
{
	public:
		// Constructor
		CRetainMram(void);

		// IStratonRetain
		BOOL CanLoad(DWORD dwCrc);
		BOOL Load(T5PTR_DBRETAIN pRetain);
		BOOL CanSave(T5PTR_DBRETAIN pRetain);
		void Save(T5PTR_DBRETAIN pRetain);

	protected:
		// Constants
		static const UINT constMagic = 0x5245544E;

		// Data
		UINT	m_uAddr;
		UINT	m_uLast;

		// Implementation
		BOOL Save(UINT &uAddr, T5_PTR pData, UINT uSize);
		BOOL Load(UINT &uAddr, T5_PTR pData, UINT uSize);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CVariableMap;

//////////////////////////////////////////////////////////////////////////
//
// Variable Map Wrapper
//

class CVariableMap
{
	public:
		// Constructor
		CVariableMap(void);
		CVariableMap(T5PTR_DB pBase);

		// Managemenet
		void Bind(T5PTR_DB pBase);

		// Enumeration
		T5PTR_DBMAP GetFirst(void);
		T5PTR_DBMAP GetNext(void);

		// Fast Access
		T5PTR_DBMAP GetByIndex(T5_WORD wType, T5_WORD wIndex);
		T5PTR_DBMAP GetByNumTag(T5_WORD wIndex);
		T5PTR_DBMAP GetBySymbol(T5_PTCHAR sSymbol);

		// Array Data
		T5_PTR IsArrayOfSingleVar(T5PTR_DBMAP pVar, T5_WORD &wCount, T5_WORD &wType);

		// External Address
		T5_RET SetXVAddress(T5PTR_DBMAP pVar, T5_PTR pData);

	protected:
		// Data
		T5PTR_DB m_pBase;
		T5_WORD  m_wPos;
	};


//////////////////////////////////////////////////////////////////////////
//
// Variable Wrapper
//

class CVariable
{
	public:
		// Constructor
		CVariable(T5PTR_DBMAP pVar);

		// Development
		void ShowAll(void);

		// Attributes
		BOOL		IsExtern(void);
		BOOL		IsArray (void);
		BOOL		IsAllowed(void);
		BOOL		IsProfile(PCTXT pName);
		CString		GetPropAsText(void);
		CString		GetDataAsText(void);

		// Properties
		T5_PTCHAR	GetSymbol(void);
		T5_WORD		GetType(void);
		T5_WORD		GetIndex(void);
		T5_WORD		GetDim(void);
		T5_WORD		GetStringLength(void);
		T5_WORD		GetNumTag(void);
		T5_PTCHAR	GetProfileName(void);
		T5_PTR		GetProps(void);
		T5_WORD		GetPropsSize(void);

		// VM Access
		T5_LONG		GetLongValue(void);
		T5_DWORD	GetDWordValue(void);
		T5_LINT		GetLIntValue(void);
		T5_REAL		GetRealValue(void);
		T5_LREAL	GetLRealValue(void);
		T5_PTBYTE	GetStringValue(T5_BYTE &bLen);
		T5_RET		SetLongValue(T5_LONG Value);
		T5_RET		SetDWordValue(T5_DWORD Value);
		T5_RET		SetLIntValue(T5_LINT Value);
		T5_RET		SetRealValue(T5_REAL Value);
		T5_RET		SetLRealValue(T5_LREAL Value);
		T5_RET		SetStringValue(T5_PTBYTE Value, T5_BYTE bLen);

	protected:
		// Data
		T5PTR_DBMAP m_pVar;
	};

//////////////////////////////////////////////////////////////////////////
//
// External Data Item
//

class CVariableData
{
	public:
		// Constructor
		CVariableData(T5PTR_DBMAP pVar);

		// Destructor
		~CVariableData(void);

		// Operations
		virtual void GetData(void);
		virtual void SetData(void);

		// Attributes
		BOOL IsReadOnly(void);

		// Diagnostic
		virtual void ShowData(IDiagOutput *pOut);

		// Access
		BOOL IsAvail(void);

		// Public Data
		T5PTR_DBMAP m_pVar;

		// Profile
		struct CProfile
		{
			DWORD	m_dwSrc;
			DWORD	m_dwAcc;
			};

	protected:
		// Data
		CProfile	m_Profile;
		IDataServer   * m_pServer;
		BOOL		m_fReadOnly;
	};

//////////////////////////////////////////////////////////////////////////
//
// External Data Item
//

class CVariableDataArray : public CVariableData
{
	public:
		// Constructor
		CVariableDataArray(T5PTR_DBMAP pVar, UINT uSize);

	protected:
		// Data
		UINT m_uSize;
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Access
//

extern T5_LONG	Variable_GetData(UINT uIndex);

extern void	Variable_SetData(UINT uIndex, T5_LONG Data);

//////////////////////////////////////////////////////////////////////////
//
// Profile Manager
//

class CProfileManager : public IDiagProvider
{
	public:
		// Constructor
		CProfileManager(T5PTR_DB pDbase, PCTXT pName);

		// Destructor
		~CProfileManager(void);

		// Management
		void Load(T5PTR_DB pBase, PCTXT pName);
		void Kill(void);

		// Operations
		void GetData(void);
		void SetData(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Diagnostics
		enum
		{
			diagStatus	= 1,
			diagVars	= 2,
			diagData	= 3,
			};

		// Data
		ULONG		 m_uRefs;
		CString		 m_Name;
		CVariableMap	 m_VarMap;
		UINT		 m_uCount;
		CVariableData ** m_ppData;
		IDiagManager   * m_pDiag;
		UINT             m_uProv;

		// Implementation
		void		Count(void);
		void		Alloc(void);
		void		Build(void);		
		CVariableData * Create(T5PTR_DBMAP pVar);

	// pm_diag.cpp

		// Diagnostics
		BOOL AddDiagCmds(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagVars  (IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagData  (IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// Pointer

extern CProfileManager * g_pPM;

// Instantiator

global void Create_ProfileManager(T5PTR_DB pDbase);

//////////////////////////////////////////////////////////////////////////
//
// Memory Manager
//

class CMemoryManager
{
	public:
		// Constructor
		CMemoryManager(void);

		// Operations
		void	Open(void);
		void	Close(void);
		BOOL	AllocRawVMDB(DWORD dwSize);
		PVOID	LinkVMDB(DWORD dwSize);
		void	UnlinkVMDB(void);
		void	SaveVMDB(PCTXT pName);
		BOOL	FreeVMDB(void);
		BOOL	LoadCode(void);
		void	FreeCode(void);
		PVOID	LinkCode(void);
		void	UnlinkCode(void);
		T5_RET	WriteCodeRequest(T5_WORD wRequest, T5PTR_CSLOAD pLoad);
		T5_RET	WriteSyncRequest(T5_WORD wRequest, T5PTR_CSLOAD pLoad);

	protected:
		// Data
		T5PTR_MM m_pData;
	};

extern CMemoryManager * g_pMM;

extern void Create_MemoryManager(void);

//////////////////////////////////////////////////////////////////////////
//
// Virtual Machine
//

class CVirtualMachine : public IDiagProvider
{
	public:
		// Constructor
		CVirtualMachine(CControlProject *pProject);

		// Destructor
		~CVirtualMachine(void);

		// Entry Points
		void Init(void);
		void Poll(void);
		BOOL Service(PCBYTE pIn, PBYTE pOut);
		void Term(void);

		// Operations
		void  SetState(UINT uState);
		void  BuildMainLinks(void);
		DWORD GetTotalDBSize(void);
		BOOL  GetDBSize(void);
		BOOL  Build(void);
		BOOL  LoadCode(void);
		void  FreeCode(void);
		void  FreeVMDB(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// States
		enum {
			vmIdle,
			vmOpen,
			vmInit,
			vmPoll,
			vmKill,
			vmFail,
			vmHalt,
			};

		// Diagnostics
		enum {
			diagStatus = 1,
			diagStart  = 2,
			diagStop   = 3,
			diagVars   = 4,
			};

		// Data
		ULONG		    m_uRefs;
		T5PTR_DB	    m_pBase;
		T5_PTR		    m_pCode;
		T5STR_DBTABLE	    m_Table[T5MAX_TABLE];
		UINT		    m_uState;
		DWORD		    m_dwTime;
		CControlProject   * m_pProj;
		IDiagManager      * m_pDiag;
		UINT		    m_uProv;

		// Implementation
		BOOL LoadVMDB(void);
		BOOL ColdStart(BOOL fWarm);
		void DoIdle(void);
		BOOL DoOpen(void);
		void DoInit(void);
		void DoPoll(void);
		void DoKill(void);
		void DoFail(void);

	// vm_diag.cpp

		// Diagnostics
		BOOL AddDiagCmds(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagStart (IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagStop  (IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagVars  (IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// Pointer

extern CVirtualMachine * g_pVM;

// Instantiator

extern void Create_VirtualMachine(CControlProject *pProject);

//////////////////////////////////////////////////////////////////////////
//
// Control Manager
//

class CControlItem : public CItem, 
		     public IStratonControl, 
		     public ITaskEntry
{
	public:
		// Constructor
		CControlItem(void);

		// Destructor
		~CControlItem(void);

		// IBase
		UINT Release(void);

		// IStratonControl
		void LoadControl(PCBYTE &pData);
		BOOL Service(PCBYTE pIn, PBYTE pOut);
		BOOL IsRunning(void);
		void GetTaskList(CTaskList &List);

		// Initialization
		void Load(PCBYTE &pData);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Data Members
		CControlProject * m_pProject;
		BOOL		  m_fValid;

	protected:
		// Data Members
		IMutex  * m_pMutex;
		/*BOOL      m_fRun;*/
		BOOL	  m_fUseVM;

		// Entry Points
		void MainInit(void);
		void MainExec(void);
		void MainTerm(void);
		void VirtInit(void);
		void VirtExec(void);
		void VirtTerm(void);
		void CheckComms(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dump
//

extern	void	Dump(PCTXT pLabel, PBYTE pData, UINT uSize);
extern	void	Dump(PCBYTE pData, UINT uSize);
extern	void	Dump(PBYTE pData, UINT uSize);

// End of File

#endif
