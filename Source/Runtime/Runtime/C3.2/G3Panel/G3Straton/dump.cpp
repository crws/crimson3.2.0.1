
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Memory Dump
//

// Prototype

static	void	DumpLeg(UINT uSize);
static	void	DumpAsc(PBYTE pData, UINT uSize);
static	void	DumpHex(PBYTE pData, UINT uSize);

// Code

global	void	Dump(PCTXT pLabel, PBYTE pData, UINT uSize)
{
#if 0
	AfxTrace("%s (%d)", pLabel, uSize);

	Dump(pData, uSize);

#endif
	}

global	void	Dump(PCBYTE pData, UINT uSize)
{
#if 0

	Dump(PBYTE(pData), uSize);

#endif
	}

global	void	Dump(PBYTE pData, UINT uSize)
{
	if( pData ) {

		AfxTrace("\n");

		uSize = ((uSize + 15) / 16) * 16;

		for( UINT n = 0; n < uSize; n += 16 ) {

			if( n == 0 ) {

				DumpLeg(16);				
				}

			DumpHex(pData, 16);

			DumpAsc(pData, 16);

			pData += 16;
			}

		AfxTrace("\n");
		}
	else
		AfxTrace(" - null data pointer\n");
	}

static	void	DumpLeg(UINT uSize)
{
	for( UINT m = 0; m < 2; m ++ ) {

		for( UINT n = 0; n < uSize; n ++ ) {

			if( !(n%(uSize / 2)) ) {

				AfxTrace(m ? "-" : " ");
				}

			AfxTrace(m ? "---" : CPrintf("%0.2d ", n));
			}

		AfxTrace("\n");
		}
	}

static	void	DumpAsc(PBYTE pData, UINT uSize)
{
	for( UINT n = 0; n < uSize; n ++ ) {

		char c = *pData++;

		AfxTrace(isprint(c) ? "%c" : ".", c);
		}

	AfxTrace("\n");
	}

static	void	DumpHex(PBYTE pData, UINT uSize)
{
	for( UINT n = 0; n < uSize; n ++ ) {

		if( !(n%8) ) {

			AfxTrace(" ");
			}

		AfxTrace("%2.2X ", *pData++);
		}
	}

// End of File
