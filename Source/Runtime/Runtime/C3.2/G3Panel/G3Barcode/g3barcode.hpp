
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Barcode Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3BARCODE_HPP

#define	INCLUDE_G3BARCODE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <c3shim.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Barcode Engine Access
//

DLLAPI void Barcode_Render( PCUTF   pText,
			    UINT    uCode,
			    int     xSpace,
			    int     ySpace,
			    UINT    uMask,
			    PBYTE & pData,
			    int   & nStep,
			    int   & xSize,
			    int   & ySize
			    );

// End of File

#endif
