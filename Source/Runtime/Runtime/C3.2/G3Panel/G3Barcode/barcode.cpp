
#include "intern.hpp"

#include "zint\zint.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Barcode Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Scaling Methods
//

enum
{
	scale2X18,
	scale2X,
	scaleFixed
	};

//////////////////////////////////////////////////////////////////////////
//
// API
//

static int GetScaleType(UINT uCode)
{
	switch( uCode ) {

		case 28:
		case 53:
		case 55:
		case 56:
		case 80:
		case 106:
		case 107:
			return scale2X;

		case 23:
		case 24:
		case 40:
		case 41:
		case 42:
		case 43:
		case 44:
		case 45:
		case 57:
		case 58:
		case 63:
		case 64:
		case 65:
		case 66:
		case 67:
		case 68:
		case 70:
		case 71:
		case 76:
		case 79:
		case 82:
		case 83:
		case 84:
		case 90:
		case 92:
		case 97:
		case 102:
		case 103:
		case 104:
		case 105:
		case 108:
		case 109:
		case 112:
		case 141:
		case 142:
			return scaleFixed;
		}

	return scale2X18;
	}

static void SetOptions(struct zint_symbol *p1)
{
	switch( p1->symbology ) {

		case BARCODE_DATAMATRIX:

			p1->option_3 = DM_SQUARE;
			
			break;
		}
	}

DLLAPI void Barcode_Render( PCUTF   pText,
			    UINT    uCode,
			    int     xSpace,
			    int     ySpace,
			    UINT    uMask,
			    PBYTE & pData,
			    int   & nStep,
			    int   & xSize,
			    int   & ySize
			    )
{
	int nType = GetScaleType(uCode);

	int nMin, nDiv, nSub;

	switch( nType ) {

		case scale2X18:
			nMin = 38;
			nDiv = 2;
			nSub = 9;
			break;

		case scale2X:
			nMin = 20;
			nDiv = 2;
			nSub = 0;
			break;

		case scaleFixed:
			nMin = 16;
			nDiv = 1;
			nSub = 0;
			break;
		
		default:
			return;
		}

	if( ySpace >= nMin ) {

		// TODO -- Real UTF8 encoding, please!

		char *pCopy = (char *) alloca(wstrlen(pText) + 1);

		for( int nCopy = 0; (pCopy[nCopy] = char(pText[nCopy])); nCopy++ );

		float scale = 1;

		for( int nPass = 0; nPass < 2; nPass++ ) {

			struct zint_symbol *p1 = ZBarcode_Create();

			p1->input_mode = UNICODE_MODE;
	
			p1->symbology  = uCode;

			p1->option_1   = 0;

			p1->option_2   = 0;

			p1->option_3   = 0;

			p1->scale  = scale;

			p1->height = int(ySpace / nDiv / p1->scale) - nSub;

			SetOptions(p1);

			ZBarcode_Encode(p1, PBYTE(pCopy), 0);

			ZBarcode_Buffer(p1, 0);

			if( !p1->bitmap_width ) {

				ZBarcode_Delete(p1);

				break;
				}

			if( nPass == 0 ) {

				float sx = float(xSpace) / float(p1->bitmap_width);

				float sy = float(ySpace) / float(p1->bitmap_height);

				scale = (nType == scaleFixed) ? min(sx, sy) : sx;

				if( (uMask & 1) && scale < 1 ) scale = 1;

				if( (uMask & 2) && scale > 1 ) scale = 1;

				if( (uMask & 4) )	       scale = max(1,float(int(scale)));

				ZBarcode_Delete(p1);
				}

			if( nPass == 1 ) {

				xSize = p1->bitmap_width;

				ySize = p1->bitmap_height;

				nStep = 4 * ((xSize + 31) / 32);

				pData = New BYTE [ nStep * ySize ];

				memset(pData, 0, nStep * ySize);

				PBYTE pFrom = PBYTE(p1->bitmap);

				PBYTE pDest = pData;

				for( int y = 0; y < ySize; y++ ) {

					PBYTE pFrom1 = pFrom;

					PBYTE pDest1 = pDest;

					for( int x = 0; x < xSize; ) {

						if( x++ < xSize && !pFrom1[0*3] ) *pDest1 |= 0x80;
						if( x++ < xSize && !pFrom1[1*3] ) *pDest1 |= 0x40;
						if( x++ < xSize && !pFrom1[2*3] ) *pDest1 |= 0x20;
						if( x++ < xSize && !pFrom1[3*3] ) *pDest1 |= 0x10;
						if( x++ < xSize && !pFrom1[4*3] ) *pDest1 |= 0x08;
						if( x++ < xSize && !pFrom1[5*3] ) *pDest1 |= 0x04;
						if( x++ < xSize && !pFrom1[6*3] ) *pDest1 |= 0x02;
						if( x++ < xSize && !pFrom1[7*3] ) *pDest1 |= 0x01;

						pFrom1 += 24;

						pDest1 += 1;
						}

					#if defined(_E_LITTLE)

					for( int n = 0; n < nStep; n += 4 ) {

						PDWORD(pDest+n)[0] = HostToMotor(PDWORD(pDest+n)[0]);
						}

					#endif

					pFrom += 3 * xSize;

					pDest += nStep;
					}

				ZBarcode_Delete(p1);
				}
			}
		}
	}

// End of File
