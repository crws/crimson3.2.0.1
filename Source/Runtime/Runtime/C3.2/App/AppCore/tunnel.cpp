
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "g3slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tunnel Service
//

class CTunnel : public ILinkService
{
	public:
		// Constructor
		CTunnel(void);

		// ILinkService
		void Timeout(void);
		UINT Process(CLinkFrame &Req, CLinkFrame &Rep);
		void EndLink(CLinkFrame &Req);
	};

//////////////////////////////////////////////////////////////////////////
//
// Tunnel Loader Service
//

global	ILinkService *	Create_TunnelService(void)
{
	return New CTunnel;
	}

// Constructor

CTunnel::CTunnel(void)
{
	}

// ILinkService

void CTunnel::Timeout(void)
{
	}

UINT CTunnel::Process(CLinkFrame &Req, CLinkFrame &Rep)
{
	if( Req.GetService() == servTunnel ) {

		if( Req.GetOpcode() == tunnelTunnel ) {

			UINT   uPtr  = 0;

			BYTE   bDrop = Req.ReadByte(uPtr);

			BYTE   bSkip = Req.ReadByte(uPtr);

			PCBYTE pData = Req.ReadData(uPtr, 64);

			BYTE   bData[64];

			memcpy(bData, pData, 64);

			if( RackTunnel(bDrop, bData) ) {

				Rep.StartFrame(servTunnel, tunnelTunnel);

				Rep.AddByte(bDrop);

				Rep.AddByte(1);

				Rep.AddData(bData, 64);

				return procOkay;
				}

			Rep.StartFrame(servTunnel, tunnelTunnel);

			Rep.AddByte(bDrop);

			Rep.AddByte(0);

			return procOkay;
			}
		}

	return procError;
	}

void CTunnel::EndLink(CLinkFrame &Req)
{
	}

// End of File
