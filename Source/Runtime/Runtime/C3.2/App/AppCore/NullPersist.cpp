
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "NullPersist.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Null Persistance Manager
//

// Instantiator

global IPersist * Create_NullPersist(void)
{
	return New CNullPersist;
	}

// Constructor

CNullPersist::CNullPersist(void)
{
	UINT nb  = 65536 * 4;

	m_pData  = New BYTE [ nb ];

	m_fDirty = FALSE;

	memset(m_pData, 0, nb);
	}

// IPersist

void CNullPersist::Init(void)
{
	}

void CNullPersist::ByeBye(void)
{
	}

void CNullPersist::Term(void)
{
	}

BOOL CNullPersist::IsDirty(void)
{
	return m_fDirty;
	}

void CNullPersist::Commit(BOOL fReset)
{
	m_fDirty = FALSE;
	}

BYTE CNullPersist::GetByte(DWORD dwAddr)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	UINT  uAddr = LOWORD(dwAddr);

	PBYTE pPage = m_pData + 65536 * uBank + uAddr;

	return *PBYTE(pPage);
	}

WORD CNullPersist::GetWord(DWORD dwAddr)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	UINT  uAddr = LOWORD(dwAddr);

	PBYTE pPage = m_pData + 65536 * uBank + uAddr;

	return *PWORD(pPage);
	}

LONG CNullPersist::GetLong(DWORD dwAddr)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	UINT  uAddr = LOWORD(dwAddr);

	PBYTE pPage = m_pData + 65536 * uBank + uAddr;

	return *PLONG(pPage);
	}

void CNullPersist::GetData(PBYTE pData, DWORD dwAddr, UINT uCount)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	UINT  uAddr = LOWORD(dwAddr);

	PBYTE pPage = m_pData + 65536 * uBank + uAddr;

	memcpy(pData, pPage, uCount);
	}

void CNullPersist::PutByte(DWORD dwAddr, BYTE bData)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	UINT  uAddr = LOWORD(dwAddr);

	PBYTE pPage = m_pData + 65536 * uBank + uAddr;

	if( *PBYTE(pPage) != bData ) {

		*PBYTE(pPage) = bData;

		m_fDirty = TRUE;
		}
	}

void CNullPersist::PutWord(DWORD dwAddr, WORD wData)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	UINT  uAddr = LOWORD(dwAddr);

	PBYTE pPage = m_pData + 65536 * uBank + uAddr;

	if( *PWORD(pPage) != wData ) {

		*PWORD(pPage) = wData;

		m_fDirty = TRUE;
		}
	}

void CNullPersist::PutLong(DWORD dwAddr, LONG lData)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	UINT  uAddr = LOWORD(dwAddr);

	PBYTE pPage = m_pData + 65536 * uBank + uAddr;

	if( *PLONG(pPage) != lData ) {

		*PLONG(pPage) = lData;

		m_fDirty = TRUE;
		}
	}

void CNullPersist::PutData(PBYTE pData, DWORD dwAddr, UINT uCount)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	UINT  uAddr = LOWORD(dwAddr);

	PBYTE pPage = m_pData + 65536 * uBank + uAddr;

	if( memcmp(pPage, pData, uCount) ) {

		memcpy(pPage, pData, uCount);

		m_fDirty = TRUE;
		}
	}

// End of File
