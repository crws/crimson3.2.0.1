
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "NullProm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flash Memory Manager
//

// Instantiator

global IProm * Create_NullProm(void)
{
	return New CNullProm();
	}

// Constructor

CNullProm::CNullProm(void)
{
	}

// IProm

void CNullProm::Init(DWORD dwParam)
{
	}

void CNullProm::Task(HTASK hTask, DWORD dwParam)
{
	}

void CNullProm::Term(DWORD dwParam)
{
	}

void CNullProm::Sync(BOOL fTerm)
{
	}

void CNullProm::Lock(void)
{
	}

void CNullProm::Free(void)
{
	}

UINT CNullProm::GetCount(void)
{
	return 0;
	}

UINT CNullProm::GetSize(void)
{
	return 0;
	}

PBYTE CNullProm::GetAddr(UINT uSect)
{
	return NULL;
	}

void CNullProm::BatchStart(void)
{
	}

void CNullProm::BatchEnd(void)
{
	}

void CNullProm::QueueErase(UINT uSect)
{
	}

void CNullProm::QueueWrite(UINT uSect, UINT uAddr, PCBYTE pData, UINT uCount)
{
	}

void CNullProm::QueueWrite(UINT uSect, UINT uAddr, PCBYTE pData, UINT uCount, BOOL fBuff)
{
	}

// End of File
