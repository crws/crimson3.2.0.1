
#include "Intern.hpp"

#include "NandFlashPersist.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Persistence Manager
//

// Instantiator

global IPersist * Create_NandFlashPersist(UINT uStart, UINT uEnd)
{
	CNandBlock Start(0, uStart);

	CNandBlock End  (0, uEnd);

	return New CNandFlashPersist(Start, End);
	}

// Constructor

CNandFlashPersist::CNandFlashPersist(CNandBlock const &Start, CNandBlock const &End)
{
	for( UINT uBank = 0; uBank < elements(m_Bank); uBank++ ) {

		CBank &Bank  = m_Bank[uBank];
		
		Bank.m_pData = NULL;
		}

	m_BlockStart = Start;
	
	m_BlockEnd   = End;
	}

// Destructor

CNandFlashPersist::~CNandFlashPersist(void)
{
	}

// IPersist

void CNandFlashPersist::Init(void)
{
	CNandClient::Init();

	InitBanks();

	LoadBanks();

	m_BlockScan.Invalidate();
	}

void CNandFlashPersist::Term(void)
{
//	Commit(false);

	FreeBanks();
	}

void CNandFlashPersist::ByeBye(void)
{
	// NOTE -- Not needed on this platform as the memory
	// we are using is not preserved through a reset cycle
	// and therefore does not need to be trashed.
	}

BOOL CNandFlashPersist::IsDirty(void)
{
	for( UINT uBank = 0; uBank < elements(m_Bank); uBank++ ) {

		CBank const &Bank = m_Bank[uBank];

		if( Bank.m_fDirty ) {
			
			return true;
			}
		}

	return false;
	}

void CNandFlashPersist::Commit(BOOL fReset)
{
	for( UINT uBank = 0; uBank < elements(m_Bank); uBank ++ ) {

		CBank &Bank = m_Bank[uBank];

		if( Bank.m_fDirty ) {

			CNandBlock Block;

			while( FindFreeBlock(Block) ) {

				CNandPage Page  = CNandPage(Block, 1);

				UINT      uSize = C64K;

				UINT      uHead = sizeof(CPageHeader);

				UINT      uMost = m_uPageSize - uHead;

				PBYTE     pData = Bank.m_pData;

				while( uSize ) {

					PPAGE pPage = PPAGE(m_pPageData);

					UINT  uHead = sizeof(CPageHeader);

					UINT  uMost = m_uPageSize - uHead;

					UINT  uCopy = Min(uSize, uMost);

					memcpy(m_pPageData + uHead, pData, uCopy);

					pPage->Pad1 = 0;
					
					pPage->Pad2 = 0;
					
					pPage->Pad3 = 0;

					if( uSize == C64K ) {

						pPage->Magic = magicHead;
						
						pPage->Param = uBank;
						}
					else {
						if( uSize <= uMost ) {

							pPage->Magic = magicTail;

							pPage->Param = CRC32(Bank.m_pData, C64K);
							}
						else {
							pPage->Magic = magicBody;

							pPage->Param = 0;
							}
						}

					if( WritePage(Page, true) ) {

						Page.m_uPage++;

						uSize -= uCopy;

						pData += uCopy;

						continue;
						}

					break;
					}

				if( !uSize ) {

					if( Bank.m_Block.IsValid() ) {

						FormatBlock(Bank.m_Block);
						}

					Bank.m_fDirty = false;
					
					Bank.m_Block  = Block;

					break;
					}

				// NOTE -- If we get here with data still not copied, we know
				// that one of the writes failed, in which case the whole block
				// is suspect and we need to find another empty one to use.

				MarkBlockBad(Block);
				}

			if( Bank.m_fDirty ) {

				// NOTE -- If we get here, we know that we've given up after
				// not being able to find a block to store out data. We have
				// the option of failing silently, or resetting the unit. We
				// ideally ought to have a better way of reporting this!

				if( false ) {

					Bank.m_fDirty = false;
					}
				else {
					AfxTrace("CNandFlashPersist::Commit -- Can't Find Free Block\n");

					HostBreak();
					}
				}
			}
		}

	if( fReset ) {

		// !!!C3!!! -- What do we do here???

		/*for(;;) HostMaxIPL();*/
		}
	}

BYTE CNandFlashPersist::GetByte(DWORD dwAddr)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	CBank &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	return *PBYTE(pPage);
	}

WORD CNandFlashPersist::GetWord(DWORD dwAddr)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	CBank &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	return *PWORD(pPage);
	}

LONG CNandFlashPersist::GetLong(DWORD dwAddr)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	CBank &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	return *PLONG(pPage);
	}

void CNandFlashPersist::GetData(PBYTE pData, DWORD dwAddr, UINT uCount)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	CBank &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	memcpy(pData, pPage, uCount);
	}

void CNandFlashPersist::PutByte(DWORD dwAddr, BYTE bData)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	CBank &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	if( *PBYTE(pPage) != bData ) {

		*PBYTE(pPage) = bData;

		Bank.m_fDirty = true;
		}
	}

void CNandFlashPersist::PutWord(DWORD dwAddr, WORD wData)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	CBank &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	if( *PWORD(pPage) != wData ) {

		*PWORD(pPage) = wData;

		Bank.m_fDirty = true;
		}
	}

void CNandFlashPersist::PutLong(DWORD dwAddr, LONG lData)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	CBank &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	if( *PLONG(pPage) != lData ) {

		*PLONG(pPage) = lData;

		Bank.m_fDirty = true;
		}
	}

void CNandFlashPersist::PutData(PBYTE pData, DWORD dwAddr, UINT uCount)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	CBank &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	if( memcmp(pPage, pData, uCount) ) {

		memcpy(pPage, pData, uCount);

		Bank.m_fDirty = true;
		}
	}

// Implementation

bool CNandFlashPersist::InitBanks(void)
{
	// NOTE -- All banks are marked as clean, and have
	// the database GUID installed in their first 16
	// bytes. This marker is never written after here.

	BYTE bGuid[16];

	g_pDbase->GetVersion(bGuid);
		
	for( UINT uBank = 0; uBank < elements(m_Bank); uBank++ ) {

		CBank &Bank   = m_Bank[uBank];
		
		Bank.m_pData  = New BYTE [ C64K ];

		Bank.m_fDirty = false;

		Bank.m_Block.Invalidate();

		memset(Bank.m_pData, 0x0, C64K);

		memcpy(Bank.m_pData, bGuid, 16);
		}

	return true;
	}

bool CNandFlashPersist::FreeBanks(void)
{
	for( UINT uBank = 0; uBank < elements(m_Bank); uBank ++ ) {

		CBank &Bank = m_Bank[uBank];

		delete [] Bank.m_pData;
		}

	return true;
	}

bool CNandFlashPersist::LoadBanks(void)
{
	// NOTE -- When we leave here, all the blocks that we're not using
	// are erased and have the format marker stored in the first page
	// so that they are ready for use. We have at most one copy of each
	// bank and we know where it's located. This is important as the
	// Commit code assumes that it doesn't have to perform a pre-erase.

	CNandBlock Block;

	Block.Invalidate();

	while( GetNextGoodBlock(Block) ) {

		if( ReadPage(Block) ) {

			PCPAGE pPage = PCPAGE(m_pPageData);

			if( pPage->Magic == magicBank ) {

				CNandPage Page = CNandPage(Block, 1);

				if( ReadPage(Page) ) {

					if( pPage->Magic == magicHead ) {

						UINT  uBank = pPage->Param;

						UINT  uSize = C64K;

						PBYTE pData = New BYTE [ uSize ];

						PBYTE pDest = pData;

						for(;;) {

							UINT uHead = sizeof(CPageHeader);

							UINT uMost = m_uPageSize - uHead;

							UINT uCopy = Min(uSize, uMost);

							memcpy(pDest, m_pPageData + uHead, uCopy);
					
							pDest += uCopy;

							uSize -= uCopy;

							if( uSize ) {

								Page.m_uPage++;

								if( ReadPage(Page) ) {

									if( pPage->Magic == magicBody && uSize >= uMost ) {

										continue;
										}
							
									if( pPage->Magic == magicTail && uSize <  uMost ) {

										continue;
										}
									}
								}

							break;
							}

						if( !uSize ) {

							if( pPage->Param == CRC32(pData, C64K) ) {

								CBank &Bank = m_Bank[uBank];

								// NOTE -- The GUID was already put in place by InitBanks
								// and so we use that marker to check that this block is
								// really associated with this database.

								if( !memcmp(Bank.m_pData, pData, 16) ) {

									if( Bank.m_Block.IsValid() ) {

										// NOTE -- We don't use generation numbers, but just
										// take the last version that we find and destroy the
										// other ones so that they can be written later.

										FormatBlock(Bank.m_Block);
										}

									Bank.m_fDirty = false;

									Bank.m_Block  = Block;

									memcpy(Bank.m_pData, pData, C64K);

									delete [] pData;

									continue;
									}
								}
							}

						delete [] pData;
						}
					else {
						// NOTE -- If we get here, we have a valid block with
						// a format marker in it but no actual data. We can
						// avoid erasing it to speed up the process and keep
						// the wear on the chip to a minimum.

						continue;
						}
					}
				}
			}

		// NOTE -- We fall through to here if anything goes
		// wrong with copying data from this block or if we
		// don't find the format marker. Unlike the database
		// or firmware, this is acceptable and could occur as
		// a result of an operation interrupt by power-down.

		FormatBlock(Block);
		}

	return true;
	}

bool CNandFlashPersist::FindFreeBlock(CNandBlock &Block)
{
	bool fWrap = true;

	for(;;) {

		if( GetNextGoodBlock(m_BlockScan, fWrap) ) {

			for( UINT uBank = 0; uBank < elements(m_Bank); uBank ++ ) {

				CBank &Bank = m_Bank[uBank];

				if( Bank.m_Block == m_BlockScan ) {

					break;
					}
				}

			if( uBank == elements(m_Bank) ) {

				Block = m_BlockScan;

				return true;
				}

			continue;
			}

		break;
		}

	// NOTE -- If we get here, we have a problem as we're out of
	// usable blocks and so we can't store out data any more. The
	// unit is effectively broken and needs to be replaced.

	return false;
	}

bool CNandFlashPersist::FormatBlock(CNandBlock &Block)
{
	// NOTE -- This is used to prepare a block for later use
	// by erasing it and then writing the format marker. We
	// kill the format marker first so that we're left in a
	// detectable state should the erase get interrupted.

	PPAGE pPage = PPAGE(m_pTestData);

	memset(m_pTestData, 0, m_uPageSize);

	WritePage(Block, m_pTestData, false);

	if( EraseBlock(Block, true) ) {

		pPage->Magic = magicBank;

		if( !WritePage(Block, m_pTestData, true) ) {

			MarkBlockBad(Block);

			return false;
			}

		return true;
		}

	return false;
	}

// End of File
