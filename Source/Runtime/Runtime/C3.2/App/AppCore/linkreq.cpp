
#include "intern.hpp"

#include "g3link.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Request
//

// Constructor

CLinkFrame::CLinkFrame(void)
{
	m_pData = NULL;

	m_pBulk = NULL;
	}

// Destructor

CLinkFrame::~CLinkFrame(void)
{
	FreeData();

	FreeBulk();
	}

// Frame Creation

void CLinkFrame::StartFrame(BYTE bService, BYTE bOpcode)
{
	FreeData();

	FreeBulk();

	m_bService   = bService;

	m_bOpcode    = bOpcode;

	m_bFlags     = 0;

	m_pData      = NULL;

	m_pBulk	     = NULL;

	m_uDataAlloc = 0;

	m_uBulkAlloc = 0;

	m_uDataCount = 0;

	m_uBulkCount = 0;

	m_fSlow      = FALSE;
	}

// Operations

void CLinkFrame::MarkAsync(void)
{
	m_bFlags |= flagsAsync;
	}

void CLinkFrame::MarkVerySlow(void)
{
	m_fSlow = TRUE;
	}

void CLinkFrame::SetFlags(BYTE bFlags)
{
	m_bFlags = bFlags;
	}

// Simple Data

void CLinkFrame::AddByte(BYTE bData)
{
	ExpandData(sizeof(BYTE));

	m_pData[m_uDataCount++] = bData;
	}

void CLinkFrame::AddWord(WORD wData)
{
	ExpandData(sizeof(WORD));

	m_pData[m_uDataCount++] = HIBYTE(wData);
	m_pData[m_uDataCount++] = LOBYTE(wData);
	}

void CLinkFrame::AddLong(LONG lData)
{
	ExpandData(sizeof(LONG));

	m_pData[m_uDataCount++] = HIBYTE(HIWORD(lData));
	m_pData[m_uDataCount++] = LOBYTE(HIWORD(lData));
	m_pData[m_uDataCount++] = HIBYTE(LOWORD(lData));
	m_pData[m_uDataCount++] = LOBYTE(LOWORD(lData));
	}

void CLinkFrame::AddGuid(GUID Guid)
{
	AddData(PCBYTE(&Guid), sizeof(Guid));
	}

// Larger Data

void CLinkFrame::AddData(PCBYTE pData, UINT uCount)
{
	if( uCount ) {

		ExpandData(uCount);

		memcpy(m_pData + m_uDataCount, pData, uCount);

		m_uDataCount += uCount;
		}
	}

void CLinkFrame::AddBulk(PCBYTE pData, UINT uCount)
{
	if( uCount ) {

		ExpandBulk(uCount);

		memcpy(m_pBulk + m_uBulkCount, pData, uCount);

		m_uBulkCount += uCount;
		}
	}

// Data Reads

PCBYTE CLinkFrame::ReadData(UINT &uPtr, UINT uCount) const
{
	PCBYTE pData = m_pData + uPtr;

	uPtr += uCount;

	return pData;
	}

GUID & CLinkFrame::ReadGuid(UINT &uPtr) const
{
	return (GUID &) (ReadData(uPtr, 16)[0]);
	}

BYTE CLinkFrame::ReadByte(UINT &uPtr) const
{
	return m_pData[uPtr++];
	}

WORD CLinkFrame::ReadWord(UINT &uPtr) const
{
	BYTE hi = ReadByte(uPtr);
	
	BYTE lo = ReadByte(uPtr);

	return MAKEWORD(lo, hi);
	}

DWORD CLinkFrame::ReadLong(UINT &uPtr) const
{
	WORD hi = ReadWord(uPtr);

	WORD lo = ReadWord(uPtr);

	return MAKELONG(lo, hi);
	}

// Attributes

BYTE CLinkFrame::GetService(void) const
{
	return m_bService;
	}

BYTE CLinkFrame::GetOpcode(void) const
{
	return m_bOpcode;
	}

BYTE CLinkFrame::GetFlags(void) const
{
	return m_bFlags;
	}

BOOL CLinkFrame::IsAsync(void) const
{
	return (m_bFlags & flagsAsync) ? TRUE : FALSE;
	}

BOOL CLinkFrame::IsVerySlow(void) const
{
	return m_fSlow;
	}

UINT CLinkFrame::GetDataSize(void) const
{
	return m_uDataCount;
	}

UINT CLinkFrame::GetBulkSize(void) const
{
	return m_uBulkCount;
	}

PBYTE CLinkFrame::GetData(void) const
{
	return m_pData;
	}

PBYTE CLinkFrame::GetBulk(void) const
{
	return m_pBulk;
	}	     

BYTE CLinkFrame::GetAt(UINT uPos)
{
	if( m_uDataCount >= uPos ) {

		return m_pData[uPos];
		}
	else {
		return 0;
		}
	}

// Implementation

void CLinkFrame::FreeData(void)
{
	if( m_pData ) {

		delete m_pData;

		m_pData = NULL;
		}
	}

void CLinkFrame::FreeBulk(void)
{
	if( m_pBulk ) {

		delete m_pBulk;

		m_pBulk = NULL;
		}
	}

void CLinkFrame::ExpandData(UINT uExtra)
{
	if( m_uDataCount + uExtra > m_uDataAlloc ) {

		UINT uAlloc = m_uDataAlloc + max(uExtra, 256);

		PBYTE pData = New BYTE [ uAlloc ];

		if( m_pData ) {

			memcpy(pData, m_pData, m_uDataAlloc);

			delete m_pData;
			}

		m_uDataAlloc = uAlloc;

		m_pData      = pData;
		}
	}

void CLinkFrame::ExpandBulk(UINT uExtra)
{
	if( m_uBulkCount + uExtra > m_uBulkAlloc ) {

		UINT uAlloc = m_uBulkAlloc + max(uExtra, 256);

		PBYTE pBulk = New BYTE [ uAlloc ];

		if( m_pBulk ) {

			memcpy(pBulk, m_pBulk, m_uBulkAlloc);

			delete m_pBulk;
			}

		m_uBulkAlloc = uAlloc;

		m_pBulk      = pBulk;
		}
	}

// End of File
