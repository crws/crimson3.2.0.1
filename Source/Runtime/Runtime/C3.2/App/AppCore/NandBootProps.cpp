
#include "intern.hpp"

#include "NandBootProps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Nand Firmware Properties Object
//

extern	void	Dump(PCTXT pLabel, PCBYTE pData, UINT uCount);

// Instantiator

global IFirmwareProps *	Create_NandBootProps(UINT uStart, UINT uEnd)
{
	return New CNandBootProps(uStart, uEnd);
	}

// Constructor

CNandBootProps::CNandBootProps(UINT uStart, UINT uEnd)
{
	m_pNandMemory = NULL;

	AfxGetObject("nand", 0, INandMemory, m_pNandMemory);

	m_pNandMemory->GetGeometry(m_Geometry);

	m_BlockStart = CNandBlock(0, uStart);

	m_BlockEnd   = CNandBlock(0, uEnd);

	m_uPageSize  = m_Geometry.m_uBytes * m_Geometry.m_uSectors;

	m_uAlloc     = m_Geometry.m_uPages * m_uPageSize * (uEnd - uStart);

	m_pPageData  = New BYTE [ m_uPageSize ];

	m_pImage     = NULL;

	m_uImage     = 0;

	ReadCode();
	}

// Destructor

CNandBootProps::~CNandBootProps(void)
{
	delete [] m_pPageData;

	delete [] m_pImage;
	}

// IFirmwareProps

bool CNandBootProps::IsCodeValid(void)
{
	#if defined(_M_IMX51)

	PDWORD p = PDWORD(m_pImage + 0x00000000);

	return p[1] == 0x000000B1;

	#endif

	#if defined(_M_ARM9)

	PDWORD p = PDWORD(m_pImage + 0x00000020);

	return p[0] == 0x00524556;

	#endif
	
	#if defined(_M_AM437)

	PDWORD p = PDWORD(m_pImage + 0x00000000);

	return p[1] == 0x40300000;

	#endif

	return false;
	}

PCBYTE CNandBootProps::GetCodeVersion(void)
{
	static BYTE bNull[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };

	if( ReadCode() ) {

		#if defined(_M_IMX51)

		PDWORD p = PDWORD(m_pImage + 0x00000000);

		return (p[1] == 0x000000B1) ? PCBYTE(p+7) : bNull;

		#endif

		#if defined(_M_ARM9)

		PDWORD p = PDWORD(m_pImage + 0x00000020);		

		return (p[0] == 0x00524556) ? PCBYTE(p+1) : bNull;

		#endif

		#if defined(_M_AM437)

		PDWORD p = PDWORD(m_pImage + 0x00000800);		

		return (p[2] == 0x00524556) ? PCBYTE(p+3) : bNull;

		#endif
		}

	return bNull;
	}

UINT CNandBootProps::GetCodeSize(void)
{
	if( ReadCode() ) {

		return m_uImage;
		}

	return 0;
	}

PCBYTE CNandBootProps::GetCodeData(void)
{
	if( ReadCode() ) {

		return m_pImage;
		}

	return 0;
	}

// Implementation

bool CNandBootProps::AllocImage(void)
{
	if( !m_pImage ) {

		m_pImage = New BYTE [ m_uAlloc ];

		m_uImage = 0;

		return true;
		}

	return false;
	}

bool CNandBootProps::ReadCode(void)
{
	if( !m_pImage ) {

		AllocImage();		
		
		CNandPage Page = m_BlockStart;

		#if defined(_M_IMX51)

		UINT   uOffset = 1024;

		#else

		UINT   uOffset = 0;

		#endif

		for(;;) {

			if( ReadPage(Page) ) {

				UINT uCopy = m_uPageSize - uOffset;

				memcpy( m_pImage    + m_uImage, 
					m_pPageData + uOffset,
					uCopy
					);

				m_uImage += uCopy;

				if( !GetNextPage(Page) ) {
				
					break;
					}

				uOffset = 0;
		
				continue;
				}

			return false;
			}		
		}	
		
	return true;
	}

bool CNandBootProps::GetNextPage(CNandPage &Page)
{
	CNandPage Scan = Page;

	if( ++Scan.m_uPage >= m_Geometry.m_uPages ) {
		
		Scan.m_uPage   = 0;

		if( !GetNextBlock(Scan) ) {
			
			return false;
			}
		}

	Page = Scan;

	return true;
	}

bool CNandBootProps::GetNextBlock(CNandBlock &Block)
{
	CNandBlock Scan = Block;

	if( ++Scan.m_uBlock >= m_Geometry.m_uBlocks ) {

		Scan.m_uBlock = 0;

		return false;
		}

	if( Scan == m_BlockEnd ) {
		
		return false;
		}

	Block = Scan;

	return true;
	}

bool CNandBootProps::ReadPage(CNandPage Page)
{
	return m_pNandMemory->ReadPage( Page.m_uChip, 
					Page.m_uBlock, 
					Page.m_uPage, 
					m_pPageData
					);
	}

// End of File
