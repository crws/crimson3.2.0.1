
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Runtime
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#include "DebugCmds.hpp"

////////////////////////////////////////////////////////////////////////
//
// Debug Commands Object
//

// Instantiator

void DebugInit(void)
{
	New CDebugCmds;
	}

// Constructor

CDebugCmds::CDebugCmds(void)
{
	StdSetRef();

	m_pDiag   = NULL;

	m_dwStart = GetNow();

	AddDiagCmds();
	}

// Destructor

CDebugCmds::~CDebugCmds(void)
{
	if( m_pDiag ) {

		m_pDiag->RevokeProvider(m_uProv);

		m_pDiag->Release();

		m_pDiag = NULL;
		}
	}

// IUnknown

HRESULT CDebugCmds::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagProvider);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CDebugCmds::AddRef(void)
{
	StdAddRef();
	}

ULONG CDebugCmds::Release(void)
{
	StdRelease();
	}

// IDiagProvider

UINT CDebugCmds::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		// TODO -- Add ability to delete with wildcards.

		case cmdAuto:		return CmdAuto(pOut, pCmd);		
		case cmdCFTest:		return CmdCFTest(pOut, pCmd);		
		case cmdChangeDir:	return CmdChangeDir(pOut, pCmd);	
		case cmdChkDsk:		return CmdChkDsk(pOut, pCmd);		
		case cmdClearDbase:	return CmdClearDbase(pOut, pCmd);	
		case cmdClearGMC:	return CmdClearGMC(pOut, pCmd);	
		case cmdCrash:		return CmdCrash(pOut, pCmd);		
		case cmdCrashDump:	return CmdCrashDump(pOut, pCmd);	
		case cmdCycle:		return CmdCycle(pOut, pCmd);		
		case cmdDelete:		return CmdDelete(pOut, pCmd);		
		case cmdDir:		return CmdDir(pOut, pCmd);		
		case cmdFakeTouch:	return CmdFakeTouch(pOut, pCmd);	
		case cmdFormat:		return CmdFormat(pOut, pCmd);		
		case cmdFRAMDump:	return CmdFRAMDump(pOut, pCmd);	
		case cmdHello:		return CmdHello(pOut, pCmd);	
		case cmdHexDump:	return CmdHexDump(pOut, pCmd);		
		case cmdMakeDir:	return CmdMakeDir(pOut, pCmd);	
		case cmdMemCheck:	return CmdMemCheck(pOut, pCmd);	
		case cmdMemStat:	return CmdMemStat(pOut, pCmd);		
		case cmdMemTest:	return CmdMemTest(pOut, pCmd);		
		case cmdMount:		return CmdMount(pOut, pCmd);		
		case cmdPing:		return CmdPing(pOut, pCmd);		
		case cmdPwd:		return CmdPwd(pOut, pCmd);		
		case cmdRemoveDir:	return CmdRemoveDir(pOut, pCmd);	
		case cmdReset:		return CmdReset(pOut, pCmd);		
		case cmdSNTP:		return CmdSNTP(pOut, pCmd);		
		case cmdStart:		return CmdStart(pOut, pCmd);		
		case cmdStop:		return CmdStop(pOut, pCmd);		
		case cmdTime:		return CmdTime(pOut, pCmd);		
		case cmdTraps:		return CmdTraps(pOut, pCmd);		
		case cmdType:		return CmdType(pOut, pCmd);		
		case cmdUnmount:	return CmdUnmount(pOut, pCmd);		
		case cmdUpTime:		return CmdUpTime(pOut, pCmd);		
		case cmdWho:		return CmdWho(pOut, pCmd);		
		}

	return 0;
	}

// Registration

void CDebugCmds::AddDiagCmds(void)
{
	AfxGetObject("aeon.diagmanager", 0, IDiagManager, m_pDiag);

	if( m_pDiag ) {

		m_uProv = m_pDiag->RegisterProvider(this, "");

		m_pDiag->RegisterCommand(m_uProv, cmdAuto,	 "auto");	
		m_pDiag->RegisterCommand(m_uProv, cmdType,	 "cat");	
	//	m_pDiag->RegisterCommand(m_uProv, cmdCFTest,	 "cftest");	
		m_pDiag->RegisterCommand(m_uProv, cmdChangeDir,	 "cd");
		m_pDiag->RegisterCommand(m_uProv, cmdChkDsk,	 "chkdsk");	
		m_pDiag->RegisterCommand(m_uProv, cmdClearDbase, "cleardb");
		m_pDiag->RegisterCommand(m_uProv, cmdClearGMC,	 "cleargmc");
		m_pDiag->RegisterCommand(m_uProv, cmdCrash,	 "crash");	
		m_pDiag->RegisterCommand(m_uProv, cmdCrashDump,	 "crashdump");
		m_pDiag->RegisterCommand(m_uProv, cmdCycle,	 "cycle");	
		m_pDiag->RegisterCommand(m_uProv, cmdDelete,	 "delete");	
		m_pDiag->RegisterCommand(m_uProv, cmdDir,	 "dir");		
		m_pDiag->RegisterCommand(m_uProv, cmdFakeTouch,	 "faketouch");
		m_pDiag->RegisterCommand(m_uProv, cmdFormat,	 "format");	
		m_pDiag->RegisterCommand(m_uProv, cmdFRAMDump,	 "framdump");
		m_pDiag->RegisterCommand(m_uProv, cmdHello,	 "hello");
		m_pDiag->RegisterCommand(m_uProv, cmdHexDump,	 "hd");
		m_pDiag->RegisterCommand(m_uProv, cmdDir,	 "ls");		
		m_pDiag->RegisterCommand(m_uProv, cmdMakeDir,	 "mkdir");
	//	m_pDiag->RegisterCommand(m_uProv, cmdMemCheck,	 "memcheck");
	//	m_pDiag->RegisterCommand(m_uProv, cmdMemStat,	 "memstat");
		m_pDiag->RegisterCommand(m_uProv, cmdMemTest,	 "memtest");
		m_pDiag->RegisterCommand(m_uProv, cmdMount,	 "mount");	
		m_pDiag->RegisterCommand(m_uProv, cmdPing,	 "ping");	
		m_pDiag->RegisterCommand(m_uProv, cmdPwd,	 "pwd");
		m_pDiag->RegisterCommand(m_uProv, cmdRemoveDir,	 "rmdir");
	//	m_pDiag->RegisterCommand(m_uProv, cmdReset,	 "reset");	
		m_pDiag->RegisterCommand(m_uProv, cmdSNTP,	 "sntp");	
		m_pDiag->RegisterCommand(m_uProv, cmdStart,	 "start");	
		m_pDiag->RegisterCommand(m_uProv, cmdStop,	 "stop");	
		m_pDiag->RegisterCommand(m_uProv, cmdTime,	 "time");	
		m_pDiag->RegisterCommand(m_uProv, cmdTraps,	 "traps");	
		m_pDiag->RegisterCommand(m_uProv, cmdType,	 "type");	
		m_pDiag->RegisterCommand(m_uProv, cmdUnmount,	 "unmount");
		m_pDiag->RegisterCommand(m_uProv, cmdUpTime,	 "uptime");	
		m_pDiag->RegisterCommand(m_uProv, cmdWho,	 "who");
		}
	}

// Command Handlers

UINT CDebugCmds::CmdAuto(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		if( pCmd->FromConsole() ) {

			SystemAuto();

			return 0;
			}

		pOut->Error("only supported from system console");

		return 4;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdCFTest(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if 0

	if( !pCmd->GetArgCount() ) {

		if( CfTestReady() ) {

			DISK Disk;

			CfGetGeometry(Disk);

			UINT  c = Disk.uTotal;

			PBYTE p = New BYTE [ 512 ];

			UINT  a = 0;

			UINT  b = 0;

			UINT  t = 1;

			pOut->Print("Disk has %u (%8.8X) sectors or %u bytes\n\n", c, c, c*512);

			for( UINT n = 0; n < c; n++ ) {

				if( !(n % 256) ) {

					pOut->Print(".");
					}

				if( !CfVerify(n, 1) ) {

					pOut->Print("%8.8X: veri failed\n", n);

					if( ++b == 8 ) {
					
						break;
						}

					continue;
					}

				for( UINT r = 0; r < t; r++ ) {

					if( CfReadSector(n, p) ) {

						break;
						}
					}

				if( r == t ) {

					pOut->Print("%8.8X : read failed, ", n);
					}

				if( true ) {

					BOOL q = FALSE;

					BYTE x = BYTE(rand());

					memset(p, x, 512);

					if( CfWriteSector(n, p, TRUE) ) {

						/*pOut->Print("write ok, ");*/

						if( CfReadSector(n, p) ) {

							/*pOut->Print("read-back ok, ");*/

							for( UINT i = 0; i < 512; i++ ) {

								if( p[i] != x ) {

									break;
									}
								}

							if( i == 512 ) {

								/*pOut->Print("read-back checks\n");*/

								q = TRUE;
								}
							else
								pOut->Print("read-back wrong\n");
							}
						else
							pOut->Print("read-back failed\n");
						}
					else
						pOut->Print("write failed\n");
					
					if( !q && ++b == 8 ) {

						break;
						}

					continue;
					}

				a++;
				}

			pOut->Print("%s%u passed and %u failed\n", b ? "\n" : "", a, b);

			delete p;

			return 0;
			}
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdChangeDir(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		if( !chdir(pCmd->GetArg(0)) ) {

			char path[MAX_PATH];

			getcwd(path, sizeof(path));

			pOut->Print("cd: directory is %s\n", path);
							
			return 0;
			}

		pOut->Error("can't find directory");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdChkDsk(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	/* !!! ADD ME !!! */

	return 0;
	}

UINT CDebugCmds::CmdClearDbase(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		if( pCmd->FromConsole() ) {

			g_pDbase->Clear();

			return CmdReset(pOut, pCmd);
			}

		pOut->Error("only supported from system console");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdClearGMC(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		TrapAcknowledge();

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdCrash(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		Sleep(100);

		HostTrap(1);

		PVBYTE p1 = PVBYTE(0);

		*p1++;

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdCrashDump(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		// TODO -- This is not provided...

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdCycle(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		if( pCmd->FromConsole() ) {

			if( !SystemStop() ) {

				pOut->Error("timeout on stop");

				return 2;
				}

			if( !SystemStart() ) {

				pOut->Error("timeout on start");

				return 3;
				}

			return 0;
			}

		pOut->Error("only supported from system console");

		return 4;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdDelete(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		if( !unlink(pCmd->GetArg(0)) ) {

			return 0;
			}
	
		pOut->Error("can't open file");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdDir(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		char path[MAX_PATH];

		getcwd(path, sizeof(path));

		pOut->Print("Directory of %s\n\n", path);

		CAutoDirentList List;

		if( List.Scan(".") ) {

			for( int n = 0; n < List.GetCount(); n++ ) {

				dirent *pFile = List[n];

				char full[MAX_PATH];

				PathMakeAbsolute(full, pFile->d_name);

				struct stat s;

				stat(full, &s);

				struct tm *tm = gmtime(&s.st_mtime);

				pOut->Print("  %2.2d/%2.2d/%4.4d ", 1+tm->tm_mon, tm->tm_mday, 1900+tm->tm_year);
		
				pOut->Print("%2.2d:%2.2d  ", tm->tm_hour, tm->tm_min);

				if( pFile->d_type == DT_DIR ) {

					pOut->Print("%-12s ", "<DIR>");
					}
				else
					pOut->Print("%-12u ", DWORD(s.st_size));

				pOut->Print("%s\n", pFile->d_name);
				}
			}

		// TODO -- Print used and free space?

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}
	
UINT CDebugCmds::CmdFakeTouch(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	// TODO -- Do we need this? We can't abort it very easily
	// using this method. Best we could do would be to start
	// a thread for it, and then have another command to stop.

/*	if( !pCmd->GetArgCount() ) {

		UINT uCount = 0;

		for(;;) {

			UINT t1 = 10 + rand() % 190;

			UINT t2 = 10 + rand() % 190;

			if( TcpDebugRead(t1) == 0x1B ) {

				break;
				}
			else {
				CInput Input;
			
				Input.x.i.m_Type   = typeTouch;

				Input.x.i.m_State  = stateDown;

				Input.x.i.m_Local  = TRUE;

				Input.x.d.t.m_XPos = (rand() % DispGetCx()) / 4;

				Input.x.d.t.m_YPos = (rand() % DispGetCy()) / 4;

				Input.x.i.m_State  = stateDown;

				InputStore(Input.m_Ref);

				pOut->Print( "tfake: %8.8u - touch (%3u, %3u) - ",
					  ++uCount,
					  Input.x.d.t.m_XPos,
					  Input.x.d.t.m_YPos
					  );

				if( TcpDebugRead(t1) == 0x1B ) {

					Input.x.i.m_State  = stateUp;

					InputStore(Input.m_Ref);

					pOut->Print("aborted\n");

					break;
					}
				else {
					Input.x.i.m_State  = stateUp;

					InputStore(Input.m_Ref);

					pOut->Print("release\n");
					}
				}
			}

		pOut->Print("tfake: done\n");

		return 0;
		}

*/	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdFormat(IDiagOutput *pOut, IDiagCommand *pCmd)
{
/*	if( pCmd->GetArgCount() == 1 ) {

		PCTXT p = pCmd->GetArg(0);

		if( strlen(p) == 1 ) {
			
			if( isalpha(p[0]) ) {

				UINT iDisk = m_pDiskMan->FindDisk(p[0]);

				if( iDisk < NOTHING ) {

					if( m_pDiskMan->FormatDisk(iDisk) ) {

						pOut->Print("format operation in progress\n");

						// TODO -- Wait for completion here?

						return 0;
						}

					pOut->Print("operation failed");

					return 2;
					}

				pOut->Print("no such disk");

				return 2;
				}
			}
		}

*/	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdFRAMDump(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() >= 0 && pCmd->GetArgCount() <= 2 ) {

		UINT uInit = 0;

		UINT uSize = FRAMGetSize();

		if( ParseRange(pCmd, uInit, uSize, 0, 256) ) {

			PBYTE pData = New BYTE [ uSize ];

			FRAMGetData(uInit, pData, uSize);

			pOut->Dump(pData, uSize, uInit);

			delete [] pData;

			return 0;
			}
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdHello(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	pOut->Print("Crimson 3.2 Debug Console\n");

	pOut->Print("Time is ");

	ShowTime(pOut);

	pOut->Print("\n");

	return 0;
	}

UINT CDebugCmds::CmdHexDump(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() >= 0 && pCmd->GetArgCount() <= 3 ) {

		CAutoFile File(pCmd->GetArg(pCmd->GetArgCount() - 1), "rb");

		if( File ) {

			UINT uInit = 0;

			UINT uSize = File.GetSize();

			if( ParseRange(pCmd, uInit, uSize, 0, 256) ) {

				CAutoArray<BYTE> Data(uSize);

				File.Seek(uInit);

				if( File.Read(Data, uSize) == uSize ) {

					File.Close();

					pOut->Dump(Data, uSize, uInit);

					return 0;
					}

				pOut->Error("unable to read file");

				return 3;
				}

			pOut->Error(NULL);

			return 1;
			}

		pOut->Error("unable to open file");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdMakeDir(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		if( !mkdir(pCmd->GetArg(0), 0) ) {

			pOut->Print("mkdir: directory created\n");
							
			return 0;
			}

		pOut->Error("can't make directory");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdMemCheck(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if 0

	if( !pCmd->GetArgCount() ) {

		// TODO -- Should use pOut

		extern void AfxCheckMemory(void);

		AfxCheckMemory();

		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdMemStat(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if 0

	if( !pCmd->GetArgCount() ) {

		// TODO -- Is this right?

		extern void AfxCheckMemory(void);

		AfxCheckMemory();

		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdMemTest(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		// TODO -- This doesn't do much these days...

		CmdMemStat(pOut, pCmd);

		UINT  uCount = 1;

		PBYTE *pData = New PBYTE [ uCount ];

		for( UINT a = 0; a < uCount; a++ ) {

			pData[a] = New BYTE [ 1024 ];

			memset(pData[a], BYTE(a), 1024);
			}

		CmdMemStat(pOut, pCmd);

		for( UINT b = 0; b < uCount; b++ ) {

			for( UINT i = 0; i < 1024; i++ ) {

				if( pData[b][i] != BYTE(b) ) {

					pOut->Print("Error at %p\n", pData[b] + i);

					break;
					}
				}

			memset(pData[b], 0, 1024);

			delete [] pData[b];
			}

		delete [] pData;

		CmdMemStat(pOut, pCmd);

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdMount(IDiagOutput *pOut, IDiagCommand *pCmd)
{
/*	if( !pCmd->GetArgCount() ) {

		m_pDiskMan->CheckDisk(m_pDiskMan->FindDisk('C'));

		m_pDiskMan->CheckDisk(m_pDiskMan->FindDisk('D'));

		return 0;
		}

*/	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdPing(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		if( g_pRouter ) {

			// TODO -- Better version with name resolution?

			CIPAddr Ip(pCmd->GetArg(0));

			CString St(Ip.GetAsText());

			UINT uTotal = 0;

			UINT uCount = 0;

			for( UINT n = 0; n < 4; n++ ) {

				UINT uTime = g_pRouter->Ping(Ip, 5000);

				if( uTime == NOTHING ) {

					if( !uCount ) {
						
						pOut->Print("ping: no reply\n");

						break;
						}

					pOut->Print("Reply from %s : LOST\n", PCTXT(St));
					}
				else {
					pOut->Print("Reply from %s : %5ums\n", PCTXT(St), uTime);

					uTotal += uTime;

					uCount += 1;
					}

				Sleep(50);
				}

			if( n == 4 ) {

				pOut->Print("\nAverage time is %ums\n", uTotal / uCount);
				}

			return 0;
			}

		pOut->Error("ip not enabled\n");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdPwd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		char path[MAX_PATH];

		getcwd(path, sizeof(path));

		pOut->Print("pwd: directory is %s\n", path);

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdRemoveDir(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		if( !rmdir(pCmd->GetArg(0)) ) {

			pOut->Print("rmdir: directory removed\n");
							
			return 0;
			}

		pOut->Error("can't remove directory");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdReset(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if 0

	if( !pCmd->GetArgCount() ) {

		if( pCmd->FromConsole() ) {

			Critical(TRUE);
		
			NICTerm();

			DriveTerm();

			for(;;) HostMaxIPL();

			return 0;
			}

		pOut->Error("only supported from system console");

		return 2;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdSNTP(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 0 || pCmd->GetArgCount() == 1 ) {

		if( g_pRouter ) {

			// TODO -- Better version with name resolution?

			CIPAddr IP;

			INT     TZ;

			if( pCmd->GetArgCount() == 0 ) {

				DWORD Data = g_pRouter->GetOption(42);

				if( !Data ) {

					pOut->Error("dhcp cannot provide time server");

					return 2;
					}

				IP = IPREF(Data);

				TZ = g_pRouter->GetOption(2);
				}
			else {
				IP = pCmd->GetArg(0);

				TZ = -5 * 60 * 60;
				}

			ISocket *pSocket = g_pRouter->CreateSocket(IP_UDP);

			pSocket->Connect(IP, 123);

			BYTE bData[48];

			UINT uSize = sizeof(bData);

			memset(bData, 0, sizeof(bData));

			bData[0] = 0x1B;

			pSocket->Send(bData, uSize);

			SetTimer(1000);

			for(;;) {

				if( !GetTimer() ) {

					pOut->Error("no reply");

					break;
					}

				UINT uSize = sizeof(bData);

				if( pSocket->Recv(bData, uSize) == S_OK ) {

					DWORD t = MotorToHost(PDWORD(bData)[10]);

					t -= (97U * 365U + 24) * 24U * 60U * 60U;

					t += TZ;

					CTime Time;

					Time.uSeconds = GetSec(t);
					Time.uMinutes = GetMin(t);
					Time.uHours   = GetHour(t);
					Time.uDate    = GetDate(t);
					Time.uMonth   = GetMonth(t);
					Time.uYear    = GetYear(t);

					SetTime(Time);

					SystemTimeChange();

					pOut->Print(	"Time set to %2.2u/%2.2u/%2.2u %2.2u:%2.2u:%2.2u\n",
							Time.uMonth,
							Time.uDate,
							Time.uYear % 100,
							Time.uHours,
							Time.uMinutes,
							Time.uSeconds
							);

					break;
					}

				Sleep(10);
				}

			pSocket->Close();

			pSocket->Release();

			return 0;
			}

		pOut->Error("ip not enabled\n");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdStart(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		if( pCmd->FromConsole() ) {

			if( !SystemStart() ) {

				pOut->Error("timeout on start");

				return 2;
				}
			
			return 0;
			}

		pOut->Error("only supported from system console");

		return 4;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdStop(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		if( pCmd->FromConsole() ) {

			if( !SystemStop() ) {

				pOut->Error("timeout on stop");

				return 2;
				}
			
			return 0;
			}

		pOut->Error("only supported from system console");

		return 4;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdTime(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->Print("Time is ");

		ShowTime(pOut);

		pOut->Print("\n\nBattery is %s\n", IsBatteryOK() ? "OKAY" : "BAD");

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdTraps(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		// TODO -- Is this obsolete?

		pOut->Print("Last seven resets:\n\n");

		for( UINT n = 1; n < 8; n++ ) {

			DWORD dwData[4];

			pOut->Print("  %u)  ", n);

			if( TrapGetData(n, dwData) ) {

				DWORD Time = dwData[1];

				pOut->Print( "%2.2u/%2.2u/%2.2u %2.2u:%2.2u:%2.2u  ",
					  GetMonth(Time),
					  GetDate (Time),
					  GetYear (Time) % 100,
					  GetHour (Time),
					  GetMin  (Time),
					  GetSec  (Time)
					  );

				if( !dwData[3] ) {

					pOut->Print("Software Reset");
					}
				else {
					UINT  uType  = ((dwData[2] >> 18) & 0xFF);

					DWORD dwAddr = dwData[3];

					pOut->Print("Exception 0x%2.2X at %8.8X", uType, dwAddr);
					}
				}
			else {
				pOut->Print("00/00/00 00:00:00  ");
			
				pOut->Print("Hardware Reset");
				}

			pOut->Print("\n");
			}

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdType(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() >= 0 && pCmd->GetArgCount() <= 3 ) {
		
		CAutoFile File(pCmd->GetArg(pCmd->GetArgCount() - 1), "rb");

		if( File ) {

			UINT uData = File.GetSize();

			if( uData < 512 * 1024 ) {

				CAutoArray<char> Data(uData+2);

				if( File.Read(Data, uData) == uData ) {

					File.Close();

					Data[uData+0] = '\n';

					Data[uData+1] = 0;

					CArray <char *> Lines;

					for( char *pScan = Data; pScan < Data + uData; ) {

						char *pFind = strchr(pScan, '\n');
							
						if( pFind - pScan > 200 ) {

							pOut->Error("lines too long or file not ASCII");

							return 2;
							}

						if( pFind > pScan && pFind[-1] == '\r' ) {

							pFind[-1] = 0;
							}
						else
							*pFind = 0;

						Lines.Append(pScan);

						pScan = pFind + 1;
						}

					UINT uInit = 0;

					UINT uRead = Lines.GetCount();

					if( ParseRange(pCmd, uInit, uRead, 1, 20) ) {

						UINT d = 0;

						for( UINT x = uInit+uRead; x; x /= 10 ) {

							d++;
							}

						if( uInit ) {

							pOut->Print("(earlier data skipped)\n");
							}

						for( UINT n = uInit; n < uInit+uRead; n++ ) {

							pOut->Print("%*.*u: %s\n", d, d, n+1, Lines[n]);
							}

						if( uInit+uRead < Lines.GetCount() ) {

							pOut->Print("(further data skipped)\n");
							}

						return 0;
						}

					pOut->Error(NULL);

					return 1;
					}
				}

			pOut->Error("file is too long");

			return 4;
			}

		pOut->Error("unable to open file");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdUnmount(IDiagOutput *pOut, IDiagCommand *pCmd)
{
/*	if( !pCmd->GetArgCount() ) {

		if( FindFileSystem() ) {

			m_pDiskMan->EjectDisk(m_pFileSys->GetDisk());
			}
	
		FreeFileSystem();

		return 0;
		}

*/	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdUpTime(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		DWORD Time = GetNow() - m_dwStart;

		UINT  Days = Time / 24 / 60 / 60;

		PCTXT pEnd = (Days == 1) ? "" : "s";

		pOut->Print("Time is ");

		ShowTime(pOut);

		pOut->Print(	"\n\nUp for %u day%s, %2.2u:%2.2u:%2.2u\n",
				Days, pEnd,
				GetHour(Time),
				GetMin (Time),
				GetSec (Time)
				);

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CDebugCmds::CmdWho(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		char sModel[32];

		WhoGetModel(sModel);
	
		pOut->Print("Name    : %s\n", WhoGetName(FALSE));

		pOut->Print("Generic : %s\n", WhoGetName(TRUE));

		pOut->Print("Model   : %s\n", sModel);

		pOut->Print("Keys    : %u\n", WhoGetKeys());

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

// Implementation

BOOL CDebugCmds::ParseRange(IDiagCommand *pCmd, UINT &uInit, UINT &uSize, UINT uBase, UINT uDefault)
{
	// Possible syntaxes...
	//
	// b		Display default number of entries from b
	// +n		Display first n entries
	// -n		Display last n entries
	// b n		Display n entries from b
	// b+n		Display n entries from b
	// b-n		Display entries from b to n inclusive

	UINT uLimit = uSize;

	UINT uArgs  = pCmd->GetArgCount();

	if( uArgs == 1 ) {

		uInit = uBase;

		uSize = uDefault;
		}
	else {
		CString a0 = (uArgs >= 2) ? pCmd->GetArg(0) : "";

		CString a1 = (uArgs >= 3) ? pCmd->GetArg(1) : "";

		if( uArgs == 2 ) {

			char const *s = "-+";

			for( int n = 0; s[n]; n++ ) {

				UINT f = a0.Find(s[n]);

				if( f && f < NOTHING ) {

					a1 = a0.Mid (f);

					a0 = a0.Left(f);

					uArgs++;

					break;
					}
				}
			}

		if( uArgs == 2 ) {

			if( a0.StartsWith("+") ) {

				uInit = uBase;

				uSize = ParseNumber(a0.Mid(1));
				}
			else {
				if( a0.StartsWith("-") ) {

					uSize = ParseNumber(a0.Mid(1));

					MakeMin(uSize, uLimit);

					uInit = uLimit - uSize;

					return TRUE;
					}

				uInit = ParseNumber(a0);

				uSize = uDefault;

				MakeMax(uInit, uBase);

				MakeMin(uInit, uBase + uLimit - 1);
				}
			}

		if( uArgs == 3 ) {

			uInit = ParseNumber(a0);

			MakeMax(uInit, uBase);

			MakeMin(uInit, uBase + uLimit - 1);

			if( a1.StartsWith("-") ) {

				uSize = 1 + ParseNumber(a1.Mid(1));

				if( uSize <= uInit ) {

					return FALSE;
					}

				uSize -= uInit;
				}
			else {
				if( a1.StartsWith("+") ) {

					uSize = ParseNumber(a1.Mid(1));
					}
				else 
					uSize = ParseNumber(a1);
				}
			}
		}

	uInit -= uBase;

	MakeMin(uSize, uLimit - uInit);

	return TRUE;
	}

UINT CDebugCmds::ParseNumber(CString const &Text)
{
	if( Text.StartsWith("0x") ) {

		return strtoul(PCSTR(Text)+2, NULL, 16);
		}

	return strtoul(Text, NULL, 10);
	}

void CDebugCmds::ShowTime(IDiagOutput *pOut)
{
	DWORD Time = GetNow();

	pOut->Print(	"%2.2u/%2.2u/%2.2u %2.2u:%2.2u:%2.2u",
			GetMonth(Time),
			GetDate (Time),
			GetYear (Time) % 100,
			GetHour (Time),
			GetMin  (Time),
			GetSec  (Time)
			);
	}

// End of File
