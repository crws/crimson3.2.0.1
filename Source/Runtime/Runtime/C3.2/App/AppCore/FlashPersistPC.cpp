
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flash Persistance Manager
//

class CFlashPersistPC : public IPersist
{
	public:
		// Constructor
		CFlashPersistPC( UINT uDramTop,
			       UINT uPageCount,
			       UINT uPageStart,
			       UINT uBankStart
			       );

		// IPersist
		void  Init(void);
		void  Term(void);
		void  ByeBye(void);
		BOOL  IsDirty(void);
		PVOID GetBase(void);
		void  Commit(void);
		BYTE  GetByte(DWORD dwAddr);
		WORD  GetWord(DWORD dwAddr);
		LONG  GetLong(DWORD dwAddr);
		void  GetData(PBYTE pData, DWORD dwAddr, UINT uCount);
		void  PutByte(DWORD dwAddr, BYTE bData);
		void  PutWord(DWORD dwAddr, WORD wData);
		void  PutLong(DWORD dwAddr, LONG lData);
		void  PutData(PBYTE pData, DWORD dwAddr, UINT uCount);

	protected:
		// Constants
		static UINT const C64K = 65536;

		// Bank Structure
		struct BANK
		{
			PBYTE	m_pData;
			PBYTE	m_pSig;
			BOOL	m_fDirty;
			UINT	m_uPage;
			UINT    m_uUsed;
			};

		// Data Members
		UINT m_uDramTop;
		UINT m_uPageCount;
		UINT m_uPageStart;
		UINT m_uBankStart;
		UINT m_uBlocks;
		BANK m_Bank [4];
		BOOL m_fUsed[32];
		UINT m_uWalk;

		PBYTE		m_pData;
		win32::HANDLE	m_hFile;
		win32::HANDLE	m_hMap;

		// Implementation
		BOOL FindBank(UINT uBank, BOOL fCopy);
	};

//////////////////////////////////////////////////////////////////////////
//
// Flash Persistance Manager
//

// Instantiator

global IPersist * Create_FlashPersistPC(UINT uDramTop, UINT uPageCount, UINT uPageStart, UINT uBankStart)
{
	return New CFlashPersistPC(uDramTop, uPageCount, uPageStart, uBankStart);
	}

// Constructor

CFlashPersistPC::CFlashPersistPC(UINT uDramTop, UINT uPageCount, UINT uPageStart, UINT uBankStart)
{
	}

// IPersist

void CFlashPersistPC::Init(void)
{
	char sFile[MAX_PATH];

	strcpy(sFile, ProcGetPath());

	strcat(sFile, "ra.bin");

	m_hFile = win32::CreateFile(		sFile,
						GENERIC_READ | GENERIC_WRITE,
						FILE_SHARE_READ,
						NULL,
						OPEN_ALWAYS,
						FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING,
						NULL
						);

	m_hMap  = win32::CreateFileMapping(	m_hFile,
						NULL,
						PAGE_READWRITE,
						0,
						0x40000,
						NULL
						);

	m_pData = PBYTE(win32::MapViewOfFile(	m_hMap,
						FILE_MAP_WRITE,
						0,
						0,
						0x40000
						));

	for( UINT uPage = 0; uPage < elements(m_fUsed); uPage++ ) {

		m_fUsed[uPage] = FALSE;
		}

	for( UINT uBank = 0; uBank < elements(m_Bank ); uBank++ ) {

		BANK &Bank    = m_Bank[uBank];

		Bank.m_pData  = m_pData + uBank * 0x10000;

		Bank.m_pSig   = Bank.m_pData + 64100;

		Bank.m_fDirty = FALSE;

		Bank.m_uPage  = NOTHING;

		BYTE bGuid[16];

		g_pDbase->GetVersion(bGuid);

		if( memcmp(Bank.m_pData, bGuid, 16) ) {

			if( !FindBank(uBank, TRUE) ) {

				memset(Bank.m_pData, 0, 0x10000);

				memcpy(Bank.m_pData, bGuid, 16);

				Bank.m_pSig[0] = uBank;

				Bank.m_fDirty  = TRUE;
				}
			}
		else {
			FindBank(uBank, FALSE);

			Bank.m_pSig[0] = uBank;

			Bank.m_fDirty  = TRUE;
			}
		}

	m_uWalk = rand() % elements(m_fUsed);

	//ByeByeSetHook(ByeBye);
	}

void CFlashPersistPC::Term(void)
{
	win32::UnmapViewOfFile(m_pData);

	win32::CloseHandle(m_hMap);

	win32::CloseHandle(m_hFile);
	}

void CFlashPersistPC::ByeBye(void)
{
	(PDWORD(m_Bank[0].m_pData)[0]) ^= NOTHING;
	
	(PDWORD(m_Bank[1].m_pData)[0]) ^= NOTHING;
	
	(PDWORD(m_Bank[2].m_pData)[0]) ^= NOTHING;
	
	(PDWORD(m_Bank[3].m_pData)[0]) ^= NOTHING;
	
	BYTE bData[2] = { 0x00, 0x00 };

	FRAMPutData(Mem(Loading), bData, 2);
	}

BOOL CFlashPersistPC::IsDirty(void)
{
	for( UINT uBank = 0; uBank < elements(m_Bank); uBank++ ) {
		
		if( m_Bank[uBank].m_fDirty ) {

			return TRUE;
			}
		}

	return FALSE;
	}

PVOID CFlashPersistPC::GetBase(void)
{
	return m_Bank[elements(m_Bank)-1].m_pData;
	}

void CFlashPersistPC::Commit(void)
{
	for( UINT uBank = 0; uBank < elements(m_Bank); uBank++ ) {
		
		BANK &Bank = m_Bank[uBank];

		if( Bank.m_fDirty ) {

			UINT uPrev = Bank.m_uPage;

			UINT uPage;

			do {
				uPage   = m_uWalk;

				m_uWalk = (m_uWalk + 1) % elements(m_fUsed);
				
				} while( m_fUsed[uPage] );

			BYTE bData[16];

			memset(bData, 0, 16);

			g_pProm->BatchStart();

			g_pProm->QueueWrite(uPage, 0, bData, 16);

			g_pProm->QueueErase(uPage);

			g_pProm->QueueWrite(uPage, 0, Bank.m_pData, 0x10000);

			if( uPrev < NOTHING ) {

				g_pProm->QueueWrite(uPrev, 0, bData, 16);

				m_fUsed[uPrev] = FALSE;
				}

			g_pProm->BatchEnd();

			Bank.m_fDirty = FALSE;

			Bank.m_uPage  = uPage;

			m_fUsed[uPage] = TRUE;
			}
		}
	}

BYTE CFlashPersistPC::GetByte(DWORD dwAddr)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	BANK  &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	return *PBYTE(pPage);
	}

WORD CFlashPersistPC::GetWord(DWORD dwAddr)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	BANK  &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	return *PWORD(pPage);
	}

LONG CFlashPersistPC::GetLong(DWORD dwAddr)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	BANK  &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	return *PLONG(pPage);
	}

void CFlashPersistPC::GetData(PBYTE pData, DWORD dwAddr, UINT uCount)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	BANK  &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	memcpy(pData, pPage, uCount);
	}

void CFlashPersistPC::PutByte(DWORD dwAddr, BYTE bData)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	BANK  &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	if( *PBYTE(pPage) != bData ) {

		*PBYTE(pPage) = bData;

		Bank.m_fDirty = TRUE;
		}
	}

void CFlashPersistPC::PutWord(DWORD dwAddr, WORD wData)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	BANK  &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	if( *PWORD(pPage) != wData ) {

		*PWORD(pPage) = wData;

		Bank.m_fDirty = TRUE;
		}
	}

void CFlashPersistPC::PutLong(DWORD dwAddr, LONG lData)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	BANK  &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	if( *PLONG(pPage) != lData ) {

		*PLONG(pPage) = lData;

		Bank.m_fDirty = TRUE;
		}
	}

void CFlashPersistPC::PutData(PBYTE pData, DWORD dwAddr, UINT uCount)
{
	UINT  uBank = HIWORD(dwAddr) - 1;

	BANK  &Bank = m_Bank[uBank];

	PBYTE pPage = Bank.m_pData + 16 + LOWORD(dwAddr);

	if( memcmp(pPage, pData, uCount) ) {

		memcpy(pPage, pData, uCount);

		Bank.m_fDirty = TRUE;
		}
	}

BOOL CFlashPersistPC::FindBank(UINT uBank, BOOL fCopy)
{
	for( UINT uPage = 0; uPage < elements(m_Bank) + 1; uPage++ ) {

		PBYTE pPage = g_pProm->GetAddr(uPage);

		BYTE bGuid[16];

		g_pDbase->GetVersion(bGuid);

		if( !memcmp(pPage, bGuid, 16) ) {

			if( pPage[64100] == uBank ) {

				BANK &Bank = m_Bank[uBank];

				if( fCopy ) {

					memcpy(Bank.m_pData + 0x10, pPage + 0x10, 0xFFF0);

					memcpy(Bank.m_pData + 0x00, pPage + 0x00, 0x0010);
					}

				Bank.m_uPage   = uPage;

				m_fUsed[uPage] = TRUE;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// End of File
