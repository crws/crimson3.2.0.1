
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Runtime
//
// Copyright (c) 1993-2001 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <c3core.hpp>

#include <g3ctrl.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Application Headers
//

#include "g3slave.hpp"

#include "global.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Database Version
//

/*
#include "../../../../../../version/dbver.hpp"
*/

//////////////////////////////////////////////////////////////////////////
//
// GDI Creation
//

extern IGDI * Create_GDI(void);

//////////////////////////////////////////////////////////////////////////
//
// USB Driver
//

extern IUsbFuncEvents * Create_UsbLink(UINT uPriority, ILinkService *pService);
	
//////////////////////////////////////////////////////////////////////////
//
// USB Manager
//

extern interface IUsbManager * Create_UsbManager(void);
	
//////////////////////////////////////////////////////////////////////////
//
// G3Link Transports
//

extern void Create_XPSerial (UINT uPriority, UINT uPort, ILinkService *pService, IGrabber *pGrabber);

extern void Create_XPSerial (UINT uPriority, ILinkService *pService, IGrabber *pGrabber);

extern void Create_XPNetwork(UINT uPriority, ILinkService *pService);

extern void Create_XPUSB    (UINT uPriority, ILinkService *pService, PCTXT pID, WORD wID);

extern BOOL Delete_XPSerial (void);

extern BOOL Delete_XPNetwork(void);

extern BOOL Purge_XPNetwork(void);

//////////////////////////////////////////////////////////////////////////
//
// Boot Version
//

extern UINT AppGetBootVersion(void);

// End of File

#endif
