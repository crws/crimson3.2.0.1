
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_License_HPP

#define INCLUDE_License_HPP

//////////////////////////////////////////////////////////////////////////
//
// License Manager
//

class CLicense : public ILicense
{
	public:
		// Constructor
		CLicense(void);

		// ILicense
		void Install(UINT LicenseId);
		void Uninstall(UINT LicenseId);
		BOOL Validate(UINT LicenseId);

	protected:
		// Data
		UINT m_uLicenses[8];

		// Implementation
		UINT FindLicense(UINT LicenseId);
		BOOL CheckPlatform(UINT LicenseId);
		BOOL CheckControlLicense(void);
	};

// End of File

#endif
