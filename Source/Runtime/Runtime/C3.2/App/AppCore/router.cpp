
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "g3slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Request Router Service
//

class CRouterService : public ILinkService
{
	public:
		// Constructor
		CRouterService(void);

		// ILinkService
		void Timeout(void);
		UINT Process(CLinkFrame &Req, CLinkFrame &Rep);
		void EndLink(CLinkFrame &Req);

	protected:
		// Data Members
		ILinkService *m_p[5];
	};

//////////////////////////////////////////////////////////////////////////
//
// Request Router Service
//

global	ILinkService *	Create_RouterService(void)
{
	return New CRouterService;
	}

// Constructor

CRouterService::CRouterService(void)
{
	m_p[0] = Create_ConfigService();

	m_p[1] = Create_BootService();

	m_p[2] = Create_TunnelService();

	m_p[3] = Create_DataService();

	m_p[4] = Create_StratonService();
	}

// ILinkService

void CRouterService::Timeout(void)
{
	m_p[0]->Timeout();

	m_p[1]->Timeout();

	m_p[2]->Timeout();

	m_p[3]->Timeout();

	m_p[4]->Timeout();
	}

UINT CRouterService::Process(CLinkFrame &Req, CLinkFrame &Rep)
{
	switch( Req.GetService() ) {

		case servConfig:
			return m_p[0]->Process(Req, Rep);
		
		case servBoot:
			return m_p[1]->Process(Req, Rep);

		case servTunnel:
			return m_p[2]->Process(Req, Rep);

		case servData:
			return m_p[3]->Process(Req, Rep);

		case servStraton:
			return m_p[4]->Process(Req, Rep);
		}

	return procError;
	}

void CRouterService::EndLink(CLinkFrame &Req)
{
	m_p[0]->EndLink(Req);

	m_p[1]->EndLink(Req);

	m_p[2]->EndLink(Req);

	m_p[3]->EndLink(Req);

	m_p[4]->EndLink(Req);
	}

// End of File
