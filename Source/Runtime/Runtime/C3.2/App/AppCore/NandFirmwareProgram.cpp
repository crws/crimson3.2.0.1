
#include "intern.hpp"

#include "NandFirmwareProgram.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Nand Firmware Programming Object
//

#if defined(_M_IMX51)

// System

clink	BYTE	_log_chain;

#endif

// Externals

extern	void	ChainCode(PCBYTE pImage, UINT uImage, UINT uParam);

// Instantiator

global IFirmwareProgram * Create_NandFirmwareProgram(UINT uStart, UINT uEnd)
{
	return New CNandFirmwareProgram(uStart, uEnd);
	}

// Constructor

CNandFirmwareProgram::CNandFirmwareProgram(UINT uStart, UINT uEnd) : CNandFirmwareProps(uStart, uEnd)
{
	m_uPtr   = 0;

	m_fWrite = false;
	}

// Destructor

CNandFirmwareProgram::~CNandFirmwareProgram(void)
{
	}

// IFirmwareProps

bool CNandFirmwareProgram::IsCodeValid(void)
{
	return CNandFirmwareProps::IsCodeValid();
	}

PCBYTE CNandFirmwareProgram::GetCodeVersion(void)
{
	return CNandFirmwareProps::GetCodeVersion();
	}

UINT CNandFirmwareProgram::GetCodeSize(void)
{
	return CNandFirmwareProps::GetCodeSize();
	}

PCBYTE CNandFirmwareProgram::GetCodeData(void)
{
	return CNandFirmwareProps::GetCodeData();
	}

// IFirmwareProgram

bool CNandFirmwareProgram::ClearProgram(UINT uBlocks)
{
	CNandBlock Block  = m_BlockStart;

	UINT       uCount = 0;

	SkipBadBlocks(Block);

	for(;;) {

		if( EraseBlock(Block, true) ) {

			if( !uCount++ ) {

				m_PageStart   = Block;

				m_PageCurrent = Block;

				m_uPtr        = 0;

				m_uImage      = 0;

				m_fValid      = false;

				m_fWrite      = true;

				m_PageCurrent.m_uPage++;
				}
			}

		if( !GetNextGoodBlock(Block) ) {

			break;
			}
		}

	return 2 * uCount >= uBlocks;
	}

bool CNandFirmwareProgram::WriteProgram(PCBYTE pData, UINT uCount)
{
	if( m_fWrite ) {

		AllocImage();

		while( uCount ) {

			UINT uSpace = m_uPageSize - m_uPtr;

			UINT uCopy  = Min(uCount, uSpace);

			memcpy(m_pPageData + m_uPtr, pData, uCopy);

			if( (m_uPtr += uCopy) == m_uPageSize ) {

				CNandPage Page;

				if( !WriteWithReloc(Page) ) {

					return false;
					}

				memcpy(m_pImage + m_uImage, m_pPageData, m_uPtr);

				m_uImage += m_uPtr;

				m_uPtr    = 0;
				}

			pData  += uCopy;

			uCount -= uCopy;
			}
		
		return true;
		}

	return false;
	}

bool CNandFirmwareProgram::WriteVersion(PCBYTE pData)
{
	if( m_fWrite ) {

		if( FlushCode() ) {

			memset(m_pPageData, 0xFF, m_uPageSize);

			CHead * pHead   = (CHead *) m_pPageData;

			pHead->m_uImage = m_uImage;

			memcpy(pHead->m_bMagic, m_bMagic, 16);

			memcpy(pHead->m_bGuid, pData,     16);

			if( WritePage(m_PageStart, true) ) {

				m_fValid = true;

				m_fWrite = false;

				m_Head   = *pHead;

				return true;
				}

			MarkBlockBad(m_PageStart);
			}
		}

	return false;
	}

bool CNandFirmwareProgram::StartProgram(void)
{
	if( m_fValid ) {

		if( ReadCode() ) {

			PBYTE pExpand = New BYTE [ 24 * 1024 * 1024 ];

			UINT  uExpand = fastlz_decompress(m_pImage, m_uImage, pExpand);

			if( uExpand > 0 ) {

				ChainCode(pExpand, uExpand, 0);
				
				return true;
				}			

			AfxTrace("!!!image decompression failed (%d) 0x%8.8X!!!\n", 
				  m_uImage,
				  m_pImage
				  );

			return false;
			}
		}

	return false;
	}

// Implementation

bool CNandFirmwareProgram::FlushCode(void)
{
	if( m_uPtr ) {

		CNandPage Page;

		if( !WriteWithReloc(Page) ) {

			return false;
			}

		memcpy(m_pImage + m_uImage, m_pPageData, m_uPtr);

		m_uImage += m_uPtr;

		m_uPtr    = 0;
		}

	return true;
	}

// End of File
