
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "NullDatabase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Null Database Manager
//

// Instantiator

global IDatabase * Create_NullDatabase(void)
{
	return New CNullDatabase;
	}

// Constructor

CNullDatabase::CNullDatabase(void)
{
	}

// IDatabase

void CNullDatabase::Init(void)
{
	}

void CNullDatabase::Clear(void)
{
	}

BOOL CNullDatabase::IsValid(void)
{
	return FALSE;
	}

void CNullDatabase::SetValid(BOOL fValid)
{
	}

void CNullDatabase::SetRunning(BOOL fRun)
{
	}

BOOL CNullDatabase::GarbageCollect(void)
{
	return TRUE;
	}

BOOL CNullDatabase::GetVersion(PBYTE pGuid)
{
	memset(pGuid, 0, 16);

	return TRUE;
	}

DWORD CNullDatabase::GetRevision(void)
{
	return 0;
	}

BOOL CNullDatabase::SetVersion(PCBYTE pData)
{
	return TRUE;
	}

BOOL CNullDatabase::SetRevision(DWORD Revision)
{
	return TRUE;
	}

BOOL CNullDatabase::CheckSpace(UINT uSize)
{
	return FALSE;
	}

BOOL CNullDatabase::CanCompress(void)
{
	return FALSE;
	}

BOOL CNullDatabase::WriteItem(CItemInfo const &Info, PCVOID pData)
{
	return FALSE;
	}

BOOL CNullDatabase::GetItemInfo(UINT uItem, CItemInfo &Info)
{
	return FALSE;
	}

PCVOID CNullDatabase::LockItem(UINT uItem, CItemInfo &Info)
{
	return NULL;
	}

PCVOID CNullDatabase::LockItem(UINT uItem)
{
	return NULL;
	}

void CNullDatabase::PendItem(UINT uItem, BOOL fPend)
{
	}

void CNullDatabase::LockPendingItems(BOOL fLock)
{
	}

void CNullDatabase::FreeItem(UINT uItem)
{
	}

// End of File
