
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NullDatabase_HPP

#define INCLUDE_NullDatabase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Null Database Manager
//

class CNullDatabase : public IDatabase
{
	public:
		// Constructor
		CNullDatabase(void);

		// IDatabase
		void   Init(void);
		void   Clear(void);
		BOOL   IsValid(void);
		void   SetValid(BOOL fValid);
		void   SetRunning(BOOL fRun);
		BOOL   GarbageCollect(void);
		BOOL   GetVersion(PBYTE pGuid);
		DWORD  GetRevision(void);
		BOOL   SetVersion(PCBYTE pData);
		BOOL   SetRevision(DWORD dwRes);
		BOOL   CanCompress(void);
		BOOL   CheckSpace(UINT uSize);
		BOOL   WriteItem(CItemInfo const &Info, PCVOID pData);
		BOOL   GetItemInfo(UINT uItem, CItemInfo &Info);
		PCVOID LockItem(UINT uItem, CItemInfo &Info);
		PCVOID LockItem(UINT uItem);
		void   PendItem(UINT uItem, BOOL fPend);
		void   LockPendingItems(BOOL fLock);
		void   FreeItem(UINT uItem);
	};

// End of File

#endif
