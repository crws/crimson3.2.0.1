
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandFlashEventStorage_HPP

#define	INCLUDE_NandFlashEventStorage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "..\..\G3Panel\G3Comms\g3comms.hpp"

#include "..\..\G3Panel\G3Comms\events.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include "CxNand.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Event Storage
//

class CNandFlashEventStorage : public CNandClient,
			       public IEvStg
{
	public:
		// Constructor
		CNandFlashEventStorage(CNandBlock const &Start, CNandBlock const &End);

		// Destructor
		~CNandFlashEventStorage(void);

		// IEvStg
		void Init(void);
		void BindManager(CEventManager *pManager);
		BOOL ReadMemory(CEventLogger *pLog);
		BOOL WriteMemory(CEventInfo const &Info);
		BOOL ClearMemory(void);

	protected:
		// Constants
		static DWORD const magicSlot     = 0x45564E54;
		static DWORD const magicEmpty	 = 0xFFFFFFFF;

		// Sector Header
		struct CSectorHeader
		{
			DWORD		Magic;
			GUID		Guid;
			DWORD	        Mark;
			};

		// Event Header
		struct CSlot
		{
			CSectorHeader m_Header;
			DWORD	      m_Time;
			DWORD	      m_Type   :2;
			DWORD         m_Source :3;
			DWORD         m_HasText:1;
			DWORD         m_Code   :26;
			WCHAR	      m_Text[0];
			};

		// Type Definitions
		typedef	CSectorHeader	      * PSECTOR;
		typedef	CSectorHeader   const * PCSECTOR;
		typedef	CSlot	              * PSLOT;
		typedef	CSlot           const * PCSLOT;

		// Data
		CEventManager * m_pManager;
		CNandSector	m_SectorHead;
		CNandSector	m_SectorTail;
		CNandSector	m_SectorCurrent;
		UINT            m_Next;
		BOOL		m_fFoundEmpty;

		// Write with Relocation
		BOOL WriteWithReloc(CNandSector &Sector);
		BOOL CopySectors(CNandSector &Dest, CNandSector &From, CNandSector const &Stop);

		// Queue
		BOOL IsEmpty(void);
		void FindQueue(void);
		BOOL FindNextBlock (CNandBlock  &Next);
		BOOL FindNextSector(CNandSector &Sector);
		void MarkSector(PSECTOR pSector);

		// Slot Helpers
		void SlotToInfo(CEventInfo &Info, PCSLOT pSlot);
		void InfoToSlot(PSLOT pSlot, CEventInfo const &Info);

		// Debug
		void DebugShowQueue(void);
	};

// End of File

#endif
