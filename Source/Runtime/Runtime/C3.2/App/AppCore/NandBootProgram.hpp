
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_NandBootProgram_HPP

#define INCLUDE_NandBootProgram_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "NandBootProps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Boot Programming Object
//

class CNandBootProgram : public CNandBootProps,
			 public IFirmwareProgram
{
	public:
		// Constructor
		CNandBootProgram(UINT uStart, UINT uEnd);

		// Destructor
		~CNandBootProgram(void);

		// IFirmwareProps
		bool   IsCodeValid(void);
		PCBYTE GetCodeVersion(void);
		UINT   GetCodeSize(void);
		PCBYTE GetCodeData(void);

		// IFirmwareProgram
		bool ClearProgram(UINT uBlocks);
		bool WriteProgram(PCBYTE pData, UINT uCount);
		bool WriteVersion(PCBYTE pData);
		bool StartProgram(void);

	protected:
		// Data Members
		UINT  m_uPtr;
		bool  m_fWrite;

		// Implementation
		bool WritePage(CNandPage Page);
	};

// End of File

#endif
