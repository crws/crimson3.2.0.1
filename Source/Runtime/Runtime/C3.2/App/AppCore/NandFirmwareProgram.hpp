
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_NandFirmwareProgram_HPP

#define INCLUDE_NandFirmwareProgram_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "NandFirmwareProps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Firmware Programming Object
//

class CNandFirmwareProgram : public CNandFirmwareProps,
			     public IFirmwareProgram
{
	public:
		// Constructor
		CNandFirmwareProgram(UINT uStart, UINT uEnd);

		// Destructor
		~CNandFirmwareProgram(void);

		// IFirmwareProps
		bool   IsCodeValid(void);
		PCBYTE GetCodeVersion(void);
		UINT   GetCodeSize(void);
		PCBYTE GetCodeData(void);

		// IFirmwareProgram
		bool ClearProgram(UINT uBlocks);
		bool WriteProgram(PCBYTE pData, UINT uCount);
		bool WriteVersion(PCBYTE pData);
		bool StartProgram(void);

	protected:
		// Data Members
		UINT  m_uPtr;
		bool  m_fWrite;

		// Implementation
		bool FlushCode(void);
	};

// End of File

#endif
