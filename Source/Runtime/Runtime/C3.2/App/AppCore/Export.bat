@echo off

rem %1 = project name
rem %2 = project path
rem %3 = output path
rem %4 = configuration
rem %5 = platform

copy global.hpp ..\..\..\..\include\c3.2 >nul

attrib -r ..\..\..\..\include\c3.2\*.*
