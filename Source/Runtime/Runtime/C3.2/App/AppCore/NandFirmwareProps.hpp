
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_NandFirmwareProps_HPP

#define INCLUDE_NandFirmwareProps_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include <CxNand.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Nand Firmware Properties Object
//

class CNandFirmwareProps : public CNandClient,
			   public IFirmwareProps
{
	public:
		// Constructor
		CNandFirmwareProps(UINT uStart, UINT uEnd);

		// Destructor
		~CNandFirmwareProps(void);

		// IFirmwareProps
		bool   IsCodeValid(void);
		PCBYTE GetCodeVersion(void);
		UINT   GetCodeSize(void);
		PCBYTE GetCodeData(void);

	protected:
		// Image Header
		struct CHead
		{
			BYTE m_bMagic[16];
			BYTE m_bGuid [16];
			UINT m_uImage;
			};

		// Data Members
		bool	  m_fValid;
		CHead	  m_Head;
		CNandPage m_PageStart;
		UINT      m_uAlloc;
		PBYTE	  m_pImage;
		UINT	  m_uImage;

		// Magic Number
		static BYTE m_bMagic[16];

		// Implementation
		bool AllocImage(void);
		void CheckCode(void);
		bool ReadCode(void);
	};

// End of File

#endif
