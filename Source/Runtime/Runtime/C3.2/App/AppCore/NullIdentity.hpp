
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NullIndentity_HPP

#define INCLUDE_NullIndentity_HPP

//////////////////////////////////////////////////////////////////////////
//
// Null Identity Manager
//

class CNullIdentity : public ICrimsonIdentity
{
	public:
		// Constructor
		CNullIdentity(void);

		// ICrimsonIdentity
		void Init(void);
		int  GetOemName(PTXT   pName, UINT uSize);
		int  GetOemApp (PTXT   pName, UINT uSize);
		bool GetKeyData(PBYTE  pData, UINT uSize);
		bool SetOemName(PCTXT  pName, UINT uSize);
		bool SetOemApp (PCTXT  pName, UINT uSize);
		bool SetKeyData(PCBYTE pData, UINT uSize);
	};

// End of File

#endif
