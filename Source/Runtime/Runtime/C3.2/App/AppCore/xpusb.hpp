
//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_XPUSB_HPP

#define	INCLUDE_XPUSB_HPP

//////////////////////////////////////////////////////////////////////////
//
// G3 Link USB Signatures
//

#define	SIG_G3CB	HostToMotor(DWORD(0x47334342))

#define SIG_G3SB	HostToMotor(DWORD(0x47335342))

//////////////////////////////////////////////////////////////////////////
//
// Structures
//

struct G3CB;
struct G3SB;

//////////////////////////////////////////////////////////////////////////
//
// G3 Link USB Command Block
//

#pragma pack(1)

struct G3CB
{
	DWORD	m_dwSig;
	WORD    m_wTag;
	BYTE	m_bService;
	BYTE	m_bOpcode;
	BYTE    m_bFlags;
	BYTE	m_bDataSize;
	WORD    m_wBulkSize;
	BYTE	m_bData[19];

	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// G3 Link USB Status Block
//

#pragma pack(1)

struct G3SB 
{
	DWORD	m_dwSig;
	WORD	m_wTag;
	BYTE	m_bOpcode;
	BYTE	m_bReadBulk;
	WORD	m_wBulkSize;

	};

#pragma pack()

// End of File

#endif
