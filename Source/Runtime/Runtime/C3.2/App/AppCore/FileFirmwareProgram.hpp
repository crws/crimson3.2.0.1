
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_FileFirmwareProgram_HPP

#define INCLUDE_FileFirmwareProgram_HPP

//////////////////////////////////////////////////////////////////////////
//
// File Firmware Programming Object
//

class CFileFirmwareProgram : public IFirmwareProgram
{
	public:
		// Constructor
		CFileFirmwareProgram(void);

		// Destructor
		~CFileFirmwareProgram(void);

		// IFirmwareProps
		bool   IsCodeValid(void);
		PCBYTE GetCodeVersion(void);
		UINT   GetCodeSize(void);
		PCBYTE GetCodeData(void);

		// IFirmwareProgram
		bool ClearProgram(UINT uBlocks);
		bool WriteProgram(PCBYTE pData, UINT uCount);
		bool WriteVersion(PCBYTE pData);
		bool StartProgram(void);

	protected:
		// Filenames
		char fileTemp    [16];
		char filePrevious[16];
		char fileBinary  [16];

		// Data Members
		DWORD m_dwInit;
		DWORD m_dwAddr;

		// Implementation
		void  MakeFileNames(void);
		DWORD WriteHeader(CAutoFile &File);
	};

// End of File

#endif
