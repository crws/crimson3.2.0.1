
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Runtime
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ControlTask_HPP

#define INCLUDE_ControlTask_HPP

//////////////////////////////////////////////////////////////////////////
//
// System-Wide Data
//

global	IRouter	      *	g_pRouter	= NULL;

global	IMatrixSsl    *	g_pMatrix	= NULL;

global  IGrabber      *	g_pGrabber      = NULL;

global	UINT		g_uGrabber	= 0;

global	IGDI	      *	g_pGDI		= NULL;

global	IUsbManager   *	g_pUsbMan	= NULL;

global	UINT		g_uLanguage	= 0;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	LOAD_TIMEOUT	30

#define	LOAD_CYCLES	4

#define	BATT_MAGIC_1	'B'

#define	BATT_MAGIC_2	'm'

#define TIMER_RATE	50

#define	ToTimer(t)	((t) / TIMER_RATE)

#define	FLASH_RATE	ToTimer(200)

//////////////////////////////////////////////////////////////////////////
//
// Main Control Task
//

class CControlTask : public IClientProcess,
		     public IEventSink
{
	public:
		// Constructor
		CControlTask(void);

		// Destructor
		~CControlTask(void);

		// Operations
		BOOL Auto(void);
		BOOL Start(void);
		BOOL Stop(UINT uTimeout);
		void Update(UINT hItem, PCBYTE pData, UINT uSize);
		void Restart(void);
		void Reboot(void);
		BOOL EnableMount(BOOL fEnable);
		void SetAlarm(UINT uState);
		void SetSiren(BOOL fOn);
		void SetTimeout(void);
		void SysDebug(PCTXT pForm, va_list pArgs);
		void SetLedMode(UINT uMode);
		UINT GetLedMode(void);
		void SkipNetUpdate(void);

		// Attributes
		BOOL GetSiren(void) const;
		BOOL IsFlashEnabled(void) const;
		BOOL IsRunningControl(void) const;

		// Entry Points
		BOOL Init(void);
		int  Exec(void);
		void Stop(void);
		void Term(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// Instance Pointer
		static CControlTask * m_pThis;

	protected:
		// LED Types
		enum
		{
			ledStd,
			ledEdge,
			ledDA
			};

		// Data Members
		ULONG		 m_uRefs;
		HTASK		 m_hThis;
		ITimer	       * m_pTimer;
		UINT		 m_uTimer;
		UINT		 m_uLedType;
		BOOL		 m_fAuto;
		IEvent	       * m_pReqStop;
		IEvent	       * m_pReqStart;
		IEvent	       * m_pAckStop;
		IEvent	       * m_pAckStart;
		ISemaphore     * m_pTms;
		UINT		 m_uReboot;
		UINT		 m_hUpdate;
		UINT		 m_uUpdate;
		PBYTE		 m_pUpdate;
		BOOL		 m_fRun;
		UINT		 m_uLoadTime;
		BOOL		 m_fLoaded;
		UINT		 m_uTimeout1;
		UINT		 m_uTimeout2;
		UINT		 m_uAlarm;
		BOOL		 m_fSiren;
		UINT		 m_uSiren;
		UINT		 m_uBright;
		BOOL		 m_fSmall;
		BOOL		 m_fNarrow;
		int		 m_xDebug;
		int		 m_yDebug;
		UINT		 m_uFont[4];
		UINT		 m_uIndex;
		CTaskList        m_List;
		CArray <HTASK>   m_Task;
		BOOL		 m_fSkipNet;
		UINT		 m_uLedMode;
		UINT		 m_t1;

		// Timer Helpers
		void FindLedType(void);
		void PollSiren(void);
		void PollRunning(void);
		void PollStopped(void);
		void PollDriveStatus(UINT uLED, BOOL fState);

		// Power Up
		void PowerUp(void);
		void CreateGDI(void);
		void CreateSMS(void);
		BOOL CreatePNP(void);
		void LoadNetwork(void);

		// Programming Link
		BOOL CreateGrab(void);
		void CreateLink(void);

		// Key Sequences
		BOOL CheckFailed(void);
		BOOL CheckBattery(void);

		// System Loading
		BOOL LoadSystem(void);
		void FreeSystem(void);
		void SetLoading(BOOL fLoad);
		BOOL WasLoading(void);
		void MinWait(UINT dt);

		// Task Management
		void PerformStart(void);
		void PerformStop(void);
		void GetTaskList(CTaskList &List);
		void LockDrives(BOOL fLock);
		void StopDrives(void);
		void NotifyPNP(UINT uMsg);
		void CreateTasks(void);
		void InitTasks(void);
		void StartTasks(void);
		void StopTasks(void);
		void TermTasks(void);
		void DeleteTasks(void);
		BOOL WaitTransition(void);

		// Configuration Backup
		BOOL SaveImage(void);

		// User Interface
		void DrawFrame(void);
		void TextOut(int yp, PCTXT pText, UINT uFont);
		void ShowTitle(void);
		BOOL ShowCopyright(void);
		void ShowDebug(PCTXT pForm, ...);
		void ShowStatus(PCTXT pText);
		void ShowInvalid(void);
		void ShowError(PCTXT pHead, PCTXT pOne, PCTXT pTwo);
		UINT GetAnswer(UINT uTime);
		UINT GetAnswerKeys(UINT uTime);
		UINT GetAnswerTouch(UINT uTime);
	};

// End of File

#endif
