
//////////////////////////////////////////////////////////////////////////
//
// Crimson 2.0 / 3.0 Shared Runtime
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "g3slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ASCII Codes
//

#define	STX 0x02
#define	ETX 0x03
#define	ENQ 0x05
#define	ACK 0x06
#define	DLE 0x10
#define	NAK 0x15
#define	SYN 0x16

////////////////////////////////////////////////////////////////////////
//
// Serial Client Transport
//

class CSerialClient : public IClientProcess
{
	public:
		// Constructor
		CSerialClient(UINT uPriority, UINT uPort, ILinkService *pService, IGrabber *pGrabber);

		// Operations
		BOOL Open(void);
		void Close(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

	protected:
		// Data
		ULONG		m_uRefs;
		UINT		m_uPriority;
		UINT		m_uPort;
		ILinkService *  m_pService;
		IGrabber     *  m_pGrabber;
		HTASK		m_hTask;
		BOOL		m_fInit;
		UINT		m_uTimeout;
		IPortObject  *  m_pPort;
		IDataHandler *  m_pData;
		CLinkFrame	m_Req;
		CLinkFrame	m_Rep;
		BYTE		m_bRxBuff[1280];
		BYTE		m_bTxBuff[2560];
		WORD		m_wCheck;
		UINT		m_uSize;
		UINT		m_uPtr;
		PBYTE		m_pCode;
		UINT		m_uCode;

		// Destructor
		~CSerialClient(void);

		// Frame Processing
		UINT Process(void);
		void Timeout(void);
		void EndLink(CLinkFrame &Req);

		// Frame Buidling
		void NewFrame(void);
		void AddCtrl(BYTE bCtrl);
		void AddData(PCBYTE pData, UINT uCount);
		void AddByte(BYTE bData);

		// Transport Layer
		BOOL OpenPort(void);
		void ClosePort(void);
		BOOL HasPortRequest(void);
		BOOL GetFrame(void);
		BOOL PutFrame(void);
		BOOL GetAck(void);
		void SendByte(BYTE b);

		// Session Timeout
		void ResetTimeout(void);
		void CheckTimeout(void);
		
		// Fake Loader
		UINT CatchLoadWriteProgram(void);
		UINT CatchLoadUpdateProgram(void);
	};

////////////////////////////////////////////////////////////////////////
//
// Serial Client Transport
//

// Static Data

static CSerialClient * m_pThis = NULL;

// Launchers

global void Create_XPSerial(UINT uPriority, UINT uPort, ILinkService *pService, IGrabber *pGrabber)
{
	m_pThis = New CSerialClient(uPriority, uPort, pService, pGrabber);
	
	m_pThis->Open();
	}

global void Create_XPSerial(UINT uPriority, ILinkService *pService, IGrabber *pGrabber)
{
	m_pThis = New CSerialClient(uPriority, 0, pService, pGrabber);
	
	m_pThis->Open();
	}

global BOOL Delete_XPSerial(void)
{
	if( m_pThis ) {

		m_pThis->Close();

		m_pThis->Release();

		m_pThis = NULL;

		return TRUE;
		}

	return FALSE;
	}

// Constructor

CSerialClient::CSerialClient(UINT uPriority, UINT uPort, ILinkService *pService, IGrabber *pGrabber)
{
	StdSetRef();

	m_uPriority = uPriority;

	m_uPort     = uPort;

	m_pService  = pService;

	m_pGrabber  = pGrabber;

	m_hTask     = NULL;

	m_fInit	    = TRUE;

	m_pCode     = NULL;

	m_uCode     = 0;
	}

// Denstructor

CSerialClient::~CSerialClient(void)
{
	}

// Operations

BOOL CSerialClient::Open(void)
{
	m_hTask = CreateClientThread(this, 0, m_uPriority);

	return TRUE;
	}

void CSerialClient::Close(void)
{
	DestroyThread(m_hTask);
	}

// IUnknown

HRESULT CSerialClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
	}

ULONG CSerialClient::AddRef(void)
{
	StdAddRef();
	}

ULONG CSerialClient::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CSerialClient::TaskInit(UINT uTask)
{
	SetThreadName("XPSerial");

	return OpenPort();
	}

INT CSerialClient::TaskExec(UINT uTask)
{
	for(;;) {

		if( GetFrame() ) {

			m_Req.StartFrame(m_bRxBuff[2], m_bRxBuff[4]);

			m_Req.SetFlags(m_bRxBuff[5]);

			m_Req.AddData(m_bRxBuff + 6, m_uSize - 6);

			UINT p = Process();

			if( p == procError ) {

				m_Rep.StartFrame(m_Req.GetService(), opNak);
				}

			PutFrame();

			if( p == procEndLink ) {

				Sleep(50);
				
				ClosePort();

				EndLink(m_Req);
				}
			}
		else {
			if( HasPortRequest() ) {

				ClosePort();

				OpenPort();
				}
			}
		}

	return 0;
	}

void CSerialClient::TaskStop(UINT uTask)
{
	}

void CSerialClient::TaskTerm(UINT uTask)
{
	ClosePort();
	}

// Frame Processing

UINT CSerialClient::Process(void)
{
	if( m_pService ) {

		if( m_Req.GetService() == servProm ) {

			switch( m_Req.GetOpcode() ) {
				
				case promWriteProgram:

					return CatchLoadWriteProgram();
				
				case promUpdateProgram:

					return CatchLoadUpdateProgram();
				}
			}

		return m_pService->Process(m_Req, m_Rep);
		}

	return procError;
	}

void CSerialClient::Timeout(void)
{
	if( m_pService ) {

		m_pService->Timeout();
		}
	}

void CSerialClient::EndLink(CLinkFrame &Req)
{
	if( m_pService ) {

		m_pService->EndLink(Req);
		}

	for(;;) Sleep(FOREVER);
	}

// Frame Buidling

void CSerialClient::NewFrame(void)
{
	m_uPtr = 0;
	}

void CSerialClient::AddCtrl(BYTE bCtrl)
{
	m_bTxBuff[m_uPtr++] = DLE;

	m_bTxBuff[m_uPtr++] = bCtrl;
	}

void CSerialClient::AddData(PCBYTE pData, UINT uCount)
{
	while( uCount-- ) {

		AddByte(*pData++);
		}
	}

void CSerialClient::AddByte(BYTE bData)
{
	if( bData == DLE ) {

		m_bTxBuff[m_uPtr++] = bData;

		m_bTxBuff[m_uPtr++] = bData;
		}
	else
		m_bTxBuff[m_uPtr++] = bData;

	m_wCheck += bData;
	}

// Transport Layer

BOOL CSerialClient::OpenPort(void)
{
	if( !m_pGrabber || m_pGrabber->Claim() ) {

		GuardThread(TRUE);

		m_pPort = SerialGetPort(m_uPort);

		if( m_pPort ) {

			m_pData = Create_SingleDataHandler();

			m_pPort->Bind(m_pData);

			CSerialConfig Config;

			Config.m_uPhysical = physicalRS232;
			Config.m_uBaudRate = 115200;
			Config.m_uDataBits = 8;
			Config.m_uStopBits = 1;
			Config.m_uParity   = 0;
			Config.m_uFlags    = 0;

			m_pData->SetRxSize(2560);

			if( m_pPort->Open(Config) ) {

				GuardThread(FALSE);

				return TRUE;
				}

			ClosePort();
			}

		GuardThread(FALSE);
		}

	return FALSE;
	}

void CSerialClient::ClosePort(void)
{
	GuardThread(TRUE);

	if( m_pData ) {

		m_pPort->Close();

		m_pData->Release();

		m_pData = NULL;
	
		if( m_pGrabber ) {

			m_pGrabber->Free();
			}
		}

	GuardThread(FALSE);
	}

BOOL CSerialClient::HasPortRequest(void)
{
	return m_pGrabber ? m_pGrabber->HasRequest() : FALSE;
	}

BOOL CSerialClient::GetFrame(void)
{
	UINT uState   = 0;
	UINT uCount   = 0;
	UINT uDelay   = 0;
	WORD wCheck   = 0;
	BOOL fDLE     = FALSE;
	
	ResetTimeout();

	for(;;) {
	
		UINT w = m_pData->Read(50);

		if( w == NOTHING ) {

			if( HasPortRequest() ) {
				
				return FALSE;
				}
			
			if( ++uDelay >= 10 ) {
						
				uDelay = 0;

				uState = 0;
				}
			
			CheckTimeout();
			
			continue;
			}
		else {
			CheckTimeout();

			uDelay = 0;
			}

		if( w == DLE ) {
		
			if( !fDLE ) {
			
				fDLE = TRUE;
				
				continue;
				}
			else
				fDLE = FALSE;
			}
			
		switch( uState ) {
		
			case 0:
				if( fDLE ) {
				
					if( w == STX ) {
	
						wCheck = 0x5678;
						
						uCount = 0;
						
						uState = 1;

						break;
						}
					}
				
				if( HasPortRequest() ) {

					return FALSE;
					}

				break;
				
			case 1:
				if( fDLE ) {
				
					if( w == ETX ) {

						uState = 2;
						}
					else
						uState = 0;
					}
				else {
					m_bRxBuff[uCount] = w;
				
					if( ++uCount == sizeof(m_bRxBuff) ) {

						uState = 0;
						}
						
					wCheck += w;
					}
				
				break;
				
			case 2:
				if( fDLE ) {

					uState = 0;
					}
				else {
					wCheck = wCheck - 0x001 * w;
					
					uState = 3;
					}
				
				break;
				
			case 3:
				if( fDLE ) {

					uState = 0;
					}
				else {
					wCheck = wCheck - 0x100 * w;
					
					if( wCheck ) {

						SendByte(NAK);

						uState = 0;
						}
					else {
						SendByte(ACK);

						ResetTimeout();
						
						m_uSize = uCount;

						return TRUE;
						}
					}
				break;
			}
		
		fDLE = FALSE;
		}

	return FALSE;
	}

BOOL CSerialClient::PutFrame(void)
{
	NewFrame();

	AddCtrl(STX);

	m_wCheck = 0x5678;

	AddByte(0x00);
	
	AddByte(0x00);
	
	AddByte(m_bRxBuff[2]);
	
	AddByte(m_bRxBuff[3]);
	
	AddByte(m_Rep.GetOpcode());
	
	AddByte(0x00);

	AddData(m_Rep.GetData(), m_Rep.GetDataSize());

	AddCtrl(ETX);

	WORD wCheck = m_wCheck;

	AddByte(LOBYTE(wCheck));

	AddByte(HIBYTE(wCheck));

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return GetAck();
	}

BOOL CSerialClient::GetAck(void)
{
	SetTimer(200);

	for(;;) {

		UINT w = m_pData->Read(50);

		if( w == NOTHING ) {

			if( !GetTimer() ) {

				return FALSE;
				}

			continue;
			}

		if( w == ACK ) {

			return TRUE;
			}

		if( w == NAK ) {

			return FALSE;
			}
		}

	return FALSE;
	}

void CSerialClient::SendByte(BYTE b)
{
	m_pData->Write(b, FOREVER);
	}

// Timeouts

void CSerialClient::ResetTimeout(void)
{
	if( m_fInit ) {

		m_uTimeout = GetTickCount() + ToTicks(g_uTimeout); 
		}
	}

void CSerialClient::CheckTimeout(void)
{
	if( m_fInit ) {

		if( GetTickCount() > m_uTimeout ) {

			Timeout();
			
			m_fInit = FALSE;
			}
		}
	}

// Fake Loader

UINT CSerialClient::CatchLoadWriteProgram(void)
{
	if( g_pBootProgram ) {

		UINT  uPtr   = 0;

		DWORD dwAddr = m_Req.ReadLong(uPtr);

		UINT  uCount = m_Req.ReadWord(uPtr);

		PBYTE pData  = PBYTE(m_Req.ReadData(uPtr, uCount));

		UINT  uAlloc = 2 * 1024 * 1024;

		if( !dwAddr ) {

			if( m_pCode ) {
			
				delete m_pCode;			
				}

			m_pCode = New BYTE [ uAlloc ];

			m_uCode = 0;
			}

		if( (dwAddr + uCount) <= uAlloc ) {

			memcpy(m_pCode + dwAddr, pData, uCount);

			m_uCode += uCount;

			m_Rep.StartFrame(servProm, opAck);

			return procOkay;
			}
		}

	m_Rep.StartFrame(servProm, opNak);

	return procOkay;
	}

UINT CSerialClient::CatchLoadUpdateProgram(void)
{
	if( g_pBootProgram ) {

		UINT  uPtr    = 0;	
	
		BOOL  fRemote = m_Req.ReadByte(uPtr);

		if( g_pBootProgram->ClearProgram(8) ) {

			if( g_pBootProgram->WriteProgram(m_pCode, m_uCode) ) {

				m_Rep.StartFrame(servProm, opAck);			

				delete m_pCode;

				m_pCode = NULL;

				return procOkay;
				}
			}		

		delete m_pCode;
		}

	m_Rep.StartFrame(servProm, opNak);

	return procOkay;
	}

// End of File
