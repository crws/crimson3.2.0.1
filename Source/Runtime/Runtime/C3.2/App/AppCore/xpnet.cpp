
//////////////////////////////////////////////////////////////////////////
//
// Crimson Runtime
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "g3slave.hpp"

////////////////////////////////////////////////////////////////////////
//
// Network Client Transport
//

class CNetworkClient : public IClientProcess
{
	public:
		// Constructor
		CNetworkClient(UINT uPriority, ILinkService *pService);

		// Destructor
		~CNetworkClient(void);

		// Operations
		BOOL Open(void);
		void Close(void);
		void Purge(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

	protected:
		// Data
		ULONG		m_uRefs;
		UINT		m_uPriority;
		ILinkService *  m_pService;
		HTASK		m_hTask;
		ISocket      *  m_pSocket;
		BOOL		m_fSafe;
		BOOL		m_fStop;
		BYTE		m_bSeq;
		CLinkFrame	m_Req;
		CLinkFrame	m_Rep;
		PBYTE		m_pCode;
		UINT		m_uCode;

		// Entry Point
		void Task(void);

		// Frame Processing
		UINT Process(void);
		void Timeout(void);
		void EndLink(CLinkFrame &Req);

		// Transport Layer
		BOOL GetFrame(void);
		BOOL PutFrame(void);
		BOOL IsActive(void);
		BOOL IsOpen(void);
		BOOL CheckAccess(void);
		
		// Fake Boot Loader
		UINT CatchForceReset(void);
		UINT CatchClearProgram(void);
		UINT CatchWriteProgram(void);
		UINT CatchWriteVersion(void);
		UINT CatchStartProgram(void);
		void RouterStop(void);
		
		// Fake Loader
		UINT CatchLoadWriteProgram(void);
		UINT CatchLoadUpdateProgram(void);
	};

////////////////////////////////////////////////////////////////////////
//
// Network Client Transport
//

// Static Data

static CNetworkClient * m_pThis = NULL;

// Launcher

global void Create_XPNetwork(UINT uPriority, ILinkService *pService)
{
	m_pThis = New CNetworkClient(uPriority, pService);
	
	m_pThis->Open();
	}

global BOOL Delete_XPNetwork(void)
{
	if( m_pThis ) {

		m_pThis->Close();

		delete m_pThis;

		m_pThis = NULL;

		return TRUE;
		}

	return FALSE;
	}

// Purging

global BOOL Purge_XPNetwork(void)
{
	if( m_pThis ) {

		m_pThis->Purge();

		return TRUE;
		}

	return FALSE;
	}

// Constructor

CNetworkClient::CNetworkClient(UINT uPriority, ILinkService *pService)
{
	StdSetRef();

	m_uPriority = uPriority;

	m_pService  = pService;

	m_hTask     = NULL;

	m_pSocket   = NULL;

	m_fStop     = FALSE;

	m_pCode     = NULL;

	m_uCode     = 0;
	}

// Destructor

CNetworkClient::~CNetworkClient(void)
{
	}

// Operations

BOOL CNetworkClient::Open(void)
{
	m_hTask = CreateClientThread(this, 0, m_uPriority);

	return TRUE;
	}

void CNetworkClient::Close(void)
{
	DestroyThread(m_hTask);
	}

void CNetworkClient::Purge(void)
{
	while( m_pSocket ) {

		Sleep(50);
		}
	}

// IUnknown

HRESULT CNetworkClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
	}

ULONG CNetworkClient::AddRef(void)
{
	StdAddRef();
	}

ULONG CNetworkClient::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CNetworkClient::TaskInit(UINT uTask)
{
	SetThreadName("XpNetwork");

	return TRUE;
	}

INT CNetworkClient::TaskExec(UINT uTask)
{
	BOOL fCheck   = TRUE;

	UINT uTimeout = 0;
	
	for(;;) {

		if( IsActive() ) {

			GuardThread(TRUE);

			if( !m_pSocket ) {

				AfxNewObject("sock-tcp", ISocket, m_pSocket);

				m_pSocket->Listen(CNetConfig::m_Config.m_uDownPort);

				fCheck    = TRUE;
				}

			GuardThread(FALSE);

			UINT uPhase = 0;

			m_pSocket->GetPhase(uPhase);

			if( uPhase == PHASE_OPEN ) {

				if( fCheck ) {

					if( !CheckAccess() ) {

						m_pSocket->Abort();

						continue;
						}

					fCheck = FALSE;
					}

				if( GetFrame() ) {

					UINT p = Process();

					if( p == procError ) {

						m_Rep.StartFrame(m_Req.GetService(), opNak);
						}

					PutFrame();

					if( p == procEndLink ) {

						m_pSocket->Close();

						m_pSocket->Release();

						Sleep(50);

						RouterStop();

						EndLink(m_Req);
						}

					uTimeout = 0;

					continue;
					}

				if( (uTimeout += 5) >= 30000 ) {
				
					m_pSocket->Abort();
					}

				Sleep(5);
				}

			if( uPhase == PHASE_CLOSING ) {
					
				m_pSocket->Close();
				}

			if( uPhase == PHASE_ERROR ) {

				GuardThread(TRUE);

				m_pSocket->Release();

				m_pSocket = NULL;

				GuardThread(FALSE);

				continue;
				}

			Sleep(5);
			}
		else {
			GuardThread(TRUE);

			if( m_pSocket ) {

				m_pSocket->Abort();

				m_pSocket->Release();

				m_pSocket = NULL;
				}

			GuardThread(FALSE);

			Sleep(50);
			}
		}

	return 0;
	}

void CNetworkClient::TaskStop(UINT uTask)
{
	}

void CNetworkClient::TaskTerm(UINT uTask)
{
	if( m_pSocket ) {

		m_pSocket->Abort();

		m_pSocket->Release();

		m_pSocket = NULL;
		}
	}

// Frame Processing

UINT CNetworkClient::Process(void)
{
	if( m_pService ) {

		if( m_Req.GetService() == servProm ) {

			switch( m_Req.GetOpcode() ) {
				
				case promWriteProgram:

					return CatchLoadWriteProgram();
				
				case promUpdateProgram:

					return CatchLoadUpdateProgram();
				}
			}

		if( m_Req.GetService() == servBoot ) {

			switch( m_Req.GetOpcode() ) {

				case bootForceReset:

					return CatchForceReset();

				case bootClearProgram:

					return CatchClearProgram();

				case bootWriteProgram:

					return CatchWriteProgram();

				case bootWriteVersion:

					return CatchWriteVersion();
				}

			if( m_Req.GetOpcode() == bootStartProgram ) {

				if( CatchStartProgram() == procOkay ) {

					return procOkay;
					}

				Sleep(500);
				}
			}

		return m_pService->Process(m_Req, m_Rep);
		}

	return procError;
	}

void CNetworkClient::Timeout(void)
{
	if( m_pService ) {

		m_pService->Timeout();
		}
	}

void CNetworkClient::EndLink(CLinkFrame &Req)
{
	if( m_pService ) {

		m_pService->EndLink(Req);
		}

	for(;;) Sleep(FOREVER);
	}

// Transport Layer

BOOL CNetworkClient::GetFrame(void)
{
	WORD wFrame = 0;

	UINT uSize  = sizeof(wFrame);

	if( m_pSocket->Recv(PBYTE(&wFrame), uSize) == S_OK ) {

		wFrame      = MotorToHost(wFrame);

		PBYTE pData = New BYTE [ wFrame ];

		UINT  uPtr  = 0;

		SetTimer(30000);

		while( IsOpen() ) {

			UINT uSize = wFrame - uPtr;

			if( m_pSocket->Recv(pData + uPtr, uSize) == S_OK ) {

				if( (uPtr += uSize) == wFrame ) {

					m_bSeq = pData[1];

					m_Req.StartFrame(pData[0], pData[2]);

					m_Req.SetFlags  (pData[3]);

					m_Req.AddData   (pData + 4, uPtr- 4);

					delete pData;

					return TRUE;
					}

				SetTimer(30000);
				}

			if( !GetTimer() ) {

				m_pSocket->Abort();

				break;
				}

			Sleep(5);
			}

		delete pData;

		return FALSE;
		}

	return FALSE;
	}

BOOL CNetworkClient::PutFrame(void)
{
	CBuffer *pBuff = BuffAllocate(1280);

	UINT     uBuff = 1274;

	if( pBuff ) {

		UINT  uSize = m_Rep.GetDataSize();

		PBYTE pFrom = m_Rep.GetData();

		PBYTE pHead = pBuff->AddTail(6);

		pHead[0] = HIBYTE(uSize + 4);
		
		pHead[1] = LOBYTE(uSize + 4);

		pHead[2] = m_Rep.GetService();

		pHead[3] = m_bSeq;

		pHead[4] = m_Rep.GetOpcode();

		pHead[5] = 0;

		for(;;) {

			UINT  uLump = min(uSize, uBuff);

			PBYTE pData = pBuff->AddTail(uLump);

			memcpy(pData, pFrom, uLump);

			pFrom += uLump;

			uSize -= uLump;

			for(;;) {

				if( !IsOpen() ) {

					pBuff->Release();

					return FALSE;
					}

				if( m_pSocket->Send(pBuff) == S_OK ) {

					break;
					}

				Sleep(5);
				}

			if( uSize ) {

				if( (pBuff = BuffAllocate(1280)) ) {

					uBuff = 1280;

					continue;
					}

				return FALSE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CNetworkClient::IsActive(void)
{
	if( g_pRouter ) {
		
		if( CNetConfig::m_Config.m_uDownload ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CNetworkClient::IsOpen(void)
{
	if( IsActive() ) {

		UINT uPhase = 0;

		m_pSocket->GetPhase(uPhase);

		return uPhase == PHASE_OPEN;
		}

	return FALSE;
	}

BOOL CNetworkClient::CheckAccess(void)
{
/*	if( CNetConfig::m_Config.m_uDownload == 2 ) {

		CIPAddr IP;

		if( m_pSocket->GetRemote(IP) == S_OK ) {

			if( g_pRouter->IsSafeIP(IP) ) {

				return FALSE;
				}

			return TRUE;
			}

		return FALSE;
		}

*/	return TRUE;
	}

// Fake Boot Loader

UINT CNetworkClient::CatchForceReset(void)
{
	m_Rep.StartFrame(servBoot, opAck);

	m_fStop = TRUE;

	return procOkay;
	}

UINT CNetworkClient::CatchClearProgram(void)
{
	if( g_pFirmPend ) {

		UINT uPtr   = 0;

		BYTE bCount = m_Req.ReadByte(uPtr);

		if( g_pFirmPend->ClearProgram(bCount) ) {

			m_Rep.StartFrame(servBoot, opAck);

			return procOkay;
			}
		}

	m_Rep.StartFrame(servBoot, opNak);

	return procOkay;
	}

UINT CNetworkClient::CatchWriteProgram(void)
{
	if( g_pFirmPend ) {

		UINT  uPtr   = 0;

		DWORD dwAddr = m_Req.ReadLong(uPtr);

		WORD  wCount = m_Req.ReadWord(uPtr);

		PBYTE pData  = PBYTE(m_Req.ReadData(uPtr, wCount));

		if( g_pFirmPend->WriteProgram(pData, wCount) ) {

			m_Rep.StartFrame(servBoot, opAck);

			return procOkay;
			}
		}

	m_Rep.StartFrame(servBoot, opNak);

	return procOkay;
	}

UINT CNetworkClient::CatchWriteVersion(void)
{
	if( g_pFirmPend ) {

		UINT  uPtr   = 0;

		PBYTE pData  = PBYTE(m_Req.ReadData(uPtr, 16));

		if( g_pFirmPend->WriteVersion(pData) ) {

			m_Rep.StartFrame(servBoot, opAck);

			return procOkay;
			}
		}

	m_Rep.StartFrame(servBoot, opNak);

	return procOkay;
	}

UINT CNetworkClient::CatchStartProgram(void)
{
	if( g_pFirmPend ) {

		if( m_fStop ) {

			ImageDisable();

			SystemStop  ();

			m_Req.StartFrame(servBoot, bootForceReset);

			m_Req.AddLong(0);

			return procEndLink;
			}
		}

	m_Rep.StartFrame(servBoot, opNak);

	return procOkay;
	}

void CNetworkClient::RouterStop(void)
{
	if( g_pRouter ) {

		g_pRouter->Close();

		delete g_pRouter;

		g_pRouter = NULL;
		}
	}

// Fake Loader

UINT CNetworkClient::CatchLoadWriteProgram(void)
{
	if( g_pBootProgram ) {

		UINT  uPtr   = 0;

		DWORD dwAddr = m_Req.ReadLong(uPtr);

		UINT  uCount = m_Req.ReadWord(uPtr);

		PBYTE pData  = PBYTE(m_Req.ReadData(uPtr, uCount));

		UINT  uAlloc = 2 * 1024 * 1024;

		if( !dwAddr ) {

			if( m_pCode ) {
			
				delete m_pCode;			
				}

			m_pCode = New BYTE [ uAlloc ];			

			m_uCode = 0;
			}

		if( (dwAddr + uCount) <= uAlloc ) {			

			memcpy(m_pCode + dwAddr, pData, uCount);

			m_uCode += uCount;

			m_Rep.StartFrame(servProm, opAck);

			return procOkay;
			}
		}

	m_Rep.StartFrame(servProm, opNak);

	return procOkay;
	}

UINT CNetworkClient::CatchLoadUpdateProgram(void)
{
	if( g_pBootProgram ) {

		UINT  uPtr    = 0;	
	
		BOOL  fRemote = m_Req.ReadByte(uPtr);

		if( g_pBootProgram->ClearProgram(8) ) {

			if( g_pBootProgram->WriteProgram(m_pCode, m_uCode) ) {

				m_Rep.StartFrame(servProm, opAck);			

				delete m_pCode;

				m_pCode = NULL;

				return procOkay;
				}
			}		
		}

	delete m_pCode;

	m_pCode = NULL;

	m_Rep.StartFrame(servProm, opNak);

	return procOkay;
	}

// End of File
