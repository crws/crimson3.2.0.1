
//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "image.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Database Image Loading
//

// Static Data

static	CAutoFile	m_File;

static	DWORD		m_dwPos;

static	DWORD		m_dwSize;

static	BYTE		m_bGUID[16];

static	DWORD		m_dwTime;

static  BOOL		m_fVerify = FALSE;

// Prototypes

global	BOOL	ImageCheck(void);
global	BOOL	ImageLoad(void);
global	UINT	ImageSave(PCTXT pName);
global	void	ImageDisable(void);
global	void	ImageEnable(void);
global	BOOL	ImageVerify(void);
global  BOOL	ImageCanVerify(void);
static	void	SkipFileHeader(CAutoFile &File);
static	BOOL	SaveFileHeader(CAutoFile &File);
static	BOOL	SaveTargTable(CAutoFile &File);
static	void	SkipFileTable(CAutoFile &File);
static	BOOL	SaveFirmware(CAutoFile &File, CImageFileRecord &Rec);
static	BOOL	SaveBootLoad(CAutoFile &File, CImageFileRecord &Rec);
static	BOOL	SaveDatabase(CAutoFile &File, CImageFileRecord &Rec);
static	BOOL	OpenImage(void);
static	BOOL	TouchImage(BOOL fKill);
static	void	IntelToHost(CImageHeader &Header);
static	void	IntelToHost(CImageTargRecord &Targ);
static	void	IntelToHost(CImageFileRecord &File);
static	void	SelectFile(CImageFileRecord &File);
static	BOOL	AllowCRC(void);
static	BOOL	SaveCRC(CAutoFile &File);
 
// Code

global	BOOL	ImageCheck(void)
{
	if( OpenImage() ) {

		BYTE bGuid[16];

		g_pDbase->GetVersion(bGuid);

		if( memcmp(m_bGUID, bGuid, 16) ) {

			return TRUE;
			}

		if( m_dwTime != g_pDbase->GetRevision() ) {

			return TRUE;
			}

		m_File.Close();

		return FALSE;
		}

	return FALSE;
	}

global	BOOL	ImageLoad(void)
{
	g_pDbase->SetValid(FALSE);

	g_pDbase->Clear();

	m_File.Seek(m_dwPos);

	for( BOOL fInit = TRUE;; fInit = FALSE ) {

		UINT uItem;

		m_File.Read(PBYTE(&uItem), sizeof(uItem));

		uItem = IntelToHost(DWORD(uItem));

		if( fInit || uItem ) {

			UINT uClass;

			UINT uSize;

			UINT uData;

			m_File.Read(PBYTE(&uClass), sizeof(uClass));

			m_File.Read(PBYTE(&uSize),  sizeof(uSize));

			m_File.Read(PBYTE(&uData),  sizeof(uData));

			uClass = IntelToHost(DWORD(uClass));

			uSize  = IntelToHost(DWORD(uSize));

			uData  = IntelToHost(DWORD(uData));

			if( uSize ) {

				PBYTE pItem = New BYTE [ uData ];

				m_File.Read(pItem, uData);

				if( !g_pDbase->CanCompress() && (uSize > uData) ) {

					PBYTE pData = New BYTE [ uSize ];

					fastlz_decompress(pItem, uData, pData);

					delete pItem;

					pItem = pData;

					uData = uSize;
					}

				CItemInfo Info;

				Info.m_uItem  = uItem;
				Info.m_uClass = uClass;
				Info.m_uSize  = uSize;
				Info.m_uComp  = uData;

				g_pDbase->WriteItem(Info, pItem);

				delete pItem;
				}
			else {
				CItemInfo Info;

				Info.m_uItem  = uItem;
				Info.m_uClass = uClass;
				Info.m_uSize  = 0;
				Info.m_uComp  = 0;

				g_pDbase->WriteItem(Info, NULL);
				}
			}
		else {
			g_pDbase->SetVersion (m_bGUID);

			g_pDbase->SetRevision(m_dwTime);

			g_pDbase->SetValid(TRUE);

			break;
			}
		}

	m_File.Close();

	return TRUE;
	}

global	UINT	ImageSave(PCTXT pName)
{
	CAutoFile File(pName, "r+", "w+");

	if( File ) {

		SkipFileHeader(File);

		SaveTargTable (File);

		DWORD dwFile = File.GetPos();

		SkipFileTable (File);

		CImageFileRecord Firm, Boot, Base;

		if( g_pProm ) {

			g_pProm->Lock();
			}

		BOOL fSaved = SaveFirmware(File, Firm) &&
			      SaveBootLoad(File, Boot) &&
			      SaveDatabase(File, Base) ;;

		if( g_pProm ) {

			g_pProm->Free();
			}

		if( fSaved ) {

			File.Truncate();

			File.Seek(0);

			File.Write(PBYTE(&Firm), sizeof(Firm));

			File.Write(PBYTE(&Boot), sizeof(Boot));

			File.Write(PBYTE(&Base), sizeof(Base));

			if( SaveFileHeader(File) ) {

				if( SaveCRC(File) ) {

					// TODO -- Set revision to avoid reload on next reset.

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

global	void	ImageDisable(void)
{
	TouchImage(TRUE);
	}

global	void	ImageEnable(void)
{
	TouchImage(FALSE);
	}

global BOOL	ImageVerify(void)
{
	if( m_fVerify ) {

		CRC32_Clear();

		UINT  uTotal = m_File.GetSize() - 4;

		UINT  uChunk = 0x10000;

		PBYTE pCheck = New BYTE [ uChunk ];

		m_File.Seek(0);

		UINT u = 0;

		while( u < uTotal ) {

			UINT uRead = u + uChunk < uTotal ? uChunk : uTotal - u;			

			if( !m_File.Read(pCheck, uRead) ) {

				delete [] pCheck;

				m_File.Close();

				return FALSE;
				}

			CRC32_AddData(pCheck, uRead);

			u += uRead;
			}		

		DWORD dwCRC = CRC32_GetValue();		

		DWORD dwChk = 0;

		m_File.Read(PBYTE(&dwChk), sizeof(dwChk));

		if( dwCRC != IntelToHost(dwChk) ) {

			delete [] pCheck;

			m_File.Close();

			return FALSE;
			}

		delete [] pCheck;
		}
	
	return TRUE;
	}

global BOOL	ImageCanVerify(void)
{
	return m_fVerify;
	}

static	void	SkipFileHeader(CAutoFile &File)
{
	CImageHeader Head;

	memset(&Head, 0, sizeof(Head));

	File.Write(PBYTE(&Head), sizeof(Head));
	}

static	BOOL	SaveFileHeader(CAutoFile &File)
{
	CImageHeader Head;

	memset(&Head, 0, sizeof(Head));

	Head.m_dwMagic    = 0x334943;
	Head.m_wVersion   = 2;
	Head.m_wFlags     = AllowCRC() ? 4 : 0;
	Head.m_wTargCount = 1;
	Head.m_wFileCount = 3;
	Head.m_wDbase     = 2;

	strcpy(Head.m_sOEM, GetOemName());

	IntelToHost(Head);

	File.Seek(0);

	return File.Write(PBYTE(&Head), sizeof(Head)) == sizeof(Head);
	}

static	BOOL	SaveTargTable(CAutoFile &File)
{
	CImageTargRecord Targ;

	strcpy(Targ.m_sName, WhoGetName(FALSE));

	memset(Targ.m_wFile, 0xFF, sizeof(Targ.m_wFile));

	Targ.m_wFile[0] = 0;

	Targ.m_wFile[1] = 1;

	IntelToHost(Targ);

	return File.Write(PBYTE(&Targ), sizeof(Targ)) == sizeof(Targ);
	}

static	void	SkipFileTable(CAutoFile &File)
{
	CImageFileRecord Rec[3];

	memset(Rec, 0, sizeof(Rec));

	File.Write(PBYTE(Rec), sizeof(Rec));
	}

static	BOOL	SaveFirmware(CAutoFile &File, CImageFileRecord &Rec)
{
	UINT    uSize = g_pFirmProps->GetCodeSize();
	
	PCBYTE pData  = g_pFirmProps->GetCodeData();

	PCBYTE pGUID  = g_pFirmProps->GetCodeVersion();

	memset(&Rec, 0, sizeof(Rec));

	Rec.m_dwPos   = File.GetPos();

	Rec.m_dwSize  = uSize;

	memcpy(Rec.m_bGUID, pGUID, 16);

	strcpy(Rec.m_sName, WhoGetName(FALSE));

	if( File.Write(PBYTE(pData), uSize) == uSize ) {

		IntelToHost(Rec);

		return TRUE;
		}

	return FALSE;
	}
		
static	BOOL	SaveBootLoad(CAutoFile &File, CImageFileRecord &Rec)
{
	UINT     uSize = g_pBootProgram->GetCodeSize();	

	PCBYTE   pData = g_pBootProgram->GetCodeData();	

	DWORD  Version = AppGetBootVersion();

	memset(&Rec, 0, sizeof(Rec));

	Rec.m_dwPos    = File.GetPos();

	Rec.m_dwSize   = uSize;

	PDWORD(Rec.m_bGUID)[0] = Version;

	strcpy(Rec.m_sName, "prom");

	if( File.Write(PBYTE(pData), uSize) == uSize ) {

		IntelToHost(Rec);

		return TRUE;
		}

	return FALSE;
	}
		
static	BOOL	SaveDatabase(CAutoFile &File, CImageFileRecord &Rec)
{
	memset(&Rec, 0, sizeof(Rec));

	Rec.m_dwPos   = File.GetPos();

	Rec.m_dwSize  = 0;

	g_pDbase->GetVersion(Rec.m_bGUID);

	strcpy(Rec.m_sName, "dbase");

	for( UINT h = 0; h < 4096; h++ ) {

		CItemInfo Info;

		PBYTE pData = PBYTE(g_pDbase->LockItem(h, Info));

		if( pData ) {

			if( Info.m_uSize ) {

				DWORD r[4];

				r[0] = HostToIntel(DWORD(h));
				r[1] = HostToIntel(DWORD(Info.m_uClass));
				r[2] = HostToIntel(DWORD(Info.m_uSize));
				r[3] = HostToIntel(DWORD(Info.m_uSize));
			
				File.Write(PBYTE(r), sizeof(r));

				if( File.Write(pData, Info.m_uSize) == Info.m_uSize ) {

					continue;
					}

				g_pDbase->FreeItem(h);

				return FALSE;
				}			

			g_pDbase->FreeItem(h);
			}
		}

	h = 0;

	File.Write(PBYTE(&h), sizeof(h));

	Rec.m_dwSize = File.GetPos() - Rec.m_dwPos;

	IntelToHost(Rec);

	return TRUE;
	}
		
static	BOOL	OpenImage(void)
{
	char sName[16];

	strcpy(sName, "C:\\image.");

	strcat(sName, GetOemImageExt());

	if( m_File.Open(sName, "r") ) {

		CImageHeader Header;

		m_File.Read(PBYTE(&Header), sizeof(Header));

		if( !(Header.m_wFlags & 0x0002) ) {

			IntelToHost(Header);

			if( Header.m_dwMagic == 0x334943 && Header.m_wVersion == 0x000002 ) {

				PCTXT pOEM = GetOemName();

				if( !stricmp(Header.m_sOEM, pOEM) ) {

					UINT nt = Header.m_wTargCount;

					UINT nf = Header.m_wFileCount;

					UINT st = nt * sizeof(CImageTargRecord);

					UINT sf = nf * sizeof(CImageFileRecord);

					CImageTargRecord *pTarg = New CImageTargRecord [ nt ];

					CImageFileRecord *pFile = New CImageFileRecord [ nf ];

					m_File.Read(PBYTE(pTarg), st);

					m_File.Read(PBYTE(pFile), sf);

					m_fVerify = (Header.m_wFlags & 0x0004);

					for( UINT t = 0; t < nt; t++ ) {

						CImageTargRecord &Targ = pTarg[t];

						if( !stricmp(Targ.m_sName, WhoGetName(FALSE)) ) {

							IntelToHost(Targ);

							WORD wFile = Header.m_wDbase;

							if( wFile < 0xFFFF ) {

								SelectFile(pFile[wFile]);

								delete [] pTarg;

								delete [] pFile;

								return TRUE;
								}

							break;
							}
						}

					delete [] pTarg;
					}
				}
			}

		m_File.Close();
		}

	return FALSE;
	}

static	BOOL	TouchImage(BOOL fKill)
{
	char sName[16];

	strcpy(sName, "C:\\image.");

	strcat(sName, GetOemImageExt());

	CAutoFile File(sName, "r+");

	if( File ) {

		CImageHeader Header;

		File.Read(PBYTE(&Header), sizeof(Header));

		if( fKill ) {

			Header.m_wFlags |= 0x0003;
			}
		else {
			Header.m_wFlags &= ~0x0003;
			}

		File.Seek(0);

		File.Write(PBYTE(&Header), sizeof(Header));

		return TRUE;
		}

	return FALSE;
	}

static	void	IntelToHost(CImageHeader &Header)
{
	Header.m_dwMagic    = IntelToHost(Header.m_dwMagic);
	Header.m_wVersion   = IntelToHost(Header.m_wVersion);
	Header.m_wFlags     = IntelToHost(Header.m_wFlags);
	Header.m_wTargCount = IntelToHost(Header.m_wTargCount);
	Header.m_wFileCount = IntelToHost(Header.m_wFileCount);
	Header.m_wDbase     = IntelToHost(Header.m_wDbase);
	}

static	void	IntelToHost(CImageTargRecord &Targ)
{
	Targ.m_wFile[0] = IntelToHost(Targ.m_wFile[0]);
	Targ.m_wFile[1] = IntelToHost(Targ.m_wFile[1]);
	Targ.m_wFile[2] = IntelToHost(Targ.m_wFile[2]);
	Targ.m_wFile[3] = IntelToHost(Targ.m_wFile[3]);
	Targ.m_wFile[4] = IntelToHost(Targ.m_wFile[4]);
	Targ.m_wFile[5] = IntelToHost(Targ.m_wFile[5]);
	Targ.m_wFile[6] = IntelToHost(Targ.m_wFile[6]);
	Targ.m_wFile[7] = IntelToHost(Targ.m_wFile[7]);
	}

static	void	IntelToHost(CImageFileRecord &File)
{
	File.m_dwPos  = IntelToHost(File.m_dwPos);
	File.m_dwSize = IntelToHost(File.m_dwSize);
	}

static	void	SelectFile(CImageFileRecord &File)
{
	IntelToHost(File);

	m_dwPos  = File.m_dwPos;

	m_dwSize = File.m_dwSize;

	m_dwTime = m_File.GetTime();
	
	memcpy(m_bGUID, File.m_bGUID, 16);
	}

static	BOOL	AllowCRC(void)
{
	return TRUE;
	}

static	BOOL	SaveCRC(CAutoFile &File)
{
	if( AllowCRC() ) {

		CRC32_Clear();

		UINT  uSize = File.GetSize();

		UINT  uLump = 0x10000;		

		PBYTE pData = New BYTE [ uLump ];

		File.Seek(0);

		while( uSize ) {

			UINT uRead = Min(uSize, uLump);			

			if( File.Read(pData, uRead) ) {
			
				CRC32_AddData(pData, uRead);

				uSize -= uRead;
				
				continue;
				}

			break;
			}

		delete pData;

		if( !uSize ) {			

			DWORD dwCRC = HostToIntel(CRC32_GetValue());

			File.SeekEnd();

			return File.Write(PBYTE(&dwCRC), sizeof(dwCRC)) == sizeof(dwCRC);
			}

		return FALSE;
		}

	return TRUE;
	}

// End of File
