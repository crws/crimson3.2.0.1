
#include "intern.hpp"

#include "FileFirmwareProgram.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2001 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// File Firmware Programming Object
//

// Instantiator

global IFirmwareProgram * Create_FileFirmwareProgram(void)
{
	return New CFileFirmwareProgram;
	}

// Constructor

CFileFirmwareProgram::CFileFirmwareProgram(void)
{
	MakeFileNames();
	}

// Destructor

CFileFirmwareProgram::~CFileFirmwareProgram(void)
{
	}

// IFirmwareProps

bool CFileFirmwareProgram::IsCodeValid(void)
{
	return false;
	}

PCBYTE CFileFirmwareProgram::GetCodeVersion(void)
{
	return NULL;
	}

UINT CFileFirmwareProgram::GetCodeSize(void)
{
	return 0;
	}

PCBYTE CFileFirmwareProgram::GetCodeData(void)
{
	return NULL;
	}

// IFirmwareProgram

bool CFileFirmwareProgram::ClearProgram(UINT uBlocks)
{
	CAutoFile File(fileTemp, "r+");

	if( !File ) {

		if( !rename(fileBinary, fileTemp) || !rename(filePrevious, fileTemp) ) {
			
			File.Open(fileTemp, "r+");
			}
		}

	if( !File ) {

		File.Open(fileTemp, "w+");
		}

	if( File ) {

		m_dwInit = WriteHeader(File);

		m_dwAddr = m_dwInit;

		File.Seek(m_dwInit + uBlocks * 0x10000);

		File.Truncate();

		return true;
		}

	return false;
	}

bool CFileFirmwareProgram::WriteProgram(PCBYTE pData, UINT uCount)
{
	CAutoFile File(fileTemp, "r+");

	if( File ) {

		File.Seek(m_dwAddr);

		File.Write(pData, uCount);

		m_dwAddr += uCount;

		return true;
		}

	return false;
	}

bool CFileFirmwareProgram::WriteVersion(PCBYTE pData)
{
	CAutoFile File(fileTemp, "r+");

	if( File ) {

		DWORD dwSize = HostToMotor(File.GetSize() - m_dwInit);

		File.SeekEnd(-16);

		File.Write(pData, 16);

		File.Seek(16);

		File.Write(PBYTE(&dwSize), sizeof(DWORD));

		File.Close();

		rename(fileTemp, fileBinary);

		return true;
		}

	return false;
	}

bool CFileFirmwareProgram::StartProgram(void)
{
	return true;
	}

// Implementation

void CFileFirmwareProgram::MakeFileNames(void)
{
	strcpy(fileTemp,     "C:\\");
	strcpy(filePrevious, "C:\\");
	strcpy(fileBinary,   "C:\\");

	strcat(fileTemp,     WhoGetName(TRUE));
	strcat(filePrevious, WhoGetName(TRUE));
	strcat(fileBinary,   WhoGetName(TRUE));

	strcat(fileTemp,     ".tmp");
	strcat(filePrevious, ".old");
	strcat(fileBinary,   ".rom");
	}

DWORD CFileFirmwareProgram::WriteHeader(CAutoFile &File)
{
	DWORD dwMagic  = HostToMotor(DWORD(0x4649524D));

	WORD  wVersion = MotorToHost(WORD(1));

	WORD  wCount   = MotorToHost(WORD(1));

	File.Write(PBYTE(&dwMagic),  sizeof(DWORD));

	File.Write(PBYTE(&wVersion), sizeof(WORD ));

	File.Write(PBYTE(&wCount  ), sizeof(WORD ));

	DWORD dwSkip = HostToMotor(DWORD(0));

	DWORD dwPos  = HostToMotor(DWORD(0x0100));

	DWORD dwSize = HostToMotor(DWORD(0));

	WORD  wNames = HostToMotor(WORD (1));

	File.Write(PBYTE(&dwSkip), sizeof(DWORD));

	File.Write(PBYTE(&dwPos ), sizeof(DWORD));

	File.Write(PBYTE(&dwSize), sizeof(DWORD));
	
	File.Write(PBYTE(&wNames), sizeof(WORD ));

	char sModel[17];
				
	WhoGetModel(sModel);

	PTXT pSplit = strchr(sModel, '-');

	if( pSplit ) {

		*pSplit = 0;
		}

	if( !strcmp(sModel, "<??>") ) {

		strcpy(sModel, WhoGetName(FALSE));
		}

	File.Write(PBYTE(sModel), 16);

	return 0x0100;
	}

// End of File
