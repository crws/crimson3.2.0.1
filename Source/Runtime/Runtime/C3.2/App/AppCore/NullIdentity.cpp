
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "NullIdentity.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Null Identity Manager
//

// Constructor

CNullIdentity::CNullIdentity(void)
{
	}

// IIdentity

void CNullIdentity::Init(void)
{
	}

int CNullIdentity::GetOemName(PTXT pName, UINT uSize)
{
	pName[0] = 0;

	return 0;
	}

int CNullIdentity::GetOemApp(PTXT pName, UINT uSize)
{
	pName[0] = 0;

	return 0;
	}

bool CNullIdentity::GetKeyData(PBYTE pData, UINT uSize)
{
	memset(pData, 0, uSize);

	return false;
	}

bool CNullIdentity::SetOemName(PCTXT pName, UINT uSize)
{
	return false;
	}

bool CNullIdentity::SetOemApp(PCTXT pName, UINT uSize)
{
	return false;
	}

bool CNullIdentity::SetKeyData(PCBYTE pData, UINT uSize)
{
	return false;
	}

// End of File
