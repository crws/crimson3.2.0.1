
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GLOBAL_HPP

#define	INCLUDE_GLOBAL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Shared Application Functions
//

// control.cpp

extern	BOOL	ControlInit(void);
extern	int     ControlExec(void);
extern	void    ControlStop(void);
extern	void    ControlTerm(void);
extern	BOOL	SystemAuto(void);
extern	BOOL	SystemStart(void);
extern	BOOL	SystemStop(void);
extern	BOOL	SystemStop(UINT uTimeout);
extern	void	SystemUpdate(UINT hItem, PCBYTE pData, UINT uSize);
extern	void	SystemReboot(void);
extern	void	SystemRestart(void);
extern	BOOL	EnableMount(BOOL fEnable);
extern	void	SetAlarm(UINT uState);
extern	void	SetSiren(BOOL fOn);
extern	BOOL	GetSiren(void);
extern	void	KickTimeout(void);
extern	void	SysDebug(PCTXT pText, ...);
extern	void	SetLedMode(UINT uState);
extern	UINT	GetLedMode(void);
extern	void	SkipNetUpdate(void);
extern	BOOL	IsFlashEnabled(void);
extern	BOOL	IsRunningControl(void);

// oem.cpp

extern	BOOL	OemInit(void);
extern	BOOL	IsOem(void);
extern	PCTXT	GetOemName(void);
extern	PCTXT	GetOemImageExt(void);
extern	PCTXT	GetOemAppName(void);
extern	PCTXT	GetOemKeyList(void);
extern	WORD	GetOemDriverBase(void);

// debug.cpp

extern	void	DebugInit(void);
extern	BOOL	DebugMode(void);
extern	UINT	DebugPort(void);

// image.cpp

extern	BOOL	ImageCheck(void);
extern	BOOL	ImageLoad(void);
extern	UINT	ImageSave(PCTXT pName);
extern	void	ImageDisable(void);
extern	void	ImageEnable(void);
extern	BOOL	ImageVerify(void);
extern  BOOL	ImageCanVerify(void);

// fastlz.c

clink  int   fastlz_compress  (void const *input, int length, void *output);
clink  int   fastlz_decompress(void const *input, int length, void *output);

// crc32.cpp

extern DWORD CRC32(PCBYTE pData, UINT uSize);
	
//////////////////////////////////////////////////////////////////////////
//
// Database Managers
//

extern IDatabase * Create_NullDatabase		(	void
							);

extern IDatabase * Create_MemoryMappedDatabase	(	UINT uDataStart,
							UINT uPageSize
							);

extern IDatabase * Create_MemoryMappedDatabase	(	UINT uDataStart,
							UINT uPageSize,
							UINT uPageStart,
							UINT uBankStart
							);

extern IDatabase * Create_CachingDatabase	(	UINT uDataStart,
							UINT uPageSize,
							UINT uPool
							);

extern IDatabase * Create_CachingDatabase	(	UINT uDataStart,
							UINT uPageSize,
							UINT uPageStart,
							UINT uBankStart,
							UINT uPool
							);

extern IDatabase * Create_NandFlashDatabase	(	UINT uStart,
							UINT uEnd,
							UINT uPool
							);

//////////////////////////////////////////////////////////////////////////
//
// Event Storage Managers
//

extern IEvStg * Create_PromEventStorage		(	void
							);

extern IEvStg * Create_NandFlashEventStorage	(	UINT uStart,
							UINT uEnd
							);

//////////////////////////////////////////////////////////////////////////
//
// Bootloader Property Managers
//

extern IFirmwareProps *	Create_NandBootProps	(	UINT uStart,
							UINT uEnd
							);

//////////////////////////////////////////////////////////////////////////
//
// Firmware Property Managers
//

extern IFirmwareProps *	Create_PromFirmwareProps(	void
							);

extern IFirmwareProps *	Create_NandFirmwareProps(	UINT uStart,
							UINT uEnd
							);

//////////////////////////////////////////////////////////////////////////
//
// Bootloader Programming Managers
//

extern IFirmwareProgram * Create_NandBootProgram(	UINT uStart,
							UINT uEnd
							);

//////////////////////////////////////////////////////////////////////////
//
// Firmware Programming Managers
//

extern IFirmwareProgram * Create_FileFirmwareProgram(	void
							);

extern IFirmwareProgram * Create_NandFirmwareProgram(	UINT uStart,
							UINT uEnd
							);

//////////////////////////////////////////////////////////////////////////
//
// Identity Managers
//

extern ICrimsonIdentity * Create_NullIdentity		(	void
								);

extern ICrimsonIdentity * Create_PromIdentity		(	void
								);

extern ICrimsonIdentity * Create_NandFlashIdentity	(	UINT uStart,
								UINT uEnd
								);

//////////////////////////////////////////////////////////////////////////
//
// Persistance Managers
//

extern IPersist  * Create_NullPersist		(	void
							);

extern IPersist  * Create_FlashPersist		(	UINT uDramTop,
							UINT uPageCount,
							UINT uPageStart,
							UINT uBankStart,
							UINT uBankCount
							);

extern IPersist * Create_NandFlashPersist	(	UINT uStart,
							UINT uEnd
							);

//////////////////////////////////////////////////////////////////////////
//
// Prom Managers
//

extern IProm * Create_NullProm			(	void
							);

extern IProm * Create_FlashProm16		(	UINT uPageCount,
							UINT uPageSize,
							BOOL fTopBoot,
							BOOL fLayout
							);

extern IProm * Create_FlashPromPC		(	UINT uPageCount,
							UINT uPageSize,
							BOOL fTopBoot,
							BOOL fLayout
							);

extern IProm * Create_FlashProm32		(	UINT uPageCount,
							UINT uPageSize,
							BOOL fTopBoot
							);

//////////////////////////////////////////////////////////////////////////
//
// License Managers
//

extern ILicense * Create_License		(	void
							);
// End of File

#endif
