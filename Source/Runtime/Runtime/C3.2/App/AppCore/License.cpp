
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "License.hpp"

//////////////////////////////////////////////////////////////////////////
//
// License Manager
//

// Instantiator

global ILicense * Create_License(void)
{
	return New CLicense();
	}

// Constructor

CLicense::CLicense(void)
{
	memset(m_uLicenses, 0, sizeof(m_uLicenses));
	}

// ILicense

void CLicense::Install(UINT LicenseId)
{
	if( FindLicense(LicenseId) == NOTHING ) {

		for( UINT i = 0; i < elements(m_uLicenses); i ++ ) {

			if( !m_uLicenses[i] ) {

				m_uLicenses[i] = LicenseId;

				return;
				}
			}
		}
	}

void CLicense::Uninstall(UINT LicenseId)
{
	UINT i = FindLicense(LicenseId);

	if( i <	NOTHING ) {

		m_uLicenses[i] = 0;
		}
	}

BOOL CLicense::Validate(UINT LicenseId)
{
	return CheckPlatform(LicenseId) || FindLicense(LicenseId) != NOTHING;
	}

// Implementation

UINT CLicense::FindLicense(UINT LicenseId)
{
	if( LicenseId ) {

		for( UINT i = 0; i < elements(m_uLicenses); i ++ ) {

			if( m_uLicenses[i] == LicenseId ) {

				return i;
				}
			}
		}

	return NOTHING;
	}

BOOL CLicense::CheckPlatform(UINT LicenseId)
{
	switch( LicenseId ) {

		case 0x1012:	return CheckControlLicense();
		}	

	return FALSE;
	}

BOOL CLicense::CheckControlLicense(void)
{
	char sModel[32];

	if( WhoGetModel(sModel) ) {

		return !strcmp(sModel, "gsr") || !strcmp(sModel, "gc") || !strcmp(sModel, "gcn");
		}

	return FALSE;
	}

// End of File
