
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Runtime
//
// Copyright (c) 1993-2001 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "g3slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Configuration Service
//

class CConfigService : public ILinkService
{
	public:
		// Constructor
		CConfigService(void);

		// ILinkService
		void Timeout(void);
		UINT Process(CLinkFrame &Req, CLinkFrame &Rep);
		void EndLink(CLinkFrame &Req);

	protected:
		// Data Members
		CItemInfo m_Info;
		PBYTE	m_pData;

		// Implementation
		UINT ReadIPConfig(CLinkFrame &Req, CLinkFrame &Rep);
		UINT CheckVersion(CLinkFrame &Req, CLinkFrame &Rep);
		UINT ClearConfig(CLinkFrame &Req, CLinkFrame &Rep);
		UINT ClearGarbage(CLinkFrame &Req, CLinkFrame &Rep);
		UINT CheckItem(CLinkFrame &Req, CLinkFrame &Rep);
		UINT WriteItem(CLinkFrame &Req, CLinkFrame &Rep);
		UINT WriteItemEx(CLinkFrame &Req, CLinkFrame &Rep);
		UINT WriteData(CLinkFrame &Req, CLinkFrame &Rep);
		UINT WriteVersion(CLinkFrame &Req, CLinkFrame &Rep);
		UINT HaltSystem(CLinkFrame &Req, CLinkFrame &Rep);
		UINT StartSystem(CLinkFrame &Req, CLinkFrame &Rep);
		UINT WriteTime(CLinkFrame &Req, CLinkFrame &Rep);
		UINT ReadItem(CLinkFrame &Req, CLinkFrame &Rep);
		UINT ReadData(CLinkFrame &Req, CLinkFrame &Rep);
		UINT FlashMount(CLinkFrame &Req, CLinkFrame &Rep);
		UINT FlashDismount(CLinkFrame &Req, CLinkFrame &Rep);
		UINT FlashVerify(CLinkFrame &Req, CLinkFrame &Rep);
		UINT FlashFormat(CLinkFrame &Req, CLinkFrame &Rep);
		UINT CheckCompresion(CLinkFrame &Req, CLinkFrame &Rep);
		UINT CheckControl(CLinkFrame &Req, CLinkFrame &Rep);

		// Item Helpers
		void FreeItem(void);
		void InitItem(UINT uItem, UINT uClass, UINT uSize, UINT uComp);
		BOOL WriteItem(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Configuration Service
//

global	ILinkService *	Create_ConfigService(void)
{
	return New CConfigService;
	}

// Constructor

CConfigService::CConfigService(void)
{
	m_pData = NULL;
	}

// ILinkService

void CConfigService::Timeout(void)
{
	}

UINT CConfigService::Process(CLinkFrame &Req, CLinkFrame &Rep)
{
	if( Req.GetService() == servConfig ) {

		switch( Req.GetOpcode() ) {

			case configReadIP:
				return ReadIPConfig(Req, Rep);

			case configCheckVersion:
				return CheckVersion(Req, Rep);

			case configClearConfig:
				return ClearConfig(Req, Rep);

			case configClearGarbage:
				return ClearGarbage(Req, Rep);

			case configCheckItem:
				return CheckItem(Req, Rep);

			case configWriteItem:
				return WriteItem(Req, Rep);

			case configWriteItemEx:
				return WriteItemEx(Req, Rep);

			case configWriteData:
				return WriteData(Req, Rep);

			case configWriteVersion:
				return WriteVersion(Req, Rep);

			case configHaltSystem:
				return HaltSystem(Req, Rep);

			case configStartSystem:
				return StartSystem(Req, Rep);

			case configWriteTime:
				return WriteTime(Req, Rep);

			case configReadItem:
				return ReadItem(Req, Rep);

			case configReadData:
				return ReadData(Req, Rep);

			case configFlashMount:
				return FlashMount(Req, Rep);

			case configFlashDismount:
				return FlashDismount(Req, Rep);

			case configFlashVerify:
				return FlashVerify(Req, Rep);

			case configFlashFormat:
				return FlashFormat(Req, Rep);

			case configCheckCompression:
				return CheckCompresion(Req, Rep);

			case configCheckControl:
				return CheckControl(Req, Rep);
			}
		}

	return procError;
	}

void CConfigService::EndLink(CLinkFrame &Req)
{
	}

// Implementation

UINT CConfigService::ReadIPConfig(CLinkFrame &Req, CLinkFrame &Rep)
{
	UINT uPtr  = 0;
	
	UINT uPort = UINT(Req.ReadByte(uPtr));

	CIPAddr IP;

	CIPAddr Mask;

	CMACAddr Mac;

	IRouter *pRouter = g_pRouter;

	if( pRouter && pRouter->GetConfig(uPort, IP, Mask, Mac) ) {

		CString Addr = IP.GetAsText();
		
		Rep.StartFrame(servConfig, opReply);

		Rep.AddData(PBYTE(PCTXT(Addr)), Addr.GetLength()+1);

		return procOkay;
		}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
	}

UINT CConfigService::CheckVersion(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	if( g_pDbase->IsValid() ) {

		UINT   uPtr  = 0;

		PCBYTE pData = Req.ReadData(uPtr, 16);

		BYTE   bGuid[16];

		g_pDbase->GetVersion(bGuid);

		if( !memcmp(bGuid, pData, 16) ) {

			Rep.StartFrame(servConfig, opReply);

			Rep.AddByte(1);

			return procOkay;
			}
		}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
	}

UINT CConfigService::ClearConfig(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	g_pDbase->Clear();

	Rep.StartFrame(servConfig, opAck);

	return procOkay;
	}

UINT CConfigService::ClearGarbage(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	if( g_pDbase->GarbageCollect() ) {

		Rep.StartFrame(servConfig, opReply);

		Rep.AddByte(1);

		return procOkay;
		}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
	}

UINT CConfigService::CheckItem(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	UINT  uPtr   = 0;

	UINT  uItem  = Req.ReadWord(uPtr);

	UINT  uClass = Req.ReadWord(uPtr);

	CItemInfo Info;

	if( g_pDbase->GetItemInfo(uItem, Info) ) {

		DWORD CRC = Req.ReadLong(uPtr);

		if( Info.m_uClass == uClass || Info.m_uClass == 0 ) {

			if( Info.m_CRC == CRC ) {

				Rep.StartFrame(servConfig, opReply);

				Rep.AddByte(1);

				return procOkay;
				}
			}
		}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
	}

UINT CConfigService::WriteItem(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	UINT  uPtr   = 0;

	UINT  uItem  = Req.ReadWord(uPtr);

	UINT  uClass = Req.ReadWord(uPtr);

	UINT  uSize  = Req.ReadLong(uPtr);

	if( g_pDbase->CheckSpace(uSize) ) {

		InitItem(uItem, uClass, uSize, uSize);

		Rep.StartFrame(servConfig, opReply);

		Rep.AddByte(1);

		return procOkay;
		}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
	}

UINT CConfigService::WriteItemEx(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	UINT  uPtr   = 0;

	UINT  uItem  = Req.ReadWord(uPtr);

	UINT  uClass = Req.ReadWord(uPtr);

	UINT  uSize  = Req.ReadLong(uPtr);

	UINT  uComp  = Req.ReadLong(uPtr);

	if( g_pDbase->CheckSpace(uComp) ) {

		InitItem(uItem, uClass, uSize, uComp);

		Rep.StartFrame(servConfig, opReply);

		Rep.AddByte(1);

		return procOkay;
		}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
	}

UINT CConfigService::WriteData(CLinkFrame &Req, CLinkFrame &Rep)
{
	if( m_pData ) {

		UINT  uPtr   = 0;

		DWORD dwAddr = Req.ReadLong(uPtr);

		WORD  wCount = Req.ReadWord(uPtr);

		PBYTE pData  = PBYTE(Req.ReadData(uPtr, wCount));

		if( dwAddr + wCount <= m_Info.m_uComp ) {

			memcpy(m_pData + dwAddr, pData, wCount);

			if( dwAddr + wCount == m_Info.m_uComp ) {

				if( !WriteItem() ) {

					Rep.StartFrame(servConfig, opReply);

					Rep.AddByte(0);

					return procOkay;
					}
				}

			Rep.StartFrame(servConfig, opReply);

			Rep.AddByte(1);

			return procOkay;
			}
		}
	
	return procError;
	}

UINT CConfigService::WriteVersion(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	UINT   uPtr  = 0;

	PCBYTE pData = Req.ReadData(uPtr, 16);

	ImageDisable();

	g_pDbase->SetRunning(FALSE);

	g_pDbase->SetVersion (pData);

	g_pDbase->SetRevision(0);
	
	g_pDbase->SetValid(TRUE);

	Rep.StartFrame(servConfig, opAck);

	return procOkay;
	}

UINT CConfigService::HaltSystem(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	UINT uPtr     = 0;

	UINT uTimeout = Req.ReadLong(uPtr);

	BOOL fStopped = SystemStop(uTimeout);

	Rep.StartFrame(servConfig, opReply);

	if( fStopped ) {

		g_pDbase->SetValid(FALSE);

		Rep.AddByte(1);
		}
	else
		Rep.AddByte(0);

	return procOkay;
	}

UINT CConfigService::StartSystem(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	SystemStart();

	Rep.StartFrame(servConfig, opAck);

	return procOkay;
	}

UINT CConfigService::WriteTime(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	UINT   uPtr  = 0;

	PCBYTE pTime = Req.ReadData(uPtr, 9);

	CTime Time;

	Time.uSeconds = pTime[0];
	Time.uMinutes = pTime[1];
	Time.uHours   = pTime[2];
	Time.uDate    = pTime[3];
	Time.uMonth   = pTime[4];
	Time.uYear    = pTime[5];

	SetTime(Time);

	WORD wZone = MAKEWORD(pTime[7], pTime[6]);

	BOOL fDST  = pTime[8];

	SetTimeZoneMins(wZone, fDST);

	SystemTimeChange();

	Rep.StartFrame(servConfig, opAck);

	return procOkay;
	}

UINT CConfigService::ReadItem(CLinkFrame &Req, CLinkFrame &Rep)
{
	FreeItem();

	UINT uPtr  = 0;

	UINT uItem = Req.ReadWord(uPtr);

	Rep.StartFrame(servConfig, opReply);

	CItemInfo Info;

	if( g_pDbase->GetItemInfo(uItem, Info) ) {

		Rep.AddLong(Info.m_uComp);

		Rep.AddLong(Info.m_uSize);

		return procOkay;
		}

	Rep.AddLong(0);

	return procOkay;
	}

UINT CConfigService::ReadData(CLinkFrame &Req, CLinkFrame &Rep)
{
	UINT uPtr    = 0;

	UINT uItem   = Req.ReadWord(uPtr);

	UINT uAddr   = Req.ReadLong(uPtr);

	UINT uCount  = Req.ReadWord(uPtr);

	CItemInfo Info;

	PCBYTE pItem = PCBYTE(g_pDbase->LockItem(uItem, Info));

	if( pItem ) {

		if( uAddr + uCount <= Info.m_uComp ) {

			Rep.StartFrame(servConfig, opReply);

			Rep.AddData(pItem + uAddr, uCount);

			g_pDbase->FreeItem(uItem);

			return procOkay;
			}

		g_pDbase->FreeItem(uItem);
		}

	return procError;
	}

UINT CConfigService::FlashMount(CLinkFrame &Req, CLinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	if( EnableMount(TRUE) ) {

		Rep.AddByte(1);

		return procOkay;
		}

	Rep.AddByte(0);

	return procOkay;
	}

UINT CConfigService::FlashDismount(CLinkFrame &Req, CLinkFrame &Rep)
{
	// TODO -- Implement!!!

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(1);

	return procOkay;
	}

UINT CConfigService::FlashVerify(CLinkFrame &Req, CLinkFrame &Rep)
{
	// TODO -- Implement!!!

	UINT uPtr = 0;

	BOOL fRaw = Req.ReadByte(uPtr);

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(1);
	
	return procOkay;
	}

UINT CConfigService::FlashFormat(CLinkFrame &Req, CLinkFrame &Rep)
{
	// TODO -- Implement!!!

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(1);
	
	return procOkay;
	}

UINT CConfigService::CheckCompresion(CLinkFrame &Req, CLinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	if( g_pDbase->CanCompress() ) {

		Rep.AddByte(1);

		return procOkay;
		}

	Rep.AddByte(0);

	return procOkay;
	}

UINT CConfigService::CheckControl(CLinkFrame &Req, CLinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	if( IsRunningControl() ) {

		Rep.AddByte(1);

		return procOkay;
		}

	Rep.AddByte(0);

	return procOkay;
	}

// Item Helpers

void CConfigService::FreeItem(void)
{
	if( m_pData ) {

		delete m_pData;

		m_pData = NULL;
		}
	}

void CConfigService::InitItem(UINT uItem, UINT uClass, UINT uSize, UINT uComp)
{
	m_Info.m_uItem  = uItem;

	m_Info.m_uClass = uClass;

	m_Info.m_uSize  = uSize;

	m_Info.m_uComp  = uComp;

	m_pData         = New BYTE [ uComp ];
	}

BOOL CConfigService::WriteItem(void)
{
	if( m_pData ) {

		if( g_pDbase->WriteItem(m_Info, m_pData) ) {

			FreeItem();

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

// End of File
