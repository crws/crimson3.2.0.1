
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shared Runtime
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "g3slave.hpp"

#include "xpusb.hpp"

////////////////////////////////////////////////////////////////////////
//
// Usb Client Transport
//

class CUsbClient : public IUsbFuncEvents
{
	public:
		// Constructor
		CUsbClient(UINT uPriority, ILinkService *pService);

		// Destructor
		~CUsbClient(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbEvents
		void METHOD OnBind(IUsbDriver *pDriver);
		BOOL METHOD GetDriverInterface(IUsbDriver *&pDriver);
		void METHOD OnInit(void);
		void METHOD OnStart(void);
		void METHOD OnStop(void);

		// IUsbFuncEvents
		void METHOD OnState(UINT uState);
		BOOL METHOD OnSetupClass(UsbDeviceReq &r);
		BOOL METHOD OnSetupVendor(UsbDeviceReq &r);
		BOOL METHOD OnSetupOther(UsbDeviceReq &r);
		BOOL METHOD OnGetDevice(UsbDesc &d);
		BOOL METHOD OnGetQualifier(UsbDesc &d);
		BOOL METHOD OnGetConfig(UsbDesc &d);
		BOOL METHOD OnGetInterface(UsbDesc &d);
		BOOL METHOD OnGetEndpoint(UsbDesc &d);
		BOOL METHOD OnGetString(UsbDesc *&p, UINT iIndex);

	protected:
		// Data
		ULONG                m_uRefs;
		UINT		     m_uPriority;
		ILinkService       * m_pService;
		UINT                 m_iInterface;
		IUsbFuncCompDriver * m_pDriver;
		IEvent             * m_pEvent;
		IThread            * m_pThread;
		UINT                 m_iEndptSend;
		UINT                 m_iEndptRecv;
		BOOL		     m_fInit;
		WORD		     m_wTag;
		CLinkFrame	     m_Req;
		CLinkFrame	     m_Rep;
		G3CB		     m_CB;
		BYTE		     m_bPad;
		G3SB		     m_SB;
		BYTE		     m_bRxData[2048];
		PBYTE		     m_pCode;
		UINT		     m_uCode;

		// Thread Entry
		void ThreadEntry(void);

		// Implementation
		void WaitHost(void);
		void WaitRunning(void);
		void Shutdown(void);

		// Frame Handlers
		BOOL GetRequest(void);
		BOOL SendReply(void);
		UINT Process(void);
		void Timeout(void);
		void EndLink(CLinkFrame &Req);
		
		// Fake Loader
		UINT CatchLoadWriteProgram(void);
		UINT CatchLoadUpdateProgram(void);

		// Transport Layer
		BOOL Send(PBYTE pData, UINT uLen, BOOL fLast);
		UINT Recv(PBYTE pData, UINT uLen, UINT uTimeout);

		// Thread Entry
		static int ThreadUsbClient(IThread *pThread, void *pParam, UINT uParam); 
	};

////////////////////////////////////////////////////////////////////////
//
// Usb Client Transport
//

// Instantiator

global IUsbFuncEvents * Create_UsbLink(UINT uPriority, ILinkService *pService)
{
	return New CUsbClient(uPriority, pService);
	}

// Constructor

CUsbClient::CUsbClient(UINT uPriority, ILinkService *pService)
{
	StdSetRef();

	m_pEvent    = Create_ManualEvent();
	
	m_pDriver   = NULL;

	m_uPriority = uPriority;

	m_pService  = pService;

	m_pThread   = NULL;

	m_pCode     = NULL;

	m_uCode     = 0;
	}

// Destructor

CUsbClient::~CUsbClient(void)
{
	AfxRelease(m_pDriver);

	m_pEvent->Release();
	}

// IUnknown

HRESULT CUsbClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbFuncEvents);

	StdQueryInterface(IUsbFuncEvents);

	StdQueryInterface(IUsbEvents);

	return E_NOINTERFACE;
	}

ULONG CUsbClient::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbClient::Release(void)
{
	StdRelease();
	}

// IUsbEvents

BOOL CUsbClient::GetDriverInterface(IUsbDriver *&pDriver)
{
	return FALSE;
	}

void CUsbClient::OnBind(IUsbDriver *pDriver)
{
	pDriver->QueryInterface(AfxAeonIID(IUsbFuncCompDriver), (void **) &m_pDriver);

	m_iInterface = m_pDriver->FindInterface(this);
	}

void CUsbClient::OnInit(void)
{
	m_iEndptRecv = m_pDriver->GetFirsEndptAddr(m_iInterface) + 0;

	m_iEndptSend = m_pDriver->GetFirsEndptAddr(m_iInterface) + 1;
	}

void CUsbClient::OnStart(void)
{
	m_pThread = CreateThread(ThreadUsbClient, m_uPriority, this, 0);
	}

void CUsbClient::OnStop(void)
{
	if( m_pThread ) {
		
		m_pThread->Destroy();

		m_pThread = NULL;
		}
	}

// IUsbFuncEvents

void CUsbClient::OnState(UINT uState)
{
	if( uState == devConfigured ) {

		m_pEvent->Set();
		}
	else {
		m_pEvent->Clear();
		}
	}

BOOL CUsbClient::OnSetupClass(UsbDeviceReq &Req)
{
	return FALSE;
	}

BOOL CUsbClient::OnSetupVendor(UsbDeviceReq &Req)
{
	return FALSE;
	}

BOOL CUsbClient::OnSetupOther(UsbDeviceReq &Req)
{
	return FALSE;
	}

BOOL CUsbClient::OnGetDevice(UsbDesc &Desc)
{
	return FALSE;
	}

BOOL CUsbClient::OnGetQualifier(UsbDesc &Desc)
{
	return FALSE;
	}

BOOL CUsbClient::OnGetConfig(UsbDesc &Desc)
{
	return FALSE;
	}

BOOL CUsbClient::OnGetInterface(UsbDesc &Desc)
{
	UsbInterfaceDesc &Int = (UsbInterfaceDesc &) Desc;

	Int.m_bEndpoints = 2;
			
	Int.m_bClass	 = devVendor;
			
	Int.m_bSubClass	 = devVendor;
			
	Int.m_bProtocol	 = devVendor;
			
	return TRUE;
	}

BOOL CUsbClient::OnGetEndpoint(UsbDesc &Desc)
{
	UsbEndpointDesc &Ep = (UsbEndpointDesc &) Desc;

	if( Ep.m_bAddr == 1 ) {

		Ep.m_bDirIn    = FALSE;

		Ep.m_bTransfer = eptBulk;

		return TRUE;
		}

	if( Ep.m_bAddr == 2 ) {

		Ep.m_bDirIn    = TRUE;

		Ep.m_bTransfer = eptBulk;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUsbClient::OnGetString(UsbDesc *&pDesc, UINT i)
{
	return FALSE;
	}

// Thread Entry

void CUsbClient::ThreadEntry(void)
{
	WaitHost();

	for(;;) {

		WaitRunning();

		if( GetRequest() ) {

			UINT p = Process();

			if( p == procError ) {

				m_Rep.StartFrame(m_Req.GetService(), opNak);
				}

			SendReply();

			if( p == procEndLink ) {

				Sleep(50);

				Shutdown();

				Sleep(50);

				EndLink(m_Req);

				ExitThread(0);
				}
			}
		}
	}

// Implementation

void CUsbClient::WaitHost(void)
{
	m_fInit = true;

	SetTimer(g_uTimeout);

	while( GetTimer() ) {

		if( m_pDriver->GetState() >= devDefault && m_pDriver->GetState() <= devConfigured ) {

			return;
			}

		Sleep(20);
		}
			
	Timeout();

	m_fInit = false;
	}

void CUsbClient::WaitRunning(void)
{
	m_pEvent->Wait(FOREVER);
	}

void CUsbClient::Shutdown(void)
{
	m_pThread->Release();
	
	m_pThread = NULL;

	if( m_pDriver ) {

		m_pDriver->Stop();
		}
	}

// Frame Handlers

BOOL CUsbClient::GetRequest(void)
{
	for(;;) {

		UINT uTimeout = m_fInit ? g_uTimeout : FOREVER;

		if( Recv(PBYTE(&m_CB), sizeof(m_CB), uTimeout) == sizeof(m_CB) ) {

			if( m_CB.m_dwSig == SIG_G3CB ) {

				m_wTag = m_CB.m_wTag;

				m_Req.StartFrame(m_CB.m_bService, m_CB.m_bOpcode);

				m_Req.SetFlags(m_CB.m_bFlags);

				m_Req.AddData(m_CB.m_bData, m_CB.m_bDataSize);

				if( m_CB.m_wBulkSize ) {

					INT nCount = MotorToHost(m_CB.m_wBulkSize);

					while( nCount > 0 ) {

						UINT uSize = Recv(m_bRxData, Min(sizeof(m_bRxData), nCount), 5000);

						if( uSize != NOTHING ) {

							m_Req.AddData(m_bRxData, uSize);

							nCount -= uSize;

							continue;
							}

						return FALSE;
						}
					}

				return TRUE;
				}
			}
		else {
			if( m_fInit ) {

				Timeout();

				m_fInit = FALSE;
				}

			return FALSE;
			}
		}
	}

BOOL CUsbClient::SendReply(void)
{
	m_SB.m_dwSig   = SIG_G3SB;

	m_SB.m_wTag    = m_wTag;

	m_SB.m_bOpcode = m_Rep.GetOpcode();

	UINT  uCount = m_Rep.GetDataSize();

	PBYTE pData  = m_Rep.GetData();

	if( m_Rep.GetOpcode() == opReply ) {

		if( uCount == 1 ) {

			if( *pData == 0 ) {

				m_SB.m_bOpcode = opReplyFalse;

				uCount = 0;
				}

			if( *pData == 1 ) {

				m_SB.m_bOpcode = opReplyTrue;

				uCount = 0;
				}
			}
		}

	if( uCount ) {

		m_SB.m_bReadBulk = 1;

		m_SB.m_wBulkSize = HostToMotor(WORD(uCount));
			
		if( Send(PBYTE(&m_SB), sizeof(m_SB), FALSE) ) {

			while( uCount ) {

				UINT uSize = min(uCount, 1024);

				if( Send(pData, uSize, FALSE) ) {

					uCount -= uSize;

					pData  += uSize;
					}
				}

			m_SB.m_dwSig     = SIG_G3SB;

			m_SB.m_wTag      = m_wTag;

			m_SB.m_bOpcode   = m_Rep.GetOpcode();

			m_SB.m_bReadBulk = 0;

			m_SB.m_wBulkSize = uCount;

			return Send(PBYTE(&m_SB), sizeof(m_SB), TRUE);
			}

		return FALSE;
		}

	m_SB.m_bReadBulk = 0;

	m_SB.m_wBulkSize = 0;

	return Send(PBYTE(&m_SB), sizeof(m_SB), TRUE);
	}

UINT CUsbClient::Process(void)
{
	if( m_pService ) {

		if( m_Req.GetService() == servProm ) {

			switch( m_Req.GetOpcode() ) {
				
				case promWriteProgram:

					return CatchLoadWriteProgram();
				
				case promUpdateProgram:

					return CatchLoadUpdateProgram();
				}
			}

		return m_pService->Process(m_Req, m_Rep);
		}

	return procError;
	}

void CUsbClient::Timeout(void)
{
	if( m_pService ) {

		m_pService->Timeout();
		}
	}

void CUsbClient::EndLink(CLinkFrame &Req)
{
	if( m_pService ) {

		m_pService->EndLink(Req);
		}
	}
		
// Fake Loader

UINT CUsbClient::CatchLoadWriteProgram(void)
{
	if( g_pBootProgram ) {

		UINT  uPtr   = 0;

		DWORD dwAddr = m_Req.ReadLong(uPtr);

		UINT  uCount = m_Req.ReadWord(uPtr);

		PBYTE pData  = PBYTE(m_Req.ReadData(uPtr, uCount));

		UINT  uAlloc = 2 * 1024 * 1024;

		if( !dwAddr ) {

			if( m_pCode ) {
			
				delete m_pCode;			
				}

			m_pCode = New BYTE [ uAlloc ];			

			m_uCode = 0;
			}

		if( (dwAddr + uCount) <= uAlloc ) {			

			memcpy(m_pCode + dwAddr, pData, uCount);

			m_uCode += uCount;

			m_Rep.StartFrame(servProm, opAck);

			return procOkay;
			}
		}	

	m_Rep.StartFrame(servProm, opNak);

	return procOkay;
	}

UINT CUsbClient::CatchLoadUpdateProgram(void)
{
	if( g_pBootProgram ) {

		UINT  uPtr    = 0;	
	
		BOOL  fRemote = m_Req.ReadByte(uPtr);

		if( g_pBootProgram->ClearProgram(8) ) {

			if( g_pBootProgram->WriteProgram(m_pCode, m_uCode) ) {

				m_Rep.StartFrame(servProm, opAck);			

				delete m_pCode;

				m_pCode = NULL;

				return procOkay;
				}
			}

		delete m_pCode;
		}

	m_Rep.StartFrame(servProm, opNak);

	return procOkay;
	}

// Transport Layer

BOOL CUsbClient::Send(PBYTE pData, UINT uLen, BOOL fLast)
{
	if( m_pDriver ) {

		if( m_pDriver->SendBulk(m_iEndptSend, pData, uLen, FOREVER) == uLen ) {

			if( fLast && !(uLen % m_pDriver->GetMaxPacket(m_iEndptSend)) ) {

				return m_pDriver->SendBulk(m_iEndptSend, NULL, 0, FOREVER) == 0;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CUsbClient::Recv(PBYTE pData, UINT uLen, UINT uTimeout)
{
	if( m_pDriver ) {

		return m_pDriver->RecvBulk(m_iEndptRecv, pData, uLen, uTimeout);
		}

	return NOTHING;
	}

// Thread Entry

int CUsbClient::ThreadUsbClient(IThread *pThread, void *pParam, UINT uParam)
{
	pThread->SetName("XpUsb");

	((CUsbClient *) pParam)->ThreadEntry();

	return 0;
	}

// End of File
