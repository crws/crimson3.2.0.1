
//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3SLAVE_HPP
	
#define	INCLUDE_G3SLAVE_HPP

////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3link.hpp"

////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

struct ILinkService;
struct ISystemConsole;

////////////////////////////////////////////////////////////////////////
//
// Processing Results
//

enum
{
	procError,
	procOkay,
	procEndLink
	};

////////////////////////////////////////////////////////////////////////
//
// G3 Link Client Service
//

struct ILinkService
{
	// Methods
	virtual void Timeout(void)			       = 0;
	virtual UINT Process(CLinkFrame &Req, CLinkFrame &Rep) = 0;
	virtual void EndLink(CLinkFrame &Req)		       = 0;
	};

////////////////////////////////////////////////////////////////////////
//
// Service Instantiators
//

extern ILinkService   * Create_RouterService(void);

extern ILinkService   * Create_BootService(void);

extern ILinkService   * Create_ConfigService(void);

extern ILinkService   * Create_TunnelService(void);

extern ILinkService   * Create_DataService(void);

extern ILinkService   * Create_LoadService(void);

extern ILinkService   * Create_StratonService(void);

extern ISystemConsole * Create_SystemConsoleService(void);

//////////////////////////////////////////////////////////////////////////
//								
// Globals
//

extern UINT g_uTimeout;

//////////////////////////////////////////////////////////////////////////
//								
// Globals
//

extern void RunBoot(DWORD dwTimeout);

extern BOOL RackTunnel(BYTE bDrop, PBYTE pData);

// End of File

#endif
