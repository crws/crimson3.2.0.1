
#include "intern.hpp"

#include "NandBootProgram.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

// Externals

extern void ChainCode(PCBYTE pImage, UINT uImage, UINT uParam);

//////////////////////////////////////////////////////////////////////////
//
// Nand Firmware Programming Object
//

// Instantiator

global IFirmwareProgram * Create_NandBootProgram(UINT uStart, UINT uEnd)
{
	return New CNandBootProgram(uStart, uEnd);
	}

// Constructor

CNandBootProgram::CNandBootProgram(UINT uStart, UINT uEnd) : CNandBootProps(uStart, uEnd)
{
	m_uPtr   = 0;

	m_fWrite = false;
	}

// Destructor

CNandBootProgram::~CNandBootProgram(void)
{
	}

// IFirmwareProps

bool CNandBootProgram::IsCodeValid(void)
{
	return CNandBootProps::IsCodeValid();
	}

PCBYTE CNandBootProgram::GetCodeVersion(void)
{
	return CNandBootProps::GetCodeVersion();
	}

UINT CNandBootProgram::GetCodeSize(void)
{
	return CNandBootProps::GetCodeSize();
	}

PCBYTE CNandBootProgram::GetCodeData(void)
{
	return CNandBootProps::GetCodeData();
	}

// IFirmwareProgram

bool CNandBootProgram::ClearProgram(UINT uBlocks)
{
	UINT uCount = 0;

	CNandBlock Block  = m_BlockStart;

	do {
		if( m_pNandMemory->EraseBlock(Block.m_uChip, Block.m_uBlock) ) {			

			if( !uCount ++ ) {

				m_uPtr       = 0;

				delete m_pImage;

				m_pImage     = NULL;

				m_uImage     = 0;

				m_fWrite     = true;
				}

			continue;
			}

		} while( GetNextBlock(Block) );		

	return 2 * uCount >= uBlocks;
	}

bool CNandBootProgram::WriteProgram(PCBYTE pData, UINT uCount)
{
	if( m_fWrite ) {
		
		AllocImage();

		CNandPage Page = m_BlockStart;

		#if defined(_M_IMX51)

		UINT   uOffset = 1024;

		#else

		UINT   uOffset = 0;		

		#endif

		while( uCount ) {

			UINT uCopy = uCount;

			MakeMin(uCopy, m_uPageSize - uOffset);

			memcpy( m_pPageData + uOffset, 
				pData + m_uImage, 
				uCopy
				);

			if( WritePage(Page) ) {

				memcpy( m_pImage + m_uImage, 
					m_pPageData + uOffset, 
					uCopy
					);

				m_uImage += uCopy;

				uCount   -= uCopy;				

				if( !GetNextPage(Page) ) {
				
					break;
					}

				uOffset = 0;

				continue;
				}
				
			return false;
			}

		return true;
		}

	return false;
	}

bool CNandBootProgram::WriteVersion(PCBYTE pData)
{
	return false;
	}

bool CNandBootProgram::StartProgram(void)
{
	if( ReadCode() ) {

		#if defined(_M_IMX51)

		ChainCode(m_pImage, m_uImage, 0);

		#else

		PCBYTE pCode  = m_pImage + 8;

		UINT   uCode  = PDWORD(m_pImage)[0] - 8;

		ChainCode(pCode, uCode, 0);

		#endif
		
		return true;
		}

	return false;
	}

// Implementation

bool CNandBootProgram::WritePage(CNandPage Page)
{
	return m_pNandMemory->WritePage( Page.m_uChip, 
					 Page.m_uBlock, 
					 Page.m_uPage, 
					 m_pPageData
					 );
	}

// End of File

