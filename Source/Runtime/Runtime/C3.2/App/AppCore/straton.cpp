
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "g3slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Straton Service
//

class CStraton : public ILinkService
{
	public:
		// Constructor
		CStraton(void);

		// ILinkService
		void Timeout(void);
		UINT Process(CLinkFrame &Req, CLinkFrame &Rep);
		void EndLink(CLinkFrame &Req);

	protected:
		// Data Members
		BYTE m_bData[1024];
	};

//////////////////////////////////////////////////////////////////////////
//
// Straton Service
//

// Instantiator

global ILinkService * Create_StratonService(void)
{
	return New CStraton;
	}

// Constructor

CStraton::CStraton(void)
{
	}

// ILinkService

void CStraton::Timeout(void)
{
	}

UINT CStraton::Process(CLinkFrame &Req, CLinkFrame &Rep)
{
	if( Req.GetService() == servStraton ) {

		if( Req.GetOpcode() == stratonService ) {

			UINT   uPtr  = 0;

			PCBYTE pHead = Req.ReadData(uPtr, 4);

			UINT   uSize = MAKEWORD(pHead[3], pHead[2]);

			PCBYTE pRest = Req.ReadData(uPtr, uSize);

			// Straton doens't always clear these...

			m_bData[0] = 0;
			m_bData[1] = 0;
			m_bData[2] = 0;
			m_bData[3] = 0;

			extern BOOL Straton_Service(PCBYTE pIn, PBYTE pOut);

			if( Straton_Service(pHead, m_bData) ) {

				UINT uSize = MAKEWORD(m_bData[3], m_bData[2]);

				UINT uSend = 4 + uSize;

				Rep.StartFrame(servStraton, stratonService);

				Rep.AddData(m_bData, uSend);

				return procOkay;
				}
			}
		}

	return procError;
	}

void CStraton::EndLink(CLinkFrame &Req)
{
	}

// End of File
