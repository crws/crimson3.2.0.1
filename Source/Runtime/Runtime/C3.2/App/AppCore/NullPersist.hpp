
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NullPersist_HPP

#define INCLUDE_NullPersist_HPP

//////////////////////////////////////////////////////////////////////////
//
// Null Persistance Manager
//

class CNullPersist : public IPersist
{
	public:
		// Constructor
		CNullPersist(void);

		// IPersist
		void  Init(void);
		void  Term(void);
		void  ByeBye(void);
		BOOL  IsDirty(void);
		void  Commit(BOOL fReset);
		BYTE  GetByte(DWORD dwAddr);
		WORD  GetWord(DWORD dwAddr);
		LONG  GetLong(DWORD dwAddr);
		void  GetData(PBYTE pData, DWORD dwAddr, UINT uCount);
		void  PutByte(DWORD dwAddr, BYTE bData);
		void  PutWord(DWORD dwAddr, WORD wData);
		void  PutLong(DWORD dwAddr, LONG lData);
		void  PutData(PBYTE pData, DWORD dwAddr, UINT uCount);

	protected:
		// Data Members
		PBYTE m_pData;
		BOOL  m_fDirty;
	};

// End of File

#endif
