
#include "Intern.hpp"

#include "NandFlashEventStorage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Event Storage
//

// Macros

#define PassSector(s)	s.m_uBlock, s.m_uPage, s.m_uSector

#define PassPage  (p)	p.m_uBlock, p.m_uPage

// Instantiator

global IEvStg * Create_NandFlashEventStorage(UINT uStart, UINT uEnd)
{
	CNandBlock Start(0, uStart);

	CNandBlock End  (0, uEnd);

	return New CNandFlashEventStorage(Start, End);
	}

// Constructor

CNandFlashEventStorage::CNandFlashEventStorage(CNandBlock const &Start, CNandBlock const &End)
{
	m_BlockStart  = Start;

	m_BlockEnd    = End;

	m_pManager    = NULL;

	m_SectorHead  = m_BlockStart;

	m_SectorTail  = m_BlockStart;

	m_fFoundEmpty = TRUE;
	}

// Destructor

CNandFlashEventStorage::~CNandFlashEventStorage(void)
{
	}

// IEvStg

void CNandFlashEventStorage::Init(void)
{
	CNandClient::Init();

	FindQueue();
	}

void CNandFlashEventStorage::BindManager(CEventManager *pManager)
{
	m_pManager = pManager;
	}

BOOL CNandFlashEventStorage::ReadMemory(CEventLogger *pLog)
{
	BYTE bGuid[16];

	g_pDbase->GetVersion(bGuid);

	CNandSector Scan = m_SectorHead;

	for( UINT uSlot = 0;; uSlot ++) {

		if( ReadSector(Scan) ) {

			PCSECTOR pSector = PCSECTOR(m_pPageData);

			if( pSector->Magic == magicSlot ) {

				if( !memcmp(&pSector->Guid,  bGuid, 16) ) {
						
					PCSLOT pSlot = PCSLOT(m_pPageData);

					if( pLog ) {
				
						if( pSlot->m_HasText ) {						

							CEventInfo Info;

							SlotToInfo(Info, pSlot);

							Info.m_Text    = pSlot->m_Text;

							pLog->LogInitEvent(Info);
							}						
						else {
							CEventInfo Info;

							SlotToInfo(Info, pSlot);

							IEventSource *pSource = m_pManager->FindSource(Info.m_Source);

							if( pSource ) {

								pSource->GetEventText(Info.m_Text, Info.m_Code);
								}

							pLog->LogInitEvent(Info);
							}
						}
					}
				}
			}

		if( Scan == m_SectorTail ) {
		
			return TRUE;
			}

		if( !GetNextGoodSector(Scan) ) {

			Scan = m_BlockStart;
			}
		}

	return FALSE;
	}

BOOL CNandFlashEventStorage::WriteMemory(CEventInfo const &Info)
{
	PSECTOR pSector = PSECTOR(m_pPageData);

	BYTE bGuid[16];

	g_pDbase->GetVersion(bGuid);

	pSector->Magic = magicSlot;

	memcpy(&pSector->Guid, bGuid, 16);

	pSector->Mark = m_Next ++;

	PSLOT     pSlot = PSLOT(pSector);

	InfoToSlot(pSlot, Info);

	if( Info.m_HasText ) {

		wstrcpy(pSlot->m_Text, Info.m_Text);
		}

	CNandSector Sector = m_SectorTail;

	if( FindNextSector(Sector) ) {

		m_SectorCurrent = Sector;

		if( WriteWithReloc(Sector) ) {

			m_SectorTail = Sector;

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CNandFlashEventStorage::ClearMemory(void)
{
	EraseAll(true);

	m_SectorHead = m_BlockStart;

	m_SectorTail = m_BlockStart;

	return FALSE;
	}

// Write with Relocation

BOOL CNandFlashEventStorage::WriteWithReloc(CNandSector &Sector)
{
	// NOTE -- This is quite subtle. We are writing a New page to a block, possibly at
	// the start of the block, but most likely further down. If the write fails, we have
	// to copy the existing data from the original target block and then try again, but
	// the copy may fail or the further attempt to write may fail.
	
	CNandBlock  const ThisBlock  = m_SectorCurrent;

	CNandSector const StopSector = m_SectorCurrent;

	BOOL              fMarkBad   = FALSE;

	while( !WriteSector(Sector, true) ) {

		CNandBlock NextBlock = m_SectorCurrent;

		for(;;) {

			if( FindNextBlock(NextBlock) ) {

				CNandSector FromSector = CNandSector(ThisBlock);

				CNandSector DestSector = CNandSector(NextBlock);

				if( CopySectors(DestSector, FromSector, StopSector) ) {

					m_SectorCurrent = DestSector;
					
					break;
					}

				if( DestSector == m_BlockEnd ) {

					NextBlock = DestSector;					
					}

				continue;
				}

			return FALSE;
			}
		}

	if( fMarkBad ) {

		MarkBlockBad(ThisBlock);
		}

	Sector = m_SectorCurrent;

	return TRUE;
	}

BOOL CNandFlashEventStorage::CopySectors(CNandSector &Dest, CNandSector &From, CNandSector const &Stop)
{
	PBYTE pCopyData = PBYTE(malloc(m_Geometry.m_uBytes));

	while( From < Stop ) {

		if( ReadSector(From, pCopyData) ) {

			if( WriteSector(Dest, pCopyData, true) ) {

				if( m_SectorHead == From ) {
					
					m_SectorHead = Dest;
					}

				if( m_SectorTail == From ) {
					
					m_SectorTail = Dest;
					}

				if( !GetNextGoodSector(Dest) ) {

					free(pCopyData);

					return FALSE;
					}

				if( !GetNextGoodSector(From) ) {

					free(pCopyData);

					return FALSE;
					}

				continue;
				}

			MarkBlockBad(Dest);
			}

		free(pCopyData);

		return FALSE;
		}

	free(pCopyData);

	return TRUE;
	}

// Implementation

BOOL CNandFlashEventStorage::FindNextBlock(CNandBlock &Next)
{
	BOOL fWrap = TRUE;

	for(;;) {

		if( !GetNextGoodBlock(Next) ) {

			if( fWrap ) {

				Next = m_BlockStart;

				if( !SkipBadBlocks(Next) ) {

					return FALSE;
					}

				fWrap = FALSE;
				}
			else
				return FALSE;
			}

		if( Next == m_SectorHead ) {

			if( !GetNextGoodBlock(m_SectorHead) ) {

				m_SectorHead = m_BlockStart;

				if( !SkipBadBlocks(Next) ) {

					return FALSE;
					}
				}
			}

		if( EraseBlock(Next, true) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CNandFlashEventStorage::FindNextSector(CNandSector &Next)
{
	CNandSector Scan = Next;

	if( ++Scan.m_uSector >= m_Geometry.m_uSectors ) {

		Scan.m_uSector = 0;

		if( ++Scan.m_uPage >= m_Geometry.m_uPages ) {

			Scan.m_uPage = 0;

			if( !FindNextBlock(Scan) ) {

				return FALSE;
				}
			}
		}

	Next = Scan;

	return TRUE;
	}

BOOL CNandFlashEventStorage::IsEmpty(void)
{
	return m_fFoundEmpty;
	}

void CNandFlashEventStorage::FindQueue(void)
{
	m_SectorHead  = m_BlockStart;

	m_SectorTail  = m_BlockStart;

	UINT     uMin = NOTHING;

	UINT     uMax = 0;

	BYTE bGuid[16];

	g_pDbase->GetVersion(bGuid);

	CNandPage Page;

	Page.Invalidate();

	while( GetNextGoodPage(Page) ) {

		if( ReadPage(Page) ) {

			PBYTE pPage = m_pPageData;

			for( UINT uSect = 0; uSect < m_Geometry.m_uSectors; uSect ++ ) {

				PCSECTOR pSector = PCSECTOR(pPage + (uSect * m_Geometry.m_uBytes));

				if( pSector->Magic == magicSlot ) {

					UINT uMark = pSector->Mark;

					if( uMark < uMin ) {

						uMin = uMark;

						m_SectorHead = CNandSector(Page, uSect);
						}

					if( uMark > uMax ) {

						uMax = uMark;

						m_SectorTail = CNandSector(Page, uSect);
						}

					m_fFoundEmpty = FALSE;

					DebugShowQueue();

					continue;
					}

				if( pSector->Magic == magicEmpty ) {

					continue;
					}

				EraseBlock(Page, true);

				break;
				}

			continue;
			}

		EraseBlock(Page, true);
		}

	m_Next = uMax + 1;

	DebugShowQueue();
	}

void CNandFlashEventStorage::MarkSector(PSECTOR pSector)
{
	BYTE bGuid[16];

	g_pDbase->GetVersion(bGuid);

	pSector->Magic = magicSlot;

	memcpy(&pSector->Guid, bGuid, 16);

	pSector->Mark = m_Next ++;
	}

// Slot Helpers

void CNandFlashEventStorage::SlotToInfo(CEventInfo &Info, PCSLOT pSlot)
{
	Info.m_Time      = pSlot->m_Time;

	Info.m_Type      = pSlot->m_Type;

	Info.m_Source    = pSlot->m_Source;

	Info.m_HasText   = pSlot->m_HasText;
	
	Info.m_Code      = pSlot->m_Code;
	}

void CNandFlashEventStorage::InfoToSlot(PSLOT pSlot, CEventInfo const &Info)
{
	pSlot->m_Time    = Info.m_Time;

	pSlot->m_Type    = Info.m_Type;

	pSlot->m_Source  = Info.m_Source;

	pSlot->m_HasText = Info.m_HasText;
	
	pSlot->m_Code    = Info.m_Code;
	}

// Debug

void CNandFlashEventStorage::DebugShowQueue(void)
{
	#if 0

	AfxTrace("head %d.%d.%d\n", m_SectorHead.m_uBlock, m_SectorHead.m_uPage, m_SectorHead.m_uSector);

	AfxTrace("tail %d.%d.%d\n", m_SectorTail.m_uBlock, m_SectorTail.m_uPage, m_SectorTail.m_uSector);

	#endif
	}

// End of File
