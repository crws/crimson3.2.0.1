
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_loader_HPP

#define INCLUDE_loader_HPP
      
//////////////////////////////////////////////////////////////////////////
//
// Loader Counter Object
//

class CLoadCounter
{
	public:
		// Constructor
		CLoadCounter(void);

		// Assignment Operator
		CLoadCounter const & operator = (BYTE b);

		// Arithmetic Operators
		CLoadCounter const & operator += (BYTE b);
		CLoadCounter const & operator -= (BYTE b);

		// Comparison Operator
		BOOL operator >= (BYTE) const;

		// Operations
		void SetFlag(BOOL fState);

		// Attributes
		BYTE GetData(void) const;		
		BOOL GetFlag(void);

	protected:
		// Data
		BYTE	m_bData[2];

		// Implementation
		void Load(void);
		void Save(void);
	};

// End of File

#endif
