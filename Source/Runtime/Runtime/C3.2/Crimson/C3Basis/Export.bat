@echo off

rem %1 = build path

mkdir "%~1include\runtime\c3.2" 2>nul

copy c3basis.hpp "%~1include\runtime\c3.2" >nul
copy align.hpp   "%~1include\runtime\c3.2" >nul
copy icomms.hpp  "%~1include\runtime\c3.2" >nul
copy ascii.h     "%~1include\runtime\c3.2" >nul

attrib -r "%~1include\runtime\c3.2"\*.*
 