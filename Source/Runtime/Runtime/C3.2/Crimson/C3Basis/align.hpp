
//////////////////////////////////////////////////////////////////////////
//
// ARM Alignment Helpers
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ALIGN_HPP

#define	INCLUDE_ALIGN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class RU2;
class CRU2;
class PU2;
class U2;

//////////////////////////////////////////////////////////////////////////
//
// Reference to WORD
//

class RU2
{
	public:
		// Constructor
		RU2(WORD &v)
		{
			m_p = PBYTE(&v);
			}

		// Conversion
		operator WORD (void) const
		{
			return MAKEWORD( m_p[0],
					 m_p[1]
					 );
			}

		// Assignment
		WORD operator = (WORD v)
		{
			m_p[0] = LOBYTE(v);
			m_p[1] = HIBYTE(v);

			return v;
			}

	protected:
		// Data Members
		PBYTE m_p;
	};

//////////////////////////////////////////////////////////////////////////
//
// Constant Reference to WORD
//

class CRU2
{
	public:
		// Constructor
		CRU2(WORD const &v)
		{
			m_p = PCBYTE(&v);
			}

		// Conversion
		operator WORD (void) const
		{
			return MAKEWORD( m_p[0],
					 m_p[1]
					 );
			}

	protected:
		// Data Members
		PCBYTE m_p;
	};

//////////////////////////////////////////////////////////////////////////
//
// Pointer to WORD
//

class PU2
{
	public:
		// Constructor
		PU2(void)
		{
			m_p = NULL;
			}

		// Constructor
		PU2(PVOID p)
		{
			m_p = PBYTE(p);
			}

		// Constructor
		PU2(PU2 const &that)
		{
			m_p = that.m_p;
			}

		// Assignment
		PU2 const & operator = (PVOID p)
		{
			m_p = PBYTE(p);

			return *this;
			}

		// Assignment
		PU2 const & operator = (PU2 const &that)
		{
			m_p = that.m_p;

			return *this;
			}

		// Conversion
		operator void * (void) const
		{
			return m_p;
			}

		// Dereferencing
		RU2 operator * (void) const
		{
			return RU2(PWORD(m_p)[0]);
			}

		// Indexing
		RU2 operator [] (UINT n) const
		{
			return RU2(PWORD(m_p)[n]);
			}

		// Preincrement
		PU2 & operator ++ (int)
		{
			m_p += 2;

			return *this;
			}

		// Postincrement
		PU2 operator ++ (void)
		{
			PU2 p(m_p);

			m_p += 2;

			return p;
			}

	protected:
		// Data Members
		PBYTE m_p;
	};

//////////////////////////////////////////////////////////////////////////
//
// WORD Wrapper
//

class U2
{
	public:
		// Constructor
		U2(void)
		{
			}

		// Constructor
		U2(WORD v)
		{
			RU2(m_w) = v;
			}

		// Conversion
		operator WORD (void) const
		{
			return WORD(CRU2(m_w));
			}

		// Assignment
		WORD operator = (WORD v)
		{
			return RU2(m_w) = v;
			}

	protected:
		WORD m_w;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class RU4;
class CRU4;
class PU4;
class U4;

//////////////////////////////////////////////////////////////////////////
//
// Reference to DWORD
//

class RU4
{
	public:
		// Constructor
		RU4(DWORD &v)
		{
			m_p = PBYTE(&v);
			}

		// Conversion
		operator DWORD (void) const
		{
			return MAKELONG( MAKEWORD( m_p[0],
						   m_p[1]
						   ),
					 MAKEWORD( m_p[2],
						   m_p[3]
						   )
					 );
			}

		// Assignment
		DWORD operator = (DWORD v)
		{
			m_p[0] = LOBYTE(LOWORD(v));
			m_p[1] = HIBYTE(LOWORD(v));
			m_p[2] = LOBYTE(HIWORD(v));
			m_p[3] = HIBYTE(HIWORD(v));

			return v;
			}

	protected:
		// Data Members
		PBYTE m_p;
	};

//////////////////////////////////////////////////////////////////////////
//
// Constant Reference to DWORD
//

class CRU4
{
	public:
		// Constructor
		CRU4(DWORD const &v)
		{
			m_p = PCBYTE(&v);
			}

		// Conversion
		operator DWORD (void) const
		{
			return MAKELONG( MAKEWORD( m_p[0],
						   m_p[1]
						   ),
					 MAKEWORD( m_p[2],
						   m_p[3]
						   )
					 );
			}

	protected:
		// Data Members
		PCBYTE m_p;
	};

//////////////////////////////////////////////////////////////////////////
//
// Pointer to DWORD
//

class PU4
{
	public:
		// Constructor
		PU4(void)
		{
			m_p = NULL;
			}

		// Constructor
		PU4(PVOID p)
		{
			m_p = PBYTE(p);
			}

		// Constructor
		PU4(PU4 const &that)
		{
			m_p = that.m_p;
			}

		// Assignment
		PU4 const & operator = (PVOID p)
		{
			m_p = PBYTE(p);

			return *this;
			}

		// Assignment
		PU4 const & operator = (PU4 const &that)
		{
			m_p = that.m_p;

			return *this;
			}

		// Conversion
		operator void * (void) const
		{
			return m_p;
			}

		// Dereferencing
		RU4 operator * (void) const
		{
			return RU4(PDWORD(m_p)[0]);
			}

		// Indexing
		RU4 operator [] (UINT n) const
		{
			return RU4(PDWORD(m_p)[n]);
			}

		// Preincrement
		PU4 & operator ++ (int)
		{
			m_p += 4;

			return *this;
			}

		// Postincrement
		PU4 operator ++ (void)
		{
			PU4 p(m_p);

			m_p += 4;

			return p;
			}

	protected:
		// Data Members
		PBYTE m_p;
	};

//////////////////////////////////////////////////////////////////////////
//
// DWORD Wrapper
//

class U4
{
	public:
		// Constructor
		U4(void)
		{
			}

		// Constructor
		U4(DWORD v)
		{
			RU4(m_w) = v;
			}

		// Conversion
		operator DWORD (void) const
		{
			return DWORD(CRU4(m_w));
			}

		// Assignment
		DWORD operator = (DWORD v)
		{
			return RU4(m_w) = v;
			}

	protected:
		// Data Members
		DWORD m_w;
	};

// End of File

#endif
