
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Printer Driver Implementation
//

// Constructor

CPrintDriver::CPrintDriver(void)
{
	m_wState = 0;
	}

// IUnknown

HRESULT CPrintDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ICommsPrinter);

	return CCommsDriver::QueryInterface(riid, ppObject);
	}

ULONG CPrintDriver::AddRef(void)
{
	return CCommsDriver::AddRef();
	}

ULONG CPrintDriver::Release(void)
{
	return CCommsDriver::Release();
	}

// IDriver

WORD CPrintDriver::GetCategory(void)
{
	return DC_COMMS_PRINTER;
	}

// ICommsPrinter

void CPrintDriver::OutChar(char cData)
{
	}

void CPrintDriver::OutString(PCTXT pText)
{
	for( int n = 0; pText[n]; n++ ) {
	
		char cData = pText[n];
		
		if( cData < 32 ) {
		
			if( cData == '\n' ) {

				OutNewLine();
				}
			}
		else
			OutChar(cData);
		}
	}

void CPrintDriver::OutNewLine(void)
{
	OutChar(0x0D);

	OutChar(0x0A);
	}

void CPrintDriver::OutFormFeed(void)
{
	OutChar(0x0C);
	}

// End of File
