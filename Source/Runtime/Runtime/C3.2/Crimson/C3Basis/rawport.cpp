
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Raw Port Driver Implementation
//

// Constructor

CRawPortDriver::CRawPortDriver(void)
{
	}

// IUnknown

HRESULT CRawPortDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ICommsRawPort);

	return CCommsDriver::QueryInterface(riid, ppObject);
	}

ULONG CRawPortDriver::AddRef(void)
{
	return CCommsDriver::AddRef();
	}

ULONG CRawPortDriver::Release(void)
{
	return CCommsDriver::Release();
	}

// IDriver

WORD CRawPortDriver::GetCategory(void)
{
	return DC_COMMS_RAWPORT;
	}

// ICommsRawPort

void CRawPortDriver::Connect(void)
{
	}

void CRawPortDriver::Disconnect(void)
{
	}

UINT CRawPortDriver::Read(UINT uTime)
{
	if( m_fOpen ) {
		
		return m_pData->Read(uTime);
		}
	
	Sleep(uTime);
	
	return NOTHING;
	}

UINT CRawPortDriver::Write(BYTE bData, UINT uTime)
{
	if( m_fOpen ) {
		
		return m_pData->Write(bData, uTime);
		}

	Sleep(uTime);

	return 0;
	}

UINT CRawPortDriver::Write(PCBYTE pData, UINT uCount, UINT uTime)
{
	if( m_fOpen ) {

		return m_pData->Write(pData, uCount, uTime);
		}

	Sleep(uTime);

	return 0;
	}

void CRawPortDriver::SetRTS(BOOL fState)
{
	}

BOOL CRawPortDriver::GetCTS(void)
{
	return FALSE;
	}

// Implementation

BOOL CRawPortDriver::SkipUpdate(LPCBYTE &pData)
{
	if( GetWord(pData) == 0x1234 ) {

		if( GetByte(pData) == 0x01 ) {
		       
			if( GetWord(pData) == 0x1234 ) {

				UINT uCount = GetWord(pData);

				pData += 4 * uCount;

				Item_Align4(pData);

				uCount = GetWord(pData);
				
				pData += 1 * uCount;
			
				return TRUE;
				}
	
			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// End of File
