
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Keyboard Driver Implementation
//

// Constructor

CKeyboardDriver::CKeyboardDriver(void)
{
	m_pKeyboardHelper = NULL;

	AfxGetObject("helper-keyboard", 0, IKeyboardHelper, m_pKeyboardHelper);

	AfxAssert(m_pKeyboardHelper);
	}

// IDriver

WORD CKeyboardDriver::GetCategory(void)
{
	return DC_COMMS_KEYBOARD;
	}

// Keyboard Request

void CKeyboardDriver::PostKey(WORD wCode)
{
	m_pKeyboardHelper->PostKey(wCode);
	}

// End of File
