
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Streamer Driver Implementation
//

// Constructor

CStreamerDriver::CStreamerDriver(void)
{
	}

// IUnknown

HRESULT CStreamerDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ICommsStreamer);

	return CRawPortDriver::QueryInterface(riid, ppObject);
	}

ULONG CStreamerDriver::AddRef(void)
{
	return CRawPortDriver::AddRef();
	}

ULONG CStreamerDriver::Release(void)
{
	return CRawPortDriver::Release();
	}

// IDriver

WORD CStreamerDriver::GetCategory(void)
{
	return DC_COMMS_STREAMER;
	}

// ICommStreamer

DWORD CStreamerDriver::GetNextTime(DWORD dwTime)
{
	return NOTHING;
	}

PCTXT CStreamerDriver::GetFilename(DWORD dwTime)
{
	return "test.bin";
	}

UINT CStreamerDriver::GetDataCount(void)
{
	return 0;
	}

PCBYTE CStreamerDriver::GetDataPointer(void)
{
	return NULL;
	}

DWORD CStreamerDriver::GetValue(UINT uIndex)
{
	return 0;
	}

void CStreamerDriver::FreeData(void)
{
	}

// End of File
