
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Master Driver Implementation
//

// Constructor

CMasterDriver::CMasterDriver(void)
{
	}

// IUnknown

HRESULT CMasterDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ICommsMaster);

	return CCommsDriver::QueryInterface(riid, ppObject);
	}

ULONG CMasterDriver::AddRef(void)
{
	return CCommsDriver::AddRef();
	}

ULONG CMasterDriver::Release(void)
{
	return CCommsDriver::Release();
	}

// IDriver

WORD CMasterDriver::GetCategory(void)
{
	return DC_COMMS_MASTER;
	}

// Master Flags

WORD CMasterDriver::GetMasterFlags(void)
{
	return 0;
	}

CCODE CMasterDriver::Ping(void)
{
	return 2;
	}

CCODE CMasterDriver::Read(AREF Addr, PDWORD pData, UINT  uCount)
{
	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CMasterDriver::Write(AREF Addr, PDWORD pData, UINT  uCount)
{
	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CMasterDriver::Atomic(AREF Addr, DWORD  Data,  DWORD dwMask)
{
	return CCODE_ERROR | CCODE_HARD;
	}

// End of File
