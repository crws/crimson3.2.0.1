
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Motorola Data Packer
//

// Constructor

CMotorDataPacker::CMotorDataPacker(PDWORD pData, UINT uCount, BOOL fSign) 
{
	m_pData  = pData;

	m_uCount = uCount;

	m_fSign  = fSign;
	}

// Pack

void CMotorDataPacker::Pack(PBYTE pData) const
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		pData[i] = (BYTE) m_pData[i];
		}
	}

void CMotorDataPacker::Pack(PWORD pData) const
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		pData[i] = HostToMotor((WORD) m_pData[i]);
		}
	}

void CMotorDataPacker::Pack(PDWORD pData) const
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		pData[i] = HostToMotor(m_pData[i]);
		}
	}

// Unpack

void CMotorDataPacker::Unpack(PBYTE pData)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		BYTE bData = pData[i];

		m_pData[i] = m_fSign ? (LONG)(CHAR) bData : bData;
		}
	}

void CMotorDataPacker::Unpack(PWORD pData)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		WORD wData = MotorToHost(pData[i]);

		m_pData[i] = m_fSign ? (LONG)(SHORT) wData : wData;
		}
	}

void CMotorDataPacker::Unpack(PDWORD pData)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		m_pData[i] = MotorToHost(pData[i]);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Intel Data Packer
//

// Constructor

CIntelDataPacker::CIntelDataPacker(PDWORD pData, UINT uCount, BOOL fSign) 
{
	m_pData  = pData;

	m_uCount = uCount;

	m_fSign  = fSign;
	}

// Pack

void CIntelDataPacker::Pack(PBYTE pData) const
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		pData[i] = (BYTE) m_pData[i];
		}
	}

void CIntelDataPacker::Pack(PWORD pData) const
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		pData[i] = HostToIntel((WORD) m_pData[i]);
		}
	}

void CIntelDataPacker::Pack(PDWORD pData) const
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		pData[i] = HostToIntel(m_pData[i]);
		}
	}

// Unpack

void CIntelDataPacker::Unpack(PBYTE pData)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		BYTE bData = pData[i];

		m_pData[i] = m_fSign ? (LONG)(CHAR) bData : bData;
		}
	}

void CIntelDataPacker::Unpack(PWORD pData)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		WORD wData = IntelToHost(pData[i]);

		m_pData[i] = m_fSign ? (LONG)(SHORT) wData : wData;
		}
	}

void CIntelDataPacker::Unpack(PDWORD pData)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {

		m_pData[i] = IntelToHost(pData[i]);
		}
	}

// End of File
