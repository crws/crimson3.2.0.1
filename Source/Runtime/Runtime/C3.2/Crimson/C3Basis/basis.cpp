
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Memory Management
//

void * operator new (size_t uSize, UINT uDummy, void *pData)
{
	return pData;
	}

// End of File
