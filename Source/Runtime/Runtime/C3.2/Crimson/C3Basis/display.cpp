
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display Driver Implementation
//

// Constructor

CDisplayDriver::CDisplayDriver(void)
{
	}

// IUnknown

HRESULT CDisplayDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ICommsDisplay);

	return CCommsDriver::QueryInterface(riid, ppObject);
	}

ULONG CDisplayDriver::AddRef(void)
{
	return CCommsDriver::AddRef();
	}

ULONG CDisplayDriver::Release(void)
{
	return CCommsDriver::Release();
	}

// IDriver

WORD CDisplayDriver::GetCategory(void)
{
	return DC_COMMS_DISPLAY;
	}

// ICommsDisplay

UINT CDisplayDriver::GetSize(int cx, int cy)
{
	return ((2 * cx * cy) / 8) + 16;
	}

void CDisplayDriver::Capture(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait)
{
	}

void CDisplayDriver::Service(UINT uDrop, PBYTE pPrev, PBYTE pData, UINT uSize)
{
	}

// End of File
