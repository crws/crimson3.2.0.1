
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Core Runtime
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3FACES_HPP

#define	INCLUDE_C3FACES_HPP

//////////////////////////////////////////////////////////////////////////
//
// Standard Aeon Header
//

#include <StdEnv.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CJsonData;

class CJsonConfig;

interface ICrimsonApp;

interface ICrimsonPxe;

//////////////////////////////////////////////////////////////////////////
//
// Database Item Information
//

struct CItemInfo
{
	UINT  m_uItem;
	UINT  m_uClass;
	UINT  m_uSize;
	UINT  m_uComp;
	DWORD m_CRC;
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Information
//

struct CEventInfo
{
	DWORD	 m_Time;
	DWORD	 m_Type   :2;
	DWORD    m_Source :3;
	DWORD    m_HasText:1;
	DWORD    m_Code   :26;
	CUnicode m_Text;
	};

//////////////////////////////////////////////////////////////////////////
//
// Active Alarm
//

struct CActiveAlarm
{
	DWORD		m_Time;
	UINT		m_Source;
	UINT		m_Code;
	UINT		m_Level;
	CUnicode	m_Text;
	UINT		m_State;
	PVOID		m_pData;
	CActiveAlarm *	m_pNext;
	CActiveAlarm *	m_pPrev;
	};

//////////////////////////////////////////////////////////////////////////
//
// User Info
//

struct CUserInfo
{
	CString		m_User;
	CString		m_Real;
	BOOL		m_fLocal;
	UINT		m_uWebAccess;
	UINT		m_uFtpAccess;
};

//////////////////////////////////////////////////////////////////////////
//
// Auth Info
//

struct CAuthInfo
{
	CString		m_Password;
	CByteArray	m_Challenge;
	CByteArray	m_Response;
};

//////////////////////////////////////////////////////////////////////////
//
// PXE Interface
//

interface ICrimsonPxe : public IUnknown
{
	AfxDeclareIID(200, 1);

	virtual BOOL METHOD LockApp(ICrimsonApp * &pApp)		                             = 0;
	virtual void METHOD FreeApp(void)                                                            = 0;
	virtual void METHOD GetInitTime(time_t &time)						     = 0;
	virtual void METHOD SetSysWebLink(CString const &Link)	                                     = 0;
	virtual BOOL METHOD GetSysWebLink(CString &Link)		                             = 0;
	virtual void METHOD SetAppWebLink(CString const &Link)	                                     = 0;
	virtual BOOL METHOD GetAppWebLink(CString &Link)		                             = 0;
	virtual void METHOD SetAlarm(UINT uAlarm)                                                    = 0;
	virtual UINT METHOD GetAlarm(void)                                                           = 0;
	virtual void METHOD SetSiren(BOOL fSiren)                                                    = 0;
	virtual BOOL METHOD GetSiren(void)                                                           = 0;
	virtual UINT METHOD GetLedMode(void)                                                         = 0;
	virtual void METHOD SetLedMode(UINT uMode)                                                   = 0;
	virtual BOOL METHOD SystemStop(void)                                                         = 0;
	virtual BOOL METHOD SystemStart(void)			                                     = 0;
	virtual void METHOD SystemUpdate(UINT hItem, PBYTE pData, UINT uSize)                        = 0;
	virtual void METHOD RestartSystem(UINT uTimeout, UINT uExitCode)                             = 0;
	virtual BOOL METHOD ImageSave(PCTXT pName)	                                             = 0;
	virtual void METHOD KickTimeout(UINT uIndex)	                                             = 0;
	virtual UINT METHOD GetDefCertStep(void)	                                             = 0;
	virtual BOOL METHOD GetDefCertData(CByteArray *pData, CString &Pass)                         = 0;
	virtual BOOL METHOD GetHostName(CString &Name)			                             = 0;
	virtual BOOL METHOD GetUnitName(CString &Name)			                             = 0;
	virtual UINT METHOD GetStatus(void)				                             = 0;
	virtual BOOL METHOD GetPersonality(CString const &Name, CString &Data)                       = 0;
	virtual BOOL METHOD SetPersonality(CString const &Name, CString const &Data)                 = 0;
	virtual BOOL METHOD CommitPersonality(BOOL fRestart)                                         = 0;
	virtual void METHOD IdentifyUnit(void)					                     = 0;
	virtual BOOL METHOD GetUserInfo(CString const &User, CAuthInfo const &Auth, CUserInfo &Info) = 0;
	virtual BOOL METHOD SetUserPass(CString const &User, CString const &From, CString const &To) = 0;
	virtual WORD METHOD GetG3LinkPort(void)                                                      = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// App Interface
//

interface ICrimsonApp : public IUnknown
{
	AfxDeclareIID(200, 2);

	virtual void METHOD BindPxe(ICrimsonPxe *pPxe)          = 0;
	virtual BOOL METHOD LoadConfig(CString &Error)          = 0;
	virtual BOOL METHOD InitApp(void)                       = 0;
	virtual BOOL METHOD HaltApp(void)                       = 0;
	virtual BOOL METHOD TermApp(void)                       = 0;
	virtual BOOL METHOD IsRunningControl(void)              = 0;
	virtual BOOL METHOD GetData(DWORD &Data, DWORD Ref)     = 0;
	virtual BOOL METHOD StratonCall(PCBYTE pIn, PBYTE pOut) = 0;
	virtual BOOL METHOD RackTunnel(BYTE bDrop, PBYTE pData) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Database Manager Interface
//

interface IDatabase : public IUnknown
{
	AfxDeclareIID(200, 3);

	virtual void   METHOD Init(void)				     = 0;
	virtual void   METHOD Clear(void)				     = 0;
	virtual BOOL   METHOD IsValid(void)				     = 0;
	virtual void   METHOD SetValid(BOOL fValid)			     = 0;
	virtual void   METHOD SetRunning(BOOL fRun)			     = 0;
	virtual BOOL   METHOD GarbageCollect(void)			     = 0;
	virtual BOOL   METHOD GetVersion(PBYTE pGuid)			     = 0;
	virtual DWORD  METHOD GetRevision(void)				     = 0;
	virtual BOOL   METHOD SetVersion(PCBYTE pData)			     = 0;
	virtual BOOL   METHOD SetRevision(DWORD dwRes)			     = 0;
	virtual BOOL   METHOD CheckSpace(UINT uSize)			     = 0;
	virtual BOOL   METHOD WriteItem(CItemInfo const &Item, PCVOID pData) = 0;
	virtual BOOL   METHOD GetItemInfo(UINT uItem, CItemInfo &Info)	     = 0;
	virtual PCVOID METHOD LockItem(UINT uItem, CItemInfo &Info)	     = 0;
	virtual PCVOID METHOD LockItem(UINT uItem)			     = 0;
	virtual void   METHOD PendItem(UINT uItem, BOOL fHold)		     = 0;
	virtual void   METHOD LockPendingItems(BOOL fLock)		     = 0;
	virtual void   METHOD FreeItem(UINT uItem)			     = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Persistance Manager Interface
//

interface IPersist : public IUnknown
{
	AfxDeclareIID(200, 4);

	virtual void  METHOD Init(void)					     = 0;
	virtual void  METHOD Term(void)					     = 0;
	virtual void  METHOD ByeBye(void)				     = 0;
	virtual BOOL  METHOD IsDirty(void)				     = 0;
	virtual void  METHOD Commit(BOOL fReset)			     = 0;
	virtual BYTE  METHOD GetByte(DWORD dwAddr)			     = 0;
	virtual WORD  METHOD GetWord(DWORD dwAddr)			     = 0;
	virtual LONG  METHOD GetLong(DWORD dwAddr)			     = 0;
	virtual void  METHOD GetData(PBYTE pData, DWORD dwAddr, UINT uCount) = 0;
	virtual void  METHOD PutByte(DWORD dwAddr, BYTE bData)		     = 0;
	virtual void  METHOD PutWord(DWORD dwAddr, WORD wData)		     = 0;
	virtual void  METHOD PutLong(DWORD dwAddr, LONG lData)		     = 0;
	virtual void  METHOD PutData(PBYTE pData, DWORD dwAddr, UINT uCount) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Logger Interface
//

interface IEventLogger : public IUnknown
{
	AfxDeclareIID(200, 5);

	virtual void	 LockEventData(void)              = 0;
	virtual void	 FreeEventData(void)              = 0;
	virtual UINT     GetEventSequence(void)           = 0;
	virtual UINT     GetEventCount(void)              = 0;
	virtual CUnicode GetEventText(UINT uPos)          = 0;
	virtual CUnicode GetEventName(UINT uPos)          = 0;
	virtual DWORD    GetEventTime(UINT uPos)          = 0;
	virtual UINT     GetEventType(UINT uPos)          = 0;
	virtual void	 AddEvent(CEventInfo const &Info) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Source Interface
//

interface IEventSource : public IUnknown
{
	AfxDeclareIID(200, 6);

	virtual BOOL GetEventText(CUnicode &Text, UINT Code) = 0;
	virtual void AcceptAlarm(UINT Method, UINT Code)     = 0;
	virtual void AcceptAlarm(CActiveAlarm *pInfo)        = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Manager Interface
//

interface IEventManager : public IUnknown
{
	AfxDeclareIID(200, 7);

	virtual IEventSource * FindSource(UINT Source) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Storage Interface
//

interface IEventStorage : public IUnknown
{
	AfxDeclareIID(200, 8);

	virtual void METHOD Init(void)				 = 0;
	virtual void METHOD BindManager(IEventManager *pManager) = 0;
	virtual BOOL METHOD ReadMemory(IEventLogger *pLogger)	 = 0;
	virtual BOOL METHOD WriteMemory(CEventInfo const &Info)  = 0;
	virtual BOOL METHOD ClearMemory(void)			 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Identity Interface
//

interface ICrimsonIdentity : public IUnknown
{
	AfxDeclareIID(200, 9);

	virtual void METHOD Init(void)				 = 0;
	virtual int  METHOD GetOemName(PTXT   pName, UINT uSize) = 0;
	virtual int  METHOD GetOemApp (PTXT   pName, UINT uSize) = 0;
	virtual bool METHOD GetKeyData(PBYTE  pData, UINT uSize) = 0;
	virtual bool METHOD GetSerial (PTXT   pData, UINT uSize) = 0;
	virtual bool METHOD SetOemName(PCTXT  pName, UINT uSize) = 0;
	virtual bool METHOD SetOemApp (PCTXT  pName, UINT uSize) = 0;
	virtual bool METHOD SetKeyData(PCBYTE pData, UINT uSize) = 0;
	virtual bool METHOD SetSerial (PCTXT  pName, UINT uSize) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// License Manager Interface
//

interface ILicenseManager : public IUnknown
{
	AfxDeclareIID(200, 10);

	virtual void METHOD Install(UINT LicenseId)   = 0;
	virtual void METHOD Uninstall(UINT LicenseId) = 0;
	virtual BOOL METHOD Validate(UINT LicenseId)  = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Config Update Interface
//

interface IConfigUpdate : public IUnknown
{
	AfxDeclareIID(200, 11);

	virtual void METHOD OnConfigUpdate(char cTag) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Config Storage Interface
//

interface IConfigStorage : public IUnknown
{
	AfxDeclareIID(200, 12);

	virtual bool METHOD AddUpdateSink(IConfigUpdate *pUpdate)                 = 0;
	virtual bool METHOD SetConfig(char cTag, CString const &Text, bool fEdit) = 0;
	virtual bool METHOD SetEdited(char cTag, bool fEdit)                      = 0;
	virtual bool METHOD GetConfig(char cTag, CString &Text)                   = 0;
	virtual bool METHOD IsEdited(char cTag)                                   = 0;
	virtual bool METHOD ClearData(void)					  = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Schema Generator Interface
//

interface ISchemaGenerator : public IUnknown
{
	AfxDeclareIID(200, 13);

	virtual bool METHOD Open(void)                           = 0;
	virtual bool METHOD GetDefault(char cTag, CString &Text) = 0;
	virtual bool METHOD GetSchema(char cTag, CString &Text)  = 0;
	virtual bool METHOD GetNaming(char cTag, CString &Text)  = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Hardware Applicator Interface
//

interface IHardwareApplicator : public IUnknown
{
	AfxDeclareIID(200, 15);

	virtual bool METHOD ApplySettings(CJsonConfig *pJson) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Network Applicator Interface
//

interface INetApplicator : public IUnknown
{
	AfxDeclareIID(200, 16);

	virtual bool METHOD ApplySettings(CJsonConfig *pJson)   = 0;
	virtual bool METHOD GetUnitName(CString &Name)          = 0;
	virtual bool METHOD GetmDnsNames(CStringArray &List)    = 0;
	virtual bool METHOD GetInfo(CString &Info, PCSTR pName) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Firmware Properties Interface
//

interface IFirmwareProps : public IUnknown
{
	AfxDeclareIID(200, 17);

	virtual bool   METHOD IsCodeValid(void)    = 0;
	virtual PCBYTE METHOD GetCodeVersion(void) = 0;
	virtual UINT   METHOD GetCodeSize(void)    = 0;
	virtual PCBYTE METHOD GetCodeData(void)    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//								
// Firmware Programming Interface
//

interface IFirmwareProgram : public IFirmwareProps
{
	AfxDeclareIID(200, 18);

	virtual bool METHOD ClearProgram(UINT uBlocks)	            = 0;
	virtual bool METHOD WriteProgram(PCBYTE pData, UINT uCount) = 0;
	virtual bool METHOD WriteVersion(PCBYTE pData)	            = 0;
	virtual bool METHOD StartProgram(UINT uParam)		    = 0;
};

//////////////////////////////////////////////////////////////////////////
//								
// Model Information Interface
//

interface IPxeModel : public IUnknown
{
	AfxDeclareIID(200, 19);

	virtual void    METHOD MakeAppObjects(void)                              = 0;
	virtual void    METHOD MakePxeObjects(void)                              = 0;
	virtual BOOL    METHOD AdjustHardware(CJsonData *pData)                  = 0;
	virtual BOOL    METHOD ApplyModelSpec(CString Model)                     = 0;
	virtual BOOL	METHOD AppendModelSpec(CString &Model, CJsonData *pData) = 0;
	virtual BOOL    METHOD GetDispList(CArray<DWORD> &List)                  = 0;
	virtual UINT    METHOD GetObjCount(char cTag)                            = 0;
	virtual PCDWORD METHOD GetUsbPaths(char cTag)                            = 0;
	virtual UINT    METHOD GetPortType(UINT uPort)                           = 0;
};

//////////////////////////////////////////////////////////////////////////
//								
// Location Services
//

interface IPxeLocation : public IUnknown
{
	AfxDeclareIID(200, 20);

	virtual BOOL   METHOD IsValid(void) = 0;
	virtual double METHOD GetLat(void)  = 0;
	virtual double METHOD GetLong(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Port Grabber Interface
//

interface IPortGrabber : public IUnknown
{
	AfxDeclareIID(200, 21);

	virtual BOOL METHOD HasRequest(void) = 0;
	virtual BOOL METHOD Claim(void)      = 0;
	virtual void METHOD Free(void)	     = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// USB Module Information Object
//

interface IUsbModule : public IUnknown
{
	AfxDeclareIID(200, 22);

	virtual DWORD METHOD GetSlot(void) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Device Identifier
//

interface IDeviceIdentifier : public IUnknown
{
	AfxDeclareIID(200, 23);

	virtual BOOL METHOD RunOperation(BYTE bCode, BOOL fUsb, CBuffer *pBuff) = 0;
};

// End of File

#endif
