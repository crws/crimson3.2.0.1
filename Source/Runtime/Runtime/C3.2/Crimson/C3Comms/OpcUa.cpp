
#include "intern.hpp"

#include "OpcUa.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Client Driver
//

// Instantiator

clink void CreateOpcUaDriver(void *pData, UINT *pSize)
{
	if( !pData ) {

		*pSize = sizeof(COpcUaDriver);

		return;
	}

	NewHere(pData) COpcUaDriver;
}

// Constructor

COpcUaDriver::COpcUaDriver(void)
{
	m_Ident   = 0x40E2;

	m_uDebug  = 0;

	m_pClient = NULL;

	AfxNewObject("opcua-client", IOpcUaClient, m_pClient);
}

// Destructor

COpcUaDriver::~COpcUaDriver(void)
{
	AfxRelease(m_pClient);
}

// Configuration

void MCALL COpcUaDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uDebug = GetByte(pData);

		COpcUaClientConfig Config;

		Config.m_uSize  = sizeof(Config);
		Config.m_uDebug = m_uDebug;

		m_pClient->OpcLoad(Config);
	}
}

void MCALL COpcUaDriver::CheckConfig(CSerialConfig &Config)
{
}

// Master Flags

WORD MCALL COpcUaDriver::GetMasterFlags(void)
{
	return MF_TASK_NOTIFY;
}

// Management

void MCALL COpcUaDriver::Attach(IPortObject *pPort)
{
}

void MCALL COpcUaDriver::Detach(void)
{
}

void MCALL COpcUaDriver::Open(void)
{
}

// User Access

UINT MCALL COpcUaDriver::DrvCtrl(UINT uFunc, PCTXT Value)
{
	if( uFunc == NOTHING ) {

		if( m_uDebug ) {

			AfxTrace("opc: control %s\n", Value);
		}

		if( !stricmp(Value, "Init") ) {

			m_pClient->OpcInit();
		}

		if( !stricmp(Value, "Stop") ) {

			m_pClient->OpcStop();
		}

		if( !stricmp(Value, "Term") ) {

			m_pClient->OpcTerm();
		}
	}

	return 0;
}

// Device

CCODE MCALL COpcUaDriver::DeviceOpen(ICommsDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			AfxTrace("opc: create device\n");

			m_pCtx = New CContext;

			CString Host = UniConvert(GetWide(pData));

			UINT    Port = GetWord(pData);

			CString User = UniConvert(GetWide(pData));

			CString Pass = UniConvert(GetWide(pData));

			m_pCtx->m_fPartial = GetByte(pData);

			m_pCtx->m_StrSize  = GetWord(pData);

			m_pCtx->m_StrCode  = GetByte(pData);

			m_pCtx->m_StrPack  = 4 / (m_pCtx->m_StrCode + 1);

			m_pCtx->m_StrPitch = ((m_pCtx->m_StrSize + m_pCtx->m_StrPack - 1) / m_pCtx->m_StrPack);

			m_pCtx->m_StrTable = 64000 / m_pCtx->m_StrPitch;

			if( true ) {

				CStringArray Nodes;

				m_pCtx->m_uNodes = GetWord(pData);

				m_pCtx->m_pNodes = New CNode[m_pCtx->m_uNodes];

				for( UINT n = 0; n < m_pCtx->m_uNodes; n++ ) {

					CString Node = UniConvert(GetWide(pData));

					Nodes.Append(Node);
				}

				memset(m_pCtx->m_pNodes, 0, sizeof(CNode) * m_pCtx->m_uNodes);

				m_pCtx->m_uDevice = m_pClient->OpcOpenDevice(CPrintf("opc.tcp://%s:%u", PCTXT(Host), Port),
									     User,
									     Pass,
									     Nodes
				);
			}

			if( true ) {

				m_pCtx->m_uSlots = GetWord(pData);

				m_pCtx->m_pSlots = New WORD[m_pCtx->m_uSlots];

				for( UINT s = 0; s < m_pCtx->m_uSlots; s++ ) {

					m_pCtx->m_pSlots[s] = GetWord(pData);
				}
			}

			if( true ) {

				m_pCtx->m_uScalars = m_pCtx->m_uNodes - GetWord(pData);

				for( UINT n = m_pCtx->m_uScalars; n < m_pCtx->m_uNodes; n++ ) {

					CNode &Node  = m_pCtx->m_pNodes[n];

					Node.m_uSize = GetWord(pData);

					Node.m_pData = New DWORD[Node.m_uSize];

					memset(Node.m_pData, 0, Node.m_uSize * sizeof(DWORD));
				}
			}

			m_pCtx->m_uLump  = 50;

			m_pCtx->m_fValid = TRUE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL COpcUaDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		AfxTrace("opc: delete device\n");

		m_pClient->OpcCloseDevice(m_pCtx->m_uDevice);

		for( UINT n = m_pCtx->m_uScalars; n < m_pCtx->m_uNodes; n++ ) {

			CNode &Node = m_pCtx->m_pNodes[n];

			delete[] Node.m_pData;
		}

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

void MCALL COpcUaDriver::Service(void)
{
	UINT  *pList = New UINT[m_pCtx->m_uScalars];

	UINT  *pType = New UINT[m_pCtx->m_uScalars];

	DWORD *pData = New DWORD[m_pCtx->m_uScalars];

	UINT   uRead = 0;

	{
		for( UINT n = 0; n < m_pCtx->m_uScalars; n++ ) {

			CNode &Node = m_pCtx->m_pNodes[n];

			if( Node.m_fUsed ) {

				pList[uRead] = n;

				pType[uRead] = Node.m_Type;

				pData[uRead] = 0;

				uRead++;
			}
		}
	}

	if( uRead ) {

		UINT uDone = 0;

		BOOL fGood = TRUE;

		while( uDone < uRead ) {

			UINT uLump = min(m_pCtx->m_uLump, uRead - uDone);

			if( m_pClient->OpcReadData(m_pCtx->m_uDevice, pList + uDone, uLump, pType + uDone, pData + uDone) ) {

				uDone += uLump;

				continue;
			}

			fGood = FALSE;

			break;
		}

		if( fGood ) {

			for( UINT n = 0; n < uRead; n++ ) {

				CNode &Node = m_pCtx->m_pNodes[pList[n]];

				if( pType[n] == Node.m_Type ) {

					if( pType[n] == typeString ) {

						free(PVOID(Node.m_Data));
					}

					Node.m_Data  = pData[n];

					Node.m_fGood = TRUE;
				}
			}
		}
		else {
			for( UINT n = 0; n < uRead; n++ ) {

				CNode &Node = m_pCtx->m_pNodes[pList[n]];

				Node.m_fGood = FALSE;
			}
		}
	}

	{
		for( UINT n = m_pCtx->m_uScalars; n < m_pCtx->m_uNodes; n++ ) {

			CNode &Node = m_pCtx->m_pNodes[n];

			if( Node.m_fUsed ) {

				if( m_pClient->OpcReadArray(m_pCtx->m_uDevice, n, Node.m_Type, Node.m_uSize, Node.m_pData) ) {

					Node.m_fGood = TRUE;
				}
				else {
					Node.m_fGood = FALSE;
				}
			}
		}
	}

	{
		for( UINT n = 0; n < m_pCtx->m_uNodes; n++ ) {

			CNode & Node = m_pCtx->m_pNodes[n];

			Node.m_fUsed = FALSE;
		}
	}

	delete[] pList;

	delete[] pType;

	delete[] pData;
}

CCODE MCALL COpcUaDriver::Ping(void)
{
	if( m_pClient->OpcPingDevice(m_pCtx->m_uDevice) ) {

		return CCODE_SUCCESS;
	}

	return CCODE_ERROR;
}

CCODE MCALL COpcUaDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uNode = NOTHING;

	UINT uType = 0;

	if( Addr.a.m_Table >= addrArray && Addr.a.m_Table < addrNamed ) {

		UINT uSlot = 64000 * (Addr.a.m_Table - addrArray) + Addr.a.m_Offset;

		UINT uInit = 0;

		if( Addr.a.m_Type == addrLongAsLong ) {

			uType = typeInteger;
		}
		else
			uType = typeReal;

		for( UINT n = m_pCtx->m_uScalars; n < m_pCtx->m_uNodes; n++ ) {

			CNode &Node = m_pCtx->m_pNodes[n];

			if( uSlot >= uInit && uSlot < uInit + Node.m_uSize ) {

				Node.m_Type  = uType;

				Node.m_fUsed = TRUE;

				UINT uFrom = uSlot - uInit;

				UINT uCopy = min(uCount, Node.m_uSize - uFrom);

				if( Node.m_fGood ) {

					for( UINT c = 0; c < uCopy; c++ ) {

						pData[c] = Node.m_pData[uFrom + c];
					}

					return uCopy;
				}

				return uCopy | CCODE_NO_DATA;
			}

			uInit += Node.m_uSize;
		}

		*pData = 0;

		return 1;
	}

	if( Addr.a.m_Table >= addrString && Addr.a.m_Table < addrArray ) {

		UINT uSlot = 0;

		uSlot += (Addr.a.m_Table - 1) * m_pCtx->m_StrTable;

		uSlot += (Addr.a.m_Offset / m_pCtx->m_StrPitch);

		if( uSlot < m_pCtx->m_uSlots ) {

			uType = typeString;

			uNode = m_pCtx->m_pSlots[uSlot];
		}
	}

	if( Addr.a.m_Table == addrNamed ) {

		if( Addr.a.m_Type == addrLongAsLong ) {

			uType = typeInteger;
		}
		else
			uType = typeReal;

		uNode = Addr.a.m_Offset;
	}

	if( uNode < m_pCtx->m_uScalars ) {

		CNode & Node = m_pCtx->m_pNodes[uNode];

		Node.m_Type  = uType;

		Node.m_fUsed = TRUE;

		if( Node.m_fGood ) {

			if( uType == typeString ) {

				PCUTF p = PCUTF(Node.m_Data);

				UINT  l = wstrlen(p);

				UINT  c = min(uCount, m_pCtx->m_StrSize / m_pCtx->m_StrPack);

				for( UINT n = 0; n < c; n++ ) {

					UINT cp = m_pCtx->m_StrPack * (Addr.a.m_Offset % m_pCtx->m_StrPitch + n);

					if( m_pCtx->m_StrCode == 0 ) {

						char c1 = (cp + 0 < l) ? char(p[cp + 0]) : 0;
						char c2 = (cp + 1 < l) ? char(p[cp + 1]) : 0;
						char c3 = (cp + 2 < l) ? char(p[cp + 2]) : 0;
						char c4 = (cp + 3 < l) ? char(p[cp + 3]) : 0;

						pData[n] = MAKELONG(MAKEWORD(c1, c2), MAKEWORD(c3, c4));
					}
					else {
						WCHAR c1 = (cp + 0 < l) ? p[cp + 0] : 0;
						WCHAR c2 = (cp + 1 < l) ? p[cp + 1] : 0;

						pData[n] = MAKELONG(c1, c2);
					}
				}

				return c;
			}
			else {
				*pData = Node.m_Data;

				return 1;
			}
		}

		if( uType == typeString ) {

			return CCODE_NO_DATA | uCount;
		}

		return CCODE_NO_DATA | 1;
	}

	*pData = 0;

	return 1;
}

CCODE MCALL COpcUaDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table >= addrArray && Addr.a.m_Table < addrNamed ) {

		UINT uSlot = 64000 * (Addr.a.m_Table - addrArray) + Addr.a.m_Offset;

		UINT uInit = 0;

		UINT uType;

		if( Addr.a.m_Type == addrLongAsLong ) {

			uType = typeInteger;
		}
		else
			uType = typeReal;

		for( UINT n = m_pCtx->m_uScalars; n < m_pCtx->m_uNodes; n++ ) {

			CNode &Node = m_pCtx->m_pNodes[n];

			if( uSlot >= uInit && uSlot < uInit + Node.m_uSize ) {

				if( !m_pCtx->m_fPartial && !Node.m_fGood ) {

					if( !m_pClient->OpcReadArray(m_pCtx->m_uDevice, n, uType, Node.m_uSize, Node.m_pData) ) {

						return CCODE_ERROR;
					}
				}

				UINT uFrom = uSlot - uInit;

				UINT uCopy = min(uCount, Node.m_uSize - uFrom);

				for( UINT c = 0; c < uCopy; c++ ) {

					Node.m_pData[uFrom + c] = pData[c];
				}

				if( !m_pCtx->m_fPartial ) {

					if( m_pClient->OpcWriteArray(m_pCtx->m_uDevice, n, uType, NOTHING, Node.m_uSize, Node.m_pData) ) {

						return uCopy;
					}
				}
				else {
					if( m_pClient->OpcWriteArray(m_pCtx->m_uDevice, n, uType, uFrom, uCopy, Node.m_pData + uFrom) ) {

						return uCopy;
					}
				}

				return CCODE_ERROR;
			}

			uInit += Node.m_uSize;
		}

		return uCount;
	}

	if( Addr.a.m_Table >= addrString && Addr.a.m_Table < addrArray ) {

		UINT uSlot = 0;

		uSlot += (Addr.a.m_Table - 1) * m_pCtx->m_StrTable;

		uSlot += (Addr.a.m_Offset / m_pCtx->m_StrPitch);

		AfxDump(PCBYTE(pData), 4 * uCount);

		if( uSlot < m_pCtx->m_uSlots ) {

			UINT    List[] = { m_pCtx->m_pSlots[uSlot] };

			UINT    Type[] = { typeString };

			CNode & Node   = m_pCtx->m_pNodes[List[0]];

			UINT s = m_pCtx->m_StrSize + 1;

			PUTF p = New WCHAR[s];

			memset(p, 0, 2 * s);

			if( Node.m_fGood && Node.m_Data ) {

				wstrcpy(p, PUTF(Node.m_Data));
			}
			else {
				DWORD Data = 0;

				if( !m_pClient->OpcReadData(m_pCtx->m_uDevice, List, elements(List), Type, &Data) ) {

					free(p);

					return CCODE_ERROR;
				}

				wstrcpy(p, PUTF(Data));

				free(PUTF(Data));
			}

			for( UINT n = 0; n < uCount; n++ ) {

				UINT cp = m_pCtx->m_StrPack * (Addr.a.m_Offset % m_pCtx->m_StrPitch + n);

				if( m_pCtx->m_StrCode == 0 ) {

					p[cp + 0] = LOBYTE(LOWORD(pData[n]));
					p[cp + 1] = HIBYTE(LOWORD(pData[n]));
					p[cp + 2] = LOBYTE(HIWORD(pData[n]));
					p[cp + 3] = HIBYTE(HIWORD(pData[n]));
				}
				else {
					p[cp + 0] = LOWORD(pData[n]);
					p[cp + 1] = HIWORD(pData[n]);
				}
			}

			DWORD Data = DWORD(p);

			if( m_pClient->OpcWriteData(m_pCtx->m_uDevice, List, elements(List), Type, &Data) ) {

				free(p);

				return uCount;
			}

			free(p);

			return CCODE_ERROR;
		}
	}

	if( Addr.a.m_Table == addrNamed ) {

		UINT List[] = { Addr.a.m_Offset };

		UINT Type[] = { typeInteger };

		if( Addr.a.m_Type == addrRealAsReal ) {

			Type[0] = typeReal;
		}

		m_pClient->OpcWriteData(m_pCtx->m_uDevice, List, elements(List), Type, pData);

		return uCount;
	}

	return uCount;
}

// End of File
