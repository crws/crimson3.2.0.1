
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Dummy Serial Port Driver
//

class CDummySerialDriver : public CCommsDriver
{
	public:
		// Constructor
		CDummySerialDriver(void);

		// Entry Points
		DEFMETH(void) Service(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Linked Serial Port Driver
//

class CLinkedSerialDriver : public CCommsDriver
{
	public:
		// Constructor
		CLinkedSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Entry Points
		DEFMETH(void) Service(void);

	protected:
		// Data Members
		UINT	 m_uPort;
		ISocket *m_pSock;

		// Implementation
		BOOL OpenSocket(void);
		BOOL TestSocket(void);
	};

// End of File
