
#include "intern.hpp"

#include "virtser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Dummy Serial Port Driver
//

// Instantiator

clink void CreateDummySerial(void *pData, UINT *pSize)
{
	if( !pData ) {
		
		*pSize = sizeof(CDummySerialDriver);
		
		return;
		}
		
	NewHere(pData) CDummySerialDriver;
	}

// Constructor

CDummySerialDriver::CDummySerialDriver(void)
{
	m_Ident = 0x3705;
	}

// Entry Points

void MCALL CDummySerialDriver::Service(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Linked Serial Port Driver
//

// Instantiator

clink void CreateLinkedSerial(void *pData, UINT *pSize)
{
	if( !pData ) {
		
		*pSize = sizeof(CLinkedSerialDriver);
		
		return;
		}
		
	NewHere(pData) CLinkedSerialDriver;
	}

// Constructor

CLinkedSerialDriver::CLinkedSerialDriver(void)
{
	m_pSock = NULL;
	}

// Configuration

void CLinkedSerialDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uPort = GetWord(pData);
		}
	}

// Entry Points

void CLinkedSerialDriver::Service(void)
{
	CBuffer *  pBuff  = NULL;

	UINT const uSize  = 256;

	UINT       uByte  = NOTHING;
	
	UINT       uPhase = 0;

	for(;;) {
		
		if( TestSocket() ) {
			
			if( m_pSock->Recv(pBuff) == S_OK ) {

				PCBYTE pData = PCBYTE(pBuff->GetData());

				UINT   uSize = pBuff->GetSize();

				m_pData->Write(pData, uSize, FOREVER);

				BuffRelease(pBuff);
				}
			}

		if( (uByte = m_pData->Read(20)) < NOTHING ) {

			if( OpenSocket() ) {

				while( !(pBuff = BuffAllocate(uSize)) ) {

					Sleep(10);
					}

				while( uByte < NOTHING ) {

					*pBuff->AddTail(1) = uByte;

					if( pBuff->GetSize() == uSize ) {

						break;
						}

					uByte = m_pData->Read(20);
					}

				while( m_pSock->Send(pBuff) == E_FAIL ) {

					m_pSock->GetPhase(uPhase);

					if( uPhase == PHASE_OPEN ) {

						Sleep(10);

						continue;
						}

					BuffRelease(pBuff);

					break;
					}

				SetTimer(10000);
				}
			}
		}
	}

// Implementation

BOOL CLinkedSerialDriver::OpenSocket(void)
{
	if( !TestSocket() ) {

		if( !m_pSock ) {

			m_pSock = CreateSocket(IP_TCP);

			if( m_pSock ) {

				DWORD        Loc = 0x7F000001;

				IPADDR const &IP = (IPADDR const &) Loc;

				m_pSock->Connect(IP, m_uPort);
		
				SetTimer(10000);

				while( GetTimer() ) {

					if( TestSocket() ) {

						return TRUE;
						}

					Sleep(10);
					}

				return FALSE;
				}

			return FALSE;
			}

		return TestSocket();
		}

	return TRUE;
	}

BOOL CLinkedSerialDriver::TestSocket(void)
{
	if( m_pSock ) {

		UINT uPhase = 0;

		m_pSock->GetPhase(uPhase);

		if( uPhase == PHASE_ERROR ) {

			m_pSock->Release();

			m_pSock = NULL;

			return FALSE;
			}

		if( uPhase == PHASE_CLOSING ) {

			m_pSock->Close();

			m_pSock->Release();

			m_pSock = NULL;

			return FALSE;
			}

		if( uPhase == PHASE_OPEN ) {

			if( !GetTimer() ) {

				m_pSock->Close();

				m_pSock->Release();

				m_pSock = NULL;

				return FALSE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
