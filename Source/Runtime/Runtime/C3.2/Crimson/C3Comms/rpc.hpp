
//////////////////////////////////////////////////////////////////////////
//
// Remoting Frame
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//


class CRemotingFrame
{
	public:
		// Constructor
		CRemotingFrame(void);

		// Attributes
		bool IsValid(void);
		bool IsRaw(void);

		// Frame Creattion
		void Start(WORD wService, WORD wOpcode);
		void Start(WORD wService, WORD wOpcode, WORD wTransact);
		void End(void);
		void Clear(void);
		void SetService(WORD wService);
		void SetOpcode(WORD wOpcode);
		void SetTransact(WORD wTransact);
		void SetCount(WORD wCount);

		// Simple Data
		void AddByte(BYTE  bData);
		void AddWord(WORD  wData);
		void AddLong(LONG  lData);
		void AddGuid(GUID  Guid);
		void AddText(PCTXT pText);

		// Larger Data
		void AddData(PCBYTE pData, UINT uCount);

		// Data Reads
		PCBYTE ReadData(UINT &uPtr, UINT uCount) const;
		GUID & ReadGuid(UINT &uPtr) const;
		BYTE   ReadByte(UINT &uPtr) const;
		WORD   ReadWord(UINT &uPtr) const;
		DWORD  ReadLong(UINT &uPtr) const;

		// Attributes
		DWORD GetMagic(void) const;
		WORD  GetService(void) const;
		WORD  GetOpcode(void) const;
		WORD  GetTransact(void) const;
		UINT  GetCount(void) const;
		UINT  GetSize(void) const;
		PBYTE GetData(void) const;
		UINT  GetSpace(void) const;
		UINT  GetHeadSize(void) const;
		UINT  GetLimit(void) const;
	
	protected:
		// Header
		enum
		{
			Magic	= 0x4C696E6B,
			};

		// Frames
		BYTE m_bData[8192-1];
		WORD m_wCount;
		WORD m_wTrans;
	};

//////////////////////////////////////////////////////////////////////////
//
// Remoting Link
//

class CRemotingLink
{
	public:
		// Constructor
		CRemotingLink(void);

		// Destructor
		~CRemotingLink(void);

		// Management
		void Attach(IPortObject  *pPort);
		void Attach(IDataHandler *pData);
		void Detach(void);

		// Attributes
		CRemotingFrame const & GetReq(void) const;
		CRemotingFrame const & GetRep(void) const;

		// Port
		bool CheckPort(void);
		bool ClaimPort(void);
		void FreePort(void);
			
		// Driver
		bool  DriverSelect(WORD wIdent);
		bool  DriverCheck(CSerialConfig const &Config);
		bool  DriverConfig(PCBYTE pConfig, UINT uSize);
		bool  DriverFlags(UINT uSelect, WORD &Flags);
		bool  DriverOpen(void);
		bool  DriverClose(void);
		bool  DriverService(void);
		CCODE DriverCtrl(UINT uFunc, PCTXT pValue);

		// Device
		CCODE DeviceCtrl(DWORD dwDevice, UINT uFunc, PCTXT pValue);
		CCODE DeviceOpen(DWORD dwDevice, PCBYTE pConfig, UINT uSize);
		CCODE DeviceClose(DWORD dwDevice, bool fPersist);

		// Comms
		CCODE Ping(void);
		CCODE Read(AREF Addr, PDWORD pData, UINT uCount);
		CCODE Write(AREF Addr, PDWORD pData, UINT uCount);
		CCODE Atomic(AREF Addr, DWORD Data, DWORD dwMask);

		// Service Codes
		enum
		{
			servComms	= 0x01,
			};

		// Commands
		enum
		{
			opAck		= 0x01,
			opNak		= 0x02,
			opRep		= 0x03,
			opDriverSelect	= 0x10,
			opDriverCheck	= 0x11,
			opDriverConfig	= 0x12,
			opDriverFlags	= 0x13,
			opDriverOpen	= 0x14,
			opDriverClose	= 0x15,
			opDriverCtrl	= 0x16,
			opDriverService = 0x17,
			opDeviceOpen	= 0x20,
			opDeviceClose	= 0x21,
			opDeviceCtrl    = 0x22,
			opPing		= 0x30,
			opRead		= 0x31,
			opWrite		= 0x32,
			opAtomic	= 0x33,
			};

		// Transaction 
		bool RecvFrame(CRemotingFrame &Frame, UINT uTimeout);
		bool SendFrame(CRemotingFrame &Frame, UINT uTimeout);
		bool Transact(void);

	protected:
		// Frames
		CRemotingFrame   m_Req;
		CRemotingFrame   m_Rep;
		IMutex         * m_pMutex;
		IDataHandler   * m_pData;
		IPortObject    * m_pPort;
		DWORD            m_hPort;
	};

//////////////////////////////////////////////////////////////////////////
//
// Remting Master Driver
//

class CRemotingMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CRemotingMasterDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData, UINT uSize);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		DEFMETH(void) Close(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(ICommsDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersistConnection);
			
		// Entry Points
		DEFMETH(void) Service(void);
		
		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);
		DEFMETH(UINT) DevCtrl(void * pContext, UINT uFunc, PCTXT Value);

		// Master Flags
		DEFMETH(WORD) GetMasterFlags(void);

		// Master
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Atomic(AREF Addr, DWORD  Data,  DWORD dwMask);

	protected:
		// Data
		PCBYTE        m_pConfig;
		UINT          m_uConfig;
		CSerialConfig m_Serial;
		CRemotingLink m_Link;
		WORD          m_MasterFlags;

		// Implementation
		void Restart(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Remting Slave Driver
//

class CRemotingSlaveDriver : public CSlaveDriver
{
	public:
		// Constructor
		CRemotingSlaveDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData, UINT uSize);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
			
		// Entry Points
		DEFMETH(void) Service(void);
		
		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);
		DEFMETH(UINT) DevCtrl(void * pContext, UINT uFunc, PCTXT Value);

	protected:
		// Data
		PCBYTE          m_pConfig;
		UINT            m_uConfig;
		CSerialConfig   m_Serial;
		CRemotingLink   m_Link;

		// Implementation
		void Start(void);
		void Read (CRemotingFrame const &Req, CRemotingFrame &Rep);
		void Write(CRemotingFrame const &Req, CRemotingFrame &Rep);
	};

// End of File
