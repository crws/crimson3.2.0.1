
#include "intern.hpp"

#include "DebugSerial.hpp"

#include "AnsiDebugConsole.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Debug Serial Driver
//

// Instantiator

clink void CreateDebugSerialDriver(void *pData, UINT *pSize)
{
	if( !pData ) {
		
		*pSize = sizeof(CDebugSerialDriver);
		
		return;
		}
		
	NewHere(pData) CDebugSerialDriver;
	}

// Constructor

CDebugSerialDriver::CDebugSerialDriver(void)
{
	m_pMutex   = Create_Mutex();

	m_pConsole = NULL;

	m_Flags    = DF_CALL_CLOSE;
	}

// Destructor

CDebugSerialDriver::~CDebugSerialDriver(void)
{
	delete m_pConsole;

	m_pMutex->Release();
	}

// Management

void CDebugSerialDriver::Attach(IPortObject *pPort)
{
	if( pPort ) {

		m_pData = MakeDoubleDataHandler();

		m_pData->SetRxSize(1024);

		m_pData->SetTxSize(128 * 1024);

		pPort->Bind(m_pData);
		}
	}

// Entry Points

void CDebugSerialDriver::Close(void)
{
	delete m_pConsole;

	m_pConsole = NULL;
	}

void CDebugSerialDriver::Service(void)
{
	m_pConsole = New CAnsiDebugConsole(this, false, true);

	Write("\n\n\n\n\n\n\n\n\n\n==========================\n");

	m_pConsole->Hello();

	for(;;) {

		UINT c = m_pData->Read(FOREVER);

		if( c < NOTHING ) {

			m_pConsole->OnChar(char(c));
			}
		}
	}

// IUnknown

HRESULT CDebugSerialDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IDiagConsole);

	return CCommsDriver::QueryInterface(riid, ppObject);
	}

ULONG CDebugSerialDriver::AddRef(void)
{
	return CCommsDriver::AddRef();
	}

ULONG CDebugSerialDriver::Release(void)
{
	return CCommsDriver::Release();
	}

// IDiagConsole

void CDebugSerialDriver::Write(PCTXT pText)
{
	if( *pText ) {

		m_pMutex->Wait(FOREVER);

		if( !strchr(pText, '\n') ) {

			m_pData->Write(PCBYTE(pText), strlen(pText), FOREVER);
			}
		else {
			GuardTask(TRUE);

			CString s(pText);

			s.Replace("\n", "\r\n");

			m_pData->Write(PCBYTE(PCTXT(s)), s.GetLength(), FOREVER);

			GuardTask(FALSE);
			}

		m_pMutex->Free();
		}
	}

// End of File
