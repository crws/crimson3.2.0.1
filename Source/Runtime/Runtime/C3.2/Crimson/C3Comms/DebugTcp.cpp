
#include "intern.hpp"

#include "DebugTcp.hpp"

#include "AnsiDebugConsole.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Debug TCP Driver
//

// Instantiator

clink void CreateDebugTcpDriver(void *pData, UINT *pSize)
{
	if( !pData ) {
		
		*pSize = sizeof(CDebugTcpDriver);
		
		return;
		}
		
	NewHere(pData) CDebugTcpDriver;
	}

// Constructor

CDebugTcpDriver::CDebugTcpDriver(void)
{
	StdSetRef();

	m_pSock    = NULL;
		   
	m_uSize    = 1024 * 1024;
		   
	m_uHead    = 0;
		   
	m_uTail    = 0;
		   
	m_pBuff    = New BYTE [ m_uSize ];
		   
	m_pSema    = Create_Semaphore(m_uSize - 1);

	m_pMutex   = Create_Mutex();
	
	m_pConsole = NULL;

	m_Flags    = DF_CALL_CLOSE;
	}

// Destructor

CDebugTcpDriver::~CDebugTcpDriver(void)
{
	delete m_pConsole;

	m_pMutex->Release();

	m_pSema->Release();

	delete [] m_pBuff;
	}

// Configuration

void CDebugTcpDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uPort = GetWord(pData);

		m_uMode = GetByte(pData);
		}
	}

// Entry Points

void CDebugTcpDriver::Close(void)
{
	delete m_pConsole;

	m_pConsole = NULL;
	}

void CDebugTcpDriver::Service(void)
{
	for(;;) {

		if( CheckSocket() ) {

			bool fFull = (m_uMode > 0);

			m_pConsole = New CAnsiDebugConsole(this, true, fFull);

			UINT uEat  = 0;

			bool fNeg  = false;

			Enable(true);

			if( m_uMode == 1 ) {

				Write("\xFF\xFD\x2D");
				}

			if( m_uMode == 2 ) {

				fNeg = true;
				}

			m_pConsole->Hello();

			while( CheckSocket() ) {

				bool     fBusy = false;

				CBuffer *pBuff;

				if( m_pSock->Recv(pBuff) == S_OK ) {

					if( pBuff ) {

						PCBYTE p = pBuff->GetData();

						UINT   c = pBuff->GetSize();

						for( UINT n = 0; n < c; n++ ) {

							if( uEat ) {

								uEat--;

								continue;
								}

							if( p[n] == 0xFF ) {

								if( fNeg ) {

									Write("\xFF\xFD\x2D");

									fNeg = false;
									}

								uEat = 2;

								continue;
								}

							if( m_pConsole->OnChar(p[n]) ) {

								m_pSock->Close();

								break;
								}
							}

						pBuff->Release();

						fBusy = true;
						}
					}

				if( m_uHead != m_uTail ) {

					PBYTE p = m_pBuff + m_uHead;

					UINT  n = ((m_uTail > m_uHead) ? m_uTail : m_uSize) - m_uHead;

					if( m_pSock->Send(p, n) == S_OK ) {

						m_pSema->Signal(n);

						m_uHead = (m_uHead + n) % m_uSize;

						fBusy   = true;
						}
					}

				if( !fBusy ) {

					Sleep(10);
					}
				}

			Enable(false);

			delete m_pConsole;

			m_pConsole = NULL;
			}

		Sleep(100);
		}
	}

// IUnknown

HRESULT CDebugTcpDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IDiagConsole);

	return CCommsDriver::QueryInterface(riid, ppObject);
	}

ULONG CDebugTcpDriver::AddRef(void)
{
	return CCommsDriver::AddRef();
	}

ULONG CDebugTcpDriver::Release(void)
{
	return CCommsDriver::Release();
	}

// IDiagConsole

void CDebugTcpDriver::Write(PCTXT pText)
{
	if( *pText ) {

		if( m_fEnable ) {

			m_pMutex->Wait(FOREVER);

			while( m_fEnable && *pText ) {

				if( !m_pSema->Wait(0) ) {

					break;
					}

				if( *pText == '\n') {
					
					if( !m_pSema->Wait(0) ) {

						m_pSema->Signal(1);

						break;
						}

					m_pBuff[m_uTail] = '\r';

					m_uTail = (m_uTail + 1) % m_uSize;
					}

				m_pBuff[m_uTail] = *pText++;

				m_uTail = (m_uTail + 1) % m_uSize;
				}

			m_pMutex->Free();
			}
		}
	}

// Implementation

BOOL CDebugTcpDriver::CheckSocket(void)
{
	if( !m_pSock ) {

		m_pSock = CreateSocket(IP_TCP);

		m_pSock->Listen(m_uPort);

		return FALSE;
		}

	if( m_pSock ) {

		UINT uPhase = 0;

		m_pSock->GetPhase(uPhase);

		if( uPhase == PHASE_ERROR ) {

			m_pSock->Release();

			m_pSock = NULL;

			return FALSE;
			}

		if( uPhase == PHASE_CLOSING ) {

			m_pSock->Close();

			m_pSock->Release();

			m_pSock = NULL;

			return FALSE;
			}

		if( uPhase == PHASE_OPEN ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CDebugTcpDriver::Enable(bool fEnable)
{
	m_fEnable = (m_fEnable && fEnable);

	m_pMutex->Wait(FOREVER);

	m_uHead   = 0;

	m_uTail   = 0;

	m_fEnable = (m_fEnable || fEnable);

	m_pMutex->Free();
	}

// End of File
