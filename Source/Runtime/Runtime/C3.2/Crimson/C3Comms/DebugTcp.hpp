
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CAnsiDebugConsole;

//////////////////////////////////////////////////////////////////////////
//
// Debug TCP Driver
//

class CDebugTcpDriver : public CCommsDriver, public IDiagConsole
{
	public:
		// Constructor
		CDebugTcpDriver(void);

		// Destructor
		~CDebugTcpDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Entry Points
		DEFMETH(void) Close(void);
		DEFMETH(void) Service(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagConsole
		DEFMETH(void) Write(PCTXT pText);

	protected:
		// Data Members
		ULONG		    m_uRefs;
		UINT	            m_uPort;
		UINT	            m_uMode;
		ISocket           * m_pSock;
		bool	            m_fEnable;
		IMutex            * m_pMutex;
		CAnsiDebugConsole * m_pConsole;
		ISemaphore        * m_pSema;
		UINT	            m_uSize;
		UINT	            m_uHead;
		UINT	            m_uTail;
		PBYTE	            m_pBuff;

		// Implementation
		BOOL CheckSocket(void);
		void Enable(bool fEnable);
	};

// End of File
