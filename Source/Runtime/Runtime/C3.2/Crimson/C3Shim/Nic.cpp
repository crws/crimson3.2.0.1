
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shims
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Nic Shims
//

// Code

void NICInit(void)
{
}

PCBYTE NICGetMac(void)
{
	AfxGetAutoObject(pUtils, "ip", 0, INetUtilities);

	if( pUtils ) {

		static CMacAddr Mac;

		if( pUtils->GetInterfaceMac(1, Mac) ) {

			return Mac.m_Addr;
		}
	}

	static BYTE const b[] = { 0x11, 0x11, 0x11, 0x11, 0x11, 0x11 };

	return b;
}

BOOL NICReadMac(UINT Port, CMacAddr &Mac)
{
	AfxGetAutoObject(pUtils, "ip", 0, INetUtilities);

	if( pUtils ) {

		UINT uFace = (Port == NOTHING) ? 1 : 1 + Port;

		if( pUtils->GetInterfaceMac(uFace, Mac) ) {

			return TRUE;
		}
	}

	return FALSE;
}

// End of File
