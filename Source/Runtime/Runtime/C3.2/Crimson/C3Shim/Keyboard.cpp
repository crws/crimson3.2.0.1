
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shims
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Keyboard Shims
//

// Data

static IKeyboard * m_pKeyb = NULL;

static ILeds     * m_pLeds = NULL;

// Code

void KeyInit(void)
{
	AfxGetObject("keyboard", 0, IKeyboard, m_pKeyb);

	AfxGetObject("leds", 0, ILeds, m_pLeds);
}

BOOL KeyCheck(void)
{
	return m_pKeyb ? TRUE : FALSE;
}

BOOL IsKeyDown(UINT uCode)
{
	return TRUE;
}

void KeySetBeep(BOOL fEnable)
{
	m_pKeyb->SetBeepMode(fEnable);
}

void KeySetLEDLevel(UINT uLevel, BOOL fSave)
{
	if( m_pLeds ) {

		return m_pLeds->SetLedLevel(uLevel, fSave);
	}
}

void KeySetLED(UINT uLed, UINT uState)
{
	if( m_pLeds ) {

		return m_pLeds->SetLed(uLed, uState);
	}
}

UINT KeyGetLEDLevel(void)
{
	if( m_pLeds ) {

		return m_pLeds->GetLedLevel();
	}

	return 100;
}

// End of File
