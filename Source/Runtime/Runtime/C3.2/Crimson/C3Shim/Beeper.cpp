
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shims
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Beeper Shims
//

// Data

static IBeeper * m_pBeep = NULL;

// Code

void BeepInit(void)
{
	AfxGetObject("beeper", 0, IBeeper, m_pBeep);
	}

void Beep(UINT uFreq, UINT uTime)
{
	if( m_pBeep ) {

		m_pBeep->Beep(uFreq, uTime);
		}
	}

void BeepOff(void)
{
	if( m_pBeep ) {

		m_pBeep->BeepOff();
		}
	}

// End of File
