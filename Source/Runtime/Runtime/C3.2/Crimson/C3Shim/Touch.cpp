
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shims
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Touchscreen Shims
//

// Data

static ITouchScreen * m_pTouch = NULL;

// Code

void TouchInit(void)
{
	AfxGetObject("touch", 0, ITouchScreen, m_pTouch);
}

BOOL TouchCheck(void)
{
	return m_pTouch ? TRUE : FALSE;
}

void TouchSetMap(PCBYTE pMap)
{
	m_pTouch->SetMap(pMap);
}

BOOL TouchTranslate(struct CInput &i)
{
	return FALSE;
}

void TouchGetRaw(int &xPos, int &yPos)
{
	if( m_pTouch ) {

		m_pTouch->GetRaw(xPos, yPos);

		return;
	}

	xPos = 0;

	yPos = 0;
}

void TouchSetCalib(BOOL fFactory, int xMin, int yMin, int xMax, int yMax)
{
}

BOOL TouchSetCalibEx(BOOL fFactory, int xMin, int yMin, int xMax, int yMax)
{
	return TRUE;
}

void TouchClearCalib(BOOL fPersist)
{
}

void TouchSetBeep(BOOL fEnable)
{
	m_pTouch->SetBeepMode(fEnable);
}

// End of File
