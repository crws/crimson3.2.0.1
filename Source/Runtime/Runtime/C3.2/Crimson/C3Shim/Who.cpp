
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shims
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Platform Shims
//

// Data

static IPlatform *	m_pPlatform = NULL;

static IFeatures *	m_pFeatures = NULL;

static UINT		m_uGroup    = 0;

// Code

void WhoInit(void)
{
	AfxGetObject("platform", 0, IPlatform, m_pPlatform);

	AfxGetObject("dev.security", 0, IFeatures, m_pFeatures);
}

void WhoSetRequiredGroup(UINT uGroup)
{
	UINT uLimit = 1000;

	if( m_pFeatures ) {

		uLimit = m_pFeatures->GetEnabledGroup();
	}

	m_uGroup = min(uLimit, uGroup);
}

UINT WhoGetActiveGroup(void)
{
	return m_uGroup;
}

BOOL WhoHasFeature(UINT Feature)
{
	if( Feature & rfMaskHardware ) {

		if( m_pPlatform->GetFeatures() & Feature ) {

			return TRUE;
		}

		return FALSE;
	}
	else {
		UINT uGroup = WhoGetActiveGroup();

		switch( Feature ) {

			case rfConsole:
			case rfDnsApi:
			case rfPlayMusic:
			{
				return TRUE;
			}

			case rfRemoteDisplay:
			{
				return FALSE;
			}

			case rfCamera:
			{
				return uGroup >= SW_GROUP_3A;
			}

			case rfPdfViewer:
			case rfFileViewer:
			case rfLogToDisk:
			case rfPortSharing:
			case rfFileSystemApi:
			case rfWebServer:
			case rfPrintScreen:
			case rfSqlQuery:
			{
				return uGroup >= SW_GROUP_3B;
			}
		}

		return FALSE;
	}
}

PCTXT WhoGetName(BOOL fGeneric)
{
	return m_pPlatform->GetModel();
}

BOOL WhoGetModel(PTXT pModel)
{
	strcpy(pModel, m_pPlatform->GetModel());

	return TRUE;
}

// End of File
