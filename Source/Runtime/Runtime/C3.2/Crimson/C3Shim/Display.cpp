
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shims
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display Shims
//

// Data

static IDisplay * m_pDisp = NULL;

// Code

void DispInit(void)
{
	AfxGetObject("display", 0, IDisplay, m_pDisp);
}

int DispGetCx(void)
{
	if( m_pDisp ) {

		int cx, cy;

		m_pDisp->GetSize(cx, cy);

		return cx;
	}

	return 0;
}

int DispGetCy(void)
{
	if( m_pDisp ) {

		int cx, cy;

		m_pDisp->GetSize(cx, cy);

		return cy;
	}

	return 0;
}

void DispUpdate(PCVOID pData)
{
	if( m_pDisp ) {

		m_pDisp->Update(pData);
	}
}

void DispEnableBacklight(BOOL fEnable)
{
	if( m_pDisp ) {

		m_pDisp->EnableBacklight(fEnable);
	}
}

void DispSetBrightness(UINT uLevel, BOOL fSave)
{
	if( m_pDisp ) {

		m_pDisp->SetBacklight(uLevel);
	}
}

void DispSetContrast(UINT uLevel, BOOL fSave)
{
}

UINT DispGetBrightness(void)
{
	if( m_pDisp ) {

		return m_pDisp->GetBacklight();
	}

	return 100;
}

UINT DispGetContrast(void)
{
	return 50;
}

// End of File
