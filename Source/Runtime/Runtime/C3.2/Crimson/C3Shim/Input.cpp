
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shims
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Input Queue Shims
//

// Data

static IInputQueue * m_pInput = NULL;

// Code

void InputInit(void)
{
	AfxGetObject("input-d", 0, IInputQueue, m_pInput);
	}

void InputEmpty(void)
{
	m_pInput->Clear();
	}

INPUT InputRead(UINT uTime)
{
	return m_pInput->Read(uTime);
	}

void InputStore(INPUT Input)
{
	m_pInput->Store(Input);
	}

// End of File
