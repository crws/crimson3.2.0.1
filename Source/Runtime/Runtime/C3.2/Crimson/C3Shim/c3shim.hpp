
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Core Runtime
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3SHIM_HPP

#define	INCLUDE_C3SHIM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Standard Aeon Header
//

#include <StdEnv.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Version Information
//

#if defined(_DEBUG)

#include "../../../../../../Build/Bin/Version/Debug/version.hxx"

#else

#include "../../../../../../Build/Bin/Version/Release/version.hxx"

#endif

//////////////////////////////////////////////////////////////////////////
//
// Platform Features
//

enum
{
	// Software
	rfConsole	  = 0x00000100,
	rfSystemMenu	  = 0x00000200,
	rfCamera          = 0x00000400,
	rfPdfViewer       = 0x00000800,
	rfFileViewer      = 0x00001000,
	rfLogToDisk       = 0x00002000,
	rfPortSharing     = 0x00004000,
	rfFileSystemApi   = 0x00008000,
	rfDnsApi          = 0x00010000,
	rfWebServer       = 0x00020000,
	rfPlayMusic       = 0x00040000,
	rfPrintScreen     = 0x00080000,
	rfRemoteDisplay	  = 0x00100000,
	rfSqlQuery        = 0x00200000,
	rfHoneywell       = 0x80000000,
};

//////////////////////////////////////////////////////////////////////////
//
// Object Broker
//

extern IObjectBroker * piob;

//////////////////////////////////////////////////////////////////////////
//
// Renamed Types
//

typedef CMacAddr		CMACAddr;

typedef CIpAddr			CIPAddr;

typedef HTHREAD			HTASK;

typedef IGdi			IGDI;

typedef IGdiFont		IGDIFont;

typedef ITlsLibrary		IMatrixSsl;

typedef ITlsClientContext	IMatrixClientContext;

typedef ITlsServerContext	IMatrixServerContext;

//////////////////////////////////////////////////////////////////////////
//
// Renamed APIs
//

#define stricmp  strcasecmp

#define strnicmp strncasecmp

//////////////////////////////////////////////////////////////////////////
//
// Shim Initialization
//

extern void ShimInit(void);

//////////////////////////////////////////////////////////////////////////
//
// Memory Management
//

STRONG_INLINE void * Malloc(size_t n)
{
	return malloc(n);
}

STRONG_INLINE void * ReAlloc(void *p, size_t n)
{
	return realloc(p, n);
}

STRONG_INLINE void Free(void *p)
{
	free(p);
}

//////////////////////////////////////////////////////////////////////////
//
// Executive
//

STRONG_INLINE HTASK GetCurrentTask(void)
{
	return GetCurrentThread();
}

STRONG_INLINE UINT GetCurrentTaskIndex(void)
{
	return GetThreadIndex();
}

STRONG_INLINE void SetTaskLimit(UINT uTime, UINT uSleep)
{
}

STRONG_INLINE void TimerDelay(UINT uTime)
{
	Sleep(uTime);
}

STRONG_INLINE void GuardTask(BOOL fGuard)
{
	GuardThread(fGuard);
}

//////////////////////////////////////////////////////////////////////////
//
// Graphics Objects
//

extern ITouchMap * Create_TouchMap(IGdi *pGdi);

extern IRegion * Create_StdRegion(void);

extern IGdi * Create_GDI(void);

//////////////////////////////////////////////////////////////////////////
//
// Serial Ports
//

STRONG_INLINE IPortObject * SerialGetPort(UINT uPort)
{
	IPortObject *pPort = NULL;

	AfxGetObject("pnp.pnp-uart", uPort, IPortObject, pPort);

	return pPort;
}

STRONG_INLINE IDataHandler * Create_SingleDataHandler(void)
{
	IDataHandler *pData = NULL;

	AfxNewObject("data-s", IDataHandler, pData);

	return pData;
}

STRONG_INLINE IDataHandler * Create_DoubleDataHandler(void)
{
	IDataHandler *pData = NULL;

	AfxNewObject("data-d", IDataHandler, pData);

	return pData;
}

//////////////////////////////////////////////////////////////////////////
//
// Crytpography
//

STRONG_INLINE void MD5(PBYTE data, int bytes, PBYTE hash)
{
	md5(data, bytes, hash);
}

//////////////////////////////////////////////////////////////////////////
//
// Beeper
//

extern void BeepInit(void);
extern void Beep(UINT uFreq, UINT uTime);
extern void BeepOff(void);

//////////////////////////////////////////////////////////////////////////
//
// Display
//

extern void DispInit(void);
extern int  DispGetCx(void);
extern int  DispGetCy(void);
extern void DispUpdate(PCVOID pData);
extern void DispEnableBacklight(BOOL fEnable);
extern void DispSetBrightness(UINT uLevel, BOOL fSave=FALSE);
extern void DispSetContrast(UINT uLevel, BOOL fSave=FALSE);
extern UINT DispGetBrightness(void);
extern UINT DispGetContrast(void);

//////////////////////////////////////////////////////////////////////////
//
// Serial Memory
//

extern void FRAMInit(void);
extern UINT FRAMGetSize(void);
extern BOOL FRAMGetData(WORD wAddr, PBYTE pData, UINT uCount);
extern BOOL FRAMPutData(WORD wAddr, PBYTE pData, UINT uCount);
extern WORD FRAMGetAddr(UINT uName);

//////////////////////////////////////////////////////////////////////////
//
// Input Queue
//

extern void  InputInit(void);
extern void  InputEmpty(void);
extern INPUT InputRead(UINT uTime);
extern void  InputStore(INPUT Input);

//////////////////////////////////////////////////////////////////////////
//
// Keyboard
//

extern void KeyInit(void);
extern BOOL KeyCheck(void);
extern BOOL IsKeyDown(UINT uCode);
extern void KeySetBeep(BOOL fBeep);
extern void KeySetLEDLevel(UINT uLevel, BOOL fSave=FALSE);
extern UINT KeyGetLEDLevel(void);
extern void KeySetLED(UINT uLed, UINT uState);

//////////////////////////////////////////////////////////////////////////
//
// NIC
//

extern void   NICInit(void);
extern PCBYTE NICGetMac(void);
extern BOOL   NICReadMac(UINT Port, CMacAddr &Mac);

//////////////////////////////////////////////////////////////////////////
//
// Touchscreen
//

extern void TouchInit(void);
extern BOOL TouchCheck(void);
extern void TouchSetMap(PCBYTE pMap);
extern BOOL TouchTranslate(struct CInput &i);
extern void TouchGetRaw(int &xPos, int &yPos);
extern void TouchSetCalib(BOOL fFactory, int xMin, int yMin, int xMax, int yMax);
extern BOOL TouchSetCalibEx(BOOL fFactory, int xMin, int yMin, int xMax, int yMax);
extern void TouchClearCalib(BOOL fPersist = TRUE);
extern void TouchSetBeep(BOOL fSet);

//////////////////////////////////////////////////////////////////////////
//
// Trap History
//

extern void TrapInit(void);
extern BOOL TrapGetData(UINT uSlot, PDWORD pData);
extern void TrapAcknowledge(void);

//////////////////////////////////////////////////////////////////////////
//
// Platform Information
//

extern void  WhoInit(void);
extern void  WhoSetRequiredGroup(UINT uGroup);
extern UINT  WhoGetActiveGroup(void);
extern BOOL  WhoHasFeature(UINT Feature);
extern PCTXT WhoGetName(BOOL fGeneric);
extern BOOL  WhoGetModel(PTXT pModel);

//////////////////////////////////////////////////////////////////////////
//
// Real Time Clock
//

extern BOOL IsBatteryOK(void);

//////////////////////////////////////////////////////////////////////////
//
// Serial Memory Locations
//

enum {
	memDatabase,
	memMounted,
	memLoading,
	memTimeMagic,
	memTimeZoneHours,
	memTimeDST,
	memTimeZoneMinutes,
	memDisplay,
	memContrast,
	memBrightness,
	memTouch,
	memTouchFactory,
	memTouchSettings,
	memLanguage,
	memMacId0,
	memMacId1,
	memRtc,
	memFastFile,
	memBattery,
	memSignalLed,
	memDNS0,
	memDNS1,
	memNetCfgComp,
	memNetCfgSize,
	memNetCfgData,
	memDataLog0,
	memDataLog1,
	memDataLog2,
	memDataLog3,
	memDataLog4,
	memDataLog5,
	memDataLog6,
	memDataLog7,
	memCFData,
	memCFMedia,
	memCFLBA,
	memCFState,
	memTrap0,
	memTrap1,
	memTrap2,
	memTrap3,
	memTrap4,
	memTrap5,
	memTrap6,
	memTrap7,
	memIdentity,
	memStack,
	memModule0,
	memModule1,
	memModule2,
	memModule3,
	memModule4,
	memModule5,
	memModule6,
	memModule7,
	memModule8,
	memModule9,
	memModule10,
	memModule11,
	memModule12,
	memModule13,
	memModule14,
	memModule15,
	memControl,
	memCount,
};

#define Mem(x)(FRAMGetAddr(mem##x))

//////////////////////////////////////////////////////////////////////////
//
// RLC Bitmap
//

struct BITMAP_RLC
{
	DWORD	Frame;
	DWORD	Format;
	DWORD	Width;
	DWORD	Height;
	DWORD	Stride;
	BYTE	Data[0];
};

//////////////////////////////////////////////////////////////////////////
//
// Crimson Type Modifiers
//

#define COCLASS		class 

#define	INTERFACE	struct

#define	STRUCT		struct

#define	MCALL		METHOD

#define	DEFMETH(t)	virtual t MCALL

#define	SLOW		/**/

//////////////////////////////////////////////////////////////////////////
//
// Crimson Base Interface
//

INTERFACE IBase
{
	virtual UINT Release(void) = 0;

};

//////////////////////////////////////////////////////////////////////////
//
// Host Trap
//

#if !defined(HostTrap)

STRONG_INLINE void HostTrap(UINT uTrap)
{
	// TODO -- This ought to shutdown the app in Win32 or Linux
	// and then allow some sort of all-father to restart it!!!
}

#endif

STRONG_INLINE void ByeByeSetHook(void (*)(void))
{
}

// End of File

#endif
