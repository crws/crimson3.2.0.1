
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shims
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Serial Memory Shims
//

// Data

static ISerialMemory * m_pMem = NULL;

// Code

void FRAMInit(void)
{
	AfxGetObject("fram", 0, ISerialMemory, m_pMem);
	}

UINT FRAMGetSize(void)
{
	return m_pMem->GetSize();
	}

BOOL FRAMGetData(WORD wAddr, PBYTE pData, UINT uCount)
{
	return m_pMem->GetData(wAddr, pData, uCount);
	}

BOOL FRAMPutData(WORD wAddr, PBYTE pData, UINT uCount)
{
	return m_pMem->PutData(wAddr, pData, uCount);
	}

WORD FRAMGetAddr(UINT uName)
{
	switch( uName ) {

		case memDatabase:        return 0x0000;
		case memMounted:         return 0x0004;
		case memLoading:         return 0x0008;
		case memDisplay:         return 0x000A;
		case memTimeMagic:       return 0x0010;
		case memTimeZoneHours:   return 0x0014;
		case memTimeDST:         return 0x0015;
		case memTimeZoneMinutes: return 0x0016;
		case memLanguage:        return 0x0020;
		case memMacId0:          return 0x002C;
		case memMacId1:          return 0x0032;
		case memRtc:             return 0x0040;
		case memTouch:           return 0x0050;
		case memFastFile:        return 0x0060;
		case memBattery:         return 0x0064;
		case memSignalLed:       return 0x0068;
		case memDNS0:            return 0x0070;
		case memDNS1:            return 0x0076;
		case memNetCfgComp:      return 0x007C;
		case memNetCfgSize:      return 0x007E;
		case memNetCfgData:      return 0x0080;
		case memCFData:          return 0x0400;
		case memCFMedia:         return 0x0600;
		case memCFLBA:           return 0x0604;
		case memCFState:         return 0x0608;
		case memDataLog0:        return 0x0670;
		case memDataLog1:        return 0x0680;
		case memDataLog2:        return 0x0690;
		case memDataLog3:        return 0x06A0;
		case memDataLog4:        return 0x06B0;
		case memDataLog5:        return 0x06C0;
		case memDataLog6:        return 0x06D0;
		case memDataLog7:        return 0x06E0;
		case memTrap0:           return 0x0700;
		case memTrap1:           return 0x0800;
		case memTrap2:           return 0x0900;
		case memTrap3:           return 0x0A00;
		case memTrap4:           return 0x0B00;
		case memTrap5:           return 0x0C00;
		case memTrap6:           return 0x0D00;
		case memTrap7:           return 0x0E00;
		case memIdentity:        return 0x1000;
		case memModule0:         return 0x2000;
		case memModule1:         return 0x2040;
		case memModule2:         return 0x2080;
		case memModule3:         return 0x20C0;
		case memModule4:         return 0x2100;
		case memModule5:         return 0x2140;
		case memModule6:         return 0x2180;
		case memModule7:         return 0x21C0;
		case memModule8:         return 0x2200;
		case memModule9:         return 0x2240;
		case memModule10:        return 0x2280;
		case memModule11:        return 0x22C0;
		case memModule12:        return 0x2300;
		case memModule13:        return 0x2340;
		case memModule14:        return 0x2380;
		case memModule15:        return 0x23C0;
		case memControl:         return 0x3000;
		}

	return 0xFFFF;
	}

// End of File
