
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Core Runtime
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "rc4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Database Item
//

// Destructor

CItem::~CItem(void)
{
}

// Persistance

void CItem::Load(PCBYTE &pData)
{
}

// Init Debug

void CItem::ValidateLoad(PCTXT pClass, PCBYTE &pData)
{
	WORD w;

	if( (w = GetWord(pData)) == 0x1234 ) {

		if( FALSE ) {

			AfxTrace("  Loading %-24s from %p - \t%4.4X", pClass, pData, w);

			AfxTrace("  OKAY\n");
		}

		return;
	}

	AfxTrace("  Loading %-24s from %p - \t%4.4X", pClass, pData, w);

	AfxTrace("  ERROR\n");

	HostBreak();
}

// Init Helpers

PCUTF CItem::GetCryp(PCBYTE &pData)
{
	UINT   s = GetWord(pData);

	PCBYTE p = PCBYTE(pData);

	PBYTE  w = PBYTE(alloca(s));

	pData   += s;

	////////

	rc4_key key;

	PCSTR pPass = "PineappleHead";

	prepare_key(PBYTE(pPass), strlen(pPass), &key);

	memcpy(w, p, s);

	rc4(w, s, &key);

	////////

	PCBYTE t = w;

	return GetWide(t);
}

// Other Items

UINT CItem::GetItem(PCBYTE &pData, CItem *pItem, WORD Class)
{
	UINT   uItem = GetWord(pData);

	PCBYTE pInit = PCBYTE(g_pDbase->LockItem(uItem));

	if( pInit ) {

		WORD Found = GetWord(pInit);

		if( Found == Class ) {

			if( FALSE ) {

				AfxTrace("OKAY\n");
			}

			pItem->Load(pInit);

			g_pDbase->FreeItem(uItem);

			return uItem;
		}

		AfxTrace("Loading %4.4X from %4.4X at %p - ", Class, uItem, pData);

		AfxTrace("ERROR - Found %4.4X\n", Found);

		HostBreak();
	}

	AfxTrace("Loading %4.4X from %4.4X at %p - ", Class, uItem, pData);

	AfxTrace("ERROR - No Item\n");

	HostBreak();

	return 0;
}

// Data Fallback

DWORD CItem::GetValue(UINT Type)
{
	if( (Type & typeMask) == typeString ) {

		return DWORD(wstrdup(L""));
	}

	if( (Type & typeMask) == typeReal ) {

		return R2I(0);
	}

	return 0;
}

BOOL CItem::SetValue(UINT Type, DWORD Data)
{
	if( (Type & typeMask) == typeString ) {

		Free(PUTF(Data));
	}

	return TRUE;
}

// International Strings

PCTXT CItem::GetIntl(PCTXT pText)
{
	return pText;
}

// End of File
