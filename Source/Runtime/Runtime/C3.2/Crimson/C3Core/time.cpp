
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Time Manager APIs
//

// Static Data

static INT	   m_offset = 0;

static BOOL	   m_dst    = FALSE;

static ITimeZone * m_pZone  = NULL;

static IMutex    * m_pLock  = NULL;

// Constants

#define	DAYS  (60L * 60L * 24L)

#define	QUAD  (3 * 365 + 366)

#define	WEEKS (DAYS * 7)

// Prototypes

static void ReadTimeZone(void);
static void SaveTimeZone(void);
static UINT GetCummDays(UINT y, UINT m);
static UINT GetFromDays(UINT y, UINT d);
static UINT GetYearWeeks(UINT y);

// Code

global void TimeZoneInit(void)
{
	AfxGetObject("dev.tz", 0, ITimeZone, m_pZone);

	m_pLock = Create_Qutex();
}

global void SetTimeZone(INT nZone, BOOL fDst)
{
	CAutoLock Lock(m_pLock);

	m_offset = nZone * 60;

	m_dst    = fDst;

	SaveTimeZone();
}

global void SetTimeZone(INT nZone)
{
	CAutoLock Lock(m_pLock);

	m_offset = nZone * 60;

	SaveTimeZone();
}

global void SetTimeZoneMins(INT nZone, BOOL fDst)
{
	CAutoLock Lock(m_pLock);

	m_offset = nZone;

	m_dst    = fDst;

	SaveTimeZone();
}

global void SetTimeZoneMins(INT nZone)
{
	CAutoLock Lock(m_pLock);

	m_offset = nZone;

	SaveTimeZone();
}

global void SetDaylight(BOOL fDst)
{
	CAutoLock Lock(m_pLock);

	m_dst = fDst;

	SaveTimeZone();
}

static void ReadTimeZone(void)
{
	if( m_pZone ) {

		m_offset = m_pZone->GetOffset();

		m_dst    = m_pZone->GetDst();
	}
}

static void SaveTimeZone(void)
{
	if( m_pZone ) {

		if( m_pZone->Lock(2) ) {

			m_pZone->SetOffset(m_offset);

			m_pZone->SetDst(m_dst);

			m_pZone->SetNext(0x7FFFFFFF);

			m_pZone->Lock(0);
		}
	}
}

global INT GetTimeZone(void)
{
	CAutoLock Lock(m_pLock);

	ReadTimeZone();

	return m_offset / 60;
}

global INT GetTimeZoneMins(void)
{
	CAutoLock Lock(m_pLock);

	ReadTimeZone();

	return m_offset / 60;
}

global BOOL GetDaylight(void)
{
	CAutoLock Lock(m_pLock);

	ReadTimeZone();

	return m_dst;
}

global INT GetZuluOffset(void)
{
	CAutoLock Lock(m_pLock);

	ReadTimeZone();

	return -60 * (m_offset + (m_dst ? 60 : 0));
}

global DWORD GetNow(void)
{
	timeval tv;

	gettimeofday(&tv, NULL);

	tv.tv_sec -= GetZuluOffset();

	return 1 * (tv.tv_sec - 852076800);
}

global UINT GetNowFraction(void)
{
	timeval tv;

	gettimeofday(&tv, NULL);

	return 0x10000 * tv.tv_usec / 1000000;
}

global DWORD GetNowTimes5(void)
{
	timeval tv;

	gettimeofday(&tv, NULL);

	tv.tv_sec -= GetZuluOffset();

	return 5 * (tv.tv_sec - 852076800) + (tv.tv_usec / 200000);
}

global void GetTime(CTime &time)
{
	DWORD t = GetNow();

	time.uSeconds = GetSec(t);
	time.uMinutes = GetMin(t);
	time.uHours   = GetHour(t);
	time.uDate    = GetDate(t);
	time.uMonth   = GetMonth(t);
	time.uYear    = GetYear(t);
	time.uDay     = GetDay(t);
}

global BOOL SetTime(CTime &time)
{
	DWORD t = 0;

	t += Time(time.uHours, time.uMinutes, time.uSeconds);

	t += Date(time.uYear, time.uMonth, time.uDate);

	timeval tv;

	tv.tv_sec  = t + 852076800;

	tv.tv_sec += GetZuluOffset();

	tv.tv_usec = 0;

	return settimeofday(&tv, NULL) == 0;
}

global DWORD Time(UINT h, UINT m, UINT s)
{
	return s + 60L * (m + (60L * h));
}

global DWORD Date(UINT y, UINT m, UINT d)
{
	d = d - 1;

	m = GetCummDays(y, m);

	y = y % 100;

	y = (y < 97) ? (y + 3) : (y - 97);

	y = y * 365 + y / 4;

	return (m + d + y) * DAYS;
}

global void GetWholeDate(DWORD t, UINT *p)
{
	UINT d = GetDays(t);

	UINT r = d % QUAD / 365;

	UINT q = d / QUAD;

	if( r > 3 ) r = 3;

	p[2] = 1997 + r + 4 * q;

	d = d - 365 * r - QUAD * q;

	p[1] = GetFromDays(p[2], d);

	d = d - GetCummDays(p[2], p[1]);

	p[0] = 1 + d;
}

global void GetWholeTime(DWORD t, UINT *p)
{
	p[0] = GetSec(t);

	p[1] = GetMin(t);

	p[2] = GetHour(t);
}

global UINT GetYear(DWORD t)
{
	UINT d = GetDays(t);

	UINT r = d % QUAD / 365;

	UINT q = d / QUAD;

	if( r > 3 ) r = 3;

	return 1997 + r + 4 * q;
}

global UINT GetMonth(DWORD t)
{
	UINT y = GetYear(t);

	UINT d = (t - Date(y, 1, 1)) / DAYS;

	return GetFromDays(y, d);
}

global UINT GetDate(DWORD t)
{
	UINT y = GetYear(t);

	UINT m = GetMonth(t);

	UINT d = (t - Date(y, m, 1)) / DAYS;

	return 1 + d;
}

global UINT GetDays(DWORD t)
{
	return t / DAYS;
}

global UINT GetWeeks(DWORD t)
{
	return t / WEEKS;
}

global UINT GetWeek(DWORD t)
{
	UINT y = GetYear(t);

	UINT m = GetMonth(t);

	UINT n = GetCummDays(y, m) + GetDate(t);

	UINT d = GetDay(Date(y, 1, 1));

	UINT week = ((n + 7) - (7 - d) + 5) / 7;

	switch( d ) {

		case 5:
		case 6:
			week -= 1;
			break;
	}

	if( week > GetYearWeeks(y) ) {

		return 1;
	}

	if( week < 1 ) {

		return GetYearWeeks(y-1);
	}

	return week;
}

global UINT GetWeekYear(DWORD t)
{
	UINT y = GetYear(t);

	if( GetWeek(t) == 1 ) {

		if( GetMonth(t) == 12 ) {

			return y + 1;
		}
	}

	else if( GetWeek(t) == GetYearWeeks(y-1) ) {

		if( GetMonth(t) == 1 ) {

			return y - 1;
		}
	}

	return y;
}

global UINT GetDay(DWORD t)
{
	UINT d = GetDays(t);

	UINT n = (3 + d) % 7;

	return n;
}

global UINT GetHours(DWORD t)
{
	return (UINT) (t / 3600);
}

global UINT GetHour(DWORD t)
{
	return (UINT) (t / 3600 % 24);
}

global UINT GetMin(DWORD t)
{
	return (UINT) (t / 60 % 60);
}

global UINT GetSec(DWORD t)
{
	return (UINT) (t % 60);
}

global UINT GetMonthDays(UINT y, UINT m)
{
	switch( m ) {

		case 2:
			return (y % 4) ? 28 : 29;

		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
	}

	return 31;
}

static UINT GetCummDays(UINT y, UINT m)
{
	if( y % 4 ) {

		switch( m ) {

			case  1: return 0;
			case  2: return 31;
			case  3: return 31+28;
			case  4: return 31+28+31;
			case  5: return 31+28+31+30;
			case  6: return 31+28+31+30+31;
			case  7: return 31+28+31+30+31+30;
			case  8: return 31+28+31+30+31+30+31;
			case  9: return 31+28+31+30+31+30+31+31;
			case 10: return 31+28+31+30+31+30+31+31+30;
			case 11: return 31+28+31+30+31+30+31+31+30+31;
			case 12: return 31+28+31+30+31+30+31+31+30+31+30;

		}

		return 365;
	}
	else {
		switch( m ) {

			case  1: return 0;
			case  2: return 31;
			case  3: return 31+29;
			case  4: return 31+29+31;
			case  5: return 31+29+31+30;
			case  6: return 31+29+31+30+31;
			case  7: return 31+29+31+30+31+30;
			case  8: return 31+29+31+30+31+30+31;
			case  9: return 31+29+31+30+31+30+31+31;
			case 10: return 31+29+31+30+31+30+31+31+30;
			case 11: return 31+29+31+30+31+30+31+31+30+31;
			case 12: return 31+29+31+30+31+30+31+31+30+31+30;

		}

		return 366;
	}

	return 0;
}

static UINT GetFromDays(UINT y, UINT d)
{
	UINT m;

	for( m = 1; m < 12; m++ ) {

		if( d >= GetCummDays(y, 1 + m) ) {

			continue;
		}

		break;
	}

	return m;
}

static UINT GetYearWeeks(UINT y)
{
	UINT const LongYears[] = {

		  4,   9,  15,  20,  26,  32,  37,  43,  48,  54,
		 60,  65,  71,  76,  82,  88,  93,  99, 105, 111,
		116, 122, 128, 133, 139, 144, 150, 156, 161, 167,
		172, 178, 184, 189, 195, 201, 207, 212, 218, 224,
		229, 235, 240, 246, 252, 257, 263, 268, 274, 280,
		285, 291, 296, 303, 308, 314, 320, 325, 331, 336,
		342, 348, 353, 359, 364, 370, 376, 381, 387, 392,
		398

	};

	UINT n = y % 400;

	for( UINT i = 0; i < elements(LongYears); i++ ) {

		if( n == LongYears[i] ) {

			return 53;
		}
	}

	return 52;
}

// End of File
