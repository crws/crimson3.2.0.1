
#include "intern.hpp"

#include "execute.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Byte Code Executer
//

// Externals

extern DWORD C3ExecuteCodeDebug (PCBYTE pCode, IDataServer *pData, PDWORD pParam);

extern DWORD C3ExecuteCodeNormal(PCBYTE pCode, IDataServer *pData, PDWORD pParam);

// Call Router

DLLAPI DWORD C3ExecuteCode( PCBYTE	  pCode,
			    IDataServer * pData,
			    PDWORD	  pParam
			    )
{
	if( pCode[4] == bcDebugInfo ) {

		return C3ExecuteCodeDebug(pCode, pData, pParam);
		}

	return C3ExecuteCodeNormal(pCode, pData, pParam);
	}

// End of File
