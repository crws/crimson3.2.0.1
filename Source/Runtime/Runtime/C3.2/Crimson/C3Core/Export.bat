@echo off

rem %1 = build path

mkdir "%~1include\runtime\c3.2" 2>nul

copy c3core.hpp "%~1include\runtime\c3.2" >nul
copy netcfg.hpp "%~1include\runtime\c3.2" >nul
copy crc16.ipp  "%~1include\runtime\c3.2" >nul

attrib -r "%~1include\runtime\c3.2"\*.*
