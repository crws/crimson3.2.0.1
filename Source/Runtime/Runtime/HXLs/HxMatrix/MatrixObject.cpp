
#include "Intern.hpp"

#include "MatrixObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MatrixClientContext.hpp"

#include "MatrixServerContext.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Default Root Cert
//

static BYTE const RootCert[] = {

	#include "std-root-certs.crt.dat"
};

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Object
//

// Registration

global void Register_Matrix(void)
{
	piob->RegisterSingleton("matrix.tls", 0, New CMatrixSsl);
}

// Constructor

CMatrixSsl::CMatrixSsl(void)
{
	StdSetRef();

	m_pStream = New CTcpStream;

	piob->RegisterSingleton("net.pcap", 99, m_pStream);

	LoadDefaultRoots();
}

// Destructor

CMatrixSsl::~CMatrixSsl(void)
{
	// This deletes m_pStream

	piob->RevokeSingleton("net.pcap", 99);
}

// IUnknown

HRESULT CMatrixSsl::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ITlsLibrary);

	StdQueryInterface(ITlsLibrary);

	return E_NOINTERFACE;
}

ULONG CMatrixSsl::AddRef(void)
{
	StdAddRef();
}

ULONG CMatrixSsl::Release(void)
{
	StdRelease();
}

// IMatrixSsl

BOOL CMatrixSsl::LoadTrustedRoots(PCBYTE pRoot, UINT uRoot)
{
	if( pRoot && uRoot ) {

		m_Roots.Empty();

		m_Roots.Append(pRoot, uRoot);

		return TRUE;
	}

	LoadDefaultRoots();

	return TRUE;
}

BOOL CMatrixSsl::CreateClientContext(ITlsClientContext * &pCtx)
{
	pCtx = New CMatrixClientContext(this);

	return TRUE;
}

BOOL CMatrixSsl::CreateServerContext(ITlsServerContext * &pCtx)
{
	pCtx = New CMatrixServerContext(this);

	return TRUE;
}

// Attributes

CByteArray const & CMatrixSsl::GetTrustedRoots(void) const
{
	return m_Roots;
}

// Operations

sslKeys_t * CMatrixSsl::CreateKeys(void)
{
	sslKeys_t *pKeys = NULL;

	matrixSslNewKeys(&pKeys, NULL);

	return pKeys;
}

void * CMatrixSsl::CreateStream(IPREF LocAddr, WORD LocPort, IPREF RemAddr, WORD RemPort)
{
	return m_pStream->CreateStream(LocAddr, LocPort, RemAddr, RemPort);
}

void CMatrixSsl::LogStream(void *pStream, PCBYTE pData, UINT uData, BOOL fRecv)
{
	m_pStream->LogStream(pStream, pData, uData, fRecv);
}

void CMatrixSsl::DeleteStream(void *pStream, BOOL fReset)
{
	m_pStream->DeleteStream(pStream, fReset);
}

// Implementation

BOOL CMatrixSsl::LoadDefaultRoots(void)
{
	m_Roots.Empty();

	m_Roots.Append(RootCert, sizeof(RootCert));

	return TRUE;
}

// End of File
