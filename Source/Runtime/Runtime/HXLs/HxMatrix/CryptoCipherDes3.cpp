
#include "Intern.hpp"

#include "CryptoCipherDes3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DES3 Symmetric Cipher
//

// Instantiator

static IUnknown * Create_CryptoCipherDes3(PCTXT pName)
{
	return New CCryptoCipherDes3;
}

// Registration

global void Register_CryptoCipherDes3(void)
{
	piob->RegisterInstantiator("crypto.cipher-des3", Create_CryptoCipherDes3);
}

// Constructor

CCryptoCipherDes3::CCryptoCipherDes3(void)
{
}

// ICryptoHash

CString CCryptoCipherDes3::GetName(void)
{
	return "des3";
}

void CCryptoCipherDes3::Initialize(PCBYTE pPass, UINT uPass)
{
	if( uPass > 8 ) {

		BYTE bVec[8];

		BYTE bKey[24];

		MakeBytes(bVec, sizeof(bVec), pPass, 8);

		pPass += 8;

		uPass -= 8;

		MakeBytes(bKey, sizeof(bKey), pPass, uPass);

		psDes3Init(&m_Ctx, bVec, bKey);
	}
}

void CCryptoCipherDes3::Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)
{
	psDes3Encrypt(&m_Ctx, pIn, pOut, uSize);
}

void CCryptoCipherDes3::Decrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)
{
	psDes3Decrypt(&m_Ctx, pIn, pOut, uSize);
}

void CCryptoCipherDes3::Finalize(void)
{
	psDes3Clear(&m_Ctx);
}

// End of File
