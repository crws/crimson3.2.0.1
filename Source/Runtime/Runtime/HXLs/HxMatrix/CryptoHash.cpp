
#include "Intern.hpp"

#include "CryptoHash.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cryptographic Hash
//

// Constructor

CCryptoHash::CCryptoHash(void)
{
	StdSetRef();

	m_uState = stateNew;

	m_uHash  = 0;

	m_pHash  = NULL;
	}

// IUnknown

HRESULT CCryptoHash::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ICryptoHash);

	StdQueryInterface(ICryptoHash);

	return E_NOINTERFACE;
	}

ULONG CCryptoHash::AddRef(void)
{
	StdAddRef();
	}

ULONG CCryptoHash::Release(void)
{
	StdRelease();
	}

// ICryptoHash

UINT CCryptoHash::GetHashSize(void)
{
	return m_uHash;
	}

PCBYTE CCryptoHash::GetHashData(void)
{
	AfxAssert(m_uState == stateDone);

	return m_pHash;
	}

BOOL CCryptoHash::GetHashData(CByteArray &Data)
{
	AfxAssert(m_uState == stateDone);

	Data.Empty();

	Data.Append(m_pHash, m_uHash);

	return TRUE;
	}

CString CCryptoHash::GetHashString(UINT Code)
{
	AfxAssert(m_uState == stateDone);

	return Encode(Code, m_pHash, m_uHash);
	}

void CCryptoHash::Update(CByteArray const &Data)
{
	((ICryptoHash *) this)->Update(Data.GetPointer(), Data.GetCount());
	}

void CCryptoHash::Update(CString const &Data)
{
	((ICryptoHash *) this)->Update(PCBYTE(PCTXT(Data)), Data.GetLength());
	}

// End of File
