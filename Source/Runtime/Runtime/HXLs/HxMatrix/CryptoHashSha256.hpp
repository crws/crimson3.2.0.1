
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHashSha256_HPP

#define INCLUDE_CryptoHashSha256_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoHash.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SHA256 Cryptographic Hash
//

class CCryptoHashSha256 : public CCryptoHash
{
	public:
		// Constructor
		CCryptoHashSha256(void);

		// ICryptoHash
		CString GetName(void);
		void    GetHashOid(CByteArray &oid);
		void    Initialize(void);
		void    Update(PCBYTE pData, UINT uData);
		void    Finalize(void);

	protected:
		// Data Members
		BYTE       m_bHash[SHA256_HASH_SIZE];
		psSha256_t m_Ctx;
	};

// End of File

#endif
