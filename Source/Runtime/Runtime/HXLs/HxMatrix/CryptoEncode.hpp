
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoEncode_HPP

#define INCLUDE_CryptoEncode_HPP

//////////////////////////////////////////////////////////////////////////
//
// Cryptographic Encoding Helper
//

class CCryptoEncode
{
	protected:
		// String Encoder
		static CString Encode(UINT Code, PCBYTE pData, UINT uData);

		// Encoding Helpers
		static BYTE EncodeBase64(BYTE bData);
		static BOOL EncodeBase64(PTXT pBuff, PCBYTE pData, UINT uSize, BOOL fBreak);
		static BYTE EncodeBase64Url(BYTE bData);
		static BOOL EncodeBase64Url(PTXT pBuff, PCBYTE pData, UINT uSize);
		static BOOL EncodeUrl(CString &Text);
	};

// End of File

#endif
