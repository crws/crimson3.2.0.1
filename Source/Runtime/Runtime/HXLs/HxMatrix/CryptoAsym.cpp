\
#include "Intern.hpp"

#include "CryptoAsym.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Asymmetric Encryption Provider
//

// Constructor

CCryptoAsym::CCryptoAsym(void)
{
	StdSetRef();

	m_uState   = stateNew;

	m_uKeyBits = 0;

	m_uKeySize = 0;
	}

// Destructor

CCryptoAsym::~CCryptoAsym(void)
{
	}

// IUnknown

HRESULT CCryptoAsym::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ICryptoAsym);

	StdQueryInterface(ICryptoAsym);

	return E_NOINTERFACE;
	}

ULONG CCryptoAsym::AddRef(void)
{
	StdAddRef();
	}

ULONG CCryptoAsym::Release(void)
{
	StdRelease();
	}

// ICryptoAsym

UINT CCryptoAsym::GetKeyBytes(void)
{
	return m_uKeySize;
	}

BOOL CCryptoAsym::Initialize(PCBYTE pKey, UINT uKey, CString const &Pass, UINT uFlags)
{
	AfxAssert(FALSE);

	return FALSE;
	}

BOOL CCryptoAsym::Initialize(CByteArray const &Key, CString const &Pass, UINT uFlags)
{
	return ((ICryptoAsym *) this)->Initialize(Key.GetPointer(), Key.GetCount(), Pass, uFlags);
	}

BOOL CCryptoAsym::Encrypt(CByteArray &Out, CByteArray const &In)
{
	Out.Empty();

	Out.SetCount(GetKeyBytes());

	return ((ICryptoAsym *) this)->Encrypt(PBYTE(Out.GetPointer()), In.GetPointer(), In.GetCount());
	}

// End of File
