
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHashMd5_HPP

#define INCLUDE_CryptoHashMd5_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoHash.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MD5 Cryptographic Hash
//

class CCryptoHashMd5 : public CCryptoHash
{
	public:
		// Constructor
		CCryptoHashMd5(void);

		// ICryptoHash
		CString GetName(void);
		void    GetHashOid(CByteArray &oid);
		void    Initialize(void);
		void    Update(PCBYTE pData, UINT uData);
		void    Finalize(void);

	protected:
		// Data Members
		BYTE    m_bHash[MD5_HASH_SIZE];
		psMd5_t m_Ctx;
	};

// End of File

#endif
