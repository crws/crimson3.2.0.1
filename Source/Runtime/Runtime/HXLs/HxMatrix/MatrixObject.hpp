
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MatrixObject_HPP

#define INCLUDE_MatrixObject_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MatrixLibrary.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Object
//

class CMatrixSsl : public CMatrixLibrary, public ITlsLibrary
{
	public:
		// Constructor
		CMatrixSsl(void);

		// Destructor
		~CMatrixSsl(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IMatrixSsl
		BOOL METHOD LoadTrustedRoots(PCBYTE pRoot, UINT uRoot);
		BOOL METHOD CreateClientContext(ITlsClientContext * &pCtx);
		BOOL METHOD CreateServerContext(ITlsServerContext * &pCtx);

		// Attributes
		CByteArray const & GetTrustedRoots(void) const;

		// Operations
		sslKeys_t * CreateKeys(void);
		void      * CreateStream(IPREF LocAddr, WORD LocPort, IPREF RemAddr, WORD RemPort);
		void        LogStream(void *pStream, PCBYTE pData, UINT uData, BOOL fRecv);
		void        DeleteStream(void *pStream, BOOL fReset);

	protected:
		// Data Members
		UINT         m_uRefs;
		CTcpStream * m_pStream;
		CByteArray   m_Roots;

		// Implementation
		BOOL LoadDefaultRoots(void);
	};

// End of File

#endif
