
#include "Intern.hpp"

#include "MatrixClientContext.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classe
//

#include "MatrixObject.hpp"

#include "MatrixClientSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix Client Context
//

// Constructor

CMatrixClientContext::CMatrixClientContext(CMatrixSsl *pSsl) : CMatrixContext(pSsl)
{
}

// Destructor

CMatrixClientContext::~CMatrixClientContext(void)
{
}

// IUnknown

HRESULT CMatrixClientContext::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ITlsClientContext);

	StdQueryInterface(ITlsClientContext);

	return E_NOINTERFACE;
}

ULONG CMatrixClientContext::AddRef(void)
{
	StdAddRef();
}

ULONG CMatrixClientContext::Release(void)
{
	StdRelease();
}

// ITlsClientContext

BOOL CMatrixClientContext::LoadTrustedRoots(PCBYTE pRoot, UINT uRoot)
{
	return CMatrixContext::LoadTrustedRoots(pRoot, uRoot);
}

BOOL CMatrixClientContext::LoadClientCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass)
{
	return CMatrixContext::LoadIdentCert(pCert, uCert, pPriv, uPriv, pPass);
}

ISocket * CMatrixClientContext::CreateSocket(ISocket *pSock, PCTXT pName, UINT uCheck)
{
	if( MakeKeys(TRUE) ) {

		if( !pSock ) {

			AfxNewObject("sock-tcp", ISocket, pSock);
		}

		if( !uCheck ) {

			pName = NULL;
		}

		return New CMatrixClientSocket(m_pSsl, pSock, m_pKeys, pName, uCheck);
	}

	return pSock;
}

// End of File
