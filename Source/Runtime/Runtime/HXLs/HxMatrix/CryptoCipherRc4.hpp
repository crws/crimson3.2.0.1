
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoCipherRc4_HPP

#define INCLUDE_CryptoCipherRc4_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoSym.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RC4 Symmetric Cipher
//

class CCryptoCipherRc4 : public CCryptoSym
{
	public:
		// Constructor
		CCryptoCipherRc4(void);

		// ICryptoSym
		CString GetName(void);
		void    Initialize(PCBYTE pPass, UINT uPass);
		void    Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize);
		void    Decrypt(PBYTE pOut, PCBYTE pIn, UINT uSize);
		void    Finalize(void);

	protected:
		// Data Members
		psArc4_t m_Ctx;
	};

// End of File

#endif
