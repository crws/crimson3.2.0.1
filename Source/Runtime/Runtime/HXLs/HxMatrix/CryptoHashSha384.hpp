
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHashSha384_HPP

#define INCLUDE_CryptoHashSha384_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoHash.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SHA384 Cryptographic Hash
//

class CCryptoHashSha384 : public CCryptoHash
{
	public:
		// Constructor
		CCryptoHashSha384(void);

		// ICryptoHash
		CString GetName(void);
		void    GetHashOid(CByteArray &oid);
		void    Initialize(void);
		void    Update(PCBYTE pData, UINT uData);
		void    Finalize(void);

	protected:
		// Data Members
		BYTE       m_bHash[SHA384_HASH_SIZE];
		psSha384_t m_Ctx;
	};

// End of File

#endif
