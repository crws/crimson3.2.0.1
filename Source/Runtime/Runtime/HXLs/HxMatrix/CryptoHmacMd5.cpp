
#include "Intern.hpp"

#include "CryptoHmacMd5.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MD5 Cryptographic HMAC
//

// Instantiator

static IUnknown * Create_CryptoHmacMd5(PCTXT pName)
{
	return New CCryptoHmacMd5;
	}

// Registration

global void Register_CryptoHmacMd5(void)
{
	piob->RegisterInstantiator("crypto.hmac-md5", Create_CryptoHmacMd5);
	}

// Constructor

CCryptoHmacMd5::CCryptoHmacMd5(void)
{
	m_uHash = sizeof(m_bHash);

	m_pHash = m_bHash;
	}

// ICryptoHmac

CString CCryptoHmacMd5::GetName(void)
{
	return "md5";
	}

void CCryptoHmacMd5::Initialize(PCBYTE pPass, UINT uPass)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	psHmacMd5Init(&m_Ctx, pPass, uPass);

	m_uState = stateActive;
	}

void CCryptoHmacMd5::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	psHmacMd5Update(&m_Ctx, pData, uData);
	}

void CCryptoHmacMd5::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	psHmacMd5Final(&m_Ctx, m_pHash);

	m_uState = stateDone;
	}

// End of File
