
#include "Intern.hpp"

#include "CryptoHmacSha384.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SHA384 Cryptographic HMAC
//

// Instantiator

static IUnknown * Create_CryptoHmacSha384(PCTXT pName)
{
	return New CCryptoHmacSha384;
	}

// Registration

global void Register_CryptoHmacSha384(void)
{
	piob->RegisterInstantiator("crypto.hmac-sha384", Create_CryptoHmacSha384);
	}

// Constructor

CCryptoHmacSha384::CCryptoHmacSha384(void)
{
	m_uHash = sizeof(m_bHash);

	m_pHash = m_bHash;
	}

// ICryptoHmac

CString CCryptoHmacSha384::GetName(void)
{
	return "sha384";
	}

void CCryptoHmacSha384::Initialize(PCBYTE pPass, UINT uPass)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	psHmacSha384Init(&m_Ctx, pPass, uPass);

	m_uState = stateActive;
	}

void CCryptoHmacSha384::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	psHmacSha384Update(&m_Ctx, pData, uData);
	}

void CCryptoHmacSha384::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	psHmacSha384Final(&m_Ctx, m_pHash);

	m_uState = stateDone;
	}

// End of File
