
#include "Intern.hpp"

#include "MatrixSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MatrixObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix Socket
//

// Constructor

CMatrixSocket::CMatrixSocket(CMatrixSsl *pSsl, ISocket *pSock, sslKeys_t *pKeys)
{
	StdSetRef();

	m_pSsl     = pSsl;

	m_pSock    = pSock;

	m_pKeys    = pKeys;

	m_uState   = stateInit;

	m_pAppData = NULL;

	m_uAppData = 0;

	m_pSession = NULL;

	m_pStream  = NULL;

	InitOptions();
}

// Destructor

CMatrixSocket::~CMatrixSocket(void)
{
	KillStream(TRUE);

	m_pSock->Release();

	KillSession();

	KillOptions();
}

// IUnknown

HRESULT CMatrixSocket::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ISocket);

	StdQueryInterface(ISocket);

	return E_NOINTERFACE;
}

ULONG CMatrixSocket::AddRef(void)
{
	StdAddRef();
}

ULONG CMatrixSocket::Release(void)
{
	StdRelease();
}

// ISocket Methods

HRM CMatrixSocket::Listen(WORD Loc)
{
	return E_FAIL;
}

HRM CMatrixSocket::Listen(IPADDR const &IP, WORD Loc)
{
	return E_FAIL;
}

HRM CMatrixSocket::Connect(IPADDR const &IP, WORD Rem)
{
	return E_FAIL;
}

HRM CMatrixSocket::Connect(IPADDR const &IP, WORD Rem, WORD Loc)
{
	return E_FAIL;
}

HRM CMatrixSocket::Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc)
{
	return E_FAIL;
}

HRM CMatrixSocket::Recv(PBYTE pData, UINT &uSize, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pData, uSize) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	uSize = 0;

	return E_FAIL;
}

HRM CMatrixSocket::Recv(PBYTE pData, UINT &uSize)
{
	PumpRecvData();

	if( m_uAppData ) {

		UINT uRecv = min(uSize, m_uAppData);

		memcpy(pData, m_pAppData, uRecv);

		m_pAppData += uRecv;

		m_uAppData -= uRecv;

		PumpRecvDone();

		uSize = uRecv;

		return S_OK;
	}

	uSize = 0;

	return E_FAIL;
}

HRM CMatrixSocket::Send(PBYTE pData, UINT &uSize)
{
	UINT Phase;

	if( GetPhase(Phase) == S_OK ) {

		if( Phase == PHASE_OPEN ) {

			PBYTE pWork = NULL;

			UINT  uWork = matrixSslGetWritebuf(m_pSession, &pWork, uSize);

			if( int(uWork) > 0 ) {

				UINT uSend = min(uSize, uWork);

				memcpy(pWork, pData, uSend);

				m_pSsl->LogStream(m_pStream, pWork, uSend, FALSE);

				matrixSslEncodeWritebuf(m_pSession, uSend);

				PumpSendData();

				uSize = uSend;

				return S_OK;
			}
		}
	}

	uSize = 0;

	return E_FAIL;
}

HRM CMatrixSocket::Recv(CBuffer * &pBuff, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pBuff) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	pBuff = NULL;

	return E_FAIL;
}

HRM CMatrixSocket::Recv(CBuffer * &pBuff)
{
	PumpRecvData();

	if( m_uAppData ) {

		UINT uRecv = min(1600, m_uAppData);

		if( (pBuff = BuffAllocate(uRecv)) ) {

			memcpy(pBuff->AddTail(uRecv), m_pAppData, uRecv);

			m_pAppData += uRecv;

			m_uAppData -= uRecv;

			PumpRecvDone();

			return S_OK;
		}
	}

	pBuff = NULL;

	return E_FAIL;
}

HRM CMatrixSocket::Send(CBuffer *pBuff)
{
	UINT Phase;

	while( GetPhase(Phase) == S_OK ) {

		if( Phase == PHASE_OPEN ) {

			UINT  uSize = pBuff->GetSize();

			PBYTE pWork = NULL;

			UINT  uWork = matrixSslGetWritebuf(m_pSession, &pWork, uSize);

			if( int(uWork) > 0 ) {

				UINT uSend = min(uSize, uWork);

				memcpy(pWork, pBuff->StripHead(uSend), uSend);

				m_pSsl->LogStream(m_pStream, pWork, uSend, FALSE);

				matrixSslEncodeWritebuf(m_pSession, uSend);

				PumpSendData();

				if( !pBuff->GetSize() ) {

					pBuff->Release();

					return S_OK;
				}

				continue;
			}

			break;
		}
	}

	return E_FAIL;
}

HRM CMatrixSocket::GetLocal(IPADDR &IP)
{
	return m_pSock->GetLocal(IP);
}

HRM CMatrixSocket::GetRemote(IPADDR &IP)
{
	return m_pSock->GetRemote(IP);
}

HRM CMatrixSocket::GetLocal(IPADDR &IP, WORD &Port)
{
	return m_pSock->GetLocal(IP, Port);
}

HRM CMatrixSocket::GetRemote(IPADDR &IP, WORD &Port)
{
	return m_pSock->GetRemote(IP, Port);
}

HRM CMatrixSocket::GetPhase(UINT &Phase)
{
	UINT Lower;

	if( m_pSock->GetPhase(Lower) == S_OK ) {

		switch( Lower ) {

			case PHASE_IDLE:

				if( m_uState == stateOpening ) {

					Phase = PHASE_IDLE;

					return S_OK;
				}

				return E_FAIL;

			case PHASE_OPENING:

				if( m_uState == stateOpening ) {

					Phase = PHASE_OPENING;

					return S_OK;
				}

				return E_FAIL;

			case PHASE_OPEN:

				if( m_uState == stateHandshake || m_uState == stateOpen ) {

					PumpRecvData();
				}

				if( m_uState == stateOpening ) {

					InitSession();

					m_uState = stateHandshake;

					Phase    = PHASE_OPENING;

					return S_OK;
				}

				if( m_uState == stateHandshake ) {

					Phase = PHASE_OPENING;

					return S_OK;
				}

				if( m_uState == stateOpen ) {

					Phase = PHASE_OPEN;

					return S_OK;
				}

				if( m_uState == stateError ) {

					m_pSock->Abort();

					Phase = PHASE_ERROR;

					return S_OK;
				}

				return E_FAIL;

			case PHASE_CLOSING:

				PumpSendData();

				m_uState = stateClosing;

				Phase    = PHASE_CLOSING;

				return S_OK;

			case PHASE_ERROR:

				if( m_uState != stateError ) {

					KillStream(TRUE);
				}

				m_uState = stateError;

				Phase    = PHASE_ERROR;

				return S_OK;
		}
	}

	return E_FAIL;
}

HRM CMatrixSocket::SetOption(UINT uOption, UINT uValue)
{
	if( uOption == OPT_GET_CHILD ) {

		ISocket **ppSock = (ISocket **) uValue;

		*ppSock          = m_pSock;

		return S_OK;
	}

	if( uOption == OPT_SET_CHILD ) {

		ISocket *pSock = (ISocket *) uValue;

		m_pSock        = pSock;

		return S_OK;
	}

	return m_pSock->SetOption(uOption, uValue);
}

HRM CMatrixSocket::Abort(void)
{
	KillStream(TRUE);

	return m_pSock->Abort();
}

HRM CMatrixSocket::Close(void)
{
	matrixSslEncodeClosureAlert(m_pSession);

	while( PumpSendData() ) {

		// TODO -- This should use a new state to
		// allow the data to be pumped in the GetPhase
		// call and then to close the socket only
		// when it has been transmitted.

		Sleep(10);
	}

	KillStream(FALSE);

	return m_pSock->Close();
}

// Implementation

void CMatrixSocket::InitOptions(void)
{
	m_pOptions = New sslSessOpts_t;

	memset(m_pOptions, 0, sizeof(sslSessOpts_t));

	m_pOptions->versionFlag = 0;

	m_pOptions->userPtr     = this;
}

void CMatrixSocket::InitStream(void)
{
	if( !m_pStream ) {

		IPADDR LocAddr, RemAddr;

		WORD   LocPort, RemPort;

		m_pSock->GetLocal(LocAddr, LocPort);

		m_pSock->GetRemote(RemAddr, RemPort);

		m_pStream = m_pSsl->CreateStream(LocAddr, LocPort, RemAddr, RemPort);
	}
}

BOOL CMatrixSocket::UseExisting(void)
{
	if( m_uState == stateInit ) {

		UINT Lower;

		if( m_pSock->GetPhase(Lower) == S_OK ) {

			switch( Lower ) {

				case PHASE_OPEN:

					InitSession();

					m_uState = stateHandshake;

					return TRUE;

				case PHASE_ERROR:

					// TODO -- Does this clean up okay? It'll only happen if we try to
					// convert an existing socket to an SSL socket, and the connection
					// has died underneath us before we get it done. The effect is that
					// all the destructor stuff will get called without an InitSession.

					m_uState = stateError;

					return TRUE;

				case PHASE_CLOSING:

					// TODO -- Likewise.

					m_uState = stateClosing;

					return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CMatrixSocket::PumpSendData(void)
{
	BOOL	       fData = FALSE;

	int32          nData;

	unsigned char *pData;

	while( (nData = matrixSslGetOutdata(m_pSession, &pData)) > 0 ) {

		UINT uCount = nData;

		if( m_pSock->Send(pData, uCount) == S_OK ) {

			int32 nCode = matrixSslSentData(m_pSession, uCount);

			if( nCode == MATRIXSSL_HANDSHAKE_COMPLETE ) {

				m_uState = stateOpen;
			}

			if( nCode == MATRIXSSL_REQUEST_CLOSE ) {

				m_pSock->Close();

				return FALSE;
			}

			fData = TRUE;
		}
		else {
			UINT Phase;

			if( m_pSock->GetPhase(Phase) == S_OK ) {

				if( Phase == PHASE_ERROR ) {

					return FALSE;
				}
			}

			break;
		}
	}

	return fData;
}

BOOL CMatrixSocket::PumpRecvData(void)
{
	if( !m_uAppData ) {

		PBYTE  pBuff = NULL;

		UINT   uBuff = matrixSslGetReadbuf(m_pSession, &pBuff);

		UINT   uRead = uBuff;

		if( m_pSock->Recv(pBuff, uRead) == S_OK ) {

			PBYTE  pData = NULL;

			uint32 uData = 0;

			int32  nCode = matrixSslReceivedData(m_pSession, uRead, &pData, &uData);

			for( ;;) {

				if( nCode == MATRIXSSL_RECEIVED_ALERT ) {

					CheckAlert(pData[0], pData[1]);

					nCode = matrixSslProcessedData(m_pSession, &pData, &uData);

					continue;
				}

				if( nCode == MATRIXSSL_APP_DATA ) {

					m_uState   = stateOpen;

					m_pAppData = pData;

					m_uAppData = uData;

					InitStream();

					m_pSsl->LogStream(m_pStream, pData, uData, TRUE);
				}

				if( nCode == MATRIXSSL_REQUEST_SEND ) {

					PumpSendData();
				}

				if( nCode == MATRIXSSL_HANDSHAKE_COMPLETE ) {

					m_uState = stateOpen;

					InitStream();
				}

				break;
			}

			PumpSendData();

			return TRUE;
		}
	}

	PumpSendData();

	return FALSE;
}

BOOL CMatrixSocket::PumpRecvDone(void)
{
	if( !m_uAppData ) {

		m_pAppData = NULL;

		for( ;;) {

			PBYTE  pMore = NULL;

			uint32 uMore = 0;

			int32  nCode = matrixSslProcessedData(m_pSession, &pMore, &uMore);

			if( nCode == MATRIXSSL_RECEIVED_ALERT ) {

				CheckAlert(pMore[0], pMore[1]);

				continue;
			}

			if( nCode == MATRIXSSL_APP_DATA ) {

				m_pAppData = pMore;

				m_uAppData = uMore;
			}

			if( nCode == MATRIXSSL_REQUEST_SEND ) {

				PumpSendData();
			}

			break;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CMatrixSocket::CheckAlert(BYTE bLevel, BYTE bDesc)
{
	AfxTrace("Alert %u.%u\n", bLevel, bDesc);

	if( bLevel == SSL_ALERT_LEVEL_FATAL ) {

		m_pSock->Abort();

		return TRUE;
	}

	return FALSE;
}

void CMatrixSocket::KillOptions(void)
{
	if( m_pOptions ) {

		delete m_pOptions;
	}
}

void CMatrixSocket::KillStream(BOOL fReset)
{
	if( m_pStream ) {

		m_pSsl->DeleteStream(m_pStream, fReset);

		m_pStream = NULL;
	}
}

void CMatrixSocket::KillSession(void)
{
	if( m_pSession ) {

		matrixSslDeleteSession(m_pSession);
	}
}

// End of File
