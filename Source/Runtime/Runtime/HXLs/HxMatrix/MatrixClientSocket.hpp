
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MatrixClientSocket_HPP

#define INCLUDE_MatrixClientSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MatrixSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix Client Socket
//

class CMatrixClientSocket : public CMatrixSocket
{
	public:
		// Constructor
		CMatrixClientSocket(CMatrixSsl *pSsl, ISocket *pSock, sslKeys_t *pKeys, PCTXT pName, UINT uCheck);

		// Destructor
		~CMatrixClientSocket(void);

		// ISocket Methods
		HRM Connect(IPADDR const &IP, WORD Rem);
		HRM Connect(IPADDR const &IP, WORD Rem, WORD Loc);
		HRM Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc);

	protected:
		// Data Members
		CString           m_Name;
		UINT		  m_uCheck;
		sslSessionId_t	* m_pSid;
		tlsExtension_t	* m_pHello;

		// Overridables
		void InitSession(void);

		// Implementation
		void InitSid(void);
		void InitHello(void);
		void KillSid(void);
		void KillHello(void);

		// Callbacks
		int32 CheckCert(psX509Cert_t *cert, int32 alert);

		// Callback Hooks
		static int32 HookCheckCert(ssl_t *ssl, psX509Cert_t *cert, int32 alert);
	};

// End of File

#endif
