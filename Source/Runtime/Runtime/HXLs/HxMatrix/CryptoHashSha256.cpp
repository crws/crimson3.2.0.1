
#include "Intern.hpp"

#include "CryptoHashSha256.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SHA256 Cryptographic Hash
//

// Instantiator

static IUnknown * Create_CryptoHashSha256(PCTXT pName)
{
	return New CCryptoHashSha256;
	}

// Registration

global void Register_CryptoHashSha256(void)
{
	piob->RegisterInstantiator("crypto.hash-sha256", Create_CryptoHashSha256);
	}

// Constructor

CCryptoHashSha256::CCryptoHashSha256(void)
{
	m_uHash = sizeof(m_bHash);

	m_pHash = m_bHash;

	psSha256PreInit(&m_Ctx);
	}

// ICryptoHash

CString CCryptoHashSha256::GetName(void)
{
	return "sha256";
	}

void CCryptoHashSha256::GetHashOid(CByteArray &oid)
{
	BYTE b[] = { 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x01 };

	oid.Empty();

	oid.Append(b, sizeof(b));
	}

void CCryptoHashSha256::Initialize(void)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	psSha256Init(&m_Ctx);

	m_uState = stateActive;
	}

void CCryptoHashSha256::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	psSha256Update(&m_Ctx, pData, uData);
	}

void CCryptoHashSha256::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	psSha256Final(&m_Ctx, m_pHash);

	m_uState = stateDone;
	}

// End of File
