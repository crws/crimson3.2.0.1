
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoVerifyEc_HPP

#define INCLUDE_CryptoVerifyEc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoVerify.hpp"

//////////////////////////////////////////////////////////////////////////
//
// EC Cryptographic Verifier
//

class CCryptoVerifyEc : public CCryptoVerify
{
	public:
		// Constructor
		CCryptoVerifyEc(void);

		// Destructor
		~CCryptoVerifyEc(void);

		// ICryptoVerify
		CString GetName(void);
		BOOL    Verify(PCBYTE pSig, UINT uSig, UINT uFormat);
		BOOL    Initialize(PCBYTE pKey, UINT uKey);

	protected:
		// Data Members
		psEccCurve_t const *m_pCurve;
		psEccKey_t	    m_Key;

		// Implementation
		void MakeFile(char *pName, PCBYTE pData, UINT uSize);
		void PostInitialize(void);
	};

// End of File

#endif
