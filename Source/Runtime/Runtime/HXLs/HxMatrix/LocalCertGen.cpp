
#include "Intern.hpp"

#include "LocalCertGen.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Key Strings
//

// TODO -- Should we bury these somewhere? !!!

#define GetStringOne() "3X2jCwVAqKz*!I8TjAxh"

#define GetStringTwo() "HC4DZQHk4jMSbt!ZTIr9"

//////////////////////////////////////////////////////////////////////////
//
// Key and Cert Data
//

static BYTE cert_key[] = {
	#include "cert.key.dat"
	0
};

static BYTE root_key[] = {
	#include "root.key.dat"
	0
};

static BYTE root_crt[] = {
	#include "root.crt.dat"
	0
};

//////////////////////////////////////////////////////////////////////////
//
// Local Certificate Generator
//

// Static Data

CString CLocalCertGen::m_LastList;

BYTE    CLocalCertGen::m_bLastRoot[4096] = { 0 };

BYTE    CLocalCertGen::m_bLastCert[4096] = { 0 };

UINT    CLocalCertGen::m_uLastRoot = 0;

UINT    CLocalCertGen::m_uLastCert = 0;

// Instantiator

static IUnknown * Create_LocalCertGen(PCTXT pName)
{
	return New CLocalCertGen;
}

// Registration

global void Register_LocalCertGen(void)
{
	piob->RegisterInstantiator("crypto.certgen", Create_LocalCertGen);
}

// Constructor

CLocalCertGen::CLocalCertGen(void)
{
	StdSetRef();

	memset(&m_RootKey, 0, sizeof(m_RootKey));

	memset(&m_CertKey, 0, sizeof(m_CertKey));

	m_pRootCert = NULL;

	memset(&m_Config, 0, sizeof(m_Config));

	m_pReq  = NULL;

	m_pDN   = NULL;

	m_pV3   = NULL;

	m_pPub  = NULL;

	m_pCert = NULL;

	m_pPriv = NULL;
}

// Destructor

CLocalCertGen::~CLocalCertGen(void)
{
	if( TRUE ) {

		psClearPubKey(&m_RootKey);

		psClearPubKey(&m_CertKey);
	}

	if( m_pRootCert ) {

		psX509FreeCert(m_pRootCert);
	}

	if( TRUE ) {

		psX509FreeCertConfig(&m_Config);
	}

	if( m_pDN ) {

		psX509FreeDNStruct(m_pDN, NULL);

		psFree(m_pDN, NULL);
	}

	if( m_pReq ) {

		psFree(m_pReq, NULL);
	}

	if( m_pV3 ) {

		x509FreeExtensions(m_pV3);

		psFree(m_pV3, NULL);
	}

	if( TRUE ) {

		psDeletePubKey(&m_pPub);
	}

	if( m_pCert != m_bLastCert ) {

		delete[] m_pCert;
	}

	if( m_pPriv ) {

		psFree(m_pPriv, NULL);
	}
}

// IUnknown

HRESULT CLocalCertGen::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ICertGen);

	StdQueryInterface(ICertGen);

	return E_NOINTERFACE;
}

ULONG CLocalCertGen::AddRef(void)
{
	StdAddRef();
}

ULONG CLocalCertGen::Release(void)
{
	StdRelease();
}

// ICertGen

void CLocalCertGen::AddName(PCTXT pName)
{
	CString Name = pName;

	m_Names.Append(Name);

	m_List += "DNS:";

	m_List += Name;

	m_List += ' ';
}

void CLocalCertGen::AddAddr(IPREF Addr)
{
	CString Name = Addr.GetAsText();

	m_Addrs.Append(Name);

	if( FALSE ) {

		// Not needed for Chrome and breaks root name constraints.

		m_Names.Append(Name);
	}

	m_List += "IP:";

	m_List += Name;

	m_List += ' ';
}

void CLocalCertGen::SetRootCert(PCBYTE pCert, UINT uCert)
{
	m_CustRootCert.Empty();

	m_CustRootCert.Append(pCert, uCert);

	m_CustRootCert.Append(0);
}

void CLocalCertGen::SetRootPriv(PCBYTE pPriv, UINT uPriv)
{
	m_CustRootPriv.Empty();

	m_CustRootPriv.Append(pPriv, uPriv);

	m_CustRootPriv.Append(0);
}

void CLocalCertGen::SetRootPass(PCTXT pPass)
{
	m_CustRootPass = pPass;
}

bool CLocalCertGen::Generate(void)
{
	CAutoGuard Guard;

	UINT   uRoot = m_CustRootCert.GetCount() ? m_CustRootCert.GetCount()   : sizeof(root_crt);

	PCBYTE pRoot = m_CustRootCert.GetCount() ? m_CustRootCert.GetPointer() : root_crt;

	if( m_LastList != m_List || m_uLastRoot != uRoot || memcmp(m_bLastRoot, pRoot, uRoot) ) {

		if( LoadRootKey() && LoadCertKey() ) {

			if( LoadRootCert() ) {

				if( LoadCertConfig() ) {

					psWriteCertReqMem(NULL, &m_CertKey, &m_Config, &m_pReq, &m_uReq);

					psParseCertReqBuf(NULL, m_pReq, m_uReq, &m_pDN, &m_pPub, &m_pV3);

					psX509SetCAIssuedCertExtensions(NULL, &m_Config, m_pV3, m_pRootCert);

					PCTXT pFile = "/tmp/cert.pem";

					if( WriteCert(pFile) ) {

						if( ReadCert(pFile) ) {

							unlink(pFile);

							m_LastList.Empty();

							memset(m_bLastCert, 0, sizeof(m_bLastCert));

							memcpy(m_bLastCert, m_pCert, m_uLastCert = m_uCert);

							memcpy(m_bLastRoot, pRoot, m_uLastRoot = uRoot);

							m_LastList = m_List;

							m_uPriv    = sizeof(cert_key) - 1;

							m_pPriv    = LoadBlob(cert_key, sizeof(cert_key), TRUE);

							return true;
						}
					}

					unlink(pFile);
				}
			}
		}

		AfxTrace("certgen: can't make certificate\n");

		return false;
	}

	if( LoadCertKey() ) {

		m_pCert = m_bLastCert;

		m_uCert = m_uLastCert;

		m_uPriv = sizeof(cert_key) - 1;

		m_pPriv = LoadBlob(cert_key, sizeof(cert_key), TRUE);

		return true;
	}

	return false;
}

PCBYTE CLocalCertGen::GetRootData(void)
{
	return root_crt;
}

UINT CLocalCertGen::GetRootSize(void)
{
	return sizeof(root_crt);
}

PCBYTE CLocalCertGen::GetCertData(void)
{
	return m_pCert;
}

UINT CLocalCertGen::GetCertSize(void)
{
	return m_uCert;
}

PCBYTE CLocalCertGen::GetPrivData(void)
{
	return m_pPriv;
}

UINT CLocalCertGen::GetPrivSize(void)
{
	return m_uPriv;
}

PCTXT CLocalCertGen::GetPrivPass(void)
{
	return GetStringTwo();
}

// Implementation

bool CLocalCertGen::LoadRootKey(void)
{
	if( !m_RootKey.keysize ) {

		char file[20];

		char const *pass;

		if( !m_CustRootPriv.IsEmpty() ) {

			MakeFile(file, m_CustRootPriv.GetPointer(), m_CustRootPriv.GetCount(), FALSE);

			pass = m_CustRootPass;
		}
		else {
			MakeFile(file, root_key, sizeof(root_key), TRUE);

			pass = GetStringOne();
		}

		if( psParseUnknownPrivKey(NULL, 1, file, pass, &m_RootKey) < 0 ) {

			AfxTrace("certgen: can't load root key\n");

			return false;
		}
	}

	return true;
}

bool CLocalCertGen::LoadCertKey(void)
{
	if( !m_CertKey.keysize ) {

		char file[20];

		MakeFile(file, cert_key, sizeof(cert_key), TRUE);

		if( psParseUnknownPrivKey(NULL, 1, file, GetStringTwo(), &m_CertKey) < 0 ) {

			AfxTrace("certgen: can't load cert key\n");

			return false;
		}
	}

	return true;
}

bool CLocalCertGen::LoadRootCert(void)
{
	if( !m_pRootCert ) {

		char file[20];

		if( !m_CustRootCert.IsEmpty() ) {

			MakeFile(file, m_CustRootCert.GetPointer(), m_CustRootCert.GetCount(), FALSE);
		}
		else {
			MakeFile(file, root_crt, sizeof(root_crt), FALSE);
		}

		if( psX509ParseCertFile(NULL, file, &m_pRootCert, CERT_STORE_DN_BUFFER) < 0 ) {

			AfxTrace("certgen: can't load root cert\n");

			return false;
		}
	}

	return true;
}

bool CLocalCertGen::LoadCertConfig(void)
{
	BYTE serial[8];

	AfxGetAutoObject(pEntropy, "entropy", 0, IEntropy);

	pEntropy->GetEntropy(serial, sizeof(serial));

	BOOL fCommon  = FALSE;

	psX509SetValidDays(NULL, &m_Config, 3650);

	psX509SetSerialNum(NULL, &m_Config, serial, sizeof(serial));

	psX509SetCertHashAlg(NULL, &m_Config, ALG_SHA256);

	for( UINT n = 0; n < m_Names.GetCount(); n++ ) {

		if( !m_Names[n].IsEmpty() ) {

			subjectAltNameEntry_t san;

			memset(&san, 0, sizeof(san));

			san.dNSName    = (char *) (PCTXT) m_Names[n];

			san.dNSNameLen = strlen(san.dNSName);

			if( !fCommon ) {

				psX509SetDNAttribute(NULL,
						     &m_Config,
						     "commonName",
						     10,
						     san.dNSName,
						     san.dNSNameLen,
						     ASN_UTF8STRING
				);

				fCommon = TRUE;
			}

			psX509AddSubjectAltName(NULL, &m_Config, &san);
		}
	}

	for( UINT a = 0; a < m_Addrs.GetCount(); a++ ) {

		if( !m_Addrs[a].IsEmpty() ) {

			subjectAltNameEntry_t san;

			memset(&san, 0, sizeof(san));

			san.iPAddress    = (char *) (PCTXT) m_Addrs[a];

			san.iPAddressLen = strlen(san.iPAddress);

			if( !fCommon ) {

				psX509SetDNAttribute(NULL,
						     &m_Config,
						     "commonName",
						     10,
						     san.iPAddress,
						     san.iPAddressLen,
						     ASN_UTF8STRING
				);

				fCommon = TRUE;
			}

			psX509AddSubjectAltName(NULL, &m_Config, &san);
		}
	}

	psX509AddExtendedKeyUsage(NULL, &m_Config, "serverAuth");

	return true;
}

bool CLocalCertGen::WriteCert(PCTXT pFile)
{
	if( psX509WriteCAIssuedCert(NULL,
				    &m_Config,
				    m_pPub,
				    m_pDN->dnenc,
				    m_pDN->dnencLen,
				    m_pRootCert,
				    &m_RootKey,
				    PTXT(pFile)
	) < 0 ) {

		AfxTrace("certgen: can't generate certificate\n");

		return false;
	}

	return true;
}

bool CLocalCertGen::ReadCert(PCTXT pFile)
{
	FILE *pStream = fopen(pFile, "ra");

	if( pStream ) {

		fseek(pStream, 0, SEEK_END);

		UINT uSize = ftell(pStream);

		if( uSize < sizeof(m_bLastCert) ) {

			if( (m_uCert = uSize) ) {

				m_pCert = New BYTE[m_uCert + 1];

				fseek(pStream, 0, SEEK_SET);

				if( fread(m_pCert, 1, m_uCert, pStream) == m_uCert ) {

					m_pCert[m_uCert] = 0;

					fclose(pStream);

					return true;
				}
			}
		}

		fclose(pStream);
	}

	AfxTrace("certgen: can't read from temporary file\n");

	return false;
}

PBYTE CLocalCertGen::LoadBlob(PCBYTE pData, UINT uSize, BOOL fDecrypt)
{
	// Note the sizing here! The size that is passed in includes the NUL
	// added in this source file after the #includes with the data. We do
	// not decrypt this byte, but we do copy it into the buffer so that
	// we end up with a NUL at the end of our uSize bytes.

	PBYTE pCopy = PBYTE(psMalloc(NULL, uSize));

	memcpy(pCopy, pData, uSize);

	if( fDecrypt ) {

		PCTXT   pPass = "The file cannot be found.";

		BYTE    bHash[16];

		md5(PBYTE(pPass), strlen(pPass), bHash);

		rc4(pCopy, uSize-1, bHash, 16);
	}

	return pCopy;
}

void CLocalCertGen::MakeFile(char *pName, PCBYTE pData, UINT uSize, BOOL fDecrypt)
{
	// Note the sizing here! The blob we load has a NUL at the end,
	// which we need to stop Matrix from going crazy. But it must
	// not be included in the length of the file that we create! It
	// is just there to stop buggy Matrix code from running past
	// the end of the file or getting confused if it is unlucky
	// enough to see a non-NUL isspace character after the data.

	SPrintf(pName, "%X,%X", LoadBlob(pData, uSize, fDecrypt), uSize-1);
}

// End of File
