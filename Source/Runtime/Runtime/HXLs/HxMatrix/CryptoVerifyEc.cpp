
#include "Intern.hpp"

#include "CryptoVerifyEc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// EC Cryptographic Verifier
//

// Instantiator

static IUnknown * Create_CryptoVerifyEc(PCTXT pName)
{
	return New CCryptoVerifyEc;
	}

// Registration

global void Register_CryptoVerifyEc(void)
{
	piob->RegisterInstantiator("crypto.verify-ec-p256", Create_CryptoVerifyEc);
	}

// Constructor

CCryptoVerifyEc::CCryptoVerifyEc(void)
{
	m_pCurve = NULL;

	getEccParamById(IANA_SECP256R1, &m_pCurve);
	
	psEccInitKey(NULL, &m_Key, m_pCurve);
	}

// Destructor

CCryptoVerifyEc::~CCryptoVerifyEc(void)
{
	psEccClearKey(&m_Key);
	}

// ICryptoVerify

CString CCryptoVerifyEc::GetName(void)
{
	return "ec-p256";
	}

BOOL CCryptoVerifyEc::Verify(PCBYTE pSig, UINT uSig, UINT uFormat)
{
	AfxAssert(m_uState == stateDone);

	UINT    uHash  = m_pHash->GetHashSize();

	PCBYTE  pHash  = m_pHash->GetHashData();
	
	int32_t status = 0;

	if( uFormat == 0 ) {

		CByteArray Der;

		Der.Append(0x30);

		Der.Append(uSig + 4);

		Der.Append(0x02);

		Der.Append(uSig / 2);

		Der.Append(pSig + 0 * (uSig / 2), uSig / 2);

		Der.Append(0x02);

		Der.Append(uSig / 2);

		Der.Append(pSig + 1 * (uSig / 2), uSig / 2);

		psEccDsaVerify(NULL, &m_Key, pHash, uHash, Der.data(), Der.size(), &status, NULL);
	}

	if( uFormat == 1 ) {

		psEccDsaVerify(NULL, &m_Key, pHash, uHash, pSig, uSig, &status, NULL);
	}

	return status == 1;
}

BOOL CCryptoVerifyEc::Initialize(PCBYTE pKey, UINT uKey)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	if( m_pHash ) {

		CAutoArray<BYTE> Data(1+uKey);

		Data[0] = ANSI_UNCOMPRESSED;

		memcpy(Data + 1, pKey, uKey);

		if( psEccX963ImportKey(NULL, Data, 1+uKey, &m_Key, m_pCurve) == 0 ) {

			PostInitialize();

			return TRUE;
			}
		}

	return FALSE;
	}

// Implementation

void CCryptoVerifyEc::PostInitialize(void)
{
	m_pHash->Initialize();

	m_uState = stateActive;
	}

// End of File
