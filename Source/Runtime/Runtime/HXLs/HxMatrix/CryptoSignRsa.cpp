
#include "Intern.hpp"

#include "CryptoSignRsa.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// RSA Cryptographic Signer
//

// Instantiator

static IUnknown * Create_CryptoSignRsa(PCTXT pName)
{
	return New CCryptoSignRsa;
	}

// Registration

global void Register_CryptoSignRsa(void)
{
	piob->RegisterInstantiator("crypto.sign-rsa", Create_CryptoSignRsa);
	}

// Constructor

CCryptoSignRsa::CCryptoSignRsa(void)
{
	psRsaInitKey(NULL, &m_Key);
	}

// Destructor

CCryptoSignRsa::~CCryptoSignRsa(void)
{
	psRsaClearKey(&m_Key);
	}

// ICryptoSign

CString CCryptoSignRsa::GetName(void)
{
	return "rsa";
	}

BOOL CCryptoSignRsa::Initialize(PCBYTE pKey, UINT uKey, CString const &Pass)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	if( m_pHash ) {

		char sFile[32];

		MakeFile(sFile, pKey, uKey);

		if( psPkcs1ParsePrivFile(NULL, sFile, Pass, &m_Key) == 0 ) {

			delete [] m_pSig;

			PostInitialize();

			return TRUE;
			}
		}

	return FALSE;
	}

void CCryptoSignRsa::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	m_pHash->Finalize();
	
	if( m_uKeySize ) {

		UINT   uHash = m_pHash->GetHashSize();

		PCBYTE pHash = m_pHash->GetHashData();

		CByteArray EM;

		CByteArray T;
	
		CByteArray oid;

		m_pHash->GetHashOid(oid);

		UINT nid = oid.GetCount();

		T.Append(0x30);			// Sequence
		T.Append(0x08+nid+uHash);	// Length

		T.Append(0x30);			// Sequence
		T.Append(0x04+nid);		// Length

		T.Append(0x06);			// OID
		T.Append(nid);			// Length
		T.Append(oid);			// Algorithm
		T.Append(0x05);			// NULL
		T.Append(0x00);			// EOC

		T.Append(0x04);			// Octet String
		T.Append(uHash);		// Length
		T.Append(pHash, uHash);		// Hash

		EM.Append(0x00);
		EM.Append(0x01);

		for( UINT uPad = m_uKeySize - T.GetCount() - 3; uPad--; EM.Append(0xFF) );

		EM.Append(0x00);

		EM.Append(T);

		psSize_t n = m_uSig;

		psRsaCrypt( NULL,
			    &m_Key,
			    EM.GetPointer(),
			    EM.GetCount  (),
			    m_pSig,
			    &n,
			    PS_PRIVKEY,
			    NULL
			    );
		}

	m_uState = stateDone;
	}

// Implementation

void CCryptoSignRsa::MakeFile(char *pName, PCBYTE pData, UINT uSize)
{
	// Note the sizing here! We allocate one more byte and put
	// a NUL in there to terminate the data -- but we do not
	// include that NUL in the size of the file. It is just
	// there to stop Matrix wildly running beyond the end.

	PBYTE pCopy = PBYTE(psMalloc(NULL, uSize + 1));

	memcpy(pCopy, pData, uSize);

	pCopy[uSize] = 0;

	SPrintf(pName, "%X,%X", pCopy, uSize);
	}

void CCryptoSignRsa::PostInitialize(void)
{
	m_pHash->Initialize();

	m_uKeySize = 1 * m_Key.size;

	m_uKeyBits = 8 * m_Key.size;

	m_uSig     = m_uKeySize;

	m_pSig     = New BYTE [ m_uSig ];

	m_uState = stateActive;
	}

// End of File
