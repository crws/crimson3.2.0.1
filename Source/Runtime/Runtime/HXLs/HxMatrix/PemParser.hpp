
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PemParser_HPP

#define INCLUDE_PemParser_HPP

//////////////////////////////////////////////////////////////////////////
//
// PEM Parser
//

class CPemParser : public IPemParser
{
public:
	// Constructor
	CPemParser(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IPemParser
	BOOL Decode(CByteArray &Data, PCSTR pPass, PCSTR  pText);
	BOOL Decode(CByteArray &Data, PCSTR pPass, PCBYTE pText, UINT uSize);

protected:
	// Data Members
	ULONG m_uRefs;

	// Implementation
	BOOL IntDecode(CByteArray &Data, PCSTR pPass, CString &Text);
};

// End of File

#endif
