
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MatrixClientContext_HPP

#define INCLUDE_MatrixClientContext_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MatrixContext.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix Client Context
//

class CMatrixClientContext : public CMatrixContext, public ITlsClientContext
{
	public:
		// Constructor
		CMatrixClientContext(CMatrixSsl *pSsl);

		// Destructor
		~CMatrixClientContext(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ITlsClientContext
		BOOL	  LoadTrustedRoots(PCBYTE pRoot, UINT uRoot);
		BOOL      LoadClientCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass);
		ISocket * CreateSocket(ISocket *pSock, PCTXT pName, UINT uCheck);
	};

// End of File

#endif
