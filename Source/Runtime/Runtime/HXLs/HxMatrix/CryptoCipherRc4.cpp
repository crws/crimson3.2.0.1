
#include "Intern.hpp"

#include "CryptoCipherRc4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// RC4 Symmetric Cipher
//

// Instantiator

static IUnknown * Create_CryptoCipherRc4(PCTXT pName)
{
	return New CCryptoCipherRc4;
	}

// Registration

global void Register_CryptoCipherRc4(void)
{
	piob->RegisterInstantiator("crypto.cipher-rc4", Create_CryptoCipherRc4);
	}

// Constructor

CCryptoCipherRc4::CCryptoCipherRc4(void)
{
	}

// ICryptoHash

CString CCryptoCipherRc4::GetName(void)
{
	return "rc4";
	}

void CCryptoCipherRc4::Initialize(PCBYTE pPass, UINT uPass)
{
	psArc4Init(&m_Ctx, pPass, uPass);
	}

void CCryptoCipherRc4::Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)
{
	psArc4(&m_Ctx, pIn, pOut, uSize);
	}

void CCryptoCipherRc4::Decrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)
{
	psArc4(&m_Ctx, pIn, pOut, uSize);
	}

void CCryptoCipherRc4::Finalize(void)
{
	psArc4Clear(&m_Ctx);
	}

// End of File
