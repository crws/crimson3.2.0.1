
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern void Register_Matrix(void);
extern void Register_CryptoHashMd5(void);
extern void Register_CryptoHashSha1(void);
extern void Register_CryptoHashSha256(void);
extern void Register_CryptoHashSha384(void);
extern void Register_CryptoHashSha512(void);
extern void Register_CryptoHashNull(void);
extern void Register_CryptoHmacMd5(void);
extern void Register_CryptoHmacSha1(void);
extern void Register_CryptoHmacSha256(void);
extern void Register_CryptoHmacSha384(void);
extern void Register_CryptoCipherRc4(void);
extern void Register_CryptoCipherRc2(void);
extern void Register_CryptoCipherDes3(void);
extern void Register_CryptoCipherAes(void);
extern void Register_CryptoCipherRsa(void);
extern void Register_CryptoSignLegacy(void);
extern void Register_CryptoSignRsa(void);
extern void Register_CryptoVerifyEc(void);
extern void Register_LocalCertGen(void);
extern void Register_PemParser(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

void Register_HxMatrix(void)
{
	Register_Matrix();

	Register_CryptoHashMd5();
	Register_CryptoHashSha1();
	Register_CryptoHashSha256();
	Register_CryptoHashSha384();
	Register_CryptoHashSha512();
	Register_CryptoHashNull();

	Register_CryptoHmacMd5();
	Register_CryptoHmacSha1();
	Register_CryptoHmacSha256();
	Register_CryptoHmacSha384();

	Register_CryptoCipherRc4();
	Register_CryptoCipherRc2();
	Register_CryptoCipherDes3();
	Register_CryptoCipherAes();

	Register_CryptoCipherRsa();

	Register_CryptoSignLegacy();
	Register_CryptoSignRsa();

	Register_CryptoVerifyEc();

	Register_LocalCertGen();

	Register_PemParser();
}

void Revoke_HxMatrix(void)
{
	piob->RevokeGroup("matrix.");

	piob->RevokeGroup("crypto.");
}

// End of File
