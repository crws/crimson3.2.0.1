
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoVerify_HPP

#define INCLUDE_CryptoVerify_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoEncode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cryptographic Verifier
//

class CCryptoVerify : public CCryptoEncode, public ICryptoVerify
{
	public:
		// Constructor
		CCryptoVerify(void);

		// Destructor
		virtual ~CCryptoVerify(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICryptoVerify
		BOOL Verify(PCBYTE pSig, UINT uSig, UINT uFormat);
		BOOL Initialize(PCBYTE pKey, UINT uKey);
		BOOL Initialize(CByteArray const &Key);
		void SetHash(ICryptoHash *pHash);
		BOOL SetHash(PCSTR pName);
		void Update(PCBYTE pData, UINT uData);
		void Update(CByteArray const &Data);
		void Update(CString const &Data);
		void Finalize(void);

	protected:
		// States
		enum
		{
			stateNew,
			stateActive,
			stateDone
			};

		// Data Members
		ULONG	      m_uRefs;
		UINT	      m_uState;
		ICryptoHash * m_pHash;
	};

// End of File

#endif
