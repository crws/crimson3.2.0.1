
#include "Intern.hpp"

#include "CryptoHashNull.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Null Cryptographic Hash
//

// Instantiator

static IUnknown * Create_CryptoHashNull(PCTXT pName)
{
	return New CCryptoHashNull;
	}

// Registration

global void Register_CryptoHashNull(void)
{
	piob->RegisterInstantiator("crypto.hash-null", Create_CryptoHashNull);
	}

// Constructor

CCryptoHashNull::CCryptoHashNull(void)
{
	m_uHash = sizeof(m_bHash);

	m_pHash = m_bHash;
	}

// ICryptoHash

CString CCryptoHashNull::GetName(void)
{
	return "null";
	}

void CCryptoHashNull::GetHashOid(CByteArray &oid)
{
	}

void CCryptoHashNull::Initialize(void)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	memset(m_bHash, 0, sizeof(m_bHash));

	m_uState = stateActive;
	}

void CCryptoHashNull::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	memcpy(m_bHash, pData, min(uData, sizeof(m_bHash)));
	}

void CCryptoHashNull::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	m_uState = stateDone;
	}

// End of File
