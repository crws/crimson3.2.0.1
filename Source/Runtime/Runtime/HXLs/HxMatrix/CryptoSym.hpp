
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoSym_HPP

#define INCLUDE_CryptoSym_HPP

//////////////////////////////////////////////////////////////////////////
//
// Symmetric Encryption Provider
//

class CCryptoSym : public ICryptoSym
{
	public:
		// Constructor
		CCryptoSym(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICryptoSym
		void Initialize(CByteArray const &Pass);
		void Initialize(CString const &Pass);
		void Encrypt(PBYTE pData, UINT uSize);
		void Encrypt(CByteArray &Out, CByteArray const &In);
		void Encrypt(CByteArray &Data);
		void Decrypt(PBYTE pData, UINT uSize);
		void Decrypt(CByteArray &Out, CByteArray const &In);
		void Decrypt(CByteArray &Data);

	protected:
		// States
		enum
		{
			stateNew,
			stateActive,
			stateDone
			};

		// Data Members
		ULONG m_uRefs;
		UINT  m_uState;

		// Implementation
		void MakeBytes(PBYTE pOut, UINT uOut, PCBYTE pIn, UINT uIn);
	};

// End of File

#endif
