
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHmac_HPP

#define INCLUDE_CryptoHmac_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoEncode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cryptographic HMAC
//

class CCryptoHmac : public CCryptoEncode, public ICryptoHmac
{
	public:
		// Constructor
		CCryptoHmac(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICryptoHmac
		void    GetHashOid(CByteArray &oid);
		UINT    GetHashSize(void);
		PCBYTE  GetHashData(void);
		BOOL    GetHashData(CByteArray &Data);
		CString GetHashString(UINT Code);
		void    Initialize(void);
		void    Initialize(CByteArray const &Pass);
		void    Initialize(CString const &Pass);
		void    Update(CByteArray const &Data);
		void    Update(CString const &Data);

	protected:
		// States
		enum
		{
			stateNew,
			stateActive,
			stateDone
			};

		// Data Members
		ULONG m_uRefs;
		UINT  m_uState;
		PBYTE m_pHash;
		UINT  m_uHash;
	};

// End of File

#endif
