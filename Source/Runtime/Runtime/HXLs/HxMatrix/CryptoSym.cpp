
#include "Intern.hpp"

#include "CryptoSym.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Symmetric Encryption Provider
//

// Constructor

CCryptoSym::CCryptoSym(void)
{
	StdSetRef();

	m_uState = stateNew;
	}

// IUnknown

HRESULT CCryptoSym::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ICryptoSym);

	StdQueryInterface(ICryptoSym);

	return E_NOINTERFACE;
	}

ULONG CCryptoSym::AddRef(void)
{
	StdAddRef();
	}

ULONG CCryptoSym::Release(void)
{
	StdRelease();
	}

// ICryptoSym

void CCryptoSym::Initialize(CByteArray const &Pass)
{
	((ICryptoSym *) this)->Initialize(Pass.GetPointer(), Pass.GetCount());
	}

void CCryptoSym::Initialize(CString const &Pass)
{
	((ICryptoSym *) this)->Initialize(PCBYTE(PCTXT(Pass)), Pass.GetLength());
	}

void CCryptoSym::Encrypt(PBYTE pData, UINT uSize)
{
	// Override with failed assert if cipher won't work in-place.

	((ICryptoSym *) this)->Encrypt(pData, pData, uSize);
	}

void CCryptoSym::Encrypt(CByteArray &Out, CByteArray const &In)
{
	Out.Empty();

	Out.SetCount(In.GetCount());

	((ICryptoSym *) this)->Encrypt(PBYTE(Out.GetPointer()), In.GetPointer(), In.GetCount());
	}

void CCryptoSym::Encrypt(CByteArray &Data)
{
	((ICryptoSym *) this)->Encrypt(PBYTE(Data.GetPointer()), Data.GetPointer(), Data.GetCount());
	}

void CCryptoSym::Decrypt(PBYTE pData, UINT uSize)
{
	// Override with failed assert if cipher won't work in-place.

	((ICryptoSym *) this)->Decrypt(pData, pData, uSize);
	}

void CCryptoSym::Decrypt(CByteArray &Out, CByteArray const &In)
{
	Out.Empty();

	Out.SetCount(In.GetCount());

	((ICryptoSym *) this)->Decrypt(PBYTE(Out.GetPointer()), In.GetPointer(), In.GetCount());
	}

void CCryptoSym::Decrypt(CByteArray &Data)
{
	((ICryptoSym *) this)->Decrypt(PBYTE(Data.GetPointer()), Data.GetPointer(), Data.GetCount());
	}

// Implementation

void CCryptoSym::MakeBytes(PBYTE pOut, UINT uOut, PCBYTE pIn, UINT uIn)
{
	if( uIn <= uOut ) {

		for( UINT n = 0; n < uOut; n++ ) {

			pOut[n] = pIn[n % uIn];
			}
		}
	else {
		for( UINT n = 0; n < uOut; n++ ) {

			BYTE b = 0;

			for( UINT m = n; m < uIn; m++ ) {

				b ^= pIn[m];
				}

			pOut[n] = b;
			}
		}
	}

// End of File
