
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHashNull_HPP

#define INCLUDE_CryptoHashNull_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoHash.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Null Cryptographic Hash
//

class CCryptoHashNull : public CCryptoHash
{
	public:
		// Constructor
		CCryptoHashNull(void);

		// ICryptoHash
		CString GetName(void);
		void    GetHashOid(CByteArray &oid);
		void    Initialize(void);
		void    Update(PCBYTE pData, UINT uData);
		void    Finalize(void);

	protected:
		// Data Members
		BYTE m_bHash[32];
	};

// End of File

#endif
