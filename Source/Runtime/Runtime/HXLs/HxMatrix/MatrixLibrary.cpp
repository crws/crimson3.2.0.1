
#include "Intern.hpp"

#include "MatrixLibrary.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Matrix Library User
//

// Static Data

UINT CMatrixLibrary::m_uLibrary = 0;

// Constructor

CMatrixLibrary::CMatrixLibrary(void)
{
	InitLibrary();
	}

// Destructor

CMatrixLibrary::~CMatrixLibrary(void)
{
	KillLibrary();
	}

// Implementation

BOOL CMatrixLibrary::InitLibrary(void)
{
	if( !m_uLibrary++ ) {

		if( matrixSslOpen() >= 0 ) {

			return TRUE;
			}

		AfxTrace("matrix: failed to initialize library\n");
		}

	return FALSE;
	}

BOOL CMatrixLibrary::KillLibrary(void)
{
	if( !--m_uLibrary ) {

		matrixSslClose();

		return TRUE;
		}

	return FALSE;
	}

// End of File
