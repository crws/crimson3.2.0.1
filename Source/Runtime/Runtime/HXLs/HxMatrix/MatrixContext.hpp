
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MatrixContext_HPP

#define INCLUDE_MatrixContext_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMatrixSsl;

//////////////////////////////////////////////////////////////////////////
//
// Matrix Context
//

class CMatrixContext
{
	public:
		// Constructor
		CMatrixContext(CMatrixSsl *pSsl);

		// Destructor
		~CMatrixContext(void);

	protected:
		// Data Members
		ULONG        m_uRefs;
		CMatrixSsl * m_pSsl;
		char	     m_sCert[20];
		char	     m_sPriv[20];
		char	     m_sRoot[20];
		char	     m_sPass[64];
		sslKeys_t  * m_pKeys;

		// Implementation
		BOOL LoadTrustedRoots(PCBYTE pRoot, UINT uRoot);
		BOOL LoadIdentCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass);
		BOOL MakeKeys(BOOL fRoot);
		BOOL KillKeys(void);
		void MakeFile(char *pName, PCBYTE pData, UINT uSize);
		BOOL IsKeyEc(void);
	};

// End of File

#endif
