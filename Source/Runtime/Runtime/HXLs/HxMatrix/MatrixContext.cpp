
#include "Intern.hpp"

#include "MatrixContext.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MatrixObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix Context
//

// Constructor

CMatrixContext::CMatrixContext(CMatrixSsl *pSsl)
{
	StdSetRef();

	m_pSsl  = pSsl;

	m_pKeys = NULL;

	m_sCert[0] = 0;

	m_sPriv[0] = 0;

	m_sRoot[0] = 0;

	m_sPass[0] = 0;
}

// Destructor

CMatrixContext::~CMatrixContext(void)
{
	KillKeys();
}

// Implementation

BOOL CMatrixContext::LoadTrustedRoots(PCBYTE pRoot, UINT uRoot)
{
	if( !m_pKeys ) {

		MakeFile(m_sRoot, pRoot, uRoot);

		return TRUE;
	}

	AfxAssert(FALSE);

	return FALSE;
}

BOOL CMatrixContext::LoadIdentCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass)
{
	if( !m_pKeys ) {

		MakeFile(m_sCert, pCert, uCert);

		MakeFile(m_sPriv, pPriv, uPriv);

		strcpy(m_sPass, pPass ? pPass : "");

		return TRUE;
	}

	AfxAssert(FALSE);

	return FALSE;
}

BOOL CMatrixContext::MakeKeys(BOOL fRoot)
{
	int n;

	if( !m_pKeys ) {

		m_pKeys = m_pSsl->CreateKeys();

		if( fRoot && !m_sRoot[0] ) {

			CByteArray const &Root = m_pSsl->GetTrustedRoots();

			MakeFile(m_sRoot, Root.data(), Root.size());
		}

		if( IsKeyEc() ) {

			if( (n = matrixSslLoadEcKeys(m_pKeys,
						     m_sCert[0] ? m_sCert : NULL,
						     m_sCert[0] ? m_sPriv : NULL,
						     m_sCert[0] ? m_sPass : NULL,
						     m_sRoot[0] ? m_sRoot : NULL)) >= 0 ) {

				return TRUE;
			}
		}
		else {
			if( (n = matrixSslLoadRsaKeys(m_pKeys,
						      m_sCert[0] ? m_sCert : NULL,
						      m_sCert[0] ? m_sPriv : NULL,
						      m_sCert[0] ? m_sPass : NULL,
						      m_sRoot[0] ? m_sRoot : NULL)) >= 0 ) {

				return TRUE;
			}
		}

		AfxTouch(n);

		return FALSE;
	}

	return TRUE;
}

BOOL CMatrixContext::KillKeys(void)
{
	if( m_pKeys ) {

		matrixSslDeleteKeys(m_pKeys);

		m_pKeys = NULL;

		return TRUE;
	}

	return FALSE;
}

void CMatrixContext::MakeFile(char *pName, PCBYTE pData, UINT uSize)
{
	if( uSize ) {

		// Note the sizing here! We allocate one more byte and put
		// a NUL in there to terminate the data -- but we do not
		// include that NUL in the size of the file. It is just
		// there to stop Matrix wildly running beyond the end.

		PBYTE pCopy = PBYTE(psMalloc(NULL, uSize + 1));

		memcpy(pCopy, pData, uSize);

		pCopy[uSize] = 0;

		SPrintf(pName, "%X,%X", pCopy, uSize);
	}
}

BOOL CMatrixContext::IsKeyEc(void)
{
	if( m_sPriv[0] ) {

		PSTR  s = NULL;

		PCSTR p = PCSTR(strtoul(m_sPriv, &s, 16));

		if( s && *s == ',' ) {

			PCSTR e = strchr(p, '\n');

			if( CString(p, e-p).Find(" EC ") < NOTHING ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

// End of File
