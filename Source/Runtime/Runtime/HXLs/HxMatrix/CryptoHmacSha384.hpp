
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHmacSha384_HPP

#define INCLUDE_CryptoHmacSha384_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoHmac.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SHA384 Cryptographic HMAC
//

class CCryptoHmacSha384 : public CCryptoHmac
{
	public:
		// Constructor
		CCryptoHmacSha384(void);

		// ICryptoHmac
		CString GetName(void);
		void    Initialize(PCBYTE pPass, UINT uPass);
		void    Update(PCBYTE pData, UINT uData);
		void    Finalize(void);

	protected:
		// Data Members
		BYTE           m_bHash[SHA384_HASH_SIZE];
		psHmacSha384_t m_Ctx;
	};

// End of File

#endif
