
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MatrixLibrary_HPP

#define INCLUDE_MatrixLibrary_HPP

//////////////////////////////////////////////////////////////////////////
//
// Matrix Library User
//

class CMatrixLibrary
{
	public:
		// Constructor
		CMatrixLibrary(void);

		// Destructor
		~CMatrixLibrary(void);

	protected:
		// Static Data
		static UINT m_uLibrary;

		// Implementation
		BOOL InitLibrary(void);
		BOOL KillLibrary(void);
	};

// End of File

#endif
