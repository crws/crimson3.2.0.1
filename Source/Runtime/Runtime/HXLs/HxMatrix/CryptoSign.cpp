
#include "Intern.hpp"

#include "CryptoSign.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cryptographic Signer
//

// Constructor

CCryptoSign::CCryptoSign(void)
{
	StdSetRef();

	m_uState = stateNew;
	
	m_pHash  = NULL;

	m_uSig   = 0;

	m_pSig   = NULL;
	}

// Destructor

CCryptoSign::~CCryptoSign(void)
{
	AfxRelease(m_pHash);

	delete [] m_pSig;
	}

// IUnknown

HRESULT CCryptoSign::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ICryptoSign);

	StdQueryInterface(ICryptoSign);

	return E_NOINTERFACE;
	}

ULONG CCryptoSign::AddRef(void)
{
	StdAddRef();
	}

ULONG CCryptoSign::Release(void)
{
	StdRelease();
	}

// ICryptoSign

UINT CCryptoSign::GetSigSize(void)
{
	return m_uSig;
	}

PCBYTE CCryptoSign::GetSigData(void)
{
	AfxAssert(m_uState == stateDone);

	return m_pSig;
	}

BOOL CCryptoSign::GetSigData(CByteArray &Data)
{
	AfxAssert(m_uState == stateDone);

	Data.Empty();

	Data.Append(m_pSig, m_uSig);

	return TRUE;
	}

CString CCryptoSign::GetSigString(UINT Code)
{
	AfxAssert(m_uState == stateDone);

	return Encode(Code, m_pSig, m_uSig);
	}

BOOL CCryptoSign::Initialize(PCBYTE pKey, UINT uKey, CString const &Pass)
{
	AfxAssert(FALSE);

	return FALSE;
	}

BOOL CCryptoSign::Initialize(CByteArray const &Key, CString const &Pass)
{
	return ((ICryptoSign *) this)->Initialize(Key.GetPointer(), Key.GetCount(), Pass);
	}

void CCryptoSign::SetHash(ICryptoHash *pHash)
{
	AfxAssert(!m_pHash);

	m_pHash = pHash;
	}

BOOL CCryptoSign::SetHash(PCSTR pName)
{
	AfxAssert(!m_pHash);

	return AfxNewObject(pName, ICryptoHash, m_pHash) == S_OK;
	}

void CCryptoSign::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	AfxAssert(m_pHash);

	m_pHash->Update(pData, uData);
	}

void CCryptoSign::Update(CByteArray const &Data)
{
	AfxAssert(m_uState == stateActive);

	AfxAssert(m_pHash);

	m_pHash->Update(Data);
	}

void CCryptoSign::Update(CString const &Data)
{
	AfxAssert(m_uState == stateActive);

	AfxAssert(m_pHash);

	m_pHash->Update(Data);
	}

// End of File
