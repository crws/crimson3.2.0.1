
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoCipherRc2_HPP

#define INCLUDE_CryptoCipherRc2_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoSym.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RC2 Symmetric Cipher
//

class CCryptoCipherRc2 : public CCryptoSym
{
	public:
		// Constructor
		CCryptoCipherRc2(void);

		// ICryptoSym
		CString GetName(void);
		void    Initialize(PCBYTE pPass, UINT uPass);
		void    Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize);
		void    Decrypt(PBYTE pOut, PCBYTE pIn, UINT uSize);
		void    Finalize(void);

	protected:
		// Data Members
		psRc2Cbc_t m_Ctx;
	};

// End of File

#endif
