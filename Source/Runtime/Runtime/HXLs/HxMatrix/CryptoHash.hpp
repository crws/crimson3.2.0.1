
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHash_HPP

#define INCLUDE_CryptoHash_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoEncode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cryptographic Hash
//

class CCryptoHash : public CCryptoEncode, public ICryptoHash
{
	public:
		// Constructor
		CCryptoHash(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICryptoHash
		UINT    GetHashSize(void);
		PCBYTE  GetHashData(void);
		BOOL    GetHashData(CByteArray &Data);
		CString GetHashString(UINT Code);
		void    Update(CByteArray const &Data);
		void    Update(CString const &Data);

	protected:
		// States
		enum
		{
			stateNew,
			stateActive,
			stateDone
			};

		// Data Members
		ULONG m_uRefs;
		UINT  m_uState;
		PBYTE m_pHash;
		UINT  m_uHash;
	};

// End of File

#endif
