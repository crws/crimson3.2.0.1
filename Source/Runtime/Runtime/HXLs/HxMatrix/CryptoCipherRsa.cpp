
#include "Intern.hpp"

#include "CryptoCipherRsa.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// RSA Asymetric Cipher
//

// Instantiator

static IUnknown * Create_CryptoCipherRsa(PCTXT pName)
{
	return New CCryptoCipherRsa;
}

// Registration

global void Register_CryptoCipherRsa(void)
{
	piob->RegisterInstantiator("crypto.cipher-rsa", Create_CryptoCipherRsa);
}

// Constructor

CCryptoCipherRsa::CCryptoCipherRsa(void)
{
	m_uFlags = 0;

	psRsaInitKey(NULL, &m_Key);
}

// Destructor

CCryptoCipherRsa::~CCryptoCipherRsa(void)
{
	psRsaClearKey(&m_Key);
}

// ICryptoAsym

CString CCryptoCipherRsa::GetName(void)
{
	return "rsa";
}

BOOL CCryptoCipherRsa::Initialize(PCBYTE pKey, UINT uKey, CString const &Pass, UINT uFlags)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	if( (uFlags & rsaFromMask) == rsaFromPEM ) {

		return FALSE;
	}

	if( (uFlags & rsaFromMask) == rsaFromBlob ) {

		psRsaCopyKey(&m_Key, (psRsaKey_t const *) pKey);

		PostInitialize();

		m_uFlags = uFlags;

		return TRUE;
	}

	if( (uFlags & rsaFromMask) == rsaFromDER ) {

		if( (uFlags & asymKeyMask) == asymKeyPrivate ) {

			if( psRsaParsePkcs1PrivKey(NULL, pKey, uKey, &m_Key) == 0 ) {

				PostInitialize();

				m_uFlags = uFlags;

				return TRUE;
			}
		}

		return FALSE;
	}

	if( (uFlags & rsaFromMask) == rsaFromCert ) {

		psX509Cert_t *pc = NULL;

		if( psX509ParseCert(NULL, pKey, uKey, &pc, 0) > 0 ) {

			if( (uFlags & asymKeyMask) == asymKeyPublic ) {

				psRsaCopyKey(&m_Key, &pc->publicKey.key.rsa);

				psX509FreeCert(pc);

				PostInitialize();

				m_uFlags = uFlags;

				return TRUE;
			}
		}

		psX509FreeCert(pc);

		return FALSE;
	}

	return FALSE;
}

BOOL CCryptoCipherRsa::Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)
{
	UINT uKey = 0;

	if( (m_uFlags & asymKeyMask) == asymKeyPublic ) {

		uKey = PS_PUBKEY;
	}

	if( (m_uFlags & asymKeyMask) == asymKeyPrivate ) {

		uKey = PS_PRIVKEY;
	}

	if( uKey ) {

		UINT pad = m_uFlags & rsaPadMask;

		if( pad == rsaPadNone ) {

			psSize_t out = m_uKeySize;

			psRsaCrypt(NULL, &m_Key, pIn, uSize, pOut, &out, uKey, 0);

			return TRUE;
		}

		if( pad == rsaPadPkcs15 ) {

			if( uKey == PS_PUBKEY ) {

				if( !psRsaEncryptPub(NULL, &m_Key, pIn, uSize, pOut, m_uKeySize, 0) ) {

					return TRUE;
				}
			}
			else {
				if( !psRsaEncryptPriv(NULL, &m_Key, pIn, uSize, pOut, m_uKeySize, 0) ) {

					return TRUE;
				}
			}
		}

		if( pad == rsaPadOaep || pad == rsaPadOaep256 ) {

			int32    hash = (pad == rsaPadOaep) ? PKCS1_SHA1_ID : PKCS1_SHA256_ID;

			psSize_t out  = m_uKeySize;

			int r = psPkcs1OaepEncode(NULL,
						  pIn,
						  uSize,
						  NULL,
						  0,
						  NULL,
						  0,
						  m_uKeyBits,
						  hash,
						  pOut,
						  &out
			);

			if( !r && out == m_uKeySize ) {

				int r = psRsaCrypt(NULL,
						   &m_Key,
						   pOut,
						   out,
						   pOut,
						   &out,
						   uKey,
						   NULL
				);

				if( !r && out == m_uKeySize ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CCryptoCipherRsa::Decrypt(CByteArray &Out, PCBYTE pIn)
{
	UINT uKey = 0;

	if( (m_uFlags & asymKeyMask) == asymKeyPublic ) {

		uKey = PS_PUBKEY;
	}

	if( (m_uFlags & asymKeyMask) == asymKeyPrivate ) {

		uKey = PS_PRIVKEY;
	}

	if( uKey ) {

		CAutoArray<BYTE> Work(m_uKeySize);

		psSize_t         out = m_uKeySize;

		if( !psRsaCrypt(NULL, &m_Key, pIn, m_uKeySize, Work, &out, uKey, NULL) ) {

			if( out == m_uKeySize ) {

				UINT pad = m_uFlags & rsaPadMask;

				if( pad == rsaPadNone ) {

					Out.Empty();

					Out.Append(Work, out);

					return TRUE;
				}

				if( pad == rsaPadPkcs15 ) {

					if( Work[0] == 0x00 && Work[1] == uKey ) {

						for( UINT p = 2; p < out; p++ ) {

							if( Work[p] == 0x00 ) {

								Out.Empty();

								Out.Append(Work + p + 1, out - p - 1);

								return TRUE;
							}
						}
					}
				}

				if( pad == rsaPadOaep || pad == rsaPadOaep256 ) {

					int32 hash = (pad == rsaPadOaep) ? PKCS1_SHA1_ID : PKCS1_SHA256_ID;

					Out.Empty();

					Out.SetCount(out);

					int r = psPkcs1OaepDecode(NULL,
								  Work,
								  out,
								  NULL,
								  0,
								  m_uKeyBits,
								  hash,
								  Out.data(),
								  &out
					);

					if( !r ) {

						Out.SetCount(out);

						return TRUE;
					}
				}
			}
		}
	}

	return FALSE;
}

void CCryptoCipherRsa::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	m_uState = stateDone;
}

// Implementation

void CCryptoCipherRsa::MakeFile(char *pName, PCBYTE pData, UINT uSize)
{
	// Note the sizing here! We allocate one more byte and put
	// a NUL in there to terminate the data -- but we do not
	// include that NUL in the size of the file. It is just
	// there to stop Matrix wildly running beyond the end.

	PBYTE pCopy = PBYTE(psMalloc(NULL, uSize + 1));

	memcpy(pCopy, pData, uSize);

	pCopy[uSize] = 0;

	SPrintf(pName, "%X,%X", pCopy, uSize);
}

void CCryptoCipherRsa::PostInitialize(void)
{
	m_uKeySize = 1 * m_Key.size;

	m_uKeyBits = 8 * m_Key.size;

	m_uState   = stateActive;
}

// End of File
