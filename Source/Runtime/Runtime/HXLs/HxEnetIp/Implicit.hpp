
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Implicit_HPP

#define INCLUDE_Implicit_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

#include "Consumer.hpp"

#include "Producer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Implicit
//

class CImplicit : public IImplicit
{
	public:
		// Constructor
		CImplicit(void);
		
		// Destructor
		~CImplicit(void);

		// Binding
		void Bind(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IImplicit
		BOOL METHOD GetFirstConsumer(IConsumer * &pConsumer);
		BOOL METHOD GetNextConsumer (IConsumer * &pConsumer);
		BOOL METHOD GetFirstProducer(IProducer * &pProducer);
		BOOL METHOD GetNextProducer (IProducer * &pProducer);
		UINT METHOD Send(DWORD dwInst, PBYTE pData, UINT uSize);
		UINT METHOD Recv(DWORD dwInst, PBYTE pData, UINT uSize);

		// Entry Points
		void OnEvent(INT32 nEvent, INT32 nParam);

	protected:
		// Data
		ULONG	  m_uRefs;
		CProducer m_Producer[ MAX_CONNECTIONS ];
		CConsumer m_Consumer[ MAX_CONNECTIONS ];

		// Event Handlers
		void OnNewConnection(INT32 nInstance);
		void OnConnectionClosed(INT32 nInstance);
		void OnConnectionData(INT32 nInstance);
		void OnExplicitResp(INT32 ReqId, BOOL fSuccess);

		// Connections
		void ConnectBind(void);
		BOOL ConnectFindFree(CConsumer * &pConsumer);
		BOOL ConnectFindFree(CProducer * &pProducer);
		void ConnectFree(CConsumer &Consumer);
		void ConnectFree(CProducer &Producer);

		// Debug
		void Dump(void);
	};

// End of File

#endif
