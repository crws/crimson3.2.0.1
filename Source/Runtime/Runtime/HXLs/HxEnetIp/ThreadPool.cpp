
#include "Intern.hpp"

#include "ThreadPool.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Thread Pool
//

// Constructor

CThreadPool::CThreadPool(void)
{
	m_uCount  = 0;

	m_pLimit  = Create_Semaphore(0);

	m_pMutex  = Create_Mutex();

	m_pWorker = NULL;
	}

// Destructor

CThreadPool::~CThreadPool(void)
{
	Close();

	m_pLimit->Release();

	m_pMutex->Release();
	}

// Management

BOOL CThreadPool::Open(UINT uPriority, UINT uCount)
{
	if( !m_pWorker ) {

		m_uCount   = uCount;

		m_pWorker = new CWorker [ m_uCount ];

		memset(m_pWorker, 0, sizeof(CWorker) * m_uCount);
	
		for( UINT i = 0; i < m_uCount; i ++ ) {

			CWorker &Worker = m_pWorker[ i ];

			Worker.m_uID     = i;
			
			Worker.m_pRun    = Create_AutoEvent();

			Worker.m_pThread = ::CreateThread(TaskThreadPool, uPriority + i, this, i);

			m_pLimit->Signal(1);
			}
		
		return TRUE;
		}

	return FALSE;
	}

void CThreadPool::Close(void)
{
	if( m_pWorker ) {
	
		ClaimAll();

		for( UINT i = 0; i < m_uCount; i ++ ) {

			CWorker &Worker = m_pWorker[ i ];

			DestroyThread(Worker.m_pThread);

			Worker.m_pRun->Release();
			}

		delete [] m_pWorker;

		m_pWorker = NULL;
		}
	}

BOOL CThreadPool::CreateThread(PTHREAD pFunc, PVOID pData)
{
	m_pLimit->Wait(FOREVER);

	for( UINT i = 0; i < m_uCount; i ++ ) {

		CWorker &Worker = m_pWorker[i];

		m_pMutex->Wait(FOREVER);

		if( Worker.m_pfFunc == NULL ) {

			Worker.m_pfFunc = pFunc;

			Worker.m_pvData = pData;

			m_pMutex->Free();

			Worker.m_pRun->Set();
			
			return TRUE;
			}

		m_pMutex->Free();
		}
	
	return FALSE;
	}

void CThreadPool::ClaimAll(void)
{
	for( UINT i = 0; i < m_uCount; i ++ ) {
	
		m_pLimit->Wait(FOREVER);
		}
	}

// Entry Point

void CThreadPool::TaskEntry(UINT uThread)
{
	CWorker &Worker = m_pWorker[uThread];

	for(;;) {
	
		Worker.m_pRun->Wait(FOREVER);

		(*Worker.m_pfFunc)(Worker.m_pvData);

		Worker.m_pfFunc = NULL;

		m_pLimit->Signal(1);
		}
	}

// Entry Point

int CThreadPool::TaskThreadPool(IThread *pThread, void *pParam, UINT uParam)
{
	pThread->SetName(CPrintf("EtherNetIP%2.2d", uParam));

	((CThreadPool *) pParam)->TaskEntry(uParam);

	return 0;
	}

// End of File
