/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** OUTPOINT.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Output Point object 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************/

#ifndef OUTPOINT_H
#define OUTPOINT_H

#define OUTPOINT_CLASS	          0x09				/* Output Point class identifier */
#define OUTPOINT_CLASS_REVISION      1				/* Output Point class revision */
#define OUTPOINT_MAX_INSTANCES       8				/* Output Point maximum number of instances */
#define OUTPOINT_NUM_INSTANCES       8				/* Output Point number of instances implemented */

#define OUTPOINT_MAX_IMP_CLASS_ATTR		 7			/* Output Point maximum supported class attribute ID */
#define OUTPOINT_MAX_IMP_INST_ATTR		 8			/* Output Point maximum supported instance attribute ID */


/* Class and instance attribute numbers */
#define OUTPOINT_CLASS_ATTR_REVISION              1
#define OUTPOINT_CLASS_ATTR_MAX_INSTANCE          2
#define OUTPOINT_CLASS_ATTR_NUM_INSTANCES         3
#define OUTPOINT_CLASS_ATTR_MAX_CLASS_ATTR        6
#define OUTPOINT_CLASS_ATTR_MAX_INST_ATTR         7

#define OUTPOINT_INST_VALUE           0x03			/* Value */
#define OUTPOINT_INST_FAULT_ACTION    0x05			/* Fault Action */
#define OUTPOINT_INST_FAULT_VALUE     0x06			/* Fault Value */
#define OUTPOINT_INST_IDLE_ACTION     0x07			/* Idle Action */
#define OUTPOINT_INST_IDLE_VALUE      0x08			/* Idle Value */


/* Class attribute structure */
typedef struct tagOUTPOINT_CLASS_ATTR
{
   UINT16  iRevision;
   UINT16  iMaxInstance;
   UINT16  iMaxClassAttr;
   UINT16  iMaxInstanceAttr;
}
OUTPOINT_CLASS_ATTR;

#define OUTPOINT_CLASS_ATTR_SIZE	8


/* Instance attribute structure */
typedef struct tagOUTPOINT_DATA
{
   UINT8   bValue;
   UINT8   bFaultAction;
   UINT8   bFaultValue;
   UINT8   bIdleAction;
   UINT8   bIdleValue;
} 
OUTPOINT_DATA;

#define OUTPOINT_DATA_SIZE		5

extern OUTPOINT_DATA	outpointData[OUTPOINT_NUM_INSTANCES];


extern void outpointInit();
extern void outpointParseClassInstanceRequest( INT32 nRequest );
extern void outpointParseClassRequest( INT32 nRequest );
extern void outpointSendClassAttrAll( INT32 nRequest );
extern void outpointSendClassAttrSingle( INT32 nRequest );
extern void outpointParseInstanceRequest( INT32 nRequest );
extern void outpointSendInstanceAttrSingle( INT32 nRequest );
extern void outpointSetInstanceAttrSingle( INT32 nRequest );

#endif /* #ifndef OUTPOINT_H */

