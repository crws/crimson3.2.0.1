/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** OUTPOINT.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Output Point object implementation 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

OUTPOINT_DATA	outpointData[OUTPOINT_NUM_INSTANCES];


/*---------------------------------------------------------------------------
** outpointInit( )
**
** Initialize Output Point object
**---------------------------------------------------------------------------
*/

void outpointInit()
{		
	INT32 i;

	for( i = 0; i < OUTPOINT_NUM_INSTANCES; i++ )
	{
		outpointData[i].bValue = 0;
		outpointData[i].bFaultAction = 0;
		outpointData[i].bFaultValue = 0;
		outpointData[i].bIdleAction = 0;
		outpointData[i].bIdleValue = 0;
	}	   
}


/*---------------------------------------------------------------------------
** outpointParseClassInstanceRequest( )
**
** Determine if it's request for the Class or for the particular instance 
**---------------------------------------------------------------------------
*/

void outpointParseClassInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 0 )
		outpointParseClassRequest( nRequest );
	else
		outpointParseInstanceRequest( nRequest );
}

/*---------------------------------------------------------------------------
** outpointParseClassRequest( )
**
** Respond to the Output Point class request
**---------------------------------------------------------------------------
*/

void outpointParseClassRequest( INT32 nRequest )
{
	switch( gRequests[nRequest].bService )
	{
		case SVC_GET_ATTR_ALL:
			outpointSendClassAttrAll( nRequest );
			break;	
		case SVC_GET_ATTR_SINGLE:
			outpointSendClassAttrSingle( nRequest );
			break;			
		default:		
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
			break;
	}
}

/*---------------------------------------------------------------------------
** outpointSendClassAttrAll( )
**
** GetAttributeAll service for Output Point class is received 
**---------------------------------------------------------------------------
*/

void outpointSendClassAttrAll( INT32 nRequest )
{
	OUTPOINT_CLASS_ATTR attr;
	
	attr.iRevision = ENCAP_TO_HS(OUTPOINT_CLASS_REVISION);
	attr.iMaxInstance = ENCAP_TO_HS(OUTPOINT_MAX_INSTANCES);
	attr.iMaxClassAttr = ENCAP_TO_HS(OUTPOINT_MAX_IMP_CLASS_ATTR);
	attr.iMaxInstanceAttr = ENCAP_TO_HS(OUTPOINT_MAX_IMP_INST_ATTR);
	
	utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&attr, OUTPOINT_CLASS_ATTR_SIZE );			
}

/*---------------------------------------------------------------------------
** outpointSendClassAttrSingle( )
**
** GetAttributeSingle service for Output Point class is received 
**---------------------------------------------------------------------------
*/

void outpointSendClassAttrSingle( INT32 nRequest )
{	
	UINT16 iVal;
	
	switch( gRequests[nRequest].iAttribute )
	{		
		case OUTPOINT_CLASS_ATTR_REVISION:
			iVal = ENCAP_TO_HS(OUTPOINT_CLASS_REVISION);			
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;			
		case OUTPOINT_CLASS_ATTR_MAX_INSTANCE:
			iVal = ENCAP_TO_HS(OUTPOINT_MAX_INSTANCES);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;		
		case OUTPOINT_CLASS_ATTR_NUM_INSTANCES:
			iVal = ENCAP_TO_HS(OUTPOINT_NUM_INSTANCES);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;		
		case OUTPOINT_CLASS_ATTR_MAX_CLASS_ATTR:
			iVal = ENCAP_TO_HS(OUTPOINT_MAX_IMP_CLASS_ATTR);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case OUTPOINT_CLASS_ATTR_MAX_INST_ATTR:
			iVal = ENCAP_TO_HS(OUTPOINT_MAX_IMP_INST_ATTR);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}			
}


/*---------------------------------------------------------------------------
** outpointParseInstanceRequest( )
**
** Respond to the Output Point instance request
**---------------------------------------------------------------------------
*/

void outpointParseInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance >= 1 && gRequests[nRequest].iInstance <= OUTPOINT_NUM_INSTANCES )
	{
		switch( gRequests[nRequest].bService )
		{
			case SVC_GET_ATTR_SINGLE:
				outpointSendInstanceAttrSingle( nRequest );
				break;
			case SVC_SET_ATTR_SINGLE:
				outpointSetInstanceAttrSingle( nRequest );
				break;
			case SVC_RESET:
				outpointInit();
				break;
			default:
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
				break;
		}
	}
	else
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;
}



/*--------------------------------------------------------------------------------
** outpointSendInstanceAttrSingle( )
**
** GetAttributeSingle request has been received for the Output Point object instance
**--------------------------------------------------------------------------------
*/

void outpointSendInstanceAttrSingle( INT32 nRequest )
{	
	UINT8 bVal;	

	switch( gRequests[nRequest].iAttribute )
	{
		case OUTPOINT_INST_VALUE:
			bVal = outpointData[gRequests[nRequest].iInstance-1].bValue;			
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, &bVal, sizeof(UINT8) );		
			break;

		case OUTPOINT_INST_FAULT_ACTION:
			bVal = outpointData[gRequests[nRequest].iInstance-1].bFaultAction;
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, &bVal, sizeof(UINT8) );		
			break;

		case OUTPOINT_INST_FAULT_VALUE:
			bVal = outpointData[gRequests[nRequest].iInstance-1].bFaultValue;
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, &bVal, sizeof(UINT8) );		
			break;
 
		case OUTPOINT_INST_IDLE_ACTION:
			bVal = outpointData[gRequests[nRequest].iInstance-1].bIdleAction;
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, &bVal, sizeof(UINT8) );		
			break;

		case OUTPOINT_INST_IDLE_VALUE:
			bVal = outpointData[gRequests[nRequest].iInstance-1].bIdleValue;
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, &bVal, sizeof(UINT8) );		
			break;

		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}		
}

/*--------------------------------------------------------------------------------
** outpointSetInstanceAttrSingle( )
**
** SetAttributeSingle request has been received for the Output Point object instance
**--------------------------------------------------------------------------------
*/

void outpointSetInstanceAttrSingle( INT32 nRequest )
{	
	UINT8* pData = MEM_PTR(gRequests[nRequest].iDataOffset);

	switch( gRequests[nRequest].iAttribute )
	{		
		case OUTPOINT_INST_VALUE:
			outpointData[gRequests[nRequest].iInstance-1].bValue = *pData;			
			break;

		case OUTPOINT_INST_FAULT_ACTION:
			outpointData[gRequests[nRequest].iInstance-1].bFaultAction = *pData;			
			break;

		case OUTPOINT_INST_FAULT_VALUE:
			outpointData[gRequests[nRequest].iInstance-1].bFaultValue = *pData;			
			break;

		case OUTPOINT_INST_IDLE_ACTION:
			outpointData[gRequests[nRequest].iInstance-1].bIdleAction = *pData;			
			break;

		case OUTPOINT_INST_IDLE_VALUE:
			outpointData[gRequests[nRequest].iInstance-1].bIdleValue = *pData;			
			break;

		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}	

	utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );	
}














