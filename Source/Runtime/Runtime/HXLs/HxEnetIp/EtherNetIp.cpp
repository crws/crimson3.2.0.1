
#include "Intern.hpp"

#include "EtherNetIp.hpp"

#include "Implicit.hpp"

#include "Explicit.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Stack
//

// Instantiator

global IEtherNetIp * Create_EtherNetIp(void)
{
	CEtherNetIp *p = New CEtherNetIp;

	return p;
	}

// Static Data

CEtherNetIp * CEtherNetIp::m_pThis = NULL; 

// Constructors

CEtherNetIp::CEtherNetIp(void)
{
	StdSetRef();

	m_pThis     = this;

	m_nOpen     = 0;

	m_nConnect  = 0;

	m_pReady    = Create_ManualEvent();

	m_pMutex    = Create_Mutex();

	InitObjects();
	}

// Destructor

CEtherNetIp::~CEtherNetIp(void)
{
	m_nOpen = min(1, m_nOpen);

	Close();
	
	FreeObjects();

	m_pReady->Release();

	m_pMutex->Release();

	m_pThis = NULL;
	}

// IUnknown

HRESULT CEtherNetIp::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IEtherNetIp);

	StdQueryInterface(IEtherNetIp);

	return E_NOINTERFACE;
	}

ULONG CEtherNetIp::AddRef(void)
{
	StdAddRef();
	}

ULONG CEtherNetIp::Release(void)
{
	StdRelease();
	}

// IEtherNetIp

WORD METHOD CEtherNetIp::GetVendorId(void)
{
	return 176;
	}

BOOL METHOD CEtherNetIp::Open(void)
{
	if( AtomicIncrement(&m_nOpen) == 1 ) {
	
		platformInit();

		clientRegisterEventCallBack(EthernetIPCallback);
		}

	return TRUE;
	}

BOOL METHOD CEtherNetIp::Close(void)
{
	if( AtomicDecrement(&m_nConnect) == 0 ) {

		m_pReady->Clear();

		clientStop();
		}

	if( AtomicDecrement(&m_nOpen) == 0 ) {

		platformStop();
		}

	return TRUE;
	}

BOOL METHOD CEtherNetIp::Connect(void)
{
	CIpAddr  IP, Mask;

	CMacAddr MAC;

	if( platformFindEthernet(IP, Mask, MAC) ) {

		if( AtomicIncrement(&m_nConnect) == 1 ) {

			clientStart();
		
			EtIPIdentityInfo Ident;

			clientGetIdentityInfo(&Ident);

			Ident.iVendor		= GetVendorId();
			Ident.iProductType      = 0x00;
			Ident.iProductCode	= 100;
			Ident.bMajorRevision	= 1;
			Ident.bMinorRevision	= 0;
		
			strcpy(Ident.productName, "G3");

			clientSetIdentityInfo(&Ident);	

			m_pReady->Set();

			return TRUE;
			}

		m_pReady->Wait(FOREVER);

		return TRUE;
		}
	
	return FALSE;
	}

IImplicit * METHOD CEtherNetIp::GetImplicit(void)
{
	m_pMutex->Wait(FOREVER);

	if( !m_pImplicit ) {

		m_pImplicit = New CImplicit;

		m_pImplicit->Bind();

		m_pMutex->Free();

		return m_pImplicit;
		}

	m_pMutex->Free();

	return NULL;
	}

IExplicit * METHOD CEtherNetIp::GetExplicit(void)
{
	m_pMutex->Wait(FOREVER);

	for( UINT i = 0; i < elements(m_pExplicit); i ++ ) {

		if( !m_pExplicit[i] ) {

			m_pExplicit[i] = New CExplicit();

			m_pExplicit[i]->Bind(i);

			m_pMutex->Free();

			return m_pExplicit[i];
			}
		}

	m_pMutex->Free();

	return NULL;
	}

void METHOD CEtherNetIp::FreeImplicit(IImplicit *p)
{
	m_pImplicit = NULL;

	p->Release();
	}

void METHOD CEtherNetIp::FreeExplicit(IExplicit *p)
{
	UINT i = ((CExplicit *) p)->GetIndex();

	if( i != NOTHING ) {

		m_pExplicit[i] = NULL;
		}

	p->Release();
	}

// Object Helpers

void CEtherNetIp::InitObjects(void)
{
	m_pImplicit = NULL;

	memset(m_pExplicit, 0, sizeof(m_pExplicit));
	}

void CEtherNetIp::FreeObjects(void)
{
	if( m_pImplicit ) {

		m_pImplicit->Release();

		m_pImplicit = NULL;
		}

	for( UINT i = 0; i < elements(m_pExplicit); i ++ ) {

		if( m_pExplicit[i] ) {

			m_pExplicit[i]->Release();

			m_pExplicit[i] = NULL;
			}
		}
	}

// Entry Point

void CEtherNetIp::OnEvent(INT32 nEvent, INT32 nParam)
{
	/*AfxTrace("CStack::OnEvent : Event %2.2d : Param : 0x%8.8x\n", nEvent, nParam);*/
	
	switch( nEvent ) {

		case NM_CONNECTION_ESTABLISHED:
		case NM_CONNECTION_CLOSED:
		case NM_CONNECTION_NEW_INPUT_DATA:
			
			OnEventI(nEvent, nParam);

			break;

		case NM_REQUEST_RESPONSE_RECEIVED:
		case NM_REQUEST_FAILED_INVALID_NETWORK_PATH:
		case NM_REQUEST_TIMED_OUT:

			OnEventE(nEvent, nParam);

			break;

		case NM_OUT_OF_MEMORY:
		case NM_UNABLE_START_THREAD:
		case NM_ERROR_USING_WINSOCK:
		case NM_SESSION_COUNT_LIMIT_REACHED:
		case NM_CONNECTION_COUNT_LIMIT_REACHED:
		case NM_PENDING_REQUESTS_LIMIT_REACHED:
			
			/*AfxTrace("EnthernetIP Limits Reached : Event %2.2d : Param : 0x%8.8x\n", nEvent, nParam);*/
			
			OnEventI(nEvent, nParam);

			OnEventE(nEvent, nParam);
			
			break;
		}
	}

void CEtherNetIp::OnEventI(INT32 nEvent, INT32 nParam)
{
	if( m_pImplicit ) {

		m_pImplicit->OnEvent(nEvent, nParam);
		}
	}

void CEtherNetIp::OnEventE(INT32 nEvent, INT32 nParam)
{
	for( UINT i = 0; i < elements(m_pExplicit); i ++ ) {
	
		if( m_pExplicit[i] ) {
		
			m_pExplicit[i]->OnEvent(nEvent, nParam);
			}
		}
	}

// Friends

global void EthernetIPCallback(INT32 nEvent, INT32 nParam)
{
	CEtherNetIp::m_pThis->OnEvent(nEvent, nParam);
	}

// End of File
