/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** ROUTER.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Message Router object 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************/

#ifndef ROUTER_H
#define ROUTER_H

#define ROUTER_CLASS						2	/* Message Router class Id */
#define ROUTER_CLASS_REVISION				1	/* Message Router class revision */

#if defined( ET_IP_SCANNER )
#define ROUTER_OBJECT_COUNT					9	/* Number of classes supported by the router */
#else
#define ROUTER_OBJECT_COUNT					8	/* Number of classes supported by the router */
#endif


/* Message Router attributes */
#define	ROUTER_ATTR_CLASS_LIST				1	
#define	ROUTER_ATTR_MAX_CONNECTIONS			2
#define	ROUTER_ATTR_OPEN_CONNECTIONS		3
#define	ROUTER_ATTR_OPEN_CONNECTION_IDS		4

extern UINT16  routerObjects[ROUTER_OBJECT_COUNT];	/* Array used to service ROUTER_ATTR_CLASS_LIST attribute */ 


#define CPF_TAG_ADR_NULL      0        /* Null Address Tag */
#define CPF_TAG_ADR_LOCAL     0x81U    /* Local Address Tag */
#define CPF_TAG_ADR_OFFLINK   0x82U    /* OffLink Address Tag */
#define CPF_TAG_PKT_PCCC      0x91U    /* PCCC Packet Tag */
#define CPF_TAG_ADR_CONNECTED 0xa1U    /* Connected (CIP) Address Tag */
#define CPF_TAG_ADR_SEQUENCED 0x8002U  /* Connected (CIP) Sequenced Tag */
#define CPF_TAG_PKT_UCMM      0xb2U    /* Unconnected Messaging Packet Tag */
#define CPF_TAG_PKT_CONNECTED 0xb1U    /* Connected Messaging Packet Tag */
#define CPF_TAG_SOCKADDR_OT   0x8000U  /* Socket address, originator->target */
#define CPF_TAG_SOCKADDR_TO   0x8001U  /* Socket address, target->originator */

#define CPF_INVALID_TAG_TYPE  (-1)


/* Definitions for Common Packet Format Encoding.  The data portion of a
 * SendPacket command has a common general-purpose format used for
 * encapsulation.  The format consists of a set of tagged fields
 * preceeded by a two byte count field.  The count field indicates
 * the number of tags in the packet.
 */

/* All tags have a common format header. */

typedef struct cpf_tag 
{
    UINT16 iTag_type;      /* Type of tag */
    UINT16 iTag_length;    /* Length of field (excludes tag) */
} 
CPF_TAG;

#define CPF_TAG_SIZE    4     /* Gets around alignment */


/* Define format for the currently defined and supported tag data.  These definitions
 * DO NOT include the common header (type, length) which precedes each tag.
 */

/* NULL ADDRESS TAG consists of the header. */

/* CONNECTED ADDRESS TAG */
typedef struct cpf_adr_connected 
{
    UINT32 lCid;           /* Connection identifier */
} 
CPF_ADR_CONNECTED;

typedef struct cpf_adr_sequenced 
{
    UINT32 lCid;           /* Connection identifier */
    UINT32 lSeq;           /* Connection sequence numbered */
} 
CPF_ADR_SEQUENCED;

/* UCMM PACKET TAG consists of the header */

/* CONNECTED PACKET TAG consists of the header */

/* Define Generic Address Tag */
typedef struct cpf_adr_tag 
{
    CPF_TAG sTag;                       /* Common Tag */
    union 
    {
       UINT8 g[1];                      /* Generic data */
       CPF_ADR_CONNECTED sC;            /* Connected address */
       CPF_ADR_SEQUENCED sS;            /* Sequenced address */
    } data;
    UINT8 pad[1];
} 
CPF_ADR_TAG;

#define CPF_ADR_TAG_SIZE sizeof(CPF_ADR_TAG)

/* Define Generic Packet Tag */
typedef struct cpf_pkt_tag 
{
    CPF_TAG sTag;                       /* Common Tag */
    UINT8*  pPacketPtr;
	BOOL    bConnected;
	INT32   nConnection;
	UINT16  iConnSequence;
} 
CPF_PKT_TAG;

#define CPF_PKT_TAG_SIZE sizeof(CPF_PKT_TAG)

/* Define Generic Socket Address Tag */
typedef struct cpf_sockaddr_tag 
{
    CPF_TAG sTag;                       /* Common Tag */
    struct sockaddr_in sAddr;    
} 
CPF_SOCKADDR_TAG;

#define CPF_SOCKADDR_TAG_SIZE (CPF_TAG_SIZE + sizeof(struct sockaddr_in))

typedef struct tagPDU_HDR
{
   UINT8 bService;
   UINT8 bSize;   
} 
PDU_HDR;

#define PDU_HDR_SIZE	  2

#define REPLY_BIT_MASK            0x80		/* Is set to indicate a response as opposed to a request */

/* Message Router service codes */
#define SVC_GET_ATTR_ALL          0x01		
#define SVC_SET_ATTR_ALL          0x02
#define SVC_GET_ATTR_LIST         0x03
#define SVC_SET_ATTR_LIST         0x04
#define SVC_RESET                 0x05
#define SVC_START                 0x06
#define SVC_STOP                  0x07
#define SVC_CREATE                0x08
#define SVC_DELETE                0x09
#define SVC_APPLY_ATTRIBUTES      0x0D
#define SVC_GET_ATTR_SINGLE       0x0E
#define SVC_SET_ATTR_SINGLE       0x10
#define SVC_FIND_NEXT_OBJ_INST    0x11
#define SVC_RESTORE               0x15
#define SVC_SAVE                  0x16
#define SVC_NO_OP                 0x17
#define SVC_GET_MEMBER            0x18
#define SVC_SET_MEMBER            0x19
#define SVC_CIP_DATA_TABLE_READ   0x4C
#define SVC_CIP_DATA_TABLE_WRITE  0x4D


#define MAX_LINK_ADDRESS	64
#define MAX_SYMBOL_SIZE		128

/* EPATH iFilled member masks. Bitwise OR iFilled with the following constants to indicate
   that the one of the other members of EPATH has been set */
#define EPATH_CLASS					0x0001
#define EPATH_INSTANCE				0x0002
#define EPATH_ATTRIBUTE				0x0004
#define EPATH_MEMBER				0x0008
#define EPATH_CONSUMING_CONN_POINT	0x0010
#define EPATH_PRODUCING_CONN_POINT	0x0020
#define EPATH_PORT					0x0040
#define EPATH_LINK_ADDRESS			0x0080
#define EPATH_DEVICE_ID				0x0100
#define EPATH_OT_INHIBIT_INTERVAL	0x0200
#define EPATH_TO_INHIBIT_INTERVAL	0x0400
#define EPATH_SYMBOL				0x0800
#define EPATH_EXT_SYMBOL			0x1000
#define EPATH_DATA					0x2000
#define EPATH_SLOT_ADDRESS			0x4000

typedef struct tagEPATH
{
	UINT16			iFilled;		/* Indicate which of the following members are set */
	UINT16			iClass;
	UINT16			iInstance;
	UINT8			bAttribute;
	UINT16			iMember;
	UINT16			iConsumingConnPoint;
	UINT16			iProducingConnPoint;
	UINT16			iPort;
	UINT8			bLinkAddress[MAX_LINK_ADDRESS];
	UINT8			bLinkAddressSize;
	UINT8			bSlot;
	EtIPDeviceID	deviceId;
	UINT8			bProductionOTInhibitInterval;
	UINT8			bProductionTOInhibitInterval;
	UINT8			bSymbol[MAX_SYMBOL_SIZE];
	UINT8			bSymbolSize;
	UINT8			bExtSymbol[MAX_SYMBOL_SIZE];
	UINT8			bExtSymbolSize;
	UINT8			bData[MAX_REQUEST_DATA_SIZE];
	UINT16			iDataSize;	
}
EPATH;

/*---------------------------------------------------------------------------
**
** IOI Segment Type Mask and Values
**
**---------------------------------------------------------------------------
*/

#define SEGMENT_TYPE_MASK     0xE0   /* IOI Segment Type Mask           */

#define PATH_SEGMENT          0x00   /* Path type segment indicator     */
#define LOGICAL_SEGMENT       0x20   /* logical type segment indicator  */
#define NETWORK_SEGMENT       0x40   /* when/how to send                */
#define SYMBOLIC_SEGMENT      0x60   /* symbolic type segment indicator */
#define DATA_SEGMENT          0x80   /* data segment indicator          */

#define LOGICAL_SEG_TYPE_MASK    0x1C

/* The following constants are bitwise ORed with the LOGICAL_SEGMENT constant to form one of the 
   Logical Segment types */
#define LOGICAL_SEG_CLASS        0x00
#define LOGICAL_SEG_INSTANCE     0x04
#define LOGICAL_SEG_MEMBER		 0x08
#define LOGICAL_SEG_CONN_POINT   0x0C
#define LOGICAL_SEG_ATTRIBUTE    0x10
#define LOGICAL_SEG_KEY          0x14

#define LOGICAL_SEG_FORMAT_MASK  0x03

#define LOGICAL_SEG_8_BIT        0x00	/* Indicates that the value is 8 bit long */
#define LOGICAL_SEG_16_BIT       0x01	/* Indicates that the value is 16 bits long */

#define EXTENDED_LINK_MASK		0x10
#define PORT_ID_MASK			0x0F
#define EXTENDED_PORT_FLAG		0x0F
#define COMMON_KEY_FORMAT		0x04

#define NET_SEGMENT_TYPE_MASK   0x1F
#define PRODUCTION_INHIBIT_TYPE 0x03

#define SYM_SEGMENT_SIZE_MASK   0x1F
#define EXTENDED_SYM_TYPE_MASK  0xE0

#define EXTENDED_SYM_TYPE_DOUBLEBYTE	0x20
#define EXTENDED_SYM_TYPE_TRIPLEBYTE	0x40
#define EXTENDED_SYM_TYPE_SPECIAL		0xC0

#define PORT_SEGMENT					0x1		/* Port segment is used to indicate the slot the target device is in */
#define SIMPLE_DATA_SEGMENT				0x80	/* Simple data segment is used to pass device specific configuration with the Forward Open */
#define EXT_SYMBOL_SEGMENT				0x91	/* Used to include simple ANSI strings in the request or response */


/* CIP Reply header */
typedef struct tagREPLY_HEADER
{
   UINT8 bService;
   UINT8 bIoiSize;         

   UINT8 bGenStatus;					/* 0 indicates successful reply, any other value indicates a failure */
   UINT8 bObjStatusSize;				/* size in words of the extended error that will follow the Reply Header in case of a failure */
} 
REPLY_HEADER;

#define REPLY_HEADER_SIZE   4

#define ROUTER_ERROR_BASE	0x10000

/* Message Router Generic error codes */
#define ROUTER_ERROR_SUCCESS					0x00  /* We done good...               */
#define ROUTER_ERROR_FAILURE					0x01  /* Connection failure            */
#define ROUTER_ERROR_NO_RESOURCE				0x02  /* Resource(s) unavailable       */
#define ROUTER_ERROR_INVALID_PARAMETER_VALUE    0x03  /* Obj specific data bad         */
#define ROUTER_ERROR_INVALID_SEG_TYPE			0x04  /* Invalid segment type in path  */
#define ROUTER_ERROR_INVALID_DESTINATION		0x05  /* Invalid segment value in path */
#define ROUTER_ERROR_PARTIAL_DATA				0x06  /* Not all expected data sent    */
#define ROUTER_ERROR_CONN_LOST					0x07  /* Messaging connection lost     */
#define ROUTER_ERROR_BAD_SERVICE				0x08  /* Unimplemented service code    */
#define ROUTER_ERROR_BAD_ATTR_DATA				0x09  /* Bad attribute data value      */
#define ROUTER_ERROR_ATTR_LIST_ERROR			0x0A  /* Get/set attr list failed      */
#define ROUTER_ERROR_ALREADY_IN_REQUESTED_MODE	0x0B  /* Obj already in requested mode */
#define ROUTER_ERROR_OBJECT_STATE_CONFLICT		0x0C  /* Obj not in proper mode        */
#define ROUTER_ERROR_OBJ_ALREADY_EXISTS			0x0D  /* Object already created        */
#define ROUTER_ERROR_ATTR_NOT_SETTABLE			0x0E  /* Set of get only attr tried    */
#define ROUTER_ERROR_PERMISSION_DENIED			0x0F  /* Insufficient access permission*/
#define ROUTER_ERROR_DEV_IN_WRONG_STATE			0x10  /* Device not in proper mode     */
#define ROUTER_ERROR_REPLY_DATA_TOO_LARGE		0x11  /* Response packet too large     */
#define ROUTER_ERROR_FRAGMENT_PRIMITIVE			0x12  /* Primitive value will fragment */
#define ROUTER_ERROR_NOT_ENOUGH_DATA			0x13  /* Goldilocks complaint #1       */
#define ROUTER_ERROR_ATTR_NOT_SUPPORTED			0x14  /* Attribute is undefined        */
#define ROUTER_ERROR_TOO_MUCH_DATA				0x15  /* Goldilocks complaint #2       */
#define ROUTER_ERROR_OBJ_DOES_NOT_EXIST			0x16  /* Non-existant object specified */
#define ROUTER_ERROR_NO_FRAGMENTATION			0x17  /* Fragmentation not active      */
#define ROUTER_ERROR_DATA_NOT_SAVED				0x18  /* Attr data not previously saved*/
#define ROUTER_ERROR_DATA_WRITE_FAILURE			0x19  /* Attr data not saved this time */
#define ROUTER_ERROR_REQUEST_TOO_LARGE			0x1A  /* Routing failure on request    */
#define ROUTER_ERROR_RESPONSE_TOO_LARGE			0x1B  /* Routing failure on response   */
#define ROUTER_ERROR_MISSING_LIST_DATA			0x1C  /* Attr data not found in list   */
#define ROUTER_ERROR_INVALID_LIST_STATUS		0x1D  /* Returned list of attr w/status*/
#define ROUTER_ERROR_SERVICE_ERROR				0x1E  /* Embedded service failed       */
#define ROUTER_ERROR_VENDOR_SPECIFIC			0x1F  /* Vendor specific error		   */
#define ROUTER_ERROR_INVALID_PARAMETER			0x20  /* Invalid parameter			   */
#define ROUTER_ERROR_WRITE_ONCE_FAILURE			0x21  /* Write once previously done    */
#define ROUTER_ERROR_INVALID_REPLY				0x22  /* Invalid reply received        */
#define ROUTER_ERROR_BAD_KEY_IN_PATH			0x25  /* Electronic key in path failed */
#define ROUTER_ERROR_BAD_PATH_SIZE				0x26  /* Invalid path size             */
#define ROUTER_ERROR_UNEXPECTED_ATTR			0x27  /* Cannot set attr at this time  */
#define ROUTER_ERROR_INVALID_MEMBER				0x28  /* Member ID in list nonexistant */
#define ROUTER_ERROR_MEMBER_NOT_SETTABLE		0x29  /* Cannot set value of member    */
#define ROUTER_ERROR_STILL_PROCESSING			0xFF  /* Special marker to indicate    */
												/* we haven't finished processing */
												/* the request yet               */

#define ROUTER_ERROR_ENCAP_PROTOCOL				0x6A  /* Error in encapsulation header */	

/* Message Router Extended error codes */
#define ROUTER_EXT_ERR_DUPLICATE_FWD_OPEN				0x100
#define ROUTER_EXT_ERR_CLASS_TRIGGER_INVALID			0x103
#define ROUTER_EXT_ERR_OWNERSHIP_CONFLICT				0x106
#define ROUTER_EXT_ERR_CONNECTION_NOT_FOUND				0x107
#define ROUTER_EXT_ERR_INVALID_CONN_TYPE				0x108
#define ROUTER_EXT_ERR_INVALID_CONN_SIZE				0x109
#define ROUTER_EXT_ERR_DEVICE_NOT_CONFIGURED			0x110
#define ROUTER_EXT_ERR_RPI_NOT_SUPPORTED				0x111
#define ROUTER_EXT_ERR_CONNECTION_LIMIT_REACHED			0x113
#define ROUTER_EXT_ERR_VENDOR_PRODUCT_CODE_MISMATCH		0x114
#define ROUTER_EXT_ERR_PRODUCT_TYPE_MISMATCH			0x115
#define ROUTER_EXT_ERR_REVISION_MISMATCH				0x116
#define ROUTER_EXT_ERR_INVALID_CONN_POINT				0x117
#define ROUTER_EXT_ERR_INVALID_CONFIG_FORMAT			0x118
#define ROUTER_EXT_ERR_NO_CONTROLLING_CONNECTION		0x119
#define ROUTER_EXT_ERR_TARGET_CONN_LIMIT_REACHED		0x11A
#define ROUTER_EXT_ERR_RPI_SMALLER_THAN_INHIBIT			0x11B
#define ROUTER_EXT_ERR_CONNECTION_TIMED_OUT				0x203
#define ROUTER_EXT_ERR_UNCONNECTED_SEND_TIMED_OUT		0x204
#define ROUTER_EXT_ERR_PARAMETER_ERROR					0x205
#define ROUTER_EXT_ERR_MESSAGE_TOO_LARGE				0x206
#define ROUTER_EXT_ERR_UNCONN_ACK_WITHOUT_REPLY			0x207
#define ROUTER_EXT_ERR_NO_BUFFER_MEMORY_AVAILABLE		0x301
#define ROUTER_EXT_ERR_BANDWIDTH_NOT_AVAILABLE			0x302
#define ROUTER_EXT_ERR_TAG_FILTERS_NOT_AVAILABLE		0x303
#define ROUTER_EXT_ERR_REAL_TIME_DATA_NOT_CONFIG		0x304
#define ROUTER_EXT_ERR_PORT_NOT_AVAILABLE				0x311
#define ROUTER_EXT_ERR_LINK_ADDR_NOT_AVAILABLE			0x312
#define ROUTER_EXT_ERR_INVALID_SEGMENT_TYPE_VALUE		0x315
#define ROUTER_EXT_ERR_PATH_CONNECTION_MISMATCH			0x316
#define ROUTER_EXT_ERR_INVALID_NETWORK_SEGMENT			0x317
#define ROUTER_EXT_ERR_INVALID_LINK_ADDRESS				0x318
#define ROUTER_EXT_ERR_SECOND_RESOURCES_NOT_AVAILABLE	0x319
#define ROUTER_EXT_ERR_CONNECTION_ALREADY_ESTABLISHED	0x31A
#define ROUTER_EXT_ERR_DIRECT_CONN_ALREADY_ESTABLISHED	0x31B
#define ROUTER_EXT_ERR_MISC								0x31C
#define ROUTER_EXT_ERR_REDUNDANT_CONNECTION_MISMATCH	0x31D
#define ROUTER_EXT_ERR_NO_MORE_CONSUMER_RESOURCES		0x31E
#define ROUTER_EXT_ERR_NO_TARGET_PATH_RESOURCES			0x31F
#define ROUTER_EXT_ERR_VENDOR_SPECIFIC					0x320

/* Used to respond to GetAttributeAll for the Message Router class object */
typedef struct tagROUTER_CLASS_ATTRIBUTE
{
   UINT16  iRevision;
   UINT16  iOptionalAttributeNbr;
   UINT16  iOptionalServiceNbr;
   UINT16  iMaxClassAttr;
   UINT16  iMaxInstanceAttr;
}
ROUTER_CLASS_ATTRIBUTE;

#define ROUTER_CLASS_ATTRIBUTE_SIZE		10

extern CPF_SOCKADDR_TAG gtoTag;		/* Store Target->Originator address tag that came with the UCMM or Class3 message */
extern CPF_SOCKADDR_TAG gotTag;		/* Store Originator->Target address tag that came with the UCMM or Class3 message */

extern void routerDataTransferReceived( INT32 nSession );
extern void  routerParseObjectRequest( INT32 nSession, CPF_PKT_TAG* pPacketTag, UINT32 lContext1, UINT32 lContext2  );
extern void routerProcessObjectRequest( INT32 nRequest );
extern INT32  routerParsePDU( UINT8* pPacketPtr, EPATH* pPDU );

extern void routerParseClassInstanceRequest( INT32 nRequest );
extern void routerParseClassRequest( INT32 nRequest );
extern void routerParseInstanceRequest( INT32 nRequest );
extern void routerSendClassAttrAll( INT32 nRequest );
extern void routerSendInstanceAttrSingle( INT32 nRequest );


#endif /* #ifndef ROUTER_H */
