
#include "Intern.hpp"

#include "Explicit.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Explicit
//

// Instantiator

global IUnknown * Create_Explicit(PCSTR pName)
{
	CExplicit *p = New CExplicit;

	return p;
	}

// Constructors

CExplicit::CExplicit(void)
{
	StdSetRef();

	m_pReqFlag  = Create_ManualEvent();

	m_pRequest  = New EtIPObjectRequest;

	m_pResponse = New EtIPObjectResponse;

	m_uIndex    = NOTHING;
	
	memset(m_pRequest,  0, sizeof(EtIPObjectRequest));

	memset(m_pResponse, 0, sizeof(EtIPObjectRequest));

	m_pReqFlag->Set();
	}

// Destructor

CExplicit::~CExplicit(void)
{
	m_pReqFlag->Release();
	
	delete m_pRequest;

	delete m_pResponse;
	}

// Binding

void CExplicit::Bind(UINT uIndex)
{
	m_uIndex = uIndex;
	}

UINT CExplicit::GetIndex(void) const
{
	return m_uIndex;
	}

// IUnknown

HRESULT CExplicit::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IExplicit);

	StdQueryInterface(IExplicit);

	return E_NOINTERFACE;
	}

ULONG CExplicit::AddRef(void)
{
	StdAddRef();
	}

ULONG CExplicit::Release(void)
{
	StdRelease();
	}

// IExplicit

void CExplicit::SetTimeout(UINT uTimeout)
{
	m_pRequest->lExplicitMessageTimeout = uTimeout;
	}

BOOL CExplicit::Send(CIPADDR &Addr)
{
	return Send(Addr, NULL, 0);
	}

BOOL CExplicit::Send(CIPADDR &Addr, CBuffer *pMsg)
{
	if( Send(Addr, pMsg->GetData(), pMsg->GetSize()) ) {

		pMsg->Release();

		return TRUE;
		}

	return FALSE;
	}

BOOL CExplicit::Send(CIPADDR &Addr, PBYTE pData, UINT uSize)
{
	m_pReqFlag->Wait(FOREVER);
	
	if( uSize <= MAX_REQUEST_DATA_SIZE ) {

		m_pRequest->bService   = Addr.m_bService;
		m_pRequest->iClass     = Addr.m_wClass;
		m_pRequest->iInstance  = Addr.m_wInst;
		m_pRequest->iAttribute = Addr.m_wAttr;
		m_pRequest->iMember    = Addr.m_wMember;
		m_pRequest->iTagSize   = WORD(Addr.m_uTagSize);
		m_pRequest->iDataSize  = WORD(uSize);

		memcpy(m_pRequest->requestData, pData, uSize);

		memcpy(m_pRequest->requestTag, Addr.m_bTagData, Addr.m_uTagSize);

		CString sNet = CIpAddr(Addr.m_ip).GetAsText();

		if( Addr.m_wPort != EIP_EMPTY ) {

			if( Addr.m_wSlot == EIP_EMPTY ) {

				sNet += CPrintf(",%d", Addr.m_wPort);
				}
			else {
				sNet += CPrintf(",%d,%d", Addr.m_wPort, Addr.m_wSlot);
				}			

			if( Addr.m_pPath ) {

				sNet += CPrintf(",%s", Addr.m_pPath);
				}
			}

		m_pReqFlag->Clear();
		
		platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);

		m_ReqId = clientSendUnconnectedRequest(sNet, m_pRequest);

		platformReleaseMutex(ghClientMutex);

		if( m_ReqId >= REQUEST_INDEX_BASE ) {

			return TRUE;
			}

		m_pReqFlag->Set();
		}

	return FALSE;
	}

BOOL CExplicit::Recv(PBYTE pData, UINT &uSize)
{
	if( m_pReqFlag->Wait(FOREVER) ) {

		if( !m_pResponse->bGeneralStatus ) {

			uSize = min(uSize, m_pResponse->iDataSize);

			memcpy(pData, m_pResponse->responseData, uSize);

			return TRUE;
			}

		m_bError = m_pResponse->bGeneralStatus;
		}
	
	return FALSE;
	}

BOOL CExplicit::Recv(CBuffer * &pMsg)
{
	pMsg = NULL;

	if( m_pReqFlag->Wait(FOREVER) ) {

		if( !m_pResponse->bGeneralStatus ) {

			UINT uCount = m_pResponse->iDataSize;

			pMsg = BuffAllocate(uCount);

			if( pMsg ) {

				PBYTE p = pMsg->AddTail(uCount);

				memcpy(p, m_pResponse->responseData, uCount);

				return TRUE;
				}
			}

		m_bError = m_pResponse->bGeneralStatus;
		}
	
	return FALSE;
	}

BOOL CExplicit::Abort(IPADDR const &Addr)
{
	CString Net(IPREF(Addr).GetAsText());

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);

	extern INT32 requestKillSession(UINT8* szIPAddr);

	requestKillSession(PBYTE(PCSTR(Net)));

	platformReleaseMutex(ghClientMutex);

	return TRUE;
	}

BYTE CExplicit::GetLastError(void)
{
	return m_bError;
	}

// Entry Point

void CExplicit::OnEvent(INT32 nEvent, INT32 nParam)
{
	switch( nEvent ) {

		case NM_REQUEST_RESPONSE_RECEIVED:

			OnResponse(nParam);

			break;

		case NM_REQUEST_TIMED_OUT:

			OnTimeout(nParam);

			break;

		case NM_REQUEST_FAILED_INVALID_NETWORK_PATH:
		case NM_SESSION_COUNT_LIMIT_REACHED:

			OnFailed(nParam);

			break;
		}
	}

// Event Handlers

void CExplicit::OnResponse(INT32 ReqId)
{
	if( ReqId == m_ReqId ) {

		/*AfxTrace("[%d] Response\n", m_uIndex);*/

		if( clientGetResponse(ReqId, m_pResponse) ) {

			m_pResponse->bGeneralStatus = 1;
			}

		m_pReqFlag->Set();
		}
	}

void CExplicit::OnTimeout(INT32 ReqId)
{
	if( ReqId == m_ReqId ) {

		/*AfxTrace("[%d] Timeout\n", m_uIndex); */

		m_pResponse->bGeneralStatus = 1;
		
		m_pReqFlag->Set();
		}
	}

void CExplicit::OnFailed(INT32 ReqId)
{
	if( ReqId == m_ReqId ) {

		/*AfxTrace("[%d] Failed\n", m_uIndex);*/

		m_pResponse->bGeneralStatus = 1;
		
		m_pReqFlag->Set();
		}
	}

// End of File
