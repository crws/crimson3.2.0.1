/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** REQUEST.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Request array used for scheduling outgoing activity
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

INT32    gnRequests;				/* Number of outstanding requests */
REQUEST  gRequests[MAX_REQUESTS];	/* Request array */

INT32    gnIndex;					/* Index to be assigned to the next opened request */


/*---------------------------------------------------------------------------
** requestInit( )
**
** Initialize request array
**---------------------------------------------------------------------------
*/

void requestInit()
{
	INT32    i;
	
	gnRequests = 0;
	
	for( i = 0; i < MAX_REQUESTS; i++ )
		requestInitialize( i );	

	gnIndex = REQUEST_INDEX_BASE;
}


/*---------------------------------------------------------------------------
** requestInitialize( )
**
** Initialize a request
**---------------------------------------------------------------------------
*/

void requestInitialize( INT32 nRequest )
{		
	memset( &gRequests[nRequest], 0, sizeof(REQUEST) );	

	gRequests[nRequest].bOpenPriorityTickTime = PRIORITY_TICK_TIME;
	gRequests[nRequest].bOpenTimeoutTicks = TIMEOUT_TICKS;		
	gRequests[nRequest].iTagCount = 2;
	gRequests[nRequest].iConnectionSerialNbr = INVALID_CONNECTION_SERIAL_NBR;
}

/*---------------------------------------------------------------------------
** requestNew( )
**
** Allocate and initialize new request in the array 
**---------------------------------------------------------------------------
*/

INT32 requestNew( INT32 nType, BOOL bIncoming, BOOL bDeleteOnSend )
{
	INT32 nRequest;

	if ( gnRequests >= MAX_REQUESTS )	
		return ERROR_STATUS;			/* Request limit has been reached */

	nRequest = gnRequests;	
	requestInitialize( nRequest );
	gRequests[nRequest].nIndex = gnIndex++;
	gRequests[nRequest].lContext1 = gHostAddr[0].sin_addr.s_addr;
	gRequests[nRequest].lContext2 = gRequests[nRequest].nIndex;
	gRequests[nRequest].nType = nType;	
	gRequests[nRequest].bIncoming = bIncoming;	
	gRequests[nRequest].bDeleteOnSend = bDeleteOnSend;		
	
	if ( gnIndex == INDEX_END )				/* Wrap the unique request identifier if need to */
		gnIndex = REQUEST_INDEX_BASE;

	gnRequests++;

	return nRequest;
}

/*---------------------------------------------------------------------------
** requestInitializeFromObjectRequest( )
**
** Initialize parameters from EtIPObjectRequest structure. 
** Return 0 in case of success, or one of the error codes.
**---------------------------------------------------------------------------
*/
INT32 requestInitializeFromObjectRequest( INT32 nRequest, EtIPObjectRequest* pRequest )
{	
	if ( !pRequest->lExplicitMessageTimeout )
		pRequest->lExplicitMessageTimeout = DEFAULT_TIMEOUT;
	gRequests[nRequest].lRequestTimeoutTick = platformGetTickCount() + pRequest->lExplicitMessageTimeout;

	gRequests[nRequest].bService = pRequest->bService;
	gRequests[nRequest].iClass = pRequest->iClass;
	gRequests[nRequest].iInstance = pRequest->iInstance;
	gRequests[nRequest].iAttribute = pRequest->iAttribute;		
	gRequests[nRequest].iMember = pRequest->iMember;		
	gRequests[nRequest].iTagOffset = utilAddToMemoryPool( (unsigned char*)pRequest->requestTag, pRequest->iTagSize );
	gRequests[nRequest].iTagSize = pRequest->iTagSize;
	gRequests[nRequest].iDataOffset = utilAddToMemoryPool( pRequest->requestData, pRequest->iDataSize );
	gRequests[nRequest].iDataSize = pRequest->iDataSize;

	if ( (gRequests[nRequest].iDataSize && gRequests[nRequest].iDataOffset == INVALID_MEMORY_OFFSET) ||
		 (gRequests[nRequest].iTagSize && gRequests[nRequest].iTagOffset == INVALID_MEMORY_OFFSET) )
	{
		notifyEvent( NM_OUT_OF_MEMORY, 0 );
		requestRemove( nRequest );
		return NM_OUT_OF_MEMORY;
	}
			
	return 0;
}


/*---------------------------------------------------------------------------
** requestNewUCMM( )
**
** Queue new UCMM request message. 
** Return the request index or one of the error codes.
**---------------------------------------------------------------------------
*/

INT32 requestNewUCMM( const UINT8* szIPAddr, EtIPObjectRequest* pRequest )
{
	INT32 nRequest;

	nRequest = requestNew( ObjectRequest, FALSE, FALSE );		/* Schedule new UCMM request */
	
	if ( nRequest == ERROR_STATUS )						/* No more space for requests */
	{
		notifyEvent( NM_PENDING_REQUESTS_LIMIT_REACHED, 0 );
		return NM_PENDING_REQUESTS_LIMIT_REACHED;		
	}

	if ( requestInitializeFromObjectRequest( nRequest, pRequest ) == NM_OUT_OF_MEMORY )
	{			
		requestRemove( nRequest );
		return NM_OUT_OF_MEMORY;
	}
	
	gRequests[nRequest].lIPAddress = utilAddrFromPath( (const char*)szIPAddr );
	if ( gRequests[nRequest].lIPAddress == ERROR_STATUS )
	{			
		requestRemove( nRequest );
		return NM_REQUEST_FAILED_INVALID_NETWORK_PATH;
	}
		
	gRequests[nRequest].nState = RequestLogged;					
	return gRequests[nRequest].nIndex;
}

/*---------------------------------------------------------------------------
** requestKillSession( )
**
** Queue new Unconnected Send request disconnect request.
** Return the request index or one of the error codes.
**---------------------------------------------------------------------------
*/

INT32 requestKillSession(UINT8* szIPAddr)
{
	INT32 nRequest = requestNew(KillRequest, FALSE, FALSE);

	if( nRequest == ERROR_STATUS )
	{
		notifyEvent(NM_PENDING_REQUESTS_LIMIT_REACHED, 0);

		return NM_PENDING_REQUESTS_LIMIT_REACHED;
	}

	gRequests[nRequest].lIPAddress = utilAddrFromPath((const char*) szIPAddr);

	if( gRequests[nRequest].lIPAddress == ERROR_STATUS )
	{
		requestRemove(nRequest);

		return NM_REQUEST_FAILED_INVALID_NETWORK_PATH;
	}

	gRequests[nRequest].nState = RequestLogged;

	return gRequests[nRequest].nIndex;
}

/*---------------------------------------------------------------------------
** requestNewUnconnectedSend( )
**
** Queue new Unconnected Send request message.
** Return the request index or one of the error codes.
**---------------------------------------------------------------------------
*/
INT32 requestNewUnconnectedSend( UINT8* szIPAddr, EtIPObjectRequest* pRequest,
			char* extendedPath, UINT16 iExtendedPathLen)
{
	INT32 nRequest;

	nRequest = requestNew( UnconnectedSendRequest, FALSE, FALSE );	/* Schedule new Unconnected Send request */
	
	if ( nRequest == ERROR_STATUS )						/* No more space for requests */
	{
		notifyEvent( NM_PENDING_REQUESTS_LIMIT_REACHED, 0 );
		return NM_PENDING_REQUESTS_LIMIT_REACHED;		
	}

	if ( requestInitializeFromObjectRequest( nRequest, pRequest ) == NM_OUT_OF_MEMORY )
	{
		requestRemove( nRequest );
		return NM_OUT_OF_MEMORY;
	}

	gRequests[nRequest].iExtendedPathSize = iExtendedPathLen;
	gRequests[nRequest].iExtendedPathOffset = utilAddToMemoryPool( (unsigned char*)extendedPath, iExtendedPathLen );
	if ( gRequests[nRequest].iExtendedPathSize && gRequests[nRequest].iExtendedPathOffset == INVALID_MEMORY_OFFSET )
	{
		notifyEvent( NM_OUT_OF_MEMORY, 0 );
		requestRemove( nRequest );
		return NM_OUT_OF_MEMORY;		/* Out of memory */
	}
		
	gRequests[nRequest].lIPAddress = utilAddrFromPath( (const char*)szIPAddr );
	if ( gRequests[nRequest].lIPAddress == ERROR_STATUS )
	{
		requestRemove( nRequest );
		return NM_REQUEST_FAILED_INVALID_NETWORK_PATH;
	}
					
	gRequests[nRequest].nState = RequestLogged;	
	return gRequests[nRequest].nIndex;
}


/*---------------------------------------------------------------------------
** requestRemove( )
**
** Remove the request from the array
**---------------------------------------------------------------------------
*/

void requestRemove( INT32 nRequest )
{
	INT32 i;
	
	if ( gnRequests )
	{			
		utilRemoveFromMemoryPool( &gRequests[nRequest].iTagOffset, &gRequests[nRequest].iTagSize );
		utilRemoveFromMemoryPool( &gRequests[nRequest].iExtendedPathOffset, &gRequests[nRequest].iExtendedPathSize );
		utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );

		gnRequests--;
			
		/* Shift the requests with the bigger index to fill in the void */
		for( i = nRequest; i < gnRequests; i++ )				
			memcpy( &gRequests[i], &gRequests[i+1], sizeof(REQUEST) );	
	}
}

/*---------------------------------------------------------------------------
** requestRemoveAll( )
**
** Remove all requests from the array
**---------------------------------------------------------------------------
*/

void requestRemoveAll( )
{
	INT32 nRequest;
		
	for( nRequest = (gnRequests-1); nRequest >= 0; nRequest-- )
	{	
		utilRemoveFromMemoryPool( &gRequests[nRequest].iTagOffset, &gRequests[nRequest].iTagSize );
		utilRemoveFromMemoryPool( &gRequests[nRequest].iExtendedPathOffset, &gRequests[nRequest].iExtendedPathSize );
		utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );
	}	

	gnRequests = 0;
}

/*---------------------------------------------------------------------------
** requestSend( )
**
** Send the request
**---------------------------------------------------------------------------
*/

void requestSend( INT32 nRequest )
{
	if ( gRequests[nRequest].nType == ObjectRequest )
	{
		ucmmSendUnconnectedRequest( nRequest );	
		gRequests[nRequest].nState = RequestWaitingForResponse;		
	}
	else if ( gRequests[nRequest].nType == UnconnectedSendRequest )
	{		
		if ( gRequests[nRequest].iExtendedPathSize )
			ucmmPassUnconnectedSendRequest( nRequest );
		else
			ucmmPassUnconnectedSendRequestFinalLeg( nRequest );								
		gRequests[nRequest].nState = RequestWaitingForResponse;
	}	
}


/*---------------------------------------------------------------------------
** requestService( )
**
** Service a particular request
**---------------------------------------------------------------------------
*/

void requestService( INT32 nRequest )
{
	INT32  nSession;
	UINT32 lTick = platformGetTickCount();
	INT32  nIndex = gRequests[nRequest].nIndex;
			
	switch( gRequests[nRequest].nState )
	{
		case RequestLogged:
		{
			if ( gRequests[nRequest].bIncoming && gRequests[nRequest].nType == ObjectRequest )
			{
				routerProcessObjectRequest( nRequest );														
			}
			else if( gRequests[nRequest].nType == KillRequest )
			{
				nSession = sessionFindAddress(gRequests[nRequest].lIPAddress, Outgoing);

				if( nSession != INVALID_SESSION )
				{
					extern BOOL socketKillTCPConnection(INT32 nSession);

					socketKillTCPConnection(nSession);
				}

				requestRemove(nRequest);
			}
			else
			{
				nSession = sessionFindAddress( gRequests[nRequest].lIPAddress, Outgoing );
			
				if ( nSession == INVALID_SESSION )
				{
					nSession = sessionNew( gRequests[nRequest].lIPAddress, FALSE );
  	  		  
					if ( nSession == INVALID_SESSION )	/* Maximum number of open sessions has been reached */
					{							
						requestRemove(nRequest);
						notifyEvent(NM_SESSION_COUNT_LIMIT_REACHED, nIndex);
						return;
					}
				}
				else if ( gSessions[nSession].lState == OpenSessionEstablished )
				{
					requestSend( nRequest );
					return;
				}
				
				/* Session has not been successfully established yet */
				gRequests[nRequest].nState = RequestWaitingForSession;							
			}
		}
		break;

		case RequestWaitingForSession:
		{		
			nSession = sessionFindAddress( gRequests[nRequest].lIPAddress, Outgoing );
			
			if ( nSession == INVALID_SESSION || IS_TICK_GREATER( lTick, gRequests[nRequest].lRequestTimeoutTick ) )
			{	
				requestRemove(nRequest);
				notifyEvent(NM_REQUEST_FAILED_INVALID_NETWORK_PATH, nIndex);
				break;
			}

			if ( gSessions[nSession].lState == OpenSessionEstablished )
			{				
				requestSend( nRequest );
			}
		}
		break;
		
		case RequestWaitingForResponse:
		{	
			if ( IS_TICK_GREATER( lTick, gRequests[nRequest].lRequestTimeoutTick ) )
			{	
				requestRemove(nRequest);
				if ( !gRequests[nRequest].bIncoming )
					notifyEvent(NM_REQUEST_TIMED_OUT, nIndex);			
			}
		}
		break;

		case RequestResponseReceived:
		{		
			/* If request is an incoming one send the response, otherwise check for the client read timeout */
			if ( gRequests[nRequest].bIncoming )
			{
				ucmmSendObjectResponse( nRequest );
				requestRemove( nRequest );
			}
			else if ( gRequests[nRequest].bDeleteOnSend || IS_TICK_GREATER( lTick, gRequests[nRequest].lRequestTimeoutTick ) )
				requestRemove(nRequest);			
		}
		break;
	}
}

/*---------------------------------------------------------------------------
** requestResponseReceived( )
**
** Received the response
**---------------------------------------------------------------------------
*/

void requestResponseReceived( INT32 nRequest )
{	
	/* Set the timeout (10 seconds) for the client application to get the results */
	gRequests[nRequest].lRequestTimeoutTick = platformGetTickCount() + DEFAULT_TIMEOUT;
	gRequests[nRequest].nState = RequestResponseReceived;

	notifyEvent(NM_REQUEST_RESPONSE_RECEIVED, gRequests[nRequest].nIndex);	
}

/*---------------------------------------------------------------------------
** requestGetByRequestId( )
**
** Find a request based on the request Id
**---------------------------------------------------------------------------
*/

INT32 requestGetByRequestId( INT32 nRequestId )
{	
	INT32 nRequest;		
	
	for( nRequest = 0; nRequest < gnRequests; nRequest++ )		/* Find our request */
	{
		if ( gRequests[nRequest].nIndex == nRequestId )
			return nRequest;
	}

	return INVALID_REQUEST;
}
	
