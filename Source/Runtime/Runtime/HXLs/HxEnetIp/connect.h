/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** CONNECT.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Connection collection header file
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#ifndef CONNECT_H
#define CONNECT_H

#define INVALID_CONNECTION				(-1)			/* Invalid connection index */
#define INVALID_CONNECTION_SERIAL_NBR	0xffff			/* Invalid connection serial number */

#define FORWARD_CLOSE_TIMEOUT		100

#define CONNECTION_CONFIGURATION_DELAY	10

#define HEARTBEAT_CONN_POINT	1		/* Dummy connection point to indicate there is no data transferred, just the heartbeat (0 data length) message */
#define LISTEN_ONLY_CONN_POINT	0xff	/* Dummy connection point to indicate there is no data transferred, just the listen only heartbeat (0 data length) message */

#define DEFAULT_EXPLICIT_REQUEST_TIMEOUT			10000	/* Default explicit request timeout of 10 seconds */
#define DEFAULT_WATCHDOG_TIMEOUT_RECONNECT_DELAY	10000	/* Default automatic reconnect delay of 10 seconds if connection configuration iWatchdogTimeoutAction is set to TimeoutDelayAutoReset */


extern INT32   gnConnections;       /* Total number of all connections */ 

typedef enum tagConfigurationState
{
	ConfigurationLogged							= 1,	/* Connection request has just arrived */
    ConfigurationWaitingForSession				= 2,	/* Waiting for the the session to be opened	*/	
	ConfigurationWaitingForForwardOpenResponse  = 3,	/* Forward Open has just been sent; waiting for the response.*/
	ConfigurationFailedInvalidNetworkPath		= 5,	/* Unable to connect to the target */
	ConfigurationClosing						= 6,	/* Forward Close to be issued */
	ConfigurationWaitingForForwardCloseResponse = 7,	/* Waiting for the response to the issued Forward Close */
}
ConfigurationState;

typedef struct tagCONNECTION_CFG							
{
	INT32  nInstance;									/* Instance of the Connection object. Will be between 1 and MAX_CONNECTIONS */
	BOOL   bOriginator;									/* Indicates whether this stack is the originator of the connection */
	UINT16 iConnectionFlag;								/* Store connection flag set by RSNetworx */			
	UINT16 iConnectionPathOffset;						/* Null terminated connection path string, usually, just the IP address (i.e."216.233.160.112") */
	UINT16 iConnectionPathSize;							/* The size of the connection path string */ 
	UINT8  bSlot;										/* Target device slot number starting with 0 if applicable, or INVALID_SLOT_INDEX if not applicable */
	UINT16 iConfigConnInstance;							/* Connection object (usually Assembly) configuration instance. Could be INVALID_INSTANCE. Default value is 3. */	
	UINT16 iProducingConnPoint;							/* Connection object (usually Assembly) Producing by the Target (Out) connection point. Could be INVALID_CONN_POINT. Default value is 1. */
	UINT16 iConsumingConnPoint;							/* Connection object (usually Assembly) Consuming by the Target (In) connection point. Could be INVALID_CONN_POINT. Default value is 2. */	
	UINT16 iConnectionTagOffset;						/* Connection tag. Optionally used instead of specifying connection points. */
	UINT16 iConnectionTagSize;							/* Connection tag size. Must be set to 0 if connection tag is not used */
	UINT32 lProducingDataRate;							/* Producing I/O data rate in microseconds */
	UINT32 lConsumingDataRate;							/* Consuming I/O data rate in microseconds */
	UINT8  bProductionOTInhibitInterval;				/* The scanner (originator) will not produce with the higher rate than inhibit interval. Value of 0 disables it. */
	UINT8  bProductionTOInhibitInterval;				/* The adapter (target) will not produce with the higher rate than inhibit interval. Value of 0 disables it. */
	BOOL   bOutputRunProgramHeader;						/* Set to TRUE if the first 4 bytes of the output data represent Run/Program header */
	UINT16 iOutputDataOffset;							/* Output I/O data offset in bytes and starting with 0 in the output data table */
	UINT16 iOutputDataSize;								/* Producing I/O size in bytes including possible Run/Program header, but not including data sequence count */
	BOOL   bInputRunProgramHeader;						/* Set to TRUE if the first 4 bytes of the input data represent Run/Program header */
	UINT16 iInputDataOffset;							/* Input I/O data offset in bytes and starting with 0 in the input data table */	
	UINT16 iInputDataSize;								/* Consuming I/O size in bytes including possible Run/Program header, but not including data sequence count */	
    UINT16 iProducingConnectionType;					/* Must be PointToPoint */
	UINT16 iConsumingConnectionType;					/* Could be either PointToPoint or Multicast */    
	UINT16 iProducingPriority;							/* Must be one of the EtIPConnectionPriority values */    
	UINT16 iConsumingPriority;							/* Must be one of the EtIPConnectionPriority values */    
	UINT8  bTransportClass;								/* Indicates whether transport is Class 1 or Class 3 */
	UINT8  bTransportType;								/* One of the EtIPTransportType enumeration values */
	UINT8  bTimeoutMultiplier;							/* Timeout multiplier of 0 indicates that timeout should be 4 times larger than producing data rate, 1 - 8 times larger, 2 - 16 times, and so on */
	UINT16 iWatchdogTimeoutAction;						/* Watchdog Timeout Action from the EtIPWatchdogTimeoutAction enumeration */
	UINT32 lWatchdogTimeoutReconnectDelay;				/* If Watchdog Timeout Action is TimeoutDelayReset, Watchdog Timeout Reconnect Delay specifies reconnect delay in milliseconds */	
	EtIPDeviceID   deviceId;							/* Optional target device identification. It will be specified in the connection path when sending Forward Open. */
	EtIPDeviceID   proxyDeviceId;						/* Optional proxy device identification. Currently used only by RSNetworx.  */
	EtIPDeviceID   remoteDeviceId;						/* Optional remote target device identification. Currently used only by RSNetworx.  */
	UINT16 iConnectionNameOffset;						/* Optional user recognizable connection name */
	UINT16 iConnectionNameSize;							/* Connection name size. Must be set to 0 if connection name is not used */
	UINT16 iModuleConfig1Offset;						/* Optional first part of the target device specific configuration */
	UINT16 iModuleConfig1Size;							/* The size of the first part of the target device specific configuration. Must be set to 0 if configuration is not passed to the target device */
	UINT16 iModuleConfig2Offset;						/* Optional second part of the target device specific configuration */
	UINT16 iModuleConfig2Size;							/* The size of the second part of the target device specific configuration. Must be set to 0 if configuration is not passed to the target device */	
	UINT8  bService;									/* Service code for Class3 connections */
	UINT16 iClass;										/* Request Class number for Class3 connections */
	UINT16 iInstance;									/* Request Instance number for Class3 connections */
	UINT16 iAttribute;									/* Request Attribute  for Class3 connections */
	UINT16 iMember;										/* Request Member  for Class3 connections */
	UINT16 iTagOffset;									/* Request symbolic string Tag for Class3 connections */
	UINT16 iTagSize;									/* Tag size in bytes for Class3 connections */
	UINT16 iDataOffset;									/* Request and response data for Class3 connections */
	UINT16 iDataSize;									/* Data size in bytes for Class3 connections */

}
CONNECTION_CFG;

typedef struct  tagConnection 
{    
	UINT32	lIPAddress;					/* Target IP address */
	
	UINT32  lConnectionState;			/* Connection state from the EtIPConnectionState enumeration */
	UINT32  lConfigurationState;		/* The state of the connection configuration. Applicable only when lConnectionState is ConnectionConfiguring. */
	
	UINT32  lStartTick;					/* When the connection should start configuring. Used to delay connection open retry after a time out. */
	UINT32  lConfigurationTimeoutTick;	/* When the connection should time out if configuration status does not change for a prolonged period of time */

	UINT8   bGeneralStatus;				/* Together with Extended Status is used to provide more error information. 0 if connection is running fine, 0xD0 if connecting or disconnecting. */
	UINT16  iExtendedStatus;			/* Together with General Status is used to provide more error information. 2 if connecting, 3 if disconnecting, or any other extended error code. */

	BOOL    bTransferImmediately;		/* Change Of State transfer flag */
	
	UINT16  iOwnerConnectionSerialNbr;	/* If this is a listen only connection iOwnerConnectionSerialNbr is set to the previously established Exclusive Owner connection serial number. */

	CONNECTION_CFG cfg;					/* Connection configuration */
	
	UINT32	lClass1Socket;				/* UDP socket handle */

	BOOL    bCCConfigured;				/* TRUE if Forward Open was preceeded with the CC config request for an incoming connection, FALSE otherwise. Used only for incoming connections. */ 
	INT32   nCCProducingInstance;		/* Used only for incoming connections to store the CC instance corresponding to the Producing connection */
				
    struct sockaddr_in sReceiveAddr;	/* Socket address for receiving data */
    struct sockaddr_in sTransmitAddr;   /* Socket address for transmitting data */
	    
	UINT32  lAddrSeqNbr;				/* Packet Sequence number to send with Class1 messages */
	
	UINT16  iInDataSeqNbr;				/* Data Sequence number received with the last connected message */
	UINT16  iOutDataSeqNbr;				/* Data Sequence number to send with the next connected message */
		
	UINT8   bOpenPriorityTickTime;		/* Priority bit is bit 4. Low nibble is the number of milliseconds per tick */
	UINT8   bOpenTimeoutTicks;			/* Number of ticks to timeout */
	
	UINT32  lProducingCID;				/* Producing Connection Id identifies data produced on this connection */
	UINT32  lConsumingCID;				/* Consuming Connection Id identifies data consumed on this connection */
	
	UINT32  lInhibitInterval;			/* The data should not be transmitted on COS until inhibit interval has passed since the last transmission */
	UINT32  lTimeoutInterval;			/* Connection is timed out if there is no data from the device for this period */
		
	UINT16  iConnectionSerialNbr;		/* Unique number identifying this connection */
	UINT16  iVendorID;					/* Unique number identifying the originator vendor */
	UINT32  lDeviceSerialNbr;			/* Unique number identifying the originator device serial number */

	UINT32  lProduceTick;				/* When to produce next time on this connection */
	UINT32  lTimeoutTick;				/* When to timeout if no packets are received by then */
	UINT32  lInhibitExpireTick;			/* When to enable COS production */

} CONNECTION;

extern CONNECTION gConnections[];	/* Connection parameters array */

#define NUM_CONNECTION_GROUPS	((MAX_CONNECTIONS-1)/64 + 1)
#define GET_CONNECTION_GROUP( nConnection )	( ( gConnections[nConnection].cfg.nInstance - 1 ) / 64 )

extern void connectionInit();								/* Initialize connection array object */
extern void connectionInitialize( INT32 nConnection );		/* Initialize connection object */
extern INT32 connectionNew( UINT32 lIPAddress );			/* Allocate a new connection in the array */
extern void connectionAssignClass1Socket( INT32 nConnection );	/* Assign UDP socket to read/write I/O */
extern void connectionReleaseClass1Socket( INT32 nConnection );	/* Release UDP socket */
extern void connectionGoOffline( INT32 nConnection ); /* Take the connection offline */
extern void connectionRemove( INT32 nConnection );	/* Remove a particular connection from the array */
extern void connectionRemoveAll();							/* Clean up connection array */
extern BOOL connectionService(INT32 nConnection);			/* Service the specified connection */
extern INT32 connectionGetConnectedCount();					/* Returns number of active connections */
extern void connectionUpdateAllSequenceCount();				/* Update the data sequence count to indicate new data for all connections */
extern void connectionTimedOut(INT32 nConnection);			/* Specified connection timed out */
extern void connectionRecalcTimeouts( INT32 nConnection );	/* Reset timeout ticks */
extern void connectionResetAllTicks( INT32 nConnection );	/* Reset all ticks */
extern void connectionResetProducingTicks( INT32 nConnection );	/* Set the next tick to produce the I/O */
extern void connectionAdvanceProducingTicks( INT32 nConnection ); /* Advance the next tick to produce the I/O */
extern void connectionResetConsumingTicks( INT32 nConnection ); /* I/O was recieved - reset the timeout tick */
extern BOOL connectionIsTimedOut( INT32 nConnection );			/* Did we time out */
extern BOOL connectionIsTimeToProduce( INT32 nConnection );		/* Is it time to send the next cyclic I/O */
extern BOOL connectionIsInhibitExpired( INT32 nConnection );	/* Can we send I/O on the COS request */
extern INT32 connectionGetIndexFromInstance(INT32 nInstance);	/* Returns connection index based on the connection instance */
extern INT32 connectionGetIndexFromConnPoint(INT32 nConnPoint, BOOL* pbProducing);	/* Returns connection index based on the connection point */
extern INT32 connectionGetUnusedInstance();						/* Returns the first unused instance */
extern INT32 connectionGetNumOriginatorEstablished();			/* Returns the number of connections with the status ConnectionEstablished */
extern INT32 connectionGetNumOriginatorEstablishedOrClosing();	/* Returns the number of connections with the status ConnectionEstablished or ConnectionClosing */
extern INT32 connectionGetMaxInstance();						/* Return maximum instance in the connection array */
extern BOOL  connectionAnotherPending( INT32 nConnection );		/* Return TRUE if another connection with the same IP address is being opened or closed */
extern void  connectionDelayPending( INT32 nConnection );		/* Postpone the configuration of all other connections with the same IP address for CONNECTION_CONFIGURATION_DELAY period */
extern INT32 connectionGetIndexFromSerialNumber(UINT16 iConnectionSerialNbr); /* Return an array index based on the connection Serial Number */
extern INT32 connectionGetFirstMulticastProducer(INT32 nConnection);	/*Returns the connection index for the first connection producing to the same multicast as nConnection does */
extern INT32 connectionGetAnyMulticastProducer(INT32 nConnection);	/*Returns the connection index for any connection producing to the same multicast as nConnection does */
extern INT32 connectionGetMulticastProducerCount(INT32 nConnection); /* Returns the number of all other connections producing to the same multicast as nConnection does */
extern void connectionConvertToInternalCfgStorage(EtIPConnectionConfig* pcfg, CONNECTION_CFG* pConfig);
extern void connectionConvertFromInternalCfgStorage(CONNECTION_CFG* pcfg, EtIPConnectionConfig* pConfig);

#ifdef ET_IP_SCANNER
extern void connectionFailedForSession( INT32 nSession, UINT8 bGenStatus, UINT16 iExtendedErrorCode ); /* Fail the configuring connection for the particular session */
extern void connectionSetConnectionFlag(INT32 nConnection);     /* Set connection flag member based on the configuration parameters */
#endif

#endif /* #ifndef CONNECT_H */
