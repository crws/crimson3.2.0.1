/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** UCMM.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Services unconnected messages requests and repsonses
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"


/* Response data for the List Services command */
static ENCAP_OBJ_SERVICE list_services_data = {{ENCAP_VALUE_SHORT( ENCAP_OBJTYPE_SERVICE_COMM ), 
												ENCAP_VALUE_SHORT( ENCAP_SERVICE_SIZE )},
												{ENCAP_VALUE_SHORT( ENCAP_COMMSERVICE_VERSION ),
												ENCAP_VALUE_SHORT( ENCAP_COMMSERVICE_CIP_DIRECT | ENCAP_COMMSERVICE_IO_CONNECT ),
												ENCAP_COMMSERVICE_NAME}};

static ENCAP_RC_DATA register_data = {
   ENCAP_VALUE_SHORT( ENCAP_PROTOCOL_VERSION ),
   ENCAP_VALUE_SHORT( 0 )
};


/*---------------------------------------------------------------------------
** ucmmProcessEncapMsg( )
**
** Parse and process TCP packet 
**---------------------------------------------------------------------------
*/

void ucmmProcessEncapMsg( INT32 nSession )
{
   ENCAPH   *psEncapHdr = ( ENCAPH * )gmsgBuf;
   INT32    i;

   /* Discard No Operation commands */
   if ( psEncapHdr->iCommand == ENCAP_CMD_NOOP )
		return;
         
   switch ( psEncapHdr->iCommand )
   {
         /* List Services command */
         case ENCAP_CMD_LISTSERVICES:
			DumpStr0("encapProcessEncapMsg rcvd ENCAP_CMD_LISTSERVICES cmd");
		    ucmmRespondListServices( nSession );
            break;

         /* List Interfaces command */
         case ENCAP_CMD_LISTINTERFACES:
			DumpStr0("encapProcessEncapMsg rcvd ENCAP_CMD_LISTINTERFACES cmd");
			ucmmRespondListInterfaces( nSession );
            break;

         /* List Targets command */
         case ENCAP_CMD_LISTTARGETS:
            DumpStr0("encapProcessEncapMsg rcvd ENCAP_CMD_LISTTARGETS cmd");
		    ucmmRespondListTargets( nSession );
            break;

         /* Register Session command */
         case ENCAP_CMD_REGISTERSESSION:
            DumpStr0("encapProcessEncapMsg rcvd ENCAP_CMD_REGISTERSESSION cmd");
			/* Determine whether this is a request or a response */
			if ( !gSessions[nSession].bIncoming )
				ucmmParseRegisterSessionResponse( nSession );
			else
				ucmmRespondRegisterSession( nSession );
            break;

         /* UnRegister Session command */
         case ENCAP_CMD_UNREGISTERSESSION:
            DumpStr0("encapProcessEncapMsg rcvd ENCAP_CMD_UNREGISTERSESSION cmd");
			ucmmRespondUnRegisterSession( nSession );
            break;

         /* Indicate Status command received */
         case ENCAP_CMD_INDICATESTATUS:
            DumpStr0("encapProcessEncapMsg rcvd ENCAP_CMD_INDICATESTATUS cmd");			
            break;

         /* UCMM or Class 3 message received */
         case ENCAP_CMD_SEND_RRDATA:	/* UCMM */
         case ENCAP_CMD_SEND_UNITDATA:	/* Class 3 */
			if ( gSessions[nSession].lSessionTag != psEncapHdr->lSession )
			{
			   for( i = 0; i < gnSessions; i++ )
			   {
				   if ( gSessions[i].lSessionTag == psEncapHdr->lSession )
				   {
					   nSession = i;
					   break;
				   }
			   }
			   /* If could not match session identifier - ignore */
			   if ( i == gnSessions )
				   return;
			}
            DumpStr0("encapProcessEncapMsg rcvd ENCAP_CMD_SEND_RRDATA cmd");
			routerDataTransferReceived( nSession );
            break;

         /* Command not handled.  Return with error. */
         default:           
            ucmmErrorReply( nSession, ENCAP_E_UNHANDLED_CMD );         
   }
}


/*---------------------------------------------------------------------------
** ucmmIssueRegisterSession( )
**
** Open a new session with a server by issuing a Register Session request
**---------------------------------------------------------------------------
*/

void ucmmIssueRegisterSession( INT32 nSession )
{		
	ENCAPH*        pHdr = ( ENCAPH * )gmsgBuf;
	UINT8*         pData = &gmsgBuf[ENCAPH_SIZE];
	ENCAP_RC_DATA* rc = (ENCAP_RC_DATA*)pData;
	
	/* Store the new packet in the gmsgBuf buffer */
	memset( gmsgBuf, 0, (ENCAPH_SIZE + ENCAP_RC_DATA_SIZE) );
	
	pHdr->iCommand = ENCAP_CMD_REGISTERSESSION;
	pHdr->iLength = sizeof(ENCAP_RC_DATA_SIZE);
	pHdr->lStatus = 0;
		
    rc->iRc_version = ENCAP_TO_HS(gSessions[nSession].iClientEncapProtocolVersion);
    rc->iRc_flags = ENCAP_TO_HS(gSessions[nSession].iClientEncapProtocolFlags);
	
	socketEncapSendData( nSession );		    
}

/*---------------------------------------------------------------------------
** ucmmParseRegisterSessionResponse( )
**
** Parse a response to our Register Session request. 
**---------------------------------------------------------------------------
*/

void ucmmParseRegisterSessionResponse( INT32 nSession )
{	
	ENCAPH*        pHdr = ( ENCAPH * )gmsgBuf;
	unsigned char* pData = &gmsgBuf[ENCAPH_SIZE];
	ENCAP_RC_DATA* rc = (ENCAP_RC_DATA*)pData;
		
	if ( pHdr->iLength != ENCAP_RC_DATA_SIZE )
    {        
		sessionRemove( nSession, FALSE );			
        return;
    }

	/* Extract and store Session ID for future use */
	gSessions[nSession].lSessionTag = pHdr->lSession;

	gSessions[nSession].iClientEncapProtocolVersion = ENCAP_CVT_HS( rc->iRc_version );
	gSessions[nSession].iClientEncapProtocolFlags = ENCAP_CVT_HS( rc->iRc_flags );    
	gSessions[nSession].lState = OpenSessionEstablished;		
}

/*---------------------------------------------------------------------------
** ucmmRespondRegisterSession( )
**
** Respond to the incoming Register Session request. Act as a server.
**---------------------------------------------------------------------------
*/

void ucmmRespondRegisterSession( INT32 nSession )
{	
	ENCAPH         Hdr;
	ENCAPH*        pHdr = ( ENCAPH * )gmsgBuf;
	UINT8*         pData = &gmsgBuf[ENCAPH_SIZE];
	ENCAP_RC_DATA* rc = (ENCAP_RC_DATA*)pData;
	
	if ( pHdr->iLength != ENCAP_RC_DATA_SIZE )	/* Invalid header data */
    {
        ucmmErrorReply( nSession, ENCAP_E_BADDATA );         
        return;
    }

	/* Verify that versions of the protocol match */
	ENCAP_CVT_HS( rc->iRc_version );
    if ( rc->iRc_version != ENCAP_PROTOCOL_VERSION )
	{
        ucmmErrorReply( nSession, ENCAP_E_UNSUPPORTED_REV );         
        return;
    }

	/* Verify that server can support the requested capabilities */
    ENCAP_CVT_HS( rc->iRc_flags );
    if ( rc->iRc_flags != 0 )
	{
        ucmmErrorReply( nSession, ENCAP_E_UNSUPPORTED_REV );         
        return;
    }            

	if ( pHdr->lStatus == ENCAP_E_SUCCESS )		/* Check that status is 0 */
    {        
		/* Only 1 Register Session per socket is allowed */
		if ( gSessions[nSession].lState == OpenSessionEstablished )
		{
			ucmmErrorReply( nSession, ENCAP_E_BAD_SESSIONID );         
			return;
		}   
		
		memcpy( &Hdr, pHdr, ENCAPH_SIZE );
		memset( pHdr, 0, ENCAPH_SIZE );
		
		gSessions[nSession].lSessionTag = utilGetUniqueID(); /* Choose Session Tag */
		
		pHdr->iCommand = Hdr.iCommand;
		pHdr->lSession = gSessions[nSession].lSessionTag;
		pHdr->lStatus = 0;
        pHdr->iLength = sizeof(register_data);
				
		memcpy( pData, &register_data, sizeof( register_data ) );

		socketEncapSendData( nSession );		

		gSessions[nSession].lState = OpenSessionEstablished;
    }
}


/*---------------------------------------------------------------------------
** ucmmIssueUnRegisterSession( )
**
** Terminate a session with the server by issuing Unregister Session request
**---------------------------------------------------------------------------
*/

void ucmmIssueUnRegisterSession( INT32 nSession )
{		
	ENCAPH*        pHdr = ( ENCAPH * )gmsgBuf;
			
	memset( pHdr, 0, ENCAPH_SIZE );
	
	pHdr->iCommand = ENCAP_CMD_UNREGISTERSESSION;
	pHdr->lSession = gSessions[nSession].lSessionTag;
	pHdr->iLength = 0;
	pHdr->lStatus = 0;
	    
	socketEncapSendData( nSession );		    
}


/*---------------------------------------------------------------------------
** ucmmRespondUnRegisterSession( )
**
** Received Unregister Session request. Cleanup appropriate session object.
**---------------------------------------------------------------------------
*/

void ucmmRespondUnRegisterSession( INT32 nSession )
{		
	sessionRemove( nSession, FALSE );			    
}


/*---------------------------------------------------------------------------
** ucmmIssueListServices( )
**
** Send List Services request to the server
**---------------------------------------------------------------------------
*/

void ucmmIssueListServices( INT32 nSession )
{		
	ENCAPH*        pHdr = ( ENCAPH * )gmsgBuf;
	
	memset( gmsgBuf, 0, ENCAPH_SIZE );
	
	pHdr->iCommand = ENCAP_CMD_LISTSERVICES;
	pHdr->lSession = gSessions[nSession].lSessionTag;
	pHdr->iLength = 0;
	pHdr->lStatus = 0;
	    
	socketEncapSendData( nSession );		    
}


/*---------------------------------------------------------------------------
** ucmmRespondListServices( )
**
** Respond to the List Services request
**---------------------------------------------------------------------------
*/

void ucmmRespondListServices( INT32 nSession )
{	
	ENCAPH   Hdr;
	ENCAPH*  pHdr = ( ENCAPH * )gmsgBuf;
	UINT8*    pData; 

	if ( pHdr->iLength != 0 )	/* Invalid header data */
    {
        if ( nSession >= 0 )
			ucmmErrorReply( nSession, ENCAP_E_BADDATA );         
        return;
    }

	if ( pHdr->lStatus == ENCAP_E_SUCCESS )	/* Check that status is 0 */
    {        
		memcpy( &Hdr, pHdr, ENCAPH_SIZE );
		memset( pHdr, 0, ENCAPH_SIZE );
		
		pHdr->iCommand = Hdr.iCommand;
		pHdr->lSession = Hdr.lSession;
		pHdr->lStatus = 0;
        pHdr->iLength = sizeof(UINT16) + sizeof(list_services_data);
		
		pData = &gmsgBuf[ENCAPH_SIZE];
		UINT16_SET(pData, 1);			/* object count */

		pData += sizeof(UINT16);
		memcpy( pData, &list_services_data, sizeof( list_services_data ) );

		if ( nSession >= 0 )
			socketEncapSendData( nSession );		
    }
}

/*---------------------------------------------------------------------------
** ucmmRespondListInterfaces( )
**
** Respond to the List Interfaces request
**---------------------------------------------------------------------------
*/

void ucmmRespondListInterfaces( INT32 nSession )
{	
	ENCAPH   Hdr;
	ENCAPH*  pHdr = ( ENCAPH * )gmsgBuf;
	UINT8*   pData; 

	if ( pHdr->iLength != 0 )	/* Invalid header data */
    {
        if ( nSession >= 0 )
			ucmmErrorReply( nSession, ENCAP_E_BADDATA );         
        return;
    }

	if ( pHdr->lStatus == ENCAP_E_SUCCESS )
    {        
		memcpy( &Hdr, pHdr, ENCAPH_SIZE );
		memset( pHdr, 0, ENCAPH_SIZE );
		
		pHdr->iCommand = Hdr.iCommand;
		pHdr->lSession = Hdr.lSession;
		pHdr->lStatus = 0;
        pHdr->iLength = sizeof(UINT16);
		
		pData = &gmsgBuf[ENCAPH_SIZE];
		UINT16_SET(pData, 0);			/* object count */

		if ( nSession >= 0 )
			socketEncapSendData( nSession );		
    }
}

/*---------------------------------------------------------------------------
** ucmmRespondListTargets( )
**
** Respond to the List Targets reaquest
**---------------------------------------------------------------------------
*/

void ucmmRespondListTargets( INT32 nSession )
{	
	ENCAPH   Hdr;
	ENCAPH*  pHdr = ( ENCAPH * )gmsgBuf;
	UINT8*   pData; 
	UINT16   iItemLen; 
	UINT8	 bProductNameSize = UINT8(strlen(gIDInfo.productName));	/* Product name and the trailing 0 */

	if ( pHdr->iLength != 0 )		/* Invalid header data */
    {
        if ( nSession >= 0 )
			ucmmErrorReply( nSession, ENCAP_E_BADDATA );         
        return;
    }

	if ( pHdr->lStatus == ENCAP_E_SUCCESS )
    {        
		iItemLen = sizeof(UINT16) +			/* Protocol Version */
				   sizeof(struct sockaddr_in) +	/* Socket address */				   
				   sizeof(UINT16) +			/* Vendor ID */
				   sizeof(UINT16) +			/* Product Type */
				   sizeof(UINT16) +			/* Product Code */
				   sizeof(UINT16) +			/* Revision */
				   sizeof(UINT16) +			/* Device Status */
				   sizeof(UINT32) +			/* Serial Number */
				   sizeof(UINT8) +			/* Product name size */				   
				   bProductNameSize +		/* Product name */				   
				   sizeof(UINT8);			/* Target State */				   				   
		
		memcpy( &Hdr, pHdr, ENCAPH_SIZE );
		memset( pHdr, 0, ENCAPH_SIZE );
		
		pHdr->iCommand = Hdr.iCommand;
		pHdr->lSession = Hdr.lSession;
		pHdr->lStatus = 0;
        pHdr->iLength = sizeof(UINT16) +	/* Object Count */
						sizeof(UINT16) +    /* Target code */
						sizeof(UINT16) +    /* Target length */
						iItemLen;		    /* Target */
			
		pData = &gmsgBuf[ENCAPH_SIZE];
		
		UINT16_SET(pData, 1);						/* Object count */
		pData += sizeof(UINT16);

		UINT16_SET(pData, IDENTITY_CIP_TARGET);		/* Target code */
		pData += sizeof(UINT16);

		UINT16_SET(pData, iItemLen);				/* Target length */
		pData += sizeof(UINT16);

		UINT16_SET(pData, ENCAP_PROTOCOL_VERSION);	/* Protocol Version */
		pData += sizeof(UINT16);
				
		memcpy(pData, &gHostAddr, sizeof(struct sockaddr_in));	/* Socket address */
		pData += sizeof(struct sockaddr_in);
		
		UINT16_SET(pData, gIDInfo.iVendor );			/* Vendor ID */
		pData += sizeof(UINT16);
	
		UINT16_SET(pData, gIDInfo.iProductType);	/* Product Type */
		pData += sizeof(UINT16);

		UINT16_SET(pData, gIDInfo.iProductCode);	/* Product Code */
		pData += sizeof(UINT16);

		*pData++ = gIDInfo.bMajorRevision;			/* Revision */
		*pData++ = gIDInfo.bMinorRevision;

		UINT16_SET(pData, gIDInfo.iStatus);			/* Device Status */
		pData += sizeof(UINT16);

		UINT32_SET(pData, gIDInfo.lSerialNumber);	/* Serial Number */
		pData += sizeof(UINT32);

		*pData++ = bProductNameSize;				/* Product name size */			
		
		memcpy( pData, gIDInfo.productName, bProductNameSize );	/* Product name */			
		pData += bProductNameSize;

		*pData++ = DEFAULT_DEVICE_STATE;				/* Target State */			

		if ( nSession >= 0 )
			socketEncapSendData( nSession );		
    }
}


/*---------------------------------------------------------------------------
** ucmmErrorReply( )
**
** Send encapsulated message response indicating an error.
** lStatus contains error code.
**---------------------------------------------------------------------------
*/

void ucmmErrorReply( INT32 nSession, UINT32 lStatus  )
{	
	ENCAPH    Hdr;
	ENCAPH*   pHdr = ( ENCAPH * )gmsgBuf;
		
	memcpy( &Hdr, pHdr, ENCAPH_SIZE );
	memset( pHdr, 0, ENCAPH_SIZE );
		
	pHdr->iCommand = Hdr.iCommand;
	pHdr->lSession = Hdr.lSession;
	pHdr->lStatus =  lStatus;
    pHdr->iLength = 0;
				
	socketEncapSendData( nSession );		    
}

/*---------------------------------------------------------------------------
** ucmmSendConnectedRequest( )
**
** Send Class 3 message
**---------------------------------------------------------------------------
*/

void ucmmSendConnectedRequest( INT32 nConnection )
{
	ENCAPH*        pHdr = ( ENCAPH * )gmsgBuf;
	CPF_TAG		   tag;
	UINT8*		   pData;			
	ENCAP_DT_HDR   dataHdr;
	PDU_HDR        pdu_hdr;
	INT32          nSession;
	UINT16         iPDUSize = sizeof(UINT8) * 2;			/* Service Code + ioi size */;

	iPDUSize += (gConnections[nConnection].cfg.iClass != INVALID_CLASS) ? (IS_BYTE(gConnections[nConnection].cfg.iClass) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += (gConnections[nConnection].cfg.iInstance != INVALID_INSTANCE) ? (IS_BYTE(gConnections[nConnection].cfg.iInstance) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += (gConnections[nConnection].cfg.iAttribute != INVALID_ATTRIBUTE) ? sizeof(UINT16) : 0;
	iPDUSize += (gConnections[nConnection].cfg.iMember != INVALID_MEMBER) ? (IS_BYTE(gConnections[nConnection].cfg.iMember) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += (gConnections[nConnection].cfg.iTagSize) ? ( (gConnections[nConnection].cfg.iTagSize%2) ? (sizeof(UINT16) + gConnections[nConnection].cfg.iTagSize + 1) : (sizeof(UINT16) + gConnections[nConnection].cfg.iTagSize) ) : 0;
		
	nSession = sessionFindAddressEstablished( gConnections[nConnection].lIPAddress, Outgoing );

	if ( nSession == INVALID_SESSION )
	{
		DumpStr1("ucmmSendConnectedRequest Session terminated from under us: IP address 0x%x", gConnections[nConnection].lIPAddress);
		return;
	}
					
	memset( pHdr, 0, ENCAPH_SIZE );

	pHdr->iCommand = ENCAP_CMD_SEND_UNITDATA;
	pHdr->lSession = gSessions[nSession].lSessionTag;
	pHdr->lStatus = 0;
	pHdr->lContext1 = 0;
	pHdr->lContext2 = 0;
	pHdr->iLength = ENCAP_DT_HDR_SIZE +				/* Required data header */
					   sizeof(UINT16) +				/* Tag (or object) count */
					   CPF_TAG_SIZE * 2 +			/* Two tag headers */
					   sizeof(UINT32) +				/* Class 3 Connection ID */
					   sizeof(UINT16) +				/* Connection sequence count */
					   iPDUSize +					/* Service + ioi size + IOI */
					   gConnections[nConnection].cfg.iDataSize;	/* Class 3 data length */
	
	pData = &gmsgBuf[ENCAPH_SIZE];					/* Store response packet in the gmsgBuf buffer */
	
	dataHdr.lTarget = ENCAP_TO_HL(INTERFACE_HANDLE);
	dataHdr.iTimeout = ENCAP_TO_HS(CONNECTION_TIMEOUT);
	
	memcpy( pData, &dataHdr, ENCAP_DT_HDR_SIZE );	/* Add data header */
	pData += ENCAP_DT_HDR_SIZE;

	UINT16_SET(pData, 2);							/* Add tag count */
	pData += sizeof(UINT16);

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_ADR_CONNECTED);			/* Indicate that this is Class 3 message */
	tag.iTag_length = UINT16(ENCAP_TO_HL(sizeof(UINT32)));

	memcpy(pData, &tag, CPF_TAG_SIZE);
	pData += CPF_TAG_SIZE;

	UINT32_SET( pData, gConnections[nConnection].lProducingCID );	/* Supply connection ID */
	pData += sizeof(UINT32);

	tag.iTag_type = UINT16(ENCAP_TO_HL(CPF_TAG_PKT_CONNECTED));
	tag.iTag_length = ENCAP_TO_HS(sizeof(UINT16) + iPDUSize + gConnections[nConnection].cfg.iDataSize);	
					  
	memcpy( pData, &tag, CPF_TAG_SIZE );
	pData += CPF_TAG_SIZE;

	UINT16_SET( pData, gConnections[nConnection].iOutDataSeqNbr );		/* Supply the sequence count */
	pData += sizeof(UINT16);

	gConnections[nConnection].iOutDataSeqNbr++;							/* Auto increment the sequence count */

	pdu_hdr.bService = gConnections[nConnection].cfg.bService; 
	pdu_hdr.bSize = UINT8(( iPDUSize - sizeof(UINT16) ) / 2);    /* Path size in words */

	memcpy( pData, &pdu_hdr, PDU_HDR_SIZE );		/* Add Protocol Data Unit */
	pData += PDU_HDR_SIZE;

	if ( gConnections[nConnection].cfg.iClass != INVALID_CLASS )
	{
		if ( IS_BYTE(gConnections[nConnection].cfg.iClass) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_CLASS | LOGICAL_SEG_8_BIT; /* One byte class logical segment */
			*pData++ = (UINT8)gConnections[nConnection].cfg.iClass;
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_CLASS | LOGICAL_SEG_16_BIT; /* Two byte class logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gConnections[nConnection].cfg.iClass);
			pData += sizeof(UINT16);
		}
	}

	if ( gConnections[nConnection].cfg.iInstance != INVALID_INSTANCE )
	{
		if ( IS_BYTE(gConnections[nConnection].cfg.iInstance) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_INSTANCE | LOGICAL_SEG_8_BIT; /* One byte instance logical segment */
			*pData++ = (UINT8)gConnections[nConnection].cfg.iInstance;	
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_INSTANCE | LOGICAL_SEG_16_BIT; /* Two byte instance logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gConnections[nConnection].cfg.iInstance);	
			pData += sizeof(UINT16);
		}
	}

	if ( gConnections[nConnection].cfg.iAttribute != INVALID_ATTRIBUTE )
	{
		*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_ATTRIBUTE | LOGICAL_SEG_8_BIT;	/* One byte attribute logical segment */
		*pData++ = (UINT8)gConnections[nConnection].cfg.iAttribute;			
	}

	if ( gConnections[nConnection].cfg.iMember != INVALID_MEMBER )
	{
		if ( IS_BYTE(gConnections[nConnection].cfg.iMember) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_MEMBER | LOGICAL_SEG_8_BIT; /* One byte instance logical segment */
			*pData++ = (UINT8)gConnections[nConnection].cfg.iMember;	
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_MEMBER | LOGICAL_SEG_16_BIT; /* Two byte instance logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gConnections[nConnection].cfg.iMember);	
			pData += sizeof(UINT16);
		}
	}

	if ( gConnections[nConnection].cfg.iTagSize )
	{
		//*pData++ = EXT_SYMBOL_SEGMENT;
		//*pData++ = (UINT8)gConnections[nConnection].cfg.iTagSize;

		memcpy(pData, MEM_PTR(gConnections[nConnection].cfg.iTagOffset), gConnections[nConnection].cfg.iTagSize );
		pData += gConnections[nConnection].cfg.iTagSize;

		//if ( gConnections[nConnection].cfg.iTagSize % 2 ) /* add a pad byte */
		//	*pData++ = 0;
	}

	memcpy( pData, MEM_PTR(gConnections[nConnection].cfg.iDataOffset), gConnections[nConnection].cfg.iDataSize );
		
	socketEncapSendData( nSession );		
}


/*---------------------------------------------------------------------------
** ucmmSendUnconnectedRequest( )
**
** Send UCMM request
**---------------------------------------------------------------------------
*/
void ucmmSendUnconnectedRequest( INT32 nRequest )
{
	ENCAPH*            pHdr = ( ENCAPH * )gmsgBuf;
	CPF_TAG			   tag;
	UINT8*			   pData;	
//	UINT32			   lTotalLen = ENCAPH_SIZE;
	PDU_HDR            pdu_hdr;
	ENCAP_DT_HDR       dataHdr;
	INT32              nSession;
	UINT16			   iPDUSize = sizeof(UINT8) * 2;			/* Service Code + ioi size */;

	iPDUSize += (gRequests[nRequest].iClass != INVALID_CLASS) ? (IS_BYTE(gRequests[nRequest].iClass) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += (gRequests[nRequest].iInstance != INVALID_INSTANCE) ? (IS_BYTE(gRequests[nRequest].iInstance) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += (gRequests[nRequest].iAttribute != INVALID_ATTRIBUTE) ? sizeof(UINT16) : 0;
	iPDUSize += (gRequests[nRequest].iMember != INVALID_MEMBER) ? (IS_BYTE(gRequests[nRequest].iMember) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += gRequests[nRequest].iTagSize;
			
	nSession = sessionFindAddressEstablished( gRequests[nRequest].lIPAddress, Outgoing );

	if ( nSession == INVALID_SESSION )
	{
		DumpStr1("ucmmSendUnconnectedRequest invalid IP address 0x%x", gRequests[nRequest].lIPAddress);
		return;
	}
		
	memset( pHdr, 0, ENCAPH_SIZE );
		
	pHdr->iCommand = ENCAP_CMD_SEND_RRDATA;
	pHdr->lSession = gSessions[nSession].lSessionTag;
	pHdr->lContext1 = gRequests[nRequest].lContext1;
	pHdr->lContext2 = gRequests[nRequest].lContext2;
	pHdr->lStatus = 0;
	pHdr->iLength = ENCAP_DT_HDR_SIZE +				/* Required data header */
					   sizeof(UINT16) +				/* Tag (or object) count */
					   CPF_TAG_SIZE * 2 +			/* Two tag headers */
					   iPDUSize +					/* Service + ioi size + IOI */
					   gRequests[nRequest].iDataSize;				/* UCMM data request length */
	
	pData = &gmsgBuf[ENCAPH_SIZE];					/* Store message in the gmsgBuf buffer */
	
	dataHdr.lTarget = ENCAP_TO_HL(INTERFACE_HANDLE);
	dataHdr.iTimeout = ENCAP_TO_HS(CONNECTION_TIMEOUT);
	
	memcpy( pData, &dataHdr, ENCAP_DT_HDR_SIZE );	/* Add data header */
	pData += ENCAP_DT_HDR_SIZE;

	UINT16_SET(pData, gRequests[nRequest].iTagCount);				/* Specify how many tags do follow */
	pData += sizeof(UINT16);

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_ADR_NULL);				/* Indicates local target */
	tag.iTag_length = 0;

	memcpy(pData, &tag, CPF_TAG_SIZE);
	pData += CPF_TAG_SIZE;

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_PKT_UCMM);				/* UCMM tag header */
	tag.iTag_length = ENCAP_TO_HS(iPDUSize + gRequests[nRequest].iDataSize);	

	memcpy( pData, &tag, CPF_TAG_SIZE );
	pData += CPF_TAG_SIZE;

	pdu_hdr.bService = gRequests[nRequest].bService; 
	pdu_hdr.bSize = UINT8( ( iPDUSize - sizeof(UINT16) ) / 2);    /* Path size in words */

	memcpy( pData, &pdu_hdr, PDU_HDR_SIZE );		/* Add Protocol Data Unit */
	pData += PDU_HDR_SIZE;

	if ( gRequests[nRequest].iClass != INVALID_CLASS )
	{
		if ( IS_BYTE(gRequests[nRequest].iClass) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_CLASS | LOGICAL_SEG_8_BIT; /* One byte class logical segment */
			*pData++ = (UINT8)gRequests[nRequest].iClass;
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_CLASS | LOGICAL_SEG_16_BIT; /* Two byte class logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gRequests[nRequest].iClass);
			pData += sizeof(UINT16);
		}
	}

	if ( gRequests[nRequest].iInstance != INVALID_INSTANCE )
	{
		if ( IS_BYTE(gRequests[nRequest].iInstance) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_INSTANCE | LOGICAL_SEG_8_BIT; /* One byte instance logical segment */
			*pData++ = (UINT8)gRequests[nRequest].iInstance;	
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_INSTANCE | LOGICAL_SEG_16_BIT; /* Two byte instance logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gRequests[nRequest].iInstance);	
			pData += sizeof(UINT16);
		}
	}

	if ( gRequests[nRequest].iAttribute != INVALID_ATTRIBUTE )
	{
		*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_ATTRIBUTE | LOGICAL_SEG_8_BIT;	/* One byte attribute logical segment */
		*pData++ = (UINT8)gRequests[nRequest].iAttribute;			
	}

	if ( gRequests[nRequest].iMember != INVALID_MEMBER )
	{
		if ( IS_BYTE(gRequests[nRequest].iMember) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_MEMBER | LOGICAL_SEG_8_BIT; /* One byte instance logical segment */
			*pData++ = (UINT8)gRequests[nRequest].iMember;	
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_MEMBER | LOGICAL_SEG_16_BIT; /* Two byte instance logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gRequests[nRequest].iMember);	
			pData += sizeof(UINT16);
		}
	}

	if ( gRequests[nRequest].iTagSize )
	{
		//*pData++ = EXT_SYMBOL_SEGMENT;
		//*pData++ = (UINT8)gRequests[nRequest].iTagSize;

		memcpy(pData, MEM_PTR(gRequests[nRequest].iTagOffset), gRequests[nRequest].iTagSize );
		pData += gRequests[nRequest].iTagSize;

		//if ( gRequests[nRequest].iTagSize % 2 ) /* add a pad byte */
		//	*pData++ = 0;
	}
	
	memcpy( pData, MEM_PTR(gRequests[nRequest].iDataOffset), gRequests[nRequest].iDataSize );		/* Add request data if provided */
		
	socketEncapSendData( nSession );			
}


/*---------------------------------------------------------------------------
** ucmmSendObjectResponse( )
**
** Send UCMM or Class 3 message response 
**---------------------------------------------------------------------------
*/

void ucmmSendObjectResponse( INT32 nRequest )
{
	if ( gRequests[nRequest].iConnectionSerialNbr != INVALID_CONNECTION_SERIAL_NBR )
		ucmmSendConnectedObjectResponse( nRequest );
	else
		ucmmSendUnconnectedObjectResponse( nRequest );
}


/*---------------------------------------------------------------------------
** ucmmSendConnectedObjectResponse( )
**
** Send Class 3 message response
**---------------------------------------------------------------------------
*/

void ucmmSendConnectedObjectResponse( INT32 nRequest )
{
	ENCAPH*        pHdr = ( ENCAPH * )gmsgBuf;
	CPF_TAG		   tag;
	UINT8*		   pData;			
	ENCAP_DT_HDR   dataHdr;
	REPLY_HEADER   replyHdr;
	UINT16         iTagCount = gRequests[nRequest].iTagCount;
	INT32          nConnection;
	INT32          nSession;
		
	nSession = sessionFindAddressEstablished( gRequests[nRequest].lIPAddress, Incoming );

	if ( nSession == INVALID_SESSION )
	{
		DumpStr1("ucmmSendConnectedObjectResponse Session terminated from under us: IP address 0x%x", gRequests[nRequest].lIPAddress);
		return;
	}

	nConnection = connectionGetIndexFromSerialNumber( gRequests[nRequest].iConnectionSerialNbr );

	if ( nConnection == ERROR_STATUS )
	{
		DumpStr1("ucmmSendConnectedObjectResponse Connection closed from under us: Serial Number specified 0x%hx", gRequests[nRequest].iConnectionSerialNbr );
		return;
	}
		
	memset( pHdr, 0, ENCAPH_SIZE );

	if ( gRequests[nRequest].bGeneralError )						/* We have an error */
	{		
		utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&gRequests[nRequest].iExtendedError, sizeof(UINT16) );			

		if ( gRequests[nRequest].iDataSize && gRequests[nRequest].iDataOffset == INVALID_MEMORY_OFFSET )
		{
			notifyEvent( NM_OUT_OF_MEMORY, 0 );
			return;	/* Out of memory */
		}
	}
		
	pHdr->iCommand = ENCAP_CMD_SEND_UNITDATA;
	pHdr->lSession = gSessions[nSession].lSessionTag;
	pHdr->lStatus = 0;
	pHdr->lContext1 = gRequests[nRequest].lContext1;
	pHdr->lContext2 = gRequests[nRequest].lContext2;
	pHdr->iLength = ENCAP_DT_HDR_SIZE +				/* Required data header */
					   sizeof(UINT16) +				/* Tag (or object) count */
					   CPF_TAG_SIZE * 2 +			/* Two tag headers */
					   sizeof(UINT32) +				/* Class 3 Connection ID */
					   sizeof(UINT16) +				/* Connection sequence count */
					   REPLY_HEADER_SIZE +			/* Reply header */  
					   gRequests[nRequest].iDataSize;				/* Class 3 data length */
	
	pData = &gmsgBuf[ENCAPH_SIZE];					/* Store response packet in the gmsgBuf buffer */
	
	dataHdr.lTarget = ENCAP_TO_HL(INTERFACE_HANDLE);
	dataHdr.iTimeout = 0;
	
	memcpy( pData, &dataHdr, ENCAP_DT_HDR_SIZE );	/* Add data header */
	pData += ENCAP_DT_HDR_SIZE;

	UINT16_SET(pData, iTagCount);					/* Add tag count */
	pData += sizeof(iTagCount);

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_ADR_CONNECTED);			/* Indicate that this is Class 3 message */
	tag.iTag_length = ENCAP_TO_HS(sizeof(UINT32));

	memcpy(pData, &tag, CPF_TAG_SIZE);
	pData += CPF_TAG_SIZE;

	UINT32_SET( pData, gConnections[nConnection].lProducingCID );	/* Supply connection ID */
	pData += sizeof(UINT32);

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_PKT_CONNECTED);
	tag.iTag_length = ENCAP_TO_HS(sizeof(UINT16) + REPLY_HEADER_SIZE + gRequests[nRequest].iDataSize);	
					  
	memcpy( pData, &tag, CPF_TAG_SIZE );
	pData += CPF_TAG_SIZE;

	UINT16_SET( pData, gRequests[nRequest].iConnSequence );		/* Supply the sequence count */
	pData += sizeof(UINT16);

	replyHdr.bService = gRequests[nRequest].bService | REPLY_BIT_MASK;	/* Populate Reply header */
	replyHdr.bIoiSize = 0;
	replyHdr.bGenStatus = gRequests[nRequest].bGeneralError;				/* 0 indicates a success */
	replyHdr.bObjStatusSize = gRequests[nRequest].bGeneralError ? 1 : 0;  /* If there is an error provide one word long extended error information */

	memcpy(pData, &replyHdr, REPLY_HEADER_SIZE);			/* Add reply header */
	pData += REPLY_HEADER_SIZE;

	memcpy( pData, MEM_PTR(gRequests[nRequest].iDataOffset), gRequests[nRequest].iDataSize );

	/* Store Class 3 response results in the Assembly */
	assemblySetConnectionOutputData( nConnection, pData, gRequests[nRequest].iDataSize );	/* Store data in assembly object */	
		
	socketEncapSendData( nSession );			
}

/*---------------------------------------------------------------------------
** ucmmSendUnconnectedObjectResponse( )
**
** Send UCMM response
**---------------------------------------------------------------------------
*/

void ucmmSendUnconnectedObjectResponse( INT32 nRequest )
{
	ENCAPH*            pHdr = ( ENCAPH * )gmsgBuf;
	CPF_TAG			   tag;
	UINT8*			   pData;			
	ENCAP_DT_HDR       dataHdr;
	REPLY_HEADER       replyHdr;
	UINT16             iTagCount = gRequests[nRequest].iTagCount;
	INT32              nSession;
	UINT16             iExtError;
		
	nSession = sessionFindAddressEstablished( gRequests[nRequest].lIPAddress, Incoming );

	if ( nSession == INVALID_SESSION )
	{
		DumpStr1("ucmmSendUnconnectedObjectResponse Session terminated from under us: IP address 0x%x", gRequests[nRequest].lIPAddress);
		return;
	}
	
	memset( pHdr, 0, ENCAPH_SIZE );

	if ( gRequests[nRequest].bGeneralError )						/* We have an error */
	{
		iExtError = ENCAP_TO_HS(gRequests[nRequest].iExtendedError);
		utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iExtError, sizeof(UINT16) );		

		if ( gRequests[nRequest].iDataSize && gRequests[nRequest].iDataOffset == INVALID_MEMORY_OFFSET )
		{
			notifyEvent( NM_OUT_OF_MEMORY, 0 );
			return;	/* Out of memory */
		}
	}
		
	pHdr->iCommand = ENCAP_CMD_SEND_RRDATA;
	pHdr->lSession = gSessions[nSession].lSessionTag;
	pHdr->lStatus = 0;
	pHdr->lContext1 = gRequests[nRequest].lContext1;
	pHdr->lContext2 = gRequests[nRequest].lContext2;
	pHdr->iLength = ENCAP_DT_HDR_SIZE +				/* Required data header */
					   sizeof(UINT16) +				/* Tag (or object) count */
					   CPF_TAG_SIZE * 2 +			/* Two tag headers */
					   REPLY_HEADER_SIZE +			/* Reply header size */
					   gRequests[nRequest].iDataSize;				/* UCMM data length */
	
	pData = &gmsgBuf[ENCAPH_SIZE];
	
	dataHdr.lTarget = ENCAP_TO_HL(INTERFACE_HANDLE);
	dataHdr.iTimeout = ENCAP_TO_HS(CONNECTION_TIMEOUT);
	
	memcpy( pData, &dataHdr, ENCAP_DT_HDR_SIZE );	/* Add data header */
	pData += ENCAP_DT_HDR_SIZE;

	UINT16_SET(pData, iTagCount);					/* Add tag count (2) */
	pData += sizeof(iTagCount);

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_ADR_NULL);				/* Indicate that the target is local */
	tag.iTag_length = 0;

	memcpy(pData, &tag, CPF_TAG_SIZE);
	pData += CPF_TAG_SIZE;

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_PKT_UCMM);				/* UCMM tag */
	
	/* If we have more than 2 tags and iPacketTagSize is set - calculate the tag length */
	/* based on the iPacketTagSize. Otherwise use iDataSize to find out the tag size. */
	if ( !gRequests[nRequest].iPacketTagSize || gRequests[nRequest].iTagCount == 2 )		
		tag.iTag_length = REPLY_HEADER_SIZE + gRequests[nRequest].iDataSize;	
	else
		tag.iTag_length = REPLY_HEADER_SIZE + gRequests[nRequest].iPacketTagSize;	
	
	ENCAP_CVT_HS(tag.iTag_length);
	
	memcpy( pData, &tag, CPF_TAG_SIZE );
	pData += CPF_TAG_SIZE;

	/* Add reply header that has the same service code as the UCMM request and uses 
	   REPLY_BIT_MASK to indicate a response */
	replyHdr.bService = gRequests[nRequest].bService | REPLY_BIT_MASK;	
	replyHdr.bIoiSize = 0;
	replyHdr.bGenStatus = gRequests[nRequest].bGeneralError;				/* 0 indicates a success */
	replyHdr.bObjStatusSize = gRequests[nRequest].bGeneralError ? 1 : 0;	/* If there is an error provide one word long extended error information */

	memcpy(pData, &replyHdr, REPLY_HEADER_SIZE);	
	pData += REPLY_HEADER_SIZE;

	memcpy( pData, MEM_PTR(gRequests[nRequest].iDataOffset), gRequests[nRequest].iDataSize );	/* Add UCMM data */
		
	socketEncapSendData( nSession );			
}

/*---------------------------------------------------------------------------
** ucmmSendServiceErrorResponse( )
**
** Send an error response to the UCMM or Class 3 request based on the service
** code and the error information
**---------------------------------------------------------------------------
*/

void ucmmSendServiceErrorResponse( INT32 nSession, UINT8 bService, UINT8 bError,
								  UINT16 iExtendedError )
{
	INT32 nRequest;

	nRequest = requestNew( ObjectRequest, FALSE, TRUE );		/* Schedule new UCMM request */
	
	if ( nRequest == ERROR_STATUS )						/* No more space for requests */
	{
		notifyEvent( NM_PENDING_REQUESTS_LIMIT_REACHED, 0 );
		return;		
	}

	gRequests[nRequest].lIPAddress = gSessions[nSession].sClientAddr.sin_addr.s_addr;
	gRequests[nRequest].bService = bService;
	gRequests[nRequest].bGeneralError = bError;
	gRequests[nRequest].iExtendedError = iExtendedError;
	
	ucmmSendUnconnectedObjectResponse( nRequest );
	requestRemove( nRequest );
}


/*---------------------------------------------------------------------------
** ucmmPassUnconnectedSendRequest( )
**
** Send Unconnected Send request
**---------------------------------------------------------------------------
*/
void ucmmPassUnconnectedSendRequest( INT32 nRequest )
{
	ENCAPH*        pHdr = ( ENCAPH * )gmsgBuf;
	CPF_TAG		   tag;
	UINT8*		   pData;	
//	UINT32		   lTotalLen = ENCAPH_SIZE;
	PDU_HDR        pdu_hdr;
	ENCAP_DT_HDR   dataHdr;	
	unsigned char  unconnectedSendPath[]  = { 0x52, 0x02, 0x20, 0x06, 0x24, 0x01 };
	INT32          nSession;
	UINT16         iExtendedPathSize = gRequests[nRequest].iExtendedPathSize;
	UINT16         iPDUSize = sizeof(UINT8) * 2;			/* Service Code + ioi size */;

	iPDUSize += (gRequests[nRequest].iClass != INVALID_CLASS) ? (IS_BYTE(gRequests[nRequest].iClass) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += (gRequests[nRequest].iInstance != INVALID_INSTANCE) ? (IS_BYTE(gRequests[nRequest].iInstance) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += (gRequests[nRequest].iAttribute != INVALID_ATTRIBUTE) ? sizeof(UINT16) : 0;
	iPDUSize += (gRequests[nRequest].iMember != INVALID_MEMBER) ? (IS_BYTE(gRequests[nRequest].iMember) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += gRequests[nRequest].iTagSize;

	nSession = sessionFindAddressEstablished( gRequests[nRequest].lIPAddress, Outgoing );

	if ( nSession == INVALID_SESSION )
	{
		DumpStr1("ucmmPassUnconnectedSendRequest Session terminated from under us: IP address 0x%x", gRequests[nRequest].lIPAddress);
		return;
	}

	if ( iExtendedPathSize % 2 )
		iExtendedPathSize++;
	
	memset( pHdr, 0, ENCAPH_SIZE );
		
	pHdr->iCommand = ENCAP_CMD_SEND_RRDATA;
	pHdr->lSession = gSessions[nSession].lSessionTag;
	pHdr->lStatus = 0;
	pHdr->lContext1 = gRequests[nRequest].lContext1;
	pHdr->lContext2 = gRequests[nRequest].lContext2;
	pHdr->iLength = ENCAP_DT_HDR_SIZE +				/* Required data header */
					   sizeof(UINT16) +				/* Tag (or object) count */
					   CPF_TAG_SIZE * 2 +			/* Two tag headers */
					   sizeof(unconnectedSendPath)+ /* Unconnected Send path segment */
					   sizeof(UINT16) +				/* Priority & tick time and number of ticks */
					   sizeof(UINT16) +				/* The size of the embedded UCMM message */
					   iPDUSize +					/* Service + ioi size + IOI */
					   gRequests[nRequest].iDataSize +	/* Data length */
					   sizeof(UINT16) +				/* Extended path length */
					   iExtendedPathSize;			/* Intermediate and final leg paths */

	if ( gRequests[nRequest].iDataSize % 2 )		/* Count the pad byte */
		pHdr->iLength++;
	
	pData = &gmsgBuf[ENCAPH_SIZE];					/* Store message in the gmsgBuf buffer */
	
	dataHdr.lTarget = ENCAP_TO_HL(INTERFACE_HANDLE);
	dataHdr.iTimeout = ENCAP_TO_HS(CONNECTION_TIMEOUT);
	
	memcpy( pData, &dataHdr, ENCAP_DT_HDR_SIZE );	/* Add data header */
	pData += ENCAP_DT_HDR_SIZE;

	UINT16_SET(pData, 2);							/* Tag count = 2 */
	pData += sizeof(UINT16);

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_ADR_NULL);				/* Indicates local target */
	tag.iTag_length = 0;

	memcpy(pData, &tag, CPF_TAG_SIZE);
	pData += CPF_TAG_SIZE;

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_PKT_UCMM);				/* UCMM tag header */
	tag.iTag_length = sizeof(unconnectedSendPath)+  /* Unconnected Send path segment */
					   sizeof(UINT16) +				/* Priority & tick time and number of ticks */
					   sizeof(UINT16) +				/* The size of the embedded UCMM message */
					   iPDUSize +					/* Service + ioi size + IOI */
					   gRequests[nRequest].iDataSize +	/* Data length */
					   sizeof(UINT16) +				/* Extended path length */
					   iExtendedPathSize;			/* Intermediate and final leg paths */

	if ( gRequests[nRequest].iDataSize % 2 )		/* Count the pad byte */
		tag.iTag_length++;

	ENCAP_CVT_HS(tag.iTag_length);

	memcpy( pData, &tag, CPF_TAG_SIZE );
	pData += CPF_TAG_SIZE;

	memcpy( pData, unconnectedSendPath, sizeof(unconnectedSendPath) );
	pData += sizeof(unconnectedSendPath);

	*pData++ = gRequests[nRequest].bOpenPriorityTickTime;
	*pData++ = gRequests[nRequest].bOpenTimeoutTicks;

	UINT16_SET(pData, iPDUSize + gRequests[nRequest].iDataSize);
	pData += sizeof(UINT16);

	pdu_hdr.bService = gRequests[nRequest].bService; 
	pdu_hdr.bSize = UINT8(( iPDUSize - sizeof(UINT16) ) / 2);    /* Path size in words */

	memcpy( pData, &pdu_hdr, PDU_HDR_SIZE );		/* Add Protocol Data Unit */
	pData += PDU_HDR_SIZE;

	if ( gRequests[nRequest].iClass != INVALID_CLASS )
	{
		if ( IS_BYTE(gRequests[nRequest].iClass) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_CLASS | LOGICAL_SEG_8_BIT; /* One byte class logical segment */
			*pData++ = (UINT8)gRequests[nRequest].iClass;
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_CLASS | LOGICAL_SEG_16_BIT; /* Two byte class logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gRequests[nRequest].iClass);
			pData += sizeof(UINT16);
		}
	}

	if ( gRequests[nRequest].iInstance != INVALID_INSTANCE )
	{
		if ( IS_BYTE(gRequests[nRequest].iInstance) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_INSTANCE | LOGICAL_SEG_8_BIT; /* One byte instance logical segment */
			*pData++ = (UINT8)gRequests[nRequest].iInstance;	
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_INSTANCE | LOGICAL_SEG_16_BIT; /* Two byte instance logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gRequests[nRequest].iInstance);	
			pData += sizeof(UINT16);
		}
	}

	if ( gRequests[nRequest].iAttribute != INVALID_ATTRIBUTE )
	{
		*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_ATTRIBUTE | LOGICAL_SEG_8_BIT;	/* One byte attribute logical segment */
		*pData++ = (UINT8)gRequests[nRequest].iAttribute;			
	}

	if ( gRequests[nRequest].iMember != INVALID_MEMBER )
	{
		if ( IS_BYTE(gRequests[nRequest].iMember) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_MEMBER | LOGICAL_SEG_8_BIT; /* One byte instance logical segment */
			*pData++ = (UINT8)gRequests[nRequest].iMember;	
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_MEMBER | LOGICAL_SEG_16_BIT; /* Two byte instance logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gRequests[nRequest].iMember);	
			pData += sizeof(UINT16);
		}
	}

	if ( gRequests[nRequest].iTagSize )
	{
		//*pData++ = EXT_SYMBOL_SEGMENT;
		//*pData++ = (UINT8)gRequests[nRequest].iTagSize;

		memcpy(pData, MEM_PTR(gRequests[nRequest].iTagOffset), gRequests[nRequest].iTagSize );
		pData += gRequests[nRequest].iTagSize;

		//if ( gRequests[nRequest].iTagSize % 2 ) /* add a pad byte */
		//	*pData++ = 0;
	}

	if ( gRequests[nRequest].iDataSize )
	{
		memcpy( pData, MEM_PTR(gRequests[nRequest].iDataOffset), gRequests[nRequest].iDataSize );		/* Add request data if provided */
		pData += gRequests[nRequest].iDataSize;

		if ( gRequests[nRequest].iDataSize % 2 )						/* Insert the pad byte if data length is odd */
			*pData++ = 0;
	}
		
	UINT16_SET(pData, iExtendedPathSize / 2);
	pData += sizeof(UINT16);

	memcpy( pData, MEM_PTR(gRequests[nRequest].iExtendedPathOffset), gRequests[nRequest].iExtendedPathSize);
	pData += gRequests[nRequest].iExtendedPathSize;

	if ( gRequests[nRequest].iExtendedPathSize % 2 )	/* Put the pad byte in if necessary */
		*pData = 0;
		
	socketEncapSendData( nSession );			
}
	
/*---------------------------------------------------------------------------
** ucmmPassUnconnectedSendRequestFinalLeg( )
**
** Send the final leg of the Unconnected Send request as a UCMM message
**---------------------------------------------------------------------------
*/
void ucmmPassUnconnectedSendRequestFinalLeg( INT32 nRequest )
{
	ENCAPH*            pHdr = ( ENCAPH * )gmsgBuf;
	CPF_TAG			   tag;
	UINT8*			   pData;	
//	UINT32			   lTotalLen = ENCAPH_SIZE;
	PDU_HDR            pdu_hdr;
	ENCAP_DT_HDR       dataHdr;
	INT32              nSession;
	UINT16			   iPDUSize = sizeof(UINT8) * 2;			/* Service Code + ioi size */;

	iPDUSize += (gRequests[nRequest].iClass != INVALID_CLASS) ? (IS_BYTE(gRequests[nRequest].iClass) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += (gRequests[nRequest].iInstance != INVALID_INSTANCE) ? (IS_BYTE(gRequests[nRequest].iInstance) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += (gRequests[nRequest].iAttribute != INVALID_ATTRIBUTE) ? sizeof(UINT16) : 0;
	iPDUSize += (gRequests[nRequest].iMember != INVALID_MEMBER) ? (IS_BYTE(gRequests[nRequest].iMember) ? sizeof(UINT16) : 2*sizeof(UINT16)) : 0;
	iPDUSize += gRequests[nRequest].iTagSize;
			
	nSession = sessionFindAddressEstablished( gRequests[nRequest].lIPAddress, Outgoing );

	if ( nSession == INVALID_SESSION )
	{
		DumpStr1("ucmmPassUnconnectedSendRequestFinalLeg Session terminated from under us: IP address 0x%x", gRequests[nRequest].lIPAddress);
		return;
	}
		
	memset( pHdr, 0, ENCAPH_SIZE );
		
	pHdr->iCommand = ENCAP_CMD_SEND_RRDATA;
	pHdr->lSession = gSessions[nSession].lSessionTag;
	pHdr->lStatus = 0;
	pHdr->lContext1 = gRequests[nRequest].lContext1;
	pHdr->lContext2 = gRequests[nRequest].lContext2;
	pHdr->iLength = ENCAP_DT_HDR_SIZE +				/* Required data header */
					   sizeof(UINT16) +				/* Tag (or object) count */
					   CPF_TAG_SIZE * 2 +			/* Two tag headers */
					   iPDUSize +					/* Service + ioi size + IOI */
					   gRequests[nRequest].iDataSize;	/* UCMM data request length */

	pData = &gmsgBuf[ENCAPH_SIZE];					/* Store message in the gmsgBuf buffer */
	
	dataHdr.lTarget = ENCAP_TO_HL(INTERFACE_HANDLE);
	dataHdr.iTimeout = ENCAP_TO_HS(CONNECTION_TIMEOUT);
	
	memcpy( pData, &dataHdr, ENCAP_DT_HDR_SIZE );	/* Add data header */
	pData += ENCAP_DT_HDR_SIZE;

	UINT16_SET(pData, 2);							/* Tag count = 2 */
	pData += sizeof(UINT16);

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_ADR_NULL);				/* Indicates local target */
	tag.iTag_length = 0;

	memcpy(pData, &tag, CPF_TAG_SIZE);
	pData += CPF_TAG_SIZE;

	tag.iTag_type = ENCAP_TO_HS(CPF_TAG_PKT_UCMM);				/* UCMM tag header */
	tag.iTag_length = ENCAP_TO_HS(iPDUSize + gRequests[nRequest].iDataSize);	

	memcpy( pData, &tag, CPF_TAG_SIZE );
	pData += CPF_TAG_SIZE;

	pdu_hdr.bService = gRequests[nRequest].bService; 
	pdu_hdr.bSize = UINT8(( iPDUSize - sizeof(UINT16) ) / 2);    /* Path size in words */

	memcpy( pData, &pdu_hdr, PDU_HDR_SIZE );		/* Add Protocol Data Unit */
	pData += PDU_HDR_SIZE;

	if ( gRequests[nRequest].iClass != INVALID_CLASS )
	{
		if ( IS_BYTE(gRequests[nRequest].iClass) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_CLASS | LOGICAL_SEG_8_BIT; /* One byte class logical segment */
			*pData++ = (UINT8)gRequests[nRequest].iClass;
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_CLASS | LOGICAL_SEG_16_BIT; /* Two byte class logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gRequests[nRequest].iClass);
			pData += sizeof(UINT16);
		}
	}

	if ( gRequests[nRequest].iInstance != INVALID_INSTANCE )
	{
		if ( IS_BYTE(gRequests[nRequest].iInstance) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_INSTANCE | LOGICAL_SEG_8_BIT; /* One byte instance logical segment */
			*pData++ = (UINT8)gRequests[nRequest].iInstance;	
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_INSTANCE | LOGICAL_SEG_16_BIT; /* Two byte instance logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gRequests[nRequest].iInstance);
			pData += sizeof(UINT16);
		}
	}

	if ( gRequests[nRequest].iAttribute != INVALID_ATTRIBUTE )
	{
		*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_ATTRIBUTE | LOGICAL_SEG_8_BIT;	/* One byte attribute logical segment */
		*pData++ = (UINT8)gRequests[nRequest].iAttribute;	
	}

	if ( gRequests[nRequest].iMember != INVALID_MEMBER )
	{
		if ( IS_BYTE(gRequests[nRequest].iMember) )
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_MEMBER | LOGICAL_SEG_8_BIT; /* One byte instance logical segment */
			*pData++ = (UINT8)gRequests[nRequest].iMember;	
		}
		else
		{
			*pData++ = LOGICAL_SEGMENT | LOGICAL_SEG_MEMBER | LOGICAL_SEG_16_BIT; /* Two byte instance logical segment */
			*pData++ = 0;	/* Insert a pad byte */
			UINT16_SET(pData, gRequests[nRequest].iMember);
			pData += sizeof(UINT16);
		}
	}

	if ( gRequests[nRequest].iTagSize )
	{
		//*pData++ = EXT_SYMBOL_SEGMENT;
		//*pData++ = (UINT8)gRequests[nRequest].iTagSize;

		memcpy(pData, MEM_PTR(gRequests[nRequest].iTagOffset), gRequests[nRequest].iTagSize );
		pData += gRequests[nRequest].iTagSize;

		//if ( gRequests[nRequest].iTagSize % 2 ) /* add a pad byte */
		//	*pData++ = 0;
	}

	if ( gRequests[nRequest].iDataSize )
		memcpy( pData, MEM_PTR(gRequests[nRequest].iDataOffset), gRequests[nRequest].iDataSize );		/* Add request data if provided */
		
	socketEncapSendData( nSession );			
}

