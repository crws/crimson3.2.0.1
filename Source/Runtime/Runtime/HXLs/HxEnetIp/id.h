/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** ID.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Identity object 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************/

#ifndef ID_H
#define ID_H


#define ID_CLASS						1			/* Identity class identifier */
#define ID_CLASS_REVISION				1			/* Identity class revision */

#define ID_PRODUCT_TYPE                 11			/* Scanner product type */

#define ID_MAJOR_REVISION               1			/* Scanner major revision */
#define ID_MINOR_REVISION               1			/* Scanner minor revision */
#define ID_SERIAL_NUMBER                0x504d4153	/* Scanner serial number */

#ifdef ET_IP_SCANNER

#define ID_VENDOR                       1			/* Scanner vendor ID */
#define ID_PRODUCT_CODE                 14			/* Scanner product code */

#define ID_PRODUCT_NAME                 "EtherNetIP Master Stack Library"	/* Scanner product name */
#define ID_PRODUCT_NAME_SIZE            32									/* Scanner product name length including terminating 0 */

#else /* #ifdef ET_IP_SCANNER */

#define ID_VENDOR                       1			/* Adapter vendor ID */
#define ID_PRODUCT_CODE                 15			/* Adapter product code */

#define ID_PRODUCT_NAME                 "EtherNetIP Adapter Library"		/* Adapter product name */
#define ID_PRODUCT_NAME_SIZE            27									/* Adapter product name length including terminating 0 */

#endif /*#ifdef ET_IP_SCANNER */

#define ID_ATTR_VENDOR_ID				1			/* Identity instance attributes */
#define ID_ATTR_DEVICE_TYPE				2
#define ID_ATTR_PRODUCT_CODE			3
#define ID_ATTR_REVISION				4
#define ID_ATTR_STATUS					5
#define ID_ATTR_SERIAL_NBR				6
#define ID_ATTR_PRODUCT_NAME			7

/*---------------------------------------------------------------------------
**
** General status bits.
**
**---------------------------------------------------------------------------
*/
#define ID_STATUS_OWNED                      0x0001
#define ID_STATUS_CONFIGURED                 0x0004

/*---------------------------------------------------------------------------
**
** Device state bits.
**
**---------------------------------------------------------------------------
*/
#define ID_STATUS_SELF_TESTING               0x0000
#define ID_STATUS_NVS_UPDATE                 0x0010
#define ID_STATUS_COMM_FAULT                 0x0020
#define ID_STATUS_AWAIT_CONN                 0x0030
#define ID_STATUS_NVS_BAD_CONFIG             0x0040
#define ID_STATUS_MAJOR_FAULT                0x0050
#define ID_STATUS_CONNECTED                  0x0060
#define ID_STATUS_IDLE                       0x0070


/*---------------------------------------------------------------------------
**
** Fault bits.
**
**---------------------------------------------------------------------------
*/
#define ID_STATUS_MINOR_RECOVERABLE_FAULT    0x0100
#define ID_STATUS_MINOR_UNRECOVERABLE_FAULT  0x0200
#define ID_STATUS_MAJOR_RECOVERABLE_FAULT    0x0400
#define ID_STATUS_MAJOR_UNRECOVERABLE_FAULT  0x0800


/* Structure to send when GetAttributeAll service for Identity class is received */
typedef struct tagID_CLASS_ATTRIBUTE
{
   UINT16  iRevision;
   UINT16  iMaxInstance;
   UINT16  iMaxClassAttr;
   UINT16  iMaxInstanceAttr;
}
ID_CLASS_ATTRIBUTE;

/* Structure to send when GetAttributeAll service for Identity instance is received */
typedef struct tagID_INSTANCE_ATTRIBUTE
{
   UINT16  iVendor;
   UINT16  iProductType;
   UINT16  iProductCode;
   UINT8   bMajorRevision;
   UINT8   bMinorRevision;
   UINT16  iStatus;
   UINT32  lSerialNumber;
}
ID_INSTANCE_ATTRIBUTE;

#define ID_INSTANCE_ATTRIBUTE_SIZE	14

extern EtIPIdentityInfo gIDInfo;

extern void idInit();
extern void idParseClassInstanceRequest( INT32 nRequest );
extern void idParseClassRequest( INT32 nRequest );
extern void idParseInstanceRequest( INT32 nRequest );
extern void idSendClassAttrAll( INT32 nRequest );
extern void idSendInstanceAttrAll( INT32 nRequest );
extern void idSendInstanceAttrSingle( INT32 nRequest );
extern void idReset( INT32 nRequest );
extern UINT16 idGetStatus();


#endif /* #ifndef ID_H */
