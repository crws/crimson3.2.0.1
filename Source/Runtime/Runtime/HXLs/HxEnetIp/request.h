/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** REQUEST.h
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Request array is used for scheduling outgoing object requests
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#ifndef REQUEST_H
#define REQUEST_H


#define MAX_PACKET_SIZE  2048		/* Max packet length */

#define MAX_EXTENDED_PATH_SIZE	128 /* Max remaining path size in the Unconnected Send message */

#define INVALID_REQUEST				(-1)

typedef enum tagRequestState
{
	RequestLogged							= 1,	/* Request has just arrived */
    RequestWaitingForSession				= 2,	/* Waiting for the the TCP connection to be established to the target */	
	RequestWaitingForResponse				= 3,	/* Waiting for the response	*/
	RequestResponseReceived					= 4,	/* Response received	*/
}
RequestState;

typedef enum tagRequestType
{
	ObjectRequest,			/* For UCMM or Class3 requests */
	UnconnectedSendRequest,		/* For Unconnected Sends */
	KillRequest			/* Kill Request */
}
RequestType;

/* Structure used to pass around UCMM or Class3 information when parsing the request */
typedef struct tagREQUEST
{
	INT32    nType;						/* From the RequestType enumeration */
	INT32  nIndex;						/* Unique request identifier */
	UINT32 lRequestTimeoutTick;			/* When the request should time out on the lack of response */
	INT32  nState;						/* From the RequestState enumeration */
	BOOL   bIncoming;					/* Request can be originated by another device (incoming) or by the client application (outgoing) */
	BOOL   bDeleteOnSend;				/* Whether the request should be immediately deleted on send */
	UINT32 lIPAddress;					/* IP address of the target device for the outgoing request */
	UINT32 lSessionID;					/* Session used to transfer the request and the response */
	UINT32 lFromIPAddress;					/* IP address of the originator of the Unconnected Send */
	UINT8  bOpenPriorityTickTime;			/* Priority bit is bit 4. Low nibble is the number of milliseconds per tick */
    UINT8  bOpenTimeoutTicks;				/* Number of ticks to timeout */
	UINT16 iExtendedPathOffset;				/* Extended path describe the path for the target bridge to use to pass forward the request */
	UINT16 iExtendedPathSize;				/* Size of the extended path in bytes */
	UINT8  bService;					/* Service code */
	UINT16 iClass;						/* Request Class number */
	UINT16 iInstance;					/* Request Instance number*/
	UINT16 iAttribute;					/* Request Attribute */
	UINT16 iMember;						/* Request Member */
	UINT16 iTagOffset;					/* Request symbolic string Tag */
	UINT16 iTagSize;					/* Tag size in bytes */
	UINT16 iDataOffset;					/* Request and response data */
	UINT16 iDataSize;					/* Data size in bytes */
	UINT16 iConnectionSerialNbr;		/* Connection serial number for the Class 3 responses */
	UINT16 iConnSequence;				/* Connection sequence for the Class 3 responses */
	UINT32 lContext1;					/* Sender Context information */
	UINT32 lContext2;					/* Sender Context information */
	UINT8  bGeneralError;				/* General request status. 0 indicates a success */
	UINT16 iExtendedError;				/* Extended request status. Used only if General Status is not equal to 0 */
	UINT16 iTagCount;					/* Number of tags in the data member */
	UINT16 iPacketTagSize;				/* Packet tag size in bytes */
}
REQUEST;

#define MAX_REQUESTS	100					/* Maximum number of outstanding requests */

#define INDEX_END		0x7fffffff			/* Should wrap at this point */


extern REQUEST gRequests[/*MAX_REQUESTS*/];		/* Requests array */
extern INT32   gnRequests;					/* Number of outstanding requests */
extern INT32   gnIndex;						/* Index to be assigned to the next request */
extern UINT32  glConfigTickChanged;			/* Last time configuration changed */

extern void  requestInit();
extern void  requestInitialize( INT32 nRequest );
extern INT32 requestNew( INT32 nType, BOOL bIncoming, BOOL bDeleteOnSend );
extern INT32 requestInitializeFromObjectRequest( INT32 nRequest, EtIPObjectRequest* pRequest );
extern INT32 requestNewUCMM( const UINT8* szIPAddr, EtIPObjectRequest* pRequest );
extern INT32 requestNewUnconnectedSend( UINT8* szIPAddr, EtIPObjectRequest* pRequest, 
			char* extendedPath, UINT16 iExtendedPathLen);
extern void  requestRemove( INT32 nRequest );
extern void  requestRemoveAll( );
extern void requestSend( INT32 nRequest );
extern void requestService( INT32 nRequest );
extern void requestResponseReceived( INT32 nRequest );
extern INT32 requestGetByRequestId( INT32 nRequestId );

#endif /* #ifndef REQUEST_H */
