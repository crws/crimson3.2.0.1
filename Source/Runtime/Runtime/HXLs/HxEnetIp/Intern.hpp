
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Intern_HPP

#define INCLUDE_Intern_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include "StdEnv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Broker
//

extern IObjectBroker * piob;

//////////////////////////////////////////////////////////////////////////
//
// Custom Macros
//

#define __declspec(x)	/**/

#ifndef CALLBACK

#define CALLBACK	/**/

#endif

//////////////////////////////////////////////////////////////////////////
//
// Tuning Constants
//

#define ET_IP_CLIENT

#define ASYNCHRONOUS_CONNECTION

#undef  TRACE_OUTPUT

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "Platform.h"
#include "Socket.h"
#include "etipadapter.h"
#include "id.h"
#include "io.h"
#include "assembly.h"
#include "enetlink.h"
#include "notify.h"
#include "outpoint.h"
#include "port.h"
#include "request.h"
#include "tcpip.h"
#include "trace.h"
#include "ucmm.h"
#include "util.h"
#include "router.h"
#include "connect.h"
#include "client.h"
#include "connmgr.h"
#include "session.h"
#include "datatbl.h"

//////////////////////////////////////////////////////////////////////////
//
// Overrides
//

#undef	MAX_SESSIONS			/**/
#define MAX_SESSIONS			32
#undef	MEMORY_POOL_SIZE		/**/
#define MEMORY_POOL_SIZE		16384
#undef	MAX_CONNECTIONS			/**/
#define	MAX_CONNECTIONS			16
#undef	MAX_REQUESTS			/**/	
#define MAX_REQUESTS			32
#undef	ASSEMBLY_SIZE			/**/
#define ASSEMBLY_SIZE			4096
#undef  NUM_CONNECTION_GROUPS		/**/
#define NUM_CONNECTION_GROUPS		MAX_CONNECTIONS
#undef  GET_CONNECTION_GROUP		/**/
#define GET_CONNECTION_GROUP(c)		(gConnections[c].cfg.nInstance - 1)
#undef  ERROR_STATUS			/**/
#define ERROR_STATUS			UINT32(-1)

// End of File

#endif
