
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ThreadPool_HPP

#define INCLUDE_ThreadPool_HPP

//////////////////////////////////////////////////////////////////////////
//
// Entry Points
//

typedef DWORD (*PTHREAD)(PVOID);

//////////////////////////////////////////////////////////////////////////
//
// ThreadPool
//

class CThreadPool 
{
	public:
		// Constructor
		CThreadPool(void);

		// Destructor
		~CThreadPool(void);

		// Management
		BOOL Open(UINT uPriority, UINT uCount);
		void Close(void);
		BOOL CreateThread(PTHREAD pFunc, PVOID Data);
		void ClaimAll(void);

	protected:
		// Custom Types
		struct CWorker 
		{
			UINT      m_uID;
			IThread	* m_pThread;
			IEvent  * m_pRun;
			PTHREAD	  m_pfFunc;
			PVOID	  m_pvData;
			};

		// Data
		ISemaphore * m_pLimit;
		IMutex     * m_pMutex;
		CWorker    * m_pWorker;
		UINT         m_uCount;

		// Entry Point
		void TaskEntry(UINT uThread);

		// Entry Point
		static int TaskThreadPool(IThread *pThread, void *pParam, UINT uParam); 
	};

// End of File

#endif
