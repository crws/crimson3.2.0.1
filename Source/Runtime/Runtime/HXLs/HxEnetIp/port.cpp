/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** PORT.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Port object implementation 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

/*---------------------------------------------------------------------------
** portParseClassInstanceRequest( )
**
** Determine if it's request for the Class or for the particular instance 
**---------------------------------------------------------------------------
*/

void portParseClassInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 0 )
		portParseClassRequest( nRequest );
	else
		portParseInstanceRequest( nRequest );
}

/*---------------------------------------------------------------------------
** portParseClassRequest( )
**
** Respond to the Port class request
**---------------------------------------------------------------------------
*/

void portParseClassRequest( INT32 nRequest )
{
	switch( gRequests[nRequest].bService )
	{
		case SVC_GET_ATTR_ALL:
			portSendClassAttrAll( nRequest );
			break;
		case SVC_GET_ATTR_SINGLE:
			portSendClassAttrSingle( nRequest );
			break;
		default:		
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
			break;
	}
}

/*---------------------------------------------------------------------------
** portSendClassAttrAll( )
**
** GetAttributeAll service for Port object class is received 
**---------------------------------------------------------------------------
*/

void portSendClassAttrAll( INT32 nRequest )
{
	PORT_CLASS_ATTR attr;

	attr.iRevision = ENCAP_TO_HS(PORT_CLASS_REVISION);
	attr.iMaxInstance = ENCAP_TO_HS(1);
	attr.iNumInstances = ENCAP_TO_HS(1);
	attr.iEntryPort = ENCAP_TO_HS(1);
	attr.lPadBytes = 0;
	attr.iPortType = ENCAP_TO_HS(TCP_IP_PORT_TYPE);
	attr.iPortNumber = ENCAP_TO_HS(TCP_IP_PORT_NUMBER);

	utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&attr, PORT_CLASS_ATTR_SIZE );		
}

/*---------------------------------------------------------------------------
** portSendClassAttrSingle( )
**
** GetAttributeSingle service for Port object class is received 
**---------------------------------------------------------------------------
*/

void portSendClassAttrSingle( INT32 nRequest )
{	
	UINT16 iVal;
	UINT16 iArray[4];
	
	switch( gRequests[nRequest].iAttribute )
	{
		case PORT_CLASS_ATTR_REVISION:
			iVal = ENCAP_TO_HS(PORT_CLASS_REVISION);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case PORT_CLASS_ATTR_MAX_INSTANCE:
			iVal = ENCAP_TO_HS(1);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case PORT_CLASS_ATTR_NUM_INSTANCES:
			iVal = ENCAP_TO_HS(1);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case PORT_CLASS_ATTR_ENTRY_PORT:
			iVal = ENCAP_TO_HS(1);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case PORT_CLASS_ATTR_ALL_PORTS:
			iArray[0] = iArray[1] = 0;
			iArray[2] = ENCAP_TO_HS(TCP_IP_PORT_TYPE);
			iArray[3] = ENCAP_TO_HS(TCP_IP_PORT_NUMBER);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)iArray, sizeof(UINT16)*4 );		
			break;
		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}		
}


/*---------------------------------------------------------------------------
** portParseInstanceRequest( )
**
** Respond to the Port object instance request
**---------------------------------------------------------------------------
*/

void portParseInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 1 )
	{
		switch( gRequests[nRequest].bService )
		{
			case SVC_GET_ATTR_ALL:
				portSendInstanceAttrAll( nRequest );
				break;
			case SVC_GET_ATTR_SINGLE:			
				portSendInstanceAttrSingle( nRequest );
				break;		
			default:
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
				break;
		}
	}
	else
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;
}


/*---------------------------------------------------------------------------
** portSendInstanceAttrAll( )
**
** GetAttributeAll service for Port instance is received
**---------------------------------------------------------------------------
*/

void portSendInstanceAttrAll( INT32 nRequest )
{
	UINT8* pData;
		
	utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, NULL, 10 );		

	if ( gRequests[nRequest].iDataSize && gRequests[nRequest].iDataOffset == INVALID_MEMORY_OFFSET )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_NO_RESOURCE;	/* Out of memory */	
		return;
	}
	
	pData = MEM_PTR( gRequests[nRequest].iDataOffset );
		
	UINT16_SET( pData, TCP_IP_PORT_TYPE );
	pData += sizeof(UINT16);
	
	UINT16_SET( pData, TCP_IP_PORT_NUMBER );
	pData += sizeof(UINT16);

	UINT16_SET( pData, 2 );
	pData += sizeof(UINT16);

	*pData++ = 0x12;
	*pData++ = 0x02;

	UINT16_SET( pData, 0 );
}


/*--------------------------------------------------------------------------------
** portSendInstanceAttrSingle( )
**
** GetAttributeSingle request has been received for the Port object instance
**--------------------------------------------------------------------------------
*/

void portSendInstanceAttrSingle( INT32 nRequest )
{	
	UINT16 iVal;
	UINT8  bVal[4];
	
	switch( gRequests[nRequest].iAttribute )
	{
		case PORT_INST_ATTR_PORT_TYPE:
			iVal = ENCAP_TO_HS(TCP_IP_PORT_TYPE);			
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;

		case PORT_INST_ATTR_PORT_NUMBER:
			iVal = ENCAP_TO_HS(TCP_IP_PORT_NUMBER);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		
		case PORT_INST_ATTR_PORT_OBJECT:			
			bVal[0] = 2;
			bVal[1] = 0;
			bVal[2] = 0x12;
			bVal[3] = 0x02;
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, bVal, 4 );		
			break;		

		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}	
}

