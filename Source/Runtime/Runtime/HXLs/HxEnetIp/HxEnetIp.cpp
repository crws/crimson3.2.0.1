
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern IEtherNetIp * Create_EtherNetIp(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

void Register_HxEnetIp(void)
{
	piob->RegisterSingleton("comms.enetip", 0, Create_EtherNetIp());
	}

void Revoke_HxEnetIp(void)
{
	piob->RevokeGroup("comms.enetip");
	}

// End of File
