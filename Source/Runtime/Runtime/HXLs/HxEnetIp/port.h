/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** PORT.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Port object 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************/


#ifndef PORT_H
#define PORT_H


#define PORT_CLASS					0xF4			/* Port object class identifier */
#define PORT_CLASS_REVISION			1				/* Port object class revision */

#define TCP_IP_PORT_TYPE			4
#define TCP_IP_PORT_NUMBER			2

/* Class and instance attribute numbers */
#define PORT_CLASS_ATTR_REVISION              1
#define PORT_CLASS_ATTR_MAX_INSTANCE          2
#define PORT_CLASS_ATTR_NUM_INSTANCES         3
#define PORT_CLASS_ATTR_ENTRY_PORT			  8
#define PORT_CLASS_ATTR_ALL_PORTS			  9

#define PORT_INST_ATTR_PORT_TYPE			  1
#define PORT_INST_ATTR_PORT_NUMBER			  2
#define PORT_INST_ATTR_PORT_OBJECT		      3


/* Class attribute structure */
typedef struct tagPORT_CLASS_ATTR
{
   UINT16  iRevision;
   UINT16  iMaxInstance;
   UINT16  iNumInstances;
   UINT16  iEntryPort;
   UINT32  lPadBytes;
   UINT16  iPortType;
   UINT16  iPortNumber;
}
PORT_CLASS_ATTR;

#define PORT_CLASS_ATTR_SIZE	16

extern void portParseClassInstanceRequest( INT32 nRequest );
extern void portParseClassRequest( INT32 nRequest );
extern void portSendClassAttrAll( INT32 nRequest );
extern void portSendClassAttrSingle( INT32 nRequest );
extern void portParseInstanceRequest( INT32 nRequest );
extern void portSendInstanceAttrAll( INT32 nRequest );
extern void portSendInstanceAttrSingle( INT32 nRequest );

#endif /* #ifndef PORT_H */

