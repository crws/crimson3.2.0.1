
#include "Intern.hpp"

#include "Implicit.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Implicit
//

// Instantiator

global IUnknown * Create_Implicit(PCSTR pName)
{
	CImplicit *p = New CImplicit;

	return p;
	}

// Constructors

CImplicit::CImplicit(void)
{
	StdSetRef();
	}

// Destructor

CImplicit::~CImplicit(void)
{
	}

// Binding

void CImplicit::Bind(void)
{
	ConnectBind();
	}

// IUnknown

HRESULT CImplicit::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IImplicit);

	StdQueryInterface(IImplicit);

	return E_NOINTERFACE;
	}

ULONG CImplicit::AddRef(void)
{
	StdAddRef();
	}

ULONG CImplicit::Release(void)
{
	StdRelease();
	}

// IExplicit

BOOL METHOD CImplicit::GetFirstConsumer(IConsumer * &pConsumer)
{
	pConsumer = NULL;

	return GetNextConsumer(pConsumer);
	}

BOOL METHOD CImplicit::GetNextConsumer (IConsumer * &pConsumer)
{
	UINT uIndex = pConsumer ? ((CConsumer *) pConsumer)->GetIndex() + 1: 0;

	while( uIndex < elements(m_Consumer) ) {

		if( m_Consumer[ uIndex ].IsActive() ) {
		
			pConsumer = &m_Consumer[ uIndex ];

			return TRUE;
			}

		uIndex ++;
		}
	
	pConsumer = NULL;
	
	return FALSE;
	}

BOOL METHOD CImplicit::GetFirstProducer(IProducer * &pProducer)
{
	pProducer = NULL;
	
	return GetNextProducer(pProducer);
	}

BOOL METHOD CImplicit::GetNextProducer (IProducer * &pProducer)
{
	UINT uIndex = pProducer ? ((CProducer *) pProducer)->GetIndex() + 1: 0;

	while( uIndex < elements(m_Consumer) ) {

		if( m_Producer[ uIndex ].IsActive() ) {
		
			pProducer = &m_Producer[ uIndex ];

			return TRUE;
			}

		uIndex ++;
		}
	
	pProducer = NULL;
	
	return FALSE;
	}

UINT METHOD CImplicit::Send(DWORD dwInst, PBYTE pData, UINT uSize)
{
	GuardThread(TRUE);

	UINT uResult = clientSetConnectionOutputData(dwInst, pData, uSize);

	GuardThread(FALSE);

	return uResult;
	}

UINT METHOD CImplicit::Recv(DWORD dwInst, PBYTE pData, UINT uSize)
{
	GuardThread(TRUE);

	UINT uResult = clientGetConnectionInputData(dwInst, pData, uSize);

	GuardThread(FALSE);

	return uResult;
	}

// Entry Point

void CImplicit::OnEvent(INT32 nEvent, INT32 nParam)
{
	switch( nEvent ) {

		case NM_CONNECTION_ESTABLISHED:

			OnNewConnection(nParam);

			break;

		case NM_CONNECTION_CLOSED:

			OnConnectionClosed(nParam);

			break;

		case NM_CONNECTION_NEW_INPUT_DATA:
			
			OnConnectionData(nParam);

			break;
		}
	}

// Event Handlers

void CImplicit::OnNewConnection(INT32 nInstance)
{
	EtIPConnectionConfig *pCfg = new EtIPConnectionConfig;
	
	if( !clientGetConnectionConfig(nInstance, pCfg) ) {

		if( pCfg->iInputDataOffset != INVALID_OFFSET && pCfg->iInputDataSize ) {
		
			#ifdef _XDEBUG

			AfxTrace( "Input : %d : Offset %d : Size %d : Target Inst %d\n", 			
				  pCfg->iConsumingConnPoint,
				  pCfg->iInputDataOffset,
				  pCfg->iInputDataSize,
				  pCfg->iConfigConnInstance
				  );
			
			#endif

			CConsumer *pConsumer;

			if( ConnectFindFree(pConsumer) ) {

				pConsumer->SetIdent(nInstance, pCfg->iConsumingConnPoint);

				pConsumer->SetMapping(pCfg->iInputDataOffset, pCfg->iInputDataSize);
				}
			}

		if( pCfg->iOutputDataOffset != INVALID_OFFSET && pCfg->iOutputDataSize ) {
		
			#ifdef _XDEBUG

			AfxTrace( "Output : %d : Offset %d : Size %d\n", 			
			          pCfg->iProducingConnPoint,
				  pCfg->iOutputDataOffset,
				  pCfg->iOutputDataSize
				  );

			#endif

			CProducer *pProducer;
			
			if( ConnectFindFree(pProducer) ) {

				pProducer->SetIdent(nInstance, pCfg->iProducingConnPoint);

				pProducer->SetMapping(pCfg->iOutputDataOffset, pCfg->iOutputDataSize);
				}
			}
		}
	
	delete pCfg;

	Dump();
	}

void CImplicit::OnConnectionClosed(INT32 nInstance)
{
	for( UINT i = 0; i < elements(m_Consumer); i ++ ) {

		if( m_Consumer[i].GetInstance() == DWORD(nInstance) ) {

			ConnectFree(m_Consumer[i]);
			}
		}

	for( UINT i = 0; i < elements(m_Producer); i ++ ) {

		if( m_Producer[i].GetInstance() == DWORD(nInstance) ) {

			ConnectFree(m_Producer[i]);
			}
		}
	Dump();
	}

void CImplicit::OnConnectionData(INT32 nInstance)
{
	for( UINT i = 0; i < elements(m_Consumer); i ++ ) {

		if( m_Consumer[i].GetInstance() == DWORD(nInstance) ) {

			m_Consumer[i].SetNewData();
			}
		}

	Dump();	
	}

// Connections

void CImplicit::ConnectBind(void)
{
	for( UINT i = 0; i < elements(m_Producer); i ++ ) {
		
		m_Producer[i].Bind(i, this);
		}

	for( UINT i = 0; i < elements(m_Consumer); i ++ ) {
		
		m_Consumer[i].Bind(i, this);
		}
	}

BOOL CImplicit::ConnectFindFree(CConsumer * &pConsumer)
{
	for( UINT i = 0; i < elements(m_Consumer); i ++ ) {

		if( m_Consumer[ i ].IsFree() ) {
			
			m_Consumer[i].Create();
			
			pConsumer = &m_Consumer[i];

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CImplicit::ConnectFindFree(CProducer * &pProducer)
{
	for( UINT i = 0; i < elements(m_Producer); i ++ ) {

		if( m_Producer[ i ].IsFree() ) {
			
			m_Producer[i].Create();
			
			pProducer = &m_Producer[i];

			return TRUE;
			}
		}

	return FALSE;
	}

void CImplicit::ConnectFree(CConsumer &Consumer)
{
	Consumer.Close();
	}

void CImplicit::ConnectFree(CProducer &Producer)
{
	Producer.Close();
	}

// Debug

void CImplicit::Dump(void)
{
	#ifdef _XDEBUG

	AfxTrace("Dump Consumers\n");
		
	IConsumer *pConsumer = NULL;

	for(;;) {

		if( GetNextConsumer(pConsumer) ) {

			CConsumer *p = (CConsumer *) pConsumer;

			AfxTrace( "Index %d :"
				  "Inst %d :"
				  "Ident %d :" 
				  "Count %d:"
				  "Dirty %d\n",
				  p->GetIndex(),
				  p->GetInstance(),
				  p->GetIdent(),
				  p->GetSize(),
				  p->HasNewData()
				  );

			continue;
			}

		break;
		}
	
	AfxTrace("Dump Producers\n");
		
	IProducer *pProducer = NULL;

	for(;;) {

		if( GetNextProducer(pProducer) ) {

			CProducer *p = (CProducer *) pProducer;

			AfxTrace( "Index %d :"
				  "Inst %d :"
				  "Ident %d :" 
				  "Count %d:"
				  "\n",
				  p->GetIndex(),
				  p->GetInstance(),
				  p->GetIdent(),
				  p->GetSize()
				  );

			continue;
			}

		break;
		}
	
	#endif
	}

// End of File
