
#include "Intern.hpp"

#include "Producer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Producer
//

// Instantiator

global IUnknown * Create_Producer(PCSTR pName)
{
	CProducer *p = New CProducer;

	return p;
	}

// Constructors

CProducer::CProducer(void)
{
	StdSetRef();

	m_pAssy = NULL;

	m_fUsed = FALSE;
	}

// Destructor

CProducer::~CProducer(void)
{
	}

// Binding

void CProducer::Bind(UINT uIndex, IImplicit *pAssembly)
{
	m_pAssy  = pAssembly;

	m_uIndex = uIndex;
	}

// Mapping

void CProducer::SetIdent(DWORD dwInstance, DWORD dwIdent)
{
	m_dwInst  = dwInstance;

	m_dwIdent = dwIdent;
	}

void CProducer::SetMapping(WORD wOffset, WORD wSize)
{
	m_wOffset = wOffset;

	m_wSize   = wSize; 
	}

// Attributes

DWORD CProducer::GetInstance(void) const
{
	return m_dwInst;
	}

UINT CProducer::GetIndex(void) const
{
	return m_uIndex;
	}

BOOL CProducer::IsFree(void) const
{
	return !m_fUsed;
	}

// Operations

void CProducer::Create(void)
{
	m_dwInst  = INVALID_INSTANCE;

	m_dwIdent = INVALID_CONN_POINT;

	m_wSize   = 0;

	m_wOffset = 0;

	m_fUsed   = TRUE;
	}

void CProducer::Close(void)
{
	m_fUsed = FALSE;
	}

// IUnknown

HRESULT CProducer::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IProducer);

	StdQueryInterface(IProducer);

	return E_NOINTERFACE;
	}

ULONG CProducer::AddRef(void)
{
	StdAddRef();
	}

ULONG CProducer::Release(void)
{
	StdRelease();
	}

// IProducer

DWORD METHOD CProducer::GetIdent(void)
{
	return m_dwIdent;
	}

UINT METHOD CProducer::GetSize(void)
{
	return m_wSize;
	}

BOOL METHOD CProducer::IsActive(void)
{
	if( !m_pAssy || !m_fUsed ) {

		return FALSE;
		}

	if( m_dwInst == INVALID_INSTANCE || m_dwIdent == INVALID_CONN_POINT ) {

		return FALSE;
		}

	return TRUE;
	}

UINT METHOD CProducer::Send(PBYTE pData, UINT uSize)
{
	return IsActive() ? m_pAssy->Send(m_dwInst, pData, uSize) : 0; 
	}

// End of File
