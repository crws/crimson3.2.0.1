
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Data
//

global	INT32		nHostAddrCount;

global	sockaddr_in	gHostAddr[4];

global	sockaddr_in	gMulticastBaseAddr;

global	UINT8		gmsgBuf[MAX_PACKET_SIZE];

static	PSOCK		pSockTCP;

static	PSOCK		pSockUDP;

global	UINT32		glClass1Socket[NUM_CONNECTION_GROUPS];

static	BOOL		fStopping;

////////////////////////////////////////////////////////////////////////
//
// Prototypes
//

static INT32 socketEncapSend(INT32 nSession, UINT uLen);
static INT32 socketClass1Send(INT32 nConnection, UINT uLen);
static BOOL  socketCheck(PSOCK &pSock);
static void  socketInitTargetData(void);
static void  socketGetMcastBaseAddr(IPREF IP, IPREF Mask);

////////////////////////////////////////////////////////////////////////
//
// Code
//

global void socketInit(void)
{
	fStopping = FALSE;
	
	platformSocketLibInit();

	socketInitTargetData();

	memset(glClass1Socket, 0, sizeof(glClass1Socket));

	pSockUDP = (PSOCK) socketClass1Init(TRUE, SMALL_BUFFER_SIZE);	
	
	pSockTCP = NULL;
	}

global void socketCleanup(void)
{		
	fStopping = TRUE;
	
	Sleep(5000);

	platformCloseSocket((UINT32 &)pSockUDP, TRUE);

	platformCloseSocket((UINT32 &)pSockTCP, TRUE);
	
	for( UINT iGroup = 0; iGroup < NUM_CONNECTION_GROUPS; iGroup ++ ) {

		platformCloseSocket(glClass1Socket[iGroup], TRUE);
		}

	platformSocketLibCleanup();
	}

global BOOL socketCheckIncomingSession()
{
	if( fStopping ) {

		return FALSE;
		}
	
	if( !pSockTCP ) {

		if( gnSessions >= MAX_SESSIONS ) {

			return FALSE;
			}

		pSockTCP = (ISocket *) platformCreateSocket(IP_TCP);

		if( !pSockTCP ) {

			return FALSE;
			}

		pSockTCP->Listen(ENCAP_SERVER_PORT);
		}

	if( socketCheck(pSockTCP) ) {

		CIpAddr IP;

		pSockTCP->GetRemote(IP);

		INT32 nIndex = sessionNew((DWORD &)IP, TRUE);

		if( nIndex != INVALID_SESSION ) {

			SESSION &ss = gSessions[nIndex];

			ss.lSocket			= DWORD(pSockTCP);

			ss.sClientAddr.sin_addr.s_un_b	= IP;

			pSockTCP = NULL;

			return TRUE;
			}

		platformCloseSocket((UINT32 &)pSockTCP, FALSE);

		return FALSE;
		}

	return FALSE;
	}

global BOOL socketStartTCPConnection(INT32 nSession, PBOOL pfDone)                 
{	
	*pfDone = FALSE;

	SESSION &ss    = gSessions[nSession];

	ISocket *pSock = (ISocket *) platformCreateSocket(IP_TCP);

	if( pSock ) {

		IPADDR &IP   = ss.sClientAddr.sin_addr.s_un_b;

		WORD   wPort = ntohs(ss.sClientAddr.sin_port);

		pSock->Connect(IP, wPort);

		ss.lSocket = UINT32(pSock);	
		
		return TRUE;
		}

	notifyEvent(NM_ERROR_USING_WINSOCK, 0);	    		

	return FALSE;
	}

global BOOL socketContinueTCPConnection(INT32 nSession, PBOOL pfDone) 
{
	PSOCK &pSock = (PSOCK &) gSessions[nSession].lSocket;

	UINT Phase;

	pSock->GetPhase(Phase);

	if( Phase == PHASE_OPENING ) {

		*pfDone = FALSE;

		return TRUE;
		}
	
	if( Phase == PHASE_OPEN ) {

		*pfDone = TRUE;

		return TRUE;
		}

	if( Phase == PHASE_ERROR ) {

		platformCloseSocket((UINT32 &)pSock, TRUE);

		return FALSE;
		}

	if( Phase == PHASE_CLOSING ) {

		platformCloseSocket((UINT32 &)pSock, FALSE);

		return FALSE;
		}

	return FALSE;
	}

global BOOL socketKillTCPConnection(INT32 nSession)
{
	PSOCK &pSock = (PSOCK &) gSessions[nSession].lSocket;

	if( !pSock ) {

		return FALSE;
	}

	pSock->Abort();

	return TRUE;
	}

global PLATFORM_THREAD_RET PLATFORM_THREAD_MOD socketTcpConnect(void *pParam)
{
	// NON ASYNC
	
	return 0;
	}

global INT32 socketEncapRecv(INT32 nSession)
{	
	SESSION &ss = gSessions[nSession];
	
	PSOCK &pSock = (PSOCK &) gSessions[nSession].lSocket;

	if( !pSock ) {

		return ERROR_STATUS;
		}

	UINT32 uTime    = platformGetTickCount();

	UINT32 uRxCount = 0;

	ENCAPH *pHdr    = (ENCAPH *) gmsgBuf;

	memset(pHdr, 0, ENCAPH_SIZE);

	if( ss.iPartialRecvPacketOffset != INVALID_MEMORY_OFFSET ) {

		if( IS_TICK_GREATER(uTime, ss.lPartialRecvPacketTimeoutTick) ) {
			
			utilRemoveFromMemoryPool( &ss.iPartialRecvPacketOffset, 
						  &ss.iPartialRecvPacketSize
						  );
			}
		else {
			uRxCount = ss.iPartialRecvPacketSize;

			memcpy(gmsgBuf, MEM_PTR(ss.iPartialRecvPacketOffset), uRxCount);
			}
		}

	while( uRxCount < ENCAPH_SIZE ) {

		if( !socketCheck(pSock) ) {

			sessionRemove(nSession, TRUE);
				
			return ERROR_STATUS;
			}

		UINT uCount = ENCAPH_SIZE - uRxCount;

		if( pSock->Recv(gmsgBuf + uRxCount, uCount) == S_OK ) {

			uRxCount += uCount;

			continue;
			}
		
		if( uRxCount && uRxCount > ss.iPartialRecvPacketSize ) {

			utilResetMemoryPoolOffset( &ss.iPartialRecvPacketOffset, 
						   &ss.iPartialRecvPacketSize, 
						   gmsgBuf, 
						   (UINT16)uRxCount
						   );
			
			ss.lPartialRecvPacketTimeoutTick = uTime + PARTIAL_PACKET_TIMEOUT;
			}
		
		return ERROR_STATUS;
		}

	////////
		
	ENCAP_CVT_HS (pHdr->iLength);
	ENCAP_CVT_HS (pHdr->iCommand);
	ENCAP_CVT_HL (pHdr->lStatus);
	ENCAP_CVT_HL (pHdr->lContext1);
	ENCAP_CVT_HL (pHdr->lContext2);
	ENCAP_CVT_HL (pHdr->lOpt);  

	UINT32 uLength = pHdr->iLength + ENCAPH_SIZE;

	UINT   uCount  = 0;

	UINT32 uOffset = 0;

	while ( uRxCount < uLength ) {

		if( pHdr->iCommand == ENCAP_CMD_NOOP || uLength > MAX_PACKET_SIZE ) {

			uOffset = 0;
		
			uCount  = (uLength - uRxCount) > MAX_PACKET_SIZE ? MAX_PACKET_SIZE : (uLength - uRxCount);
			}
		else {
			uOffset = uRxCount;

			uCount  = uLength - uRxCount;
			}

		if( !socketCheck(pSock) ) {

			sessionRemove(nSession, TRUE);
			
			return ERROR_STATUS;
			}
		
		if( pSock->Recv(gmsgBuf + uOffset, uCount) == S_OK ) {

			uRxCount += uCount;

			continue;
			}
		
		if( uRxCount && uRxCount > ss.iPartialRecvPacketSize ) {

			utilResetMemoryPoolOffset( &ss.iPartialRecvPacketOffset,
						   &ss.iPartialRecvPacketSize, 
						   gmsgBuf, 
						   (UINT16)uRxCount
						   );
			
			ss.lPartialRecvPacketTimeoutTick = uTime + PARTIAL_PACKET_TIMEOUT;
			}				
			
		return ERROR_STATUS;
		}

	if( ss.iPartialRecvPacketOffset != INVALID_MEMORY_OFFSET ) {

		utilRemoveFromMemoryPool( &ss.iPartialRecvPacketOffset, 
					  &ss.iPartialRecvPacketSize 
					  );
		}

	if( pHdr->iCommand == ENCAP_CMD_NOOP ) {

		return ERROR_STATUS;
		}

	if( uLength > MAX_PACKET_SIZE ) {

		return ERROR_STATUS;
		}

	return uRxCount;
	}

global INT32 socketEncapSendData(INT32 nSession)
{
	SESSION &ss   = gSessions[nSession];

	ENCAPH  *pHdr = (ENCAPH *) gmsgBuf;

	INT32   nLen  = ENCAPH_SIZE + pHdr->iLength;

	pHdr->lSession = ss.lSessionTag;

	ENCAP_CVT_HS (pHdr->iLength);
	ENCAP_CVT_HS (pHdr->iCommand);
	ENCAP_CVT_HL (pHdr->lStatus);
	ENCAP_CVT_HL (pHdr->lContext1);
	ENCAP_CVT_HL (pHdr->lContext2);
	ENCAP_CVT_HL (pHdr->lOpt);

	return socketEncapSend(nSession, nLen);
	}

global INT32 socketEncapSendPartial(INT32 nSession)
{
	SESSION &ss  = gSessions[nSession];
	
	INT32   nLen = ss.iPartialSendPacketSize;			

	memcpy(gmsgBuf, MEM_PTR(ss.iPartialSendPacketOffset), nLen);	   

	return socketEncapSend(nSession, nLen);
	}

static INT32 socketEncapSend(INT32 nSession, UINT uLen)
{   
	SESSION &ss    = gSessions[nSession];

	PSOCK   &pSock = (PSOCK &) ss.lSocket;
	
	if( !pSock || uLen > MAX_PACKET_SIZE ) {

		return ERROR_STATUS;
		}

	UINT uCount = min(uLen, 1024);

	if( pSock->Send(gmsgBuf, uCount) == E_FAIL ) {

		if( !socketCheck(pSock) ) {

			sessionRemove(nSession, TRUE);

			return ERROR_STATUS;
			}
		}

	if( uCount < uLen ) {

		utilResetMemoryPoolOffset( &ss.iPartialSendPacketOffset,
					   &ss.iPartialSendPacketSize, 
					   &gmsgBuf[uCount], 
					   UINT16(uLen - uCount)
					   );
		}
	
	return uCount;
	}

global INT32 socketClass1Init(BOOL fBroadcast, UINT32)
{
	WORD    wPort  = fBroadcast ? ENCAP_SERVER_PORT : CLASS1_UDP_PORT;
	
	ISocket *pSock = (ISocket *) platformCreateSocket(IP_UDP);

	if( pSock ) {
		
		if( pSock->Listen(wPort) != S_OK )  {

			return (INT32) pSock;
			}

		return INT32(pSock);
		}

	notifyEvent(NM_ERROR_USING_WINSOCK, 0);

	return ERROR_STATUS;
	}

global INT32 socketJoinMulticastGroup(INT32 nConnection)
{
	// We never originate Target -> Originator multicast connections.
	
	/*AfxTrace("Level 2 Multicasting not supported\n");*/

	return FALSE;
	}

global INT32 socketDropMulticastGroup(INT32 nConnection)
{
	// We never originate Target -> Originator multicast connections.
	
	/*AfxTrace("Level 2 Multicasting not supported\n");*/

	return FALSE;
	}

global INT32 socketClass1Recv(UINT32 uClass1Socket)
{
	PSOCK pSock  = PSOCK(uClass1Socket);

	UINT  uCount = min(1024, MAX_PACKET_SIZE);

	if( pSock && pSock->Recv(gmsgBuf, uCount) == S_OK ) {

		CIpAddr IP;

		pSock->GetRemote(IP);

		return ioParseClass1Packet((DWORD &) IP, gmsgBuf, uCount);
		}

	return ERROR_STATUS;
	}

global void socketGetBroadcasts()
{   
	UINT uCount = min(1024, MAX_PACKET_SIZE);

	if( pSockUDP && pSockUDP->Recv(gmsgBuf, uCount) == S_OK ) {

		ENCAPH *pHdr = (ENCAPH *) gmsgBuf;

		if( pHdr->iCommand == ENCAP_CMD_LISTTARGETS ) {

			ucmmRespondListTargets( -1 );
			}

		else if( pHdr->iCommand == ENCAP_CMD_LISTSERVICES ) {

			ucmmRespondListServices( -1 );
			}

		else if( pHdr->iCommand == ENCAP_CMD_LISTINTERFACES ) {

			ucmmRespondListInterfaces( -1 );
			}
		else {
			return;
			}

		UINT uLength = ENCAPH_SIZE + pHdr->iLength;

		pSockUDP->Send(gmsgBuf, uLength);

		pSockUDP->Close();

		pSockUDP->Listen(ENCAP_SERVER_PORT);
		}
	}

global INT32 socketClass1SendData(INT32 nConnection)
{
	CPFHDR *pHdr = (CPFHDR *) gmsgBuf;

	UINT    uLen = CPFHDR_SIZE + ENCAP_TO_HS(pHdr->iDs_length);

	return socketClass1Send(nConnection, uLen);
	}

static INT32 socketClass1Send(INT32 nConnection, UINT uLen)
{
	CONNECTION     &Con  = (CONNECTION &) gConnections[nConnection];

	CONNECTION_CFG &Cfg  = Con.cfg;

	if( Cfg.bTransportClass != Class1 ) {
	
		return ERROR_STATUS;
		}

	if( Con.lConnectionState != ConnectionEstablished ) {

		return ERROR_STATUS;
		}

	CIpAddr &IP   = (CIpAddr &) Con.sTransmitAddr.sin_addr.s_un_b; 

	PSOCK   pSock = (ISocket *) platformCreateSocket(IP_UDP);

	if( !pSock ) {
		
		return ERROR_STATUS;
		}

	pSock->Connect(IP, CLASS1_UDP_PORT, CLASS1_UDP_PORT);

	if( pSock->Send(gmsgBuf, uLen) != S_OK ) {

		INT32 nSession;
		
		if( (nSession = sessionFindAddress(Con.lIPAddress, Outgoing)) != INVALID_SESSION ) {

			sessionRemove(nSession, TRUE);
			}

		if( (nSession = sessionFindAddress(Con.lIPAddress, Incoming)) != INVALID_SESSION ) {

			sessionRemove(nSession, TRUE);
			}

		platformCloseSocket((UINT32 &)pSock, FALSE); 
		
		return ERROR_STATUS;
		}

	platformCloseSocket((UINT32 &)pSock, FALSE); 

	return uLen;
	}

static void socketInitTargetData(void)
{
	CIpAddr  IP, Mask;

	CMacAddr DiscardMAC;

	if( platformFindEthernet(IP, Mask, DiscardMAC) ) {

		nHostAddrCount = 1;

		gHostAddr[0].sin_addr.s_un_b = IP;

		socketGetMcastBaseAddr(IP, Mask);
		}
	else {
		nHostAddrCount = 0;
		}
	}

static void socketGetMcastBaseAddr(IPREF IP, IPREF Subnet)
{
	CIpAddr Base(239, 192,   1,  0);

	CIpAddr Mask(255, 255, 252,  0);

	DWORD dwMask = ~(Mask | Subnet);

	DWORD dwHost = IP.GetValue() & dwMask;

	DWORD dwBase = Base.GetValue() | (dwHost << 5);

	////////

	memset(&gMulticastBaseAddr, 0, sizeof(sockaddr_in));

	gMulticastBaseAddr.sin_port        = htons(WORD(CLASS1_UDP_PORT));
	
	gMulticastBaseAddr.sin_addr.s_addr = htonl(dwBase);  
	}

static BOOL socketCheck(PSOCK &pSock)
{
	if( pSock ) {
	
		UINT Phase;

		pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			platformCloseSocket((UINT32 &)pSock, TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			platformCloseSocket((UINT32 &)pSock, FALSE);

			return FALSE;
			}

		if( Phase == PHASE_OPEN || Phase == PHASE_OPENING ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Winsock Implementations

global DWORD inet_addr(PCTXT p)
{
	CIpAddr IP(p);

	return IP.IsEmpty() ? ERROR_STATUS : (DWORD &) IP;
	}

global PTXT inet_ntoa(in_addr &in)
{
	static char Text[16];

	strcpy(Text, ((CIpAddr &) in.s_un_b).GetAsText());

	return Text;
	}

// End of File
