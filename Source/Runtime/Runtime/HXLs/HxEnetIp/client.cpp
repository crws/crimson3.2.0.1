/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** CLIENT.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Library main processing task. 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

BOOL					gbTerminated;		/* Indicates whether client is stopped at this moment */

extern PLATFORM_EVENT_TYPE     ghTickEvent;		/* Wake-up event for the main task */

PLATFORM_MUTEX_TYPE     ghClientMutex;	/* Used to protect integrity of the client calls */

INT32					glTCPSession;

#ifdef ET_IP_SCANNER
BOOL 	   gbRunMode;				/* Indicate whether Run or Idle state will be sent when producing connection data */
BOOL       gbSupportDynamicTargets;	/* Indicate whether this stack could be used as a target without initially creating a corresponding connection in the CC object */
#endif

INT32  gpClientAppProcClasses[MAX_CLIENT_APP_PROC_CLASS_NBR];		/* Class number array that will be processed by the client application */
UINT32 glClientAppProcClassNbr;										/* Number of classes registered for client application processing */
INT32  gpClientAppProcServices[MAX_CLIENT_APP_PROC_SERVICE_NBR];	/* Service number array that will be processed by the client application */
UINT32 glClientAppProcServiceNbr;									/* Number of services registered for client application processing */

/*---------------------------------------------------------------------------
** clientStart( )
**
** Start executing the client.
**---------------------------------------------------------------------------
*/

void clientStart()
{	
	gbTerminated = FALSE;	

#ifdef TRACE_FILE_OUTPUT
	InitializeDump();		/* Open the debug dump file if defug file output is requested */
#endif /* #ifdef TRACE_FILE_OUTPUT	*/
		
	socketInit();				/* Initialize Windows sockets */
	
	giMemoryPoolOffset = 0;		/* Mark memory pool as empty */
	
	/* Make sure that any of the API calls running in the context of a client thread
	   are not executed at the same time as another API call or the EML thread is executed */
	ghClientMutex = platformInitMutex(_T("EtIPClientMutex"));
	platformReleaseMutex(ghClientMutex);

#ifdef ET_IP_SCANNER	
	gbRunMode = TRUE;
	gbSupportDynamicTargets = TRUE;
	glEditSignature = 0;
#endif
	

	DumpStr0("Starting to Initialize Objects");
	idInit();						/* Initialize identity information */
	sessionInit();					/* Initialize session array */
	connectionInit();				/* Initialize connection array */	
	requestInit();					/* Init requests array */	
	assemblyInit();					/* Init assembly tables */
	dataTableInit();				/* Init data table parameters */
	notifyInit();					/* Initialize event array */
	glClientAppProcClassNbr = 0;	/* Number of classes registered for client app processing */
	glClientAppProcServiceNbr = 0;	/* Number of services registered for client app processing */
	glTCPSession = 0;
				
	tcpipInit();			/* Init TCP/IP object */
	enetlinkInit();			/* Init Ethernet Link object */

	ghTickEvent = platformStartTimer();	

	/* Create main dll thread */
	if(!platformCreateThread(clientMainThread,NULL,THREAD_PRIORITY_TIME_CRITICAL))
	{
		DumpStr0("Unable to start main thread");
		notifyEvent( NM_UNABLE_START_THREAD, 0 );
		return;
	}	
}

/*---------------------------------------------------------------------------
** clientStop( )
**
** Stop running the client. 
**---------------------------------------------------------------------------
*/

void clientStop( )
{	
	INT32 i;
	//BOOL bWaitForConnectionsToClose = FALSE;

	if ( gbTerminated )							/* Stack is already stopped - return */
	    return;
	
	DumpStr0("clientStop");
	
	/* Close all connections */
	for( i = 0; i < gnConnections; i++ )
		gConnections[i].lConfigurationState = ConfigurationClosing;	

	for( i = 0; i < 100; i++ )
	{
		if ( !connectionGetNumOriginatorEstablishedOrClosing() )
			break;
		platformSleep( 5 );
	}
	
	/* Close all sessions */
	for( i = 0; i < gnSessions; i++ )
	{
		/* If we are the ones who initiate a connection drop - issue Forward Close */
		if ( !gSessions[i].bIncoming && gSessions[i].lState == OpenSessionEstablished )	
		{				
			ucmmIssueUnRegisterSession( i );	/* Send Unregister Session request to the server */				
		}	
	}
	
	connectionRemoveAll();
	requestRemoveAll();
	sessionRemoveAll();	

	socketCleanup();

#ifdef TRACE_FILE_OUTPUT
	FinishDump();			/* Close debug dump file */
#endif /* #ifdef TRACE_FILE_OUTPUT	*/

	clientRelease();
}

#ifdef ET_IP_SCANNER	

/*---------------------------------------------------------------------------
** clientGetRunMode()
**
** Returns Run mode setting. Initially set to TRUE. May be modified by 
** calling clientSetRunMode() function.
** The Run mode is being sent as the first bit of the first 4 status bytes, 
** which are sent with every producing I/O packet.
**---------------------------------------------------------------------------
*/
BOOL clientGetRunMode()
{
	BOOL bRunMode;

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);

	bRunMode = gbRunMode;

	platformReleaseMutex(ghClientMutex);

	return gbRunMode;
}

/*---------------------------------------------------------------------------
** clientSetRunMode()
**
** Set Run mode by passing TRUE, or Idle mode by pasing FALSE as a parameter.
** The state is being sent as the first bit of the first 4 status bytes, 
** which are sent with every producing I/O packet.
**---------------------------------------------------------------------------
*/
void clientSetRunMode( BOOL bRunMode )
{	
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	/* Update the data sequence count to indicate new data when the run mode changes */
	if ( gbRunMode != bRunMode )
		connectionUpdateAllSequenceCount();
		
	gbRunMode = bRunMode;

	platformReleaseMutex(ghClientMutex);
}

/*-------------------------------------------------------------------------------
** clientGetDynamicTargetSupportFlag()
**
** Returns Dynamic Target Support Flag. Initially set to TRUE. May be modified  
** by calling clientSetDynamicTargetSupportFlag() function.
** If Dynamic Target Support Flag is set to TRUE, this stack could be used as 
** a target without initially creating a corresponding connection in the CC object. 
**--------------------------------------------------------------------------------
*/
BOOL clientGetDynamicTargetSupportFlag()
{
	BOOL bSupportDynamicTargets;

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);

	bSupportDynamicTargets = gbSupportDynamicTargets;

	platformReleaseMutex(ghClientMutex);
	
	return bSupportDynamicTargets;
}

/*--------------------------------------------------------------------------------
** clientSetDynamicTargetSupportFlag()
**
** Sets Dynamic Target Support Flag. Initially this flag is set to TRUE.
** If Dynamic Target Support Flag is set to TRUE, this stack could be used as 
** a target without initially creating a corresponding connection in the CC object. 
**---------------------------------------------------------------------------------
*/
void clientSetDynamicTargetSupportFlag( BOOL bSupportDynamicTargets )
{
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	gbSupportDynamicTargets = bSupportDynamicTargets;
	platformReleaseMutex(ghClientMutex);
}

#endif /* #ifdef ET_IP_SCANNER	*/

/*---------------------------------------------------------------------------
** clientRelease( )
**
** Release all resources
**---------------------------------------------------------------------------
*/

void clientRelease()
{
	gbTerminated = TRUE;	/* Set the flag to stop the main thread */
	platformSleep(10);
	/* Stop the timer */
	platformStopTimer();
	platformDiscardMutex( ghClientMutex );	
}

/*---------------------------------------------------------------------------
** clientMainThread( )
**
** Main processing thread. Is executed once a millisecond. 
** Running until nothing else to do. Then sleeps the rest of the time slice.
**---------------------------------------------------------------------------
*/

PLATFORM_THREAD_RET PLATFORM_THREAD_MOD clientMainThread( void* pParam )
{	
	while( !gbTerminated )					/* Run until Client Stop command is received */
	{	
		clientMainTask( 0, 0, 0, 0, 0 );		/* Execute main task */		
		platformWaitEvent(ghTickEvent, 1);		/* Wait for event or one tick */
	}	
	return((PLATFORM_THREAD_RET)1);
}

/*---------------------------------------------------------------------------
** clientMainTask( )
**
** Main processing thread. Is executed once a millisecond. 
** Running until nothing else to do. Then sleeps the rest of the time slice.
**---------------------------------------------------------------------------
*/
void clientMainTask(UINT wTimerID, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2)
{    
	INT32			nConnection, nSession, nRequest;			
	INT32			nConnProcessed = 0;
	static  INT32	nConnProcessedTotal = 0;
	static  UINT32	lTickLast = 0;	
	UINT32			lTick = platformGetTickCount();
	INT32           lDataReceived = 0;
	INT32           nConnGroup;
	INT32           nIndex;
	INT32           nInstance;
	INT32           nSessions;

	DumpStr0NoNewLine("*");
	
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	/* Check incoming I/O for Class1 connections */
	for( nConnGroup = 0; nConnGroup < NUM_CONNECTION_GROUPS; nConnGroup++ )
	{
		if ( glClass1Socket[nConnGroup] != INVALID_SOCKET )
		{		
			do 
			{
				lDataReceived = socketClass1Recv( glClass1Socket[nConnGroup] );
			}
			while( lDataReceived > 0 );
		}
	}	
	
	/* Service connections including sending I/O data if needed */
	for ( nConnection = 0; nConnection < gnConnections; nConnection++ ) 
	{
		nInstance = gConnections[nConnection].cfg.nInstance;
		if ( connectionService( nConnection ) )
			nConnProcessed++;
		/* Offset nConnection index if connection was removed */
		if ( nConnection < gnConnections )
		{
			if ( gConnections[nConnection].cfg.nInstance != nInstance )
				nConnection--;
		}
		else
			nConnection--;
	}

	/* Total all Class1 sent in the period of TCP_MINIMUM_RATE msec */
	nConnProcessedTotal += nConnProcessed;

	/* Process TCP requests at least once in TCP_MINIMUM_RATE msec. Process more if amount of I/O traffic
	   allows. Decrease CONN_PROCESSED_LIMIT constant to give Class1 I/O more priority, or
	   increase to give more priority to TCP traffic */
	if ( lTick/TCP_MINIMUM_RATE != lTickLast/TCP_MINIMUM_RATE || ( !nConnProcessed && nConnProcessedTotal < CONN_PROCESSED_LIMIT ) )
	{
		/* Check for the incoming broadcast List Targets request */
		socketGetBroadcasts();
				
		/* Check for the incoming TCP connection requests from other devices */
		socketCheckIncomingSession();
				
		/* Service all TCP sessions */
		for ( nSession = 0; nSession < gnSessions; nSession++ ) 
		{
			nSessions = gnSessions;
			sessionService( nSession );
			/* Offset nSession index if session was removed */
			if ( nSessions != gnSessions )
				nSession--; 
		}
				
		/* Service any requests */
		for ( nRequest = 0; nRequest < gnRequests; nRequest++ ) 
		{
			nIndex = gRequests[nRequest].nIndex;
			requestService( nRequest );
			/* Check if the request was removed from underneath us */
			if ( nRequest >= gnRequests )
				break;
			if ( nIndex != gRequests[nRequest].nIndex )
				nRequest--;
		}
	}

#ifdef ET_IP_SCANNER			
	/* Service CC object */		
	configService();		
#endif

	if ( lTick/TCP_MINIMUM_RATE != lTickLast/TCP_MINIMUM_RATE )
	{
		//if ( (lTick-lTickLast) > 10 )
		//	DumpStr1NoNewLine("<%d>", (lTick-lTickLast));
		lTickLast = lTick;
		nConnProcessedTotal = 0;
	}	

	platformReleaseMutex(ghClientMutex);

	/* Now that we released the mutex, we can send any events logged to the client app */
	notifyService();
}

/*---------------------------------------------------------------------------
** clientGetInputData( )
**
** Return input data table contents for some or all connections.
** pBuf is the buffer allocated by the calling application to be filled
** with the input table data. 
** nOffset is the starting offset in the input data table we should start 
** copying from.
** nLength is the size of the preallocated buffer.
** Returns the actual size of the data copied into the pBuf buffer.
**---------------------------------------------------------------------------
*/
INT32 clientGetInputData(UINT8* pBuf, INT32 nOffset, INT32 nLength)
{
	INT32 nRet;

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	nRet = assemblyGetInputData( pBuf, nOffset, nLength );

	platformReleaseMutex(ghClientMutex);

	return nRet;
}

/*---------------------------------------------------------------------------
** EtIPSetInputData( )
**
** Set input data table contents for some or all connections.
** pBuf contains the input data that should be used to populate input data
** table. 
** nOffset is the starting offset in the input data table we should start 
** copying to.
** nLength is the size of the input data in bytes.
** Returns the actual size of the data copied from the pBuf buffer to the
** input data table.
**---------------------------------------------------------------------------
*/
INT32 clientSetInputData(UINT8* pBuf, INT32 nOffset, INT32 nLength)
{
	INT32 nRet;
	
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	nRet = assemblySetInputData( pBuf, nOffset, nLength );

	platformReleaseMutex(ghClientMutex);

	return nRet;
}

/*---------------------------------------------------------------------------
** EtIPGetOutputData( )
**
** Return output data table contents for some or all connections.
** pBuf is the buffer allocated by the calling application to be filled
** with the output table data. 
** nOffset is the starting offset in the output data table we should start 
** copying from.
** nLength is the size of the preallocated buffer.
** Returns the actual size of the data copied into the pBuf buffer.
**---------------------------------------------------------------------------
*/

INT32 clientGetOutputData(UINT8* pBuf, INT32 nOffset, INT32 nLength)
{
	INT32 nRet;
	
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	nRet = assemblyGetOutputData( pBuf, nOffset, nLength );

	platformReleaseMutex(ghClientMutex);

	return nRet;
}

/*---------------------------------------------------------------------------
** clientSetOutputData( )
**
** Set output data table contents for some or all connections.
** pBuf contains the output data that should be used to populate output data
** table. 
** nOffset is the starting offset in the output data table we should start 
** copying to.
** nLength is the size of the output data in bytes.
** Returns the actual size of the data copied from the pBuf buffer to the
** output data table.
**---------------------------------------------------------------------------
*/

INT32 clientSetOutputData(UINT8* pBuf, INT32 nOffset, INT32 nLength)
{
	INT32 nRet;

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	nRet = assemblySetOutputData( pBuf, nOffset, nLength );

	platformReleaseMutex(ghClientMutex);

	return nRet;
}

/*---------------------------------------------------------------------------
** clientGetConnectionInputData( )
**
** Return input I/O data including possible status and configuration for the 
** particular connection.
** nConnectionID is the connection ID obtained by calling EtIPGetConnectionIDs().
** pBuf is the buffer allocated by the calling application to be filled
** with the input I/O data. 
** nLength is the size of the preallocated buffer.
** Returns the actual size of the data copied into the pBuf buffer.
**---------------------------------------------------------------------------
*/
INT32 clientGetConnectionInputData(INT32 nConnectionInstance, UINT8* pBuf, INT32 lLength)
{
	INT32 nConnection;
	INT32 nRet;

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);

	if ( (nConnection = connectionGetIndexFromInstance(nConnectionInstance)) == ERROR_STATUS )
	{
		platformReleaseMutex(ghClientMutex);
		return 0;
	}
	
	if ( gConnections[nConnection].lConnectionState != ConnectionEstablished )
	{
		platformReleaseMutex(ghClientMutex);
		return 0;
	}
		
	nRet = assemblyGetConnectionInputData( nConnection, pBuf, lLength );	/* Return whatever we got in assembly object */

	platformReleaseMutex(ghClientMutex);	
	
	return nRet;
}

/*---------------------------------------------------------------------------
** EtIPSetConnectionInputData( )
**
** Set input I/O data for the particular connection. Include 
** 4 bytes of Run/Idle status if appropriate.
** nConnectionIntance is the connection instance obtained by calling 
** EtIPGetConnectionInstances().
** pBuf contains the input data.
** nLength is the size of input data in bytes.
** Returns the actual size of the data copied from the pBuf buffer,
** or 0 if the nConnectionInstance is invalid or not in the 
** ConnectionEstablished state.
** This function is ignored for Class3 connections.
**---------------------------------------------------------------------------
*/
INT32 clientSetConnectionInputData(INT32 nConnectionInstance, UINT8* pBuf, INT32 lLength)
{
	INT32 nConnection;
	INT32 nRet;

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);

	if ( (nConnection = connectionGetIndexFromInstance(nConnectionInstance)) == ERROR_STATUS )
	{
		platformReleaseMutex(ghClientMutex);	
		return 0;
	}
	
	if ( gConnections[nConnection].lConnectionState != ConnectionEstablished )
	{
		platformReleaseMutex(ghClientMutex);	
		return 0;
	}
		
	nRet = assemblySetConnectionInputData( nConnection, pBuf, lLength );	/* Return whatever we got in assembly object */

	platformReleaseMutex(ghClientMutex);	

	return nRet;
}

/*---------------------------------------------------------------------------
** EtIPGetConnectionOutputData( )
**
** Return output I/O data not including possible 4 bytes of Run/Idle status. 
** nConnectionInstance is the connection instance obtained by calling 
** EtIPGetConnectionInstances().
** pBuf is the buffer allocated by the calling application to be filled
** with the output I/O data. 
** nLength is the size of the preallocated buffer.
** Returns the actual size of the data copied into the pBuf buffer, 
** or 0 if the nConnectionInstance is invalid or not in the 
** ConnectionEstablished state.
**---------------------------------------------------------------------------
*/
INT32 clientGetConnectionOutputData(INT32 nConnectionInstance, UINT8* pBuf, INT32 lLength)
{
	INT32 nConnection;
	INT32 nRet;

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);

	if ( (nConnection = connectionGetIndexFromInstance(nConnectionInstance)) == ERROR_STATUS )
	{
		platformReleaseMutex(ghClientMutex);
		return 0;
	}
	
	if ( gConnections[nConnection].lConnectionState != ConnectionEstablished )
	{
		platformReleaseMutex(ghClientMutex);
		return 0;
	}
		
	nRet = assemblyGetConnectionOutputData( nConnection, pBuf, lLength );	/* Return whatever we got in assembly object */

	platformReleaseMutex(ghClientMutex);

	return nRet;
}

/*---------------------------------------------------------------------------
** clientSetConnectionOutputData( )
**
** Set output I/O data including possible status and configuration for the 
** particular connection. 
** nConnectionID is the connection ID obtained by calling EtIPGetConnectionIDs().
** pBuf contains the output data that should be used to send to the 
** target device.
** nLength is the size of output data in bytes.
** Returns the actual size of the data copied from the pBuf buffer.
**---------------------------------------------------------------------------
*/
INT32 clientSetConnectionOutputData(INT32 nConnectionInstance, UINT8* pBuf, INT32 lLength)
{
	INT32 nConnection, nLen;
	
	DumpStr2 ("clientSetConnectionOutputData Instance %d, Len %d", nConnectionInstance, lLength );
	DumpBuf((char*)pBuf, lLength);

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	/* Find the appropriate connection based on the instance number */
	if ( (nConnection = connectionGetIndexFromInstance(nConnectionInstance)) == ERROR_STATUS )
	{
		platformReleaseMutex(ghClientMutex);
		DumpStr0("clientSetConnectionOutputData could not get index");
		return 0;
	}
	
	/* Verify that connection is active and not listen-only */
	if ( gConnections[nConnection].lConnectionState != ConnectionEstablished )
	{
		platformReleaseMutex(ghClientMutex);
		DumpStr0("clientSetConnectionOutputData lConnectionState is not Established");
		return 0;
	}
	
	nLen = assemblySetConnectionOutputData( nConnection, pBuf, lLength );	/* Set assembly object data */
	
	if ( (gConnections[nConnection].cfg.bTransportType == ChangeOfState ||			/* If the connection is ChangeOfState */
		  gConnections[nConnection].cfg.bTransportType == ApplicationTriggered) &&	/* or ApplicationTriggered */
		 connectionIsInhibitExpired( nConnection ) )								/* and inhibit timeout expired then produce immediately */
	{		
		gConnections[nConnection].bTransferImmediately = TRUE;
	}

	platformReleaseMutex(ghClientMutex);

	return nLen;
}


/*---------------------------------------------------------------------------
** clientGetNumConnections( )
**
** Returns total number of connections.
**---------------------------------------------------------------------------
*/
INT32 clientGetNumConnections()
{
	return gnConnections;
}

/*---------------------------------------------------------------------------
** clientGetConnectionInstances( )
**
** Returns total number of connections in the state other than 
** ConnectionNonExistent.
** pnConnectionIDArray should point to the array of MAX_CONNECTIONS integers.
** It will receive the connection instances.
** Example:
**
** int ConnectionInstanceArray[MAX_CONNECTIONS];
** int nNumConnections = EtIPGetConnectionInstances(ConnectionInstanceArray);
**
** After return ConnectionInstanceArray[0] will have the first connection instance,
** ConnectionInstanceArray[1] will have the second connection instance ...
** ConnectionInstanceArray[nNumConnections-1] will have the last connection 
** instance.
**---------------------------------------------------------------------------
*/
INT32 clientGetConnectionInstances(INT32 *pnConnectionInstanceArray)
{
	INT32 nConnection, nRet;

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);

	for( nConnection = 0; nConnection < gnConnections; nConnection++ )
	{
		*pnConnectionInstanceArray = gConnections[nConnection].cfg.nInstance;
		pnConnectionInstanceArray++;
	}

	nRet = gnConnections;

	platformReleaseMutex(ghClientMutex);

	return nRet;
}


/*---------------------------------------------------------------------------
** clientGetConnectionState( )
**
** Return the state of the particular connection from the EtIPConnectionState
** enumeration.
** nConnectionInstance is the connection instance obtained by calling 
** EtIPGetConnectionInstances() and should be between 1 and MAX_CONNECTIONS.
**---------------------------------------------------------------------------
*/
INT32 clientGetConnectionState(INT32 nConnectionInstance)
{
   INT32  nConnection;
   INT32  nState = ConnectionNonExistent;	
   
   if ( nConnectionInstance < 1 || nConnectionInstance > MAX_CONNECTIONS )
	   return nState;

   platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
   
   if ( (nConnection = connectionGetIndexFromInstance(nConnectionInstance)) != INVALID_CONNECTION )
		nState = gConnections[nConnection].lConnectionState;

   platformReleaseMutex(ghClientMutex);
   
   DumpStr3("clientGetConnectionState Instance %d, Index %d, State 0x%x", nConnectionInstance, nConnection, nState);

   return nState;
}

/*---------------------------------------------------------------------------
** clientGetConnectionConfig( )
**
** Return configuration for the particular connection.
** nConnectionInstance is the connection instance obtained by calling 
** EtIPGetConnectionInstances().
** pConfig is the pointer to the EtIPConnectionConfig structure allocated
** by the calling application. This structure is filled by this stack
** in case of a successful response.
** Returns 0 in case of a success, or ERR_INVALID_CONNECTION_INSTANCE_SPECIFIED
** if nConnectionInstance is out of the connection instance range: from 1 to 
** MAX_CONNECTIONS, or is in ConnectionNonExistent state.
**---------------------------------------------------------------------------
*/
INT32 clientGetConnectionConfig(INT32 nConnectionInstance, EtIPConnectionConfig* pConfig)
{
	INT32  nConnection;
	
    platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	if ( (nConnection = connectionGetIndexFromInstance(nConnectionInstance)) == INVALID_CONNECTION )
	{
		platformReleaseMutex(ghClientMutex);
		return ERR_INVALID_CONNECTION_INSTANCE_SPECIFIED;
	}

	connectionConvertFromInternalCfgStorage( &gConnections[nConnection].cfg, pConfig );

	platformReleaseMutex(ghClientMutex);

	return OK;
}

#ifdef ET_IP_SCANNER
/*---------------------------------------------------------------------------
** EtIPGetConnectionErrorInfo( )
**
** If the user called one of the EtIPOpenConnection() or EtIPResetConnection() 
** functions and later received NM_CONN_CONFIG_FAILED_ERROR_RESPONSE notification.
** message, he could use EtIPGetConnectionErrorInfo() function call to get the 
** error codes and the corresponding error description.
** You may also use this function to check for an error response from a 
** connection message for Class3 connections.
** nConnectionInstance is the connection instance passed to EtIPOpenConnection()
** or returned from EtIPGetConnectionInstances() call.
** pErrorInfo is the pointer to the EtIPErrorInfo structure allocated
** by the calling application. This structure is filled by this stack
** with an error information.
** Returns 0 in case of success, or ERR_INVALID_CONNECTION_INSTANCE_SPECIFIED 
** if connection instance is out of range.
**---------------------------------------------------------------------------
*/
INT32 clientGetConnectionErrorInfo(INT32 nConnectionInstance, EtIPErrorInfo* pErrorInfo)
{
	INT32 nConnection;

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	if ( (nConnection = connectionGetIndexFromInstance(nConnectionInstance)) == INVALID_CONNECTION )
	{
		platformReleaseMutex(ghClientMutex);
		return ERR_INVALID_CONNECTION_INSTANCE_SPECIFIED;
	}
	
	pErrorInfo->bGeneralStatus = gConnections[nConnection].bGeneralStatus;
	pErrorInfo->iExtendedStatus = gConnections[nConnection].iExtendedStatus;
	
	platformReleaseMutex(ghClientMutex);
	
	notifyGetCIPErrorInfo( pErrorInfo );
	return OK;
}
#endif

/*---------------------------------------------------------------------------
** clientRegisterEventCallBack( )
**
** Register client application callback function
**---------------------------------------------------------------------------
*/
void clientRegisterEventCallBack( LogEventCallbackType *pfnLogEvent )
{
	gfnLogEvent = pfnLogEvent;
}

/*---------------------------------------------------------------------------
** clientSendUnconnectedRequest( )
**
** Send either UCMM or Unconnected Send depending on whether szNetworkPath 
** specifies local or remote target.
** clientGetResponse will be used to get the response.
** szNetworkPath is zero terminated network path string (i.e."216.233.160.112" 
** or "216.233.160.112,2,216.233.160.145" or "216.233.160.112,1,0").
** pRequest points to a structure containing the request parameters.
** Check the return value. If it's greater than or equal to REQUEST_INDEX_BASE,
** then it is a request Id to be used when calling EtIPGetResponse.
** If it is less than REQUEST_INDEX_BASE then it's one of the 
** _EtIPNotificationMessages_ errors.
**---------------------------------------------------------------------------
*/
INT32 clientSendUnconnectedRequest( const char* szNetworkAddr, EtIPObjectRequest* pRequest )
{
	INT32  nRequest;
	BOOL   bLocalRequest;
	char   extendedPath[MAX_CONNECTION_PATH_SIZE];
	UINT16 iExtendedPathLen;
	char   szNetworkPath[MAX_CONNECTION_PATH_SIZE];
		
	strcpy( szNetworkPath, szNetworkAddr );
	
	if ( !utilParseNetworkPath( szNetworkPath, &bLocalRequest, extendedPath, &iExtendedPathLen ) )
		return NM_REQUEST_FAILED_INVALID_NETWORK_PATH;
		
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	if ( bLocalRequest )	
		nRequest = requestNewUCMM( (const unsigned char *)szNetworkPath, pRequest );	/* Schedule UCMM */
	else
		nRequest = requestNewUnconnectedSend( (unsigned char *)szNetworkPath, pRequest, extendedPath, iExtendedPathLen  );/* Schedule Unconnected Send */

	platformReleaseMutex(ghClientMutex);
			
	return nRequest;
}


/*---------------------------------------------------------------------------
** clientGetResponse( )
**
** Get a response for the previously sent unconnected message.
** nRequestId is a request Id returned from the previously sent 
** EtIPSendUCMMRequest message. 
** Returns 0 in case of success or one of the error codes.
**---------------------------------------------------------------------------
*/
			 
INT32 clientGetResponse(INT32 nRequestId, EtIPObjectResponse* pResponse)
{
	INT32 nRequest;		
	
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	nRequest = requestGetByRequestId( nRequestId );

	if ( nRequest == INVALID_REQUEST )
	{
		platformReleaseMutex(ghClientMutex);
		return ERR_OBJECT_REQUEST_UNKNOWN_INDEX;		/* Request Id was not found */
	}
	
	if ( gRequests[nRequest].nState != RequestResponseReceived )
	{
		platformReleaseMutex(ghClientMutex);
		DumpStr2("clientGetResponse Request = 0x%x, state = %d - response not ready yet", nRequestId, gRequests[nRequest].nState);
		return ERR_NO_AVAILABLE_OBJECT_RESPONSE;	/* Request is still pending without a response */
	}
			
	pResponse->bGeneralStatus = gRequests[nRequest].bGeneralError;
				
	if ( pResponse->bGeneralStatus )	/* If other than 0 collect the remaining error info */
	{
		if ( gRequests[nRequest].iDataSize >= sizeof(UINT16) )
			pResponse->iExtendedStatus = UINT16_GET(pResponse->responseData);
		else
			pResponse->iExtendedStatus = 0;
		pResponse->iDataSize = 0;
		notifyGetCIPErrorDescription( pResponse->bGeneralStatus, pResponse->iExtendedStatus, (PBYTE) pResponse->errorDescription );				
	}
	else
	{
		pResponse->iExtendedStatus = 0;
		pResponse->errorDescription[0] = 0;
		pResponse->iDataSize = gRequests[nRequest].iDataSize;
		memcpy( pResponse->responseData, MEM_PTR(gRequests[nRequest].iDataOffset), pResponse->iDataSize );				
	}
						
	requestRemove( nRequest );
	platformReleaseMutex(ghClientMutex);
	return OK;	
}

/*---------------------------------------------------------------------------
** clientRegisterObjectsForClientProcessing( )
**
** Provides the list of the objects that will be processed by the client 
** application. When the request is received for one of these objects
** the NM_CLIENT_OBJECT_REQUEST_RECEIVED notification message will be 
** sent to the client app. The client application can use EtIPGetClientRequest()
** and EtIPSendClientResponse() after that to get the request and 
** send the response.
** pClassNumberList is the pointer to the integer array. Each integer represent
** the class number of the object that will be proccessed by the client 
** application.
** nNumberOfClasses is the number of classes in the list.
**---------------------------------------------------------------------------
*/
void clientRegisterObjectsForClientProcessing(INT32* pClassNumberList, INT32 nNumberOfClasses,
					int* pServiceNumberList, int nNumberOfServices)
{
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);

	memcpy( gpClientAppProcClasses, pClassNumberList, nNumberOfClasses*sizeof(INT32) );
	glClientAppProcClassNbr = nNumberOfClasses;
	memcpy( gpClientAppProcServices, pServiceNumberList, nNumberOfServices*sizeof(INT32) );
	glClientAppProcServiceNbr = nNumberOfServices;

	platformReleaseMutex(ghClientMutex);
}


/*---------------------------------------------------------------------------
** EtIPGetClientRequest( )
**
** Gets the pending request that should be processed by the client
** application. The objects for the client application processing must
** be registered in advance by calling EtIPRegisterObjectsForClientProcessing()
** function.
** The client application will get NM_CLIENT_OBJECT_REQUEST_RECEIVED callback
** with a new request Id. The same request Id should be passed when calling
** EtIPGetClientRequest() function.
** pRequest is the pointer to the EtIPObjectRequest structure that will have 
** the request parameters on return.
** Returns 0 if successful or ERR_OBJECT_REQUEST_UNKNOWN_INDEX if there is no 
** pending request with this Id.
**---------------------------------------------------------------------------
*/
INT32 clientGetClientRequest( INT32 nRequestId, EtIPObjectRequest* pRequest )
{
	INT32 nRequest;	
		
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	nRequest = requestGetByRequestId( nRequestId );

	if ( nRequest == INVALID_REQUEST )
	{
		platformReleaseMutex(ghClientMutex);
		return ERR_OBJECT_REQUEST_UNKNOWN_INDEX;		/* Request Id was not found */
	}
	
	pRequest->bService = gRequests[nRequest].bService;
	pRequest->iClass = gRequests[nRequest].iClass;
	pRequest->iInstance = gRequests[nRequest].iInstance;
	pRequest->iAttribute = gRequests[nRequest].iAttribute;
	pRequest->iMember = gRequests[nRequest].iMember;
	pRequest->iTagSize = gRequests[nRequest].iTagSize;
	memcpy(pRequest->requestTag, MEM_PTR(gRequests[nRequest].iTagOffset), pRequest->iTagSize);
	pRequest->iDataSize = gRequests[nRequest].iDataSize;
	memcpy(pRequest->requestData, MEM_PTR(gRequests[nRequest].iDataOffset), pRequest->iDataSize);
			
	utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );
	utilRemoveFromMemoryPool( &gRequests[nRequest].iTagOffset, &gRequests[nRequest].iTagSize );
	DumpStr1("clientGetClientRequest new client request = 0x%x", gRequests[nRequest].nIndex);

	platformReleaseMutex(ghClientMutex);
		
	return OK;
}	

/*---------------------------------------------------------------------------
** clientSendClientResponse( )
**
** Send a response to the previously received request registered to be
** processed by the client application.
** The client application previously polled the request by calling
** EtIPGetClientRequest() function and now sends the response to that
** request.
** Returns 0 if successful or ERR_OBJECT_REQUEST_UNKNOWN_INDEX if there is no 
** pending request with this Id.
**---------------------------------------------------------------------------
*/
INT32 clientSendClientResponse(INT32 nRequestId, EtIPObjectResponse* pResponse)
{
	INT32 nRequest;	

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
		
	nRequest = requestGetByRequestId( nRequestId );

	if ( nRequest == INVALID_REQUEST )
	{
		platformReleaseMutex(ghClientMutex);
		return ERR_OBJECT_REQUEST_UNKNOWN_INDEX;		/* Request Id was not found */
	}
		
	gRequests[nRequest].bGeneralError = pResponse->bGeneralStatus;
	gRequests[nRequest].iExtendedError = pResponse->iExtendedStatus;
	gRequests[nRequest].iDataSize = pResponse->iDataSize;
	gRequests[nRequest].iDataOffset = utilAddToMemoryPool(pResponse->responseData, pResponse->iDataSize);
	
	if ( gRequests[nRequest].iDataSize && gRequests[nRequest].iDataOffset == INVALID_MEMORY_OFFSET )
	{			
		requestRemove( nRequest );	/* We are out of memory */
		platformReleaseMutex(ghClientMutex);
		return NM_OUT_OF_MEMORY;
	}

	gRequests[nRequest].nState = RequestResponseReceived;					

	platformReleaseMutex(ghClientMutex);
	
	return OK;
}

/*---------------------------------------------------------------------------
** clientGetIdentityInfo( )
**
** Returns identity information.
** The client application is responsible for allocating EtIPIdentityInfo
** structure and passing its pointer with the EtIPGetIdentityInfo function
** call. On return the structure fill be filled with identity information.
**---------------------------------------------------------------------------
*/
void clientGetIdentityInfo(EtIPIdentityInfo* pInfo)
{
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	memcpy( pInfo, &gIDInfo, sizeof(EtIPIdentityInfo) );	
	platformReleaseMutex(ghClientMutex);	
}

/*---------------------------------------------------------------------------
** clientSetIdentityInfo( )
**
** Updates identity information.
** pInfo is the pointer to the EtIPIdentityInfo structure with the new
** identity information.
** To change only some of the identity parameters the client application
** can call EtIPGetIdentityInfo, modify only appropriate structure members
** and then call EtIPSetIdentityInfo.
**---------------------------------------------------------------------------
*/
void clientSetIdentityInfo(EtIPIdentityInfo* pInfo)
{
	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	memcpy( &gIDInfo, pInfo, sizeof(EtIPIdentityInfo) );
	platformReleaseMutex(ghClientMutex);	
}

/*---------------------------------------------------------------------------
** clientGetHostIPAddress( )
**
** Returns a string with the host IP address.
** szHostIPAddress should point to the preallocated string where IP address
** will be stored.
** Returns TRUE on success, or FALSE if unabled to obtain the IP address.
**---------------------------------------------------------------------------
*/
BOOL clientGetHostIPAddress( char* szHostIPAddress )
{
	char* pAddr;
	INT32  i;

	platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
	
	for( i = 0; i < nHostAddrCount; i++ )
	{
		pAddr = inet_ntoa( gHostAddr[i].sin_addr );

		if ( pAddr )
		{
			if ( i == 0 )
				strcpy( szHostIPAddress, pAddr );
			else
			{
				strcat( szHostIPAddress, "/" );
				strcat( szHostIPAddress, pAddr );
			}
		}
		else if ( i == 0 )
		{
			platformReleaseMutex(ghClientMutex);
			return FALSE;
		}
	}

	platformReleaseMutex(ghClientMutex);
	return TRUE;
}


