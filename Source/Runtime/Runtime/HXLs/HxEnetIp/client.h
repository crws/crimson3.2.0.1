/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** CLIENT.h
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Library main processing task.
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#ifndef CLIENT_H
#define CLIENT_H


#define OK              (0)			/* Return in case of a success */
#define ERROR_STATUS	(-1)		/* Return in case of a failure */

#define DEFAULT_TIMEOUT			10000	/* Default time out of 10 seconds */
#define NON_CRITICAL_TIMEOUT	60000	/* Larger timeout of 60 sec for non-critical events */

#define TCP_MAXIMUM_PROCESS_TIME	2

#define CONN_PROCESSED_LIMIT		20	/* Increase to bump up TCP priority; decrease to raise UDP priority */
#define TCP_MINIMUM_RATE			10	/* Minimum rate at which TCP packets will be processed, even if there is plenty of UDP traffic. If UDP traffic allows, TCP will be processed more frequently. */

extern BOOL		gbTerminated;			/* Flag indicating whether the client is stopped */

#ifdef ET_IP_SCANNER	
extern BOOL 	  gbRunMode;				/* Indicate whether Run or Idle state will be sent when producing connection data */
extern BOOL       gbSupportDynamicTargets;	/* Indicate whether this stack could be used as a target without initially creating a corresponding connection in the CC object */
#endif

extern PLATFORM_MUTEX_TYPE     ghClientMutex;	/* Used to protect integrity of the client calls */

extern INT32  gpClientAppProcClasses[MAX_CLIENT_APP_PROC_CLASS_NBR];	/* Class number array that will be processed by the client application */
extern UINT32 glClientAppProcClassNbr;									/* Number of classes registered for client application processing */
extern INT32  gpClientAppProcServices[MAX_CLIENT_APP_PROC_SERVICE_NBR];	/* Service number array that will be processed by the client application */
extern UINT32 glClientAppProcServiceNbr;								/* Number of services registered for client application processing */

extern void  clientStart();
extern void  clientStop();
extern BOOL  clientGetRunMode();						
extern void  clientSetRunMode( BOOL bRunMode );						
extern BOOL  clientGetDynamicTargetSupportFlag();						
extern void  clientSetDynamicTargetSupportFlag( BOOL bSupportDynamicTargets );						
extern void  clientRelease();
extern INT32 clientGetInputData( UINT8* pBuf, INT32 nOffset, INT32 nLength);
extern INT32 clientSetInputData( UINT8* pBuf, INT32 nOffset, INT32 nLength);
extern INT32 clientGetOutputData( UINT8* pBuf, INT32 nOffset, INT32 nLength);
extern INT32 clientSetOutputData( UINT8* pBuf, INT32 nOffset, INT32 nLength);	
extern INT32 clientGetConnectionInputData( INT32 nConnectionInstance, UINT8* pBuf, INT32 nLength);
extern INT32 clientSetConnectionInputData( INT32 nConnectionInstance, UINT8* pBuf, INT32 nLength);
extern INT32 clientGetConnectionOutputData( INT32 nConnectionInstance, UINT8* pBuf, INT32 nLength);
extern INT32 clientSetConnectionOutputData( INT32 nConnectionInstance, UINT8* pBuf, INT32 nLength);
extern INT32 clientGetNumConnections();
extern INT32 clientGetConnectionInstances(INT32 *pnConnectionInstanceArray);
extern INT32 clientGetConnectionState(INT32 nConnectionInstance);
extern INT32 clientGetConnectionConfig(INT32 nConnectionInstance, EtIPConnectionConfig* pConfig);
extern void clientRegisterEventCallBack( LogEventCallbackType *pfnLogEvent );
extern INT32 clientSendUnconnectedRequest( const char* szNetworkPath, EtIPObjectRequest* pRequest);
extern INT32 clientGetResponse(INT32 nRequestId, EtIPObjectResponse* pResponse);
extern PLATFORM_THREAD_RET PLATFORM_THREAD_MOD clientMainThread( void* pParam );
extern void clientMainTask(UINT wTimerID, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2);
extern BOOL clientGetHostIPAddress( char* szHostIPAddress );
extern void clientRegisterObjectsForClientProcessing(INT32* pClassNumberList, INT32 nNumberOfClasses, int* pServiceNumberList, int nNumberOfServices);
extern INT32 clientGetClientRequest( INT32 nRequestId, EtIPObjectRequest* pRequest );
extern INT32 clientSendClientResponse( INT32 nRequestId, EtIPObjectResponse* pResponse ); 
extern void clientGetIdentityInfo( EtIPIdentityInfo* pInfo );
extern void clientSetIdentityInfo( EtIPIdentityInfo* pInfo );

#ifdef ET_IP_SCANNER
extern INT32 clientGetConnectionErrorInfo(INT32 nConnectionInstance, EtIPErrorInfo* pErrorInfo);
#endif

#endif /* #ifndef CLIENT_H */
