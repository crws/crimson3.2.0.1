
#include "Intern.hpp"

#include "Platform.h"

#include "ThreadPool.hpp"

#include "EtherNetIp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Data
//

global	IEvent		* ghTickEvent	= NULL;

static  IEvent	        * m_pTickEvent  = NULL;

static	CThreadPool	* m_pThreadPool = NULL;

static	IEtherNetIp	* m_pEtherNetIp	= NULL;

static	INetUtilities	* m_pUtils      = NULL;

////////////////////////////////////////////////////////////////////////
//
// Code
//

global void platformInit(void)
{
	m_pEtherNetIp  = NULL;

	m_pTickEvent   = Create_ManualEvent();

	m_pThreadPool  = New CThreadPool();

	m_pThreadPool->Open(2000, 1);

	AfxGetObject("enetip", 0, IEtherNetIp, m_pEtherNetIp);

	AfxGetObject("ip", 0, INetUtilities, m_pUtils);
}

global void platformStop(void)
{
	AfxRelease(m_pTickEvent);

	m_pThreadPool->Close();

	delete m_pThreadPool;

	AfxRelease(m_pEtherNetIp);

	AfxRelease(m_pUtils);
}

global IEvent * platformStartTimer(void)
{
	return m_pTickEvent;
}

global void platformStopTimer(void)
{
}

global BOOL platformCreateThread(ThreadFuncType pfnFunc, void *pParam, int iPriority)
{
	return m_pThreadPool->CreateThread(pfnFunc, pParam);
}

global void platformInitLogFile(void)
{
}

global void platformCloseLogFile(void)
{
}

global void platformWriteLog(PTXT pBuf, BOOL fTimeStamp)
{
	if( fTimeStamp ) {

		AfxTrace("%s\n", pBuf);
	}
}

global void platformSocketLibInit(void)
{
}

global void platformSocketLibCleanup()
{
}

global INT32 platformCreateSocket(WORD wID)
{
	ISocket *p = NULL;

	switch( wID ) {

		case IP_TCP:
			AfxNewObject("sock-tcp", ISocket, p);
			break;

		case IP_UDP:
			AfxNewObject("sock-udp", ISocket, p);
			break;

		case IP_RAW:
			AfxNewObject("sock-raw", ISocket, p);
			break;
	}

	return p ? UINT32(p) : INVALID_SOCKET;
}

global void platformCloseSocket(UINT32 &uSock, BOOL fAbort)
{
	if( uSock ) {

		PSOCK &pSock = (PSOCK &) uSock;

		if( fAbort ) {

			pSock->Abort();
		}
		else {
			pSock->Close();
		}

		pSock->Release();

		pSock = PSOCK(INVALID_SOCKET);
	}
}

global void platformGetMacID(PBYTE pMacId)
{
	CMacAddr &Mac = *(CMacAddr *) pMacId;

	CIpAddr Null;

	platformFindEthernet(Null, Null, Mac);
}

global void platformGetTcpIpCfgData(TCPIP_CONFIG_DATA *psCfg)
{
	memset(psCfg, 0, sizeof(TCPIP_CONFIG_DATA));

	CMacAddr MAC;

	CIpAddr  IP;

	platformFindEthernet(IP, (CIpAddr &) psCfg->lSubnetMask, MAC);

	psCfg->bDhcpState = TRUE;

	m_pUtils->GetDefaultGateway(IP, (IPADDR &) psCfg->lGatewayAddr);
}

global BOOL platformStartPing(INT32 nSession, BOOL* bPingSuccessfullyCompleted)
{
	// TODO -- Should be an async ping.

	*bPingSuccessfullyCompleted = TRUE;

	CIpAddr IP = gSessions[nSession].sClientAddr.sin_addr.s_un_b;

	return m_pUtils->Ping(IP, PING_TIMEOUT) != NOTHING;
}

global BOOL platformContinuePing(INT32 nSession, BOOL* bPingSuccessfullyCompleted)
{
	*bPingSuccessfullyCompleted = TRUE;

	return TRUE;
}

global BOOL platformPing(UINT32 lIPAddress)
{
	CIpAddr &IP = (CIpAddr &) lIPAddress;

	return m_pUtils->Ping(IP, PING_TIMEOUT) != NOTHING;
}

global BOOL platformFindEthernet(CIpAddr &IP, CIpAddr &Mask, CMacAddr &MAC)
{
	UINT c = m_pUtils->GetInterfaceCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_pUtils->IsInterfaceUp(n) ) {

			m_pUtils->GetInterfaceMac(n, MAC);

			if( !MAC.IsEmpty() ) {

				m_pUtils->GetInterfaceAddr(n, IP);

				m_pUtils->GetInterfaceMask(n, Mask);

				if( !IP.IsEmpty() && !IP.IsLoopback() ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

global UINT32 platformGetTickCount(void)
{
	return ToTime(GetTickCount());
}

global void platformSleep(UINT32 uTime)
{
	Sleep(uTime);
}

global IMutex * platformInitMutex(PCSTR pName)
{
	// Note mutex is created into owned state, as the calling
	// code immediately calls platformReleaseMutex. I could have
	// fix that there, but I don't want to change the stack.

	IMutex *pMutex = Create_Mutex();

	pMutex->Wait(FOREVER);

	return pMutex;
}

global void platformReleaseMutex(IMutex *pMutex)
{
	pMutex->Free();
}

global void platformDiscardMutex(IMutex *pMutex)
{
	pMutex->Release();
}

global void platformWaitMutex(IMutex *pMutex, UINT32 uTimeout)
{
	pMutex->Wait(uTimeout);
}

global void platformSetEvent(IEvent *pEvent)
{
	pEvent->Set();
}

global void platformWaitEvent(IEvent *pEvent, UINT32 uTimeout)
{
	pEvent->Wait(uTimeout);
}

// End of File
