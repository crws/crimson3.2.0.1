/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** TCPIP.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** TCP/IP object implementation 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"


TCPIP_INTERFACE_INSTANCE_ATTRIBUTES gsTcpIpInstAttr;

/*---------------------------------------------------------------------------
** tcpipInit( )
**
** Initialize TCP/IP object
**---------------------------------------------------------------------------
*/

void tcpipInit()
{		
	UINT8 str[512];
	TCPIP_CONFIG_DATA sCfgData;

	/* Set the User-defined bitmaps used for network configuration options
	** These should be or'd values of the TCPIP_CAPABILITY_xxx and TCPIP_CONTROL_xxx
	** literals */
	gsTcpIpInstAttr.lConfigCapability = TCPIP_CAPABILITY_DNS | TCPIP_CAPABILITY_DHCP;
	gsTcpIpInstAttr.lConfigControl = TCPIP_CONTROL_DNS_ENABLE;

	/* Set the User-defined path to the object associated with the underlying physical
	** communication interface (e.g., an [F6] Ethernet Link Object Class) */
	gsTcpIpInstAttr.iLinkObjPathSize = 4;		/* 4 bytes long */
	gsTcpIpInstAttr.LinkObjPath[0] = 0x20;		/* Class logical segment */
	gsTcpIpInstAttr.LinkObjPath[1] = 0xF6;		/* Ethernet Link class ID */
	gsTcpIpInstAttr.LinkObjPath[2] = 0x24;		/* Instance logical segment */
	gsTcpIpInstAttr.LinkObjPath[3] = 0x01;		/* Instance 1 */

	memset( &gsTcpIpInstAttr.InterfaceConfig, 0, sizeof(TCPIP_INTERFACE_CONFIG) );

	gsTcpIpInstAttr.InterfaceConfig.lIpAddr = htonl(gHostAddr[0].sin_addr.s_addr);

	if ( gsTcpIpInstAttr.InterfaceConfig.lIpAddr )
		gsTcpIpInstAttr.lInterfaceStatus = TCPIP_STATUS_VALID_CONFIGURATION;
	else
		gsTcpIpInstAttr.lInterfaceStatus = TCPIP_STATUS_NOT_CONFIGURED;

	memset(&sCfgData,0,sizeof(sCfgData));

	platformGetTcpIpCfgData( &sCfgData );

	gsTcpIpInstAttr.InterfaceConfig.lSubnetMask = sCfgData.lSubnetMask;
	gsTcpIpInstAttr.InterfaceConfig.lGatewayAddr = sCfgData.lGatewayAddr;
	gsTcpIpInstAttr.InterfaceConfig.lNameServer = sCfgData.lNameServer;
	gsTcpIpInstAttr.InterfaceConfig.lNameServer2 = sCfgData.lNameServer2;
	strcpy((char*)gsTcpIpInstAttr.InterfaceConfig.szDomainName, (const char*)sCfgData.szDomainName);
				
	gsTcpIpInstAttr.lConfigControl &= ~TCPIP_CONTROL_CONFIG_BITMASK;

	if( sCfgData.bDhcpState )
		gsTcpIpInstAttr.lConfigControl |= TCPIP_CONTROL_DHCP;
	else
		gsTcpIpInstAttr.lConfigControl |= TCPIP_CONTROL_USE_STORED_CONFIG;

	/* Get host name of the local machine (or device) */
	if ( gethostname((char*)str, TCPIP_MAX_HOST_LENGTH) == INVALID_SOCKET )
	{
		memset(gsTcpIpInstAttr.szHostName,0,sizeof(gsTcpIpInstAttr.szHostName));
	}
	else
	{
	    strcpy((char*)gsTcpIpInstAttr.szHostName, (const char*)str);
		DumpStr1( "Host Name: %s", str );
	}   

	
}


/*---------------------------------------------------------------------------
** tcpipParseClassInstanceRequest( )
**
** Determine if it's request for the Class or for the particular instance 
**---------------------------------------------------------------------------
*/

void tcpipParseClassInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 0 )
		tcpipParseClassRequest( nRequest );
	else
		tcpipParseInstanceRequest( nRequest );
}

/*---------------------------------------------------------------------------
** tcpipParseClassRequest( )
**
** Respond to the TCP/IP class request
**---------------------------------------------------------------------------
*/

void tcpipParseClassRequest( INT32 nRequest )
{
	switch( gRequests[nRequest].bService )
	{
		case SVC_GET_ATTR_ALL:
			tcpipSendClassAttrAll( nRequest );
			break;
		case SVC_GET_ATTR_SINGLE:
			tcpipSendClassAttrSingle( nRequest );
			break;
		default:		
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
			break;
	}
}

/*---------------------------------------------------------------------------
** tcpipSendClassAttrAll( )
**
** GetAttributeAll service for TCP/IP class is received 
**---------------------------------------------------------------------------
*/

void tcpipSendClassAttrAll( INT32 nRequest )
{
	TCPIP_CLASS_ATTR attr;

	attr.iRevision = ENCAP_TO_HS(TCPIP_CLASS_REVISION);
	attr.iMaxInstance = ENCAP_TO_HS((UINT16)nHostAddrCount);
	attr.iNumInstances = ENCAP_TO_HS((UINT16)nHostAddrCount);
		
	utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&attr, sizeof(TCPIP_CLASS_ATTR_SIZE) );			
}

/*---------------------------------------------------------------------------
** tcpipSendClassAttrSingle( )
**
** GetAttributeSingle service for TCP/IP class is received 
**---------------------------------------------------------------------------
*/

void tcpipSendClassAttrSingle( INT32 nRequest )
{	
	UINT16 iVal;
	
	switch( gRequests[nRequest].iAttribute )
	{
		case TCPIP_CLASS_ATTR_REVISION:
			iVal = ENCAP_TO_HS(TCPIP_CLASS_REVISION);			
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );			
			break;
		case TCPIP_CLASS_ATTR_MAX_INSTANCE:
			iVal = ENCAP_TO_HS((UINT16)nHostAddrCount);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );			
			break;
		case TCPIP_CLASS_ATTR_NUM_INSTANCES:
			iVal = ENCAP_TO_HS((UINT16)nHostAddrCount);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );			
			break;		
		default:
			utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}		
}


/*---------------------------------------------------------------------------
** tcpipParseInstanceRequest( )
**
** Respond to the TCP/IP instance request
**---------------------------------------------------------------------------
*/

void tcpipParseInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance >= 1 && gRequests[nRequest].iInstance <= nHostAddrCount )
	{
		switch( gRequests[nRequest].bService )
		{
			case SVC_GET_ATTR_ALL:
				tcpipSendInstanceAttrAll( nRequest );
				break;
			case SVC_GET_ATTR_SINGLE:
				tcpipSendInstanceAttrSingle( nRequest );
				break;
			case SVC_SET_ATTR_ALL:
				tcpipSetInstanceAttrAll( nRequest );
				break;
			case SVC_SET_ATTR_SINGLE:
				tcpipSetInstanceAttrSingle( nRequest );
				break;
			default:
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
				break;
		}
	}
	else
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;
}


/*---------------------------------------------------------------------------
** tcpipSendInstanceAttrAll( )
**
** GetAttributeAll service for TCP/IP instance is received
**---------------------------------------------------------------------------
*/

void tcpipSendInstanceAttrAll( INT32 nRequest )
{
	UINT8* pData;
	INT16  iHostNameLen, iDomainNameLen;

	utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );

	gsTcpIpInstAttr.InterfaceConfig.lIpAddr = htonl(gHostAddr[gRequests[nRequest].iInstance-1].sin_addr.s_addr);

	if ( gsTcpIpInstAttr.InterfaceConfig.lIpAddr )
		gsTcpIpInstAttr.lInterfaceStatus = TCPIP_STATUS_VALID_CONFIGURATION;
	else
		gsTcpIpInstAttr.lInterfaceStatus = TCPIP_STATUS_NOT_CONFIGURED;
		
	iDomainNameLen = INT16(strlen((const char*)gsTcpIpInstAttr.InterfaceConfig.szDomainName));
	iHostNameLen = INT16(strlen((const char*)gsTcpIpInstAttr.szHostName));
	
	gRequests[nRequest].iDataSize = sizeof(UINT32) * 3 + sizeof(UINT16) + gsTcpIpInstAttr.iLinkObjPathSize + TCPIP_INTERFACE_CONFIG_SIZE +
		iHostNameLen + sizeof(UINT16) + iDomainNameLen + sizeof(UINT16); 

	if ( iDomainNameLen & 1 )
		gRequests[nRequest].iDataSize++;

	if ( iHostNameLen & 1 )
		gRequests[nRequest].iDataSize++;

    gRequests[nRequest].iDataOffset = utilAddToMemoryPool( NULL, gRequests[nRequest].iDataSize );
	
	if ( gRequests[nRequest].iDataSize && gRequests[nRequest].iDataOffset == INVALID_MEMORY_OFFSET )
	{
		notifyEvent( NM_OUT_OF_MEMORY, 0 );
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_NO_RESOURCE;	/* Out of memory */	
		return;
	}

	pData = MEM_PTR( gRequests[nRequest].iDataOffset );
			
	UINT32_SET( pData, gsTcpIpInstAttr.lInterfaceStatus );
	pData += sizeof(UINT32);
	
	UINT32_SET( pData, gsTcpIpInstAttr.lConfigCapability );
	pData += sizeof(UINT32);
	
	UINT32_SET( pData, gsTcpIpInstAttr.lConfigControl );
	pData += sizeof(UINT32);
	
	UINT16_SET( pData, gsTcpIpInstAttr.iLinkObjPathSize/2 );
	pData += sizeof(UINT16);
	
	if ( gsTcpIpInstAttr.iLinkObjPathSize )
	{
		memcpy( pData, gsTcpIpInstAttr.LinkObjPath, gsTcpIpInstAttr.iLinkObjPathSize );
		pData += gsTcpIpInstAttr.iLinkObjPathSize;	
	}

	memcpy( pData, (UINT8*)&gsTcpIpInstAttr.InterfaceConfig, TCPIP_INTERFACE_CONFIG_SIZE );
	pData += TCPIP_INTERFACE_CONFIG_SIZE;
	
	UINT16_SET( pData, iDomainNameLen );
	pData += sizeof(UINT16);
	strncpy( (char*)pData, (const char*)gsTcpIpInstAttr.InterfaceConfig.szDomainName, iDomainNameLen );	
	pData += iDomainNameLen;
	if ( iDomainNameLen & 1 )
		*pData++ = 0;
	
	UINT16_SET( pData, iHostNameLen );
	pData += sizeof(UINT16);
	strncpy( (char*)pData, (const char*)gsTcpIpInstAttr.szHostName, iHostNameLen );
	pData += iHostNameLen;
	if ( iHostNameLen & 1 )
		*pData = 0;				
}


/*--------------------------------------------------------------------------------
** tcpipSendInstanceAttrSingle( )
**
** GetAttributeSingle request has been received for the TCP/IP object instance
**--------------------------------------------------------------------------------
*/

void tcpipSendInstanceAttrSingle( INT32 nRequest )
{	
	UINT8* pData;
	UINT32 lVal;
	UINT16 iDataSize;
	UINT16 iLen;
	
	switch( gRequests[nRequest].iAttribute )
	{
		case TCPIP_INST_ATTR_INTERFACE_STATUS:
			lVal = ENCAP_TO_HL(gsTcpIpInstAttr.lInterfaceStatus);			
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&lVal, sizeof(UINT32) );			
			break;

		case TCPIP_INST_ATTR_CONFIG_CAPABILITY:
			lVal = ENCAP_TO_HL(gsTcpIpInstAttr.lConfigCapability);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&lVal, sizeof(UINT32) );			
			break;

		case TCPIP_INST_ATTR_CONFIG_CONTROL:
			lVal = ENCAP_TO_HL(gsTcpIpInstAttr.lConfigControl);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&lVal, sizeof(UINT32) );			
			break;

		case TCPIP_INST_ATTR_LINK_PATH:
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, NULL, (UINT16)(sizeof(UINT16) + gsTcpIpInstAttr.iLinkObjPathSize) );			
			if ( gRequests[nRequest].iDataOffset != INVALID_MEMORY_OFFSET )
			{
				pData = MEM_PTR( gRequests[nRequest].iDataOffset );
				
				UINT16_SET( pData, gsTcpIpInstAttr.iLinkObjPathSize/2 );
				pData += sizeof(UINT16);
				
				if ( gsTcpIpInstAttr.iLinkObjPathSize )
					memcpy( pData, gsTcpIpInstAttr.LinkObjPath, gsTcpIpInstAttr.iLinkObjPathSize );
			}
			break;

		case TCPIP_INST_ATTR_INTERFACE_CONFIG:
			gsTcpIpInstAttr.InterfaceConfig.lIpAddr = htonl(gHostAddr[gRequests[nRequest].iInstance-1].sin_addr.s_addr);
			if ( gsTcpIpInstAttr.InterfaceConfig.lIpAddr )
				gsTcpIpInstAttr.lInterfaceStatus = TCPIP_STATUS_VALID_CONFIGURATION;
			else
				gsTcpIpInstAttr.lInterfaceStatus = TCPIP_STATUS_NOT_CONFIGURED;

			iLen = UINT16(strlen((const char*)gsTcpIpInstAttr.InterfaceConfig.szDomainName));
			iDataSize = TCPIP_INTERFACE_CONFIG_SIZE + iLen + sizeof(UINT16);
			if ( iDataSize & 1)  /* Pad with 0 if domain name length is odd */
				iDataSize++;
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, NULL, iDataSize );			
			if ( gRequests[nRequest].iDataOffset != INVALID_MEMORY_OFFSET )
			{
				pData = MEM_PTR( gRequests[nRequest].iDataOffset );
				memcpy( pData, (UINT8*)&gsTcpIpInstAttr.InterfaceConfig, TCPIP_INTERFACE_CONFIG_SIZE );
				pData += TCPIP_INTERFACE_CONFIG_SIZE;
				UINT16_SET( pData, iLen );
				pData += sizeof(UINT16);
				strncpy( (char*)pData, (const char*)gsTcpIpInstAttr.InterfaceConfig.szDomainName, iLen );
				if ( iLen & 1)  /* Pad with 0 if domain name length is odd */
				{
					pData += iLen;
					*pData = 0;
				}
			}
			break;

		case TCPIP_INST_ATTR_HOST_NAME:
			iLen = UINT16(strlen((const char*)gsTcpIpInstAttr.szHostName));
			iDataSize = iLen + sizeof(UINT16);
			if ( iDataSize & 1)  /* Pad with 0 if host name length is odd */
				iDataSize++;
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, NULL, iDataSize );			
			if ( gRequests[nRequest].iDataOffset != INVALID_MEMORY_OFFSET )
			{
				pData = MEM_PTR( gRequests[nRequest].iDataOffset );				
				UINT16_SET( pData, iLen );
				pData += sizeof(UINT16);
				strncpy( (char*)pData, (const char*)gsTcpIpInstAttr.szHostName, iLen );
				if ( iLen & 1)  /* Pad with 0 if domain name length is odd */
				{
					pData += iLen;
					*pData = 0;
				}
			}
			break;		
		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}	
}

/*---------------------------------------------------------------------------
** tcpipSetInstanceAttrAll( )
**
** SetAttributeAll service for TCP/IP instance is received.
** Only interface config data is transferred.
**---------------------------------------------------------------------------
*/

void tcpipSetInstanceAttrAll( INT32 nRequest )
{
	UINT8* pData = MEM_PTR(gRequests[nRequest].iDataOffset);
	UINT16 iLen;
	
	gsTcpIpInstAttr.lConfigControl = UINT32_GET( pData );
	pData += sizeof(UINT32);
	
	memcpy( (UINT8*)&gsTcpIpInstAttr.InterfaceConfig, pData, TCPIP_INTERFACE_CONFIG_SIZE );
	pData += TCPIP_INTERFACE_CONFIG_SIZE;

	iLen = UINT16_GET(pData);
	if ( iLen > TCPIP_MAX_DOMAIN_LENGTH )
		iLen = TCPIP_MAX_DOMAIN_LENGTH;
	pData += sizeof(UINT16);
	
	strncpy( (char*)gsTcpIpInstAttr.InterfaceConfig.szDomainName, 
				(const char*)pData, 
				iLen );
	gsTcpIpInstAttr.InterfaceConfig.szDomainName[iLen] = 0;
			
	utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );	
}

/*--------------------------------------------------------------------------------
** tcpipSetInstanceAttrSingle( )
**
** SetAttributeSingle request has been received for the TCP/IP object instance
**--------------------------------------------------------------------------------
*/

void tcpipSetInstanceAttrSingle( INT32 nRequest )
{	
	UINT8* pData = MEM_PTR(gRequests[nRequest].iDataOffset);
	UINT16 iLen;

	switch( gRequests[nRequest].iAttribute )
	{		
		case TCPIP_INST_ATTR_CONFIG_CONTROL:
			gsTcpIpInstAttr.lConfigControl = UINT32_GET( pData );			
			break;

		case TCPIP_INST_ATTR_INTERFACE_CONFIG:
			memcpy( (UINT8*)&gsTcpIpInstAttr.InterfaceConfig, pData, TCPIP_INTERFACE_CONFIG_SIZE );
			pData += TCPIP_INTERFACE_CONFIG_SIZE;
			iLen = UINT16_GET(pData);
			if ( iLen > TCPIP_MAX_DOMAIN_LENGTH )
				iLen = TCPIP_MAX_DOMAIN_LENGTH;
			pData += sizeof(UINT16);
			strncpy( (char*)gsTcpIpInstAttr.InterfaceConfig.szDomainName, (const char*)pData, iLen );
			gsTcpIpInstAttr.InterfaceConfig.szDomainName[iLen] = 0;			
			break;

		case TCPIP_INST_ATTR_HOST_NAME:
			iLen = UINT16_GET( pData );
			pData += sizeof(UINT16);
			if ( iLen > TCPIP_MAX_HOST_LENGTH )
				iLen = TCPIP_MAX_HOST_LENGTH;
			strncpy( (char*)gsTcpIpInstAttr.szHostName, (const char*)pData, iLen );
			gsTcpIpInstAttr.szHostName[iLen] = 0;						
			break;		
		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			return;
	}	

	utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );	
}














