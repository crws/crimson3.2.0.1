
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Explicit_HPP

#define INCLUDE_Explicit_HPP

//////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Explicit
//

class CExplicit : public IExplicit
{
	public:
		// Constructor
		CExplicit(void);
		
		// Destructor
		~CExplicit(void);

		// Binding
		void Bind(UINT uIdx);
		UINT GetIndex(void) const;

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IExplicit
		void METHOD SetTimeout(UINT uTimeout);
		BOOL METHOD Send(CIPADDR &Addr)	;
		BOOL METHOD Send(CIPADDR &Addr, PBYTE pData, UINT uSize);
		BOOL METHOD Send(CIPADDR &Addr, CBuffer *pMsg);
		BOOL METHOD Recv(PBYTE pData, UINT &uSize);
		BOOL METHOD Recv(CBuffer * &pMsg);
		BOOL METHOD Abort(IPADDR const &Addr);
		BYTE METHOD GetLastError(void);

		// Entry Points
		void OnEvent(INT32 nEvent, INT32 nParam);

	protected:
		// Data
		ULONG		     m_uRefs;
		EtIPObjectRequest  * m_pRequest;
		EtIPObjectResponse * m_pResponse;
		IEvent             * m_pReqFlag;
		BYTE		     m_bError;
		INT32                m_ReqId;
		UINT                 m_uIndex;

		// Event Handlers
		void OnResponse(INT32 ReqId);
		void OnTimeout(INT32 ReqId);
		void OnFailed(INT32 ReqId);
	};

// End of File

#endif
