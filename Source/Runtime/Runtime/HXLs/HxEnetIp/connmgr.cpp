/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** CONNMGR.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** This module contains the implementation of the Connection Manager class
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"


/*---------------------------------------------------------------------------
** connmgrIncomingConnection( )
**
** Incoming connection request received - store addresses and parse
** Forward Open
**---------------------------------------------------------------------------
*/
void  connmgrIncomingConnection( INT32 nRequest )
{
	INT32		nConnection;
	INT32		nSession;
	FWD_OPEN    FwdOpen;
	//ENCAPH*     pHdr = ( ENCAPH * )gmsgBuf;
	INT32       i;
	EPATH       path;
	INT32       nPathSize;
	UINT8*      pPacketPtr = MEM_PTR(gRequests[nRequest].iDataOffset);
	INT32       nOTConnection = INVALID_CONNECTION;
	INT32       nTOConnection = INVALID_CONNECTION;
	INT32       nOrigTOConnection = INVALID_CONNECTION;
	INT32       nMulticastProducer;
	INT32       pdu_len;
	PDU_HDR     pdu_hdr;
		
	nSession = sessionFindAddressEstablished( gRequests[nRequest].lIPAddress, Incoming );

	if ( nSession == INVALID_SESSION )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
		gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_VENDOR_SPECIFIC;
		DumpStr1("connmgrIncomingConnection ERROR: unrecognized IP address 0x%x", gRequests[nRequest].lIPAddress);
		return;
	}

	/* Must have the class and instance logical segments */
	if ( gRequests[nRequest].iDataSize < (FWD_OPEN_SIZE + 4) )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_NOT_ENOUGH_DATA;
		gRequests[nRequest].iExtendedError = 0;		
		return;
	}

	nConnection = connectionNew( gRequests[nRequest].lIPAddress );  

	if ( nConnection == ERROR_STATUS )		 /* Can not exceed the maximum number of concurent connections limit */
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_NO_RESOURCE;
		notifyEvent( NM_CONNECTION_COUNT_LIMIT_REACHED, 0 );
		connectionRemove( nConnection );
		return;
	}
	
	gConnections[nConnection].cfg.bOriginator = FALSE;
	gConnections[nConnection].cfg.iWatchdogTimeoutAction = TimeoutAutoDelete;
			
	if ( gotTag.sTag.iTag_type == CPF_TAG_SOCKADDR_OT && gotTag.sAddr.sin_addr.s_addr != 0 )
		memcpy( &gConnections[nConnection].sReceiveAddr, &gotTag.sAddr, sizeof(struct sockaddr_in) );
	
	if ( gtoTag.sTag.iTag_type == CPF_TAG_SOCKADDR_TO && gtoTag.sAddr.sin_addr.s_addr != 0 )
		memcpy( &gConnections[nConnection].sTransmitAddr, &gtoTag.sAddr, sizeof(struct sockaddr_in) );
	
	
	/* Parse Forward Open parameters */
	connmgrInitFwdOpenTypeFromBuf( pPacketPtr, &FwdOpen );   

	gConnections[nConnection].bOpenPriorityTickTime = FwdOpen.bOpenPriorityTickTime;
	gConnections[nConnection].bOpenTimeoutTicks = FwdOpen.bOpenTimeoutTicks;
	
    gConnections[nConnection].cfg.bTimeoutMultiplier = FwdOpen.bTimeoutMult;
    gConnections[nConnection].cfg.lProducingDataRate = FwdOpen.lLeTORpi;
    gConnections[nConnection].cfg.lConsumingDataRate = FwdOpen.lLeOTRpi;

	gConnections[nConnection].lConsumingCID = FwdOpen.lLeOTConnId;
	gConnections[nConnection].lProducingCID = FwdOpen.lLeTOConnId;

	gConnections[nConnection].lAddrSeqNbr = 0;
	gConnections[nConnection].iOutDataSeqNbr = 0;
	gConnections[nConnection].iInDataSeqNbr = 0xffff;
        		
    gConnections[nConnection].cfg.iInputDataSize    = ( FwdOpen.iLeOTConnParams & DATA_SIZE_MASK ) - 2;
	gConnections[nConnection].cfg.iOutputDataSize    = ( FwdOpen.iLeTOConnParams & DATA_SIZE_MASK ) - 2;
	
	gConnections[nConnection].cfg.iConsumingConnectionType = FwdOpen.iLeOTConnParams & CM_CP_TYPE_MASK;
	gConnections[nConnection].cfg.iProducingConnectionType = FwdOpen.iLeTOConnParams & CM_CP_TYPE_MASK;
	
	gConnections[nConnection].cfg.iConsumingPriority = FwdOpen.iLeOTConnParams & CM_CP_PRIORITY_MASK;
	gConnections[nConnection].cfg.iProducingPriority = FwdOpen.iLeTOConnParams & CM_CP_PRIORITY_MASK;
	
    gConnections[nConnection].cfg.bTransportClass         = FwdOpen.bClassTrigger & TRANSPORT_CLASS_MASK;
	gConnections[nConnection].cfg.bTransportType          = FwdOpen.bClassTrigger & CLASS_TYPE_MASK;

	if ( ( gConnections[nConnection].cfg.iConsumingConnectionType != PointToPoint ) || 
		 ( gConnections[nConnection].cfg.iProducingConnectionType != PointToPoint && gConnections[nConnection].cfg.iProducingConnectionType != Multicast ) )
	{	
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
		gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_INVALID_CONN_TYPE;
		connectionRemove( nConnection );
		return;
	}

	/* Support only Class 1 and Class 3 connections */
	if ( ( gConnections[nConnection].cfg.bTransportClass != Class1 && gConnections[nConnection].cfg.bTransportClass != Class3 ) ||
		 ( gConnections[nConnection].cfg.bTransportType != Cyclic && 
		   gConnections[nConnection].cfg.bTransportType != ChangeOfState && gConnections[nConnection].cfg.bTransportType != ApplicationTriggered ) )
	{	
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
		gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_CLASS_TRIGGER_INVALID;		
		connectionRemove( nConnection );
		return;
	}

	/* Connection Serial number, Vendor Id and Device Serial Number combination
	   is unique and will identify this connection */
	gConnections[nConnection].iConnectionSerialNbr   = FwdOpen.iLeOrigConnectionSn;
    gConnections[nConnection].iVendorID              = FwdOpen.iLeOrigVendorId;
    gConnections[nConnection].lDeviceSerialNbr       = FwdOpen.lLeOrigDeviceSn;

	for( i = 0; i < gnConnections; i++ )
	{
		/* Check if we already have a connection with the same serial numbers,
		   then it's a duplicate Forward Open */
		if ( i != nConnection && gConnections[i].lConnectionState == ConnectionEstablished &&
			 gConnections[i].iConnectionSerialNbr == gConnections[nConnection].iConnectionSerialNbr &&
			 gConnections[i].iVendorID == gConnections[nConnection].iVendorID &&
			 gConnections[i].lDeviceSerialNbr == gConnections[nConnection].lDeviceSerialNbr )
		{
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
			gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_DUPLICATE_FWD_OPEN;					
			connectionRemove( nConnection );
			return;
		}
	}

	pPacketPtr += (FWD_OPEN_SIZE - 2);

	memcpy( &pdu_hdr, pPacketPtr, PDU_HDR_SIZE );	/* The first 2 bytes is PDU header */
	pdu_len = pdu_hdr.bSize * 2;					/* PDU Length not including the PDU header */
	
	/* Check if not enough data */
	if ( gRequests[nRequest].iDataSize < (FWD_OPEN_SIZE + pdu_len) )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_NOT_ENOUGH_DATA;
		gRequests[nRequest].iExtendedError = 0;		
		connectionRemove( nConnection );
		return;
	}
	
	/* Check if too much data */
	if ( gRequests[nRequest].iDataSize > (FWD_OPEN_SIZE + pdu_len) )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_TOO_MUCH_DATA;
		gRequests[nRequest].iExtendedError = 0;		
		connectionRemove( nConnection );
		return;
	}

	/* Parse Message Router Protocol Data Unit part which is part of the connection path */
	nPathSize = routerParsePDU( pPacketPtr, &path );

	if ( !( path.iFilled & EPATH_INSTANCE ) ) /* Instance must be specified */
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
		gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_INVALID_SEGMENT_TYPE_VALUE;
		connectionRemove( nConnection );
		return;
	}

	gConnections[nConnection].cfg.iConfigConnInstance = path.iInstance;
			
	/* Must have the class and instance logical segments */
	if ( nPathSize >= ROUTER_ERROR_BASE || 
		 ( (path.iFilled & EPATH_PORT) && (path.iPort == 0) )  ||
		 ( !(path.iFilled & EPATH_CLASS) ) || 
		 ( ( (path.iFilled & EPATH_CLASS ) && (path.iClass == INVALID_CLASS || path.iClass == INVALID_CLASS_8BIT) ) ||
		   ( (path.iFilled & EPATH_INSTANCE ) && (path.iInstance == INVALID_INSTANCE || path.iInstance == INVALID_INSTANCE_8BIT) ) ) )
	{	
		DumpStr4("connmgrIncomingConnection ret error 0x315, iFilled = 0x%x, port = %d, class = %d, instance = %d",
			path.iFilled, path.iPort, path.iClass, path.iInstance);
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
		gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_INVALID_SEGMENT_TYPE_VALUE;							
		connectionRemove( nConnection );
		return;
	}	
	
	if ( path.iFilled & EPATH_INSTANCE )
		gConnections[nConnection].cfg.iConfigConnInstance = path.iInstance;
	else
		gConnections[nConnection].cfg.iConfigConnInstance = INVALID_INSTANCE;

	if ( path.iFilled & EPATH_CONSUMING_CONN_POINT )
		gConnections[nConnection].cfg.iConsumingConnPoint = path.iConsumingConnPoint;
	else
		gConnections[nConnection].cfg.iConsumingConnPoint = INVALID_CONN_POINT;

	if ( path.iFilled & EPATH_PRODUCING_CONN_POINT )
		gConnections[nConnection].cfg.iProducingConnPoint = path.iProducingConnPoint;
	else
		gConnections[nConnection].cfg.iProducingConnPoint = INVALID_CONN_POINT;

	if ( path.iFilled & EPATH_SLOT_ADDRESS )
		gConnections[nConnection].cfg.bSlot = path.bSlot;
	else
		gConnections[nConnection].cfg.bSlot = INVALID_SLOT_INDEX;

	if ( path.iFilled & EPATH_EXT_SYMBOL )
	{
		gConnections[nConnection].cfg.iConnectionTagSize = path.bExtSymbolSize;
		gConnections[nConnection].cfg.iConnectionTagOffset = utilAddToMemoryPool( path.bExtSymbol, gConnections[nConnection].cfg.iConnectionTagSize );			
	}

	if ( path.iFilled & EPATH_DATA )
	{
		gConnections[nConnection].cfg.iModuleConfig1Size = path.iDataSize;
		gConnections[nConnection].cfg.iModuleConfig1Offset = utilAddToMemoryPool( path.bData, gConnections[nConnection].cfg.iModuleConfig1Size );			
	}	
	
	/* Only one production inhibit interval segment may be included and it's for adapter's production */
	if ( path.iFilled & EPATH_OT_INHIBIT_INTERVAL )
		gConnections[nConnection].cfg.bProductionTOInhibitInterval = path.bProductionOTInhibitInterval;
	else
		gConnections[nConnection].cfg.bProductionTOInhibitInterval = 0;
	gConnections[nConnection].cfg.bProductionOTInhibitInterval = 0;

	/* If production inhibit interval is larger than producing rate - return an error */
	if ( (gConnections[nConnection].cfg.bProductionTOInhibitInterval) && ((UINT32)(gConnections[nConnection].cfg.bProductionTOInhibitInterval) > (gConnections[nConnection].cfg.lProducingDataRate/1000)))
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
		gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_RPI_NOT_SUPPORTED;										
		connectionRemove( nConnection );
		return;
	}

	/* Reset timeout ticks based on the connection settings */
	connectionRecalcTimeouts( nConnection );

	/* Check if keying is being used, then it must match the target Ids */
	if ( path.iFilled & EPATH_DEVICE_ID )
	{
		/* Check if Vendor Id and Product code are filled and match the target ones */
		if ( ( path.deviceId.iVendorID && path.deviceId.iVendorID != gIDInfo.iVendor ) ||
			 ( path.deviceId.iProductCode && path.deviceId.iProductCode != gIDInfo.iProductCode ) )
		{
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
			gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_VENDOR_PRODUCT_CODE_MISMATCH;										
			connectionRemove( nConnection );
			return;
		}
		
		/* Check if Product type is filled and match the target one */
		if ( path.deviceId.iProductType && path.deviceId.iProductType != gIDInfo.iProductType )
		{
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
			gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_PRODUCT_TYPE_MISMATCH;										
			connectionRemove( nConnection );
			return;
		}

		/* Check if Revisions are filled and match the target ones */
		if ( ( (path.deviceId.bMajorRevision&MAJOR_REVISION_MASK) &&
			   (path.deviceId.bMajorRevision&MAJOR_REVISION_MASK) != gIDInfo.bMajorRevision ) ||
			 ( path.deviceId.bMinorRevision > gIDInfo.bMinorRevision && 
			   !( (path.deviceId.bMajorRevision&MAJOR_REVISION_MASK) == 0 && 
			      (path.deviceId.bMajorRevision&REVISION_RELAXED_FLAG) ) ) )			
		{
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
			gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_REVISION_MISMATCH;													
			connectionRemove( nConnection );
			return;
		}		

		memcpy( &gConnections[nConnection].cfg.deviceId, &path.deviceId, sizeof(EtIPDeviceID) );
	}
		
	/* Check what incoming connection are already configured for these connection points */	
	for( i = 0; i < gnConnections; i++ )
	{
		if ( i == nConnection || gConnections[i].cfg.bOriginator )
			continue;

		if ( gConnections[i].cfg.iConfigConnInstance == gConnections[nConnection].cfg.iConfigConnInstance ) 
		{
			if ( gConnections[i].cfg.iConsumingConnPoint != HEARTBEAT_CONN_POINT && gConnections[i].cfg.iConsumingConnPoint != INVALID_CONN_POINT && gConnections[i].cfg.iConsumingConnPoint == gConnections[nConnection].cfg.iConsumingConnPoint )
				nOTConnection = i;			
			if ( gConnections[i].cfg.iProducingConnPoint != HEARTBEAT_CONN_POINT && gConnections[i].cfg.iProducingConnPoint != INVALID_CONN_POINT && gConnections[i].cfg.iProducingConnPoint == gConnections[nConnection].cfg.iProducingConnPoint )
				nTOConnection = i;			
		}
	}

	/* If a new instance */
	if ( nOTConnection == INVALID_CONNECTION && nTOConnection == INVALID_CONNECTION )
	{
#ifdef ET_IP_SCANNER
		if ( !gbSupportDynamicTargets )	/* Verify that incoming connections can be accepted without the prior configuration through a CC object */			
		{
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
			gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_DEVICE_NOT_CONFIGURED;
			connectionRemove( nConnection );
			return;
		}
		else
		{
#endif
			gConnections[nConnection].cfg.nInstance = connectionGetUnusedInstance();
			gConnections[nConnection].cfg.bInputRunProgramHeader = TRUE;	/* default values for incoming connection */
			gConnections[nConnection].cfg.bOutputRunProgramHeader = FALSE;
			gConnections[nConnection].cfg.iInputDataOffset = INVALID_OFFSET;
			gConnections[nConnection].cfg.iOutputDataOffset = INVALID_OFFSET;
#ifdef ET_IP_SCANNER			
			gConnections[nConnection].bCCConfigured = FALSE;				/* Forward Open was not preceeded with the CC config request for an incoming connection */			
			connectionSetConnectionFlag( nConnection );
			glEditSignature = 0;
		}
#endif
	}
	else 
	{
		gConnections[nConnection].bCCConfigured = TRUE;				/* Forward Open was preceeded with the CC config request for an incoming connection */

		if ( nOTConnection != INVALID_CONNECTION )
		{
			/* Check if there is already exclusive owner for this O->T connection point */
			if ( gConnections[nOTConnection].lConnectionState != ConnectionConfiguring || gConnections[nOTConnection].lConfigurationState != ConfigurationLogged )
			{
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
				gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_OWNERSHIP_CONFLICT;
				connectionRemove( nConnection );
				return;		
			}
#ifdef ET_IP_SCANNER
			else /* Connection was configured through CC object - transfer parameters */
			{
				gConnections[nConnection].cfg.nInstance = gConnections[nOTConnection].cfg.nInstance; 		
				gConnections[nConnection].cfg.bInputRunProgramHeader = gConnections[nOTConnection].cfg.bInputRunProgramHeader;
				gConnections[nConnection].cfg.iInputDataOffset = gConnections[nOTConnection].cfg.iInputDataOffset;
				memcpy( &gConnections[nConnection].cfg.deviceId, &gConnections[nOTConnection].cfg.deviceId, sizeof(EtIPDeviceID) );
				memcpy( &gConnections[nConnection].cfg.proxyDeviceId, &gConnections[nOTConnection].cfg.proxyDeviceId, sizeof(EtIPDeviceID) );
				memcpy( &gConnections[nConnection].cfg.remoteDeviceId, &gConnections[nOTConnection].cfg.remoteDeviceId, sizeof(EtIPDeviceID) );
				gConnections[nConnection].cfg.iConnectionNameOffset = gConnections[nOTConnection].cfg.iConnectionNameOffset;
				gConnections[nConnection].cfg.iConnectionNameSize = gConnections[nOTConnection].cfg.iConnectionNameSize;
				gConnections[nConnection].cfg.iConnectionFlag = gConnections[nOTConnection].cfg.iConnectionFlag;

				if ( nTOConnection != INVALID_CONNECTION )
				{
					gConnections[nConnection].nCCProducingInstance = gConnections[nTOConnection].cfg.nInstance; 	
					gConnections[nConnection].cfg.bOutputRunProgramHeader = gConnections[nTOConnection].cfg.bOutputRunProgramHeader;
					if ( gConnections[nConnection].cfg.bOutputRunProgramHeader )
						gConnections[nConnection].cfg.iOutputDataSize -= sizeof(UINT32);				
					gConnections[nConnection].cfg.iOutputDataOffset = gConnections[nTOConnection].cfg.iOutputDataOffset;				
				}
				else
				{
					gConnections[nConnection].nCCProducingInstance = INVALID_CONNECTION;
					gConnections[nConnection].cfg.bOutputRunProgramHeader = FALSE;								
				}
			}
#endif			
		}	
		else 
		{						
			if ( gConnections[nTOConnection].lConnectionState != ConnectionConfiguring || gConnections[nTOConnection].lConfigurationState != ConfigurationLogged )
			{
				if ( gConnections[nConnection].cfg.iProducingConnectionType == Multicast &&
					 gConnections[nTOConnection].cfg.iProducingConnectionType == Multicast )
				{							
					gConnections[nConnection].cfg.nInstance = connectionGetUnusedInstance();
					gConnections[nConnection].cfg.bInputRunProgramHeader = gConnections[nTOConnection].cfg.bInputRunProgramHeader;	/* default values for incoming connection */
					gConnections[nConnection].cfg.bOutputRunProgramHeader = gConnections[nTOConnection].cfg.bOutputRunProgramHeader;
					gConnections[nConnection].cfg.iInputDataOffset = INVALID_OFFSET;
					gConnections[nConnection].cfg.iOutputDataOffset = INVALID_OFFSET; /* We'll copy the appropriate output offset from another multicast later */
					gConnections[nConnection].bCCConfigured = gConnections[nTOConnection].bCCConfigured;
				}
				else
				{
					gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
					gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_OWNERSHIP_CONFLICT;
					connectionRemove( nConnection );
					return;		
				}
			}	
#ifdef ET_IP_SCANNER			
			else 
			{
				gConnections[nConnection].cfg.nInstance = gConnections[nTOConnection].cfg.nInstance; 		
				gConnections[nConnection].cfg.bInputRunProgramHeader = FALSE;
				gConnections[nConnection].nCCProducingInstance = INVALID_CONNECTION;
				gConnections[nConnection].cfg.bOutputRunProgramHeader = gConnections[nTOConnection].cfg.bOutputRunProgramHeader;
				if ( gConnections[nConnection].cfg.bOutputRunProgramHeader )
					gConnections[nConnection].cfg.iOutputDataSize -= sizeof(UINT32);
				gConnections[nConnection].cfg.iOutputDataOffset = gConnections[nTOConnection].cfg.iOutputDataOffset;			
			}
			gConnections[nConnection].cfg.iConnectionFlag = gConnections[nTOConnection].cfg.iConnectionFlag;
#endif
		}
	
		DumpStr5("connmgrIncomingConnection found match: O->T connection %d, T->O connection %d, new %d, number of connections %d, IP addr: 0x%x",
				nOTConnection, nTOConnection, nConnection, gnConnections, gConnections[nConnection].lIPAddress);
								
		nOrigTOConnection = nTOConnection;

#ifdef ET_IP_SCANNER		
		if ( nOTConnection != INVALID_CONNECTION )
		{
			/* We can remove now nOTConnection connection since we created a new one at nConnection */
			gConnections[nOTConnection].cfg.nInstance = INVALID_INSTANCE;
			connectionRemove( nOTConnection );		

			if ( nOTConnection < nConnection )
				nConnection--;

			if ( nTOConnection != INVALID_CONNECTION && nOTConnection < nTOConnection )
				nTOConnection--;

			DumpStr5("connmgrIncomingConnection removed O->T connection %d, T->O connection now %d, new now %d, number of connections now %d, IP addr: 0x%x",
				nOTConnection, nTOConnection, nConnection, gnConnections, gConnections[nConnection].lIPAddress);
		}

		if ( nTOConnection != INVALID_CONNECTION && nOTConnection != nOrigTOConnection && 
			 gConnections[nTOConnection].lConnectionState == ConnectionConfiguring && 
			 gConnections[nTOConnection].lConfigurationState == ConfigurationLogged )
		{
			/* We can remove now nOTConnection connection since we created a new one at nConnection */
			gConnections[nTOConnection].cfg.nInstance = INVALID_INSTANCE;
			connectionRemove( nTOConnection );		

			if ( nTOConnection < nConnection )
				nConnection--;

			DumpStr3("connmgrIncomingConnection removed T->O connection %d, new now %d, number of connections now %d",
				nTOConnection, nConnection, gnConnections );
		}
#endif
	}
			
	/* If Consuming Connection Id was not specified in the Forward Open, provide it in the reply */
	if ( gConnections[nConnection].lConsumingCID == 0 )
		gConnections[nConnection].lConsumingCID = utilGetUniqueID();
	
	/* If Producing Connection Id was not specified in the Forward Open, provide it in the reply */
	if ( gConnections[nConnection].lProducingCID == 0 )
		gConnections[nConnection].lProducingCID = utilGetUniqueID();

	gConnections[nConnection].sReceiveAddr.sin_family = 
	gConnections[nConnection].sTransmitAddr.sin_family = AF_INET;
	
	gConnections[nConnection].sReceiveAddr.sin_port = 
	gConnections[nConnection].sTransmitAddr.sin_port = htons( CLASS1_UDP_PORT );

	gConnections[nConnection].sReceiveAddr.sin_addr.s_addr = 0;
	
	if ( gConnections[nConnection].cfg.iProducingConnectionType != Multicast )
		gConnections[nConnection].sTransmitAddr.sin_addr.s_addr = gSessions[nSession].sClientAddr.sin_addr.s_addr;
	else
	{
		gConnections[nConnection].sTransmitAddr.sin_addr.s_addr = htonl(ntohl(gMulticastBaseAddr.sin_addr.s_addr) + gConnections[nConnection].cfg.nInstance);				
	
		/* Check if this is a multicast connection hooking up to one of the previosly 
		established multicast connections. */
		nMulticastProducer = connectionGetAnyMulticastProducer( nConnection );

		if ( nMulticastProducer != INVALID_CONNECTION )
		{
			gConnections[nConnection].lProducingCID = gConnections[nMulticastProducer].lProducingCID;
			gConnections[nConnection].sTransmitAddr = gConnections[nMulticastProducer].sTransmitAddr;
			gConnections[nConnection].cfg.iOutputDataOffset = gConnections[nMulticastProducer].cfg.iOutputDataOffset;
		}
	}

	/* Check if this is a listen only connection hooking up to one of the previosly 
	   established Exclusive Owner connections. */
	if ( gConnections[nConnection].cfg.iProducingConnectionType == Multicast &&
		 gConnections[nConnection].cfg.iConsumingConnPoint == LISTEN_ONLY_CONN_POINT )
	{			
		/* Find exclusive owner */
		for( i = 0; i < gnConnections; i++ )
		{			
			if ( i != nConnection &&
				 gConnections[nConnection].cfg.bTransportType == gConnections[i].cfg.bTransportType &&  
				 gConnections[nConnection].cfg.bTransportClass == gConnections[i].cfg.bTransportClass && 
				 gConnections[nConnection].cfg.iConfigConnInstance == gConnections[i].cfg.iConfigConnInstance &&
				 gConnections[nConnection].cfg.iProducingConnPoint == gConnections[i].cfg.iProducingConnPoint &&
				 gConnections[nConnection].cfg.lProducingDataRate == gConnections[i].cfg.lProducingDataRate &&  
				 gConnections[nConnection].cfg.iOutputDataSize == gConnections[i].cfg.iOutputDataSize &&
				 gConnections[i].iOwnerConnectionSerialNbr == INVALID_CONNECTION_SERIAL_NBR )
			{				
				/* This is a listen-only incoming connection. Make sure that we specify
				   the same Connection ID and the IP address to listen on. */
				gConnections[nConnection].iOwnerConnectionSerialNbr = gConnections[i].iConnectionSerialNbr;
				gConnections[nConnection].lProducingCID = gConnections[i].lProducingCID;
				gConnections[nConnection].sTransmitAddr = gConnections[i].sTransmitAddr;
				gConnections[nConnection].cfg.iOutputDataOffset = gConnections[i].cfg.iOutputDataOffset;
				break;
			}
		}
	}

	/* Allocate assembly space */
	if ( gConnections[nConnection].cfg.iInputDataOffset == INVALID_OFFSET )
	{
		if ( assemblyAssignInputOffsetForConnPoint( nConnection, gConnections[nConnection].cfg.iConsumingConnPoint ) == INVALID_OFFSET )
		{
			notifyEvent( NM_OUT_OF_MEMORY, 0 );
			return;
		}
	}

	if ( gConnections[nConnection].cfg.iOutputDataOffset == INVALID_OFFSET )
	{
		if ( assemblyAssignOutputOffsetForConnPoint( nConnection, gConnections[nConnection].cfg.iProducingConnPoint ) == INVALID_OFFSET )
		{
			notifyEvent( NM_OUT_OF_MEMORY, 0 );
			return;
		}	
	}
		
	/* Send Forward Open reply */
	connmgrPrepareFwdOpenReply( nSession, nConnection, nRequest );
		
	/* Reset timeout and producing ticks */
	connectionResetAllTicks( nConnection );

	/* Postpone the configuration of all other connections with the same IP address for CONNECTION_CONFIGURATION_DELAY period */
	connectionDelayPending( nConnection );

	if ( gConnections[nConnection].cfg.bTransportClass == Class1 )
		connectionAssignClass1Socket(nConnection);	
	
	gConnections[nConnection].lConnectionState = ConnectionEstablished;	
	notifyEvent( NM_CONNECTION_ESTABLISHED, gConnections[nConnection].cfg.nInstance );	
}

/*---------------------------------------------------------------------------
** connmgrPrepareFwdOpenReply( )
** 
** Send successful Forward Open Reply
**---------------------------------------------------------------------------
*/

void  connmgrPrepareFwdOpenReply( INT32 nSession, INT32 nConnection, INT32 nRequest )
{	
	FWD_OPEN_REPLY*   pFwdReply;
	CPF_TAG           tag;
	UINT8*            pData;	
	UINT8*            pConnPath;
		
	utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );
	utilRemoveFromMemoryPool( &gRequests[nRequest].iTagOffset, &gRequests[nRequest].iTagSize );
	gRequests[nRequest].iDataSize = FWD_OPEN_REPLY_SIZE;
	
	if ( gConnections[nConnection].cfg.bTransportClass == Class1 )
	{
		gRequests[nRequest].iTagCount += 2;
		gRequests[nRequest].iDataSize += 2*CPF_TAG_SIZE;
		gRequests[nRequest].iDataSize += 2*sizeof(struct sockaddr_in);
	}	
	
	gRequests[nRequest].iDataOffset = utilAddToMemoryPool( NULL, gRequests[nRequest].iDataSize );			
	pData = MEM_PTR(gRequests[nRequest].iDataOffset);
  
    pFwdReply = (FWD_OPEN_REPLY*)pData;
    	
	UINT32_SET( &pFwdReply->lLeOTConnId, gConnections[nConnection].lConsumingCID );
    UINT32_SET( &pFwdReply->lLeTOConnId, gConnections[nConnection].lProducingCID );

    UINT16_SET( &pFwdReply->iLeOrigConnectionSn, gConnections[nConnection].iConnectionSerialNbr );
    UINT16_SET( &pFwdReply->iLeOrigVendorId, gConnections[nConnection].iVendorID );
    UINT32_SET( &pFwdReply->lLeOrigDeviceSn, gConnections[nConnection].lDeviceSerialNbr );
    UINT32_SET( &pFwdReply->lLeOTApi, gConnections[nConnection].cfg.lConsumingDataRate );
    UINT32_SET( &pFwdReply->lLeTOApi, gConnections[nConnection].cfg.lProducingDataRate );
	UINT16_SET( &pFwdReply->iReserved, 0 );

	pData += FWD_OPEN_REPLY_SIZE;
	gRequests[nRequest].iPacketTagSize = FWD_OPEN_REPLY_SIZE;
		
	/* Store connection path to the originator in the connection configuration */	
	pConnPath = (unsigned char*)inet_ntoa( gSessions[nSession].sClientAddr.sin_addr );
	if ( pConnPath )
	{		
		gConnections[nConnection].cfg.iConnectionPathSize = UINT16(strlen((const char*)pConnPath) + 1);
		gConnections[nConnection].cfg.iConnectionPathOffset = utilAddToMemoryPool( pConnPath, gConnections[nConnection].cfg.iConnectionPathSize );
		DumpStr1("conmgrPrepareForwardOpenReply store IP address %s", pConnPath);
	}
	gConnections[nConnection].lIPAddress = gSessions[nSession].sClientAddr.sin_addr.s_addr;
		
	if ( gConnections[nConnection].cfg.bTransportClass == Class1 )
	{
		tag.iTag_type = ENCAP_TO_HS(CPF_TAG_SOCKADDR_OT);
		tag.iTag_length = ENCAP_TO_HS(sizeof(struct sockaddr_in));

		memcpy(pData, &tag, CPF_TAG_SIZE);
		pData += CPF_TAG_SIZE;
			
		memcpy(pData, &gConnections[nConnection].sReceiveAddr, sizeof(struct sockaddr_in));
		*(UINT16*)pData = htons(AF_INET);
		pData += sizeof(struct sockaddr_in);		

		tag.iTag_type = ENCAP_TO_HS(CPF_TAG_SOCKADDR_TO);
		tag.iTag_length = ENCAP_TO_HS(sizeof(struct sockaddr_in));

		memcpy(pData, &tag, CPF_TAG_SIZE);
		pData += CPF_TAG_SIZE;

		memcpy(pData, &gConnections[nConnection].sTransmitAddr, sizeof(struct sockaddr_in));
		*(UINT16*)pData = htons(AF_INET);
		pData += sizeof(struct sockaddr_in);	
	}
}


/*---------------------------------------------------------------------------
** connmgrProcessFwdClose( )
**
** Parse Forward Close request
**---------------------------------------------------------------------------
*/

void  connmgrProcessFwdClose( INT32 nRequest )
{	
	FWD_CLOSE	 FwdClose;
	INT32        nConnection;
	EPATH        path;
	INT32        nPathSize;
	UINT8*       pPacketPtr = MEM_PTR(gRequests[nRequest].iDataOffset);
	
	/* Parse Forward Close parameters */
	connmgrInitFwdCloseTypeFromBuf( pPacketPtr, &FwdClose );   

	pPacketPtr += (FWD_CLOSE_SIZE - 2);
	
	/* Parse Message Router Protocol Data Unit part which is part of the connection path */
	nPathSize = routerParsePDU( pPacketPtr, &path );

	/* Must have the class and instance logical segments */
	if ( nPathSize >= ROUTER_ERROR_BASE || 
		 ( (path.iFilled & EPATH_PORT) && !path.iPort )  )
	{	
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_SEG_TYPE;		
		return;
	}

	/* Find the connection that should be closed */
	for( nConnection = 0; nConnection < gnConnections; nConnection++ )
	{
		if ( FwdClose.iLeOrigConnectionSn == gConnections[nConnection].iConnectionSerialNbr &&
			 FwdClose.iLeOrigVendorId == gConnections[nConnection].iVendorID &&
			 FwdClose.lLeOrigDeviceSn == gConnections[nConnection].lDeviceSerialNbr &&
			 gConnections[nConnection].lConnectionState == ConnectionEstablished )
				break;
	}

	/* Connection not found - return an error */
	if ( nConnection >= gnConnections )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_FAILURE;
		gRequests[nRequest].iExtendedError = ROUTER_EXT_ERR_CONNECTION_NOT_FOUND;		
		return;
	}
	
	/* Prepare successful Forward Close reply */
	connmgrPrepareFwdCloseReply( nConnection, FwdClose.bClosePathSize, nRequest  );

#ifdef ET_IP_SCANNER
	if ( gConnections[nConnection].bCCConfigured )
	{
		connectionGoOffline( nConnection );

		notifyEvent( NM_CONNECTION_CLOSED, gConnections[nConnection].cfg.nInstance  );
	
		gConnections[nConnection].lConnectionState = ConnectionConfiguring;
		gConnections[nConnection].lConfigurationState = ConfigurationLogged;
	}
	else
#endif
		connectionRemove( nConnection );
}

/*---------------------------------------------------------------------------
** connmgrPrepareFwdCloseReply( )
**
** Prepare successful Forward Close reply
**---------------------------------------------------------------------------
*/

void  connmgrPrepareFwdCloseReply( INT32 nConnection, UINT8 bPathSize, INT32 nRequest )
{
	FWD_CLOSE_REPLY*        pFwdReply;
	
	utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, NULL, FWD_CLOSE_REPLY_SIZE );	

	if ( gRequests[nRequest].iDataSize && gRequests[nRequest].iDataOffset == INVALID_MEMORY_OFFSET )	/* we are out of memory */
	{
		notifyEvent( NM_OUT_OF_MEMORY, 0 );
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_NO_RESOURCE;
		return;
	}
		  
    pFwdReply = (FWD_CLOSE_REPLY*)MEM_PTR(gRequests[nRequest].iDataOffset);

    UINT32_SET(&pFwdReply->lLeOrigDeviceSn, gConnections[nConnection].lDeviceSerialNbr);
    UINT16_SET(&pFwdReply->iLeOrigVendorId, gConnections[nConnection].iVendorID);
    UINT16_SET(&pFwdReply->iLeOrigConnectionSn, gConnections[nConnection].iConnectionSerialNbr);
    pFwdReply->bPathSize           = bPathSize;
    pFwdReply->bReserved           = 0;
}


/*---------------------------------------------------------------------------
** connmgrInitFwdOpenTypeFromBuf()
**
** Populate Forward Open structure from the provided buffer
**---------------------------------------------------------------------------
*/

void connmgrInitFwdOpenTypeFromBuf( UINT8* buf, FWD_OPEN* fwdOpenType )
{
	fwdOpenType->bOpenPriorityTickTime = *(UINT8*)&buf[offs_fwdopen_bOpenPriorityTickTime];	
	fwdOpenType->bOpenTimeoutTicks = *(UINT8*)&buf[offs_fwdopen_bOpenTimeoutTicks];

	fwdOpenType->lLeOTConnId = UINT32_GET(&buf[offs_fwdopen_lLeOTConnId]);
	fwdOpenType->lLeTOConnId = UINT32_GET(&buf[offs_fwdopen_lLeTOConnId]);

	fwdOpenType->iLeOrigConnectionSn = UINT16_GET(&buf[offs_fwdopen_iLeOrigConnectionSn]);
	fwdOpenType->iLeOrigVendorId = UINT16_GET(&buf[offs_fwdopen_iLeOrigVendorId]);
	fwdOpenType->lLeOrigDeviceSn = UINT32_GET(&buf[offs_fwdopen_lLeOrigDeviceSn]);

	fwdOpenType->bTimeoutMult = *(UINT8*)&buf[offs_fwdopen_bTimeoutMult];

	fwdOpenType->bReserved = *(UINT8*)&buf[offs_fwdopen_bReserved];
	fwdOpenType->iReserved = UINT16_GET(&buf[offs_fwdopen_iReserved]);

	fwdOpenType->lLeOTRpi = UINT32_GET(&buf[offs_fwdopen_lLeOTRpi]);
	fwdOpenType->iLeOTConnParams = UINT16_GET(&buf[offs_fwdopen_iLeOTConnParams]);

	fwdOpenType->lLeTORpi = UINT32_GET(&buf[offs_fwdopen_lLeTORpi]);
	fwdOpenType->iLeTOConnParams = UINT16_GET(&buf[offs_fwdopen_iLeTOConnParams]);

	fwdOpenType->bClassTrigger = *(UINT8*)&buf[offs_fwdopen_bClassTrigger];
	fwdOpenType->bConnPathSize = *(UINT8*)&buf[offs_fwdopen_bConnPathSize];
}

/*---------------------------------------------------------------------------
** connmgrInitBufFromFwdOpenType()
**
** Populate the buffer from the provided Forward Open structure
**---------------------------------------------------------------------------
*/

void connmgrInitBufFromFwdOpenType( UINT8* buf, FWD_OPEN* fwdOpenType )
{
	*(UINT8*)&buf[offs_fwdopen_bOpenPriorityTickTime] = fwdOpenType->bOpenPriorityTickTime;	
	*(UINT8*)&buf[offs_fwdopen_bOpenTimeoutTicks] = fwdOpenType->bOpenTimeoutTicks;

	UINT32_SET(&buf[offs_fwdopen_lLeOTConnId], fwdOpenType->lLeOTConnId);
	UINT32_SET(&buf[offs_fwdopen_lLeTOConnId], fwdOpenType->lLeTOConnId);

	UINT16_SET(&buf[offs_fwdopen_iLeOrigConnectionSn], fwdOpenType->iLeOrigConnectionSn);
	UINT16_SET(&buf[offs_fwdopen_iLeOrigVendorId], fwdOpenType->iLeOrigVendorId);
	UINT32_SET(&buf[offs_fwdopen_lLeOrigDeviceSn], fwdOpenType->lLeOrigDeviceSn);

	*(UINT8*)&buf[offs_fwdopen_bTimeoutMult] = fwdOpenType->bTimeoutMult;

	*(UINT8*)&buf[offs_fwdopen_bReserved] = fwdOpenType->bReserved;
	UINT16_SET(&buf[offs_fwdopen_iReserved], fwdOpenType->iReserved);

	UINT32_SET(&buf[offs_fwdopen_lLeOTRpi], fwdOpenType->lLeOTRpi);
	UINT16_SET(&buf[offs_fwdopen_iLeOTConnParams], fwdOpenType->iLeOTConnParams);

	UINT32_SET(&buf[offs_fwdopen_lLeTORpi], fwdOpenType->lLeTORpi);
	UINT16_SET(&buf[offs_fwdopen_iLeTOConnParams], fwdOpenType->iLeTOConnParams);

	*(UINT8*)&buf[offs_fwdopen_bClassTrigger] = fwdOpenType->bClassTrigger;
	*(UINT8*)&buf[offs_fwdopen_bConnPathSize] = fwdOpenType->bConnPathSize;
}



/*---------------------------------------------------------------------------
** connmgrInitFwdCloseTypeFromBuf()
**
** Populate Forward Close structure from the provided buffer
**---------------------------------------------------------------------------
*/

void connmgrInitFwdCloseTypeFromBuf( UINT8* buf, FWD_CLOSE* fwdCloseType )
{
	fwdCloseType->bOpenPriorityTickTime = *(UINT8*)&buf[offs_fwdclose_bOpenPriorityTickTime];	
	fwdCloseType->bOpenTimeoutTicks = *(UINT8*)&buf[offs_fwdclose_bOpenTimeoutTicks];

	fwdCloseType->iLeOrigConnectionSn = UINT16_GET(&buf[offs_fwdclose_iLeOrigConnectionSn]);
	fwdCloseType->iLeOrigVendorId = UINT16_GET(&buf[offs_fwdclose_iLeOrigVendorId]);
	fwdCloseType->lLeOrigDeviceSn = UINT32_GET(&buf[offs_fwdclose_lLeOrigDeviceSn]);

	fwdCloseType->bClosePathSize = *(UINT8*)&buf[offs_fwdclose_bClosePathSize];
	fwdCloseType->bReserved = *(UINT8*)&buf[offs_fwdclose_bReserved];	
}

/*---------------------------------------------------------------------------
** connmgrInitBufFromFwdCloseType()
**
** Populate the buffer from the provided Forward Close structure
**---------------------------------------------------------------------------
*/

void connmgrInitBufFromFwdCloseType( UINT8* buf, FWD_CLOSE* fwdCloseType )
{
	*(UINT8*)&buf[offs_fwdclose_bOpenPriorityTickTime] = fwdCloseType->bOpenPriorityTickTime;	
	*(UINT8*)&buf[offs_fwdclose_bOpenTimeoutTicks] = fwdCloseType->bOpenTimeoutTicks;

	UINT16_SET(&buf[offs_fwdclose_iLeOrigConnectionSn], fwdCloseType->iLeOrigConnectionSn);
	UINT16_SET(&buf[offs_fwdclose_iLeOrigVendorId], fwdCloseType->iLeOrigVendorId);
	UINT32_SET(&buf[offs_fwdclose_lLeOrigDeviceSn], fwdCloseType->lLeOrigDeviceSn);

	*(UINT8*)&buf[offs_fwdclose_bClosePathSize] = fwdCloseType->bClosePathSize;
	*(UINT8*)&buf[offs_fwdclose_bReserved] = fwdCloseType->bReserved;	
}


/*---------------------------------------------------------------------------
** connmgrParseClassInstanceRequest( )
**
** Determine if it's request for the Class or for the particular instance 
**---------------------------------------------------------------------------
*/

void connmgrParseClassInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 0 )
		connmgrParseClassRequest( nRequest );
	else
		connmgrParseInstanceRequest( nRequest );
}

/*---------------------------------------------------------------------------
** connmgrParseClassRequest( )
**
** Respond to the class request
**---------------------------------------------------------------------------
*/

void connmgrParseClassRequest( INT32 nRequest )
{
	gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;	
}

/*---------------------------------------------------------------------------
** connmgrParseInstanceRequest( )
**
** Respond to the instance request
**---------------------------------------------------------------------------
*/

void connmgrParseInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 1 )
	{
		switch( gRequests[nRequest].bService )
		{
			case FWD_OPEN_CMD_CODE:				
				connmgrIncomingConnection( nRequest );
				break;

			case FWD_CLOSE_CMD_CODE:
				connmgrProcessFwdClose( nRequest );
				break;

			case UNCONNECTED_SEND_CMD_CODE:
				connmgrProcessUnconnectedSend( nRequest );
				break;
		
			default:	/* Instance requests are not supported at this moment */
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
				break;
		}
	}
	else	/* only Instance 1 is supported at this moment */
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;
}

/*---------------------------------------------------------------------------
** connmgrProcessUnconnectedSend( )
**
** Parse Unconnected Send request 
**---------------------------------------------------------------------------
*/

void  connmgrProcessUnconnectedSend( INT32 nRequest )
{
	EPATH			pdu;
	INT32			nPDUSize;		
	UINT8*          pData = MEM_PTR(gRequests[nRequest].iDataOffset);
	INT32           nMsgSize;
	INT32           nPathSize;
	UINT8           connectionPath[MAX_CONNECTION_PATH_SIZE];
	INT32           nNewRequest;
	
	if ( gRequests[nRequest].iDataSize < sizeof(UINT32) )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_PARTIAL_DATA;
		return;
	}

	nNewRequest = requestNew( UnconnectedSendRequest, TRUE, TRUE );	/* Schedule new Unconnected Send request */

	if ( nNewRequest == ERROR_STATUS )						/* No more space for requests */
	{
		notifyEvent( NM_PENDING_REQUESTS_LIMIT_REACHED, 0 );
		return;		
	}

	gRequests[nNewRequest].lFromIPAddress = gRequests[nRequest].lIPAddress;
	gRequests[nNewRequest].lRequestTimeoutTick = platformGetTickCount() + DEFAULT_TIMEOUT;	/* Set the request timeout tick */

	gRequests[nNewRequest].lContext1 = gRequests[nRequest].lContext1;
	gRequests[nNewRequest].lContext2 = gRequests[nRequest].lContext2;

	gRequests[nNewRequest].bOpenPriorityTickTime = *pData++;
	gRequests[nNewRequest].bOpenTimeoutTicks = *pData++;

	nMsgSize = UINT16_GET(pData);
	pData += sizeof(UINT16);
	
	/* Parse the next part of the packet that must be Router PDU (Protocol Data Unit) */
	nPDUSize = routerParsePDU( pData, &pdu );

	/* Continue is we got the Class and Instance or a Symbolic string segment identifiers */
	if ( (nPDUSize < ROUTER_ERROR_BASE) && (((pdu.iFilled & EPATH_CLASS) && (pdu.iFilled & EPATH_INSTANCE)) || (pdu.iFilled & EPATH_EXT_SYMBOL)) )
	{
		gRequests[nNewRequest].bService = *pData;
		gRequests[nNewRequest].iClass = ( pdu.iFilled & EPATH_CLASS ) ? pdu.iClass : INVALID_CLASS;
		gRequests[nNewRequest].iInstance = ( pdu.iFilled & EPATH_INSTANCE ) ? pdu.iInstance : INVALID_INSTANCE;
		gRequests[nNewRequest].iAttribute = ( pdu.iFilled & EPATH_ATTRIBUTE ) ? pdu.bAttribute : INVALID_ATTRIBUTE;
		gRequests[nNewRequest].iMember = ( pdu.iFilled & EPATH_MEMBER ) ? pdu.iMember : INVALID_MEMBER;
		
		if ( pdu.iFilled & EPATH_EXT_SYMBOL )
		{
			gRequests[nNewRequest].iTagSize = pdu.bExtSymbolSize;
			gRequests[nNewRequest].iTagOffset = utilAddToMemoryPool( pdu.bExtSymbol, gRequests[nNewRequest].iTagSize );	
		}
		else
		{
			gRequests[nNewRequest].iTagSize = 0;
			gRequests[nNewRequest].iTagOffset = INVALID_MEMORY_OFFSET;
		}		
	
		pData += (PDU_HDR_SIZE + nPDUSize);
		gRequests[nNewRequest].iDataSize = (UINT16)(nMsgSize - PDU_HDR_SIZE - nPDUSize);
		gRequests[nNewRequest].iDataOffset = utilAddToMemoryPool( pData, gRequests[nNewRequest].iDataSize );	

		if ( (gRequests[nNewRequest].iDataSize && gRequests[nNewRequest].iDataOffset == INVALID_MEMORY_OFFSET) ||
			 (gRequests[nNewRequest].iTagSize && gRequests[nNewRequest].iTagOffset == INVALID_MEMORY_OFFSET) )
		{
			requestRemove( nNewRequest );
			notifyEvent( NM_OUT_OF_MEMORY, 0 );
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_NO_RESOURCE;	/* Out of memory */
			return;
		}	

		pData += gRequests[nNewRequest].iDataSize;
	}
	else
	{
		requestRemove( nNewRequest );
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_PARTIAL_DATA;
		return;
	}

	nPathSize = UINT16_GET(pData) * 2;  /* Path size is quoted in words */
	*(pData+1) = 2;						/* Make it parse only the first path segment */
	
	nPDUSize = routerParsePDU( pData, &pdu );
	
	if ( !(pdu.iFilled & EPATH_PORT) )
	{
		requestRemove( nNewRequest );
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_PARTIAL_DATA;
		return;
	}

	if ( nPDUSize % 2 )
		nPDUSize++;						/* Pad byte if path length is odd */

	memcpy( connectionPath, pdu.bLinkAddress, pdu.bLinkAddressSize );
	connectionPath[pdu.bLinkAddressSize] = 0;
	
	pData += (PDU_HDR_SIZE + nPDUSize);
		
	gRequests[nNewRequest].iExtendedPathSize = (UINT16)(nPathSize - nPDUSize);
	gRequests[nNewRequest].iExtendedPathOffset = utilAddToMemoryPool( pData, gRequests[nNewRequest].iExtendedPathSize );
	
	gRequests[nNewRequest].lIPAddress = utilAddrFromPath( (const char*)connectionPath );
	if ( gRequests[nNewRequest].lIPAddress == ERROR_STATUS )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_PARAMETER_VALUE;
		requestRemove( nNewRequest );
		return;
	}
					
	gRequests[nNewRequest].nState = RequestLogged;	
	
	requestRemove( nRequest );	
}

