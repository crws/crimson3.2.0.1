/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** ENETLINK.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Ethernet Link object implementation 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

UINT32   glInterfaceSpeed;
UINT32   glInterfaceFlags;
UINT8    gmacAddr[MAC_ADDR_LENGTH];


/*---------------------------------------------------------------------------
** enetlinkInit( )
**
** Initialize Ethernet Link object
**---------------------------------------------------------------------------
*/

void enetlinkInit()
{		
   /* If product can be accessed via a CIP port other than TCP/IP, then
   ** the link active bit must be determined.  If product only responds
   ** to one source of CIP requests (Ethernet port e.g.), then we can
   ** assume that it is active, since we got this request */	
   glInterfaceFlags = ENETLINK_FLAG_LINK_ACTIVE;

   /* The interface flags and interface speed must be determined
   ** from your hardware interface */
   glInterfaceFlags &= ~EL_FLAG_DUPLEX_MODE;
   glInterfaceSpeed = 10;
   
   platformGetMacID(gmacAddr);   

}

/*---------------------------------------------------------------------------
** enetlinkParseClassInstanceRequest( )
**
** Determine if it's request for the Class or for the particular instance 
**---------------------------------------------------------------------------
*/

void enetlinkParseClassInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 0 )
		enetlinkParseClassRequest( nRequest );
	else
		enetlinkParseInstanceRequest( nRequest );
}

/*---------------------------------------------------------------------------
** enetlinkParseClassRequest( )
**
** Respond to the Ethernet Link class request
**---------------------------------------------------------------------------
*/

void enetlinkParseClassRequest( INT32 nRequest )
{
	switch( gRequests[nRequest].bService )
	{
		case SVC_GET_ATTR_ALL:
			enetlinkSendClassAttrAll( nRequest );
			break;
		case SVC_GET_ATTR_SINGLE:
			enetlinkSendClassAttrSingle( nRequest );
			break;
		default:		
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
			break;
	}
}

/*---------------------------------------------------------------------------
** enetlinkSendClassAttrAll( )
**
** GetAttributeAll service for Ethernet Link class is received 
**---------------------------------------------------------------------------
*/

void enetlinkSendClassAttrAll( INT32 nRequest )
{
	ENETLINK_CLASS_ATTR attr;

	attr.iRevision = ENCAP_TO_HS(ENETLINK_CLASS_REVISION);
	attr.iMaxInstance = ENCAP_TO_HS(1);
	attr.iNumInstances = ENCAP_TO_HS(1);

	utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&attr, ENETLINK_CLASS_ATTR_SIZE );		
}

/*---------------------------------------------------------------------------
** enetlinkSendClassAttrSingle( )
**
** GetAttributeSingle service for Ethernet Link class is received 
**---------------------------------------------------------------------------
*/

void enetlinkSendClassAttrSingle( INT32 nRequest )
{	
	UINT16 iVal;
	
	switch( gRequests[nRequest].iAttribute )
	{
		case ENETLINK_CLASS_ATTR_REVISION:
			iVal = ENCAP_TO_HS(ENETLINK_CLASS_REVISION);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case ENETLINK_CLASS_ATTR_MAX_INSTANCE:
			iVal = ENCAP_TO_HS(1);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		case ENETLINK_CLASS_ATTR_NUM_INSTANCES:
			iVal = ENCAP_TO_HS(1);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );		
			break;
		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}		
}


/*---------------------------------------------------------------------------
** enetlinkParseInstanceRequest( )
**
** Respond to the Ethernet Link instance request
**---------------------------------------------------------------------------
*/

void enetlinkParseInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 1 )
	{
		switch( gRequests[nRequest].bService )
		{
			case SVC_GET_ATTR_ALL:
				enetlinkSendInstanceAttrAll( nRequest );
				break;
			case SVC_GET_ATTR_SINGLE:
			case ENETLINK_GET_AND_CLEAR:
				enetlinkSendInstanceAttrSingle( nRequest );
				break;		
			case SVC_SET_ATTR_ALL:
			case SVC_SET_ATTR_SINGLE:
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SETTABLE;
				break;
			default:
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
				break;
		}
	}
	else
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;
}


/*---------------------------------------------------------------------------
** enetlinkSendInstanceAttrAll( )
**
** GetAttributeAll service for Ethernet Link instance is received
**---------------------------------------------------------------------------
*/

void enetlinkSendInstanceAttrAll( INT32 nRequest )
{
	UINT8* pData;
		
	utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, NULL, 2 * sizeof(UINT32) + MAC_ADDR_LENGTH );		

	if ( gRequests[nRequest].iDataSize && gRequests[nRequest].iDataOffset == INVALID_MEMORY_OFFSET )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_NO_RESOURCE;	/* Out of memory */	
		return;
	}
	
	pData = MEM_PTR( gRequests[nRequest].iDataOffset );
		
	UINT32_SET( pData, glInterfaceSpeed );
	pData += sizeof(UINT32);
	
	UINT32_SET( pData, glInterfaceFlags );
	pData += sizeof(UINT32);
		
	memcpy( pData, gmacAddr, MAC_ADDR_LENGTH );

}


/*--------------------------------------------------------------------------------
** enetlinkSendInstanceAttrSingle( )
**
** GetAttributeSingle request has been received for the Ethernet Link object instance
**--------------------------------------------------------------------------------
*/

void enetlinkSendInstanceAttrSingle( INT32 nRequest )
{	
	UINT32 lVal;
	
	switch( gRequests[nRequest].iAttribute )
	{
		case ENETLINK_INST_ATTR_INTERFACE_SPEED:
			lVal = ENCAP_TO_HL(glInterfaceSpeed);			
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&lVal, sizeof(UINT32) );		
			break;

		case ENETLINK_INST_ATTR_INTERFACE_FLAGS:
			lVal = ENCAP_TO_HL(glInterfaceFlags);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&lVal, sizeof(UINT32) );		
			break;
		
		case ENETLINK_INST_ATTR_PHYSICAL_ADDRESS:			
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, gmacAddr, MAC_ADDR_LENGTH );		
			break;		

		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}	
}

