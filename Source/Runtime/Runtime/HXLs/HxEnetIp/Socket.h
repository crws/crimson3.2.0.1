//////////////////////////////////////////////////////////////////////////
//
// EthernetIP Gateway Environment
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SOCKET_H

#define	INCLUDE_SOCKET_H

//////////////////////////////////////////////////////////////////////////
//
// Encap Ports
//

#define ENCAP_SERVER_PORT	0xAF12

#define CLASS1_UDP_PORT		0x08AE

#define INVALID_SOCKET		DWORD(0)

//////////////////////////////////////////////////////////////////////////
//
// Winsock Types
//

struct in_addr 
{
	union {
		IPADDR s_un_b;
		DWORD  s_addr;
		};
	};

struct sockaddr_in
{
	SHORT	sin_family;
	WORD	sin_port;
	in_addr	sin_addr;
	CHAR	sin_zero[8];
	};

struct sockaddr 
{
	WORD	sa_family;
	char	sa_data[14];
	};   

struct hostent 
{
	PTXT	h_name;
	PTXT  *	h_aliases;
	SHORT	h_addrtype;
	SHORT	h_length;
	PTXT  *	h_addr_list;
	};

typedef ISocket *	PSOCK;

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define IS_MULTICAST(i)		(((UINT32)(i) & 0xf0000000) == 0xe0000000) 

#define INVALID_IP_ADDRESS	IP_EMPTY

#define	htonl(x)		HostToNet(DWORD(x))

#define htons(x)		HostToNet(WORD(x))

#define ntohl(x)		NetToHost(DWORD(x))

#define ntohs(x)		NetToHost(WORD(x))

#define	gethostbyname(p)	NULL

#define gethostname(p, n)	INVALID_SOCKET

#define AF_INET			2

//////////////////////////////////////////////////////////////////////////
//
// Global Data
//

extern UINT8			gmsgBuf[];

extern UINT32			glClass1Socket[];

extern INT32			nHostAddrCount;

extern sockaddr_in		gHostAddr[];

extern sockaddr_in		gMulticastBaseAddr;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define WAIT_TO_RECOVER		500

#define COMMON_BUFFER_SIZE	32000

#define SMALL_BUFFER_SIZE	4000

#define PARTIAL_PACKET_TIMEOUT	1000

//////////////////////////////////////////////////////////////////////////
//
// Function Prototypes
//

extern void  socketInit(void);
extern void  socketCleanup(void);
extern INT32 socketClass1Init(BOOL bBroadcast, UINT32 lBufferSize);
extern BOOL  socketCheckIncomingSession(void);
extern INT32 socketEncapRecv(INT32 nSession);
extern INT32 socketEncapSendData(INT32 nSession);
extern INT32 socketEncapSendPartial(INT32 nSession);
extern INT32 socketClass1Recv(UINT32 lClass1Socket);
extern INT32 socketClass1SendData(INT32 nConnection);
extern INT32 socketJoinMulticastGroup(INT32 nConnection);
extern INT32 socketDropMulticastGroup(INT32 nConnection);
extern void  socketGetBroadcasts(void);
extern BOOL  socketStartTCPConnection(INT32 nSession, BOOL *pfDone);
extern BOOL  socketContinueTCPConnection(INT32 nSession, BOOL *pfDone);

//////////////////////////////////////////////////////////////////////////
//
// Winsock Implementations
//

extern DWORD	inet_addr(PCTXT p);
extern PTXT	inet_ntoa(in_addr &in);

// End of File

#endif
