/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** CONNECT.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** This module contains the implementation of the Connection collection
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

INT32      gnConnections;
CONNECTION gConnections[MAX_CONNECTIONS];


/*---------------------------------------------------------------------------
** connectionInit( )
**
** Initialize connection array
**---------------------------------------------------------------------------
*/

void connectionInit()
{
	INT32    i;
	
	gnConnections = 0;

	for( i = 0; i < MAX_CONNECTIONS; i++ )
	{
		connectionInitialize( i );	
	}
}

/*---------------------------------------------------------------------------
** connectionInitialize( )
**
** Initialize connection object
**---------------------------------------------------------------------------
*/

void connectionInitialize( INT32 nConnection )
{
	memset( (void *)&gConnections[nConnection], 0, sizeof(CONNECTION) );
	
	gConnections[nConnection].lConnectionState = ConnectionNonExistent;
		
	gConnections[nConnection].iOwnerConnectionSerialNbr = INVALID_CONNECTION_SERIAL_NBR;
	
	gConnections[nConnection].lClass1Socket = INVALID_SOCKET;	
}

/*---------------------------------------------------------------------------
** connectionNew( )
**
** Add new connection to the connection array
**---------------------------------------------------------------------------
*/

INT32 connectionNew( UINT32 lIPAddress )
{
	INT32 nConnection;
	
	nConnection = gnConnections;

	if ( nConnection >= MAX_CONNECTIONS )
		return INVALID_CONNECTION;
	
	connectionInitialize( nConnection );
			
	gConnections[nConnection].lIPAddress = lIPAddress;
	
	gConnections[nConnection].cfg.iWatchdogTimeoutAction = TimeoutDelayAutoReset;
	gConnections[nConnection].cfg.lWatchdogTimeoutReconnectDelay = DEFAULT_WATCHDOG_TIMEOUT_RECONNECT_DELAY;
			
	gnConnections++;
		
	return nConnection;
}

/*---------------------------------------------------------------------------
** connectionRemoveAll( )
**
** Clean up connection array
**---------------------------------------------------------------------------
*/

void connectionRemoveAll()
{
	INT32    i;
		
	for( i = (gnConnections-1); i >= 0; i-- )
	{
		connectionRemove( i );	
	}

	gnConnections = 0;
}

#ifdef ET_IP_SCANNER
/*---------------------------------------------------------------------------
** connectionFailedForSession( )
**
** Fail all connections for the specifed session
**---------------------------------------------------------------------------
*/
void connectionFailedForSession( INT32 nSession, UINT8 bGenStatus, UINT16 iExtendedErrorCode )
{
	INT32 i;

	for( i = 0; i < gnConnections; i++ )
	{
		/* There is only one connection being configured for the specified IP address */
		if ( gConnections[i].lIPAddress == gSessions[nSession].sClientAddr.sin_addr.s_addr )
		{				
			if ( gConnections[i].lConnectionState != ConnectionConfiguring ||
				 gConnections[i].lConfigurationState != ConfigurationWaitingForForwardOpenResponse )
					continue;
			else 
			{				
				gConnections[i].bGeneralStatus = bGenStatus;
				gConnections[i].iExtendedStatus = iExtendedErrorCode;								
				notifyEvent(NM_CONN_CONFIG_FAILED_ERROR_RESPONSE, gConnections[i].cfg.nInstance );				
				if ( gConnections[i].cfg.iWatchdogTimeoutAction != TimeoutAutoDelete &&
					 gConnections[i].cfg.iWatchdogTimeoutAction != TimeoutDeferredDelete )
					connectionTimedOut( i );				
			}			
		}
	}
}
#endif

/*---------------------------------------------------------------------------
** connectionService( )
**
** Service a particular connection. Return TRUE if read and written on the 
** socket, FALSE otherwise.
**---------------------------------------------------------------------------
*/
BOOL connectionService( INT32 nConnection )
{	
#ifdef ET_IP_SCANNER
	INT32   nSession, nIndex;	
#endif
	UINT32  lTick = platformGetTickCount();
	INT32   nMulticastProducer = INVALID_CONNECTION;
	BOOL    bRet = FALSE;
	
	/* Check if this connection was scheduled for closing */
	if ( gConnections[nConnection].lConfigurationState == ConfigurationClosing &&
		 gConnections[nConnection].lConnectionState != ConnectionClosing )
	{
		/* If we are the ones who initiate a connection drop - issue Forward Close */
		if ( gConnections[nConnection].cfg.bOriginator && gConnections[nConnection].lConnectionState == ConnectionEstablished )	
		{
			gConnections[nConnection].lStartTick = platformGetTickCount();
			gConnections[nConnection].lConnectionState = ConnectionClosing;
		}
		else
		{
			connectionRemove( nConnection );
			return bRet;
		}
	}

	switch( gConnections[nConnection].lConnectionState )
	{	
#ifdef ET_IP_SCANNER		
		case ConnectionConfiguring:
		{
			switch( gConnections[nConnection].lConfigurationState )
			{
				case ConfigurationLogged:
				{
					/* If incoming connection scheduled by CC object, but not opened yet - leave it alone */
					if ( !gConnections[nConnection].cfg.bOriginator )
						return bRet;

					/* Check if there is a delay on opening a connection  or 
					   if another connection is being configured on the same IP address. If Yes, wait until it's finished. */
					if ( IS_TICK_GREATER( gConnections[nConnection].lStartTick, lTick ) ||
						 connectionAnotherPending( nConnection ) ) 
						return bRet;
					
					/* Check if the session with this server has already been established */
					nSession = sessionFindAddress( gConnections[nConnection].lIPAddress, Outgoing );
			
					if ( nSession == INVALID_SESSION )
					{
						nIndex = sessionNew( gConnections[nConnection].lIPAddress, FALSE );
						DumpStr1("connectionService new session logged with IPAddr: 0x%x", gConnections[nConnection].lIPAddress); 
  	  	  
					    if ( nIndex == INVALID_SESSION )	/* Maximum number of open sessions has been reached */
						{							
							connectionRemove( nConnection );							
							notifyEvent(NM_SESSION_COUNT_LIMIT_REACHED, 0);
							return bRet;
						}					
					}
					else if ( gSessions[nSession].lState == OpenSessionEstablished)
					{
						scanmgrOutgoingConnection( nSession, nConnection );
						connectionRecalcTimeouts( nConnection );		
						gConnections[nConnection].lConfigurationState = ConfigurationWaitingForForwardOpenResponse;						
						gConnections[nConnection].lConfigurationTimeoutTick = lTick + DEFAULT_TIMEOUT;					
						return bRet;
					}

					/* Session has not been successfully established yet */
					gConnections[nConnection].lConfigurationState = ConfigurationWaitingForSession;							
					gConnections[nConnection].lConfigurationTimeoutTick = lTick + DEFAULT_TIMEOUT;					
				}
				break;						
				
				case ConfigurationWaitingForSession:
				{
					nSession = sessionFindAddress( gConnections[nConnection].lIPAddress, Outgoing );
			
					if ( nSession == INVALID_SESSION || IS_TICK_GREATER( lTick, gConnections[nConnection].lConfigurationTimeoutTick ) )
					{	
						gConnections[nConnection].lConfigurationState = ConfigurationFailedInvalidNetworkPath;							
						notifyEvent( NM_CONN_CONFIG_FAILED_INVALID_NETWORK_PATH, gConnections[nConnection].cfg.nInstance );
						connectionTimedOut( nConnection );						
					}
					else if ( gSessions[nSession].lState == OpenSessionEstablished )
					{
						scanmgrOutgoingConnection( nSession, nConnection );
						gConnections[nConnection].lConfigurationState = ConfigurationWaitingForForwardOpenResponse;						
					}
				}
				break;

				case ConfigurationWaitingForForwardOpenResponse:
				{
					if ( IS_TICK_GREATER( lTick, gConnections[nConnection].lConfigurationTimeoutTick ) )
					{	
						gConnections[nConnection].lConfigurationState = ConfigurationFailedInvalidNetworkPath;							
						notifyEvent( NM_CONN_CONFIG_FAILED_INVALID_NETWORK_PATH, gConnections[nConnection].cfg.nInstance );
						connectionTimedOut( nConnection );												
					}
				}
				break;

				default:
					break;
			}
		}
		break;	
#endif	
	    case ConnectionEstablished:
		{				
			/* Check if it's time to produce on connection or ChangeOfState */
			if ( gConnections[nConnection].cfg.bTransportClass == Class1 || gConnections[nConnection].cfg.bOriginator )
			{
				nMulticastProducer = connectionGetFirstMulticastProducer( nConnection );
								
				if ( ( gConnections[nConnection].iOwnerConnectionSerialNbr == INVALID_CONNECTION_SERIAL_NBR && IS_TICK_GREATER(lTick, gConnections[nConnection].lProduceTick ) ) ||
						( gConnections[nConnection].bTransferImmediately && gConnections[nConnection].cfg.bTransportClass == Class1 ) )
				{
					if ( gConnections[nConnection].cfg.bTransportClass == Class1 ) /* For Class 1 using UDP packets to transport I/O */
					{							
						if ( nMulticastProducer == INVALID_CONNECTION )
						{
							if ( ioSendClass1Packet( nConnection ) )							
								connectionAdvanceProducingTicks( nConnection );	
						}
						else				
							connectionResetProducingTicks( nConnection );	/* Reset the producing tick */
						bRet = TRUE;
						gConnections[nConnection].bTransferImmediately = FALSE;			
					}
					else if ( gConnections[nConnection].cfg.bTransportClass == Class3 && /* For Class 1 using TCP packets to transport object request */
							  gConnections[nConnection].cfg.bOriginator )
					{
						ucmmSendConnectedRequest( nConnection );
						connectionAdvanceProducingTicks( nConnection );	
					}
				}																	
			}
			
			if ( connectionIsTimedOut( nConnection ) )		/* Is connection timed out */
			{
#ifdef ET_IP_SCANNER						
				/* Check if need to drop the session that went offline */
				if ( gConnections[nConnection].cfg.bOriginator )
				{
					if ( sessionIsIdle( gConnections[nConnection].lIPAddress, nConnection, -1 ) )
					{
						nSession = sessionFindAddress( gConnections[nConnection].lIPAddress, Outgoing );
						if ( nSession != INVALID_SESSION )
						{	
							sessionRemove( nSession, TRUE );
							break;					
						}
					}
				}
#endif				
				gConnections[nConnection].bGeneralStatus = ROUTER_ERROR_FAILURE;
				gConnections[nConnection].iExtendedStatus = ROUTER_EXT_ERR_CONNECTION_TIMED_OUT;				
				notifyEvent( NM_CONNECTION_TIMED_OUT, gConnections[nConnection].cfg.nInstance );
				connectionTimedOut( nConnection );				
			}
		}
		break;
#ifdef ET_IP_SCANNER		
		case ConnectionClosing:
			switch( gConnections[nConnection].lConfigurationState )
			{
				case ConfigurationClosing:			

					/* Check if there is a delay on closing a connection  or 
					   if another connection is being configured on the same IP address. If Yes, wait until it's finished. */
					if ( IS_TICK_GREATER( gConnections[nConnection].lStartTick, lTick ) ||
						 connectionAnotherPending( nConnection ) ) 
						return bRet;
					
					gConnections[nConnection].lConfigurationTimeoutTick = platformGetTickCount() + FORWARD_CLOSE_TIMEOUT;					
					scanmgrIssueFwdClose( nConnection );
					gConnections[nConnection].lConfigurationState = ConfigurationWaitingForForwardCloseResponse;
					break;

				case ConfigurationWaitingForForwardCloseResponse:
					if ( IS_TICK_GREATER( lTick, gConnections[nConnection].lConfigurationTimeoutTick ) )
						connectionRemove( nConnection );	/* Forward Close response did not come - close now */
					break;
				
				default:
					break;
			}			
			break;
#endif

		default:
			break;
	}

	return bRet;
}				

/*---------------------------------------------------------------------------
** connectionTimedOut( )
**
** The peer stopped communicating. Process based on the iWatchdogTimeoutAction
** member value.
**---------------------------------------------------------------------------
*/
void connectionTimedOut( INT32 nConnection )
{
	switch( gConnections[nConnection].cfg.iWatchdogTimeoutAction )
	{
		case TimeoutAutoDelete:
		case TimeoutDeferredDelete:
			connectionRemove( nConnection );
			break;

		case TimeoutAutoReset:
			connectionGoOffline( nConnection );			
			gConnections[nConnection].lConnectionState = ConnectionConfiguring;
			gConnections[nConnection].lConfigurationState = ConfigurationLogged;
			gConnections[nConnection].lStartTick = platformGetTickCount();			
			break;

		case TimeoutDelayAutoReset:						
			connectionGoOffline( nConnection );			
			gConnections[nConnection].lConnectionState = ConnectionConfiguring;
			gConnections[nConnection].lConfigurationState = ConfigurationLogged;
			gConnections[nConnection].lStartTick = platformGetTickCount() + gConnections[nConnection].cfg.lWatchdogTimeoutReconnectDelay;			
			break;

		case TimeoutManualReset:
			connectionGoOffline( nConnection );
			gConnections[nConnection].lConnectionState = ConnectionTimedOut;			
			break;

		default:
			break;	
	}	
}

/*---------------------------------------------------------------------------
** connectionAssignClass1Socket( )
**
** Assign UDP socket based on the connection instance.
**---------------------------------------------------------------------------
*/
void connectionAssignClass1Socket( INT32 nConnection )
{	
	INT32 nConnGroup = GET_CONNECTION_GROUP( nConnection );
	
	if ( glClass1Socket[nConnGroup] == INVALID_SOCKET )
		glClass1Socket[nConnGroup] = socketClass1Init( FALSE, COMMON_BUFFER_SIZE );	

	gConnections[nConnection].lClass1Socket = glClass1Socket[nConnGroup];
}

/*---------------------------------------------------------------------------
** connectionReleaseClass1Socket( )
**
** Drop from the multicast group and close the socket if needed.
**---------------------------------------------------------------------------
*/
void connectionReleaseClass1Socket( INT32 nConnection )
{	
	INT32 nConnGroup = GET_CONNECTION_GROUP( nConnection );
	INT32  i;
	
	if ( gConnections[nConnection].lClass1Socket == INVALID_SOCKET )
		return;
	
	/* Take us from the multicast receive list */
	if ( IS_MULTICAST( ntohl(gConnections[nConnection].sReceiveAddr.sin_addr.s_addr) ) )
		socketDropMulticastGroup(nConnection);		

	for ( i = 0; i < gnConnections; i++ )
	{
		if ( i != nConnection &&
			 GET_CONNECTION_GROUP( i ) == nConnGroup && 
			 gConnections[i].lClass1Socket != INVALID_SOCKET )
				break; 
	}

	if ( i == gnConnections && glClass1Socket[nConnGroup] != INVALID_SOCKET )
	{
		DumpStr1("Closing socket %x", glClass1Socket[nConnGroup]);
		platformCloseSocket( glClass1Socket[nConnGroup] );
		glClass1Socket[nConnGroup] = INVALID_SOCKET; 		
	}

	gConnections[nConnection].lClass1Socket = INVALID_SOCKET;
}



/*---------------------------------------------------------------------------
** connectionGoOffline( )
**
** Outgoing connections are taken offline instead of removing them.
**---------------------------------------------------------------------------
*/

void connectionGoOffline( INT32 nConnection )
{
	INT32 i, nRequest;	
	UINT16 iConnectionSerialNbr;

	if ( gConnections[nConnection].lConnectionState != ConnectionEstablished )
		return;

	/* Increase the sequence count just in case the connection is coming back */
	gConnections[nConnection].iOutDataSeqNbr++;
		
	connectionReleaseClass1Socket( nConnection );
			
	iConnectionSerialNbr = gConnections[nConnection].iConnectionSerialNbr;

	/* Remove incoming Class3 requests for this connection */
	for( nRequest = (gnRequests-1); nRequest >= 0; nRequest-- )
	{
		if ( gRequests[nRequest].nType == ObjectRequest && gRequests[nRequest].iConnectionSerialNbr == gConnections[nConnection].iConnectionSerialNbr )		
			requestRemove( nRequest );			
	}	
	
	for( i = 0; i < gnConnections; i++ )
	{
		/* Remove listen only incoming connections that depended on the connection being removed */
		if ( i != nConnection && gConnections[i].iOwnerConnectionSerialNbr == gConnections[nConnection].iConnectionSerialNbr )
			gConnections[i].lConfigurationState = ConfigurationClosing;		
	}	
}


/*---------------------------------------------------------------------------
** connectionRemove( )
**
** Remove a connection from the connection array
**---------------------------------------------------------------------------
*/

void connectionRemove( INT32 nConnectionRemove )
{
	INT32 nConnection;
	INT32 nInstance = gConnections[nConnectionRemove].cfg.nInstance;	

	connectionGoOffline( nConnectionRemove );
					
	/* Clean up dynamic data */
	utilRemoveFromMemoryPool( &gConnections[nConnectionRemove].cfg.iConnectionPathOffset, &gConnections[nConnectionRemove].cfg.iConnectionPathSize );
	utilRemoveFromMemoryPool( &gConnections[nConnectionRemove].cfg.iConnectionNameOffset, &gConnections[nConnectionRemove].cfg.iConnectionNameSize );
	utilRemoveFromMemoryPool( &gConnections[nConnectionRemove].cfg.iModuleConfig1Offset, &gConnections[nConnectionRemove].cfg.iModuleConfig1Size );
	utilRemoveFromMemoryPool( &gConnections[nConnectionRemove].cfg.iModuleConfig2Offset, &gConnections[nConnectionRemove].cfg.iModuleConfig2Size );
	utilRemoveFromMemoryPool( &gConnections[nConnectionRemove].cfg.iConnectionTagOffset, &gConnections[nConnectionRemove].cfg.iConnectionTagSize );
	utilRemoveFromMemoryPool( &gConnections[nConnectionRemove].cfg.iTagOffset, &gConnections[nConnectionRemove].cfg.iTagSize );
	utilRemoveFromMemoryPool( &gConnections[nConnectionRemove].cfg.iDataOffset, &gConnections[nConnectionRemove].cfg.iDataSize );
		
	gnConnections--;
	
	for( nConnection = nConnectionRemove; nConnection < gnConnections; nConnection++ )		/* Shift the sessions with the higher index to fill in the void */
		memcpy( &gConnections[nConnection], &gConnections[nConnection+1], sizeof(CONNECTION) );	
	
	if ( nInstance != INVALID_INSTANCE )			/* Instance would be 0 if we were in the early stages of establishing a connection when there was an error */
		notifyEvent( NM_CONNECTION_CLOSED, nInstance );

	DumpStr3("connectionRemove %d with Instance %d, total now %d", nConnectionRemove, nInstance, gnConnections);
}

/*---------------------------------------------------------------------------
** connectionRecalcTimeouts( )
**
** Recalculate timeout and inhibit intervals based on the connection settings
**---------------------------------------------------------------------------
*/

void connectionRecalcTimeouts( INT32 nConnection )
{
	INT32 i;
	
	/* By default, inhibit interval is the quarter of the producing I/O rate */
	if ( gConnections[nConnection].cfg.bOriginator )
	{
		if ( (gConnections[nConnection].cfg.bProductionOTInhibitInterval) && (gConnections[nConnection].cfg.bProductionOTInhibitInterval < (gConnections[nConnection].cfg.lProducingDataRate/1000)))
			gConnections[nConnection].lInhibitInterval = gConnections[nConnection].cfg.bProductionOTInhibitInterval;
		else
			gConnections[nConnection].lInhibitInterval = (gConnections[nConnection].cfg.lProducingDataRate/1000) >> 2; 
	}
	else
	{
		if ( (gConnections[nConnection].cfg.bProductionTOInhibitInterval) && (gConnections[nConnection].cfg.bProductionTOInhibitInterval < (gConnections[nConnection].cfg.lProducingDataRate/1000)))
			gConnections[nConnection].lInhibitInterval = gConnections[nConnection].cfg.bProductionTOInhibitInterval;
		else
			gConnections[nConnection].lInhibitInterval = (gConnections[nConnection].cfg.lProducingDataRate/1000) >> 2; 
	}
	
	/* Timeout multiplier of 0 indicates that timeout should be 4 times larger than producing 
       data rate, 1 - 8 times larger, 2 - 16 times, and so on */
	gConnections[nConnection].lTimeoutInterval = (gConnections[nConnection].cfg.lConsumingDataRate/1000) << 2; 
	for( i = 0; i < gConnections[nConnection].cfg.bTimeoutMultiplier; i++ )
		gConnections[nConnection].lTimeoutInterval <<= 1;    
	DumpStr2("connectionRecalcTimeouts Time Out set to %d for connection %d",
		gConnections[nConnection].lTimeoutInterval, nConnection);
}

/*---------------------------------------------------------------------------
** connectionResetAllTicks( )
**
** Reset produce and timeout ticks
**---------------------------------------------------------------------------
*/

void connectionResetAllTicks( INT32 nConnection )
{
	UINT32 lTick = platformGetTickCount();

	gConnections[nConnection].lProduceTick = lTick + gConnections[nConnection].cfg.lProducingDataRate/1000 - 1;
	gConnections[nConnection].lTimeoutTick = ( gConnections[nConnection].lTimeoutInterval > DEFAULT_TIMEOUT ) ? 
							( lTick + gConnections[nConnection].lTimeoutInterval ) : ( lTick + DEFAULT_TIMEOUT );			
	gConnections[nConnection].lInhibitExpireTick = lTick + gConnections[nConnection].lInhibitInterval;
}

/*---------------------------------------------------------------------------
** connectionAdvanceProducingTicks( )
**
** I/O has just been sent, advance produce tick for next production
**---------------------------------------------------------------------------
*/

void connectionAdvanceProducingTicks( INT32 nConnection )
{
	UINT32 lTick = platformGetTickCount();
	
	gConnections[nConnection].lProduceTick += gConnections[nConnection].cfg.lProducingDataRate/1000;	
	gConnections[nConnection].lInhibitExpireTick = lTick + gConnections[nConnection].lInhibitInterval;
}

/*---------------------------------------------------------------------------
** connectionResetProducingTicks( )
**
** Reset produce tick for next production
**---------------------------------------------------------------------------
*/

void connectionResetProducingTicks( INT32 nConnection )
{
	UINT32 lTick = platformGetTickCount();
	
	gConnections[nConnection].lProduceTick = lTick + gConnections[nConnection].cfg.lProducingDataRate/1000;	
	gConnections[nConnection].lInhibitExpireTick = lTick + gConnections[nConnection].lInhibitInterval;
}

/*---------------------------------------------------------------------------
** connectionResetConsumingTicks( )
**
** I/O has just been received, reset the timeout tick
**---------------------------------------------------------------------------
*/

void connectionResetConsumingTicks( INT32 nConnection )
{
	UINT32 lTick = platformGetTickCount();

	gConnections[nConnection].lTimeoutTick = lTick + gConnections[nConnection].lTimeoutInterval;	
}

/*---------------------------------------------------------------------------
** connectionIsTimedOut( )
**
** Return TRUE if connection is timed out, FALSE otherwise
**---------------------------------------------------------------------------
*/

BOOL connectionIsTimedOut( INT32 nConnection )
{
	UINT32 lTick = platformGetTickCount();
	
	if ( IS_TICK_GREATER(lTick, gConnections[nConnection].lTimeoutTick) )	
	{
		DumpStr3("connectionIsTimedOut TIMED OUT Connection %d CurrentTick %d Timing out at %d", nConnection,
			lTick, gConnections[nConnection].lTimeoutTick);
		return TRUE;
	}
	else
		return FALSE;
}

/*---------------------------------------------------------------------------
** connectionIsTimeToProduce( )
**
** Return TRUE if it's the time for a cyclic production, FALSE otherwise
**---------------------------------------------------------------------------
*/

BOOL connectionIsTimeToProduce( INT32 nConnection )
{
	UINT32 lTick = platformGetTickCount();

	if ( IS_TICK_GREATER(lTick, gConnections[nConnection].lProduceTick) )	
	    return TRUE;
	else
		return FALSE;
}


/*---------------------------------------------------------------------------
** connectionIsInhibitExpired( )
**
** Return TRUE if we can produce on a COS request, FALSE otherwise
**---------------------------------------------------------------------------
*/

BOOL connectionIsInhibitExpired( INT32 nConnection )
{
	UINT32 lTick = platformGetTickCount();

	if ( IS_TICK_GREATER(gConnections[nConnection].lInhibitExpireTick, lTick) )	
	    return FALSE;
	else
		return TRUE;
}

/*---------------------------------------------------------------------------
** connectionGetIndexFromInstance( )
**
** Returns index in the connection array based on the connection instance.
** Returns INVALID_CONNECTION if the connection instance was not found.
**---------------------------------------------------------------------------
*/

INT32 connectionGetIndexFromInstance(INT32 nInstance)
{
	INT32  nConnection;	
	
	for ( nConnection = 0; nConnection < gnConnections; nConnection++ ) 
	{
		if ( gConnections[nConnection].cfg.nInstance == nInstance ||
			 gConnections[nConnection].nCCProducingInstance == nInstance )			 
			return nConnection;						
	}

	return INVALID_CONNECTION;
}

/*---------------------------------------------------------------------------
** connectionGetIndexFromConnPoint( )
**
** Returns index in the connection array based on the connection point.
** Returns INVALID_CONNECTION if the connection point was not found.
**---------------------------------------------------------------------------
*/

INT32 connectionGetIndexFromConnPoint(INT32 nConnPoint, BOOL* pbProducing)
{
	INT32  nConnection;	
	
	for ( nConnection = 0; nConnection < gnConnections; nConnection++ ) 
	{
		if ( gConnections[nConnection].cfg.iProducingConnPoint == nConnPoint )
		{
			*pbProducing = TRUE;
			return nConnection;						
		}
		else if ( gConnections[nConnection].cfg.iConsumingConnPoint == nConnPoint )			 
		{
			*pbProducing = FALSE;
			return nConnection;						
		}			
	}

	return INVALID_CONNECTION;
}

/*---------------------------------------------------------------------------
** connectionGetNumOriginatorEstablished( )
**
** Returns the number of the originating connections with the status 
** ConnectionEstablished
**---------------------------------------------------------------------------
*/

INT32 connectionGetNumOriginatorEstablished()
{
	INT32  nConnection;	
	INT32  nEstablished = 0;
	
	for ( nConnection = 0; nConnection < gnConnections; nConnection++ ) 
	{
		if ( gConnections[nConnection].cfg.bOriginator && gConnections[nConnection].lConnectionState == ConnectionEstablished )
			nEstablished++;						
	}

	return nEstablished;
}

/*---------------------------------------------------------------------------
** connectionGetNumOriginatorEstablishedOrClosing( )
**
** Returns the number of the originating connections with the status 
** ConnectionEstablished or ConnectionClosing
**---------------------------------------------------------------------------
*/

INT32 connectionGetNumOriginatorEstablishedOrClosing()
{
	INT32  nConnection;	
	INT32  nEstablished = 0;
	
	for ( nConnection = 0; nConnection < gnConnections; nConnection++ ) 
	{
		if ( gConnections[nConnection].cfg.bOriginator && 
			 (gConnections[nConnection].lConnectionState == ConnectionEstablished || gConnections[nConnection].lConnectionState == ConnectionClosing) )
			nEstablished++;						
	}

	return nEstablished;
}


/*---------------------------------------------------------------------------
** connectionGetMaxInstance( )
**
** Returns maximum instance in the connection array.
**---------------------------------------------------------------------------
*/

INT32 connectionGetMaxInstance()
{
	INT32  nConnection;	
	INT32 nMaxInstance = 0;
	
	for ( nConnection = 0; nConnection < gnConnections; nConnection++ ) 
	{
		if ( gConnections[nConnection].cfg.nInstance > nMaxInstance )
			nMaxInstance = gConnections[nConnection].cfg.nInstance;						
	}

	return nMaxInstance;
}

/*---------------------------------------------------------------------------
** connectionGetUnusedInstance( )
**
** Returns the first unused instance in the connection array.
**---------------------------------------------------------------------------
*/

INT32 connectionGetUnusedInstance()
{
	INT32  nConnection;	
	INT32  nInstance = 1;
			
	for( nInstance = 1; nInstance <= MAX_CONNECTIONS; nInstance++ )
	{
		nConnection = connectionGetIndexFromInstance( nInstance );

		if ( nConnection == ERROR_STATUS )
			return nInstance;
	}

	return ERROR_STATUS;
}


/*---------------------------------------------------------------------------
** connectionAnotherPending( )
**
** Return TRUE if there is another pending connection with the same IP 
** address that's in the process of opening or closing.
**---------------------------------------------------------------------------
*/

BOOL connectionAnotherPending( INT32 nConnection )
{
	INT32  i;	
	
	for ( i = 0; i < gnConnections; i++ ) 
	{
		/* Find another connection with the same IP address */
		if ( i != nConnection && gConnections[nConnection].lIPAddress == gConnections[i].lIPAddress )
		{
			/* Check if the connection is being opened */
			if ( gConnections[i].lConnectionState == ConnectionConfiguring && 
			     ( gConnections[i].lConfigurationState == ConfigurationWaitingForSession || 
			       gConnections[i].lConfigurationState == ConfigurationWaitingForForwardOpenResponse ) )
				   return TRUE;						

			/* Check if the connection is being closed */
			if ( gConnections[i].lConnectionState == ConnectionClosing && 
			     gConnections[i].lConfigurationState == ConfigurationWaitingForForwardCloseResponse )
				   return TRUE;						
		}
	}

	return FALSE;
}

/*---------------------------------------------------------------------------
** connectionDelayPending( )
**
** Postpone the configuration of all other connections with the same IP 
** address for CONNECTION_CONFIGURATION_DELAY period.
**---------------------------------------------------------------------------
*/

void connectionDelayPending( INT32 nConnection )
{
	INT32  i;	
	
	for ( i = 0; i < gnConnections; i++ ) 
	{
		/* Find another connection with the same IP address */
		if ( i != nConnection && gConnections[nConnection].lIPAddress == gConnections[i].lIPAddress )
		{
			/* Check if the connection is being opened or closed */
			if ( ( gConnections[i].lConnectionState == ConnectionConfiguring && gConnections[i].lConfigurationState == ConfigurationLogged ) ||
				 ( gConnections[i].lConnectionState == ConnectionClosing && gConnections[i].lConfigurationState == ConfigurationClosing ) )
			{
				gConnections[i].lStartTick = platformGetTickCount() + CONNECTION_CONFIGURATION_DELAY;
			}				
		}
	}
}

/*---------------------------------------------------------------------------
** connectionGetIndexFromSerialNumber( )
**
** Returns index in the connection array based on the connection serial number.
** Returns INVALID_CONNECTION if the connection serial number was not found.
**---------------------------------------------------------------------------
*/

INT32 connectionGetIndexFromSerialNumber(UINT16 iConnectionSerialNbr)
{
	INT32  nConnection;	
	
	for ( nConnection = 0; nConnection < gnConnections; nConnection++ ) 
	{
		if ( gConnections[nConnection].iConnectionSerialNbr == iConnectionSerialNbr )
			return nConnection;						
	}

	return INVALID_CONNECTION;
}

/*---------------------------------------------------------------------------
** connectionGetConnectedCount( )
**
** Returns number of active connections
**---------------------------------------------------------------------------
*/

INT32 connectionGetConnectedCount()
{
	INT32  nConnection;	
	INT32  nCount = 0;
	
	for ( nConnection = 0; nConnection < gnConnections; nConnection++ ) 
	{
		if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
			nCount++;						
	}

	return nCount;
}

/*---------------------------------------------------------------------------
** connectionUpdateAllSequenceCount( )
**
** Update the data sequence count to indicate new data for all connections
** when, for example, the run mode changes
**---------------------------------------------------------------------------
*/

void connectionUpdateAllSequenceCount()
{
	INT32  nConnection;	
		
	for( nConnection = 0; nConnection < gnConnections; nConnection++ )
	{
		if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
			gConnections[nConnection].iOutDataSeqNbr++;
	}
}

/*---------------------------------------------------------------------------
** connectionGetFirstMulticastProducer( )
**
** Returns the connection index for the first connection producing to the 
** same multicast as nConnection does 
**---------------------------------------------------------------------------
*/

INT32 connectionGetFirstMulticastProducer(INT32 nConnection)
{
	INT32  i;	

	/* Is not applicable if Originator or if T->O is PointToPoint */
	if ( gConnections[nConnection].cfg.bOriginator || 
		 gConnections[nConnection].cfg.iProducingConnectionType != Multicast )
		return INVALID_CONNECTION;
	
	for ( i = 0; i < nConnection; i++ ) 
	{
		if ( gConnections[i].lConnectionState == ConnectionEstablished && 
			 gConnections[i].cfg.iProducingConnectionType == Multicast && 
			 gConnections[i].cfg.iProducingConnPoint == gConnections[nConnection].cfg.iProducingConnPoint )
			return i;						
	}

	return INVALID_CONNECTION;
}

/*---------------------------------------------------------------------------
** connectionGetAnyMulticastProducer( )
**
** Returns the connection index for any connection producing to the 
** same multicast as nConnection does 
**---------------------------------------------------------------------------
*/

INT32 connectionGetAnyMulticastProducer(INT32 nConnection)
{
	INT32  i;	

	/* Is not applicable if Originator or if T->O is PointToPoint */
	if ( gConnections[nConnection].cfg.bOriginator || 
		 gConnections[nConnection].cfg.iProducingConnectionType != Multicast )
		return INVALID_CONNECTION;
	
	for ( i = 0; i < gnConnections; i++ ) 
	{
		if ( i != nConnection &&
			 gConnections[i].lConnectionState == ConnectionEstablished && 
			 gConnections[i].cfg.iProducingConnectionType == Multicast && 
			 gConnections[i].cfg.iProducingConnPoint == gConnections[nConnection].cfg.iProducingConnPoint &&
			 gConnections[i].cfg.iOutputDataSize == gConnections[nConnection].cfg.iOutputDataSize )
			return i;						
	}

	return INVALID_CONNECTION;
}

/*---------------------------------------------------------------------------
** connectionGetMulticastProducerCount( )
**
** Returns the number of all other connections producing to the 
** same multicast as nConnection does 
**---------------------------------------------------------------------------
*/

INT32 connectionGetMulticastProducerCount(INT32 nConnection)
{
	INT32  i;	
	INT32  nCount = 0;

	/* Is not applicable if Originator or if T->O is PointToPoint */
	if ( gConnections[nConnection].cfg.bOriginator || 
		 gConnections[nConnection].cfg.iProducingConnectionType != Multicast )
		return 0;
	
	for ( i = 0; i < gnConnections; i++ ) 
	{
		if ( i != nConnection &&
			 gConnections[i].lConnectionState == ConnectionEstablished && 
			 gConnections[i].cfg.iProducingConnectionType == Multicast && 
			 gConnections[i].cfg.iProducingConnPoint == gConnections[nConnection].cfg.iProducingConnPoint &&
			 gConnections[i].cfg.iOutputDataSize == gConnections[nConnection].cfg.iOutputDataSize )
			nCount++;						
	}

	return nCount;
}

#ifdef ET_IP_SCANNER
/*---------------------------------------------------------------------------
** connectionSetConnectionFlag( )
**
** Set connection flag member based on the configuration parameters
**---------------------------------------------------------------------------
*/

void connectionSetConnectionFlag(INT32 nConnection)
{
	gConnections[nConnection].cfg.iConnectionFlag = ( gConnections[nConnection].cfg.bOriginator ) ? 
			ORIGINATOR_CONNECTION_FLAG : TARGET_CONNECTION_FLAG;
		
	if ( gConnections[nConnection].cfg.bOriginator )
	{
		if ( gConnections[nConnection].cfg.iConsumingConnPoint == HEARTBEAT_CONN_POINT )
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_OT_FORMAT_HEARTBEAT;
		else if ( gConnections[nConnection].cfg.bOutputRunProgramHeader )
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_OT_FORMAT_RUN_IDLE;
		else
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_OT_FORMAT_PURE_DATA;

		if ( gConnections[nConnection].cfg.iProducingConnPoint == HEARTBEAT_CONN_POINT )
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_TO_FORMAT_HEARTBEAT;
		else if ( gConnections[nConnection].cfg.bInputRunProgramHeader )
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_TO_FORMAT_RUN_IDLE;
		else
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_TO_FORMAT_PURE_DATA;
	}
	else	/* If Target  */
	{
		if ( gConnections[nConnection].cfg.iConsumingConnPoint == HEARTBEAT_CONN_POINT )
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_OT_FORMAT_HEARTBEAT;
		else if ( gConnections[nConnection].cfg.bOutputRunProgramHeader )
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_TO_FORMAT_RUN_IDLE;
		else
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_TO_FORMAT_PURE_DATA;

		if ( gConnections[nConnection].cfg.iProducingConnPoint == HEARTBEAT_CONN_POINT )
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_TO_FORMAT_HEARTBEAT;
		else if ( gConnections[nConnection].cfg.bInputRunProgramHeader )
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_OT_FORMAT_RUN_IDLE;
		else
			gConnections[nConnection].cfg.iConnectionFlag |= CONFIG_TYPE_OT_FORMAT_PURE_DATA;
	}
}
#endif		


/*---------------------------------------------------------------------------
** connectionConvertToInternalCfgStorage( )
**
** Convert configuration structure to the internal storage presentation
** where part of the data is stored in the dynamic memory pool.
**---------------------------------------------------------------------------
*/

void connectionConvertToInternalCfgStorage(EtIPConnectionConfig* pcfg, CONNECTION_CFG* pConfig)
{
	pConfig->nInstance = pcfg->nInstance;
	pConfig->bOriginator = pcfg->bOriginator;
	pConfig->iConnectionFlag = pcfg->iConnectionFlag;
	pConfig->iConnectionPathSize = UINT16(strlen(pcfg->connectionPath) + 1);
	pConfig->iConnectionPathOffset = utilAddToMemoryPool( (unsigned char*)pcfg->connectionPath, pConfig->iConnectionPathSize );
	pConfig->bSlot = pcfg->bSlot;	
	pConfig->iConfigConnInstance = pcfg->iConfigConnInstance;
	pConfig->iConsumingConnPoint = pcfg->iConsumingConnPoint;
	pConfig->iProducingConnPoint = pcfg->iProducingConnPoint;
	pConfig->iConnectionTagSize = pcfg->iConnectionTagSize;
	pConfig->iConnectionTagOffset = utilAddToMemoryPool( (unsigned char*)pcfg->connectionTag, pConfig->iConnectionTagSize );
	pConfig->lProducingDataRate = pcfg->lProducingDataRate;
	pConfig->lConsumingDataRate = pcfg->lConsumingDataRate;
	pConfig->bProductionOTInhibitInterval = pcfg->bProductionOTInhibitInterval;
	pConfig->bProductionTOInhibitInterval = pcfg->bProductionTOInhibitInterval;
	pConfig->bOutputRunProgramHeader = pcfg->bOutputRunProgramHeader;
	pConfig->iOutputDataOffset = pcfg->iOutputDataOffset;
	pConfig->iOutputDataSize = pcfg->iOutputDataSize;
	pConfig->bInputRunProgramHeader = pcfg->bInputRunProgramHeader;
	pConfig->iInputDataOffset = pcfg->iInputDataOffset;
	pConfig->iInputDataSize = pcfg->iInputDataSize;
	pConfig->iProducingConnectionType = pcfg->iProducingConnectionType;
	pConfig->iConsumingConnectionType = pcfg->iConsumingConnectionType;
	pConfig->iProducingPriority = pcfg->iProducingPriority;
	pConfig->iConsumingPriority = pcfg->iConsumingPriority;
	pConfig->bTransportClass = pcfg->bTransportClass;
	pConfig->bTransportType = pcfg->bTransportType;
	pConfig->bTimeoutMultiplier = pcfg->bTimeoutMultiplier;
	pConfig->iWatchdogTimeoutAction = pcfg->iWatchdogTimeoutAction;
	pConfig->lWatchdogTimeoutReconnectDelay = pcfg->lWatchdogTimeoutReconnectDelay;
	memcpy(&pConfig->deviceId, &pcfg->deviceId, sizeof(EtIPDeviceID));
	pConfig->iConnectionNameSize = pcfg->iConnectionNameSize * 2;
	pConfig->iConnectionNameOffset = utilAddToMemoryPool( (unsigned char*)pcfg->connectionName, pConfig->iConnectionNameSize );
	pConfig->iModuleConfig1Size = pcfg->iModuleConfig1Size;
	pConfig->iModuleConfig1Offset = utilAddToMemoryPool( pcfg->moduleConfig1, pConfig->iModuleConfig1Size );
	pConfig->iModuleConfig2Size = pcfg->iModuleConfig2Size;
	pConfig->iModuleConfig2Offset = utilAddToMemoryPool( pcfg->moduleConfig2, pConfig->iModuleConfig2Size );	
	pConfig->bService = pcfg->request.bService;
	pConfig->iClass = pcfg->request.iClass;
	pConfig->iInstance = pcfg->request.iInstance;
	pConfig->iAttribute = pcfg->request.iAttribute;
	pConfig->iMember = pcfg->request.iMember;
	pConfig->iTagSize = pcfg->request.iTagSize;
	pConfig->iTagOffset = utilAddToMemoryPool( (unsigned char*)pcfg->request.requestTag, pConfig->iTagSize );	
	pConfig->iDataSize = pcfg->request.iDataSize;
	pConfig->iDataOffset = utilAddToMemoryPool( pcfg->request.requestData, pConfig->iDataSize );	
}


/*---------------------------------------------------------------------------
** connectionConvertFromInternalCfgStorage( )
**
** Convert configuration structure from the internal storage presentation
** where part of the data is stored in the dynamic memory pool to the 
** client recognizable structure.
**---------------------------------------------------------------------------
*/

void connectionConvertFromInternalCfgStorage(CONNECTION_CFG* pcfg, EtIPConnectionConfig* pConfig)
{
	pConfig->nInstance = pcfg->nInstance;
	pConfig->bOriginator = pcfg->bOriginator;
	pConfig->iConnectionFlag = pcfg->iConnectionFlag;
	memcpy(pConfig->connectionPath, MEM_PTR(pcfg->iConnectionPathOffset), pcfg->iConnectionPathSize);	
	pConfig->connectionPath[pcfg->iConnectionPathSize] = 0;
	pConfig->bSlot = pcfg->bSlot;	
	pConfig->iConfigConnInstance = pcfg->iConfigConnInstance;
	pConfig->iConsumingConnPoint = pcfg->iConsumingConnPoint;
	pConfig->iProducingConnPoint = pcfg->iProducingConnPoint;
	memcpy(pConfig->connectionTag, MEM_PTR(pcfg->iConnectionTagOffset), pcfg->iConnectionTagSize);	
	pConfig->iConnectionTagSize = pcfg->iConnectionTagSize;
	pConfig->lProducingDataRate = pcfg->lProducingDataRate;
	pConfig->lConsumingDataRate = pcfg->lConsumingDataRate;
	pConfig->bProductionOTInhibitInterval = pcfg->bProductionOTInhibitInterval;
	pConfig->bProductionTOInhibitInterval = pcfg->bProductionTOInhibitInterval;
	pConfig->bOutputRunProgramHeader = pcfg->bOutputRunProgramHeader;
	pConfig->iOutputDataOffset = pcfg->iOutputDataOffset;
	pConfig->iOutputDataSize = pcfg->iOutputDataSize;
	pConfig->bInputRunProgramHeader = pcfg->bInputRunProgramHeader;
	pConfig->iInputDataOffset = pcfg->iInputDataOffset;
	pConfig->iInputDataSize = pcfg->iInputDataSize;
	pConfig->iProducingConnectionType = pcfg->iProducingConnectionType;
	pConfig->iConsumingConnectionType = pcfg->iConsumingConnectionType;
	pConfig->iProducingPriority = pcfg->iProducingPriority;
	pConfig->iConsumingPriority = pcfg->iConsumingPriority;
	pConfig->bTransportClass = pcfg->bTransportClass;
	pConfig->bTransportType = pcfg->bTransportType;
	pConfig->bTimeoutMultiplier = pcfg->bTimeoutMultiplier;
	pConfig->iWatchdogTimeoutAction = pcfg->iWatchdogTimeoutAction;
	pConfig->lWatchdogTimeoutReconnectDelay = pcfg->lWatchdogTimeoutReconnectDelay;
	memcpy(&pConfig->deviceId, &pcfg->deviceId, sizeof(EtIPDeviceID));
	memcpy(pConfig->connectionName, MEM_PTR(pcfg->iConnectionNameOffset), pcfg->iConnectionNameSize);	
	pConfig->iConnectionNameSize = pcfg->iConnectionNameSize / 2;
	memcpy(pConfig->moduleConfig1, MEM_PTR(pcfg->iModuleConfig1Offset), pcfg->iModuleConfig1Size);	
	pConfig->iModuleConfig1Size = pcfg->iModuleConfig1Size;
	memcpy(pConfig->moduleConfig2, MEM_PTR(pcfg->iModuleConfig2Offset), pcfg->iModuleConfig2Size);	
	pConfig->iModuleConfig2Size = pcfg->iModuleConfig2Size;	
	pConfig->request.bService = pcfg->bService;
	pConfig->request.iClass = pcfg->iClass;
	pConfig->request.iInstance = pcfg->iInstance;
	pConfig->request.iAttribute = pcfg->iAttribute;
	pConfig->request.iMember = pcfg->iMember;
	pConfig->request.iTagSize = pcfg->iTagSize;
	memcpy(pConfig->request.requestTag, MEM_PTR(pcfg->iTagOffset), pcfg->iTagSize );	
	pConfig->request.iDataSize = pcfg->iDataSize;
	memcpy(pConfig->request.requestData, MEM_PTR(pcfg->iDataOffset), pcfg->iDataSize );	
}
