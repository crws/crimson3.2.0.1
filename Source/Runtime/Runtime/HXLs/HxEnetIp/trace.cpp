/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** TRACE.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Outputting trace dump to the disk file or the debugger window
**
** To use trace functionality there should be two preprocessor definitions
** set: TRACE_OUTPUT enabling general trace support and either
** TRACE_FILE_OUTPUT to send it to the file or TRACE_DEBUGGER_OUTPUT
** to output to th edebug window. 
** In case of the file dump the program will output to the debug%n.txt file
** in the et_ip directory (or root directory for WinCE) on the c: drive.
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

#ifdef TRACE_OUTPUT

#ifdef TRACE_FILE_OUTPUT

/*---------------------------------------------------------------------------
** InitializeDump( )
**
** Prepare for the dump. If the target is the file, then make sure that the 
** output directory exists and determine the file name we are using for output.
** We are writing to the files debug0.txt, debug1.txt, and so on. So if
** the last file created is debug 75.txt we are going to create a new 
** debug76.txt and open it with the write privileges.
**---------------------------------------------------------------------------
*/
void InitializeDump()
{

	/* Create the file and open it for writing */
	platformInitLogFile();
		
}

/*---------------------------------------------------------------------------
** FinishDump( )
**
** Cleanup after dump. 
**---------------------------------------------------------------------------
*/

void FinishDump()
{
	platformCloseLogFile();
}

#endif /* #ifdef TRACE_FILE_OUTPUT */


/*---------------------------------------------------------------------------
** fDumpStr( )
**
** Dumps the string to the file or the debugger window. Accepts variable
** number of arguments. Arguments are passed using printf convention:
** %d in the format string indicates decimal integer argument, %x - hex
** integer, %s - a string, and so on.
**---------------------------------------------------------------------------
*/

void fDumpStr(const char* lpszFormat, ...)
{    
	char szBuf[1024];
	va_list args;

	/* Build the output string using printf convention */
	va_start(args, lpszFormat);	
	vsprintf(szBuf, lpszFormat, args);
    va_end(args);

	platformWriteLog(szBuf,TRUE);	
}

/*---------------------------------------------------------------------------
** fDumpStrNoNewLine( )
**
** Dumps the string to the file or the debugger window. Accepts variable
** number of arguments. Arguments are passed using printf convention:
** %d in the format string indicates decimal integer argument, %x - hex
** integer, %s - a string, and so on.
**---------------------------------------------------------------------------
*/

void fDumpStrNoNewLine(const char* lpszFormat, ...)
{    
	char szBuf[1024];
	va_list args;

	/* Build the output string using printf convention */
	va_start(args, lpszFormat);	
	vsprintf(szBuf, lpszFormat, args);
    va_end(args);

	platformWriteLog(szBuf,FALSE);
}

/*---------------------------------------------------------------------------
** fDumpBuf( )
**
** Dumps the buffer contents to the file or the debugger window. 
** Output the buffer as a string of bytes presented in the hexadecimal
** format and separated by a space.
**---------------------------------------------------------------------------
*/

void fDumpBuf(char* pBuf, int nLen)
{
	char szBuffer[MAX_PACKET_SIZE*3];	/* Each byte takes 2 chars plus spaces in between bytes */
	char szTemp[16];
    int nIndex = 0;		
	int i;
	int nByte;

	if ( nLen <= 0 || nLen > MAX_PACKET_SIZE )
		return;

	szBuffer[nIndex++] = '\n';
	
	for( i = 0; i < nLen; i++ )
	{
		nByte = (int)(unsigned char)pBuf[i];	/* Take each byte one by one */
		sprintf(szTemp, "%02x ", nByte);		/* Convert it to the Hex string presentation */
		szBuffer[nIndex++] = szTemp[0];			/* Incorporate in the big string */
		szBuffer[nIndex++] = szTemp[1];
		szBuffer[nIndex++] = ' ';
	}	
	
	szBuffer[nIndex++] = 0;

	platformWriteLog(szBuffer,FALSE);

}

#endif /* #ifdef TRACE_OUTPUT */
