/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** UTIL.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** General OS specific utilities.
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"


UINT8  gMemoryPool[MEMORY_POOL_SIZE];
UINT16 giMemoryPoolOffset;

/*---------------------------------------------------------------------------
** utilResetMemoryPoolOffset( )
**
** Removes the old data block from the memory pool and replaces it with the 
** new one with different length.
**---------------------------------------------------------------------------
*/

void utilResetMemoryPoolOffset( UINT16* piDataOffset, UINT16* piDataSize, UINT8* data, UINT16 iDataSize )
{
	utilRemoveFromMemoryPool( piDataOffset, piDataSize );
	*piDataSize = iDataSize;
	*piDataOffset = utilAddToMemoryPool( data, iDataSize );	
}

/*---------------------------------------------------------------------------
** utilAddToMemoryPool( )
**
** Stores passed in data in the memory pool.
** If data is NULL then memory is reserved, but not filled in.
** Returns the offset to the memory pool the data is stored at.
**---------------------------------------------------------------------------
*/

UINT16 utilAddToMemoryPool( UINT8* data, UINT16 iDataSize )
{	
	UINT16 iOffset = giMemoryPoolOffset;
	
	if ( !iDataSize )	/* If there is no data, do not allocate anything */
		return INVALID_MEMORY_OFFSET;

	/* Check if we have enough room */
	if ( iDataSize > (MEMORY_POOL_SIZE - giMemoryPoolOffset) )
	{
		DumpStr2("utilAddToMemoryPool ran out of memory: Requested: %hd, Available: %hd",
			iDataSize, (MEMORY_POOL_SIZE - giMemoryPoolOffset));
		notifyEvent( NM_OUT_OF_MEMORY, 0 );
		return INVALID_MEMORY_OFFSET;
	}

	if ( data )	/* If data pointer is not NULL then store the data in the dynamic pool */
		memcpy( &gMemoryPool[giMemoryPoolOffset], data, iDataSize );

	giMemoryPoolOffset += iDataSize;	/* Adjust the pool ceiling offset */
	
	return iOffset;	/* The data is stored at this offset */
}

/*---------------------------------------------------------------------------
** utilRemoveFromMemoryPool( )
**
** Removes the specified data block from the memory pool.
**---------------------------------------------------------------------------
*/

void utilRemoveFromMemoryPool( UINT16* piDataOffset, UINT16* piDataSize )
{
	INT32 i;
	UINT16 iDataOffset = *piDataOffset;
	UINT16 iDataSize = *piDataSize;
	INT32 nLen = giMemoryPoolOffset - iDataOffset - iDataSize;
		
	if ( !iDataSize || iDataOffset == INVALID_MEMORY_OFFSET )
		return;	/* Check that memory block has valid offset and size */

	/* Quick garbage collection is done every time the memory is released, instead of doing the 
	   full pool unfragmentation when needed */
	for ( i = 0; i < nLen; i++ )
	{		
		gMemoryPool[iDataOffset] = gMemoryPool[iDataOffset + iDataSize];
		iDataOffset++;
	}

	iDataOffset = *piDataOffset;	
	giMemoryPoolOffset -= iDataSize;	/* Adjust the pool ceiling offset */
	
	/* Adjust the offsets */
	for ( i = 0; i < gnConnections; i++ )
	{
		if ( gConnections[i].cfg.iConnectionPathOffset != INVALID_MEMORY_OFFSET && gConnections[i].cfg.iConnectionPathOffset > iDataOffset )
			gConnections[i].cfg.iConnectionPathOffset -= iDataSize;
		if ( gConnections[i].cfg.iConnectionTagOffset != INVALID_MEMORY_OFFSET && gConnections[i].cfg.iConnectionTagOffset > iDataOffset )
			gConnections[i].cfg.iConnectionTagOffset -= iDataSize;
		if ( gConnections[i].cfg.iConnectionNameOffset != INVALID_MEMORY_OFFSET && gConnections[i].cfg.iConnectionNameOffset > iDataOffset )
			gConnections[i].cfg.iConnectionNameOffset -= iDataSize;
		if ( gConnections[i].cfg.iModuleConfig1Offset != INVALID_MEMORY_OFFSET && gConnections[i].cfg.iModuleConfig1Offset > iDataOffset )
			gConnections[i].cfg.iModuleConfig1Offset -= iDataSize;
		if ( gConnections[i].cfg.iModuleConfig2Offset != INVALID_MEMORY_OFFSET && gConnections[i].cfg.iModuleConfig2Offset > iDataOffset )
			gConnections[i].cfg.iModuleConfig2Offset -= iDataSize;
		if ( gConnections[i].cfg.iTagOffset != INVALID_MEMORY_OFFSET && gConnections[i].cfg.iTagOffset > iDataOffset )
			gConnections[i].cfg.iTagOffset -= iDataSize;
		if ( gConnections[i].cfg.iDataOffset != INVALID_MEMORY_OFFSET && gConnections[i].cfg.iDataOffset > iDataOffset )
			gConnections[i].cfg.iDataOffset -= iDataSize;
	}

#ifdef ET_IP_SCANNER	
	for ( i = 0; i < gnConfigs; i++ )
	{
		if ( gConfigs[i].cfg.iConnectionPathOffset != INVALID_MEMORY_OFFSET && gConfigs[i].cfg.iConnectionPathOffset > iDataOffset )
			gConfigs[i].cfg.iConnectionPathOffset -= iDataSize;
		if ( gConfigs[i].cfg.iConnectionTagOffset != INVALID_MEMORY_OFFSET && gConfigs[i].cfg.iConnectionTagOffset > iDataOffset )
			gConfigs[i].cfg.iConnectionTagOffset -= iDataSize;
		if ( gConfigs[i].cfg.iConnectionNameOffset != INVALID_MEMORY_OFFSET && gConfigs[i].cfg.iConnectionNameOffset > iDataOffset )
			gConfigs[i].cfg.iConnectionNameOffset -= iDataSize;
		if ( gConfigs[i].cfg.iModuleConfig1Offset != INVALID_MEMORY_OFFSET && gConfigs[i].cfg.iModuleConfig1Offset > iDataOffset )
			gConfigs[i].cfg.iModuleConfig1Offset -= iDataSize;
		if ( gConfigs[i].cfg.iModuleConfig2Offset != INVALID_MEMORY_OFFSET && gConfigs[i].cfg.iModuleConfig2Offset > iDataOffset )
			gConfigs[i].cfg.iModuleConfig2Offset -= iDataSize;
		if ( gConfigs[i].cfg.iTagOffset != INVALID_MEMORY_OFFSET && gConfigs[i].cfg.iTagOffset > iDataOffset )
			gConfigs[i].cfg.iTagOffset -= iDataSize;
		if ( gConfigs[i].cfg.iDataOffset != INVALID_MEMORY_OFFSET && gConfigs[i].cfg.iDataOffset > iDataOffset )
			gConfigs[i].cfg.iDataOffset -= iDataSize;
	}
#endif /* #ifdef ET_IP_SCANNER	*/

	for ( i = 0; i < gnRequests; i++ )
	{
		if ( gRequests[i].iTagOffset != INVALID_MEMORY_OFFSET && gRequests[i].iTagOffset > iDataOffset )
			gRequests[i].iTagOffset -= iDataSize;
		if ( gRequests[i].iDataOffset != INVALID_MEMORY_OFFSET && gRequests[i].iDataOffset > iDataOffset )
			gRequests[i].iDataOffset -= iDataSize;
		if ( gRequests[i].iExtendedPathOffset != INVALID_MEMORY_OFFSET && gRequests[i].iExtendedPathOffset > iDataOffset )
			gRequests[i].iExtendedPathOffset -= iDataSize;		
	}

	/* Make sure the same memory block is not released twice */
	*piDataSize = 0;		
	*piDataOffset = INVALID_MEMORY_OFFSET;	
}

/*---------------------------------------------------------------------------
** utilAddrFromPath( )
**
** Get an IP address from the network path representation.
** szAddr is usually an IP string (i.e."216.233.160.129").
**---------------------------------------------------------------------------
*/

UINT32 utilAddrFromPath( const char* szAddr )
{
	UINT32      lIPAddress = ERROR_STATUS;
	struct hostent	*phost;

	/*
	** Look up the name, assumed initially it is in dotted format.
	** If it wasn't dotted, maybe it's a symbolic name 
	*/
	if ( ( lIPAddress = inet_addr( szAddr ) ) == ERROR_STATUS )
	{
		if( ( phost = gethostbyname( szAddr ) ) )
		{
			memcpy( &lIPAddress, phost->h_addr_list[0], sizeof(UINT32) );
		}	
	}

	return lIPAddress;
}

/*---------------------------------------------------------------------------
** utilParseNetworkPath( )
**
** Determine if network path is local or remote. 
** If remote extract the extended part and convert it to the Extended Port
** segment representation.
** Return TRUE if network path is valid, FALSE otherwise.
**---------------------------------------------------------------------------
*/

BOOL utilParseNetworkPath( char* szNetworkPath, BOOL* pbLocal, char* extendedPath, UINT16* piExtendedPathLen )
{
	INT32 nLen = strlen( szNetworkPath );
	INT32 i;
	BOOL  bPortFilled = FALSE;
	INT32 nPort = 0;
	UINT8  szPort[64];
	INT32 nPortSize = 0;
	INT32 nNode;
	UINT8  szNode[128];
	INT32 nNodeSize = 0;
	UINT16 iExtendedPathLen = 0;
	
	*pbLocal = TRUE;
	iExtendedPathLen = 0;
	
	for( i = 0; i < nLen; i++ )
	{
		//AfxTrace("%d\t%c\n", i, szNetworkPath[i]);

		if ( *pbLocal )
		{
			if ( szNetworkPath[i] == ',' )
			{
				if ( szNetworkPath[nLen-1] != ',' )	/* If the last char is not ',' add it now */
				{
					szNetworkPath[nLen++] = ',';
					szNetworkPath[nLen] = 0;
				}
				*pbLocal = FALSE;
				szNetworkPath[i] = 0;				
			}
		}
		else
		{
			if ( szNetworkPath[i] != ',' )
			{			
				if ( !bPortFilled )
				{
					if ( szNetworkPath[i] < '0' || szNetworkPath[i] > '9' )
						return FALSE;
					szPort[nPortSize++] = szNetworkPath[i];
				}
				else
					szNode[nNodeSize++] = szNetworkPath[i];
			}
			else
			{
				if ( !bPortFilled )					   /* Convert the port to integer and store in nPort */
				{
					if ( nPortSize == 0 )			   /* 2 commas in a row are not allowed */
						return FALSE;
					szPort[nPortSize] = 0;
					nPort = atol( (const char*)szPort );
					if ( nPort < 0 || nPort > 0xffff ) /* Port is limited to 16 bits */
						return FALSE;
					bPortFilled = TRUE;
				}
				else
				{
					if ( nNodeSize == 0 )			   /* 2 commas in a row are not allowed */
						return FALSE;
					szNode[nNodeSize] = 0;
					/* Check if this is one byte presentation of the slot or any other network path */
					/* If it is one byte representation the length is limited to 3 numeric chars */
					if ( nNodeSize <= 3 && szNode[0] >= '0' && szNode[0] <= '9' && 
						 ( nNodeSize < 2 || (szNode[1] >= '0' && szNode[1] <= '9') ) &&
						 ( nNodeSize < 3 || (szNode[2] >= '0' && szNode[2] <= '9') ) )
					{
						nNode = atol( (const char*)szNode );
						if ( nNode < 0 || nNode > 0xff ) /* Numeric node is limited to 8 bits */
							return FALSE;
						if ( nPort < 0xf )				 /* Put it in the segment byte */
							extendedPath[iExtendedPathLen++] = PATH_SEGMENT | (BYTE)nPort;
						else							 /* Add separate 16-bit port value */
						{
							extendedPath[iExtendedPathLen++] = PATH_SEGMENT | 0xf;
							UINT16_SET( &extendedPath[iExtendedPathLen], nPort );
							iExtendedPathLen += sizeof(UINT16);
						}
						extendedPath[iExtendedPathLen++] = (BYTE)nNode;	/* Add numeric node */
					}
					else	/* We have a string presentation of the network path */
					{
						if ( utilAddrFromPath( (const char*)szNode ) == ERROR_STATUS )
							return FALSE;
						if ( nPort < 0xf )				 /* Put it in the segment byte */
							extendedPath[iExtendedPathLen++] = PATH_SEGMENT | EXTENDED_LINK_MASK | (BYTE)nPort;
						else							 /* Add separate 16-bit port value */
						{
							extendedPath[iExtendedPathLen++] = PATH_SEGMENT | EXTENDED_LINK_MASK | 0xf;
							UINT16_SET( &extendedPath[iExtendedPathLen], nPort );
							iExtendedPathLen += sizeof(UINT16);
						}						
						if ( nNodeSize % 2 )
							szNode[nNodeSize++] = 0; /* Add pad byte to make node length even */
						extendedPath[iExtendedPathLen++] = (BYTE)nNodeSize;
						memcpy( &extendedPath[iExtendedPathLen], szNode, nNodeSize );
						iExtendedPathLen += (UINT16)nNodeSize;					
					}
					nNodeSize = 0;
					nPortSize = 0;						
					bPortFilled = FALSE;
				}
			}
		} 
	}

	*piExtendedPathLen = iExtendedPathLen;
	return TRUE;
}



/*---------------------------------------------------------------------------
** utilGetUniqueID( )
**
** Return a UINT32 value unique between lSessionID, lSessionTag for all sessions 
** and between lConsumingCID, lProducingCID, iConnectionSerialNbr for all 
** connections.
**---------------------------------------------------------------------------
*/

UINT32 utilGetUniqueID()
{
	UINT32 lVal = platformGetTickCount();	/* Take a guess */
	
	while (1)
	{		
		/* Skip 0xffff and 0xffffffff allocated to mark invalid ids */
		if ( (UINT16)lVal != INVALID_CONNECTION_SERIAL_NBR && lVal != INVALID_CONNECTION &&
			 lVal != INVALID_SESSION )
				if ( utilIsIDUnique( lVal ) )	
					return lVal;
		
		lVal++;
	}

	return ERROR_STATUS;			/* We should never get here */
}

/*---------------------------------------------------------------------------
** utilIsIDUnique( )
**
** Checks if passed value is unique between lSessionID, lSessionTag for all 
** sessions and between lConsumingCID, lProducingCID, iConnectionSerialNbr 
** for all connections.
**---------------------------------------------------------------------------
*/

BOOL utilIsIDUnique(UINT32 lVal)
{
	INT32  nSession, nConnection;	
	
	/* Check if unique between lSessionID, lSessionTag for all sessions */
	for( nSession = 0; nSession < gnSessions; nSession++ )
	{
		if ( gSessions[nSession].lSessionTag == lVal )
			return FALSE;		
	}

	/* Check if unique between lConsumingCID, lProducingCID, iConnectionSerialNbr for all connections */
	for( nConnection = 0; nConnection <= gnConnections; nConnection++ )
	{
		if ( gConnections[nConnection].lConsumingCID == lVal ||
			 gConnections[nConnection].lProducingCID == lVal ||
			 gConnections[nConnection].iConnectionSerialNbr == (UINT16)lVal )
				return FALSE;						
	}	
	
	return TRUE;
}


