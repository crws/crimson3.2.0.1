
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_Fat32FileSystem_HPP

#define	INCLUDE_Fat32FileSystem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

#include "FatFileSystem.hpp"

#include "Fat32BootSector.hpp"

#include "Fat32FSInfo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Fat32 File System
//

class CFat32FileSystem : public CFatFileSystem
{
	public:
		// Constructor
		CFat32FileSystem(UINT iDisk, UINT iVolume, UINT uBoot);

		// Destructor
		~CFat32FileSystem(void);

		// Attributes
		INT64 GetSize(void);
		INT64 GetFree(void);

		// Methods
		BOOL Init(void);
		BOOL Start(void);
		BOOL Stop(void);

		// Enumeration
		BOOL GetRoot(FSIndex &Index);
		BOOL IsRoot(FSIndex const &Index);

	protected:
		// Constants
		static UINT const entryInvalid	 = 0xFFFFFFFF;
		static UINT const entryEoc	 = 0x0FFFFFFF;
		static UINT const entryReserved	 = 0x0FFFFFF8;
		static UINT const entryBad	 = 0x0FFFFFF7;
		static UINT const entryFree	 = 0x00000000;
		static UINT const entryVolClean	 = 0x08000000;
		static UINT const entryHardError = 0x04000000;
		static UINT const entryMask	 = 0x0FFFFFFF;

		// Data
		CFat32BootSector  m_Boot;
		CFat32FSInfo      m_Info;

		// Layout
		UINT GetRootCluster(void) const;
		UINT GetFirstSector(UINT uCluster) const;

		// Implementation
		BOOL ReadBoot(void);
		BOOL CopyBoot(void);
		BOOL InitFat(void);
		BOOL InitRoot(void);
		BOOL InitInfo(void);
		BOOL ReadInfo(void);
		BOOL SaveInfo(void);
		BOOL FindInfo(void);

		// Flags
		BOOL IsVolumeClean(void);
		BOOL IsVolumeDirty(void);
		BOOL HasHardError(void);
		BOOL SetVolumeClean(void);
		BOOL SetVolumeDirty(void);
		BOOL SetHardError(void);

		// Data Allocation
		UINT AllocData(UINT uAlloc, BOOL fZero);
		UINT ReAllocData(UINT uCluster, UINT uAlloc, BOOL fZero);
		BOOL FreeData(UINT uCluster);

		// Chain
		UINT FindNext(UINT uCluster);
		UINT FindNext(UINT uCluster, UINT uCount);
		UINT FindLast(UINT uCluster);
		UINT FindCount(UINT uCluster);
		UINT FindFree(void);
		UINT FindFreeCount(void);

		// Fat Entry
		UINT ReadFatEntry(UINT uCluster);
		BOOL WriteFatEntry(UINT uCluster, UINT uEntry);
		UINT ReadFatEntry(UINT uFat, UINT uCluster);
		BOOL WriteFatEntry(UINT uFat, UINT uCluster, UINT uEntry);
		BOOL MapFatEntry(UINT uFat, UINT uCluster, CDataSector &Ptr) const;
	};

// End of File

#endif

