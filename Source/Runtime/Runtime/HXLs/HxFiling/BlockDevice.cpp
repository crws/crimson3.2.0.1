
#include "Intern.hpp"

#include "BlockDevice.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Block Device Object
//

// Constructor

CBlockDevice::CBlockDevice(void)
{
	StdSetRef();

	ClearCounters();
	}

// Destructor

CBlockDevice::~CBlockDevice(void)
{
	}

// IUnknown

HRESULT CBlockDevice::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IBlockDevice);

	StdQueryInterface(IBlockDevice);

	StdQueryInterface(IDevice);

	return E_NOINTERFACE;
	}

ULONG CBlockDevice::AddRef(void)
{
	StdAddRef();
	}

ULONG CBlockDevice::Release(void)
{
	StdRelease();
	}

// IDeviceEx

BOOL CBlockDevice::Open(IUnknown *pUnk)
{
	return TRUE;
	}

// IBlockDevice

BOOL CBlockDevice::IsReady(void)
{
	return false;
	}

void CBlockDevice::Attach(IEvent *pEvent)
{
	}

void CBlockDevice::Arrival(PVOID pDevice)
{
	}

void CBlockDevice::Removal(void)
{
	}

UINT CBlockDevice::GetSectorCount(void)
{
	return GetCylinderCount() * GetHeadCount() * GetSectorsPerHead();
	}

UINT CBlockDevice::GetSectorSize(void)
{
	return 512;
	}

UINT CBlockDevice::GetCylinderCount(void)
{
	return 0;
	}

UINT CBlockDevice::GetHeadCount(void)
{
	return 0;
	}

UINT CBlockDevice::GetSectorsPerHead(void)
{
	return 0;
	}

BOOL CBlockDevice::WriteSector(UINT uSector, PCBYTE pData)
{
	m_uWriteCount++;

	return true;
	}

BOOL CBlockDevice::ReadSector(UINT uSector, PBYTE pData)
{
	m_uReadCount++;

	return true;
	}

void CBlockDevice::ClearCounters(void)
{
	m_uReadCount  = 0;

	m_uWriteCount = 0;
	}

UINT CBlockDevice::GetReadCount(void)
{
	return m_uReadCount;
	}

UINT CBlockDevice::GetWriteCount(void)
{
	return m_uWriteCount;
	}

// End of File