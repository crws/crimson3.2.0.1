
#include "Intern.hpp"

#include "FileCopy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// RLOS File Copier
//

// Instantiators

static IUnknown * Create_RlosFileCopier(PCTXT pName)
{
	return New CRlosFileCopy();
	}

// Registration

global void Register_RlosFileCopier(void)
{
	piob->RegisterInstantiator("fs.copier", Create_RlosFileCopier);
	}

// Constructor

CRlosFileCopy::CRlosFileCopy(void)
{
	StdSetRef();

	m_pFileMan  = NULL;
	
	m_pFileSys1 = NULL;

	m_pFileSys2 = NULL;

	m_pFile1    = NULL;

	m_pFile2    = NULL;

	AfxGetObject("fileman", 0, IFileManager, m_pFileMan);

	AllocBuffer(8192);
	}

// Destrictor

CRlosFileCopy::~CRlosFileCopy(void)
{
	FreeFiles();

	FreeFileSystems();
	
	FreeBuffer();
	
	m_pFileMan->Release();
	}

// IUnknown

HRESULT CRlosFileCopy::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IFileCopier);

	StdQueryInterface(IFileCopier);

	return E_NOINTERFACE;
	}

ULONG CRlosFileCopy::AddRef(void)
{
	StdAddRef();
	}

ULONG CRlosFileCopy::Release(void)
{
	StdRelease();
	}

// IFileCopier

BOOL CRlosFileCopy::CopyFile(PCTXT pSrc, PCTXT pDst, UINT uFlags)
{
	CFilename Src = pSrc;

	CFilename Dst = pDst;

	if( LockFileSystems(Src, Dst) ) {

		MakeFiles();

		BOOL fOk = CopyFile(Src, Dst, uFlags);

		FreeFiles();
		
		FreeFileSystems();

		return fOk;
		}

	return false;
	}

BOOL CRlosFileCopy::MoveFile(PCTXT pSrc, PCTXT pDst, UINT uFlags)
{
	CFilename Src = pSrc;

	CFilename Dst = pDst;

	if( LockFileSystems(Src, Dst) ) {

		MakeFiles();

		BOOL fOk = MoveFile(Src, Dst, uFlags);

		FreeFiles();
		
		FreeFileSystems();

		return fOk;
		}

	return false;
	}

BOOL CRlosFileCopy::CopyDir(PCTXT pSrc, PCTXT pDst, UINT uFlags)
{
	CFilename Src = pSrc;

	CFilename Dst = pDst;

	if( LockFileSystems(Src, Dst) ) {

		MakeFiles();

		BOOL fOk = CopyDir(Src, Dst, uFlags);

		FreeFiles();

		FreeFileSystems();

		return fOk;
		}

	return false;
	}

BOOL CRlosFileCopy::MoveDir(PCTXT pSrc, PCTXT pDst, UINT uFlags)
{
	CFilename Src = pSrc;

	CFilename Dst = pDst;

	if( LockFileSystems(Src, Dst) ) {

		MakeFiles();

		BOOL fOk = MoveDir(Src, Dst, uFlags);

		FreeFiles();

		FreeFileSystems();

		return fOk;
		}

	return false;
	}

// Helpers

BOOL CRlosFileCopy::OpenFiles(CFilename const &Src, CFilename const &Dst, UINT uFlags)
{
	UINT uMode = (uFlags & flagDelete) ? fileWrite | fileRead : fileRead;

	if( !m_pFile1->Open(Src, uMode) ) {

		return false;
		}

	if( m_pFile2->Open(Dst, fileWrite) ) {

		if( uFlags & copyOverwrite ) {

			return true;
			}

		if( uFlags & copyDiff ) {

			if( HasChanged() ) {

				return true;
				}
			}
			
		if( uFlags & copyNew ) {
		
			if( IsMoreRecent() ) {

				return true;
				}
			}

		return false;
		}
	else {
		CFilename Dir;
		
		if( Dst.GetDirectory(Dir) ) {

			if( !m_pFileSys2->CreateDir(Dir) ) {

				return false;
				}
			}

		if( !m_pFile2->Create(Dst) ) {

			return false;
			}
		
		return true;
		}
	}

BOOL CRlosFileCopy::CopyFile(CFilename const &Src, CFilename const &Dst, UINT uFlags)
{
	if( !OpenFiles(Src, Dst, uFlags) ) {

		return false;
		}

	UINT uCount = m_pFile1->GetLength();

	if( !m_pFile2->SetLength(uCount) ) {

		m_pFile2->Delete();

		return false;
		}

	while( uCount ) {

		UINT uThis = Min(m_uBuffer, uCount);

		if( m_pFile1->Read(m_pBuffer, uThis) == uThis ) {

			if( m_pFile2->Write(m_pBuffer, uThis) == uThis ) {

				uCount -= uThis;

				continue;
				}
			}

		m_pFile2->Delete();

		return false;
		}

	if( !m_pFile2->SetUnixTime(m_pFile1->GetUnixTime()) ) {

		return false;
		}

	return true;
	}

BOOL CRlosFileCopy::MoveFile(CFilename const &Src, CFilename const &Dst, UINT uFlags)
{
	if( CopyFile(Src, Dst, uFlags | flagDelete) ) {

		return m_pFile1->Delete();
		}
	
	return false;
	}

BOOL CRlosFileCopy::CopyDir(CFilename const &Src, CFilename const &Dst, UINT uFlags)
{
	FSIndex Index;

	if( !m_pFileSys1->MoveDir(Src, Index) ) {

		return false;
		}

	if( !m_pFileSys2->CreateDir(Dst) ) {

		return false;
		}

	m_pFileSys1->GetFirst(Index);

	do {
		if( !m_pFileSys1->IsValid(Index) ) {

			continue;
			}

		if( m_pFileSys1->IsSpecial(Index) ) {

			continue;
			}

		if( m_pFileSys1->IsFile(Index) ) {

			CFilename Name;

			if( m_pFileSys1->GetName(Index, Name) ) {

				CFilename FileSrc = Src + Name;

				CFilename FileDst = Dst + Name;

				CopyFile(FileSrc, FileDst, uFlags);
				}

			continue;
			}

		if( m_pFileSys1->IsDirectory(Index) ) {

			if( uFlags & copyRecursive ) { 

				CFilename Name;

				if( m_pFileSys1->GetName(Index, Name) ) {

					CFilename SubSrc = Src + Name;

					CFilename SubDst = Dst + Name;

					CopyDir(SubSrc, SubDst, uFlags);
					}
				}
			
			continue;
			}

		} while( m_pFileSys1->GetNext(Index) );

	return true;
	}

BOOL CRlosFileCopy::MoveDir(CFilename const &Src, CFilename const &Dst, UINT uFlags)
{
	FSIndex iSrc;

	if( !m_pFileSys1->MoveDir(Src, iSrc) ) {

		return false;
		}

	if( !m_pFileSys2->CreateDir(Dst) ) {

		return false;
		}

	FSIndex iScan = iSrc;

	m_pFileSys1->GetFirst(iScan);

	do {
		if( !m_pFileSys1->IsValid(iScan) ) {

			continue;
			}

		if( m_pFileSys1->IsSpecial(iScan) ) {

			continue;
			}

		if( m_pFileSys1->IsFile(iScan) ) {

			CFilename Name;

			if( m_pFileSys1->GetName(iScan, Name) ) {

				CFilename FileSrc = Src + Name;

				CFilename FileDst = Dst + Name;

				MoveFile(FileSrc, FileDst, uFlags);
				}

			continue;
			}

		if( m_pFileSys1->IsDirectory(iScan) ) {

			if( uFlags & copyRecursive ) { 

				CFilename Name;

				if( m_pFileSys1->GetName(iScan, Name) ) {

					CFilename SubSrc = Src + Name;

					CFilename SubDst = Dst + Name;

					MoveDir(SubSrc, SubDst, uFlags);
					}
				}

			continue;
			}

		} while( m_pFileSys1->GetNext(iScan) );

	if( IsDirectoryEmpty(m_pFileSys1, iSrc) ) {

		return m_pFileSys1->RemoveDir(Src);
		}

	return true;
	}

// File / Directory

BOOL CRlosFileCopy::IsDirectoryEmpty(IFilingSystem *pFileSys, FSIndex const &Index)
{
	FSIndex iScan = Index;

	do {
		if( pFileSys->IsSpecial(iScan) ) {

			continue;
			}

		if( pFileSys->IsFile(iScan) ) {

			return false;
			}
			
		if( pFileSys->IsDirectory(iScan) ) {

			return false;
			}

		} while( pFileSys->GetNext(iScan) );

	return true;
	}

BOOL CRlosFileCopy::HasChanged(void)
{
	if( m_pFile1->GetLength() != m_pFile2->GetLength() ) {

		return true;
		}

	if( m_pFile1->GetUnixTime() != m_pFile2->GetUnixTime() ) {

		return true;
		}

	return false;
	}

BOOL CRlosFileCopy::IsMoreRecent(void)
{
	return m_pFile1->GetUnixTime() > m_pFile2->GetUnixTime();
	}

// Implementation

BOOL CRlosFileCopy::LockFileSystems(CFilename const &Src, CFilename const &Dst)
{
	FreeFileSystems();

	if( LockFileSystem(Src, m_pFileSys1) ) {
	
		if( LockFileSystem(Dst, m_pFileSys2) ) {

			return true;
			}

		FreeFileSystem(m_pFileSys1);
		}

	return false;
	}

void CRlosFileCopy::FreeFileSystems(void)
{
	FreeFileSystem(m_pFileSys1);
	
	FreeFileSystem(m_pFileSys2);
	}

BOOL CRlosFileCopy::LockFileSystem(CFilename const &Name, IFilingSystem *&pFileSystem)
{
	pFileSystem = m_pFileMan->LockFileSystem(Name.HasDrive() ? Name.GetDrive() : 'C');

	return pFileSystem != NULL;
	}

void CRlosFileCopy::FreeFileSystem(IFilingSystem *&pFileSys)
{
	m_pFileMan->FreeFileSystem(pFileSys, true);

	pFileSys = NULL;
	}

void CRlosFileCopy::MakeFiles(void)
{
	m_pFile1 = Create_File(m_pFileSys1);

	m_pFile2 = Create_File(m_pFileSys2);
	}

void CRlosFileCopy::FreeFiles(void)
{
	FreeFile(m_pFile1);

	FreeFile(m_pFile2);
	}

void CRlosFileCopy::FreeFile(IFile *&pFile)
{
	if( pFile ) {

		pFile->Close();

		pFile->Release();

		pFile = NULL;
		}
	}

// Buffer

void CRlosFileCopy::AllocBuffer(UINT uSize)
{
	m_pBuffer = New BYTE[ uSize ];

	m_uBuffer = uSize;
	}

void CRlosFileCopy::FreeBuffer(void)
{
	if( m_pBuffer ) {

		delete [] m_pBuffer;

		m_pBuffer = NULL;

		m_uBuffer = 0;
		}
	}
	
// End of File
