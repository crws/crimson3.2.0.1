
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_Fat16FileSystem_HPP

#define	INCLUDE_Fat16FileSystem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

#include "FatFileSystem.hpp"

#include "Fat16BootSector.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Fat16 File System
//

class CFat16FileSystem : public CFatFileSystem
{
	public:
		// Constructor
		CFat16FileSystem(UINT iDisk, UINT iVolume, UINT uBoot);

		// Destructor
		~CFat16FileSystem(void);

		// Attributes
		INT64 GetSize(void);
		INT64 GetFree(void);

		// Methods
		BOOL Init(void);
		BOOL Start(void);

		// Enumeration
		BOOL GetRoot(FSIndex &Index);
		BOOL IsRoot(FSIndex const &Index);

	protected:
		// Constants
		enum
		{
			entryEoc	= 0xFFFF,
			entryReserved	= 0xFFF8,
			entryBad	= 0xFFF7,
			entryFree	= 0x0000,
			entryVolClean	= 0x0800,
			entryHardError	= 0x0400,
			};

		// Data
		CFat16BootSector m_Boot;

		// Layout
		UINT GetFirstSector(UINT uCluster) const;

		// Implementation
		BOOL ReadBoot(void);
		BOOL InitFat(void);
		BOOL InitRoot(void);

		// Flags
		BOOL IsVolumeClean(void);
		BOOL IsVolumeDirty(void);
		BOOL HasHardError(void);
		BOOL SetVolumeClean(void);
		BOOL SetVolumeDirty(void);
		BOOL SetHardError(void);

		// Data Allocation
		UINT AllocData(UINT uAlloc, BOOL fZero);
		UINT ReAllocData(UINT uCluster, UINT uAlloc, BOOL fZero);
		BOOL FreeData(UINT uCluster);

		// Chain
		UINT FindNext(UINT uCluster);
		UINT FindNext(UINT uCluster, UINT uCount);
		UINT FindLast(UINT uCluster);
		UINT FindCount(UINT wCluster);
		UINT FindFree(void);
		UINT FindFreeCount(void);

		// Fat Entry
		UINT ReadFatEntry(UINT uCluster);
		BOOL WriteFatEntry(UINT uCluster, WORD wEntry);
		UINT ReadFatEntry(UINT uFat, UINT uCluster);
		BOOL WriteFatEntry(UINT uFat, UINT uCluster, WORD wEntry);
		BOOL MapFatEntry(UINT uFat, UINT uCluster, CDataSector &Ptr) const;

		// Data
		BOOL ReadCluster(WORD wCluster, PBYTE pData);
		BOOL WriteCluster(WORD wCluster, PCBYTE pData);
		BOOL ReadCluster(WORD wCluster, PBYTE pData, UINT uCount);
		BOOL WriteCluster(WORD wCluster, PCBYTE pData, UINT uCount);
	};

// End of File

#endif

